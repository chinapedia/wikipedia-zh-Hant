**特利騰大公會議**（，又譯**脫利騰會議**、**特倫多會議**、**特倫特會議**、**特倫托會議**或**天特會議**），是指[天主教會於](../Page/天主教會.md "wikilink")1545年至1563年間在[北義大利的](../Page/北義大利.md "wikilink")[特倫托與](../Page/特倫托.md "wikilink")[波隆那召開的](../Page/波隆那.md "wikilink")[大公會議](../Page/大公會議.md "wikilink")。這個會議是天主教會最重要的大公會議，促使該會議的原因是因為[馬丁·路德的](../Page/馬丁·路德.md "wikilink")[宗教改革](../Page/宗教改革.md "wikilink")，也有人把這會議形容為[反宗教改革的方案](../Page/反宗教改革.md "wikilink")，因當中代表了天主教會對宗教改革的決定性回應。\[1\]

除了法令之外，會議針對[新教發表譴責並定義何謂](../Page/新教.md "wikilink")[異端](../Page/異端.md "wikilink")，也對於天主教會的教義和教導作出澄清。這涉及了廣泛的神學問題，包括了[宗教經典](../Page/宗教經典.md "wikilink")、[正典](../Page/正典.md "wikilink")、[聖傳](../Page/聖傳.md "wikilink")、[原罪](../Page/原罪.md "wikilink")、[稱義](../Page/稱義.md "wikilink")、[救恩](../Page/救恩.md "wikilink")、[聖禮](../Page/聖禮.md "wikilink")、[彌撒和](../Page/彌撒.md "wikilink")[敬奉](../Page/敬奉.md "wikilink")。\[2\]會議在1545年12月13日至1563年12月4日期間共在[特倫托開了](../Page/特倫托.md "wikilink")25次會議，除了在1547年也在[波隆那開了大約](../Page/波隆那.md "wikilink")9至11次的會議。\[3\]而會議的召集人[保祿三世主持了頭八次的會議](../Page/保祿三世.md "wikilink")，而第12至16次會議由[儒略三世主持](../Page/儒略三世.md "wikilink")，最後17至25次的會議由[庇護四世主持](../Page/庇護四世.md "wikilink")。

該會議也對天主教儀式和實踐有顯著的影響。在商議的期間，會議用了[武加大譯本作為官方聖經版本和委託人創立一個標準的版本](../Page/武加大譯本.md "wikilink")，雖然這個版本在1590年代才完成。\[4\]但在1565年，就在特倫托會議完結後大約一年，[庇護四世發佈](../Page/庇護四世.md "wikilink")《[特利騰信條](../Page/特利騰信條.md "wikilink")》（又稱《律但丁信條》，Tridentine
Creed），以特倫托的拉丁文為信條之名，而他的接班人[庇護五世也分別在](../Page/庇護五世.md "wikilink")1566年發佈了《[羅馬探題](../Page/羅馬探題.md "wikilink")》（Roman
Catechism）、1568年發佈了《[每日頌禱修訂版](../Page/每日頌禱修訂版.md "wikilink")》（The
Breviary）和在1570年發佈了《彌撒書》（Missal）。這些書引申出維持四百年的[脫利騰彌撒儀式](../Page/脫利騰彌撒.md "wikilink")，直到350年後的[第一次梵蒂岡大公會議召開](../Page/第一次梵蒂岡大公會議.md "wikilink")。

## 背景資料

在十六世紀初期，羅馬教會不能忽視這個時期的神學爭論，這是很顯然的。教會也不願如此，教會中有許多天主教徒甚至天主教領袖，對改教派所宣揚的某些教義，頗表同情。有許多教義的本身，不是改教派所產生的，而是中古教會的產物，因為中古教會裡有些人對於迷信的習俗，與教士的腐化墜落，久已不滿。在教皇反對[馬丁·路德的教諭公佈以後](../Page/馬丁·路德.md "wikilink")，[馬丁·路德向教會總議會的上訴](../Page/馬丁·路德.md "wikilink")，不是沒有人注意的。反對召開總議會的是教皇本人，他認為這次上訴為不尊重他是「基督的代表」的地位。但要給改教派一個清楚的正式答覆，關於天主教教義的認真討論是有其必要的，這是清清楚楚的事實。

### 會議前的攔阻及事件

於1517年3月15日，[第五次拉特蘭大公會議已經停止](../Page/第五次拉特蘭大公會議.md "wikilink")，當時在討論不同方面的建議，包括主教的遴選、稅收、審查、講道，停止相應行動，但不包括德意志教會及其他歐洲教會的主要問題。數個月後，1517年10月31日，[馬丁·路德於](../Page/馬丁·路德.md "wikilink")[維滕貝格發表](../Page/維滕貝格.md "wikilink")《[九十五條論綱](../Page/九十五條論綱.md "wikilink")》。

### 德意志的議會

過了一段時間，[馬丁·路德在](../Page/馬丁·路德.md "wikilink")[大公會議的地位有所轉變](../Page/大公會議.md "wikilink")，\[5\]但在1520年，他就反對[教廷](../Page/教廷.md "wikilink")，上訴到[神聖羅馬皇帝](../Page/神聖羅馬皇帝.md "wikilink")，提出如有必要，利用德意志議會，\[6\]開放教皇權。教皇頒布《[主，請起來](../Page/主，請起來.md "wikilink")》
（Exsurge
Domine）的[敕令](../Page/敕令.md "wikilink")，譴責路德的[九十五條論綱為異端後](../Page/九十五條論綱.md "wikilink")，德意志輿論認為舉行議會是協調當時分歧的最好方法。日益減少的德意志天主教徒，希望議會能澄清問題。\[7\]
議會花了接近一代的時間才能兌現，部分原因是教皇的攔阻，因路德要求把教皇從議會中剔除，另一方面是因為法國和德意志持續的政治鬥爭，和土耳其在[地中海的威脅](../Page/地中海.md "wikilink")。\[8\]

特倫托會議是羅馬天主教[內部革新的主軸](../Page/反宗教改革.md "wikilink")。正當[基督新教興盛之時](../Page/新教.md "wikilink")，[羅馬天主教會持續腐化](../Page/羅馬天主教會.md "wikilink")，[英國也宣布國王的權力凌駕於](../Page/英國.md "wikilink")[教皇](../Page/教皇.md "wikilink")。天主教會內部召開革新會議的呼聲，從下層[教士起以至大學裡都不絕於耳](../Page/教士.md "wikilink")，最後連[查理五世也決定要解決這些宗教問題](../Page/查理五世.md "wikilink")。[查理五世是](../Page/查理五世.md "wikilink")[宗教改革運動的激烈反對者](../Page/宗教改革運動.md "wikilink")，為了促成[基督新教與天主教合一](../Page/基督新教.md "wikilink")，他多次去不同的地方與人尋求協議。[查理五世強烈贊成舉行議會](../Page/查理五世.md "wikilink")，但他需要法國國王[法蘭西斯一世的支持](../Page/法蘭西斯一世.md "wikilink")，[法蘭西斯一世卻在軍事上攻擊他](../Page/法蘭西斯一世.md "wikilink")。

[法蘭西斯一世反對一個常務會議的原因是](../Page/法蘭西斯一世.md "wikilink")，法國中有部分[新教教徒表示支持](../Page/新教徒.md "wikilink")，在1533年，他進一步把事情複雜化，提出常務議會中要包含歐洲的[天主教和](../Page/天主教.md "wikilink")[新教的統治者](../Page/新教.md "wikilink")，並就新教與[舊教當中的神學糾紛之間作出妥協](../Page/舊教.md "wikilink")。這提議因為提議肯定和認同[新教徒](../Page/新教徒.md "wikilink")，以及在教會事務中提升了歐洲[世俗](../Page/世俗.md "wikilink")[君王的地位](../Page/君王.md "wikilink")，高過[神職人員之上](../Page/神職人員.md "wikilink")，遭到教皇的反對。查理當時面對[奧圖曼土耳其的攻擊](../Page/奧圖曼土耳其.md "wikilink")，為了保留[德意志新教](../Page/德意志.md "wikilink")[王公的支持](../Page/王公.md "wikilink")，推遲特倫托會議的開始。\[9\][教皇](../Page/教皇.md "wikilink")[保羅三世](../Page/保羅三世.md "wikilink")（Pope
Paul
III）終於在壓力之下，同意在1545年召開首次特倫托會議，處理[教會](../Page/教會.md "wikilink")[改革和應付與日俱增的更正教的威脅](../Page/改革.md "wikilink")，宣告歐洲[天主教勢力](../Page/天主教.md "wikilink")[反宗教改革的浪潮的開始](../Page/反宗教改革.md "wikilink")。

## 場合，會議與出席

## 歷程

特倫托會議前後共召開過三輪，初期出席會議者僅[意大利](../Page/意大利.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[德意志與](../Page/德意志.md "wikilink")[法國等四個地區的教士](../Page/法國.md "wikilink")，[英國沒有派人參加](../Page/英國.md "wikilink")，其中意大利所派的人數比其他三個地方的總人數還多，因此整個會議都被羅馬教皇控制，由教皇所提出的議案幾乎都能獲得支持而通過。

第二輪於1551年召開，於1552年[儒略三世时中断](../Page/儒略三世.md "wikilink")。1562年，[庇護四世召开第三輪特倫托會議](../Page/庇護四世.md "wikilink")。會議上，[罗马受到来自](../Page/罗马.md "wikilink")[法国和](../Page/法国.md "wikilink")[西班牙的](../Page/西班牙.md "wikilink")[压力](../Page/压力.md "wikilink")，但[庇護四世仍然取得了满意的成果](../Page/庇護四世.md "wikilink")。會議致力于对当时天主教会内部的改革，建立培养[神职人员的](../Page/神职人员.md "wikilink")[修道院等](../Page/修道院.md "wikilink")。会议的结果最终颁布為[天特會議信綱](../Page/天特會議信綱.md "wikilink")（Professio
fidei
tridentina，又譯脫利騰信德宣言）。當大會於1563年12月4日閉幕的時候，前後共召開過二十五場討論，用了近十八年的時間。共有四位[教宗使節](../Page/教宗.md "wikilink")、三位[宗主教](../Page/宗主教.md "wikilink")、二十五位[總主教](../Page/總主教.md "wikilink")、一百二十九位[主教](../Page/主教.md "wikilink")、七位[修道院院長](../Page/修道院.md "wikilink")、七位天主教特別團體的領袖，十位[檢察官與歐洲一些天主教國家的](../Page/檢察官.md "wikilink")[大使聯合起來](../Page/大使.md "wikilink")，共同簽署這個大會所通過的繁多諭令。庇護四世當時正臥病在床，但回應此會議的結論說：「這一切都是天主[聖神所感動出來的成果](../Page/聖神.md "wikilink")。」一位[樞機主教曾這樣描述](../Page/樞機主教.md "wikilink")：「在教會史上，沒有任何大會決定過這麼多問題，確立過這麼多教義，或者制定過這麼多法規。」

## 議決摘要

是次為羅馬天主教會之革新運動，被稱為「[反改教運動」](../Page/反宗教改革.md "wikilink")（Counter
Reformation）。部分議決內容茲摘要如下：

  - 取消[会吏长一職](../Page/会吏长.md "wikilink")：会吏长即总执事，初期由主教任命，协助主教处理事务。[中世纪时](../Page/中世纪.md "wikilink")，由于权力增大，时有妄用职权之弊，故在特倫托會議中予以取消。
  - 第三次特倫托會議重申教徒必須繳付[什一稅](../Page/什一稅.md "wikilink")。
  - 廢止售賣[贖罪券的議案在第三次特倫托會議上被提出](../Page/贖罪券.md "wikilink")，到了1567年獲[庇護五世](../Page/庇護五世.md "wikilink")[批准](../Page/批准.md "wikilink")。
  - 對於[改革宗一向秉持為教義的](../Page/改革宗.md "wikilink")「[Eternal
    Security](../Page/圣徒蒙保守.md "wikilink")」神學觀念，特倫托會議頒佈：「如果有人說，[一旦稱義便不會失去救恩](../Page/一次得救永遠得救.md "wikilink")，因此，跌倒犯[罪的人從來就沒有稱義過](../Page/罪_\(宗教\).md "wikilink")！這人是受絕罰的」
  - 为了对抗[基督新教的影响](../Page/基督新教.md "wikilink")，特倫托會議重新肯定[耶柔米的](../Page/耶柔米.md "wikilink")[武加大譯本为圣经权威版本](../Page/武加大譯本.md "wikilink")，会议委托教皇制定一部标准文本。
  - 制定Tridentine
    Mass禮儀，即[梵二前用](../Page/第二次梵諦岡大公會議.md "wikilink")[拉丁文舉行的](../Page/拉丁文.md "wikilink")[彌撒禮儀](../Page/彌撒禮儀.md "wikilink")。
  - 在教理方面，教會採取「完全閉關主義」，對任何中世紀教理的攻擊不作妥協的主張，也不加修改\[10\]\[11\]

### 對新教的回應

對於[馬丁·路德極力攻擊的各項](../Page/馬丁·路德.md "wikilink")[教義問題](../Page/教義.md "wikilink")，天特會議有以下的決議：

  - 羅馬教會之一切教會聖傳，與《[聖經](../Page/聖經.md "wikilink")》具有同等地位。
  - 所有[基督徒必須承認教宗之地位](../Page/基督徒.md "wikilink")。
  - 宣佈馬丁·路德所謂因信仰而獲贖罪（[因信稱義](../Page/因信稱義.md "wikilink")）之說為[異端](../Page/異端.md "wikilink")。
  - 羅馬教會之所有聖職者、主教及總主教都必須以[基督之清靜生活為](../Page/基督.md "wikilink")[道德標準](../Page/道德.md "wikilink")。\[12\]

特倫托會議針對馬丁·路德的各種「反改革」議決，很大程度上卻改革了羅馬教會。

## 影響

[查理五世决心惩罚](../Page/查理五世_\(神聖羅馬帝國\).md "wikilink")[德意志的](../Page/德意志.md "wikilink")[基督新教](../Page/基督新教.md "wikilink")[王公](../Page/王公.md "wikilink")，此會議顯出[教皇制度的成功](../Page/教皇制度.md "wikilink")，肯定了[教皇的最高權柄](../Page/教皇.md "wikilink")；糾正許多教會弊端；為聖職人員預備更好的[教育](../Page/教育.md "wikilink")，也對神職人員有些規定：在[大城市教會中](../Page/大城市.md "wikilink")，規定要向會眾講解[聖經及](../Page/聖經.md "wikilink")[得救之道](../Page/得救.md "wikilink")；教士要駐在任職，不得兼任數職等。總的而言，特倫托會議給了[羅馬天主教在往後世紀有明確立場](../Page/羅馬天主教.md "wikilink")，對其後展開的佈道宣教及[宗教戰爭有極大的幫助](../Page/宗教戰爭.md "wikilink")。

## 参见

  - [脱利腾弥撒](../Page/脱利腾弥撒.md "wikilink")

## 参考文献

## 外部連結

  -
  - [The text of the Council of
    Trent](http://history.hanover.edu/texts/trent.html) translated by J.
    Waterworth, 1848

  - [Documents of the Council in
    latin](http://www.documentacatholicaomnia.eu/01_10_1545-1563-_Concilium_Tridentinum.html)

{{-}}

[Category:基督教神学](../Category/基督教神学.md "wikilink")
[Category:基督教會議](../Category/基督教會議.md "wikilink")
[Category:天主教历史](../Category/天主教历史.md "wikilink")
[Category:1545年](../Category/1545年.md "wikilink")
[Category:1549年](../Category/1549年.md "wikilink")
[Category:1562年](../Category/1562年.md "wikilink")
[Category:大公會議](../Category/大公會議.md "wikilink")

1.  "Trent, Council of" in Cross, F. L. (ed.) The Oxford Dictionary of
    the Christian Church, Oxford University Press, 2005

2.  Wetterau, Bruce. World History. New York: Henry Holt and Company,
    1994.

3.  Hubert Jedin, Konciliengeschichte, Verlag Herder, Freiburg, \[p.?\]
    138

4.
5.  Jedin, Hubert (1959), Konziliengeschichte, Herder, p. 80

6.  An den Adel deutscher Nation (in German), 1520

7.  Jedin 81

8.  Jedin 81

9.  Jedin 79–82

10. [華爾克著](../Page/華爾克.md "wikilink")。《基督教會史》。香港：基督教文藝出版社，1990）661-2

11. [祁伯爾著](../Page/祁伯爾.md "wikilink")。林靜芝譯。《歷史的軌跡──二千年教會史》。台北：校園書房出版社，2003）283-4

12. 馮作民，《西洋全史八─宗教改革》，（台北：燕京文化事業股份有限公司，1976），150-152。