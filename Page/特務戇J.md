《**特務戇J**》（）是一部於2003年上映的[英國](../Page/英國.md "wikilink")[喜劇電影](../Page/喜劇電影.md "wikilink")，由執導，也是《[戇豆先生](../Page/戇豆先生.md "wikilink")》[罗温·阿特金森主演的作品之一](../Page/路雲·雅堅遜.md "wikilink")。《特務戇J》与[罗温·阿特金森主演的](../Page/路雲·雅堅遜.md "wikilink")《憨豆先生》系列并没有任何联系，惟二者的主人公之扮演者为同一人且性格十分相似而得名。

## 剧情

片中的主角尊尼·英烈（[路雲·雅堅遜飾](../Page/路雲·雅堅遜.md "wikilink")），外號「憨豆」，是英國的特務之一。

一次情報部收到線報，指有人會於將舉行的英國皇室珠寶展中盜走寶物，便派出一號精英特務追查，但不幸[殉職](../Page/殉職.md "wikilink")，在情報部人手凋零之下，便派出「戇J」接手該案，但由於「戇J」智勇俱無，寶物還是於展出當日被偷走。及後查出其幕後策劃者為法國商人蘇華（[約翰·馬克維奇飾](../Page/約翰·馬克維奇.md "wikilink")），他在英國皇室族譜中發現自己是皇族後裔，為了奪取權力，便策劃其篡奪王位的陰謀，在一秘密會議上表示會在英國興建全球最大監獄，把全球犯人均囚在英國，並威逼英女王退位。

「戇J」與一名女特務（[娜塔莉·安博莉亚飾](../Page/娜塔莉·安博莉亚.md "wikilink")）潛入蘇華的基地，但被他們發現，並投進監獄之中，幸得另一特務波夫的協助，他們得以逃走。

在蘇華的登基儀式上，「戇J」聯同女特務一同阻止其加冕，皇冠突然落在「戇J」頭上，「戇J」瞬即得到權力，在「英皇戇J」的命令下，蘇華被捕，並以[叛國罪起訴](../Page/叛國罪.md "wikilink")。最後英女王復位，並冊封「戇J」為[爵士](../Page/爵士.md "wikilink")。

## 角色

  - [路雲·雅堅遜](../Page/路雲·雅堅遜.md "wikilink") 飾演 強尼·英格力
  - [本·米勒](../Page/本·米勒.md "wikilink") 飾演 Angus Bough
  - [約翰·馬克維奇](../Page/約翰·馬克維奇.md "wikilink") 飾演 Pascal Edward Sauvage
  - [娜塔莉·安博莉亚](../Page/娜塔莉·安博莉亚.md "wikilink") 飾演 洛娜·坎貝爾

## 評價

[爛番茄新鮮度](../Page/爛番茄.md "wikilink")33%，基於118條評論，平均分為4.8/10\[1\]，而在[Metacritic上得到](../Page/Metacritic.md "wikilink")51分\[2\]，[IMDB上得](../Page/IMDB.md "wikilink")6.1分，差評居多。

## 續集

續集改由[奧利弗·帕克執導](../Page/奧利弗·帕克.md "wikilink")，並於2011年9月15日上映。

## 参考资料

## 外部網站

  -
  -
  -
  -
  -
  -
  - {{@movies|fjen50274166}}

  -
  -
  -
[Category:2003年电影](../Category/2003年电影.md "wikilink")
[Category:英语电影](../Category/英语电影.md "wikilink")
[Category:英國電影作品](../Category/英國電影作品.md "wikilink")
[Category:英國動作片](../Category/英國動作片.md "wikilink")
[Category:英國喜劇片](../Category/英國喜劇片.md "wikilink")
[Category:英國間諜片](../Category/英國間諜片.md "wikilink")
[Category:喜劇間諜片](../Category/喜劇間諜片.md "wikilink")
[Category:倫敦背景電影](../Category/倫敦背景電影.md "wikilink")
[Category:倫敦取景電影](../Category/倫敦取景電影.md "wikilink")
[Category:香港取景電影](../Category/香港取景電影.md "wikilink")
[Category:2000年代喜劇片](../Category/2000年代喜劇片.md "wikilink")
[Category:虚构特务与间谍](../Category/虚构特务与间谍.md "wikilink")
[Category:环球影业电影](../Category/环球影业电影.md "wikilink")
[Category:通道影業電影](../Category/通道影業電影.md "wikilink")

1.
2.