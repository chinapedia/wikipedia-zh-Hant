《**-{zh:特務行不行;zh-hans:特务行不行;zh-hant:特務行不行;zh-cn:糊涂侦探;zh-hk:特務S嘜;zh-mo:特務S嘜;zh-sg:特务行不行;zh-tw:特務行不行;}-**》（）是一部於2008年上映的[美國](../Page/美國.md "wikilink")[喜劇](../Page/喜劇片.md "wikilink")[動作片](../Page/動作片.md "wikilink")，為[彼德·席格執導](../Page/彼德·席格.md "wikilink")，劇情改編自60年代深受美國民眾喜愛的喜劇影集《[糊塗情報員](../Page/糊塗情報員.md "wikilink")》，[華納兄弟出品](../Page/華納兄弟.md "wikilink")，由[史提夫·卡爾](../Page/史提夫·卡爾.md "wikilink")、[安妮·海瑟薇](../Page/安妮·海瑟薇.md "wikilink")、[巨石強森](../Page/巨石強森.md "wikilink")、[亞倫·阿金](../Page/亞倫·阿金.md "wikilink")、等主演。

電影上映後，儘管獲得兩極的評價，仍以8000萬美元的預算，收穫了2.3億美元的票房佳績\[1\]\[2\]。

## 劇情

情報分析員麥斯終於夢想成真，成為美國間諜情報局特控辦的特務，與偶像星級特務23共事，並被委派與外表漂亮兼身手了得的特務99合作，搗破邪惡犯罪組織亂搞黨企圖控制全世界的奸計。

## 演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>香港配音</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/史提夫·卡爾.md" title="wikilink">史提夫·卡爾</a></p></td>
<td><p>麥斯威爾·斯麥特<br />
Maxwell Smart</p></td>
<td><p><a href="../Page/陳欣.md" title="wikilink">陳欣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安妮·海瑟薇.md" title="wikilink">安妮·海瑟薇</a></p></td>
<td><p>特務99</p></td>
<td><p><a href="../Page/張頌欣.md" title="wikilink">張頌欣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巨石強森.md" title="wikilink">巨石強森</a></p></td>
<td><p>特務23</p></td>
<td><p><a href="../Page/招世亮.md" title="wikilink">招世亮</a></p></td>
</tr>
</tbody>
</table>

## 行銷

《特務行不行》電影戲院上映後十一天，即2008年7月1日發行電影延伸劇情《[特務到底行不行](../Page/特務到底行不行.md "wikilink")》（Get
Smart's Bruce and Lloyd: Out of
Control）[DVD](../Page/DVD.md "wikilink")。內容為獨立於電影外的故事，事件與電影同步進行發生。

除了電視廣告與電影預告片宣傳外，[華納兄弟與](../Page/華納兄弟.md "wikilink")[百事可樂合作](../Page/百事可樂.md "wikilink")，生產
Sierra Mist 口味汽水，稱作「Undercover Orange」為電影宣傳。同時建立線上論壇「CONTROL Vs.
KAOS」，訪客可以參與任務競賽等。另有七分鐘的宣傳影片，是電影中的動作場面片段，麥斯和特務99於飛機上跳傘，可在[iTunes免費下載](../Page/iTunes.md "wikilink")。

[Subway連鎖餐廳舉辦活動](../Page/Subway.md "wikilink")，贈予電影中展示的1965年的 Sunbeam
Tiger 跑車。

## 評價

[爛番茄新鮮度](../Page/爛番茄.md "wikilink")51%，基於214條評論，平均分為5.5/10，觀眾評價高達67%\[3\]；而在[Metacritic上得到](../Page/Metacritic.md "wikilink")54分\[4\]，評價兩極，且多數好評傾向觀眾。

## 票房

美國首映週末收穫3868萬美元位居當週冠軍，台灣首週獲1043萬新台幣奪冠。

## 參見

  - 《[糊塗情報員](../Page/糊塗情報員.md "wikilink")》
  - 《[特務到底行不行](../Page/特務到底行不行.md "wikilink")》

## 參考來源

## 外部連結

  -
  - {{@movies|fgen50425061|特務行不行}}

  -
  -
  -
  -
  -
  -
  -
  -
[Category:2008年電影](../Category/2008年電影.md "wikilink")
[Category:2000年代動作片](../Category/2000年代動作片.md "wikilink")
[Category:2000年代喜劇片](../Category/2000年代喜劇片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:俄語電影](../Category/俄語電影.md "wikilink")
[Category:美國喜劇動作片](../Category/美國喜劇動作片.md "wikilink")
[Category:喜劇間諜片](../Category/喜劇間諜片.md "wikilink")
[Category:華納兄弟電影](../Category/華納兄弟電影.md "wikilink")
[Category:亞特拉斯娛樂電影](../Category/亞特拉斯娛樂電影.md "wikilink")
[Category:威秀电影](../Category/威秀电影.md "wikilink")

1.
2.
3.
4.