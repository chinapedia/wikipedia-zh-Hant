**特吕格韦·哈尔夫丹·赖伊**\[1\]（[挪威语](../Page/挪威语.md "wikilink")：****，），[挪威外交家](../Page/挪威.md "wikilink")，首任正式的[联合国秘书长](../Page/联合国秘书长.md "wikilink")。

赖伊出生于[奥斯陆](../Page/奥斯陆.md "wikilink")，1911年加入[挪威工党](../Page/挪威工党.md "wikilink")，1919年他自[奥斯陆大学法学院毕业后出任该党秘书长](../Page/奥斯陆大学.md "wikilink")。1922年他取得了律师资格，并于当年被选入[挪威议会](../Page/挪威议会.md "wikilink")。1935年，他加入[约翰·尼高斯沃尔内阁](../Page/约翰·尼高斯沃尔.md "wikilink")，任司法大臣，不久转任贸易大臣。

赖伊曾是[十月革命的拥护者](../Page/十月革命.md "wikilink")，他曾与[列宁会面](../Page/列宁.md "wikilink")，但在[一月剧变后对布尔什维克改变了态度](../Page/一月剧变.md "wikilink")。在[托洛茨基被](../Page/托洛茨基.md "wikilink")[斯大林驱逐出境后接纳他在挪威避难](../Page/斯大林.md "wikilink")，但是不久后，由于托洛茨基没有遵守其“不涉足政治”的诺言，而被赖伊要求离境。

1940年，[德国占领挪威](../Page/德国.md "wikilink")，赖伊命令挪威的所有船只开赴[英国港口以避免落入德国手中](../Page/英国.md "wikilink")。随后，赖伊被任命为挪威流亡政府外长。

1946年，赖伊率领挪威代表团出席[联合国的成立大会](../Page/联合国.md "wikilink")，并对[联合国安全理事会的设立多有贡献](../Page/联合国安全理事会.md "wikilink")。2月1日，他被选为首任正式的联合国秘书长。他的当选是[美国和](../Page/美国.md "wikilink")[苏联两大势力妥协的结果](../Page/苏联.md "wikilink")。

在联合国秘书长任内，他支持[以色列和](../Page/以色列.md "wikilink")[印度尼西亚的独立运动](../Page/印度尼西亚.md "wikilink")，并促使苏联从[伊朗撤军](../Page/伊朗.md "wikilink")，以及[印度和](../Page/印度.md "wikilink")[巴基斯坦在](../Page/巴基斯坦.md "wikilink")[克什米尔停火](../Page/克什米尔.md "wikilink")。1950年，[朝鲜战争爆发](../Page/朝鲜战争.md "wikilink")，他支持[韩国的态度导致了苏联不满](../Page/韩国.md "wikilink")，随后，他耗费了大量精力以争取苏联结束对联合国会议的抵制。赖伊还反对[西班牙加入联合国](../Page/西班牙.md "wikilink")，因为他反对[佛朗哥在西班牙的独裁统治](../Page/佛朗哥.md "wikilink")；并主张接纳[中华人民共和国取代](../Page/中华人民共和国.md "wikilink")[中华民国作为](../Page/中华民国.md "wikilink")[中国在联合国的正式代表](../Page/中国.md "wikilink")。

由于未能迅速处理[柏林危机和朝鲜战争](../Page/柏林危机.md "wikilink")，赖伊受到了大量的批评。批评人士认为他远未发挥联合国秘书长一职所应有的影响力，而且过于傲慢和固执。

1950年，由于苏联抵制[联合国大会](../Page/联合国大会.md "wikilink")，且美国表示不接受赖伊之外的任何人选担任联合国秘书长，赖伊得以顺利连任。但由于苏联强烈反对其继续任职，且美国参议员[约瑟夫·麦卡锡也指责其雇用](../Page/约瑟夫·麦卡锡.md "wikilink")“不忠诚的美国人”，赖伊被迫于1952年11月10日辞职。

辞职后，赖伊一度出任奥斯陆地方首长、挪威能源委员会主席、内政部长、贸易部长等职务。他此后再未受到广泛欢迎，被认为是一位务实果断但缺乏才能的政客。

賴伊於1968年12月30日逝世，著有《為了和平》（1954年出版）。

## 注释

## 外部链接

  - [赖伊的联合国官方简历](http://www.un.org/Overview/SG/sg1bio.html)

{{-}}

{{-}}

[Category:联合国秘书长](../Category/联合国秘书长.md "wikilink")
[Category:挪威外交大臣](../Category/挪威外交大臣.md "wikilink")
[Category:挪威國會議員](../Category/挪威國會議員.md "wikilink")
[Category:挪威政治人物](../Category/挪威政治人物.md "wikilink")
[Category:挪威工黨黨員](../Category/挪威工黨黨員.md "wikilink")
[Category:朝鮮戰爭人物](../Category/朝鮮戰爭人物.md "wikilink")
[Category:挪威第二次世界大戰人物](../Category/挪威第二次世界大戰人物.md "wikilink")
[Category:挪威信義宗教徒](../Category/挪威信義宗教徒.md "wikilink")
[Category:奧斯陸大學校友](../Category/奧斯陸大學校友.md "wikilink")
[Category:奧斯陸人](../Category/奧斯陸人.md "wikilink")

1.  在[挪威语中](../Page/挪威语.md "wikilink")，姓氏“Lie”的发音更接近“利”，“赖伊”系根据英语发音译出。根据《[世界人名翻译大辞典](../Page/世界人名翻译大辞典.md "wikilink")》，译作“赖伊”，其他Lie姓译作“利”，参考：