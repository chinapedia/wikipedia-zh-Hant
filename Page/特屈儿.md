**特屈儿**（**Tetryl**），也称**2,4,6-三硝基苯甲硝胺**，是一种[炸药](../Page/炸药.md "wikilink")，分子式为[C](../Page/碳.md "wikilink")<sub>7</sub>[H](../Page/氢.md "wikilink")<sub>5</sub>[N](../Page/氮.md "wikilink")<sub>5</sub>[O](../Page/氧.md "wikilink")<sub>8</sub>。纯品为白色无味晶体，但不纯或受光时会发黄。微溶于水。

特屈儿的热安定性较好，对摩擦及撞击都不很敏感，[爆速可达](../Page/爆速.md "wikilink")7570m/s。它的生产方法有多种，比如：

  - 由浓[硝酸与](../Page/硝酸.md "wikilink")[硫酸的混酸与](../Page/硫酸.md "wikilink")[二甲基苯胺反应生产](../Page/二甲基苯胺.md "wikilink")；
  - [苦味酸与](../Page/苦味酸.md "wikilink")[五氯化磷反应生成三硝基氯苯](../Page/五氯化磷.md "wikilink")，与[甲胺反应生成甲胺基三硝基苯](../Page/甲胺.md "wikilink")，再用硝酸硝化得到；
  - [氯苯硝化生成](../Page/氯苯.md "wikilink")2,4-二硝基氯苯，与甲胺反应生成*N*-甲基-2,4-二硝基苯胺，然后硝化得到。

## 参见

  - [六硝基苯](../Page/六硝基苯.md "wikilink")
  - [三硝基甲苯](../Page/三硝基甲苯.md "wikilink")

## 参考资料

  - Cooper, Paul W., *Explosives Engineering*, New York: Wiley-VCH,
    1996. ISBN 0-471-18636-8

## 外部链接

  - [特屈儿](http://www.atsdr.cdc.gov/tfacts80.html)
  - [Occupational Safety and Health Guideline for
    Tetryl](http://www.osha.gov/SLTC/healthguidelines/tetryl/recognition.html),


[Category:炸藥](../Category/炸藥.md "wikilink")
[Category:硝基化合物](../Category/硝基化合物.md "wikilink")
[Category:胺](../Category/胺.md "wikilink")