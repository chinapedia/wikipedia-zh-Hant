**特技飛車(Hard Drivin')**
是一款[街機遊戲](../Page/街機遊戲.md "wikilink")，可供玩家駕駛大馬力[跑車在障礙賽道上奔馳](../Page/跑車.md "wikilink")。此遊戲開創了以[三維電腦圖形作為駕駛環境的先河](../Page/三維電腦圖形.md "wikilink")。該街機上的方向盤更設有撞車時的[力回饋功能](../Page/力回饋.md "wikilink")，是街機遊戲史上的第一個。

## 玩法

**特技飛車**類似一般的駕駛遊戲。遊戲中的車輛與[法拉利的Testarossa相似](../Page/法拉利.md "wikilink")。這遊戲與其他同期的駕駛遊戲的分別在於它加入了類似[雲霄飛車中的迴圈及其他危險障礙](../Page/雲霄飛車.md "wikilink")。玩家一般需在障礙賽道上駕駛一至兩圈，而在一些模式中玩家更可同電腦控制的跑車Phantom
Photon一較高下。由於這遊戲加入了挑戰玩家的膽量的元素，它與*Out Run*及*Pole
Position*這類一般的賽車遊戲不同。此遊戲更被視為電子遊戲*Stunts*的始祖，或啟發了其製作，因為*Stunts*
無論在視覺上、控制上及賽道均與**特技飛車**很相似。**特技飛車**亦有很真實的傳動系統([變速器](../Page/變速器.md "wikilink"))，即玩家須要如駕駛真車一樣駕馭此遊戲。

## 外部連結

  - [Defunct Games](../Page/Defunct_Games.md "wikilink"): [*Hard Drivin'
    Review*](http://www.defunctgames.com/shows.php?id=review-422)
    ([Atari Lynx](../Page/Atari_Lynx.md "wikilink"))(英文)
  - [*Hard Drivin*
    Resource](https://web.archive.org/web/20051218223831/http://www.mamedb.com/game/harddriv)(英文)
  - [Ferrari F-40 Model Car
    Prize](https://web.archive.org/web/20071031015054/http://amigareviews.classicgaming.gamespy.com/harddri1.htm)(英文)
  - [Arcade-History.com](http://www.arcade-history.com/)上的[*Hard
    Drivin*](http://www.arcade-history.com/?n=hard-drivin'&page=detail&id=1083)(英文)

[H](../Category/1988年电子游戏.md "wikilink")
[H](../Category/1989年电子游戏.md "wikilink")
[H](../Category/街機遊戲.md "wikilink")
[H](../Category/Mega_Drive游戏.md "wikilink")
[H](../Category/DOS遊戲.md "wikilink")
[H](../Category/任天堂GameCube游戏.md "wikilink")
[H](../Category/PlayStation_2游戏.md "wikilink")
[H](../Category/Xbox游戏.md "wikilink")