**特狽路龍**（[學名](../Page/學名.md "wikilink")*Taveirosaurus*）是[鳥臀目的一](../Page/鳥臀目.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於[上白堊紀的](../Page/上白堊紀.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")。特狽路龍是以化石發現地的[葡萄牙特狽路村](../Page/葡萄牙.md "wikilink")（Taveiro）為名。在1991年，Miguel
Telles Antunes與Denise Sigogneau-Russell發現這些化石，並且正式敘述、命名。

特狽路龍最初根據牙齒而被歸類於[厚頭龍下目](../Page/厚頭龍下目.md "wikilink")，但最近的研究並沒有將特狽路龍歸類於此一[演化支](../Page/演化支.md "wikilink")\[1\]\[2\]\[3\]。

## 參考資料

## 外部連結

  - [恐龍名稱及讀音](https://web.archive.org/web/20060315213448/http://www.dinosauria.com/dml/names/dinot.htm)
  - [恐龍博物館──特狽路龍](https://web.archive.org/web/20071109012923/http://www.dinosaur.net.cn/museum/Taveirosaurus.htm)

[Category:鳥臀目](../Category/鳥臀目.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")

1.
2.
3.