**特種部隊**（）普遍指向接受特別及高度強度[軍事訓練的](../Page/軍訓.md "wikilink")[軍事](../Page/軍事.md "wikilink")[單位](../Page/單位.md "wikilink")，專門執行[特種作戰](../Page/特種作戰.md "wikilink")、[偵察](../Page/偵察.md "wikilink")、[滲透](../Page/滲透_\(军事\).md "wikilink")、[狙擊及](../Page/狙擊.md "wikilink")[反恐等非常规作战](../Page/反恐.md "wikilink")。对齐细分可以分為近軍事部隊的特種部隊與軍事化的反恐部隊。反恐部隊主要指以[作戰](../Page/戰爭.md "wikilink")、、[反間諜](../Page/反間諜.md "wikilink")、[國防和](../Page/國防.md "wikilink")[反恐怖主義為任務的軍隊特種部隊](../Page/反恐怖主義.md "wikilink")，可以但不限於稱為警衛連、偵察連、[突擊隊](../Page/突擊隊.md "wikilink")、[特遣隊](../Page/特遣隊.md "wikilink")、[別動隊等](../Page/別動隊.md "wikilink")。在中文語境中，特種部隊一般不包括非軍事單位[特種警察部隊](../Page/特種警察部隊.md "wikilink")。

值得一提的是在美國境內，除了[陸軍特種部隊以外的其他美軍特種作戰單位](../Page/美國陸軍特種部隊.md "wikilink")（如：[海豹部隊](../Page/海豹部隊.md "wikilink")）都被統一的稱為“特種作戰部隊”（Special
Operations Forces），而不是“特種部隊”（Special Forces）。

## 各國特種部隊

###

[Afghan_commandos_take_training_lead.jpg](https://zh.wikipedia.org/wiki/File:Afghan_commandos_take_training_lead.jpg "fig:Afghan_commandos_take_training_lead.jpg")

  - [突擊隊](../Page/突擊隊_\(阿富汗國家軍隊\).md "wikilink")

  -
###

  -
###

  - [澳大利亞國防軍突擊隊](../Page/澳大利亞國防軍突擊隊.md "wikilink")

  -
  -
  - [特種空勤團](../Page/特種空勤團_\(澳大利亞\).md "wikilink")

  -
###

[Wachsam_(19855663715).jpg](https://zh.wikipedia.org/wiki/File:Wachsam_\(19855663715\).jpg "fig:Wachsam_(19855663715).jpg")

  - [眼鏡蛇作戰司令部](../Page/眼鏡蛇作戰司令部.md "wikilink")

  - （Jagdkommando）

###

  -   - 陸軍所屬：
          - 第38衛隊機動旅

          -
          - 第33衛隊特種作戰分遣隊

          - 第527特種作戰連

          - 第22特種作戰連

  - 內務部特種部隊

  - [克格勃](../Page/白羅斯共和國國家安全委員會.md "wikilink")[阿爾法小組](../Page/阿爾法小組_\(白羅斯\).md "wikilink")

###

[SFG_32.jpg](https://zh.wikipedia.org/wiki/File:SFG_32.jpg "fig:SFG_32.jpg")\]\]

  - [特種部隊旅](../Page/特種部隊群.md "wikilink")

###

  -
  -
  - [第二聯合特遣部隊](../Page/第二聯合特遣部隊.md "wikilink")

###

  - [中國人民解放軍陸軍](../Page/中國人民解放軍陸軍.md "wikilink")
      - [中部战区](../Page/中部战区.md "wikilink")[82集团军特战第82旅(响箭特种作战旅)](../Page/中国人民解放军陆军第八十二集团军.md "wikilink")
      - [西部战区](../Page/西部战区.md "wikilink")[77集团军特战第77旅(西南猎鹰特种作战旅)](../Page/中国人民解放军陆军第七十七集团军.md "wikilink")
      - [东部战区](../Page/东部战区.md "wikilink")[73集团军特战第73旅(东海飞龙特种作战旅)](../Page/中国人民解放军陆军第七十三集团军.md "wikilink")
      - [东部战区](../Page/东部战区.md "wikilink")[71集团军特战第71旅（原特战第36旅）](../Page/中国人民解放军陆军第七十一集团军.md "wikilink")
      - [北部战区](../Page/北部战区.md "wikilink")[80集团军特战第80旅(泰山雄鹰特种作战旅)](../Page/中国人民解放军陆军第八十集团军.md "wikilink")
      - [南部战区](../Page/南部战区.md "wikilink")[75集团军特战第75旅（南國利劍特种作战旅）](../Page/中国人民解放军陆军第七十五集团军.md "wikilink")
      - [西部战区](../Page/西部战区.md "wikilink")
        [76集团军特战第76旅（雪枫特种作战旅）](../Page/中国人民解放军陆军第七十六集团军.md "wikilink")
      - [北部战区](../Page/北部战区.md "wikilink")[79集团军特战第79旅(原东北虎特种作战团)](../Page/中国人民解放军陆军第七十九集团军.md "wikilink")
      - [北部战区](../Page/北部战区.md "wikilink")[78集团军特战第78旅（原特战第67旅）](../Page/中国人民解放军陆军第七十八集团军.md "wikilink")
      - [新疆军区特战第84旅(昆仑利刃特种作战旅)](../Page/新疆军区.md "wikilink")\[1\]
      - [西藏军区特战第85旅(原高原雪豹特种作战团)](../Page/西藏军区.md "wikilink")
      - [驻港部队摩步旅特种作战营](../Page/中國人民解放軍駐香港部隊.md "wikilink")
      - [驻澳部队特种作战连](../Page/中國人民解放軍駐澳門部隊.md "wikilink")
  - [中國人民解放軍海軍](../Page/中國人民解放軍海軍.md "wikilink")
      - [海军陆战队特种作战旅（蛟龙突击队）](../Page/中国人民解放军海军陆战队.md "wikilink")
      - [海军陆战队第1旅两栖侦察特战队](../Page/中國人民解放軍海軍陸戰隊.md "wikilink")
      - [海军陆战队第2旅两栖侦察特战队（原陆战第164旅两栖侦察特战队南国雄狮）](../Page/中國人民解放軍海軍陸戰隊.md "wikilink")
  - [中國人民解放軍空軍](../Page/中國人民解放軍空軍.md "wikilink")
      - [中国人民解放军空降兵军特种作战旅（雷神突击队）](../Page/中国人民解放军空降兵军.md "wikilink")
  - [中國人民解放軍火箭軍](../Page/中國人民解放軍火箭軍.md "wikilink")
      - [火箭军特种作战团（利刃）](../Page/火箭军特种作战团（利刃）.md "wikilink")
  - [中國人民武裝警察部隊](../Page/中國人民武裝警察部隊.md "wikilink")
      - [獵鷹突擊隊](../Page/中国人民武装警察部队猎鹰突击队.md "wikilink")
      - [雪豹突擊隊](../Page/中国人民武装警察部队雪豹突击队.md "wikilink")

###

  - [中華民國陸軍](../Page/中華民國陸軍.md "wikilink")
      - [陸軍航空特戰指揮部](../Page/陸軍航空特戰指揮部.md "wikilink")
          - [高空特勤中隊](../Page/高空特種勤務中隊.md "wikilink")
          - [陸軍101兩棲偵察營](../Page/中華民國陸軍101兩棲偵察營.md "wikilink")
          - [中華民國空降特戰部隊 特種作戰指揮部](../Page/陸軍特種作戰指揮部.md "wikilink")
              - 本部連
              - 狙擊連
              - 通資連
              - 特種作戰第一營
              - 特種作戰第二營
              - 特種作戰第三營
              - 特種作戰第四營
              - 特種作戰第五營
  - [中華民國憲兵](../Page/中華民國憲兵.md "wikilink")（官科兵種屬於陸軍，因為有警察權，其指揮部直接隸屬國防部參謀本部，不隸屬陸軍司令部）
      - [憲兵特勤隊](../Page/憲兵特勤隊.md "wikilink")
  - [中華民國海軍](../Page/中華民國海軍.md "wikilink")
      - [海軍水下作業大隊](../Page/海軍水下作業大隊.md "wikilink")
      - [海軍水中爆破大隊](../Page/海軍水中爆破大隊.md "wikilink")
      - [海軍陸戰隊](../Page/中華民國海軍陸戰隊.md "wikilink")
          - [海軍陸戰隊兩棲偵搜大隊](../Page/海軍陸戰隊兩棲偵搜大隊.md "wikilink")
          - [海軍陸戰隊特勤中隊](../Page/中華民國海軍陸戰隊特勤隊.md "wikilink")
          - [海軍陸戰隊莒拳隊](../Page/海軍陸戰隊莒拳隊.md "wikilink")
  - [海巡署](../Page/海巡署.md "wikilink")[海洋巡防總局乙種第十一海岸巡防隊](../Page/行政院海岸巡防署海洋巡防總局.md "wikilink")（海巡特勤隊）
  - [天威部隊](../Page/天威部隊.md "wikilink")
      - 由[陸軍101兩棲偵察營](../Page/中華民國陸軍101兩棲偵察營.md "wikilink")、[高空特勤中隊](../Page/高空特勤中隊.md "wikilink")、[海軍陸戰隊兩棲偵搜大隊](../Page/海軍陸戰隊兩棲偵搜大隊.md "wikilink")、[海軍陸戰隊特勤中隊及](../Page/中華民國海軍陸戰隊特勤隊.md "wikilink")[憲兵特勤隊等聯合編成](../Page/憲兵特勤隊.md "wikilink")

###

  -
###

  -
  - 天狼星巡邏隊

###

  -
  -
###

  - [陸軍](../Page/陸軍.md "wikilink")
      - [第1海軍陸戰空降團](../Page/第一海軍陸戰步兵傘降團.md "wikilink")

      -
      - [第4特種部隊直昇機團](../Page/第4特種部隊直昇機團.md "wikilink")
  - [海軍](../Page/海軍.md "wikilink")
      - [法國海軍突擊隊](../Page/法國海軍.md "wikilink")
      - [法國反恐與人質救援部隊](../Page/法國反恐與人質救援部隊.md "wikilink")
  - [空軍](../Page/空軍.md "wikilink")
      - [第十空降突擊隊](../Page/第十空降突擊隊.md "wikilink")
  - [憲兵](../Page/憲兵.md "wikilink")
      - [法國國家憲兵干預隊](../Page/法國國家憲兵干預隊.md "wikilink")
  - [法國對外安全總局](../Page/法國對外安全總局.md "wikilink")

###

  -
  -
###

  - [陸軍](../Page/陸軍.md "wikilink")
      - [特種部隊指令部](../Page/特種部隊指令部.md "wikilink")
      - [空降獵兵](../Page/空降獵兵.md "wikilink")
      - [第200長距離偵察隊](../Page/第200長距離偵察隊.md "wikilink")
  - [海軍](../Page/海軍.md "wikilink")
      -
      - 海上保護組

###

  -
  -
  -
  -
  - [特別邊防部隊](../Page/特別邊防部隊.md "wikilink")

###

[Practice_the_release_of_65th_Airborne_Special_Forces_Brigad_hostages_with_uzi.jpg](https://zh.wikipedia.org/wiki/File:Practice_the_release_of_65th_Airborne_Special_Forces_Brigad_hostages_with_uzi.jpg "fig:Practice_the_release_of_65th_Airborne_Special_Forces_Brigad_hostages_with_uzi.jpg")進行人質救援訓練\]\]

  -
  - [聖城軍](../Page/聖城軍.md "wikilink")

###

  - [伊拉克特種作戰部隊](../Page/伊拉克特種作戰部隊.md "wikilink")

###

[ARW_30th_Anniversary_-_4478685364.jpg](https://zh.wikipedia.org/wiki/File:ARW_30th_Anniversary_-_4478685364.jpg "fig:ARW_30th_Anniversary_-_4478685364.jpg")

  - [愛爾蘭陸軍遊騎兵](../Page/愛爾蘭陸軍遊騎兵.md "wikilink")

###

  -
  -
  - [特別干預組](../Page/特別干預組.md "wikilink")

###

[Flickr_-_Israel_Defense_Forces_-_The_Exemplary_IDF_Unit_of_2011,_Shayetet_13.jpg](https://zh.wikipedia.org/wiki/File:Flickr_-_Israel_Defense_Forces_-_The_Exemplary_IDF_Unit_of_2011,_Shayetet_13.jpg "fig:Flickr_-_Israel_Defense_Forces_-_The_Exemplary_IDF_Unit_of_2011,_Shayetet_13.jpg")

  - [以色列國防軍特種部隊](../Page/以色列國防軍.md "wikilink")
      - [總參謀部偵察部隊](../Page/總參謀部偵察部隊.md "wikilink")

      -
      - [第13突擊隊](../Page/第13突擊隊.md "wikilink")

      - 217部隊（櫻桃部隊）
  - [以色列情報及特殊使命局特別行動科](../Page/以色列情報及特殊使命局.md "wikilink")

###

  - [陸上自衛隊](../Page/陸上自衛隊.md "wikilink")
      - [特殊作戰群](../Page/陸上自衛隊特殊作戰群.md "wikilink")

      - [第1空降團](../Page/陸上自衛隊第1空降團.md "wikilink")

      - [水陸機動團](../Page/陸上自衛隊水陸機動團.md "wikilink")

          - [西部方面隊普通科連隊](../Page/陸上自衛隊西部方面隊普通科連隊.md "wikilink")

      - [中央即應集團](../Page/陸上自衛隊中央即應集團.md "wikilink")

      -
      -
  - [海上自衛隊](../Page/海上自衛隊.md "wikilink")
      -
  - [航空自衛隊](../Page/航空自衛隊.md "wikilink")
      -
###

  - [哈薩克共和國武裝力量特種部隊](../Page/哈薩克軍事.md "wikilink")
  - [哈薩克共和國內務部](../Page/哈薩克共和國內務部.md "wikilink")
      - 老鷹特種部隊
  - [哈薩克共和國國家安全委員會](../Page/哈薩克共和國國家安全委員會.md "wikilink")
      - [獅子特種部隊](../Page/獅子特種部隊.md "wikilink")

###

  - [大韓民國陸軍](../Page/大韓民國陸軍.md "wikilink")
      - [陸軍特戰司令部](../Page/大韓民國陸軍特戰司令部.md "wikilink")
          - [第707特殊任務營](../Page/第707特殊任務營.md "wikilink")
  - [大韓民國海軍](../Page/大韓民國海軍.md "wikilink")

[2012.4.22_UDTseal팀_헬기침투_훈련_(7182879903).jpg](https://zh.wikipedia.org/wiki/File:2012.4.22_UDTseal팀_헬기침투_훈련_\(7182879903\).jpg "fig:2012.4.22_UDTseal팀_헬기침투_훈련_(7182879903).jpg")

  -   - [海軍特戰戰團](../Page/大韓民國海軍特戰戰團.md "wikilink")（參照[美國海軍](../Page/美國海軍.md "wikilink")[海豹部隊建立](../Page/海豹部隊.md "wikilink")）

      -
      - [大韓民國海軍陸戰隊](../Page/大韓民國海軍陸戰隊.md "wikilink")

          -
  - [大韓民國空軍](../Page/大韓民國空軍.md "wikilink")

      -
      - （Combat Control Team，CCT）

###

  - [朝鮮人民軍特種部隊](../Page/朝鮮人民軍特種部隊.md "wikilink")
  - [朝鮮人民軍特種作戰軍](../Page/朝鮮人民軍特種作戰軍.md "wikilink")

###

  - [马来西亚皇家海军](../Page/马来西亚皇家海军.md "wikilink")
      - [海军特种作战部队](../Page/海军特种作战部队.md "wikilink")（PASKAL）
  - [马来西亚陆军](../Page/马来西亚陆军.md "wikilink")
      - 马来西亚陆军突击队（Grup Gerak Khas）
      - 第10伞兵旅（10th Parachute Brigade）
  - [马来西亚皇家空军](../Page/马来西亚皇家空军.md "wikilink")
      - 马来西亚空勤队

###

  - [猎人中隊](../Page/猎人中隊.md "wikilink")

###

[缩略图](https://zh.wikipedia.org/wiki/File:Naval_SSG.jpg "fig:缩略图")

  -
  -
  -
###

  - [海軍特別作戰部隊](../Page/海軍特別作戰部隊.md "wikilink")

###

[GROM_with_Navy_SEALs_01.jpg](https://zh.wikipedia.org/wiki/File:GROM_with_Navy_SEALs_01.jpg "fig:GROM_with_Navy_SEALs_01.jpg")

  - [行動應變及機動組](../Page/行動應變及機動組.md "wikilink")
  - [福爾摩沙部隊](../Page/福爾摩沙部隊.md "wikilink")

###

[Evstafiev-spetsnaz-prepare-for-mission.jpg](https://zh.wikipedia.org/wiki/File:Evstafiev-spetsnaz-prepare-for-mission.jpg "fig:Evstafiev-spetsnaz-prepare-for-mission.jpg")期間的格魯烏特種部隊\]\]

  - [格魯烏特種部隊](../Page/格魯烏特種部隊.md "wikilink")
      - [俄羅斯陸軍](../Page/俄羅斯陸軍.md "wikilink")
          - 第2獨立特別用途旅
          - 第3獨立特別用途近衞旅
          - 第10獨立特別用途旅
          - 第14獨立特別用途旅
          - 第16獨立特別用途旅
          - 第22獨立特別用途近衞旅
          - 第24獨立特別用途旅
          - 第346獨立特別用途旅
          - 第25獨立特別用途團
      - [俄羅斯空降軍](../Page/俄羅斯空降軍.md "wikilink")
          -
      - [俄羅斯海軍](../Page/俄羅斯海軍.md "wikilink")
          - 海上偵察站
              - 第442海上特種偵察站（[太平洋艦隊](../Page/太平洋艦隊.md "wikilink")）
              - 第420海上特種偵察站（[北方艦隊](../Page/北方艦隊.md "wikilink")）
              - 第431海上特種偵察站（[黑海艦隊](../Page/黑海艦隊.md "wikilink")）
              - 第561海上特種偵察站（[波羅的海艦隊](../Page/波羅的海艦隊.md "wikilink")）
          - 戰鬥潛水員單位
              - 第102反破壞特別用途單位
              - 第136反破壞特別用途單位
              - 第101反破壞特別用途單位
              - 第137反破壞特別用途單位
              - 第159反破壞特別用途單位
              - 第311反破壞特別用途單位
              - 第313反破壞特別用途單位
              - 第473反破壞特別用途單位
              - 第152反破壞特別用途單位
              - 第140反破壞特別用途單位
              - 第160反破壞特別用途單位
              - 第269反破壞特別用途單位
              - 第153反破壞特別用途單位
  - [俄羅斯聯邦國防部](../Page/俄羅斯聯邦國防部.md "wikilink")
      - [俄羅斯聯邦武裝力量特種作戰部隊](../Page/俄羅斯聯邦武裝力量特種作戰部隊.md "wikilink")
          - “庫賓卡—2”特別用途中心
          - “Senezh”特別用途中心
  - [俄羅斯國家近衛軍](../Page/俄羅斯國家近衛軍.md "wikilink")
      - （2016年4月5日前為[俄羅斯聯邦內務部所屬的](../Page/俄羅斯聯邦內務部.md "wikilink")[俄羅斯內衞部隊](../Page/俄羅斯內衞部隊.md "wikilink")）

          - [第604特种作战中心](../Page/第604特种作战中心.md "wikilink")
          - 第7特种作战部队
          - 第12特种作战部队
          - 第15特种作战部队
          - 第17特种作战部队
          - 第19特种作战部队
          - 第21特种作战部队
          - 第23特种作战部队
          - 第25特种作战部队
          - 第26特种作战部队
          - 第27特种作战部队
          - 第28特种作战部队
          - 第29特种作战部队
          - 第33特种作战部队
          - 第34特种作战部队

[Fsb_alpha_group.jpg](https://zh.wikipedia.org/wiki/File:Fsb_alpha_group.jpg "fig:Fsb_alpha_group.jpg")[阿尔法小组的隊員進行著](../Page/阿尔法小组.md "wikilink")[CQB訓練](../Page/近身距離作戰.md "wikilink")\]\]

  - [俄羅斯聯邦安全局](../Page/俄羅斯聯邦安全局.md "wikilink")
      - 特別用途中心所屬
          - [阿尔法小组](../Page/阿尔法小组.md "wikilink")（A部門）
          - [溫佩爾小組](../Page/信號旗特種部隊.md "wikilink")（V部門）
          - S部門（旋風特種部隊）
          - K部門（聯邦安全局特別用途中心駐[葉先圖基特種部隊](../Page/葉先圖基.md "wikilink")）
          - 聯邦安全局特別用途中心駐[克里米亞共和國特種部隊](../Page/克里米亞共和國.md "wikilink")
      - 區域特別用途部隊
          - [冰雹特種部隊](../Page/冰雹特種部隊.md "wikilink")（[聖彼得堡區域特別用途部隊](../Page/聖彼得堡.md "wikilink")）
          - 殺人鯨特種部隊（[摩爾曼斯克區域特別用途部隊](../Page/摩爾曼斯克.md "wikilink")）
          - 烏鴉特種部隊（[沃羅涅日區域特別用途部隊](../Page/沃羅涅日.md "wikilink")）
          - [哈巴羅夫斯克區域特別用途部隊](../Page/哈巴羅夫斯克.md "wikilink")
          - [符拉迪沃斯托克區域特別用途部隊](../Page/符拉迪沃斯托克.md "wikilink")
          - [伊爾庫茨克區域特別用途部隊](../Page/伊爾庫茨克.md "wikilink")
          - [下諾夫哥羅德區域特別用途部隊](../Page/下諾夫哥羅德.md "wikilink")
          - [葉卡捷琳堡區域特別用途部隊](../Page/葉卡捷琳堡.md "wikilink")
          - [新西伯利亞區域特別用途部隊](../Page/新西伯利亞.md "wikilink")
          - [克拉斯諾達爾區域特別用途部隊](../Page/克拉斯諾達爾.md "wikilink")
          - [車里雅賓斯克區域特別用途部隊](../Page/車里雅賓斯克.md "wikilink")
          - [加里寧格勒區域特別用途部隊](../Page/加里寧格勒.md "wikilink")
          - 聯邦安全局駐[車臣共和國行動支援部隊](../Page/車臣共和國.md "wikilink")（花崗岩特種部隊）
          - 聯邦安全局駐[達吉斯坦共和國行動支援部隊](../Page/達吉斯坦共和國.md "wikilink")（里海特種部隊）
          - 聯邦安全局駐[印古什共和國行動支援部隊](../Page/印古什共和國.md "wikilink")
          - 聯邦安全局駐[卡巴爾達-巴爾卡爾共和國行動支援部隊](../Page/卡巴爾達-巴爾卡爾共和國.md "wikilink")
          - 聯邦安全局駐[巴什科爾托斯坦共和國行動支援部隊](../Page/巴什科爾托斯坦共和國.md "wikilink")
          - 聯邦安全局駐[韃靼斯坦共和國行動支援部隊](../Page/韃靼斯坦共和國.md "wikilink")
          - 聯邦安全局駐[卡累利阿共和國分部行動支援部隊](../Page/卡累利阿共和國.md "wikilink")（狼獾特種部隊）
          - 聯邦安全局駐俄羅斯聯邦各地的行動支援小組
      - 聯邦安全局邊防部門
          - 西格瑪特種部隊（繼承自03年解散的同名部隊）
          - 邊境分遣隊和各區域分部的獨立特別情報小組
          - 機動作戰部
          - 邊防部門區域特別用途部隊
          - 邊防部門區域特勤單位
  - [俄羅斯對外情報局](../Page/俄羅斯對外情報局.md "wikilink")
      - [屏障特種部隊](../Page/屏障特種部隊.md "wikilink")

###

  -   -
      -
###

  -
###

  -
  - [特別干預組](../Page/特別干預組.md "wikilink")

###

  - [特別行動任務組](../Page/特別行動任務組.md "wikilink")
  - [海岸突击队](../Page/海岸突击队.md "wikilink")

###

  -
  -
###

  -
  -
  -
  -
  -
  -
  -
  -
###

  - 國防部特種部隊
      - 第3獨立特種部隊團
      - 第8獨立特種部隊團
      - 第10獨立特種部隊團
      - 第140獨立特別用途中心
      - 第801反牽制支隊
      - 第73特別海上中心
  - 内務部特種部隊
      - 獵豹特種部隊
      - 猛虎特種部隊
      - 歐米茄特種部隊
      - 蠍子特種部隊
      - 獵鷹特種部隊
      - 眼鏡蛇特種部隊
      - 格里芬特種部隊
      - 捷豹特種部隊
      - 泰坦特種部隊
      - 酒吧特種部隊
      - 金鼓特種部隊
      - 影子特種部隊
      - 織女星特種部隊
  - [烏克蘭安全局](../Page/烏克蘭安全局.md "wikilink")[阿爾法小組](../Page/阿爾法小組_\(烏克蘭\).md "wikilink")

###

[Special_Air_Service_in_North_Africa_E_21337.jpg](https://zh.wikipedia.org/wiki/File:Special_Air_Service_in_North_Africa_E_21337.jpg "fig:Special_Air_Service_in_North_Africa_E_21337.jpg")時期的[英國陸軍](../Page/英國陸軍.md "wikilink")[特種空勤團](../Page/特種空勤團.md "wikilink")\]\]

  - [陸軍](../Page/陸軍.md "wikilink")

      - [特種空勤團](../Page/特種空勤團.md "wikilink")

      -
      - [陸軍傘兵團第1營](../Page/陸軍傘兵團第1營.md "wikilink")

      -
      -
  - [海軍](../Page/海軍.md "wikilink")

      - [特殊舟艇隊](../Page/特殊舟艇隊.md "wikilink")

  -
  - [秘密情報局E中隊](../Page/秘密情報局E中隊.md "wikilink")

###

[缩略图](https://zh.wikipedia.org/wiki/File:Navy_SEALs_coming_out_of_water.JPEG "fig:缩略图")[海豹部隊](../Page/海豹部隊.md "wikilink")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:MH6_at_NASCAR.jpg "fig:缩略图")160特種空降連\]\]
[75th_Ranger_Regiment_conducing_operations_in_Iraq,_26_April_2007.jpg](https://zh.wikipedia.org/wiki/File:75th_Ranger_Regiment_conducing_operations_in_Iraq,_26_April_2007.jpg "fig:75th_Ranger_Regiment_conducing_operations_in_Iraq,_26_April_2007.jpg")
[Delta-Operators-1.jpg](https://zh.wikipedia.org/wiki/File:Delta-Operators-1.jpg "fig:Delta-Operators-1.jpg")穿著平民服裝執行任務的三角洲部隊隊員\]\]
[DEVGRU_soldiers_protecting_Hamid_Karzai.jpg](https://zh.wikipedia.org/wiki/File:DEVGRU_soldiers_protecting_Hamid_Karzai.jpg "fig:DEVGRU_soldiers_protecting_Hamid_Karzai.jpg")
[USMC_MP_SRT_MP5.JPEG](https://zh.wikipedia.org/wiki/File:USMC_MP_SRT_MP5.JPEG "fig:USMC_MP_SRT_MP5.JPEG")特別反應小組\]\]

  - [美國陸軍特戰司令部](../Page/美國陸軍特戰司令部.md "wikilink")

      - [第75游騎兵團](../Page/第75游騎兵團.md "wikilink")

      - [陸軍特種部隊](../Page/美國陸軍特種部隊.md "wikilink")（綠扁帽部隊）

      -
      -
      - [陸軍特戰航空司令部](../Page/美国陆军特战司令部.md "wikilink")

          - [第160航空特戰團](../Page/美國陸軍第160特種作戰航空團.md "wikilink")

      -
  -   - [美國海軍三棲特戰隊](../Page/海豹部隊.md "wikilink")（海豹部隊）

      -
      -
      -
  - [美國空軍特戰司令部](../Page/美國空軍特戰司令部.md "wikilink")

      -
      -
      -
      -
      - [第720特殊戰術大隊](../Page/美國空軍第720特殊戰術大隊.md "wikilink")

      - [空軍特戰學校](../Page/空軍特戰學校.md "wikilink")

      -
      -
      -   -
      -
      -
      - [傘降救援隊](../Page/美國空軍傘降救援隊.md "wikilink")

          -
  -   -
      -
      -
  -   - [美國陸軍特種部隊第一特種作戰分遣隊](../Page/三角洲部隊.md "wikilink")（三角洲部隊）

      -
      -
      - [美國海軍特種作戰研究大隊](../Page/美國海軍特種作戰研究大隊.md "wikilink")

  - 其他

      - [中央情报局](../Page/中央情报局.md "wikilink")[特別活動科](../Page/中央情報局特別行動科.md "wikilink")
      - [88號特遣部隊](../Page/88號特遣部隊.md "wikilink")
      - [特別反應小組](../Page/特別反應小組_\(美國\).md "wikilink")
      - [美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")
          -
          -   -
          -   - [海軍陸戰隊武裝偵察連](../Page/美國海軍陸戰隊武裝偵察部隊.md "wikilink")

          -
          -
      - [美國海軍](../Page/美國海軍.md "wikilink")
          -
          -
      - [美國海岸警衞隊](../Page/美國海岸警衞隊.md "wikilink")
          -
          -
          - （TACLET）

## 跨境任務实例

  - 於1976年，以色列特工及[總參謀部偵察部隊前往](../Page/總參謀部偵察部隊.md "wikilink")[烏干達执行](../Page/烏干達.md "wikilink")[恩德培行动](../Page/恩德培行动.md "wikilink")，拯救被巴游[挾持的一百多名](../Page/挾持.md "wikilink")[人質](../Page/人質.md "wikilink")，幾乎救回所有人質，且順道全體殲灭[烏干達空軍](../Page/烏干達空軍.md "wikilink")。
  - 於1977年，[德國聯邦警察第九國境守備隊到](../Page/德國聯邦警察第九國境守備隊.md "wikilink")[索馬里拯救被](../Page/索馬里.md "wikilink")[劫機的人質](../Page/劫機.md "wikilink")，凱旋而回。
  - 於2010年4月10日，[法國突擊隊攻擊被劫持的法國遊艇](../Page/法國.md "wikilink")[塔尼號](../Page/塔尼號.md "wikilink")，殺死2名[索馬里海盜](../Page/索馬里海盜.md "wikilink")，救出人質。
  - 於2011年4月11日，[美國海軍特種作戰研究大隊成功營救被脅持的](../Page/美國海軍特種作戰研究大隊.md "wikilink")[快桅阿拉巴馬號](../Page/快桅阿拉巴馬號.md "wikilink")[船長](../Page/船長.md "wikilink")[理查德·菲利普斯](../Page/理查德·菲利普斯.md "wikilink")，三名海盜被[狙擊手擊斃](../Page/狙擊手.md "wikilink")。\[2\]
  - 於2011年1月2日，马来西亚[海军特种作战部队在亚丁湾成功营救](../Page/海军特种作战部队.md "wikilink")23名被索马里海盗挟持的马来西亚货轮[桂花号船员](../Page/桂花号.md "wikilink")，并且生擒7名海盗，缴获武器。

## 参考文献

## 外部連結

  - [朝鲜特種部隊](http://japanese.yonhapnews.co.kr/Politics2/2009/02/23/0900000000AJP20090223002600882.HTML)

## 參見

  - [低強度戰爭](../Page/低強度戰爭.md "wikilink")
  - [快速反應部隊](../Page/快速反應部隊.md "wikilink")
  - [步兵](../Page/步兵.md "wikilink")
      - [散兵](../Page/散兵.md "wikilink")

      - [輕裝步兵](../Page/輕裝步兵.md "wikilink")、[重裝步兵](../Page/重裝步兵.md "wikilink")、[线列步兵](../Page/线列步兵.md "wikilink")、[摩托化步兵](../Page/摩托化步兵.md "wikilink")、[機械化步兵](../Page/機械化步兵.md "wikilink")

      - 、[傘兵](../Page/傘兵.md "wikilink")、[空降部隊](../Page/空降部隊.md "wikilink")

      -
[vi:Đặc công](../Page/vi:Đặc_công.md "wikilink")

[特種部隊](../Category/特種部隊.md "wikilink")

1.
2.