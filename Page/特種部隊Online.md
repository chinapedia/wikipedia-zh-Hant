是一款[線上](../Page/線上遊戲.md "wikilink")[第一人稱射擊遊戲](../Page/第一人稱射擊遊戲.md "wikilink")。開發者是[南韓的](../Page/南韓.md "wikilink")[Dragon
Fly](../Page/Dragon_Fly.md "wikilink")（龍飛）公司。

## 运营历史

2004年在南韓首先發行，發行商為[Pmang](../Page/Pmang.md "wikilink")。

2006年10月13日 -
20日，台湾[华义国际进行](../Page/华义国际.md "wikilink")[封测](../Page/封测.md "wikilink")。当月25日，台湾[公测](../Page/公测.md "wikilink")\[1\]。

2007年5月中國由[中华网游戏集团开始负责运营](../Page/中华网.md "wikilink")。2011年9月22日，在其新版本发布会上，主持人曾喊出“虐死非专业”之口号，矛头直指同时期竞争对手[腾讯旗下的FPS游戏](../Page/腾讯.md "wikilink")《[穿越火线](../Page/穿越火线.md "wikilink")》\[2\]。但大约一年后，2012年8月17日以停止服务告终\[3\]。

2012年11月台灣更名為New SFOnline。

2017年韓國原廠不再與台灣代理商華義國際續約。台灣[華義國際於](../Page/華義國際.md "wikilink")2017年10月24日12:00結束營運，轉由韓國原廠繼續營運\[4\]。

2017年10月24日23:11台灣LaMate國際公司宣布由韓國Dragon fly與原台灣華義國際交接台灣地區SF代理權。

2018年12月，中国[创天互娱公司宣布获得](../Page/创天互娱.md "wikilink")《特种部队Online》运营代理权\[5\]。

2019年1月3日，中国[创天互娱公司宣布](../Page/创天互娱.md "wikilink")“复活”SF在中国的运营\[6\]\[7\]，1月11日开启[删档](../Page/删档.md "wikilink")[内测](../Page/内测.md "wikilink")。

## 遊戲模式

  - 個人戰
  - 團體戰
  - 軍團戰
  - CTC大頭模式2
  - CTC大頭模式
  - 生死格鬥
  - 生死格鬥2
  - 殭屍模式2
  - 殭屍模式
  - 殭屍坦克
  - 佔領戰
  - 狙擊戰or副武戰
  - 手槍專用模式
  - 1:1模式
  - 海盜模式
  - 新手訓練模式
  - 選手訓練場
  - 散彈槍對決
  - 水球模式
  - 泰坦模式

## 相關條目

  - [特種部隊2 Online](../Page/特種部隊2_Online.md "wikilink")（本遊戲的第二代作品）
  - [台灣電子競技聯盟](../Page/台灣電子競技聯盟.md "wikilink")
  - [SF世界盃](../Page/SF世界盃.md "wikilink")

## 參考資料

## 外部链接

  - [韓國SF官方網站](http://sf.dfl.co.kr/Main/HomeMain)
  - <s>[台灣新SF官方網站](https://web.archive.org/web/20171026195620/http://www.sfonline.com.tw/index.php)</s>（已關閉）
  - [台灣SF官方網站](https://sfonline.cosmosinfra.net/)
  - [中国SF官方网站](http://sf.8yx.com/)
  - [泰國SF官方網站](http://sf-web.gg.in.th/Landing/)

[Category:多人線上第一人稱射擊遊戲](../Category/多人線上第一人稱射擊遊戲.md "wikilink")
[Category:2004年电子游戏](../Category/2004年电子游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:韩国开发电子游戏](../Category/韩国开发电子游戏.md "wikilink")
[Category:華義國際遊戲](../Category/華義國際遊戲.md "wikilink")

1.
2.

3.
4.

5.

6.

7.