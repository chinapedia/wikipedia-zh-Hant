**特许公认会计师**（**Chartered Certified
Accountants**），简称**ACCA**，是得到皇家特许（[Royal
Charter](../Page/:en:Royal_Charter.md "wikilink")）銜頭的[英国四大擁有法定權力会计师資格之一](../Page/英国.md "wikilink")。

**[特许公认会计师公会](../Page/特许公认会计师公会.md "wikilink")**（**The Association of
Chartered Certified
Accountants**）是專門授予特许公认会计师資格的會計師公會，她也是世界上其中一個最著名的会计师專業團體。同時特许公认会计师公会亦是[国际会计准则理事会](../Page/国际会计准则理事会.md "wikilink")（[IASB](../Page/:en:IASB.md "wikilink")）和[国际会计师联合会](../Page/国际会计师联合会.md "wikilink")（[IFAC](../Page/:en:IFAC.md "wikilink")）的创始成员。现時全球170多个国家和地区有超过11万名会员和26万名ACCA学生会员。

## 特许公认会计师(ACCA)資格

### 普通會員

只要ACCA考生完成特许公认会计师专业资格考试（[ACCA](../Page/ACCA.md "wikilink") Professional
Scheme），以及得到3年ACCA認可的工作經驗，這樣便可以成為ACCA會員。ACCA會員可以被允许使用**特许公认会计师**（[Chartered
Certified
Accountant](../Page/:en:Chartered_Certified_Accountant.md "wikilink")）衔头，在個人名片上可以使用*ACCA*簡寫头衔，以及享有法律賦予特许公认会计师的職務權力和ACCA會員的應有福利。

#### 資深會員

  - 只要成為ACCA會員的5年後，以及在成為ACCA會員後的5年內遵守符合ACCA的會員規則，這樣便可以成為ACCA資深會員。ACCA資深會員可以被允许使用**資深特许公认会计师**（[Fellowship
    of Chartered Certified
    Accountant](../Page/:en:Chartered_Certified_Accountant.md "wikilink")）衔头，在個人名片上可以使用*FCCA*簡寫头衔，以及享有ACCA資深會員的應有福利。

## 法律認可

  - **特许公认会计师**資格得到[英國及](../Page/英國.md "wikilink")[愛爾蘭共和國的法律認可](../Page/愛爾蘭.md "wikilink")（Companies
    Act 1989, Insolvency Act & Financial Services and Markets
    Act），賦予特许公认会计师(ACCA會員)在當地可以擔任法定審計、破產管理、及商業投資顧問的法定工作。
  - 與此同時，根據歐盟互認協定（Mutual Recognition
    Directive），[ACCA专业资格得到](../Page/ACCA.md "wikilink")[歐洲聯盟成員國](../Page/歐洲聯盟.md "wikilink")、[歐洲經濟共同體國家成員國](../Page/歐洲經濟共同體.md "wikilink")、及[瑞士的法律認可](../Page/瑞士.md "wikilink")，賦予特许公认会计师(ACCA會員)法定權力可以在當地執業。
  - ACCA
    亦是[辛巴威的公共会计及審計委员会](../Page/辛巴威.md "wikilink")（PAAB）成員之一，在當地享有制定及修改當地会计及審計相關的法例，以及直接賦予特许公认会计师(ACCA會員)法定權力可以在當地執業。
  - 除此之外，[澳大利亞法例Corporation](../Page/澳大利亞.md "wikilink") Act 2001,
    section 1282 & Practice Statement 180 Auditor recognition
    賦予特许公认会计师(ACCA會員)在澳洲可以擔任法定審計和破產管理的法定工作、[紐西蘭法例Section](../Page/紐西蘭.md "wikilink")
    199 of the Companies Act 1993: Qualifications of Auditors
    賦予特许公认会计师(ACCA會員)在紐西蘭可以擔任法定審計的法定工作、以及[加拿大政府賦予特许公认会计师](../Page/加拿大.md "wikilink")(ACCA會員)法定權力可以為加拿大政府機構擔任法定審計工作。

## 参见章节

  - [特许公认会计师公会](../Page/特许公认会计师公会.md "wikilink")
  - [註冊會計師](../Page/註冊會計師.md "wikilink")
  - [英國會計師](../Page/英國會計師.md "wikilink")
  - [美國會計師](../Page/美國會計師.md "wikilink")
  - [會計師名銜](../Page/會計師名銜.md "wikilink")

## 外部链接

  - [特许公認会计师公会（ACCA）](http://www.acca-bj.com)

  - [特许公認会计师公会（ACCA）](http://www.accaglobal.com)

  - [中国注册会计师协会](https://web.archive.org/web/20131011124509/http://mailto:examkj@cicpa.org/)

  - [香港会计师公会](http://www.hkicpa.org.hk)

  - [美国会计师公会](http://www.aicpa.org)

[nl:Boekhouder](../Page/nl:Boekhouder.md "wikilink")

[Category:教育组织](../Category/教育组织.md "wikilink")
[Category:会计学](../Category/会计学.md "wikilink")
[Category:專業團體](../Category/專業團體.md "wikilink")
[Category:會計師](../Category/會計師.md "wikilink")