**特鲁瓦**（[法语](../Page/法语.md "wikilink")：**Troyes**）位于[法国](../Page/法国.md "wikilink")[大东部大区](../Page/大东部大区.md "wikilink")[奥布省](../Page/奥布省.md "wikilink")，[塞纳河畔](../Page/塞纳河.md "wikilink")，是[奥布省的首府](../Page/奥布省.md "wikilink")。在[中世纪时期](../Page/中世纪.md "wikilink")，特鲁瓦是重要贸易城市，[金衡制](../Page/金衡制.md "wikilink")（Troy
Weight）即得名于此。

世界著名品牌“鳄鱼”的拥有者[拉科斯特衬衫公司总部和](../Page/拉科斯特衬衫公司.md "wikilink")[特鲁瓦足球俱乐部都位于该市](../Page/特鲁瓦足球俱乐部.md "wikilink")。

## 交通

[特鲁瓦站位于](../Page/特鲁瓦站.md "wikilink")[巴黎－米卢斯铁路线上](../Page/巴黎－米卢斯铁路.md "wikilink")，前往巴黎大约需要1小时40分钟。

## 名人

  - [赫里欧](../Page/赫里欧.md "wikilink")：政治家，曾三任法国总理

## 人口

特鲁瓦埃人口变化图示

## 友好城市

  - [图尔奈](../Page/图尔奈.md "wikilink") （1951年）

  - [达姆施塔特](../Page/达姆施塔特.md "wikilink") （1958年）\[1\]

  - [阿尔克马尔](../Page/阿尔克马尔.md "wikilink") （1958年）

  - [綠山城](../Page/綠山城.md "wikilink") （1970年）\[2\]

<!-- end list -->

  - Chesterfield （1978年）\[3\]\[4\]

## 参见

  - [奥布省市镇列表](../Page/奥布省市镇列表.md "wikilink")

## 参考文献

## 外部链接

  - [特鲁瓦市议会网站](http://www.ville-troyes.fr/premiere.htm)
  - [拉科斯特衬衫公司官方网站](http://www.lacoste.com/)

[T](../Category/奥布省市镇.md "wikilink")

1.
2.
3.
4.