**牻牛儿苗目**（[学名](../Page/学名.md "wikilink")：*Geraniales*）是[双子叶植物下的一个小](../Page/双子叶植物.md "wikilink")[目](../Page/目.md "wikilink")，其中最大的[科是](../Page/科.md "wikilink")[牻牛儿苗科](../Page/牻牛儿苗科.md "wikilink")，大约有800多[种](../Page/种.md "wikilink")。但是也有的科只有不到40种。绝大部分牻牛儿苗目的[植物是](../Page/植物.md "wikilink")[草本](../Page/草本.md "wikilink")，但是也有少数品种是[灌木和小](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")。

牻牛儿苗目植物的经济价值不高，有些品种可以提炼芳香油或作为草药，有些品种为观赏花卉。

## 分类

根据[APG 分类法](../Page/APG_分类法.md "wikilink")，牻牛儿苗目曾经分为下列各科：

  - [花茎草科](../Page/花茎草科.md "wikilink")
  - [牻牛儿苗科](../Page/牻牛儿苗科.md "wikilink") +
    [高柱花科](../Page/高柱花科.md "wikilink")
  - [鞘叶树科](../Page/鞘叶树科.md "wikilink")
  - [杜香果科](../Page/杜香果科.md "wikilink")
  - [蜜花科](../Page/蜜花科.md "wikilink")
  - [曲胚科](../Page/曲胚科.md "wikilink")

在2016年发表的[APG IV 分类法中](../Page/APG_IV_分类法.md "wikilink")，本目只有以下两科：\[1\]

  - [牻牛儿苗科](../Page/牻牛儿苗科.md "wikilink") Geraniaceae
    （包含曾经的[高柱花科](../Page/高柱花科.md "wikilink")
    Hypseocharitaceae）
  - [花茎草科](../Page/花茎草科.md "wikilink") Francoaceae
    （包含曾经的[娑羽树科](../Page/娑羽树科.md "wikilink")
    Bersamaceae\[2\]、[红娟木科](../Page/红娟木科.md "wikilink")
    Greyiaceae\[3\]、[杜香果科](../Page/杜香果科.md "wikilink")
    Ledocarpaceae、[蜜花科](../Page/蜜花科.md "wikilink")
    Melianthaceae、[Rhynchothecaceae](../Page/Rhynchothecaceae.md "wikilink")、[曲胚科](../Page/曲胚科.md "wikilink")
    Vivianiaceae）

根据以前[克朗奎斯特分类法](../Page/克朗奎斯特分类法.md "wikilink")，牻牛儿苗目分为五科：

  - [牻牛儿苗科](../Page/牻牛儿苗科.md "wikilink")
  - [酢浆草科](../Page/酢浆草科.md "wikilink")
  - [沼花科](../Page/沼花科.md "wikilink")
  - [旱金莲科](../Page/旱金莲科.md "wikilink")
  - [凤仙花科](../Page/凤仙花科.md "wikilink")

## 参考文献

[\*](../Category/牻牛儿苗目.md "wikilink")

1.
2.
3.