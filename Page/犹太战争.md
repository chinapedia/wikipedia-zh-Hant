**猶太戰爭**，亦稱**大起義**或**猶太人大起義**，是位於地中海東岸[黎凡特的](../Page/黎凡特.md "wikilink")[猶太人在西元](../Page/猶太人.md "wikilink")66年至135年間，對抗[羅馬帝國的一連串戰爭大規模的起義](../Page/羅馬帝國.md "wikilink")。历史上第一次的猶太-羅馬戰爭（西元66-73年）以及巴．克巴起義（Bar
Kokhba revolt，西元132-136年）為猶太愛國者的起義，努力去回復猶太人國家的獨立。

## 历史

基多戰爭（the Kitos
War，西元115-117）是一個民族-宗教的衝突，戰事大多在猶太省以外進行。因此，許多資料使用了「猶太-羅馬戰爭」指涉第一次的猶太戰爭（西元66-73年）和巴．克巴起義（西元132-136年），也有其他資料也將基多戰爭（西元115-117）囊括進「猶太-羅馬戰爭」之中。[Roberts_Siege_and_Destruction_of_Jerusalem.jpg](https://zh.wikipedia.org/wiki/File:Roberts_Siege_and_Destruction_of_Jerusalem.jpg "fig:Roberts_Siege_and_Destruction_of_Jerusalem.jpg")

第一次猶太戰爭（西元66-73年)，有時被稱為「大起義」（ the Great Revolt ，希伯來文: המרד הגדול‎
*ha-Mered
Ha-Gadol*)），参加者主要为农民、手工业者和奴隶，组成著名的反抗組織[奮銳黨和短劍黨](../Page/奮銳黨.md "wikilink")（Sicarii，音譯作西卡里），和下层人民结合，焚毁藏在神庙中的债务账册，消灭[耶路撒冷的罗马驻军](../Page/耶路撒冷.md "wikilink")，罗马皇帝[尼禄派](../Page/尼禄.md "wikilink")[韦斯巴芗前往镇压](../Page/韦斯巴芗.md "wikilink")，公元70年[提图斯](../Page/提图斯.md "wikilink")（Titus
Flavius，韦斯巴芗之子）攻陷耶路撒冷，大肆破坏和屠杀，焚毀[第二聖殿](../Page/第二聖殿.md "wikilink")，大批[犹太人被俘为奴](../Page/犹太人.md "wikilink")。

公元131年，起义再度爆发，皇帝[哈德良派军镇压](../Page/哈德良.md "wikilink")；公元135年，罗马军队又攻陷耶路撒冷，几十万犹太人被杀，幸存者多流徙异地至今。

## 参考文献

## 参见

  - [耶路撒冷历史](../Page/耶路撒冷历史.md "wikilink")
  - [第二圣殿](../Page/第二圣殿.md "wikilink")
  - [奮銳黨](../Page/奮銳黨.md "wikilink")

{{-}}

[Category:羅馬帝國戰爭](../Category/羅馬帝國戰爭.md "wikilink")
[Category:巴勒斯坦军事史](../Category/巴勒斯坦军事史.md "wikilink")
[Category:犹太历史](../Category/犹太历史.md "wikilink")
[Category:1世纪亚洲战争](../Category/1世纪亚洲战争.md "wikilink")
[Category:60年代亚洲](../Category/60年代亚洲.md "wikilink")
[Category:70年代亚洲](../Category/70年代亚洲.md "wikilink")
[Category:60年代军事](../Category/60年代军事.md "wikilink")
[Category:70年代军事](../Category/70年代军事.md "wikilink")