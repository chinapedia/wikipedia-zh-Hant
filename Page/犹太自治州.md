border |flag_caption=[州旗](../Page/猶太自治州州旗.md "wikilink") |anthem=無
|anthem_ref=\[1\] |holiday |holiday_ref |political_status=自治州
|political_status_ref= |political_status_link=俄罗斯自治州
|federal_district=[遠東聯邦管區](../Page/遠東聯邦管區.md "wikilink")
|economic_region=[遠東經濟地區](../Page/遠東經濟地區.md "wikilink")
|adm_ctr_type=行政中心 |adm_ctr_name=[比罗比詹](../Page/比罗比詹.md "wikilink")
|adm_ctr_ref=\[2\] |pop_2010census=176558 |pop_2010census_rank=80
|urban_pop_2010census=67.6% |rural_pop_2010census=32.4%
|pop_2010census_ref=\[3\] |pop_density |pop_density_as_of
|pop_density_ref |pop_latest=170,000 |pop_latest_ref=\[4\]
|pop_latest_date=2016 |area_km2=36000 |area_km2_rank=61
|established_date= 1934年5月7日 |established_date_ref=\[5\]
|license_plates=79 |ISO=RU-YEV |gov_as_of= |leader_title=
|leader_title_ref=\[6\] |leader_name=[Alexander
Levintal](../Page/Alexander_Levintal.md "wikilink")
|leader_name_ref=\[7\] |legislature= |legislature_ref=\[8\]
|website=<http://www.eao.ru> |website_ref |date= }}
**犹太自治州**（；，），是[俄罗斯目前唯一一个](../Page/俄罗斯.md "wikilink")[自治州](../Page/俄罗斯自治州.md "wikilink")，属于[远东联邦管区](../Page/远东联邦管区.md "wikilink")，与[哈巴罗夫斯克边疆区](../Page/哈巴罗夫斯克边疆区.md "wikilink")、[阿穆尔州和](../Page/阿穆尔州.md "wikilink")[中国](../Page/中国.md "wikilink")[黑龙江省接壤](../Page/黑龙江省.md "wikilink")。面积36000平方公里，首府[比罗比詹](../Page/比罗比詹.md "wikilink")。人口根据2010年普查為176558人（占俄罗斯全国人口0.1%），其中[犹太人只有](../Page/犹太人.md "wikilink")1628人，占全州总人口1.0%，信奉[犹太教的则只占全州总人口的](../Page/犹太教.md "wikilink")0.2%。犹太自治州目前是除[以色列外世界上唯一的一处犹太人官方地区](../Page/以色列.md "wikilink")。

## 氣候

受到東亞[季風和](../Page/季風.md "wikilink")[西伯利亞高壓影響](../Page/西伯利亞高壓.md "wikilink")，该州夏季气候溫暖潮濕，冬季乾冷風大。

## 歷史

  - 3世纪中国的历史学家[陈寿在其著作中记载这一地区生活着](../Page/陈寿.md "wikilink")[挹娄人](../Page/挹娄.md "wikilink")。
  - 5世纪挹娄改称勿吉，又改称[靺鞨](../Page/靺鞨.md "wikilink")。
  - 7世纪靺鞨诸部形成[渤海国](../Page/渤海国.md "wikilink")，但[黑龙江流域的](../Page/黑龙江.md "wikilink")[黑水靺鞨部单独建国](../Page/黑水靺鞨.md "wikilink")。
  - 11世纪[契丹人的](../Page/契丹人.md "wikilink")[辽朝统治这一地区](../Page/辽朝.md "wikilink")。
  - 12世纪靺鞨人的后代[女真人在首领](../Page/女真人.md "wikilink")[完颜阿骨打的带领下建立](../Page/完颜阿骨打.md "wikilink")[金朝](../Page/金朝.md "wikilink")。
  - 13世纪[蒙古人的](../Page/蒙古族.md "wikilink")[元朝控制这一地区](../Page/元朝.md "wikilink")。
  - 14世纪蒙古势力被驱逐，女真诸部落臣服于[明朝](../Page/明朝.md "wikilink")。
  - 16世纪[努尔哈赤统一女真诸部](../Page/努尔哈赤.md "wikilink")，其子[多尔衮在](../Page/多尔衮.md "wikilink")1644年进入[山海关](../Page/山海关.md "wikilink")，经过十几年的战争[满洲人](../Page/满族.md "wikilink")（努尔哈赤之子[皇太极改女真为满洲](../Page/皇太极.md "wikilink")）完全控制[华北和](../Page/华北.md "wikilink")[华南](../Page/华南.md "wikilink")，成为[中国历史上最后一个世袭](../Page/中国历史.md "wikilink")[王朝](../Page/王朝.md "wikilink")——[清朝](../Page/清朝.md "wikilink")。
  - 17世纪[俄国开始侵略原属中国的](../Page/俄罗斯沙皇国.md "wikilink")[黑龙江流域](../Page/黑龙江.md "wikilink")，经过数场战斗以后，于1689年中俄签订《[尼布楚条约](../Page/尼布楚条约.md "wikilink")》，该地区归属清朝。
  - 19世纪[鸦片战争后](../Page/鸦片战争.md "wikilink")[俄国再次入侵该地区](../Page/俄罗斯帝国.md "wikilink")，1858年《[瑷珲条约](../Page/瑷珲条约.md "wikilink")》当中该地区被割让给俄国。
  - [斯大林考虑到](../Page/斯大林.md "wikilink")[苏联](../Page/苏联.md "wikilink")[远东地区与中國历史上的关系](../Page/远东.md "wikilink")，急需增加人口，1928年苏共中央执行委员会决定把[黑龙江沿岸的一塊未开垦土地闢为](../Page/黑龙江.md "wikilink")[犹太人迁入地](../Page/犹太人.md "wikilink")。1929年在黑龙江的支流比罗河与比詹河之间，设立“比罗比詹民族地区”。犹太人被强制移送到这里，在此之前这里没有一个犹太人。

## 人口

### 人口分布

以下为犹太自治州各人口分布统计列表（依俄罗斯2010年人口普查统计）：

| \# | 行政区                                        | 行政中心  | 总人口    | 城市人口   | 乡村人口  |
| -- | ------------------------------------------ | ----- | ------ | ------ | ----- |
| 1  | 比罗比詹市                                      | 比罗比詹  | 75413  | 75413  | 0     |
| 2  | [比罗比詹区](../Page/比罗比詹区.md "wikilink")       | 比罗比詹  | 11907  | 0      | 11907 |
| 3  | [列宁斯科耶区](../Page/列宁斯科耶区.md "wikilink")     | 列宁斯科耶 | 20684  | 0      | 20684 |
| 4  | [奥布卢奇耶区](../Page/奥布卢奇耶区.md "wikilink")     | 奥布卢奇耶 | 29035  | 24952  | 4083  |
| 5  | [十月区](../Page/十月区_\(犹太自治州\).md "wikilink") | 阿穆尔捷特 | 11354  | 0      | 11354 |
| 6  | [斯米多维奇区](../Page/斯米多维奇区.md "wikilink")     | 斯米多维奇 | 28165  | 19016  | 9149  |
|    | 犹太自治州                                      | 比罗比詹  | 176558 | 119381 | 57177 |

### 民族構成

1939—2010年间人口统计中的民族构成:

| 民族                                 | 1939年\[9\]     | 1959年\[10\]     | 1970年\[11\]     | 1979年\[12\]     | 1989年           | 2002年\[13\]\[14\] | 2010年\[15\]     |
| ---------------------------------- | -------------- | --------------- | --------------- | --------------- | --------------- | ----------------- | --------------- |
| [俄罗斯人](../Page/俄罗斯人.md "wikilink") | 75093 (68.9 %) | 127281 (78.2 %) | 144286 (83.7 %) | 158765 (84.1 %) | 178087 (83.2 %) | 171697 (89.9 %)   | 160185 (92.7 %) |
| [乌克兰人](../Page/乌克兰人.md "wikilink") | 9933 (9.1 %)   | 14425 (8.9 %)   | 10558 (6.1 %)   | 11870 (6.3 %)   | 15921 (7.4 %)   | 8483 (4.4 %)      | 4871 (2.8 %)    |
| [犹太人](../Page/犹太人.md "wikilink")   | 17695 (16.2 %) | 14269 (8.8 %)   | 11452 (6.6 %)   | 10163 (5.4 %)   | 8887 (4.1 %)    | 2327 (1.2 %)      | 1628 (1.0 %)    |

## 行政區劃

[Jewish_Autonomous_Oblast_s_ciframi_color.svg](https://zh.wikipedia.org/wiki/File:Jewish_Autonomous_Oblast_s_ciframi_color.svg "fig:Jewish_Autonomous_Oblast_s_ciframi_color.svg")
**猶太自治州**是俄罗斯联邦主体之一，行政上下辖1个市（；）和5个县（；），进一步分为10个镇（；）和18乡（；）。市县如下：\[16\]。

猶太自治州的行政中心是**[比羅比詹](../Page/比羅比詹.md "wikilink")**市。

### 縣市一覽

<table>
<thead>
<tr class="header">
<th><p>序号</p></th>
<th><p>名稱</p></th>
<th><p>行政區劃編碼</p></th>
<th><p>人口（2013），<br />
千人[17]</p></th>
<th><p>行政中心</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/比羅比詹.md" title="wikilink">比羅比詹</a></p></td>
<td><p>99401000</p></td>
<td><p>75.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><a href="../Page/比羅比詹斯基縣.md" title="wikilink">比羅比詹斯基縣</a></p></td>
<td><p>99205000000</p></td>
<td><p>12.1</p></td>
<td><p>比羅比詹</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><a href="../Page/列寧斯基縣.md" title="wikilink">列寧斯基縣</a></p></td>
<td><p>99210000000</p></td>
<td><p>19.9</p></td>
<td><p><a href="../Page/列寧城.md" title="wikilink">列寧城</a></p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="../Page/奥布卢琴斯基縣.md" title="wikilink">奥布卢琴斯基縣</a></p></td>
<td><p>99220000000</p></td>
<td><p>27.5</p></td>
<td><p><a href="../Page/奥布卢奇耶.md" title="wikilink">奥布卢奇耶</a></p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><a href="../Page/奥科提雅布尔斯基县.md" title="wikilink">奥科提雅布尔斯基县</a></p></td>
<td><p>99225000000</p></td>
<td><p>10.7</p></td>
<td><p><a href="../Page/阿穆爾澤特.md" title="wikilink">阿穆爾澤特</a></p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="../Page/斯密多維奇斯基縣.md" title="wikilink">斯密多維奇斯基縣</a></p></td>
<td><p>99230000000</p></td>
<td><p>26.9</p></td>
<td><p><a href="../Page/斯密多維奇.md" title="wikilink">斯密多維奇</a></p></td>
</tr>
</tbody>
</table>

## 注释

## 参考文献

## 外部連結

  - [犹太自治州](http://www.eao.ru/chi/)官方網站
  - [中俄经贸合作网](http://www.crc.mofcom.gov.cn/index.shtml)

{{-}}

[猶太自治州](../Category/猶太自治州.md "wikilink")
[J](../Category/遠東聯邦管區.md "wikilink")
[J](../Category/猶太.md "wikilink")

1.  [Конкурс на гимн Еврейской АО не состоялся из-за низкого уровня
    представленных работ (The competition for anthem of the Jewish
    Autonomous Region did not take place because of the low level of the
    works)](http://fedpress.ru/news/society/news_society/konkurs-na-gimn-evreiskoi-ao-ne-sostoyalsya-iz-za-nizkogo-urovnya-predstavlennykh-rabot)

2.  Charter of the Jewish Autonomous Oblast, Article 5

3.
4.

5.  Charter of the Jewish Autonomous Oblast, Article 4

6.  Charter of the Jewish Autonomous Oblast, Article 22

7.  Official website of the Jewish Autonomous Oblast. [Alexander
    Borisovich Levintal](http://eao.ru/?p=6), Governor of the Jewish
    Autonomous Oblast

8.  Charter of the Jewish Autonomous Oblast, Article 15

9.  [Демоскоп Weekly — Приложение. Справочник статистических
    показателей](http://demoscope.ru/weekly/ssp/rus_nac_39.php?reg=23)

10. [Демоскоп Weekly — Приложение. Справочник статистических
    показателей](http://demoscope.ru/weekly/ssp/rus_nac_59.php?reg=14)

11. [Демоскоп Weekly — Приложение. Справочник статистических
    показателей](http://demoscope.ru/weekly/ssp/rus_nac_70.php?reg=81)

12. [Демоскоп Weekly — Приложение. Справочник статистических
    показателей](http://demoscope.ru/weekly/ssp/rus_nac_79.php?reg=81)

13. [Демоскоп Weekly — Приложение. Справочник статистических
    показателей](http://demoscope.ru/weekly/ssp/rus_nac_02.php?reg=88)

14. [Национальный состав населения Еврейской автономной
    области](http://worldgeo.ru/russia/lists/?id=33&code=79) на
    сайте *Всемирная География*

15.

16.

17.