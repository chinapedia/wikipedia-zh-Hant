**狗魚科**為[輻鳍魚綱](../Page/輻鳍魚綱.md "wikilink")[狗魚目的一](../Page/狗魚目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，其下仅有一[属](../Page/属_\(生物\).md "wikilink")**狗鱼属**，包括7種。

## 分布

本[科魚類分布於](../Page/科.md "wikilink")[北美洲及](../Page/北美洲.md "wikilink")[欧亚大陆淡水區域](../Page/欧亚大陆.md "wikilink")。\[1\]

## 特徵

本[科魚體延長](../Page/科.md "wikilink")，吻端尖，尾鰭分叉，側線完整且連續，具8個或更多眶下管；鰓條骨10至20根，具有鼻骨，脊椎骨43至67個，體長最大可達1.4公尺。

## 分類

  - （*Esox americanus*）
      - [帶紋狗魚](../Page/帶紋狗魚.md "wikilink")（''Esox americanus americanus
        ''）
      - [蟲紋狗魚](../Page/蟲紋狗魚.md "wikilink")（''Esox americanus
        vermiculatus ''）
  - [義大利狗魚](../Page/義大利狗魚.md "wikilink")（*Esox cisalpinus*）
  - [白斑狗魚](../Page/白斑狗魚.md "wikilink")（*Esox lucius*）適合烹飪食用。
  - [北美狗魚](../Page/北美狗魚.md "wikilink")（*Esox masquinongy*）
  - [暗色狗魚](../Page/暗色狗魚.md "wikilink")（*Esox niger*）
  - [黑斑狗魚](../Page/黑斑狗魚.md "wikilink")（*Esox reichertii*）

## 生態

本[科魚類棲息在淡水的溪流或湖泊中](../Page/科.md "wikilink")，屬肉食性魚類，以小魚、[昆蟲及](../Page/昆蟲.md "wikilink")[兩棲類等為食](../Page/兩棲類.md "wikilink")，成魚冬季會往水較深的地方遷移。

## 經濟利用

## 参考资料

[\*](../Category/狗鱼科.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  [我国东北的狗鱼](http://www.cnki.com.cn/Article/CJFDTotal-SWXT195707007.htm)