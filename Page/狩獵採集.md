**狩獵採集**（hunter-gatherer）是指一种通过猎捕食物和直接采摘可食用果实的[生存技能](../Page/生存技能.md "wikilink")，而不太靠[驯养或](../Page/驯养.md "wikilink")[农业的生存状态](../Page/农业.md "wikilink")。

狩獵採集可能是人类出现以来到[旧石器时代为止唯一的生存技能](../Page/旧石器时代.md "wikilink")。

农业大概在1万2000年前在[两河流域](../Page/两河流域.md "wikilink")、[亚洲及](../Page/亚洲.md "wikilink")[中美洲](../Page/中美洲.md "wikilink")、[安第斯地区出现](../Page/安第斯.md "wikilink")。从那时开始农业社會渐渐开始扩张取代狩獵採集社會。

## 延伸閱讀

  -
  -
  - (Reviewed in *[The Montreal
    Review](http://www.themontrealreview.com/2009/A-cooperative-species-human-reciprocity-and-its-evolution-by-Bowles-and-Gintis.php)*)

  -
  -
  -
  -
  -
  -
## 外部連結

  - [Nature's Secret Larder - Wild Foods & Hunting
    Tools.](http://www.naturessecretlarder.co.uk)

  - [A wiki dedicated to the scientific study of the diversity of
    foraging societies without recreating
    myths](http://foragers.wikidot.com/start)

  -
[S](../Category/原始社会.md "wikilink") [S](../Category/石器时代.md "wikilink")