**狭纹虎鲨**（[學名](../Page/學名.md "wikilink")*Heterodontus
zebra*），又名**斑紋異齒鮫**、**斑紋異齒鯊**，俗名角鯊、虎沙，是[軟骨魚綱](../Page/軟骨魚綱.md "wikilink")[虎鯊目](../Page/虎鯊目.md "wikilink")[虎鯊科](../Page/虎鯊科.md "wikilink")[虎鲨属下的一个](../Page/虎鲨属.md "wikilink")[种](../Page/种.md "wikilink")。

## 分布

本魚分布於西[太平洋](../Page/太平洋.md "wikilink")[亞熱帶介乎](../Page/亞熱帶.md "wikilink")40°N至20°S的海域，包括中国大陆和[台灣](../Page/台灣.md "wikilink")\[1\]、[日本](../Page/日本.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[越南](../Page/越南.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")[昆士蘭等](../Page/昆士蘭.md "wikilink")。

## 深度

水深50至200公尺。

## 特徵

本魚頭大而鈍，眼大，口小，低的眶頂冠在[眼睛後逐漸傾斜](../Page/眼睛.md "wikilink")，體側扁，略呈卵圓形。有[背鰭及](../Page/背鰭.md "wikilink")[臀鰭](../Page/臀鰭.md "wikilink")，淺淡色的身體上有窄而直的[斑馬狀斑汶](../Page/斑馬.md "wikilink")。牠就像其他的[虎鯊科一樣](../Page/虎鯊科.md "wikilink")，[尾鰭有長的圓突](../Page/尾鰭.md "wikilink")，[腹鰭較背鰭短](../Page/腹鰭.md "wikilink")，而[脊椎升起至尾鰭處](../Page/脊椎.md "wikilink")。第一、二背鰭皆具硬棘，尾鰭具深缺刻。體長可達1.25公尺。

## 生態

本魚主要生活於[大陸棚海域](../Page/大陸棚.md "wikilink")，平時停棲於海底，游泳能力不佳，行動緩慢。屬肉食性，以小型底棲[無脊椎動物為食](../Page/無脊椎動物.md "wikilink")。[卵生的](../Page/卵生.md "wikilink")，卵大且具具螺旋形卵殼，四角另具短鬚狀突起。

## 經濟利用

不常見，價值性不高，偶爾在水族館展示。其背鰭具硬棘。 大型魚種易經由食物鍊累積大量重金屬，導致神經系統病變，不宜食用。

## 參考

  -
  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

  - {{ cite book | title = 魚類圖鑑 | publisher = 遠流出版社 | date = 2003年 }}

[HZ](../Category/IUCN無危物種.md "wikilink")
[HZ](../Category/觀賞魚.md "wikilink")
[HZ](../Category/食用鱼.md "wikilink")
[zebra](../Category/虎鲨属.md "wikilink")

1.  <http://fishdb.sinica.edu.tw/chi/species.php?id=383115>