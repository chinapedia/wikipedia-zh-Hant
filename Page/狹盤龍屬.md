**狹盤龍屬**（屬名：*Stenopelix*）又名**細盤龍**，意為“狹窄的[骨盆](../Page/骨盆.md "wikilink")”，是小型[鳥臀目](../Page/鳥臀目.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，生存於早[白堊紀的](../Page/白堊紀.md "wikilink")[德國](../Page/德國.md "wikilink")。狹盤龍是原始厚頭龍類恐龍，生存於[白堊紀的](../Page/白堊紀.md "wikilink")[巴列姆階](../Page/巴列姆階.md "wikilink")。狹盤龍的標本缺少頭顱骨，牠的分類是基於臀部特徵。

狹盤龍的分類過去曾有過爭議。牠們最初根據[恥骨與](../Page/恥骨.md "wikilink")[髖臼不連接](../Page/髖臼.md "wikilink")，以及堅固的尾部[肋骨](../Page/肋骨.md "wikilink")，而被歸類於厚頭龍類。後來發現恥骨與髖臼連接，而尾部肋骨其實是[薦骨部位的肋骨](../Page/薦骨.md "wikilink")。狹盤龍的彎曲[坐骨](../Page/坐骨.md "wikilink")、缺少[閉孔](../Page/閉孔.md "wikilink")（Obturator
foramen），這是其他厚頭龍類沒有的特徵。

但狹盤龍仍被認為屬於[厚頭龍下目](../Page/厚頭龍下目.md "wikilink")。一些[古生物學家將狹盤龍視為厚頭龍下目中的歸類不明屬](../Page/古生物學家.md "wikilink")。

## 參考資料

  - [*Stenopelix* in The Dinosaur
    Encyclopaedia](http://web.me.com/dinoruss/de_4/5a786d8.htm) at Dino
    Russ's Lair
  - [*Stenopelix*](https://web.archive.org/web/20090929110400/http://www.thescelosaurus.com/ornithischia.htm)
    at *Thescelosaurus*\! (scroll down to Pachycephalosauria)

[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:厚頭龍下目](../Category/厚頭龍下目.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")