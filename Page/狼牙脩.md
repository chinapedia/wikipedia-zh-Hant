**狼牙脩**（）是古代[東南亞的](../Page/東南亞.md "wikilink")[印度化](../Page/印度.md "wikilink")[國家之一](../Page/國家.md "wikilink")，其领土包括今[马来半岛东岸](../Page/马来半岛.md "wikilink")[北大年以东和东北地区](../Page/北大年.md "wikilink")，约位于[東經](../Page/東經.md "wikilink")101°18'，[北緯](../Page/北緯.md "wikilink")6°48'，及今[马来西亚的](../Page/马来西亚.md "wikilink")[吉打州](../Page/吉打.md "wikilink")。

狼牙脩在[宋代称为](../Page/宋代.md "wikilink")“凌牙斯加”、[元代称为](../Page/元代.md "wikilink")“龍牙犀角”、[明代称为](../Page/明代.md "wikilink")“狼西加”。“Langkasuka”一词首见于，[印度尼西亞](../Page/印度尼西亞.md "wikilink")11世纪[泰米尔文碑銘作](../Page/泰米尔文.md "wikilink")
Ilangasongam,14世纪《爪哇史颂》（Nagarakrtagama），作Langkasuka，16世纪出版的[阿拉伯文](../Page/阿拉伯文.md "wikilink")《吉打纪年》（Kitab
al-Minhaj al-fakhir fi-ilm al-bahr al-zakhir）作Langashuka。“Langkasuka”
的得名或来自[印度文化](../Page/印度文化.md "wikilink")，依照印度的[佛經](../Page/佛經.md "wikilink")《[枵伽經](../Page/枵伽經.md "wikilink")》里的記載，Lanka是傳說中一個在马来亚峰(英文：Mount
Malaya)上面的山城；而在[梵文中Sukha乃是](../Page/梵文.md "wikilink")“快樂世界─Sukhavati”一詞中的前半截。因此Langkasuka或取義於“快樂的马来半岛山城”。

狼牙脩建國年代约于公元1世紀末至2世紀。

狼牙脩的居民以[吉蔑人](../Page/高棉族.md "wikilink")為主，而統治階級是[印度人](../Page/印度.md "wikilink")。狼牙脩开始是盛行[佛教](../Page/佛教.md "wikilink")，使用[南印度的](../Page/南印度.md "wikilink")[文字](../Page/文字.md "wikilink")。但至元代称“龍牙犀角”时期，已不见佛教盛况，那时期[伊斯兰教已传入东南亚多时](../Page/伊斯兰教.md "wikilink")。由於[地理位置優越](../Page/地理.md "wikilink")，狼牙脩是當時[世界的重要航站](../Page/世界.md "wikilink")。

## 历史

狼牙脩建国于1世紀。515年狼牙脩国王婆加達多遣使中國，其后523年,
531年，568年多次朝貢。12世紀时狼牙脩为[三佛齐屬國](../Page/三佛齐.md "wikilink")。15世紀狼牙脩国被[北大年国取代](../Page/北大年.md "wikilink")。

[Emissary_from_Langkasuka.JPG](https://zh.wikipedia.org/wiki/File:Emissary_from_Langkasuka.JPG "fig:Emissary_from_Langkasuka.JPG")《[職貢圖](../Page/職貢圖.md "wikilink")》
狼牙脩使者 (526年-539年)\]\]

### 中国古籍文獻中的“狼牙脩”

中国古籍文献中对于同一个国家，常有多个音译，狼牙脩也不例外：

1.  《[梁書](../Page/梁書.md "wikilink")》、《[南史](../Page/南史.md "wikilink")》、《[通典](../Page/通典.md "wikilink")》、《[舊唐書](../Page/舊唐書.md "wikilink")》、《[新唐書](../Page/新唐書.md "wikilink")》中作“狼牙脩”；
2.  《[續高僧傳](../Page/續高僧傳.md "wikilink")·拘那羅陀》作“稷伽脩”；
3.  《[隋書](../Page/隋書.md "wikilink")》中的《赤土傳》及《北史赤土傳》 中作“狼牙須”；
4.  [玄奘](../Page/玄奘.md "wikilink")《[大唐西域記](../Page/大唐西域記.md "wikilink")·三摩呾咤国》作“迦摩浪迦”；
5.  《[大唐西域求法高僧傳](../Page/大唐西域求法高僧傳.md "wikilink")》作“郎伽戍”、“郎伽”；
6.  《[南海寄歸內法傳](../Page/南海寄歸內法傳.md "wikilink")》中作“郎伽戍”；
7.  [宋](../Page/宋.md "wikilink")《[諸蕃志](../Page/諸蕃志.md "wikilink")》中作“凌牙斯加”；
8.  [元](../Page/元.md "wikilink")《[島夷誌略](../Page/島夷誌略.md "wikilink")》作“龍牙犀角”；
9.  [明](../Page/明.md "wikilink")《[鄭和航海圖](../Page/鄭和航海圖.md "wikilink")·第十四图》中作“狼西加”。

### 狼亚脩的地理位置

历史上学者们对狼牙脩的地理位置一度并不十分确定。

中国[二十四史之一的](../Page/二十四史.md "wikilink")《梁书》有着[世界上最早的狼牙脩地理位置记载](../Page/世界.md "wikilink")：「其界東西三十日行，南北二十日行，去廣州二萬四千里。」

19世纪末至20世纪初中为不少学者如[鄒代鈞](../Page/鄒代鈞.md "wikilink")、[桑田六郎](../Page/桑田六郎.md "wikilink")、[柔克義](../Page/柔克義.md "wikilink")、[伯希和等曾对狼牙脩的](../Page/伯希和.md "wikilink")[地理位置](../Page/地理.md "wikilink")，做出各种推论，众说纷纭，莫衷一是。

以下是当时各种不同说法：

1.  [荷蘭人葛路耐](../Page/荷蘭.md "wikilink")（英文:W.P
    Groeneveldt）凭其所知将中国各种有关[南洋的文献收集](../Page/南洋.md "wikilink")，翻译成[英文](../Page/英文.md "wikilink")，并於1876年出版英文版《[南洋群島文獻錄](../Page/南洋群島文獻錄.md "wikilink")》，这书首批在[巴達維亞](../Page/巴達維亞.md "wikilink")发行，但书中誤认为“狼牙脩”為[錫蘭古都楞伽](../Page/錫蘭.md "wikilink")（英文：Langka）的對音；
2.  [美國汉学家](../Page/美國.md "wikilink")[柔克義](../Page/柔克义.md "wikilink")，則认为為狼牙脩就是《岛夷志略》的“龍牙犀角”，誤认为就是《鄭和航海圖》的[龍牙交椅](../Page/龍牙交椅.md "wikilink")(Langkawi)。
3.  [英國學者祈利亞](../Page/英國.md "wikilink")（英文：Gerinl）考證它是[暹羅灣內的](../Page/暹羅灣.md "wikilink")“狼卡修”
    （英文：Kah Langkaciu），但未被多数人所接受；
4.  [法国](../Page/法国.md "wikilink")[汉学家](../Page/汉学家.md "wikilink")[伯希和指狼牙脩在今日的](../Page/伯希和.md "wikilink")[日萊峰](../Page/日萊峰.md "wikilink")（马来文：Gunung
    Jerai）；当时伯希和还不知道《爪哇史颂》的Lengkasuka，而是根据《[諸蕃志](../Page/諸蕃志.md "wikilink")》中汉语“凌牙斯加”，还原出Lankasuka。
5.  晚清的中国学者[丁謙斷指在北大年](../Page/丁謙.md "wikilink")、及马来半岛的[吉蘭丹及](../Page/吉蘭丹.md "wikilink")[丁加奴等地](../Page/丁加奴.md "wikilink")；
6.  [馮承鈞稱主要在馬來半島之中](../Page/馮承鈞.md "wikilink")；
7.  中国清末[地理学家](../Page/地理学.md "wikilink")[鄒代鈞則指在](../Page/鄒代鈞.md "wikilink")[婆羅洲](../Page/婆羅洲.md "wikilink")；
8.  日本的桑田六郎則指在[巨港](../Page/巨港.md "wikilink")；
9.  [新加坡文史學家](../Page/新加坡.md "wikilink")[許雲樵教授指出是在](../Page/許雲樵.md "wikilink")[泰国](../Page/泰国.md "wikilink")[春蓬府附近](../Page/春蓬府.md "wikilink")
10. 鮑威里（英文：Paul Wheatley）所作的圖示，相當於北大年及[吉蘭丹](../Page/吉蘭丹.md "wikilink")；

直到[日本學者](../Page/日本.md "wikilink")[藤田豐八著](../Page/藤田豐八.md "wikilink")《狼牙脩國考》一书，考证《[島夷誌略](../Page/島夷誌略.md "wikilink")》中“龍牙犀角”，指出**龍牙犀角**就是《梁書》的“狼牙脩”，《[大唐西域求法高僧傳](../Page/大唐西域求法高僧傳.md "wikilink")》及《[南海寄歸內法傳](../Page/南海寄歸內法傳.md "wikilink")》中“郎伽戍”，《諸蕃志》的“凌牙斯加”，《鄭和航海圖》的“狼西加”，第一次指出这些不同的名字都是《爪哇史颂》的LAngkasuka的对音，位于暹羅南綞的[北大年](../Page/北大年.md "wikilink")。至此，龍牙犀角、狼牙脩、凌牙斯加、狼西加等均是Langasuka，已为中外学界普遍接受，殆成定论：

1.  [蘇繼廎根据](../Page/蘇繼廎.md "wikilink")《[大唐西求高僧傳](../Page/大唐西求高僧傳.md "wikilink")》、《諸蕃志》的記載，考证狼牙脩的地理位置，认为[伯希和](../Page/伯希和.md "wikilink")、[柔克义均誤](../Page/柔克义.md "wikilink")，以藤田豐八的[北大年说为正确](../Page/北大年.md "wikilink")。
2.  [中國科學院院士](../Page/中國科學院院士.md "wikilink")[向达考證出](../Page/向达.md "wikilink")《[郑和航海图](../Page/郑和航海图.md "wikilink")》五百余地名。[向达](../Page/向达.md "wikilink")：
    “**狼西加** ……应作狼牙西加，为Langkasuka 对音，即大泥地方”。（大泥是北大年的别名）。
3.  杨博文註釋《諸蕃志》凌牙斯加國條：“《[梁書](../Page/梁書.md "wikilink")》之狼牙脩、《隋書》之狼牙須、《續高僧傳·拘那羅陀傳》之稜伽脩，《[事林廣記](../Page/事林廣記.md "wikilink")》作**凌牙蘇家**,《[島夷誌略](../Page/島夷誌略.md "wikilink")》作**龍牙犀角**,皆係《爪哇史頌》所舉之Lengkasuka對音，也即印尼古代碑銘中Ilangasogam，地當今[北大年](../Page/北大年.md "wikilink")（Patani）”。
4.  英国学者**米爾斯**（J.V.Mills）对于**狼西加**的定位，迄今最為精確
    ：“**狼西加**（Langkasuka，在[北大年以東和東北一帶](../Page/北大年.md "wikilink")，東經101°18'
    ”
5.  米爾斯：“**龍牙犀角**即Langkasuka”
6.  米爾斯在前人研究成果的基礎上，恢復《郑和航海图》上七百多个地名，并且画成地图《1433年的中国和南亚》。据图[狼西加](../Page/狼西加.md "wikilink")（Langkasuka）约在東經101°18'，北緯6°48'.

虽然历史学界定论狼牙脩是位于北大年，但狼牙脩发源地及领土变化还是有争议。

以下是二种对狼牙脩发源地及领土变化的推论：

1.  狼牙脩建國于現代[馬來西亞的](../Page/馬來西亞.md "wikilink")[吉打州內](../Page/吉打.md "wikilink")，然後發展到[泰國的](../Page/泰國.md "wikilink")[北大年地區](../Page/北大年.md "wikilink")。
2.  狼牙脩建國于現代[泰國的](../Page/泰國.md "wikilink")[六坤](../Page/六坤.md "wikilink")（英文：Ligor），由吉蔑人所建立，後併吞古代的[赤土國领土南至](../Page/赤土.md "wikilink")[馬來西亞的](../Page/馬來西亞.md "wikilink")[吉打州內](../Page/吉打.md "wikilink")，[馬來人后来遷居狼牙脩](../Page/馬來人.md "wikilink")，领土最後發展到北大年。

### 历史学界对狼牙脩历史的归结

历史学界归纳各种不同的文献及理论，多少整理出狼牙脩的历史面貌如下：

1世紀以后，印度人在吉打一帶進行[殖民活動時建立狼牙脩](../Page/殖民.md "wikilink")。在中国的[萧梁至](../Page/南朝梁.md "wikilink")[隋朝時代](../Page/隋朝.md "wikilink")，即6世纪，狼牙脩的疆域，地跨馬來半島北部，包括今日马来西亚的[玻璃市](../Page/玻璃市.md "wikilink")、吉蘭丹、吉打及泰国的宋卡、北大年及一帶的地方；其政治中心，則有知名的固羅、赤土、羯荼、羯陀、基拉、吉陀等，都是以都城聞名於世。它的勢力，在6、7世紀期間達到了最高峰，7世紀以後其在吉打的领土就渐渐被建立在同一地點的‘羯荼’国（英文：Kedah）所取代。”

而8世紀以來，狼牙脩国各都城，先後被[室利佛逝及南印度的](../Page/室利佛逝.md "wikilink")[注輦](../Page/注輦.md "wikilink")，以及后来的[暹羅及](../Page/暹羅.md "wikilink")[滿者伯夷控制](../Page/滿者伯夷.md "wikilink")。当中国进入[宋](../Page/宋朝.md "wikilink")[元時代](../Page/元朝.md "wikilink")，由於中國航運的發達，马来半岛東海岸的[经济有所提升](../Page/经济.md "wikilink")，於是狼牙脩興隆起來。狼牙脩国至[明代还存在](../Page/明朝.md "wikilink")，领土似乎局限於北大年一隅，这就是《鄭和航海圖》中的“郎西加”。

## 狼牙脩遗迹及出土文物

1804年，在当时是英国[殖民地的马来半岛](../Page/殖民地.md "wikilink")，一名驻[槟城英籍警宫James](../Page/槟城.md "wikilink")
Low在马来半岛陆续发现一些梵文[石碑及](../Page/石碑.md "wikilink")[佛寺遗址](../Page/佛寺.md "wikilink")，他在莫目河和布央河之间发现的Bukit
Mariam佛教遗址，碑文上的四句[偈语为](../Page/偈语.md "wikilink")“造业由于无知，有业必须轮回，有知即可无业，无业使免轮回”。

1963年英国[剑桥大学考古学家](../Page/剑桥大学.md "wikilink") Stewart Wavell
曾率領考古隊攷察Langkasuka（狼牙脩），著有考古报告书：《The Naga King's
Daughter》。

## 狼牙脩与中国的交流

### 狼牙脩及梁武帝的交往

中国[南北朝時代](../Page/南北朝.md "wikilink")，[梁武帝蕭衍極力提倡佛法](../Page/梁武帝.md "wikilink")，其聲望因而遠播於東南亞許多崇奉[佛教的國家](../Page/佛教.md "wikilink")，狼牙脩亦是其中之一，依照《梁書》卷五十四的《狼牙脩國傳》中记载，在公元515年（天監14年），当时狼牙脩國的國王“婆加達多”，派使者阿撤多出使[南京](../Page/南京.md "wikilink")，拜见梁武帝。并交给梁武帝[國書](../Page/國書.md "wikilink")。狼牙脩的使者阿撤多交给梁武帝國書全文记载在《粱书》。

### 唐朝名僧义净大师可能到过狼牙脩

唐朝名僧[义净大师曾经由海路到印度取经](../Page/义净.md "wikilink")，依照《宋高僧传》卷一，义净在公元671年出国，公元695年回国，期间极可能曾在同样信奉佛教的狼牙脩停留。

而义净大师在其著作《大唐西域求法记》中提起了三位曾到狼牙脩的法师，可见有当时有中国[僧人到过狼牙脩](../Page/僧人.md "wikilink")，而狼牙脩或是要到印度取经的中国僧人停留之处。且唐代的狼牙脩显然善待中国僧人。

以下是节录自《大唐西域求法记》的文字：

“义朗律师者，[益州](../Page/益州.md "wikilink")[成都人也](../Page/成都.md "wikilink")……与同州僧智岸并第一人名义玄……既至乌雷同附商船，挂百丈陵万波，越舸[扶南](../Page/扶南.md "wikilink")，缀缆“郎迦戌”，蒙郎迦戌国王待以上宾之礼，智岸遇疾于此而亡，郎公既怀死别之恨，与附舶向[师子洲](../Page/师子洲.md "wikilink")，披求异典，顶礼佛牙，渐之西国。传闻如此而今不知在何所，狮子州既不见，[中印度复不闻](../Page/中印度.md "wikilink")，多是魂归异代矣。年四十余耳。”

“义辉法师，落阳人也。……欲思观梵文，亲听微言，遂指掌中天，还望东夏。惜哉！苗而不实，壮志先秋。至“郎迦戌国”婴疾而亡，年三十矣。”

“道琳法师者，[荆州](../Page/荆州.md "wikilink")[江凌人也](../Page/江凌.md "wikilink")。梵名-{尸}-罗钵颇（注：唐名戒光）欲寻流讨源，远游西国。乃杖锡遐逝，鼓舶南溟。越铜柱而届“郎迦”，历[诃陵而经](../Page/诃陵.md "wikilink")[裸国](../Page/裸国.md "wikilink")。所在国王礼待
，极至殷厚。经乎数载
，到东印度[耽摩立底国](../Page/耽摩立底国.md "wikilink")。……自尔之後，不悉何托净（自印度）回至南海[羯荼国](../Page/羯荼国.md "wikilink")，有北方胡至，云有两僧，胡国逢见，说其状迹，应是其人。与智弘相隋，拟归故国，闻为途贼所拥还，乃覆向北天，年应五十余矣。”

## 注释

1.  狼牙脩建国年代是[历史学者从](../Page/历史.md "wikilink")[中国史书](../Page/中国.md "wikilink")《梁書》卷五十四的《狼牙脩國傳》中推算而来，因书中记载有指狼牙脩“立国以来四百余年”之句。而中国[南北朝中的萧粱立国年代为](../Page/南北朝.md "wikilink")502年～557年，在公元6世纪。

2.  吉蔑人或名蒙吉蔑，英文：Man Khmer，[高棉人的一種](../Page/高棉.md "wikilink")。

3.  英文名称《Notes On The Malay Archipelago And Malacca》

4.  巴達維亞，今[印尼](../Page/印尼.md "wikilink")[首都](../Page/首都.md "wikilink")[雅加達](../Page/雅加達.md "wikilink")。

5.  [许云樵著](../Page/许云樵.md "wikilink")
    《[南洋史](../Page/南洋史.md "wikilink")》上卷
    [盘盘](../Page/盘盘.md "wikilink")[箇罗与狼牙修](../Page/箇罗.md "wikilink")
    第 158页 星洲世界书局 1961

6.  见《[岛夷志略](../Page/岛夷志略.md "wikilink")》 **181-183页**，龍牙犀角条，蘇繼廎注解,
    2000年 [中华书局](../Page/中华书局.md "wikilink")，ISBN 7-10102028-7

7.  见《[郑和航海图](../Page/郑和航海图.md "wikilink")·第十四图》**第30页**，[向达注](../Page/向达.md "wikilink")
    **狼西加**条，2000年 中华书局 2000年 ISBN 710102025-9

8.  见《[诸蕃志](../Page/诸蕃志.md "wikilink")》**第45-46頁**“凌牙斯加国”，楊博文注释
    [中华书局](../Page/中华书局.md "wikilink") 2000年 ISBN 7-101-02059-3

9.  The Overall Survey of the Ocean's Shores, **page 202**. 1997,
    ISBN 9748496783

10. ibid, **page 204**.

11. 见米爾斯所作地图《China in South Asia， 1433》ibid。

[Category:馬來西亞歷史](../Category/馬來西亞歷史.md "wikilink")
[Category:东南亚古国](../Category/东南亚古国.md "wikilink")
[Category:已不存在的亞洲君主國](../Category/已不存在的亞洲君主國.md "wikilink")