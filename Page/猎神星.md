****（Artemis）是第105颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1868年9月16日由[詹姆斯·克雷格·沃森发现](../Page/詹姆斯·克雷格·沃森.md "wikilink")。的[直径为](../Page/直径.md "wikilink")119.1千米，[质量为](../Page/质量.md "wikilink")1.8×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1335.678天。

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1868年发现的小行星](../Category/1868年发现的小行星.md "wikilink")