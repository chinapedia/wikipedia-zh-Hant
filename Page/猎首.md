[缩略图](../Page/图像:Chromesun_mississippian_priest_digital_painting.jpg.md "wikilink")
**猎首**（），又称**猎头**，中国古籍作**馘首**，[台湾原住民稱](../Page/台湾原住民.md "wikilink")**[出草](../Page/出草.md "wikilink")**，指将人杀死后砍下头颅并收集的一种习俗。许多古代文明都曾流行过猎首的做法，有史籍记载的包括：[古中国人](../Page/古中国.md "wikilink")、[台湾原住民](../Page/台湾原住民.md "wikilink")、[日本人](../Page/日本.md "wikilink")、[阿富汗的](../Page/阿富汗.md "wikilink")[努里斯坦人](../Page/努里斯坦人.md "wikilink")、古[印度的](../Page/印度.md "wikilink")[阿萨姆人和](../Page/阿萨姆邦.md "wikilink")[那加兰人](../Page/那加兰邦.md "wikilink")、[缅甸的](../Page/缅甸.md "wikilink")[佤族](../Page/佤族.md "wikilink")、[婆罗洲人](../Page/婆罗洲.md "wikilink")、[印度尼西亚人](../Page/印度尼西亚.md "wikilink")、[菲律宾人](../Page/菲律宾.md "wikilink")、[密克罗尼西亚人](../Page/密克罗尼西亚.md "wikilink")、[美拉尼西亚人](../Page/美拉尼西亚.md "wikilink")、[新西兰](../Page/新西兰.md "wikilink")[毛利人](../Page/毛利人.md "wikilink")、[亚马逊平原与美国大平原地区的](../Page/亚马逊平原.md "wikilink")[印第安人](../Page/印第安人.md "wikilink")、[尼日利亚人](../Page/尼日利亚.md "wikilink")、[欧洲的](../Page/欧洲.md "wikilink")[凯尔特人和](../Page/凯尔特.md "wikilink")[斯基泰人](../Page/斯基泰.md "wikilink")，等等。猎首的做法到了[第二次世界大战期间还在](../Page/第二次世界大战.md "wikilink")[太平洋战场出现过](../Page/太平洋战争.md "wikilink")，但到今天已经在全世界范围内基本绝迹了。

在[人类学界](../Page/人类学.md "wikilink")，猎首行为在社会中的角色一直是一个讨论的热点课题。当代学者普遍认为，猎首行为是曾经在社会群体和个人的等级制度的形成、强化和维持过程中扮演关键角色的一种仪式。一些人类学家提出的理论认为，猎首行为源于[宗教观念中](../Page/宗教.md "wikilink")“[灵魂栖于头颅中](../Page/灵魂.md "wikilink")”的看法，而猎首则是为了带回并最终征服敌人的灵魂。在人类学著作中，猎首行为出现的直接原因包括羞辱对手、统计战功、完成某些仪式、维持[星象平衡](../Page/星象.md "wikilink")、展现[阳刚](../Page/阳刚.md "wikilink")、勇猛、[嗜血和威信等等](../Page/吃人.md "wikilink")\[1\]。

## 部分文化中的猎首行为

### 中国

据[河南](../Page/河南.md "wikilink")[殷墟出土的](../Page/殷墟.md "wikilink")[甲骨文记载](../Page/甲骨文.md "wikilink")，[商代时中国便有猎首的风俗](../Page/商代.md "wikilink")。[殷商人把俘获的异族的](../Page/殷商.md "wikilink")[酋长当作](../Page/酋长.md "wikilink")[人牲](../Page/人牲.md "wikilink")[斩首](../Page/斩首.md "wikilink")，[祭祖之后](../Page/祭祖.md "wikilink")，用其[头盖骨刻上文字以记事](../Page/头盖骨.md "wikilink")，作为战胜的纪念，称为“[人头骨刻辞](../Page/人头骨刻辞.md "wikilink")”\[2\]。

到了[春秋战国时期](../Page/春秋战国.md "wikilink")，猎首行为在中国大规模兴起。[司馬遷](../Page/司馬遷.md "wikilink")《[史記](../Page/史記.md "wikilink")》記載，[春秋時](../Page/春秋.md "wikilink")，[晉國](../Page/晉國.md "wikilink")[趙襄子攻殺](../Page/趙襄子.md "wikilink")[知襄子之後](../Page/知襄子.md "wikilink")，將知襄子的[首級割下](../Page/首級.md "wikilink")，雕刻上漆，當[飲酒之](../Page/飲酒.md "wikilink")[首爵](../Page/骷髏杯.md "wikilink")。[南方](../Page/南方.md "wikilink")[楚國](../Page/楚國.md "wikilink")，在[屈原](../Page/屈原.md "wikilink")《[楚辞](../Page/楚辞.md "wikilink")》中的《[国殇](../Page/国殇.md "wikilink")》、《[礼魂](../Page/礼魂.md "wikilink")》篇中都有民间猎首习俗的记载\[3\]。[秦国在](../Page/秦国.md "wikilink")[商鞅变法后规定](../Page/商鞅变法.md "wikilink")，士兵的军功一律以斩获并带回的敌人首级多少来计算，計算功勞也非常公平。这一规定使得秦军的士气大为增强，其凶残程度也令各国士兵闻之而胆寒。有观点认为，猎首行为在秦灭[六国的战争中起了重要的作用](../Page/六国.md "wikilink")。

由於首級攜帶不易，除了重要敵將外，其餘都簡化為割耳或割鼻以替代之。

部分少数民族，如[佤族](../Page/佤族.md "wikilink")，直至20世纪50年代仍有猎首的习俗。\[4\]

### 日本

在古代[日本的戰爭中](../Page/日本.md "wikilink")，往往都是由[武士進行一對一的單挑](../Page/日本武士.md "wikilink")。在作戰前，雙方互相通報姓名，然後進行[決鬥](../Page/決鬥.md "wikilink")。當殺死對方後，武士將其[首級砍下來](../Page/首級.md "wikilink")，送往軍中驗看。然後按其斬殺的敵人地位的高低來論戰功行賞。這被日本人稱為。

在《[雍州府志](../Page/雍州府志.md "wikilink")·十·陵墓》中，有關於日本首實檢的記載：「凡本朝軍士，得敵首謂取首。或謂高名，依忠功高得武名之謂也。敵之所隨身物或冑或刀等物，添首取之來，謂分取高名。倭俗，一種謂一分。依之一種分來，故稱分取。敵首攜歸入主君之一覽，是謂實檢，蓋檢軍實之義乎。記首多少之書，謂首帖。」

### 台湾

猎首行为曾经在[台湾原住民中非常普遍](../Page/台湾原住民.md "wikilink")\[5\]，除隔絕於台灣東南外海的[蘭嶼](../Page/蘭嶼.md "wikilink")[達悟族](../Page/達悟族.md "wikilink")（舊稱[雅美族](../Page/雅美族.md "wikilink")）外，其他原住民族群不同部落之间都会相互猎首。[汉人移民到](../Page/汉人.md "wikilink")[台湾的早期](../Page/台湾.md "wikilink")，也常常成为原住民猎首的对象；[清代的](../Page/清.md "wikilink")《[蓬萊小語](../Page/蓬萊小語.md "wikilink")》：「時人入山，常遇靈怪悲號迴野，俗謂『討路費，散[冥鏹](../Page/冥鏹.md "wikilink")，可免。』遇怪悲號猶可，遇番悲號，則以首級為路費矣。」當時人認為在[山中遇到](../Page/山.md "wikilink")[鬼](../Page/鬼.md "wikilink")、[怪](../Page/妖怪.md "wikilink")，也比遇到出草的台灣原住民好多了。

[台湾日治时期的](../Page/台湾日治时期.md "wikilink")[臺灣總督府禁止出草](../Page/臺灣總督府.md "wikilink")，是世界首個真正嚴禁「出草」的[官府](../Page/官府.md "wikilink")。出草被視為野蠻行为，更被列为與[刑事](../Page/刑事.md "wikilink")[犯罪](../Page/犯罪.md "wikilink")，日本人反對出草的出發點與漢人相同。一些原住民坚持出草是傳統信仰，抵制政府的禁令。[日本人使用了許多高壓政策和計謀](../Page/日本人.md "wikilink")，包括血腥報復、連坐出草部落居民等方法来遏制出草。该猎首传统1930年代后逐渐消失。

### 东南亚和美拉尼西亚

[HeadTray2.jpg](https://zh.wikipedia.org/wiki/File:HeadTray2.jpg "fig:HeadTray2.jpg")1900年代早期制作的双头骨饰品（右）及七头骨饰品照片（左），现藏于[芝加哥](../Page/芝加哥.md "wikilink")[菲尔德自然历史博物馆](../Page/菲尔德自然历史博物馆.md "wikilink")。\]\]
东南亚和[美拉尼西亚的许多](../Page/美拉尼西亚.md "wikilink")[南岛民族中都有猎首的传统](../Page/南岛民族.md "wikilink")。人类学文献中对近代[菲律宾](../Page/菲律宾.md "wikilink")[易隆高族](../Page/易隆高族.md "wikilink")、[砂拉越的](../Page/砂拉越.md "wikilink")[伊班族和](../Page/伊班族.md "wikilink")[巴拉湾族](../Page/巴拉湾族.md "wikilink")、[加里曼丹岛](../Page/加里曼丹岛.md "wikilink")（[婆罗洲](../Page/婆罗洲.md "wikilink")）的[达雅族](../Page/达雅族.md "wikilink")、[美拉尼西亚的](../Page/美拉尼西亚.md "wikilink")[瓦纳族](../Page/瓦纳族.md "wikilink")、[苏拉威西岛西南部的](../Page/苏拉威西岛.md "wikilink")[马普伦多族等部族的猎首习俗有较详细的记载](../Page/马普伦多族.md "wikilink")。在这些部族中，猎取敌方首级多数为一种宗教仪式，而非战争行为。猎首通常标志着部族成员对己方战死者哀悼的结束。参与猎首行为可作为部族男性成员成年的标志，部族首领会依据猎取首级的多少对部族成员进行奖赏。

肯尼思·乔治（Kenneth
George）在对马普伦多族的考察中发现：他们的一年一度的“猎首”行为已经演化成以[椰子代替真实头颅的一种纯宗教仪式](../Page/椰子.md "wikilink")，称为“pangngae”\[6\]。该仪式在每年[水稻收获的时节举行](../Page/水稻.md "wikilink")，其目的包括展现不同部族文化间的分歧和辩论，显示男性的阳刚之美，分发公共物资，以及抵制外来文明对马普伦多族文明的同化等等。

[砂拉越地区的猎首行为](../Page/砂拉越.md "wikilink")1830年代被[拉者](../Page/拉者.md "wikilink")[詹姆斯·布鲁克勒令禁止](../Page/詹姆斯·布鲁克.md "wikilink")，而[呂宋](../Page/呂宋.md "wikilink")[易隆高族的猎首传统则在](../Page/易隆高族.md "wikilink")1930年代被当时管辖[菲律宾的](../Page/菲律宾.md "wikilink")[美國屬地](../Page/美國屬地.md "wikilink")[当局废止](../Page/当局.md "wikilink")。

### 印度

結合了[古印度](../Page/古印度.md "wikilink")[土著文化](../Page/土著.md "wikilink")，少部份[印度教](../Page/印度教.md "wikilink")[性力派信徒會有以人體](../Page/性力派.md "wikilink")、人[血或](../Page/血.md "wikilink")[首級为祭品](../Page/首級.md "wikilink")，奉獻给[时母](../Page/时母.md "wikilink")、[難近母等](../Page/難近母.md "wikilink")[女神的现象](../Page/女神.md "wikilink")。在[英国](../Page/英国.md "wikilink")[殖民统治时期](../Page/英属印度.md "wikilink")，少數信徒仍然有这种行为，在[英国殖民当局全力禁止下才逐渐消失](../Page/英国.md "wikilink")。

### 新西兰

[新西兰的土著](../Page/新西兰.md "wikilink")[毛利人在部族冲突中一般会挖去头盖骨然后用烟熏的方式保存敌人的头颅](../Page/毛利人.md "wikilink")。如今不少毛利人部族正试图追索流散于世界其他地方博物馆的他们祖先的头颅。

### 亚马逊地区

居住在今天[厄瓜多尔](../Page/厄瓜多尔.md "wikilink")、[秘鲁和](../Page/秘鲁.md "wikilink")[亚马逊平原地区的](../Page/亚马逊平原.md "wikilink")[舒阿尔族有猎取人头后脱水作为祭品的习俗](../Page/舒阿尔族.md "wikilink")。今天舒阿尔人出售给游客的纪念品中就包括这些祭物的仿制品。而在一些边远地区的舒阿尔部落中，猎首现象仍未绝迹。

### 凯尔特人

古代欧洲[凯尔特人的猎首行为也与宗教相关](../Page/凯尔特.md "wikilink")，但具体目的至今不明。[古罗马人和](../Page/古罗马.md "wikilink")[古希腊人均有关于凯尔特人将私敌的首级用钉子钉于自家墙上或悬挂于马脖子下的的习俗](../Page/古希腊.md "wikilink")\[7\]。[苏格兰的](../Page/苏格兰.md "wikilink")[盖尔人将这一习俗延续了很长的时间](../Page/盖尔人.md "wikilink")，可能在皈依[基督教后才被摈弃](../Page/基督教.md "wikilink")。在[日耳曼人和](../Page/日耳曼人.md "wikilink")[伊比利亚人部族中亦有猎首习俗](../Page/伊比利亚.md "wikilink")，但原因至今仍未能查明。

## 第二次世界大战中的猎首行为

[第二次世界大战中](../Page/第二次世界大战.md "wikilink")，[盟军](../Page/同盟国_\(第二次世界大战\).md "wikilink")（主要是[美军](../Page/美国军事.md "wikilink")）士兵在[太平洋战场上曾大量收集](../Page/太平洋战争.md "wikilink")[日军阵亡士兵的头盖骨作为战利品或馈赠亲友的纪念品](../Page/日军.md "wikilink")（但在欧洲战场上的盟军却无此行为）。美军太平洋舰队总司令曾于1942年9月严令禁止上述行为，但未取得显著效果。1944年5月22日的《[生活](../Page/生活_\(雜誌\).md "wikilink")》杂志曾刊登一幅年轻女子以她男友（海军士兵）寄给她的日军头盖骨为背景的照片\[8\]
\[9\]，当时曾在[美国引起公愤](../Page/美国.md "wikilink")。

## 相關條目

  - [出草](../Page/出草.md "wikilink")

  - [梟首](../Page/梟首.md "wikilink")

  - [骷髏杯](../Page/骷髏杯.md "wikilink")

  - [縮頭術](../Page/縮頭術.md "wikilink")

  - [嘎巴拉碗](../Page/嘎巴拉碗.md "wikilink")

  -
  - [人牲](../Page/人牲.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:人类学](../Category/人类学.md "wikilink")
[Category:人祭](../Category/人祭.md "wikilink")

1.  山田仁史：《首狩の宗教民族学》筑摩書房，2015（日文）
2.  黄天树：《甲骨文中有关猎首风俗的记载》，《中国文化研究》杂志，2005年第2期
3.  凌纯声：《国殇礼魂与馘首祭枭》，中央研究院民族学研究所集刊，1960，**9**：pp. 436
4.  [佤族人民自己革除“猎头祭谷”习俗](http://www.mzb.com.cn/html/report/85290-1.htm)
5.  台北原住民[圓山遺址\#圓山文化](../Page/圓山遺址#圓山文化.md "wikilink")
6.  Kenneth George, *Showing signs of violence: The cultural politics of
    a twentieth-century headhunting ritual*, Berkeley: University of
    California Press, 1996, ISBN 0-520-20041-1
7.  George Diodorus, *The Historical Library of Diodorus the Sicilian*,
    [Google Books
    预览](http://books.google.com/books?id=agd-eLVNRMMC&printsec=titlepage#PPA315,M1)。
8.  Paul Fussell, *Wartime: Understanding and Behavior in the Second
    World War*, New York: Oxford University Press, 1990, p. 117.
9.  Simon Harrison, "Skull Trophies of the Pacific War: Transgressive
    Objects of remembrance./Les Trophees De la Guerre Du Pacifique Des
    Cranes Comme Souvenirs Transgressifs", *Journal of the Royal
    Anthropological Institute*, 2006, 12 (4): 817