**猛鹿**（）是[日本將棋的棋子之一](../Page/日本將棋.md "wikilink")。有在[和將棋](../Page/和將棋.md "wikilink")、[大局將棋出現](../Page/大局將棋.md "wikilink")。

猛鹿是[登猿](../Page/登猿.md "wikilink")（）與大局將棋的[盲犬](../Page/盲犬.md "wikilink")（）的升級棋。可升級為**行豬**（，日本簡化字為）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>猛鹿</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p><font color="white">■</font></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p><font color="white">■</font></p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>麈</strong></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
<td><p>可循前方、斜向走一格。</p></td>
<td><p>行豬</p></td>
</tr>
</tbody>
</table>

## 參見

  - [日本將棋](../Page/日本將棋.md "wikilink")
  - [日本將棋變體](../Page/日本將棋變體.md "wikilink")
  - [和將棋](../Page/和將棋.md "wikilink")
  - [大局將棋](../Page/大局將棋.md "wikilink")

[Category:日本將棋棋子](../Category/日本將棋棋子.md "wikilink")
[Category:和將棋棋子](../Category/和將棋棋子.md "wikilink")
[Category:大局將棋棋子](../Category/大局將棋棋子.md "wikilink")