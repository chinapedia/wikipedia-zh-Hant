**猴头菇**（[学名](../Page/学名.md "wikilink")：**），又称**猴头菌**、**猴頭蘑**、**猴菇**、**刺猬菌**、**猬菌**、**猴蘑**，[傘菌綱](../Page/傘菌綱.md "wikilink")，[猴頭菇科](../Page/猴頭菇科.md "wikilink")，是[中国传统的名贵菜肴](../Page/中国.md "wikilink")，肉嫩、味香、鲜美可口。

《中國藥用真菌》記載：“猴頭菇味甘、性平、能利五臟、助消化、滋補、對消化不良、神經衰退與十二指腸潰瘍及胃潰瘍有良好的功效。”因此，廣泛的將猴頭菇應用於食道癌、胃癌、十二指腸癌等消化系統的腫瘤治療上。經多種臨床實驗證明，經常服食猴頭菇會增強人體消化系統的免疫功能。

其营养价值极高。[氨基酸种类多过](../Page/氨基酸.md "wikilink")16种以上，并含有多种[维生素和较高的矿物质成分](../Page/维生素.md "wikilink")。猴头菇还含有很多药效成分。具有利五脏、助消化、滋补身体等功能，对[消化不良](../Page/消化不良.md "wikilink")、[胃溃疡](../Page/胃溃疡.md "wikilink")、[十二指肠溃疡](../Page/十二指肠溃疡.md "wikilink")、[神经衰弱均有一定疗效](../Page/神经衰弱.md "wikilink")。同时，猴头菇还含有多种糖类物质对[癌细胞有明显的抑制作用](../Page/癌细胞.md "wikilink")。

目前栽培方式以[太空包栽培法居多](../Page/太空包.md "wikilink")。

[Category:猴頭菇屬](../Category/猴頭菇屬.md "wikilink")
[Category:食用菌](../Category/食用菌.md "wikilink")
[Category:食療](../Category/食療.md "wikilink")
[Category:藥用真菌](../Category/藥用真菌.md "wikilink")