[Performing_monkey_at_Dazaifu_Tenman-gū,_Fukuoka_Prefecture,_Japan_-_20081214.jpg](https://zh.wikipedia.org/wiki/File:Performing_monkey_at_Dazaifu_Tenman-gū,_Fukuoka_Prefecture,_Japan_-_20081214.jpg "fig:Performing_monkey_at_Dazaifu_Tenman-gū,_Fukuoka_Prefecture,_Japan_-_20081214.jpg")
**猴戲**，又名**猴子戲**，泛指[猴子或其他](../Page/猴子.md "wikilink")[靈長類動物](../Page/靈長類.md "wikilink")（如[猩猩](../Page/猩猩.md "wikilink")）參與演出的[表演藝術](../Page/表演藝術.md "wikilink")。

## 概要

### 中國傳統猴戲

猴戲在[中國已有悠久](../Page/中國.md "wikilink")[歷史](../Page/歷史.md "wikilink")，最遲於[唐朝已有出現](../Page/唐朝.md "wikilink")，發源於今河南省[新野縣](../Page/新野縣.md "wikilink")。古人把猴子視為[馬的守護神](../Page/馬.md "wikilink")，常於[馬廄內養猴子以留住馬匹](../Page/馬廄.md "wikilink")，並讓猴子表演猴戲作[祭祀之用](../Page/祭祀.md "wikilink")，因此猴子又有「馬留」的別稱，此名稱至今仍保留在粵語中。[日本於](../Page/日本.md "wikilink")[奈良時代從中國傳入猴戲](../Page/奈良時代.md "wikilink")。

後來猴戲的[宗教性變淡](../Page/宗教.md "wikilink")，有些只是純[娛樂](../Page/娛樂.md "wikilink")[觀眾](../Page/觀眾.md "wikilink")，一些[小販為招攬](../Page/小販.md "wikilink")[顧客](../Page/顧客.md "wikilink")，也會養猴子表演賣技藝，尤其常見於賣[武術](../Page/武術.md "wikilink")、賣[藥等](../Page/藥.md "wikilink")[行業](../Page/行業.md "wikilink")。現時[香港唯一一隻獲](../Page/香港.md "wikilink")[漁農自然護理署發牌由人](../Page/漁農自然護理署.md "wikilink")[飼養的猴子](../Page/馴養動物.md "wikilink")[金鷹](../Page/金鷹_\(猴子\).md "wikilink")，其[已故前主人](../Page/死亡.md "wikilink")[陳日標生前以賣藥為業](../Page/陳日標.md "wikilink")，金鷹就在主人賣藥時表演。

### 日本傳統猴戲

**另參見：[廐神](../Page/廐神.md "wikilink")**
[日本把猴戲稱為](../Page/日本.md "wikilink")（），有時也寫成，屬於[傳統藝能](../Page/日本傳統藝能.md "wikilink")。猴戲最初傳入日本時，也是於馬廄中作為祭祀[驅邪之用](../Page/驅邪.md "wikilink")，常見於[武家的馬廄](../Page/武家.md "wikilink")。[室町時代起猴戲宗教性淡化](../Page/室町時代.md "wikilink")，發展成[街頭藝術](../Page/街頭藝術.md "wikilink")，多以[太鼓伴奏](../Page/太鼓.md "wikilink")。由於日文中「猿」的讀音是，與表示離開、驅離之意的「去る」唸法一樣，有吉祥的意味，因此發展成過年時常見的表演。[江戶時代更認為是種讓病魔遠離的象徵](../Page/江戶時代.md "wikilink")，所以還有專門引導猴子耍猴戲驅邪魔、病魔的[職業](../Page/職業.md "wikilink")。

此外，日本的[狂言中也有猴戲表演](../Page/狂言.md "wikilink")，稱為[靫猿](../Page/靫猿.md "wikilink")。

### 現代猴戲表演

現在一些[動物園](../Page/動物園.md "wikilink")、[主題公園](../Page/主題公園.md "wikilink")、[馬戲團也有猴戲的表演項目](../Page/馬戲團.md "wikilink")，常見的有踏[單車](../Page/單車.md "wikilink")、[跳火圈](../Page/跳火圈.md "wikilink")、走[鋼線](../Page/鋼線.md "wikilink")、翻[筋斗](../Page/筋斗.md "wikilink")、[芭蕾舞](../Page/芭蕾舞.md "wikilink")、走[平行木](../Page/平行木.md "wikilink")、[默劇等](../Page/默劇.md "wikilink")。

### 其他作用

有些猴戲是為了[行騙](../Page/欺騙.md "wikilink")，例如假裝[死亡](../Page/死亡.md "wikilink")，引發觀眾的[同情心](../Page/同情心.md "wikilink")，因而獲得特別的償[錢](../Page/錢.md "wikilink")。

### 其他用法

猴戲除了指真正由猿猴表演的項目外，還用來比喻人們一些裝模作樣流於表面的行為。

## 內部連結

  -
  - [工作猴](../Page/工作猴.md "wikilink")（）

  - [阿笨與阿占](../Page/阿笨與阿占.md "wikilink")

  - [金鷹 (猴子)](../Page/金鷹_\(猴子\).md "wikilink")

  - [大笪地](../Page/大笪地.md "wikilink")

  - [西遊記](../Page/西遊記.md "wikilink")

  - [馬戲班](../Page/馬戲班.md "wikilink")

  - [廐神](../Page/廐神.md "wikilink")

## 外部連結

  - [不要政治**馬騮戲**，只要真普選](http://hk.groups.yahoo.com/group/hkfs-news/message/104)

  - [東北地方の厩猿信仰](https://web.archive.org/web/20130521132816/http://www.isop.ne.jp/atrui/ushi/07_tomo/mayazaru/02_nakamura.html)

  - [牛馬の守護神 -
    厩猿信仰](https://web.archive.org/web/20130521133146/http://www.isop.ne.jp/atrui/ushi/07_tomo/mayazaru/06_3.html)

## 參考資料

  - [猿回し](http://iroha-japan.net/iroha/C04_vaudeville/11_sarumawashi.html)

  - [東北地方の厩猿信仰](https://web.archive.org/web/20130521132816/http://www.isop.ne.jp/atrui/ushi/07_tomo/mayazaru/02_nakamura.html)

  - [牛馬の守護神 -
    厩猿信仰](https://web.archive.org/web/20130521133146/http://www.isop.ne.jp/atrui/ushi/07_tomo/mayazaru/06_3.html)

[Category:表演藝術](../Category/表演藝術.md "wikilink")
[Category:街頭藝術](../Category/街頭藝術.md "wikilink")
[Category:馴養動物](../Category/馴養動物.md "wikilink")
[Category:靈長目](../Category/靈長目.md "wikilink")
[Category:祭祀](../Category/祭祀.md "wikilink")