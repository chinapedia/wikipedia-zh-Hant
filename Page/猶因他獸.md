**猶因他獸**（[學名](../Page/學名.md "wikilink")：，意思為「**[猶因塔山脈的野獸](../Page/猶因塔山脈.md "wikilink")**」），又稱為**恐角獸**，是一[屬已](../Page/屬.md "wikilink")[滅絕的](../Page/滅絕.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")，總共包含2種。*U.
anceps*發現於早中期[始新世的](../Page/始新世.md "wikilink")[美國](../Page/美國.md "wikilink")[懷俄明州近](../Page/懷俄明州.md "wikilink")[福特·布瑞橘爾](../Page/福特·布瑞橘爾.md "wikilink")，*U.
insperatus*則發現於中晚期使新世的[中國](../Page/中國.md "wikilink")[河南省](../Page/河南省.md "wikilink")。\[1\]猶因他獸為[化石戰爭的焦點之一](../Page/化石戰爭.md "wikilink")，[古生物學家](../Page/古生物學家.md "wikilink")[愛德華·德林克·科普](../Page/愛德華·德林克·科普.md "wikilink")（Edward
Drinker Cope）與[奧塞內爾·查利斯·馬什](../Page/奧塞內爾·查利斯·馬什.md "wikilink")（Othniel
Charles Marsh）先後針對*U. anceps*的化石遺骸提出了22種命名，然而現今多已廢棄不採用。\[2\]

猶因他獸生存於4500-4000萬年前[始新世早至中期](../Page/始新世.md "wikilink")。牠們是[草食性的](../Page/草食性.md "wikilink")，以樹葉、草及灌木為食物。猶因他獸生活在近水的地方，並以其像劍的犬齒來刺起水中或[沼澤的](../Page/沼澤.md "wikilink")[植物來吃](../Page/植物.md "wikilink")。牠們的滅絕可能是因[氣候的改變及雷獸的競爭所致](../Page/氣候.md "wikilink")。

## 描述

[Uinttather_DB.jpg](https://zh.wikipedia.org/wiki/File:Uinttather_DB.jpg "fig:Uinttather_DB.jpg")
猶因他獸為大型的[草食性動物](../Page/草食性.md "wikilink")，體長可達，肩高可達，體重則有2[噸](../Page/噸.md "wikilink")，體型上類似現今的[犀牛](../Page/犀牛.md "wikilink")，縱使兩者之間並沒有很近的親緣關係。\[3\]

### 顱骨

[Dinoceras_mirabile_Marsh_MNHN.jpg](https://zh.wikipedia.org/wiki/File:Dinoceras_mirabile_Marsh_MNHN.jpg "fig:Dinoceras_mirabile_Marsh_MNHN.jpg")\]\]
猶因他獸最特別的地方是牠的[頭顱骨](../Page/頭顱骨.md "wikilink")，非常巨大堅實，然後卻又具有明顯的凹陷，這項特殊的特徵除了猶因他獸外只存在[雷獸科的物種上](../Page/雷獸科.md "wikilink")。由於牠的頭蓋骨非常厚，故[顱腔很細小](../Page/顱腔.md "wikilink")。頭顱骨上有很多竇房，可以減輕其重量，如同[大象的顱骨](../Page/大象.md "wikilink")。

最大的上[犬齒與](../Page/犬齒.md "wikilink")[劍齒虎的相似](../Page/劍齒虎.md "wikilink")，可能是用來作為防禦武器使用，或者是用來在拔取[沼澤中的水生](../Page/沼澤.md "wikilink")[植物](../Page/植物.md "wikilink")。上犬齒呈現[兩性異形](../Page/兩性異形.md "wikilink")，雄性的較雌性的為大。

雄性頭顱骨的前額部份上有6隻骨質[角](../Page/角_\(動物\).md "wikilink")，類似於現今的[長頸鹿](../Page/長頸鹿.md "wikilink")（和犀牛角不同，犀牛角由[角蛋白凝合而成](../Page/角蛋白.md "wikilink")）。這些角的功用仍是一個謎，有可能用作防禦或是雄性間在爭奪配偶時行展示用途。

## 外部連結

  - [Academy of Natural
    Sciences](http://www.ansp.org/museum/leidy/paleo/uintatherium.php)
  - [National Park
    Service](http://www.cr.nps.gov/history/online_books/fobu/sec4b.htm)

## 參考文獻

[Category:猶因他獸科](../Category/猶因他獸科.md "wikilink")
[Category:恐角目](../Category/恐角目.md "wikilink")
[Category:亞洲史前哺乳動物](../Category/亞洲史前哺乳動物.md "wikilink")
[Category:北美洲史前哺乳動物](../Category/北美洲史前哺乳動物.md "wikilink")

1.
2.  Bill Bryson, "A Short History Of Nearly Everything", Doubleday
    (2003).
3.  <http://www.enchantedlearning.com/subjects/mammals/Iceagemammals.shtml>