**猶尼昂县**（**Union County, South
Carolina**）是[美國](../Page/美國.md "wikilink")[南卡羅萊納州西北部的一個縣](../Page/南卡羅萊納州.md "wikilink")。面積1,336平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口29,881人。縣治[猶尼昂](../Page/猶尼昂_\(南卡羅萊納州\).md "wikilink")
(Union)。

成立於1785年。縣名指的是服務[長老會和](../Page/長老會.md "wikilink")[聖公會的聯合教會](../Page/美國聖公會.md "wikilink")
(Union Church)。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[U](../Category/南卡罗来纳州行政区划.md "wikilink")

1.