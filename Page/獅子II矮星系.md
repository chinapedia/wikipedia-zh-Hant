**獅子II** （或**Leo
B**）是[本星系群中的一個](../Page/本星系群.md "wikilink")[矮星系](../Page/矮星系.md "wikilink")，位於[獅子座](../Page/獅子座.md "wikilink")，距離大約701,000[光年](../Page/光年.md "wikilink")（215[Kpc](../Page/秒差距.md "wikilink")），直徑大約4,200光年，是[銀河系的衛星系](../Page/銀河系.md "wikilink")。牠於1950年被[羅伯特
G.
哈靈頓和](../Page/羅伯特_G._哈靈頓.md "wikilink")[艾伯特·喬治·威爾遜在](../Page/艾伯特·喬治·威爾遜.md "wikilink")[加利福尼亞州的](../Page/加利福尼亞州.md "wikilink")[威爾遜山天文台和](../Page/威爾遜山天文台.md "wikilink")[帕洛馬天文台共同發現](../Page/帕洛馬天文台.md "wikilink")。

## 相關條目

  - [獅子座 I 矮星系](../Page/獅子座_I_矮星系.md "wikilink")

## 外部連結

  - [Leo II @
    SEDS](https://web.archive.org/web/20080301224948/http://www.seds.org/~spider/spider/LG/leo2.html)
  - [The Internal Kinematics of the Leo II Dwarf Spheroidal
    Galaxy](https://web.archive.org/web/20070929131842/http://www.aas.org/publications/baas/v26n4/aas185/abs/S5104.html)
  - [The Stellar Populations of the Leo II Dwarf Spheroidal
    Galaxy](https://web.archive.org/web/20070926223728/http://www.aas.org/publications/baas/v26n4/aas185/abs/S5105.html)
  - [Leo II @ The Encyclopedia of Astrobiology, Astronomy &
    Spaceflight](http://www.daviddarling.info/encyclopedia/L/Leo_II.html)
  - \[<http://adsabs.harvard.edu/cgi-bin/nph-bib_query?bibcode=1993MNRAS.261>..657D\&db_key=AST\&data_type=HTML\&format=
    Deep CCD photometry of the dwarf spheroidal galaxy Leo II\]

## 參考資料

  - R. G. Harrington and A. G. Wilson, 1950. "Two New Stellar Systems in
    Leo." *[Publications of the Astronomical Society of the
    Pacific](../Page/Publications_of_the_Astronomical_Society_of_the_Pacific.md "wikilink")*,
    Vol. 62, No. 365, p. 118

<div class="references-small">

<references/>

</div>

[Category:橢圓星系](../Category/橢圓星系.md "wikilink")
[Category:矮橢圓星系](../Category/矮橢圓星系.md "wikilink")
[Category:特殊星系](../Category/特殊星系.md "wikilink")
[Category:銀河次集團](../Category/銀河次集團.md "wikilink")
[Category:獅子座](../Category/獅子座.md "wikilink")