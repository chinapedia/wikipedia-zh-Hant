**獅子座A**（也稱為獅子座 III）
是一個屬於[本星系群的](../Page/本星系群.md "wikilink")[不規則星系](../Page/不規則星系.md "wikilink")，距離[地球](../Page/地球.md "wikilink")225萬光年遠。

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:矮不規則星系](../Category/矮不規則星系.md "wikilink")
[Category:獅子座](../Category/獅子座.md "wikilink")
[Category:不規則星系](../Category/不規則星系.md "wikilink")