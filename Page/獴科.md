**獴科**（学名
*Herpestidae*），[哺乳綱](../Page/哺乳綱.md "wikilink")[食肉目的一科](../Page/食肉目.md "wikilink")，外形较像猫，有[貓鼬的別稱](../Page/貓鼬.md "wikilink")。最新的分类方法只包括獴亚科一个亚科。

## 分类

  - [沼泽獴属](../Page/沼泽獴属.md "wikilink") *Atilax*
      - [沼泽獴](../Page/沼泽獴.md "wikilink") *Atilax paludinosus*
  - [臭獴属](../Page/臭獴属.md "wikilink") *Bdeogale*
  - [长毛獴属](../Page/长毛獴属.md "wikilink") *Crossarchus*
  - [笔尾獴属](../Page/笔尾獴属.md "wikilink") *Cynictis*
      - [笔尾獴](../Page/笔尾獴.md "wikilink") *Cynictis penicillata*
  - [草原獴属](../Page/草原獴属.md "wikilink") *Dologale*
      - [草原獴](../Page/草原獴.md "wikilink") *Dologale dybowskii*
  - [貂獴属](../Page/貂獴属.md "wikilink") *Galerella*
  - [侏獴属](../Page/侏獴属.md "wikilink") *Helogale*
      - [侏獴](../Page/侏獴.md "wikilink") *Helogale parvula*
  - [獴属](../Page/獴属.md "wikilink") *Herpestes*
      - [短尾獴](../Page/短尾獴.md "wikilink") *Herpestes brachyurus*
      - [灰獴](../Page/灰獴.md "wikilink") *Herpestes edwardsii*
      - [褐獴](../Page/褐獴.md "wikilink") *Herpestes fuscus*
      - [埃及獴](../Page/埃及獴.md "wikilink") *Herpestes ichneumon*
      - [长鼻獴](../Page/长鼻獴.md "wikilink") *Herpestes naso*
      - [领獴](../Page/领獴.md "wikilink") *Herpestes semitorquatus*
      - [赤獴](../Page/赤獴.md "wikilink") *Herpestes smithii*
      - [食蟹獴](../Page/食蟹獴.md "wikilink") *Herpestes urva*
      - [纹颈獴](../Page/纹颈獴.md "wikilink") *Herpestes vitticollis*
      - [紅頰獴](../Page/紅頰獴.md "wikilink") *Herpestes javanicus*
  - [白尾獴属](../Page/白尾獴属.md "wikilink") *Ichneumia*
      - [白尾獴](../Page/白尾獴.md "wikilink") *Ichneumia albicauda*
  - [库氏獴属](../Page/库氏獴属.md "wikilink") *Liberiictus*
      - [库氏獴](../Page/库氏獴.md "wikilink") *Liberiictis kuhni*
  - [缟獴属](../Page/缟獴属.md "wikilink") *Mungos*
      - [缟獴](../Page/缟獴.md "wikilink") *Mungos mungo*
  - [梅氏獴属](../Page/梅氏獴属.md "wikilink") *Rhynchogale*
      - [梅氏獴](../Page/梅氏獴.md "wikilink") *Rhynchogale melleri*
  - [细尾獴属](../Page/细尾獴属.md "wikilink") *Suricata*
      - [细尾獴](../Page/细尾獴.md "wikilink") *Suricata suricatta*

以下幾屬過去歸爲**獴科**，現在被分入[食蟻狸科](../Page/食蟻狸科.md "wikilink")（）：

  - [環尾獴属](../Page/環尾獴属.md "wikilink") *Galidia*
      - [環尾獴](../Page/環尾獴.md "wikilink") *Galidia elegans*
  - [寬尾獴属](../Page/寬尾獴属.md "wikilink") *Galidictis*
  - [窄纹獴属](../Page/窄纹獴属.md "wikilink") *Mungotictis*
      - [窄纹獴](../Page/窄纹獴.md "wikilink") *Mungotictis decemlineata*
  - [純色獴属](../Page/純色獴属.md "wikilink") *Salanoia*
      - [純色獴](../Page/純色獴.md "wikilink") *Salanoia concolor*

## 外部链接

[\*](../Category/獴科.md "wikilink")
[Category:食肉目](../Category/食肉目.md "wikilink")