[Balai_sorcière_admin.jpg](https://zh.wikipedia.org/wiki/File:Balai_sorcière_admin.jpg "fig:Balai_sorcière_admin.jpg")
[Persecution_of_witches.jpg](https://zh.wikipedia.org/wiki/File:Persecution_of_witches.jpg "fig:Persecution_of_witches.jpg")
[Wickiana5.jpg](https://zh.wikipedia.org/wiki/File:Wickiana5.jpg "fig:Wickiana5.jpg")[巴登三名巫師被處以](../Page/巴登_\(瑞士\).md "wikilink")[火刑](../Page/火刑.md "wikilink")，由收集在[Wickiana集中](../Page/Wickiana.md "wikilink")，1585年\]\]

**獵巫**是原指搜捕[女巫与](../Page/女巫.md "wikilink")[巫师或施行](../Page/巫师.md "wikilink")[巫術的證據將被指控的人帶上](../Page/巫術.md "wikilink")[宗教審判](../Page/宗教.md "wikilink")[法庭](../Page/法庭.md "wikilink")。今日此類事件被視為一種[道德恐慌及](../Page/道德恐慌.md "wikilink")[政治迫害](../Page/政治迫害.md "wikilink")。從12世紀開始，到16世紀是最高峰的時期。當時[基督教](../Page/基督教.md "wikilink")（包括[天主教](../Page/天主教.md "wikilink")、[東正教等](../Page/東正教.md "wikilink")）已經傳入[歐洲將近一千年](../Page/歐洲.md "wikilink")，並且幾乎成為歐洲的唯一宗教。源自於對於未知的恐懼和對巫術的害怕導致獵殺女巫，而[法蘭西英雄](../Page/法蘭西王國.md "wikilink")[聖女貞德也曾被視為女巫](../Page/聖女貞德.md "wikilink")。

其實初時男巫和女巫被指控的人數一樣多，直到了1487年[海因里希·克雷默及約翰](../Page/海因里希·克雷默.md "wikilink")·斯普倫格著作的獵巫手冊《[女巫之槌](../Page/女巫之槌.md "wikilink")》出版後，令整個歐洲社會把獵巫的矛頭指向女性。書中宣稱「巫術是來自肉體的色慾，這在女人身上是永難滿足的，[魔鬼知道女人喜愛肉體樂趣](../Page/魔鬼.md "wikilink")，於是以性的愉悅誘使她們效忠」。所以在不少有關女巫指控的法庭文獻當中，都有跟[魔鬼性交的罪名](../Page/魔鬼.md "wikilink")。被判[死刑的女巫](../Page/死刑.md "wikilink")，財產會被沒收，令政界和司法界對獵巫運動更加積極。在巔峰期的16世紀，獵巫審判大多是由世俗法庭而非宗教法院審判。一开始猎巫都是用[絞刑的](../Page/絞刑.md "wikilink")，但因为據說女巫与巫师死掉后留下的的[尸体会变成](../Page/尸体.md "wikilink")[吸血鬼](../Page/吸血鬼.md "wikilink")，所以后来就使用[火刑](../Page/火刑.md "wikilink")，因为要把尸体也摧毁掉以绝后患。

廣義來說，女巫是指使用[魔術](../Page/魔術.md "wikilink")、唸咒、妖術的女性，在西方童話故事裡頗為常見，大多以鷹鉤鼻的老年女性的形象出現，披著黑色斗篷、相貌異於常人、喜歡喃喃自語，在大鍋煮著[蜥蜴](../Page/蜥蜴.md "wikilink")、[蝙蝠](../Page/蝙蝠.md "wikilink")、[蜘蛛](../Page/蜘蛛.md "wikilink")、[毒蛇等食材](../Page/毒蛇.md "wikilink")，熬製成濃稠的湯藥。這個印象，一般認為在[狩獵女巫的歷史中就已經定型了](../Page/狩獵女巫.md "wikilink")。另外，運用西洋[占星術或](../Page/占星術.md "wikilink")[神祕學](../Page/神祕學.md "wikilink")，為人[占卜吉凶](../Page/占卜.md "wikilink")、祈福、施咒的婦女，有時也被視為女巫。

在[迫害的巔峰期](../Page/迫害.md "wikilink")，很多不懂巫術的女人也遭人誣告罪成而被活活燒死。從1450到1750年約有三萬五千至十萬人以「獵巫」的名義被處決，尤其集中在[宗教改革時期](../Page/宗教改革.md "wikilink")；直到17世紀末[北美殖民地的](../Page/北美.md "wikilink")[塞勒姆審巫案還使](../Page/塞勒姆審巫案.md "wikilink")19人死於絞刑，1人被石頭堆壓死，5人死於獄中。

## 人類學上的起因

對巫術的信仰在世界各地都顯示出相似性。它提出了一個框架來解釋如疾病或死亡等隨機不幸的發生，而巫婆巫師則提供了邪惡的形象。\[1\]地理和文化上相對分離的社會（[歐洲](../Page/歐洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[印度](../Page/印度.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")）普遍存在獵巫行為，這一點自1960年以來已引發了[人类学背景下對該行為的關注](../Page/人类学.md "wikilink")。對[魔法和](../Page/魔法.md "wikilink")[占卜的信仰](../Page/占卜.md "wikilink")，以及用魔法影響個人幸福的嘗試（增加壽命，贏得愛情等等）都是人類文化的共通之處。現代地理大發現時代的早期收集的關於[美洲](../Page/美洲.md "wikilink")、[亞洲和](../Page/亞洲.md "wikilink")[非洲](../Page/非洲.md "wikilink")[土著人行為的報告顯示](../Page/土著.md "wikilink")，不衹是巫術信仰，獵巫行為的週期性爆發也是人類文化的普遍性。\[2\]

獵巫在現代仍有發生，其多發於無知或未教育的人，與世隔離，過著傳統生活型態的人可能會指控某人為女巫。但今日此詞多用於搜索誤導或潛在的敵人（如色情業者、[次文化族群和異見份子](../Page/次文化.md "wikilink")），就如歷史上的獵巫一般以[歇斯底里](../Page/歇斯底里.md "wikilink")、成見和不公正對待被指控的人。一項研究發現，巫術信仰與下列反社會的態度相關：較低的信任水平、慈善捐助和群體參與。\[3\]另一項研究發現，收入衝擊（由于極端降雨）導致了[坦桑尼亞](../Page/坦桑尼亞.md "wikilink")「女巫」的謀殺案大幅增加。\[4\]

## 参考文献

## 外部链接

  - [有關女巫](http://www.onewitch.url)

## 參见

  - [安东·普雷托里亚斯](../Page/安东·普雷托里亚斯.md "wikilink")
  - [魔鬼交易](../Page/魔鬼交易.md "wikilink")
  - [政治迫害](../Page/政治迫害.md "wikilink")

[\*](../Category/獵巫運動.md "wikilink")
[Category:屠殺](../Category/屠殺.md "wikilink")
[Category:基督教歷史](../Category/基督教歷史.md "wikilink")
[Category:教會法](../Category/教會法.md "wikilink")
[Category:群体心理学](../Category/群体心理学.md "wikilink")

1.  Jean Sybil La Fontaine, *Speak of the devil: tales of satanic abuse
    in contemporary England*, Cambridge University Press, 1998, ISBN
    978-0-521-62934-8, pp. 34–37.
2.  Behringer (2004), 50.
3.
4.