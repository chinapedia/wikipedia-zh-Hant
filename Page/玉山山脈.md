[Taiwan-Yushan_Range.jpg](https://zh.wikipedia.org/wiki/File:Taiwan-Yushan_Range.jpg "fig:Taiwan-Yushan_Range.jpg")
**玉山山脈**為[台灣五大山脈之一](../Page/台灣五大山脈.md "wikilink")。位於[南台灣](../Page/南台灣.md "wikilink")，北起南投縣[水里鄉](../Page/水里鄉.md "wikilink")[濁水溪南岸](../Page/濁水溪.md "wikilink")，南抵[高雄市的](../Page/高雄市.md "wikilink")[六龜區](../Page/六龜區.md "wikilink")[十八羅漢山附近](../Page/十八羅漢山.md "wikilink")，長約180[公里](../Page/公里.md "wikilink")，是[台灣五大山脈中最短者](../Page/台灣五大山脈.md "wikilink")。玉山山脈的東側為[中央山脈南端](../Page/中央山脈.md "wikilink")，兩者隔荖濃溪為界；西側則隔著[旗山溪](../Page/旗山溪.md "wikilink")，與[阿里山山脈為鄰](../Page/阿里山山脈.md "wikilink")。由於[日治時代](../Page/台灣日治時期.md "wikilink")[日本人發現此山比](../Page/日本人.md "wikilink")[富士山還高](../Page/富士山.md "wikilink")，為當時[日本國境內最高峰](../Page/日本國.md "wikilink")，被稱為**[新高山脈](../Page/新高山脈.md "wikilink")**。玉山山脈對[台灣的代表性可從](../Page/台灣.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[新台幣](../Page/新台幣.md "wikilink")[1000元大鈔上發現](../Page/1000元.md "wikilink")

## 特色

[Mount_Yu_Shan_-_Taiwan.jpg](https://zh.wikipedia.org/wiki/File:Mount_Yu_Shan_-_Taiwan.jpg "fig:Mount_Yu_Shan_-_Taiwan.jpg")
玉山山脈的走向先是東西向，再轉為北北東—南南西方向，最高峰[玉山](../Page/玉山.md "wikilink")（又稱[玉山主峰](../Page/玉山主峰.md "wikilink")）海拔達3952公尺，是台灣第一高峰，也使[臺灣島成為世界地勢高度第四高的島嶼](../Page/臺灣島.md "wikilink")。著名的[台灣百岳高峰中](../Page/台灣百岳.md "wikilink")，玉山山脈名列前十名的就有五座，如[玉山主峰](../Page/玉山主峰.md "wikilink")（3952公尺）為第一高峰、[玉山東峰](../Page/玉山東峰.md "wikilink")（3869公尺）為台灣百岳第三高峰、[玉山北峰](../Page/玉山北峰.md "wikilink")（3858公尺）為台灣百岳第四高峰、[玉山南峰](../Page/玉山南峰.md "wikilink")（3844公尺）為台灣百岳第五高峰、[東小南山](../Page/東小南山.md "wikilink")（3711公尺）為台灣百岳第九高峰。這些高山皆位於[玉山主峰周邊](../Page/玉山主峰.md "wikilink")，合稱[玉山群峰](../Page/玉山#玉山群峰.md "wikilink")，隸屬[玉山國家公園管理局轄區](../Page/玉山國家公園.md "wikilink")。

## 形成

[Yushan_satellite_photo.jpg](https://zh.wikipedia.org/wiki/File:Yushan_satellite_photo.jpg "fig:Yushan_satellite_photo.jpg")
玉山山塊因[歐亞](../Page/歐亞板塊.md "wikilink")、[菲律賓海板塊相擠撞而高隆](../Page/菲律賓海板塊.md "wikilink")，主稜脈略呈十字形，南北長而東西短，十字之交點即為[玉山主峰](../Page/玉山主峰.md "wikilink")，區域範圍位居台灣中央地帶，地跨[花蓮](../Page/花蓮.md "wikilink")、[高雄](../Page/高雄.md "wikilink")、[南投](../Page/南投.md "wikilink")、[嘉義](../Page/嘉義.md "wikilink")4縣市4鄉10村，地形可概分為[東埔山塊](../Page/東埔山塊.md "wikilink")、[玉山山塊及](../Page/玉山山塊.md "wikilink")[中央山塊三大區](../Page/中央山塊.md "wikilink")，是[濁水溪](../Page/濁水溪.md "wikilink")、[高屏溪及](../Page/高屏溪.md "wikilink")[秀姑巒溪三大河川最重要的上游](../Page/秀姑巒溪.md "wikilink")[集水區](../Page/集水區.md "wikilink")\[1\]。

由於組成玉山山脈組成岩層的地質年代較老\[2\]；而隔鄰的[阿里山山脈的岩層則大多是地質年代較年輕的](../Page/阿里山山脈.md "wikilink")[砂岩](../Page/砂岩.md "wikilink")、[頁岩和](../Page/頁岩.md "wikilink")[砂頁岩互層](../Page/砂頁岩.md "wikilink")\[3\]，從地質年代上連續的特性以及兩條山脈間多條組條大斷層的地質證據推論，阿里山山脈原本位於玉山山脈之上\[4\]，因東方板塊強烈擠壓而斷裂，上方較年輕的岩層往西滑動，形成了阿里山山脈，下方較老的岩層則形成玉山山脈。

## 百岳名峰

據統計，**玉山山脈**3000公尺以上的高山總計有三十座，屬於[台灣百岳的名峰則有十二座](../Page/台灣百岳.md "wikilink")，分別是\[5\]：

  - [玉山主峰](../Page/玉山主峰.md "wikilink")（3952公尺）
  - [玉山東峰](../Page/玉山東峰.md "wikilink")（3869公尺）
  - [玉山北峰](../Page/玉山北峰.md "wikilink")（3858公尺）
  - [玉山南峰](../Page/玉山南峰.md "wikilink")（3844公尺）
  - [東小南山](../Page/東小南山.md "wikilink")（3711公尺）
  - [玉山西峰](../Page/玉山西峰.md "wikilink")（3518公尺）
  - [南玉山](../Page/南玉山.md "wikilink")（3383公尺）
  - [八通關山](../Page/八通關山.md "wikilink")（3335公尺）
  - [郡大山](../Page/郡大山.md "wikilink")（3265公尺）
  - [玉山前峰](../Page/玉山前峰.md "wikilink")（3239公尺）
  - [西巒大山](../Page/西巒大山.md "wikilink")（3081公尺）
  - [鹿山](../Page/鹿山.md "wikilink")（2981公尺）

## 主稜線山峰列表

以下由北而南，列出主稜線上主要山峰：

<table>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p>標高（公尺）</p></th>
<th><p>座標</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/雙龍山.md" title="wikilink">雙龍山</a></p></td>
<td><p>1,094</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西巒大山.md" title="wikilink">西巒大山</a></p></td>
<td><p>3,081</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金子山.md" title="wikilink">金子山</a></p></td>
<td><p>3,005</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/清水山.md" title="wikilink">清水山</a></p></td>
<td><p>3,048</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郡大山北峰.md" title="wikilink">郡大山北峰</a></p></td>
<td><p>3,241</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郡大山.md" title="wikilink">郡大山</a></p></td>
<td><p>3,265</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/對關山.md" title="wikilink">對關山</a></p></td>
<td><p>2,936</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/八通關大山.md" title="wikilink">八通關大山</a></p></td>
<td><p>3,335</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八通關山西峰.md" title="wikilink">八通關山西峰</a></p></td>
<td><p>3,245</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/玉山北峰.md" title="wikilink">玉山北峰</a></p></td>
<td><p>3,858</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/玉山主峰.md" title="wikilink">玉山主峰</a></p></td>
<td><p>3,952</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/玉山南山三叉峰.md" title="wikilink">玉山南山三叉峰</a></p></td>
<td><p>3,807</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小南山_(高雄市).md" title="wikilink">小南山</a></p></td>
<td><p>3,582</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南玉山.md" title="wikilink">南玉山</a></p></td>
<td><p>3,383</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新素左屈山.md" title="wikilink">新素左屈山</a></p></td>
<td><p>2,810</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔界碗山.md" title="wikilink">魔界碗山</a></p></td>
<td><p>2,862</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南面山.md" title="wikilink">南面山</a></p></td>
<td><p>2,828</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/五溪山.md" title="wikilink">五溪山</a></p></td>
<td><p>2,057</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新望嶺.md" title="wikilink">新望嶺</a></p></td>
<td><p>2,481</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/飭山.md" title="wikilink">飭山</a></p></td>
<td><p>2,275</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我丹山.md" title="wikilink">我丹山</a></p></td>
<td><p>2,044</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大竹溪山.md" title="wikilink">大竹溪山</a></p></td>
<td><p>1,664</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/番子寮山.md" title="wikilink">番子寮山</a></p></td>
<td><p>1,208</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/內英山.md" title="wikilink">內英山</a></p></td>
<td><p>1,151</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/荖濃越山.md" title="wikilink">荖濃越山</a></p></td>
<td><p>1,065</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廍亭山.md" title="wikilink">廍亭山</a></p></td>
<td><p>1,044</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南廍亭山.md" title="wikilink">南廍亭山</a></p></td>
<td><p>987</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大寮山.md" title="wikilink">大寮山</a></p></td>
<td><p>590</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/后山.md" title="wikilink">-{后}-山</a></p></td>
<td><p>529</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大貢占山.md" title="wikilink">大貢-{占}-山</a></p></td>
<td><p>823</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狗寮山.md" title="wikilink">狗寮山</a></p></td>
<td><p>558</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大崩逢山.md" title="wikilink">大崩逢山</a></p></td>
<td><p>422</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/茶頂山.md" title="wikilink">茶頂山</a></p></td>
<td><p>457</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/月眉山_(高雄市).md" title="wikilink">月眉山</a></p></td>
<td><p>295</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 參考書籍

  -
[Category:玉山山脈](../Category/玉山山脈.md "wikilink")
[Category:南投縣地理](../Category/南投縣地理.md "wikilink")
[Category:嘉義縣地理](../Category/嘉義縣地理.md "wikilink")
[Category:高雄市地理](../Category/高雄市地理.md "wikilink")

1.
2.
3.
4.
5.