**玉山東峰**標高3,869公尺，日治時期名為**東山**、**新高東山**、**台東新高**，山體岩壁崢嶸，巍峨險要，台灣[登山家](../Page/登山家.md "wikilink")[刑天正稱其為](../Page/刑天正.md "wikilink")**天壘峰**\[1\]，為台灣[玉山山脈中的一座高山](../Page/玉山山脈.md "wikilink")，為[台灣百岳排名第三的高峰](../Page/台灣百岳.md "wikilink")\[2\]，也是[台灣百岳名峰中的](../Page/台灣百岳.md "wikilink")「十峻」之首，位於[玉山主峰東側](../Page/玉山主峰.md "wikilink")，歸屬[玉山國家公園境內](../Page/玉山國家公園.md "wikilink")，為[濁水溪支流](../Page/濁水溪.md "wikilink")[陳有蘭溪](../Page/陳有蘭溪.md "wikilink")，以及[高屏溪主流](../Page/高屏溪.md "wikilink")[荖濃溪發源地](../Page/荖濃溪.md "wikilink")，該處有著名的[金門峒大斷崖](../Page/金門峒大斷崖.md "wikilink")。

## 首登

1899年，日本地質學家[齊藤讓從今日的](../Page/齊藤讓.md "wikilink")[八通關循玉山東稜試圖登上玉山主峰](../Page/八通關.md "wikilink")，但因天氣不佳，齊藤最後登頂玉山東峰，並在山頂留下鐵牌名片。後來德國探險家史坦貝爾也登臨玉山東峰，在山頂發現齊藤的名牌，證實齊藤讓在近代歷史中首登玉山東峰的紀錄\[3\]。史坦貝爾據此稱玉山東峰為**齊藤峰**（**齊藤岳**），但此稱未被廣泛採用。

## 參考文獻

<div class="references-small">

<references />

</div>

[category:台灣百岳](../Page/category:台灣百岳.md "wikilink")
[category:南投縣山峰](../Page/category:南投縣山峰.md "wikilink")
[category:高雄市山峰](../Page/category:高雄市山峰.md "wikilink")

[Category:信義鄉 (臺灣)](../Category/信義鄉_\(臺灣\).md "wikilink")
[Category:桃源區](../Category/桃源區.md "wikilink")
[Category:玉山山脈](../Category/玉山山脈.md "wikilink")
[Category:玉山國家公園](../Category/玉山國家公園.md "wikilink")

1.  台灣百岳全集，連鋒宗編著，上河文化發行，2007年6月初版
2.  台灣第三高峰為[雪山山脈的北稜角](../Page/雪山_\(臺灣\).md "wikilink")，高3,880公尺
3.  MIT台灣誌「台灣五頂峰：玉山山脈」，中國電視公司監製，大麥影像傳播工作室製作，麥覺明導演，2007-03-20