**玉带海雕**（[拉丁文](../Page/拉丁文.md "wikilink")[學名](../Page/學名.md "wikilink")：**''Haliaeetus
leucoryphus**''\[1\]）又稱**黑鷹**和**腰玉**\[2\]，是大型而全身呈棕色的[海雕屬成員之一](../Page/海雕屬.md "wikilink")。牠們在由[裏海和](../Page/裏海.md "wikilink")[黃海中間的地區](../Page/黃海.md "wikilink")、從[哈薩克到](../Page/哈薩克.md "wikilink")[蒙古國](../Page/蒙古國.md "wikilink")、從[喜馬拉雅山脈到](../Page/喜馬拉雅山脈.md "wikilink")[印度北部等的](../Page/印度.md "wikilink")[亞洲中部地區進行繁殖](../Page/中亞.md "wikilink")。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[新疆](../Page/新疆.md "wikilink")、[青海](../Page/青海.md "wikilink")、[内蒙古](../Page/内蒙古.md "wikilink")、[黑龙江](../Page/黑龙江.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[四川](../Page/四川.md "wikilink")、[河北](../Page/河北.md "wikilink")、[山西](../Page/山西.md "wikilink")、[江苏等地](../Page/江苏.md "wikilink")，多见于沼泽、草原以及沙漠或高原。该物种的模式产地在俄罗斯乌拉尔河。\[3\]

## 保护

  - [中国国家重点保护动物等级](../Page/中國國家重點保護野生動物名錄.md "wikilink")：一级

## 參考資料

<div class="references-small">

  - Database entry includes a brief justification of why this species is
    vulnerable, and the criteria used

<!-- end list -->

  - **Wink**, M.; Heidrich, P. & Fentzloff, C. (1996): A mtDNA phylogeny
    of sea eagles (genus *Haliaeetus*) based on nucleotide sequences of
    the cytochrome *b* gene. *Biochemical Systematics and Ecology*
    **24**: 783-791.  [PDF
    fulltext](http://www.uni-heidelberg.de/institute/fak14/ipmb/phazb/pubwink/1996/20_1996.pdf)

</div>

### 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  - ARKive：[圖像及影片](http://www.arkive.org/species/GES/birds/Haliaeetus_leucoryphus/)
  - [BirdLife](http://www.birdlife.org/datazone/species/index.html?action=SpcHTMDetails.asp&sid=3363&m=0)
  - [玉带海雕分佈](http://131.220.109.5/groms/Species_HTMLs/Hleucory_Bild.html)（此網站下載的時間需時較久）

[Category:海鵰屬](../Category/海鵰屬.md "wikilink")
[Category:中国国家一级保护动物](../Category/中国国家一级保护动物.md "wikilink")

1.  註釋：在**[語源學](../Page/語源學.md "wikilink")**上，「*Haliaeetus*」是[新拉丁語](../Page/新拉丁語.md "wikilink")，意為「海雕」。「*leucoryphus*」意為「白色的頭」，來自[古希臘語中的](../Page/古希臘語.md "wikilink")「*leukos*」（白色的）加「*corypha*」（頭部）
2.  [玉帶海雕](http://jky.qzedu.cn/zhsj/pwdw/ydhd.htm)  -
    身披「玉带」的海雕—玉带海雕，作者為李湘涛
3.