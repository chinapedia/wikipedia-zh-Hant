霊御殿|kana=たまうどぅん|romaji=Tamaudun}}
**玉陵**（），又稱**玉御殿**或**靈御殿**，是[琉球國](../Page/琉球國.md "wikilink")[第二尚氏王朝歷代國王的](../Page/第二尚氏王朝.md "wikilink")[陵墓](../Page/陵墓.md "wikilink")。位于現[沖繩縣](../Page/沖繩縣.md "wikilink")[那霸市](../Page/那霸市.md "wikilink")[首里](../Page/首里.md "wikilink")[金城町](../Page/金城町.md "wikilink")。1501年（[明](../Page/明.md "wikilink")[弘治十四年](../Page/弘治.md "wikilink")），第三代國王[尚真王](../Page/尚真王.md "wikilink")（1477年至1526年在位）為改葬其父[尚圓王而修建](../Page/尚圓王.md "wikilink")。目前玉陵已經被列為[世界遺產](../Page/世界遺產.md "wikilink")，是琉球最大的[破風墓](../Page/破風墓.md "wikilink")。

## 概要

整個玉陵可分為中室、東室和西室3部分。中室為放置葬禮後洗骨前遺骨的房間，東室安放洗骨後的國王、王妃的遺骨，西室安葬其它王室成員。建造物外是被石壁隔開的外庭和中庭，中庭是用珊瑚礁碎片铺成。

在[第二次世界大戰中](../Page/第二次世界大戰.md "wikilink")，玉陵的東室、西室被嚴重破壞。現在能看見的大部分建筑都是第二次世界大戰之后重新建設的。為紀念在第二次世界大戰中死亡的沖繩縣立一中的學生而建立的「一中健兒之塔」就在玉陵附近。

2000年，以「[琉球王國的城堡以及相關遺產群](../Page/琉球王國的城堡以及相關遺產群.md "wikilink")」的一部分而登入[世界遺產名錄](../Page/世界遺產.md "wikilink")\[1\]。除了世界遺產之外，玉陵也是指定史跡，玉陵墓室石牆為[重要文化財](../Page/重要文化財.md "wikilink")。石雕獅子和玉陵碑為縣定有形文化財。

## 玉陵中的被葬者

[玉陵碑文記載](../Page/玉陵碑文.md "wikilink")，根據[尚真王在世時的約定](../Page/尚真王.md "wikilink")，有資格埋葬於玉陵中的人物為尚真王、王太后[宇喜也嘉](../Page/宇喜也嘉.md "wikilink")（，尚真王之母）、初代[聞得大君音智殿茂金](../Page/聞得大君.md "wikilink")（尚真王妹）、佐司笠按司真鍋樽（尚真王長女）、中城王子[尚清](../Page/尚清.md "wikilink")（後繼位）、今歸仁王子[尚韶威](../Page/尚韶威.md "wikilink")（尚真王第三子）、越來王子[尚龍德](../Page/尚龍德.md "wikilink")（尚真王第四子）、金武王子[尚享仁](../Page/尚享仁.md "wikilink")（尚真王第六子）、豐見城王子[尚源道](../Page/尚源道.md "wikilink")（尚真王第七子）以及上述人員的子孫。然而尚清王繼位之後違反了這個約定，將廢太子[尚維衡](../Page/尚維衡.md "wikilink")（浦添王子朝滿）的遺骨遷葬於玉陵。此後各王子子孫也不能埋葬於此，而是葬於各家族的墳地中。因此玉陵中的埋葬者多為歷代國王和王妃。

1931年，琉球末代王世子[尚典的夫人祥子](../Page/尚典.md "wikilink")（野嵩按司加那志）是最後一位被埋葬於玉陵的人物。

玉陵事實上的所有被葬者名單如下：

東室被葬者（40人、37個[石廚子](../Page/石廚子.md "wikilink")）：\[2\]

1.  [尚圓王](../Page/尚圓王.md "wikilink") (1415–1476) (第二尚氏王朝初代王)
2.  [尚真王](../Page/尚真王.md "wikilink") (1465–1526)
    (第3代王)、[尚清王](../Page/尚清王.md "wikilink")
    (1497–1555) (第4代王)
3.  [尚元王](../Page/尚元王.md "wikilink") (1528–1572) (第5代王)
4.  梅岳（尚元王妃） (1605年卒) (第3代聞得大君)
5.  [尚永王](../Page/尚永王.md "wikilink") (1559–1588) (第6代王)、阿應理屋按司加那志 (日期不詳)
6.  [向坤功](../Page/向坤功.md "wikilink")（尚永王妃） (1562–1637)
7.  [尚豐王](../Page/尚豐王.md "wikilink") (1590–1640) (第8代王)
8.  [尚梅岩](../Page/尚梅岩.md "wikilink")（尚豐王妃）
    (生卒年不詳)、[尚恭](../Page/尚恭.md "wikilink")（尚豐王長子）
    (1612–1631)
9.  不詳
10. [馬蘭閨](../Page/馬蘭閨.md "wikilink")（尚豐王繼妃） (1588–1661)
11. [尚賢王](../Page/尚賢王.md "wikilink") (1625–1647) (第9代王)
12. [向花囿](../Page/向花囿.md "wikilink")（尚賢王妃） (1630–1666)
13. [尚質王](../Page/尚質王.md "wikilink") (1629–1668) (第10代王)
14. [向栢窓](../Page/向栢窓.md "wikilink")（尚質王妃） (1629–1699)
15. [尚貞王](../Page/尚貞王.md "wikilink") (1645–1709) (第11代王)
16. [章月心](../Page/章月心.md "wikilink")（尚貞王妃） (1645–1703) (第6代聞得大君)
17. [尚純](../Page/尚純.md "wikilink") (1660–1706) (尚貞王長子)
18. [毛義雲](../Page/毛義雲.md "wikilink")（尚純妃） (1664–1723) (第7代聞得大君)
19. [尚益王](../Page/尚益王.md "wikilink") (1678–1712) (第12代王)
20. [毛坤宏](../Page/毛坤宏.md "wikilink")（尚益王妃） (1680–1745) (第8代聞得大君)
21. [尚敬王](../Page/尚敬王.md "wikilink") (1700–1751) (第13代王)
22. [馬仁室](../Page/馬仁室.md "wikilink")（尚敬王妃） (1705–1779) (第9代聞得大君)
23. [尚穆王](../Page/尚穆王.md "wikilink") (1739–1794) (第14代王)
24. [向淑德](../Page/向淑德.md "wikilink")（尚穆王妃） (1740–1779)
25. [尚哲](../Page/尚哲.md "wikilink") (1759–1788) (尚穆王長子)
26. [向德澤](../Page/向德澤.md "wikilink")（尚哲妃） (1762–1795) (第12代聞得大君)
27. [尚溫王](../Page/尚溫王.md "wikilink") (1784–1802) (第15代王)
28. [向仙德](../Page/向仙德.md "wikilink")（尚溫王妃） (1785–1869) (第14代聞得大君)
29. [尚成王](../Page/尚成王.md "wikilink") (1800–1803) (第16代王)
30. [尚灝王](../Page/尚灝王.md "wikilink") (1787–1834) (第17代王)
31. [向順德](../Page/向順德.md "wikilink")（尚灝王妃） (1791–1854)
32. [尚育王](../Page/尚育王.md "wikilink") (1813–1847) (第18代王)
33. [向元貞](../Page/向元貞.md "wikilink")（尚育王妃） (1814–1864)
34. [尚泰王](../Page/尚泰王.md "wikilink") (1843–1901) (第19代王)
35. [章賢室](../Page/章賢室.md "wikilink")（尚泰王妃） (1843–1868)
36. [尚典](../Page/尚典.md "wikilink") (1864–1920) (尚泰王長子)
37. [尚祥子](../Page/尚祥子.md "wikilink")（尚典妃） (生卒年不詳)

<!-- end list -->

  - 中室被葬者：（1人、1個[石廚子](../Page/石廚子.md "wikilink")）：\[3\]

<!-- end list -->

1.  不詳

<!-- end list -->

  - 西室被葬者：（32人、32個[石廚子](../Page/石廚子.md "wikilink")）：\[4\]

<!-- end list -->

1.  不詳
2.  [聞得大君月清](../Page/聞得大君.md "wikilink") (生卒年不詳) (初代聞得大君)
3.  峰間聞得大君梅南 (尚維衡長女) (第2代聞得大君)、[尚維衡](../Page/尚維衡.md "wikilink") (尚真王長子)
    (生卒年不詳)
4.  [尚韶威](../Page/尚韶威.md "wikilink") (生卒年不詳) (尚真王三子，第二尚氏王朝初代北山監守)
5.  [尚一枝](../Page/尚一枝.md "wikilink")（尚元王長女）(首里大君(高級[祝女](../Page/祝女.md "wikilink")))
    (1570年卒)
6.  雪嶺（尚元王妃） (生卒年不詳)
7.  [毛梅嶺](../Page/毛梅嶺.md "wikilink")（尚元王妃） (生卒年不詳)
8.  不詳
9.  不詳
10. [尚月嶺](../Page/尚月嶺.md "wikilink")（尚永王次女）(1584–1653) (第4代聞得大君)
11. 不詳
12. 不詳
13. 不詳
14. [松涼月](../Page/松涼月.md "wikilink")（尚豐王妃） (1597–1634)
15. [毛雪嶺](../Page/毛雪嶺.md "wikilink")（尚恭妃） (1697年卒)
16. 亮直（[尚文妃](../Page/尚文.md "wikilink")） (生卒年不詳)
17. 不詳
18. 不詳
19. 不詳
20. 不詳
21. [尚久](../Page/尚久.md "wikilink") （尚元王三子，尚豐王父親）(1560–1620)
22. [尚膺](../Page/尚膺.md "wikilink")（尚灝王次子） (1813–1815)
23. [尚健](../Page/尚健.md "wikilink")（尚灝王四子） (1818年生)
24. [尚腆](../Page/尚腆.md "wikilink")（尚灝王七子） (1829–1833)
25. [尚濬](../Page/尚濬.md "wikilink")（尚育王長子） (1832–1844)
26. 不詳
27. 不詳
28. 不詳
29. 不詳
30. 不詳
31. 不詳
32. [漢那政子](../Page/漢那政子.md "wikilink")（尚泰王五女）、[東八重子](../Page/東八重子.md "wikilink")（尚泰王六女）
    (生卒年不詳)

<!-- end list -->

  - 史書記載被葬者：

<!-- end list -->

1.  [尚法](../Page/尚法.md "wikilink")（尚哲王長子）
    (1781–1782)（先葬於西森御墓。乾隆五十九年(1794)甲寅四月十三日，移葬於玉陵）
2.  [尚洽](../Page/尚洽.md "wikilink")（尚哲王三子） (1785–1795)（先葬於西森御墓，後移葬於西玉陵）

## 建築

  - 墓室（東室、中室、西室）
  - 前門
  - 後門
  - 玉陵碑
  - 玉陵奉圓館（展示館）

Image:Tamaudun04bs4272.jpg|東室 Image:Tamaudun05bs3200.jpg|中室
Image:Tamaudun06bs4272.jpg|西室
Image:Tamaudun21bs4272.jpg|[玉陵碑](../Page/玉陵碑文.md "wikilink")
Image:Tamaudun03bs3200.jpg|前門 Image:Tamaudun09bs4272.jpg|後門
Image:Tamaudun13n4272.jpg|玉陵奉圓館

## 玉陵以外的尚家墓所

[第二尚氏王朝諸王中](../Page/第二尚氏王朝.md "wikilink")，第2代[尚宣威王和第](../Page/尚宣威王.md "wikilink")7代[尚寧王沒有被葬在玉陵](../Page/尚寧王.md "wikilink")。尚寧王由於統治期內國家被[薩摩藩侵佔](../Page/薩摩藩.md "wikilink")，認為無顏面對祖宗，遺言不入玉陵，被葬在了[浦添極樂陵](../Page/浦添極樂陵.md "wikilink")（今[沖繩縣](../Page/沖繩縣.md "wikilink")[浦添市仲間](../Page/浦添市.md "wikilink")），而[尚宣威王的墳墓](../Page/尚宣威王.md "wikilink")，相傳在今[沖繩市的](../Page/沖繩市.md "wikilink")[八重島](../Page/八重島.md "wikilink")。

此外，[伊是名玉陵位於](../Page/伊是名玉陵.md "wikilink")[伊是名島](../Page/伊是名島.md "wikilink")[伊是名城山麓的旁邊](../Page/伊是名城.md "wikilink")，是[尚稷夫妻](../Page/尚稷.md "wikilink")（[尚圓王父母](../Page/尚圓王.md "wikilink")）以及歷代[祝女的墓所](../Page/祝女.md "wikilink")。2005年，尚家21代當主[尚昌的長女](../Page/尚昌.md "wikilink")[井伊文子](../Page/井伊文子.md "wikilink")、22代當主[尚裕被葬於此](../Page/尚裕.md "wikilink")。[山川之玉陵則是琉球王室另外的墓所](../Page/山川之玉陵.md "wikilink")，葬有[尚泰王第七子](../Page/尚泰王.md "wikilink")[尚時](../Page/尚時.md "wikilink")。

尚家第21代當主[尚昌的墳墓不在玉陵](../Page/尚昌.md "wikilink")，而是在[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[東台區上野](../Page/東台區.md "wikilink")。

## 相關條目

  -
  - [天山陵](../Page/天山陵.md "wikilink")

  - [浦添極樂陵](../Page/浦添極樂陵.md "wikilink")

  - [佐敷極樂陵](../Page/佐敷極樂陵.md "wikilink")

  - [琉球王國的城堡以及相關遺產群](../Page/琉球王國的城堡以及相關遺產群.md "wikilink")

  - [日本世界遺產](../Page/日本世界遺產.md "wikilink")

## 腳註

## 外部連結

  - [UNESCO世界遺產列表：琉球王國的城堡以及相關遺產群](http://whc.unesco.org/en/list/972/)

  - [沖繩的世界遺産玉陵](http://gusuku.que.jp/isan/7_tamaudun/tamaudun.html)

  - [玉陵的照片](https://web.archive.org/web/20071014001750/http://www.hdrjapan.com/kings-tomb-okinawa-japan/)

  -
[T](../Category/日本世界遺產.md "wikilink")
[Category:沖繩縣建築物](../Category/沖繩縣建築物.md "wikilink")
[Category:第二尚氏王朝](../Category/第二尚氏王朝.md "wikilink")
[Category:琉球王室陵寢](../Category/琉球王室陵寢.md "wikilink")
[Category:琉球傳統建築](../Category/琉球傳統建築.md "wikilink")
[Category:那霸市](../Category/那霸市.md "wikilink")

1.

2.  [](http://gusuku.que.jp/isan/7_tamaudun/ichiran.html)

3.
4.