**王世懋**（），[字](../Page/表字.md "wikilink")**敬美**，[號](../Page/號.md "wikilink")**麟州**，又號**損齋**，或曰**牆東生**。[直隸](../Page/南直隸.md "wikilink")[太倉](../Page/太倉.md "wikilink")（今屬[江蘇省](../Page/江蘇省.md "wikilink")）人。[明朝政治人物](../Page/明朝.md "wikilink")。[南京刑部尚书](../Page/南京刑部尚书.md "wikilink")、[史學家](../Page/史學家.md "wikilink")[王世貞之弟](../Page/王世貞.md "wikilink")。

## 生平

順天府鄉試第四十三名，嘉靖三十八年（1559年）己未科三甲進士。历任[南京](../Page/南京.md "wikilink")[礼部](../Page/礼部.md "wikilink")[主事](../Page/主事.md "wikilink")，[陕西](../Page/陕西.md "wikilink")、[福建](../Page/福建.md "wikilink")[提学副使](../Page/提学.md "wikilink")，官至[南京](../Page/南京.md "wikilink")[太常](../Page/太常.md "wikilink")[少卿](../Page/少卿.md "wikilink")。[嘉靖三十八年](../Page/嘉靖.md "wikilink")（1559年）其父王忬為[嚴嵩所冤殺](../Page/嚴嵩.md "wikilink")，兄弟二人相泣号恸，持丧而归。

[隆庆元年](../Page/隆庆_\(明穆宗\).md "wikilink")（1567年）王氏兄弟進京訟父冤，为其父平反。[万历十六年](../Page/万历.md "wikilink")（1588年）病卒。《[明史](../Page/明史.md "wikilink")》附其傳於王世貞傳後。

## 著作

《[四庫全書總目提要](../Page/四庫全書總目提要.md "wikilink")》谓其“能不为党同伐异之言”。著有《王仪部集》、《二酋委谭摘录》、《名山游记》、《奉常集词》、《窥天外乘》、《藝圃擷餘》等。

## 家族

曾祖父[王輅](../Page/王輅.md "wikilink")，贈通議大夫南京兵部右侍郎；祖父[王倬](../Page/王倬.md "wikilink")，通議大夫南京兵部右侍郎；父親[王忬](../Page/王忬.md "wikilink")，曾任總督軍務都察院右都御史兼兵部左侍郎。母郁氏（封安人）\[1\]。

## 參考書目

  - 《孙鑛居业编》
  - 《弇州续稿》
  - 《[书史会要](../Page/书史会要.md "wikilink")》
  - 《[明史](../Page/明史.md "wikilink")·[卷二百八十七](../Page/s:明史/卷287.md "wikilink")》

## 参考文献

[Category:南京禮部主事](../Category/南京禮部主事.md "wikilink")
[Category:明朝陝西按察使司副使](../Category/明朝陝西按察使司副使.md "wikilink")
[Category:明朝福建按察使司副使](../Category/明朝福建按察使司副使.md "wikilink")
[Category:南京太常寺少卿](../Category/南京太常寺少卿.md "wikilink")
[S](../Category/太倉瑯琊王氏.md "wikilink")

1.