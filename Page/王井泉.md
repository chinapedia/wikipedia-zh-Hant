**王井泉**（），[台北](../Page/台北.md "wikilink")[大稻埕人](../Page/大稻埕.md "wikilink")。台灣日治時期文化運動支持者，其具體事跡有以下幾件，第一，「王井泉喜歡幫助人，尤其是文化人，有文化甘草之稱。與台中中央書局[張星建](../Page/張星建.md "wikilink")、[吳天賞等號稱](../Page/吳天賞.md "wikilink")『鐵三角』，他以亭會友，使山水亭成了大稻埕的梁山泊。文人學士於店中高談闊論，評畫賞文。[呂赫若](../Page/呂赫若.md "wikilink")、[林茂生](../Page/林茂生.md "wikilink")、[張文環](../Page/張文環.md "wikilink")、[黃得時等皆為座上客](../Page/黃得時.md "wikilink")，不滿日本至上的日本青年[金關丈夫](../Page/金關丈夫.md "wikilink")、[池田敏雄等也是常客](../Page/池田敏雄_\(民俗研究者\).md "wikilink")」\[1\]；其次，王井泉曾經參與台灣文學社團[啟文社的成立](../Page/啟文社.md "wikilink")，並且支持出版重要的台灣文學刊物《台灣文學》；再者，為了與「[皇民奉公會](../Page/皇民奉公會.md "wikilink")」外圍的「[台灣演劇協會](../Page/台灣演劇協會.md "wikilink")」對抗，王井泉與一些文化人組織「[厚生演劇研究會](../Page/厚生演劇研究會.md "wikilink")」，1943年在台北「永樂座」公演\[2\]。遺憾的是，二次大戰之後，台灣經濟蕭條，山水亭被迫關門，王井泉只好在辜偉甫的[榮星花園服務照顧花草](../Page/榮星花園.md "wikilink")，無法再像當年那樣，扮演文學、文化界的園丁。

## 相關研究

  - 台灣文藝雜誌社在1965年曾在《[台灣文藝](../Page/台灣文藝.md "wikilink")》策畫刊出「王井泉特輯」。
  - 婁子匡/撰，〈王井亭與山水亭〉，《台北文獻》直字678期合刊，1969年12月。
  - 王古勳/撰，〈山水亭：大稻埕的梁山泊〉，《台灣文化》，1986年12月。
  - 莊永明/著，〈「山水亭」的「古井」王井泉〉，《台灣近代名人誌》第3冊，台北：自立晚報社，1987年。
  - 莊永明/著，〈台灣文化界的園丁〉，《台灣紀事》，台北：時報文化，1989年。

## 資料來源

<references />

[Category:大同區人](../Category/大同區人.md "wikilink")
[Jing井](../Category/王姓.md "wikilink")
[Category:台灣日治時期人物](../Category/台灣日治時期人物.md "wikilink")
[Category:台灣文學家](../Category/台灣文學家.md "wikilink")

1.  [王井泉 臺灣記憶 Taiwan
    Memory--國家圖書館](http://memory.ncl.edu.tw/tm_cgi/hypage.cgi?HYPAGE=toolbox_figure_detail.hpg&subject_name=%e8%87%ba%e7%81%a3%e4%ba%ba%e7%89%a9%e8%aa%8c%281895%2d1945%29&subject_url=toolbox_figure.hpg&project_id=twpeop&dtd_id=15&xml_id=0000293084)

2.