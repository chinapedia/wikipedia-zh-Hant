**王仲荦**（），原名**牛**，后改名**元崇**，字**仲荦**，[中国著名](../Page/中国.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")，[魏](../Page/魏.md "wikilink")[晋](../Page/晋.md "wikilink")[南北朝史权威](../Page/南北朝.md "wikilink")，晚年致力于[敦煌学和中国古代物价的研究](../Page/敦煌学.md "wikilink")。他是中国史学界[魏晋封建论的代表人物](../Page/魏晋封建论.md "wikilink")。[浙江](../Page/浙江.md "wikilink")[余姚人](../Page/余姚.md "wikilink")。

## 简历

王仲荦于1913年11月9日。1930年至1936年间，师从[章太炎学习](../Page/章太炎.md "wikilink")[国学](../Page/国学.md "wikilink")。1935年，毕业于上海正风文学院。1938年，与章太炎夫人[汤国梨在上海创立了](../Page/汤国梨.md "wikilink")[太炎文学院](../Page/太炎文学院.md "wikilink")，并亲任教授兼院长室秘书主任。1942年，前往后方[重庆](../Page/重庆.md "wikilink")[中央大学中文系任教](../Page/中央大学.md "wikilink")，历任讲师、副教授。[抗日战争胜利以后](../Page/抗日战争.md "wikilink")，中央大学迁回[南京](../Page/南京.md "wikilink")，王仲荦亦赴南京任教。1947年，赴[青岛的](../Page/青岛.md "wikilink")[国立山东大学任教](../Page/国立山东大学.md "wikilink")。1948年，升为[教授](../Page/教授.md "wikilink")。曾任国立山东大学历史系主任，国立山东大学校学术委员会副主任委员，国立山东大学古籍研究所副所长。

1951年，与山东大学郑宜秀结婚。

## 任职

历任中国史学会理事、中国唐史学会副会长、山东省理学会理事长、国务院古籍整理小组成员、国务院学位委员会历史学科第一届评议组成员、教育部历史学科第一届学位评议组成员等职。

## 著述

王仲荦著有多部重要著作：

  - 《魏晋南北朝史》
  - 《隋唐五代史》
  - 《关于中国奴隶社会的瓦解及封建关系的形成问题》
  - 《北周六典》
  - 《北周地理志》
  - 《西崑酬唱集注》
  - 《敦煌石室地志考释》

其中《北周六典》旁征博引，内容极其丰富广博。《魏晋南北朝史》和《隋唐五代史》两部[断代史均为大家之作](../Page/断代史.md "wikilink")、经典之作，曾于1988年荣获中国国家教委高等教育优秀教材奖。

王仲荦亦发表有大量重要历史研究论文。

王仲荦曾参加[点校](../Page/校對.md "wikilink")[二十四史](../Page/二十四史.md "wikilink")，并负责对《[宋书](../Page/宋书.md "wikilink")》、《[南齐书](../Page/南齐书.md "wikilink")》和《[南史](../Page/南史.md "wikilink")》点校定稿工作。

[Category:中国历史学家](../Category/中国历史学家.md "wikilink")
[Category:国立山东大学教授](../Category/国立山东大学教授.md "wikilink")
[Category:余姚人](../Category/余姚人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Z](../Category/王姓.md "wikilink")