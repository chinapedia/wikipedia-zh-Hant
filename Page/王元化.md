**王元化**（\[1\]），[中国共产党官员](../Page/中国共产党.md "wikilink")、学者、文艺理论家。外祖父[桂美鹏](../Page/桂美鹏.md "wikilink")，母亲[桂月华](../Page/桂月华.md "wikilink")，父亲[王芳荃](../Page/王芳荃.md "wikilink")。

## 生平

王元化1920年11月30日生于[湖北](../Page/湖北.md "wikilink")[武昌的](../Page/武昌.md "wikilink")[基督徒家庭](../Page/基督徒.md "wikilink")，全家为[圣公会](../Page/圣公会.md "wikilink")[武昌圣三一堂的教友](../Page/武昌圣三一堂.md "wikilink")。王元化于1938年初在上海加入[中国共产党](../Page/中国共产党.md "wikilink")，1949年中国共产党取得政权后，任[中国作家协会上海分会党组成员](../Page/中国作家协会.md "wikilink")、上海文艺工作委员会文学处处长、上海新文艺出版社副社长等职。1955年受[胡风事件牵连](../Page/胡风反革命集团案.md "wikilink")，受到迫害。1981年平反后，于1983年7月至1985年5月，任[中共上海市委宣传部部长](../Page/中共上海市委.md "wikilink")。王元化曾任[中共十二大代表](../Page/中共十二大.md "wikilink")，上海市第八届人大常委会委员，上海市第七届政协常委。

王元化是一位在国内外享有盛誉的著名学者、思想家、文艺理论家，在中国古代[文论研究](../Page/文论.md "wikilink")、当代文艺理论研究、中国文学批评史、中国近现代思想学术史研究上开辟新路，做出了开创性的贡献，是中国1949年以来学术界的标志性领军人物。他曾任[国务院学位委员会第一](../Page/国务院学位委员会.md "wikilink")、二届学科评议组成员、[华东师范大学教授](../Page/华东师范大学.md "wikilink")、博士生导师。他自称一生经历了三次大的反思，对于从“[五四](../Page/五四.md "wikilink")”以来的中国思想界、知识界、学术界进行了深刻的反思，其论点独立于权力之外，独立于公众舆论之外，具有自由的思想和独立的人格。由于他成长于1920年代的[清华园](../Page/清华园.md "wikilink")，受到当时大学者[王国维](../Page/王国维.md "wikilink")、[陈寅恪影响甚深](../Page/陈寅恪.md "wikilink")，并专心研读陈寅恪的著作。一生信奉[共产主义的他](../Page/共产主义.md "wikilink")，晚年对于中国现在的[物质主义](../Page/物质主义.md "wikilink")、[享乐主义盛行](../Page/享乐主义.md "wikilink")，[文明的物质化](../Page/文明.md "wikilink")、庸俗化和异化深感忧虑，并引用[马克斯·韦伯的话说过](../Page/马克斯·韦伯.md "wikilink")：“世界不再令人着迷。”

他著述宏富，在其60余年的学术生涯中，出版了《向着真实》《文学沉思录》《[文心雕龙讲疏](../Page/文心雕龙.md "wikilink")》《文心雕龙创作论》《清园夜读》《思辨随笔》《九十年代反思录》《读莎士比亚》《读黑格尔》等，逝世前《王元化集》（十卷）出版。
1998年，获上海市文学艺术杰出贡献奖，同年，论文集《思辨随笔》获国家图书奖，2006年获上海市哲学社会科学学术贡献奖。

## 年表

  - 1939年初，在[皖南](../Page/皖南.md "wikilink")[新四军军部](../Page/新四军.md "wikilink")。
  - 1990年12月间，与妻子[张可同去香港探亲访友](../Page/张可.md "wikilink")（当时其儿子及媳妇居香港九龙[美孚新邨](../Page/美孚新邨.md "wikilink")）。
  - 1991年2月中旬，经由[日本](../Page/日本.md "wikilink")[成田机场转飞](../Page/成田机场.md "wikilink")[美国](../Page/美国.md "wikilink")[夏威夷](../Page/夏威夷.md "wikilink")，出席[夏威夷大学](../Page/夏威夷大学.md "wikilink")[东西方中心召开的](../Page/东西方中心.md "wikilink")“文化与社会：二十世纪中国的历史反思”（Culture
    And Society：Historical Reflections on 20th Century China
    ）会议（会期为2月18日至22日）。

## 参考文献

## 外部链接

  - [王元化-一九九一年回忆录（2009年网址）](https://web.archive.org/web/20081204194106/http://www.chinese-thought.org/bsgwwyhzy/001399.htm)
  - [王元化-一九九一年回忆录（2013年网址）](https://web.archive.org/web/20131219001206/http://www.fangshu.org/bsgwwyhzy/001399.htm)
  - [王元化-一九九一年回忆录（2015年网址）](http://www.aisixiang.com/data/77906.html)
  - [名字的来历](http://www.gmw.cn/content/2009-01/16/content_879188.htm)

[Category:中华人民共和国作家](../Category/中华人民共和国作家.md "wikilink")
[Category:中国文学理论家](../Category/中国文学理论家.md "wikilink")
[Category:上海市政協委員](../Category/上海市政協委員.md "wikilink")
[Category:上海市人大代表](../Category/上海市人大代表.md "wikilink")
[Category:中共上海市委宣傳部部長](../Category/中共上海市委宣傳部部長.md "wikilink")
[Category:中共十二大代表](../Category/中共十二大代表.md "wikilink")
[Category:中国共产党党员
(1938年入党)](../Category/中国共产党党员_\(1938年入党\).md "wikilink")
[Category:華東師範大學教授](../Category/華東師範大學教授.md "wikilink")
[Category:武汉人](../Category/武汉人.md "wikilink")
[Y元化](../Category/王姓.md "wikilink")

1.