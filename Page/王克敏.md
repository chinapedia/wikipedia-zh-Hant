[Wang_Kemin.jpg](https://zh.wikipedia.org/wiki/File:Wang_Kemin.jpg "fig:Wang_Kemin.jpg")》1940年4月3日号中的王克敏\]\]
**王克敏**（）字**叔魯**，[浙江](../Page/浙江.md "wikilink")[杭州人](../Page/杭州.md "wikilink")，[中华民国政治人物](../Page/中华民国.md "wikilink")、銀行家、外交官，[日本](../Page/日本.md "wikilink")[傀儡政權](../Page/傀儡政權.md "wikilink")「[中華民國臨時政府](../Page/中華民國臨時政府_\(1937–1940\).md "wikilink")」的首腦之一。

## 生平

### 清朝、北京政府

光绪二十九年（1903年）举人。[光緒二十七年](../Page/光緒.md "wikilink")（1901年）被清朝官派往[日本](../Page/日本.md "wikilink")。历任浙江留日官費生经理員、留日浙江学生監督、駐日公使館参赞。光緒三十二年（1906年），出任留日学生副監督。翌年归国，先后在[度支部](../Page/度支部.md "wikilink")、[外務部任职](../Page/外務部.md "wikilink")。光緒三十四年（1908年），在[直隸总督](../Page/直隸总督.md "wikilink")[杨士骧手下负责外交事務](../Page/杨士骧.md "wikilink")。[宣統二年](../Page/宣統.md "wikilink")（1910年），任直隸交涉使。\[1\]\[2\]

[民国二年](../Page/民国紀元.md "wikilink")（1913年），王克敏外遊[法国](../Page/法国.md "wikilink")，归国後任中法实业銀行董事。民国六年（1917年）7月，任[中国銀行总裁](../Page/中国銀行.md "wikilink")。同年11月，任[王士珍臨時内閣的財政总長](../Page/王士珍.md "wikilink")，兼任中国銀行总裁、盐務署督办。民国7年（1918年）辞任。同年12月，南北政府举办和平善後会議，王克敏为北京政府代表之一。民国九年（1920年）以后，历任中法实业銀行总裁、[天津保商銀行总理](../Page/北洋保商銀行.md "wikilink")、中国銀行总裁。\[3\]\[4\]

民国十二年（1923年）7月，王克敏就任[高凌霨内閣財政总長](../Page/高凌霨.md "wikilink")。不久，在[奉系领导人](../Page/奉系.md "wikilink")[張作霖的反对下](../Page/張作霖.md "wikilink")，王克敏仅任职一周便被迫辞职。10月，辞任中国銀行总裁。同年11月，[直系](../Page/直系.md "wikilink")[曹锟大总统統为解决](../Page/曹锟.md "wikilink")[金佛郎案](../Page/金佛郎案.md "wikilink")，任命和该案关系密切的王克敏为財政总長。次年7月，时任财政部长的王克敏与国务总理[孙宝琦因](../Page/孙宝琦.md "wikilink")“金佛郎案”发生矛盾，王克敏受到大总统[曹锟袒护](../Page/曹锟.md "wikilink")，导致孙宝琦辞职。民国十三年（1924年）10月，[冯玉祥等人发动](../Page/冯玉祥.md "wikilink")[北京政变](../Page/北京政变.md "wikilink")，拘禁大总统曹锟，王克敏失势。后来，王克敏复归公職，投靠昔日政敵張作霖，出任关税自主委員会委員。\[5\]\[6\]

### 国民政府

[Wang_Kemin2.jpg](https://zh.wikipedia.org/wiki/File:Wang_Kemin2.jpg "fig:Wang_Kemin2.jpg")
民国十七年（1928年）6月，[北伐军击败張作霖](../Page/北伐军.md "wikilink")，發佈通緝令捉拿王克敏，王克敏逃往[大連](../Page/大連市.md "wikilink")。後来，在[張学良庇護下](../Page/張学良.md "wikilink")，出任東北边防軍司令長官公署参議兼財務处处長。经張学良斡旋，民国十八年（1929年）11月通缉令被解除。\[7\]\[8\]

民国二十一年（1932年）后，历任東北政務委員会委員、北平政務委員会財務整理委員会副委員長、行政院駐平政務整理員会財務处主任、華北战区救濟委員会常務委員、[天津特别市市長](../Page/天津市.md "wikilink")、代理行政院駐平政務整理委員会委員長、[冀察政務委員会委員](../Page/冀察政務委員会.md "wikilink")、冀察政務委員会经济委員会主席。\[9\]\[10\]

### 中華民国臨時政府

民国二十六年（1937年），[抗日战争爆发後](../Page/抗日战争.md "wikilink")，同年10月，日本[北支那方面軍特務部長](../Page/北支那方面軍.md "wikilink")[喜多誠一在北平物色了王克敏](../Page/喜多誠一.md "wikilink")、[王揖唐等人](../Page/王揖唐.md "wikilink")，开始组织親日政权。同年12月14日，[中華民国臨時政府在北平成立](../Page/中華民國臨時政府_\(1937–1940\).md "wikilink")，王克敏任行政委員会委員長兼議政委員会常務委員兼行政部总長，成为该政府事实上的最高首脑。翌年，王克敏开始和在[南京建立](../Page/南京市.md "wikilink")[中華民国維新政府的](../Page/中華民国維新政府.md "wikilink")[梁鴻志就双方合并事宜进行交涉](../Page/梁鴻志.md "wikilink")。同年3月28日军统北平站站长[陳恭澍组织刺杀王克敏未遂](../Page/陳恭澍.md "wikilink")，王克敏仅受伤\[11\]。同年9月22日，[中華民国政府联合委員会在北平設立](../Page/中華民国政府联合委員会.md "wikilink")，成为臨時政府与維新政府的联合機关。民国二十八年（1939年）6月，王克敏开始同[汪精卫交涉双方合并事宜](../Page/汪精卫.md "wikilink")，达成妥协。\[12\]\[13\]

民国二十九年（1940年）3月，[南京国民政府成立](../Page/汪精卫政权.md "wikilink")，王克敏的臨時政府、梁鴻志的維新政府均併入。南京国民政府下的原臨時政府統治区设立[華北政務委員会](../Page/華北政務委員会.md "wikilink")，王克敏任委員長兼内政总署督办。不久，王克敏与[兴亚院華北連絡部部長](../Page/兴亚院.md "wikilink")[森冈皋](../Page/森冈皋.md "wikilink")（喜多誠一的後任）、汪精卫对立，同年6月被迫辞任。\[14\]\[15\]

此後，王克敏任南京国民政府中央政治委員会委員。民国三十二年（1943年）6月，再度出任華北政務委員会委員長。此后历任中央政治委員会当然委員、延聘委員，最高国防会議委員，全国经济委員会副委員長，物資調查委員会委員長。民国三十四年（1945年）2月，王克敏因病辞去所有职务。8月，[日本投降](../Page/日本投降.md "wikilink")。12月6日，王克敏因漢奸罪在北平被捕，25日在獄中死去，终年73岁。一般认为他是自殺，但也有说法认为是病死。\[16\]

## 家族

  - [王存善](../Page/王存善.md "wikilink")，王克敏之父。
  - 王遵级，王克敏侄女，1939年跑到[延安参加共產黨](../Page/延安.md "wikilink")，被[康生当做特嫌关押五年](../Page/康生.md "wikilink")。[中华人民共和国建国后任](../Page/中华人民共和国.md "wikilink")[中国科学院地学部办公室主任](../Page/中国科学院.md "wikilink")。

## 注释

## 参考文献

  - <span style="font-size:90%;">王春南「王克敏」</span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>

| （[北京政府](../Page/北洋政府.md "wikilink")）                                                                                                                                                                                                                                                                                                                                           |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| （[国民政府](../Page/国民政府.md "wikilink")）                                                                                                                                                                                                                                                                                                                                           |
| [Flag_of_the_Republic_of_China_(1912-1928).svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Republic_of_China_\(1912-1928\).svg "fig:Flag_of_the_Republic_of_China_(1912-1928).svg") [中華民国臨時政府](../Page/中華民国臨時政府_\(1937–1940\).md "wikilink")                                                                                                                         |
| [Flag_of_the_Republic_of_China-Nanjing_(Peace,_Anti-Communism,_National_Construction).svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Republic_of_China-Nanjing_\(Peace,_Anti-Communism,_National_Construction\).svg "fig:Flag_of_the_Republic_of_China-Nanjing_(Peace,_Anti-Communism,_National_Construction).svg") 南京国民政府（[汪精卫政权](../Page/汪精卫政权.md "wikilink")） |

[分類:暗殺未遂倖存者](../Page/分類:暗殺未遂倖存者.md "wikilink")

[Category:清朝外交官](../Category/清朝外交官.md "wikilink")
[Category:直系人物](../Category/直系人物.md "wikilink")
[Category:奉系人物](../Category/奉系人物.md "wikilink")
[Category:中華民國銀行家](../Category/中華民國銀行家.md "wikilink")
[Category:中華民國財政總長](../Category/中華民國財政總長.md "wikilink")
[Category:汪精衛政權人物](../Category/汪精衛政權人物.md "wikilink")
[Category:中华民国大陆时期自杀政治人物](../Category/中华民国大陆时期自杀政治人物.md "wikilink")
[Category:杭州人](../Category/杭州人.md "wikilink")
[K](../Category/王姓.md "wikilink")
[Category:光緒二十九年癸卯恩科舉人](../Category/光緒二十九年癸卯恩科舉人.md "wikilink")

1.  王（2002）、453頁。

2.  徐主編（2007）、100頁。

3.
4.
5.  王（2002）、453-454頁。

6.
7.  王（2002）、454頁。

8.
9.  王（2002）、454-455頁。

10.
11.

12. 王（2002）、455-456頁。

13.
14. 王（2002）、456-457頁。

15.
16. 徐（2007）、100頁主张自殺說。王（2002）、457頁以及余（2006）、1480頁主张病死說。