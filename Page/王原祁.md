**王原祁**（），[字](../Page/表字.md "wikilink")**茂京**，[号](../Page/号.md "wikilink")**麓台**，一号**石师道人**，[江南](../Page/江南.md "wikilink")[太仓州](../Page/太仓州.md "wikilink")（今[江蘇省](../Page/江蘇省.md "wikilink")[太倉市](../Page/太倉市.md "wikilink")）人。[清代政治人物](../Page/清代.md "wikilink")、[画家](../Page/画家.md "wikilink")，“[四王](../Page/四王.md "wikilink")”之一。

## 生平

[王时敏之孙](../Page/王时敏.md "wikilink")，[康熙九年](../Page/康熙.md "wikilink")（1670年）[进士](../Page/进士.md "wikilink")，授[任縣知縣](../Page/任縣.md "wikilink")。行取[給事中](../Page/給事中.md "wikilink")，不久改[中允](../Page/中允.md "wikilink")。歷官有聲。曾为宫廷作画并鉴定古画，后任书画谱馆总裁。官至[户部左侍郎](../Page/户部左侍郎.md "wikilink")，人称“王[司农](../Page/司农.md "wikilink")”。康熙四十三年（1704年）入直[南書房](../Page/南書房.md "wikilink")，[康熙帝常觀其作畫](../Page/康熙帝.md "wikilink")。次年奉旨與[孫岳頒](../Page/孫岳頒.md "wikilink")、[宋駿業](../Page/宋駿業.md "wikilink")、[吳暻](../Page/吳暻.md "wikilink")、王銓等《佩文齋書畫譜》100卷，康熙五十年（1711年）又主持《萬壽盛典圖》总裁。康熙五十四年（1715年）卒於官。《清史稿》有傳。\[1\]

## 著作

王原祁得祖父和[王鑒之真傳](../Page/王鑑_\(畫家\).md "wikilink")，筆墨功力深厚，喜臨摹[黃公望](../Page/黃公望.md "wikilink")。喜欢用干笔焦墨，层层皴擦，自称笔端有“金刚杵”。设色长于浅绛，其重彩之作，青、绿、朱、赭，相映鲜明，有独到之处，惟有丘壑缺少变化，但功力深厚。與[王時敏](../Page/王時敏.md "wikilink")、王鑑、[王翬](../Page/王翬.md "wikilink")、[吳歷](../Page/吳歷.md "wikilink")、[惲壽平合稱](../Page/惲壽平.md "wikilink")“四王吳惲”或“清六家”。奉詔修撰《佩文齋書畫譜》100卷，《萬壽盛典》120卷。另有《論畫十則》1卷，《罨畫樓集》3卷。

<File:Wang> Yuanqi, after Wang Meng's Mountain Dwelling on a Summer
Day.jpg|《夏日山居图》，现藏[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")
<File:Paysage> de fleuve (1704) par le peintre chinois Wang Yuanqi
(1642-1715).jpg| Étagement de Montagnes verdoyantes par le peintre
chinois Wang Yuanqi (1642-1715).jpg|

工詩文，有《雨窗漫筆》1卷，《麓臺題畫稿》1卷。

## 弟子

弟子很多，有“娄东派”之称，

## 參考文獻

  - 《清史稿》
  - 《國朝耆獻類徵初編》56卷，9

[Category:清朝任縣知縣](../Category/清朝任縣知縣.md "wikilink")
[Category:清朝給事中](../Category/清朝給事中.md "wikilink")
[Category:清朝翰林院侍講](../Category/清朝翰林院侍講.md "wikilink")
[Category:清朝翰林院侍讀](../Category/清朝翰林院侍讀.md "wikilink")
[Category:清朝詹事府少詹事](../Category/清朝詹事府少詹事.md "wikilink")
[Category:清朝詹事府詹事](../Category/清朝詹事府詹事.md "wikilink")
[Category:清朝翰林院掌院學士](../Category/清朝翰林院掌院學士.md "wikilink")
[Category:清朝戶部侍郎](../Category/清朝戶部侍郎.md "wikilink")
[Category:清朝画家](../Category/清朝画家.md "wikilink")
[Category:太倉人](../Category/太倉人.md "wikilink")
[Category:太倉太原王氏](../Category/太倉太原王氏.md "wikilink")

1.  《清史稿·卷五零四》：（王時敏）孫原祁，字茂京，號麓台。幼作山水，張齋壁，時敏見之，訝曰：「吾何時為此耶？」問知，乃大奇曰：「此子業且出我右！」康熙九年成進士，授任縣知縣。行取給事中，尋改中允，直南書房。累擢戶部侍郎，歷官有聲。時海內清晏，聖祖右文，幾馀怡情翰墨，常召入便殿，從容奏對。或於御前染翰，上憑幾觀之，不覺移晷。命鑑定內府名跡，充書畫譜總裁、萬壽盛典總裁，恩禮特異。五十四年，卒於官，年七十四。
    原祁畫為時敏親授，於黃公望淺絳法，獨有心得，晚复好用吳鎮墨法。時敏嘗曰：「元季四家，首推子久，得其神者，惟董宗伯；得其形者，予不敢讓；若形神俱得，吾孫其庶幾乎？」王翬名傾一時，原祁高曠之致突過之。每畫必以宣德紙，重毫筆，頂煙墨，曰：「三者一不備，不足以發古雋渾逸之趣。」或問王翬，曰「太熟」；復問查士標，曰「太生」。蓋以不生不熟自居。中年後，供奉內廷，乞畫者多出代筆，而自署名。每歲晏，與門下賓客畫，人一幅，為製裘之需，好事者緘金以待。弟子最著者黃鼎、唐岱，並別有傳。