**王司馬**（）\[1\]，原名**黄永興**，是[香港著名的](../Page/香港.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")，亦是一名[音響](../Page/音響.md "wikilink")[愛好者](../Page/爱好者.md "wikilink")。他在[澳門出生](../Page/澳門.md "wikilink")\[2\]及就讀小學，後到[廣州唸](../Page/廣州.md "wikilink")[中學](../Page/中学.md "wikilink")，課餘兼習[美術](../Page/美術.md "wikilink")。1957年回到澳門，一邊教書並繼續習畫。[六十年代](../Page/六十年代.md "wikilink")，在[金庸的促成下](../Page/金庸.md "wikilink")，與[漫畫家](../Page/漫畫家.md "wikilink")[王澤聯合執筆](../Page/王家禧.md "wikilink")「老夫子與契爺」漫畫。1961年遷居香港及在一家電影廣告公司工作，並開始從事[漫畫創作](../Page/漫畫.md "wikilink")。1965年進入《[明報](../Page/明報.md "wikilink")》工作，後升任為美術主任\[3\]，[金庸小說的插畫多是出自王司馬手筆](../Page/金庸.md "wikilink")。1967年，王司馬的長子出世。王司馬在1983年9月因患惡性腫瘤\[4\]在香港[聖德肋撒醫院](../Page/聖德肋撒醫院.md "wikilink")（[九龍法國醫院](../Page/九龍.md "wikilink")）病逝\[5\]，享年43歲，10月3日於[世界殯儀館出殯](../Page/世界殯儀館.md "wikilink")\[6\]。

## 主要漫畫著作

  - [牛仔](../Page/牛仔_\(漫畫\).md "wikilink")
  - [狄保士](../Page/狄保士.md "wikilink")
  - [靚女蘇珊](../Page/靚女蘇珊.md "wikilink")

## 資料來源

<references/>

[Category:香港漫畫家](../Category/香港漫畫家.md "wikilink")
[Category:澳門文化界人士](../Category/澳門文化界人士.md "wikilink")

1.  [書影作家香港王司馬](http://yvonnefrank.wordpress.com/category/%E6%9B%B8%E5%BD%B1%E4%BD%9C%E5%AE%B6%E9%A6%99%E6%B8%AF%E7%8E%8B%E5%8F%B8%E9%A6%AC/)

2.
3.

4.  [香港人物: 文化人物](http://szlib.szptt.net.cn/hk97/m3.htm)

5.
6.