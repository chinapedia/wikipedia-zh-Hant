**王嘉廉**（，）是一位华人企业家，[组合国际电脑股份有限公司联合创始人](../Page/组合国际电脑股份有限公司.md "wikilink")。

## 生平

1944年生於[中國](../Page/中國.md "wikilink")[上海](../Page/上海.md "wikilink")，他于1953年远渡[美国](../Page/美国.md "wikilink")，后加入美国国籍，1976年在[纽约和Russell](../Page/纽约.md "wikilink")
Artzt创立了[组合国际电脑股份有限公司](../Page/组合国际电脑股份有限公司.md "wikilink")（，現名「科技」）\[1\]。2018年10月21日死于肺癌\[2\]。

他的著作有《》（1994年，）及《》（1997年，）。

王嘉廉是[國家冰球聯盟的](../Page/國家冰球聯盟.md "wikilink")[紐約島人拥有者](../Page/紐約島人.md "wikilink")。

## 参考资料

## 外部链接

  - [Charles B Wang Project Hope](http://www.nyiprojecthope.com/)
  - [Charles B Wang Community Health Center](http://www.cbwchc.org/)

[Category:移民美国的中国人](../Category/移民美国的中国人.md "wikilink")
[Category:华人企业家](../Category/华人企业家.md "wikilink")
[Category:上海企业家](../Category/上海企业家.md "wikilink")
[Category:百人会会员](../Category/百人会会员.md "wikilink")
[Category:皇后學院校友](../Category/皇后學院校友.md "wikilink")
[Category:球会班主](../Category/球会班主.md "wikilink")
[J](../Category/王姓.md "wikilink")

1.
2.