**王子朝**（），**[姬姓](../Page/姬姓.md "wikilink")**，名**朝**，是東周[春秋時期](../Page/春秋時期.md "wikilink")[周景王的庶出的長子](../Page/周景王.md "wikilink")（非嫡系），為[周景王所寵愛](../Page/周景王.md "wikilink")。

## 生平

前527年六月乙丑日，王太子壽卒。秋八月戊寅日，王穆后崩。景王立王子猛爲太子而寵愛王子朝，於是欲立王子朝。[單穆公單旗](../Page/單穆公.md "wikilink")、[劉文公劉狄等支持王子猛](../Page/劉文公.md "wikilink")，王子朝師大夫[賓起](../Page/賓起.md "wikilink")、[召莊公召奐](../Page/召莊公.md "wikilink")、尹文公等支持王子朝。前520年四月，[周景王大合公卿前去郊外打獵](../Page/周景王.md "wikilink")，欲趁機殺死王子猛的黨羽，立王子朝為太子。然而未即出行就病重了，臨死前囑咐大夫[賓孟擁立王子朝](../Page/賓孟.md "wikilink")。然而國人卻發動政變，殺死賓起，擁立王子猛即位，是為[周悼王](../Page/周悼王.md "wikilink")。

隨後王子朝在六月率百工和[靈王](../Page/周靈王.md "wikilink")、[景王的族人起兵](../Page/周景王.md "wikilink")，與悼王爭奪王位。此舉亦得到了[毛伯得](../Page/毛伯得.md "wikilink")、[尹文公](../Page/尹文公.md "wikilink")、[召莊公的支持](../Page/召莊公.md "wikilink")。王子朝擊敗了悼王。悼王出奔，並向[晉國告急](../Page/晉國.md "wikilink")。十月，晉國派大夫[籍談](../Page/籍談.md "wikilink")、[荀躒率軍保護悼王回到王城](../Page/智文子.md "wikilink")。

十一月十二日，王子朝遂殺悼王，[晉國攻其而立](../Page/晉國.md "wikilink")[周敬王](../Page/周敬王.md "wikilink")。在晉國的幫助下，敬王把王子朝趕到了王京洛邑（今[河南](../Page/河南.md "wikilink")[洛陽西南](../Page/洛陽.md "wikilink")）。隨後晉軍便撤軍回國了。王子朝再度擊敗敬王，佔領了王城，自稱周王。敬王退往王城東面的[狄泉](../Page/狄泉.md "wikilink")，人稱「東王」；王子朝則被稱作「西王」。雙方混戰了三年之久。

前516年，晉國卿士[趙鞅大合諸侯](../Page/趙鞅.md "wikilink")，前往支持周敬王。隨後在趙鞅和[荀躒率晉軍入周](../Page/荀躒.md "wikilink")，佔領王城。王子朝兵敗，[召簡公盈知王子朝無德而逐其](../Page/召簡公.md "wikilink")，迎敬王，結盟與[單武公](../Page/單武公.md "wikilink")、[劉文公](../Page/劉文公.md "wikilink")，王子朝與召氏之族、[毛伯得](../Page/毛伯得.md "wikilink")、[南宮嚚](../Page/南宮嚚.md "wikilink")（[南宮極之子](../Page/南宮極.md "wikilink")）、[尹氏固带着周朝的大量典籍](../Page/尹氏固.md "wikilink")，出奔[楚國避難](../Page/楚國.md "wikilink")。尹氏固之復也，有婦人遇之周郊，尤之，曰：“處則勸人爲禍，行則數日而反，是夫也，其過三歲乎？”

前513年三月己卯日，敬王殺召簡公、尹氏固、及[原伯魯之子于京師](../Page/原伯魯.md "wikilink")。

前505年春，吳國大克楚國時，[周敬王使成周人殺王子朝於楚國國都](../Page/周敬王.md "wikilink")[郢](../Page/郢.md "wikilink")。

## 子女

  -
  -
  -
## 後代研究

据[石井宏明](../Page/石井宏明.md "wikilink")，东周王子乱当中，[王子朝之乱是最后的](../Page/王子朝之乱.md "wikilink")，而且规模最大的一个，其中“东周王朝军事力量受到了致命的损失”（《东周王朝研究》。北京：中央民族出版社，1999，70页）。

## 参考文献

  - 《[春秋左氏传](../Page/春秋左氏传.md "wikilink")》

[Category:东周君主儿子](../Category/东周君主儿子.md "wikilink")
[Category:春秋戰國政治人物](../Category/春秋戰國政治人物.md "wikilink")
[Category:春秋時代遇刺身亡者](../Category/春秋時代遇刺身亡者.md "wikilink")
[Category:王子之乱](../Category/王子之乱.md "wikilink")
[Category:姬姓](../Category/姬姓.md "wikilink")