**王宏恩**（[布農語](../Page/布農語.md "wikilink")：，），[台灣](../Page/台灣.md "wikilink")[布農族](../Page/布農族.md "wikilink")[音樂人](../Page/音樂人.md "wikilink")。[台東縣](../Page/台東縣.md "wikilink")[延平鄉](../Page/延平鄉.md "wikilink")[武陵村人](../Page/武陵村\(台东县\).md "wikilink")，畢業於[彰化縣](../Page/彰化縣.md "wikilink")[大葉大學](../Page/大葉大學.md "wikilink")[視覺傳達設計系](../Page/視覺傳達設計系.md "wikilink")，當完兵之後即在[台北發展](../Page/台北.md "wikilink")。

## 生平與事業

### 學生時期（─2000年）

1999年，王宏恩在大學二年級時，以〈[雨過天晴](../Page/雨過天晴.md "wikilink")〉這首創作，參加[衛生局主辦之反毒歌曲創作比賽](../Page/衛生福利部.md "wikilink")，得到了第四名。2000年，大學三年級時，王宏恩找自己的朋友和堂兄弟，組成樂團**[山上的孩子](../Page/山上的孩子.md "wikilink")**，不久以自己的創作歌曲〈[山谷裡的吶喊](../Page/山谷裡的吶喊.md "wikilink")〉，參加[鄧麗君文教基金會主辦之歌曲創作比賽得到冠軍](../Page/鄧麗君文教基金會.md "wikilink")，可惜沒有在此比賽之演唱組中打倒同為原住民族歌手的冠軍[秀蘭瑪雅](../Page/秀蘭瑪雅.md "wikilink")。

### 風潮唱片（2000年─2002年）

2000年從[大葉大學畢業之後](../Page/大葉大學.md "wikilink")，正式加入[風潮唱片](../Page/風潮唱片.md "wikilink")。同年6月21日發行首張創作專輯《[獵人](../Page/獵人.md "wikilink")》，包辦了此專輯的作詞、作曲和製作人。不久，王宏恩被各地PUB和野台邀請演唱，以及往後的國際海洋音樂節演出。2001年曾以《獵人》專輯入圍第12屆[金曲獎](../Page/金曲獎.md "wikilink")[最佳方言男演唱人獎](../Page/最佳方言男演唱人獎.md "wikilink")；同年8月30日，發行第二張創作專輯《[Biung](../Page/Biung.md "wikilink")》，同樣包辦全專輯之作詞、作曲、製作人和編曲，並找上[丘旺蒼協同製作](../Page/丘旺蒼.md "wikilink")、編曲，2002年便以《Biung》專輯在第13屆**金曲獎**中，獲得**最佳方言男演唱人獎**，此張專輯被[中華音樂人交流協會選中為](../Page/中華音樂人交流協會.md "wikilink")**年度十大優良專輯**之一。

### 喜瑪拉雅（2004年）

2004年，王宏恩在[電視劇](../Page/電視劇.md "wikilink")《[風中緋櫻](../Page/風中緋櫻.md "wikilink")》中飾演**比荷·瓦利斯**；王宏恩曾演唱過一首由[朱約信改編](../Page/朱約信.md "wikilink")《[布農聖詩](../Page/布農聖詩.md "wikilink")》272首的作品〈如果心靈憂傷〉，並以〈如果心情不好〉為歌名收錄於百代唱片的《搖滾主耶穌》專輯；爾後王宏恩加入[喜瑪拉雅](../Page/喜瑪拉雅.md "wikilink")，於同年12月24日，發行第三張創作專輯《[走風的人](../Page/走風的人.md "wikilink")》。

## 音樂作品列表

### 音樂專輯

| 專輯封面 | 專輯資訊                                                           |
| :--- | :------------------------------------------------------------- |
|      | 《[獵人](../Page/獵人_\(音樂專輯\).md "wikilink")》 2000年6月21日 (臺灣)      |
|      | 《[王宏恩 Biung](../Page/王宏恩_Biung.md "wikilink")》 2001年8月30日 (臺灣) |
|      | 《[走風的人](../Page/走風的人.md "wikilink")》 2004年12月24日 (臺灣)          |
|      | 《[戰舞](../Page/戰舞_\(音樂專輯\).md "wikilink")》 2006年8月25日 (臺灣)      |
|      | 《向前衝》 2010年6月29日 (臺灣)                                          |
|      | 《[LOKAH](../Page/LOKAH.md "wikilink")》 2013年12月27日 (臺灣)        |
|      | 《[會走路的樹](../Page/會走路的樹.md "wikilink")》 2017年12月29日 (臺灣)        |
|      |                                                                |

## 得獎入圍紀錄

  - [第12屆金曲獎](../Page/第12屆金曲獎.md "wikilink")：最佳方言男演唱人獎入圍
  - [第13屆金曲獎](../Page/第13屆金曲獎.md "wikilink")：**最佳方言男演唱人獎得主**
  - [第16屆金曲獎](../Page/第16屆金曲獎.md "wikilink")：**最佳作曲人獎得主**〈親愛的寶貝〉
  - [第16屆金曲獎](../Page/第16屆金曲獎.md "wikilink")：最佳國語流行音樂演唱專輯獎入圍
  - [第16屆金曲獎](../Page/第16屆金曲獎.md "wikilink")：最佳專輯製作人獎入圍
  - [第16屆金曲獎](../Page/第16屆金曲獎.md "wikilink")：最佳國語男演唱人獎入圍
  - [第16屆金曲獎](../Page/第16屆金曲獎.md "wikilink")：最佳編曲人獎入圍
  - 1996年廣播電視小金鐘獎：最佳少年節目獎《WAWA 伊拉\~嗚》入圍
  - 1996年廣播電視小金鐘獎：最佳演出人（含主持人及演員）獎入圍
  - 1996年廣播電視小金鐘獎：**最佳原創音樂獎得主**
  - 2002年[中華音樂人交流協會](../Page/中華音樂人交流協會.md "wikilink")：年度十大優良專輯《[王宏恩
    Biung](../Page/王宏恩_Biung.md "wikilink")》
  - 2006年[中華音樂人交流協會](../Page/中華音樂人交流協會.md "wikilink")：年度十大專輯《[戰舞](../Page/戰舞_\(音樂專輯\).md "wikilink")》
  - 2006年[中華音樂人交流協會](../Page/中華音樂人交流協會.md "wikilink")：新一代百大專輯 《走風的人》

## 表演經歷

### 節目主持

  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")《WAWA伊拉～嗚》
  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")《原視音雄榜》
  - [公共電視台](../Page/公共電視台.md "wikilink")《跟書去旅行》
  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")《部落風》
  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")《Lima 幫幫忙》

### 電影

  - 2005年 《[等待飛魚](../Page/等待飛魚.md "wikilink")》

### 電視劇

  - 2003年[風中緋櫻飾](../Page/風中緋櫻.md "wikilink")**比荷·瓦利斯**
  - 2013年[愛的生存之道飾](../Page/愛的生存之道.md "wikilink")**紀安蕾的丈夫**

### 舞台劇

  - 2011年 《米靈岸 (Miling'an) 音樂劇場》
  - 2015年 《男言之隱》故事工廠 第3回作品 奇幻愛情輕喜劇

## 外部連結

  - [福茂唱片介紹王宏恩](https://web.archive.org/web/20120516091125/http://www.linfairrecords.com/artists/profile.php?id=539)

[Category:台灣華語流行音樂歌手](../Category/台灣華語流行音樂歌手.md "wikilink")
[Category:台灣原住民歌手](../Category/台灣原住民歌手.md "wikilink")
[Category:台灣電視主持人](../Category/台灣電視主持人.md "wikilink")
[Category:布農族人](../Category/布農族人.md "wikilink")
[Category:延平鄉人](../Category/延平鄉人.md "wikilink")
[Hong宏](../Category/王姓.md "wikilink")
[Category:大葉大學校友](../Category/大葉大學校友.md "wikilink")
[Category:國立臺東高級中學校友](../Category/國立臺東高級中學校友.md "wikilink")
[Category:台灣音樂製作人](../Category/台灣音樂製作人.md "wikilink")