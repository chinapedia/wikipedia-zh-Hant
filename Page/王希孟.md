**王希孟**（），[中国](../Page/中国.md "wikilink")[宋代画家](../Page/宋代.md "wikilink")，擅作[青绿山水](../Page/青绿山水.md "wikilink")。[北宋](../Page/北宋.md "wikilink")[政和年间入](../Page/政和.md "wikilink")[翰林书画院](../Page/翰林书画院.md "wikilink")，即当时的宫廷画院，得[宋徽宗指导画技](../Page/宋徽宗.md "wikilink")。

## 千里江山图

[青綠山水](../Page/青綠山水.md "wikilink")《千里江山图》是他18岁时作品，也是唯一传世的作品。
画中远近山水，气势开阔，村舍集市、渔船客舟、桥梁水车和林木飞禽，笔墨工致，位置得宜；全卷青绿重设色，表现了山河秀丽。

《千里江山图》后来被当时的宰相[蔡京收藏](../Page/蔡京.md "wikilink")，他在上面的题跋记述了宋徽宗指点王希孟，收他入翰林书画院的经历，王希孟在二十多岁的时候去世，关于他的史料很少。《千里江山图》后在[清](../Page/清朝.md "wikilink")[乾隆年间收入宫中](../Page/乾隆.md "wikilink")，现保存在[北京故宫博物院](../Page/北京故宫博物院.md "wikilink")。该作品已于2019年改编成[手机游戏](../Page/手机游戏.md "wikilink")《[绘真·妙笔千山](../Page/绘真·妙笔千山.md "wikilink")》。

## 参考资料

  - 国宝档案 2004年第55集 千里江山图

[W](../Category/宋朝画家.md "wikilink") [W](../Category/北宋人.md "wikilink")
[X](../Category/王姓.md "wikilink")
[Category:山水畫畫家](../Category/山水畫畫家.md "wikilink")