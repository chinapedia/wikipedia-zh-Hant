**王廷湊**（），一作**王庭凑**，[唐代](../Page/唐代.md "wikilink")[回紇阿布思族](../Page/回紇.md "wikilink")，[軍人](../Page/軍人.md "wikilink")，擁兵自立為[成德節度使](../Page/成德節度使.md "wikilink")。

## 簡介

王廷湊[籍貫隸](../Page/籍貫.md "wikilink")[安東都護府](../Page/安東都護府.md "wikilink")。曾祖父「沒諾干」是阿布思族人，由[成德節度使](../Page/成德節度使.md "wikilink")[王武俊收為](../Page/王武俊.md "wikilink")[養子](../Page/養子.md "wikilink")，改名「王五哥」。以軍功累授左武衛將軍同正，贈[越州](../Page/越州.md "wikilink")[都督](../Page/都督.md "wikilink")。「王五哥」生「王末垣活」，「王末垣活」生「王升」，「王升」生王廷湊。

王廷湊沈鷙少言，喜讀《[鬼谷子](../Page/鬼谷子.md "wikilink")》、[兵書](../Page/兵書.md "wikilink")，在王承宗主宰[成德時](../Page/成德.md "wikilink")，王廷湊為成德軍都知兵馬使。王承宗死後，投靠新任[節度使](../Page/節度使.md "wikilink")[田弘正](../Page/田弘正.md "wikilink")。田弘正御下不嚴，子弟生活奢侈，日費二十萬[錢](../Page/錢.md "wikilink")。[長慶元年](../Page/長慶.md "wikilink")（821年），潛懷異志的王廷湊勾結牙兵，將田弘正殺死，田家屬將佐三百餘口並遇害。

王廷湊殺田弘正後，要求[唐](../Page/唐.md "wikilink")[朝廷授](../Page/朝廷.md "wikilink")[成德節度使](../Page/成德節度使.md "wikilink")，又取[冀州](../Page/冀州.md "wikilink")，殺[刺史](../Page/刺史.md "wikilink")[王進岌](../Page/王進岌.md "wikilink")，史稱其“兇毒好亂，無君不仁”“恃具兇悖，肆毒甘亂”。[朝廷命令](../Page/朝廷.md "wikilink")[韓愈前往](../Page/韓愈.md "wikilink")[鎮州宣諭](../Page/鎮州.md "wikilink")。

唐廷以田弘正之子[田敦禮為](../Page/田敦禮.md "wikilink")[魏博節度使](../Page/魏博節度使.md "wikilink")，全軍三萬人討王廷湊，同时命令横海、昭义、河东、义武诸军协同作战。值大雪繽紛，軍不得進。

長慶二年（822年）正月敦禮自潰於南宮（今[河北](../Page/河北.md "wikilink")[南宮西北](../Page/南宮.md "wikilink")），[魏州兵馬使](../Page/魏州.md "wikilink")[史憲誠發動](../Page/史憲誠.md "wikilink")[兵變](../Page/兵變.md "wikilink")，要求田敦禮行「[河朔舊事](../Page/藩鎮割據.md "wikilink")」，恢復獨立，動搖人心。田敦禮引刃[自刎](../Page/自刎.md "wikilink")，史憲誠自稱[留後](../Page/留後.md "wikilink")。自此「[河北三鎮](../Page/河北三鎮.md "wikilink")」復叛。

文宗太和八年（834年），王廷湊亡，其子[王元逵繼任](../Page/王元逵.md "wikilink")[成德節度使](../Page/成德節度使.md "wikilink")。

## 子孙

  - 长子，831年卒
  - 次子王元逵
  - 女，史孝章妻，约822年出嫁，不迟于829年去世

## 參考書目

  - 《新唐書·王廷湊傳》

[W](../Category/成德軍節度使.md "wikilink") [T](../Category/王姓.md "wikilink")