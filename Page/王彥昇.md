**王彥昇**（），字**光烈**，[宋朝](../Page/宋朝.md "wikilink")[蜀人](../Page/蜀.md "wikilink")，[陈桥兵变的策划者和参与者之一](../Page/陈桥兵变.md "wikilink")。性格残忍，在兵變中殺死意圖抵抗的[韓通](../Page/韓通.md "wikilink")，並追至韓家，將他[滅門](../Page/滅門.md "wikilink")。

## 生平

膂力过人，擅长击剑，号“**王剑儿**”。本是蜀地人，[后唐](../Page/后唐.md "wikilink")[同光中](../Page/同光.md "wikilink")，举家迁至[洛阳](../Page/洛阳.md "wikilink")。

最初，彥昇在宦官骠骑大将军[孟汉琼部下](../Page/孟汉琼.md "wikilink")，孟汉琼以其驍勇，推荐给[唐明宗](../Page/唐明宗.md "wikilink")[李嗣源](../Page/李嗣源.md "wikilink")，补[东班承旨](../Page/东班承旨.md "wikilink")。[后晋](../Page/后晋.md "wikilink")[天福年间](../Page/天福.md "wikilink")，转[内殿直](../Page/内殿直.md "wikilink")。

晋[开运初](../Page/开运.md "wikilink")，[契丹围](../Page/契丹.md "wikilink")[魏博](../Page/魏博.md "wikilink")，晋少帝[石重贵幸](../Page/石重贵.md "wikilink")[澶州](../Page/澶州.md "wikilink")，王彦升与[罗彦环应募](../Page/罗彦环.md "wikilink")，护送少帝突围，以功迁护圣[指挥使](../Page/指挥使.md "wikilink")。

[后周](../Page/后周.md "wikilink")[广顺年间](../Page/广顺.md "wikilink")，跟从[向拱进兵太原](../Page/向拱.md "wikilink")，领兵至[虒亭南面](../Page/虒亭.md "wikilink")，斩敌帅王璋于阵中，以功迁[龙捷右第九军](../Page/龙捷.md "wikilink")[都虞候](../Page/都虞候.md "wikilink")，之后累转[铁骑右第二军](../Page/铁骑.md "wikilink")[都校](../Page/都校.md "wikilink")、领合州[刺史](../Page/刺史.md "wikilink")。又跟从[张永德攻打](../Page/张永德.md "wikilink")[瀛州](../Page/瀛州.md "wikilink")，攻占束城，改[散员都指挥使](../Page/散员都指挥使.md "wikilink")。

### 陈桥兵变

[顯德七年正月](../Page/顯德.md "wikilink")，[禁軍統帥](../Page/禁軍.md "wikilink")[赵匡胤假意領兵抗敵](../Page/赵匡胤.md "wikilink")，至陈桥時發動叛亂，得[石守信帮助领兵入汴京](../Page/石守信.md "wikilink")，是為[陳橋兵變](../Page/陳橋兵變.md "wikilink")。彥昇以所部人马率先进入京城，在路上遇到准备募兵抵抗的「马步军副都指挥使」[韩通](../Page/韩通.md "wikilink")，并一直追击到韩通的府邸，將其一家人全部滅門。

赵匡胤入城之初，曾晓谕诸将，保持軍紀，不要殺人。故听说彥昇殺害韩通一家，非常不悦。由于建国之初，需笼络人心，未加罪，仍升為铁骑[左厢都指挥使](../Page/左厢都指挥使.md "wikilink")，拜恩州[团练使](../Page/团练使.md "wikilink")，后任[京城巡检之职](../Page/京城巡检.md "wikilink")。彥昇一日借巡检之名，半夜至[宰相](../Page/宰相.md "wikilink")[王溥府第](../Page/王溥.md "wikilink")，王溥惊悸，出来迎接，彥昇落座以后，说其夜巡很睏，过来找王溥喝個酒，其实彥昇想向王溥索贿。王溥假装不知道彥昇目的，摆上酒宴款待一番，敷衍了彥昇。第二天，王溥将此事密奏宋太祖，太祖将彥昇外放为唐州刺史。

宋乾德初年，任命彥昇为申州团练使。宋开宝二年，改任防州防御使，年底，又移任原州。由于身处边塞，经常有[西戎骚扰](../Page/西戎.md "wikilink")，一旦擒获[戰俘后](../Page/戰俘.md "wikilink")，并不处刑，而是召集同僚和下属一起饮宴，然后把戰俘拉到宴会上，用手扯断戰俘的耳朵，配著酒吃。此时戰俘满身是血，吓得浑身哆嗦，一动也不敢动，每次都有好幾百個戰俘有如此遭遇。周边的戎狄以此对王彥昇畏惧三分，不敢再骚扰边塞\[1\]。

宋乾德七年，由于彥昇染病，允許回到京城，卒於乾州，时年五十八岁。宋太祖因为其杀韩通之故，终身未授其[节钺](../Page/节钺.md "wikilink")。

## 小說裏的彥昇

在古典小说《飞龙全传》中有关于杀韩通的描写：

其中彥昇的职务是**禁军教头**，这个官职恰好和《[水浒传](../Page/水浒传.md "wikilink")》中[林冲一样](../Page/林冲.md "wikilink")，对照《[宋史](../Page/宋史.md "wikilink")》中王彦升的官职，大体可以推断出林冲的官职。

## 注釋

## 參考書目

  -
[Category:後唐人](../Category/後唐人.md "wikilink")
[Category:后晋人](../Category/后晋人.md "wikilink")
[Category:后周人](../Category/后周人.md "wikilink")
[Category:宋朝军事人物](../Category/宋朝军事人物.md "wikilink")
[Y](../Category/王姓.md "wikilink")
[Category:食人](../Category/食人.md "wikilink")

1.  [王辟之](../Page/王辟之.md "wikilink")《澠水燕談錄》：“西人有犯漢法者，彥昇不加刑，召僚屬飲宴，引所犯，以手捽斷其耳，大嚼，巵酒下之。其人流血被體，股栗不敢動。前後啗者數百人。西人畏之，不敢犯塞。”