**王德輝**（，），籍貫[浙江](../Page/浙江.md "wikilink")[溫州](../Page/溫州.md "wikilink")，生於[上海](../Page/上海.md "wikilink")，[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink")，1948年到香港定居，1960年接掌[華懋集團並一直執掌](../Page/華懋集團.md "wikilink")[董事局主席一職](../Page/董事局主席.md "wikilink")。他在1990年4月10日被[綁架後至今](../Page/綁架.md "wikilink")[下落不明](../Page/失蹤.md "wikilink")，不過[香港高等法院在](../Page/香港高等法院.md "wikilink")1999年宣佈他在法律上已經[死亡](../Page/死亡.md "wikilink")。

## 簡歷

王德輝父親[王廷歆本身經營染料生意](../Page/王廷歆.md "wikilink")，與妻任玉珍育有幾名子女，王德輝是其中之一。王德輝自少與龔如心來往，與龔如心結婚後接掌華懋集團，與妻子把家族生意轉型為一家以房地產為主的公司。

## 綁架案

王德輝先後兩次被綁架。1983年4月王德輝與龔如心，如常駕車離開山頂百祿徑寓所上班，途中被數名槍匪綁架，勒索1億美元，王被放入一個大冰箱運回「參竇」，龔則獲釋返家準備贖款救人。[龔如心依綁匪指示](../Page/龔如心.md "wikilink")，將1100萬美元現金經[洗錢手法匯到](../Page/洗錢.md "wikilink")[台灣](../Page/台灣.md "wikilink")，但被警方查扣，而在香港的綁匪也全部被捕，王德輝因此獲救。

七年之後，退休香港[警署警長](../Page/警署警長.md "wikilink")[鍾維政翻閱以前的案例](../Page/鍾維政.md "wikilink")，發現當年歹徒手法高明，只可惜在幾處重要環節上失手，才會被捕。因此，他認為，如果由他出馬，彌補當年疏忽之處，用同樣手法再一次綁架王德輝，就一定能夠得手，且不會被捕。1990年4月10日，王德輝在[跑馬地馬會打完壁球後回家途中](../Page/跑馬地馬場.md "wikilink")，再度被綁架，綁匪不但不怕警方錄音、監聽，而且還公然在香港的[東方日報](../Page/東方日報_\(香港\).md "wikilink")、[星島日報上刊登廣告](../Page/星島日報.md "wikilink")，指名要龔如心與他們連絡，龔如心兩日後接獲綁匪勒索10億美金，第一期款6000萬美元，先付2800萬美元予綁匪後，後來香港警方在1991至1993年先後拘捕6人，匪徒將2.6億港元贖金透過[地下金融匯往](../Page/地下金融.md "wikilink")[台灣](../Page/台灣.md "wikilink")，被[法務部調查局主動查獲](../Page/法務部調查局.md "wikilink")，部分疑犯則對調查局供稱王在1990年4月13日被香港遠洋漁船載至公海，自此下落不明。

於香港被捕的匪徒分別被判監5年至16年不等，而被台灣查獲的共犯[陳麒元](../Page/陳麒元_\(香港\).md "wikilink")（香港華僑）、[鍾華](../Page/鍾華.md "wikilink")（香港沙田人，中國大陸偸渡到香港）和[鍾志能](../Page/鍾志能.md "wikilink")（鍾維政之子）則在台灣被控「擄人勒贖罪」。根據當時有效之[中華民國刑法特別法](../Page/中華民國刑法.md "wikilink")《[動員戡亂時期懲治盜匪條例](../Page/動員戡亂.md "wikilink")》第二條第一項第九款，擄人勒贖罪處唯一[死刑](../Page/死刑.md "wikilink")，但由於[香港已廢除死刑](../Page/香港.md "wikilink")，在香港的主犯不能被處死，為免出現共犯被處死但主謀只需判監的情況，故有關方面判處兩人[無期徒刑](../Page/無期徒刑.md "wikilink")。不過，主謀退休警長[鍾維政和大圈仔黃壽至今仍然在逃](../Page/鍾維政.md "wikilink")。\[1\]

## 王德輝爭產案

1999年9月22日，[香港高等法院宣佈他在法律上已經死亡](../Page/香港高等法院.md "wikilink")。

已故[華懋集團董事局主席](../Page/華懋集團.md "wikilink")[龔如心和家翁王廷歆就王德輝身家的爭產案](../Page/龔如心.md "wikilink")，[香港終審法院](../Page/香港終審法院.md "wikilink")5位法官在2005年9月，一致裁定推翻下級法院的裁決，龔如心上訴得直。經過8年訴訟，龔如心成為王德輝[港幣](../Page/港幣.md "wikilink")400億遺產的唯一承繼人。

2007年4月13日，龔如心死後，其家屬以王德輝名義在報章刊登龔如心的訃聞。

## 家屬

  - [父親](../Page/父親.md "wikilink") [王廷歆](../Page/王廷歆.md "wikilink")，已故。
  - 弟妹：
      - 王德嫻
        [化學學士](../Page/化學.md "wikilink")、[電腦](../Page/電腦.md "wikilink")[碩士](../Page/碩士.md "wikilink")，現為電腦顧問。
      - 王德華
        [香港工專畢業](../Page/香港理工大學.md "wikilink")，現為香港[註冊會計師](../Page/註冊會計師.md "wikilink")。
      - 王德淼
        中學會考十優，[史丹福大學電機工程系](../Page/史丹福大學.md "wikilink")，獲[獎學金至博士班](../Page/獎學金.md "wikilink")，曾為[三藩市當互聯網保安顧問](../Page/三藩市.md "wikilink")。
      - 王德栽
        [藥劑學博士](../Page/藥劑學.md "wikilink")，移居美國任[藥劑師](../Page/藥劑師.md "wikilink")。
  - 無子女

## 参考文献

{{-}}

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港億萬富豪](../Category/香港億萬富豪.md "wikilink")
[Category:香港下落不明者](../Category/香港下落不明者.md "wikilink")
[Category:经缺席宣判死亡的人士](../Category/经缺席宣判死亡的人士.md "wikilink")
[Category:溫州人](../Category/溫州人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:華懋集團](../Category/華懋集團.md "wikilink")
[Category:被绑架的香港人](../Category/被绑架的香港人.md "wikilink")
[D德](../Category/王姓.md "wikilink")

1.  [死亡恐嚇未除　歹徒轉向其助理下手
    龔如心保鑣由50增至100](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20040329&sec_id=4104&subsec=11867&art_id=3947048)