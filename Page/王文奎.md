**王文奎**（），字**清遠**，隸漢軍鑲白旗，[浙江省](../Page/浙江省.md "wikilink")[紹興府](../Page/紹興府.md "wikilink")[會稽縣曹娥村](../Page/會稽縣.md "wikilink")（今[上虞](../Page/上虞.md "wikilink")）人。

## 簡介

為明末[清初的官員](../Page/清朝.md "wikilink")，原姓[沈](../Page/沈氏.md "wikilink")，少時寄養外家，冒姓[王氏](../Page/王氏.md "wikilink")。[明熹宗](../Page/明熹宗.md "wikilink")[天啟七年](../Page/天啟.md "wikilink")（1627年），北遊遼東。[崇禎二年](../Page/崇禎.md "wikilink")（1629年），[清兵破](../Page/清.md "wikilink")[遵化](../Page/遵化.md "wikilink")，被俘投降，翌年攜至[瀋陽](../Page/瀋陽.md "wikilink")，命值文館，為清朝出謀劃策，因受[皇太極器重](../Page/皇太極.md "wikilink")，召見詢問國政，曾疏請定「衣冠之製及不拘族類，不限貴賤，不分新舊，選拔人才諸事」。明崇禎九年、清[崇德元年](../Page/崇德.md "wikilink")（1636年），授弘文院學士，被皇太極視為親信。明崇禎十七年、清[順治元年](../Page/順治.md "wikilink")（1644年），命為右副都御史，巡撫[保定](../Page/保定.md "wikilink")，鎮壓反清起義軍。次年，加兵部右侍郎，總督淮揚漕運。累擢[漕運總督](../Page/漕運總督.md "wikilink")。後坐督運愆遲，左遷陝西督糧道，卒於官。

## 參考文獻

  - 《[清史稿](../Page/清史稿.md "wikilink")》，卷二百三十九，列傳二十六
  - 《明清臺灣檔案彙編》
  - 《會稽縣志－沈文奎傳》 (康熙)

[W王](../Category/清朝陝西三邊總督.md "wikilink")
[Category:清朝漕運總督](../Category/清朝漕運總督.md "wikilink")
[Category:清朝保定巡撫](../Category/清朝保定巡撫.md "wikilink")
[W王](../Category/上虞人.md "wikilink")
[W文](../Category/王姓.md "wikilink")