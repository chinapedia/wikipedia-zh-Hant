**王晓红**（），[中國](../Page/中國.md "wikilink")[江苏](../Page/江苏.md "wikilink")[常州人](../Page/常州.md "wikilink")，[游泳](../Page/游泳.md "wikilink")[运动员](../Page/运动员.md "wikilink")，中国泳坛90年代“五朵金花”辉煌时代成员之一。她是一位大器晚成的运动员。

## 生平

在[南韓](../Page/南韓.md "wikilink")[汉城（現稱首爾）奥运会](../Page/1988年夏季奥林匹克运动会.md "wikilink")，已经20岁的王晓红一鸣惊人，夺得女子200米[蝶泳第七名](../Page/蝶泳.md "wikilink")，100米蝶泳第八名。在[巴塞罗那奥运会](../Page/1992年夏季奥林匹克运动会.md "wikilink")，王晓红发挥出色，获女子200米蝶泳第二名、100米蝶泳第四名。

王晓红退役后留学[美国](../Page/美国.md "wikilink")，获得[计算机硕士学位](../Page/计算机.md "wikilink")，一度就职于[朗讯](../Page/朗讯.md "wikilink")\[1\]，现任美國加州凱豊游泳俱樂部教练\[2\]\[3\]，育有一子一女。

## 参考资料

<references />

[Xiaohong](../Page/category:王姓.md "wikilink")

[Category:江苏籍游泳运动员](../Category/江苏籍游泳运动员.md "wikilink")
[Category:中国奥运游泳运动员](../Category/中国奥运游泳运动员.md "wikilink")
[Category:游泳教练](../Category/游泳教练.md "wikilink")
[Category:常州籍运动员](../Category/常州籍运动员.md "wikilink")
[Category:中国奥林匹克运动会银牌得主](../Category/中国奥林匹克运动会银牌得主.md "wikilink")
[Category:1992年夏季奧林匹克運動會獎牌得主](../Category/1992年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奧林匹克運動會游泳獎牌得主](../Category/奧林匹克運動會游泳獎牌得主.md "wikilink")
[Category:1988年夏季奧林匹克運動會游泳運動員](../Category/1988年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:1992年夏季奧林匹克運動會游泳運動員](../Category/1992年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:亞洲運動會游泳獎牌得主](../Category/亞洲運動會游泳獎牌得主.md "wikilink")
[Category:世界游泳錦標賽游泳賽事獎牌得主](../Category/世界游泳錦標賽游泳賽事獎牌得主.md "wikilink")
[Category:1990年亞洲運動會金牌得主](../Category/1990年亞洲運動會金牌得主.md "wikilink")
[Category:1990年亞洲運動會銀牌得主](../Category/1990年亞洲運動會銀牌得主.md "wikilink")

1.  [林莉庄泳钱红杨文意王晓虹--五朵金花今安在(四)](http://sports.163.com/tm/030331/030331_316561\(4\).html)

2.  [Calphin Staff](http://www.calphin.com/about_staffs.php)
3.  [Founders](http://www.calphin.com/about_founders.php)