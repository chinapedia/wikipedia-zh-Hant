**王晶**（），中國女演員，與香港導演[王晶同名](../Page/王晶_\(導演\).md "wikilink")，故有“**小王晶**”之名。出生於[新疆](../Page/新疆.md "wikilink")，[中央戏剧学院表演系本科](../Page/中央戏剧学院.md "wikilink")00届。

## 作品

### 电影作品

  - 2007年《[无敌粉丝狂](../Page/无敌粉丝狂.md "wikilink")》饰：小莉
    导演：[王晶](../Page/王晶_\(導演\).md "wikilink") (香港)
  - 2008年《[生死搜救](../Page/生死搜救.md "wikilink")》饰:彩云
    导演:[阿强](../Page/阿强.md "wikilink")(香港)
  - 2008年《[金钱帝国](../Page/金钱帝国.md "wikilink")》饰：萍萍 导演：王晶(香港)
  - 2008年《[离歌](../Page/离歌.md "wikilink")》饰：林果果
    编剧：[饶雪漫](../Page/饶雪漫.md "wikilink")

### 電視劇

  - 1999年《[共和国往事](../Page/共和国往事.md "wikilink")》饰:刘心
    导演：[尤小刚](../Page/尤小刚.md "wikilink")
  - 2001年《[背叛](../Page/背叛_\(2001年電視劇\).md "wikilink")》饰:叶子
    导演：[史晨风](../Page/史晨风.md "wikilink")
  - 2001年《[烈火英雄](../Page/烈火英雄.md "wikilink")》饰:女二号
    导演：[宁海强](../Page/宁海强.md "wikilink")
  - 2002年《[我和连长](../Page/我和连长.md "wikilink")》饰:班长小可
    导演：[舒崇服](../Page/舒崇服.md "wikilink")
  - 2001年《[奇人安世敏](../Page/奇人安世敏.md "wikilink")》饰:玉姑
    导演：[张乙](../Page/张乙.md "wikilink")
  - 2003年《[清明酒家](../Page/清明酒家.md "wikilink")》飾:如花
    导演：[洪丹强](../Page/洪丹强.md "wikilink")
  - 2003年《[法理人生](../Page/法理人生.md "wikilink")》饰:王芳 导演：史晨风
  - 2004年《[壮志凌云包青天](../Page/壮志凌云包青天.md "wikilink")》饰:柳燕儿（反一号）
    导演：[吴耀权](../Page/吴耀权.md "wikilink")
  - 2005年《[欢天喜地七仙女](../Page/欢天喜地七仙女.md "wikilink")》饰:三公主黄儿
    导演：[郑基成](../Page/郑基成.md "wikilink")
  - 2005年《[京华烟云](../Page/京华烟云_\(2005年电视剧\).md "wikilink")》饰:格格黛芬
    导演：[张子恩](../Page/张子恩.md "wikilink")
  - 2005年《[浴火凤凰](../Page/浴火鳳凰_\(2006年電視劇\).md "wikilink")》 饰：杜晓彤
    导演：[王晶](../Page/王晶_\(導演\).md "wikilink") (香港)
  - 2006年《[楚留香传奇](../Page/楚留香傳奇_\(電視劇\).md "wikilink")》饰：宫南燕
    导演：[刘逢声](../Page/刘逢声.md "wikilink")
  - 2007年《[A计划](../Page/A計劃_\(電視劇\).md "wikilink")》饰：云飘飘/独孤醒来 导演:王晶 (香港)
  - 2007年《[仁者黄飞鸿](../Page/仁者黄飞鸿.md "wikilink")》 饰：武田静香 导演：王晶(香港)
  - 2007年《[十大奇冤](../Page/十大奇冤.md "wikilink")》饰：辣椒（女一号） 导演：王晶(香港)
  - 2008年《[宠物医院](../Page/宠物医院.md "wikilink")》 饰：琪琪
    导演：[章乐斌](../Page/章乐斌.md "wikilink")
  - 2009年《[熊貓人](../Page/熊貓人.md "wikilink")》 饰：路詩晴
    导演：[周杰倫](../Page/周杰倫.md "wikilink")
  - 待播中《[龙凤店传奇](../Page/龙凤店传奇.md "wikilink")》

### 話劇

  - 《我在天堂等你》饰:藏族女奴尼玛 导演：王群（北京战友话剧团）
  - 《阳台》 饰：李丽 导演：陈佩斯
  - 《托儿》 饰：小凤 导演：陈佩斯
  - 《儿子》饰:邓小平女儿毛毛 导演：查丽芳
  - 《谁的木箱》饰:826286 导演：银园春
  - 《泥巴人》女大学生“小惠”
  - 《巴山情》小秀小云母女俩

### 歌曲

  - 《英雄一怒为红颜》楚留香传奇 合作：小壮
  - EP《现在的我》

[J晶](../Category/王姓.md "wikilink")
[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")