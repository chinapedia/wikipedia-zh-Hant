**王智慧**（，，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")。出道初期曾以**閔智慧**（）作為藝名。

## 演出作品

### 電視劇

  - 2003年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[1%的可能性](../Page/1%的可能性.md "wikilink")》飾
    韓珠熙
  - 2004年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[不良少女](../Page/不良少女.md "wikilink")》
  - 2004年：[KBS](../Page/韓國放送公社.md "wikilink")《[北京，我的愛](../Page/北京，我的愛.md "wikilink")》飾
    王思朗
  - 2005年：[KBS2](../Page/KBS2.md "wikilink")《[在芙蓉雞龍山](../Page/在芙蓉雞龍山.md "wikilink")》飾
    黃芙蓉\[1\]
  - 2006年：中國《[生死絕戀](../Page/生死絕戀.md "wikilink")》飾 雪怡
  - 2007年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[遇上完美鄰居的方法](../Page/遇上完美鄰居的方法.md "wikilink")》飾
    高慧美
  - 2009年：MBC《[朋友，我們的傳說](../Page/朋友，我們的傳說.md "wikilink")》飾 崔貞淑
  - 2010年：MBC《[個人取向](../Page/個人取向.md "wikilink")》飾 金仁希
  - 2010年：KBS《[總統](../Page/總統_\(韓國電視劇\).md "wikilink")》飾 張仁英
  - 2011年：SBS《[守護老闆](../Page/守護老闆.md "wikilink")》飾 徐娜允
  - 2011年：[Channel
    A](../Page/Channel_A.md "wikilink")《[蔬菜店的小夥子](../Page/蔬菜店的小夥子.md "wikilink")》飾
    陳真心／慕佳恩
  - 2012年：KBS《[加油，金先生！](../Page/加油，金先生！.md "wikilink")》飾 李友卿
  - 2013年：SBS《[奇怪的保姆](../Page/奇怪的保姆.md "wikilink")》飾 尹松花
  - 2014年：MBC《[Hotel King](../Page/Hotel_King.md "wikilink")》飾 宋彩京
  - 2014年：SBS《[美女的诞生](../Page/美女的诞生.md "wikilink")》飾 喬彩妍
  - 2016年：SBS《[對，就是那樣](../Page/對，就是那樣.md "wikilink")》飾 洪幼梨
  - 2016年：SBS《[一滴一滴的愛](../Page/一滴一滴的愛.md "wikilink")》飾 殷點點
  - 2018年：[OCN](../Page/OCN.md "wikilink")《[Player](../Page/Player_\(韓國電視劇\).md "wikilink")》飾
    柳賢子（議員老婆）

### 電影

  - 2006年：《[九尾狐家族](../Page/九尾狐家族.md "wikilink")》
  - 2007年：《[美麗星期天](../Page/美麗星期天.md "wikilink")》
  - 2010年：《[食客：泡菜戰爭](../Page/食客：泡菜戰爭.md "wikilink")》

### MV

  - 2002年：Yarn《天黑之前》
  - 2002年：Click-B《To Be Continued》
  - 2002年：[JTL](../Page/JTL.md "wikilink")《幸福過的記憶》
  - 2006年：金昌鉉《Eternity》
  - 2006年：[李昇基](../Page/李昇基.md "wikilink")《善意的謊言》

### 綜藝

  - [SBS](../Page/SBS.md "wikilink") [Running
    Man](../Page/Running_Man.md "wikilink") 20141102 Ep219
  - [SBS](../Page/SBS.md "wikilink")
    [叢林的法則-薩摩亞篇](../Page/叢林的法則.md "wikilink")

## 獲獎

  - 2003年：第一屆安德烈金最佳明星獎－新人明星獎
  - 2003年：MTV中國超級明星形象獎－新人獎
  - 2011年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－新星賞（守護老闆）

## 備註

## 外部連結

  - [Keyeast官網個人網頁](http://www.keyeast.co.kr/index/artist/profile.asp?art_idx=21)
  - [王智慧的CYWorld](http://cy.cyworld.com/home/56862346)
  - [王智慧的新浪微博](https://www.weibo.com/u/5306695989?refer_flag=1005055013_&is_all=1)

[W](../Category/韓國電視演員.md "wikilink")
[W](../Category/建國大學校友.md "wikilink")
[W](../Category/慶尚南道出身人物.md "wikilink")
[W](../Category/王姓.md "wikilink")

1.