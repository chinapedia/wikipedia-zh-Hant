**王永年**（），文字工作者，筆名王仲年、雷怡、楊綺，生于[浙江](../Page/浙江.md "wikilink")[舟山](../Page/舟山.md "wikilink")[定海](../Page/定海.md "wikilink")（現[宁波市](../Page/宁波市.md "wikilink")[定海县](../Page/定海县.md "wikilink")），1946年從[上海](../Page/上海.md "wikilink")[聖約翰大學英國語文學系畢業](../Page/聖約翰大學.md "wikilink")，曾任中學[俄語教師](../Page/俄語.md "wikilink")、外國文學編輯，1959年起擔任[中華人民共和國](../Page/中華人民共和國.md "wikilink")[新華通訊社](../Page/新華通訊社.md "wikilink")[西班牙語譯審](../Page/西班牙語.md "wikilink")，曾派駐墨西哥等[西語系國家](../Page/西語.md "wikilink")，在新華通訊社對外部、新華通訊社[拉丁美洲總分社歷任高級職稱](../Page/拉丁美洲.md "wikilink")，1990年代中以正高級職稱離休。

政治面貌是[中國民主同盟](../Page/中國民主同盟.md "wikilink")（[民主黨派人士](../Page/民主黨派.md "wikilink")）。

他翻譯新聞稿以精練、準確著名，到1980年代中期稿件就已超過500萬字，不論將[中文翻譯成](../Page/中文.md "wikilink")[西班牙文或](../Page/西班牙文.md "wikilink")[西班牙文翻做](../Page/西班牙文.md "wikilink")[中文都極為精到](../Page/中文.md "wikilink")，廣受歡迎，1979年，由他翻譯成[西班牙文的](../Page/西班牙文.md "wikilink")2篇[中國新聞稿在](../Page/中國.md "wikilink")[墨西哥得獎](../Page/墨西哥.md "wikilink")。

王永年精通[英文](../Page/英文.md "wikilink")、[俄文](../Page/俄文.md "wikilink")、[西班牙文](../Page/西班牙文.md "wikilink")、[義大利文等多種外語](../Page/義大利文.md "wikilink")，工作餘暇翻譯多種世界文學名著，以**王仲年**筆名翻譯的系列[歐·亨利小說](../Page/歐·亨利.md "wikilink")，出版多種版本，暢銷多年，歷久不衰，備受英美文學研究者的好評，他又從[義大利原文翻譯](../Page/義大利.md "wikilink")[義大利文學巨著](../Page/義大利.md "wikilink")《[十日談](../Page/十日談.md "wikilink")》和《約婚夫婦》，是[中國從原文翻譯](../Page/中國.md "wikilink")《十日談》的第1人。

1993年，他編選、翻譯[阿根廷作家](../Page/阿根廷.md "wikilink")、詩人[博爾赫斯的詩文選集](../Page/博爾赫斯.md "wikilink")《巴比倫彩票》，是公認最傳神、最精準的中譯本，也因此應邀擔任《博爾赫斯全集》（[林一安主編](../Page/林一安.md "wikilink")）的主要譯者，譯出博爾赫斯幾乎全部的小說（僅4篇由陳泉翻譯）和大量博爾赫斯的詩、散文、評論。

他翻譯的博爾赫斯作品中的許多篇，已出版數個以上版本，並收入[陳眾議主編的另](../Page/陳眾議.md "wikilink")1套《博爾赫斯文集》內。

他也翻譯不少[智利詩人](../Page/智利.md "wikilink")、作家巴勃羅·[聶魯達](../Page/聶魯達.md "wikilink")（Pablo
Neruda）的詩和散文。

他翻譯的[英語文學作品還有](../Page/英語.md "wikilink")[諾貝爾文學獎](../Page/諾貝爾文學獎.md "wikilink")[美國得主約翰](../Page/美國.md "wikilink")·[史坦貝克](../Page/史坦貝克.md "wikilink")（John
Steinbeck）的《伊甸之東》（*East of
Eden*）、[辛克萊·劉易斯](../Page/辛克萊·劉易斯.md "wikilink")（Sinclair
Lewis）的《巴比特》（*Babbitt*）、[諾貝爾文學獎](../Page/諾貝爾文學獎.md "wikilink")[南非得主](../Page/南非.md "wikilink")[庫切](../Page/庫切.md "wikilink")（J.
M. Coetzee）的《彼得堡的大師》（*The Master of
Petersburg*），19世紀[英國路易士](../Page/英國.md "wikilink")·卡羅爾（Lewis
Carroll ）的《愛麗絲漫遊奇境（鏡中奇遇）》（*Alice in Wonderland, Through the Looking
Glass*），20世紀美國[傑克·凱魯亞克](../Page/傑克·凱魯亞克.md "wikilink")（Jack
Kerouac）的《[在路上](../Page/在路上.md "wikilink")》（*On the Road*）等。

西班牙語文學譯作還有[諾貝爾文學獎女性](../Page/諾貝爾文學獎.md "wikilink")[智利得主](../Page/智利.md "wikilink")[加夫列拉·米斯特拉爾](../Page/加夫列拉·米斯特拉爾.md "wikilink")（Gabriela
Mistral）的詩文集、諾貝爾文學獎[哥倫比亞得主](../Page/哥倫比亞.md "wikilink")[加西亚·马尔克斯的](../Page/加西亚·马尔克斯.md "wikilink")《迷宮中的將軍》（*El
general en su laberinto*）等。

意大利語文學譯作還有16世紀托尔夸托·塔索（Torquato Tasso）的《耶路撒冷的解放》（''La Gerusalemme
liberata ''）等。

2005年，[北京](../Page/北京.md "wikilink")[人民文學出版社推出他在](../Page/人民文學出版社.md "wikilink")2002年譯完的《歐·亨利小說全集》（*The
Complete Works of
O.Henry*），共180萬字，譯介全部的歐·亨利作品給整個[漢語世界](../Page/漢語.md "wikilink")。

2008年，[上海譯文出版社出台了他翻譯的](../Page/上海譯文出版社.md "wikilink")《博爾赫斯談話錄》，內收博爾赫斯接受[英語](../Page/英語.md "wikilink")[傳媒訪問的多篇談話紀錄](../Page/傳媒.md "wikilink")。

他還懂得[日語](../Page/日語.md "wikilink")、[德語](../Page/德語.md "wikilink")、[葡語](../Page/葡語.md "wikilink")、[法語等語文](../Page/法語.md "wikilink")，只有日語和英語是在校學的，西語等多種外語是自學。

上大學時，曾和[張愛玲同班讀過書](../Page/張愛玲.md "wikilink")。

他擔任主要譯者的《博爾赫斯全集》由[臺灣商務印書館以](../Page/臺灣商務印書館.md "wikilink")《波赫士全集》名稱在[台灣推出](../Page/台灣.md "wikilink")1套4卷的[繁體漢字版](../Page/繁體漢字.md "wikilink")。

《十日談》和《歐·亨利小說選》20世紀末在台灣出了繁體漢字版。

[category:中国翻译家](../Page/category:中国翻译家.md "wikilink")
[category:定海人](../Page/category:定海人.md "wikilink")
[Y](../Page/category:王姓.md "wikilink")