**王海玲**（），籍貫[中國](../Page/中國.md "wikilink")[浙江](../Page/浙江.md "wikilink")；[台灣](../Page/台灣.md "wikilink")1970年代末期[校園民歌知名](../Page/校園民歌.md "wikilink")[歌手](../Page/歌手.md "wikilink")。

出生於台灣[台北的王海玲](../Page/台北.md "wikilink")，於1979年冬季以[北一女學生的資格參加](../Page/北一女.md "wikilink")[金韻獎](../Page/金韻獎.md "wikilink")，以一曲〈My
Heart Belongs to
Me〉打敗了[施孝榮](../Page/施孝榮.md "wikilink")，獲得冠軍，隨後同年灌錄了《[忘了我是誰](../Page/忘了我是誰.md "wikilink")》收錄於1980年夏季推出的《金韻獎（六）》，她也憑著此曲走紅台灣校園民歌界。出道僅高中二年級，也因此她被稱為台灣「最年輕的校園民歌手」。後來畢業自[國立台灣大學的王海玲](../Page/國立台灣大學.md "wikilink")，後來大部分時間均轉為幕後工作，今仍從事唱片製作及教唱等工作，她的學生包括[張韶涵](../Page/張韶涵.md "wikilink")、[蕭煌奇等](../Page/蕭煌奇.md "wikilink")。其妹[王海娟為雜誌社出版界知名人士](../Page/王海娟.md "wikilink")。

## 專輯或單曲

  - 《忘了我是誰》（《金韻獎6》，1982年）
  - 《我心似清泉》
  - 《不要說再見》
  - 《偈》
  - 《別忘了我是誰》，2002年

## 參考資料

  - [朱陵真，《黃金小孩--王海玲》，1981年](http://www.tonight.tv/modules/news/article.php?storyid=283)
  - [東森新聞網，《忘了我是誰，王海玲難忘民歌歲月，眾多藝人喊她老師》](http://www.ettoday.com/2007/07/05/10845-2121917.htm)

[Category:台灣校園民歌歌手](../Category/台灣校園民歌歌手.md "wikilink")
[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:國立臺灣大學校友](../Category/國立臺灣大學校友.md "wikilink")
[Category:臺北市立第一女子高級中學校友](../Category/臺北市立第一女子高級中學校友.md "wikilink")
[Category:臺北市立金華國民中學校友](../Category/臺北市立金華國民中學校友.md "wikilink")
[Category:浙江裔台灣人](../Category/浙江裔台灣人.md "wikilink")
[Category:臺北市人](../Category/臺北市人.md "wikilink")
[Category:王姓](../Category/王姓.md "wikilink")