**王淑麗**（英語：Rita
wang，），畢業於[東吳大學德文系](../Page/東吳大學.md "wikilink")，暱稱RITA。現任[東森新聞台氣象主播及](../Page/東森新聞台.md "wikilink")《[淑麗氣象趴趴GO](../Page/淑麗氣象趴趴GO.md "wikilink")》主持人。

## 家庭

  - 夫：TVBS資深攝影記者羅樹明。
  - 一個兒子。

## 學歷

  - [東吳大學德文系](../Page/東吳大學.md "wikilink")

## 經歷

  - [環球電視交通線記者](../Page/環球電視.md "wikilink")
  - [TVBS新聞台記者](../Page/TVBS新聞台.md "wikilink")，主播，主持人，氣象主播
  - [東森新聞台主播](../Page/東森新聞台.md "wikilink")
  - [東森新聞台氣象主播及淑麗氣象趴趴GO節目主持人](../Page/東森新聞台.md "wikilink")

## 簡歷

  - 前任的[TVBS氣象主播](../Page/TVBS.md "wikilink")，通常可在[TVBS-NEWS的](../Page/TVBS-NEWS.md "wikilink")《[早安台灣](../Page/早安台灣.md "wikilink")》與[TVBS的](../Page/TVBS_\(頻道\).md "wikilink")《[TVBS晚間新聞](../Page/TVBS晚間新聞.md "wikilink")》的氣象時間看見她播報氣象。曾兼任TVBS頻道每周一至周四下午2:00至3:00的《[搞懂生活](../Page/搞懂生活.md "wikilink")》主持人。曾是TVBS的文字記者兼任新聞主播與氣象主播，當時除了可以看到她所採訪的新聞之外，在颱風天亦會看到她到[中央氣象局透過](../Page/中央氣象局.md "wikilink")[SNG連線播報氣象](../Page/SNG.md "wikilink")。2005年3月因為健康因素，一度離開工作5年的TVBS，3個月後再度重返TVBS，但只負責氣象播報的工作，不再播報新聞與採訪新聞。在加入TVBS之前，王淑麗曾在[東森新聞台與](../Page/東森新聞台.md "wikilink")[環球電視擔任主播](../Page/環球電視_\(台灣\).md "wikilink")。

<!-- end list -->

  - 1995年[環球電視記者](../Page/環球電視_\(台灣\).md "wikilink")、氣象主播、新聞主播。
  - 1999年[東森電視台新聞主播](../Page/東森電視台.md "wikilink")。
  - 2000年[TVBS交通記者兼](../Page/TVBS.md "wikilink")《整點新聞》主播。
  - 2005年3月15日[TVBS交通記者兼整點新聞主播兼氣象主播](../Page/TVBS.md "wikilink")。
  - 2005年6月8日－2007年[TVBS專任氣象主播](../Page/TVBS.md "wikilink")。
  - 2006年11月6日－2007年2月23日[TVBS](../Page/TVBS.md "wikilink")《搞懂生活》主持人。
  - 2010年1月1日－至今加入[東森電視新聞部擔任](../Page/東森電視.md "wikilink")[東森新聞台氣象主播](../Page/東森新聞台.md "wikilink")，[東森電視新聞部](../Page/東森電視.md "wikilink")《[淑麗氣象趴趴GO](../Page/淑麗氣象趴趴GO.md "wikilink")》節目主持人。

## 曾經主持

  - [TVBS頻道](../Page/TVBS頻道.md "wikilink")《搞懂生活》

## 現任

  - [東森新聞台](../Page/東森新聞台.md "wikilink")《淑麗氣象趴趴GO》\[1\]
  - 東森新聞台《淑麗氣象時間》\[2\]

## 書籍

|        |                         |        |         |                    |
| ------ | ----------------------- | ------ | ------- | ------------------ |
| **年份** | **書名**                  | **作者** | **出版社** | **ISBN**           |
| 2013年  | 《淑麗氣象趴趴GO：全台60個私房景點任你玩》 | 王淑麗    | 人類智庫    | ISBN 9789865855628 |
|        |                         |        |         |                    |

## 電視廣告

  - [和泰汽車與您攜手一同守護孩子平安上下學](../Page/和泰汽車.md "wikilink")
  - [交通部公路總局](../Page/交通部公路總局.md "wikilink")102年度機車燃料費開徵宣導
  - [蘇黎世產物保險](../Page/蘇黎世產物保險.md "wikilink")
  - 2014年和泰汽車[兒福聯盟公益廣告](../Page/兒福聯盟.md "wikilink") 暑假作業篇
  - 財政部憑證報稅\[3\]
  - 東森傳愛 聽見愛的聲音\[4\]

## 獎項

|- |2015年||||最佳年度氣象主播獎|| |- |2016年||||最佳氣象主播獎|| |- |2016年||||最佳氣象團隊播報獎||
|- |2017年||||最佳氣象主播獎|| |- |2017年||||最佳氣象團隊播報獎|| |}

## 參考文獻

## 外部連結

  - －痞客邦

  -
[S淑](../Category/王姓.md "wikilink")
[Category:高雄市人](../Category/高雄市人.md "wikilink")
[Category:東吳大學校友](../Category/東吳大學校友.md "wikilink")
[Category:台湾记者](../Category/台湾记者.md "wikilink")
[Category:台灣電視主播](../Category/台灣電視主播.md "wikilink")
[Category:台灣電視氣象主播](../Category/台灣電視氣象主播.md "wikilink")
[Category:台灣新聞節目主持人](../Category/台灣新聞節目主持人.md "wikilink")
[Category:環球主播](../Category/環球主播.md "wikilink")
[Category:東森主播](../Category/東森主播.md "wikilink")
[Category:TVBS主播](../Category/TVBS主播.md "wikilink")

1.
2.
3.  .MOF財政部頻道.2018-04-26
4.  .東森新聞 CH51.2018-10-07