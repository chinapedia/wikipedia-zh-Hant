[王翬.jpg](https://zh.wikipedia.org/wiki/File:王翬.jpg "fig:王翬.jpg")》之王翬像\]\]
**王翬**（），字**石谷**，号**耕烟散人**、**乌目山人**、**清晖老人**；[清初画家](../Page/清.md "wikilink")，[江苏](../Page/江苏.md "wikilink")[常熟人](../Page/常熟.md "wikilink")。

## 生平

王翬出身於繪畫世家。祖上五代擅畫，曾祖[王伯臣善畫花鳥](../Page/王伯臣.md "wikilink")，祖父[王載仕擅長山水](../Page/王載仕.md "wikilink")、人物、花卉；生父[王雲客善畫山水](../Page/王雲客.md "wikilink")。少時先拜同里[張珂為師](../Page/張珂.md "wikilink")，專摹元代[黃公望的山水畫](../Page/黃公望.md "wikilink")，少时山水画見賞於[王鉴](../Page/王鉴.md "wikilink")，被收為弟子，后转师[王時敏](../Page/王時敏.md "wikilink")，對傳統古畫的鑑賞、臨摹，功力極深，宋以來許多失傳的古畫，借王翬的臨摹得以傳世。筆參古今，貌含南北，畫技之精熟為清代第一。清代[张庚的](../Page/张庚.md "wikilink")《国朝画徵录》
评其为“画有南北宗，至石谷而合焉”。康熙三十年（1691年）由宋駿業之薦，上京主持《[康熙南巡圖](../Page/康熙南巡圖.md "wikilink")》的一系列製作。南巡圖繪畢，曾獲當時皇太子胤礽接見，賜座、賜食，並賜「山水清暉」四字。歸里之後，求畫者甚眾。所作多为仿古，功力较深，但有时过于圆熟或伤于刻露，而丘壑尤少变化，晚年于简练中求苍浑，为论者所重；偶写花卉，秀隽有致。

王翬從學弟子甚多，是“虞山派”的創始人。王翬與[王時敏](../Page/王時敏.md "wikilink")、[王鑑](../Page/王鑑_\(畫家\).md "wikilink")、[王原祁合稱](../Page/王原祁.md "wikilink")「[四王](../Page/四王.md "wikilink")」，又与吴历，恽寿平合称“四王吴恽”或“清六家”。

## 后世

2008年9月起，纽约的大都会博物馆展出了王翚的27幅山水画，该展览由四部分27件作品组成，按时间顺序讲述了王翚的艺术发展历程，从早年对传统山水画风的精湛诠释，到1689年获选为康熙皇帝南巡作画而达到事业高峰为止。\[1\]

## 主要作品

### 康熙南巡圖

该图是王翚带领约一千名画工，包括画家[杨晋](../Page/杨晋.md "wikilink")、[冷枚](../Page/冷枚.md "wikilink")、[王云](../Page/王云.md "wikilink")、[徐玫等人](../Page/徐玫.md "wikilink")，历时六年完成的共十二卷的长篇巨制，描绘康熙皇帝六次南巡的情况。该图为绢本，设色，纵高67.8厘米，每一卷的长度为14米至26米不等，今尚存九卷，第一，九，十，十一，十二卷现藏于北京故宫博物院；第二，第四卷现藏于法国巴黎的吉美博物馆；第三，第七卷现藏于美国纽约的大都会艺术博物馆。

### 其他

<File:Wang> Hui10.jpg| <File:Wang> Hui6.jpg| <File:Wang> Hui 001.jpg|

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [2011年列名世界拍賣收入記錄前30名的中國畫家名單已公佈 - 第30名為王翬 Wang Hui
    王翚](https://web.archive.org/web/20120525205204/http://tw.knowledge.yahoo.com/question/article?qid=1712040606153)

[W王](../Category/清朝画家.md "wikilink") [W王](../Category/苏州人.md "wikilink")
[王姓](../Category/王姓.md "wikilink")

1.  [《山水清辉：王翚(1632-1717)艺术展》纽约大都会博物馆开幕](http://humanities.cn/modules/news/view.article.php?a66)