**王若琳**（，），生於[臺灣並從小成長於](../Page/臺灣.md "wikilink")[美國的創作歌手](../Page/美國.md "wikilink")。因此能說流利英語及良好華語。她亦能說簡單日語及葡萄牙語。
她在2015年12月在日本演唱時與日本歌迷用簡單日語交流\[1\]，亦演唱日語歌曲[チキチキバンバン](../Page/チキチキバンバン.md "wikilink")\[2\]及在2008年在節目[週日狂熱夜第](../Page/週日狂熱夜.md "wikilink")57集中演唱葡萄牙語的歌曲
[take me to Aruanda](../Page/take_me_to_Aruanda.md "wikilink"). \[3\]

## 生平

### 早年生活

她的[父親是](../Page/父親.md "wikilink")[音樂製作人](../Page/音樂製作人.md "wikilink")[王治平](../Page/王治平.md "wikilink")\[4\]，母親范中芬是一位[作詞者](../Page/作詞人.md "wikilink"),
寫過［金思頓的夢想］和［愛什麼稀罕］。王若琳出生於[臺北](../Page/臺北.md "wikilink")，但從小就與母親和妹妹住在[洛杉磯](../Page/洛杉磯.md "wikilink")。她在成長過程中大量接觸到各種西洋經典[流行音樂](../Page/流行音樂.md "wikilink")，如[披頭四](../Page/披頭四.md "wikilink")、[比利·喬之類的創作](../Page/比利·喬.md "wikilink")。

### 演藝生涯

在2008年前後，她回到[臺北和父親住在一起](../Page/臺北.md "wikilink")，並發行了第一張[專輯](../Page/音樂專輯.md "wikilink")《Start
from
Here》；該張專輯以雙碟形式發行於2008年1月，其中一張以[英語演唱](../Page/英语.md "wikilink")，另一則以[國語](../Page/國語歌.md "wikilink")。當時，該張專輯在臺灣流行音樂排行榜登上首位，並在[東南亞地區廣為流行](../Page/东南亚.md "wikilink")。

2009年1月，她發行了第二張個人專輯《Joanna &
王若琳》；該張專輯由她自己與父親共同擔任製作人。該張專輯使得她更開始備受關注；[張學友當時曾評論她的獨特歌聲在](../Page/張學友.md "wikilink")[爵士樂領域發揮出色](../Page/爵士樂.md "wikilink")。儘管備受好評，王若琳曾在2013年接受美國中文電視SinoVision記者的訪問坦言演唱流行歌曲pop
songs
並非是自己所喜愛\[5\]，曾於2010年間回到美國繼續完成學業，但於2011年後復出，進行自己喜愛的音樂活動，包括翻唱80至90年代中英文歌曲及演唱自己編曲及填詞的英文搖滾歌曲，並發行多個心儀的專輯。\[6\]\[7\]

## 作品

### 專輯

<table>
<tbody>
<tr class="odd">
<td><p>發行日期</p></td>
<td><p>專輯名稱</p></td>
<td><p>曲目</p></td>
</tr>
<tr class="even">
<td><p>2008-01-11</p></td>
<td><p>Start From Here</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008-05-06</p></td>
<td><p>Start From Here（SACD Version）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009-01-16</p></td>
<td><p>Joanna &amp; 王若琳</p></td>
<td><p>{{hidden</p></td>
</tr>
<tr class="odd">
<td><p>2011-08-24<strong>(日本)</strong><br />
2011-10-28<strong>(台灣)</strong></p></td>
<td><p>The Adventures Of Bernie The Schoolboy(博尼的大冒险)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011-09-23</p></td>
<td><p>The Things We Do For Love（為愛做的一切）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013-03-16</p></td>
<td><p>GALAXY CRISIS : The Strangest Midnight Broadcast（銀河的危機）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014-03-25</p></td>
<td><p>Midnight Cinema（午夜劇院）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015-06-02</p></td>
<td><p>Bob Music（鮑伯音樂）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016-11-11</p></td>
<td><p>House of Bullies（霸凌之家）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018-07-</p></td>
<td><p>摩登悲劇</p></td>
<td><p>{{hidden| |</p></td>
</tr>
</tbody>
</table>

### 單曲

  - 《給牠一個家》(2016年) :
    與[張祖誠](../Page/張祖誠.md "wikilink")、[任家萱](../Page/任家萱.md "wikilink")、[蕭敬騰](../Page/蕭敬騰.md "wikilink")、[王治平](../Page/王治平.md "wikilink")、[李毓芬](../Page/李毓芬.md "wikilink")、[周興哲](../Page/周興哲.md "wikilink")、[王思佳](../Page/王思佳.md "wikilink")、[周予天](../Page/周予天.md "wikilink")、[方炯鑌](../Page/方炯鑌.md "wikilink")、[黃荻鈞](../Page/黃荻鈞.md "wikilink")、[黃路梓茵](../Page/黄路梓茵.md "wikilink")、[Lollipop@F](../Page/Lollipop@F.md "wikilink")、[TOP1男子漢](../Page/TOP1男子漢.md "wikilink")、[李昶俊](../Page/李昶俊.md "wikilink")、[余荃斌共同演唱](../Page/余荃斌.md "wikilink")
  - 《Random Day》(2017年) :與[王詩安合唱](../Page/王詩安.md "wikilink")

### 微電影

  - 2014年《誰殺了王若琳?》飾演王若琳

## 獲獎

  - [新加坡金曲獎](../Page/新加坡金曲獎.md "wikilink")「優秀新人獎」(2008年)
  - 第五屆[勁歌王總選](../Page/「勁歌王」全球華人樂壇音樂盛典.md "wikilink")「台灣區最有前途女新人」(2008年)
  - 第九屆[CCTV-MTV音樂盛典](../Page/CCTV-MTV音樂盛典.md "wikilink")「港台地區年度最具潛力女歌手」(2008年)
  - 新浪2008網路聖典「年度新銳歌手」(2008年)
  - 光線音樂風雲榜新人聖典2008「最佳年度專輯」(2008年)
  - 香港新城電台2008[新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")「新城勁爆國語創作歌手」(2008年)
  - 香港新城電台2008新城勁爆頒獎禮「新城全國樂迷投選勁爆新人王」(2008年)
  - 移動無線音樂咪咕匯2008「最具潛力新人女歌手」(2008年)
  - 首届[蒙牛酸酸乳音樂風雲榜新人盛典](../Page/蒙牛集团.md "wikilink")「最佳新人專輯」
  - 北京流行音樂典禮「最佳創作新人（港台） 」
  - [MusicRadio中國TOP排行榜](../Page/MusicRadio.md "wikilink")
    「港台地區年度最具潛力新人獎」、「港台年度金曲獎」（《迷宮》）
  - [金音創作獎](../Page/金音創作獎.md "wikilink")「最佳創作歌手獎」\[8\](2013年)

## 評價

  - [陶喆](../Page/陶喆.md "wikilink")：有些音樂人的才能是後天的，是靠多年的苦學和磨練得來的。但有些音樂人的福份是上帝賜給他們的
    … 這叫天份。Joanna就是這少有的第二種音樂人。而我們的耳朵也終於有福份，可以聽到一個有靈魂的聲音。
  - [朱衛茵](../Page/朱衛茵.md "wikilink")：身為DJ二十年，難得有一個聲音在第一個瞬間就感動了我。她讓我想起了The
    Beatles, Linda Ronstadt，讓我想起很久以前那個最初的自己。

<!-- end list -->

  - [林暐哲](../Page/林暐哲.md "wikilink")：在Joanna的歌聲中，我聽到輕而易舉的感動，更聽到睥睨一切的驕傲。

<!-- end list -->

  - [張懸](../Page/張懸.md "wikilink")：既像咖啡有的濃郁又有酒精能帶來的醉意。讓我們聽歌的耳朵一邊被沁透，一邊生出溫熱。王若琳她的聲音，真是美得不得了……\[9\]

## 參考資料

## 外部連結

  -
[W王](../Category/台灣女歌手.md "wikilink") [W](../Category/左撇子.md "wikilink")
[R](../Category/王姓.md "wikilink")
[Category:台灣音樂製作人](../Category/台灣音樂製作人.md "wikilink")
[Category:晶片音樂作曲家](../Category/晶片音樂作曲家.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.