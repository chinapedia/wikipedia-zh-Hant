**王重民**（），字**有三**，號**冷廬主人**，[河北](../Page/河北.md "wikilink")[高陽人](../Page/高阳县.md "wikilink")，[中國](../Page/中國.md "wikilink")[目錄學家和](../Page/目錄學.md "wikilink")[版本學家](../Page/版本學.md "wikilink")。

## 生平

1924年，王重民入讀[北京高等師範學校](../Page/北京高等師範學校.md "wikilink")，研究中國[古籍](../Page/古籍.md "wikilink")。1928年畢業。在[北京圖書館開始了](../Page/北京圖書館.md "wikilink")[編目科工作](../Page/編目.md "wikilink")，主要整理[敦煌遺書](../Page/敦煌.md "wikilink")、[太平天國](../Page/太平天國.md "wikilink")[史料及古籍](../Page/史料.md "wikilink")。1934年至1939年，在[英國](../Page/英國.md "wikilink")、[德國](../Page/德國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[美國等國各大](../Page/美國.md "wikilink")[圖書館抄錄了很多資料](../Page/圖書館.md "wikilink")，並發表多篇關於敦煌及太平天國史料的文章。尤其是在落實中法兩國政府交流項目上，他被選派到法国国家图书馆，從事對該館所藏大量敦煌文獻的編目工作，而法方派到北京圖書館的則是[杜乃揚女士](../Page/杜乃揚.md "wikilink")，她負責整理北圖所藏的法文書籍目錄。1939年至1947年，他赴[美國國會圖書館鑒定](../Page/美國國會圖書館.md "wikilink")[善本書](../Page/善本.md "wikilink")。\[1\]後返回中國，擔任[北京大學教授](../Page/北京大學.md "wikilink")，並建議成立[圖書館學系](../Page/圖書館學.md "wikilink")。1949年，出任圖書館學-{系系}-主任。

1950年起，王重民講授和編寫了很多圖書館學的課題與文章，譬如：《[中文](../Page/中文.md "wikilink")[工具書使用法](../Page/工具書.md "wikilink")》、《參考資料與參考工作》、《普通目錄學》、《歷史書籍目錄學》、《中國目錄學史》、《中國目錄學史料》、《[近代目錄史料](../Page/近代史.md "wikilink")》、《中國書史》。主要著作有《[道德經碑幢](../Page/道德經.md "wikilink")[刻石考](../Page/刻石.md "wikilink")》、《[老子考](../Page/老子.md "wikilink")》、《[國學](../Page/國學.md "wikilink")[論文](../Page/論文.md "wikilink")[索引](../Page/索引.md "wikilink")》、《清代文集篇目分類索引》、《[日本訪書志補](../Page/日本.md "wikilink")》、《太平天國官書》、《敦煌[曲子詞集](../Page/曲子詞.md "wikilink")》、《敦煌古籍敘錄》、《善本醫籍經眼錄》、《[徐光啟傳](../Page/徐光啟.md "wikilink")》、《中國善本書提要》、《中國目錄學史料》、《[四庫抽毀書提要稿](../Page/四庫全書.md "wikilink")》、《美國國會圖書館藏中國善本書錄》（由[袁同礼修订](../Page/袁同礼.md "wikilink")\[2\]）等。

1957年8月被划为[右派](../Page/右派.md "wikilink")，撤销系主任职务。1962年摘掉右派帽子。1963年参加了四清运动。文革中被抄家。1974年，王重民接受了辨伪《[史纲评要](../Page/史纲评要.md "wikilink")》的工作。王重民在北大图书馆寻出一部明代万曆版的《史纲要领》，经比较断定《[史纲评要](../Page/史纲评要.md "wikilink")》是托名李贽的著作。1975年4月，因此受批判而在[颐和园长廊自缢身亡](../Page/颐和园.md "wikilink")\[3\]。

1978年5月30日，中共北京大学委员会在全系大会上为其平反，1979年又发出为其1957年错划右派的改正通知。

## 参考资料

## 外部链接

  - [王重民：七十老翁何所求](https://www.thepaper.cn/newsDetail_forward_1530158)

[Category:北京师范大学校友](../Category/北京师范大学校友.md "wikilink")
[Category:北京大学教授](../Category/北京大学教授.md "wikilink")
[Category:中国目录学家](../Category/中国目录学家.md "wikilink")
[Category:敦煌学家](../Category/敦煌学家.md "wikilink")
[Category:文革自殺者](../Category/文革自殺者.md "wikilink")
[W王](../Category/文革非正常死亡人物.md "wikilink")
[Category:文革被迫害学者](../Category/文革被迫害学者.md "wikilink")
[Category:自杀作家](../Category/自杀作家.md "wikilink")
[Category:高陽人](../Category/高陽人.md "wikilink")
[C重](../Category/王姓.md "wikilink")

1.

2.
3.