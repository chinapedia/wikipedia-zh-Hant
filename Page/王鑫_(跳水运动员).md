**王鑫**（）是[湖北省](../Page/湖北省.md "wikilink")[武汉市](../Page/武汉市.md "wikilink")[漢口人](../Page/漢口.md "wikilink")，中國女子[跳水運動員](../Page/跳水.md "wikilink")，原名**王若雪**。她在[2008年北京奥运会上与](../Page/2008年北京奥运会.md "wikilink")[陈若琳搭档夺得了](../Page/陈若琳.md "wikilink")[双人10米跳台金牌](../Page/2008年夏季奧林匹克運動會跳水比賽.md "wikilink")。[北京奥运会後](../Page/北京奥运会.md "wikilink")，由於身高、體重都增高了不少，王鑫在10米跳台項目失去了競爭力，逐渐淡出了中國跳水隊主力陣容，改向跳板方向發展。

## 主要成績

  - 2006年，[多哈亞運會女子個人](../Page/多哈亞運會.md "wikilink")10米跳台冠軍。
  - 2007年，2007年，[墨爾本](../Page/墨爾本.md "wikilink")[2007年世界游泳錦標賽女子](../Page/第十二屆世界游泳錦標賽.md "wikilink")10米跳台冠軍。
  - 2007年，跳水系列賽美國站女子個人10米跳台冠軍。
  - 2008年，北京奥运会[双人10米跳台金牌](../Page/2008年夏季奧林匹克運動會跳水比賽.md "wikilink")。
  - 2008年，北京奧運會[個人10米跳台銅牌](../Page/2008年夏季奧林匹克運動會跳水比賽.md "wikilink")。
  - 2011年，深圳大學生運動會[個人10米跳台銅牌](../Page/2011年夏季世界大學生運動會跳水比賽.md "wikilink")。

## 參考資料

## 外部連結

[Category:中国跳水运动员](../Category/中国跳水运动员.md "wikilink")
[Category:中国奥运跳水运动员](../Category/中国奥运跳水运动员.md "wikilink")
[Category:武汉籍运动员](../Category/武汉籍运动员.md "wikilink")
[Xin](../Category/王姓.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:中国奥林匹克运动会铜牌得主](../Category/中国奥林匹克运动会铜牌得主.md "wikilink")
[Category:世界游泳錦標賽跳水項目冠軍](../Category/世界游泳錦標賽跳水項目冠軍.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會跳水運動員](../Category/2008年夏季奧林匹克運動會跳水運動員.md "wikilink")
[Category:奧林匹克運動會跳水獎牌得主](../Category/奧林匹克運動會跳水獎牌得主.md "wikilink")
[Category:漢口人](../Category/漢口人.md "wikilink")
[Category:2006年亞洲運動會跳水運動員](../Category/2006年亞洲運動會跳水運動員.md "wikilink")
[Category:2006年亚洲运动会金牌得主](../Category/2006年亚洲运动会金牌得主.md "wikilink")
[Category:亞洲運動會跳水獎牌得主](../Category/亞洲運動會跳水獎牌得主.md "wikilink")
[W](../Category/1992年出生.md "wikilink")