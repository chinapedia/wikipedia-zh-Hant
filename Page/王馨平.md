**王馨平**（英文名：****，），[香港及](../Page/香港.md "wikilink")[台灣女](../Page/台灣.md "wikilink")[歌手及](../Page/歌手.md "wikilink")[演員](../Page/演員.md "wikilink")，籍貫[江蘇](../Page/江蘇.md "wikilink")[無錫](../Page/無錫.md "wikilink")。畢業於[台灣](../Page/台灣.md "wikilink")[國立政治大學新聞系](../Page/國立政治大學.md "wikilink")，著名歌曲有《別問我是誰》、《問你問我》、《伴你一生》和《生命有價》等。她與[劉小慧](../Page/劉小慧.md "wikilink")、[黎瑞恩](../Page/黎瑞恩.md "wikilink")、[湯寶如](../Page/湯寶如.md "wikilink")、[周慧敏](../Page/周慧敏.md "wikilink")、[關淑怡](../Page/關淑怡.md "wikilink")、[陳慧嫻並稱](../Page/陳慧嫻.md "wikilink")「[寶麗金七小花](../Page/寶麗金.md "wikilink")」\[1\]。她的父親為港台武打明星[王羽](../Page/王羽.md "wikilink")，母親為已故香港女星[林翠](../Page/林翠.md "wikilink")（原名曾懿貞），舅父為香港演員[曾江](../Page/曾江.md "wikilink")，另有一個同母異父的哥哥[陳山河](../Page/陳山河.md "wikilink")。

她曾與歌手[鄭嘉穎拍拖](../Page/鄭嘉穎.md "wikilink")，但因父親高調反對而最終於1998年分開，後與於金融界任職的男友李家輝（Stephen
Lee）結婚，婚後淡出演藝圈。

## 演藝歷程

### 出道經過（1992年）

1992年，大學四年級時，她的同學將她唱歌的樣本寄給唱片公司，後得[香港](../Page/香港.md "wikilink")[寶麗金唱片賞識](../Page/寶麗金.md "wikilink")，並與唱片公司簽約。之後唱片公司安排她拍攝[張學友](../Page/张学友.md "wikilink")《情網》MTV，以及[黎明的音樂特輯](../Page/黎明.md "wikilink")《星空下的緣份》。

### 走紅期（1993年—1995年）

1992年10月，王馨平簽約寶麗金唱片，1993年6月17日推出首張粵語大碟《[王馨平](../Page/王馨平_\(專輯\).md "wikilink")》，正式進入香港樂壇，第一主打為《在你走的一天》。這首歌曲是她的第一首快歌，於當年獲得TVB十大[勁歌金曲最受歡迎女新人獎](../Page/勁歌金曲.md "wikilink")－銀獎。第二主打為[甄妮作曲的](../Page/甄妮.md "wikilink")《難忘初戀》，第三主打為《夢裡緣份》。同年10月5日，推出首張國語大碟《[別問我是誰](../Page/別問我是誰.md "wikilink")》，正式進軍[台灣樂壇](../Page/台灣.md "wikilink")，第一主打為《別問我是誰》，並憑此曲登上台灣金曲龍虎榜的冠軍，作為新人的第一首歌曲來說，成績婓然。第二主打為《伴你一生》，是[台灣金曲龍虎榜的勁歌推薦](../Page/台灣金曲龍虎榜.md "wikilink")，第三主打為《愛我一生一世好不好》，本專輯於台灣的總銷售量為60萬張，驚人的佳績使仍是新人的王馨平成功打開了台灣市場。同年12月17日，推出第二張粵語大碟《[愛一生也不夠](../Page/愛一生也不夠.md "wikilink")》，第一主打歌為《不要慰問》，第二主打歌為《寒冰》，第三主打歌為《愛一生也不夠》（《伴你一生》粵語版），專輯亦在港得到不錯反應；首年優秀的歌唱發展使王馨平於年尾香港樂壇頒獎禮上奪得商業電台叱吒樂壇新力軍金獎、香港電台最有前途女新人金獎、以及無視電視最受歡迎新人獎銀獎。首年發展的王馨平可算是立刻走紅。

1994年，與台灣歌手[高明駿合唱歌曲](../Page/高明駿.md "wikilink")《今生註定》，此歌與[許志安](../Page/許志安.md "wikilink")《唯獨你是不可取替》、[鄭秀文](../Page/鄭秀文.md "wikilink")《衝動點唱》及[黎明詩](../Page/黎明詩.md "wikilink")《敢恨敢愛》是同曲改編，登上台灣KTV排行榜長達8個月。同年6月24日，推出第三張粵語大碟《[理想情人](../Page/理想情人.md "wikilink")》，第一主打為快歌《舞林大會》，第二主打為《理想情人》，這首歌曲在流行榜的位置相當不俗。第三主打為《想你親口說愛我》。同年年中，推出與[鄭嘉穎](../Page/鄭嘉穎.md "wikilink")、[湯寶如合唱歌曲](../Page/湯寶如.md "wikilink")《飛出戀愛街》，以及與[周慧敏](../Page/周慧敏.md "wikilink")、[湯寶如合唱卡通片](../Page/湯寶如.md "wikilink")《[美少女戰士](../Page/美少女戰士.md "wikilink")》同名主題曲。同年10月24日，推出第二張國語大碟《[一生癡戀](../Page/一生癡戀.md "wikilink")》，第一主打為《一生癡戀》，在台灣金曲龍虎榜位置不俗。第二主打為與台灣創作歌手[林東松合唱的](../Page/林東松.md "wikilink")《如果讓我再愛一次》。第三主打為電影《中南海保鏢》國語版主題曲《請你看著我的眼睛》。同年11月18日，推出第四張粵語大碟《[飛](../Page/飛_\(王馨平專輯\).md "wikilink")》，第一主打為[王雙駿作曲的](../Page/王雙駿.md "wikilink")《瀟灑女人》，第二主打為電影《[青春火花](../Page/青春火花.md "wikilink")》主題曲《勝利的姿態》，第三主打為電影《[中南海保鑣](../Page/中南海保鑣.md "wikilink")》主題曲《不要躲避我的眼睛》，第四主打為改編自[張宇](../Page/張宇.md "wikilink")《飛》的《飛》。此外，她與[許志安及](../Page/許志安.md "wikilink")[黃秋生合作拍攝電影](../Page/黃秋生.md "wikilink")《[夢差人](../Page/夢差人.md "wikilink")》，這部電影是她的首部電影。

1995年3月，與[鄭嘉穎](../Page/鄭嘉穎.md "wikilink")、[曹永廉](../Page/曹永廉.md "wikilink")、[陳藝鳴](../Page/陳藝鳴.md "wikilink")、[泰迪羅賓合作拍攝電影](../Page/泰迪羅賓.md "wikilink")《[香江花月夜](../Page/香江花月夜_\(1995年電影\).md "wikilink")》。同年4月10日，推出首張粵語新曲+精選大碟《[願望](../Page/願望_\(王馨平精選輯\).md "wikilink")》，主打歌《問你問我》推出後反應頗佳，MV更邀得灸手可熱的著名影星[任達華擔任男主角](../Page/任達華.md "wikilink")，亦成為了王馨平代表作品之一，而這張新曲精選專輯在香港亦得到熱賣。同年7月10日，推出第三張國語大碟《[織心](../Page/織心.md "wikilink")》，第一主打為《織心》，第二主打為《普通女人》，這兩首歌成為台灣金曲龍虎榜及Channel
V華語榜中榜Top
20的金曲推介，第三主打為翻唱[鄧麗君的](../Page/鄧麗君.md "wikilink")《月亮代表我的心》，此歌為電影《[香江花月夜](../Page/香江花月夜_\(1995年電影\).md "wikilink")》的插曲。同年8月24日，推出第五張粵語大碟《[馨平個性](../Page/馨平個性.md "wikilink")》，第一主打為《普通女人》的粵語版，這首歌曲曾攀上無視電視勁歌金榜的冠軍位置，第二主打為《沒有月亮的晚上》，此歌成為勁歌金曲的的勁歌推介，第三主打為《個性》，值得一提是專輯內一首勵志歌曲作品《生命有價》在電視節目的熱播下，迅速地在坊間流行起來，很多不同年齡層的學生及家長均對此曲瑯瑯上口，成為了不少香港人的集體成長回憶金曲之一，時至今日仍具一定傳唱度。同年10月，與[任達華合作拍攝電影](../Page/任達華.md "wikilink")《[熱線追擊](../Page/熱線追擊.md "wikilink")》。其後，Linda因父親[王羽和](../Page/王羽.md "wikilink")[甄妮的上輩恩怨激化](../Page/甄妮.md "wikilink")，導致她與[甄妮爆發經理人合約糾紛](../Page/甄妮.md "wikilink")，最後她與[甄妮解除](../Page/甄妮.md "wikilink")[家平製作園地的經理人合約](../Page/家平製作園地.md "wikilink")。

### 轉型期（1996年—1997年）

1996年，無經理人合約的她於同年2月12日推出第六張粵語大碟《[著迷](../Page/著迷_\(王馨平專輯\).md "wikilink")》，此專輯中的選曲及形象均嘗試轉走較成熟風格，亦嘗試藍調風格歌曲，第一主打為一改型像的快歌《著迷》，第二主打為[趙增熹作曲的](../Page/趙增熹.md "wikilink")《原來如此》，第三主打為藍調怨曲《心藍》，此歌成為勁歌金曲的勁歌推介，第四主打為電影《[雷雨](../Page/雷雨.md "wikilink")》主題曲《醉如夢》，惜是次專輯反應一般。同年5月10日，推出第四張國語大碟《[受害者](../Page/受害者_\(王馨平專輯\).md "wikilink")》，第一主打為慢歌《受害者》，這首歌為她當時在合約糾紛中的心情寫照，此歌在台灣[Channel
V](../Page/Channel_V.md "wikilink")[華語榜中榜的表現不俗](../Page/華語榜中榜.md "wikilink")。第二主打為《網》，第三主打為《逃》，此專輯在台灣銷量及口碑不俗。同年6月7日，與[李麗珍](../Page/李麗珍.md "wikilink")、[莫文蔚](../Page/莫文蔚.md "wikilink")、[李蕙敏合作拍攝電影](../Page/李蕙敏.md "wikilink")《[四個32A和一個香蕉少年](../Page/四個32A和一個香蕉少年.md "wikilink")》。同年11月18日，推出第五張國語大碟《[馨平氣和](../Page/馨平氣和.md "wikilink")》，第一主打為《心平氣和》，這首歌是她首次嘗試走鄉村民謠曲風路線的歌。第二主打為慢歌《愛讓人太盲目》，這首歌是她在學生時代的初戀寫照。第三主打為《一個人過的夜晚沒是非》，第四主打為《I
Love The Way I Am》。是年王馨平因兼顧台灣市場，在港的宣傳及曝光相對較少。

1997年4月11日，推出第二張粵語新曲+精選大碟《[Time After
Time](../Page/Time_After_Time_\(新曲加精選\).md "wikilink")》。第一主打為快歌《男人都這麼樣》，第二主打為慢歌《髮邊的一句說話》，此曲是某洗髮液廣告歌曲並曾在電視上熱播，第三主打為《全靠我》，第四主打為翻唱[譚詠麟的](../Page/譚詠麟.md "wikilink")《最愛的你》，此新曲+精選大碟亦是她在寶麗金唱片的最後一張專輯。同年8月，與[沈殿霞](../Page/沈殿霞.md "wikilink")、[吳孟達](../Page/吳孟達.md "wikilink")、[莫少聰](../Page/莫少聰.md "wikilink")、[伍詠薇合作拍攝電影](../Page/伍詠薇.md "wikilink")《[喜氣逼人](../Page/喜氣逼人.md "wikilink")》。其後，她為動畫電影《小倩》演唱其粵語插曲《姥姥之歌》及國語插曲《我最美》之後與寶麗金唱片正式約滿。

### 離開寶麗金（1998年—2001年）

1998年，與寶麗金唱片約滿之後，她幾乎絕跡於香港樂壇，這段期間她首次為[亞洲電視劇集](../Page/亞洲電視.md "wikilink")《[我和殭屍有個約會](../Page/我和殭屍有個約會.md "wikilink")》演唱其主題曲《夢裡是誰》和插曲《愛債幾時還》，亦參與[亞洲電視電視劇](../Page/亞洲電視.md "wikilink")《[我來自廣州](../Page/我來自廣州.md "wikilink")》的演出。直至1999年，她與台灣[協和經紀公司簽約](../Page/協和經紀.md "wikilink")，加盟台灣[坎城唱片](../Page/坎城唱片.md "wikilink")，並於同年12月2日推出第六張國語大碟《[打碎心瓶](../Page/打碎心瓶.md "wikilink")》，決心轉戰台灣樂壇。這張專輯較熟悉的歌曲有《就當不曾認識你吧》、《計較》，以及Linda親自創作的《打碎心瓶》及《第二個可能》。不料，因為坎城唱片經營不善及其繼母[王凱貞發生婚外情而影響了後期宣傳](../Page/王凱貞.md "wikilink")，使得此專輯亦成為她暫別樂壇的最後一張專輯。

2000年，她轉往影壇及戲劇圈發展，與[黃維德](../Page/黃維德.md "wikilink")、[喻可欣](../Page/喻可欣.md "wikilink")、[雷宇揚合演電影](../Page/雷宇揚.md "wikilink")《[砵蘭街皇后](../Page/砵蘭街皇后.md "wikilink")》，以及與[呂良偉合演電影](../Page/呂良偉.md "wikilink")《[月夜閃靈](../Page/月夜閃靈.md "wikilink")》。2001年與[李婷宜及](../Page/李婷宜.md "wikilink")[張瑞哲](../Page/張瑞哲.md "wikilink")、[林鳳英合演電影](../Page/林鳳英.md "wikilink")《[情迷夢幻號](../Page/情迷夢幻號.md "wikilink")》，亦與台灣演員[范鴻軒及中國大陸演員](../Page/范鴻軒.md "wikilink")[黃格達](../Page/黃格達.md "wikilink")、[劉牧](../Page/劉牧.md "wikilink")、[涂松岩主演中國大陸劇集](../Page/涂松岩.md "wikilink")《[都是愛情惹的禍](../Page/都是愛情惹的禍_\(電視劇\).md "wikilink")》。

### 組織家庭（2001年-2010年）

2001年10月，王馨平宣佈與金融界男友－李家輝（Stephen Lee）正式結婚。

2002年，轉往主持界發展，於[TVB8主持談話性節目](../Page/TVB8.md "wikilink")《[星光剪影](../Page/星光剪影.md "wikilink")》，隨後於2004年主持《談談情唱唱歌》及擔任《[娛樂最前線](../Page/娛樂最前線.md "wikilink")》的主播。

2007年10月，她在參與[黎小田](../Page/黎小田.md "wikilink")《問我30年黎小田演唱會》時表示，如有人肯出錢替她出新歌出唱片，就會考慮復出。

2008年，她出席《博愛歡樂傳萬家》，表演了《自作多情》、《小玩意》等曲目。

### 復出樂壇（2011年至今）

2011年5月，她於[微博中證實會重新推出唱片](../Page/微博.md "wikilink")，並指出她早在2010年下半年開始錄音工作，同時亦參與全部製作部份。

2011年7月26日，推出第一張翻唱專輯《[馨情](../Page/馨情.md "wikilink")》，主要以翻唱經典國語歌為主，共翻唱了10首國語歌曲及收錄一首粵語單曲。第一主打歌粵語新歌《家後》，曾在香港上電台及勁歌金曲宣傳，第二主打為《好男人》。而她曾提及自己作曲的《愛點滴》則收錄在同年推出的《Day
By Day》合輯中。

2012年參與[彭浩翔賣座電影](../Page/彭浩翔.md "wikilink")《春嬌與志明》演出，並由於片中選取了王馨平的1993年代表作《別問我是誰》作為電影歌曲，使此曲在相距近二十年後再度掀起流行熱潮，而《別問我是誰》早年上載在[YouTube上的MV亦突然暴升至超過](../Page/YouTube.md "wikilink")100萬點擊率，掀起不少70及80後的集體回憶之外，亦吸納了不少90後年青聽眾認識此曲，使王馨平在退出樂壇多年後再度受到注意及報導。

2013年3月，推出國語歌曲\[2\]《平凡》。

2014年2月12日，王馨平與[李樂詩](../Page/李樂詩_\(歌手\).md "wikilink")、[李幸倪合作舉行慈善音樂會](../Page/李幸倪.md "wikilink")——《「盛載愛」音樂會》\[3\]，收益撥歸[香港傷健協會](../Page/香港傷健協會.md "wikilink")\[4\]。

2014年7月23日，她於[微博中表示](../Page/微博.md "wikilink")7月會推出第二張翻唱專輯《[Truly](../Page/Truly.md "wikilink")》，主要以翻唱經典國語歌及粵語歌為主，共翻唱了4首國語歌曲及6首粵語歌曲，並指出她早在2013年開始錄音工作，同時亦參與選曲部份，第一主打歌為翻唱[黎明的粵語舊作](../Page/黎明.md "wikilink")《今夜你會不會來》。

2015年8月13日，推出第三張新曲+精選《[情網](../Page/情網.md "wikilink")》。收錄兩首翻唱新歌，及收錄4首寶麗金時代舊歌及9首前兩張翻唱專輯的歌曲。

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/王馨平_(專輯).md" title="wikilink">王馨平</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1993年6月17日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/別問我是誰.md" title="wikilink">別問我是誰</a><small><strong>（國語專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金_(臺灣).md" title="wikilink">寶麗金 (臺灣)</a></p></td>
<td style="text-align: left;"><p>1993年10月5日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3</p></td>
<td style="text-align: left;"><p><a href="../Page/愛一生也不夠.md" title="wikilink">愛一生也不夠</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1993年12月17日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4</p></td>
<td style="text-align: left;"><p><a href="../Page/不要慰問.md" title="wikilink">不要慰問</a></p></td>
<td style="text-align: left;"><p>混音EP</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1994年2月23日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>5</p></td>
<td style="text-align: left;"><p><a href="../Page/理想情人.md" title="wikilink">理想情人</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1994年6月24日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>6</p></td>
<td style="text-align: left;"><p><a href="../Page/一生癡戀.md" title="wikilink">一生癡戀</a><small><strong>（國語專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金_(臺灣).md" title="wikilink">寶麗金 (臺灣)</a></p></td>
<td style="text-align: left;"><p>1994年10月24日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>7</p></td>
<td style="text-align: left;"><p><a href="../Page/舞林大會_(EP).md" title="wikilink">舞林大會</a></p></td>
<td style="text-align: left;"><p>混音EP</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1994年11月10日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>8</p></td>
<td style="text-align: left;"><p><a href="../Page/飛_(王馨平專輯).md" title="wikilink">飛</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1994年11月18日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>9</p></td>
<td style="text-align: left;"><p><a href="../Page/願望_(王馨平精選輯).md" title="wikilink">願望</a></p></td>
<td style="text-align: left;"><p>新歌+精選</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1995年4月10日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>10</p></td>
<td style="text-align: left;"><p><a href="../Page/織心.md" title="wikilink">織心</a><small><strong>（國語專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金_(臺灣).md" title="wikilink">寶麗金 (臺灣)</a></p></td>
<td style="text-align: left;"><p>1995年7月10日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>11</p></td>
<td style="text-align: left;"><p><a href="../Page/馨平個性.md" title="wikilink">馨平個性</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1995年8月24日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>12</p></td>
<td style="text-align: left;"><p><a href="../Page/著迷_(王馨平專輯).md" title="wikilink">著迷</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1996年2月12日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>13</p></td>
<td style="text-align: left;"><p><a href="../Page/受害者_(王馨平專輯).md" title="wikilink">受害者</a><small><strong>（國語專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金_(臺灣).md" title="wikilink">寶麗金 (臺灣)</a></p></td>
<td style="text-align: left;"><p>1996年5月10日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>14</p></td>
<td style="text-align: left;"><p><a href="../Page/馨平氣和.md" title="wikilink">馨平氣和</a><small><strong>（國語專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金_(臺灣).md" title="wikilink">寶麗金 (臺灣)</a></p></td>
<td style="text-align: left;"><p>1996年11月18日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>15</p></td>
<td style="text-align: left;"><p><a href="../Page/Time_After_Time_(王馨平精選輯).md" title="wikilink">Time After Time</a></p></td>
<td style="text-align: left;"><p>新歌+精選</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1997年4月11日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>16</p></td>
<td style="text-align: left;"><p><a href="../Page/初戀_(王馨平精選輯).md" title="wikilink">初戀</a></p></td>
<td style="text-align: left;"><p>精選</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1998年10月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>17</p></td>
<td style="text-align: left;"><p><a href="../Page/打碎心瓶.md" title="wikilink">打碎心瓶</a><small><strong>（國語專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p>-{<a href="../Page/坎城唱片.md" title="wikilink">坎城唱片</a>}-<br />
<a href="../Page/友善的狗.md" title="wikilink">友善的狗</a></p></td>
<td style="text-align: left;"><p>1999年12月2日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>18</p></td>
<td style="text-align: left;"><p><a href="../Page/環球真經典系列_(王馨平).md" title="wikilink">環球真經典系列</a></p></td>
<td style="text-align: left;"><p>精選</p></td>
<td style="text-align: left;"><p><a href="../Page/環球唱片_(香港).md" title="wikilink">環球唱片 (香港)</a></p></td>
<td style="text-align: left;"><p>2001年7月21日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>19</p></td>
<td style="text-align: left;"><p><a href="../Page/馨情.md" title="wikilink">馨情</a></p></td>
<td style="text-align: left;"><p>翻唱大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/新世紀工作室.md" title="wikilink">新世紀工作室</a></p></td>
<td style="text-align: left;"><p>2011年7月26日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/馨情.md" title="wikilink">馨情 （K2HD CD限量版）</a></p></td>
<td style="text-align: left;"><p>翻唱大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/新世紀工作室.md" title="wikilink">新世紀工作室</a></p></td>
<td style="text-align: left;"><p>2011年12月1日</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>20</p></td>
<td style="text-align: left;"><p><a href="../Page/Truly.md" title="wikilink">Truly</a></p></td>
<td style="text-align: left;"><p>翻唱大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LUVLEE_Ltd..md" title="wikilink">LUVLEE Ltd.</a><br />
<a href="../Page/新世紀工作室.md" title="wikilink">新世紀工作室</a><br />
<a href="../Page/秘密花園娛樂.md" title="wikilink">秘密花園娛樂</a></p></td>
<td style="text-align: left;"><p>2014年7月23日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>21</p></td>
<td style="text-align: left;"><p><a href="../Page/情網.md" title="wikilink">情網</a></p></td>
<td style="text-align: left;"><p>新曲+精選</p></td>
<td style="text-align: left;"><p><a href="../Page/新世紀工作室.md" title="wikilink">新世紀工作室</a><br />
<a href="../Page/環球唱片_(香港).md" title="wikilink">環球唱片 (香港)</a></p></td>
<td style="text-align: left;"><p>2015年8月13日</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 合輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/熱力節拍Summer_Party.md" title="wikilink">熱力節拍Summer Party</a></p></td>
<td style="text-align: left;"><p>新曲＋精選</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1993年8月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/The_World_Song_為全世界歌唱.md" title="wikilink">The World Song 為全世界歌唱</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1995年11月</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### VCD專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><strong>著迷</strong></p></td>
<td style="text-align: left;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: left;"><p>1996年10月</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 單曲

  - 1994年：《飛出戀愛街》([鄭嘉穎](../Page/鄭嘉穎.md "wikilink")、[湯寶如合唱](../Page/湯寶如.md "wikilink"))（收錄於鄭嘉穎《[全因身邊有你](../Page/全因身邊有你.md "wikilink")》專輯）
  - 1994年：《鼓舞飛揚（十分精彩版）》、《鼓舞飛揚（特別精選版）》(寶麗金群星合唱)（收錄於《[鼓舞飛揚](../Page/鼓舞飛揚.md "wikilink")》合輯）
  - 1995年：《鼓舞飛揚'95》(寶麗金群星合唱)（收錄於《[鼓舞飛揚'95](../Page/鼓舞飛揚'95.md "wikilink")》合輯）
  - 1995年：《為全世界歌唱》(寶麗金群星合唱)（收錄於《[The World Song
    為全世界歌唱](../Page/The_World_Song_為全世界歌唱.md "wikilink")》合輯）
  - 1995年：《出位地帶》(湯寶如、[CD
    Voice合唱](../Page/CD_Voice.md "wikilink"))（收錄於《The
    World Song 為全世界歌唱》合輯）

### 未出版歌曲

  - 2013年：《平凡》
  - 2015年：《補償》
  - 2016年：《貝殼》、《青春不散》（[崔恕合唱](../Page/崔恕.md "wikilink")）
  - 2017年：《時差》([何乾樑合唱](../Page/何乾樑.md "wikilink"))

### 動畫歌曲

  - 1994年：《美少女戰士》（[TVB動畫](../Page/TVB.md "wikilink")《[美少女戰士](../Page/美少女戰士.md "wikilink")》粵語主題曲）([周慧敏及](../Page/周慧敏.md "wikilink")[湯寶如合唱](../Page/湯寶如.md "wikilink"))

## 派台歌曲成績

<div class="notice metadata" id="disambig">

<small>[Nuvola_apps_kalarm.png](https://zh.wikipedia.org/wiki/File:Nuvola_apps_kalarm.png "fig:Nuvola_apps_kalarm.png")这是一個**[动态的](../Category/动态列表.md "wikilink")[未完成列表](../Category/未完成列表.md "wikilink")**，内容可能会随时增加，所以它可能永远也不会補充完整，但歡迎您'''隨時<span class="plainlinks">\[
修改\]</span>并[列明来源](../Page/Wikipedia:列明来源.md "wikilink")。</small>

</div>

<includeonly></includeonly><noinclude>

| **派台歌曲成績**                                                         |
| ------------------------------------------------------------------ |
| 唱片                                                                 |
| **1993年**                                                          |
| [王馨平](../Page/王馨平_\(專輯\).md "wikilink")                            |
| 王馨平                                                                |
| 王馨平                                                                |
| [別問我是誰](../Page/別問我是誰.md "wikilink")                               |
| [愛一生也不夠](../Page/愛一生也不夠.md "wikilink")                             |
| **1994年**                                                          |
| 愛一生也不夠                                                             |
| [全因身邊有你](../Page/全因身邊有你.md "wikilink")                             |
| 愛一生也不夠                                                             |
| 愛一生也不夠                                                             |
| [理想情人](../Page/理想情人.md "wikilink")                                 |
| 理想情人                                                               |
| [一生癡戀](../Page/一生癡戀.md "wikilink")                                 |
| [給世界多一個微笑](../Page/給世界多一個微笑.md "wikilink")                         |
| [飛](../Page/飛_\(王馨平專輯\).md "wikilink")                             |
| 飛                                                                  |
| 飛                                                                  |
| 飛                                                                  |
| **1995年**                                                          |
| [願望](../Page/願望_\(王馨平精選輯\).md "wikilink")                          |
| 願望                                                                 |
| 願望                                                                 |
| [織心](../Page/織心.md "wikilink")                                     |
| [馨平個性](../Page/馨平個性.md "wikilink")                                 |
| 馨平個性                                                               |
| 馨平個性                                                               |
| **1996年**                                                          |
| [着迷](../Page/着迷_\(王馨平專輯\).md "wikilink")                           |
| 着迷                                                                 |
| 着迷                                                                 |
| [受害者](../Page/受害者_\(王馨平專輯\).md "wikilink")                         |
| 受害者                                                                |
| [馨平氣和](../Page/馨平氣和.md "wikilink")                                 |
| **1997年**                                                          |
| [Time After Time](../Page/Time_After_Time_\(新曲加精選\).md "wikilink") |
| Time After Time                                                    |
| Time After Time                                                    |
| **2000年**                                                          |
| [打碎心瓶](../Page/打碎心瓶.md "wikilink")                                 |
| **2011年**                                                          |
| [馨情](../Page/馨情.md "wikilink")                                     |
| 馨情                                                                 |
| **2013年**                                                          |
|                                                                    |
| [寶記正傳](../Page/寶記正傳.md "wikilink")                                 |
| **2014年**                                                          |
| [Truly](../Page/Truly.md "wikilink")                               |
| **2015年**                                                          |
|                                                                    |
| **2017年**                                                          |
|                                                                    |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

  - 仍在榜上（\*）

## 影視演出

### 電影

|        |                                                                 |            |
| ------ | --------------------------------------------------------------- | ---------- |
| **年份** | **片名**                                                          | **角色**     |
| 1993年  | 《[廣東五虎之鐵拳無敵孫中山](../Page/廣東五虎之鐵拳無敵孫中山.md "wikilink")》            |            |
| 1994年  | 《[夢差人](../Page/夢差人.md "wikilink")》（台譯：夢幻警探）                     |            |
| 1995年  | 《[香江花月夜](../Page/香江花月夜.md "wikilink")》                          |            |
| 1995年  | 《[熱線追擊](../Page/熱線追擊.md "wikilink")》（台譯：震撼性醜聞）                  |            |
| 1995年  | 《[正牌香蕉俱樂部](../Page/正牌香蕉俱樂部.md "wikilink")》                      |            |
| 1996年  | 《[四個32A和一個香蕉少年](../Page/四個32A和一個香蕉少年.md "wikilink")》（台譯：小蜜桃大騷包） |            |
| 1997年  | 《[喜氣逼人](../Page/喜氣逼人.md "wikilink")》                            |            |
| 2000年  | 《[砵蘭街皇后](../Page/砵蘭街皇后.md "wikilink")》（台譯：哭泣的天空）                | |-2000年 站壁 |
| 2001年  | 《[情迷夢幻號](../Page/情迷夢幻號.md "wikilink")》                          |            |
| 2012年  | 《[春嬌與志明](../Page/春嬌與志明.md "wikilink")》                          |            |
| 2012年  | 《[起勢搖滾](../Page/起勢搖滾.md "wikilink")》                            |            |
| 2013年  | 《[控制](../Page/控制.md "wikilink")》                                |            |

### 配音

|        |                                      |        |
| ------ | ------------------------------------ | ------ |
| **年份** | **片名**                               | **角色** |
| 1997年  | 《[小倩](../Page/小倩.md "wikilink")》（國語） | 小蘭     |
|        |                                      |        |

### 電視劇

|        |                                    |                                                    |        |
| ------ | ---------------------------------- | -------------------------------------------------- | ------ |
| **年份** | **電視台**                            | **劇名**                                             | **角色** |
| 1996年  | [香港電台](../Page/香港電台.md "wikilink") | 《[香江歲月](../Page/香江歲月.md "wikilink")》               |        |
| 1998年  | [亞洲電視](../Page/亞洲電視.md "wikilink") | 《[我來自廣州](../Page/我來自廣州.md "wikilink")》             |        |
| 2001年  | [CCTV](../Page/CCTV.md "wikilink") | 《[都是愛情惹的禍](../Page/都是愛情惹的禍_\(電視劇\).md "wikilink")》 |        |
| 2012年  | 香港電台                               | 《[火速救兵II](../Page/火速救兵II.md "wikilink")》           | June   |
| 2015年  | 香港電台                               | 《[獅子山下2015](../Page/獅子山下.md "wikilink")》（第一集：一場飯局） | Elaine |
|        |                                    |                                                    |        |

### 嘉賓

  - 2012年9月19日：[SS小燕之夜](../Page/SS小燕之夜.md "wikilink")（女孩\!
    妳知道妳的禮儀價值千萬嗎\!?\!\!）
  - 2015年：[今晚睇李](../Page/今晚睇李.md "wikilink")
  - 2017年6月14日、8月24日（錄影）：[流行經典50年](../Page/流行經典50年.md "wikilink")

## 節目主持

|        |                                    |                                          |
| ------ | ---------------------------------- | ---------------------------------------- |
| **年份** | **電視台**                            | **節目**                                   |
| 2002年  | [TVB8](../Page/TVB8.md "wikilink") | 《[星光剪影](../Page/星光剪影.md "wikilink")》     |
| 2004年  | [TVB8](../Page/TVB8.md "wikilink") | 《[談談情唱唱歌](../Page/談談情唱唱歌.md "wikilink")》 |
| 2004年  | [TVB8](../Page/TVB8.md "wikilink") | 《[娛樂最前線](../Page/娛樂最前線.md "wikilink")》   |

## 獎項

|                                 |                                            |                                                                      |
| ------------------------------- | ------------------------------------------ | -------------------------------------------------------------------- |
| **年份**                          | **獎項**                                     | 備註                                                                   |
| 1993年                           | 香港電台十大中文金曲頒獎典禮－最有前途新人獎金獎                   |                                                                      |
| 商業電台叱咤樂壇流行榜頒獎典禮－叱咤樂壇新力軍女歌手金獎    | <small>上榜歌曲：在你走的一天、難忘初戀、別問我是誰、不要慰問</small> |                                                                      |
| 無視電視十大勁歌金曲頒獎典禮－最受歡迎新人獎銀獎        |                                            |                                                                      |
| 台灣大成報最有前途新人                     |                                            |                                                                      |
| 1994年                           | 福建電台十大流行曲《一生痴戀》                            |                                                                      |
| 1995年                           | 無視電視兒歌金曲頒獎典禮－十大兒歌金曲獎《美少女戰士》                | 與[周慧敏](../Page/周慧敏.md "wikilink")、[湯寶如合唱](../Page/湯寶如.md "wikilink") |
| 無視電視兒歌金曲頒獎典禮－十大兒歌金曲獎《生命有價》      |                                            |                                                                      |
| 1999年                           | 中國大陸七省市MTV總冠軍「馨平氣和」                        |                                                                      |
| 中國廣州十大金曲－《愛不了忘不了》（電視劇《雪花神劍》主題曲） |                                            |                                                                      |
| 大眾電視金鷹獎世紀回眸港台最佳藝人獎              |                                            |                                                                      |
| 2012年                           | 2012年度YAHOO\!搜尋人氣大獎－網上熱爆電影歌曲《別問我是誰》        |                                                                      |
|                                 |                                            |                                                                      |

## 參考文獻

## 外部連結

  -
  - [王馨平的馨聞台](http://mypaper.pchome.com.tw/news/sexyghost)

  - [重新做人　王馨平](http://ericlow-momentsoflifesblog.blogspot.com/2010/05/blog-post_9262.html)

  -
[](../Category/动态列表.md "wikilink") [Hing](../Category/王姓.md "wikilink")
[Category:上海裔台灣人](../Category/上海裔台灣人.md "wikilink")
[Category:香港裔台灣人](../Category/香港裔台灣人.md "wikilink")
[Category:台灣裔香港人](../Category/台灣裔香港人.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:香港瑜伽教師](../Category/香港瑜伽教師.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[W王](../Category/國立政治大學校友.md "wikilink")
[Category:國立臺灣師範大學附屬高級中學校友](../Category/國立臺灣師範大學附屬高級中學校友.md "wikilink")
[Category:臺北市私立再興高級中學校友](../Category/臺北市私立再興高級中學校友.md "wikilink")

1.  [蘋果日報](../Page/蘋果日報.md "wikilink") -
    [寶麗金七小花情路崎嶇](http://hk.apple.nextmedia.com/entertainment/art/20140116/18594172)
    蘋果日報 香港 2014年1月16日
2.  [王馨平的微博](http://weibo.com/u/1868352002?topnav=1&wvr=5&topsug=1)
3.  《喜見父親王羽康復 王馨平 為慈善不收歌酬》，《[新報](../Page/新報.md "wikilink")》，2014年1月11日
4.  《Gin Lee 實力強 王馨平唔輸得》，《[明報](../Page/明報.md "wikilink")》，2014年1月11日