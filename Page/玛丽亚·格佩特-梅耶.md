[Maria_Goeppert-Mayer_(3321963421).jpg](https://zh.wikipedia.org/wiki/File:Maria_Goeppert-Mayer_\(3321963421\).jpg "fig:Maria_Goeppert-Mayer_(3321963421).jpg")

**玛丽亚·格佩特-梅耶**（，），[德裔](../Page/德国.md "wikilink")[美国物理学家](../Page/美国.md "wikilink")。1963年因提出[原子核殼層模型而獲得](../Page/原子核殼層模型.md "wikilink")[諾貝爾物理獎](../Page/諾貝爾物理獎.md "wikilink")。是繼[瑪麗·居里之後第二位拿到此獎的女性](../Page/瑪麗·居里.md "wikilink")。

## 生平

瑪麗亞·格佩特-梅耶出生於[德意志帝國統治下的](../Page/德意志帝國.md "wikilink")[普鲁士王国](../Page/普鲁士王国.md "wikilink")[西里西亚省的](../Page/西里西亚省.md "wikilink")[卡托維茲](../Page/卡托維茲.md "wikilink")（現屬於[波蘭](../Page/波蘭.md "wikilink")）。

1910年，由於其父弗里德里希被任命為[哥廷根大學的兒科教授而舉家遷往](../Page/哥廷根大學.md "wikilink")[哥廷根居住](../Page/哥廷根.md "wikilink")，格佩特-梅耶是她父親的家族中的第七代教授。在小時候，她的生長環境裡就充滿了大學裡的學生、教授、學者，其中甚至包括了未來的諾貝爾獎得主[恩里科·費米](../Page/恩里科·費米.md "wikilink")、[維爾納·海森堡](../Page/維爾納·海森堡.md "wikilink")、[保羅·狄拉克和](../Page/保羅·狄拉克.md "wikilink")[沃爾夫岡·包立](../Page/沃爾夫岡·包立.md "wikilink")。

1924年，她通過了畢業考得以進入大學就讀，在秋天成為了哥廷根大學的一份子。在那兒她向[馬克斯·玻恩](../Page/馬克斯·玻恩.md "wikilink")、[詹姆斯·法蘭克和](../Page/詹姆斯·法蘭克.md "wikilink")[阿道夫·奧托·賴因霍爾德·溫道斯等未來的諾貝爾獎得主學習](../Page/阿道夫·奧托·賴因霍爾德·溫道斯.md "wikilink")。

1930年她完成了博士學位，並在同年嫁給了詹姆斯·法蘭克的助手[約瑟夫·愛德華·梅耶博士](../Page/約瑟夫·愛德華·梅耶.md "wikilink")。之後這對夫婦移居梅耶的祖國美國。

未來的幾年裡，格佩特-梅耶在她丈夫任職的學校擔任非正式或志願的職務，1931年到1939年在[約翰·霍普金斯大學](../Page/約翰·霍普金斯大學.md "wikilink")、1940年到1946年在[哥倫比亞大學](../Page/哥倫比亞大學.md "wikilink")，之後則是在[芝加哥大學](../Page/芝加哥大學.md "wikilink")。這段期間，格佩特-梅耶之所以無法取得職位部分是因為[性別歧視以及嚴格避免](../Page/性別歧視主義.md "wikilink")[裙帶關係的規定](../Page/裙帶關係.md "wikilink")。儘管如此，她還是找到了其他機會，包括[薩拉勞倫斯學院的教職](../Page/薩拉勞倫斯學院.md "wikilink")、哥倫比亞大學研究計畫和[洛斯阿拉莫斯國家實驗室的研究職位](../Page/洛斯阿拉莫斯國家實驗室.md "wikilink")。

在她丈夫任職芝加哥大學的期間，格佩特-梅耶成為了物理系的志願副教授。此外，鄰近的[阿貢國家實驗室於](../Page/阿貢國家實驗室.md "wikilink")1946年7月1日成立，格佩特-梅耶取得了理論物理組的兼職工作。這段時間，她發展了解釋原子核殼層結構的數學模型。由於這個研究成果，她與[約翰內斯·延森](../Page/約翰內斯·延森.md "wikilink")、[尤金·維格納共同獲得](../Page/尤金·維格納.md "wikilink")1963年的[諾貝爾物理獎](../Page/諾貝爾物理獎.md "wikilink")。

格佩特-梅耶的模型解釋了「為何特定數量的核子使原子核特別穩定」這個困惑物理學家許久的問題。這些數量被稱作[幻數](../Page/幻數_\(物理學\).md "wikilink")。背離當時已知的知識，她假設原子核就像一系列的封閉殼層，而質子與中子傾向兩個合成一對。這就像地球繞著太陽公轉時同時繞著自轉軸自轉。格佩特-梅耶優雅地描述了這個概念：

在1940年代與1950年代早期，格佩特-梅耶為[愛德華·泰勒研究光學問題](../Page/愛德華·泰勒.md "wikilink")，其研究結果被應用到設計第一顆[氫彈](../Page/氫彈.md "wikilink")。

1960年，格佩特-梅耶到[加州大學聖地牙哥分校擔任教授](../Page/加州大學聖地牙哥分校.md "wikilink")。雖然到任不久後就中風，其後仍繼續教學與研究數年。
\[1\]

1972年，格佩特-梅耶因[心肌梗塞於](../Page/心肌梗塞.md "wikilink")[加州](../Page/加州.md "wikilink")[聖地牙哥過世](../Page/聖地牙哥.md "wikilink")。

## 其他知名工作

瑪麗亞·格佩特-梅耶1931年的博士論文是研究原子的雙光子吸收之可能性。這個現象一直到1960年代[雷射發明後才受到證實](../Page/雷射.md "wikilink")。為紀念他在這個領域的貢獻，雙光子的[吸收截面單位被命名作GM](../Page/吸收截面.md "wikilink")（Goeppert-Mayer）。

## 紀念

格佩特-梅耶死後，[美國物理學會設立了以之命名的獎項](../Page/美國物理學會.md "wikilink")，頒給傑出的年輕女性研究者。對象是所有取得博士學位的女性物理學家。兩間格佩特-梅耶曾經待過的大學也紀念她。[芝加哥大學每年頒獎給傑出年輕女性科學家或工程師](../Page/芝加哥大學.md "wikilink")。[加州大學聖地牙哥分校則每年舉辦瑪麗亞](../Page/加州大學聖地牙哥分校.md "wikilink")·格佩特-梅耶座談會，聚集女性研究者一同討論現代科學。

[金星上的](../Page/金星.md "wikilink")[格佩特-梅耶撞擊坑](../Page/格佩特-梅耶撞擊坑.md "wikilink")（半徑約35公里）也是以之命名。

## 參考文獻

<references/>

## 外部链接

  - [诺贝尔官方网站关于玛丽亚·格佩特-梅耶简介](http://nobelprize.org/nobel_prizes/physics/laureates/1963/mayer-bio.html)

## 參閱

  - [諾貝爾獎女性得主列表](../Page/諾貝爾獎女性得主列表.md "wikilink")

{{-}}

[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:女性諾貝爾獎獲得者](../Category/女性諾貝爾獎獲得者.md "wikilink")
[Category:德國物理學家](../Category/德國物理學家.md "wikilink")
[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:女性物理學家](../Category/女性物理學家.md "wikilink")
[Category:哥倫比亞大學教師](../Category/哥倫比亞大學教師.md "wikilink")
[Category:約翰霍普金斯大學教師](../Category/約翰霍普金斯大學教師.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:歸化美國公民的德國人](../Category/歸化美國公民的德國人.md "wikilink")
[Category:西里西亞人](../Category/西里西亞人.md "wikilink")
[G](../Category/聖地牙哥加州大學教師.md "wikilink")

1.  Sachs, Robert G. "Maria Goeppert Mayer", *Biographical Memoirs*
    **50**（National Academy of Sciences, 1979）.