**玛塔·路易斯公主**（**Princess Märtha
Louise**，），[挪威国王](../Page/挪威国王.md "wikilink")[哈拉尔五世和](../Page/哈拉尔五世.md "wikilink")[宋雅王后的长女](../Page/宋雅王后.md "wikilink")。

## 家庭

2002年5月24日在[特隆海姆尼达罗斯大教堂和阿利](../Page/特隆海姆尼达罗斯大教堂.md "wikilink")·贝恩（Ari
Behn，1972年出生）结婚，翌年4月29日，在奧斯睦大學國家醫院誕下首名女儿[莫德·安吉丽卡·贝恩](../Page/莫德·安吉丽卡·贝恩.md "wikilink")（Maud
Angelica Behn）。2005年4月8日，誕下第二名女儿，取名為Leah Isadora Behn。

## 作品

她同時也是一位作家，在其加入了童話風的歷史繪本作品《為什麼國王一家人不戴皇冠》*Hvorfor de kongelige ikke har
krone på
hodet*，講述其祖父挪威國王兼帆船選手[奧拉夫五世的童年往事](../Page/奥拉夫五世.md "wikilink")。在2004年一出版便受到挪威大小讀者的熱烈喜愛，更成為年度暢銷書。

## 自稱有千里眼能力

2007年7月24日宣佈她具有千里眼的能力，未來想透過傳授與天使對話的方法幫助人們。她說：「當我第一次和馬兒相處時，我開始和天使有了接觸。後來我才了解這份能和天使接觸的天賦有多大價值，我希望能和他人共享。」\[1\]

## 祖先

<center>

</center>

## 參考資料

<div class="references-small">

<references />

</div>

[M](../Category/格呂克斯堡王朝.md "wikilink")
[M](../Category/挪威公主.md "wikilink")
[M](../Category/挪威君主女儿.md "wikilink")
[M](../Category/奧斯陸人.md "wikilink")

1.  [明報，7月25日](http://hk.news.yahoo.com/070725/3/2c8c1.html)