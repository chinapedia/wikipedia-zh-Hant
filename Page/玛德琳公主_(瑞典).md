**玛德琳公主**（，），全名，海爾辛蘭及-{A|耶斯特里克蘭}-女公爵，是[瑞典國王](../Page/瑞典君主.md "wikilink")[卡尔十六世·古斯塔夫及王后](../Page/卡尔十六世·古斯塔夫.md "wikilink")[西爾維婭的第二個女兒](../Page/希爾維亞王后.md "wikilink")，現時為瑞典王位的[第六順序繼承人](../Page/瑞典王位繼承.md "wikilink")。其長姊為瑞典王儲[維多利亞公主](../Page/維多利亞王儲.md "wikilink")，其兄長為韋姆蘭公爵[卡爾·菲利普王子](../Page/卡爾·菲利普王子_\(瑞典\).md "wikilink")。

## 生平與教育

1982年玛德琳公主生於[瑞典](../Page/瑞典.md "wikilink")[卓宁霍姆宫](../Page/卓宁霍姆宫.md "wikilink")，她是[卡尔十六世·古斯塔夫和](../Page/卡尔十六世·古斯塔夫.md "wikilink")[希爾維亞王后的第二名女兒和第三名孩子](../Page/希爾維亞王后.md "wikilink")。

1985至1989年，玛德琳公主於斯德哥爾摩Västerled堂區幼稚園開始接受學前教育。1989年秋，她入讀布魯瑪區的Smedslättsskolan就讀小學。初中時，她就讀於在Carlssons
skola，後來轉到Enskilda Gymnasiet學習，並在那裏完成中學課程。她在2001年於Enskilda高中畢業。

2001年秋，她赴倫敦學習英語。2002年春，她就讀法律學基本課程和考取[歐洲電腦執照](../Page/歐洲電腦執照.md "wikilink")。2003年1月，她入讀[斯德哥尔摩大学學習藝術史](../Page/斯德哥尔摩大学.md "wikilink")。她於3個學期修讀了60個瑞典學分。2004年秋，她在同一間大學學習民族學。她操流利的[瑞典語](../Page/瑞典語.md "wikilink")、[英语](../Page/英语.md "wikilink")、[德语](../Page/德语.md "wikilink")，和中等的[法语](../Page/法语.md "wikilink")。她於2006年1月取得藝術史文學士學位，2007年於[斯德哥尔摩大学修讀兒童心理學](../Page/斯德哥尔摩大学.md "wikilink")。

## 日常生活

玛德琳公主熱愛[騎馬](../Page/马术.md "wikilink")。多年來，她擁有不少屬於她的馬匹（在王家馬槽中飼養），並以Anna
Svensson之名參加過馬術比賽。她喜歡滑雪，也對文化有濃厚興趣，尤其是戲劇、舞蹈和藝術。2001年成年時，在她公爵銜頭海爾辛蘭和耶斯特里克蘭所在的[耶夫勒堡省](../Page/耶夫勒堡省.md "wikilink")，以她名義設立了鼓勵年青人參與騎馬活動的獎學金，「金騎師」等獎項在2001年由馬德琳公主頒發。

## 公益

玛德琳公主是（「我的大日子」）基金會的創始人。2006年，她在[美国](../Page/美国.md "wikilink")[纽约為](../Page/纽约.md "wikilink")[联合国儿童基金会的保護兒童部門工作了](../Page/联合国儿童基金会.md "wikilink")6個月。

## 個人生活

[Prinsessan_Madeleine_&_Christopher_O’Neill_efter_bröllopet.jpg](https://zh.wikipedia.org/wiki/File:Prinsessan_Madeleine_&_Christopher_O’Neill_efter_bröllopet.jpg "fig:Prinsessan_Madeleine_&_Christopher_O’Neill_efter_bröllopet.jpg")
[Madeleine_of_Sweden_&_Christopher_O'Neill-4.jpg](https://zh.wikipedia.org/wiki/File:Madeleine_of_Sweden_&_Christopher_O'Neill-4.jpg "fig:Madeleine_of_Sweden_&_Christopher_O'Neill-4.jpg")
2009年8月11日，王宮宣布玛德琳公主與律師男朋友約拿斯·貝里-{}-斯特倫（1979年生）訂婚\[1\]\[2\]。玛德琳表示他們同年6月初在卡普里島訂婚。同日在[厄蘭島索里登宮舉行訂婚晚宴](../Page/厄兰岛.md "wikilink")。根據憲法，國王須就是項訂婚取得[政府正式同意方生效](../Page/瑞典政府.md "wikilink")\[3\]。同年，王室宣布貝里-{}-斯特倫婚後將會獲冊封為海爾辛蘭及耶斯特里克蘭公爵，與玛德琳公主的銜頭相稱。

婚禮原訂於2010年下半年舉行，但因「短期內發生的很多事」而延後，其中包括其姐維多利亞女王儲6月的婚禮。雖然外界對馬德琳公主的關係有所揣測，王后在2010年4月表示他們的關係良好，所有事情均正常\[4\]。然而，玛德琳公主於2010年4月宣布取消訂婚，婚禮不會舉行\[5\]。

與貝里-{}-斯特倫分手後，馬德琳移居[纽约](../Page/纽约.md "wikilink")，為其母創辦的世界童年基金會工作。2012年10月25日，王宮宣布玛德琳公主與英美雙重國籍金融界人士[克里斯托弗·奧尼爾](../Page/克里斯托弗·奧尼爾.md "wikilink")（1974年生）訂婚\[6\]。

2012年12月23日，王室宣布婚禮將於2013年6月8日在[斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink")[王宮教堂舉行](../Page/斯德哥爾摩王宮.md "wikilink")\[7\]。多個王室的成員出席婚禮。其夫奧尼爾未有加入瑞典國籍，因此沒有獲冊封任何爵位，也不是王室成員。二人婚後定居紐約。

2013年9月3日，王宮宣布玛德琳公主懷有身孕\[8\]\[9\]。2014年2月20日，馬德琳公主於紐約誕下一女：

  - [**萊奧諾尔**·丽丽安·玛丽亚](../Page/萊奧諾公主_\(瑞典\).md "wikilink")（**Leonore**
    Lilian Maria，2014年2月20日-），哥特兰女公爵；\[10\]

2015年6月15日，玛德琳公主於[丹德吕德市丹德呂德医院誕下一子](../Page/丹德吕德市.md "wikilink")：

  - [**尼古拉**·保羅·古斯塔夫](../Page/尼古拉王子_\(瑞典\).md "wikilink")（**Nicolas**
    Paul Gustaf，2015年6月15日-），翁厄曼蘭公爵

2018年3月9日，玛德琳公主於[丹德吕德市丹德呂德医院誕下一女](../Page/丹德吕德市.md "wikilink")：

  - [**艾德莉安**·约瑟芬·雅丽丝](../Page/艾德莉安公主_\(瑞典\).md "wikilink")（**Adrienne**
    Josephine Alice，2018年3月9日-），布莱金厄女公爵

## 參考資料

## 外部連結

  - [王宮─馬德琳公主](http://www.royalcourt.se/royalcourt/theroyalfamily/hrhprincessmadeleine.4.396160511584257f218000839.html)

|-

[M](../Category/瑞典君主女儿.md "wikilink")
[M](../Category/瑞典公主.md "wikilink")
[Category:瑞典女公爵](../Category/瑞典女公爵.md "wikilink")
[M](../Category/斯德哥爾摩大學校友.md "wikilink")
[M](../Category/德國裔瑞典人.md "wikilink")

1.

2.

3.
4.

5.

6.  <http://www.youtube.com/v/b5pcpvp7pEA&fs=1&source=uds/>

7.

8.

9.

10.