**玛乔丽**·**“玛琦”**·**辛普森**（**Marjorie "Marge"
Simpson**），本姓**布维尔**（**Bouvier**），是[美国](../Page/美国.md "wikilink")[动画](../Page/动画.md "wikilink")[电视剧](../Page/电视剧.md "wikilink")《[辛普森一家](../Page/辛普森一家.md "wikilink")》中的一名[虚构角色](../Page/虚构角色.md "wikilink")，由[朱莉·凯夫纳](../Page/朱莉·凯夫纳.md "wikilink")
配音。她是一位善良且极具耐心的角色，是[霍默·辛普森的妻子](../Page/霍默·辛普森.md "wikilink")，[巴特](../Page/巴特·辛普森.md "wikilink")、[莉萨和](../Page/莉萨·辛普森.md "wikilink")[玛吉的母亲](../Page/玛吉·辛普森.md "wikilink")。她最引人注目的特点便是她以一种罕见的高度伫立的[蓝色头发](../Page/发色.md "wikilink")。系列的创造者[马特·格勒宁用自己母亲玛格丽特](../Page/马特·格勒宁.md "wikilink")·“玛琦”·格勒宁的名字来为她命名。
同時也在2009年，辛普森家族播出20周年時，成為首位登上成人雜誌[花花公子封面的卡通角色](../Page/花花公子.md "wikilink")。

## 生平經歷

[Category:辛普森一家角色](../Category/辛普森一家角色.md "wikilink")
[Category:女性美國人動漫角色](../Category/女性美國人動漫角色.md "wikilink")
[Category:虛構法國裔美國人](../Category/虛構法國裔美國人.md "wikilink")