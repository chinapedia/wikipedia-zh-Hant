**玫瑰聖母聖殿主教座堂**，簡稱**玫瑰堂**，俗稱**前金天主堂**\[1\]，是位於[台灣](../Page/台灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[苓雅區的](../Page/苓雅區.md "wikilink")[天主教](../Page/天主教.md "wikilink")[教堂](../Page/教堂.md "wikilink")，坐落於[愛河橋畔](../Page/愛河.md "wikilink")，創始於1859年（[清](../Page/清朝.md "wikilink")[咸豐九年](../Page/咸豐.md "wikilink")），為[天主教會在台灣重新開教以來建立的第一座教堂](../Page/天主教會.md "wikilink")，為現代[台灣天主教會的發源地](../Page/台灣天主教.md "wikilink")，現在亦為[天主教高雄教區之](../Page/天主教高雄教區.md "wikilink")[主教座堂](../Page/主教座堂.md "wikilink")。2001年在[臺灣歷史建築百景票選中榮獲全國第一](../Page/臺灣歷史建築百景列表.md "wikilink")。

## 歷史沿革

  - 1858年：[中國簽訂](../Page/中國.md "wikilink")[天津條約後](../Page/天津條約.md "wikilink")，禁教解除。西班牙屬地[菲律賓的](../Page/菲律賓.md "wikilink")[道明會派遣](../Page/道明會.md "wikilink")[郭德剛](../Page/郭德剛.md "wikilink")（Fernando
    Sainz）及[洪保祿二位神父到台灣傳教](../Page/洪保祿.md "wikilink")。
  - 1859年5月15日：[道明會神父郭德剛和洪保祿](../Page/道明會.md "wikilink")，和四名中國籍傳教員：楊篤、蔡向、嚴超及修生瑞斌，自[廈門起航](../Page/廈門.md "wikilink")，5月18日抵達[打狗港](../Page/打狗港.md "wikilink")。同年12月，以[龍銀六十二圓在當時](../Page/龍銀.md "wikilink")[前金靠海區域的教堂現址購置一筆土地](../Page/前金區.md "wikilink")，並以稻稈茅草簡單的搭蓋了臨時傳教所。
  - 1860年：以「土角磚」改建傳教所並命名為「聖母堂」。
  - 1862年：第一次改建，再以紅磚、[咾咕石及三合土重新改建成聖堂](../Page/咾咕石.md "wikilink")。
  - 1863年：聖母堂完工，自西班牙迎奉聖母像供奉，並更名為「玫瑰聖母堂」。
  - 1874年：船政大臣[沈葆禎奉命為](../Page/沈葆禎.md "wikilink")[欽差大臣到台灣辦理海防兼處理各國商貿海事](../Page/欽差大臣.md "wikilink")，並受當時的神父萬金本堂所託向清廷請奏准予傳教自由。[清穆宗納奏](../Page/清穆宗.md "wikilink")，賜照准「奉旨」和「天主堂」兩塊聖石，命沈葆禎親筆以剞石。
  - 1875年：沈葆禎再度到台灣，送予玫瑰聖母堂及[萬巒](../Page/萬巒鄉.md "wikilink")[萬金天主堂聖石](../Page/萬金聖母聖殿.md "wikilink")。
  - 1928年：[李安斯神父再次籌備改建聖堂](../Page/李安斯.md "wikilink")，隔年正式施工，即今日所見之聖堂。但在拆除舊教堂時，「奉旨」碑被埋於土中。
  - 1948年：道明會士[陳若瑟神父出任天主教](../Page/陳若瑟_\(臺灣\).md "wikilink")[高雄監牧區](../Page/高雄監牧區.md "wikilink")[監牧](../Page/監牧.md "wikilink")，並指定該聖堂為[主教座堂](../Page/主教座堂.md "wikilink")。
  - 1961年3月：高雄監牧區升格為[教區](../Page/天主教高雄教區.md "wikilink")，[鄭天祥神父為第一任主教](../Page/鄭天祥.md "wikilink")，並指定該座堂為主教座堂，為全台灣建築規模最大的主教座堂。
  - 1972年：教友捐獻興建聖母亭挖掘地基時，挖出「奉旨」碑。
  - 1995年：[教廷提升玫瑰聖母堂為](../Page/教廷.md "wikilink")[宗座聖殿](../Page/宗座聖殿.md "wikilink")。7月1日起開始大規模整修。
  - 1998年：舉行落成典禮及冊封聖殿晉陞隆重大典。
  - 2001年：被列入高雄歷史建築十景。
  - 2001年：在[行政院文化建設委員會主辦的全國](../Page/行政院文化建設委員會.md "wikilink")「臺灣歷史建築百景」活動中獲選第一名。

## 建築特色

玫瑰聖母堂為哥德式與羅馬式的尖塔建築，被列為[歷史建築](../Page/台灣歷史建築.md "wikilink")。教堂正面為高聳的尖塔，兩旁另有小型衛塔，正門上懸有[同治敕令的](../Page/同治.md "wikilink")「奉旨」石碑。教台正中央的玫瑰聖母像，由道明會玫瑰會省之會士自[西班牙引進](../Page/西班牙.md "wikilink")，至今已有140年以上的歷史。

[File:前金天主堂.jpg|舊天主堂](File:前金天主堂.jpg%7C舊天主堂)
[File:高雄玫瑰堂.jpg|教堂內景](File:高雄玫瑰堂.jpg%7C教堂內景)
<File:Holy> Rosary Minor Basilica-Cathedral.jpg|教堂祭台
[File:玫瑰聖母主教座堂.JPG|教堂正面全景](File:玫瑰聖母主教座堂.JPG%7C教堂正面全景)

## 彌撒禮儀時間

以下時間以當地時間（[臺灣時間](../Page/國家標準時間.md "wikilink")）為準

#### 彌撒

  - [主日彌撒](../Page/主日.md "wikilink")：

週六 PM 08:00 （臺灣話）
週日
AM 06:00 （臺灣話）
AM 09:00 （普通話）
AM 11:00 （英語）
PM 04:00 （普通話）

  - 平日彌撒：AM 06:30

## 參訪時間

彌撒時間教堂以服務教友為主，非教友可於彌撒時間前後自由參觀教堂，亦可洽詢教堂服務志工。

## 註釋

<references/>

## 外部連結

  - [玫瑰聖母聖殿主教座堂官方網站](http://www.rosary.org.tw/)

  -
  - [【歷史】玫瑰聖母聖殿主教座堂系列二\~發展歷程](http://801.travel-web.com.tw/Show/Style3/Column/c1_Column.asp?GroupId=A100118&SItemId=0131030&ProgramNo=A100118000001&SubjectNo=2251)

  - [歷史文化學習網](https://web.archive.org/web/20071030014532/http://culture.edu.tw/history/smenu_photomenu.php?smenuid=174)

  - [玫瑰聖母堂——高雄市政府文化局](http://heritage.khcc.gov.tw/Heritage.aspx?KeyID=c5f02f5f-84bf-4c9f-8a0a-a5bbd804890e)

  - [玫瑰聖母堂——文化部文化資產局](https://nchdb.boch.gov.tw/assets/advanceSearch/historicalBuilding/20030226000002)

{{-}}

[Category:台灣天主教主教座堂](../Category/台灣天主教主教座堂.md "wikilink")
[Category:台湾宗座圣殿](../Category/台湾宗座圣殿.md "wikilink")
[Category:高雄市教堂](../Category/高雄市教堂.md "wikilink")
[Category:高雄市歷史建築](../Category/高雄市歷史建築.md "wikilink")
[Category:台灣西式建築](../Category/台灣西式建築.md "wikilink")
[Category:台灣日治時期宗教建築](../Category/台灣日治時期宗教建築.md "wikilink")
[Category:苓雅區](../Category/苓雅區.md "wikilink")
[Category:苓雅寮](../Category/苓雅寮.md "wikilink")
[Category:文建會歷史建築百景](../Category/文建會歷史建築百景.md "wikilink")
[Category:高雄市的台灣之最](../Category/高雄市的台灣之最.md "wikilink")
[Category:1860年台灣建立](../Category/1860年台灣建立.md "wikilink")

1.  但實際該教堂上並非位於[前金區](../Page/前金區.md "wikilink")，而是位於[苓雅區與前金區交界之處](../Page/苓雅區.md "wikilink")。