[Regenwurm1.jpg](https://zh.wikipedia.org/wiki/File:Regenwurm1.jpg "fig:Regenwurm1.jpg")\]\]
**环节动物门**（[學名](../Page/學名.md "wikilink")：）是[动物界的一个](../Page/动物界.md "wikilink")[门](../Page/门_\(生物\).md "wikilink")，该门动物为[两侧对称](../Page/两侧对称.md "wikilink")、分节的[裂生体腔动物](../Page/裂生体腔.md "wikilink")，有的具[疣足和](../Page/疣足.md "wikilink")[刚毛](../Page/刚毛.md "wikilink")，多闭管式[循环系统](../Page/循环系统.md "wikilink")、链式[神经系统](../Page/神经系统.md "wikilink")。目前已知的环节动物约有13000[种](../Page/种.md "wikilink")。常见环节动物有：[蚯蚓](../Page/蚯蚓.md "wikilink")、[蚂蟥](../Page/蚂蟥.md "wikilink")（又称[水蛭](../Page/水蛭.md "wikilink")）、[沙蚕等](../Page/沙蚕.md "wikilink")。

环节动物和节肢动物共同构成[关节动物](../Page/关节动物.md "wikilink")（Articulata）。

## 身体构造

环节动物的消化道由[口](../Page/口.md "wikilink")，[咽](../Page/咽.md "wikilink")，[食道](../Page/食道.md "wikilink")，[腺囊](../Page/腺囊.md "wikilink")，肌肉[胃](../Page/胃.md "wikilink")，中[肠和](../Page/肠.md "wikilink")[肛门组成](../Page/肛门.md "wikilink")。

循环系统由心脏，腹背两套血管和滋养器官的小型侧边血管组成。身體細長

蚯蚓每一个体节里有两个[后肾管](../Page/后肾管.md "wikilink")，它们负责收集前一体节的体液。体液通过肾孔流到下一体节，途中经过盘曲的肾管，直到膀胱。然后通过肾孔排出体外。终尿里面是含氮的物质。而无机盐则会被肾管上皮[重吸收回到血液](../Page/重吸收.md "wikilink")。为了平衡通过皮肤吸收的水分，蚯蚓的尿液会稀释得很厉害。蚯蚓每天可排出約体重的60%稀薄尿液。后肾管被腹膜“绑”着固定于体腔壁上。

环节动物的繁殖能力非常强：体节的分离和出芽生殖的无性生殖。但有性生殖也很重要。多毛纲的动物大多是雌雄异体。而[原环虫纲则是雌雄同体](../Page/原环虫纲.md "wikilink")。

## 分类

  - 通常被分为三个纲：

<!-- end list -->

  - [多毛纲](../Page/多毛纲.md "wikilink")（Polychaeta）、[寡毛纲](../Page/寡毛纲.md "wikilink")（Oligochaeta）、[蛭纲](../Page/蛭纲.md "wikilink")（Hirudinea）

<!-- end list -->

  - 亦有学者将之分为五个纲：[多毛纲](../Page/多毛纲.md "wikilink")、[環帶綱](../Page/環帶綱.md "wikilink")（Clitellata）、[吸口虫纲](../Page/吸口虫纲.md "wikilink")（Acanthobdellida）、[寡毛纲](../Page/寡毛纲.md "wikilink")、[蛭纲](../Page/蛭纲.md "wikilink")

见：[环节动物分类表](../Page/环节动物分类表.md "wikilink")

[Category:动物](../Category/动物.md "wikilink")
[Category:无脊椎动物](../Category/无脊椎动物.md "wikilink")
[Category:冠輪動物](../Category/冠輪動物.md "wikilink")
[\*](../Category/环节动物门.md "wikilink")