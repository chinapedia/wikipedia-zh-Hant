**环花草科**，也被翻译为[巴拿马草科](../Page/巴拿马草科.md "wikilink")，共有11-12[属约](../Page/属.md "wikilink")180余[种](../Page/种.md "wikilink")，全部产于[美洲](../Page/美洲.md "wikilink")[热带地区](../Page/热带.md "wikilink")，有多年生[草本也有](../Page/草本.md "wikilink")[灌木](../Page/灌木.md "wikilink")，一般无[茎或具短茎](../Page/茎.md "wikilink")，稀为[藤本或附生](../Page/藤本.md "wikilink")[植物](../Page/植物.md "wikilink")；[叶有叶柄和扇形的叶片](../Page/叶.md "wikilink")；[花单性](../Page/花.md "wikilink")，雌雄同株。

其中[巴拿马草属的巴拿马草叶子是编制著名的巴拿马草帽的原料](../Page/巴拿马草属.md "wikilink")。有的品种也作为观赏植物被其他地区引种。

1981年的[克朗奎斯特分类法单独设立一个单科的](../Page/克朗奎斯特分类法.md "wikilink")[环花草目](../Page/环花草目.md "wikilink")，属于[槟榔亚纲](../Page/槟榔亚纲.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG分类法认为应该分在](../Page/APG分类法.md "wikilink")[露兜树目中](../Page/露兜树目.md "wikilink")，2003年经过修订的[APG
II分类法维持原分类](../Page/APG_II分类法.md "wikilink")。

  - 属：

<!-- end list -->

  - ''[Asplundia](../Page/Asplundia.md "wikilink") ''Harling
  - [巴拿马草属](../Page/巴拿马草属.md "wikilink") *Carludovica* Ruiz & Pav.
  - ''[Chorigyne](../Page/Chorigyne.md "wikilink") ''R.Erikss.
  - [环花草属](../Page/环花草属.md "wikilink") *Cyclanthus* Poit.
  - ''[Dianthoveus](../Page/Dianthoveus.md "wikilink") ''Hammel & Wilder
  - ''[Dicranopygium](../Page/Dicranopygium.md "wikilink") ''Harling
  - ''[Evodianthus](../Page/Evodianthus.md "wikilink") ''Oerst.
  - ''[Ludovia](../Page/Ludovia.md "wikilink") ''Brongn.
  - ''[Schultesiophytum](../Page/Schultesiophytum.md "wikilink")
    ''Harling
  - ''[Sphaeradenia](../Page/Sphaeradenia.md "wikilink") ''Harling
  - ''[Stelestylis](../Page/Stelestylis.md "wikilink") ''Drude
  - ''[Thoracocarpus](../Page/Thoracocarpus.md "wikilink") ''Harling

## 外部链接

  - 在[L. Watson和M.J.
    Dallwitz（1992年）《有花植物分科》](http://delta-intkey.com/angio/)中的[环花草科](http://delta-intkey.com/angio/www/cyclanth.htm)
  - [环花草属](http://www2.botany.gu.se/staff/rogeri/cycintro.html)
  - [NCBI分类中的环花草科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=25913&lvl=3&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL分类中的环花草科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Cyclanthaceae)

[category:植物科名](../Page/category:植物科名.md "wikilink")

[\*](../Category/环花草科.md "wikilink")