[Torus.jpg](https://zh.wikipedia.org/wiki/File:Torus.jpg "fig:Torus.jpg")

## 几何

在[几何上](../Page/几何.md "wikilink")，一个**环面**是一个[甜甜圈形状的](../Page/甜甜圈.md "wikilink")[旋转曲面](../Page/旋转曲面.md "wikilink")，由一个[圆绕一个和该圆](../Page/圆.md "wikilink")[共面的一个轴回转所生成](../Page/共面.md "wikilink")。[球面可以视为环面的特殊情况](../Page/球面.md "wikilink")，也就是旋转轴是该圆的[直径时](../Page/直径.md "wikilink")。若转轴和圆不相交，圆面中间有一个洞，就像一个[甜甜圈](../Page/甜甜圈.md "wikilink")，一个[呼啦圈](../Page/呼啦圈.md "wikilink")，或者一个充了气的[轮胎](../Page/轮胎.md "wikilink")。另一个情况，也就是轴是圆的一根[弦的时候](../Page/弦.md "wikilink")，就产生一个挤扁了的球面，就像一个圆的座垫那样。英文*Torus*曾是[拉丁文的这种形状的座垫](../Page/拉丁文.md "wikilink")。

圆环面可以参数式地定义为：

\[x(u, v) =  (R + r\cos{v}) \cos{u} \,\]

\[y(u, v) =  (R + r \cos{v}) \sin{u} \,\]

\[z(u, v) = r \sin{v} \,\]

其中

  -
    *u*, *v* ∈ \[0, 2π\],
    *R*是管子的中心到画面的中心的距离，
    *r*是圆管的半径。

[直角坐标系中的关于z](../Page/直角坐标系.md "wikilink")-[轴方位角对称的环面方程是](../Page/坐标轴.md "wikilink")

\[\left(R - \sqrt{x^2 + y^2}\right)^2 + z^2 = r^2\]

该圆环面的[表面积和内部](../Page/表面积.md "wikilink")[体积如下](../Page/体积.md "wikilink")

\[A = 4\pi^2 Rr = \left( 2\pi r \right) \left( 2 \pi R \right) \,\]

\[V = 2\pi^2R r^2 = \left( \pi r^2 \right) \left( 2\pi R \right). \,\]

根据更一般的定义，环面的[生成元不必是圆](../Page/生成元.md "wikilink")，而可以是[椭圆或任何](../Page/椭圆.md "wikilink")[圆锥曲线](../Page/圆锥曲线.md "wikilink")。

## 拓扑

[torus_cycles.png](https://zh.wikipedia.org/wiki/File:torus_cycles.png "fig:torus_cycles.png")
[Inside-out_torus_(animated,_small).gif](https://zh.wikipedia.org/wiki/File:Inside-out_torus_\(animated,_small\).gif "fig:Inside-out_torus_(animated,_small).gif")

[拓扑学上](../Page/拓扑学.md "wikilink")，一个**环面**是一个定义为两个[圆的](../Page/圆.md "wikilink")[积的闭合](../Page/积拓扑.md "wikilink")[曲面](../Page/曲面.md "wikilink")：*S*<sup>1</sup>
× *S*<sup>1</sup>。
上述曲面，若采用**R**<sup>3</sup>诱导的[相对拓扑](../Page/相对拓扑.md "wikilink")，则[同胚于一个拓扑环面](../Page/同胚.md "wikilink")，只要它不和自己的轴相交。

该环面也可用[欧几里得平面的一个](../Page/欧几里得平面.md "wikilink")[商空间来表述](../Page/商空间.md "wikilink")，这是通过如下的等价关系来完成的

  -
    (*x*,*y*) \~ (*x*+1,*y*) \~ (*x*,*y*+1)

或者等价地说，作为[单位正方形将对边粘合的商空间](../Page/单位正方形.md "wikilink")，表述为[基本多边形](../Page/基本多边形.md "wikilink")
\(ABA^{-1}B^{-1}\)。

环面的[基本群是圆的基本群和自身的](../Page/基本群.md "wikilink")[直积](../Page/直积.md "wikilink")：

\[\pi_1(\mathbb{T}^2) = \pi_1(S^1) \times \pi_1(S^1) \cong \mathbb{Z} \times \mathbb{Z}\]
直观地讲，这意味着一个先绕着环面的“洞”（譬如，沿着某个纬度方向的圆）然后绕着环面“实体”（譬如，沿着特定经度方向的圆）的闭[路径可以变形成为先绕实体后绕空心的路径](../Page/路径_\(拓扑学\).md "wikilink")。所以，严格的经度方向和严格的纬度方向的路径是可交换的。这可以想象成为两个鞋带互相穿过然后解开再系上。

环面的第一[同调群和基本群](../Page/同调群.md "wikilink")[同构](../Page/同构.md "wikilink")（因为基本群是[交换群](../Page/交换群.md "wikilink")）。

## *n*维环面

环面很容易推广到任意维。***n*维环面**可以定义为*n*个圆的乘积：

\[\mathbb{T}^n = S^1 \times S^1 \times \cdots \times S^1\]
上面所述的环面就是2维环面。1维环面就是圆。3维环面很难描述。和2维环面一样，*n*维环面可以表述为**R**<sup>*n*</sup>在各个坐标方向整数平移下的商空间。也即，*n*维环面是**R**<sup>*n*</sup>模(modulo)整数[格点](../Page/格点.md "wikilink")**Z**<sup>*n*</sup>的[群作用](../Page/群作用.md "wikilink")（该作用就是向量和）。等价地说，*n*维环面是*n*维[立方体把相对的面两两粘合起来得到的空间](../Page/立方体.md "wikilink")。

*n*维环是*n*维[紧致](../Page/紧致空间.md "wikilink")[流形的一个例子](../Page/流形.md "wikilink")。它也是紧致[可交换](../Page/可交换.md "wikilink")[李群的一个例子](../Page/李群.md "wikilink")。这是因为[单位圆是一个紧致可交换李群](../Page/单位圆.md "wikilink")（如果把它作为定义了乘法的单位长度[复数来看](../Page/复数.md "wikilink")）。环面上的群乘法可以定义为各坐标分别相乘。

环面群在紧致李群理论中有重要的作用。部分原因在于所有紧致李群中总是存在一个[极大环面](../Page/极大环面.md "wikilink")；也就是最大可能维度的闭[子群环面](../Page/子群.md "wikilink")。

*n*维环面的[基本群是一个](../Page/基本群.md "wikilink")*n*阶[自由可交换群](../Page/自由可交换群.md "wikilink")。*n*维环面的*k*阶[同调群是](../Page/同调群.md "wikilink")*n*[取](../Page/二项式系数.md "wikilink")*k*阶的自由可交换群。因此可以推出*n*维环面的[欧拉示性数](../Page/欧拉示性数.md "wikilink")
是0。[上同调环](../Page/上同调环.md "wikilink")*H*<sup>·</sup>(**T**<sup>*n*</sup>,**Z**)可以等同为**Z**-[模](../Page/模.md "wikilink")
**Z**<sup>*n*</sup>上的[外代数](../Page/外代数.md "wikilink")，其生成元为*n*非平凡闭链的对偶。

## 环面着色

如果把环面分成若干区域，那么总是可以用最多7种颜色来着色，使得每对相邻区域有不同的颜色。（这和[四色问题不同](../Page/四色问题.md "wikilink")。）在下面的例子中，环面被分为7个区域，两两相邻，说明7色是必须的：[Torus-with-seven-colours.png](https://zh.wikipedia.org/wiki/File:Torus-with-seven-colours.png "fig:Torus-with-seven-colours.png")

## 参見

  - [代数环面](../Page/代数环面.md "wikilink")
  - [圆环](../Page/圆环.md "wikilink")
  - [環圈](../Page/環圈.md "wikilink")
  - [椭圆曲线](../Page/椭圆曲线.md "wikilink")
  - [极大环面](../Page/极大环面.md "wikilink")
  - [周期格](../Page/周期格.md "wikilink")
  - [球面](../Page/球面.md "wikilink")
  - [曲面](../Page/曲面.md "wikilink")
  - [环体](../Page/环体.md "wikilink")
  - [环面 (原子物理)](../Page/环面_\(原子物理\).md "wikilink")
  - [Villarceau圆](../Page/Villarceau圆.md "wikilink")
  - [环面曲线](../Page/环面曲线.md "wikilink")

## 外部链接

  - [环面的建立](http://www.cut-the-knot.org/shortcut.shtml#torus) 位于网站
  - [更多环的图像](http://www.mathsisfun.com/geometry/torus.html) *(位于
    [趣味数学](http://www.mathsisfun.com/))*
  - Eric W. Weisstein. "环面." 位于MathWorld--Wolfram网络资源。
    <http://mathworld.wolfram.com/Torus.html>
  - [气泡圈的图像和视频](https://web.archive.org/web/20061011195647/http://www.bubblerings.com/bubblerings/media.cfm)
    位于David Whiteis的[BubbleRings.com](http://www.bubblerings.com)

[Category:曲面](../Category/曲面.md "wikilink")