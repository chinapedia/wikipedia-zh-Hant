《**现代启示录**》（）是一部1979年的電影，由[美国導演](../Page/美国.md "wikilink")[弗朗西斯·科波拉執導](../Page/弗朗西斯·科波拉.md "wikilink")，根据波蘭裔[英国](../Page/英国.md "wikilink")[作家](../Page/作家.md "wikilink")[约瑟夫·康拉德的中篇小说](../Page/约瑟夫·康拉德.md "wikilink")《[黑暗之心](../Page/黑暗之心.md "wikilink")》改编，但背景設置在[越戰時期的越南](../Page/越戰.md "wikilink")。內容讲述戰爭期间，一名[美國陸军特种部队军官奉命去刺杀另外一名叛逃並據地為王的美軍上校](../Page/美國陸军特种部队.md "wikilink")。通过他在途中的种种经历，深刻地揭露[战争对人性的摧残和人性最深層的恐懼](../Page/战争.md "wikilink")，並反映原始叢林文化和美軍的戰爭行為，對文明是否真有進步做出質疑。

片中[第1騎兵師搭乘](../Page/美國第1騎兵師.md "wikilink")[UH-1直升機](../Page/UH-1.md "wikilink")，進行空中機動作戰的片段，導演法蘭西斯·柯波拉匠心獨具地配上了[華格納歌劇配樂](../Page/華格納.md "wikilink")「[女武神的飛行](../Page/女武神的飛行.md "wikilink")」，展現出了軍事行動的磅礡氣勢，卻又同時襯托出了畫面上戰爭進行的殘酷血腥，這也成了影史上的經典片段。

影片獲得[坎城影展](../Page/坎城影展.md "wikilink")[金棕櫚獎等獎項](../Page/金棕櫚獎.md "wikilink")。2001年推出了该片的加长版《现代启示录重生版》（*Apocalypse
Now Redux*），全长202分钟。

## 劇情

1968年，特種部隊上尉班傑明·韋勒（Benjamin L. Willard）奉命前往越南「制裁」已失去控制的上校華特·寇茲（Walter E.
Kurtz）。寇茲已不受上級指揮，在中立的[柬埔寨指揮自己的部隊](../Page/柬埔寨.md "wikilink")，並被土著當作半神崇拜。在溯游而上的過程中，韋勒見證美軍使用各種激烈殘暴的行為來殺害人類，讓他不禁開始思考所謂文明的意義。一日，韋勒得知六個月之前有人被指派相同的任務前往刺殺寇茲，但是失敗；上級說他自殺，後來韋勒發現他實際上加入了寇茲。最後在叢林深處，韋勒終於找到寇茲；片末韋勒用[大砍刀殺死寇茲](../Page/大砍刀.md "wikilink")，最後在土著的注視及膜拜下帶著寇茲留下的紀錄與最後一名船員蘭斯一同乘船離開。

本片根據小說《[黑暗之心](../Page/黑暗之心.md "wikilink")》改編，并受到研究巫术与宗教关系问题的著作《[金枝](../Page/金枝:_巫术与宗教之研究.md "wikilink")》的影响，大意是奉命消滅邪惡力量的英雄，過程中受到恐懼、暴力、仇恨的摧殘腐化；在電影《[星際大戰](../Page/星際大戰三部曲：西斯大帝的復仇.md "wikilink")》與遊戲《[暗黑破壞神](../Page/暗黑破壞神_\(遊戲\).md "wikilink")》都有類似的情節。

## 角色

  - [馬丁·辛](../Page/馬丁·辛.md "wikilink") 飾演
    主角班傑明·韋勒[上尉](../Page/上尉.md "wikilink")
  - [馬龍·白蘭度](../Page/馬龍·白蘭度.md "wikilink") 飾演
    華特·寇茲[上校](../Page/上校.md "wikilink")
  - [勞勃·杜瓦](../Page/勞勃·杜瓦.md "wikilink") 飾演
    比爾·吉爾戈[中校](../Page/中校.md "wikilink")

## 製作

1976年3月1日，剧组飞往[菲律宾](../Page/菲律宾.md "wikilink")[马尼拉开始了](../Page/马尼拉.md "wikilink")5个月的拍摄。

## 獎項

  - 奥斯卡奖

<!-- end list -->

  - **最佳摄影奖**-Vittorio Storaro
  - **最佳音效奖**-Walter Murch, Mark Berger, Richard Beggs, Nathan Boxer
  - 最佳影片提名
  - 最佳导演提名
  - 最佳改编剧本提名
  - 最佳男配角提名-Robert Duvall
  - 最佳艺术指导提名-Dean Tavoularis, Angelo P. Graham 和 George R. Nelson
  - 最佳剪接提名

<!-- end list -->

  - 金球奖

<!-- end list -->

  - 最佳剧情片提名
  - **最佳导演奖**
  - **最佳男配角奖**-Robert Duvall
  - **最佳配乐奖**-Carmine Coppola 和 Francis Ford Coppola

<!-- end list -->

  - 英国电影学院奖

<!-- end list -->

  - **最佳导演奖**
  - **最佳男配角奖**-Robert Duvall
  - 最佳配乐提名

## 評價

本片目前在[烂番茄网站的好评是](../Page/烂番茄.md "wikilink")97%，平均分为8.9。

## 荣誉

[美国电影学会荣誉](../Page/美国电影学会.md "wikilink")：

  - [AFI百年百大电影](../Page/AFI百年百大电影.md "wikilink") - \#28
  - [AFI百年百大电影台词](../Page/AFI百年百大电影台词.md "wikilink")："I love the smell
    of napalm in the morning." - \#12

2001年被美國國家電影保護局典藏。

## 外部連結

  -

  -

  -

[Category:1979年电影](../Category/1979年电影.md "wikilink")
[Category:1970年代劇情片](../Category/1970年代劇情片.md "wikilink")
[Category:弗朗西斯·科波拉电影](../Category/弗朗西斯·科波拉电影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:美國戰爭劇情片](../Category/美國戰爭劇情片.md "wikilink")
[Category:英国小说改编电影](../Category/英国小说改编电影.md "wikilink")
[Category:菲律宾取景电影](../Category/菲律宾取景电影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:法語電影](../Category/法語電影.md "wikilink")
[Category:越戰电影](../Category/越戰电影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:米拉麥克斯電影](../Category/米拉麥克斯電影.md "wikilink")
[Category:存在主義作品](../Category/存在主義作品.md "wikilink")
[Category:三小时以上電影](../Category/三小时以上電影.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")
[Category:奥斯卡最佳摄影获奖电影](../Category/奥斯卡最佳摄影获奖电影.md "wikilink")
[Category:金球奖最佳导演获奖电影](../Category/金球奖最佳导演获奖电影.md "wikilink")
[Category:金棕櫚獎獲獎電影](../Category/金棕櫚獎獲獎電影.md "wikilink")
[Category:奥斯卡最佳音响效果获奖电影](../Category/奥斯卡最佳音响效果获奖电影.md "wikilink")