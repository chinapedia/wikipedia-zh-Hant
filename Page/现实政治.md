**現實政治**（[德語](../Page/德語.md "wikilink")：）源自[十九世紀德國](../Page/十九世紀.md "wikilink")，由[古代德國](../Page/德國歷史.md "wikilink")（[普魯士](../Page/普魯士.md "wikilink")）[鐵血宰相](../Page/鐵血宰相.md "wikilink")[奧托·馮·俾斯麥所提出](../Page/奧托·馮·俾斯麥.md "wikilink")，當代英文相關討論沿用德文之Realpolitik。

[Bismarck_pickelhaube.jpg](https://zh.wikipedia.org/wiki/File:Bismarck_pickelhaube.jpg "fig:Bismarck_pickelhaube.jpg")

## 主要主張及歷史脈絡

現實政治主張，當政者應以[國家利益做為從事](../Page/國家利益.md "wikilink")[內政](../Page/內政.md "wikilink")[外交的最高考量](../Page/外交.md "wikilink")，而不應該受到當政者的感情、道德倫理觀、理想、甚至[意識形態的左右](../Page/意識形態.md "wikilink")，所有的一切都應為國家利益服務。

然而現實政治從19世紀後期的普魯士[俾斯麥到](../Page/俾斯麥.md "wikilink")20世紀的美國[基辛格](../Page/基辛格.md "wikilink")，雖都以國家利益至上為最高判準，但於國際外交及手段的歷史脈絡上有顯著的不同。

19世紀後期的普魯士，受現實政治主張[戰爭為合理維護國家利益的手段](../Page/戰爭.md "wikilink")，無需為出兵找道德的名號，因此在此思想的指導下，有[德意志統一戰爭](../Page/德意志統一.md "wikilink")，例如[普丹戰爭](../Page/普丹戰爭.md "wikilink")、[普奧戰爭和](../Page/普奧戰爭.md "wikilink")[普法戰爭等](../Page/普法戰爭.md "wikilink")。

20世紀的美國[基辛格](../Page/基辛格.md "wikilink")，則宣稱單純以美國國家利益做考量，企圖在[冷戰意識形態的對立格局](../Page/冷戰.md "wikilink")，主張與[蘇聯的](../Page/蘇聯.md "wikilink")[關係低盪而和](../Page/缓和政策.md "wikilink")[中華人民共和國修好](../Page/中華人民共和國.md "wikilink")，使[美軍得以談判並撤出](../Page/美軍.md "wikilink")[越戰](../Page/越戰.md "wikilink")。此時，所謂的現實政治主張以力量的[均衡為行動指南](../Page/均勢.md "wikilink")。

在美國的脈絡下，現實政治常用來與美國[理想主義](../Page/理想主義.md "wikilink")——特別是強調[民主](../Page/民主.md "wikilink")、[公義](../Page/公義.md "wikilink")、[人權](../Page/人權.md "wikilink")、[和平的](../Page/和平.md "wikilink")——做為對比。

## 評論

普遍認為，[普魯士在此思想指導下完成](../Page/普魯士.md "wikilink")[德意志統一戰爭後稱霸](../Page/德意志統一.md "wikilink")[歐陸](../Page/歐羅巴.md "wikilink")，此外亦導致各國[窮兵黷武](../Page/黷武主義.md "wikilink")，從而加速[第一次世界大戰的到來](../Page/第一次世界大戰.md "wikilink")。

## 參見

  - [尼可羅·馬基亞維利](../Page/尼可羅·馬基亞維利.md "wikilink")
  - [君王論](../Page/君王論.md "wikilink")
  - [霸道](../Page/霸道.md "wikilink")
  - [孫武](../Page/孫武.md "wikilink")
  - [國際關係現實主義](../Page/國際關係現實主義.md "wikilink")
  - [霍布斯](../Page/霍布斯.md "wikilink")
  - [俾斯麥](../Page/俾斯麥.md "wikilink")
  - [克勞塞維茨](../Page/克勞塞維茨.md "wikilink")
  - [民族主義](../Page/民族主義.md "wikilink")
  - [政治哲学](../Page/政治哲学.md "wikilink")
  - [政治](../Page/政治.md "wikilink")[意識形態](../Page/政治意識形態.md "wikilink")

[Category:國際關係](../Category/國際關係.md "wikilink")
[Category:德意志帝国政治](../Category/德意志帝国政治.md "wikilink")
[Category:政治理論](../Category/政治理論.md "wikilink")
[Category:德语词汇或短语](../Category/德语词汇或短语.md "wikilink")