在[固体物理学中](../Page/固体物理学.md "wikilink")，**玻恩-冯·卡门边界条件**（Born-von Karman
boundary
condition，又译“**玻恩-卡曼边界条件**”）是[布拉菲点阵上给定](../Page/布拉菲点阵.md "wikilink")[函数的空间周期性](../Page/函数.md "wikilink")[边界条件](../Page/边界条件.md "wikilink")。该条件常在固体物理学中用于描述理想[晶体的性质](../Page/晶体.md "wikilink")。

玻恩-冯·卡门边界条件可描述为

\[\psi(\mathbf{r}+N_i \mathbf{a}_i)=\psi(\mathbf{r})\],

式中*i* 表示布拉菲点阵的任意维度方向，*a<sub>i</sub>*
为[晶格基](../Page/晶格.md "wikilink")[矢](../Page/矢量.md "wikilink")，*N<sub>i</sub>*
表示任意[整数](../Page/整数.md "wikilink")（假设晶格无限大）。上述定义表明，对于任意平移矢量*T*，均有：

\[\psi(\mathbf{r}+\mathbf{T})=\psi(\mathbf{r})\]

其中：

\[\mathbf{T} = \sum_i N_i \mathbf{a}_i\].

玻恩-冯·卡门边界条件是固体物理学中分析许多晶体性质，如[布拉格衍射和](../Page/布拉格定律.md "wikilink")[带隙结构的重要条件](../Page/电子能带结构.md "wikilink")。将晶体势能函数写成满足该条件的周期函数，并带入[薛定谔方程](../Page/薛定谔方程.md "wikilink")，即得到晶体[能带结构中重要的](../Page/能带结构.md "wikilink")[布洛赫定理的证明](../Page/布洛赫定理.md "wikilink")。

## 参考文献

  - [黄昆原著](../Page/黄昆.md "wikilink")，韩汝琦改编，《固体物理学》，高等教育出版社，北京，1988，ISBN
    7-04-001025-9

  - Ashcroft, Neil W. and Mermin, N. David: *Solid State Physics*,
    Harcourt, Orlando, 1976

  -
[Category:固体物理学](../Category/固体物理学.md "wikilink")
[Category:凝聚体物理学](../Category/凝聚体物理学.md "wikilink")
[Category:邊界條件](../Category/邊界條件.md "wikilink")