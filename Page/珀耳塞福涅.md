**珀耳塞福涅**（、），又譯為**普西芬妮**、**泊瑟芬**。她是[希腊神话中](../Page/希腊神话.md "wikilink")[冥界的王后](../Page/冥界_\(古希臘\).md "wikilink")，主神[宙斯和大地之神](../Page/宙斯.md "wikilink")[-{zh-cn:得墨忒耳;
zh-hk:得茉忒薾;
zh-tw:狄蜜特;}-的女兒](../Page/得墨忒耳.md "wikilink")，冥界之神[黑帝斯的妻子](../Page/黑帝斯.md "wikilink")。珀耳塞福涅越快樂大地的花朵就越綻放，越悲傷大地就越一片荒蕪，花朵漸漸枯萎，後來母親經過太陽神[赫利俄斯的協調](../Page/赫利俄斯.md "wikilink")，及宙斯的出面處理，終於找回珀耳塞福涅，但在黑帝斯的要求下吃了四顆石榴籽，註定每年要回冥界四個月，因此人間才開始有了分明的春夏秋冬四季，宙斯也把她升到天上變為[處女座](../Page/處女座.md "wikilink")。

在[希腊](../Page/希腊.md "wikilink")，方言的不同使珀耳塞福涅的名称也有所不同。[罗马人首次听到她的名字的方言时稱她為普洛塞庇涅](../Page/罗马.md "wikilink")（Proserpine），因此在后来的[罗马神话中她就叫](../Page/罗马神话.md "wikilink")[普洛塞庇娜](../Page/普洛塞庇娜.md "wikilink")（Proserpina）。罗马女神普洛塞庇娜后来成为[文艺复兴的代表形象](../Page/文艺复兴.md "wikilink")。珀耳塞福涅这个名字的意思为“光明的破坏者（light-destroyer）”，[阿提卡人更喜欢用她神秘的别名](../Page/阿提卡.md "wikilink")**科瑞**（**,
**、“少女”之意）来称呼她。珀耳塞福涅与[赫卡忒关系较为密切](../Page/赫卡忒.md "wikilink")，有时二者也会相互混同起来。

在希腊艺术中珀耳塞福涅一般被画为一个身穿[长袍](../Page/长袍.md "wikilink")，抱着收割的稻穗或者麦穗，庄重微笑的妇女。

## 传说

[Rembrandt_-_The_Rape_of_Proserpine_-_Google_Art_Project.jpg](https://zh.wikipedia.org/wiki/File:Rembrandt_-_The_Rape_of_Proserpine_-_Google_Art_Project.jpg "fig:Rembrandt_-_The_Rape_of_Proserpine_-_Google_Art_Project.jpg")/画，1631）\]\]
按照[赫西俄德的](../Page/赫西俄德.md "wikilink")《[神谱](../Page/神谱.md "wikilink")》的叙述，珀耳塞福涅是宙斯和狄蜜特的女兒：“他（指宙斯）来到慷慨的狄蜜特的床上，她生出了白臂的珀耳塞福涅，她被黑帝斯从她母亲身边偷走。”

但是不像其他[奥林匹斯山上的神的儿女](../Page/奥林匹斯山.md "wikilink")，珀耳塞福涅一开始没有固定的地位。据说[赫耳墨斯](../Page/赫耳墨斯.md "wikilink")、[阿瑞斯](../Page/阿瑞斯.md "wikilink")、[阿波罗和](../Page/阿波罗.md "wikilink")[赫菲斯托斯都向她求婚](../Page/赫淮斯托斯.md "wikilink")，但是狄蜜特没有答应他们。狄蜜特把珀耳塞福涅藏到深山里不让她与其他神接触。因此在珀耳塞福涅成为冥-{后}-之前她的生活非常安静。有一天珀耳塞福涅正在与其他[水仙女或者](../Page/水仙女.md "wikilink")[海仙女](../Page/海仙女.md "wikilink")（在[荷马史诗中她与](../Page/荷马史诗.md "wikilink")[雅典娜和](../Page/雅典娜.md "wikilink")[阿提蜜斯](../Page/阿耳忒弥斯.md "wikilink")）一起在[恩纳采花](../Page/恩纳.md "wikilink")，突然黑帝斯从地缝中升出将她拉走。伴随她的仙女由於没有阻止黑帝斯，被狄蜜特变成了[賽蓮](../Page/塞壬.md "wikilink")。狄蜜特失去女儿后非常悲伤，因此大地上万物停止生长（狄蜜特是大地女神）。太阳神[赫利俄斯看到了一切](../Page/赫利俄斯.md "wikilink")，将珀耳塞福涅的下落告诉了得墨忒耳。

最后宙斯无法阻止大地荒芜，因此派遣[荷米斯說服黑帝斯将珀耳塞福涅还给狄蜜特](../Page/赫耳墨斯.md "wikilink")。但[荷米斯到达前黑帝斯说服珀耳塞福涅吃了四颗](../Page/赫耳墨斯.md "wikilink")[石榴籽](../Page/石榴.md "wikilink")，这使珀耳塞福涅每年有四个月的时间須重返冥界。在另一个版本中[阿斯卡拉福斯告诉其他神珀耳塞福涅吃了冥界的石榴籽](../Page/阿斯卡拉福斯.md "wikilink")。每年狄蜜特与她的女儿团聚时大地上万物生长，但在另六个月里珀耳塞福涅返回冥界时地面上则万物枯竭。此外还有一个版本说[赫卡忒营救了珀耳塞福涅](../Page/赫卡忒.md "wikilink")，并成为珀耳塞福涅在冥府的保护者。最早的关于珀耳塞福涅的故事则说珀耳塞福涅本来就是可怕的死亡女神，是冥河女神[斯堤克斯的女儿](../Page/斯堤克斯.md "wikilink")。

有人认为这个故事的根本源泉是古希腊的结婚仪式。古希腊人觉得结婚是新郎将新娘从她娘家抢走了。有人认为对季节的解释是后来添加上去的。

[Orpheus_and_Eurydice_by_Peter_Paul_Rubens.jpg](https://zh.wikipedia.org/wiki/File:Orpheus_and_Eurydice_by_Peter_Paul_Rubens.jpg "fig:Orpheus_and_Eurydice_by_Peter_Paul_Rubens.jpg")/画，1636-1638）\]\]
作为冥-{后}-的珀耳塞福涅只有一次显示了怜悯，她被[俄耳甫斯的歌深深打动](../Page/俄耳甫斯.md "wikilink")，同意俄耳甫斯将他的妻子欧律狄刻带回人间，但是珀耳塞福涅提出了一个苛刻的要求：欧律狄刻必须走在俄耳甫斯的身后，而且在两人到达地面以前俄耳甫斯不准转身看欧律狄一眼。俄耳甫斯同意了，但就在他们即将回到人间时他还是忍不住回头去看他的妻子是否跟在他身后，结果因此永远地失去了他的妻子。

在[阿佛洛狄忒的叙利亚情人](../Page/阿佛洛狄忒.md "wikilink")[阿多尼斯的故事中珀耳塞福涅也出现过](../Page/阿多尼斯.md "wikilink")。阿多尼斯出生后就被阿佛洛狄忒在她的好朋友海伦娜的帮助下绑架了。阿佛洛狄忒被阿多尼斯无限的美深深打动，她求珀耳塞福涅来帮她照看这个美少年，但是珀耳塞福涅也被阿多尼斯的美所打动，因此不肯将他交还给阿佛洛狄忒。最后宙斯或者[卡利俄珀出面调停](../Page/卡利俄珀.md "wikilink")，决定让阿多尼斯每年四个月与阿佛洛狄忒待在一起，四个月与珀耳塞福涅待在一起，剩下的四个月裏他可以自由决定。但是阿多尼斯总是决定与阿佛洛狄忒待在一起，而不愿与冷酷无情的冥-{后}-待在一起。

由于珀耳塞福涅是哈得斯的合法妻子，所以珀耳塞福涅把哈得斯的情妇都踩在脚下，例如哈得斯爱上了一个叫[门塔的仙女时](../Page/门塔.md "wikilink")，珀耳塞福涅就将门塔变成了[薄荷](../Page/薄荷.md "wikilink")。

[忒修斯和](../Page/忒修斯.md "wikilink")[皮里托奧斯兩人曾发誓都要娶到](../Page/皮里托奧斯.md "wikilink")[宙斯的女儿为妻](../Page/宙斯.md "wikilink")。忒修斯选择了[海伦](../Page/海伦.md "wikilink")，两人成功的绑架了她，只等她成人即可成婚；皮里托奧斯选择了珀耳塞福涅，于是两人又出发去冥府。冥王哈得斯假意设宴欢迎他们，等他们一坐下就派出毒蛇紧紧缠住了两人的脚（一说他们的腿被地面长出的石头固定住了）。直到后来，另一位英雄[赫拉克勒斯做第十二项任务来到冥府](../Page/赫拉克勒斯.md "wikilink")，才解救了被困的忒修斯，但是当他试图释放皮里托奧斯时，大地却震动起来，于是皮里托奧斯被永远留在在了冥府，据说雅典人的大腿较细，就是因为忒修斯被解救时腿被夹住扯下了一些皮肉。

## 象征与崇拜

早在[迈锡尼时代](../Page/迈锡尼时代.md "wikilink")，在皮罗斯就有对地狱女神的崇拜。然而珀耳塞福涅这个名字原先并不是来源自希腊语，那就说明她是一位古老的地方性女神，对于她的崇拜在希腊人入侵[巴尔干半岛以前就开始盛行了](../Page/巴尔干半岛.md "wikilink")。直到希腊征服者时代开始，对她的崇拜和对处女神科瑞的崇拜逐渐合二为一。科瑞被尊奉为丰收女神，而且可能最初是与地母神得墨忒耳混为一体的女神。随着希腊宗教的进一步发展，珀耳塞福涅和科瑞合并起来成为了宙斯和得墨忒耳的女儿，不过在整个希腊历史时期内，珀耳塞福涅和得墨忒耳这两位女神始终共享奉祀。作为冥界女神，珀耳塞福涅与她的母亲得墨忒耳仍然有密切关系。在厄琉西斯，对这两位女神的共同崇拜，具有秘密祭典的性质。随着宗教观念的进一步发展，两位丰收女神开始被看作是与[农业密不可分的定居生活的奠基者和立法者](../Page/农业.md "wikilink")。在雅典，人们把得墨忒耳和珀耳塞福涅共同称为“忒斯摩福拉（Thesmophoria,
立法者）”，并且认为她们是婚姻和家庭关系的庇护者。秋天，举行有专门纪念她们的节日—立法女神节。

在[俄耳甫斯教教徒的神秘教义中](../Page/俄耳甫斯教.md "wikilink")，珀耳塞福涅具有独立的意义。这个教义说她是宙斯的妻子，[扎格柔斯的母亲](../Page/扎格柔斯.md "wikilink")，也是开创自然界的女神。她的常用别称有：“娘娘”、“大女神”等。在后来的宗教体系中，她同其他的地母神或地狱神（[盖亚](../Page/盖亚.md "wikilink")、[瑞亚](../Page/瑞亚.md "wikilink")、赫卡忒、[伊西斯](../Page/伊西斯.md "wikilink")）混同起来。在罗马，她的名字叫做普洛塞庇娜，并且同古老的乡村植物女神利柏拉混为一体。罗马人从[意大利南部和](../Page/意大利南部.md "wikilink")[西西里岛的希腊人那里学来了对珀耳塞福涅的崇拜](../Page/西西里岛.md "wikilink")，并把有关她的一切神话都移植到普洛塞庇娜身上。

在古希腊艺术造型中，珀耳塞福涅的形象反映出她的双重性。作为冥国的女统治者，她的形象是一手执火炬，一手持石榴，并同冥王哈得斯在一起的威严而冷酷的冥界王后；作为丰收女神，她的形象是手持谷穗，或者是和宁芙仙女们一同在草地里采花的美丽少女。珀耳塞福涅回到冥府时大地则万物凋零，重返大地和母亲在一起时则万物复苏，是再生与死亡的象征。

人们敬献给冥界女神珀耳塞福涅的供品是黑色的、没有生殖能力的母牛。珀耳塞福涅没有专门祭祀她的神庙。

代表[處女座和](../Page/處女座.md "wikilink")[季節](../Page/季節.md "wikilink")，象徵著死亡與毀滅。一个无辜的少女、少女母亲的沉痛、以及少女的回归。这个故事也常被用来作为古代神话对季节变化来源的典故。但这些只是珀耳塞福涅的一面，对于古希腊人来说珀耳塞福涅还有另一面，就是她成为了冥界的王后，冥王哈得斯的妻子，从以前的天真烂漫瞬间变得冷酷无情，异常可怕。珀耳塞福涅在冥界有着极大的权力，她指挥冥界的妖魔鬼怪，断绝濒死之人同生人之间的最后联系；珀耳塞福涅同时也是一种阴暗魔力的化身，她毫不留情地把一切生命拉进地下黑暗深渊的怀抱，所以人们将[坟墓称为](../Page/坟墓.md "wikilink")“珀耳塞福涅的房间”。古希腊人因此不敢直呼其名，而只愿意称她为“科瑞”（**,
少女）。她的故事也是[厄琉息斯秘密仪式的背景](../Page/厄琉息斯秘密仪式.md "wikilink")，传说这个仪式可以使参加它的人在可怕的珀耳塞福涅眼下与死去的传说中的英雄共餐，从而获得永生。

在希腊，伟大女神典型的呈现出[三位一体的形象](../Page/三位一体.md "wikilink")（[三相女神](../Page/三相女神.md "wikilink")），有关珀耳塞福涅神话的密切关系者，她们是科瑞（少女）、得墨忒耳（母亲）、赫卡忒（老太婆）。同时珀耳塞福涅也是赫卡忒作为三相女神的一个面相：赫卡忒在天国是象征月亮的[塞勒涅](../Page/塞勒涅.md "wikilink")，在大地是象征女猎手的[阿耳忒弥斯](../Page/阿耳忒弥斯.md "wikilink")，在冥界她是象征破坏者的珀耳塞福涅。

## 现代研究

一些现代学者认为珀耳塞福涅的原型比希腊神话要早，她是[石器时代或者](../Page/石器时代.md "wikilink")[米诺斯文明的女神](../Page/米诺斯文明.md "wikilink")。有人甚至将她与[克诺塞斯的](../Page/克诺塞斯.md "wikilink")“迷宫女神”联系到一起。

另一方面越来越多的学者批评所有的上古文化都有一个大地母亲神的假设。

## 家庭

  - 父 宙斯
  - 母 得墨忒耳
  - 兄弟 [伊阿科斯](../Page/伊阿科斯.md "wikilink")（父亲不明，可能是宙斯）
      - 子 [扎格柔斯](../Page/扎格柔斯.md "wikilink")
      - 女 [墨利諾厄](../Page/墨利諾厄.md "wikilink")
  - 夫 [哈得斯](../Page/哈得斯.md "wikilink")
  - 情人 [阿多尼斯](../Page/阿多尼斯.md "wikilink")

## 参考文献

  - 《神话辞典》，（苏联）M·H·鲍特文尼克等，[商务印书馆](../Page/商务印书馆.md "wikilink")，pp.240-241，ISBN
    7-100-02485-4
  - 《希腊罗马神话》，（德）奥托·泽曼，[上海人民出版社](../Page/上海人民出版社.md "wikilink")，pp.179-180，ISBN
    7-208-05449-5
  - 《古希腊罗马神话鉴赏辞典》，晏立农、马淑琴，吉林人民出版社，p.400，ISBN 7-206-04846-3

## 相關條目

  - [處女座](../Page/處女座.md "wikilink")

[P](../Page/分類:克托尼俄斯.md "wikilink") [P](../Page/分類:希臘女神.md "wikilink")
[P](../Page/分類:死亡与再生之神.md "wikilink") [P](../Page/分類:破坏神.md "wikilink")
[P](../Page/分類:丰穰神.md "wikilink")