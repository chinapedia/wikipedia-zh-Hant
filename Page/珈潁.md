**珈潁**（，），原名**陳嘉穎**，前[香港](../Page/香港.md "wikilink")[亞洲電視女藝員](../Page/亞洲電視.md "wikilink")，[謝雪心的女兒](../Page/謝雪心.md "wikilink")，畢業於[香港演藝學院戲劇學院表演系文憑課程](../Page/香港演藝學院.md "wikilink")。首個擔任主持之節目是[香港電台的電視節目](../Page/香港電台.md "wikilink")[都市半日閒](../Page/都市半日閒.md "wikilink")，第一部參演之電視劇為[影城大亨](../Page/影城大亨.md "wikilink")，及後演出多部重頭劇集包括[電視風雲](../Page/電視風雲.md "wikilink")、[親子情未了](../Page/親子情未了.md "wikilink")、[八號巴士站站情以及](../Page/八號巴士站站情.md "wikilink")[情陷夜中環等](../Page/情陷夜中環.md "wikilink")，2009年8月正式離開[香港](../Page/香港.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")。

## 演出作品

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

#### 2000年

  - [影城大亨](../Page/影城大亨.md "wikilink")
  - [電視風雲](../Page/電視風雲.md "wikilink")

#### 2001年

  - [騷東坡](../Page/騷東坡.md "wikilink")
  - [祖先開眼](../Page/祖先開眼.md "wikilink")
  - [非常好警](../Page/非常好警.md "wikilink")

#### 2002年

  - [親子情未了](../Page/親子情未了.md "wikilink")

#### 2003年

  - [萬家燈火](../Page/萬家燈火.md "wikilink")
  - [女俠叮叮噹](../Page/女俠叮叮噹.md "wikilink")
  - [暴風型警](../Page/暴風型警.md "wikilink")

#### 2004年

  - [子是故人來](../Page/子是故人來.md "wikilink")
  - [八號巴士站站情](../Page/八號巴士站站情.md "wikilink")

#### 2005年

  - [美麗傳說2星願](../Page/美麗傳說2星願.md "wikilink")
  - [危險人物之省港梟雄](../Page/危險人物_\(電視劇\).md "wikilink")

#### 2006年

  - [廉政透視](../Page/廉政透視.md "wikilink")
  - [情陷夜中環](../Page/情陷夜中環.md "wikilink")
  - [香港奇案實錄之重裝悍匪](../Page/香港奇案實錄.md "wikilink")
  - [香港奇案實錄之魔鬼天使](../Page/香港奇案實錄.md "wikilink")

#### 2008年

  - [今日法庭](../Page/今日法庭.md "wikilink")
  - [東邊西邊](../Page/東邊西邊.md "wikilink")

### 主持節目（[亞洲電視](../Page/亞洲電視.md "wikilink")）

#### 2000年

  - [騰飛在金秋](../Page/騰飛在金秋.md "wikilink")

#### 2001年

  - [尋找隱世醫術](../Page/尋找隱世醫術.md "wikilink")
  - [駕駛生活新空間](../Page/駕駛生活新空間.md "wikilink")
  - [五十二週年國慶](../Page/五十二週年國慶.md "wikilink")
  - [十大電視廣告頒獎典禮](../Page/十大電視廣告頒獎典禮.md "wikilink")（2001-2005）

#### 2002年

  - [威馬工展迎新禧](../Page/威馬工展迎新禧.md "wikilink")
  - [下午麼麼茶](../Page/下午麼麼茶.md "wikilink")（2002-2004）

#### 2003年

  - [拍住時代走](../Page/拍住時代走.md "wikilink")
  - [529 星光薈萃賀台慶](../Page/529_星光薈萃賀台慶.md "wikilink")
  - [心連心香港再起飛](../Page/心連心香港再起飛.md "wikilink")（直播主持）
  - [下一站尖沙咀](../Page/下一站尖沙咀.md "wikilink")

#### 2004年

  - [至In至潮中日韓](../Page/至In至潮中日韓.md "wikilink")
  - [台灣溫泉之旅](../Page/台灣溫泉之旅.md "wikilink")
  - [奇妙旅程](../Page/奇妙旅程.md "wikilink")
  - [大王駕到](../Page/大王駕到.md "wikilink")
  - [傑志對AC米蘭](../Page/傑志對AC米蘭.md "wikilink")（表演嘉賓）
  - [亞姐親善之旅日本行](../Page/亞姐親善之旅日本行.md "wikilink")
  - [黃霑博士追思會](../Page/黃霑博士追思會.md "wikilink")（現場主持）

#### 2005年

  - [百萬富翁之新春藝員過肥年](../Page/百萬富翁之新春藝員過肥年.md "wikilink")
  - [開心假期泰好玩](../Page/開心假期泰好玩.md "wikilink")（第1、2、7集）
  - [大遊俠](../Page/大遊俠.md "wikilink")
  - [曼聯亞洲之旅–鹿島鹿角對曼聯](../Page/曼聯亞洲之旅–鹿島鹿角對曼聯.md "wikilink")

#### 2006年

  - [夏日活力天下遊](../Page/夏日活力天下遊.md "wikilink")
  - [走進百姓家](../Page/走進百姓家.md "wikilink")
  - [從香港出發](../Page/從香港出發.md "wikilink")（福建）
  - [迪士尼狂歡倒數除夕夜](../Page/迪士尼狂歡倒數除夕夜.md "wikilink")（直播主持）

#### 2007年

  - [豬事八卦蘇民峰](../Page/豬事八卦蘇民峰.md "wikilink")
  - [梨園戲寶慶元宵](../Page/梨園戲寶慶元宵.md "wikilink")
  - [亞視娛樂一週](../Page/亞視娛樂一週.md "wikilink")
  - [美食之最大賞](../Page/美食之最大賞.md "wikilink")
  - [亞洲大地任我行](../Page/亞洲大地任我行.md "wikilink")
  - [回歸十周年系列活動](../Page/回歸十周年系列活動.md "wikilink")

#### 2008年

  - [今日法庭](../Page/今日法庭.md "wikilink")
  - [師奶我最大](../Page/師奶我最大.md "wikilink")

### 主持新聞資訊節目（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 2003年-2008年：[群星情報站](../Page/群星情報站.md "wikilink")
  - 2004年：[新聞故事](../Page/新聞故事.md "wikilink") 歌后梅艷芳
  - 2004年：[送別．梅艷芳](../Page/送別．梅艷芳.md "wikilink")
  - 2006年：[亞洲新視野](../Page/亞洲新視野.md "wikilink")
  - 2007年：[台海和平與戰爭](../Page/台海和平與戰爭.md "wikilink")
  - 2008年：[四川大地震系列](../Page/四川大地震系列.md "wikilink")
  - 2008年：[血脈創造奇蹟](../Page/血脈創造奇蹟.md "wikilink")
  - 2008年：[奧運--五環傳奇](../Page/奧運--五環傳奇.md "wikilink")

### 電視劇（[香港電台](../Page/香港電台.md "wikilink")）

  - 2003年：[賭海迷徒](../Page/賭海迷徒.md "wikilink")
  - 2004年：[春風伴我行](../Page/春風伴我行.md "wikilink")
  - 2005年：[積金創未來](../Page/積金創未來.md "wikilink")
  - 2007年：[一家人](../Page/一家人.md "wikilink")
  - 2007年：[非常平等任務](../Page/非常平等任務.md "wikilink")
  - 2009年：[非常平等任務2009](../Page/非常平等任務2009.md "wikilink")
  - 2011年：[非常平等任務](../Page/非常平等任務.md "wikilink")

### 擔任司儀（節目及活動）

  - 2004年：[佛指舍利祈福大會開幕典禮](../Page/佛指舍利祈福大會開幕典禮.md "wikilink")
  - 2004年：[佛指舍利普照香江](../Page/佛指舍利普照香江.md "wikilink")
  - 2004年：[金猴賀歲迎新春](../Page/金猴賀歲迎新春.md "wikilink")
  - 2005年：[亞洲電視節目巡禮](../Page/亞洲電視節目巡禮.md "wikilink")
  - 2005年：[粵港一家親](../Page/粵港一家親.md "wikilink")
  - 2005年：[粵港奧四洲盃粵曲大賽](../Page/粵港奧四洲盃粵曲大賽.md "wikilink")
  - 2006年：[祥祥狗狗賀新禧](../Page/祥祥狗狗賀新禧.md "wikilink")
  - 2007年：[豬年吉祥賀新春](../Page/豬年吉祥賀新春.md "wikilink")
  - 2007年：[中國電影展 2007 開幕典禮](../Page/中國電影展_2007_開幕典禮.md "wikilink")
  - 2008年：[第四屆中國動漫節](../Page/第四屆中國動漫節.md "wikilink")
  - 2008年：[中藝融和珍寶精品展開幕禮](../Page/中藝融和珍寶精品展開幕禮.md "wikilink")
  - 2008年：[梵樂天人頌](../Page/梵樂天人頌.md "wikilink")

### 廣告

  - 美仙嬌沐浴露\[1\]

## 參考資料

## 外部連結

  - [亞洲電視藝員資訊 -
    珈潁](https://web.archive.org/web/20070526164914/http://app.hkatv.com/info/atvstar/main.php?id=135&atype=)

[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[K嘉穎](../Category/陳姓.md "wikilink")
[Category:香港演艺学院校友](../Category/香港演艺学院校友.md "wikilink")
[Category:香港佛教徒](../Category/香港佛教徒.md "wikilink")

1.  <http://www.southcn.com/ent/xingwen/200211270662.htm>