**珊翠絲**（又名施佩蓮，Pailin Reayon
Sanchez），[香港著名女性評馬人](../Page/香港.md "wikilink")，她有四份之一[西班牙血統](../Page/西班牙.md "wikilink")，是1個中西混血兒。她主要在[有線電視及](../Page/香港有線電視.md "wikilink")[蘋果日報等主持賽馬節目及撰寫馬評專欄](../Page/蘋果日報.md "wikilink")。其丈夫是前華籍騎師[徐貴良](../Page/徐貴良.md "wikilink")，2人育有2名女兒。她亦與友人合資於[油麻地開設](../Page/油麻地.md "wikilink")[京](../Page/北京市.md "wikilink")[川](../Page/四川省.md "wikilink")[滬菜館](../Page/上海市.md "wikilink")。

2007年1月5日，珊翠絲位於[沙田](../Page/沙田.md "wikilink")[大埔道](../Page/大埔道.md "wikilink")[香港賽馬會副練馬師宿舍的家被賊人潛入屋意圖](../Page/香港賽馬會.md "wikilink")[爆竊](../Page/爆竊.md "wikilink")，在她與丈夫徐貴良返回家中時被撞破，其家中並無損失。

2011年，珊翠絲獲香港有線新聞執行董事[趙應春晉升為清談節目](../Page/趙應春.md "wikilink")《Money
Cafe》主持，以取代[胡孟青及](../Page/胡孟青.md "wikilink")[孫柏文兩人的工作](../Page/孫柏文.md "wikilink")\[1\]
。

2010年代開始，她亦積極投身馬主行列，以歡樂群英團體（與王志達合夥）、白雪公主團體（與曾炳威、曾向得父子合夥）\[2\]名義擁有名下馬匹「心好事成」、「堅離地」。

## 資料來源

  - [馬會宿舍當值保安員疑監守自盜
    徐貴良珊翠絲撞破家賊](https://web.archive.org/web/20070129020625/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070106&sec_id=4104&subsec_id=12731&art_id=6690701)
    蘋果日報，2007年1月6日
  - [練馬師馬會宿舍
    險遭保安員爆竊](http://www.mingpaovan.com/htm/News/20070106/HK-gcd1.htm)
    明報，2007年1月6日
  - [飲食人誌：馬評人闖飲食業
    巧創名駒飲品](http://motor.atnext.com/moDspContent.cfm?article_ID='%22L%5B%3BQ%2FK%26%22P%20%20%0A&CAT_ID=18)
    蘋果日報
  - <http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20110817&sec_id=15307&art_id=15531063>

[Category:香港馬評人](../Category/香港馬評人.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")

1.  [有線搞新意思　評馬靚姐講股
    胡孟青怒劈炮-龍鳳大茶樓-Realforum](https://realforum.zkiz.com/thread.php?tid=24120&page=0)
2.  [香港賽馬會：賽馬團體名單](http://member.hkjc.com/member/image/common/list_of_registered_syndiates.pdf)