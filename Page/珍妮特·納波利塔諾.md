**珍妮特·纳波利塔诺**（，），[美國政治家和律师](../Page/美國.md "wikilink")，现是第20任[加州大學校長](../Page/加州大學.md "wikilink")。她曾是第21任[亞利桑那州州長](../Page/亞利桑那州.md "wikilink")，是該州的第三位女性州長。2006年至2007年期間任全國州長聯會的主席。《[時代](../Page/時代_\(雜誌\).md "wikilink")》雜誌曾將她列為全美最傑出的5位州長。2009年起至2013年9月任[奧巴馬內閣中的](../Page/奧巴馬.md "wikilink")[國土安全部長](../Page/美國國土安全部.md "wikilink")。

{{-}}

[Category:美国国土安全部长](../Category/美国国土安全部长.md "wikilink")
[Category:亚利桑那州州长](../Category/亚利桑那州州长.md "wikilink")
[Category:美國女性閣員](../Category/美國女性閣員.md "wikilink")
[Category:美國女性州長](../Category/美國女性州長.md "wikilink")
[Category:美國民主黨州長](../Category/美國民主黨州長.md "wikilink")
[Category:美國民主黨員](../Category/美國民主黨員.md "wikilink")
[Category:美國女性律師](../Category/美國女性律師.md "wikilink")
[Category:美國循道宗教徒](../Category/美國循道宗教徒.md "wikilink")
[Category:聖克拉拉大學校友](../Category/聖克拉拉大學校友.md "wikilink")
[Category:維吉尼亞大學校友](../Category/維吉尼亞大學校友.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:奧地利裔美國人](../Category/奧地利裔美國人.md "wikilink")
[Category:意大利裔美國人](../Category/意大利裔美國人.md "wikilink")
[Category:亞利桑那州人](../Category/亞利桑那州人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")