**珠江客運有限公司**（**Chu Kong Passenger Transport Co.,
Ltd**）是[珠江船務企業(集團)有限公司的全資](../Page/珠江船務.md "wikilink")[附屬機構](../Page/子公司.md "wikilink")，於1985年7月在香港註冊成立，主要業務是代理[廣東省內](../Page/廣東省.md "wikilink")[珠江三角洲內河口岸以及部份沿海口岸來往](../Page/珠江三角洲.md "wikilink")[香港的水路客運航線業務](../Page/香港.md "wikilink")。

現時，該公司與內地港口的聯營公司，合共有35艘[雙體船](../Page/雙體船.md "wikilink")，共有11,252個客位，到達內地18個[港口](../Page/港口.md "wikilink")，是粵港澳地區規模最大的客運船隊之一。

## 航線及船隊

現時，該公司的航線大多以[尖沙咀](../Page/尖沙咀.md "wikilink")[中港碼頭靠泊](../Page/中港碼頭.md "wikilink")，而[珠海線半數以上班次以](../Page/珠海.md "wikilink")[上環](../Page/上環.md "wikilink")[港澳碼頭為終點](../Page/港澳碼頭.md "wikilink")。同時，[海天碼頭也有部分往來珠海](../Page/海天碼頭.md "wikilink")、中山的班次，而虎門及深圳福永線則全部班次皆以海天碼頭為終點。部分航班會採用[北長洲海峽進出香港](../Page/北長洲海峽.md "wikilink")，並沿[港珠澳大橋以南往來粵港](../Page/港珠澳大橋.md "wikilink")，大致與港澳高速船相若；而另一部分航班則會下穿[青馬大橋](../Page/青馬大橋.md "wikilink")、途經[龍鼓水道及沿港珠澳大橋以北往來粵港](../Page/龍鼓水道.md "wikilink")，需時相若。

<table>
<tbody>
<tr class="odd">
<td><p>圖片</p></td>
<td><p>船名<br />
(中文/英文)</p></td>
<td><p>船種</p></td>
<td><p>出廠年份</p></td>
<td><p>總載客量(人)</p></td>
<td><p>航速(節)</p></td>
<td><p>備註</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>東太安<br />
Dong Tai An</p></td>
<td><p>Aluminium Craft 35m</p></td>
<td><p>1993年</p></td>
<td><p>271</p></td>
<td><p>30</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>東區2號<br />
Dong Qu Er Hao</p></td>
<td><p>Aluminium Craft 35m</p></td>
<td><p>1994年中</p></td>
<td><p>270</p></td>
<td><p>30</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>順景<br />
Shun Jing</p></td>
<td><p>Austal 40m</p></td>
<td><p>1994年3月</p></td>
<td><p>335</p></td>
<td><p>34</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>中山<br />
Zhong Shan</p></td>
<td><p>Austal 40m</p></td>
<td><p>1994年11月</p></td>
<td><p>351</p></td>
<td><p>34</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>新鶴山<br />
Xin He Shan</p></td>
<td><p>Austal 40m</p></td>
<td><p>1994年7月</p></td>
<td><p>338</p></td>
<td><p>33.5</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>順德<br />
Shun De</p></td>
<td><p>Austal 40m</p></td>
<td><p>1995年8月</p></td>
<td><p>332</p></td>
<td><p>34</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>海洋<br />
Hai Yang</p></td>
<td><p>Austal 40m</p></td>
<td><p>1995年8月</p></td>
<td><p>338</p></td>
<td><p>33.5</p></td>
<td><p>蓝色干线塗裝，現行走珠海內陸線或珠海往返深圳線。</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:CKS_HaiChi.jpg" title="fig:CKS_HaiChi.jpg">CKS_HaiChi.jpg</a></p></td>
<td><p>海弛<br />
Hai Chi</p></td>
<td><p>Austal 40m</p></td>
<td><p>1997年6月</p></td>
<td><p>338</p></td>
<td><p>34.5</p></td>
<td><p>前名「南華[Nan Hua]」，蓝色干线塗裝</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>太建<br />
Tai Jian</p></td>
<td><p>Austal 40m</p></td>
<td><p>1997年6月</p></td>
<td><p>318</p></td>
<td><p>34.5</p></td>
<td><p>由東莞線轉行中山線。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>南沙陸拾捌<br />
NANSHA No.68</p></td>
<td><p>New Tech 42M</p></td>
<td><p>1998年6月</p></td>
<td><p>385</p></td>
<td><p>48</p></td>
<td><p>由早興船務租用至珠海九洲港線，蓝色干线塗裝</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>惠嘉明星<br />
Hui Jia Ming Xing</p></td>
<td><p>珠海市琛龍船廠<br />
30米沿海高速客船</p></td>
<td><p>2005年5月</p></td>
<td><p>150</p></td>
<td><p>27</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>海舜<br />
Hai Shun</p></td>
<td><p>珠海市琛龍船廠<br />
30.9米沿海高速客船</p></td>
<td><p>2006年10月</p></td>
<td><p>149</p></td>
<td><p>27</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>海喬<br />
Hai Qiao</p></td>
<td><p>珠海顯利船廠<br />
26米鋁合金雙體船</p></td>
<td><p>2009年</p></td>
<td><p>199</p></td>
<td><p>28</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>海鈺<br />
Hai Yu</p></td>
<td><p>宏德Seacat 33m</p></td>
<td><p>2009年7月</p></td>
<td><p>243</p></td>
<td><p>27.5</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>海鈞<br />
Hai Jun</p></td>
<td><p>宏德Seacat 33m</p></td>
<td><p>2009年7月</p></td>
<td><p>243</p></td>
<td><p>27.5</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>海鉅<br />
Hai Ju</p></td>
<td><p>英輝35米鋁合金雙體船</p></td>
<td><p>2012年上半年</p></td>
<td><p>198</p></td>
<td><p>29</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>海堯<br />
Hai Yao</p></td>
<td><p>英輝35米鋁合金雙體船</p></td>
<td><p>2012年上半年</p></td>
<td><p>198</p></td>
<td><p>29</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>海鈺<br />
Hai Yu</p></td>
<td><p>宏德Seacat 33m</p></td>
<td><p>2009年7月</p></td>
<td><p>243</p></td>
<td><p>27.5</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>海鈞<br />
Hai Jun</p></td>
<td><p>宏德Seacat 33m</p></td>
<td><p>2009年7月</p></td>
<td><p>243</p></td>
<td><p>27.5</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>海琴<br />
Hai Qin</p></td>
<td><p>英輝coastal cruiser 288</p></td>
<td><p>2016年1月</p></td>
<td><p>288</p></td>
<td><p>34</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>海璟<br />
Hai Jing</p></td>
<td><p>英輝coastal cruiser 288</p></td>
<td><p>2016年1月</p></td>
<td><p>288</p></td>
<td><p>34</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>惠嘉福星<br />
Hui Jia Fu Xing</p></td>
<td><p>江龍船舶造船<br />
27.6M Catamaran</p></td>
<td><p>2012年11月</p></td>
<td><p>199</p></td>
<td><p>25</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/file:CKS_JinZhuHu.jpg.md" title="wikilink">120px</a></p></td>
<td><p>金珠湖<br />
Jin Zhu Hu</p></td>
<td><p>挪威Brødrene Aa<br />
40.8米碳纖維複合材料雙體船</p></td>
<td><p>2016年12月</p></td>
<td><p>270</p></td>
<td><p>35</p></td>
<td><p>最初行走番禺蓮花山線，其後轉行中山線。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>鎂珠湖<br />
Mei Zhu Hu</p></td>
<td><p>挪威Brødrene Aa<br />
40.8米碳纖維複合材料雙體船</p></td>
<td><p>2017年3月</p></td>
<td><p>270</p></td>
<td><p>35</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>狮子洋7<br />
Shi Zi Yang 7</p></td>
<td><p>廣东宏深船舶<br />
40米鋁合金雙體船</p></td>
<td><p>2017年4月</p></td>
<td><p>199</p></td>
<td><p>30.5</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>狮子洋8<br />
SHI ZI YANG 8</p></td>
<td><p>廣东宏深船舶<br />
40米鋁合金雙體船</p></td>
<td><p>2018年8月</p></td>
<td><p>199</p></td>
<td><p>30.5</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>新海威<br />
XIN HAI WEI</p></td>
<td><p>澳龍船艇<br />
40米鋁合金雙體船</p></td>
<td><p>2018年1月</p></td>
<td><p>338</p></td>
<td><p>33.5</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>新海山<br />
XIN HAI SHAN</p></td>
<td><p>英輝coastal cruiser 288</p></td>
<td><p>2018年1月</p></td>
<td><p>338</p></td>
<td><p>33.5</p></td>
<td><p>蓝色干线塗裝</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>江門<br />
JIANG MEN</p></td>
<td><p>廣东宏深船舶<br />
40米鋁合金雙體船</p></td>
<td><p>2018年8月</p></td>
<td><p>199</p></td>
<td><p>30.5</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>銀珠湖<br />
Yin Zhu Hu</p></td>
<td><p>挪威Brødrene Aa<br />
40.8米碳纖維複合材料雙體船</p></td>
<td><p>2018年9月</p></td>
<td><p>270</p></td>
<td><p>35</p></td>
<td><p>最初行走番禺蓮花山線，其後轉行高明線。轉行高明線後因機件故障需暫停服務，由鈺銖湖頂替。復修後返回行走番禺蓮花山線。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/file:CKS_YuChuHu.jpg.md" title="wikilink">120px</a></p></td>
<td><p>鈺珠湖<br />
Yu Zhu Hu</p></td>
<td><p>挪威Brødrene Aa<br />
40.8米碳纖維複合材料雙體船</p></td>
<td><p>2018年10月</p></td>
<td><p>270</p></td>
<td><p>35</p></td>
<td><p>最初行走番禺蓮花山線，其後轉行高明線頂替待修中的銀珠湖。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/file:CKS_ZhongShan6.jpg.md" title="wikilink">120px</a></p></td>
<td><p>中山6<br />
Zhong Shan 6</p></td>
<td><p>挪威Brødrene Aa<br />
42.8米碳纖維複合材料雙體船</p></td>
<td><p>2018年11月</p></td>
<td><p>300</p></td>
<td><p>40</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>中山20<br />
Zhong Shan 20</p></td>
<td><p>挪威Brødrene Aa<br />
42.8米碳纖維複合材料雙體船</p></td>
<td><p>2019年</p></td>
<td><p>300</p></td>
<td><p>40</p></td>
<td></td>
</tr>
</tbody>
</table>

| align=center width=10% bgcolor= \#D3D3D3| 航線                      | align=center width=20% bgcolor= \#D3D3D3| 內地承辦公司                                                                                                       | align=center width=7% bgcolor= \#D3D3D3| 內地港口 | align=center width= bgcolor= \#D3D3D3| 船名 | align=center width=9% bgcolor= \#D3D3D3| 香港航行需時 | align=center width= bgcolor= \#D3D3D3| 備註                                                                                                                                           |
| ----------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------- | ----------------------------------------- | ----------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [珠海線](../Page/珠海.md "wikilink")                                   | [珠海高速客輪有限公司](../Page/珠海高速客輪.md "wikilink")（[珠海九洲控股与](../Page/珠海九洲控股.md "wikilink")[珠江船务的合资公司](../Page/珠江船務企業\(集團\)有限公司.md "wikilink")、以「九洲藍色海上幹線」品牌營運） | [九洲港](../Page/九洲港.md "wikilink")              | 海弛、海琴、海璟、南沙陸拾捌、新海威、新海山                    | 1小時10分                                          | 九洲港設有汽車客運站，可往來廣東省部分地區；半數以上班次停泊港澳碼頭                                                                                                                                                  |
| [中山線](../Page/中山市.md "wikilink")                                  | 中山市中港客運聯營有限公司                                                                                                                                          | [中山港](../Page/中山港.md "wikilink")              | 中山、金珠湖、太建、中山6、中山20                        | 1小時40分                                          | 另有免費接駁巴士往來小欖、雅居樂新城、台山、開平。由中山6及中山20行走之航班,航行時間為1小時20分。                                                                                                                                |
| [順德線](../Page/順德區.md "wikilink")                                  | 佛山市順德區順港客運聯營有限公司                                                                                                                                       | [順德港](../Page/順德港.md "wikilink")              | 順德、順景                                     | 2小時                                             | 另有免費接駁巴士往來大良、容桂、陳村、樂從、圴安、九江、黃圃、小欖、碧桂園、禪城                                                                                                                                            |
| [番禺線](../Page/番禺.md "wikilink")                                   | 廣州市番禺區蓮花山港客運有限公司                                                                                                                                       | [蓮花山港](../Page/蓮花山_\(番禺\).md "wikilink")      | 銀珠湖、鎂珠湖、宇航伍號                              | 2小時                                             | 另有免費接駁巴士往來番禺各大酒店、[祈福新邨](../Page/祈福新邨.md "wikilink")                                                                                                                                 |
| [番禺](../Page/番禺.md "wikilink")、[南沙合作線](../Page/南沙区.md "wikilink") | [廣州市番禺南沙港客運有限公司](../Page/南沙客运港#运营公司.md "wikilink")                                                                                                     | [南沙港](../Page/南沙客运港.md "wikilink")            | 銀珠湖、鎂珠湖、中山、金珠湖、太建、宇航伍號、鈺珠湖                | 1小時30分                                          | 每天16:15分中港城往南沙港及其回程航班由中山線之船隻行走, 其餘班次則由番禺蓮花山線之船隻以掛港形式提供服務。另有免費接駁巴士往來南沙、番禺各大酒店；2017年3月1日起改由珠江客運代理（可於[南沙客运港站換乘](../Page/南沙客运港站.md "wikilink")[广州地铁4号线](../Page/广州地铁4号线.md "wikilink")） |
| [東莞線](../Page/東莞.md "wikilink")                                   | [東莞市虎門龍威客運有限公司](../Page/狮子洋飞航.md "wikilink")（以「狮子洋飞航」品牌營運）                                                                                             | [虎門港](../Page/虎門港.md "wikilink")              | 獅子洋7,獅子洋8                                 | 1小時20分                                          | 全部班次停泊[海天碼頭](../Page/海天碼頭.md "wikilink")                                                                                                                                            |
| [深圳機場線](../Page/深圳機場.md "wikilink")                               | 深圳機場高速客運有限公司                                                                                                                                           | [福永碼頭](../Page/福永碼頭.md "wikilink")            | 新鶴山                                       | 50分                                             | 全部班次停泊海天碼頭                                                                                                                                                                          |
| [高明線](../Page/高明.md "wikilink")                                   | 佛山市高明區明珠客運聯營有限公司                                                                                                                                       | [高明港](../Page/高明港.md "wikilink")              | 鈺珠湖                                       | 3小時                                             | 2018年12月1日起恢復航班                                                                                                                                                                     |
| [高明](../Page/高明.md "wikilink")、[鶴山合作線](../Page/鶴山.md "wikilink")  | 鶴山市鶴港客運合營有限公司                                                                                                                                          | [鶴山港](../Page/鶴山港.md "wikilink")              | 鈺珠湖                                       | 2小時30分                                          | 2018年12月1日起恢復航班, 由高明線之船隻以掛港形式提供服務。                                                                                                                                                  |
| [江門線](../Page/江門.md "wikilink")                                   | 江門市港澳客運聯營有限公司                                                                                                                                          | [江門港](../Page/江門港.md "wikilink")              | 江門                                        | 3小時                                             | 2018年8月29日復航                                                                                                                                                                        |
| [江門](../Page/江門.md "wikilink")、[斗門合作線](../Page/斗門.md "wikilink")  | 斗門香港客運聯營有限公司                                                                                                                                           | [斗門港](../Page/斗門港.md "wikilink")              | 江門                                        | 2小時10分                                          | 2018年9月1日起恢复航班, 由江門線之船隻以掛港形式提供服務。                                                                                                                                                   |
|                                                                   |                                                                                                                                                        |                                               |                                           |                                                 |                                                                                                                                                                                     |

## 意外

  - 1995年，「海昌」號於馬灣海域與正在前往虎門的「太平」號相撞，「太平」號左邊船身嚴重損毀，隨後退役；而「海昌」號則成功修復，繼續營運。
  - 2009年11月5日晚上7時45分左右，番禺蓮花山港客運有限公司旗下的「三埠」號高速客輪在準備駛近蓮花山港水域時，與一艘運沙船相撞，而運沙船的輸送帶插穿客輪左邊客艙，造成2名荷蘭籍乘客死亡，另有9名乘客受傷。事發後，「三埠」號暫時停止營運，並等待番禺海事局等部門作進一步調查。其後「三埠」號被運到香港維修，並正式改名為「蓮珠湖」，完成搶修工程後於2010年2月13日復出。

## 已退役船隊

  -

<table>
<thead>
<tr class="header">
<th><p>圖片</p></th>
<th><p>船名<br />
(中文/英文)</p></th>
<th><p>船種</p></th>
<th><p>出廠年份</p></th>
<th><p>!退役年份</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>海山<br />
Hai Shan</p></td>
<td><p>Fjellstrand 31.5m</p></td>
<td><p>1984年</p></td>
<td><p>2008年12月</p></td>
<td><p>前名「北秀湖BEI XIU HU」<br />
退役後售予珠海一家遊艇公司，並易名為「紳斯威828」</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>荔灣湖<br />
Li Wan Hu</p></td>
<td><p>Fjellstrand 31.5m</p></td>
<td><p>1984年3月</p></td>
<td><p>待查</p></td>
<td><p>去向未明</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>秀麗湖<br />
Xiu Li Hu</p></td>
<td><p>Fjellstrand 31.5m</p></td>
<td><p>1984年10月</p></td>
<td><p>待查</p></td>
<td><p>去向未明</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>瓊州1號<br />
Qiong ZHou Yi Hao</p></td>
<td><p>Fjellstrand 31.5m</p></td>
<td><p>1984年12月</p></td>
<td><p>待查</p></td>
<td><p>去向未明</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>江華3號<br />
Jiang Hua San Hao</p></td>
<td><p>Fjellstrand 31.5m</p></td>
<td><p>1984年12月</p></td>
<td><p>2011年</p></td>
<td><p>前名「蓮花湖 LIAN HUA HU」，現時去向不明</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>銀星<br />
Yin Xing</p></td>
<td><p>Fjellstrand 31.5m</p></td>
<td><p>1985年3月</p></td>
<td><p>待查</p></td>
<td><p>去向未明</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>海天<br />
Hai Tian</p></td>
<td><p>Fjellstrand 31.5m</p></td>
<td><p>1985年7月</p></td>
<td><p>2010年</p></td>
<td><p>去向未明</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>九洲<br />
Jiu Zhou</p></td>
<td><p>Marinteknik 34m</p></td>
<td><p>1987年</p></td>
<td><p>待查</p></td>
<td><p>去向未明</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>江華二號<br />
Jiang Hua Er Hao</p></td>
<td><p>Marinteknik 35m</p></td>
<td><p>1987年11月</p></td>
<td><p>2008年7月</p></td>
<td><p>2008年7月21日於内伶仃島燒毀，之後不獲維修而直接退役</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>新寧<br />
Xin Ning</p></td>
<td><p>Precision Marine 38M</p></td>
<td><p>1988年</p></td>
<td><p>2009年2月</p></td>
<td><p>2009年2月售予上海，當時主要航行上海南門至寶楊路航線<br />
最終在2013年退役，之後去向不明</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>順風<br />
Shun Feng</p></td>
<td><p>Precision Marine 38M</p></td>
<td><p>1988年12月</p></td>
<td><p>2009年初</p></td>
<td><p>2009年初轉售予土耳其<br />
現於及希臘愛琴海岸島嶼航行</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>通州<br />
Tong Zhou</p></td>
<td><p>Austal 38m</p></td>
<td><p>1990年</p></td>
<td><p>2010年8月</p></td>
<td><p>2010年8月售予南韓</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>振興湖<br />
Zhen Xing Hu</p></td>
<td><p>Wavemaster 34m</p></td>
<td><p>1990年3月</p></td>
<td><p>待查</p></td>
<td><p>現由<a href="../Page/斯洛文尼亞.md" title="wikilink">斯洛文尼亞的Jadrolinija渡輪公司擁有</a>，船名改為SILBA</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>海威<br />
Hai Wei</p></td>
<td><p>Austal 40m</p></td>
<td><p>1990年11月</p></td>
<td><p>2016年</p></td>
<td><p>已售予印尼</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>雙子星<br />
Shuang Zi Xing</p></td>
<td><p>Kvaerner Fjellstrand<br />
Flyingcat 40</p></td>
<td><p>1991年</p></td>
<td><p>2006年8月</p></td>
<td><p>退役後售予廈門並易名為「五緣Wu Yuan」<br />
主要用於航行「小三通」航線</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>太平<br />
Tai Ping</p></td>
<td><p>新加坡科技海事公司<br />
34m Catamaran</p></td>
<td><p>1991年2月</p></td>
<td><p>1995年</p></td>
<td><p>因交通意外，船隻嚴重損毀，之後不獲維修而直接退役</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>五邑湖<br />
Wu Yi Hu</p></td>
<td><p>WaveMaster 39m</p></td>
<td><p>1991年11月</p></td>
<td><p>2016年11月</p></td>
<td><p>2016年末售予泰國皇家船務，並易名為「ROYAL 1」<br />
主要航行來往<a href="../Page/芭堤雅.md" title="wikilink">芭堤雅至</a><a href="../Page/華欣.md" title="wikilink">華欣的航線</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>海濱<br />
Hai Bin</p></td>
<td><p>Austal 40m</p></td>
<td><p>1992年1月</p></td>
<td><p>2017年</p></td>
<td><p>去向未明</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>岐江<br />
Qi Jiang</p></td>
<td><p>Austal 40m</p></td>
<td><p>1992年2月</p></td>
<td><p>2017年2月</p></td>
<td><p>前名「新端州 Xin Duan Zhou」<br />
退役後售予泰國皇家船務，並易名為「ROYAL 2」<br />
主要航行來往<a href="../Page/象岛.md" title="wikilink">象岛</a>-<a href="../Page/芭提雅.md" title="wikilink">芭提雅</a>-<a href="../Page/色桃邑.md" title="wikilink">色桃邑的航線</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>海珠<br />
|Hai Zhu</p></td>
<td><p>Austal 40m</p></td>
<td><p>1992年3月</p></td>
<td><p>2017年3月</p></td>
<td><p>退役後售予泰國皇家船務，並易名為「ROYAL 3」<br />
主要航行來往<a href="../Page/象岛.md" title="wikilink">象岛</a>-<a href="../Page/芭提雅.md" title="wikilink">芭提雅</a>-<a href="../Page/色桃邑.md" title="wikilink">色桃邑的航線</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:CKS_NanGui.jpg" title="fig:CKS_NanGui.jpg">CKS_NanGui.jpg</a></p></td>
<td><p>南桂<br />
Nan Gui</p></td>
<td><p>Austal 40m</p></td>
<td><p>1992年8月</p></td>
<td><p>2017年6月</p></td>
<td><p>去向未明</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>開平<br />
Kai Ping</p></td>
<td><p>Austal 40m</p></td>
<td><p>1992年9月</p></td>
<td><p>2003年2月</p></td>
<td><p>退役後售予台灣並易名為「東方之星 Oriental Star」<br />
主要用於航行「小三通」航線</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>蓮山湖<br />
Lian Shan Hu</p></td>
<td><p>Austal 40m</p></td>
<td><p>1992年11月</p></td>
<td><p>2017年11月</p></td>
<td><p>2008至09年間曾租借予<a href="../Page/金光飛航.md" title="wikilink">金光飛航</a><br />
退役後去向未明</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:CUI_HENG_HU_China_Ferry_Terminal_to_Zhongshan_31-05-2016.jpg" title="fig:CUI_HENG_HU_China_Ferry_Terminal_to_Zhongshan_31-05-2016.jpg">CUI_HENG_HU_China_Ferry_Terminal_to_Zhongshan_31-05-2016.jpg</a></p></td>
<td><p>翠亨湖<br />
Cui Heng Hu</p></td>
<td><p>Austal 40m</p></td>
<td><p>1992年11月</p></td>
<td><p>2017年11月</p></td>
<td><p>退役後去向未明</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:CKS_PengLaiHu.jpg" title="fig:CKS_PengLaiHu.jpg">CKS_PengLaiHu.jpg</a></p></td>
<td><p>蓬萊湖<br />
Peng Lai Hu</p></td>
<td><p>WaveMaster 39m</p></td>
<td><p>1992年12月</p></td>
<td><p>2017年12月29日</p></td>
<td><p>退役後去向未明</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>東區1號<br />
Dong Qu Yi Hao</p></td>
<td><p>Aluminium Craft 28m</p></td>
<td><p>1993年</p></td>
<td><p>2006年6月</p></td>
<td><p>2006年6月19日與新輪85相撞，之後不獲復修而退役</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chu_Kong_Passenger_Transport_catamaran_Hai_Chang_(Hong_Kong).jpg" title="fig:Chu_Kong_Passenger_Transport_catamaran_Hai_Chang_(Hong_Kong).jpg">Chu_Kong_Passenger_Transport_catamaran_Hai_Chang_(Hong_Kong).jpg</a></p></td>
<td><p>海昌<br />
Hai Chang</p></td>
<td><p>Austal 40m</p></td>
<td><p>1993年1月</p></td>
<td><p>2018年2月26日</p></td>
<td><p>退役後去向未明</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chu_Kong_-_Hai_Liang_5084.JPG" title="fig:Chu_Kong_-_Hai_Liang_5084.JPG">Chu_Kong_-_Hai_Liang_5084.JPG</a></p></td>
<td><p>海亮<br />
Hai Liang</p></td>
<td><p>Austal 40m</p></td>
<td><p>1993年1月</p></td>
<td><p>2018年2月26日</p></td>
<td><p>退役後去向未明</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:CKS_XingZhong.jpg" title="fig:CKS_XingZhong.jpg">CKS_XingZhong.jpg</a></p></td>
<td><p>興中<br />
Xing Zhong</p></td>
<td><p>Austal 40m</p></td>
<td><p>1993年4月</p></td>
<td><p>2018年6月</p></td>
<td><p>退役後去向未明</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>台山<br />
Tai Shan</p></td>
<td><p>Austal 40m</p></td>
<td><p>1993年5月</p></td>
<td><p>2018年6月</p></td>
<td><p>退役後去向未明</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>岡州<br />
Gang Zhou</p></td>
<td><p>Austal 40m</p></td>
<td><p>1993年7月</p></td>
<td><p>2009年3月</p></td>
<td><p>2009年3月5日於虎门燒毀，之後不獲維修而直接退役</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>高明<br />
Gao Ming</p></td>
<td><p>Austal 40m</p></td>
<td><p>1993年7月</p></td>
<td><p>2018年8月</p></td>
<td><p>退役後去向未明(現存放青衣)</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>蓮港湖<br />
LIAN GANG HU</p></td>
<td><p>Austal 40m</p></td>
<td><p>1994年1月</p></td>
<td><p>2019年2月11日</p></td>
<td><p>退役後去向未明</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>逸仙湖<br />
YI XIAN HU</p></td>
<td><p>Austal 40m</p></td>
<td><p>1994年1月</p></td>
<td><p>2019年2月17日</p></td>
<td><p>屬於中山線之船隻, 後期行走了一小段時間番禺蓮花山線直至退役。退役後去向未明。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>蓬江<br />
Peng Jiang</p></td>
<td><p>Wavemaster 35M<br />
Low Wash Catamaran</p></td>
<td><p>1993年7月</p></td>
<td><p>2012年5月</p></td>
<td><p>退役後售予廈門一家渡輪公司，主要航行「小三通」航線</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chu_Kong_Passenger_Transport_catamaran_Shun_Shui_(Hong_Kong).jpg" title="fig:Chu_Kong_Passenger_Transport_catamaran_Shun_Shui_(Hong_Kong).jpg">Chu_Kong_Passenger_Transport_catamaran_Shun_Shui_(Hong_Kong).jpg</a></p></td>
<td><p>順水<br />
Shun Shui</p></td>
<td><p>Austal 40m</p></td>
<td><p>1993年第3季</p></td>
<td><p>2018年10月31日</p></td>
<td><p>退役後去向未明(現存順德港 3 號碼頭)</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>蓮珠湖<br />
Lian Zhu Hu</p></td>
<td><p>Austal 40m</p></td>
<td><p>1993年10月</p></td>
<td><p>2012年中旬</p></td>
<td><p>已售予越南並易名為「明港一號」</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>增城1號<br />
Zeng Cheng No 1</p></td>
<td><p>Austal 40m</p></td>
<td><p>1996年6月</p></td>
<td><p>2002年</p></td>
<td><p>未有岀售</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>海琨<br />
Hai Kun</p></td>
<td><p>Austal 40m</p></td>
<td><p>1997年9月</p></td>
<td><p>2016年2月</p></td>
<td><p>退役後售予海南三亞<br />
於2017年1月起航行鹿回头至南山寺航線</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [珠江客運有限公司官方網頁](http://www.cksp.com.hk/)

[Category:中国民营企业](../Category/中国民营企业.md "wikilink")
[Category:广东水路运输](../Category/广东水路运输.md "wikilink")
[Category:中国航运公司](../Category/中国航运公司.md "wikilink")
[Category:香港渡輪公司](../Category/香港渡輪公司.md "wikilink")
[Category:广东省航运集团](../Category/广东省航运集团.md "wikilink")