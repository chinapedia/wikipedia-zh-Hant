**珠海學院**（，簡稱CHCHE）是[香港一所具有學士學位頒授資格的認可專上學院](../Page/香港.md "wikilink")，同時與[台灣](../Page/台灣.md "wikilink")、[中國大陸或海外大學合辦課程](../Page/中國大陸.md "wikilink")，開辦一系列[學士](../Page/學士.md "wikilink")、[碩士](../Page/碩士.md "wikilink")、[博士課程](../Page/博士.md "wikilink")。

因其與[珠海市相稱](../Page/珠海市.md "wikilink")，外界容易將誤會珠海學院與珠海市有所關係。學院於1947年創校並取名珠海，乃冀學生能若「珠」之晶瑩璀璨，如「海」之浩蘊深藏，相對1979年才建市的珠海市更早起用「珠海」命名。因此除名稱相近外，兩者並無任何關係。珠海學院為香港歷史最悠久的私立大專院校\[1\]。

## 歷史沿革

珠海學院最早為[廣東省](../Page/廣東省.md "wikilink")[廣州市的](../Page/廣州.md "wikilink")**私立珠海大學**，創立於1947年，原校址在廣州市[東山區竹絲崗二馬路](../Page/東山區_\(廣州市\).md "wikilink")，由[陳濟棠將軍](../Page/陳濟棠.md "wikilink")、[陳濟棠兄長](../Page/陳濟棠.md "wikilink")[陳維周](../Page/陳維周.md "wikilink")、中國文學家[黃麟書](../Page/黃麟書.md "wikilink")、廣州市長[李揚敬將軍](../Page/李揚敬.md "wikilink")、區芳浦及教育家[江茂森等廣東省籍人士所創辦](../Page/江茂森.md "wikilink")。

1949年因中國大陸政權更迭而遷到香港，由於香港政府初期只承認其所辦理之公營[香港大學頒發學位](../Page/香港大學.md "wikilink")，故私立大專院校受當時香港教育條例所限，故易名為「珠海書院」。早期珠海書院於[中華民國教育部以](../Page/中華民國教育部.md "wikilink")「私立珠海大學」的名義註冊，並由教育部頒授學位，其國際認受性雖然與中華民國大學學位等同，但不獲香港政府認可，因此有收生上的劣勢。

1956年，八所私立大學書院倡議合併，除[廣僑書院](../Page/廣僑書院.md "wikilink")、[平正會計專科學校](../Page/平正會計專科學校.md "wikilink")、[華僑書院](../Page/華僑書院.md "wikilink")、[文化書院](../Page/文化書院.md "wikilink")、[光夏書院五書院外](../Page/光夏書院.md "wikilink")，還有香江書院（私營教育商人陳樹渠創立）、**珠海書院**與[廣大書院](../Page/廣大書院.md "wikilink")。但在談判期間，後三所包括珠海書院退出，前五所達成合併共識，成立「聯大學院」，但[教育司署不允](../Page/教育局_\(香港\)#歷史.md "wikilink")，故改名為「[聯合書院](../Page/聯合書院.md "wikilink")」，而珠海書院則繼續獨立辦學。

2000年起，珠海書院繼續向前發展，成立委員會。香港回歸及台灣民進黨上台執政後，書院難以再獲得台灣當局的支持，於是決定按香港法例要求向[香港學術評審局尋求學術評審](../Page/香港學術評審局.md "wikilink")，以期開辦香港認可的本地學位課程。

2004年5月，珠海書院通過了香港學術評審局對七個學位課程的評審，同年7月根據《專上學院條例》以「珠海學院」名義註冊，並於同年10月獲香港特區[行政長官會同行政會議批准](../Page/行政長官會同行政會議.md "wikilink")，得頒發香港政府認可的學位。2006年，財務金融學、商業資訊和建築等三項學位課程順利通過香港學術評審局評審。十個學士課程獲得香港政府認可。

2009年6月，珠海學院獲教育局撥出[屯門](../Page/屯門.md "wikilink")[青山公路土地](../Page/青山公路.md "wikilink")，佔地一點六公頃的前油庫，興建全新校舍。[香港立法會財務委員會已通過給予免息貸款三億五千萬元以供珠海學院興建新校舍](../Page/香港立法會.md "wikilink")，暫不計算貸款，學院本身已留三至四億元建校，希望在2013年前，即香港推行大學四年制前的竣工。

2010年首次涵蓋政府交由[大學教育資助委員會統籌的第五輪](../Page/大學教育資助委員會.md "wikilink")「[配對補助金計劃](../Page/配對補助金計劃.md "wikilink")」，珠海學院在該次補助金計劃共籌得一億一千萬元\[2\]。

珠海學院在開辦香港本地學位課程初期，曾經同時以「珠海書院」的名義繼續維持[中華民國教育部認可之學士學位及研究所學位課程](../Page/中華民國教育部.md "wikilink")，但中華民國認可的學士學位及台灣僑生先修部課程均已停止取錄新生，台灣學士學位課程也隨著高年級學生的畢業而結束，珠海書院也完成其歷史角色，此後該校已轉變為按《專上學院條例》註冊的珠海學院，頒授經[香港學術評審局評審的學士學位](../Page/香港學術評審局.md "wikilink")。珠海書院雖然已註冊為珠海學院，但仍繼續為珠海大學在香港復校而努力。珠海學院的中國文學研究所及中國歷史研究所則繼續招考由中華民國教育部頒發畢業證書的碩士及博士生，近年吸引台灣教育界人士來港進修，但這些台灣學位不受香港及中國內地直接認可。

2011年5月12日，珠海學院於屯門[舊咖啡灣的新校舍舉行動土儀式](../Page/舊咖啡灣.md "wikilink")。珠海學院新校舍佔地約1.6公頃，以雙主樓設計，提供圖書館、食堂和演講廳等設施，可容納約4000名學生，預計於2012學年啟用。

雖然珠海學院新校舍原定2012年底竣工，但礙於政府要預留空間供[屯門公路擴建而須要更改建築圖則設計](../Page/屯門公路.md "wikilink")，預計推遲至2015年底才落成啟用，新校舍在更改圖則後，主要在原有的教學、行政大樓及學生活動中心外，在同期工程加入學生及教職員宿舍，預計首階段提供二百個宿位，長遠冀能提供一千個宿位。而新校舍造價上升至近十億元，學院計劃今年繼續舉行多項籌款活動。並正構思約十個新課程，包括媒體與市場學、出版、歷史文化、環境相關等課程，待新校舍竣工後將陸續推出。學院將於2014年才進行學科範圍評審，有望一五至一六學年取得大學之名，配合屆時新校舍的落成。

2014年3月26日，珠海學院宣佈，其新聞及傳播學系以及中國文學學系已通香港學術及職業資歷評審局評審，除了英國語文學系，其他八個學系亦正申請自我評審資格，可望在年底前完成。完成課程評審後，校方還要做包括師資、學生水平等的院校評審，預計最快明年可以提出正名為大學的申請\[3\]
\[4\]。2014年7月獲納入政府的[指定專業／界別課程資助計劃](../Page/指定專業／界別課程資助計劃.md "wikilink")，從2015年起提供在計劃下受公帑資助的學位課程。2016年5月6日啟動了院校評審程序，正式向教育局申請升格\[5\]。

2016年3月，[海航集團副董事長](../Page/海航集團.md "wikilink")[逯鷹加入珠海學院的校董會成為副主席](../Page/逯鷹.md "wikilink")，而學院的股權由[江茂森家族持有的五珠有限公司轉移至香港珠海教育基金有限公司](../Page/江茂森.md "wikilink")。由於該基金董事會以及校董會成員有不少均具中國大陸背景，創校校訓「以建民國
以進大同」因為帶有中華民國色彩已於2016年被棄用，因此有聲音表示學院已經「棄藍轉紅」；另一方面，也顯示出香港[親中華民國或](../Page/香港親台團體.md "wikilink")[國民黨](../Page/中國國民黨.md "wikilink")、臺灣的機構團體，因難敵[中國大陸在](../Page/中國大陸.md "wikilink")[共產黨的統治下](../Page/中國共產黨.md "wikilink")[經濟崛起而式微](../Page/中國崛起.md "wikilink")\[6\]\[7\]。珠海學院獲得海航集團入股後，將籌建航空旅遊學院開辦酒店、旅遊及航空相關課程。

2018年3月，珠海學院建築系建築碩士課程，通過香港建築師學會及建築師註冊管理局評審，畢業生完成課程和實習後可成為學會會員及有資格參加測試成為註冊建築師，成為首間取得有關專業認證的私立院校。是次認證有效期五年，追溯至2017年11月起生效，直至2022年，與港大、中大的永久認證不同。\[8\]

### 廣州舊校舍現況

1948年「私立珠海大學」在廣州市[東山區竹絲崗二馬路之原有校舍](../Page/东山区_\(广州\).md "wikilink")，已於1998年被拆卸重建為「珠鷹大廈」\[9\]。該處鄰近[廣州地鐵一號線](../Page/廣州地鐵.md "wikilink")[東山口站C出口](../Page/東山口站.md "wikilink")。

### 升格大學

珠海學院校監[李焯芬於](../Page/李焯芬.md "wikilink")2016年5月宣布學院已向政府提交申請升格，預計兩年內取得私立大學名銜。

然而，由於中學畢業生人數持續下跌、先前與中華民國（臺灣）有聯繫，而學生寧願直接赴台升學等原因而導致招生非常不易\[10\]\[11\]，學院近年收生人數出現急降\[12\]。有學生關注學院收生人數能否於評審期間維持升格所需一千五百名的最低要求。\[13\]。結果在2016/17學年，學院的註冊全日制學生人數僅有973人。\[14\]

<table>
<caption>根據《專上學院條例》（<a href="../Page/香港法例.md" title="wikilink">香港法例第</a>320章）註冊的認可專上學院獲取大學名銜的修訂路線圖[15]</caption>
<thead>
<tr class="header">
<th><p>路線</p></th>
<th><p>達標情況</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>至少在三個範疇獲得學科範圍評審資格，而有關資格可以是相關學科範圍下的子範圍，但必須分屬不同的學科範圍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>展示若干程度的研究能力，成功獲得公帑資助計劃下與研究有關的資助</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>在緊接申請大學名銜前連續兩個學年，其修讀學位課程的學生人數（相等於全日制學生人數）最少達1,500人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>獲取<a href="../Page/香港學術及職業資歷評審局.md" title="wikilink">香港學術及職業資歷評審局院校評審資格</a>，以證明有關院校在管治及管理、財政可持續性、學術環境、質素保證及研究能力方面，基本上已有能力達到一所大學應有的水準</p></td>
<td></td>
</tr>
</tbody>
</table>

## 收生

院校學士學位課程近年出現收生不足的問題，2016-17年學年預計第一年學士學位課程收生1040個，最後僅取錄166人\[16\]，註冊率不足16%。於2017/18學年，院校第一年學士學位課程收生進一步下跌，原預計收生1040人，最終只有123人入讀\[17\]。

## 校徽

「珠海大學」創校於廣州[白雲山麓](../Page/白云山_\(广州\).md "wikilink")，故校徽以白雲珠海為圖案主體而示山高水長之義，徽中海上日出，以表朝氣蓬勃之意，日之上十二光線，是象徵十二時勤業之旨，型之以圓，所以勵志也。

## 校歌

### 珠海校歌

「珠海校歌」在1947年「珠海大學」創立於廣州時所作。由當時文學院院長任泰（東白）教授作詞，黃友棣教授譜曲。珠海創校於對日抗戰勝利後兩年，時值百廢待舉，內亂方殷，故歌詞開首即有「中原板蕩，珠海興庠」之句；珠海自始即以促進中西學術交流，融冶舊學新知為職志，故校歌中有「中西薈萃，今古發揚」之句；且「大學之道，在明明德，在新民，在止於至善」，古有明訓，因此校歌中亦提及「新民明德，源遠流長」，以示辦學之宗旨。至於曲譜則採用進行曲之旋律，莊嚴壯麗，用以激起學子奮發向上之雄心壯志。

  - 作曲：[黃友棣教授](../Page/黃友棣.md "wikilink")
  - 作詞：任泰（東白）教授
  - 音樂：[外部連結](https://web.archive.org/web/20071009203244/http://www.chcjalumni.com/SONG_024.MID)

### 六十周年校慶頌歌

  - 作曲：[黃友棣教授](../Page/黃友棣.md "wikilink")
  - 作詞：莫雲漢教授

## 校園環境

[Chu_Hai_College.jpg](https://zh.wikipedia.org/wiki/File:Chu_Hai_College.jpg "fig:Chu_Hai_College.jpg")新校舍\]\]

### 早期校舍（1992年以前）

珠海學院遷港早期、校舍先後位於[旺角](../Page/旺角.md "wikilink")[黑布街及](../Page/黑布街.md "wikilink")[亞皆老街的](../Page/亞皆老街.md "wikilink")[德明中學校舍內](../Page/德明中學.md "wikilink")，1972年學院從德明公司分出後、遷往位於旺角亞皆老街[大同中學分校校舍](../Page/香港大同中學.md "wikilink")，鄰近旺角火車站。

### 海濱花園校舍（1992年至2016年）

其後學院於1992年遷入荃灣[海濱花園的前](../Page/海濱花園_\(住宅\)#屋苑設施.md "wikilink")[地利亞修女紀念學校（荃灣）的舊校舍](../Page/地利亞修女紀念學校（荃灣）.md "wikilink")。校舍設有電腦室、圖書館等設施，亦設有電視、電台製作室，更在鄰近的[海濱廣場及](../Page/海濱廣場_\(荃灣\).md "wikilink")[海灣花園購物商場設置新圖書館](../Page/海灣花園#屋苑設施#海灣花園購物商場.md "wikilink")、建築系工作室及土木工程系工作室。

珠海學院位於荃灣的前校舍屬於[中華民國教育部資產](../Page/中華民國教育部.md "wikilink")，是教育部依據[行政院大陸委員會港澳會報及委員會會議決議](../Page/行政院大陸委員會.md "wikilink")，由[中華民國外交部轉撥教育部](../Page/中華民國外交部.md "wikilink")4,700萬港元，再由教育部與太平洋文化基金會簽署合約委辦，由該基金會出面洽購廣邑公司，以香港廣邑公司名義購置應該校舍，後租予珠海學院使用，遷校前每月租金為4萬港元。\[18\]

為配合未來發展及升格大學的需要，珠海學院於2009年獲政府撥地於屯門興建新校舍。\[19\]構思的新校舍設有現代化課室、演講廳和建築系工作室，亦有模擬電視台、電台、排版工作室等。整項工程本預計於2012年完成，耗資港幣3至4億\[20\]，後因地盤地形複雜及更改設計而延遲取得地契，導致造價大幅超支1.3倍，至約港幣10億元，珠海需向政府增貸2.5億，並預計延至2015年末完成。\[21\]最終新校舍於2016年完工，造價約為港幣11億元。\[22\]

### 屯門新校舍（2016年起）

新校舍由許李嚴建築師事務有限公司（Rocco Design Architects
Limited）之[嚴迅奇設計](../Page/嚴迅奇.md "wikilink")，位於[屯門](../Page/屯門.md "wikilink")[青山灣](../Page/青山灣.md "wikilink")[掃管軍營原址](../Page/掃管軍營.md "wikilink")，佔地約17.2萬方呎，樓高8層，總樓面約28.5萬方呎。設施包括課室、圖書館、演講廳、體育中心、活動廣場、餐廳、職員辦公室、學生和教師宿舍等。校舍以「樹下閱讀」為設計概念，下方有草坪供師生閒坐，上方設兩道天橋連接兩棟樓宇。校園獲[香港建築師學會頒發](../Page/香港建築師學會.md "wikilink")「2016/2017全年境內建築大獎」。

新校舍並獲[香港賽馬會贊助港幣兩億元興建教學樓](../Page/香港賽馬會.md "wikilink")，新校舍地基及大樓建築工程分別於2014年6月26日\[23\]及2016年5月19日\[24\]完成，並於2016年8月22日遷入及啟用。\[25\]2019年1月14日，主校舍及賽馬會教學大樓啟用，並舉行開幕典禮，至此，青山灣校舍全部設施已啟用。\[26\]

Chu Hai College construction site.jpg|新校舍工地（2012年4月） Chu Hai College of
Higher Education new campus under construction in February
2015.JPG|新校舍工地（2015年2月） Chu Hai College of Higher Education
new campus under construction in April 2015.JPG|新校舍工地（2015年4月） Chu Hai
College of Higher Education new campus under construction in September
2015.JPG|新校舍工地（2015年9月） Chu Hai College of Higher Education new campus
under construction in February 2016.JPG|新校舍工地（2016年2月） Chu Hai College
of Higher Education new campus.jpg|新校舍工地（2016年5月）

Chu Hai College of Higher Education (Tsuen Wan
campus).jpg|[荃灣原校舍](../Page/荃灣.md "wikilink")
陳濟棠圖書館.jpg|原位於海濱廣場的陳濟棠圖書館（於新校舍重置後易名為江茂森圖書館）
Studio - CHCHE.jpg|原位於海灣花園購物商場的建築系工作室

## 組織機構

### 歷任大學校長

  - [黃麟書](../Page/黃麟書.md "wikilink")：（1947-1949）創辦人之一兼私立珠海大學首任校長，並帶領[廣州私立珠海大學遷到香港](../Page/廣州.md "wikilink")。

### 歷任書院校長

  - [黃麟書](../Page/黃麟書.md "wikilink")：（1949），珠海書院首任校長。
  - [唐惜分](../Page/唐惜分.md "wikilink")：（1949-1957），著名教育家，珠海書院第二任校長。
  - [江茂森](../Page/江茂森.md "wikilink")：（1967-1977），1901年生，[廣東省](../Page/廣東省.md "wikilink")[茂名縣人士](../Page/茂名縣.md "wikilink")，[中華民國當代著名教育家](../Page/中華民國.md "wikilink")、[中國國民黨員](../Page/中國國民黨.md "wikilink")，畢業於廣州市國立中山大學，歷任軍政要職，尤得前[廣東省主席](../Page/廣東省.md "wikilink")[陳濟棠將軍所器重](../Page/陳濟棠.md "wikilink")，1949年後，移居[香港](../Page/香港.md "wikilink")，創辦了[廣州私立珠海大學](../Page/廣州.md "wikilink")、[香港](../Page/香港.md "wikilink")[德明中學及](../Page/德明中學.md "wikilink")[九龍](../Page/九龍.md "wikilink")[大同中學](../Page/大同中學.md "wikilink")，出版中國文學鉅著：「廣東文徵」，1980年退休，兩年後於[香港病逝](../Page/香港.md "wikilink")。兒子為現任珠海學院校董會主席[江可伯及已故副校長](../Page/江可伯.md "wikilink")[江佑伯](../Page/江佑伯.md "wikilink")。
  - [江可伯](../Page/江可伯.md "wikilink")：（1977-1980），創辦人之一兼校長[江茂森之長子](../Page/江茂森.md "wikilink")，曾任珠海學院校監，現任校董會主席。
  - [梁永燊](../Page/梁永燊.md "wikilink")：（1980-1994），1921年生，[廣東省](../Page/廣東省.md "wikilink")[茂名縣人士](../Page/茂名縣.md "wikilink")，[國立中山大學](../Page/國立中山大學.md "wikilink")（廣州）法學士、[中華民國的立法委員](../Page/中華民國.md "wikilink")、培知中學創辦人，曾任珠海書院的訓導長、副校長及校長等。1994年10月病逝於[香港](../Page/香港.md "wikilink")，遺體長眠[美國](../Page/美國.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")。
  - [陳樹柏](../Page/陳樹柏.md "wikilink")：（1995-1996），創辦人之一[陳濟棠的兒子](../Page/陳濟棠.md "wikilink")，[維珍尼亞軍校念了](../Page/維珍尼亞軍校.md "wikilink")4年炮科，後以優異的成績取得碩士學位，留在[維珍尼亞軍校做講師](../Page/維珍尼亞軍校.md "wikilink")，後取得[伊利諾大學博士學位](../Page/伊利諾大學.md "wikilink")。在美國各大學任教育工作接近半個世紀，其中在[聖塔克拉拉大學教學](../Page/聖塔克拉拉大學.md "wikilink")30年，1994年9月在美國[矽谷創辦](../Page/矽谷.md "wikilink")[國際科技大學](../Page/國際科技大學.md "wikilink")。
  - [田震亞](../Page/田震亞.md "wikilink")：（1994-1995任代校長，1996-1997），曾任客座教授、教務長，後前校長[梁永燊於任內逝世](../Page/梁永燊.md "wikilink")，1994年暫代校長一職，[陳樹柏接任校長後出任到副校長兼亞洲研究中心主任](../Page/陳樹柏.md "wikilink")。任內協助創辦資訊科學系。
  - [劉興漢](../Page/劉興漢.md "wikilink")：（1997-2002），經濟學家，曾就讀[珠海書院經濟研究所](../Page/珠海書院.md "wikilink")，後負笈美國[南加州大學取得高等教育博士](../Page/南加州大學.md "wikilink")，先後任教美國、台灣、香港各大學，現為台灣[萬能科技大學講座教授](../Page/萬能科技大學.md "wikilink")、[國立政治大學兼任教授](../Page/國立政治大學.md "wikilink")。
  - [張忠柟](../Page/張忠柟.md "wikilink")：（2002-2004），密碼學專家，曾擔任[珠海書院理工學院院長](../Page/珠海書院.md "wikilink")、副校長、在[美國曾任](../Page/美國.md "wikilink")[聖塔克拉拉大學及](../Page/聖塔克拉拉大學.md "wikilink")[史丹福大學研究員及講師](../Page/史丹福大學.md "wikilink")。[國立中興大學應用數學學士](../Page/國立中興大學.md "wikilink")、[美國](../Page/美國.md "wikilink")[史丹福大學碩士及博士](../Page/史丹福大學.md "wikilink")。

### 歷任學院校長

  - [張忠柟](../Page/張忠柟.md "wikilink")：（2004-2018）
  - [李焯芬](../Page/李焯芬.md "wikilink")：（2018-，暫任），著名專業地質工程師及水利專家，曾任香港大學副校長、香港大學專業進修學院院長及香港大學土木工程系講座教授。現為珠海學院校監，同時兼任香港中華文化促進中心理事會主席、香港大學饒宗頤學術館館長、共建維港委員會主席等公職。

### 學院和學系

珠海學院有提供十六項政府認可學士學位課程及兩項政府認可碩士課程。

  - 文學與社會科學院\[27\]
      - 國學文學碩士
      - 中國文學（榮譽）文學士
      - 中文文藝創作（榮譽）文學士
      - 新聞及傳播（榮譽）文學士
      - 廣告及企業傳播（榮譽）文學士
      - 傳播及跨媒體（榮譽）文學士
      - 專業英語傳意（榮譽）文學士

<!-- end list -->

  - 商學院\[28\]
      - 會計及銀行（榮譽）商學士
      - 工商管理學（榮譽）學士
      - 財務金融學（榮譽）工商管理學士
      - 財務及資訊管理（榮譽）工商管理學士

<!-- end list -->

  - 理工學院\[29\]
      - 建築學碩士
      - 資訊科學（榮譽）理學士（大學聯招課程編號：JSSC03）
      - 土木工程（榮譽）工學士
      - 建造工程及管理（榮譽）工學士
      - 建築學（榮譽）理學士（大學聯招課程編號：JSSC02）

另外珠海學院目前還保留歷史學系、社會學及社會工作學系在[中華民國教育部的以](../Page/中華民國教育部.md "wikilink")「私立珠海大學」名義立案，而該等課程歸屬文學院，但相關的學系已停止收生。另設有中國文學研究所和中國歷史研究所，同樣以上述名義於中華民國教育部立案，目前仍在招生中。

以「**私立珠海大學**」名義於中華民國教育部立案的系所：

  - 文學院
      - 歷史學文學士（已停止招生）
      - 社會學及社會工作學學士（已停止招生）
      - 中國文學研究所
          - 文學碩士
          - 文學博士
      - 中國歷史研究所
          - 文學碩士
          - 文學博士

## 知名度

大公報所進行的調查結果顯示，部分自資院校例如香港珠海學院缺乏知名度。雖然已辦學近70年，但是有58%受訪高中生表示「未聽過」珠海學院，是為兩間認知率不足半的大專院校之一。有受訪者高中生表明不會入讀不認識的院校，反映知名度偏低可能會影響其收生\[30\]。為了紓緩問題，珠海學院近年紛紛開拓內地市場，加強到內地招生。\[31\]

## 文化傳统

### 電腦專業文憑課程

珠海學院的電腦專業文憑課程在未得到香港政府認可以前，稱為電腦文憑課程。

### 新聞系課程

由於香港在1950年代的社會意識普遍認同[中華民國政府](../Page/中華民國政府.md "wikilink")，加上珠海學院是香港第一間創立新聞系，所以珠海學院出身的記者亦比較受歡迎。本港很多電視台新聞記者及主播也是該校的畢業生。

### 學生刊物

學生刊物包括：

  - 學生會編輯委員會刊物《海聲》（Chuhai Magazine）
  - 新聞及傳播學系實習刊物《珠海新聞》\[32\]
  - 新聞及傳播學系實習網媒《珠時網》（CHC Online）\[33\]

## 學生組織

### 香港珠海學院學生會

成立於1968年。學生會分為幹事會、評議會、編輯委員會及仲裁委員會共四大架構，每年由各會員投票組成。評議會為全民投票閉會期間，本會最高立法及監察幹事會執行會務之權力機構；幹事會為本會執行會務之機構；編輯委員會負責監察編委會刊物出版事宜之行政機構，直接向全體會員負責；仲裁委員會則為本會最高裁決機關。由四十六屆學生會起，幹事會產生方法正式變為內閣競選全民投票制；由四十九屆學生會起，編輯委員會產生方法正式變為內閣競選全民投票制。學生會每年舉辦不同種類的活動給各同學，如歌唱比賽、球類比賽、外地學術交流、書展、棋類比賽及捐血活動等，滿足同學們的需求。

### 珠海學院學生關注社會組

珠海學院學生關注社會組
，是一個由學生自發成立的社會關注組織，旨在加強學校同學對社會的責任感。透過舉辦不同活動，培養同學獨立批判思維，提升公民意識，並以行動積極回應社會議題。

## 爭議事件

  - 學生冒認教職員身份在網上討論區發言，珠海書院被指鎮壓學生網上言論事件

<!-- end list -->

  -
    事緣，珠海書院的學生在網上討論區的留言板「珠海2號系會室」內進行討論，內容圍繞學系實習機會分配的公平性。過程中，有學生言行不當，一方面冒認教職員身份，以校長或陳望祖或其他講師的名字發言。另一方面，還針對部份教職員發表涉及其身材及私生活等的言論，當中包括時任系主任曹虹。出於無法確認留言者身份，校方調查出現困難。為此，「放風」要求學生自首，指自首的話還有可能從輕發落，並威脅如果不自首，便會把所有責任轉嫁身為該網網主的學生，即是「骾晒佢」。最終，一共有四名學生自首。然而，珠海書院似乎食言，多名「自首」的學生需要面對相當嚴厲的處罰，被處罰停學半年或一年，甚至被開除學籍。另一方面，又多次威脅「自首」的學生會採取法律行動，使學生擔驚受怕，完全無法應付生活。為此，有學生對自首感到非常後悔。另外，學生聲稱他們受到威嚇，將會因為「敗壞」了珠海的名聲而無法在新聞界立足，即是「話我呢行（新聞界）冇得立足」。不少人認為珠海書院的懲罰過重，有鎮壓之嫌，違背教育精神，故對其提出質疑。\[34\]

<!-- end list -->

  - 學生在年宵攤檔銷售聲稱為其所原創的侵權物品事件

<!-- end list -->

  -
    據香港獨立媒體報道，珠海學院的學生在旺角花墟開辦年宵攤檔，即是「馬8良品」，銷售的產品包括另類漫畫圖樣的布袋。在其攤檔的面書專頁，「馬8良品」聲稱售賣布袋上漫畫圖案是由設計的布袋是由珠海學院學生「親自」和「自家」的設計但是，原圖創作者，即是「另類漫畫」發現，「馬8良品」聲稱售賣布袋上漫畫圖案是由設計的布袋是由珠海學院學生「親自」和「自家」的設計的圖，實為由「另類漫畫」所創作的「劉江圖」。學生拒絕認錯，指這是「網上的集體二次創作出來的產物」，原圖作者拒絕接納。事件最終驚動校方，校方要求學生主動和原圖創作者聯絡，解釋相關事宜\[35\]。

<!-- end list -->

  - 珠海新傳系被轟落後，珠海新聞系學生擬罷交學費事件

事緣，珠海學院新傳系的多項政策和實質運作，包括教學、配套和行政等方面也長期為學生所垢病，積存了不少的問題和怒氣\[36\]。為此，珠海學院新傳系學生在2015年5月自發成立「珠海學院新傳政策關注組」，以糾正學系積存已久的政策問題。該關注組所指出的問題包括但不限於

（1）有高達六成選修科和新聞科目無關，這使新傳系變得有名無實，即所謂「新聞系淪為社會政治學」\[37\] ；

（2）新傳系大部分教師並非新聞專業出身，這引起學生及市民對教學質素憂慮，例如派出修讀社會科學而且没有採訪經驗的導師任教採訪\[38\]；

（3）本地實習機制透明度缺乏透明度；

（4）教學設施落後，錄影室規模比小學課室還要小，無法進行高清製作\[39\]；

（5）校內媒體《珠海新聞》被「娛樂化」等；以及，

（6）學生缺乏到媒體機構實習的機會。

在多個學生所關注的問題中，以「實務操作課程嚴重不足」最受注目。關注組指「實務科目嚴重不足」使近年珠海學院新傳系的畢業生未能與其他院校的學生競爭，進而對學生的就業前景造成不利\[40\]
\[41\]。據修讀「電子組」的關注組召集人梁樂祈所指，4年課程中，只有不超過5篇的新聞稿的功課\[42\]。

為此，學生成立「珠海學院新傳政策關注組」表達訴求。除了爭取增加實務科目比例外，也爭取增加本地實習機制透明度和完善校內製作室設備等。\[43\]

關注組聲明如果校方不改善，便會發動罷交學費行動，並已獲得超過一百五十名學生聯署支持\[44\]，人數為全系學生500人的百份之20\[45\]。

事件引起多個媒體關注，也有人在報章撰文認同學生所關注的問題。例如由高級講師梁德民任教的「電台節目及製作」，竟然同時要求三班一共90人以「家暴」為主題，完成訪問及習作，又不准許學生找相同人士訪問，否則扣分\[46\]
，結果出現有社工組織一日內竟接獲10多個經電話傳達的訪問邀請的情況，最終導致相關機構投訴。撰文者認為這情況相當荒謬，激起學生反感也是合情合理。\[47\]

為此，學院方便邀請向學生出席會面，討論校政，又指校長張忠柟、學務長古穎慈及新傳系相關人員也會出席。\[48\]

珠海學院「新傳政策關注組」隨後，就改革該校新聞及傳播學系課程舉行公投，投票率高達八成三。\[49\]結果為超過八成的新傳系學生同意「現有課程未能達到其開辦目的」，另外，更有九成七學生指「學系應增加更多新聞媒體實務相關課程」。\[50\]
「新傳政策關注組」指出公投有意義重大，反映改革迫切性。\[51\]

但是，珠海新聞傳系系主任皇甫河旺拒絕對公投作出任何特別回應。原因在於他認為，沒有一間大學能成功從發動公投表達訴求。\[52\]

為此，有學生指可能發動罷交學費，學生事務長古頴慈便回應指，學生在沒有合理理由下遲交學費超過兩星期，會被視作退學處理。 \[53\]

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/青山公路.md" title="wikilink">青山公路</a><a href="../Page/青山灣.md" title="wikilink">青山灣段</a></dt>

</dl>
<p><a href="../Page/咖啡灣.md" title="wikilink">咖啡灣巴士站</a>    </p>
<dl>
<dt><a href="../Page/紅色公共小巴.md" title="wikilink">紅色公共小巴</a></dt>

</dl>
<ul>
<li>荃灣至元朗線 (通宵服務)[54]</li>
<li>旺角至元朗線 (下午至通宵服務)[55]</li>
<li>元朗至佐敦道線 (24小時服務)[56]</li>
<li>銅鑼灣至元朗線 (下午至通宵服務)[57]</li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [珠海學院主頁](http://www.chuhai.edu.hk)
  - [珠海新聞](http://jcchuhainews.chuhai.hk)
  - [珠海學院學生會](http://home.chuhai.hk/~su/)
  - [香港珠海學院學生會 Student Union of Chu Hai College of Higher Education
    Facebook專頁](https://www.facebook.com/CHCSU/)
  - [珠海學院學生關注社會組 Facebook專頁](https://www.facebook.com/chcsscg/)
  - [竹絲崗二馬路的私立珠海大學的遺址影像(2009年2月)](http://www.youtube.com/watch?v=LnQyxCD6Xg0)
  - [The new page for the faculty of business, which provides more
    information about the people and activities related to the
    faculty](http://fob.chuhai.hk)

[Category:香港大專院校](../Category/香港大專院校.md "wikilink")
[Category:青山灣](../Category/青山灣.md "wikilink")
[Category:掃管笏](../Category/掃管笏.md "wikilink")
[Category:1947年創建的教育機構](../Category/1947年創建的教育機構.md "wikilink")
[Category:廣東已不存在的高等院校](../Category/廣東已不存在的高等院校.md "wikilink")
[Category:原东山区](../Category/原东山区.md "wikilink")
[Category:广州教育史](../Category/广州教育史.md "wikilink")
[Category:珠海學院](../Category/珠海學院.md "wikilink")
[Category:海航集团](../Category/海航集团.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13. [【與校長對話】九月遷新校舍 校方高層對未來發展充滿信心
    東方日報](http://chconline.chuhai.hk/stories/view/76?from=story_index&category_id=7)
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27. [文學與社會科學院](https://www.chuhai.edu.hk/zh-hant/文學與社會科學院/?MenuID=20)
28. [商學院](https://www.chuhai.edu.hk/zh-hant/商學院/?MenuID=21)
29. [理工學院](https://www.chuhai.edu.hk/zh-hant/理工學院/?MenuID=22)
30.
31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.
48.
49.
50.
51.
52.
53.
54.
55.
56.
57. [銅鑼灣波斯富街　＞　屯門及元朗](http://www.16seats.net/chi/rmb/r_hn68.html)