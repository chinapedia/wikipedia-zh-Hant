[Maglev_in_Daejeon_01.jpg](https://zh.wikipedia.org/wiki/File:Maglev_in_Daejeon_01.jpg "fig:Maglev_in_Daejeon_01.jpg")生產的磁浮列車\]\]
**現代Rotem**（；）是[南韓一家出產](../Page/南韓.md "wikilink")[鐵路車輛](../Page/鐵路車輛.md "wikilink")、[軍事及廠房產品的公司](../Page/軍事.md "wikilink")，為[現代汽車集團的一員](../Page/現代汽車.md "wikilink")，在韓國有逾3,800名員工，其產品出口國家達35個。

使用現代Rotem製地鐵列車的系統包括[港鐵](../Page/港鐵.md "wikilink")、[台鐵](../Page/台鐵.md "wikilink")、[印度](../Page/印度.md "wikilink")[新德里捷運及](../Page/新德里捷運.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[溫哥華架空列車的](../Page/溫哥華架空列車.md "wikilink")[加拿大綫](../Page/加拿大綫.md "wikilink")、[哈萨克斯坦的](../Page/哈萨克斯坦.md "wikilink")[阿拉木图地铁](../Page/阿拉木图地铁.md "wikilink")。

現代Rotem對於產品的品質、安全性與環境皆相當重視，獲得ISO/KSA國際品質管理機構認證與國際鐵道產業標準IRIS認證，具備相當之品質競爭力。

## 沿革

[Korail_KTX-2.jpg](https://zh.wikipedia.org/wiki/File:Korail_KTX-2.jpg "fig:Korail_KTX-2.jpg")高铁最高时速可达350公里／小时\]\]
1999年7月，[現代精工](../Page/現代精工.md "wikilink")、[大宇重工業與](../Page/大宇重工業.md "wikilink")[韓進重工業的鐵路車輛製造部門整合為](../Page/韓進重工業.md "wikilink")**韓國鐵路車輛公司**
（**Ko**rea **Ro**lling **S**tock
Corporation，[縮寫](../Page/縮寫.md "wikilink")：KOROS）。[2001年10月](../Page/2001年10月.md "wikilink")，韓國鐵路車輛公司被現代汽車集團收購。[2002年1月](../Page/2002年1月.md "wikilink")，韓國鐵路車輛公司更名為**Rotem**（全稱**R**ailr**o**ading
**T**echnology
Syst**em**）。2007年11月，Rotem更名為現代Rotem。2013年10月30日，現代Rotem於韓國[股票上市](../Page/股票上市.md "wikilink")。

## CBTC

  - [加拿大捷運捷運車輛](../Page/溫哥華架空列車.md "wikilink")40輛

  - [聖保羅地鐵4號線捷運車輛](../Page/聖保羅地鐵4號線.md "wikilink")84輛

  - [韓國地鐵新盆唐線捷運車輛](../Page/韓國地鐵新盆唐線.md "wikilink")72輛、號誌

  - [釜山-金海輕軌輕軌車輛](../Page/釜山-金海輕軌.md "wikilink")50輛、統包工程

  - [仁川地鐵2號線](../Page/仁川地鐵2號線.md "wikilink")74輛、統包工程

  - [牛耳新設線車輛](../Page/牛耳新設線.md "wikilink")36輛、統包工程

## 鐵路車輛

### 2011－2015

  - [突尼西亞鐵路](../Page/突尼西亞鐵路.md "wikilink")76輛
  - [首爾地鐵9號線](../Page/首爾地鐵9號線.md "wikilink")48輛
  - [韓國鐵道公司](../Page/韓國鐵道公司.md "wikilink")[盆唐線](../Page/盆唐線.md "wikilink")30輛
  - [德里RS3地鐵](../Page/德里RS3地鐵.md "wikilink")196輛
  - [韓國鐵道公司](../Page/韓國鐵道公司.md "wikilink")[京春線](../Page/京春線.md "wikilink")64輛
  - [首爾地鐵1號線](../Page/首爾地鐵1號線.md "wikilink")50輛
  - [烏克蘭90區際雙系統](../Page/烏克蘭90區際雙系統.md "wikilink")90輛
  - [紐西蘭威靈頓Matangi號](../Page/紐西蘭威靈頓Matangi號.md "wikilink")96輛
  - [韓國鐵道公司](../Page/韓國鐵道公司.md "wikilink")[水仁線](../Page/水仁線.md "wikilink")24輛
  - [美國賓州東南地區交通局（SEPTA）](../Page/美國賓州東南地區交通局（SEPTA）.md "wikilink")120輛
  - 希臘[雅典地鐵第三階段](../Page/雅典地鐵.md "wikilink")102輛
  - [韓國鐵道公司](../Page/韓國鐵道公司.md "wikilink")[京義線](../Page/京義線.md "wikilink")58輛
  - [突尼西亞鐵路](../Page/突尼西亞鐵路.md "wikilink")108輛
  - [仁川地鐵2號線](../Page/仁川地鐵2號線.md "wikilink")74輛
  - [班加羅爾地鐵](../Page/班加羅爾地鐵.md "wikilink")150輛
  - [土耳其馬爾馬雷CR2](../Page/土耳其馬爾馬雷CR2.md "wikilink")60輛
  - [土耳其伊茲密爾郊區線](../Page/土耳其伊茲密爾郊區線.md "wikilink")72輛

## 另見

  - [港鐵Rotem電動列車](../Page/港鐵Rotem電動列車.md "wikilink")（港鐵[將軍澳綫及](../Page/將軍澳綫.md "wikilink")[東涌綫](../Page/東涌綫.md "wikilink")）
  - [港鐵現代Rotem電動列車](../Page/港鐵現代Rotem電動列車.md "wikilink")（港鐵[東鐵綫](../Page/東鐵綫.md "wikilink")）
  - [台鐵EMU600型電聯車](../Page/台鐵EMU600型電聯車.md "wikilink")
  - [台鐵推拉式自強號車廂](../Page/台鐵推拉式自強號.md "wikilink")
  - [KTX-II](../Page/KTX-II.md "wikilink")
  - [台鐵EMU900型電聯車](../Page/台鐵EMU900型電聯車.md "wikilink")

## 外部連結

  - [現代Rotem](http://www.hyundai-rotem.co.kr)

[Category:韓國鐵路公司](../Category/韓國鐵路公司.md "wikilink")