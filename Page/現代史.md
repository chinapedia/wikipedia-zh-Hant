**現代史**（），又譯為**近代史**，指**現代**（又譯**近代**，，）這個歷史時期的[歷史](../Page/歷史.md "wikilink")。在西方，現代普遍被定義為[中世紀之後的歷史時期](../Page/中世紀.md "wikilink")，約開始於16世紀。現代可以被進一步區分為[近世](../Page/近世.md "wikilink")（Early
modern period）與現代晚期（late modern
period），這兩個時期以[法國大革命及](../Page/法國大革命.md "wikilink")[工業革命作為分界](../Page/工業革命.md "wikilink")。在現代史之中，與現在時間緊密相關的歷史，稱為[當代史](../Page/當代史.md "wikilink")（Contemporary
history）。

近代和現代合稱近現代，即以近代為現代之前的一個時期；但有時近代和現代沒有截然的含義區別，視為同義，相互替代使用。

## 概論

中國現代的起始時期，有幾種觀點：

  - [辛亥革命建立](../Page/辛亥革命.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")。推翻世襲帝制，建立共和民主政體。
  - 民國八年[五四運動](../Page/五四運動.md "wikilink")。[中國共產黨在此間孕育](../Page/中國共產黨.md "wikilink")，其史觀持此觀點，並以[鴉片戰爭作為中國近代的開始](../Page/第一次鸦片战争.md "wikilink")。

現代時期的開始代表了[歐洲](../Page/歐洲.md "wikilink")[文藝復興的結束](../Page/文藝復興.md "wikilink")。確切的時間點會依據各個領域的界定而有所不同，比如說，一個[歷史學家可能會將現代的開始放在](../Page/歷史學.md "wikilink")[1650年](../Page/1650年.md "wikilink")，而一個[音樂家則可能會把時間放在](../Page/音樂家.md "wikilink")[音樂的](../Page/音樂.md "wikilink")[浪漫時期結束之後](../Page/浪漫音樂.md "wikilink")，也就是大約[1900年](../Page/1900年.md "wikilink")。當代史學者習慣把一戰爆發，作為近代的終結、現代的開端。

同樣地，依據各個領域的不同觀點，現代時期可能會被界定為一直延伸到我們現在所處的時間，或者也有可能被認為結束於[後現代主義的開始](../Page/後現代主義.md "wikilink")（可能在[1960年代到](../Page/1960年代.md "wikilink")[1980年代初期之間的任何一點](../Page/1980年代.md "wikilink")）。如果是將現代定義為「在後現代之前」的話，那麼它可能特別指涉了[現代主義](../Page/現代主義.md "wikilink")。有的觀點則認為，後現代主義只不過是現代主義本身最晚一個時期的發展而已。

## 革命

現代時期在許多領域中發生了許多顛覆性的變化，將整個「舊世界」轉變成現代世界。這個新世界在[核子時代](../Page/核子時代.md "wikilink")、[性革命和](../Page/性革命.md "wikilink")[-{zh-hans:信息时代;zh-hant:資訊時代;}-達到頂峰](../Page/信息时代.md "wikilink")，然後一直到了[千禧年交替的現在](../Page/千禧年.md "wikilink")。現代世界基本上被三個重大的革命所形塑：

  - [1776年的](../Page/1776年.md "wikilink")[美國革命](../Page/美國革命.md "wikilink")
  - [1789年的](../Page/1789年.md "wikilink")[法國大革命](../Page/法國大革命.md "wikilink")
  - [18世紀和](../Page/18世紀.md "wikilink")[19世紀期間發生在](../Page/19世紀.md "wikilink")[英國的](../Page/英國.md "wikilink")[工業革命](../Page/工業革命.md "wikilink")

而這三個[革命又為下面這個革命埋下了種子](../Page/革命.md "wikilink")：

  - [1917年的](../Page/1917年.md "wikilink")[俄國革命](../Page/1917年俄国革命.md "wikilink")

這些革命也影響到[18世紀](../Page/18世紀.md "wikilink")、[19世紀和](../Page/19世紀.md "wikilink")[20世紀中的一些重大](../Page/20世紀.md "wikilink")[戰爭的發生](../Page/戰爭.md "wikilink")，比如：

  - [美國獨立戰爭](../Page/美國獨立戰爭.md "wikilink")
  - [歐洲的](../Page/歐洲.md "wikilink")[拿破崙戰爭](../Page/拿破崙戰爭.md "wikilink")
  - [第一次世界大戰](../Page/第一次世界大戰.md "wikilink")（1914年—1918年）
  - [第二次世界大戰](../Page/第二次世界大戰.md "wikilink")（1939年—1945年）
  - [冷戰](../Page/冷戰.md "wikilink")（1945年－1991年）

美國革命與法國大革命雖然未能完全結束了專制權力，但讓[民主](../Page/民主.md "wikilink")、[自由](../Page/自由.md "wikilink")、[平等](../Page/平等原則.md "wikilink")、[博愛等成為當今世界的普世價值](../Page/七美德.md "wikilink")，變成了新的[政府準則與](../Page/政府.md "wikilink")[社會規範](../Page/社會.md "wikilink")，才使得這個世界變成了「現代」世界。

像[拿破崙等人為歐洲帶來了新的](../Page/拿破崙.md "wikilink")[法律制度](../Page/法律.md "wikilink")。這些法律制度是以[能力與成就為依歸](../Page/精英政治.md "wikilink")，而非像封建時代那樣是以[社會階級系統為依歸](../Page/社會階級.md "wikilink")。現代的[自由主義](../Page/自由主義.md "wikilink")[政治制度](../Page/政治制度.md "wikilink")，讓原本沒有[公民權的平民獲得了](../Page/公民權.md "wikilink")[權力](../Page/權力.md "wikilink")。對個人的[國族](../Page/國家.md "wikilink")、[文化和](../Page/文化.md "wikilink")[語言重視的增加](../Page/語言.md "wikilink")，則造成了具有強大力量的[民族主義](../Page/民族主義.md "wikilink")。這些改變最終產生了一些新的[意識形態](../Page/意識形態.md "wikilink")，比如[法西斯主義](../Page/法西斯主義.md "wikilink")、[社會主義和](../Page/社會主義.md "wikilink")[共產主義](../Page/共產主義.md "wikilink")。

[俄羅斯帝國和](../Page/俄羅斯帝國.md "wikilink")[德意志帝國可以說代表了這股潮流的一個極端](../Page/德意志帝國.md "wikilink")。由於希望將過去的遺跡完全抹殺，並且創造一個沒有[階級的社會](../Page/社會階級.md "wikilink")，俄國在[1917年發生了](../Page/1917年.md "wikilink")[二月革命和](../Page/俄国二月革命.md "wikilink")[十月革命](../Page/十月革命.md "wikilink")，造成了[沙皇](../Page/沙皇.md "wikilink")[尼古拉二世和其家人被處決](../Page/尼古拉二世_\(俄罗斯\).md "wikilink")、[蘇聯成立](../Page/蘇聯.md "wikilink")、[農奴制的改變](../Page/農奴制.md "wikilink")、以及強迫性地使俄國現代化。在德國，[1918年](../Page/1918年.md "wikilink")[德國皇帝威廉二世退位後](../Page/威廉二世_\(德国\).md "wikilink")，社會形勢動盪不安，促成了[希特勒和](../Page/希特勒.md "wikilink")[納粹主義的興起](../Page/納粹主義.md "wikilink")。

## 現代

新成立的[美國保障了白種男性公民的](../Page/美國.md "wikilink")[投票權](../Page/投票權.md "wikilink")，並且使用[憲法來制衡政府權力](../Page/美國憲法.md "wikilink")，創建了[三權分立的政治系統](../Page/三權分立.md "wikilink")，即[立法](../Page/立法.md "wikilink")、[司法](../Page/司法.md "wikilink")、以及由[民選出來的](../Page/選舉.md "wikilink")[總統所領導的](../Page/美國總統.md "wikilink")[行政系統](../Page/行政.md "wikilink")，來達到政府的權力平衡與互相監督。

在科技上的發現、研究與應用改變了物品被製造和販售的方式。比如說，[英國的](../Page/英國.md "wikilink")[現代機械化加速了像](../Page/英国工业革命.md "wikilink")[布料和](../Page/布料.md "wikilink")[鐵這些](../Page/鐵.md "wikilink")[商品的製造速度](../Page/商品.md "wikilink")。[引擎的發明使得](../Page/引擎.md "wikilink")[汽車](../Page/汽車.md "wikilink")、[火車](../Page/火車.md "wikilink")、[船隻和](../Page/船隻.md "wikilink")[飛機得以運作](../Page/飛機.md "wikilink")，[馬](../Page/馬.md "wikilink")、[牛等牲畜不再成為搬運重物的必要工具](../Page/牛.md "wikilink")。物品原料因此能夠大量長距離地運輸到另一個地方，並且快速地被製造，然後販售到全世界去，讓英國成為最富有的國家。

科學的發明與發現持續推動著社會進步。[電話](../Page/電話.md "wikilink")、[無線電](../Page/無線電.md "wikilink")、[X光線](../Page/X光線.md "wikilink")、[顯微鏡](../Page/顯微鏡.md "wikilink")、[電等為生活帶來了快速的變化](../Page/電.md "wikilink")。像[盤尼西林這類](../Page/盤尼西林.md "wikilink")[抗生素的發現](../Page/抗生素.md "wikilink")，帶來了對抗疾病的新方法。而[進化論](../Page/進化論.md "wikilink")、[心理分析等理論的提出](../Page/心理分析.md "wikilink")，也改變了人類對自我了解的舊有觀念。

[來福槍](../Page/來福槍.md "wikilink")、[加農砲](../Page/加农炮.md "wikilink")、[手槍](../Page/手槍.md "wikilink")、[機關槍](../Page/機關槍.md "wikilink")、[坦克車](../Page/坦克車.md "wikilink")、[飛機](../Page/飛機.md "wikilink")、[噴射機](../Page/噴射機.md "wikilink")、[飛彈等新式](../Page/飛彈.md "wikilink")[武器的發明](../Page/武器.md "wikilink")，也改變了[戰爭的型態](../Page/戰爭.md "wikilink")。而[原子彈](../Page/原子彈.md "wikilink")、[氫彈](../Page/氫彈.md "wikilink")、[化學武器](../Page/化學武器.md "wikilink")、[生物武器等大規模毀滅性武器](../Page/生物武器.md "wikilink")，使得[地球在幾分鐘之內就被摧毀的事情變為可能](../Page/地球.md "wikilink")。這些全部都是現代世界的標識之一。

隨著[教會影響力的縮小](../Page/教會.md "wikilink")，對[宗教的新態度以及追求](../Page/宗教.md "wikilink")[個人自由的精神](../Page/個人自由.md "wikilink")，帶來了對性自由的渴望，並且在第二次世界大戰後得到[西方世界的廣泛接受](../Page/西方世界.md "wikilink")。[1960年代的](../Page/1960年代.md "wikilink")[性革命](../Page/性革命.md "wikilink")，[自由戀愛的倡導](../Page/自由戀愛.md "wikilink")，隨之以來的[民權運動及](../Page/民權運動.md "wikilink")[種族隔離終結](../Page/種族隔離.md "wikilink")，促成[政治和](../Page/政治.md "wikilink")[經濟上的](../Page/經濟.md "wikilink")[性別平等](../Page/性別平等.md "wikilink")、[婦女解放運動](../Page/婦女解放運動.md "wikilink")、[LGBT權利運動以及](../Page/LGBT權利運動.md "wikilink")[避孕技術提升](../Page/避孕.md "wikilink")，為個人的私領域帶來了更多的選擇。

{{-}}

[Category:各时期历史](../Category/各时期历史.md "wikilink")
[Category:現代史](../Category/現代史.md "wikilink")