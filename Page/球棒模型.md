[thumb的塑料球棒模型](../Page/file:proline_model.jpg.md "wikilink").\]\]
[ZYKHEXAN_Zyklohexan_Rotation.gif](https://zh.wikipedia.org/wiki/File:ZYKHEXAN_Zyklohexan_Rotation.gif "fig:ZYKHEXAN_Zyklohexan_Rotation.gif")[環己烷的電腦球棒模型](../Page/環己烷.md "wikilink")。\]\]

**球棒模型**（英語：**Ball-and-stick
models**）是一種[空間填充模型](../Page/空間填充模型.md "wikilink")（space-filling
model），用來表現化學[分子的](../Page/分子.md "wikilink")[三維空間分佈](../Page/三維空間.md "wikilink")。在此作圖方式中，[線代表](../Page/線.md "wikilink")[共價鍵](../Page/共價鍵.md "wikilink")，可連結以球型表示的[原子中心](../Page/原子.md "wikilink")。\[1\]

最早的球棒分子模型是由德國化學家[奧古斯特·威廉·馮·霍夫曼](../Page/奧古斯特·威廉·馮·霍夫曼.md "wikilink")（August
Wilhelm von Hofmann）所作，目的是用來講課。

## 常見用色

並沒有任何正式的標準顏色設定，但一般會使用[CPK配色](../Page/CPK配色.md "wikilink")，下表列出一些特定[元素的常用顏色](../Page/元素.md "wikilink")：

| 元素                           | 用色 |
| ---------------------------- | -- |
| [氧](../Page/氧.md "wikilink") | 紅  |
| [碳](../Page/碳.md "wikilink") | 灰  |
| [氮](../Page/氮.md "wikilink") | 藍  |
| [氫](../Page/氫.md "wikilink") | 白  |
| [硫](../Page/硫.md "wikilink") | 黃  |
| [碘](../Page/碘.md "wikilink") | 紫  |
| [氯](../Page/氯.md "wikilink") | 綠  |
| [溴](../Page/溴.md "wikilink") | 橙  |

**元素與色彩**

## 註釋

[Category:分子建模](../Category/分子建模.md "wikilink")

1.