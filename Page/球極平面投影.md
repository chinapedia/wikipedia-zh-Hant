**球極平面投影**（），在[幾何學裏](../Page/幾何學.md "wikilink")，是一種將圓球面[投影至平面的](../Page/投影.md "wikilink")[映射](../Page/映射.md "wikilink")。在[構造地質學裏](../Page/構造地質學.md "wikilink")，稱為**球面立體投影**或**球面投影**。除了投影點以外，這投影在整個球面都有定義。在這定義域裏，這映射具有[光滑性](../Page/光滑函數.md "wikilink")、[雙射性和](../Page/雙射.md "wikilink")[共形性](../Page/共形映射.md "wikilink")。共形性的意思就是[角度維持不變](../Page/角度.md "wikilink")。但是，這映射不會維持[距離不變](../Page/距離.md "wikilink")，也不會維持[面積不變](../Page/面積.md "wikilink")；它不會維持圖案的距離與面積。

直覺而言，球極平面投影是一種以平面來看球面的方法。使用這方法，在圖案品質方面，必須接受一些不可避免的妥協。因為圓球與平面出現於許多數學方面的問題和應用，球極平面投影也非常地常見。在各個領域，例如，[複分析](../Page/複分析.md "wikilink")，[地圖學](../Page/地圖學.md "wikilink")，[地質學](../Page/地質學.md "wikilink")，與[攝影](../Page/攝影.md "wikilink")，球極平面投影都有廣泛的用處。實際上，球極平面投影經常是用[電腦繪成](../Page/電腦.md "wikilink")，或者用手工直接繪在一種特別的繪圖紙，稱為**烏爾夫網圖**。

## 公式

球面球座標投影到平面極座標：

\[(R, \Theta) = \left(\cot{\frac{\varphi}{2}}, \theta\right),\] 反解：

\[(\varphi, \theta) = \left(2 \arctan\left(\frac{1}{R}\right), \Theta\right)\]
特別地，當 *R* = 0 時，φ = π

## 圖片

<File:Stereographic> projection in 3D.png|這三維透視圖可以顯示出球極平面投影圖的製作方法。
<file:Dent> de Vaulion - 360 degree panorama.jpg|瑞士沃伦山球极平面投影图，由120张照片合成。
<file:Globe>
panorama03.jpg|[加勒比海](../Page/加勒比海.md "wikilink")[法属圣马丁首府](../Page/法属圣马丁.md "wikilink")[马里戈特的海灘](../Page/马里戈特.md "wikilink")360度球极平面投影图，由9张照片合成。

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  -
  - <http://planetmath.org/encyclopedia/StereographicProjection.html>

  - [Table of examples and properties of all common
    projections](http://www.radicalcartography.net/?projectionref), from
    radicalcartography.net

  - [three dimensional Java
    Applet](http://torus.math.uiuc.edu/jms/java/stereop/)

  - [Stereographic Projection and
    Inversion](http://www.cut-the-knot.org/pythagoras/StereoProAndInversion.shtml)
    from [cut-the-knot](../Page/cut-the-knot.md "wikilink")

  - [Examples of miniplanet panoramas, majority in
    UK](https://web.archive.org/web/20100708213142/http://www.miniplanets.co.uk/)

  - [Examples of miniplanet panoramas, majority in Czech
    Republic](http://www.miniplanet.net)

  - [Examples of miniplanet panoramas, majority in
    Poland](http://panoramy.zbooy.pl/360/?lang=e)

  - [DoITPoMS Teaching and Learning Package- "The Stereographic
    Projection"](http://www.doitpoms.ac.uk/tlplib/stereographic/index.php)

[Category:地圖投影法](../Category/地圖投影法.md "wikilink")
[Category:射影幾何](../Category/射影幾何.md "wikilink")
[Category:共形映射](../Category/共形映射.md "wikilink")