[Riken.svg](https://zh.wikipedia.org/wiki/File:Riken.svg "fig:Riken.svg")

[缩略图](https://zh.wikipedia.org/wiki/File:RIKEN_AICS_28.08.2011.jpg "fig:缩略图")
**國立研究開發法人理化学研究所**（**Institute of Physical and Chemical Research**
，[日語](../Page/日語.md "wikilink")：），簡稱**理研**或**RIKEN**。是日本資本主義之父[澀澤榮一於](../Page/澀澤榮一.md "wikilink")1917年设立，涵蓋[物理学](../Page/物理学.md "wikilink")、[化学](../Page/化学.md "wikilink")、[工学](../Page/工学.md "wikilink")、[生物学](../Page/生物学.md "wikilink")、[医科学等領域](../Page/医学.md "wikilink")，由[基礎研究至應用研究均有執行的大型](../Page/基礎研究.md "wikilink")[自然科学研究机构](../Page/自然科学.md "wikilink")。

理化学研究所的本部位於[埼玉縣](../Page/埼玉縣.md "wikilink")[和光市](../Page/和光市.md "wikilink")，在[茨城縣](../Page/茨城縣.md "wikilink")[筑波市](../Page/筑波市.md "wikilink")、[兵庫縣](../Page/兵庫縣.md "wikilink")[佐用郡](../Page/佐用郡.md "wikilink")、[神奈川縣](../Page/神奈川縣.md "wikilink")[横濱市](../Page/横濱市.md "wikilink")、[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")、[宮城縣](../Page/宮城縣.md "wikilink")[仙台市](../Page/仙台市.md "wikilink")、[愛知縣](../Page/愛知縣.md "wikilink")[名古屋市及](../Page/名古屋市.md "wikilink")[東京都](../Page/東京都.md "wikilink")[板橋區設有分所](../Page/板橋區_\(日本\).md "wikilink")\[1\]。

預算965億日圓，人数3560名（2017年4月1日）。

## 歷史

[RIKEN_building_in_Taisho_period.JPG](https://zh.wikipedia.org/wiki/File:RIKEN_building_in_Taisho_period.JPG "fig:RIKEN_building_in_Taisho_period.JPG")時期的理化學研究所\]\]

1913年，科學家[高峰讓吉倡議設立](../Page/高峰讓吉.md "wikilink")「國民科學研究所」，構想獲得澀澤榮一的支持並開始研議。

1915年，第36屆[帝國議會決議成立理化學研究所](../Page/帝國議會_\(日本\).md "wikilink")\[2\]。

1917年（[大正6年](../Page/大正6年.md "wikilink")），由日本皇室及政府補助經費，加上民間的捐款，財團法人理化學研究所正式於現今東京都文京區本駒込设立，由[伏見宮貞愛親王擔任總裁](../Page/伏見宮貞愛親王.md "wikilink")、[菊池大麓出任所長](../Page/菊池大麓.md "wikilink")。

[鈴木梅太郎](../Page/鈴木梅太郎.md "wikilink")、[寺田寅彦](../Page/寺田寅彦.md "wikilink")、[中谷宇吉郎](../Page/中谷宇吉郎.md "wikilink")、[長岡半太郎](../Page/長岡半太郎.md "wikilink")、[嵯峨根遼吉](../Page/嵯峨根遼吉.md "wikilink")、[池田菊苗](../Page/池田菊苗.md "wikilink")、[本多光太郎](../Page/本多光太郎.md "wikilink")、[湯川秀樹](../Page/湯川秀樹.md "wikilink")、[朝永振一郎](../Page/朝永振一郎.md "wikilink")、[仁科芳雄](../Page/仁科芳雄.md "wikilink")、[菊池正士知名科學家皆在此參與研究](../Page/菊池正士.md "wikilink")。

在[第二次世界大戰前曾經形成以](../Page/第二次世界大戰.md "wikilink")[理研控股為名的企業集團](../Page/理研集團.md "wikilink")（[十五大財閥之一](../Page/財閥#十五大財閥.md "wikilink")），[二战期间曾为](../Page/二战.md "wikilink")[日本核研究的研究机构](../Page/日本核研究.md "wikilink")。戰後[日本盟軍佔領時期](../Page/日本盟軍佔領時期.md "wikilink")，理研遭到[駐日盟軍總司令部](../Page/駐日盟軍總司令部.md "wikilink")（GHQ）解散。

1958年（昭和33年），以[特殊法人](../Page/特殊法人.md "wikilink")「理化學研究所」之名義重新出發，於2003年（平成15年）10月改組為[文部科學省轄下的](../Page/文部科學省.md "wikilink")[獨立行政法人](../Page/獨立行政法人.md "wikilink")「獨立行政法人理化學研究所」。

## 學術醜聞

2014年起發生該所研究員[小保方晴子的](../Page/小保方晴子.md "wikilink")[STAP細胞醜聞](../Page/STAP細胞.md "wikilink")，導致胚胎學研究者，理化学研究所再生中心副所長[笹井芳樹自殺](../Page/笹井芳樹.md "wikilink")。\[3\]

## 歷任理事長

### 特殊法人時期

<table>
<thead>
<tr class="header">
<th><p>任別</p></th>
<th><p>姓名</p></th>
<th><p>任期</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1任</p></td>
<td><p><a href="../Page/長岡治男.md" title="wikilink">長岡治男</a></p></td>
<td><p>1958年10月 - 1966年10月</p></td>
<td><p><a href="../Page/日本合板船.md" title="wikilink">日本合板船社長</a>、<a href="../Page/長岡半太郎.md" title="wikilink">長岡半太郎的長男</a></p></td>
</tr>
<tr class="even">
<td><p>第2任</p></td>
<td></td>
<td><p>1966年12月 - 1970年4月</p></td>
<td><p><a href="../Page/大阪大學.md" title="wikilink">大阪大學名譽教授</a>、校長</p></td>
</tr>
<tr class="odd">
<td><p>第3任</p></td>
<td><p><a href="../Page/星野敏雄.md" title="wikilink">星野敏雄</a></p></td>
<td><p>1970年4月 - 1975年4月</p></td>
<td><p><a href="../Page/東京工業大學.md" title="wikilink">東京工業大學教授</a></p></td>
</tr>
<tr class="even">
<td><p>第4任</p></td>
<td></td>
<td><p>1975年4月 - 1980年4月</p></td>
<td><p><a href="../Page/千葉工業大學.md" title="wikilink">千葉工業大學理事長</a></p></td>
</tr>
<tr class="odd">
<td><p>第5任</p></td>
<td><p><a href="../Page/宮島龍興.md" title="wikilink">宮島龍興</a></p></td>
<td><p>1980年4月 - 1988年4月</p></td>
<td><p><a href="../Page/筑波大學.md" title="wikilink">筑波大學学長</a></p></td>
</tr>
<tr class="even">
<td><p>第6任</p></td>
<td><p><a href="../Page/小田稔.md" title="wikilink">小田稔</a></p></td>
<td><p>1988年4月 - 1993年9月</p></td>
<td><p><a href="../Page/東京大學.md" title="wikilink">東京大學教授</a>、原教授</p></td>
</tr>
<tr class="odd">
<td><p>第7任</p></td>
<td><p><a href="../Page/有馬朗人.md" title="wikilink">有馬朗人</a></p></td>
<td><p>1993年10月 - 1998年6月</p></td>
<td><p>東京大學名譽教授、原東京大學校長</p></td>
</tr>
<tr class="even">
<td><p>第8任</p></td>
<td><p><a href="../Page/小林俊一.md" title="wikilink">小林俊一</a></p></td>
<td><p>1998年8月 - 2003年9月</p></td>
<td><p>東京大學名譽教授</p></td>
</tr>
</tbody>
</table>

### 獨立行政法人時期

| 任別   | 姓名                                 | 任期                 | 備註                                              |
| ---- | ---------------------------------- | ------------------ | ----------------------------------------------- |
| 第9任  | [野依良治](../Page/野依良治.md "wikilink") | 2003年10月 - 2015年2月 | 2001年獲得[諾貝爾化學獎得主](../Page/諾貝爾化學獎.md "wikilink") |
| 第10任 | [松本紘](../Page/松本紘.md "wikilink")   | 2015年3月 - 迄今       | 前[京都大學總長](../Page/京都大學.md "wikilink")           |

## 參考資料

## 參看

## 外部連結

  - [理化學研究所官方網站](http://www.riken.go.jp/index_j.html)

[Category:日本獨立行政法人](../Category/日本獨立行政法人.md "wikilink")
[Category:日本研究機構](../Category/日本研究機構.md "wikilink")

1.  <http://www.riken.go.jp/r-world/info/inquiry/index.html>
2.  [理化学研究所設置に関する建議案委員会. 第36回帝国議会. 【6月】. 大正4年6月7日 · 第1号 · 大正4年6月8日 ·
    第2号.](http://teikokugikai-i.ndl.go.jp/SENTAKU/syugiin/036/7241/main.html)
    帝国議会会議録検索システム
3.