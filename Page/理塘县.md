**理塘县**（）是[中国](../Page/中国.md "wikilink")[四川省](../Page/四川省.md "wikilink")[甘孜藏族自治州辖县](../Page/甘孜藏族自治州.md "wikilink")，位于甘孜州西南部，舊名**理化縣**，屬[西康省](../Page/西康省.md "wikilink")，在[雅江縣西二百八十里](../Page/雅江縣.md "wikilink")，人口约4.75万（2001年），[海拔](../Page/海拔.md "wikilink")4014米。境內漠番雜處，有藏傳佛教大寺，為川藏間往來要道，土地平闊，人煙稠密，商業稱盛。

理塘诞生过许多[藏族名人](../Page/藏族.md "wikilink")，包括[格桑嘉措](../Page/格桑嘉措.md "wikilink")、[楚臣嘉措](../Page/楚臣嘉措.md "wikilink")、[外蒙古的](../Page/外蒙古.md "wikilink")[哲布尊丹巴](../Page/哲布尊丹巴.md "wikilink")、[拉卜楞寺](../Page/拉卜楞寺.md "wikilink")[第五世嘉木样活佛等](../Page/第五世嘉木样活佛.md "wikilink")。

主要寺院有[长青春科尔寺等](../Page/长青春科尔寺.md "wikilink")。理塘作為地名出現在六世[達賴喇嘛](../Page/達賴喇嘛.md "wikilink")[倉央嘉措的情詩中](../Page/倉央嘉措.md "wikilink")。

## 沿革\[1\]

  - 元至元九年（1272年）置[李唐州](../Page/李唐州.md "wikilink")，后设奔不儿亦思刚招讨使司（萬戶府）；元二十五年（1288年）設錢糧總管府。
  - 明置里塘宣撫司，後為扎兀東思麻千戶所；明末清初為固始汗屬地；
  - 清康熙四十八年（1709年）設正副營官，屬青海岱慶和碩齊部。康熙五十八年（1719年），大兵經裹塘，青海遣<u>達瓦藍占巴</u>據此阻師，誘斬之，附近裏塘之<u>瓦述毛丫</u>，<u>瓦述祟喜</u>各土司，皆呈戶口納糧，後內附[四川省](../Page/四川省.md "wikilink")；雍正七年（1729年）置裏塘正副宣撫司，屬打箭爐廳；乾隆五十七年(1792年)設糧務委員。光緒三十二年（1906年）置[順化縣](../Page/順化縣.md "wikilink")；三十四年(1908年)奏准置[裏化廳](../Page/裏化廳.md "wikilink")，轄稻壩（稻城）、定鄉（鄉城）、順化（理塘）等縣。宣統三年(1911年)升裹化府。
  - 民國二年（1913年），易**裏**為**理**，置理化縣，屬川邊特別行政區，十四年（1925年），改屬西康西康道；國民政府成立，廢道，直屬西康省政府。
  - 1951年12月14日，更名為理塘縣。\[2\]

## 地理

理塘县属高原型副极地气候，日夜温差大，日照终年充足。冬寒冷干燥，夏凉而湿润；降雨集中在6月至9月。1月平均气温−5.8℃，7月平均气温10.6℃，年平均气温
3.25℃。

## 行政区划

理塘县辖1个镇、23个乡：

  - 镇：高城镇。
  - 乡：君坝乡、哈依乡、觉吾乡、莫坝乡、亚火乡、绒坝乡、呷柯乡、奔戈乡、村戈乡、禾尼乡、曲登乡、喇嘛垭乡、章纳乡、上木拉乡、下木拉乡、中木拉乡、濯桑乡、甲洼乡、藏坝乡、格木乡、拉波乡、麦洼乡、德巫乡。

## 外部链接

  - <http://www.dreams-travel.com/sc/sc_ganzi/litang/>
  - <https://web.archive.org/web/20050926110851/http://www.sichuantour.com/scene/detail.asp?ID=357>

## 参考文献

[Category:四川省县份](../Category/四川省县份.md "wikilink")
[Category:西康省縣份](../Category/西康省縣份.md "wikilink")
[Category:甘孜县份](../Category/甘孜县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")
[\*](../Category/理塘县.md "wikilink")

1.  段木干主編.《中外地名大辭典》（1至5冊）.人文出版社,民國70年06月.第3359頁
2.  丹珠昂奔 周潤年 莫福山 李雙劍主編.《藏族大辭典》.甘肅人民出版社,2003年02月第1版.第1079頁