[Ideal_gas_isotherms.png](https://zh.wikipedia.org/wiki/File:Ideal_gas_isotherms.png "fig:Ideal_gas_isotherms.png")\]\]

在[熱力學裏](../Page/熱力學.md "wikilink")，描述[理想氣體宏觀物理行為的](../Page/理想氣體.md "wikilink")[状态方程稱為](../Page/状态方程.md "wikilink")**理想氣體狀態方程**（ideal
gas equation of
state）。[理想气体定律表明](../Page/理想气体定律.md "wikilink")，理想氣體狀態方程為\[1\]

\[{p} {V} = {n} {R} {T} = {N} {K} {T}\]；

其中，\(p\)為理想气体的[-{A](../Page/压强.md "wikilink")，\(V\)为理想气体的[体积](../Page/体积.md "wikilink")，\(n\)為气体[物质的量](../Page/物质的量.md "wikilink")(通常是[-{A](../Page/摩尔_\(单位\).md "wikilink"))，\(R\)为[理想气体常数](../Page/理想气体常数.md "wikilink")，\(T\)為理想气体的热力学[温度](../Page/温度.md "wikilink")，\(K\)为[波尔兹曼常数](../Page/波尔兹曼常数.md "wikilink")，\(N\)表示气体粒子数。

理想氣體方程以变量多、适用范围广而著称，對於很多種不同狀況，理想氣體狀態方程都可以正確地近似實際[氣體的物理行為](../Page/氣體.md "wikilink")，包括常温常压下的空气也可以近似地适用。

理想气体定律是建立於[-{A](../Page/玻意耳-马略特定律.md "wikilink")、[查理定律](../Page/查理定律.md "wikilink")、[盖-吕萨克定律等人提出的经验定律](../Page/盖-吕萨克定律.md "wikilink")。最先由物理學者埃米爾·克拉佩龍於1834年提出\[2\]。奧格斯特·克羅尼格（August
Krönig）於1856年、[魯道夫·克勞修斯於](../Page/魯道夫·克勞修斯.md "wikilink")1857年分別獨立地從[氣體動理論推導出理想气体定律](../Page/分子運動論.md "wikilink")。\[3\]

## 应用

一定量处于平衡态的[气体](../Page/气体.md "wikilink")，其状态与[-{A](../Page/压强.md "wikilink")、[*V*和](../Page/体积.md "wikilink")[*T*有关](../Page/温度.md "wikilink")，表达这几个量之间的关系的方程称为气体的状态方程，不同的气体有不同的状态方程。但真实气体的方程通常十分复杂，而理想气体的状态方程具有非常简单的形式。

在普通狀況，像[標準狀況](../Page/標準狀況.md "wikilink")，大多數實際氣體的物理行為近似於理想氣體。在合理容限內，很多種氣體，例如[氢气](../Page/氢气.md "wikilink")、[氧气](../Page/氧气.md "wikilink")、[氮气](../Page/氮气.md "wikilink")、[惰性氣體等等](../Page/稀有气体.md "wikilink")，以及有些較重氣體，例如[二氧化碳](../Page/二氧化碳.md "wikilink")，都可以被視為理想氣體。一般而言，在較高溫度，較低壓強，氣體的物理行為比較像理想氣體。這是因為，對抗分子間作用力的[機械功](../Page/機械功.md "wikilink")，與粒子的[動能相比](../Page/動能.md "wikilink")，變得較不顯著；另外，[分子的大小](../Page/分子.md "wikilink")，與分子與分子之間的相隔空間相比，也變得較不顯著。\[4\]

## 研究过程

在17和18世纪，许多科学家对低压气体经过不断地试验、观察、归纳总结，通过汇集许多双变量的实验定律，推導出了理想气体定律。\[5\]\[6\]

### \-{A|zh-hans:玻意耳-马略特定律;zh-hant:波以耳定律}-

[BoyleLaw_J_TestTube.png](https://zh.wikipedia.org/wiki/File:BoyleLaw_J_TestTube.png "fig:BoyleLaw_J_TestTube.png")

1662年，英国化学家[-{A使用J型玻璃管进行实验](../Page/罗伯特·波义耳.md "wikilink")：用[水银压缩被密封于玻璃管内的空气](../Page/水银.md "wikilink")。加入水银量的不同会使其中空气所受的压力也不同。在實驗中，-{A|zh-hans:波义耳;zh-hant:波以耳;zh-hk:玻意耳}--经过观察管内空气的体积随水银柱高度不同而发生的变化，记录了如下一组数据：\[7\]

|                                                            |
| ---------------------------------------------------------- |
| 一定量空气在室温、大气压为29.1 [inHg下](../Page/英寸汞柱.md "wikilink")\[8\] |
| *l*(刻度读数)                                                  |
| Δ*h*/(in Hg)                                               |

*l*與Δ*h*的實驗數據

经过观察，他认为在管粗细均匀的情况下，管中空气的体积与空气柱 *l*
成正比，而空气所受压力为大气压与水银柱压差Δ*h*的和；据此，他认为在恒温下，一定量的空气所受的压力与气体的体积成反比。\[9\]

其他兩位科學家，贝蒂和布里兹曼也研究了氢气的体积和压力的关系，下面是他们的实验数据：\[10\]

| 100℃                | 10℃                |
| ------------------- | ------------------ |
| \(\tfrac{V}{dm^3}\) | \(\tfrac{p}{atm}\) |
| 2.000               | 15.28              |
| 1.000               | 30.52              |
| 0.667               | 45.75              |
| 0.500               | 60.99              |
| 0.400               | 76.26              |

氢气的pV乘积

多种气体的试验均得到了相同的结果，这个结果总结为-{A|zh-hans:玻意耳-马略特定律;zh-hant:波以耳定律}-，即：温度恒定时，一定量气体的压力和它的体积的乘积为恒量。\[11\]

数学表达式为：\({pV}=\)恒量（n、T恒定）或\({p_1} {V_1} = {p_2} {V_2}\)（\({n_1}={n_2}\)、\({T_1}={T_2}\)）。\[12\]

### 查理定律

[查理定律](../Page/查理定律.md "wikilink")，又稱查理--{zh-cn:盖-吕萨克;zh-tw:給呂薩克;}-定律，是[盖-吕萨克在](../Page/盖-吕萨克.md "wikilink")1802年發布，但他參考了的1787年研究，故後來該定律多稱作查理定律。\[13\]

1787年，查理研究[氧气](../Page/氧气.md "wikilink")、[氮气](../Page/氮气.md "wikilink")、[氢气](../Page/氢气.md "wikilink")、[二氧化碳等气体及](../Page/二氧化碳.md "wikilink")[空气从](../Page/空气.md "wikilink")0℃加热到100℃时的膨胀情况，发现在压力不太大时，任何气体的膨胀速率是一样的，而且是[摄氏温度的线性函数](../Page/摄氏温标.md "wikilink")。即某一气体在100℃中的体积为\({V_{100}}\)，而在0℃时为\({V_0}\)，经过实验，表明任意气体由0℃升高到100℃，体积增加37%\[14\]
。数学表达式为：

\(\frac{V_{100}-V_0}{V_0}=0.366=\frac{100}{273}\)

推广到一般情况，若t℃是体积为\({V_t}\)，代替\({V_{100}}\)，则有：

\(\frac{V_t-V_0}{V_0}=\frac{t}{273}\)或\({V_t}={V_0}\left(1+\frac{t}{273}\right)\)

即：恒压时，一定量气体每升高1℃，它的体积膨胀了0℃时的\(\frac{1}{273}\)\[15\]。
当时查理认为是膨胀\(\frac{1}{267}\)，1847年[法国](../Page/法国.md "wikilink")[化学家](../Page/化学家.md "wikilink")[雷诺将其修正为](../Page/雷诺.md "wikilink")\(\frac{1}{273.15}\)。

### \-{A|zh-hans:盖吕萨克定律;zh-hant:給呂薩克定律}-

。

### \-{A|zh-hans:查理-盖吕萨克定律;zh-hant:查理-給呂薩克定律}-

\-{A|zh-hans:查理-盖吕萨克定律;zh-hant:給呂薩克定律}-是近1个世纪后，物理学家[克劳修斯和](../Page/克劳修斯.md "wikilink")[开尔文建立了](../Page/威廉·湯姆森.md "wikilink")[热力学第二定律](../Page/热力学第二定律.md "wikilink")，并提出了[热力学温标](../Page/热力学温标.md "wikilink")（即[绝对温标](../Page/绝对温标.md "wikilink")）的概念，后来，-{A|zh-hans:查理-盖吕萨克气体定律;zh-hant:給呂薩克氣體定律}-被表述为：压力恒定时，一定量气体的体积（*V*）与其温度（*T*）成正比。其数学表达式为：\[16\]

\(\frac{V}{T}=\)n（n为恒量）\[17\]

或\(\frac{V_1}{T_1}=\frac{V_2}{T_2}\)（n不变）\[18\]

### 综合

19世纪中叶，法国科学家克拉珀龙综合[-{A和](../Page/波义耳定律.md "wikilink")[-{A](../Page/查理-盖吕萨克定律.md "wikilink")，把描述气体状态的3个参数：[p](../Page/压力.md "wikilink")、[V](../Page/体积.md "wikilink")、[T归于一个方程式](../Page/温度.md "wikilink")，表述为：一定量气体，体积和压力的乘积与热力学温度成正比。\[19\]

推导过程如下：设某气体原始状态是\(p_1\)、\(V_1\)、\(T_1\)，最终状态为\(p_2\)、\(V_2\)、\(T_2\)；\[20\]

首先假定温度\(T_1\)不变，则\(p_1V_1=p_2V^\prime\)；\[21\]

接着假设压力\(p_2\)不变，则\(\frac{V^\prime}{T_1}=\frac{V_2}{T_2}\)或\(V^\prime=V_2\frac{T_1}{T_2}\)\[22\]

将\(V^\prime\)带入第一步，得\(\frac{p_1V_1}{T_1}=\frac{p_2V_2}{T_2}=\)恒量\[23\]

在这个方程中，对于1
[mol的气体](../Page/摩尔_\(单位\).md "wikilink")，恒量为R，而n(mol)的气体，恒量为nR，R称为摩尔气体常数。\[24\]

### 推广

经过Horstmam和[门捷列夫](../Page/门捷列夫.md "wikilink")（門得列夫）等人的支持和提倡，19世纪末，人们开始普遍地使用现行的理想气体状态方程：\(pV=nRT\)\[25\]

## 理想气体常数

[理想气体常数](../Page/理想气体常数.md "wikilink")（或称[摩尔气体常数](../Page/摩尔气体常数.md "wikilink")、[普适气体常数](../Page/普适气体常数.md "wikilink")）的数值随p和V的单位不同而异，以下是几种常见的表述\[26\]：

\(\begin{matrix}R=\frac{pV}{nT} & = & 8.314 Pa\cdot{m^3}\cdot{mol^{-1}}\cdot{K^{-1}} \\ \ & = & 0.0831 bar\cdot{dm^3}\cdot{mol^{-1}}\cdot{K^{-1}} \\ \ & = & 0.0821 atm\cdot{L}\cdot{mol^{-1}}\cdot{K^{-1}} \\ \ & = & 62.4 mmHg\cdot{dm^3}\cdot{mol^{-1}}\cdot{K^{-1}} \\ \ & = & 8.314 J\cdot{mol^{-1}}\cdot{K^{-1}} \\ \ & = & 1.987 cal\cdot{mol^{-1}}\cdot{K^{-1}} \end{matrix}\)

## 使用到该方程的定律

### \-{A|zh-hans:阿伏伽德罗定律;zh-hant:亞佛加厥定律}-

\-{A|zh-hans:阿伏伽德罗定律;zh-hant:亞佛加厥定律}-是[亞佛加厥假說在](../Page/阿伏伽德罗定律.md "wikilink")19世纪末由[气体分子运动论给予理论证明后才成为定律](../Page/气体分子运动论.md "wikilink")。它被表述为：在相同的温度与相同的压力下，相同体积的气体所含物质的量相同。\[27\]

通过理想气体方程很容易导出这个定律：若有A、B两种气体，它们的气体方程分别是\(p_AV_A=n_ART_A\)和\(p_BV_B=n_BRT_B\)，当\(p_A=p_B, T_A=T_B, V_A=V_B\)时，显然\(n_A=n_B\)。这个定律也是理想气体方程的一个例证。\[28\]

### 气体分压定律

气体分压定律是1807年由[道尔顿首先提出的](../Page/道尔顿.md "wikilink")，因此也叫[道尔顿分压定律](../Page/道尔顿分压定律.md "wikilink")。这个定律在现代被表述为：在温度与体积恒定时，混合气体的总压力等于组分气体分压力之和，各别气体分压等于该气体占据总气体体积时表现的-{A|zh-hans:压强;zh-hant:壓力}-。\[29\]

使用数学方程表示为

\(p_{sum}=\sum{p_i}\)和\(p_i=p_{sum}\times\frac{n_i}{\sum{n_i}}=p_{sum}\times\frac{V_i}{\sum{V_i}}\)。

道尔顿分压定律还可以表现为：气体分压与总压比等于该气体的-{A|zh-hans:摩尔;zh-hant:莫耳}-数。同样，
单一组分气体在同温同压下，与混合气体总体积之比等于-{A|zh-hans:摩尔;zh-hant:莫耳}-分数之比，继而等于分压与总压之比。\[30\]

在恒温、恒体积的条件下，

将\(pV=nRT\)代入\(p_{sum}=\sum{p_i}\)，

可得\(p_{sum}=\sum{p_i}=\frac{\sum{n_i}RT}{V}\)，

易得\(\frac{p_i}{p_{sum}}=\frac{n_i}{\sum{n_i}}=\frac{n_A}{n_{sum}}\)或\(p_i=p_{sum}\times\frac{n_i}{n_{sum}}\)。

当温度与压力相同的条件下，由于\(V_{sum}=\sum{V_i}\)，代入\(pV=nRT\)，

易得\(\frac{n_i}{n_{sum}}=\frac{n_A}{\sum{n_i}}=\frac{V_i}{\sum{V_i}}=\frac{V_i}{V_{sum}}\)，

代入\(\frac{p_i}{p_{sum}}=\frac{n_i}{\sum{n_i}}=\frac{n_A}{n_{sum}}\)或\(p_i=p_{sum}\times\frac{n_i}{n_{sum}}\)，

可得\(p_i=p_{sum}\times\frac{V_i}{V_{sum}}\)或\(V_i=V_{sum}\times\frac{p_i}{p_{sum}}\)。

## 实际气体中的问题

当理想气体状态方程运用于实际气体时会有所偏差，因为理想气体的基本假设在实际气体中并不成立。如实验测定1
mol[乙炔在](../Page/乙炔.md "wikilink")20℃、101kPa时，体积为24.1
dm<sup>3</sup>，\(pV=2420 kPa\cdot{dm^3}\)，而同样在20℃时，在842 kPa下，体积为0.114
dm<sup>3</sup>，\(pv=960 kPa\cdot{dm^3}\)，它们相差很多，这是因为，它不是理想气体所致。\[31\]

一般来说，[沸点低的气体在较高的温度和较低的压力时](../Page/沸点.md "wikilink")，更接近理想气体，如[氧气的沸点为](../Page/氧气.md "wikilink")-183℃、[氢气沸点为](../Page/氢气.md "wikilink")-253℃，它们在常温常压下[-{A与理想值仅相差](../Page/摩尔体积.md "wikilink")0.1%左右，而[二氧化硫的沸点为](../Page/二氧化硫.md "wikilink")-10℃，在常温常压下[-{A与理想值的相差达到了](../Page/摩尔体积.md "wikilink")2.4%。\[32\]

### 压缩係数

由于实际气体和理想值之间存在偏差，因此常用压缩係数Z表示实际气体的实验值和理想值之间的偏差，计算Z的方程为：\(Z=\frac{pV}{nRT}\)。\[33\]

当气压很低时，各种气体的性质都接近于理想气体，随压力升高，各种气体偏离理想状态的情况不同，压缩係数Z便会随之改变。\[34\]

Z受到两个因素的影响：

1.  实际气体分子间的吸引力会使其对器壁碰撞而产生的压力比理想值小，这会使Z减小；
2.  实际气体分子所占用的空间体积使实测体积一定大于理想状态，这会使Z增大。\[35\]

这两个因素有时会互相抵消，使气体在一定状态下十分接近于理想气体，如[二氧化碳在](../Page/二氧化碳.md "wikilink")40℃、52
MPa时，Z≈1。\[36\]

### \-{A|zh-hans:范德瓦耳斯方程;zh-hant:凡德瓦方程式;zh-hk:范氏方程}-

[VdW-Attr-Repul_SVG_path.svg](https://zh.wikipedia.org/wiki/File:VdW-Attr-Repul_SVG_path.svg "fig:VdW-Attr-Repul_SVG_path.svg")气体70°C时的p-V等温线\]\]

\-{A|zh-hans:范德瓦耳斯方程;zh-hant:凡德瓦方程式;zh-hk:范氏方程}-是[荷兰](../Page/荷兰.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")[約翰內斯·范德瓦耳斯根据以上观点于](../Page/約翰內斯·范德瓦耳斯.md "wikilink")1873年提出的一种[实际气体状态方程](../Page/实际气体状态方程.md "wikilink")，这个方程通常有两种形式：\[37\]

其具体形式为\(\left(p + \frac{a'}{v^2}\right)\left(v-b'\right) = kT\)\[38\]

其中与理想气体状态方程不同的几个参数为：

  - a' 为度量分子间引力的[唯象参数](../Page/唯象理論.md "wikilink")
  - b' 为单个分子本身包含的体积
  - v 为每个分子平均占有的空间大小（即气体的体积除以总分子数量）
  - k 为[-{A](../Page/玻尔兹曼常数.md "wikilink")

而更常用的形式为\[\left(p + a \frac{n^2}{V^2}\right)\left(V-nb\right) = nRT\]

其中几个参数为：

  - V 为总体积
  - a 为度量分子间引力的参数\(a=N_A^2 a'\)
  - b 为1摩尔分子本身包含的体积之和\(b=N_A b'\)
  - \(N_A\)为亞佛加厥常数。

a和b都是常数，叫做[范德瓦耳斯常数](../Page/范德瓦耳斯常数.md "wikilink")，其中a用于校正压力，b用于修正体积。\[39\]

在较低的压力情况下，理想气体状态方程是范德瓦耳斯方程的一个良好近似。而随着气体压力的增加，范氏方程和理想气体方程结果的差别会变得十分明显。\[40\]

## 参见

  - [热力学](../Page/热力学.md "wikilink")
  - [气体化合体积定律](../Page/气体化合体积定律.md "wikilink")
  - [气体扩散定律](../Page/气体扩散定律.md "wikilink")
  - [气体分子运动论](../Page/气体分子运动论.md "wikilink")
  - [完全气体](../Page/完全气体.md "wikilink")

## 註釋

## 參考文獻

### 具体引用

### 一般参考

  -
## 外部链接

  - [理想氣體方程式](http://content.edu.tw/senior/chemistry/tp_sc/content1/number1/3/3-8.htm)

  - [電腦實驗](https://web.archive.org/web/20050524122427/http://students.puiching.edu.mo/chemclub/new/index.php?mod=exp)


{{-}}

[L](../Category/热力学.md "wikilink") [L](../Category/方程.md "wikilink")
[L](../Category/物理化学.md "wikilink") [L](../Category/气体定律.md "wikilink")

1.

2.  [Facsimile at the Bibliothèque nationale de France
    (pp. 153–90).](http://gallica.bnf.fr/ark:/12148/bpt6k4336791/f157.table)

3.

4.

5.
6.
7.
8.
9.
10.
11.
12.
13. . [English translation
    (extract).](http://web.lemoyne.edu/~giunta/gaygas.html)

14. J. Dalton (1802) ["Essay II. On the force of steam or vapour from
    water and various other liquids, both in vacuum and in air" and
    Essay IV. "On the expansion of elastic fluids by
    heat,"](https://books.google.com/books?id=3qdJAAAAYAAJ&pg=PA595#v=onepage&q&f=false)
    *Memoirs of the Literary and Philosophical Society of Manchester*,
    vol. 5, pt. 2, pages 550-574 and pages 595–602.

15. .

16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26. [工程热力学 - 朱明善 - Google
    图书](https://books.google.com.hk/books?id=270U7NS4kEgC&lpg=PP1&hl=zh-CN&pg=PA73#v=onepage&q&f=false)

27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.
38. Cross, Michael, [*First Order Phase
    Transitions*](http://www.pma.caltech.edu/~mcc/Ph127/b/Lecture3.pdf)

39.
40.