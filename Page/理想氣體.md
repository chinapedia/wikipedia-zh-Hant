**理想氣體**為假想的[气体](../Page/气体.md "wikilink")。其假設為：

  - 氣體分子本身不占有[體積](../Page/體積.md "wikilink")
  - 氣體分子持續以直線運動，並且與容器器壁間發生[彈性碰撞](../Page/彈性碰撞.md "wikilink")，因而對器壁施加[壓力](../Page/壓力.md "wikilink")
  - 氣體分子間無作用力，亦即不吸引也不排斥
  - 氣體分子的平均能量與[开尔文溫度成正比](../Page/开尔文.md "wikilink")

其特性為：

  - 理想氣體適用理想氣體狀態方程式
  - 理想氣體絕不液化或固化

真實氣體在愈低壓、愈高溫的狀態，氣體分子間作用力愈小，性質愈接近理想氣體。最接近理想氣體的氣體為[氦氣](../Page/氦.md "wikilink")。

## 理想气体状态方程

**理想气体状态方程**是描述[理想气体处于](../Page/理想气体.md "wikilink")[平衡态时的](../Page/平衡态.md "wikilink")[状态方程](../Page/状态方程.md "wikilink")。他建立在[波以耳定律](../Page/波以耳定律.md "wikilink")，[查理定律](../Page/查理定律.md "wikilink")，[盖-吕萨克定律和](../Page/盖-吕萨克定律.md "wikilink")[阿伏伽德罗定律等经验定律上](../Page/阿伏伽德罗定律.md "wikilink")。

处于平衡态的[气体](../Page/气体.md "wikilink")，其状态可以用两个独立变数，[压强P和](../Page/压强.md "wikilink")[体积V](../Page/体积.md "wikilink")，来描写它的平衡态，[温度T是](../Page/温度.md "wikilink")[压强P和](../Page/压强.md "wikilink")[体积V的函数](../Page/体积.md "wikilink")，表达这几个量之间的关系的方程称之为气体的状态方程。不同的气体有不同的状态方程。这些方程通常很复杂。但在压强很小，温度不太高也不太低的情况下，各种气体的行为都趋于理想气体。理想气体的状态方程具有非常简单的形式。

理想气体状态方程一般写作

  -
    <font size=5>\({p} {V} = {n} {R} {T}\)</font> 或
    <font size=5>\({p} {V} = {N} {k} {T}\)</font>

<!-- end list -->

  -
    其中：
      -
        *P*表理想氣體的[压强](../Page/压强.md "wikilink")，[單位為](../Page/国际单位制.md "wikilink")[標準大氣壓](../Page/標準大氣壓.md "wikilink")（[atm](../Page/atm.md "wikilink")）；
        *V*表理想氣體的[體積](../Page/體積.md "wikilink")，單位為[公升](../Page/公升.md "wikilink")（L）；
        *n*表理想氣體的[物質的量](../Page/物質的量.md "wikilink")，單位為[莫耳](../Page/莫耳.md "wikilink")（mol）；
        *R*為[理想氣體常數](../Page/理想氣體常數.md "wikilink")，約為0.082
        atm·L·mol<sup>−1</sup>·K<sup>−1</sup>；
        *k*为[波尔兹曼常数](../Page/波尔兹曼常数.md "wikilink")，*k* = 1.38066 x
        10<sup>−23</sup> J/K = 8.617385 × 10<sup>−5</sup> eV/K；
        N表单位体积气体粒子数
        *T*表理想氣體的[溫度](../Page/溫度.md "wikilink")，單位為[开尔文溫度](../Page/开尔文溫度.md "wikilink")（K）。

## 参见

  - [理想气体状态方程](../Page/理想气体状态方程.md "wikilink")
  - [阿伏伽德罗定律](../Page/阿伏伽德罗定律.md "wikilink")
  - [玻意耳定律](../Page/玻意耳定律.md "wikilink")
  - [查理定律](../Page/查理定律.md "wikilink")
  - [盖-吕萨克定律](../Page/盖-吕萨克定律.md "wikilink")
  - [格銳目定律](../Page/格銳目定律.md "wikilink")
  - [亨利定律](../Page/亨利定律.md "wikilink")
  - [完全气体](../Page/完全气体.md "wikilink")

真实气体的状态方程参见：

  - [范德瓦耳斯方程](../Page/范德瓦耳斯方程.md "wikilink")
  - [昂内斯方程](../Page/昂内斯方程.md "wikilink")

[Category:气体](../Category/气体.md "wikilink")
[Category:热力学](../Category/热力学.md "wikilink")