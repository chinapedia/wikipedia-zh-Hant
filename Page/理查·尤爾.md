理查·尤爾（，）是[南北戰爭期間的](../Page/南北戰爭.md "wikilink")[邦聯](../Page/邦聯.md "wikilink")（南軍）[中將](../Page/中將.md "wikilink")，為[石牆傑克森和](../Page/石牆傑克森.md "wikilink")[李將軍的手下之一](../Page/李將軍.md "wikilink")，但後於[蓋茨堡之役與](../Page/蓋茨堡之役.md "wikilink")[史波特斯凡尼亞郡府之役中失手](../Page/史波特斯凡尼亞郡府之役.md "wikilink")，影響其聲譽。

## 生平

### 戰前

1817年2月8日
尤爾生於[華盛頓特區的Georgetown](../Page/華盛頓特區.md "wikilink")。三歲起於[威廉王子縣長大](../Page/威廉王子縣.md "wikilink")。1840年，他於[西點軍校畢業後](../Page/西點軍校.md "wikilink")，前往[墨西哥](../Page/墨西哥.md "wikilink")[戰鬥](../Page/美墨戰爭.md "wikilink")。

### 戰時

1861年內戰之始，尤爾退出聯邦，加入維吉尼亞州的邦聯軍隊。他第一年就成了少校，因為懂得不少狡滑的戰術，而邦聯軍正好需要這一類將領。

[第一次牛奔河之役中尤爾很少投入戰事](../Page/第一次牛奔河之役.md "wikilink")。而在[河谷會戰中](../Page/河谷會戰.md "wikilink")，他的作戰方案因為有別於石牆傑克森的方案而與之爭執，但最後一切聽命於傑克森。

後，他亦參與了[七天戰役](../Page/七天戰役.md "wikilink")，[細德山之役和](../Page/細德山之役.md "wikilink")[第二次牛奔河之役等等](../Page/第二次牛奔河之役.md "wikilink")。一直以來他都因為英勇敢戰而聲名大增。1862年8月28日，尤爾於Groveton受重傷，其間成功向曾拒絕他的Lizinka
Campbell Brown求婚。不久，石牆身亡，尤爾得晉升成為邦聯軍中的第二兵團的司令。

1863年，尤爾的部隊大破從[雪倫多亞河谷侵入的北軍](../Page/雪倫多亞河谷.md "wikilink")，成為第一批北上[馬里蘭州及](../Page/馬里蘭州.md "wikilink")[賓州的南軍](../Page/賓州.md "wikilink")。他奉李將軍的指示攻擊當地的一些北軍，但又猶豫不決，最後負傷逃離戰場。

此後，尤爾再不能於大型戰役中立戰功，相反，他多次被打敗，更於1865年4月6日向北軍投降。

### 戰後

獲釋後，尤爾到
[田納西州的](../Page/田納西州.md "wikilink")[斯普林希爾](../Page/斯普林希爾.md "wikilink")
(Spring Hill)(他妻子的一個農場)度過餘生，死於1872年1月25日。

## 外部連結

  - <http://southern-lady.com/ewell.html>

[Category:美国内战军事史](../Category/美国内战军事史.md "wikilink")
[Category:美國中將](../Category/美國中將.md "wikilink")
[Category:死于肺炎的人](../Category/死于肺炎的人.md "wikilink")
[Category:美國歷史
(1865年-1918年)](../Category/美國歷史_\(1865年-1918年\).md "wikilink")
[Category:華盛頓哥倫比亞特區人](../Category/華盛頓哥倫比亞特區人.md "wikilink")
[Category:弗吉尼亚州南北战争人物](../Category/弗吉尼亚州南北战争人物.md "wikilink")
[Category:西點軍校校友](../Category/西點軍校校友.md "wikilink")
[Category:1849至1865年的美国历史](../Category/1849至1865年的美国历史.md "wikilink")
[Category:美国内战战俘](../Category/美国内战战俘.md "wikilink")