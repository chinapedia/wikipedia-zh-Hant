[Richard-A-Posner.jpg](https://zh.wikipedia.org/wiki/File:Richard-A-Posner.jpg "fig:Richard-A-Posner.jpg")

**理查德·艾倫·波斯纳**（**Richard Allen
Posner**，）是[美国联邦上诉法院](../Page/美国.md "wikilink")[法官](../Page/法官.md "wikilink")，曾任[芝加哥大学法学院教授](../Page/芝加哥大学.md "wikilink")，[法律经济学运动的重要人物](../Page/法律经济学.md "wikilink")。

## 生平

波斯纳出生於紐約的[猶太家庭](../Page/猶太.md "wikilink")，1959年自[耶鲁大学](../Page/耶鲁大学.md "wikilink")（以[summa
cum
laude成績](../Page/summa_cum_laude.md "wikilink")）畢業，主修英語，三年後的1962年並自[哈佛大学法学院第一名畢業](../Page/哈佛大学.md "wikilink")，同時任《[哈佛法律評論](../Page/哈佛法律評論.md "wikilink")》主編。曾任美国最高法院大法官[小威廉·布伦南的法律助手](../Page/小威廉·布伦南.md "wikilink")。1968年，任教于[史丹佛大学法学院](../Page/史丹佛大学.md "wikilink")，1969年，任教于[芝加哥大学法学院](../Page/芝加哥大学.md "wikilink")。

1981年，[里根总统任命波斯纳为联邦第七巡回区上诉法院法官](../Page/罗纳德·里根.md "wikilink")。

2017年9月1日，波斯納宣佈從第七巡回区退休，次日生效。\[1\]

波斯纳的著作很多，包括《法理学问题》、《超越法律》、《法律和道德理论的疑问》、《司法经济学》、《性与理性》、《法律的经济学分析》、[《法官如何思考》](http://www.ideobook.com/693/how-judges-think-chinese/)、[《资本主义的失败：〇八危机与经济萧条的降临》](http://www.ideobook.com/924/a-failure-of-capitalism-chinese/)、[《论剽窃》](http://www.ideobook.com/669/posner-little-book-of-plagiarism/)、[《并非自杀契约：国家紧急状态时期的宪法》](http://www.ideobook.com/990/posner-not-a-suicide-pact/)等。

## 參考文獻

## 外部連結

  - [與Gary Becker合著的部落格](http://www.becker-posner-blog.com)
  - [理查德·波斯纳及其中文版文集](http://posner.ideobook.com)
  - [“法与经济学”与成文法体系
    ——围绕着波斯纳的“效率性”原理展开的讨论](http://www.chinalegaltheory.com/homepage/Article_Show.asp?ArticleID=120)

[Category:美国法学家](../Category/美国法学家.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:哈佛法學院校友](../Category/哈佛法學院校友.md "wikilink")
[Category:美国法官](../Category/美国法官.md "wikilink")
[Category:纽约州共和党人](../Category/纽约州共和党人.md "wikilink")

1.