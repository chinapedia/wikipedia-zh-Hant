**理查德·哈莱克·布劳德海德**（，）是[美国著名的](../Page/美国.md "wikilink")20世纪[美国文学学者](../Page/美国文学.md "wikilink")。[杜克大学现任校长](../Page/杜克大学.md "wikilink")。

## 生平

布劳德海德1947年生于[俄亥俄州](../Page/俄亥俄州.md "wikilink")[代顿](../Page/代顿.md "wikilink")，六岁时搬家到[康涅狄格州的](../Page/康涅狄格州.md "wikilink")[费尔菲尔德](../Page/费尔菲尔德县_\(康乃狄克州\).md "wikilink")，他就学于[马萨诸塞州的](../Page/马萨诸塞州.md "wikilink")[菲利普学院](../Page/菲利普学院.md "wikilink")，1968年毕业于[耶鲁学院](../Page/耶鲁学院.md "wikilink")，1972年在[耶鲁大学获得英语博士学位](../Page/耶鲁大学.md "wikilink")。

1972年起耶鲁大学做英语系的[助理教授](../Page/助理教授.md "wikilink")，1985年生为正教授，并成为英语系系主任。1993年起到2004年做耶鲁学院的院长。2004年起担任杜克大学的第九任校长。

## 参看

  - [杜克大学](../Page/杜克大学.md "wikilink")
  - [耶鲁大学](../Page/耶鲁大学.md "wikilink")

## 外部链接

  - [布劳德海德简历](https://web.archive.org/web/20071220021209/http://www.duke.edu/president/bio/CurriculumVitae.html)

[Category:美国教育家](../Category/美国教育家.md "wikilink")
[Category:杜克大学校长](../Category/杜克大学校长.md "wikilink")
[Category:耶鲁大学校友](../Category/耶鲁大学校友.md "wikilink")
[Category:耶鲁大学教授](../Category/耶鲁大学教授.md "wikilink")
[Category:俄亥俄州代顿人](../Category/俄亥俄州代顿人.md "wikilink")
[Category:清华大学名誉博士](../Category/清华大学名誉博士.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")
[Category:菲利普斯学院校友](../Category/菲利普斯学院校友.md "wikilink")