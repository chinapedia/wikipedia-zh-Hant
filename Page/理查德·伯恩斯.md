**理查德·伯恩斯**（****，），[英格兰](../Page/英格兰.md "wikilink")[拉力赛车手](../Page/拉力赛.md "wikilink")，2001年度[世界拉力锦标赛冠军](../Page/世界拉力锦标赛.md "wikilink")，出生于[伯克郡的](../Page/伯克郡.md "wikilink")[雷丁](../Page/雷丁.md "wikilink")。

在8岁的时候，伯恩斯就开始驾驶着他父亲的在自家住宅附近的田野上驰骋了。当他11岁那年，他加入了一家17岁以下赛车俱乐部，并在1984年成为了一名赛车手。2年后，伯恩斯的父亲送他前往位于[波厄斯郡的](../Page/波厄斯郡.md "wikilink")[纽顿](../Page/纽顿.md "wikilink")（Newtown,
Powys）附近的简·丘吉尔的[威尔士森林拉力学校](../Page/威尔士.md "wikilink")（Jan Churchill's
Welsh Forest Rally
School）学习赛车技术。当时他驾驶的是[福特护卫](../Page/福特护卫.md "wikilink")（Ford
Escort）赛车。正是在这段时间里他明确了自己今后努力的目标——成为一名职业拉力车手。在他的强烈要求下，他父亲终于同意他加入他家乡雷丁当地的克雷文赛车俱乐部（Craven
Motor
Club），在那里他的才华受到了资深拉力赛车人[大卫·威廉姆斯的赏识](../Page/大卫·威廉姆斯.md "wikilink")。期间他参加了Panaround、Bagshot、中威尔士（Mid-Wales）、米尔布鲁克（Millbrook）、塞文谷（Severn
Valley）、Kayel Graphics以及寒武等多站拉力赛事。

1990年，理查德·伯恩斯加入了[标致挑战赛](../Page/标致.md "wikilink")。当时他驾驶的是大卫·威廉姆斯为他购买的[标致205](../Page/标致205.md "wikilink")
GTI（Peugeot 205
GTI）赛车。同年他还第一次参加了当年的[世界拉力锦标赛英国站](../Page/世界拉力锦标赛.md "wikilink")。

1993年他加入了[富士车队](../Page/富士.md "wikilink")，并与[阿利斯特·麦克雷一起征战英国拉力锦标赛](../Page/阿利斯特·麦克雷.md "wikilink")。当年他获得了沃克斯豪尔运动（Vauxhall
Sport）、[倍耐力](../Page/倍耐力.md "wikilink")（Pirelli）、[苏格兰和](../Page/苏格兰.md "wikilink")[马恩国际](../Page/马恩国际.md "wikilink")（Manx
International）四站拉力赛的冠军，并成为了有史以来最年轻的英国拉力锦标赛冠军。

1998年的[肯尼亚拉力赛](../Page/肯尼亚拉力赛.md "wikilink")（Safari
Rally）中，伯恩斯驾驶着[三菱卡里斯玛GT](../Page/三菱卡里斯玛.md "wikilink")（Mitsubishi
Carisma GT）获得了他的第一个世界拉力锦标赛分站冠军。

2001年11月25日理查德·伯恩斯成为了历史上第一位获得世界拉力锦标赛冠军的英格兰车手。当他以第三名的身份，驾驶着他的[速霸陸Impreza赛车冲过当年的最后一站](../Page/速霸陸Impreza.md "wikilink")——英国拉力赛的终点线后，从他口中说出的第一句话是：“我们是世界上最棒的。”（"We're
the best in the
world".）为了纪念他历史性的世界冠军，富士（斯巴鲁）汽车还特意制造了一款名为RB5的翼豹特别版在英国发售[1](https://web.archive.org/web/20051223132005/http://stefanostadal.homedns.org/car_impreza.htm)。

2003年11月，伯恩斯在威尔士拉力赛中出现眩晕症状，随后被诊断出患有一种名为[星细胞瘤的](../Page/星细胞瘤.md "wikilink")[脑瘤](../Page/脑瘤.md "wikilink")。之后他接受了[化疗和](../Page/化疗.md "wikilink")[放疗](../Page/放疗.md "wikilink")，并于2005年4月接受了据说“非常成功”的手术。

2005年11月25日，在多天的昏迷之后，伯恩斯不幸去世。这一天恰好是他夺得世界冠军的四周年纪念日。

## 外部链接

  - [官方网站](http://www.richard-burns.co.uk)

<!-- end list -->

  - BBC Sport:
      - [理查德·伯恩斯简介](http://news.bbc.co.uk/sport1/hi/motorsport/world_rally/2658543.stm)
      - [伯恩斯被诊断出患有脑瘤](http://news.bbc.co.uk/sport1/hi/motorsport/world_rally/3277637.stm)
      - [伯恩斯的手术十分成功](http://news.bbc.co.uk/sport1/hi/motorsport/world_rally/4485241.stm)
      - [伯恩斯去世](http://news.bbc.co.uk/sport1/hi/motorsport/world_rally/4472642.stm)

[category:賽車手](../Page/category:賽車手.md "wikilink")

[B](../Category/英國運動員.md "wikilink")
[Category:世界拉力锦标赛车手](../Category/世界拉力锦标赛车手.md "wikilink")