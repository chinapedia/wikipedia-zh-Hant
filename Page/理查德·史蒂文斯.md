**理查德·史蒂文斯**（William Richard (Rich)
Stevens，），美国计算机科学家，是众多的畅销[UNIX](../Page/Unix.md "wikilink")、[TCP](../Page/传输控制协议.md "wikilink")/[IP书籍的作者](../Page/网际协议.md "wikilink")。

## 生平

史蒂文斯于 1951
年出生于北罗德西亚（今[尚比亚](../Page/尚比亚.md "wikilink")）的[盧安夏](../Page/盧安夏.md "wikilink")。他的父亲在当地的一家铜厂工作。他的家曾先后搬到[盐湖城](../Page/盐湖城.md "wikilink")、新墨西哥的Hurley、[华盛顿和南非的](../Page/華盛頓哥倫比亞特區.md "wikilink")[帕拉博魯瓦](../Page/帕拉博魯瓦.md "wikilink")。史蒂文斯曾就读于位于[弗吉尼亚](../Page/弗吉尼亚.md "wikilink")[韋恩斯伯勒的](../Page/韋恩斯伯勒_\(維吉尼亞州\).md "wikilink")[Fishburne
军校](../Page/Fishburne_军校.md "wikilink")。并于1973年取得了[密执安大学的](../Page/密执安大学.md "wikilink")[航天工程工科学士学位](../Page/航天工程.md "wikilink")，于1978年取得工科硕士学位，于1982年取得了[亚利桑那大学的](../Page/亚利桑那大学.md "wikilink")[系统工程学博士学位](../Page/系统工程学.md "wikilink")。他于1975年搬到了[土桑并受雇于当地的](../Page/图森_\(亚利桑那州\).md "wikilink")
[基特峰國家天文台担任程序员工作](../Page/基特峰國家天文台.md "wikilink")，并在那里工作到1982年。
从1982年到1990年，他在位于[紐哈芬市](../Page/紐哈芬市.md "wikilink"), CT的Health
Systems
International担任计算机服务副总监。他于1990年搬回了土桑，在他的专业领域从事写作和顾问工作。　他在20世纪70年还曾狂热地从事飞行运动并担任兼职飞行教练工作。

## 著作

  - 1990年 - “UNIX Network Programming” - ISBN 0-139-49876-1
  - 1992年 - *Advanced Programming in the UNIX Environment* - ISBN
    0-201-56317-7
      - 國際中文版 ISBN 986-7727-82-7
  - 1994年 - *TCP/IP Illustrated, Volume 1: The Protocols* - ISBN
    0-201-63346-9
      - 國際中文版 初版 ISBN 957-0390-29-8 二版 ISBN 9867727878
  - 1995年 - *TCP/IP Illustrated, Volume 2: The Implementation* (with
    [Gary R. Wright](../Page/Gary_R._Wright.md "wikilink")) - ISBN
    0-201-63354-X
      - 國際中文版 上 ISBN 957-0390-32-8 下 ISBN 957-0390-35-2
  - 1996年 - *TCP/IP Illustrated, Volume 3: TCP for Transactions,
    [HTTP](../Page/HTTP.md "wikilink"),
    [NNTP](../Page/Network_News_Transfer_Protocol.md "wikilink"), and
    the [UNIX Domain](../Page/unix_domain_sockets.md "wikilink")
    Protocols* - ISBN 0-201-63495-3
  - 1998年 - *UNIX Network Programming, Volume 1, Second Edition:
    Networking APIs: [Sockets](../Page/Socket.md "wikilink") and
    [XTI](../Page/XTI.md "wikilink")* - ISBN 0-134-90012-X
      - 正體中文版《UNIX 網路程式設計：網路應用程式設計界面：Sockets 與 XTI》 ISBN 986-7790-42-1
  - 1999年 - *UNIX Network Programming, Volume 2, Second Edition:
    [Interprocess
    Communications](../Page/Interprocess_Communication.md "wikilink")* -
    ISBN 0-130-81081-9
      - 正體中文版《UNIX 網路程式設計：IPC 行程間通訊程式設計》 ISBN 986-7790-42-1

## RFC

Stevens 还协助制定了一些 [IETF](../Page/IETF.md "wikilink") 的
[RFC](../Page/RFC.md "wikilink") 文件。 —
一些[BSD套接字](../Page/BSD套接字.md "wikilink")
API针对[IPv6的更新文档](../Page/IPv6.md "wikilink") 和一个
[TCP](../Page/传输控制协议.md "wikilink") 拥塞控制方面的标准文档。

  - Stevens, W. R., and Thomas, M. 1998年. "Advanced Sockets API for
    IPv6," RFC 2292
  - Gilligan, R. E., Thomson, S., Bound, J., and Stevens, W. R. 1999年.
    "Basic Socket Interface Extensions for IPv6," RFC 2553
  - Allman, M., Paxson, V., Stevens, W. R. 1999年. "TCP Congestion
    Control," RFC 2581

## 外部链接

  - [W.Richard Stevens的主页](http://www.kohala.com/start)
  - [Rachel Chalmers](../Page/Rachel_Chalmers.md "wikilink")
    (2000年[九月一日](../Page/九月一日.md "wikilink")) 在
    [Salon.com](../Page/Salon.com.md "wikilink")
    建立的[“Unix大师中的大师”](http://dir.salon.com/tech/feature/2000/09/01/rich_stevens/index.html)
  - Stevens
    不仅仅是一个UNIX和TCP/IP方面的大师，他还是一个烤制巧克力甜点的面点师。他的[超级巧克力小甜饼](http://www.kohala.com/start/recipes/ultimatecookie.html)不比他的著作逊色。
  - [Usenet上的讣告帖子](http://groups.google.com/groups?hl=en&selm=7qt2rn%24dfd%241%40agate-ether.berkeley.edu&filter=0)

[R](../Category/1951年出生.md "wikilink")
[R](../Category/1999年逝世.md "wikilink")
[R](../Category/计算机科学家.md "wikilink")
[R](../Category/密歇根大學校友.md "wikilink")
[R](../Category/亞利桑那大學校友.md "wikilink")