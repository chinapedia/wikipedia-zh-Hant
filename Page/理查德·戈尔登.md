**小理查德·弗兰西斯·戈尔登**（，）曾是一位[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")，执行过[双子星11号和](../Page/双子星11号.md "wikilink")[阿波罗12号任务](../Page/阿波罗12号.md "wikilink")，在阿波罗12号任务中担任飞行员。\[1\]

2017年11月6日，戈尔登於[聖馬可斯過世](../Page/聖馬可斯_\(加利福尼亞州\).md "wikilink")，享壽88歲\[2\]。

## 參考資料

[G](../Category/宇航员.md "wikilink") [G](../Category/美国宇航员.md "wikilink")
[G](../Category/阿波罗计划.md "wikilink")
[G](../Category/第三组宇航员.md "wikilink")
[G](../Category/華盛頓大學校友.md "wikilink")
[Category:阿波羅12號](../Category/阿波羅12號.md "wikilink")

1.
2.