**理查德·羅伊斯·施罗克**（，），現任美国[麻省理工學院化學系正](../Page/麻省理工學院.md "wikilink")[教授](../Page/教授.md "wikilink")，因为在[有机化学中](../Page/有机化学.md "wikilink")[烯烃复分解反应的贡献](../Page/烯烃复分解反应.md "wikilink")，成为2005年度诺贝尔化学奖获得者之一\[1\]。

施罗克生于[印第安纳州的](../Page/印第安纳州.md "wikilink")，在[加利福尼亚州的](../Page/加利福尼亚州.md "wikilink")[圣迭戈上的高中](../Page/圣迭戈.md "wikilink")。1967年，在[加州大学河边分校获得](../Page/加州大学河边分校.md "wikilink")[学士学位](../Page/学士.md "wikilink")，1971年在[哈佛大学获得](../Page/哈佛大学.md "wikilink")[博士学位](../Page/博士.md "wikilink")。1971年至1972年，施罗克获得[美国国家科学基金会资助在](../Page/美国国家科学基金会.md "wikilink")[剑桥大学进行](../Page/剑桥大学.md "wikilink")[博士后学习](../Page/博士后.md "wikilink")。1972年，他受雇于[杜邦公司](../Page/杜邦公司.md "wikilink")。1975年，施罗克在[麻省理工学院任教](../Page/麻省理工学院.md "wikilink")，1980年成为正式[教授](../Page/教授.md "wikilink")。1989年，他获得[弗雷德里克·G·凯斯教授](../Page/弗雷德里克·G·凯斯.md "wikilink")（化学）称号。施罗克是[美国国家艺术与科学院和](../Page/美国国家艺术与科学院.md "wikilink")[美国国家科学院院士](../Page/美国国家科学院.md "wikilink")。育有两个孩子。

2005年，施罗克与[罗伯特·格拉布](../Page/罗伯特·格拉布.md "wikilink")、[伊夫·肖万共同获得了](../Page/伊夫·肖万.md "wikilink")[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。

2006年9月，施罗克携夫人首次出访中国，并于9月12日受聘为[武汉大学荣誉教授](../Page/武汉大学.md "wikilink")。

## 外部链接

  - 施罗克研究小组，<http://web.mit.edu/rrs/www/home.html>
  - 研究概述，<http://web.mit.edu/chemistry/www/faculty/schrock.html>
  - 施罗克获诺贝尔奖，<http://web.mit.edu/newsoffice/2005/schrock.html>
  - <https://web.archive.org/web/20060907000622/http://www.chemistry.msu.edu/Lectureships/lectures.asp?series=DK&Year=2001>

[Schrock, Richard R.](../Category/1945年出生.md "wikilink") [Schrock,
Richard R.](../Category/美国化学家.md "wikilink") [Schrock, Richard
R.](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:河濱加州大學校友](../Category/河濱加州大學校友.md "wikilink")
[Category:麻省理工學院教師](../Category/麻省理工學院教師.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:劍橋大學校友](../Category/劍橋大學校友.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:印第安納州人](../Category/印第安納州人.md "wikilink")

1.  <http://www.ch.ntu.edu.tw/nobel/2005.html>