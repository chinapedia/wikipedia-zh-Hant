[Richard_L._Armitage.jpeg](https://zh.wikipedia.org/wiki/File:Richard_L._Armitage.jpeg "fig:Richard_L._Armitage.jpeg")

**理查德·李·阿米蒂奇**（**Richard Lee
Armitage**，），[美国](../Page/美国.md "wikilink")[小布什政府时期任](../Page/乔治·W·布什.md "wikilink")[副国务卿](../Page/美国国务卿.md "wikilink")，是當時[美国国务院第二号人物](../Page/美国国务院.md "wikilink")。

## 生平

1967年，阿米蒂奇從[美國海軍學院畢業](../Page/美國海軍學院.md "wikilink")，成為[美國海軍](../Page/美國海軍.md "wikilink")[少尉](../Page/少尉.md "wikilink")，參加[越戰](../Page/越戰.md "wikilink")。1973年，《[巴黎和平協約](../Page/巴黎和平協約.md "wikilink")》成立，阿米蒂奇因拒絕[停戰而被勒令退伍](../Page/停戰.md "wikilink")。1981年，阿米蒂奇擔任[雷根政府的](../Page/雷根.md "wikilink")[美國國防部代理次長](../Page/美國國防部.md "wikilink")。1987年，[日本爆發](../Page/日本.md "wikilink")[東芝機械CoCom違反事件](../Page/東芝機械CoCom違反事件.md "wikilink")，阿米蒂奇表態反對美國對日本實行[經濟制裁](../Page/經濟制裁.md "wikilink")。

2001年3月23日，[美国参议院正式批准阿米蒂奇为副国务卿](../Page/美国参议院.md "wikilink")。2001年3月26日，阿米蒂奇正式就职副国务卿。2002年8月26日，阿米蒂奇在[北京市表示](../Page/北京市.md "wikilink")，美國既不支持也不反對[台灣獨立](../Page/台灣獨立.md "wikilink")。

2003年[美伊戰爭開戰前](../Page/美伊戰爭.md "wikilink")，阿米蒂奇將[中央情報局](../Page/中央情報局.md "wikilink")（CIA）[情報員的名字洩密](../Page/情報員.md "wikilink")，以讓小布什打文宣戰時取得上風；這段過程稱為[普拉姆身份泄密案](../Page/普拉姆身份泄密案.md "wikilink")（Plame
affair），被改編成2010年電影《[不公平的戰爭](../Page/不公平的戰爭.md "wikilink")》（*[Fair
Game](../Page/:en:Fair_Game_\(2010_film\).md "wikilink")*）。2004年7月，阿米蒂奇表示，《[日本國憲法](../Page/日本國憲法.md "wikilink")》[第九條是](../Page/日本憲法第九條.md "wikilink")《[美日安保條約](../Page/美日安保條約.md "wikilink")》的障礙。

2004年12月10日，阿米蒂奇接受美國[公共電視網](../Page/公共電視網.md "wikilink")（PBS）新聞節目《查理羅斯秀》（*Charlie
Rose*）專訪時說，[中國崛起過程中](../Page/中國崛起.md "wikilink")，[美國與中國的關係絕不是](../Page/中美關係.md "wikilink")「中國贏，美國就輸」的[零和遊戲](../Page/零和遊戲.md "wikilink")，美中關係最大的[地雷大概是](../Page/地雷.md "wikilink")[台灣](../Page/台灣.md "wikilink")，「我們都同意只有一個中國，台灣是中國的一部分」（We
all agree that there is but one China, and Taiwan is part of
China.），《[台灣關係法](../Page/台灣關係法.md "wikilink")》並沒有規定美國必須保衛台灣；他也說，如果台灣受到中國攻擊，美國是否宣戰，必須由[美國國會決定](../Page/美國國會.md "wikilink")。

2005年2月22日，阿米蒂奇卸任副国务卿。[2008年美國總統選舉](../Page/2008年美國總統選舉.md "wikilink")，阿米蒂奇為[約翰·麥肯策劃](../Page/約翰·麥肯.md "wikilink")[亞洲外交政策綱領](../Page/亞洲.md "wikilink")。

2014年2月27日，阿米蒂奇在[美日研究所](../Page/美日研究所.md "wikilink")（U.S.-Japan Research
Institute）舉行的「中日關係變局對美日聯盟的意涵」（Sino-Japan Dynamics and Implications for
the U.S.-Japan
Alliance）研討會說，當前[美日關係情況不太好](../Page/美日關係.md "wikilink")，[日韓關係很糟也不是秘密](../Page/日韓關係.md "wikilink")，[韓裔美國人把日韓爭端帶到美國本土](../Page/韓裔美國人.md "wikilink")，中國大陸樂見這些情勢，美國夾在中間都不討好；日本與美國都有[茶黨問題](../Page/茶黨.md "wikilink")，許多來自日方的評論聽來糟糕、毫無助益；[慰安婦議題反映日本的道德觀](../Page/慰安婦.md "wikilink")，日方應做表率；[日本首相](../Page/日本首相.md "wikilink")[安倍晉三的外交政策其實對](../Page/安倍晉三.md "wikilink")[東南亞和](../Page/東南亞.md "wikilink")[南亞用心極深](../Page/南亞.md "wikilink")，但在慰安婦議題上令他大感失望\[1\]。

## 简历

## 註解

[Category:總統公民獎章獲得者](../Category/總統公民獎章獲得者.md "wikilink")
[Category:美国副国务卿](../Category/美国副国务卿.md "wikilink")
[Category:美國國務院官員](../Category/美國國務院官員.md "wikilink")
[Category:美國共和黨黨員](../Category/美國共和黨黨員.md "wikilink")
[Category:美國越戰人物](../Category/美國越戰人物.md "wikilink")
[Category:美國海軍軍官](../Category/美國海軍軍官.md "wikilink")
[Category:美國海軍學院校友](../Category/美國海軍學院校友.md "wikilink")
[Category:波士頓人](../Category/波士頓人.md "wikilink")

1.