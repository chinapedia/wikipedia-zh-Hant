[教宗](../Page/教宗.md "wikilink")[聖](../Page/聖人.md "wikilink")**理諾**（，），為[羅馬公教會](../Page/羅馬公教.md "wikilink")[羅馬城第二任教會領導者](../Page/羅馬.md "wikilink")。[羅馬天主教會視其為承繼](../Page/羅馬天主教會.md "wikilink")[聖伯多禄的第二任](../Page/聖伯多禄.md "wikilink")[教宗](../Page/教宗.md "wikilink")。但一些[新教學者視其為第一任羅馬主教](../Page/新教.md "wikilink")，由[聖保祿任命](../Page/聖保祿.md "wikilink")，記於《[使徒宪典](../Page/使徒宪典.md "wikilink")》。

## 生平

出生於[義大利的](../Page/義大利.md "wikilink")[沃爾泰拉](../Page/沃爾泰拉.md "wikilink")。在公元67年6月29日上任，於公元76年9月23日被殺[殉道](../Page/殉道.md "wikilink")。

最早將理諾作為主教一世記錄下來是[愛任紐](../Page/愛任紐.md "wikilink")（Irenaeus:130年－202年），他在180年寫道「受祝福的使徒，在教宗任內致力建立並壯大了教會\[1\]」；牛津教宗字典中，將這段話解釋為愛任紐認為理諾是首任羅馬主教\[2\]；[耶柔米將理諾介紹為](../Page/耶柔米.md "wikilink")「[彼得之後的羅馬教會首位的領導者](../Page/彼得_\(使徒\).md "wikilink")\[3\]」；而在[優西比烏](../Page/優西比烏.md "wikilink")(Eusebius
Caesariensis:約260年或275年—339年5月30日?)的說法中，理諾是「在[彼得](../Page/彼得_\(使徒\).md "wikilink")（Peter:
1年－67年6月29日）與[保羅](../Page/保羅_\(使徒\).md "wikilink")（Paul:約3年－約67年）相繼殉道後，羅馬教會中第一位具有主教資格（Episcopate）者\[4\]」；[金口若望](../Page/约翰一世_\(君士坦丁堡大主教\).md "wikilink")（John
Chrysostom:
347年－407年）則寫道「某些說法認為理諾是羅馬教會中，接續[彼得作為主教的人](../Page/彼得_\(使徒\).md "wikilink")\[5\]」；而Liberian
Catalogue也認為[彼得是首任羅馬主教](../Page/彼得_\(使徒\).md "wikilink")，理諾是其繼任者\[6\]。

## 參考

[(Book VII Section
IV)](http://www.ccel.org/ccel/schaff/anf07.ix.viii.iv.html)

## 譯名列表

  - 理诺：[思高本圣经弟后](../Page/思高译本.md "wikilink")4:21作理诺。
  - 李諾：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作李諾。
  - 利奴：[和合本提後](../Page/和合本.md "wikilink")4:21作「利奴」。

{{-}}

[L](../Category/教宗.md "wikilink")
[L](../Category/義大利出生的教宗.md "wikilink")
[L](../Category/比萨人.md "wikilink")
[L](../Category/基督教聖人.md "wikilink")

1.  *Against Heresies*
    [3:3.3](http://www.newadvent.org/fathers/0103303.htm)
2.  J. N. D. Kelly, *Oxford Dictionary of Popes*, 2005
3.  "Post Petrum primus Ecclesiam Romanam tenuit Linus"
    -*[Chronicon](../Page/Chronicon_\(Jerome\).md "wikilink")*;
    [14g](http://www.tertullian.org/fathers/jerome_chronicle_06_latin_part2.htm)
    (p. 267)
4.  *Church History*
    [3.2](http://www.ccel.org/ccel/schaff/npnf201/Page_133.html)
5.  [Homily X on 2 Timothy](http://www.newadvent.org/fathers/230710.htm)
6.  *The Chronography of 354 AD* [Part 13: Bishops of
    Rome](http://www.tertullian.org/fathers/chronography_of_354_13_bishops_of_rome.htm)