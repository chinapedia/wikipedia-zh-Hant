**瓊·拜亞**（，，），[美國鄉村民謠女](../Page/美國.md "wikilink")[歌手](../Page/歌手.md "wikilink")，作曲家。她的很多作品都與[時事和社會問題有關](../Page/時事.md "wikilink")，並於1960年代活躍於反戰運動。

## 生平

瓊出生于[紐約](../Page/紐約.md "wikilink")，父親為著名[墨西哥裔美國物理學家Albert](../Page/墨西哥.md "wikilink")
Baez，母親為[蘇格蘭裔Joan](../Page/蘇格蘭.md "wikilink") Bridge
Baez；瓊有一姐一妹。瓊育有一子，名Gabriel Harris。

瓊的很多作品都與[時事和社會問題有關](../Page/時事.md "wikilink")，並於1960年代活躍於[反戰運動](../Page/反戰.md "wikilink")，並在當時交往了著名民歌手[巴布·狄倫](../Page/巴布·狄倫.md "wikilink")，唯兩人戀情不長，因狄倫漂泊不定，1965年即告分手。

1968年，瓊與反越戰拒徵兵社會運動者[David
Harris結婚](../Page/David_Harris.md "wikilink")，1972年仳離。

1975年，瓊為了紀念與[巴布·狄倫的往日情](../Page/巴布·狄倫.md "wikilink")，譜出了名曲「Diamond and
Rust」（鑽石與鐵鏽），也成為瓊最富盛名的歌曲。

1989年，在[北京的](../Page/北京.md "wikilink")[天安门](../Page/天安门.md "wikilink")[六四事件之后](../Page/六四事件.md "wikilink")，瓊写了一首名为“中国”的歌曲，谴责[中国共产党对数千名呼吁建立民主共和主义的学生示威者使用暴力和血腥的方式进行镇压](../Page/中国共产党.md "wikilink")。\[1\]

瓊出道表演已超過50年，發行超過30張專輯，著名作品包括「Diamond and Rust」和「Where Have All The
Flowers Gone」。最新的作品為2008年發行的「Day After Tomorrow」。

2015年3月，瓊·拜亞與[艾未未一起獲](../Page/艾未未.md "wikilink")[國際特赦組織頒發最高人權獎](../Page/國際特赦組織.md "wikilink")\[2\]。
2017年4月7日被引入[搖滾名人堂](../Page/搖滾名人堂.md "wikilink") \[3\]。

## 外部連結

  - [官方網站](http://www.joanbaez.com/)
  - [Joan Baez Current European Record
    Label](https://web.archive.org/web/20080328004720/http://www.proper-records.co.uk/artists.php?action=alview&alid=1003)
  - [Joan Baez's career on A\&M Records with gallery, international
    discography](http://www.onamrecords.com/gallery/Joan%20Baez)
  - [Joan Baez Main Page by Richard L.
    Hess](http://www.richardhess.com/joan/)
  - [the text of "Open letter to the Socialist Republic of Vietnam"
    (half way down the
    page)](http://perso.wanadoo.fr/joan-baez/engagement/vietnam.html)
  - ["Joan Baez: The Folk Heroine Mellows With
    Age"](https://web.archive.org/web/20090112070915/http://crawdaddy.wolfgangsvault.com/Article.aspx?id=4558)
    1984 article and interview, reprinted in 2007 by
    *[Crawdaddy\!](../Page/Crawdaddy!.md "wikilink")*

## 音像

  - [Joan Baez - It Ain't Me Babe
    (LIVE 1965)](http://www.videosift.com/video/Joan-Baez-It-Aint-Me-Babe-Live-1965-great-version)

## 参考资料

[B](../Category/纽约市人.md "wikilink")
[B](../Category/20世纪歌手.md "wikilink")
[B](../Category/美國女歌手.md "wikilink")
[B](../Category/美國創作歌手.md "wikilink")
[B](../Category/美國民謠歌手.md "wikilink")
[B](../Category/美國鄉村音樂歌手.md "wikilink")
[B](../Category/美國社會運動者.md "wikilink")
[B](../Category/美國反伊拉克戰爭人士.md "wikilink")
[B](../Category/美國反越戰人士.md "wikilink")
[B](../Category/美國LGBT權利運動家.md "wikilink")
[B](../Category/墨西哥裔美國人.md "wikilink")
[B](../Category/苏格兰裔美國人.md "wikilink")
[B](../Category/搖滾名人堂入選者.md "wikilink")

1.  [China by Joan Baez](http://www.songfacts.com/detail.php?id=14854/),
    Songfacts
2.
3.