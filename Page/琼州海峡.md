<table>
<thead>
<tr class="header">
<th><p>琼州海峡</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Leizhou_peninsula.jpg" title="fig:Leizhou_peninsula.jpg">Leizhou_peninsula.jpg</a><br />
</p>
<center>
<p>琼州海峡卫星照片</p>
</center></td>
</tr>
<tr class="even">
<td><p><strong>国家</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>海洋</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>面积</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>平均深度</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>最大深度</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>连接陆地</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>海域管辖</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>沟通海域</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>成因</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>走向</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>沿岸城市</strong></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

**琼州海峡**，是[海南省的](../Page/海南省.md "wikilink")[海南岛与](../Page/海南岛.md "wikilink")[广东省的](../Page/广东省.md "wikilink")[雷州半岛之间所夹的水道](../Page/雷州半岛.md "wikilink")，因海南岛的别称“琼州岛”而得名，是[中国三大海峡之一](../Page/海峡#中國接鄰的著名海峡.md "wikilink")。琼州海峡东西长约80公里，南北平均宽为29.5公里，最宽处直线距离为33.5公里，最窄处直线距离仅18公里左右，平均水深44米，最大深度114米，水域面积2300余平方千米。\[1\]

## 历史

[海南島因瓊州海峽之隔](../Page/海南島.md "wikilink")，歷史上被視為荒蠻之地。唐代高僧[鑑真第五次東渡日本](../Page/鑑真.md "wikilink")，被困於海南島，唐代宰相[李德裕也曾有](../Page/李德裕.md "wikilink")“一去一萬里，千之千不還”之哀，宋代[蘇東坡有](../Page/蘇東坡.md "wikilink")“滄海何曾斷地脈，珠崖從此破天荒”之嘆。元符三年（1100年）六月蘇東坡渡过琼州海峡返北，在悍江边吟道：“我心本如此，月满江不湍”。1605年7月，海峡地域发生大[地震](../Page/地震.md "wikilink")，引发两岸部分陆地沉降，导致一些村庄淹没在海水中。

1949年12月，[毛泽东向](../Page/毛泽东.md "wikilink")[解放军第四野战军发出强渡琼州海峡的命令](../Page/中国人民解放军.md "wikilink")，准备占领中华民国政府控制的海南岛。1950年3月起，解放軍在海南地方武装瓊崖縱隊的配合下开始[海南島戰役](../Page/海南島戰役.md "wikilink")，[邓华为总指揮](../Page/邓华.md "wikilink")。解放军首先派小股先遣队潜渡琼州海峡并成功登陆，4月16日发起大规模渡海强攻，23日占领[海口市](../Page/海口市.md "wikilink")，5月1日攻佔海南全島，中华民国国军则退居台湾。\[2\]

1988年3月21日，[中国](../Page/中华人民共和国.md "wikilink")[北京体育学院教师](../Page/北京体育学院.md "wikilink")[张健以游泳方式横渡海峡成功](../Page/张健_\(游泳运动员\).md "wikilink")，耗时9小时12分，涉水长度29.5公里，此举轰动一时\[3\]。

## 地理

[Olivine_basalt2.jpg](https://zh.wikipedia.org/wiki/File:Olivine_basalt2.jpg "fig:Olivine_basalt2.jpg")\]\]

琼州海峡是[中国大陆](../Page/中国大陆.md "wikilink")[雷州半岛与](../Page/雷州半岛.md "wikilink")[海南岛的连接水域](../Page/海南岛.md "wikilink")，东西沟通[中国南海北部与](../Page/中国南海.md "wikilink")[北部湾](../Page/北部湾.md "wikilink")\[4\]。海峡海水的透明度5米，盐度数值为30，年平均温度25-27摄氏度\[5\]。琼州海峡在[地质史上与](../Page/地质.md "wikilink")[海南岛和](../Page/海南岛.md "wikilink")[雷州半岛为完整陆地](../Page/雷州半岛.md "wikilink")，由于[第四纪](../Page/第四纪.md "wikilink")[冰川时期的地壳运动](../Page/冰川.md "wikilink")，在现今的海峡区域形成断陷，海水将断陷区覆盖形成[海峡](../Page/海峡.md "wikilink")。海峡两岸蜿蜒曲折，凸出的海角与凹入的海湾交替分布。北岸为[玄武岩地质构造](../Page/玄武岩.md "wikilink")；南岸为熔岩地台；[火山岩在两岸均有分布](../Page/火山岩.md "wikilink")。海峡南北较浅，中部最深，南北横切面大致呈“V”字形构造。琼州海峡[海流汹涌](../Page/海流.md "wikilink")，强烈侵蚀着海地地貌，形成海底冲槽和沙垅。\[6\]

## 海上交通

琼州海峡是中国大陆连接[海南岛的陆海交通咽喉](../Page/海南岛.md "wikilink")。公路交通由大陆的G75高速公路通过海安汽车轮渡至[海口连接](../Page/海口市.md "wikilink")[海南公路网](../Page/海南省.md "wikilink")；铁路则通过海安的[粤海铁路专用码头将火车轮渡到海口与海南铁路接驳](../Page/粤海铁路.md "wikilink")。瓊州海峽東部海況險惡，被視為远洋航行畏途。

### 交通瓶颈与跨海大桥计划

[广东省](../Page/广东省.md "wikilink")[徐闻海安港的官员认为](../Page/徐闻县.md "wikilink")，海安港汽车轮渡的旅客及汽车流量巨大，并以每年以百分之十五的幅度增长，海峡运输压力明显，建造桥梁为可行之举，可以缓解当前交通压力\[7\]。据[中国媒体报道](../Page/中华人民共和国.md "wikilink")，[中华人民共和国将在琼州海峡建设跨海大桥](../Page/中华人民共和国.md "wikilink")，桥梁分上下两层，上层为公路，下层为[铁路轨道](../Page/铁路轨道.md "wikilink")，总投资达1400亿元[人民币](../Page/人民币.md "wikilink")，预计2020年建成。媒体引述专家观点认为，琼州海峡大桥建成后，对南海资源开发及推动中国与[东盟国家的自由贸易区建设具有重大意义](../Page/东南亚国家联盟.md "wikilink")。\[8\]

## 沿岸行政区

[海峡南岸的[海口市](../Page/海口市.md "wikilink")|thumb|right|150px](https://zh.wikipedia.org/wiki/File:Haikou_skyline_6_-_2009_09_07.jpg "fig:海峡南岸的海口市|thumb|right|150px")

  -   - [广东省](../Page/广东省.md "wikilink")
          - [湛江市](../Page/湛江市.md "wikilink")
              - [徐聞縣](../Page/徐聞縣.md "wikilink")
      - [海南省](../Page/海南省.md "wikilink")
          - [海口市](../Page/海口市.md "wikilink")

## 参考文献

## 参见

  - [中华人民共和国领海基线](../Page/中华人民共和国领海基线.md "wikilink")、[内水](../Page/内水.md "wikilink")
  - [南中国海](../Page/南中国海.md "wikilink")、[北部湾](../Page/北部湾.md "wikilink")
  - [雷州半岛](../Page/雷州半岛.md "wikilink")、[海南岛](../Page/海南岛.md "wikilink")

{{-}}

[Category:中华人民共和国内水海峡](../Category/中华人民共和国内水海峡.md "wikilink")
[Category:海南地理](../Category/海南地理.md "wikilink")
[Category:廣東地理](../Category/廣東地理.md "wikilink")
[Category:北部灣](../Category/北部灣.md "wikilink")
[Category:南中國海](../Category/南中國海.md "wikilink")

1.

2.

3.

4.
5.

6.
7.
8.