**瑞亞克洋**（Rheic
Ocean）是個史前海洋，存在於大約5億到3億年前，北接[波羅地大陸與](../Page/波羅地大陸.md "wikilink")[阿瓦隆尼亞大陸](../Page/阿瓦隆尼亞大陸.md "wikilink")，南接[岡瓦那大陸](../Page/岡瓦那大陸.md "wikilink")（在[志留紀時期則南接岡瓦那大陸分裂出來的Hunic陸塊](../Page/志留紀.md "wikilink")）。

在[奧陶紀中期](../Page/奧陶紀.md "wikilink")，因為[原特提斯洋](../Page/原特提斯洋.md "wikilink")[中洋脊的關係](../Page/中洋脊.md "wikilink")，阿瓦隆尼亞大陸從岡瓦那大陸分裂出來，並逐漸橫越[巨神海](../Page/巨神海.md "wikilink")，瑞亞克洋即從阿瓦隆尼亞大陸與岡瓦那大陸之間形成。在奧陶紀晚期，瑞亞克洋以非常快的速度擴張，相當於今日[東太平洋海隆的速度](../Page/東太平洋海隆.md "wikilink")（約每年17公分）。在奧陶紀末期，波羅地大陸與[勞倫大陸碰撞](../Page/勞倫大陸.md "wikilink")、合併，形成[歐美大陸](../Page/歐美大陸.md "wikilink")，此時的瑞亞克洋已取代巨神海的原有位置，而巨神海則縮小成一個狹窄的海道，介於阿瓦隆尼亞大陸與歐美大陸（勞倫大陸部分）之間。

在[泥盆紀時期](../Page/泥盆紀.md "wikilink")，岡瓦那大陸開始往歐美大陸移動，瑞亞克洋開始縮小。到了泥盆紀晚期，瑞亞克洋成為岡瓦那大陸與歐美大陸之間的一個狹窄海域。

在早[石炭紀](../Page/石炭紀.md "wikilink")（[密西西比亞紀](../Page/密西西比亞紀.md "wikilink")），岡瓦那大陸與歐美大陸開始接合，當時的[美國東部與](../Page/美國.md "wikilink")[非洲連接](../Page/非洲.md "wikilink")，使得瑞亞克洋的東部閉合。

在[二疊紀](../Page/二疊紀.md "wikilink")，當時的[南美洲與美國東部連接](../Page/南美洲.md "wikilink")，瑞亞克洋消失。這次的碰撞產生了[瓦失陶](../Page/瓦失陶造山運動.md "wikilink")-[阿利根尼](../Page/阿利根尼造山幕.md "wikilink")-[華力西](../Page/華力西造山運動.md "wikilink")[造山運動](../Page/造山運動.md "wikilink")。

## 名稱

位於波羅地大陸與勞倫西亞大陸之間的史前海洋，名為[巨神海](../Page/巨神海.md "wikilink")（Iapetus
Ocean），是以[希臘神話中](../Page/希臘神話.md "wikilink")，[提坦神](../Page/提坦神.md "wikilink")[阿特拉斯的父親](../Page/阿特拉斯.md "wikilink")[伊阿珀托斯為名](../Page/伊阿珀托斯.md "wikilink")，因為巨神海位於[大西洋](../Page/大西洋.md "wikilink")（以阿特拉斯為名）的原有位置。而瑞亞克洋則是以伊阿珀托斯的妹妹[瑞亞為名](../Page/瑞亞.md "wikilink")。

## 外部連結

  - [Website of the PALEOMAP Project](http://www.scotese.com/)
      - [Middle Silurian
        paleoglobe](http://www.scotese.com/newpage2.htm) showing the
        expanding Rheic Ocean
      - [Early Carboniferous
        paleoglobe](http://www.scotese.com/newpage4.htm) showing the
        almost disappeared Rheic Ocean

[R](../Category/古海洋.md "wikilink") [R](../Category/寒武紀.md "wikilink")
[R](../Category/奧陶紀.md "wikilink") [R](../Category/志留紀.md "wikilink")
[R](../Category/泥盆紀.md "wikilink") [R](../Category/石炭紀.md "wikilink")