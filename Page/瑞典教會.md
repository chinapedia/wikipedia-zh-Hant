[Svenska_kyrkan_vapen.svg](https://zh.wikipedia.org/wiki/File:Svenska_kyrkan_vapen.svg "fig:Svenska_kyrkan_vapen.svg")
**瑞典教會**（），又譯為**瑞典信義會**，是[瑞典主要的](../Page/瑞典.md "wikilink")[新教](../Page/新教.md "wikilink")[教會](../Page/教會.md "wikilink")。2015年，63.2%的[瑞典人屬信義會會友](../Page/瑞典人.md "wikilink")，是瑞典最大的基督教教會，亦是世界上最大的[信義宗教會](../Page/信義宗.md "wikilink")，然而，只有2%的信徒會恆常參與周日[禮拜](../Page/禮拜.md "wikilink")\[1\]。它擁有這麼多不活躍會眾，是因為長久以來，其會眾的嬰孩會於出生時自動成為會眾，以不用繳納[什一稅](../Page/什一稅.md "wikilink")，此舉直至1996年才取消。2000年，瑞典政府取消其[國教地位](../Page/國教.md "wikilink")，但瑞典信義會在國內仍有重要地位。

但近年來，瑞典信義會會眾數目不斷減少\[2\]。

瑞典信義會的[主教長為](../Page/主教長.md "wikilink")[烏普薩拉大主教](../Page/烏普薩拉大主教.md "wikilink")，現任主教長為[安特耶·雅克伦](../Page/安特耶·雅克伦.md "wikilink")。

## 歷史

### 中世紀

9世紀時，基督教開始傳入瑞典一些部落，但由於瑞典與其他北歐國家一樣，直至公元1000年它才進行基督教化。瑞典國王[奧洛夫接受了洗禮](../Page/奧洛夫.md "wikilink")。然而，由於當時國界未定，故此瑞典全境直至12世紀拆除[烏普薩拉神殿前仍未全面基督教化](../Page/烏普薩拉神殿.md "wikilink")。在北方的[拉普蘭](../Page/拉普蘭.md "wikilink")，推行基督教又花上了另一個世紀的努力。

北歐的基督教會原由[不萊梅大教區管轄](../Page/不萊梅大教區.md "wikilink")。1104年，隆德教區成為北歐全地的大教區，後來烏普薩拉於1164年成立，成為瑞典的大教區，直至今日。1248年3月，教廷特使[摩德納的威廉出席了](../Page/摩德納的威廉.md "wikilink")[謝寧厄](../Page/謝寧厄.md "wikilink")（Skänninge）的教會聚會，使瑞典教會與羅馬天主教會的關係更緊密。

在全國都最受尊崇的天主教聖人是12世紀時的瑞典國王聖埃里克（[埃里克九世](../Page/埃里克九世.md "wikilink")）和在14世紀見異像的[聖女畢哲](../Page/聖女畢哲.md "wikilink")，但也有聖人在一些區域受尊崇，如[南曼蘭的](../Page/南曼蘭.md "wikilink")[聖博特維德](../Page/聖博特維德.md "wikilink")、[聖埃斯基耳和](../Page/聖埃斯基耳.md "wikilink")[斯莫蘭的](../Page/斯莫蘭.md "wikilink")[聖西格弗里德](../Page/聖西格弗里德.md "wikilink")。

瑞典過去一直是信奉天主教，於16世紀20年代[宗教改革後改信](../Page/宗教改革.md "wikilink")[新教](../Page/新教.md "wikilink")。

### 宗教改革

1523年，[古斯塔夫·瓦薩取得權力後不久](../Page/古斯塔夫·瓦薩.md "wikilink")，便致信羅馬天主教教宗，要求確認任命[約翰內斯·馬格努斯為](../Page/約翰內斯·馬格努斯.md "wikilink")[瑞典大主教](../Page/瑞典大主教.md "wikilink")，以接替被國會取締和放逐的[古斯塔夫·特羅勒](../Page/古斯塔夫·特羅勒.md "wikilink")。

古斯塔夫承諾若教廷承認他對主教的任命，便會聽命於羅馬教廷。然而，教宗要求特羅勒復位，令古斯塔夫晉升了瑞典的[改革派](../Page/宗教改革.md "wikilink")[奧洛斯·佩特利等人以報復](../Page/奧洛斯·佩特利.md "wikilink")。他支持改革派文章的出版，並委派佩特利負責推廣這些文章。1526年，所有有關天主教的出版被打壓，而教會所得[什一奉獻的三分之二被挪用作償還國債](../Page/什一奉獻.md "wikilink")。

[古斯塔夫·瓦薩策劃慶功典禮時](../Page/古斯塔夫·瓦薩.md "wikilink")，瑞典正式放棄了[舊教的傳統](../Page/舊教.md "wikilink")\[3\]。

宗教改革取締了部分罗马天主教禮儀。然而，這些轉變比[德意志宗教改革的溫和得多](../Page/德意志宗教改革.md "wikilink")；現時瑞典的教會仍保留罗马天主教的製品，如[十字架](../Page/十字架.md "wikilink")、[受难像](../Page/基督受難.md "wikilink")、和[聖像](../Page/聖像.md "wikilink")，还包括祭衣和礼服等。瑞典教会亦施行传统的[七聖事](../Page/聖禮.md "wikilink")，举行隆重的[弥撒礼仪](../Page/弥撒.md "wikilink")。而部分基於[聖人曆的節日於](../Page/聖人曆.md "wikilink")18世紀才因強烈反對而撤銷。

古斯塔夫·瓦薩死後，統治瑞典的是傾向恢復[天主教的](../Page/天主教.md "wikilink")[約翰三世和同時統治天主教波蘭的](../Page/約翰三世_\(瑞典\).md "wikilink")[齊格蒙特三世](../Page/齊格蒙特三世.md "wikilink")。齊格蒙特三世的叔父興兵奪位，自立為[卡爾九世後](../Page/卡爾九世.md "wikilink")，儘管他本人信奉[喀爾文主義的](../Page/喀爾文主義.md "wikilink")[改革宗](../Page/改革宗.md "wikilink")，而非路德派的信義宗，但他以推廣信義宗作為與其姪兒爭權的手段。

1526年，《[新約](../Page/新約.md "wikilink")》被翻譯成瑞典語，而整本《[聖經](../Page/聖經.md "wikilink")》也於1541年完成翻譯。1618年和1703年出版了修訂翻譯版。1917年和2000年瑞典定立新的翻譯本為官方譯本。瑞典宗教改革者和[馬丁·路德編寫的聖詩也被翻譯](../Page/馬丁·路德.md "wikilink")。半官方的讚美詩集出現於17世紀40年代，而瑞典信義會於1695年、1819、1937和1986年通過《瑞典詩集》（）為官方的詩集。

## 行政區劃

瑞典信義會劃分為13個[教區](../Page/教區.md "wikilink")，每個教區設一主教，但烏普薩拉教區另設有[烏普薩拉大主教](../Page/烏普薩拉大主教.md "wikilink")。

### 教區、主教座、座堂

| | [教區](../Page/教區.md "wikilink")                                                                                                                                     | | [主教座](../Page/主教座.md "wikilink")   | | [主教座堂](../Page/主教座堂.md "wikilink")       |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------ | ------------------------------------------ |
| [Uppsala_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Uppsala_stift_vapen.svg "fig:Uppsala_stift_vapen.svg") [烏普薩拉教區](../Page/烏普薩拉教區.md "wikilink")         | [烏普薩拉](../Page/烏普薩拉.md "wikilink")   | [烏普薩拉大教堂](../Page/烏普薩拉大教堂.md "wikilink")   |
| [Göteborg_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Göteborg_stift_vapen.svg "fig:Göteborg_stift_vapen.svg") [哥德堡教區](../Page/哥德堡教區.md "wikilink")        | [哥德堡](../Page/哥德堡.md "wikilink")     | [哥特堡大教堂](../Page/哥特堡大教堂.md "wikilink")     |
| [Härnösand_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Härnösand_stift_vapen.svg "fig:Härnösand_stift_vapen.svg") [海訥桑德教區](../Page/海訥桑德教區.md "wikilink")   | [海訥桑德](../Page/海訥桑德.md "wikilink")   | [海訥桑德大教堂](../Page/海訥桑德大教堂.md "wikilink")   |
| [Karlstad_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Karlstad_stift_vapen.svg "fig:Karlstad_stift_vapen.svg") [卡爾斯塔德教區](../Page/卡爾斯塔德教區.md "wikilink")    | [卡爾斯塔德](../Page/卡爾斯塔德.md "wikilink") | [卡爾斯塔德大教堂](../Page/卡爾斯塔德大教堂.md "wikilink") |
| [Linköping_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Linköping_stift_vapen.svg "fig:Linköping_stift_vapen.svg") [林雪平教區](../Page/林雪平教區.md "wikilink")     | [林雪平](../Page/林雪平.md "wikilink")     | [林雪坪大教堂](../Page/林雪坪大教堂.md "wikilink")     |
| [Lund_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Lund_stift_vapen.svg "fig:Lund_stift_vapen.svg") [隆德教區](../Page/隆德教區.md "wikilink")                      | [隆德](../Page/隆德.md "wikilink")       | [隆德大教堂](../Page/隆德大教堂.md "wikilink")       |
| [Luleå_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Luleå_stift_vapen.svg "fig:Luleå_stift_vapen.svg") [呂勒奧教區](../Page/呂勒奧教區.md "wikilink")                 | [呂勒奧](../Page/呂勒奧.md "wikilink")     | [呂勒奧大教堂](../Page/呂勒奧大教堂.md "wikilink")     |
| [Skara_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Skara_stift_vapen.svg "fig:Skara_stift_vapen.svg") [斯卡拉教區](../Page/斯卡拉教區.md "wikilink")                 | [斯卡拉](../Page/斯卡拉.md "wikilink")     | [斯卡拉大教堂](../Page/斯卡拉大教堂.md "wikilink")     |
| [Stockholm_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Stockholm_stift_vapen.svg "fig:Stockholm_stift_vapen.svg") [斯德哥爾摩教區](../Page/斯德哥爾摩教區.md "wikilink") | [斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink") | [斯德哥爾摩大教堂](../Page/斯德哥爾摩大教堂.md "wikilink") |
| [Strängnäs_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Strängnäs_stift_vapen.svg "fig:Strängnäs_stift_vapen.svg") [斯特蘭奈斯教區](../Page/斯特蘭奈斯教區.md "wikilink") | [斯特蘭奈斯](../Page/斯特蘭奈斯.md "wikilink") | [斯特蘭奈斯大教堂](../Page/斯特蘭奈斯大教堂.md "wikilink") |
| [Visby_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Visby_stift_vapen.svg "fig:Visby_stift_vapen.svg") [維斯比教區](../Page/維斯比教區.md "wikilink")                 | [維斯比](../Page/維斯比.md "wikilink")     | [維斯比大教堂](../Page/維斯比大教堂.md "wikilink")     |
| [Västerås_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Västerås_stift_vapen.svg "fig:Västerås_stift_vapen.svg") [韋斯特羅斯教區](../Page/韋斯特羅斯教區.md "wikilink")    | [韋斯特羅斯](../Page/韋斯特羅斯.md "wikilink") | [韋斯特羅斯大教堂](../Page/韋斯特羅斯大教堂.md "wikilink") |
| [Växjö_stift_vapen.svg](https://zh.wikipedia.org/wiki/File:Växjö_stift_vapen.svg "fig:Växjö_stift_vapen.svg") [韋克舍教區](../Page/韋克舍教區.md "wikilink")                 | [韋克舍](../Page/韋克舍.md "wikilink")     | [韋克舍大教堂](../Page/韋克舍大教堂.md "wikilink")     |

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
## 外部連結

  - [瑞典信義會](http://www.svenskakyrkan.se/)

[Category:歐洲信義宗](../Category/歐洲信義宗.md "wikilink")
[Category:瑞典基督教](../Category/瑞典基督教.md "wikilink")
[Category:瑞典歷史](../Category/瑞典歷史.md "wikilink")
[Category:國民教會](../Category/國民教會.md "wikilink")

1.  [聖餐和崇拜](http://www.svenskakyrkan.se/SVK/eng/liturgy.htm)，瑞典信義會
2.  [瑞典信義會](http://www.svenskakyrkan.se/)，[《1972年—2006年會眾人數統計表》](http://www.svenskakyrkan.se/statistik/xls/medlem_diagram.xls)
3.  《[不列顛百科全書](../Page/不列顛百科全書.md "wikilink")》第11版，「瑞典」詞條