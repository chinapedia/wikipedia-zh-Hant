**瑞吉·金·威利斯**(Reggie Gene
Willits)1981年5月30日生於[奧克拉荷馬州](../Page/奧克拉荷馬州.md "wikilink")(Oklahoma)的齊卡夏(Chickasha)，他目前是一名[美國職棒大聯盟的外野手](../Page/美國職棒大聯盟.md "wikilink")，目前效力於[洛杉磯安那罕天使隊](../Page/洛杉磯安那罕天使.md "wikilink")。威利斯畢業於奧克拉荷馬州立大學(University
of Oklahoma)。

## 職業生涯

2006年4月26日，威利斯獲得晉升至大聯盟。

## 外部連結

  -
  - [Minor League Splits and Situational
    Stats](http://www.minorleaguesplits.com/cgi-bin/pl.cgi?pl=435065)

[Category:美國棒球選手](../Category/美國棒球選手.md "wikilink")
[Category:洛杉磯安那罕天使隊球員](../Category/洛杉磯安那罕天使隊球員.md "wikilink")