**瑞士人民黨**，也译作**瑞士國民黨**，是[瑞士的一个](../Page/瑞士.md "wikilink")[右翼民粹主义政黨](../Page/右翼民粹主义.md "wikilink")，也是瑞士德語區最大的政黨。

## 歷史

2003年國會選舉，瑞士人民黨首次成為[国民院第一大黨](../Page/国民院_\(瑞士\).md "wikilink")，在200席中佔有55席。人民黨在[瑞士聯邦委員會中](../Page/瑞士聯邦委員會.md "wikilink")，增加至兩名成員。

在2007年10月的議會選舉中，人民黨議席增加至62席，延續了国民院第一大黨的地位。

2011年国民院選舉，人民黨雖然議席減少，國民院獲得54席，仍然維持国民院第一大黨地位。

2014年2月9日，人民黨推動公投，對來自歐盟的移民人數設上限。公投結果，投票率55.6%，以50.3%支持獲得通過，瑞士需重新與歐盟談判。\[1\]

2015年國會選舉，受惠於提出反移民的立場及主張，人民黨的議席大幅增加，國民院增至65席，鞏固作為国民院第一大黨的地位。

## 政治立場

瑞士人民黨強烈反對無限量地引入外國移民，尤其是來自[歐洲聯盟的](../Page/歐洲聯盟.md "wikilink")[中歐及](../Page/中歐.md "wikilink")[東歐地區的移民](../Page/東歐.md "wikilink")，亦反對[穆斯林移民](../Page/穆斯林.md "wikilink")。

## 外部連結

  - ["Political Map of Switzerland" "Hermann, M. und Leuthold, H.
    (2003): Die politische Landkarte des Nationalrats 1999-2003. In:
    Tages-Anzeiger, 11. Oktober, 2003,
    Zürich."](https://web.archive.org/web/20071025173858/http://sotomo.geo.unizh.ch/papers/parlaKarte.99-03.pdf)
  - [Swiss People's Party (in German and French)](http://www.svp.ch)
  - [Controversial website containing games supporting SVP ideology (in
    German and French)](http://www.zottel-game.ch)

## 參考

[Category:瑞士政黨](../Category/瑞士政黨.md "wikilink")
[Category:保守主義政黨](../Category/保守主義政黨.md "wikilink")
[Category:農業政黨](../Category/農業政黨.md "wikilink")
[Category:歐洲懷疑主義政黨](../Category/歐洲懷疑主義政黨.md "wikilink")
[Category:右翼民粹主義](../Category/右翼民粹主義.md "wikilink")

1.  [人民黨公投告捷
    瑞士恐自毀長城](http://orientaldaily.on.cc/cnt/china_world/20140211/00192_001.html)
    2014-02-11 [東方日報 (香港)](../Page/東方日報_\(香港\).md "wikilink")\]