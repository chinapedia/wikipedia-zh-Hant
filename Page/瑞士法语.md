[Map_Languages_CH_zh.png](https://zh.wikipedia.org/wiki/File:Map_Languages_CH_zh.png "fig:Map_Languages_CH_zh.png")

瑞士法語（[法語](../Page/法語.md "wikilink")：français de
Suisse）指的是[瑞士法語區](../Page/瑞士.md "wikilink")（稱作
[羅曼地](../Page/羅曼地.md "wikilink")）所說的[法語](../Page/法語.md "wikilink")。[法蘭克-普羅旺斯語](../Page/法蘭克-普羅旺斯語.md "wikilink")（Franco-Provençal）或[羅曼什語](../Page/羅曼什語.md "wikilink")（Romansh）與瑞士法語不同，雖然它們同屬[羅曼語族](../Page/羅曼語族.md "wikilink")，且通行區域距離法語區不遠。

瑞士法語與法國法語僅有些微的不同，多半是詞彙上的：瑞士法語使用者與法國法語使用者幾乎可以無障礙的交流，而說法國法語的人聽瑞士法語講話時可能只會遇到幾個不熟悉的詞語，無礙理解。瑞士法語與法國法語的差異遠小於[瑞士德語與](../Page/瑞士德語.md "wikilink")[標準德語的差異](../Page/標準德語.md "wikilink")。當然還是有例外的情形；不過，法語區內的方言多已消失，所以也不再流通。

瑞士法語並沒有統一的標準：不同州（Cantons）之間，甚至於城鎮間，使用不同的詞彙，通常源於當地的區域語言或[德語](../Page/德語.md "wikilink")，因為瑞士主要是說德語。

## 瑞士法語與標準法語的差別

由於瑞士與法國的行政與政治系統不同，形成瑞士法語與法國法語間不少的差異。瑞士法語與[比利時法語在有些語彙上與法國法語間有共同的相異處](../Page/比利時法語.md "wikilink")，例如：

  - 代表 70， 代表 90，有時候  代表
    80，與以下法國法語[二十進位計數系統](../Page/二十進位.md "wikilink")
    ("[vigesimal](../Page/vigesimal.md "wikilink")" [French counting
    system](../Page/French_language#Numerals.md "wikilink")) 所用的字彙不同：其中
    (字面上意思為'六十和十')代表 70， (字面上意思為'四個二十')代表 80，以及 (字面上意思為'四個二十和十')代表 90。

  - (早餐) 與  (午餐)
    這兩個字跟[比利時法語與](../Page/比利時法語.md "wikilink")[魁北克法語中的意思相同](../Page/魁北克法語.md "wikilink")。依照法國法語的用法，他們的意思分別為午餐及晚餐，據說是源於[路易十四中午起床的緣故](../Page/路易十四.md "wikilink")；請參閱比利時法語字彙
    ([Belgian French
    vocabulary](../Page/Belgian_French#Vocabulary.md "wikilink"))。

以下為其他與比利時法語相異的例子：

  - 這個字有時候用作 80，而不用法國法語的  (字面上意思為'四個二十')，特別是在
    [Vaud](../Page/Vaud.md "wikilink")、[Valais](../Page/Valais.md "wikilink")
    和 [Fribourg](../Page/Fribourg.md "wikilink") 這幾州；目前已不再使用
    這個字(源自[拉丁文](../Page/拉丁文.md "wikilink") )。.

  - [canton](../Page/Canton_\(subnational_entity\).md "wikilink")
    這個字在每個國家都代表著不同意思。

  - 在法國，郵政信箱稱為 ，但是在瑞士，則叫作 。

  - 用作'下午好'。

## 參見

  - [法語方言](../Page/法語方言.md "wikilink")
  - [比利時法語](../Page/比利時法語.md "wikilink")

[Category:法語](../Category/法語.md "wikilink")
[Category:瑞士语言](../Category/瑞士语言.md "wikilink")