**瑞士證券交易所**（SWX Swiss Exchange - Switzerland's stock
exchange）位於[蘇黎世](../Page/蘇黎世.md "wikilink")，為[瑞士主要的股票](../Page/瑞士.md "wikilink")、[債券及](../Page/債券.md "wikilink")[期權等衍生工具的交易所](../Page/期權.md "wikilink")。是世界上技術最先進的證券交易所之一。1995年由[蘇黎世](../Page/蘇黎世.md "wikilink")，[巴塞爾](../Page/巴塞爾.md "wikilink")，[日內瓦瑞士國內](../Page/日內瓦.md "wikilink")3間有百年历史的證券交易所合併而成。現在瑞士首都[伯恩還保留了伯恩小證券交易所](../Page/伯恩.md "wikilink")。

## 历史

  - 1995年全球首個使用全自動的交易、結算、交收系統。
  - 1988年與[法蘭克福證券交易所成立](../Page/法蘭克福證券交易所.md "wikilink")[Eurex](../Page/Eurex.md "wikilink")，為全球最大的衍生商品交易所。

## 瑞士市場指數

瑞士證券交易所提供了主要指數，瑞士市場指數（SMI-Swiss Market Index），該指數由20個最重要的證券組成。

## 參見

  -
## 参考文献

## 外部連結

  - [SWX Swiss Exchange](http://www.swx.com/)
  - [Eurex](http://www.eurexchange.com/)
  - [Berne eXchange website](http://www.berne-x.com)

[Category:歐洲證券交易所](../Category/歐洲證券交易所.md "wikilink")
[Category:1995年建立](../Category/1995年建立.md "wikilink")
[Category:瑞士经济](../Category/瑞士经济.md "wikilink")
[Category:苏黎世经济](../Category/苏黎世经济.md "wikilink")