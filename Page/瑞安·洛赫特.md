**瑞安·-{zh-tw:史蒂芬;zh-cn:斯蒂芬;zh-hk:史提芬;zh-hant:斯蒂芬;zh-hans:斯蒂芬;}-·洛赫特**（，，；），[美國](../Page/美國.md "wikilink")[游泳男運動員](../Page/游泳.md "wikilink")。他在世界比賽以致[奧運會都有上佳的表現](../Page/奧運會.md "wikilink")，例如在[2004年雅典奧運中在](../Page/2004年雅典奧運.md "wikilink")4×200-{zh-hans:米;
zh-hant:公尺;}-自由泳接力中代表美國隊勇奪金牌，以及在200-{zh-hans:米;
zh-hant:公尺;}-混合泳項目中奪得銀牌。另外，他在[2008年北京奧運會中亦奪得](../Page/2008年北京奧運會.md "wikilink")200-{zh-hans:米;
zh-hant:公尺;}-混合泳銅牌、400-{zh-hans:米; zh-hant:公尺;}-混合泳銅牌及200-{zh-hans:米;
zh-hant:公尺;}-背泳金牌。

洛赫特的強項在於[背泳以及混合泳](../Page/背泳.md "wikilink")，但是他也是一名[自由泳接力的泳手](../Page/自由泳.md "wikilink")。他在混合泳的主要對手是[迈克尔·菲尔普斯而背泳的主要對手是](../Page/迈克尔·菲尔普斯.md "wikilink")[阿龙·佩尔索尔](../Page/阿龙·佩尔索尔.md "wikilink")。

## 個人生活

洛赫特出生於[美國紐約州的](../Page/美國.md "wikilink")[羅徹斯特](../Page/羅徹斯特_\(紐約州\).md "wikilink")。他於佛羅里達州升學並且於[佛羅里達大學完成大學課程](../Page/佛羅里達大學.md "wikilink")。

## 生涯

### 大學生涯

在2006年[阿特蘭大舉行的](../Page/阿特蘭大.md "wikilink")[全美大學生游泳錦標賽中](../Page/全美大學生游泳錦標賽.md "wikilink")，洛赫特贏得了個人的三項冠軍並打破了美國200米混合泳及200米背泳的世界紀錄，並差點兒也打破400米混合泳的美國紀錄。在4x100-{zh-hans:米;
zh-hant:公尺;}-四式接力中，洛赫特打破了100米背泳紀錄，這是首次美國背泳紀錄比蝶泳紀錄快\[1\]。

### 國際舞台

[Michael_Phelps_Ryan_Lochte_Laszlo_Cseh_medals_2008_Olympics.jpg](https://zh.wikipedia.org/wiki/File:Michael_Phelps_Ryan_Lochte_Laszlo_Cseh_medals_2008_Olympics.jpg "fig:Michael_Phelps_Ryan_Lochte_Laszlo_Cseh_medals_2008_Olympics.jpg")400-{zh-hans:米;
zh-hant:公尺;}-混合泳比賽結果
洛赫特 (左) -{zh-hans:迈克尔·菲尔普斯;zh-hk:米高·菲比斯;zh-tw:麥可·費爾普斯;}- (中)
拉斯洛·-{zh-hans:切赫;zh-hk:施赫;zh-tw:塞赫;}- (右)\]\]
洛赫特於[2004年雅典奧運會](../Page/2004年雅典奧運會.md "wikilink")200-{zh-hans:米;
zh-hant:公尺;}-混合泳的美國奧運選拔賽中僅僅落後於游泳天才[迈克尔·菲尔普斯之後](../Page/迈克尔·菲尔普斯.md "wikilink")，而且入選了4x200-{zh-hans:米;
zh-hant:公尺;}-的[美國接力隊](../Page/美國.md "wikilink")。在奧運會中，洛赫特與菲尔普斯、[克萊特·凱勒](../Page/克萊特·凱勒.md "wikilink")、[彼得·范德爾卡伊力拒](../Page/彼得·范德爾卡伊.md "wikilink")[澳洲隊力來犯](../Page/澳洲.md "wikilink")，成功摘下金牌。在200-{zh-hans:米;
zh-hant:公尺;}-混合泳亦再次落後於菲尔普斯，僅得銀牌。而在隨後於[印第安納波利斯舉行的](../Page/印第安納波利斯.md "wikilink")2004年[世界短池游泳錦標賽中](../Page/世界短池游泳錦標賽.md "wikilink")，洛赫特於200-{zh-hans:米;
zh-hant:公尺;}-混合泳奪得銀牌及於200-{zh-hans:米;
zh-hant:公尺;}-自由泳的銅牌。另外在4x200-{zh-hans:米;
zh-hant:公尺;}-自由泳接力中亦奪得了金牌。

於2005年[蒙特利爾](../Page/蒙特利爾.md "wikilink")[世界游泳錦標賽中](../Page/世界游泳錦標賽.md "wikilink")，他亦贏得了200-{zh-hans:米;
zh-hant:公尺;}-背泳\[2\]及混合泳的銅牌，亦再次為美國4x200-{zh-hans:米;
zh-hant:公尺;}-自由泳接力的冠軍。

於2007年[墨爾本](../Page/墨爾本.md "wikilink")[世界游泳錦標賽中](../Page/世界游泳錦標賽.md "wikilink")，他首次於長池的比賽中奪得金牌，在200-{zh-hans:米;
zh-hant:公尺;}-背泳的比賽中以破世界紀錄的成績擊敗老對手[阿龙·佩尔索尔](../Page/阿龙·佩尔索尔.md "wikilink")，這也是洛赫特第一個個人世界紀錄\[3\]。

[2008年北京奧運會中](../Page/2008年北京奧運會.md "wikilink")，雖然兩項混合泳的比賽均僅次於菲尔普斯及匈牙利選手[拉斯洛·-{zh-hans:切赫;zh-hk:施赫;zh-tw:塞赫;}-](../Page/拉斯洛·切赫.md "wikilink")，只得銅牌\[4\]\[5\]。可是，洛赫特在背泳中卻有上佳的演出。在200-{zh-hans:米;
zh-hant:公尺;}-背泳決賽中更以破世界紀錄的成績壓倒[阿龙·佩尔索尔](../Page/阿龙·佩尔索尔.md "wikilink")，奪得首面個人比賽金牌\[6\]。

[2009年羅馬世界游泳錦標賽中](../Page/2009年世界游泳錦標賽.md "wikilink")，奪得200米背泳中銅牌，200米及400米個人混合泳兩面金牌，其中200米混合泳以1分54秒10成績破世界記錄。他在4x100米自由泳接力中，再次協助美國奪得金牌同時打破大會記錄。其後4x200米自由泳接力中，再次協助美國奪得金牌同時刷新世界記錄至6分58秒55。

## 爭議

洛赫特和其他三名美國游泳選手在[2016年奧運會時聲稱當地的加油站遭到槍手搶劫](../Page/2016年奧運會.md "wikilink")，洛赫特說事情是發生在加油站，加油站的警衛持槍對著他們。後來發現洛赫特是在酒醉後破壞加油站設備，並且在加油站公開小便，警衛拔槍制止，之後才答應賠償，許多原先所說的情節都和事實不符\[7\]\[8\]\[9\]。

2016年9月8日，瑞安·洛赫特被禁賽10個月、失去奧運獎金及美國游泳協會資助、不得參加表揚儀式並同時10個小時社會服務，其餘三名隊友則被禁賽4個月。\[10\]

## 参考文献

## 外連連結

  - [Ryan Lochte's U.S. Olympic Team
    bio](http://www.usoc.org/26_21758.htm)
  - [USASwimming
    profile](http://www.usaswimming.org/USASWeb/DesktopModules/BioViewManaged.aspx?personid=ef40c7a1-7499-4931-8027-f8a7f1c4175d&TabId=388&Mid=597)

{{-}}

[Category:美国游泳运动员](../Category/美国游泳运动员.md "wikilink")
[Category:游泳世界紀錄保持者](../Category/游泳世界紀錄保持者.md "wikilink")
[Category:美國奧林匹克運動會金牌得主](../Category/美國奧林匹克運動會金牌得主.md "wikilink")
[Category:美國奧林匹克運動會銀牌得主](../Category/美國奧林匹克運動會銀牌得主.md "wikilink")
[Category:美國奧林匹克運動會銅牌得主](../Category/美國奧林匹克運動會銅牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2012年夏季奧林匹克運動會獎牌得主](../Category/2012年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2016年夏季奧林匹克運動會獎牌得主](../Category/2016年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:佛羅里達大學校友](../Category/佛羅里達大學校友.md "wikilink")
[Category:2004年夏季奧林匹克運動會游泳運動員](../Category/2004年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會游泳運動員](../Category/2008年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2012年夏季奧林匹克運動會游泳運動員](../Category/2012年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2016年夏季奧林匹克運動會游泳運動員](../Category/2016年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:奧林匹克運動會游泳獎牌得主](../Category/奧林匹克運動會游泳獎牌得主.md "wikilink")
[Category:世界游泳錦標賽游泳賽事獎牌得主](../Category/世界游泳錦標賽游泳賽事獎牌得主.md "wikilink")
[Category:世界短道游泳錦標賽獎牌得主](../Category/世界短道游泳錦標賽獎牌得主.md "wikilink")
[Category:男子自由式游泳運動員](../Category/男子自由式游泳運動員.md "wikilink")
[Category:紐約州人](../Category/紐約州人.md "wikilink")
[Category:佛羅里達州人](../Category/佛羅里達州人.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:古巴裔美國人](../Category/古巴裔美國人.md "wikilink")
[Category:混合泳运动员](../Category/混合泳运动员.md "wikilink")

1.
2.
3.
4.  [男子400米个人混合泳决赛](https://web.archive.org/web/20080813032846/http://results.beijing2008.cn/WRM/CHI/INF/SW/C73A1/SWM054101.shtml#SWM054101)
5.  [男子200米个人混合泳决赛](https://web.archive.org/web/20080818172250/http://results.beijing2008.cn/WRM/CHI/INF/SW/C73A1/SWM052101.shtml#SWM052101)
6.  [男子200米仰泳决赛](https://web.archive.org/web/20080818172245/http://results.beijing2008.cn/WRM/CHI/INF/SW/C73A1/SWM042101.shtml#SWM042101)
7.
8.
9.  [美國游泳選手在里約被搶證實是謊報](https://tw.news.yahoo.com/%E7%BE%8E%E5%9C%8B%E6%B8%B8%E6%B3%B3%E9%81%B8%E6%89%8B%E5%9C%A8%E9%87%8C%E7%B4%84%E8%A2%AB%E6%90%B6%E8%AD%89%E5%AF%A6%E6%98%AF%E8%AC%8A%E5%A0%B1-225620866.html)
10.