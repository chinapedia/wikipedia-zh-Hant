**瑞文戴爾**
（，[西方通用语](../Page/西方通用语.md "wikilink")：Karningul，[辛達語](../Page/辛達語.md "wikilink")：**伊姆拉崔**
**Imladris**，意為「深之裂谷」），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·鲁埃尔·托爾金的史詩式奇幻小說](../Page/約翰·羅納德·鲁埃尔·托爾金.md "wikilink")《[魔戒](../Page/魔戒.md "wikilink")》中，位於[中土大陸](../Page/中土大陸.md "wikilink")[迷霧山脈中的](../Page/迷霧山脈.md "wikilink")[精靈據點](../Page/精靈_\(中土大陸\).md "wikilink")\[1\]。

瑞文戴爾號稱是「大海以東最後一處精靈的庇護所」。她由[半精靈](../Page/半精靈.md "wikilink")[愛隆於](../Page/愛隆.md "wikilink")[第二紀元建立](../Page/第二紀元.md "wikilink")，除愛隆以外，還有[亞玟和](../Page/亞玟.md "wikilink")[葛羅芬戴爾等著名精靈居住在那裡](../Page/葛羅芬戴爾.md "wikilink")。

## 历史

第二紀元，在[伊瑞詹被索倫的軍隊攻陷之後](../Page/伊瑞詹.md "wikilink")，愛隆帶著生還的[諾多精靈建立了精靈庇護所瑞文戴爾](../Page/諾多精靈.md "wikilink")\[2\]。

瑞文戴爾在古代對抗[索倫的戰爭中](../Page/索倫_\(魔戒\).md "wikilink")，擔當重要的角色，第二紀元3429年，索倫向[剛鐸發動攻擊](../Page/剛鐸.md "wikilink")，其時[登丹人最高君王](../Page/登丹人.md "wikilink")[伊蘭迪爾及諾多族最高君王](../Page/伊蘭迪爾.md "wikilink")[吉爾加拉德為反擊索倫](../Page/吉爾加拉德.md "wikilink")，組成聯盟，以精靈及[人類兩者之力對抗邪惡](../Page/人類_\(中土大陸\).md "wikilink")，史稱[精靈及人類最後同盟](../Page/精靈及人類最後同盟.md "wikilink")。聯合大軍於瑞文戴爾整軍，其軍容令人追想[維拉攻打](../Page/維拉.md "wikilink")[魔苟斯的](../Page/魔苟斯.md "wikilink")[憤怒之戰](../Page/憤怒之戰.md "wikilink")。他們經三年整頓集結之後，出兵至[達哥拉](../Page/達哥拉.md "wikilink")，索倫在此戰敗亡，而伊蘭迪爾及吉爾加拉德亦一同陣亡\[3\]。

第三纪元2941年，[比爾博·巴金斯與](../Page/比爾博·巴金斯.md "wikilink")[甘道夫及](../Page/甘道夫.md "wikilink")[矮人山下國王](../Page/矮人_\(中土大陸\).md "wikilink")[索林·橡木盾等探險隊員途經瑞文戴爾](../Page/索林·橡木盾.md "wikilink")，瑞文戴爾之名第一次出現在托爾金作品中。描述中，瑞文戴爾隱藏於重重險要的[山谷](../Page/山谷.md "wikilink")、[森林](../Page/森林.md "wikilink")、[沼澤和](../Page/沼澤.md "wikilink")[溪流之中](../Page/溪流.md "wikilink")，隱而不顯。有一條被[苔蘚覆著的白色碎石小徑可通入瑞文戴爾山谷](../Page/苔蘚.md "wikilink")，進入以後，會嘗到充滿[樹木清香的空氣](../Page/樹木.md "wikilink")，溫暖燈光自山谷內耀起\[4\]。

經過[松樹](../Page/松樹.md "wikilink")、[樺樹](../Page/樺樹.md "wikilink")、[橡樹組成的林內小徑後](../Page/橡樹.md "wikilink")，會到達河流邊緣的開闊草地，精靈們在那裡歌唱。接著，又要渡過一座非常狹窄，沒有護欄的小橋，橋底下是湍急的河流，由山中雪水融化而成，眾人拜會愛隆，愛隆解讀了古代矮人王族地圖上的月之文字\[5\]。

幾十年之後，[魔戒聖戰爆發](../Page/魔戒聖戰.md "wikilink")，作品中更細緻描述瑞文戴爾內部景色。精靈王的宮殿布滿懸崖之中，[瀑布處處](../Page/瀑布.md "wikilink")，遠古諾多族大動亂而流亡中土大陸的精靈貴族，人類英雄後代——[亞爾諾的末落王孫也在此出沒](../Page/亞爾諾.md "wikilink")，他們皆由愛隆王庇護，愛隆王擁有的[精靈三戒之一氣之戒](../Page/精靈三戒.md "wikilink")[維雅](../Page/精靈三戒#維雅.md "wikilink")，保護瑞文戴爾不受邪惡入侵\[6\]。

[Rivendell_2.jpg](https://zh.wikipedia.org/wiki/File:Rivendell_2.jpg "fig:Rivendell_2.jpg")

[佛羅多·巴金斯和他的](../Page/佛羅多·巴金斯.md "wikilink")[哈比人伴侶在](../Page/哈比人.md "wikilink")[甘道夫指示下攜](../Page/甘道夫.md "wikilink")[至尊魔戒離開](../Page/至尊魔戒.md "wikilink")[夏爾](../Page/夏爾.md "wikilink")，前往瑞文戴爾，途中被[戒靈追殺](../Page/戒靈.md "wikilink")，佛羅多更被[魔窟劍刺了一劍](../Page/魔窟劍.md "wikilink")，幸得葛羅芬戴爾出現救助。他們走到瑞文戴爾的界河，戒靈窮追不捨，佛羅多以維拉星宿女神[瓦爾妲及以曾打敗索倫](../Page/瓦爾妲.md "wikilink")、愛隆和[努曼諾爾帝國皇帝](../Page/努曼諾爾帝國.md "wikilink")[愛洛斯的祖先](../Page/愛洛斯.md "wikilink")[露西安之名](../Page/露西安.md "wikilink")，誓言戒靈絕得不到他和至尊魔戒，接著河水化成白馬騎士狀，把戒靈淹沒（因[阿爾達所有水體皆由維拉大海之神](../Page/阿爾達.md "wikilink")[烏歐牟管轄](../Page/烏歐牟.md "wikilink")，戒靈力量無力化）。

來到瑞文戴爾，佛羅多得到治療，他們遇見[退休的比爾博](../Page/退休.md "wikilink")，他在那裡度過111歲的[生日](../Page/生日.md "wikilink")，著力寫作史書《[西境紅皮書](../Page/西境紅皮書.md "wikilink")》。精靈、矮人和人類三個[種族的領導者在瑞文戴爾召開](../Page/中土大陸的種族.md "wikilink")[愛隆會議](../Page/愛隆會議.md "wikilink")，會上愛隆決定由精靈、矮人、人類和哈比人組成[魔戒遠征隊](../Page/魔戒遠征隊.md "wikilink")，將至尊魔戒投入[末日火山之中摧毀](../Page/末日火山.md "wikilink")。

最後，索倫再一次敗亡。在[第四紀元到來前](../Page/第四紀元.md "wikilink")，愛隆等[魔戒持有者乘船前往](../Page/魔戒持有者.md "wikilink")[阿門洲](../Page/阿門洲.md "wikilink")，一去不回頭，精靈亦漸漸遷出，渡海而去，瑞文戴爾隨時間流逝而隱沒違忘。

## 創作

[瑞士小鎮](../Page/瑞士.md "wikilink")[盧達本納](../Page/盧達本納.md "wikilink")（Lauterbrunnental）是瑞文戴爾的靈感來源，托爾金曾於1911年在那裡旅行過\[7\]。

托爾金曾經親自繪製過以瑞文戴爾為主題的插畫\[8\]。

## 改編

在[彼得·傑克森執導的](../Page/彼得·傑克森.md "wikilink")《[魔戒](../Page/魔戒電影三部曲.md "wikilink")》電影三部曲中，瑞文戴爾的外景是在[紐西蘭](../Page/紐西蘭.md "wikilink")[威靈頓地區的](../Page/威靈頓地區.md "wikilink")[凱多可地區公園取景](../Page/凱多可地區公園.md "wikilink")\[9\]\[10\]。片中瑞文戴爾內部的一些布景和[壁畫都是藝術家](../Page/壁畫.md "wikilink")[艾倫·李所設計的](../Page/阿兰·李_\(插画家\).md "wikilink")\[11\]。

## 參考文獻

<div class="references-small">

</div>

[de:Regionen und Orte in Tolkiens
Welt\#Bruchtal](../Page/de:Regionen_und_Orte_in_Tolkiens_Welt#Bruchtal.md "wikilink")
[lb:Länner a Stied aus
Middle-earth\#Rivendell](../Page/lb:Länner_a_Stied_aus_Middle-earth#Rivendell.md "wikilink")

[R](../Category/中土大陸的城鎮.md "wikilink")

1.

2.

3.

4.

5.
6.  《[魔戒](../Page/魔戒.md "wikilink")》
    [約翰·羅納德·鲁埃尔·托爾金著](../Page/約翰·羅納德·鲁埃尔·托爾金.md "wikilink")

7.  [Rivendell in
    Switzerland](http://scv.bu.edu/~aarondf/Rivimages/realriv.html).
    *scv.bu.edu*

8.

9.

10.

11.