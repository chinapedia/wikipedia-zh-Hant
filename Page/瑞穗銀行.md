**瑞穗銀行**（，[英語](../Page/英語.md "wikilink")：，简称：MHBK）是[日本的](../Page/日本.md "wikilink")[商業銀行](../Page/商業銀行.md "wikilink")（[都市銀行](../Page/都市銀行.md "wikilink")）之一，為[瑞穗金融集團成員](../Page/瑞穗金融集團.md "wikilink")，同时也是日本3大[巨型銀行之一](../Page/巨型銀行.md "wikilink")。

## 簡介

瑞穗银行主要經營[个人](../Page/个人.md "wikilink")、[中小企业和](../Page/中小企业.md "wikilink")[地方政府的银行业务](../Page/地方政府.md "wikilink")。而大型企業、[金融机构和海外业务原由同集团下的](../Page/金融机构.md "wikilink")[瑞穗实业银行](../Page/瑞穗实业银行.md "wikilink")（）来担当，但兩者於2013年7月1日合併。

### 成立

瑞穗銀行誕生於2002年，由当時瑞穗金融集團的、、分割合併而成，在法律手續上是以第一勸業銀行作為企業主體，總部位于[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")[内幸町的旧第一勸業銀行總部](../Page/内幸町.md "wikilink")，行名取自「[瑞穗](../Page/瑞穗金融集團.md "wikilink")」的[平假名](../Page/平假名.md "wikilink")“”（Mizuho）。

### 偿还公共资金

2005年，瑞穗银行通过数年的努力，处理了许多[九〇年代残留下来的不良债务](../Page/日本泡沫经济.md "wikilink")，最终把将近2,800亿[日元的公共资金归还给了政府财政部门](../Page/日元.md "wikilink")，一时瑞穗的信用急长。同时在2006年7月4日，[三菱UFJ金融集团也偿还了它们所有的政府救助金](../Page/三菱UFJ金融集团.md "wikilink")。

2006年11月8日，总公司瑞穗金融集团在[纽约证券交易所上市](../Page/纽约证券交易所.md "wikilink")，这也是日本银行自[泡沫经济崩溃以来第一次在纽交所上市](../Page/日本泡沫经济.md "wikilink")。

### 企业营运

瑞穗银行的主要客户为相对富裕的阶层，提供给他们相应的私人理财业务，特别是其下的瑞穗个人理财公司（2005年10月1日正式成立），专门为个人[资产超过](../Page/资产.md "wikilink")5亿日元的客户提供服务。

## 历史

  - 2000年9月29日 - 第一劝业银行、富士银行和日本兴业银行通过股票转移的方法成立瑞穗持股（），並將這三家銀行納為子公司。
  - 2002年4月1日 -
    第一勸業銀行合併富士銀行與日本興業銀行的個人金融業務，並更名為**株式會社瑞穗銀行**。其中，日本興業銀行的個人金融業務在更名當日先移交予2002年1月8日成立的瑞穗统合准备银行（），隨即於同日併入瑞穗銀行；日本興業銀行本身則將富士銀行併入，更名為[瑞穗實業銀行](../Page/瑞穗實業銀行.md "wikilink")。
  - 2013年7月1日 -
    瑞穗實業銀行吸收合併瑞穗銀行，但沿用瑞穗銀行的[公司名稱](../Page/公司.md "wikilink")。\[1\]

## 參見

  - [瑞穗金融集团](../Page/瑞穗金融集团.md "wikilink")
  - [巨型銀行](../Page/巨型銀行.md "wikilink")

## 參考資料

## 外部链接

  - [瑞穗銀行](https://www.mizuhobank.co.jp)

[category:芙蓉集團](../Page/category:芙蓉集團.md "wikilink")

[Category:日本銀行](../Category/日本銀行.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:瑞穗金融集團](../Category/瑞穗金融集團.md "wikilink")
[Category:東京證券交易所已除牌公司](../Category/東京證券交易所已除牌公司.md "wikilink")
[Category:經營問題](../Category/經營問題.md "wikilink")
[Category:第一勸銀集團](../Category/第一勸銀集團.md "wikilink")
[Category:都市銀行](../Category/都市銀行.md "wikilink")

1.