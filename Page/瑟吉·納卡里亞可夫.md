**瑟吉·納卡里亞可夫**（Sergei
Nakariakov，1977年5月10日－）岀生於[俄羅斯](../Page/俄羅斯.md "wikilink")[高尔基](../Page/下诺夫哥罗德.md "wikilink")，為一世界知名的[小號獨奏家](../Page/小號.md "wikilink")，當他13歲時，就已被芬蘭的出版界讚譽為「小號的[帕格尼尼](../Page/帕格尼尼.md "wikilink")」（Paganini
of the Trumpet），之後在1997年，音樂與劇場雜誌也稱他為「小號的卡路索」（Caruso of the
Trumpet）。除小號以外，納卡里亞可夫同時致力於[柔音號的演奏與推廣](../Page/柔音號.md "wikilink")。

## 生平

納卡里亞可夫從小學習鋼琴，但在1986年的一次車禍中背部受傷，不能久坐，於是他被迫放棄鋼琴，轉而隨父親米哈伊爾・納卡里亞可夫（Mikhail
Nakariakov）學習小號演奏。

1987年，10歲的納卡里亞可夫開始參加在[聖彼得堡舉行的多項青年音樂家比賽](../Page/聖彼得堡.md "wikilink")，1991年在波哥雷里奇音樂節登台，同年8月與立陶宛室內管弦樂團於薩爾茲堡音樂節演出，此後活躍於世界各大音樂廳與音樂節中。獨奏會的鋼琴伴奏經常是由他的姊姊薇拉・納卡里亞可娃（Vera
Nakariakova）擔任。

1992年納卡里亞可夫與[Teldec唱片公司簽約並發行首張專輯](../Page/Teldec.md "wikilink")《新小號時代》（Trumpet
Works），其中包含了[拉威爾](../Page/拉威爾.md "wikilink")、[蓋希文等作曲家的作品](../Page/蓋希文.md "wikilink")，以及阿爾班的《威尼斯狂歡節》（Carnival
of Venice）。

## 專輯

  - 新小號時代（Trumpet Works）
  - 海頓、胡麥爾：小喇叭協奏曲（Haydn, Hommel, Tomasi, Jolivet: Trumpet Concertos）
  - 卡門幻想曲（Carmen Fantasy - Virtuoso Music for Trumpet）
  - 巴洛克小號協奏作品集（ Baroque Trumpet Concertos）
  - 悲歌（Elegie）
  - 海頓、孟德爾頌：小號改編作品集（Haydn, Hoffmeister, Mendelssohn: Concertos For
    Trumpet）
  - 超越極限（No Limit）
  - 莫斯科捎來的愛（From Moscow with Love）
  - 往事迴響（Echos from The Past）

## 外部連結

  - [納卡里亞可夫的官方網站](http://www.nakariakov.info/)

[Category:1977年出生](../Category/1977年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[N](../Category/俄羅斯小號演奏家.md "wikilink")
[N](../Category/古典小號演奏家.md "wikilink")