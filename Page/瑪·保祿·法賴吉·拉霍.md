**保祿·法賴吉·拉霍**（，**；，**，）是[加色丁天主教摩蘇爾總主教](../Page/加色丁天主教摩蘇爾總教區.md "wikilink")\[1\]。

別號保祿·法賴吉·拉霍，幾乎一生皆生活在[摩蘇爾](../Page/摩蘇爾.md "wikilink")，在[亞述家園的中心地帶](../Page/亞述家園.md "wikilink")，有一處已建立很久的[加色丁基督徒社區](../Page/加色丁基督徒.md "wikilink")。

## 傳記

保祿·法賴吉·拉霍於1942年出生在[摩蘇爾](../Page/摩蘇爾.md "wikilink")。自1965年6月10日被授予圣職之后，幾乎都在摩蘇爾聖依撒意亞教堂工作。\[2\]
拉霍后來在摩蘇爾的一個新區岹凱帕（Tel Keppe）建立了一座圣心教堂。他還為殘障兒童設立了一所孤兒院\[3\]。

### 摩蘇爾總主教

在2001年2月16日，他被授以[聖職加色丁摩蘇爾總主教](../Page/授職.md "wikilink")，給予他教化十個教區超過20,000[天主教徒的責任](../Page/天主教徒.md "wikilink")。

### 對沙里亞的不安

拉霍对在[伊拉克憲法加入](../Page/伊拉克憲法.md "wikilink")[沙里亞条目表示深切的擔憂](../Page/沙里亞.md "wikilink")，他窮盡一生為各種困境祈禱。在2007年他去[羅馬旅行期間](../Page/羅馬.md "wikilink")，他曾對巴比倫宗主教[厄瑪奴耳三世德里](../Page/厄瑪奴耳三世德里.md "wikilink")（后來被任命為[樞機主教](../Page/樞機主教.md "wikilink")）說過他在伊拉克受到槍手的威脅。\[4\]
接著伊拉克戰爭的開始，對迫害基督徒加劇。在2004年6月之後一波炸彈攻擊他的主教住所，當地的穆斯林宗教領袖給予他住處及支持。

## 綁架並殺害

2008年2月29日，一則由[天主教新聞服務的報導](../Page/天主教新聞服務.md "wikilink")，於摩蘇爾市的努爾（Al-Nur）地區，拉霍總主教在他的汽車中遭到[劫持](../Page/劫持.md "wikilink")；他的安全人員及司機當場遇害身亡。\[5\]

2008年3月13日，報導說總主教的遺體已經尋獲，被淺埋在摩蘇爾附近。\[6\]\[7\]

保祿·法賴吉·拉霍總主教被認為是目前的[伊拉克戰爭中被殺害的](../Page/伊拉克戰爭.md "wikilink")[加色丁天主教地位最高的神職人員](../Page/加色丁天主教會.md "wikilink")。\[8\]

## 引用

## 參照

## 外部連結

  - [Obituary in *The Times*, 13 March
    2008](http://www.timesonline.co.uk/tol/comment/obituaries/article3547009.ece)

  - [Archbishop Paulos Faraj
    Rahho](http://www.catholic-hierarchy.org/bishop/brahho.html)
    Catholic Hierarchy

  - [伊拉克總主教遭綁架 教宗本篤呼籲釋放人質](http://kkp.catholic.org.hk/gl/gl3342.htm)

  - [羅馬教皇譴責對伊拉克大主教的殘殺](http://www.ntdtv.com/xtr/b5/2008/03/13/a_73981.html)

  - [伊拉克被綁主教被撕票
    曾被索100萬美元贖金](http://cnt.reuters.com/article/europeNews/idCNTChina-797720080314)

  - [遭綁架伊拉克大主教遺體尋獲](http://tw.news.yahoo.com/article/url/d/a/080314/19/vap6.html)

[Category:伊拉克天主教徒](../Category/伊拉克天主教徒.md "wikilink")
[Category:殉職者](../Category/殉職者.md "wikilink")
[Category:加色丁總主教](../Category/加色丁總主教.md "wikilink")
[Category:在伊拉克被謀殺身亡者](../Category/在伊拉克被謀殺身亡者.md "wikilink")
[Category:摩苏尔人](../Category/摩苏尔人.md "wikilink")

1.  [Archbishop Paul Faraj Rahho: The Times
    obituary](http://www.timesonline.co.uk/tol/comment/obituaries/article3547009.ece).
    March 14, 2008.

2.
3.
4.
5.

6.  .

7.

8.