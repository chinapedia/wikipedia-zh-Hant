**瑪利諾神父教會學校**（中學部）（）是一所[天主教的津貼中學由美国天主教传教会于](../Page/天主教.md "wikilink")1957年成立。瑪利諾神父教會學校（小學部）（）是一所天主教的資助小學。直至2008至2009學年，中學部全校學生共965人。目前中一至中三開設一班以個別科目中文為教學語言一班。

學校科研成績突出，共培育了多名全國賽一等獎學生及三名[星之子](../Page/星之子.md "wikilink")，包括: 劉德誠
(25073)、劉德健 (25065)及 簡泳怡 (31313)。

現屆中學部校長為何力生先生。\[1\]小學部校長為吳偉文博士。

2014年，高中選科制度從2014-2015中四屆修改，由兩科選修科跟固定班別一科自選，改為三科自選，增加自主性及彈性。

## 歷史

瑪利諾神父教會學校中學部為香港首間津貼中學，與小學部一同創辦於1957年，創校校監為賴存忠神父，\[2\]創校校長為[唐余湘畹女士](../Page/唐余湘畹.md "wikilink")，中學部是一間創校歷史悠久、校風良好、淳樸的Band1英中。
小學部在2003年11月申請新校舍，位於深水埗海麗街11號的新校舍於2008年11月落成。小學部於2009年1月，上、下午校一至六年級24班學生同時遷進新校舍，並於同年2月轉為全日制。現時桃源街小學部舊校舍，已供本校中學部發展。
[MaryknollFathersSchool.jpg](https://zh.wikipedia.org/wiki/File:MaryknollFathersSchool.jpg "fig:MaryknollFathersSchool.jpg")

### 歷任校監

  - 第1任：賴存忠神父 Rev. Peter Alphonsus Reilly, M.M.（1957年 - 1994年）
  - 第2任：江天文神父 Rev. John Geitner, M.M.（1995年 - 2011年）
  - 第3任：沈嘉仁神父 Rev. Michael Sloboda, M.M. （2012年 - 2016年）
  - 第4任：葉嘉雯女士 Ms. Agnes Garman Yeh（2017年 - 至今）

## 中學部學校設施

中學部：

A座：

  - 綜合實驗室A - 為中二生科目 綜合科學所設之實驗室
  - 綜合實驗室B - 為中一生科目 綜合科學所設之實驗室
  - 地理室
  - 兩間科研室
  - 共8間中二及中三課室
  - 校務處、校長室、校監室
  - 醫療室
  - 學校大門（面向救世軍）

B座：

  - 多用途室A（學生會及四社室）
  - 多用途室B - 設有午间活动（12:50PM - 1:30PM）
  - 學生活動室
  - 電腦輔助教學室（電腦室A）
  - 語言學習室（電腦室B）
  - 音樂室A
  - 小賣部
  - 小聖堂
  - 社工室(會面室A)
  - 會面室B
  - 圓桌廣場

C座：

  - 體育室
  - 生物實驗室
  - 化學實驗室
  - 物理實驗室
  - 圖書館
  - 禮堂
  - 男/女更衣室
  - SGT 1-4
  - 共8間中四及中五課室
  - 校園電視台
  - 電腦室C

D座：

  - SGT 5-8
  - 共8間中一及中六課室
  - [黑房](../Page/黑房.md "wikilink")
  - 訓導室（領袖生室）

E座：

  - 英語天地 English Zone
  - Debate hall
  - cafe
  - 教員室（3-4樓全層）

<!-- end list -->

  - 另外設有一個有蓋籃球場、一個足球場以及一個露天籃球場。A座及C座後面均設有花圃。C座後面有很大的後院。

## 班級結構

中一至中六級設四班（A、B、C、D），每班約30-40人；在2011年,最後一年中七級共設兩班（中七：6UA、6US；Arts &
Science），每班約30人。

在新高中學制推行前，學校推行二文二理制度。中四級及中五級的A、B班為文科班，C、D班為理科班。而現在學校推行新高中學制，同學可以以自己的心願以成續排名選科。
中文、英文、數學及通識為所有高中學生的必修科目。另外每班將會有兩科指定的選修科（A班：X1：世界歷史科，X2：地理科、B班：X1：經濟科、X2：資訊及通訊科技科、C班：X1：生物科，X2:化學科，D班：X1:化學科,
X2:物理科。每班之X1科可以以設計與應用科技科代替，X2科則可以以視覺藝術科代替。X3選修科目學生可選擇企業、會計與財務概論科、生物科、中國歷史科、經濟科、地理科、資訊及通訊科技科以及物理科（不得和X1及X2科目重複），也同時提供X4科目，學生可以自行選擇參與或不參與。X4選修科目提供多種外語學科，如下：[法文](../Page/法文.md "wikilink")、[西班牙文及](../Page/西班牙文.md "wikilink")[日文](../Page/日文.md "wikilink")，另有[音樂科及學生選擇](../Page/音樂.md "wikilink")（視覺藝術科、設計與應用科技科及音樂科均由藝術與科技教育中心提供上課地點及導師）。

## 香港傑出學生選舉

直至2018年(第33屆)，在[香港傑出學生選舉共出產](../Page/香港傑出學生選舉.md "wikilink")1名傑出學生。

## 著名校友

  - [黎棟國](../Page/黎棟國.md "wikilink")：前[保安局局長](../Page/保安局.md "wikilink")，現第13届港区全国政协委员（1963年小六；1968年中五）\[3\]
  - [鄺美玲](../Page/鄺美玲.md "wikilink")：香港第一位女外科醫生
  - [楊瑞麟](../Page/楊瑞麟.md "wikilink")：[無綫電視藝員](../Page/無綫電視.md "wikilink")
  - [譚得志（快必）](../Page/快必.md "wikilink")：前[香港電台唱片騎師](../Page/香港電台.md "wikilink")，现[人民力量副主席](../Page/人民力量.md "wikilink")
  - [峻光](../Page/峻光.md "wikilink")：香港[創作男歌手](../Page/創作歌手.md "wikilink")
  - [蘇曜華](../Page/蘇曜華.md "wikilink")：香港醫院藥劑師學會會長
  - [曾淑儀](../Page/曾淑儀.md "wikilink")：前[香港電台唱片騎師](../Page/香港電台.md "wikilink")
  - [陳珍妮](../Page/陳珍妮.md "wikilink")：前[無綫新聞主播](../Page/無綫新聞.md "wikilink")
  - [姚健文](../Page/姚健文.md "wikilink")：前[亞視新聞記者](../Page/亞視新聞.md "wikilink")／主播
  - [高芳婷](../Page/高芳婷.md "wikilink")：前[無綫新聞主播](../Page/無綫新聞.md "wikilink")
  - [何錦欣](../Page/何錦欣.md "wikilink")：前[無綫新聞記者](../Page/無綫新聞.md "wikilink")／主播
  - [繆美詩](../Page/繆美詩.md "wikilink")：前[亞視新聞記者](../Page/亞視新聞.md "wikilink")／主播
  - [戴祖而](../Page/戴祖而.md "wikilink")：現任[NOW新聞台記者](../Page/NOW新聞台.md "wikilink")
  - [余嘉允](../Page/余嘉允.md "wikilink")：獨立傳媒人，澳洲政府配音員，前[DBC數碼電台](../Page/DBC數碼電台.md "wikilink")、[D100節目主持及](../Page/D100.md "wikilink")[香港網絡廣播電台台長](../Page/香港網絡廣播電台.md "wikilink")
  - [楊思琦](../Page/楊思琦.md "wikilink")：2001年[香港小姐冠軍](../Page/香港小姐.md "wikilink")
  - [黃遠志](../Page/黃遠志.md "wikilink")：[華英中學經濟科教師](../Page/華英中學.md "wikilink"),教科書作者
  - [譚汝謙](../Page/譚汝謙.md "wikilink")：知名歷史學家，美国默士达大学终身教授
  - [關信輝](../Page/關信輝.md "wikilink")：電影人，現任電影導演、編劇
  - [張文瑛](../Page/張文瑛.md "wikilink")：1974年[香港小姐冠軍](../Page/香港小姐.md "wikilink")
  - [譚真一](../Page/譚真一.md "wikilink")：香港童星
  - [林兆波](../Page/林兆波.md "wikilink")：香港第一先生
  - [張耀堂](../Page/張耀堂.md "wikilink")：[八達通集團行政總裁](../Page/八達通.md "wikilink")

## 參考文獻

## 外部連結

  - [瑪利諾神父教會學校](http://www.mfs1.edu.hk/)
  - [瑪利諾神父教會學校(小學部)](http://www.mfsp.edu.hk/)
  - [中學概覽 2011-2012](http://ssp.proj.hkedcity.net/chi/detail_basic.php?sch_id=1096)
  - [藝術與科技教育中心](http://www.atec.edu.hk/)

[瑪](../Category/香港天主教學校.md "wikilink")
[Category:石硤尾](../Category/石硤尾.md "wikilink")
[Category:長沙灣](../Category/長沙灣.md "wikilink")
[M](../Category/深水埗區中學.md "wikilink")
[Category:瑪利諾外方傳教會](../Category/瑪利諾外方傳教會.md "wikilink")
[Category:1957年創建的教育機構](../Category/1957年創建的教育機構.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")
[Category:深水埗區小學](../Category/深水埗區小學.md "wikilink")

1.
2.
3.