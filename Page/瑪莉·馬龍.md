**瑪莉·馬龍**（，），[愛爾蘭人](../Page/愛爾蘭.md "wikilink")，1883年獨自移民至美國，是[美國第一位被發現的](../Page/美國.md "wikilink")[傷寒健康](../Page/傷寒.md "wikilink")[帶原者](../Page/帶原者.md "wikilink")，因此被稱為**傷寒瑪莉**（）。瑪莉是一個廚師，並因此造成53人感染、3人死亡，但她堅決否認這項事實，也拒絕停止下廚，因此兩度遭[公共衛生的主管機關隔離](../Page/公共衛生.md "wikilink")，最後於隔離期間去世\[1\]。

## 疫情

瑪莉·馬龍於1900年至1907年在[紐約市內擔任廚師](../Page/紐約市.md "wikilink")，這段時間內她造成22人感染[傷寒熱](../Page/傷寒熱.md "wikilink")，其中1人不治身亡。不到兩週，[馬馬羅內克](../Page/馬馬羅內克.md "wikilink")（Mamaroneck）居民便開始受此病襲擊。

1901年瑪莉·馬龍決定搬至[曼哈頓](../Page/曼哈頓.md "wikilink")，在新的家庭中幫忙廚務，但該戶成員卻開始發燒、腹瀉，一位洗衣女孩因此死亡。隨後瑪莉改替一名律師工作，直到家中8名成員有7位染上傷寒，在照顧的過程中又將傷寒的感染向外擴散。1904年，她在[長島找到一份新工作](../Page/長島.md "wikilink")，兩週內11個家庭中有6戶因傷寒住院，瑪莉便因此再轉換工作，造成另外3個家庭的感染。

## 調查

在曾經因雇用瑪莉而爆發地區流行病的家庭之一聘請衛生官員[喬治·梭佩](../Page/喬治·梭佩.md "wikilink")（George
Soper）調查衛生狀況，經過仔細調查及推敲，確認瑪莉最有可能是疾病的帶原者。由於一般常見的感染者，多是經由食入帶原者所烹調的飲食而致病，人類帶原者常常是過去曾感染[傷寒的患者在症狀消失之後](../Page/傷寒.md "wikilink")，體內仍存有[傷寒桿菌](../Page/傷寒桿菌.md "wikilink")，但可以在[糞便或](../Page/糞便.md "wikilink")[尿液中發現這種](../Page/尿液.md "wikilink")[病原體](../Page/病原體.md "wikilink")。

當喬治·梭佩告將這項結論告訴瑪莉時，她憤怒地拒絕配合提供[排泄物的檢體樣本](../Page/排泄.md "wikilink")，隨後梭佩便將他的發現於1907年6月15日刊登在《[美國醫學聯合會期刊](../Page/美國醫學聯合會期刊.md "wikilink")》。梭佩第二次約談瑪莉時，帶著一位醫師相伴，但仍然無功而返，瑪莉基於某著名化學家的檢驗，否認自己是帶原者，該份檢驗報告認為瑪莉體內沒有這種細菌；而梭佩告訴她可能身為帶原者時，健康無症狀帶原的現象在學界仍不明瞭。

## 隔離

[Mary_Mallon_in_hospital.jpg](https://zh.wikipedia.org/wiki/File:Mary_Mallon_in_hospital.jpg "fig:Mary_Mallon_in_hospital.jpg")
[紐約市健康部門派遣](../Page/紐約市.md "wikilink")[薩拉·約瑟芬·貝克醫師](../Page/薩拉·約瑟芬·貝克.md "wikilink")（Sara
Josephine
Baker）和瑪莉約談，但她卻認定「法律是毫無理由地騷擾，因為她沒犯下任何過失。」\[2\]數天後貝克偕同數位警員至瑪莉的工作場地，帶走她且暫時監禁，隨後調查員在檢查過後發現她是一位帶原者，因此將她安置在[北兄弟島](../Page/北兄弟島.md "wikilink")（North
Brother
Island）的醫院隔離3年，並且要求她在離開後不得任職與食物有關的職業。但她又改名為「瑪莉·布朗」，在紐約的斯隆醫院（Sloan）掌廚，於1915年使25人感染，其中2位不治，公共衛生主管機關再次將她逮捕，判處終身隔離。結果她因此聲名大噪，接受記者們的訪問，前提是不能接受任何她提供的飲食，即使是水。最後她被允許在此島一處實驗室當技術人員\[3\]。終其一生她前後被強制隔離拘禁達26年，且此項隔離處置從未經過法院審查。

## 逝世

傷寒瑪莉1938年11月11日死於[肺炎](../Page/肺炎.md "wikilink")，而非傷寒，享年69歲，推測感染肺炎的原因是死前六個月[中風導致她癱瘓在床](../Page/中風.md "wikilink")\[4\]
。然而，驗屍後卻發現她的[膽囊中有許多活體傷寒桿菌](../Page/膽囊.md "wikilink")，遺骸最後在[布朗克斯的](../Page/布朗克斯.md "wikilink")[聖雷蒙墓園](../Page/聖雷蒙墓園.md "wikilink")（Saint
Raymond's Cemetery）[火化](../Page/火化.md "wikilink")\[5\]。

## 評論

瑪莉造成的問題部份乃肇因於她不顧一切地否認自身的處境，即使身上帶有足以致命的病原體，她仍保持[健康狀態](../Page/健康.md "wikilink")，且毫無感染過[傷寒的紀錄](../Page/傷寒.md "wikilink")\[6\]。但有些學者的研究認為，傷寒瑪莉所受待遇，與因其低下階層愛爾蘭裔移民的出身背景以致遭歧視不無關係\[7\]。過去有段時間，人們以「傷寒瑪莉」稱呼類似瑪莉·馬龍這種身為帶原者卻拒絕採取適當防範措施的人，由於該詞具有部分歧視、諷刺意味，現在一般將帶有病原體卻沒有[症狀的人稱為](../Page/症狀.md "wikilink")「[帶原者](../Page/帶原者.md "wikilink")」。

## 參考文獻

## 外部連結

  - ["Dinner with Typhoid Mary," Long Island
    History](https://web.archive.org/web/20090208020456/http://www.newsday.com/community/guide/lihistory/ny-history-hs702a,0,6698943.story)

  - [A more detailed profile of Typhoid
    Mary](http://history1900s.about.com/library/weekly/aa062900a.htm)
  - [PBS NOVA site: "The Most Dangerous Woman in
    America"](http://www.pbs.org/wgbh/nova/typhoid/)

{{-}}

[M](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:公共衛生史](../Category/公共衛生史.md "wikilink")
[Category:伤寒](../Category/伤寒.md "wikilink")

1.

2.

3.

4.
5.

6.

7.  Gregory P. Campbell, The Global H1N1 Pandemic, Quarantine Law, And
    The Due Process Conflict, 12 San Diego Int'l L.J.497, at 508(2011).