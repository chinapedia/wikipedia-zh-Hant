[IWM-SE-5742-tank-Surabaya-194511.jpg](https://zh.wikipedia.org/wiki/File:IWM-SE-5742-tank-Surabaya-194511.jpg "fig:IWM-SE-5742-tank-Surabaya-194511.jpg")的瑪蒙-哈寧頓CTLS，1945年。\]\]
**瑪蒙-哈寧頓CTLS轻型坦克**（Marmon-Herrington
CTLS）是[二戰時期](../Page/二戰.md "wikilink")[美軍的](../Page/美軍.md "wikilink")[輕型坦克](../Page/輕型坦克.md "wikilink")。

## 簡介

瑪蒙-哈寧頓CTLS可載兩名車組乘員，裝有三把[機槍](../Page/機槍.md "wikilink")，一把裝在旋轉炮塔上，另外兩把裝在車頭近駕駛員的位置。

當時[荷屬東印度群島](../Page/荷屬東印度群島.md "wikilink")（現為[印尼](../Page/印尼.md "wikilink")）的美軍亦有用於對抗[日軍](../Page/日軍.md "wikilink")。1942年中期由[荷蘭批量生產並運往](../Page/荷蘭.md "wikilink")[澳大利亞作訓練用途](../Page/澳大利亞.md "wikilink")。在[珍珠港事件後](../Page/珍珠港事件.md "wikilink")，駐守[珍珠港的瑪蒙](../Page/珍珠港.md "wikilink")-哈寧頓CTLS被[美國海軍陸戰隊用於](../Page/美國海軍陸戰隊.md "wikilink")[阿拉斯加州北部](../Page/阿拉斯加州.md "wikilink")，名為**T14**及**T15**。

## 資料來源

<div class="references-small">

<references />

  - Leland Ness(2002)*Janes World War II Tanks and Fighting Vehicles：A
    Complete Guide*，Harper Collins，ISBN 0-00-711228-9

  - \-[overvalwagen.com-瑪蒙-哈寧頓坦克系列](https://web.archive.org/web/20060414173105/http://www.overvalwagen.com/tanks.html)

  - \-[瑪蒙-哈寧頓軍用車輛](https://web.archive.org/web/20031122193252/http://www.geocities.com/marmonherrington/tank.html)

</div>

[Category:輕型坦克](../Category/輕型坦克.md "wikilink")
[Category:美國二戰坦克](../Category/美國二戰坦克.md "wikilink")
[Category:美國海軍陸戰隊裝備](../Category/美國海軍陸戰隊裝備.md "wikilink")