**瑪麗蓮·沃斯·莎凡特**（**Marilyn vos
Savant**，）曾經被記載為[吉尼斯世界記錄所認定擁有最高](../Page/吉尼斯世界記錄.md "wikilink")[智商的](../Page/智商.md "wikilink")[人類及](../Page/人類.md "wikilink")[女性](../Page/女性.md "wikilink")
(1984 to
1989)。她於1946年出生於[美國](../Page/美國.md "wikilink")[密苏里州的](../Page/密苏里州.md "wikilink")[圣路易斯](../Page/圣路易斯_\(密苏里州\).md "wikilink")，瑪麗蓮在剛滿10歲的1956年9月時初次接受[史丹福-比奈智力測驗](../Page/史丹福-比奈智力測驗.md "wikilink")
（心智年齡比例智商），測得智商高達228，並登上世界紀錄。然而，[智商的判定與比較方式後來遭到爭議](../Page/智商.md "wikilink")，
隨後[吉尼斯世界記錄在](../Page/吉尼斯世界記錄.md "wikilink")1990年移除了＂智商最高的人＂這個項目。

1985年35歲的瑪麗蓮參加了Hoeflin's Mega
Test的成人標準差智力測驗，48題中，她回答正確達46題，標準偏差值為16，智商為186。

瑪麗蓮現在從事[文學創作](../Page/文學.md "wikilink")，也編寫[劇本](../Page/劇本.md "wikilink")，並長期在《Parade》雜誌開闢名為〈Ask
Marilyn〉的專欄，專門回覆讀者各式各樣的問題，從[數學到](../Page/數學.md "wikilink")[人生都有](../Page/人生.md "wikilink")，她在雜誌上正確回答了[蒙提霍爾問題](../Page/蒙提霍爾問題.md "wikilink")。瑪麗蓮也是[門薩國際](../Page/門薩國際.md "wikilink")、[Mega
Society及](../Page/Mega_Society.md "wikilink")[普羅米修斯社團](../Page/普羅米修斯社團.md "wikilink")（Prometheus
Society）等高智商組織的成員之一。

## 參見

  - [蒙提霍爾問題](../Page/蒙提霍爾問題.md "wikilink")

## 外部連結

  -
  - [《Parade》雜誌官網](http://www.parade.com/index.jsp)，內有〈Ask Marilyn〉專欄

[S](../Category/美国作家.md "wikilink") [S](../Category/密苏里州人.md "wikilink")
[S](../Category/聖路易斯華盛頓大學校友.md "wikilink")
[Category:智力](../Category/智力.md "wikilink")
[Category:金氏世界紀錄](../Category/金氏世界紀錄.md "wikilink")
[M](../Category/門薩會員.md "wikilink")