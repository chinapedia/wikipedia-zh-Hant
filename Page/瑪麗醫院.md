[High_West_6.jpg](https://zh.wikipedia.org/wiki/File:High_West_6.jpg "fig:High_West_6.jpg")\]\]
**瑪麗醫院**（）是一所位於[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區_\(香港\).md "wikilink")[薄扶林](../Page/薄扶林.md "wikilink")\[1\]的大型公立地區綜合醫院，並提供24小時急症服務，也是香港兩所教學醫院之一，為[香港大學醫學院教學醫院](../Page/香港大學醫學院.md "wikilink")。該院於於1937年4月13日啟用，是當時香港以至[遠東地區規模最大的](../Page/遠東.md "wikilink")[醫院之一](../Page/醫院.md "wikilink")。

瑪麗醫院由[醫院管理局管理](../Page/醫院管理局.md "wikilink")，隸屬[港島西聯網](../Page/港島西聯網.md "wikilink")，為聯網中的龍頭醫院及專科轉介中心。全醫院共有約1,440張床位及逾3,800名職員。該院設有24小時[急症室服務](../Page/急症室.md "wikilink")、[外科](../Page/外科.md "wikilink")、[器官移植](../Page/器官移植.md "wikilink")、[兒科](../Page/兒科.md "wikilink")、各類專科及復康護理服務。

## 歷史

1925年，[港督](../Page/香港總督.md "wikilink")[金文泰上任](../Page/金文泰.md "wikilink")，致力改善香港的公營醫療服務。1929年9月，位於[何文田](../Page/何文田.md "wikilink")[亞皆老街的公立醫院](../Page/亞皆老街.md "wikilink")[九龍醫院開始啟用](../Page/九龍醫院.md "wikilink")，金文泰亦於同年宣佈於[香港島興建一所規模更大的醫院](../Page/香港島.md "wikilink")，以代替興建了超過半世紀、位於[西營盤的](../Page/西營盤.md "wikilink")[公立醫院](../Page/西營盤賽馬會分科診所.md "wikilink")。新醫院選址[薄扶林道與](../Page/薄扶林道.md "wikilink")[沙宣道對上的](../Page/沙宣道.md "wikilink")[西高山山腰](../Page/西高山.md "wikilink")，面對[西博寮海峽](../Page/西博寮海峽.md "wikilink")，環境清幽景色怡人。醫院於1933年正式開始興建，因資金不足，延至1937年4月13日正式落成啟用，由繼任港督[郝德傑主持開幕儀式](../Page/郝德傑.md "wikilink")，並以當時英皇[佐治五世的配偶](../Page/喬治五世_\(英國\).md "wikilink")[瑪麗皇后命名為瑪麗醫院](../Page/瑪麗_\(德克\).md "wikilink")。

瑪麗醫院落成時為遠東地區規模最大的醫院，H型的7層主建築物共分為6個翼，啟用同年起亦成為香港大學的醫科生臨床實習及教授的地方。[香港日佔時期](../Page/香港日佔時期.md "wikilink")，醫院曾被[日軍佔領和徵用作為軍用醫院](../Page/日軍.md "wikilink")。1945年10月1日，即香港重光後一個多月，瑪麗醫院便開始重新投入運作。由1955年開始，醫院經歷多次的擴建，其中以1960年代及1980年代的規模最大。1959年大學病理大樓落成，初時樓高3層，及後加至5層；1974年臨床病理大樓落成\[2\]；1980年落成的K座高達137米（28層），曾是全亞洲最高和全球第二高的醫院建築物，直到養和醫院李樹培院擴建工程完工才被超越；但目前仍是全球第四高醫院建築物。截至2005年，瑪麗醫院已經擁有共14座建築物。

醫院不同部分經過多次拆卸改建，建院初期的建築物至今只剩下主樓及護士宿舍A座。主樓樓高八層，以[新古典主義風格為設計](../Page/新古典主義.md "wikilink")。護士宿舍A座樓高六層，亦是以新古典主義風格為設計，以鋼構架建築，建築物的材料是在水泥外鋪上「批盪」，效果看起來像是用石砌出來的；內部的木樓梯、木門、及地板仍在使用。瑪麗醫院於2007年向政府申請拆卸護士宿舍A座，以興建急症、創傷暨心臟服務中心，但[建築署評估後認為有極高歷史文物價值](../Page/建築署.md "wikilink")，不可清拆，當局須另覓地點興建大樓。

2011年年初，全港首間移植及免疫遺傳學部化驗室於瑪麗醫院開幕。\[3\]3月11日，瑪麗醫院公布取得為期4年的[澳洲醫療服務標準委員會](../Page/澳洲醫療服務標準委員會.md "wikilink")（ACHS）評核認證。

[File:qmh1947.jpg|1947年的瑪麗醫院](File:qmh1947.jpg%7C1947年的瑪麗醫院)
<File:Queen> Mary Hospital 2.jpg|2008年的瑪麗醫院 <File:HK>
QueenMaryHospital.JPG|瑪麗醫院主樓 <File:Queen> mary nurse
quarter.jpg|瑪麗醫院護士宿舍正門

### 重建計劃

瑪麗醫院大部分[建築物落成至今已經有相當長的時間](../Page/建築物.md "wikilink")，，需要考慮重建瑪麗醫院或額外撥地興建教學醫院。[醫院管理局已經就重建瑪麗醫院的建議提交予政府考慮](../Page/醫院管理局.md "wikilink")，政府原則上接納了重建計劃。有待醫院管理局進行前期策劃工作，以確定工程計劃的技術可行性。分階段將瑪麗醫院更新為一所現代化的醫療科學中心，以應付社會對臨床服務和教學日益增加的需求。

在[2012年至2013年度香港政府財政預算案](../Page/2012年至2013年度香港政府財政預算案.md "wikilink")，政府宣佈會在2012年年內展開瑪麗醫院及[廣華醫院的重建計劃](../Page/廣華醫院.md "wikilink")，以提升瑪麗醫院[急症室和](../Page/急症室.md "wikilink")[心臟科的服務](../Page/心臟科.md "wikilink")\[4\]\[5\]\[6\]。瑪麗醫院將會分4期，合共約10年完成重建，預計須要68億港元\[7\]\[8\]2014年，需要進行首階段的工程時，會向立法會申請撥款。首階段工程包括拆卸醫療輔助倉庫，以及擴闊道路，預計心臟科及癌症中心等將於2016年竣工；第二及第三階段的工程，分別是加強急症室、[手術室及心臟科的服務](../Page/手術室.md "wikilink")\[9\]\[10\]\[11\]\[12\]。

2012年，瑪麗醫院向職員發放最新重建資訊，提及原來重建方案在醫院南面，包括拆卸教授樓、新教授樓、癌症中心、行政樓、護士學校、護士宿舍B座及具有爭議性的護士宿舍A座；然而，此方案未能夠根治多項局限瑪麗醫院未來發展的問題。醫院管理局和港島西聯網的規劃小組其後探討發展醫院北面的概念設計，即拆卸臨床病理大樓和醫生宿舍，把重建核心場向北移動，醫院管理局總部和[建築署目前正在評估最新概念的可行性](../Page/建築署.md "wikilink")。根據最新建議，為將瑪麗醫院的重建重心向北移動，拆卸兩座病理大樓和醫生宿舍共3座樓，不但可以使重建工程加快40個月完成、擴大樓面面積，更可以保留具有76年歷史、屬於二級歷史建築的護士宿舍A座；此外，再將兩座病理大樓附近的K座擴建為超級K座，比起現時的K座樓面而積多逾一倍，令到所有手術室可以同處一層，而整座急症室則可以設立於地面，不用再分散；另外，亦考慮於[薄扶林道近](../Page/薄扶林道.md "wikilink")[西環方向多興建一條道路](../Page/西環.md "wikilink")\[13\]。就此，醫院管理局發出了標書物色[顧問進行](../Page/顧問.md "wikilink")[評估](../Page/評估.md "wikilink")，包括例如拆卸病理大樓、病理大樓內的[化驗室和](../Page/化驗室.md "wikilink")[殮房等](../Page/殮房.md "wikilink")，可以在哪裡再安置，以及化驗室服務如何不會受到影響\[14\]。

瑪麗醫院第一期重建工程延至2017年才展開，工程包括拆卸兩座病理大樓及興建一座新醫院大樓，預算2023年完成重建。重建期間的病理工作暫時搬到南面的空置職員宿舍\[15\]。

重建後的瑪麗醫院將會加入24小時運作的緊急樓層，為大型急症及嚴重病症治療區，當中包括[急症室](../Page/急症室.md "wikilink")、[深切治療部等](../Page/深切治療部.md "wikilink")，主要集合心臟、心肺移植、心胸和神經外科治療專科，用以即時處理嚴重病症。此舉可以大幅度地減省運送病人及施行手術程序的時間\[16\]\[17\]。

## 服務範圍

  - 24小時[急症室服務](../Page/急症室.md "wikilink")
  - [一級創傷中心](../Page/一級創傷中心.md "wikilink")
  - [麻醉科](../Page/麻醉科.md "wikilink")
  - [心臟暨胸肺麻醉科](../Page/心臟暨胸肺麻醉科.md "wikilink")
  - [心臟暨胸肺外科](../Page/心臟暨胸肺外科.md "wikilink")
  - [腫瘤科](../Page/腫瘤科.md "wikilink")
  - [耳鼻喉科](../Page/耳鼻喉科.md "wikilink")
  - [內科](../Page/內科.md "wikilink")
  - [微生物科](../Page/微生物科.md "wikilink")
  - [腦外科](../Page/腦外科.md "wikilink")
  - [婦產科](../Page/婦產科.md "wikilink")
  - [口腔頷面外科](../Page/口腔頷面外科.md "wikilink")
  - [牙科](../Page/牙科.md "wikilink")
  - [眼科](../Page/眼科.md "wikilink")
  - [矯形及創傷外科](../Page/矯形及創傷外科.md "wikilink")
  - [心臟科](../Page/心臟科.md "wikilink")
  - [兒童及青少年科](../Page/兒童及青少年科.md "wikilink")
  - [病理及臨床生化學科](../Page/病理及臨床生化學科.md "wikilink")
  - [精神科](../Page/精神科.md "wikilink")
  - [放射科](../Page/放射科.md "wikilink")
  - [外科](../Page/外科.md "wikilink")

## 出生名人

  - [梁振英](../Page/梁振英.md "wikilink")，前[香港特首](../Page/香港特首.md "wikilink")，1954年8月12日\[18\]
  - [黃永光](../Page/黃永光.md "wikilink")，[北京市](../Page/北京市.md "wikilink")[政协委员](../Page/政协委员.md "wikilink")、[信和集团执行董事](../Page/信和集团.md "wikilink")，1978年\[19\]

## 逝世名人

  - [謝雨川](../Page/謝雨川.md "wikilink")，商家及立法局議員，1976年5月18日（去世，下同），78歲\[20\]
  - [陳百強](../Page/陳百強.md "wikilink")，歌手，1993年10月25日，35歲\[21\]
  - [胡振中](../Page/胡振中.md "wikilink")，[天主教香港教区](../Page/天主教香港教区.md "wikilink")[主教](../Page/主教.md "wikilink")，2002年9月23日，77歲\[22\]
  - [羅文](../Page/羅文.md "wikilink")，歌手，2002年10月18日，57歲\[23\]
  - [張國榮](../Page/張國榮.md "wikilink")，歌手及演員，2003年4月1日，46歲\[24\]
  - [林振強](../Page/林振強.md "wikilink")，作家和作曲家，2003年11月16日，54岁
  - [林百欣](../Page/林百欣.md "wikilink")，企業家，2005年2月18日，90歲\[25\]
  - [徐四民](../Page/徐四民.md "wikilink")，前中国[政协常委](../Page/政协常委.md "wikilink")，2007年9月9日，93歲\[26\]
  - [沈殿霞](../Page/沈殿霞.md "wikilink")，演員，2008年2月19日，62歲\[27\]
  - [陳棣榮](../Page/陳棣榮.md "wikilink")，前皇家香港輔助警察隊總監，2010年5月31日，76歲
  - [喬治·圖普五世](../Page/喬治·圖普五世.md "wikilink")，[汤加國王](../Page/東加國王.md "wikilink")，2012年3月18日，63歲\[28\]
  - [羅明珠](../Page/羅明珠.md "wikilink")，演員，「[開心少女組](../Page/開心少女組.md "wikilink")」成員，2016年5月27日，47歲\[29\]
  - [李君夏](../Page/李君夏.md "wikilink")，[香港警队第一位华人处长](../Page/香港警队.md "wikilink")，2017年1月23日，80歲\[30\]
  - [葉繼歡](../Page/葉繼歡.md "wikilink")，香港[賊王](../Page/賊王.md "wikilink")，2017年4月19日，55歲\[31\]
  - [林燕妮](../Page/林燕妮.md "wikilink")，有「香江才女」、「香江淑女」之稱，2018年5月31日，75歲\[32\]\[33\]\[34\]\[35\]\[36\]（另外，有報道指她於[養和醫院逝世](../Page/養和醫院.md "wikilink")\[37\]）
  - [王敏剛](../Page/王敏剛.md "wikilink")，[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink"),
    2019年3月11日， 70歲\[38\]

## 組織架構

  - 港島西醫院聯網總監和瑪麗醫院

<!-- end list -->

  - **行政總監**：
      - [李德麗](../Page/李德麗.md "wikilink") 醫生

<!-- end list -->

  - 醫院管治委員會

<!-- end list -->

  - **主席**：
      - 彭耀佳 先生
  - **委員**：
      - 康諾恩 博士
      - [梁卓偉](../Page/梁卓偉.md "wikilink") 教授
      - 陳捷貴 先生
      - 余嘯天 先生
  - **增選委員**：
      - 鄺永銓 先生
      - 李子建 教授
      - 勞建青 先生
      - 嚴嘉洵 女士

## 軼事

  - 該醫院是[電視劇](../Page/電視劇.md "wikilink")《[On Call
    36小時](../Page/On_Call_36小時.md "wikilink")》的主要拍攝場地。
  - 1989年8月4日，[葉繼歡在赤柱監獄服刑期間訛稱腹痛](../Page/葉繼歡.md "wikilink")，被押往瑪麗醫院檢查，其間葉突然表示內急須去廁所。葉在廁所找來兩個玻璃瓶，打破其一作武器，之後雙手各執玻璃樽衝出指嚇兩名懲教人員，並衝出醫院大門。適逢一輛客貨車停在醫院門口，他強行登車挾持37歲客貨車司機及其6歲兒子，威迫司機開車載他離去，途中又逼令司機把衣服及波鞋脫下讓他換上，葉在黃竹坑下車轉乘巴士逃逸。然而在2017年4月19日凌晨1時2分，他因肺癌在瑪麗醫院羈留病房離世。
  - 1999年，該院一名醫生替病人進行切除瘜肉手術時用[手提電話通話](../Page/手提電話.md "wikilink")，而通話內容與手術無關。事主向醫管局公眾投訴委員會投訴，委員會認為該醫生行為失當，投訴成立。醫委會2001年展開聆訊，未能證實事件與病人[腸臟穿破後遺症有關係](../Page/腸臟.md "wikilink")，裁定該醫生沒有專業失德。公眾批評「醫醫相衛」。\[39\]\[40\]\[41\]
  - 2012年3月18日[湯加國王](../Page/湯加.md "wikilink")[喬治·圖普五世在此醫院駕崩](../Page/喬治·圖普五世.md "wikilink")\[42\]\[43\]。

## 交通

<div class="NavFrame collapsed" style="background-color: Yellow; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: Yellow; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background-color: Yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{南港島綫西段色彩}}">█</font>[南港島綫（西段）](../Page/南港島綫（西段）.md "wikilink")：[瑪麗醫院站](../Page/瑪麗醫院站.md "wikilink")（計劃中）

<!-- end list -->

  - 瑪麗醫院專線小巴總站

<!-- end list -->

  - [薄扶林道](../Page/薄扶林道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - [香港仔至](../Page/香港仔.md "wikilink")[中環線](../Page/中環.md "wikilink")\[44\]
  - 香港仔至[西環線](../Page/西環.md "wikilink")\[45\]
  - 香港仔至[旺角線](../Page/旺角.md "wikilink") (24小時服務)\[46\]
  - 香港仔至[觀塘線](../Page/觀塘.md "wikilink") (平日下午時段服務)\[47\]

<!-- end list -->

  - [沙宣道](../Page/沙宣道.md "wikilink")

</div>

</div>

## 參考資料

## 外部連結

  - [瑪麗醫院](http://www3.ha.org.hk/qmh/index.htm)
  - [醫管局：瑪麗醫院](http://www.ha.org.hk/hesd/nsapi/?MIval=ha_view_content&c_id=100131)
  - [瑪麗醫院重建工程第一期](http://www32.ha.org.hk/capitalworksprojects/tc/Project/10years/Queen-Mary-Hospital-Phase1/Special-Features/Heritage-Protection.html)

{{-}}

[Category:薄扶林](../Category/薄扶林.md "wikilink") [Category:南區醫院
(香港)](../Category/南區醫院_\(香港\).md "wikilink")
[Category:香港教學醫院](../Category/香港教學醫院.md "wikilink")
[Category:香港大學](../Category/香港大學.md "wikilink")
[Category:亞洲之最](../Category/亞洲之最.md "wikilink")
[Category:1937年完工建築物](../Category/1937年完工建築物.md "wikilink")
[Category:以英國王室成員命名的事物](../Category/以英國王室成員命名的事物.md "wikilink")

1.

2.

3.  [全港首間移植及免疫遺傳學部化驗室開幕](http://hk.news.yahoo.com/article/110128/18/mga7.html)

4.  [來年重建瑪麗及廣華醫院](http://hk.news.yahoo.com/來年重建瑪麗及廣華醫院-033800662.html)

5.  [預算案:來年重建瑪麗及廣華](http://hk.news.yahoo.com/預算案-來年重建瑪麗及廣華-035300380.html)
    《星島日報》 2012年2月1日

6.  [預算案提出撥款重建瑪麗及廣華醫院](http://hk.news.yahoo.com/預算案提出撥款重建瑪麗及廣華醫院-055000113.html)

7.  [醫管局歡迎瑪麗和廣華重建工程開展](http://hk.news.yahoo.com/醫管局歡迎瑪麗和廣華重建工程開展-054300496.html)

8.  [胡定旭：重建廣華及瑪麗需150億](http://hk.news.yahoo.com/胡定旭-重建廣華及瑪麗需150億-055100617.html)

9.  [胡定旭感謝政府重建瑪麗和廣華醫院確保服務不受影響](http://hk.news.yahoo.com/胡定旭感謝政府重建瑪麗和廣華醫院確保服務不受影響-060400599.html)

10. [廣華及瑪麗醫院重建涉154億元](http://hk.news.yahoo.com/廣華及瑪麗醫院重建涉154億元-064300949.html)

11. [瑪麗醫院重建需時約10年](http://hk.news.yahoo.com/瑪麗醫院重建需時約10年-074800288.html)

12. [盧寵茂指重建瑪麗期待已久冀可提升醫院效率](http://hk.news.yahoo.com/盧寵茂指重建瑪麗期待已久冀可提升醫院效率-104000823.html)

13. [瑪麗重建研北移
    擬擴建超級K座](http://hk.news.yahoo.com/瑪麗重建研北移-擬擴建超級k座-211746250.html)《明報》
    2012年10月9日

14.

15.

16.

17.

18.

19.

20.

21.
22.

23.

24.

25.

26.

27.
28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38. <https://hk.news.appledaily.com/local/realtime/article/20190311/59358194>

39.

40.

41.

42.

43.

44. [香港仔東勝道　—　中環交易廣場](http://www.16seats.net/chi/rmb/r_h71.html)

45. [香港仔漁暉道　—　石塘咀創業商場](http://www.16seats.net/chi/rmb/r_h43.html)

46. [香港仔東勝道　—　旺角金雞廣場](http://www.16seats.net/chi/rmb/r_kh70.html)

47. [香港仔湖北街　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)