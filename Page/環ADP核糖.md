**環ADP核糖**（**Cyclic ADP
Ribose**；**cADPR**）是一種環狀[腺嘌呤](../Page/腺嘌呤.md "wikilink")[核苷酸](../Page/核苷酸.md "wikilink")，含有兩個[磷酸基團](../Page/磷酸根.md "wikilink")、兩個[核糖](../Page/核糖.md "wikilink")，以及一個[腺嘌呤](../Page/腺嘌呤.md "wikilink")[鹼基](../Page/鹼基.md "wikilink")。是一種[鈣信號](../Page/鈣信號.md "wikilink")（calcium
signaling）的信使。

## 參見

  - [NAADP](../Page/NAADP.md "wikilink")
  - [IP3](../Page/肌醇三磷酸.md "wikilink")

## 外部連結

  - <http://www.tc.umn.edu/~leehc/>

[Category:核苷酸](../Category/核苷酸.md "wikilink")