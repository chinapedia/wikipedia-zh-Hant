[HK_AsiaWorld_-Expo_Hall_10.jpg](https://zh.wikipedia.org/wiki/File:HK_AsiaWorld_-Expo_Hall_10.jpg "fig:HK_AsiaWorld_-Expo_Hall_10.jpg")及[大中華](../Page/大中華.md "wikilink")[上海等地舉行](../Page/上海.md "wikilink")，圖中[亞洲國際博覽館是其一](../Page/亞洲國際博覽館.md "wikilink")。\]\]
 **環球資源**，**Global
Sources**，是[美國](../Page/美國.md "wikilink")[納斯達克](../Page/納斯達克.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")，(NASDAQ:
GSOL)，獲[福布斯](../Page/福布斯.md "wikilink")(Forbes)
[雜誌評選為](../Page/雜誌.md "wikilink")[亞洲區首](../Page/亞洲.md "wikilink")200家[企業之一](../Page/企業.md "wikilink")。
環球資源的主要業務是[國際](../Page/國際.md "wikilink")[貿易](../Page/貿易.md "wikilink")[展覽營辦商](../Page/展覽.md "wikilink")、[電子商務](../Page/電子商務.md "wikilink")[B2B及商貿](../Page/B2B.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")[出版社](../Page/出版社.md "wikilink")。

環球資源服務的[顧客多數是](../Page/顧客.md "wikilink")[國際貿易的商人](../Page/國際貿易.md "wikilink")，他們的產品分類包括:
[汽摩](../Page/車輛.md "wikilink")[配件及用品](../Page/配件.md "wikilink")、[嬰兒及](../Page/嬰兒.md "wikilink")[兒童用品](../Page/兒童.md "wikilink")、[電腦](../Page/電腦.md "wikilink")、消費類[電子產品](../Page/電子.md "wikilink")、[電子零件](../Page/電子零件.md "wikilink")、[服裝及](../Page/服裝.md "wikilink")[紡織品](../Page/紡織品.md "wikilink")、[流行服飾及配件](../Page/時尚.md "wikilink")、[禮品及](../Page/禮物.md "wikilink")[贈品](../Page/贈品.md "wikilink")、[家居用品](../Page/日用品.md "wikilink")、[五金及DIY產品](../Page/五金.md "wikilink")、安防產品、休閒及[運動用品](../Page/運動.md "wikilink")、[通訊設施等](../Page/通訊.md "wikilink")。最初由其猶太人創辦人韓禮士
(Merle A.
Hinrichs)在香港創立，後進入臺灣市場，規模一度超過5000員工。2000年前正式投資進入中國内地市場，並開始專營為經營網絡B2B，漸漸減少貿易雜誌份額。

## 外部參考

  - [環球資源企業網](http://corporate.china.globalsources.com/)
  - [環球資源的主要高層管理人及簡介](http://corporate.china.globalsources.com/CAREERS/MEMBER.HTM)
  - [環球資源系列采購交易會](http://www.chinasourcingfair.com)
  - [環球資源在納斯達克上市的報價及公司資料](http://www.marketwatch.com/quotes/gsol)

[category:中国民营企业](../Page/category:中国民营企业.md "wikilink")

[Category:中国网站](../Category/中国网站.md "wikilink")