**瓊·愛倫**（，）是[美國](../Page/美國.md "wikilink")[女演員](../Page/女演員.md "wikilink")。曾獲三次[奧斯卡提名](../Page/奧斯卡.md "wikilink")，代表作包括《[白宮風暴](../Page/白宮風暴.md "wikilink")》、《[神鬼認證：神鬼疑雲](../Page/神鬼認證：神鬼疑雲.md "wikilink")》、《[神鬼認證：最後通牒](../Page/神鬼認證：最後通牒.md "wikilink")》、《[愛你愛到快抓狂](../Page/愛你愛到快抓狂.md "wikilink")》以及《[暗潮洶湧](../Page/暗潮洶湧.md "wikilink")》。

## 作品

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>名稱</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1985</p></td>
<td><p>Compromising Positions</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td><p><a href="../Page/1987大懸案.md" title="wikilink">1987大懸案</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Zeisters</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佩姬蘇要出嫁.md" title="wikilink">佩姬蘇要出嫁</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1988</p></td>
<td><p><a href="../Page/塔克：其人其夢.md" title="wikilink">塔克：其人其夢</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1989</p></td>
<td><p><a href="../Page/冷暖天涯.md" title="wikilink">冷暖天涯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p>Ethan Frome</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天生小棋王.md" title="wikilink">天生小棋王</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Josh and S.A.M.</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
<td><p><a href="../Page/愛你情深.md" title="wikilink">愛你情深</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白宮風暴.md" title="wikilink">白宮風暴</a></p></td>
<td></td>
<td><p>提名<a href="../Page/奧斯卡最佳女配角獎.md" title="wikilink">奧斯卡最佳女配角獎</a><br />
提名<a href="../Page/英國電影學院獎最佳女配角.md" title="wikilink">英國電影學院獎最佳女配角</a><br />
提名<a href="../Page/美國演員工會獎最佳女主角.md" title="wikilink">美國演員工會獎最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/激情年代.md" title="wikilink">激情年代</a></p></td>
<td></td>
<td><p>提名<a href="../Page/奧斯卡最佳女配角獎.md" title="wikilink">奧斯卡最佳女配角獎</a><br />
提名<a href="../Page/金球獎最佳電影女配角.md" title="wikilink">金球獎最佳電影女配角</a></p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><a href="../Page/冰風暴.md" title="wikilink">冰風暴</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/變臉_(1997年電影).md" title="wikilink">變臉</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p><a href="../Page/一人有一點顏色.md" title="wikilink">歡樂谷</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p>It's All the Rage</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p>When the Sky Falls</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/暗潮洶湧.md" title="wikilink">暗潮洶湧</a></p></td>
<td></td>
<td><p>提名<a href="../Page/奧斯卡最佳女主角獎.md" title="wikilink">奧斯卡最佳女主角獎</a><br />
提名<a href="../Page/金球獎最佳戲劇類電影女主角.md" title="wikilink">金球獎最佳戲劇類電影女主角</a><br />
提名<a href="../Page/美國演員工會獎最佳女主角.md" title="wikilink">美國演員工會獎最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/亞法隆之謎.md" title="wikilink">亞法隆之謎</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p>Off the Map</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/手札情緣.md" title="wikilink">手札情緣</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神鬼認證：神鬼疑雲.md" title="wikilink">神鬼認證：神鬼疑雲</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Yes</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/愛你把幾火.md" title="wikilink">愛你把幾火</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p>Bonneville</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Good Sharma</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/神鬼認證：最後通牒.md" title="wikilink">神鬼認證：最後通牒</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/絕命尬車.md" title="wikilink">絕命尬車</a></p></td>
<td><p>韓娜席</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/忠犬小八.md" title="wikilink">忠犬小八</a></p></td>
<td><p>Cate Wilson</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/神鬼認證4.md" title="wikilink">神鬼認證4</a></p></td>
<td><p>潘蜜拉·蘭迪</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/房間_(電影).md" title="wikilink">不存在的房間</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 連結

  -
  -
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:伊利諾州人](../Category/伊利諾州人.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")