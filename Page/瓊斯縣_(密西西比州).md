**瓊斯县**（）是[美國](../Page/美國.md "wikilink")[密西西比州南部的一個縣](../Page/密西西比州.md "wikilink")。面積1,812平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口64,958人。縣治[勞雷爾](../Page/勞雷爾_\(密西西比州\).md "wikilink")
（Laurel）和[埃利斯維爾](../Page/埃利斯維爾_\(密西西比州\).md "wikilink") （Ellisville）。

瓊斯縣成立於1826年1月24日（1865至1869年之間易名為戴維斯縣）。縣名是紀念在[美國獨立戰爭期間任艦長](../Page/美國獨立戰爭.md "wikilink")，後來在[俄羅斯海軍任上將的](../Page/俄羅斯海軍.md "wikilink")[約翰·保羅·瓊斯](../Page/約翰·保羅·瓊斯.md "wikilink")\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[J](../Category/密西西比州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.