**危地马拉城**（）是[瓜地馬拉的首都和第一大城市](../Page/瓜地馬拉.md "wikilink")、[瓜地馬拉省首府](../Page/瓜地馬拉省.md "wikilink")，同時也是[中美洲第一大城市](../Page/中美洲.md "wikilink")。人口根據不同的估算方式，在200萬至350萬人之間。位於瓜地馬拉中部的山谷中。全市由工程師Raúl
Aguilar
Batres做妥善的都市規劃，共分為22個區，每區皆有直向與橫向的街道。分區編號從第一區編至第二十五區，第20、22、23區並不存在。

## 歷史

瓜地馬拉市的地點在2000年以前，就有古[馬雅的大城市Kaminaljuyu](../Page/瑪雅文明.md "wikilink")，因此遺留了許多古代建築。[西班牙殖民時代早期](../Page/西班牙.md "wikilink")，瓜地馬拉市是一個小城鎮，1775年由於地震摧娙了原來的舊都城[安提瓜瓜地馬拉](../Page/安地瓜.md "wikilink")，使這裡成為[西屬中美洲的首府](../Page/新西班牙總督轄區.md "wikilink")，城市才日漸發展。

該市是瓜地馬拉的政治、經濟、文化中心。位於該市的[拉奧羅拉國際機場是全國出入最主要交通孔道](../Page/拉奧羅拉國際機場.md "wikilink")。市內共有八所大學以及許多博物館和畫廊。

## 地理

### 地形

瓜地馬拉市位於瓜地馬拉中部高地地區，屬多山地形，市內地形呈現明顯高低起伏，平均海拔約1,600公尺。整座城市為其他高山或火山環繞，[帕卡亞火山](../Page/帕卡亞火山.md "wikilink")、[水火山](../Page/水火山.md "wikilink")、[富埃戈火山等於市區內可見](../Page/富埃戈火山.md "wikilink")。

### 氣候

瓜地馬拉有「恆春之國」(País de siempre
primavera)的美譽，而瓜地馬拉市為該氣候之典型城市，全年平均氣溫約在20\~30度C之間。瓜地馬拉每年11月至4月為乾季，5月至10月為雨季，雨季時降雨量豐富，偶爾會釀災。此外，夏季期間偶爾會有颶風侵襲。

### 自然災害

#### 地震

瓜地馬拉市經常有地震發生，最近一次的災難性地震發生於1917/1918年、1976年以及2012年。

#### 熱帶風暴與颶風

瓜地馬拉市經常受此自然天氣現象所帶來的災害襲擊，其中1998年11月的密契(Mitch)颶風以及2005年的史坦(Stan)颶風，是瓜地馬拉市有紀錄以來，最嚴重的颶風災害。

2010年，瓜地馬拉市亦受亞加達(Agatha)颶風襲擊，其短時間內龐大的降雨量，加上排水系統崩潰，於瓜地馬拉市北部曾造成一巨大坑洞。\[1\]

#### 火山活動

瓜地馬拉市周邊可見四座火山，其中有兩座是活火山。最接近瓜地馬拉市，且最活躍的火山為[帕卡亞火山](../Page/帕卡亞火山.md "wikilink")，其有時會排出大量的火山灰。

2010年五月27日晚間七時十分，該火山曾經再次噴發，火山灰噴至1,500公尺高，影響地區包括瓜地馬拉市及周邊三個省份，落下火山灰雨，[拉奧羅拉國際機場也因此關閉](../Page/拉奧羅拉國際機場.md "wikilink")，五天後才恢復正常。瓜地馬拉國家災害防治協調中心(CONRED)於火山爆發時針對火山鄰近社區發布紅色警戒，並建議疏散。\[2\]此次火山噴發共造成2人死亡\[3\]、數人受傷。

#### 土石流

由於強降雨帶來的土石流，在瓜地馬拉雨季期間很常見，其中又以地勢脆弱且陡峭的山邊最易發生。2005年十月的史坦(Stan)颶風侵襲期間，即發生多起土石流案件。

## 瓜地馬拉市全景圖

## 旅遊與購物

### 第一區

  - 瓜地馬拉歷史中心\[4\]：範圍含括大部分的瓜地馬拉市第一區，其中有下列重要景點：
      - 憲法廣場
      - 瓜地馬拉國立博物館(文化宮) \[5\]
      - 第六街行人徒步區(Paseo de la Sexta Avenida) \[6\]
      - 瓜地馬拉大都會教堂
      - 瓜地馬拉中央市場(Mercado Central)\[7\]
      - 瓜地馬拉國家圖書館
  - 瓜地馬拉鐵道博物館(Museo del Ferrocarril)\[8\]
  - 瓜地馬拉中央銀行\[9\]
  - 和平紀念碑廣場與瓜地馬拉法院
  - 四月電影院(Teatro Abril) \[10\]
  - Miguel Ángel Asturias文化中心\[11\]：內有瓜地馬拉國家戲劇院、國家音樂廳、軍事博物館等。
  - Hogar Rafael Ayau Orthodox Orphanage
    [4](https://web.archive.org/web/20100828214540/http://www.hogarrafaelayau.org/)

### 第二區

  - 瓜地馬拉立體地圖(*Mapa en Relieve*)
  - Avenida Simeón Cañas：例假日封街之休閒大街。\[12\]

### 第三區

  - 大墓園(General Cemetery)

### 第四區

  - Cuatro Grados Norte：酒吧與餐廳林立的行人徒步文化區。
  - 外國人移民簽證辦公室
  - INGUAT瓜地馬拉觀光局

### 第五區

  - 火星運動公園(Campo Marte)：為瓜地馬拉市最大之運動公園，偶爾亦有比賽或活動於此地舉行。
  - Mateo Flores體育館
  - 植物園
  - 自然歷史博物館
  - Yurrita教堂

### 第七區

  - [馬雅遺跡](../Page/馬雅.md "wikilink")[Kaminaljuyu](../Page/Kaminaljuyu.md "wikilink")
  - Galerias Primma：為一內部設有雲霄飛車的購物商場。
  - Peri-Rossevert

### 第八區

  - El Guarda市場(Mercado El Guarda)：位於重要公車轉乘站El Trebol旁。

### 第九區

[Plaza_Fontabella_and_Edificio_Atlantis.JPG](https://zh.wikipedia.org/wiki/File:Plaza_Fontabella_and_Edificio_Atlantis.JPG "fig:Plaza_Fontabella_and_Edificio_Atlantis.JPG")

  - [Terminal市場](../Page/Terminal市場.md "wikilink")：瓜地馬拉市規模最大之傳統市場，亦為重要公車轉乘站。
  - [Reformador塔](../Page/Reformador塔.md "wikilink")(Torre del
    Reformador/Tower of the Reformer).
  - 工業廣場 [5](http://www.coperex.com.gt/)
  - 改革大道與[El
    Obelisco](../Page/Obelisco_\(Guatemala_City\).md "wikilink")：例假日封街之休閒大街。

### 第十區

  - Ixchel馬雅衣飾博物館(Museo Ixchel del Traje Indígena/Mayan dress museum)
  - Popol Vuh博物館
  - [蘇活區](../Page/蘇活區.md "wikilink")(Zona Viva)
  - Plaza Fontabella生活中心
  - Oakland Mall

### 第十一區

  - Miraflores博物館
    [6](https://web.archive.org/web/20080128160541/http://www.museomiraflores.com/),
    Miraflores為瓜地馬拉市與中美洲最大的購物中心之一。
  - Tikal Futura飯店：其特色建築為瓜地馬拉市現代化地標之一。

### 第十二區

  - [聖卡洛斯大學](../Page/聖卡洛斯大學.md "wikilink")
  - IRTRA遊樂園(Instituto de Recreación de los Trabajadores，勞工娛樂機構)\[13\]

### 第十三區

[Guatemala_City_Buildings.JPG](https://zh.wikipedia.org/wiki/File:Guatemala_City_Buildings.JPG "fig:Guatemala_City_Buildings.JPG")往外望去的瓜地馬拉市街景\]\]

  - 國立考古學暨民族學博物館：收藏瓜地馬拉市[馬雅遺跡](../Page/馬雅.md "wikilink")[Kaminaljuyu之部分文物](../Page/Kaminaljuyu.md "wikilink")。
  - 現代藝術博物館
  - 兒童博物館
  - 國立歷史博物館
  - [拉奧羅拉動物園](../Page/拉奧羅拉動物園.md "wikilink") \[14\]
  - [拉奧羅拉國際機場](../Page/拉奧羅拉國際機場.md "wikilink")
  - 美洲大道：例假日封街之休閒大街。
  - Domo Polideportivo Indoor Sports Stadium
  - [手工藝市場](../Page/手工藝市場.md "wikilink")(Mercado de Artesanías)
  - Reloj de Flores (Landsaped Floral Clock)
  - Antiguo Aqueducto (Remains of the Historical Aqueduct)
  - [Justo Rufino Barrios](../Page/Justo_Rufino_Barrios.md "wikilink")
    紀念雕像 (Monument of one of Guatemala's much acclaimed past
    President, responsible for the introduction of the railroads among
    other services to the country.)
  - [Tecún Úman紀念雕像](../Page/Tecún_Úman.md "wikilink") (Monument to a
    famed Mayan leader and warrior in Guatemala's History)
  - Velodromo Nacional (Nacional Velodrome)

### 第十六區

  - [卡亞拉徒步區](../Page/卡亞拉徒步區.md "wikilink")(Paseo
    Cayalá)：為一集團創設之購物商場、住宅區，內有餐廳、夜店等。\[15\]

### 第十七區

  - Potales

## 教育

### 大學

瓜地馬拉市境內共有14間大學，分別列表如下：

  - [聖卡洛斯大學](../Page/聖卡洛斯大學.md "wikilink")(Universidad de San
    Carlos/USAC)：創立於1676年，為瓜地馬拉全國唯一的國立大學，亦為全國歷史最悠久、美洲第三古老的大學。
  - [馬洛京大學](../Page/馬洛京大學.md "wikilink")(Universidad Francisco
    Marroquin)：創立於1971年，由瓜地馬拉工商界人士集資設立，是瓜國四大名校之一。位於第十區，為一學制相當完整之大學。另附設Museo
    Ixchel del Traje Indígena與Museo Popol Vuh兩間博物館。
  - [狄瓦耶大學](../Page/狄瓦耶大學.md "wikilink")(Universidad del
    Valle)：創立於1966年，位於第十五區，設有4系。
  - [藍狄瓦大學](../Page/藍狄瓦大學.md "wikilink")(Universidad Rafael
    Landivar)：創立於1961年，位於第十六區，設有9系(Facultad)與數間研究所。
  - [瑪麗亞洛加爾培斯大學](../Page/瑪麗亞洛加爾培斯大學.md "wikilink")(Universidad Mariano
    Gálvez)：創立於1966年，位於第二區，設有14系。
  - [伽利略大學](../Page/伽利略大學.md "wikilink")(Universidad
    Galileo)：創立於2000年，位於第十區，與[馬洛京大學](../Page/馬洛京大學.md "wikilink")(Universidad
    Francisco
    Marroquin)僻鄰，目前全校學生數約4萬人。該校唯一設備新穎之大學，目前擁有9系(Facultad)、6所(Institutos)與7校(Escuela)。
  - Universidad Salesiana Mesoamericana
  - Universidad Popular de Guatemala
  - Universidad Panamericana：創立於1998年，位於第十六區，設有5系。
  - Universidad San Pablo de Guatemala：創立於2006年，位於第十四區。
  - Universidad Mesoamericana
  - Universidad del Istmo：創立於1997年，位於第十三區，設有6系。
  - Universidad Rural：創立於1995年，位於第二區，設有4系。
  - Universidad Internaciones de Guatemala

部分大學有提供對外開放之課程，可至各校網站查詢。

### 中小學

瓜地馬拉市境內有多間中小學。此外，瓜地馬拉美國學校、馬雅國際學校、法國學校等亦坐落於瓜地馬拉市。

### 語言學校

  - 牛津語言中心(Oxford Language Center)
  - IGA - Instituto Guatemalteco Americano
  - Easy-Fácil
  - 歐洲學院(Academia Europea)
  - Berlitz
  - [聖卡洛斯大學附設語言學校](../Page/聖卡洛斯大學.md "wikilink")(USAC - University of
    San Carlos)
  - Instituto Austriaco Guatemalteco - Österreichische Schule

## 經濟

## 人口

## 交通

### 航空

[拉奧羅拉國際機場為瓜地馬拉市唯一且主要的對外機場](../Page/拉奧羅拉國際機場.md "wikilink")，亦為全國規模最大之機場，每日均有航班來往美國[洛杉磯](../Page/洛杉磯.md "wikilink")、[亞特蘭大](../Page/亞特蘭大.md "wikilink")、[邁阿密](../Page/邁阿密.md "wikilink")，以及[薩爾瓦多](../Page/薩爾瓦多.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[墨西哥等地](../Page/墨西哥.md "wikilink")。

### 公路

瓜地馬拉市為國內三條快速道路的主要聯絡點，分別為CA-1[泛美公路](../Page/泛美公路.md "wikilink")([墨西哥邊境經瓜地馬拉市至](../Page/墨西哥.md "wikilink")[薩爾瓦多邊境](../Page/薩爾瓦多.md "wikilink"))、CA-9跨洋公路(Puerto
San Jose至Puerto Santo Tomas de
Castilla)，以及前往[北碇省的公路](../Page/北碇省.md "wikilink")。

### 大眾運輸

瓜地馬拉市區主要以公車接駁，市區公車主要為[美國淘汰之校車改裝](../Page/美國.md "wikilink")，以[Terminal站](../Page/Terminal.md "wikilink")、Trebol站與Centra
Sur站為主要轉運站，可前往市區大部分地區，以及國內其他省分，車票單趟為1Q(假日為1.5Q，另部分路線為1.1Q)。此外，市區亦有[計程車](../Page/計程車.md "wikilink")，主要有Taxis
Amarillo(黃色計程車)、Taxis Verde(綠色計程車)、Taxis Mariscal、Taxis Las
Americas、Servitaxis Rotativos與Blanco y Azul(白色與藍色計程車)等公司營運，至少25Q起。

#### Transmetro

為擴大運能、提升大眾運輸安全度與可靠性，2007年起瓜地馬拉市於市中心闢建[BRT路線公車](../Page/BRT.md "wikilink")，稱作TransMetro。目前共有三條路線，分別為Eje
Sur(南線)、Eje Corredor Central(中央走廊線)，以及逢例假日行駛的Eje Centro
Histórico(歷史中心線)。

##### Eje sur(南線)

[TRANSMETRO.JPG](https://zh.wikipedia.org/wiki/File:TRANSMETRO.JPG "fig:TRANSMETRO.JPG")
Eje sur(南線)行駛區間為瓜地馬拉第一區 - Centra Sur，共有四種營運模式如下：

|         |                                  |
| ------- | -------------------------------- |
| **T70** | 正常營運模式(每站皆停)                     |
| **T71** | Trébol - Centra Sur (每站皆停)       |
| **D70** | Trébol直達車(僅停靠Centra Sur與Trébol)  |
| **D71** | Plaza El Amate直達車(僅停靠端點站與Trébol) |

以下為車站說明：

|         |                                                                         |                                                                                   |
| ------- | ----------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| **T70** | [Centra Sur](../Page/Centra_Sur.md "wikilink") (南中心站/Centra Sur站)       | 端點站，本站可站外轉乘至瓜地馬拉太平洋沿海城市。周邊有CENMA果菜市場與Centra Sur購物中心。                              |
| **D70** |                                                                         |                                                                                   |
| **D71** |                                                                         |                                                                                   |
| **T70** | Estación Monte María (瑪莉亞山站)                                            |                                                                                   |
| **T70** | Estación Javier (哈維耶站)                                                  |                                                                                   |
| **T70** | Estación Las Charcas (池塘站)                                              |                                                                                   |
| **T70** | Estación El Carmen (卡門站)                                                |                                                                                   |
| **T70** | Estación Reformita (Reformita站)                                         |                                                                                   |
| **T70** | Estación Mariscal (元帥站)                                                 |                                                                                   |
| **T70** | [Estación Trébol](../Page/Estación_Trébol.md "wikilink") (酢漿草站/Trébol站) | 重要車站，可站外轉乘至瓜地馬拉中西部各城市。                                                            |
| **D70** |                                                                         |                                                                                   |
| **T70** | Estación Santa Cecilia (聖塞西莉亞站)                                         |                                                                                   |
| **T70** | Estación Bolívar (玻利瓦爾站)                                                |                                                                                   |
| **T70** | Estación Don Bosco (Bosco先生站)                                           |                                                                                   |
| **T70** | Estación El Calvario (騎兵站，此為轉乘站)                                        | 可於站內轉乘Transmetro中央走廊線。                                                            |
| **TCC** |                                                                         |                                                                                   |
| **T70** | Estación Plaza Municipal (市府廣場站)                                        | 原可於假日期間站內轉乘Transmetro歷史中心線，自2013年年底起，改由Eje Corredor Central(中央走廊線)的Tipografía站轉乘。 |
| **T70** | Estación Plaza Barrios (巴里奧斯廣場站，此為轉乘站)                                  | 可於站內轉乘Transmetro中央走廊線。                                                            |
| **TCC** |                                                                         |                                                                                   |
| **T70** | Estación Plaza El Amate (Amate廣場站)                                      |                                                                                   |

##### Eje Corredor Central(中央走廊線)

[Transmetro001.JPG](https://zh.wikipedia.org/wiki/File:Transmetro001.JPG "fig:Transmetro001.JPG")

Eje Corredor Central(中央走廊線)行駛區間為瓜地馬拉第一區 -
Hangares，行經第一、四、九、十三區，是第二條開通的Transmetro路線。本路線與Eje
Sur(南線)在Plaza Barrios與El Calvario共站，可站內轉乘。本路線目前僅一種營運模式如下：

|         |               |
| ------- | ------------- |
| **TCC** | 正常營運模式 (每站皆停) |

以下為車站說明：

|         |                                              |                                                |
| ------- | -------------------------------------------- | ---------------------------------------------- |
| **TCC** | Estación Plaza Barrios (巴里奧斯廣場站，此為轉乘站)       | 可於站內轉乘Transmetro南線。                            |
| **T70** |                                              |                                                |
| **TCC** | Estación Tipografía (活版印刷站，此為轉乘站)            | 2013年年底通車新站，可於站內轉乘Eje Centro Histórico(歷史中心線)。 |
| **TCH** |                                              |                                                |
| **TCC** | Estación El Calvario (騎兵站，此為轉乘站)             | 可於站內轉乘Transmetro南線。                            |
| **T70** |                                              |                                                |
| **TCC** | Estación 4 Grados Sur (南四度站)                 |                                                |
| **TCC** | Estación Exposición (展覽站)                    |                                                |
| **TCC** | Estación Terminal (Terminal站)                | 重要車站，可於站外轉乘至瓜地馬拉南部與東部各城市與瓜地馬拉市其他地區。            |
| **TCC** | Estación Industria (工業站)                     | 鄰近工業廣場。                                        |
| **TCC** | Estación Tívoli (蒂沃利站)                       |                                                |
| **TCC** | Estación Montufár (Montufár站)                |                                                |
| **TCC** | Estación Acueducto (水道站)                     |                                                |
| **TCC** | Estación Fuerza Aérea (空軍站)                  |                                                |
| **TCC** | Estación Hangares (機棚站)                      | 端點站。                                           |
| **TCC** | Estación Plaza Argentina (阿根廷廣場站)            |                                                |
| **TCC** | Estación Los Arcos (拱門站)                     |                                                |
| **TCC** | Estación Plaza España (西班牙廣場站)               |                                                |
| **TCC** | Estación IGSS Zona 9 (第九區IGSS站)              |                                                |
| **TCC** | Estación Seis 26 (6-26站)                     |                                                |
| **TCC** | Estación Torre del Reformador (Reformador塔站) |                                                |
| **TCC** | Estación Plaza de la República (共和國廣場站)      |                                                |
| **TCC** | Estación Banco de Guatemala (瓜地馬拉中央銀行站)      | 附近有瓜地馬拉中央銀行、法院等。                               |

##### Eje Centro Histórico(歷史中心線)

[Transmetro_central_noche.jpg](https://zh.wikipedia.org/wiki/File:Transmetro_central_noche.jpg "fig:Transmetro_central_noche.jpg")

Eje Corredor Central(中央走廊線)完工後, Eje Centro Histórico(歷史中心線)旋即開始建造。\[16\]
本路線於2012年12月19日完工通車，行經瓜地馬拉第一區，\[17\]但目前仍有多站仍在施工中。本路線目前僅假日營運，營運模式如下：

|         |               |
| ------- | ------------- |
| **TCH** | 歷史中心路線 (各站皆停) |

以下為車站說明：

|         |                                           |                                                  |
| ------- | ----------------------------------------- | ------------------------------------------------ |
| **TCH** | Estación Tipografía (活版印刷站，此為轉乘站)         | 2013年年底通車新站，可於站內轉乘Eje Corredor Central(中央走廊線)。   |
| **TCC** |                                           |                                                  |
| **TCH** | Estación Gómez Carrillo (Gómez Carrillo站) | 2013年底前為臨時站，2013年由原Parque Gómez Carrillo站更名營運至今。 |
| **TCH** | Estación San Agustín (露天劇院站)              |                                                  |
| **TCH** | Estación Concha Acústica (露天劇院站)          |                                                  |
| **TCH** | Estación San Sebastián (聖塞巴斯蒂安站)          |                                                  |
| **TCH** | Estación Mercado Central (中央市場站)          |                                                  |
| **TCH** | Estación Beatas de Belén (伯利恆的祝福站)        |                                                  |
|         |                                           |                                                  |

##### 計畫中路線

其他尚有El eje norte(北線)等計畫路線，預計2020年完工通車。\[18\]

## 姊妹市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/馬德里.md" title="wikilink">馬德里</a>, <a href="../Page/西班牙.md" title="wikilink">西班牙</a></p></li>
<li><p><a href="../Page/拉巴斯.md" title="wikilink">拉巴斯</a>, <a href="../Page/波利維亞.md" title="wikilink">波利維亞</a></p></li>
<li><p><a href="../Page/馬納瓜.md" title="wikilink">馬納瓜</a>, <a href="../Page/尼加拉瓜.md" title="wikilink">尼加拉瓜</a></p></li>
<li><p><a href="../Page/聖胡安_(波多黎各).md" title="wikilink">聖胡安</a>, <a href="../Page/波多黎各.md" title="wikilink">波多黎各</a></p></li>
<li><p><a href="../Page/聖薩爾瓦多.md" title="wikilink">聖薩爾瓦多</a>, <a href="../Page/薩爾瓦多.md" title="wikilink">薩爾瓦多</a></p></li>
<li><p><a href="../Page/聖荷西.md" title="wikilink">聖荷西</a>，<a href="../Page/哥斯大黎加.md" title="wikilink">哥斯大黎加</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/聖克魯斯-德特內里費.md" title="wikilink">聖克魯斯-德特內里費</a>, <a href="../Page/西班牙.md" title="wikilink">西班牙</a></p></li>
<li><p><a href="../Page/卡法薩巴.md" title="wikilink">卡法薩巴</a>, <a href="../Page/以色列.md" title="wikilink">以色列</a></p></li>
<li><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a>, <a href="../Page/中華民國.md" title="wikilink">中華民國</a></p></li>
<li><p><a href="../Page/利馬.md" title="wikilink">利馬</a>, <a href="../Page/祕魯.md" title="wikilink">祕魯</a></p></li>
<li><p><a href="../Page/卡拉卡斯.md" title="wikilink">卡拉卡斯</a>，<a href="../Page/委內瑞拉.md" title="wikilink">委內瑞拉</a></p></li>
<li><p><a href="../Page/北京市.md" title="wikilink">北京市</a>，<a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></li>
<li><p><a href="../Page/華盛頓特區.md" title="wikilink">華盛頓特區</a>，<a href="../Page/美國.md" title="wikilink">美國</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/聖佩德羅蘇拉.md" title="wikilink">聖佩德羅蘇拉</a>(<a href="../Page/汕埠.md" title="wikilink">汕埠</a>), <a href="../Page/宏都拉斯.md" title="wikilink">宏都拉斯</a></p></li>
<li><p><a href="../Page/波哥大.md" title="wikilink">波哥大</a>, <a href="../Page/哥倫比亞.md" title="wikilink">哥倫比亞</a></p></li>
<li><p><a href="../Page/墨西哥市.md" title="wikilink">墨西哥市</a>, <a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></li>
<li><p><a href="../Page/薩爾蒂約.md" title="wikilink">薩爾蒂約</a>, <a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></li>
<li><p><a href="../Page/巴拿馬市.md" title="wikilink">巴拿馬市</a>, <a href="../Page/巴拿馬.md" title="wikilink">巴拿馬</a></p></li>
<li><p><a href="../Page/聖地牙哥.md" title="wikilink">聖地牙哥</a>，<a href="../Page/智利.md" title="wikilink">智利</a></p></li>
<li><p><a href="../Page/哈瓦那.md" title="wikilink">哈瓦那</a>，<a href="../Page/古巴.md" title="wikilink">古巴</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 出身名人

  - [Raúl Aguilar Batres](../Page/Raúl_Aguilar_Batres.md "wikilink"),
    工程師, 瓜地馬拉市街道系統規劃者
  - [Miguel Ángel
    Asturias](../Page/Miguel_Ángel_Asturias.md "wikilink"), 作家、詩人,
    諾貝爾文學獎得主
  - [Manuel Colom Argueta](../Page/Manuel_Colom_Argueta.md "wikilink"),
    曾任瓜地馬拉市市長
  - [Jorge de León](../Page/Jorge_de_León.md "wikilink"), 表演藝術家\[19\]
  - [Carlos Mérida](../Page/Carlos_Mérida.md "wikilink"), 畫家
  - [Carlos Peña](../Page/Carlos_Peña_\(singer\).md "wikilink"), 歌手,
    2007拉丁美洲偶像冠軍
  - [Fernando Quevedo](../Page/Fernando_Quevedo.md "wikilink"), 理論物理學家,
    現為劍橋大學教授
  - [Rodolfo Robles](../Page/Rodolfo_Robles.md "wikilink"), 醫師,
    discovered onchocercosis "Robles' Disease"
  - [Fabiola Rodas](../Page/Fabiola_Rodas.md "wikilink"), Winner of The
    Third TV Azteca's [Desafio de
    Estrellas](../Page/Desafio_de_Estrellas.md "wikilink") 2nd Place in
    The Last Generation of [La
    Academia](../Page/La_Academia.md "wikilink")
  - [Carlos Ruíz](../Page/Carlos_Ruiz_\(footballer\).md "wikilink"), 足球員
  - [Shery](../Page/Shery.md "wikilink"), 歌手、作詞作曲家
  - [Jaime Viñals](../Page/Jaime_Viñals.md "wikilink"), 登山者
    (擁有登遍世界七大高峰紀錄)
  - [Luis von Ahn](../Page/Luis_von_Ahn.md "wikilink"), 電腦科學家,
    [CAPTCHA創立者](../Page/CAPTCHA.md "wikilink")，亦為[卡內基梅隆大學副教授](../Page/卡內基梅隆大學.md "wikilink")
  - [Ted Hendricks](../Page/Ted_Hendricks.md "wikilink"), [Oakland
    Raiders](../Page/Oakland_Raiders.md "wikilink") NFL Hall Of Fame
    Linebacker. 5-Time Super Bowl Champion
  - [里卡多阿爾侯納](../Page/里卡多阿爾侯納.md "wikilink")(Ricardo Arjona), 歌手、作詞作曲家
  - [阿爾瓦羅·恩里克·阿爾蘇·伊里戈延](../Page/阿爾瓦羅·恩里克·阿爾蘇·伊里戈延.md "wikilink")，曾任瓜地馬拉總統
  - [奧斯卡·貝爾赫](../Page/奧斯卡·貝爾赫.md "wikilink")，曾任瓜地馬拉總統
  - [阿爾瓦羅·科洛姆](../Page/阿爾瓦羅·科洛姆.md "wikilink")，曾任瓜地馬拉總統
  - [奧托·佩雷斯·莫利納](../Page/奧托·佩雷斯·莫利納.md "wikilink")，現任瓜地馬拉總統

## 參考文獻

## 延伸閱讀

## 外部連結

[Category:危地马拉城市](../Category/危地马拉城市.md "wikilink")
[Category:北美洲首都](../Category/北美洲首都.md "wikilink")

1.  [Socavón causado por la tormenta
    Agatha](http://www.elpais.com/fotogaleria/tormenta/tropical/Agatha/arrasa/America/Central/elpgal/20100531elpepuint_2/Zes/1)
2.  [Llueve ceniza y piedras del Volcán de
    Pacaya](http://www.elperiodico.com.gt/es/20100527/pais/154149/)
3.  Diario de Navarra: [El volcán Pacay continúa activo y obliga a
    seguir con
    evacuaciones](http://www.diariodenavarra.es/20100530/internacional/el-volcan-pacay-continua-activo-obliga-seguir-evacuaciones.html?not=2010053001435867&idnot=2010053001435867&dia=20100530&seccion=internacional&seccion2=internacional&chnl=30)

4.  [瓜地馬拉歷史中心官方網站](http://www.centrohistorico.net/)
5.  [國立文化宮官方網站](http://www.museosdeguatemala.org/)
6.  [Paseo de la sexta官方網站](http://www.paseodelasexta.com.gt/)
7.  [1](http://www.mercadocentral.com.gt/)
8.  [瓜地馬拉鐵道博物館官網](http://www.museofegua.com/)
9.  [2](http://www.banguat.gob.gt/default.asp)
10. [3](http://www.teatroabril.com/)
11.
12. [Avenida Simeón
    Cañas簡介(西文)](http://cultura.muniguate.com/index.php/component/content/article/41-cantonjocotenango/131-avenidasimeon)
13. [IRTRA樂園網站首頁](http://www.irtra.org.gt/)
14.
15. [遠離犯罪 瓜國建世外桃源](http://www.cna.com.tw/News/aOPL/201302100030-1.aspx)
16.
17.
18.
19. Estey, Myles. ["A generation of young artists is gaining recognition
    for their gritty depictions of the modern realities of the Central
    American
    nation."](http://mobile.globalpost.com/dispatch/news/regions/americas/110811/art-guatemala-artists-central-america)
     *Global Post.* 15 Aug 2011. Retrieved 11 Feb 2012.