**瓜州县**，原名**安西县**，位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省西北部](../Page/甘肃省.md "wikilink")，是[酒泉市下属的一个](../Page/酒泉市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。

## 历史

瓜州一名见于《[汉书](../Page/汉书.md "wikilink")·地理志》载：“古瓜州地生美瓜”。[唐朝武德五年](../Page/唐朝.md "wikilink")（622年）改称瓜州；[清朝](../Page/清朝.md "wikilink")[康熙末年](../Page/康熙.md "wikilink")[策旺阿喇布坦叛乱被平定之后瓜州改称安西](../Page/策旺阿喇布坦.md "wikilink")，雍正年间设安西卫；民国2年（1913年）改为安西县；2006年2月25日改名为瓜州县。

## 行政区划

下辖10个[镇](../Page/镇.md "wikilink")、2个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、3个[民族乡](../Page/民族乡.md "wikilink")：

。

## 名胜古迹

瓜州境内文物古迹众多，有[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")4个。唐代名城[锁阳城](../Page/锁阳城遗址.md "wikilink")、“康熙梦城”桥湾城历史文化底蕴深厚，[东千佛洞](../Page/东千佛洞.md "wikilink")、[榆林窟与敦煌莫高窟媲美](../Page/榆林窟.md "wikilink")。

## 外部链接

  - [瓜州县政府网站](http://www.guazhou.gov.cn/)
  - [瓜州县地方综合信息门户网](http://www.guazhouxian.com/)

## 参考文献

[瓜州县](../Category/瓜州县.md "wikilink")
[县](../Category/酒泉区县市.md "wikilink")
[酒泉](../Category/甘肃省县份.md "wikilink")