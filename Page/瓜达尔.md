**瓜达尔**（、）位于[巴基斯坦西南沿海](../Page/巴基斯坦.md "wikilink")，靠近[波斯湾的](../Page/波斯湾.md "wikilink")[霍尔木兹海峡](../Page/霍尔木兹海峡.md "wikilink")，在盛产[石油的中东地区](../Page/石油.md "wikilink")，人口稠密的南亚和新兴经济和资源丰富的中亚地区之间。[瓜达尔港的修建](../Page/瓜达尔港.md "wikilink")，预计会带来数十亿美元的收入，创造2万个就业岗位。目前有一新興建的港口，可以連通[南亞及](../Page/南亞.md "wikilink")[西亞](../Page/西亞.md "wikilink")。

## 历史

18世纪统治巴基斯坦的[卡拉特可汗把这个地方赠给](../Page/卡拉特可汗.md "wikilink")[阿曼苏丹国](../Page/阿曼.md "wikilink")，在1958年巴基斯坦重新买回。

[Category:俾路支省地理](../Category/俾路支省地理.md "wikilink")