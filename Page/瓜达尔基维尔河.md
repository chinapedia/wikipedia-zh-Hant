[Localización_del_río_Guadalquivir.png](https://zh.wikipedia.org/wiki/File:Localización_del_río_Guadalquivir.png "fig:Localización_del_río_Guadalquivir.png")
**瓜达尔基维尔河**（[西班牙语](../Page/西班牙语.md "wikilink")：Guadalquivir）是[西班牙的第五长](../Page/西班牙.md "wikilink")[河](../Page/河.md "wikilink")（仅次于[塔古斯河和](../Page/塔古斯河.md "wikilink")[埃布罗河](../Page/埃布罗河.md "wikilink")），也是[安达卢西亚自治区境内的第一长河](../Page/安达卢西亚自治区.md "wikilink")。它的名字源于[阿拉伯语阿尔](../Page/阿拉伯语.md "wikilink")-瓦德-阿尔-卡比尔（，音译al-Wadī
al-Kabīr），意为“大河”。在[阿拉伯人征服之前](../Page/阿拉伯人.md "wikilink")，这条河的名字是巴埃提斯河，[罗马帝国在西班牙的](../Page/罗马帝国.md "wikilink")[行省](../Page/罗马行省.md "wikilink")[巴埃提卡行省即以此河命名](../Page/巴埃提卡行省.md "wikilink")。

瓜达尔基维尔河全长657公里，流域面积约58,000平方公里。它发源于[哈恩省境内的](../Page/哈恩省.md "wikilink")[卡索拉山脉](../Page/卡索拉山脉.md "wikilink")，流经[科尔多瓦和](../Page/科尔多瓦_\(西班牙\).md "wikilink")[塞维利亚两大](../Page/塞维利亚.md "wikilink")[城市](../Page/城市.md "wikilink")，在[桑卢卡尔-德巴拉梅达附近的小渔村注入](../Page/桑卢卡尔-德巴拉梅达.md "wikilink")[加的斯湾](../Page/加的斯湾.md "wikilink")。河流入海口处的[湿地构成了](../Page/湿地.md "wikilink")[多尼安纳国家公园](../Page/多尼安纳国家公园.md "wikilink")[自然保护区](../Page/自然保护区.md "wikilink")。

瓜达尔基维尔河是西班牙境内唯一可以通航的大河。在罗马帝国时代，直到[科尔多瓦的水道都可以通行](../Page/科尔多瓦_\(西班牙\).md "wikilink")；现在的通航段只到塞维利亚。

## 图片

<File:río> Guadalquivir Cordoba.jpg|科尔多瓦河段 <File:Cordoba> PuenteRomano
mezquita.jpg|科尔多瓦河段上的古罗马桥梁
[File:Cormoranes.jpg|多尼安纳国家公园](File:Cormoranes.jpg%7C多尼安纳国家公园)

## 外部链接

  - [More information from the United Nations Environmental
    Program.](https://web.archive.org/web/20050319232918/http://www.grid.unep.ch/product/publication/freshwater_europe/guadalquivir.php)

[G](../Category/西班牙河流.md "wikilink")