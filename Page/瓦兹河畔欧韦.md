**瓦兹河畔欧韦**（）是位于[法国北部](../Page/法国.md "wikilink")[法兰西岛大区](../Page/法兰西岛大区.md "wikilink")[瓦勒德瓦兹省的一个市镇](../Page/瓦勒德瓦兹省.md "wikilink")，属于[蓬图瓦兹区](../Page/蓬图瓦兹区.md "wikilink")。

[文森特·梵高生命的最后时光即在瓦兹河畔欧韦渡过](../Page/文森特·梵高.md "wikilink")，并在自杀后葬于此地。其弟[西奥·梵高也被埋葬这里](../Page/西奥·梵高.md "wikilink")。

## 人口

瓦兹河畔欧韦人口变化图示

## 参见

  - [瓦勒德瓦兹省市镇列表](../Page/瓦勒德瓦兹省市镇列表.md "wikilink")

## 参考文献

[Category:瓦兹河谷省市镇](../Category/瓦兹河谷省市镇.md "wikilink")