**瓦剌**（；[卫拉特语](../Page/卫拉特语.md "wikilink")：；[英语](../Page/英语.md "wikilink"):）是西部[蒙古族](../Page/蒙古族.md "wikilink")，[元朝称](../Page/元朝.md "wikilink")**斡亦剌惕**、[明朝称](../Page/明朝.md "wikilink")**瓦剌**（）、[清朝稱](../Page/清朝.md "wikilink")**卫拉特**、**漠西蒙古**等，意即居住于大漠（[戈壁沙漠](../Page/戈壁.md "wikilink")）以西的蒙古族。

**厄鲁特**（）本是[瓦剌的一部](../Page/準噶爾部.md "wikilink")，但在清朝文献中也用来泛指瓦剌。

历史上[蒙古民族是由两个基本部分组成的](../Page/蒙古.md "wikilink")。古代两分为“草原百姓”和“林中百姓”（斡亦剌惕、[不里牙惕](../Page/布里亞特人.md "wikilink")）。到后来为东部蒙古（中央蒙古）和西部蒙古（以瓦剌为主）。「瓦剌」的意思是「森林之民」或「邻近者」。\[1\]

## 起源

瓦剌先世为“斡亦剌惕”。原居住于[叶尼塞河上游八河地区](../Page/叶尼塞河.md "wikilink")。[成吉思汗立国时](../Page/成吉思汗.md "wikilink")，**[忽都合別乞](../Page/忽都合別乞.md "wikilink")**領有四千户。与[成吉思汗](../Page/成吉思汗.md "wikilink")[黄金家族有世婚关系](../Page/黄金家族.md "wikilink")，在蒙古国中一直享有“亲视诸王”的特殊地位。14世纪时，以[元朝皇室衰微](../Page/元朝.md "wikilink")，遂乘机扩大实力，积极参予各派系纷争。

在[马哈木](../Page/马哈木.md "wikilink")、太平、把秃孛罗分领瓦剌时，瓦剌实力相当强大，时称“四万卫拉特”。辖境除叶尼塞河上游外，还包括[额尔齐斯河上游](../Page/额尔齐斯河.md "wikilink")、[科布多东南](../Page/科布多.md "wikilink")[札布罕河流域等地](../Page/札布罕河.md "wikilink")。他们为同控制着汗位的东部蒙古贵族分庭抗礼，并进而称雄于[漠北](../Page/漠北.md "wikilink")，一面结好于明廷，遣使向明廷贡马。一面又积极同东部蒙古统治集团进行斗争。

## 瓦剌兴起

1414年，[明成祖统兵北征西部蒙古](../Page/明成祖.md "wikilink")，与瓦剌战于忽兰忽失温（今蒙古国[乌兰巴托东](../Page/乌兰巴托.md "wikilink")），马哈木败。东部蒙古[阿鲁台又发兵往击](../Page/阿鲁台.md "wikilink")。马哈木又败，积忧愤死。马哈木的儿子[脫歡继袭](../Page/脫懽.md "wikilink")，明廷仍封之为顺宁王。

1423年，东部蒙古阿鲁台与明廷关系恶化，被明军击败，脱欢乘隙于饮马河（今[克鲁伦河](../Page/克鲁伦河.md "wikilink")）破其众，俘其大量马驼牛羊和部众。饮马河之捷，使脱欢政治、经济和军事实力得到大大加强，统一瓦剌各部。

1434年，又出兵击阿鲁台于母纳山（今[内蒙古](../Page/内蒙古.md "wikilink")[乌拉山](../Page/乌拉山.md "wikilink")）、杀阿鲁台及其子[失捏干](../Page/失捏干.md "wikilink")，尽收其部众，东西蒙古一时俱为所有。脱欢本欲自立为汗，但因他不是[黄金家族后裔](../Page/尼伦.md "wikilink")，受到部下的强烈反对。于是拥立元裔[脱脱不花为汗](../Page/脱脱不花.md "wikilink")，并让其管辖阿鲁台旧有部众，居住于[呼伦贝尔草原一带](../Page/呼伦贝尔.md "wikilink")；又将己女嫁与脱脱不花为妻，自为太师，居住漠北，直接掌握蒙古的政治、经济实权。

1439年，脱欢病死，子[也先继位](../Page/也先.md "wikilink")，瓦剌在其统治时期，势力达到全盛。

瓦剌控制东部蒙古各部，一面又利用军事征讨、封官设治、联姻结盟等手段，把乞儿吉思、哈密、沙州、罕东、赤斤、[兀良哈三卫等](../Page/兀良哈三卫.md "wikilink")，分别置于自己统治之下。又结好[女真各部](../Page/女真.md "wikilink")，使之为其效力。极盛时势力东抵[朝鲜](../Page/朝鲜.md "wikilink")，西部至[塔拉斯河並與](../Page/塔拉斯河.md "wikilink")[巴爾喀什湖以西的](../Page/巴爾喀什湖.md "wikilink")[金帳汗國接壤](../Page/金帳汗國.md "wikilink")，北括[貝加爾湖及南](../Page/貝加爾湖.md "wikilink")[西伯利亚](../Page/西伯利亚.md "wikilink")，南临[长城](../Page/长城.md "wikilink")。

## 土木之变

在[也先统治初期](../Page/也先.md "wikilink")，瓦剌与明廷关系比较密切。1449年夏，也先借口出兵进攻[大同](../Page/大同市.md "wikilink")、[宣府](../Page/宣府.md "wikilink")、[辽东](../Page/辽东.md "wikilink")、[甘肃](../Page/甘肃.md "wikilink")。[明英宗在宦官](../Page/明英宗.md "wikilink")[王振怂恿下率兵亲征](../Page/王振.md "wikilink")，也先诱明军至大同，破其前锋。接着，又聚军于[土木堡](../Page/土木堡.md "wikilink")，歼灭明军主力，俘明英宗以去，史称为[土木之变](../Page/土木之变.md "wikilink")。同年十月，也先进围[北京](../Page/北京.md "wikilink")，企图迫明廷订城下之盟。但因北京的明军和市民奋力抵抗，也先计划未能实现。由于伤亡惨重，加上兵士厌战，内部矛盾重重。1450年也先被迫将明英宗送还，双方重新恢复正常通贡互市关系。

也先与[脱脱不花向不相睦](../Page/脱脱不花.md "wikilink")。脱脱不花名义上虽然是汗，但实际权力却操在也先手里。也先自恃势强，垂涎汗位，欲立己姊子“为太子”，脱脱不花拒之，双方发生激烈战争。脱脱不花初与弟[阿噶多尔济联兵](../Page/阿噶多尔济.md "wikilink")，彼此实力大致相当，难分胜负。后因兄弟内讧，[阿噶多尔济叛投也先](../Page/阿噶多尔济.md "wikilink")。脱脱不花势孤被败，遁入[兀良哈](../Page/唐努乌梁海.md "wikilink")（唐努乌梁海）。脱脱不花死后，也先自称“天圣大可汗”，建号“[添元](../Page/添元.md "wikilink")”，以次子[阿失帖木儿为太师](../Page/阿失帖木儿.md "wikilink")。

## 西蒙古

后来东部蒙古[达延汗再兴](../Page/达延汗.md "wikilink")，瓦剌部则移师西北地区，势力一度扩张至[伊犁河流域一带](../Page/伊犁河.md "wikilink")。为了保证贸易的顺利进行，阿失帖木儿还不时遣使向明朝通贡。16世纪，在[俺答汗等的持续打击下](../Page/俺答汗.md "wikilink")，瓦剌势力衰落，退出了今天[蒙古国地区](../Page/蒙古国.md "wikilink")，当地被[外喀尔喀占领](../Page/外喀尔喀.md "wikilink")。到16世纪末，瓦剌在[达赖泰什](../Page/达赖泰什.md "wikilink")、[哈喇忽剌](../Page/哈喇忽剌.md "wikilink")、[博贝密尔咱](../Page/博贝密尔咱.md "wikilink")、[和鄂尔勒克领导下重新复兴](../Page/和鄂尔勒克.md "wikilink")，瓦剌繼續在當地存在，直至被[俄羅斯沙皇國吞併](../Page/俄羅斯沙皇國.md "wikilink")。

瓦剌分为四大部：[杜尔伯特](../Page/杜尔伯特部_\(绰罗斯氏\).md "wikilink")（[绰罗斯氏](../Page/绰罗斯.md "wikilink")）、[准噶尔](../Page/准噶尔部.md "wikilink")（[绰罗斯氏](../Page/绰罗斯.md "wikilink")）、[和硕特](../Page/和硕特.md "wikilink")（成吉思汗二弟[合撒儿之后](../Page/合撒儿.md "wikilink")）、[土尔扈特](../Page/土尔扈特.md "wikilink")。另有[辉特等小部](../Page/輝特部.md "wikilink")。

17世纪初，准噶尔部强势控制[天山南北](../Page/天山.md "wikilink")，土尔扈特部西迁到[伏尔加河沿岸](../Page/伏尔加河.md "wikilink")，和硕特部又在[固始汗率领下迁到](../Page/固始汗.md "wikilink")[青海](../Page/青海.md "wikilink")。

## 清代

准噶尔部在1759年为清军所灭，[土尔扈特部迁回新疆](../Page/土尔扈特.md "wikilink")，史称土尔扈特部东归。

## 领袖

<table>
<thead>
<tr class="header">
<th><p>名號</p></th>
<th><p>在位時間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/孛罕.md" title="wikilink">孛罕</a>（<a href="../Page/太师.md" title="wikilink">太师</a>)</p></td>
<td><p>？—13世纪末</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乌林台巴达.md" title="wikilink">乌林台巴达</a>（乌林台巴达台什）(<a href="../Page/太师.md" title="wikilink">太师</a>)</p></td>
<td><p>？—13世纪末</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/浩海达裕.md" title="wikilink">浩海达裕</a>（浩海太尉）（<a href="../Page/太尉.md" title="wikilink">太尉</a>）</p></td>
<td><p>？—1409年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/猛可帖木儿.md" title="wikilink">猛可帖木儿</a></p></td>
<td><p>？—1409年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马哈木.md" title="wikilink">马哈木</a>（<a href="../Page/太师.md" title="wikilink">太师</a>）</p></td>
<td><p>1409年—1416年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/脱懽.md" title="wikilink">脱懽</a>（<a href="../Page/太师.md" title="wikilink">太师</a>）</p></td>
<td><p>1416年—1439年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/也先.md" title="wikilink">也先</a><br />
额森（<a href="../Page/太师.md" title="wikilink">太师</a>）（大元天盛可汗）</p></td>
<td><p>1439年—1454年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿失帖木儿.md" title="wikilink">阿失帖木儿</a><br />
俺檀汗<br />
额斯墨特达尔罕诺颜 （<a href="../Page/太师.md" title="wikilink">太师</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克舍.md" title="wikilink">克舍</a><br />
额斯图木</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/养罕.md" title="wikilink">养罕</a><br />
哈木克台吉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/卜六.md" title="wikilink">卜六</a><br />
阿喇哈青森</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/翁郭楚.md" title="wikilink">翁郭楚</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布拉台吉.md" title="wikilink">布拉台吉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈剌忽剌.md" title="wikilink">哈剌忽剌</a></p></td>
<td><p>17世纪初—1634年</p></td>
</tr>
</tbody>
</table>

### [准噶尔部](../Page/准噶尔部.md "wikilink")

| 名號                                           | 在位時間        |
| -------------------------------------------- | ----------- |
| [巴图尔珲台吉](../Page/巴图尔珲台吉.md "wikilink")       | 1634年—1653年 |
| [僧格](../Page/僧格.md "wikilink")               | 1653年—1671年 |
| [噶尔丹](../Page/噶尔丹.md "wikilink")             | 1671年—1697年 |
| [策妄阿拉布坦](../Page/策妄阿拉布坦.md "wikilink")       | 1697年—1727年 |
| [噶尔丹策零](../Page/噶尔丹策零.md "wikilink")         | 1727年—1745年 |
| [策妄多尔济那木扎尔](../Page/策妄多尔济那木扎尔.md "wikilink") | 1745年—1750年 |
| [喇嘛达尔扎](../Page/喇嘛达尔扎.md "wikilink")         | 1750年—1753年 |
| [达瓦齐](../Page/达瓦齐.md "wikilink")             | 1750年—1755年 |
| [阿睦尔撒纳](../Page/阿睦尔撒纳.md "wikilink")         | 1755年—1757年 |

### [杜爾伯特部](../Page/杜爾伯特部.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>世代</p></th>
<th><p>姓名</p></th>
<th><p>年份</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一代</p></td>
<td><p><a href="../Page/車凌.md" title="wikilink">車凌</a></p></td>
<td><p>1756年－1758年</p></td>
<td><p>1753年降清</p></td>
</tr>
<tr class="even">
<td><p>第二代</p></td>
<td><p><a href="../Page/索諾木袞布.md" title="wikilink">索諾木袞布</a></p></td>
<td><p>1758年－1769年</p></td>
<td><p>車凌子</p></td>
</tr>
<tr class="odd">
<td><p>第三代</p></td>
<td><p><a href="../Page/瑪克蘇爾札布.md" title="wikilink">瑪克蘇爾札布</a></p></td>
<td><p>1769年－1812年</p></td>
<td><p>索諾木袞布子</p></td>
</tr>
<tr class="even">
<td><p>第四代</p></td>
<td><p><a href="../Page/旺拉布.md" title="wikilink">旺拉布</a></p></td>
<td><p>1812年－1820年</p></td>
<td><p>瑪克蘇爾札布子</p></td>
</tr>
<tr class="odd">
<td><p>第五代</p></td>
<td><p><a href="../Page/拉木札布.md" title="wikilink">拉木札布</a></p></td>
<td><p>1820年－1821年</p></td>
<td><p>旺拉布子</p></td>
</tr>
<tr class="even">
<td><p>第六代</p></td>
<td><p><a href="../Page/齊旺巴勒楚克.md" title="wikilink">齊旺巴勒楚克</a></p></td>
<td><p>1821年－1843年</p></td>
<td><p>旺拉布弟</p></td>
</tr>
<tr class="odd">
<td><p>第七代</p></td>
<td><p><a href="../Page/密什克多爾濟.md" title="wikilink">密什克多爾濟</a></p></td>
<td><p>1843年－1848年</p></td>
<td><p>齊旺巴勒楚克次子</p></td>
</tr>
<tr class="even">
<td><p>第八代</p></td>
<td><p><a href="../Page/散都克多爾濟.md" title="wikilink">散都克多爾濟</a></p></td>
<td><p>1848年－1854年</p></td>
<td><p><a href="../Page/滿達拉.md" title="wikilink">滿達拉長子</a></p></td>
</tr>
<tr class="odd">
<td><p>第九代</p></td>
<td><p><a href="../Page/勒札勒喇布坦希哩珠特.md" title="wikilink">勒札勒喇布坦希哩珠特</a></p></td>
<td><p>1854年－1863年</p></td>
<td><p>散都克多爾濟子</p></td>
</tr>
<tr class="even">
<td><p>第十代</p></td>
<td><p><a href="../Page/那克旺札勒禪.md" title="wikilink">那克旺札勒禪</a></p></td>
<td><p>1864年－1870年</p></td>
<td><p><a href="../Page/滿達拉.md" title="wikilink">滿達拉三子</a></p></td>
</tr>
<tr class="odd">
<td><p>第十一代</p></td>
<td><p><a href="../Page/噶勒章那木濟勒.md" title="wikilink">噶勒章那木濟勒</a></p></td>
<td><p>1870年－?</p></td>
<td><p><a href="../Page/巴咱爾扎那.md" title="wikilink">巴咱爾扎那子</a><br />
<a href="../Page/密什克多爾濟.md" title="wikilink">密什克多爾濟嗣子</a></p></td>
</tr>
</tbody>
</table>

### [和硕特部](../Page/和硕特.md "wikilink")

| 汗號                                     | 姓名                                   | 在位時間        |
| -------------------------------------- | ------------------------------------ | ----------- |
| [固始汗](../Page/固始汗.md "wikilink")       |                                      | 1606年—1654年 |
| [鄂齐尔图汗](../Page/鄂齐尔图汗.md "wikilink")   |                                      | 1654年—1675年 |
| [达延鄂齐尔汗](../Page/达延鄂齐尔汗.md "wikilink") |                                      | 1654年—1670年 |
| [达赖汗](../Page/达赖汗.md "wikilink")       | [朋素克](../Page/朋素克.md "wikilink")     | 1671年—1701年 |
| [拉藏汗](../Page/拉藏汗.md "wikilink")       |                                      | 1701年—1717年 |
|                                        | [罗卜藏丹津](../Page/罗卜藏丹津.md "wikilink") | 1714年—1755年 |

### [土尔扈特部](../Page/土尔扈特.md "wikilink")

| 名號                                   | 在位時間        |
| ------------------------------------ | ----------- |
| [和鄂尔勒克](../Page/和鄂尔勒克.md "wikilink") | 1628年—1643年 |
| [书库尔岱青](../Page/书库尔岱青.md "wikilink") | 1645年—1667年 |
| [朋楚克](../Page/朋楚克.md "wikilink")     | 1667年—1670年 |
| [阿玉奇](../Page/阿玉奇.md "wikilink")     | 1670年—1724年 |
| [策凌敦多布](../Page/策凌敦多布.md "wikilink") | 1724年—1735年 |
| [敦罗卜旺布](../Page/敦罗卜旺布.md "wikilink") | 1735年—1741年 |
| [敦罗布剌什](../Page/敦罗布剌什.md "wikilink") | 1741年—1761年 |
| [渥巴锡](../Page/渥巴锡.md "wikilink")     | 1761年—1771年 |

## 关联条目

### 清朝划分

  - [漠南蒙古](../Page/漠南蒙古.md "wikilink")
  - [漠西蒙古](../Page/漠西蒙古.md "wikilink")
  - [漠北蒙古](../Page/漠北蒙古.md "wikilink")
  - [青海蒙古](../Page/青海蒙古.md "wikilink")

### 明朝划分

  - **瓦剌**
  - [鞑靼](../Page/鞑靼_\(蒙古\).md "wikilink")
  - [兀良哈](../Page/兀良哈三卫.md "wikilink")

### 沙俄划分

  - [喀尔喀](../Page/喀尔喀.md "wikilink")
  - [卡尔梅克](../Page/卡尔梅克.md "wikilink")

## 参考文献

## 外部連結

  - [Oyirad-mongols (shot notes, photo, historical review,
    maps)](http://www.hamagmongol.narod.ru/oyirad/index_e.htm)
  - [Mongolistic forum](http://khamagmongol.com/chuulgan)
  - [Khoyt S.K. White spots in dorwod (derbet) ethnogenesis - in
    russian](http://www.hamagmongol.narod.ru/library/hoyt_dorwod_2006_r.htm)
  - [Khoyt S.K. Last data by localisation and number of oyirad (oirat)
    (rar) - in
    russian](http://www.hamagmongol.narod.ru/library/pe_2008/hoyt_locnum_2008_r.htm)
  - [Khoyt S.K. Last data by localisation and number of oyirad (oirat)
    (htm republication) - in
    russian](http://www.bumbinorn.ru/2008/12/07/poslednie_dannye_po_lokalizacii_i_chislennosti_ojjrat_48654.html)
  - [Khoyt S.K. Other ethnic inclusion in european oyirad (oirat) groups
    by family trees (genealogys) datas. doc, 114 kb (rar, 14 kb) - in
    russian](http://www.hamagmongol.narod.ru/library/hoyt_inclusion_2008_r.htm)
  - [Khoyt S.K. The miscegenation problem in kalmyk society. - in
    russian](http://www.hamagmongol.narod.ru/library/pe_2008/hoyt_miscegenation_2008_r.htm)
  - [Oirad Mongols auf
    mandal.ca](http://www.mandal.ca/mongolia/o/Oirad_Mongols.html)

{{-}}

[卫拉特](../Category/卫拉特.md "wikilink")

1.