**瓦尔达斯·阿达姆库斯**（[立陶宛语](../Page/立陶宛语.md "wikilink")：****，），[立陶宛政治家](../Page/立陶宛.md "wikilink")，2004年7月12日起任[总统](../Page/立陶宛总统.md "wikilink")。阿达姆库斯在1998年至2003年间也曾任总统，但在2003年的选举中被[罗兰达斯·帕克萨斯击败](../Page/罗兰达斯·帕克萨斯.md "wikilink")。后来帕克萨斯在2004年4月6日被议会弹劾下台，阿达姆库斯在接下来的选举中顺利当选。

## 簡歷

## 總統任內（1998年至2003年）

## 再任總統（2004年至2009年）

### 內政事務

### 外交事務

## 獲得榮譽

阿達姆庫斯已榮獲以下榮譽：

| Year | Award                                                                                                                           | Issuer                                 |
| ---- | ------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| 1985 | President's Award for Distinguished Federal Civilian Service                                                                    |                                        |
| 1998 | The Grand Cross of the [Order of the Falcon](../Page/Order_of_the_Falcon.md "wikilink")                                         | [冰島](../Page/冰島.md "wikilink")         |
| 1998 | The Grand Cross of the [Order of St. Olav](../Page/The_Royal_Norwegian_Order_of_St._Olav.md "wikilink")                         | [挪威](../Page/挪威.md "wikilink")         |
| 1998 | The First Class of the Order of Yaroslav the Wise                                                                               | [烏克蘭](../Page/烏克蘭.md "wikilink")       |
| 1999 | The Collar and the Grand Cross of the Order of Mary's Land                                                                      | [愛沙尼亞](../Page/愛沙尼亞.md "wikilink")     |
| 1999 | The Grand Cross of the Order of the Saviour                                                                                     | [希臘](../Page/希臘.md "wikilink")         |
| 1999 | The Class Collar and the Grand Cross of the Order for the Services                                                              | [意大利](../Page/意大利.md "wikilink")       |
| 1999 | The [Order of the White Eagle](../Page/Order_of_the_White_Eagle.md "wikilink")                                                  | [波蘭](../Page/波蘭.md "wikilink")         |
| 1999 | The Grand Cross of the Order for the Services                                                                                   | [馬爾他](../Page/馬爾他.md "wikilink")       |
| 1999 | The Grand Cross of the Order for the Services                                                                                   | [匈牙利](../Page/匈牙利.md "wikilink")       |
| 2000 | The Grand Cross of the Order of Friendship                                                                                      | [哈薩克斯坦](../Page/哈薩克斯坦.md "wikilink")   |
| 2001 | The Collar and the Grand Cross of the Order of Three Stars                                                                      | [拉脫維亞](../Page/拉脫維亞.md "wikilink")     |
| 2001 | The Grand Cross of the [Order of the French Legion of Honour](../Page/Légion_d'honneur.md "wikilink")                           | [法國](../Page/法國.md "wikilink")         |
| 2001 | The Collar of the [Order of the Star of Romania](../Page/Steaua_României.md "wikilink")                                         | [羅馬尼亞](../Page/羅馬尼亞.md "wikilink")     |
| 2002 | The Order of St. Meshrop Mashtots                                                                                               | [亞美尼亞](../Page/亞美尼亞.md "wikilink")     |
| 2002 | The Collar and the Grand Cross of the [Order of the White Rose](../Page/Order_of_the_White_Rose.md "wikilink")                  | [芬蘭](../Page/芬蘭.md "wikilink")         |
| 2002 | The Order For Special Merits                                                                                                    | [烏茲別克斯坦](../Page/烏茲別克斯坦.md "wikilink") |
| 2002 | St Andrew 'Dialogue of Civilisation' prize laureate                                                                             | [俄羅斯](../Page/俄羅斯.md "wikilink")       |
| 2003 | The [Order of Vytautas the Great with the Golden Chain](../Page/Order_of_Vytautas_the_Great.md "wikilink")                      | [立陶宛](../Page/立陶宛.md "wikilink")       |
| 2004 | The golden collar and the Grand Cross of the [Order of the White Star](../Page/Order_of_the_White_Star.md "wikilink")           | 愛沙尼亞                                   |
| 2005 | The Order of Isabel the Catholic with the collar                                                                                | [西班牙](../Page/西班牙.md "wikilink")       |
| 2005 | The Special Class of the Grand Cross of the Order for the Services                                                              | [德國](../Page/德國.md "wikilink")         |
| 2006 | The Grand Cross of the [Order of Leopold](../Page/Order_of_Leopold.md "wikilink")                                               | [比利時](../Page/比利時.md "wikilink")       |
| 2006 | The Grand Cross of the [Order of the Bath](../Page/Order_of_the_Bath.md "wikilink")                                             | [英國](../Page/英國.md "wikilink")         |
| 2006 | The First Class of the Order for Merits to Ukraine                                                                              | 烏克蘭                                    |
| 2007 | The Order "Mother Theresa"                                                                                                      | [阿爾巴尼亞](../Page/阿爾巴尼亞.md "wikilink")   |
| 2007 | The Grand Cordon of the [Supreme Order of the Chrysanthemum](../Page/Supreme_Order_of_the_Chrysanthemum.md "wikilink")          | [日本](../Page/日本.md "wikilink")         |
| 2007 | The [St. George's Victory Order](../Page/Orders,_decorations,_and_medals_of_Georgia#St._George.27s_Victory_Order.md "wikilink") | [格魯吉亞](../Page/格魯吉亞.md "wikilink")     |
| 2007 | European of the Year                                                                                                            |                                        |
| 2008 | The Grand Cross of the [Order of the Netherlands Lion](../Page/Order_of_the_Netherlands_Lion.md "wikilink")                     | [荷蘭](../Page/荷蘭.md "wikilink")         |
| 2008 | Collar of the [Order of the Merit of Chile](../Page/Order_of_the_Merit_of_Chile.md "wikilink")                                  | [智利](../Page/智利.md "wikilink")         |
| 2009 | Grand Star of the Decoration of Honour for Services to the Republic of Austria                                                  | [奧地利](../Page/奧地利.md "wikilink")       |
| 2009 | Order of Stara Planina                                                                                                          | [保加利亞](../Page/保加利亞.md "wikilink")     |

## 榮譽博士學位

阿達姆庫斯獲立陶宛、美國和其他國家的大學頒授[名譽博士學位](../Page/名譽博士學位.md "wikilink")，其中包括：

  - [維爾紐斯大學](../Page/維爾紐斯大學.md "wikilink")，(立陶宛，1989年)
  - [聖約瑟夫學院](../Page/聖約瑟夫學院\(印第安納州\).md "wikilink")，([美國](../Page/美國.md "wikilink")，1991年)
  - [西北大學](../Page/西北大學_\(伊利諾州\).md "wikilink")，(美國，1994年)
  - [考納斯理工大學](../Page/考納斯理工大學.md "wikilink")，(立陶宛，1998年)
  - [美國天主教大學](../Page/美國天主教大學.md "wikilink")，(美國，1998年)
  - [立陶宛農業大學](../Page/立陶宛農業大學.md "wikilink")，(美國，1999年)
  - [伊利諾理工大學](../Page/伊利諾理工大學.md "wikilink")，(美國，1999年)
  - [歐亞大學](../Page/歐亞大學.md "wikilink")，([哈薩克斯坦](../Page/哈薩克斯坦.md "wikilink")，2000年)
  - DePaul大學，(美國，2001年)
  - Mykolas Romeris大學，(立陶宛，2001年)
  - [卡爾尼斯基斯-馬格納斯大學](../Page/卡爾尼斯基斯-馬格納斯大學.md "wikilink")，(立陶宛，2002年)
  - [立陶宛體育教育學院](../Page/立陶宛體育教育學院.md "wikilink")，(立陶宛，2004年)
  - [埃里溫國立大學](../Page/埃里溫國立大學.md "wikilink")，([亞美尼亞](../Page/亞美尼亞.md "wikilink")，2006年)
  - [巴庫國立大學](../Page/巴庫國立大學.md "wikilink")，([阿塞拜疆](../Page/阿塞拜疆.md "wikilink")，2006年)
  - [頓涅茨克大學](../Page/頓涅茨克大學.md "wikilink")，([烏克蘭](../Page/烏克蘭.md "wikilink")，2006年)
  - [聖母大學](../Page/聖母大學.md "wikilink")，(美國，2007年)
  - [哥白尼大學](../Page/哥白尼大學.md "wikilink")，([波蘭](../Page/波蘭.md "wikilink")，2007年)
  - [塔林大學](../Page/塔林大學.md "wikilink")，([愛沙尼亞](../Page/愛沙尼亞.md "wikilink")，2008年)
  - [智利大學](../Page/智利大學.md "wikilink")，([智利](../Page/智利.md "wikilink")，
    2008年)
  - [克萊佩達大學](../Page/克萊佩達大學.md "wikilink")，(立陶宛，2008年)

## 參考資料

## 另見

## 外部链接

  - [2005年阿达姆库斯接受乌克兰记者的一次访问](http://www.wumag.kiev.ua/index2.php?param=pgs20052/50)
  - [www.president.lt/ru](http://www.president.lt/ru/)
  - [www.president.lt/en](http://www.president.lt/en/)
  - [www.youtube.com/spaudostarnyba](http://www.youtube.com/spaudostarnyba/)

[A](../Category/立陶宛总统.md "wikilink")
[A](../Category/立陶宛政治家.md "wikilink")
[A](../Category/伊利諾理工學院校友.md "wikilink")
[A](../Category/慕尼黑大學校友.md "wikilink")
[Category:联合国教科文组织亲善大使](../Category/联合国教科文组织亲善大使.md "wikilink")