**瓦爾特·内林**（，），是德國在[第二次世界大戰中之上將](../Page/第二次世界大戰.md "wikilink")，他在[埃爾溫·隆美爾元帥的屬下任](../Page/埃爾溫·隆美爾.md "wikilink")[非洲軍軍長](../Page/非洲軍.md "wikilink")，並負重傷（[重傷勳章金一級](../Page/重傷勳章.md "wikilink")）。

[波蘭戰役時](../Page/波蘭戰役.md "wikilink")，内林在[坦克名將](../Page/坦克.md "wikilink")[海因茨·古德里安麾下參與戰鬥](../Page/海因茨·古德里安.md "wikilink")。

1942年5月起，内林奉派至[非洲軍領軍作戰](../Page/非洲軍.md "wikilink")，並參與[阿萊曼戰役](../Page/阿萊曼戰役.md "wikilink")
(1942年8月31日 -
9月7日)，他也因此這場戰役遭到[空襲負傷](../Page/空襲.md "wikilink")；11月至12月，他帶傷繼續指揮德軍的[突尼西亞戰役](../Page/突尼西亞戰役.md "wikilink")。

北非戰役結束後，他被調派至東戰場對俄國作戰；起先他統領[德國第24裝甲軍](../Page/德國第24裝甲軍.md "wikilink")，1944年7月起改為統領[第4裝甲軍團](../Page/德國第4裝甲軍團.md "wikilink")，1945年3月大戰結束前二個月，改派他統領[第1裝甲軍團](../Page/德國第1裝甲軍團.md "wikilink")。

## 軍人獎勳章

  - [重傷勳章金一級](../Page/重傷勳章.md "wikilink")
  - 「坦克軍勳章」銀級
  - 非洲軍團勳章
  - [銀制勇敢勳章](../Page/銀制勇敢勳章.md "wikilink")
  - [鐵十字勳章一級及二級](../Page/鐵十字勳章.md "wikilink")
  - [橡葉騎士佩寶劍鐵十字勳章](../Page/鐵十字勳章.md "wikilink")
      - 騎士鐵十字勳章(1941年7月24日)
      - 383\. 加「橡葉」(1944年2月8日)
      - 124\. 加「寶劍」(1945年1月22日)
  - 德軍戰史紀念人物
  - [聯邦十字勳章一級](../Page/聯邦十字勳章.md "wikilink")(1973年7月27日)

## 注釋

<div class="references-small">

  - Berger, Florian, *Mit Eichenlaub und Schwertern. Die
    höchstdekorierten Soldaten des Zweiten Weltkrieges*. Selbstverlag
    Florian Berger, 2006. ISBN 3-9501307-0-5.
  - Fellgiebel, Walther-Peer. *Die Träger des Ritterkreuzes des Eisernen
    Kreuzes 1939-1945*. Friedburg, Germany: Podzun-Pallas, 2000. ISBN
    3-7909-0284-5.

</div>

## 相關聯結

  - [1](https://web.archive.org/web/20080207231551/http://www.spartacus.schoolnet.co.uk/GERnehring.htm)
  - [Walther Nehring @ Lexikon der
    Wehrmacht](http://www.lexikon-der-wehrmacht.de/Personenregister/NehringW-R.htm)

[N](../Category/德國軍事人物.md "wikilink")
[N](../Category/橡葉騎士佩寶劍鐵十字勳章人物.md "wikilink")