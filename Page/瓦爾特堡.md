**瓦爾特堡**(Wartburg)位在[德國](../Page/德國.md "wikilink")[圖林根的城堡](../Page/圖林根.md "wikilink")，屬[艾森纳赫市](../Page/艾森纳赫.md "wikilink")，为[罗曼式建筑](../Page/罗曼式建筑.md "wikilink")。由路德因·德·史賓格(Ludwig
der
Springer)於1073年起建，1999年列為世界文化遺產。對於他的命名有一個笑話性傳說，路德因·德·史賓格建堡時，當他第一眼看到這個地點，他說：“等一下，山麓--你將成為我的城堡！”（Wart',
Berg--du sollst mir eine Burg werden\!，德文的Wart為等待之意）。

1867年[路德维希二世访问瓦尔特堡](../Page/路德维希二世.md "wikilink")，后来修建[新天鹅堡时参考了瓦尔特堡的宴会厅](../Page/新天鹅堡.md "wikilink")。

1952年—1966年，[东德政府出资对城堡进行了修复](../Page/东德.md "wikilink")，基本使它恢复到了16世纪时的模样。[马丁·路德翻译](../Page/马丁·路德.md "wikilink")[新约时所居住的房间也按照当年的样子得到复原](../Page/新约.md "wikilink")。這個城堡今日所見，與大自然合為一體的景觀，為大公爵卡爾·亞歷山大(Carl
Alexander)與威廉·恩斯特(Wilhelm Ernst)時期的規模。

## 連結

  - [瓦爾特堡基金會官方網站，包括年表與建築描寫](http://www.wartburg.de)
  - [照片](https://web.archive.org/web/20120304030937/http://www.historische-orte.de/wartburg.htm)

[W](../Category/德国世界遗产.md "wikilink")
[Category:德國城堡](../Category/德國城堡.md "wikilink")