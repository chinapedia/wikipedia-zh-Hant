**瓦爾盜龍屬**（[屬名](../Page/屬.md "wikilink")：，意為「[瓦爾河的盜賊](../Page/瓦爾河.md "wikilink")」）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[手盜龍類](../Page/手盜龍類.md "wikilink")[馳龍科的一](../Page/馳龍科.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[法國](../Page/法國.md "wikilink")，化石發現於法國南部的[福昂富](../Page/福昂富.md "wikilink")。

## 發現與命名

[Variraptor_mechinorum1.JPG](https://zh.wikipedia.org/wiki/File:Variraptor_mechinorum1.JPG "fig:Variraptor_mechinorum1.JPG")
[Variraptor_SIZE.png](https://zh.wikipedia.org/wiki/File:Variraptor_SIZE.png "fig:Variraptor_SIZE.png")
在1992到1995年間，業餘地質學家Patrick Méchin、Annie
Méchin-Salessy夫婦在[法國東南部](../Page/法國.md "wikilink")[瓦爾省發現一些小型獸腳類](../Page/瓦爾省.md "wikilink")[恐龍的化石](../Page/恐龍.md "wikilink")，發現於Grès
à
Reptiles組第層，地質年代相當於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[坎潘階到](../Page/坎潘階.md "wikilink")[馬斯垂克階交界期](../Page/馬斯垂克階.md "wikilink")。在1992年，這些新化石原先被歸類於[沼澤鳥龍](../Page/沼澤鳥龍.md "wikilink")，沼澤鳥龍本身是個[疑名](../Page/疑名.md "wikilink")\[1\]。在1997年，[埃瑞克·比弗托](../Page/埃瑞克·比弗托.md "wikilink")（Eric
Buffetaut）、Jean Le Loeuff等人發表文章，認為這些化石代表新物種\[2\]。在1998年，比弗托、Le
Loeuff將這些化石建立為新屬，[模式種是](../Page/模式種.md "wikilink")*V.
mechinorum*。屬名在[拉丁文意為](../Page/拉丁文.md "wikilink")「瓦爾的盜賊」，是以[法國南部的](../Page/法國.md "wikilink")[瓦爾省及](../Page/瓦爾省.md "wikilink")[瓦爾河為名](../Page/瓦爾河.md "wikilink")。種名則是以發現化石的Méchin夫婦為名\<ref
name=Le Loeuff\&Buffetaut\> Le Loeuff, Jean, Buffetaut, Eric (1999) A
new dromaeosaurid theropod from the Upper Cretaceous of Southern France.
Oryctos 1, 105-112</ref>。

瓦爾盜龍的[模式標本有三個](../Page/模式標本.md "wikilink")，包括：一節後段[背椎](../Page/背椎.md "wikilink")（編號MDE-D168）、一根由5節[薦椎癒合而成的](../Page/薦椎.md "wikilink")[薦骨](../Page/薦骨.md "wikilink")（編號MDE-D169）、一個[腸骨](../Page/腸骨.md "wikilink")（編號CM-645）。以上三個標本都存放於當地博物館。

其他被編入瓦爾盜龍的標本，包括一個右[肱骨](../Page/肱骨.md "wikilink")（編號MDE-D158），肱骨有一個較衍化的三角嵴，顯示前肢有強壯的捕食功能。另外被歸類於瓦爾盜龍的骨頭，包括了一根[股骨及一些脊椎骨](../Page/股骨.md "wikilink")。這些不完整的骨頭，在脊椎骨及肱骨的形狀上有[馳龍科的特徵](../Page/馳龍科.md "wikilink")，有些很像[恐爪龍的](../Page/恐爪龍.md "wikilink")。這些其他化石不一定屬於瓦爾盜龍。瓦爾盜龍的成年個體可能會比恐爪龍稍為小型，約有2公尺長。

## 分類

在2000年，[羅南·阿蘭](../Page/羅南·阿蘭.md "wikilink")（Ronan
Allain）與[菲利普·塔丘特](../Page/菲利普·塔丘特.md "wikilink")（Philippe
Taquet）將發現於同一地層的小型獸腳類化石，命名為[火盜龍](../Page/火盜龍.md "wikilink")。他們並提出，瓦爾盜龍缺乏可鑑定的特徵，因此是個[疑名](../Page/疑名.md "wikilink")\[3\]。

在2009年，埃瑞克·比弗托等人發表文章主張瓦爾盜龍的模式標本具有可鑑定特徵。由於瓦爾盜龍、火盜龍的化石沒有重複部分，因此無法斷定是否為相同動物\[4\]。

瓦爾盜龍被命名時，被歸類於[獸腳亞目](../Page/獸腳亞目.md "wikilink")[手盜龍類的](../Page/手盜龍類.md "wikilink")[馳龍科](../Page/馳龍科.md "wikilink")，這個分類被廣泛接受。在2000年，[奧利佛·勞赫](../Page/奧利佛·勞赫.md "wikilink")（Oliver
Rauhut）提出質疑，將瓦爾盜龍歸類於範圍更大的[虛骨龍類](../Page/虛骨龍類.md "wikilink")\[5\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [恐龍百科全書有關瓦爾盜龍的介紹](http://www.dinoruss.com/de_4/5caa488.htm)
  - [恐龍博物館──瓦爾盜龍](https://web.archive.org/web/20070922051335/http://www.dinosaur.net.cn/museum/Variraptor.htm)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:馳龍科](../Category/馳龍科.md "wikilink")

1.  Le Loeuff, J., Buffetaut, E., Mechin, P., Mechin-Salessy, A., 1992,
    "The first record of dromaeosaurid dinosaurs (Saurischia, Theropoda)
    in the Maastrichtian of southern Europe: palaeobiogeographical
    implications", *Bulletin de la Société Géologique de France*
    **163**: 337–343
2.  Eric Buffetaut, Jean Le Loeuff, Lionel Cavin, Sylvain Duffaud,
    Emmanuel Gheerbrant, Yves Laurent, Michel Martin, Jean-Claude Rage,
    Haiyan Tong & Denis Vasse, 1997, "Les vertébrés continentaux du
    Crétacé supérieur du Sud de la France: un aperçu sur des
    découvertes récentes", *Geobios* **30** Supplement 1, 1997: 101-108
3.  Allain, R. and Taquet, P., 2000, "A new genus of Dromaeosauridae
    (Dinosauria, Theropoda) from the Upper Cretaceous of France",
    *Journal of Vertebrate Paleontology*, **20**(2), 404-407
4.  Chanthasit, P., and Buffetaut, E., 2009, "New data on the
    Dromaeosauridae (Dinosauria: Theropoda) from the Late Cretaceous of
    southern France", *Bulletin de la Société Géologique de France*
    **180**(2):145-154
5.  Rauhut, 2000, *The interrelationships and evolution of basal
    theropods (Dinosauria, Saurischia)*. PhD dissertation, University of
    Bristol, Bristol. 583 pp \*Allain and Taquet, 2000, "A new genus of
    Dromaeosauridae (Dinosauria, Theropoda) from the Upper Cretaceous of
    France", *Journal of Vertebrate Paleontology*, **20**(2), 404-407