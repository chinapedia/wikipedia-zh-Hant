[Wassenaar_Arrangement_members_map.svg](https://zh.wikipedia.org/wiki/File:Wassenaar_Arrangement_members_map.svg "fig:Wassenaar_Arrangement_members_map.svg")
**《关于常规武器与两用产品和技术出口控制的瓦瑟纳尔协定》**\[1\]（又译作《关于传统武器与军民两用货物与技术的出口控制的瓦瑟纳尔协议》）（），简称“瓦瑟纳尔协定”\[2\]，是一項由40個國家簽署，管制傳統[武器及軍商兩用貨品](../Page/武器.md "wikilink")[出口的](../Page/出口.md "wikilink")[條約](../Page/條約.md "wikilink")。

這個協定於1996年5月12日於[荷蘭](../Page/荷蘭.md "wikilink")[瓦聖納由](../Page/瓦聖納.md "wikilink")33個國家簽署。该协定未正式列举被管制国家，只在口头上将[伊朗](../Page/伊朗.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[朝鲜和利比亚](../Page/朝鮮民主主義人民共和國.md "wikilink")4国列入管制对象\[3\]。[中国和](../Page/中华人民共和国.md "wikilink")[以色列不是缔约国](../Page/以色列.md "wikilink")，但仍受到缔约国向非缔约国出售限制货品或技术的报告审核限制。协定秘书处设在维也纳。每6个月，协定成员国需通报向协定成员国以外国家出口传统武器的情况。分作八类：主战坦克、装甲战车、大口径火炮、军机、军用直升机、战舰、导弹或导弹系统、小型武器或轻武器。

## 缔约国

至2018年2月，共42個簽署國：

<table width="100%">

<tr>

<td valign=top width="33%">

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -

</td>

<td valign=top width="33%">

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -

</td>

<td valign=top width="33%">

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -

</td>

</tr>

</table>

## 参考文献

[Category:裁軍條約](../Category/裁軍條約.md "wikilink")
[Category:國際組織](../Category/國際組織.md "wikilink")
[Category:進出口管制](../Category/進出口管制.md "wikilink")
[Category:20世紀條約](../Category/20世紀條約.md "wikilink")
[Category:反共主义](../Category/反共主义.md "wikilink")
[Category:阿根廷條約](../Category/阿根廷條約.md "wikilink")
[Category:澳大利亞條約](../Category/澳大利亞條約.md "wikilink")
[Category:奧地利條約](../Category/奧地利條約.md "wikilink")
[Category:比利時條約](../Category/比利時條約.md "wikilink")
[Category:保加利亞條約](../Category/保加利亞條約.md "wikilink")
[Category:加拿大條約](../Category/加拿大條約.md "wikilink")
[Category:克羅地亞條約](../Category/克羅地亞條約.md "wikilink")
[Category:捷克條約](../Category/捷克條約.md "wikilink")
[Category:丹麥條約](../Category/丹麥條約.md "wikilink")
[Category:愛沙尼亞條約](../Category/愛沙尼亞條約.md "wikilink")
[Category:芬蘭條約](../Category/芬蘭條約.md "wikilink")
[Category:法國條約](../Category/法國條約.md "wikilink")
[Category:德國條約](../Category/德國條約.md "wikilink")
[Category:希臘條約](../Category/希臘條約.md "wikilink")
[Category:匈牙利條約](../Category/匈牙利條約.md "wikilink")
[Category:印度條約](../Category/印度條約.md "wikilink")
[Category:愛爾蘭條約](../Category/愛爾蘭條約.md "wikilink")
[Category:義大利條約](../Category/義大利條約.md "wikilink")
[Category:日本條約](../Category/日本條約.md "wikilink")
[Category:拉脫維亞條約](../Category/拉脫維亞條約.md "wikilink")
[Category:立陶宛條約](../Category/立陶宛條約.md "wikilink")
[Category:盧森堡條約](../Category/盧森堡條約.md "wikilink")
[Category:馬耳他條約](../Category/馬耳他條約.md "wikilink")
[Category:墨西哥條約](../Category/墨西哥條約.md "wikilink")
[Category:荷蘭條約](../Category/荷蘭條約.md "wikilink")
[Category:新西蘭條約](../Category/新西蘭條約.md "wikilink")
[Category:挪威條約](../Category/挪威條約.md "wikilink")
[Category:波蘭條約](../Category/波蘭條約.md "wikilink")
[Category:葡萄牙條約](../Category/葡萄牙條約.md "wikilink")
[Category:韓國條約](../Category/韓國條約.md "wikilink")
[Category:羅馬尼亞條約](../Category/羅馬尼亞條約.md "wikilink")
[Category:俄羅斯條約](../Category/俄羅斯條約.md "wikilink")
[Category:斯洛伐克條約](../Category/斯洛伐克條約.md "wikilink")
[Category:斯洛文尼亞條約](../Category/斯洛文尼亞條約.md "wikilink")
[Category:南非條約](../Category/南非條約.md "wikilink")
[Category:西班牙條約](../Category/西班牙條約.md "wikilink")
[Category:瑞典條約](../Category/瑞典條約.md "wikilink")
[Category:瑞士條約](../Category/瑞士條約.md "wikilink")
[Category:土耳其條約](../Category/土耳其條約.md "wikilink")
[Category:烏克蘭條約](../Category/烏克蘭條約.md "wikilink")
[Category:英國條約](../Category/英國條約.md "wikilink")
[Category:美國條約](../Category/美國條約.md "wikilink")

1.

2.
3.