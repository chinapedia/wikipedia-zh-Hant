[Tereshkova_heureka.jpg](https://zh.wikipedia.org/wiki/File:Tereshkova_heureka.jpg "fig:Tereshkova_heureka.jpg")
[Soviet_Union-1963-Stamp-0.10._Valentina_Tereshkova.jpg](https://zh.wikipedia.org/wiki/File:Soviet_Union-1963-Stamp-0.10._Valentina_Tereshkova.jpg "fig:Soviet_Union-1963-Stamp-0.10._Valentina_Tereshkova.jpg")的紀念郵票\]\]

**瓦莲京娜·弗拉基米罗芙娜·捷列什科娃**（，）是人类历史上进入[太空的第一位女性](../Page/太空.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")。1963年6月16日，她单独乘坐“[东方六号](../Page/东方六号.md "wikilink")”宇宙飞船进入太空。[苏联英雄](../Page/苏联英雄.md "wikilink")，[苏联空军少将](../Page/苏联空军.md "wikilink")\[1\]。她还是技术科学副博士，两次獲授[列宁勋章](../Page/列宁勋章.md "wikilink")；荣获[联合国和平金奖](../Page/联合国.md "wikilink")，以及世界许多国家授予的高级奖章，是世界上十几个城市的荣誉市民；月球背面的一座环形山以她的名字命名。目前捷列什科娃还在继续积极地从事社会活动，希望能夠参加[火星探险](../Page/火星.md "wikilink")\[2\]。在[2014年冬季奥运会](../Page/2014年冬季奥运会.md "wikilink")，她是开幕式上奥林匹克旗帜的旗手。

## 早年经历

捷列什科娃生于俄罗斯[雅罗斯拉夫尔州](../Page/雅罗斯拉夫尔州.md "wikilink")[图塔耶夫的一个村子](../Page/图塔耶夫.md "wikilink")。父亲弗拉基米尔·捷列什科夫（1912-1940）是拖拉机手，[苏芬战争中是坦克车长](../Page/苏芬战争.md "wikilink")、中士，在前线阵亡时捷列什科娃年仅2岁。母亲叶莲娜（1913-1987）是纺织厂女工。捷列什科娃有一个姐姐、一个弟弟。捷列什科娃于1945年8岁时在雅罗斯拉夫尔第32学校上学，1953年七年级毕业时为了补贴家用而离校，在当地的雅罗斯拉夫尔轮胎厂的组装与硫化车间当一名学徒工，负责操作切割机，同时在夜校学习青年工人的课程\[3\]。1955年4月，调入当地的“红色[彼列科普纺织厂](../Page/彼列科普.md "wikilink")，与其母亲、姐姐一起工作。1959年捷列什科娃加入了当地的业余航空俱乐部，成为一名跳伞员，1959年5月21日22岁时完成了首次空中跳伞，在当地总计完成了90次跳伞。1955年至1959年，接受了雅罗斯拉夫尔轻工业学院的[函授教育](../Page/函授教育.md "wikilink")。1960年8月11日，捷列什科娃成为[雅罗斯拉夫尔市](../Page/雅罗斯拉夫尔市.md "wikilink")的[共青团组织的书记](../Page/苏联共青团.md "wikilink")。

## 成为宇航员

1961年，[加加林飞上太空](../Page/加加林.md "wikilink")，捷列什科娃同众多少女一样，将[加加林作为了自己的偶像](../Page/加加林.md "wikilink")。她和航天俱乐部的女友们联名给莫斯科写了一封信，呼吁也派一名女性宇航员进入太空。料想不到，没过多久，这些在信上署名的姑娘都應邀前往[莫斯科](../Page/莫斯科.md "wikilink")。在莫斯科，还有来自全国各地的女性，所有人的目标都是：成为第一位女性宇航员。

考核是严格的，经过了三个月的各种类型的试验，有医学、体育、还有特殊使命方面的，经过层层筛选，幸运女神降临在了捷列什科娃的身上。报名条件包括：年龄30岁以下，有跳伞经历，身高170厘米以下，体重70千克以下。1962年2月16日，当听到自己的名字从400多竞争者中入选5名女宇航员之列时，捷列什科娃的心里顿时充满了无比的兴奋以及征服太空的信心。5位入选者應召入伍，从[列兵军衔开始](../Page/列兵.md "wikilink")。1962年3月加入[苏联共产党](../Page/苏联共产党.md "wikilink")。

成为宇航员的受训内容包括耐热训练：身穿飞行服耐受70摄氏度、30%湿度；静音训练，在无声环境中渡过10天；在乌米格-15上经受失重训练，每个架次飞行经受3-4次每次40秒的失重状态下写字、吃饭、使用无线电口播等；跳伞训练，包括水上跳伞。1962年11月9日，捷列什科娃完成了宇航员训练，成绩为优秀。

1963年3月，捷列什科娃入选為[东方六号飞船任务的第一顺位的宇航员](../Page/东方六号.md "wikilink")。

1963年6月16日，捷列什科娃乘坐东方六号宇航飞船升空，成为人类第一位进入太空的女性。也是迄今为止人类唯一一位独立驾驶宇航飞船完成太空飞行的女性。在3天（总计70小时50分钟）绕地球48圈的太空飞行中，捷列什科娃的无线电呼号是“海鸥”。捷列什科娃这次太空飞行时间比此前所有美国宇航员的太空飞行时间总和还要长。捷列什科娃驾驶的东方六号与[瓦列里·贝科夫斯基驾驶的](../Page/瓦列里·贝科夫斯基.md "wikilink")[东方五号执行了空间编队交会](../Page/东方五号.md "wikilink")、无线电通联等任务。由于东方号飞船上没有太空马桶不能排大便，所以3天飞行是禁食的。东方六号返回地面时降落在[阿尔泰边疆区](../Page/阿尔泰边疆区.md "wikilink")，坐标为
53.209375°N
80.80395°E。东方六号距地面7千米高度时，按程序捷列什科娃连同座椅弹射出飞船、单独伞降，至4千米高度时宇航员与座椅按程序分离。捷列什科娃落地时由于强风在切掉降落伞前，其鼻子在地面上擦伤。

完成航天飞行后，捷列什科娃成为宇航员指令长，直至1966年3月14日。

## 荣誉和奖励

捷列什科娃从太空返回后，被问及国家应如何奖赏她的英勇成就。捷列什科娃回答：希望能搜寻、确认她父亲牺牲的地点及战斗经过。这一请求得到了满足。苏联政府在捷列什科娃的父亲牺牲地建立了一座纪念牌。

政府奖励捷列什科娃一套在雅罗斯拉夫尔的3室的住宅。捷列什科娃与母亲、姨妈及其女儿在一起住了3年。此后，捷列什科娃作为公众人物，移居莫斯科。

1963年6月22日，成为[苏联英雄](../Page/苏联英雄.md "wikilink")。1963年6月22日与1981年5月6日，两次荣获[列宁勋章](../Page/列宁勋章.md "wikilink")。
1971年12月1日，获得[十月革命勋章](../Page/十月革命勋章.md "wikilink")。1987年3月5日因其社会活动获得[劳动红旗勋章](../Page/劳动红旗勋章.md "wikilink")。

1966年至1989年，[苏联最高苏维埃代表](../Page/苏联最高苏维埃.md "wikilink")。1974年至1989年，[苏联最高苏维埃主席团成员](../Page/苏联最高苏维埃主席团.md "wikilink")。1971年至1989年，苏联共产党中央委员会委员。

2008年至2011年，雅罗斯拉夫尔州杜马议员，[统一俄罗斯党党员](../Page/统一俄罗斯党.md "wikilink")。2011年，在雅罗斯拉夫尔州当选为俄罗斯联邦[国家杜马议员](../Page/国家杜马.md "wikilink")\[4\]。
2016年連任。

[Dmitry_Medvedev_12_April_2011-10.jpeg](https://zh.wikipedia.org/wiki/File:Dmitry_Medvedev_12_April_2011-10.jpeg "fig:Dmitry_Medvedev_12_April_2011-10.jpeg")从[俄国总统](../Page/俄国总统.md "wikilink")[德米特里·梅德韦杰夫](../Page/德米特里·梅德韦杰夫.md "wikilink"),于2011年4月12日在莫斯科[克里姆林宫](../Page/克里姆林宫.md "wikilink").\]\]

  - 2011年4月12日，在莫斯科[克里姆林宫](../Page/克里姆林宫.md "wikilink")，捷列什科娃接受从[俄国总统](../Page/俄国总统.md "wikilink")[梅德韦杰夫颁发的](../Page/德米特里·梅德韦杰夫.md "wikilink")[友谊勋章](../Page/友谊勋章.md "wikilink")。

## 家庭

[RIAN_archive_611957_Valentina_Tereshkova_and_Andrian_Nikolaev.jpg](https://zh.wikipedia.org/wiki/File:RIAN_archive_611957_Valentina_Tereshkova_and_Andrian_Nikolaev.jpg "fig:RIAN_archive_611957_Valentina_Tereshkova_and_Andrian_Nikolaev.jpg")

在[东方六号飞行之后谣言开始流传](../Page/东方六号.md "wikilink")，她可能嫁给（1929年-2004年），唯一的单身汉宇航员。

1963年11月3日与宇航员在莫斯科结婚。

1964年6月8日，女儿伊莉娜·安德烈雅诺夫娜·尼古拉耶娃出生\[5\]。这是人类第一个父母都是宇航员的人。外孙阿列克谢·马雅罗夫生于1996年，安德烈·罗迪奥诺夫生于2004年6月。

1982年与安德烈安·尼古拉耶夫的婚姻离异。

## 军衔

  - 1962年2月，入伍为列兵；
  - 1962年12月15日，中尉；
  - 1963年6月16日，上尉；
  - 1963年6月16日，大尉；
  - 1965年1月9日，少校；
  - 1967年10月14日，中校；
  - 1970年4月30日，上校；
  - 1975年，上校工程师；
  - 1995年，少将；
  - 1997年4月30日，退役。

捷列什科娃是苏联、俄罗斯历史上第一位女将军。

## 流行文化

  - 在航天模擬遊戲Kerbal Space Program中，有一個女性坎巴拉太空人名叫『Valentina』。以示對泰勒斯可娃的紀念。

## 參考文獻

  -
## 外部連結

  - [](http://www.cosmoworld.ru/spaceencyclopedia/tereshkova/index.shtml?tereshkova.html)

  - [](http://www.peoples.ru/military/cosmos/tereshkova/)
  - [](http://www.africana.ru/obschestva/RAMS/Tereshkova.htm)
  - [](http://www.izvestia.ru/news/news128392/)
  - [](http://www.astronaut.ru/bookcase/article/article111.htm?reload_coolmenus)
  - [](https://web.archive.org/web/20120603164429/http://angelina-tihonova.ru/category/valentina_tereshkova/)

  - [](http://www.duma.gov.ru/structure/deputies/131193/)

[Category:蘇聯英雄](../Category/蘇聯英雄.md "wikilink")
[Category:社會主義勞動英雄](../Category/社會主義勞動英雄.md "wikilink")
[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:勞動紅旗勳章獲得者](../Category/勞動紅旗勳章獲得者.md "wikilink")
[Category:蘇聯太空人](../Category/蘇聯太空人.md "wikilink")
[T](../Category/女太空人.md "wikilink")
[Category:蘇聯政治人物](../Category/蘇聯政治人物.md "wikilink")
[Category:蘇聯空軍將領](../Category/蘇聯空軍將領.md "wikilink")
[Category:女性空軍將領](../Category/女性空軍將領.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:白俄羅斯裔俄羅斯人](../Category/白俄羅斯裔俄羅斯人.md "wikilink")
[Category:雅羅斯拉夫爾州人](../Category/雅羅斯拉夫爾州人.md "wikilink")
[Category:太空人出身的政治人物](../Category/太空人出身的政治人物.md "wikilink")

1.
2.
3.  [Valentina
    Tereshkova](http://starchild.gsfc.nasa.gov/docs/StarChild/whos_who_level2/tereshkova.html).
    Starchild.gsfc.nasa.gov. Retrieved on 2013-03-04.
4.
5.  Feldman, Heather. *[Valentina Tereshkova: The First Woman in
    Space](http://books.google.com/books?id=r_LHO5_AzwQC&pg=PT24#v=onepage&q&f=false)*.
    The Rosen Publishing Group, 2003. ISBN 0-8239-6246-6