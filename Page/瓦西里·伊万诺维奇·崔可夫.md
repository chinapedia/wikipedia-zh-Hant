**瓦西里·伊万诺维奇·崔可夫**（又译**楚伊科夫**） （[俄语](../Page/俄语.md "wikilink")：,
），[苏联元帅](../Page/苏联元帅.md "wikilink")。

## 生平

崔可夫1900年2月12日出生于贫苦农民家庭，1917年在俄[波罗的海舰队当水兵](../Page/波罗的海舰队.md "wikilink")。[十月革命后加入](../Page/十月革命.md "wikilink")[红军](../Page/蘇聯紅軍.md "wikilink")。1919年加入[苏共](../Page/苏共.md "wikilink")。[苏联国内战争期间](../Page/苏联国内战争.md "wikilink")，转战南部战线、东部战线、西部战线，历任连长、团长，负过伤，并获得两枚红旗勋章。

1922年8月进入[伏龙芝军事学院第五期学习](../Page/伏龙芝军事学院.md "wikilink")，1925年8月毕业后继续在[伏龙芝军事学院东方系中国部深造](../Page/伏龙芝军事学院.md "wikilink")，在此期间学会了[汉语](../Page/汉语.md "wikilink")，1926年，崔可夫作为[使節首来中国](../Page/使節.md "wikilink")，到过[哈尔滨](../Page/哈尔滨.md "wikilink")、[奉天](../Page/奉天.md "wikilink")、[大连](../Page/大连.md "wikilink")、[天津](../Page/天津.md "wikilink")、[北京](../Page/北京.md "wikilink")。1927年秋天从伏龙芝军事学院东方系毕业后派赴[中国](../Page/中国.md "wikilink")[国民革命军部队中担任军事顾问](../Page/国民革命军.md "wikilink")，走遍了[华南](../Page/华南.md "wikilink")，到过[四川](../Page/四川.md "wikilink")，说一口流利的[華語](../Page/華語.md "wikilink")。1929年[中东路事件](../Page/中东路事件.md "wikilink")，在司令员布柳赫尔手下执行特别任务并做特别报告，处于所有情报的汇集中心。1929年11月19日，作为苏联远东军司令部全权代表，要求[满洲里守军第](../Page/满洲里.md "wikilink")15旅旅长[梁忠甲以下无条件投降](../Page/梁忠甲.md "wikilink")。

[大清洗期间](../Page/大清洗.md "wikilink")，崔可夫等年轻军官得以快速晉升，以填补那些被[整肅掉的高级将领留出的空位](../Page/整肅.md "wikilink")。1938年崔可夫已经是集团军司令，少将军衔。1939年9月崔可夫指挥第4集团军参加了入侵[波兰的行动](../Page/波兰第二共和国.md "wikilink")。在其后的[苏芬战争中崔可夫担任第](../Page/苏芬战争.md "wikilink")9集团军司令，但因作战不利被解职。

1940年12月，崔可夫因有出使[中国的经历](../Page/中国.md "wikilink")，且通晓[中文](../Page/中文.md "wikilink")，[斯大林再次選中出使](../Page/斯大林.md "wikilink")[中国](../Page/中国.md "wikilink")，担任驻华[武官](../Page/駐外武官.md "wikilink")，[苏联军事顾问团团长](../Page/苏联军事顾问团_\(抗日战争\).md "wikilink")、成为[蔣介石的军事顾问](../Page/蔣介石.md "wikilink")。在华期间，崔可夫除协助[中國抗日戰爭外](../Page/中國抗日戰爭.md "wikilink")，还大量收集了中日两国情报，并据此准确判断出[大日本帝国南进的可能性更大](../Page/大日本帝国.md "wikilink")。

[太平洋战争爆发后](../Page/太平洋战争.md "wikilink")，崔可夫请求回国参战，1942年2月下半月崔可夫结束了在[中国的使命飞回](../Page/中国.md "wikilink")[阿拉木图](../Page/阿拉木图.md "wikilink")，转乘火车前往古比雪夫。被任命为驻[图拉](../Page/图拉.md "wikilink")、[梁赞地区的预备第一集团军司令员](../Page/梁赞.md "wikilink")。5月该集团军改称第64集团军。7月率部开赴[斯大林格勒前线](../Page/斯大林格勒.md "wikilink")，9月奉派至第62军團担任司令，崔可夫指挥部队一次又一次地击退了数倍于己的德军进攻，完成了苏联最高统帅部“**不许后退一步**”的任务。崔可夫因在[斯大林格勒战役中的英勇表现获得](../Page/斯大林格勒战役.md "wikilink")“苏联英雄”称号。

1943年4月第62军團改编为第8親卫军團，崔可夫担任司令并指挥该军團参加了[库斯克會戰](../Page/库斯克會戰.md "wikilink")、[白俄羅斯戰役](../Page/巴格拉基昂行動.md "wikilink")、[明斯克战役](../Page/巴格拉基昂行動.md "wikilink")、[维斯瓦河—奥得河战役](../Page/維斯瓦河-奧德河攻勢.md "wikilink")、东波美拉尼亚攻势，直到[柏林战役](../Page/柏林战役.md "wikilink")。其间崔可夫又一次获得“苏联英雄”称号，并晋升为上将军衔。

战后，崔可夫先后担任[驻德苏军集群第一副总司令](../Page/驻德苏军集群.md "wikilink")，1948年11月12日晋升[大将军衔](../Page/大将_\(蘇聯\).md "wikilink")。驻德苏军集群总司令（1949年—1953年）。[基辅军区司令员](../Page/基辅军区.md "wikilink")（1953年—1960年）。1955年3月11日晋升[苏联元帅军衔](../Page/苏联元帅.md "wikilink")。苏联国防部副部长兼[苏联陆军总司令](../Page/苏联陆军.md "wikilink")（1960年—1964年）。苏联民防总司令（1961年—1972年）。1961年起直至逝世，为[苏共中央委员](../Page/蘇聯共產黨中央委員會.md "wikilink")。

崔可夫元帅著作颇多，主要有《集体英雄主义的集团军》、《战火中的180天》、《空前的功绩》、《在乌克兰的战斗》、《斯大林格勒近卫军西进》、《在战火中锤炼青春》、《第三帝国的末日》、《本世纪之战》、《在华使命
蒋介石的苏联军事顾问的回忆》、《斯大林格勒∶经验与教训》、《从斯大林格勒到柏林》等。

[Category:蘇聯英雄](../Category/蘇聯英雄.md "wikilink")
[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:十月革命勳章獲得者](../Category/十月革命勳章獲得者.md "wikilink")
[Category:紅旗勳章獲得者](../Category/紅旗勳章獲得者.md "wikilink")
[Category:蘇沃洛夫勳章獲得者](../Category/蘇沃洛夫勳章獲得者.md "wikilink")
[Category:紅星勳章獲得者](../Category/紅星勳章獲得者.md "wikilink")
[Category:蘇聯元帥](../Category/蘇聯元帥.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:蘇聯第二次世界大戰人物](../Category/蘇聯第二次世界大戰人物.md "wikilink")
[Category:蘇聯中日戰爭人物](../Category/蘇聯中日戰爭人物.md "wikilink")
[Category:俄國內戰人物](../Category/俄國內戰人物.md "wikilink")
[Category:在中华民国的苏联人](../Category/在中华民国的苏联人.md "wikilink")
[Category:伏龍芝軍事學院校友](../Category/伏龍芝軍事學院校友.md "wikilink")
[Category:圖拉州人](../Category/圖拉州人.md "wikilink")
[Category:俄羅斯駐外武官](../Category/俄羅斯駐外武官.md "wikilink")