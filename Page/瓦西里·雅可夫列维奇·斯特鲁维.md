**瓦西里·雅可夫列维奇·斯特鲁维**（，），德語名**弗里德里希·格奧爾格·威廉·馮·斯特魯維**（），[俄国天文学家](../Page/俄国.md "wikilink")，[波羅的海德國人](../Page/波羅的海德國人.md "wikilink")，生於[阿通納](../Page/阿通納.md "wikilink")（今[漢堡市的一個區](../Page/漢堡.md "wikilink")）。[普尔科沃天文台的创建者和首任台长](../Page/普尔科沃天文台.md "wikilink")。他的贡献主要在[天体测量学和](../Page/天体测量学.md "wikilink")[恒星天文学方面](../Page/恒星天文学.md "wikilink")。他的一个儿子，两个孙子、两个曾孙和一個玄孫也都是[天文学家](../Page/天文学家.md "wikilink")。

## 參見

  - [斯特魯維家族](../Page/斯特魯維家族.md "wikilink")
  - [斯特魯維測地弧](../Page/斯特魯維測地弧.md "wikilink")

## 參考資料

[S](../Category/俄国天文学家.md "wikilink")
[S](../Category/德国天文学家.md "wikilink")
[S](../Category/德國裔俄羅斯人.md "wikilink")
[S](../Category/英國皇家天文學會金質獎章獲得者.md "wikilink")
[S](../Category/英國皇家學會院士.md "wikilink")
[S](../Category/瑞典皇家科學院院士.md "wikilink")
[S](../Category/皇家獎章獲得者.md "wikilink")
[S](../Category/塔爾圖大學校友.md "wikilink")
[S](../Category/漢堡人.md "wikilink")
[S](../Category/波罗的海德意志人.md "wikilink")