[Mail_in_the_bottle.jpg](https://zh.wikipedia.org/wiki/File:Mail_in_the_bottle.jpg "fig:Mail_in_the_bottle.jpg")
[Message_in_a_bottle_start.JPG](https://zh.wikipedia.org/wiki/File:Message_in_a_bottle_start.JPG "fig:Message_in_a_bottle_start.JPG"),
2011)\]\]
**瓶中信**，是一種[通信的形式](../Page/通信.md "wikilink")。將[訊息](../Page/訊息.md "wikilink")（可以是一封[信](../Page/书信.md "wikilink")、一張[名片](../Page/名片.md "wikilink")，更可以是古靈精怪的物品）放入密封的[容器](../Page/容器.md "wikilink")（通常是一瓶[玻璃瓶](../Page/玻璃.md "wikilink")，[塑膠製也有](../Page/塑膠.md "wikilink")），投入[海中](../Page/海.md "wikilink")、[洋中](../Page/洋.md "wikilink")，或收藏在隱蔽的地方（類似[時間囊](../Page/時間囊.md "wikilink")）藉以將訊息傳到未知的對象。

瓶中信的訊息接收者並沒有一個特定對象，訊息亦可能隨著[洋流流動直到永遠](../Page/洋流.md "wikilink")。瓶中信常被人們[聯想為困](../Page/聯想.md "wikilink")[荒島的遇難者之求救信號](../Page/荒島.md "wikilink")。然而，也有許多人認為發布瓶中信是一種[樂趣](../Page/樂趣.md "wikilink")，可以[交流遠方的消息和交新的](../Page/沟通.md "wikilink")[朋友](../Page/朋友.md "wikilink")。不過，投擲瓶中信同時，要先考慮承受觸犯亂丟[垃圾罪名而帶來的](../Page/垃圾.md "wikilink")[罰款和](../Page/罰款.md "wikilink")[刑期之風險](../Page/監獄.md "wikilink")。

瓶中信也為[科學家帶來](../Page/科學家.md "wikilink")[研究洋流的重要資料](../Page/研究.md "wikilink")。「瓶中信」一詞，成為[諺語](../Page/諺語.md "wikilink")，意思為不意欲送到特定[目的地的信息](../Page/目的地.md "wikilink")。

## 歷史

第一次記錄瓶中信的記載，是公元前310年，[古希臘](../Page/古希臘.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[泰奧弗拉斯托斯將放有訊息的](../Page/泰奧弗拉斯托斯.md "wikilink")[瓶子擲到海中](../Page/花瓶.md "wikilink")，用以研究[地中海是否由](../Page/地中海.md "wikilink")[大西洋的海](../Page/大西洋.md "wikilink")[水流入形成](../Page/水.md "wikilink")。

當[克里斯多福·哥倫布發現](../Page/克里斯多福·哥倫布.md "wikilink")[新世界之後回程到](../Page/新世界.md "wikilink")[西班牙途中](../Page/西班牙.md "wikilink")，遇到一場劇烈[風暴](../Page/風暴.md "wikilink")。他認為自己很可能會被[海洋吞噬](../Page/海洋.md "wikilink")，於是把他的[筆記](../Page/筆記.md "wikilink")、向[西班牙女王](../Page/西班牙君主列表.md "wikilink")[伊莎貝拉一世交待的發現報告一起放在一桶密封的](../Page/伊莎貝拉一世.md "wikilink")[酒桶中投擲入海](../Page/酒桶.md "wikilink")。他的桶中文件大約300年後被發現。

16世紀的[英國海軍將瓶中信設入其他用途](../Page/英國海軍.md "wikilink")，他們將敵軍位置用瓶中信的方式送到岸上。[英國女王](../Page/英國君主列表.md "wikilink")[伊莉莎白一世甚至下御令](../Page/伊莉莎白一世.md "wikilink")，任何人打開「封塞之海中瓶」，必被處以[死刑](../Page/死刑.md "wikilink")。\[1\]

1991年8月，[蘇共中的保守派發動了一場](../Page/蘇共.md "wikilink")[政變](../Page/八一九事件.md "wikilink")，軟禁了當時正在[黑海畔渡假的](../Page/黑海.md "wikilink")[蘇聯總統](../Page/蘇聯.md "wikilink")[戈巴契夫](../Page/戈巴契夫.md "wikilink")，當時戈爾巴喬夫亦有向黑海投了一封瓶中信。

## 類似瓶中信的媒介

[The_Sounds_of_Earth_Record_Cover_-_GPN-2000-001978.jpg](https://zh.wikipedia.org/wiki/File:The_Sounds_of_Earth_Record_Cover_-_GPN-2000-001978.jpg "fig:The_Sounds_of_Earth_Record_Cover_-_GPN-2000-001978.jpg")，漂流在宇宙間的另類瓶中信。\]\]

[氣球郵件是利用氣球傳送信息到沒有明確目的地的方法](../Page/氣球郵件.md "wikilink")，類似瓶中信。氣球郵件的好處是[地球上任何地方都可以放出和接收到氣球郵件的信息](../Page/地球.md "wikilink")。更好之處，是氣球郵件非常容易地被放出，也不必冒上海浪將信息洗走之風險。

[美國太空總署也利用類似瓶中信的媒介發射了幾則跨](../Page/美國太空總署.md "wikilink")[星訊息](../Page/星.md "wikilink")。他們使用了一碟9[英吋乘](../Page/英吋.md "wikilink")6英吋的鍍[金](../Page/金.md "wikilink")[鋁板](../Page/鋁.md "wikilink")，通稱作[先驅者鍍金鋁板](../Page/先驅者鍍金鋁板.md "wikilink")，第一塊鍍金鋁板隨著[先驅者10號於](../Page/先驅者10號.md "wikilink")1972年3月2日被發射上[太空](../Page/太空.md "wikilink")，而第二塊則隨[先驅者11號於](../Page/先驅者11號.md "wikilink")1973年4月5日被發射到太空去。

1977年[夏天](../Page/夏天.md "wikilink")，美國[太空總署](../Page/太空總署.md "wikilink")[旅行者號計劃發射了兩艘](../Page/旅行者號計劃.md "wikilink")[太空船](../Page/太空船.md "wikilink")。太空船每艘都運載12英寸鍍金的[銅盤](../Page/銅.md "wikilink")，通稱作[旅行者金唱片](../Page/旅行者金唱片.md "wikilink")，[記錄包含代表地球上](../Page/記錄.md "wikilink")[人類](../Page/人類.md "wikilink")[文化和](../Page/文化.md "wikilink")[生活的](../Page/生活.md "wikilink")[聲音及](../Page/聲音.md "wikilink")[圖像](../Page/圖像.md "wikilink")。

## 現代使用

現代人雖然可以用[電話](../Page/電話.md "wikilink")、[郵件](../Page/郵件.md "wikilink")、[電子郵件](../Page/電子郵件.md "wikilink")、[即時通訊軟體等等的方法聯繫](../Page/即時通訊.md "wikilink")，交流互動。但瓶中信這種充滿未知、神秘魅力的通訊方法，依然吸引現代人採用，甚至發掘更多用處。瓶中信不單單帶來[友情](../Page/友情.md "wikilink")、[愛情](../Page/愛情.md "wikilink")、異國交流，或會帶來[拯救](../Page/拯救.md "wikilink")；或會帶來斥責亂丟垃圾之回函；或會帶來更多的[謎團](../Page/謎團.md "wikilink")。

1904年至1906年間，英國[普利茅斯海洋生物協會長](../Page/普利茅斯海洋生物協會.md "wikilink") George
Parker Bidder
為了觀測[北海](../Page/北海_\(大西洋\).md "wikilink")[洋流流動方向進行實驗](../Page/洋流.md "wikilink")，投擲了
1020
個瓶中信，裡面有一張明信片及一份問卷，上頭用英語、德語以及荷蘭語三種語言寫著希望撿到瓶子的人可以寫下發現地點及時間。2015年，一對夫婦在德國北部海岸的阿羅姆羅島（Amrum）度假時，發現了其中一個瓶中信，這可能會打破[金氏世界紀錄](../Page/金氏世界紀錄.md "wikilink")，成為最古老的瓶中信\[2\]\[3\]。

1941年，兩名鐵匠[西奧多·傑克武尼和](../Page/西奧多·傑克武尼.md "wikilink")[埃米爾·高德特](../Page/埃米爾·高德特.md "wikilink")，把一封瓶中信放入美國[羅得島州](../Page/羅得島州.md "wikilink")[北金斯敦的](../Page/北金斯敦.md "wikilink")[匡西特海軍航空站一堵牆內](../Page/匡西特海軍航空站.md "wikilink")，直到六十年後重見天日，但人們依然不理解兩人之動機。\[4\]

1963年，一個10歲的英國女孩[安妮·瑞維特自](../Page/安妮·瑞維特.md "wikilink")[法國發出一封內有自己住宅地址的瓶中信](../Page/法國.md "wikilink")，由另一名[荷蘭](../Page/荷蘭.md "wikilink")[諾德韋克海灘的男孩](../Page/諾德韋克海灘.md "wikilink")[尼爾斯·埃爾菲斯拾到](../Page/尼爾斯·埃爾菲斯.md "wikilink")，後來兩人聯絡，發展友誼，最後成了[夫妻](../Page/夫妻.md "wikilink")。\[5\]

1990年代初期，美國海洋學家[克堤斯·埃貝斯邁爾在朋友家發現一個來自台灣的瓶中信](../Page/克堤斯·埃貝斯邁爾.md "wikilink")，內容是台灣人聲援當時被判刑的中國知名異議人士[魏京生](../Page/魏京生.md "wikilink")\[6\]。

在[日本](../Page/日本.md "wikilink")，非常多[學生使用瓶中信方式進行交流](../Page/學生.md "wikilink")。1997年11月13日，位於琉球群島的日本[沖繩縣那霸市](../Page/沖繩縣.md "wikilink")[真和志小學](http://www.nahaken-okn.ed.jp/mawas-es/)為慶祝創校一百一十六年，舉辦了瓶中信祝福活動。半年之後，其中一名叫綾乃的女學生之瓶中信，漂流到[台灣](../Page/台灣.md "wikilink")[臺東縣](../Page/臺東縣.md "wikilink")[長濱鄉](../Page/長濱鄉.md "wikilink")，由一名老翁[劉桂魁拾獲](../Page/劉桂魁.md "wikilink")，其後兩人見面，成為忘年之交，劉桂魁並收了綾乃為乾孫女。\[7\]

此外，日本亦有專門發放瓶中信的[組織](../Page/组织_\(社会学\).md "wikilink")[黑潮物語元氣之子會](../Page/黑潮物語元氣之子會.md "wikilink")，該會成立於1998年，專收集來自日本海內外的[信件或](../Page/信件.md "wikilink")[繪圖](../Page/繪圖.md "wikilink")，然後在[沖繩等地放流](../Page/沖繩.md "wikilink")。2006年2月1日，[農曆新年期間](../Page/農曆新年.md "wikilink")，其中一封該會發放的瓶中信乘住[黑潮漂流到台灣](../Page/黑潮.md "wikilink")[台東縣](../Page/台東縣.md "wikilink")[成功鎮](../Page/成功鎮.md "wikilink")[三仙台](../Page/三仙台.md "wikilink")，被[中華搜救總隊隊員](../Page/中華搜救總隊.md "wikilink")[李訓旺拾獲](../Page/李訓旺.md "wikilink")，李訓旺後來遠赴日本，推動兩地交流。\[8\]

2005年[7月](../Page/2005年7月.md "wikilink")，英國女孩亞麗莎將她的照片及住址塞入一個玻璃瓶，然後將瓶中信從英國[蘭開夏海邊丟入大海](../Page/蘭開夏.md "wikilink")。47天後，亞麗莎收到來自[紐西蘭的男童伯斯回信](../Page/紐西蘭.md "wikilink")，表示在他住家附近的一个[船塢中發現這個瓶中信](../Page/船塢.md "wikilink")。這樣的消息令科學家嘖嘖稱奇，瓶中信漂流的速度比郵輪慢了不過7天，而且瓶中信要經[大西洋](../Page/大西洋.md "wikilink")，繞過非洲南端再到紐西蘭，或是用比較快的方式，穿過[美洲](../Page/美洲.md "wikilink")[巴拿馬運河](../Page/巴拿馬運河.md "wikilink")，一路衝到紐西蘭。有科學家表示，可能有人從中將瓶中信撿起來，再丟到紐西蘭外海，但為何要這樣做，則是未解之謎。\[9\]\[10\]

有時瓶中信也可作另類用法，2005年5月24日，英國利物浦[市議會為](../Page/市議會.md "wikilink")[老人和一些經常患](../Page/老人.md "wikilink")[病的人想出了一個對付](../Page/病.md "wikilink")[緊急情況的好辦法](../Page/緊急情況.md "wikilink")。市議會[鼓勵人們應該將一些非常重要的個人](../Page/激勵.md "wikilink")[醫療資訊寫成一封瓶中信保存在](../Page/醫療.md "wikilink")[冰箱裏](../Page/冰箱.md "wikilink")，以便在遭遇突發疾病時能夠迅速找到，為挽救[生命爭取時間](../Page/生命.md "wikilink")。\[11\]

瓶中信為大眾帶來樂趣，同時亦帶來拯救。[2005年5月](../Page/2005年5月.md "wikilink")，來自[厄瓜多爾和](../Page/厄瓜多爾.md "wikilink")[秘魯的](../Page/秘魯.md "wikilink")86名[偷渡客](../Page/偷渡.md "wikilink")，打算偷渡到[瓜地馬拉](../Page/瓜地馬拉.md "wikilink")，然後越過邊境進入[墨西哥](../Page/墨西哥.md "wikilink")。但[人蛇集團為免被查緝人員發現](../Page/人蛇.md "wikilink")，將他們遺棄。結果他們在[哥斯大黎加](../Page/哥斯大黎加.md "wikilink")600公里的[可可斯島海域發生船難](../Page/可可斯島.md "wikilink")，於是發出瓶中信求救。漂流三日後，瓶中信被哥斯大黎加[海洋保育組織人員發現](../Page/海洋保育.md "wikilink")，86名偷渡客因而獲救。\[12\]\[13\]

然而，某些人對瓶中信通訊方式沒這樣歡迎。[2005年8月](../Page/2005年8月.md "wikilink")，住在紐約[長島的](../Page/長島.md "wikilink")[美國海岸防衛隊隊長班奈特發出瓶中信](../Page/美國海岸防衛隊.md "wikilink")，這封瓶中信漂越大西洋，歷經將近4千8百多公里後到達英國[多塞特郡的](../Page/多塞特郡.md "wikilink")[普利港](../Page/普利港.md "wikilink")，被一名住在[伯恩茅斯](../Page/伯恩茅斯.md "wikilink")，叫[亨利·比格爾斯伍的人撿到](../Page/亨利·比格爾斯伍.md "wikilink")。亨利·比格爾斯伍回函訓斥班奈特的行為，並在信中表示：

## 其他

[尼古拉斯·史柏斯之](../Page/尼古拉斯·史柏斯.md "wikilink")[小說](../Page/小說.md "wikilink")《[瓶中信](../Page/瓶中信_\(小說\).md "wikilink")》，後被改拍成美國[電影](../Page/電影.md "wikilink")《[瓶中信](../Page/瓶中信_\(電影\).md "wikilink")》。

## 參考資料

<references />

[Category:通信方式](../Category/通信方式.md "wikilink")
[Category:傳播](../Category/傳播.md "wikilink")
[Category:海洋学](../Category/海洋学.md "wikilink")

1.
2.  [MBA sent what is probably oldest message in a bottle ever
    found\!](http://www.mba.ac.uk/2015/08/25/mba-sent-what-is-probably-oldest-message-in-a-bottle-ever-found/)

3.  [古老瓶中信，帶你一窺 108
    年前的洋流探測科學](http://technews.tw/2015/08/26/message-in-a-bottle-amrum/)
4.  美國：60年瓶中信 答案一朝現
    <http://news.xinhuanet.com/world/2006-09/05/content_5052157.htm>
5.  男孩女孩借漂流瓶10歲结識 瓶中信促成傳奇姻缘
    <http://abroad.163.com/editor/strangenews/050111/050111_34746.html>
6.  [聲援魏京生
    台灣瓶中信漂10年](http://news.rti.org.tw/index_newsContent.aspx?nid=245629)
7.  瓶中信 忘年交 <http://www.tzuchi.com.tw/file/Hospmagazine/8907/12.htm>
8.  瓶中信搭黑潮下花蓮 台日的收發信人喜相逢
    <http://www.epochtimes.com/b5/6/3/10/n1250356.htm>
9.  瓶中信漂9000英哩 英女童拋澳男童撿
    <http://www.epochtimes.com/b5/6/1/20/n1196755.htm>
10. 比郵輪還快？　 瓶中信47天飄流3萬公里
    <http://www.ettoday.com/2006/10/15/91-2003443.htm>
11. 備個“瓶中信” 放入冰箱能救命
    <http://news.xinhuanet.com/world/2005-05/25/content_3001535.htm>
12. 「請救救我們！」　８６偷渡客瓶中信求救 <http://tw.epochtimes.com/bt/5/6/1/n939983.htm>
13.