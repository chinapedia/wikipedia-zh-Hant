[SugarcaneYield.png](https://zh.wikipedia.org/wiki/File:SugarcaneYield.png "fig:SugarcaneYield.png")

**甘蔗**是[禾本科的](../Page/禾本科.md "wikilink")[单子叶植物](../Page/单子叶植物.md "wikilink")，为**甘蔗属**（[学名](../Page/学名.md "wikilink")：）的总称。甘蔗是温带和热带农作物，是制造[蔗糖的原料](../Page/蔗糖.md "wikilink")，且可提煉[乙醇作為](../Page/乙醇.md "wikilink")[生質能源](../Page/生質能源.md "wikilink")。全世界有一百多个国家出产甘蔗，最大的甘蔗生产国是[巴西](../Page/巴西.md "wikilink")、[印度和](../Page/印度.md "wikilink")[中国](../Page/中国.md "wikilink")。中国最常见的食用甘蔗为中国竹蔗。

## 分类

本属属于[黍亚科](../Page/黍亚科.md "wikilink")[鬚芒草總族](../Page/鬚芒草總族.md "wikilink")[鬚芒草族](../Page/鬚芒草族.md "wikilink")[甘蔗亚族](../Page/甘蔗亚族.md "wikilink")。

### 物种

本属已确认的物种至少有36个，如下：

  - *Saccharum alopecuroides*
  - *Saccharum angustifolium*
  - [斑茅](../Page/斑茅.md "wikilink") *Saccharum arundinaceum*
  - *Saccharum asperum*
  - *Saccharum beccarii*
  - [孟加拉甘蔗](../Page/孟加拉甘蔗.md "wikilink") *Saccharum bengalense*
  - *Saccharum brevibarbe*
  - *Saccharum coarctatum*
  - *Saccharum contortum*
  - *Saccharum fallax*
  - *Saccharum filifolium*
  - [台蔗茅](../Page/台蔗茅.md "wikilink") *Saccharum formosanum*
  - *Saccharum giganteum*
  - *Saccharum griffithii*
  - *Saccharum hildebrandtii*
  - *Saccharum kajkaiense*
  - *Saccharum kanashiroi*
  - *Saccharum longisetosum*
  - *Saccharum macrantherum*
  - *Saccharum maximum*
  - *Saccharum narenga*
  - [秀貴甘蔗](../Page/秀貴甘蔗.md "wikilink") *Saccharum officinarum*
  - *Saccharum perrieri*
  - [狭叶斑茅](../Page/狭叶斑茅.md "wikilink") *Saccharum procerum*
  - [沙生蔗茅](../Page/沙生蔗茅.md "wikilink") *Saccharum ravennae*
  - [新畿内亚野生蔗](../Page/新畿内亚野生蔗.md "wikilink")*Saccharum robustum*
  - *Saccharum rufipilum*
  - *Saccharum sikkimense*
  - [中國竹蔗](../Page/中國竹蔗.md "wikilink") *Saccharum sinense*
  - [割手密](../Page/割手密.md "wikilink") *Saccharum spontaneum*
  - *Saccharum stewartii* 
  - *Saccharum strictum*
  - *Saccharum viguieri*
  - *Saccharum villosum*
  - *Saccharum wardii*
  - *Saccharum williamsii*

## 傳播歷史

[1962-07_1962年_甘蔗制糖厂.jpg](https://zh.wikipedia.org/wiki/File:1962-07_1962年_甘蔗制糖厂.jpg "fig:1962-07_1962年_甘蔗制糖厂.jpg")
甘蔗起源问题现在认识不一，有多种说法。一说起源于中国，中国华南、西南南部一带可能是甘蔗的原产地之一。其主要论据是：

1.  于公元前4世纪末，中国语系中早有“柘”或“薯柘”一词，后来“柘”字衍作“蔗”。
2.  甘蔗的命名，纯粹是按中国驯化植物的习惯来命名的，没有加上引进植物的“胡”、“番”、“洋”和外来地名，也不译音。
3.  甘蔗的近缘植物在中国分布很广，类型也相当多，甘蔗亚族中9个属植物都有。主要的甘蔗属割手密，北自秦岭
    ，南至海南岛都有分布。尤其是华南、西南南部一带分布最多。
4.  澳大利亚的科学工作者分析了甘蔗的异构酶和黄酮类化合物，判断甘蔗物种间亲缘关系，准确的追溯到甘蔗的祖先是甘蔗属的[割手密](../Page/割手密.md "wikilink")（學名：）和芒属的[五节芒](../Page/五节芒.md "wikilink")（學名：）中国南部、西南部，是世界上甘蔗野生种割手密和五节芒的一个重要分布中心。

由此推论，中国华南、西南南部一带为甘蔗的原产地之一，甘蔗属的中国种（），可以肯定是起源于中国。

另一种说法為甘蔗原产地可能是[新畿内亞](../Page/新畿内亞.md "wikilink")（[新畿内亚野生蔗](../Page/新畿内亚野生蔗.md "wikilink")）或[印度](../Page/印度.md "wikilink")，后来传播到南洋群岛，大约在周朝周宣王时传入中国南方。先秦时代的“柘”就是甘蔗，到了[汉代才出现](../Page/汉代.md "wikilink")“蔗”字\[1\]，“柘”和“蔗”的读音可能来自梵文sakara。10到13世纪（[宋代](../Page/宋代.md "wikilink")），江南各省普遍种植甘蔗；中南半岛和南洋各地如[真腊](../Page/真腊.md "wikilink")、[占城](../Page/占城.md "wikilink")、[三佛齐](../Page/三佛齐.md "wikilink")、[苏吉丹也普遍种甘蔗制糖](../Page/苏吉丹.md "wikilink")。

[亚历山大大帝东征](../Page/亚历山大大帝.md "wikilink")[印度时](../Page/印度.md "wikilink")，部下一位将领曾说印度出产一种不需[蜜蜂就能产生](../Page/蜜蜂.md "wikilink")[蜜糖的草](../Page/蜜糖.md "wikilink")。公元6世纪[伊朗](../Page/伊朗.md "wikilink")[萨珊王朝国王](../Page/萨珊王朝.md "wikilink")[霍斯劳一世](../Page/霍斯劳一世.md "wikilink")（Khosrau
I
Anushirvan）将甘蔗引入伊朗种植。8到10世纪甘蔗的种植遍及[伊拉克](../Page/伊拉克.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[西西里](../Page/西西里.md "wikilink")、[伊比利亚半岛等地](../Page/伊比利亚半岛.md "wikilink")。后来葡萄牙和西班牙殖民者又把甘蔗带到了美洲。

## 臺灣甘蔗農業

在[日據時代到](../Page/台灣日據時期.md "wikilink")[光復初期](../Page/臺灣光復.md "wikilink")，甘蔗為臺灣最重要的經濟作物之一。臺灣一年四季皆適合甘蔗生長，惟7-9月間常有颱風來襲，對甘蔗生長有不良影響。依據陳正祥氏的說法，臺灣最適合種植甘蔗的地區為[雲林縣](../Page/雲林縣.md "wikilink")[虎尾鎮到](../Page/虎尾鎮.md "wikilink")[屏東縣的](../Page/屏東縣.md "wikilink")[平原](../Page/平原.md "wikilink")，因本區地形平坦，氣候良好。臺灣常見的經濟作物甘蔗有兩種：

  - [中國竹蔗](../Page/中國竹蔗.md "wikilink")（）：即中文傳統上的甘蔗，俗稱「白甘蔗」\[2\]，外皮綠色，質地粗硬，不適合生吃，產量多，含糖量高。
  - [秀貴甘蔗](../Page/秀貴甘蔗.md "wikilink")（）：俗稱「紅甘蔗」\[3\]，皮墨紅色，莖肉富纖維質多汁液，清甜嫩脆，食而不膩。

另外還有：

  - [新幾內亞野生種甘蔗](../Page/新幾內亞野生種甘蔗.md "wikilink")：特徵為葉片亦為紫色，因糖分低，多不用來製糖，亦不食用。先民以其抗病力強，遂以訛傳訛的成為中藥材。在臺灣品種日漸退化，莖細長，反而不太像甘蔗，甚至於被誤認為茅草
    。
  - [印度蔗](../Page/印度蔗.md "wikilink")（）
  - 野生甘蔗

## 甘蔗的營養

[Cut_sugarcane.jpg](https://zh.wikipedia.org/wiki/File:Cut_sugarcane.jpg "fig:Cut_sugarcane.jpg")
《[本草綱目](../Page/本草綱目.md "wikilink")》記載甘蔗能夠止咳化痰、利尿、養顏美容。\[4\]主治：

1.  发热口干、小便赤涩。取甘蔗去皮，嚼汁咽下。饮浆亦可。
2.  反胃吐食。用甘蔗汁七升、生姜汁一升，和匀，每日细细饮服。
3.  干呕不息。有蔗汁温服半升，每日三次。加姜汁更好。
4.  虚热咳嗽，口干涕唾。用甘蔗汁一程式半、青粱米四合，煮粥吃。
5.  每日二次。极润心肺。

## 参考文献

## 参见

  - [甘蔗汁](../Page/甘蔗汁.md "wikilink")
  - [蘭姆酒](../Page/蘭姆酒.md "wikilink")
  - [蔗蝦](../Page/蔗蝦.md "wikilink")
  - [製糖](../Page/製糖.md "wikilink")

## 外部連結

  - 季羡林：〈[清代的甘蔗种植和制糖术](http://www.nssd.org/articles/article_read.aspx?id=1003368932)〉。
  - [甘蔗
    Ganzhe](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01119)
    藥用植物圖像資料庫 (香港浸會大學中醫藥學院)

[甘蔗属](../Category/甘蔗属.md "wikilink")
[Category:能源作物](../Category/能源作物.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")
[Category:经济作物](../Category/经济作物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  [东汉](../Page/东汉.md "wikilink")[杨孚](../Page/杨孚.md "wikilink")《[异物志](../Page/异物志.md "wikilink")》：“甘蔗，远近皆有。交趾所产，特好，本末无厚薄，其味甘”
2.  [甘蔗](http://plant.tesri.gov.tw/plant100/WebPlantDetail.aspx?tno=628108030)，臺灣野生植物資料庫
3.  [秀貴甘蔗](http://plant.tesri.gov.tw/plant100/WebPlantDetail.aspx?tno=628108020)，臺灣野生植物資料庫
4.  《本草綱目》 甘蔗 释名 名竿蔗、遮。 气味 （蔗）甘、平、涩、无毒。