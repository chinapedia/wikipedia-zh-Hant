**甘露子**（[学名](../Page/学名.md "wikilink")：**），又名**宝塔菜**、**螺丝菜**、**草石蠶**、**地环儿**、地蚕、地牯牛、地钮、旱螺蛳、罗汉菜、益母膏、米累累、地母、地蕊、地蚕、螺丝钻、螺蛳菜等。在中日文资料中常误从[本草纲目及](../Page/本草纲目.md "wikilink")[植物名实图考作](../Page/植物名实图考.md "wikilink")**草石蚕**，非是。为[唇形科](../Page/唇形科.md "wikilink")[水苏属](../Page/水苏属.md "wikilink")[宿根植物](../Page/宿根.md "wikilink")。喜生于近水低湿地，原产地是[中国](../Page/中国.md "wikilink")。

## 形态

多年生[草本](../Page/草本.md "wikilink")；地下有匍匐枝，成熟时顶端膨大成螺旋状的直立方形肉质块茎，卵形至长椭圆状卵形叶子叶子对生；夏季开淡紫色唇形花，轮伞花序生花6朵至多数，由2－13轮在茎顶列成穗状花序。
[Crosnes.jpg](https://zh.wikipedia.org/wiki/File:Crosnes.jpg "fig:Crosnes.jpg")
[Stachys_affinis_dyed_red.jpg](https://zh.wikipedia.org/wiki/File:Stachys_affinis_dyed_red.jpg "fig:Stachys_affinis_dyed_red.jpg")中染红的宝塔菜和煮熟的黑豆\]\]

## 用途

宝塔菜的地下根茎洁白，质地脆嫩无纤维，可腌制成[酱菜](../Page/酱菜.md "wikilink")，也可凉拌食用，还是提取[水苏糖的原料](../Page/水苏糖.md "wikilink")。宝塔菜是[扬州酱菜中的著名品种](../Page/扬州酱菜.md "wikilink")。

## 外部链接

  -
  -
  - [植物通](http://www.plant.ac.cn/latin/Labiatae/Stachys-sieboldi-Miq.htm)

  - [中国植物志 第66卷：甘露子 Stachys sieboldii
    Miq.](http://db.kib.ac.cn/eflora/view/search/chs_contents.aspx?CPNI=CPNI-256-40549)

[Category:水苏属](../Category/水苏属.md "wikilink")
[Category:根菜類](../Category/根菜類.md "wikilink")