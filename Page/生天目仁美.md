**生天目仁美**（，），[日本女性](../Page/日本.md "wikilink")[配音員](../Page/配音員.md "wikilink")、[演員](../Page/演員.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市出身](../Page/橫濱市.md "wikilink")\[1\]\[2\]（在[新瀉縣](../Page/新瀉縣.md "wikilink")[佐渡市出生](../Page/佐渡市.md "wikilink")）。身高163cm。B型[血](../Page/血型.md "wikilink")\[3\]。

## 簡介

[賢Production所屬](../Page/賢Production.md "wikilink")\[4\]，時期（1998年4月加入，2002年3月退團）\[5\]以演員身份曾參加電視劇拍攝和進行舞台表演。專門學校東京Conservatoire尚美（現在改名）畢業。

她為人熟悉的代表配音作品有動畫《[DokiDoki！光之美少女](../Page/DokiDoki!_光之美少女.md "wikilink")》主角相田愛／愛心天使、《[藥師寺涼子怪奇事件簿](../Page/藥師寺涼子怪奇事件簿.md "wikilink")》主角藥師寺涼子、《[無敵看板娘](../Page/無敵看板娘.md "wikilink")》主角鬼丸美樹、《[女高中生
GIRLS-HIGH](../Page/女子高生.md "wikilink")》主角高橋繪-{里}-子、《[真月譚
月姬](../Page/月姬.md "wikilink")》女主角愛爾奎特·布倫史塔德、《[愛的魔法](../Page/愛的魔法.md "wikilink")》女主角的宮間夕菜、《[境界之輪迴](../Page/境界之輪迴.md "wikilink")》的六文、《[干支魂](../Page/干支魂.md "wikilink")》的小蛇、《[神奇寶貝XY](../Page/神奇寶貝XY.md "wikilink")》希特隆的[哈力栗](../Page/第六世代寶可夢列表.md "wikilink")、《[Muv-Luv
Alternative Total
Eclipse](../Page/Muv-Luv_Alternative_Total_Eclipse.md "wikilink")》的克麗斯嘉·巴切諾娃、《[黑魔女學園](../Page/黑魔女學園.md "wikilink")》的麻倉良太郎、《[織田信奈的野望](../Page/織田信奈的野望.md "wikilink")》的柴田勝家、《[一起一起這裡那裡](../Page/一起一起這裡那裡.md "wikilink")》的片瀨真宵、《[卡片鬥爭\!\!
先導者](../Page/卡片戰鬥先導者.md "wikilink")》系列的鳴海淺香、《[惡魔高校D×D](../Page/惡魔高校D×D.md "wikilink")》的天野夕麻〈雷娜蕾〉、《[蘿黛的後宮玩具](../Page/蘿黛的後宮玩具.md "wikilink")》的茱蒂·斯諾雷維克、《[我的妹妹哪有這麼可愛！](../Page/我的妹妹哪有這麼可愛！_\(動畫\).md "wikilink")》的槙島沙織〈沙織·巴吉納〉、《[侵略！花枝娘](../Page/侵略！花枝娘.md "wikilink")》的辛蒂·坎貝爾、《[簡單易懂的現代魔法](../Page/簡單易懂的現代魔法.md "wikilink")》的姊原美鎖、《[藍龍](../Page/藍龍_\(動畫\).md "wikilink")》的諾伊、《[夏日風暴！](../Page/夏日風暴！.md "wikilink")》的Master／沙也加、《[一騎當千](../Page/一騎當千.md "wikilink")》的關羽雲長、《[乃木-{坂}-春香的秘密](../Page/乃木坂春香的秘密.md "wikilink")》的綾瀨琉子、《[旋風管家](../Page/旋風管家_\(動畫\).md "wikilink")》的、《[GO！純情水泳社！](../Page/GO！純情水泳社！.md "wikilink")》的織塚桃子、《[BLEACH](../Page/BLEACH_\(動畫\).md "wikilink")》的[伊勢七緒](../Page/護廷十三隊#一番隊（總番隊）.md "wikilink")、《[灼眼的夏娜](../Page/灼眼的夏娜_\(動畫\).md "wikilink")》的瑪瓊琳·朵、《[草莓危機](../Page/草莓危機.md "wikilink")》的花園靜馬、《[校園迷糊大王](../Page/校園迷糊大王.md "wikilink")》的周防美琴、《[草莓棉花糖](../Page/草莓棉花糖.md "wikilink")》的伊藤伸惠、《[極上生徒會](../Page/極上生徒會.md "wikilink")》的神宮司奏、《[GANTZ](../Page/殺戮都市.md "wikilink")》的岸本惠、《[瑪莉亞的凝望](../Page/瑪莉亞的凝望.md "wikilink")》的鳥居江利子\[6\]；遊戲（含改編動畫）《[ToHeart2](../Page/ToHeart2.md "wikilink")》的十波由真、《[夜明前的琉璃色
～Crescent Love～](../Page/夜明前的琉璃色.md "wikilink")》女主角菲娜·法姆·亞修萊特、《[Canvas2
～彩虹色的圖畫～](../Page/Canvas2.md "wikilink")》的桔梗霧、《[CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")》的蒼井瀨、《[WHITE
ALBUM2](../Page/白色相簿2.md "wikilink")》的冬馬和紗\[7\]等；電影《[飢餓遊戲](../Page/飢餓遊戲_\(電影系列\).md "wikilink")》系列、及韓國電視劇《[灰姑娘的姐姐](../Page/灰姑娘的姐姐.md "wikilink")》女主角宋恩祖（[文瑾瑩飾演](../Page/文瑾瑩.md "wikilink")）、《[華政](../Page/華政.md "wikilink")》主角[貞明公主](../Page/貞明公主.md "wikilink")（[李沇熹飾演](../Page/李沇熹.md "wikilink")）的日語配音\[8\]。

### 來歷

生天目仁美從專門學校時代開始，不斷嘗試參加各種新人徵選活動。直到電台節目《》
舉辦新人聲優通過徵選之後，以新人聲優組合「美夢成真俱樂部」的成員身分出道（包含同樣經由公開徵選而入選的[田村由香里](../Page/田村由香里.md "wikilink")、[堀江由衣](../Page/堀江由衣.md "wikilink")、[淺川悠等人](../Page/淺川悠.md "wikilink")）。畢業後，她被[柄本明的](../Page/柄本明.md "wikilink")[芝居所感動](../Page/芝居.md "wikilink")，因此加入，從舞台演員活動起家\[9\]。退團後，進入[賢Production旗下聲優學校](../Page/賢Production.md "wikilink")接受訓練，結訓後正式成為賢Production所屬。

2003年，生天目於School Duo受訓時期，幫對戰格鬥遊戲《》角色、及電視動畫《[真月譚
月姬](../Page/月姬.md "wikilink")》女主角愛爾奎特·布倫史塔德與《[愛的魔法](../Page/愛的魔法.md "wikilink")》女主角宮間夕菜進行配音而備受注目，從此她正式投入聲優業界。

2011年，組成演劇組合「缶」，2014年為止已進行了3場公演。現在休止中。

2004年至2005年間與同事務所的同事[伊藤靜組成雙人聲優組合](../Page/伊藤靜.md "wikilink")「（<small></small>）」活動。

### 人物

喜歡的言語是「冬天過後就是春天（<small></small>）」\[10\]。家庭構成為雙親及一個弟弟。家族是虔誠的[創價學會學會員](../Page/創價學會.md "wikilink")。本身屬於東京都新宿區新宿青春支部女子部員。

本身是[讀賣巨人球迷](../Page/讀賣巨人.md "wikilink")\[11\]。國中時期是[壘球社所屬](../Page/壘球.md "wikilink")\[12\]，高中時期是[排球社所屬](../Page/排球.md "wikilink")\[13\]。

母親經常以[筆名](../Page/筆名.md "wikilink")「**滿某某小姐**（<small></small>）」投稿到生天目的電台節目大談女兒的事，因此生天目她的粉絲會以「某小姐（<small></small>）」來稱呼生天目的母親\[14\]。

2014年8月4日，生天目在部落格為聲演角色《[DokiDoki！光之美少女](../Page/DokiDoki!_光之美少女.md "wikilink")》女主角相田愛一同慶生，同時也發表已經結婚的消息\[15\]。

通常暱稱「****」\[16\]。其它還有「****」（取名）、「****」（[安元洋貴取名](../Page/安元洋貴.md "wikilink")）、「****」（[鈴村健一取名](../Page/鈴村健一.md "wikilink")）。

## 逸事

2003年動畫版《[真月譚
月姬](../Page/月姬.md "wikilink")》的角色發佈會中，有關方面決定起用當時還是寂寂無名、演[舞台劇出身的生天目仁美配演女主角愛爾奎特](../Page/舞台劇.md "wikilink")，震驚外界。在同期播映的《[愛的魔法](../Page/愛的魔法.md "wikilink")》再度被起用，為女主角宮間夕菜配音。在《愛的魔法》第一次錄音後，有感聲優的工作與演舞台劇有很大的差距，一時適應不來，回家途中一個人在[電車上流淚](../Page/電車.md "wikilink")。

跟同一事務所的[伊藤靜組成](../Page/伊藤靜.md "wikilink")「生天目仁美與伊藤靜」參加活動時期，由於經常在公開活動中「言論失控」，需要伊藤及[豬口有佳出手壓制](../Page/豬口有佳.md "wikilink")。

在演出《[草莓棉花糖](../Page/草莓棉花糖.md "wikilink")》後，與同劇的聲優[千葉紗子](../Page/千葉紗子.md "wikilink")、[折笠富美子](../Page/折笠富美子.md "wikilink")、[川澄綾子及](../Page/川澄綾子.md "wikilink")[能登麻美子關係良好](../Page/能登麻美子.md "wikilink")，因此組成了「棉花糖會（）」，偶然會相約一起出外活動。

非常喜歡[能登麻美子](../Page/能登麻美子.md "wikilink")，曾在手提電話的鈴聲用了能登麻美子的聲音、多次在網上電台節目高呼「最愛麻美子」\[17\]。

在《》，[清水香里對於穿著浴衣出場的生天目的評語為](../Page/清水香里.md "wikilink")：「第一次看到她那麼女性化！」另外，生天目是[校園迷糊大王工作人員中公認的](../Page/校園迷糊大王.md "wikilink")「[性騷擾大王](../Page/性騷擾.md "wikilink")」，曾被她[非禮胸部的女聲優有](../Page/非禮.md "wikilink")：[清水香里](../Page/清水香里.md "wikilink")、[能登麻美子](../Page/能登麻美子.md "wikilink")、[小清水亞美](../Page/小清水亞美.md "wikilink")、[堀江由衣](../Page/堀江由衣.md "wikilink")、[福井裕佳梨](../Page/福井裕佳梨.md "wikilink")。

生天目仁美自言她的「正妻」是[伊藤靜](../Page/伊藤靜.md "wikilink")，愛人一號是[能登麻美子](../Page/能登麻美子.md "wikilink")，愛人二號是[清水香里](../Page/清水香里.md "wikilink")；而能登麻美子則說愛人三號是[小清水亞美](../Page/小清水亞美.md "wikilink")，愛人四號是[後藤麻衣](../Page/後藤麻衣.md "wikilink")。至於[尤加奈則是她的](../Page/尤加奈.md "wikilink")「戀愛預感」。

和[佐藤利奈](../Page/佐藤利奈.md "wikilink")、[下屋則子](../Page/下屋則子.md "wikilink")、[廣橋涼組成](../Page/廣橋涼.md "wikilink")「[KAT-TUN會](../Page/KAT-TUN.md "wikilink")」（住在旅館，一直到早上為止都在看KAT-TUN的DVD，一邊聊天）。最近[植田佳奈也加入了](../Page/植田佳奈.md "wikilink")（植田在blog中表示）。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 年份不詳

<!-- end list -->

  - [麵包超人](../Page/麵包超人.md "wikilink")（貓美〈第8代〉）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [犬夜叉](../Page/犬夜叉.md "wikilink")（村子女孩）
  - [拜託了☆雙子星](../Page/拜託了☆雙子星.md "wikilink")（女學生）
  - [你所期望的永遠](../Page/你所期望的永遠.md "wikilink")（女學生B）
  - [真月譚 月姬](../Page/月姬.md "wikilink")（**愛爾奎特·布倫史塔德**）
  - [愛的魔法](../Page/愛的魔法.md "wikilink")（**宮間夕菜**）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [詩∽片](../Page/詩∽片.md "wikilink")（橘泉水）

  - （梅）

  - [妖精的旋律](../Page/妖精的旋律.md "wikilink")（白河、少年時期的恭太）

  - [GANTZ](../Page/殺戮都市.md "wikilink")（**岸本惠**\[18\]）

  - [混沌武士](../Page/混沌武士.md "wikilink")（村子女孩）

  - [校園迷糊大王](../Page/校園迷糊大王.md "wikilink")（**周防美琴**）

  - [沙漠神行者](../Page/沙漠神行者.md "wikilink")（鞠子\[19\]）

  - [老師的時間](../Page/老師的時間.md "wikilink")（女學生、外國女性）

  - [索斯機械獸IV](../Page/索斯機械獸IV.md "wikilink")（貝蒂）

  - [捉鬼天狗幫](../Page/捉鬼天狗幫.md "wikilink")（志乃）

  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")（海豚）

  - （女忍者、女學生）

  - [光與水的女神](../Page/光與水的女神.md "wikilink")（梅、女學生A）

  - [光之美少女](../Page/光之美少女.md "wikilink")（中川弓子）

  - [BLEACH](../Page/BLEACH_\(動畫\).md "wikilink")（越智美諭、國枝鈴、[伊勢七緒](../Page/護廷十三隊#一番隊（總番隊）.md "wikilink")）

  - [瑪莉亞的凝望](../Page/瑪莉亞的凝望.md "wikilink")（鳥居江利子）

      - 瑪莉亞的凝望
      - 瑪莉亞的凝望 ～春～

  - [妄想代理人](../Page/妄想代理人.md "wikilink")（廣播）

  - [洛克人EXE Stream](../Page/洛克人EXE_Stream.md "wikilink")（盧特）

  - [魔法咪路咪路Wonderful](../Page/魔法咪路咪路.md "wikilink")（卡梅-{里}-）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [極限女孩](../Page/極限女孩.md "wikilink")（**鳳薇薇安**）
  - [草莓棉花糖](../Page/草莓棉花糖.md "wikilink")（**伊藤伸惠**、女教師）
  - [Canvas2 ～彩虹色的圖畫～](../Page/Canvas2.md "wikilink")（**桔梗霧**）
  - [玻璃假面 (2005年版)](../Page/玻璃假面.md "wikilink")（奧菲莉婭、小宮部長）
  - [極上生徒會](../Page/極上生徒會.md "wikilink")（**神宮司奏**）
  - [灼眼的夏娜](../Page/灼眼的夏娜_\(動畫\).md "wikilink")（**『悼文吟誦人』瑪瓊琳·朵**）
  - [星艦駕駛員](../Page/星艦駕駛員.md "wikilink")（迪塔·米爾科夫）
  - [ToHeart2](../Page/ToHeart2.md "wikilink")（**十波由真**）
  - [聖魔之血](../Page/聖魔之血.md "wikilink")（凱特·史考特\[20\]）
  - [蜂蜜幸運草](../Page/蜂蜜幸運草.md "wikilink")（蝶子）
  - [嬉皮笑園](../Page/嬉皮笑園.md "wikilink")（南條操、小Q）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [增血鬼果林](../Page/增血鬼果林.md "wikilink")（小孩、女子學生、女）

  - [星際海盜](../Page/星際海盜.md "wikilink")（四月）

  - （羅娜·法娜）

  - [地獄少女](../Page/地獄少女.md "wikilink")（小川步美〈柴田步美〉 ）

  - [女高中生 GIRLS-HIGH](../Page/女子高生.md "wikilink")（**高橋繪-{里}-子**）

  - 校園迷糊大王 二學期（**周防美琴**）

  - [草莓危機](../Page/草莓危機.md "wikilink")（**花園靜馬**）

  - [真仙魔大戰](../Page/真仙魔大戰.md "wikilink")（上御殿）

  - [小小備長炭](../Page/小小備長炭.md "wikilink")（教師、作法老師）

  - [武裝鍊金](../Page/武裝鍊金.md "wikilink")（早-{坂}-櫻花）

  - BLEACH（狩矢神〈少年時期〉）

  - [無敵看板娘](../Page/無敵看板娘.md "wikilink")（**鬼丸美輝**\[21\]）

  - [夜明前的琉璃色 ～Crescent
    Love～](../Page/夜明前的琉璃色.md "wikilink")（**菲娜·法姆·亞修萊特**）

  - [怪醫美女RAY THE ANIMATION](../Page/怪醫美女RAY.md "wikilink")（佐藤利江）

  - [鍊金三級魔法少女](../Page/鍊金三級魔法少女.md "wikilink")（**莉露**）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [一騎當千 龍之命運](../Page/一騎當千.md "wikilink")（關羽雲長）

  - [王牌投手 振臂高揮](../Page/王牌投手_振臂高揮.md "wikilink") - 篠岡的前輩

  - [GO！純情水泳社！](../Page/GO！純情水泳社！.md "wikilink")（**織塚桃子**）

  - [鋼鐵三國志](../Page/鋼鐵三國志.md "wikilink")（**孫權仲謀**）

  - 灼眼的夏娜II（**『悼文吟誦人』瑪瓊琳·朵**）

  - [天翔少女](../Page/天翔少女.md "wikilink")（安岐夕子）

  - [藍蘭島漂流記](../Page/藍蘭島漂流記.md "wikilink")（**梅梅**、小魔）

  - [交響情人夢](../Page/交響情人夢_\(動畫\).md "wikilink")（多賀谷彩子\[22\]）

  - [蟲之歌](../Page/蟲之歌.md "wikilink")（**立花利菜**）

  - [旋風管家！](../Page/旋風管家_\(動畫\).md "wikilink")（****、魔女）\[23\]

  - [PRISM ARK](../Page/PRISM_ARK.md "wikilink")（**神樂**、柯梅特、梅特）

  - [BLUE DROP ～天使們的戲曲～](../Page/BLUE_DROP.md "wikilink")（機械操作人員、學生1）

  - （銀之瑪莉亞、瑪麗）

  - [火箭女孩](../Page/火箭女孩.md "wikilink")（**茉莉**）

<!-- end list -->

  - 2008年

<!-- end list -->

  - 一騎當千 GreatGuardians（**關羽雲長**）

  - [CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")（**蒼井瀨名**\[24\]）

  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（姬宮百萬子／不解宮百萬）

  - [鬼太郎 (第5作)](../Page/鬼太郎.md "wikilink")（實）

  - （頒獎式準備委員B）

  - [S·A特優生 ～Special A～](../Page/S·A特優生.md "wikilink")（**東堂明**、瀧島家女僕B）

  - [秀逗魔導士REVOLUTION](../Page/秀逗魔導士_\(動畫\).md "wikilink")（少女A、女性客人A）

  - [鶺鴒女神](../Page/鶺鴒女神.md "wikilink")（鈿女）

  - （**洛基·羅曼**）

  - [隱王](../Page/隱王.md "wikilink")（藤堂卡塔麗娜）

  - [乃木-{坂}-春香的秘密](../Page/乃木坂春香的秘密.md "wikilink")（綾瀨琉子、綾瀨裕人〈幼少時期〉）

  - [蒼色騎士](../Page/蒼色騎士.md "wikilink")（薩夏）

  - [藍龍](../Page/藍龍_\(動畫\).md "wikilink")（**諾伊**）

  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")（芭莉絲）

  - （雅）

  - [記憶女神的女兒們](../Page/記憶女神的女兒們.md "wikilink")（神山瑠音）

  - [藥師寺涼子怪奇事件簿](../Page/藥師寺涼子怪奇事件簿.md "wikilink")（**藥師寺涼子**\[25\]、莫奈美）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [明日的與一！](../Page/明日的與一！.md "wikilink")（鷹司安琪拉）
  - [海物語 ～有你相伴～](../Page/海物語.md "wikilink")（章魚美女士兵）
  - [女王之刃](../Page/女王之刃_\(動畫\).md "wikilink")（甲魔忍軍頭領 靜）
      - 女王之刃 流浪的戰士
      - 女王之刃 王座的繼承者
  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（小凜）
  - [神曲奏界 crimson S](../Page/神曲奏界.md "wikilink")（艾蓮德絲）\[26\]
  - [SKIP BEAT！華麗的挑戰](../Page/SKIP_BEAT！華麗的挑戰.md "wikilink")（百瀨逸美）
  - [楚楚可憐超能少女組](../Page/楚楚可憐超能少女組.md "wikilink")（塞拉、瑪薩拉）
  - [DARKER THAN BLACK
    -流星之雙子-](../Page/DARKER_THAN_BLACK.md "wikilink")（理花子）
  - [信蜂](../Page/信蜂.md "wikilink")（內莉）
  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink")（舍監）
  - [夏日風暴！／夏日風暴！
    ～春夏冬中～](../Page/夏日風暴！.md "wikilink")（**Master**／**沙也加**\[27\]\[28\]）
  - 乃木-{坂}-春香的秘密 Purezza♪（綾瀨瑠子、姬宮深藍）
  - 旋風管家\!\!（**桂雪路**）
  - 瑪莉亞的凝望 4th Season（鳥居江利子、有馬菜菜）
  - [戰鬥陀螺 鋼鐵奇兵](../Page/金屬對決_戰鬥陀螺.md "wikilink")（修）
  - [小雙俠 (2008年版)](../Page/小雙俠_\(2008年動畫版\).md "wikilink")（）
  - [簡單易懂的現代魔法](../Page/簡單易懂的現代魔法.md "wikilink")（**姊原美鎖**）

<!-- end list -->

  - 2010年

<!-- end list -->

  - 一騎當千 XTREME XECUTOR（**關羽雲長**）
  - [野狼大神與七位夥伴](../Page/野狼大神.md "wikilink")（村野雪女）
  - [我的妹妹哪有這麼可愛！](../Page/我的妹妹哪有這麼可愛！_\(動畫\).md "wikilink")（**槙島沙織**〈**沙織·巴吉納**〉\[29\]）
  - [學園默示錄 HIGHSCHOOL OF THE DEAD](../Page/學園默示錄.md "wikilink")（女）
  - [侵略！花枝娘](../Page/侵略！花枝娘.md "wikilink")（**辛蒂·坎貝爾**）
  - [心靈偵探 八雲](../Page/心靈偵探_八雲.md "wikilink")（新井真央）
  - 鶺鴒女神 ～Pure Engagement～（鈿女）
  - [哆啦A夢 (水田山葵版)](../Page/哆啦A夢_\(2005年電視動畫\).md "wikilink")（女孩子B）
  - [吊帶襪天使](../Page/吊帶襪天使.md "wikilink")（丈夫惡靈）
  - [FORTUNE ARTERIAL
    -赤之約束-](../Page/FORTUNE_ARTERIAL.md "wikilink")（**悠木奏**）
  - [嬌蠻貓娘大橫行](../Page/嬌蠻貓娘大橫行.md "wikilink")（穗乃花的母親）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [蘿黛的後宮玩具](../Page/蘿黛的後宮玩具.md "wikilink")（**茱蒂·斯諾雷維克**）
  - [卡片鬥爭\!\! 先導者](../Page/卡片戰鬥先導者.md "wikilink")（鳴海淺香）
  - [神的記事本](../Page/神的記事本.md "wikilink")（**明老闆—**）
  - [30歲的保健體育](../Page/30歲的保健體育.md "wikilink")（和田、保加利亞小姐）
  - 灼眼的夏娜III -FINAL-（**『悼文吟誦人』瑪瓊琳·朵**）
  - 侵略\!? 花枝娘（**辛蒂·坎貝爾**）
  - [寵物反斗星](../Page/寵物反斗星.md "wikilink")（Tsurara女王\[30\]）
  - [夏目友人帳 參](../Page/夏目友人帳.md "wikilink")（附身田沼的妖怪）
  - [變身公主](../Page/變身公主.md "wikilink")（乙姬）
  - [Persona4 the ANIMATION](../Page/女神異聞錄4.md "wikilink")（柏木典子）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [一起一起這裡那裡](../Page/一起一起這裡那裡.md "wikilink")（**片瀨真宵**）

  - （阿時）

  - [織田信奈的野望](../Page/織田信奈的野望.md "wikilink")（**柴田勝家**\[31\]）

  - 卡片鬥爭\!\! 先導者 亞洲巡迴賽篇（鳴海淺香）

  - [少女與戰車](../Page/少女與戰車.md "wikilink")（逸見艾麗卡\[32\]）

  - [黑魔女學園](../Page/黑魔女學園.md "wikilink")（**麻倉良太郎**）

  - [這樣算是殭屍嗎？OF THE DEAD](../Page/這樣算是殭屍嗎？.md "wikilink")（克莉絲）

  - [戰國Collection](../Page/戰國Collection.md "wikilink")（山口）

  - [偵探歌劇 少女福爾摩斯 第2幕](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")（日笠）

  - [Muv-Luv Alternative Total
    Eclipse](../Page/Muv-Luv_Alternative_Total_Eclipse.md "wikilink")（**克麗斯嘉·巴切諾娃**\[33\]）

  - [惡魔高校D×D](../Page/惡魔高校D×D.md "wikilink")（天野夕麻〈雷娜蕾〉）

  - 旋風管家！CAN'T TAKE MY EYES OFF YOU（桂雪路）

  - [向陽素描×蜂窩](../Page/向陽素描_\(動畫\).md "wikilink")（安藤）

  - [惡魔奶爸](../Page/惡魔奶爸.md "wikilink")（鳥居）

<!-- end list -->

  - 2013年

<!-- end list -->

  - 我的妹妹哪有這麼可愛。（**槙島沙織**〈**沙織·巴吉納**〉）
  - 卡片鬥爭\!\! 先導者 聯合王牌篇（鳴海淺香）
  - 蠟筆小新（幼稚園園童）
  - [團地友夫](../Page/團地友夫.md "wikilink")（菊川光雄\[34\]、吉本的母親）
  - 科學超電磁砲S（舍監）
  - [DokiDoki！光之美少女](../Page/DokiDoki!_光之美少女.md "wikilink")（**相田愛**／**愛心天使**\[35\]）
  - 旋風管家！Cuties（桂雪路）
  - [神奇寶貝XY](../Page/神奇寶貝XY.md "wikilink")（薩奇、希特隆的[哈力栗](../Page/第六世代寶可夢列表.md "wikilink")、米茹菲的[胖甜妮](../Page/第六世代寶可夢列表.md "wikilink")、可爾妮的[功夫鼬](../Page/第五世代寶可夢列表.md "wikilink")、花潔夫人、[瑪綉的粉香香](../Page/精靈寶可夢動畫角色列表#卡洛斯地方.md "wikilink")、翔太的胖甜妮
    等）
  - 迷你先導（鳴海淺香、卡片的聲音〈女〉）
  - [名偵探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")（板倉美加）
  - [WHITE ALBUM2](../Page/白色相簿2.md "wikilink")（**冬馬和紗**\[36\]）
  - [Walkure Romanze
    少女騎士物語](../Page/Walkure_Romanze_少女騎士物語.md "wikilink")（龍造寺茜\[37\]）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [斬！赤紅之瞳](../Page/斬！赤紅之瞳.md "wikilink")（妮烏\[38\]）

  - 卡片鬥爭\!\! 先導者 雙鬥搭檔篇（鳴海淺香）

  - [鋼彈創戰者TRY](../Page/GUNDAM創戰者TRY.md "wikilink")（星野真理香）

  - [偽戀](../Page/偽戀.md "wikilink")（日原教子）

  - （小魚乾喵面\[39\]）

  - [Happiness Charge
    光之美少女！](../Page/Happiness_Charge_光之美少女！.md "wikilink")（愛心天使）\[40\]

  - [Fate/kaleid liner 魔法少女☆伊莉雅
    2wei！](../Page/Fate/kaleid_liner_魔法少女☆伊莉雅.md "wikilink")（**巴潔特·法迦·克米茲**\[41\]）

  - [黑色子彈](../Page/黑色子彈.md "wikilink")（椎名和美—）

  - [RAIL WARS！](../Page/RAIL_WARS!_-日本國有鐵道公安隊-.md "wikilink")（門田櫻）

  - [三坪房間的侵略者\!?](../Page/三坪房間的侵略者！？.md "wikilink")（放送委員）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [Aikatsu！偶像活動！](../Page/Aikatsu！偶像活動！_\(動畫\).md "wikilink")（黑澤凛的前輩）
  - [ALDNOAH.ZERO (第二期)](../Page/ALDNOAH.ZERO.md "wikilink")（拉斐亞）
  - [干支魂](../Page/干支魂.md "wikilink")（**小蛇**\[42\]、星之子）
  - [境界之輪迴](../Page/境界之輪迴.md "wikilink")（**六文**\[43\]）
  - [金田一少年之事件簿R
    SP「明智警部之事件簿」](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（杉田圓\[44\]）
  - [食戟之靈](../Page/食戟之靈.md "wikilink")（大御堂文緒〈年輕時〉）
  - [那就是聲優！](../Page/那就是聲優！.md "wikilink")（**汐汐留光**\[45\]）
  - 偽戀：（日原教子）
  - 惡魔高校D×D BorN（天野夕麻〈雷娜蕾〉）
  - Fate/kaleid liner 魔法少女☆伊莉雅 2wei Herz！（**巴潔特·法迦·克米茲**\[46\]）
  - 神奇寶貝
    XY\&Z（希特隆的哈力栗、米茹菲的胖甜妮、薩奇、翔太的胖甜妮、絢香的[阿勃梭魯](../Page/阿勃梭魯.md "wikilink")、艾嵐的[瑪狃拉](../Page/第四世代寶可夢列表.md "wikilink")、瑪綉的粉香香
    等）
  - 名偵探柯南（關根康美）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [蒼之彼方的四重奏](../Page/蒼之彼方的四重奏.md "wikilink")（有坂牡丹\[47\]）
  - [房東妹子青春期](../Page/房東妹子青春期.md "wikilink")（**白井麗子**\[48\]）
  - 卡片鬥爭\!\! 先導者G GIRS危機篇（鳴海淺香）
  - 卡片鬥爭\!\! 先導者G 超越之門篇（鳴海淺香）
  - 鋼彈創戰者TRY ISLAND WARS（星野真理香）
  - [機動戰士鋼彈 鐵血的孤兒](../Page/機動戰士GUNDAM_鐵血的孤兒.md "wikilink")（茵瑪津·多卡）
  - 境界之輪迴 (第2期)（**六文**、洋子的母親）
  - [為美好的世界獻上祝福！](../Page/為美好的世界獻上祝福！.md "wikilink")（瑟娜）
  - [在下坂本，有何貴幹？](../Page/在下坂本，有何貴幹？.md "wikilink")（八木\[49\]、家庭科教師）
  - [TABOO TATTOO
    -禁忌咒紋-](../Page/TABOO_TATTOO－禁忌咒紋－.md "wikilink")（**卡爾·夏克哈**\[50\]）
  - [DAYS](../Page/DAYS_\(漫畫\).md "wikilink")（風間曉子)
  - [春&夏事件簿 ～春太與千夏的青春～](../Page/春&夏事件簿.md "wikilink")（上條南風）
  - Fate/kaleid liner 魔法少女☆伊莉雅 3rei\!\!（**巴潔特·法迦·克米茲**\[51\]）
  - [Lostorage incited
    WIXOSS](../Page/Lostorage_incited_WIXOSS.md "wikilink")（雪目\[52\]）
  - [WWW.WORKING\!\!](../Page/WORKING!!_\(網絡漫畫\).md "wikilink")（進藤悠態的母親）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [CHAOS;CHILD](../Page/CHAOS;CHILD.md "wikilink")（蒼井瀨名）

  - 為美好的世界獻上祝福！2（瑟娜\[53\]、豆之助、長相可怕男子的搭檔）

  - （端木寺蕓）

  - 境界之輪迴 (第3期)（**六文**）

  - [不起眼女主角培育法♭](../Page/不起眼女主角培育法.md "wikilink")（紅-{坂}-朱音）

  - [劍姬神聖譚 在地下城尋求邂逅是否搞錯了什麼
    外傳](../Page/在地下城尋求邂逅是否搞錯了什麼.md "wikilink")（椿·柯布蘭德）

  - [情色漫畫老師](../Page/情色漫畫老師.md "wikilink")（槙島沙織〈沙織·巴吉納〉）\[54\]

  - [來自深淵](../Page/來自深淵_\(漫畫\).md "wikilink")（拉菲）

  - 名偵探柯南（福原由佳）

  - [寶石之國](../Page/寶石之國.md "wikilink")（[榍石](../Page/榍石.md "wikilink")\[55\]）

  - [側車搭檔](../Page/側車搭檔.md "wikilink")（**和田初音**\[56\]）

  - [Dies
    irae](../Page/Dies_irae_-Also_sprach_Zarathustra-.md "wikilink")（**冰室玲愛**、**伊札克**\[57\]）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [刻刻](../Page/刻刻.md "wikilink")（佑河早苗）
  - [伊藤潤二驚選集](../Page/伊藤潤二驚選集.md "wikilink")（曾我\[58\]）
  - 靈契 -黃泉之契-（端木寺芸）
  - [妖精森林的小不點](../Page/妖精森林的小不點.md "wikilink")（愛結音）
  - 惡魔高校D×D HERO（天野夕麻〈雷娜蕾〉）
  - [Hisone與Masotan](../Page/Hisone與Masotan.md "wikilink")（昭島英子）
  - 卡片鬥爭\!\! 先導者 (2018年版)（鳴海淺香）
  - [Free！-Dive to the Future-](../Page/Free!.md "wikilink")（栗宮茜）
  - [少女☆歌劇Revue
    Starlight](../Page/少女☆歌劇Revue_Starlight.md "wikilink")（中學老師）
  - [RELEASE THE SPYCE](../Page/RELEASE_THE_SPYCE.md "wikilink")（源鈴）
  - [刀劍神域 Alicization](../Page/刀劍神域.md "wikilink")（法娜提歐·辛賽西斯·滋）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [環戰公主](../Page/環戰公主.md "wikilink")（**黑田-{怜}-奈**\[59\]）

### OVA

  - 2004年

<!-- end list -->

  - [飛越巔峰2](../Page/飛越巔峰2.md "wikilink")（維塔·諾瓦）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [鴉 -KARAS-](../Page/鴉_-KARAS-.md "wikilink")（炎）
  - [校園迷糊大王OVA 一學期補習](../Page/校園迷糊大王.md "wikilink")（**周防美琴**）
  - 歌魂斗（對手聲音）
  - [撲殺天使朵庫蘿](../Page/撲殺天使朵庫蘿.md "wikilink")（沼田春奈）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [灼眼的夏娜SP「戀愛與溫泉的校外教學！」](../Page/灼眼的夏娜_\(動畫\)#灼眼的夏娜（第一期）.md "wikilink")（**『悼文吟誦人』瑪瓊琳·朵**）
  - [BALDR FORCE EXE
    RESOLUTION](../Page/BALDR_FORCE.md "wikilink")（紫藤彩音）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [草莓棉花糖OVA](../Page/草莓棉花糖.md "wikilink")（**伊藤伸惠**）
  - [OVA ToHeart2](../Page/ToHeart2.md "wikilink")（**十波由真**）

<!-- end list -->

  - 2008年

<!-- end list -->

  - ToHeart2ad（**十波由真**）
  - [舞-乙HiME 0
    ～S.ifr～](../Page/舞-乙HiME_0～S.ifr～.md "wikilink")（西弗爾·布蘭的母親）

<!-- end list -->

  - 2009年

<!-- end list -->

  - 灼眼的夏娜S（**『悼文吟誦人』瑪瓊琳·朵**）
  - 校園迷糊大王 三學期（**周防美琴**）
  - ToHeart2 adplus（**十波由真**）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [女王之刃 美麗的鬥士們](../Page/女王之刃_\(動畫\).md "wikilink")（甲魔忍軍頭領 靜）
  - ToHeart2 adnext（**十波由真**）
  - [小泉麻將傳說 -The Legend of KOIZUMI-](../Page/渣和無用改革.md "wikilink")（鳩山夫人）
  - [眼鏡女友](../Page/眼鏡女友.md "wikilink")（眼鏡店店員）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [蘿黛的後宮玩具EX](../Page/蘿黛的後宮玩具.md "wikilink")（**茱蒂·斯諾雷維克**）

  - [幻想嘉年華](../Page/幻想嘉年華.md "wikilink")（巴潔特·法迦·克米茲）

  - （**南悠里**）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [一騎當千 集鍔闘士血風錄](../Page/一騎當千.md "wikilink")（**關羽雲長**\[60\]）
  - ToHeart2迷宮旅人（**十波由真**）\[61\]
  - [乃木-{坂}-春香的秘密 Finale♪](../Page/乃木坂春香的秘密.md "wikilink")（綾瀨瑠子\[62\]）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [少女與戰車「這才是真正的安齊奧之戰！」](../Page/少女與戰車#OVA與劇場版.md "wikilink")（逸見艾麗卡）
  - [侵略！花枝娘](../Page/侵略！花枝娘.md "wikilink")（辛蒂·坎貝爾）\[63\]
  - [旋風管家](../Page/旋風管家_\(動畫\).md "wikilink")（）\[64\]

<!-- end list -->

  - 2015年

<!-- end list -->

  - 一騎當千 Extravaganza Epoch（**關羽雲長**\[65\]）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [為美好的世界獻上祝福！2](../Page/為美好的世界獻上祝福！.md "wikilink")（美少女魔像）\[66\]

### 電影動畫

  - 2007年

<!-- end list -->

  - [劇場版 BLEACH The DiamondDust Rebellion
    鑽石星塵的反叛](../Page/BLEACH_漂靈：劇場版－鑽石星塵的反叛.md "wikilink")（伊勢七緒）
  - [劇場版
    灼眼的夏娜](../Page/灼眼的夏娜_\(動畫\)#劇場版.md "wikilink")（**『悼文吟誦人』瑪瓊琳·朵**）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [劇場版 旋風管家 HEAVEN IS A PLACE ON
    EARTH](../Page/劇場版_旋風管家_HEAVEN_IS_A_PLACE_ON_EARTH.md "wikilink")（****）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [劇場版 FAIRY TAIL 鳳凰巫女](../Page/FAIRY_TAIL#劇場版.md "wikilink")（協調員）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [劇場版
    DokiDoki！光之美少女：小愛结婚了\!\!?連結未來的希望禮服](../Page/DokiDoki！光之美少女#劇場版.md "wikilink")（**相田愛**／**愛心天使**\[67\]）
  - [劇場版 光之美少女 All Stars New
    Stage2：心之朋友](../Page/光之美少女_All_Stars_New_Stage2_心之朋友.md "wikilink")（**相田愛**／**愛心天使**\[68\]）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [劇場版 光之美少女 All Stars New
    Stage3：永遠的朋友](../Page/光之美少女_All_Stars_New_Stage3_永遠的朋友.md "wikilink")（**相田愛**／**愛心天使**\[69\]）
  - [劇場版 卡片鬥爭\!\! 先導者 Neon
    Messiah](../Page/卡片戰鬥先導者.md "wikilink")（鳴海淺香、伊吹浩二〈小學時期〉）
  - [Pokemon the movie XY
    破壞之繭與蒂安希](../Page/Pokemon_the_movie_XY_破壞之繭與蒂安希.md "wikilink")（希特隆的[哈力栗](../Page/第六世代寶可夢列表.md "wikilink")、絢香的[阿勃梭魯](../Page/阿勃梭魯.md "wikilink")）
      - [皮卡丘，這是什麼鑰匙？](../Page/Pokemon_the_movie_XY_破壞之繭與蒂安希#副篇與相關作品.md "wikilink")（哈力栗）

<!-- end list -->

  - 2015年

<!-- end list -->

  - （逸見艾麗卡）

  - [劇場版 光之美少女 All
    Stars：春之嘉年華♪](../Page/光之美少女_All_Stars_春之嘉年華♪.md "wikilink")（**相田愛**／**愛心天使**\[70\]）

  - [皮卡丘與神奇寶貝樂隊](../Page/Pokemon_the_movie_XY_光環的超魔神_胡帕#副篇與相關作品.md "wikilink")（哈力栗、花潔夫人）

  - [Pokemon the movie XY 光環的超魔神
    胡帕](../Page/Pokemon_the_movie_XY_光環的超魔神_胡帕.md "wikilink")（希特隆的哈力栗）

  - [High Speed！ -Free！Starting
    Days-](../Page/Free!.md "wikilink")（椎名旭的姊姊）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [劇場版 光之美少女 All
    Stars：大家唱歌吧♪奇蹟的魔法！](../Page/光之美少女_All_Stars_大家歌唱吧♪奇跡的魔法！.md "wikilink")（**相田愛**／**愛心天使**）
  - [Pokemon the movie XY\&Z 波爾凱尼恩與機關人偶
    瑪機雅娜](../Page/Pokemon_the_movie_XY&Z_波爾凱尼恩與機關人偶_瑪機雅娜.md "wikilink")（希特隆的哈力栗、拉凱爾的胖甜妮、[大比鳥](../Page/第一世代寶可夢列表.md "wikilink")）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [劇場版 HUG！光之美少女♡光之美少女 All Stars
    Memories](../Page/HUG！光之美少女♡光之美少女_All_Stars_Memories.md "wikilink")（**相田愛**／**愛心天使**\[71\]）

### 網路動畫

  - 2007年

<!-- end list -->

  - [Candy☆Boy](../Page/Candy☆Boy.md "wikilink")（**櫻井奏**）

  - （神樂香具耶）

### 遊戲

  - 2003年

<!-- end list -->

  - （、）

<!-- end list -->

  - 2004年

<!-- end list -->

  - （莉姆露露、Chample）

  - （瑪琪辛、三葉）

  - [ToHeart2 XRATED](../Page/ToHeart2.md "wikilink")（十波由真）

  - （白金）

  -
  - （妮娜·卡蒂納）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [永恆傳奇Online](../Page/永恆傳奇.md "wikilink")（**蘿瑞塔**）

  - [草莓棉花糖](../Page/草莓棉花糖.md "wikilink")（**伊藤伸惠**）

  - [極上生徒會](../Page/極上生徒會.md "wikilink")（**神宮司奏**）

  - [DUEL SAVIOR
    DESTINY](../Page/DUEL_SAVIOR#DUEL_SAVIOR_DESTINY.md "wikilink")（娜娜司）

  - （友美·O·檜山）

  - （瑪琪辛、三葉）

  - [火爆玫瑰](../Page/火爆玫瑰.md "wikilink")（嘉蒂·凱恩／貝琪）

  - （**拉寇兒·雅柏蓋特**）

  - [汪達與巨像](../Page/汪達與巨像.md "wikilink")（MONO）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [Canvas2 ～彩虹色的圖畫～](../Page/Canvas2.md "wikilink")（**桔梗霧**）

  - [靈彈魔女](../Page/靈彈魔女.md "wikilink")（**艾莉絲亞**）

  - [We Are\*](../Page/We_Are*.md "wikilink")（笛吹詩忍）

  - [草莓危機](../Page/草莓危機.md "wikilink")（**花園靜馬**）

  - [夜明前的琉璃色 ～Brighter than dawning
    blue～](../Page/夜明前的琉璃色.md "wikilink")（**菲娜·法姆·亞修萊特**）

  - [PRISM ARK](../Page/PRISM_ARK.md "wikilink")（劇中歌曲“amaranth”的歌唱）

  - （**桔梗·旬家**）

  - （李淑花）

  - 火爆玫瑰XX（嘉蒂·凱恩／貝琪）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [一騎當千 Shining Dragon](../Page/一騎當千.md "wikilink")（**關羽雲長**）

  - 天使計畫（艾蜜莉亞）

  - （）

  - （）

  - [七色★星露 pure\!\!](../Page/七色★星露.md "wikilink")（克羅瓦）

  - （桂雪路）

  - [這個美妙的世界](../Page/這個美妙的世界.md "wikilink")（來夢、虛西 充姬）

  - （安莉）

  - [星海遊俠1 初次啟航](../Page/星海遊俠1_初次啟航.md "wikilink")（**米莉·奇利特**）

  - （**克麗斯嘉·巴切諾娃**）

  - [洛克人ZX 降臨](../Page/洛克人ZX·降臨.md "wikilink")（**阿特拉斯**）

<!-- end list -->

  - 2008年

<!-- end list -->

  - 一騎當千 Eloquent Fist（**關羽雲長**）

  - [無盡探險](../Page/無盡探險.md "wikilink")（**艾雅**）

  - [CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")（**蒼井瀨名**）

  - [侍魂閃](../Page/侍魂_閃.md "wikilink")（莉姆露露）

  - [劍魂IV](../Page/劍魂IV.md "wikilink")（艾咪·索蕾爾、亞斯他錄·梅德爾）

  - [乃木-{坂}-春香的秘密 開始了，角色扮演♥](../Page/乃木坂春香的秘密.md "wikilink")（綾瀨瑠子）

  - [花樣男子 -戀愛女子-](../Page/花樣男子.md "wikilink")（道明寺椿）

  - [女神戰記 負罪者](../Page/北歐女神_負罪者.md "wikilink")（**艾莉絲**／**地獄犬**）

  - [Fate/unlimited
    codes](../Page/Fate/unlimited_codes.md "wikilink")（**巴潔特·法迦·克米茲**）

  - [武裝神姬 BATTLE RONDO](../Page/武裝神姬.md "wikilink")（工程機型MMS 葛拉普拉布）

  - [BLAZER DRIVE徽章戰役](../Page/燃燒驅動.md "wikilink")（**卡蓮**）

  - [PRISM ARK -AWAKE-](../Page/PRISM_ARK.md "wikilink")（**神樂**）

  - （綾野）

  - （楠木悠里、Maiden櫻崎）

  - （**門田櫻**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - （雪莉）

  - [CHAOS;HEAD NOAH](../Page/CHAOS;HEAD.md "wikilink")（蒼井瀨名）

  - [雀龍門](../Page/真·雀龍門.md "wikilink")（新米雀士）

  - [魔法飛球 PangYa](../Page/魔法飛球.md "wikilink")（安莉）

  - [星海遊俠4 -最後的希望-](../Page/星海遊俠4_-最後的希望-.md "wikilink")（**蜜利雅·迪歐尼潔司**）

  - [鶺鴒女神 ～未來的贈禮～](../Page/鶺鴒女神.md "wikilink")（鈿女）

  - （**本條-{咲}-**）

  - [劍魂：破碎的命運](../Page/魂之系列.md "wikilink")（艾咪·索蕾爾）

  - ToHeart2 PORTABLE（十波由真）

  - [電擊學園RPG 女神的十字架](../Page/電擊學園RPG_女神的十字架.md "wikilink")（瑪瓊琳·朵）

  - [FINAL FANTASY CRYSTAL CHRONICLES Echoes of
    Time](../Page/最終幻想水晶編年史_時之回聲.md "wikilink")（**不死名媛**、伊莉娜）

  - [水之魔石](../Page/水之魔石.md "wikilink")（菲德）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [閃耀十字軍 STARLIT BRAVE\!\!](../Page/閃耀十字軍.md "wikilink")（菲娜·法姆·亞修萊特）

  - （**梅露維納**）

  - [一騎當千 XROSS IMPACT](../Page/一騎當千.md "wikilink")（**關羽雲長**）

  - （幼女類型、御姊類型）

  - [CHAOS;HEAD Love
    Chu☆Chu！](../Page/CHAOS;HEAD.md "wikilink")（**蒼井瀨名**）

  - 乃木-{坂}-春香的秘密 同人誌創作開始♥（綾瀨瑠子）

  - 地獄犬計劃（藍）

  - （**瑪麗亞·泰瑞絲**）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [AQUAPAZZA -AQUAPLUS DREAM
    MATCH-](../Page/AQUAPAZZA.md "wikilink")（十波由真）

  - （）

  - （**槙島沙織**〈**沙織·巴吉納**〉）

  - （-{笹}-原七瀨\[72\]）

  - （十波由真）

  - ToHeart2 DX PLUS（十波由真）

  - （舍監）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [英雄傳說 零之軌跡 Evolution](../Page/英雄傳說VII.md "wikilink")（格蕾絲·琳\[73\]）

  - 我的妹妹哪有這麼可愛 攜帶版哪有可能繼續（**槙島沙織**〈**沙織·巴吉納**〉\[74\]）

  - （**名香野陽向**\[75\]）

  - （薇歐拉）

  - [Dies irae ～Amantes
    amentes～](../Page/Dies_irae_-Also_sprach_Zarathustra-.md "wikilink")（冰室玲愛\[76\]、伊札克\[77\]）

  - PACHISLOT ToHeart2（十波真由\[78\]）

  - [WHITE ALBUM2 ～幸福的另一側～](../Page/白色相簿2.md "wikilink")（**冬馬和紗**\[79\]）

  - [極度混亂](../Page/極度混亂.md "wikilink")（飛鈴）

  - （槙島沙織〈沙織·巴吉納〉、柴田勝家、冬馬和紗）

  - （**克蘿莉卡**\[80\]）

<!-- end list -->

  - 2013年

<!-- end list -->

  - （槙島沙織〈沙織·巴吉納〉\[81\]）

  - 海賊幻想曲（海軍大將）

  - 卡片鬥爭\!\! 先導者 衝向勝利\!\!（鳴海淺香）

  - Girls×Magic（**乃木-{坂}-寧寧**\[82\]）

  - （常世\[83\]）

  - （**愛爾莎**\[84\]）

  - 新星的Grand Union（**布蘭諾瓦**\[85\]）

  - （**東雲吉野**\[86\]）

  - ToHeart 心動派對（冬馬和紗）

  - DokiDoki！光之美少女 變裝生活！（**相田愛**／**愛心天使**）

  - [Muv-Luv Alternative Total
    Eclipse](../Page/Muv-Luv_Alternative_Total_Eclipse.md "wikilink")（**克麗斯嘉·巴切諾娃**\[87\]）

  - [魔女與百騎兵](../Page/魔女與百騎兵.md "wikilink")（魔莉卡）

<!-- end list -->

  - 2014年

<!-- end list -->

  - 英雄傳說 碧之軌跡 Evolution（格蕾絲·琳\[88\]）

  - 少女與戰車 戰車道的極致！（逸見艾麗卡\[89\]）

  - [企業☆女孩](../Page/企業☆女孩.md "wikilink")（****）

  - [碧藍幻想](../Page/碧藍幻想.md "wikilink")（希格\[90\]、莉姆露露\[91\]）

  - （****\[92\]）

  - [巴哈姆特之怒](../Page/巴哈姆特之怒.md "wikilink")（愛莎\[93\]、、）

  - （樹麻凜\[94\]）

  - 戰場的婚禮（魔術王克莉絲緹娜）

  - [SOUL SACRIFICE DELTA](../Page/闇魂獻祭.md "wikilink")（莉歐妮絲\[95\]）

  - [任天堂明星大亂鬥3DS/Wii
    U](../Page/任天堂明星大亂鬥3DS/Wii_U.md "wikilink")（[哈力栗](../Page/第六世代寶可夢列表.md "wikilink")、[水水獺](../Page/第五世代寶可夢列表.md "wikilink")、索羅亞克、[哲爾尼亞斯](../Page/第六世代寶可夢列表.md "wikilink")）

  - [惡魔高校D×D NEW FIGHT](../Page/惡魔高校D×D.md "wikilink")（天野夕麻〈雷娜蕾〉\[96\]）

  - \[97\]

  - [Fate/hollow
    ataraxia](../Page/Fate/hollow_ataraxia.md "wikilink")（**巴潔特·法迦·克米茲**\[98\]）

<!-- end list -->

  - 2015年

<!-- end list -->

  - 超銀河船團（****\[99\]、\[100\]）

  - [勇者鬥惡龍VIII
    天空、碧海、大地與被詛咒的公主](../Page/勇者鬥惡龍VIII_天空、碧海、大地與被詛咒的公主.md "wikilink")（**蓋爾妲**\[101\]）

  - （星野靜流\[102\]）

  - [夢幻之星Online2](../Page/夢幻之星在線2.md "wikilink")（\[103\]）

  - 魔女與百騎兵 Revival（魔莉卡\[104\]）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [蒼之彼方的四重奏](../Page/蒼之彼方的四重奏.md "wikilink")（有-{坂}-牡丹\[105\]）

  - [鬥陣特攻](../Page/鬥陣特攻.md "wikilink")（**辛梅塔**\[106\]）

  - 極限凸騎 怪物卡片對決 NAKED（**愛爾莎**\[107\]）

  - 宿星的絕望鄉（**妖魔刹那**\[108\]）

  - （[立花道雪dousetsu](../Page/立花道雪.md "wikilink")\[109\]）

  - （\[110\]、艾莉絲／冥狼犬艾莉絲）

  - （艾莉潔\[111\]）

  - （不死名媛）

  - （米莉·奇利特、 穆莉雅·迪歐尼賽斯、）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [永遠的7日之都](../Page/永遠的7日之都.md "wikilink")（瑟雷斯）

  - [火焰之紋章 回聲 另一位英雄王](../Page/火焰之紋章_回聲_另一位英雄王.md "wikilink")（卡秋亞\[112\]）

  - [火焰之紋章 英雄](../Page/火焰之紋章_英雄.md "wikilink")（卡秋亞、拉切爾）

  - （**相田愛**／**愛心天使**）

  - [艦隊Collection](../Page/艦隊Collection.md "wikilink")（旗風、Luigi
    Torelli／UIT-25／伊504\[113\]）

  - [闇影詩章](../Page/闇影詩章.md "wikilink")（冥府之長 艾夏、狂熱機士兵、迷宮探險家 科羅伊）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [秋之回憶 -無垢少女-](../Page/秋之回憶_-無垢少女-.md "wikilink")（久世奏波）
  - [Princess Connect！
    Re:Dive](../Page/超異域公主連結_Re:Dive.md "wikilink")（靜流\[114\]）
  - 少女與戰車 戰車夢幻大會戰（逸見艾麗卡\[115\]）
  - [秋之回憶 -無垢少女-](../Page/秋之回憶_-無垢少女-.md "wikilink")（久世奏波\[116\]）
  - 東京Conception（**和倉紗友里**\[117\]）
  - 藍寶石球體 ～蒼之境界～（提亞瑪特\[118\]）

### 廣播劇CD

  - （桃乃木優）

  - [千歲Get You\!\!](../Page/千歲Get_You!!.md "wikilink")（藤麻子）

  - （**敷島有紀**）

  - [CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink") 廣播劇CD「The parallel
    bootleg」（**蒼井瀨名**）

  - [CHAOS;HEAD 有聲系列
    完整BOX](../Page/CHAOS;HEAD.md "wikilink")（**蒼井瀨名**\[119\]）

  - （武屋真由、久我真）

  - [風之聖痕](../Page/風之聖痕.md "wikilink")（篠宮由香里）

  - （黑野美砂子）

  - [武裝鍊金](../Page/武裝鍊金.md "wikilink")（早-{坂}-櫻花）

  - （葛麗緹亞·黛德麗）

  - [Venus Versus
    Virus](../Page/Venus_Versus_Virus.md "wikilink")（名橋露琪亞）

  - [女王之刃](../Page/女王之刃.md "wikilink")（甲魔忍軍頭領 靜）

  - [最終神話戰爭伊特奧佩拉](../Page/大前茜#最終神話戰爭伊特奧佩拉.md "wikilink") 原聲廣播劇CD系列（）

      - 第1章 罅割
      - 第2章 彷徨冥界扉（伊蒂拉）
      - 第3章 輝悠遠女神

  - （安田史泰拉）

  - [聖痕鍊金士](../Page/聖痕鍊金士.md "wikilink") ～皇女之卵～ CD廣播劇專輯（織部真冬）

  - [我要成為世界最強偶像](../Page/我要成為世界最強偶像.md "wikilink")（西原涼）

  - [07-GHOST](../Page/07-GHOST.md "wikilink") 給神的情書（莉莉亞）

  - [桃組+戰記](../Page/桃組+戰記.md "wikilink")（柴浦清子）

  - [Fate/hollow ataraxia](../Page/Fate/hollow_ataraxia.md "wikilink")
    廣播劇CD =（**巴潔特·法迦·克米茲**）\[120\]

  - [PRISM ARK系列](../Page/PRISM_ARK.md "wikilink")（**神樂**）

      - PRISM ARK DRAMA CD「地獄修女 特里莎的變化」
      - PRISM ARK 彩虹～☆廣播劇CD 「校長的磨練場 PROVING GROUNDS OF THE "KOUCHOU
        SENSEI"」
      - PRISM ARK 彩虹～☆廣播劇CD2 「PRISM學藝會特別篇 新說？PRISM ARK！」
      - PRISM ARK 彩虹～☆廣播劇CD3 「超魔法合神 RISMAX ARK」

  - [乃木-{坂}-春香的秘密](../Page/乃木坂春香的秘密.md "wikilink")（綾瀨瑠子）

  - （深海渚／激爆藍）

  - [旋風管家](../Page/旋風管家.md "wikilink") 廣播劇CD（桂雪路）

      - 廣播劇CD 1 綾崎妙麗與秘密的課外活動
      - 廣播劇CD 2 白皇學院的巴士之旅與瑪莉亞的獨白

  - [信蜂](../Page/信蜂.md "wikilink") 廣播劇CD（內莉）

  - [落花流水](../Page/落花流水.md "wikilink")（綾瀨曉）

  - （高杉千鶴子）

  - 世界迷或劇場 童話「白雪公主」（**皇后**、鏡）

  - \#000000極限之黑系列（鳥丸寧）

      - \#000000極限之黑 Love Me Tender
      - \#000000極限之黑 Missing Link

  - [我的妹妹哪有這麼可愛！](../Page/我的妹妹哪有這麼可愛！#廣播劇CD.md "wikilink")（**槙島沙織**〈**沙織·巴吉納**〉）

  - [櫻花樹下的小惡魔](../Page/櫻花樹下的小惡魔.md "wikilink")（**花-{坂}-紫苑**）

  - （泉川老師）

  - [WHITE ALBUM2](../Page/白色相簿2#廣播劇CDD.md "wikilink") -introductory
    chapter- 「祭典之前 ～2人的24小時～」（**冬馬和紗**）

  - 「天河傳說殺人事件」（**水上秀美**）

  - （藍）

  - 你的侍奉只有這種程度嗎？（十三世）

  - [Walkure Romanze
    少女騎士物語](../Page/Walkure_Romanze_少女騎士物語.md "wikilink")（龍造寺茜\[121\]）

  - [南-{鎌}-倉高校女子自行車社](../Page/南鎌倉高校女子自行車社.md "wikilink")（森四季）\[122\]

  - [霧雨飄零之森](../Page/霧雨飄零之森.md "wikilink") 廣播劇CD 第一顆雨水（**神崎詩織**\[123\]）

  - [女子高校拷問部](../Page/女子高校拷問部.md "wikilink")（藤原志士子\[124\]）

### 廣播劇

  - [乃木坂春香的秘密](../Page/乃木坂春香的秘密.md "wikilink")（綾瀨瑠子，[電擊大賞](../Page/電擊大賞.md "wikilink")：2007年10月27日起，全4回）
  - [VOMIC](../Page/VOMIC.md "wikilink")
      - （篠原流香，：2008年2月7日－28日，全4回）

      - （小平瑠音，：2011年5月）

      - [黑鐵](../Page/黑鐵.md "wikilink")（**刀條小百合**，Sakiyomi
        Jum-Bang！：2012年5月）

### 外語配音

※作品依英文名稱及英文原名排序。

#### 電影

  - （Kim〈Noureen DeWulf 飾演〉）

  - （**Julie**〈Emma Lahana 主演〉）

  - （**Carla Miller**〈Aura Garrido 主演〉）

  - [飢餓遊戲系列](../Page/飢餓遊戲_\(電影系列\).md "wikilink")（Johanna Mason〈[Jena
    Malone](../Page/吉娜·瑪隆.md "wikilink") 飾演〉）

      - [飢餓遊戲：星火燎原](../Page/飢餓遊戲：星火燎原_\(電影\).md "wikilink")
      - [飢餓遊戲：自由幻夢I](../Page/飢餓遊戲：自由幻夢Ⅰ.md "wikilink")
      - [飢餓遊戲：自由幻夢 終結戰](../Page/飢餓遊戲：自由幻夢_終結戰.md "wikilink")

  - （**Beth Cooper**〈[Hayden Panettiere](../Page/希丹·彭尼蒂亞.md "wikilink")
    主演〉）

  - [神秘的古廟](../Page/神秘的古廟.md "wikilink")

  - [醜聞](../Page/醜聞_\(2003年電影\).md "wikilink")

#### 電視影集

  -
  - [灰姑娘的姐姐](../Page/灰姑娘的姐姐.md "wikilink")（**宋恩祖**〈[文瑾瑩](../Page/文瑾瑩.md "wikilink")
    主演〉）

  - [鐵證懸案 (第6季)](../Page/鐵證懸案.md "wikilink")（Alisson Thurston）

  - [嘻哈帝國](../Page/嘻哈帝國.md "wikilink")（Anika Calhoun〈Grace Gealey 飾演〉）

  - [華政](../Page/華政.md "wikilink")（**[貞明公主](../Page/貞明公主.md "wikilink")**〈[李沇熹](../Page/李沇熹.md "wikilink")
    主演〉\[125\]）

  - 黄飛鴻之理想年代

  - [失蹤現場 (第4季)](../Page/失蹤現場.md "wikilink")（Heidi、Loretta）－第90集。

#### 動畫

  - [麗莎和卡斯柏](../Page/麗莎和卡斯柏.md "wikilink")（Gaspard的母親、Victoria）

  - [淘氣小企鵝](../Page/小企鵝寶露露.md "wikilink")（**卡龍**\[126\]）

  - [變形金剛：領袖之證](../Page/變形金剛：領袖之證.md "wikilink")（June Darby\[127\]）

  - （Windblade）

      - （****）

  - [天才阿公](../Page/天才阿公.md "wikilink")（Aunt Grandma 等）

#### 其它

  - （**Linda Harrigan**）

### 特攝影集

  - 2015年

<!-- end list -->

  - [假面騎士Ghost](../Page/假面騎士Ghost.md "wikilink")（昆蟲眼魔的配音\[128\]）

### 實寫

  -
  -
  - 師株式社2

  -
  -
  - （2013年10月30日）

  - （AD哈力栗的聲音）

### 廣播節目

※記號表示說明網路廣播。

  - （2003年12月23日－2004年3月5日，[虎之穴公式官網](../Page/Comic虎之穴.md "wikilink")）※

  - 仁美與[有佳的](../Page/豬口有佳.md "wikilink")（2004年3月31日－2006年3月29日，[文化放送](../Page/文化放送_\(日本\).md "wikilink")）

  - （2004年8月17日－2005年1月18日，虎之穴公式官網）※

  - [極限女孩Radio](../Page/極限女孩.md "wikilink")（2005年1月－4月）

  - （2005年1月13日－3月31日，）

  - （2005年8月8日－2008年9月1日，虎之穴公式官網）※

  - （2005年11月3日－2007年6月28日，[音泉](../Page/音泉.md "wikilink")、※、※）

  - [女高中生digital電台](../Page/女子高生.md "wikilink")（2005年12月－2006年10月，）※

  - （2006年4月－11月，TE-A room）※

  - （2006年5月－12月，TE-A room）※

  - [一騎當千DDR ～Dragon Destiny
    Radio～](../Page/一騎當千.md "wikilink")（2006年11月－2008年2月，）※

  - （2007年5月23日－6月，音泉）※

  - GO！純情水泳社！ 夏季\!\! 彩排（2007年7月4日－10月，音泉）※

  - 一騎當千GGR ～Great Guardians Radio～（2008年3月－2009年3月，Media Factory
    Radio）※

  - （2008年7月－2009年4月，文化放送）※

  - 小生意氣\!\!（2008年9月18日－2009年7月，animate TV）※

  - [Radio Total
    Eclipse](../Page/Muv-Luv_Alternative_Total_Eclipse.md "wikilink")（2009年1月4日－2013年3月29日，文化放送）

  - [電台講座 簡單易懂的現代魔法
    第1](../Page/簡單易懂的現代魔法.md "wikilink")（2009年5月8日－12月25日，[簡單易懂的現代魔法公式官網](../Page/簡單易懂的現代魔法.md "wikilink")）※

  - 一騎當千 XTREAME XECUTOR RADIO（2009年12月25日－2010年12月3日，）※

  - 淺野真澄、生天目仁美的HiBiKi當千（2011年10月20日－2016年3月24日，HiBiKi Radio Station）※

  - [WHITE ALBUM2
    同好會電台](../Page/白色相簿2.md "wikilink")（2012年8月9日－2014年4月25日，音泉）※

  - WHITE ALBUM2 同好會電台 2018（2018年7月13日－2018年9月14日，音泉）※

### 廣播CD

  - 廣播CD「[站起來！我們是先導者](../Page/卡片戰鬥先導者.md "wikilink")」Vol.8（嘉賓身份出演)\[129\]
  - 廣播CD [WHITE ALBUM2 同好會電台](../Page/白色相簿2.md "wikilink")
    Vol.1－2\[130\]
  - [淺野真澄與生天目仁美的戀愛指南書](../Page/淺野真澄.md "wikilink")？、??

### DVD

  - Charinco

### 舞台

  - 缶

      - 缶「」（2011年5月31日－6月5日，新宿莫里哀劇院）

      - 缶二本目「」（2013年3月5日－10日，）

      - 缶三本目「君眉毛剃日」（2014年10月29日－11月3日，池袋綠色劇院 BIG TREE THEATER）

  - 第十回紀念公演「異說 [金瓶梅](../Page/金瓶梅.md "wikilink")」（2012年4月26日－30日，）

### 其它

  - Flash Novel （上尉）

  - （姬宮深藍與巧克力搖滾）

      - 標題曲「」為電視動畫《[乃木-{坂}-春香的秘密](../Page/乃木坂春香的秘密.md "wikilink")》片頭主題曲。B面歌曲「」用作PS2移植遊戲《乃木-{坂}-春香的秘密
        開始了，角色扮演♥》片頭主題曲。

  - 舞華蒼魔鏡（河城似鳥）

  - 東方二次創作同人動畫 夢想夏鄉 ～A Summer Day's Dream～（八意永琳）

  - [Go！Princess 光之美少女](../Page/Go!_Princess_光之美少女.md "wikilink")
    音樂劇Show 拯救公主樂園！（心天使）

  - 超級英雄&女英雄
    暑假特輯（2013年8月25日，[朝日電視台](../Page/朝日電視台.md "wikilink")，舞台上負責發出心天使的聲音）

  - 妄想☆Boatman 第11回、第12回（，妄想的聲音）

## 音樂作品

  -
    聲優組合生天目仁美與伊藤靜的歌曲請參照「」，N±的歌曲請參照「」。

### 單曲

| 枚數  | 發行日期                 | 單曲名稱      | 規格編號      | 最高排名 |
| --- | -------------------- | --------- | --------- | ---- |
| 1st | 2006年8月18日           | ****      | SDCR-0005 |      |
| 2nd | 2006年11月22日          | ****      |           |      |
| ※   | ****（Crescent Love ） | FCCM-0164 | 第77名      |      |

### 專輯

| 枚數  | 發行日期       | 專輯名稱 | 規格編號      | 最高排名 |
| --- | ---------- | ---- | --------- | ---- |
| 1st | 2007年5月25日 | **** | SDCR-0011 |      |

| 枚數  | 發行日期       | 專輯名稱 | 規格編號 | 最高排名 |
| --- | ---------- | ---- | ---- | ---- |
| 1st | 2006年2月24日 | **** |      |      |

迷你專輯

### 角色歌曲

  - 年份不明

<!-- end list -->

  - Haif Moon vol.1 featuring 生天目仁美「Palette」
  - Life ～Everyone has their own way of life

<!-- end list -->

  - 2004年

<!-- end list -->

  - 校園迷糊大王 漫畫形象專輯 Vol.3 周防美琴（9月23日）

<!-- end list -->

  - 2005年

<!-- end list -->

  - 校園迷糊大王：莎拉·亞迪耶瑪斯（4月27日）

  - （5月25日）

  - [極上生徒会 角色單曲 Vol.1 執行部書記 蘭堂梨乃&會長
    神宮司奏](../Page/極上生徒會.md "wikilink")（6月22日）

  - [嬉皮笑園](../Page/嬉皮笑園.md "wikilink") 歌唱專輯「學園天國」（10月26日）

<!-- end list -->

  - 2006年

<!-- end list -->

  - We Are\* Piece of Peace Vol.4 笛吹詩忍（9月8日）

<!-- end list -->

  - 2007年

<!-- end list -->

  - 角色歌曲精選輯 Vol.6 門田櫻（10月10日）

  - PRISM ARK PRIVATE SONG Vol.2

<!-- end list -->

  - 2008年

<!-- end list -->

  - （1月25日）

  - （7月23日）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [簡單易懂的現代魔法 SPECIAL CODE
    ALBUM](../Page/簡單易懂的現代魔法.md "wikilink")（9月30日）

  - （10月23日）

  - （12月23日）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [一起一起這裡那裡](../Page/一起一起這裡那裡.md "wikilink")（4月18日）

  - （5月16日）

  - [織田信奈的野望 歌姬02 Music of the different world
    前田犬千代／柴田勝家](../Page/織田信奈的野望#CD.md "wikilink")（9月5日）

<!-- end list -->

  - 2013年

<!-- end list -->

  - （7月17日）

  - （9月4日）

  - （10月23日）

  - （11月6日）

<!-- end list -->

  - 2014年

<!-- end list -->

  - （1月15日）

### 其它參加樂曲

  - 雪降歌 ～scene:X'mas～（2006年11月22日）

  - ～夏日歌日記～（2007年6月29日）

### 作詞

  - （2006年）

  - 卒業（2006年）

  - （2006年）

  - 旅立（2006年）

  - （2007年）

  - ～眼差～（2007年）

  - （2007年）

## 腳註

### 注釋

### 參考來源

## 外部連結

  - －生天目仁美的官方個人部落格。

  - [賢Production公式官網的聲優簡介](http://www.kenproduction.co.jp/talent/member.php?mem=w42)


  -

  -

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")
[Category:賢Production](../Category/賢Production.md "wikilink")
[Category:創價學會信徒](../Category/創價學會信徒.md "wikilink")

1.
2.

3.
4.
5.
6.
7.
8.
9.
10.

11.

12.
13.

14.

15.

16.
17. 廣播節目《[PONY CANYON STYLE
    MARUNABI\!?](../Page/PONY_CANYON_STYLE_MARUNABI！？.md "wikilink")》第36回（2005年5月31日播放）、49回（2005年8月30日播放）。

18.

19.

20.

21.

22.

23. 同時擔任第41話至第42話的標題命名和繪製第41話的插圖。

24.

25.

26. 第二奏的片尾曲工作人員表誤記成「**生田目仁美**」。

27.

28.

29.

30. 第63話「」工作人員表。

31.

32.

33.

34.

35.

36.

37.

38.

39.

40. 光之美少女10週年紀念的話。

41. 《[月刊Comp
    Ace](../Page/月刊Comp_Ace.md "wikilink")》2014年7月號，[角川書店](../Page/角川書店.md "wikilink")，2014年5月28日。

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54. 第11話客串。

55.

56.

57.

58.

59.

60.

61.

62.

63. 隨著單行本第17冊限定版的發行附贈OVA。

64. 隨著單行本第42、43冊特別版的發行附贈OVA。

65.

66. 隨著輕小說單行本第12冊限定版的發行附贈BD。

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81. 《[電擊PlayStation](../Page/電擊PlayStation.md "wikilink")》Vol.543，[ASCII
    Media Works](../Page/ASCII_Media_Works.md "wikilink")，2013年5月30日。

82.

83.

84.

85.

86. 《》2012年10月號，[enterbrain](../Page/enterbrain.md "wikilink")，2012年8月20日。

87.

88.

89.

90.

91.

92.

93.

94.

95.

96.

97.

98.

99.

100.

101.

102.

103.

104.

105.

106.

107.

108.

109.

110.

111.

112.

113. 《[Comptiq](../Page/Comptiq.md "wikilink")》2017年10月號，第8－25頁。

114.

115.

116.

117.

118.

119.

120. 隨著漫畫雜誌《[Comptiq](../Page/Comptiq.md "wikilink")》2015年1月號的發行附贈廣播劇CD。

121.

122. 隨著單行本第5冊初回限定版的發行附贈廣播劇CD。

123.

124.

125.

126.

127.

128.

129.

130.