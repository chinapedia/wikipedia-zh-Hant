[Flightless_Dung_Beetle_Circellium_Bachuss,_Addo_Elephant_National_Park,_South_Africa.JPG](https://zh.wikipedia.org/wiki/File:Flightless_Dung_Beetle_Circellium_Bachuss,_Addo_Elephant_National_Park,_South_Africa.JPG "fig:Flightless_Dung_Beetle_Circellium_Bachuss,_Addo_Elephant_National_Park,_South_Africa.JPG")
**生态位**（Ecological
niche），又称**小生境**、**生態區位**、**生态栖位**或是**生态龛位**，生态位是一个[物种所处的环境以及其本身生活习性的总称](../Page/物种.md "wikilink")。每个物种都有自己独特的生态位，借以跟其他物种作出区别。生态位包括该物种觅食的地点，食物的种类和大小，还有其每日的和季节性的[生物节律](../Page/生物节律.md "wikilink")。\[1\]

生态位的概念是由[Joseph
Grinnell於](../Page/:en:Joseph_Grinnell.md "wikilink")1917年首次提出的。这个概念在许多方面有广泛应用，比如[市场营销方面的](../Page/市场营销.md "wikilink")「[利基](../Page/利基.md "wikilink")」概念。

[群落生境](../Page/群落生境.md "wikilink")（其同义词为栖息地）只是生态位这个概念的一部分。生态位的含义远不止是“生活空间”（温度，[空气湿度等环境因素的综合](../Page/空气湿度.md "wikilink")，它是生物生存的依据）的一个抽象概念，它描述了一个物种在其群落生境中的**功能作用**，而且它带有构成群落生境的自然因素所留下的烙印。它是一个物种为求生存而所需的广义“资源”。例如：蝙蝠需要在某地夜间捕食蚊子。这里面某地的自然因素（例如空气质量，其他关系到蝙蝠栖息地的因素），蝙蝠夜间运动的可行性，蚊子都是蝙蝠的生态位的一部分。一个物种只能占有一个生态位。

生态位的环境因素（温度，食物，地表湿度，生存环境等）的综合，构成概念**生态位空间**。这是一种n[维](../Page/维度.md "wikilink")[超体积](../Page/超体积.md "wikilink")，但出于可视化的原因会将它简化为二维或三维龛位图进行显示。每种环境因素成为一个维度。在两个生态龛位中，考虑观察的维度越多，两个生态龛位的差别就越明显，越容易被区分开来。

生态位分两个层次：

  - **基本生态位**：是生态位空间的一部分，一个物种有在其中生存的可能。这个基本生态位是由物种的[变异和适应能力决定的](../Page/变异.md "wikilink")，而并非其地理因素。或者说基本生态位是实验室条件下的生态位，里面不存在捕食者和竞争。
  - **现实生态位**：是基本生态位的一部分，但考虑到生物因素和它们之间的相互作用。或者说是自然界中真实存在的生态位。

人们可以从特殊的性质或角度考虑，定义更多的生态位：

  - 营养生态位：根据营养情况划分的生态位。
  - 最小环境：对一个物种来说可持续生存的最小环境。

两个地区，虽然地理上被分隔，但却有着相似的非生物因素，在这两个地区生活的物种会占有相似的生态位。这会导致[趋同演化的发生](../Page/趋同演化.md "wikilink")，即是两个物种虽然无亲缘关系，但却各自独立的发展出相似的身体构造去适应环境。南极的捕鱼能手，不会飞的鸟——[企鹅和已](../Page/企鹅.md "wikilink")[灭绝的欧洲的](../Page/灭绝.md "wikilink")[大海雀一样占有相似的生态位](../Page/大海雀.md "wikilink")。澳洲的[袋鼹和欧洲的](../Page/袋鼹.md "wikilink")[田鼠都有挖土铲的前肢](../Page/田鼠.md "wikilink")，它们在泥土中挖掘通道，捕食细小的动物，它们占有相似的细小地下肉食性动物生态位。

对于一个地理上不存在分隔的生态系统中，在[生态平衡时](../Page/生态平衡.md "wikilink")，各个生物的生态位原则上不重合。若有重合，那么必然是不稳定的，它必然会通过物种间的竞争来削减生态位的重叠，直到平衡为止。[竞争](../Page/竞争.md "wikilink")，比如需要相似生态位的[入侵物种的进入](../Page/入侵物种.md "wikilink")，会导[原生物種](../Page/原生物種.md "wikilink")[存在区域减少](../Page/存在区域.md "wikilink")。如果存在区域太小，会导致一个物种的[灭绝](../Page/灭绝.md "wikilink")。这就是[竞争排除原则](../Page/竞争排除原则.md "wikilink")。

[进化导致的是两个有亲缘关系的物种去占据不同的生态位](../Page/进化.md "wikilink")，减少竞争的机会。一个很好的例子是[拉帕戈斯群岛的](../Page/拉帕戈斯群岛.md "wikilink")[達爾文雀的进化](../Page/達爾文雀.md "wikilink")（[适应辐射](../Page/适应辐射.md "wikilink")）。

换句话说，进化以及新的物种出现的重要原因是空余的生态位出现了，而这种情况大多是由于[灾难或者](../Page/灾难.md "wikilink")[地理分隔导致的](../Page/地理分隔.md "wikilink")。

生态位的数量与生态系统的[气候](../Page/气候.md "wikilink")，地理和生物因素有关。相应的物种数会因这些因素的差异导致有很大的不同。极地，例如[格陵兰的](../Page/格陵兰.md "wikilink")[冰川](../Page/冰川.md "wikilink")，[南极洲或是](../Page/南极洲.md "wikilink")[高原的生态位数就不及](../Page/高原.md "wikilink")[热带的](../Page/热带.md "wikilink")[原始森林或是](../Page/原始森林.md "wikilink")[珊瑚礁的多](../Page/珊瑚礁.md "wikilink")。

## 社会学上的应用

## 参看

  - [存在区域](../Page/存在区域.md "wikilink")
  - [灾变论](../Page/灾变论.md "wikilink")

[Category:生態學](../Category/生態學.md "wikilink")
[Category:景观生态学](../Category/景观生态学.md "wikilink")

1.