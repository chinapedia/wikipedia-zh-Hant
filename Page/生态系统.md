[Blue_Linckia_Starfish.JPG](https://zh.wikipedia.org/wiki/File:Blue_Linckia_Starfish.JPG "fig:Blue_Linckia_Starfish.JPG")是一個高生產力的[海洋生態系統](../Page/海洋生態系統.md "wikilink")。\[1\]\]\]
[River_gambia_Niokolokoba_National_Park.gif](https://zh.wikipedia.org/wiki/File:River_gambia_Niokolokoba_National_Park.gif "fig:River_gambia_Niokolokoba_National_Park.gif")生態系統有丰富[生物多样性](../Page/生物多样性.md "wikilink")。这是在[塞内加尔的](../Page/塞内加尔.md "wikilink")[尼奥科罗-科巴国家公园内的](../Page/尼奥科罗-科巴国家公园.md "wikilink")[冈比亚河](../Page/冈比亚河.md "wikilink")。\]\]

**生态系统**（）是指在一个特定环境内，相互作用的所有[生物和此一](../Page/生物.md "wikilink")[环境的統稱](../Page/环境.md "wikilink")。此特定環境裡的[非生物因子](../Page/非生物因子.md "wikilink")（例如[空氣](../Page/空氣.md "wikilink")、[水及](../Page/水.md "wikilink")[土壤等](../Page/土壤.md "wikilink")）與其間的生物之间具交互作用\[2\]，不斷地進行物质的交換和能量的傳遞，並藉由[物质流和](../Page/物质.md "wikilink")[能量流的连接](../Page/能量.md "wikilink")，而形成一個整体，即稱此為**生態系統**或**生態系**。

## 辭源

生態系统一詞，最早是由[英國的生物學家](../Page/英國.md "wikilink")所提出，意指由[物理因子與生物所構成的整個環境](../Page/物理性质.md "wikilink")。1936年提出，生态系统是指在一定的空间内生物成分和非生物成分通过物质循环和能量流动而互相作用、互相依存而构成的一个生态学功能单位。\[3\]

## 大小

生态系统的范围没有固定的大小，如一整个[森林可能是一个生态系统](../Page/森林.md "wikilink")，一个小池塘也可能是一个生态系统，在[南美](../Page/南美.md "wikilink")[亚马逊河流域](../Page/亚马逊河.md "wikilink")，有时一棵大树可能就是一个生态系统，大部分[动物终生不离开这棵树](../Page/动物.md "wikilink")。生态系统的边界一般是由于环境突然有很大变化造成的，如池塘外则不适合池塘内生物的生存，[沙漠边界](../Page/沙漠.md "wikilink")、[水体边界](../Page/水体.md "wikilink")、[山与](../Page/山.md "wikilink")[平原](../Page/平原.md "wikilink")、[沼泽的交界](../Page/沼泽.md "wikilink")，一般都是一个生态系统和另一个生态系统的交界。当然生态系统的边界不是绝对的，有很多生物可以越过边界生存，如青蛙在水中参与一个生态系统，也可以跳到岸上参与另一个生态系统。

## 分類

主要的生態系可為[森林生態系統](../Page/森林生態系統.md "wikilink")、[水域生態系統](../Page/水域生態系統.md "wikilink")、[陸域生態系統](../Page/陸域生態系統.md "wikilink")、[珊瑚礁生態系統等](../Page/珊瑚礁生態系統.md "wikilink")。[生物圈是地球上最大的生态系统](../Page/生物圈.md "wikilink")。

### 自然生态系统

如：[生物圈](../Page/生物圈.md "wikilink")、[海洋](../Page/海洋.md "wikilink")、[深海熱泉](../Page/深海熱泉.md "wikilink")、[珊瑚礁](../Page/珊瑚礁.md "wikilink")、陆地、[森林](../Page/森林.md "wikilink")、[雨林](../Page/雨林.md "wikilink")、[草原](../Page/草原.md "wikilink")、[湖泊](../Page/湖泊.md "wikilink")、[池塘](../Page/池塘.md "wikilink")、[湿地](../Page/湿地.md "wikilink")……
可依環境特性分為[陸域生態系及](../Page/陸域生態系.md "wikilink")[水域生態系兩大部分](../Page/水域生態系.md "wikilink")，下再細分成其他各個生態系。

### 人工生态系统

如：农田、果园、自给自足的宇宙飞船、用于验证生态学原理的各种封闭的微宇宙……

## 平衡

一个生态系统内，各种生物之间以及和环境之间是存在一种平衡关系的，任何外来的物种或物质侵入这个生态系统，都会破坏这种平衡，平衡被破坏后，可能会逐渐达到另一种平衡关系。但如果生态系统的平衡被严重地破坏，可能会造成永久的失衡。

## 人類活動的影響

[人类活动是造成生态系统失衡的最主要因素](../Page/人类.md "wikilink")。[农业活动就是人类破坏自然生态系统](../Page/农业.md "wikilink")，创造自己的人为生态系统的范例，人类开垦荒地，将原有生态系统破坏，原有的物种无法继续生存，人类种植自己需要的庄稼，随之而来的是害虫和捕食害虫的动物，以及为避免被人类锄草而生长的和庄稼形状非常相似的杂草，这些物种形成一个新的生态系统，人为生态系统如果离开人的活动就无法继续存在下去，如果人将[农田抛荒](../Page/农田.md "wikilink")，很快就会重新产生新的生态系统，即杂草遍地，也许会[沙漠化](../Page/沙漠.md "wikilink")，但不会自动恢复到原有的生态系统。

有时人类活动造成物种的长距离迁移，也可能会对生态系统造成人类没有预料到的破坏，[老鼠和](../Page/老鼠.md "wikilink")[苍蝇随着人类散布到全世界](../Page/苍蝇.md "wikilink")，人类只是随着兴趣带到[澳大利亚的](../Page/澳大利亚.md "wikilink")32对[兔子](../Page/兔子.md "wikilink")，给澳洲的生态系统几乎造成毁灭性的灾难。目前外来物种[紫茎泽兰](../Page/紫茎泽兰.md "wikilink")、[大米草](../Page/大米草.md "wikilink")、和[水葫芦已经开始对](../Page/水葫芦.md "wikilink")[中国大陆的生态系统造成威胁](../Page/中国.md "wikilink")，同时随着商品进入的[美国白蛾等新型害虫也开始适应新的环境](../Page/美国白蛾.md "wikilink")。

人类活动造成的[环境污染也会从物质和能量方面破坏生态系统的平衡](../Page/环境污染.md "wikilink")，有是会造成永久性的破坏。

人类目前已经认识到生态系统平衡被破坏的后果，正在力图帮助恢复其平衡，但这需要付出资金和能量，恢复比破坏要困难的多，而不会有[经济上的回报](../Page/经济.md "wikilink")，所以一般经济实体不会自动的去做这种事，需要政府和志愿者的投入，也需要舆论和教育的促进。

生態系統可透過[生態球進行模擬](../Page/生態球.md "wikilink")。

## 注釋

<references />

## 參考

  - [生物群系](../Page/生物群系.md "wikilink")
  - [生物圈](../Page/生物圈.md "wikilink")
  - [人類生態學](../Page/人類生態學.md "wikilink")
  - [地球科学](../Page/地球科学.md "wikilink")
  - [生態系統列表](../Page/生態系統列表.md "wikilink")

[生態系統](../Category/生態系統.md "wikilink")
[Category:生物系统](../Category/生物系统.md "wikilink")
[Category:超个体](../Category/超个体.md "wikilink")
[Category:共生](../Category/共生.md "wikilink")
[Category:系統生態學](../Category/系統生態學.md "wikilink")
[Category:自然史](../Category/自然史.md "wikilink")

1.
2.  ”Biology Concepts & Connections Sixth Edition”, Campbell, Neil A.
    (2009), page 2, 3 and G-9. Retrieved 2010-06-14.
3.  。