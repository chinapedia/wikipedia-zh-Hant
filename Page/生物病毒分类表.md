## [病毒](../Page/病毒.md "wikilink")（Viruses）

### [DNA病毒](../Page/去氧核糖核酸病毒.md "wikilink")（DNA Viruses）

#### [雙鏈DNA病毒](../Page/雙鏈DNA病毒.md "wikilink")（dsDNA Viruses）

  - [有尾噬菌體目](../Page/有尾噬菌體目.md "wikilink") Caudovirales
      - [肌尾噬菌體科](../Page/肌尾噬菌體科.md "wikilink") Myoviridae
      - [長尾噬菌體科](../Page/長尾噬菌體科.md "wikilink") Siphoviridae
      - [短尾噬菌體科](../Page/短尾噬菌體科.md "wikilink") Podoviridae
      - 科未定
  - [皰疹病毒目](../Page/皰疹病毒目.md "wikilink") Herpesvirales
      - [魚皰疹病毒科](../Page/魚皰疹病毒科.md "wikilink") Alloherpesviridae
      - [皰疹病毒科](../Page/皰疹病毒科.md "wikilink") Herpesviridae
      - Malacoherpesviridae
      - 科未定
  - Ligamenvirales
      - [脂毛噬菌體科](../Page/脂毛噬菌體科.md "wikilink") Lipothrixviridae
      - [古噬菌體科](../Page/古噬菌體科.md "wikilink") Rudiviridae
  - 目未定
      - [腺病毒科](../Page/腺病毒科.md "wikilink") Adenoviridae
      - Ampullaviridae
      - [囊泡病毒科](../Page/囊泡病毒科.md "wikilink") Ascoviridae
      - [非洲豬瘟病毒科](../Page/非洲豬瘟病毒科.md "wikilink") Asfarviridae
      - [棒狀病毒科](../Page/棒狀病毒科.md "wikilink") Baculoviridae
      - Bicaudaviridae
      - Clavaviridae
      - [覆蓋噬菌體科](../Page/覆蓋噬菌體科.md "wikilink") Corticoviridae
      - [微小紡錘形噬菌體科](../Page/微小紡錘形噬菌體科.md "wikilink") Fuselloviridae
      - Globuloviridae
      - [滴狀病毒科](../Page/滴狀病毒科.md "wikilink") Guttaviridae
      - Hytrosaviridae
      - [虹彩病毒科](../Page/虹彩病毒科.md "wikilink") Iridoviridae
      - Mimiviridae
      - [線極病毒科](../Page/線極病毒科.md "wikilink") Nimaviridae
      - [乳頭瘤病毒科](../Page/乳頭瘤病毒科.md "wikilink") Papillomaviridae
      - [藻類去氧核糖核酸病毒科](../Page/藻類去氧核糖核酸病毒科.md "wikilink") Phycodnaviridae
      - [芽生噬菌體科](../Page/芽生噬菌體科.md "wikilink") Plasmaviridae
      - [多去氧核糖核酸病毒科](../Page/多去氧核糖核酸病毒科.md "wikilink") Polydnaviridae
      - [多瘤病毒科](../Page/多瘤病毒科.md "wikilink") Polyomaviridae
      - [痘病毒科](../Page/痘病毒科.md "wikilink") Poxviridae
      - [復層噬菌體科](../Page/復層噬菌體科.md "wikilink") Tectiviridae
      - 科未定

#### [單鏈DNA病毒](../Page/單鏈DNA病毒.md "wikilink")（ssDNA Viruses）

  - Anelloviridae
  - Bacillariodnaviridae
  - Bidnaviridae
  - [圓環病毒科](../Page/圓環病毒科.md "wikilink") Circoviridae
  - [聯體病毒科](../Page/聯體病毒科.md "wikilink") Geminiviridae
  - [絲狀噬菌體科](../Page/絲狀噬菌體科.md "wikilink") Inoviridae
  - [微小噬菌體科](../Page/微小噬菌體科.md "wikilink") Microviridae
  - [矮化病毒科](../Page/矮化病毒科.md "wikilink") Nanoviridae
  - [小DNA病毒科](../Page/小DNA病毒科.md "wikilink") Parvoviridae
  - 科未定

### [RNA病毒](../Page/核糖核酸病毒.md "wikilink")（RNA viruses）

#### [雙鏈RNA病毒](../Page/雙鏈RNA病毒.md "wikilink")（dsRNA Viruses）

  - [雙核糖核酸病毒科](../Page/雙核糖核酸病毒科.md "wikilink") Birnaviridae
  - [金色病毒科](../Page/金色病毒科.md "wikilink") Chrysoviridae
  - [囊狀噬菌體科](../Page/囊狀噬菌體科.md "wikilink") Cystoviridae
  - [內源核糖核酸病毒科](../Page/內源核糖核酸病毒科.md "wikilink") Endornaviridae
  - [低毒性病毒科](../Page/低毒性病毒科.md "wikilink") Hypoviridae
  - [巨大雙分核糖核酸病毒科](../Page/巨大雙分核糖核酸病毒科.md "wikilink") Megabirnaviridae
  - [分體病毒科](../Page/分體病毒科.md "wikilink") Partitiviridae
  - [小雙節病毒科](../Page/小雙節病毒科.md "wikilink") Picobirnaviridae
  - Quadriviridae
  - [呼腸孤病毒科](../Page/呼腸孤病毒科.md "wikilink") Reoviridae
  - [整體病毒科](../Page/整體病毒科.md "wikilink") Totiviridae
  - 科未定

#### [单链RNA病毒](../Page/单链RNA病毒.md "wikilink")（ssRNA Viruses）

  - [網巢病毒目](../Page/網巢病毒目.md "wikilink") Nidovirales
      - [動脈炎病毒科](../Page/動脈炎病毒科.md "wikilink") Arteriviridae
      - [冠狀病毒科](../Page/冠狀病毒科.md "wikilink") Coronaviridae
      - [海洋病毒科](../Page/海洋病毒科.md "wikilink") Mesoniviridae
      - [桿狀套病毒科](../Page/桿狀套病毒科.md "wikilink") Roniviridae
      - 科未定
  - [小核糖核酸病毒目](../Page/小核糖核酸病毒目.md "wikilink") Picornavirales
      - [二順反子病毒科](../Page/二順反子病毒科.md "wikilink") Dicistroviridae
      - [傳染性軟化病毒科](../Page/傳染性軟化病毒科.md "wikilink") Iflaviridae
      - [海洋RNA病毒科](../Page/海洋RNA病毒科.md "wikilink") Marnaviridae
      - [微小核糖核酸病毒科](../Page/微小核糖核酸病毒科.md "wikilink") Picornaviridae
      - [伴生豇豆病毒科](../Page/伴生豇豆病毒科.md "wikilink") Secoviridae
      - 科未定
  - [蕪菁黃花葉病毒目](../Page/蕪菁黃花葉病毒目.md "wikilink") Tymovirales
      - [甲型線形病毒科](../Page/甲型線形病毒科.md "wikilink") Alphaflexiviridae
      - [乙型線形病毒科](../Page/乙型線形病毒科.md "wikilink") Betaflexiviridae
      - [丙型線形病毒科](../Page/丙型線形病毒科.md "wikilink") Gammaflexiviridae
      - [蕪菁發黃鑲嵌病毒科](../Page/蕪菁發黃鑲嵌病毒科.md "wikilink") Tymoviridae
      - 科未定
  - [單股反鏈病毒目](../Page/單股反鏈病毒目.md "wikilink") Mononegavirales
      - [玻那病毒科](../Page/玻那病毒科.md "wikilink") Bornaviridae
      - [纖維病毒科](../Page/纖維病毒科.md "wikilink") Filoviridae
      - [副黏液病毒科](../Page/副黏液病毒科.md "wikilink") Paramyxoviridae
      - [炮彈病毒科](../Page/炮彈病毒科.md "wikilink") Rhabdoviridae
      - 科未定
  - 目未定
      - Alphatetraviridae
      - Alvernaviridae
      - [星狀病毒科](../Page/星狀病毒科.md "wikilink") Astroviridae
      - [桿菌狀核糖核酸病毒科](../Page/桿菌狀核糖核酸病毒科.md "wikilink") Barnaviridae
      - [甜菜壞死黃脈病毒科](../Page/甜菜壞死黃脈病毒科.md "wikilink") Benyviridae
      - [雀麥花葉病毒科Bromoviridae](../Page/雀麥花葉病毒科.md "wikilink")
      - [杯狀病毒科](../Page/杯狀病毒科.md "wikilink") Caliciviridae
      - Carmotetraviridae
      - [修道院病毒科](../Page/修道院病毒科.md "wikilink") Closteroviridae
      - [黃病毒科](../Page/黃病毒科.md "wikilink") Flaviviridae
      - [肝炎病毒科](../Page/肝炎病毒科.md "wikilink") Hepeviridae
      - [光滑病毒科](../Page/光滑病毒科.md "wikilink") Leviviridae
      - [黃症病毒科](../Page/黃症病毒科.md "wikilink") Luteoviridae
      - [裸露核糖核酸病毒](../Page/裸露核糖核酸病毒.md "wikilink") Narnaviridae
      - [野田病毒科](../Page/野田病毒科.md "wikilink") Nodaviridae
      - Permutotetraviridae
      - [馬鈴薯Y病毒科](../Page/馬鈴薯Y病毒科.md "wikilink") Potyviridae
      - [披衣病毒科](../Page/披衣病毒科.md "wikilink") Togaviridae
      - [番茄叢矮病毒科](../Page/番茄叢矮病毒科.md "wikilink") Tombusviridae
      - [帚狀病毒科](../Page/帚狀病毒科.md "wikilink") Virgaviridae
      - [沙狀病毒科](../Page/沙狀病毒科.md "wikilink") Arenaviridae
      - [本雅病毒科](../Page/本雅病毒科.md "wikilink") Bunyaviridae
      - [蛇形病毒科](../Page/蛇形病毒科.md "wikilink") Ophioviridae
      - [正黏液病毒科](../Page/正黏液病毒科.md "wikilink") Orthomyxoviridae
      - 科未定

### [逆轉錄病毒](../Page/逆轉錄病毒.md "wikilink")（Reverse Transcribing Viruses）

  - Caulimoviridae
  - Hepadnaviridae
  - Metaviridae
  - Retroviridae
  - 科未定

## [亞病毒因子](../Page/亞病毒因子.md "wikilink")（Subviral Agents）

### [衛星](../Page/衛星_\(亞病毒\).md "wikilink")（Satellites）

  - 卫星病毒 Satellite virus
      - 单链核糖核酸卫星病毒 Single-stranded RNA satellite virus
      - 双链DNA卫星病毒 Double-stranded DNA satellite virus
      - 属未定
  - Satellite nucleic acid
      - Single-stranded satellite DNA
      - Double-stranded satellite RNA
      - Single-stranded satellite RNA
      - RNA satellite
      - 属未定

### [類病毒](../Page/類病毒.md "wikilink")（Viroids）

  - Pospiviroidae
      - Pospiviroid
      - Hostuviroid
      - Cocadviroid
      - Apscaviroid
      - Coleviroid
      - 属未定
  - Avsunviroidae
      - Avsunviroid
      - Pelamoviroid
      - Elaviroid
  - 科未定

### [朊毒體](../Page/朊毒體.md "wikilink")（Prions）

  - Mammal-prion
  - Fungi-prion

### [干扰缺损病毒](../Page/干扰缺损病毒.md "wikilink")（Defective Interfering）

  - 缺损干扰RNA Defective Interfering RNA
  - Defective interfering particle

[Category:病毒学](../Category/病毒学.md "wikilink")
[Category:生物學列表](../Category/生物學列表.md "wikilink")
[Category:病毒](../Category/病毒.md "wikilink")