[SawachiDish_Sashimi01.jpg](https://zh.wikipedia.org/wiki/File:SawachiDish_Sashimi01.jpg "fig:SawachiDish_Sashimi01.jpg")[Korean_food_4.JPG](https://zh.wikipedia.org/wiki/File:Korean_food_4.JPG "fig:Korean_food_4.JPG")
[鮭魚生魚片.jpg](https://zh.wikipedia.org/wiki/File:鮭魚生魚片.jpg "fig:鮭魚生魚片.jpg")生魚片\]\]
**生魚片**，又称“**鱼生**”，古稱**魚膾**、**膾**或**鱠**，是以新鮮的[魚](../Page/魚.md "wikilink")[貝類生切成片](../Page/貝.md "wikilink")，蘸[調味料食用的](../Page/調味料.md "wikilink")[食物總稱](../Page/食物.md "wikilink")。在[中國大陸](../Page/中國大陸.md "wikilink")、[日本](../Page/日本.md "wikilink")、[朝鮮半島甚至歐洲等地都有類似的吃法](../Page/朝鮮半島.md "wikilink")，可說是相當國際化的一種料理。
-{H|魚生=\>zh-tw:生魚片;}-

吃生魚片有低機率感染[廣節裂頭絛蟲](../Page/絛蟲.md "wikilink")，所以一般只能吃[海水魚的生魚片](../Page/海水魚.md "wikilink")、且一定要經過低溫冷凍或其他殺菌處理。

## 生食規範

  - 捕獲時需及時分離易有寄生蟲的的部位，並儘量以完全海水水產魚貝類為主，因能於淡水生活之水產與陸地生物較多共通寄生蟲。並維持低溫。
  - 為預防海獸胃線蟲，需經冷凍使中心溫度低於攝氏零下20度24小時，歐盟亦同。（美國規範壽司用生食水產須經冷凍使中心溫度低於攝氏零下35度15小時或零下20度7日）
    國際食品法典規範生食用水產為殺死寄生蟲須以攝氏零下20度7日，或是攝氏零下35度20小時\[1\]\[2\]
  - 東京都是要求生食用海水水產須經冷凍使中心溫度低於攝氏零下20度48小時，陸生動物肉則不建議生食。\[3\]\[4\]
  - 為預防旋尾線蟲，需經冷凍使中心溫度低於攝氏零下30度4日（等同殺蟲效果：攝氏零下35度15小時或零下40度40分）。\[5\])

美國FDA有列出各種魚種的風險比如寄生蟲，污染物等。\[6\]

## 生魚片在中國

### 先秦時代

**膾**最初的意思是指切細的生肉，《[汉书](../Page/汉书.md "wikilink")·[东方朔传](../Page/东方朔.md "wikilink")》：“生肉为脍。”《[礼记](../Page/礼记.md "wikilink")·内则》：“肉腥细者为脍。”有的肉在蒸煮烹饪以后就丧失了原味，不夠鮮嫩，鲜鱼就是其中一種。

[中國早於](../Page/中國.md "wikilink")[周朝就已有吃生魚片](../Page/周朝.md "wikilink")（魚膾）的記載，最早可追溯至[周宣王五年](../Page/周宣王.md "wikilink")（公元前823年）。出土青銅器「兮甲盤」的銘文記載，當年周師於[彭衙](../Page/彭衙.md "wikilink")（今[陕西](../Page/陕西.md "wikilink")[白水縣之內](../Page/白水縣.md "wikilink")）迎擊[獫狁](../Page/獫狁.md "wikilink")，凱旋。大將[尹吉甫私宴張仲及其他友人](../Page/尹吉甫.md "wikilink")，主菜是燒[甲魚加生](../Page/甲魚.md "wikilink")[鯉魚片](../Page/鯉魚.md "wikilink")。《[詩經](../Page/詩經.md "wikilink")·小雅·六月》記載了這件事：「飲御諸友，炰鱉膾鯉」，「膾鯉」就是生鯉魚。《[禮記](../Page/禮記.md "wikilink")》又有：「膾，春用蔥，秋用芥」，《[論語](../Page/論語.md "wikilink")》中又有對膾等食品「不得其醬不食」\[7\]的記述，故先秦之時的生魚膾當用加[蔥](../Page/蔥.md "wikilink")、[芥的醬來調味](../Page/芥.md "wikilink")。《[孟子](../Page/孟子.md "wikilink")·盡心下》亦有提及膾\[8\]，這亦是[成語](../Page/成語.md "wikilink")「膾炙人口」的由來，原意是指膾炙的美味，後來指作品受歡迎和為人熟悉。

關於中國南方食用生魚片的記載，最早追溯至[東漢](../Page/東漢.md "wikilink")[趙曄的](../Page/趙曄.md "wikilink")《[吳越春秋](../Page/吳越春秋.md "wikilink")》，據《吳越春秋·闔閭內傳》所載，[吳軍攻破](../Page/吳國.md "wikilink")[楚](../Page/楚.md "wikilink")[郢都後](../Page/郢都.md "wikilink")，[吳王闔閭設魚膾席慰勞](../Page/吳王闔閭.md "wikilink")[伍子胥](../Page/伍子胥.md "wikilink")，吳地才有了魚膾，當時是公元前505年。

### 秦漢魏晉南北朝

[秦](../Page/秦朝.md "wikilink")[漢之後](../Page/漢朝.md "wikilink")，[牛](../Page/牛.md "wikilink")、[羊等家畜和野獸的膾漸少見](../Page/羊.md "wikilink")，膾通常都是魚膾，又衍生出一個「鱠」字專指生魚片。「膾」和「鱠」經常混用，但不可與表示用火加工食物的「燴」字混淆。

[東漢時](../Page/東漢.md "wikilink")，[廣陵](../Page/廣陵.md "wikilink")[太守](../Page/太守.md "wikilink")[陳登很愛吃生魚膾](../Page/陳登.md "wikilink")，因為過量食用而得[肠道传染病及](../Page/肠道传染病.md "wikilink")[寄生虫一类的重病](../Page/寄生虫.md "wikilink")，后经名醫[华佗医治才康复](../Page/华佗.md "wikilink")，但他康復後仍然繼續吃生魚片，終因為貪吃生魚片而死。\[9\]

魚膾在古代是很普遍的食品，[東漢](../Page/東漢.md "wikilink")[應劭在](../Page/應劭.md "wikilink")《[風俗通義](../Page/風俗通義.md "wikilink")》收錄了各地的風俗習慣和奇人奇事，其中一條是：「[祝阿](../Page/祝阿.md "wikilink")（今[山東](../Page/山東.md "wikilink")[齊河縣](../Page/齊河縣.md "wikilink")[祝阿鎮](../Page/祝阿鎮.md "wikilink")）不食生魚」。代表了[應劭認為不食生魚是奇風異俗](../Page/應劭.md "wikilink")。祝阿人這個習俗一直堅持到隋朝，在《隋書·地理志》中亦有記載。

[三国](../Page/三国.md "wikilink")[魏的](../Page/曹魏.md "wikilink")[曹植也喜歡吃魚生](../Page/曹植.md "wikilink")，他的《[名都篇](../Page/名都篇.md "wikilink")》裡有：“脍鲤臇胎虾，炮鳖炙熊蹯」，把魚生蘸着小蝦醬吃。

[南北朝時](../Page/南北朝.md "wikilink")，出現[金齏玉膾](../Page/金齏玉膾.md "wikilink")，是中國古代生魚片菜色中最著名的，此名稱出現在北魏[賈思勰所著](../Page/賈思勰.md "wikilink")《[齊民要術](../Page/齊民要術.md "wikilink")》書中。在“八和齏”一節裏詳細地介紹了金齏的做法。「[八和齏](../Page/八和齏.md "wikilink")」是一種[調味品](../Page/調味品.md "wikilink")，是用[蒜](../Page/蒜.md "wikilink")、[薑](../Page/薑.md "wikilink")、[橘](../Page/橘.md "wikilink")、[白梅](../Page/白梅.md "wikilink")、[熟粟黃](../Page/熟粟黃.md "wikilink")、[粳米飯](../Page/粳米飯.md "wikilink")、[鹽](../Page/食盐.md "wikilink")、[醬八種料製成的](../Page/醬.md "wikilink")，用來蘸魚膾。

### 隋唐五代

[隋朝時](../Page/隋朝.md "wikilink")，[隋煬帝到江都](../Page/隋煬帝.md "wikilink")，吳郡松江獻[鱸魚](../Page/鱸魚.md "wikilink")，煬帝說：「所謂金齏玉膾，東南佳味也。」可見隋煬帝也很喜歡吃魚膾。除了蘸醬佐食外，亦有並用各種[生菜拌食的食法](../Page/生菜.md "wikilink")，這種食法還很講究色彩和造型上的視覺美感。

[唐是食用生魚片的高峰期](../Page/唐朝.md "wikilink")，有不少詩詞反映魚膾的流行程度。[李白的](../Page/李白.md "wikilink")《[魯中都有小吏逢七朗以鬥酒雙魚贈余於逆旅因鱠魚飲酒留詩而去](../Page/魯中都有小吏逢七朗以鬥酒雙魚贈余於逆旅因鱠魚飲酒留詩而去.md "wikilink")》於詩題就提及生魚片；[王維在](../Page/王維.md "wikilink")《[洛陽女兒行](../Page/洛陽女兒行.md "wikilink")》詩中寫道「侍女金盤膾鯉魚」；[王昌齡的](../Page/王昌齡.md "wikilink")《[送程六](../Page/送程六.md "wikilink")》詩道「青魚雪落鱠橙虀」；[白居易的](../Page/白居易.md "wikilink")《[輕肥](../Page/輕肥.md "wikilink")》詩就提到：「膾切天池鱗」，又有《松江亭攜樂觀漁宴宿》寫道：「朝盤鱠紅鯉」；晚唐[夏彥謙的](../Page/夏彥謙.md "wikilink")《夏日訪友》詩則有「冰鯉斫銀鱠」；[五代](../Page/五代.md "wikilink")[後蜀君主](../Page/後蜀.md "wikilink")[孟昶寵妃](../Page/孟昶.md "wikilink")[花蕊夫人的](../Page/花蕊夫人.md "wikilink")《[宮詞](../Page/宮詞.md "wikilink")》亦提到「日午殿頭宣索鱠」。可見唐至五代時，生魚片不但是宮廷中常見的食品，也是平民的日常菜餚，甚至出遊時也會就地取材。

### 宋遼金元

[宋朝時食用魚膾依然很普遍](../Page/宋朝.md "wikilink")，文献中有名可吃的鱼脍达三十八种，如“鱼鳔二色脍”、“红丝水晶脍”、“鲜虾蹄子脍”、“鲫鱼脍”、“沙鱼脍”、“水母脍”、“三珍脍”等。相传[扬州城内](../Page/扬州.md "wikilink")[梅圣俞家有一](../Page/梅圣俞.md "wikilink")[女傭擅做鱼生](../Page/女傭.md "wikilink")，[欧阳修知道後每就常常帶鲜鱼找那位女傭为他做鱼生](../Page/欧阳修.md "wikilink")。此外，[蘇軾與](../Page/蘇軾.md "wikilink")[陸游都喜歡吃生魚片](../Page/陸游.md "wikilink")，他們現存的與魚膾有關的詩詞就分別有十三首和三十七首之多。蘇軾的《將之湖州戲贈莘老》列舉了湖州的美味，其中一句是「吳兒膾縷薄欲飛」，就是指湖州的生魚片。陸游《[秋郊有懷四首](../Page/秋郊有懷四首.md "wikilink")》提到「縷飛綠鯽膾，花簇赬鯉鮓」，又有《[醉中懷江湖舊遊偶作短歌](../Page/醉中懷江湖舊遊偶作短歌.md "wikilink")》寫道「野魚可膾菰可烹」。

[金朝女真人亦有食用生魚片的習慣](../Page/金朝.md "wikilink")，據[南宋史家](../Page/南宋.md "wikilink")[徐夢莘的](../Page/徐夢莘.md "wikilink")《[三朝北盟會編](../Page/三朝北盟會編.md "wikilink")》記載，女真人於入主中原前就有以生魚片作為菜餚：「止以魚生、獐生，間用燒肉」。金朝末年的名醫[張從正在醫書](../Page/張從正.md "wikilink")《[儒門事親](../Page/儒門事親.md "wikilink")》中記載了女真入主中原後的飲食，他說：「又如北方貴人，愛食乳酪、牛酥、羊生、魚膾、鹿脯、豬臘、海味甘肥之物」就是女真貴族，入主中原後仍然喜愛生魚片。

[元朝時宮廷也有生魚片菜餚](../Page/元朝.md "wikilink")，[蒙古](../Page/蒙古.md "wikilink")[太醫](../Page/太醫.md "wikilink")[忽思慧的](../Page/忽思慧.md "wikilink")《[飲膳正要](../Page/飲膳正要.md "wikilink")·聚珍異饌》篇著錄了他幾代元朝[皇帝的菜譜](../Page/皇帝.md "wikilink")，其中一道菜就是魚膾，是以生[鯉魚片](../Page/鯉魚.md "wikilink")，加入[芥末爆炒過的薑絲](../Page/芥末.md "wikilink")、蔥絲、[蘿蔔絲和](../Page/蘿蔔.md "wikilink")[香菜絲](../Page/香菜.md "wikilink")，經胭脂著色，用鹽、醋提味。而《食物相反》、《食物中毒》、《魚品》諸篇亦論及魚膾。[元曲中亦有關於魚膾的內容](../Page/元曲.md "wikilink")，[關漢卿的](../Page/關漢卿.md "wikilink")[雜劇](../Page/雜劇.md "wikilink")《[望江亭中秋切膾旦](../Page/望江亭中秋切膾旦.md "wikilink")》，就有譚記兒喬扮漁婦，為楊衙內切膾的情節。[張可久的](../Page/張可久.md "wikilink")《南呂·閱金經·湖上書事》有「玉手銀絲膾」。

### 明清

[明朝](../Page/明朝.md "wikilink")[刘伯温把鱼生的製作方法写进](../Page/刘伯温.md "wikilink")《[多能鄙事](../Page/多能鄙事.md "wikilink")》一文中：「鱼不拘大小，以鲜活为上，去头尾，肚皮，薄切摊白纸上晾片时，细切为丝，以萝卜细剁姜丝拌鱼入碟，杂以生菜、芥辣、醋浇。」但當時魚膾流行的程度已經大為降低，雖然不少白話[小說如](../Page/小說.md "wikilink")《[三國演義](../Page/三國演義.md "wikilink")》、《[水滸傳](../Page/水滸傳.md "wikilink")》、《[喻世明言](../Page/喻世明言.md "wikilink")》、《[二刻拍案驚奇](../Page/二刻拍案驚奇.md "wikilink")》等都有描繪食用生魚片，但都是前朝之事，不能確定是原始話本的遺存，還是當時社會風俗的反映，而其他明代長篇小說如《金瓶梅》、《西遊記》、《封神演義》及短篇小說集如《醒世恒言》、《警世通言》、《初刻拍案驚奇》裏，都沒有提及魚膾，在小說中出現的頻率亦遠低於在唐宋詩詞。但[李時珍的](../Page/李時珍.md "wikilink")《[本草綱目](../Page/本草綱目.md "wikilink")》仍有記載魚膾：「劊切而成，故謂之膾，凡諸魚鮮活者，薄切洗淨血腥，沃以蒜齏薑醋五味食之」。

約在明清之交，禽、獸[肉膾已經消失了](../Page/肉膾.md "wikilink")，但清代文獻仍然有魚膾的記載，[高士奇的](../Page/高士奇.md "wikilink")《[西苑侍直](../Page/西苑侍直.md "wikilink")》詩有：「霑恩饌給銀絲膾」，所記的是[康熙十八年與十九年](../Page/康熙.md "wikilink")（1679和1680年）夏天的事，可知清宮中有生魚片供應。生活在江浙一帶的名醫[王士雄](../Page/王士雄.md "wikilink")（1808－1868年）在所著《[隨息居飲食譜](../Page/隨息居飲食譜.md "wikilink")》裏也對魚膾進行了論述。[李調元](../Page/李調元.md "wikilink")《南越筆記》亦記載：「粤俗嗜鱼生」\[10\]反映魚生以殘餘的形态繼續流行於江南和嶺南地區。

### 近現代

現代中國北方[滿族和](../Page/滿族.md "wikilink")[赫哲族的一些村落仍然有吃生魚片的習俗](../Page/赫哲族.md "wikilink")，南方某些[漢族聚居區亦遺留吃生魚片的習俗](../Page/漢族.md "wikilink")，但是大多數海內外華人的意識裏，生魚片是[日本料理](../Page/日本料理.md "wikilink")，很少人知道中國也有類似的料理。

## 東亞生魚片文化

### 嶺南魚生

[Japanese_butcher.jpg](https://zh.wikipedia.org/wiki/File:Japanese_butcher.jpg "fig:Japanese_butcher.jpg")
魚生古稱「魚膾」\[11\]\[12\]，以[鯇魚切片](../Page/鯇魚.md "wikilink")，調以酒和其他佐料而食\[13\]據《南越游记》载，嶺南人喜以草魚作魚生，佐以瓜子、花生、蘿蔔、木耳、芹菜、油煎面餌、粉絲、豆腐乾混合而成的調料\[14\]。

#### 順德魚生

順德魚生\[15\]一般以淡水魚為主，品質好的順德魚生以約750克的「壯魚」為食材，買回來後先放在山泉水餓養幾天，以消耗體內脂肪，令魚肉實甘爽。殺魚時在魚下頜處和尾部各割一刀，然後放回水中讓魚在游動放血，了無淤血的魚片便潔白如雪，晶瑩剔透，放血的程序若把握不好，魚肉帶紅色水分也多。片好之後要再放進冷凍一會，魚生才會爽滑和有甜味。

進食時蘸以蒜片、薑絲、蔥絲、[洋蔥絲](../Page/洋蔥.md "wikilink")、[椒絲](../Page/辣椒.md "wikilink")、[醬油](../Page/醬油.md "wikilink")、[花生碎](../Page/花生.md "wikilink")、[芝麻](../Page/芝麻.md "wikilink")、[指天椒](../Page/指天椒.md "wikilink")、香[芋絲](../Page/芋.md "wikilink")、炸[粉絲](../Page/粉絲.md "wikilink")，再加上油、鹽、糖混合成的調料。魚皮則可涼拌。

#### 潮汕魚生

潮汕魚生\[16\]多以養於沙塘中的[草魚為食材](../Page/草魚.md "wikilink")，重三斤左右最为适宜。早上把買回來的鱼放在清水裡养着，到下午才去鱼鳞、开膛，掏出内臟後将一层鱼皮削去，然後沿脊骨取下左右两边肉，切除肋骨和鱼腹。擦乾血污，把鱼放在比较通风的地方，讓魚被風吹至富有彈性。晚上就可以切片食用。

佐料分咸甜两种：咸的是[豆酱拌小磨](../Page/豆酱.md "wikilink")[香油](../Page/香油.md "wikilink")；甜的則是[三渗酱](../Page/三渗酱.md "wikilink")、[梅膏酱](../Page/梅膏酱.md "wikilink")，另备有一碟生切萝蔔丝或[杨桃片](../Page/杨桃.md "wikilink")。可謂色、香、味、型俱佳。通常在吃鱼生會喝鱼头汤和吃鱼片稀粥。

#### 佛山九江魚生

佛山[九江魚生以拌](../Page/九江镇_\(佛山市\).md "wikilink")（[粵語稱為](../Page/粵語.md "wikilink")「撈」）的形式食用，故又稱「撈魚生」（拌魚生）\[17\]，現時的製法可追溯至清末，通常以[海鯇為食材](../Page/海鯇.md "wikilink")，買回來後養一個月，期間不餵食，讓魚體內的廢物排出，並減掉多餘脂肪。然後殺魚、放血、開膛，之後不能冷藏，要現做現吃。上桌時在魚生的表面放上[檸檬葉絲](../Page/檸檬.md "wikilink")，吃時魚生會有一股清香。

配料可多至十九種，包括炸米粉絲、炸芋絲、炸麻花、京[蔥白](../Page/蔥白.md "wikilink")、薑絲、蘿蔔絲、尖椒絲、指天椒、[欖角碎](../Page/欖角.md "wikilink")、酸[藠頭](../Page/藠頭.md "wikilink")、生蒜片、花生、芝麻、白砂糖、白醋、[花生油](../Page/花生油.md "wikilink")、[鹽](../Page/食盐.md "wikilink")、胡椒粉。冬季先放魚肉再放配料攪拌，讓魚吸收配料的香味。夏季則先把配料拌勻再放魚肉，保持魚肉鮮美。魚肉以外的配料，放下去的先後也有一定程序，比例也要掌握得好，雙手各持一雙[筷子](../Page/筷子.md "wikilink")，先把油、鹽、胡椒粉拌一下，然後把蔥、芝麻等配料倒下去再拌幾拌，再放油、鹽、胡椒粉拌幾下，再下炸米粉絲、炸芋絲、炸麻花等脆口配料，再拌幾下。進食時以筷子把拌勻的魚肉和配料一同挾進口。

傳統製法程序繁複，配料繁多，在[南莊](../Page/南莊鎮_\(佛山市\).md "wikilink")、[廣州等地的九江魚生會把程序簡化或減少配料](../Page/廣州.md "wikilink")，但用作提味的欖角如果去掉，魚生就沒那麼鮮美了。

### 客家生魚膾

魚膾也是[客家傳統食品](../Page/客家.md "wikilink")，[寧化](../Page/寧化.md "wikilink")、[五華](../Page/五華縣.md "wikilink")、[興寧等地的客家人都有吃魚膾的習慣](../Page/興寧.md "wikilink")。

#### 寧化生魚膾

[福建寧化的](../Page/福建.md "wikilink")[客家人的魚膾](../Page/客家人.md "wikilink")\[18\]以約兩三斤重的鮮活草魚為食材，清水漂浸半日，製作時以右手用毛巾裹住魚頭，令魚不再蹦跳，左手持刀快速除鱗、開膛去內臟，沖洗乾淨后不再下水。然後去頭尾劈成兩片，再去魚皮、剔骨。擦乾魚肉的血污後切成透明的薄片。切時師傅要站成[弓步](../Page/弓步.md "wikilink")，左手指尖輕輕壓住魚肉，右手刀口前傾，走刀飛速而用力均勻，切好後立即攔上[麻油上桌](../Page/麻油.md "wikilink")。

這種魚膾製作講究，不但注重[衛生](../Page/衛生.md "wikilink")，製作時還不能靠近高溫、不能使用[電扇](../Page/電扇.md "wikilink")，保持魚片潔[白無染](../Page/白.md "wikilink")。[佐料有上等](../Page/佐料.md "wikilink")[醬油](../Page/醬油.md "wikilink")、[芥末醬和](../Page/芥末醬.md "wikilink")[薑汁](../Page/薑.md "wikilink")。

#### 五華生魚膾

也叫[琴江魚生](../Page/琴江.md "wikilink")，製作過程與寧化生魚膾相若，先把草魚漂浸，然後快速除鱗、去內臟、去骨，再把魚肉切成薄片。特別之處是把切好的魚肉放在竹篩上晾干水分。

進食前先準備一大碗和好的薑蒜汁和白醋浸泡魚肉除腥，佐料有醋醃蒜片、薄荷葉、薑絲、炸花生米、[花椒粒](../Page/花椒.md "wikilink")、香菜、[芹菜葉](../Page/芹菜.md "wikilink")、辣椒、胡椒等，分別用小碗盛載。吃時先挾生魚片放進醋裡浸約半分鐘，然後用勺子把佐料舀到自己碗裡，佐料可依照個人喜好決定種類和分量，再把生魚片放在最上面，然後一整碗扒進嘴裡。

#### 興寧生魚膾

與五華魚膾相似，製法和吃法略有不同，其中以大坪魚膾\[19\]為極品。把鮮魚[去鱗](https://web.archive.org/web/20170410213756/http://kejiawc.vicp.net/wc/up/upload/upfile/soft/2004811249050sdg.jpg)後，切開[放血](https://web.archive.org/web/20070927024959/http://kejiawc.vicp.net/wc/up/upload/upfile/soft/2004811249051mnb.jpg)，然後[剖開](https://web.archive.org/web/20070927024945/http://kejiawc.vicp.net/wc/up/upload/upfile/soft/2004811249052zvbvs.jpg)、去骨，再[切片](https://web.archive.org/web/20070927025012/http://kejiawc.vicp.net/wc/up/upload/upfile/soft/2004811249053xvn.jpg)，魚片較五華魚膾厚。把魚片放在竹篩上面[上桌](https://web.archive.org/web/20170410213758/http://kejiawc.vicp.net/wc/up/upload/upfile/soft/2004811249064kjh.jpg)，以高山[茶油調味](../Page/茶油.md "wikilink")。

### 赫哲族生魚膾

聚居於中國黑龍江省和中[俄邊境一帶的](../Page/俄羅斯.md "wikilink")[赫哲族有吃生魚的習慣](../Page/赫哲族.md "wikilink")，對於魚皮、鱼子、魚肉、魚脆骨都有生吃的妙法。加入蔬菜的涼拌生魚稱為他勒卡。原本傳統上是不加任何調味，但近年來開始會佐以醬油、醋一起食用。

### 日本刺身

日本生魚片稱為**刺身**，一般都是用新鮮海魚、海貝製作，蘸以[醬油](../Page/醬油.md "wikilink")、[山葵等](../Page/山葵.md "wikilink")，是日本菜中接近最清淡的菜式，也是具有代表性的菜式之一。日本漢字-{「}-膾-{」}-出現《古事記》與《日本書紀》，「なます」的語源為「なま（生）」＋「しし（肉）」即生肉，後轉換成「なま（生）」＋「すく（剥く）」，[室町時代以後才轉變成](../Page/室町時代.md "wikilink")「なま（生）」＋「す（酢）」，生酢。同樣以鱠及膾分開表示獸肉與漁產類。
应当指出的是，“膾”是日语中的意思撒满了醋撕碎了那切絲蔬菜的事。

### 朝鮮膾

朝鮮亦有食用生魚片的飲食習慣，稱為**膾**
（[諺文](../Page/諺文.md "wikilink")：****；[朝鮮漢字](../Page/朝鮮漢字.md "wikilink")：**膾**），把切薄片的生魚肉用生菜包裹並加上醬料食用。

### 新馬式魚生

[新加坡和](../Page/新加坡.md "wikilink")[馬來西亞的](../Page/馬來西亞.md "wikilink")[華人於](../Page/華人.md "wikilink")[农历新年或](../Page/农历新年.md "wikilink")[人日有吃魚生的習慣](../Page/人日.md "wikilink")，稱為「七彩魚生」\[20\]或「撈起魚生」（「撈」為粵語，即漢語的「拌」），取代傳統的[七菜羹](../Page/七菜羹.md "wikilink")。這種魚生是由九江魚生演變而成，近日也有用鲑鱼生鱼片。醬料以[酸梅膏加](../Page/酸梅.md "wikilink")[橘子汁添糖水調成](../Page/橘子.md "wikilink")。

## 衛生問題

生魚片是生食的海鮮，常吃容易造成[寄生蟲感染](../Page/寄生蟲.md "wikilink")；現代人只吃海水魚的生魚片，而且經由冷凍殺蟲處理；海水魚的寄生蟲幾乎無法感染人類，而冷凍處理可以殺死[海獸胃線蟲](../Page/海獸胃線蟲.md "wikilink")。

言而，淡水魚的養殖環境較容易滋生細菌和寄生蟲，因此食用淡水魚的生魚片染病風險較高，部分國家以保障公眾健康為由禁止出售淡水魚生，如[香港的](../Page/香港.md "wikilink")《食物業規例》禁止出售中式魚生、[新加坡國家環境局亦於](../Page/新加坡.md "wikilink")2015年宣布無限期禁止食肆出售淡水魚生魚料理。

食用生魚片可引致細菌感染和寄生蟲感染，如淡水魚生魚片驗出[中華肝吸蟲](../Page/中華肝吸蟲.md "wikilink")。感染肝吸蟲可引致長期嚴重感染，可引起食慾不振、腹瀉和發燒，亦可造成[膽管梗阻和](../Page/膽管梗阻.md "wikilink")[肝硬化](../Page/肝硬化.md "wikilink")，甚至可引發慢性[黃疸和間接造成](../Page/黃疸.md "wikilink")[膽管癌](../Page/膽管癌.md "wikilink")。而新加坡亦於2015年爆發[乙型鏈球菌導致患者死亡](../Page/乙型鏈球菌.md "wikilink")，發現感染與生食淡水魚有直接關係。即使生活在水質較佳的深水魚亦可能引發相同問題，但一般而言深水魚感染機會較淡水魚低。

## 其他地區

魚膾除了於[東亞文化圈傳播外](../Page/東亞文化圈.md "wikilink")，世界上其他地區亦有一些以魚生為食材的食品：

  - [拉丁美洲](../Page/拉丁美洲.md "wikilink")[檸汁醃魚生](../Page/檸汁醃魚生.md "wikilink")（[ceviche](../Page/ceviche.md "wikilink")）
  - [意大利生魚片](../Page/意大利.md "wikilink")（）
  - [菲律賓醋生魚片](../Page/菲律賓.md "wikilink")（）
  - [馬里亞納群島](../Page/馬里亞納群島.md "wikilink")[查莫洛酸辣涼拌沙拉](../Page/查莫洛人.md "wikilink")（）

<!-- end list -->

  -
    由菲律賓醋生魚片演變而來。

<!-- end list -->

  - [夏威夷魚生沙拉](../Page/夏威夷.md "wikilink")（）
  - [歐洲韃靼肉膾或魚膾](../Page/歐洲.md "wikilink")（[Tartare](../Page/韃靼牛肉.md "wikilink")）

## 相關條目

  - [肉膾](../Page/肉膾.md "wikilink")
  - [刺身](../Page/刺身.md "wikilink")
  - [膾 (韓國)](../Page/膾_\(韓國\).md "wikilink")
  - [壽司](../Page/壽司.md "wikilink")
  - [鐵火丼](../Page/鐵火丼.md "wikilink")

## 注釋

<div class="references-small">

<references />

</div>

## 參考資料

  - [挪威三文魚中式魚生亮相廣州(圖)](http://news.xinhuanet.com/food/2005-05/17/content_2964949.htm)
  - [中國古代的生食肉類餚饌──膾生](http://www.ihp.sinica.edu.tw/~bihp/71/71.2/meat.html)
  - [中國古代生魚片文化](http://news.xinhuanet.com/food/2004-10/18/content_2103935.htm)
  - 飲御諸友，膾三文魚
  - [到五華做一次“泥美人”](https://web.archive.org/web/20070929092944/http://pdf.sznews.com/big5/content/2002-09/29/content_1375034.htm)

<!-- end list -->

  - [脍炙](http://www.100md.com/html/DirDu/2006/03/27/97/46/68.htm)
  - [讲饮讲食：中国人吃鱼生](https://web.archive.org/web/20070503121350/http://www.ycwb.com/gb/content/2005-03/06/content_860076.htm)

[Category:魚生](../Category/魚生.md "wikilink")
[Category:中國飲食](../Category/中國飲食.md "wikilink")
[Category:日本飲食](../Category/日本飲食.md "wikilink")

1.  [CODE OF PRACTICE FOR FISH AND FISHERY PRODUCTS
    CAC/RCP 52-2003](http://www.fao.org/fao-who-codexalimentarius/sh-proxy/en/?lnk=1&url=https%253A%252F%252Fworkspace.fao.org%252Fsites%252Fcodex%252FStandards%252FCAC%2BRCP%2B52-2003%252FCXP_052e.pdf)Codex
    Alimentarius
2.  [厚生勞動省](http://www.mhlw.go.jp/stf/seisakunitsuite/bunya/0000042953.html)
3.  [東京都健康安全研究センター](http://www.tokyo-eiken.go.jp/assets/issue/kurashi/ck43/2001430all.pdf)
4.  [東京都福祉保健局](http://www.fukushihoken.metro.tokyo.jp/shokuhin/musi/index.html)
5.  [農林水產省引自厚生勞動省](http://www.maff.go.jp/j/syouan/tikusui/gyokai/g_kenko/busitu/pdf/hotaru_ika.pdf)
6.  [Fish and Fishery Products Hazards and Controls Guidance - Fourth
    Edition](http://www.fda.gov/food/guidanceregulation/guidancedocumentsregulatoryinformation/seafood/ucm2018426.htm#toc)FDA
7.  《论语·乡党第十》：食不厌精，脍不厌细。食饐而餲，鱼馁而肉败，不食。色恶，不食。臭恶，不食。失饪，不食。不时，不食。割不正，不食。不得其酱，不食。
8.  曾晳嗜羊棗，而曾子不忍食羊棗。公孫丑問曰：「膾炙與羊棗孰美？」孟子曰：「膾炙哉！」公孫丑曰：「然則曾子何為食膾炙而不食羊棗？」曰：「膾炙所同也，羊棗所獨也。諱名不諱姓，姓所同也，名所獨也。」
9.  《三国志·魏书·方技传第二十九》：广陵太守陈登得病，胸中烦懑，面赤不食。佗脉之曰：“府君胃中有虫数升，欲成内疽，食腥物所为也。”即作汤二升，先服一升，斯须尽服之。食顷，吐出三升许虫，赤头皆动，半身是生鱼脍也，所苦便愈。佗曰：“此病后三期当发，遇良医乃可济救。”依期果发动，时佗不在，如言而死。
10. 清·李調元《南越筆記》：「粤俗嗜鱼生，以鲈以（鱼奥）以[鰽白以黄鱼以青鲚以雪龄以鲩为上](../Page/鰽白.md "wikilink")。鲩又以白鲩为上。以初出水泼剌者，去其皮剑，洗其血腥，细脍之以为生，红肌白理，轻可吹起，薄如蝉翼，两两相比，沃以老醪，和以椒芷，入口冰融，至甘旨矣。而鲥与嘉鱼尤美。」
11. 《隋唐嘉話》：南人魚膾，以細縷金橙拌之，號為「金虀玉膾」。
12. 《廣東新語》：膾之為片，紅肌白理，輕可吹起，薄如蟬翼
13. 《南越筆記》：鯇以白鯇為上，以初出水潑刺者，去其劍皮，洗其血腥，細切之為片，紅肌白理，輕可以起，兩兩相比，沃以老醪，和以椒芷，入口冰融，至廿旨矣
14. 岭南人喜取草鱼活者，剖割成屑，佐以瓜子、落花生、萝卜、木耳、芹菜、油煎面饵、粉丝、腐干，汇而食之，名曰鱼生。”“复有鱼生粥，其中所有诸品，因鱼生之名而名之。”
15. [順德的傳統小食
    順德魚生](http://big5.china.com.cn/city/txt/2006-09/20/content_7589178.htm)
16. [潮汕海鲜饮食](http://www.chaofood.com/culture/culture_detail.asp?sendid=3157)
17.
18. [客家小吃：生魚片與酒釀](http://www.sm.gov.cn/bmzd/taiban/ftb/smtc/kjxc3.HTM)
19.
20. <http://www.got1mag.com/blogs/kimcherng.php/2007/02/24/>