**田中公平**（），[日本](../Page/日本.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。主要擔任[動畫的樂曲製作](../Page/動畫.md "wikilink")。[大阪府](../Page/大阪府.md "wikilink")[大阪市中央區](../Page/大阪市.md "wikilink")[心齋橋出身](../Page/心齋橋.md "wikilink")。

## 經歷

[東京藝術大学音樂學部作曲科畢業](../Page/東京藝術大学.md "wikilink")。曾于[美國](../Page/美國.md "wikilink")[伯克利音樂学院留學](../Page/伯克利音樂学院.md "wikilink")2年，回國后開始創作活動。主要從事為[動畫](../Page/動畫.md "wikilink")、[遊戲軟體配樂的工作](../Page/遊戲軟體.md "wikilink")，創作出了《檄！帝国华击团》等膾炙人口的名作。

## 代表作品

### 動畫

  - 『ARIEL』
  - 『[櫻花大戰系列](../Page/櫻花大戰系列.md "wikilink")』
  - 『[飛越巔峰系列](../Page/飛越巔峰.md "wikilink")』
  - 『』系列
  - 『[城市風雲兒](../Page/城市風雲兒.md "wikilink")』
  - 『[黑色推銷員](../Page/黑色推銷員.md "wikilink")』
  - 『Assemble Insert』
  - 『[超能力魔美](../Page/超能力魔美.md "wikilink")』
  - 『究極超人R』
  - 『[21衛門](../Page/21衛門.md "wikilink")』
  - 『[絕對無敵](../Page/絕對無敵.md "wikilink")』
  - 『[動物橫町](../Page/動物橫町.md "wikilink")』
  - 『[地球防衛企業](../Page/地球防衛企業.md "wikilink")』
  - 『[機動戰士GUNDAM第08MS小隊](../Page/機動戰士GUNDAM第08MS小隊.md "wikilink")』
  - 『[機動天使](../Page/機動天使.md "wikilink")』
  - 『[大耳鼠](../Page/大耳鼠.md "wikilink")』
  - 『[機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")』
  - 『[魔動王](../Page/魔動王.md "wikilink")』
  - 『[勇者王](../Page/勇者王.md "wikilink")』
  - 『[ONE
    PIECE](../Page/ONE_PIECE#動畫.md "wikilink")』（與[濱口史郎合作](../Page/濱口史郎.md "wikilink")）
  - 『[怪俠索羅利](../Page/怪俠索羅利.md "wikilink")』
  - 『[武裝鍊金](../Page/武裝鍊金.md "wikilink")』
  - 『[旋風管家](../Page/旋風管家.md "wikilink")』 -
    [角色歌曲作曲](../Page/角色歌曲.md "wikilink")
  - 『[十字架與吸血鬼](../Page/十字架與吸血鬼.md "wikilink")』（與濱口史郎合作）
  - 『真[魔神英雄传](../Page/魔神英雄传.md "wikilink") 魔神山編』
  - 『[银河铁道999剧场版](../Page/银河铁道999.md "wikilink")』
  - 『[魔法提琴手](../Page/魔法提琴手.md "wikilink")』
  - 『[GUNDAM EVOLVE](../Page/GUNDAM_EVOLVE.md "wikilink")』
  - 『[捍衛者21](../Page/捍衛者21.md "wikilink")』
  - 『[冰菓](../Page/古籍研究社系列.md "wikilink")』

### 遊戲

  - 『[櫻花大戰](../Page/櫻花大戰.md "wikilink")』
      - 「檄！帝國華擊團」是從第一到四代、動畫版、OVA、一代重製版的主題曲，在動畫、遊戲界中，以遊戲發售日算起相差十年，如此長壽的主題曲是實屬首次。
      - 「奇迹之鐘」是另一首受歡迎的插曲，分別於第二代與電影版中。
  - 『[櫻花大戰2](../Page/櫻花大戰2.md "wikilink")』
  - 『[櫻花大戰3](../Page/櫻花大戰3.md "wikilink")』
  - 『[櫻花大戰4](../Page/櫻花大戰4.md "wikilink")』
  - 『[重力異想世界](../Page/重力異想世界.md "wikilink")』
  - 『[超異域公主連結 Re:Dive](../Page/超異域公主連結_Re:Dive.md "wikilink")』

## 相關條目

  - [川井憲次](../Page/川井憲次.md "wikilink")

## 外部連結

  - [](http://ameblo.jp/kenokun/): 田中公平 Official WebSite

  - [](https://web.archive.org/web/20150423120130/http://tanakakouhei.com/):
    簡介、作品列表

[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:動畫音樂作曲家](../Category/動畫音樂作曲家.md "wikilink")
[Category:伯克利音樂學院校友](../Category/伯克利音樂學院校友.md "wikilink")
[Category:東京藝術大學校友](../Category/東京藝術大學校友.md "wikilink")