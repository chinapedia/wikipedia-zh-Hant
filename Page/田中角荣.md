**田中角荣**（；），[日本](../Page/日本.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")、[建築師](../Page/建築師.md "wikilink")，歷任[眾議院議員](../Page/眾議院_\(日本\).md "wikilink")（16期）、（第12代）、[大藏大臣](../Page/大藏大臣.md "wikilink")（第67－69代）、[通商產業大臣](../Page/通商產業大臣.md "wikilink")（第33代）、[內閣總理大臣](../Page/日本內閣總理大臣.md "wikilink")（第64、65代）。1972年（[昭和](../Page/昭和.md "wikilink")47年）9月，就任首相2个月後，与[中华人民共和国的首脑进行会谈](../Page/中华人民共和国.md "wikilink")，并[建立外交关系](../Page/中日聯合聲明.md "wikilink")，也因此为外界所知\[1\]。

## 個人生涯

出身[新潟縣農家](../Page/新潟縣.md "wikilink")，在1940年代開設建築公司，1945年[日本二戰投降後開始從政](../Page/日本二戰投降.md "wikilink")，曾組成[自民黨最大派閥](../Page/自由民主党_\(日本\).md "wikilink")－田中派，並任[黨魁](../Page/黨魁.md "wikilink")。1972年出任[總理大臣](../Page/日本內閣總理大臣.md "wikilink")，是首名誕生於[大正時代的首相](../Page/大正.md "wikilink")，也是第一個「庶民宰相」。任內經歷簽署[日中聯合聲明](../Page/日中聯合聲明.md "wikilink")（與[中華民國斷交](../Page/中華民國.md "wikilink")，與[中华人民共和国建交](../Page/中华人民共和国.md "wikilink")）、[金大中事件](../Page/金大中绑架事件.md "wikilink")、等重大事件；並提出「[日本列島改造論](../Page/日本列島改造論.md "wikilink")」，推動各項基建工程，在正反兩面均對[日本史近代發展有著深遠的影響](../Page/日本历史.md "wikilink")。1974年因[官商勾結而下台](../Page/官商勾結.md "wikilink")，兩年後的1976年更因[洛克希德事件中被揭發獲得巨額](../Page/洛克希德事件.md "wikilink")[賄款而掀起更大波瀾](../Page/賄款.md "wikilink")，最終被迫退黨。但退黨後仍在政府及政壇保持極大的影響力，在[四十日抗爭等事件依然在幕後操盤](../Page/四十日抗爭.md "wikilink")，田中被稱為「今[太閣](../Page/太閣.md "wikilink")」（現代的[豐臣秀吉](../Page/丰臣秀吉.md "wikilink")），直到1987年田中派分裂為止。

其長女[田中真纪子亦跟從父親步伐從政](../Page/田中真纪子.md "wikilink")，是日本第一位[女性](../Page/女性.md "wikilink")[外務大臣](../Page/日本外務大臣.md "wikilink")。

## 生平

  - 1918年（大正7年）出生於新潟縣一個農戶家庭。
  - 1939年（昭和14年）參軍，派駐於[滿州國](../Page/滿州國.md "wikilink")。
  - 1941年（昭和16年）回國，加入坂本工程公司。
  - 1942年（昭和17年）接管坂本公司，改名為田中工程公司。
  - 1950年（昭和25年）任長岡鐵路公司總經理。
  - 1953年（昭和28年）任理研化学公司董事。
  - 1954年（昭和29年）任副干事长（翌年日本自由党、[日本民主党合并成自由民主党](../Page/日本民主黨_\(1954年\).md "wikilink")）。
  - 1957年（昭和32年）出任邮政大臣。
  - 1961年（昭和36年）任自民党政务调查会长。
  - 1963年（昭和38年）出任大藏大臣。
  - 1965年（昭和40年）任自民党干事长。
  - 1966年（昭和41年）[黑霧事件 (政界)](../Page/黑霧事件_\(政界\).md "wikilink")
  - 1971年（昭和46年）任[通商产业大臣](../Page/通商产业大臣.md "wikilink")。
  - 1972年（昭和47年）出任[内阁总理大臣](../Page/内阁总理大臣.md "wikilink")。同年9月底访问[中華人民共和國](../Page/中华人民共和国.md "wikilink")，签署《[日中聯合聲明](../Page/日中聯合聲明.md "wikilink")》，实现了[中日邦交正常化](../Page/中日关系.md "wikilink")。同年，與[中華民國斷交](../Page/中華民國.md "wikilink")。
  - 1973年（昭和48年）與[蘇聯領導人](../Page/苏联.md "wikilink")[勃列日涅夫签署联合声明](../Page/勃列日涅夫.md "wikilink")。
  - 1974年（昭和49年）被《[文艺春秋](../Page/文艺春秋.md "wikilink")》揭发金权问题，辞去首相和自民党[总裁职务](../Page/总裁.md "wikilink")。
  - 1976年（昭和51年）[洛克希德事件曝光](../Page/洛克希德事件.md "wikilink")，退出自民党。7月27日，東京地方检察廳逮捕田中角荣，宣布他涉嫌在1973年8月至1974年2月任首相期间，透過丸红公司4次收受洛克西德公司的贿款共5億日元
  - 1983年（昭和58年）因洛克希德事件，被[东京地方法院判处](../Page/东京地方法院.md "wikilink")4年徒刑、罚款5亿。他最終並無服刑。
  - [1984年](../Page/1984年.md "wikilink")（昭和59年）10月 -
    自民党总裁选。田中角荣决定拥护田中派（木曜俱乐部）会長[二階堂進副总裁](../Page/二階堂進.md "wikilink")，支持[中曾根康弘的再選](../Page/中曾根康弘.md "wikilink")。12月，田中派内的中堅与年轻人准备设立以[竹下登为中心的](../Page/竹下登.md "wikilink")「」。
  - [1985年](../Page/1985年.md "wikilink")（昭和60年）
      - [2月7日](../Page/2月7日.md "wikilink") - 創政会成立。
      - [2月27日](../Page/2月27日.md "wikilink") -
        [脑梗塞发作入院](../Page/脑梗塞.md "wikilink")。患与，从此无法参与政治活動。
      - 6月 - 田中事務所关闭。
      - 9月 - [洛克希德事件控訴審開始](../Page/洛克希德事件.md "wikilink")，田中本人缺席。
      - 10月 - [关越自動車道全通](../Page/关越自動車道.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")（昭和61年）7月 -
    [第38回总選举](../Page/第38屆日本眾議院議員總選舉.md "wikilink")。以第一名当選。田中本人完全没有参加任何选举活动，唯有越山会等组织的支持者参与活動，自民党大勝，在约4年的任期中，田中一次都没有进入众议院过。
  - [1987年](../Page/1987年.md "wikilink")（昭和62年）
      - [7月4日](../Page/7月4日.md "wikilink") -
        竹下成立[经世会](../Page/平成研究會.md "wikilink")，超过半数的田中派参加其中。二階堂小组决定留在木曜俱乐部，包含中间派在内的田中派分裂了。
      - [7月29日](../Page/7月29日.md "wikilink") -
        [洛克希德事件控訴審判決](../Page/洛克希德事件.md "wikilink")。[東京高等裁判所支持一審判決](../Page/東京高等裁判所.md "wikilink")，驳回了田中的上訴，田中一側当日便。
      - 10月 - 竹下访问田中宅邸，却被眞紀子吃了闭门羹。之後因而使分裂浮上水面。
      - 11月 - [竹下内閣成立](../Page/竹下內閣.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")（平成元年）10月 - 直紀表示田中角荣不会参与下次的总选举。
  - 1990年（平成2年）
      - 1月24日 - 因衆議院解散而从政界引退。担任衆議院議員43年、当選16回。各地的越山会也都解散。
      - 2月 - 。原越山会員、前[小千谷市長的](../Page/小千谷市.md "wikilink")当选。
  - [1992年](../Page/1992年.md "wikilink")（平成4年）
      - 8月 -
        访问中国。这是他20年来首次访问中国，受到了中国政府的招待，[田中眞紀子也与他一同前往中国](../Page/田中真紀子.md "wikilink")。
      - 12月 - 经世会分裂。
  - 1993年（平成5年）逝世。

## 著作

  - 『』 [日本经济新闻社](../Page/日本经济新闻社.md "wikilink") 1966年
  - 『大臣日記』 新潟日報事業社 1972年
  - 『[日本列島改造論](../Page/日本列島改造論.md "wikilink")』  1972年
  - 『自传 我的少年时代（）』 [講談社](../Page/讲谈社.md "wikilink") 1973年

## 参考文献

## 外部連結

  - [（財）田中榮記念館](http://www.tanaka-zaidan.net/)（[道の駅](../Page/道之驛.md "wikilink")・西山ふるさと公苑内）
  - [Kakuei Tanaka - a political biography of modern
    Japan:](http://www.rcrinc.com/tanaka/)
  - [Fujiland: Kakuei Tanaka: Lockheed &
    Loopholes](http://fujiland-mag.blogspot.com/2010/11/kakuei-tanaka-lockheed-loopholes.html)

{{-}}
[category:商人出身的政治人物](../Page/category:商人出身的政治人物.md "wikilink")

[Category:日本內閣總理大臣](../Category/日本內閣總理大臣.md "wikilink")
[Category:日本郵政大臣](../Category/日本郵政大臣.md "wikilink")
[Category:日本通商產業大臣](../Category/日本通商產業大臣.md "wikilink")
[Category:日本大藏大臣](../Category/日本大藏大臣.md "wikilink")
[Category:第一次岸內閣閣僚](../Category/第一次岸內閣閣僚.md "wikilink")
[Category:第二次池田內閣閣僚](../Category/第二次池田內閣閣僚.md "wikilink")
[Category:第三次池田內閣閣僚](../Category/第三次池田內閣閣僚.md "wikilink")
[Category:第一次佐藤內閣閣僚](../Category/第一次佐藤內閣閣僚.md "wikilink")
[Category:第三次佐藤內閣閣僚](../Category/第三次佐藤內閣閣僚.md "wikilink")
[Category:第一次田中角榮內閣閣僚](../Category/第一次田中角榮內閣閣僚.md "wikilink")
[Category:第二次田中角榮內閣閣僚](../Category/第二次田中角榮內閣閣僚.md "wikilink")
[Category:日本自由民主黨總裁](../Category/日本自由民主黨總裁.md "wikilink")
[Category:日本自由民主黨幹事長](../Category/日本自由民主黨幹事長.md "wikilink")
[Category:日本自由民主黨政務調查會長](../Category/日本自由民主黨政務調查會長.md "wikilink")
[Category:日本自由黨
(1945年—1955年)黨員](../Category/日本自由黨_\(1945年—1955年\)黨員.md "wikilink")
[Category:日本賄賂罪罪犯](../Category/日本賄賂罪罪犯.md "wikilink")
[Category:二战后日本政治人物](../Category/二战后日本政治人物.md "wikilink")
[Category:新潟縣出身人物](../Category/新潟縣出身人物.md "wikilink")
[Category:馬主](../Category/馬主.md "wikilink") [Category:日本眾議院議員
1947–1948](../Category/日本眾議院議員_1947–1948.md "wikilink")
[Category:日本眾議院議員
1949–1952](../Category/日本眾議院議員_1949–1952.md "wikilink")
[Category:日本眾議院議員
1952–1953](../Category/日本眾議院議員_1952–1953.md "wikilink")
[Category:日本眾議院議員
1953–1955](../Category/日本眾議院議員_1953–1955.md "wikilink")
[Category:日本眾議院議員
1955–1958](../Category/日本眾議院議員_1955–1958.md "wikilink")
[Category:日本眾議院議員
1958–1960](../Category/日本眾議院議員_1958–1960.md "wikilink")
[Category:日本眾議院議員
1960–1963](../Category/日本眾議院議員_1960–1963.md "wikilink")
[Category:日本眾議院議員
1963–1966](../Category/日本眾議院議員_1963–1966.md "wikilink")
[Category:日本眾議院議員
1967–1969](../Category/日本眾議院議員_1967–1969.md "wikilink")
[Category:日本眾議院議員
1969–1972](../Category/日本眾議院議員_1969–1972.md "wikilink")
[Category:日本眾議院議員
1972–1976](../Category/日本眾議院議員_1972–1976.md "wikilink")
[Category:日本眾議院議員
1976–1979](../Category/日本眾議院議員_1976–1979.md "wikilink")
[Category:日本眾議院議員
1979–1980](../Category/日本眾議院議員_1979–1980.md "wikilink")
[Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:前日本自由民主黨黨員](../Category/前日本自由民主黨黨員.md "wikilink")
[Category:获称“中国人民的老朋友”的日本首相](../Category/获称“中国人民的老朋友”的日本首相.md "wikilink")
[Category:洛克希德事件相關人物](../Category/洛克希德事件相關人物.md "wikilink")
[Category:新潟縣選出日本眾議院議員](../Category/新潟縣選出日本眾議院議員.md "wikilink")

1.  NHK-BS1「日中“密使外交”の全貌～佐藤栄作の極秘交渉～」2017年9月24日放送