**田亮**是[中国](../Page/中国.md "wikilink")[重庆人](../Page/重庆.md "wikilink")，[跳水](../Page/跳水.md "wikilink")[运动员](../Page/运动员.md "wikilink")，[演员](../Page/演员.md "wikilink")。前[陝西跳水队註册运动员](../Page/陝西.md "wikilink")。在[2000年悉尼奥运会](../Page/2000年夏季奥林匹克运动会.md "wikilink")，获得男子10米高台[跳水冠军](../Page/跳水.md "wikilink")；[2004年雅典奥运会](../Page/2004年夏季奥林匹克运动会.md "wikilink")，和[杨景辉一起获得男子双人](../Page/杨景辉.md "wikilink")10米高台[跳水冠军](../Page/跳水.md "wikilink")，同时还获得个人季军。

## 运动生涯

1986年入小学，上体育时因[跳远成绩好而开始跟随启蒙教练](../Page/跳远.md "wikilink")[张挺](../Page/张挺.md "wikilink")，在重慶市業餘體校学習跳水。1990年進入陝西隊。1993年進入國家隊。1996年，在奥运选拔赛中，擊敗[1992年巴塞隆拿奧運會10米台跳水比賽冠軍](../Page/1992年夏季奧林匹克運動會跳水比賽.md "wikilink")[孫淑偉](../Page/孫淑偉.md "wikilink")，取得[1996年亞特蘭大奧運會10米台跳水比賽參賽資格](../Page/1996年夏季奧林匹克運動會跳水比賽.md "wikilink")；但在決賽中表現不佳，僅獲得第四名。\[1\]1999年[新西蘭第十一屆](../Page/新西蘭.md "wikilink")[跳水世界盃男子](../Page/跳水世界盃.md "wikilink")10米高台單人、雙人冠軍。

2000年，他参加[悉尼第十二屆世界盃跳水賽](../Page/悉尼.md "wikilink")，获得男子10米高台單人、雙人冠軍。同年参加[悉尼奧運會](../Page/悉尼奧運會.md "wikilink")，与[胡佳搭檔參加](../Page/胡佳_\(跳水運動員\).md "wikilink")[2000年悉尼奧運會雙人10米台跳水比賽](../Page/2000年夏季奧林匹克運動會跳水比賽.md "wikilink")，不敵[俄羅斯組合](../Page/俄羅斯.md "wikilink")，獲得亞軍。\[2\]其後，參加[單人10米台跳水比賽](../Page/2000年夏季奧林匹克運動會跳水比賽.md "wikilink")，險勝隊友胡佳，奪冠军。\[3\]

2001年，他参加[福岡第九屆](../Page/福岡.md "wikilink")[世界游泳錦標賽](../Page/世界游泳錦標賽.md "wikilink")，获得男子10米高台單人、雙人冠軍。2003年，参加[巴塞隆納第十屆](../Page/巴塞隆納.md "wikilink")[世界游泳錦標賽男子](../Page/世界游泳錦標賽.md "wikilink")10米高台單人、雙人季軍。2004年，参加[雅典第十四屆世界盃跳水賽男子](../Page/雅典.md "wikilink")10米高台單人、雙人冠軍。此外，他与[杨景辉搭檔參加](../Page/杨景辉.md "wikilink")[2004年雅典奧運會雙人10米台跳水比賽](../Page/2004年夏季奧林匹克運動會跳水比賽.md "wikilink")，獲得冠軍。其後，參加[單人10米台跳水比賽](../Page/2004年夏季奧林匹克運動會跳水比賽.md "wikilink")。因第四跳的失誤，負於隊友[胡佳](../Page/胡佳_\(跳水運動員\).md "wikilink")，獲得季軍。

2005年1月26日，因為他在雅典奧運後擬和香港[英皇娛樂簽約成為藝人](../Page/英皇娛樂.md "wikilink")，並很久才回国家隊集訓，[國家體育總局認為他這樣是犯規行為](../Page/國家體育總局.md "wikilink")，亦反對他成為藝人，決定把他從國家隊的名單中除名，逐回陝西隊；同年他在[南京](../Page/南京.md "wikilink")[第十届全运会男子](../Page/十運會.md "wikilink")10米高台单人冠軍、双人第四名。

2006年，田亮在[西安開設](../Page/西安.md "wikilink")「陝西田亮體育產業發展有限公司」，並任[董事長](../Page/董事長.md "wikilink")。2007年1月16日，成為[西安體院高級教練](../Page/西安體院.md "wikilink")。2007年3月下旬，決定不參加同年4月全國冠軍賽。2007年3月26日，[陕西省体育局局长李明华證實田亮已正式退役](../Page/陕西省体育局.md "wikilink")，李稱這是田亮與陝西省游泳運動管理中心反覆溝通後之決定，田亮將繼續擔任陝西省游泳運動管理中心副主任、西安跳水學校校長，同年9月將繼續[清華大學經濟管理學院體育人文社會學專業碩士](../Page/清華大學經濟管理學院.md "wikilink")[研究生學業](../Page/研究生.md "wikilink")。

## 投身演艺圈

另由於2004年雅典奥运会後，田亮由於俊俏外表與突出之表現，多次邀請出席公開場合、商品代言；亦因此曾打算進軍香港娛樂圈，亦因此曾與多位游泳隊員或藝人傳出緋聞，包括[郭晶晶](../Page/郭晶晶.md "wikilink")、[葉璇](../Page/葉璇.md "wikilink")、[戴娇倩](../Page/戴娇倩.md "wikilink")、[刘玉丹](../Page/刘玉丹.md "wikilink")、[吴敏霞](../Page/吴敏霞.md "wikilink")、[叶一茜等](../Page/叶一茜.md "wikilink")，而他與郭晶晶之關係因涉及[霍啟剛之](../Page/霍啟剛.md "wikilink")[三角關係](../Page/三角關係.md "wikilink")，最為傳媒注目。

田亮與[叶一茜的關係在](../Page/叶一茜.md "wikilink")2006年10月下旬終為雙方證實。两人於2007年11月29日奉子成婚，妻子2008年4月在老家[福建产下一女田雨橙](../Page/福建.md "wikilink")，並於2011年再度懷孕，到香港产下兒子田宸羽；其子為香港籍身分。而被部分內地網民質疑違反國家的[計劃生育政策](../Page/計劃生育.md "wikilink")，田應面臨[雙開](../Page/雙開.md "wikilink")（開除黨籍、開除公職）懲處\[4\]。但也有人認為，田亮在港產子完全符合有關法律規定。\[5\]

### 著作

  - 《最亮的十米》，[作家出版社](../Page/作家出版社.md "wikilink")2006年3月出版，ISBN
    9787506336093
  - 《臭爸爸》，[长江文艺出版社](../Page/长江文艺出版社.md "wikilink")2014年7月出版，ISBN
    9787535473257

### 电视剧

  - 2009年 《[牛郎织女](../Page/牛郎织女_\(2009年电视剧\).md "wikilink")》饰
    [牛郎](../Page/牛郎.md "wikilink")
  - 2009年 《[雷鋒](../Page/雷鋒_\(电视剧\).md "wikilink")》饰
    [雷鋒](../Page/雷鋒.md "wikilink")
  - 2010年 《[无懈可击之美女如云](../Page/无懈可击之美女如云.md "wikilink")》 饰 石堅
  - 2011年 《[泪洒尘缘](../Page/泪洒尘缘.md "wikilink")》 饰 董浩天
  - 2012年 《[王的女人](../Page/王的女人_\(中国电视剧\).md "wikilink")》 饰
    [韩信](../Page/韩信.md "wikilink")
  - 2014年 《[骄阳似我](../Page/骄阳似我_\(电视剧\).md "wikilink")》 飾演 林風

### 电影

  - 2008年 《[出水芙蓉](../Page/出水芙蓉_\(2010年电影\).md "wikilink")》
  - 2011年 《[笑詠春](../Page/笑詠春.md "wikilink")》
  - 2011年 《[不再讓你孤單](../Page/不再讓你孤單.md "wikilink")》\[6\]
  - 2012年 《[跑出一片天](../Page/跑出一片天.md "wikilink")》
  - 2013年 《[都是手机惹的祸](../Page/都是手机惹的祸.md "wikilink")》
  - 2013年 《[不二神探](../Page/不二神探.md "wikilink")》
  - 2014年
    《[爸爸去哪儿](../Page/爸爸去哪兒_\(電影\).md "wikilink")》攜同女兒[田雨橙參加](../Page/田雨橙.md "wikilink")
  - 2014年 《[國寶疑雲](../Page/國寶疑雲.md "wikilink")》
  - 2015年 《[爸爸的假期](../Page/爸爸的假期.md "wikilink")》
  - 2015年 《[将错就错](../Page/将错就错.md "wikilink")》
  - 2015年 《[海岛之恋](../Page/海岛之恋.md "wikilink")》(2011年拍攝)
  - 2017年 《[瑪格麗特的春天](../Page/瑪格麗特的春天.md "wikilink")》

### 綜藝節目

  - 2013年 《[中国星跳跃](../Page/中国星跳跃.md "wikilink")》明星队长
  - 2013年 《[爸爸去哪兒
    (第一季)](../Page/爸爸去哪兒_\(第一季\).md "wikilink")》攜同女兒[田雨橙參加](../Page/田雨橙.md "wikilink")
  - 2015年 《[一路上有你
    (第一季)](../Page/一路上有你_\(電視節目\).md "wikilink")》攜同妻子[叶一茜參加](../Page/叶一茜.md "wikilink")
  - 2015年 《[爲她而戰](../Page/爲她而戰.md "wikilink")》
  - 2015年 《[真心英雄](../Page/真心英雄_\(电视节目\).md "wikilink")》
  - 2015年 《[全员加速中](../Page/全员加速中.md "wikilink")1》嘉宾
  - 2016年 《[全员加速中](../Page/全员加速中.md "wikilink")2》嘉宾
  - 2016年 《[来吧冠军](../Page/来吧冠军.md "wikilink") (第一季)》足球特辑嘉宾
  - 2016年 [星廚駕到第三季](../Page/星廚駕到.md "wikilink") 固定出演
  - 2016年 《[爸爸去哪兒
    (第四季)](../Page/爸爸去哪兒_\(第四季\).md "wikilink")》攜同兒子[田宸羽參加](../Page/田宸羽.md "wikilink")
  - 2017年 [王牌对王牌 (真人秀)](../Page/王牌对王牌_\(真人秀\).md "wikilink")
  - 2017年 《[我們的征途](../Page/我們的征途.md "wikilink")》

### 電視主持

  - 2014年 《[頂級跑車秀](../Page/頂級跑車秀.md "wikilink")（英文：[Top
    Gear](../Page/Top_Gear.md "wikilink")）中國版本》

## 音乐作品

### 单曲

| 發行日期    | 歌曲名稱                                                                                                                                                                                                                                                                                                        | 所屬專輯、备注          |
| ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| 2008年6月 | 《十項全能》合唱与[成龍](../Page/成龍.md "wikilink")、[容祖兒](../Page/容祖兒.md "wikilink")、[黃耀明](../Page/黃耀明.md "wikilink")、[謝霆鋒](../Page/謝霆鋒.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")、[泳兒](../Page/泳兒.md "wikilink")、[李準基](../Page/李準基.md "wikilink")、[林峯](../Page/林峯.md "wikilink")、[鄭希怡](../Page/鄭希怡.md "wikilink") |                  |
| 2011年4月 | 《想你的笑》合唱与[叶一茜](../Page/叶一茜.md "wikilink")                                                                                                                                                                                                                                                                   |                  |
| 2015年1月 | 《一路上有爱》合唱与[叶一茜](../Page/叶一茜.md "wikilink")、[何洁](../Page/何洁.md "wikilink")、[赫子铭](../Page/赫子铭.md "wikilink")、[张智霖](../Page/张智霖.md "wikilink")、[袁咏仪](../Page/袁咏仪.md "wikilink")                                                                                                                                | 浙江卫视《一路上有你》节目主题曲 |

## 參考資料

## 外部連結

[category:重庆籍运动员](../Page/category:重庆籍运动员.md "wikilink")

[Category:田姓](../Category/田姓.md "wikilink")
[Category:重庆演员](../Category/重庆演员.md "wikilink")
[Category:中国电视男演员](../Category/中国电视男演员.md "wikilink")
[Category:中国跳水运动员](../Category/中国跳水运动员.md "wikilink")
[Category:中国奥运跳水运动员](../Category/中国奥运跳水运动员.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:中国奥林匹克运动会银牌得主](../Category/中国奥林匹克运动会银牌得主.md "wikilink")
[Category:中国奥林匹克运动会铜牌得主](../Category/中国奥林匹克运动会铜牌得主.md "wikilink")
[Category:世界游泳錦標賽跳水項目冠軍](../Category/世界游泳錦標賽跳水項目冠軍.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:國際游泳名人堂成員](../Category/國際游泳名人堂成員.md "wikilink")
[Category:2000年夏季奥林匹克运动会奖牌得主](../Category/2000年夏季奥林匹克运动会奖牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會獎牌得主](../Category/2004年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奧林匹克運動會跳水獎牌得主](../Category/奧林匹克運動會跳水獎牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會跳水運動員](../Category/1996年夏季奧林匹克運動會跳水運動員.md "wikilink")
[Category:2000年夏季奧林匹克運動會跳水運動員](../Category/2000年夏季奧林匹克運動會跳水運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會跳水運動員](../Category/2004年夏季奧林匹克運動會跳水運動員.md "wikilink")
[Category:1998年亞洲運動會跳水運動員](../Category/1998年亞洲運動會跳水運動員.md "wikilink")
[Category:2002年亞洲運動會跳水運動員](../Category/2002年亞洲運動會跳水運動員.md "wikilink")
[Category:1998年亞洲運動會金牌得主](../Category/1998年亞洲運動會金牌得主.md "wikilink")
[Category:2002年亞洲運動會金牌得主](../Category/2002年亞洲運動會金牌得主.md "wikilink")
[Category:亞洲運動會跳水獎牌得主](../Category/亞洲運動會跳水獎牌得主.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:2000年跳水世界杯金牌得主](../Category/2000年跳水世界杯金牌得主.md "wikilink")

1.  [亲历奥运-田亮（上）](http://space.tv.cctv.com/act/video.jsp?videoId=VIDE1208273775325606)


2.
3.  [图文-悉尼奥运(27届)中国金牌榜
    田亮10米台压群雄](http://2008.sina.com.cn/hd/aq/p/2007-08-06/220221700.shtml)

4.  [2012年4月14日之東方日報——田亮「超生」
    做雙非爸爸](http://orientaldaily.on.cc/cnt/news/20120414/00176_035.html)

5.  [新快报：田亮赴港生二胎是“违法生育”吗？](http://yzz661023.blog.163.com/blog/static/3236657201232185512776/)


6.