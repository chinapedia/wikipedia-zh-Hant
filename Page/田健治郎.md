[田健治郎與內地延長主義_Den_Kenjirō_and_Extension_of_Home_Rule_Policy_treating_Taiwan_as_an_extension_of_Japan.jpg](https://zh.wikipedia.org/wiki/File:田健治郎與內地延長主義_Den_Kenjirō_and_Extension_of_Home_Rule_Policy_treating_Taiwan_as_an_extension_of_Japan.jpg "fig:田健治郎與內地延長主義_Den_Kenjirō_and_Extension_of_Home_Rule_Policy_treating_Taiwan_as_an_extension_of_Japan.jpg")之報導\]\]
**田健治郎**（），[字](../Page/表字.md "wikilink")**子勤**，[號](../Page/號.md "wikilink")**讓山**，[日本](../Page/日本.md "wikilink")[兵庫縣人](../Page/兵庫縣.md "wikilink")，[台灣日治時期第](../Page/台灣日治時期.md "wikilink")8任[總督](../Page/台灣總督.md "wikilink")。精通[漢學](../Page/漢學.md "wikilink")，畢業於[東京帝國大學](../Page/東京大學.md "wikilink")，歷任[眾議員](../Page/眾議員.md "wikilink")、[貴族院議員](../Page/貴族院.md "wikilink")、[神奈川等縣警部長](../Page/神奈川.md "wikilink")、[遞信省大臣](../Page/遞信省.md "wikilink")、農商務大臣兼[司法大臣等](../Page/法務大臣.md "wikilink")。

## 生平

早年任警務官僚，1890年受遞信大臣[後藤象二郎賞識](../Page/後藤象二郎.md "wikilink")，入[遞信省任職](../Page/遞信省.md "wikilink")。歷任通信局長、遞信次官、鉄道会議幹事。1895年日本內閣成立[臺灣事務局時](../Page/臺灣.md "wikilink")，即為交通部門的代表委員，開始與臺灣有了淵源。1901年當選眾議院議員，並連任一次。後又一度回任遞信次官。1906年敕選為[貴族院議員](../Page/貴族院議員.md "wikilink")，翌年封[男爵](../Page/男爵.md "wikilink")。1916年—1918年，在[寺內正毅](../Page/寺內正毅.md "wikilink")[內閣中擔任](../Page/內閣.md "wikilink")[遞信大臣](../Page/遞信大臣.md "wikilink")。

1919年10月29日成為台灣第一任文官總督。田健治郎任內的治台方針為[內地延長主義](../Page/內地延長主義.md "wikilink")，即是同化政策。標榜「內台融合」、「一視同仁」、「同化政策」，欲讓台灣人如日本人一般效忠[天皇](../Page/天皇.md "wikilink")。總督任內完成許多重大改革，如改革地方制度為五州二廳、以「[法三號](../Page/法三號.md "wikilink")」取代「[三一法](../Page/三一法.md "wikilink")」為統治之根本大法，並設府評議會討論律令施行之相關問題。改革地方行政為州市街庄制，並設協議會以備諮詢。廢止[笞刑](../Page/笞刑.md "wikilink")、廢止小學教師配戴[武士刀](../Page/武士刀.md "wikilink")、通過[日台共學制度](../Page/日台共學制度.md "wikilink")、台日通婚制、任用[八田與一工程師修建](../Page/八田與一.md "wikilink")[嘉南大圳](../Page/嘉南大圳.md "wikilink")、促成[東宮行啟](../Page/東宮行啟.md "wikilink")（時為皇太子的[昭和天皇來臺](../Page/昭和天皇.md "wikilink")）之巡禮儀式等。

1923年9月辭總督職，因[關東大地震而返日轉任日本農商務大臣兼](../Page/關東大地震.md "wikilink")[司法大臣](../Page/法務大臣.md "wikilink")。1925年任樞密顧問官。

昭和5年（1930年）、於東京市玉川村（現在東京都世田谷区）上野毛自宅（万象閣）昏倒因腦出血及肺炎死亡。葬於多磨靈園。

## 研究

鍾淑敏著有《田健治郎傳》。

關於田健治郎1905年－1930年以漢文書寫的日記，為日本近代政治史重要史料，2000年中研院台史所籌備處業已出版上冊。

## 著作

  -
  -
## 相關條目

  - [台灣日治時期](../Page/台灣日治時期.md "wikilink")
  - [台灣總督](../Page/台灣總督.md "wikilink")
  - 台灣事務局
  - [內地延長主義](../Page/內地延長主義.md "wikilink")
  - 以「[法三號](../Page/法三號.md "wikilink")」取代「[三一法](../Page/三一法.md "wikilink")」
  - 實施新「[台灣教育令](../Page/台灣教育令.md "wikilink")」

## 參考文獻

  -
  - 1932年刊の復刻

  -
## 外部連結

  - [田健治郎](http://www.eonet.ne.jp/~zarigani/kilp/kilp23.htm)
  - [田健治郎の墓](http://www6.plala.or.jp/guti/cemetery/PERSON/T/den_k.html)
  - [国立国会図書館 憲政資料室
    田健治郎関係文書](https://web.archive.org/web/20080612205852/http://www.ndl.go.jp/jp/data/kensei_shiryo/kensei/dennkennjirou.html)
  - [臺灣史檔案資源系統 -
    田健治郎文書](http://tais.ith.sinica.edu.tw/sinicafrsFront/browsingLevel1.jsp?xmlId=0000310479)

[Category:貴族院敕選議員](../Category/貴族院敕選議員.md "wikilink")
[Category:日本司法大臣](../Category/日本司法大臣.md "wikilink")
[Category:日本遞信官僚](../Category/日本遞信官僚.md "wikilink")
[Category:臺灣總督](../Category/臺灣總督.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")
[Category:兵庫縣出身人物](../Category/兵庫縣出身人物.md "wikilink") [Category:日本眾議院議員
1898–1902](../Category/日本眾議院議員_1898–1902.md "wikilink")
[Category:日本眾議院議員
1902](../Category/日本眾議院議員_1902.md "wikilink")
[Category:日本男爵](../Category/日本男爵.md "wikilink")
[Category:兵庫縣選出日本眾議院議員](../Category/兵庫縣選出日本眾議院議員.md "wikilink")