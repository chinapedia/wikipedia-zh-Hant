**田昭武**（），中国著名电化学家，[福建省](../Page/福建省.md "wikilink")[福州市](../Page/福州市.md "wikilink")[螺洲人](../Page/螺洲镇.md "wikilink")。著名物理化学家[田中群之父](../Page/田中群.md "wikilink")。

## 生平

先后就读于福建师院附属第二小学、[福州第一中学](../Page/福州第一中学.md "wikilink")、[永安中学和](../Page/永安中学.md "wikilink")[厦门大学化学系](../Page/厦门大学.md "wikilink")。毕业后留校，从事科研教学工作近五十年。现任厦门大学教授（1978－），[中国科学院院士](../Page/中国科学院院士.md "wikilink")（1980－），1984年获[英国](../Page/英国.md "wikilink")[威尔士大学名誉博士学位](../Page/威尔士大学.md "wikilink")。
1982－1989任厦门大学校长，1986年当选为中国化学会理事长，1996年当选为[国际电化学会副主席和](../Page/国际电化学会.md "wikilink")[第三世界科学院院士](../Page/第三世界科学院.md "wikilink")。他重视交叉学科，研究领域扩展到[光电化学](../Page/光电化学.md "wikilink")、[电化学扫描](../Page/电化学.md "wikilink")[隧道显微技术](../Page/隧道.md "wikilink")、[三维微加工技术](../Page/三维微加工技术.md "wikilink")、芯片生化实验室、[谱学电化学和](../Page/谱学电化学.md "wikilink")[量子电化学等](../Page/量子电化学.md "wikilink")。提出多孔电极极化的“特征电流”概念和“不平整液膜”模型，创立电极绝对等效电路的新解法和测量电极瞬间阻抗的选相调辉技术。首创并推广电化学技术和仪器，如新一代的离子色谱抑制器、微区腐蚀测量系统和国内第一台电化学综合测试仪等。在化学电源、金属腐蚀和电化学分析方面，都有结合生产实际的研究成果。

## 外部链接

  - [中国化学会历届理事会](http://www.chemsoc.org.cn/nlr/?hid=153&cid=29)
    中国化学会官方网站。 2017-11-28
  - [厦门大学历任领导](https://www.xmu.edu.cn/about/lirenlingdao) 厦门大学官方网站。
    2017-11-28

{{-}}

[T田](../Category/中国化学家.md "wikilink")
[T田](../Category/中国科学院化学部院士.md "wikilink")
[T田](../Category/仓山人.md "wikilink")
[Category:厦门大学校友](../Category/厦门大学校友.md "wikilink")
[Category:厦门大学教授](../Category/厦门大学教授.md "wikilink")
[Category:厦门大学校长](../Category/厦门大学校长.md "wikilink")
[ZHAO](../Category/田姓.md "wikilink")
[Category:世界科学院院士](../Category/世界科学院院士.md "wikilink")