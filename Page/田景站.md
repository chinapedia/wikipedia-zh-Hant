[TinKingNStation_Platform3.jpg](https://zh.wikipedia.org/wiki/File:TinKingNStation_Platform3.jpg "fig:TinKingNStation_Platform3.jpg")的終點站\]\]

**田景站**（[英文](../Page/英文.md "wikilink")：**Tin King
Stop**）是[港鐵](../Page/港鐵.md "wikilink")[輕鐵車站](../Page/香港輕鐵.md "wikilink")。代號140，屬單程車票[第3收費區](../Page/輕鐵第3收費區.md "wikilink")，共有3個月台，為[507總站](../Page/香港輕鐵507綫.md "wikilink")。車站位於鳴琴路、田景路北面交界，在[田景邨北面](../Page/田景邨.md "wikilink")，為田景站及週邊地區居民提供服務。

## 車站結構

### 車站樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td></td>
<td><p><a href="../Page/寶田邨.md" title="wikilink">寶田邨</a></p></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p> 往<a href="../Page/元朗站_(輕鐵).md" title="wikilink">元朗</a>（<a href="../Page/建生站.md" title="wikilink">建生</a>）|}}</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p> 往<a href="../Page/三聖站.md" title="wikilink">三聖</a> {{!}}  往<a href="../Page/屯門碼頭站.md" title="wikilink">屯門碼頭</a>（<a href="../Page/良景站.md" title="wikilink">良景</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>田景路、<a href="../Page/田景邨.md" title="wikilink">田景邨</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 車站月台

田景站設3個月台，其中1個為[507總站月台](../Page/香港輕鐵507綫.md "wikilink")，並設有掉頭方向路軌至本站2號月台供507綫停站上客，現時用作507綫掉頭使用。

### 車站周邊

  - [田景邨](../Page/田景邨.md "wikilink")
  - [寶田邨](../Page/寶田邨.md "wikilink")
  - [兆邦苑](../Page/兆邦苑.md "wikilink")
  - [兆隆苑](../Page/兆隆苑.md "wikilink")
  - [盈豐園](../Page/盈豐園.md "wikilink")
  - 寶田商場
  - [屯門西北游泳池](../Page/屯門西北游泳池.md "wikilink")
  - [珀御](../Page/珀御.md "wikilink")(興建中)

## 鄰近車站

## 接駁交通

<div class="NavFrame collapsed" style="background-color: #FFFF00; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

接駁交通列表

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [港鐵巴士](../Page/港鐵巴士.md "wikilink")（[八達通免費轉乘優惠](../Page/八達通.md "wikilink")）：
      - [K58](../Page/港鐵巴士K58綫.md "wikilink") -

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參見

  - [香港輕鐵](../Page/香港輕鐵.md "wikilink")

## 外部連結

  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵街道圖](http://www.mtr.com.hk/archive/ch/services/maps/07gif)
  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵行車時間表](http://www.mtr.com.hk/chi/lr_bus/schedule/schedule_index.html)

[Category:屯門](../Category/屯門.md "wikilink")
[Category:以屋苑命名的香港輕鐵車站](../Category/以屋苑命名的香港輕鐵車站.md "wikilink")
[Category:1988年启用的铁路车站](../Category/1988年启用的铁路车站.md "wikilink")
[Category:屯門區鐵路車站](../Category/屯門區鐵路車站.md "wikilink")