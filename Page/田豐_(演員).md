**田豐**（），本名**田毓錕**，為[香港暨](../Page/香港.md "wikilink")[臺灣資深實力派](../Page/臺灣.md "wikilink")[演員](../Page/演員.md "wikilink")，生於[河南](../Page/河南.md "wikilink")[鄭州](../Page/鄭州.md "wikilink")。曾任[台北市电影电视演艺业职业工会評議委員](../Page/台北市电影电视演艺业职业工会.md "wikilink")，2014年獲頒[第51屆金馬獎終身成就獎](../Page/第51屆金馬獎.md "wikilink")\[1\]。

## 生平

田豐於1928年6月4日生於河南鄭州，學生時代便愛好戲劇，並開始參與舞臺劇演出。1949年，他加入軍中話劇團，隨軍來臺。旋即演出[張徹執導之臺灣首部國語片](../Page/張徹.md "wikilink")《[阿里山風雲](../Page/阿里山風雲.md "wikilink")》，躍上大銀幕\[2\]。唯臺灣當時國語電影產量不多，他的主業仍是舞臺劇演員\[3\]。

1960年，田豐赴[香港發展](../Page/香港.md "wikilink")，剛進[邵氏公司兩個月就拍了九部電影](../Page/邵氏公司.md "wikilink")\[4\]，遂有「田九部」稱號\[5\]。其後，他又演出《楊貴妃》（1962）、《[獨臂刀](../Page/獨臂刀.md "wikilink")》（1967）等多部邵氏電影\[6\]。據與他多次合作的演員[李影回憶](../Page/李影_\(臺灣演員\).md "wikilink")，當時他們與共演《故都春夢》（1964）的[井淼](../Page/井淼.md "wikilink")、[雷鳴並稱香港影壇的](../Page/雷鳴_\(臺灣演員\).md "wikilink")「臺灣四大金剛」\[7\]。而在田豐超過六十年的表演生涯中，合作過的導演包括[李翰祥](../Page/李翰祥.md "wikilink")、[胡金銓](../Page/胡金銓.md "wikilink")、[張徹](../Page/張徹.md "wikilink")、[羅維](../Page/羅維.md "wikilink")、[程剛](../Page/程剛.md "wikilink")、[侯孝賢](../Page/侯孝賢.md "wikilink")、[吳宇森](../Page/吳宇森.md "wikilink")、[成龍](../Page/成龍.md "wikilink")、[王家衛等](../Page/王家衛.md "wikilink")，演出作品超過兩百部，角色類型更是宜古宜今、可忠可奸，被視作萬能配角\[8\]。

除了幕前演出，田豐也參與幕後工作：1950年代臺灣盛行[台語片](../Page/台語片.md "wikilink")，他曾擔任過副導、製片等職，從導演[白克處學習到](../Page/白克.md "wikilink")[電影理論](../Page/電影理論.md "wikilink")\[9\]，後又與[李行](../Page/李行.md "wikilink")、[張方霞合導](../Page/張方霞.md "wikilink")《[王哥柳哥遊台灣](../Page/王哥柳哥遊台灣.md "wikilink")》。赴港發展以後，他曾任《[梁山伯與祝英台](../Page/梁山伯與祝英台_\(1963年電影\).md "wikilink")》（1963）的副導演，更執導《金印仇》（1971）等電影\[10\]。

1971年，田豐為了要賺更多錢供兩個孩子海外求學，而離開邵氏公司\[11\]。1970年代末、1980年代初，香港國語片產量減少，粵語電視劇興起。田豐在港演出銳減，一度移民[美國](../Page/美國.md "wikilink")。後在[林福地邀請下](../Page/林福地.md "wikilink")，回臺跨足小螢幕，於是有[臺視連續劇](../Page/臺視.md "wikilink")《[星星知我心](../Page/星星知我心.md "wikilink")》中膾炙人口的演出。2000年，他以《[公視人生劇展](../Page/公視人生劇展.md "wikilink")－風車》中的顏教授一角，入圍[第35屆金鐘獎最佳單元劇男配角獎](../Page/第35屆金鐘獎.md "wikilink")。2002年，他以《公視人生劇展－老魏》中的[政治犯老魏一角](../Page/政治犯.md "wikilink")，入圍[第37屆金鐘獎最佳單元劇男主角](../Page/第37屆金鐘獎.md "wikilink")。2014年11月22日，他獲頒[第51屆金馬獎終身成就獎](../Page/第51屆金馬獎.md "wikilink")\[12\]。

2010年代，田豐雖年過八旬仍然新潮，熱衷使用[電子郵件及](../Page/電子郵件.md "wikilink")[臉書](../Page/臉書.md "wikilink")\[13\]。此外，他愛好喝茶並收藏茶壶，2010年8月2日曾在[华视节目](../Page/华视.md "wikilink")《[天天哈哈笑](../Page/天天哈哈笑.md "wikilink")》展示了蒐集多年的茶壶。

2015年10月22日晚間傳出，田豐日前在香港過世（經查證為2015年10月6日上午八時前後），享壽八十七歲\[14\]；次日，[台北金馬影展執行委員會證實此一消息](../Page/台北金馬影展執行委員會.md "wikilink")\[15\]。

## 幕後工作作品

### 導演

  - 1959年 《[王哥柳哥遊台灣](../Page/王哥柳哥遊台灣.md "wikilink")》（與李行、張方霞合導）
  - 1971年 《金印仇》 The Golden Seal
  - 1976年 《大江南北》 The Double Double Crosser
    （[王羽](../Page/王羽.md "wikilink")、[嘉凌主演](../Page/嘉凌.md "wikilink")）

### 副導演

  - 1957年 《台南霧夜大血案》
  - 1963年 《[梁山伯與祝英台](../Page/梁山伯與祝英台_\(1963年電影\).md "wikilink")》
  - 1964年 《玉堂春》

## 演出作品

### 電影

  - 1950年 《[阿里山風雲](../Page/阿里山風雲.md "wikilink")》
  - 1952年 《春滿人間》
  - 1953年 《嘉禾生春》
  - 1953年 《烽火麗人》
  - 1953年 《寶島大動脈》
  - 1956年 《沒有女人的地方》
  - 1958年 《血戰》
  - 1959年 《蕩婦與聖女》
  - 1960年 《太平洋之鯊》 Shark Of The Pacific
  - 1960年 《良心與罪惡》
  - 1960年 《[喋血販馬場](../Page/喋血販馬場.md "wikilink") Time is Running Short》
  - 1960年 《[花田錯](../Page/花田錯.md "wikilink") Bride Napping》
  - 1963年 《[妲己](../Page/妲己.md "wikilink") The Last Woman of Shang》
  - 1963年 《[鳳還巢](../Page/鳳還巢.md "wikilink") Return of the Phoenix》
  - 1963年 《[夜半驚魂](../Page/夜半驚魂.md "wikilink") The horrible night》
  - 1963年 《[寶蓮燈](../Page/寶蓮燈.md "wikilink") The Lotus Lamp》
  - 1964年 《[红伶淚](../Page/红伶淚.md "wikilink") Vermillion Door》
  - 1964年 《[萬古流芳](../Page/萬古流芳.md "wikilink") The Grand Substitution》
  - 1964年 《[小雲雀](../Page/小雲雀.md "wikilink") The Lark》
  - 1964年 《[血手印](../Page/血手印.md "wikilink") The Crimson Palm》〔飾-包興〕
  - 1964年 《[血濺牡丹紅](../Page/血濺牡丹紅.md "wikilink") The Warlord and the
    Actress》
  - 1965年 《[江湖奇俠](../Page/江湖奇俠.md "wikilink") Temple of the Red
    Lotus》〔飾-甘龍〕
  - 1965年 《[鴛鴦劍俠](../Page/鴛鴦劍俠.md "wikilink") The Twin Swords》〔飾-甘龍〕
  - 1965年 《[宋宮秘史](../Page/宋宮秘史.md "wikilink") Inside the Forbidden City》
  - 1965年 《[蝴蝶盃](../Page/蝴蝶盃_\(1965年電影\).md "wikilink") The Butterfly
    Chalice》〔飾-胡彥〕
  - 1966年 《[藍與黑續集](../Page/蓝与黑_\(电影\).md "wikilink") The Blue and the
    Black Part 2》〔飾-曹副官〕
  - 1966年 《[邊城三俠](../Page/邊城三俠.md "wikilink") Magnificent Trio》
  - 1967年 《[大刺客](../Page/大刺客.md "wikilink") The Assassin》〔飾-嚴仲子〕
  - 1967年 《[獨臂刀](../Page/獨臂刀.md "wikilink") One Armed Swordsman》〔飾-齊如風〕
  - 1967年 《[斷腸劍](../Page/斷腸劍.md "wikilink") The Trail of the Broken
    Blade》〔飾-屠千秋〕
  - 1967年 《[儒俠](../Page/儒俠_\(1967年電影\).md "wikilink") The Silent
    Swordsman》〔飾-吳浩然〕
  - 1967年 《[神劍震江湖](../Page/神劍震江湖.md "wikilink") The Thundering Sword》
  - 1967年 《[船](../Page/船.md "wikilink") My Dreamboat》
  - 1967年 《[香江花月夜](../Page/香江花月夜.md "wikilink") Hong Kong
    Nocturne》〔飾-嚴老師〕
  - 1968年 《[神刀](../Page/神刀.md "wikilink") The Sword of Swords》
  - 1968年 《[女俠黑蝴蝶](../Page/女俠黑蝴蝶.md "wikilink") Black Butterfly》
  - 1968年 《[怪俠](../Page/怪俠.md "wikilink") The Magnificent Swordsman》
  - 1968年 《[玉面飛狐](../Page/玉面飛狐.md "wikilink") The Silver Fox》
  - 1969年 《[虎膽](../Page/虎膽.md "wikilink") Raw courage》〔飾-嚴蒼〕
  - 1969年 《[十二金錢鏢](../Page/十二金錢鏢.md "wikilink") Twelve Deadly
    Coins》〔飾-俞劍平〕
  - 1970年 《[獨臂刀王](../Page/獨臂刀王.md "wikilink") Return of the one-armed
    swordsman》〔飾-無相王〕
  - 1970年 《[慾焰狂流](../Page/慾焰狂流.md "wikilink") Torrent of Desire》
  - 1970年 《[大羅劍俠](../Page/大羅劍俠.md "wikilink") The secret of the
    dirk》〔飾-王山虎〕
  - 1970年 《 Brothers five》〔飾-龍振風〕
  - 1971年 《[萬箭穿心](../Page/馬新貽.md "wikilink") Oath of death》〔飾-馬慶庭〕
  - 1971年 《[冰天俠女](../Page/冰天俠女.md "wikilink") Vengeance of a snow
    girl》〔飾-高允〕
  - 1971年 《[影子神鞭](../Page/影子神鞭.md "wikilink") The shadow whip》〔飾-方承天〕
  - 1971年 《[夕陽戀人](../Page/夕陽戀人.md "wikilink") Sunset》
  - 1971年 《[金印仇](../Page/金印仇.md "wikilink") The Golden Seal》
  - 1972年 《[精武門](../Page/精武門_\(電影\).md "wikilink") Fist of
    fury》〔飾-范君俠／大師兄〕
  - 1972年 《[十四女英豪](../Page/十四女英豪.md "wikilink") The 14 amazons》〔飾-西夏王〕
  - 1973年 《迎春閣之風波 The Fate of Lee Khan》
  - 1973年 《[冷面虎](../Page/冷面虎.md "wikilink") A Man Called Tiger》
  - 1973年 《[馬路小英雄](../Page/馬路小英雄.md "wikilink") Back Alley Princess》
  - 1973年 《[海員七號](../Page/海員七號.md "wikilink") Seaman no.7》
  - 1974年 《[綽頭狀元](../Page/綽頭狀元.md "wikilink") Naughty\!Naughty\!》
  - 1979年 《[山中傳奇](../Page/山中傳奇.md "wikilink") Legend of the
    mountain》〔飾-老張〕
  - 1979年 《空山靈雨 Raining in the Mountain》
  - 1980年 《[師弟出馬](../Page/師弟出馬.md "wikilink") The young master》〔飾-田師父〕
  - 1980年 《[隻手遮天](../Page/隻手遮天.md "wikilink") Absolute Monarch》
  - 1980年 《[名劍](../Page/名劍.md "wikilink") The sword》〔飾-花千樹〕
  - 1980年 《[救世者](../Page/救世者.md "wikilink") The Saviour》
  - 1981年 《[執法者](../Page/執法者.md "wikilink") The Executor》〔飾-陳振東爵士〕
  - 1981年 《[線人](../Page/線人.md "wikilink") The Informer》
  - 1981年 《[粉骷髏](../Page/粉骷髏.md "wikilink") The Phantom Killer》
  - 1982年 《[龍少爺](../Page/龍少爺.md "wikilink") Dragon Lord》〔飾-賀原外〕
  - 1983年 《[天下第一](../Page/天下第一_\(1983年\).md "wikilink") All the King's
    Men》〔飾-[柴榮](../Page/柴榮.md "wikilink")〕
  - 1983年 《[碼頭](../Page/碼頭.md "wikilink") The Pier》
  - 1983年 《[陰陽錯](../Page/陰陽錯.md "wikilink") Esprit d'amour》
  - 1986年 《[童年往事](../Page/童年往事.md "wikilink") A Time to Live, a Time to
    Die》
  - 1986年 《[英雄本色](../Page/英雄本色.md "wikilink") A Better
    Tomorrow》〔飾-宋景文／宋子豪、宋子杰父〕
  - 1987年 《[好小子Ⅲ苦兒流浪記](../Page/好小子_\(1986年電影\).md "wikilink") Kung Fu
    Kids Ⅲ》
  - 1989年 《[奇蹟](../Page/奇蹟_\(電影\).md "wikilink") Mr. Canton and Lady
    Rose》〔飾-顧新全〕
  - 1989年 《[三狼奇案](../Page/三狼奇案.md "wikilink") Sentenced to Hang》
  - 1989年 《[發達祕笈](../Page/發達祕笈.md "wikilink") How to Be a
    Millionaire》〔飾-申氏集團董事長〕
  - 1989年 《[我在黑社會的日子](../Page/我在黑社會的日子.md "wikilink") Triads - The
    Inside Story》〔飾-李坤／坤叔／李萬豪父〕
  - 1990年 《[客途秋恨](../Page/客途秋恨_\(1990年電影\).md "wikilink") Song of the
    Exile》
  - 1990年 《[壯志豪情](../Page/壯志豪情.md "wikilink") Whampoa Blues》
  - 1990年 《[至尊計狀元才](../Page/至尊計狀元才.md "wikilink") No Risk, No
    Gain》〔飾-楊震〕
  - 1991年 《[中環英雄](../Page/中環英雄.md "wikilink") Don't Fool Me》
  - 1991年 《[賭俠2之上海灘賭聖](../Page/賭俠2之上海灘賭聖.md "wikilink")》God of Gamblers
    III: Back to Shanghai〔飾-上海市長〕
  - 1991年 《[魔畫情](../Page/魔畫情.md "wikilink") Fantasy Romance》
  - 1991年 《[至尊無上Ⅱ永霸天下](../Page/至尊無上Ⅱ永霸天下.md "wikilink") Casino RaidersⅡ》
  - 1991年 《[玉圃團之偷情寶鑑](../Page/玉圃團之偷情寶鑑.md "wikilink") Sex and Zen》
  - 1992年 《[龍騰四海](../Page/龍騰四海.md "wikilink") Gun n' Rose》
  - 1993年 《[畫魂](../Page/畫魂.md "wikilink") La Peintre》
  - 1993年 《[青蛇](../Page/青蛇.md "wikilink") Green Snake》
  - 1999年 《[千喜巨龍](../Page/千喜巨龍.md "wikilink") Century Hero》
  - 2004年 《[五月之戀](../Page/五月之戀.md "wikilink") Love of May》〔飾-趙更生／趙瑄爺爺〕
  - 2004年 《[經過](../Page/經過.md "wikilink") The Passage》
  - 2004年 《[愛神](../Page/愛神_\(2004年電影\).md "wikilink")》

### 電視劇

  - 《[天龍訣](../Page/天龍訣.md "wikilink")》（1979年）飾
    [白蓮教不老神仙](../Page/白蓮教.md "wikilink")
  - 《[湖海爭霸錄](../Page/湖海爭霸錄.md "wikilink")》（1980年）飾 晏海安
  - 《[星星知我心](../Page/星星知我心.md "wikilink")》（1983年）飾 梁圳山（客串演出）
  - 《[星星的故鄉](../Page/星星的故鄉.md "wikilink")》（1984年，星星知我心[續集](../Page/續集.md "wikilink")）飾
    梁圳山（客串演出）
  - 《[昨夜星辰](../Page/昨夜星辰.md "wikilink")》（1984年）
  - 《書劍江山》（1984年）
  - 《[楓葉盟](../Page/楓葉盟.md "wikilink")》（1985年）飾 秦天岳
  - 《金粉王朝》（1986年）飾 涂慕海
  - 《我心深處》（1986年）飾 赤牛
  - 《風雲人物》（1986年）
  - 《秋海棠》（1986年）
  - 《新[絕代雙驕](../Page/絕代雙驕.md "wikilink")》（1986年）飾 江別鶴
  - 《[玫瑰人生](../Page/玫瑰人生_\(電視劇\).md "wikilink")》（1987年）飾 胡之偉
  - 《洗錢》（1990年）
  - 《[包青天](../Page/包青天_\(1993年電視劇\).md "wikilink")》（1993年）之〈真假包公〉飾 石國柱
    〈尋親記〉飾 顧清風 〈乞丐王孫〉飾 秦華 〈菩薩嶺〉 飾 忘了和尚 〈陰陽判〉飾 孫文輝 〈孝子章洛〉飾 葉青士
    〈天下第一莊〉飾 裴天瀾 〈五鼠鬧東京〉飾 孟若虚
  - 《孝的故事》（1994年）之〈紙鳶恩怨情〉
  - 《[劉伯溫傳奇](../Page/劉伯溫傳奇.md "wikilink")》（1995年）〈寸草心〉飾 唐鷹
  - 《[三國英雄傳之關公](../Page/三國英雄傳之關公.md "wikilink")》 （1996年）飾 普救寺和尚
  - 《圍爐》（1996年）
  - 《[台灣靈異事件](../Page/台灣靈異事件.md "wikilink")》（1997年）之〈幽靈船〉
  - 《[施公奇案](../Page/施公奇案_\(台灣\).md "wikilink")》（1997年）之〈孤雛淚〉飾 明王
  - 《[布袋和尚](../Page/布袋和尚.md "wikilink")》（1997年）飾 弘德師叔
  - 《[布衣钦差](../Page/布衣钦差.md "wikilink")》/《[浪子大钦差](../Page/浪子大钦差.md "wikilink")》（1998年）飾
    安老爷
  - 《[老房有喜](../Page/老房有喜.md "wikilink")》（1998年）飾 蘇老太爺
  - 《[麻辣鮮師](../Page/麻辣鮮師.md "wikilink")》（2000年）飾 伍將軍（客串演出）
  - 《風車》（2000年，公視人生劇展）飾 顏允文
  - 《台灣玉》（2000年，公視人生劇展）
  - 《老魏》（2002年，公視人生劇展）
  - 《[孽子](../Page/孽子_\(電視劇\).md "wikilink")》（2003年）飾 王家副官
  - 《晚安，明天見》（2003年，人間衛視人間關懷劇場）
  - 《[偵探物語](../Page/偵探物語.md "wikilink")》之〈寄件人，不詳〉（2005年）

## 參考資料

<references/>

## 外部連結

  -
  -
  -
  -
  - [天天哈哈笑
    田豐（优酷网视频）1](http://v.youku.com/v_show/id_XMTk0OTc0OTg0.html)、[2](http://v.youku.com/v_show/id_XMTk0OTc1MzM2.html)、[3](http://v.youku.com/v_show/id_XMTk0OTc1NjYw.html)、[4](http://v.youku.com/v_show/id_XMTk0OTc1Njgw.html)、[5](http://v.youku.com/v_show/id_XMTk0OTc1ODA4.html)

[T田](../Category/前麗的電視藝員.md "wikilink")
[T田](../Category/香港電影演員.md "wikilink")
[Category:香港電影人](../Category/香港電影人.md "wikilink")
[T田](../Category/臺灣電影男演員.md "wikilink")
[T田](../Category/臺灣電視男演員.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:香港武打演員](../Category/香港武打演員.md "wikilink")
[T田](../Category/郑州人.md "wikilink") [F豐](../Category/田姓.md "wikilink")
[T](../Category/金馬獎終身成就獎得主.md "wikilink")
[Category:台灣戰後河南移民](../Category/台灣戰後河南移民.md "wikilink")
[Category:河南演员](../Category/河南演员.md "wikilink")

1.
2.

3.

4.

5.

6.
7.

8.
9.
10.
11.

12.
13.
14.

15.