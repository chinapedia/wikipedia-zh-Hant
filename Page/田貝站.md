**田貝站**是[深圳地鐵](../Page/深圳地鐵.md "wikilink")[3號綫與](../Page/深圳地鐵3號綫.md "wikilink")[7號綫的換乘站](../Page/深圳地鐵7號綫.md "wikilink")，其中[3号綫部分](../Page/深圳地鐵3号綫.md "wikilink")，位於[翠竹路](../Page/翠竹路_\(深圳羅湖\).md "wikilink")[田貝四路至](../Page/田貝四路.md "wikilink")[太宁路段](../Page/太宁路.md "wikilink")，沿翠竹路南北向佈置，於2011年6月28日啟用\[1\]
。[7號綫部分位於](../Page/深圳地鐵7號綫.md "wikilink")[田貝四路](../Page/田貝四路.md "wikilink")[翠竹路西側](../Page/翠竹路_\(深圳羅湖\).md "wikilink")，在2016年10月28日啟用。

## 車站結構

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面（G）</strong></p></td>
<td></td>
<td><p>出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一層（B1）</strong></p></td>
<td><p>站廳</p></td>
<td><p>站廳、售票機</p></td>
</tr>
<tr class="odd">
<td><p><strong>地下二層（B2）</strong></p></td>
<td></td>
<td><p><strong></strong> <strong>往益田</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，左邊車門將會開啟</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong>往雙龍</strong> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>地下三層（B3）</strong></p></td>
<td></td>
<td><p><strong></strong> <strong>往西麗湖</strong></p></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，左邊車門將會開啟</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong></strong> <strong>往太安</strong> </p></td>
<td></td>
</tr>
</tbody>
</table>

## 运营时刻表

<table>
<thead>
<tr class="header">
<th><p>行驶方向</p></th>
<th><p>首班车</p></th>
<th><p>末班车</p></th>
<th><p>所属线路</p></th>
<th><p>高峰间隔</p></th>
<th><p>平峰间隔</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>益田</p></td>
<td><p>06:36</p></td>
<td><p>23:43</p></td>
<td></td>
<td><p>5分钟</p></td>
<td><p>8分钟</p></td>
</tr>
<tr class="even">
<td><p>双龙</p></td>
<td><p>06:31</p></td>
<td><p>23:27</p></td>
<td></td>
<td><p>5分钟</p></td>
<td><p>8分钟</p></td>
</tr>
<tr class="odd">
<td><p>西麗湖</p></td>
<td><p>06:32</p></td>
<td><p>23:02</p></td>
<td></td>
<td><p>10分钟</p></td>
<td><p>10分钟</p></td>
</tr>
<tr class="even">
<td><p>太安</p></td>
<td><p>06:38</p></td>
<td><p>23:54</p></td>
<td></td>
<td><p>10分钟</p></td>
<td><p>10分钟</p></td>
</tr>
</tbody>
</table>

## 出口

田貝站為地下車站，共設有7個出入口，其中A、B、C、D出口（俱為3號線車站設施）提供雙向扶手電梯。

<table>
<thead>
<tr class="header">
<th><p>出口編號</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>翠竹路（东）、太宁路、翠竹公园、康宁医院、翠竹街市</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>翠竹路（东）、太宁路、太安路、百仕达花园</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>翠竹路（西）、水贝一路、田贝四路、家和居装饰材料广场（总店）、文锦北路、贝丽花园、阳光天地园</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>翠竹路（西）、翠北小学、翠竹小区、田贝三路、田贝二路、罗湖供电局、马古岭小区、华盐大厦</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>田贝四路、深圳市翠北实验小学、贝丽南路</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>贝丽花园、阳光天地家园、贝丽北路、田贝四路、深圳市水田小学、水田二街、水贝万山</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>翠竹路西侧、水贝一路、田贝四路</p></td>
</tr>
</tbody>
</table>

## 路線轉乘

7號線大堂緊鄰3號線大堂，而兩線的月台沒有轉乘通道。乘客轉乘時，需乘扶手電梯、升降機或沿樓梯步行到車站大堂，再步行到另一線路的車站大堂，然後再乘扶手電梯、升降機或沿樓梯步行到另一線路的月台。

## 車站設施及藝術牆

本站設有三個洗手間，分別位於D出口旁（非付費區）、G出口旁（非付費區），及7號線月台近洪湖站的一端（付費區）。

## 鄰接車站

## 參考資料

<div class="references-small">

<references />

  - [專題：深圳地鐵3號線](https://web.archive.org/web/20070929093206/http://big5.sznews.com/zhuanti/content/2007-02/13/content_871294.htm)，2007年2月13日，深圳新聞網
  - [轨道交通近期建设线路线站位初步方案展示（二）](https://web.archive.org/web/20070928010916/http://www.szplan.gov.cn/main/zsgg/200707090211043.shtml)，深圳市規劃局

</div>

[Category:羅湖區](../Category/羅湖區.md "wikilink")
[Category:2011年啟用的鐵路車站](../Category/2011年啟用的鐵路車站.md "wikilink")
[Category:深圳地铁换乘站](../Category/深圳地铁换乘站.md "wikilink")

1.  《[我市擬在地鐵上蓋物業建政策性住房](http://www.sz.gov.cn/szyw/200708/t20070802_223172.htm)
    》，2007年8月2日，深圳新聞網