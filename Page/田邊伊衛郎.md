**田邊伊衛郎**（）是[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。6月13日出生。[東京都出身](../Page/東京都.md "wikilink")。[雙子座AB型](../Page/雙子座.md "wikilink")。

## 概要

[武藏野美術大學出身](../Page/武藏野美術大學.md "wikilink")。2000年以《》獲得第47回[小學館新人漫畫大賞少年部門佳作](../Page/小學館新人漫畫大賞.md "wikilink")。2002年於《[少年Sunday特別增刊R](../Page/少年Sunday特別增刊R.md "wikilink")》發表《LOST
PRINCESS》出道。

2003年《[結界師](../Page/結界師.md "wikilink")》於《[週刊少年Sunday](../Page/週刊少年Sunday.md "wikilink")》開始連載，此作品獲得第52回[小學館漫畫賞少年向部門](../Page/小學館漫畫賞.md "wikilink")。

初出道的時候的[筆名是](../Page/筆名.md "wikilink")「」。《結界師》連載開始時，改名為「」。

幾乎沒有露面過，第一次露面是在小學館漫畫賞頒獎典禮時被拍的照片。

[自畫像是一隻](../Page/自畫像.md "wikilink")[企鵝](../Page/企鵝.md "wikilink")，用意是為了記住自己第一次畫的漫畫的角色。

## 得獎經歷

  - 2000年，《闇の中》第47回小學館新人漫畫大賞少年部門佳作。
  - 2006年，《結界師》第52回小學館漫畫賞少年向部門。

## 作品列表

  - [結界師](../Page/結界師.md "wikilink")、全35冊、([週刊少年Sunday](../Page/週刊少年Sunday.md "wikilink")、2003年～2011年）

  - [終焉之笑](../Page/終焉之笑.md "wikilink")、全1冊、原名《》、([週刊少年Sunday](../Page/週刊少年Sunday.md "wikilink")、2012年11月21日-2012年12月19日）

  - 、11冊待續、原名《BIRDMEN》、 (2013年7月～連載中)

  - 魔畫師（田邊伊衛郎短篇集）、全1冊

      -
        收錄作品：
          - 黑暗中、《原名》、2000年新人漫畫獎佳作
          - LOST PRINCESS（2002年）
          - 結界師、短篇版刊載於增刊號
          - 魔畫師、增刊號刊載作品
          - 生日、GESSAN刊載作品

## 師匠

  - [安達充](../Page/安達充.md "wikilink")

  -
  - [雷句誠](../Page/雷句誠.md "wikilink")

## 助手

  - [四位晴果](../Page/四位晴果.md "wikilink")

  -
## 外部連結

  -
  -
  -
[Category:生年不詳](../Category/生年不詳.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:日本女性漫畫家](../Category/日本女性漫畫家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")