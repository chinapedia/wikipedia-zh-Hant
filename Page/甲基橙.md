**甲基橙**是一种常用的[酸碱指示剂或](../Page/酸碱指示剂.md "wikilink")[pH指示剂](../Page/pH指示剂.md "wikilink")。

## 物理性质

  - 橙黄色粉末或鱼鳞状[晶体有时顯現红色](../Page/晶体.md "wikilink")。
  - 微溶于[水](../Page/水.md "wikilink")，较易溶于热水，不溶于[乙醇等](../Page/乙醇.md "wikilink")[有机溶剂](../Page/有机溶剂.md "wikilink")。
  - 又名做**金莲橙D**、（酸性）**Ⅲ号橙**。
  - 甲基橙（methyl
    orange）本身为[酸性](../Page/酸性.md "wikilink")，变色范围介於[pH值](../Page/pH值.md "wikilink")3.1\~4.4。
  - pH\<3.1时变红，pH\>4.4时变黄，pH在3.1\~4.4时呈橙色。

## 指示剂颜色

[Methyl_orange_02035.JPG](https://zh.wikipedia.org/wiki/File:Methyl_orange_02035.JPG "fig:Methyl_orange_02035.JPG")

## 另见

  - [甲基红](../Page/甲基红.md "wikilink")
  - [甲基黄](../Page/甲基黄.md "wikilink")
  - [甲基绿](../Page/甲基绿.md "wikilink")
  - [甲基蓝](../Page/甲基蓝.md "wikilink")
  - [甲基紫](../Page/甲基紫.md "wikilink")

## 参考资料

[Category:酸碱指示剂](../Category/酸碱指示剂.md "wikilink")
[Category:偶氮染料](../Category/偶氮染料.md "wikilink")