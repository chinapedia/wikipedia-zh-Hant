**甲碸黴素**，又名**硫黴素**、**甲碸氯黴素**或**賽芬妥黴素**，是一種[酰胺醇类抗生素](../Page/酰胺醇类抗生素.md "wikilink")。它是[氯黴素的](../Page/氯黴素.md "wikilink")[甲基](../Page/甲基.md "wikilink")[磺醘基相似體](../Page/磺醘基.md "wikilink")，有著相似的反應，强度却小2.5－5倍。與氯黴素相似，甲碸黴素不溶於水，但卻极易溶於[脂類](../Page/脂類.md "wikilink")。与氯黴素相比，甲碸黴素的主要优点是不會引起[再生不良性貧血](../Page/再生不良性貧血.md "wikilink")。

## 应用

许多國家主要将甲碸黴素用于[獸药](../Page/獸药.md "wikilink")，但在[中國及](../Page/中國.md "wikilink")[意大利该](../Page/意大利.md "wikilink")[抗生素还可以用于](../Page/抗生素.md "wikilink")[人類](../Page/人類.md "wikilink")[疾病的治疗](../Page/疾病.md "wikilink")。在[巴西](../Page/巴西.md "wikilink")，甲碸黴素的应用也十分广泛，特别是用于治疗[性传播疾病及](../Page/性传播疾病.md "wikilink")[盆腔炎](../Page/盆腔炎.md "wikilink")。\[1\]

## 参考文献

## 外部連結

  - [Overview at World Health Organization - Food and Agriculture
    Organization](http://www.fao.org/docrep/W4601E/w4601e0d.htm)

  -
  -
[Category:砜](../Category/砜.md "wikilink")
[Category:酰胺醇类](../Category/酰胺醇类.md "wikilink")
[Category:苯乙胺类](../Category/苯乙胺类.md "wikilink")
[Category:二醇](../Category/二醇.md "wikilink")
[Category:乙酰胺](../Category/乙酰胺.md "wikilink")
[Category:有机氯化合物](../Category/有机氯化合物.md "wikilink")

1.