**甲酰胺**是[甲酸衍生出的](../Page/甲酸.md "wikilink")[酰胺](../Page/酰胺.md "wikilink")，分子式为HCONH<sub>2</sub>。它是无色液体，与水混溶，有与[氨类似的气味](../Page/氨.md "wikilink")。主要用于生产[磺胺类药物](../Page/磺胺.md "wikilink")，合成[维生素及用作纸张和纤维的](../Page/维生素.md "wikilink")[软化剂](../Page/软化剂.md "wikilink")。纯的甲酰胺可以溶解许多不溶于水的[离子化合物](../Page/离子化合物.md "wikilink")，因此也被用作[溶剂](../Page/溶剂.md "wikilink")。

## 用途

  - 甲酰胺是[玻璃化冷冻液的组分之一](../Page/玻璃化.md "wikilink")，被用于超低温保存组织和器官。
  - 甲酰胺被用作凝胶电泳中RNA的稳定剂，也用于稳定[毛细管电泳中的变性单股DNA](../Page/毛细管电泳.md "wikilink")。
  - [烧结时可在](../Page/烧结.md "wikilink")[溶胶凝胶中加入甲酰胺以防止开裂](../Page/溶胶凝胶.md "wikilink")。
  - 纯的甲酰胺可用作[纳米薄膜聚合物静电](../Page/纳米薄膜.md "wikilink")[自组装中的替代溶剂](../Page/自组装.md "wikilink")。\[1\]

## 生产

实验室和过去的工业上，都先以[甲酸和](../Page/甲酸.md "wikilink")[氨反应生成](../Page/氨.md "wikilink")[甲酸铵](../Page/甲酸铵.md "wikilink")，再使其热分解来制备甲酰胺：

  -
    [HCOOH](../Page/甲酸.md "wikilink") +
    [NH<sub>3</sub>](../Page/氨.md "wikilink") →
    [HCOONH<sub>4</sub>](../Page/甲酸铵.md "wikilink")
    HCOONH<sub>4</sub> → HCONH<sub>2</sub> +
    [H<sub>2</sub>O](../Page/水分子.md "wikilink")

现在工业上以[甲酸甲酯的](../Page/甲酸甲酯.md "wikilink")[氨解反应来制取甲酰胺](../Page/氨解反应.md "wikilink")：

  -
    HCOOCH<sub>3</sub> + [NH<sub>3</sub>](../Page/氨.md "wikilink") →
    HCONH<sub>2</sub> + [CH<sub>3</sub>OH](../Page/甲醇.md "wikilink")

## 参见

  - [*N*,*N*-二甲基甲酰胺](../Page/N,N-二甲基甲酰胺.md "wikilink")

## 参考资料

<references/>

[Category:酰胺](../Category/酰胺.md "wikilink")
[Category:酰胺溶剂](../Category/酰胺溶剂.md "wikilink")
[Category:一碳有机物](../Category/一碳有机物.md "wikilink")

1.