[Positronium.svg](https://zh.wikipedia.org/wiki/File:Positronium.svg "fig:Positronium.svg")和[正電子正繞着它們共同](../Page/正電子.md "wikilink")[質心的軌道](../Page/質心.md "wikilink")。這一個束縛量子態稱為**電子偶素**。.\]\]

**电子偶素**或稱**正電子素**（）是一个[电子与一个](../Page/电子.md "wikilink")[正电子组成的](../Page/正电子.md "wikilink")的束缚态(e<sup>+</sup>e<sup>-</sup>)，化学符号是Ps。最早由[麻省理工学院物理学家](../Page/麻省理工学院.md "wikilink")在1951年发现。

由于电子和正电子最终会[湮灭产生](../Page/湮灭.md "wikilink")[光子](../Page/光子.md "wikilink")，电子偶素的[半衰期是很短的](../Page/半衰期.md "wikilink")。根据电子与正电子[自旋状态的不同](../Page/自旋.md "wikilink")，电子偶素主要分为两种。[单态](../Page/单态.md "wikilink")（<sup>1</sup>S<sub>0</sub>，自旋相反，总自旋为0）即仲电子偶素（para-positronium，简记为p-Ps），三重态（<sup>3</sup>S<sub>1</sub>，自旋同向,总自旋为1）即正电子偶素（ortho-positronium，简记为o-Ps）。在[真空中](../Page/真空.md "wikilink")，单态的电子偶素半衰期为125[ps](../Page/数量级_\(时间\)#皮秒（ps）：10-12秒.md "wikilink")，之后湮灭产生两个光子（511[keV](../Page/电子伏特.md "wikilink")）；[三重态电子偶素半衰期为](../Page/三重态.md "wikilink")142[ns](../Page/数量级_\(时间\)#纳秒（ns）：10-9秒.md "wikilink")，湮灭产生三个光子，有时会产生多个光子。光子总能量为1022keV，即电子和正电子的总质量。

在介质中，三重态电子偶素的半衰期会相应变化，这就给人们提供了研究物质性质的一种手段。利用正电子或电子偶素研究物质内部性质已经成为一个应用非常广泛的学科。它主要利用电子偶素在介质中的湮灭时间谱图（）的拟合为测量手段，而且是一种非破坏性的方法。

## 奇异化合物

正电子的分子键合已经被预测\[1\]。
[氫化電子偶素](../Page/氫化電子偶素.md "wikilink")（PsH）分子已经可以被制备\[2\]。
电子偶素也可形成氰化物，并可与卤素或锂形成键\[3\]。

2007年9月12日，[加利福尼亞大學河濱分校的David](../Page/加利福尼亞大學河濱分校.md "wikilink")
Cassidy和Allen Mills报道了第一次观察到[雙電子偶素分子](../Page/雙電子偶素.md "wikilink") -
由两个电子偶素原子组成的分子\[4\]\[5\] 。

## 参阅

  - [雙電子偶素](../Page/雙電子偶素.md "wikilink")
  - [量子電動力學](../Page/量子電動力學.md "wikilink")
  - [奇異原子](../Page/奇異原子.md "wikilink")
  - [正电子](../Page/正电子.md "wikilink")

## 参考资料

[D](../Category/奇異原子.md "wikilink")
[D](../Category/量子电动力学.md "wikilink")
[\*](../Category/电子偶素.md "wikilink")
[D](../Category/自旋电子学.md "wikilink")

1.
2.
3.
4.
5.