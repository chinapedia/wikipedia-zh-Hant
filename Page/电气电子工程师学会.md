[3_Park_Avenue.JPG](https://zh.wikipedia.org/wiki/File:3_Park_Avenue.JPG "fig:3_Park_Avenue.JPG")

**電機電子工程師學會**（，簡稱為**IEEE**，英文读作“”\[ai trɪpl
i:\]）是一个建立於1963年1月1日的国际性[电子技术与](../Page/电子技术.md "wikilink")[电子工程师协会](../Page/电子工程.md "wikilink")，亦是世界上最大的专业技术组织之一，擁有來自175個國家的42萬會員。

除設立於[美國](../Page/美國.md "wikilink")[紐約市的總部以外](../Page/紐約市.md "wikilink")，亦在全球150多個國家擁有分會，並且還有35個專業學會及2個聯合會。其每年均會發表多種雜誌、學報、書籍，亦舉辦至少300次的專業會議。

目前IEEE在工業界所定義的標準有著極大的影響。

## 概述

IEEE定位在「科学和教育，并直接面向[电子电气工程](../Page/电子电气工程.md "wikilink")、[通讯](../Page/通讯.md "wikilink")、[计算机工程](../Page/计算机工程.md "wikilink")、[计算机科学理论和原理研究的组织](../Page/计算机科学.md "wikilink")，以及相关工程分支的[艺术和](../Page/艺术.md "wikilink")[科学](../Page/科学.md "wikilink")」。为了实现这一目标，IEEE承担着多个[科学期刊和](../Page/科学期刊.md "wikilink")[会议组织者的角色](../Page/会议.md "wikilink")。它也是一个广泛的[工业标准开发者](../Page/标准组织.md "wikilink")，主要领域包括[电能](../Page/电能.md "wikilink")、[能源](../Page/能源.md "wikilink")、[生物技术和保健](../Page/健康学.md "wikilink")、[信息技术](../Page/信息技术.md "wikilink")、[信息安全](../Page/信息安全.md "wikilink")、通讯、消费电子、运输、航天技术和[纳米技术](../Page/纳米技术.md "wikilink")。在教育领域IEEE积极发展和参与，例如在高等院校推行电子工程课程的[学校授权体制](../Page/学校授权.md "wikilink")。

IEEE制定了全世界电子和电气还有计算机科学领域30%的文献，另外它还制定了超过900个现行工业标准。每年它还发起或者合作举办超过300次国际技术会议。IEEE由37个协会组成，还组织了相关的专门技术领域，每年本地组织有规律的召开超过300次会议。IEEE出版广泛的同级评审期刊，是主要的国际标准机构（900现行标准，700研发中标准）。

IEEE大多数成员是电子工程师，计算机工程师和计算机科学家，不过因为组织广泛的兴趣也吸引了其它学科的工程师（例如：[机械工程](../Page/机械工程.md "wikilink")、[土木工程](../Page/土木工程.md "wikilink")、[生物](../Page/生物.md "wikilink")、[物理和](../Page/物理.md "wikilink")[数学](../Page/数学.md "wikilink")）。

IEEE坐落于美国[纽约州](../Page/纽约州.md "wikilink")，1963年由（IRE，创立于1912年）和[美国电气工程师协会](../Page/美国电气工程师协会.md "wikilink")（AIEE，创建于1884年）合并而成，它有一个区域和技术互为补充的组织结构，以地理位置或者技术中心作为组织单位（例如IEEE
[费城分会和IEEE计算机协会](../Page/费城.md "wikilink")）。它管理着推荐规则和执行计划的分散组织（例如IEEE-USA明确服务于美国的成员、专业人士和公众）。

## 著名主席和當時的名稱

  - （AIEE, 1889年-1890年）

  - [电话发明者](../Page/电话.md "wikilink")[亚历山大·贝尔](../Page/亚历山大·贝尔.md "wikilink")（AIEE,
    1891年-1892年）

  - （AIEE, 1901年-1902年）

  - [歐文·朗繆爾](../Page/歐文·朗繆爾.md "wikilink")（IRE, 1923年）

  - [李·德富雷斯特](../Page/李·德富雷斯特.md "wikilink")（IRE, 1930年）

  - 「矽谷之父」[弗雷德里克·特曼](../Page/弗雷德里克·特曼.md "wikilink")（IRE, 1941年）

  - [惠普创始人](../Page/惠普.md "wikilink")[威廉·休利特](../Page/威廉·休利特.md "wikilink")（IRE,
    1954年）

  - （IRE, 1959年; IEEE, 1963年）

  - （IEEE, 1978年）

## 历史

著重的主要是有线通讯（[电报和](../Page/电报.md "wikilink")[电话](../Page/电话.md "wikilink")），照明和电力系统。关心的大多是[无线电工程](../Page/无线电.md "wikilink")，它由2个更小的组织组成，无线和电报工程师协会和无线电协会。随着1930年代[电子学的兴起](../Page/电子学.md "wikilink")，电气工程大抵上也成了IRE的成员，但是电子管技术的应用变得如此广泛以至于IRE和AIEE领域边界变得越来越模糊。二战以后，两个组织竞争日益加剧，1961年两个组织的领导人果断决定将二者合并，终于1963年1月1日合并成立IEEE。

2012年，哥本哈根大學助教Radu
Dragusin發現IEEE的網站將網站日誌檔案及緩衝檔案的資料夾設為公開FTP，導致99979名會員的帳號密碼及其活動記錄外洩，約為全體會員數目41.1萬的24%。\[1\]

## 著名委员会和格式

  - [IEEE
    754](../Page/IEEE_754.md "wikilink")──[浮点算法规范](../Page/浮点.md "wikilink")

  - [IEEE
    802](../Page/IEEE_802.md "wikilink")──[局域网及](../Page/局域网.md "wikilink")[城域网](../Page/城域网.md "wikilink")

  - [IEEE 802.11](../Page/IEEE_802.11.md "wikilink")──无线网络

  - [IEEE 802.16](../Page/IEEE_802.16.md "wikilink")──無線寛頻網路

  - [IEEE 829](../Page/IEEE_829.md "wikilink")──软件测试文书

  - ──未来总线Futurebus

  - [IEEE
    1003](../Page/IEEE_1003.md "wikilink")──[POSIX](../Page/POSIX.md "wikilink")

  - [IEEE
    1076](../Page/VHDL.md "wikilink")──[VHDL](../Page/VHDL.md "wikilink")（[VHSIC](../Page/VHSIC.md "wikilink")[硬件描述语言](../Page/硬件描述语言.md "wikilink")）

  - [IEEE
    1149.1](../Page/JTAG.md "wikilink")──[JTAG](../Page/JTAG.md "wikilink")

  - ──Open Firmware

  - [IEEE 1284](../Page/並列埠.md "wikilink")──[-{zh-hant: 並列埠; zh-hans:
    并口}-](../Page/并口.md "wikilink")

  - [IEEE
    P1363](../Page/公钥密码.md "wikilink")──[公钥密码](../Page/公钥密码.md "wikilink")

  - [IEEE
    1364](../Page/IEEE_1364.md "wikilink")——[Verilog硬件描述语言](../Page/Verilog.md "wikilink")

  - [IEEE
    1394](../Page/IEEE_1394.md "wikilink")──串行总线「[火线](../Page/IEEE_1394.md "wikilink")」

  - [IEEE 1619](../Page/IEEE_1619.md "wikilink")──存储安全

  - ──PLC

  - ──軟件生命週期過程（IT）

## 常见标准

  - [IEEE 802.1](../Page/IEEE_802.1.md "wikilink")──高级接口High Level
    Interface(Internetworking)
      - [IEEE
        802.1d](../Page/IEEE_802.1d.md "wikilink")──[生成树协议](../Page/生成树协议.md "wikilink")
      - [IEEE 802.1p](../Page/IEEE_802.1p.md "wikilink")──General
        Registration Protocol
      - [IEEE
        802.1q](../Page/IEEE_802.1q.md "wikilink")──[虚拟局域网](../Page/虚拟局域网.md "wikilink")
          - [IEEE
            802.1ad](../Page/IEEE_802.1ad.md "wikilink")──[虛擬區域網路堆疊](../Page/虛擬區域網路堆疊.md "wikilink")（Vlan
            Stackiing；QinQ）
      - [IEEE 802.1x](../Page/IEEE_802.1x.md "wikilink")──基于端口的访问控制（Port
        Based Network Access Control）
  - [IEEE
    802.2](../Page/LLC.md "wikilink")──[逻辑链路控制](../Page/逻辑链路控制.md "wikilink")
  - [IEEE
    802.3](../Page/CSMA/CD.md "wikilink")──带冲突检测的载波侦听多路访问协议CSMA/CD（半雙工乙太網）
      - [IEEE
        802.3u](../Page/IEEE_802.3u.md "wikilink")──[快速乙太網](../Page/乙太網#快速乙太網\(100Mbps\).md "wikilink")
      - [IEEE
        802.3z](../Page/IEEE_802.3z.md "wikilink")──[千兆乙太網](../Page/千兆乙太網.md "wikilink")
      - [IEEE
        802.3ae](../Page/IEEE_802.3ae.md "wikilink")──[万兆乙太網](../Page/乙太網#万兆乙太網.md "wikilink")

<!-- end list -->

  - [IEEE
    802.4](../Page/IEEE_802.4.md "wikilink")──令牌通行[总线](../Page/总线.md "wikilink")
  - [IEEE 802.5](../Page/IEEE_802.5.md "wikilink")──令牌通行环（Token-Passing
    Ring）
  - [IEEE
    802.6](../Page/IEEE_802.6.md "wikilink")──[城域网](../Page/城域网.md "wikilink")
  - [IEEE 802.7](../Page/IEEE_802.7.md "wikilink")──宽带局域网（Brandband LAN）
  - [IEEE 802.8](../Page/IEEE_802.8.md "wikilink")──光纤局域网
  - [IEEE
    802.9](../Page/IEEE_802.9.md "wikilink")──[集成数据和语音网络](../Page/IP電話.md "wikilink")
      - [IEEE
        802.9a](../Page/IEEE_802.9a.md "wikilink")──IsoENET（proposed）
  - [IEEE 802.10](../Page/IEEE_802.10.md "wikilink")──网络安全（Network
    Security）
  - [IEEE
    802.11](../Page/IEEE_802.11.md "wikilink")──[無線區域網路](../Page/無線區域網路.md "wikilink")
  - [IEEE 802.12](../Page/IEEE_802.12.md "wikilink")──100VG-AnyLAN（Voice
    Grade - Sprache geeignet）
  - [IEEE
    802.14](../Page/IEEE_802.14.md "wikilink")──[有线电视](../Page/有线电视.md "wikilink")
  - [IEEE
    802.15](../Page/IEEE_802.15.md "wikilink")──[無線個人區域網路](../Page/無線個人區域網路.md "wikilink")
  - [IEEE
    802.16](../Page/IEEE_802.16.md "wikilink")──[無線寛頻網路](../Page/無線寛頻網路.md "wikilink")
  - [IEEE 802.17](../Page/IEEE_802.17.md "wikilink")──弹性分组环（Resilient
    Packet Ring）

## 所設獎項

  - [IEEE榮譽獎章](../Page/IEEE榮譽獎章.md "wikilink")

<!-- end list -->

  - [IEEE愛迪生獎章](../Page/IEEE愛迪生獎章.md "wikilink")
  - [IEEE西澤潤一獎](../Page/IEEE西澤潤一獎.md "wikilink")
  - [IEEE理察·衛斯里·漢明獎章](../Page/IEEE理察·衛斯里·漢明獎章.md "wikilink")

<!-- end list -->

  - [IEEE约翰·冯·诺依曼奖](../Page/IEEE约翰·冯·诺依曼奖.md "wikilink")
  - [貝爾獎](../Page/貝爾獎.md "wikilink")

## 参考文献

## 外部链接

  - [IEEE网站](http://www.ieee.org/)

      - [IEEE亞太區(Region 10)官方網站](http://ewh.ieee.org/reg/10/)

          - [IEEE中国官方网站](http://cn.ieee.org/)

          - [台北區IEEE官方網站](http://www.ieee.org.tw/)

          - [台南區IEEE官方網站](http://ewh.ieee.org/r10/tainan/)

          - [香港區IEEE官方網站](https://web.archive.org/web/20071027081739/http://www.ieee.org.hk/)

          - [澳門區IEEE官方網站](http://ieeemacau.eee.umac.mo/)

          - [IEEE北京分會](http://www.cie-china.org/index.asp)

          - [IEEE成都分會](https://web.archive.org/web/20080604154533/http://ewh.ieee.org/r10/chengdu/new/)

          - [東京區IEEE官方網站](http://www.ieee-jp.org/section/tokyo/index_e.htm)

  - [AIEE-IRE
    merger的历史](http://www.ieee.org/organizations/history_center/oral_histories/oh_merger_menu.html)

  - [IEEE标准下载](http://www.techstreet.com/info/ieee.html)

## 参見

  - [IEEE计算机协会](../Page/IEEE计算机协会.md "wikilink")

  - [IEEE微波理论和技术协会](../Page/IEEE微波理论和技术协会.md "wikilink")

  - ,IEE（在[英国的类似IEEE的组织](../Page/英国.md "wikilink")）

  - [工程倫理](../Page/工程倫理.md "wikilink")

{{-}}

[IEEE](../Category/IEEE.md "wikilink")
[Category:国际组织](../Category/国际组织.md "wikilink")
[Category:非营利组织](../Category/非营利组织.md "wikilink")
[Category:标准制订机构](../Category/标准制订机构.md "wikilink")
[Category:工程协会](../Category/工程协会.md "wikilink")
[Category:1963年建立的组织](../Category/1963年建立的组织.md "wikilink")

1.