[缩略图](https://zh.wikipedia.org/wiki/File:Recarregável.JPG "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:195201_1952年广州兴华电池厂.png "fig:缩略图")兴华电池厂\]\]
**电池**，一般狹義上的定義是將本身儲存的[化學能轉成](../Page/化學能.md "wikilink")[電能的裝置](../Page/電能.md "wikilink")，廣義的定義為將預先**儲存**起的能量**轉化**為可供外用電能的裝置\[1\]。因此，像太陽能電池只有轉化而無儲存功能的裝置不算是電池。其他名稱有**電瓶**、**電芯**，而中文「池」及「瓶」也有儲存作用之意。

英文中，單一個電池結構叫做「Cell」（單電池），內部有多個Cell並連或串連的結構叫做「Battery
Cell」（電池組）。市售一般乾電池其實構造上是「Cell」但英文上習慣稱「Battery」，汽車用[鉛酸電池與方形](../Page/鉛酸電池.md "wikilink")[9V電池則是真正的](../Page/9V電池.md "wikilink")「Battery」。

## 電池發展史

電池的[歷史可以追溯到兩千多年前的古](../Page/歷史.md "wikilink")[伊拉克](../Page/伊拉克.md "wikilink")[時代](../Page/時代.md "wikilink")。是在首都[巴格達發現的](../Page/巴格達.md "wikilink")[素燒](../Page/素燒.md "wikilink")[陶壺](../Page/陶壺.md "wikilink")（[巴格達電池](../Page/巴格達電池.md "wikilink")）它是一種使用[銅和](../Page/銅.md "wikilink")[鐵的電池](../Page/鐵.md "wikilink")。[考古者](../Page/考古.md "wikilink")[發現的物品被認為是至今發現的最早的電池](../Page/發現.md "wikilink")[證據](../Page/證據.md "wikilink")。
而[現代真正作為化學能的儲藏體](../Page/現代.md "wikilink")，根據人們的需要可控制地放出電能的[裝置](../Page/裝置.md "wikilink")，首先由[亞歷山德羅·伏特](../Page/亞歷山德羅·伏特.md "wikilink")[發明](../Page/發明.md "wikilink")，當時稱為Volta
Pile（[伏打電堆](../Page/伏打電堆.md "wikilink")）。

<File:Voltaic> pile battery.png|目前已知的全球第一個電池 <File:Volta>
batteries.jpg|[伏打電堆](../Page/伏打電堆.md "wikilink") BATERIA.JPG|車用鉛酸電池
CSIRO ScienceImage 8257 The Flexible Integrated Energy Device FIED
showing the flexible batteries.jpg|穿戴式鋰電池

## 電池種類

### 化學電池

**[化学电池](../Page/電化電池.md "wikilink")**、**電化電池**、**電化學電池**或**电化学池**是指通过[氧化还原反應](../Page/氧化还原反應.md "wikilink")，把[正极](../Page/正极.md "wikilink")、[负极](../Page/负极.md "wikilink")[活性物质的化学能](../Page/活性物质.md "wikilink")，转化为电能的一类装置。与普通氧化还原反应不同的是[氧化和](../Page/氧化.md "wikilink")[还原反应是分开进行的](../Page/还原.md "wikilink")，氧化在负极，还原在正极，而[电子得失是通过外部](../Page/电子.md "wikilink")[线路进行的](../Page/线路.md "wikilink")，所以形成了电流。这是所有电池的本质特点。经过长期的研究、发展，化学电池迎来了品种繁多，应用广泛的局面。大到一座[建筑方能](../Page/建筑.md "wikilink")[容纳得下的巨大装置](../Page/容纳.md "wikilink")，小到以毫米计的類型。现代[电子技术的发展](../Page/电子技术.md "wikilink")，对化学电池提出了很高的要求。每一次化学电池技术的突破，都带来了[电子设备](../Page/电子设备.md "wikilink")[革命性的发展](../Page/革命.md "wikilink")。世界上很多电化学[科学家](../Page/科学家.md "wikilink")，把兴趣集中在做为[电动汽车动力的化学电池领域](../Page/电动汽车动力.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:Fotothek_df_n-10_0000553.jpg "fig:缩略图")\]\]

#### 乾電池和液體電池

[乾電池和](../Page/乾電池.md "wikilink")[液体电池的区分仅限于早期电池发展的那段](../Page/液体电池.md "wikilink")[时期](../Page/时期.md "wikilink")。最早的电池由装满[电解液的](../Page/电解液.md "wikilink")[玻璃容器和两个电极组成](../Page/玻璃.md "wikilink")。后来推出了以糊状[电解液为基础的电池](../Page/电解液.md "wikilink")，也称做乾電池。

现在仍然有“[液体](../Page/液体.md "wikilink")”电池。一般是[体积非常庞大的品种](../Page/体积.md "wikilink")。如那些做为[不间断电源的大型固定型](../Page/不间断电源.md "wikilink")[鉛酸蓄電池或與](../Page/鉛酸蓄電池.md "wikilink")[太阳能電池配套使用的铅酸蓄电池](../Page/太阳能電池.md "wikilink")。对于[移动设备](../Page/移动设备.md "wikilink")，有些使用的是全[密封](../Page/密封.md "wikilink")，免维护的[铅酸蓄电池](../Page/铅酸蓄电池.md "wikilink")，这类电池已经成功使用了许多年，其中的电解液[硫酸是由](../Page/硫酸.md "wikilink")[硅凝胶固定](../Page/硅凝胶.md "wikilink")。

#### 一次性電池和可充電電池

  - 一次性電池

[一次性電池](../Page/原電池.md "wikilink")(Primary
Battery)俗稱「用完即棄」電池及原電池，因為它們的[電量耗盡後](../Page/電量.md "wikilink")，無法再充電使用，只能丟棄。常見的一次性電池包括：
\*
[鋅錳電池](../Page/鋅錳電池.md "wikilink")—電壓約1.5V，電池容量較低，能輸出的電流也較低，幾乎被鹼錳電池所取代，唯獨是不會在長期存放後漏出有害腐蝕液體，所以仍被使用於低用電量同時需長期使用的裝置，例如鐘、紅外線搖控等。

  - [鹼錳電池](../Page/鹼錳電池.md "wikilink")—電壓約1.5V，電池容量及輸出的電池較鋅錳電池高，但不及[鎳氫電池](../Page/鎳氫電池.md "wikilink")，長期存放後漏出有害腐蝕液體。
  - [鋰電池Li](../Page/鋰電池.md "wikilink")—電壓約3V，電池容量及輸出的電池極高，可以存放十年仍有相當電力，但價錢較貴。

其他的一次性电池包括有[鋅電池](../Page/鋅電池.md "wikilink")、[鋅空電池](../Page/鋅空電池.md "wikilink")、[鋅汞電池](../Page/鋅汞電池.md "wikilink")、[水銀電池](../Page/水銀電池.md "wikilink")、[氫氧電池和](../Page/氫氧電池.md "wikilink")[鎂錳電池](../Page/鎂錳電池.md "wikilink")。

  - 可充電電池

[可充電電池又稱二次電池](../Page/蓄電池.md "wikilink")(Secondary
Battery)或二級電池、[蓄電池](../Page/蓄電池.md "wikilink")。可充電電池按製作[材料和](../Page/材料.md "wikilink")[工藝上的不同](../Page/工藝.md "wikilink")，其優點是在充電後可多次循環使用，它們可全充[放電兩百多次甚至達](../Page/放電.md "wikilink")2500次，充電電池的輸出電流[負荷力要比大部分一次性電池高](../Page/負荷力.md "wikilink")。常見的類型有：
\*
[鉛酸電池](../Page/鉛酸蓄電池.md "wikilink")—每個Cell的電壓約2V，容量低但可輸出較大的功率、電池，常使用於汽車中作啟動引擎用，或用於不斷電系統(UPS)、無線電機、通信機。

  - [鎳鎘電池NiCd](../Page/鎳鎘電池.md "wikilink")—電壓約1.2V，有較強烈的[記憶效應](../Page/記憶效應_\(電池\).md "wikilink")，而且容量較低，含有毒物質，對環境有害，現已被淘汰。
  - [鎳氫電池NiMH](../Page/鎳氫電池.md "wikilink")—電壓約1.2V，有極輕微的記憶效應，容量較鎳鎘電池及鹼性電池大，可充放電循環使用數百至二千幾次。舊鎳氫電池有較大的自放電，新的低自放電鎳氫電池自放電低至與鹼性電池相約，而且可在低溫下使用(-20℃)，充電裝置、電壓與鎳鎘電池相同，已取代了鎳鎘電池，同時也可取代絕大部份鹼性電池的用途，也有用於[混合動力車的](../Page/混合動力車.md "wikilink")。
  - [鋰離子電池Li](../Page/鋰離子電池.md "wikilink")-ion—電壓約3.6V、3.7V，鋰離子電池具有重量輕（容量是同重量的鎳氫電池的1.5倍\~2倍）、容量大、無記憶效應等優點，具有很低的自放電率，因而即使價格相對較高，仍然得到了普遍應用，包括許多電子產品，而且不含有毒物質，但這類用於[消費性電子產品的Li](../Page/消費性電子產品.md "wikilink")-ion電池在存放一段時期後電量會永久減少。另也有用於[純電動車及](../Page/純電動車.md "wikilink")[混合動力車的Li](../Page/混合動力車.md "wikilink")-ion電池，用於這用途的鋰離子電池容量相對略低，但有較大的輸出、充電電流，也有的有較長的壽命，但成本較高。

#### 燃料電池

[Microbial_electrolysis_cell.png](https://zh.wikipedia.org/wiki/File:Microbial_electrolysis_cell.png "fig:Microbial_electrolysis_cell.png")
[燃料電池是一種将](../Page/燃料電池.md "wikilink")[燃料的化學能透過電化學反應直接轉化成電能的裝置](../Page/燃料.md "wikilink")，與一般電化電池不同的是反應物不儲存在電池內，只在反應時灌入電池。

燃料電池是利用[氫氣在陽極進行氧化反應](../Page/氫氣.md "wikilink")，將氫氣氧化成[氫離子](../Page/氫離子.md "wikilink")，而[氧氣在陰極進行還原反應](../Page/氧氣.md "wikilink")，與由陽極傳來的氫離子結合生成水。氧化還原反應過程中就可以產生電流。

燃料電池的技術包括了出現[鹼性燃料電池](../Page/鹼性燃料電池.md "wikilink")(AFC)、[磷酸燃料電池](../Page/磷酸燃料電池.md "wikilink")(PAFC)、[質子交換膜燃料電池](../Page/質子交換膜燃料電池.md "wikilink")(PEMFC)、[熔融碳酸鹽燃料電池](../Page/熔融碳酸鹽燃料電池.md "wikilink")(MCFC)、[固態氧化物燃料電池](../Page/固態氧化物燃料電池.md "wikilink")(SOFC)，以及[直接甲醇燃料電池](../Page/直接甲醇燃料電池.md "wikilink")(DMFC)等，而其中，利用[甲醇氧化反應作為正極反應的燃料電池技術](../Page/甲醇.md "wikilink")，更是被業界所看好而積極發展。

#### 水啟動電池

本身不含電解質，出廠時還不會產生電力，須待加入水之後，才讓成份間的化學反應得以開始，並產生電力。

### 核電池

[Plutonium_pellet.jpg](https://zh.wikipedia.org/wiki/File:Plutonium_pellet.jpg "fig:Plutonium_pellet.jpg")238加热至烧红的程度。\]\]
**[原子能电池](../Page/核電池.md "wikilink")**（又称**核电池**，氚电池或放射性同位素發电装置）是指使用[放射性](../Page/放射性.md "wikilink")[同位素](../Page/同位素.md "wikilink")[衰变时产生之能量来产生电力的装置](../Page/衰变.md "wikilink")。这会使人误解成[核反应堆](../Page/核反应堆.md "wikilink")，但实际上这种电池不是利用[链式反应来产生能量](../Page/链式反应.md "wikilink")。核电池比起一般**电池**有很长的寿命且其输出能量远比一般化学电池为高，可惜其制作成本也相对很高，使这种电池多用于一些需长时间运作又难以更换电池的仪器之上。
[缩略图](https://zh.wikipedia.org/wiki/File:BlackTerror2452.jpg "fig:缩略图")
核电池之技术早在1913年已经被[亨利·莫塞莱所发明](../Page/亨利·莫塞莱.md "wikilink")，使众科学家都期望此技术能够用于太空仪器上。但由于一直无法提高能源效率，这技术到近年纳米技术研发出更有效之[半导体后再被关注](../Page/半导体.md "wikilink")。此種電池現應用於衛星、宇航工具，也有於地面上使用。第一個離開[太陽系的太空探測器](../Page/太陽系.md "wikilink")[航行者一號就是用這類電池作為能源](../Page/航行者一號.md "wikilink")，供應電力給探測器上的儀器。
核电池大致分成两种类，分别是[热转换型核电池](../Page/热转换型核电池.md "wikilink")（例如[放射性同位素熱電機](../Page/放射性同位素熱電機.md "wikilink")、[斯特林放射性同位素高階熱電機](../Page/斯特林放射性同位素高階熱電機.md "wikilink")）及[非热转换型核电池](../Page/非热转换型核电池.md "wikilink")。

## 電池容量

電池容量是指電池所能儲存的[電荷量](../Page/電荷.md "wikilink")，電池容量的符號為Q，單位為[庫倫](../Page/庫倫.md "wikilink")（C），但日常生活中多以[安培小時](../Page/安培小時.md "wikilink")（Ah）為單位，由於日常生活使用的電池也有容量相對較少，所以也有用[毫安培小時](../Page/毫安培小時.md "wikilink")（mAh）單位，也即千分之一[安培小時](../Page/安培小時.md "wikilink")，例如[手機所使用的電池通常以後者為標記](../Page/手機.md "wikilink")。

決定電池容量的因素有： 電池的種類（也即制造電池的物質）：同一體積，不同種類的電池有不同的容量，例如鋰電池的容量較很多其他電池為高。
電池的體積：由於物質的化學能的[能量密度是固定的](../Page/能量密度.md "wikilink")，因此體積越大，總藏能量就越多，例如一枚AA電池的容量比AAA電池為大。
電池的溫度：一般情況下，溫度越低，電池的有效容量會減小，不同種類的電池減小的程度各有不同，所以在寒冷地區使用電池時需要特別留意。
放電速率：放電電流越大，同一電池的有效容量會越小，所以推高耗電的電器時電池的容量會減少，例如一枚能點亮2W燈泡一小時的電池，推動4W燈泡時就不能有半小時，必定比半小時短些，短多少就視乎電池種類、溫度…等因素而定。

所以同一枚電池在不同環境下會有不同容量，所以一般電池所標示的容量只可作參考，實際使用仍會因環境及其他工作條件而有所變化。而標示容量一般都以[室溫情況下作準](../Page/室溫.md "wikilink")。

### 容量、放電電流與C值

要計算一電池的在某一放電流下能連續放電多久，方法是將容量除以電流：

\(t = \frac{Q}{I}\)

當中，t（h）是放電時間（單位是小時）、Q（Ah）是電池容量（單位是安倍小時）、I（A）是電流（安培）。

而放電（或充電）電流也有用C值來表述，1個C的放電電流會剛好在一小時把電池完全放電，也就是1個C的電流是相對電池容量而定。例如一枚600mAh的電池，1個C的電流即是600mA，以這個電流放電會在一小時內用完電量。同樣地，對一枚2500mAh的電池，1C就是2500mA。又例如以0.5C對電池放電的話，不管電池容量多小，電池都會在2小時用完（1/0.5=2）。

用C作表述的特點在於在相同電池種類、操作環境下，不同容量的電池在同一C值放電率下，理論上都應該有相約放電時間。所以當要比較不同電池性能時，會選擇同一C值的放電或充電速率作比較。電池生產商在電池的規格上也多以C值表述放電電流及充電電流的速率。

## 电池型号

常见的电池型号如下表：\[2\]\[3\]

<table>
<thead>
<tr class="header">
<th><p>美国型号</p></th>
<th><p>中国大陆型号</p></th>
<th><p>臺灣型號</p></th>
<th><p>尺寸（高度×直径）,mm</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>7号电池</p></td>
<td><p>4號電池</p></td>
<td><p>44.0*10.0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/AA電池.md" title="wikilink">AA</a></p></td>
<td><p>5号电池</p></td>
<td><p>3號電池</p></td>
<td><p>49.0*14.0</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2号电池</p></td>
<td><p>2號電池</p></td>
<td><p>49.5*25.3</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1号电池</p></td>
<td><p>1號電池</p></td>
<td><p>59.0*32.3</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>9号电池</p></td>
<td><p>6號電池</p></td>
<td><p>41.5*8.1</p></td>
</tr>
<tr class="even">
<td><p>A</p></td>
<td><p>4号电池</p></td>
<td><p>| 49.0*16.8</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SC</p></td>
<td><p>3号电池</p></td>
<td><p>| 42.0*22.1</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>N</p></td>
<td><p>8号电池</p></td>
<td><p>5號電池</p></td>
<td><p>28.5*11.7</p></td>
</tr>
<tr class="odd">
<td><p>F</p></td>
<td></td>
<td><p>| 89.0*32.3</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  -
  -
  - [鉛酸電池](../Page/鉛酸電池.md "wikilink")

  - [鎳鎘電池](../Page/鎳鎘電池.md "wikilink")(NiCd)

  - [鎳氫電池](../Page/鎳氫電池.md "wikilink")(Ni-MH)

  - [鋰離子電池](../Page/鋰離子電池.md "wikilink")(Li-ion)

  - [燃料電池](../Page/燃料電池.md "wikilink")

  - [鋅錳電池](../Page/鋅錳電池.md "wikilink")

  - [鹼錳電池](../Page/鹼錳電池.md "wikilink")

  - [鋰電池](../Page/鋰電池.md "wikilink")

  - [水銀電池](../Page/水銀電池.md "wikilink")

  - [鋅汞電池](../Page/鋅汞電池.md "wikilink")

  - [纳米线电池](../Page/纳米线电池.md "wikilink")

  - [純電動車](../Page/純電動車.md "wikilink")

  - [混合動力車](../Page/混合動力車.md "wikilink")

  - [電量狀態](../Page/電量狀態.md "wikilink")

  - [涓流充電](../Page/涓流充電.md "wikilink")

  - [充電器](../Page/充電器.md "wikilink")

  - [鈕扣電池](../Page/鈕扣電池.md "wikilink")

## 參考資料

## 外部链接

  - [电池乱扔危害大，附上电池回收法](http://sinai.blog.163.com/blog/static/12071148820116322827929/)

[si:විදුලි කෝෂය
(විද්‍යුතය)](../Page/si:විදුලි_කෝෂය_\(විද්‍යුතය\).md "wikilink")

[电池](../Category/电池.md "wikilink")
[Category:意大利发明](../Category/意大利发明.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")

1.
2.  [常见电池型号、尺寸及用途介绍](http://www.360doc.com/content/13/1217/19/235269_337953672.shtml).
    2016-8-1
3.