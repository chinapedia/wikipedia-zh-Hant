历史上[个人电脑曾经使用过多种](../Page/个人电脑.md "wikilink")**电脑显示标准**或**显示方式**。显示标准通常是**显示分辨率**（由屏幕横向和纵向[像素数目来定义](../Page/像素.md "wikilink")）、颜色比特数和[屏幕刷新频率](../Page/屏幕刷新频率.md "wikilink")（单位[赫兹](../Page/赫兹.md "wikilink")）的一个组合。早期的显示适配器只是简单的帧缓冲区，新的显示标准还定义更详细的显示功能和软件控制接口。

早期，大多数电脑显示器的[纵横比是](../Page/纵横比_\(图像\).md "wikilink")4:3，也有一些是5:4。之后16:10、16:9的宽屏显示器相继成为主流，最近21:9等新的显示方式也已产生。

## 显示标准

个人电脑显示标准主要有：

  - [MDA](../Page/单色显示适配器.md "wikilink")
    单色显示适配器（MDA）。[IBM机上的最初标准](../Page/IBM机.md "wikilink")。最高支持720x350的[文本模式](../Page/文本模式.md "wikilink")。
  - [大力神图形卡](../Page/大力神图形卡.md "wikilink")（Herculus）
    一种单色显示。分辨率为720x348，可以显示清晰的文字和图形。在過去個人電腦的早期年代，由於Herculus能夠以較廉宜的價錢及較高的解像度（相對於CGA的640x200單色顯示、或EGA的640x350的16色顯示）顯示圖形，而且給予用戶較大的彈性去利用圖型卡，當時台灣的中文系統都以這顯示卡為設計標準，並把畫面重新調適為640x390，使電腦除了能夠保留有25個標準行於一般程序使用以外，還有額外多兩行供系統使用（主要是讓用戶選字及提供系統訊息）。
  - [彩色图形适配器](../Page/彩色图形适配器.md "wikilink")
    彩色图形适配器（CGA）。
      - 所謂“彩色顯示”，其實只限於文本模式。
      - 圖形模式不單止只有分辨率为320x240，而且每个像素只可以从分成4組的16色[调色板选择其中一組固定的](../Page/调色板.md "wikilink")4色。
      - 另外還有640x200，不過只有單色顯示。
      - 有非標準的16色160x100模式，但其實只是用文本模式來模擬。它把4個頁面的文字壓縮在一個頁面顯示，並且利用[CP437的方塊符號來表示色點](../Page/CP437.md "wikilink")。有不少的早期遊戲都是採用這種特殊的160x100x16模式顯示的。
  - [增强图形适配器](../Page/增强图形适配器.md "wikilink")
    增强图形适配器（EGA）。分辨率为640x350，每个像素可以有16种颜色（每个像素用4比特表示）。16种颜色可以从64色[调色板选择](../Page/调色板.md "wikilink")。
  - [QVGA](../Page/QVGA.md "wikilink")
    四分之一VGA (320x240)
  - [视频图形阵列](../Page/视频图形阵列.md "wikilink")
    视频图形阵列（VGA）实际上有几种不同分辨率。最常见的是640x480（每像素4比特，16种颜色可选）。还有320x200（每像素8比特，256种颜色可选）和720x400的[文本模式](../Page/文本模式.md "wikilink")。
  - [MCGA](../Page/多色图形适配器.md "wikilink")
    多色图形适配器（MCGA）。1987年某些[PS/2型号上出现](../Page/PS/2.md "wikilink")，比[VGA成本低](../Page/VGA.md "wikilink")。MCGA有可以在262144色[调色板中选](../Page/调色板.md "wikilink")256种颜色的彩色模式。或640x480的单色模式。相比之下VGA有256k显示存储器，MCGA则只有64k，因而单色模式的分辨率只能达到640x480，256色的彩色模式分辨率只能达到320x200。
  - [SVGA](../Page/SVGA.md "wikilink")
    高级视频图形阵列（Super
    VGA或SVGA)，由VESA为IBM兼容机推出的标准。分辨率为800x600（每像素4比特，16种颜色可选）。
  - [XGA](../Page/XGA.md "wikilink")
    扩展图形阵列（Extended Graphics
    Array或XGA）是[IBM在](../Page/IBM.md "wikilink")1990年推出的一个标准。XGA支持1024x768分辨率，256色调色板（每像素8比特），或者640x480分辨率（每像素16比特，或称高色）。XGA-2更进一步支持1024x768（高色，更高帧频），和1360x1024分辨率（每像素4比特，16种颜色可选）。
  - [SXGA](../Page/SXGA.md "wikilink")
    高级扩展图形阵列（Super
    XGA或SXGA）。一个分辨率为1280x1024的*[既成事实](../Page/既成事实.md "wikilink")*显示标准，每个像素用32比特表示（本色）。这种被广泛采用的显示标准的[纵横比是](../Page/纵横比.md "wikilink")5:4而不是常见的4:3。
      - 某些厂家，意识到*[既成事实](../Page/既成事实.md "wikilink")*的工业标准视频图形阵列，因而创造了扩展视频图形阵列（XVGA）这个名词。
      - [8514（显示标准）](../Page/8514（显示标准）.md "wikilink")
        在XGA之前，和VGA大概同时出现。8514/A显卡最大分辨率是1024x768，256种颜色（每个像素用8比特表示）。帧频为43.5赫兹。
  - [UXGA](../Page/UXGA.md "wikilink")
    超级扩展图形阵列（Ultra
    XGA或UXGA）是一种*[既成事实](../Page/既成事实.md "wikilink")*的标准。分辨率为1600x1200（每像素32比特，或称[本色](../Page/本色.md "wikilink")）。
  - [WXGA](../Page/WXGA.md "wikilink")
    宽屏扩展图形阵列（WXGA）是一种分辨率为1280x720、纵横比为16:9的XGA格式。这种显示纵横比在最近的笔记本电脑上变得流行起来。
  - [WSXGA / WXGA+](../Page/WXGA_Plus.md "wikilink")
    宽屏扩展图形阵列＋（WXGA+）是一种分辨率为1440x900、纵横比为16:10的WXGA格式。
  - [WUXGA](../Page/WUXGA.md "wikilink")
    宽屏超级扩展图形阵列（WUXGA）是一种分辨率为1920x1200、纵横比为16:10的UXGA格式。这种纵横比在高档15和17英寸笔记本电脑上越来越流行。
  - 此外还有更高的显示标准，例如[QXGA和](../Page/QXGA.md "wikilink")[HXGA](../Page/HXGA.md "wikilink")。按这种定义方式，分辨率最高的*[既成事实](../Page/既成事实.md "wikilink")*标准是分辨率为7680x4800的[WHUXGA](../Page/WHUXGA.md "wikilink")。
  - [SSWUXGA](../Page/SSWUXGA.md "wikilink")
    超高級宽屏超级扩展图形阵列（SSWUXGA）是一种分辨率为19200x12000、纵横比为16:10的UXGA格式。

## 参见

  - [电脑显示器](../Page/显示器.md "wikilink")
  - [显示分辨率](../Page/显示分辨率.md "wikilink")
  - [液晶显示器](../Page/液晶显示器.md "wikilink")
  - [等离子体显示器](../Page/等离子体显示器.md "wikilink")
  - [苹果电脑](../Page/苹果电脑.md "wikilink")
  - [IBM兼容机](../Page/IBM兼容机.md "wikilink")

## 外部链接

  - [计算机屏幕信息](https://web.archive.org/web/20050306043047/http://www.qg.fi/screeninformation.html)

[Category:电脑显示标准](../Category/电脑显示标准.md "wikilink")
[Category:顯示科技](../Category/顯示科技.md "wikilink")