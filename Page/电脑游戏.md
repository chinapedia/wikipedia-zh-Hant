[1981_Gulf_of_Sidra_incident._F-14_Fast_Eagle_107,_from_VF-41_about_to_shoot_down_a_Libyan_Su-22_with_an_AIM-9_Sidewinde.png](https://zh.wikipedia.org/wiki/File:1981_Gulf_of_Sidra_incident._F-14_Fast_Eagle_107,_from_VF-41_about_to_shoot_down_a_Libyan_Su-22_with_an_AIM-9_Sidewinde.png "fig:1981_Gulf_of_Sidra_incident._F-14_Fast_Eagle_107,_from_VF-41_about_to_shoot_down_a_Libyan_Su-22_with_an_AIM-9_Sidewinde.png")
[Daimonin_Stoneglow_night_indexedto256.png](https://zh.wikipedia.org/wiki/File:Daimonin_Stoneglow_night_indexedto256.png "fig:Daimonin_Stoneglow_night_indexedto256.png")

**电脑游戏**（，或稱，全寫）是一个相对于[主机游戏和](../Page/电视游戏.md "wikilink")[街机游戏的概念](../Page/街机游戏.md "wikilink")，指在[个人电脑上运行的](../Page/个人电脑.md "wikilink")[游戏软件](../Page/游戏软件.md "wikilink")，是一种本身提供[娱乐功能的](../Page/娱乐.md "wikilink")[电脑软件](../Page/电脑软件.md "wikilink")。电脑游戏产业与[电脑硬件](../Page/电脑硬件.md "wikilink")、[电影](../Page/电影.md "wikilink")、电脑软件、[互联网的发展联系甚密](../Page/互联网.md "wikilink")。电脑游戏为游戏参与者提供了一个[虚拟的空间](../Page/虚拟.md "wikilink")，从一定程度上让人可以摆脱[现实世界](../Page/现实世界.md "wikilink")，在另一个世界中扮演真实世界中扮演不了的[角色](../Page/角色.md "wikilink")。同時電腦多媒體技術的發展，使游戏给了人们很多体验和享受。

## 發展歷史

电脑游戏的出现与1960年代[電子計算機出現在](../Page/電子計算機.md "wikilink")[美国大学校园有密切的联系](../Page/美国.md "wikilink")。当年的环境培养出了一群编程的高手。在1962年，一位叫斯蒂夫·拉塞尔的大学生在美国[DEC公司生产的](../Page/DEC公司.md "wikilink")[PDP-1](../Page/PDP-1.md "wikilink")[型電子計算機上编製的电脑游戏就是在当时很有名的](../Page/型電子計算機.md "wikilink")《[宇宙战争](../Page/宇宙战争.md "wikilink")》（Space
War）。遊戲業界人士一般认为，斯蒂夫·拉塞尔就是[发明电脑游戏的人](../Page/发明.md "wikilink")。

1970年代，随着電子計算機技术日漸发展，其成本變得越来越低。在1971年，被誉为“电子游戏之父”的[诺兰·布什内尔发明了第一台商业化](../Page/诺兰·布什内尔.md "wikilink")[电子游戏机](../Page/电子游戏机.md "wikilink")。不久，他更创办了世上第一間电子游戏公司——[雅達利](../Page/雅達利.md "wikilink")。而在1970年代，随着[苹果电脑的面世](../Page/苹果电脑.md "wikilink")，电脑游戏才真正踏上了商业化的道路。這個时候，电脑游戏的图形效果还非常简陋，但是游戏的类型化已经开始漸漸出现了。

1980年代和90年代，随着电脑软硬件技术的进步，[個人電腦的使用越来越广泛](../Page/個人電腦.md "wikilink")，[多媒体技术也开始成熟](../Page/多媒体技术.md "wikilink")，电脑游戏成为了这些技术进步的先行者。3Dfx公司的3D显示卡也给行业带来了一场图像革命。

进入21世纪，[因特网的广泛使用为电脑游戏的发展带来了强大的动力](../Page/因特网.md "wikilink")。[大型多人在线游戏等](../Page/大型多人在线游戏.md "wikilink")[網絡遊戲成为了电脑游戏的一个新的发展方向](../Page/網絡遊戲.md "wikilink")。世界各地舉辦了許多各種各樣的[電子競技比賽](../Page/電子競技.md "wikilink")，創造了相當多的額外利潤。及後，[遊戲開發商開始開發一些](../Page/遊戲開發商.md "wikilink")[網頁遊戲](../Page/網頁遊戲.md "wikilink")，方便一些不想安裝遊戲[軟件的玩家](../Page/軟件.md "wikilink")。踏入2010年代，因應世界各地的手機熱潮，一部份電腦遊戲的[遊戲開發者轉而開發](../Page/遊戲開發者.md "wikilink")[手機遊戲](../Page/手機遊戲.md "wikilink")，與電腦遊戲和網絡遊戲三足鼎立，各據市場一方。

## 遊戲

### 電腦專用遊戲

专门为电脑开发的游戏往往对[鼠标](../Page/鼠标.md "wikilink")、[键盘有相当的依赖](../Page/键盘.md "wikilink")，都设计成方便点击及支持多按键，例如即时战略游戏（如《[星际争霸](../Page/星际争霸.md "wikilink")》、《[红色警戒](../Page/红色警戒系列_\(终极动员令\).md "wikilink")》等）和拥有技能较多的[角色扮演游戏](../Page/電子角色扮演遊戲.md "wikilink")（如《[魔兽世界](../Page/魔兽世界.md "wikilink")》），需要对单位进行编队或者灵活快速使用各种道具技能，鼠标与键盘尤其是快捷键的运用相当重要；多数非游戏厂商开发的作品，不会购买授权，所以只在最宽松的电脑上发布。前两类若移植到游戏机上，受限於手柄按键有限，对角色队伍的控制不佳。而在游戏机移植电脑的游戏，在键盘映射後对操作有一定麻烦，不过大可使用外设手柄解决。

### 遊戲類型

  - 非線上電腦遊戲
  - [線上遊戲](../Page/線上遊戲.md "wikilink")（網絡遊戲）
  - [網頁遊戲](../Page/網頁遊戲.md "wikilink")

### 技术

電腦遊戲與電子遊戲一樣都有一套專門的開發技術。不過相對於電子遊戲，電腦遊戲的開發更容易讓初學者入手，一般程序員都可以寫出簡單的電腦遊戲。

### 分类

通常按照游戏性质可分为：动作游戏，文字类，纸牌类，益智类，策略类

## 比賽

[世界电子竞技大赛](../Page/世界电子竞技大赛.md "wikilink")（簡稱）是目前世界上最受歡迎的國際性商业電玩比賽。

## 參考文獻

  - 孙百英主编，游戏之王：纵横电脑游戏世界，科学普及出版社，1998年。ISBN 7110044939
  - 高庆生总编，大众软件，大众软件杂志社，1995年。ISSN 1007-0060 邮发代号 82-726
  - 西門孟編著，遊戲產業概論，學林出版社，2008年。ISBN 978-7-80730-599-6

## 参看

  - [电子竞技](../Page/电子竞技.md "wikilink")
  - [ACG](../Page/ACG.md "wikilink")

[电脑游戏](../Category/电脑游戏.md "wikilink")
[Category:个人电脑](../Category/个人电脑.md "wikilink")
[Category:電子遊戲平台](../Category/電子遊戲平台.md "wikilink")
[Category:電子遊戲術語](../Category/電子遊戲術語.md "wikilink")