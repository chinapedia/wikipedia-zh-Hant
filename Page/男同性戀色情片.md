[Michael_Lucas_Men_of_Israel_film_shoot.jpg](https://zh.wikipedia.org/wiki/File:Michael_Lucas_Men_of_Israel_film_shoot.jpg "fig:Michael_Lucas_Men_of_Israel_film_shoot.jpg")
**男同性恋色情片**（，），在[華人地區又稱為](../Page/華人地區.md "wikilink")**Gay片**、**G片**\[1\]、**㚻片**，是指以展示[男性裸體或者](../Page/裸體.md "wikilink")[男男性交為主要內容的一種](../Page/男男性行為者.md "wikilink")[色情影片](../Page/色情影片.md "wikilink")。

## 歷史

[Michael_Lucas_Men_of_Israel_film_shoot_2.jpg](https://zh.wikipedia.org/wiki/File:Michael_Lucas_Men_of_Israel_film_shoot_2.jpg "fig:Michael_Lucas_Men_of_Israel_film_shoot_2.jpg")
[Pedro_Andreas_&_Daniel_Marvin.jpg](https://zh.wikipedia.org/wiki/File:Pedro_Andreas_&_Daniel_Marvin.jpg "fig:Pedro_Andreas_&_Daniel_Marvin.jpg")
男同色情片是一種包含以展現[男性間的性活動為主要內容的色情作品](../Page/男男性行為者.md "wikilink")，其主要目地是為了引起觀眾的[性刺激](../Page/性刺激.md "wikilink")。此外尚有所謂[軟性](../Page/軟調色情.md "wikilink")[男同性戀](../Page/男同性戀.md "wikilink")[色情作品](../Page/色情.md "wikilink")，其主要受眾为[男同性戀者](../Page/男同性戀者.md "wikilink")、[女异性戀者與](../Page/女异性戀者.md "wikilink")[男女雙性戀者](../Page/雙性戀.md "wikilink")。\[2\]

雖然由於社會大眾普遍為[異性戀者](../Page/異性戀者.md "wikilink")，色情文學通常也集中於男女間的性描寫，但描摹刻畫同性戀的藝術和手工藝品同異性戀一樣具有悠久的歷史，至早可以追溯到[古希臘時期](../Page/古希臘.md "wikilink")。歷史上男性間的性行為曾借助於每一種可能的媒介來表現自己，但目前則主要集中在[成人網站](../Page/成人網站.md "wikilink")、家庭影片（包括[DVD和](../Page/DVD.md "wikilink")[蓝光光碟](../Page/蓝光光碟.md "wikilink")）、[有线电视](../Page/有线电视.md "wikilink")、新兴的[视频点播服务與无线市场](../Page/视频点播.md "wikilink")，以及互联网上的图片及视频等[大眾媒體領域](../Page/大眾媒體.md "wikilink")。

## 影片類型

<table>
<thead>
<tr class="header">
<th><p>rowspan=|中文</p></th>
<th><p>英文</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>種族</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞洲人.md" title="wikilink">亞洲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歐洲民族.md" title="wikilink">歐洲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/白人.md" title="wikilink">白人</a></p></td>
<td></td>
<td><p>白色人種</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黑人.md" title="wikilink">黑人</a></p></td>
<td></td>
<td><p>黑色人種</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉丁裔.md" title="wikilink">拉丁</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/泰國人.md" title="wikilink">泰國</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/人種.md" title="wikilink">人種</a></p></td>
<td></td>
<td><p>不同膚色人種性交，一般是黑種人與白種人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/族群.md" title="wikilink">族裔</a></p></td>
<td></td>
<td><p>對於歐美人來說比較少見的族裔人士</p></td>
</tr>
<tr class="even">
<td><p><strong>身份</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/運動員.md" title="wikilink">體育</a></p></td>
<td></td>
<td><p>體育運動員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大學.md" title="wikilink">大學</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大學運動員.md" title="wikilink">大學運動員</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/性偏離.md" title="wikilink">變態</a></p></td>
<td></td>
<td><p>心理變態、性虐待</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/教師.md" title="wikilink">教師</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/軍人.md" title="wikilink">軍人</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/妓男.md" title="wikilink">妓男</a></p></td>
<td></td>
<td><p>指為錢入行的<a href="../Page/異性戀.md" title="wikilink">男異性戀者</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西裝.md" title="wikilink">正裝</a></p></td>
<td></td>
<td><p>商務正式套裝</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戀父情結.md" title="wikilink">戀父</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大叔.md" title="wikilink">熟男</a></p></td>
<td></td>
<td><p>成熟男性</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Cosplay.md" title="wikilink">扮裝</a></p></td>
<td></td>
<td><p>通過服飾、飾品、道具還有化妝的手段打扮成影視、動漫、遊戲裡面的人物角色</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/搖滾.md" title="wikilink">搖滾</a></p></td>
<td></td>
<td><p>模仿一種搖滾歌手的穿衣風格</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/直男.md" title="wikilink">直男</a></p></td>
<td></td>
<td><p>男異性戀者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/流氓.md" title="wikilink">流氓</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人妖.md" title="wikilink">人妖</a></p></td>
<td></td>
<td><p>變性人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/少男.md" title="wikilink">少男</a></p></td>
<td></td>
<td><p>年輕帥氣的小鮮肉</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/民工.md" title="wikilink">民工</a></p></td>
<td></td>
<td><p>体力劳动者</p></td>
</tr>
<tr class="even">
<td><p><strong>方式</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/口交.md" title="wikilink">口交</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/肛交.md" title="wikilink">肛交</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雙龍.md" title="wikilink">雙龍</a></p></td>
<td></td>
<td><p>兩人同時將陽具插入同一人之肛門以進行性交</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/自慰.md" title="wikilink">自慰</a></p></td>
<td></td>
<td><p>自慰</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/手交.md" title="wikilink">手交</a></p></td>
<td></td>
<td><p>互相幫對方自慰</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/群交.md" title="wikilink">群交</a></p></td>
<td></td>
<td><p>多人性交</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/虐戀.md" title="wikilink">調教</a></p></td>
<td></td>
<td><p>性虐待</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/舔肛.md" title="wikilink">舔肛</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/假屌.md" title="wikilink">假屌</a></p></td>
<td></td>
<td><p><a href="../Page/假陰莖.md" title="wikilink">假陰莖</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/顏射.md" title="wikilink">顏射</a></p></td>
<td></td>
<td><p>精液射於臉部</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/體內射精.md" title="wikilink">內射</a></p></td>
<td></td>
<td><p>精液射在<a href="../Page/肛門.md" title="wikilink">肛門裡面</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/性癖好.md" title="wikilink">癖好</a></p></td>
<td></td>
<td><p>特殊性癖好</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三人性行為.md" title="wikilink">三劈</a></p></td>
<td></td>
<td><p>三個人一起<a href="../Page/做愛.md" title="wikilink">做愛</a></p></td>
</tr>
<tr class="even">
<td><p><strong>特征</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/肌肉.md" title="wikilink">肌肉</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/健美.md" title="wikilink">健美</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/熊族.md" title="wikilink">熊族</a></p></td>
<td></td>
<td><p>「熊」在<a href="../Page/男同性戀.md" title="wikilink">同志圈裡指體型壯碩</a>、毛髮旺盛的男性</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/精瘦.md" title="wikilink">精瘦</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/肥胖.md" title="wikilink">肥胖</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/異裝.md" title="wikilink">異裝</a></p></td>
<td></td>
<td><p>男扮女裝，<a href="../Page/異裝癖.md" title="wikilink">異裝癖</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人類陰莖.md" title="wikilink">大屌</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/剃光頭.md" title="wikilink">光頭</a></p></td>
<td></td>
<td><p>沒有頭髮</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金髮.md" title="wikilink">金髮</a></p></td>
<td></td>
<td><p>金色頭髮</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紅髮.md" title="wikilink">紅髮</a></p></td>
<td></td>
<td><p>紅色頭髮</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戀足.md" title="wikilink">戀足</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戀物.md" title="wikilink">戀物</a></p></td>
<td></td>
<td><p>一般戀<a href="../Page/襪.md" title="wikilink">襪</a>、<a href="../Page/鞋.md" title="wikilink">鞋</a>、<a href="../Page/內褲.md" title="wikilink">內褲等</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無套性交.md" title="wikilink">無套</a></p></td>
<td></td>
<td><p>不戴安全套</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/兩性.md" title="wikilink">兩性</a></p></td>
<td></td>
<td><p>男主角同時與男人又與女人<a href="../Page/性交.md" title="wikilink">性交</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/少毛.md" title="wikilink">少毛</a></p></td>
<td></td>
<td><p>身體較少毛髮</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/多毛.md" title="wikilink">多毛</a></p></td>
<td></td>
<td><p>身體毛髮旺盛</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/硬調色情.md" title="wikilink">打真軍</a></p></td>
<td></td>
<td><p>又叫「硬調色情」</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/猛男.md" title="wikilink">猛男</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第一視角.md" title="wikilink">第一視角</a></p></td>
<td></td>
<td><p>以主角的第一視角拍攝整個性愛過程</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/虛擬實境.md" title="wikilink">虛擬實境</a></p></td>
<td></td>
<td><p>一種新技術，可以構建一個三維空間的虛擬世界，讓觀眾有身臨其境的感覺</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/紋身.md" title="wikilink">紋身</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/制服.md" title="wikilink">制服</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>特殊</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/假日.md" title="wikilink">節假日</a></p></td>
<td></td>
<td><p>「海邊度假」、「<a href="../Page/聖誕節.md" title="wikilink">聖誕節</a>」之類的主題</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/公眾地方.md" title="wikilink">公眾地方</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/工作.md" title="wikilink">工作</a></p></td>
<td></td>
<td><p>工作場合</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狂歡.md" title="wikilink">狂歡</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戶外.md" title="wikilink">戶外</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/按摩.md" title="wikilink">按摩</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/偷拍.md" title="wikilink">偷拍</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尋歡洞.md" title="wikilink">廁板洞</a></p></td>
<td></td>
<td><p>在公共廁所的廁所隔板處挖洞，可以把陰莖伸過去讓另一個人<a href="../Page/口交.md" title="wikilink">含舔</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/綁縛_(BDSM).md" title="wikilink">捆綁</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上班.md" title="wikilink">上班</a></p></td>
<td></td>
<td><p>工作</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/攝像頭.md" title="wikilink">攝像頭</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>其他</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/經典.md" title="wikilink">經典</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/教學.md" title="wikilink">教學</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/另類.md" title="wikilink">另類</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/業餘.md" title="wikilink">業餘</a></p></td>
<td></td>
<td><p>素人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/動畫.md" title="wikilink">動畫</a></p></td>
<td></td>
<td><p>包括二維與三維動畫片</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高清.md" title="wikilink">高清</a></p></td>
<td></td>
<td><p>高畫質、高清晰度</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 片商

## 相关条目

  - [攻受](../Page/攻受.md "wikilink")
  - [男男同性色情片演員](../Page/男男同性色情片演員.md "wikilink")
  - [男男性行為](../Page/男男性行為.md "wikilink")
  - [LGBT用語](../Page/LGBT用語.md "wikilink")
  - [性产业](../Page/性产业.md "wikilink")

## 註解

## 参考资料

[G](../Category/男同性戀.md "wikilink")
[Category:男色](../Category/男色.md "wikilink")
[G](../Category/色情片.md "wikilink")

1.  可見媒體報導範例：
2.  例如：《》、《》等杂志。