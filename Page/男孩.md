[Felicidade_A_very_happy_boy_2.jpg](https://zh.wikipedia.org/wiki/File:Felicidade_A_very_happy_boy_2.jpg "fig:Felicidade_A_very_happy_boy_2.jpg")
[LouisXVII-3.jpg](https://zh.wikipedia.org/wiki/File:LouisXVII-3.jpg "fig:LouisXVII-3.jpg")幼年（8岁）\]\]
**男孩**、**男童**或**男孩兒**，意於是[雄性的](../Page/雄性.md "wikilink")[人類](../Page/人類.md "wikilink")[兒童或](../Page/兒童.md "wikilink")[青少年](../Page/青少年.md "wikilink")，為相對於[雌性兒童](../Page/雌性.md "wikilink")（即[女孩](../Page/女孩.md "wikilink")）。「男孩」這個詞通常用來表示生物學的[性區别](../Page/性_\(生物學\).md "wikilink")，有時亦可指文化上[性別角色區別](../Page/性別角色.md "wikilink")（也可能是兩者）。成年的雄性人類一般稱為「[男人](../Page/男人.md "wikilink")」。

在[中文中](../Page/中文.md "wikilink")，通常以**少男**或泛稱**少年**表明男孩與男人的過渡時期（[青春期](../Page/青春期.md "wikilink")）。[古漢語](../Page/古漢語.md "wikilink")「少年」指[青年男子](../Page/青年.md "wikilink")，只限用男性。[日文裡的](../Page/日文.md "wikilink")“”則專指“男孩”，和現代中文的意思是有一定的區别（現代中文裡的“少年”通常指[青少年](../Page/青少年.md "wikilink")，包括男女，為中性用詞），但也有例外，例如在[日本法律中的](../Page/日本法律.md "wikilink")和中文意思相同；[中文裡使用日文借詞時少年的意思和日文相同](../Page/漢語中的日語借詞.md "wikilink")（例如[少年漫畫](../Page/少年漫畫.md "wikilink")、[美少年等](../Page/美少年.md "wikilink")），由於廣義上的男孩也可指青年甚至成年時期的，因此不少中譯日系動漫作品中亦常直接使用日式漢字詞彙「」來指「男孩」。

## 口語上的用法

有些情況下，成年男性會被稱為男孩。由青年男性組成的[樂團常統稱為](../Page/樂團.md "wikilink")「[男孩樂團](../Page/男孩團.md "wikilink")」。男人在傳統角色上未被承認（或否認）亦被稱為男孩。年輕的男性在尚未自覺是為男人時（例如開始步入社會，有了家庭、妻子和孩子），稱其為男人可能會令他感覺不舒服。相反的，一位自覺是男人的男性，稱其為男孩也可能會令他感到不舒服。

在中文上，男孩很常用來形容心智尚未成熟或保有童心的男人，有時候也會有貶低人的意思。在[美國](../Page/美國.md "wikilink")，男孩這個字有時候是用來歧視黑人或男性的[奴役](../Page/奴役.md "wikilink")（在歷史上）。在[種族隔離政策時期或其他歷史背景下的南非](../Page/种族隔离制度.md "wikilink")，廣泛的用來指黑人。在[英國](../Page/英國.md "wikilink")，足球員常被足球經理人稱為「某某男孩」，這個用法不限於年輕的足球員，僅管它很少被用于資深的球員。

在社會上，男孩（女孩）或男人（女人）的稱謂常會造成很多的困惑。最好不要花太多時間在瞭解其背後上的語意。當有不同看法時，有些人可能會覺得受到侵犯。

## 參見

  - [男性](../Page/男性.md "wikilink")
  - [男孩團體](../Page/男孩團體.md "wikilink")
  - [男童軍](../Page/男童軍.md "wikilink")
  - [兒童](../Page/兒童.md "wikilink")
  - [正太](../Page/正太.md "wikilink")

## 外部連結

  - [男孩服裝歷史（Historical Boys'
    Clothing）](http://members.tripod.com/~histclo/)

[Category:兒童](../Category/兒童.md "wikilink")
[Category:男性](../Category/男性.md "wikilink")