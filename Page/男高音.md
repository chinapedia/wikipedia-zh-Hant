[Sopran.png](https://zh.wikipedia.org/wiki/File:Sopran.png "fig:Sopran.png")\]\]
[Range_of_alto_voice_marked_on_keyboard.svg](https://zh.wikipedia.org/wiki/File:Range_of_alto_voice_marked_on_keyboard.svg "fig:Range_of_alto_voice_marked_on_keyboard.svg")\]\]
[Tenor.png](https://zh.wikipedia.org/wiki/File:Tenor.png "fig:Tenor.png")
[Range_of_bass_voice_marked_on_keyboard.png](https://zh.wikipedia.org/wiki/File:Range_of_bass_voice_marked_on_keyboard.png "fig:Range_of_bass_voice_marked_on_keyboard.png")\]\]
在[音乐](../Page/音乐.md "wikilink")，**男高音**（）是一名音域佔c－c2共15度的[歌唱家](../Page/歌唱家.md "wikilink")。他们按音質、音色、音区等不同特点分为：[抒情花腔男高音](../Page/抒情花腔男高音.md "wikilink")（lirico-Leggero
tenor）、[抒情男高音](../Page/抒情男高音.md "wikilink")（lyric
tenor）、英雄男高音（heldentenor）和[戏剧男高音](../Page/戏剧男高音.md "wikilink")（dramatic
tenor）。[柴可夫斯基的歌剧](../Page/柴可夫斯基.md "wikilink")《[黑桃皇后](../Page/黑桃皇后.md "wikilink")》中的男主人公格尔曼，就是典型的戏剧男高音。

## 著名男高音歌唱家

### 三大男高音

  - [卢奇亚诺·帕瓦罗蒂](../Page/卢奇亚诺·帕瓦罗蒂.md "wikilink")（Luciano
    PAVAROTTI，意大利，20世紀後半葉三大男高音，1935年—2007年）
  - [普拉西多·多明戈](../Page/普拉西多·多明戈.md "wikilink")（Placido
    DOMINGO，西班牙，20世紀後半葉三大男高音，1941年－）
  - [何塞·卡雷拉斯](../Page/何塞·卡雷拉斯.md "wikilink")（José CARRERAS
    Coll，西班牙，20世紀後半葉三大男高音，1946年—）

### 著名男高音歌唱家

  - [恩里科·卡鲁索](../Page/恩里科·卡鲁索.md "wikilink")（Enrico
    CARUSO，[意大利](../Page/意大利.md "wikilink")，1873年－1921年）
  - [賓尼亞米諾‧吉利](../Page/賓尼亞米諾‧吉利.md "wikilink")（Beniamino
    Gigli[義大利](../Page/義大利.md "wikilink")，1890年，3月20日－1957年，3月20日）
  - [約翰‧約拿坦‧『尤西』‧ 畢約林](../Page/約翰‧約拿坦‧『尤西』‧_畢約林.md "wikilink")（Johan
    Jonatan "Jussi"
    Björling，[瑞典](../Page/瑞典.md "wikilink")，1911年，2月5日－1960年，9月9日）
  - [沃夫冈·文狄格森](../Page/沃夫冈·文狄格森.md "wikilink")（Wolfgang
    WINDGASSEN，[德国](../Page/德国.md "wikilink")，1914年—1974年）
  - [马里奥·德·摩纳哥](../Page/马里奥·德·摩纳哥.md "wikilink")（Mario del
    MONACO，[意大利](../Page/意大利.md "wikilink")，1915年－）
  - [弗兰科·科莱里](../Page/弗兰科·科莱里.md "wikilink")（Franco
    CORELLI，意大利，1921年—2003年）
  - [朱塞佩‧德‧史蒂法諾](../Page/朱塞佩‧德‧史蒂法諾.md "wikilink")（Giuseppe di
    Stefano，[義大利](../Page/義大利.md "wikilink")，1921年—）
  - [尼可莱·盖达](../Page/尼可莱·盖达.md "wikilink")（Nicolai
    GEDDA，[瑞典](../Page/瑞典.md "wikilink")，1925年－2017年）
  - [詹姆斯·金](../Page/詹姆斯·金.md "wikilink")（James
    KING，[美国](../Page/美国.md "wikilink")，1925年－）
  - [杰姆斯·麦克莱肯](../Page/杰姆斯·麦克莱肯.md "wikilink")（James
    McCRACKEN，美国，1926年－）
  - [阿弗雷多·克劳斯](../Page/阿弗雷多·克劳斯.md "wikilink")（Alfredo
    KRAUS，[西班牙](../Page/西班牙.md "wikilink")，1927年－1999年）
  - [五十岚喜芳](../Page/五十岚喜芳.md "wikilink")（IGARASHI
    Kiyoshi，[日本](../Page/日本.md "wikilink")，1928年—2011年）
  - [弗里茨‧翁德里希](../Page/弗里茨‧翁德里希.md "wikilink")（Fritz
    Wunderlich，[德國](../Page/德國.md "wikilink")，1930年9月26日－1966年9月17日）
  - [彼得·許萊爾](../Page/彼得·許萊爾.md "wikilink")（Peter
    Schreier，[德國](../Page/德國.md "wikilink")，1935年7月29日－）
  - 马切罗·[阿瓦雷兹](../Page/阿瓦雷兹.md "wikilink")（Marcelo
    Álvarez，阿根廷，现在最红也是公认最优秀的抒情男高音，1962年－）
  - 萨尔瓦托·[里契特拉](../Page/里契特拉.md "wikilink")（[Salvatore
    Licitra](../Page/Salvatore_Licitra.md "wikilink")，意大利，被认为是帕瓦罗蒂的最佳接班人，1968年－2011）
  - [魏松](../Page/魏松.md "wikilink")（WEI
    Song，[中国](../Page/中国.md "wikilink")，1954年－）
  - [戴玉强](../Page/戴玉强.md "wikilink")（DAI
    Yuqiang，[中国](../Page/中国.md "wikilink")，1963年出生，当今中国最著名的抒情男高音之一）
  - [锦织健](../Page/锦织健.md "wikilink")（NISIKIORI Ken，日本，1960年—）
  - [莫华伦](../Page/莫华伦.md "wikilink")（Warren MOK，香港著名男高音）
  - [羅伯托·阿藍尼亞](../Page/羅伯托·阿藍尼亞.md "wikilink")（Roberto
    ALAGNA，[法國](../Page/法國.md "wikilink")，1963年—）
  - [厄斯‧布勒](../Page/厄斯‧布勒.md "wikilink")（[Urs
    Bühler](../Page/Urs_Bühler.md "wikilink")，[瑞士](../Page/瑞士.md "wikilink")，1971年－）
  - [埃米利歐‧羅蘭多‧費亞松‧毛萊昂](../Page/埃米利歐‧羅蘭多‧費亞松‧毛萊昂.md "wikilink")（Emilio
    Rolando Villazón
    Mauleón，[法國](../Page/法國.md "wikilink")，生於[墨西哥](../Page/墨西哥.md "wikilink")，1972年2月22日－）
  - [大衛‧米勒](../Page/大衛·米勒.md "wikilink")（[David
    Miller](../Page/David_Miller_\(tenor\).md "wikilink")，[美國](../Page/美國.md "wikilink")，1973年－）
  - [安德烈·波伽利](../Page/安德烈·波伽利.md "wikilink")（Andrea
    Bocelli，[義大利](../Page/義大利.md "wikilink")，1958年9月22日－）
  - [胡安·迪亞戈·佛瑞茲](../Page/胡安·迪亞戈·佛瑞茲.md "wikilink") (Juan Diego
    Flórez，[祕魯](../Page/祕魯.md "wikilink")，1973年1月13日－）
  - [柯大衛](../Page/柯大衛.md "wikilink") (David Quah
    ，[馬來西亞籍](../Page/馬來西亞.md "wikilink")），香港著名男高音，生於[馬來西亞](../Page/馬來西亞.md "wikilink")[檳城](../Page/檳城.md "wikilink")，演唱逾三十套的歌劇，現於[香港演藝學院教授聲樂以及擔任唱歌指導](../Page/香港演藝學院.md "wikilink")。
  - [艾飛包爾](../Page/艾飛包爾.md "wikilink") (Alfie Boe,
    [英國](../Page/英國.md "wikilink"),
    [1973](../Page/1973.md "wikilink")-）

## 男高音分类

  - 抒情男高音
      - 大号抒情男高音（重抒情男高音）
      - 普通抒情男高音
      - 轻型抒情男高音（罗西尼男高音）
  - 戏剧男高音
  - [假声男高音](../Page/假声男高音.md "wikilink")
  - 强力男高音
  - 花腔男高音

## 其他声部

  - [男中音](../Page/男中音.md "wikilink")（Baritone）
  - [男低音](../Page/男低音.md "wikilink")（Bass）
  - [女高音](../Page/女高音.md "wikilink")（Soprano）
  - [女中音](../Page/女中音.md "wikilink")（Mezzo-soprano，又作[次女高音](../Page/次女高音.md "wikilink")）
  - [女低音](../Page/女低音.md "wikilink")（Contralto，又作Alto）

[Category:声乐](../Category/声乐.md "wikilink")
[男高音](../Category/男高音.md "wikilink")