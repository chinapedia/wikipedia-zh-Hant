**町屋二丁目停留場**（）是一位於[東京都](../Page/東京都.md "wikilink")[荒川區](../Page/荒川區.md "wikilink")[荒川六丁目](../Page/荒川_\(荒川區\).md "wikilink")，屬於[荒川線的](../Page/荒川線.md "wikilink")[停留場](../Page/鐵路車站.md "wikilink")。

## 停留場構造

[對向式月台](../Page/對向式月台.md "wikilink")2面2線的[地面車站](../Page/地面車站.md "wikilink")。

## 相鄰停留場

  - [PrefSymbol-Tokyo.svg](https://zh.wikipedia.org/wiki/File:PrefSymbol-Tokyo.svg "fig:PrefSymbol-Tokyo.svg")
    東京都交通局
    [Tokyo_Sakura_Tram_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyo_Sakura_Tram_symbol.svg "fig:Tokyo_Sakura_Tram_symbol.svg")
    都電荒川線（東京櫻電）
      -
        [東尾久三丁目](../Page/東尾久三丁目停留場.md "wikilink")（SA 08）－**町屋二丁目（SA
        07）**－[町屋站前](../Page/町屋站.md "wikilink")（SA 06）

## 外部連結

  - [東京都交通局
    町屋二丁目停留場](http://www.kotsu.metro.tokyo.jp/toden/stations/machiya2-chome/index.html)

[ChiyaNichoume](../Category/日本鐵路車站_Ma.md "wikilink")
[Category:荒川線車站](../Category/荒川線車站.md "wikilink")
[Category:荒川區鐵路車站](../Category/荒川區鐵路車站.md "wikilink")
[Category:1913年啟用的鐵路車站](../Category/1913年啟用的鐵路車站.md "wikilink")