《**画魂**》（）是根据华裔旅法画家[潘玉良的故事改编的一部电影](../Page/潘玉良.md "wikilink")。影片由[黃蜀芹导演](../Page/黃蜀芹.md "wikilink")，[张艺谋監製](../Page/张艺谋.md "wikilink")，1994年3月12日上映。

## 演員名單

|                                  |                                  |
| -------------------------------- | -------------------------------- |
| 演員                               | 角色                               |
| [鞏　俐](../Page/鞏俐.md "wikilink")  | [潘玉良](../Page/潘玉良.md "wikilink") |
| [爾冬陞](../Page/爾冬陞.md "wikilink") | [潘贊化](../Page/潘贊化.md "wikilink") |
| [達式常](../Page/達式常.md "wikilink") | 王守信                              |
| [沈海蓉](../Page/沈海蓉.md "wikilink") | 潘大太太                             |
| [張瓊姿](../Page/張瓊姿.md "wikilink") | 千歲紅                              |
| [高俊霞](../Page/高俊霞.md "wikilink") | 賀瓊                               |
| [周紹棟](../Page/周紹棟.md "wikilink") | 薛無                               |
| [方岑](../Page/方岑.md "wikilink")   | 小蘭                               |
| [尤麗華](../Page/尤麗華.md "wikilink") | 老鴇                               |
| [邊威](../Page/邊威.md "wikilink")   | 龜頭                               |
| 卡洛斯                              | 法國畫商                             |
| [田豐](../Page/田豐.md "wikilink")   | 商會會長                             |
| [赵薇](../Page/赵薇.md "wikilink")   | 一群众演員 - 妓女                       |

## 外部链接

  -
  -
  - [画魂](http://www.dianying.com/ft/title/hh-19942)在中文電影資料庫

  -
  - {{@movies|fCcmb2022426}}

  -
  -
[H画](../Category/張藝謀電影.md "wikilink")
[H画](../Category/1994年电影.md "wikilink")
[H画](../Category/畫家主角題材電影.md "wikilink")
[H画](../Category/1990年代劇情片.md "wikilink")
[H画](../Category/中國劇情片.md "wikilink")
[H画](../Category/中國傳記片.md "wikilink")
[H画](../Category/中國小說改編電影.md "wikilink")
[Category:法國背景電影](../Category/法國背景電影.md "wikilink")