**畑健二郎**（），[日本男性](../Page/日本.md "wikilink")[漫画家](../Page/漫画家.md "wikilink")。出生於[福岡縣](../Page/福岡縣.md "wikilink")[福岡市出身](../Page/福岡市.md "wikilink")，[血型A型](../Page/血型.md "wikilink")。[久米田康治為其師父](../Page/久米田康治.md "wikilink")。[大阪藝術大學藝術學部畢業](../Page/大阪藝術大學.md "wikilink")。

## 作品

  - （2002年11月増刊号、週刊少年Sunday特別增刊R）

  - （2003年2月号、週刊少年Sunday超增刊）

  - （2003年7 - 11月号、週刊少年Sunday超增刊）

  - （2004年10号、週刊少年Sunday） （收錄在『少年Sunday公式手冊 旋風管家！』中。（

  - [旋風管家](../Page/旋風管家.md "wikilink")（ハヤテのごとく\!），全52卷（2004年45号 -
    2017年20号、週刊少年Sunday）

  - [現視研第](../Page/現視研.md "wikilink")9卷 特裝版 別冊附錄漫畫（2006年）

  - [那就是聲優](../Page/那就是聲優.md "wikilink")，擔任作畫（2011年至2017年8月、原作：[浅野真澄](../Page/浅野真澄.md "wikilink")、[同人誌型態](../Page/同人誌.md "wikilink")）

  - 邁向星世界！（アド アストラ ペル アスペラ；AD ASTRA★PER ASPERA），共1卷（2015年40号
    -、週刊少年Sunday）（月刊、現在連載停止中。）

  - （2017年12号 - 連載中、週刊少年Sunday）

### 單行本

  - 畑健二郎初期作品集 旋風管家！之前（畑健二郎初期作品集 ハヤテのごとく\!の前）全1卷

<!-- end list -->

  -

      -
        收錄：、

## 外部連結

  - [ハヤテのごとく！](http://websunday.net/hayate/)，週刊少年Sunday上，旋風管家的網頁。

  - [週刊少年Sunday的畑健二郎個人網頁](http://websunday.net/backstage/hata/)

  - [畑健二郎於twitter的個人頁](http://twitter.com/hatakenjiro)\[1\]

## 參考資料

<div class="references-small">

<references />

</div>

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:福岡市出身人物](../Category/福岡市出身人物.md "wikilink")
[Category:大阪艺术大学校友](../Category/大阪艺术大学校友.md "wikilink")

1.  [バックステージVol.287](http://websunday.net/backstage/hata/287.html)，2010年5月12日。