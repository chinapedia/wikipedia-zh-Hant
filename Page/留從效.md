**留从效**（），字元-{範}-，[五代時](../Page/五代.md "wikilink")[泉州](../Page/泉州.md "wikilink")[南安桃林场](../Page/南安.md "wikilink")(今[永春县](../Page/永春县.md "wikilink")[桃城镇留安村](../Page/桃城镇.md "wikilink"))人\[1\]，曾任[清源军节度使](../Page/清源军节度使.md "wikilink")、晋江王。

## 生平

年少时父亲即去世，孝顺母亲及兄长，远近知名，喜欢读书，又颇好兵法。闽帝王延羲在位时任泉州散员指挥使。

[后晋](../Page/后晋.md "wikilink")[开运元年](../Page/开运.md "wikilink")（944年），[朱文進](../Page/朱文進.md "wikilink")、[連重遇殺](../Page/連重遇.md "wikilink")[王延羲](../Page/王延羲.md "wikilink")，朱文進並自立為閩主。留從效與[張漢思](../Page/張漢思.md "wikilink")、[董思安](../Page/董思安.md "wikilink")、[陳洪進等人不願做朱](../Page/陳洪進.md "wikilink")、連一黨的下屬，遂反，殺朱文進任命的泉州[刺史](../Page/刺史.md "wikilink")[黃紹頗](../Page/黃紹頗.md "wikilink")，留從效自稱平賊統軍使，並迎殷帝[王延政之姪](../Page/王延政.md "wikilink")[王繼勳為泉州刺史](../Page/王繼勳.md "wikilink")，奉王延政為主。

不久，朱文進被殺，王延政亦降於[南唐](../Page/南唐.md "wikilink")，留從效遂歸[南唐](../Page/南唐.md "wikilink")，被任命為泉州都指揮使。南唐與割據福州的[李仁達決裂](../Page/李仁達.md "wikilink")，留從效奉南唐命令領兵助攻福州，但失敗。南唐[保大四年](../Page/保大.md "wikilink")（946年），李仁達派弟李仁通討伐泉州，留從效乘機罷黜泉州刺史王繼勳，掌控軍政\[2\]，南唐因勢力未能到達此地，只能追認留從效為泉州刺史。同年再占領漳州（後一度改名南州，今福建[漳州](../Page/漳州.md "wikilink")），任命董思安為刺史。

保大七年（949年），留從效之兄南州副史[留從願毒殺董思安](../Page/留從願.md "wikilink")，自任刺史，南唐帝[李璟只得在泉州設清源軍](../Page/李璟.md "wikilink")，以留從效為[節度使](../Page/節度使.md "wikilink")\[3\]。後來又被南唐任命為同平章事兼[侍中](../Page/侍中.md "wikilink")、中書令，封鄂國公，最後進封為晉江王。

南唐向[後周稱臣後](../Page/後周.md "wikilink")，留從效曾派使者至後周請求歸附，但不被後周世宗[柴榮所允許](../Page/柴榮.md "wikilink")。但在宋[建隆元年](../Page/建隆.md "wikilink")（960年），[宋朝新建](../Page/宋朝.md "wikilink")，太祖[趙匡胤接受了留從效稱臣的請求](../Page/趙匡胤.md "wikilink")。

留從效出身寒微，頗知百姓疾苦，因此為政以勤儉養民為要務，人民非常愛戴他。每年又開[進士](../Page/進士.md "wikilink")、[明經考試](../Page/明經.md "wikilink")，稱之「秋堂」。不過也由於懼怕其地被南唐所收回，因此向南唐、後周及宋都進貢大量財物。

留從效無子，而以其兄留從願之子[留紹錤](../Page/留紹錤.md "wikilink")、[留紹鎡為子](../Page/留紹鎡.md "wikilink")。宋建隆三年（962年），留從效病重，留從願在漳州，留紹錤剛好至南唐進貢，遂讓留紹鎡領軍務以繼其位，不久去世，被南唐贈[太尉](../Page/太尉.md "wikilink")、靈州大都督。

## 注釋

<div class="references-small">

<references />

</div>

[category:闽南人](../Page/category:闽南人.md "wikilink")

[Category:五代十国人物](../Category/五代十国人物.md "wikilink")
[Category:閩國人物](../Category/閩國人物.md "wikilink")
[Category:中國閩南人](../Category/中國閩南人.md "wikilink")
[Category:永春人](../Category/永春人.md "wikilink")
[C](../Category/留姓.md "wikilink")

1.
2.  [資治通鑑認為此時在](../Page/資治通鑑.md "wikilink")946年，而[宋史認為在](../Page/宋史.md "wikilink")947年，今從前者。
3.  留從效於949年-962年任清源節度使之職，但實際自946年就開始割據泉、漳二州。