**留邊蘂町**（）為過去位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[網走支廳的行政區劃](../Page/網走支廳.md "wikilink")，已於2006年3月5日與北見市、[常呂町](../Page/常呂町.md "wikilink")、[端野町](../Page/端野町.md "wikilink")[合併為新設立的](../Page/市町村合併.md "wikilink")[北見市](../Page/北見市.md "wikilink")。轄區內有「北狐狸牧場」、「北海道狐狸村·馴鹿旅遊牧場」等以放養為主題的旅遊牧場。在二次世界大戰前，曾經有亞洲最大的水銀礦山，並有全日本唯一的水銀回收工廠。

名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「ru-pes-pe」，意思為沿著道路的河川。\[1\]

## 歷史

  - 1915年4月1日：從野付牛町（後來的北見市）分割出獨立設置成村，此時名為武華村。\[2\]
  - 1921年6月15日：改制並改名為留邊蘂町。
  - 1938年4月1日：成為北海道一級町。
  - 2006年3月5日：與北見市、[端野町](../Page/端野町.md "wikilink")、[常呂町合併為新設立的](../Page/常呂町.md "wikilink")[北見市](../Page/北見市.md "wikilink")。

## 產業

主要產業包括[農業](../Page/農業.md "wikilink")、[林業](../Page/林業.md "wikilink")、[旅遊業](../Page/旅遊業.md "wikilink")，主要農產包括[洋蔥](../Page/洋蔥.md "wikilink")、[小麥](../Page/小麥.md "wikilink")。

## 交通

### 機場

  - [女滿別機場](../Page/女滿別機場.md "wikilink")（位於[大空町](../Page/大空町.md "wikilink")）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [石北本線](../Page/石北本線.md "wikilink")：（[常紋號誌站](../Page/常紋號誌站.md "wikilink")）
        - [金華车站](../Page/金華站_\(北海道\).md "wikilink") -
        [西留邊蘂車站](../Page/西留邊蘂車站.md "wikilink") -
        [留邊蘂車站](../Page/留邊蘂車站.md "wikilink")

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li>北見・網走自動車道（興建中）</li>
</ul>
<dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道39號</li>
<li>國道242號</li>
</ul>
<dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要道道</a></dt>

</dl>
<ul>
<li>北海道道88號本別留邊蘂線</li>
<li>北海道道103號留邊蘂濱佐呂間線</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道247號置戶溫根湯線</li>
<li>北海道道307號留邊蘂停車場線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

### 娛樂

  - [溫根湯溫泉](../Page/溫根湯溫泉.md "wikilink")
  - 鹽別溫泉
  - [瀧之湯溫泉](../Page/瀧之湯溫泉.md "wikilink")
  - [北見溫泉](../Page/北見溫泉.md "wikilink")
  - 釣り堀厚和
  - 留邊蘂町弓道場
  - 八方台：森林公園、滑雪場
  - 創造之森

### 觀光

  - [北狐狸牧場](../Page/北狐狸牧場.md "wikilink")

  - 山之水族館、鄉土館

  - 北海道狐狸村、馴鹿觀光牧場

  - 留邊蘂町開拓資料館

  - 小野塚正信記念館

  - ：北海道的指定自然文物

  -   - 果夢林之館：木工體驗工房

  - 石北峰

  - 北狐狸牧場

### 祭典

  - 溫根湯杜鵑祭：5月上旬
  - 溫根湯溫泉祭：8月的第一個星期六及星期日

## 教育

### 高等學校

  - 道立北海道留邊蘂高等學校

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>留邊蘂町立溫根湯中學校</li>
<li>留邊蘂町立瑞穗中學校</li>
</ul></td>
<td><ul>
<li>留邊蘂町立留邊蘂中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>留邊蘂町立溫根湯小學校</li>
<li>留邊蘂町立瑞穗小學校</li>
</ul></td>
<td><ul>
<li>留邊蘂町立大和小學校</li>
<li>留邊蘂町立留邊蘂小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 本地出身的名人

  - [神田山陽](../Page/神田山陽.md "wikilink")：講談師。
  - [柴田三雄](../Page/柴田三雄.md "wikilink")：[攝影師](../Page/攝影師.md "wikilink")。
  - [松樹路人](../Page/松樹路人.md "wikilink")：[西洋畫家](../Page/西洋畫家.md "wikilink")。
  - 柴田ヨクサル：[漫畫家](../Page/漫畫家.md "wikilink")。

## 外部連結

  - [北見市留邊蘂綜合支所](https://web.archive.org/web/20070817034637/http://www.city.kitami.lg.jp/rub_top/top.htm)

## 參考資料

<div style="font-size: 85%">

<references />

</div>

[Category:北見市](../Category/北見市.md "wikilink")
[Category:鄂霍次克管內](../Category/鄂霍次克管內.md "wikilink")

1.
2.