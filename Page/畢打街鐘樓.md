[Pedderstreetclocktower.jpg](https://zh.wikipedia.org/wiki/File:Pedderstreetclocktower.jpg "fig:Pedderstreetclocktower.jpg")
[Pedder_Street_Clock_Tower_in_1861.jpg](https://zh.wikipedia.org/wiki/File:Pedder_Street_Clock_Tower_in_1861.jpg "fig:Pedder_Street_Clock_Tower_in_1861.jpg")
[Pedder_Street_1870s.jpg](https://zh.wikipedia.org/wiki/File:Pedder_Street_1870s.jpg "fig:Pedder_Street_1870s.jpg")

**畢打街鐘樓**（****）是[香港昔日一座](../Page/香港.md "wikilink")[鐘樓](../Page/鐘樓.md "wikilink")，原址位於[香港島](../Page/香港島.md "wikilink")[中環](../Page/中環.md "wikilink")[皇后大道中及](../Page/皇后大道.md "wikilink")[畢打街交界處](../Page/畢打街.md "wikilink")。

## 歷史

畢打街鐘樓建於1861年至1862年，樓高80英尺（24.4米），分為5層，具有報時及報火警的作用，於夜間也有[導航的作用](../Page/導航.md "wikilink")。該鐘樓由一名稱為Rawlings的人[設計](../Page/設計.md "wikilink")\[1\]，但是因為籌款不足，設計中的部份細節需要省略。鐘樓得到[德忌利士洋行](../Page/德忌利士洋行.md "wikilink")[東主](../Page/東主.md "wikilink")[德忌利士](../Page/德忌利士.md "wikilink")（）捐款興建，是當時中環的[地標之一](../Page/地標.md "wikilink")，也被稱為**大鐘樓**（****）。1863年1月1日午夜12時，畢打街鐘樓第一次響起，迎接[元旦的來臨](../Page/元旦.md "wikilink")\[2\]。

1913年\[3\]，由於皇后大道中需要進行道路擴闊工程，鐘樓因此被拆卸，其中一面鐘面於1915年被安裝到當時剛興建成的[九廣鐵路](../Page/九廣鐵路.md "wikilink")[尖沙咀站的](../Page/尖沙咀站_\(九廣鐵路\).md "wikilink")[尖沙咀鐘樓](../Page/尖沙咀鐘樓.md "wikilink")，直至1921年該鐘樓換上全新四鐘面報時大鐘為止\[4\]。

雖然畢打街鐘樓被拆卸，但是[香港人於](../Page/香港人.md "wikilink")20世紀初仍然習慣稱該處為**大鐘樓**。另一方面，位於鐘樓原址附近，於1926年建成的全香港最高建築物[告羅士打行附設的](../Page/告羅士打行.md "wikilink")[告羅士打鐘樓](../Page/告羅士打鐘樓.md "wikilink")，取代了畢打街鐘樓的地位。[香港日治時期](../Page/香港日治時期.md "wikilink")，大鐘樓一帶曾經被更名為[昭和廣場](../Page/昭和廣場.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

</div>

[Category:香港已不存在的建築物](../Category/香港已不存在的建築物.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港鐘樓](../Category/香港鐘樓.md "wikilink")
[Category:香港建築之最](../Category/香港建築之最.md "wikilink")

1.  [The Clock Tower, Hong
    Kong](http://irc.aa.tufs.ac.jp/thomson/vol_1/mother/105.html)
    Illustrations of China and Its People, John Thomson 1837-1921,
    (London,1873-1874)
2.
3.
4.