[College_graduate_students.jpg](https://zh.wikipedia.org/wiki/File:College_graduate_students.jpg "fig:College_graduate_students.jpg")的畢業典禮。\]\]

**畢業**指完成某個學歷階段之事，這必須取得及格的[成績](../Page/成績.md "wikilink")，並符合學校規定的成績及年級，學生會得到[畢業證書做為畢業的認證](../Page/畢業證書.md "wikilink")，成為[畢業生](../Page/畢業生.md "wikilink")。多數[學校會舉行證書的派發儀式](../Page/學校.md "wikilink")，名為「畢業典禮」。不過參加畢業典禮並非取得畢業證書或學位證書的必要條件，一部份學生可能在畢業典禮舉行時尚未達到畢業的資格但仍會出席畢業典禮，也有一些學生是在學期中就已達到畢業的資格，提前取得[畢業證書或](../Page/畢業證書.md "wikilink")[學位證書而離校](../Page/學位證書.md "wikilink")。如果学生在其中一個學歷階段有[留级太多年](../Page/留级.md "wikilink")、教育主管當局的人又不破例给超龄学生读，他們僅能[肄業而无法毕业](../Page/肄業.md "wikilink")，得不到毕业证书。再者，若學生所繳證明文件，有假借、冒用、偽造或變造等情事，雖在畢業後始發覺，但仍足以取消畢業資格。\[1\]

## 引申

[篠田麻里子在AKB48劇場外講話.jpg](https://zh.wikipedia.org/wiki/File:篠田麻里子在AKB48劇場外講話.jpg "fig:篠田麻里子在AKB48劇場外講話.jpg")於2013年從[AKB48畢業時](../Page/AKB48.md "wikilink")，在畢業公演後於[AKB48劇場外向歌迷講話](../Page/AKB48劇場.md "wikilink")\]\]

在日本，「畢業」（）一詞也延伸引用為某人完成某事而將離開。日本女子[偶像組合的成員離隊常以](../Page/日本偶像.md "wikilink")「畢業」稱之（例如[AKB48及](../Page/AKB48.md "wikilink")[早安少女組](../Page/早安少女組.md "wikilink")）。女子偶像宣佈畢業的時期大多集中於春天前後的入學期或年末，在畢業時人氣偶像大多會舉行盛大的儀式。由於社會對偶像藝人的接受程度不一，偶像畢業後大多難以取得成功，與此同時偶像畢業亦容易導致組合本身的支持者也相應減少，因此在成員畢業時組合多數會介紹新人，從而穩定支持者的數目。

在政治上，一些成功脫貧而不再是[國際開發協會或](../Page/國際開發協會.md "wikilink")[政府開發援助一類組織的會員的國家](../Page/政府開發援助.md "wikilink")，日本傳媒會稱呼她們為「畢業國」（）；而職場上，「畢業」一詞也有[解雇的意味](../Page/解雇.md "wikilink")。

## 參考資料

## 參見

  - [博士](../Page/博士.md "wikilink")
  - [硕士](../Page/硕士.md "wikilink")
  - [學士](../Page/學士.md "wikilink")
  - [母校](../Page/母校.md "wikilink")
  - [四方帽](../Page/四方帽.md "wikilink")
  - [畢業娃娃](../Page/畢業娃娃.md "wikilink")
  - [送花](../Page/送花.md "wikilink")
  - [攝影師](../Page/攝影師.md "wikilink")
  - [履歷表](../Page/履歷表.md "wikilink")
  - [學歷造假](../Page/學歷造假.md "wikilink")
  - [假證書](../Page/假證書.md "wikilink")
  - [畢業禮](../Page/畢業禮.md "wikilink")
  - [肄業](../Page/肄業.md "wikilink")
  - [肄業證書](../Page/肄業證書.md "wikilink")
  - [畢業證書](../Page/畢業證書.md "wikilink")
  - [結業證書](../Page/結業證書.md "wikilink")
  - [畢業旅行](../Page/畢業旅行.md "wikilink")

[Category:教育](../Category/教育.md "wikilink")

1.  例如[國立臺灣大學學則](http://host.cc.ntu.edu.tw/sec/all_Law/2/2-01.html)第九條