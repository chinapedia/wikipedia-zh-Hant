**番木鳖碱**（），又稱**番木鼈鹼**，**马钱子碱**，又译**士的宁**，**士的年**（有时被错写成**土的宁**），是一种剧毒的化学物质，一般用来毒杀[老鼠等](../Page/老鼠.md "wikilink")[啮齿类动物](../Page/啮齿类.md "wikilink")。对人类亦有剧毒（成人的[致死量约为](../Page/致死量.md "wikilink")5mg/kg体重）\[1\]。番木鱉鹼可提煉自[馬錢子](../Page/馬錢子.md "wikilink")（*Strychnos
nux-vomica*
L.），978年賜死[南唐末代君主](../Page/南唐.md "wikilink")[李後主用的](../Page/李後主.md "wikilink")[牽機藥可能就是番木鱉鹼](../Page/牽機藥.md "wikilink")。

## 中毒症状

番木鱉鹼會使[中樞神經失常](../Page/中樞神經.md "wikilink")。中毒一般20分钟后开始发作，症状为[肌肉剧烈](../Page/肌肉.md "wikilink")[抽搐](../Page/抽搐.md "wikilink")，直到[窒息或精疲力竭而死](../Page/窒息.md "wikilink")，呈現[角弓反張](../Page/角弓反張.md "wikilink")，症狀甚似[破傷風](../Page/破傷風.md "wikilink")。死後嘴角通常會露出詭異的笑容，因此經常成為[推理小說中的題材](../Page/推理小說.md "wikilink")。

## 毒理

[毒理学研究显示番木鳖碱对](../Page/毒理学.md "wikilink")[脑部和](../Page/脑.md "wikilink")[脊髓](../Page/脊髓.md "wikilink")[细胞的](../Page/细胞.md "wikilink")[甘胺酸](../Page/甘胺酸.md "wikilink")[受体有](../Page/受体.md "wikilink")[拮抗作用](../Page/拮抗作用.md "wikilink")。

## 治疗

没有已知的解药，临床上一般使用[巴比妥类](../Page/巴比妥.md "wikilink")[镇静剂和肌肉](../Page/镇静剂.md "wikilink")[弛缓剂等来缓解症状](../Page/弛缓剂.md "wikilink")，必要时可使用[人工呼吸](../Page/人工呼吸.md "wikilink")。如果抢救及时，病人在中毒后存活超过24小时，就有可能康复。

## 關連項目

  -
  - [番木鳖碱毒理](../Page/番木鳖碱毒理.md "wikilink")（Strychnine poisoning）

  - [斯泰尔斯的神秘案件](../Page/斯泰尔斯的神秘案件.md "wikilink")

  - [血字的研究](../Page/血字的研究.md "wikilink")

## 参考文献

## 外部連結

  - [士的寧
    Strychnine](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00083)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[Category:植物毒素](../Category/植物毒素.md "wikilink")
[Category:惊厥剂](../Category/惊厥剂.md "wikilink")
[Category:内酰胺](../Category/内酰胺.md "wikilink")
[Category:吲哚生物碱](../Category/吲哚生物碱.md "wikilink")
[Category:杀鸟剂](../Category/杀鸟剂.md "wikilink")
[Category:苦味化合物](../Category/苦味化合物.md "wikilink")
[Category:剧毒化学品](../Category/剧毒化学品.md "wikilink")

1.