[Fanhan_heshi_zhangzhongzhu.jpg](https://zh.wikipedia.org/wiki/File:Fanhan_heshi_zhangzhongzhu.jpg "fig:Fanhan_heshi_zhangzhongzhu.jpg")
《**番汉合時掌中珠**》（[西夏文](../Page/西夏文.md "wikilink")：）是一本[西夏文和](../Page/西夏文.md "wikilink")[汉语双解](../Page/汉语.md "wikilink")[词典](../Page/词典.md "wikilink")，由[西夏人](../Page/西夏.md "wikilink")[骨勒茂才在](../Page/骨勒茂才.md "wikilink")[西夏仁宗](../Page/西夏仁宗.md "wikilink")[乾祐二十一年](../Page/乾祐.md "wikilink")（1190年）编写刊行。

## 出土经过

该书于1909年由[俄国探险家](../Page/俄国.md "wikilink")[彼得·库兹米奇·科兹洛夫在](../Page/彼得·库兹米奇·科兹洛夫.md "wikilink")[中国](../Page/中国.md "wikilink")[内蒙古](../Page/内蒙古.md "wikilink")[额济纳旗出土](../Page/额济纳旗.md "wikilink")，随同大量其他文物被运回俄国，后被俄国著名[汉学家](../Page/汉学家.md "wikilink")、[圣彼得堡大学教授](../Page/圣彼得堡大学.md "wikilink")发现其价值并收藏。

1912年，中国学者[罗振玉首次从伊凤阁处得知此书](../Page/罗振玉.md "wikilink")，借得十页后影印出版。1922年罗振玉在[天津再次会见伊凤阁](../Page/天津.md "wikilink")，方才得见全文，于是命其子[罗福成抄录刊行](../Page/罗福成.md "wikilink")。

[比利时汉学家](../Page/比利时.md "wikilink")于1970年代末访问[苏联时](../Page/苏联.md "wikilink")，拍摄到了全本《番汉合时掌中珠》。1982年，陆宽田在美国出版全部影印原件。该书终于得见天日。

## 内容体例

该书全书共37页，[蝴蝶装](../Page/蝴蝶装.md "wikilink")，为[木刻](../Page/木刻.md "wikilink")[雕版印刷](../Page/雕版印刷.md "wikilink")。书前有序言，序言有西夏文和汉文两种，内容相同。谓“不学番言，则岂和番人之众；不会汉语，则岂入汉人之数。”表明本书目的在于便于[西夏人和](../Page/西夏人.md "wikilink")[汉人互相学习对方语言](../Page/汉人.md "wikilink")。

该书中共收录词语414条，按[三才分为九类](../Page/三才.md "wikilink")：天体、天相、天变、地体、地相、地用、人体、人相、人事。其中人事类包含最广，占到全部篇幅的一半。

该书每一词语并列四项，自右向左为西夏文汉字[注音](../Page/注音.md "wikilink")、西夏文、汉文、汉文西夏文字注音。该书对解读已经失传的西夏语有着十分重要的意义。

## 相關詞條

  - 《[同音](../Page/同音.md "wikilink")》
  - 《[文海(西夏韻書)](../Page/文海\(西夏韻書\).md "wikilink")》
  - 《[文海寶韻](../Page/文海寶韻.md "wikilink")》
  - 《[五音切韻](../Page/五音切韻.md "wikilink")》

## 参考资料

  - [中国大百科全书·番汉合时掌中珠](https://web.archive.org/web/20070509080709/http://lswh.lcu.edu.cn/dbk/Resource/Book/Edu/JXCKS/TS011096/0424_ts011096.htm)

[Category:流失海外的中国文物](../Category/流失海外的中国文物.md "wikilink")
[Category:雙語辭典](../Category/雙語辭典.md "wikilink")
[Category:西夏文](../Category/西夏文.md "wikilink")
[Category:西夏文物](../Category/西夏文物.md "wikilink")
[Category:阿拉善文物](../Category/阿拉善文物.md "wikilink")
[Category:额济纳旗](../Category/额济纳旗.md "wikilink")