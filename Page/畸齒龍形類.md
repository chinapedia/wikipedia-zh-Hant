**畸齒龍形類**（Heterodontosauriformes）是群[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，屬於[鳥臀目](../Page/鳥臀目.md "wikilink")。畸齒龍形動物是由[徐星等人](../Page/徐星.md "wikilink")，在發現[隱龍之後於](../Page/隱龍.md "wikilink")2006年所建立，包含[頭飾龍類與](../Page/頭飾龍類.md "wikilink")[畸齒龍科](../Page/畸齒龍科.md "wikilink")（該科起初被分類於[鳥腳下目](../Page/鳥腳下目.md "wikilink")）\[1\]。

## 參考資料

[\*](../Category/鳥臀目.md "wikilink")

1.