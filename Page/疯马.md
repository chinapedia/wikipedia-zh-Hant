**疯马**（：Tȟašúŋke
Witkó；；\[1\]），[北美洲](../Page/北美洲.md "wikilink")[原住民](../Page/原住民.md "wikilink")[印第安人民族](../Page/印第安人.md "wikilink")[苏族的](../Page/苏族.md "wikilink")[酋長和](../Page/酋長.md "wikilink")[军事家](../Page/军事家.md "wikilink")。他以勇猛善战著称，是[北美印第安战争的靈魂人物之一](../Page/北美印第安战争.md "wikilink")。

瘋馬曾與[奧格拉拉族頭目](../Page/奧格拉拉.md "wikilink")「[紅雲](../Page/紅雲.md "wikilink")」、[薩滿](../Page/薩滿.md "wikilink")[巫師](../Page/巫師.md "wikilink")「[坐牛](../Page/坐牛.md "wikilink")」一同在美西地区抵抗[白人的入侵](../Page/白人.md "wikilink")。其參與過最有名的战役是为了保护圣地而战的[小大角战役](../Page/小大角战役.md "wikilink")。他們率領數千名[印第安人](../Page/印第安人.md "wikilink")，以優勢兵力，在黑山山谷中歼灭了知名的[美國第1騎兵師第7騎兵團兩百餘](../Page/美國第1騎兵師第7騎兵團.md "wikilink")[騎兵](../Page/騎兵.md "wikilink")，擊殺了該騎兵團指揮官[卡士達](../Page/乔治·阿姆斯特朗·卡斯特.md "wikilink")，並[伐取了其](../Page/馘首.md "wikilink")[首級](../Page/首級.md "wikilink")。

[美国政府得知消息](../Page/美国政府.md "wikilink")，停止向苏族提供食物，同时赶走草原上的[野牛](../Page/野牛.md "wikilink")，使他們無法獵食。为了保全[部落](../Page/部落.md "wikilink")，在1877年的五月初，疯马等酋長在罗宾逊军区投降，在1877年因为被指有[越獄嫌疑](../Page/越獄.md "wikilink")，遭[美軍處決](../Page/美軍.md "wikilink")。

## 参考文獻與腳註

## 延伸閱讀

  - [Ambrose, Stephen E.](../Page/Stephen_E._Ambrose.md "wikilink")
    *Crazy Horse and Custer: The epic clash of two great warriors at the
    Little Bighorn*. 1975.

  - Bray, Kingsley M. *Crazy Horse: A Lakota Life*. 2006. ISBN
    0-8061-3785-1

  - Clark, Robert. *The Killing of Chief Crazy Horse: Three Eyewitness
    Views by the Indian, Chief He Dog the Indian White, William Garnett
    the White Doctor, Valentine McGillycuddy*. 1988. ISBN 0-8032-6330-9

  - *The Journey of Crazy Horse: A Lakota History*. 2004.

  - Guttmacher, Peter and David W. Baird. Ed. *Crazy Horse: Sioux War
    Chief*. New York Philadelphia: Chelsea House, 1994. 0–120. ISBN
    0-7910-1712-5

  - . *Crazy Horse (Penguin Lives)*. Puffin Books. 1999. ISBN
    0-670-88234-8

  - Pinn, Lionel Kitpu'se. *Greengrass Pipe Dancers*. 2000. ISBN
    0-87961-250-9

  - . *The Killing of Crazy Horse*. Random House, Inc. 2010. ISBN
    978-0-375-41446-6.

  - . *Crazy Horse, the Strange Man of the Oglalas, a biography*. 1942.
    ISBN 0-8032-9211-2

  - "Debating Crazy Horse: Is this the Famous Oglala". *Whispering Wind
    magazine*, Vol 34 \#3, 2004. A discussion on the improbability of
    the Garryowen photo being that of Crazy Horse (the same photo shown
    here). The clothing, the studio setting all date the photo
    1890–1910.

  - *The Authorized Biography of Crazy Horse and His Family Part One:
    Creation, Spirituality, and the Family Tree*. DVD. William Matson
    and Mark Frethem, producers. Documentary based on over 100 hours of
    footage shot of family oral history detailed interviews and all
    Crazy Horse sites. Family had final approval on end product.
    Reelcontact.com, 2006.

  - *The Authorized Biography of Crazy Horse and His Family Part Two:
    Defending the Homeland Prior to the 1868 Treaty*'. DVD William
    Matson and Mark Frethem, Producers. Reel Contact Productions, 2007.

  - Russell Freedman, *The Life and Death of Crazy Horse*. Holiday
    House. 1996. ISBN 978-0-8234-1219-8

## 外部链接

  - [Final Days and Death of Crazy
    Horse](http://friendslittlebighorn.com/crazyhorsedeath.htm)

  - [PBS Biography of Crazy
    Horse](http://www.pbs.org/weta/thewest/people/a_c/crazyhorse.htm)

  - [Newly Discovered Alleged Photo of Crazy
    Horse](https://web.archive.org/web/20120303055536/http://www.top50states.com/aborigine-people.html)

  - [A timeline of Crazy Horse's
    life](https://web.archive.org/web/20041022074314/http://www.emayzine.com/lectures/CRAZYHOR.html)

  - [Trimble: What did Crazy Horse look
    like?](http://www.indiancountrytoday.com/archive/28164089.html),
    Indian Country Today

  - [William Bordeaux's Crazy Horse
    sketch](http://beinecke.library.yale.edu/dl_crosscollex/brbldl/oneITEM.asp?pid=2007524&iid=1039539&srchtype=)
    at Yale University's Beinecke Rare Books and Manuscript Library

  - [Complete Crazy Horse Surrender
    Ledger](http://www.astonisher.com/archives/museum/crazy_horse_surrender.html)

  -
[Category:1877年罪案](../Category/1877年罪案.md "wikilink")
[Category:拉科塔領袖](../Category/拉科塔領袖.md "wikilink")
[Category:被謀殺的美洲原住民](../Category/被謀殺的美洲原住民.md "wikilink")
[Category:南達科他州美洲原住民歷史](../Category/南達科他州美洲原住民歷史.md "wikilink")
[Category:印第安人戰爭之美洲原住民](../Category/印第安人戰爭之美洲原住民.md "wikilink")
[Category:因刺刀致死者](../Category/因刺刀致死者.md "wikilink")
[Category:美國舊西部人物](../Category/美國舊西部人物.md "wikilink")
[Category:在內布拉斯加州被謀殺身亡者](../Category/在內布拉斯加州被謀殺身亡者.md "wikilink")
[Category:小大角戰役](../Category/小大角戰役.md "wikilink")
[Category:美國軍事人物](../Category/美國軍事人物.md "wikilink")

1.  其名字意思是「他的马是疯狂的」，英文名字則是拉科塔語名字的意譯。[黑麋鹿解釋道](../Page/黑麋鹿.md "wikilink")：「『瘋馬』這名字並不表示他的馬瘋了，而是他的馬在夢中以古怪的方式跳舞」。引自黑麋鹿口述，約翰·內哈特紀錄，賓靜蓀譯《巫士．詩人．神話：黑麋鹿如是說》（台北：立緒文化，2003），頁75-76。