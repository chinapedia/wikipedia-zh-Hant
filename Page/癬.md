**癬**是指[皮癬菌滋生在](../Page/皮癬菌.md "wikilink")[皮膚或](../Page/皮膚.md "wikilink")[黏膜表面引起的一種](../Page/黏膜.md "wikilink")[皮膚病](../Page/皮膚病.md "wikilink")，例如[足癬](../Page/足癬.md "wikilink")（俗稱香港腳）、手癬等。但部分病名雖包含癬字但卻不是癬，如「[牛皮癬](../Page/牛皮癬.md "wikilink")」。

## 形成黴菌的有利條件

1.  潮濕環境
2.  衛生環境惡劣
3.  個人免疫力低
4.  患有某些疾病如糖尿病的患者亦較容易生癬

## 癬的種類

由於癬是形成在皮膚的表面，故此在身體上舉凡被皮膚覆蓋的地方都有機會生成癬，特別是皮膚比較侷促的地方如腋下、趾隙間。
常見的癬有以下各種：
\# [頭癬](../Page/頭癬.md "wikilink")

1.  [足癬](../Page/足癬.md "wikilink")
2.  手癬
3.  [股癬](../Page/股癬.md "wikilink")
4.  體癬 ([:en:Tinea corporis](../Page/:en:Tinea_corporis.md "wikilink"))
5.  [甲癬](../Page/甲癬.md "wikilink")
6.  紅癬

## 參考資料

[bcl:Bu'ni](../Page/bcl:Bu'ni.md "wikilink")
[bn:দাদ](../Page/bn:দাদ.md "wikilink")
[da:Ringorm](../Page/da:Ringorm.md "wikilink")
[es:Dermatofitosis](../Page/es:Dermatofitosis.md "wikilink")
[ky:Трихофития](../Page/ky:Трихофития.md "wikilink")
[ms:Kurap](../Page/ms:Kurap.md "wikilink")
[my:ပွေး(ရောဂါ)](../Page/my:ပွေး\(ရောဂါ\).md "wikilink")
[pt:Dermatofitose](../Page/pt:Dermatofitose.md "wikilink") [sl:Glivične
kožne bolezni](../Page/sl:Glivične_kožne_bolezni.md "wikilink")
[sv:Ringorm](../Page/sv:Ringorm.md "wikilink")
[ur:داد](../Page/ur:داد.md "wikilink")
[wa:Diermifitôze](../Page/wa:Diermifitôze.md "wikilink")

[Category:皮膚和皮下組織疾病](../Category/皮膚和皮下組織疾病.md "wikilink")
[Category:動物身上的真菌病](../Category/動物身上的真菌病.md "wikilink")