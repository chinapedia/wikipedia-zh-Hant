**癸丑**为[干支之一](../Page/干支.md "wikilink")，顺序为第50个。前一位是[壬子](../Page/壬子.md "wikilink")，后一位是[甲寅](../Page/甲寅.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之癸屬陰之水](../Page/天干.md "wikilink")，[地支之](../Page/地支.md "wikilink")-{丑}-屬陰之土，是土尅水相尅。

[太平天国时](../Page/太平天国.md "wikilink")，曾把“丑”改为“好”\[1\]。因此1853年在太平天国记为癸好。

## 癸丑年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")50年称“**癸丑年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘53，或年份數減3，除以10的餘數是0，除以12的餘數是2，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“癸丑年”：

<table>
<caption><strong>癸丑年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/53年.md" title="wikilink">53年</a></li>
<li><a href="../Page/113年.md" title="wikilink">113年</a></li>
<li><a href="../Page/173年.md" title="wikilink">173年</a></li>
<li><a href="../Page/233年.md" title="wikilink">233年</a></li>
<li><a href="../Page/293年.md" title="wikilink">293年</a></li>
<li><a href="../Page/353年.md" title="wikilink">353年</a></li>
<li><a href="../Page/413年.md" title="wikilink">413年</a></li>
<li><a href="../Page/473年.md" title="wikilink">473年</a></li>
<li><a href="../Page/533年.md" title="wikilink">533年</a></li>
<li><a href="../Page/593年.md" title="wikilink">593年</a></li>
<li><a href="../Page/653年.md" title="wikilink">653年</a></li>
<li><a href="../Page/713年.md" title="wikilink">713年</a></li>
<li><a href="../Page/773年.md" title="wikilink">773年</a></li>
<li><a href="../Page/833年.md" title="wikilink">833年</a></li>
<li><a href="../Page/893年.md" title="wikilink">893年</a></li>
<li><a href="../Page/953年.md" title="wikilink">953年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1013年.md" title="wikilink">1013年</a></li>
<li><a href="../Page/1073年.md" title="wikilink">1073年</a></li>
<li><a href="../Page/1133年.md" title="wikilink">1133年</a></li>
<li><a href="../Page/1193年.md" title="wikilink">1193年</a></li>
<li><a href="../Page/1253年.md" title="wikilink">1253年</a></li>
<li><a href="../Page/1313年.md" title="wikilink">1313年</a></li>
<li><a href="../Page/1373年.md" title="wikilink">1373年</a></li>
<li><a href="../Page/1433年.md" title="wikilink">1433年</a></li>
<li><a href="../Page/1493年.md" title="wikilink">1493年</a></li>
<li><a href="../Page/1553年.md" title="wikilink">1553年</a></li>
<li><a href="../Page/1613年.md" title="wikilink">1613年</a></li>
<li><a href="../Page/1673年.md" title="wikilink">1673年</a></li>
<li><a href="../Page/1733年.md" title="wikilink">1733年</a></li>
<li><a href="../Page/1793年.md" title="wikilink">1793年</a></li>
<li><a href="../Page/1853年.md" title="wikilink">1853年</a></li>
<li><a href="../Page/1913年.md" title="wikilink">1913年</a></li>
<li><a href="../Page/1973年.md" title="wikilink">1973年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2033年.md" title="wikilink">2033年</a></li>
<li><a href="../Page/2093年.md" title="wikilink">2093年</a></li>
<li><a href="../Page/2153年.md" title="wikilink">2153年</a></li>
<li><a href="../Page/2213年.md" title="wikilink">2213年</a></li>
<li><a href="../Page/2273年.md" title="wikilink">2273年</a></li>
<li><a href="../Page/2333年.md" title="wikilink">2333年</a></li>
<li><a href="../Page/2393年.md" title="wikilink">2393年</a></li>
<li><a href="../Page/2453年.md" title="wikilink">2453年</a></li>
<li><a href="../Page/2513年.md" title="wikilink">2513年</a></li>
<li><a href="../Page/2573年.md" title="wikilink">2573年</a></li>
<li><a href="../Page/2633年.md" title="wikilink">2633年</a></li>
<li><a href="../Page/2693年.md" title="wikilink">2693年</a></li>
<li><a href="../Page/2753年.md" title="wikilink">2753年</a></li>
<li><a href="../Page/2813年.md" title="wikilink">2813年</a></li>
<li><a href="../Page/2873年.md" title="wikilink">2873年</a></li>
<li><a href="../Page/2933年.md" title="wikilink">2933年</a></li>
<li><a href="../Page/2993年.md" title="wikilink">2993年</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 1913年 - [癸丑报灾](../Page/癸丑报灾.md "wikilink")

## 癸丑月

天干丁年和壬年，[小寒到](../Page/小寒.md "wikilink")[立春的時間段](../Page/立春.md "wikilink")，就是**癸丑月**：

  - ……
  - [1978年](../Page/1978年.md "wikilink")1月小寒到2月立春
  - [1983年](../Page/1983年.md "wikilink")1月小寒到2月立春
  - [1988年](../Page/1988年.md "wikilink")1月小寒到2月立春
  - [1993年](../Page/1993年.md "wikilink")1月小寒到2月立春
  - [1998年](../Page/1998年.md "wikilink")1月小寒到2月立春
  - [2003年](../Page/2003年.md "wikilink")1月小寒到2月立春
  - [2008年](../Page/2008年.md "wikilink")1月小寒到2月立春
  - ……

## 癸丑日

## 癸丑時

天干戊日和癸日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）1時到3時，就是**癸丑時**。

## 參考文獻

<div class="references-small">

<references />

</div>

  -
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/癸丑年.md "wikilink")

1.