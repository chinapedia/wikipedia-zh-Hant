[Noboribetsu-shi_Town_Centre_201406.JPG](https://zh.wikipedia.org/wiki/File:Noboribetsu-shi_Town_Centre_201406.JPG "fig:Noboribetsu-shi_Town_Centre_201406.JPG")
**登別市**（）為[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[膽振綜合振興局的](../Page/膽振綜合振興局.md "wikilink")[城市](../Page/城市.md "wikilink")，屬於[室蘭市的衛星城市](../Page/室蘭市.md "wikilink")，轄區內西南部的工業區屬於室蘭市週邊工業區。市內的[登別溫泉和](../Page/登別溫泉.md "wikilink")溫泉為著名的旅遊勝地。

東南海岸區域為平原，內陸為台地和丘陵，更內陸的區域為山地。人口主要集中在沿海的登別、幌別、鷲別三個地區，分別位於[登別川](../Page/登別川.md "wikilink")、[幌別川](../Page/幌別川.md "wikilink")、[鷲別川的河口](../Page/鷲別川.md "wikilink")，其中鷲別與室蘭市區相連。

名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「nupur-pet」（深色的河）\[1\]或「nupki-pet」（混濁的河）\[2\]。

## 历史

  - 1869年：[太政官指示由](../Page/太政官.md "wikilink")[仙台藩白石城主](../Page/仙台藩.md "wikilink")[片倉小十郎管理](../Page/片倉小十郎.md "wikilink")[幌別郡](../Page/幌別郡.md "wikilink")。\[3\]
  - 1873年：幌別郡設置登別村、幌別村、鷲別村三村。
  - 1874年：幌別郡設置各村戶長役場。
  - 1901年：鷲別村的部份區域分村成立[室蘭郡](../Page/室蘭郡.md "wikilink")[輪西村](../Page/輪西村.md "wikilink")（現在的[室蘭市](../Page/室蘭市.md "wikilink")）。
  - 1919年4月1日：幌別村、登別村、鷲別村[合併成立為二級村幌別村](../Page/市町村合併.md "wikilink")。
  - 1951年4月1日：改制為幌別町。
  - 1961年4月1日：改名登別町。
  - 1970年8月1日：改制為登別市。

## 行政

  - 市長：上野晃 1988年8月28日上任，為北海道各城市現任市長中在職最久者，也是現任北海道市長會會長。\[4\]

## 產業

海岸區域有大量工業區，東部有著名的登別溫泉，周邊以觀光產業為主。

## 交通

[Noboribetsu_station.jpg](https://zh.wikipedia.org/wiki/File:Noboribetsu_station.jpg "fig:Noboribetsu_station.jpg")

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [室蘭本線](../Page/室蘭本線.md "wikilink")：[鷲別車站](../Page/鷲別車站.md "wikilink")
        - [幌別車站](../Page/幌別車站.md "wikilink") -
        [富浦車站](../Page/富浦車站_\(北海道\).md "wikilink")
        - [登別車站](../Page/登別車站.md "wikilink")
  - [登別溫泉軌道](../Page/登別溫泉軌道.md "wikilink")（1915年-1933年、為[路面電車](../Page/路面電車.md "wikilink")）

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li><a href="../Page/道央自動車道.md" title="wikilink">道央自動車道</a>：<a href="../Page/登別室蘭交流道.md" title="wikilink">登別室蘭交流道</a> - <a href="../Page/富浦休息區.md" title="wikilink">富浦休息區</a> - <a href="../Page/登別東交流道.md" title="wikilink">登別東交流道</a></li>
</ul>
<dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道36號.md" title="wikilink">國道36號</a></li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道2號洞爺湖登別線（登別溫泉通）</li>
<li>北海道道144號登別室蘭內部線</li>
<li>北海道道286號登別停車場線</li>
<li>北海道道327號弁景幌別線（）</li>
<li>北海道道350號倶多樂湖公園線</li>
<li>北海道道387號幌別停車場線</li>
<li>北海道道701號登別港線（海岸通）</li>
<li>北海道道782號上登別室蘭線（富士通）</li>
</ul></td>
</tr>
</tbody>
</table>

### 巴士

  - 道南巴士
  - 北海道中央巴士

## 文化及旅遊

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="文化及運動設施">文化及運動設施</h3>
<ul>
<li>登別市民會館</li>
<li>登別市立圖書館</li>
<li>登別市鄉土資料館</li>
<li>登別市營陸上競技場</li>
<li>登別市綜合體育館</li>
</ul>
<h3 id="觀光景點">觀光景點</h3>
<ul>
<li></li>
<li><a href="../Page/登別溫泉.md" title="wikilink">登別溫泉</a>
<ul>
<li><a href="../Page/地獄谷.md" title="wikilink">地獄谷</a></li>
</ul></li>
<li>新登別溫泉（位於登別溫泉西方約1公里）</li>
<li><a href="../Page/川又溫泉.md" title="wikilink">川又溫泉</a>（無人露天溫泉）</li>
<li>登別棕熊牧場</li>
<li>登別尼克斯海洋公園</li>
<li>登別伊達時代村</li>
<li><a href="../Page/登別原始林.md" title="wikilink">登別原始林</a> （日本國家<a href="../Page/天然記念物.md" title="wikilink">天然記念物</a>）</li>
<li>花之隧道（道道2號洞爺湖登別線沿路之櫻並木）</li>
</ul></td>
<td><h3 id="節慶">節慶</h3>
<ul>
<li><a href="../Page/漬物祭.md" title="wikilink">漬物祭</a>（1月）</li>
<li>登別溫泉湯祭（2月）</li>
<li>鯉魚旗馬拉松（5月5日）</li>
<li>豐水祭（7月）</li>
<li><a href="../Page/登別地獄祭.md" title="wikilink">登別地獄祭</a>（8月）</li>
<li>刈田神社祭典（9月）</li>
<li>登別漁港祭（9月）</li>
</ul></td>
</tr>
</tbody>
</table>

## 教育

### 高等學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>道立高等学校
<ul>
<li><a href="http://www.n-seiryo.hokkaido-c.ed.jp/">北海道登別青嶺高等學校</a></li>
<li><a href="https://web.archive.org/web/20070705172222/http://www.nobokou.hokkaido-c.ed.jp/">北海道登別高等學校</a>（已於2007年3月廢校）</li>
</ul></li>
</ul></td>
<td><ul>
<li>私立高等學校
<ul>
<li><a href="https://web.archive.org/web/20060712202812/http://www2.ocn.ne.jp/~nohtani/">登別大谷高等學校</a></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

### 中高一貫

  - [北海道登別明日中等教育學校](http://www.akebi.hokkaido-c.ed.jp/)（2007年4月開始招生）

### 專門學校

  - [日本工學院](../Page/日本工學院.md "wikilink")[北海道專門學校](https://web.archive.org/web/20070430180157/http://www.neec.ac.jp/cie/tw/)

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="http://www.noboribetsu.ed.jp/~jh_info/">登別市立幌別中學校</a></li>
<li><a href="http://www.noboribetsu.ed.jp/~js_info/">登別市立西陵中學校</a></li>
<li>登別市立鷲別中學校</li>
</ul></td>
<td><ul>
<li><a href="http://www.noboribetsu.ed.jp/~jn_info/">登別市立登別中學校</a></li>
<li><a href="http://www.noboribetsu.ed.jp/~jr_info/">登別市立綠陽中學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="http://www.noboribetsu.ed.jp/~hr_info/">登別市立幌別小學校</a></li>
<li><a href="http://www.noboribetsu.ed.jp/~hn_info/">登別市立幌別西小學校</a></li>
<li><a href="http://www.noboribetsu.ed.jp/~hh_info/">登別市立幌別東小學校</a></li>
<li><a href="http://www.noboribetsu.ed.jp/~ao_info/">登別市立青葉小學校</a></li>
<li><a href="http://www.noboribetsu.ed.jp/~ws_info/">登別市立鷲別小學校</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.noboribetsu.ed.jp/~wk_info/">登別市立若草小學校</a></li>
<li><a href="http://www.noboribetsu.ed.jp/~nb_info/">登別市立登別小學校</a></li>
<li><a href="https://web.archive.org/web/20060828224900/http://www.noboribetsu.ed.jp/%7Eon_info/">登別市立登別溫泉小學校</a></li>
<li><a href="http://www.noboribetsu.ed.jp/~tn_info/">登別市立富岸小學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [白石市](../Page/白石市.md "wikilink")（[宮城縣](../Page/宮城縣.md "wikilink")）

### 海外

  - [廣州市](../Page/廣州市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")
    [廣東省](../Page/廣東省.md "wikilink")）：締結於2002年5月19日。

## 本地出身的名人

  - [Non](../Page/Non.md "wikilink")：[歌手](../Page/歌手.md "wikilink")
  - [相原コージ](../Page/相原コージ.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
  - [吉田兄弟](../Page/吉田兄弟.md "wikilink")
  - [知里幸恵](../Page/知里幸恵.md "wikilink")
  - [知里真志保](../Page/知里真志保.md "wikilink")：語言學者
  - [長嶋有](../Page/長嶋有.md "wikilink")：芥川賞作家
  - [生田斗真](../Page/生田斗真.md "wikilink")

## 參考資料

## 外部連結

  - [登別市觀光資訊網站](http://www.city.noboribetsu.hokkaido.jp/index.html)

  - [登別溫泉觀光網站](http://www.expandbiz.net/noboribetu/index.html)

  - [登別溫泉觀光協會](http://www.noboribetsu-spa.jp/)

  - [カルルス旅館公會](https://web.archive.org/web/20060616185417/http://karls.hp.infoseek.co.jp/)

  - [登別棕熊牧場](https://web.archive.org/web/20060717115300/http://www.kamori.co.jp/bearpark/)

      - [登別棕熊牧場](https://web.archive.org/web/20071008053814/http://www.kamori.co.jp/bearpark/part/traditional_chinese.pdf)中文介紹

  - [登別尼克斯海洋公園](http://www.nixe.co.jp/)

  - [登別伊達時代村](https://web.archive.org/web/20041204175851/http://www.jidaimura.co.jp/web2003/nobori/)

  - [登別市立圖書館](http://library.city.noboribetsu.hokkaido.jp/opac/)

  - [登別商工會議所](http://www.noboribetsu.cci.or.jp/)

  - [日本國土地理院
    地形圖閱覽系統 2萬5千分1地形圖：室蘭東北區域](http://mapbrowse.gsi.go.jp/cgi-bin/nph-mm.cgi?mesh=6341404&res=0&offset=-2&method=d?1573,1)

<!-- end list -->

1.
2.  登別市官方網頁 - 概要 <http://www.city.noboribetsu.hokkaido.jp/index.html>
3.  登別市官方網頁 - 資料 <http://www.city.noboribetsu.hokkaido.jp/sisei.html>
4.  北海道市長會 - 市長名簿<http://www.hk-mayors.gr.jp/>