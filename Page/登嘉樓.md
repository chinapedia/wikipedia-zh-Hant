**登嘉楼**（），旧称**丁加奴**，是[馬來西亞位於](../Page/馬來西亞.md "wikilink")[西馬的州属](../Page/馬來半島.md "wikilink")，東臨[南中國海](../Page/南中國海.md "wikilink")。西北部與[吉蘭丹州交界](../Page/吉蘭丹.md "wikilink")，西南部有[彭亨州](../Page/彭亨.md "wikilink")。登嘉楼[面积](../Page/面积.md "wikilink")12,955[平方公里](../Page/平方公里.md "wikilink")，人口120萬人，首府是[瓜拉登嘉楼](../Page/瓜拉登嘉楼.md "wikilink")。

人口比例爲：[馬來人佔](../Page/馬來人.md "wikilink")94%（859,402人）、[華人佔](../Page/華人.md "wikilink")5%（42,970人）、印度人4,355人、其他3,238人（1995年统计数字）。

## 名称演变

登嘉楼之[马来文](../Page/马来文.md "wikilink")《爪哇史頌》本为Trengganu，[葡萄牙人稱其為Talagano](../Page/葡萄牙人.md "wikilink")，后来改成Terengganu。

[华文名称方面](../Page/华文.md "wikilink")，中国《[汉书](../Page/汉书.md "wikilink")》称为[都元国](../Page/都元国.md "wikilink")，《[康泰吴时外国传](../Page/康泰吴时外国传.md "wikilink")》和《[水经注](../Page/水经注.md "wikilink")》称为[屈都乾](../Page/屈都乾.md "wikilink")，[宋代](../Page/宋代.md "wikilink")《[諸蕃志](../Page/諸蕃志.md "wikilink")》中称**登牙儂**。[元代](../Page/元代.md "wikilink")《[島夷志略](../Page/島夷志略.md "wikilink")》称**丁家廬**，元代《大德南海志》作丁茄蘆，[明代](../Page/明代.md "wikilink")《[鄭和航海圖](../Page/鄭和航海圖.md "wikilink")》中作**丁家下路**，《[明史](../Page/明史.md "wikilink")》称为丁机宜，清代《海國見聞錄》作丁葛奴\[1\]。

1957年8月31日[馬來亞聯合邦自](../Page/馬來亞聯合邦.md "wikilink")[英国殖民者手中独立以来](../Page/英国.md "wikilink")，Terengganu的中文译名一直都是丁加奴，包括马来西亚所有中小学的华文教科书都用丁加奴。20世紀70年代，当地华人开始用登嘉楼这名称，当地一些华族会馆有用此名，例如“登嘉楼海南会馆”，“登嘉楼福建会馆”等，另外当地商家及各华团多用登嘉楼。

2004年9月，当地华人以丁加奴此名带有贬义，即添“丁”“加”做“奴”才为由，正式向[马来西亚华语规范理事会要求改名为登嘉楼](../Page/马来西亚华语规范理事会.md "wikilink")，有“登”上更“嘉”一层“楼”之意。\[2\]

某些人对此事有其他意见，；例如有者认为“登嘉楼”三字笔划多，书写不便，建议沿用原来的“丁加”二字，把“奴”改成“努”，也有“努力”的含意。

2005年4月8日，[马来西亚华语规范理事会经过多次开会讨论后正式宣布](../Page/马来西亚华语规范理事会.md "wikilink")，决定使用“登嘉楼”，州首府[瓜拉丁加奴则改称](../Page/瓜拉丁加奴.md "wikilink")[瓜拉登嘉楼](../Page/瓜拉登嘉楼.md "wikilink")，而全马来西亚的中小学华文教科书也将逐渐改名。

## 历史

登嘉楼最早的記載出現於中國历史典籍和商人與航海家的紀錄。《[汉书](../Page/汉书.md "wikilink")》称为[都元国](../Page/都元国.md "wikilink")，《[康泰吴时外国传](../Page/康泰吴时外国传.md "wikilink")》和《[水经注](../Page/水经注.md "wikilink")》称为[屈都乾](../Page/屈都乾.md "wikilink")，[都乾都是](../Page/都乾.md "wikilink")[马来西亚](../Page/马来西亚.md "wikilink")[登嘉楼濒海县城](../Page/登嘉楼.md "wikilink")
Kuala Dungan 之对音；都乾是dungun 的对音，马来语中羊味果之意。\[3\]

《[汉书·地理志](../Page/汉书.md "wikilink")》中有记载：“自日南障塞、徐闻、合浦，船行可五月，有[都元国](../Page/都元国.md "wikilink")”

《[康泰吴时外国传](../Page/康泰吴时外国传.md "wikilink")》记载

《[水经注](../Page/水经注.md "wikilink")》引晋书地道记

中國宋代《[諸蕃志](../Page/諸蕃志.md "wikilink")》中的登牙儂。14世紀初葉，[航海家](../Page/航海家.md "wikilink")[汪大淵造訪此地](../Page/汪大淵.md "wikilink")，在所著《[島夷志略](../Page/島夷志略.md "wikilink")》一書有專篇《丁家廬》記載：“山高曠，田中下，民足食，春多雨，氣候微熱，男女椎髻，穿綠頡布短衫，繫遮裡絹。……產降真、黃蠟、玳瑁。”“风俗尚怪。男女椎鬌,穿绿颉布短衫,系遮里绢。刻木为神,杀人血和酒祭之。每水旱疫疠,祷之立应。及婚姻病丧,则卜其吉凶,亦验。今酋长主事贪禁,勤俭守土。地产降真、脑子、黄蜡、玳瑁。货用青白花磁器、占城布、小红绢、斗锡、酒之厲”。15世紀《[鄭和航海圖](../Page/鄭和航海圖.md "wikilink")》中作「丁家下路」。《明史》卷三二五作「丁機宜」：“丁机宜，爪哇属国也，幅员甚狭，仅千余家。柔佛黠而雄，丁机宜与接壤，时被其患。后以厚币求婚，稍获宁处。其国以木为城。酋所居，旁列钟鼓楼，出入乘象。以十月为岁首。性好洁，酋所食啖，皆躬自割烹。民俗类爪哇，物产悉如柔佛。酒禁甚严，有常税。然大家皆不饮，维细民无籍者饮之，其曹偶咸非笑。婚者，男往女家持其门户，故生女胜男。丧用火葬。华人往商，交易甚平”。

一如馬來其他的州屬，丁加奴在伊斯蘭教未傳入前，是佛教與印度教的文化混合[萬物有靈論的傳統信仰社會](../Page/萬物有靈論.md "wikilink")。8世紀左右，丁加奴屬於[三佛齊的勢力下](../Page/三佛齊.md "wikilink")，1365年成為[滿者伯夷帝國的籓屬](../Page/滿者伯夷.md "wikilink")，15世紀成為[滿剌加蘇丹國](../Page/滿剌加蘇丹國.md "wikilink")（馬六甲王朝）的一部份，16世紀臣服於[暹羅](../Page/暹羅.md "wikilink")。18世紀出現首位[蘇丹](../Page/苏丹_\(称谓\).md "wikilink")。

1909年，[大英帝國取代暹羅取得了對丁加奴的控制](../Page/大英帝國.md "wikilink")，屬於大英帝國不列顛之保護領。而後丁加奴成為大英帝國的[馬來屬邦之一](../Page/馬來屬邦.md "wikilink")。1928年發生人民起義反抗英國統治。二戰期間，馬來亞為日本佔領，日本政府將[吉蘭丹](../Page/吉蘭丹.md "wikilink")、[吉打](../Page/吉打.md "wikilink")、[玻璃市](../Page/玻璃市.md "wikilink")、丁加奴歸入暹羅的一部份。而後日軍挫敗，上述馬來亞各邦再度回到英國控制之下。1948年丁加奴成為[馬來亞聯合邦的一部份](../Page/馬來亞聯合邦.md "wikilink")。1957年成為獨立的[馬來亞的一州](../Page/馬來亞.md "wikilink")。

2005年4月8日，中文名稱改為登嘉楼。2006年12月13日，登嘉楼苏丹[端姑米占·再纳·阿比丁继位成为](../Page/端姑米占·再纳·阿比丁.md "wikilink")[马来西亚最高元首](../Page/马来西亚最高元首.md "wikilink")。

## 行政区划

  - （）

  - （）

  - （）

  - （）

  - [瓜拉登嘉楼县](../Page/瓜拉登嘉楼.md "wikilink")（）

  - [马江县](../Page/马江县.md "wikilink")（）

  - （）

  - （） (从瓜登脱离自立为县)\[4\]

## 华文中小学列表

### 华文中学

  - 登嘉楼[中华维新华文中学](../Page/中华维新华文中学.md "wikilink") SMJK Chung Hwa Wei Sin

### 华文小学

  - 启蒙华文小学 SJK (C) CHEE MONG
  - 中华华文小学 SJK (C) CHONG HWA
  - 甘马挽朱盖华文小学 SJK (C) CHUKAI
  - 中华华文小学 SJK (C) CHUNG HWA
  - 中华维新华文小学 SJK (C) CHUNG HWA WEI SIN
  - 雅姆华文小学 SJK (C) JABOR
  - 甘马挽港口华文小学 SJK (C) KUALA KEMAMAN
  - 光华华文小学 SJK (C) KWANG HWA
  - 乐群华文小学 SJK (C) LOK KHOON
  - 五里新村华文小学 SJK (C) SIN CHONE

## 参考资料

## 參看

  - [实兆湿地](../Page/实兆湿地.md "wikilink")（Setiu Wetland）：登嘉樓的著名濕地及生態景點。

## 外部链接

  -
  - [登嘉楼州地图](http://ditu.ps123.net/world/UploadFile/201312/2013121320103118.jpg)

[\*](../Category/登嘉楼.md "wikilink")
[登](../Category/马来西亚州属.md "wikilink")

1.  杨松年 《战前新马文学本地意识到形成与发展》第6页 Word Scientific, Singapore
2.
3.  [许云樵辑注](../Page/许云樵.md "wikilink")《[康泰吴时外国传](../Page/康泰吴时外国传.md "wikilink")·屈都乾》
    28页 新加坡南洋研究所 1971
4.  [Kuala Nerus daerah
    kelapan](http://www.utusan.com.my/utusan/Dalam_Negeri/20140919/dn_01/Kuala-Nerus-daerah-kelapan)