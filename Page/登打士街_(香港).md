[Dundas_Street_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Dundas_Street_\(Hong_Kong\).jpg "fig:Dundas_Street_(Hong_Kong).jpg")的路段，左方為[廣華醫院](../Page/廣華醫院.md "wikilink")。\]\]
**登打士街**（），是[香港的一條著名街道](../Page/香港.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[油麻地及](../Page/油麻地.md "wikilink")[旺角交界](../Page/旺角.md "wikilink")。道路東接[窩打老道](../Page/窩打老道.md "wikilink")，西接[渡船街](../Page/渡船街_\(香港\).md "wikilink")，與[豉油街及](../Page/豉油街.md "wikilink")[碧街等街道平行](../Page/碧街.md "wikilink")。傳統上，登打士街亦是[油麻地和](../Page/油麻地.md "wikilink")[旺角的分界線](../Page/旺角.md "wikilink")，以南屬油尖區，以北屬旺角。早年，登打士街近[彌敦道是](../Page/彌敦道.md "wikilink")1920-30年代開業的東方煙廠（Orient
Tobacco Manufactory of
Hongkong）故址，位置是今天[信和中心和兆萬中心的所在地](../Page/信和中心.md "wikilink")\[1\]\[2\]，附近的[煙廠街即因東方煙廠而得名](../Page/煙廠街.md "wikilink")。

## 特色

登打士街以樓上[咖啡店而著名於香港](../Page/咖啡店.md "wikilink")，為樓上咖啡店的集中地。初時那些咖啡店集中在近[西洋菜南街盡頭及](../Page/西洋菜街.md "wikilink")[通菜街](../Page/通菜街.md "wikilink")（[女人街一段](../Page/女人街.md "wikilink")）盡頭一帶。然而現時咖啡店已開始蔓延到[彌敦道以西一段的登打士街](../Page/彌敦道.md "wikilink")\[3\]。

另一方面，登打士街後半段（[廣華醫院段](../Page/廣華醫院.md "wikilink")），向來為街頭小食的木人巷，因此成為深受香港年青人歡迎的地方之一。而該地段的租金及成交價亦能媲美[銅鑼灣](../Page/銅鑼灣.md "wikilink")[波斯富街名店區](../Page/波斯富街.md "wikilink")，其中2011年登打士街43H號地下雙號數，實用面積只有約63方呎的商舖，以4,000萬成交，實用面積呎價達65萬元，成為旺角「舖王」，全港計排行第3\[4\]。

到2013年下半年，該地段的業主因叫租進取，空置舖位驟增。據《[經濟日報](../Page/經濟日報.md "wikilink")》記者2013年10月直擊，每隔數個門牌便出現1個空置舖位，合共至少8個，以該段的街舖約50多間計，空置舖位佔比達16%。有該區地產代理認為叫價太高加上定位錯誤，預料租金已經見頂\[5\]。

## 沿路地點

  - [潮流特區](../Page/潮流特區.md "wikilink")
  - [兆萬中心](../Page/兆萬中心.md "wikilink")
  - [家樂坊](../Page/家樂坊.md "wikilink")
  - [登打士廣場](../Page/登打士廣場.md "wikilink")
  - [廣華醫院](../Page/廣華醫院.md "wikilink")
  - [好景商業中心](../Page/好景商業中心.md "wikilink")

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")：

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")、<font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[油麻地站A](../Page/油麻地站.md "wikilink")1、A2出口、[旺角站E](../Page/旺角站.md "wikilink")1、E2出口

## 參考來源

[Category:油麻地街道](../Category/油麻地街道.md "wikilink")
[Category:旺角街道](../Category/旺角街道.md "wikilink")
[Category:香港特色街道](../Category/香港特色街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")

1.  東方煙廠舊址 facebook專頁歷史時空
    \[www.facebook.com/2021670614786179/photos/a.2021727131447194/2398807783739125/?type=3\&theater\]\[www.facebook.com/2021670614786179/photos/a.2021727131447194/2398807820405788/?type=3\&theater\]
2.
3.
4.
5.