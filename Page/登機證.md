[[高麗航空新型電腦化的登機證](../Page/高麗航空.md "wikilink")，載有二維條碼(使用[PDF417條碼](../Page/PDF417條碼.md "wikilink")，為IATA標準)|thumb](https://zh.wikipedia.org/wiki/File:Air_Koryo_boarding_card.JPG "fig:高麗航空新型電腦化的登機證，載有二維條碼(使用PDF417條碼，為IATA標準)|thumb")

**登機證**是一種提供[民用航空乘客登機](../Page/民用航空.md "wikilink")-{}-證明的票證，通常是在乘客辦理完[登機報到手續後由](../Page/登機報到.md "wikilink")[航空公司發給](../Page/航空公司.md "wikilink")。登機證上，至少會標明乘客姓名、班機號碼、搭機日期和時間。一般登機證為長條狀[感熱紙形式](../Page/感熱紙.md "wikilink")，背面有[磁條供自動讀碼機讀取](../Page/磁條.md "wikilink")。有部份航空公司，特別是[廉價航空公司](../Page/廉價航空公司.md "wikilink")，為降低用紙成本，改以普通紙列印登機證，並以[二維條碼取代紙背的磁條](../Page/二維條碼.md "wikilink")，亦鼓勵乘客於航空公司網站上線上報到並自行列印登機證。

部份航空公司亦以登機證兼作[機場貴賓室的邀請卡](../Page/機場貴賓室.md "wikilink")，供[頭等艙](../Page/頭等艙.md "wikilink")、[商務艙或其他符合資格旅客進入貴賓室使用](../Page/商務艙.md "wikilink")。

多數機場和航空公司都備有自動讀碼機，以便在[空橋或登機門核對乘客的登機證](../Page/空橋.md "wikilink")，同時讀碼機也會將資料傳送到航空公司的資料庫，確認乘客是否登機以及其行李是否在機上，提升了登機的效率，避免乘客上錯飛機。

## 電子登機證

[Mobile_boarding_pass_KLM.JPG](https://zh.wikipedia.org/wiki/File:Mobile_boarding_pass_KLM.JPG "fig:Mobile_boarding_pass_KLM.JPG")2010年啟用的電子登機證和紙质登機證\]\]

### 概要

許多航空公司正逐漸開始使用電子登機證。乘客若欲以電子登機證登機，須事先於[網路或](../Page/网络.md "wikilink")[行動通訊裝置辦理報到](../Page/手机.md "wikilink")，之後便會收到一份由航空公司回傳、載有專屬[二維條碼的](../Page/二維條碼.md "wikilink")[簡訊或](../Page/簡訊.md "wikilink")[電子郵件](../Page/電子郵件.md "wikilink")，以替代紙本登機證。此外，部分航空公司也開發了專有的[行動應用程式供乘客辦理自助報到](../Page/行動應用程式.md "wikilink")，在應用程式中直接顯示登機證。\[1\]\[2\]

電子登機證上載有的[條碼與標準紙本登機證的條碼相同](../Page/條碼.md "wikilink")，可供機器讀取。登機門的[地勤人員只要掃描裝置上的](../Page/地勤.md "wikilink")[條碼即可](../Page/條碼.md "wikilink")。

### 上線的航空公司

目前使用電子登機證的航空公司包括[聯合航空](../Page/聯合航空.md "wikilink")、[亞洲航空](../Page/亞洲航空.md "wikilink")（第一個使用簡訊電子登機證）、[新加坡航空](../Page/新加坡航空.md "wikilink")、[加拿大航空](../Page/加拿大航空.md "wikilink")、[西捷航空](../Page/西捷航空.md "wikilink")（北美第一個使用）、[國泰航空](../Page/國泰航空.md "wikilink")、[達美航空](../Page/達美航空.md "wikilink")、[捷藍航空](../Page/捷藍航空.md "wikilink")、[美國航空等等](../Page/美國航空.md "wikilink")。[臺灣兩大航空公司](../Page/臺灣.md "wikilink")[中華航空與](../Page/中華航空.md "wikilink")[長榮航空也在](../Page/長榮航空.md "wikilink")2012年推出電子登機證服務。[日本則在國內線試辦針對常客發行類似](../Page/日本.md "wikilink")[Suica的電子登機卡](../Page/Suica.md "wikilink")。

也有航空公司支援使用[Apple公司的](../Page/Apple.md "wikilink")[Wallet載具](../Page/Wallet_\(iOS\).md "wikilink")。
[Electronic_boarding_pass_of_Air_China_flight_on_iPhone_7.jpg](https://zh.wikipedia.org/wiki/File:Electronic_boarding_pass_of_Air_China_flight_on_iPhone_7.jpg "fig:Electronic_boarding_pass_of_Air_China_flight_on_iPhone_7.jpg")
[iPhone 7](../Page/iPhone_7.md "wikilink") 中
[Wallet](../Page/Wallet.md "wikilink")
应用中的[中国国际航空公司的电子登机牌](../Page/中国国际航空公司.md "wikilink")。\]\]

### 優點

  - 實用性：旅客可能沒有[印表機印出紙本登機證](../Page/印表機.md "wikilink")，透過行動裝置登機證可免去在機場自助報到機前排隊的困擾。
  - 安全性：二維條碼與傳統的一維條碼相較之下較不易偽造\[3\]。
  - 便利性：旅客若無託運[行李](../Page/行李.md "wikilink")，可憑該條碼直接通關、登機，省去在[機場報到櫃檯排隊辦理報到手續的時間](../Page/機場.md "wikilink")。
  - 環保：省去紙质登機證的用紙，相當環保。

### 缺點

電子登機證也有其缺點，行動裝置電量不足無法開機時即無法驗證登機證，有時讀取設備也會無法辨識電子登機證。\[4\]對於有多位乘客的訂位紀錄而言，使用電子登機證可能會相當不便，因為多數航空公司的應用程式無法處理多張電子登機證。

## 參考文獻

## 參見

  - [機票](../Page/機票.md "wikilink")
  - [電子機票](../Page/電子機票.md "wikilink")
  - [二次安检](../Page/二次安检.md "wikilink")

[Category:民用航空](../Category/民用航空.md "wikilink")
[Category:票券](../Category/票券.md "wikilink")

1.  <http://www.nytimes.com/2008/03/18/technology/18check.html?_r=2>

2.  <http://www.letsflycheaper.com/blog/mobile-boarding-pass/>

3.  <http://travel.nytimes.com/2011/11/06/travel/testing-e-boarding-passes.html?ref=travel>

4.