**發紺**（，「紺」音「gàn」），或稱**紫紺**、**蒼藍症**，是因在接近[皮膚表面的](../Page/皮膚.md "wikilink")[血管出現脫](../Page/血管.md "wikilink")[氧後的](../Page/氧.md "wikilink")[血紅蛋白](../Page/血紅蛋白.md "wikilink")，令皮膚或[黏膜帶](../Page/黏膜.md "wikilink")[青色的徵狀](../Page/青色.md "wikilink")。根據Lundsgaard和Van
Slyke的著作\[1\]，當去氧[血紅素的濃度達到](../Page/血紅素.md "wikilink")5.0
g/dL以上時，就會開始發紺\[2\]，但常常被忽略的是，這個數字是從動脈及周圍靜脈的血氧濃度估計來的。\[3\]因為[缺氧的評估是以動脈血氣分析或是脈搏血氧飽和度](../Page/缺氧.md "wikilink")，可能會有高估的情形，去氧血紅素濃度達到2.0
g/dL是比較可靠會有發紺的條件\[4\]不過是否出現發紺和去氧血紅素絕對的量有關，若血紅素的量比[貧血下的量要多很多](../Page/貧血.md "wikilink")，發紺的青色會更加明顯。若膚色較深的人，比較不容易看到發紺的情形。若發紺的症狀剛開始在嘴唇或是手指出現，需在三至五分鐘內進行干預，因為發紺可能是因為[缺氧或嚴重的循環衰竭所造成](../Page/缺氧.md "wikilink")。

## 定義

發紺被定義為帶藍色的變色，尤其是皮膚和黏膜，由於脫氧血紅蛋白引起的脫氧血濃度過高。

發紺分為兩種主要類型：中心性發紺和末梢性發紺。

## 種類

發紺可以是在[手指](../Page/手指.md "wikilink")，包括[指甲](../Page/指甲.md "wikilink")，及其他四肢部位（稱為「末梢性發紺」），或是在[嘴唇及面部](../Page/嘴唇.md "wikilink")（稱為「中心性發紺」）。

### 中心性發紺

中心性發紺，或稱中央性發紺是因[空氣循環或通風的問題](../Page/空氣.md "wikilink")，導致在[肺部缺少](../Page/肺部.md "wikilink")[氧合的](../Page/氧合.md "wikilink")[血液](../Page/血液.md "wikilink")，或是因在[皮膚表面](../Page/皮膚.md "wikilink")[血管內血液循環的減慢](../Page/血管.md "wikilink")，令較多[氧氣被抽離](../Page/氧氣.md "wikilink")。激烈的發紺可以引起[窒息或](../Page/窒息.md "wikilink")[哽塞](../Page/哽塞.md "wikilink")，是最明顯呼吸被阻礙的徵狀。發紺的基本原理是脫氧的[血紅蛋白呈現](../Page/血紅蛋白.md "wikilink")[青色](../Page/青色.md "wikilink")，而[血管收縮更令情況明顯](../Page/血管收縮.md "wikilink")。因此，[缺氧導致](../Page/缺氧.md "wikilink")[嘴唇及其他](../Page/嘴唇.md "wikilink")[黏膜呈現青色](../Page/黏膜.md "wikilink")。

其他亦會引起發紺徵狀的疾病：

  - [肺部疾病](../Page/肺部.md "wikilink")，如[肺炎](../Page/肺炎.md "wikilink")、[肺氣腫](../Page/肺氣腫.md "wikilink")、[重症肺結核](../Page/肺結核.md "wikilink")、[肺梗塞](../Page/肺梗塞.md "wikilink")、[肺水腫](../Page/肺水腫.md "wikilink")、[氣胸和大量](../Page/氣胸.md "wikilink")[胸膜腔積液等](../Page/胸膜腔.md "wikilink")。
  - [呼吸道阻塞](../Page/呼吸道.md "wikilink")，如[哮喘](../Page/哮喘.md "wikilink")、[喉炎](../Page/喉炎.md "wikilink")、[白喉](../Page/白喉.md "wikilink")、[氣管異物或瘤等](../Page/氣管異物.md "wikilink")。
  - [心臟疾病](../Page/心臟.md "wikilink")，如[心臟病](../Page/心臟病.md "wikilink")、[心力衰竭](../Page/心力衰竭.md "wikilink")、[動脈或](../Page/動脈.md "wikilink")[靜脈阻塞等](../Page/靜脈.md "wikilink")。
  - [血液中含有](../Page/血液.md "wikilink")[變性血紅蛋白或](../Page/變性血紅蛋白.md "wikilink")[硫血紅蛋白](../Page/硫血紅蛋白.md "wikilink")。

### 末梢性發紺

末梢性發紺是在四肢或末端的發紺，成因是[微血管內](../Page/微血管.md "wikilink")[血液循環不良](../Page/血液循環.md "wikilink")，例如在天氣冷時手會出現發紺。另外，情緒緊張、驚嚇、[休克等狀況下](../Page/休克.md "wikilink")，亦可以導致末梢性發紺，[皮膚會冰冷發黑](../Page/皮膚.md "wikilink")。但是，這種徵狀並非長久，有時只要在正常體溫下就能回復。

但是，現時有一種稱為「」（或「手足紫紺症」）的病症，它有著末梢性發紺的徵狀，但若不適當處理這種病症是會致命的。

## 診斷方法

以雙目視之

## 內部連結

  -
  - [藍嬰症](../Page/藍嬰症.md "wikilink")

## 外部連結

  - [eMedicine](http://www.emedicine.com/med/topic3002.htm)
  - [Health-cares.net](https://web.archive.org/web/20061230164533/http://skin-care.health-cares.net/cyanosis.php)
  - [Texas Heart
    Institute](https://web.archive.org/web/20060515151443/http://www.tmc.edu/thi/cyanosis.html)

## 參考文獻

[Category:症狀](../Category/症狀.md "wikilink")

1.  Lundsgaard C, Van Slyke DD. Cyanosis. Medicine. 2(1):1-76, February
    1923.
2.
3.  Cyanosis. Lundsgaard C, Van SD, Abbott ME. Cyanosis. Can Med Assoc J
    1923 Aug;13(8):601-4.
4.  Goss GA, Hayes JA, Burdon JG. Deoxyhaemoglobin concentrations in the
    detection of central cyanosis. Thorax 1988 Mar;43(3):212-3.