**Audio-Animatronics®**又称为**发声机械动画人偶**、**电子音响动作装置**、**音动电子御制**，是[華特迪士尼幻想工程的一個版权注册](../Page/華特迪士尼幻想工程.md "wikilink")[商標和独门技术](../Page/商標.md "wikilink")。這些發聲機動實際上是一些機械人，它們身體能運動和發出聲音，但奇妙的是他们的所有动作都能和音乐、说话完美契合。例如一个人偶说“哦～啊”的发音时，他的嘴型也会同样呈现o、a的发音状态，动作和说话连成一气、时间分秒不差，仿佛是真实的人类在说话一样的栩栩如生、活灵活现的感觉。但这个技术不但可以用于人类形态的人偶，还可以用在动物、卡通人物、机器人、怪兽、妖精等其他电影角色上。

## 使用發聲機動的迪士尼遊樂設施

### [迪士尼樂園度假區](../Page/迪士尼樂園度假區.md "wikilink")

#### [迪士尼樂園](../Page/迪士尼樂園.md "wikilink")

  - **[美國小鎮大街](../Page/美國小鎮大街.md "wikilink")**
      - [Great Moments with Mr.
        Lincoln](../Page/Great_Moments_with_Mr._Lincoln.md "wikilink")
        (on hiatus; return date unknown)
  - **[探險世界](../Page/探險世界.md "wikilink")**
      - [Walt Disney's Enchanted Tiki
        Room](../Page/Walt_Disney's_Enchanted_Tiki_Room.md "wikilink")
      - [Indiana Jones Adventure: Temple of the Forbidden
        Eye](../Page/Indiana_Jones_Adventure.md "wikilink")
      - [Jungle Cruise](../Page/Jungle_Cruise.md "wikilink")
  - **[新奧爾良廣場](../Page/新奧爾良廣場.md "wikilink")**
      - [幽靈公館](../Page/幽靈公館.md "wikilink")
      - [加勒比海盜](../Page/加勒比海盜.md "wikilink")
      - [Club 33](../Page/Club_33.md "wikilink")
  - **[邊域世界](../Page/邊域世界.md "wikilink")**
      - Mark Twain Riverboat (*since removed*)
      - [巨雷山](../Page/巨雷山.md "wikilink")
      - Mine Train Through Nature's Wonderland (*since removed*)
  - **[動物天地](../Page/動物天地.md "wikilink")**
      - [飛濺山](../Page/飛濺山.md "wikilink")
      - [Country Bear
        Jamboree](../Page/Country_Bear_Jamboree.md "wikilink") (*since
        removed*)
  - **[幻想世界](../Page/幻想世界.md "wikilink")**
      - [小小世界](../Page/小小世界.md "wikilink")
      - [Matterhorn Bobsleds](../Page/Matterhorn_Bobsleds.md "wikilink")
      - [愛麗絲夢遊仙境](../Page/愛麗絲夢遊仙境.md "wikilink")

（注意：幻想世界只有這三個遊樂設施包含人形木偶。其他幻想世界遊樂設施包含的是較為便宜的'moving mannequin'
technique。）

  - **[米奇卡通城](../Page/米奇卡通城.md "wikilink")**
      - [兔子羅傑卡通轉轉車](../Page/兔子羅傑卡通轉轉車.md "wikilink")
  - **[明日世界](../Page/明日世界.md "wikilink")**
      - [Star Tours](../Page/Star_Tours.md "wikilink")
      - [巴斯光年星際歷險](../Page/巴斯光年星際歷險.md "wikilink")
      - [Finding Nemo Submarine
        Voyage](../Page/Finding_Nemo_Submarine_Voyage.md "wikilink")
      - [Submarine Voyage thru Liquid
        Space](../Page/Submarine_Voyage.md "wikilink") (*since removed*)
      - [Innoventions](../Page/Innoventions.md "wikilink")
      - [America Sings](../Page/America_Sings.md "wikilink") (*since
        removed*)
      - Flight to the Moon (*since removed*)
      - [Mission to Mars](../Page/Mission_to_Mars.md "wikilink")(*since
        removed*)
      - [Carousel of
        Progress](../Page/Carousel_of_Progress.md "wikilink") (*since
        moved to Walt Disney World's Magic Kingdom*)
  - **巡遊**
      - [華特迪士尼夢想巡遊](../Page/華特迪士尼夢想巡遊.md "wikilink")

#### [迪士尼加州冒險](../Page/迪士尼加州冒險.md "wikilink")

  - **A Bug's Land**
      - [It's Tough to be a
        Bug\!](../Page/It's_Tough_to_be_a_Bug!.md "wikilink")
  - **荷里活制片廠**
      - [Jim Henson's Muppet\*Vision
        3D](../Page/Jim_Henson's_MuppetVision_3D.md "wikilink")
      - [怪獸公司：大眼仔與毛毛的拯救！](../Page/怪獸公司：大眼仔與毛毛的拯救！.md "wikilink")
      - [活生生恐龍](../Page/活生生恐龍.md "wikilink")（臨時步行木偶）
      - [布公仔流動實驗](../Page/布公仔流動實驗.md "wikilink")（臨時步行木偶）
  - **Paradise Pier**
      - [Toy Story Midway
        Mania\!](../Page/Toy_Story_Midway_Mania!.md "wikilink")

### [華特迪士尼世界度假區](../Page/華特迪士尼世界度假區.md "wikilink")

#### [神奇王國](../Page/神奇王國.md "wikilink")

#### [Epcot](../Page/:en:Epcot.md "wikilink")

#### [迪士尼荷里活影城](../Page/迪士尼荷里活影城.md "wikilink")

#### [迪士尼動物王國](../Page/迪士尼動物王國.md "wikilink")

### [東京迪士尼度假區](../Page/東京迪士尼度假區.md "wikilink")

#### [東京迪士尼樂園](../Page/東京迪士尼樂園.md "wikilink")

  - **[探險樂園](../Page/探險世界.md "wikilink")**
      - （[西部沿河鐵路的一部分](../Page/西部沿河鐵路.md "wikilink")）

      - 叢林巡航

      - 提基神殿：史迪仔呈獻「」

      - [加納比海盜](../Page/加納比海盜.md "wikilink")
  - **[西部樂園](../Page/邊域世界.md "wikilink")**
      - 鄉村頑熊劇場
      - 豪華馬克吐溫號
      - 巨雷山
  - **[動物天地](../Page/動物天地.md "wikilink")**
      - [飛濺山](../Page/飛濺山.md "wikilink")
  - **[夢幻樂園](../Page/幻想世界.md "wikilink")**
      - 灰姑娘城堡神秘之旅（已關閉）
      - 小飛俠天空之旅
      - 白雪公主冒險旅程
      - 米老鼠劇場
      - 小木偶奇遇記
      - 幽靈公館
      - [小小世界](../Page/小小世界.md "wikilink")
      - 小熊維尼獵蜜記
      - 米奇幻想曲（2011年開幕）
  - **卡通城**
      - 兔子羅傑卡通轉轉車
  - **[明日樂園](../Page/明日世界.md "wikilink")**
      - 星際旅行

      - 巴斯光年的星際歷險

      - （已關閉）

      - （已關閉）

      - 怪獸電力公司「迷藏巡遊車」
  - **巡遊**
      - 歡騰！
      - 東京迪士尼樂園電子大遊行～夢之光

#### [東京迪士尼海洋](../Page/東京迪士尼海洋.md "wikilink")

  - **地中海港灣**
      - 密西哥傳奇

      -
  - **失落河三角洲**
      - 奪寶奇兵之水晶骷髏魔宮
  - **阿拉伯海岸**
      - 辛巴達傳奇之旅
      - 神燈劇場
  - **美人魚礁湖**
      - 美人魚礁湖劇場
  - **神秘島**
      - [地心探險之旅](../Page/地心探險之旅.md "wikilink")
      - [海底兩萬里](../Page/海底兩萬里.md "wikilink")

### [巴黎迪士尼樂園度假區](../Page/巴黎迪士尼樂園度假區.md "wikilink")

#### [巴黎迪士尼樂園](../Page/巴黎迪士尼樂園.md "wikilink")

#### [華特迪士尼影城](../Page/華特迪士尼影城.md "wikilink")

### [香港迪士尼樂園度假區](../Page/香港迪士尼樂園度假區.md "wikilink")

#### [香港迪士尼樂園](../Page/香港迪士尼樂園.md "wikilink")

  - **[美國小鎮大街](../Page/美國小鎮大街.md "wikilink")**
      - 香港迪士尼樂園鐵路
      - 迪士尼樂園的故事（已關閉）
      - 大街詭異酒店（已關閉）
      - 哈囉阿古（已關閉）
  - **[探險世界](../Page/探險世界.md "wikilink")**
      - [獅子王慶典](../Page/獅子王慶典.md "wikilink")
      - 森林河流之旅
      - 泰山樹屋
  - **[幻想世界](../Page/幻想世界.md "wikilink")**
      - [小小世界](../Page/小小世界.md "wikilink")
      - 米奇幻想曲
      - 小熊維尼歷險之旅
  - **[明日世界](../Page/明日世界.md "wikilink")**
      - 巴斯光年星際歷險
  - **巡遊**
      - 米奇水花巡遊
      - 夜光鬼魅巡遊
  - **娛樂節目**
      - 布公仔流動實驗

[de:Animatronic](../Page/de:Animatronic.md "wikilink")
[nl:Animatronic](../Page/nl:Animatronic.md "wikilink")
[pt:Animatronic](../Page/pt:Animatronic.md "wikilink")

[Category:迪士尼科技](../Category/迪士尼科技.md "wikilink")
[Category:迪士尼樂園](../Category/迪士尼樂園.md "wikilink")
[Category:東京迪士尼樂園](../Category/東京迪士尼樂園.md "wikilink")
[Category:机器人](../Category/机器人.md "wikilink")