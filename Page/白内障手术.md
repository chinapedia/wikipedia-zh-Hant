**[白内障手术](../Page/白内障.md "wikilink")**（）是一种[眼科](../Page/眼科.md "wikilink")[手术](../Page/手术.md "wikilink")，其目的是为了去除已经混浊的[水晶体](../Page/水晶体.md "wikilink")，并装入透明的[人工晶体](../Page/人工晶体.md "wikilink")，以取代天然晶体的作用。白内障的可能原因包括眼睛受外力創傷、一些藥物，特別是長期使用[類固醇](../Page/類固醇.md "wikilink")、以及長期暴露於[红外线和](../Page/红外线.md "wikilink")[微波](../Page/微波.md "wikilink")。其第一症狀通常為對於一般光線感到強烈的刺眼、夜晚視線昏暗、光源不足的地方，視線更加模糊\[1\]

移除混浊的水晶体後，會植入[人工晶体](../Page/人工晶体.md "wikilink")。白内障手术通常是[眼科醫師在](../Page/眼科学.md "wikilink")進行而不需要住院。無論是以在眼球表面（topical）、眼球周邊（peribulbar）、或眼球後方（retrobulbar）施以[局部麻醉](../Page/局部麻醉.md "wikilink")，
病人頂多感覺到稍微不舒服或幾乎沒感覺。此手術有超過90%的成功率幫助患者恢復因白內障而受損的視力；其衍生出的手術機率十分低\[2\]。、高手術量、低侵入性、手術傷口小的以及快速的術後恢復已是世界共同的白內障手術標準流程。

## 文獻

[Category:眼科手術](../Category/眼科手術.md "wikilink")

1.  *U.S. News & World Report*, December 17, 2007, page 64.
2.  University of Illinois Eye Center.
    ["Cataracts."](http://www.uic.edu/com/eye/LearningAboutVision/EyeFacts/Cataracts.shtml)
    Retrieved August 18, 2006.