**白刺属**（學名：**）是[被子植物](../Page/被子植物.md "wikilink")[白刺科之下三個屬之一](../Page/白刺科.md "wikilink")。原生於[非洲](../Page/非洲.md "wikilink")，本科物種目前分佈於[欧洲](../Page/欧洲.md "wikilink")、[亚洲](../Page/亚洲.md "wikilink")、[俄罗斯及](../Page/俄罗斯.md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")。

## 物種

本屬現時有物種九個，以下為部分物種：

  - ** \[1\]
  - [帕米爾白刺](../Page/帕米爾白刺.md "wikilink") *[Nitraria
    pamirica](../Page/Nitraria_pamirica.md "wikilink")*
  - [毛瓣白刺](../Page/毛瓣白刺.md "wikilink") *[Nitraria
    praevisa](../Page/Nitraria_praevisa.md "wikilink")*
  - **
  - [大白刺](../Page/大白刺.md "wikilink") *[Nitraria
    roborowskii](../Page/Nitraria_roborowskii.md "wikilink")*
  - *[Nitraria schoberi](../Page/Nitraria_schoberi.md "wikilink")*
  - [小果白刺](../Page/小果白刺.md "wikilink") *[Nitraria
    sibirica](../Page/Nitraria_sibirica.md "wikilink")*
  - [泡泡刺](../Page/泡泡刺.md "wikilink") *[Nitraria
    sphaerocarpa](../Page/Nitraria_sphaerocarpa.md "wikilink")*
  - [白刺](../Page/白刺.md "wikilink") *[Nitraria
    tangutorum](../Page/Nitraria_tangutorum.md "wikilink")*

## 參考文獻

  -
## 外部連結

  -

[白刺属](../Category/白刺属.md "wikilink")

1.