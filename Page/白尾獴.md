**白尾獴**
是一种生活在[撒哈拉沙漠以南的非洲广大地区以及](../Page/撒哈拉沙漠.md "wikilink")[阿拉伯半岛南部的](../Page/阿拉伯半岛.md "wikilink")[獴科动物](../Page/獴科.md "wikilink")。

## 亚种

白尾獴包括以下亚种\[1\]

  - **白尾獴指名亚种**（[学名](../Page/学名.md "wikilink")： (G.\[Baron\] Cuvier,
    1829)）
  - （[学名](../Page/学名.md "wikilink")： (Hollister, 1916)）
  - （[学名](../Page/学名.md "wikilink")： (Thomas, 1890)）
  - （[学名](../Page/学名.md "wikilink")： Roberts, 1924）
  - （[学名](../Page/学名.md "wikilink")： (Thomas, 1904)）
  - （[学名](../Page/学名.md "wikilink")： (Thomas, 1904)）
  - （[学名](../Page/学名.md "wikilink")： (Temminck, 1853)）

## 参考文献

[Category:獴科](../Category/獴科.md "wikilink")
[Category:西南亞哺乳動物](../Category/西南亞哺乳動物.md "wikilink")

1.