**白山市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[吉林省下辖的](../Page/吉林省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于[长白山西侧](../Page/长白山.md "wikilink")，东经126°7'至128°18'，北纬41°21'至42°48'，东与[延边相邻](../Page/延边.md "wikilink")；西与[通化接壤](../Page/通化.md "wikilink")；北与[吉林毗连](../Page/吉林市.md "wikilink")；南与朝鲜第三大城市[惠山市隔](../Page/惠山市.md "wikilink")[鸭绿江相望](../Page/鸭绿江.md "wikilink")。东西相距180公里，南北相距163公里，国境线长454公里。2009年市区面积2736平方公里，行政区土地面积名列全省第三；建成区面积40平方公里；市区61.2万人，占全人口45.6%。目前白山市区非农业人口已达46.7万人，市区的城镇化率达78.8%。全市人口125.37万，全市城镇化率为68%，境内有[长白山机场](../Page/长白山机场.md "wikilink")，是东北东部重要的节点城市和吉林省东南部的中心城市。

## 历史沿革

1902年，清政府置临江县于猫耳山，属吉林副都统辖区兴京厅。1907年，改属[奉天省](../Page/奉天省.md "wikilink")，仍隶兴京厅。[民国二年](../Page/民国.md "wikilink")（1913年），属奉天省东路道。1934年，改属[滿洲國](../Page/滿洲國.md "wikilink")[安东省](../Page/安東省_\(滿洲國\).md "wikilink")。1937年划归[通化省](../Page/通化省.md "wikilink")。1945年满洲国灭亡后，划归安东省通化专署。1946年划归[辽宁省](../Page/辽宁省.md "wikilink")。1949年改属[辽东省](../Page/辽东省.md "wikilink")。1954年8月，划归吉林省。1959年3月，撤消临江县，设立浑江市（县级），属通化地区。1985年3月，浑江市升格为地级市，设立八道江、临江、三岔子3个市辖区，并辖抚松、靖宇、长白3县。1994年4月，浑江市更名为白山市，市政府驻八道江区。2010年5月，八道江区更名为浑江区。

## 地理

## 政治

### 现任领导

<table>
<caption>白山市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党白山市委员会.md" title="wikilink">中国共产党<br />
白山市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/白山市人民代表大会.md" title="wikilink">白山市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/白山市人民政府.md" title="wikilink">白山市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议白山市委员会.md" title="wikilink">中国人民政治协商会议<br />
白山市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/张志军_(1964年).md" title="wikilink">张志军</a>[1]</p></td>
<td><p><a href="../Page/李宇忠.md" title="wikilink">李宇忠</a>[2]</p></td>
<td><p><a href="../Page/王冰_(1969年).md" title="wikilink">王冰</a>[3]</p></td>
<td><p><a href="../Page/裘会文.md" title="wikilink">裘会文</a>[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p><a href="../Page/满族.md" title="wikilink">满族</a></p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/辽宁省.md" title="wikilink">辽宁省</a><a href="../Page/义县.md" title="wikilink">义县</a></p></td>
<td></td>
<td></td>
<td><p>吉林省<a href="../Page/磐石市.md" title="wikilink">磐石市</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2015年9月</p></td>
<td><p>2016年1月</p></td>
<td><p>2018年1月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖2个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")、1个[自治县](../Page/自治县.md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[浑江区](../Page/浑江区.md "wikilink")、[江源区](../Page/江源区.md "wikilink")
  - 县级市：[临江市](../Page/临江市.md "wikilink")
  - 县：[抚松县](../Page/抚松县.md "wikilink")、[靖宇县](../Page/靖宇县.md "wikilink")
  - 自治县：[长白朝鲜族自治县](../Page/长白朝鲜族自治县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>白山市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>220600</p></td>
</tr>
<tr class="odd">
<td><p>220602</p></td>
</tr>
<tr class="even">
<td><p>220605</p></td>
</tr>
<tr class="odd">
<td><p>220621</p></td>
</tr>
<tr class="even">
<td><p>220622</p></td>
</tr>
<tr class="odd">
<td><p>220623</p></td>
</tr>
<tr class="even">
<td><p>220681</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>白山市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>白山市</p></td>
<td><p>1296127</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>浑江区</p></td>
<td><p>364723</p></td>
<td><p>28.14</p></td>
</tr>
<tr class="even">
<td><p>江源区</p></td>
<td><p>254293</p></td>
<td><p>19.62</p></td>
</tr>
<tr class="odd">
<td><p>抚松县</p></td>
<td><p>297960</p></td>
<td><p>22.99</p></td>
</tr>
<tr class="even">
<td><p>靖宇县</p></td>
<td><p>131631</p></td>
<td><p>10.16</p></td>
</tr>
<tr class="odd">
<td><p>长白朝鲜族自治县</p></td>
<td><p>72550</p></td>
<td><p>5.60</p></td>
</tr>
<tr class="even">
<td><p>临江市</p></td>
<td><p>174970</p></td>
<td><p>13.50</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")1296575人\[8\]。同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共减少15787人，下降1.2%。年平均下降率为0.12%。其中，男性为661038人，占总人口的50.98%；女性为635537人，占总人口的49.02%。总人口性别比（以女性为100）为104.01。0－14岁的人口为147103人，占11.35%；15－64的人口为1015663人，占78.33%；65岁及以上的人口为133809人，占10.32%。

## 参考文献

[category:吉林地级市](../Page/category:吉林地级市.md "wikilink")
[白山](../Page/category:白山.md "wikilink")

[吉](../Category/中国中等城市.md "wikilink")
[Category:中华人民共和国首批资源枯竭型城市](../Category/中华人民共和国首批资源枯竭型城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.