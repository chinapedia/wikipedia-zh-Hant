**白斑刺尻魚**，又名**白斑棘蝶魚**，俗名白點新娘，隸[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[鱸亞目](../Page/鱸亞目.md "wikilink")[蓋刺魚科](../Page/蓋刺魚科.md "wikilink")。

## 分布

居於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[聖誕島](../Page/聖誕島.md "wikilink")、[日本](../Page/日本.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[羅得豪島](../Page/羅得豪島.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[斐濟](../Page/斐濟.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[東加](../Page/東加.md "wikilink")、[萬納杜等海域](../Page/萬納杜.md "wikilink")。

## 深度

水深4至30公尺。

## 特徵

魚體橢圓形；背部輪廓略突出，頭背於眼上方略突。吻鈍而小。眶前骨游離，下緣凸出，後方具棘；前鰓蓋骨具鋸齒，具一長強棘；間鰓蓋骨短圓。上下頜相等，齒細長而稍內彎。體色紫黑到黑色，在體側中央側線上有一形狀似橢圓形的白色橫斑。腹鰭黃色、臀鰭外緣有寬黃邊，此外身上無其他斑紋。體被稍大櫛鱗，軀幹前背部具副鱗。幼魚體色和成魚相似，但其身上白橫斑較細長且臀鰭外緣黃邊較窄小。背鰭硬棘十四枚，軟條十六枚；臀鰭硬棘三枚，軟條十七枚；背鰭與臀鰭軟條部後端尖形；腹鰭鈍形；尾鰭圓形。體長可達十八公分。

## 生態

喜歡棲息在有海流的珊瑚礁或崖壁下方，常單獨或三兩成群，雜食，以藻類、珊瑚蟲及附著生物為食。

## 用途

可愛易馴，多供觀賞，少人食用。

## 注腳

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[tibicen](../Category/刺尻魚屬.md "wikilink")