**白细胞介素-8**（，简称为**白介素-8**或**IL-8**，亦称为趋化因子**CXCL8**）是[巨噬细胞和](../Page/巨噬细胞.md "wikilink")[上皮细胞等分泌的](../Page/上皮细胞.md "wikilink")[细胞因子](../Page/细胞因子.md "wikilink")\[1\]。白细胞介素-8结合趨化因子受体白细胞介素-8受体α(IL8RA,
又叫[CXCR1](../Page/CXCR1.md "wikilink"))和白细胞介素-8受体β(IL8RB,
又叫[CXCR2](../Page/CXCR2.md "wikilink"))而对嗜中性粒细胞(neutrophils)有细胞趋化作用而实现其对炎症反应的调节\[2\]\[3\]。
白细胞介素-8还有很强的促血管生成作用。白细胞介素-8在[小支气管炎](../Page/小支气管炎.md "wikilink")\[4\]
和[囊性纤维化](../Page/囊性纤维化.md "wikilink")\[5\] 的发病中起重要作用。

## 参见

  - [趋化因子](../Page/趋化因子.md "wikilink")
  - [CXCR1](../Page/CXCR1.md "wikilink")
  - [CXCR2](../Page/CXCR2.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Charo IF, Ransohoff RM. The many roles of chemokines and chemokine
    receptors in inflammation. N Engl J Med. 2006
    Feb 9;354(6):610-21.](http://content.nejm.org/cgi/content/extract/354/6/610)

[Category:细胞因子](../Category/细胞因子.md "wikilink")
[Category:免疫学](../Category/免疫学.md "wikilink")

1.  Baggiolini, M.; Walz, A.; Kunkel, S. L. Neutrophil-activating
    peptide-1/interleukin 8, a novel cytokine that activates
    neutrophils. J. Clin. Invest. 84: 1045-1049, 1989.
2.  Ahuja SK, Ozcelik T, Milatovitch A, Francke U, Murphy PM. Molecular
    evolution of the human interleukin-8 receptor gene cluster. Nat
    Genet. 1992 Sep;2(1):31-6.
3.  Baggiolini, M.; Walz, A.; Kunkel, S. L. Neutrophil-activating
    peptide-1/interleukin 8, a novel cytokine that activates
    neutrophils. J. Clin. Invest. 84: 1045-1049, 1989.
4.  Emi, M. et al., Association of diffuse panbronchiolitis with
    microsatellite polymorphism of the human interleukin 8 (IL-8) gene.
    J. Hum. Genet. 44: 169-172, 1999.
5.  Srivastava, M. et al., Digitoxin mimics gene therapy with CFTR and
    suppresses hypersecretion of IL-8 from cystic fibrosis lung
    epithelial cells. Proc. Nat. Acad. Sci. 101: 7693-7698, 2004.