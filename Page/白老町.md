**白老町**（）是[北海道](../Page/北海道.md "wikilink")[膽振綜合振興局中部的一個城鎮](../Page/膽振綜合振興局.md "wikilink")，位於[登別市和](../Page/登別市.md "wikilink")[苫小牧市之間](../Page/苫小牧市.md "wikilink")。市區主要位於沿海地區，而其他區域則以林地為主。在日本人移居至此前，此地原為一大型[阿伊努族聚落](../Page/阿伊努族.md "wikilink")，因此到現在仍有許多擁有阿伊努族血統的人居住於此。

町名源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「siraw-o-i」，意思是[虻很多的地方](../Page/虻.md "wikilink")。

## 歷史

  - 1856年：[仙台藩在此設置仙台陣屋](../Page/仙台藩.md "wikilink")，負責北方的警備。\[1\]
  - 1869年8月15日：設置白老郡。
  - 1872年：設置開拓使白老派出所。
  - 1874年：廢除開拓使白老派出所。
  - 1880年：設置白老外2村戶長役場
  - 1919年4月1日：敷生村、白老村、社台村合併為白老村，並成為北海道二級村。
  - 1954年11月1日：改制為白老町。

## 產業

以觀光業、畜牧業及造紙工業為主，

## 交通

### 機場

  - [新千歲機場](../Page/新千歲機場.md "wikilink")（位於[千歲市和](../Page/千歲市.md "wikilink")[苫小牧市](../Page/苫小牧市.md "wikilink")）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [室蘭本線](../Page/室蘭本線.md "wikilink")：[虎杖濱車站](../Page/虎杖濱車站.md "wikilink")
        - [竹浦車站](../Page/竹浦車站.md "wikilink") -
        [北吉原車站](../Page/北吉原車站.md "wikilink") -
        [萩野車站](../Page/萩野車站.md "wikilink") -
        [白老車站](../Page/白老車站.md "wikilink") -
        [社台車站](../Page/社台車站.md "wikilink")

|                                                                                                       |                                                                                                 |
| ----------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| [Hagino_station.jpg](https://zh.wikipedia.org/wiki/File:Hagino_station.jpg "fig:Hagino_station.jpg") | [Shiraoist_02.jpg](https://zh.wikipedia.org/wiki/File:Shiraoist_02.jpg "fig:Shiraoist_02.jpg") |

### 道路

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速國道</dt>

</dl>
<ul>
<li><a href="../Page/道央自動車道.md" title="wikilink">道央自動車道</a>：<a href="../Page/萩野休息區.md" title="wikilink">萩野休息區</a> - <a href="../Page/白老交流道.md" title="wikilink">白老交流道</a></li>
</ul>
<dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道36號.md" title="wikilink">國道36號</a></li>
</ul></td>
<td><dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道86號白老大瀧線</li>
</ul>
<dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道350號俱多樂湖公園線</li>
<li>北海道道388號白老停車場線</li>
<li>北海道道701號登別港線</li>
<li>北海道道1045號千歲白老線</li>
</ul></td>
</tr>
</tbody>
</table>

### 巴士

  - 道南巴士
  - 北海道中央巴士

## 觀光景點

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="觀光">觀光</h3>
<ul>
<li>纜索瀑布（<a href="../Page/日本瀑布百選.md" title="wikilink">日本瀑布百選</a>）</li>
<li>阿伊努民族博物館</li>
<li>仙台藩白老陣屋跡</li>
<li>白老溫泉</li>
<li>虎杖濱溫泉</li>
<li>俱多樂湖</li>
</ul></td>
<td><h3 id="祭典">祭典</h3>
<ul>
<li>白老牛肉祭（每年6月）</li>
<li>元氣町白老港祭（每年8月）</li>
<li>俱多樂湖放河灯（每年8月）</li>
<li>白老チェプ祭（每年9月）</li>
<li>白老八幡神社例大祭（每年9月）</li>
</ul>
<h3 id="文化遺產">文化遺產</h3>
<ul>
<li>阿伊努古式舞踊（國指定重要無形民俗文化財）</li>
<li>白老仙台藩陣屋跡（國指定史跡）</li>
<li>白老町虎杖濱越後盆舞蹈（白老町指定無形民俗文化財）</li>
</ul></td>
</tr>
</tbody>
</table>

## 教育

### 專門學校

  - [日本航空學園](../Page/日本航空學園.md "wikilink")

### 高等學校

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="http://www.shiraoihigashi.hokkaido-c.ed.jp/">道立北海道白老東高等學校</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.hokkaidosakae.ed.jp/">私立北海道榮高等學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>白老町立虎杖中學校</li>
<li>白老町立白老中學校</li>
</ul></td>
<td><ul>
<li>白老町立竹浦中學校</li>
</ul></td>
<td><ul>
<li>白老町立萩野中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>白老町立虎杖小學校</li>
<li>白老町立白老小學校</li>
</ul></td>
<td><ul>
<li>白老町立社台小學校</li>
<li>白老町立竹浦小學校</li>
</ul></td>
<td><ul>
<li>白老町立萩野小學校</li>
<li>白老町立綠丘小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [森田村](../Page/森田村.md "wikilink")（[青森縣](../Page/青森縣.md "wikilink")[西津輕郡](../Page/西津輕郡.md "wikilink")）：已於2005年2月11日合併為[津輕市](../Page/津輕市.md "wikilink")。\[2\]
  - [仙台市](../Page/仙台市.md "wikilink")（[宮城縣](../Page/宮城縣.md "wikilink")）

### 海外

  - （[加拿大](../Page/加拿大.md "wikilink")
    [英屬哥倫比亞](../Page/英屬哥倫比亞.md "wikilink")）。

## 本地出身的名人

  - [高梨利洋](../Page/高梨利洋.md "wikilink")：前職業棒球選手。
  - [山本宏美](../Page/山本宏美.md "wikilink")：[競速滑冰選手](../Page/競速滑冰.md "wikilink")。
  - [森竹竹市](../Page/森竹竹市.md "wikilink")：詩人、阿伊努三大歌人之一。
  - VOICE：雙胞胎歌手。

## 參考資料

## 外部連結

  - [白老觀光協會](http://www.shiraoi.net/)

  - [仙台藩白老元陣屋資料館](https://web.archive.org/web/20070703204433/http://www.town.shiraoi.hokkaido.jp/ka/jinya/)

  - [阿伊努民族博物館](http://www.ainu-museum.or.jp/)

<!-- end list -->

1.  白老町官方網頁 - 白老町的歷史
    <http://www.town.shiraoi.hokkaido.jp/ka/keiei-hp/kikaku/14toukeisyo/sankousiryou/siraoityounorekisi.htm>

2.  白老町官方網頁 - 姊妹城市交流
    <http://www.town.shiraoi.hokkaido.jp/english/profile/introduction_to_sister_cities.htm>