**白腹軍艦鳥**（[學名](../Page/學名.md "wikilink")：**）是一种热带大型[海鸟](../Page/海鸟.md "wikilink")，属于[军舰鸟科](../Page/军舰鸟科.md "wikilink")。其主要分布于[印度洋](../Page/印度洋.md "wikilink")，以[圣诞岛为繁殖地](../Page/圣诞岛.md "wikilink")。捕食水面和浅滩中的[鱼类](../Page/鱼类.md "wikilink")；经常抢夺其他海鸟所捕获的鱼，故又被称为“海盗鸟”。

## 特徵

[白腹軍艦鳥](../Page/白腹軍艦鳥.md "wikilink")[翼展可超過](../Page/翼.md "wikilink")200厘米\[1\]，[喙長而尖端彎曲](../Page/喙.md "wikilink")，[腳趾間有](../Page/腳趾.md "wikilink")[蹼](../Page/蹼.md "wikilink")（[蹼膜有很深的缺口](../Page/蹼膜.md "wikilink")），[翅膀比較長而狹窄](../Page/翅膀.md "wikilink")，[尾部猶如深](../Page/尾.md "wikilink")[叉的形狀](../Page/叉.md "wikilink")。[背部黑色](../Page/背.md "wikilink")。[腹部白色](../Page/腹.md "wikilink")。雄性鳥的嘴是暗灰色；雌性鳥的嘴則是紅色。

## 分佈\[2\]

### 國家或地區

#### 栖息地

  - [汶萊達魯薩蘭國](../Page/汶萊達魯薩蘭國.md "wikilink")
  - [柬埔寨](../Page/柬埔寨.md "wikilink")
  - [中國南部沿海](../Page/中國.md "wikilink")（包括[香港](../Page/香港.md "wikilink")[米埔](../Page/米埔.md "wikilink")）
  - [聖誕島](../Page/聖誕島.md "wikilink")
  - [印尼](../Page/印尼.md "wikilink")
  - [馬來西亞](../Page/馬來西亞.md "wikilink")
  - [新加坡](../Page/新加坡.md "wikilink")
  - [斯里蘭卡](../Page/斯里蘭卡.md "wikilink")
  - [泰國](../Page/泰國.md "wikilink")
  - [東帝汶民主共和國](../Page/東帝汶民主共和國.md "wikilink")

#### 漂泊不定

  - [澳洲](../Page/澳洲.md "wikilink")
  - [可可斯群島](../Page/可可斯群島.md "wikilink")
  - [印度](../Page/印度.md "wikilink")
  - [日本](../Page/日本.md "wikilink")
  - [肯亞](../Page/肯亞.md "wikilink")
  - [越南](../Page/越南.md "wikilink")

### 海域

  - [印度洋東部](../Page/印度洋.md "wikilink")
  - [太平洋西部的中心](../Page/太平洋.md "wikilink")

## 歷史

1988年列為[受威脅](../Page/受威脅.md "wikilink")，1994年提升為[易危](../Page/易危.md "wikilink")，自2000年起維持在最高評級[極危內](../Page/極危.md "wikilink")。主要原因是其繁殖地出現了極大量的[細足捷蟻](../Page/細足捷蟻.md "wikilink")（*Anoplolepis
gracilipes*），嚴重破壞了其存活空間。

## 腳注

## 外部連結

  - [米埔內的特別鳥種](https://web.archive.org/web/20071220190717/http://www.wwf.org.hk/chi/maipo/wildlife/birds/specialty.php)

  - [動物資料室](http://www.chiculture.net/0207/0207home/home_index.shtml)

  - [新聞中的科學--軍艦鳥怎能連飛26天？](https://web.archive.org/web/20080730044857/http://www.mingdao.edu.tw/library/index/science/095/001_04.htm)

  - [birdlife](http://www.birdlife.org/datazone/search/species_search.html?ction=SpcHTMDetails.asp&sid=3847&m=0)

[Category:中国国家一级保护动物](../Category/中国国家一级保护动物.md "wikilink")
[Category:軍艦鳥屬](../Category/軍艦鳥屬.md "wikilink")

1.
2.