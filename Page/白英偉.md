**白英偉**（），前任[澳門特別行政區](../Page/澳門特別行政區.md "wikilink")[警察總局局長](../Page/澳門警察總局.md "wikilink")。\[1\]現己退休。1976年于澳門利宵中學[預科畢業](../Page/預科.md "wikilink")。他能說流利的[廣東話](../Page/廣東話.md "wikilink")、[葡萄牙語](../Page/葡萄牙語.md "wikilink")、[普通話及](../Page/普通話.md "wikilink")[英語](../Page/英語.md "wikilink")。

  - 1977年至1983年工作于澳門[工務局](../Page/工務局.md "wikilink")。
  - 1983年10月正式加入澳門警隊，開始他的警隊生涯，當時任職副區長。
  - 1989年晉陞為[區長](../Page/區長.md "wikilink")。
  - 1995年1月獲第一屆“警官培訓課程”學士學位後，晉陞為副警司。
  - 1996年至1998年分別晉陞為警司、副警務總長、及警務總長。
  - 1998年修讀“指揮及領導課程”而晉陞為副警務總監，其後再晉陞為警務總監。
  - 1999年3月任[治安警察局局長](../Page/治安警察局.md "wikilink")。
  - 2001年9月任澳門特別行政區警察總局局長。
  - 2004年12月4日再次被任命為澳門特別行政區警察總局局長。

## 參考文獻

<div class="references-small">

<references />

</div>

{{-}}

[Category:澳門政治人物](../Category/澳門政治人物.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:白姓](../Category/白姓.md "wikilink")
[Category:归化中华人民共和国之澳门居民](../Category/归化中华人民共和国之澳门居民.md "wikilink")
[Category:土生葡人](../Category/土生葡人.md "wikilink")

1.  [國務院任命澳門特別行政區政府主要官員和檢察長](http://news.xinhuanet.com/newscenter/2004-12/09/content_2313422.htm)