[Coat_of_arms_of_Lorenzo_Bianchi.svg](https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Lorenzo_Bianchi.svg "fig:Coat_of_arms_of_Lorenzo_Bianchi.svg")

**白英奇**（，），1952年至1969年期間擔任[天主教香港教區主教](../Page/天主教香港教區.md "wikilink")。

1899年4月1日，白英奇出生在[意大利](../Page/意大利.md "wikilink")[布雷西亚的歌丹諾高奇Corteno](../Page/布雷西亚.md "wikilink")。他在年轻时加入了[宗座外方传教会](../Page/宗座外方传教会.md "wikilink")（PIME），1922年9月23日在米兰晋升神父，1923年9月13日抵达[香港](../Page/香港.md "wikilink")。

1924年，他被派往[新界的](../Page/新界.md "wikilink")[西贡传教](../Page/西貢市.md "wikilink")；从1925年到1929年在[海丰县传教](../Page/海丰县.md "wikilink")；从1930年到1941年，以及从1948年到1949年，他曾两度任海丰县。1949年4月21日，他被任命为香港教区[辅理主教](../Page/辅理主教.md "wikilink")，10月9日祝圣，随后回到海丰，1950年3月22日在当地被拘禁。1951年9月3日，[恩理觉主教去世](../Page/恩理觉.md "wikilink")，白英奇继任为香港教区主教，但他仍在被囚中，无法回到香港管理教务。直到1952年10月17日才获释返回[香港](../Page/香港.md "wikilink")，10月26日接任香港教區主教职务。1968年11月30日，白英奇主教宣布退休，由輔理主教[徐诚斌繼任主教](../Page/徐诚斌.md "wikilink")。1969年4月19日，白英奇返回意大利，1983年2月13日去世，終年83歲。

香港[明爱白英奇专业学校就是以白英奇主教命名](../Page/明爱白英奇专业学校.md "wikilink")。

2015年3月25日，白英奇主教的骨灰由意大利遷回香港，連同昔日天主教香港代牧區第二任[宗座代牧和主教的骨灰一同合葬於](../Page/宗座代牧.md "wikilink")[聖母無原罪主教座堂內的](../Page/聖母無原罪主教座堂.md "wikilink")[褔傳小堂](../Page/褔傳小堂.md "wikilink")。

## 参考文献

## 外部链接

  - [香港天主教教區檔案網頁](http://archives.catholic.org.hk/Succession%20Line/L-Bianchi.htm)

## 参见

  - [天主教香港教区](../Page/天主教香港教区.md "wikilink")
  - Corteno Golgi al Suo Vescovo Missionario Mons. Lorenzo Bianchi,
    1972.
  - Lorenzo Bianchi di Hong Kong, by Piero Gheddo, Instituto Geografico
    de Agostini, 1988.
  - Lawrence Bianchi of Hong Kong, by Piero Gheddo, Catholic Truth
    Society Hong Kong, 1988.
  - 白英奇主教傳, 基德多著, 香港公教真理學會, 1988.
  - From Milan to Hong Kong 150 Years of Mission, by Gianni Criveller,
    Vox Amica Press, 2008.
  - 從米蘭到香港150年傳教使命, 柯毅霖著, 良友之聲出版社, 2008.
  - 先賢錄－－香港天主教神職及男女修會會士 (1841-2010), 天主教香港教區檔案處, 2010.

{{-}}  |width=25% align=center|**前任：**
[恩理覺主教](../Page/恩理覺.md "wikilink") |width=50% align=center
colspan="2"|**[香港教區主教](../Page/香港教區主教.md "wikilink")**
1952年 - 1969年 |width=25% align=center|**繼任：**
[徐誠斌主教](../Page/徐誠斌.md "wikilink")

[Category:天主教香港教区主教](../Category/天主教香港教区主教.md "wikilink")
[Category:意大利天主教主教](../Category/意大利天主教主教.md "wikilink")
[Category:1899年出生](../Category/1899年出生.md "wikilink")
[Category:1983年逝世](../Category/1983年逝世.md "wikilink")