[Bai_Hao_Yin_Zhen_tea_leaf_(Fuding).jpg](https://zh.wikipedia.org/wiki/File:Bai_Hao_Yin_Zhen_tea_leaf_\(Fuding\).jpg "fig:Bai_Hao_Yin_Zhen_tea_leaf_(Fuding).jpg")来自福建福鼎的最高级的白茶\]\]

**白茶**是指一种采摘后，不经过[杀青或揉捻](../Page/杀青.md "wikilink")，只经过晒或文火干燥后加工的[茶](../Page/茶.md "wikilink")。白茶[白毫显露](../Page/白毫.md "wikilink")。比较出名的出自[福建东北部地区](../Page/福建.md "wikilink")，有[白毫银针](../Page/白毫银针.md "wikilink")，白牡丹，[贡眉](../Page/贡眉.md "wikilink")、寿眉等。

## 起源

白茶的名字最早出现在唐朝[陆羽的](../Page/陆羽.md "wikilink")《茶经》七之事中，其记载:“永嘉县东三百里有白茶山。”\[1\][陈橼教授在](../Page/陈橼.md "wikilink")《[茶叶通史](../Page/茶叶通史.md "wikilink")》中指出:“永嘉东三百里是海,是南三百里之误。南三百里是福建[福鼎](../Page/福鼎.md "wikilink")(唐为长溪县辖区)，系白茶原产地。”可见唐代长溪县(福建福鼎)已培育出“白茶”品种。因其仅有名称，能否作为起源证据还有待进一步商榷。

宋朝製茶「朝採即蒸」、「即蒸即焙」，不經[萎凋和發酵](../Page/萎凋.md "wikilink")，只有[綠茶](../Page/綠茶.md "wikilink")。《[大觀茶論](../Page/大觀茶論.md "wikilink")》所載「白茶」為全國僅六株的特殊茶樹，非今日之白茶。\[2\]

有的学者认为白茶始于神农尝百草时期，[湖南农学院的](../Page/湖南农学院.md "wikilink")[杨文辉先生就持此观点](../Page/杨文辉.md "wikilink")。他发表的《关于白茶起源时期的商榷》一文中提出白茶的出现早于绿茶。

## 功效

白茶具有[保健功效](../Page/保健.md "wikilink")，已被人们普遍认可。研究表明白茶比绿茶更为具有抗病毒和抗细菌功效\[3\]也具有更多的[儿茶素](../Page/儿茶素.md "wikilink")、[可可碱以及更少的](../Page/可可碱.md "wikilink")[咖啡因](../Page/咖啡因.md "wikilink")。\[4\]\[5\]儿茶素具有抗菌、除臭、防蛀等多种功效。白茶中茶多酚的含量較高，它是天然的抗氧化劑，可以起到提高免疫力和保護心血管等作用。此外，绿茶相比白茶则含有更少的[氟化物](../Page/氟化物.md "wikilink")，這被認為是有益健康的。\[6\]

## 参考文献

[Category:中國發明](../Category/中國發明.md "wikilink")
[白茶](../Category/白茶.md "wikilink")

1.  茶经/七之事，http://zh.wikisource.org/w/index.php?title=%E8%8C%B6%E7%B6%93/%E4%B8%83%E4%B9%8B%E4%BA%8B\&variant=zh-cn
2.
3.  Science Daily (May 28, 2004).
4.  "New" white tea, surprisingly, may have a healthful edge.
    Environmental Nutrition. Sept 2003. FindArticles.com. 10 Dec. 2007.
    [1](http://findarticles.com/p/articles/mi_m0854/is_9_26/ai_n18616257)
5.  Santana-Rios G, Orner GA, Amantana A, Provost C, Wu SY, Dashwood RH.
    , Mutation Research-Genetic Toxicology and Environmental
    Mutagenesis, Vol. 495, no. 1-2, pp. 61-74. (22 Aug 2001).
6.  Linus Pauling Institute: Micronutrient Information Center.
    [2](http://lpi.oregonstate.edu/infocenter/phytochemicals/tea/)