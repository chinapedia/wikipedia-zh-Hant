**白蘭**（[学名](../Page/学名.md "wikilink")：），在[華南](../Page/華南.md "wikilink")、[西南及](../Page/西南.md "wikilink")[东南亚地区生長的](../Page/东南亚.md "wikilink")[常綠](../Page/常綠植物.md "wikilink")[原生植物](../Page/本土植物.md "wikilink")，屬[木蘭科](../Page/木蘭科.md "wikilink")[含笑屬的](../Page/含笑屬.md "wikilink")[乔木](../Page/乔木.md "wikilink")，一般可長至10－13米的高度。是由和自然杂交得到\[1\]，同[屬的還有大約](../Page/屬.md "wikilink")49[種](../Page/種.md "wikilink")[植物](../Page/植物.md "wikilink")。其[花形似](../Page/花.md "wikilink")[黄葛树芽包](../Page/黄葛树.md "wikilink")，在[中国西南地区还有](../Page/中国.md "wikilink")**黄葛兰**的别名。

白蘭就是[臺灣俗稱的](../Page/臺灣.md "wikilink")**玉蘭花**；雖然曾經在植物學的分類上白兰并非[玉兰属的植物](../Page/玉兰属.md "wikilink"),但近代透過基因檢測發現白蘭所屬的含笑屬(Michelia)比許多玉蘭屬(Magnolia)的植物更為接近玉蘭。為免將知名度甚高的玉蘭屬分拆,植物學家決定將含笑屬也並入玉蘭屬之中,使白蘭成為玉蘭屬的一員。\[2\]

## 特征

[HK_HKZBG_WhiteJadeOrchidTree_LCSDCW43.JPG](https://zh.wikipedia.org/wiki/File:HK_HKZBG_WhiteJadeOrchidTree_LCSDCW43.JPG "fig:HK_HKZBG_WhiteJadeOrchidTree_LCSDCW43.JPG")[己連拿利入口的白蘭花高達](../Page/己連拿利.md "wikilink")30米\]\]
白蘭花的幼枝及芽呈綠色，密披微柔毛。葉淺綠色，互生，長橢圓形而先端較尖，革質，表皮無毛，長約10－20厘米，寬4－9.5厘米，面無毛，底疏生微柔毛。白蘭花一般可開花兩次，第一次在夏季，第二次在秋季，夏季花比較多，花色一般乳白色，披針形花披片10片，長3－4厘米，藥隔伸出成短尖頭，雌蕊群披微柔毛。蓇葖果成熟時紅色，少見結實。

## 種植及用途

[Michelia_alba_(Campii).jpg](https://zh.wikipedia.org/wiki/File:Michelia_alba_\(Campii\).jpg "fig:Michelia_alba_(Campii).jpg")
在亞洲，尤其在[東南亞與中國的熱帶或](../Page/東南亞.md "wikilink")[亞熱帶地區](../Page/亞熱帶.md "wikilink")，白蘭因為有著濃烈的香氣，而成為一種[園藝植物並廣泛種植](../Page/園藝植物.md "wikilink")\[3\]。白蘭還可以壓榨出[精油](../Page/精油.md "wikilink")。在中國，白蘭可作為玉蘭茶的原料\[4\]。在[中醫領域](../Page/中醫.md "wikilink")，白蘭的花可用來行氣止咳\[5\]\[6\]。

可用種子、嫁接或壓條繁殖。

[中国南方妇女常摘取花朵来佩戴在身](../Page/中国.md "wikilink")，也可提取香精或熏茶；在云南等地它还被俗称为“缅桂花”。

臺灣玉蘭花的最大產地在[屏東縣](../Page/屏東縣.md "wikilink")[高樹鄉](../Page/高樹鄉.md "wikilink")，其次為[鹽埔鄉](../Page/鹽埔鄉.md "wikilink")\[7\]。在臺灣，在絡繹不絕的車道上向駕駛兜售「玉蘭花」（臺灣慣稱白蘭為「玉蘭花」）的賣花人，為臺灣街頭常景（有賣花阿婆、賣花阿伯、賣花郎、賣花女）。
[車城福安宮玉蘭花小販.jpg](https://zh.wikipedia.org/wiki/File:車城福安宮玉蘭花小販.jpg "fig:車城福安宮玉蘭花小販.jpg")。\]\]

## 参考文献

## 書籍

  - 《[香港古樹名木](../Page/香港古樹名木.md "wikilink")》，邢福武、丘國賢
    主編，[天地圖書](../Page/天地圖書.md "wikilink")，ISBN
    978-988-211-585-9

## 外部連結

  -
[Category:含笑属](../Category/含笑属.md "wikilink")
[Category:乔木](../Category/乔木.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")
[Category:台灣植物](../Category/台灣植物.md "wikilink")

1.
2.

3.  {{ Citation | last1=Valder | first1=Peter | lastauthoramp=yes |
    title=The garden plants of China | publisher=Timber Press |
    year=1999}}

4.  {{ cite web
    |url=<http://www.ars-grin.gov/cgi-bin/npgs/html/taxon.pl?452629>
    |title=*Magnolia* × *alba* information from NPGS/GRIN
    |author=[GRIN](../Page/Germplasm_Resources_Information_Network.md "wikilink")
    |work=Taxonomy for Plants
    |publisher=[USDA](../Page/United_States_Department_of_Agriculture.md "wikilink"),
    [ARS](../Page/Agricultural_Research_Service.md "wikilink"), National
    Genetic Resources Program |location=National Germplasm Resources
    Laboratory, [Beltsville,
    Maryland](../Page/Beltsville,_Maryland.md "wikilink") |date=April
    19, 2007 |accessdate=May 18, 2011}}

5.  {{ Citation | last1=Zhou | first1=Jiaju | last2=Xie | first2=Guirong
    | last3=Yan | first3=Xinjian | lastauthoramp=yes |
    title=Encyclopedia of Traditional Chinese Medicines - Molecular
    Structures, Pharmacological Activities, Natural Sources and
    Applications: Isolated Compounds T-z, References for Isolated
    Compounds Tcm Original Plants and Congeners | publisher=Springer |
    place=China | year=2010}}

6.

7.