**白虎观会议**，是[中国](../Page/中国.md "wikilink")[东汉](../Page/东汉.md "wikilink")[章帝年间召开的重要](../Page/汉章帝.md "wikilink")[儒家学术会议](../Page/儒家.md "wikilink")，对于中国的思想史有着重要意义。

## 背景

自[汉武帝时](../Page/汉武帝.md "wikilink")[董仲舒提出](../Page/董仲舒.md "wikilink")[罢黜百家，独尊儒术以来](../Page/罢黜百家，独尊儒术.md "wikilink")，儒家思想逐渐成为[汉朝君主的统治思想支柱](../Page/汉朝.md "wikilink")。但是由于当时各家儒学学派传承不同，对于儒家经典的版本、内容多有争议。虽然[汉宣帝时曾召开](../Page/汉宣帝.md "wikilink")[石渠阁会议加以统一](../Page/石渠议奏.md "wikilink")，但是经历[王莽](../Page/王莽.md "wikilink")[新朝的战乱之后](../Page/新朝.md "wikilink")，各家的歧异再次抬头。

同时，由于[汉光武帝把](../Page/汉光武帝.md "wikilink")[谶纬正式确立为官方统治思想之一](../Page/谶纬.md "wikilink")，各家儒学学派对此意见并不一致，也同样需要中央政府出面将儒学和谶纬学结合起来，创建统一的思想标准，以巩固自身统治地位。

在这种情况下，章帝[建初四年](../Page/建初_\(东汉\).md "wikilink")（79年），按照[校書郎](../Page/校書郎.md "wikilink")[楊終的奏议](../Page/楊終.md "wikilink")，东汉中央政府决定仿西汉石渠阁会议召集各地著名儒生于[洛阳](../Page/洛阳.md "wikilink")[白虎观](../Page/白虎观.md "wikilink")，公开讨论儒学经典[五经](../Page/五经.md "wikilink")。

## 过程

当年冬十一月壬戌，章帝邀集名儒、诸王集于白虎观，由[五官中郎将](../Page/五官中郎将.md "wikilink")[魏应代表皇帝发问](../Page/魏应.md "wikilink")，其后各家儒生加以讨论，形成共识后由[侍中](../Page/侍中.md "wikilink")[淳于恭加以回答](../Page/淳于恭.md "wikilink")，此后章帝再亲自决定对此答案是否满意。会议连续举行了一个多月，会后，[班固奉旨对会议内容加以总结](../Page/班固.md "wikilink")，写成《[白虎通义](../Page/白虎通义.md "wikilink")》四卷。

## 结果

虎观会议最终确定了讖纬之术与儒学同等的法律地位。

## 外部链接

  - 《[中国大百科全书](../Page/中国大百科全书.md "wikilink")》[白虎观会议](http://www.white-collar.net/02-lib/01-zg/03-guoxue/%C6%E4%CB%FB%C0%FA%CA%B7%CA%E9%BC%AE/%C0%FA%CA%B7%B9%A4%BE%DF%C0%E0/%D6%D0%B9%FA%B4%F3%B0%D9%BF%C6%C8%AB%CA%E9%D6%D0%B9%FA%C0%FA%CA%B7/Resource/Book/Edu/JXCKS/TS011096/0041_ts011096.htm)条目

{{-}}

[Category:东汉政府](../Category/东汉政府.md "wikilink")
[Category:儒家](../Category/儒家.md "wikilink")
[Category:中国思想史](../Category/中国思想史.md "wikilink")
[Category:中国中央政府会议](../Category/中国中央政府会议.md "wikilink")
[Category:洛阳政治事件](../Category/洛阳政治事件.md "wikilink")
[Category:1世纪中国政治事件](../Category/1世纪中国政治事件.md "wikilink")
[Category:70年代中国政治](../Category/70年代中国政治.md "wikilink")
[Category:79年](../Category/79年.md "wikilink")
[Category:漢朝歷史事件](../Category/漢朝歷史事件.md "wikilink")