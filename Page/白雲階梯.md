《**白雲階梯**》（****；****）是[韓國](../Page/大韓民國.md "wikilink")[KBS電視台於](../Page/韓國放送公社.md "wikilink")2006年9月至11月期間播放的[月火迷你連續劇](../Page/KBS月火迷你連續劇.md "wikilink")，由[申東旭](../Page/申東旭.md "wikilink")、[韓智慧](../Page/韓智慧.md "wikilink")、[林晶恩](../Page/林晶恩.md "wikilink")、[金正賢等演出](../Page/金正賢.md "wikilink")。本劇改編自日本作家[渡邊淳一的同名小說](../Page/渡邊淳一.md "wikilink")。目前《白雲階梯》被翻拍成本劇（韓國版）及兩部日劇，日劇相關內容請參見《[白雲階梯](../Page/白雲階梯_\(日本電視劇\).md "wikilink")》。

## 劇情介紹

崔鐘秀自小與吳允熙在孤兒院長大。鐘秀因沒有能力承擔大學學費而放棄考試，後來，他到了一個小島，遇到一間小醫院的所長，所長勸他留下來當他的助手，鐘秀因此懂得照顧病人及做一些小手術。尹貞媛為了宣傳父親的醫院，來到一間在小島上的小醫院作訪問。在那裏，貞媛遇到了鐘秀，但她卻突然因急性闌尾炎而陷入昏迷，鐘秀給她做手術，成功地救活她，貞媛的家人誤會鐘秀是能幹醫生而邀請他到首爾的醫院工作……

## 演員表

### 主要角色

  - [申東旭](../Page/申東旭.md "wikilink") 飾演 崔鐘秀
  - [韓智慧](../Page/韓智慧.md "wikilink") 飾演 尹貞媛
  - [金正鉉](../Page/金正鉉.md "wikilink") 飾演 金道憲
  - [林晶恩](../Page/林晶恩.md "wikilink") 飾演 吳允熙

### 其他角色

  - [崔鐘源](../Page/崔鐘源.md "wikilink") 飾演 卞所長
  - [金勇建](../Page/金勇建.md "wikilink") 飾演 尹博士
  - [梁錦錫](../Page/梁錦錫.md "wikilink") 飾演 貞媛母
  - [李美英](../Page/李美英.md "wikilink") 飾演 允熙母
  - [延奎鎮](../Page/延奎鎮.md "wikilink") 飾演 金社長

## 參考

<references />

## 外部連結

  - [本劇韓國KBS官方網站](http://www.kbs.co.kr/drama/cloud/)

[Category:2006年韓國電視劇集](../Category/2006年韓國電視劇集.md "wikilink")
[Category:日本小說改編韓國電視劇](../Category/日本小說改編韓國電視劇.md "wikilink")
[Category:韓國愛情劇](../Category/韓國愛情劇.md "wikilink")
[Category:韓國醫學劇](../Category/韓國醫學劇.md "wikilink")
[Category:美亞電視外購劇集](../Category/美亞電視外購劇集.md "wikilink")
[Category:白雲階梯](../Category/白雲階梯.md "wikilink")