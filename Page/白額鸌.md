**白额鹱**（[学名](../Page/学名.md "wikilink")：）为[鹱科](../Page/鹱科.md "wikilink")[剪水鹱属的](../Page/剪水鹱属.md "wikilink")[鸟类](../Page/鸟类.md "wikilink")，又名**白颏风鹱**，俗名大灰鹱、大水鸟，屬於[鸌形目](../Page/鸌形目.md "wikilink")[鸌科](../Page/鸌科.md "wikilink")[鸌屬的](../Page/鸌屬.md "wikilink")[遠洋性海鳥](../Page/遠洋性海鳥.md "wikilink")，主要食物為[魚類](../Page/魚類.md "wikilink")，分布于日本、菲律宾、印度尼西亚以及澳大利亚北部的海域中以及[中国各海区等地](../Page/中国.md "wikilink")，多见于海滨一带以及繁殖于海边岩穴中。该物种的模式产地在日本海。\[1\]

## 参考文献

[Category:鸌屬](../Category/鸌屬.md "wikilink")

1.