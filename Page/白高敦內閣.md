[GordonBrown1234_cropped.jpg](https://zh.wikipedia.org/wiki/File:GordonBrown1234_cropped.jpg "fig:GordonBrown1234_cropped.jpg")
[白高敦在](../Page/白高敦.md "wikilink")2007年6月27日接替[貝理雅出任](../Page/貝理雅.md "wikilink")[英國首相](../Page/英國首相.md "wikilink")，他在6月28日公佈了內閣名單，是為首次組閣；在2009年6月5日，他復對內閣進行大規模改組。

## 第一次白高敦內閣 （2007年6月—2009年6月）

  - 白高敦 —
    [英國首相](../Page/英國首相.md "wikilink")、[第一財政大臣兼](../Page/第一財政大臣.md "wikilink")[公務員事務部部長](../Page/公務員事務部部長.md "wikilink")
  - [戴理德](../Page/戴理德.md "wikilink") —
    [財政大臣](../Page/財政大臣.md "wikilink")
  - [施仲宏](../Page/施仲宏.md "wikilink") —
    [司法大臣兼](../Page/司法大臣_\(英国\).md "wikilink")[大法官](../Page/大法官_\(英國\).md "wikilink")
  - [夏雅雯](../Page/夏雅雯.md "wikilink") —
    [下議院領袖](../Page/下議院領袖.md "wikilink")、[掌璽大臣兼](../Page/掌璽大臣.md "wikilink")[婦女及平等部長](../Page/婦女及平等部長.md "wikilink")
  - [艾嘉蓮女男爵](../Page/艾嘉蓮.md "wikilink") —
    [上議院領袖兼](../Page/上議院領袖.md "wikilink")[樞密院議長](../Page/樞密院議長.md "wikilink")
  - [文禮彬](../Page/文禮彬.md "wikilink") —
    [外交及聯邦事務大臣](../Page/外交及聯邦事務大臣.md "wikilink")
  - [施卓琪](../Page/施卓琪.md "wikilink") —
    [內政大臣](../Page/內政大臣.md "wikilink")
  - [彭德](../Page/彭德.md "wikilink") —
    [國防大臣兼](../Page/國防大臣.md "wikilink")[蘇格蘭大臣](../Page/蘇格蘭大臣.md "wikilink")
  - [博雅文](../Page/博雅文.md "wikilink") —
    [兒童、學校及家庭大臣](../Page/兒童、學校及家庭大臣.md "wikilink")
  - [鄧俊安](../Page/鄧俊安.md "wikilink") —
    [創新大學及技能大臣](../Page/創新大學及技能大臣.md "wikilink")
  - [彭浩禮](../Page/彭浩禮.md "wikilink") —
    [環境食物及郊野事務大臣](../Page/環境食物及郊野事務大臣.md "wikilink")
  - [莊翰生](../Page/莊翰生.md "wikilink") —
    [衛生大臣](../Page/衛生大臣.md "wikilink")
  - [韓培德](../Page/韓培德.md "wikilink") —
    [就業及退休保障大臣兼](../Page/就業及退休保障大臣.md "wikilink")[威爾斯大臣](../Page/威爾斯大臣.md "wikilink")
  - [夏敦](../Page/夏敦.md "wikilink") —
    [商業、企業及規管改革大臣](../Page/貿易及工業大臣.md "wikilink")
  - [艾力生](../Page/艾力生.md "wikilink") —
    [國際發展大臣](../Page/國際發展大臣.md "wikilink")
  - [貝禮高](../Page/貝禮高.md "wikilink") —
    [文化媒體及體育大臣](../Page/文化媒體及體育大臣.md "wikilink")
  - [簡樂芙](../Page/簡樂芙.md "wikilink") —
    [運輸大臣](../Page/運輸大臣.md "wikilink")
  - [貝海珊](../Page/貝海珊.md "wikilink") —
    [社區及地方政府大臣](../Page/社區及地方政府大臣.md "wikilink")
  - [禤智輝](../Page/禤智輝.md "wikilink") —
    [財政部政務次官兼下院黨鞭](../Page/財政部政務次官.md "wikilink")
  - [伍劭恩](../Page/伍劭恩.md "wikilink") —
    [北愛爾蘭大臣](../Page/北愛爾蘭大臣.md "wikilink")
  - [文立彬](../Page/文立彬.md "wikilink") —
    [內閣辦公室部長兼](../Page/內閣辦公室部長.md "wikilink")[蘭開斯特公爵領地事務大臣](../Page/蘭開斯特公爵領地事務大臣.md "wikilink")
  - [貝安德](../Page/貝安德.md "wikilink") —
    [財政部首席秘書](../Page/財政部首席秘書.md "wikilink")

### 變動

  - 2008年1月24日：[韓培德因醜聞辭職](../Page/韓培德.md "wikilink")，其原任的[就業及退休保障大臣及](../Page/就業及退休保障大臣.md "wikilink")[威爾斯大臣職務分別由](../Page/威爾斯大臣.md "wikilink")[文化媒體及體育大臣](../Page/文化媒體及體育大臣.md "wikilink")[貝禮高及重返內閣的](../Page/貝禮高.md "wikilink")[馬偉輝接任](../Page/馬偉輝.md "wikilink")。另外，文化媒體及體育大臣一職由[財政部首席秘書](../Page/財政部首席秘書.md "wikilink")[貝安德接任](../Page/貝安德.md "wikilink")，而財政部首席秘書則由原房屋及規劃國務部長[顧綺慧上任](../Page/顧綺慧.md "wikilink")。
  - 2008年10月3日：白高敦改組內閣，[文德森入閣任商業](../Page/文德森.md "wikilink")、企業及規管改革大臣，被取代的夏敦改任國防大臣，而原國防大臣兼蘇格蘭大臣[彭德離開內閣](../Page/彭德.md "wikilink")。[貝嘉晴出任](../Page/貝嘉晴.md "wikilink")[房屋及規劃部長](../Page/房屋及規劃部長.md "wikilink")，不在內閣供職，但可出席內閣會議，被取代的[費嘉琳則改以](../Page/費嘉琳.md "wikilink")[歐洲部長身份出席內閣會議](../Page/歐洲部長.md "wikilink")。另外，[羅卓雅女男爵入閣任上議院領袖兼樞密院議長](../Page/羅卓雅.md "wikilink")，以取代出閣擔任新任[歐盟專員的艾嘉蓮女男爵](../Page/歐盟.md "wikilink")。而原任貿易部長的[瓊斯勳爵亦離開內閣](../Page/迪格比·瓊斯，伯明翰的瓊斯男爵.md "wikilink")，該職不在內閣供職。其他變動包括文立彬出任新設的[能源及氣候變化大臣](../Page/能源及氣候變化大臣.md "wikilink")，由[白理安接替任內閣辦公室部長兼蘭開斯特公爵領地事務大臣](../Page/白理安.md "wikilink")；禤智輝接替離開內閣的簡樂芙任運輸大臣；[麥偉俊出任蘇格蘭大臣及](../Page/麥偉俊.md "wikilink")[麥克納爾蒂轉任就業部長兼倫敦部長等等](../Page/托尼·麥克納爾蒂.md "wikilink")。

## 第二次白高敦內閣 （2009年6月—2010年5月）

  - 白高敦 — 英國首相、第一財政大臣兼公務員事務部部長
  - 戴理德 — 財政大臣
  - 文禮彬 — 外交及聯邦事務大臣
  - 施仲宏 — 司法大臣兼大法官
  - 莊翰生 — 內政大臣
  - 貝安德 — 衛生大臣
  - 文德森勳爵 — 商業創新及技能大臣、第一國務大臣兼樞密院議長
  - 彭浩禮 — 環境食物及郊野事務大臣
  - 艾力生 — 國際發展大臣
  - [艾思和](../Page/艾思和.md "wikilink") — 國防大臣
  - 夏雅雯 — 下議院領袖、掌璽大臣兼婦女及平等部長
  - 鄧俊安 — 社區及地方政府大臣
  - [艾德思勳爵](../Page/艾德思.md "wikilink") — 運輸大臣
  - 博雅文 — 兒童、學校及家庭大臣
  - 文立彬 — 能源及氣候變化大臣
  - 顧綺慧 — 就業及退休保障大臣
  - 伍劭恩 — 北愛爾蘭大臣
  - 羅卓雅女男爵 — 上議院領袖兼蘭開斯特公爵領地事務大臣
  - [白德生](../Page/白德生.md "wikilink") — 文化媒體及體育大臣
  - [白理安](../Page/白理安.md "wikilink") — 財政部首席秘書
  - 韓培德 — 威爾斯大臣
  - 麥偉俊 — 蘇格蘭大臣
  - [蔣黛思](../Page/蔣黛思.md "wikilink") —
    內閣辦公室部長、[奧運部長兼](../Page/奧運部長.md "wikilink")[財政部主計長](../Page/財政部主計長.md "wikilink")

## 相關條目

  - [英國內閣](../Page/英國內閣.md "wikilink")

[B](../Category/英國內閣.md "wikilink")
[Category:2007年在英国设立](../Category/2007年在英国设立.md "wikilink")