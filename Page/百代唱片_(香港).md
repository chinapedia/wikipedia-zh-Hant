**百代唱片**（**EMI Hong
Kong**）曾是[香港一間主要的](../Page/香港.md "wikilink")[唱片公司](../Page/唱片.md "wikilink")，為世界四大唱片公司之一的[EMI在香港的子公司](../Page/EMI.md "wikilink")，於2003年與[黃柏高共同在香港成立](../Page/黃柏高.md "wikilink")[金牌娛樂](../Page/金牌大風.md "wikilink")，把當中包括香港華語歌手在港的唱片發行及宣傳工作轉往[金牌娛樂](../Page/金牌大風.md "wikilink")，但仍維持非華語歌曲及古典音樂的宣傳及發行於2006年[EMI年中報告中](../Page/EMI.md "wikilink")，有提及過
**EMI HONG KONG**
的成績，於過去一年，作為EMI業務伙伴，但不持有任何權益的[金牌娛樂佔據了](../Page/金牌大風.md "wikilink")[香港音樂市場超過](../Page/香港音樂.md "wikilink")30%，是其中一間市場佔有率最高的唱片公司，在2013年之前其運作實際由[華納唱片
(香港)維持](../Page/華納唱片_\(香港\).md "wikilink")。但隨着EMI於2013年末被[環球唱片收購後](../Page/環球唱片.md "wikilink")，其運作、音像產品及版權現交由[環球唱片管理](../Page/環球唱片_\(香港\).md "wikilink")。

[抗日歌曲](../Page/抗日.md "wikilink")、現[中華人民共和國國歌](../Page/中華人民共和國國歌.md "wikilink")《[義勇軍進行曲](../Page/義勇軍進行曲.md "wikilink")》在1935年第一次灌錄成唱片發行就是由百代製作的。

## 歌手列表（1979－2002）

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>男</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/羅文.md" title="wikilink">羅文</a><br />
（1979年－1983年）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/陳司翰.md" title="wikilink">陳司翰</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/區瑞強.md" title="wikilink">區瑞強</a><br />
（1983年－1987年）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/葉振棠.md" title="wikilink">葉振棠</a><br />
（1979年－1985年）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>女</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/葉麗儀.md" title="wikilink">葉麗儀</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/曾路得.md" title="wikilink">曾路得</a><br />
（1981年－1982年）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/劉雅麗.md" title="wikilink">劉雅麗</a><br />
（1994年－1996年）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/陳美玲.md" title="wikilink">陳美玲</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/薛家燕.md" title="wikilink">薛家燕</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/江欣慈.md" title="wikilink">江欣慈</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>組合</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/Shine.md" title="wikilink">Shine</a></p></td>
</tr>
</tbody>
</table>

[Category:香港唱片公司](../Category/香港唱片公司.md "wikilink")
[Category:HKRIA版權風波](../Category/HKRIA版權風波.md "wikilink")
[Category:EMI](../Category/EMI.md "wikilink")
[Category:環球唱片](../Category/環球唱片.md "wikilink")