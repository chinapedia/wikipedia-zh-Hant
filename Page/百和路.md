[Pak_Wo_Road_near_CCC_Kei_San_Secondary_School.jpg](https://zh.wikipedia.org/wiki/File:Pak_Wo_Road_near_CCC_Kei_San_Secondary_School.jpg "fig:Pak_Wo_Road_near_CCC_Kei_San_Secondary_School.jpg")\]\]
[Pak_Wo_Road_(west_section_near_Sheung_Shui_Government_Secondary_School).JPG](https://zh.wikipedia.org/wiki/File:Pak_Wo_Road_\(west_section_near_Sheung_Shui_Government_Secondary_School\).JPG "fig:Pak_Wo_Road_(west_section_near_Sheung_Shui_Government_Secondary_School).JPG")\]\]
**百和路**（）是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[北區](../Page/北區_\(香港\).md "wikilink")[粉嶺及](../Page/粉嶺.md "wikilink")[上水發展成新市鎮時興建的一條區域幹道](../Page/上水.md "wikilink")。此路呈東西走向，東起粉嶺[和合石](../Page/和合石.md "wikilink")，接連[馬會道](../Page/馬會道.md "wikilink")；西至上水[彩園邨](../Page/彩園邨.md "wikilink")，接連彩園路。除接連彩園路的一段，全線皆為雙程4線分隔道路，以道路起點的和合石及道路中段所佔用的原[百福村道命名](../Page/百福村道.md "wikilink")。

## 特色

[新市鎮內的粉嶺南發展區基本是以百和路作中軸線向外發展](../Page/新市鎮.md "wikilink")，另外此路是由粉嶺南前往上水（經掃管埔路或彩園路）及粉嶺北（經馬會道）最直接的道路，所以使用率不低，因此大部分由區外進入北區的[巴士路線也會在繞經粉嶺南後](../Page/巴士.md "wikilink")，取道百和路前往上水或[聯和墟](../Page/聯和墟.md "wikilink")，反方向亦然。在每年[清明節及](../Page/清明節.md "wikilink")[重陽節時](../Page/重陽節.md "wikilink")，不少行人及特別巴士路線亦取道百和路來往[和合石墳場及](../Page/和合石墳場.md "wikilink")[粉嶺站](../Page/粉嶺站.md "wikilink")。

## 歷史

百和路於修築時的編號為D6（馬會道至掃管埔路）（其中一部分是由百福村道改建而成\[1\]）及L10（餘下部分，另加上彩園路）\[2\]。2008年，香港協辦[北京奧運中的](../Page/2008年夏季奧林匹克運動會.md "wikilink")[馬術比賽](../Page/2008年夏季奧林匹克運動會馬術比賽.md "wikilink")，其中在8月11日越野賽舉行當日，於百和路[粉嶺鐵路站外設置其中一個穿梭巴士站](../Page/粉嶺站.md "wikilink")，來往位於上水[雙魚河的賽場](../Page/雙魚河.md "wikilink")，並實施一系列交通及人流改道措施。當時亦有[區議員在](../Page/區議員.md "wikilink")[北區區議會的會議上表示](../Page/北區區議會.md "wikilink")，擔心是次措施會令本來在上班時間經已繁忙的百和路人流與車流再加添上大壓力，影響北區往區外上班的人士，以及北區居民的正常生活。\[3\]

## 未來發展

百和路暫時未有道路擴展計劃或其他相關規劃。

## 沿路景點

  - [蓬瀛仙館](../Page/蓬瀛仙館.md "wikilink")

## 交匯街道

<table>
<thead>
<tr class="header">
<th><p>東行</p></th>
<th><p>西行</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><em>東接<a href="../Page/馬會道.md" title="wikilink">馬會道</a></em></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/9號幹線.md" title="wikilink">9號幹線</a>7C出口</p></td>
<td><p>和興路</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9號幹線.md" title="wikilink">9號幹線</a>7B出口</p></td>
</tr>
<tr class="even">
<td><p>華明路、一鳴路</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>偉明街</p></td>
</tr>
<tr class="even">
<td><p>華明路、一鳴路</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em>迴旋處</em><br />
蝴蝶山路、置華里、置福圍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>裕泰路</p></td>
<td><p>裕泰路、吉祥街</p></td>
</tr>
<tr class="odd">
<td><p>吉祥街</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>掃管埔路</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>清曉路</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>保健路、保榮路</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em>西接彩園路</em></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 主要建築物及公共設施

從南端[馬會道連接處到北端](../Page/馬會道.md "wikilink")[彩園路交界處](../Page/彩園路.md "wikilink")：

  - [和合石村](../Page/和合石.md "wikilink")
  - [和興村](../Page/和興村.md "wikilink")
  - 和興遊樂場
  - 和興體育館
  - 粉嶺南政府綜合大樓
  - [華心邨](../Page/華心邨.md "wikilink")
  - [花都廣場](../Page/花都廣場.md "wikilink")
  - [景盛苑](../Page/景盛苑.md "wikilink")
  - 偉明街花園
  - [欣盛苑](../Page/欣盛苑.md "wikilink")
  - [香海正覺蓮社佛教普光學校](../Page/香海正覺蓮社佛教普光學校.md "wikilink")
  - 百福田心遊樂場
  - [蝴蝶山村](../Page/蝴蝶山村.md "wikilink")
  - [蓬瀛仙館](../Page/蓬瀛仙館.md "wikilink")
  - [粉嶺鐵路站](../Page/粉嶺站.md "wikilink")
  - [觀宗寺](../Page/觀宗寺.md "wikilink")
  - [百福村](../Page/百福村.md "wikilink")
  - [粉嶺置福圍](../Page/粉嶺置福圍.md "wikilink")
  - [中華基督教會基新中學](../Page/中華基督教會基新中學.md "wikilink")
  - [警察機動部隊總部](../Page/警察機動部隊.md "wikilink")
  - 百和路遊樂場
  - [嘉福邨](../Page/嘉福邨.md "wikilink")
  - [嘉盛苑](../Page/嘉盛苑.md "wikilink")
  - 百福兒童遊樂場
  - [翠彤苑](../Page/翠彤苑.md "wikilink")
  - [蔚翠花園](../Page/蔚翠花園.md "wikilink")
  - [華慧園](../Page/華慧園.md "wikilink")
  - [欣翠花園](../Page/欣翠花園.md "wikilink")
  - [海裕苑](../Page/海裕苑.md "wikilink")
  - [維也納花園](../Page/維也納花園.md "wikilink")
  - 維瀚路花園
  - 保榮路遊樂場
  - [上水官立中學](../Page/上水官立中學.md "wikilink")
  - [吳屋村](../Page/吳屋村.md "wikilink")
  - 保榮路體育館
  - 保榮路寵物公園
  - [威尼斯花園](../Page/威尼斯花園.md "wikilink")
  - [太平邨](../Page/太平邨.md "wikilink")
  - [彩園邨](../Page/彩園邨.md "wikilink")
  - [旭蒲苑](../Page/旭蒲苑.md "wikilink")
  - [上水鐵路站](../Page/上水站.md "wikilink")

<File:Fung> Ying Seen Koon01.jpg|百和路旁的蓬瀛仙館 <File:Ka> Fuk Estate.JPG|嘉福邨
<File:Pak> Wo Road Playground.JPG|百和路遊樂場

## 途經公共交通

<div class="NavFrame collapsed" style="color: gray; background-color: Yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: Yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

百和路交通路線列表

</div>

<div class="NavContent" style="background-color: Yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{東鐵綫色彩}}">█</font>[東鐵綫](../Page/東鐵綫.md "wikilink")：[粉嶺站](../Page/粉嶺站.md "wikilink")、[上水站](../Page/上水站.md "wikilink")

<!-- end list -->

  - [港鐵](../Page/港鐵.md "wikilink")[巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [九龍巴士](../Page/九龍巴士.md "wikilink")

<!-- end list -->

  - [新界](../Page/新界.md "wikilink")[專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參考資料

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

## 外部連結

  - [百和路地圖](http://www.centamap.com/gc/centamaplocation.aspx?x=831161&y=839859&sx=831162&sy=839859&z=2&lg=b5)

[Category:北區街道 (香港)](../Category/北區街道_\(香港\).md "wikilink") [Category:北區
(香港)](../Category/北區_\(香港\).md "wikilink")
[Category:粉嶺](../Category/粉嶺.md "wikilink")
[Category:上水](../Category/上水.md "wikilink")

1.  [香港地方－討論文庫－粉嶺道路更名](http://www.hk-place.com/db.php?post=d006026)
2.  [香港地方－道路及鐵路－未命名道路編號](http://www.hk-place.com/view.php?id=320)
3.  [北區區議會－交通及運輸委員會（2004-2007年度）第21次會議會議記錄](http://www.districtcouncils.gov.hk/archive/north_d/chinese/doc/TT07m21.doc)