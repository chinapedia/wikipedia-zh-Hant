《**包公案**》是[中国](../Page/中国.md "wikilink")[明朝末年的](../Page/明朝.md "wikilink")[公案小说](../Page/公案小说.md "wikilink")。《包公案》全名**增像包龙图判百家公案**，为[中国古代](../Page/中国.md "wikilink")[公案小说](../Page/公案小说.md "wikilink")，全书共十卷，由[明朝](../Page/明朝.md "wikilink")[安遇时等所作](../Page/安遇时.md "wikilink")。以[北宋清官](../Page/北宋.md "wikilink")[包拯為主角](../Page/包拯.md "wikilink")，叙述了百余件包公[断案的故事](../Page/审判.md "wikilink")。与《[施公案](../Page/施公案.md "wikilink")》、《[藍公案](../Page/藍公案.md "wikilink")》并称“三公奇案”。施、藍都是[清代的](../Page/清代.md "wikilink")[閩南人](../Page/閩南人.md "wikilink")。

该小说塑造了清正廉明的包拯形象，对后来的《[三侠五义](../Page/三侠五义.md "wikilink")》有重要影响。最早《[宋史](../Page/宋史.md "wikilink")》只记包拯“割牛舌”
一案图。\[1\][胡适在](../Page/胡适.md "wikilink")《三侠五义·序》說包公“是一個箭垛式的人物”，民間的傳說將各種各樣的斷案故事都投射到他身上，元、明兩代的戲曲小說不斷的擴充包公故事，元代有[关汉卿的](../Page/关汉卿.md "wikilink")《包待制三勘蝴蝶梦》、《包待制智斩鲁斋郎》，无名氏的《包待制陈州粜米》，[郑廷玉有杂剧](../Page/郑廷玉.md "wikilink")《包待制智勘后庭花》。

明代成化年間已有公案小说《新刊京本通俗演义增像百家公案全传》，主角是包公審案，最早的《包公案》版本是明万历二十二年（1594年）
朱仁斋与耕堂刊本，前附有《国史本传》、《包待制出身源流》。

《包公案》一書被认为是世界上最早的[推理小说](../Page/推理小说.md "wikilink")。\[2\]但《包公案》中的許多案件的偵破，不見得有精彩的推理工夫，常常是因為冤魂托夢。

## 參見

  - [龍圖公案](../Page/龍圖公案.md "wikilink")
  - [施公案](../Page/施公案.md "wikilink")
  - [鹿洲公案](../Page/鹿洲公案.md "wikilink")

## 參考文獻

  - 參考書目

<!-- end list -->

  - 朱万曙，《明清两代包公戏探微》，《中国古近代文学研究》1991年7月

## 外部链接

  - [《包公案》全文在线阅读(简体)](http://www.zggdwx.com/baogong.html)

[Category:推理小說](../Category/推理小說.md "wikilink")
[Category:明朝小說](../Category/明朝小說.md "wikilink")
[Category:公案小說](../Category/公案小說.md "wikilink")
[Category:包青天題材小說](../Category/包青天題材小說.md "wikilink")

1.  胡適：《中國古典小說研究》
2.