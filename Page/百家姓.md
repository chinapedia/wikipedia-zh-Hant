《**百家姓**》是一本关于[漢姓的书](../Page/漢姓.md "wikilink")，成书於[北宋初](../Page/北宋.md "wikilink")。编者为钱塘（今浙江杭州）的一位士大夫（作者姓名沒有記載），虽然名为百家，实际上不止百家。据有关资料记载
共收录姓氏411个，后增补到504个，其中[单姓](../Page/单姓.md "wikilink")444个、[复姓](../Page/复姓.md "wikilink")60个。

《**百家姓**》与《[三字经](../Page/三字经.md "wikilink")》、《[千字文](../Page/千字文.md "wikilink")》并称“三百千”，是[中国古代幼儿的启蒙读物](../Page/中国古代.md "wikilink")。
由於這些百家姓是按韻編排，每句四字讀起來朗朗上口，便於學習和記憶，因此流傳極廣，至今仍是婦孺皆知。

## 排序

《百家姓》各个姓氏排列次序不是以人口數量多少，而是以政治地位为准则。

根據[南宋學者](../Page/南宋.md "wikilink")[王明清考證](../Page/王明清.md "wikilink")，「[趙](../Page/趙氏.md "wikilink")[錢](../Page/錢姓.md "wikilink")[孫](../Page/孫氏.md "wikilink")[李](../Page/李氏.md "wikilink")」之所以是《百家姓》前四姓，是因為百家姓在北宋初年的[吳越](../Page/江浙.md "wikilink")[錢塘地區形成](../Page/錢塘.md "wikilink")，所以就用當時最重要的家庭姓氏：

1.  [趙氏](../Page/趙氏.md "wikilink")：[宋朝皇帝的姓氏](../Page/宋朝.md "wikilink")。
2.  [錢氏](../Page/錢姓.md "wikilink")：[吳越國王的姓氏](../Page/吳越國.md "wikilink")。
3.  [孫氏](../Page/孫氏.md "wikilink")：吳越國王[錢俶正妃的姓氏](../Page/錢俶.md "wikilink")。
4.  [李氏](../Page/李氏.md "wikilink")：[南唐國王的姓氏](../Page/南唐.md "wikilink")。

而接下來的四個姓氏「[周](../Page/周氏.md "wikilink")[吳](../Page/吳姓.md "wikilink")[鄭](../Page/鄭氏.md "wikilink")[王](../Page/王氏.md "wikilink")」都是吳越開國國王[錢鏐的妻子姓氏](../Page/錢鏐.md "wikilink")。

王明清《玉照新志》記載：“如市井間所印《百家姓》，（王）明清嘗詳考之，以是兩浙錢氏有國時小民所著，何則？其首-{云}-‘趙錢孫李’，蓋錢氏奉正朔，趙本朝國姓，所以錢次之；孫乃忠懿（[錢俶](../Page/錢俶.md "wikilink")）之正妃；又其次，則江南李氏。次句-{云}-‘周吳鄭王’，皆武肅（[錢鏐](../Page/錢鏐.md "wikilink")）而下后妃。”。

## 內容

| **百家姓** |
| ------- |
| 1-3     |
| 4-6     |
| 7-9     |
| 10-12   |
| 13-15   |
| 16-18   |
| 19-21   |
| 22-24   |
| 25-27   |
| 28-30   |
| 31-33   |
| 34-36   |

  - 需要注意以下姓氏的繁简：[-{于}-](../Page/于姓.md "wikilink")、[-{於}-](../Page/於姓.md "wikilink")、[-{郁}-](../Page/郁姓.md "wikilink")、[-{鬱}-](../Page/鬱姓.md "wikilink")、[-{后}-](../Page/后姓.md "wikilink")、[-{後}-](../Page/後姓.md "wikilink")、[淳-{于}-](../Page/淳于姓.md "wikilink")、[單-{于}-](../Page/單于姓.md "wikilink")、[鮮-{于}-](../Page/鮮于姓.md "wikilink")、[锺](../Page/锺姓.md "wikilink")、[鍾離](../Page/鍾離姓.md "wikilink")、[-{党}-](../Page/党姓.md "wikilink")、[-{黨}-](../Page/黨姓.md "wikilink")（-{党}-、-{黨}-百家姓中只取其一）。
  - 最后一句“百家姓终”僅表示書本已到終點。其中[家](../Page/家姓.md "wikilink")、[終与前文重复](../Page/終姓.md "wikilink")，[百](../Page/百姓_\(姓氏\).md "wikilink")、[姓二字虽均可做姓](../Page/姓姓.md "wikilink")，但十分少见。

## 依人口排序

[中國科學院](../Page/中國科學院.md "wikilink")[遺傳與發育生物學研究所研究員](../Page/中國科學院遺傳與發育生物學研究所.md "wikilink")[袁義達按照目前國際上公認的有關姓氏頻率研究方法](../Page/袁義達.md "wikilink")，從迄今為止所發現的收集[宋朝](../Page/宋朝.md "wikilink")、[元朝](../Page/元朝.md "wikilink")、[明朝的文獻](../Page/明朝.md "wikilink")，採集統計樣本，根據每一人物的姓名、籍貫或居住地、工作地區、文獻表明的地點進行統計，從而推算出當時全國姓氏的期望分佈頻率\[1\]

## 漢姓转写

亞洲各地冠漢姓者各使用不同的系統將其轉寫為[拉丁字母](../Page/拉丁字母.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>地區</p></th>
<th><p>漢字讀音標準</p></th>
<th><p>現羅馬化方案</p></th>
<th><p>舊羅馬化方案</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/中國大陸.md" title="wikilink">中國大陸</a></p></td>
<td><p><a href="../Page/普通話.md" title="wikilink">普通話</a></p></td>
<td><p><a href="../Page/漢語拼音.md" title="wikilink">漢語拼音</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/臺灣.md" title="wikilink">臺灣</a></p></td>
<td><p><a href="../Page/中華民國國語.md" title="wikilink">中華民國國語</a></p></td>
<td><p><a href="../Page/威妥瑪拼音.md" title="wikilink">威妥瑪拼音</a>、<a href="../Page/注音二式.md" title="wikilink">注音二式</a>、<a href="../Page/通用拼音.md" title="wikilink">通用拼音</a>、<a href="../Page/漢語拼音.md" title="wikilink">漢語拼音</a></p></td>
<td></td>
<td><p>存在各方案混用的情況</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td><p><a href="../Page/粵語.md" title="wikilink">粵語</a></p></td>
<td><p><a href="../Page/香港政府粵語拼音.md" title="wikilink">香港政府粵語拼音</a></p></td>
<td></td>
<td><p>政府允許使用漢語拼音註冊新生嬰兒名字 存在姓與名分別用漢語拼音與廣州話，或相反的情況</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/澳門.md" title="wikilink">澳門</a></p></td>
<td><p><a href="../Page/澳門政府粵語拼音.md" title="wikilink">澳門政府粵語拼音</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北韓.md" title="wikilink">北韓</a></p></td>
<td><p><a href="../Page/韓國漢字音.md" title="wikilink">韓國漢字音</a></p></td>
<td><p><a href="../Page/馬科恩-賴肖爾表記法.md" title="wikilink">馬科恩-賴肖爾表記法</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南韓.md" title="wikilink">南韓</a></p></td>
<td><p><a href="../Page/文化观光部2000年式.md" title="wikilink">文化觀光部國語羅馬字表記法</a></p></td>
<td><p><a href="../Page/馬科恩-賴肖爾表記法.md" title="wikilink">馬科恩-賴肖爾表記法</a></p></td>
<td><p>有舊方案姓氏搭配新方案名字的情況</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td><p><a href="../Page/漢越音.md" title="wikilink">漢越音</a></p></td>
<td><p><a href="../Page/國語字.md" title="wikilink">越南國語字</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
<td><p>無，根據各地華人母語發音拼寫，包括<a href="../Page/泉漳話.md" title="wikilink">泉漳話</a>、<a href="../Page/潮州話.md" title="wikilink">潮州話</a>、<a href="../Page/廣州話.md" title="wikilink">廣州話</a>、<a href="../Page/客家話.md" title="wikilink">客家話</a>、<a href="../Page/福州話.md" title="wikilink">福州話</a>、<a href="../Page/海南話.md" title="wikilink">海南話等</a></p></td>
<td><p>現以現代標準漢語為標準華語， 但改寫姓氏會帶來法律問題，</p>
<p>因此方言音姓氏搭配漢語拼音名字的情況十分常見</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬來西亞.md" title="wikilink">馬來西亞</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印尼.md" title="wikilink">印尼</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

由於人口流動，一個地區的翻譯有時候會在另一個地區出現，本表只列出該地區政府使用的標準轉寫系統。

## 参考文献

## 外部連結

  - [黃帝祠-百家姓](https://web.archive.org/web/20100122161522/http://www.chinaemperorhall.com/index.asp)

  - [族譜](https://web.archive.org/web/20101021220454/http://big5.chinataiwan.org/zppd/)

  - [民俗:百家姓氏](https://web.archive.org/web/20081023034443/http://www.ccler.com/minsu/baijiaxing/)

  - [百家姓氏查询](http://www.yueyilife.com/html/79/)

## 参见

  - [漢姓](../Page/漢姓.md "wikilink")
  - [百姓](../Page/百姓.md "wikilink")
  - [姓氏](../Page/姓氏.md "wikilink")
  - [氏](../Page/氏.md "wikilink")
  - [氏族志](../Page/氏族志.md "wikilink")
  - [姓名学](../Page/姓名学.md "wikilink")

{{-}}

[漢字姓氏](../Category/漢字姓氏.md "wikilink")
[B百](../Category/北宋典籍.md "wikilink")
[Category:蒙學](../Category/蒙學.md "wikilink")
[Category:中国名数100](../Category/中国名数100.md "wikilink")

1.  [“百家姓”排列终有序
    姓氏文化有何内涵？](http://news.xinhuanet.com/focus/2006-03/05/content_4232963.htm)（新华网）