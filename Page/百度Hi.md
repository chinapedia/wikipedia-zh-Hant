**百度Hi**，是[百度公司开发的一款](../Page/百度.md "wikilink")[即时通讯软體](../Page/即时通讯软體.md "wikilink")。百度Hi具備文字消息、視訊、通話、文件傳輸等功能。

## 版本

2008年中國舉辦[北京奧運會](../Page/北京奧運會.md "wikilink")，百度隨即在北京奧運會開幕的前一天推出了「百度Hi
奧運版」（百度Hi 2.0
Beta1）。與原先的版本相比，功能方面增加了「添加奧運獎牌」功能，此功能可以在個性簽名中顯示奧運金牌的標識和中國代表隊在本屆奧運會上奪得的[金牌數量](../Page/金牌.md "wikilink")，並即時更新。此功能在北京奧運會閉幕後停止。<small>\[1\]</small>

百度推出类似[QQ活跃等级制的活跃度服务](../Page/QQ.md "wikilink")。\[2\]

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [百度Hi](http://im.baidu.com/)
  - [百度Hi最新動態官方博客](http://hi.baidu.com/baiduhi/)

[Category:Windows即时通讯客户端](../Category/Windows即时通讯客户端.md "wikilink")
[Category:百度软件](../Category/百度软件.md "wikilink")

1.  [百度Hi奧運版上線啦_baiduhi](http://hi.baidu.com/baiduhi/blog/item/025422d394822fd9a8ec9a27.html)
2.  [百度Hi - 活跃度](http://im.baidu.com/html/wealth/grade.html)