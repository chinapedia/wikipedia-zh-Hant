**皆川亮二**、日本[漫画家](../Page/漫画家.md "wikilink")。[東京都](../Page/東京都.md "wikilink")[墨田區出身](../Page/墨田區.md "wikilink")。代表作是〈[ARMS神臂](../Page/ARMS神臂.md "wikilink")〉、〈[轟天高校生](../Page/轟天高校生.md "wikilink")〉。

## 作品

1988年推出第壹本個人作品《Heaven》

  - [轟天高校生](../Page/轟天高校生.md "wikilink")、全11冊、原[東立代理](../Page/東立.md "wikilink")、現已絕版。原名《》（{{lang|ja|[高樹宙原作](../Page/高樹宙.md "wikilink")）

<!-- end list -->

  -
    劇場版動畫電影《Spriggan》，由[大友克洋於](../Page/大友克洋.md "wikilink")1998年製作，以此漫畫的「諾亞方舟篇」和「御神苗抹殺計畫」二篇章為劇本。（台版DVD，《世界末日》，由[普威爾國際代理](../Page/普威爾國際.md "wikilink")。

<!-- end list -->

  - 《KYO》（1995年）
  - [ARMS神臂](../Page/ARMS神臂.md "wikilink")、全22冊（東立版）、原名《》
  - [D-LIVE\!\~生存競爭](../Page/D-LIVE!~生存競爭.md "wikilink")、全15冊（東立版）、原名《》<ref>D-LIVE\!\~生存競爭

<div class="references-small" style="-moz-column-count:3; column-count:3;">

1.  ISBN 9861129405
2.  ISBN 9861129413
3.  ISBN 9861140530
4.  ISBN 9861140549
5.  ISBN 9861145214
6.  ISBN 9861152768
7.  ISBN 9861155902
8.  ISBN 9861160450
9.  ISBN 9861165029
10. ISBN 9861169504
11. ISBN 9861169512
12. ISBN 986118001X
13. ISBN 9861181598
14. ISBN 9861185410
15. ISBN 9861189394

</div>

</ref>

  - 、全11冊、原名《》

  - [和平捍衛者](../Page/和平捍衛者.md "wikilink")（東立版）；PEACE
    MAKER（[玉皇朝版](../Page/玉皇朝.md "wikilink")）、原名《》、全17冊。

## 參考資料

## 助手

  - [夏目義徳](../Page/夏目義徳.md "wikilink")
  - [田中顕](../Page/田中顕.md "wikilink")
  - [森尾正博](../Page/森尾正博.md "wikilink")

## 外部連結

  -
  -
[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")