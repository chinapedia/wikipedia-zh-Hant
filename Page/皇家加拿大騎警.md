**加拿大皇家騎警**（[英文](../Page/英文.md "wikilink")：****，縮寫：**RCMP**；[法文](../Page/法文.md "wikilink")：****，縮寫：**GRC**），既是[加拿大的](../Page/加拿大.md "wikilink")[聯邦警察](../Page/聯邦警察.md "wikilink")（Federal
Police），在加拿大各地負責執行聯邦法規，又通過與三個特區、八個省（[安大略及](../Page/安大略.md "wikilink")[魁北克除外](../Page/魁北克.md "wikilink")）、200個城鎮、及近200個原住民社區簽訂的合約提供全加拿大的[國民警察](../Page/國民警察.md "wikilink")（National
Police
Service），保持加拿大人民及社區的安全；加拿大皇家騎警是[聯邦制](../Page/聯邦制.md "wikilink")[國家中獨有的集聯邦警察](../Page/國家.md "wikilink")、省警、市警於一體的警務機構。加拿大皇家騎警亦是[加拿大的](../Page/加拿大.md "wikilink")[國家象徵之一](../Page/國家象徵.md "wikilink")。

加拿大皇家骑警的前身是於1873年建立的「[西北骑警](../Page/西北骑警.md "wikilink")」，那时只是设立农村警务的一个「临时性的尝试」。

## 起源

[Nwmp_lancer.jpg](https://zh.wikipedia.org/wiki/File:Nwmp_lancer.jpg "fig:Nwmp_lancer.jpg")
[加拿大在殖民初期并没有较强大的](../Page/加拿大.md "wikilink")[警察隊伍](../Page/警察.md "wikilink")。即使在1867年加拿大建国时，[蒙特利尔和](../Page/蒙特利尔.md "wikilink")[多伦多这些最大的城市中的专职警察也很少](../Page/多伦多.md "wikilink")。当时只有一支规模很小的国家警察队伍来负责贯彻实施联邦法律。小城镇和农村地区甚至都没有警察，执法的任务由法庭任命的临时警察或士兵来承担。

1870年，[加拿大从](../Page/加拿大.md "wikilink")[哈德逊湾公司手中买下了](../Page/哈德逊湾公司.md "wikilink")[美国边界以北从](../Page/美国.md "wikilink")[五大湖到](../Page/五大湖.md "wikilink")[洛磯山脈之间的一大片土地](../Page/洛磯山脈.md "wikilink")，加拿大政府觉得在这人口稀少的广大地区，需要有一支有效的执法隊伍，以防止大批白人拓荒者突然湧入，與传统印第安人之間造成之衝突，特別是处理土地问题。为保证白人拓荒者，與及印第安人都能得到公平的待遇，政府决定组建一支警备队来维持秩序，直至西部土地由加拿大政府正式和平接管為止。这支「警备隊」成立于1873年，称为西北骑警。原来计划土地问题和平解决后就解散该警备队。

最初，西北骑警只招募了150人，但是，很快就增加到300人。当时的西北骑警骑马巡逻西北地区，穿的就是现在著名的红色紧身短上衣。

## 发展

[Nwmp_1900.jpg](https://zh.wikipedia.org/wiki/File:Nwmp_1900.jpg "fig:Nwmp_1900.jpg")地區的西北骑警\]\]
西北骑警开始时只有300名骑警，他們要保卫与[美国北部边界接壤的广阔地带人民的安全](../Page/美国.md "wikilink")。那时加拿大西部地区比美国西部还大。在美國，印第安人與白人拓荒者之間，經常發生很多武力衝突，以至战争。加拿大政府有見及此，加強了對印第安原居民的保護，並建立了這支西北骑警来处理這地區的土地问题，保证印第安人與白人均得到公正的待遇。因此得到印第安人的信赖，称他们为白人兄弟。这对印第安人面对条约谈判，并调解与白人拓荒者之间的冲突，產生了积极的作用。

1883年，西北骑警扩充到了500人，并負有若干新的职责，其中包括在兴建加拿大太平洋铁路期间，维持治安的责任。1885年由路易.雷尔领导的梅迪人暴动（西北叛乱）以后，这支警力再一次增加，达到1000人。

在19世纪末和20世纪初，加拿大西北的[育空地区掀起了大规模的](../Page/育空.md "wikilink")[淘金热](../Page/淘金热.md "wikilink")，世界各地众多的采金者都汇集到这里，也同时带来了发生暴乱的可能。西北骑警的存在保证了这场淘金热井然有序，使暴乱冲突尽量减少。淘金热以后，西北骑警把注意力转到北极地区，并在那里设置分局以制止欺压原住民的事件，同时避免了欧洲国家对加拿大领土的威胁。

20世纪初，加拿大人已公认西北骑警是一个永久性的机构。1904年，英王[爱德华七世在这支隊伍的名称上](../Page/爱德华七世.md "wikilink")，赐封「皇家」[称号](../Page/称号.md "wikilink")，以表彰其事蹟。1920年，「皇家西北骑警」成为「**皇家加拿大骑警**」，正式扩大成为国家的武裝警察。同年，其总部从[萨斯克奇温省](../Page/萨斯喀彻温.md "wikilink")（Saskatchewan）的[首府](../Page/首府.md "wikilink")[里贾纳市迁到](../Page/里贾纳.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[首都](../Page/首都.md "wikilink")[渥太华市](../Page/渥太华.md "wikilink")。

1928年，「加拿大皇家骑警」根据与各个不同省市签定的合同，开始在联邦政府辖区以外的各个地区执行警务。加拿大[宪法规定执法任务是各省的职责](../Page/宪法.md "wikilink")，但大多数省份都认为只有通过加拿大皇家骑警才能最有效地履行这一职责。加拿大皇家骑警的任务基本上是「维护治安」，但它也对加拿大在大战时的努力，作出很大的贡献。加拿大皇家骑警在海外也有过贡献，参与了[第二次布尔战争和两次](../Page/第二次布尔战争.md "wikilink")[世界大战](../Page/世界大战.md "wikilink")。

## 今日皇家加拿大骑警

[Rcmp_sled_dogs_1957.jpg](https://zh.wikipedia.org/wiki/File:Rcmp_sled_dogs_1957.jpg "fig:Rcmp_sled_dogs_1957.jpg")
[Northwest_Mounted_Police_re-enactors.jpg](https://zh.wikipedia.org/wiki/File:Northwest_Mounted_Police_re-enactors.jpg "fig:Northwest_Mounted_Police_re-enactors.jpg")
今天的加拿大皇家骑警由一名总监(专员)统领。该总监向[联邦政府报告工作](../Page/加拿大政府.md "wikilink")，同时也向那些由加拿大皇家骑警提供警察服务的各省检查长报告工作。

这支警力拥有16000名维持治安的警官和大约5,000名文职人员，在全国各地设有6个刑事侦察实验室，在[渥太华市設有電腦化的警务信息中心和加拿大警官学院](../Page/渥太华.md "wikilink")，在[里賈納市还设有一个培训学校](../Page/里賈納.md "wikilink")。警官学院给加拿大其他警力和世界各国的警力队伍提供各种先进的培训项目。

皇家加拿大骑警的主要职责包括：

  - 执行大约200个城市与市镇的市警力的任务；
  - 根据签订的合同书，在育空地区、西北地区和加拿大十个省当中的八个省，提供警察服务（[安大略省和](../Page/安大略省.md "wikilink")[魁北克省各有其自己的警力](../Page/魁北克省.md "wikilink")）；
  - 负责贯彻执行大约140项联邦的法律与法规，涉及范围包括辑毒、商业犯罪、管理移民、与护照事宜、关税与货物税以及货币伪造案等；
  - 在国际上代表加拿大作为[国际刑警组织的一个成员](../Page/国际刑警.md "wikilink")，同时也在30个国家派有联络官；
  - 1984年，加拿大安全情报局接收了皇家加拿大骑警的搜集情报的职责；但皇家加拿大骑警仍负责执行国家安全任务。

多年来皇家加拿大骑警已从小小的、临时性的农村警力，演变为享有国际声誉的一支警察力量。但纵观其历史，它一贯强调和平解决分歧，只有在不得已的情况下才動用武力。加拿大皇家骑警的[座右铭是](../Page/座右铭.md "wikilink")「维护正义」。100多年以来，在国内外这支警备队一直是加拿大独具特色的象征。

加拿大的骑警比警察多，要成为骑警，需经数月艰苦训练。训练结束时，这些骑警更像一名士兵，一名特殊的士兵。他们要经历当勘探队员、樵夫、登山者、护士、炊事员、侦探、绘制地图、记者等种种训练。要能像西部牛仔一样骑马，像[印第安人一样用桨划独木舟](../Page/印第安人.md "wikilink")，穿上雪靴能行走，像[爱斯基摩人一样赶乘狗拉雪橇](../Page/爱斯基摩人.md "wikilink")。当前，加拿大全国都有加拿大皇家骑警在執勤。

## 組織

皇家加拿大騎警是在《皇家加拿大騎警法》（Royal Canadian Mounted Police
Act）的授權下設立並組織，指挥官為皇家騎警警监（Commissioner），由[加拿大總督任命](../Page/加拿大總督.md "wikilink")、对[公共安全部](../Page/加拿大公共安全部.md "wikilink")（Public
Safety
Canada）负责。截至2005年4月，皇家加拿大騎警有警監一人、副警監七人（分管：聯邦服務及中央區，大西洋區，國民警察，企業化管理暨主計處，戰略指揮，運營，西北區，太平洋區）、共有職員22,561名。加拿大皇家騎警總部設於[渥太華](../Page/渥太華.md "wikilink")。

### 2005年4月的警力狀況：

  - 警监 Commissioner 1
  - 副警监 Deputy Commissioner 7
  - 助理警监 Assistant Commissioner 24
  - 總警司 Chief Superintendent 52
  - 警司 Superintendent 143
  - 警督 Inspectors 346
  - 警队总警长 Corps Sergeant Major 1
  - 总警长 Sergeant Major 6
  - 副总警长 Staff Sergeant Major 5
  - 高级警长 Staff Sergeant 742
  - 警长 Sergeant 1,616
  - 警目 Corporal 2,928
  - 警員 Constable 10,136
  - 特勤警員(非警衔，系任命) Special Constable 82
  - 文职 Civilian Member 2,605
  - 公務員 Public Servant 3,867
  - 共計 22,561

### 高層領導：

  - 警監 (Bob Paulson)
  - 副警監（Paul Gauvin）：企業化管理暨主計處（Corporate Management and Comptrollership）
  - 副警監（Gerry Braun）：西北區（North West Region）
  - 副警監（Bev Busson）：太平洋區（Pacific Region）
  - 副警監（Timothy Killam）：運營及整合（Operations and Integration）
  - 副警監（Pierre-Yves Bourduas）：聯邦服務及中央區（Federal Services and Central
    Region）
  - 副警監（Harper Boucher）：大西洋區（Atlantic Region）
  - 副警監（Peter Martin）：國民警察（National Police Services）
  - 助理警監（Barbara George）：人力資源官（Chief Human Resources Officer）
  - 助理警監（Rodney Smith）：品行指導官（Ethics and Integrity Advisor）

## 参考文献

## 外部連結

  - [加拿大皇家騎警官方網站](http://www.rcmp-grc.gc.ca/)

[Cat:骑警](../Page/Cat:骑警.md "wikilink")

[Category:加拿大警察](../Category/加拿大警察.md "wikilink")
[Category:国家宪兵](../Category/国家宪兵.md "wikilink")