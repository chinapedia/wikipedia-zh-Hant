**皇家北方音乐学院 （Royal Northern College of
Music）**，是欧洲重要的[音乐学院之一](../Page/音乐学院.md "wikilink")，坐落在[英国](../Page/英国.md "wikilink")[英格兰西北部城市](../Page/英格兰.md "wikilink")[曼彻斯特](../Page/曼彻斯特.md "wikilink")。

## 历史

这所学院的历史可以追溯到19世纪晚期[哈雷交响乐团创始人](../Page/哈雷交响乐团.md "wikilink")[查尔斯·哈雷爵士创办的](../Page/查尔斯·哈雷爵士.md "wikilink")**曼彻斯特皇家音乐学院**，在1973年，曼彻斯特皇家音乐学院与北方音乐学校合并组成现在的皇家北方音乐学院。

目前开设[音乐学系](../Page/音乐学.md "wikilink")，[作曲与](../Page/作曲.md "wikilink")[当代音乐系](../Page/当代音乐.md "wikilink")，[键盘乐系](../Page/键盘.md "wikilink")，[弦乐系](../Page/弦乐.md "wikilink")，[声乐与](../Page/声乐.md "wikilink")[歌剧和](../Page/歌剧.md "wikilink")[弦管系等](../Page/弦管.md "wikilink")[本科及](../Page/本科.md "wikilink")[研究生课程](../Page/研究生.md "wikilink")。

## 参考来源

  - Kennedy, Michael (1971) *The History of the Royal Manchester College
    of Music*. Manchester University Press
  - [Royal Manchester College of Music
    Archive](http://www.nationalarchives.gov.uk/a2a/records.aspx?cat=1179-rmcm&cid=0#0):
    National Archives
  - [View of the
    college](https://web.archive.org/web/20100528220945/http://www.rncm.ac.uk/content/view/127/151/)

## 外部链接

  - [Royal Northern College of Music](http://www.rncm.ac.uk/)

[Category:英格兰大学](../Category/英格兰大学.md "wikilink")
[Category:曼彻斯特教育](../Category/曼彻斯特教育.md "wikilink")
[Category:英國音樂學校](../Category/英國音樂學校.md "wikilink")
[Category:1973年創建的教育機構](../Category/1973年創建的教育機構.md "wikilink")