**皇家客運有限公司**（英語：Royal
Bus），簡稱**皇家客運**，成立於1997年，初期主要經營服務的範圍涵蓋國民旅遊、各級學校校外教學、畢業旅行、各機關團體相關業務、機場接送機服務等各項租車業務，目前也經營公路客運。該公司的總部位於新北市金山區中山路，主要的客運路線是1717路公路客運，由[台北車站區經](../Page/台北車站.md "wikilink")[陽金公路至](../Page/陽金公路.md "wikilink")[新北市金山海邊](../Page/金山區_\(台灣\).md "wikilink")。其在市區的部份是有固定站牌；行駛在山區路段時，乘客可以隨招隨停。
另皇家客運已於2011年12月汰舊部份車輛，採購[臺灣宇通低地板車輛](../Page/臺灣宇通.md "wikilink")（車號為591-FU\~597-FU）與舊車交叉發車，2012年12月則是購入[三陽金龍的乙類大客車](../Page/三陽金龍.md "wikilink")。
[Taipei_Station_bus_stop_board_of_Royal_Bus_20100608.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Station_bus_stop_board_of_Royal_Bus_20100608.jpg "fig:Taipei_Station_bus_stop_board_of_Royal_Bus_20100608.jpg")

## 使用車種

### [中國大陸](../Page/中華人民共和國.md "wikilink")[宇通（Yutong）](../Page/宇通客車.md "wikilink")（[臺灣關係企業](../Page/臺灣.md "wikilink")：全球宇通實業）

[皇家客運597-FU_公園路_20170211.jpg](https://zh.wikipedia.org/wiki/File:皇家客運597-FU_公園路_20170211.jpg "fig:皇家客運597-FU_公園路_20170211.jpg")

  - 組件進口車（座椅及底盤部份骨架為臺灣在地生產）
  - 引擎：康明斯(Cummins)ISBe4+250B
      - 排氣量：6692.4cc
      - 最大馬力：250ps(180.5kW)/2300rpm
      - 最大扭力：102.8kg-m/1200\~2600rpm
  - 變速系統：ZF Ecomat4 6HP504C 6速自排(附3.2.1檔選擇鈕)
  - 車長 11,990X2,500X3,150mm
  - 軸距 5875mm
  - 領牌年份： 2011
  - 領牌範圍：591-FU\~597-FU
  - 1717:591-FU\~597-FU

### [中國](../Page/中國.md "wikilink")[廈門金龍 （KINGLONG）](../Page/金龍汽車.md "wikilink")（台灣關係企業：[三陽金龍](../Page/三陽金龍.md "wikilink")

  - 車體廠：原廠
  - 引擎康明斯(Cummins)ISBe4+185
      - 排氣量：4461.6cc
      - 最大馬力：183HP(131.7kW)/2250rpm
      - 最大扭力：70.16kg-m/1200rpm
  - 變速系統：手排(前6後1)
  - 車長 -----mm
  - 軸距 -----mm
  - 領牌年度 2012
  - 領牌車號 610-FZ～612-FZ
  - 612-FZ已淘汰
  - 1717：610-FZ\~611-FZ

## 歷史車輛

<File:RoyalBus> 130FB Front.jpg|皇家客運目前使用的Fuso MP <File:RoyalBus>
125AB.jpg|皇家客運的Volvo B10停在[台大醫院公園路圍牆邊](../Page/台大醫院.md "wikilink")
<File:Royal> Bus 138FB - left & front.jpg|皇家客運的Fuso MP紅色塗裝版的左前方外觀
<File:Royal> Bus 138FB - left & back.jpg|皇家客運的Fuso MP紅色塗裝版的左後方外觀

## 關係企業

  - [金海岸通運有限公司](../Page/金海岸通運有限公司.md "wikilink")（車輛支援）
  - [皇嘉遊覽車客運股份有限公司](../Page/皇嘉遊覽車客運股份有限公司.md "wikilink")（共用場站）

## 參見

  - [中華民國汽車客運業者列表](../Page/中華民國汽車客運業者列表.md "wikilink")

## 外部連結

  - [皇家客運](http://www.royalbus.com.tw/)

[Category:台灣客運公司](../Category/台灣客運公司.md "wikilink")
[Category:總部位於新北市的工商業機構](../Category/總部位於新北市的工商業機構.md "wikilink")