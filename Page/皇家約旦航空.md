**皇家約旦航空**\[1\]（，，也被称为**约旦皇家航空**\[2\]）是一家总部设在[約旦](../Page/約旦.md "wikilink")[安曼的](../Page/安曼.md "wikilink")[航空公司](../Page/航空公司.md "wikilink")。其国际航线覆盖了4大洲。其主要基地位于安曼的[阿勒娅王后国际机场](../Page/阿勒娅王后国际机场.md "wikilink")（AMM）\[3\]。

皇家约旦航空是[阿拉伯航空运输组织的成员](../Page/阿拉伯航空运输组织.md "wikilink")，并于2007年4月加入[寰宇一家航空联盟](../Page/寰宇一家.md "wikilink")，是第一家加入全球三大航空聯盟之一的中東籍航空公司，同年也獲《航空財經時報》選為最佳航空公司。

## 歷史

### 初期

皇家約旦航空於1963年12月9日成立，當時公司名稱為「Alia Royal Jordanian
Airlines」。6天後（12月15日）正式開始營運。約旦航空是由私人股東等集資金成立的，但由政府接管。

約旦航空初始時，擁有兩架[Handley Page Dart
Herald及一架](../Page/ld.md "wikilink")[DC-7客機](../Page/DC-7.md "wikilink")，服務往返安曼與[科威特城](../Page/科威特城.md "wikilink")、[贝鲁特](../Page/贝鲁特.md "wikilink")、[开罗的航線](../Page/开罗.md "wikilink")。1964年，第二架DC-7客機投入服務，[吉达航線也於同年開辦](../Page/吉达.md "wikilink")。1965年開辦首條歐洲航線（[羅馬](../Page/罗马市.md "wikilink")）。以色列在1967年的一次對約旦的空襲中，令約旦航空的兩架DC-7遭到破壞，及後由兩架[福克F27飛機取代](../Page/福克F27.md "wikilink")。

1968年，約旦航空開始擴充航線，在該年開辦的航點有[尼科西亚](../Page/尼科西亚.md "wikilink")、[班加西](../Page/班加西.md "wikilink")、[宰赫兰](../Page/宰赫兰.md "wikilink")、[多哈](../Page/多哈.md "wikilink")。翌年開通[慕尼黑](../Page/慕尼黑.md "wikilink")、[伊斯坦堡](../Page/伊斯坦堡.md "wikilink")、[德黑兰航線](../Page/德黑兰.md "wikilink")。

### 1970年代

[Boeing_707-3D3C_JY-ADO_Alia_LHR_22.08.71_edited-2.jpg](https://zh.wikipedia.org/wiki/File:Boeing_707-3D3C_JY-ADO_Alia_LHR_22.08.71_edited-2.jpg "fig:Boeing_707-3D3C_JY-ADO_Alia_LHR_22.08.71_edited-2.jpg")\]\]
70年代是約旦航空的大幅擴展時期。

首先，約旦航空進入噴射客機年代，[波音707在](../Page/波音707.md "wikilink")1971年加入機隊，並開辦[法兰克福](../Page/法兰克福.md "wikilink")、[阿布札比航點](../Page/阿布札比.md "wikilink")，稍後再增設[马德里](../Page/马德里.md "wikilink")、[哥本哈根](../Page/哥本哈根.md "wikilink")、[卡拉奇航點](../Page/卡拉奇.md "wikilink")。除波音707外，在七十年代加入約旦航空的噴射客機還有波音720和[波音747](../Page/波音747.md "wikilink")。

此外，空廚部正式成立，另於安曼機場開設免稅店，同時也開大量航點，包括[巴林](../Page/巴林.md "wikilink")、[杜拜](../Page/杜拜.md "wikilink")、[马斯喀特](../Page/马斯喀特.md "wikilink")、[拉巴特](../Page/拉巴特.md "wikilink")、[日內瓦](../Page/日內瓦.md "wikilink")、[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")、[巴格达](../Page/巴格达.md "wikilink")、[曼谷](../Page/曼谷.md "wikilink")、[維也納](../Page/維也納.md "wikilink")
、[拉纳卡](../Page/拉纳卡.md "wikilink")（取代尼科西亞）、[大马士革](../Page/大马士革.md "wikilink")、[紐約](../Page/紐約.md "wikilink")、[休斯敦](../Page/休斯敦.md "wikilink")、[拉斯海瑪酋長國](../Page/拉斯海瑪酋長國.md "wikilink")。1979年，約旦航空更成為阿拉伯航空技術聯盟（Arab
Airlines Technical Consortium，AATC）的始創成員之一。

### 1980年代

[Royal_Jordanian_Airlines_L-1011_in_Geneva.jpg](https://zh.wikipedia.org/wiki/File:Royal_Jordanian_Airlines_L-1011_in_Geneva.jpg "fig:Royal_Jordanian_Airlines_L-1011_in_Geneva.jpg")\]\]
[的黎波里和](../Page/的黎波里.md "wikilink")[突尼斯航線在](../Page/突尼斯.md "wikilink")80年代開辦。80年代也是約旦航空踏入電腦化的年代。同時，[洛歇L-1011](../Page/洛歇L-1011.md "wikilink")、[空中巴士A310](../Page/空中客车A310.md "wikilink")、[A320加入機隊](../Page/空中客车A320.md "wikilink")。

不得不提的是，Alia在1986年正式改名為現時的Royal Jordanian
Airlines，首名女性飛行員也於同年加入。80年代開辦的航點有[贝尔格莱德](../Page/贝尔格莱德.md "wikilink")、[芝加哥](../Page/芝加哥.md "wikilink")、[洛杉矶](../Page/洛杉矶.md "wikilink")、[邁阿密](../Page/邁阿密.md "wikilink")、[布加勒斯特](../Page/布加勒斯特.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[利雅得](../Page/利雅得.md "wikilink")、[吉隆坡](../Page/吉隆坡.md "wikilink")、[萨那](../Page/萨那.md "wikilink")、[莫斯科](../Page/莫斯科.md "wikilink")、[蒙特利尔](../Page/蒙特利尔.md "wikilink")、[德里](../Page/德里.md "wikilink")、[加尔各答](../Page/加尔各答.md "wikilink")、[安哥拉](../Page/安哥拉.md "wikilink")。

### 1990年代

皇家約旦航空在90年代再作進一步擴展。皇家約旦航空與其餘9家阿拉伯地區的航空公司簽訂協議，共同使用伽利略預訂系統（Galileo
Reservations
System），此外也加設IMCS維修及工程系統，並在安曼市第7環增設新的航空大樓，以配合[拉法赫航線開辦](../Page/拉法赫.md "wikilink")（現已停辦）。其餘在90年代開辦的航點有[多伦多](../Page/多伦多.md "wikilink")、[可倫坡](../Page/可倫坡.md "wikilink")、[雅加达](../Page/雅加达.md "wikilink")、[柏林](../Page/柏林.md "wikilink")、[孟買](../Page/孟買.md "wikilink")、[米蘭](../Page/米蘭.md "wikilink")、[特拉维夫](../Page/特拉维夫.md "wikilink")，更成為[美國環球航空的代碼共享夥伴之一](../Page/環球航空.md "wikilink")。

1996年2月10日，全資附屬公司Royal
Wings成立，負責飛行約旦國內航線（安曼至[亞喀巴](../Page/亞喀巴.md "wikilink")），初期使用F-27飛機，現由A320負責飛行。除本土航線外，Royal
Wings也有提供往[埃及](../Page/埃及.md "wikilink")、[塞浦路斯及](../Page/塞浦路斯.md "wikilink")[以色列的包機服務](../Page/以色列.md "wikilink")。

### 2000年代

2000年，美國[聯邦航空局更新航空公司的維修及工程部門牌照](../Page/聯邦航空局.md "wikilink")，此外免稅店服務也私營化。2001年2月5日，由政府擁有，組合自各私營有限公司的新股份公司RJI成立，皇家約旦航空改名為阿麗亞—皇家約旦航空公司（Alia
- The Royal Jordanian Airlines Company），但旅客仍稱它為皇家約旦航空。

2007年4月，皇家約旦航空加盟[寰宇一家航空聯盟](../Page/寰宇一家.md "wikilink")，成為阿拉伯地區首間加盟三大航空聯盟的航空公司。不久後，航空公司宣布購買10架[波音787客機](../Page/波音787.md "wikilink")，預計於2010年左右投入服務\[4\]，這是首次以「皇家約旦航空」的名義購買波音客機。

同年5月25日，停辦了10年的蒙特利爾航線重辦。7月11日，皇家約旦航空慶祝安曼直航紐約航線開通30周年，成為阿拉伯航空公司中，服務美國最長時間的航線。7月16日，皇家約旦航空第6次贏得《航空策略獎》的技術獎。7月23日，皇家約旦航空開展貨運業務，首個航點是[大马士革](../Page/大马士革.md "wikilink")，以[波音737飛行](../Page/波音737.md "wikilink")。

皇家約旦航空是中東首家在機上提供互聯網、流動電話、電郵、短訊、留言信箱服務的航空公司\[5\]，更耗資1000萬[約旦第納爾把](../Page/約旦第納爾.md "wikilink")3架A310客機的客艙升級。

皇家約旦航空在2007年12月17日私營化，政府售出了71%的股份，市場資金總值2億6000萬約旦第納爾。

2008年1月22日，開辦[香港航線](../Page/香港.md "wikilink")，途經曼谷，冬季每周3班，夏季每周5班\[6\]。同年3月13日，A319客機投入服務，成為首間擁有A320系列中，A319、A320、A321均有的中東航空公司\[7\]。8月17日，以[ERJ195客機營運每周](../Page/ERJ-170系列.md "wikilink")2班直航[基辅航班](../Page/基辅.md "wikilink")。8月24日，皇家約旦航空在[阿勒娅王后国际机场南大樓的](../Page/阿勒娅王后国际机场.md "wikilink")2樓開設新候機酒廊，最高可處理340名旅客，成為中東地區第二大的機場候機酒廊\[8\]。2008年7月，皇家約旦航空的載客人數上升了18%\[9\]。

2009年2月3日，為慶祝寰宇一家成立10周年，在一架A319客機上塗上寰宇一家的色彩\[10\]，更是皇家約旦航空首架披上特別色彩的飛機。

## 航點與代碼共享

**代碼共享**
皇家約旦航空與以下航空公司簽訂代碼共享協議：

  - [義大利航空](../Page/義大利航空.md "wikilink")
  - [美國航空](../Page/美國航空.md "wikilink")
  - [英國航空](../Page/英國航空.md "wikilink")
  - [海灣航空](../Page/海灣航空.md "wikilink")
  - [西班牙國家航空](../Page/西班牙國家航空.md "wikilink")
  - [馬來西亞航空](../Page/馬來西亞航空.md "wikilink")
  - [中東航空](../Page/中東航空.md "wikilink")
  - [阿曼航空](../Page/阿曼航空.md "wikilink")
  - [卡達航空](../Page/卡達航空.md "wikilink")
  - [西伯利亞航空](../Page/西伯利亞航空.md "wikilink")
  - [土耳其航空](../Page/土耳其航空.md "wikilink")

## 機隊

[royal.jordanian.a321-200.jy-ayg.arp.jpg](https://zh.wikipedia.org/wiki/File:royal.jordanian.a321-200.jy-ayg.arp.jpg "fig:royal.jordanian.a321-200.jy-ayg.arp.jpg")
[Royal_Jordanian_A330-200_JY-AIG_LHR_2014-03-29.png](https://zh.wikipedia.org/wiki/File:Royal_Jordanian_A330-200_JY-AIG_LHR_2014-03-29.png "fig:Royal_Jordanian_A330-200_JY-AIG_LHR_2014-03-29.png")
[JY-BAA_Boeing_787-8_Royal_Jordanian_Airlines_heading_to_std_323_on_its_first_visit.jpg](https://zh.wikipedia.org/wiki/File:JY-BAA_Boeing_787-8_Royal_Jordanian_Airlines_heading_to_std_323_on_its_first_visit.jpg "fig:JY-BAA_Boeing_787-8_Royal_Jordanian_Airlines_heading_to_std_323_on_its_first_visit.jpg")
截至2017年11月，皇家約旦航空的機隊如下\[11\]：

<center>

<table>
<caption><strong>皇家約旦航空機隊</strong></caption>
<thead>
<tr class="header">
<th><p>機型</p></th>
<th><p>數量</p></th>
<th><p>訂購中</p></th>
<th><p>載客量</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><abbr title="商務">C</abbr></p></td>
<td><p><abbr title="經濟">Y</abbr></p></td>
<td><p>總數</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A320.md" title="wikilink">空中巴士A319-132</a></p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>14</p></td>
<td><p>96</p></td>
</tr>
<tr class="odd">
<td><p>空中巴士A320-232</p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>16</p></td>
<td><p>120</p></td>
</tr>
<tr class="even">
<td><p>空中巴士A321-231</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>20</p></td>
<td><p>147</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音787.md" title="wikilink">波音787-8</a></p></td>
<td><p>7</p></td>
<td><p>15</p></td>
<td><p>24</p></td>
<td><p>247</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ERJ-170系列.md" title="wikilink">Embraer 175</a></p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>12</p></td>
<td><p>60</p></td>
</tr>
<tr class="odd">
<td><p>Embraer 195 AR</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>12</p></td>
<td><p>88</p></td>
</tr>
<tr class="even">
<td><p>皇家約旦航空貨運</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>空中巴士A330</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>空中巴士A340-642 Prestige</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>貴賓專用</p></td>
<td><p>約旦國王專機</p></td>
</tr>
<tr class="odd">
<td><p>總數</p></td>
<td><p>25</p></td>
<td><p>1</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</center>

截至2017年11月，約旦皇家航空機隊的平均機齡是7.6年\[12\]。

## 參考資料

<references/>

## 外部链接

  - [皇家约旦航空](http://www.rj.com)

  -
  -
  -
  -
  - [约旦皇家航空机队机龄](http://www.airfleets.net/ageflotte/Royal%20Jordanian%20Airlines.htm)

  - [约旦皇家航空机队资料](http://www.planespotters.net/Airline/Royal_Jordanian?show=all)

  - [约旦皇家航空乘客意见](http://www.airlinequality.com/Forum/rja.htm)

[Category:約旦航空公司](../Category/約旦航空公司.md "wikilink")
[Category:中东航空公司](../Category/中东航空公司.md "wikilink")
[Category:亚洲万里通](../Category/亚洲万里通.md "wikilink")
[Category:1963年成立的航空公司](../Category/1963年成立的航空公司.md "wikilink")
[Category:寰宇一家](../Category/寰宇一家.md "wikilink")

1.
2.
3.  ["Company
    Overview"](http://www.rj.com/AboutUs/OurCompany/CompanyOverview/tabid/158/locale/en-US/default.aspx).
    Royal Jordanian. Retrieved on 30 May 2009.
4.  Golden, Lara Lynn (20 May 2007). Press release ["Royal Jordanian
    negotiating for 12 787s through direct purchase and lease
    contracts"](http://www.ameinfo.com/120713.html). AME Info FZ LLC /
    Emap Limited.
5.  [1](http://www.onair.aero/)
6.  ["RJ starts operating flights between Amman and Hong Kong
    today"](http://www.ameinfo.com/144431.html). AME Info FZ LLC / Emap
    Limited.
7.
8.
9.  Ammari, Siba Sami (27 August 2008). Press release ["RJ reports 18%
    increase in passenger numbers last
    month"](http://www.ameinfo.com/167140.html). AME Info FZ LLC / Emap
    Limited.
10. [2](http://www.oneworld.com/ow/news/details?objectID=16589)
11. [Royal Jordanian Fleet
    Information](http://www.rj.com/en/tabid/114/locale/en-US/default.aspx)
12. [Royal Jordanian Fleet
    Age](http://www.airfleets.net/ageflotte/Royal%20Jordanian%20Airlines.htm)