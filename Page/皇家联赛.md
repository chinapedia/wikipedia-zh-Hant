**皇家聯賽盃**（英文：**Royal
League**）或稱**北歐三國聯賽**是一項每年一度由[斯堪的納維亞的國家所屬球會所參加的足球賽事](../Page/斯堪的納維亞.md "wikilink")，參加資格必須是[丹麥](../Page/丹麥.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[挪威的聯賽前列首四名合共十二隊參加](../Page/挪威.md "wikilink")。賽事首先將十二支球隊分為三組，四隊以雙循環賽制作賽，小組首兩名及最佳成績的第三名可以晉身半準決賽階段。淘汰階段採取主客兩回合進行，直至決定出決賽階段的球隊。冠軍獎金高達二百六十萬元。

## 歷屆冠軍

  - 2004-05年度 - 哥本哈根（丹麦）
  - 2005-06年度 - 哥本哈根（丹麦）
  - 2006-07年度 - 邦比（丹麦）
  - 2007-08年度 - 因財政問題停辦
  - 2008-09年度 - 因電視播影權問題停辦

## 歷屆決賽

<table>
<thead>
<tr class="header">
<th><p>球季</p></th>
<th><p>冠軍</p></th>
<th><p>比數</p></th>
<th><p>亞軍</p></th>
<th><p>場地</p></th>
<th><p>入場人數</p></th>
<th><p>日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2004–05</p></td>
<td><p><a href="../Page/哥本哈根足球會.md" title="wikilink">哥本哈根</a>'''</p></td>
<td><p><strong>1–1</strong></p></td>
<td><p><a href="../Page/哥登堡.md" title="wikilink">哥登堡</a></p></td>
<td><p>舊烏利維球場, <a href="../Page/哥德堡.md" title="wikilink">哥德堡</a></p></td>
<td><p>10,216</p></td>
<td><p>2005年5月26日</p></td>
</tr>
<tr class="even">
<td><p>2005–06</p></td>
<td><p><a href="../Page/哥本哈根足球會.md" title="wikilink">哥本哈根</a></p></td>
<td><p><strong>1–0</strong></p></td>
<td><p><a href="../Page/利尼史特朗.md" title="wikilink">利尼史特朗</a></p></td>
<td><p><a href="../Page/帕肯球場.md" title="wikilink">帕肯球場</a>, <a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td><p>13,617</p></td>
<td><p>2006年4月6日</p></td>
</tr>
<tr class="odd">
<td><p>2006–07</p></td>
<td><p><a href="../Page/邦比.md" title="wikilink">邦比</a></p></td>
<td><p><strong>1–0</strong></p></td>
<td><p><a href="../Page/哥本哈根足球會.md" title="wikilink">哥本哈根</a></p></td>
<td><p>邦比球場, 邦比市</p></td>
<td><p>17,914</p></td>
<td><p>2007年3月15日</p></td>
</tr>
</tbody>
</table>

[Category:國際俱樂部足球賽事](../Category/國際俱樂部足球賽事.md "wikilink")