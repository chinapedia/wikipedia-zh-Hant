**皇家音乐学院（）**位于[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")[多伦多市](../Page/多伦多市.md "wikilink")，是加拿大最大且历史最悠久的音乐学校。[爱德華·费歇尔](../Page/爱德華·费歇尔.md "wikilink")（Edward
Fisher）于1886年创建了这所学校，并命名为多伦多音乐学院。1947年，英國国王[乔治六世授予音乐学院](../Page/乔治六世.md "wikilink")[皇家特许状](../Page/皇家特许状.md "wikilink")。

## 介紹

### 背景

音乐学院在1991年之前从属于[多伦多大学](../Page/多伦多大学.md "wikilink")，自此之后便成为一所完全独立的机构。她的校址（原称：McMaster
Hall，现被更名为 Ihnatowycz
Hall）曾是现位于安大略省[汉密尔顿市的](../Page/汉密尔顿市.md "wikilink")[麦克马斯特大学原址](../Page/麦克马斯特大学.md "wikilink")。音乐学院目前正在进行一项重点扩展计划
- [TELUS](../Page/TELUS.md "wikilink")
演出学习中心包括一个1,140座位的新音乐厅，预期将于2007年开放使用。扩展计划使得音乐学院临时迁至「Dufferin」和「Bloor
West」地区的「90 Croatia Street」。

皇家音乐学院创造出[加拿大最受欢迎的综合性教学方法](../Page/加拿大.md "wikilink")，包括十个级别水平的指导方针。ARCT
教师或表演者文凭证书是所有级别中的最高级别，并在世界范围内被认可。综合性理论及历史并修课程是获得文凭证书的必需。许多加拿大省份认可高级别的课程，并奖励高中学分给成功结业的学生。

### 管理五个主要下属部门

  - 皇家音乐学院社区学院（）：作为皇家音乐学院最早的部分，是北美最大的立足于社区的音乐学校之一。课程为各年龄层和水平学生所设计，其中突出的早期儿童教育课程及终身学习的观念被社会所认可。社区学院所提供的高质量的实践性及理论性教学表现了音乐传统的多样性，其中包括[早期音乐](../Page/早期音乐.md "wikilink")、[古典音乐](../Page/古典音乐.md "wikilink")、[流行音乐](../Page/流行音乐.md "wikilink")、[民族音乐](../Page/民族音乐.md "wikilink")、[爵士乐以及](../Page/爵士乐.md "wikilink")[世界音乐](../Page/世界音乐.md "wikilink")。
  - Glenn Gould
    学院（）：是世界知名的学士和研究生级别音乐表演及教学的专业训练中心。学校专为有天赋的年轻音乐家提供在职业生涯各方面所预备的课程以及个人的训练环境。
  - 年轻艺术家表演学院（）：提供一个综合课程结构以提高那些有天赋的年轻音乐家的演奏技巧、音乐修养和理论知识，其中包括专为9至18岁有天赋的音乐家所提供的高级别[钢琴](../Page/钢琴.md "wikilink")、[弦乐](../Page/弦乐.md "wikilink")、[管乐](../Page/管乐.md "wikilink")、[铜管乐和](../Page/铜管乐.md "wikilink")[声乐的表演训练](../Page/声乐.md "wikilink")。
  - 融合艺术性教学（）：是一项广受赞誉的教学倡导，它帮助公立学校的老师运用艺术教授基础课程的方法激发学生。融合艺术性教学创立于1994年，是一种提高公共教育目标、形式和文化的方法。它是现今世界上最具创新的教育项目之一，并具有全国性及世界性的发展潜力。
  - 皇家音乐学院考级（）：在300多个加拿大社区设立了考点并为国际广泛应用，它所制定和拥护的音乐考级标准也使它成为世界上同类考试中受到很高关注的一个。
  - Frederick Harris
    音乐有限公司（）：成立于1904年，是加拿大最大且历史最久的[音乐印刷出版社](../Page/音乐印刷出版社.md "wikilink")。作为皇家音乐学院所有授权出版物的独家代理出版社，其出版物行销于25个国家。这些受国际性赞誉的出版物提供给老师及学生们最高质量且独一无二的创新教学资料。

## 知名教师及校友

### 知名教师

  - [希利·威兰](../Page/希利·威兰.md "wikilink")（Healey Willan）
  - [欧内斯特·麦克米兰](../Page/欧内斯特·麦克米兰.md "wikilink")（Ernest MacMillan）爵士
  - [阿尔伯特·葛瑞罗](../Page/阿尔伯特·葛瑞罗.md "wikilink")（Alberto Guerrero）
  - [伯伊德·尼尔](../Page/伯伊德·尼尔.md "wikilink")（Boyd
    Neel）（曾于1953年-1971年担任音乐学院院长）
  - [劳润德·芬尼维斯](../Page/劳润德·芬尼维斯.md "wikilink")（Lorand Fenyves）
  - [鲍里斯·伯林](../Page/鲍里斯·伯林.md "wikilink")（Boris Berlin）
  - [尼古拉斯·高德斯密特](../Page/尼古拉斯·高德斯密特.md "wikilink")（Nicholas
    Goldschmidt）。

### 知名校友

许多曾参加皇家音乐学院课程学习或皇家音乐学院考级的人，都已在音乐界或其他领域取得了成就。其中两名学生曾在电视节目《天堂瀑布》*（Paradise
Falls）*中共同担任主角。往届学生包括：

  - [布鲁斯·库克本](../Page/布鲁斯·库克本.md "wikilink")（Bruce Cockburn）-
    民乐和[摇滚乐](../Page/摇滚乐.md "wikilink")[吉他演奏家](../Page/吉他.md "wikilink")、歌唱家兼作曲家，曾被列入[加拿大音乐名人堂](../Page/加拿大音乐名人堂.md "wikilink")（Canadian
    Music Hall of Fame）
  - [艾琳·克雷蒂安](../Page/艾琳·克雷蒂安.md "wikilink")（Aline Chrétien）-
    前[加拿大总理](../Page/加拿大总理.md "wikilink")[讓·克雷蒂安](../Page/讓·克雷蒂安.md "wikilink")（Jean
    Chrétien）的夫人
  - [阿德里安娜·克拉克森](../Page/阿德里安娜·克拉克森.md "wikilink")（Adrienne Clarkson）-
    [加拿大总督](../Page/加拿大总督.md "wikilink")（1999年-2005年）
  - [奈達·蔻儿](../Page/奈達·蔻儿.md "wikilink")（Naida Cole）-
    钢琴家，曾与[蒙特利尔交响乐团](../Page/蒙特利尔交响乐团.md "wikilink")、国家艺术中心管弦乐团以及其它乐团共同演出
  - [大衛·福斯特](../Page/大衛·福斯特.md "wikilink")（David Foster）-
    [格莱美获奖音乐家](../Page/格莱美.md "wikilink")、制作人及作曲家
  - [格倫·古尔德](../Page/格倫·古尔德.md "wikilink")（Glenn Gould）- 钢琴家，出色地投身于现场表演
  - [罗伯特·高勒特](../Page/罗伯特·高勒特.md "wikilink")（Robert Goulet）-
    主要以歌唱家闻名，此外也是一名演员
  - [劳伦斯·高文](../Page/劳伦斯·高文.md "wikilink")（Lawrence Gowan）-
    [Styx](../Page/Styx.md "wikilink") 乐团键盘手
  - [诺曼·杰维森](../Page/诺曼·杰维森.md "wikilink")（Norman Jewison）-
    电影导演，以《炎热的夜晚》*（Heat of the Night）*及其它作品出名
  - [伊利·卡斯纳](../Page/伊利·卡斯纳.md "wikilink")（Eli Kassner）-
    [多伦多吉他学会](../Page/多伦多吉他学会.md "wikilink")（Guitar
    Society of Toronto）创始人、世界知名吉他教师
  - [洛伊絲·马歇尔](../Page/洛伊絲·马歇尔.md "wikilink")（Lois Marshall）-
    女高音歌唱家，[加拿大勋章获得者](../Page/加拿大勋章.md "wikilink")
  - [金·史拉纳](../Page/金·史拉纳.md "wikilink")（Kim Schraner）-
    [加拿大广播公司](../Page/加拿大广播公司.md "wikilink")（CBC）电视节目《间谍网络》*（Spynet）*女主角，曾出演《天堂瀑布》*（Paradise
    Falls）*
  - [保羅·沙佛](../Page/保羅·沙佛.md "wikilink")（Paul Shaffer）-《大卫深夜秀》*（Late
    Show with David Letterman）*乐队指挥
  - [米切爾·夏普](../Page/米切爾·夏普.md "wikilink")（Mitchell Sharp）-
    前[加拿大财政部长](../Page/加拿大财政部长.md "wikilink")
  - [特蕾薩·斯特拉塔斯](../Page/特蕾薩·斯特拉塔斯.md "wikilink")（Teresa Stratas）-
    歌剧女高音歌唱家，曾参与演出《歌剧阿依达》*（Metropolitan Opera）*
  - [切瑞利·泰勒](../Page/切瑞利·泰勒.md "wikilink")（Cherilee Taylor）-
    连续剧《天堂瀑布》*（Paradise Falls）*女演员
  - [琼·维克斯](../Page/琼·维克斯.md "wikilink")（Jon Vickers）-
    男高音歌唱家，曾出演《[歌剧](../Page/歌剧.md "wikilink")[阿依达](../Page/阿依达.md "wikilink")》*（Metropolitan
    Opera）*
  - [格雷格·威尔斯](../Page/格雷格·威尔斯.md "wikilink")（Greg Wells）-
    录音制作人、作曲家以及多种乐器演奏家，曾与 Rufus
    Wainwright、P\!nk、Natasha Bedingfield 以及其他音乐人合作
  - [拉斐爾·维拉纽瓦](../Page/拉斐爾·维拉纽瓦.md "wikilink")（Rafael Villanueva）-
    [多米尼加国家交响乐团副总监](../Page/多米尼加.md "wikilink")
  - [龍笛 (音樂家)](../Page/龍笛_\(音樂家\).md "wikilink") (Ron Korb) -
    加拿大籍[長笛演奏家及](../Page/長笛.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")
  - [史蒂芬·哈珀](../Page/史蒂芬·哈珀.md "wikilink")（Stephen Harper）-
    [加拿大总理](../Page/加拿大总理.md "wikilink")
  - [李樂詩](../Page/李樂詩_\(歌手\).md "wikilink")（Joyce Lee）-
    [香港](../Page/香港.md "wikilink")[女歌手](../Page/女歌手.md "wikilink")
  - [Henry](../Page/劉憲華.md "wikilink")（Henry Lau）-
    [加拿大生长](../Page/加拿大.md "wikilink")，父[香港母](../Page/香港.md "wikilink")[台湾人](../Page/台湾.md "wikilink")，男歌手
    韩国团体[SUPER JUNIOR的子团](../Page/SUPER_JUNIOR.md "wikilink")[Super
    Junior-M的团员](../Page/Super_Junior-M.md "wikilink")

## 外部链接

  - [皇家音乐学院](http://www.rcmusic.ca/)
  - [皇家音乐学院社区学院](https://web.archive.org/web/20070518232005/http://www.rcmusic.ca/ContentPage.aspx?name=communitySchool)
  - [Glenn Gould
    学院](https://web.archive.org/web/20060829074921/http://www.rcmusic.ca/ContentPage.aspx?name=glennGould)
  - [年轻艺术家表演学院](https://web.archive.org/web/20070518231240/http://www.rcmusic.ca/ContentPage.aspx?name=YAPA)
  - [融合艺术性教学](https://web.archive.org/web/20070608193504/http://www.ltta.ca/)
  - [皇家音乐学院考级](http://www.rcmexaminations.org/)
  - [Frederick Harris 音乐有限公司](http://www.frederickharrismusic.com/)

[多伦多皇家音乐学院](../Category/多伦多皇家音乐学院.md "wikilink")
[Category:英国音乐学校](../Category/英国音乐学校.md "wikilink")
[Category:多倫多教育](../Category/多倫多教育.md "wikilink")
[Category:1886年創建的教育機構](../Category/1886年創建的教育機構.md "wikilink")
[Category:1886年加拿大建立](../Category/1886年加拿大建立.md "wikilink")