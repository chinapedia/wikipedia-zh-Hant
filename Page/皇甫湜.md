**皇甫湜**（777年—835年），字**持正**，[睦州](../Page/睦州.md "wikilink")[新安](../Page/新安.md "wikilink")（今[浙江](../Page/浙江.md "wikilink")[淳安](../Page/淳安.md "wikilink")）人。唐代文學家。是引發[牛李黨爭的人物之一](../Page/牛李黨爭.md "wikilink")。

[唐宪宗宰相](../Page/唐宪宗.md "wikilink")[王涯的外甥](../Page/王涯.md "wikilink")，元和三年（808年）与[牛僧孺](../Page/牛僧孺.md "wikilink")、[李宗闵并登贤良方正科第三等](../Page/李宗闵.md "wikilink")，三人猛烈抨击时政，引起宰相[李吉甫不满](../Page/李吉甫.md "wikilink")，舅王涯、[裴垍被免去翰林学士之职](../Page/裴垍.md "wikilink")，考官杨于陵、韦贯之也被外贬。皇甫湜补授[陆浑尉](../Page/陆浑.md "wikilink")，久久不得晋升。后官至工部郎中。[裴度辟為判官](../Page/裴度.md "wikilink")。皇甫湜出韓愈門下，從[韓愈學古文](../Page/韓愈.md "wikilink")，思想傾向與韓愈相近，為文亦言聖道，但其闢佛不若韓愈積極。文章奇僻，今存詩三首，《[全唐詩](../Page/全唐詩.md "wikilink")》僅收其《[題浯溪石](../Page/題浯溪石.md "wikilink")》一首。《[答李生書](../Page/答李生書.md "wikilink")》三篇，是皇甫湜文論的代表作，反覆論辯“奇”與“常”在文章中的關係。他說“夫意新則異於常，異於常則怪矣；詞高則出於眾，出於眾則奇矣。”又說:“夫文者非他，言之華者也，其用在通理而已，固不務奇，然亦無傷於奇也。”宋人輯有《皇甫持正文集》。

皇甫湜跟他的老師韩昌黎一樣，常給人家寫[墓誌銘一類的文章](../Page/墓誌銘.md "wikilink")，稿費不菲，價碼是“每字三匹絹，更減八分錢不得”。韓文公似特重皇甫湜，囑咐他寫墓誌銘，[李翱只令作](../Page/李翱.md "wikilink")[行狀](../Page/行狀.md "wikilink")。曾作《昌黎韓先生墓誌銘》、《韓文公神道碑》是研究韓愈的重要史料。

## 參考書目

  - 《[全唐詩](../Page/全唐詩.md "wikilink")》

[Category:777年出生](../Category/777年出生.md "wikilink")
[H](../Category/835年逝世.md "wikilink")
[H](../Category/唐朝作家.md "wikilink")
[T](../Category/皇甫姓.md "wikilink")
[Category:淳安人](../Category/淳安人.md "wikilink")