**皮埃尔·赖伐尔**（[法语](../Page/法语.md "wikilink")：，），[法国政治家](../Page/法国.md "wikilink")，曾任[总理](../Page/法国总理.md "wikilink")。1883年6月28日生于沙泰勒东。1935年6月－1936年1月两次组阁。[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，支持[菲利普·贝当上台](../Page/菲利普·贝当.md "wikilink")。法国沦亡后，任[维希政府副总理](../Page/维希政府.md "wikilink")（1940年7月12日－12月13日），在[希特勒支持下](../Page/希特勒.md "wikilink")，1942年4月出任总理，此后一直左右贝当政府。法国光复后，在[奥地利被捕](../Page/奥地利.md "wikilink")。1945年10月9日被巴黎高等法院以叛国罪判处死刑，15日被处决。

[L](../Category/法國總理.md "wikilink")
[L](../Category/法國第二次世界大戰人物.md "wikilink")
[L](../Category/被處決的法國人.md "wikilink")
[L](../Category/時代年度風雲人物.md "wikilink")
[L](../Category/第二次世界大戰領袖.md "wikilink")
[Category:與納粹德國合作的法國人](../Category/與納粹德國合作的法國人.md "wikilink")
[Category:法國反共主義者](../Category/法國反共主義者.md "wikilink")