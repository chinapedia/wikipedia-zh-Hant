**皮埃蒙特** （, ）是[意大利西北的一个大区](../Page/意大利.md "wikilink")。大区面积有25,399
平方公里及大约四百三十万人口。大区首府是[都灵](../Page/都灵.md "wikilink")。

皮埃蒙特三面被[阿尔卑斯山山脉包围](../Page/阿尔卑斯山.md "wikilink")，包括[波河的起源维素山脉](../Page/波河.md "wikilink")，及罗莎山脉。大区与[法国](../Page/法国.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、及意大利[伦巴第](../Page/伦巴第.md "wikilink")、[利古里亚](../Page/利古里亚.md "wikilink")、[艾米利亚-罗马涅及](../Page/艾米利亚-罗马涅.md "wikilink")[瓦莱达奥斯塔大区相邻](../Page/瓦莱达奥斯塔.md "wikilink")。

皮埃蒙特的低地是一块农产丰足的地区，盛产[小麦](../Page/小麦.md "wikilink")、[稻米](../Page/稻米.md "wikilink")、[玉蜀黍及](../Page/玉蜀黍.md "wikilink")[葡萄](../Page/葡萄.md "wikilink")。该区也是主要的工业中心，特别是[都灵](../Page/都灵.md "wikilink")，是[菲亚特车厂的总部所在地](../Page/菲亚特.md "wikilink")。

2006年2月，都灵成功主办[2006年冬季奥林匹克运动会](../Page/2006年冬季奥林匹克运动会.md "wikilink")。
皮爾蒙特王國以前的首都位於義大利旁的海上的一個小島嶼

## 历史

1046年，[萨伏依伯爵将皮埃蒙特加入成为](../Page/萨伏依.md "wikilink")[萨伏依的主要领地](../Page/萨伏依.md "wikilink")，首都位于[尚贝里](../Page/尚贝里.md "wikilink")（现[法国](../Page/法国.md "wikilink")[萨瓦省省会](../Page/萨瓦省.md "wikilink")）。萨伏依王室于1416年被擢升为公国，1563年公爵[伊曼纽尔·菲利贝托移至皮埃蒙特居住](../Page/伊曼纽尔·菲利贝托.md "wikilink")。1720年，萨伏依公爵成为萨丁尼亚国王，并日后发展为[萨丁尼亚-皮埃蒙特王国及提升都灵的重要性而成为欧洲其中一国的](../Page/萨丁尼亚王国.md "wikilink")[首都](../Page/首都.md "wikilink")。

[阿尔巴共和国是](../Page/阿尔巴共和国.md "wikilink")[法国大革命时期法国在皮埃蒙特的](../Page/法国大革命.md "wikilink")[卫星国](../Page/卫星国.md "wikilink")，于1796年建立，1801年被法国合并。在1802年6月一个新的卫星国，[苏巴平宁共和国](../Page/苏巴平宁共和国.md "wikilink")（Subalpine
Republic），於皮埃蒙特成立并在9月又被法国吞并。在[维也纳会议中](../Page/维也纳会议.md "wikilink")，[萨丁尼亚-皮埃蒙特王国重新復原](../Page/萨丁尼亚王国.md "wikilink")，更获得了[热那亚共和国以增强实力](../Page/热那亚共和国.md "wikilink")，作为[法国的一个障碍](../Page/法国.md "wikilink")。

早前於1820年-1821年及1848年-1849年所发起对抗[奥地利帝国的战争失败之后](../Page/奥地利帝国.md "wikilink")，皮埃蒙特成为了1859年-1861年间[意大利统一运动的发起地区](../Page/意大利统一.md "wikilink")。统一之后，萨伏依王室成为了[意大利国王王室](../Page/意大利国王.md "wikilink")，而都灵也短暂地成为意大利的首都。但是，王国的领土增加却降低了皮埃蒙特在王国中的地位，国都先迁至[佛罗伦萨](../Page/佛罗伦萨.md "wikilink")，后来再迁至[罗马](../Page/罗马.md "wikilink")。其中一个可证明皮埃蒙特在意大利历史上占有代表地位的，就是意大利的[王储冠以](../Page/王储.md "wikilink")**皮埃蒙特亲王**衔。

## 知名人物

  - [翁贝托·埃可](../Page/翁贝托·埃可.md "wikilink")，国际知名作家。
  - [约瑟夫·拉格朗日](../Page/约瑟夫·拉格朗日.md "wikilink")，数学家和天文学家。

[PiazzaCastello.jpg](https://zh.wikipedia.org/wiki/File:PiazzaCastello.jpg "fig:PiazzaCastello.jpg")

## 其它

**"皮埃蒙特"** 亦是一个小丘陵的名称，而**"皮埃蒙特"**
这字在[地理上亦通称为一些丘陵地带](../Page/地理.md "wikilink")。

皮埃蒙特有百分之七点六的地区是[保护区](../Page/保护区.md "wikilink")。

皮埃蒙特是意大利其中一个重要的酿酒区，整区的葡萄园面积超过700平方公里（170,000 英亩）。

[Torino-panoramadaicappuccini.jpg](https://zh.wikipedia.org/wiki/File:Torino-panoramadaicappuccini.jpg "fig:Torino-panoramadaicappuccini.jpg")
[GrattacieloPiemonte.jpg](https://zh.wikipedia.org/wiki/File:GrattacieloPiemonte.jpg "fig:GrattacieloPiemonte.jpg")，為皮埃蒙特大區政府建築\]\]
除省会都灵之外，区内其它主要城市包括[韦尔切利](../Page/韦尔切利.md "wikilink")、[韦尔巴尼亚](../Page/韦尔巴尼亚.md "wikilink")、[蒙卡列里](../Page/蒙卡列里.md "wikilink")、[里沃利及](../Page/里沃利.md "wikilink")[伊夫雷亚](../Page/伊夫雷亚.md "wikilink")。

## 参考文献

## 外部链接

### 政府机构

  - [皮埃蒙特官方网站](http://www.regione.piemonte.it/)
  - [皮埃蒙特环境保护局](http://www.arpa.piemonte.it/)
  - [皮埃蒙特学校区](http://www.piemonte.istruzione.it/)

### 特色网站

  - [每日新闻](https://web.archive.org/web/20060602111751/http://www.ilgiornaledelpiemonte.com/)
  - [皮埃蒙特周刊](http://www.piemonte-online.com/)
  - [皮埃蒙特文化协会](http://www.nostereis.org/)
  - [制造业者公会](https://web.archive.org/web/20060627182830/http://www.federpiemonte.org/)
  - [皮埃蒙特消闲网](http://cucina.piemonte.net/)
  - [皮埃蒙特天气预报](https://web.archive.org/web/20060614035442/http://www.tempoitalia.it/meteoregione/previsione.php?reg=piemonte)

### 旅游网站

  - [皮埃蒙特地图](https://web.archive.org/web/20060623033911/http://www.italy-weather-and-maps.com/maps/italy/piedmont.gif)
  - [环球地理](http://www.globalgeografia.com/italia/piemonte.htm)

[\*](../Category/皮埃蒙特大區.md "wikilink")