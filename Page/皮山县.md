**皮山县**（[维吾尔文](../Page/维吾尔文.md "wikilink")：****，[拉丁维文](../Page/拉丁维文.md "wikilink")：****）是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[和田地区所辖的一个](../Page/和田地区.md "wikilink")[县](../Page/县.md "wikilink")。总面积为39412平方公里，2002年人口为22万。

[新疆生产建设兵团和田农场管理局皮山县农场](../Page/新疆生产建设兵团.md "wikilink")，和田地区水泥厂地区杜瓦煤矿等10个企事业单位在境内。

皮山县是一个以[维吾尔族为主体的多民族聚居的县](../Page/维吾尔族.md "wikilink")。有12个民族，2001年总人口198275人。其中维吾尔族占97.96%，[汉族占](../Page/汉族.md "wikilink")0.01%，此外还有[塔吉克族](../Page/塔吉克族.md "wikilink")，[柯尔克孜族等其它少数民族人口占](../Page/柯尔克孜族.md "wikilink")2.03%。2002年全县旅居国外的侨民分居13个国家和地区，其中侨居[阿拉伯的侨民最多](../Page/阿拉伯.md "wikilink")。

## 行政区划

下辖6个[镇](../Page/行政建制镇.md "wikilink")、8个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、2个[民族乡](../Page/民族乡.md "wikilink")：

。

## 参考资料

## 外部链接

  - [皮山县政府网站](http://www.ps.gov.cn/)

[皮山县](../Category/皮山县.md "wikilink") [县](../Category/和田县市.md "wikilink")
[和田](../Category/新疆县份.md "wikilink")
[新](../Category/国家级贫困县.md "wikilink")