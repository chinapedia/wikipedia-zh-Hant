**皮特·科内利斯·蒙德里安**（Piet Cornelies
Mondrian，），[荷蘭畫家](../Page/荷蘭.md "wikilink")，[風格派運動幕後藝術家和非具象繪畫的創始者之一](../Page/風格派.md "wikilink")，對後代的[建築](../Page/建築.md "wikilink")、[設計等影響很大](../Page/設計.md "wikilink")。自称“新造型主义”，又称“几何形体派”。

## 生平

### 啟蒙青少年時期（1880年-1892年）

蒙德里安生于荷兰中部的[阿默斯福特](../Page/阿默斯福特.md "wikilink")，父親是一位清教徒和热衷美術的小學校長，環境條件使蒙德里安從小就能接觸美術，而宗教對蒙德里安來說更是他的啟發、轉變風格的關鍵。

八歲時蒙德里安立志要當畫家，但是家人認為藝術家是一項不穩定的工作，蒙德里安與父母多次商量之後，他承諾要取得[美術教師資格養家餬口](../Page/美術.md "wikilink")，這才讓蒙德里安的父母答應讓他學習繪畫。但是蒙德里安擁有教師資格之後，卻未曾在[教育界服務過](../Page/教育界.md "wikilink")。

在17歲取得小學教師資格之前，蒙德里安是在他的叔父福爾茲·蒙德里安的指導下學習繪畫。福爾茲是一位海牙畫派的畫家，因此蒙德里安得到寫實浪漫的真傳。

### [阿姆斯特丹時期](../Page/阿姆斯特丹.md "wikilink")（1907年-1912年）

1892年蒙德里安進入阿姆斯特丹的國立藝術學院，正式接受學院派的訓練，也奠定了他深厚的寫實能力。

風景畫是蒙德里安初期的繪畫主題，此時的作品仍舊瀰漫著十七世紀荷蘭繪畫的風格與精神，受到了印象主義、象徵主義和表現主義的影響漸漸脫離了海牙畫派的表面形式。

1903年蒙德里安以「靜物」（Still
Life）獲得藝術家會的肯定，堅定他將繪畫當作終生職業意念。在他的作品中可以看見嚴謹的構圖和豪放生動的筆觸，兼具現代與古典的優點，在同輩畫家中已漸漸豎立自己的風格。

1909年蒙德里安經歷了自己的宗教革命，他加入了「荷蘭通神論者協會」，接觸了新柏拉圖主義和多神論思想，使得蒙德里安發現自己，思考人類存在的價值。這項轉變也改變了蒙德里安創作的方向，開啟新造型主義的思考方向。

### 巴黎时期（1911年-1914年）立體主義的震撼

1911年蒙德里安見識了[畢卡索和布拉克等立體派的作品](../Page/畢卡索.md "wikilink")，感受極大的震撼。[立體派講究的立體事實和明確客觀都是蒙德里安追求的目標](../Page/立體派.md "wikilink")。隨後前往巴黎研究立體派的繪畫風格。他不斷分析眼睛所見的影像，並且加入了音樂性作品充滿了節奏感。蒙德里安成功的從立體派中吸取精華，作品以抽象的方式呈現，並加入了自我的風格，脫離了立體派。

### 風格派（1914年-1919年）

1914年回到荷蘭後[第一次世界大戰爆發](../Page/第一次世界大戰.md "wikilink")，蒙德里安留在荷蘭致力於「繪畫中的新造型」，集結許多有志一同的朋友激盪出新造型主義。

1917年蒙德里安的朋友出版「風格」雜誌讓蒙德里安等畫家發表創作理念。1918年簽署了反戰、反個人主義，宣揚和平團結的「風格派宣言」。蒙德里安想利用藝術將生命昇華，他利用抽象的造型與中性的色彩來傳達秩序與和平的理念。

### 關鍵的轉變（1919年-1938年）

在這一時期蒙德里安發現了新的個人形式，他使用更基本的元素創作（直線、直角、三原色）組成抽象畫面，此時期的代表作「線與色彩的構成」色彩柔和、充滿輕快和諧的節奏感。這一時期的作品有《構圖第十號》。

### 倫敦時期（1938年-1940年）

[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，蒙德里安心情大受干擾，他的畫失去了快樂的色彩節奏，由黑色線條貫穿畫面，給人極度的憂鬱感。這是他第五度轉型。

### 纽约时期（1940年-1944年）

生命中最後四年，蒙德里安移居[美國](../Page/美國.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")，在這五光十色的大都會，蒙德里安感受到沒有戰事紛擾的世界，在紐約創作的作品比過去更為明亮、更為抽像，反映了紐約的現代經驗。他融合了過去不同時期作品風格加以延伸，色彩、線條呈現輕快的律動，畫面的音樂性在此達到最高境界。

这一时期的作品之一：《百老汇爵士乐》
[230px](../Page/file:Piet_Mondrian,_1942_-_Broadway_Boogie_Woogie.jpg.md "wikilink")

## 画风

主张以几何形体构成“形式的美”，作品多以垂直线和水平线，长方形和正方形的各种格子组成，反对用曲线，完全摒弃艺术的客观形象和生活内容。

## 作品

<File:Piet> Mondriaan, 1930 - Mondrian Composition II in Red, Blue, and
Yellow.jpg|紅黃藍的構成II <File:Piet> Mondriaan, 1939-1942 - Composition
10.jpg|構成第十號 <File:Gray> Tree 1911.jpg|灰樹 <File:Blossoming> apple tree,
by Piet Mondriaan.jpg|盛開的蘋果樹 <File:Piet> Mondriaan - Arbre.jpg|水平的樹

## 外部链接

  - [Mondrian Trust](http://www.mondriantrust.com), the official holder
    of reproduction rights to Mondrian's works.
  - [PietMondrian.net](http://www.pietmondrian.net), the official holder
    of reproduction rights to Mondrian's works.
  - [Piet Mondrian: The Transatlantic
    Paintings](https://web.archive.org/web/20060104034432/http://www.artmuseums.harvard.edu/mondrian/)
  - [Mondrian at
    Artchive](http://www.artchive.com/artchive/M/mondrian.html)
  - [*The Mondriaan site*](http://www.mondriaan.net/)
  - [Guggenheim NY Mondrian
    collection](https://web.archive.org/web/20060106080426/http://www.guggenheimcollection.org/site/artist_works_112_0.html)
  - [Piet
    Mondrian](http://www.moreeuw.com/histoire-art/biographie-piet-mondrian.htm)

[Category:荷兰画家](../Category/荷兰画家.md "wikilink")
[Category:荷兰裔美国人](../Category/荷兰裔美国人.md "wikilink")