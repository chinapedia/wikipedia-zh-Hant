**皮特县**（）是[美國](../Page/美國.md "wikilink")[北卡羅萊納州東部的一個縣](../Page/北卡羅萊納州.md "wikilink")。面積1,696平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口133,798人。縣治[格林維爾](../Page/格林維爾_\(北卡羅萊納州\).md "wikilink")（Greenville）。

成立於1790年4月24日。縣名紀念[英國首相](../Page/英國首相.md "wikilink")[老皮特](../Page/老皮特.md "wikilink")
（William Pitt, 1st Earl of Chatham）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[P](../Category/北卡羅萊納州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.