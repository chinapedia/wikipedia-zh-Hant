**皮埃尔-奥古斯特·雷诺瓦**（，）是一位著名的[法國](../Page/法國.md "wikilink")[畫家](../Page/畫家.md "wikilink")，也是[印象派發展史上的領導人物之一](../Page/印象派.md "wikilink")。其畫風承襲[彼得·保羅·魯本斯與](../Page/彼得·保羅·魯本斯.md "wikilink")[讓-安東尼·華托的傳統](../Page/讓-安東尼·華托.md "wikilink")，對於女性形體的描繪特別著名\[1\]。

## 生平

### 早年生活

[Pierre-Auguste_Renoir,_La_loge_(The_Theater_Box).jpg](https://zh.wikipedia.org/wiki/File:Pierre-Auguste_Renoir,_La_loge_\(The_Theater_Box\).jpg "fig:Pierre-Auguste_Renoir,_La_loge_(The_Theater_Box).jpg")画廊\]\]

皮埃尔-奥古斯特·雷诺瓦出生於[法國](../Page/法國.md "wikilink")[上維埃納省](../Page/上維埃納省.md "wikilink")[利摩日的一個](../Page/利摩日.md "wikilink")[工人階級的家庭之中](../Page/工人.md "wikilink")。童年時期的雷诺瓦，曾在一家[瓷器工廠工作](../Page/瓷器.md "wikilink")，而雷诺瓦的繪畫才能使他得到了繪製瓷器的差事\[2\]。而在他進入美術學校之前，還有受外國傳教士的委託，繪畫室內的裝飾物和裝飾扇子的經歷\[3\]。也包括畫簾子、爲[紋章着色等](../Page/紋章.md "wikilink")。這些餬口的工作他顯得極爲稱職，或許也因此使他更想脫離藝匠處境。於是，他走上了傳統的、也是當時所有想成爲畫家者所循的道路。1860年1月起，他獲得[羅浮宮博物館的臨摹證](../Page/羅浮宮博物館.md "wikilink")\[4\]，在那裡參觀並學習法國有名畫家的各種技法。

1862年，雷诺瓦前往[巴黎](../Page/巴黎.md "wikilink")，在[查爾斯·格萊爾門下開始學習美術](../Page/查爾斯·格萊爾.md "wikilink")。他在那裡認識了[艾佛烈·希斯里](../Page/艾佛烈·希斯里.md "wikilink")
、[弗雷德瑞克·巴吉爾和](../Page/弗雷德瑞克·巴吉爾.md "wikilink")[克勞德·莫內](../Page/克勞德·莫內.md "wikilink")\[5\]。在1860年代期間，雷诺瓦其實沒有財力去購置顏料。1864年，他的畫作在法國[沙龍第一次展出](../Page/沙龍.md "wikilink")\[6\]，但是他在接下來的10年也無法廣為人知，在某種程度上是因為[普法戰爭所引發的動亂](../Page/普法戰爭.md "wikilink")。

1871年，[巴黎公社暴動期間](../Page/巴黎公社.md "wikilink")，他在[塞納河作畫時](../Page/塞納河.md "wikilink")，曾被政府誤以為是間諜而逮捕\[7\]。1874年，他與Jules
Le Cœur和他的家族結束一段長達10年的友誼，雷諾瓦失去該組織寶貴的財務支持\[8\]。

### 成名

[Pierre-Auguste_Renoir,_Le_Moulin_de_la_Galette.jpg](https://zh.wikipedia.org/wiki/File:Pierre-Auguste_Renoir,_Le_Moulin_de_la_Galette.jpg "fig:Pierre-Auguste_Renoir,_Le_Moulin_de_la_Galette.jpg")》（1876年）\]\]

當他的六幅畫作在印象派畫展中展出，雷諾瓦終於獲得評論家的好評。同年，他的作品在[倫敦與](../Page/倫敦.md "wikilink")[杜蘭德-魯埃爾](../Page/杜蘭德-魯埃爾.md "wikilink")（Paul
Durand-Ruel）共同展出\[9\]。1870年代，他參加了[落選者沙龍](../Page/落選者沙龍.md "wikilink")，開始了印象畫派的說明和推廣。雷諾瓦在這個時期不像[卡米耶·畢沙羅或](../Page/卡米耶·畢沙羅.md "wikilink")[高更等畫家走避他鄉](../Page/高更.md "wikilink")，而是待在巴黎，畫了很多街頭生活風景做為紀錄，這些畫作他都用大尺寸的畫布繪畫。經典之作《[煎餅磨坊的舞會](../Page/煎餅磨坊的舞會.md "wikilink")》（Le
Bal au Moulin de la Galette或Dance at Le Moulin de la
Galette）就是他在1876年畫出來的，此畫有一米三高，一米七寬，現收藏於法國[奧塞博物館](../Page/奧塞博物館.md "wikilink")。

[Pierre-Auguste_Renoir_006.jpg](https://zh.wikipedia.org/wiki/File:Pierre-Auguste_Renoir_006.jpg "fig:Pierre-Auguste_Renoir_006.jpg")\]\]

1881年，他前往[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")，一个他同[歐仁·德拉克洛瓦联系起来的国家](../Page/歐仁·德拉克洛瓦.md "wikilink")\[10\]，然後到了[西班牙](../Page/西班牙.md "wikilink")[馬德里](../Page/馬德里.md "wikilink")，參觀[委拉斯開茲的作品](../Page/委拉斯開茲.md "wikilink")。隨後，他來到[意大利](../Page/意大利.md "wikilink")，在[佛羅倫薩和](../Page/佛羅倫薩.md "wikilink")[羅馬參觀](../Page/羅馬.md "wikilink")[拉斐爾與](../Page/拉斐爾.md "wikilink")[提香的畫作](../Page/提香.md "wikilink")。1882年1月15日，雷諾瓦在[西西里大區](../Page/西西里大區.md "wikilink")[巴勒莫會見了作曲家](../Page/巴勒莫.md "wikilink")[理查德·瓦格納](../Page/理查德·瓦格納.md "wikilink")。雷諾瓦在短短35分鐘內完成瓦格納的[肖像畫](../Page/肖像畫.md "wikilink")。同年，雷諾瓦感染[肺炎](../Page/肺炎.md "wikilink")，在阿爾及利亞休養六個星期，他的[呼吸系統因此永久受損](../Page/呼吸系統.md "wikilink")\[11\]。

1881年，他畫出了另一個經典的作品《[船上的午宴](../Page/船上的午宴.md "wikilink")》（Luncheon of the
Boating
Party）。但在此畫之後，雷諾瓦逐漸放棄專注在製造光影的呈現，而趨向於較平實的描繪。雷諾瓦在1883年於[根西島度過了夏天](../Page/根西島.md "wikilink")，創造了15幅畫作。這些畫的主題包括海灘、[懸崖和](../Page/懸崖.md "wikilink")[海灣](../Page/海灣.md "wikilink")。

雷諾瓦在[蒙馬特生活與工作時](../Page/蒙馬特.md "wikilink")，他僱用[蘇珊·法拉登來擔任](../Page/蘇珊·法拉登.md "wikilink")[模特](../Page/模特.md "wikilink")\[12\]，許多他的同業也學習他們的繪畫技術，最終她成為了早期的女畫家之一。

[Renoir23.jpg](https://zh.wikipedia.org/wiki/File:Renoir23.jpg "fig:Renoir23.jpg")》，繪於1892年\]\]

1887年，[維多利亞女王慶祝她登基五十週年](../Page/維多利亞女王.md "wikilink")，雷諾瓦捐贈了幾幅畫作給“法國印象派繪畫展”作為忠誠的象徵。

1890年，他娶了艾琳。他在1885年已經擁有一個孩子[皮埃爾·雷諾瓦](../Page/皮埃爾·雷諾瓦.md "wikilink")\[13\]。在結婚後，雷諾瓦以他的妻子和日常家庭生活的場景創作許多畫作，包括他們的孩子和護士、艾琳的[親戚](../Page/親戚.md "wikilink")[加布里埃爾·勒納爾](../Page/加布里埃爾·勒納爾.md "wikilink")。雷諾瓦有三個兒子，其中[尚·雷諾瓦成為電影導演](../Page/尚·雷諾瓦.md "wikilink")，而皮埃爾成為了[舞台劇和](../Page/舞台劇.md "wikilink")[電影](../Page/電影.md "wikilink")[演員](../Page/演員.md "wikilink")。

### 晚年

雷諾瓦大約在1892年罹患[類風濕關節炎](../Page/類風濕關節炎.md "wikilink")。1907年，他搬到溫暖的“Collettes，因為這裡接近[地中海海岸](../Page/地中海.md "wikilink")\[14\]。雷諾瓦在生命最後20年中持續作畫，即使關節炎嚴重限制了他的活動，他不得不坐在[輪椅](../Page/輪椅.md "wikilink")。他的右肩膀因關節變形，所以雷諾瓦改變他的繪畫技巧。傳說雷諾瓦將筆綁在他癱瘓的[手指中](../Page/手指.md "wikilink")\[15\]，但是這是誤傳，當時雷諾瓦仍然能夠使用畫筆，雖然他需要一位助手把它放在他的手\[16\]。

在此期間，他與一個年輕的藝術家[理查德·吉諾](../Page/理查德·吉諾.md "wikilink")（Richard
Guino）合作來創作[雕塑](../Page/雕塑.md "wikilink")。當時雷諾瓦的肖像畫主角是[奧地利女演員Tilla](../Page/奧地利.md "wikilink")
Durieux。雷諾瓦晚年仍勤勉的在繪畫上精進，他畫了很多裸體的女性，將柔軟的視覺觸感表現的栩栩如生。

雷諾瓦在1919年參觀了[羅浮宮](../Page/羅浮宮.md "wikilink")，看到自己的作品被懸掛在其中。1919年12月3日，他於[普羅旺斯-阿爾卑斯-蔚藍海岸](../Page/普羅旺斯-阿爾卑斯-蔚藍海岸.md "wikilink")[卡涅與世長辭](../Page/卡涅.md "wikilink")。

[國際天文學聯合會於](../Page/國際天文學聯合會.md "wikilink")1976年將[水星上](../Page/水星.md "wikilink")18.29°S，51.89°W的[撞擊坑命名為](../Page/撞擊坑.md "wikilink")[雷諾瓦撞擊坑](../Page/雷諾瓦撞擊坑.md "wikilink")\[17\]。

## 繪畫風格

[Renoir_-_The_Two_Sisters,_On_the_Terrace.jpg](https://zh.wikipedia.org/wiki/File:Renoir_-_The_Two_Sisters,_On_the_Terrace.jpg "fig:Renoir_-_The_Two_Sisters,_On_the_Terrace.jpg")\]\]

雷诺阿以人物畫出名，這之中又以畫甜美、悠閒的氣氛還有豐滿、明亮的[臉和](../Page/臉.md "wikilink")[手最為經典](../Page/手.md "wikilink")。印象派風格的雷諾瓦，其特色在描繪迷人的感覺，從他的畫作中你很少感覺到苦痛或是[宗教情懷](../Page/宗教.md "wikilink")，但常常能感受到家庭的溫暖，如[母親或是年長姐姐般的笑容](../Page/母親.md "wikilink")。雷諾瓦認為繪畫並非[科學性的分析光線](../Page/科學性.md "wikilink")，也並非巧心的安排布局，因為繪畫是要帶給觀者愉悅，讓繪畫掛置的環境充滿了畫家想要的感覺。

雷诺阿最初的作品受到[欧仁·德拉克罗瓦與](../Page/欧仁·德拉克罗瓦.md "wikilink")[讓-巴蒂斯·卡米耶·柯洛的影響](../Page/讓-巴蒂斯·卡米耶·柯洛.md "wikilink")。他也很佩服[居斯塔夫·库尔贝和](../Page/居斯塔夫·库尔贝.md "wikilink")[愛德華·馬奈的風格](../Page/愛德華·馬奈.md "wikilink")，而他的早期作品將他們的風格融合其中。同時，雷諾阿也欣賞[埃德加·德加的運動感](../Page/埃德加·德加.md "wikilink")。18世紀大師[弗朗索瓦·布歇是雷諾瓦另一位非常欽佩的畫家](../Page/弗朗索瓦·布歇.md "wikilink")\[18\]。

他早期的成熟作品是典型的印象派風格，描述現實生活，並充滿了閃閃發亮的[色彩和](../Page/色彩.md "wikilink")[光線](../Page/光線.md "wikilink")。然而，到了19世紀80年代中期，他打破了之前的風格，採用一個更加具有紀律性的技術來描繪人物畫，特別是女性泳客。

雷诺阿於1881年意大利之旅中，看到[拉斐爾和其他](../Page/拉斐爾.md "wikilink")[文藝復興時期大師的作品](../Page/文藝復興時期.md "wikilink")，也使他確信之前的呈現方式有誤。他在未來的幾年中，其畫作風格更加銳利，試圖返回[古典主義風格](../Page/古典主義.md "wikilink")。這段時期有時被稱為「安格爾時期」，他專注於繪畫，並強調外部的輪廓\[19\]。

1890年後，他又改變了畫作風格，再次返回到早期作品的風格。從這個時期開始，他集中注意在[裸體和家庭場景畫](../Page/裸體.md "wikilink")。

## 重要作品

  - 《[煎餅磨坊的舞會](../Page/煎餅磨坊的舞會.md "wikilink")》，1876年
  - 《[船上的午宴](../Page/船上的午宴.md "wikilink")》，1880年
  - 《[小艾琳](../Page/小艾琳.md "wikilink")》，1880年
  - 《[彈鋼琴的少女](../Page/彈鋼琴的少女.md "wikilink")》，1892年

## 參見

  - [让·雷诺阿](../Page/让·雷诺阿.md "wikilink")，雷诺阿次子，后来成为电影导演
  - [雷诺阿 (电影)](../Page/雷诺阿_\(电影\).md "wikilink")，讲述雷诺阿晚年生活的2012年法国电影

## 參考資料

<div class="references-small">

<references />

</div>

## 相關文獻

  -
  -
  -
  -
  -
  -
  -
## 外部链接

  - [雷诺阿的作品和照片](http://www.chez.com/renoir/)
  - [Renoir](http://www.moreeuw.com/histoire-art/pierre-auguste-renoir.htm)

[R](../Category/法国画家.md "wikilink") [R](../Category/印象派畫家.md "wikilink")
[皮耶-奧古斯特·雷諾瓦](../Category/皮耶-奧古斯特·雷諾瓦.md "wikilink")

1.  Read, Herbert: *The Meaning of Art*, page 127. Faber, 1931.

2.  让·雷諾瓦（Renoir, Jean）：*Renoir, My
    Father*（《雷諾瓦，我的父親》），第57～67頁。Collins，1962年。

3.  安伯伊斯·佛拉德（Vollard, Ambroise）: *Renoir, An Intimate
    Record*（《雷諾瓦，一份詳盡紀錄》），第24～29頁。Knopf，1925年。

4.

5.  同上，第30頁。

6.  尼可拉斯·瓦德雷（Wadley, Nicholas）：*RenoirW, A
    Retrospective*（《雷諾瓦，一個回憶》），第15頁。Park
    Lane，1989年。

7.  Renoir, Jean, pages 118–21. Different and less life-threatening
    versions are offered by Paul Valéry and Vollard. In all accounts,
    however, their re-acquaintance led to great celebration.

8.  Wadley, page 15.

9.
10. Poulet, A. L., & Murphy, A. R. (1979). *Corot to Braque: French
    Paintings from the Museum of Fine Arts, Boston*, page 117. Boston:
    The Museum. ISBN 978-0-87846-134-9.

11. Wadley, page 25.

12. Wadley, pages 371, 374.

13.
14. Wadley, page 28.

15. André, Albert: *Renoir*. Crés, 1928.

16.

17.

18. Rey, Robert: *La Peinture française à la fin du XIXe siècle, la
    renaissance du sentiment classique : Degas, Renoir, Cézanne,
    Gauguin, Seurat*, Les Beaux-Arts, Van Oest, 1931 (thesis).

19. Asked late in life if he felt an affinity to Ingres, he responded:
    "I should very much like to". Rey, quoted in Wadley, page 336.