**益田市**（）是位於日本[島根縣西部的](../Page/島根縣.md "wikilink")[市](../Page/市.md "wikilink")，與[濱田市同為島根縣西部的主要都市](../Page/濱田市.md "wikilink")。與[濱田市](../Page/濱田市.md "wikilink")、[大田市并稱為石見三田](../Page/大田市.md "wikilink")。

市中心位於[高津川下游的](../Page/高津川.md "wikilink")[益田平原三角州地區](../Page/益田平原.md "wikilink")，轄區南部位於[中國山地](../Page/中國山地.md "wikilink")。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[美濃郡益田町](../Page/美濃郡.md "wikilink")、高津村、吉田村、鎌手村、種村、北仙道村、真砂村、豐川村、豐田村、高城村、二條村、美濃村、中西村、安田村、小野村、都茂村、東仙道村、二川村、匹見上村、匹見下村、道川村。
  - 1922年10月1日：高津村改制為[高津町](../Page/高津町.md "wikilink")。
  - 1934年7月1日：吉田村改制為[吉田町](../Page/吉田町.md "wikilink")。
  - 1941年2月11日：益田町、高津町、吉田町合併為**石見町**。
  - 1943年7月15日：石見町改名為**益田町**。
  - 1952年8月1日：益田町、北仙道村、豐川村、豐田村、高城村、中西村、安田村、小野村合併為**益田市**。
  - 1954年4月1日：都茂村、東仙道村、二川村合併為美都村。
  - 1955年2月1日：匹見上村、匹見下村、道川村合併為為匹見村。
  - 1955年3月25日：鎌手村、種村、真砂村、二條村、美濃村被併入益田市。
  - 1956年4月1日：匹見村改制為[匹見町](../Page/匹見町.md "wikilink")。
  - 1957年4月1日：美都村改制為[美都町](../Page/美都町.md "wikilink")。
  - 2004年11月1日：美都町、匹見町被併入益田市。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>益田町</p></td>
<td><p>1941年2月11日<br />
石見町<br />
1943年7月15日<br />
益田町</p></td>
<td><p>1952年8月1日<br />
益田市</p></td>
<td><p>益田市</p></td>
<td><p>益田市</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>高津村</p></td>
<td><p>1922年10月1日<br />
高津町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>吉田村</p></td>
<td><p>1934年7月1日<br />
吉田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>北仙道村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>豐川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>豐田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高城村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中西村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>安田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>小野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鎌手村</p></td>
<td><p>1955年3月25日<br />
併入益田市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>種村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>真砂村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>二條村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>美濃村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>都茂村</p></td>
<td><p>1954年4月1日<br />
美都村</p></td>
<td><p>1957年4月1日<br />
美都町</p></td>
<td><p>2004年11月1日<br />
併入益田市</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>東仙道村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>二川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>匹見上村</p></td>
<td><p>1955年2月1日<br />
匹見村</p></td>
<td><p>1956年4月1日<br />
匹見町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>匹見下村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>道川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任市長

1.  [伊藤正男](../Page/伊藤正男.md "wikilink")：1952年8月25日-1964年8月31日，共三任
2.  [島田暉山](../Page/島田暉山.md "wikilink")：964年8月2日-1976年8月1日，共三任
3.  [神崎治一郎](../Page/神崎治一郎.md "wikilink")：1976年8月2日-1992年8月1日，共四任
4.  [涉谷義人](../Page/涉谷義人.md "wikilink")：1992年8月2日-1996年8月1日
5.  [田中八洲男](../Page/田中八洲男.md "wikilink")：1996年8月2日-2000年8月1日
6.  [牛尾郁夫](../Page/牛尾郁夫.md "wikilink")：2000年8月2日-2008年8月2日，共兩任
7.  [福原慎太郎](../Page/福原慎太郎.md "wikilink")：2008年8月2日-2012年8月1日，一任
8.  [山本浩章](../Page/山本浩章.md "wikilink")：2012年8月2日 -現任

## 產業

### 漁港

  - 土田漁港
  - 大濱漁港
  - 木部漁港
  - 津田漁港
  - 小濱漁港
  - 飯浦漁港

## 交通

[Hagi-Iwami_Airport_(IWJ_RJOW)_2.jpg](https://zh.wikipedia.org/wiki/File:Hagi-Iwami_Airport_\(IWJ_RJOW\)_2.jpg "fig:Hagi-Iwami_Airport_(IWJ_RJOW)_2.jpg")
[JR-Masuda-sta.jpg](https://zh.wikipedia.org/wiki/File:JR-Masuda-sta.jpg "fig:JR-Masuda-sta.jpg")
益田市的對外交通主要依靠鐵路，在2001年[山陰本線完成高速化事業後](../Page/山陰本線.md "wikilink")，現在從縣都[松江市搭乘特急列車可在兩個小時內抵達](../Page/松江市.md "wikilink")。

雖然轄區內有[石見機場](../Page/石見機場.md "wikilink")，但是每天僅有[日空航空往返](../Page/日空航空.md "wikilink")[東京和](../Page/東京.md "wikilink")[大阪各一班而以](../Page/大阪.md "wikilink")，且大阪之航線已決定於2011年1月5日停飛。

### 機場

  - [石見機場](../Page/石見機場.md "wikilink")（又稱：萩・石見機場）

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [山陰本線](../Page/山陰本線.md "wikilink")：（←[濱田市](../Page/濱田市.md "wikilink")）
        - [鎌手車站](../Page/鎌手車站.md "wikilink") -
        [石見津田車站](../Page/石見津田車站.md "wikilink") -
        [益田車站](../Page/益田車站_\(日本\).md "wikilink") -
        [戶田小濱車站](../Page/戶田小濱車站.md "wikilink") -
        [飯浦車站](../Page/飯浦車站.md "wikilink") -
        （[萩市](../Page/萩市.md "wikilink")→）
      - [山口線](../Page/山口線.md "wikilink")：（←[津和野町](../Page/津和野町.md "wikilink")）
        - [石見橫田車站](../Page/石見橫田車站.md "wikilink") -
        [本俣賀車站](../Page/本俣賀車站.md "wikilink") - 益田車站

### 道路

  - [高速道路](../Page/高速道路.md "wikilink")

<!-- end list -->

  - [山陰自動車道](../Page/山陰自動車道.md "wikilink")：[鎌手交流道](../Page/鎌手交流道.md "wikilink")
    - [遠田交流道](../Page/遠田交流道.md "wikilink") -
    [久城交流道](../Page/久城交流道.md "wikilink") -
    [高津交流道](../Page/高津交流道.md "wikilink") -
    [萩・石見空港交流道](../Page/萩・石見空港交流道.md "wikilink") -
    [須子交流道](../Page/須子交流道.md "wikilink")

## 觀光資源

[Ikouji.jpg](https://zh.wikipedia.org/wiki/File:Ikouji.jpg "fig:Ikouji.jpg")

### 景點

  - 蟠龍湖
  - [醫光寺](../Page/醫光寺.md "wikilink")
  - [萬福寺 (益田市)](../Page/萬福寺_\(益田市\).md "wikilink")
  - [七尾城](../Page/七尾城_\(石見國\).md "wikilink")（益田城）
  - [三宅御土居](../Page/三宅御土居.md "wikilink")
  - 七尾公園（七尾城遺跡、賞櫻景點）
  - [高津柿本神社](../Page/高津柿本神社.md "wikilink")
  - [雙川峽](../Page/雙川峽.md "wikilink")
  - [匹見峽](../Page/匹見峽.md "wikilink")

### 祭典

  - 益田祇園祭（7月下旬，夏休的第一個周末）
  - 益田祭（4月第3個星期日）
  - 益田七尾祭（11月3日）
  - 益田萬葉祭（4月29日）
  - 水鄉祭（8月第1個星期六）
  - 八朔祭（9月1日）
  - [流鏑馬](../Page/流鏑馬.md "wikilink")（9月1日）
  - [石見神樂](../Page/石見神樂.md "wikilink")

## 教育

### 高等學校

  - 島根縣立益田高等學校
  - 島根縣立益田翔陽高等學校
  - 七尾學園 益田東高等學校
  - 益田永島學園 明誠高等學校

### 特殊學校

  - 島根縣立益田養護學校

## 姊妹、友好都市

### 日本

  - [雪舟高峰會](../Page/雪舟.md "wikilink")：參加的市町村益田市、[豐後大野市](../Page/豐後大野市.md "wikilink")（[大分縣](../Page/大分縣.md "wikilink")）、[井原市](../Page/井原市.md "wikilink")（[岡山縣](../Page/岡山縣.md "wikilink")）、[總社市](../Page/總社市.md "wikilink")（岡山縣）、[川崎町](../Page/川崎町_\(福岡縣\).md "wikilink")（[福岡縣](../Page/福岡縣.md "wikilink")）、[山口市](../Page/山口市.md "wikilink")（[山口縣](../Page/山口縣.md "wikilink")）
  - [高槻市](../Page/高槻市.md "wikilink")（[大阪府](../Page/大阪府.md "wikilink")）

### 海外

  - [寧波市](../Page/寧波市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")）

  - [瓦納卡](../Page/瓦納卡.md "wikilink")（[紐西蘭](../Page/紐西蘭.md "wikilink")[奧塔哥大區](../Page/奧塔哥大區.md "wikilink")）

## 本地出身之名人

  - [青木直人](../Page/青木直人.md "wikilink")（[記者](../Page/記者.md "wikilink")）
  - [阿知波信介](../Page/阿知波信介.md "wikilink")（[演員](../Page/演員.md "wikilink")）
  - [石川裕司](../Page/石川裕司.md "wikilink")（[職業足球選手](../Page/職業足球.md "wikilink")）
  - [岩本薫](../Page/岩本薫.md "wikilink")（[圍棋九段棋士](../Page/圍棋.md "wikilink")、曾任[日本棋院理事長](../Page/日本棋院.md "wikilink")）
  - [岡崎綾子](../Page/岡崎綾子.md "wikilink")（[柔道選手](../Page/柔道.md "wikilink")）
  - [川本貢司](../Page/川本貢司.md "wikilink")（[指揮家](../Page/指揮家.md "wikilink")）
  - [栗山文昭](../Page/栗山文昭.md "wikilink")（[指揮家](../Page/指揮家.md "wikilink")）
  - [田畑修一郎](../Page/田畑修一郎.md "wikilink")（[作家](../Page/作家.md "wikilink")）
  - [田渕久美子](../Page/田渕久美子.md "wikilink")（[編劇](../Page/編劇.md "wikilink")）
  - [秦佐八郎](../Page/秦佐八郎.md "wikilink")（[細菌學者](../Page/細菌學者.md "wikilink")、[醫學博士](../Page/醫學博士.md "wikilink")、[諾貝爾生理學或醫學獎候選人](../Page/諾貝爾生理學或醫學獎.md "wikilink")）
  - [豐田真奈美](../Page/豐田真奈美.md "wikilink")（[女子職業摔角選手](../Page/女子職業摔角.md "wikilink")）
  - [七咲友梨](../Page/七咲友梨.md "wikilink")（[女演員](../Page/女演員.md "wikilink")）
  - [日高郁人](../Page/日高郁人.md "wikilink")（[職業摔角選手](../Page/職業摔角選手.md "wikilink")）
  - [桝木亞子](../Page/桝木亞子.md "wikilink")（[泳裝模特兒](../Page/泳裝.md "wikilink")、[女演員](../Page/女演員.md "wikilink")）
  - [御神本訓史](../Page/御神本訓史.md "wikilink")（[騎師](../Page/騎師.md "wikilink")）
  - [溝口善兵衛](../Page/溝口善兵衛.md "wikilink")（[島根縣](../Page/島根縣.md "wikilink")[知事](../Page/知事.md "wikilink")、前[財務省](../Page/財務省_\(日本\).md "wikilink")[財務官](../Page/財務官_\(日本\).md "wikilink")）

## 外部連結

  - [益田市觀光協會](https://web.archive.org/web/20100922091431/http://masudashi.com/)

  - [匹見町觀光協會](http://www.iwami.or.jp/hish/)

  - [美濃商工會](https://web.archive.org/web/20111006031012/http://www.mito-sk.jp/)