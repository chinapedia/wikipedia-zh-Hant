**盎司**（；符號：oz），或作**盎斯**，[港譯](../Page/香港用語.md "wikilink")「安-{}-士」或「盎士」，為[英制](../Page/英制.md "wikilink")[質量](../Page/質量.md "wikilink")、[重量或](../Page/重量.md "wikilink")[體積的单位](../Page/體積.md "wikilink")，旧称**英两**或**唡**。

## 作為質量單位

  - [常衡盎司](../Page/常衡.md "wikilink")：質量单位，整体缩写为oz.av。

<!-- end list -->

  -
    1盎司 = 28.349523125[克](../Page/克.md "wikilink")
    1盎司 = 16[打兰](../Page/打兰.md "wikilink")（dram）
    1盎司 = 437.5[格林](../Page/格林_\(單位\).md "wikilink")
    16盎司 = 1[磅](../Page/磅.md "wikilink")（pound）

<!-- end list -->

  - [金衡盎司](../Page/金衡.md "wikilink")：質量单位，整体缩写为oz.tr（英）、oz.t（美），常见于金银等[贵金属的计量](../Page/贵金属.md "wikilink")。

<!-- end list -->

  -
    1盎司 = 480格林
    1盎司 = 31.1034768 克
    12盎司 = 1磅

<!-- end list -->

  - [药衡盎司](../Page/药衡.md "wikilink")：質量单位，同金衡盎司，整体缩写为ap oz。

<!-- end list -->

  -
    1盎司 = 31.1034768克

## 作為重量的單位

1[盎司力](../Page/盎司力.md "wikilink")（Ounce-force）等於[磅力或](../Page/磅力.md "wikilink")。\[1\]

## 作為體積單位

  - 液盎司（Fluid ounce）是容量计量单位，符号为fl. oz。

<!-- end list -->

  -
    160英-{制}-液盎司 = 1英-{制}-[加侖](../Page/加侖.md "wikilink")
    1英-{制}-液盎司 = 28.4130625[毫升](../Page/毫升.md "wikilink")
    128美-{制}-液盎司 = 1美-{制}-加侖
    1美-{制}-液盎司 = 29.5735295625毫升

## 用盎司表示厚度

在[印刷電路板及](../Page/印刷電路板.md "wikilink")[柔性印刷電路板的製作中](../Page/柔性印刷电路板.md "wikilink")，會用盎司表示其[銅箔的厚度](../Page/銅箔.md "wikilink")，1盎司銅箔表示在一平方[英尺的面積上舖重量一盎司的銅](../Page/英尺.md "wikilink")，相當於0.034[公厘或](../Page/公厘.md "wikilink")1.3[mil](../Page/密耳.md "wikilink")\[2\]。

## 參看

  - [英制](../Page/英制.md "wikilink")

## 參考

## 外部链接

  - [计量单位转换的计算工具](https://web.archive.org/web/20041119002646/http://amblyopia.nease.net/7ykwz/cxdq/dlh.htm)

[category: 體積單位](../Page/category:_體積單位.md "wikilink")

[Category:英制](../Category/英制.md "wikilink")
[Category:美國度量衡單位](../Category/美國度量衡單位.md "wikilink")

1.  [ounce-force](https://en.oxforddictionaries.com/definition/ounce-force)
2.  [cadence
    銅箔厚度的計算](https://www.cadence.com/tw/Resources/thickness.pdf)