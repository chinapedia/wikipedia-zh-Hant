**盎格鲁美洲**（[英语](../Page/英语.md "wikilink")：****），又称**英語美洲**，用作描述以[英語為主要語言](../Page/英語.md "wikilink")，或者與[英格蘭或](../Page/英格蘭.md "wikilink")[英倫三島在歷史](../Page/英倫三島.md "wikilink")、語言或文化上有密切關係的[美洲地區](../Page/美洲.md "wikilink")，也可以指[英語世界的美洲部份](../Page/英語世界.md "wikilink")，與操[羅曼語族諸語的](../Page/羅曼語族.md "wikilink")[拉丁美洲區分](../Page/拉丁美洲.md "wikilink")。

具體而言，英語美洲包括[北美洲的](../Page/北美洲.md "wikilink")[美國和](../Page/美國.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")\[1\]，而**Anglophone
America**一詞也可以用於較闊的定義，當中包括[中美洲的](../Page/中美洲.md "wikilink")[伯利茲](../Page/伯利茲.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")，[加勒比海的](../Page/加勒比海.md "wikilink")[牙買加](../Page/牙買加.md "wikilink")，[南美洲的](../Page/南美洲.md "wikilink")[圭亞那](../Page/圭亞那.md "wikilink")、[北大西洋上的](../Page/北大西洋.md "wikilink")[百慕大以及幾個靠近南美洲的加勒比海國家](../Page/百慕大.md "wikilink")。注意[蘇利南](../Page/蘇利南.md "wikilink")、[阿魯巴島和](../Page/阿魯巴島.md "wikilink")[荷屬安地列斯由於以](../Page/荷屬安地列斯.md "wikilink")[荷蘭語為通用語](../Page/荷蘭語.md "wikilink")，並不屬本區範圍。在[南大西洋的](../Page/南大西洋.md "wikilink")[福克蘭群島以英語為官方語言](../Page/福克蘭群島.md "wikilink")。

英語美洲可以形容以下的情況：

  - 描述英國、美國和加拿大英語地區的共同文化環境，例如「英語美洲文化與法國文化不同」。[邱吉爾](../Page/邱吉爾.md "wikilink")、[小羅斯福](../Page/小羅斯福.md "wikilink")、[雷根等政府領袖用此形勢英美兩國的特殊關係](../Page/雷根.md "wikilink")。

<!-- end list -->

  - 描述[英國](../Page/英國.md "wikilink") （或特指英格蘭）和美洲
    （特指美國）之間的關係，如「英美在[1812年戰爭前夕關係緊張](../Page/1812年戰爭.md "wikilink")」。

本詞作為名詞使用時，可以指使用英語的[白人](../Page/白人.md "wikilink")，簡稱‘Anglo’，最常見於對說英語的美國白人和在[美墨戰爭期間在](../Page/美墨戰爭.md "wikilink")[美國西南部說](../Page/美國西南部.md "wikilink")[西班牙語的白人歷史的研究當中](../Page/西班牙語.md "wikilink")。但本詞一般忽略英格蘭、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[日耳曼和其他](../Page/日耳曼.md "wikilink")[北歐移民後裔的差異](../Page/北歐.md "wikilink")，因而構成了說英語的美國白人的主體。

## 参考文献

## 参见

  - [盎格魯-薩克遜人](../Page/盎格魯-薩克遜人.md "wikilink")

{{-}}

[Category:美洲](../Category/美洲.md "wikilink")
[Category:地缘政治](../Category/地缘政治.md "wikilink")
[Category:英语](../Category/英语.md "wikilink")

1.  "[North America](http://www.bartleby.com/65/na/NAmer.html)"
    ''[1](http://www.bartleby.com/65) 哥倫比亞百科全書，第六版，2001年5月。紐約：哥倫比亞大學出版社。