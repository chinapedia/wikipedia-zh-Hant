[Kimphucformerhome2008.jpg](https://zh.wikipedia.org/wiki/File:Kimphucformerhome2008.jpg "fig:Kimphucformerhome2008.jpg")
**盏盘县**（\[1\]）是[越南](../Page/越南.md "wikilink")[西寧省下轄的一個縣](../Page/西寧省.md "wikilink")。

## 地理

盏盘县位于西宁省南部，北接[边求县和](../Page/边求县.md "wikilink")[鹅油县](../Page/鹅油县.md "wikilink")，东接[平阳省](../Page/平阳省.md "wikilink")[油汀县和](../Page/油汀县.md "wikilink")[胡志明市](../Page/胡志明市.md "wikilink")[纠支县](../Page/纠支县.md "wikilink")，南接[隆安省](../Page/隆安省.md "wikilink")[德和县和](../Page/德和县.md "wikilink")[德惠县](../Page/德惠县.md "wikilink")，西接[柬埔寨](../Page/柬埔寨.md "wikilink")，面积334.61平方千米，22号国道横穿县境。

## 历史

[阮朝时](../Page/阮朝.md "wikilink")，盏盘县隶属[嘉定省](../Page/嘉定省.md "wikilink")[西宁府](../Page/西宁府.md "wikilink")[光化县](../Page/光化县.md "wikilink")。1867年，[法属交趾支那划分全境为](../Page/法属交趾支那.md "wikilink")24个清查辖，在盏盘设立光化清查辖，后又更名为盏盘清查辖。1870年，法属交趾支那和[柬埔寨划定边界](../Page/柬埔寨保护国.md "wikilink")，盏盘清查辖管辖的高棉人地区（今柬埔寨[柴桢省](../Page/柴桢省.md "wikilink")）划归柬埔寨。

1900年1月1日，西宁参办辖改制为西宁省，盏盘成为下属的[盏盘郡](../Page/盏盘郡.md "wikilink")。1963年，盏盘郡划归新成立的[厚义省](../Page/厚义省.md "wikilink")。

在[越南战争期间](../Page/越南战争.md "wikilink")，盏盘曾多次遭到轰炸。1972年6月8日，南越飞行员在盏盘误认敌军而对难民进行轰炸。当时美联社的摄影记者[黄幼公拍下](../Page/黄幼公.md "wikilink")[潘氏金福被](../Page/潘氏金福.md "wikilink")[燒夷彈轟炸之後哭叫著逃跑的照片](../Page/燒夷彈.md "wikilink")，成为[反戰](../Page/反戰.md "wikilink")[象徵](../Page/象徵.md "wikilink")。

1975年，[越南南方共和国接管盏盘](../Page/越南南方共和国.md "wikilink")，盏盘县隶属西宁省。

2004年1月12日，盏盘县以禄兴社1681公顷土地和2636人、敦顺社2606公顷土地和6281人析置兴顺社\[2\]。

## 行政区划

盏盘县下辖1市镇10社。

  - 盏盘市镇（Thị trấn Trảng Bàng）
  - 安和社（Xã An Hòa）
  - 安静社（Xã An Tịnh）
  - 平盛社（Xã Bình Thạnh）
  - 敦顺社（Xã Đôn Thuận）
  - 嘉平社（Xã Gia Bình）
  - 嘉禄社（Xã Gia Lộc）
  - 兴顺社（Xã Hưng Thuận）
  - 禄兴社（Xã Lộc Hưng）
  - 福祉社（Xã Phước Chỉ）
  - 福留社（Xã Phước Lưu）

## 经济

盞盤縣的經濟以傳統手工為主，近年來開拓了提供外商投資的工業區。

## 饮食

盏盘县有一項知名料理為[盞盤湯粉](../Page/盞盤湯粉.md "wikilink")（一種麵食，搭配[豬肉與當地](../Page/豬肉.md "wikilink")[藥草](../Page/藥草.md "wikilink")）。

## 注释

[Category:西寧省](../Category/西寧省.md "wikilink")
[Z](../Category/越南县份.md "wikilink")

1.  漢字寫法來自《[南圻六省地輿誌](../Page/南圻六省地輿誌.md "wikilink")》。
2.  [21/2004/NĐ-CP：西宁省调整行政区划](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-dinh-21-2004-ND-CP-thanh-lap-xa-thuoc-huyen-Chau-Thanh-Trang-Bang-dieu-chinh-dia-gioi-hanh-chinh-huyen-Duong-Minh-Chau-Tan-Chau-tinh-Tay-Ninh-53359.aspx)