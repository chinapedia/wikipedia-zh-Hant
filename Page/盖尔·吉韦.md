**盖尔·吉韦**（，），是一名[法國職業足球員](../Page/法國.md "wikilink")，司職[後衛](../Page/後衛_\(足球\).md "wikilink")，現時效力[英冠球會](../Page/英冠.md "wikilink")[布力般流浪](../Page/布莱克本流浪者足球俱乐部.md "wikilink")。

## 生平

### 摩納哥

基維乃[摩納哥一手培訓出來的球員](../Page/摩納哥足球會.md "wikilink")，曾是2004年[歐冠杯亞軍成員之一](../Page/歐冠杯.md "wikilink")。2004年後擔任隊長一職。2006年他入選了[法國國家隊出戰](../Page/法國國家足球隊.md "wikilink")[世界杯](../Page/世界杯.md "wikilink")，雖然獲得亞軍，但並沒有正選上陣。

### 馬賽

2007年夏季他轉投了[法甲另一支球會](../Page/法甲.md "wikilink")[馬賽](../Page/馬賽足球會.md "wikilink")，轉會費為500萬[歐羅](../Page/歐羅.md "wikilink")。然而在球會打了一季，他與主教練艾历·格雷茨（Erik
Gerets）產生不和，因而於2008-09年上半球季沒有上陣過任何比賽。

### 布力般流浪

2009年1月15日基維被外借到[英超球會](../Page/英超.md "wikilink")[布力般流浪直到球季結束](../Page/布莱克本流浪者足球俱乐部.md "wikilink")，並有優先收購權\[1\]。5月26日布力般流浪正式收購基維，作價350萬[英鎊](../Page/英鎊.md "wikilink")，簽約四年\[2\]。

## 榮譽

  - [歐洲聯賽冠軍杯亞軍](../Page/歐洲聯賽冠軍杯.md "wikilink")：2004年；
  - [世界杯亞軍](../Page/世界杯.md "wikilink")：[2006年](../Page/2006年世界盃足球賽.md "wikilink")；

## 參考資料

## 外部連結

  - [球員個人官方網站](https://web.archive.org/web/20070929084233/http://www.gaelgivet.org/)

[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:摩納哥球員](../Category/摩納哥球員.md "wikilink")
[Category:馬賽球員](../Category/馬賽球員.md "wikilink")
[Category:布力般流浪球員](../Category/布力般流浪球員.md "wikilink")
[Category:羅訥河口省人](../Category/羅訥河口省人.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")

1.   [Blackburn sign Marseille's
    Givet](http://news.bbc.co.uk/sport2/hi/football/teams/b/blackburn_rovers/7830562.stm)，2009年1月15日，[BBC](../Page/BBC.md "wikilink")
    Sport，於2009年1月16日查閱
2.