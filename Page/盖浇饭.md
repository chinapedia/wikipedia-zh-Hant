[Kung_Pao_Chicken.jpg](https://zh.wikipedia.org/wiki/File:Kung_Pao_Chicken.jpg "fig:Kung_Pao_Chicken.jpg")蓋飯\]\]
[Scramble_Egg_with_Shrimps_with_Rice.jpg](https://zh.wikipedia.org/wiki/File:Scramble_Egg_with_Shrimps_with_Rice.jpg "fig:Scramble_Egg_with_Shrimps_with_Rice.jpg")[炒蛋飯](../Page/炒蛋.md "wikilink")\]\]
**-{zh-cn:盖浇饭;zh-hans:蓋澆飯;zh-hant:盖浇饭;zh-tw:蓋澆飯;zh-hk:碟頭飯;zh-mo:碟頭飯;}-**，簡稱**蓋飯**，是常见的[米饭料理做法](../Page/米饭.md "wikilink")，和[中国北方的](../Page/中国.md "wikilink")[打鹵麵有异曲同工之妙](../Page/打鹵麵.md "wikilink")，就是在米饭上浇入菜[卤](../Page/卤.md "wikilink")，做成一人份量的飯，不用碟碗齐上，以供客人迅速進食，省去挾菜麻煩，是大众方便[食品](../Page/食品.md "wikilink")，也是中式及日式[快餐常見的食品](../Page/快餐.md "wikilink")。

蓋澆飯可使用各种鹵菜，一般汁较浓、多。卤的口味和做法也有地域的差别。有[勾芡的](../Page/勾芡.md "wikilink")**-{zh-cn:盖浇饭;zh-hans:蓋澆飯;zh-hant:盖浇饭;zh-tw:蓋澆飯;zh-hk:碟頭飯;zh-mo:碟頭飯;}-**，則另稱為**燴飯**。在[香港與](../Page/香港.md "wikilink")[澳門](../Page/澳門.md "wikilink")，**-{zh-cn:盖浇饭;zh-hans:蓋澆飯;zh-hant:盖浇饭;zh-tw:蓋澆飯;zh-hk:碟頭飯;zh-mo:碟頭飯;}-**常用[碟子盛載](../Page/碟子.md "wikilink")，因此多稱為**碟頭飯**。在[日本](../Page/日本.md "wikilink")，蓋澆飯多以大[碗盛裝](../Page/碗.md "wikilink")，別稱為**[丼物](../Page/丼物.md "wikilink")**。蓋澆飯常在一些小[餐廳](../Page/餐廳.md "wikilink")、[路邊攤](../Page/路邊攤.md "wikilink")、中式和日式[速食店供應](../Page/速食店.md "wikilink")，在香港，一般碟頭飯餐會附有[飲品](../Page/飲品.md "wikilink")（凍飲要加收一至四元）或餐湯（[例湯](../Page/例湯.md "wikilink")）。

## 歷史

蓋澆飯的歷史可追溯至[西周](../Page/西周.md "wikilink")，當時稱為**淳熬**，做法是先「煎醢」（「醢」即肉醬），加在[大米飯上](../Page/大米.md "wikilink")，澆上油脂\[1\]。另有一種以小米製的叫「淳毋」。

[隋唐時发展成](../Page/隋唐.md "wikilink")「御黄王母饭」，有肉絲、雞蛋做配菜\[2\]，成为[唐代](../Page/唐代.md "wikilink")「烧尾宴」上的食品之一。後來就演變成現在的蓋澆飯。

至於日本的[丼物](../Page/丼物.md "wikilink")，相傳是在[幕府](../Page/幕府.md "wikilink")[室町時代出現](../Page/室町時代.md "wikilink")；炸豬排蓋飯的起源，則是在1921年([大正](../Page/大正.md "wikilink")10年)，由[早稻田高等學院的學生](../Page/早稻田.md "wikilink")，首先將炸豬排放在白飯上，淋上醬汁一起食用。

## 各地的蓋澆飯

[Lurou_fan(Taiwanese_cuisine).jpg](https://zh.wikipedia.org/wiki/File:Lurou_fan\(Taiwanese_cuisine\).jpg "fig:Lurou_fan(Taiwanese_cuisine).jpg")\]\]
[Hayashi_rice.jpg](https://zh.wikipedia.org/wiki/File:Hayashi_rice.jpg "fig:Hayashi_rice.jpg")\]\]
[Unagi_donburi.jpg](https://zh.wikipedia.org/wiki/File:Unagi_donburi.jpg "fig:Unagi_donburi.jpg")飯\]\]
[Rainbow_Drive-In_mix_plate_lunch_(2194653288).jpg](https://zh.wikipedia.org/wiki/File:Rainbow_Drive-In_mix_plate_lunch_\(2194653288\).jpg "fig:Rainbow_Drive-In_mix_plate_lunch_(2194653288).jpg")、牛肉和鸡肉的米饭\]\]

### 中國大陸

中国大陆的[快餐店内的诸多炒菜均可作为盖饭](../Page/快餐店.md "wikilink")，其中以[川菜为主](../Page/川菜.md "wikilink")。如[宫保鸡丁](../Page/宫保鸡丁.md "wikilink")、鱼香肉丝、[麻婆豆腐](../Page/麻婆豆腐.md "wikilink")、[回锅肉等](../Page/回锅肉.md "wikilink")。

### 香港

  - [燒味飯](../Page/燒味.md "wikilink")：直接將切好的[燒味放在白飯上](../Page/燒味.md "wikilink")，通常在燒味店售賣。
  - [廚房飯](../Page/廚房.md "wikilink")：先把[白飯裝在碟上](../Page/白飯.md "wikilink")，按客人下單炒菜，[炒好後舖在飯面上](../Page/炒.md "wikilink")。如菜遠[排骨飯](../Page/排骨.md "wikilink")、豉椒[鮮魷飯等](../Page/魷魚.md "wikilink")。也有些是預先煮好菜色再鋪上白飯上，又稱為兩餸飯或三餸飯，選擇可達20種以上，常見有煎紅衫魚、豆角/菜甫煎蛋、炸豬扒、生炒骨、麻婆豆腐、魚香茄子等。
    在新加坡亦有相類似的，名曰經濟米飯。
  - 中式飯類：以蒸為主要烹飪手法，如鳳爪排骨飯、梅菜蒸魚飯等。
  - 西式飯類：如排類（[香港粤語慣稱](../Page/香港粤語.md "wikilink")「-{扒}-」，如雜扒飯）、[午餐肉](../Page/午餐肉.md "wikilink")、[香腸](../Page/香腸.md "wikilink")、[火腿或](../Page/火腿.md "wikilink")[蛋](../Page/蛋.md "wikilink")，[煎蛋免治牛飯](../Page/煎蛋免治牛飯.md "wikilink")、[葡國雞飯等](../Page/葡國雞.md "wikilink")。
  - [炒飯](../Page/炒飯.md "wikilink")：將白飯及食材一同下鍋炒，以一份碟頭飯的份量出售，如[揚州炒飯](../Page/揚州炒飯.md "wikilink")、[鴛鴦炒飯等](../Page/鴛鴦炒飯.md "wikilink")。

### 臺灣

  - [滷肉飯](../Page/滷肉飯.md "wikilink")／[肉燥飯](../Page/肉燥飯.md "wikilink")
  - [排骨飯](../Page/排骨飯.md "wikilink")
  - [焢肉飯](../Page/焢肉飯.md "wikilink")
  - [牛腩飯](../Page/牛腩飯.md "wikilink")
  - [雞肉飯](../Page/雞肉飯.md "wikilink")
  - [火雞肉飯](../Page/火雞肉飯.md "wikilink")
  - [燒肉飯](../Page/燒肉飯.md "wikilink")
  - 各式[燴飯](../Page/燴.md "wikilink")：如海鮮燴飯、牛肉燴飯等，也有在炒飯上勾芡的蓋飯。

### 日本

  - [鳗鱼饭](../Page/鳗鱼饭.md "wikilink")：日本最早出現的蓋飯
  - [-{zh:牛丼; zh-cn:日式牛肉饭; zh-tw:牛丼;
    zh-hk:日式牛肉飯;}-](../Page/牛丼.md "wikilink")
  - [日式咖哩饭](../Page/日式咖哩饭.md "wikilink")
  - [日式豬排蓋飯](../Page/日式豬排蓋飯.md "wikilink")
  - [鯛魚飯](../Page/鯛魚飯.md "wikilink")
  - [親子丼](../Page/親子丼.md "wikilink")：以一種肉類和它的卵作配菜，如[鮭魚與鮭](../Page/鮭魚.md "wikilink")[魚子](../Page/魚子.md "wikilink")、[雞肉與雞](../Page/雞肉.md "wikilink")[蛋等](../Page/蛋.md "wikilink")。

### 東南亞

  - 泰式[酸辣海鮮蓋飯](../Page/酸辣海鮮蓋飯.md "wikilink")
  - 马来西亚、新加坡[中式杂菜饭](../Page/中式杂菜饭.md "wikilink")，米饭配上各式菜肴，有肉、蔬菜、各类炸鱼等，早餐即以各式炒粉麵取代飯。

### 夏威夷

  - [夏威夷米饭汉堡](../Page/夏威夷米饭汉堡.md "wikilink")（Loco moco）

  -
### 歐洲

  - [白汁海鮮飯](../Page/白汁海鮮飯.md "wikilink")

## 健康問題

香港[食物環境衞生署在](../Page/食物環境衞生署.md "wikilink")2004年3月至2005年7月測試香港的[粥粉麵飯發現](../Page/粥.md "wikilink")，碟頭飯由於份量較多，平均每碟含有近一千[卡路里的](../Page/卡路里.md "wikilink")[熱量](../Page/熱量.md "wikilink")，部分碟頭飯[纖維含量更近乎零](../Page/纖維.md "wikilink")，而碟頭飯的[鈉含量亦過高](../Page/鈉.md "wikilink")。

## 參考

  - [茶餐廳](../Page/茶餐廳.md "wikilink")
  - [炒底](../Page/炒底_\(茶餐廳\).md "wikilink")
  - [盒飯](../Page/盒飯.md "wikilink")

## 注释

## 外部連結

  - [盖浇饭为“淳熬”传](http://news.xinhuanet.com/food/2005-08/08/content_3323531.htm)

[Category:米饭](../Category/米饭.md "wikilink")
[Category:中国米饭](../Category/中国米饭.md "wikilink")
[Category:香港米飯](../Category/香港米飯.md "wikilink")
[Category:台灣米飯類食品](../Category/台灣米飯類食品.md "wikilink")

1.  《礼记注疏》：「煎醢加以陆稻上，沃之以膏。」
2.  韦巨源 《食单》：「编缕卵脂，盖饭表面，杂味。」