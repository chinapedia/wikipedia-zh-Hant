**盛力會**（せいりきかい）、本部位於[大阪市](../Page/大阪市.md "wikilink")[中央區](../Page/中央區_\(大阪市\).md "wikilink")[瓦屋町](../Page/瓦屋町.md "wikilink").（株）日本エステートネーヅメント旁.的設置，為[日本的](../Page/日本.md "wikilink")[指定暴力團之一](../Page/指定暴力團.md "wikilink")・六代目[山口組旗下的](../Page/山口組.md "wikilink")2次團體。正式與非正式成員約800人。2009年2月
組解散、[倭和會引繼地盤](../Page/倭和會.md "wikilink")。

其組織遍佈[大阪府](../Page/大阪府.md "wikilink")、[東京都](../Page/東京都.md "wikilink")、[宮城縣](../Page/宮城縣.md "wikilink")、[福島縣](../Page/福島縣.md "wikilink")、[關東地區](../Page/關東地區.md "wikilink")、[愛知縣](../Page/愛知縣.md "wikilink")、[岐阜縣](../Page/岐阜縣.md "wikilink")、[京都府](../Page/京都府.md "wikilink")、[奈良縣](../Page/奈良縣.md "wikilink")、[三重縣](../Page/三重縣.md "wikilink")、[和歌山縣](../Page/和歌山縣.md "wikilink")、[兵庫縣](../Page/兵庫縣.md "wikilink")、[島根縣](../Page/島根縣.md "wikilink")、[鳥取縣](../Page/鳥取縣.md "wikilink")、[山口縣](../Page/山口縣.md "wikilink")、[福岡縣](../Page/福岡縣.md "wikilink")、[熊本縣](../Page/熊本縣.md "wikilink")、[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")、[宮崎縣等地](../Page/宮崎縣.md "wikilink")。

## 略史

會長・[盛力健兒在](../Page/盛力健兒.md "wikilink")1941年
5月29日出生。出身於初代山健組組長[山本健一親分的若頭補佐](../Page/山本健一.md "wikilink")。初代山健組當時，山健組的健龍會會長・[渡邊芳則](../Page/渡邊芳則.md "wikilink")（之後的[五代目山口組組長](../Page/五代目山口組.md "wikilink")）、盛力會會長・盛力健兒、[健心會會長](../Page/健心會.md "wikilink")・[杉秀夫等](../Page/杉秀夫.md "wikilink")3人被稱為「山健三羽」，為當時山健組的武鬥派代表。

1978年的「第三次大阪戰爭」，山健組的若頭補佐兼[大阪地區責任者](../Page/大阪.md "wikilink")、盛力會會長及盛力會幹部7人，因涉及多起與[松田組的槍擊事件及暴力謀殺有關](../Page/松田組.md "wikilink")，而遭到[逮捕](../Page/逮捕.md "wikilink")。在1989年6月服役期間的盛力會會長・盛力健兒，被升格為五代目山口組的直參若中。

2009年2月、盛力健兒被山口組本部「除籍」處分，處分原因在去年2008年10月、[後藤組](../Page/後藤組.md "wikilink")・後藤忠政組長的處分引起的內部騷動，當時有14名山口組的直系組長連署簽名「連判狀」，批判山口組執行部的處分作為，而盛力健兒為參予連署批判的直系組長其中一人。

## 最高幹部

  - 會長・[盛力健兒](../Page/盛力健兒.md "wikilink")（本名・平川一茂。六代目山口組若中、洪門五聖山顧問、亞洲武道總會長）
  - 副會長・[飯田倫功](../Page/飯田倫功.md "wikilink")（[八洲連合](../Page/八洲連合.md "wikilink")[飯田組組長](../Page/飯田組.md "wikilink")）
  - 理事長・[青山一雄](../Page/青山一雄.md "wikilink")（[青山總業總裁](../Page/青山總業.md "wikilink")）
  - 本部長・[松元國弘](../Page/松元國弘.md "wikilink")（[松元會會長](../Page/松元會.md "wikilink")）
  - 理事長補佐・[大西弘之](../Page/大西弘之.md "wikilink")（[力弘總業總長](../Page/力弘總業.md "wikilink")）
  - 組織委員長・[足立英明](../Page/足立英明.md "wikilink")（二代目[久富連合會會長](../Page/久富連合會.md "wikilink")）

## 其他組員

  - [末廣勝茂](../Page/末廣勝茂.md "wikilink")（[盛心會會長](../Page/盛心會.md "wikilink")）
  - [植村良朗](../Page/植村良朗.md "wikilink")（二代目[南組組長](../Page/南組.md "wikilink")）

## 其他

  - 八洲連合
      - 飯田組（博多區）
      - 盛心會（志免町）
      - 岸本会（博多區）
      - 石本組（志免町）
      - 二代目岸本総業（博多區）
      - 二代目岸本組（博多區）

[Category:山口組消失的組織](../Category/山口組消失的組織.md "wikilink")
[Category:山健組系統](../Category/山健組系統.md "wikilink")
[Category:盛力會](../Category/盛力會.md "wikilink")