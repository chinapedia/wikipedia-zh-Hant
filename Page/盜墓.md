**盜墓**是指進入[陵墓或](../Page/陵墓.md "wikilink")[地下墓室盜取](../Page/地下墓室.md "wikilink")[陪葬之物或掘出屍體的行為](../Page/陪葬.md "wikilink")。

盜墓往往影響了[歷史學家及](../Page/歷史學家.md "wikilink")[考古學家的研究工作](../Page/考古學家.md "wikilink")，因為盜墓人往往先於專家开掘墓穴，其目的通常只是為了墓穴內的具高價值的精美陪葬品，在盜墓過程中極有可能故意或无意的破壞了墓穴的完整。

盜墓也有可能是為了洩恨报复。[唐代宗](../Page/唐代宗.md "wikilink")[大曆二年](../Page/大曆.md "wikilink")（767年）[郭子儀在靈州](../Page/郭子儀.md "wikilink")（今[寧夏](../Page/寧夏.md "wikilink")[靈武縣](../Page/靈武縣.md "wikilink")）大破[吐蕃](../Page/吐蕃.md "wikilink")。是年十二月，有盗墓贼掘郭子仪之父的坟墓。民間傳聞是[魚朝恩遣人所為](../Page/魚朝恩.md "wikilink")。朝廷擔心郭子儀發兵[謀叛](../Page/叛國.md "wikilink")，事發後郭子儀入朝流涕說：“臣久主兵，不能禁暴，军人残人之墓，固亦多矣。此臣不忠不孝，上获天谴，非人患也。”

盜墓的歷史由來已久，例如根據考古學家的研究發現，[秦始皇陵曾經遭盜墓贼挖掘及破壞](../Page/秦始皇陵.md "wikilink")，[兵馬俑手上的](../Page/兵馬俑.md "wikilink")[武器大多都被拿走](../Page/武器.md "wikilink")，[陵墓亦有給放火燒毀的跡象](../Page/陵墓.md "wikilink")。估計是[項羽所為](../Page/項羽.md "wikilink")，他推倒秦俑士兵，令其身首異處。

又例如直至1968年所發現[漢代墓穴的兩套](../Page/漢代.md "wikilink")[玉衣](../Page/玉衣.md "wikilink")，一直以來人們都只是以為玉衣的存在只是個傳說而已，但現時大部分人都相信歷來有大量盜墓贼盜去了大部分的玉衣。

在[中世紀及](../Page/中世紀.md "wikilink")[文藝復興時期的](../Page/文藝復興.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，有記錄指學習醫術及美術的學生會從[殮房](../Page/殮房.md "wikilink")、民居、墓地等地方盜取的屍體，以學習[人體解剖學](../Page/人體解剖學.md "wikilink")\[1\]。文藝復興時期的著名藝術家[達芬奇和](../Page/達芬奇.md "wikilink")[米開朗基羅都曾經從](../Page/米開朗基羅.md "wikilink")[殯儀館或墓地裏盜取屍體](../Page/殯儀館.md "wikilink")，透過解剖認識人體，令他們作品更臻完美，不過這行為通常被視為[盜屍](../Page/盜屍.md "wikilink")，而非盜墓。

## 以盜墓為題材的大眾文化作品

  - 《[夜盜珍妃墓](../Page/夜盜珍妃墓.md "wikilink")》：1989年電影，敘述[瑾妃墓被盜事件](../Page/瑾妃.md "wikilink")
  - 《[盜墓](../Page/盜墓_\(小說\).md "wikilink")》：[衛斯理科幻小說系列的一部作品](../Page/衛斯理系列.md "wikilink")
  - 《[盜墓者](../Page/盜墓者.md "wikilink")》電子遊戲及電影系列
  - 《[盜墓筆記](../Page/盜墓筆記.md "wikilink")》[南派三叔的一部作品](../Page/南派三叔.md "wikilink")
  - 《[鬼吹灯](../Page/鬼吹灯.md "wikilink")》[张牧野](../Page/张牧野.md "wikilink")

## 相關條目

  - [中國盜墓史](../Page/中國盜墓史.md "wikilink")
  - [盜屍](../Page/盜屍.md "wikilink")
  - [發丘中郎將](../Page/發丘將军.md "wikilink")（看時辰方位）
  - [摸金校尉](../Page/摸金校尉.md "wikilink")（看時辰方位）
  - [卸嶺力士](../Page/卸嶺力士.md "wikilink")（硬開挖）
  - [搬山道人](../Page/搬山道人.md "wikilink")（硬開挖，如民國[孫殿英等人](../Page/孫殿英.md "wikilink")）
  - [十二大盜墓狂人](../Page/十二大盜墓狂人.md "wikilink")（含[劉豫等人](../Page/劉豫.md "wikilink")）
  - [洛陽鏟](../Page/洛陽鏟.md "wikilink")
  - [朱漆臉](../Page/朱漆臉.md "wikilink")（聞名於盜[宋太祖陵墓](../Page/宋太祖.md "wikilink")）

## 資料來源

[Category:考古學](../Category/考古學.md "wikilink")
[Category:藝術史](../Category/藝術史.md "wikilink")
[Category:犯罪](../Category/犯罪.md "wikilink")
[\*](../Category/盜墓.md "wikilink")

1.  [Park, Katharine 1994 'The Criminal and the Saintly Body: Autopsy
    and Dissection in Renaissance Italy' in Renaissance Quarterly,
    Vol. 47, No. 1. (Spring, 1994)
    p.17](http://links.jstor.org/sici?sici=0034-4338%28199421%2947%3A1%3C1%3ATCATSB%3E2.0.CO%3B2-H)