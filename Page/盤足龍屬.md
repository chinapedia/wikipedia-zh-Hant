**盤足龍屬**（[學名](../Page/學名.md "wikilink")：*Euhelopus*）是[蜥腳下目](../Page/蜥腳下目.md "wikilink")[盤足龍科的一](../Page/盤足龍科.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，化石發現於[中國](../Page/中國.md "wikilink")[山東省](../Page/山東省.md "wikilink")。過去被認為生存於[侏羅紀晚期](../Page/侏羅紀.md "wikilink")[啟莫里階](../Page/啟莫里階.md "wikilink")，在2009年，被修正至[白堊紀早期的](../Page/白堊紀.md "wikilink")[巴列姆階](../Page/巴列姆階.md "wikilink")/[阿普第階](../Page/阿普第階.md "wikilink")，約1億3000萬到1億1200萬年前\[1\]。盤足龍是大型的[草食性恐龍](../Page/草食性.md "wikilink")，身長約15公尺，重約15到20噸\[2\]，前肢長於後肢。

## 發現及物種

[Tapuiasaurus_phylogeny.png](https://zh.wikipedia.org/wiki/File:Tapuiasaurus_phylogeny.png "fig:Tapuiasaurus_phylogeny.png")的[演化樹](../Page/演化樹.md "wikilink")，顯示盤足龍屬於[巨龍形類](../Page/巨龍形類.md "wikilink")\]\]
盤足龍是在1929年由[卡爾·維曼](../Page/卡爾·維曼.md "wikilink")（Carl
Wiman）所描述、命名，當時命名為*Helopus*，但因這個學名已被一種[鳥類所有](../Page/鳥類.md "wikilink")，所以在1956年由[阿爾弗雷德·羅默](../Page/阿爾弗雷德·羅默.md "wikilink")（Alfred
Sherwood
Romer）更名為*Euhelopus*。[植物界也有一個相同的屬名](../Page/植物界.md "wikilink")，但不同[界允許相同的屬名](../Page/界_\(生物\).md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")**師氏盤足龍**（*E.
zdanskyi*），目前已經發現一個部分骨骼，包含大部分頸部、[脊柱](../Page/脊柱.md "wikilink")、以及缺少牙齒的頭顱骨\[3\]。[模式標本目前存放在](../Page/模式標本.md "wikilink")[瑞典](../Page/瑞典.md "wikilink")[烏普薩拉](../Page/烏普薩拉.md "wikilink")[奧普塞拉大學的古生物博物館](../Page/奧普塞拉大學.md "wikilink")

## 參考文獻

<div class="references-small">

<references>

</references>

  -

</div>

## 外部連結

  - [恐龍博物館——盤足龍](https://web.archive.org/web/20070804155036/http://www.dinosaur.net.cn/museum/Euhelopus.htm)
  - [盤足龍基本數據](http://www.dinoruss.com/de_4/5a6c227.htm)

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:大鼻龍類](../Category/大鼻龍類.md "wikilink")

1.

2.
3.  "Euhelopus." In: Dodson, Peter & Britt, Brooks & Carpenter, Kenneth
    & Forster, Catherine A. & Gillette, David D. & Norell, Mark A. &
    Olshevsky, George & Parrish, J. Michael & Weishampel, David B. *The
    Age of Dinosaurs*. Publications International, LTD. p. 70. ISBN
    978-0-7853-0443-2.