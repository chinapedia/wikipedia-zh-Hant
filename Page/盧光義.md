**盧光義**（1928年12月1日-2010年12月7日），籍贯[湖南](../Page/湖南.md "wikilink")[益阳](../Page/益阳.md "wikilink")，[中華民國陸軍退役](../Page/中華民國陸軍.md "wikilink")[中將](../Page/中將.md "wikilink")，第十四任[陸軍軍官學校](../Page/陸軍軍官學校.md "wikilink")[校长](../Page/校长.md "wikilink")，為該校第22期毕业。曾任[陸軍砲兵訓練指揮部砲兵學校副校長](../Page/陸軍砲兵訓練指揮部.md "wikilink")、第八軍團（今[陸軍第八軍團指揮部](../Page/陸軍第八軍團指揮部.md "wikilink")）司令、國防部情報局（今[國防部軍事情報局](../Page/國防部軍事情報局.md "wikilink")）局長暨國防部聯合作戰訓練部（今[國防部參謀本部訓練參謀次長室](../Page/國防部參謀本部.md "wikilink")\[1\]）前副主任等職務\[2\]，陸軍中將，官科[砲兵](../Page/砲兵.md "wikilink")，2011年（民國100年）1月獲頒總統[褒揚令](../Page/褒揚令.md "wikilink")，曾獲頒雲麾、忠勤等近三十座勳獎章\[3\]。

## 參考來源

[Category:台灣軍事人物](../Category/台灣軍事人物.md "wikilink")
[Category:益阳人](../Category/益阳人.md "wikilink")
[Category:中華民國陸軍中將](../Category/中華民國陸軍中將.md "wikilink")
[Category:中華民國陸軍軍官學校校長](../Category/中華民國陸軍軍官學校校長.md "wikilink")
[Category:湖南裔台灣人](../Category/湖南裔台灣人.md "wikilink")
[Guang光義](../Category/盧姓.md "wikilink")
[Category:黄埔军校第廿二期](../Category/黄埔军校第廿二期.md "wikilink")

1.
2.
3.