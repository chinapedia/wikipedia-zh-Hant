**盧恩字母**（Runes）又稱為**路尼字母**或**北欧字母**，是一类已灭绝的[字母](../Page/字母.md "wikilink")，在[中世纪的](../Page/中世纪.md "wikilink")[歐洲用来书写某些北欧](../Page/歐洲.md "wikilink")[日耳曼语族的语言](../Page/日耳曼语族.md "wikilink")，特别在[斯堪的納維亞半岛与](../Page/斯堪的納維亞半岛.md "wikilink")[不列颠群岛通用](../Page/不列颠群岛.md "wikilink")。斯堪的纳维亚半岛所用的盧恩文字被稱作**弗薩克文（Futhark）**，不列顛島所用的盧恩文字被稱作**弗托克文（Futhorc）**。

## 来源

卢恩（rune）来自古英语（rūn），义爲“秘密”，实际上是一种咒文。它的来源和[北欧神话中的](../Page/北欧神话.md "wikilink")[奥丁息息相关](../Page/奥丁.md "wikilink")。

传说，奧丁是智慧知識之神，他曾經倒吊在世界之树上九天九夜，《賢者之歌》中記到：

於是，奥丁取得了卢恩文字（Runes）。这是一种咒文，只要将它刻在木、石、金属甚或任何材料上，就能得到无穷的威力。奥丁取得了盧恩文字的奥秘後，由[諾倫三女神](../Page/諾倫三女神.md "wikilink")（Norns）把这种文字记载的命运，刻在黄金宝盾上。

## 歷史

至今发现最早的如尼刻文定期为公元[二世纪左右](../Page/二世纪.md "wikilink")，随着[基督教傳入北欧](../Page/基督教.md "wikilink")，如尼字母逐漸被[拉丁字母取代](../Page/拉丁字母.md "wikilink")。公元[六世纪左右](../Page/六世纪.md "wikilink")，盧恩字母在中欧消失；公元[十一世纪左右也在斯堪的纳维亚半岛消失](../Page/十一世纪.md "wikilink")，但在斯堪的纳维亚，盧恩字母还常常被用在装饰图案上。盧恩符文曾是古老歐洲民族如維京、日耳曼、塞爾特、和盎格魯薩克遜所廣泛使用的字母。盧恩已存在數千年之久，有些古老符文曾出現在世界上的古代洞穴壁畫中。

三種最著名的盧恩字母是：

  - （约150年至800年）

  - （400年至1100年）

  - （年800至1100年）

後弗薩克文進一步被分成：

  - [丹麥語弗薩克字體](../Page/丹麥.md "wikilink")
  - [瑞典語](../Page/瑞典.md "wikilink")–[挪威語盧恩字體](../Page/挪威.md "wikilink")（另：Short-twig或Rök
    Runes）
  - Hälsinge Runes（不屬於任何國家的盧恩文）

後弗薩克文字進一步再發展成：

  - Marcomannic Runes
  - Medieval Runes（1100–1500）
  - [Dalecarlian](../Page/Dalecarlian.md "wikilink") Runes（ca.
    1500–1800s）

## 字母表

### 古弗薩克文

<table>
<thead>
<tr class="header">
<th><p>盧恩字母</p></th>
<th><p><a href="../Page/UCS.md" title="wikilink">UCS</a></p></th>
<th><p><a href="../Page/轉寫.md" title="wikilink">轉寫</a></p></th>
<th><p><a href="../Page/IPA.md" title="wikilink">IPA</a></p></th>
<th><p><a href="../Page/原始日耳曼語.md" title="wikilink">原始日耳曼語名字</a></p></th>
<th><p>含義</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_fehu.svg" title="fig:Runic_letter_fehu.svg">Runic_letter_fehu.svg</a></p></td>
<td></td>
<td><p>f</p></td>
<td></td>
<td><p>*fehu</p></td>
<td><p>"财富，牛"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_uruz.svg" title="fig:Runic_letter_uruz.svg">Runic_letter_uruz.svg</a></p></td>
<td></td>
<td><p>u</p></td>
<td><p>, </p></td>
<td><p>?*ūruz</p></td>
<td><p>"<a href="../Page/原牛.md" title="wikilink">aurochs</a>"（或*ûram "water/slag"?）</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_thurisaz.svg" title="fig:Runic_letter_thurisaz.svg">Runic_letter_thurisaz.svg</a></p></td>
<td></td>
<td><p><a href="../Page/þ.md" title="wikilink">þ</a></p></td>
<td><p>, </p></td>
<td><p>?*þurisaz</p></td>
<td><p>"<a href="../Page/索尔.md" title="wikilink">索尔</a>, <a href="../Page/霜巨人.md" title="wikilink">巨人</a>"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ansuz.svg" title="fig:Runic_letter_ansuz.svg">Runic_letter_ansuz.svg</a></p></td>
<td></td>
<td><p>a</p></td>
<td><p>, </p></td>
<td><p>*ansuz</p></td>
<td><p>"<a href="../Page/阿萨神族.md" title="wikilink">诸神</a>"</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_raido.svg" title="fig:Runic_letter_raido.svg">Runic_letter_raido.svg</a></p></td>
<td></td>
<td><p>r</p></td>
<td></td>
<td><p>*raidō</p></td>
<td><p>"骑乘，旅行"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_kauna.svg" title="fig:Runic_letter_kauna.svg">Runic_letter_kauna.svg</a></p></td>
<td></td>
<td><p>k</p></td>
<td></td>
<td><p>?*kaunan</p></td>
<td><p>"火焰、火炬"（或*Kenaz）</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_gebo.svg" title="fig:Runic_letter_gebo.svg">Runic_letter_gebo.svg</a></p></td>
<td></td>
<td><p>g</p></td>
<td></td>
<td><p>*gebō</p></td>
<td><p>"赠礼"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_wunjo.svg" title="fig:Runic_letter_wunjo.svg">Runic_letter_wunjo.svg</a></p></td>
<td></td>
<td><p>w</p></td>
<td></td>
<td><p>*wunjō</p></td>
<td><p>"喜悦"</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_haglaz.svg" title="fig:Runic_letter_haglaz.svg">Runic_letter_haglaz.svg</a> <a href="https://zh.wikipedia.org/wiki/File:Runic_letter_haglaz_variant.svg" title="fig:Runic_letter_haglaz_variant.svg">Runic_letter_haglaz_variant.svg</a></p></td>
<td></td>
<td><p>h</p></td>
<td></td>
<td><p>*hagalaz</p></td>
<td><p>"<a href="../Page/冰雹.md" title="wikilink">冰雹</a>"（降水）</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_naudiz.svg" title="fig:Runic_letter_naudiz.svg">Runic_letter_naudiz.svg</a></p></td>
<td></td>
<td><p>n</p></td>
<td></td>
<td><p>*naudiz</p></td>
<td><p>"需求"</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_isaz.svg" title="fig:Runic_letter_isaz.svg">Runic_letter_isaz.svg</a></p></td>
<td></td>
<td><p>i</p></td>
<td><p>, </p></td>
<td><p>*īsaz</p></td>
<td><p>"冰"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_jeran.svg" title="fig:Runic_letter_jeran.svg">Runic_letter_jeran.svg</a></p></td>
<td></td>
<td><p>j</p></td>
<td></td>
<td><p>*jēra-</p></td>
<td><p>"丰收，丰年"</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_iwaz.svg" title="fig:Runic_letter_iwaz.svg">Runic_letter_iwaz.svg</a></p></td>
<td></td>
<td><p>ï（或æ）</p></td>
<td><p>, （?）</p></td>
<td><p>*ī(h)waz/*ei(h)waz</p></td>
<td><p>"紫杉树"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_pertho.svg" title="fig:Runic_letter_pertho.svg">Runic_letter_pertho.svg</a></p></td>
<td></td>
<td><p>p</p></td>
<td></td>
<td><p>?*perþ-</p></td>
<td><p>含義不明，可能"pear-tree，签筒，决疑，等待裝滿的杯子"</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_algiz.svg" title="fig:Runic_letter_algiz.svg">Runic_letter_algiz.svg</a></p></td>
<td></td>
<td><p>z</p></td>
<td></td>
<td><p>?*algiz</p></td>
<td><p>含義不明，可能"<a href="../Page/駝鹿.md" title="wikilink">驼鹿</a>"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_sowilo.svg" title="fig:Runic_letter_sowilo.svg">Runic_letter_sowilo.svg</a> <a href="https://zh.wikipedia.org/wiki/File:Runic_letter_sowilo_variant.svg" title="fig:Runic_letter_sowilo_variant.svg">Runic_letter_sowilo_variant.svg</a></p></td>
<td></td>
<td><p>s</p></td>
<td></td>
<td><p>*sōwilō</p></td>
<td><p>"太阳"</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_tiwaz.svg" title="fig:Runic_letter_tiwaz.svg">Runic_letter_tiwaz.svg</a></p></td>
<td></td>
<td><p>t</p></td>
<td></td>
<td><p>*tīwaz/*teiwaz</p></td>
<td><p>"<a href="../Page/提爾.md" title="wikilink">提尔</a>"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_berkanan.svg" title="fig:Runic_letter_berkanan.svg">Runic_letter_berkanan.svg</a></p></td>
<td></td>
<td><p>b</p></td>
<td></td>
<td><p>*berkanan</p></td>
<td><p>"<a href="../Page/桦木属.md" title="wikilink">birch</a>"樺木</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ehwaz.svg" title="fig:Runic_letter_ehwaz.svg">Runic_letter_ehwaz.svg</a></p></td>
<td></td>
<td><p>e</p></td>
<td><p>, </p></td>
<td><p>*ehwaz</p></td>
<td><p>"马"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_mannaz.svg" title="fig:Runic_letter_mannaz.svg">Runic_letter_mannaz.svg</a></p></td>
<td></td>
<td><p>m</p></td>
<td></td>
<td><p>*mannaz</p></td>
<td><p>"人"</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_laukaz.svg" title="fig:Runic_letter_laukaz.svg">Runic_letter_laukaz.svg</a></p></td>
<td></td>
<td><p>l</p></td>
<td></td>
<td><p>*laguz</p></td>
<td><p>"水，湖泊"（或可能*laukaz "leek"）</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ingwaz.svg" title="fig:Runic_letter_ingwaz.svg">Runic_letter_ingwaz.svg</a> <a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ingwaz_variant.svg" title="fig:Runic_letter_ingwaz_variant.svg">Runic_letter_ingwaz_variant.svg</a> <a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ingwaz_variant.png" title="fig:Runic_letter_ingwaz_variant.png">Runic_letter_ingwaz_variant.png</a></p></td>
<td></td>
<td><p><a href="../Page/ŋ.md" title="wikilink">ŋ</a></p></td>
<td></td>
<td><p>*ingwaz</p></td>
<td><p>"天使，Ingwaz神"</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_othalan.svg" title="fig:Runic_letter_othalan.svg">Runic_letter_othalan.svg</a></p></td>
<td></td>
<td><p>o</p></td>
<td><p>, </p></td>
<td><p>*ōþila-/*ōþala-</p></td>
<td><p>"heritage, estate, possession"</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_dagaz.svg" title="fig:Runic_letter_dagaz.svg">Runic_letter_dagaz.svg</a></p></td>
<td></td>
<td><p>d</p></td>
<td></td>
<td><p>*dagaz</p></td>
<td><p>"day"</p></td>
</tr>
</tbody>
</table>

### 盎格魯撒克遜弗托克文

<table>
<thead>
<tr class="header">
<th><p>盧恩字母</p></th>
<th><p><a href="../Page/UCS.md" title="wikilink">UCS</a></p></th>
<th><p><a href="../Page/古英語.md" title="wikilink">古英語名字</a></p></th>
<th><p>含義</p></th>
<th><p><a href="../Page/轉寫.md" title="wikilink">轉寫</a></p></th>
<th><p><a href="../Page/IPA.md" title="wikilink">IPA</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_fehu.svg" title="fig:Runic_letter_fehu.svg">Runic_letter_fehu.svg</a></p></td>
<td></td>
<td><p>feoh</p></td>
<td><p>财富</p></td>
<td><p>f</p></td>
<td><p>[f], [v]</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_uruz.svg" title="fig:Runic_letter_uruz.svg">Runic_letter_uruz.svg</a></p></td>
<td></td>
<td><p>ur</p></td>
<td><p><a href="../Page/原牛.md" title="wikilink">原牛</a></p></td>
<td><p>u</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_thurisaz.svg" title="fig:Runic_letter_thurisaz.svg">Runic_letter_thurisaz.svg</a></p></td>
<td></td>
<td><p>þorn</p></td>
<td><p>刺</p></td>
<td><p>þ, ð</p></td>
<td><p>[θ], [ð]</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_os.svg" title="fig:Runic_letter_os.svg">Runic_letter_os.svg</a></p></td>
<td></td>
<td><p>ós</p></td>
<td><p>[一个]神</p></td>
<td><p>ó</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_raido.svg" title="fig:Runic_letter_raido.svg">Runic_letter_raido.svg</a></p></td>
<td></td>
<td><p>rad</p></td>
<td><p>骑乘</p></td>
<td><p>r</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_cen.svg" title="fig:Runic_letter_cen.svg">Runic_letter_cen.svg</a></p></td>
<td></td>
<td><p>cen</p></td>
<td><p>火炬</p></td>
<td><p>c</p></td>
<td><p>[k]</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_gebo.svg" title="fig:Runic_letter_gebo.svg">Runic_letter_gebo.svg</a></p></td>
<td></td>
<td><p>gyfu</p></td>
<td><p>礼物</p></td>
<td></td>
<td><p>[g], [j]</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_wunjo.svg" title="fig:Runic_letter_wunjo.svg">Runic_letter_wunjo.svg</a></p></td>
<td></td>
<td><p>wynn</p></td>
<td><p>欢乐</p></td>
<td><p>w, </p></td>
<td><p>[w]</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_haglaz_variant.svg" title="fig:Runic_letter_haglaz_variant.svg">Runic_letter_haglaz_variant.svg</a></p></td>
<td></td>
<td><p>hægl</p></td>
<td><p>hail (降水)</p></td>
<td><p>h</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_naudiz.svg" title="fig:Runic_letter_naudiz.svg">Runic_letter_naudiz.svg</a></p></td>
<td></td>
<td><p>nyd</p></td>
<td><p>需要，遇险</p></td>
<td><p>n</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_isaz.svg" title="fig:Runic_letter_isaz.svg">Runic_letter_isaz.svg</a></p></td>
<td></td>
<td><p>is</p></td>
<td><p>冰</p></td>
<td><p>i</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ger.svg" title="fig:Runic_letter_ger.svg">Runic_letter_ger.svg</a></p></td>
<td></td>
<td><p>ger</p></td>
<td><p>年，收获</p></td>
<td><p>j</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_iwaz.svg" title="fig:Runic_letter_iwaz.svg">Runic_letter_iwaz.svg</a></p></td>
<td></td>
<td><p>eoh</p></td>
<td><p>紫杉</p></td>
<td><p>eo</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_pertho.svg" title="fig:Runic_letter_pertho.svg">Runic_letter_pertho.svg</a></p></td>
<td></td>
<td><p>peorð</p></td>
<td><p>(未知)</p></td>
<td><p>p</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_algiz.svg" title="fig:Runic_letter_algiz.svg">Runic_letter_algiz.svg</a></p></td>
<td></td>
<td><p>eolh</p></td>
<td><p>麋鹿莎草</p></td>
<td><p>x</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_sigel.svg" title="fig:Runic_letter_sigel.svg">Runic_letter_sigel.svg</a></p></td>
<td></td>
<td><p>sigel</p></td>
<td><p>太阳</p></td>
<td><p>s</p></td>
<td><p>[s], [z]</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_tiwaz.svg" title="fig:Runic_letter_tiwaz.svg">Runic_letter_tiwaz.svg</a></p></td>
<td></td>
<td><p>Tiw</p></td>
<td><p>上帝Tiw</p></td>
<td><p>t</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_berkanan.svg" title="fig:Runic_letter_berkanan.svg">Runic_letter_berkanan.svg</a></p></td>
<td></td>
<td><p>beorc</p></td>
<td><p>桦木</p></td>
<td><p>b</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ehwaz.svg" title="fig:Runic_letter_ehwaz.svg">Runic_letter_ehwaz.svg</a></p></td>
<td></td>
<td><p>eh</p></td>
<td><p>马</p></td>
<td><p>e</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_mannaz.svg" title="fig:Runic_letter_mannaz.svg">Runic_letter_mannaz.svg</a></p></td>
<td></td>
<td><p>mann</p></td>
<td><p>人</p></td>
<td><p>m</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_laukaz.svg" title="fig:Runic_letter_laukaz.svg">Runic_letter_laukaz.svg</a></p></td>
<td></td>
<td><p>lagu</p></td>
<td><p>湖</p></td>
<td><p>l</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ingwaz_variant.svg" title="fig:Runic_letter_ingwaz_variant.svg">Runic_letter_ingwaz_variant.svg</a></p></td>
<td></td>
<td><p>ing</p></td>
<td><p>Ing (一名英雄)</p></td>
<td><p>ŋ</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_othalan.svg" title="fig:Runic_letter_othalan.svg">Runic_letter_othalan.svg</a></p></td>
<td></td>
<td><p>éðel</p></td>
<td><p>房地产</p></td>
<td><p>œ</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_dagaz.svg" title="fig:Runic_letter_dagaz.svg">Runic_letter_dagaz.svg</a></p></td>
<td></td>
<td><p>dæg</p></td>
<td><p>一天</p></td>
<td><p>d</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ac.svg" title="fig:Runic_letter_ac.svg">Runic_letter_ac.svg</a></p></td>
<td></td>
<td><p>ac</p></td>
<td><p>橡树</p></td>
<td><p>a</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ansuz.svg" title="fig:Runic_letter_ansuz.svg">Runic_letter_ansuz.svg</a></p></td>
<td></td>
<td><p><a href="../Page/æ.md" title="wikilink">æsc</a></p></td>
<td><p>梣树</p></td>
<td><p>æ</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_yr.svg" title="fig:Runic_letter_yr.svg">Runic_letter_yr.svg</a></p></td>
<td></td>
<td><p>yr</p></td>
<td><p>弓</p></td>
<td><p>y</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ior.svg" title="fig:Runic_letter_ior.svg">Runic_letter_ior.svg</a></p></td>
<td></td>
<td><p>ior</p></td>
<td><p>鳗鱼</p></td>
<td><p>ia, io</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Runic_letter_ear.svg" title="fig:Runic_letter_ear.svg">Runic_letter_ear.svg</a></p></td>
<td></td>
<td><p>ear</p></td>
<td><p>坟墓</p></td>
<td><p>ea</p></td>
<td></td>
</tr>
</tbody>
</table>

## 日常用途

盧恩文字在日常生活的應用如同文字記載，也被當時人們運用於飾品及建築。另外，古代歐洲僧侶曾將其視為與神明溝通之工具。 盧恩文字會被用在巫術中。

## 盧恩碑文

| 地區                                 | 盧恩碑文的數目     |
| ---------------------------------- | ----------- |
| [瑞典](../Page/瑞典.md "wikilink")     | 3432        |
| [挪威](../Page/挪威.md "wikilink")     | 1552        |
| [丹麥](../Page/丹麥.md "wikilink")     | 844         |
| **斯堪地那維亞總計**                       | **5826**    |
| 除斯堪地那維亞和弗里西亞外的歐洲大陸                 | 80          |
| 弗里西亞                               | 20          |
| 除愛爾蘭外的不列顛群島                        | \> 200      |
| [格陵蘭](../Page/格陵蘭.md "wikilink")   | \> 100      |
| [冰島](../Page/冰島.md "wikilink")     | \< 100      |
| [愛爾蘭島](../Page/愛爾蘭島.md "wikilink") | 16          |
| [法羅群島](../Page/法羅群島.md "wikilink") | 9           |
| **非斯堪地那維亞總計**                      | **\> 500**  |
| **總計**                             | **\> 6400** |

## 當代應用

### 商標

[蓝牙的标志是盧恩字母](../Page/蓝牙.md "wikilink")
[Runic_letter_ior.svg](https://zh.wikipedia.org/wiki/File:Runic_letter_ior.svg "fig:Runic_letter_ior.svg")（，ᚼ）和
[Runic_letter_berkanan.svg](https://zh.wikipedia.org/wiki/File:Runic_letter_berkanan.svg "fig:Runic_letter_berkanan.svg")（，ᛒ）的组合，也就是的首字母HB的合写。

### 占卜

以古早傳下來的，將刻上了盧恩字母的石頭、樹、金屬、玻璃等物件，全部擲出之後挑出一個並且唸咒的方式。不過，現今一般的作法，改以將全部的盧恩字母放在袋中，一邊默想問題，一邊抓出一個盧恩字母來作為回答。另外，近來也常出現抽取印製在紙上的字母卡這種方式。

### 大眾文化

在游戏《[以撒的结合](../Page/以撒的结合.md "wikilink")》中，有八个卢恩字母被引用为符文。

## 盧恩字母編碼

支援盧恩文範圍的Unicode字型包括[免費的](../Page/免費Unicode字型.md "wikilink")[Junicode](../Page/Junicode.md "wikilink")、[Free
Mono](../Page/免費UCS大綱字型.md "wikilink")、[Code2000](../Page/Code2000.md "wikilink")、[Caslon
Roman](../Page/Caslon_Roman.md "wikilink")，以及收費的[Everson
Mono](../Page/Everson_Mono.md "wikilink")、[TITUS Cyberbit
Basic等](../Page/TITUS_Cyberbit_Basic.md "wikilink")。

## 相似字母

  - [古匈牙利字母](../Page/古匈牙利字母.md "wikilink")

## 参考文獻

  - Erik Brate: *Sveriges runinskrifter*, 1922（[online
    text](http://www.runor.se/) in
    [Swedish](../Page/Swedish_language.md "wikilink")）
  - R.I. Page. *An Introduction to English Runes*. The Boydell Press,
    Woodbridge, 1999. ISBN 0-85115-768-8.
  - Orrin W. Robinson. *Old English and its Closest Relatives: A Survey
    of the Earliest Germanic Languages* Stanford University Press, 1992.
    ISBN 0-8047-1454-1
  - Thomas Karlsson. *Uthark - Nightside of the runes*, 2002.
    [(Amazon.co.uk)](http://www.amazon.co.uk/exec/obidos/ASIN/9197410217/qid=1122336489/sr=8-1/ref=sr_8_xs_ap_i1_xgl/026-3308096-6778846)

## 外部連結

  - [History, development and systems of
    runes](http://www.arild-hauge.com/eindex.htm)
  - [Mystery Of The Futhark
    Alphabet](http://www.antalyaonline.net/futhark/)
  - [the
    Futhark](http://archive.wikiwix.com/cache/20110223144839/http://ancientscripts.com/futhark.html)（ancientscripts.com）
  - [Omniglot Rune Page](http://www.omniglot.com/writing/runic.htm)
  - [The Development of Old Germanic
    Alphabets](http://titus.uni-frankfurt.de/didact/idg/germ/runealph.htm)（titus.uni-frankfurt.de,
    with an image of the [Negau
    helmet](../Page/Negau_helmet.md "wikilink")）
  - Encoding
      - [Unicode Code Chart
        (PDF)](http://www.unicode.org/charts/PDF/U16A0.pdf)
  - Esoteric Rune Study
      - [Rune Gild](http://www.runegild.org/)
      - [Rune School](http://www.runeschool.org)
      - [Rune-Net](http://www.mackaos.com.au/Rune-Net)
      - [Runewebvitki](http://www.runewebvitki.com/)

[Category:字母系统](../Category/字母系统.md "wikilink")
[Category:欧洲文字](../Category/欧洲文字.md "wikilink")