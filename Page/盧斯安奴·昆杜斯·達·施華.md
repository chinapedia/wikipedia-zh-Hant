**盧斯奧**全名為**盧斯安奴·昆杜斯·達·施華**（**Luciano Quadros da
Silva**，），[巴西](../Page/巴西.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。擔任[門將](../Page/門將.md "wikilink")。現正效力[香港甲組足球聯賽球隊](../Page/香港甲組足球聯賽.md "wikilink")[傑志](../Page/傑志.md "wikilink")。曾在2007年/2008年[香港足球高級銀牌賽獲得最佳防守球員](../Page/香港足球高級銀牌賽.md "wikilink")。
在香港效力一年後，盧斯奧選擇離隊[東方加盟其他球會](../Page/東方足球隊.md "wikilink")。

其後四海空降甲組，四海的老闆給盧斯奧優厚的條件，加盟[四海](../Page/四海.md "wikilink")。

1月期間，盧斯奧再轉會至[傑志](../Page/傑志.md "wikilink")，穿上33號球衣。

2009年－2010年球季，傑志宣佈已從西班牙羅致了5至6名的外援，現有的外援全部不獲留用。

[Category:1974年出生](../Category/1974年出生.md "wikilink")
[Category:東方球員](../Category/東方球員.md "wikilink")
[Category:四海球員](../Category/四海球員.md "wikilink")
[Category:傑志球員](../Category/傑志球員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:聖卡坦奴球員](../Category/聖卡坦奴球員.md "wikilink")
[Category:阿雷格里港人](../Category/阿雷格里港人.md "wikilink")
[Category:香港巴西籍足球運動員](../Category/香港巴西籍足球運動員.md "wikilink")