**盧維思**（**Michael
Rowse**，），[英國裔](../Page/英國.md "wikilink")[香港人](../Page/香港人.md "wikilink")，[退休](../Page/退休.md "wikilink")[香港公務員](../Page/香港公務員.md "wikilink")，前[投資推廣署署長](../Page/投資推廣署署長.md "wikilink")。

## 簡歷

盧維思於1972年從[英國來到香港](../Page/英國.md "wikilink")，早期擔任[英文](../Page/英文.md "wikilink")[教師及](../Page/教師.md "wikilink")[記者](../Page/記者.md "wikilink")，其後在[廉政公署任職](../Page/廉政公署.md "wikilink")。

1980年，盧維思成為[政務主任](../Page/政務主任.md "wikilink")，其後調任多個[香港政府部門](../Page/香港政府部門.md "wikilink")。1989年，盧維思出任[資訊統籌處副處長](../Page/資訊統籌處.md "wikilink")。1990年，盧維思成為首席助理[庫務司](../Page/庫務司.md "wikilink")，其後升任副庫務司。

1997年，盧維思出任[工商服務業推廣署署長](../Page/工商服務業推廣署署長.md "wikilink")，於1999年開始兼任[旅遊事務專員](../Page/旅遊事務專員.md "wikilink")。1999年初期，香港政府與[美國](../Page/美國.md "wikilink")[迪士尼公司商討興建](../Page/迪士尼公司.md "wikilink")[香港迪士尼樂園](../Page/香港迪士尼樂園.md "wikilink")，盧維思當時以旅遊事務專員的身份作為香港政府的談判代表，為他帶來「米老鼠」的綽號。2000年7月，成為首任投資推廣署署長。2001年10月，盧維思宣佈放棄[英國國籍](../Page/英國國籍.md "wikilink")，取得[香港永久性居民身份證](../Page/香港身份證.md "wikilink")、[香港特別行政區護照](../Page/香港特別行政區護照.md "wikilink")、[回鄉證及](../Page/回鄉證.md "wikilink")[歸化中國公民證書](../Page/歸化.md "wikilink")，成為首位入籍[中国籍](../Page/中国籍.md "wikilink")（[香港永久居民](../Page/香港永久居民.md "wikilink")）的外籍公務員。

2008年，盧維思退休。

## 維港巨星匯事件

2003年，[香港政府為振興](../Page/香港政府.md "wikilink")[嚴重急性呼吸系統綜合症疫症過後的疲弱](../Page/嚴重急性呼吸系統綜合症.md "wikilink")[香港經濟](../Page/香港經濟.md "wikilink")，成立[重建經濟活力小組](../Page/重建經濟活力小組.md "wikilink")，撥款10億港元舉辦一連串活動。小組主席是時任[財政司司長](../Page/財政司司長.md "wikilink")[唐英年](../Page/唐英年.md "wikilink")，委任主辦機構[美國總商會建議舉辦耗費逾](../Page/香港美國商會.md "wikilink")1億港元的大型表演活動[維港巨星匯](../Page/維港巨星匯.md "wikilink")，由[投資推廣署負責協辦](../Page/投資推廣署.md "wikilink")，可惜門票銷售情況不佳，投資推廣署被批評監管不力。

2004年9月，[公務員事務局決定向](../Page/公務員事務局.md "wikilink")[投資推廣署署長盧維思展開紀律聆訊](../Page/投資推廣署署長.md "wikilink")。2005年10月，盧維思被裁定失職，被罰一個月約16萬港元的薪酬，他不服及上訴。2006年1月，盧維思的上訴被香港政府駁回。2007年4月17日，盧維思決定就紀律聆訊結果申請[司法覆核](../Page/司法覆核.md "wikilink")。2008年2月25日至7月4日的審訊其間，盧維思揭發唐英年曾經在內部會議中認為事件中無人失職，惟盧維思面臨紀律聆訊前，卻要求從內部會議紀錄中刪除自己有關的言論，被[法官夏正民形容為](../Page/法官.md "wikilink")「玩政治花招」。盧維思隨後在司法覆核中勝訴。有[立法會議員認為](../Page/立法會議員.md "wikilink")，事件涉誠信問題，要求唐英年作交代。\[1\]\[2\]\[3\]\[4\]\[5\]\[6\]\[7\]\[8\]\[9\]\[10\]他其後於2009年11月3日出版《No
Minister &
No，Minister》，講述維港匯事件內幕，指責年時官員卸責，當中包括唐英年等，該出版行為其後被香港政府指控盧維思被未獲得審批，屬於違規行為。\[11\]

## 家庭

盧維思有兩段婚姻，與現任妻子[黃麗君育有兩名子女](../Page/黃麗君.md "wikilink")，其妻現職公關公司主管。

## 榮譽

  - [太平紳士](../Page/太平紳士.md "wikilink")

## 著作

  - 《[問責不問責：巨星滙的真相](../Page/問責不問責：巨星滙的真相.md "wikilink")》(*No Minister &
    No, Minister: The True Story of Harbour Fest*)

## 註腳

## 資料來源

  - [盧維思擬與政府打官司](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070127&sec_id=4104&subsec_id=12731&art_id=6757594)
    蘋果日報，2007年1月27日
  - [盧維思茶敘送禮
    答謝傳媒支持](http://www.mpfinance.com/htm/Finance/20061209/News/ek1_ek1a1.htm)
    明報，2006年12月9日
  - [盧維思：首位入籍中國的港府“洋高官”](http://news.eastday.com/epublish/big5/paper148/20011015/class014800005/hwz512052.htm)
    eastday.com
  - [香港投資推廣署署長盧維思：“我相信香港始終會做到最好”](http://big5.cctv.com/special/613/3/35695.html)
    CCTV

[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:在香港的英國人](../Category/在香港的英國人.md "wikilink")
[Category:歸化中華人民共和國之香港居民](../Category/歸化中華人民共和國之香港居民.md "wikilink")
[Category:放棄英國國籍人士](../Category/放棄英國國籍人士.md "wikilink")

1.  [盧維思被罰司法覆核勝訴](http://www.mpinews.com/htm/INews/20080704/gb51622a.htm)
     - 明報 2008年7月4日, 2008年8月23日 索取
2.  [盧維思：裁決還他清白](http://www.mpinews.com/htm/INews/20080704/gb51705a.htm)
     - 明報 2008年7月4日, 2008年8月23日 索取
3.  [盧維思勝訴
    洗脫「失職」法官：巨星匯紀律聆訊過程不合法](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20080705&sec_id=4104&subsec_id=12731&art_id=11313663)
     - 蘋果日報 2008年7月5日, 2008年8月25日 索取
4.  [盧維思上訴得直成功推翻被指失職裁決](http://www.rthk.org.hk/rthk/news/expressnews/20080704/news_20080704_55_503320.htm)
    - 香港電台 2008年7月4日, 2008年8月25日 索取
5.  [盧維思獲判勝訴
    李卓人要求檢討機制](http://www.rthk.org.hk/rthk/news/expressnews/20080704/news_20080704_55_503326.htm)
    - 香港電台 2008年7月4日, 2008年8月25日 索取
6.  [盧維思形容今次勝訴是香港法治的勝利](http://www.rthk.org.hk/rthk/news/expressnews/20080704/news_20080704_55_503328.htm)
    - 香港電台 2008年7月4日, 2008年8月25日 索取
7.  [法庭：政府對盧維思的研訊沒有釐定舉證標準](http://www.rthk.org.hk/rthk/news/expressnews/20080704/news_20080704_55_503333.htm)
    - 香港電台 2008年7月4日, 2008年8月25日 索取
8.  [政府不評論盧維思勝訴](http://www.rthk.org.hk/rthk/news/expressnews/20080704/news_20080704_55_503356.htm)
    - 香港電台 2008年7月4日, 2008年8月25日 索取
9.  [盧維思成功翻案脫污名](http://www.singtao.com/index_archive.asp?d_str=20080705&htmlpage=main&news=0705ao04.html)
    - 星島日報 2008年7月5日, 2008年8月25日 索取
10. 盧維思稱無失職討回清白- 東方日報 2008年7月5日, 2008年8月25日 索取
11. [政府指盧維思出書違規 屬「退休後工作」
    未獲審批可罰長俸](http://news.mingpao.com/20091103/gba1.htm)