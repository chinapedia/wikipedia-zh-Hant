[Moggallana̞-statue.jpg](https://zh.wikipedia.org/wiki/File:Moggallana̞-statue.jpg "fig:Moggallana̞-statue.jpg")
**目犍連**（；）也稱**大目犍連**、**沒特伽羅**、**目犍蓮**，簡稱為**目連**、**目蓮**。[釋迦牟尼佛的](../Page/釋迦牟尼佛.md "wikilink")[十大弟子之一](../Page/十大弟子.md "wikilink")，[佛教](../Page/佛教.md "wikilink")、[道教及](../Page/道教.md "wikilink")[民間傳說的創作故事](../Page/民間傳說.md "wikilink")《[目連救母](../Page/目連救母.md "wikilink")》中的「目連」正是指此人。普遍認為[說一切有部所傳](../Page/說一切有部.md "wikilink")《[阿毘達磨法蘊足論](../Page/阿毘達磨法蘊足論.md "wikilink")》是其作品。

## 生平

目犍連與[舍利弗兩人是好友](../Page/舍利弗.md "wikilink")，皈依[釋迦牟尼佛之前](../Page/釋迦牟尼佛.md "wikilink")，是[六師外道中](../Page/六師外道.md "wikilink")[散若夷的弟子](../Page/刪闍夜毗羅胝子.md "wikilink")。後來二人一起改投釋迦僧團，皆成為[釋迦佛身邊的](../Page/釋迦佛.md "wikilink")[十大弟子之一](../Page/十大弟子.md "wikilink")，目犍連並以[神通第一著稱](../Page/神通.md "wikilink")。在[藏傳佛教](../Page/藏傳佛教.md "wikilink")，目犍連與舍利弗往往會被雕塑在釋迦牟尼佛身邊，隨侍[釋迦佛一同接受供養](../Page/釋迦佛.md "wikilink")、膜拜，而[漢傳佛教中](../Page/漢傳佛教.md "wikilink")，釋迦佛身邊則雕塑[阿難與](../Page/阿難.md "wikilink")[摩訶迦葉較多](../Page/摩訶迦葉.md "wikilink")。

目犍連雖然有「神通第一」之名，可是卻在一次弘法經行中，死於[裸形外道](../Page/耆那教.md "wikilink")（亦有說為執杖[梵志](../Page/婆羅門.md "wikilink")）的[暗殺](../Page/暗殺.md "wikilink")。[佛教一般認為](../Page/佛教.md "wikilink")，他是無意抵抗[業報](../Page/業報.md "wikilink")，而非不知曉暗殺行動將會發生，因为他曾經两次逃脱谋杀，佛教常以此教導“定[業不可消](../Page/業_\(佛教\).md "wikilink")”之理。
[Moggallana_-_paranibbana.jpg](https://zh.wikipedia.org/wiki/File:Moggallana_-_paranibbana.jpg "fig:Moggallana_-_paranibbana.jpg")

在《[妙法蓮華經](../Page/妙法蓮華經.md "wikilink")》中，目犍連被釋迦佛授記將來無量[劫後會成佛](../Page/劫.md "wikilink")，佛號是**多摩羅跋栴檀香如來**。\[1\]

## 民間傳說

[Moggallana_saves_his_mother.jpg](https://zh.wikipedia.org/wiki/File:Moggallana_saves_his_mother.jpg "fig:Moggallana_saves_his_mother.jpg")

因為[目連救母](../Page/目連救母.md "wikilink")，故目連尊者是[盂蘭盆節中民間奉祀的對象](../Page/盂蘭盆節.md "wikilink")，另外[地藏菩薩也有入](../Page/地藏菩薩.md "wikilink")[地獄救母的傳說](../Page/地獄.md "wikilink")，與[目連救母的故事類似](../Page/目連救母.md "wikilink")，故有些人常會將[目連與](../Page/目連.md "wikilink")[地藏混淆](../Page/地藏.md "wikilink")，但其實民間信仰認為，目連輔佐[地藏王菩薩](../Page/地藏王菩薩.md "wikilink")，立志普救[枉死城眾生](../Page/枉死城.md "wikilink")\[2\]，故各大[地藏庵時常以目連尊者配祀](../Page/地藏庵.md "wikilink")，如[新莊地藏庵](../Page/新莊地藏庵.md "wikilink")。在[地府中有目連廳](../Page/地府.md "wikilink")，即目連治事之處，中有四大[判官](../Page/判官.md "wikilink")：查察司判官[李玄邃](../Page/李玄邃.md "wikilink")、賞善司判官[楊玄感](../Page/楊玄感.md "wikilink")、罰惡司判官[韓子通](../Page/韓子通.md "wikilink")、陰律司判官[崔子玉等四大判官複審](../Page/崔子玉.md "wikilink")，判斷死者在死後增了多少[冥福](../Page/冥福.md "wikilink")。

有廟宇功德堂共同奉有[地藏菩薩與](../Page/地藏菩薩.md "wikilink")[目連尊者](../Page/目連尊者.md "wikilink")，二尊同為出家相、持錫杖、托缽、持明珠等；或戴五佛冠等，區分為[地藏菩薩座騎為](../Page/地藏菩薩.md "wikilink")[諦聽](../Page/諦聽.md "wikilink")，[目連尊者為騎](../Page/目連尊者.md "wikilink")[象](../Page/象.md "wikilink")、[麒麟](../Page/麒麟.md "wikilink")、[獅子甚至](../Page/獅子.md "wikilink")[龍等](../Page/龍.md "wikilink")。

### 作旬

[中國南方傳說](../Page/中國南方.md "wikilink")，人死後每七日須做法事，是為「[作七](../Page/作七.md "wikilink")」，連作七次，由[頭七到](../Page/頭七.md "wikilink")[滿七共](../Page/滿七.md "wikilink")「七七四十九日」。「滿七」之後，每一旬（十日）舉行法事一次，稱「作旬」，直到人死的第一百日。[閩南人稱此為](../Page/閩南人.md "wikilink")「過王官」，「王」為閻王，「官」則[目連尊者部下的查察司判官](../Page/目連尊者.md "wikilink")[李玄邃](../Page/李玄邃.md "wikilink")、賞善司判官[楊玄感](../Page/楊玄感.md "wikilink")、罰惡司判官[韓子通](../Page/韓子通.md "wikilink")、陰律司判官[崔子玉等](../Page/崔子玉.md "wikilink")「目連廳四大[判官](../Page/判官.md "wikilink")」複審。

如果沒有立過大功或犯過大錯的人（有大功會立即升達[天堂](../Page/天堂.md "wikilink")，有大錯會即刻貶入[十八層地獄](../Page/十八層地獄.md "wikilink")），死亡四十九日之內，靈魂會經過[十殿閻君的一到七殿審查](../Page/十殿閻君.md "wikilink")，每七日抵達一殿。四十九日後，要由「目連廳四大[判官](../Page/判官.md "wikilink")」複審，最後由目連尊者判斷死者獲得了多少[冥福](../Page/冥福.md "wikilink")，故富貴[門閥多在](../Page/門閥.md "wikilink")「**尾七**」（「第四十九日」）之後，每一旬（十日）舉行法事一次，稱「作旬」，以祈禱於四判官與目連尊者。第五十九日為「初旬」，由陰律司崔判官複審；第六十九日為「二旬」，由查察司李判官複審；第七十九日為「三旬」，由賞善司楊判官複審；第八十九日為「四旬」，由罰惡司韓判官複審；第九十九日為「五旬」，由目連尊者親自複審，但「五旬」通常會與「百日」合併辦理。「百日」、「一年」、「三年」，過完最後的閻君三殿，然後發往[轉世](../Page/轉世.md "wikilink")。

[清治時期起](../Page/清治臺灣.md "wikilink")，[臺灣人通常只作法事至](../Page/臺灣人.md "wikilink")「尾七」即止，而不「作旬」，直接作「百日」，故[臺灣人則直接稱](../Page/臺灣人.md "wikilink")「作七」為「**作旬**」。

### 圖表

| 名號                                               | 祭祀時機         | 死亡日數                                         |
| ------------------------------------------------ | ------------ | -------------------------------------------- |
| 一殿[秦廣王蔣王](../Page/秦廣王.md "wikilink")             | 頭七           | 7                                            |
| 二殿[楚江王歷王](../Page/楚江王.md "wikilink")（或說姓厲，又稱初江王） | 二七           | 14                                           |
| 三殿[宋帝王余王](../Page/宋帝王.md "wikilink")             | 三七           | 21                                           |
| 四殿[五官王呂王](../Page/五官王.md "wikilink")（又稱伍官王、仵官王）  | 四七           | 28                                           |
| 五殿[森羅王包王](../Page/森羅王.md "wikilink")（又稱閻羅王、閻羅天子） | 五七           | 35                                           |
| 六殿[卞城王畢王](../Page/卞城王.md "wikilink")（又稱變成王）      | 六七           | 42                                           |
| 七殿[泰山王董王](../Page/泰山王.md "wikilink")（又稱太山王）      | 七七           | 49                                           |
| 目連廳陰律司判官[崔子玉](../Page/崔子玉.md "wikilink")         | 初旬           | 59                                           |
| 目連廳查察司判官[李玄邃](../Page/李玄邃.md "wikilink")         | 二旬           | 69                                           |
| 目連廳賞善司判官[楊玄感](../Page/楊玄感.md "wikilink")         | 三旬           | 79                                           |
| 目連廳罰惡司判官[韓子通](../Page/韓子通.md "wikilink")         | 四旬           | 89                                           |
| 目連尊者                                             | 五旬（可與百日合併辦理） | 99                                           |
| 八殿[都市王黃王](../Page/都市王.md "wikilink")             | 百日（卒哭）       | 100                                          |
| 九殿[平等王陸王](../Page/平等王.md "wikilink")（又稱平正王）      | 小祥（一年）       | 一年                                           |
| 十殿[轉輪王薛王](../Page/轉輪王.md "wikilink")（又稱輪轉王）      | 大祥（三年）       | [三年之喪](../Page/三年之喪.md "wikilink")，超過兩年，即為三年 |
|                                                  |              |                                              |

## 参考文献

## 外部链接

  - [目连救母 寒衣节
    思念先祖的日子](https://globalbuddhism.com/BuddhismWisdom/BuddhismStory/12189.html)
  - [十大弟子傳](https://web.archive.org/web/20101204133935/http://www.book853.com/show.aspx?id=270&cid=43)
    星雲大師著

## 参见

  - [十大弟子](../Page/十大弟子.md "wikilink")

  -
  - [目連救母](../Page/目連救母.md "wikilink")

  - [舍利弗](../Page/舍利弗.md "wikilink")

[Category:阿羅漢](../Category/阿羅漢.md "wikilink")
[M](../Category/佛陀弟子.md "wikilink")

1.  《妙法蓮華經》卷3〈6
    授記品〉：「爾時世尊復告大眾：『我今語汝，是大目犍連，當以種種供具供養八千諸佛，恭敬尊重。諸佛滅後，各起塔廟，高千由旬，縱廣正等五百由旬，皆以金、銀、琉璃、車璩、馬瑙、真珠、玫瑰、七寶合成，眾華、瓔珞、塗香、末香、燒香、繒蓋、幢幡，以用供養。過是已後，當復供養二百萬億諸佛，亦復如是。當得成佛，號曰多摩羅跋栴檀香如來、應供、正遍知、明行足、善逝、世間解、無上士、調御丈夫、天人師、佛、世尊。劫名喜滿，國名意樂。其土平正，頗梨為地，寶樹莊嚴，散真珠華，周遍清淨，見者歡喜。多諸天、人、菩薩、聲聞，其數無量。佛壽二十四小劫，正法住世四十小劫，像法亦住四十小劫。』」
2.  [鬼月的來源：目蓮救母](http://www.kidsnews.com.tw/thread-92716-1-1.html)