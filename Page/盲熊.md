**盲熊**（）是[日本將棋的棋子之一](../Page/日本將棋.md "wikilink")。只在[摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")、[泰將棋](../Page/泰將棋.md "wikilink")、[大局將棋出現](../Page/大局將棋.md "wikilink")。

## 盲熊在摩訶大大將棋與泰將棋

可升級為[奔熊](../Page/奔熊.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>盲熊</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p><font color="white">■</font></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p><font color="white">■</font></p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>熊</strong></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p>｜</p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p>｜</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
<td><p>可循後方自由行走，或循斜向走一格。不得越過其他棋子。</p></td>
<td><p>奔熊</p></td>
</tr>
</tbody>
</table>

## 盲熊在大局將棋

可升級為[飛鹿](../Page/飛鹿.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>盲熊</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p><font color="white">■</font></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p><font color="white">■</font></p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>○</p></td>
<td><p><strong>熊</strong></p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
<td><p>可循橫向、斜向走一格。</p></td>
<td><p>飛鹿</p></td>
</tr>
</tbody>
</table>

## 參見

  - [日本將棋](../Page/日本將棋.md "wikilink")
  - [日本將棋變體](../Page/日本將棋變體.md "wikilink")
  - [摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")
  - [泰將棋](../Page/泰將棋.md "wikilink")
  - [大局將棋](../Page/大局將棋.md "wikilink")

[Category:日本將棋棋子](../Category/日本將棋棋子.md "wikilink")
[Category:摩訶大大將棋棋子](../Category/摩訶大大將棋棋子.md "wikilink")
[Category:泰將棋棋子](../Category/泰將棋棋子.md "wikilink")
[Category:大局將棋棋子](../Category/大局將棋棋子.md "wikilink")