[Lineale.jpg](https://zh.wikipedia.org/wiki/File:Lineale.jpg "fig:Lineale.jpg")

**直尺**，亦稱為**間尺**，是一種用於量度[長度的](../Page/長度.md "wikilink")[儀器或](../Page/测量仪器.md "wikilink")[文具](../Page/文具.md "wikilink")。這種文具極為普遍，幾乎每位[小學生都有](../Page/小學.md "wikilink")，通常用於量度較短的距離或畫出直線。現代的直尺則多與[三角尺](../Page/三角尺.md "wikilink")、[量角器](../Page/量角器.md "wikilink")、[圓規等製成套裝出售](../Page/圓規.md "wikilink")。

  - 「直尺」在[尺規作圖中只能畫出無窮長的](../Page/尺規作圖.md "wikilink")[直線](../Page/直線.md "wikilink")，是不能有刻度的。但現今的直尺通常泛指[刻度尺](../Page/刻度尺.md "wikilink")。

## 歷史

直尺的發明已不可考究，坊間流傳直尺和圓規是由[伏羲及](../Page/伏羲.md "wikilink")[女媧發明的](../Page/女媧.md "wikilink")。[山東](../Page/山東.md "wikilink")[東漢](../Page/東漢.md "wikilink")[武梁祠](../Page/武梁祠.md "wikilink")[石室留有](../Page/石室.md "wikilink")「*伏羲氏手執矩，女媧氏手執規*」的塑像，其中的「矩」所指的便是「直尺」了。

  - 數學王子[高斯曾於](../Page/高斯.md "wikilink")19歲時只用[圓規和直尺畫出](../Page/圓規.md "wikilink")[正十七邊形](../Page/正十七邊形.md "wikilink")，解決了兩千年來懸而未決的難題。

## 外觀

  - **質料**各不同，有[鐵](../Page/鐵.md "wikilink")、[木](../Page/木.md "wikilink")、[塑膠](../Page/塑膠.md "wikilink")......有一定[硬度](../Page/硬度.md "wikilink")，否則為[軟尺](../Page/軟尺.md "wikilink")。
  - **最少刻度**，一般都是1毫米。
  - **標度單位**常為厘米。
  - **長度**，常見的是1米的米尺和15厘米的短尺，但沒有實際規限，所以長度由一厘米到四五米的都有。
  - **刻度線**有兩種：頂端（直尺一開始就是）和非頂端（刻度線前方尚有一段空白，約1厘米）。
  - **最大誤差**，通常不多於0.2毫米。
      - 合規格的[最大誤差](../Page/最大誤差.md "wikilink")：
          - 長度小於30厘米的最大誤差為±0.1毫米
          - 長度30厘米到50厘米的最大誤差為±0.15毫米
          - 長度50厘米到1[米的最大誤差為](../Page/米.md "wikilink")±0.2毫米

## 使用技巧

  - 垂視直尺，減低[視差](../Page/視差.md "wikilink")。
  - 儘量把測物貼至最近[刻度線](../Page/刻度線.md "wikilink")。
  - 不要以端點作[測量點](../Page/測量點.md "wikilink")，端點易磨損而有較大誤差。
  - 以不同起點測量多次，取其[平均數](../Page/算術平均數.md "wikilink")，結果會較為準確，可有效減低因刻度不平均而引起的誤差。

## 用處

  - 量度距離。
  - 畫[直線](../Page/直線.md "wikilink")。

## 參考文獻

  - [數學與彩陶](http://zhangis.whosee.com/zdz22/etz2.htm)
  - [直尺](https://web.archive.org/web/20040829135740/http://zqdzzx.net/Grade/physical/shiyan/shiyan/01/01changdu/zhichi.htm)
  - [鋼直尺](http://www.imrtvu.edu.cn/kj/wy/dxwl/daxuewulixxyd/html/w_ruler.htm)
  - [幾何作圖---『規矩』vs.『規』『矩』](http://math.ntnu.edu.tw/~horng/letter/vol2no12b.htm)

[Category:文具](../Category/文具.md "wikilink")
[Category:尺子](../Category/尺子.md "wikilink")