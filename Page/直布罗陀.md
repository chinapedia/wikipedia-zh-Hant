[Rock_of_Gibraltar_northwest.jpg](https://zh.wikipedia.org/wiki/File:Rock_of_Gibraltar_northwest.jpg "fig:Rock_of_Gibraltar_northwest.jpg")
**直布罗陀**（）是14個[英國海外領土之一](../Page/英國海外領土.md "wikilink")\[1\]，也是最小的一個，位于[伊比利亚半岛的末端](../Page/伊比利亚半岛.md "wikilink")，是通往[地中海的入口](../Page/地中海.md "wikilink")\[2\]\[3\]。

直布罗陀面积約为6平方公里，北接[西班牙](../Page/西班牙.md "wikilink")[安達魯西亞](../Page/安達魯西亞.md "wikilink")[加的斯省](../Page/加的斯省.md "wikilink")，是英國與[歐洲大陸陸地接洽的唯一地區](../Page/歐洲大陸.md "wikilink")。[直布罗陀巨岩是直布罗陀主要的地标之一](../Page/直布罗陀巨岩.md "wikilink")。直布罗陀的人口集中在该区域的南部，容纳了[直布罗陀人和其他民族共三万多人](../Page/直布罗陀人.md "wikilink")。\[4\]居民人数包括常住的[直布罗陀人](../Page/直布罗陀人.md "wikilink")，一些常驻的[英国人](../Page/英国人.md "wikilink")（包括[英国军人的妻子和家人](../Page/英国军人.md "wikilink")，未包括军人本身）和非英国居民。没有包括参观的游客和短暂停留的人士。

2009年，本地出生的人数达到23,907人，含3,129名英国移民居住者和其余民族2,395人，总人口数达到31,623人。

## 歷史

[Xibraltarplano-es.jpg](https://zh.wikipedia.org/wiki/File:Xibraltarplano-es.jpg "fig:Xibraltarplano-es.jpg")
 位于直布罗陀的戈咸岩洞（Gorham's
Cave）化石证明了[尼安德塔人曾在公元前](../Page/尼安德特人.md "wikilink")28,000至24,000年在直布罗陀聚居，而之後则一直沒有很多人類遺跡。

古时，直布罗陀是[北非](../Page/北非.md "wikilink")[迦太基人的聚居地](../Page/迦太基人.md "wikilink")，在公元前二世紀被[羅馬帝國征服](../Page/羅馬帝國.md "wikilink")。公元五世紀[西羅馬帝國滅亡後](../Page/西羅馬帝國.md "wikilink")，部分[哥特人和當地](../Page/哥特人.md "wikilink")[伊比利亞人和](../Page/伊比利亞人.md "wikilink")[摩爾人](../Page/摩爾人.md "wikilink")（主要是腓尼基人後代）爭奪此地。[伊斯蘭教興起後成為北非信奉伊斯蘭教的摩爾人侵略歐洲的踏腳石](../Page/伊斯蘭教.md "wikilink")，一部摩爾人在[伊比利亞半島南部](../Page/伊比利亚半岛.md "wikilink")（包括了直布羅陀半島），成立了若干小國並接受基督教和伊斯蘭教共存，在十一至十五世紀時[西班牙王國和](../Page/西班牙王國.md "wikilink")[葡萄牙王國抗擊要統一信仰的](../Page/葡萄牙王國.md "wikilink")[北非](../Page/北非.md "wikilink")[穆斯林侵略時](../Page/穆斯林.md "wikilink")，和一部分混血和信奉伊斯蘭教但寬容基督教的歐洲摩爾人聯手，驅逐了侵略者。在[地理大發現及](../Page/地理大發現.md "wikilink")[西班牙帝國國勢強盛前後](../Page/西班牙帝國.md "wikilink")，[西班牙对直布罗陀地区信奉](../Page/西班牙.md "wikilink")[伊斯蘭教的](../Page/伊斯蘭教.md "wikilink")[摩爾人采取驅逐或強逼改宗的政策](../Page/摩爾人.md "wikilink")，而很多人即使改宗後仍被逼遷到北非的[摩洛哥](../Page/摩洛哥.md "wikilink")。

公元1704年，[英國在](../Page/大英帝國.md "wikilink")[西班牙王位繼承戰爭期間攻佔直布羅陀](../Page/西班牙王位繼承戰爭.md "wikilink")，1713年《[烏德勒支和約](../Page/烏德勒支和約.md "wikilink")》中，[西班牙波旁王朝為換取英國承認其合法性](../Page/西班牙波旁王朝.md "wikilink")，正式將直布羅陀割讓予英國。自此，直布羅陀一直被英國統治，不少[意大利人和](../Page/意大利人.md "wikilink")[英國人也開始遷入](../Page/英國人.md "wikilink")，部分摩爾人也回流充實了當地人口。雖然西班牙一直聲稱擁有直布羅陀主權，目前直布罗陀人大多否認西班牙對當地有宗主權。1960年代塞浦路斯和马耳他从英国独立后，直布罗陀成为英国在欧洲的最后一块殖民地，也是欧洲大陆仅存的一块殖民地。

在2006年修订的新直布罗陀宪法中想要改善直布罗陀和英国的关系\[5\]。

## 政治

英國在1713年[西班牙王位繼承戰爭中](../Page/西班牙王位繼承戰爭.md "wikilink")，自西班牙取得直布羅陀，並在1830年正式宣佈直布羅陀為其[殖民地](../Page/殖民地.md "wikilink")。

作為[英國海外領地](../Page/英國海外領地.md "wikilink")，直布羅陀的最高元首為英國女王[伊莉沙白二世](../Page/伊莉沙白二世.md "wikilink")。女王委派[總督代其在當地履行職務](../Page/總督.md "wikilink")。英國負責直布羅陀的[国防](../Page/国防.md "wikilink")、[外交](../Page/外交.md "wikilink")、以及維持治安和財政穩定，實行[地方自治](../Page/地方自治.md "wikilink")。

[直布羅陀總督不直接參與直布羅陀的管理](../Page/直布羅陀總督.md "wikilink")，是形式上的政府[虛位元首](../Page/虛位元首.md "wikilink")。主要職權為任命政府各主要官員和負責治安，也負責督導皇家直布羅陀警察。具體行政事務交由[議會多數黨領袖擔任的](../Page/直布羅陀議會.md "wikilink")[直布羅陀首席部長帶領下之](../Page/直布羅陀首席部長.md "wikilink")[直布羅陀政府負責](../Page/直布羅陀政府.md "wikilink")。

[直布羅陀議會是一個為數](../Page/直布羅陀議會.md "wikilink")17人的立法機構。總部位於[約翰·麥金托什廣場](../Page/約翰·麥金托什廣場.md "wikilink")。議會由選民直選產生。每位選民均有十票。由於直布羅陀面積狹小，議會沒有將直布羅陀劃分選區。所以，每一位議員都代表直布羅陀全體人民。

## 主權爭議

[西班牙一直聲稱擁有直布羅陀的](../Page/西班牙.md "wikilink")[主權](../Page/主權.md "wikilink")，因此主權問題是當地重要的政治議題。但由於西班牙經濟不佳和政治長期不穩定，當地所有黨派都反對[英國將直布羅陀的主權移交予](../Page/英國.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")，他們主張由[直布羅陀居民作決定](../Page/直布羅陀人.md "wikilink")。

另外，於1954年，西班牙最高領導人[佛朗哥限制向直布羅陀供水](../Page/佛朗哥.md "wikilink")，以此試圖奪回直布羅陀主權。\[6\]。之後，[佛朗哥政權不斷對直布羅陀施壓](../Page/佛朗哥時期.md "wikilink")，其結果導致在1967年直布羅陀舉行史上首次公投上，支持主權移交西班牙只有44票，支持英國繼續享有主權則有12,138票。\[7\]。1975年佛朗哥去世後，新的民主政府重新開放邊界，加上英國和西班牙相繼加入歐盟，局勢大幅紓緩。

1997年後由工黨執政的[英國政府被指通過秘密談判試圖將直布羅陀交還給](../Page/英國政府.md "wikilink")[西班牙以博取](../Page/西班牙.md "wikilink")[歐盟的好感](../Page/歐盟.md "wikilink")\[8\]，直布羅陀在2002年舉行了[公民投票](../Page/公民投票.md "wikilink")，對由[英國與](../Page/英國.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[共同管治該地的提議作出表決](../Page/共同管治.md "wikilink")，但最終該提議卻被比率高達98.97%的反對票否定\[9\]。直布羅陀維持由英國統治。

直布羅陀是[歐盟的一部份](../Page/歐盟.md "wikilink")，但不屬於[申根區](../Page/申根區.md "wikilink")。\[10\]因此英國和西班牙之間有邊界管制。

自2004年起，當地居民屬英國西南選區。

2016年的[英國去留歐盟公投](../Page/英國去留歐盟公投.md "wikilink")，直布羅陀96%選民選擇留在歐盟\[11\]，引發西班牙再次對其主權申索主張。西班牙外相加西亞-馬加略（José
Manuel
García-Margallo）表示將尋求與英國共治直布羅陀，並最終全權管治直布羅陀。\[12\]\[13\]唯[直布羅陀首席部長](../Page/直布羅陀首席部長.md "wikilink")（Fabian
Picardo）隨即否定加西亞的説法，指先前的地方公投已反對共同管治，強調「不存在任何有關直布羅陀主權會談的空間，連會談的會談對話也不可能」，呼籲直布羅陀居民無須理會。\[14\]\[15\]

## 地理

### 位置

[Hércules3D.jpg](https://zh.wikipedia.org/wiki/File:Hércules3D.jpg "fig:Hércules3D.jpg")、英国的海外领土直布罗陀（左）；[摩洛哥和](../Page/摩洛哥.md "wikilink")[休达](../Page/休达.md "wikilink")（右）。\]\]
直布罗陀是位於西班牙南面的地中海沿岸的一个[半岛](../Page/半岛.md "wikilink")，面积只有6.8平方公里，海岸線長12公里，扼守[地中海和](../Page/地中海.md "wikilink")[大西洋之間的航海要道](../Page/大西洋.md "wikilink")—[直布羅陀海峽](../Page/直布羅陀海峽.md "wikilink")。

### 天然資源

除有限淡水資源，該處並沒有其他明顯的天然資源，因此當地一直依賴[海水淡化](../Page/海水淡化.md "wikilink")，直至近期當地人才興建集水設施蓄水。

### 氣候

直布罗陀屬[地中海氣候](../Page/地中海氣候.md "wikilink")，[夏天乾燥暖和](../Page/夏天.md "wikilink")，[冬天濕潤有雨而不太冷](../Page/冬天.md "wikilink")。

| 月份                                                                                                 | 全年均值    | 一月      | 二月      | 三月      | 四月      | 五月      | 六月      | 七月      | 八月      | 九月      | 十月      | 十一月     | 十二月     |
| -------------------------------------------------------------------------------------------------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- |
| 平均最高气温°C (°F)                                                                                      | 21 (70) | 16 (61) | 16 (62) | 17 (64) | 18 (66) | 21 (71) | 24 (76) | 27 (81) | 27 (82) | 26 (79) | 21 (71) | 18 (66) | 16 (62) |
| 平均最低气温°C (°F)                                                                                      | 15 (60) | 11 (52) | 11 (52) | 12 (54) | 13 (56) | 15 (60) | 17 (64) | 20 (68) | 20 (69) | 20 (68) | 16 (62) | 13 (57) | 12 (54) |
| *数据来源: [Weatherbase](http://www.weatherbase.com/weather/weather.php3?s=59480&refer=&units=metric)* |         |         |         |         |         |         |         |         |         |         |         |         |         |

### 地質

當地岩石以[侏羅紀時期形成的](../Page/侏羅紀.md "wikilink")[石灰岩為主](../Page/石灰岩.md "wikilink")，並因侵蝕作用而在[直布羅陀巨巖內形成了](../Page/直布羅陀巨巖.md "wikilink")[鐘乳洞](../Page/鐘乳洞.md "wikilink")（St.
Machael Cave）。高而斜的巨岩限制了當地的土地發展，因而迫使當地不斷填海，以滿足土地需求，相關土地佔總面積一成。

### 生態

當地動植物與[西班牙相似](../Page/西班牙.md "wikilink")。但值得一提的是，直布羅陀是全歐洲唯一有野生猴子（巴巴利獼猴）生活和有[直布羅陀屈曲花出現的地方](../Page/直布羅陀屈曲花.md "wikilink")，而這兩種生物主要分佈於北非。

## 人口及語言

[Abdulaziz_Mosque_Gibraltar.jpg](https://zh.wikipedia.org/wiki/File:Abdulaziz_Mosque_Gibraltar.jpg "fig:Abdulaziz_Mosque_Gibraltar.jpg")。\]\]
人口三萬餘，三分之二的人口為[義大利人](../Page/義大利人.md "wikilink")、和[西班牙人的後裔](../Page/西班牙人.md "wikilink")、英國人約5,000人；摩洛哥人約3,000人；其餘少數人口為印度人、葡萄牙人、巴基斯坦人。整個半島分為東西兩部份，人口主要集中在西岸。直布羅陀的人口密度之高位居世界前列，每平方公里4,530人。\[16\]

[直布罗陀人是几百年来移居此地的许多欧洲移民的种族文化大拼盘](../Page/直布罗陀人.md "wikilink")。这些人是在1704年大多数西班牙人离开之后前往直布罗陀的经济移民的后裔。少数于1704年8月留在当地的少数西班牙人后来加上了随着黑塞乔治王子（Prince
George of
Hesse）的舰队一起来到直布罗陀的两百多加泰罗尼亚人。\[17\]到了1753年热那亚人、和[葡萄牙人成为了新增人口中的大多数](../Page/葡萄牙人.md "wikilink")。其他族群包括梅诺卡人（当[梅诺卡岛在](../Page/梅诺卡岛.md "wikilink")1802年归还西班牙时被迫离家来到此地）、撒丁岛人、西西里人和其他意大利人、法国人、德国人和英国人。来自西班牙的移民以及与周边西班牙城镇进行的跨境婚姻是直布罗陀历史上的一个固有特色，直到[佛朗哥将军关闭了与直布罗陀的边境](../Page/佛朗哥.md "wikilink")，直布羅陀人與西班牙親屬的聯繫因此中斷。1982年西班牙政府重新开放了陆地边界，但其他限制规定仍然保留不变。

官方語言為[英語及](../Page/英語.md "wikilink")[西班牙語](../Page/西班牙語.md "wikilink")，[意大利語和](../Page/意大利語.md "wikilink")[葡萄牙語同樣通行](../Page/葡萄牙語.md "wikilink")，另外，部份[直布羅陀人也會使用](../Page/直布羅陀.md "wikilink")，這是一種[英語中夾雜](../Page/英語.md "wikilink")[西班牙語的語言](../Page/西班牙語.md "wikilink")，在對話中，部份的[直布羅陀人通常會由](../Page/直布羅陀.md "wikilink")[英語開始](../Page/英語.md "wikilink")，但隨著對話逐漸的深入，他們就會在[英語中夾雜一些](../Page/英語.md "wikilink")[西班牙語](../Page/西班牙語.md "wikilink")。\[18\]

## 宗教

根據2012年的人口普查，大約72.1％的直布羅陀人是[羅馬天主教信徒](../Page/羅馬天主教.md "wikilink")。\[19\]始建於16世紀的[聖母加冕主教座堂為](../Page/聖母加冕主教座堂.md "wikilink")[天主教直布羅陀教區的主教座堂也是該地區最古老的天主教堂](../Page/天主教直布羅陀教區.md "wikilink")。其他基督教派包括[英格蘭教會](../Page/英格蘭教會.md "wikilink")（7.7％）、[衛理公會](../Page/衛理公會.md "wikilink")、[蘇格蘭教會](../Page/蘇格蘭教會.md "wikilink")、[普利茅斯弟兄會](../Page/普利茅斯弟兄會.md "wikilink")、[五旬節運動](../Page/五旬節運動.md "wikilink")、[耶穌基督後期聖徒教會和](../Page/耶穌基督後期聖徒教會.md "wikilink")[耶和華見證人](../Page/耶和華見證人.md "wikilink")，[聖三一教堂是](../Page/聖三一座堂_\(直布羅陀\).md "wikilink")[聖公會歐洲教區的主教座堂](../Page/聖公會歐洲教區.md "wikilink")。7.1％的人表示他們沒有宗教信仰。直布羅陀世俗人道主義協會也定期舉行會議。[伊斯蘭教為直布羅陀第三大宗教](../Page/伊斯蘭教.md "wikilink")（3.6％），其他宗教還有[猶太教](../Page/猶太教.md "wikilink")（2.4％）、[印度教](../Page/印度教.md "wikilink")（2％）和[巴哈伊教](../Page/巴哈伊教.md "wikilink")。\[20\]

## 經濟

[Gibraltar_aerial_view_looking_northwest.jpg](https://zh.wikipedia.org/wiki/File:Gibraltar_aerial_view_looking_northwest.jpg "fig:Gibraltar_aerial_view_looking_northwest.jpg")
[Gibraltar_Airport_Main_Highway.jpg](https://zh.wikipedia.org/wiki/File:Gibraltar_Airport_Main_Highway.jpg "fig:Gibraltar_Airport_Main_Highway.jpg")
[Gibraltar_navy.jpg](https://zh.wikipedia.org/wiki/File:Gibraltar_navy.jpg "fig:Gibraltar_navy.jpg")基地\]\]
傳統上[英軍主導直布羅陀的](../Page/英軍.md "wikilink")[經濟](../Page/經濟.md "wikilink")，海軍船塢製造大量經濟活動。但過去20年，英軍對直布羅陀經濟的影響力急劇減少。1984年，英軍經濟活動仍佔當地生產總值超過60%，現在卻只佔7%。

現在直布羅陀的經濟以[服務性產業為主](../Page/服務業.md "wikilink")，其中以[金融和](../Page/金融.md "wikilink")[旅遊業為最重要](../Page/旅遊業.md "wikilink")，不少[英國](../Page/英國.md "wikilink")[銀行和國際](../Page/銀行.md "wikilink")[銀行在當地設有辦事處](../Page/銀行.md "wikilink")，直布羅陀已成為金融中心。由於當地不設[利得稅等](../Page/利得稅.md "wikilink")[稅收](../Page/稅.md "wikilink")，這也吸引不少博彩商和[線上遊戲商前來直布羅陀設置據點](../Page/線上遊戲.md "wikilink")。直布羅陀的[本地生產總值為](../Page/本地生產總值.md "wikilink")4億3000萬鎊，人均產值為15,700鎊。主要出口市場分別為[法國](../Page/法國.md "wikilink")（19.4%）、[西班牙](../Page/西班牙.md "wikilink")（14.1%）、[土耳其](../Page/土耳其.md "wikilink")（12.1%）、[瑞士](../Page/瑞士.md "wikilink")（11.7%）、[德國](../Page/德國.md "wikilink")（10.1%）、[英國](../Page/英國.md "wikilink")（9.1%）和[希臘](../Page/希臘.md "wikilink")（6.8%）。进口主要來自西班牙（19.9%）、[俄羅斯](../Page/俄羅斯.md "wikilink")（18.4%）和[英國](../Page/英國.md "wikilink")（10.8%）。

### 淡水供应

直布罗陀没有河湖。历史上靠积攒雨水维持，导致霍乱、黄热病反复暴发流行。目前，直布罗陀完全依靠海水淡化，同时还提供了一套用于卫生目的的海水供水管网。每年淡水使用约100万立方米，海水使用400万立方米。参见：

### 貨幣

由於直布羅陀是英國的海外領土，享有較大的自主權，因此直布羅陀可以發行自己的護照、車牌及貨幣等。其中直布羅陀鎊與英鎊等值，英鎊和歐元也同時流通於直布羅陀境內。但是直布羅陀內所有貨品以直布羅陀鎊或英鎊結算，若以歐元結算必需補足匯率差價。

## 對外交通

[Bay_of_Gibraltar_from_The_Rock_12.jpg](https://zh.wikipedia.org/wiki/File:Bay_of_Gibraltar_from_The_Rock_12.jpg "fig:Bay_of_Gibraltar_from_The_Rock_12.jpg")頂端俯瞰街景及海港\]\]

### 航空

每天有航班來往[倫敦](../Page/倫敦.md "wikilink")[格域機場](../Page/格域機場.md "wikilink")。

直布羅陀機場設於與[西班牙的邊界旁](../Page/西班牙.md "wikilink")，基於地理上的限制，其跑道長度很短，只能供中小型飛機升降（如[波音737](../Page/波音737.md "wikilink")、[空中巴士A320等](../Page/空中客車A320.md "wikilink")），直布羅陀唯一過境道路須通過跑道路面，故每當有航班升降時，鐵路型的道口欄杆便落下封閉道路讓飛機安全通過，禁止車輛通行。故這是世界上唯一僅有的跑道平交道，饒富特色。

### 船運

有渡輪穿越[直布羅陀海峽](../Page/直布羅陀海峽.md "wikilink")，往來北非[摩洛哥](../Page/摩洛哥.md "wikilink")[丹吉爾](../Page/丹吉尔.md "wikilink")，單程約80分鐘。

### 鐵道

直布羅陀沒有鐵道接駁，旅客一般須步行過境到西班牙的[利尼亞乘搭地區巴士前往](../Page/拉利内亚德拉孔塞普西翁.md "wikilink")[阿爾赫西拉斯市](../Page/阿爾赫西拉斯.md "wikilink")，再轉乘[西班牙國鐵火車](../Page/西班牙國鐵.md "wikilink")。

### 公路

直布羅陀為英國海外領土，早期[道路通行方向為靠左行駛](../Page/道路通行方向.md "wikilink")。1929年起為了方便往來，與西班牙道路系統整合，改為靠右行駛。

直布羅陀市內設有數條免費巴士線，並設有觀光性質的10號紅色[雙層巴士](../Page/雙層巴士.md "wikilink")（也有單層）線前往市中心，車資可用[直布羅陀鎊](../Page/直布羅陀鎊.md "wikilink")（1.3直布羅陀鎊）、[英鎊或](../Page/英鎊.md "wikilink")[歐元](../Page/歐羅.md "wikilink")（2歐元）繳付；直布羅陀公民則可出示居民卡免費搭乘。

境內有長途巴士站，但班次不多，旅客一般會到西班牙利尼亞的長途巴士站轉搭巴士前往其他地方。

## 参考文献

## 外部連結

  - [直布羅陀政府](http://www.gibraltar.gov.gi/)
  - [CIA
    Factbook](http://www.cia.gov/cia/publications/factbook/geos/gi.html)

{{-}}

[\*](../Category/直布羅陀.md "wikilink")
[Category:歐洲海岬](../Category/歐洲海岬.md "wikilink")
[Category:歐洲半島](../Category/歐洲半島.md "wikilink")
[Category:英語國家地區](../Category/英語國家地區.md "wikilink")
[Category:英國海外領土及皇室屬土](../Category/英國海外領土及皇室屬土.md "wikilink")
[Category:属地首府](../Category/属地首府.md "wikilink")
[Category:城邦](../Category/城邦.md "wikilink")

1.

2.  [Gibraltar](http://dictionary.reference.com/browse/Gibraltar)
    Dictionary.com

3.  [Gibraltar](http://www.thefreedictionary.com/Gibraltar)
    TheFreeDictionary.com

4.

5.

6.

7.

8.

9.

10. [擬徵高額過境費
    西國槓英屬直布羅陀](http://tw.news.yahoo.com/%E6%93%AC%E5%BE%B5%E9%AB%98%E9%A1%8D%E9%81%8E%E5%A2%83%E8%B2%BB-%E8%A5%BF%E5%9C%8B%E6%A7%93%E8%8B%B1%E5%B1%AC%E7%9B%B4%E5%B8%83%E7%BE%85%E9%99%80-213000181.html)

11.

12.

13.

14.
15.

16.

17.

18.

19.

20.