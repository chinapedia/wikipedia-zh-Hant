[Hércules3D.jpg](https://zh.wikipedia.org/wiki/File:Hércules3D.jpg "fig:Hércules3D.jpg")、英国的海外领土[直布罗陀](../Page/直布罗陀.md "wikilink")（左）；[摩洛哥和](../Page/摩洛哥.md "wikilink")[休达](../Page/休达.md "wikilink")（右）。\]\]
[Straße_von_gibraltar_panorama_V1.jpg](https://zh.wikipedia.org/wiki/File:Straße_von_gibraltar_panorama_V1.jpg "fig:Straße_von_gibraltar_panorama_V1.jpg")

**直布羅陀海峽**（；；）是位於[歐洲與](../Page/歐洲.md "wikilink")[非洲之間](../Page/非洲.md "wikilink")，分隔[大西洋與](../Page/大西洋.md "wikilink")[地中海的](../Page/地中海.md "wikilink")[海峽](../Page/海峽.md "wikilink")，其名取自[伊比利半島南端的](../Page/伊比利半島.md "wikilink")[直布羅陀](../Page/直布羅陀.md "wikilink")，雖然海峽兩岸理應為[西班牙和](../Page/西班牙.md "wikilink")[摩洛哥兩國分別對望](../Page/摩洛哥.md "wikilink")，但直布羅陀本地卻是屬於[英國海外領地](../Page/英國海外領地.md "wikilink")，形成三國鼎立的局勢。水深300[米](../Page/米.md "wikilink")，最窄處寬14.3[公里](../Page/公里.md "wikilink")。修建直布罗陀海峡通道的计划早在1970年代末提出。1979年6月，西班牙和摩洛哥两国[国王在摩洛哥的](../Page/国王.md "wikilink")[非斯会晤](../Page/非斯.md "wikilink")，達成了協議，研究通道可行與否。1980年10月，双方签署了相关的[科技合作协定](../Page/科技.md "wikilink")。

## 圖片集

<File:3> Yang Ming vessel Strait of Gibraltar
040917.jpg|[臺灣](../Page/臺灣.md "wikilink")[陽明海運貨輪通過直布羅陀海峽](../Page/陽明海運.md "wikilink")
<File:Strait> of gibraltar-rightee.jpg|直布羅陀海峽
[File:StraitOfGibraltar.JPG|直布羅陀海峽](File:StraitOfGibraltar.JPG%7C直布羅陀海峽)
<File:STS059-238-074> Strait of
Gibraltar.jpg|從太空鳥瞰直布羅陀海峽與[地中海](../Page/地中海.md "wikilink")

{{-}}

[Category:歐洲海峽](../Category/歐洲海峽.md "wikilink")
[Category:非洲海峽](../Category/非洲海峽.md "wikilink")
[Category:地中海海峽](../Category/地中海海峽.md "wikilink")
[Category:西班牙海峽](../Category/西班牙海峽.md "wikilink")
[Category:摩洛哥海峽](../Category/摩洛哥海峽.md "wikilink")
[Category:英國海峽](../Category/英國海峽.md "wikilink")
[Category:英國海外領土](../Category/英國海外領土.md "wikilink")
[Category:直布羅陀地理](../Category/直布羅陀地理.md "wikilink")
[Category:跨國海峽](../Category/跨國海峽.md "wikilink")
[Category:西班牙-摩洛哥邊界](../Category/西班牙-摩洛哥邊界.md "wikilink")
[Category:西班牙-直布羅陀邊界](../Category/西班牙-直布羅陀邊界.md "wikilink")