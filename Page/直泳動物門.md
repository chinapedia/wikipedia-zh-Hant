**直泳動物門**（[學名](../Page/學名.md "wikilink")：）是由所知甚少的[海底](../Page/海底.md "wikilink")[無脊椎動物](../Page/無脊椎動物.md "wikilink")[寄生蟲所組成的一個小](../Page/寄生蟲.md "wikilink")[門](../Page/門_\(生物\).md "wikilink")，是最簡單的[多細胞生物之一](../Page/多細胞生物.md "wikilink")。

寄生的生物有[扁形動物](../Page/扁形動物.md "wikilink")、[多毛綱的蟲](../Page/多毛綱.md "wikilink")、[雙殼綱的](../Page/雙殼綱.md "wikilink")[軟體動物和](../Page/軟體動物.md "wikilink")[棘皮動物等](../Page/棘皮動物.md "wikilink")。

已知的物種大約有20種，其中以 *Rhopalura ophiocomae* 最為知名。

於1880年時，原本是被歸類為[中生動物門的一](../Page/中生動物門.md "wikilink")[綱](../Page/綱.md "wikilink")，而有時則被歸為其中的一[目](../Page/目_\(生物\).md "wikilink")。但目前的研究顯現，直泳蟲和中生動物門的另一群－[菱形蟲](../Page/菱形蟲.md "wikilink")，幾乎可以說是完全地不同。

已知種：

  - Family [Rhopaluridae](../Page/Rhopaluridae.md "wikilink")
      - *[Ciliocincta julini](../Page/Ciliocincta_julini.md "wikilink")*
        (Caullery and Mesnil, 1899) -
        北[大西洋東部](../Page/大西洋.md "wikilink")，寄生於多毛鋼
      - *[Intoshia
        leptoplanae](../Page/Intoshia_leptoplanae.md "wikilink")*
        ([Giard](../Page/Charles_Frédéric_Girard.md "wikilink"), 1877) -
        北大西洋東部，寄生於扁形動物(*[Leptoplana](../Page/Leptoplana.md "wikilink")*)
      - *[Intoshia linei](../Page/Intoshia_linei.md "wikilink")* (Giard,
        1877) - 北大西洋東部，寄生於[紐形動物](../Page/紐形動物.md "wikilink")
        (*[Lineus](../Page/Lineus.md "wikilink")*) = *Rhopalura linei*
      - *[Intoshia major](../Page/Intoshia_major.md "wikilink")*
        (Shtein, 1953) -
        [北極海](../Page/北極海.md "wikilink")，寄生於[腹足綱](../Page/腹足綱.md "wikilink")
        (*[Lepeta](../Page/Lepeta.md "wikilink")*,
        *[Natica](../Page/Natica.md "wikilink")*,
        *[Solariella](../Page/Solariella.md "wikilink")*) = *Rhopalura
        major*
      - *[Intoshia
        paraphanostomae](../Page/Intoshia_paraphanostomae.md "wikilink")*
        (Westblad, 1942) -
        北大西洋東部，寄生於扁形動物(*[Acoela](../Page/Acoela.md "wikilink")*)
      - *[Rhopalura elongata](../Page/Rhopalura_elongata.md "wikilink")*
        (Shtein, 1953) -
        北極海，寄生於雙殼綱(*[Astarte](../Page/Astarte.md "wikilink")*)
      - *[Rhopalura gigas](../Page/Rhopalura_gigas.md "wikilink")*
        (Giard, 1877)
      - *[Rhopalura granosa](../Page/Rhopalura_granosa.md "wikilink")*
        (Atkins, 1933) -
        北大西洋東部，寄生於雙殼綱(*[Pododesmus](../Page/Pododesmus.md "wikilink")*)
      - *[Rhopalura intoshi](../Page/Rhopalura_intoshi.md "wikilink")*
        (Metchnikoff) - 地中海，寄生於紐形動物
      - *[Rhopalura
        litoralis](../Page/Rhopalura_litoralis.md "wikilink")* (Shtein,
        1954) - 北極海，寄生於腹足綱(*[Lepeta](../Page/Lepeta.md "wikilink")*,
        *[Natica](../Page/Natica.md "wikilink")*,
        *[Solariella](../Page/Solariella.md "wikilink")*)
      - *[Rhopalura
        metschnikowi](../Page/Rhopalura_metschnikowi.md "wikilink")*
        (Caullery and Mesnil, 1901) - 北大西洋東部，寄生於多毛鋼和紐形動物
      - *[Rhopalura
        murmanica](../Page/Rhopalura_murmanica.md "wikilink")* (Shtein,
        1953) - 北極海，寄生於腹足綱(*[Rissoa](../Page/Rissoa.md "wikilink")*,
        *[Columbella](../Page/Columbella.md "wikilink")*)
      - *[Rhopalura
        ophiocomae](../Page/Rhopalura_ophiocomae.md "wikilink")* (Giard,
        1877) - 北大西洋東部，寄生於[蛇尾綱](../Page/蛇尾綱.md "wikilink")(usually
        *[Amphipholis](../Page/Amphipholis.md "wikilink")*)
      - *[Rhopalura
        pelseeneri](../Page/Rhopalura_pelseeneri.md "wikilink")*
        (Caullery and Mesnil, 1901) - 北大西洋東部，寄生於多毛鋼和紐形動物
      - *[Rhopalura philinae](../Page/Rhopalura_philinae.md "wikilink")*
        (Lang, 1951) - 北大西洋東部，寄生於腹足綱
      - *[Rhopalura
        pterocirri](../Page/Rhopalura_pterocirri.md "wikilink")* (de
        Saint-Joseph, 1896) 北大西洋東部，寄生於多毛鋼
      - *[Rhopalura
        variabili](../Page/Rhopalura_variabili.md "wikilink")*
        (Alexandrov and Sljusarev, 1992) -
        北極海，寄生於扁形動物(*[Macrorhynchus](../Page/Macrorhynchus.md "wikilink")*)
      - *[Stoecharthrum
        giardi](../Page/Stoecharthrum_giardi.md "wikilink")* (Caullery
        and Mesnil, 1899) - 北大西洋東部，寄生於多毛鋼
      - *[Stoecharthrum
        monnati](../Page/Stoecharthrum_monnati.md "wikilink")* (Kozloff,
        1993) - 北大西洋東部，寄生於軟體動物

<!-- end list -->

  - Family [Pelmatosphaeridae](../Page/Pelmatosphaeridae.md "wikilink")
      - *[Pelmatosphaera
        polycirri](../Page/Pelmatosphaera_polycirri.md "wikilink")*
        (Caullery and Mesnil, 1904) - 北大西洋東部，寄生於多毛鋼和紐形動物

## 參考文獻

  - Giard, E., "The Orthonectida, a new class of the phylum of the
    worms" *Quart. J. Microsc. Sci.*, 1880 n.s. 20: 225-240
  - Hanelt B, Van Schyndel D, Adema CM, Lewis LA, Loker ES, "The
    phylogenetic position of Rhopalura ophiocomae (Orthonectida) based
    on 18S ribosomal DNA sequence analysis", *Mol Biol Evol.* 1996
    Nov;13(9):1187-91

[Category:動物](../Category/動物.md "wikilink")
[Category:无脊椎动物](../Category/无脊椎动物.md "wikilink")
[Category:直泳動物門](../Category/直泳動物門.md "wikilink")
[Category:中生动物门](../Category/中生动物门.md "wikilink")