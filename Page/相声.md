[天津名流茶馆的相声表演.jpg](https://zh.wikipedia.org/wiki/File:天津名流茶馆的相声表演.jpg "fig:天津名流茶馆的相声表演.jpg")名流茶馆的相声表演\]\]
**相声**是一种[中国](../Page/中国.md "wikilink")[曲艺表演艺术](../Page/曲艺.md "wikilink")，於清朝道光年間，在北京出現，其前身為[八角鼓](../Page/八角鼓.md "wikilink")。主要功夫分四门功课：说、学、逗、唱。

相声一词，古作**像生**，原指模拟别人的言行，后发展为**象声**。象声又称**隔壁戲**。经[清朝时期的发展直至](../Page/清朝.md "wikilink")[民国初年](../Page/民国.md "wikilink")，象声逐渐从一个人摹拟口技发展成为单口笑话，名称也就随之转变为相声。由单口相声，后来逐步发展出对口相声、群口相声，综合为一体，成为名副其实的相声。而经过多年的发展，对口相声最终成为最受观众喜爱的相声形式。相声在[两岸三地有不同的发展模式](../Page/两岸三地.md "wikilink")。

## 历史

### 起源

起源自滿族曲藝[八角鼓](../Page/八角鼓.md "wikilink")\[1\]。[张三禄是目前见于](../Page/张三禄.md "wikilink")[文字记载最早的相声](../Page/文字记载.md "wikilink")[艺人](../Page/艺人.md "wikilink")。根据相关记载并推测：张三禄本是北京的[八角鼓丑角艺人](../Page/八角鼓.md "wikilink")，后創造了相声。他的艺术生涯始于[清朝的](../Page/清朝.md "wikilink")[道光年间](../Page/道光.md "wikilink")。在《[随缘乐](../Page/随缘乐.md "wikilink")》子弟书中说：“学相声好似还魂张三禄，铜骡子于三胜到像活的一样。”\[2\]
在張三祿之後，分為朱、沈、阿三派。其中以藝名窮不怕的[朱少文門徒最多](../Page/朱少文.md "wikilink")，現今多數相聲藝人都以他為始祖\[3\]。

### 早期发展

[抗日战争时期](../Page/抗日战争.md "wikilink")，一些相声演员表现出民族气节。[常宝堃曾经两次因为讽刺日偽佔區現狀而被捕](../Page/常宝堃.md "wikilink")\[4\]，[张寿臣公开赞扬](../Page/张寿臣.md "wikilink")[吉鸿昌等人的抗日](../Page/吉鸿昌.md "wikilink")，批评当局的[不抵抗政策](../Page/不抵抗政策.md "wikilink")\[5\]，也曾因为讽刺当时[天津的](../Page/天津.md "wikilink")[警察](../Page/警察.md "wikilink")“贱”遇到麻烦\[6\]。

### 在中国大陆的發展

1949年后，一大批以[侯宝林为代表的从](../Page/侯宝林.md "wikilink")[中华人民共和国成立之前就在说相声的演员逐渐转型](../Page/中华人民共和国.md "wikilink")，将相声的内容加以改造，去掉了大量[色情](../Page/色情.md "wikilink")、挖苦别人生理缺陷，以及对捧哏方人身攻击之类的段子，以此迎合新的政治与文化形式。相声的流行的一个原因是因为它是一种以声音为主的艺术，适合以被普及的[无线广播作为主要媒体](../Page/广播电台.md "wikilink")。相声被称为“[文艺战线上的](../Page/文艺战线.md "wikilink")[轻骑兵](../Page/轻骑兵.md "wikilink")”。
除了重新整理的传统相声之外，初期还有很多讽刺型的相声，讽刺“[旧社会](../Page/旧社会.md "wikilink")”或者新时代思想落后的人。但由于共产党的政策，一些人意识到歌颂[社会主义的相声的需要](../Page/社会主义.md "wikilink")。1958年[总路线时期](../Page/社会主义建设总路线.md "wikilink")，一批歌颂型相声开始大量出现。其间以[马季等为代表人物](../Page/马季.md "wikilink")。
尽管如此，在随后的[文化大革命中](../Page/文化大革命.md "wikilink")，很多相声艺人遭到打压，曾一度让相声在中国大陆绝迹，只有一些歌颂型相声被允许演出。
文革之后，相声得以恢复。以[姜昆](../Page/姜昆.md "wikilink")、[李文华的](../Page/李文華_\(相聲演員\).md "wikilink")《[如此照相](../Page/如此照相.md "wikilink")》和[常宝华](../Page/常宝华.md "wikilink")、[常贵田的](../Page/常贵田.md "wikilink")《[帽子工厂](../Page/帽子工厂.md "wikilink")》为代表的一大批讽刺“[四人帮](../Page/四人帮.md "wikilink")”的相声迅速流行。从前侯宝林等人的相声也重新在[广播电台播出](../Page/广播电台.md "wikilink")。

进入1980年代，在日益流行的曲艺形式[小品的冲击下](../Page/喜剧小品.md "wikilink")，表演形式简单的相声不再得到以[电视为主要传播媒体的观众的青睐](../Page/电视.md "wikilink")。一些新的相声形式，如弹唱相声、相声剧等被发展出来，但市场仍然不大（与此同时，相声的大量元素被吸收到小品中）。尽管如此，这段时间里相声还是获得了不小的发展：新一代演员涌现出现，各种内容和形式上崭新的相声段子不断登台，形成了有别之前的“当代相声”。其中无论是歌颂娱乐型还是针砭时弊性的段子，都很多深受大众欢迎的例子。在这一时期的各种大小文艺场合，相声仍是娱乐大众的主角。

1990年代中后期开始，相声开始逐渐式微，新段子越来越少，脍炙人口的作品凤毛麟角，而且内容中讽刺时政的内容也日益罕见，老式的纯娱乐风格相声开始逐渐居多。在此同时，包括许多知名演员在内的相声演员离开了相声舞台转而从事其他工作，可是新人中能接班的却不多。相声的地位逐渐为繁荣的小品所取代。

在21世纪初，相声在中国大陆处于青黄不接的局面：老一辈艺术家纷纷陨落，1980年代当红的演员们对于相声的发展也表现出了力不从心的状态；在为了重振相声举办的“[全国相声大赛](../Page/全国相声大赛.md "wikilink")”中，新生代亦始终不见勃兴。（中国[中央电视台分别在](../Page/中央电视台.md "wikilink")2002年[元旦](../Page/元旦.md "wikilink")、2003年元旦、2006年[国庆节和](../Page/中华人民共和国国庆节.md "wikilink")2008年[五一](../Page/国际劳动节.md "wikilink")[黄金周期间举办了四届全国相声大赛](../Page/黃金週_\(中國\).md "wikilink")。首届和第二届受到了观众的好评，第三届第四届相声大赛却被指“看不到相声的相声大赛”\[7\]。第三届相声大赛闭幕式中[马季与主持人](../Page/马季.md "wikilink")[周涛和](../Page/周涛.md "wikilink")[毕福剑合说的相声](../Page/毕福剑.md "wikilink")《学相声》成了马季最后的公开相声演出。）相声的发展前途不被多数人看好，但是在媒体以外的地方，许多以传统方式演出的相声剧团还是保留了一定水平并具有相当多观众的。在[天津的许多小剧场与](../Page/天津.md "wikilink")[茶馆都可以听到相当精采的传统相声](../Page/茶馆.md "wikilink")。而同样曾在茶馆传统方式演出的[郭德纲在](../Page/郭德纲.md "wikilink")2005年之后走红，虽然不同于真正的[茶馆相声](../Page/茶馆.md "wikilink")，给观众带了一些对传统的认同，并让中国大陆看到相声复兴的希望；[郭德纲不但改编过大量的传统相声](../Page/郭德纲.md "wikilink")，同时也创作了大量的相声，如大获成功的《我》字系列、《你》字系列；同时推广了相声的本门唱[太平歌词](../Page/太平歌词.md "wikilink")（原先理解的相声唱是京剧、评剧等）。在以郭德纲为主的相声演员的努力下，中国大陆的相声艺术也首次在非华语地区举行大型演出。
\[8\]

### 在台灣的發展

1949年，[中華民國政府退守](../Page/中華民國政府.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，一批相声演员也到了臺灣。当年，陳逸安、[魏龍豪](../Page/魏龍豪.md "wikilink")（魏甦）、[吳兆南](../Page/吳兆南.md "wikilink")（吳宗炎）結識，在[中國廣播公司](../Page/中國廣播公司.md "wikilink")、[警察廣播電台等廣播電台一同主持相聲節目](../Page/警察廣播電台.md "wikilink")。隨後在1967年起，開始收集資料灌製『相聲集錦』、『相聲選粹』、『相聲捕軼』以及『相聲拾穗』。

最初，相声的主要听众是以[眷村为主的](../Page/眷村.md "wikilink")[外省人](../Page/外省人.md "wikilink")。近年来，由[表演工作坊](../Page/表演工作坊.md "wikilink")（簡稱“表坊”）於1985年推出舞台劇『[那一夜，我們說相聲](../Page/那一夜，我們說相聲.md "wikilink")』（由[李立群](../Page/李立群.md "wikilink")、[李國修主演](../Page/李國修.md "wikilink")）之後，造成了轟動。

接著，“表坊”於1989年推出了舞台劇《[這一夜，誰來說相聲？](../Page/這一夜，誰來說相聲？.md "wikilink")》（由李立群、[金士傑](../Page/金士傑.md "wikilink")、[陳立華三人主演](../Page/陳立華.md "wikilink")），1991年推出《臺灣怪談》（李立群單口相聲），1993年推出舞台劇《那一夜，我們說相聲》（由李立群、[馮翊綱重新詮釋](../Page/馮翊綱.md "wikilink")），1997年推出舞台劇《又一夜，他們說相聲》（馮翊綱、[趙自強](../Page/趙自強.md "wikilink")、[卜學亮三人主演](../Page/卜學亮.md "wikilink")），2000年，推出舞台劇《千禧夜，我們說相聲》（趙自強、[金士傑](../Page/金士傑.md "wikilink")、[倪敏然三人主演](../Page/倪敏然.md "wikilink")）；最後，在2005年，推出了舞台劇《這一夜，Women說相聲》（[方芳](../Page/方芳.md "wikilink")、[鄧程慧](../Page/鄧程慧.md "wikilink")、[蕭艾三人主演](../Page/蕭艾.md "wikilink")）。雖皆以相聲命名，實則為舞台劇。

在1988年四月，馮翊綱、[宋少卿組成](../Page/宋少卿.md "wikilink")[相聲瓦舍](../Page/相聲瓦舍.md "wikilink")（2001年加入[黃士偉](../Page/黃士偉.md "wikilink")），開啟了[舞台劇融合相聲藝術的創作表演](../Page/舞台劇.md "wikilink")。在2004年7月8日，輔佐『可以演戲劇團』推出第一部作品『給我一個膠帶』。

1993年，[劉增鍇](../Page/劉增鍇.md "wikilink")、[林文彬創立](../Page/林文彬.md "wikilink")[台北曲藝團](../Page/台北曲藝團.md "wikilink")，除了相聲以外，同時推出許多中國特有的[說唱藝術](../Page/說唱藝術.md "wikilink")，如[雙簧](../Page/雙簧.md "wikilink")、[評書](../Page/評書.md "wikilink")、[數來寶](../Page/數來寶.md "wikilink")、[快書](../Page/快書.md "wikilink")、[京韻大鼓](../Page/京韻大鼓.md "wikilink")、[梅花大鼓](../Page/梅花大鼓.md "wikilink")、[西河大鼓](../Page/西河大鼓.md "wikilink")、[單弦](../Page/單弦.md "wikilink")、[太平歌詞等](../Page/太平歌詞.md "wikilink")，並多次推動兩岸相聲藝術交流演出。

1999年8月26日，[吳兆南於收徒大典時宣佈成立](../Page/吳兆南.md "wikilink")[吳兆南相聲劇藝社](../Page/吳兆南相聲劇藝社.md "wikilink")，限定僅吳兆南直系徒子徒孫可以成為正式團員。除了相聲以外，另包括各項[曲藝](../Page/曲藝.md "wikilink")，如[雙簧](../Page/雙簧.md "wikilink")、[數來寶](../Page/數來寶.md "wikilink")、[快書](../Page/快書.md "wikilink")、[京韻大鼓](../Page/京韻大鼓.md "wikilink")、[單弦](../Page/單弦.md "wikilink")、[太平歌詞及](../Page/太平歌詞.md "wikilink")[京劇](../Page/京劇.md "wikilink")、[舞台劇等](../Page/舞台劇.md "wikilink")。

### 在香港的發展

[香港自古以来就有流传于](../Page/香港.md "wikilink")[中原地区的](../Page/中原地区.md "wikilink")[北派相声](../Page/北派相声.md "wikilink")，早在清朝时期，来自[中国大陆的文人](../Page/中国大陆.md "wikilink")、说书人就把相声这门传统艺术带到[南粤和](../Page/广东省.md "wikilink")[香港](../Page/香港.md "wikilink")。

随着历史原因，[香港被割让为](../Page/香港.md "wikilink")[英国殖民地](../Page/英国殖民地.md "wikilink")。自此之后，相声在香港的发展进入了独特的本土化时期。民国初年，香港的相声主要出现在街头卖艺中，专门的相声艺人大多是以[说书及](../Page/说书.md "wikilink")[口技等杂耍艺人为主](../Page/口技.md "wikilink")。他们大多来自社会的各个阶层，懂得看书，喜欢聊天，因此运用相声的形式谋生。

踏入四五十年代，[香港电影开始急速发展](../Page/香港电影.md "wikilink")，相声艺术也开始融入进新兴媒体。在显存的无数旧香港电影拷贝中，我们可以看到[单口相声](../Page/单口相声.md "wikilink")、[对口相声的表演形式](../Page/对口相声.md "wikilink")，开始融入进早期的[香港电影中](../Page/香港电影.md "wikilink")，大多以演员独白、互相调侃的表演形式出现。

1957年，全球第一家[华语电视媒体诞生](../Page/华语电视.md "wikilink")，就是现今的[亚洲电视前身](../Page/亚洲电视.md "wikilink")[丽的电视](../Page/丽的电视.md "wikilink")。相声开始跃进电视媒体，成为[综艺节目的固定表演项目](../Page/综艺节目.md "wikilink")。其后1967年，[香港无线电视](../Page/香港无线电视.md "wikilink")（[TVB](../Page/TVB.md "wikilink")）诞生，全球最长寿的[综艺节目](../Page/综艺节目.md "wikilink")《[欢乐今宵](../Page/欢乐今宵.md "wikilink")》启播，相声艺术开始在电视综艺节目中以更广泛的变种形式出现，如主持人的讲稿中，节目与节目间的串场等。

### 在马新地区的发展

兩岸分治之后，一部分中国南部的演出团体前往[马来亚地区发展](../Page/英屬馬來亞.md "wikilink")（当时[新加坡](../Page/新加坡.md "wikilink")，[马来西亚尚未独立](../Page/马来西亚.md "wikilink")）。相声艺人[冯翔](../Page/冯翔.md "wikilink")、[白言](../Page/白言.md "wikilink")、[路丁在马新地区表演相声](../Page/路丁.md "wikilink")。因为马新地区所独有的多元语言环境使得“马新相声”较中國大陸的相声和台湾相声别具一格，但也因为了中文并非主流语言的问题使得马新相声界的职业演员很少。

## 相声的分类

相声按演员人数分可分为：

  - [单口相声](../Page/单口相声.md "wikilink")
      - 长篇单口相声，通常分为数次表演，类似于[评书](../Page/评书.md "wikilink")，最早有“八大棍”之称，指的就是从评书中抽取的八段互不相关的段子。但单口相声更多注重笑料。
  - [对口相声](../Page/对口相声.md "wikilink")，演员人数为2人
  - [群口相声](../Page/群口相声.md "wikilink")，演员人数在3人或3人以上

按内容功能分类，可分为：

  - [讽刺型相声](../Page/讽刺.md "wikilink")：可以讽刺自己或者别人，如[侯宝林的](../Page/侯宝林.md "wikilink")《[夜行记](../Page/夜行记.md "wikilink")》（讽刺不遵守[交通规则的人](../Page/交通规则.md "wikilink")）、[姜昆和](../Page/姜昆.md "wikilink")[李文华的](../Page/李文华_\(相声演员\).md "wikilink")《如此[照相](../Page/照相.md "wikilink")》（讽刺[文革时期的社会现象](../Page/文革.md "wikilink")）。
  - 歌颂型相声：如[马季的](../Page/马季.md "wikilink")《新〈桃花源记〉》（歌颂[社会主义新农村](../Page/社会主义.md "wikilink")）、[侯跃文的](../Page/侯跃文.md "wikilink")《[京九演义](../Page/京九演义.md "wikilink")》（歌颂[京九铁路的建设者](../Page/京九铁路.md "wikilink")）。
  - 娱乐型相声：《说方言》、《爱情歌曲》之类。

**按著作时代分类，可分为：**

  - [传统相声](../Page/传统相声.md "wikilink")：清末民初时期
  - [新相声](../Page/新相声.md "wikilink")：1949年之后
  - [当代相声](../Page/当代相声.md "wikilink")：1980年代后期之后

**按流派分类，可分为：** \[9\]

  - 马派相声：代表人物是[马三立](../Page/马三立.md "wikilink")、[马志明](../Page/馬志明_\(相聲演員\).md "wikilink")。
  - 常派相声：代表人物是[常连安](../Page/常连安.md "wikilink")、[常宝堃](../Page/常宝堃.md "wikilink")。
  - 侯派相声：代表人物是[侯宝林](../Page/侯宝林.md "wikilink")。
  - 刘派相声：代表人物是[刘宝瑞](../Page/刘宝瑞.md "wikilink")。

## 艺术特色

### 四大基本功

说、学、逗、唱是相声演员的四大基本功。

  - 说：讲故事，还有说话和铺垫的方式。主要是说一些大笑话，小笑话，[绕口令等等](../Page/绕口令.md "wikilink")。
  - 学：包括各种口技,各种人物说话、各地[方言](../Page/方言.md "wikilink")、和其他声音。也包括模仿特定人物的“唱”和动作表情等
  - 逗：制造笑料，但逗是相声的灵魂。
  - 唱：本门唱为太平歌词。其他的唱如京剧，评戏，梆子等为学唱。

又傳統上，相声艺人把相声的基本功细分为十三门，分别是：

  - [要钱](../Page/要钱.md "wikilink")
  - [口技](../Page/口技.md "wikilink")
  - [数来宝](../Page/数来宝.md "wikilink")
  - [太平歌词](../Page/太平歌词.md "wikilink")
  - [白沙撒字](../Page/白沙撒字.md "wikilink")
  - [单口相声](../Page/单口相声.md "wikilink")
  - [逗哏](../Page/逗哏.md "wikilink")
  - [捧哏](../Page/捧哏.md "wikilink")
  - [群口相声](../Page/群口相声.md "wikilink")
  - [怯口](../Page/怯口.md "wikilink")/[倒口](../Page/倒口.md "wikilink")
  - [柳活](../Page/柳活.md "wikilink")
  - [贯口](../Page/贯口.md "wikilink")
  - [开场小唱](../Page/开场小唱.md "wikilink")

### 術語

  - [定场诗](../Page/定场诗.md "wikilink")
  - [逗哏](../Page/逗哏.md "wikilink")
  - [捧哏](../Page/捧哏.md "wikilink")
  - [包袱](../Page/包袱_\(相聲\).md "wikilink")：相聲中的笑料
  - [柳活](../Page/柳活.md "wikilink")：以學唱（戲劇）為主的相聲
  - [腿子活](../Page/腿子活.md "wikilink")：在相聲表演中，演員為表演戲劇，帶點小化妝，分包趕角，進入角色來表演，之後還要退出來敘事的段子。
  - [貫口](../Page/貫口.md "wikilink")（活）：大段連貫且富於節奏性的台詞，以《[大保鏢](../Page/大保鏢.md "wikilink")》和《[文章會](../Page/文章會.md "wikilink")》為典型，相聲行內有「文怕《文章會》，武怕《大保鏢》」。
  - [怯口活（怯口）](../Page/怯口活（怯口）.md "wikilink")：運用方言或外語表演，如：[豆腐堂會](../Page/豆腐堂會.md "wikilink")、[山西家信等](../Page/山西家信.md "wikilink")。
  - [現掛](../Page/現掛.md "wikilink")（[砸掛](../Page/砸掛.md "wikilink")，抓哏）：現場抓緊取題材引起笑聲

### 道具

  - 一桌
  - 二椅
  - 五子：
      - [扇子](../Page/扇子.md "wikilink")：作為模擬其它道具的物品，例如馬鞭、毛筆等，亦可寫小抄。
      - [醒子](../Page/醒子.md "wikilink")：来自[评书](../Page/评书.md "wikilink")，经常在长篇单口相声中也会用到。
      - [折葉子](../Page/折葉子.md "wikilink")
      - [絹子](../Page/絹子.md "wikilink")
      - [玉子](../Page/玉子_\(樂器\).md "wikilink")：唱太平歌词时伴奏的乐器，一般是两个竹板。

## 相关表演形式

  - [双簧](../Page/双簧.md "wikilink")
  - [口技](../Page/口技.md "wikilink")
  - [开场小唱](../Page/开场小唱.md "wikilink")
  - [太平歌词](../Page/太平歌词.md "wikilink")
  - [參軍戲](../Page/參軍戲.md "wikilink")
  - [落語](../Page/落語.md "wikilink")
  - [答嘴鼓](../Page/答嘴鼓.md "wikilink")
  - [滑稽戲](../Page/滑稽戲.md "wikilink")
  - [栋笃笑](../Page/栋笃笑.md "wikilink")
  - [漫才](../Page/漫才.md "wikilink")
  - [雙人搭檔](../Page/雙人搭檔.md "wikilink")

## 相声师承关系总谱

## 著名演员

### 中国大陆著名相声演员

  - 第一代相声演员：[张三禄](../Page/张三禄.md "wikilink")
  - 第二代相声演员：[朱绍文](../Page/朱绍文.md "wikilink")(窮不怕)、[阿彦涛](../Page/阿彦涛.md "wikilink")、[沈春和等](../Page/沈春和.md "wikilink")
  - 第三代相声演员：[恩绪](../Page/恩绪.md "wikilink")、[徐长福等](../Page/徐长福.md "wikilink")
  - 第四代相声演员：[裕德隆](../Page/裕德隆.md "wikilink")、[刘德智](../Page/刘德智.md "wikilink")、[李德祥](../Page/李德祥.md "wikilink")、[李德钖](../Page/李德钖.md "wikilink")(万人迷)、[张德泉](../Page/张德泉.md "wikilink")(张麻子)、[周德山](../Page/周德山.md "wikilink")(周蛤蟆)、[马德禄](../Page/马德禄.md "wikilink")、[焦德海等](../Page/焦德海.md "wikilink")
  - 第五代相声演员：[张寿臣](../Page/张寿臣.md "wikilink")、[常连安](../Page/常连安.md "wikilink")、[郭荣起](../Page/郭荣起.md "wikilink")、[马三立](../Page/马三立.md "wikilink")、[郭启儒](../Page/郭启儒.md "wikilink")、[朱阔泉](../Page/朱阔泉.md "wikilink")、[焦少海等](../Page/焦少海.md "wikilink")
  - 第六代相声演员：[侯宝林](../Page/侯宝林.md "wikilink")、[张永熙](../Page/张永熙.md "wikilink")、[杨少华](../Page/杨少华.md "wikilink")、[刘宝瑞](../Page/刘宝瑞.md "wikilink")、[赵佩茹](../Page/赵佩茹.md "wikilink")、[郭全宝](../Page/郭全宝.md "wikilink")、[王凤山](../Page/王凤山.md "wikilink")、[马志明](../Page/马志明_\(相声演员\).md "wikilink")
    、[尹笑声](../Page/尹笑声.md "wikilink")、[常宝堃](../Page/常宝堃.md "wikilink")、[常宝华](../Page/常宝华.md "wikilink")、[常宝霆](../Page/常宝霆.md "wikilink")、[李文华](../Page/李文华_\(相声演员\).md "wikilink")、[杨振华等](../Page/杨振华.md "wikilink")
  - 第七代相声演员：[马季](../Page/马季.md "wikilink")、[侯耀文](../Page/侯耀文.md "wikilink")、[石富宽](../Page/石富宽.md "wikilink")、[苏文茂](../Page/苏文茂.md "wikilink")、[唐杰忠](../Page/唐杰忠.md "wikilink")、[张文顺](../Page/张文顺.md "wikilink")、[李伯祥](../Page/李伯祥.md "wikilink")、[杜国芝](../Page/杜国芝.md "wikilink")、[马志存](../Page/马志存.md "wikilink")、[高英培](../Page/高英培.md "wikilink")、[师胜杰](../Page/师胜杰.md "wikilink")、[常贵田](../Page/常贵田.md "wikilink")、[赵振铎](../Page/赵振铎.md "wikilink")、[范振钰](../Page/范振钰.md "wikilink")、[王文林](../Page/王文林.md "wikilink")、[牛群](../Page/牛群.md "wikilink")、[侯耀华等](../Page/侯耀华.md "wikilink")
  - 第八代相声演员：[姜昆](../Page/姜昆.md "wikilink")、[冯巩](../Page/冯巩.md "wikilink")、[赵炎](../Page/赵炎.md "wikilink")、[刘伟](../Page/刘伟_\(相声演员\).md "wikilink")、[笑林](../Page/笑林_\(相声演员\).md "wikilink")、[王谦祥](../Page/王谦祥.md "wikilink")、[李增瑞](../Page/李增瑞.md "wikilink")、[李金斗](../Page/李金斗.md "wikilink")、[郭德纲](../Page/郭德纲.md "wikilink")、[郑宏伟](../Page/郑宏伟.md "wikilink")、[魏源成](../Page/魏源成.md "wikilink")、[于谦](../Page/于謙_\(相聲演員\).md "wikilink")、[王自健](../Page/王自健.md "wikilink")、[王平](../Page/王平_\(相声演员\).md "wikilink")、[高峰](../Page/高峰_\(相声演员\).md "wikilink")、[徐德亮](../Page/徐德亮.md "wikilink")、[李菁](../Page/李菁_\(相聲演員\).md "wikilink")、[王玥波](../Page/王玥波.md "wikilink")、[奇志等](../Page/杨奇志.md "wikilink")
  - 第九代相声演员：[何伟](../Page/何伟.md "wikilink")、[张云雷](../Page/张云雷.md "wikilink")、[大山](../Page/大山_\(演員\).md "wikilink")、[大兵](../Page/任军.md "wikilink")、[刘云天](../Page/刘云天.md "wikilink")、[苗阜](../Page/苗阜.md "wikilink")、[岳云鹏](../Page/岳云鹏.md "wikilink")、[赵卫国](../Page/赵卫国_\(相声演员\).md "wikilink")、等

### 台灣著名相聲演員

由於台灣相聲表演者較多為相聲愛好者而未有拜師，因此許多人在相聲系譜內並未列入。但許多人造詣甚高，被尊為大師、[人間國寶](../Page/人間國寶.md "wikilink")。

  - 早期演員：[魏龍豪](../Page/魏龍豪.md "wikilink")、[吳兆南](../Page/吳兆南.md "wikilink")、[陳逸安](../Page/陳逸安.md "wikilink")、周胖子(周志泉)、[丁仲](../Page/丁仲.md "wikilink")、張三、金超群等
  - [吳兆南相聲劇藝社](../Page/吳兆南相聲劇藝社.md "wikilink")：[吳兆南](../Page/吳兆南.md "wikilink")、[江南](../Page/江南.md "wikilink")、[侯冠群](../Page/侯冠群.md "wikilink")、[郎祖筠](../Page/郎祖筠.md "wikilink")、[劉增鍇](../Page/劉增鍇.md "wikilink")、[劉爾金](../Page/劉爾金.md "wikilink")、[樊光耀](../Page/樊光耀.md "wikilink")、[姬天語](../Page/姬天語.md "wikilink")、[劉越逖](../Page/劉越逖.md "wikilink")、[林明等](../Page/林明.md "wikilink")

<!-- end list -->

  - [台北曲藝團](../Page/台北曲藝團.md "wikilink")：[劉增鍇](../Page/劉增鍇.md "wikilink")、[粟奕倩](../Page/粟奕倩.md "wikilink")、[葉怡均](../Page/葉怡均.md "wikilink")、[朱德剛](../Page/朱德剛.md "wikilink")、[劉越逖](../Page/劉越逖.md "wikilink")(已改名劉士民)、[樊光耀](../Page/樊光耀.md "wikilink")、郭志傑、[謝小玲](../Page/謝小玲.md "wikilink")、[陳慶昇](../Page/陳慶昇.md "wikilink")、[黃逸豪](../Page/黃逸豪.md "wikilink")、[吳佩凌](../Page/吳佩凌.md "wikilink")、[魏竹嶢](../Page/魏竹嶢.md "wikilink")

<!-- end list -->

  - [相聲瓦舍](../Page/相聲瓦舍.md "wikilink")：[馮翊綱](../Page/馮翊綱.md "wikilink")、[宋少卿](../Page/宋少卿.md "wikilink")、[黃士偉](../Page/黃士偉.md "wikilink")(第七代相聲演員)等
  - [漢霖民俗說唱藝術團](../Page/漢霖民俗說唱藝術團.md "wikilink")：[王振全](../Page/王振全.md "wikilink")、[王漢霖](../Page/王漢霖.md "wikilink")

### 中国大陆著名吳语相声演员

  - [錢程](../Page/錢程.md "wikilink")、[杨华生](../Page/杨华生.md "wikilink")、[王桂林](../Page/王桂林.md "wikilink")、[姚慕雙](../Page/姚慕雙.md "wikilink")、[周柏春](../Page/周柏春.md "wikilink")、[舒悦](../Page/舒悦.md "wikilink")。

### 中国大陆著名粤语相声演员

  - [张悦楷](../Page/张悦楷.md "wikilink")、[林兆明](../Page/林兆明.md "wikilink")、杨达、[黄俊英](../Page/黄俊英_\(演员\).md "wikilink")、何宝文、吴克、蔡传兴等

### 中国大陆著名湘语相声演员

  - [大兵](../Page/任军.md "wikilink")、[杨五六](../Page/杨五六.md "wikilink")、[周卫星等](../Page/周卫星.md "wikilink")

## 經典劇目

  - 傳統相聲

《[豆腐堂會](../Page/豆腐堂會.md "wikilink")》、《[大保鏢](../Page/大保鏢.md "wikilink")》、《[文章會](../Page/文章會.md "wikilink")》（与《大保镖》合称《文武双全》）、《[誇住宅](../Page/誇住宅.md "wikilink")》、《[開粥廠](../Page/開粥廠.md "wikilink")》、《[當行論](../Page/當行論.md "wikilink")》、《[報菜名](../Page/報菜名.md "wikilink")》、《[地理圖](../Page/地理圖.md "wikilink")》、《[黃鶴樓](../Page/黄鹤楼\(相声\).md "wikilink")》、《[八大改行](../Page/八大改行.md "wikilink")》、《[八扇屏](../Page/八扇屏.md "wikilink")》、《[三近視](../Page/三近視.md "wikilink")》、《[白事會](../Page/白事會.md "wikilink")》、《[扒馬褂](../Page/扒馬褂.md "wikilink")》、《[論捧逗](../Page/論捧逗.md "wikilink")》、《[繞口令](../Page/繞口令.md "wikilink")》、《[關公戰秦瓊](../Page/關公戰秦瓊.md "wikilink")》、《[蛤蟆鼓](../Page/蛤蟆鼓.md "wikilink")》、《[打牌論](../Page/打牌論.md "wikilink")》、《[君臣斗](../Page/君臣斗.md "wikilink")》、《[珍珠翡翠白玉湯](../Page/珍珠翡翠白玉湯.md "wikilink")》、《[山東人鬥法](../Page/山東人鬥法.md "wikilink")》、《[假行家](../Page/假行家.md "wikilink")》、《[金剛腿](../Page/金剛腿.md "wikilink")》、《[怯教書](../Page/怯教書.md "wikilink")》、《[反七口](../Page/反七口.md "wikilink")》、《[黄半仙](../Page/黄半仙.md "wikilink")》

  - 現代相聲

《[似曾相識的人](../Page/似曾相識的人.md "wikilink")》、《[如此照相](../Page/如此照相.md "wikilink")》、《[81層樓](../Page/81層樓.md "wikilink")》、《[買猴](../Page/買猴.md "wikilink")》、《[釣魚](../Page/釣魚.md "wikilink")》、《[夜行記](../Page/夜行記.md "wikilink")》、《[訓徒](../Page/訓徒.md "wikilink")》、《[五官爭功](../Page/五官爭功.md "wikilink")》、《[虎口遐想](../Page/虎口遐想.md "wikilink")》、《[宇宙牌香菸](../Page/宇宙牌香菸.md "wikilink")》、《[小偷公司](../Page/小偷公司.md "wikilink")》、《[老鼠夜話](../Page/老鼠夜話.md "wikilink")》、《[很難說的國語](../Page/很難說的國語.md "wikilink")》等。

## 注释

## 参考资料

  - 《相声艺术漫谈》，马季

## 相關條目

  - [曲艺](../Page/曲艺.md "wikilink")
  - [喜剧小品](../Page/喜剧小品.md "wikilink")
  - [评书](../Page/评书.md "wikilink")
  - [落語](../Page/落語.md "wikilink")
  - [漫才](../Page/漫才.md "wikilink")

[相聲](../Category/相聲.md "wikilink")
[表演藝術](../Category/中華民國無形文化資產傳統藝術類.md "wikilink")

1.  [連闊如](../Page/連闊如.md "wikilink")《江湖叢談》第七章〈相聲口技〉：「就以相聲這種藝術說吧，其發源就係由北京產生的。……滿人入主中華，康乾時代……在斯時最盛行的為八角鼓了，相聲這種藝術就是由八角鼓中產生的。按，八角鼓之源流始於清朝中葉，乾隆時代有大小金川之戰……當奏曲時所用之八角鼓，其八角即暗示八旗之意。」

2.  [中国近代相声历史资料](http://news.163.com/06/1220/20/32QH2IKK00011244.html)[网易](../Page/网易.md "wikilink")，2006-12-20，2007-5-2造访。

3.  連闊如《江湖叢談》第七章〈相聲口技〉：「在那時八角鼓之有名丑角兒為張三祿。……後因其性怪僻，不易搭班，受人排擠，……張三祿不願說八角鼓兒，自稱其藝為相聲。……張三祿乃相聲發始創藝之一，其後相聲之派別分三大派，一為朱派，二為阿派，三為沈派。朱派係「窮不怕」，其名為朱少文。……雖稱朱沈阿三大派，但沈二的門戶不旺，……阿剌三的支派也是和沈派相同的。如今平津等地說相聲的藝人，十有七八是朱派流傳的。」

4.
5.  [张立林《相声宗师张寿臣》](http://www.cnquyi.com/news_detail.php?catalogid=10&&productid=33135&&version=cn)
     曲艺网，2007-03-14，2007-5-2造访

6.  [记张寿臣先生二三事](http://www.newxs.net/Article/xsbn/xsmj/2008136865.htm)“听相声”网站，叶利中，
    2005-11-5，2007-5-2造访。

7.  [《相声大赛看不到相声》](http://www.hnol.net/content/2006-10/10/content_4612240.htm)，《[三湘都市报](../Page/三湘都市报.md "wikilink")》2006年10月10日。

8.

9.