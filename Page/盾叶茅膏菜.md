**盾叶茅膏菜**（[学名](../Page/学名.md "wikilink")：**），又名-{zh-cn:**茅膏菜**;zh-hk:**盾葉茅膏菜**;zh-tw:**盾葉茅膏菜**}-、**盾葉毛氈苔**、**石龙牙草**、**石持草**，是[多年生](../Page/多年生.md "wikilink")[草本](../Page/草本.md "wikilink")[-{茅膏菜}-属](../Page/茅膏菜属.md "wikilink")[食虫植物](../Page/食虫植物.md "wikilink")。具有明显的[茎](../Page/茎.md "wikilink")，株高10-30厘米，[叶为半月形](../Page/叶.md "wikilink")，上面生满能分泌粘液的[腺毛](../Page/腺毛.md "wikilink")。当小虫落上被粘液粘上，叶子卷缩，分泌[消化液将虫子消化](../Page/消化液.md "wikilink")，是干旱少养分土壤中生长的植物为自己补充营养的一种方式。花白色或淡红色，螺状[聚伞花序](../Page/聚伞花序.md "wikilink")。

盾叶茅膏菜為攀附性的[球根毛氈苔](../Page/球根毛氈苔.md "wikilink")，是分布最廣的種類。[種加詞](../Page/種加詞.md "wikilink")「*peltata*」來源於[拉丁文](../Page/拉丁文.md "wikilink")「盾形」之意，描述莖上長出的葉片形狀。此品種雖然特色鮮明，但又和其他幾個相近的種類混在一起，例如在澳洲就有至少4個茅膏菜的型：-{zh-cn:盾狀盾叶茅膏菜;zh-hk:盾葉茅膏菜;zh-tw:盾葉茅膏菜}-（）（[自動名](../Page/自動名.md "wikilink")）、-{zh-cn:耳狀盾叶茅膏菜;zh-hk:耳葉茅膏菜;zh-tw:耳葉茅膏菜}-（）、多叶-{茅膏菜}-（），又稱為盾叶茅膏菜多叶变种（）、小型-{茅膏菜}-（），又稱為盾叶茅膏菜小型变种（）。

球根毛氈苔發展出[夏休機制](../Page/夏休.md "wikilink")，利用地下的球根度過度乾旱的夏季。地上部的型態可以分為蓮座及直立，茅膏菜為直立型，花朵單枝或分枝。

## 描述

盾叶茅膏菜是多年生的[球根型草本植物](../Page/球根.md "wikilink")，球根存在於地下約4-6公分處，[地上部的高度根據不同類型](../Page/地上部.md "wikilink")，約5-50公分。-{zh-cn:耳狀盾叶茅膏菜;zh-hk:耳葉茅膏菜;zh-tw:耳葉茅膏菜}-的最強健，直立莖高度可達50公分，多叶茅膏菜則是最短的，只有5-10公分。茅膏菜具有明顯的[蓮座葉](../Page/蓮座葉.md "wikilink")，尤以多叶茅膏菜最為顯著，成熟的-{zh-cn:耳狀盾叶茅膏菜;zh-hk:耳葉茅膏菜;zh-tw:耳葉茅膏菜}-植株的蓮座葉則幾乎不被注意。盾叶茅膏菜的[直立莖多為單幹或者偶有分枝](../Page/直立莖.md "wikilink")，花朵顏色各異，白色或亮粉紅較多。植株顏色也有所差異，多叶茅膏菜就算接受全日照依然呈現明亮的草綠色，-{zh-cn:耳狀盾叶茅膏菜;zh-hk:耳葉茅膏菜;zh-tw:耳葉茅膏菜}-具有多種紅色系的色調，小型茅膏菜則是橘色或暗紅。

## 棲地與分布

盾叶茅膏菜通常發現於[森林中平坦開闊的](../Page/森林.md "wikilink")[灌木區](../Page/灌木區.md "wikilink")、[再生林等地區邊緣的](../Page/再生林.md "wikilink")[草叢](../Page/草叢.md "wikilink")，或者路邊修剪過的地方。土壤多為鬆軟細密的[黏土](../Page/黏土.md "wikilink")、[泥炭](../Page/泥炭.md "wikilink")、[砂質土等](../Page/砂質土.md "wikilink")，冬季濕潤夏季乾燥。盾叶茅膏菜的分布範圍廣，並且存在多種個體，可以在東、南、西南[澳洲](../Page/澳洲.md "wikilink")、[塔斯馬尼亞](../Page/塔斯馬尼亞.md "wikilink")、紐西蘭[北島](../Page/北島.md "wikilink")、[東南亞](../Page/東南亞.md "wikilink")、[印度](../Page/印度.md "wikilink")、[云南](../Page/云南.md "wikilink")、[四川](../Page/四川.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台湾](../Page/台湾.md "wikilink")、[日本](../Page/日本.md "wikilink")[關東以西等地發現其蹤跡](../Page/关东地方.md "wikilink")。

## 分類學

盾叶茅膏菜於1797年首次由 Carl Peter Thunberg
發表，此一品種分布廣泛，不同棲地各自具有不同的習性，因而冒出了一堆[同物異名和](../Page/同物異名.md "wikilink")[種下分類群](../Page/種下分類群.md "wikilink")，包含[變種](../Page/變種.md "wikilink")、[亞種等](../Page/亞種.md "wikilink")。許多亞種經過研究，發現是同一品種的同物異名而取消了，至今僅存-{zh-cn:盾狀盾叶茅膏菜;zh-hk:盾葉茅膏菜;zh-tw:盾葉茅膏菜}-（）及-{zh-cn:耳狀盾叶茅膏菜;zh-hk:耳葉茅膏菜;zh-tw:耳葉茅膏菜}-（）兩種[分類地位](../Page/分類地位.md "wikilink")，後者先於1848年由
Jules Émile Planchon 以耳叶-{茅膏菜}-（）的名稱發表，後來於1981年由 Barry John Conn
將之歸類為盾叶茅膏菜的亞種，但是至今仍有部分學者認為此二品種應是彼此獨立的。兩個亞種的差異可以從[種子形狀和](../Page/種子.md "wikilink")[萼片上的軟毛來區分](../Page/萼片.md "wikilink")，盾叶茅膏菜的種子卵形，萼片具軟毛；耳叶-{茅膏菜}-種子長條形，萼片光滑。

另一方面，多叶-{茅膏菜}-（）和小型-{茅膏菜}-（）也於1982年被 Marchant
降級成盾叶茅膏菜的[同物異名](../Page/同物異名.md "wikilink")，但是塔斯馬尼亞植物標本館和澳洲物種名錄仍主張此二品種應為獨立品種。

多叶-{茅膏菜}-與典型的盾叶茅膏菜最大的差異在其具有明顯的[蓮座葉](../Page/蓮座葉.md "wikilink")，呈亮綠色新月形，葉片較大，並具有許多側生短莖。相較之下典型的盾叶茅膏菜的蓮座葉沒有這麼明顯，直立莖也沒有分枝。多叶-{茅膏菜}-生長環境被限制在[草地](../Page/草地.md "wikilink")、[疏林地等被雜草掩蓋的地區](../Page/疏林地.md "wikilink")。

小型-{茅膏菜}-的植株與典型的盾叶茅膏菜長得很像，但是體型較小，可以從它紅色的莖葉來區分。在澳洲東南方的塔斯馬尼亞島，小型-{茅膏菜}-生長在潮濕的泥炭地，而且與盾叶茅膏菜和多叶-{茅膏菜}-的冬春生長的習性不同，它於晚春至夏季生長。

## 栽培

盾叶茅膏菜是最容易栽培的球根毛之一，它和一般球毛略有不同。一般的球毛於濕冷的季節生長、乾熱的季節結球[休眠](../Page/休眠.md "wikilink")，休眠期間水分過多的話球根容易腐爛，但是盾叶茅膏菜在夏季能夠耐得住較潮濕的環境，這是它被歸類於好種球毛的原因之一。

## 外部链接

  -
<File:Drosera> pelata flower.jpg|日本型
<small>[成東－東金食虫植物群落](../Page/成東－東金食虫植物群落.md "wikilink")</small>
<File:Droseraceae> peltata.jpg|日本型，捕蠅 <File:Droseraceae> peltata caught
a fly.jpg|日本型的葉，近观 <File:Drosera> peltata basal rosette.jpg|根生葉簇
[File:DroseraPeltataLamina.jpg|葉，近观](File:DroseraPeltataLamina.jpg%7C葉，近观)
[File:DroseraPeltataFlora.jpg|花，近观](File:DroseraPeltataFlora.jpg%7C花，近观)

[peltata](../Category/茅膏菜属.md "wikilink")