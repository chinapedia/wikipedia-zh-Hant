**眞子內親王**（），[日本皇族](../Page/日本皇室.md "wikilink")。[秋篠宮文仁親王與](../Page/秋篠宮文仁親王.md "wikilink")[秋篠宮文仁親王妃紀子長女](../Page/秋篠宮妃紀子.md "wikilink")，[今上天皇](../Page/今上天皇.md "wikilink")[明仁的第一個孫女](../Page/明仁.md "wikilink")，有一妹（[秋篠宮佳子內親王](../Page/秋篠宮佳子內親王.md "wikilink")）和一弟（[秋篠宮悠仁親王](../Page/秋篠宮悠仁.md "wikilink")）。使用的徽印是[木香薔薇](../Page/木香薔薇.md "wikilink")。

## 生平

[Princess_Mako_and_Princess_Kako_at_the_Tokyo_Imperial_Palace_(cropped).jpg](https://zh.wikipedia.org/wiki/File:Princess_Mako_and_Princess_Kako_at_the_Tokyo_Imperial_Palace_\(cropped\).jpg "fig:Princess_Mako_and_Princess_Kako_at_the_Tokyo_Imperial_Palace_(cropped).jpg")
1991年10月23日，秋篠宮文仁親王與親王妃紀子的首名孩子於[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")[皇居](../Page/皇居.md "wikilink")，日本皇室同日為眞子舉辦
。1991年10月29日，日本皇室為眞子舉辦
，秋篠宮文仁親王親自取名為「眞子」。1998年4月，[學習院初等科入學](../Page/學習院大學.md "wikilink")。2004年3月，學習院初等科畢業。2004年4月，學習院女子中等科入學。2007年3月，學習院女子中等科畢業。2007年4月，學習院女子高等科入學。

在[日本媒體報導下](../Page/日本媒體.md "wikilink")，網路上流出她的照片，讓不少[2ch](../Page/2ch.md "wikilink")[網路論壇的](../Page/網路論壇.md "wikilink")[網民驚豔](../Page/網民.md "wikilink")，並引起了之後許多以眞子內親王為主題創作的[二次元作品](../Page/二次元.md "wikilink")；不過由於這些作品的成分複雜，這個[網絡爆紅現象引起日本警方注意](../Page/網絡爆紅現象.md "wikilink")，在2ch鬧出不小的風波。

2009年11月5日，宮內廳宣布眞子通過[國際基督教大學](../Page/國際基督教大學.md "wikilink")
（AO入試）。2010年3月，眞子從學習院女子高等科畢業。2010年4月，眞子入讀國際基督教大學教養學部藝術及科學學科，是第一位就讀國際基督教大學的日本皇室成員；對於信奉[佛教與](../Page/佛教.md "wikilink")[神道教的日本皇室成員來說](../Page/神道教.md "wikilink")，眞子從學習院高等科畢業之後決定不直升[學習院大學而透過AO入試考上國際基督教大學](../Page/學習院大學.md "wikilink")，頗為罕見。2010年7月遠赴[愛爾蘭進行語言研修](../Page/愛爾蘭.md "wikilink")。

2011年10月23日，因其已成年，故授予[寶冠大綬章](../Page/寶冠章.md "wikilink")。2011年（[平成二十三年](../Page/平成.md "wikilink")）11月，為了向[皇室歷代祖先報告自己己經成年](../Page/日本皇室.md "wikilink")，首次前往[東京](../Page/東京.md "wikilink")[八王子皇室陵墓參拜曾祖父](../Page/八王子.md "wikilink")[昭和天皇及曾祖母](../Page/昭和天皇.md "wikilink")[香淳皇后的](../Page/香淳皇后.md "wikilink")[武藏野陵和高祖父](../Page/武藏野陵.md "wikilink")[大正天皇及高祖母](../Page/大正天皇.md "wikilink")[貞明皇后的](../Page/貞明皇后_\(日本\).md "wikilink")[多摩陵](../Page/武藏陵區.md "wikilink")。2012年（平成二十四年）9月，前往[英國](../Page/英國.md "wikilink")[愛丁堡大學留學](../Page/愛丁堡大學.md "wikilink")，2014年轉至[萊斯特大學進修](../Page/萊斯特大學.md "wikilink")。

2017年9月4日，日本宮內廳宣布真子內親王與大學同學小室圭訂婚。\[1\]婚禮原定於2018年11月舉行，但宮內廳於2018年2月6日突然宣佈因為準備不周，婚禮推遲至[2020年](../Page/2020年.md "wikilink")。\[2\]

## 世系

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>秋篠宮眞子內親王</strong></p></td>
<td style="text-align: center;"><p><strong>父：</strong><br />
<a href="../Page/秋篠宮文仁親王.md" title="wikilink">秋篠宮文仁親王</a></p></td>
<td style="text-align: center;"><p><strong>祖父：</strong><br />
日本天皇<a href="../Page/明仁.md" title="wikilink">明仁</a></p></td>
<td style="text-align: center;"><p><strong>曾祖父：</strong><br />
<a href="../Page/昭和天皇.md" title="wikilink">昭和天皇</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>曾祖母：</strong><br />
<a href="../Page/香淳皇后.md" title="wikilink">香淳皇后</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>祖母：</strong><br />
<a href="../Page/美智子.md" title="wikilink">美智子</a></p></td>
<td style="text-align: center;"><p><strong>曾祖父：</strong><br />
<a href="../Page/正田英三郎.md" title="wikilink">正田英三郎</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>曾祖母：</strong><br />
正田富美子</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>母：</strong><br />
<a href="../Page/秋篠宮妃紀子.md" title="wikilink">秋篠宮妃紀子</a></p></td>
<td style="text-align: center;"><p><strong>祖父：</strong><br />
<a href="../Page/川嶋辰彥.md" title="wikilink">川嶋辰彥</a></p></td>
<td style="text-align: center;"><p><strong>曾祖父：</strong><br />
川嶋考彥</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>曾祖母：</strong><br />
川嶋紀子</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>祖母：</strong><br />
川嶋和代</p></td>
<td style="text-align: center;"><p><strong>曾祖父：</strong><br />
杉本嘉助</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>曾祖母：</strong><br />
杉本榮子</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 參考

## 外部链接

  - [秋篠宮眞子殿下畫像保管庫](http://www.yuko2ch.net/mako/)

[Category:日本近現代內親王](../Category/日本近現代內親王.md "wikilink")
[Category:秋篠宮](../Category/秋篠宮.md "wikilink")
[Category:國際基督教大學校友](../Category/國際基督教大學校友.md "wikilink")
[Category:愛丁堡大學校友](../Category/愛丁堡大學校友.md "wikilink")
[Category:萊斯特大學校友](../Category/萊斯特大學校友.md "wikilink")

1.  [日本宮內廳宣布
    真子公主與小室圭訂婚\[影\]](http://www.cna.com.tw/news/firstnews/201709030043-1.aspx)
2.