**真人**是[宗教或](../Page/宗教.md "wikilink")[哲學的境界](../Page/哲學.md "wikilink")；具有相當修為的男性[高人](../Page/高人.md "wikilink")、[道士](../Page/道士.md "wikilink")、[僧人亦獲尊稱為](../Page/僧人.md "wikilink")「真人」。相對女性高仙則稱為**元君**。\[1\]

## 簡介

[先秦](../Page/先秦.md "wikilink")[道家学派的](../Page/道家.md "wikilink")[莊子有真人的描述](../Page/莊子.md "wikilink")
，如《[莊子](../Page/莊子_\(書\).md "wikilink").大宗師》：「古之真人，其寢不夢，其覺無憂，其食不甘，其息深深。」《天下》称：「[关尹](../Page/关尹.md "wikilink")、[老子古之博大真人](../Page/老子.md "wikilink")。」[秦始皇受到](../Page/秦始皇.md "wikilink")[方士影響](../Page/方士.md "wikilink")，亦自称“真人”。《史记秦始皇本纪》载：卢生说始皇曰：“臣等求芝奇药仙者常弗遇，类物有害之者。方中，人主时为微行以辟恶鬼，恶鬼辟，真人至。人主所居而人臣知之，则害于神。真人者，入水不濡，入火不贇，陵云气，与天地久长。今上治天下，未能恬俊。原上所居宫毋令人知，然后不死之药殆可得也。”于是始皇曰：“吾慕真人，自谓‘真人’，不称‘朕’。”

[道教興起後](../Page/道教.md "wikilink")，對[祖师多称为真人](../Page/祖师.md "wikilink")。道教尊[庄子为](../Page/庄子.md "wikilink")“南华真人”、[列子为](../Page/列子.md "wikilink")“冲虚真人”；知名的仙人[鍾離權稱](../Page/鍾離權.md "wikilink")「正陽真人」、[呂洞賓稱](../Page/呂洞賓.md "wikilink")「純陽真人」、[劉海蟾稱](../Page/劉海蟾.md "wikilink")「廣陽真人」，其餘[全真道祖师](../Page/全真道.md "wikilink")[王重阳](../Page/王重阳.md "wikilink")、[马丹阳](../Page/马丹阳.md "wikilink")、[丘长春](../Page/丘长春.md "wikilink")、[张伯端等均尊为](../Page/张伯端.md "wikilink")“真人”。[正一道](../Page/正一道.md "wikilink")[張天师](../Page/張天师.md "wikilink")，在[明代改敕](../Page/明代.md "wikilink")“正一嗣教大真人”。道教的[中醫專著](../Page/中醫.md "wikilink")《[黃帝內經](../Page/黃帝內經.md "wikilink")》記載「真人」的境界，《[黃帝內經](../Page/黃帝內經.md "wikilink")·上古天真論篇第一》：「余聞上古有**真人**者，提挈天地，把握[陰陽](../Page/陰陽.md "wikilink")，呼吸精氣，獨立守神，[肌肉若一](../Page/肌肉.md "wikilink")，故能壽敝天地，無有終時，此其道生。」

[佛教的](../Page/佛教.md "wikilink")《[无量壽经](../Page/无量壽经.md "wikilink")》中也有真人的说法。民間對高僧也往往稱真人，一如[上人](../Page/上人.md "wikilink")，如[清水祖師被稱為](../Page/清水祖師.md "wikilink")[清水真人](../Page/清水真人.md "wikilink")。

在[日本](../Page/日本.md "wikilink")，[天武天皇定](../Page/天武天皇.md "wikilink")[八色姓](../Page/八色姓.md "wikilink")，把真人定为第一等授予子孙。

## 參看

  - [至人](../Page/至人.md "wikilink")
  - [中醫](../Page/中醫.md "wikilink")
  - [道教](../Page/道教.md "wikilink")
  - [道家](../Page/道家.md "wikilink")
  - [內丹術](../Page/內丹術.md "wikilink")

## 参考文献

[Category:道家](../Category/道家.md "wikilink")
[Category:道教](../Category/道教.md "wikilink")
[Category:道教人物](../Category/道教人物.md "wikilink")
[Category:道教術語](../Category/道教術語.md "wikilink")
[Category:稱謂](../Category/稱謂.md "wikilink")

1.  明[彭大翼](../Page/彭大翼.md "wikilink")《山堂肆考·女仙》說「男性高仙稱為真人，女性稱為元君」