**真實模式**（）是Intel
[80286和之後的](../Page/80286.md "wikilink")[x86相容](../Page/x86.md "wikilink")[CPU的操作模式](../Page/CPU.md "wikilink")。真實模式的特性是一個20位元的區段記憶體位址空間（意思為只有1
[MB的記憶體可以被定址](../Page/MB.md "wikilink")），可以直接軟體存取[BIOS常式以及周邊硬體](../Page/BIOS.md "wikilink")，沒有任何硬體等級的[記憶體保護觀念或](../Page/記憶體保護.md "wikilink")[多工](../Page/多工.md "wikilink")。所有的[80286系列和之後的x](../Page/80286.md "wikilink")86
CPU都是以真實模式下開機；[80186和早期的CPU僅僅只有一種操作模式](../Page/80186.md "wikilink")，也就是相當於後來晶片的這種真實模式。

286架構導入[保護模式](../Page/保護模式.md "wikilink")，允許硬體等級的記憶體保護。然而要使用這些新的特色，需要額外先前不需要的軟體指令。由於x86微處理機主要的設計規格，是能夠完全地向前相容於針對先前所有x86晶片所撰寫的軟體，因此286晶片的開機是處於'真實模式'—也就是關閉新的記憶體保護特性的模式，所以可以執行針對舊的微處理器所設計的軟體。到現在為止，即使最新的x86
CPU一開始在電源開啟處於真實模式下，也能夠執行針對先前任何晶片所撰寫的軟體。

IBM引进的PC-BIOS与[DOS作業系統](../Page/DOS.md "wikilink")（[MS-DOS](../Page/MS-DOS.md "wikilink"),
[DR-DOS等等](../Page/DR-DOS.md "wikilink")）都是在真實模式下運作。早期的[Microsoft](../Page/Microsoft.md "wikilink")
[Windows版本](../Page/Windows.md "wikilink")（主要地只是[圖形用戶界面外殼](../Page/圖形用戶界面.md "wikilink")，而事實上不是作業系統）也在真實模式下運行，直到[Windows
3.0是第一种在保护模式下运行的Windows系列的操作系统](../Page/Windows_3.x.md "wikilink")。Windows
3.0进一步增强了这方面功能，能夠在真實模式或是保護模式下运行。Windows 3.0在保護模式下有兩種运行"喜好" -
"標準模式"，也就是使用保護模式來運行；而"386-增強模式"，允許使用32位元定址，因此無法在286上執行（儘管都有保護模式，但是286只是16位元晶片；32位元的暫存器在[80386系列中推出](../Page/80386.md "wikilink")）。Windows
3.1不再允许在真實模式下运行，只能在保护模式下运行，因此也是第一個最少需要80286處理器的主流作業環境（不管[Windows/286](../Page/Windows/286.md "wikilink")，因為並非主流產品）。幾乎所有的現今x86作業系統（[Linux](../Page/Linux.md "wikilink")、[Windows
95和之後](../Page/Windows_95.md "wikilink")、[OS/2等等](../Page/OS/2.md "wikilink")）都會在啟動後將CPU切換到保護模式。X86-64CPU上的长模式操作系统，把保护模式也作为启动中的一个踏脚石。

80286的地址总线为24位元宽，即使在真實模式下也是在使用24位元的内存地址读写内存数据。所以80286及以后的CPU以真實模式运行时，需要控制A20地址线是否被使用。

## 參考

  - [IA-32](../Page/IA-32.md "wikilink")
  - [x86](../Page/x86.md "wikilink")
  - [x86組合語言](../Page/x86組合語言.md "wikilink")
  - [保護模式](../Page/保護模式.md "wikilink")
  - [unreal mode](../Page/unreal_mode.md "wikilink")
  - [长模式](../Page/长模式.md "wikilink")
  - [系统管理模式](../Page/系统管理模式.md "wikilink")

[Category:X86架構](../Category/X86架構.md "wikilink")