**真田昌幸**（），日本[戰國時代武將及大名](../Page/日本戰國時代.md "wikilink")，[信濃](../Page/信濃國.md "wikilink")[上田城城主](../Page/上田城.md "wikilink")，[真田幸隆之三男](../Page/真田幸隆.md "wikilink")，其兄為[真田信綱](../Page/真田信綱.md "wikilink")、[真田昌輝](../Page/真田昌輝.md "wikilink")。生下四男七女，當中包括了長男[真田信幸及次男](../Page/真田信之.md "wikilink")[真田幸村](../Page/真田信繁.md "wikilink")。昌幸[正室是](../Page/正室.md "wikilink")[寒松院](../Page/山手殿.md "wikilink")，另有多名[侧室](../Page/侧室.md "wikilink")。官位是「[從五位下](../Page/從五位.md "wikilink")[安房守](../Page/安房國.md "wikilink")」。

## 生平

### 武田信玄家臣

1547年，昌幸出生。7歲時，父親幸隆將他送往甲斐，成為[武田晴信](../Page/武田晴信.md "wikilink")（信玄）的人質。真田昌幸年少時，過繼到信玄母親娘家武藤氏做養子，名為武藤喜兵衛，擔任起[武田信玄的近侍與見習](../Page/武田信玄.md "wikilink")。初陣便是在有著龍爭虎鬥之稱的[第四次川中島之戰](../Page/川中島之戰.md "wikilink")，當時的身份為武田家足輕大將。

1564年，迎娶遠江國領主[尾藤賴忠](../Page/尾藤賴忠.md "wikilink")（[宇多賴忠](../Page/宇多賴忠.md "wikilink")）之女山之手殿為正室。山之手殿為喜兵衛誕下了信幸（信之）及信繁。1569年，武田信玄準備攻擊[北條家](../Page/後北條氏.md "wikilink")，喜兵衛負責來往各部隊的傳達情報的任務。喜兵衛在[三增峠之戰取得了一番槍](../Page/三增峠之戰.md "wikilink")（最多戰功者）的稱號；1572年，參與[三方原之戰](../Page/三方原之戰.md "wikilink")；1574年，父親幸隆病逝，由長兄信綱繼承家督。1575年，[武田勝賴徵召大規模部隊西](../Page/武田勝賴.md "wikilink")-{征}-，在[長篠之戰](../Page/長篠之戰.md "wikilink")（設樂原）被德川織田聯軍大敗，兩名兄長[真田信綱及](../Page/真田信綱.md "wikilink")[真田昌輝陣亡](../Page/真田昌輝.md "wikilink")，真田昌幸遂改回原姓，繼任家督。

1578年，上杉謙信去世，武田氏與上杉氏達成了甲越同盟。同年，昌幸攻擊上野北條氏領地[沼田城及](../Page/沼田城.md "wikilink")[名胡桃城](../Page/名胡桃城.md "wikilink")；1580年，敘任從五位下[安房守](../Page/安房守.md "wikilink")。1582年初，織田軍大舉入侵甲州，武田軍節節敗退，昌幸曾向勝賴在文書中提議來到[岩櫃城](../Page/岩櫃城.md "wikilink")，途中卻因為[岩殿城主](../Page/岩殿城.md "wikilink")[小山田信茂向織田信長投誠](../Page/小山田信茂.md "wikilink")，導致勝賴無法前往；武田家在天目山之戰大敗後，勝賴全族自盡身亡。至此真田家四處隨著局勢依附強權求存。

### 投機求存

武田家滅亡後，真田昌幸曾與上杉、北條等示好，最終則歸順織田家，受[關東方面大將](../Page/關東.md "wikilink")[瀧川一益的節](../Page/瀧川一益.md "wikilink")-{制}-。6月爆發[本能寺之變](../Page/本能寺之變.md "wikilink")，[織田信長死亡](../Page/織田信長.md "wikilink")，織田家陷入內亂，導致上野、信濃、甲斐等領土陷入戰亂。北條家最後以5萬600大軍在[神流川之戰中使瀧川一益潰不成軍](../Page/神流川之戰.md "wikilink")，一舉奪取關東、[上野主導權](../Page/上野.md "wikilink")。瀧川一益則逃亡到伊勢長島，真田家見勢改投北條家。

接著北條家準備經略信濃，想藉由一場大戰事順便統管真田家領地，認知到真田家存續受威脅時，真田昌幸又背棄北條家，透過武田舊臣屬[曾根昌世的居中協調與德川家親善](../Page/曾根昌世.md "wikilink")，並派長子[真田信幸](../Page/真田信幸.md "wikilink")（關原戰後改名信之）至德川家成為人質。之後德川、北條兩家和解；德川家康為了取信北條家，要求形式上屬於己方的真田家將沼田城歸還北條方。真田昌幸加以回絕，除了著手修築[上田城外](../Page/上田城.md "wikilink")，並倒向上杉。真田昌幸的反覆引來德川、北條的不滿；1585年，兩家分別攻擊真田家在信濃、上野的領地。信濃[上田城遭受德川家的](../Page/上田城.md "wikilink")[鳥居元忠領軍攻打](../Page/鳥居元忠.md "wikilink")，爆發[第一次上田城之戰](../Page/第一次上田城之戰.md "wikilink")（別稱[神川合戰](../Page/神川合戰.md "wikilink")）。此役，真田軍僅以2000兵，擊退德川方面的7000大軍，戰後仍然與家康對立，並於同年攻擊德川領地[佐久郡](../Page/佐久郡.md "wikilink")。此後真田昌幸將次男[真田信繁送至](../Page/真田信繁.md "wikilink")[上杉景勝處做為人質](../Page/上杉景勝.md "wikilink")，以取得上杉家的保護。

### 豐臣家大名

1586年，上杉景勝歸順[羽柴秀吉](../Page/羽柴秀吉.md "wikilink")，真田昌幸亦隨同效忠。不久，德川家康亦臣服秀吉，於是，真田的威脅只剩北條。真田家與北條家為了[上野](../Page/上野.md "wikilink")[沼田的領地](../Page/沼田城.md "wikilink")，長年紛爭不斷，1589年終於由[豐臣家加以調停](../Page/豐臣家.md "wikilink")，[名胡桃城歸真田家](../Page/名胡桃城.md "wikilink")，[沼田城歸北條家](../Page/沼田城.md "wikilink")。惟北條家家臣[豬俣邦憲私自出兵奪取名胡桃城](../Page/豬俣邦憲.md "wikilink")，豐臣家以此為口實，於次年召集眾大名展開[小田原征伐](../Page/小田原征伐.md "wikilink")。1590年，真田昌幸被編入[前田利家及上杉景勝的聯合軍](../Page/前田利家.md "wikilink")，北條家在這場戰爭滅亡。戰後，昌幸因功由原本的地方豪族被提拔為大名。1592年，秀吉發動[文祿之戰期間](../Page/文祿之戰.md "wikilink")，真田昌幸亦出兵支援，文獻留下昌幸當時在名護屋城留守的記錄。

### 關原之戰及晚年

1598年8月，豐臣秀吉死亡。豐臣氏文、武兩派之間的鬥爭亦逐漸白熱化，當中影響力最大者為五大老之一的德川家康。1600年7月，家康向全日本大名發出討伐上杉景勝，當時家康對昌幸發出的書狀是可以保證保留[小縣郡一帶的領土](../Page/小縣郡.md "wikilink")。20日，父子三人在犬伏討論日後去向，最終安排長子信幸支持德川軍，並改名真田信之離開真田家，而自己與信繁則支持[石田三成](../Page/石田三成.md "wikilink")。此後，上田城成為上杉景勝及近畿的石田三成互相通信的重要中途站。另一方面，德川家康派出次男[秀忠攻擊小縣郡](../Page/德川秀忠.md "wikilink")。9月1日，秀忠的部隊到達小縣郡。秀忠派遣使者，要求真田昌幸投降。昌幸假裝投降，而暗中準備迎擊德川軍。翌日，昌幸表明不會投降。德川軍首先進攻[戶石城](../Page/戶石城.md "wikilink")，守城的信繁拒絕交戰，開城撤離。9月3日，德川軍向上田城發動攻擊，真田軍以遊擊戰反擊，連日戰爭使德川軍陷入苦戰。7日，秀忠撤兵繼續前往關原，但已經趕不及參加關原的戰爭。

然而在[關原之戰取得勝利的家康決定懲處真田昌幸及信繁](../Page/關原之戰.md "wikilink")。原先-{欲}-命令他們切腹自盡，惟因長男信之與親家本多忠勝向家康求情，12月德川家康免除兩人死罪，改將兩人流放到紀伊國[高野山附近的](../Page/高野山.md "wikilink")[九度山](../Page/九度山町.md "wikilink")，命令終生不得離開，隨行家臣共16人。幽禁期間，發明了[真田紐販賣餬口](../Page/真田紐.md "wikilink")，大受民眾歡迎。昌幸生前仍然希望德川家赦免他，但是苦等不到赦免而在1611年於失意中病逝，享年65歲。抑鬱而終的昌幸，外加德川家蓄意惡整的態度，使得信繁後來死命逃脫投奔豐臣家留下伏筆。

## 人物評價

被豐臣秀吉評為「表裏比興之者」，意為內外不一、居心叵測的牆頭草，昌幸與忠於武田氏的父兄不同，一心只想延續真田家的存在，認為忠義死節沒有任何意義，東西軍兩邊都安插人馬可以確定不論哪方勝敗日本都能飄著真田家旗幟。後世研究多說是信之與昌幸理念不合才分道揚鑣乃無稽之談，實為昌幸刻意的政治操作，當時也有許多知名武士安排子女加入不同陣營以求存續。

## 墓地

昌幸的墓地在[長野市松代町松代真田山](../Page/長野市.md "wikilink")[長國寺及](../Page/長國寺.md "wikilink")[和歌山縣](../Page/和歌山縣.md "wikilink")[九度山町](../Page/九度山町.md "wikilink")[九度山真田庵佉羅陀山善名稱院](../Page/九度山.md "wikilink")。

## 家系

  -
    父：[真田幸隆](../Page/真田幸隆.md "wikilink")
    母：河源隆正之妹‧恭雲院（另一說為阿緒方）
    長兄：[真田信綱](../Page/真田信綱.md "wikilink")
    二兄：[真田昌輝](../Page/真田昌輝.md "wikilink")
    四弟：[真田信尹](../Page/真田信尹.md "wikilink")
    五弟：[金井高勝](../Page/金井高勝.md "wikilink")
    正室：[山手殿](../Page/山手殿.md "wikilink")（[寒松院](../Page/寒松院.md "wikilink")）（出自有多種說法）
    長男：[真田信幸](../Page/真田信之.md "wikilink")
    次男：[真田信繁](../Page/真田信繁.md "wikilink")
    三男：[真田信勝](../Page/真田信勝.md "wikilink")
    四男：[真田昌親](../Page/真田昌親.md "wikilink")
    長女：[村松殿](../Page/村松殿.md "wikilink")（[小山田茂誠室](../Page/小山田茂誠.md "wikilink")）
    次女：[真田幸政室](../Page/真田幸政.md "wikilink")
    三女：[鎌原重春室](../Page/鎌原重春.md "wikilink")
    四女：保科正光室
    五女：於菊，宇田賴次室，之後為瀧川一積室
    六女：清光院，[妻木賴照室](../Page/妻木賴照.md "wikilink")
    七女：於樂

## 登場作品

  - 小說

<!-- end list -->

  - [真田太平記](../Page/真田太平記.md "wikilink")（[週刊朝日](../Page/週刊朝日.md "wikilink")、[池波正太郎著](../Page/池波正太郎.md "wikilink")）
  - 謀將 真田昌幸（、著）
  - 真田昌幸（[PHP研究所](../Page/PHP研究所.md "wikilink")、龍崎攻著）
  - 真田昌幸（[學研](../Page/學研.md "wikilink")、著）
  - 戰國勇者（學研、津野田幸著）
  - 城を噛ませた男（[光文社](../Page/光文社.md "wikilink")、著）
  - 九度山秘録: 信玄、昌幸、そして稚児（[河出書房新社](../Page/河出書房新社.md "wikilink")、著）

<!-- end list -->

  - 影視劇

<!-- end list -->

  - 宮本武藏（1965年、NTV、演：）

  - 戰國太平記 真田幸村（1966年、TBS、演：）

  - （1979年、東映、演：）

  - [戰國自衛隊](../Page/戰國自衛隊.md "wikilink")（1979年、東宝、演：[角川春樹](../Page/角川春樹.md "wikilink")）

  - [猿飛佐助](../Page/:JA:猿飛佐助_\(1980年のテレビドラマ\).md "wikilink")（1980年、NTV、演：）

  - （1980年、NHK、演：）

  - [關原](../Page/:JA:関ヶ原_\(テレビドラマ\).md "wikilink")（1981年、TBS、演：）

  - [真田太平記](../Page/:JA:真田太平記_\(テレビドラマ\).md "wikilink")（1985年、NHK、演：[丹波哲郎](../Page/丹波哲郎.md "wikilink")）

  - （1986年、TBS、演：）

  - （1989年、TX、演：）

  - [利家與松](../Page/利家與松.md "wikilink")（2002年、[NHK大河劇](../Page/NHK大河劇.md "wikilink")、演：）

  - [少女殺手阿墨：Death or Love](../Page/少女殺手阿墨.md "wikilink")（2005年、東寶、演：）

  - [風林火山](../Page/風林火山.md "wikilink")（2007年、NHK大河劇、演：）

  - [天地人](../Page/天地人_\(大河劇\).md "wikilink")（2009年、NHK大河劇、演：[岩松了](../Page/岩松了.md "wikilink")）

  - [江～公主們的戰國～](../Page/江～公主們的戰國～.md "wikilink")（2011年、NHK大河劇、演：）

  - [真田丸](../Page/真田丸_\(大河劇\).md "wikilink")（2016年、NHK大河劇、演：）

<!-- end list -->

  - 人偶劇

<!-- end list -->

  - 真田十勇士（1975年、NHK、配音：）

<!-- end list -->

  - 漫畫

<!-- end list -->

  - 真田魂（[白泉社](../Page/白泉社.md "wikilink")、作）

<!-- end list -->

  - 歌謠浪曲

<!-- end list -->

  - 真田軍記 沼田城物語（[三波春夫唱](../Page/三波春夫.md "wikilink")）
  - 続・沼田城物語 関ケ原前夜（三波春夫）

## 參考資料

  - [真田安房守昌幸](https://web.archive.org/web/20070112084639/http://homepage1.nifty.com/azalea-house/masayuki.htm)
  - [真田紐介紹](https://ja.wikipedia.org/wiki/%E7%9C%9F%E7%94%B0%E7%B4%90)

[Category:戰國大名](../Category/戰國大名.md "wikilink")
[Masayuki](../Category/真田氏.md "wikilink")
[Category:真田十勇士](../Category/真田十勇士.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Category:武藤氏](../Category/武藤氏.md "wikilink")