**真雙型齒翼龍**（[屬名](../Page/屬.md "wikilink")：）又名**真雙齒翼龍**或**真二形齒翼龍**，是[翼龍類的一屬](../Page/翼龍類.md "wikilink")，化石發現於[義大利](../Page/義大利.md "wikilink")[貝爾加莫](../Page/貝爾加莫.md "wikilink")，年代為[三疊紀晚期的](../Page/三疊紀.md "wikilink")[諾利階](../Page/諾利階.md "wikilink")\[1\]
。牠們的標本是在1973年由Mario Pandolfi發現，並在同一年由Rocco
Zambell所敘述。該標本出土於[頁岩層](../Page/頁岩.md "wikilink")，是目前已知最古老的翼龍類標本，但牠們擁有少數的原始特徵\[2\]。

牠們的翼展約100公分，而且長尾巴的末端可能有個鑽石形標狀物，類似晚期的[喙嘴翼龍](../Page/喙嘴翼龍.md "wikilink")，這個標狀物可能在飛行時充當舵使用\[3\]。真雙型齒翼龍目前已發現數個骨骼，包含幼年體化石。

## 齒列與食性

真雙型齒翼龍的牙齒為明顯的[異型齒](../Page/異型齒.md "wikilink")，這也是牠們的名稱由來。真雙型齒翼龍的頜部長6公分，卻具有110顆牙齒。頜部前段的牙齒為長牙齒，上頜兩側各有4顆長牙，下頜兩側各有2顆長牙；後段的牙齒為小型、多齒尖（通常是三、四顆，可多達五個）的牙齒，上頜每邊各有25顆，下頜每邊各有26顆\[4\]。

科學家根據真雙型齒翼龍的牙齒型態，推測牠們是以[魚類為食](../Page/魚類.md "wikilink")，某個標本的胃部曾經發現某種魚類（*Parapholidophorus*）的化石。真雙型齒翼龍的幼體化石具有稍微不同的齒列，可能是以昆蟲為食\[5\]。當真雙型齒翼龍咬合時，上頜下頜牙齒直接接觸，尤其是後段牙齒。翼龍類普遍有明顯牙合（Dental
occlusion）現象。真雙型齒翼龍的牙齒有多齒尖、以及牙齒磨損狀況，顯示牠們可以做出某種程度的咬碎、咀嚼動作。牙齒側邊的磨損狀況，也顯示牠們也會以有硬殼的[無脊椎動物為食](../Page/無脊椎動物.md "wikilink")\[6\]。

## 系統發生學與分類學

雖然真雙型齒翼龍的生存年代早，牠們卻很少原始特徵，科學家無法藉由此種動物推論翼龍類的起源與分類\[7\]。真雙型齒翼龍具有牙齒、尾巴動作靈活，這是原始特徵；其他有長尾部的翼龍類，尾巴脊椎長、動作較不靈活。由於早期翼龍類的化石很少，科學家對於翼龍類的演化起源有不同看法，例如：[恐龍](../Page/恐龍.md "wikilink")、[主龍形類](../Page/主龍形類.md "wikilink")、[原蜥形目](../Page/原蜥形目.md "wikilink")。

根據目前理論，[鳥頸類主龍演化出](../Page/鳥頸類主龍.md "wikilink")[恐龍形態類](../Page/恐龍形態類.md "wikilink")、翼龍類的祖先。真雙型齒翼龍同時有原始、衍化特徵，無助於研究翼龍類的起源。由於真雙型齒翼龍的牙齒有多齒尖，是種衍化的特徵，而[侏羅紀翼龍類的牙齒只有一個齒尖](../Page/侏羅紀.md "wikilink")，顯示真雙型齒翼龍與侏羅紀翼龍類的直系祖先是遠親。科學家推論，真雙型齒翼龍是翼龍類演化過程中的一個旁支，[曲頜形翼龍科](../Page/曲頜形翼龍科.md "wikilink")\[8\]。

## 發現與種

真雙型齒翼龍目前有兩個種。[模式種](../Page/模式種.md "wikilink")*E.
ranzii*是在1973年被敘述、命名，種名是以Silvio
Ranzi教授為名。[正模標本](../Page/正模標本.md "wikilink")（編號MCSNB
2888）發現於義大利。第二個種是*E. cromptonellus*，是在2001年被敘述、命名，種名是以Alfred W.
Crompton教授為名。化石（編號MGUH VP
3393）是在19世紀初發現於[格陵蘭](../Page/格陵蘭.md "wikilink")，是個未成年個體標本，翼展約24公分。

在1995年，兩個發現於[義大利的化石](../Page/義大利.md "wikilink")，被命名為*E.
rosenfeldi*。在2009年，這兩個化石被建立為新屬，[卡尼亞翼龍](../Page/卡尼亞翼龍.md "wikilink")（*Carniadactylus*）\[9\]。

### 德州化石？

在1986年，[德州西部出土數個頜部碎片](../Page/德州.md "wikilink")，上有多齒尖的牙齒\[10\]，其中一個下頜化石，上面的兩顆牙齒有五齒尖\[11\]。另一個上頜化石則有七顆多齒尖的牙齒\[12\]。這些牙齒非常類似真雙型齒翼龍，可能屬於這個屬\[13\]。

## 大眾文化

真雙型齒翼龍曾出現在[探索頻道節目](../Page/探索頻道.md "wikilink")《[動物末日](../Page/動物末日.md "wikilink")》（*Animal
Armageddon*），被形容是類似[海鷗的動物](../Page/海鷗.md "wikilink")，以[魚類](../Page/魚類.md "wikilink")、腐肉為食。

## 參考資料

  - Dixon, Dougal. "The Complete Book of Dinosaurs." Hermes House, 2006.
  - Fantastic Facts About Dinosaurs (ISBN 978-0-7525-3166-3)

[Category:喙嘴翼龍亞目](../Category/喙嘴翼龍亞目.md "wikilink")
[Category:三疊紀翼龍類](../Category/三疊紀翼龍類.md "wikilink")

1.  Wellnhofer, P. (1991). "Summary of Triassic Pterosaurs." *The
    Illustrated Encyclopedia of Pterosaurs.* London, UK: Salamander
    Books Limited. p. 67. ISBN 978-0-86101-566-5.

2.  "Eudimorphodon." In: Cranfield, Ingrid (ed.). *The Illustrated
    Directory of Dinosaurs and Other Prehistoric Creatures*. London:
    Salamander Books, Ltd. Pp. 280-281.

3.

4.
5.
6.  Osi, A. (2010). "Feeding-related characters in basal pterosaurs:
    implications for jaw mechanism, dental function and diet."
    *Lethaia*,

7.
8.
9.  Dalla Vecchia, F.M. (2009). "Anatomy and systematics of the
    pterosaur *Carniadactylus* (gen. n.) *rosenfeldi* (Dalla Vecchia,
    1995)." *Rivista Italiana de Paleontologia e Stratigrafia*,
    **115**(2): 159–188.

10.
11.
12.
13.