[染色体](../Page/染色体.md "wikilink")**着丝粒**（），又稱**中節**，主要作用是使复制的染色体在[有丝分裂和](../Page/有丝分裂.md "wikilink")[减数分裂中可均等地分配到子细胞中](../Page/减数分裂.md "wikilink")。在很多高等[真核生物中](../Page/真核生物.md "wikilink")，着丝粒看起来像是在染色体一个点上的浓缩区域，这个区域包含[着丝点](../Page/着丝点.md "wikilink")（[希腊语](../Page/希腊语.md "wikilink")*kínesis*
運動；*chóros* 部位），又称主缢痕（primary constriction）。

着丝粒（染色体的主缢痕）为[染色质的结构](../Page/染色质.md "wikilink")，将染色体分成二臂，在细胞分裂前期和中期，把两个姐妹染色单体连在一起，到后期两个染色单体的着丝粒分开。着丝粒两侧各有一个由蛋白质构成的3层盘状特化结构，为非染色体性质物质的附加物，称为[着丝点](../Page/着丝点.md "wikilink")。

在大部分真核生物中每个[纺锤丝附着在不同的着丝粒上](../Page/纺锤丝.md "wikilink")。如[啤酒酵母](../Page/啤酒酵母.md "wikilink")（*Saccharomyces
cerevisiae*）附着在每个着丝粒上仅一条纺锤丝。广义上說着丝粒也常指着丝点，然而狭义上的着丝点是將染色体和纺锤丝[微管相結合的](../Page/微管.md "wikilink")[蛋白质复合体](../Page/蛋白质复合体.md "wikilink")。

若着丝粒丢失了，那么染色体就失去了附着到[纺锤丝上的能力](../Page/纺锤丝.md "wikilink")，[细胞分裂时染色体就会随机地进入子细胞](../Page/细胞分裂.md "wikilink")。然而有着丝粒的染色体也会出现这种异常分配，那就是复制后的两个染色体拷贝并不总是正确地分离进入子细胞。在此过程中发生错误的[概率通常是很低的](../Page/概率.md "wikilink")。如在[酵母中分配发生错误的概率低于十万分之一](../Page/酵母.md "wikilink")。若发生错误会引起染色体数目的改变。

## 延伸閱讀

  -
  -
[Category:遗传学](../Category/遗传学.md "wikilink")
[Category:细胞生物学](../Category/细胞生物学.md "wikilink")