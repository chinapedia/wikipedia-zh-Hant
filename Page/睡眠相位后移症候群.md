**睡眠相位后移症候群**（Delayed sleep-phase
syndrome(Disorder)，簡稱**DSPS**或**DSPD**）是一种慢性[睡眠紊乱](../Page/睡眠障碍.md "wikilink")，患者一般都会晚睡晚起，生活節奏受嚴重影響。

在臨床診斷中，部分患者要到等至天亮才能入睡，一旦睡着，睡眠时間卻跟正常人相約。除了一些患有[睡眠呼吸中止症等](../Page/睡眠呼吸中止症.md "wikilink")[睡眠障碍的患者外](../Page/睡眠障碍.md "wikilink")，他們的睡眠品質大多正常，然而這種晚睡晚起的狀態，令人難以按时上班上课，擾亂作息時序。

此症常在儿童期或青春期出現\[1\]，一些人的-{A|zh-hans:症状;zh-hant:徵狀}-会隨年紀增長而消失，但一些人卻終生受此症困擾。外國一些研究指每2000人約3人會患上此症，佔人口比例約0.15%，男女患者數目相約；而長期受失眠困擾的病人中,約7-10%患有此症。\[2\]

睡眠相位后移症候群在1981年由美國紐約Montefiore Medical
Center的醫生最早提出\[3\]\[4\]，但由於此症候群長期受忽視，不少病人沒嘗試接受治療，有時亦會誤诊成一般失眠。

## 定义

根据[国际睡眠障碍分类](../Page/国际睡眠障碍分类.md "wikilink")（ICSD），睡眠相位后移症候群的主要特征是：

1.  睡眠开始和醒来的时间，比理想情况不可控制的晚。
2.  每天入睡时间基本相同。
3.  一旦入睡，并不会轻易醒来。
4.  在早晨理想时间起床是非常困难的。
5.  相对非常严重的，没有能力去把睡眠相位提前，如强迫在正常的时间睡觉和起床。\[5\]

如下一些特征把睡眠相位后移症候群从其它睡眠障碍中区分出来

1.  患者至少能在早上甚至是下午有正常的睡眠（甚至睡眠质量更好）。相比之下，长期[失眠患者不会感觉在早上入睡比晚上更容易](../Page/失眠.md "wikilink")。
2.  患者每天晚上几乎是准时的感觉到睡意。如果他们在这个时候躺下休息，他们能迅速的进入梦乡。儿童患者在没有感觉到睡意的时候会拒绝上床睡觉，但是如果能允许他们在平常睡觉时间前不在床上躺着，就上床时间的争吵就会消失。
3.  患者可以睡得很好并且很规律，如果让他们遵循自己的[生物钟](../Page/生物钟.md "wikilink")，如周末或者假期的时候。
4.  DSPS是一个长期的病症，诊断这个综合征需要根据至少一个月的临床表现。

一个被强迫按照朝九晚五方式生活的DSPS患者经常被拿来与一个经常有6个小时[时差的人来做比较](../Page/时差.md "wikilink")。他们都在工作日的晚上少睡幾個小时，但是会在周末或者下午补回来。周末睡觉或者下午的瞌睡会让DSPS从白天的困倦中解脱出来，但是却更加强化了他的睡眠相位后移。

DSPS患者通常都是严重的[夜猫子](../Page/夜猫子.md "wikilink")，他们感觉半夜三更的时候头脑最敏捷，做事情效率最高。DSPS患者不能轻易的强迫自己早睡。他们会在床上辗转反侧好几个小时，有些时候，到了去上课或上班的时候都还没合眼。

当DSPS患者寻求医疗帮助的时候，他们一定很多次尝试改变自己的睡眠规律了。早期的失败的尝试一般包括：放松技术、早睡、[催眠](../Page/催眠.md "wikilink")、[酒精](../Page/酒精.md "wikilink")、安眠药、床头读物和[家庭疗法](../Page/家庭疗法.md "wikilink")。其中部份使用过[安眠药的患者说](../Page/安眠药.md "wikilink")，这种药让他们感觉疲惫或放松，却不能带来睡意。患者经常求助于家人在早上唤醒他们，或者他们会设置许多[闹钟](../Page/闹钟.md "wikilink")。因为这种症状在青春期的时候比较普遍，通常是患者的父母最先寻求帮助，因为他们费了九牛二虎的力气也不能叫醒他们的孩子去准时上学。

## 流行

根据ICSD的诊断标准，对1万名[挪威](../Page/挪威.md "wikilink")[成年人的随机调查显示DSPS的患病率为](../Page/成年人.md "wikilink")0.17%；相似的调查针对1,525名[日本成年人显示患病率为](../Page/日本.md "wikilink")0.13%。另外尚有一些调查指出在[青少年中的患病率高达](../Page/青少年.md "wikilink")7%。

## 生理学解释

DSPS是一种人体的计时系统（生物鐘）紊乱。一般认为，重置生物鐘（入睡、醒來時間）的能力减弱是导致DSPS的原因。DSPS患者通常有非常长的[日夜节律周期](../Page/日夜节律.md "wikilink")，或者对日光重置生物钟的反应很弱。

如果头天晚上没睡足，一个有正常日夜节律系统的人通常会很快的入睡。早睡通常会自动的提前他们的日夜节律时钟。相比之下，就算DSPS患者缺乏睡眠，他們仍然不能在他们的通常睡觉时间之前入睡。研究人员发现，缺乏睡眠不能夠重置DSPS患者的日夜节律时钟。

努力按照正常作息生活的DSPS患者会很难入睡，也很难醒来。因为他们的生物鐘不是在那个相位上。而正常人上夜班如果沒有调整好，也会有相同的症状。

DSPS患者的其他日夜节律机能上也有后移，比如[褪黑素的分泌和体温最低值](../Page/褪黑素.md "wikilink")，相应的入睡和醒来的周期。入睡，自然的醒来，还有其他很多的内部机能都同时后移了相同的时间。如果考虑到不能被周围人接受的睡觉和醒来的时间，非杓型（non-dipper）高血压也和DSPS有关。

多数情况下，无法知道为什么DSPS患者的生物鐘异常。DSPS看上去是遗传的:大量的证据显示这个问题和hPer3 (human period
3)有关。也有一些有记录的病例是由于[脑外伤引起的DSPS和非](../Page/脑外伤.md "wikilink")24小时睡眠周期综合征。

也有非常少的病例是DSPS发展成[非24小时睡眠周期综合症](../Page/非24小时睡眠周期综合症.md "wikilink")，这是一种非常严重的导致衰弱的睡眠紊乱，患者每天都晚睡一点。

## 诊断

DSPS靠临床观察，机体活动变化记录仪监测或者睡眠记录来持续跟踪患者长达至少3个星期。

DSPS经常被误诊。经常被误诊为一种精神障碍。DSPS经常和很多其他疾病混淆，如，精神心理的失眠，[抑郁症](../Page/抑郁症.md "wikilink")，精神障碍如[精神分裂症或者](../Page/精神分裂症.md "wikilink")[注意力不足过动症](../Page/注意力不足过动症.md "wikilink")，还有其他睡眠障碍，或一些故意的行为，如不想去上学。睡眠医药的从业者指出，DSPS的诊断准确率实在太低，急需对睡眠障碍者进行更好的[生理学教育](../Page/生理学.md "wikilink")。

## 对患者的影响

由于大众对这种疾病的认识不够，导致了DSPS患者承受了更多的压力，如被视为无纪律或懒惰。父母通常被责备没有适当的教育子女，学校则很少能容忍长期的迟到，
缺勤，或者上课睡觉的学生，并且不会认为他们是得了一种长期的疾病。

[世界卫生组织](../Page/世界卫生组织.md "wikilink")2004年关于睡眠对于健康影响的会议上，睡眠专家指出：

到了DSPS患者得到正确的诊断之前，他们已经很长时间被误诊，或者被加上懒惰，不思进取的员工或学生的标签。误诊为心理问题给患者和家人带来了巨大的痛苦，还导致了一些患者服用了一些不合适的[精神药品](../Page/精神药品.md "wikilink")。对于很多患者，准确的诊断出DSPS是他一生的转折点。

## 治疗

对DSPS的治疗非常特别。这跟治疗失眠不一样，重视患者睡眠质量的同时要兼顾时间问题。症状較轻的患者可以用每天提早15分钟起床直到达到期望时间的办法，严重一些的患者，在治療方面，通常有以下的一些治疗方法：

开始DSPS治疗以前，患者通常被要求按照他们自己的作息来有规律的睡眠，要保证他们在“白天”不瞌睡，让患者在治疗开始的时候讓精神放鬆是非常重要的。

1.  [光线治疗](../Page/光线治疗.md "wikilink")，配合[全光谱灯](../Page/全光谱灯.md "wikilink")，通常早上30－90分钟照射10,000[勒克斯](../Page/勒克斯.md "wikilink")。日光也是可以的。光线治疗通常会要求患者在早晨花更多的时间。这个方法需要几天至2周才能见效，之后可以不时的使用来保持效果。避免晚上有强光也会有帮助。

2.  調整[枕頭高度](../Page/枕頭.md "wikilink")。枕頭太高或太低會影響頸椎生理曲線。\[6\]

3.  不用有色玻璃窗，對治療DSPS有幫助。\[7\]

4.  把晚上開著的6500K [色溫的燈管都換成](../Page/色溫.md "wikilink")2700K 或者4000K
    的燈管。\[8\]相對黃光而言，白光能推遲生物鐘。\[9\]

5.  變換睡姿。\[10\]\[11\]

6.  晚餐不要太晚。\[12\]

7.  不蒙被或者不使用過於沉重的被子。\[13\]

8.  ，连续几天晚睡觉几个小时，来重置生物鐘。時間治療的安全性還不完全清楚。\[14\]

9.  睡前1小时或更早的时候补充小剂量（约1[毫克](../Page/毫克.md "wikilink")）[褪黑素](../Page/褪黑素.md "wikilink")，也可能帮助建立早一些的作息，特别是结合[光线治疗在自主醒来的时候](../Page/光线治疗.md "wikilink")。但是一些褪黑素的副作用是扰乱睡眠，噩梦，白天萎靡不振。长期的褪黑素副作用还不知道，也不能随便生产。在一些国家，[荷尔蒙是处方药](../Page/荷尔蒙.md "wikilink")，或者根本不能买到。在美国和加拿大，褪黑素的供应不受限制。

10. 一些人声称大剂量的[维生素B12可以让睡意正常出现](../Page/维生素B12.md "wikilink")，但是疗效似乎不彰。

11. 最近新批准的新药[Ramelteon有明确的疗效](../Page/Ramelteon.md "wikilink")，它在某些方面的功效类似人工合成的褪黑素。生产Ramelteon和生产其它处方药一样受到监督，所有避免了传统褪黑素提纯和剂量的问题。

12. [莫达非尼](../Page/莫达非尼.md "wikilink")（Modafinil）是一种美国批准的治疗[轮班工作睡眠紊乱](../Page/轮班工作睡眠紊乱.md "wikilink")（SWSD）的药物，SWSD和DSPS有很多共同特点，很多临床医生用这个疾病来描述DSPS病患。但是莫达菲尼不能解决DSPS的深层原因，他只能提高缺乏睡眠的病患的生活质量。如果在理想睡觉时间前12小时以内服用莫达菲尼会恶化DSPS症状，因为实际上他会推后入睡醒来的周期。

13. 定期清潔、吸[塵蟎](../Page/塵蟎.md "wikilink")。\[15\]

曾經有過利用[曲唑酮來治疗DSPS的成功案例可查](../Page/曲唑酮.md "wikilink")。\[16\]

## 适应晚睡的时间

长期的治疗成功的比率并没有确切的数字，但是有经验的临床医生明白，DSPS是非常难治愈的。

在晚上工作，或者在家工作的人，DSPS似乎不是什么大问题。他们中多数人并没意识到这是一种“紊乱”。一些DSPS患者打瞌睡，更有一些人白天睡4小时，晚上睡4小时，尽管白天长时间的瞌睡导致晚上更加没有睡意。一些比较适合DSPS患者工作的岗位包括保安，在剧院或者媒体工作，自由作家，呼叫中心，护理，出租车或者卡车司机。

一些DSPS病患虽然经历了很多年的治疗，仍然不能适应早睡。一些睡眠研究者就把这些无法治疗的DSPS病例称为“睡眠周期紊乱缺陷”。

DSPS患者的理疗包括[接受事实](../Page/接受.md "wikilink")，选择可以晚睡的职业。在一些学校，患有DSPS的学生可以在他们精力最集中的时间段参加考试。

## DSPS和抑郁症

在有记载的DSPS病例中，近半数还罹患[抑郁症或者其他心理疾病](../Page/抑郁症.md "wikilink")。DSPS和抑郁症之间的联系还不是很清楚。有一半的DSPS患者没有抑郁症说明DSPS不是简单的抑郁症的症状。就算在抑郁症患者中，时间治疗的疗效也比直接治疗抑郁要好。

普遍认为，DSPS是引起抑郁的重要原因，因为它是一种带来很大压力和误解的紊乱。存在能联系睡眠机制和抑郁的神经化学物质也是一种可能。

患有抑郁症的DSPS患者应该同时接受针对两种疾病的治疗。一些证据显示，DSPS的有效治疗可以改善患者的精神面貌，从而让抑郁症的治疗更加有效果，同时，对抑郁症的治疗可以让患者的DSPS治疗成功率更高。

## 流行文化中的DSPS

  - 在卡通剧《[凱文的幻虎世界](../Page/凱文的幻虎世界.md "wikilink")》（*[Calvin and
    Hobbes](../Page/Calvin_and_Hobbes.md "wikilink")*）的一集中，当Calvin被叫起床的时候，他说：「不！不！不！我还想睡一会！」那天，他在学校睏得不行，但是当他被母亲带上楼，按在床上的时候，他又叫嚷到：「现在就睡？我一点都不困」。最后，Calvin说：「我的生物時鐘被设置在了[东京时间](../Page/UTC+9.md "wikilink")」。\[17\]

## 參見

  - [睡眠障碍](../Page/睡眠障碍.md "wikilink")
  - [相位反应曲线](../Page/相位反应曲线.md "wikilink")
  - [失眠](../Page/失眠.md "wikilink")
  - [睡眠惰性](../Page/睡眠惰性.md "wikilink")
  - [生物节律](../Page/生物节律.md "wikilink")（生物钟学）
  - [季节性情绪失调](../Page/季节性情绪失调.md "wikilink")

## 引用

## 參考資料

  -
  -
  -
  -
## 外部链接

  - [Stanford University - Delayed Sleep Phase
    Syndrome](http://www.stanford.edu/~dement/delayed.html)
  - [ClevelandClinic.org - Delayed Sleep Phase Syndrome and Advanced
    Sleep Phase
    Syndrome](http://www.clevelandclinic.org/health/health-info/docs/3700/3714.asp?index=12116&src=news)
  - [Center for Environmental Therapeutics](http://www.cet.org/) -
    Discusses the use of light therapy, for SAD, nonseasonal depression,
    and DSPS. You can use the *Ask the Doctor* forum to have questions
    answered by clinical and research specialists from Columbia
    University. There is a self-assessment questionnaire to choose the
    optimum timing of light therapy for any individual.
  - [DSPSinfo.org](http://www.dspsinfo.org/) - Written by and for DSPS
    sufferers
  - [Niteowl mailing
    list](http://lists.circadiandisorders.org/listinfo.cgi/niteowl-circadiandisorders.org):
    an active support group for people with DSPS, and their families,
    since 1995.
  - [Sleep
    Discrimination](https://web.archive.org/web/20090718152336/http://www.nightworkers.com/discrimation.html)
    - Night People, the Overlooked Minority

[no:Døgnrytmeforstyrrelse\#Forsinket
søvnfasesyndrom](../Page/no:Døgnrytmeforstyrrelse#Forsinket_søvnfasesyndrom.md "wikilink")

[Category:睡眠障碍](../Category/睡眠障碍.md "wikilink")
[Category:晝夜節律](../Category/晝夜節律.md "wikilink")
[Category:症候群](../Category/症候群.md "wikilink")
[Category:睡眠生理學](../Category/睡眠生理學.md "wikilink")

1.  Dagan Y; Eisenstein M Circadian rhythm sleep disorders: toward a
    more precise definition and diagnosis. *Chronobiol Int* 1999
    Mar;16(2):213-22

2.
3.  \*

4.

5.  American Academy of Sleep Medicine *[International Classification of
    Sleep Disorders, Revised Edition](http://www.absm.org/ICSD.htm) *
    2001.

6.  <https://books.google.com/books?id=FbCZDQAAQBAJ&pg=PT83&lpg=PT83#v=onepage&q&f=false>

7.

8.

9.  <http://ucwap.caixin.com/2014-12-29/100769054.html>

10. <http://news.xinhuanet.com/tech/2008-05/19/content_8203851.htm>

11.

12.

13.

14.

15.

16. Nakasei, Shinji et al. Trazodone advanced a delayed sleep phase of
    an elderly male: A case report *Sleep and Biological Rhythms* Volume
    3 Page 169 - October 2005

17. <http://picayune.uclick.com/comics/ch/1995/ch950103.gif>