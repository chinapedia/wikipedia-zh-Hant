**睡莲**（[学名](../Page/学名.md "wikilink")：**）又称**子午莲**、**水芹花**、**瑞蓮**、**水洋花**或**小莲花**，是屬於[睡蓮目](../Page/睡蓮目.md "wikilink")[睡蓮科](../Page/睡蓮科.md "wikilink")[睡蓮属的](../Page/睡蓮属.md "wikilink")[水生植物](../Page/水生植物.md "wikilink")。

## 形态

多年生水生[草本](../Page/草本.md "wikilink")，外型与[荷花相似](../Page/荷花.md "wikilink")，不同的是荷花的叶子和花挺出水面，而睡莲的叶子和花浮在水面上，不同於[荷花的](../Page/荷花.md "wikilink")“出淤泥而不染”。

睡莲秋季开白色花，午后开放，晚间闭合，可以连续开闭三四日；睡莲因昼舒夜卷（一些品種會白晝開花而夜間閉合）而被誉为“花中睡美人”。

## 用途

除[荷花外](../Page/荷花.md "wikilink")，睡莲是水生花卉中又一名贵花卉。

睡蓮的用途甚廣，可用於食用、製茶、[插花](../Page/插花.md "wikilink")、藥用等。但睡莲的种子由于含单宁量高，一般不作食用。《本草綱目拾遺》記載「子午蓮，綱目水草部，入蘋，以為此即大葉之蘋也，古人以為食品，祭用蘋、蘩」、「紅白蓮花，此不知即蓮花否，而功與蓮同，以類相從」。睡蓮切花離水時間超過1小時以上可能使吸水性喪失，而失去開放能力。

## 分布

睡莲在[北半球广泛分布](../Page/北半球.md "wikilink")，[东亚](../Page/东亚.md "wikilink")（如[中国](../Page/中国.md "wikilink")、[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")、[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")、[蒙古等](../Page/蒙古.md "wikilink")）、[中亚](../Page/中亚.md "wikilink")、[西伯利亚](../Page/西伯利亚.md "wikilink")、[东南亚](../Page/东南亚.md "wikilink")（如[越南](../Page/越南.md "wikilink")、[缅甸](../Page/缅甸.md "wikilink")）、[印度](../Page/印度.md "wikilink")、[东欧](../Page/东欧.md "wikilink")、[北欧](../Page/北欧.md "wikilink")、[北美洲西北部](../Page/北美洲.md "wikilink")，都有睡莲的分布。\[1\]\[2\]

## 图画

<File:Nymphaea> Tetragona 1.JPG <File:Nymphaea> Tetragona 2.JPG
<File:Nymphaea> Tetragona 3.JPG <File:Nymphaea> Tetragona 4.JPG
<File:Nymphaea> Tetragona 5.JPG <File:Nymphaea> Tetragona 6.JPG
<File:Nymphaea> Tetragona 7.JPG <File:Nymphaea> Tetragona 8.JPG
<File:Nymphaea> Tetragona 9.JPG <File:Hitsujigusa.jpg> <File:Nymphaea>
tetragona1.jpg <File:Nymphaea> tetragona2.jpg

## 睡蓮和荷花的區別

睡蓮和荷花二者的外观虽然相似，但亲缘关系甚远，两者可从其葉子的生長位置和葉子的形狀区分开来。

<table>
<tbody>
<tr class="odd">
<td><p>睡蓮</p></td>
<td><p><a href="../Page/荷花.md" title="wikilink">荷花</a>(蓮)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Rosa_Seerose.jpg" title="fig:Rosa_Seerose.jpg">Rosa_Seerose.jpg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Lotus_Nelumbo_nucifera_Flower_Large_3264px.jpg" title="fig:Lotus_Nelumbo_nucifera_Flower_Large_3264px.jpg">Lotus_Nelumbo_nucifera_Flower_Large_3264px.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p>葉子貼在水面，<br />
葉有深裂縫。</p></td>
<td><p>葉子伸出水面，<br />
葉圓形，無裂縫。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/植物界.md" title="wikilink">植物界</a>(Plantae)<br />
&gt; <a href="../Page/被子植物分支.md" title="wikilink">被子植物分支</a>(angiosperms)<br />
&gt; <a href="../Page/睡蓮目.md" title="wikilink">睡蓮目</a>(Nymphaeales)<br />
&gt; <a href="../Page/睡蓮科.md" title="wikilink">睡蓮科</a>(Nymphaeaceae)<br />
&gt; <a href="../Page/睡蓮屬.md" title="wikilink">睡蓮屬</a>(<em>Nymphaea</em>)<br />
&gt; <strong>睡蓮(<em>Nymphaea tetragona</em>)</strong></p></td>
<td><p><a href="../Page/植物界.md" title="wikilink">植物界</a>(Plantae)<br />
&gt; <a href="../Page/被子植物分支.md" title="wikilink">被子植物分支</a>(angiosperms)<br />
&gt; <a href="../Page/真双子叶植物分支.md" title="wikilink">真双子叶植物分支</a>(eudicots)<br />
&gt; <a href="../Page/山龙眼目.md" title="wikilink">山龙眼目</a>(Proteales)<br />
&gt; <a href="../Page/莲科.md" title="wikilink">莲科</a>(Nelumbonaceae)<br />
&gt; <a href="../Page/莲属.md" title="wikilink">莲属</a>(<em>Nelumbo</em>)<br />
&gt; <strong><a href="../Page/荷花.md" title="wikilink">荷花</a>(<em>N. nucifera</em>)</strong></p></td>
</tr>
</tbody>
</table>

## 参见

  - [荷花](../Page/荷花.md "wikilink")

## 参考文献

## 外部链接

  -
[tetragona](../Category/睡莲属.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")

1.  关克俭．[睡莲](http://v2.cvh.org.cn/zhiwuzhi/page/27/009a.pdf)\[A\]．见：中国科学院中国植物志编辑委员会．[中国植物志](../Page/中国植物志.md "wikilink")
    第二十七卷\[M\]．北京：科学出版社，1976：9-10
2.   USDA, ARS, National Genetic Resources Program. *Germplasm Resources
    Information Network - (GRIN)* \[Online Database\]. National
    Germplasm Resources Laboratory, Beltsville, Maryland. URL:
    <http://www.ars-grin.gov/cgi-bin/npgs/html/taxon.pl?25446> (14 April
    2010)