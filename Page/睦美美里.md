**睦美美里**（、），是一位[日本女性](../Page/日本.md "wikilink")[角色设定](../Page/角色设定.md "wikilink")、[原畫師及](../Page/原畫師.md "wikilink")[插画家](../Page/插画家.md "wikilink")。目前就职于[Leaf](../Page/Leaf.md "wikilink")[东京开发室](../Page/东京.md "wikilink")。[東京都出生](../Page/東京都.md "wikilink")、成长于[埼玉县](../Page/埼玉县.md "wikilink")，毕业于[立正大学](../Page/立正大学.md "wikilink")[经济学院](../Page/经济学.md "wikilink")。

## 人物

  - 筆名由來是高中時代喜歡[豬股睦美](../Page/豬股睦美.md "wikilink")，。原畫的[落款簡寫](../Page/落款.md "wikilink")「」。

<!-- end list -->

  - 大学毕业后，在[Cocktail
    Soft](../Page/Cocktail_Soft.md "wikilink")（属于[F\&C子会社](../Page/F&C.md "wikilink")）就职，曾使用「CARROT」名義。由于为「」做人物设定，而赢得广泛的人气。

<!-- end list -->

  - 1998年，和[甘露树](../Page/甘露树.md "wikilink")、[CHARM一起从](../Page/CHARM.md "wikilink")[Cocktail
    Soft跳槽到](../Page/Cocktail_Soft.md "wikilink")[Leaf东京开发室](../Page/Leaf.md "wikilink")，据称理由是「自己想要表现的Leaf能够帮助实现」。在[Leaf期间](../Page/Leaf.md "wikilink")，为[漫畫派對](../Page/漫畫派對.md "wikilink")、[传颂之物](../Page/传颂之物.md "wikilink")、[没有天使的12月](../Page/没有天使的12月.md "wikilink")、[ToHeart2等作品进行主要的人物设定](../Page/ToHeart2.md "wikilink")，据传还参加了一部企划活动。另外，其弟藤原龍也在該公司任職。

<!-- end list -->

  - 自己的职业定义为「」，原因是除了原画、CG等其它一切与游戏企划开发的活动自称都会参加。

<!-- end list -->

  - 受她画风影响的同人作家数量非常大。一部分作家被怀疑有[摹写](../Page/摹写.md "wikilink")（）的嫌疑，这些作家的画风常被揶揄为「323绘」（）。

<!-- end list -->

  - 网络上（例如[2ch](../Page/2ch.md "wikilink")）常用「323」（谐音）、「」这些的缩语来指代。「」这种说法来自游戏《[漫畫派對](../Page/漫畫派對.md "wikilink")》中的参与设定的角色大庭詠美；大庭詠美常自称「」。

<!-- end list -->

  - 2011年9月22日發售的[PS3遊戲](../Page/PS3.md "wikilink")《[ToHeart2 DX
    PLUS](../Page/ToHeart2.md "wikilink")》（）與鷲見努共同擔任導演，為初執導作品。\[1\]

## 同人活動

早期曾與[西又葵交流](../Page/西又葵.md "wikilink")，以[同人社團](../Page/同人社團.md "wikilink")「Cut
a Dash」積極參加同人活動。在同人誌圈內具有博大的人氣，在[Comic
Market上經常發生上千人排隊購買同人誌的盛況](../Page/Comic_Market.md "wikilink")，據傳每次發售販賣本數可達2萬本以上。曾為2005年12月舉行的Comic
Market 69創作了場刊的封面繪圖。

## 主要参加的游戏作品

  - 成人游戏

<!-- end list -->

  - Cocktail Soft（IDES→F\&C）部分
      - [CANCAN BUNNY PREMIERE
        2](../Page/CANCAN_BUNNY_PREMIERE_2.md "wikilink")
      - [歡迎來到Pia Carrot\!\!2](../Page/歡迎來到Pia_Carrot!!2.md "wikilink")
      - [CANCAN BUNNY 1
        Primo](../Page/CANCAN_BUNNY_1_Primo.md "wikilink")
      - [ぴあきゃろTOYBOX](../Page/ぴあきゃろTOYBOX.md "wikilink")
  - Leaf（[Aquaplus](../Page/Aquaplus.md "wikilink")）部分
      - [漫畫派對](../Page/漫畫派對.md "wikilink")
      - [去豬名川吧！！](../Page/去豬名川吧！！.md "wikilink")
      - [沒有天使的12月](../Page/沒有天使的12月.md "wikilink")
      - [傳頌之物](../Page/傳頌之物.md "wikilink")
      - [和阿露露一起玩！！](../Page/和阿露露一起玩！！.md "wikilink")
      - [ToHeart2](../Page/ToHeart2.md "wikilink") XRATED
      - [Fullani](../Page/Fullani.md "wikilink")
      - [ToHeart2
        AnotherDays](../Page/ToHeart2_AnotherDays.md "wikilink")
      - [你在米吉多之丘的呼喚](../Page/你在米吉多之丘的呼喚.md "wikilink")

<!-- end list -->

  - 全年龄游戏

<!-- end list -->

  - Cocktail Soft（IDES→F\&C）部分
      - [CANCAN BUNNY PREMIERE
        2](../Page/CANCAN_BUNNY_PREMIERE_2.md "wikilink")
  - Leaf（[Aquaplus](../Page/Aquaplus.md "wikilink")）部分
      - 漫畫派對（[DC](../Page/Dreamcast.md "wikilink")）
      - [ToHeart2](../Page/ToHeart2.md "wikilink")（[PS2](../Page/PS2.md "wikilink")）
      - [ダンジョントラベラーズ2
        王立図書館とマモノの封印](../Page/ダンジョントラベラーズ2_王立図書館とマモノの封印.md "wikilink")
      - [ダンジョントラベラーズ2-2
        闇堕ちの乙女とはじまりの書](../Page/ダンジョントラベラーズ2-2_闇堕ちの乙女とはじまりの書.md "wikilink")
      - [傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")
      - [傳頌之物 二人的白皇](../Page/傳頌之物_二人的白皇.md "wikilink")

## 参考资料

## 延伸閱讀

<div class="references-small">

  -
  - [七尾奈留](../Page/七尾奈留.md "wikilink")

  - [摹写问题](http://d.hatena.ne.jp/keyword/%a5ȥ쥹)

  - [ちゃん様称呼的来由](http://d.hatena.ne.jp/keyword/%a4%c1%a4%e3%a4%f3%cd%cd)

</div>

## 外部链接

  - PLATINUM PENGUIN BOX<sup>&</sup>→[Platinum Penguin
    Cafe→MITSUMI-WEB.COM](http://mitsumi-web.com/)

  - [MITSUMI-WEB.COM內嵌舊網誌](https://web.archive.org/web/20141028174754/http://mitsumi.sblo.jp/)

  -
[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:電子遊戲繪圖及原畫家](../Category/電子遊戲繪圖及原畫家.md "wikilink")
[Category:同人創作者](../Category/同人創作者.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")

1.