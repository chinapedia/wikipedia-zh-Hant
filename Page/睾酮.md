**睪酮**（testosterone）（又稱**睪固酮**、**睪丸素**、**睪丸酮**或**睪甾酮**、**睪脂酮**）是[類固醇](../Page/類固醇.md "wikilink")[激素](../Page/激素.md "wikilink")，由[男性的](../Page/男性.md "wikilink")[睪丸或](../Page/睪丸.md "wikilink")[女性的](../Page/女性.md "wikilink")[卵巢分泌](../Page/卵巢.md "wikilink")，[腎上腺亦分泌少量睪酮](../Page/腎上腺.md "wikilink")。睪酮是主要的[雄激素及](../Page/雄激素.md "wikilink")[蛋白同化甾类](../Page/蛋白同化甾类.md "wikilink")。不論是男性或女性，對健康都有重要影響，包括增強[性慾](../Page/性慾.md "wikilink")、[力量](../Page/力量.md "wikilink")、[免疫功能](../Page/免疫.md "wikilink")、對抗[骨質疏鬆症等功效](../Page/骨質疏鬆症.md "wikilink")。據統計，成年男性分泌睪酮的份量是成年女性的分泌量的約20倍，體內含量是成年女性的7\~8倍。

## 产生

與其他[類固醇](../Page/類固醇.md "wikilink")[激素一樣](../Page/激素.md "wikilink")，睪酮是由[膽固醇所衍生的](../Page/膽固醇.md "wikilink")。[男性](../Page/男性.md "wikilink")[睪丸可以產生大量睪酮](../Page/睪丸.md "wikilink")，[女性](../Page/女性.md "wikilink")[卵巢](../Page/卵巢.md "wikilink")、[胎盤及男性或女性的](../Page/胎盤.md "wikilink")[腎上腺](../Page/腎上腺.md "wikilink")[網狀帶亦會分泌少量的睾酮](../Page/網狀帶.md "wikilink")。

睪酮是經由[血液中的](../Page/血液.md "wikilink")[性激素結合球蛋白運送至身體的目標組織](../Page/性激素結合球蛋白.md "wikilink")。男性[生殖腺內的](../Page/生殖腺.md "wikilink")[塞爾托利氏細胞需要睪酮來生成](../Page/塞爾托利氏細胞.md "wikilink")[精子](../Page/精子.md "wikilink")。

## 運作方式

睪酮對[人類及其他](../Page/人.md "wikilink")[脊椎動物通過兩個方式來產生效用](../Page/脊椎動物.md "wikilink")：

1.  直接或以[血清雙氫睪酮](../Page/血清雙氫睪酮.md "wikilink")（DHT）形式使[雄激素](../Page/雄激素.md "wikilink")[受體活躍化](../Page/受體.md "wikilink")
2.  轉化為[雌二醇使某些](../Page/雌二醇.md "wikilink")[雌激素受體活躍化](../Page/雌激素.md "wikilink")

游離的睪酮會被運送至目標組織細胞的[細胞質中](../Page/細胞質.md "wikilink")，與雄激素受體結合，或被[5α還原酶轉為血清雙氫睪酮](../Page/5α還原酶.md "wikilink")（DHT）。DHT同樣會與雄激素受體結合（這種結合更穩固，DHT的男性激素強度約是睪酮的2.5倍）。睪酮受體或血清雙氫睪酮受體會進行結構性的轉變，並讓它們進入[細胞核](../Page/細胞核.md "wikilink")，與[染色體DNA內特定的](../Page/染色體.md "wikilink")[核苷酸結合](../Page/核苷酸.md "wikilink")。結合的區域稱為[激素反應元件](../Page/激素反應元件.md "wikilink")（HRE），並影響某些[基因的複製活動](../Page/基因.md "wikilink")，產生男性激素效用。

在其他[脊索動物的組織內也會有雄激素受體](../Page/脊索動物.md "wikilink")，而雄性及雌性對睪酮的反應也相當一致。在出生前雄性與雌性的睪酮劑量的差異，會導致在青春期或整個生命上的生物差異。

睪酮[芳構化成雌二醇會對](../Page/芳構化.md "wikilink")[腦及](../Page/腦.md "wikilink")[骨這兩個人體重要的組織構成主要的影響](../Page/骨.md "wikilink")。在骨中，雌二醇加速了[軟骨成長為骨的過程](../Page/軟骨.md "wikilink")，使骨端接合及完成生長的程序。在中樞神經中，雌二醇是給予[下視丘的主要迴饋訊號](../Page/下視丘.md "wikilink")，尤其在影響[排卵素的分泌](../Page/排卵素.md "wikilink")。在很多[哺乳動物的腦部中](../Page/哺乳動物.md "wikilink")，某些性別差異區域在出生前或產期的雄性化，都是受著睪酮所產生的雌二醇影響，決定將來的雄性性行為。

## 對人類的影響

概括地說，[雄激素助長](../Page/雄激素.md "wikilink")[蛋白質的合成及擁有雄激素](../Page/蛋白質.md "wikilink")[受體的組織的生長](../Page/受體.md "wikilink")，睪酮的效用可以分為合成代謝及雄性化效應。合成代謝效應包括[肌肉質量及力量的增長](../Page/肌肉.md "wikilink")、增加骨質密度及強度、刺激線性生長及骨骼成熟等。雄性化效應則包括性器官的成熟（尤其是[陰莖及胎兒](../Page/陰莖.md "wikilink")[陰囊的生成](../Page/陰囊.md "wikilink")）、出生後（通常是在青春期）聲線的轉沉、[鬍鬚及體毛的生長等](../Page/鬍鬚.md "wikilink")。這些效應一般都是男性的[第二性徵](../Page/第二性徵.md "wikilink")。

睪酮的影響亦可以自然產生睪酮的時期來區分。在出生後睪酮對男性或女性的不同影響，主要是受著運送游離睪酮的份量及時間所影響。在妊娠後7－12個星期，大部份出生後的男激素效應便會出現，這包括：

  - 生殖男性化，如陰莖的漲大等
  - [前列腺及](../Page/前列腺.md "wikilink")[精囊的生育](../Page/精囊.md "wikilink")

此外，睾酮会降低信任感，而[催产素则会提高信任感](../Page/催产素.md "wikilink")（Bos &
others，2010）\[1\]

### 對幼嬰的影響

現時就睪酮對幼嬰的影響所知甚少。在男性幼嬰出生後的頭一個星期，睪酮的份量開始上升，並維持與青春期相約的份量數個月。在約4－6個月大，睪酮份量會下降至僅能測量的份量。這樣的上升未有對身體上其他地方帶來改變，其作用仍是未知的，但有學者認為是協助所謂的「腦部男性化」。

### 對青春期的影響

在青春期時，睪酮會令孩童出現第一種雄激素上升可見的徵狀。這些現象包括：

  - 產生成年人的體味
  - [皮膚及頭髮增加油性](../Page/皮膚.md "wikilink")，長出[青春痘](../Page/青春痘.md "wikilink")
  - 長出[體毛及](../Page/體毛.md "wikilink")[鬍鬚](../Page/鬍鬚.md "wikilink")
  - 整體成長劇增，加速骨骼成長
  - 維持[陰莖正常的發育](../Page/陰莖.md "wikilink")
  - 維持正常的性慾及性功能
  - 長出[喉核](../Page/喉核.md "wikilink")，令聲線變得低沉
  - 面部皮下脂肪減少
  - 肌肉強度及質量上升

### 對成人的影響

睪酮對成人的影響，對男性比較明顯，但對男女兩性都同樣重要。隨著年紀漸長，睪酮的份量下降，這些影響亦隨之而減少。這些影響包括：

  - 維持肌肉強度及品量
  - 維持骨質密度及強度
  - 維持性慾及勃起次數（濫用補充劑可能會導致疼痛性[陰莖異常](../Page/陰莖.md "wikilink")[勃起](../Page/勃起.md "wikilink")、和[性慾亢進](../Page/性慾亢進.md "wikilink")）
  - 提神及提升體能（對照睪酮缺乏患者體能較佳，但與正常人無異，刻意補充對體能不會有顯著的增加）
  - 成年男子不論服用或施打多少睪酮都無法再次促使[陰莖的成長與發育](../Page/陰莖.md "wikilink")（相反的，錯誤的使用激素，將可能造成體內原生激素紊亂，將無法自行生成使[睪丸及](../Page/睪丸.md "wikilink")[陰莖萎縮](../Page/陰莖.md "wikilink")，衍生出其他生殖系統疾病）

### 對運動員的影響

由於睪酮能提升體能，一些[運動員為保持體育狀態或表現](../Page/運動員.md "wikilink")，會於賽前使用睪酮。男運動員在賽前數小時，可以將睪酮注射[陰囊內](../Page/陰囊.md "wikilink")，激化身體的狀態。但現時大部份的體育項目都已把睪酮定為[禁藥](../Page/體育禁藥.md "wikilink")。

過度超量或是長期的使用睪酮，會導致[睪丸及](../Page/睪丸.md "wikilink")[男性生殖器萎縮](../Page/男性生殖器.md "wikilink")，且導致永久性的[陽痿](../Page/勃起功能障碍.md "wikilink")。如果為女性，則會出現[乳房萎縮](../Page/乳房.md "wikilink")、刺激[毛髮過度生長](../Page/體毛.md "wikilink")、[陰蒂肥大貌似小](../Page/陰蒂.md "wikilink")[陰莖和](../Page/陰莖.md "wikilink")[长胡子等種種男性化現象](../Page/有胡女性.md "wikilink")。

## 醫藥效用

睪酮首先是在1935年從一頭[雄牛中抽取](../Page/牛.md "wikilink")，經歷多年，已經發展了多種不同的醫藥種類。現時在[北美洲睪酮可以有注射式](../Page/北美洲.md "wikilink")（如環戊丙酸睪酮及庚酸睪酮）、口服的、口頰的及皮膚滲透式的包、膏與凝膠。

睪酮最初的用途是治療[性腺功能低下症](../Page/性腺功能低下症.md "wikilink")，即補充自體分泌不足或沒有自然分泌睪酮的病症。在[激素替代疗法](../Page/激素替代疗法.md "wikilink")（HRT）下適當地使用睪酮，可以維持[血液中的睪酮量維持在正常水平](../Page/血液.md "wikilink")。

與其他[激素一樣](../Page/激素.md "wikilink")，睪酮及其他[同化激素除了因缺乏而補充](../Page/同化激素.md "wikilink")，亦有其他用法，但相對的附帶著更大更多的副作用。如[不育](../Page/不育.md "wikilink")、性慾喪失（[性冷感](../Page/性冷感.md "wikilink")）或[陽痿](../Page/陽痿.md "wikilink")、[骨質疏鬆症等](../Page/骨質疏鬆症.md "wikilink")。1940年代後期，睪酮更被以抗衰老藥物為商業噱頭，情況與近代的[生長激素一樣](../Page/生长激素.md "wikilink")。

利用睪酮的男性化作用，[跨性别男性](../Page/跨性别.md "wikilink")（即由[女性經由變性成為](../Page/女性.md "wikilink")[男性](../Page/男性.md "wikilink")）會以睪酮作為替代，以維持至一般正常男性的睪酮水平。同樣地，[跨性别女性會使用藥物抑制睪酮的生成](../Page/跨性别者.md "wikilink")，以讓[雌激素發揮效用](../Page/雌激素.md "wikilink")。

## 生物合成

[孕烯醇酮是所有](../Page/孕烯醇酮.md "wikilink")[同化激素的前體](../Page/同化激素.md "wikilink")，亦是[膽固醇的衍生物](../Page/膽固醇.md "wikilink")，可以合成睪酮。合成睪酮可以從兩個方式進行，即δ-5方式及δ-4方式。δ-5方式是將孕烯醇酮轉化[脫氫表雄甾酮為](../Page/脫氫表雄甾酮.md "wikilink")[雄烯二酮](../Page/雄烯二酮.md "wikilink")。

[<File:Reaction-Progesterone-Androstendione.png>](https://zh.wikipedia.org/wiki/File:Reaction-Progesterone-Androstendione.png "fig:File:Reaction-Progesterone-Androstendione.png")

δ-4方式是將[妊娠素的C](../Page/妊娠素.md "wikilink")-17部份[羥基化得出](../Page/羥基化.md "wikilink")[醋酸羥孕酮](../Page/醋酸羥孕酮.md "wikilink")，再轉化為雄烯二酮。雄烯二酮就是睪酮的直接前體。將在C-17部份的[酮降為](../Page/酮.md "wikilink")[醇就可以得出睪酮](../Page/醇.md "wikilink")。睪酮就是[雌二醇的前體](../Page/雌二醇.md "wikilink")。

[<File:Reaction-Androstendione-Testosterone.png>](https://zh.wikipedia.org/wiki/File:Reaction-Androstendione-Testosterone.png "fig:File:Reaction-Androstendione-Testosterone.png")

在過程中加入[鋅睪酮的合成份量](../Page/鋅.md "wikilink")。鋅是[類固醇受體的運作有著重要影響](../Page/類固醇受體.md "wikilink")，且是能使睪酮合成的[酶的協助因素](../Page/酶.md "wikilink")。

## 参考文献

## 外部連結

  - [NIST entry for
    Testosterone](http://webbook.nist.gov/cgi/cbook.cgi?ID=C58220&Units=SI)
  - [NIST results of search for Testosterone (Shows
    androstenone.)](http://webbook.nist.gov/cgi/cbook.cgi?Name=testosterone&Units=SI)
  - [NIST Standard Reference
    Database](http://webbook.nist.gov/chemistry/)

{{-}}

[Category:雄激素类](../Category/雄激素类.md "wikilink")
[Category:睾丸激素](../Category/睾丸激素.md "wikilink")
[Category:卵巢激素](../Category/卵巢激素.md "wikilink")
[Category:肾上腺皮质的激素](../Category/肾上腺皮质的激素.md "wikilink")
[Category:勃起功能障碍药物](../Category/勃起功能障碍药物.md "wikilink")
[Category:神经内分泌学](../Category/神经内分泌学.md "wikilink")
[Category:合成代谢类固醇](../Category/合成代谢类固醇.md "wikilink")
[Category:人体激素](../Category/人体激素.md "wikilink")
[睾酮](../Category/睾酮.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")

1.