**瞼板腺**（）又稱**麥氏腺**（）是一種在[眼瞼周圍的特殊](../Page/眼瞼.md "wikilink")[皮脂腺](../Page/皮脂腺.md "wikilink")，位於之中。主要的功能為分泌[油脂層](../Page/油脂層.md "wikilink")，以延緩水液層的[蒸發](../Page/蒸發.md "wikilink")。此外油脂層還可維持淚液膜的[表面張力](../Page/表面張力.md "wikilink")，避免淚液流到臉頰，並可潤滑眼瞼及[眼球的接觸面](../Page/眼球.md "wikilink")，使眼睛閉起時，可維持眼皮和眼球之間維持氣密的狀態\[1\]。上眼皮大約有50個瞼板腺，而下眼皮大約有25個瞼板腺。

瞼板腺不健全會造成[乾眼症](../Page/乾眼症.md "wikilink")，這是一種很常見的[眼疾](../Page/眼疾.md "wikilink")。也因此可能造成[眼瞼炎](../Page/眼瞼炎.md "wikilink")，眼瞼炎是眼瞼邊緣[皮膚發炎的現象](../Page/皮膚發炎.md "wikilink")。它通常會影響雙[眼](../Page/眼.md "wikilink")，如果瞼板腺受到感染則會變成[瞼板腺炎](../Page/瞼板腺炎.md "wikilink")。阻塞物可以被細菌的[解脂酵素分解](../Page/解脂酵素.md "wikilink")，因而釋放[脂肪酸](../Page/脂肪酸.md "wikilink")，這些脂肪酸可以刺激眼睛，有時可以造成[角膜炎](../Page/角膜炎.md "wikilink")。

麥氏腺的名稱來自於一名[德國](../Page/德國.md "wikilink")[醫生](../Page/醫生.md "wikilink")（1638年-1700年）。

## 功能

### 麥氏層

瞼板腺的分泌物又稱為麥氏層（），本詞彙由尼古拉德斯（Nicolaides）等人於1981年首次提出\[2\]。油脂是麥氏層的最主要成分\[3\][人類的瞼板腺大約會分泌](../Page/人類.md "wikilink")90種的[蛋白質](../Page/蛋白質.md "wikilink")。\[4\]

## 參考資料

## 外部連結

  -
  - [Rethinking Meibomian Gland Dysfunction: How to Spot It, Stage It
    and Treat
    It](http://www.aao.org/publications/eyenet/201107/cornea.cfm)

[Category:眼解剖学](../Category/眼解剖学.md "wikilink")

1.  "eye, human." Encyclopædia Britannica. [Encyclopaedia Britannica
    Ultimate Reference
    Suite](../Page/Encyclopaedia_Britannica_Ultimate_Reference_Suite.md "wikilink").
    Chicago: Encyclopædia Britannica, 2010.

2.
3.

4.  Tsai PS, Evans JE, Green KM, Sullivan RM, Schaumberg DA, Richards
    SM, Dana MR, Sullivan DA. "Proteomic analysis of human meibomian
    gland secretions." *Br J Ophthalmol.* 2006 Mar;90(3):372-7. PMID
    16488965.