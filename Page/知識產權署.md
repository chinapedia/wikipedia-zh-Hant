[知識產權署商標註冊處.PNG](https://zh.wikipedia.org/wiki/File:知識產權署商標註冊處.PNG "fig:知識產權署商標註冊處.PNG")
**知識產權署**（[英語](../Page/英語.md "wikilink")：**I**ntellectual **P**roperty
**D**epartment，**IPD**）是[中華人民共和國](../Page/中華人民共和國.md "wikilink")[香港特別行政區政府](../Page/香港特別行政區政府.md "wikilink")[商務及經濟發展局轄下的](../Page/商務及經濟發展局.md "wikilink")[部門](../Page/香港特別行政區政府部門.md "wikilink")，於1990年成立，接管原屬[註冊總署的商標註冊處及專利權註冊處](../Page/註冊總署.md "wikilink")，並負責處理和提倡[知識產權相關的事務](../Page/知識產權.md "wikilink")。設有署長，署長之外，下設副署長、數個助理署長以及註冊處。現任署長為[梁家麗](../Page/梁家麗.md "wikilink")。截至2006年12月31日，該署已經處理了209,699個[商標註冊](../Page/商標.md "wikilink")，28,535項各式[專利及](../Page/專利.md "wikilink")28,988項[外觀設計](../Page/外觀設計.md "wikilink")。
知識產權署原隸屬[工商及科技局](../Page/工商及科技局.md "wikilink")，2007年7月1日[決策局重組後劃入新成立的](../Page/決策局.md "wikilink")[商務及經濟發展局](../Page/商務及經濟發展局.md "wikilink")。

## 爭議

2007年4月3日，該署在處理一宗[商標註冊個案時誤將申請人](../Page/商標.md "wikilink")[個人私隱全數上傳至網絡上](../Page/個人私隱.md "wikilink")，引起[私隱專員關注](../Page/香港個人資料私隱專員公署.md "wikilink")。署方已就事件公開道歉。

## 歷任署長

1.  [戴婉瑩](../Page/戴婉瑩.md "wikilink")（1990年－1994年）
2.  [謝肅方](../Page/謝肅方.md "wikilink")（1994年10月31日－2011年4月11日）
3.  [張錦輝](../Page/張錦輝.md "wikilink")（2011年4月12日－2014年5月15日）
4.  [梁家麗](../Page/梁家麗.md "wikilink")（2014年5月16日－）

## 参考文献

  - 知識產權署統計：http://www.ipd.gov.hk/chi/intellectual_property/ip_statistics/2006/statistics_chi.pdf

## 外部連結

  - [香港特別行政區政府 知識產權署](http://www.ipd.gov.hk/) 官方网站

{{-}}

[Category:知识产权相关政府机构](../Category/知识产权相关政府机构.md "wikilink")