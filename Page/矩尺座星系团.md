**矩尺座星系團**（**ACO 3627** or **Abell
3627**）是一個隱藏在銀河系[隱匿帶內](../Page/隱匿帶.md "wikilink")，富含星系的[星系集團](../Page/星系集團.md "wikilink")，靠近[巨引源所在的中心方向上](../Page/巨引源.md "wikilink")。\[1\]雖然他很接近又很明亮，但是他位於[隱匿帶](../Page/隱匿帶.md "wikilink")，也就是被[銀河系的](../Page/銀河系.md "wikilink")[平面遮蔽的區域](../Page/銀河座標系統.md "wikilink")，因而有些星系在可見光的波段中會被[星際塵埃遮蔽掉](../Page/星際塵埃.md "wikilink")，使得光度暗淡或是消失不見（看不見）。他的總質量估計是10<sup>15</sup>[太陽質量](../Page/太陽質量.md "wikilink")。\[2\].

## 參見

  - [ESO 137-001](../Page/ESO_137-001.md "wikilink")

## 外部連結

  - [Norma
    Cluster](http://nedwww.ipac.caltech.edu/cgi-bin/nph-objsearch?objname=Norma+Cluster)
    in the [NASA Extragalactic
    Database](../Page/NASA/IPAC河外星系資料庫.md "wikilink")
  - [ESO Press
    Photos 46a-j/99](http://www.eso.org/outreach/press-rel/pr-1999/phot-46-99.html)

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:矩尺座](../Category/矩尺座.md "wikilink")
[Category:大引力子](../Category/大引力子.md "wikilink")
[Category:矩尺座星系团](../Category/矩尺座星系团.md "wikilink")
[Category:長蛇-半人馬超星系團](../Category/長蛇-半人馬超星系團.md "wikilink")
[3627](../Category/阿貝爾天體.md "wikilink")

1.  [R. C. Kraan-Korteweg, in Lecture Notes in Physics 556, edited by D.
    Pageand J.G. Hirsch, p. 301（Springer,
    Berlin, 2000）.](http://arxiv.org/abs/astro-ph/0006199)
2.  \[<http://adsabs.harvard.edu/cgi-bin/bib_query?1996ApJ>...467..168B
    H. Boehringer *et al.*, *Astrophys. J.* **467**, 168 (1996).\], [R.
    C. Kraan-Korteweg *et al.*, *Nature* **379**, 519
    (1996).](http://www.nature.com/nature/journal/v379/n6565/abs/379519a0.html)