在數學，**矩陣理論**是一門研究[矩陣在](../Page/矩陣.md "wikilink")[數學上的應用的科目](../Page/數學.md "wikilink")。矩陣理論本來是[線性代數的一個小分支](../Page/線性代數.md "wikilink")，但其後由於陸續在[圖論](../Page/圖論.md "wikilink")、[代數](../Page/代數.md "wikilink")、[組合數學和](../Page/組合數學.md "wikilink")[統計上得到應用](../Page/統計.md "wikilink")，漸漸發展成為一門獨立的學科。

有關矩陣理論所用到的名詞的定義，請參考[矩陣理論專有名詞表](../Page/矩陣理論專有名詞表.md "wikilink")。

## 歷史

方陣如[幻方及](../Page/幻方.md "wikilink")[拉丁方陣的研究歷史悠久](../Page/拉丁方陣.md "wikilink")，最早的幻方出現於[中國的](../Page/中國.md "wikilink")[龜背圖上](../Page/龜背圖.md "wikilink")。

[萊布尼茲](../Page/萊布尼茲.md "wikilink")，微積分的始創者之一，首先在1693年利用[行列式來解題](../Page/行列式.md "wikilink")；而[加布里尔·克拉默率先利用行列式解](../Page/加布里尔·克拉默.md "wikilink")[聯立線性方程组](../Page/线性方程组求解.md "wikilink")，在1750年引進了[克莱姆法则](../Page/克莱姆法则.md "wikilink")。

於1800年年代，出現了由著名數學家[高斯發明的](../Page/高斯.md "wikilink")[高斯消去法](../Page/高斯消去法.md "wikilink")，以及比較慢的改良版本[高斯－約當消去法](../Page/高斯－約當消去法.md "wikilink")。

1848年[西爾維斯特率先使用](../Page/詹姆斯·約瑟夫·西爾維斯特.md "wikilink")“matrix”這個字。[阿瑟·凱萊](../Page/阿瑟·凱萊.md "wikilink")、[哈密頓](../Page/哈密頓.md "wikilink")、[格拉斯曼](../Page/赫尔曼·格拉斯曼.md "wikilink")、[弗罗贝尼乌斯及](../Page/费迪南德·格奥尔格·弗罗贝尼乌斯.md "wikilink")[馮·諾伊曼都是對矩陣理論有貢獻的著名數學家](../Page/馮·諾伊曼.md "wikilink")。

## 簡介

[矩陣是一個矩形的數學方陣](../Page/矩阵.md "wikilink")。一個方陣可看作兩個[矢量空間的](../Page/矢量空間.md "wikilink")[線性變陣](../Page/線性變陣.md "wikilink")，故矩陣理論可當作[線性代數的一個分枝](../Page/線性代數.md "wikilink")。

在[圖論](../Page/圖論.md "wikilink")，每一個加上標示[圖對應唯一的](../Page/圖.md "wikilink")[非負矩陣](../Page/非負矩陣.md "wikilink")，稱為[鄰接矩陣.](../Page/邻接矩阵.md "wikilink")

[排列矩陣是](../Page/排列矩陣.md "wikilink")[排列的矩陣表達式](../Page/排列.md "wikilink")，在[組合數學極為重要](../Page/組合數學.md "wikilink")。

[正定矩陣及](../Page/正定矩陣.md "wikilink")[半正定矩陣可用來尋找](../Page/半正定矩陣.md "wikilink")[實數](../Page/實數.md "wikilink")[函數的極大值或極小值](../Page/函數.md "wikilink")。

任意[環矩陣亦非常重要](../Page/環.md "wikilink")。舉例說，多項式環的矩陣用於[控制理論](../Page/控制理論.md "wikilink")。

另外，不同的矩陣環經常是提供數學上反例的素材。

## 常用的理論

  - [凱萊-哈密頓定理](../Page/凱萊-哈密頓定理.md "wikilink")
  - [高斯－约当消去法](../Page/高斯－约当消去法.md "wikilink")
  - [QR分解法](../Page/QR分解法.md "wikilink")
  - [奇異值分解](../Page/奇異值分解.md "wikilink")

## 外部連結

  - [A Brief History of Linear Algebra and Matrix
    Theory](http://www.uoregon.edu/~vitulli/341.fall01/lin.alg.history.html)

[ar:نظرية المصفوفات](../Page/ar:نظرية_المصفوفات.md "wikilink")
[es:Teoría de matrices](../Page/es:Teoría_de_matrices.md "wikilink")

[J](../Category/矩陣論.md "wikilink")