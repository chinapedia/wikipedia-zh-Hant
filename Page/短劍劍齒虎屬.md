**短劍劍齒虎**（*Machairodus*），又名**短劍虎**，是生存於1500-200萬年前[歐洲](../Page/歐洲.md "wikilink")、[亞洲](../Page/亞洲.md "wikilink")、[非洲及](../Page/非洲.md "wikilink")[北美洲的大型](../Page/北美洲.md "wikilink")[劍齒虎亞科](../Page/劍齒虎亞科.md "wikilink")。短劍劍齒虎的[物種在體型及比例上有很大的差異](../Page/物種.md "wikilink")，最大約有[獅子的大小](../Page/獅子.md "wikilink")。牠們有很大的[犬齒](../Page/犬齒.md "wikilink")，齒上有鋸齒，但卻在幾年內就會磨損。\[1\]

短劍劍齒虎有可能是[似劍齒虎的祖先](../Page/似劍齒虎.md "wikilink")。短劍劍齒虎與所有[貓科都是從](../Page/貓科.md "wikilink")[原小熊貓](../Page/原小熊貓.md "wikilink")[演化而來](../Page/演化.md "wikilink")。

## 已描述的物種

  - 非洲短劍劍齒虎（*M. africanus*）
  - 阿芬短劍劍齒虎（*M. aphanistus*）
  - 巨型短劍劍齒虎（*M. giganteus*）
  - *M. oradensis*
  - *M. colorandensis*
  - *M. transvaalensis*
  - *M. alberdiae*
  - *M. copei*
  - *M. laskarevi*
  - *M. irtyschensis*
  - *M. kurteni*
  - *M. fires*
  - *M. ischimicus*
  - *M. schlosseri*
  - *M. palanderi*
  - *M. palmidens*
  - *M. inexpectatus*
  - *M. robinsoni* ?

## 參考文獻

[Category:中新世動物](../Category/中新世動物.md "wikilink")
[Category:中新世哺乳類](../Category/中新世哺乳類.md "wikilink")
[Category:短劍劍齒虎屬](../Category/短劍劍齒虎屬.md "wikilink")

1.