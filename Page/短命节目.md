**短命节目**，与**[长寿节目](../Page/长寿节目.md "wikilink")**相对。指因收视较低或特殊原因很快停播的[电视或](../Page/电视.md "wikilink")[广播节目](../Page/广播.md "wikilink")。有时也包括漫画连载等。由于电视领域竞争激烈，短命节目有增多的趋势。

## 知名的短命节目

###

  - [華視](../Page/華視.md "wikilink")1987年[八點檔](../Page/八點檔.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")《[圓環兄弟情](../Page/圓環兄弟情.md "wikilink")》：由於本劇製作單位與華視談判破裂，本劇只播出6集就被腰斬，創下當時紀錄。
  - [台視綜藝節目](../Page/台視.md "wikilink")-{《天王天后》}-：[胡瓜](../Page/胡瓜.md "wikilink")、[董至成主持](../Page/董至成.md "wikilink")。只播了兩集。2002年9月7日\~9月14日週六20:00\~21:55播出。
  - [台視週一晚間益智綜藝節目](../Page/台視.md "wikilink")《非常挑戰》：1997年4月14日至1997年7月28日，每週一21:00\~22:00播出。[高怡平](../Page/高怡平.md "wikilink")、[呂珍儀主持](../Page/呂珍儀.md "wikilink")。由於高怡平以「壓力過大」（高怡平自言，錄影前一天入睡前必須背誦題目，錄影時要用[機關槍的速度來提問](../Page/機關槍.md "wikilink")）為由請辭，播出十集之後就停播了。
  - [華視綜藝節目](../Page/華視.md "wikilink")《環遊世界80天之我要玩下去》：[Ben](../Page/白吉勝.md "wikilink")、[麻衣](../Page/麻衣.md "wikilink")、[申東靖](../Page/申東靖.md "wikilink")、[胡嘉愛主持](../Page/胡嘉愛.md "wikilink")。2008年6月27日至8月8日週五22:00-24:00[現場直播](../Page/現場直播.md "wikilink")，共播出7集後便因收視率太低而停播。
  - [華視綜藝節目](../Page/華視.md "wikilink")《幸福A計畫》：[徐乃麟](../Page/徐乃麟.md "wikilink")、[小鐘](../Page/小鐘.md "wikilink")、[趙虹喬主持](../Page/趙虹喬.md "wikilink")。2009年4月10日至6月5日週五20:00-22:00播出，共播出9集後便因收視率太低而停播。
  - [華視綜藝節目](../Page/華視.md "wikilink")《[寶島縱貫線](../Page/寶島縱貫線.md "wikilink")》：[吳宗憲](../Page/吳宗憲.md "wikilink")、[萱野可芬](../Page/萱野可芬.md "wikilink")、[萱野可芳主持](../Page/萱野可芳.md "wikilink")。2014年7月5日週六20:00-22:00播出第一集後，製作公司[映畫傳播以三大理由主動向華視提出停播要求](../Page/映畫傳播.md "wikilink")，因此本節目僅播出一集。

###

  - [麗的電視](../Page/麗的電視.md "wikilink")（RTV）《[哈囉夜歸人](../Page/哈囉夜歸人.md "wikilink")》因討論話題大膽，只播出5集就被[港英政府禁播](../Page/港英政府.md "wikilink")。

<!-- end list -->

  - [無綫電視](../Page/無綫電視.md "wikilink")（TVB）2003年外購電視劇《[還珠格格III天上人間](../Page/還珠格格III天上人間.md "wikilink")》、2007年外購電視劇《[一生為奴](../Page/一生為奴.md "wikilink")》，都是因收視不理想，播出不久便遭腰斬。

<!-- end list -->

  - [無綫電視](../Page/無綫電視.md "wikilink")（TVB）节目《[爆足一周](../Page/爆足一周.md "wikilink")》，因为劣评如潮，在2012年10月28日播放第四集后腰斩。

<!-- end list -->

  - [美亞電視](../Page/美亞電視.md "wikilink")（MATV）旗下的美亞電視劇台2013年5月在BOYABO網站播出的《大生活》、《家有公婆》，播出不久因突置換為LSTV頻道而被腰斬。

[Category:電視術語](../Category/電視術語.md "wikilink")