**短尾蛇屬**（[學名](../Page/學名.md "wikilink")：*Brachyophis*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[穴蝰科下的一個](../Page/穴蝰科.md "wikilink")[單型](../Page/單型.md "wikilink")[屬](../Page/屬.md "wikilink")，屬下只有**短尾蛇**（*B.
revoili*）一種[有毒蛇種](../Page/毒蛇.md "wikilink")，主要分布於[非洲](../Page/非洲.md "wikilink")，目前共有3個亞種已被確認。\[1\]

## 亞種

| 亞種\[2\]           | 學名及命名者\[3\]                                              | 異稱 | 地理分布 |
| ----------------- | -------------------------------------------------------- | -- | ---- |
| **B. r. cornii**  | Brachyophis revoili cornii，<small>Scortecci，1932</small> |    |      |
| **B. r. krameri** | Brachyophis revoili krameri，<small>Lanza，1966</small>    |    |      |
| **B. r. revoili** | Brachyophis revoili revoili，<small>Mocquard，1888</small> |    |      |
|                   |                                                          |    |      |

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：短尾蛇屬](http://reptile-database.reptarium.cz/species.php?genus=Brachyophis&species=revoili)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:穴蝰科](../Category/穴蝰科.md "wikilink")

1.

2.
3.