**短途馬錦標**（**Sprinters
Stakes**），是[日本中央競馬會舉辦的短途賽事](../Page/日本中央競馬會.md "wikilink")，於[中山競馬場](../Page/中山競馬場.md "wikilink")[草地舉行](../Page/草地.md "wikilink")，路程草地1200米，3歲或以上馬匹可參與的賽事。總獎金為2億235萬[日圓](../Page/日圓.md "wikilink")\[1\]。

## 歷史

賽事由1967年開始舉辦，初時在9月舉行。1968年改為3月舉行，1969年至1980年在9月未及10月初舉行，1981年起在2月及3月期間舉行，1984年[日本中央競馬會實行級際賽際度時為三級賽](../Page/日本中央競馬會.md "wikilink")，1990年（平成2年）升格為日本一級賽，將舉行日期定為12月中下旬（[有馬紀念賽馬日一星期前舉行](../Page/有馬紀念.md "wikilink")）。1994年開放給外國馬匹參與。2000年，賽事改回9月下旬及10月第一個星期舉行。2002年，由於[東京競馬場的賽期移師中山競馬場舉行](../Page/東京競馬場.md "wikilink")，於是該屆賽事由[新潟競馬場代辦](../Page/新潟競馬場.md "wikilink")。

2005年成為[世界短途挑戰賽](../Page/世界短途挑戰賽.md "wikilink")（Global Sprint
Challenge）其中一站，上一站是日本[阪神競馬場舉行的](../Page/阪神競馬場.md "wikilink")[人馬錦標](../Page/人馬錦標.md "wikilink")，下一站是[柏天尼經典賽](../Page/柏天尼經典賽.md "wikilink")。2005年10月2日，[精英大師勝出賽事](../Page/精英大師.md "wikilink")，是首匹外國馬勝出此項大賽。

2006年成為國際一級賽，[兼併目標成為當屆賽事的冠軍](../Page/兼併目標.md "wikilink")。

2010年香港馬[極奇妙以一放到底的姿態成為第二匹勝出此賽的香港馬](../Page/極奇妙.md "wikilink")，蹄速1:07.4（Flying
Time計時）。

2011年由[真機伶勝出](../Page/真機伶.md "wikilink")，但香港代表[天久於直路上被](../Page/天久.md "wikilink")[安藤勝己策騎的](../Page/安藤勝己.md "wikilink")[三人共舞嚴重干擾](../Page/三人共舞.md "wikilink")。而在賽後的研訊中未有對人馬作出處罰，更無更改賽果，與[綠色駿威於](../Page/綠色駿威.md "wikilink")[人馬錦標發生碰撞事件的處理手法完全不同](../Page/人馬錦標.md "wikilink")。賽後連直播主持亦百思不得其解。

## 歷屆冠軍

| **年份** | **勝出馬匹**                                  | **年齡** | **騎師**                             | **練馬師**                              | **馬主**                                | **時間** |
| ------ | ----------------------------------------- | ------ | ---------------------------------- | ------------------------------------ | ------------------------------------- | ------ |
| 1994年  | [櫻花進王](../Page/櫻花進王.md "wikilink")        | 5      | [小島太](../Page/小島太.md "wikilink")   | [境勝太郎](../Page/境勝太郎.md "wikilink")   | [櫻花商事](../Page/櫻花商事.md "wikilink")    | 1:07.1 |
| 1995年  | [菱曙](../Page/菱曙.md "wikilink")            | 3      | [角田晃一](../Page/角田晃一.md "wikilink") | [佐山優](../Page/佐山優.md "wikilink")     | [阿部雅一郎](../Page/阿部雅一郎.md "wikilink")  | 1:08.1 |
| 1996年  | [花公園](../Page/花公園.md "wikilink")          | 4      | [田元勝貴](../Page/田元勝貴.md "wikilink") | [松元省一](../Page/松元省一.md "wikilink")   | [吉田勝己](../Page/吉田勝己.md "wikilink")    | 1:08.8 |
| 1997年  | [大樹快車](../Page/大樹快車.md "wikilink")        | 3      | [岡部幸雄](../Page/岡部幸雄.md "wikilink") | [藤澤和雄](../Page/藤澤和雄.md "wikilink")   | [大樹牧場](../Page/大樹牧場.md "wikilink")    | 1:07.8 |
| 1998年  | [礦之戀](../Page/礦之戀.md "wikilink")          | 3      | [吉田豐](../Page/吉田豐.md "wikilink")   | [稻葉隆一](../Page/稻葉隆一.md "wikilink")   | Thoroughbred Club Ruffian             | 1:08.6 |
| 1999年  | [黑鷹](../Page/黑鷹.md "wikilink")            | 5      | [橫山典弘](../Page/橫山典弘.md "wikilink") | [國枝榮](../Page/國枝榮.md "wikilink")     | [金子真人](../Page/金子真人.md "wikilink")    | 1:08.2 |
| 2000年  | [大德大和](../Page/大德大和.md "wikilink")        | 6      | [江田照男](../Page/江田照男.md "wikilink") | [石坂正](../Page/石坂正.md "wikilink")     | [中村和子](../Page/中村和子.md "wikilink")    | 1:08.6 |
| 2001年  | [多樂星](../Page/多樂星.md "wikilink")          | 5      | [蛯名正義](../Page/蛯名正義.md "wikilink") | [中野榮治](../Page/中野榮治.md "wikilink")   | [高野稔](../Page/高野稔.md "wikilink")      | 1:07.0 |
| 2002年  | [信念](../Page/信念_\(馬\).md "wikilink")      | 4      | [武豐](../Page/武豐.md "wikilink")     | [松元茂樹](../Page/松元茂樹.md "wikilink")   | [前田幸治](../Page/前田幸治.md "wikilink")    | 1:07.7 |
| 2003年  | [多旺達](../Page/多旺達.md "wikilink")          | 4      | [池添謙一](../Page/池添謙一.md "wikilink") | [坂田正大](../Page/坂田正大.md "wikilink")   | [吉田照哉](../Page/吉田照哉.md "wikilink")    | 1:08.0 |
| 2004年  | [金鎮之光](../Page/金鎮之光.md "wikilink")        | 6      | [大西直宏](../Page/大西直宏.md "wikilink") | [大根田裕之](../Page/大根田裕之.md "wikilink") | [清水貞光](../Page/清水貞光.md "wikilink")    | 1:09.9 |
| 2005年  | [精英大師](../Page/精英大師.md "wikilink")        | 6      | [高雅志](../Page/高雅志.md "wikilink")   | [告東尼](../Page/告東尼.md "wikilink")     | [施雅治先生及夫人](../Page/施雅治.md "wikilink") | 1:07.3 |
| 2006年  | [兼併目標](../Page/兼併目標.md "wikilink")        | 7      | [福達](../Page/福達.md "wikilink")     | [張奕](../Page/張奕.md "wikilink")       | 張奕                                    | 1:08.1 |
| 2007年  | [真弓快車](../Page/真弓快車.md "wikilink")        | 3      | [中舘英二](../Page/中舘英二.md "wikilink") | [石坂正](../Page/石坂正.md "wikilink")     | 戶佐真弓                                  | 1:09.4 |
| 2008年  | [不眠之夜](../Page/不眠之夜_\(賽馬\).md "wikilink") | 4      | [上村洋行](../Page/上村洋行.md "wikilink") | [橋口弘次郎](../Page/橋口弘次郎.md "wikilink") | Sunday Racing                         | 1:08.0 |
| 2009年  | [桂冠戰士](../Page/桂冠戰士.md "wikilink")        | 5      | [藤田伸二](../Page/藤田伸二.md "wikilink") | [昆貢](../Page/昆貢.md "wikilink")       | Laural Racing                         | 1:07.5 |
| 2010年  | [極奇妙](../Page/極奇妙.md "wikilink")          | 8      | [黎海榮](../Page/黎海榮.md "wikilink")   | [姚本輝](../Page/姚本輝.md "wikilink")     | [林大輝](../Page/林大輝.md "wikilink")      | 1:07.4 |
| 2011年  | [真機伶](../Page/真機伶.md "wikilink")          | 4      | [池添謙一](../Page/池添謙一.md "wikilink") | [安田隆行](../Page/安田隆行.md "wikilink")   | 鈴木隆司                                  | 1:07.4 |
| 2012年  | [龍王](../Page/龍王_\(馬\).md "wikilink")      | 4      | [岩田康誠](../Page/岩田康誠.md "wikilink") | [安田隆行](../Page/安田隆行.md "wikilink")   | Lord Horse Club                       | 1:06.7 |
| 2013年  | [龍王](../Page/龍王_\(馬\).md "wikilink")      | 5      | [岩田康誠](../Page/岩田康誠.md "wikilink") | [安田隆行](../Page/安田隆行.md "wikilink")   | Lord Horse Club                       | 1:07.2 |
| 2014年  | 瑞草祥龍                                      | 6      | 大野拓弥                               | 高木登                                  | 岡田牧雄                                  | 1:08.8 |
| 2015年  | 真誠少女                                      | 6      | 戶崎圭太                               | 藤原英昭                                 | 廣崎利洋HD（株）                             | 1:08.1 |
| 2016年  | 彎刀赤駿                                      | 5      | 杜滿萊                                | 尾関知人                                 | Tokyo Horse Racing                    | 1:07.6 |
| 2017年  | 彎刀赤駿                                      | 6      | 杜滿萊                                | 尾関知人                                 | Tokyo Horse Racing                    | 1:07.6 |

### 1967年-1993年勝出馬匹

由於不是所有馬匹擁有中文譯名，因此大部份馬名會使用英文原名代替

<div style="font-size:90%; width:33%; float:left">

  - 1967年 Onward Hill
  - 1968年 Suzu Hayate
  - 1969年 Takeshiba O
  - 1970年 Tamami
  - 1971年 Kensachi
  - 1972年 Noboru Toko
  - 1973年 Kyoei Green
  - 1974年 Sakura Iwai
  - 1975年 Sakura Iwai

</div>

<div style="font-size:90%; width:33%; float:left">

  - 1976年 Jumbo King
  - 1977年 Meiwa Kimiko
  - 1978年 Meiwa Kimiko
  - 1979年 Sunny Flower
  - 1981年 Sakura God
  - 1982年 Sakura Shingeki
  - 1983年 Shin Wolf
  - 1984年 Happy Progress
  - 1985年 Marutaka Storm

</div>

<div style="font-size:90%; width:33%; float:left">

  - 1986年 Dokan Tesco
  - 1987年 King Frolic
  - 1988年 Dyna Actress
  - 1989年 Winning Smile
  - 1990年 [青竹回憶](../Page/青竹回憶.md "wikilink")
  - 1991年 [第一紅寶](../Page/第一紅寶.md "wikilink")
  - 1992年 [西野花](../Page/西野花.md "wikilink")
  - 1993年 櫻花進王

</div>

## 参考資料

[Category:日本賽馬](../Category/日本賽馬.md "wikilink")
[Category:日本體育](../Category/日本體育.md "wikilink")

1.  [JRA International
    Races 2007](http://japanracing.jp/_races/2007races/0930sprinters.html)