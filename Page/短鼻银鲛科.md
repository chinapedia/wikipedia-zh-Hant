**短鼻銀鮫科**（學名*Chimaeridae*），又名**银鲛科**，是[全頭亞綱](../Page/全頭亞綱.md "wikilink")[銀鮫目中的一科](../Page/銀鮫目.md "wikilink")。

## 分類

**短鼻銀鮫科**下分2個屬，如下：

### 銀鮫屬(*Chimaera*)

  -   - [巴哈馬銀鮫](../Page/巴哈馬銀鮫.md "wikilink")(*Chimaera bahamaensis*)
      - [古巴銀鮫](../Page/古巴銀鮫.md "wikilink")(*Chimaera cubana*)
      - [喬丹氏銀鮫](../Page/喬丹氏銀鮫.md "wikilink")(*Chimaera jordani*)：又稱喬氏銀鮫。
      - [中間銀鮫](../Page/中間銀鮫.md "wikilink")(*Chimaera media*)
      - [大西洋銀鮫](../Page/大西洋銀鮫.md "wikilink")(*Chimaera monstrosa*)
      - [歐氏銀鮫](../Page/歐氏銀鮫.md "wikilink")(*Chimaera owstoni*)
      - [豹斑銀鮫](../Page/豹斑銀鮫.md "wikilink")(*Chimaera panthera*)
      - [黑線銀鮫](../Page/黑線銀鮫.md "wikilink")(*Chimaera phantasma*)：又稱銀鮫。
      - [擬大西洋銀鮫](../Page/擬大西洋銀鮫.md "wikilink")(*Chimaera
        pseudomonstrosa*)

### 兔銀鮫屬(*Hydrolagus*)

  -   - [深水兔銀鮫](../Page/深水兔銀鮫.md "wikilink")(*Hydrolagus affinis*)
      - [非洲兔銀鮫](../Page/非洲兔銀鮫.md "wikilink")(*Hydrolagus africanus*)
      - [大眼兔銀鮫](../Page/大眼兔銀鮫.md "wikilink")(*Hydrolagus alberti*)
      - [斑點兔銀鮫](../Page/斑點兔銀鮫.md "wikilink")(*Hydrolagus barbouri*)
      - [科氏兔銀鮫](../Page/科氏兔銀鮫.md "wikilink")(*Hydrolagus colliei*)
      - [迪氏兔銀鮫](../Page/迪氏兔銀鮫.md "wikilink")(*Hydrolagus deani*)
      - [黑兔銀鮫](../Page/黑兔銀鮫.md "wikilink")(*Hydrolagus eidolon*)
      - [鬼形兔銀鮫](../Page/鬼形兔銀鮫.md "wikilink")(*Hydrolagus lemures*)
      - [大目兔銀鮫](../Page/大目兔銀鮫.md "wikilink")(*Hydrolagus
        macrophthalmus*)
      - [黑身兔銀鮫](../Page/黑身兔銀鮫.md "wikilink")（*Hydrolagus melanophasma*）
      - [暗紫兔銀鮫](../Page/暗紫兔銀鮫.md "wikilink")(*Hydrolagus mirabilis*)
      - [箕作氏兔銀鮫](../Page/箕作氏兔銀鮫.md "wikilink")(*Hydrolagus
        mitsukurii*)：又稱箕竹兔銀鮫。
      - [新西蘭兔銀鮫](../Page/新西蘭兔銀鮫.md "wikilink")(*Hydrolagus
        novaezealandiae*)
      - [奧氏兔銀鮫](../Page/奧氏兔銀鮫.md "wikilink")(*Hydrolagus ogilbyi*)
      - [蒼白兔銀鮫](../Page/蒼白兔銀鮫.md "wikilink")(*Hydrolagus pallidus*)
      - [紫色兔銀鮫](../Page/紫色兔銀鮫.md "wikilink")(*Hydrolagus purpurescens*)
      - [藍頭圓吻銀鮫](../Page/藍頭圓吻銀鮫.md "wikilink")(*Hydrolagus trolli*)： 幽靈鯊

[\*](../Category/短鼻銀鮫科.md "wikilink")