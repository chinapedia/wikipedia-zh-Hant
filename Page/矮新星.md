[Z_Camelopardalis_UV.jpg](https://zh.wikipedia.org/wiki/File:Z_Camelopardalis_UV.jpg "fig:Z_Camelopardalis_UV.jpg")

**矮新星**，或**雙子座U型變星**是[激變變星的一種](../Page/激變變星.md "wikilink")，是有來自伴星的物質堆積的[吸積盤和](../Page/吸積盤.md "wikilink")[白矮星的](../Page/白矮星.md "wikilink")[聯星系統](../Page/聯星.md "wikilink")。它們與傳統的[新星相似](../Page/新星.md "wikilink")，雖然都有白矮星週期性的爆發介入，但是機制是不同的：傳統的新星是因為累積的氫融合和爆發，而矮新星是因為吸積盤的不穩定。當盤中的氣體達到臨界[溫度時](../Page/溫度.md "wikilink")，會造成[黏滯性的改變](../Page/黏滯性.md "wikilink")，導致盤的崩潰而墜落至白矮星上，釋放出大量的[重力](../Page/重力.md "wikilink")[位能](../Page/位能.md "wikilink")。
[1](https://web.archive.org/web/20080226032415/http://home.mindspring.com/~mikesimonsen/cvnet/id1.html)

矮新星還有其它與傳統新星不同的特徵：它們的[光度低](../Page/光度.md "wikilink")，和從數天至數十天的週期。\[[https://web.archive.org/web/20080226032415/http://home.mindspring.com/\~mikesimonsen/cvnet/id1.html\]爆發時增加的的光度和再現的間隔與軌道週期有關](https://web.archive.org/web/20080226032415/http://home.mindspring.com/~mikesimonsen/cvnet/id1.html%5D爆發時增加的的光度和再現的間隔與軌道週期有關)；[哈伯太空望遠鏡近來的研究認為後者](../Page/哈伯太空望遠鏡.md "wikilink")（軌道週期）的關係可能使矮新星成為測量宇宙尺度距離有效的[標準燭光](../Page/標準燭光.md "wikilink")。
[2](https://web.archive.org/web/20080226032415/http://home.mindspring.com/~mikesimonsen/cvnet/id1.html),(S\&T)

雙子座U(UG)有三種子分類\[1\]：

1.  [天鵝座SS](../Page/天鵝座SS.md "wikilink")
    (UGSS)：[V光度會在](../Page/V通帶.md "wikilink")1-2天內增加2-6[星等](../Page/星等.md "wikilink")，並且在之後的幾天內回到原來的光度。
2.  **大熊座SU**
    (UGSU)：在正常爆發之外，有更明亮和更長期的"超級極大"爆發，或是"超級爆發"。大熊座SU還包括[大熊座ER和](../Page/大熊座ER.md "wikilink")[天箭座WZ兩種次級類型](../Page/天箭座WZ.md "wikilink")\[2\]。
3.  **[鹿豹座Z](../Page/鹿豹座Z.md "wikilink")**(UGZ)：會在一個比峰質光度略低的特定光度上暫時停留一段時間的矮新星。

## 參考資料

  - "Calibrating Dwarf Novae". *Sky & Telescope*, September 2003, p. 20.
  - [CVnet: "Introduction to CVs"
    (Accessed 4/17/06)](https://web.archive.org/web/20080226032415/http://home.mindspring.com/~mikesimonsen/cvnet/id1.html)
  - [Eric Weisstein's World of Astronomy: "Dwarf Nova".
    (Accessed 4/17/06)](http://scienceworld.wolfram.com/astronomy/DwarfNova.html)

## 參考資料

## 外部連結

  - [Spaceflight Now: "New Method of Estimated Dwarf Novae
    Distances", 5/30/03.
    (Accessed 4/17/06)](http://spaceflightnow.com/news/n0305/30distance/)

## 相關條目

  - [極超新星](../Page/極超新星.md "wikilink")（Hypernova）
  - [超新星](../Page/超新星.md "wikilink")
  - [新星](../Page/新星.md "wikilink")

[矮新星](../Category/矮新星.md "wikilink")

1.  <http://www.daviddarling.info/encyclopedia/U/U_Geminorum_star.html>
2.  <http://www.daviddarling.info/encyclopedia/S/SU_Ursae_Majoris_star.html>