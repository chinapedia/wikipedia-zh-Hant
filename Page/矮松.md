**矮松**（[學名](../Page/學名.md "wikilink")：**），又名**北美二針松**、**維吉尼亞松**或**紐澤西松**，是中型的[樹](../Page/樹.md "wikilink")，由[紐約南部的](../Page/紐約.md "wikilink")[長島向南經](../Page/長島_\(紐約\).md "wikilink")[阿巴拉契亞山脈至西](../Page/阿巴拉契亞山脈.md "wikilink")[田納西州及](../Page/田納西州.md "wikilink")[阿拉巴馬都可以找到](../Page/阿拉巴馬.md "wikilink")。矮松一般高9-18米，在良好的環境下可以生長得更高。樹幹[直徑達半米](../Page/直徑.md "wikilink")。矮松喜歡良好排水的[土壤](../Page/土壤.md "wikilink")，在較為貧瘠及沙質的土壤會生長得較快，但卻較為矮小。它的壽命約有65-90年。[樹葉形狀簡單](../Page/樹葉.md "wikilink")。

矮松的針葉較短，只有4-8厘米長，呈黃綠色，一般都是成對及扭曲的。松果長4-7厘米，會掛在樹上多年，第二年就會放出[種子](../Page/種子.md "wikilink")。一些矮松的生長習慣會扭曲樹幹。

矮松適合用來重新造林，並作為野生動物的生活地方。它亦會被用作[聖誕樹](../Page/聖誕樹.md "wikilink")、[木漿及木料](../Page/木漿.md "wikilink")。

## 參考

## 外部連結

  - [矮松的資料及分佈](http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=200005370)
  - [矮松的圖片](http://www.cas.vanderbilt.edu/bioimages/species/frame/pivi2.htm)

[Category:松属](../Category/松属.md "wikilink")