**石姓**是[東亞文化圈的](../Page/東亞文化圈.md "wikilink")[姓氏之一](../Page/姓氏.md "wikilink")，在古籍《[百家姓](../Page/百家姓.md "wikilink")》中排名第188位，在[中国大陆人口排名第](../Page/中国大陆.md "wikilink")81位（2006年统计数据）；韓國人口排名第68位，使用人口約56500（석（韓國姓氏石通昔，為新羅三大王姓））；臺灣人口排名第68位；日本人口排名4722。

## 姓氏源头

  - 出自姬姓，为石碏之后裔。据《[元和姓纂](../Page/元和姓纂.md "wikilink")》及《春秋公子谱》等所载，春秋时卫靖伯之孙石碏，有大功于卫，世为卫大夫，石碏之孙骀仲，以祖父字为氏。
  - 出自他姓，以字为氏。据《春秋公子谱》所载，春秋时宋公子段，字子石；春秋时郑公子丰有子名公孙段，字子石。两者其后皆以字为氏。
  - 出自他族加入或他族改石姓的。隋唐时期“昭武九姓”之一，当时西域石国（今[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")[塔什干一带](../Page/塔什干.md "wikilink")）有人迁居中原，改为石姓；据《[魏书](../Page/魏书.md "wikilink")·官氏志》所载，北魏孝文帝汉化改革时，将鲜卑乌石兰氏改单姓石；据《[后赵录](../Page/十六國春秋.md "wikilink")》所载，十六国[张訇督改为石会](../Page/张訇督.md "wikilink")，[冉闵改为石闵](../Page/冉闵.md "wikilink")；据《[北史](../Page/北史.md "wikilink")》所载，有娄姓改为石姓；上党石姓，羯人之姓，其先为匈奴别部，后散居上党、武乡；[石敬瑭之四代祖](../Page/石敬瑭.md "wikilink")[石璟](../Page/石璟.md "wikilink")，为沙陀部人，石敬塘冒姓石；G、据《九国志》所载，万州石姓系出[波斯](../Page/波斯.md "wikilink")，五代十国时前蜀利州司马[石处温是也](../Page/石处温.md "wikilink")；金时女真人斡勒氏、石盏氏改单姓石；今侗、水、阿昌、满、拉祜、回、土家、东乡、黎、羌、蒙族有此姓。

## 名人

### 古代

  - [石碏](../Page/石碏.md "wikilink")：石姓始祖，春秋时代的卫国贤臣
  - [石申](../Page/石申.md "wikilink")：战国时期[魏国天文学家](../Page/魏国.md "wikilink")，《[甘石星经](../Page/甘石星经.md "wikilink")》作者之一
  - [石奋](../Page/石奋.md "wikilink")：西汉大臣，号称“万石君”
  - [石庆](../Page/石庆.md "wikilink")：石奋之子，汉武帝时曾任丞相
  - [石苞](../Page/石苞.md "wikilink")：司马昭亲信，[晋朝开国功臣](../Page/晋朝.md "wikilink")
  - [石崇](../Page/石崇.md "wikilink")：石苞之子，因极度奢侈而「名留青史」
  - [石鑒](../Page/石鑒_\(西晉\).md "wikilink")：西晉太尉
  - [石冰](../Page/石冰.md "wikilink")：西晋[荆州蛮族首领](../Page/荆州.md "wikilink")，起兵反晋失败
  - [五胡十六国](../Page/五胡十六国.md "wikilink")[后赵的皇族](../Page/后赵.md "wikilink")：
      - [石邪](../Page/石邪.md "wikilink")、[石周曷朱](../Page/石周曷朱.md "wikilink")、[石勒](../Page/石勒.md "wikilink")、[石弘](../Page/石弘.md "wikilink")、[石寇觅](../Page/石寇觅.md "wikilink")、[石虎](../Page/石虎.md "wikilink")、[石世](../Page/石世.md "wikilink")、[石遵](../Page/石遵.md "wikilink")、[石鉴](../Page/石鉴.md "wikilink")、[石祗](../Page/石祗.md "wikilink")
  - 五代[后晋皇族](../Page/后晋.md "wikilink")：
      - [石敬瑭](../Page/石敬瑭.md "wikilink")、[石重贵](../Page/石重贵.md "wikilink")
  - [石恪](../Page/石恪.md "wikilink")：[五代画家](../Page/五代.md "wikilink")
  - [石守信](../Page/石守信.md "wikilink")：[北宋将领](../Page/北宋.md "wikilink")，开国功臣
  - [石亨](../Page/石亨.md "wikilink")：[明朝政治家](../Page/明朝.md "wikilink")
  - [石敬岩](../Page/石敬岩.md "wikilink")：明代武術家
  - [石达开](../Page/石达开.md "wikilink")：清代[太平天国将领](../Page/太平天国.md "wikilink")，起兵反清
  - [石柳邓](../Page/石柳邓.md "wikilink")：清代[苗族首领](../Page/苗族.md "wikilink")，起兵反清
  - [石玉昆](../Page/石玉昆.md "wikilink")：清代[说书名家](../Page/说书.md "wikilink")，擅长《[龙图公案](../Page/龙图公案.md "wikilink")》
  - [石宪章](../Page/石宪章.md "wikilink")：中国书法家

### 現代

  - [石广生](../Page/石广生.md "wikilink")：中华人民共和国前国家外贸部长
  - [石富宽](../Page/石富宽.md "wikilink")：中国相声小品表演艺术家，国家一级演员，十大笑星之一
  - [石鲁](../Page/石鲁.md "wikilink")：[中国画家](../Page/中国.md "wikilink")
  - [石挥](../Page/石挥.md "wikilink")：中国导演
  - [石美玉](../Page/石美玉.md "wikilink")：中国医师
  - [石云生](../Page/石云生.md "wikilink")：中国海軍将领
  - [石刚](../Page/石刚.md "wikilink")：中国国务院[李克强总理办公室主任](../Page/李克强.md "wikilink")
  - [石美鑫](../Page/石美鑫.md "wikilink")：中国医学家
  - [石智勇](../Page/石智勇_\(1980年\).md "wikilink")：中国举重运动员，[2004年雅典奥运会举重冠军](../Page/2004年雅典奥运会.md "wikilink")
  - [石堅](../Page/石堅.md "wikilink")：香港艺人
  - [石天](../Page/石天.md "wikilink")：香港艺人
  - [石永泰](../Page/石永泰.md "wikilink")：香港資深律師
  - [石景宜](../Page/石景宜.md "wikilink")：香港出版家、香港汉荣书局创办人
  - [石偉雄](../Page/石偉雄.md "wikilink")：香港男子[競技體操運動員](../Page/競技體操.md "wikilink")，曾在兩屆亞運男子跳馬奪金
  - [石詠莉](../Page/石詠莉.md "wikilink")：香港三人組合Freeze成員
  - [石信之](../Page/石信之.md "wikilink")：香港指揮家、作曲家
  - [石天欣](../Page/石天欣.md "wikilink")：香港艺人、兼职模特儿
  - [石瑛](../Page/石瑛.md "wikilink")：中华民国官员
  - [石志偉](../Page/石志偉.md "wikilink")：台灣職業棒球選手
  - [石英](../Page/石英.md "wikilink")：台灣資深演員
  - [石安妮](../Page/石安妮.md "wikilink")：台灣演員
  - [石錦航](../Page/石錦航.md "wikilink")：藝名石頭，台灣樂團[五月天吉他手](../Page/五月天.md "wikilink")
  - [石怡潔](../Page/石怡潔.md "wikilink")：台湾新聞主播
  - [石計生](../Page/石計生.md "wikilink")：台湾東吳大學教授。
  - [石贺净](../Page/石贺净.md "wikilink")：韩国乒乓球运动员，本名石磊

### 虛構人物

  - [石秀](../Page/石秀.md "wikilink")：《[水浒传](../Page/水浒传.md "wikilink")》[108将之一](../Page/108将.md "wikilink")，绰号[拼命三郎](../Page/拼命三郎.md "wikilink")
  - [石勇](../Page/石勇.md "wikilink")：《[水浒传](../Page/水浒传.md "wikilink")》[108将之一](../Page/108将.md "wikilink")，绰号[石将军](../Page/石将军.md "wikilink")
  - [石黑龍](../Page/新著龍虎門角色列表#主要正派人物.md "wikilink")
    ：[香港漫畫](../Page/香港.md "wikilink")《[龍虎門](../Page/龍虎門.md "wikilink")》、《[新著龍虎門](../Page/新著龍虎門.md "wikilink")》主角
  - 石昊：大陆网络作家辰东所著《完美世界》主角

[石姓](../Category/石姓.md "wikilink") [S石](../Category/漢字姓氏.md "wikilink")