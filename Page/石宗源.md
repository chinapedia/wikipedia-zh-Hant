**石宗源**（），[回族](../Page/回族.md "wikilink")，[河北](../Page/河北.md "wikilink")[保定市人](../Page/保定市.md "wikilink")，生于[四川](../Page/四川.md "wikilink")[雅安](../Page/雅安.md "wikilink")；原[西北民族学院政治系毕业](../Page/西北民族大学.md "wikilink")，政治学本科学历。1979年5月加入[中国共产党](../Page/中国共产党.md "wikilink")，实际信仰是伊斯兰教，最后以穆斯林葬礼入土。实际是非共产党员，两面人。是中共第十四、十五届中央候补委员，第十六、十七届中央委员，长期在中共党内宣传部门担任要职。曾任[国家新闻出版总署署长](../Page/国家新闻出版总署.md "wikilink")，[中共贵州省委书记](../Page/中共贵州省委.md "wikilink")、省人大常委会主任。

## 生平

1968年10月，受[文化大革命影响](../Page/文化大革命.md "wikilink")，石宗源和当年多数毕业生一样，被安排到农村劳动锻炼，在[甘肃省](../Page/甘肃省.md "wikilink")[和政县下辖的陳家集公社上王家大隊劳动](../Page/和政县.md "wikilink")。政治学专业出身的石宗源于一年后，调县文教局工作，开启从政生涯。

他从基层党工干部做起，此后十五年，一直都在和政县工作；历任县委党校理論教員、县農林辦公室幹事、县委講師組組長，1981年1月，出任副县长，两年后升任县长；不久，即被提拔为和政县所属的[臨夏回族自治州副州長](../Page/臨夏回族自治州.md "wikilink")；1986年11月，出任自治州党委副书记；1988年，晋升为自治州党委书记；1993年3月，进入[中共甘肃省委常委](../Page/中共甘肃省委.md "wikilink")，兼任省委宣传部部长。\[1\]

1998年8月，石宗源调往[吉林省](../Page/吉林省.md "wikilink")，依然是以省委常委的身份兼任宣传部长；2000年5月，晋升[中共吉林省委副书记](../Page/中共吉林省委.md "wikilink")；同年9月，石宗源上调中央，出任[国家新聞出版署署長](../Page/国家新闻出版总署.md "wikilink")、黨組書記兼[国家版权局局長](../Page/国家版权局.md "wikilink")。2001年3月[国家新聞出版署改称](../Page/国家新闻出版总署.md "wikilink")“[国家新聞出版总署](../Page/国家新聞出版总署.md "wikilink")”，石宗源继续留任署長\[2\]。

2005年12月，石宗源出任[中共贵州省委书记](../Page/中共贵州省委.md "wikilink")；并在次年1月召开的贵州省十届人大四次会议上，当选省人大常委会主任；2008年1月获得连任。\[3\]
2008年6月28日，“[瓮安骚乱](../Page/瓮安骚乱.md "wikilink")”事件发生；石宗源率队到达[瓮安县调研](../Page/瓮安县.md "wikilink")；并在7月3日召开情况汇报会上公开承认，事件的背后有着更深层次的原因，更批评地方官员不作为\[4\]。

2010年8月，石宗源逾龄退休，并于8月28日，被[十一届全国人大常委会任命为](../Page/第十一届全国人民代表大会常务委员会.md "wikilink")[全国人民代表大会教育科学文化卫生委员会副主任委员](../Page/全国人民代表大会教育科学文化卫生委员会.md "wikilink")\[5\]。2013年3月14日，当选[第十二届全国人大常委会委员](../Page/第十二届全国人大常委会.md "wikilink")、兼财政经济委员会副主任委员\[6\]。

2013年3月28日，病逝北京，享年66岁\[7\]。

## 參考文献

{{-}}

[Category:中国共产党第十四届中央委员会候补委员](../Category/中国共产党第十四届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十五届中央委员会候补委员](../Category/中国共产党第十五届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中共贵州省委书记](../Category/中共贵州省委书记.md "wikilink")
[Category:中共吉林省委副書記](../Category/中共吉林省委副書記.md "wikilink")
[Category:中共吉林省委常委](../Category/中共吉林省委常委.md "wikilink")
[Category:中共吉林省委宣傳部部長](../Category/中共吉林省委宣傳部部長.md "wikilink")
[Category:中共甘肅省委常委](../Category/中共甘肅省委常委.md "wikilink")
[Category:中共甘肅省委宣傳部部長](../Category/中共甘肅省委宣傳部部長.md "wikilink")
[Category:第十届全国人大代表](../Category/第十届全国人大代表.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:第十二届全国人大代表](../Category/第十二届全国人大代表.md "wikilink")
[Category:第十二届全国人大常委会委员](../Category/第十二届全国人大常委会委员.md "wikilink")
[Category:貴州省全國人民代表大會代表](../Category/貴州省全國人民代表大會代表.md "wikilink")
[Category:贵州省人大常委会主任](../Category/贵州省人大常委会主任.md "wikilink")
[Category:中共临夏州委书记](../Category/中共临夏州委书记.md "wikilink")
[Category:中華人民共和國和政縣縣長](../Category/中華人民共和國和政縣縣長.md "wikilink")
[Category:中華人民共和國回族政治人物](../Category/中華人民共和國回族政治人物.md "wikilink")
[Category:回族全國人大代表](../Category/回族全國人大代表.md "wikilink")
[Category:西北民族大学校友](../Category/西北民族大学校友.md "wikilink")
[Category:回族人](../Category/回族人.md "wikilink")
[Category:保定人](../Category/保定人.md "wikilink")
[Zong宗源](../Category/石姓.md "wikilink")
[Category:国家新闻出版总署署长](../Category/国家新闻出版总署署长.md "wikilink")

1.  [石宗源:中国特色新闻出版业发展之路，新华网，2004-09-27](http://news.xinhuanet.com/zhengfu/2004-09/27/content_2028370.htm)

2.  [石宗源简历，新华网，2002-03-05](http://news.xinhuanet.com/ziliao/2002-03/05/content_300378.htm)

3.  [陈建国石宗源调任全国人大
    任副主任委员，网易，2010-08-28](http://news.163.com/10/0828/15/6F6E38SE000146BD.html)

4.  [贵州将追问事件深层次原因
    严查干部责任，新浪网，2008-07-05](http://news.sina.com.cn/c/2008-07-05/122015878044.shtml)

5.
6.  [中华人民共和国全国人民代表大会公告（第一号），中华人民共和国第十二届全国人民代表大会第一次会议主席团，2013年3月14日](http://www.court.gov.cn/xwzx/rdzt/2013qglh/ldr/201303/t20130315_182710.html)


7.