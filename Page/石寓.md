[Stone_House,_No._15_Kotewall_Road.JPG](https://zh.wikipedia.org/wiki/File:Stone_House,_No._15_Kotewall_Road.JPG "fig:Stone_House,_No._15_Kotewall_Road.JPG")
**石寓**（**Stone
House**，又稱**旭龢道15號**）是[香港現存以](../Page/香港.md "wikilink")[鋼筋及](../Page/鋼筋.md "wikilink")[混凝土建造的最早期建築之一](../Page/混凝土.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[半山區](../Page/半山區.md "wikilink")[旭龢道](../Page/旭龢道.md "wikilink")15號，現時已被列作[香港三級歷史建築](../Page/香港三級歷史建築.md "wikilink")。

石寓於1923年興建，當時的用途是作為修車房之用。當時旭龢道一帶設有一些修車房為半山區居民服務。到了現在，石寓已成為現存僅有的一幢修車房。

[香港重光後](../Page/香港重光.md "wikilink")，該處停止作修車房用途。於1949年到1951年間，曾經作為[香港外國記者會的會所](../Page/香港外國記者會.md "wikilink")。後來業權多番易手，直至1980年起，開始作為私人住宅用途。最近一次業權易手是2005年底進行的[拍賣](../Page/拍賣.md "wikilink")。近年該處曾經作為[私房菜場地](../Page/私房菜.md "wikilink")\[1\]\[2\]。

石寓樓高2層，是香港早期使用[鋼筋及](../Page/鋼筋.md "wikilink")[混凝土建造的建築物之一](../Page/混凝土.md "wikilink")，其特點是外牆仍以[磚頭和](../Page/磚頭.md "wikilink")[花崗岩堆砌而成](../Page/花崗岩.md "wikilink")。

## 參考資料

<references/>

## 外部連結

  - [石寓-古物古蹟辦事處](http://www.amo.gov.hk/b5/trails_west2.php?tid=7)

[Category:香港三級歷史建築](../Category/香港三級歷史建築.md "wikilink")
[Category:半山區](../Category/半山區.md "wikilink")

1.  [香江食評：石寓](http://www.drinkeat.com/restaurantdetails.php?rid=308)
2.  [HK Tatler：Stone House
    Relocates](http://hk.dining.asiatatler.com/news/stone-house-relocates)