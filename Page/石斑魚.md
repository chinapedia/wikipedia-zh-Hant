**石斑魚**（）泛指[鱸形目](../Page/鱸形目.md "wikilink")[鮨科石斑魚亞科](../Page/鮨科.md "wikilink")（[學名](../Page/學名.md "wikilink")：）裡的各[屬](../Page/屬_\(生物\).md "wikilink")[魚類](../Page/魚.md "wikilink")。

並非所有鮨科魚都被稱為「石斑魚」；該科魚類還包括[鱸魚](../Page/鱸魚.md "wikilink")。石斑魚通常指兩大[屬魚類](../Page/屬_\(生物\).md "wikilink")：石斑魚屬及喙鱸屬。除此以外，*Anyperidon*屬、駝背鱸屬、鱗鮨屬、纖齒鱸屬、貧鱠屬及鱠屬的魚類也會被稱為石斑魚，而鰓棘鱸屬的魚類則被稱為[珊瑚斑魚](../Page/珊瑚斑魚.md "wikilink")。不過，某些鴛鴛鮨屬、九棘鱸屬、側牙鱸屬、角紋鮨屬、東洋鱸屬、副花鮨屬等屬於石斑魚亞科魚，及個別的其他鮨科魚類，有時也會被冠上石斑魚的稱號。

石斑魚的英語名稱 grouper 來自於[葡萄牙語](../Page/葡萄牙語.md "wikilink") garoupa 一詞，跟英語裡的
group （團體）無關。

石斑魚屬於[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")，身體肥厚，口部大，並不適宜長途迅速游泳。石斑魚體型相當大，身長可達一公尺以上，體重超過一百公斤也不足為奇；不過石斑魚的種類頗多，體型大小也各有差別。牠們會把獵物吞噬，而不會用口把獵物逐片撕開；這是因為牠們的顎沒有很多牙齒，可是在[咽頭裡的牙板可以碾碎食物](../Page/咽頭.md "wikilink")。牠們習慣等待[魚](../Page/魚.md "wikilink")、[章魚](../Page/章魚.md "wikilink")、[螃蟹](../Page/螃蟹.md "wikilink")、[龍蝦等獵物靠近](../Page/龍蝦.md "wikilink")，而不會在廣闊的水域追逐獵物。

由於很多種類的石斑魚都是重要的[食用魚](../Page/食用鱼.md "wikilink")，因此現在魚商[養殖了不少的石斑魚](../Page/魚類養殖.md "wikilink")；而在[深海釣魚活動中](../Page/釣魚.md "wikilink")，石斑魚是很受歡迎的魚類。一些體型較小的石斑魚種會被養在[水族館裡](../Page/水族館.md "wikilink")，但牠們的生長速度其實也非常快。

一種名為[鞍帶石斑魚的魚可以長得非常巨大](../Page/鞍帶石斑魚.md "wikilink")，曾有報導說有鞍帶石斑魚長得足以把泳客或[水肺潛水員吞噬](../Page/水肺潛水.md "wikilink")：當英國作家[亞瑟·C·克拉克於](../Page/亞瑟·C·克拉克.md "wikilink")[斯里蘭卡海岸潛水時](../Page/斯里蘭卡.md "wikilink")，發現了一條二十英尺(約6公尺)長、四英尺(約1.2公尺)厚的石斑魚，生活在一個下沉了的[浮檯裡](../Page/浮檯.md "wikilink")。1970年代一本水肺潛水[雜誌記載了一宗事故](../Page/雜誌.md "wikilink")，一個潛水員在[加州潛水的時候被一條巨型石斑魚完全吞噬](../Page/加州.md "wikilink")，該魚試圖用咽頭牙板將該潛水員壓碎，幸好只能壓凹其[氧氣筒](../Page/氧氣筒.md "wikilink")，隨後把該潛水員吐出。要吞噬一個使用開放式呼吸系統的水肺潛水員，石斑魚的喉部至少要張開到兩平方英尺(約0.2平方公尺)；[鞍帶石斑魚通常不會長到這麼大](../Page/鞍帶石斑魚.md "wikilink")，因為如果要躲避[鯊魚的攻擊](../Page/鯊魚.md "wikilink")，牠必須要有足夠大的庇護所；不過隨著遠洋漁業[延繩釣使鯊魚數量減少](../Page/延繩釣.md "wikilink")，這種情況可能會有改變。（最近有報導稱獵殺鯊魚使石斑魚的數量上升，因此鸚哥魚的數量也隨之下降，導致[珊瑚礁裡的](../Page/珊瑚礁.md "wikilink")[藻類生長過速](../Page/藻.md "wikilink")。）

## 生態重要性

石斑魚數量銳減，會影響生態系統。石斑魚喜歡鑽挖石罅，清除積存在罅中的沙石，令這些隙縫成為其他生物的良好棲所。假如石斑魚數量不足，會令這些「住所」的供應量下跌。另一方面，作為雜食性動物，石斑魚會捕捉其他較少的魚類，[甲殼類如螃蟹和龍蝦](../Page/甲殼類.md "wikilink")，還有章魚這種[軟體動物](../Page/軟體動物.md "wikilink")，都是石斑魚的主要食物。石斑魚平衡了這些動物的數量\[1\]。

## 濫捕

[Epinephelus_malabaricus.jpg](https://zh.wikipedia.org/wiki/File:Epinephelus_malabaricus.jpg "fig:Epinephelus_malabaricus.jpg")
[Steamed_Grouper.jpg](https://zh.wikipedia.org/wiki/File:Steamed_Grouper.jpg "fig:Steamed_Grouper.jpg")
[香港大學的研究報告顯示](../Page/香港大學.md "wikilink")，各種石斑魚類均遭到持續的濫捕，多種石斑魚類面對絕種危機\[2\]。在[國際自然保護聯盟的](../Page/國際自然保護聯盟.md "wikilink")「瀕危物種紅色名錄」上，163種石斑魚類，有20種面臨滅絕，另有5種屬[瀕危水平](../Page/瀕危.md "wikilink")\[3\]。

單是2009年，全球就有超過27.5萬噸的石斑魚被人吃掉\[4\]。以平均每條3[公斤推算](../Page/公斤.md "wikilink")，數量相當於9000萬條。香港大學更指出，實際數量可能更多\[5\]。因為[香港和](../Page/香港.md "wikilink")[印尼的數據顯示](../Page/印尼.md "wikilink")，大部分被出售的石斑魚，重量只有1[公斤](../Page/公斤.md "wikilink")\[6\]。

根據報告，過度的[捕撈](../Page/捕食.md "wikilink")、缺乏監管和欠佳的保育工作，是石斑魚瀕臨滅絕的主要原因\[7\]。據悉，每年全球的石斑魚銷售總金額，高達10億美元\[8\]。漁民不分大小的捕撈，一方面杜絕了成年的雄魚，另一方面令小魚無法生存至成年，扼殺石斑魚的繁殖機會\[9\]。

所有石斑魚，包括常見的紅斑、星斑、鼠斑和[龍躉](../Page/龍躉.md "wikilink")，牠們出生的時候都是雌性，成年後才會轉為雄性\[10\]。然而，石斑魚要10年才到成年，許多幼魚未及成長即被人捕獲，令成功繁殖的機會銳減，魚的數量大幅下跌\[11\]。

由於數量下跌，需求卻不斷上升，漁民不惜使用稀釋的山埃捕魚，雖然能捕捉一定數量的大小石斑，卻把魚苗（俗稱魚毛）毒殺\[12\]。一些品種被趕盡殺絕，再沒有魚苗聚集\[13\]。另外，即使是人工養殖業，也會補撈魚苗\[14\]。

在亞洲，包括但不僅限於[中國](../Page/中國.md "wikilink")，由於人口眾多，客人又願意付款，即使價格上升，他們仍會點選石斑菜式\[15\]。另外，[亞洲區](../Page/亞洲區.md "wikilink")[餐館把活魚養在魚缸的招客手法](../Page/餐館.md "wikilink")，成功的吸引大量客人\[16\]。由於運費及關稅等問題，進一步推高石斑魚在中國的售價，許多中國遊客專程到香港的餐館用膳\[17\]。全球的石斑漁獲，有8成在亞洲水域捕獲\[18\]。

研究海洋和淡水生物的學者薛綺雯（Yvonne
Sadovy）指出，香港與中國大陸為鄰，是環球石斑貿易的一個主要中心，在管理海產進口和銷售方面的條例已經過時，因此未能遵守國際公約和協定\[19\]。從事漁業的香港人士指出，香港紅斑（Hong
Kong
Grouper）原本常見於[福建沿岸](../Page/福建.md "wikilink")，但因包括香港人在内的广东人杂食，市場需求大，在近20年過度捕撈，接近枯竭\[20\]。

有香港[食家得知石班魚面臨絕種](../Page/食家.md "wikilink")，表示會考慮不再吃，又稱還有許多海鮮可以代替石斑，而且同樣好吃\[21\]。

## 瀕臨絕種的石斑魚

| 中文名 | 學名 | 保護級別  極危 極危 極危 極危 瀕危 瀕危 瀕危 瀕危 易危 易危 易危 易危 易危 易危 易危 易危 易危 易危 易危 |
| --- | -- | -------------------------------------------------------------- |

## 其他石斑品種

### 常見的其他石斑魚

| 中文名 | 學名 | 保護級別  近危 近危 近危 近危 近危 無危 無危 無危 無危 無危 無危 無危 無危 無危 無危 |
| --- | -- | -------------------------------------------------- |

  - [龍躉](../Page/龍躉.md "wikilink")

## 参考文献

  -
[石斑魚亞科](../Category/石斑魚亞科.md "wikilink")
[Category:鮨科](../Category/鮨科.md "wikilink")

1.
2.

3.
4.
5.
6.
7.
8.

9.

10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.