[石林11.jpg](https://zh.wikipedia.org/wiki/File:石林11.jpg "fig:石林11.jpg")，[孙悟空被压](../Page/孙悟空.md "wikilink")[五行山下场景的取景处](../Page/五行山.md "wikilink")。\]\]
[Yunnanshilin2.jpg](https://zh.wikipedia.org/wiki/File:Yunnanshilin2.jpg "fig:Yunnanshilin2.jpg")
**石林风景名胜区**，常称**路南石林**或**云南石林**，位于[中國](../Page/中國.md "wikilink")[云南省](../Page/云南省.md "wikilink")[石林彝族自治县](../Page/石林彝族自治县.md "wikilink")，1982年11月8日经[中华人民共和国国务院批准为首批国家重点风景名胜区](../Page/中华人民共和国国务院.md "wikilink")，其时法定名称为“路南石林风景名胜区”。

## 历史

1991年，《路南石林风景名胜区保护条例》开始实施\[1\]。2001年3月6日，[中华人民共和国国土资源部批准石林建立](../Page/中华人民共和国国土资源部.md "wikilink")“云南石林岩溶峰林国家地质公园”；2004年2月13日[联合国教科文组织批准石林列入首批](../Page/联合国教科文组织.md "wikilink")[世界地质公园](../Page/世界地质公园.md "wikilink")；2007年6月27日，石林作为“[中国南方喀斯特](../Page/中国南方喀斯特.md "wikilink")”的重要组成部分成功申报世界自然遗产。

## 介绍

该风景区距[昆明](../Page/昆明.md "wikilink")120千米，面积约40多万亩。石林是典型的[喀斯特地貌](../Page/喀斯特地貌.md "wikilink")，经过亿万年地质变化形成的。石林还是[彝族传说中](../Page/彝族.md "wikilink")[阿诗玛的故乡](../Page/阿诗玛.md "wikilink")。石林风景区主要由大、小石林（李子菁石林），乃古石林，芝云洞，长湖，大叠水等景区组成。每年农历六月二十四的“[火把节](../Page/火把节.md "wikilink")”是游览石林的最佳时间。1931年云南省主席[龙云路过此处命名](../Page/龙云.md "wikilink")“石林”，[周钟岳的行书题字于](../Page/周钟岳.md "wikilink")[文化大革命时被毁](../Page/文化大革命.md "wikilink")。改革开放后，当地旅游部门自曲靖第一中学内所存之《爨宝子碑》上集字而成如今的匾额\[2\]。落款“龙云题”为龙云之子[龙绳文于](../Page/龙绳文.md "wikilink")1985年9月题写，由石林县刊刻恢复\[3\]。1962年，[朱德到访石林](../Page/朱德.md "wikilink")，即兴题写“群峰壁立，千嶂叠翠”，因只有八张纸，无处落款，现在刊刻的“朱德题”三字是从他处题词中临摹镌刻上去的\[4\]。

## 参考资料

## 相关条目

  - [中国南方喀斯特](../Page/中国南方喀斯特.md "wikilink")
  - [梭布垭石林](../Page/梭布垭石林.md "wikilink")
  - 马达加斯加[贝马拉哈国家公园石林](../Page/贝马拉哈国家公园.md "wikilink")

[Y](../Category/中國國家地質公園.md "wikilink")
[Category:中国南方喀斯特](../Category/中国南方喀斯特.md "wikilink")
[Category:昆明旅游](../Category/昆明旅游.md "wikilink")
[Category:石林地理](../Category/石林地理.md "wikilink")
[Category:昆明地理](../Category/昆明地理.md "wikilink")

1.
2.
3.
4.