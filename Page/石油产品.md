[Petro-canada_oil_bottle_products_7355.jpg](https://zh.wikipedia.org/wiki/File:Petro-canada_oil_bottle_products_7355.jpg "fig:Petro-canada_oil_bottle_products_7355.jpg")
[Grangemouth04nov06.jpg](https://zh.wikipedia.org/wiki/File:Grangemouth04nov06.jpg "fig:Grangemouth04nov06.jpg")格蘭傑默斯（Grangemouth）的一間石化[煉油廠](../Page/煉油廠.md "wikilink")\]\]
**石油產品**是從[原油經](../Page/原油.md "wikilink")[煉油廠提煉出來的一系列](../Page/煉油廠.md "wikilink")[產品](../Page/產品.md "wikilink")
[Usesofpetroleum.png](https://zh.wikipedia.org/wiki/File:Usesofpetroleum.png "fig:Usesofpetroleum.png")

依原油成份及市場[需求的不同](../Page/需求.md "wikilink")，煉油廠可以提煉出各種不同的產品。佔產量比重最高的是各種等級的[燃油](../Page/燃油.md "wikilink")。此外還有其它的化學物質，可以再經[化工過程製造出塑膠和其它物品](../Page/化工過程.md "wikilink")。因原油中含有[硫](../Page/硫.md "wikilink")，煉油廠也可從中提煉出大量的硫。[石油焦](../Page/石油焦.md "wikilink")（成份為[氫和](../Page/氫.md "wikilink")[碳](../Page/碳.md "wikilink")）也是一種石油產品。原油提煉出來的氫經常用於其它煉油過程，如[氫催化裂解](../Page/氫催化裂解.md "wikilink")（[加氫裂化](../Page/加氫裂化.md "wikilink")）和[加氫脫硫](../Page/加氫脫硫.md "wikilink")。

## 主要石油產品

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/石油化學物質.md" title="wikilink">石油化學物質</a> （ <a href="../Page/塑料.md" title="wikilink">塑料</a> ）</li>
<li><a href="../Page/瀝青.md" title="wikilink">瀝青</a></li>
<li><a href="../Page/柴油.md" title="wikilink">柴油</a></li>
<li><a href="../Page/燃油.md" title="wikilink">燃油</a></li>
<li><a href="../Page/汽油.md" title="wikilink">汽油</a></li>
</ul></td>
<td></td>
<td><ul>
<li><a href="../Page/煤氣.md" title="wikilink">煤氣</a></li>
<li><a href="../Page/液化石油氣.md" title="wikilink">液化石油氣</a></li>
<li><a href="../Page/潤滑劑.md" title="wikilink">潤滑油</a></li>
<li><a href="../Page/石蠟.md" title="wikilink">石蠟</a></li>
<li><a href="../Page/溚.md" title="wikilink">溚</a>（<a href="../Page/焦油.md" title="wikilink">焦油</a>）</li>
</ul></td>
</tr>
</tbody>
</table>

## 特種終端產品

煉油廠常將多種原料、添加物混合，以利短期儲存，或方便以[陸運](../Page/陸運.md "wikilink")、[海運大量運輸](../Page/海運.md "wikilink")。

  - 將[丙烷一類的](../Page/丙烷.md "wikilink")[氣態燃料以](../Page/氣態.md "wikilink")[液態儲存](../Page/液態.md "wikilink")，並以油罐車輸送。
  - 製造[混合物液態燃料](../Page/混合物.md "wikilink")（配製汽車用[汽油](../Page/汽油.md "wikilink")、航空汽油、[煤油](../Page/煤油.md "wikilink")、渦輪機燃油、[柴油](../Page/柴油.md "wikilink")，添加[染料](../Page/染料.md "wikilink")、[清潔劑](../Page/清潔劑.md "wikilink")、[抗爆劑](../Page/抗爆劑.md "wikilink")、[氧化劑](../Page/氧化劑.md "wikilink")、抗真菌化合物等），用[鐵路](../Page/鐵路.md "wikilink")、[貨船](../Page/貨船.md "wikilink")、運油船運送，也可以[管線直接輸送給](../Page/管線.md "wikilink")[消費者](../Page/消費者.md "wikilink")，例如用管線將燃料送到[機場](../Page/機場.md "wikilink")，或將多種燃料以管線輸送給配送者，並以[检管器將不同產品分離](../Page/检管器.md "wikilink")。
  - [潤滑劑](../Page/潤滑劑.md "wikilink")（生產輕型[機油](../Page/機油.md "wikilink")、[電動機潤滑油](../Page/電動機.md "wikilink")、[潤滑油](../Page/潤滑油.md "wikilink")，視需要而加入[粘度穩定劑](../Page/粘度穩定劑.md "wikilink")），通常以散裝運到異地包裝廠。
  - [石蠟](../Page/石蠟.md "wikilink")，用於包裝[冷凍食品等用途](../Page/冷凍食品.md "wikilink")，用常以塊狀運送到[工廠](../Page/工廠.md "wikilink")，準備加工使用。
  - [硫](../Page/硫.md "wikilink")（或[硫酸](../Page/硫酸.md "wikilink")），[原油脫硫產生的](../Page/原油.md "wikilink")[副產品](../Page/副產品.md "wikilink")，可用於工業原料，通常以其先驅物質[發煙硫酸運送](../Page/發煙硫酸.md "wikilink")。
  - [焦油散裝運至異地工廠封裝](../Page/焦油.md "wikilink")，可與[礫石混合鋪設](../Page/礫石.md "wikilink")[屋頂](../Page/屋頂.md "wikilink")。
  - [瀝青](../Page/瀝青.md "wikilink")──用來作為粘結礫石，形成瀝青[混凝土](../Page/混凝土.md "wikilink")，用於鋪路、地面等，通常以散裝運輸。
  - [石油焦](../Page/石油焦.md "wikilink")，在用特種碳素製品，如某些類型的[電極](../Page/電極.md "wikilink")，或作為[固體燃料](../Page/固體燃料.md "wikilink")。
  - 石化物質或石化原料送至[石化工廠進行進一步處理](../Page/石化工廠.md "wikilink")。這些石化物質可能是[烯](../Page/烯.md "wikilink")、烯的先驅物質或[芳香性化合物](../Page/芳香性化合物.md "wikilink")。

<!-- end list -->

  -
    石化產品有廣闊的多種用途。他們普遍用作單體或生產單體的原料。烯烴，如α─烯烴和二烯烴常被用作單體，雖然芳烴也可以被用來作為單體的前體。然後單體以各種方式聚合，形成聚合物。高分子材料（聚合物），可用於塑料、橡膠、纖維，或這些物質的中間形式。一些聚合物也被用來作為凝膠體或潤滑劑。石化產品還可以用作溶劑或生產溶劑的原料。石化產品也可以被用來作為各種化學物，如車輛的液體、表面活性劑，的先驅物質。

## 參見

  - [羊毛脂](../Page/羊毛脂.md "wikilink")：羊毛脂製品可作為有毒的石化[噴霧劑和](../Page/噴霧劑.md "wikilink")[脫脂劑的天然](../Page/脫脂劑.md "wikilink")[替代品](../Page/替代品.md "wikilink")
  - [石油副產品](../Page/石油副產品.md "wikilink")

[Category:燃料](../Category/燃料.md "wikilink")
[Category:石油工業](../Category/石油工業.md "wikilink")
[Category:石油产品](../Category/石油产品.md "wikilink")