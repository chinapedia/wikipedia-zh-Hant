[Shek_O_Quarry_2.jpg](https://zh.wikipedia.org/wiki/File:Shek_O_Quarry_2.jpg "fig:Shek_O_Quarry_2.jpg")南脊遠眺石澳石礦場\]\]
[Shek_O_Quarry_1.jpg](https://zh.wikipedia.org/wiki/File:Shek_O_Quarry_1.jpg "fig:Shek_O_Quarry_1.jpg")俯望石澳石礦場\]\]
**石澳石礦場**是[香港一個已停用的](../Page/香港.md "wikilink")[石礦場](../Page/石礦場.md "wikilink")，也是[香港島最後一個石礦場](../Page/香港島.md "wikilink")。石礦場位於[南區](../Page/南區_\(香港\).md "wikilink")[石澳附近的](../Page/石澳.md "wikilink")[鶴咀半島西岸一帶](../Page/鶴咀半島.md "wikilink")，佔地45[公頃](../Page/公頃.md "wikilink")。

## 歷史

石澳石礦場於1994年開採，原定至2005年底為止。後來於2001年8月政府批准將石礦場運作至2009年底；同時進行環境復修工程。該石礦場可開採合共2300萬[公噸的](../Page/公噸.md "wikilink")[岩石](../Page/岩石.md "wikilink")。

在1994至96年間，石礦場內灣部分曾用作[西區海底隧道沉管鑄造工場](../Page/西區海底隧道.md "wikilink")。

2009年石礦場曾經傳出規劃興建[水上活動中心](../Page/水上活動中心.md "wikilink")，惜最後未有落實早於1989年於[都會計劃提出的改造成水上活動中心概念](../Page/都會計劃.md "wikilink")。

2010年[沙中綫刊](../Page/沙中綫.md "wikilink")[憲方案指出原石礦場會用作](../Page/香港政府憲報.md "wikilink")[香港第四條過海鐵路](../Page/香港第四條過海鐵路.md "wikilink")[沉管工場](../Page/沉管式隧道.md "wikilink")（直至2017年6月，沉管已經完成澆築，並於2018年4月全面拖出已注水的礦場）；此外設計中的[東南九龍T2主幹道海底隧道段亦可能選址石澳石礦場共用沉管工場](../Page/東南九龍T2主幹道.md "wikilink")。

## 參考資料

  -
  -
  -
  - [香港的石礦場](http://www.cedd.gov.hk/tc/publications/information_notes/doc/in_2007_09c.pdf)

  -
[Category:香港礦場](../Category/香港礦場.md "wikilink")
[Category:鶴咀半島](../Category/鶴咀半島.md "wikilink")