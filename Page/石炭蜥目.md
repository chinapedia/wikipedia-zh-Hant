**石炭蜥目**（学名：*Anthracosauria*），是指一類已[滅絕](../Page/滅絕.md "wikilink")，像[爬行動物及](../Page/爬行動物.md "wikilink")[兩棲動物的](../Page/兩棲動物.md "wikilink")[四足超綱](../Page/四足超綱.md "wikilink")，生存於[石炭紀至](../Page/石炭紀.md "wikilink")[二疊紀初期](../Page/二疊紀.md "wikilink")。不過其確切的分野則視乎不同的定義而決定。

石炭蜥目最初於1934年由Gunnar Säve-Söderbergh所定，是一類生存於石炭紀至下二疊紀的大型水中兩棲動物。但根據Alfred
Sherwood
Romer的定義，石炭蜥目則包括所有非[羊膜動物](../Page/羊膜動物.md "wikilink")、像爬行動物及兩棲類的[迷齒螈](../Page/迷齒螈.md "wikilink")，原有的定義則較像他的[開](../Page/始椎類.md "wikilink")

[始椎類](../Page/始椎類.md "wikilink")。[內德·科爾伯特](../Page/內德·科爾伯特.md "wikilink")（Edwin
H. Colbert）則緣用這個定義\[1\]\[2\]，但後來於1970年則有學者重新使用Säve-Söderbergh的定義。\[3\]

隨著[支序分類學的出現](../Page/支序分類學.md "wikilink")，這個分類亦有所變更。最初石炭蜥目只是指最像爬行動物的四足超綱（[鈍頭龍亞目及Solenodonsauridae](../Page/鈍頭龍亞目.md "wikilink")）及羊膜動物。\[4\]但後來石炭蜥目，與[蜥螈形類及鈍頭龍亞目](../Page/蜥螈形類.md "wikilink")，成為了[爬行形類下的](../Page/爬行形類.md "wikilink")[側系群](../Page/側系群.md "wikilink")。\[5\]

## 名字起源

石炭蜥目的名字源自[希臘文](../Page/希臘文.md "wikilink")，意思是「煤蜥蜴」，因為牠們大部分的[化石都是在](../Page/化石.md "wikilink")[煤系地層發現的](../Page/煤系地層.md "wikilink")。

## 參考

  - Clack, J. A. (2002), *Gaining Ground: the Origin and Evolution of
    Tetrapods* Indiana Univ. Press, 369 pp.

## 外部連結

  - Palaeos
    [Anthracosauroidea](https://web.archive.org/web/20080229071820/http://www.palaeos.com/Vertebrates/Units/190Reptilomorpha/190.100.html#Anthracosauroidea)
  - Systema Naturae 2000 / Classification [Order
    Anthracosauria](http://sn2000.taxonomy.nl/Main/Classification/47168.htm)

[Category:四足總綱](../Category/四足總綱.md "wikilink")
[Category:石炭蜥目](../Category/石炭蜥目.md "wikilink")

1.
2.
3.
4.
5.