**石碇區**是[台灣](../Page/台灣.md "wikilink")[新北市下轄的一個](../Page/新北市.md "wikilink")[市轄區級行政區](../Page/市轄區.md "wikilink")，位在[台北盆地東南緣](../Page/台北盆地.md "wikilink")，境內多山，開發程度低且人口稀少。早期以煤礦為主要經濟來源，後來煤礦枯竭後人口開始大量外移。

石碇區的西側是[新店區](../Page/新店區.md "wikilink")、[深坑區及](../Page/深坑區.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[南港區](../Page/南港區.md "wikilink")、[文山區](../Page/文山區.md "wikilink")，東鄰[坪林區](../Page/坪林區.md "wikilink")，北端[平溪區](../Page/平溪區.md "wikilink")，南端[烏來區](../Page/烏來區.md "wikilink")，境內的[翡翠水庫是提供](../Page/翡翠水庫.md "wikilink")[大臺北居民用水的主要來源](../Page/大臺北.md "wikilink")，雖然該水庫的[水壩本體位於](../Page/水壩.md "wikilink")[新店區境內](../Page/新店區.md "wikilink")，但10.24平方公里的蓄水面積裡面，約有92%位於本區境內，這也使得石碇成為[大臺北地區最重要的水源保護地區](../Page/大臺北地區.md "wikilink")。

## 地名由來

「石碇」地名由來主要有兩種說法，第一說乃因石碇地區溪中甚多巨石，鄉民過溪，常須跨越橫豎在溪中的大石頭，像是踩上石質[門檻](../Page/門檻.md "wikilink")（臺語「戶碇」）一樣，故稱。另說則是小船停泊時必須繫在巨石之上，以固定船隻，「以石碇泊」，故稱
「石碇」。

古有[石碇堡](../Page/石碇堡.md "wikilink")，但今石碇區屬於[文山堡](../Page/文山堡.md "wikilink")。

## 行政區劃

<table>
<thead>
<tr class="header">
<th><p>里名</p></th>
<th><p>鄰數</p></th>
<th><p>鄰數排行</p></th>
<th><p>戶數</p></th>
<th><p>戶數排行</p></th>
<th><p>面積<br />
km<sup>2</sup></p></th>
<th><p>面積排行</p></th>
<th><p>人口<br />
2013年2月</p></th>
<th><p>人口排行</p></th>
<th><p>人口密度<br />
人/km<sup>2</sup></p></th>
<th><p>人口密度排行</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>石碇里</p></td>
<td><p>9</p></td>
<td><p>7</p></td>
<td><p>188</p></td>
<td><p>9</p></td>
<td><p>1.2482</p></td>
<td><p>11</p></td>
<td><p>489</p></td>
<td><p>8</p></td>
<td><p>391.8</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>潭邊里</p></td>
<td><p>10</p></td>
<td><p>4</p></td>
<td><p>402</p></td>
<td><p>2</p></td>
<td><p>7.4478</p></td>
<td><p>5</p></td>
<td><p>886</p></td>
<td><p>3</p></td>
<td><p>119</p></td>
<td><p>6</p></td>
</tr>
<tr class="odd">
<td><p>烏塗里</p></td>
<td><p>10</p></td>
<td><p>4</p></td>
<td><p>376</p></td>
<td><p>3</p></td>
<td><p>10.9603</p></td>
<td><p>4</p></td>
<td><p>776</p></td>
<td><p>6</p></td>
<td><p>70.8</p></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td><p>豐田-{里}-</p></td>
<td><p>7</p></td>
<td><p>7</p></td>
<td><p>230</p></td>
<td><p>8</p></td>
<td><p>13.0263</p></td>
<td><p>3</p></td>
<td><p>450</p></td>
<td><p>9</p></td>
<td><p>34.6</p></td>
<td><p>12</p></td>
</tr>
<tr class="odd">
<td><p>彭山-{里}-</p></td>
<td><p>6</p></td>
<td><p>9</p></td>
<td><p>159</p></td>
<td><p>10</p></td>
<td><p>3.6741</p></td>
<td><p>10</p></td>
<td><p>372</p></td>
<td><p>10</p></td>
<td><p>101.3</p></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p>隆盛里</p></td>
<td><p>12</p></td>
<td><p>3</p></td>
<td><p>309</p></td>
<td><p>6</p></td>
<td><p>3.8701</p></td>
<td><p>9</p></td>
<td><p>852</p></td>
<td><p>4</p></td>
<td><p>220.2</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>豐林里</p></td>
<td><p>5</p></td>
<td><p>11</p></td>
<td><p>108</p></td>
<td><p>12</p></td>
<td><p>1.2312</p></td>
<td><p>12</p></td>
<td><p>300</p></td>
<td><p>11</p></td>
<td><p>243.7</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>永定里</p></td>
<td><p>10</p></td>
<td><p>4</p></td>
<td><p>274</p></td>
<td><p>7</p></td>
<td><p>5.3643</p></td>
<td><p>8</p></td>
<td><p>725</p></td>
<td><p>7</p></td>
<td><p>135.2</p></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p>中民里</p></td>
<td><p>14</p></td>
<td><p>1</p></td>
<td><p>340</p></td>
<td><p>4</p></td>
<td><p>5.4141</p></td>
<td><p>7</p></td>
<td><p>893</p></td>
<td><p>2</p></td>
<td><p>164.9</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p>光明-{里}-</p></td>
<td><p>5</p></td>
<td><p>11</p></td>
<td><p>142</p></td>
<td><p>11</p></td>
<td><p>7.4334</p></td>
<td><p>6</p></td>
<td><p>267</p></td>
<td><p>12</p></td>
<td><p>35.9</p></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td><p>格頭里</p></td>
<td><p>13</p></td>
<td><p>2</p></td>
<td><p>490</p></td>
<td><p>1</p></td>
<td><p>21.63</p></td>
<td><p>2</p></td>
<td><p>1037</p></td>
<td><p>1</p></td>
<td><p>47.9</p></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p>永安里</p></td>
<td><p>6</p></td>
<td><p>9</p></td>
<td><p>337</p></td>
<td><p>5</p></td>
<td><p>63.44</p></td>
<td><p>1</p></td>
<td><p>818</p></td>
<td><p>5</p></td>
<td><p>12.9</p></td>
<td><p>13</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 人口

## 警政治安

  - 內政部警政署[國道公路警察局第九警察隊石碇分隊](../Page/國道公路警察局.md "wikilink")
  - 新北市政府警察局新店分局
      - 石碇分駐所
      - 中民派出所
      - 楓子林派出所
      - 豐田派出所
      - 碧山派出所

## 交通

### [國道](../Page/國道.md "wikilink")

  - [TWHW5.svg](https://zh.wikipedia.org/wiki/File:TWHW5.svg "fig:TWHW5.svg")
    [國道五號](../Page/蔣渭水高速公路.md "wikilink")
      - [石碇交流道](../Page/石碇交流道.md "wikilink")

### [省道](../Page/省道.md "wikilink")

  - [TW_PHW9.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9.svg "fig:TW_PHW9.svg")
    [北宜路連接](../Page/台9線.md "wikilink")[新北市內](../Page/新北市.md "wikilink")
    [新店區](../Page/新店區.md "wikilink")←石碇區→[坪林區](../Page/坪林區.md "wikilink")

### [市道](../Page/市道.md "wikilink")

  - [TW_CHW106.svg](https://zh.wikipedia.org/wiki/File:TW_CHW106.svg "fig:TW_CHW106.svg")
    [靜安路一段連接](../Page/靜安路_\(新北市\).md "wikilink")[新北市內](../Page/新北市.md "wikilink")
    [深坑區](../Page/深坑區.md "wikilink")[北深路一段](../Page/北深路.md "wikilink")←石碇區→[平溪區](../Page/平溪區.md "wikilink")
  - [TW_CHW106b.svg](https://zh.wikipedia.org/wiki/File:TW_CHW106b.svg "fig:TW_CHW106b.svg")
    [文山路一段連接](../Page/文山路.md "wikilink")[新北市內](../Page/新北市.md "wikilink")
    深坑區[文山路一段](../Page/文山路.md "wikilink")
  - [TW_CHW106b.svg](https://zh.wikipedia.org/wiki/File:TW_CHW106b.svg "fig:TW_CHW106b.svg")
    [碇坪路一段連接](../Page/碇坪路.md "wikilink")[新北市內](../Page/新北市.md "wikilink")
    石碇區[靜安路一段](../Page/靜安路_\(新北市\).md "wikilink")→[坪林區國中路](../Page/坪林區.md "wikilink")

### [鄉道](../Page/鄉道.md "wikilink")

  - [TW_THWtp32.png](https://zh.wikipedia.org/wiki/File:TW_THWtp32.png "fig:TW_THWtp32.png")
    [北32線](../Page/北32線.md "wikilink")（新興坑路）
  - [TW_THWtp33.png](https://zh.wikipedia.org/wiki/File:TW_THWtp33.png "fig:TW_THWtp33.png")
    [北33線](../Page/北33線.md "wikilink")（汐碇路）
  - [TW_THWtp47.png](https://zh.wikipedia.org/wiki/File:TW_THWtp47.png "fig:TW_THWtp47.png")
    [北47線](../Page/北47線.md "wikilink")（碇格路）
  - [TW_THWtp47-1.png](https://zh.wikipedia.org/wiki/File:TW_THWtp47-1.png "fig:TW_THWtp47-1.png")
    [北47-1線](../Page/北47線#支線.md "wikilink")（烏塗～十股寮）

### 公車

#### [臺北聯營公車](../Page/臺北聯營公車.md "wikilink")

  - [標誌(小)mark(S).jpg](https://zh.wikipedia.org/wiki/File:標誌\(小\)mark\(S\).jpg "fig:標誌(小)mark(S).jpg")（[深坑](../Page/深坑區.md "wikilink")
    - [圓環](../Page/台北圓環.md "wikilink")）

  - [標誌(小)mark(S).jpg](https://zh.wikipedia.org/wiki/File:標誌\(小\)mark\(S\).jpg "fig:標誌(小)mark(S).jpg")（深坑
    -
    經[TWHW3a.svg](https://zh.wikipedia.org/wiki/File:TWHW3a.svg "fig:TWHW3a.svg")[國道三號甲線](../Page/國道三號甲線_\(中華民國\).md "wikilink")
    - 捷運[古亭站](../Page/古亭站.md "wikilink"))

  - [標誌(小)mark(S).jpg](https://zh.wikipedia.org/wiki/File:標誌\(小\)mark\(S\).jpg "fig:標誌(小)mark(S).jpg")（[深坑](../Page/深坑區.md "wikilink")
    - 經[信義快速道路](../Page/信義快速道路.md "wikilink") -
    捷運[市政府站](../Page/市政府站.md "wikilink")）

  - （[捷運昆陽站](../Page/捷運昆陽站.md "wikilink") -
    [光明寺](../Page/光明寺.md "wikilink")）

  - （[木柵](../Page/木柵.md "wikilink") -
    [平溪](../Page/平溪區.md "wikilink")，[台灣好行](../Page/台灣好行.md "wikilink")**<font color="red">木柵平溪線</font>**）

  - ([景美](../Page/景美.md "wikilink") -
    華梵大學、[景美](../Page/景美.md "wikilink") -
    烏塗窟、[景美](../Page/景美.md "wikilink") - 皇帝殿)

  - （[坪林](../Page/坪林區.md "wikilink")
    -經[TWHW5.svg](https://zh.wikipedia.org/wiki/File:TWHW5.svg "fig:TWHW5.svg")[國道五號](../Page/蔣渭水高速公路.md "wikilink")、[TW_CHW106b.svg](https://zh.wikipedia.org/wiki/File:TW_CHW106b.svg "fig:TW_CHW106b.svg")[文山路及](../Page/文山路.md "wikilink")[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[國道三號](../Page/福爾摩沙高速公路.md "wikilink")
    - 捷運[新店站](../Page/新店站.md "wikilink")）

  - （捷運[新店站](../Page/新店站.md "wikilink") -
    [坪林](../Page/坪林區.md "wikilink")）

#### 國道客運

  - [葛瑪蘭客運](../Page/葛瑪蘭客運.md "wikilink")**<font color="red">1915</font>**
    ([板橋客運站](../Page/板橋客運站.md "wikilink") -
    [萬華車站](../Page/萬華車站.md "wikilink") -
    [臺北轉運站](../Page/臺北轉運站.md "wikilink") -
    經[TWHW3a.svg](https://zh.wikipedia.org/wiki/File:TWHW3a.svg "fig:TWHW3a.svg")[國道三甲](../Page/國道三號甲線_\(中華民國\).md "wikilink")、[TW_CHW106b.svg](https://zh.wikipedia.org/wiki/File:TW_CHW106b.svg "fig:TW_CHW106b.svg")[文山路及](../Page/文山路.md "wikilink")[TWHW5.svg](https://zh.wikipedia.org/wiki/File:TWHW5.svg "fig:TWHW5.svg")[國道五號](../Page/蔣渭水高速公路.md "wikilink")
    - [礁溪](../Page/礁溪鄉.md "wikilink") - [宜蘭市](../Page/宜蘭市.md "wikilink")
    - [羅東](../Page/羅東鎮_\(臺灣\).md "wikilink"))
      - 每日兩班次往返行經[文山路](../Page/文山路.md "wikilink")，於楓子林路口增設站位

### 未來

### 輕軌

  - [新北捷運](../Page/新北捷運.md "wikilink")

<!-- end list -->

  - <font color={{新北捷運色彩|深}}>█</font> </small>[深坑輕軌](../Page/深坑輕軌.md "wikilink")：[<font color="#888888">石碇服務區站</font>](../Page/石碇服務區站.md "wikilink")

## 教育

  - 大專院校
      - [華梵大學](../Page/華梵大學.md "wikilink")
  - 高級中等學校
      - [新北市立石碇高級中學](../Page/新北市立石碇高級中學.md "wikilink")
  - 國民中學
      - 新北市立石碇高級中學國中部
  - 國民小學
      - [新北市石碇區石碇國民小學](../Page/新北市石碇區石碇國民小學.md "wikilink")
      - [新北市石碇區雲海國民小學](../Page/新北市石碇區雲海國民小學.md "wikilink")
      - [新北市石碇區和平國民小學](../Page/新北市石碇區和平國民小學.md "wikilink")
      - [新北市石碇區永定國民小學](../Page/新北市石碇區永定國民小學.md "wikilink")
  - 幼兒園
      - 新北市石碇區永定國民小學幼兒園
      - 新北市立石碇幼兒園(豐林里)
      - 新北市立石碇幼兒園(中民里)

<!-- end list -->

  - 圖書館
      - [新北市立圖書館石碇分館](../Page/新北市立圖書館.md "wikilink")：位於新北市石碇區潭邊里碇坪路一段65號

## 旅遊

  - [八分寮福德宮](../Page/八分寮福德宮.md "wikilink")
  - [石碇虎爺公廟](../Page/石碇虎爺公廟.md "wikilink")
  - [二格公園](../Page/二格公園.md "wikilink")
  - [石碇五路財神廟](../Page/石碇五路財神廟.md "wikilink")
  - [石碇姑娘廟](../Page/石碇姑娘廟.md "wikilink")
  - [筆架山登山步道系統](../Page/筆架山登山步道系統.md "wikilink")
  - [橫翠微蝴蝶館](../Page/橫翠微蝴蝶館.md "wikilink")
  - [石碇老街](../Page/石碇老街.md "wikilink")
  - [石碇宗教博物館](../Page/石碇宗教博物館.md "wikilink")
  - [皇帝殿](../Page/皇帝殿山登山步道.md "wikilink")
  - [翡翠水庫](../Page/翡翠水庫.md "wikilink")
  - 石碇手工麵線
  - 千島湖
  - [新北市立圖書館石碇分館](../Page/新北市立圖書館.md "wikilink")

## 參見

  - [鹿窟](../Page/鹿窟.md "wikilink")
  - [皇帝殿山登山步道](../Page/皇帝殿山登山步道.md "wikilink")

## 參考資料

1.  伊能嘉矩，《大日本地名辭書續篇：台灣篇》
2.  黃逢永日，《台灣生熟番記事》　(台北：台灣銀行經濟研究室，台灣文獻叢刊第51種，1960)
3.  陳淑均，《噶馬蘭廳志》　(台北：台灣銀行經濟研究室台灣文獻叢刊第160種，1963)
4.  陳培桂,《淡水廳志》
5.  陳正祥,《台北市志》

## 外部連結

  - [石碇區公所](http://www.shiding.ntpc.gov.tw/)
  - [石碇觀光發展協會](https://www.facebook.com/sdtour223/)

[Category:石碇區](../Category/石碇區.md "wikilink")
[Category:無醫鄉](../Category/無醫鄉.md "wikilink")