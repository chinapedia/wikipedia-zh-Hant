**石美鑫**（），[福建](../Page/福建.md "wikilink")[福州人](../Page/福州.md "wikilink")，中国著名的外科医学家。\[1\]

## 经历

1918年1月生。1943年毕业于[国立上海医学院](../Page/国立上海医学院.md "wikilink")。1978年出任[上海医学院](../Page/上海医学院.md "wikilink")(当时名为上海第一医学院)院长。现为上海医学院附属中山医院外科学教授、上海市心血管病研究所名誉所长。并任《辞海》、《《胸部外科学》、《中国医学百科全书》副主编。

曾任国务院学位委员会委员，卫生部学位委员会副主任委员，中华胸心血管外科学会、中华医学教育学会、中华心血管病学会副主任委员，中华医学会、上海市医学会副会长，上海市科协副主席，上海市胸心血管外科学会主任委员，上海市生物医学工程学会理事长，上海市学位委员会顾问，上海医学院院长、上海医科大学顾问等职。

2000年上医和复旦大学合并。为了恢复上医，石美鑫先生领衔起草要求恢复上医校名的公开信，公开信中教授们从上医的历史渊源、医学学科生态、知识产权等方面陈述了复名的理由。在征得包括附属医院在内的全校600多名教授签名后，信件还通过上医[波士顿校友会和](../Page/波士顿.md "wikilink")[西雅图校友会又征集到](../Page/西雅图.md "wikilink")200馀名校友签名，最后递交国家教育部、卫生部等单位。与此同时，[秦伯益](../Page/秦伯益.md "wikilink")、[陈灏珠](../Page/陈灏珠.md "wikilink")、[汤钊猷](../Page/汤钊猷.md "wikilink")、[顾玉东](../Page/顾玉东.md "wikilink")、[闻玉梅](../Page/闻玉梅.md "wikilink")、[桑国卫](../Page/桑国卫.md "wikilink")、[侯惠民](../Page/侯惠民.md "wikilink")、[池志强](../Page/池志强.md "wikilink")、[顾建人等](../Page/顾建人.md "wikilink")10名中国工程院医药卫生学部院士，向上海市市长[徐匡迪院士上书](../Page/徐匡迪.md "wikilink")，请求“慎重研究”，为“上海医学院”正名。经过不懈的抗争，2001年7月27日，上海医学院正式复名。

## 著作

主编《实用外科学》、《胸心外科手术图解》、《血管外科手术图谱》、《乡村医生手册》》等。并主编《中华胸心血管外科杂志》、《上海生物医学工程杂志》。

参加编写全国高等医学院校教科书《外科学》、《沈克非外科学》、《黄家驷外科学》、《血管外科学》、《胸心外科手术学》等书。

曾经发表学术论著近百篇。

## 参考资料

## 外部链接

  - [石美鑫教授](https://web.archive.org/web/20050507080224/http://www.shmu.edu.cn/tm/zs/zssmx.htm)

[Category:中国医学家](../Category/中国医学家.md "wikilink")
[Category:石姓](../Category/石姓.md "wikilink")

1.