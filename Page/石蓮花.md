**石蓮花**([学名](../Page/学名.md "wikilink")：**），又稱**宝石花**、**粉莲**、**胧月**、**初霜**、**石莲掌**、**莲花掌**、**風車草**、**觀音草**、**蓮座草**、**石膽草**、**神明草**，屬[景天科](../Page/景天科.md "wikilink")[植物](../Page/植物.md "wikilink")，原產地[墨西哥](../Page/墨西哥.md "wikilink")[伊達爾戈州](../Page/伊達爾戈州.md "wikilink")。\[1\]

## 用途

人類食用石蓮花的歷史相當悠久，東羅馬帝國的查理曼（Charlemagne）就在屋頂上遍植石蓮花，墨西哥人認為將它種植在屋頂上有避雷擊之效。石蓮花屬寒性，甘涼，繁殖及適應力很強，盛開期為三、四月間。葉面多肉，男女老少，皆可食用。

石蓮花富含膳食纖維、磷、鈣、鉀、鈉、鎂、鐵等[礦物質](../Page/礦物質.md "wikilink")，以及[維生素C](../Page/維生素.md "wikilink")、B1、B2、B6、[葉酸](../Page/葉酸.md "wikilink")、[菸鹼酸](../Page/菸鹼酸.md "wikilink")、β-胡蘿葡素，臨床證實石蓮花可治療濕熱型肝炎、肝硬化、[痛風](../Page/痛風.md "wikilink")、[牙周病](../Page/牙周病.md "wikilink")、[皮膚病](../Page/皮膚病.md "wikilink")、[高血壓](../Page/高血壓.md "wikilink")、[便秘等疾病](../Page/便秘.md "wikilink")。

石蓮花可直接食用，口感不錯類似[蓮霧且水份多](../Page/蓮霧.md "wikilink")，但味道極酸。

## 参考文献

[H](../Category/景天科.md "wikilink")
[Category:耐旱植物](../Category/耐旱植物.md "wikilink")
[Category:多肉植物](../Category/多肉植物.md "wikilink")

1.  "Botanica. The Illustrated AZ of over 10000 garden plants and how to
    cultivate them", pp. 410-411. Könemann, 2004. ISBN 3-8331-1253-0