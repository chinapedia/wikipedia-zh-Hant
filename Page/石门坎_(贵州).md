**石门坎**（[苗语](../Page/苗语.md "wikilink")：hmaoblisnaf），是[中国](../Page/中国.md "wikilink")[贵州](../Page/贵州.md "wikilink")[威宁县](../Page/威宁县.md "wikilink")[石门乡的一个著名](../Page/石门乡_\(威宁县\).md "wikilink")[苗族村寨](../Page/苗族.md "wikilink")，石门乡政府所在地。石门坎位于[乌蒙山腹地](../Page/乌蒙山.md "wikilink")，目前经济靠[煤矿开采支撑](../Page/煤矿.md "wikilink")。

石门坎原为苗族一支[大花苗的聚居地](../Page/大花苗.md "wikilink")。1905年，[英国](../Page/英国.md "wikilink")[卫理公会](../Page/卫理公会.md "wikilink")[传教士](../Page/传教士.md "wikilink")[柏格理应大花苗的邀请](../Page/柏格理.md "wikilink")，来当地传教。柏格理在当地兴建了教堂、医院和学校，并为当地苗民引进了[土豆](../Page/土豆.md "wikilink")、[玉米等农作物以及农业试验场](../Page/玉米.md "wikilink")。他还为当地苗民改进了土灶和纺织机，并在当地人的帮助下，历史上第一次为苗语创建了书写系统：[滇东北老苗文](../Page/滇东北老苗文.md "wikilink")（又称为石门坎苗文）。柏格理及其他传教士还试图按照西方的习惯改良当地的风俗，引入了多种体育、文化运动，并坚持举办[端午节运动会](../Page/端午节.md "wikilink")，成为全苗族的盛会。

在短短时间内，石门坎一跃成为中国西南地区经济、文化最为发达的地区之一，全民受教育程度甚至超过了同时代的许多汉族地区，石门坎中学校友中甚至拥有[博士两名](../Page/博士.md "wikilink")，23人进入了大学学习，著名人物包括[杨汉先](../Page/杨汉先.md "wikilink")（[贵州大学副校长](../Page/贵州大学.md "wikilink")、创建人之一）、[张超伦](../Page/张超伦.md "wikilink")（[纽约大学医学博士](../Page/纽约大学.md "wikilink")）等。当时石门坎被称为"西南苗族最高文化区"。

1948年10月9日，石门坎发生5.8级[地震](../Page/地震.md "wikilink")，造成了沉重损失。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，石门坎被选为[滇东北苗语的代表点](../Page/苗语滇东北次方言.md "wikilink")。在[三年大饥荒和](../Page/三年大饥荒.md "wikilink")[文化大革命的打击下](../Page/文化大革命.md "wikilink")，石门坎的基础设施破坏殆尽，苗语教学一度被完全停止。目前石门坎已经成为威宁最贫穷的地区，失学率极高，[氟中毒现象普遍](../Page/氟中毒.md "wikilink")。

目前，[香港](../Page/香港.md "wikilink")[乐施会在当地推行小额贷款扶贫项目](../Page/乐施会.md "wikilink")。

## 参考资料

  - [李昌平：走进石门坎](https://web.archive.org/web/20070928191254/http://www.wu-xian.com/wx_24095_78.htm)
  - [南方周末·福音下的石门坎](http://www.nanfangdaily.com.cn/zm/20061019/wh/dl/200610190051.asp)
  - [贵州苗族教育史](https://web.archive.org/web/20060227213451/http://ethnic.eastedu.org/ziliaoku/2001/20011024-06.htm)
  - [中国青年报·石门坎往昔：贫困村的西式教育试验](http://focus.news.163.com/10/0915/09/6GK555LC00011SM9.html)

[category:威宁自治县](../Page/category:威宁自治县.md "wikilink")