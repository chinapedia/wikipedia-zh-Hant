**矽鎂石**（Humite），是棕橙色的半透明玻璃光澤寶石，它在[義大利西南部的](../Page/義大利.md "wikilink")[維蘇威火山群裏被發現](../Page/維蘇威火山.md "wikilink")。這個含有[鐵](../Page/鐵.md "wikilink")、[鎂和](../Page/鎂.md "wikilink")[氟與](../Page/氟.md "wikilink")[氫的寶石化學公式為](../Page/氫.md "wikilink")(Mg,Fe)<sub>7</sub>(SiO<sub>4</sub>)<sub>3</sub>(F,OH)<sub>2</sub>

矽鎂石於1813年首次被發表，並以礦物收藏家[亞伯拉罕·修姆](../Page/亞伯拉罕·修姆.md "wikilink")（1749-1838）之名命名為Humite。

## 物理特性

  - 斜方晶系，硬度6-6.5
  - 包裹體：平行針狀物群，負晶，晶體包體
  - 折射率：1.658-1.630
  - 折射率：0.028-0.030
  - 比重：3.1-3.2 平均3.15
  - 解理：差

<center>

[File:Humite4.jpg|暗場照明,30x](File:Humite4.jpg%7C暗場照明,30x)
[File:Humite3.jpg|暗場照明,65x](File:Humite3.jpg%7C暗場照明,65x)
[File:Humite.jpg|實物2.55ct](File:Humite.jpg%7C實物2.55ct)

</center>

## 參考

  - [矽鎂石 - Webmineral](http://webmineral.com/data/Humite.shtml)
  - [矽鎂石 - Mindat](http://www.mindat.org/min-1947.html)

[Category:寶石](../Category/寶石.md "wikilink")
[Category:晶體](../Category/晶體.md "wikilink")
[Category:矽酸鹽礦物](../Category/矽酸鹽礦物.md "wikilink")