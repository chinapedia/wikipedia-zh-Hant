**砧骨**（Incus、Anvil）是是人耳中[砧板形狀的小](../Page/砧板.md "wikilink")[骨](../Page/骨.md "wikilink")，是三块[聽小骨](../Page/聽小骨.md "wikilink")（ossicles）中的一個，连接锤骨和镫骨。

## 圖集

[`File:Gray43.png|Head`](File:Gray43.png%7CHead)` and neck of a human embryo eighteen weeks old, with Meckel’s cartilage and hyoid bar exposed.`
[`File:Gray907.png|External`](File:Gray907.png%7CExternal)` and middle ear, opened from the front. Right side.`
[`File:Gray919.png|Chain`](File:Gray919.png%7CChain)` of ossicles and their ligaments, seen from the front in a vertical, transverse section of the tympanum. `
<File:Illu>` auditory ossicles.jpg|Ossicles`

## 関連項目

[Auditory_ossicles_(bovine).jpg](https://zh.wikipedia.org/wiki/File:Auditory_ossicles_\(bovine\).jpg "fig:Auditory_ossicles_(bovine).jpg")的[中耳構造](../Page/中耳.md "wikilink")

</center>

1.槌骨　2.砧骨　2'.砧骨豆状突起　3.鐙骨　4.鼓膜　5.卵圓窗　6.鼓膜張筋　7.鐙骨筋\]\]

## 参见

  - [人體骨骼列表](../Page/人體骨骼列表.md "wikilink")

## 註釋

## 外部連結

  - [The Anatomy Wiz](http://www.anatomywiz.com?Incus1)Incus

[nl:Gehoorbeentje\#Aambeeld](../Page/nl:Gehoorbeentje#Aambeeld.md "wikilink")

[Category:解剖学](../Category/解剖学.md "wikilink")
[Category:耳科](../Category/耳科.md "wikilink")
[Category:骨骼](../Category/骨骼.md "wikilink")