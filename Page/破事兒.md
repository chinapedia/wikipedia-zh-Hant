是一部2007年上映的[香港電影](../Page/香港.md "wikilink")。小電影由七個短故事組成，每個故事的長度不一，短的只有數分鐘，長的則有十數分鐘。由[彭浩翔執導](../Page/彭浩翔.md "wikilink")，一眾明星主演。在2007年12月20日[香港上映](../Page/香港.md "wikilink")。

《破事兒》本來是[彭浩翔所著的同名短篇](../Page/彭浩翔.md "wikilink")[小說集](../Page/小說.md "wikilink")。作者認為[愛](../Page/愛.md "wikilink")[慾](../Page/慾.md "wikilink")[生](../Page/生.md "wikilink")[死也不過是些](../Page/死.md "wikilink")「破事兒」。

## 主要演員

不可抗力:

  - [林海峰](../Page/林海峰.md "wikilink") （聲音演出） - 心理醫生
  - [陳輝虹](../Page/陳輝虹.md "wikilink") - 陳教授
  - [田蕊妮](../Page/田蕊妮.md "wikilink") - 陳太 （師母）
  - [李志健](../Page/李志健.md "wikilink") - 幻想男
  - [無名](../Page/無名.md "wikilink") - 幻想女

公德心:

  - [陳冠希](../Page/陳冠希.md "wikilink") - 的士高男
  - [鄭融](../Page/鄭融.md "wikilink") - 的士高女

做節:

  - [陳奕迅](../Page/陳奕迅.md "wikilink") - 鄭錦富
  - [陳逸寧](../Page/陳逸寧.md "wikilink") - 陳惠英
  - [杜汶澤](../Page/杜汶澤.md "wikilink") - 鄭錦富友人
  - [尹志文](../Page/尹志文.md "wikilink") - 巡警/護衞員

德雅星:

  - [關智斌](../Page/關智斌.md "wikilink") - 阿池
  - [Angelababy](../Page/Angelababy.md "wikilink") - 德雅
  - [譚耀文](../Page/譚耀文.md "wikilink") - 雜誌編輯
  - [谷德昭](../Page/谷德昭.md "wikilink") （聲音演出） - 老師

大頭阿慧:

  - [鄧麗欣](../Page/鄧麗欣.md "wikilink") - 張可琪 （阿琪）
  - [鍾欣潼](../Page/鍾欣潼.md "wikilink") - 鄭詩慧 （阿慧）
  - [麥浚龍](../Page/麥浚龍.md "wikilink") - 飛鷹
  - [潘恆生](../Page/潘恆生.md "wikilink") - 慧父
  - [林一峰](../Page/林一峰.md "wikilink") - 歌手

增值:

  - [杜汶澤](../Page/杜汶澤.md "wikilink") - 阿強
  - [張錚](../Page/張錚_\(中國大陸\).md "wikilink") - 菲菲
  - [陳奕迅](../Page/陳奕迅.md "wikilink") - 陳奕迅

尊尼亞:

  - [余文樂](../Page/余文樂.md "wikilink") - 初級殺手
  - [陳子聰](../Page/陳子聰.md "wikilink") - 陳偉楊
  - [金培達](../Page/金培達.md "wikilink") - 黃生
  - [馮小剛](../Page/馮小剛.md "wikilink") - 殺手集團市場推廣主任

## 外部連結

  -
  -
  - {{@movies|fthk31149603}}

  -
  -
  -
  -
  -
  -
  - \[<http://epg.i-cable.com/new/movie/trivial_matters.php?from_ch=041《破事兒>》\]
    的有線寬頻 i-CABLE 電影專頁

[7](../Category/2000年代香港電影作品.md "wikilink")
[T](../Category/彭浩翔電影.md "wikilink")
[Category:小說改編電影](../Category/小說改編電影.md "wikilink")
[Category:2000年代喜劇片](../Category/2000年代喜劇片.md "wikilink")
[Category:喜劇劇情片](../Category/喜劇劇情片.md "wikilink")
[Category:香港喜劇片](../Category/香港喜劇片.md "wikilink")
[Category:多段式電影](../Category/多段式電影.md "wikilink")
[Category:金培达配乐电影](../Category/金培达配乐电影.md "wikilink")