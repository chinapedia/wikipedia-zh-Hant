**砷化鎘**是一種灰黑色的[半導體材料](../Page/半導體.md "wikilink")，[分子式為Cd](../Page/分子式.md "wikilink")<sub>3</sub>As<sub>2</sub>。它的[能隙有](../Page/能隙.md "wikilink")0.14[eV](../Page/电子伏特.md "wikilink")，与其他[半導體相比较窄](../Page/半導體.md "wikilink")。

## 參見

  - [半導體](../Page/半導體.md "wikilink")

## 外部連結

  - [National Pollutant Inventory - Cadmium and
    compounds](https://web.archive.org/web/20061210213049/http://www.npi.gov.au/database/substance-info/profiles/17.html)

[Category:砷化物](../Category/砷化物.md "wikilink")
[Category:镉化合物](../Category/镉化合物.md "wikilink")
[Category:半导体材料](../Category/半导体材料.md "wikilink")