**硝化纤维**（Nitrocellulose），学名**纤维素硝酸酯**，也称**硝化棉**、**硝基纖維素**，通常由棉絨纖維和木漿等[纤维材料浸入浓硝酸浓硫酸混合液中制得](../Page/纤维.md "wikilink")，多數用于制作發射藥。与[硝化甘油相比](../Page/硝化甘油.md "wikilink")，比較稳定，安全，便于运输。为白色或微黄色棉絮状物，易燃且具有爆炸性，化学稳定性较差，常温下能缓慢分解并放热，超过40°C时会加速分解，放出的热量如不能及时散失，会造成硝化棉温升加剧，达到180°C时能发生自燃。硝化棉通常加乙醇或水作湿润剂，分别称作硝化棉酒棉或硝化棉水棉。一旦湿润剂散失，极易引发火灾。如果在装卸作业中存在野蛮操作，发生硝化棉装箱包装破损、硝化棉散落的情况。试验表明，包装破损时，在50°C下2小时乙醇湿润剂会全部挥发散失。易于导致仓储自燃。

  - 1838年，T·J·[佩卢兹首先发现棉花在硝酸中浸过之后可以燃燒](../Page/佩卢兹.md "wikilink")。

  - 1845年，[德国化学家](../Page/德国.md "wikilink")[C·F·舍恩拜将棉花浸于硝酸和硫酸混合液中](../Page/克里斯提安·弗里德里希·尚班.md "wikilink")，然后洗掉多余的酸液，从而发明出硝化纤维。

  - 1860年，[普鲁士军队的少校E](../Page/普鲁士.md "wikilink")·[郐尔茨用硝化纤维制成枪](../Page/郐尔茨.md "wikilink")、炮弹的[裝藥](../Page/裝藥.md "wikilink")。

  - .

目前世界上生产硝化纤维的厂家主要有：[法国](../Page/法国.md "wikilink")[BNC公司和](../Page/BNC公司.md "wikilink")[台湾](../Page/台湾.md "wikilink")[台硝集团](../Page/台硝集团.md "wikilink")，这两家公司最近在[南京化工园合资兴建南京中硝化工有限公司](../Page/南京.md "wikilink")，工厂建成后每年可生产1.5万吨涂料用硝化纤维素。

## 制法

用浓[硫酸做催化剂](../Page/硫酸.md "wikilink")，将[纤维素](../Page/纤维素.md "wikilink")（[棉花](../Page/棉花.md "wikilink")）和[硝酸混合可制得](../Page/硝酸.md "wikilink")。反应如下：

  -
    3*n*HNO<sub>3</sub>+
    \[C<sub>6</sub>H<sub>7</sub>O<sub>2</sub>(OH)<sub>3</sub>\]<sub>n</sub>
    →
    \[C<sub>6</sub>H<sub>7</sub>O<sub>2</sub>(ONO<sub>2</sub>)<sub>3</sub>\]<sub>n</sub>
    + 3*n*H<sub>2</sub>O

將硝酸與硫酸以一比二的方式小心混合，溫度要低於20度。將纖維(棉花)浸入，20分鐘後取出、晾乾。

## 用途

含[氮量高的俗称](../Page/氮.md "wikilink")**火棉**，用以制造无烟火药；含氮量低的俗称**胶棉**，用以制造喷漆、人造革、胶片、塑料等。

  - [塗料](../Page/塗料.md "wikilink")（木器、紙類印刷物等）
  - [賽璐珞](../Page/賽璐珞.md "wikilink")（celluloid），歐盟於2006年10月26日公告，禁用於製造玩具。

## 结构图

Image:Nitrocellulose-2D-skeletal.png|[结构简式](../Page/结构简式.md "wikilink")
Image:Nitrocellulose-3D-balls.png|[球棍模型](../Page/球棍模型.md "wikilink")
Image:Nitrocellulose-3D-vdW.png|[空間填充模型](../Page/空間填充模型.md "wikilink")

[category:硝酸酯](../Page/category:硝酸酯.md "wikilink")
[category:爆炸物](../Page/category:爆炸物.md "wikilink")
[category:第五类危险品](../Page/category:第五类危险品.md "wikilink")
[category:纤维素](../Page/category:纤维素.md "wikilink")

[Category:炸藥](../Category/炸藥.md "wikilink")
[Category:棉](../Category/棉.md "wikilink")