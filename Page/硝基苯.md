**硝基苯**（俗称**人造苦杏仁油**）是一种剧毒有机物，有像杏仁油的特殊气味。[化学式为](../Page/化学式.md "wikilink")[C](../Page/碳.md "wikilink")<sub>6</sub>[H](../Page/氢.md "wikilink")<sub>5</sub>[N](../Page/氮.md "wikilink")[O](../Page/氧.md "wikilink")<sub>2</sub>。纯品是几乎无色至淡黄色的晶体或油状液体，不溶于水。

硝基苯用作溶剂和温和的[氧化剂](../Page/氧化剂.md "wikilink")。它的主要用途是制造[苯胺](../Page/苯胺.md "wikilink")，也常用作绝缘物质和光泽剂。中毒可用亚甲蓝解救。

## 合成

硝基苯主要是通过使用浓硝酸和浓硫酸的混合物对[苯进行](../Page/苯.md "wikilink")[硝化反应来制备](../Page/硝化反应.md "wikilink")。这个反应是一个典型的[亲电芳香取代反应](../Page/亲电芳香取代反应.md "wikilink")。

## 化学性质

硝基苯较易通过[催化氢化等方法还原](../Page/催化.md "wikilink")，还原的最终产物是[苯胺](../Page/苯胺.md "wikilink")。硝基苯作为温和的氧化剂，在合成[喹啉的](../Page/喹啉.md "wikilink")[斯克劳普合成法中得到了良好的应用](../Page/斯克劳普合成法.md "wikilink")，它负责将中间产物[1,2-二氢喹啉氧化为喹啉](../Page/1,2-二氢喹啉.md "wikilink")。

硝基是一个吸电子基团，这使得苯环上的[π电子密度大大降低](../Page/π键.md "wikilink")，从而使硝基苯参与亲电取代反应的能力有所减弱，同时使硝基成为了间位定位基。硝基苯仍可进行[硝化反应和](../Page/硝化反应.md "wikilink")[卤代反应](../Page/卤代反应.md "wikilink")，得到相应的间位衍生物，但不参与[傅-克反应](../Page/傅-克反应.md "wikilink")。

## 工业应用

硝基苯是工业上制备苯胺和苯胺衍生物（如[扑热息痛](../Page/扑热息痛.md "wikilink")）的重要原料，同时也被广泛用于[橡胶](../Page/橡胶.md "wikilink")、[杀虫剂](../Page/杀虫剂.md "wikilink")、[染料以及](../Page/染料.md "wikilink")[药物的生产](../Page/药物.md "wikilink")。硝基苯也被用于[涂料](../Page/涂料.md "wikilink")[溶剂](../Page/溶剂.md "wikilink")、[皮革上光剂](../Page/皮革.md "wikilink")、地板抛光剂等，在这里硝基苯主要用于掩蔽这些材料本身的异味。值得一提的是，硝基苯甚至还曾被用于[肥皂的廉价](../Page/肥皂.md "wikilink")[香料](../Page/香料.md "wikilink")。

## 外部链接

  - <http://www.epa.gov/chemfact/nitro-sd.txt>
  - <http://www.epa.gov/chemfact/nitro-sd.pdf>

[硝基苯](../Category/硝基苯.md "wikilink")
[Category:硝基溶剂](../Category/硝基溶剂.md "wikilink")
[Category:IARC第2B类致癌物质](../Category/IARC第2B类致癌物质.md "wikilink")
[Category:非线性光学材料](../Category/非线性光学材料.md "wikilink")