**硝酸鈰銨**是化學物質的一種，外觀为呈现橘紅色的[晶體](../Page/晶體.md "wikilink")，[分子式為](../Page/分子式.md "wikilink")
(NH<sub>4</sub>)<sub>2</sub>Ce(NO<sub>3</sub>)<sub>6</sub>，通常在[有機合成中作为](../Page/有機合成.md "wikilink")[氧化剂及生产其它含](../Page/氧化剂.md "wikilink")[铈化合物](../Page/铈.md "wikilink")。硝酸铈铵溶于水时，完全电离，生成、和三种离子。\[1\]

硝酸铈铵固体中含有两种[离子](../Page/离子.md "wikilink")：[NH<sub>4</sub><sup>+</sup>](../Page/铵根离子.md "wikilink")
和
\[Ce(NO<sub>3</sub>)<sub>6</sub>\]<sup>2−</sup>。六硝酸根合铈（IV）离子中，[硝酸根作为双齿](../Page/硝酸根.md "wikilink")[配体与](../Page/配体_\(化学\).md "wikilink")[Ce](../Page/铈.md "wikilink")[螯合](../Page/螯合.md "wikilink")。

  -
    [Hexanitratocerat.svg](https://zh.wikipedia.org/wiki/File:Hexanitratocerat.svg "fig:Hexanitratocerat.svg")

## 制备

硝酸铈铵可由[硝酸铈(III)为原料制备](../Page/硝酸铈\(III\).md "wikilink")：\[2\]

  -
    2 Ce(NO<sub>3</sub>)<sub>3</sub> + 6 NH<sub>3</sub>·H<sub>2</sub>O +
    H<sub>2</sub>O<sub>2</sub> → 2 Ce(OH)<sub>4</sub>↓ + 6
    NH<sub>4</sub>NO<sub>3</sub>

将[氢氧化铈(IV)用硝酸溶解](../Page/氢氧化铈\(IV\).md "wikilink")，加铵盐沉淀，得到硝酸铈铵：

  -
    Ce(OH)<sub>4</sub> + 4 H<sup>+</sup> + 2 NH<sub>4</sub><sup>+</sup>
    + 6 NO<sub>3</sub><sup>−</sup> →
    (NH<sub>4</sub>)<sub>2</sub>Ce(NO<sub>3</sub>)<sub>6</sub>↓ + 4
    H<sub>2</sub>O

## 参考文献

[Category:硝酸盐](../Category/硝酸盐.md "wikilink")
[Category:铵盐](../Category/铵盐.md "wikilink")
[Category:配位化合物](../Category/配位化合物.md "wikilink")
[Category:氧化剂](../Category/氧化剂.md "wikilink")
[Category:复盐](../Category/复盐.md "wikilink")
[Category:四价铈化合物](../Category/四价铈化合物.md "wikilink")

1.  洪广言. 现代化学基础丛书 第36卷 稀土化学导论. 北京: 科学出版社, 2014. pp. 146 ISBN
    978-7-03-040581-4
2.  王晓平, 陈国奇, 万军.
    [硝酸铈铵复盐制备工艺的研究](http://www.ixueshu.com/document/71e577ab5ad89c0a.html)\[J\].
    宁夏工程技术, 2008, 7(1):60-62.