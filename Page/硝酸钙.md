**硝酸钙**\[[Ca](../Page/钙.md "wikilink")([N](../Page/氮.md "wikilink")[O](../Page/氧.md "wikilink")<sub>3</sub>)<sub>2</sub>\]是[硝酸根](../Page/硝酸根.md "wikilink")[离子与](../Page/离子.md "wikilink")[钙离子化和生成的](../Page/钙.md "wikilink")[无机盐](../Page/无机盐.md "wikilink")。为无色透明单斜晶体。

## 物理和化学性质

硝酸钙易溶于[水](../Page/水.md "wikilink")、[甲醇](../Page/甲醇.md "wikilink")、[乙醇](../Page/乙醇.md "wikilink")、[戊醇](../Page/戊醇.md "wikilink")、[丙酮](../Page/丙酮.md "wikilink")、[醋酸甲酯及](../Page/醋酸甲酯.md "wikilink")[液氨](../Page/液氨.md "wikilink")。置于[空气中易](../Page/空气.md "wikilink")[潮解](../Page/潮解.md "wikilink")。硝酸钙是[氧化剂](../Page/氧化剂.md "wikilink")，遇到[有机物](../Page/有机物.md "wikilink")、[硫等就会燃烧爆炸](../Page/硫.md "wikilink")，发出红色火焰。于132℃分解；若再加热至495～500℃时会分解为[氧气和](../Page/氧气.md "wikilink")[亚硝酸钙](../Page/亚硝酸钙.md "wikilink")；若继续加热则分解生成[氧化氮气体和](../Page/氧化氮.md "wikilink")[氧化钙](../Page/氧化钙.md "wikilink")。

## 安全性

硝酸钙对皮肤有刺激和烧灼的作用，皮肤接触会发红、瘙痒，也可能大面积的溃疡。应避免吸入其粉末。

\(\)

## 用途

用于制造[氮肥](../Page/氮肥.md "wikilink")、[煙火和](../Page/煙火.md "wikilink")[硝酸盐](../Page/硝酸盐.md "wikilink")。

## 制取

常将[碳酸钙投入](../Page/碳酸钙.md "wikilink")[硝酸中制取硝酸钙](../Page/硝酸.md "wikilink")：

  -
    CaCO<sub>3</sub> + 2HNO<sub>3</sub> → Ca(NO<sub>3</sub>)<sub>2</sub>
    + H<sub>2</sub>O + CO<sub>2</sub>↑

[Category:钙化合物](../Category/钙化合物.md "wikilink")
[Category:硝酸盐](../Category/硝酸盐.md "wikilink")
[Category:硝酸鹽礦物](../Category/硝酸鹽礦物.md "wikilink")
[Category:烟火氧化剂](../Category/烟火氧化剂.md "wikilink")
[Category:烟火着色剂](../Category/烟火着色剂.md "wikilink")
[Category:化學肥料](../Category/化學肥料.md "wikilink")
[Category:氧化剂](../Category/氧化剂.md "wikilink")
[Category:易制爆化学品](../Category/易制爆化学品.md "wikilink")