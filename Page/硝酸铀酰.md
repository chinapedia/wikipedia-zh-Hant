**硝酸鈾醯**
（UO<sub>2</sub>(NO<sub>3</sub>)<sub>2</sub>），是一種易溶于[水的黃色固体](../Page/水.md "wikilink")，有[放射性](../Page/放射性.md "wikilink")。它的相对摩爾質量為394.04
g/mol（無水）。水合物為黃綠色的\[1\]
六水合硝酸鈾酰（UO<sub>2</sub>(NO<sub>3</sub>)<sub>2</sub>.6H<sub>2</sub>O），水合物結晶具[摩擦發光](../Page/摩擦發光.md "wikilink")（triboluminescent）性質。

硝酸鈾醯可由鈾鹽和[硝酸反應製備](../Page/硝酸.md "wikilink")。它可溶於水、[乙醇](../Page/乙醇.md "wikilink")、[丙酮和](../Page/丙酮.md "wikilink")[乙醚](../Page/乙醚.md "wikilink")，但不溶於[苯](../Page/苯.md "wikilink")、[甲苯和](../Page/甲苯.md "wikilink")[氯仿](../Page/氯仿.md "wikilink")。

## 用途

### 攝影

在19世紀前葉，許多[感光金屬鹽已選為](../Page/感光.md "wikilink")[攝影過程的候選材料](../Page/攝影過程.md "wikilink")，硝酸鈾醯也在其中。以硝酸鈾醯為材料作的相紙因此被稱為uranium
prints，urbanities或更普遍的uranotypes。

蘇格蘭人[小查爾斯·柏奈特](../Page/小查爾斯·柏奈特.md "wikilink")（J. Charles
Burnett）是第一個發明鈾攝影過程的人，在1855年和1857年，用這種化合物作為感光材料。他在1858年寫了一篇文章，比較利用鈾鹽和氧化鐵攝影。讓鈾離子接受兩個[電子和降低至較低氧化態的鈾](../Page/電子.md "wikilink")(IV)的過程的關鍵是照射[紫外線](../Page/紫外線.md "wikilink")。Uranotypes在影像中，從一個較為中性的(neutral)紅棕色(brown
russet)到強bartolozzi紅，有很長的色調等級(tone
grade)。有殘留化合物的相紙上具有輕微的[放射性](../Page/放射性.md "wikilink")，這可以作為一個識別它們的非破壞性方法。其他幾個使用化合物的更複雜的攝影過程，出現並消失在第二個世紀的後半期，例如
Wothlytype、Mercuro-Uranotype
和Auro-Uranium。至少到19世紀未，有關鈾的論文被商業化的大量製造，而實質上有較高敏感性和實用性優點的[鹵化銀卻漸漸消失](../Page/鹵化銀.md "wikilink")。不過1930年到1950年之間，Kodak
Books仍然記載著使用醋酸鈾醯的鈾碳粉 (Kodak T-9)。現在仍有一些alternative process的攝影師包括藝術家Blake
Ferris & Robert Schramm繼續以uranotype創作。

### 生物檢測

與[醋酸鈾醯](../Page/醋酸鈾醯.md "wikilink")（uranyl
acetate）混合，在[電子顯微鏡檢測中可以用來作為](../Page/電子顯微鏡.md "wikilink")[病毒的](../Page/病毒.md "wikilink")[負染色](../Page/負染色.md "wikilink")；穩定組織樣本切片的[核酸和](../Page/核酸.md "wikilink")[細胞膜](../Page/細胞膜.md "wikilink")。

### 核子

硝酸鈾醯在1950年代時用於燃料[水溶液均勻反應堆](../Page/水溶液均勻反應堆.md "wikilink")（Aqueous
Homogeneous
Reactor）中。但是，它被證明在此應用中太過於具有[腐蝕性](../Page/腐蝕.md "wikilink")，因此中止了這個試驗。

硝酸鈾醯在[核廢料再處理中扮演重要的角色](../Page/核廢料再處理.md "wikilink")；它是將核燃料棒或[黃餅溶解於硝酸中所產生的鈾化合物](../Page/黃餅.md "wikilink")，
可以進一步分離和製備[六氟化鈾](../Page/六氟化鈾.md "wikilink")，再以[同位素分離](../Page/同位素分離.md "wikilink")（isotope
separation）製備[濃縮鈾](../Page/濃縮鈾.md "wikilink")。

## 健康和環境議題

硝酸鈾醯是一種氧化性和高毒性化合物，所以不能進入人體；它會引發嚴重的[腎功能不全和](../Page/腎功能不全.md "wikilink")[急性腎小管壞死](../Page/急性腎小管壞死.md "wikilink")（acute
tubular
necrosis），且是一種[淋巴球](../Page/淋巴球.md "wikilink")[有絲分裂原](../Page/有絲分裂原.md "wikilink")（mitogen）。[靶器官包括](../Page/靶器官.md "wikilink")[腎](../Page/腎.md "wikilink")、[肝](../Page/肝.md "wikilink")、[肺及](../Page/肺.md "wikilink")[腦部](../Page/腦.md "wikilink")。當加熱或遭受衝擊而接觸氧化性物質時，它也會引發劇烈的大火與爆炸。

1999年9月30日，日本發生[東海村JCO臨界意外](../Page/東海村JCO臨界意外.md "wikilink")，即在處理硝酸鈾醯的過程意外造成[臨界狀態發生](../Page/臨界狀態.md "wikilink")，近距離的三名工作人員中，有兩人因接受到致死量的[中子射线辐射而死亡](../Page/中子.md "wikilink")。

## 參考資料

## 外部連結

  - [URANIUM DAYS: Notes On Uranium
    Photography](https://web.archive.org/web/20071223084433/http://home.nyc.rr.com/waitaminute/Process.html)
  - [Chemical Database - Uranyl nitrate,
    solid](http://environmentalchemistry.com/yogi/chemicals/cn/Uranyl%A0nitrate,%A0solid.html)

[Category:铀酰化合物](../Category/铀酰化合物.md "wikilink")
[Category:硝酸盐](../Category/硝酸盐.md "wikilink")
[Category:核材料](../Category/核材料.md "wikilink")
[Category:电子显微术染色剂](../Category/电子显微术染色剂.md "wikilink")
[Category:照相药品](../Category/照相药品.md "wikilink")
[Category:氧化剂](../Category/氧化剂.md "wikilink")

1.  Roberts, D.E. and Modise, T.S. (2007). Laser removal of loose
    uranium compound contamination from metal surfaces. Applied Surface
    Science 253, 5258-5267.