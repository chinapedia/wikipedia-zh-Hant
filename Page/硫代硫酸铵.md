**硫代硫酸銨**，[分子式為](../Page/分子式.md "wikilink")，是[硫代硫酸鹽的一種](../Page/硫代硫酸鹽.md "wikilink")。

## 性质

一種有强烈吸湿性的白色斜方结晶，加熱至150℃會分解成[亚硫酸铵](../Page/亚硫酸铵.md "wikilink")、[硫黄](../Page/硫黄.md "wikilink")、[氨](../Page/氨.md "wikilink")、[硫化氢及](../Page/硫化氢.md "wikilink")[水](../Page/水.md "wikilink")。极易溶于水，不溶于[醇和](../Page/醇.md "wikilink")[醚](../Page/醚.md "wikilink")，微溶于[丙酮](../Page/丙酮.md "wikilink")。在空气中不稳定，在[氨气中稳定](../Page/氨气.md "wikilink")。

## 制法

[碳酸氢铵与](../Page/碳酸氢铵.md "wikilink")[二氧化硫和水作用产生](../Page/二氧化硫.md "wikilink")[亚硫酸铵](../Page/亚硫酸铵.md "wikilink")，经过滤除去杂质后再与[硫黄加热煮沸反应](../Page/硫黄.md "wikilink")，再经过滤、蒸发、冷却结晶和离心分离，得到硫代硫酸铵成品。

\[\rm \ 2NH_4HCO_3+SO_2+H_2O\rightarrow (NH_4)_2SO_3+2H_2O+2CO_2\uparrow\]

\[\rm \ (NH_4)_2SO_3+S\rightarrow (NH_4)_2S_2O_3\]

## 用途

感光工业中用作照相定影剂，用于替代[硫代硫酸钠](../Page/硫代硫酸钠.md "wikilink")，具有水洗时间短和银容易回收的优点，且较钠盐更易溶解卤化银的乳膜。在医药上用作杀菌剂、分析试剂。

[Category:銨鹽](../Category/銨鹽.md "wikilink")
[Category:硫代硫酸盐](../Category/硫代硫酸盐.md "wikilink")
[Category:化學肥料](../Category/化學肥料.md "wikilink")