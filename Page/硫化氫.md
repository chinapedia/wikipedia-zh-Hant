**硫化氫**是[无机化合物](../Page/无机化合物.md "wikilink")，[化學式為H](../Page/化學式.md "wikilink")<sub>2</sub>S。正常是無色、[易燃的](../Page/易燃.md "wikilink")[酸性](../Page/酸性.md "wikilink")[氣體](../Page/氣體.md "wikilink")，氢又从水里逸出。硫化氢是[急性劇毒](../Page/毒性.md "wikilink")，吸入少量高濃度硫化氫可於短時間內致命。低濃度的硫化氫對[眼](../Page/眼.md "wikilink")、[呼吸系統及](../Page/呼吸系統.md "wikilink")[中樞神經都有影響](../Page/中樞神經.md "wikilink")。

## 分佈

硫化氫自然存在於[原油](../Page/原油.md "wikilink")、[天然氣](../Page/天然氣.md "wikilink")、[火山氣體和](../Page/火山.md "wikilink")[溫泉之中](../Page/溫泉.md "wikilink")。少量的硫化氫在原油中、但天然氣可以包含高達90％硫化氫；也可以在[細菌於缺氧狀態下分解](../Page/細菌.md "wikilink")[有機物的過程中產生](../Page/有機物.md "wikilink")，或者是一部分的腐敗海藻中常見。人體釋放出的[屁含有極小量](../Page/屁.md "wikilink")（少於屁成分的1%）的硫化氢。

## 制备

实验室一般使用

  -
    \(\mathrm{FeS+2HCl \longrightarrow FeCl_2+H_2S}\)

来制取硫化氢。
也可以用

  -
    \(\mathrm{FeS+H_2SO_4 \longrightarrow FeSO_4+H_2S}\)

但还有一个更加简单的方法，[硫化铝](../Page/硫化铝.md "wikilink")（白色）的水解\[1\]：

  -
    \(\mathrm{6H_2O+Al_2S_3=3H_2S+2Al(OH)_3}\)

此方法是利用了活泼金属（[碱金属除外](../Page/碱金属.md "wikilink")）硫化物的[水解](../Page/水解.md "wikilink")。[硒化铝](../Page/硒化铝.md "wikilink")（灰色）和[碲化铝](../Page/碲化铝.md "wikilink")（暗灰色）也能和水迅速反应，生成[硒化氢和](../Page/硒化氢.md "wikilink")[碲化氢](../Page/碲化氢.md "wikilink")。

## 性质

### 物理性質

2015年，物理學者發現，硫化氫在溫度203K
（-70 °C）、極度高壓的環境下(至少150GPa，也就是約150萬標準大氣壓）會發生[超導](../Page/超導.md "wikilink")[相變](../Page/相變.md "wikilink")，是目前已知最高溫度的[超導體](../Page/超導體.md "wikilink")\[2\]。

### 化學性質

硫化氢比同族的水不稳定，加热高于700K时即发生分解。

硫化氫比[空氣稍重](../Page/空氣.md "wikilink")，硫化氫和空氣的混合物极易爆炸。硫化氫和氧氣燃燒會產生藍色火焰，形成[二氧化硫和](../Page/二氧化硫.md "wikilink")[水](../Page/水.md "wikilink")。在一般化学反应里，硫化氫是一种[還原劑](../Page/還原劑.md "wikilink")，如硫化氫可與二氧化硫反應形成單質硫和水。

  -
    \(\rm SO_2 + 2H_2S {=\!=\!=\!=} 3S + 2H_2O\)

硫化氫可微溶於水，形成弱酸，称为“**氢硫酸**”。其水溶液包含了氢硫酸根HS<sup>-</sup>（在摄氏18度、浓度为0.01-0.1摩/公升的溶液里，[p*K<sub>a</sub>*](../Page/酸度系数.md "wikilink")
= 6.9）和离子硫S<sup>2-</sup>（p*K<sub>a</sub>* =
11.96）。一开始清澈的氢硫酸置放一段时间后会变得混浊，这是因为氢硫酸会和溶解在水中的氧起缓慢的反应，产生不溶于水的单质硫。

硫化氢和金属离子接触会形成硫化金屬，硫化金屬往往是深暗色的。用於檢測硫化氫的[醋酸鉛紙和硫化氢的气体接触时会產生灰色的](../Page/醋酸鉛.md "wikilink")[硫化鉛](../Page/硫化鉛.md "wikilink")（II）。硫化金屬與強酸反應时则会释放出硫化氫。

此外，。與[醇反應则形成](../Page/醇.md "wikilink")[硫醇](../Page/硫醇.md "wikilink")。

  -
    \(\rm 6HNO_3 + H_2S {=\!=\!=\!=} SO_2\uparrow + 6NO_2\uparrow + 4H_2O\)

硫化氫是[酸性的](../Page/酸性.md "wikilink")，它與[鹼及一些](../Page/鹼.md "wikilink")[金屬](../Page/金屬.md "wikilink")（如[銀](../Page/銀.md "wikilink")）有化學反應。例如：硫化氫和[銀接觸後](../Page/銀.md "wikilink")，會產生黑褐色的[硫化銀](../Page/硫化銀.md "wikilink")，如果[氧气存在反应趋势更大](../Page/氧.md "wikilink")：

  -
    \(\rm H_2S + 2Ag {=\!=\!=\!=} Ag_2S + H_2 \uparrow\)

该反应能够进行的原因是硫化银的[生成自由能负值很大](../Page/生成自由能.md "wikilink")，增大了正方向反应的趋势。

## 安全

硫化氫是劇毒及易爆氣體。

### 相對濃度危險度

| 濃度（单位：[ppm](../Page/百万分率.md "wikilink")） | 反應       |
| ---------------------------------------- | -------- |
| 1,000 - 2,000（0.1 - 0.2%）                | 短時間內死亡   |
| 600                                      | |一小時內死亡  |
| 200 - 300                                | 一小時內急性中毒 |
| 100 - 200                                | 嗅覺麻痺     |
| 50 - 100                                 | 氣管刺激、結膜炎 |
| 0.41                                     | 嗅到難聞的氣味  |
| 0.00041                                  | 人開始嗅到臭味  |
|                                          |          |

## 参考文献

[Category:氢化合物](../Category/氢化合物.md "wikilink")
[Category:硫化物](../Category/硫化物.md "wikilink")
[Category:无机酸](../Category/无机酸.md "wikilink")
[Category:惡臭化學品](../Category/惡臭化學品.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.  《无机化学》丛书.第二卷.§2.5含Al-S、Al-Se键的化合物及碲化物.P461
2.