**硫化钠**是一个无机盐类，化学式为Na<sub>2</sub>S，通常以九[水合物Na](../Page/水合物.md "wikilink")<sub>2</sub>S·9H<sub>2</sub>O的形式存在。无水物和九水物都是无色可溶的固体，在水溶液中[水解呈强](../Page/水解.md "wikilink")[碱性](../Page/碱性.md "wikilink")。露置在空气中时，硫化钠会放出有臭鸡蛋气味的有毒[硫化氢气体](../Page/硫化氢.md "wikilink")。

## 性质

Na<sub>2</sub>S有反[萤石结构](../Page/萤石结构.md "wikilink")，Na<sup>+</sup>占据[CaF<sub>2</sub>中的F](../Page/氟化钙.md "wikilink")<sup>−</sup>，S<sup>2−</sup>占Ca<sup>2+</sup>。在溶液中碱性S<sup>2−</sup>强烈水解，分别生成HS<sup>−</sup>和H<sub>2</sub>S，而使溶液呈强碱性：

  -
    S<sup>2−</sup> + H<sub>2</sub>O ⇌ HS<sup>−</sup> + OH<sup>−</sup>
    HS<sup>−</sup> + H<sub>2</sub>O ⇌ H<sub>2</sub>S + OH<sup>−</sup>

硫化钠能溶解硫生成多硫化钠(Na<sub>2</sub>S<sub>x</sub>)。

## 生产

工业上由[焦炭](../Page/焦炭.md "wikilink")(高温转炉、1373K)还原[Na<sub>2</sub>SO<sub>4</sub>来制备Na](../Page/硫酸钠.md "wikilink")<sub>2</sub>S。\[1\]

  -
    Na<sub>2</sub>SO<sub>4</sub> + 4C → Na<sub>2</sub>S + 4CO

也可通过[氢气](../Page/氢气.md "wikilink")(沸腾炉、1373K)还原[Na<sub>2</sub>SO<sub>4</sub>来制备Na](../Page/硫酸钠.md "wikilink")<sub>2</sub>S。

  -
    Na<sub>2</sub>SO<sub>4</sub> + 4H<sub>2</sub> → Na<sub>2</sub>S +
    4H<sub>2</sub>O

实验室中可由[钠和](../Page/钠.md "wikilink")[硫在](../Page/硫.md "wikilink")[氨中或](../Page/氨.md "wikilink")[萘的存在下于](../Page/萘.md "wikilink")[四氢呋喃中反应来制备无水硫化钠](../Page/四氢呋喃.md "wikilink")。\[2\]

  -
    2Na + S → Na<sub>2</sub>S

## 安全

Na<sub>2</sub>S及其水合物有毒，使用时需注意。其水溶液呈强碱性，会腐蚀皮肤，且会与[酸迅速反应放出剧毒](../Page/酸.md "wikilink")[硫化氢气体](../Page/硫化氢.md "wikilink")。

## 参见

  - [硫酸盐制浆法](../Page/硫酸盐制浆法.md "wikilink")
  - [硫化染料](../Page/硫化染料.md "wikilink")

## 参考资料

## 外部链接

  - chemicalland21.com
    [硫化钠（英文）](http://www.chemicalland21.com/arokorhi/industrialchem/inorganic/SODIUM%20SULFIDE.htm)

[Category:鈉化合物](../Category/鈉化合物.md "wikilink")
[Category:硫化物](../Category/硫化物.md "wikilink")
[Category:潮解物质](../Category/潮解物质.md "wikilink")

1.  Holleman, A. F.; Wiberg, E. "Inorganic Chemistry" Academic Press:
    San Diego, 2001. ISBN 0-12-352651-5.
2.