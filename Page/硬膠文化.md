**硬膠**是一個常用於[香港](../Page/香港.md "wikilink")[網上社區的詞語](../Page/網上社區.md "wikilink")，原本是指「[聚苯乙烯](../Page/聚苯乙烯.md "wikilink")」，其後有人用此詞取其[諧音代替](../Page/諧音.md "wikilink")「戇」（意謂「傻屌」）這個[粗口](../Page/髒話.md "wikilink")，形容**形象差劣**的[事物](../Page/事物.md "wikilink")。

## 出處

「硬膠」早於[周星驰電影](../Page/周星驰.md "wikilink")《[整蠱專家](../Page/整蠱專家.md "wikilink")》已出現雛型。戲中一台詞「超級戇膠膠」可見，**「膠（gaau<sup>1</sup>）」**字是用作借代[粵語粗口的](../Page/粵語髒話.md "wikilink")**「」**字（音
/gau<sup>1</sup>/）。

現時
「硬膠」一詞的使用已遍布香港網絡社區。由於大部分討論區禁止網民使用粗言穢語，有些網民便把「戇㞗」諧音成「硬膠」，但也有可能被檢舉。網民並把發言古怪、發帖毫無根據、有違道理的會員定性為「硬膠」始祖。某程度上硬膠文化與「[無厘頭文化](../Page/無厘頭文化.md "wikilink")」近似。

## 發展

硬膠風氣始於[高登討論區](../Page/高登討論區.md "wikilink")。當時的高登[管理員仍為](../Page/網站管理員.md "wikilink")「[DrJim](../Page/wikia:evchk:DrJim.md "wikilink")」，但在管理權易手至「[Fevaworks](../Page/wikia:evchk:Fevaworks.md "wikilink")」（科域盈創有限公司）後，硬膠文化反更根深柢固。如高登的網頁標題曾為

許多高登用語都受「硬膠文化」影響，如「[硬膠之星](../Page/wikia:evchk:硬膠之星.md "wikilink")」；甚至把硬膠圖像化，以小丑圖示表示，此後「[小丑神](../Page/小丑神.md "wikilink")」更成為「硬膠」的同義詞。

## 影響

### 網絡媒體方面

在[香港高登討論區內](../Page/香港高登討論區.md "wikilink")，「硬膠」2字的使用次數非常頻密，。[高登內很多網民往往在自己的](../Page/高登.md "wikilink")[網名上加入](../Page/網名.md "wikilink")“膠”一字。

### 傳統媒體方面

由於不少有寫[網誌習慣的報章專欄作者如](../Page/網誌.md "wikilink")[林忌](../Page/林忌.md "wikilink")、[黃世澤等常在其文章使用](../Page/黃世澤.md "wikilink")「硬膠」2字，因此「硬膠」除影響部分網民的用字外，其影響力亦逐漸延伸至傳統媒體。例如在[香港](../Page/香港.md "wikilink")《[蘋果日報](../Page/蘋果日報.md "wikilink")》財經版撰寫專欄的作者[孫柏文在其專欄文章](../Page/孫柏文.md "wikilink")《金手指》不時用到「膠」或「硬膠」等字眼來批判某些人或事的光怪陸離作風\[1\]。此外，《蘋果日報》亦不時出現「[膠系術語](../Page/wikia:evchk:膠系術語.md "wikilink")」如「[派膠](../Page/wikia:evchk:派膠.md "wikilink")」等\[2\]。

而曾為長期高登會員的[林忌](../Page/林忌.md "wikilink")，
於2007年發佈改編歌曲《[福佳始終有你](../Page/wikia:evchk:福佳始終有你.md "wikilink")》時，把歌詞中的「膠」字定位為「荒謬」之異體，後來在他出版的著作《[潮童不宜](../Page/wikia:evchk:潮童不宜.md "wikilink")》中，內容更是「膠」字橫行，甚似高登政治風氣盛行時之討論。

## 生肖

  - [牛年4月20日至12月31日](../Page/牛#其他.md "wikilink")

## 產生

  - [李大釗案](../Page/李大釗.md "wikilink")
  - [李倩姮案](../Page/柴灣118號城巴謀殺案.md "wikilink")
  - [潘曉穎案](../Page/潘曉穎案.md "wikilink")
  - [丁有康案](../Page/義不容情#丁家.md "wikilink")

## 導致

  - [拋棄家庭](../Page/拋棄家庭.md "wikilink")
  - [壞緣分](../Page/壞緣分.md "wikilink")

## 相關

  - [粵語髒話](../Page/粵語髒話.md "wikilink")
  - [薄熙來](../Page/薄熙來.md "wikilink")
  - [梁立人](../Page/梁立人.md "wikilink")
  - [李力持](../Page/李力持.md "wikilink")
  - [梁志健](../Page/梁志健.md "wikilink")
  - [梁子穎](../Page/梁子穎.md "wikilink")
  - [吳綺莉](../Page/吳綺莉.md "wikilink")

## 參見

  - [譚姓](../Page/譚姓.md "wikilink")\[3\]
  - [小丑神](../Page/小丑神.md "wikilink")
  - [智障](../Page/智障.md "wikilink")
  - [假包容](../Page/包容#假包容.md "wikilink")

## 參考

## 外部連結

  - [《網絡新詞
    唔識hihi886》，《蘋果日報》，2006年9月11日。](https://web.archive.org/web/20080110124754/http://www1.appledaily.atnext.com/template/apple_sub/art_main.cfm?iss_id=20060911&sec_id=38163&subsec_id=6038907&art_id=6300986)

  -
[粗](../Category/粵語.md "wikilink") [髒話](../Category/髒話.md "wikilink")
[粗](../Category/香港網絡文化.md "wikilink")
[粗](../Category/互联网用语.md "wikilink")

1.  [孫柏文, 「金手指: 第壹膠黨『[公民黨](../Page/公民黨.md "wikilink")』」, 《蘋果日報》,
    11/21/2007](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20071121&sec_id=15307&subsec_id=15320&art_id=10444835&cat_id=5885349&coln_id=5673918)

2.  [尹思哲, 「案內人隨筆: 環保成本」,
    《蘋果日報》, 11/21/2007](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20071121&sec_id=15307&subsec_id=15320&art_id=10444837&cat_id=6271926&coln_id=6339611)

3.  為[香港網絡大典的](../Page/香港網絡大典.md "wikilink")**真心膠**分類中唯一有幾個的姓氏。