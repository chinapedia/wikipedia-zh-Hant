**硼氢化钠**是一种[无机化合物](../Page/无机化合物.md "wikilink")，分子式NaBH<sub>4</sub>。硼氢化钠为白色粉末，容易吸水[潮解](../Page/潮解.md "wikilink")，可溶于水和低级[醇](../Page/醇.md "wikilink")，在室温下与[甲醇迅速反应生成氢气](../Page/甲醇.md "wikilink")。在[无机合成和](../Page/无机合成.md "wikilink")[有机合成中硼氢化钠常用做](../Page/有机合成.md "wikilink")[还原剂](../Page/还原剂.md "wikilink")。通常情况下，硼氢化钠无法还原[酯](../Page/酯.md "wikilink")，[酰胺](../Page/酰胺.md "wikilink")，[羧酸及](../Page/羧酸.md "wikilink")[腈类化合物](../Page/腈.md "wikilink")，但当酯的羰基α位有[杂原子存在时例外](../Page/杂原子.md "wikilink")，可以将酯还原。在与硼氢化钠接触后可能会有有[咽喉痛](../Page/咽喉痛.md "wikilink")、[咳嗽](../Page/咳嗽.md "wikilink")、[呼吸急促](../Page/呼吸急促.md "wikilink")、头痛、[腹痛](../Page/腹痛.md "wikilink")、[腹泻](../Page/腹泻.md "wikilink")、[眩晕](../Page/眩暈_\(醫學\).md "wikilink")、眼结膜充血、[疼痛等不良症状](../Page/疼痛.md "wikilink")。应储藏在阴凉、干燥的仓库中。防潮、防震，不可与无机酸共同储藏或运输，远离热源及易燃物品。

## 发现

硼氢化钠是由 H. C. Brown 和他的导师Schlesinger
于1942年在芝加哥大学发现的。当时的目的是为了研究[硼烷和](../Page/硼烷.md "wikilink")[一氧化碳](../Page/一氧化碳.md "wikilink")[络合物的性质](../Page/络合物.md "wikilink")，但却发现了硼烷对有机[羰基化合物的还原能力](../Page/羰基化合物.md "wikilink")。由于当时硼烷属于稀有物质，因此并没有引起有机化学家的重视。硼烷化学的发展得益于[第二次世界大战](../Page/第二次世界大战.md "wikilink")，当时美国国防部需要寻找一种分子量尽量小的挥发性[铀化合物用于裂变材料铀](../Page/铀.md "wikilink")235的富集。[硼氢化铀U](../Page/硼氢化铀.md "wikilink")(BH<sub>4</sub>)<sub>4</sub>符合这个要求。该化合物的合成需要用到[氢化锂](../Page/氢化锂.md "wikilink")，然而氢化锂的供应很少，于是便宜的[氢化钠便被用来作原料](../Page/氢化钠.md "wikilink")，而硼氢化钠就在这个过程中被发现。后来，因为六氟化铀的处理工艺问题得到解决，国防部便放弃了通过硼氢化铀来富集铀235的计划，而Brown
的研究课题就变成了如何方便地制备硼氢化钠。Army Signal
Corps公司对这个新化合物的野外就地制备大量氢气的用途产生了兴趣。在他们的资助下，开展了相关的工业化研究，产生了后来工业生产硼氢化钠的工艺：

  -
    4 NaH + B(OCH<sub>3</sub>)<sub>3</sub>(g) → NaBH<sub>4</sub> + 3
    NaOCH<sub>3</sub>

产物是两种固体。用[醚类溶剂](../Page/醚.md "wikilink")[重结晶得到纯品硼氢化钠](../Page/重结晶.md "wikilink")。

## 制备

除了上文提到的那种方法，现在还有其他的制备方法：

  - Na<sub>2</sub>B<sub>4</sub>O<sub>7</sub> + 16 Na + 8 H<sub>2</sub> +
    7 SiO<sub>2</sub> → 4 NaBH<sub>4</sub> + 7
    Na<sub>2</sub>SiO<sub>3</sub>\[1\]

<!-- end list -->

  -
    这个反应在723-773K、3-5个氢气压下即可完成反应。

<!-- end list -->

  - NaBO<sub>2</sub> + 4 Na + 2 H<sub>2</sub> + 2 SiO<sub>2</sub> →
    NaBH<sub>4</sub> + 2 Na<sub>2</sub>SiO<sub>3</sub>\[2\]

<!-- end list -->

  -
    此法比上式更节省原料。

## 应用

硼氢化钠给有机化学家们提供了一种非常便利温和的还原[醛](../Page/醛.md "wikilink")[酮类物质的手段](../Page/酮.md "wikilink")。在此之前，通常要用金属/醇的办法来还原羰基化合物，而硼氢化钠可以在非常温和的条件下实现醛酮[羰基的还原](../Page/羰基.md "wikilink")，生成一級醇、二級醇。还原步骤是先把底物溶于溶剂，一般是[甲醇或者](../Page/甲醇.md "wikilink")[乙醇](../Page/乙醇.md "wikilink")，然后用冰浴冷却，将硼氢化钠粉末加入混合物搅拌至反应完全即可。反应过程可以用[薄层层析监测](../Page/薄层层析.md "wikilink")。如果溶剂不是醇，那么需要另加[甲醇或者](../Page/甲醇.md "wikilink")[乙醇一同反应](../Page/乙醇.md "wikilink")。硼氢化钠是一种中等强度的还原剂，所以在反应中表现出良好的化学选择性，只还原活泼的醛酮羰基，而不与酯、酰胺作用。

硼氢化钠亦可用作[醛类](../Page/醛.md "wikilink")、[酮类和](../Page/酮.md "wikilink")[酰氯类的](../Page/酰氯.md "wikilink")[还原剂](../Page/还原剂.md "wikilink")，制造[硼氢化钾的中间体](../Page/硼氢化钾.md "wikilink")，制造[乙硼烷和其他](../Page/乙硼烷.md "wikilink")[高能燃料的原料](../Page/高能燃料.md "wikilink")，用作塑料工业的发泡剂，[造纸工业含汞污水的处理剂](../Page/造紙業.md "wikilink")、造纸[漂白剂](../Page/漂白劑.md "wikilink")，以及医药工业制造双氢链霉素的[氢化剂](../Page/氢化.md "wikilink")。

## 參考文獻

[Category:还原剂](../Category/还原剂.md "wikilink")
[Category:钠化合物](../Category/钠化合物.md "wikilink")
[Na](../Category/硼氢化物.md "wikilink")
[Category:易制爆化学品](../Category/易制爆化学品.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.
2.  《硼氢化合物》.郑学家 主编.化学工业出版社. ISBN 978-7-122-11506-5.第三章 硼氢化钠