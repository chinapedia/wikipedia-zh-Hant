**碌曲县**（）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省](../Page/甘肃省.md "wikilink")[甘南藏族自治州下属的一个](../Page/甘南藏族自治州.md "wikilink")[县](../Page/县级行政区.md "wikilink")。面积5189平方公里，2004年人口3万。县政府驻玛艾镇。碌曲是[洮河的藏语名称](../Page/洮河.md "wikilink")，意为泉水河。\[1\]\[2\]

## 历史

碌曲古属“羌中”。[前凉时在今晒银滩至尕海措宁一带置漒川护军](../Page/前凉.md "wikilink")。[隋置](../Page/隋朝.md "wikilink")[洮源县](../Page/洮源县.md "wikilink")、[洮阳县](../Page/洮阳县.md "wikilink")；[唐初置](../Page/唐朝.md "wikilink")[西沧州](../Page/西沧州.md "wikilink")，715年陷于[吐蕃](../Page/吐蕃.md "wikilink")；[宋为](../Page/宋朝.md "wikilink")[唃厮啰所属](../Page/唃厮啰.md "wikilink")；[元为](../Page/元朝.md "wikilink")[洮州辖域](../Page/洮州.md "wikilink")；[明为洮州卫属地](../Page/明朝.md "wikilink")；民国属[临潭县](../Page/临潭县.md "wikilink")。

1953年将临潭县所辖的西仓区（西仓、双岔、郎木寺3大部落）析置碌曲县人民委员会，1955年6月18日正式成立碌曲县，驻西仓。1958年12月20日撤销碌曲、玛曲2县设立[洮江县](../Page/洮江县.md "wikilink")，驻尕海。1961年12月15日恢复碌曲县，以合并于洮江县的原碌曲县行政区域和[德乌德市的部分地区为碌曲县行政区域](../Page/德乌德市.md "wikilink")。\[3\]\[4\]

## 地理

### 区位

碌曲县位于[甘南藏族自治州西部](../Page/甘南藏族自治州.md "wikilink")，北连[夏河](../Page/夏河.md "wikilink")，东临[卓尼](../Page/卓尼.md "wikilink")，西南为[玛曲](../Page/玛曲.md "wikilink")，西接[青海省](../Page/青海省.md "wikilink")[河南县](../Page/河南县.md "wikilink")，南邻[四川省](../Page/四川省.md "wikilink")[若尔盖县](../Page/若尔盖县.md "wikilink")。县城距[合作市](../Page/合作市.md "wikilink")78公里。\[5\]

全县东西长126公里，南北宽90公里，总面积5189平方公里，其中，草场面积约591.7万亩，耕地面积4.1万亩，森林1.3万亩。\[6\]

### 地形

全县海拔在3000～4000米之间，平均海拔3500米。西部是高原山地，东部洮河两岸山岭陡峭，河谷是主要的农业种植区。\[7\]

### 水系

碌曲县地处[长江流域与](../Page/长江.md "wikilink")[黄河流域的](../Page/黄河.md "wikilink")[分水岭](../Page/分水岭.md "wikilink")，有[洮河](../Page/洮河.md "wikilink")、[白龙江两大水系](../Page/白龙江.md "wikilink")。洮河发源于县西南[西倾山及其支脉](../Page/西倾山.md "wikilink")[李恰如山南麓的](../Page/李恰如山.md "wikilink")[代富桑草原](../Page/代富桑草原.md "wikilink")，由南向北，支流遍及全县。白龙江发源于[郎木寺](../Page/郎木寺镇.md "wikilink")，由西向东。\[8\]

## 行政区划

下辖5个[镇](../Page/镇.md "wikilink")、2个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 交通

  - [213国道](../Page/213国道.md "wikilink")

## 人口

全县总人口为30491人，其中[藏族占](../Page/藏族.md "wikilink")82％，并有[汉](../Page/汉族.md "wikilink")、[回等民族聚居](../Page/回族.md "wikilink")。\[9\]

## 经济

碌曲县经济以畜牧、旅游、水电、矿产为主，其中畜牧业为支柱产业。\[10\]

## 风景名胜

  - 尕海-则岔国家级自然保护区
      - [尕海](../Page/甘肃尕海.md "wikilink")
      - [则岔石林](../Page/则岔石林.md "wikilink")
  - [朗木寺](../Page/朗木寺.md "wikilink")

## 参考文献

## 外部链接

  - <https://web.archive.org/web/20101111235933/http://luqu.gscn.com.cn/>

[碌曲县](../Category/碌曲县.md "wikilink")
[县](../Category/甘南州县市.md "wikilink")
[甘南州](../Category/甘肃省县份.md "wikilink")

1.  [碌曲县概况](http://www.gn.gansu.gov.cn/html/2008-10/2599.html)

2.  [碌曲县情简介](http://luqu.gscn.com.cn/html/zwgl/09_08_32_0792608570940707_0982409083299241_423.html)


3.
4.  [甘肃省甘南州碌曲县](http://kjfoods.com/86/news.asp?news_id=2531)

5.
6.
7.
8.
9.
10.