**碳酸二甲酯**（**DMC**）是一个[有机化合物](../Page/有机化合物.md "wikilink")，可看作[碳酸的二](../Page/碳酸.md "wikilink")[甲基](../Page/甲基.md "wikilink")[酯](../Page/酯.md "wikilink")。它是可燃的澄清液体，沸点90°C，不溶于水，可用作[甲基化试剂](../Page/甲基化.md "wikilink")。相比其他甲基化试剂，如[碘甲烷和](../Page/碘甲烷.md "wikilink")[硫酸二甲酯](../Page/硫酸二甲酯.md "wikilink")，碳酸二甲酯毒性较小，而且可被[生物降解](../Page/生物降解.md "wikilink")。\[1\]

以前以[光气为原料制取碳酸二甲酯的方法已不常用](../Page/光气.md "wikilink")，取而代之的是用[甲醇在](../Page/甲醇.md "wikilink")[氧气存在下的催化氧化](../Page/氧气.md "wikilink")[羰基化反应制得](../Page/羰基化.md "wikilink")，较之以前的方法更加环保。\[2\]

碳酸二甲酯可对[苯胺](../Page/苯胺.md "wikilink")、[酚和](../Page/酚.md "wikilink")[羧酸进行甲基化](../Page/羧酸.md "wikilink")，但很多反应都需高压。\[3\]可以在回流DMC时加入[DBU](../Page/DBU.md "wikilink")，以催化碳酸二甲酯甲基化羧酸的反应：\[4\]

  -
    [DMC_Methylation.png](https://zh.wikipedia.org/wiki/File:DMC_Methylation.png "fig:DMC_Methylation.png")

## 参考资料

<div class="references-small">

<references/>

</div>

[Category:甲基化试剂](../Category/甲基化试剂.md "wikilink")
[Category:碳酸酯](../Category/碳酸酯.md "wikilink")
[Category:甲酯](../Category/甲酯.md "wikilink")

1.

2.
3.

4.