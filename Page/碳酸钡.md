**碳酸钡**（化学式：[Ba](../Page/钡.md "wikilink")[CO<sub>3</sub>](../Page/碳酸根.md "wikilink")），由于其较容易从自然矿物中获取，且有毒性，故有被称作**毒重石**。碳酸钡是一种不溶于[水的](../Page/水.md "wikilink")[白色](../Page/白色.md "wikilink")[沉淀](../Page/沉淀.md "wikilink")，是一种常见的[无机盐](../Page/无机盐.md "wikilink")。

## 形态

通常，碳酸钡有α、β、γ三种结晶形态，而其工业品则为白色粉末。

γ型在811°C时会转化为β型，β型在982°C时转换为α型，在9.09M[Pa时](../Page/帕斯卡.md "wikilink")，α型的熔点为1740°C，而在[常压加热到](../Page/标准大气压.md "wikilink")1450°C时，碳酸钡就会分解而成[氧化钡和](../Page/氧化钡.md "wikilink")[二氧化碳](../Page/二氧化碳.md "wikilink")。

## 溶解性

碳酸钡在水中的溶解度仅为2×10<sup>-3</sup>g，几乎不溶于[酒精](../Page/酒精.md "wikilink")，但可与[酸和](../Page/酸.md "wikilink")[氯化铵溶液反应而溶解](../Page/氯化铵.md "wikilink")。

## 化学反应

  - 分解反应：碳酸钡会在高温下分解，反应式：

<!-- end list -->

  -
    BaCO<sub>3</sub> → BaO + CO<sub>2</sub>↑

<!-- end list -->

  - 与酸反应：
      - 在遇到酸时的一般反应式：

<!-- end list -->

  -
    BaCO<sub>3</sub> + 2H<sup>+</sup> → Ba<sup>2+</sup> + H<sub>2</sub>O
    + CO<sub>2</sub>↑

<!-- end list -->

  -   - 与稀硫酸反应时会生成不可溶于酸的[硫酸钡白色沉淀](../Page/硫酸钡.md "wikilink")：

<!-- end list -->

  -
    BaCO<sub>3</sub> + H<sub>2</sub>SO<sub>4</sub> → BaSO<sub>4</sub> +
    H<sub>2</sub>O + CO<sub>2</sub>↑

## 毒性

碳酸钡是一种剧毒品，会引起急性和慢性中毒。因为钡是重金属，碳酸钡能与胃液中的[盐酸反应](../Page/盐酸.md "wikilink")，释放出钡离子，引起中毒反应。

### 慢性中毒

碳酸钡会蓄积在[骨骼上](../Page/骨骼.md "wikilink")，引起[骨髓造](../Page/骨髓.md "wikilink")[白细胞组织增生](../Page/白细胞.md "wikilink")，从而发生慢性中毒。

### 急性中毒

当发生严重的急性中毒时，会出现急性[胃](../Page/胃.md "wikilink")[肠病变](../Page/肠.md "wikilink")、[肌肉](../Page/肌肉.md "wikilink")[腱反射消失](../Page/腱.md "wikilink")、[痉挛](../Page/痉挛.md "wikilink")、肌肉麻痹等症状。

毒性：

  - 口服
      - 大鼠 LD50：418mg/kg
      - 小鼠 LD50：200mg/kg

#### 紧急治疗

对于碳酸钡急性中毒的患者，通常采取[洗胃](../Page/洗胃.md "wikilink")、[灌肠](../Page/灌肠.md "wikilink")、服用[催吐剂等措施使碳酸钡排出体外](../Page/催吐剂.md "wikilink")。

同时也可以通过服用[硫酸钾与碳酸钡反应](../Page/硫酸钾.md "wikilink")，能使有毒的钡变为不溶性[硫酸钡沉淀](../Page/硫酸钡.md "wikilink")，减轻毒性。之所以使用硫酸钾，是因为碳酸钡中毒可能会导致[低血钾症](../Page/低血钾症.md "wikilink")。*参见[钡](../Page/钡.md "wikilink")*

### 职业标准

  - [TLV-TWA](../Page/TLV-TWA.md "wikilink") 0.5mg/m<sup>3</sup>
  - [STEL](../Page/STEL.md "wikilink") 0.5mg/m<sup>3</sup>

## 用途

  - 用于[钢铁](../Page/钢铁.md "wikilink")、[焊条](../Page/焊条.md "wikilink")、[玻璃](../Page/玻璃.md "wikilink")、[颜料](../Page/颜料.md "wikilink")、[油漆](../Page/油漆.md "wikilink")、[涂料](../Page/涂料.md "wikilink")、[烟火](../Page/烟火.md "wikilink")、[搪瓷](../Page/搪瓷.md "wikilink")、电子等工业
  - 用于生成各种[钡盐](../Page/钡.md "wikilink")
  - 可用於糖的精煉
  - 製造釉料及合成大理石的顏料\[1\]

## 参考资料

## 外部链接

  - [化工产品物性辞典](http://www.chem.com.cn/xxzx/dictionary/chem_dictionary.asp?cpdh=010300607&bz=1)
  - [【危险货品】有毒物品](https://web.archive.org/web/20070310195053/http://119.sd.cn/InfoCenter/Pagelet/PageShow.ASP?CnnStr=chem&HostTable=chem&Where=RID:1078)

[Category:钡化合物](../Category/钡化合物.md "wikilink")
[Category:碳酸盐](../Category/碳酸盐.md "wikilink")
[Category:烟火着色剂](../Category/烟火着色剂.md "wikilink")

1.  [藝術與建築索引典—毒重石](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300266846)
    於2010年12月3日查閱