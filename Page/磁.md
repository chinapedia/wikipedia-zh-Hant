[Lodestone_attracting_nails.png](https://zh.wikipedia.org/wiki/File:Lodestone_attracting_nails.png "fig:Lodestone_attracting_nails.png")
**磁**是一种物理现象，**磁学**是研究磁现象的一个[物理学分支](../Page/物理学.md "wikilink")，**磁性**是[物質響應](../Page/物質.md "wikilink")[磁場作用的性质](../Page/磁場.md "wikilink")。磁性表现在[順磁性物質或](../Page/順磁性.md "wikilink")[铁磁性物質](../Page/铁磁性.md "wikilink")（如铁钉）會趨向於朝著磁場較強的區域移動，即被磁場吸引；[反磁性物質則會趨向於朝著磁場較弱的區域移動](../Page/反磁性.md "wikilink")，即被磁場排斥；還有一些物質（如[自旋玻璃](../Page/自旋玻璃.md "wikilink")、[反鐵磁性等](../Page/反鐵磁性.md "wikilink")）會與磁場有更複雜的關係。

依照溫度、[壓強等參數的不同](../Page/壓強.md "wikilink")，物質會顯示出不同的磁性。表现出[磁性的物质通称为](../Page/铁磁性.md "wikilink")[磁体](../Page/磁体.md "wikilink")，原来不具有磁性的物质获得磁性的过程称为[磁化](../Page/磁化.md "wikilink")，反之称为[退磁](../Page/退磁.md "wikilink")。磁鐵本身會產生[磁場](../Page/磁場.md "wikilink")，但[本质上](../Page/量子力学.md "wikilink")[磁场是由](../Page/磁场.md "wikilink")[电荷运动](../Page/电荷.md "wikilink")[產生](../Page/电磁感应.md "wikilink")，如磁铁内部未配對[电子的](../Page/电子.md "wikilink")[自旋](../Page/自旋.md "wikilink")，会产生磁场，当这些磁场的方向一致时，宏观上就表现为磁性\[1\]\[2\]。

## 歷史

[Thales.jpg](https://zh.wikipedia.org/wiki/File:Thales.jpg "fig:Thales.jpg")
公元前六世紀希臘哲學家[泰勒斯是最早描述磁石的磁性的幾位學者之一](../Page/泰勒斯.md "wikilink")。\[3\]古希臘人認為，泰勒斯最先發現磁石吸引鐵物質與其它磁石的性質。\[4\]磁的英文術語「」傳說是因最早在希臘發現磁石的地方[麥格尼西亞](../Page/麥格尼西亞.md "wikilink")（）而命名。\[5\]\[6\]

在中國，磁性最早出現於一本公元前4世紀編寫的書《[鬼谷子](../Page/鬼谷子.md "wikilink")》：「其察言也，不失若磁石之取鍼，舌之取燔骨」。\[7\]察析這人的言詞話語，就好像用磁石吸取鐵針，又好像用舌尖探取炙肉中的骨頭，絕對不能有所差失。\[8\]公元一世紀，即[東汉時期](../Page/東汉.md "wikilink")，[王充在古籍](../Page/王充.md "wikilink")《[論衡](../Page/論衡.md "wikilink")》中亦有關於磁性的記載：「頓牟掇芥，磁石引針」，頓牟即琥珀，摩擦後的[琥珀能吸引草芥](../Page/琥珀.md "wikilink")，而磁石能吸引鐵針。\[9\]

[Shen_Kua.JPG](https://zh.wikipedia.org/wiki/File:Shen_Kua.JPG "fig:Shen_Kua.JPG")
公元1086-1093年，北宋科學家[沈括在著作](../Page/沈括.md "wikilink")《[夢溪筆談](../Page/夢溪筆談.md "wikilink")》裏清楚地描述關於指南針的製作與使用方法。由於這方法引入了[天文學的](../Page/天文學.md "wikilink")[真北的概念](../Page/真北.md "wikilink")，航行的準確度得以大大改善。\[10\]

公元1119年，[北宋](../Page/北宋.md "wikilink")[朱彧在著作](../Page/朱彧.md "wikilink")《[萍洲可谈](../Page/萍洲可谈.md "wikilink")》裏詳細地記述：「舟师识地理，夜则观星，昼则观日，阴晦观指南针」。這是航海史最早的關於使用指南針航海的紀錄\[11\]。

美國天文學者約翰·卡森（John
Carlson）在中美洲[奧爾梅克文明發現的](../Page/奧爾梅克文明.md "wikilink")[赤鐵礦古物](../Page/赤鐵礦.md "wikilink")，卡森認為，早於公元前1000年，奧爾梅克人有可能已經發明與使用地磁磁石羅盤。\[12\]\[13\]假若這建議為正確，則這比中國的類似發現早了1000年以上。卡森推測奧爾梅克可能使用這類古物於占星或推卜用途，或找到寺廟、住家或墳墓的取向。

在歐洲，1187年，最先寫出羅盤的製作與導航用途。1269年，法國學者寫成《磁石書》（）。這是第一本尚存的描述磁石性質的著作。\[14\]德馬立克仔細標明了鐵針在塊型磁石附近各個位置的[取向](../Page/取向.md "wikilink")，從這些記號，又描繪出很多條磁場線。他發現這些磁場線相會於磁石的相反兩端位置，就好像地球的[經線相會於](../Page/經線.md "wikilink")[南極與](../Page/南極.md "wikilink")[北極](../Page/北極.md "wikilink")。因此，他稱這兩位置為「磁極」\[15\]。1282年，[葉門物理學者](../Page/葉門.md "wikilink")論述磁石與[乾羅盤的性質](../Page/羅盤.md "wikilink")。\[16\]

1600年，英國醫生[威廉·吉爾伯特發表了著作](../Page/威廉·吉爾伯特.md "wikilink")《論磁石》（）。在這篇著作裏，他設計出一種模型，稱為「小地球」。他用這模型來描述他的種種實驗。從這些實驗，他推論地球具有磁性，因此，[指南針的](../Page/指南針.md "wikilink")[磁北極會指向北方](../Page/磁北極.md "wikilink")（在此之前，很多學者認為是[北極星或位於](../Page/北極星.md "wikilink")[北極的一個巨大磁島吸引著磁北極](../Page/北極.md "wikilink")）。

1820年，由於[哥本哈根大學物理教授](../Page/哥本哈根大學.md "wikilink")[漢斯·奧斯特的貢獻](../Page/漢斯·奧斯特.md "wikilink")，物理學者開始瞭解電與磁之間的關係。奧斯特發現[載流導線的](../Page/載流導線.md "wikilink")[電流會施加](../Page/電流.md "wikilink")[作用力於磁針](../Page/作用力.md "wikilink")，使磁針偏轉指向。這跨時代的實驗知名為「奧斯特實驗」。稍後，在這新聞抵達[法國科學院僅僅一周之後](../Page/法國科學院.md "wikilink")，[安德烈-瑪麗·安培成功地做實驗顯示](../Page/安德烈-瑪麗·安培.md "wikilink")，假若所載電流的流向相同，則兩條平行的載流導線會互相吸引；否則，假若流向相反，則會互相排斥。緊接著，法國物理學家[讓-巴蒂斯特·必歐和](../Page/讓-巴蒂斯特·必歐.md "wikilink")[菲利克斯·沙伐於](../Page/菲利克斯·沙伐.md "wikilink")10月共同發表了[必歐-沙伐定律](../Page/必歐-沙伐定律.md "wikilink")；這定律能夠正確地計算出在載流導線四周的磁場。

1825年，安培又發表了[安培定律](../Page/安培定律.md "wikilink")。這定律也能夠描述載流導線產生的磁場。更重要的，這定律幫助建立整個電磁理論的基礎。於1831年，[麥可·法拉第發現](../Page/麥可·法拉第.md "wikilink")，時變磁場會生成電場。這實驗結果展示出電與磁之間更密切的關係。他又發明了[发电机和](../Page/发电机.md "wikilink")[电动机](../Page/电动机.md "wikilink")。

從1861年到1865之間，[詹姆斯·馬克士威將先前這些雜亂無章的方程式加以整合](../Page/詹姆斯·馬克士威.md "wikilink")，給出了馬克士威方程組。至此，馬克士威統一了電學、磁學、光學理論。

1905年，[阿爾伯特·愛因斯坦在他的論文裡表明](../Page/阿爾伯特·愛因斯坦.md "wikilink")，電場和磁場是處於不同參考系的觀察者所觀察到的同樣現象。詳盡細節，請參閱條目[移動中的磁鐵與導體問題](../Page/移動中的磁鐵與導體問題.md "wikilink")。

1888年，[美國機械工程師](../Page/美國.md "wikilink")發表文章於雜誌
《電世界》（），首次闡述了磁性記錄儀器。不久之後，於1898年，第一個磁性記錄儀器真正誕生——這是現代[硬碟和其他種磁存儲技術的鼻祖](../Page/硬碟.md "wikilink")。\[17\]

1895年，[皮埃爾·居里在他的博士論文裡發表了關於磁性物質的研究](../Page/皮埃爾·居里.md "wikilink")。他發現了溫度對於順磁性的效應，今稱為。他又發現鐵磁性物質的[相變會顯示出](../Page/相變.md "wikilink")[臨界溫度](../Page/臨界溫度.md "wikilink")，即鐵磁性物質失去其鐵磁性的溫度，今稱為[居里溫度](../Page/居里溫度.md "wikilink")。

从20世纪至今，磁存储技术迅速发展，[巨磁阻现象和](../Page/巨磁阻.md "wikilink")[垂直写入技术仍是目前磁学领域的最尖端课题](../Page/垂直写入技术.md "wikilink")。

## 磁源

追根究柢，磁有兩種源頭：

1.  電流是一群移動的電荷。電流或移動的電荷，會在周圍產生磁場。
2.  很多種[粒子具有內秉的磁矩](../Page/次原子粒子.md "wikilink")──。這些磁矩，會在四週產生磁場。

對於磁性物質，磁極化的主要源頭是以[原子核為中心的電子軌域運動](../Page/原子核.md "wikilink")，和電子的內秉磁矩（請參閱條目[電子磁偶極矩](../Page/電子磁偶極矩.md "wikilink")）。與這些源頭相比，[核子的](../Page/核子.md "wikilink")顯得很微弱，強度是電子磁矩的幾千分之一。當做一般運算時，可以忽略核子磁矩。但是，核子磁矩在某些領域很有用途，例如，[核磁共振](../Page/核磁共振.md "wikilink")、[核磁共振成像](../Page/核磁共振成像.md "wikilink")。

通常而言，在物質內部超多數量的電子，它們各自的磁矩（軌域磁矩和內禀磁矩）會互相抵銷。這是因為兩種機制：一種機制是遵守[包立不相容原理的後果](../Page/包立不相容原理.md "wikilink")，匹配成對的電子都具有彼此方向相反的內秉磁矩；另一種機制是電子趨向於填滿[次殼層](../Page/電子殼層.md "wikilink")，達成淨軌域運動為零。對於這兩種機制，電子排列會使得每一個電子的磁矩被完全抵銷。當然，不是每一種物質都具有這麼理想的屬性，但甚至當電子組態仍有尚未配對的電子或尚未填滿的次殼層，通常，在物質內部的各個電子，會貢獻出隨機方向的磁矩，結果是這些物質不具有磁性。

但是，有時候，或許是自發性效應，或許是由於外磁場的施加，物質內的電子磁矩會整齊地排列起來。由於這動作，很可能會造成強烈的淨磁矩與淨磁場。

由於前面表述的原因，物質的磁行為與其結構有關，特別是其[電子組態](../Page/電子組態.md "wikilink")。在高溫狀況，[隨機的](../Page/隨機.md "wikilink")[熱運動會使得電子磁矩的整齊排列更加困難](../Page/分子運動論.md "wikilink")。

## 磁学

磁学和[电学有着直接的联系](../Page/电学.md "wikilink")，合并称为[电磁学](../Page/电磁学.md "wikilink")。[电磁学是研究](../Page/电磁学.md "wikilink")[電与磁彼此之間相互關係的一門學科](../Page/電.md "wikilink")。[静磁学是电磁学的一个分支](../Page/静磁学.md "wikilink")，研究稳定磁場下的性质。[微磁學是研究介觀尺度下](../Page/微磁學.md "wikilink")[鐵磁體的磁化過程](../Page/鐵磁體.md "wikilink")。[磁化学是研究化學物質與](../Page/磁化学.md "wikilink")[電磁场的關係](../Page/電磁场.md "wikilink")。

| 磁学量名称 | SI符号和单位       | CGS符号和单位              | 单位换算            |
| ----- | ------------- | --------------------- | --------------- |
| 磁通量   | Φ             | 韦伯                    | Wb              |
| 磁感应强度 | B             | 特斯拉                   | T               |
| 磁场强度  | H             | 安/米                   | A/m             |
| 磁化强度  | M             | 安/米                   | A/m             |
| 磁极化强度 | J             | 特斯拉                   | T               |
| 磁能积   | BH            | 焦/米<sup>3</sup>       | J/m<sup>3</sup> |
| 真空磁导率 | μ<sub>0</sub> | 4π•10<sup>-7</sup>H/m | \-              |

磁学的物理量及其单位换算

## 物質的磁性

[Magnetism.JPG](https://zh.wikipedia.org/wiki/File:Magnetism.JPG "fig:Magnetism.JPG")

### 抗磁性

抗磁性是物質抗拒外磁場的趨向，因此，會被磁場排斥。所有物質都具有抗磁性。可是，對於具有順磁性的物質，順磁性通常比較顯著，遮掩了抗磁性。\[18\]
只有純抗磁性物質才能明顯地被觀測到抗磁性。例如，[惰性氣體元素和](../Page/稀有气体.md "wikilink")[抗腐蝕金屬元素](../Page/抗腐蝕金屬.md "wikilink")（[金](../Page/金.md "wikilink")、[銀](../Page/銀.md "wikilink")、[銅等等](../Page/銅.md "wikilink")）都具有顯著的抗磁性。\[19\]當外磁場存在時，抗磁性才會表現出來。假設外磁場被撤除，則抗磁性也會遁隱形跡。

在具有抗磁性的物質裏，所有電子都已成對，內秉電子磁矩不能集成宏觀效應。抗磁性的機制是電子軌域運動，用經典物理理論解釋如下：\[20\]

  -
    由於外磁場的作用，環繞著[原子核的電子](../Page/原子核.md "wikilink")，其軌域運動產生的磁矩會做[拉莫爾進動](../Page/拉莫爾進動.md "wikilink")，從而產生額外電流與伴隨的額外磁矩。這額外磁矩與外磁場呈相反方向，抗拒外磁場的作用。由這機制所帶來的[磁化率與溫度無關](../Page/磁化率.md "wikilink")，以方程式表達為
    \[\chi=-\ \frac{\mu_0 NZe^2}{6m}\langle r^2\rangle\] ；

<!-- end list -->

  -
    其中，\(\mu_0\) 是[磁常數](../Page/磁常數.md "wikilink")，\(Z\) 是原子數量密度，\(Z\)
    是[原子序](../Page/原子序.md "wikilink")，\(B\) 是磁場，\(m\) 是電子質量，\(r\)
    是軌道半徑。\(\langle r^2\rangle\) 是 \(r^2\) 的量子力學平均值。

特別注意，這解釋只能用來[啟發思考](../Page/啟發法.md "wikilink")。正確的解釋需要依賴[量子力學](../Page/量子力學.md "wikilink")。

### 順磁性

[Para-ferro-anti.jpg](https://zh.wikipedia.org/wiki/File:Para-ferro-anti.jpg "fig:Para-ferro-anti.jpg")

[鹼金屬元素和除了](../Page/鹼金屬.md "wikilink")[鐵](../Page/鐵.md "wikilink")、[鈷](../Page/鈷.md "wikilink")、[鎳以外的](../Page/鎳.md "wikilink")[過渡元素都具有順磁性](../Page/過渡元素.md "wikilink")。\[21\]在順磁性物質內部，由於原子軌域或分子軌域只含有奇數個電子，會存在有很多未配對電子。遵守[包立不相容原理](../Page/包立不相容原理.md "wikilink")，任何配對電子的自旋，其磁矩的方向都必需彼此相反。未配對電子可以自由地將磁矩指向任意方向。當施加外磁場時，這些未配對電子的磁矩趨於與外磁場呈相同方向，從而使磁場更加強烈。假設外磁場被撤除，則順磁性也會消失無蹤。

一般而言，除了金屬物質以外，\[22\]順磁性與溫度相關。由於熱騷動（）造成的碰撞會影響磁矩整齊排列，溫度越高，順磁性越微弱；溫度越低，順磁性越強烈。

在低磁場，足夠高溫的狀況，\[23\]根據，[磁化率](../Page/磁化率.md "wikilink") \(\chi\) 與絕對溫度
\(T\) 的關係式為\[24\]

\[\chi=C/T\] ；

其中，\(C\) 是依不同物質而定的[居里常數](../Page/居里常數.md "wikilink")。

### 鐵磁性

[Hysteresiscurve.svg](https://zh.wikipedia.org/wiki/File:Hysteresiscurve.svg "fig:Hysteresiscurve.svg")
在鐵磁性物質內部，如同順磁性物質，有很多未配對電子。由於（），這些電子的自旋趨於與相鄰未配對電子的自旋呈相同方向。由於鐵磁性物質內部又分為很多[磁疇](../Page/磁疇.md "wikilink")，雖然磁疇內部所有電子的自旋會單向排列，造成「飽合磁矩」，[磁疇與磁疇之間](../Page/磁疇.md "wikilink")，磁矩的方向與大小都不相同。所以，未被磁化的鐵磁性物質，其淨磁矩與磁化向量都等於零。

假設施加外磁場，這些磁疇的磁矩還趨於與外磁場呈相同方向，從而形成有可能相當強烈的磁化向量與其感應磁場。
隨著外磁場的增高，磁化強度也會增高，直到「飽和點」，淨磁矩等於飽合磁矩。這時，再增高外磁場也不會改變磁化強度。假設，現在減弱外磁場，磁化強度也會跟著減弱。但是不會與先前對於同一外磁場的磁化強度相同。磁化強度與外磁場的關係不是[一一對應關係](../Page/一一對應.md "wikilink")。磁化強度比外磁場的曲線形成了[磁滯迴線](../Page/磁滯迴線.md "wikilink")。

假設再到達飽和點後，撤除外磁場，則鐵磁性物質仍能保存一些磁化的狀態，淨磁矩與磁化向量不等於零。所以，經過磁化處理後的鐵磁性物質具有「自發磁矩」。

每一種鐵磁性物質都具有自己獨特的[居里溫度](../Page/居里溫度.md "wikilink")。假若溫度高過居里溫度，則鐵磁性物質會失去自發磁矩，從有序的「鐵磁相」轉變為無序的「順磁相」。這是因為[熱力學的無序趨向](../Page/熱力學.md "wikilink")，大大地超過了鐵磁性物質降低能量的有序趨向。根據[居里-外斯定律](../Page/居里-外斯定律.md "wikilink")，磁化率
\(\chi\) 與絕對溫度 \(T\) 的關係式為\[25\]

\[\chi=C/(T-T_c)\] ；

其中，\(T_c\) 是[居里溫度](../Page/居里溫度.md "wikilink")（採用絕對溫度單位）。

假設溫度低於居里溫度，則根據實驗得到的經驗公式，

\[\Delta M(T)/M_0=\beta T^{3/2}\] ；

其中，\(\Delta M(T)=M(T)-M_0\) 是磁化強度差，\(M(T)\) 與 \(M_0\) 是物質分別在絕對溫度 \(T\) 與
\(0K\) 的磁化強度，\(\beta\) 是依物質而定的比例常數。

這與[布洛赫溫度](../Page/费利克斯·布洛赫.md "wikilink")1.5次方定律（Bloch T<sup>3/2</sup>
law）的理論結果一致。

[鎳](../Page/鎳.md "wikilink")、[鐵](../Page/鐵.md "wikilink")、[鈷](../Page/鈷.md "wikilink")、[釓與它們的](../Page/釓.md "wikilink")[合金](../Page/合金.md "wikilink")、[化合物等等](../Page/化合物.md "wikilink")，這些常見的鐵磁性物質很容易做實驗顯示出其鐵磁性。

#### 磁疇

[Ferromag_Matl_Sketch.JPG](https://zh.wikipedia.org/wiki/File:Ferromag_Matl_Sketch.JPG "fig:Ferromag_Matl_Sketch.JPG")
[Powstawanie_domen_by_Zureks.png](https://zh.wikipedia.org/wiki/File:Powstawanie_domen_by_Zureks.png "fig:Powstawanie_domen_by_Zureks.png")
[Ferromag_Matl_Magnetized.JPG](https://zh.wikipedia.org/wiki/File:Ferromag_Matl_Magnetized.JPG "fig:Ferromag_Matl_Magnetized.JPG")
在鐵磁性物質內部，由於原子的磁矩不等於零，每一個原子的表現就好似微小的永久磁鐵。假設聚集於一個小區域的原子，其磁矩都均勻地同向平行排列，則稱這小區域為[磁疇或](../Page/磁疇.md "wikilink")[外斯疇](../Page/外斯疇.md "wikilink")（）。使用[磁力顯微鏡](../Page/磁力顯微鏡.md "wikilink")（），可以觀測到磁疇。

磁疇的存在是能量極小化的後果。這是物理大師[列夫·朗道和](../Page/列夫·朗道.md "wikilink")[葉津·李佛西茲](../Page/葉津·李佛西茲.md "wikilink")（）提出的點子。假設一個鐵磁性長方體是單獨磁疇（右圖a），則會有很多正[磁荷與負磁荷分別形成於長方塊的頂面與底面](../Page/磁荷.md "wikilink")，從而擁有較強烈的磁能。假設鐵磁性長方塊分為兩個磁疇（右圖b），其中一個磁疇的磁矩朝上，另一個朝下，則會有正磁荷與負磁荷分別形成於頂面的左右邊，又有負磁荷與正磁荷相反地分別形成於底面的左右邊，所以，磁能較微弱，大約為圖a的一半。假設鐵磁性長方塊是由多個磁疇組成（右圖c），則由於磁荷不會形成於頂面與底面，只會形成於斜虛界面，所有的磁場都包含於長方塊內部，磁能更微弱。這種組態稱為「閉磁疇」（），是最小能量態。\[26\]

如左圖所示，將鐵磁性物質置入外磁場，則磁疇壁會開始移動，假若磁疇的磁矩方向與外磁場方向近似相同，則磁疇會擴大；反之，則會縮小。這時，假若關閉磁場，則磁疇可能不會回到原先的未磁化狀態。鐵磁性物質已被磁化，形成[永久磁鐵](../Page/永久磁鐵.md "wikilink")。

假設磁化足夠強烈，所有會擴大的磁疇吞併了其它磁疇，結果只剩下單獨一個磁疇，則此物質已經達到[磁飽和](../Page/磁飽和.md "wikilink")。再增強外磁場，也無法更進一步使物質磁化。

假設外磁場為零，現將已被磁化的鐵磁性物質加熱至居里溫度，則物質內部的分子會被大幅度熱騷動，磁疇會開始分裂，每個磁疇變得越來越小，其磁矩也呈隨機方向，失去任何可偵測的磁性。假設現在將物質冷卻，則磁疇結構會自發地回復，就好像液體[凝固成固態晶體一樣](../Page/凝固.md "wikilink")。

### 反鐵磁性

[Antiferromagnetic_ordering.svg](https://zh.wikipedia.org/wiki/File:Antiferromagnetic_ordering.svg "fig:Antiferromagnetic_ordering.svg")

在反鐵磁性物質內部，相鄰價電子的自旋趨於相反方向。這種物質的淨磁矩為零，不會產生磁場。這種物質比較不常見，大多數反鐵磁性物質只存在於低溫狀況。假設溫度超過[奈爾溫度](../Page/奈爾溫度.md "wikilink")，則通常會變為具有順磁性。例如，[鉻](../Page/鉻.md "wikilink")、[錳](../Page/錳.md "wikilink")、輕[鑭系元素等等](../Page/鑭系元素.md "wikilink")，都具有反鐵磁性。

當溫度高於[奈爾溫度](../Page/奈爾溫度.md "wikilink") \(T_N\) 時，磁化率 \(\chi\) 與溫度 \(T\)
的理論關係式為\[27\]

\[\chi=\frac{2C}{T+T_N}\] 。

做實驗得到的經驗關係式為

\[\chi=\frac{2C}{T+\theta}\] ；

其中，\(\theta\) 是依物質而定的常數，與 \(T_N\) 差別很大。

理論而言，當溫度低於[奈爾溫度](../Page/奈爾溫度.md "wikilink") \(T_N\) 時，可以分成兩種狀況：\[28\]

  - 假設外磁場垂直於自旋，則垂直磁化率近似為常數 \(\chi_{\perp}\approx C/T_N\) 。
  - 假設外磁場平行於自旋，則在絕對溫度0K時，平行磁化率為零；在從0K到奈爾溫度 \(T_N\) 之間，平行磁化率會從
    \(\chi_{\parallel}(0)=0\) 平滑地單調遞增至 \(\chi_{\parallel}(T_N)=C/T_N\) 。

### 亞鐵磁性

[Ferrimagnetic_ordering.svg](https://zh.wikipedia.org/wiki/File:Ferrimagnetic_ordering.svg "fig:Ferrimagnetic_ordering.svg")

像鐵磁性物質一樣，當磁場不存在時，亞鐵磁性物質仍舊會保持磁化不變；又像反鐵磁性物質一樣，相鄰的電子自旋指向相反方向。這兩種性質並不互相矛盾，在亞鐵磁性物質內部，分別屬於不同[次晶格的不同原子](../Page/晶格.md "wikilink")，其磁矩的方向相反，數值大小不相等，所以，物質的淨磁矩不等於0，磁化強度不等於零，具有較微弱的鐵磁性。

由於亞鐵磁性物質是[絕緣體](../Page/絕緣體.md "wikilink")。處於高[頻率時變磁場的亞鐵磁性物質](../Page/頻率.md "wikilink")，由於感應出的[渦電流很少](../Page/渦電流.md "wikilink")，可以允許[微波穿過](../Page/微波.md "wikilink")，所以，可以做為像、、等等微波器件的材料。

由於組成亞鐵磁性物質的成分必需分別具有至少兩種不同的磁矩，只有化合物或合金才會表現出亞鐵磁性。常見的亞鐵磁性物質有[磁鐵礦](../Page/磁鐵礦.md "wikilink")（Fe<sub>3</sub>O<sub>4</sub>）、[鐵氧體](../Page/鐵氧體.md "wikilink")（ferrite）等等

### 超順磁性

當鐵磁體或亞鐵磁體的尺寸足夠小的時候，由於熱騷動影響，這些[奈米粒子會隨機地改變方向](../Page/奈米粒子.md "wikilink")。假設沒有外磁場，則通常它們不會表現出磁性。但是，假設施加外磁場，則它們會被磁化，就像順磁性一樣，而且磁化率超大於順磁體的磁化率。

## 磁现象

  - [磁滞现象简称磁滞](../Page/磁滞现象.md "wikilink")，是指由于磁性体在[磁化过程中存在的不可逆性](../Page/磁化.md "wikilink")，使磁性体中的[磁感应强度B的变化滞后于](../Page/磁感应强度.md "wikilink")[磁场强度H的变化的物理](../Page/磁场强度.md "wikilink")[现象](../Page/现象.md "wikilink")。
  - [电磁感应现象是指导体在磁场中运动](../Page/电磁感应现象.md "wikilink")，或[導體处在變化的](../Page/導體.md "wikilink")[磁场中](../Page/磁场.md "wikilink")，會產生[電動勢的物理现象](../Page/電動勢.md "wikilink")。

## 磁鐵

磁鐵能夠產生磁場，吸引[鐵磁性物質如](../Page/鐵磁性.md "wikilink")[鐵](../Page/鐵.md "wikilink")、[鎳](../Page/鎳.md "wikilink")、[鈷等金屬](../Page/鈷.md "wikilink")。磁體上磁性最強的部分叫[磁極](../Page/磁極.md "wikilink")。將條形磁鐵的中點用細線懸掛起來，靜止的時候，它的兩端會各指向地球南方和北方，指向北方的一端稱為[指北極或](../Page/指北極.md "wikilink")[N極](../Page/指北極.md "wikilink")，指向南方的一端為[指南極或](../Page/指南極.md "wikilink")[S極](../Page/指南極.md "wikilink")。如果將地球想成一塊大磁鐵，則目前地球的[地磁北極是指南極](../Page/地磁北極.md "wikilink")，[地磁南極則是指北極](../Page/地磁南極.md "wikilink")。磁鐵與磁鐵之間，同極相排斥、異極相吸引。所以，指南極與指南極相排斥，指北極與指北極相排斥，而指南極與指北極則相吸引。

磁鐵可分為「永久磁鐵」與「非永久磁鐵」。永久磁鐵可以是天然產物，又稱[天然磁石](../Page/磁石.md "wikilink")，也可以由人工製造（最強的磁鐵是[釹磁鐵](../Page/釹磁鐵.md "wikilink")）。非永久性磁鐵，例如[電磁鐵](../Page/電磁鐵.md "wikilink")，只有在某些條件下才會出現磁性。

### 電磁鐵

[Simple_electromagnet2.gif](https://zh.wikipedia.org/wiki/File:Simple_electromagnet2.gif "fig:Simple_electromagnet2.gif")\]\]
1820年，[丹麥物理學家](../Page/丹麥.md "wikilink")[漢斯·奧斯特發現載流導線會產生磁場](../Page/漢斯·奧斯特.md "wikilink")\[29\]。而當[直流电通過](../Page/直流电.md "wikilink")[螺線管](../Page/螺線管.md "wikilink")（线圈）时，會在螺線管之內製成均勻[磁場](../Page/磁場.md "wikilink")。如果在螺線管的中心置入[鐵磁性物質](../Page/鐵磁性.md "wikilink")（铁芯），被磁化后的鐵磁性物質會大大增強磁場。因此，电磁铁一般由環繞[鐵芯的](../Page/鐵芯.md "wikilink")[線圈構成](../Page/線圈.md "wikilink")\[30\]，電磁鐵所產生的磁場與[電流大小](../Page/電流.md "wikilink")、線圈圈數及中心的鐵磁體有關\[31\]。由於線圈的材料具有[電阻](../Page/電阻.md "wikilink")，這限制了電磁鐵所能產生的磁場大小，但隨著[超導體的發現與應用](../Page/超導體.md "wikilink")，將有機會超越現有的限制。

[電磁鐵屬非永久磁鐵](../Page/電磁鐵.md "wikilink")，可以通过控制电流將其[磁性啟動或是消除](../Page/磁性.md "wikilink")。不过，由于[H場和](../Page/磁场强度.md "wikilink")[B場是非線性關係](../Page/磁感强度.md "wikilink")，所以电磁铁断电后仍具有[剩磁](../Page/剩磁.md "wikilink")。电磁铁的应用非常普遍，例如：大型[起重機利用電磁鐵將廢棄車輛抬起](../Page/起重機.md "wikilink")，自勵式[發電機利用剩磁能夠自行啟動等](../Page/發電機.md "wikilink")\[32\]。

### 永久磁鐵

[Nd-magnet.jpg](https://zh.wikipedia.org/wiki/File:Nd-magnet.jpg "fig:Nd-magnet.jpg")立方體。\]\]
永磁鐵能夠長期保持其磁性，可分為天然的磁石（[磁铁矿](../Page/磁铁矿.md "wikilink")）和人造磁鐵（[鋁鎳鈷合金等](../Page/鋁鎳鈷合金.md "wikilink")）。永久磁鐵必須具有寬廣高長的[磁滯迴線](../Page/磁滯迴線.md "wikilink")。這樣，當外磁場為零時，仍舊能夠具有比較強烈的磁化強度；假若要將磁化強度變為零，需要施加比較強烈的外磁場。永磁体按照材料可分為[鐵氧體](../Page/鐵氧體.md "wikilink")、[鋁鎳鈷合金和](../Page/鋁鎳鈷合金.md "wikilink")[稀土磁鐵](../Page/稀土磁鐵.md "wikilink")。

## 磁单极子

如果我们将带有磁性的[金属棒截断为二](../Page/金属.md "wikilink")，新得到的两根磁棒则会“自动地”产生新的磁场，重新编排磁场的北极、南极，原先的北极南极两极在截断磁棒后会转换成四极各磁棒一南一北。如果继续截下去，磁场也同时会继续改变磁场的分布，每段磁棒总是会有相应的南北两极。而磁单极子，如果真的存在的话，则是完全不同的物体。它是一个完全独立的南极，完全没有跟任何北极链接，或者反之亦然。尽管对磁单极子的系统研究从1931年就开始了，但到目前为止，还没有被观察到，而且非常可能并不存在。\[33\]
然而，有些[理论物理学模型则预言了](../Page/理论物理学.md "wikilink")[磁单极子的存在](../Page/磁单极子.md "wikilink")。[保罗·狄拉克在](../Page/保罗·狄拉克.md "wikilink")1931年断言，因为[电场与](../Page/电场.md "wikilink")[磁场表现出某種](../Page/磁场.md "wikilink")[对称性](../Page/对称性.md "wikilink")，就像在[量子理论预言的正电荷或者负电荷并不需要相反的](../Page/量子理论.md "wikilink")[电荷存在](../Page/电荷.md "wikilink")，独立的南极或者北极应该也能被观测到。應用[量子理论](../Page/量子.md "wikilink")，狄拉克预言，如果磁单极子如果存在，就可以解释电荷的[量子化](../Page/量子化.md "wikilink")
-- 就是为何可以观察到[基本粒子带电量是电子帶电量的倍数](../Page/基本粒子.md "wikilink")。

一些[大统一理论也预言了磁单极子的存在](../Page/大统一理论.md "wikilink")：不同於基本粒子，磁单极子是[孤波](../Page/孤波.md "wikilink")（局域能量包）。使用这些模型去估计[大爆炸中产生的磁单极子的数目](../Page/大爆炸.md "wikilink")，得到的最初結果与对宇宙的观察结果相矛盾--磁单极子是如此的多而巨大，它們甚至可以阻止宇宙的膨胀。然而[宇宙暴脹理论](../Page/宇宙暴脹.md "wikilink")（也是这个理论被提出的原因之一）成功地解决了这问题。这个理论建立了一个模型，使得磁单极子在[宇宙中存在](../Page/宇宙.md "wikilink")，但数量极少的能夠与实际观测相符合。\[34\]

## 参见

  - [电](../Page/电.md "wikilink")
  - [以「磁」開頭的條目](../Page/以「磁」開頭的條目.md "wikilink")

## 註釋

## 参考资料

[C](../Category/磁学.md "wikilink") [C](../Category/電磁學.md "wikilink")

1.

2.

3.

4.

5.  Paul Hewitt, "Conceptual Physics". 10th ed. (2006), p.458

6.

7.  見《[鬼谷子](../Page/鬼谷子.md "wikilink")》[反應第二](../Page/:s:鬼谷子.md "wikilink")，

8.  Li Shu-hua, “Origine de la Boussole 11. Aimant et Boussole,” *Isis*,
    Vol. 45, No. 2. (Jul., 1954), p.175

9.

10. 見《[夢溪筆談](../Page/夢溪筆談.md "wikilink")》[夢溪筆談](../Page/:s:夢溪筆談/卷24.md "wikilink")，原文：「方家以磁石磨針鋒，則能指南，然常微偏東，不全南也，水浮多蕩搖。指爪及碗唇上皆可為之，運轉尤速，但堅滑易墜，不若縷懸為最善。其法取新纊中獨繭縷，以芥子許蠟，綴於針腰，無風處懸之，則針常指南。其中有磨而指北者。余家指南、北者皆有之。磁石之指南，猶柏之指西，莫可原其理」。

11. 見《[萍洲可談](../Page/萍洲可談.md "wikilink")》[卷二](../Page/:s:萍洲可談/卷二.md "wikilink")，原文：「……舟師識地理，夜則觀星，晝則觀日，陰晦觀指南針，或以十丈繩鉤，取海底泥嗅之，便知所至。……」

12. Carlson, John B. (1975) "Lodestone Compass: Chinese or Olmec
    Primacy?: Multidisciplinary analysis of an Olmec hematite artifact
    from San Lorenzo, Veracruz, Mexico”, Science, 189 (4205 : 5
    September), p. 753-760, DOI 10.1126/science.189.4205.753. p. 753–760

13. [Lodestone Compass: Chinese or Olmec Primacy?: Multidisciplinary
    analysis of an Olmec hematite artifact from San Lorenzo, Veracruz,
    Mexico - Carlson 189 (4205): 753 -
    Science](http://www.sciencemag.org/cgi/content/abstract/189/4205/753)

14.

15.

16.

17. [Oberlin Smith:
    Biography](http://www.ieeeghn.org/wiki/index.php/Oberlin_Smith),
    IEEE Global History Network. Accessed November 2, 2010.

18.

19.
20.

21.
22.
23. 更確切地說，當 \(\mu B/K_B T \gg 1\) 時，居里定律成立；其中，\(\mu\) 是磁矩，\(K_B\)
    是[波茲曼常數](../Page/波茲曼常數.md "wikilink")。

24.
25.
26.
27.
28.

29.  cited in

30.

31.

32.

33. Milton mentions some inconclusive events (p.60) and still concludes
    that "no evidence at all of magnetic monopoles has survived" (p.3).
    .

34. .