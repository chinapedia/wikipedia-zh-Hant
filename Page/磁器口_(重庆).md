**磁器口古镇**原名**瓷器口镇**、**白崖鎮**、**白岩场镇**，因明帝[朱允炆退位後隐居于此](../Page/朱允炆.md "wikilink")，也叫**龙隐镇**，位于[重庆市](../Page/重庆市.md "wikilink")[沙坪坝区](../Page/沙坪坝区.md "wikilink")，比邻[嘉陵江](../Page/嘉陵江.md "wikilink")。自[明](../Page/明朝.md "wikilink")、[清时期以来磁器口一直是重要的水陆码头](../Page/清朝.md "wikilink")，为嘉陵江下游物资集散地。

磁器口是重庆主城区内少有的能领略老重庆风味的地方，被稱爲「小重慶」。靠近码头处，有原国民政府主席[林森题词的](../Page/林森.md "wikilink")「小重庆碑」。

## 歷史

磁器口古镇最早叫“白岩场”或「白崖鎮」，因建在白崖山上的[白崖寺](../Page/白崖寺.md "wikilink")（也叫[白岩寺](../Page/白岩寺.md "wikilink")）而得名。[明朝建文帝](../Page/明朝.md "wikilink")[朱允炆被其四叔](../Page/朱允炆.md "wikilink")[朱棣篡位后](../Page/朱棣.md "wikilink")，逃出皇宫流落巴蜀时，曾在白崖寺隐匿达四、五年之久。因此，后来世人又将宝轮寺易名[龙隐寺](../Page/龙隐寺.md "wikilink")，所在白岩场也改称龙隐镇。

磁器口是[沙磁文化发源之地](../Page/沙磁文化.md "wikilink")，清朝时候龙隐镇一带[陶瓷业的发展](../Page/陶瓷.md "wikilink")，瓷器在很长一段时间里成为龙隐镇的主要产业。后来，就将龙隐镇改名为“瓷器口”。因**瓷**、**磁**同音，镇名被讹传为“磁器口”，并从此固定下来。

[抗战时聚集了](../Page/抗战.md "wikilink")[郭沫若](../Page/郭沫若.md "wikilink")、[徐悲鸿](../Page/徐悲鸿.md "wikilink")、[丰子恺](../Page/丰子恺.md "wikilink")、[傅抱石](../Page/傅抱石.md "wikilink")、[巴金](../Page/巴金.md "wikilink")、[冰心等文化名人](../Page/冰心.md "wikilink")。

## 現狀

磁器口現已成为重庆市的一个著名景区，来访的游客络绎不绝。内有特色小吃[陈麻花](../Page/陈麻花.md "wikilink")、[古镇鸡杂](../Page/古镇鸡杂.md "wikilink")、[毛血旺等](../Page/毛血旺.md "wikilink")。共有12条街巷，街道大多是明清建筑风格，街道由石板铺成，沿街店铺林立。古鎮素有「九宮十八廟」之說，磁器口也有[寶輪寺](../Page/宝轮寺_\(重庆\).md "wikilink")、[雲頂寺](../Page/雲頂寺.md "wikilink")、[复元寺](../Page/复元寺.md "wikilink")、[文昌宫](../Page/文昌宫.md "wikilink")、[翰林院](../Page/翰林院.md "wikilink")、[深水井](../Page/深水井.md "wikilink")、[巴渝民居官等景区](../Page/巴渝民居官.md "wikilink")。

## 画廊

[File:磁器口导游全景图.jpg|thumb|磁器口古镇入口处导示图](File:磁器口导游全景图.jpg%7Cthumb%7C磁器口古镇入口处导示图)
[File:cqk01.jpg|thumb|200px|磁器口的街道](File:cqk01.jpg%7Cthumb%7C200px%7C磁器口的街道)
[File:Ciqikou2.jpg|thumb|left|200px|磁器口老街](File:Ciqikou2.jpg%7Cthumb%7Cleft%7C200px%7C磁器口老街)
<File:Chongqing> snack.jpg|thumb|right|200px|陈麻花
[File:Ciqikou18.JPG|thumb|right|200px|磁器口张飞牛肉](File:Ciqikou18.JPG%7Cthumb%7Cright%7C200px%7C磁器口张飞牛肉)
[File:人头攒动的磁器口街道.jpg|right|200px|人头攒动的磁器口街道](File:人头攒动的磁器口街道.jpg%7Cright%7C200px%7C人头攒动的磁器口街道)

## 外部链接

[重庆磁器口管理委员会](http://www.cqkgz.com/)

[Category:重庆街道](../Category/重庆街道.md "wikilink")
[Category:重庆乡镇](../Category/重庆乡镇.md "wikilink")
[Category:沙坪坝区行政区划](../Category/沙坪坝区行政区划.md "wikilink")
[Category:中国历史文化名街](../Category/中国历史文化名街.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")
[Category:重庆旅游景点](../Category/重庆旅游景点.md "wikilink")