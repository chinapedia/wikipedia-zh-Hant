**磁通量**，符號為
\(\Phi_B\)，是通過某给定曲面的[磁場](../Page/磁場.md "wikilink")（亦称为磁通量密度）的大小的度量。磁通量的[国际单位制單位是](../Page/国际单位制.md "wikilink")[韦伯](../Page/韦伯_\(单位\).md "wikilink")。

## 描述

给定曲面上的磁通量大小与通过曲面的[磁場線的个数成正比](../Page/磁場#磁場線.md "wikilink")。此处磁场线的个数是个“净”数量，即从一个方向上通过的个数减去另一个方向上通过的个数。当一个均匀磁场[垂直通过一个平面](../Page/垂直.md "wikilink")，磁通量即是磁场与该平面[面积的乘积](../Page/面积.md "wikilink")。当均匀磁场\(\mathbf{B}\)以任意角度通过一个平面，磁通量即是磁场与该平面面积\(\mathbf{a}\)的[点积](../Page/点积.md "wikilink")。\[1\]

\[\displaystyle \Phi_B = \mathbf{B} \cdot \mathbf{a} = Ba \cos \theta\]
  

其中，\(\theta\)是磁场\(\mathbf{B}\)和平面面积法向量\(\mathbf{a}\)的夹角.

[Surface_integral_illustration.svg](https://zh.wikipedia.org/wiki/File:Surface_integral_illustration.svg "fig:Surface_integral_illustration.svg")
[Surface_normal.png](https://zh.wikipedia.org/wiki/File:Surface_normal.png "fig:Surface_normal.png")

在一般情况下，磁通量是通过磁場在曲面面积上的[积分定義的](../Page/积分.md "wikilink")（见图1和图2）。

\[\Phi_B = \iint\limits_S \mathbf{B} \cdot d\mathbf S\]

其中，\(\Phi_B \\)為磁通量，\(\mathbf{B}\)為[磁感應強度](../Page/磁感應強度.md "wikilink")，\(S\)为曲面，\(\cdot\)为点积，\(d\mathbf{S}\)为无穷小向量（见[曲面积分](../Page/曲面积分.md "wikilink")）。

磁通量通常通过通量计进行测量。通量计包括测量线圈以及估计测量线圈上电压变化的电路，从而计算磁通量。

## 通过闭曲面的磁通量

[高斯磁定律是四條](../Page/高斯磁定律.md "wikilink")[麥克斯韋方程之一](../Page/麥克斯韋方程.md "wikilink")，指出通過一[闭曲面的磁通量為零](../Page/曲面#定义.md "wikilink")。這定律是依据还没有发现[磁單極这一经验得出的](../Page/磁單極.md "wikilink")。

高斯磁定律為，对任意闭曲面：

\[\Phi_B=\int \!\!\! \int \mathbf{B} \cdot d\mathbf S = 0,\]

## 通过开曲面的磁通量

[Vector_field_on_a_surface.svg](https://zh.wikipedia.org/wiki/File:Vector_field_on_a_surface.svg "fig:Vector_field_on_a_surface.svg")
即使通过闭曲面的磁通量是零，通过[开曲面的磁通量可以不是零](../Page/曲面#定义.md "wikilink")，而且，它是电磁学中一个重要的物理量。例如，当通過一个導電线环的磁通量发生变化，这一变化會引起[電動勢的生成](../Page/電動勢.md "wikilink")，並因此在线环中產生[電流](../Page/電流.md "wikilink")。其關係式可由[法拉第電磁感應定律得出](../Page/法拉第電磁感應定律.md "wikilink")：

\[\mathcal{E} = \oint_{\partial \Sigma (t)}\left(  \mathbf{E}( \mathbf{r},\ t) +\mathbf{ v \times B}(\mathbf{r},\ t)\right) \cdot d\boldsymbol{\ell} = -{d\Phi_B \over dt},\]

其中（见图3）：

\[\mathcal{E}\]为[電動勢](../Page/電動勢.md "wikilink")

\[\Phi_B\]为通过开曲面的磁通量，这一开曲面的边界为\(\partial \Sigma (t)\)

\[\partial \Sigma (t)\]为一个随时间变化的闭曲线

\[d\boldsymbol{\ell}\]是边界\(\partial \Sigma (t)\)[无穷小向量元](../Page/无穷小量.md "wikilink")

\[\mathbf{v}\]是线段\(d\boldsymbol{\ell}\)的速度

\[\mathbf{E}\]为电场

\[\mathbf{B}\]为[磁场](../Page/磁场.md "wikilink")

在上述公式中，电动势的生成可以有两种解释：由[洛伦兹力引起的电荷在闭合曲线](../Page/洛伦兹力.md "wikilink")\(\partial \Sigma (t)\)上的运动；通过开曲面\(\Sigma (t)\)的磁通量。这一公式即是[發電機的原理](../Page/發電機.md "wikilink")。

## 与电通量的比较

[麥克斯韋方程中的](../Page/麥克斯韋方程.md "wikilink")[高斯電場定律為](../Page/高斯定律.md "wikilink")：

\[\Phi_E = \int \!\!\!\int_S \mathbf{E}\cdot d\mathbf{S} = {Q \over \epsilon_0},\]

其中

\[\mathbf{E}\]為電場

\[S\]為任意闭曲面

\[Q\]为曲面\(S\)包围的电荷

\[\epsilon_0\]為[真空電容率](../Page/真空電容率.md "wikilink")。

注意，通过闭曲面的[\(\mathbf{E}\)的通量](../Page/电通量.md "wikilink")“并不总是”零，這指出了電“單極”的存在，即自由的正負電荷。

## 参考文献

## 外部链接

  - Vicci, ：磁通量導管（專利）

## 參見

  - [磁場](../Page/磁場.md "wikilink")：代表磁力線的密度。
  - [麥克斯韋方程組](../Page/麥克斯韋方程組.md "wikilink")：是一組四條[偏微分方程式](../Page/偏微分方程式.md "wikilink")，被[詹姆斯·麥克斯韋用作描述電場和磁場](../Page/詹姆斯·麥克斯韋.md "wikilink")，以及它們與物質之間的相互作用。
  - [高斯定律](../Page/高斯定律.md "wikilink")：給出從一密閉表面流出的電通量及表面圈住的電荷之間的關係式。
  - [磁單極](../Page/磁單極.md "wikilink")：是一種大概能不嚴謹地被形容為「只有單極的磁鐵」的理論粒子。
  - [磁通量量子](../Page/磁通量量子.md "wikilink")：是流經超導體的磁通量的量子。
  - [卡爾·高斯](../Page/卡爾·高斯.md "wikilink")：跟物理教授[威廉·韋伯的合作發展出成果豐碩的研究](../Page/威廉·韋伯.md "wikilink")；它使得磁學領域得到了新知識。
  - [詹姆斯·麥克斯韋](../Page/詹姆斯·麥克斯韋.md "wikilink")：證明了電力和磁力是電磁的兩個互補層面。
  - [法拉第弔詭](../Page/法拉第弔詭.md "wikilink")：關於[法拉第電磁感應定律的](../Page/法拉第電磁感應定律.md "wikilink")[弔詭](../Page/弔詭.md "wikilink")。

{{-}}

[Category:物理量](../Category/物理量.md "wikilink")
[Category:磁学](../Category/磁学.md "wikilink")

1.