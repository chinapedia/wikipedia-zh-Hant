**磐石山**為[台灣知名山峰](../Page/台灣.md "wikilink")，也是[台灣百岳之一](../Page/台灣百岳.md "wikilink")，排名第88。磐石山高達3,106公尺，屬於[中央山脈](../Page/中央山脈.md "wikilink")，[行政區劃屬於](../Page/台灣行政區劃.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")。磐石山附近接連[奇萊主山](../Page/奇萊主山.md "wikilink")、[奇萊北峰等山](../Page/奇萊北峰.md "wikilink")。

[中華民國海軍](../Page/中華民國海軍.md "wikilink")2015年服役的[磐石號油彈補給艦](../Page/磐石號油彈補給艦.md "wikilink")，艦名即取自此山。

## 參考文獻

  - 楊建夫，《台灣的山脈》，2001年，臺北，遠足文化公司

[Category:花蓮縣山峰](../Category/花蓮縣山峰.md "wikilink")
[Category:中央山脈](../Category/中央山脈.md "wikilink")
[Category:台灣百岳](../Category/台灣百岳.md "wikilink")
[Category:太魯閣國家公園](../Category/太魯閣國家公園.md "wikilink")