**社会心理学**是从跟[社会的关联研究](../Page/社会.md "wikilink")[人屬的](../Page/人屬.md "wikilink")[心理学和](../Page/心理学.md "wikilink")[社会学的学问领域](../Page/社会学.md "wikilink")。被个人内过程的研究，对人过程的研究，集体内过程的研究，集合现象的研究等分类。而心理学家则认为，社会心理学是一门在社会情景下，以人的心理行为活动为研究对象，以实证方法为手段，基于心理学、社会学相关理论的社会科学。

一般认为，社会心理学创立于1908年，在那一年，[美国人](../Page/美国.md "wikilink")[罗斯和](../Page/罗斯.md "wikilink")[英国人](../Page/英国.md "wikilink")[麦独孤不约而同地发表了题名为社会心理学的学术专著](../Page/麦独孤.md "wikilink")。

社会心理学基于心理学和社会学两学科，因而拥有两个学科的许多基础理论。通常，学者们将基于[心理学理论和实证方法的社会心理学内容叫做](../Page/心理学.md "wikilink")[心理-社会心理学](../Page/心理-社会心理学.md "wikilink")。把基于[社会学理论和实证方法的社会心理学内容叫做](../Page/社会学.md "wikilink")[社会-社会心理学](../Page/社会-社会心理学.md "wikilink")。

[心理-社会心理学曾经被看做社会心理学的一种研究倾向](../Page/心理-社会心理学.md "wikilink")，而今作为心理学的分支科学而存在。

目前来看，心理学家说社会心理学，通常是指[心理-社会心理学](../Page/心理-社会心理学.md "wikilink")，社会学家说社会心理学，通常是指[社会-社会心理学](../Page/社会-社会心理学.md "wikilink")。但是在少数场合，社会心理学一词则被理解为[心理-社会心理学与](../Page/心理-社会心理学.md "wikilink")[社会-社会心理学研究成果的总合](../Page/社会-社会心理学.md "wikilink")。

## 心理學

心理學家對社會心理學有興趣，主要研究[態度](../Page/態度.md "wikilink")、[社會認知](../Page/社會認知.md "wikilink")、[認知障礙](../Page/認知障礙.md "wikilink")、社會影響和[人際關係的行為](../Page/人際關係.md "wikilink")。兩份重要的社會心理學著作分別是《[人格與社會心理學期刊](../Page/人格與社會心理學期刊.md "wikilink")》（The
Journal of Personality and Social
Psychology）和《[實驗社會心理學期刊](../Page/實驗社會心理學期刊.md "wikilink")》（The
Journal of Experimental Social Psychology）。

## 内在现象

### 自我概念

自我概念是指人们对自我的一切信念的一个术语。大致可分為現實自我（Actual self) 和理想自我 (Ideal
self)兩種。其中現實自我為人們自身在社會、家庭和工作等領域中的角色，而理想自我是人們渇望追求的自我形象。

### 态度

在社会心理学领域，态度被定义为是被学习的，是一个人、一件物品、一个地方或者事件的整体评估，影响着人的[思想和行动](../Page/思想.md "wikilink")。\[1\]
更简单的来说，态度是赞成或反对、喜好或厌恶的基本表达，或如本所说，喜欢和不喜欢。\[2\]例子包括喜欢巧克力冰淇淋，或赞同特定政党的价值观。

### 说服

近年来，说服这个话题受到了广泛的关注。说服是一种积极的方式去影响或指引他人更倾向于某种[态度](../Page/态度.md "wikilink")，[思想或行为通过理性或情绪化的方式](../Page/思想.md "wikilink")。说服更依赖于“呼吁”，而不是压力或[强迫](../Page/强迫.md "wikilink")。已经发现了许多影响说服过程的变量;这些通常在五个主要类别中出现:“谁”说“什么”和“谁”和“如何”。\[3\]

### 社会认知

社会认知是一个现在正不断发展的社会心理学领域，它研究人们如何感知、思考以及记住他人的信息。很多研究都基于人们对(他人)不同于非社会目标的看法。\[4\]

## 人际现象

### 社会影响

社会影响是一个包罗万象的术语，用来描述人们对彼此影响作用。它被认为是社会心理学的一个基本价值，与对态度和说服的研究有很大的重叠。社会影响的三个主要方面包括:从眾、顺从和服从。社会影响也与[群體動力學的研究密切相关](../Page/群体动力学.md "wikilink")，因为在社会群体中，[多數決原則都是最强的](../Page/多數決原則.md "wikilink")。

### Group dynamics

### 人际吸引

是與他人在情感之間互相親密的狀態，一種人關係關係的肯定形式，按照吸引程度，可分成親合、喜歡和愛情。首先親合是低層次的人際吸引，喜歡是中等程度的吸引，愛情是最強烈的吸引形式。

### 人際吸引的影響因素

時空接近性(proximity)或熟悉性(familiarity)我們愈常見到或互動頻繁者，愈容易成為朋友。[單純曝光效應](../Page/单纯曝光效应.md "wikilink")(mere
exposure effect)一重覆接觸或見到某對象，便會對該對象產生好感。

## 群体社会心理学

### 從眾行為

## 应用社会心理学

由德裔美国心理学家[库尔特·勒温倡导研究](../Page/库尔特·勒温.md "wikilink")。

## 有關文獻

  - Cote, J. E. & Levine, C. G. (2002). *Identity formation, agency, and
    culture*. Mahwah, New Jersey: Lawrence Erlbaum Associates.
  - Sewell, W. H. (1989). Some reflections on the golden age of
    interdisciplinary social psychology. *Annual Review of Sociology*,
    Volume 15.
  - [In-Mind Magazine, Social Psychology for the
    public](http://www.in-mind.org)
  - [An easy way to learn social psychology through daily statements on
    the
    iPhone](http://itunes.apple.com/nl/app/social-knowledge-the-game/id561508167?mt=8)
  - [An easy way to learn social psychology through daily statements on
    the
    Android](https://play.google.com/store/apps/details?id=nl.wligtenberg.inmind.quiz)

<!-- end list -->

  - [南博](../Page/南博.md "wikilink")『社会心理学入門』（[岩波書店](../Page/岩波書店.md "wikilink")、1998年、ISBN
    978-4004120612）
  - [山岸俊男](../Page/山岸俊男.md "wikilink")『社会心理学キーワード』（[有斐閣](../Page/有斐閣.md "wikilink")、2001年、ISBN
    978-4641058729）
  - [古畑和孝](../Page/古畑和孝.md "wikilink")『社会心理学小辞典』（[有斐閣](../Page/有斐閣.md "wikilink")、2002年、ISBN
    978-4641002180）

[\*](../Category/社會心理學.md "wikilink")
[Category:心理学分支](../Category/心理学分支.md "wikilink")
[Category:社会学分支](../Category/社会学分支.md "wikilink")

1.
2.
3.
4.