[French_Polynesia_map.jpg](https://zh.wikipedia.org/wiki/File:French_Polynesia_map.jpg "fig:French_Polynesia_map.jpg")地圖,社會群島位於左上方。地圖來源：[德克薩斯州大學奧斯汀分校Perry](../Page/德克薩斯州大學奧斯汀分校.md "wikilink")-Castañeda圖書館。\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Unofficial_flag_of_the_Leeward_Islands_\(Society_Islands\).svg "fig:缩略图")
**社會群島**（[法語](../Page/法語.md "wikilink")：*Îles de la Société* 或*Archipel
de la Société*；[英語](../Page/英語.md "wikilink")：*Society
Islands*），是位於[南太平洋的島群](../Page/南太平洋.md "wikilink")，约在[南纬](../Page/南纬.md "wikilink")16°－19°，[西经](../Page/西经.md "wikilink")147°－156°之间；隸屬於[法屬玻里尼西亞](../Page/法屬玻里尼西亞.md "wikilink")。

社會群島名稱源自派遣[庫克船長探索世界的英國](../Page/庫克船長.md "wikilink")[皇家學會](../Page/皇家學會.md "wikilink")（Royal
Society）。

社會群島從地理、政治、行政上分成兩大島群：

  - **[向風群島](../Page/向風群島_\(社會群島\).md "wikilink")**（[法語](../Page/法語.md "wikilink")：*Îles
    du Vent*；[英語](../Page/英語.md "wikilink")：*Windward Islands*），由東至西順序：
      - [美黑蒂亞島](../Page/美黑蒂亞島.md "wikilink")（Mehetia）

      - [大溪地島](../Page/大溪地.md "wikilink")（Tahiti）

      - [茉莉亞島](../Page/莫雷阿岛.md "wikilink")（Moorea）

      - （麥奧島）（Maiao）

      - [泰蒂亞羅阿環礁](../Page/泰蒂亞羅阿環礁.md "wikilink")（Tetiaroa）（已故影星[馬龍·白蘭度的私人小島](../Page/馬龍·白蘭度.md "wikilink")）

<!-- end list -->

  - **[背風群島](../Page/背風群島_\(社會群島\).md "wikilink")**（[法語](../Page/法語.md "wikilink")：*Îles
    Sous-le-Vent*；[英語](../Page/英語.md "wikilink")：*Leeward
    Islands*），由東至西順序：
      - [胡阿希內島](../Page/胡阿希內島.md "wikilink")（Huahine）（在漲潮時一分為二，包括北邊的[大胡阿希內島](../Page/大胡阿希內島.md "wikilink")（Huahine
        Nui）和南邊的[小胡阿希內島](../Page/小胡阿希內島.md "wikilink")（Huahine Iti））
      - [波拉波拉島](../Page/波拉波拉島.md "wikilink")（Bora Bora）
      - [賴阿特阿島](../Page/賴阿特阿島.md "wikilink")（Raiatea）（被[珊瑚礁包圍](../Page/珊瑚礁.md "wikilink")）
      - [塔哈島](../Page/塔哈島.md "wikilink")（Tahaa）（被[珊瑚礁包圍](../Page/珊瑚礁.md "wikilink")）
      - [莫皮蒂島](../Page/莫皮蒂島.md "wikilink")（Maupiti）
      - [图帕伊島](../Page/图帕伊島.md "wikilink")（Tupai）
      - [莫皮哈环礁](../Page/莫皮哈环礁.md "wikilink")（Maupihaa）
      - [马努阿环礁](../Page/马努阿环礁.md "wikilink")（Manuae）
      - [莫圖奧內環礁](../Page/莫圖奧內環礁.md "wikilink")（Motu One）

社會群島於1843年成為[法國的](../Page/法國.md "wikilink")[保護地](../Page/保護地.md "wikilink")，於1880年成為[殖民地](../Page/殖民地.md "wikilink")，歸入[法屬玻里尼西亞管轄](../Page/法屬玻里尼西亞.md "wikilink")。人口214，445（2002年數字），陸地面積約1.598[平方公里](../Page/平方公里.md "wikilink")（617
[平方英里](../Page/平方英里.md "wikilink")）。而[法屬玻里尼西亞首府](../Page/法屬玻里尼西亞.md "wikilink")[帕皮提](../Page/帕皮提.md "wikilink")（Papeete）位於[塔希提島上](../Page/大溪地.md "wikilink")。

## 参考文献

## 外部連結

  - [從太空俯瞰社會群島](http://earthfromspace.photoglobe.info/spc_society_islands.html)
  - [BORA BORA DISCOVERY](http://www.borabora-discovery.com)

## 参见

  - [大溪地語](../Page/大溪地語.md "wikilink")

{{-}}

[社会群岛](../Category/社会群岛.md "wikilink")