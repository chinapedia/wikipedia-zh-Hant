**約瑟夫·華盛頓·布萊恩**（，），[美國](../Page/美國.md "wikilink")[NBA聯盟前職業](../Page/NBA.md "wikilink")[籃球運動員](../Page/籃球.md "wikilink")，擔任[前鋒](../Page/前鋒.md "wikilink")。

## 簡介

喬·布萊恩於1975年的[NBA選秀被](../Page/NBA選秀.md "wikilink")[金州勇士隊選中](../Page/金州勇士隊.md "wikilink")，隨後立即被交易至[費城76人隊](../Page/費城76人隊.md "wikilink")，曾協助76人在1977年打入[總決賽](../Page/NBA總決賽.md "wikilink")，可惜最終敗於[波特蘭拓荒者隊](../Page/波特蘭拓荒者隊.md "wikilink")。

NBA生涯結束後，便前往[意大利繼續其職業籃球生涯](../Page/意大利.md "wikilink")，在[義大利職籃共達七個球季之久](../Page/義大利籃球甲級聯賽.md "wikilink")，而後也曾替多支著名歐洲球隊效力。2005年繼承前隊友[亨利·畢比](../Page/:en:Henry_Bibby.md "wikilink")，出任[WNBA](../Page/WNBA.md "wikilink")[洛杉磯火花隊的總教練](../Page/洛杉磯火花隊.md "wikilink")。

## 家庭

祖·拜仁的妻子为Pam Cox（Pam的兄弟Chubby Cox也是篮球运动员，移居委内瑞拉，Chubby的儿子John也是篮球运动员），两人
共育有兩女一子，女兒為夏里亞（Sharia）和夏雅（Shaya），其子即為退役NBA著名球星、效力[洛杉磯湖人隊的](../Page/洛杉磯湖人隊.md "wikilink")[科比·布萊恩](../Page/科比·布萊恩.md "wikilink")。

## 参考资料

## 外部链接

  - BASKETBALL-REFERENCE.COM－[生涯成績](http://www.basketball-reference.com/players/b/bryanjo01.html)

  - WNBA－[教練簡介](https://web.archive.org/web/20060428115037/http://www.wnba.com/coachfile/joe_bryant/index.html?nav=page)

  - 義大利職業籃球聯盟－[生涯成績](http://195.56.77.208/player/pbd.phtml?ply=BRY-JOE-54&from=1980&team=562&type2=t&name_search=Bryant)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:費城76人隊球員](../Category/費城76人隊球員.md "wikilink")
[Category:圣迭戈快船队球员](../Category/圣迭戈快船队球员.md "wikilink")
[Category:休斯頓火箭隊球員](../Category/休斯頓火箭隊球員.md "wikilink")