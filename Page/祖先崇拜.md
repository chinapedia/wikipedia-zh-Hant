[YuKiuAncestralHall05.jpg](https://zh.wikipedia.org/wiki/File:YuKiuAncestralHall05.jpg "fig:YuKiuAncestralHall05.jpg")的[神龕](../Page/神龕.md "wikilink")\]\]

**祖先崇拜**，或稱**祭祀祖先**、**祖靈信仰**、**敬祖**、**祭拜祖先**、**拜公妈**，是指一种，供奉、祭祀祖先，類似[宗教儀式與习惯的行為](../Page/宗教.md "wikilink")。基于死去的[祖先仍然深深存在後人心中](../Page/祖先.md "wikilink")，仍然会影响到现世，并且对子孙的生存有影响的[信仰](../Page/信仰.md "wikilink")\[1\]。俚語俗話說「吃菓子拜樹頭、飲泉水思源頭」。

在大部分不同[文化中](../Page/文化.md "wikilink")，祖先崇拜和[神灵崇拜不太一样](../Page/神灵.md "wikilink")，对神崇拜大多是希望祈求一些[利益](../Page/利益.md "wikilink")，但人们对祖先的信仰，并不仅仅是希望祈求一些好处，也是表达亲情或者對先人的尊敬。[儒教文化将祖先当作神灵一样崇拜](../Page/儒教.md "wikilink")\[2\]。受此影响，一般中国人除了敬祖外，还希望受到祖先神灵的庇佑，“祖先保佑”一样是最常见的祈福语。

## [儒家文化圈](../Page/儒家文化圈.md "wikilink")

[Ancestor_Worship.jpg](https://zh.wikipedia.org/wiki/File:Ancestor_Worship.jpg "fig:Ancestor_Worship.jpg")[忠烈祠的牌位供奉](../Page/忠烈祠.md "wikilink")\]\]
在[儒家的观念中](../Page/儒家.md "wikilink")，祖先信仰主要講究慎終追遠，表達對祖先的感念之情，也是相信祖先的在天之靈會继续保佑自己的后代。“孝”是最重要的的美德之一，俗曰「[聖人以孝治](../Page/聖人.md "wikilink")[天下](../Page/天下.md "wikilink")」，可由孝引發[忠](../Page/忠.md "wikilink")、[信](../Page/信.md "wikilink")、[仁](../Page/仁.md "wikilink")、[義等道德](../Page/義.md "wikilink")。即使对已经去世的先人，也要像他们依然活着时一样的尊敬，在节日、忌日中要供奉、祭祀，对祖先的崇拜是一种类或-{zh-hans:准;zh-hant:準}-[宗教信仰](../Page/宗教.md "wikilink")。以[漢族](../Page/漢族.md "wikilink")[客家人為例](../Page/客家人.md "wikilink")，祖先崇拜具有尊宗報本、文化教育、祈福、預兆等功能\[3\]。潮汕地区稱祭祖為**拜老公**每逢初一十五或重大节日，各家制作[红桃粿等等贡品](../Page/红桃粿.md "wikilink")，祭祀祖先。中國的許多節日，都離不開祭祀祖先這一內容，[除](../Page/除夕.md "wikilink")、[清](../Page/清明.md "wikilink")、[盂](../Page/中元.md "wikilink")、[九四節日](../Page/重陽.md "wikilink")，是中國傳統裡祭祖的四大節慶。[上巳節](../Page/上巳節.md "wikilink")、[端午](../Page/端午.md "wikilink")、[中秋](../Page/中秋.md "wikilink")、[寒衣節](../Page/寒衣節.md "wikilink")、[冬至等節慶](../Page/冬至.md "wikilink")，亦是個別地域的祭祖佳辰。

祖先祭祀的方式通常有[家祭](../Page/家祭.md "wikilink")、[墓祭](../Page/墓祭.md "wikilink")、[祠祭](../Page/祠祭.md "wikilink")\[4\]。在[儒家社會](../Page/儒家.md "wikilink")，祖先信仰具有特殊的意義，佔有重要的地位。在這一信仰影響下，「上無愧祖先，下不負子孫」成為人們生命中的重要信念\[5\]，並形成了對家庭的重視和[家文化的發達](../Page/家文化.md "wikilink")。

### [大中华地区](../Page/大中华地区.md "wikilink")

#### [中國大陆](../Page/中國大陆.md "wikilink")

在[中國歷史中的](../Page/中國歷史.md "wikilink")[漢人對於祖先崇拜](../Page/漢人.md "wikilink")，表现在每年的[扫墓與定時祭拜等](../Page/扫墓.md "wikilink")[宗教儀式](../Page/宗教.md "wikilink")，一般在[除](../Page/除夕.md "wikilink")、[清](../Page/清明節.md "wikilink")、[盂](../Page/中元節.md "wikilink")、[九四節及先人的忌日必會祭拜](../Page/重陽節.md "wikilink")。其他如[端午](../Page/端午.md "wikilink")、[中秋](../Page/中秋.md "wikilink")、[冬至等](../Page/冬至.md "wikilink")[中國傳統節日](../Page/中國傳統節日.md "wikilink")，一家團聚的時刻，也是祭拜祖先、緬懷先人功業的好時機。

有些地方有特別的節日，如[寒衣節](../Page/寒衣節.md "wikilink")、[拗九節](../Page/拗九節.md "wikilink")，[華北因為傳說](../Page/華北.md "wikilink")[孟姜女十月初一送棉襖給其夫婿](../Page/孟姜女.md "wikilink")，故添增了十月初一[寒衣節](../Page/寒衣節.md "wikilink")。[閩東因為傳說](../Page/閩東.md "wikilink")[目連尊者在正月廿九送](../Page/目連尊者.md "wikilink")[甜粥給母親](../Page/拗九粥.md "wikilink")，故添增了正月廿九[拗九節](../Page/拗九節.md "wikilink")，並且祭祖。

在逝者下葬时，随同准备许多[日用品或](../Page/日用品.md "wikilink")[陰間所用的](../Page/陰間.md "wikilink")[紙錢](../Page/紙錢.md "wikilink")[貨幣](../Page/貨幣.md "wikilink")、[紙紮祭品](../Page/紙紮祭品.md "wikilink")，一同焚化，如同送先人到另一个世界生活一样，甚至在不同季节，以不同服飾或紙製衣物烧予祖先。

现在，随着时代的进步和移风易俗的推动，很多地方兴起了低碳祭扫、文明祭祖，放鲜花、植树成为了时尚，而传统的烧纸钱，政府認為污染环境且有火灾风险，推動減量或者根本不燒，所以已经不再如以前一样盛行。

#### [港澳](../Page/港澳.md "wikilink")

#### [臺灣](../Page/臺灣.md "wikilink")

在臺灣，[閩南族群習慣在每年的](../Page/閩南族群.md "wikilink")[上巳節](../Page/上巳節.md "wikilink")（農曆3月3日）或[清明節](../Page/清明節.md "wikilink")（公曆4月5日）進行[掃墓儀式](../Page/掃墓.md "wikilink")、祭拜祖先，[客家族群則習慣在](../Page/客家族群.md "wikilink")[元宵節](../Page/元宵節.md "wikilink")（農曆1月15日）前後。一般人的家中多半設有祖先牌位，在四大節日（[除清盂九](../Page/除清盂九.md "wikilink")）與先人[忌日加以祭拜](../Page/忌日.md "wikilink")。

[金門閩南人及](../Page/金門縣.md "wikilink")[客家族群](../Page/客家族群.md "wikilink")，重視祖先崇拜更甚於[神明](../Page/神明.md "wikilink")，如金門人設置神龕，祖先牌位在大位（參拜者的右方，俗稱龍邊），神明金身在次位（參拜者的左方，俗稱虎邊）；[客家族群傳統只將](../Page/客家族群.md "wikilink")[祖先供在神桌正中](../Page/祖先.md "wikilink")，而不供神明。此二族群與[臺灣閩南人供神明在龍邊](../Page/臺灣閩南人.md "wikilink")、祖先在虎邊不同，金門人認為祭祖比拜神重要，客家裔則視祖先等同神明。

[臺灣地區的南島語族多有崇拜祖靈的習俗](../Page/臺灣原住民族.md "wikilink")\[6\]\[7\]\[8\]。

### 越南

在[越南](../Page/越南.md "wikilink")，一般[越南人](../Page/越南人.md "wikilink")，在自己的家中都设立祖先的[牌位](../Page/牌位.md "wikilink")。一般人并不一定过[生日](../Page/生日.md "wikilink")，但非常重视对先人的忌日祭祀，焚香祭祀，供奉先人的照片。

### 日本本土

在[日本本土](../Page/日本本土.md "wikilink")，一般家庭在[佛龕的中央安置祖先](../Page/佛龕.md "wikilink")[牌位](../Page/牌位.md "wikilink")，對[冥界的祖先神靈進行祀拜](../Page/冥界.md "wikilink")。

### 朝-{}-鲜半島

同样在儒家文化影響下[朝-{}-鲜半島的](../Page/朝鲜半島.md "wikilink")[北韓與](../Page/北韓.md "wikilink")[南韓](../Page/南韓.md "wikilink")，也要对先人进行“祭礼”（）和“祭祀”（）。而祭祖所祭拜食物花費的時間，與生人之間吃飯的時間可是說無異，乃是奉行《[論語](../Page/論語.md "wikilink")》說到的——祭如在，祭神如神在。最重要的是“周年祭”〔〕。\[9\]

### 日本琉球

在[琉球](../Page/琉球.md "wikilink")，家庭中設有供奉祖先的神龕，各門中（宗族的分支）設有祠堂，也會在[春分](../Page/春分.md "wikilink")、清明節、重陽節等節日掃墓祭祖，焚燒紙錢供奉祖先。

## 其它地区

在其它類型的一些文化中，也存在祖先神靈信仰或对祖先的感念活动，但和漢字文化圈的祖先信仰有很大的不同。

### 印度

在[印度农村](../Page/印度.md "wikilink")，在今日时，人们会回忆逝去的人，在进食前会向先人祈祷。[印度教中有一个仪式叫](../Page/印度教.md "wikilink")“塔帕纳”，当一个家庭中有人去世，每年在十月份，家中的男人们会向[恒河中放入写着](../Page/恒河.md "wikilink")[梵文的赞美诗祝先人早入轮回](../Page/梵文.md "wikilink")。

### 欧洲

[欧洲的](../Page/欧洲.md "wikilink")[天主教国家](../Page/天主教.md "wikilink")，每年11月1日为[万圣节與](../Page/万圣节.md "wikilink")11月2日为[萬靈節](../Page/萬靈節.md "wikilink")，是为去世的亲属点蜡烛的节日，来自非常古老的传统，教堂在11月2日正式为去世的人做弥撒。

#### 爱尔兰

根据[爱尔兰的传说](../Page/爱尔兰.md "wikilink")，在“萨万节”（[凯尔特人新年](../Page/凯尔特人.md "wikilink")，一般为8月1日）时，逝者会回到自己家中，当天不关灯，不关门，家中要备有[食品](../Page/食品.md "wikilink")，即使食品掉在地下也不能收拾。

### 加拿大和美国

[加拿大和](../Page/加拿大.md "wikilink")[美国的习惯是在](../Page/美国.md "wikilink")[复活节](../Page/复活节.md "wikilink")、[圣诞节或](../Page/圣诞节.md "wikilink")[万圣节向逝者墓地献花和点蜡烛以示缅怀](../Page/万圣节.md "wikilink")，有时也向逝者去世的地点献花和点蜡烛，在公路旁有时会见到这样的地点，都是纪念因车祸去世的人。

### 非洲

祖先崇拜在[非洲也很流行](../Page/非洲.md "wikilink")。但非洲人是敬畏祖先，害怕祖先的灵魂会打扰自己的家庭，希望他们远去成仙\[10\]\[11\]。

## 參見

  - [祖先](../Page/祖先.md "wikilink")
  - [重陽](../Page/重陽.md "wikilink")
  - [清明](../Page/清明.md "wikilink")
  - [掃墓](../Page/掃墓.md "wikilink")
  - [郡望](../Page/郡望.md "wikilink")
  - [祖籍](../Page/祖籍.md "wikilink")
  - [堂號](../Page/堂號.md "wikilink")
  - [華人三大節日](../Page/華人三大節日.md "wikilink")
  - [中國民間信仰](../Page/中國民間信仰.md "wikilink")

## 参考文献

  - [当前的祖先崇拜](http://www.asia.si.edu/exhibitions/online/teen/default.htm)

## 外部链接

  - [香港的纸供](https://web.archive.org/web/20080414052109/http://www.freestuffpage.com/forums/showthread.php?t=5452)
  - [冥钱](http://www.luckymojo.com/hellmoney.html)

[祖先崇拜](../Category/祖先崇拜.md "wikilink")
[Category:人类学](../Category/人类学.md "wikilink")
[Category:喪葬](../Category/喪葬.md "wikilink")
[Category:宗教與死亡](../Category/宗教與死亡.md "wikilink")
[Category:巫覡宗教](../Category/巫覡宗教.md "wikilink")

1.
2.
3.  [祖先崇拜与四川客家神榜文化](http://www.xbnc.org/Article_Show.asp?ArticleID=3938)
4.  [萧放：明清时期祖先信仰与家族祭祀
    《文史知识》](http://www.chinafolklore.org/blog/?uid-28-action-viewspace-itemid-6637)
5.  [沈铭贤：《“生死俱善，人道毕矣”——中国古代的生死观及其现代意义》
    《社会科学》](http://www.sass.org.cn/zxyjs/articleshow.jsp?dinji=60&sortid=76&artid=28864)
6.
7.
8.
9.  [*Ancestor Worship and Korean
    Society,*](http://books.google.com/books?id=VDRAah_jogMC&printsec=frontcover&dq=ancestor+worship&sig=7awb9V-xHxY2WXWXzxZG2vDmJf4)
    Roger Janelli, Dawnhee Janelli, Stanford University Press, 1992.
    ISBN 0804721580.
10. ["Ancestors as Elders in
    Africa,"](http://books.google.com/books?id=9hNKkzt1ovEC&pg=PA412&dq=ancestor+worship+africa&lr=&sig=ENo6fMWeVwjYpmzNFflmE0YP0ug)
    Igor Kopytoff; in *Perspectives on Africa: A Reader in Culture,
    History, and Representation* (Editors Roy Richard Grinker &
    Christopher Burghard Steiner), Blackwell Publishing, 1997. ISBN
    978-1-55786-686-8.
11. [Some reflections on ancestor workship in
    Africa,](http://lucy.ukc.ac.uk/fdtl/ancestors/fortes2.html)  Meyer
    Fortes, *African Systems of Thought*, pages 122-142, University of
    Kent.