**祖尼角龍屬**（屬名：*Zuniceratops*）意為“來自[祖尼部落的有角面孔](../Page/祖尼部落.md "wikilink")”，是種[角龍下目](../Page/角龍下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於晚[白堊紀中](../Page/白堊紀.md "wikilink")[土侖階的](../Page/土侖階.md "wikilink")[美國](../Page/美國.md "wikilink")[新墨西哥州](../Page/新墨西哥州.md "wikilink")。牠們生存於1億年前，早於外表相近的[角龍科](../Page/角龍科.md "wikilink")，並可能為角龍科的祖先。

祖尼角龍身長約3到3.5公尺，高度約1公尺。牠們可能重達100到150公斤。祖尼角龍頭後的頭盾是多孔的，但缺乏[頸盾緣骨突](../Page/頸盾緣骨突.md "wikilink")（[Epoccipital](http://en.wikipedia.org/wiki/Epoccipital)）。祖尼角龍是已知最早有額角的角龍類，也是已知最古老的[北美洲角龍類](../Page/北美洲.md "wikilink")。這些角狀物被認為依年齡而增大。

## 發現與種

[Camarasaurus_skeleton,.jpg](https://zh.wikipedia.org/wiki/File:Camarasaurus_skeleton,.jpg "fig:Camarasaurus_skeleton,.jpg")與祖尼角龍的骨架模型\]\]
祖尼角龍的化石發現於1996年，由古生物學家Douglas G.
Wolfe的8歲兒子在[新墨西哥州的Moreno](../Page/新墨西哥州.md "wikilink")
Hill組發現。目前已經發現一個頭顱骨，以及來自數個個體的骨頭。最近，其中一個被認為是[鱗骨的骨頭](../Page/鱗骨.md "wikilink")，可能來自於[懶爪龍的](../Page/懶爪龍.md "wikilink")[坐骨](../Page/坐骨.md "wikilink")。

祖尼角龍的[模式種為](../Page/模式種.md "wikilink")**克-{里}-斯多佛祖尼角龍**（*Z.
christopheri*），名稱來自於古生物學家Douglas G. Wolfe的兒子的名字Christopher James Wolfe。

## 分類

祖尼角龍是個演化過渡的例子；祖尼角龍介於早期角龍類（如[原角龍](../Page/原角龍.md "wikilink")）以及較晚的大型[角龍科恐龍之間的過渡物種](../Page/角龍科.md "wikilink")。這支持了[角龍下目恐龍起源於](../Page/角龍下目.md "wikilink")[北美洲的理論](../Page/北美洲.md "wikilink")。

雖然祖尼角龍的第一個標本只有單排牙齒（在角龍類裡不尋常），但較晚發現的祖尼角龍化石有兩排牙齒。這證據顯示這些牙齒是隨者年齡而變成雙排的。祖尼角龍如同其他角龍類，是[草食性恐龍](../Page/草食性.md "wikilink")，而且可能是[群居動物](../Page/群居動物.md "wikilink")。

## 大眾文化

祖尼角龍曾出現在[BBC電視節目](../Page/BBC.md "wikilink")《[恐龍紀元](../Page/恐龍紀元.md "wikilink")》（*When
Dinosaurs Roamed America*）。

## 參考資料

  -
  - Wolfe, D. G. (2000). New information on the skull of *Zuniceratops
    christopheri*, a neoceratopsian dinosaur from the Cretaceous Moreno
    Hill Formation, New Mexico. pp. 93-94, in S. G. Lucas and A. B.
    Heckert, eds. Dinosaurs of New Mexico. New Mexico Museum of Natural
    History and Science Bulletin No. 17

## 外部連結

  - [*Zuniceratops* in The Dinosaur
    Encyclopaedia](http://web.me.com/dinoruss/dinos/de_4/5ce496e) at
    Dino Russ's Lair

[Category:冠飾角龍類](../Category/冠飾角龍類.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")