**祖父條款**（），相对于[追溯法令](../Page/追溯法令.md "wikilink")，是代表一种允许在旧有建制下已存的事物不受新通過条例约束的特例（“老人老办法，新人新办法。”）。英语中的「祖父」（grandfather）在这场合下会被当作[动词或其他衍生形态使用](../Page/动词.md "wikilink")，例如「grandfathered
power
plant」可能指一座老发电厂未能符合新污染管制条例，但在[不溯及既往原則下获得豁免而维持原貌继续运作](../Page/不溯及既往原則.md "wikilink")。

## 起源

原来的祖父條款在1890年至1910年收录于[美国南部的](../Page/美国南部.md "wikilink")[傑姆·克勞法中](../Page/傑姆·克勞法.md "wikilink")，目的是为了在防止[黑人](../Page/非裔美国人.md "wikilink")、[美洲原住民](../Page/美洲原住民.md "wikilink")、[墨西哥裔美国人及非](../Page/墨西哥裔美国人.md "wikilink")[盎格魯撒克遜人拥有投票权的同时](../Page/盎格魯撒克遜人.md "wikilink")，保障大部分[白人的](../Page/白人.md "wikilink")[投票权](../Page/投票权.md "wikilink")。1870年之前订立的投票权禁制法由于[宪法](../Page/美国宪法.md "wikilink")《[第十五修正案](../Page/美國憲法第十五修正案.md "wikilink")》的制定而失效\[1\]，作为回應，部分州通过法例以[人頭稅及](../Page/人頭稅.md "wikilink")来评定选民资格，唯一的例外是那些在[南北战争前已经拥有选民资格的人及其后代](../Page/南北战争.md "wikilink")。由于这些受惠于新特例的选民与战前的一辈相差了大概两代，「祖父條款」一词因而产生。後來1915年及1939年[美国最高法院先後裁定祖父條款及讀寫能力測試違反憲法](../Page/美国最高法院.md "wikilink")，將之廢除；而人頭稅選民篩選程序終於1964年通过的《[第二十四修正案](../Page/美国宪法第二十四修正案.md "wikilink")》被廢止\[2\]，為了確保成效1965年再通过\[3\]，聯邦政府這一系列的行動大致上已經保護了受歧視種族的[公民權](../Page/公民權.md "wikilink")。

## 其他例子

  - 1952年[美国國會通过](../Page/美国國會.md "wikilink")《[第二十二修正案](../Page/美國憲法第二十二修正案.md "wikilink")》防止[總統尋求第三屆任期](../Page/美國總統.md "wikilink")，但该修正案对当时第二任期临近结束的[哈利·S·杜鲁门没有约束力](../Page/哈利·S·杜鲁门.md "wikilink")，杜鲁门依然可以参选下一届總統选举，儘管他最后败选而退。

## 参考

## 延伸閱讀

  -
  - [Grandfather Clause in *From Jim Crow to Civil Rights: The Supreme
    Court and the Struggle for Racial
    Equality*](https://books.google.com/books?id=NytV-qWjSBcC&pg=PA70)

[Category:美国法律](../Category/美国法律.md "wikilink")
[Category:法律术语](../Category/法律术语.md "wikilink")

1.  [第15號修正案納入憲法](http://www.loc.gov/rr/program/bib/ourdocs/15thamendment.html)
    - [美國國會圖書館](../Page/國會圖書館_\(美國\).md "wikilink")
2.  [宪法修正案第11至27號](http://www.archives.gov/exhibits/charters/constitution_amendments_11-27.html)
    - [美国国家档案和记录管理局](../Page/美国国家档案和记录管理局.md "wikilink")
3.  [投票权FAQ](http://www.usdoj.gov/crt/voting/misc/faq.htm) -
    [美国司法部民权局](../Page/美国司法部.md "wikilink")