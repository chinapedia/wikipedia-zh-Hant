**祖瓦拉**  (, [祖瓦拉柏柏尔语](../Page/祖瓦拉柏柏尔语.md "wikilink"): *Tamurt n Wat
Willul*) \[1\]
是位于[利比亚北部](../Page/利比亚.md "wikilink")[地中海沿岸的一座港口城市](../Page/地中海.md "wikilink")，人口约180,000人。它位于[的黎波里以西](../Page/的黎波里.md "wikilink")，是[努加特海姆斯省省会](../Page/努加特海姆斯省.md "wikilink")。主要以[柏柏尔语族为主](../Page/柏柏尔语族.md "wikilink")\[2\]。

## 参考

### 资源

  - [Terence Frederick
    Mitchell](../Page/Terence_Frederick_Mitchell.md "wikilink"),
    *Ferhat. An Everyday Story of Berber Folk in and around Zuara
    (Libya)*, Köln, Köppe, 2007 - ISBN 978-3-89645-396-9

## 外部链接

  - [official Zuwara website](http://www.zuwara.com) - *in Arabic*

[Z](../Category/利比亚城市.md "wikilink")

1.  Mitchell (2007: 29, 195).
2.  "Voyage du Scheikh Et-Tidjani dans la régence de Tunis pendant les
    années 706, 707 et 708 de l'hégire (1306-1309)", transl. by M. A.
    Rousseau, *Journal Asiatique* 1853, p. 121.