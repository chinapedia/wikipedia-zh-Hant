**神嘉**（525年十二月—535年三月）是[北魏時期領導](../Page/北魏.md "wikilink")[劉蠡升的年号](../Page/劉蠡升.md "wikilink")，共计9年餘。劉蠡升是建立[石平年號的](../Page/石平.md "wikilink")[劉沒鐸之](../Page/劉沒鐸.md "wikilink")[祖父](../Page/祖父.md "wikilink")。

## 大事记

## 出生

## 逝世

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|神嘉||元年||二年||三年||四年||五年||六年||七年||八年||九年||十年||十一年 |-
style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||525年||526年||527年||528年||529年||530年||531年||532年||533年||534年||535年
|- style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[乙巳](../Page/乙巳.md "wikilink")||[丙午](../Page/丙午.md "wikilink")||[丁未](../Page/丁未.md "wikilink")||[戊申](../Page/戊申.md "wikilink")||[己酉](../Page/己酉.md "wikilink")||[庚戌](../Page/庚戌.md "wikilink")||[辛亥](../Page/辛亥.md "wikilink")||[壬子](../Page/壬子.md "wikilink")||[癸丑](../Page/癸丑.md "wikilink")||[甲寅](../Page/甲寅.md "wikilink")||[乙卯](../Page/乙卯.md "wikilink")
|}

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [普通](../Page/普通_\(萧衍\).md "wikilink")（520年正月—527年三月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [大通](../Page/大通_\(萧衍\).md "wikilink")（527年三月—529年九月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [中大通](../Page/中大通.md "wikilink")（529年十月—534年十二月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [大同](../Page/大同_\(萧衍\).md "wikilink")（535年正月—546年四月）：[南朝梁政權梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [孝昌](../Page/孝昌.md "wikilink")（525年六月—528年正月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝明帝元詡年号](../Page/北魏孝明帝.md "wikilink")
      - [武泰](../Page/武泰.md "wikilink")（528年正月—四月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝明帝元詡年号](../Page/北魏孝明帝.md "wikilink")
      - [建義](../Page/建義_\(北魏孝莊帝\).md "wikilink")（528年四月—九月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝莊帝元子攸年号](../Page/北魏孝莊帝.md "wikilink")
      - [永安](../Page/永安_\(北魏孝莊帝\).md "wikilink")（528年九月—530年十月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝莊帝元子攸年号](../Page/北魏孝莊帝.md "wikilink")
      - [孝基](../Page/孝基.md "wikilink")（529年四月—五月）：[北魏政权北海王](../Page/北魏.md "wikilink")[元顥年号](../Page/元顥.md "wikilink")
      - [建武](../Page/建武_\(元顥\).md "wikilink")（529年五月—閏六月）：[北魏政权北海王](../Page/北魏.md "wikilink")[元顥年号](../Page/元顥.md "wikilink")
      - [更興或](../Page/更兴.md "wikilink")[更新](../Page/更新.md "wikilink")（530年六月—532年十二月）：[北魏政权](../Page/北魏.md "wikilink")[汝南王](../Page/汝南.md "wikilink")[元悅年号](../Page/元悅.md "wikilink")
      - [建明](../Page/建明_\(元晔\).md "wikilink")（530年十月—531年二月）：[北魏政权](../Page/北魏.md "wikilink")[元晔年号](../Page/元晔.md "wikilink")
      - [普泰](../Page/普泰.md "wikilink")（531年二月—十月）：[北魏政权](../Page/北魏.md "wikilink")[元恭年号](../Page/元恭.md "wikilink")
      - [中興](../Page/中兴_\(北魏安定王\).md "wikilink")（531年十月—532年四月）：[北魏政权](../Page/北魏.md "wikilink")[安定王元朗年号](../Page/北魏安定王.md "wikilink")
      - [太昌](../Page/太昌.md "wikilink")（532年四月—十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝武帝元修年号](../Page/北魏孝武帝.md "wikilink")
      - [永興](../Page/永兴_\(北魏孝武帝\).md "wikilink")（532年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝武帝元修年号](../Page/北魏孝武帝.md "wikilink")
      - [永熙](../Page/永熙_\(北魏孝武帝\).md "wikilink")（532年十二月—534年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝武帝元修年号](../Page/北魏孝武帝.md "wikilink")
      - [天平](../Page/天平_\(元善見\).md "wikilink")（534年十月—537年十二月）：[東魏政权東魏孝靜帝](../Page/東魏.md "wikilink")[元善見年号](../Page/元善見.md "wikilink")
      - [大統](../Page/大统_\(元宝炬\).md "wikilink")（535年正月—551年十二月）：[西魏政权西魏文帝](../Page/西魏.md "wikilink")[元宝炬年号](../Page/元宝炬.md "wikilink")
      - [天建](../Page/天建.md "wikilink")（524年六月—527年九月）：[北魏時期領導](../Page/北魏.md "wikilink")[莫折念生年号](../Page/莫折念生.md "wikilink")
      - [真王](../Page/真王_\(杜洛周\).md "wikilink")（525年八月—528年二月）：[北魏時期領導](../Page/北魏.md "wikilink")[杜洛周年号](../Page/杜洛周.md "wikilink")
      - [魯興或普興](../Page/魯興.md "wikilink")（526年正月—八月）：[北魏時期領導](../Page/北魏.md "wikilink")[鮮于修禮年号](../Page/鮮于修禮.md "wikilink")
      - [始建](../Page/始建.md "wikilink")：[北魏時期領導](../Page/北魏.md "wikilink")[陳雙熾年号](../Page/陳雙熾.md "wikilink")
      - [廣安](../Page/广安_\(葛荣\).md "wikilink")（526年九月—528年九月）：[北魏時期領導](../Page/北魏.md "wikilink")[葛荣年号](../Page/葛荣.md "wikilink")
      - [天授](../Page/天授_\(刘获\).md "wikilink")（527年七月）：[北魏時期領導](../Page/北魏.md "wikilink")[劉獲](../Page/劉獲.md "wikilink")、[鄭辯年号](../Page/鄭辯.md "wikilink")
      - [隆緒](../Page/隆緒.md "wikilink")（527年十月—528年正月）：[北魏時期領導](../Page/北魏.md "wikilink")[萧宝夤年号](../Page/萧宝夤.md "wikilink")
      - [天統](../Page/天统_\(邢杲\).md "wikilink")（528年六月—529年四月）：[北魏時期領導](../Page/北魏.md "wikilink")[邢杲年号](../Page/邢杲.md "wikilink")
      - [神獸](../Page/神獸.md "wikilink")（528年七月—530年四月）：[北魏時期領導](../Page/北魏.md "wikilink")[万俟丑奴年号](../Page/万俟丑奴.md "wikilink")
      - [皇武](../Page/皇武.md "wikilink")：[北魏時期領導](../Page/北魏.md "wikilink")[劉擧年号](../Page/劉擧.md "wikilink")
      - [上願](../Page/上願.md "wikilink")（535年）：[南朝梁時期領導](../Page/南朝梁.md "wikilink")[鮮于琛的年号](../Page/鮮于琛.md "wikilink")
      - [義熙](../Page/义熙_\(麴嘉\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴嘉年号](../Page/麴嘉.md "wikilink")
      - [甘露](../Page/甘露_\(麴光\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴光年号](../Page/麴光.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:南北朝年号](../Category/南北朝年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:520年代中国政治](../Category/520年代中国政治.md "wikilink")
[Category:530年代中国政治](../Category/530年代中国政治.md "wikilink")