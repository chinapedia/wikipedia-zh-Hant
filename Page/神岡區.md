**神岡區**是[臺灣](../Page/臺灣.md "wikilink")[臺中市的一個](../Page/臺中市.md "wikilink")[市轄區](../Page/市轄區.md "wikilink")。位於[臺中盆地北側](../Page/臺中盆地.md "wikilink")，[大肚山東部](../Page/大肚山.md "wikilink")，[大甲溪南岸](../Page/大甲溪.md "wikilink")；氣候屬[亞熱帶](../Page/亞熱帶.md "wikilink")，氣候溫和。早期有「中部開發中心」之稱，[先人至](../Page/先人.md "wikilink")[豐原](../Page/豐原區.md "wikilink")、[東勢](../Page/東勢區.md "wikilink")、[后里等地開墾](../Page/后里區.md "wikilink")，都需經過此地，因此留下的[古蹟很多](../Page/古蹟.md "wikilink")\[1\]，使得神岡區近年來逐漸由[農業轉為](../Page/農業.md "wikilink")[觀光業發展](../Page/觀光業.md "wikilink")。

## 歷史

神岡區早期為[客家人所開發](../Page/客家人.md "wikilink")，因此舊稱「**新廣-{庄}-**」，有「新開墾的廣大平原」之意，而「神岡」與「新廣」的[客家語](../Page/客家語.md "wikilink")[發音](../Page/發音.md "wikilink")（Shin-gong）極為相似，有證據顯示可能為語誤造成延用。

清乾隆中葉的台灣番界圖，此地名為「成崗-{庄}-」。至於「神崗-{庄}-」的名稱在嘉慶二十三年買賣契約就有。\[2\]

本區最早原為掌控中部地區的[平埔族群](../Page/平埔族.md "wikilink")[巴則海族大社](../Page/巴則海族.md "wikilink")[岸裡社所在](../Page/岸裡社.md "wikilink")，[康熙](../Page/康熙.md "wikilink")38年（1699年）因助官府平[吞霄社亂崛起](../Page/吞霄社.md "wikilink")，並在康熙末年請墾包括本區在內的廣大土地，是整個臺中地區開拓之始。後[粵人](../Page/粵.md "wikilink")[張達京入臺](../Page/張達京.md "wikilink")，定居於岸裡社，協助岸裡社開墾並鑿水圳灌溉，當時形成的村落有望樓（望寮）、社口（萬興莊）、北庄、浮圳（紅圳頭）等。[清朝](../Page/清朝.md "wikilink")[道光](../Page/道光.md "wikilink")22年（1842年），[泉州人來此構居](../Page/泉州.md "wikilink")，於是逐漸形成泉州人聚落。[清朝](../Page/清朝.md "wikilink")[光緒](../Page/光緒.md "wikilink")13年（1887年）臺灣建省，臺中地區置[臺灣縣](../Page/臺灣縣_\(1887年-1895年\).md "wikilink")、後改隸[臺灣府](../Page/臺灣府.md "wikilink")。

[臺灣割讓予](../Page/臺灣.md "wikilink")[日本後的](../Page/日本.md "wikilink")1901年，神岡分屬[臺中廳葫蘆墩支廳](../Page/臺中廳.md "wikilink")**社口區**（下轄-{大社庄、社口庄、下溪洲庄、三角仔庄、社皮庄、車路墘庄}-）、**神崗區**（下轄-{神崗庄、北庄、圳堵庄、新莊仔庄、山皮庄}-）。1920年[臺灣總督府再將全臺改為五州三廳](../Page/臺灣總督府.md "wikilink")，葫蘆墩支廳的葫蘆墩街改為[豐原街](../Page/豐原街.md "wikilink")、廢除社口區，社皮、車路墘併入豐原街；社口、大社、三角仔、下溪州併入神崗區改為**-{[神岡庄](../Page/神岡庄.md "wikilink")}-**（Kami-oka），隸屬於[臺中州](../Page/臺中州.md "wikilink")[豐原郡](../Page/豐原郡_\(台灣\).md "wikilink")。

[中華民國政府接收](../Page/中華民國政府.md "wikilink")[日本](../Page/大日本帝國.md "wikilink")[臺灣總督府轄區後](../Page/臺灣總督府.md "wikilink")，改為[臺中縣](../Page/臺中縣.md "wikilink")**神岡鄉**。2010年12月25日隨著[臺中縣市合併為直轄市而改制為](../Page/2010年中華民國縣市改制直轄市.md "wikilink")**神岡區**。

## 地理

神岡區東西寬9.01[公里](../Page/公里.md "wikilink")，南北長7.61[公里](../Page/公里.md "wikilink")，[面積總計](../Page/面積.md "wikilink")35.0452[平方公里](../Page/平方公里.md "wikilink")。大部分為[大甲溪沖積而成](../Page/大甲溪.md "wikilink")，為第四紀[沖積層](../Page/沖積層.md "wikilink")，大部份為礫層和礫黏土層合成。大甲溪西南側坡度陡峭，河床面積廣大，為砂磧與沖積岩所組成。

## 行政

### 區公所

神岡區為神岡區公所管轄，神岡區公所隸屬於臺中市政府，官派[區長一人綜理區務](../Page/區長.md "wikilink")，區長下設主任秘書一人，另置秘書及專員各一人。下設民政課、社會課、農業及建設課、公用課、人文課、秘書室、人事室、會計室、政風室九個課室。

### 歷任首長

  - 神岡區長

<!-- end list -->

  - 林維三：1901年－1901年

<!-- end list -->

  - 神岡-{庄}-長

<!-- end list -->

  - 林全福：1920年－1934年
  - 呂季園：1934年－1945年（1940年改名為宮下季三）

<!-- end list -->

  - 神岡鄉長

<!-- end list -->

  - 林秉堃：1945年－1948年
  - 賴振榮：1948年－1951年
  - 林孟義：1951年－1954年7月（民選首任，後因叛亂案遭停職槍決）
      - 劉偉：1954年7月－1955年9月（代理）

<!-- end list -->

  - 張立傑：
  - 王英三：
  - 劉八郎：
  - 陳啟宏：1998年－2006年
  - 王堃棠：2006年－2007年4月（當選無效）
      - 姚明仁：2007年4月－2007年（代理）
  - [羅永珍](../Page/羅永珍.md "wikilink")：2007年－2010年（民選末任）

<!-- end list -->

  - 神岡區長

<!-- end list -->

1.  陳嘉和：2010年－2013年7月18日（原中縣秘書）
2.  陳嘉榮：2013年7月18日－2013年7月29日（原龍井區公所主任秘書）
3.  劉俊信：2013年7月29日－2015年8月24日（原龍井區公所主任秘書）
4.  王基成：2015年8月24日－2019年2月12日（原潭子區公所民政課課長）\[3\]
5.  劉汶軒：2019年2月12日－現任(原神岡公所主任秘書)

### 行政區劃

  - \-{[神岡里](../Page/神岡里.md "wikilink")、[北庄里](../Page/北庄里.md "wikilink")、[庄前里](../Page/庄前里.md "wikilink")、[庄後里](../Page/庄後里.md "wikilink")}-
  - \-{[圳前里](../Page/圳前里.md "wikilink")、[山皮里](../Page/山皮里.md "wikilink")、[圳堵里](../Page/圳堵里.md "wikilink")、[新庄里](../Page/新庄里.md "wikilink")}-
  - \-{[社口里](../Page/社口里.md "wikilink")、[大社里](../Page/大社里.md "wikilink")、[岸裡里](../Page/岸裡里.md "wikilink")、[三角里](../Page/三角里.md "wikilink")、[社南里](../Page/社南里.md "wikilink")}-
  - \-{[溪洲里](../Page/溪洲里.md "wikilink")、[神洲里](../Page/神洲里.md "wikilink")、[豐洲里](../Page/豐洲里.md "wikilink")}-

## 人口

## 經濟發展

本區工業以[製造業為主](../Page/製造業.md "wikilink")，主要從事電子、鐵工廠、木器加工、塑膠等項目，大部分集中在岸裡、
豐洲、社南、新庄等里。商業活動以[中山路為主要據點](../Page/中山路_\(神岡區\).md "wikilink")，以[金融](../Page/金融.md "wikilink")[保險](../Page/保險.md "wikilink")、[不動產服務及消費性店家較為顯著](../Page/不動產.md "wikilink")。近年設有[豐洲科技工業園區](../Page/豐洲科技工業園區.md "wikilink")，朝向精密機械工業發展。

## 教育

  - 國民中學

<!-- end list -->

  - [臺中市立神岡國民中學](../Page/臺中市立神岡國民中學.md "wikilink")
  - [臺中市立神圳國民中學](http://www.szjh.tc.edu.tw)

<!-- end list -->

  - 國民小學

<!-- end list -->

  - [臺中市神岡區神岡國民小學](http://www.skaes.tc.edu.tw/)
  - [臺中市神岡區社口國民小學](http://www.skes.tc.edu.tw/)
  - [臺中市神岡區圳堵國民小學](http://www.zdes.tc.edu.tw/)
  - [臺中市神岡區岸裡國民小學](../Page/臺中市神岡區岸裡國民小學.md "wikilink")
  - [臺中市神岡區豐洲國民小學](http://www.pjes.tc.edu.tw/)

## 交通

### 公路

  -   - [-{台中}-系統交流道](../Page/台中系統交流道.md "wikilink")（165，連接-{台中}-環線）
      - [豐原交流道](../Page/豐原交流道.md "wikilink")（168）

  - [TWHW4.svg](https://zh.wikipedia.org/wiki/File:TWHW4.svg "fig:TWHW4.svg")[-{台中}-環線](../Page/國道四號_\(中華民國\).md "wikilink")

      - [神岡交流道](../Page/神岡交流道.md "wikilink")（9）

  - [TW_PHW10.svg](https://zh.wikipedia.org/wiki/File:TW_PHW10.svg "fig:TW_PHW10.svg")[台10線](../Page/台10線.md "wikilink")

### 大眾運輸

  - 本表更新時間為2018年4月17日。
  - [700路雖有經過神岡區](../Page/台中市公車700路.md "wikilink")，但該路線未在本區設站，故未標示於本表內。

<!-- end list -->

  - [臺中市公車](../Page/臺中市公車.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><p>路線號碼</p></td>
<td><p>營運區間</p></td>
<td><p>經營業者</p></td>
<td><p>備註</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車12路.md" title="wikilink"><font color="red">12</font></a></strong></p></td>
<td><p><a href="../Page/臺中市私立明德高級中學.md" title="wikilink">明德高中</a>－<a href="../Page/國立豐原高級中學.md" title="wikilink">豐原高中</a></p></td>
<td><p><a href="../Page/台中客運.md" title="wikilink">台中客運</a><br />
<a href="../Page/豐原客運.md" title="wikilink">豐原客運</a><br />
<a href="../Page/全航客運.md" title="wikilink">全航客運</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車14路.md" title="wikilink"><font color="red">14</font></a></strong></p></td>
<td><p><a href="../Page/干城站.md" title="wikilink">干城站</a>－舊庄</p></td>
<td><p>台中客運</p></td>
<td><p>經社口</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車14路.md" title="wikilink"><font color="red">14副</font></a></strong></p></td>
<td><p>干城站－神岡區公所</p></td>
<td><p>台中客運</p></td>
<td><p>經大雅</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車91路.md" title="wikilink"><font color="red">91</font></a></strong></p></td>
<td><p>臺中航空站－舊庄－中興嶺停車場</p></td>
<td><p>豐原客運</p></td>
<td><p>每日3個班次由舊-{庄}-端延駛至臺中航空站<br />
每日3個班次由臺中航空站發車<br />
所有班次皆繞駛至豐原東站</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車98路.md" title="wikilink"><font color="red">98</font></a></strong></p></td>
<td><p>神岡新-{庄}-－<a href="../Page/中部科學園區.md" title="wikilink">中部科學園區</a>－<a href="../Page/臺中榮民總醫院.md" title="wikilink">臺中榮總</a></p></td>
<td><p><a href="../Page/東南客運.md" title="wikilink">東南客運</a></p></td>
<td><p>經大雅、中部科學園區</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車98路.md" title="wikilink"><font color="red">98繞</font></a></strong></p></td>
<td><p>神岡新-{庄}-－中部科學園區－臺中榮總</p></td>
<td><p>東南客運</p></td>
<td><p>繞駛至大華國中<br />
例假日及非寒暑假輔導課週停駛</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車152路.md" title="wikilink"><font color="red">152</font></a></strong></p></td>
<td><p><a href="../Page/臺中市議會.md" title="wikilink">臺中市議會</a>－<a href="../Page/臺中市政府陽明大樓.md" title="wikilink">陽明大樓</a></p></td>
<td><p><a href="../Page/中台灣客運.md" title="wikilink">中台灣客運</a></p></td>
<td><p>實際訖站為義士祠</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車152路.md" title="wikilink"><font color="purple">152區</font></a></strong></p></td>
<td><p>臺中市議會－圓環南永安街口</p></td>
<td><p>中台灣客運</p></td>
<td><p>例假日停駛</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車182路.md" title="wikilink"><font color="red">182</font></a></strong></p></td>
<td><p>豐原東站－清水</p></td>
<td><p>豐原客運</p></td>
<td><p>經新-{庄}-</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車183路.md" title="wikilink"><font color="red">183</font></a></strong></p></td>
<td><p>豐原東站－臺中港郵局</p></td>
<td><p>豐原客運</p></td>
<td><p>經新-{庄}-</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車185路.md" title="wikilink"><font color="red">185</font></a></strong></p></td>
<td><p>豐原東站－清水</p></td>
<td><p>豐原客運</p></td>
<td><p>經東山</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車186路.md" title="wikilink"><font color="red">186</font></a></strong></p></td>
<td><p>豐原東站－臺中港郵局</p></td>
<td><p>豐原客運</p></td>
<td><p>經東山</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車218路.md" title="wikilink"><font color="red">218</font></a></strong></p></td>
<td><p>豐原東站－神岡區公所</p></td>
<td><p>豐原客運</p></td>
<td><p>經崎腳</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車220路.md" title="wikilink"><font color="red">220</font></a></strong></p></td>
<td><p>豐原東站－臺中榮總</p></td>
<td><p>豐原客運</p></td>
<td><p>經社口</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車220路.md" title="wikilink"><font color="red">220繞</font></a></strong></p></td>
<td><p>豐原東站－臺中榮總</p></td>
<td><p>豐原客運</p></td>
<td><p>繞駛至大華國中<br />
例假日及非寒暑假輔導課週停駛</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車228路.md" title="wikilink"><font color="red">228</font></a></strong></p></td>
<td><p>大鵬國小－豐原東站</p></td>
<td><p><a href="../Page/豐榮客運.md" title="wikilink">豐榮客運</a></p></td>
<td><p>經社口</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車229路.md" title="wikilink"><font color="red">229</font></a></strong></p></td>
<td><p>豐原東站－<a href="../Page/朝馬.md" title="wikilink">朝馬</a></p></td>
<td><p>豐原客運</p></td>
<td><p>經社口<br />
例假日及寒暑假停駛</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車232路.md" title="wikilink"><font color="red">232</font></a></strong></p></td>
<td><p>豐原東站－東山</p></td>
<td><p>豐原客運</p></td>
<td><p>例假日及寒暑假停駛</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車237路.md" title="wikilink"><font color="red">237</font></a></strong></p></td>
<td><p>豐原東站－<a href="../Page/大肚車站.md" title="wikilink">大肚火車站</a></p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車238路.md" title="wikilink"><font color="red">238</font></a></strong></p></td>
<td><p>豐原東站－臺中港郵局</p></td>
<td><p>豐原客運</p></td>
<td><p>經沙鹿</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車239路.md" title="wikilink"><font color="red">239</font></a></strong></p></td>
<td><p>豐原東站－梧棲</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/台中市公車989路.md" title="wikilink"><font color="red">989</font></a></strong></p></td>
<td><p>翁子－圳前仁愛公園</p></td>
<td><p>豐原客運</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/台中市公車989路.md" title="wikilink"><font color="red">989延</font></a></strong></p></td>
<td><p>翁子－神圳國中</p></td>
<td><p>豐原客運</p></td>
<td><p>經圳前仁愛公園</p></td>
</tr>
</tbody>
</table>

## 景點及文化資產

  - 岸裡社古碑亭（岸裡文物藝術館）
  - [筱雲山莊](../Page/筱雲山莊.md "wikilink")
  - [社口大夫第](../Page/社口大夫第.md "wikilink")（神岡-{庄}-長－[林振芳故居](../Page/林振芳.md "wikilink")）
  - [潭雅神綠園道](../Page/潭雅神綠園道.md "wikilink")（舊臺鐵神岡支線改建）
  - [紅圳頭義民祠](../Page/紅圳頭義民祠.md "wikilink")
  - [慧光寺](../Page/慧光寺.md "wikilink")\[4\]
  - [朝清宮天師廟](../Page/朝清宮天師廟.md "wikilink")
  - \-{[庄後武安宮](../Page/庄後武安宮.md "wikilink")}-（關聖帝君廟）
  - [山皮寶山宮](../Page/山皮寶山宮.md "wikilink")（保生大帝廟）
  - [岸裡文興宮](../Page/岸裡文興宮.md "wikilink")（包府千歲廟）
  - [社口萬興宮](../Page/社口萬興宮.md "wikilink")（天上聖母廟）
  - [圳堵張氏宗祠](../Page/圳堵張氏宗祠.md "wikilink")\[5\]
  - [豐洲公園](../Page/豐洲公園.md "wikilink")
  - [圳堵公園](../Page/圳堵公園.md "wikilink")
  - [臺灣氣球博物館（氣球工廠）](http://www.prolloon.com.tw//)－世界生產量最大的氣球工廠，地址是臺中市神岡區大豐路五段505號。
  - 社口犁記張餅店本舖（岸裡社文化節－四月）
  - 神岡浮圳

[File:豐洲大明宮.JPG|豐洲大明宮奉祀](File:豐洲大明宮.JPG%7C豐洲大明宮奉祀)[關聖帝君](../Page/關聖帝君.md "wikilink")，廟旁為豐洲公園。
[File:神岡先民靈塔.JPG|社口先民靈塔](File:神岡先民靈塔.JPG%7C社口先民靈塔)
[File:圳堵朝清宮.JPG|圳堵地區](File:圳堵朝清宮.JPG%7C圳堵地區)[朝清宮天師廟](../Page/朝清宮天師廟.md "wikilink")
[File:張氏宗祠.JPG|圳堵張氏宗祠－張趨祭祀公業](File:張氏宗祠.JPG%7C圳堵張氏宗祠－張趨祭祀公業)
<File:Shekou> Lin's Mansion.JPG|社口大夫第
<File:Shenkang_Station_Ruins.JPG>|[神岡車站](../Page/神岡車站.md "wikilink")
[File:神岡寶山宮.JPG|位在](File:神岡寶山宮.JPG%7C位在)[山皮里光復路](../Page/山皮里.md "wikilink")，主祀[保生大帝](../Page/保生大帝.md "wikilink")，為[同安縣陳氏先祖創建](../Page/同安縣.md "wikilink")。

## 参考資料

## 外部連結

  - [神岡區公所](http://www.shengang.taichung.gov.tw/)

[山線](../Category/臺中市行政區劃.md "wikilink")
[神岡區](../Category/神岡區.md "wikilink")

1.  [神岡區公所-歷史沿革](http://www.shengang.gov.tw/main-ch/about/about.html)
2.
3.  [臺中市神岡區公所區長交接暨就職典禮](http://www.shengang.taichung.gov.tw/ct.asp?xItem=1533209&ctNode=1821&mp=145010)
4.  [臺灣寺廟網－ 慧光寺](http://twgod.com/CwP/P/P3817.html)
5.  [張氏－社區小故事](http://www.hometown.org.tw/blog/DayaCulture/myBlogArticleAction.do?method=doListArticleByPk&articleId=22875)