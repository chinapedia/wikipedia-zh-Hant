**神游科技（中国）**是于2002年成立的中国大陆游戏公司，起初是美籍华人科学家、企业家[颜维群博士和日本著名游戏公司](../Page/颜维群.md "wikilink")[任天堂于](../Page/任天堂.md "wikilink")2002年合资成立。现时则已经成为任天堂的全资[子公司](../Page/子公司.md "wikilink")。\[1\]公司先前主要业务是负责[任天堂的主机](../Page/任天堂.md "wikilink")、游戏软件在中国大陆地区的本地化、发行、销售维修。任天堂2017年度的财报显示神游科技的职能已从销售转向研发。\[2\]

## 主要产品

### 硬件

  - [神游机](../Page/神游机.md "wikilink")，即[任天堂64的中国大陆改版行货](../Page/任天堂64.md "wikilink")
  - [小神游GBA](../Page/小神游GBA.md "wikilink")，中国大陆行货版[Game Boy
    Advance](../Page/Game_Boy_Advance.md "wikilink")
  - [小神游SP](../Page/小神游SP.md "wikilink")，中国大陆行货版[Game Boy Advance
    SP](../Page/Game_Boy_Advance_SP.md "wikilink")
  - [iQue
    DS](../Page/iQue_DS.md "wikilink")，中国大陆行货版[任天堂DS](../Page/任天堂DS.md "wikilink")
  - [iQue Gameboy
    Micro](../Page/iQue_Gameboy_Micro.md "wikilink")，中国大陆行货版[Game
    Boy Micro](../Page/Game_Boy_Micro.md "wikilink")
      - [iQue MP4
        影音播放君](../Page/播放君.md "wikilink")，中国大陆行货版[播放君](../Page/播放君.md "wikilink")
  - [iQue DS Lite](../Page/iQue_DS_Lite.md "wikilink")，中国大陆行货版[任天堂DS
    Lite](../Page/任天堂DS_Lite.md "wikilink")
  - [iQue
    DSi](../Page/iQue_DSi.md "wikilink")，中国大陆行货版[任天堂DSi](../Page/任天堂DSi.md "wikilink")
  - [iQue 3DS XL](../Page/iQue_3DS_XL.md "wikilink")，中国大陆行货版[任天堂3DS
    XL](../Page/任天堂3DS_XL.md "wikilink")

### 软件

## 参考资料

## 外部链接

  -
  -
[Category:2002年開業電子遊戲公司](../Category/2002年開業電子遊戲公司.md "wikilink")
[Category:任天堂的部门与子公司](../Category/任天堂的部门与子公司.md "wikilink")
[Category:中国大陆电子游戏公司](../Category/中国大陆电子游戏公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[神游科技](../Category/神游科技.md "wikilink")

1.  <http://www.nintendo.co.jp/ir/pdf/2013/security_q1303.pdf#page=5>
2.