**神經內科**（neurology）是[醫學的一個分支](../Page/醫學.md "wikilink")，專門應付神經系統的疾病，但不包括手術（其相對應的外科為[神經外科](../Page/神經外科.md "wikilink")）。\[1\]
作為[應用科學的一種](../Page/應用科學.md "wikilink")，其與神經外科均有別於隸屬自然科學的[神經科學](../Page/神經科學.md "wikilink")。前者負責在病人身上運用已有知識，而後者則專門研究與發現新的科學概念。

## 神經失調

神經失調是失調影響中央神經系統、周邊神經系統、或位於中央和周邊神經系統的自主神經系統。常見情況包括：

1.  頭疼失調,譬如[偏頭痛和緊張頭疼](../Page/偏頭痛.md "wikilink")（群族頭疼）
2.  情景引發的失調，譬如[癲癇症](../Page/癲癇症.md "wikilink")
3.  神經退化性失調，例如[老年癡呆](../Page/老年癡呆.md "wikilink")，包括[阿茲海默病](../Page/阿茲海默病.md "wikilink")
4.  腦血管的疾病，譬如瞬變局部缺血的攻擊和衝程
5.  [失眠](../Page/失眠.md "wikilink")
6.  [大腦痲痺](../Page/大腦痲痺.md "wikilink")
7.  [中央神經系統](../Page/中央神經系統.md "wikilink")（腦炎的）[細菌](../Page/細菌.md "wikilink")、[黴菌](../Page/黴菌.md "wikilink")、[病毒和寄生傳染](../Page/病毒.md "wikilink")，[腦膜炎並且周邊神經](../Page/腦膜炎.md "wikilink")（[神經炎](../Page/神經炎.md "wikilink")）
8.  瘤-
    [腦腫瘤](../Page/腦腫瘤.md "wikilink")、[脊髓腫瘤](../Page/脊髓腫瘤.md "wikilink")、周邊神經的[腫瘤](../Page/腫瘤.md "wikilink")
9.  運動混亂譬如[帕金森氏症的疾病](../Page/帕金森氏症.md "wikilink")、[舞蹈病](../Page/舞蹈病.md "wikilink")、hemiballism
10. 中央神經系統的壞死疾病、譬如多發性硬化症、和周邊神經系統
11. 脊髓神經失調：腫瘤、傳染、精神創傷、畸形（即myelocelle 、meningomyelocelle）
12. 周邊神經失調、肌肉和神經連接點的神經失調
13. 腦部創傷、[脊髓和周邊神經創傷](../Page/脊髓.md "wikilink")
14. [昏迷和有各種各樣的起因的昏迷](../Page/昏迷.md "wikilink")

神經學家負責診斷和對待所有上述條件，除了外科干預外，就成了[神經外科醫師的責任](../Page/神經外科.md "wikilink")，並且在某些情況下更涉及神經放射學家。在一些國家，神經學家的另外法律責任包括為被懷疑的已故患者尋找[腦幹死亡的證據](../Page/腦幹死亡.md "wikilink")，並且發出死亡證書。

## 與精神病學的重疊

雖然許多精神病被認為是神經的失調影響中央神經系統，他們傳統上被分開地分類，並由[精神科醫師](../Page/精神科.md "wikilink")、[臨床心理學家和](../Page/臨床心理學.md "wikilink")[心理治療師處理](../Page/心理治療.md "wikilink")。

但是有強烈的跡象表明神經化學機制在[躁鬱症和](../Page/躁鬱症.md "wikilink")[精神分裂症的發展上](../Page/精神分裂症.md "wikilink")（以實例來說）扮演著一個重要角色。同樣，神經病學疾病常有精神病學表現，這也是[行為神經學](../Page/行為神經學.md "wikilink")（Behavioral
Neurology）暨[神經精神醫學](../Page/神經精神醫學.md "wikilink")（Neuropsychiatry）學者的研究範疇。

## 註釋

## 外部連結

  - [American Academy of Neurology.](http://www.aan.com)
  - [United Council for Neurologic Subspecialties.](http://www.ucns.org)
  - [brainblog: news about our knowledge of the brain and
    behavior.](http://www.neuropsychological.blogspot.com/)

[神經內科](../Category/神經內科.md "wikilink")
[Category:医学院所教科目](../Category/医学院所教科目.md "wikilink")
[Category:医学专业](../Category/医学专业.md "wikilink")
[Category:希腊语外来词](../Category/希腊语外来词.md "wikilink")

1.