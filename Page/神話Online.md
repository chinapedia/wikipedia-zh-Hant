**神話**（Millennium Promise、Return of
Warrior、ROW），或稱**勇士歸來**，是款全[三維的](../Page/三維.md "wikilink")[大型多人線上角色扮演遊戲](../Page/大型多人線上角色扮演遊戲.md "wikilink")（MMORPG），由[全球歡樂數位](../Page/全球歡樂數位.md "wikilink")（Youxiland
Digital Co.,
Ltd.）開發。遊戲團隊共動員了150名[工程師](../Page/工程師.md "wikilink")，花費了3年的時間與[新台幣](../Page/新台幣.md "wikilink")2億元的開發[成本來完成這套作品](../Page/成本.md "wikilink")，並開發最新[3D](../Page/3D.md "wikilink")[遊戲引擎來搭配真實考據自](../Page/遊戲引擎.md "wikilink")[中世紀](../Page/中世紀.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")[騎士盔甲裝備的美術設定](../Page/騎士.md "wikilink")，來建構整個遊戲環境。在臺灣於2002年3月底正式推出PC版本。此遊戲也成為[Xbox官方遴選第一家的Xbox多人線上遊戲及線上服務伺服器平台](../Page/Xbox.md "wikilink")，[CodeFire公司與全球歡樂數位於](../Page/CodeFire.md "wikilink")2002年2月簽署合作備忘錄。2008年2月27日，在臺改由[遊戲橘子代理發行](../Page/遊戲橘子.md "wikilink")；其代理於2009年10月28日結束。2009年12月25日推出全球版，其英文名為**Return
of
Warrior**。在2010年5月1日於中國大陸公開測試，由[北京樂道天元科技代理](../Page/北京樂道天元科技.md "wikilink")。\[1\]

## 遊戲特色

玩家們透過連接至遊戲伺服器來與世界其他玩家進行戰鬥、冒險及成長，可以設定自己的人物角色，神話包含了領土發展的概念，陣營和陣營之間的戰爭，並同時與其他團體建立敵對或是友好的關係，增加了玩家們可以在遊戲中組織屬於自己的公會團體。

主題「R.Y.L」是「Risk Your
Life」的縮寫，意義為「用你的生命為賭注」。若自己人物所屬的勢力可以去攻擊並征服由怪物或其他公會及國家支配的地區，則可在比較寬廣的領域上活動，並且亦可使自己的人物更成長。若在玩家人物所屬勢力所征服的地區之村莊，當自己人物獲得村莊之擁有權時，則玩家人物即可成為「領主」，村莊就可以歸屬為自己的領地。當然，可在自己的領地上做很多投資，使領地富裕，亦可把領地建築為自己的勢力基地。但是，若無法保護來自其他勢力的侵略，則其他勢力就會征服其領地。換句話說，因玩家是用自己人物的生命力以及人物所屬的領域來進行生死戰爭，於是含有「用你的生命為賭注」之意。

## 遊戲版本

  - 神話
  - 神話2
  - 神話Return of Warrior、勇士歸來

## 參考

## 外部連結

  - [全球版官方網站](https://web.archive.org/web/20120227104051/http://www.returnofwarrior.com/index.asp)
  - [中國大陸官方網站](https://web.archive.org/web/20120127172855/http://row.ledoware.com/)

[Category:大型多人在线角色扮演游戏](../Category/大型多人在线角色扮演游戏.md "wikilink")
[Category:2001年电子游戏](../Category/2001年电子游戏.md "wikilink")

1.  <http://www.98173.com.tw/Forum/Forum_Page.aspx?tid=69365>