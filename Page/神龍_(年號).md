**神龍**（705年正月—707年九月）是[武则天和](../Page/武则天.md "wikilink")[唐中宗李显的年号](../Page/唐中宗.md "wikilink")。神龍元年二月中宗復[国号唐](../Page/国号.md "wikilink")。

## 大事记

  - 长安五年正月廿三甲辰（705年2月21日），[武则天病重](../Page/武则天.md "wikilink")，以[张柬之](../Page/张柬之.md "wikilink")、[敬暉](../Page/敬暉.md "wikilink")、[崔玄暐](../Page/崔玄暐.md "wikilink")、[袁恕己](../Page/袁恕己.md "wikilink")、[桓彥範为首的大臣们抓住时机](../Page/桓彥範.md "wikilink")，联合羽林军，发动了政变，杀死了武则天宠幸的[张易之](../Page/张易之.md "wikilink")、[张昌宗兄弟](../Page/张昌宗.md "wikilink")，[太子顯](../Page/唐中宗.md "wikilink")[監國](../Page/監國.md "wikilink")，是為[神龙革命](../Page/神龙革命.md "wikilink")。
  - 神龙元年正月廿五丙午（705年2月23日），唐中宗[復辟](../Page/復辟.md "wikilink")，武则天[退位](../Page/逊位.md "wikilink")。
  - 神龙元年十一月壬寅（705年12月16日），武则天去世。
  - 神龙二年（706年），为唐中宗复位立下大功的张柬之等人被[武三思陷害](../Page/武三思.md "wikilink")，流放远州，后被杀害。
  - 神龙三年九月初五庚子（707年10月5日），改元[景龙](../Page/景龙.md "wikilink")。

## 出生

## 逝世

神龍元年

●[武则天](../Page/武则天.md "wikilink")

## 纪年

| 神龍                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 705年                           | 706年                           | 707年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") |

## 參看

  - [中国年号列表](../Page/中国年号列表.md "wikilink")
  - 同期存在的其他政权年号
      - [慶雲](../Page/慶雲_\(文武天皇\).md "wikilink")（704年五月十日—708年正月十一日）：[飛鳥時代](../Page/飛鳥時代.md "wikilink")[文武天皇](../Page/文武天皇.md "wikilink")、[元明天皇年號](../Page/元明天皇.md "wikilink")

## 參考文獻

  - 松橋達良，《》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:武周年号](../Category/武周年号.md "wikilink")
[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:8世纪中国年号](../Category/8世纪中国年号.md "wikilink")
[Category:700年代中国政治](../Category/700年代中国政治.md "wikilink")