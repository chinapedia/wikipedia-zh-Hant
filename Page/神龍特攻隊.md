**神龍特攻隊**（原文:M.A.S.K.）是部放於1985年至1986年間播出的美日合作的[動畫影集](../Page/動畫.md "wikilink")，一共75集分為兩季，第一季有65集，第二季度10集。[香港](../Page/香港.md "wikilink")[無線電視於](../Page/無線電視.md "wikilink")1987年開始逢星期六播映．
[台灣](../Page/台灣.md "wikilink")[台視於](../Page/台視.md "wikilink")1987年（[民國76年](../Page/民國76年.md "wikilink")）7月1日至1988年（[民國77年](../Page/民國77年.md "wikilink")）12月21日以《-{神龍特攻隊}-》之名播出，於每週三下午5:00\~5:30（1987年）、5:30\~6:00（1988年）播出；並無按照原來的順序放映，不過仍有明顯地把兩季各自的集數區分出來。中国大陆則在1994年由[辽宁人民艺术剧院翻译](../Page/辽宁人民艺术剧院.md "wikilink")，并且譯为《蒙面鬥士》，在全国范围内盛行达五六年之久。

## 概要

本片描写了由百万富翁-{zh-hans:麦特.德拉克;zh-hk:麥德加;zh-tw:查麥特;}-（Matt
Trakker）成立的“-{zh-hans:机动装甲突击队; zh-hant:神龍特攻隊;}-”（**M**obile
**A**rmoured **S**trike **K**ommand，既缩写的-{zh-hans:“蒙面斗士”（M.A.S.K.);
zh-hant:M.A.S.K.;}-），在全世界范围内惩恶扬善，打击犯罪的故事。他们的夙敌是以-{zh-hans:迈尔斯.麦哈姆;
zh-hant:壞博士;}-（Miles Mayhem）为首的“-{zh-hans:黑帮;
zh-hant:蜘蛛黨（按原意應譯為：毒蛇）;}-”（**V**ery
**E**vil **N**etwork **O**f **M**ayhem，缩写V.E.N.O.M.）。

本片最大的看点就是片中正反双方各自成员所驾驶的车辆，每一辆都可以由看似普通交通工具的普通状态变形成为满载武器和装备的特殊战斗状态。还有就是他们各自戴的特种头盔/面具，每一顶都是高科技的产物，而且有不同的功能。

在每一集中，反方“-{zh-hans:黑帮;
zh-hant:蜘蛛黨;}-”总是会偷窃某些机密或贵重物品或实验某些新式武器装备，以达到自己不可告人的目的。但是每次“-{zh-hans:蒙面斗士;
zh-hant:神龍特攻隊;}-”都会闻风而动，把他们的邪恶计划扼杀在摇篮裡。

在1985-1986年间，美国KENNER玩具公司曾根据片中人物和车辆形象制作了一系列玩具，包括公司自创的。在随玩具赠送的系列漫画裡说-{zh-hans:迈哈姆;
zh-hant:壞博士;}-曾经帮助“-{zh-hans:德拉克; zh-hant:麥特;}-”创建了“-{zh-hans:麦特.德拉克组织;
zh-hant:查麥特基金會;}-”，不过后来背叛了他。

本片機械設定設計師荒牧伸志認為很多美國人沒有意識到本片是有日本人設計機械設定。

## 出場角色&機組

### 神龍特攻隊（M.A.S.K.）

#### \-{zh-hans:麦特.德拉克; zh-hk: 麥德加; zh-hant:查麥特;}-（Matt Trakker）

  -
    **-{zh-hans:蒙面斗士; zh-hk: 幻變奇兵;
    zh-hant:神龍特攻隊;}-**隊長，也是创始人之一。漫画中说他和弟弟安迪和后来的夙敌-{zh-hans:麦哈姆;
    zh-hant:壞博士;}-一起成立了“-{zh-hans:蒙面斗士; zh-hk: 幻變奇兵;
    zh-hant:神龍特攻隊;}-”。配音演员为Doug Stone。香港配音員:
    [盧國權](../Page/盧國權.md "wikilink")。他驾驶的车辆如下：

<!-- end list -->

  - **-{zh-hans:雷鹰; zh-hk: 雷電鷹; zh-hant:蒼鷹號;}-**（Thunderhawk）

<!-- end list -->

  -
    红色 [Chevrolet Camaro](../Page/Chevrolet_Camaro.md "wikilink") G3轿车,
    可以变形成一架海鸥翼式战斗机。 使用面具為“-{zh-hans:流光炮; zh-hant:光譜面具; zh-hk:電光面具}-
    ”，可以发射各种能量束，而且可以用非可见光扫描周遭环境。

<!-- end list -->

  - **-{zh-hans:犀牛; zh-hk: 大犀牛; zh-hant:大犀牛;}-**（Rhino）

<!-- end list -->

  -
    红色半掛车（集装箱车车头），可变形成为一座机动指挥中心和武器平台。配套面具为“-{zh-hans:雷光炮;
    zh-hant:Ultra-Flash面具;}-”。

<!-- end list -->

  - **-{zh-hans:火山; zh-hk: 活火山; zh-hant:火山號;}-**（Volcano）

<!-- end list -->

  -
    黑色大车轮小巴，可变形成为一辆装甲越野车。配套面具为可以发射融化金属的“-{zh-hans:熔岩炮; zh-hant:Lava
    Shot面具;}-”。

<!-- end list -->

  - **-{zh-hans:巨人; zh-hant:大甲蟲;}-一號**（Goliath I）

<!-- end list -->

  -
    一辆可以从拖车**-{zh-hans:巨人; zh-hant:大甲蟲;}-二號**（Goliath II）
    上出发使用的方程式赛车，可以变形为一架战斗机。配套面具为“-{zh-hans:黑雾炮;
    zh-hant:Shroud面具;}-”，可以让周围环境瞬间变成一片黑暗。

#### \-{zh-hans:布魯斯.佐藤; zh-hk: 西圖; zh-hant:魯斯;}-（Bruce Sato）

  -
    一位機械和玩具設計專家，美籍日本人，同时也是麦特.德拉克队伍中举足轻重的二号人物。他经常用谚语来解释他要说的某些事情，而且似乎只有麦特本人能完全理解他这些谚语的意思。香港配音員:
    [馮錦堂](../Page/馮錦堂.md "wikilink")。他驾驶的车辆为:

<!-- end list -->

  - **-{zh-hans:犀牛; zh-hk:大犀牛; zh-hant:大犀牛;}-**（Rhino）

<!-- end list -->

  -
    红色半掛车（集装箱车车头），可变形成为一座机动指挥中心和武器平台。配套面具为“-{zh-hans:环光炮; zh-hk:起重者面具;
    zh-hant:Lifter面具;}-”，可以产生反引力能量场。

#### \-{zh-hans:亚利克斯.塞克托; zh-hk: 薩德; zh-hant:大鬍子;}-（Alex Sector）

  -
    一位電腦及通訊專家。平常职业为兽医。香港配音員: [馮永和](../Page/馮永和.md "wikilink")。他驾驶的车辆有：

<!-- end list -->

  - **-{zh-hans:犀牛; zh-hant:大犀牛;}-**（Rhino）

<!-- end list -->

  -
    他在此车辆中担任副驾驶，配套面具为“-{zh-hans:飞行衬衫; zh-hk: 長腿兔面具; zh-hant:Jack
    Rabbit;}-”，可以让他跳过/飞过很大的-{zh-hans:跨; zh-hant:弧;}-度。

<!-- end list -->

  - ***The Collector***

#### \-{zh-hans:达斯蒂.哈耶; zh-hk: 希斯; zh-hant:大廚師;}-（Dusty Hayes）

  -
    一位水路车辆特级驾驶员，队伍中的地形专家兼跟踪及爆破专家。平常职业为厨师。 配音演员为 Doug Stone。香港配音員:
    [張炳強](../Page/張炳強.md "wikilink")。他驾驶的车辆为：

<!-- end list -->

  - **-{zh-hans:鳄鱼; zh-hk: 海龍; zh-hant:海豹;}-**（Gator）

<!-- end list -->

  -
    一辆[Jeep CJ7](../Page/Jeep_CJ#CJ-7.md "wikilink")
    敞篷吉普车，车体打开可以放出一小型快艇。配套面具为“-{zh-hk:真空面具;
    zh-hant:Backlash;}-”，可以用冲击波来攻击。

#### \-{zh-hans:格罗莉娅.贝克; zh-hk:碧嘉; zh-hant:小莉;}-（Gloria Baker）

  -
    一位賽車冠軍、功夫黑帶高手。配音演员为 by Sharon Noble.
    香港配音員：[黃麗芳](../Page/黃麗芳.md "wikilink")。

<!-- end list -->

  - **-{zh-hans:鲨鱼; zh-hk:大白鯊; zh-hant:小金魚;}-**（Shark）

<!-- end list -->

  -
    一辆保时捷928跑车，可以变形成为一艘潜水艇。配套面具为“-{zh-hans:电光炮;
    zh-hant:Aura;}-”，产生的能量场可以吸收一切外力。（注：官方玩具版从来未曾上市,但近年有不少自製版本在網路上販售,除了車輛有跟原著般的外型與變形功能,也附帶同造型的人偶與面具,就連外盒與說明書都仿造的如同官方發售格式）

#### \-{zh-hans:布拉德.特纳; zh-hk:杜拿; zh-hant:吉他手;}-（Brad Turner）

  -
    一位摩托車及直升機專家，平時的身分為[搖滾樂手](../Page/搖滾.md "wikilink"). 配音演员为Graeme
    McKenna. 香港配音員: [黃健強](../Page/黃健強.md "wikilink")。

<!-- end list -->

  - **-{zh-hans:神鹰; zh-hk:飛鷹; zh-hant:飛天戰神;}-**（Condor）

<!-- end list -->

  -
    一辆摩托车，可以变形为一架直升飞机。配套面具为“-{zh-hans:迷光炮; zh-hant:幻影投射;
    zh-hk:魔術面具}-”，可以发射[全息影象](../Page/全息摄影.md "wikilink")。

<!-- end list -->

  - **-{zh-hans:长须鲸; zh-hant:大鯨魚;}-**（Razorback）

<!-- end list -->

  -
    一辆Daytona赛车，可以变形为一坐机动武器平台。配套面具为“-{zh-hans:黑光炮;
    zh-hant:Eclipse;}-”，可以让某一地带暂时成为不透光的黑暗。

#### \-{zh-hans:洪达.麦克林; zh-hk:麥奇連; zh-hant:黑皮;}-（Hondo MacLean）

  -
    一位武器兼战术专家；平時的身分为历史教师。 配音演员为Doug Stone。香港配音員:
    [梁耀昌](../Page/梁耀昌.md "wikilink")。他驾驶的车辆有：

<!-- end list -->

  - **-{zh-hans:烟火; zh-hk: 紅炮仗(頭一輯曾譯為「霹靂雷」);
    zh-hant:火花號;}-**（Firecracker）

<!-- end list -->

  -
    一辆客货两用车，可变形成为一部小型机动武器平台。配備「爆炸者面具」。He uses the **Blaster** mask which
    fires a destructive energy beam.

<!-- end list -->

  - '''-{zh-hans:旋風; zh-hk: 霹靂雷; zh-hant:旋風戰車;}- '''（Hurricane）

<!-- end list -->

  -
    \-{zh-hans:洪达.麦克林; zh-hk:麥奇連; zh-hant:黑皮;}-在第二十九集“-{zh-hans:烟火;
    zh-hk: 紅炮仗; zh-hant:火花號;}-”被毁之后得到的新车。一辆1957年款 Chevy
    轿车，可以变形为一辆六轮装甲车。配備「爆炸者2號面具」。玩具版人物的衣著與電視版不同，電視版仍穿著駕駛火花號時所穿的衣服。

<!-- end list -->

  - **Sea Attack**

<!-- end list -->

  -
    劇中未登場；只有於歐洲販售玩具商品。

<!-- end list -->

  - **Ramp-Up**

<!-- end list -->

  -
    劇中未登場；平時為維修用升高台，變形後可成為有兩管機槍與中央大砲的移動平台，但僅有商品照片卻未能發售。

#### \-{zh-hans:巴迪.霍克斯; zh-hk: 霍斯; zh-hant:機械師;}- （Buddy "Clutch" Hawks）

一位伪装高手兼情报搜查员，平时职业为机械师。香港配音員: [林保全](../Page/林保全.md "wikilink")。他的车辆为：

  - **-{zh-hans:烟火; zh-hk:紅炮仗; zh-hant:火花號;}-**（Firecracker）

他是这辆车的副驾驶。配套面具为“-{zh-hans:穿光炮; zh-hk:滲透者面具;
zh-hant:Penetrator;}-”，可以让自己穿越任何固体物质。

  - **野貓**（Wildcat）

一辆紧急拖救车，可以变形为一作机动武器平台，配套面具为“-{zh-hans:沟光炮;
zh-hant:Ditcher;}-”，可以在地面上剜出坑洼和沟壕来。

#### \-{zh-hans:卡罗恩.彭斯; zh-hant:牛仔;}-（Calhoun Burns）

  - **-{zh-hans:烏鴉; zh-hant:大烏鴉;}-**

一辆黑色跑车，可以变形为一架水上飞机或快艇。

#### 傑克（Jacques LeFleur）

  - **火山號**（Volcano）

<!-- end list -->

  - **Glider Strike**

<!-- end list -->

  - **小精靈（Detonator）**

#### 勞柏博士(Julio Lopez)

  - **小跳蚤**（Firefly）

<!-- end list -->

  -
    載具模式為橘色輕量沙灘車，兩側變形後成為飛行載具，可空投炸彈

<!-- end list -->

  - ***Fireforce***

#### Riker

  - **小彈弓**(Slingshot)

<!-- end list -->

  -
    載具模式為白色的RV露營車，變形後車體左右打開並升起紅色的小型飛機與其旋轉發射台，且發射台也具備攻擊砲管功能，另車頭處也會翻出兩挺砲管

<!-- end list -->

  - **Meteor**

#### 大光頭（Boris "The Czar" Bushkin）

  - **大公牛**（Bulldog）

#### 印地安（"Chief" Nevada Rushmore）

  - **大甲蟲2號（Goliath II）**

#### 阿里（Ali Bombay）

  - **Bullet**

\----

#### \-{zh-hans:查小皮; zh-hk: 史葛; zh-hant:查小皮;}-（Scott Trakker）

麥德加兒子。香港配音員：[方煥蘭](../Page/方煥蘭.md "wikilink")。

#### \-{zh-hans:小奇; zh-hk: T仔; zh-hant:小奇;}-（T-Bob）

會說話的機械人，常伴於史葛身邊，可變形成電單車供史葛駕駛。香港配音員：[馮志潤](../Page/馮志潤.md "wikilink")。

### \-{zh-hans:蜘蛛黨; zh-hk: 萬毒魔黨; zh-hant:蜘蛛黨;}-（V.E.N.O.M.）

#### \-{zh-hans:壞博士; zh-hk: 米亨; zh-hant:壞博士;}-（Miles Mayhem）

  -
    \-{zh-hans:蜘蛛黨; zh-hk: 萬毒魔黨;
    zh-hant:蜘蛛黨;}-首領。香港配音員：[張英才](../Page/張英才.md "wikilink")。壞博士是
    MASK 早期創立的成員之一，但他企圖以 MASK 的高科技力量征服世界，所以偷走了 MASK
    一半的機體和面罩，並且殺害了查麥特的弟弟，自己成立了
    VENOM。

<!-- end list -->

  - **-{zh-hans:惡狼戰機; zh-hk: 血光刀; zh-hant:惡狼戰機;}-**（Switchblade）

<!-- end list -->

  -
    壞博士的座機。載具模式為直升機型態，變形後則是一架超音速戰鬥機；擁有兩具能量砲及一枚特大號的飛彈，火力非常強大，配套頭盔的能力為發射腐蝕性毒液

<!-- end list -->

  - ***Outlaw***

<!-- end list -->

  -

<!-- end list -->

  - ***Venom's Revenge***

<!-- end list -->

  -

<!-- end list -->

  - **禿鷹號**（Buzzard）

<!-- end list -->

  -
    載具模式為紫黑色一級方程式賽車，變形攻擊模式時一分為三，左右兩側變為獨立的雙輪攻擊車，分別由壞博士與其雙胞胎兄弟駕駛(壞博士操作右方車體)；特別處是中央部分變為攻擊飛機，且駕駛者為壞博士製作的機械人Pilot
    droid(當時的玩具僅將它的頭部做出在車體上，且藏在駕駛安全帽下面)

<!-- end list -->

  - ***Wolfbeast***

<!-- end list -->

  -

#### \-{zh-hans:黑眼鏡; zh-hk: 雷士; zh-hant:黑眼鏡;}-（Sly Rax）

香港配音員：[馮錦倫](../Page/馮錦倫.md "wikilink")(前期)、[林國雄](../Page/林國雄.md "wikilink")(後期)。

  -

<!-- end list -->

  - **-{zh-hans:食人鯊; zh-hk: 大惡鯊; zh-hant:食人鯊;}-**（Piranha）

<!-- end list -->

  -
    載具模式為右方掛有邊車的紫色摩托車，變形攻擊模式時邊車前方伸出砲管；且邊車可分離成為小型潛水艦

<!-- end list -->

  - ***Pitstop Catapult***

#### \-{zh-hans:獨眼龍; zh-hk: 狄加; zh-hant:獨眼龍;}-（Cliff Dagger）

香港配音員：[周鸞](../Page/周鸞.md "wikilink")。

  - **-{zh-hans:魔鬼戰車; zh-hk: 黑豹; zh-hant:魔鬼戰車;}-**（Jackhammer）

<!-- end list -->

  -
    載具模式為黑色的Ford Bronco休旅車，變為攻擊模式時車前會打開出現機關槍，後方車廂也會昇起變為可搭乘操作的迴轉機槍塔

<!-- end list -->

  - **Thunderball**

<!-- end list -->

  -
    平時為紅色圓形儲存槽，變形後圓體打開左右各現出兩挺機槍，中央支撐桿翻起變成防空炮管，但僅有商品照片卻未能發售

#### \-{zh-hans:紅髮女; zh-hk: 雲妮莎; zh-hant:紅髮女;}-（Vanessa Warfield）

香港配音員：[袁淑珍](../Page/袁淑珍.md "wikilink")

  -

<!-- end list -->

  - **飛魚號**（Manta）

<!-- end list -->

  -

#### \-{zh-hans:瘋狗;zh-hant:瘋狗;}- (Bruno "Mad Dog" Sheppard)

  -
    瘋狗是個徒手戰鬥專家，而且擅長綁架，是 VENOM 組織裡最兇殘且最具破壞性的成員。

<!-- end list -->

  - **毒蠍子**（Stinger）

<!-- end list -->

  -
    未變形前是一台改裝版的 Pontiac
    GTO，變成武裝模式後則是一輛履帶車，在擋風玻璃位置多了一管主砲，車尾還有一組機械鉗爪，配套頭盔能力是發射磁性光束，牽引金屬物品

<!-- end list -->

  - **Racing Arena**

<!-- end list -->

  -

<!-- end list -->

  - **Barracuda**

<!-- end list -->

  -

#### Nash "Goon" Gorey

  -

<!-- end list -->

  - ***Outlaw***

<!-- end list -->

  -

#### Lester "The Lizard" Sludge

  -

<!-- end list -->

  - **大蜥蜴**（Iguana）

<!-- end list -->

  -
    載具模式為黑紅雙色的全地形車，變為攻擊模式後車頭翻開出現雙砲管與圓形鋸，車後也會放出圓形鋸齒

#### 金毛虎（Floyd "Birdman" Malloy）

  - **戰鬥蜻蜓**（Vampire）

<!-- end list -->

  -
    載具模式為紅色的旅行機車，變攻擊模式後成為小型飛行機，車頭處下移現出機槍，車側後兩旁旅行置物箱翻開成為機翼，以及現出噴射引擎與飛彈發射器

<!-- end list -->

  - ***Vandal***

<!-- end list -->

  -

#### 惡博士（Maximus Mayhem）

  -
    壞博士的雙胞胎兄弟，特徵是左眼有帶著單邊眼鏡

<!-- end list -->

  - **禿鷹號**（Buzzard）

<!-- end list -->

  -
    載具模式為紫黑色一級方程式賽車，變形攻擊模式時一分為三，左右兩側變為獨立的雙輪攻擊車，而惡博士搭乘操作左方車體

### 其他玩具

  - **Boulder Hill**
  - **Laser Command**
  - ***Hornet***
  - **Ratfang**

## 劇集列表

  - （）內為[台視播出時之標題](../Page/台視.md "wikilink")

### 第一季

1.  The Deathstone - VENOM steals a strange meteorite with healing
    powers using *Switchblade* disguised as a UFO.
2.  The Star Chariot （**星際戰車**）
3.  The Book of Power （**神秘聖經**）
4.  **Highway to Terror** - MASK tries to retrieve a stash of military
    plutonium that VENOM plans to use to power an earthquake machine.
5.  Video VENOM　（**催眠電視**）
6.  Dinosaur Boy （**蜥蜴人**）
7.  **The Ultimate Weapon** - VENOM obtains a device that jams the
    control systems of MASK's vehicles rendering their opposition
    immobile.
8.  The Roteks （**食鐵蟲**）
9.  **The Oz Effect** - VENOM uses a strange vortex machine to abduct a
    village of Australian aborigines and forces them as slaves to mine
    valuable crystal.
10. Death from the Sky （**彗星炸彈**）
11. The Magma Mole （**鐵甲鑽地鼠**）
12. Solaria Park （**太陽能遊樂場**）
13. The Creeping Terror （**超級大毛蟲**）
14. Assault on Liberty （**自由女神失蹤記**）
15. The Sceptre of Rajim （**羅金寶杖**）
16. The Golden Goddess （**黃金女神**）
17. Mystery of the Rings （**神秘戒指**）
18. Bad Vibrations （**復仇者**）
19. Ghost Bomb （**金頭腦**）
20. Cold Fever （**病毒攻勢**）
21. Madri Gras Mystery （**超級燃料**）
22. The Secret of Life （**生命的秘密**）
23. **Vanishing Point** - VENOM hijacks planes by setting up a fake
    airport, jamming radars and confusing pilots to land there.
24. Counter-Clockwise Caper （**倒轉器**）
25. The Plant Show （**巨樹**）
26. Secret of the Andes （**黃金城之謎**）
27. Panda Power （**中國熊貓**）
28. Blackout （**黑蜘蛛**）
29. **A Matter of Gravity** - *Firecracker* is destroyed in a battle
    with VENOM, and Hondo gets a new replacement vehicle, *Hurricane*,
    in the end. (NOTE: *Hurricane* was called *Nightstalker* in this
    episode).
30. The Lost Riches of Rio （**納粹黃金**）
31. Deadly Blue Slime （**藍色大怪物**）
32. The Currency Conspiracy （**變色的鈔票**）
33. Caesar's Sword （**凱撒之劍**）
34. **Peril in Paris** - Buddie Hawks disguises himself as VENOM agent
    Dagger in order to infiltrate VENOM's secret base in Paris. There he
    uncovers VENOM's plan to find a Nazi doomsday machine.
35. In Dutch （**海底玄機**）
36. The Lippizaner Mystery （**盜馬賊**）
37. **The Sacred Rock** - VENOM frightens a tribe of Australian
    aborigines who believe their god "Mimi" is angry with them. When
    T-Bob makes an appearance, the tribe thinks he is their deity.
38. Curse of Solomon's Gorge （**所羅門寶藏**）
39. Green Nightmare （**神龍水晶**）
40. Eyes of the Skull （**透視水晶**）
41. **Stop Motion** - VENOM obtains an EMP bomb, and threatens to loot
    bank vault across the country by knocking out all electronics for
    miles around.
42. The Artemis Enigma （**金鹿角**）
43. **The Chinese Scorpion** - VENOM agent Bruno Sheppard disguises
    *Stinger* as a giant iron scorpion and kidnaps a archaeologist who
    knows the location of buried treasure inside the Great Wall of
    China.
44. Riddle of the Raven Master （**烏鴉雄兵**）
45. The Spectre of Captain Kidd（**黃金砲彈**）
46. The Secret of the Stones （**月光寶石**）
47. **The Lost Fleet** - MASK tries to stop VENOM who goes to a tropical
    island in search of a legendary "golden" fleet.
48. Quest of the Canyon （**尋金記**）
49. Follow the Rainbow （**彩虹的秘密**）
50. The Everglades Oddity （**障眼法**）
51. Dragonfire （**火龍神廟**）
52. The Royal Cape Caper （**小兵立大功**）
53. Patchwork Puzzle （**古董鈔票**）
54. Fog on Boulder Hill （**二手車的秘密**）
55. Plunder of Glowworm Grotto （**紋身的秘密**）
56. Stone Trees （**石頭樹**）
57. Incident in Istanbul （**棋逢敵手**）
58. The Creeping Desert （**沙漠奇談**）
59. The Scarlet Empress （**女王雕像**）
60. Venice Menace （**烏鴉雄兵**）
61. **Treasure of the Nazca Plain** - MASK foils a plot by VENOM to
    steal an ancient South American treasure.
62. Disappearing Act （**消失的戰法**）
63. Gate of Darkness （**黑暗之門**）
64. The Manakara Giant （**巨人之謎**）
65. Raiders of the Orient Express （**東方號快車**）

### 第二季

1.  Demolition Duel to the Death （**兵不厭詐**）
2.  Where Eagles Dare （**合約爭奪戰**）
3.  Homeward Bound （**故鄉保衛戰**）
4.  The Battle of the Giants （**超級配方**）
5.  Race Against Time （**分秒必爭**）
6.  Challenge of the Masters （**勇者之挑戰**）
7.  For One Shining Moment （**惡性難移**\[1\]）
8.  High Noon （**聲東擊西**）
9.  The Battle of Baja （**虎父虎子**）
10. Cliff Hanger （**毒種子**）

## 工作人員

  - 企画：[片山哲生](../Page/片山哲生.md "wikilink")　
  - 人物設定：清山滋崇　
  - 機械設定：[荒牧伸志](../Page/荒牧伸志.md "wikilink")
  - 分鏡：望月敬一郎／[滝沢敏文](../Page/滝沢敏文.md "wikilink")　
  - 作画監督：寺田和男　
  - 原画：小泉謙三／大宅幸男　
  - 攝影：都島雅義／Trans-Arts　
  - 剪輯：逸見俊隆／西出栄子　
  - 音楽：[Haim Saban](../Page/Haim_Saban.md "wikilink")、[Shuki
    Levy](../Page/Shuki_Levy.md "wikilink")
  - 制作協力：KK C\&D Asia／[Studio
    World](../Page/Studio_World.md "wikilink")／[葦プロダクション](../Page/葦プロダクション.md "wikilink")　
  - 製片：[片山哲生](../Page/片山哲生.md "wikilink")

## 漫畫

[台灣知名的餅乾類點心](../Page/台灣.md "wikilink")「[乖乖](../Page/乖乖.md "wikilink")」，也曾經自行繪製本作的彩色連環[漫畫附贈於其內](../Page/漫畫.md "wikilink")。

## 註釋

<div class="references-small">

<references />

</div>

## 相似作品

[Vor-Tech](../Page/Vor-Tech.md "wikilink"), an animated series with a
similar premise to M.A.S.K., aired in the mid-1990s.

### 譯名相近作品

  - [**天**龍特攻隊](../Page/天龍特攻隊.md "wikilink")(A-TEAM)
  - [**飛**龍特攻隊](../Page/飛龍特攻隊.md "wikilink")(Captain Power and the
    Soldiers of the Future)

## 外部連結

  - [台灣電視資料庫](http://tv.nccu.edu.tw/radioProgram_preview.htm?PROGRAMNAME=%AF%AB%C0s%AFS%A7%F0%B6%A4&STATION=&YEAR=1964&MONTH=1&DAY=1&YEAR2=2002&MONTH2=12&DAY2=31&CATALOG=&COUNTRY=&Login=%ACd%B8%DF)
  - [toyMATRIX網頁
    神龍特攻隊](https://web.archive.org/web/20070515005431/http://www.toyhyde.com/mask/mask.htm)
  - [All about
    M.A.S.K](http://mask.running-gag.de/index.lang+english.html)

## 參考資料

[Category:美國動畫影集](../Category/美國動畫影集.md "wikilink")
[Category:日本與海外合作的動畫](../Category/日本與海外合作的動畫.md "wikilink")
[Category:台視外購動畫](../Category/台視外購動畫.md "wikilink")
[Category:面具相關作品](../Category/面具相關作品.md "wikilink")

1.  **惡性難移**（For One Shining
    Moment）為[台灣播出時的最後一集](../Page/台灣.md "wikilink")。