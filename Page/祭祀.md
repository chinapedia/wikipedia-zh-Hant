**祭祀**，指以[線香](../Page/線香.md "wikilink")、[水或](../Page/水.md "wikilink")[肉類等供品向](../Page/肉類.md "wikilink")[神靈](../Page/神靈.md "wikilink")、[聖徒或者](../Page/聖徒.md "wikilink")[亡魂奉獻](../Page/亡魂.md "wikilink")、[祈禱的一種行為](../Page/祈禱.md "wikilink")。

## 内容

祭或祭祀，从内容上包括场地、仪式、[祭文](../Page/祭文.md "wikilink")（祝禱詞）、[祭品](../Page/祭品.md "wikilink")([Sacrifice](../Page/牺牲.md "wikilink"))等内容：

  - [仪式](../Page/仪式.md "wikilink")([Ritual](../Page/仪式.md "wikilink"))，有[祭礼](../Page/祭礼.md "wikilink")、[祭典](../Page/祭典.md "wikilink")；
  - [节日](../Page/节日.md "wikilink")，有祭典、祭日、庙会；
  - 对象，祭[亡灵](../Page/亡灵.md "wikilink")、祭[天地](../Page/天地.md "wikilink")、祭[神灵](../Page/神灵.md "wikilink")（神和世界万物），有[祭祖](../Page/祭祖.md "wikilink")、祭[烈士](../Page/烈士.md "wikilink")、祭[死难者](../Page/死难者.md "wikilink")；
  - 手段，有[活祭](../Page/活祭.md "wikilink")、[牲祭](../Page/牲祭.md "wikilink")，包括[活人祭](../Page/活人祭.md "wikilink")；
  - [祭品](../Page/牺牲.md "wikilink")(Sacrifice)，祭祀用品包括活人、动物和其他祭品；
  - 根据仪式大小分类，有官方祭典([公祭](../Page/公祭.md "wikilink"))、民间祭祀活动包括[家祭](../Page/家祭.md "wikilink")，有[祭饭](../Page/祭饭.md "wikilink")（祭席）、[祭食](../Page/祭食.md "wikilink")；
  - 设施和用具，有[祭祀建筑](../Page/祭祀建筑.md "wikilink")、祭祀用具、祭品。

## 古代祭祀仪式

古代社会，常见的祭祀的仪式主要有[动物祭](../Page/动物祭.md "wikilink")（英语：animal
sacrifice）、[人祭](../Page/人祭.md "wikilink")（英语：human
sacrifice），[杀祭](../Page/杀祭.md "wikilink")（英语：ritual
suicide）、[活人祭](../Page/活人祭.md "wikilink")（英语：ritual murder）。

## 祭神与祭祖在仪式上的区别

虽然同为祭祀，但是祭祀神靈与祭祀祖先，在仪式上并不相同。以[浙江省](../Page/浙江省.md "wikilink")[岱山县的民俗为例](../Page/岱山县.md "wikilink")，祭祀神靈（如年神）时祭桌为东西朝向（即桌缝东西向），而祭祖时南北向；[祭祖时祭桌上摆设较固定](../Page/祭祖.md "wikilink")，朝南或朝大门方向的上香，其它三面各摆四筷四酒四饭，上3\*4共十二碗菜以上，而祭神时，通常是香烛南向，北向并排由桌沿（北）向南摆放6茶6酒。

## 漢字文化圈祭祀

[GZ_FS_Prayers_2.jpg](https://zh.wikipedia.org/wiki/File:GZ_FS_Prayers_2.jpg "fig:GZ_FS_Prayers_2.jpg")\]\]

漢族自古以今保持長期的祭祀文化，而多個與漢族關係密切的民族也繼承漢族的祭祀文化。

[中華人民共和國在](../Page/中華人民共和國.md "wikilink")[文革之後](../Page/文革.md "wikilink")，長期處於祭祀的真空期，至1990年代初才慢慢恢復部分祭祀活動，近年來，很多祭禮又重新恢復，不過在城市生活的人普遍不會主動參加祭祀活動。而[台澎金馬地區則保持了長期的祭祀傳統](../Page/台澎金馬.md "wikilink")，也保留了一套較為完整的祭禮，如[媽祖](../Page/媽祖.md "wikilink")、[觀音媽](../Page/觀音媽.md "wikilink")、[關公](../Page/關公.md "wikilink")、[王爺公](../Page/王爺公.md "wikilink")、[祖師公](../Page/清水祖師.md "wikilink")、[上帝公](../Page/上帝公.md "wikilink")、[土地公](../Page/土地公.md "wikilink")、[太子爺](../Page/太子爺.md "wikilink")、[國姓爺等佛](../Page/鄭成功.md "wikilink")、道教神靈都依照時令節慶祭拜，也有[大成至聖先師的祭祀](../Page/祭孔.md "wikilink")。

東亞各地至今仍保留大量的傳統祭祀。[日本本土](../Page/日本本土.md "wikilink")、[日本琉球](../Page/琉球.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[越南](../Page/越南.md "wikilink")、等除了從中國傳入的中國節日祭祀外，也在當地發展出不少本土文化的祭祀儀式。

## 相關

  - [民間信仰](../Page/民間信仰.md "wikilink")
  - [供品](../Page/供品.md "wikilink")
  - [公媽](../Page/公媽.md "wikilink")
  - [祭祀公業](../Page/祭祀公業.md "wikilink")
  - [拜拜小百科](https://web.archive.org/web/20100308074840/http://www.balu.com.tw/encycloped/encycloped.htm)

[J](../Category/祭祀.md "wikilink")
[Category:民俗學](../Category/民俗學.md "wikilink")
[Category:文化人类学](../Category/文化人类学.md "wikilink")