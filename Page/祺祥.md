**祺祥**\[1\]原本是[清穆宗毅皇帝](../Page/清朝.md "wikilink")[载淳年号](../Page/载淳.md "wikilink")，不過未及改元即被取消。

1861年8月22日，[咸丰帝](../Page/咸丰帝.md "wikilink")（清文宗）在热河[承德去世](../Page/承德.md "wikilink")，兒子载淳在以[肃顺为首的](../Page/肃顺.md "wikilink")[顧命八大臣辅佐下登基](../Page/顧命八大臣.md "wikilink")，9月3日發命，明年改元祺祥。11月2日[東太后慈安](../Page/慈安太后.md "wikilink")、[西太后慈禧聯合](../Page/慈禧太后.md "wikilink")[恭親王](../Page/恭親王.md "wikilink")[奕訢等人](../Page/奕訢.md "wikilink")，發動[祺祥之变](../Page/祺祥之变.md "wikilink")，又稱[辛酉之变](../Page/辛酉之变.md "wikilink")，八大臣或殺或貶，從此[兩宮垂簾](../Page/兩宮垂簾.md "wikilink")，在11月7日下詔，翌年改元为[同治](../Page/同治.md "wikilink")，祺祥年號未及改元即廢除。\[2\]

## 典故出處

來自《[宋史](../Page/宋史.md "wikilink")·乐志》：「不涸不童，誕降祺祥」，

## 其他

  - [臺灣](../Page/臺灣.md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")[新港鄉](../Page/新港鄉.md "wikilink")[板頭村長天宮藏木匾](../Page/板頭村.md "wikilink")，其[落款](../Page/落款.md "wikilink")「祺祥元年壬戌置」，為臺灣唯一使用祺祥年號的[匾額](../Page/匾額.md "wikilink")，是顧命八大臣秉政到被廢之間的期間所造，當時本欲在明年掛上殿宇，其後並未更正為「同治元年壬戌置」，此情況即使在[中國大陸也很罕見](../Page/中國大陸.md "wikilink")\[3\]。

## 參考文獻

{{-}}

[Category:清朝年号](../Category/清朝年号.md "wikilink")
[清](../Category/台灣年號.md "wikilink")
[Category:1861年中国](../Category/1861年中国.md "wikilink")
[Category:19世纪中国年号](../Category/19世纪中国年号.md "wikilink")

1.
2.  乙卯，定年號祺祥。..庚申，詔改祺祥為同治。..甲子，上御太和殿即皇帝位，受朝。頒詔天下，以明年為同治元年
3.