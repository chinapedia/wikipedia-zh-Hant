**禁忌**或**忌諱**是指在一些特定的[文化或是在生活起居中被禁止的行為和思想](../Page/文化.md "wikilink")；如果被禁止的是某些[詞彙或](../Page/詞彙.md "wikilink")[物品的話](../Page/物品.md "wikilink")，則稱為**禁忌語**、**禁忌物**或**禁忌品**。有關的行為或詞彙之所以會被禁止，可能是因為：

  - 不合乎[禮儀](../Page/禮儀.md "wikilink")
  - 具有污辱的涵義
  - 違反[道德](../Page/道德.md "wikilink")[倫理](../Page/倫理.md "wikilink")
  - 觸犯[法律](../Page/法律.md "wikilink")
  - 具有危險性
  - [傳統的](../Page/傳統.md "wikilink")[迷信](../Page/迷信.md "wikilink")

傳統的文化觀念都相信，違反禁忌的行為可能會為[社會帶來破壞和騷亂](../Page/社會.md "wikilink")，也可能會造成人命的傷亡；在法律上，這些打破禁忌的人需要受到制裁。

## 世界各地禁忌

### 中國

「忌」原指先王的死日。《[周禮](../Page/周禮.md "wikilink")‧春官‧小史》：「若有事，則詔王之忌[諱](../Page/名諱.md "wikilink")。」
[鄭玄注引鄭司農曰](../Page/鄭玄.md "wikilink")：「先王死日為忌，名為諱」。今中文仍稱先人死日為「[忌日](../Page/忌日.md "wikilink")」、「死忌」\[1\]。

#### 禮儀方面

  - 在喜慶場合說不吉利的話，如：「[死](../Page/死.md "wikilink")」、「[鬼](../Page/鬼.md "wikilink")」。（用代詞則可，可稱「[過世](../Page/過世.md "wikilink")」、台語地區「[好兄弟](../Page/好兄弟.md "wikilink")」。）
  - 直呼長輩的名字。
  - 詛咒一個人(特別是有子女逝世的父母)「早死早著」。
  - 賭博時用手指指對方。（會讓賭博者心煩意燥，容易造成爭執，並在對方輸掉時造成因為被指故帶來霉運而輸掉，故用手指指對方一般被認為會帶來霉運）
  - 打麻將時旁觀者質疑對方「打錯牌」。
  - 喫飯時[筷子直立插於飯碗中](../Page/筷子.md "wikilink")。（形如[喪事拜祭死者](../Page/喪事.md "wikilink")）（參見：[腳尾飯](../Page/腳尾飯.md "wikilink")、[台灣喪葬](../Page/台灣喪葬.md "wikilink")）
  - 一般情況下，不把鐘、手帕、毛巾、梨、鞋、傘、草蓆等等作為[禮物](../Page/禮物.md "wikilink")。
      - 「[送鐘](../Page/送鐘.md "wikilink")」和「送終」讀音相同，可以在送鐘的同時亦送上一[花瓶](../Page/花瓶.md "wikilink")，以代表「始終平安」。
      - [手帕](../Page/手帕.md "wikilink")、[毛巾意謂](../Page/毛巾.md "wikilink")「拭淚」，唯獨喪家可送。
      - [梨代表](../Page/梨.md "wikilink")「分離」。探病者可送，解曰「病痛遠離」。
      - [鞋意謂](../Page/鞋.md "wikilink")「遠別」或「唉聲嘆氣」。
      - [傘意謂](../Page/傘.md "wikilink")「分散」。
          - 自2014年起，「黃傘」在香港有政治含義，意謂「爭取真普選」。
      - [草蓆意謂](../Page/草蓆.md "wikilink")「裹屍」。
          - 送上列這些物品，可收一塊錢，代表一筆[生意](../Page/生意.md "wikilink")，而非餽贈。

#### 鬼神方面

  - 在廟宇或墓碑附近小便
  - 在中元节期間靠近牆壁
  - 在死者墓前玩耍、拍手
  - 在喪禮時笑
  - 用手指指廟宇裡的神像
  - 用腳踢廟宇的門檻
  - 把香倒插進香爐
  - 用手指指月亮
  - 床尾對向房門
  - 晚上接近蕉樹叢

### 日本

在送禮時忌用[綠色](../Page/綠色.md "wikilink")，因為綠色是日本弔喪的顏色，但隨時代改變，已被認為可以接受。

### [西方世界](../Page/西方世界.md "wikilink")

  - 在[梯子下經過](../Page/梯子.md "wikilink")
  - 看黑色的[貓](../Page/貓.md "wikilink")
  - 接觸有關[納粹的事物](../Page/納粹.md "wikilink")
  - 數字[十三](../Page/十三.md "wikilink")，特別是[十三號星期五](../Page/十三號星期五.md "wikilink")。關於此迷信的另一個起源是來自《聖經》中的最後的晚餐，相傳耶穌遭其門徒之一的加略人猶大出賣而遭逮捕當天即是星期五，而且加略人猶大是當天最後的晚餐中的第13位客人。

## 宗教因素产生的禁忌

除了宗教经典规定的禁忌之外，近代新出现的事物亦会因宗教因素所禁忌。例如，[电视被](../Page/电视.md "wikilink")[犹太教哈雷迪派](../Page/哈雷迪猶太教.md "wikilink")、中国部分穆斯林\[2\]、部分佛教徒所禁忌。

### 基督教

依照《[圣经](../Page/圣经.md "wikilink")》原则，[耶和华见证人拒绝接受](../Page/耶和华见证人.md "wikilink")[输血](../Page/输血.md "wikilink")，按个人良心决定是否接受[器官移植](../Page/器官移植.md "wikilink")。

### 伊斯兰教

日常生活中，除了[食品、服饰等方面的禁忌](../Page/清真.md "wikilink")。随着[瓦哈比派的兴起](../Page/瓦哈比派.md "wikilink")，[世俗化的生活方式成为](../Page/世俗化.md "wikilink")[伊斯兰国家新的禁忌](../Page/伊斯兰国家.md "wikilink")。瓦哈比派的观点，[音乐](../Page/音乐.md "wikilink")（除[手鼓](../Page/手鼓.md "wikilink")）、[舞蹈等文化娱乐活动皆为禁忌](../Page/舞蹈.md "wikilink")\[3\]。在中国穆斯林社群中，受[泛清真化的影响](../Page/泛清真化.md "wikilink")，居住政府资助建设的住宅、收看[电视等亦成为禁忌](../Page/电视.md "wikilink")\[4\]。

### 猶太教

除了一直以来的犹太教严格的[食品规定外](../Page/符合教規的食物_\(猶太教\).md "wikilink")。各犹太教派对现代文化、事物有不同的禁忌。[犹太教哈雷迪派家中通常无](../Page/哈雷迪猶太教.md "wikilink")[电视](../Page/电视.md "wikilink")、[电脑](../Page/电脑.md "wikilink")，严格禁止孩子接触[網際網路](../Page/網際網路.md "wikilink")，但并不拒绝现代科技产品与家电，如[電話](../Page/電話.md "wikilink")、[烤箱](../Page/烤箱.md "wikilink")、[冰箱](../Page/冰箱.md "wikilink")、[微波炉等](../Page/微波炉.md "wikilink")。

### 佛教

部分佛教人士认为看电视、电影等会影响修行，因此，修行之人不能看电视。\[5\]\[6\]

## 参看

  - [饮食禁忌](../Page/饮食禁忌.md "wikilink")
  - [避讳](../Page/避讳.md "wikilink")
  - [風俗](../Page/風俗.md "wikilink")
  - [民俗學](../Page/民俗學.md "wikilink")
  - [文化](../Page/文化.md "wikilink")

## 注釋

## 參考資料

  - [平安 vs
    不安](http://old.tschurch.org/news/2012/news120909_1.htm).[臺灣基督長老教會](../Page/臺灣基督長老教會.md "wikilink").胡忠銘牧師
  - [送禮的學問](https://www.happy2u.com.tw/DOC_676.htm)
  - [什么是不能说出口的英语：禁忌语与俚语](http://www.audiy.com/englishstudy/study005.htm)

[th:คำต้องห้าม](../Page/th:คำต้องห้าม.md "wikilink")

[\*](../Category/禁忌.md "wikilink")
[Category:民俗學](../Category/民俗學.md "wikilink")
[Category:文化](../Category/文化.md "wikilink")
[Category:禮儀](../Category/禮儀.md "wikilink")

1.  <http://www.guangming.com.my/node/70788?tid=23>

2.
3.

4.

5.

6.