**禁食**（fasting）是指个人有意識地停止[進食某些或所有食物](../Page/進食.md "wikilink")，甚至[飲料](../Page/飲料.md "wikilink")。禁食亦包括禁止食用某些特定食物（例如[肉類](../Page/肉類.md "wikilink")、用不同方式处理的食物）。

## 禁食的原因

### 宗教

禁食行為存在于很多宗教的信仰準則中。其目的为準備節慶，反省自身，或教導信徒節制貪慾，進求更高層次的生活，通常被稱为**[齋戒](../Page/齋戒.md "wikilink")**。

有些宗教禁止教徒使用特定的食品，有些宗教限制禁食期長短、可進食的時間、份量、特定处理方式、戒肉等。

#### [犹太教和](../Page/犹太教.md "wikilink")[伊斯兰教](../Page/伊斯兰教.md "wikilink")

禁止教徒食某些肉类（如[猪肉](../Page/猪肉.md "wikilink")、[狗肉](../Page/狗肉.md "wikilink")、无鳞鱼类）和未按教规处理的食物。穆斯林在[斋月的白天不准进食和喝水](../Page/斋月.md "wikilink")。

#### [基督宗教](../Page/基督宗教.md "wikilink")

摩西﹑耶穌均曾進行四十天禁食。

復活節前禁食：[四旬期](../Page/四旬期.md "wikilink")/大齋期禁食數星期以至只禁食數天。有些天主教徒會将餐数限制為兩餐，戒肉或限制餐量。

現代比較放鬆的版本是不同年齡可只在某些特別日子只吃一次肉；某些特別誠心的教徒更會在某些特別日子戒肉。很多現代教會教派因信徒應繁忙的都市生活，已淡化此禁食傳統。

#### [佛教](../Page/佛教.md "wikilink")

某些教派禁止教徒食肉。

#### [锡克教](../Page/锡克教.md "wikilink")

禁止食用使用通过仪式杀死的动物的肉，譬如[伊斯兰教徒食用的](../Page/伊斯兰教徒.md "wikilink")[清真食品](../Page/清真食品.md "wikilink")。

### 政治及社會運動

禁食行為也是一種政治意志的表達方式，[基督教組織](../Page/基督教.md "wikilink")[世界宣明會亦以](../Page/世界宣明會.md "wikilink")[禁食籌款](../Page/飢餓三十人道救援行動.md "wikilink")。

### 醫療因素

也有醫療因素而進行的禁食（即**忌口**，[粵語稱此為](../Page/粵語.md "wikilink")**戒口**）。

為避免手術中[嘔吐導致](../Page/嘔吐.md "wikilink")[吸入性肺炎](../Page/吸入性肺炎.md "wikilink")，通常在接受[全身麻醉手術前](../Page/全身麻醉.md "wikilink")，也必需禁食。

另外如[血糖值等](../Page/血糖.md "wikilink")[血液檢查與胃部檢查](../Page/血液檢查.md "wikilink")﹑大腸檢查（內窺鏡）等之前，亦需禁食，以免影響檢查結果。

此外，西方民間開始推薦不同程度不同種類的禁食（可飲果菜汁/水），以調理腸胃和排毒。

但正統西方醫學並不鼓勵禁食。

## 禁食的生理效果

不同類型不同方式不同程度的禁食有不同影響。有好的影響和不好的影響，有短期影響和長期影響。

一般人的健康禁食者會飲用足夠清水或蔬菜汁或果汁或蔬果汁。旨在清理腸胃。

當人停止進食，身體會以其他方式維生，例如由[肝臟抽取](../Page/肝臟.md "wikilink")[肝醣轉化為](../Page/肝醣.md "wikilink")[葡萄糖](../Page/葡萄糖.md "wikilink")，或者從[脂肪中抽取](../Page/脂肪.md "wikilink")[脂肪酸](../Page/脂肪酸.md "wikilink")，甚至動用[蛋白質](../Page/蛋白質.md "wikilink")[組織](../Page/组织_\(生物学\).md "wikilink")。由於[腦部及](../Page/腦部.md "wikilink")[神經系統需要葡萄糖](../Page/神經系統.md "wikilink")，如果葡萄糖大量流失，身體會產生[酮](../Page/酮.md "wikilink")。但腦部某些組織仍然只需要葡萄糖，故繼續需要蛋白質。蛋白質繼續流失的結果會導致[死亡](../Page/死亡.md "wikilink")。在禁食約三日後，飢餓感會減少甚至消失。

## 参考文献

## 参见

  - [斋戒](../Page/斋戒.md "wikilink")
  - [禁食禱告](../Page/禁食禱告.md "wikilink")
  - [禁酒](../Page/禁酒.md "wikilink")
  - [节食](../Page/节食.md "wikilink")
  - [饿死](../Page/饿死.md "wikilink")

{{-}}

[禁食](../Category/禁食.md "wikilink")
[Category:营养学](../Category/营养学.md "wikilink")
[Category:禁欲主义](../Category/禁欲主义.md "wikilink")
[Category:基于生物的疗法](../Category/基于生物的疗法.md "wikilink")
[Category:宗教活動](../Category/宗教活動.md "wikilink")
[Category:飲食行為](../Category/飲食行為.md "wikilink")
[Category:膳食](../Category/膳食.md "wikilink")