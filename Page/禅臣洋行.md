[Shanghai,_Kiangsu_province,_China._Wellcome_L0055693.jpg](https://zh.wikipedia.org/wiki/File:Shanghai,_Kiangsu_province,_China._Wellcome_L0055693.jpg "fig:Shanghai,_Kiangsu_province,_China._Wellcome_L0055693.jpg")
**禅臣洋行（Siemssen &
Co.）**是一家历史悠久的从事远东贸易的[德国公司](../Page/德国.md "wikilink")，也是最早在[上海开业的德资洋行](../Page/上海.md "wikilink")。

禅臣洋行的总行设在德国[汉堡市](../Page/汉堡市.md "wikilink")，创办人特奥多尔·希姆森(Theodor
Siemssen)。1846年该行来到[中国](../Page/中国.md "wikilink")[广州设立分行](../Page/广州.md "wikilink")。1856年在上海外滩设立分行，行址在外滩28号（北京路北侧），并成为禅臣的中国总部。后又在天津、汉口、青岛、福州等重要商埠陆续开辟分行。天津禅臣洋行位于[天津德租界威尔逊路](../Page/天津德租界.md "wikilink")（今解放北路）113号。1899年,禅臣在青岛的[分行成立](../Page/太平路_\(青岛\)#禅臣洋行青岛分行早期办公楼.md "wikilink")，位于威廉皇帝海岸（今[太平路](../Page/太平路_\(青岛\).md "wikilink")）[礼和洋行东侧](../Page/太平路_\(青岛\)#礼和洋行青岛分行旧址.md "wikilink")，1900年建造（已拆除）；分行大楼后来搬迁至皇帝街，今[馆陶路5号楼房址](../Page/禅臣洋行青岛分行.md "wikilink")。

禅臣洋行最著名的业务是经营工厂和铁路的成套设备，以及西药、染料、军火等，乃至绒线、布匹、针线等日用商品，代理德、英许多著名厂商和保险公司，并向欧洲出口中国土产。包括帮助[阎锡山建设西北炼钢厂和同蒲铁路](../Page/阎锡山.md "wikilink")。

[第一次世界大战德国战败](../Page/第一次世界大战.md "wikilink")，禅臣洋行被迫退出中国市场，各地产业作为敌产被没收；战后禅臣又重返中国，业务进一步扩展。并增设了南京分行。

[第二次世界大战以后](../Page/第二次世界大战.md "wikilink")，中国国民政府查封德国在华产业，分期分批遣送德侨回国。禅臣洋行经理杨宁史（W.Jannings，1886年—）于1946年1月22日，将自己历时数十载收藏的价值连城的241件古代铜器、兵器献给国立北平故宫博物院，才得到特许留居中国，直至1954年6月16日被天津市人民政府驱逐出境。\[1\]\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:清朝对外贸易](../Category/清朝对外贸易.md "wikilink")
[Category:德國洋行](../Category/德國洋行.md "wikilink")

1.  [中华网：天津往事:瑞士人献宝故宫解放后被驱逐出境 2009-5-10添入](http://news.china.com/zh_cn/history/all/11025807/20060221/13110826_1.html)
2.  [河北区政务网：在津瑞士人杨宁史献宝故宫 2009-5-10添入](http://www.tjhbq.gov.cn/ReadNews.asp?NewsID=4754)