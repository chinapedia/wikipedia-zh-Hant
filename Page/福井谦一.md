**福井谦一**（，）[日本](../Page/日本.md "wikilink")[理论化学家](../Page/理论化学.md "wikilink")\[1\]，[美国科学院外籍](../Page/美国科学院.md "wikilink")[院士](../Page/院士.md "wikilink")，欧洲艺术科学文学院院士。[文化勳章](../Page/文化勳章.md "wikilink")、[勳一等旭日大綬章表彰](../Page/勳一等旭日大綬章.md "wikilink")。[文化功勞者](../Page/文化功勞者.md "wikilink")。追贈[從二位](../Page/從二位.md "wikilink")。

福井教授由于在1951年提出直观化的[前线轨道理论](../Page/前线轨道理论.md "wikilink")\[2\]，因而於1981年成為亞洲首位[诺贝尔化学奖得主](../Page/诺贝尔化学奖.md "wikilink")、第6位[日本人諾貝爾獎得主](../Page/日本人諾貝爾獎得主.md "wikilink")。

## 生平

1918年10月4日，福井出生于[日本](../Page/日本.md "wikilink")[奈良县井户野町的一个职员家庭](../Page/奈良县.md "wikilink")，他的[父亲毕业于](../Page/父亲.md "wikilink")[东京商科大学](../Page/东京商科大学.md "wikilink")，供职于一家[英国公司](../Page/英国.md "wikilink")。家境富裕的福井自幼便受到良好的[教育](../Page/教育.md "wikilink")，不仅学习过《[论语](../Page/论语.md "wikilink")》等传统典籍，同时也受到[欧美文化和先进](../Page/欧美.md "wikilink")[科学技术的熏陶](../Page/科学技术.md "wikilink")。

[中学时代的福井谦一先后就读于](../Page/中学.md "wikilink")和，他的[数学与](../Page/数学.md "wikilink")[德语成绩优异](../Page/德语.md "wikilink")，但是这位日后的理论化学家却对中学[化学非常不感兴趣](../Page/化学.md "wikilink")。在进入[大学的](../Page/大学.md "wikilink")[升学考试中](../Page/升学.md "wikilink")，福井受到家族[亲戚](../Page/亲戚.md "wikilink")，[京都帝国大学工业化学系教授](../Page/京都帝国大学.md "wikilink")**喜多源逸**的影响，选择了自己最不喜欢的[化学作为终身的专业](../Page/化学.md "wikilink")。
[Kenichi_Fukui_Monument_at_Kyoto_University.JPG](https://zh.wikipedia.org/wiki/File:Kenichi_Fukui_Monument_at_Kyoto_University.JPG "fig:Kenichi_Fukui_Monument_at_Kyoto_University.JPG")福井謙一紀念碑\]\]
1938年，福井考入[京都大学工业化学系](../Page/京都大学.md "wikilink")，进入大学的福井没有放弃自己对数学的兴趣，选修了大量数学和理论物理方面的课程，在这一时期打下了坚实的数理基础。1941年福井大学毕业，进入京都大学燃料化学系[儿玉信次郎](../Page/儿玉信次郎.md "wikilink")[教授的实验室攻读](../Page/教授.md "wikilink")[硕士学位](../Page/硕士学位.md "wikilink")。儿玉早年留学[德国](../Page/德国.md "wikilink")，返回日本时带回了大量欧洲的书籍资料，当时的欧洲量子理论正出于空前的发展之中，福井谦一通过这些珍贵的书籍，接触到了当时理论科学研究前沿。1948年福井获得了他的[博士学位](../Page/博士学位.md "wikilink")。毕业后的福井留在京都大学燃料化学系，在一间条件简陋的研究室中从事理论研究。

1951年，福井谦一发表了前线轨道理论的第一篇论文《[芳香碳氢化合物中反应性的分子轨道研究](../Page/芳香碳氢化合物中反应性的分子轨道研究.md "wikilink")》奠定了福井理论的基础\[3\]。福井初期的工作并不为人们所认可，他的同事和上司认为福井谦一不专心从事应用化学的研究，而是希望提出全新的化学基础理论，并且希望将当时还不为化学界所接受的[量子力学引入到化学领域中](../Page/量子力学.md "wikilink")，是不切实际和狂妄的；日本学术界对福井谦一的理论也并不重视，直到1960年代，欧美学术界开始大量引用福井的论文之后，日本人才开始重新审视福井谦一理论的价值。由于福井在前线轨道理论方面开创性的工作，京都大学逐渐形成了一个以他为核心的理论化学研究团队，福井学派也成为[量子化学领域一个重要的学派](../Page/量子化学.md "wikilink")。

1981年，福井谦一与提出[分子轨道对称性守恒原理的美国科学家](../Page/分子轨道对称性守恒原理.md "wikilink")[罗德·霍夫曼分享了诺贝尔化学奖](../Page/罗德·霍夫曼.md "wikilink")\[4\]。同年，他又获得了美国科学院外籍院士、欧洲艺术科学文学院院士、日本政府文化勋章等一系列荣誉。1982年福井从京都大学退休，被京都工艺纤维大学聘为校长，同时担任福井基础化学研究所所长，福井担任这些职位直到1998年逝世。

## 贡献

### 前线轨道理论

前线轨道理论是福井谦一赖以成名的理论，这一理论将[分子周围分布的](../Page/分子.md "wikilink")[电子云根据](../Page/电子云.md "wikilink")[能量细分为不同能级的分子轨道](../Page/能量.md "wikilink")，福井认为有[电子排布的](../Page/电子.md "wikilink")，能量最高的分子轨道（即最高占据轨道[HOMO](../Page/HOMO.md "wikilink")）和没有被电子占据的，能量最低的分子轨道（即最低未占轨道[LUMO](../Page/LUMO.md "wikilink")）是决定一个体系发生化学反应的关键，其他能量的分子轨道对于化学反应虽然有影响但是影响很小，可以暂时忽略。HOMO和LUMO便是所谓[前线轨道](../Page/前线轨道.md "wikilink")。

福井提出，通过计算参与反应的各粒子的分子轨道，获得前线轨道的能量、波函数相位、重叠程度等信息，便可以相当满意地解释各种化学反应行为，对于一些经典理论无法解释的行为，应用前线轨道理论也可以给出令人满意的解释。前线轨道理论简单、直观、有效，因而在[化学反应](../Page/化学反应.md "wikilink")、[生物大分子反应过程](../Page/生物大分子.md "wikilink")、催化机理等理论研究方面有着广泛的应用。

### 内禀反应坐标法

过渡态理论认为，化学反应的过程就是体系从[势能面上的一个最低点经过某一特定途径](../Page/势能面.md "wikilink")，滑动到另一个最低点的过程，研究体系滑动在势能面上留下的轨迹，是[化学反应动力学的一项任务](../Page/化学反应动力学.md "wikilink")，福井提出的内禀反应坐标法可以阐明体系在反应途径上每一处所受到的作用力与核位移之间的关系。

### 量子化学直观化

无论是前线轨道理论还是内禀反应坐标法，都是将复杂、抽象的量子化学公式转化为简单直观的近似理论，福井谦一所有的工作都是围绕量子化学直观化这一目标进行的，通过他提出的理论，传统的化学家可以不经过抽象的公式推导和计算，直接使用量子化学的理论指导实验，为他们打开了理论化学神秘的大门。为此福井专门编写了《[图解量子化学](../Page/图解量子化学.md "wikilink")》一书，这部书是非理论化学专业工作者了解量子化学的经典读物。

## 著作

## 參考文獻

<div class="references-small">

</div>

## 参见

  - [前线轨道理论](../Page/前线轨道理论.md "wikilink")
  - [諸熊奎治](../Page/諸熊奎治.md "wikilink")
  - [日本人諾貝爾獎得主](../Page/日本人諾貝爾獎得主.md "wikilink")
  - [京都大學諾貝爾獎得主列表](../Page/京都大學諾貝爾獎得主列表.md "wikilink")

## 外部链接

  - [Kenichi Fukui - Facts, The Nobel Prize in
    Chemistry 1981](http://www.nobelprize.org/nobel_prizes/chemistry/laureates/1981/fukui-facts.html)

  - [『福井謙一先生について』、京都大学福井謙一記念研究センター](http://www.fukui.kyoto-u.ac.jp/Fukui/fukui.html)

[Category:日本化学家](../Category/日本化学家.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:日本諾貝爾獎獲得者](../Category/日本諾貝爾獎獲得者.md "wikilink")
[Category:京都大學教師](../Category/京都大學教師.md "wikilink")
[Category:京都大學校友](../Category/京都大學校友.md "wikilink")
[Category:奈良縣出身人物](../Category/奈良縣出身人物.md "wikilink")
[Category:文化勳章獲得者](../Category/文化勳章獲得者.md "wikilink")
[Category:文化功勞者](../Category/文化功勞者.md "wikilink")
[Category:亞洲之最](../Category/亞洲之最.md "wikilink")
[Category:勳一等旭日大綬章獲得者](../Category/勳一等旭日大綬章獲得者.md "wikilink")

1.
2.
3.  Fukui K, Yonezawa T and Shingu H. A molecular orbital theory of
    reactivity in aromatic hydrocarbons. *Journal of Chemical Physics*,
    April 1952, 20(4): 722-725.
    [<doi:10.1063/1.1700523>](http://dx.doi.org/10.1063/1.1700523).
4.  Fukui K. Role of Frontier Orbitals in Chemical Reactions. *Science*,
    November 1982, 218(4574): 747–754.
    [<doi:10.1126/science.218.4574.747>](http://dx.doi.org/10.1126/science.218.4574.747).