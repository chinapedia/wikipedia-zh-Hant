**福克F27友誼式客機**（**Fokker F27
"Friendship"**）是一款由[荷蘭飛機製造商](../Page/荷蘭.md "wikilink")[福克生產的](../Page/福克公司.md "wikilink")[渦輪螺旋槳飛機](../Page/渦輪螺旋槳.md "wikilink")，屬32座等級的小型客機，經常被用於區域性或離島航線等航程較短的路線。

## 历史渊源

Fokker
F27的设计阶段始于1950年代初，原定作为[DC-3替代型号机种](../Page/DC-3.md "wikilink")。生产商衡量了好几种原型，最后选定高机翼，双[劳斯莱斯](../Page/劳斯莱斯.md "wikilink")[道蒂发动机](../Page/勞斯萊斯道蒂發動機.md "wikilink")，加压机舱的28座设计。

首架原型机PH-NIV1955年11月24日试飞成功。第一架针对尾重缺点，該飛機配備兩台達特507引擎，載客量達28人，註冊編號為PH-NIV。第二架原型机在1957年首飛，与初生产型比之加长了0.9米，因此能多载乘客至32人，并采用功率更大的[勞斯萊斯Dart](../Page/勞斯萊斯.md "wikilink")
Mk 528發動機。

1956年4月26日，美國的[費柴尔德公司](../Page/費柴尔德公司.md "wikilink")（Fairchild）與福克公司簽訂了合同，費爾柴德會在美國協助生產F27，其產品主要在美國銷售。費爾柴德的F-27與福克F-27有些不同，它的載客量達40人，且航程較遠。後來，費爾柴德公司在F27基礎上自行研發出[FH-227](../Page/FH-227.md "wikilink")，它的機身比F27長，載客量達52人，但產量並不多。

Fokker
F27总共生产了793架，其中費爾柴德在美国生产207架。因而她是历代最成功的[渦輪螺旋槳飛機](../Page/渦輪螺旋槳飛機.md "wikilink")。许多架飞机还被改装为客货两用机、或快递专用飞机。1980年代初，福克公司研制出其后继F50型飞机。新飞机自F27-500型号改进而成，加装加拿大[普惠公司研制的引擎及现代化系统](../Page/普惠公司.md "wikilink")，使得性能表现及乘客舒适性远优于F27型。

## 修改型号

Fokker
F27最先的生产型号为：F27-100，是44座客机，1958年装备[爱尔兰航空公司](../Page/爱尔兰航空公司.md "wikilink")（Aer
Lingus）。

[Fokker_F-27_300M.jpg](https://zh.wikipedia.org/wiki/File:Fokker_F-27_300M.jpg "fig:Fokker_F-27_300M.jpg")
型号一览：

  - **F27-100** - 最先的生产型号
  - **F27-200** - 100型之改型，搭載[劳斯莱斯Dart](../Page/劳斯莱斯.md "wikilink") Mk
    532型發動機
  - **F27-300 Combiplane** - 民航、运货型
  - **F27-300M Troopship** - [荷兰皇家空军专用型号](../Page/荷兰皇家空军.md "wikilink")
  - **F27-400** - 客货两用型，采用[勞斯萊斯Dart
    7涡轮螺旋桨引擎加大货物用登机门](../Page/勞斯萊斯Dart_7.md "wikilink")
  - **F27-400M** - 美国陆军专用型号**C-31A Troopship**
  - **F27-500** - 最广泛采用的型号，[机身增长](../Page/机身.md "wikilink")，采用[劳斯莱丝Dart
    Mk 528引擎](../Page/劳斯莱丝.md "wikilink")，载客量52人，1967年首飞
  - **F27-500M** - 500型之军用型
  - **F27-500F** - 供澳大利亚使用500型之改型之改型缩小登机门
  - **F27-600** - 基于F27-100加大货物用登机门
  - **F27-700** - 基于F27-100加大货物用登机门
  - **F27 Maritime** - 为无武装海岸侦察型
  - **F27 Maritime Enforcer** - 为有武装海岸侦察型
  - [**FH-227**](../Page/Fairchild_Hiller_FH-227.md "wikilink") -
    [费柴尔德公司改进型号](../Page/费柴尔德.md "wikilink")

### F27民用型采用情况

以下航空公司采用曾经Fokker F27：

<table style="width:83%;">
<colgroup>
<col style="width: 50%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: left;"><ul>
<li><a href="../Page/ABA_Airlines.md" title="wikilink">ABA Airlines</a></li>
<li><a href="../Page/Aerocaribe.md" title="wikilink">Aerocaribe</a></li>
<li><a href="../Page/Aero_Condor_Peru.md" title="wikilink">Aero Condor</a></li>
<li><a href="../Page/愛爾蘭航空.md" title="wikilink">愛爾蘭航空</a></li>
<li><a href="../Page/Afrijet.md" title="wikilink">Afrijet</a></li>
<li><a href="../Page/阿尔及利亚航空.md" title="wikilink">阿尔及利亚航空</a></li>
<li><a href="../Page/Air_Anglia.md" title="wikilink">Air Anglia</a></li>
<li><a href="../Page/Air_Congo.md" title="wikilink">Air Congo</a></li>
<li><a href="../Page/Air_Executive_Norway.md" title="wikilink">Air Executive Norway</a></li>
<li><a href="../Page/法國航空.md" title="wikilink">法國航空</a></li>
<li><a href="../Page/Air_Inter.md" title="wikilink">Air Inter</a></li>
<li><a href="../Page/科特迪瓦航空.md" title="wikilink">科特迪瓦航空</a></li>
<li><a href="../Page/Air_Max-Gabon.md" title="wikilink">Air Max-Gabon</a></li>
<li><a href="../Page/Air_New_South_Wales.md" title="wikilink">Air New South Wales</a></li>
<li><a href="../Page/新西蘭航空.md" title="wikilink">新西蘭航空</a></li>
<li><a href="../Page/新幾內亞航空.md" title="wikilink">新幾內亞航空</a></li>
<li><a href="../Page/Air_Panama.md" title="wikilink">Air Panama</a></li>
<li><a href="../Page/Air_Sinai.md" title="wikilink">Air Sinai</a></li>
<li><a href="../Page/Air_Tanzania.md" title="wikilink">Air Tanzania</a></li>
<li><a href="../Page/Air_Tropiques.md" title="wikilink">Air Tropiques</a></li>
<li><a href="../Page/Air_UK.md" title="wikilink">Air UK</a></li>
<li><a href="../Page/扎伊爾航空.md" title="wikilink">扎伊爾航空</a></li>
<li><a href="../Page/Air_West_Express.md" title="wikilink">Air West Express</a></li>
<li><a href="../Page/Airlift_International.md" title="wikilink">Airlift International</a></li>
<li><a href="../Page/Aeronica.md" title="wikilink">Aeronica</a></li>
<li><a href="../Page/全日本空輸.md" title="wikilink">全日本空輸</a></li>
<li><a href="../Page/Air_ALM.md" title="wikilink">Air ALM</a></li>
<li><a href="../Page/Amerer_Air.md" title="wikilink">Amerer Air</a></li>
<li><a href="../Page/Aloha_Airlines.md" title="wikilink">Aloha Airlines</a></li>
<li><a href="../Page/安捷航空.md" title="wikilink">安捷航空</a></li>
<li><a href="../Page/Associated_Airlines_of_Australia.md" title="wikilink">Associated Airlines of Australia</a></li>
<li>ATI - <a href="../Page/Aero_Transport_Italiani.md" title="wikilink">Aero Transport Italiani</a></li>
<li><a href="../Page/Balair.md" title="wikilink">Balair</a></li>
<li><a href="../Page/Bali_International_Air_Service.md" title="wikilink">Bali International Air Service</a></li>
<li><a href="../Page/孟加拉航空.md" title="wikilink">孟加拉航空</a></li>
<li><a href="../Page/布拉森航空.md" title="wikilink">布拉森航空</a></li>
<li><a href="../Page/英倫航空.md" title="wikilink">英倫航空</a></li>
<li><a href="../Page/Bonanza_Air_Lines.md" title="wikilink">Bonanza Air Lines</a></li>
<li><a href="../Page/Burma_Airways_Corporation.md" title="wikilink">Burma Airways Corporation</a></li>
<li><a href="../Page/Busy_Bee.md" title="wikilink">Busy Bee</a></li>
<li><a href="../Page/CATA_Línea_Aérea.md" title="wikilink">CATA Línea Aérea</a></li>
<li><a href="../Page/Channel_Express.md" title="wikilink">Channel Express</a></li>
<li><a href="../Page/古巴航空.md" title="wikilink">古巴航空</a></li>
<li><a href="../Page/德爾塔航空.md" title="wikilink">德爾塔航空</a></li>
<li><a href="../Page/DETA.md" title="wikilink">DETA</a></li>
<li><a href="../Page/東非航空.md" title="wikilink">東非航空</a></li>
<li><a href="../Page/East-West_Airlines_(Australia).md" title="wikilink">East-West Airlines (Australia)</a></li>
<li><a href="../Page/Elbee_Airlines.md" title="wikilink">Elbee Airlines</a></li>
<li><a href="../Page/Euroceltic_Airways.md" title="wikilink">Euroceltic Airways</a></li>
<li><a href="../Page/Expresso_Aéreo.md" title="wikilink">Expresso Aéreo</a></li>
<li><a href="../Page/Farnair_Hungary.md" title="wikilink">Farnair Hungary</a></li>
<li><a href="../Page/聯邦快遞.md" title="wikilink">聯邦快遞</a></li>
<li><a href="../Page/芬蘭航空.md" title="wikilink">芬蘭航空</a></li>
<li><a href="../Page/FTG_Air_Service.md" title="wikilink">FTG Air Service</a></li>
<li><a href="../Page/印度尼西亞鷹航空公司.md" title="wikilink">印度尼西亞鷹航空公司</a></li>
<li><a href="../Page/Hughes_Airwest.md" title="wikilink">Hughes Airwest</a></li>
<li><a href="../Page/西班牙國家航空.md" title="wikilink">西班牙國家航空</a></li>
<li><a href="../Page/冰島航空.md" title="wikilink">冰島航空</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/印度人航空.md" title="wikilink">印度人航空</a></li>
<li><a href="../Page/Iona_National_Airways.md" title="wikilink">Iona National Airways</a> (Ireland)</li>
<li><a href="../Page/伊朗阿塞曼航空.md" title="wikilink">伊朗阿塞曼航空</a></li>
<li><a href="../Page/Jersey_European_Airways.md" title="wikilink">Jersey European Airways</a></li>
<li><a href="../Page/大韓航空.md" title="wikilink">大韓航空</a></li>
<li><a href="../Page/肯尼亞航空.md" title="wikilink">肯尼亞航空</a></li>
<li><a href="../Page/LADE.md" title="wikilink">LADE</a></li>
<li><a href="../Page/Laoag.md" title="wikilink">Laoag</a></li>
<li><a href="../Page/Lesotho_Airways.md" title="wikilink">Lesotho Airways</a></li>
<li><a href="../Page/利比亞阿拉伯航空.md" title="wikilink">利比亞阿拉伯航空</a></li>
<li><a href="../Page/Lloyd_Aereo_Boliviano.md" title="wikilink">Lloyd Aereo Boliviano</a></li>
<li><a href="../Page/Luxair.md" title="wikilink">Luxair</a></li>
<li><a href="../Page/Maersk_Air.md" title="wikilink">Maersk Air</a></li>
<li><a href="../Page/馬來西亞航空.md" title="wikilink">馬來西亞航空</a></li>
<li><a href="../Page/馬來亞航空.md" title="wikilink">馬來西亞-新加坡航空</a></li>
<li><a href="../Page/Mesaba_Airlines.md" title="wikilink">Mesaba Airlines</a></li>
<li><a href="../Page/Mihin_Air.md" title="wikilink">Mihin Air</a></li>
<li><a href="../Page/Mississippi_Valley_Airlines.md" title="wikilink">Mississippi Valley Airlines</a></li>
<li><a href="../Page/Nigeria_Airways.md" title="wikilink">Nigeria Airways</a></li>
<li><a href="../Page/新西蘭國家航空.md" title="wikilink">新西蘭國家航空</a></li>
<li><a href="../Page/Northeast_Airlines.md" title="wikilink">Northeast Airlines</a></li>
<li><a href="../Page/Mactan.md" title="wikilink">Mactan</a></li>
<li><a href="../Page/印尼鴿記航空.md" title="wikilink">印尼鴿記航空</a></li>
<li><a href="../Page/Mountain_Air_Cargo.md" title="wikilink">Mountain Air Cargo</a> (FedEx Feeder)</li>
<li><a href="../Page/Myanma_Airways.md" title="wikilink">Myanma Airways</a></li>
<li><a href="../Page/NEPC_Airlines.md" title="wikilink">NEPC Airlines</a></li>
<li><a href="../Page/Newair_Airservice.md" title="wikilink">Newair Airservice</a> (Denmark)</li>
<li><a href="../Page/NLM_Cityhopper.md" title="wikilink">NLM Cityhopper</a></li>
<li><a href="../Page/Norcanair.md" title="wikilink">Norcanair</a></li>
<li><a href="../Page/挪威航空.md" title="wikilink">挪威航空</a></li>
<li><a href="../Page/Pacific_Air_Lines.md" title="wikilink">Pacific Air Lines</a></li>
<li><a href="../Page/巴基斯坦國際航空.md" title="wikilink">巴基斯坦國際航空</a></li>
<li><a href="../Page/Pelita_Air_Service.md" title="wikilink">Pelita Air Service</a></li>
<li><a href="../Page/Piedmont_Airlines_(1948-1989).md" title="wikilink">Piedmont Airlines</a></li>
<li><a href="../Page/菲律賓航空.md" title="wikilink">菲律賓航空</a></li>
<li><a href="../Page/Pilgrim_Airlines.md" title="wikilink">Pilgrim Airlines</a></li>
<li><a href="../Page/摩洛哥皇家航空.md" title="wikilink">摩洛哥皇家航空</a></li>
<li><a href="../Page/北歐航空.md" title="wikilink">北歐航空</a></li>
<li><a href="../Page/Scibe_Airlift_Cargo.md" title="wikilink">Scibe Airlift Cargo</a></li>
<li><a href="../Page/Sempati_Air_Transport.md" title="wikilink">Sempati Air Transport</a></li>
<li><a href="../Page/Sky_Team.md" title="wikilink">Sky Team</a></li>
<li><a href="../Page/Somali_Airlines.md" title="wikilink">Somali Airlines</a></li>
<li><a href="../Page/Starair.md" title="wikilink">Starair</a></li>
<li><a href="../Page/Sterling_Airways.md" title="wikilink">Sterling Airways</a></li>
<li><a href="../Page/蘇丹航空.md" title="wikilink">蘇丹航空</a></li>
<li><a href="../Page/TAA.md" title="wikilink">TAA</a> (<a href="../Page/Trans_Australia_Airlines.md" title="wikilink">Trans Australia Airlines</a>)</li>
<li><a href="../Page/巴西天馬航空.md" title="wikilink">巴西天馬航空</a></li>
<li><a href="../Page/T.A._de_la_Guinee-Bissau.md" title="wikilink">T.A. de la Guinee-Bissau</a></li>
<li><a href="../Page/TAVAJ_-_Linhas_Aéras_Brazil.md" title="wikilink">TAVAJ - Linhas Aéras Brazil</a></li>
<li><a href="../Page/土耳其航空.md" title="wikilink">土耳其航空</a></li>
<li><a href="../Page/Trans_Australia_Airlines.md" title="wikilink">Trans Australia Airlines</a></li>
<li><a href="../Page/Union_of_Burma_Airways.md" title="wikilink">Union of Burma Airways</a></li>
<li><a href="../Page/Uganda_Airways.md" title="wikilink">Uganda Airways</a></li>
<li><a href="../Page/Aerolineas_SOSA.md" title="wikilink">Aerolineas SOSA</a></li>
<li><a href="../Page/TAAG安哥拉航空.md" title="wikilink">TAAG安哥拉航空</a></li>
<li><a href="../Page/WDL_Aviation.md" title="wikilink">WDL Aviation</a></li>
</ul></td>
</tr>
</tbody>
</table>

截至2006年8月世界上依然有164架各种型号的Fokker-F27飞机在使用中。 \[1\]

### 军用型采用情况

  - 空军

  - 空军

  - 空军

  - 空军\<\!--: [Australian
    Navy](../Page/Australian_Navy.md "wikilink")\<\!--

  - [Flag_of_Biafra.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Biafra.svg "fig:Flag_of_Biafra.svg")
    [Biafra](../Page/Biafra.md "wikilink")--\>

  - 空军\<\!--: [Bolivian Air
    Force](../Page/Bolivian_Air_Force.md "wikilink")\<\!--

  - \--\>

  - 空军

  - 空军

  - 空军

  - 海岸卫队

  - 空军

  - 空军

  - 空军

  - 空军

  - 空军

  - 空军

  - 皇家空军

  - 皇家空军

  - 空军

  - 空军及海军

  - 海军

  - 空军及海军

  - 空军

  - 空军

  - 空军

  - 皇家海军\<\!--: [Royal Thai Navy](../Page/Royal_Thai_Navy.md "wikilink")

  - 空军

  - 空军

  - 空军

### 政府机构采用情况

  - 民航总局\<\!--: Department of Civil Aviation

\--\>

  -   - [澳大利亚邮政局](../Page/澳大利亚邮政局.md "wikilink")（Australia Post）

  - 政府

  - 地理勘探局

  - 海岸卫队

  - 政府

  - 皇家专机

  - 交通部\<\!--: Ministry of Transport, Navaids calibration flight--

### Organisations and Corporations

  - F27 Friendship Association - The Netherlands
  - Libyan Red Crescent
  - [National Iranian Oil
    Company](../Page/National_Iranian_Oil_Company.md "wikilink") (NIOC)
  - Hawkins and Powers Aviation, Inc. - USA
  - AirCruising Australia - An air based tour company --\>

## 飞行事故

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><font color=#690000>事故日期</font></p></td>
<td style="text-align: center;"><p><font color=#690000>事故地点</font></p></td>
<td style="text-align: center;"><p><font color=#690000>遇难人数</font></p></td>
<td style="text-align: center;"><p><font color=#690000>备注</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1960年6月10日</p></td>
<td style="text-align: center;"><p><a href="../Page/澳大利亚.md" title="wikilink">澳大利亚</a><a href="../Page/昆士兰州.md" title="wikilink">昆士兰州</a><br />
北部<a href="../Page/麦凯.md" title="wikilink">麦凯</a>（<a href="../Page/Mackay.md" title="wikilink">Mackay</a>）</p></td>
<td style="text-align: center;"><p>29</p></td>
<td style="text-align: center;"><p>本次事故是大洋洲航空史是遇难者人数最多的空难，调查无法确认本次事故的直接原因。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1971年<a href="../Page/第二次印巴战争.md" title="wikilink">第二次印巴战争初期</a></p></td>
<td style="text-align: center;"><p><a href="../Page/阿拉伯海.md" title="wikilink">阿拉伯海上空</a></p></td>
<td style="text-align: center;"><p>不详</p></td>
<td style="text-align: center;"><p>失事为<a href="../Page/巴基斯坦空军.md" title="wikilink">巴基斯坦空军飞机</a>，在执行侦察<a href="../Page/印度海军.md" title="wikilink">印度海军</a><a href="../Page/导弹快艇.md" title="wikilink">导弹快艇方位的任务中失踪</a>。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1972年10月13日</p></td>
<td style="text-align: center;"><p><a href="../Page/安第斯山脉.md" title="wikilink">安第斯山脉</a></p></td>
<td style="text-align: center;"><p>29</p></td>
<td style="text-align: center;"><p><a href="../Page/乌拉圭空军571号班机空难.md" title="wikilink">乌拉圭空军571号班机空难</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1987年12月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/秘鲁.md" title="wikilink">秘鲁</a><a href="../Page/利马.md" title="wikilink">利马</a></p></td>
<td style="text-align: center;"><p>不详</p></td>
<td style="text-align: center;"><p>利马Alianza足球俱乐部足球队当时全队在机上遇难。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2003年2月20日</p></td>
<td style="text-align: center;"><p><a href="../Page/巴基斯坦.md" title="wikilink">巴基斯坦</a></p></td>
<td style="text-align: center;"><p>17</p></td>
<td style="text-align: center;"><p><a href="../Page/巴基斯坦空军.md" title="wikilink">巴基斯坦空军</a><a href="../Page/元帅.md" title="wikilink">元帅Mushaf</a> Ali Mir及夫人，与15名乘员全部遇难。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2006年7月10日</p></td>
<td style="text-align: center;"><p><a href="../Page/巴基斯坦.md" title="wikilink">巴基斯坦</a><a href="../Page/木尔坦.md" title="wikilink">木尔坦机场</a>（<a href="../Page/Multan.md" title="wikilink">Multan</a>）</p></td>
<td style="text-align: center;"><p>45</p></td>
<td style="text-align: center;"><p><a href="../Page/巴基斯坦国际航空公司.md" title="wikilink">巴基斯坦国际航空公司班机</a>，起飞2-3分钟后怀疑因引擎故障坠毁，全体乘客及机组遇难。<a href="http://news.bbc.co.uk/2/hi/south_asia/5164280.stm">1</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2012年6月21日</p></td>
<td style="text-align: center;"><p><a href="../Page/印尼.md" title="wikilink">印尼</a><a href="../Page/雅加达.md" title="wikilink">雅加达</a></p></td>
<td style="text-align: center;"><p>10</p></td>
<td style="text-align: center;"><p>6月21日下午2点45分，印尼空军一架载有7人的福克-27飞机坠入首都雅加达东部的一处军方住宅区，致10人死亡10余人受伤。。</p></td>
</tr>
</tbody>
</table>

另见：[F27飞机坠毁事故列表](http://www.airdisaster.com/cgi-bin/view_manu_details.cgi?aircraft=F-27+Friendship)

## 飞机参数

[Rolls-Royce_Dart_Fokker_F_27.jpg](https://zh.wikipedia.org/wiki/File:Rolls-Royce_Dart_Fokker_F_27.jpg "fig:Rolls-Royce_Dart_Fokker_F_27.jpg")
以下主要飞机参数采录自[北欧航空所用F](../Page/北欧航空.md "wikilink")27-600型号：

  - **机身长度**：23.5米（77呎2.3吋）
  - **翼展**：29米（95呎2吋）
  - **双翼面积**：70平方米（753平方呎）
  - **最大起飞重量**：19,730公斤（43,730磅）
  - **正常巡航速度**：420公里／小時（約260英里／小時）
  - **最大航程**：1,930公里（1,200英里）
  - **實用升限**：7,500米（25,000呎）
  - **發動機**：2 x [劳斯莱斯Mk](../Page/劳斯莱斯.md "wikilink")532-7涡轮螺旋桨引擎
    （turboprops）当分别采用水喷注／乙醇喷注，时输出功率分别为：1835 SHP／1990 SHP

## 參考資料

## 相关条目

  - 費柴爾德公司的改型-[Fairchild Hiller
    FH-227](../Page/Fairchild_Hiller_FH-227.md "wikilink")
  - [Fokker F50](../Page/Fokker_F50.md "wikilink")
  - [Fokker F28](../Page/Fokker_F28.md "wikilink")
  - [ATR 42](../Page/ATR_42.md "wikilink") 及
    [72](../Page/ATR_72.md "wikilink")

## 外部链接

  - [Stork Aerospace Homepage](http://www.fokker.com/)
  - [F27 Friendship
    Association](https://web.archive.org/web/20060118004328/http://www.fokkerf27.nl/index.php)
  - [Photo
    Gallery](https://web.archive.org/web/20040227230746/http://www.fokkerf27.nl/pbook/pbook.php?language=NL&gallery_name=%5B30%5D_F27_per_cn)
    (德語及英語)
  - [External museum
    photo](http://www.airwaysmuseum.com/F27%20PH-FAY%20-%20VH-CAV.htm)
  - [Fokker, a living
    history](http://library.thinkquest.org/~C002752/fokker.cgi?page=home)
  - [Fokker F27 Info](http://www.airliners.net/info/stats.main?id=217)
  - [FedEx Feeder F27-500
    Photo](http://www.airliners.net/open.file/1033269/L/)

[Category:支线客机](../Category/支线客机.md "wikilink")
[Category:福克](../Category/福克.md "wikilink")
[Category:荷蘭航空器](../Category/荷蘭航空器.md "wikilink")

1.  《国际航空杂志》（Flight International） 2006年第3至第9期