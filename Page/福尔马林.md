[Formaldehyde-2D.svg](https://zh.wikipedia.org/wiki/File:Formaldehyde-2D.svg "fig:Formaldehyde-2D.svg")，福尔马林的主要溶质\]\]
[Methanediol-2D.png](https://zh.wikipedia.org/wiki/File:Methanediol-2D.png "fig:Methanediol-2D.png")，甲醛的水合物，福尔马林的主要溶质之一\]\]
[Methanol-2D.png](https://zh.wikipedia.org/wiki/File:Methanol-2D.png "fig:Methanol-2D.png")，福尔马林中防止甲醛聚合的溶质\]\]
**福尔马林**（），是[甲醛含量为](../Page/甲醛.md "wikilink")35%至40%（重量百分比為37%；體積百分比為40%）的[水](../Page/水.md "wikilink")[溶液](../Page/溶液.md "wikilink")，也加入10%～15%的[甲醇防止](../Page/甲醇.md "wikilink")[聚合](../Page/聚合.md "wikilink")。具有[防腐](../Page/防腐.md "wikilink")、[消毒和](../Page/消毒.md "wikilink")[漂白的功能](../Page/漂白.md "wikilink")，不同领域各有其作用，但福爾馬林會散發出刺鼻的氣味。甲醛被[國際癌症研究中心](../Page/國際癌症研究中心.md "wikilink")（IARC）列為明確人類致癌物質，影響人體健康。

## 醫學

福爾馬林在[醫學上的作用有保存](../Page/醫學.md "wikilink")[病理切片](../Page/病理切片.md "wikilink")、[手術器械和](../Page/手術器械.md "wikilink")[病房消毒等](../Page/病房.md "wikilink")。由於福爾馬林對人體有害，一些[醫護人員由於長期接觸福爾馬林](../Page/醫護人員.md "wikilink")，會出現症狀，主要是鼻癢、次是流[鼻水](../Page/鼻水.md "wikilink")、還有眼睛癢、[頭痛等](../Page/頭痛.md "wikilink")。

醫護人員會用PE[塑膠手套](../Page/塑膠手套.md "wikilink")，專防甲醛的濾毒罐[口罩來防止傷害](../Page/口罩.md "wikilink")，普通的不織布棉紗口罩及[活性碳口罩無法阻隔福爾馬林對人的傷害](../Page/活性碳.md "wikilink")。

## 生物學

生物學中以福尔马林作為[標本的保質](../Page/標本.md "wikilink")[媒介](../Page/媒介.md "wikilink")，透過浸泡方式阻隔標本本品與[空氣接觸](../Page/空氣.md "wikilink")，防腐保存。
[Steve_O'Shea_injecting_formalin.jpg](https://zh.wikipedia.org/wiki/File:Steve_O'Shea_injecting_formalin.jpg "fig:Steve_O'Shea_injecting_formalin.jpg")

## 食品业

在食物中添加福尔马林，主要是[漂白](../Page/漂白.md "wikilink")、凝固[蛋白質及防腐保鮮](../Page/蛋白質.md "wikilink")。（对人体有害）

## 养殖业

养殖业中以福尔马林为杀菌剂，让[鱼类进行药浴](../Page/鱼类.md "wikilink")，主要是用在寄生性疾病。

## 对人体的伤害

甲醛被[國際癌症研究中心](../Page/國際癌症研究中心.md "wikilink")（IARC）列為明确人類[致癌物質](../Page/致癌物質.md "wikilink")（I类），为甲醛溶液的福尔马林对人体亦有伤害。

[美國國家研究委員會](../Page/美國國家研究委員會.md "wikilink")（NRC）的文件列出福尔马林对人体的伤害：

皮肤接触福尔马林将引起刺激、[化學灼傷](../Page/烧伤#化學性灼傷.md "wikilink")、[腐蝕及](../Page/腐蝕.md "wikilink")[過敏反應](../Page/過敏.md "wikilink")。

吸入甲醛：

  - 0.01～2.0ppm：对[眼睛](../Page/眼睛.md "wikilink")、[上呼吸道](../Page/上呼吸道.md "wikilink")、[鼻子的刺激](../Page/鼻子.md "wikilink")，或者会令人有鼻塞状况。
  - 5ppm以上：[上呼吸道](../Page/上呼吸道.md "wikilink")、[下呼吸道的呼吸困難或将造成慢性](../Page/下呼吸道.md "wikilink")[肺部阻塞](../Page/肺.md "wikilink")，严重者会引起[肺水腫或](../Page/肺水腫.md "wikilink")[肺炎](../Page/肺炎.md "wikilink")。
  - 100ppm以上：會[死亡](../Page/死亡.md "wikilink")。

[Category:溶液](../Category/溶液.md "wikilink")
[Category:防腐劑](../Category/防腐劑.md "wikilink")