**福山市**（）是[日本](../Page/日本.md "wikilink")[廣島縣的中核市](../Page/廣島縣.md "wikilink")，位于廣島縣東南部（備後地方），是福山都市圈的中心城市。人口規模居于廣島縣內第二位，[中國地方第四位](../Page/中國地方.md "wikilink")（次于[廣島市](../Page/廣島市.md "wikilink")、[岡山市和](../Page/岡山市.md "wikilink")[倉敷市](../Page/倉敷市.md "wikilink")）。

市內南部的觀光景點「鞆の浦」非常有名，特產品有[鋼鐵](../Page/鋼鐵.md "wikilink")、[福山琴](../Page/福山琴.md "wikilink")、[慈姑和](../Page/慈姑.md "wikilink")[木屐](../Page/木屐.md "wikilink")。

## 地理

位于中國地方中南部，廣島縣東南端，緊鄰岡山縣。在過去中國和四國地方中，人口規模僅次于廣島市、岡山市、[松山市](../Page/松山市.md "wikilink")、倉敷市，位居第五。平成大合并中[合併了附近的四個町](../Page/市町村合併.md "wikilink")，人口據推算約有46万人。

以[高速公路里程來算](../Page/高速公路.md "wikilink")，該市距[大阪府](../Page/大阪府.md "wikilink")[大阪市有](../Page/大阪市.md "wikilink")236公里、[山口縣](../Page/山口縣.md "wikilink")[下關市有](../Page/下關市.md "wikilink")302公里、[廣島縣](../Page/廣島縣.md "wikilink")[廣島市有](../Page/廣島市.md "wikilink")107km、[岡山縣](../Page/岡山縣.md "wikilink")[岡山市有](../Page/岡山市.md "wikilink")70公里、[愛媛縣](../Page/愛媛縣.md "wikilink")[松山市有](../Page/松山市.md "wikilink")116km、[香川縣](../Page/香川縣.md "wikilink")[高松市有](../Page/高松市.md "wikilink")114km、[島根縣](../Page/島根縣.md "wikilink")[松江市有](../Page/松江市.md "wikilink")217km。

## 歷史

現在的市中心部分在古代是被稱為「穴之海」海域，在[平安時代後才漸漸因](../Page/平安時代.md "wikilink")[蘆田川帶來的泥沙淤積成為](../Page/蘆田川.md "wikilink")[三角洲](../Page/三角洲.md "wikilink")。\[1\]

[江戶時代的](../Page/江戶時代.md "wikilink")1619年，[水野勝成成為領主](../Page/水野勝成.md "wikilink")，並成立[福山藩](../Page/福山藩.md "wikilink")，及開始建築[福山城](../Page/福山城.md "wikilink")，此地區開始以[城下町的型態開發](../Page/城下町.md "wikilink")，因此以「福山」做為地名。

[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，原福山藩領地大部分都成為[福山縣的轄區](../Page/福山縣.md "wikilink")，並在市中心設置[福山町](../Page/福山町.md "wikilink")，做為[縣廳所在地](../Page/縣廳所在地.md "wikilink")；1871年改名為[深津縣](../Page/深津縣.md "wikilink")，隔年又與[倉敷縣合併為](../Page/倉敷縣.md "wikilink")[小田縣](../Page/小田縣.md "wikilink")，現廳則轉至[小田郡](../Page/小田郡.md "wikilink")[笠岡町](../Page/笠岡町.md "wikilink")。

1875年，小田現被併入[岡山縣](../Page/岡山縣.md "wikilink")，但在1876年，包含福山町所在的[沼隈郡](../Page/沼隈郡.md "wikilink")、[深津郡](../Page/深津郡.md "wikilink")、[安那郡](../Page/安那郡.md "wikilink")、[品治郡](../Page/品治郡.md "wikilink")、[蘆田郡](../Page/蘆田郡.md "wikilink")、[神石郡改隸屬](../Page/神石郡.md "wikilink")[廣島縣至今](../Page/廣島縣.md "wikilink")。

1916年改制為**福山市**時，為全日本第73個[市](../Page/市.md "wikilink")，也是廣島縣第四個成立的市。之後逐漸合併週邊地區，取代了[尾道市逐漸成為廣島縣東部的主要城市](../Page/尾道市.md "wikilink")。\[2\]

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [深津郡](../Page/深津郡.md "wikilink")：**福山町**、野上村、三吉村、川口村、手城村、深津村、奈良津村、吉津村、木之庄村、本庄村、山手村、鄉分村、引野村、市村、千田村、上岩成村、下岩成村、中津原村、森脇村、大津野村、春日村、坪生村。
      - [安那郡](../Page/安那郡.md "wikilink")：加茂村、廣瀨村、山野村、下加茂村、法成寺村、川北村、川南村、上竹田村、下竹田村、八尋村、中條村、道上村、御野村、湯田村。
      - [沼隈郡](../Page/沼隈郡.md "wikilink")：神島村、佐波村、草戶村、松永村、今津村、柳津村、金江村、藤江村、東村、本鄉村、神村、[鞆町](../Page/鞆町.md "wikilink")、田尻村、走島村、水呑村、津之鄉村、赤坂村、瀨戶村、熊野村、田島村、横島村、山南村、千年村。
      - [蘆田郡](../Page/蘆田郡.md "wikilink")：有磨村、福相村、常金丸村、藤尾村。
      - [品治郡](../Page/品治郡.md "wikilink")：江良村、倉光村、中島村、坊寺村、萬能倉村、近田村、服部村、宜山村、新市村、網引村、戶手村。
  - 1898年10月1日：
      - 深津郡和安那郡合併為[深安郡](../Page/深安郡.md "wikilink")。
      - 蘆田郡和品治郡合併為[蘆品郡](../Page/蘆品郡.md "wikilink")。
  - 1913年4月1日：野上村和三吉村被併入福山町。
  - 1913年7月1日：江良村、倉光村、中島村、坊寺村、萬能倉村合併為驛家村。
  - 1900年3月3日：松永村改制為松永町。
  - 1907年1月1日：新市村改制為[新市町](../Page/新市町.md "wikilink")。
  - 1916年7月1日：福山町改制為**福山市**。\[3\]\[4\]
  - 1926年11月1日：今津村改制為[今津町](../Page/今津町.md "wikilink")。
  - 1929年3月1日：川北村和川南村[合併為](../Page/市町村合併.md "wikilink")[神邊町](../Page/神邊町.md "wikilink")。
  - 1933年1月1日：川口村、手城村、深津村、奈良津村、吉津村、木之庄村、本庄村、神島村、佐波村、草戶村被併入福山市。
  - 1938年10月1日：上岩成村、下岩成村、中津原村、森脇村合併合併為御幸村（由於昭和天皇在1930年11月[行幸](../Page/行幸.md "wikilink")，故取此名）。
  - 1941年2月11日：上竹田村、下竹田村、八尋村合併合併為竹尋村。
  - 1941年10月1日：下加茂村、法成寺村合併合併為加法村。
  - 1942年7月1日：
      - 山手村、鄉分村被併入福山市。
      - 鞆町、田尻村、走島村合併為新設置的鞆町。
  - 1947年8月1日：水呑村改制為[水呑町](../Page/水呑町.md "wikilink")。
  - 1947年11月3日：驛家村改制為[驛家町](../Page/驛家町.md "wikilink")。
  - 1953年4月1日：今津町、松永町合併為新設置的松永町。
  - 1954年3月31日：
      - 松永町、柳津村、金江村、藤江村、東村、本鄉村、神村合併為[松永市](../Page/松永市.md "wikilink")。
      - 神邊町、竹尋村、中條村、道上村、御野村、湯田村合併合併為新設置的神邊町。
  - 1955年1月1日：驛家町、近田村、服部村、宜山村合併為新設置的驛家町。
  - 1955年2月1日：新市町、網引村、常金丸村、戶手村合併為新設置的新市町。
  - 1955年3月31日：
      - 加茂村、廣瀨村、山野村合併為[加茂町](../Page/加茂町.md "wikilink")。
      - 大津野村、春日村、坪生村合併合併為[深安町](../Page/深安町.md "wikilink")。
      - 田島村、横島村合併為[內海町](../Page/內海町_\(廣島縣\).md "wikilink")。
      - 山南村、千年村合併為[沼隈町](../Page/沼隈町.md "wikilink")。
      - 有磨村和福相村合併為[蘆田町](../Page/蘆田町.md "wikilink")。
  - 1956年9月30日：
      - 引野村、市村、千田村、御幸村、鞆町、水呑町、津之鄉村、赤坂村、瀨戶村、熊野村被併入福山市。
      - 加法村被廢除，轄區分別被併入加茂町和驛家町。
  - 1959年7月1日：藤尾村被廢除，轄區分別被併入新市町和[神石郡](../Page/神石郡.md "wikilink")[三和町](../Page/三和町_\(廣島縣神石郡\).md "wikilink")（現以合併為[神石高原町](../Page/神石高原町.md "wikilink")）。
  - 1962年1月1日：深安町被併入福山市。
  - 1966年5月1日：福山市與松永市合併為新設置的**福山市**。
  - 1974年4月1日：蘆田町被併入福山市。
  - 1975年2月1日：加茂町和驛家町被併入福山市。
  - 1998年4月1日：成為[中核市](../Page/中核市.md "wikilink")。
  - 2003年2月3日：內海町和新市町被併入福山市。
  - 2005年2月1日：沼隈町被併入福山市。
  - 2006年3月1日：神邊町被併入福山市。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1898年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>藤尾村</p></td>
<td><p>1959年7月1日<br />
併入三和町</p></td>
<td><p>2004年11月5日<br />
合併為神石高原町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1959年7月1日<br />
併入新市町</p></td>
<td><p>新市町</p></td>
<td><p>2003年2月3日<br />
併入福山市</p></td>
<td><p>福山市</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>常金丸村</p></td>
<td><p>1955年2月1日<br />
合併為新市町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>新市村</p></td>
<td><p>1907年1月1日<br />
新市町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>網引村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>戶田村</p></td>
<td><p>1895年9月21日<br />
戶手村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1895年9月21日<br />
近田村</p></td>
<td><p>1955年1月1日<br />
合併為驛家町</p></td>
<td><p>1975年2月1日<br />
併入福山市</p></td>
<td><p>福山市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>江良村</p></td>
<td><p>1913年7月1日<br />
合併為驛家村</p></td>
<td><p>1947年11月3日<br />
驛家町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>倉光村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>坊寺村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>萬能倉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>服部村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>宜山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>法成寺村</p></td>
<td><p>1941年10月1日<br />
合併為加法村</p></td>
<td><p>1956年9月30日<br />
併入驛家町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下加茂村</p></td>
<td><p>1956年9月30日<br />
併入加茂町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>加茂村</p></td>
<td><p>1955年3月31日<br />
合併為加茂町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>廣瀨村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>有磨村</p></td>
<td><p>1955年4月1日<br />
合併為蘆田町</p></td>
<td><p>1974年4月1日<br />
併入福山市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>福相村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大津野村</p></td>
<td><p>1955年3月31日<br />
合併為深安町</p></td>
<td><p>1962年1月1日<br />
併入福山市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>春日村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>坪生村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>福山町</p></td>
<td><p>1916年7月1日<br />
福山市</p></td>
<td><p>福山市</p></td>
<td><p>1966年0月1日<br />
合併為福山市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>野上村</p></td>
<td><p>1913年4月1日<br />
併入福山町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>三吉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>川口村</p></td>
<td><p>1933年1月1日<br />
併入福山市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>木之庄村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>手城村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>奈良津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>深津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>本庄村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>吉津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>神島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>草戶村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佐波村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>鄉分村</p></td>
<td><p>1942年7月1日<br />
併入福山市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山手村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>市村</p></td>
<td><p>1956年9月30日<br />
併入福山市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>千田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>引野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上岩成村</p></td>
<td><p>1938年10月1日<br />
合併為御幸村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下岩成村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>中津原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>森脇村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鞆町</p></td>
<td><p>1942年7月1日<br />
鞆町</p></td>
<td><p>1956年9月30日<br />
併入福山市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>田尻村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>走島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>水吞村</p></td>
<td><p>1947年8月1日<br />
水吞町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>赤坂村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>熊野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>瀨戶村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>津之鄉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| 松永村</p></td>
<td><p>1900年3月3日<br />
松永町</p></td>
<td><p>1953年4月1日<br />
松永町</p></td>
<td><p>1954年3月31日<br />
松永市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>今津村</p></td>
<td><p>1926年11月1日<br />
今津町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>神村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>金江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>東村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>藤江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>本鄉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>柳津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>田島村</p></td>
<td><p>1955年3月31日<br />
內海町</p></td>
<td><p>2003年2月3日<br />
併入福山市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>橫島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>千年村</p></td>
<td><p>1955年3月31日<br />
沼隈町</p></td>
<td><p>2005年2月1日<br />
併入福山市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>山南村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>川北村</p></td>
<td><p>1929年3月1日<br />
合併為神邊町</p></td>
<td><p>1954年3月31日<br />
合併為神邊町</p></td>
<td><p>2006年3月1日<br />
併入福山市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>川南村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上竹田村</p></td>
<td><p>1941年2月11日<br />
合併為竹尋村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下竹田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>八尋村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中條村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>道上村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>御野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>湯田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任市長

1.  阿武信一（1916年～1927年）
2.  中野有光（1927年～1938年）
3.  小林壽夫（1938年～1944年）
4.  三谷一二（1944年～1946年）
5.  小林和一（1946年～1947年）
6.  藤井正男（1947年～1955年）
7.  德永豐（1955年～1970年）
8.  立石定夫（1970年～1979年）
9.  中川弘（1979年～1983年）
10. 牧本幹男（1983年～1991年）
11. 三好章（1991年～2004年）
12. 羽田皓（2004年～現任）

## 交通

搭乘新幹線往[岡山車站最快](../Page/岡山站_\(日本\).md "wikilink")16分鐘可抵達，往[廣島車站約](../Page/廣島車站.md "wikilink")24分鐘，前往[新大阪車站或九州的](../Page/新大阪車站.md "wikilink")[小倉車站約一個小時可抵達](../Page/小倉車站_\(福岡縣\).md "wikilink")，而[東京車站則約需四小時](../Page/東京車站.md "wikilink")。

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [山陽新幹線](../Page/山陽新幹線.md "wikilink")：[福山車站](../Page/福山車站_\(日本\).md "wikilink")
      - [山陽本線](../Page/山陽本線.md "wikilink")：[大門車站](../Page/大門車站_\(廣島縣\).md "wikilink")
        - [東福山車站](../Page/東福山車站.md "wikilink") - 福山車站 -
        [備後赤坂車站](../Page/備後赤坂車站.md "wikilink") -
        [松永車站](../Page/松永車站.md "wikilink")
      - [福鹽線](../Page/福鹽線.md "wikilink")：福山車站 -
        [備後本庄車站](../Page/備後本庄車站.md "wikilink") -
        [橫尾車站](../Page/橫尾車站.md "wikilink") -
        [神邊車站](../Page/神邊車站.md "wikilink") -
        [湯田村車站](../Page/湯田村車站.md "wikilink")-
        [道上車站](../Page/道上車站.md "wikilink")-
        [萬能倉車站](../Page/萬能倉車站.md "wikilink") -
        [驛家車站](../Page/驛家車站.md "wikilink") -
        [近田車站](../Page/近田車站.md "wikilink") -
        [戶手車站](../Page/戶手車站.md "wikilink") -
        [上戶手車站](../Page/上戶手車站.md "wikilink") -
        [新市車站](../Page/新市車站_\(廣島縣\).md "wikilink")
  - [井原鐵道](../Page/井原鐵道.md "wikilink")
      - [井原線](../Page/井原線.md "wikilink")：[神邊車站](../Page/神邊車站.md "wikilink")
        - [湯野車站](../Page/湯野車站.md "wikilink")-
        [御領車站](../Page/御領車站_\(廣島縣\).md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [山陽自動車道](../Page/山陽自動車道.md "wikilink")：[福山東交流道](../Page/福山東交流道.md "wikilink")
    - [福山服務區](../Page/福山服務區.md "wikilink") -
    [福山西交流道](../Page/福山西交流道.md "wikilink")

### 港口

  - [福山港](../Page/福山港.md "wikilink")
      - 鞆港－[走島航線](../Page/走島.md "wikilink")
      - 鞆港－[仙醉島航線](../Page/仙醉島.md "wikilink")

## 觀光資源

  - [明王院五重塔](../Page/明王院.md "wikilink")
  - 明王院本堂
  - [福山城遺跡](../Page/福山城.md "wikilink")
  - 宮之前廢寺遺跡
  - 朝鮮通信使遺跡
  - 備後一宮
  - 鞆公園（鞆之浦）：[瀨戶內海國立公園最初的範圍](../Page/瀨戶內海國立公園.md "wikilink")。

## 教育

### 大學、短期大學

  - [福山大學](../Page/福山大學.md "wikilink")
  - [福山平成大學](../Page/福山平成大學.md "wikilink")
  - [放送大學福山學習中心](../Page/放送大學.md "wikilink")
  - [福山市立女子短期大學](../Page/福山市立女子短期大學.md "wikilink")

### 高等學校

  - [廣島大學附屬福山高等學校](../Page/廣島大學附屬福山中學校、高等學校.md "wikilink")
  - [廣島縣立福山誠之館高等學校](../Page/廣島縣立福山誠之館高等學校.md "wikilink")
  - [廣島縣立福山葦陽高等學校](../Page/廣島縣立福山葦陽高等學校.md "wikilink")
  - [廣島縣立福山明王台高等學校](../Page/廣島縣立福山明王台高等學校.md "wikilink")
  - [廣島縣立松永高等學校](../Page/廣島縣立松永高等學校.md "wikilink")
  - [廣島縣立大門高等學校](../Page/廣島縣立大門高等學校.md "wikilink")
  - [廣島縣立自彊高等學校](../Page/廣島縣立自彊高等學校.md "wikilink")
  - [廣島縣立福山工業高等學校](../Page/廣島縣立福山工業高等學校.md "wikilink")
  - [廣島縣立福山商業高等學校](../Page/廣島縣立福山商業高等學校.md "wikilink")
  - [廣島縣立戶手高等學校](../Page/廣島縣立戶手高等學校.md "wikilink")
  - [廣島縣立東高等學校](../Page/廣島縣立東高等學校.md "wikilink")

<!-- end list -->

  - [廣島縣立蘆品學習學園高等學校](../Page/廣島縣立蘆品學習學園高等學校.md "wikilink")
  - [廣島縣立沼南高等學校](../Page/廣島縣立沼南高等學校.md "wikilink")
  - [廣島縣立神邊旭高等學校](../Page/廣島縣立神邊旭高等學校.md "wikilink")
  - [廣島縣立神邊高等學校](../Page/廣島縣立神邊高等學校.md "wikilink")
  - [福山市立福山高等學校](../Page/福山市立福山中、高等學校.md "wikilink")
  - [近畿大學附屬福山高等學校](../Page/近畿大學附屬福山高等學校.md "wikilink")
  - [盈進高等學校](../Page/盈進中學校、盈進高等學校.md "wikilink")
  - [英數學館高等學校](../Page/英數學館高等學校.md "wikilink")
  - [銀河學院高等學校](../Page/銀河學院中學校、高等學校.md "wikilink")
  - [福山曉之星女子高等學校](../Page/福山曉之星女子高等學校.md "wikilink")
  - [福山英數學館](../Page/福山英數學館.md "wikilink")

### 特殊学校

  - [廣島縣立福山特別支援學校](../Page/廣島縣立福山特別支援學校.md "wikilink")
  - [廣島縣立福山北特別支援學校](../Page/廣島縣立福山北特別支援學校.md "wikilink")
  - [廣島縣立沼隈特別支援學校](../Page/廣島縣立沼隈特別支援學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [岡崎市](../Page/岡崎市.md "wikilink")（[愛知縣](../Page/愛知縣.md "wikilink")）
      -
        出生於岡崎的[德川家康與第一代](../Page/德川家康.md "wikilink")[福山藩主](../Page/福山藩.md "wikilink")[水野勝成為表親](../Page/水野勝成.md "wikilink")，兩地因而締結。\[5\]

### 海外

  - [哈密尔顿](../Page/哈密尔顿_\(安大略省\).md "wikilink")（[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")）

      -
        兩地同為煉鋼產業重鎮，因此兩地於1976年締結為友好都市。

  - [浦項市](../Page/浦項市.md "wikilink")（[韓國](../Page/韓國.md "wikilink")[慶尚北道](../Page/慶尚北道.md "wikilink")）

      -
        兩地同為煉鋼產業重鎮，因此兩地於1979年締結為友好都市。

  - [独鲁万市](../Page/独鲁万市.md "wikilink")（[菲律賓](../Page/菲律賓.md "wikilink")[礼智省](../Page/礼智省.md "wikilink")）

      -
        [二次大戰期間](../Page/二次大戰.md "wikilink")，成立於福山市的舊[帝國陸軍](../Page/帝國陸軍.md "wikilink")[第30師團](../Page/第30師團.md "wikilink")[歩兵第41連隊最終於此地全滅](../Page/歩兵第41連隊.md "wikilink")；由於會固定至独鲁万市進行祭拜，兩地於1980年締結為友好都市。

  - [茂宜郡](../Page/茂宜郡_\(夏威夷州\).md "wikilink")（[美國](../Page/美國.md "wikilink")[夏威夷州](../Page/夏威夷州.md "wikilink")）

      -
        戰前許多居民移民至此，因此兩地於2008年5月17日締結為友好都市。

## 本地出身之名人

### 政治

  - [宮澤裕](../Page/宮澤裕.md "wikilink")：[政治家](../Page/政治家.md "wikilink")、宮澤喜一之父
  - [宮澤喜一](../Page/宮澤喜一.md "wikilink")：第78任[內閣總理大臣](../Page/內閣總理大臣_\(日本\).md "wikilink")
  - [宮澤洋一](../Page/宮澤洋一.md "wikilink")：[參議院議員](../Page/參議院_\(日本\).md "wikilink")
  - [頼母木桂吉](../Page/頼母木桂吉.md "wikilink")：前[東京市長](../Page/東京市長.md "wikilink")
  - [丸山鶴吉](../Page/丸山鶴吉.md "wikilink")：曾任[警視總監](../Page/警視總監.md "wikilink")、[宮城縣知事](../Page/宮城縣知事.md "wikilink")
  - [井上角五郎](../Page/井上角五郎.md "wikilink")：企業家、前[眾議院議員](../Page/眾議院_\(日本\).md "wikilink")
  - [森下博](../Page/森下博.md "wikilink")：[森下仁丹創辦人](../Page/森下仁丹.md "wikilink")
  - [佐藤勇二](../Page/佐藤勇二.md "wikilink")：[三菱自動車工業初代社長](../Page/三菱自動車工業.md "wikilink")
  - [涉谷昇](../Page/涉谷昇.md "wikilink")：[福山通運](../Page/福山通運.md "wikilink")、[Vessel創辦人](../Page/Vessel.md "wikilink")，成立福山通運涉谷長壽健康財團、財團法人涉谷育英會。

### 文化人

  - [井伏鱒二](../Page/井伏鱒二.md "wikilink")：[小説家](../Page/小説家.md "wikilink")
  - [小山祐士](../Page/小山祐士.md "wikilink")：[編劇](../Page/編劇.md "wikilink")
  - [葛原しげる](../Page/葛原しげる.md "wikilink")：[童謠](../Page/童謠.md "wikilink")[作詞家](../Page/作詞家.md "wikilink")
  - [武田五一](../Page/武田五一.md "wikilink")：[建築師](../Page/建築師.md "wikilink")
  - [藤井厚二](../Page/藤井厚二.md "wikilink")：[建築師](../Page/建築師.md "wikilink")
  - [藤本壽德](../Page/藤本壽德.md "wikilink")：[建築師](../Page/建築師.md "wikilink")
  - [榮久庵憲司](../Page/榮久庵憲司.md "wikilink")：工業設計師
  - [清水郁太郎](../Page/清水郁太郎.md "wikilink")：醫学博士
  - [佐野俊二](../Page/佐野俊二.md "wikilink")：醫學研究者
  - [大島衣恵](../Page/大島衣恵.md "wikilink")：[喜多流](../Page/喜多流.md "wikilink")[能樂師](../Page/能樂師.md "wikilink")
  - [寺田和正](../Page/寺田和正.md "wikilink")：[企業家](../Page/企業家.md "wikilink")
  - [島田莊司](../Page/島田莊司.md "wikilink")：[推理作家](../Page/推理作家.md "wikilink")
  - [友竹正則](../Page/友竹正則.md "wikilink")：[聲樂家](../Page/聲樂家.md "wikilink")
  - [藤岡宣男](../Page/藤岡宣男.md "wikilink")：[聲樂家](../Page/聲樂家.md "wikilink")
  - [藤家虹二](../Page/藤家虹二.md "wikilink")：音樂家
  - [柳井隆雄](../Page/柳井隆雄.md "wikilink")：[編劇](../Page/編劇.md "wikilink")
  - [中野裕之](../Page/中野裕之.md "wikilink")：[電影導演](../Page/電影導演.md "wikilink")
  - [羽原信義](../Page/羽原信義.md "wikilink")：動畫導演
  - [石田敦子](../Page/石田敦子.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
  - [滿田拓也](../Page/滿田拓也.md "wikilink")：漫畫家
  - [とだ勝之](../Page/とだ勝之.md "wikilink")：漫畫家
  - [箱田真紀](../Page/箱田真紀.md "wikilink")：漫畫家
  - [松田洋子](../Page/松田洋子.md "wikilink")：漫畫家
  - [羽仁礼](../Page/羽仁礼.md "wikilink")：[報告文學作家](../Page/報告文學.md "wikilink")
  - [ヒロハラノブヒコ](../Page/ヒロハラノブヒコ.md "wikilink")：[放送作家](../Page/放送作家.md "wikilink")

### 演藝

  - [井龜明彥](../Page/井龜明彥.md "wikilink")：音樂人
  - [エムラミノル](../Page/エムラミノル.md "wikilink")：音樂人
  - [大本彩乃](../Page/大本彩乃.md "wikilink")([Perfume](../Page/Perfume.md "wikilink"))：歌手
  - [岡峰光舟](../Page/岡峰光舟.md "wikilink")：[贝斯手](../Page/贝斯手.md "wikilink")
  - [緒川たまき](../Page/緒川たまき.md "wikilink")：[女演員](../Page/女演員.md "wikilink")
  - [小原春香](../Page/小原春香.md "wikilink")：前[AKB48](../Page/AKB48.md "wikilink")、前[SDN48成員](../Page/SDN48.md "wikilink")
  - [ak](../Page/柿原朱美.md "wikilink")：音樂人、[作曲家](../Page/作曲家.md "wikilink")
  - [金兒憲史](../Page/金兒憲史.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [京野ことみ](../Page/京野ことみ.md "wikilink")：女演員
  - [國岡真由美](../Page/國岡真由美.md "wikilink")：音樂人
  - [小林克也](../Page/小林克也.md "wikilink")：俳優
  - [五味岡たまき](../Page/五味岡たまき.md "wikilink")：歌手
  - [崎谷健次郎](../Page/崎谷健次郎.md "wikilink")：音樂人
  - [白石知世](../Page/白石知世.md "wikilink")：[藝人](../Page/藝人.md "wikilink")
  - [杉原杏璃](../Page/杉原杏璃.md "wikilink")：藝人
  - [世良公則](../Page/世良公則.md "wikilink")：音樂人、俳優
  - [樽木栄一郎](../Page/樽木栄一郎.md "wikilink")：音樂人
  - [寺岡呼人](../Page/寺岡呼人.md "wikilink")：音樂人
  - [西田尚美](../Page/西田尚美.md "wikilink")：女演員
  - [速水けんたろう](../Page/速水けんたろう.md "wikilink")：藝人
  - [福山潤](../Page/福山潤.md "wikilink")：[配音員](../Page/配音員.md "wikilink")
  - [藤井謙二](../Page/藤井謙二.md "wikilink")：音樂人
  - [星奈優里](../Page/星奈優里.md "wikilink")：女演員
  - [町本絵里](../Page/町本絵里.md "wikilink")：女演員
  - [三谷昇](../Page/三谷昇.md "wikilink")：俳優
  - [もんたよしのり](../Page/もんたよしのり.md "wikilink")：音樂人
  - [橫山知枝](../Page/橫山知枝.md "wikilink")：女演員
  - [Mas Sawada](../Page/Mas_Sawada.md "wikilink")：音樂人
  - [及川奈央](../Page/及川奈央.md "wikilink")：藝人

### 體育選手

  - [村田兆治](../Page/村田兆治.md "wikilink")：前職業棒球選手
  - [小川邦和](../Page/小川邦和.md "wikilink")：前職業棒球選手
  - [藤井康雄](../Page/藤井康雄.md "wikilink")：前職業棒球選手
  - [浅野啓司](../Page/浅野啓司.md "wikilink")：前職業棒球選手
  - [箱田淳](../Page/箱田淳.md "wikilink")：前職業棒球選手
  - [神崎安隆](../Page/神崎安隆.md "wikilink")：前職業棒球選手
  - [江草仁貴](../Page/江草仁貴.md "wikilink")：職業棒球選手
  - [上本博紀](../Page/上本博紀.md "wikilink")：職業棒球選手
  - [橋誠](../Page/橋誠.md "wikilink")：職業摔角選手
  - [藤井克久](../Page/藤井克久.md "wikilink")：職業摔角選手
  - [石井義信](../Page/石井義信.md "wikilink")：前日本足球代表隊教練
  - [兼田亞季重](../Page/兼田亞季重.md "wikilink")：職業足球選手
  - [森脇良太](../Page/森脇良太.md "wikilink")：職業足球選手
  - [森孝慈](../Page/森孝慈.md "wikilink")：前日本足球代表隊選手
  - [古邊考功](../Page/古邊考功.md "wikilink")：前職業足球選手
  - [上村健一](../Page/上村健一.md "wikilink")：前職業足球選手
  - [桑田慎一朗](../Page/桑田慎一朗.md "wikilink")：職業足球手
  - [ダ・シルバ・ファビオ・岡](../Page/ダ・シルバ・ファビオ・岡.md "wikilink")：職業足球選手
  - [遠藤愛](../Page/遠藤愛.md "wikilink")：前職業網球選手
  - [島田裕二](../Page/島田裕二.md "wikilink")：摔角選手
  - [有馬隼人](../Page/有馬隼人.md "wikilink")：[美式足球選手](../Page/美式足球.md "wikilink")
  - [家本政明](../Page/家本政明.md "wikilink")：足球才辦
  - [麻生慎介](../Page/麻生慎介.md "wikilink")：競艇選手
  - [市川哲也](../Page/市川哲也.md "wikilink")：競艇選手
  - [黑川知弘](../Page/黑川知弘.md "wikilink")：福山競馬騎手

### 其他

  - [水野勝成](../Page/水野勝成.md "wikilink")：第一代[福山藩藩主](../Page/備後福山藩.md "wikilink")
  - [阿部正弘](../Page/阿部正弘.md "wikilink")：福山藩主、曾為[德川幕府](../Page/江戶幕府.md "wikilink")[老中首座](../Page/老中.md "wikilink")，締結了[日美和親條約](../Page/日美和親條約.md "wikilink")。

## 參考資料

## 外部連結

  - [福山市觀光協會](http://www.fukuyama-kanko.com/)

  - [JA福山市](http://www.jafukuyama.or.jp/)

  -
<!-- end list -->

1.

2.
3.

4.

5.