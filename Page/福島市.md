**福島市**（）是[日本](../Page/日本.md "wikilink")[東北地方](../Page/東北地方.md "wikilink")[福島縣中北部的一個城市](../Page/福島縣.md "wikilink")，福島縣[縣廳所在地](../Page/縣廳所在地.md "wikilink")。福島市內人口位于縣內第三位，僅次于[磐城市和](../Page/磐城市.md "wikilink")[郡山市](../Page/郡山市.md "wikilink")。以福島市為中心的[福島都市圈人口有](../Page/福島都市圈.md "wikilink")41萬，僅次于[郡山都市圈位居縣內第二位](../Page/郡山都市圈.md "wikilink")（東北地方内6位）。

於2008年7月1日將鄰近的[飯野町併入](../Page/飯野町.md "wikilink")。

## 人口

289,007人（推計人口、2018年10月1日）

## 教育

### 大学・短期大学

  - [福島大學](../Page/福島大學.md "wikilink")

  - [福島縣立医科大学](../Page/福島縣立醫科大學.md "wikilink")

  -
  - 櫻之聖母学院短期大学

### 高等学校

#### 県立

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li><p>（<a href="../Page/定時制.md" title="wikilink">定時制</a>）</p></li>
</ul></td>
</tr>
</tbody>
</table>

#### 私立

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li>有朋学園東日本高等学院</li>
<li>尚志高等学校</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光

### 文化財

#### 重要文化遗产（国家指定）

  - [旧廣瀬座](../Page/旧廣瀬座.md "wikilink")（明治期の[歌舞伎座](../Page/歌舞伎.md "wikilink")：[伊達市](../Page/伊達市_\(福島縣\).md "wikilink")[梁川町から移築](../Page/梁川町.md "wikilink")）
    - 福島市民家園
  - [木像十一面千手観音立像](../Page/木像十一面千手観音立像.md "wikilink") -
    小倉寺字捨石、[大蔵寺](../Page/大蔵寺.md "wikilink")
  - [陶製經筒](../Page/陶製經筒.md "wikilink") -
    飯坂町字天王寺、[天王寺](../Page/天王寺.md "wikilink")
  - [鍍金金剛鈴](../Page/鍍金金剛鈴.md "wikilink")（ときんこんごうれい）、[金剛杵](../Page/金剛杵.md "wikilink")（こんごうしょ）
    - 清明町、[真浄院](../Page/真浄院.md "wikilink")
  - [木造釋迦如來坐像](../Page/木造釋迦如來坐像.md "wikilink") -
    下鳥渡字寺東、[陽泉寺](../Page/陽泉寺.md "wikilink")（）

### 温泉

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/飯坂温泉.md" title="wikilink">飯坂温泉</a></li>
<li><a href="../Page/穴原温泉.md" title="wikilink">穴原温泉</a></li>
<li><a href="../Page/広瀬温泉.md" title="wikilink">広瀬温泉</a></li>
<li><a href="../Page/土湯温泉.md" title="wikilink">土湯温泉</a></li>
<li><a href="../Page/奥土湯温泉郷.md" title="wikilink">奥土湯温泉郷</a></li>
<li><a href="../Page/高湯温泉.md" title="wikilink">高湯温泉</a></li>
<li><a href="../Page/微温湯温泉.md" title="wikilink">微温湯温泉</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/信夫温泉.md" title="wikilink">信夫温泉</a></li>
<li><a href="../Page/土湯峠温泉郷.md" title="wikilink">土湯峠温泉郷</a>
<ul>
<li><a href="../Page/野地温泉.md" title="wikilink">野地温泉</a></li>
<li><a href="../Page/新野地温泉.md" title="wikilink">新野地温泉</a></li>
<li><a href="../Page/鷲倉温泉.md" title="wikilink">鷲倉温泉</a></li>
<li><a href="../Page/幕川温泉.md" title="wikilink">幕川温泉</a></li>
<li><a href="../Page/赤湯温泉.md" title="wikilink">赤湯温泉</a></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 福島市名人

### 福島市出身者

#### 政治家

  - [堀切善兵衛](../Page/堀切善兵衛.md "wikilink") -
    第26代[衆議院議長](../Page/衆議院議長.md "wikilink")
  - [堀切善次郎](../Page/堀切善次郎.md "wikilink") -
    第13代[東京](../Page/東京市.md "wikilink")[市長](../Page/市長.md "wikilink")
  - [佐藤善一郎](../Page/佐藤善一郎.md "wikilink") -
    前福島縣[知事](../Page/知事.md "wikilink")
  - [岡野裕](../Page/岡野裕.md "wikilink") -
    前[勞動大臣](../Page/勞動大臣.md "wikilink")
  - [阿部孝夫](../Page/阿部孝夫.md "wikilink") -
    [川崎市長](../Page/川崎市.md "wikilink")
  - [熊坂義裕](../Page/熊坂義裕.md "wikilink") -
    [宮古市長](../Page/宮古市.md "wikilink")
  - [岡崎富子](../Page/岡崎富子.md "wikilink") -
    [參議院議員](../Page/參議院議員.md "wikilink")、前[民主党副代表](../Page/民主黨_\(1996年\).md "wikilink")、前播音員

#### 作曲家

  - [菅野茂](../Page/菅野茂.md "wikilink") - [指揮者](../Page/指揮者.md "wikilink")
  - [湯浅譲二](../Page/湯浅譲二.md "wikilink")
  - [遠藤武雄](../Page/遠藤武雄.md "wikilink")
  - [須田邦夫](../Page/須田邦夫.md "wikilink")
  - [伊藤英直](../Page/伊藤英直.md "wikilink")

#### 偶像

  - [權梨世](../Page/權梨世.md "wikilink") - 曾是女子團體[Ladies'
    Code一員](../Page/Ladies'_Code.md "wikilink")

[Category:福島市](../Category/福島市.md "wikilink")