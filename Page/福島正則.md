**福島正則**（、[永祿四年](../Page/永祿.md "wikilink")－[寬永元年](../Page/寬永.md "wikilink")[七月十三](../Page/七月十三.md "wikilink")）是[安土桃山時代](../Page/安土桃山時代.md "wikilink")、[江戶時代的武將和大名](../Page/江戶時代.md "wikilink")。父親是[福島正信](../Page/福島正信.md "wikilink")，母親是[豐臣秀吉的叔母](../Page/豐臣秀吉.md "wikilink")。幼名市松。

## 經歷

[Hukushima_Masanori.JPG](https://zh.wikipedia.org/wiki/File:Hukushima_Masanori.JPG "fig:Hukushima_Masanori.JPG")
1561年（[永祿](../Page/永祿.md "wikilink")4年）出生於[尾張國海東郡](../Page/尾張國.md "wikilink")（今[愛知縣](../Page/愛知縣.md "wikilink")[海部市](../Page/海部市.md "wikilink")）。由於與[羽柴秀吉](../Page/羽柴秀吉.md "wikilink")（豐臣秀吉）有親戚關係，年幼時已為秀吉的小姓。正則首次上戰場是秀吉攻打[三木城](../Page/三木城.md "wikilink")，獲得200石之行。其後參加[中國征伐和](../Page/中国地方.md "wikilink")[山崎之戰](../Page/山崎之戰.md "wikilink")，在山崎之戰立下戰功，增加為500石的知行。之後在[賤岳之戰中](../Page/賤岳之戰.md "wikilink")，斬殺了[柴田勝家的大將](../Page/柴田勝家.md "wikilink")[拜鄉家嘉](../Page/拜鄉家嘉.md "wikilink")，立下戰功，當中福島正則加俸祿被增至5000石，其餘六人為3000石，為[賤岳七本槍之首](../Page/賤岳七本槍.md "wikilink")。後參加攻擊[根來寺的戰役](../Page/根來寺.md "wikilink")，在秀吉完成[四國征伐後為](../Page/四國征伐.md "wikilink")[伊予國今治](../Page/伊予國.md "wikilink")11萬石大名。

1592年福島正則出-{征}-朝鮮參與[文祿之役](../Page/文祿之役.md "wikilink")，是第五隊的主將，率領[戶田勝隆](../Page/戶田勝隆.md "wikilink")、[長宗我部元親](../Page/長宗我部元親.md "wikilink")、[蜂須賀家政](../Page/蜂須賀家政.md "wikilink")、[生駒親正](../Page/生駒親正.md "wikilink")、[來島通總等人](../Page/來島通總.md "wikilink")，攻擊[京畿道](../Page/京畿道.md "wikilink")，在年末正則的部隊留守京幾道。1594年在場門浦[李舜臣交戰](../Page/李舜臣.md "wikilink")，被李舜臣所擊敗。1595年，由於朝鮮戰役的功績，正則被封於[尾張](../Page/尾張國.md "wikilink")[清洲城](../Page/清洲城.md "wikilink")24萬石的大名\[1\]。

1599年[五大老](../Page/五大老.md "wikilink")[前田利家死後](../Page/前田利家.md "wikilink")，與朝鮮戰役時與[石田三成的文治派交惡](../Page/石田三成.md "wikilink")，於1599年與其餘六人策劃襲擊[石田三成](../Page/石田三成.md "wikilink")，這計劃最終未能成功。在這個時候其養子正之與德川家康養女[滿天姬結婚](../Page/滿天姬.md "wikilink")。一年後的[關原之戰中屬東軍先鋒](../Page/關原之戰.md "wikilink")，正則原定跟隨家康討伐會津國大名[上杉景勝](../Page/上杉景勝.md "wikilink")。後來石田三成起兵，大軍返回近畿，福島正則成為返回近畿的先鋒，在[岐阜城之戰與前哨隊](../Page/岐阜城.md "wikilink")[池田輝政立下了大功](../Page/池田輝政.md "wikilink")。在關原之戰中希望與石田三成交鋒，但最終沒有實現，戰場上與[宇喜多秀家隊交鋒](../Page/宇喜多秀家.md "wikilink")，雙方一直保持均勢，直到[小早川秀秋背叛西軍戰況有所改變而取得勝利](../Page/小早川秀秋.md "wikilink")。在戰後的論功行賞，正則獲得了[安藝](../Page/安藝國.md "wikilink")、[廣島](../Page/廣島藩.md "wikilink")、備後等三國49萬石大名。

慶長6年（1601年）3月正則進入封地、首先進行領内巡檢、重新計算領地的石高，推行「事實上給米制」、檢地的結果鼓勵農民公開年度實際收成，並據以進行年貢的徴收，大大減輕農民負擔、此外，也致力於領内寺社的重建與保護、慶長7年（1602年）從事嚴島神社的平家納經的修復工作。檢地結果領地石高為51萬5800。江戶時代初期，參與多項築城工作。慶長8年（1603年）、於安藝最西端著手建築亀居城。該城在毛利領最東端的岩國城對岸、扼守山陽道的交通要地。

1611年（慶長16年）促成[豐臣秀賴與](../Page/豐臣秀賴.md "wikilink")[德川家康在](../Page/德川家康.md "wikilink")[京都](../Page/京都.md "wikilink")[二條城會面](../Page/二條城.md "wikilink")，成功說服反對的[淺野長政和](../Page/淺野長政.md "wikilink")[-{淀}-殿](../Page/淀殿.md "wikilink")，但會面當天以生病為由沒有同席，只在城外派遣部隊一萬人戒備。該次會面後，加藤清正、浅野長政、幸長父子、池田輝政等親豐臣的大名相繼死去、正則自身於慶長17年（1612年）以生病為由開始隠居。

在[大坂之役中](../Page/大坂之役.md "wikilink")，豐臣秀賴希望他派兵加入豐臣方，但正則沒有答應，僅對於豐臣家自行接收了廣島藩在大阪糧食倉庫八萬石藏米一事睜一隻眼閉一隻眼，加以默認。不過也因為這件事，正則提出希望加入東軍時，德川家康不予同意，命令他留在江戶城。但同族的福島正守、福島正鎮加入豊臣軍。嫡男福島忠勝則率兵加入幕府軍。然而，戰後、正則的弟弟福島高晴被發現與豊臣家内通、遭受幕府改易。

1619年因為颱風和暴雨的影響，向幕府提出申請修築廣島城的許可令，同時自行緊急維修損毀部分，事後幕府指責他沒有正式申請，違反[武家諸法度](../Page/武家諸法度.md "wikilink")。人在江戶的正則趕緊向幕府謝罪，並表示會把擅自修改的部分破壞，但事後幕府指責他並未如實破壞擅修處，將他所擁有的廣島50萬石沒收\[2\]。據説正則因爲酒後妄言反對幕府的言論，所以被[德川秀忠沒收領地](../Page/德川秀忠.md "wikilink")。

初時幕府打算將正則父子移封至[陸奧國津輕一帶](../Page/陸奧國.md "wikilink")，在遭到當地總治者[津輕信枚反對及](../Page/津輕信枚.md "wikilink")[牧野忠成指出陸奧一帶較遠](../Page/牧野忠成.md "wikilink")，最後正則與其子[福島忠勝移封到](../Page/福島忠勝.md "wikilink")[信濃國川中島四郡的高井郡及越後國魚沼郡](../Page/信濃國.md "wikilink")（高井野藩），封秩4萬5千石。被改易後，將家督讓給嫡子忠勝後出家，號高齋。1620年將其中2萬5千石收入交給忠勝，同年忠勝病逝，其領地收入交還給幕府。

1624年（[寬永元年](../Page/寬永.md "wikilink")）福島正則病逝，在高井野生活的5年間，留下領内的總檢地、用水設置改善與新田畝開發、並致力於治水工事等等功績。家臣津田四郎兵衛在幕府的檢死役堀田正吉到達前，便將其屍體火葬，為此受到幕府追究，將剩餘的二萬石沒收，僅給予他的三子[正利](../Page/福島正利.md "wikilink")3112石，以[旗本身分繼續為幕府效力](../Page/旗本.md "wikilink")。一說正則是受不了一連串的屈辱而切腹自殺，家人為了隱瞞事實才緊急將他火化。

死後的戒名是海福寺殿月翁正印大居士。墓地在[長野縣](../Page/長野縣.md "wikilink")[小布施町梅洞山岩松院](../Page/小布施町.md "wikilink")、正則廟在[京都市](../Page/京都市.md "wikilink")[右京區妙心寺海福院](../Page/右京區.md "wikilink")、[和歌山縣](../Page/和歌山縣.md "wikilink")[高野町](../Page/高野町.md "wikilink")[高野山悉地院](../Page/高野山.md "wikilink")、[廣島市](../Page/廣島市.md "wikilink")[東區新日山不動院及](../Page/東區_\(廣島市\).md "wikilink")[愛知縣](../Page/愛知縣.md "wikilink")[海部郡](../Page/海部郡.md "wikilink")[美和町菊泉院五處](../Page/美和町.md "wikilink")。

寛永14年（1637年），福島正利無子嗣死去。福島氏由福島忠勝的孫子正勝繼嗣。

## 官職

  - [天正](../Page/天正.md "wikilink")13年[7月16日](../Page/七月十六.md "wikilink")（1585年8月11日）從五位下左衛門大夫。
  - [慶長](../Page/慶長.md "wikilink")2年[7月26日](../Page/七月廿六.md "wikilink")（1597年9月7日），任官侍從。
  - 慶長7年[3月7日](../Page/三月初七.md "wikilink")（1602年4月28日），左近衛権少將（從四位下同時就任）。
  - [元和](../Page/元和_\(日本\).md "wikilink")3年[6月21日](../Page/六月廿一.md "wikilink")（1617年7月23日）、從三位參議。同年11月辭去參議一職。

## 家臣

  - [可兒才藏](../Page/可兒才藏.md "wikilink")
  - [尾關正勝](../Page/尾關正勝.md "wikilink")
  - [大崎長行](../Page/大崎長行.md "wikilink")
  - [志賀親次](../Page/志賀親次.md "wikilink")
  - [大道寺直次](../Page/大道寺直次.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

[Category:戰國大名](../Category/戰國大名.md "wikilink")
[Category:福島氏](../Category/福島氏.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Category:廣島藩](../Category/廣島藩.md "wikilink")

1.  Berry, Mary Elizabeth. *Hideyoshi*. Cambridge: Harvard University
    Press, pp. 127-8.
2.  日本古城建築圖典 三浦正幸普、詹慕如譯 p229-230 商周出版 ISBN 978-986-6662-04-1