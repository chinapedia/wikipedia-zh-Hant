**福島車站**（）是一由[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）所經營的[鐵路車站](../Page/鐵路車站.md "wikilink")，位於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[福島區福島七丁目](../Page/福島區.md "wikilink")，是JR西日本所屬的[大阪環狀線沿線車站之一](../Page/大阪環狀線.md "wikilink")。福島車站是JR西日本旗下大阪近郊鐵路路線群[都市網路](../Page/都市網路.md "wikilink")（，Urban
Network）所屬的車站，根據[JR的](../Page/JR.md "wikilink")[特定都區市內制度](../Page/特定都區市內.md "wikilink")，被劃分為「大阪市內」的車站之一，是JR西日本的直營車站之一，但委由[西九條車站管理](../Page/西九條車站.md "wikilink")。且由於JR系統之內還有另外一個同名的[福島車站](../Page/福島車站_\(福島縣\).md "wikilink")（[福島縣](../Page/福島縣.md "wikilink")[福島市的主車站](../Page/福島市.md "wikilink")），為了區別，在車票上會以「(環)
福島」的方式標示本站，其中「環」字是所屬的大阪環狀線之意。

福島車站鄰近地區還存在有兩個名稱相同或近似的車站——位於[阪神本線上的](../Page/阪神本線.md "wikilink")[同名車站](../Page/福島車站_\(阪神\).md "wikilink")，與位於[JR東西線上的](../Page/JR東西線.md "wikilink")[新福島車站](../Page/新福島車站.md "wikilink")（位於[JR東西線上](../Page/JR東西線.md "wikilink")）。位於地底、彼此僅有一個路口之隔的阪神福島與新福島兩車站與本站之間並無直接相連，之間相隔約100公尺左右的步行距離。

## 車站結構

只限旅客線設有[島式月台](../Page/島式月台.md "wikilink")1面2線的[高架車站](../Page/高架車站.md "wikilink")。由於不設[轉轍器與絕對信號機](../Page/轉轍器.md "wikilink")，被分類為停留所。閘口位於地面。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>大阪環状線</p></td>
<td><p>內環</p></td>
<td><p><a href="../Page/西九條站.md" title="wikilink">西九條</a>、<a href="../Page/新今宮站.md" title="wikilink">新今宮方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>外環</p></td>
<td><p><a href="../Page/大阪站.md" title="wikilink">大阪</a>、<a href="../Page/京橋站_(大阪府).md" title="wikilink">京橋方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 相鄰車站

  - 西日本旅客鐵道

    大阪環狀線

      -

        大和路快速、關空快速、紀州路快速、快速（所有列車此站起往大阪方向各站停車）

          -
            [大阪](../Page/大阪站.md "wikilink")（JR-O11）－**福島（JR-O12）**－[西九條](../Page/西九條站.md "wikilink")（JR-O14）

        區間快速、直通快速（只限外環運行）、普通

          -
            大阪（JR-O11）－**福島（JR-O12）**－[野田](../Page/野田站_\(JR西日本\).md "wikilink")（JR-O13）

<!-- end list -->

  -
    東海道本線貨物支線（梅田貨物線，旅客運行只限通過列車，沒有上下車設備）
      -

          -

            －（**福島**）－西九條（大阪環狀線）

## 外部連結

  - [福島車站（JR西日本）](http://www.jr-odekake.net/eki/top.php?id=0610501)

[Kushima](../Category/日本鐵路車站_Fu.md "wikilink")
[Category:大阪環狀線車站](../Category/大阪環狀線車站.md "wikilink")
[Category:1898年启用的铁路车站](../Category/1898年启用的铁路车站.md "wikilink")
[Category:福島區鐵路車站](../Category/福島區鐵路車站.md "wikilink")