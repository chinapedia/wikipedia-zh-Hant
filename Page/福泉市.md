**福泉市**位于[中国](../Page/中国.md "wikilink")[贵州省中部](../Page/贵州省.md "wikilink")，[黔南布依族苗族自治州北部](../Page/黔南布依族苗族自治州.md "wikilink")，[贵阳市以东约](../Page/贵阳市.md "wikilink")60千米处。介于东经107°14′24″--107°45′35″和北纬26°32′28″--27°02′23″之间。东邻[凯里市和](../Page/凯里市.md "wikilink")[黄平县](../Page/黄平县.md "wikilink")，南与[麻江县接壤](../Page/麻江县.md "wikilink")，西界[贵定](../Page/贵定.md "wikilink")、[龙里](../Page/龙里.md "wikilink")、[开阳三县](../Page/开阳县.md "wikilink")，北与[瓮安县相连](../Page/瓮安县.md "wikilink")。南北最长55.2千米，东西最宽52.1千米。总面积1688平方千米。全市辖9镇6乡2个办事处16个居委会60个村委会。总人口31.67万人，其中非农业人口6.2万人，少数民族人口8.54万人。有汉、苗、布依、侗、彝、水等25个民族。

## 历史

[殷](../Page/殷商.md "wikilink")[周时期](../Page/周朝.md "wikilink")，是梁州、荆州商裔，越领且兰国地。[春秋战国时期置且兰县](../Page/春秋战国.md "wikilink")，[隋](../Page/隋.md "wikilink")[唐五代置](../Page/唐.md "wikilink")[宾化县](../Page/宾化县.md "wikilink")，[宋代为黎峨里寨](../Page/宋代.md "wikilink")，[明先后为平越安抚司](../Page/明.md "wikilink")、军民指挥司、贵州新镇道、平越军民府，[清置平越直隶州](../Page/清.md "wikilink")。1913年改为平越县，1953年改为福泉县，1996年12月2日经[国务院批准撤县设市](../Page/国务院.md "wikilink")。

## 行政区划

下辖2个[街道办事处](../Page/街道办事处.md "wikilink")、5个[镇](../Page/镇.md "wikilink")、1个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 交通

### 公路

[210国道](../Page/210国道.md "wikilink") 205省道

### 铁路

交通有[China_Railways.svg](https://zh.wikipedia.org/wiki/File:China_Railways.svg "fig:China_Railways.svg")[湘黔铁路通過](../Page/湘黔铁路.md "wikilink")

## 外部連結

  - [貴州省福泉市人民政府網](http://www.gzfuquan.gov.cn)

## 参考文献

[福泉市](../Category/福泉市.md "wikilink")
[市](../Category/黔南州县市.md "wikilink")
[黔南州](../Category/贵州县级市.md "wikilink")