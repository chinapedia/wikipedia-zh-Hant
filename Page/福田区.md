[Civic_Center,_Shenzhen_Lianhuashan_Park.JPG](https://zh.wikipedia.org/wiki/File:Civic_Center,_Shenzhen_Lianhuashan_Park.JPG "fig:Civic_Center,_Shenzhen_Lianhuashan_Park.JPG")
[SZ_Hua_Qiang_1_華強電子世界.JPG](https://zh.wikipedia.org/wiki/File:SZ_Hua_Qiang_1_華強電子世界.JPG "fig:SZ_Hua_Qiang_1_華強電子世界.JPG")\]\]
**福田区**是[中国](../Page/中华人民共和国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[深圳市下辖的一个](../Page/深圳市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。位于[深圳经济特区中部](../Page/深圳.md "wikilink")，是[经济特区内的四区之一](../Page/经济特区.md "wikilink")。福田区是[深圳市委](../Page/深圳市委.md "wikilink")、[市政府所在地](../Page/深圳市政府.md "wikilink")，是深圳市重点开发和建设的中心城区，将建设成为深圳市的[行政](../Page/行政.md "wikilink")、[文化](../Page/文化.md "wikilink")、[信息](../Page/信息.md "wikilink")、国际展览和商务中心。

## 地理

福田区东部从红岭路起与[罗湖区相连](../Page/罗湖区.md "wikilink")，西部至[华侨城与](../Page/华侨城.md "wikilink")[南山区相接](../Page/南山区_\(深圳市\).md "wikilink")，北到[鸡公山与](../Page/鸡公山_\(深圳\).md "wikilink")[龙华区相连](../Page/龙华区.md "wikilink")，南临[深圳河](../Page/深圳河.md "wikilink")、[深圳湾与](../Page/深圳湾.md "wikilink")[香港](../Page/香港.md "wikilink")[新界的](../Page/新界.md "wikilink")[米埔](../Page/米埔.md "wikilink")、[元朗相望](../Page/元朗.md "wikilink")。辖区面积78.04[平方公里](../Page/平方公里.md "wikilink")，下辖10个[街道办事处](../Page/街道办事处.md "wikilink")。截至2002年全区总[人口达](../Page/人口.md "wikilink")88.58万人，其中户籍[人口](../Page/人口.md "wikilink")35.6万人，暂住人口52.98万人。

## 历史

“福田”一名的由来，有两种说法，一种说法是源于[宋代所题](../Page/宋代.md "wikilink")“湖山拥福，田地生辉”一词；另一种说法是源于[南宋光宗皇帝](../Page/南宋.md "wikilink")[赵淳](../Page/赵淳.md "wikilink")[绍熙三年](../Page/绍熙.md "wikilink")（1192年），史书记载，上沙村始祖[黄金堂的四子](../Page/黄金堂.md "wikilink")[黄西为到松子岭南麓建村](../Page/黄西.md "wikilink")，开荒造田，块块成格，故名为“幅田”，后人谐音为福田，意为“德福于田”。福田区历史悠久，福田地域历来为宝安（新安）县地。[明朝时属新安县城归城乡去都](../Page/明朝.md "wikilink")，清时属新安县官富司辖地。民国时期到解放初期，福田地域属[宝安县第二区沙头乡](../Page/宝安县.md "wikilink")。1990年10月，成立福田區，為深圳市的市轄區。\[1\]

现今福田区主要部分为原居民上埗村范围（北至笔架山，南至深圳河，西至华富路，东至红岭南路），因此现今福田区原名[上步区](../Page/上步区.md "wikilink")，但因为普通话上埗区谐音“上不去”与当时特区建设政治气氛不相称，因此改为意头好的名字。

### 农村城市化

梅富\[2\]、布尾、上沙、下沙、沙尾、沙咀、巴登、沙埔头、新洲等13个社区在1990-2010年代仍有鲜明的农村城市化印记，表现为社区内原住民从社区所在股份公司获得年度分红收益，以及区域内密集而且高矮不一的[农民楼](../Page/农民楼.md "wikilink")，在大众文化中称谓以上社区为[城中村](../Page/城中村.md "wikilink")。虽然政府主导或地产参与了这些区域的旧城改造，将原住民建筑逐步更换为写字楼、商业中心、厂房，但以上农村城市化现象不会很快变更为现代化城镇。

## 行政區劃

下轄10個街道办事处，分別为：[园岭街道](../Page/园岭街道.md "wikilink")、[南园街道](../Page/南园街道.md "wikilink")、[福田街道](../Page/福田街道.md "wikilink")、[沙头街道](../Page/沙头街道_\(深圳市\).md "wikilink")、[梅林街道](../Page/梅林街道_\(深圳市\).md "wikilink")、[华富街道](../Page/华富街道.md "wikilink")、[香蜜湖街道](../Page/香蜜湖街道.md "wikilink")、[莲花街道](../Page/莲花街道_\(深圳市\).md "wikilink")、[华强北街道](../Page/华强北街道.md "wikilink")、[福保街道](../Page/福保街道.md "wikilink")\[3\]。其中，[华强北街道](../Page/华强北街道.md "wikilink")、[福保街道为](../Page/福保街道.md "wikilink")2009年新置的街道办事处\[4\]。

## 功能组团

### 住宅区

福田区黄木岗安置区\[5\]和莲花山安置区\[6\]是深圳历史上出现过的公务员、企事业单位过渡性住房解决方案，后因城市规划、住房改革等因素而消失。后由福田区其他地点的莲花北村、益田村、彩田村、侨香村等纯住宅社区替代，这些是深圳市区政府公务员或事业单位成员的福利性住房。福田区的大型屋苑有[福民新村](../Page/福民新村.md "wikilink")、[天澤花園](../Page/天澤花園.md "wikilink")、[皇御苑](../Page/皇御苑.md "wikilink")、[帝濤豪園等](../Page/帝濤豪園.md "wikilink")。

### 商业区

福田区的商业区主要有[华强北](../Page/华强北.md "wikilink")、會展中心、車公廟等；而深圳第一高樓[平安國際金融中心](../Page/平安國際金融中心.md "wikilink")，以及深圳中心、深業上城等超高層摩天大廈，均建於此區。

### 旅游区

坐落在福田区的商业旅游区有[香蜜湖水上世界](../Page/香蜜湖水上世界.md "wikilink")（已結業）和[赛格观光](../Page/赛格观光.md "wikilink")（[赛格广场](../Page/赛格广场.md "wikilink")）等，市政旅游景点有[红树林自然保护区](../Page/红树林自然保护区.md "wikilink")、[深圳市中心公园](../Page/深圳市中心公园.md "wikilink")、[莲花山公园](../Page/莲花山_\(深圳\).md "wikilink")、[荔枝公园](../Page/荔枝公园.md "wikilink")、[笔架山公园](../Page/笔架山公园.md "wikilink")、[梅林公园](../Page/梅林公园.md "wikilink")、[深圳园博园等](../Page/深圳园博园.md "wikilink")。

## 公共、文化设施

[COCO_ParkOutside.jpg](https://zh.wikipedia.org/wiki/File:COCO_ParkOutside.jpg "fig:COCO_ParkOutside.jpg")\]\]
[Shenzhen_Library_Overview.jpg](https://zh.wikipedia.org/wiki/File:Shenzhen_Library_Overview.jpg "fig:Shenzhen_Library_Overview.jpg")\]\]

### 电影院、戏院

福田区的影院有金盾影院、[COCO
Park深圳百老汇影城](../Page/COCO_Park.md "wikilink")、深圳金逸影城（[中心城店](../Page/怡景中心城.md "wikilink")）、深圳中影金典影城、深圳保利國際影城大中華店、深圳博納時代國際影城（[皇庭店](../Page/皇庭廣場.md "wikilink")）、深圳新南國影城（[中信店](../Page/新城市廣場_\(深圳\).md "wikilink")）、深圳東海太平洋電影城、嘉之華中心影城等。

### 图书馆

福田区的图书馆有[深圳图书馆](../Page/深圳图书馆.md "wikilink")、[福田区图书馆等](../Page/福田区图书馆.md "wikilink")，深圳市图书馆原处园岭街道园岭社区内，后于2007年7月份搬迁至中心区。

### 音樂廳

位於[深圳圖書館對面的](../Page/深圳圖書館.md "wikilink")[深圳音樂廳](../Page/深圳音樂廳.md "wikilink")，由著名建築師[黑川紀章負責設計](../Page/黑川紀章.md "wikilink")。

### 書城

[深圳書城中心城位於](../Page/深圳書城中心城.md "wikilink")[深圳地鐵](../Page/深圳地鐵.md "wikilink")[少年宮站西側](../Page/少年宮站.md "wikilink")，是深圳最大的書店。
另外，[科學館站曾設有](../Page/科學館站.md "wikilink")[深圳購書中心](../Page/深圳購書中心.md "wikilink")，惟已於2013年11月結業。

### 公园

福田区的公园有[中心公园](../Page/深圳市中心公园.md "wikilink")、[荔枝公园](../Page/荔枝公园.md "wikilink")、[莲花山公园](../Page/莲花山公园.md "wikilink")、[笔架山公园](../Page/笔架山公园.md "wikilink")、[梅林公园](../Page/梅林公园.md "wikilink")、[皇岗公园](../Page/皇岗公园.md "wikilink")、[红树林公园](../Page/红树林公园.md "wikilink")、[园博园等](../Page/园博园.md "wikilink")。其中位於本區的[深圳市中心公园位于中心区东面](../Page/深圳市中心公园.md "wikilink")。[莲花山公园位于中心区北面](../Page/莲花山公园.md "wikilink")，由主峰、小灯旗岭等数个小山峰组成。主峰上有[邓小平全身铜像](../Page/邓小平.md "wikilink")，可俯瞰中心区全貌。公园东南角为[关山月美术馆](../Page/关山月美术馆.md "wikilink")。[梅林公园位于](../Page/梅林公园.md "wikilink")[梅林水库东南向](../Page/梅林水库.md "wikilink")，地处[梅林街道龙尾社区](../Page/梅林街道_\(深圳市\).md "wikilink")。[红树林公园位于福田区与南山区交界](../Page/红树林公园.md "wikilink")、深圳南线，与香港相对。

[Shenzhen-Zhuzilin.jpg](https://zh.wikipedia.org/wiki/File:Shenzhen-Zhuzilin.jpg "fig:Shenzhen-Zhuzilin.jpg")
[Shenzhen_CBD.jpg](https://zh.wikipedia.org/wiki/File:Shenzhen_CBD.jpg "fig:Shenzhen_CBD.jpg")

### 教育

福田区有48间区属公立小学，20间公立中学，1间公立职业学校\[7\]，分布在区内8个街道范围。

截至2014年7月，福田区共有社会办小学、中学16间\[8\]，包括：

  - [深圳市福景外国语学校](../Page/深圳市福景外国语学校.md "wikilink")，招收7年级至12年级
  - [深圳市沪教院福田实验学校](../Page/深圳市沪教院福田实验学校.md "wikilink")，招收10年级至12年级
  - [深圳市皇御苑学校](../Page/深圳市皇御苑学校.md "wikilink")，招收1年级至12年级
  - [深圳市耀华实验学校](../Page/深圳市耀华实验学校.md "wikilink")，招收1年级至12年级
  - [深圳市云顶学校招收](../Page/深圳市云顶学校.md "wikilink")1年级至12年级
  - [深圳外国语学校东海附属小学](../Page/深圳外国语学校东海附属小学.md "wikilink")
  - [深圳市福田区城市绿洲学校](../Page/深圳市福田区城市绿洲学校.md "wikilink")，招收1年级至9年级
  - [深圳市福田区益田花园学校](../Page/深圳市福田区益田花园学校.md "wikilink")，招收1年级至9年级
  - [福田区南开学校](../Page/福田区南开学校.md "wikilink")，招收1年级至9年级
  - [深圳市福田区上陆学校](../Page/深圳市福田区上陆学校.md "wikilink")，招收1年级至9年级
  - [深圳市福田区中南学校](../Page/深圳市福田区中南学校.md "wikilink")，招收1年级至9年级
  - [深圳市福田区欣欣学校](../Page/深圳市福田区欣欣学校.md "wikilink")，招收1年级至9年级
  - [深圳市福田区方方乐趣中英文学校](../Page/深圳市福田区方方乐趣中英文学校.md "wikilink")，招收1年级至9年级
  - [深圳市福田区丽中小学](../Page/深圳市福田区丽中小学.md "wikilink")
  - [深圳市福田区保税区外国语小学](../Page/深圳市福田区保税区外国语小学.md "wikilink")
  - [深圳艺校福田泰然小学](../Page/深圳艺校福田泰然小学.md "wikilink")

### 幼儿园

### 深圳福田区的主要[小学](../Page/小学.md "wikilink")

福田区小学共57所，招生1-6年级的适龄对象。由于小学属于义务教育第一阶段，招生按照招生对象的户籍或居住地段就近原则进行。

  - 荔园南校区
  - 园岭小学
  - 福田小学
  - 新洲小学
  - 园岭实验
  - 园岭外语
  - 上步小学
  - 南园小学
  - 南华小学
  - [福南小学](../Page/福南小学_\(深圳\).md "wikilink")
  - 岗厦小学
  - 皇岗小学
  - 水围小学
  - 众孚小学
  - 新沙小学
  - 下沙小学
  - 狮岭小学
  - 梅林小学
  - 竹园小学
  - 竹林小学
  - 百花小学
  - 华富小学
  - 莲花小学
  - 景鹏小学
  - 福华小学
  - 福民小学
  - 华新小学
  - 美莲小学
  - 新莲小学
  - 景龙小学
  - 景莲小学
  - 益田小学
  - 梅丽小学
  - 梅园小学
  - 梅山小学
  - 景秀小学
  - 上沙小学
  - 绿洲小学
  - 荔园北校区
  - 石厦小学部
  - 福强小学
  - 全海小学
  - 彩田小学部
  - 黄埔小学部
  - 梅华小学
  - 景田小学
  - 荔外西校区
  - 天健小学
  - 荔轩小学
  - 福外小学部
  - 益强小学
  - 荔外东校区
  - 碧海小学
  - 翰林小学部
  - 福新小学
  - 明德实验小学部
  - 实验小学部

福田区主要的高级中学有深圳市[高级中学](../Page/高级中学.md "wikilink")、[实验中学](../Page/实验中学.md "wikilink")、[福田中学等](../Page/福田中学.md "wikilink")。高级和初级中学混编的学校有[深圳外国语学校](../Page/深圳外国语学校.md "wikilink")、[红岭中学](../Page/红岭中学.md "wikilink")、[新沙中学](../Page/新沙中学.md "wikilink")、[皇岗中学等](../Page/皇岗中学.md "wikilink")。初级中学和小学混编的学校：石厦学校。幼儿园有福田区机关第一幼儿园等。

### 博物館

  - [深圳科學館](../Page/深圳科學館.md "wikilink")
  - [深圳博物館](../Page/深圳博物館.md "wikilink")
  - [深圳少年宮](../Page/深圳市少年宮.md "wikilink")
  - [深圳市工業展覽館](../Page/深圳市工業展覽館.md "wikilink")
  - [關山月美術館](../Page/關山月美術館.md "wikilink")
  - [深圳市當代藝術館及城市規劃展覽館](../Page/深圳市當代藝術館及城市規劃展覽館.md "wikilink")

### 医疗和卫生保健

#### 现状

区属公立医院有福田人民医院、福田区第二人民医院、福田中医院，其中福田区人民医院另行地址管辖香蜜湖分院、南园分院。民营医院有景田医院、和平医院、友谊医院、福华中西医结合医院、[深圳希玛林顺潮眼科医院](../Page/深圳希玛林顺潮眼科医院.md "wikilink")，等等。疾病控制机构有福田区疾病预防控制中心、福田区慢性病防治院。保健机构有福田区妇幼保健院。

区内市级医疗机构和卫生专业机构有[北京大学深圳医院](../Page/北京大学深圳医院.md "wikilink")、[香港大学深圳医院](../Page/香港大学深圳医院.md "wikilink")、[深圳市第二人民医院](../Page/深圳市第二人民医院.md "wikilink")、[深圳市妇儿医院](../Page/深圳市妇儿医院.md "wikilink")、[深圳市儿童医院](../Page/深圳市儿童医院.md "wikilink")、[深圳市中医院](../Page/深圳市中医院.md "wikilink")、[深圳市疾病预防控制中心](../Page/深圳市疾病预防控制中心.md "wikilink")。

2013年末全区共有医疗卫生机构422个。其中医院23个，门诊部（所）317个，疾病预防控制中心1个，卫生监督所1个，[社区健康服务中心](../Page/社区健康服务中心.md "wikilink")75个。医院拥有病床6513张。卫生技术人员14863人，其中执业医师5462人，注册护士6721人。\[9\]

#### 规划

2014年4月，卫生部门有意将[深圳市急救中心](../Page/深圳市急救中心.md "wikilink")、深圳市血液中心、深圳市医学信息中心迁往福田区安托山地块\[10\]。同期媒体报道，该地块将新增福田区妇儿专科医院\[11\]。

### 交通

福田区的主要干道有[广深高速公路](../Page/广深高速公路.md "wikilink")、[梅观高速公路](../Page/梅观高速公路.md "wikilink")、[深南大道](../Page/深南大道.md "wikilink")、[滨河大道](../Page/滨河大道.md "wikilink")、[滨海大道](../Page/滨海大道.md "wikilink")、[皇岗路](../Page/皇岗路.md "wikilink")、[北环大道](../Page/北环大道.md "wikilink")、[香蜜湖路](../Page/香蜜湖路.md "wikilink")、[福龍路](../Page/福龍路.md "wikilink")。

#### 铁路

福田区是目前深圳拥有最多地铁车站的市辖区，亦是最多地鐵路綫途經的市轄區。途經福田区的有[深圳地铁](../Page/深圳地铁.md "wikilink")[1號綫](../Page/深圳地铁1號綫.md "wikilink")、[2號綫](../Page/深圳地铁2號綫.md "wikilink")、[3號綫](../Page/深圳地铁3號綫.md "wikilink")、[4號綫](../Page/深圳地铁4號綫.md "wikilink")、[7號綫](../Page/深圳地铁7號綫.md "wikilink")、[9號綫和](../Page/深圳地铁9號綫.md "wikilink")[11號綫](../Page/深圳地铁11號綫.md "wikilink")，興建中的[10號綫和規劃中的](../Page/深圳地铁10號綫.md "wikilink")[6號綫南延段亦會在福田區設站](../Page/深圳地铁6號綫.md "wikilink")。

[1號綫的车站有](../Page/深圳地铁1號綫.md "wikilink")[科学馆站](../Page/科学馆站.md "wikilink")、[华强路站](../Page/华强路站.md "wikilink")、[岗厦站](../Page/岗厦站.md "wikilink")、[会展中心站](../Page/会展中心站.md "wikilink")、[购物公园站](../Page/购物公园站.md "wikilink")、[香蜜湖站](../Page/香蜜湖站.md "wikilink")、[車公廟站](../Page/車公廟站_\(深圳\).md "wikilink")、[竹子林站](../Page/竹子林站.md "wikilink")、[僑城東站](../Page/僑城東站.md "wikilink")。

[4號綫的车站有](../Page/深圳地铁4號綫.md "wikilink")[福田口岸站](../Page/福田口岸站.md "wikilink")、[福民站](../Page/福民站.md "wikilink")、[会展中心站](../Page/会展中心站.md "wikilink")、[市民中心站](../Page/市民中心站.md "wikilink")、[少年宫站](../Page/少年宫站.md "wikilink")、[莲花北站](../Page/莲花北站.md "wikilink")、[上梅林站](../Page/上梅林站.md "wikilink")。

[2號綫的车站有](../Page/深圳地铁2號綫.md "wikilink")[深康站](../Page/深康站.md "wikilink")、[安托山站](../Page/安托山站.md "wikilink")、[侨香站](../Page/侨香站.md "wikilink")、[香蜜站](../Page/香蜜站.md "wikilink")、[香梅北站](../Page/香梅北站.md "wikilink")、[景田站](../Page/景田站.md "wikilink")、[莲花西站](../Page/莲花西站.md "wikilink")、[福田站](../Page/福田站.md "wikilink")、[市民中心站](../Page/市民中心站.md "wikilink")、[岗厦北站](../Page/岗厦北站.md "wikilink")、[华强北站](../Page/华强北站.md "wikilink")、[燕南站](../Page/燕南站.md "wikilink")。

[3號綫的车站有](../Page/深圳地铁3號綫.md "wikilink")[益田站](../Page/益田站_\(深圳\).md "wikilink")、[石厦站](../Page/石厦站.md "wikilink")、[购物公园站](../Page/购物公园站.md "wikilink")、[福田站](../Page/福田站.md "wikilink")、[少年宫站](../Page/少年宫站.md "wikilink")、[莲花村站](../Page/莲花村站.md "wikilink")、[华新站](../Page/华新站.md "wikilink")、[通新岭站](../Page/通新岭站.md "wikilink")、[红岭站](../Page/红岭站.md "wikilink")。

[7號綫的車站有](../Page/深圳地铁7號綫.md "wikilink")
[安托山站](../Page/安托山站.md "wikilink")、[農林站](../Page/農林站.md "wikilink")、[車公廟站](../Page/車公廟站_\(深圳\).md "wikilink")、[上沙站](../Page/上沙站.md "wikilink")、[沙尾站](../Page/沙尾站.md "wikilink")、[石廈站](../Page/石廈站.md "wikilink")、[皇崗村站](../Page/皇崗村站.md "wikilink")、[福民站](../Page/福民站.md "wikilink")、[皇崗口岸站](../Page/皇崗口岸站.md "wikilink")、[赤尾站](../Page/赤尾站.md "wikilink")、[華強南站](../Page/華強南站.md "wikilink")、[華強北站](../Page/華強北站.md "wikilink")、[華新站](../Page/華新站.md "wikilink")、[黃木崗站](../Page/黃木崗站.md "wikilink")、[八卦嶺站](../Page/八卦嶺站.md "wikilink")。

[9號綫的車站有](../Page/深圳地铁9號綫.md "wikilink")
[下沙站](../Page/下沙站.md "wikilink") 、
[車公廟站](../Page/車公廟站_\(深圳\).md "wikilink")、
[香梅站](../Page/香梅站.md "wikilink")、[景田站](../Page/景田站.md "wikilink")、[梅景站](../Page/梅景站.md "wikilink")、[下梅林站](../Page/下梅林站.md "wikilink")、[梅村站](../Page/梅村站.md "wikilink")、[上梅林站](../Page/上梅林站.md "wikilink")、[孖嶺站](../Page/孖嶺站.md "wikilink")、[銀湖站](../Page/銀湖站.md "wikilink")、[泥崗站](../Page/泥崗站.md "wikilink")、[紅嶺北站](../Page/紅嶺北站.md "wikilink")、[園嶺站](../Page/園嶺站.md "wikilink")、[紅嶺站](../Page/紅嶺站.md "wikilink")。

[11號綫的车站有](../Page/深圳地铁11號綫.md "wikilink")
[福田站](../Page/福田站.md "wikilink")、[車公廟站](../Page/車公廟站_\(深圳\).md "wikilink")。

另外，[京港客運專線及](../Page/京港客運專線.md "wikilink")[廣深港高速鐵路也在福田區設有](../Page/廣深港高速鐵路.md "wikilink")[福田站](../Page/福田站.md "wikilink")。

### 桥樑

## 参考文献

## 外部链接

  - [福田区政府网站](https://web.archive.org/web/20070701225458/http://szft.gov.cn/)
  - [福田区学区图](http://map.28dat.net/ftxx.aspx)

## 参见

  - [深圳市](../Page/深圳市.md "wikilink")

{{-}}

[Category:中国国家级生态市区县](../Category/中国国家级生态市区县.md "wikilink")
[福田区](../Category/福田区.md "wikilink")
[Category:深圳市辖区](../Category/深圳市辖区.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.
10.

11.