[Rua_da_Felicidade_01.jpg](https://zh.wikipedia.org/wiki/File:Rua_da_Felicidade_01.jpg "fig:Rua_da_Felicidade_01.jpg")
[Macau_Rua_da_Felicidade_Mo707_1a.jpg](https://zh.wikipedia.org/wiki/File:Macau_Rua_da_Felicidade_Mo707_1a.jpg "fig:Macau_Rua_da_Felicidade_Mo707_1a.jpg")
[Macau_Rua_da_Felicidade_Mo707_2a.jpg](https://zh.wikipedia.org/wiki/File:Macau_Rua_da_Felicidade_Mo707_2a.jpg "fig:Macau_Rua_da_Felicidade_Mo707_2a.jpg")
**福隆新街**，(**Rua da
Felicidade**)，是[澳門](../Page/澳門.md "wikilink")[半島](../Page/半島.md "wikilink")[中區的一條古老的](../Page/中區_\(澳門\).md "wikilink")[街道](../Page/街道.md "wikilink")，歷史約有幾百年。福隆新街曾經是[商業繁盛的地區](../Page/商業.md "wikilink")，包括鄰近的福榮里、福寧里、蓬萊新巷、清和里、[白眼塘等](../Page/白眼塘.md "wikilink")。
在1932年，[香港禁娼之後](../Page/香港.md "wikilink")，[澳門](../Page/澳門.md "wikilink")[娼妓蓬勃](../Page/娼妓.md "wikilink")，直至1940年代，澳門才先後禁娼及毒。當年，以上商業活動在福隆新街也很著名。\[1\]

## 近況

福隆新街現時已經轉營為[中西文化村老店街](../Page/中西文化村.md "wikilink")，也稱手信街。
名店有冠環球、[鉅記餅家](../Page/鉅記餅家.md "wikilink")、[佛笑樓等](../Page/佛笑樓.md "wikilink")。

## 相關

  - [文華酒店](../Page/文華酒店.md "wikilink"):
    是[澳門二星級酒店](../Page/澳門二星級酒店.md "wikilink")
  - [澳門同善堂](../Page/澳門同善堂.md "wikilink")
  - [瑞貞攤館](../Page/澳門賭場列表.md "wikilink"):
    [澳門博彩業專營時代](../Page/澳門博彩業.md "wikilink")
  - [澳門文物名錄](../Page/澳門文物名錄.md "wikilink"): 已評定之建築群
  - 新華大旅館：受自助旅行人士歡迎，它是[電影](../Page/電影.md "wikilink")[伊莎貝拉的外景場地之一](../Page/伊莎貝拉_\(電影\).md "wikilink")。

## 附近街道

  - [聖奧斯定前地](../Page/聖奧斯定前地.md "wikilink")
  - [紅窗門街](../Page/紅窗門街.md "wikilink")
  - [新馬路](../Page/新馬路.md "wikilink")
  - [清平直街](../Page/清平直街.md "wikilink")
  - [庇山耶街](../Page/庇山耶街.md "wikilink")
  - [白眼塘橫街](../Page/白眼塘橫街.md "wikilink")
  - [司打口](../Page/司打口.md "wikilink")
  - [十月初五街](../Page/十月初五街.md "wikilink")

## 電視劇取影

2013及2014年[香港](../Page/香港.md "wikilink")[無綫電視在澳門拍攝](../Page/TVB.md "wikilink")[巨輪I](../Page/巨輪.md "wikilink")
、[巨輪II](../Page/巨輪II.md "wikilink").
[田蕊妮和](../Page/田蕊妮.md "wikilink")[蕭正楠擔正男女主角](../Page/蕭正楠.md "wikilink").

## 参考文献

<div class="references-small">

<references />

</div>

[category:澳門街道](../Page/category:澳門街道.md "wikilink")

[Category:澳門旅遊景點](../Category/澳門旅遊景點.md "wikilink")

1.  [福隆新街青樓史](http://www.rthk.org.hk/elearning/travel/articles/11/f11_01_03_00_02.htm)