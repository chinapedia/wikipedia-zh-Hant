**离子积常数**是[化学平衡](../Page/化学平衡.md "wikilink")[常数的一种形式](../Page/平衡常数.md "wikilink")，多用于纯[液体和难溶](../Page/液体.md "wikilink")[电解质的](../Page/电解质.md "wikilink")[电离](../Page/电离.md "wikilink")。

形如这样的一个电离方程式：

\[R \to nM^{m+} + mN^{n-}\]

其中R为溶质，M和N分别为电离出来的[阳离子和](../Page/阳离子.md "wikilink")[阴离子](../Page/阴离子.md "wikilink")，其离子积可表示为：

\[K = [M^{m+}]^n \cdot [N^{n-}]^m\]

与一般的平衡常数表达式相比，离子积常数的表达式少了关于反应物的项。这就限制了离子积常数只适用于反应物是纯液体或纯固体的反应，因为在计算平衡常数时，纯液体和纯固体的浓度视作1。

## 纯液体的离子积

纯液体的离子积一般用于[溶剂的自耦电离](../Page/溶剂.md "wikilink")，如[水](../Page/水.md "wikilink")。水是一种极弱的电解质，它能微弱地电离：

  -
    H<sub>2</sub>O + H<sub>2</sub>O ↔ H<sub>3</sub>O<sup>+</sup> +
    OH<sup>−</sup>

通常H<sub>3</sub>O<sup>+</sup>简写为H<sup>+</sup>。

水的离子积**K<sub>w</sub>**=\[H<sup>+</sup>\]·\[OH<sup>−</sup>\]，25度时，K<sub>w</sub>=1×10<sup>−14</sup>。温度升高时，水的电离程度加大，K值也随着上升。

液[氨](../Page/氨.md "wikilink")、液态[二氧化硫等溶剂也可写出离子积表达式](../Page/二氧化硫.md "wikilink")。

## 难溶电解质的离子积

难溶电解质的离子积常数能很好地反映电解质的溶解程度，因此这一种常数又叫做**[溶度积常数](../Page/溶度积.md "wikilink")**，符号为**K**。

如[氯化银的微弱电离](../Page/氯化银.md "wikilink")：

\[AgCl(s) \to Ag^+(aq) + Cl^-(aq) , K_{sp} = 1.77 \times 10^{-10}\]

即在25℃的AgCl的[饱和溶液中](../Page/饱和溶液.md "wikilink")，银离子浓度与氯离子浓度的乘积为1.77<small>E</small>-10。根据这一点，可以计算出AgCl的[溶解度](../Page/溶解度.md "wikilink")。

在1“分子”[溶质电离出](../Page/溶质.md "wikilink")[离子数相同的情况下](../Page/离子.md "wikilink")，K越小的，[溶解度也越小](../Page/溶解度.md "wikilink")。

在不同[温度下](../Page/温度.md "wikilink")，同一物质的K会有不同的数值。

## 参看

  - [化学平衡](../Page/化学平衡.md "wikilink")
  - [水的电离](../Page/水的电离.md "wikilink")
  - [电解质](../Page/电解质.md "wikilink")
  - [电解质溶液](../Page/电解质溶液.md "wikilink")

## 参考书目

1.  AgCl的溶度积数据出处：北京师范大学无机化学教研室 等，《无机化学 第四版》，高等教育出版社，p419 ISBN 7040107686

[Category:化学动力学](../Category/化学动力学.md "wikilink")