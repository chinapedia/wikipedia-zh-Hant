**离水花科**又名[虎刺叶科或](../Page/虎刺叶科.md "wikilink")[拉美冬青科](../Page/拉美冬青科.md "wikilink")，只有1[属](../Page/属.md "wikilink")2[种](../Page/种.md "wikilink")—*[Desfontainia
spinosa](../Page/Desfontainia_spinosa.md "wikilink")* Ruiz &
Pav.和*[Desfontainia
splendens](../Page/Desfontainia_splendens.md "wikilink")*
Bonpl.，分布在从[哥斯达黎加至全部](../Page/哥斯达黎加.md "wikilink")[安第斯山区直到](../Page/安第斯山.md "wikilink")[南美洲最南端](../Page/南美洲.md "wikilink")。

本科[植物为常绿](../Page/植物.md "wikilink")[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")；单[叶对生](../Page/叶.md "wikilink")，叶有刺；[花长](../Page/花.md "wikilink")，[花冠成筒状](../Page/花冠.md "wikilink")；[果实为](../Page/果实.md "wikilink")[浆果](../Page/浆果.md "wikilink")，[白色或](../Page/白色.md "wikilink")[黄色](../Page/黄色.md "wikilink")。

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[马钱科](../Page/马钱科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该单独分出一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，但无法放到任何
一个[目中](../Page/目.md "wikilink")，直接放在[II类真菊分支之下](../Page/II类真菊分支.md "wikilink")，可选择性地与[弯药树科合并](../Page/弯药树科.md "wikilink")，2003年经过修订的[APG
II 分类法维持原分类](../Page/APG_II_分类法.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[离水花科](http://delta-intkey.com/angio/www/desfonta.htm)
  - [APG网站中的离水花科](http://www.mobot.org/MOBOT/Research/APWeb/orders/Dipsacalesweb.htm#Desfontainiaceae)
  - [NCBI中的离水花科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=26474)

[\*](../Category/离水花科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")