**禽龍科**（Iguanodontidae）是[鳥腳下目的一科](../Page/鳥腳下目.md "wikilink")，生存於[白堊紀早期到中期](../Page/白堊紀.md "wikilink")，分布於今日的[亞洲](../Page/亞洲.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、以及[北美洲等地](../Page/北美洲.md "wikilink")。最著名的屬是[禽龍](../Page/禽龍.md "wikilink")，是最早被命名的恐龍之一。

## 敘述

禽龍科具有過渡的外形，以及許多衍化的特徵。牠們的脊椎上有骨化肌腱，[前上頜骨缺乏牙齒](../Page/前上頜骨.md "wikilink")，頜部具有[齒系](../Page/齒系.md "wikilink")，固定的[胸骨](../Page/胸骨.md "wikilink")，胸骨的形狀為[腎臟狀到](../Page/腎臟.md "wikilink")[斧狀](../Page/斧.md "wikilink")。禽龍科可以用二足或四足方式移動。某些屬的[拇指具有尖刺](../Page/拇指.md "wikilink")，可能作為武器，而中間三指可用於行走，小指可用來操作物體。

## 分類學

禽龍科被定義為：包含[貝尼薩爾禽龍](../Page/禽龍.md "wikilink")，但不包含[沃克氏副櫛龍在內的最大](../Page/副櫛龍.md "wikilink")[演化支](../Page/演化支.md "wikilink")。

## 参见

  - [禽龙类](../Page/禽龙类.md "wikilink")

## 參考文獻

  - Gregory S. Paul (2008): „A revised taxonomy of the iguanodont
    dinosaur genera and species“ In: *Cretaceous Research* Volume 29,
    Issue 2, April 2008, Pages 192-216.

## 外部連結

  - [禽龍科的範圍 Taxon
    Search](https://web.archive.org/web/20080405235743/http://www.taxonsearch.org/dev/taxon_edit.php?Action=View&tax_id=179)
  - [禽龍科的特徵
    Palæos](https://web.archive.org/web/20080511170435/http://www.palaeos.com/Vertebrates/Units/320Ornithischia/600.html#Iguanodontidae)
  - [禽龍類 The
    Thescelsosaurus](https://web.archive.org/web/20070307004240/http://www.users.qwest.net/~jstweet1/iguanodontia.htm)

[\*](../Category/禽龍類.md "wikilink")