**禾本目**是[单子叶植物的一个目](../Page/单子叶植物.md "wikilink")。2003年根据[植物亲缘关系分类的](../Page/植物.md "wikilink")[APG
II
分类法将原来分在](../Page/APG_II_分类法.md "wikilink")[莎草目](../Page/莎草目.md "wikilink")、[帚灯草目](../Page/帚灯草目.md "wikilink")、[谷精草目和](../Page/谷精草目.md "wikilink")[鸭跖草目中的部分](../Page/鸭跖草目.md "wikilink")[科合并为一个禾本目](../Page/科.md "wikilink")。

禾本目植物分布相当广，几乎遍布全世界，凡是能生长种子植物的地方都有本目植物生长，而且是人类[粮食和饲养](../Page/粮食.md "wikilink")[动物饲料的主要来源](../Page/动物.md "wikilink")，与人类的关系极为密切。

## 分类

禾本目是[鸭跖草类植物的演化支之一](../Page/鸭跖草类植物.md "wikilink")，与

### 内部分类

依2009年[APG III分类法](../Page/APG_III分类法.md "wikilink")，本目共有十六科\[1\]：

  - **禾本目** Poales [Small](../Page/Small.md "wikilink") (1903)
      - [苞穗草科](../Page/苞穗草科.md "wikilink") Anarthriaceae
        [D.F.Cutler](../Page/D.F.Cutler.md "wikilink") & [Airy
        Shaw](../Page/Airy_Shaw.md "wikilink") (1965)
      - [凤梨科](../Page/凤梨科.md "wikilink") Bromeliaceae
        [Juss.](../Page/Juss..md "wikilink") (1789), nom. cons.
      - [刺鳞草科](../Page/刺鳞草科.md "wikilink") Centrolepidaceae
        [Endl.](../Page/Endl..md "wikilink") (1836), nom. cons.
      - [莎草科](../Page/莎草科.md "wikilink") Cyperaceae
        [Juss.](../Page/Juss..md "wikilink") (1789), nom. cons.
      - [二柱草科](../Page/二柱草科.md "wikilink") Ecdeiocoleaceae
        [D.F.Cutler](../Page/D.F.Cutler.md "wikilink") & [Airy
        Shaw](../Page/Airy_Shaw.md "wikilink") (1965)
      - [谷精草科](../Page/谷精草科.md "wikilink") Eriocaulaceae
        [Martinov](../Page/Martinov.md "wikilink") (1820), nom. cons.
      - [须叶藤科](../Page/须叶藤科.md "wikilink") Flagellariaceae
        [Dumort.](../Page/Dumort..md "wikilink") (1829), nom. cons.
      - [拟苇科](../Page/拟苇科.md "wikilink") Joinvilleaceae
        [Toml.](../Page/Toml..md "wikilink") &
        [A.C.Sm.](../Page/A.C.Sm..md "wikilink") (1970)
      - [灯心草科](../Page/灯心草科.md "wikilink") Juncaceae
        [Juss.](../Page/Juss..md "wikilink") (1789), nom. cons.
      - [苔草科](../Page/苔草科.md "wikilink") Mayacaceae
        [Kunth](../Page/Kunth.md "wikilink") (1842), nom. cons.
      - [禾本科](../Page/禾本科.md "wikilink") Poaceae
        [Barnhart](../Page/Barnhart.md "wikilink") (1895), nom. cons.
      - [偏穗草科](../Page/偏穗草科.md "wikilink") Rapateaceae
        [Dumort.](../Page/Dumort..md "wikilink") (1829), nom. cons.
      - [帚灯草科](../Page/帚灯草科.md "wikilink") Restionaceae
        [R.Br.](../Page/R.Br..md "wikilink") (1810), nom. cons.
      - [梭子草科](../Page/梭子草科.md "wikilink") Thurniaceae
        [Engl.](../Page/Engl..md "wikilink") (1907), nom. cons.
      - [香蒲科](../Page/香蒲科.md "wikilink") Typhaceae
        [Juss.](../Page/Juss..md "wikilink") (1789), nom.
        cons.（包含[黑三棱科](../Page/黑三棱科.md "wikilink")
        Sparganiaceae [Hanin](../Page/Hanin.md "wikilink"), nom. cons.)
      - [黄眼草科](../Page/黄眼草科.md "wikilink") Xyridaceae
        [C.Agardh](../Page/C.Agardh.md "wikilink") (1823), nom. cons.

另外，过去曾归于本目的科有：

  - [独蕊草科](../Page/独蕊草科.md "wikilink") Hydatellaceae
    [U.Hamann](../Page/U.Hamann.md "wikilink") (1976)
    （又名排水草科，现归于[睡莲目](../Page/睡莲目.md "wikilink")\[2\]。）

### 内部支序分类表

## 参考文献

## 外部链接

[\*](../Category/禾本目.md "wikilink")

1.
2.