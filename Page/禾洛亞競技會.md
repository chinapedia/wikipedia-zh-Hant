**禾洛亞競技會**（**Horoya Athlétique
Club**）是一支位于[几内亚](../Page/几内亚.md "wikilink")[科纳克里的足球俱乐部](../Page/科纳克里.md "wikilink")。

## 球队荣誉

  - **[非洲优胜者杯](../Page/非洲优胜者杯.md "wikilink"):** 1次
      - Finalist : 1978

<!-- end list -->

  - **[几内亚国家锦标赛](../Page/几内亚国家锦标赛.md "wikilink"):** 9次
      - 1986, 1988, 1989, 1990, 1991, 1992, 1994, 2000, 2001

<!-- end list -->

  - **[几内亚国家杯](../Page/几内亚国家杯.md "wikilink"): 4次**
      - 1989, 1994, 1995, 1999

[Category:几内亚足球俱乐部](../Category/几内亚足球俱乐部.md "wikilink")