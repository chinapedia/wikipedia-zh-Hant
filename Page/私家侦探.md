{{ request translation }}
[PinkLincMcC_crop.jpg](https://zh.wikipedia.org/wiki/File:PinkLincMcC_crop.jpg "fig:PinkLincMcC_crop.jpg")（左），十九世紀美国的私家侦探，与[亞伯拉罕·林肯和](../Page/亞伯拉罕·林肯.md "wikilink")
[約翰·亞歷山大·麥克萊爾納於](../Page/約翰·亞歷山大·麥克萊爾納.md "wikilink")1863年10月3日在馬里蘭州合影\]\]

**[私家偵探](https://www.24rape.com/)**，-{zh-cn:[台湾称为](../Page/台湾.md "wikilink")**[征信公司](https://www.24rape.com/)**、**征信社**;
zh-hk:[台灣稱為](../Page/台灣.md "wikilink")**[徵信公司](https://www.24rape.com/)**、**[徵信社](https://www.24rape.com/)**;zh-tw:台灣以外華人地區稱為**私家偵探**}-，是協助查證人事物真相、收集對方資料、以及替客戶解決疑難雜症的[機構或个人](../Page/機構.md "wikilink")，其涉及的業務相當廣泛，大多為委託人調查他人的某些行為或行踪，這些行為並未完全是合法進行。包括：徵信社尋人、尋址、調查[外遇](../Page/外遇.md "wikilink")、調解[婚外情](../Page/婚外情.md "wikilink")、調查[員工或](../Page/員工.md "wikilink")[老闆操守](../Page/老闆.md "wikilink")、追蹤或反跟蹤、[監聽](../Page/監聽.md "wikilink")、[账务追讨](../Page/讨债公司.md "wikilink")、[捉姦和證據保全](../Page/捉姦.md "wikilink")、工商徵信、仿冒調查、婚前徵信、感情問題處理以及各类问题等。

## 起源

\-{zh-cn:[私家侦探](https://www.credits-search.com/);
zh-hk:私家偵探;zh-tw:私家偵探}-業的存在並不僅僅是西方國家中特有的一種社會現象。

[北歐](../Page/北歐.md "wikilink")、[南美和](../Page/南美.md "wikilink")[大洋洲的許多國家中也有](../Page/大洋洲.md "wikilink")-{zh-cn:私家侦探;
zh-hk:私家偵探;zh-tw:私家偵探}-，[亞洲和](../Page/亞洲.md "wikilink")[非洲的一些國家或者地區中也有](../Page/非洲.md "wikilink")-{zh-cn:私家侦探;
zh-hk:私家偵探;zh-tw:私家偵探}-，如[香港](../Page/香港.md "wikilink")、[台灣](../Page/臺灣.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[尚比亞和](../Page/尚比亞.md "wikilink")[辛巴威等](../Page/辛巴威.md "wikilink")。

## 目的

徵信業者提供的徵信服務，依照各家營業訴求而有所不同，綜觀服務範圍涵蓋以下：
工商徵信、財產徵信、商業仿冒調查、[商標](../Page/商標.md "wikilink")、智慧財產權等侵權調查、個人或公司行號的信用調查、犯罪蒐證、債權追償、帳款催收。私人感情生活調查、婚前徵信、偷情劈腿蒐證、[婚外情及外遇蒐證等](../Page/婚外情.md "wikilink")。提供蒐證錄音、行蹤跟監、錄影拍照、法律諮詢、婚姻調解、離婚服務、尋人查址、外遇抓姦、[監護權爭取和婚姻挽回](../Page/監護權.md "wikilink")。

除了感情徵信為大宗之外，另外近年來較多人尋求的徵信服務是尋人尋物(及尋寵物)等，藉由徵信社專業的追查及蒐證能力，尋找遺失的人事物。

## 客戶來源

客戶來自各個階層，但他們聘用私家偵探的原因却各不相同。部份人士因遇上[婚姻等問題](../Page/婚姻.md "wikilink")，認為私家偵探能忠實地為委託人查到真相，因此他們求助，並且有可能以此為通姦證據申請[離婚](../Page/離婚.md "wikilink")，於通姦不合法的國家裡，更可以作為控告[配偶通姦或](../Page/配偶.md "wikilink")[第三者妨害家庭的憑證](../Page/第三者_\(愛情\).md "wikilink")。部份私家偵探社亦兼營[討債公司](../Page/討債公司.md "wikilink")。台灣通姦罪屬於刑法239條，處一年以下有期徒刑，屬於告訴乃論。
除了最大宗的婚姻問題外，徵信社在[尋人這部分的業務也是其獨特之處](../Page/尋人.md "wikilink")。在目前台灣社會，除了部分[戶政事務所](../Page/戶政事務所.md "wikilink")、[警察單位外](../Page/警察.md "wikilink")，就只有徵信社是以民營公司的立場收取酬勞的。
徵信社使用的手段有時候會介於法律邊緣，是否順利找到目標、還是拿錢不辦事等，視該徵信社是否正派。但不可諱言地，
在很多情況下，要解決問題除了請徵信社外，似乎沒有更好的選擇（像是債務尋人）。這也造成台灣民眾有相關需求、但又擔心徵信社拿錢不辦事的矛盾心理。
在台灣許多的徵信工會號稱中立，但實際情況，背後的管理者仍為各大徵信社。

## 各地情況

### 香港

在[香港](../Page/香港.md "wikilink")，私家偵探這個行業是沒有任何[監管的](../Page/監管.md "wikilink")，除了一般的領取公司註冊及商業登記外，許多私家偵探社自稱[政府註冊](../Page/政府.md "wikilink")，其實只是辦理了一般的[香港成立的](../Page/香港.md "wikilink")[公司註冊和](../Page/公司註冊.md "wikilink")[商業登記手續的公司](../Page/商業登記.md "wikilink")，可以在香港[營商](../Page/營商.md "wikilink")，並非己領有任何[牌照](../Page/牌照.md "wikilink")。

### 台灣

在[台灣](../Page/台灣.md "wikilink")，-{zh-cn:-私家侦探; zh-hk:私家偵探;
zh-tw:私家偵探}-就是「-{zh-cn:-征信社; zh-hk:徵信社;
zh-tw:[徵信社](https://www.credits-search.com/)}-」，但須依照[中華民國法律以](../Page/中華民國.md "wikilink")「-{zh-cn:-征信社;
zh-hk:徵信社; zh-tw:徵信社}-」，「[徵信公司](https://www.credits-search.com/)」的名稱註冊。

而現今台灣與大陸已經三通，更多徵信業者打著幫台灣原配到大陸抓台商二奶的噱頭，與原配週休二日直奔上海北京等地抓姦，三通開放亦替台灣徵信業者開創一筆生意。但可能有觸犯隱私權的疑慮。

另外，在部份國家私家偵探是需要領執照的。在中国，私家侦探都是非法執業的。

## 虛構作品中的私家偵探

不少文學作品、電視連續劇及漫畫中的私家偵探，形象都是正直無私、有[職業道德及屢破奇案](../Page/職業道德.md "wikilink")，有操守、有正義感及觸角敏銳，例如[柯南道爾的作品](../Page/柯南道爾.md "wikilink")《[福爾摩斯探案](../Page/福爾摩斯.md "wikilink")》。[日本漫畫](../Page/日本.md "wikilink")[柯南中的偵探社](../Page/柯南.md "wikilink")。
電影作品方面，跟徵信社有關的則有台灣電影傻瓜向前衝、洋片[假會[徵信社](https://www.credits-search.com/)等作品](../Page/假會[https:/www.credits-search.com/_徵信社.md "wikilink")。

## 参见

  - [侦探](../Page/侦探.md "wikilink")
  - [名侦探](../Page/名侦探.md "wikilink")

## 常見的徵信社服務內容

[徵信社](https://www.cue007.com/)服務內容包括：[外遇抓姦](https://www.cue007.com/catch.php)、外遇調查蒐證、老公外遇、老婆外遇、專案尋人、[行蹤調查](https://www.cue007.com/whereabouts.php)、設計離婚等徵信相關業務。

## 參考文獻

  - [徵信社協助外遇調查真相 保障委託人之權益](https://www.global-detective.org)
  - [捍衛您企業的權益,環宇徵信社助您一臂之力](https://www.huanyu007.com/business.html)
  - [消費者心中的謎題 - 徵信社收費怎麼算？](https://www.tmnewa.tw)
  - [徵信社協助女大生協找失散生母](http://www.yuoo.com.tw/news.html)
  - [求助華仲徵信社反被殺 請鬼抓藥單 - 中時電子
    / 2014-2-22](http://www.chinatimes.com/newspapers/20140222000317-260102)
  - [臥底徵信社 幫派份子勒索劈腿人 - 華視
    / 2011/09/07](http://tw.news.yahoo.com/%E8%87%A5%E5%BA%95%E5%BE%B5%E4%BF%A1%E7%A4%BE-%E5%B9%AB%E6%B4%BE%E4%BB%BD%E5%AD%90%E5%8B%92%E7%B4%A2%E5%8A%88%E8%85%BF%E4%BA%BA-220000397.html)

以上为原文在未翻译完前请查看原文 }} <span data-segmentid="6" class="cx-segment">**私家侦探**
(通常简称 **PI** 和非正式称谓**私人侦探**)**私家
[侦探](../Page/侦探.md "wikilink")**或**私人侦探**，是指可以被个人或团体[雇佣从事司法调查服务的人](../Page/雇佣.md "wikilink")。
</span><span data-segmentid="8" class="cx-segment">私家侦探经常在民事和刑事案件中为律师工作。</span>

## 历史

<span data-segmentid="11" class="cx-segment">1833年，<span lang="fr" dir="ltr">Eugène
François Vidocq</span><sup><small><u>（Eugene Francois
Vidocq/佛朗科斯·尤根·维多克/佛朗科斯·尤金·维多克/尤金·弗朗索瓦·维多克</u></small></sup></span><sup><small><u>
<span data-segmentid="11" class="cx-segment">）</span></u></small></sup><span data-segmentid="11" class="cx-segment">，是一位法国
[士兵](../Page/士兵.md "wikilink")、[罪犯](../Page/罪犯.md "wikilink")</span>
<span data-segmentid="11" class="cx-segment">和
[私掠船船长](../Page/私掠船.md "wikilink")，成立了第一家著名的私家侦探社，"Le
Bureau des Renseignements Universels pour le commerce et
l'Industrie"<sup><small><u>（法国商业与工业联合会）</u></small></sup>\[1\] ("The
Office of Universal Information For Commerce and
Industry")<sup><small><u>“工商统一信息办公室”</u></small></sup>，并雇佣了一些刑满释放的罪犯。</span>
<span data-segmentid="15" class="cx-segment">官方执法部门曾多次试图将其关闭。
</span><span data-segmentid="16" class="cx-segment">在1842年，在他破获一宗贪污案件后，警方以涉嫌
[非法监禁](../Page/非法拘禁.md "wikilink")
及冒领[钱财的](../Page/钱财.md "wikilink")[罪名](../Page/罪名.md "wikilink")[拘捕了他](../Page/拘捕.md "wikilink")。
维多克后来察觉到这是一个阴谋</span><span data-segmentid="19" class="cx-segment">。
</span><span data-segmentid="20" class="cx-segment">他被判处五年监禁，3000[法郎罚款](../Page/法郎.md "wikilink")，但上诉法院将他释放。
</span><span data-segmentid="22" class="cx-segment">是维多克</span><span data-segmentid="20" class="cx-segment">将罪证检举</span><span data-segmentid="22" class="cx-segment">、
[犯罪学和](../Page/犯罪學.md "wikilink") [弹道学](../Page/弹道学.md "wikilink")
代入到了刑事侦查中。</span>
<span data-segmentid="25" class="cx-segment">他制作了第一批鞋印的石膏模型。
</span><span data-segmentid="26" class="cx-segment">他创造了不褪色的
[墨水](../Page/墨水.md "wikilink") 和在他的印刷厂发明了模式化证券。
</span><span data-segmentid="28" class="cx-segment">他的人体测量学至今仍被法国警方部分使用。
</span><span data-segmentid="30" class="cx-segment">他还因为慈善事业而受到赞誉——他声称自己从未告发过任何出于真正需要而偷窃的人</span><span data-segmentid="28" class="cx-segment">。</span>

<span data-segmentid="31" class="cx-segment">在维多克之后、这个行业诞生了</span>
<span data-segmentid="31" class="cx-segment">在早期，私家侦探都是在客户认为警方没有做或不愿意做的事情上，充当警察的替代角色。</span>
<span data-segmentid="33" class="cx-segment">这个新的私家侦探行业的还有一个更大的作用就是帮助公司解决劳务工资纠纷。
</span><span data-segmentid="34" class="cx-segment">一些早期的私家侦探还提供武装警卫和私人</span><span data-segmentid="33" class="cx-segment">护卫</span><span data-segmentid="34" class="cx-segment">。\[2\]</span>

在伦敦, 查尔斯·弗雷德里克·菲尔德<sup><small>Charles Frederick Field</small></sup>
于1852年从警署退休后，在当地设立了一个私家侦探所，菲尔德成了查尔斯·狄更斯<sup><small>Charles
Dickens</small></sup>,的朋友，查尔斯·狄更斯写了一些关于菲尔德的文章。

1862年，菲尔德的一名雇员，匈牙利人伊格内修斯·保罗·波拉基<sup><small>Ignatius Paul
Pollaky</small></sup>离开了他，成立了一个与之竞争的机构。虽然如今记载很少，但波拉基在当时的名气让他在19世纪70年代的各种书籍中被提及并因其在1881年的喜剧歌剧“耐心
<sup><small>Patience</small></sup>”中的“敏锐洞察力<sup><small>keen
penetration</small></sup>”而被称为不朽的“帕丁顿<sup><small>Paddington</small></sup>”波拉基。


In the United States, [Allan
Pinkerton](../Page/Allan_Pinkerton.md "wikilink") established the
[Pinkerton National Detective
Agency](../Page/Pinkerton_National_Detective_Agency.md "wikilink") – a
private detective agency – in 1850. Pinkerton became famous when he
foiled [a plot](../Page/Baltimore_Plot.md "wikilink") to
[assassinate](../Page/assassinate.md "wikilink") then President-elect
[Abraham Lincoln](../Page/Abraham_Lincoln.md "wikilink") in 1861.
Pinkerton's agents performed services which ranged from undercover
investigations and detection of crimes, to plant protection and armed
security. It is sometimes claimed, probably with exaggeration, that at
the height of its existence, the Pinkerton National Detective Agency
employed more agents than the [United States
Army](../Page/United_States_Army.md "wikilink").\[3\] Allan Pinkerton
hired [Kate Warne](../Page/Kate_Warne.md "wikilink") in 1856 as a
private detective, making her the first female private detective in
America.\[4\]

During the union unrest in the US in the late 19th century, companies
sometimes hired operatives and armed guards from the Pinkertons. In the
aftermath of the [Homestead
Riot](../Page/Homestead_Strike.md "wikilink") of 1892, several states
passed so-called "anti-Pinkerton" laws restricting the importation of
private security guards during union strikes. The federal
[Anti-Pinkerton Act](../Page/Anti-Pinkerton_Act.md "wikilink") of 1893
continues to prohibit an "individual employed by the Pinkerton Detective
Agency, or similar organization" from being employed by "the Government
of the United States or the government of the District of
Columbia."\[5\]\[6\]

Pinkerton agents were also hired to track western outlaws [Jesse
James](../Page/Jesse_James.md "wikilink"), the [Reno
brothers](../Page/Reno_Gang.md "wikilink"), and the [Wild
Bunch](../Page/Butch_Cassidy's_Wild_Bunch.md "wikilink"), including
[Butch Cassidy](../Page/Butch_Cassidy.md "wikilink") and the [Sundance
Kid](../Page/Sundance_Kid.md "wikilink").

## Employment

Many private detectives/investigators with special academic and
practical experience also work with defense
[attorneys](../Page/lawyer.md "wikilink") on [capital
punishment](../Page/capital_punishment.md "wikilink") and other
[criminal defense](../Page/criminal_defense.md "wikilink") cases. Many
others are [insurance
investigators](../Page/insurance_investigator.md "wikilink") who
investigate suspicious claims. Before the advent of [no-fault
divorce](../Page/no-fault_divorce.md "wikilink"), many private
investigators sought evidence of adultery or other conduct within
marriage to establish grounds for a divorce. Despite the lack of legal
necessity for such evidence in many jurisdictions, according to press
reports, collecting evidence of spouses' and partners' adultery or other
"bad behaviour" is still one of their most profitable undertakings, as
the stakes being fought over now are child custody, alimony, or marital
property disputes.\[7\]

Private investigators can also perform [due
diligence](../Page/due_diligence.md "wikilink") for an investor
considering investing with an investment group, fund manager, or other
high-risk business or investment venture. This could help the
prospective investor avoid being the victim of a fraud or [Ponzi
scheme](../Page/Ponzi_scheme.md "wikilink"). A licensed and experienced
investigator could reveal the investment is risky and/or the investor
has a suspicious background. This is called investigative due diligence,
and is becoming more prevalent in the 21st century with the public
reports of large-scale Ponzi schemes and fraudulent investment vehicles
such as Madoff, Stanford, Petters, Rothstein, and the hundreds of others
reported by the [Securities and Exchange
Commission](../Page/U.S._Securities_and_Exchange_Commission.md "wikilink")
along with other law enforcement agencies.

## Responsibilities

Private investigators also engage in a variety of work not often
associated with the industry in the mind of the public. For example,
many are involved in [process
serving](../Page/service_of_process.md "wikilink"), the personal
delivery of summons, [subpoenas](../Page/subpoena.md "wikilink"), and
other legal documents to parties in a legal case. The tracing of
absconding debtors can also form a large part of a PI's work load. Many
agencies specialize in a particular field of expertise. For example,
some PI agencies deal only in tracing. A handful of firms specialize in
[technical surveillance
counter-measures](../Page/technical_surveillance_counter-measures.md "wikilink"),
sometimes called electronic counter measures, which is the locating and
dealing with unwanted forms of electronic surveillance (for example, a
bugged boardroom for industrial espionage purposes). This niche service
is typically conducted by those with backgrounds in
intelligence/counterintelligence, executive protection, and a small
number from law enforcement entities whose duties included the covert
installation of eavesdropping devices as a tool in organized crime,
terrorism and narco-trafficking investigations. Other PIs, also known as
corporate investigators, specialize in corporate matters, including
antifraud work, loss prevention, internal investigations of employee
misconduct (such as [Equal Employment
Opportunities](../Page/Equal_Employment_Opportunity_Commission.md "wikilink")
violations and sexual harassment), the protection of [intellectual
property](../Page/intellectual_property.md "wikilink") and [trade
secrets](../Page/trade_secret.md "wikilink"), antipiracy, [copyright
infringement](../Page/copyright_infringement.md "wikilink")
investigations, due diligence investigations, malware and cyber criminal
activity, and [computer
forensics](../Page/computer_forensics.md "wikilink") work. Some PIs act
as professional witnesses where they observe situations with a view to
reporting the actions or lack of them to a court or to gather evidence
in [antisocial
behavior](../Page/antisocial_behavior.md "wikilink").\[8\]

## Undercover investigator

An undercover investigator, undercover detective, or undercover agent is
a person who conducts investigations of suspected or confirmed criminal
activity while impersonating a disinterested third party. Undercover
investigators often infiltrate a suspected insurgent group, posing as a
person interested in purchasing illegal goods or services with the
ultimate aim of obtaining information about their assigned target.\[9\]

Many undercover investigators carry [hidden
cameras](../Page/hidden_camera.md "wikilink") and recorders strapped to
their bodies to help them document their investigations. The period of
the investigation could last for several months, or in some extreme
cases, years. Due to the dangerous nature of the job, their real
identities are kept secret throughout their active careers.\[10\]
Economic investigations, business intelligence and information on
competitors, security advice, special security services information,
criminal investigation, investigations background, and profile polygraph
tests are all typical examples of such a role.

Undercover investigators are often misinterpreted as being similar to a
police officer or deputy, however, they are quite the opposite. As
opposed to a police officer, a private or undercover detective is
trained in keeping a low profile, and are under no requirement to wear a
uniform or a badge. Police are trained to be direct in their approach
unless using a disguised vehicle.

Certain types of undercover investigators, depending on their employer,
will investigate allegations of abuse of workman's compensation. Those
claiming to be injured are often investigated and recorded with a hidden
camera/recorder. This is then presented in court or to the client who
paid for the investigation.

## Across the world

Many jurisdictions require PIs to be licensed. Depending on local laws,
they may or may not carry a firearm, some are former law enforcement
agents (including former police officers), some are former spies, some
are former military, some used to work for a private military company,
and some are former bodyguards and security guards. While PIs may
investigate criminal matters, most do not have police authority, and as
such, they are only limited to the powers of [citizen's
arrest](../Page/citizen's_arrest.md "wikilink") and detention that any
other citizen has. They are expected to keep detailed notes and to be
prepared to testify in court regarding any of their observations on
behalf of their clients. Great care is required to remain within the
scope of the law, otherwise the investigator may face criminal charges.
Irregular hours may also be required when performing surveillance
work.\[11\]

### Australia

Private investigators in Australia must be licensed by the licensing
authority relevant to the state where they are located. This applies to
all states except the Australian Capital Territory. Companies offering
investigation services must also hold a business licence and all their
operatives must hold individual licences. Generally, the licences are
administered and regulated by the state police; however, in some states,
this can also be managed by other government agencies.

To become registered in New South Wales requires a CAPI licence, which
can be applied for through the NSW Police Force website. The Australian
Capital Territory does not require PIs to be licensed, although they are
still bound by legislation. PIs working in the ACT cannot enter the NSW
area without a CAPI license, else they will be in breach of the law. In
Queensland, a private investigator need to be licensed under the
[Queensland Government](../Page/Queensland_Government.md "wikilink") and
apply for a private investigator licence\[12\] by completing an
application for a security provider licence. Applicant will need to have
a criminal history check and submit fingerprint.

### UK

In 2001, the government passed the licensing of private investigators
and private investigation firms in the UK over to the [Security Industry
Authority](../Page/Security_Industry_Authority.md "wikilink") (SIA),
which acted as the regulatory body from then on. However, due to the
cutbacks of this agency, licensing of private investigators in the UK
was halted indefinitely. At present, no government-backed authorities in
the UK license private investigators.

The SIA have announced that PIs in the UK were to become licensed for
the first time from May 2015, but this is only the scheduled date for
the issue to be discussed in parliament. In December 2014, Corporate
Livewire produced an article written by a UK private investigator at BAR
Investigations, addressing the issues surrounding private investigation
in the UK.\[13\]\[14\]

### United States

Private investigators in the United States may or may not be licensed or
registered by a government licensing authority or state police of the
state where they are located. Licensing varies from state to state and
can range from: a) no state license required; b) city or state business
license required (such as in five states
([Idaho](../Page/Idaho.md "wikilink"),
[Alaska](../Page/Alaska.md "wikilink"),
[Mississippi](../Page/Mississippi.md "wikilink"), [South
Dakota](../Page/South_Dakota.md "wikilink"), and
[Wyoming](../Page/Wyoming.md "wikilink")); c) to needing several years
of experience and licensing-related training classes and testing (as is
the case with [Virginia](../Page/Virginia.md "wikilink") and
[California](../Page/California.md "wikilink")).\[15\] In many states,
companies offering investigation services must hold an agency license,
and all of their investigators or detectives must hold individual
licenses or registrations; furthermore, certain states such as
[Washington](../Page/Washington_\(state\).md "wikilink") have separate
classes of licensing for roles such as trainers of private
investigators.\[16\] A few reciprocity agreements allow a detective
working in one state to continue work in another for a limited time
without getting a separate license, but not all states participate in
these agreements.\[17\]

In 1877, [Colorado](../Page/Colorado.md "wikilink") became the first
state in the union to institute licensing requirements for private
investigators. Because of the vague definition of term 'private
investigator', the law was declared unconstitutional in 1977, but
reinstated on a voluntary basis in July 2012 and mandatory in June
2015.\[18\]\[19\]

### Canada

Private investigators in Canada are licensed at the provincial level by
the appropriate body. For instance, in the province of
[Ontario](../Page/Ontario.md "wikilink"), private investigators are
licensed and regulated by the Ministry of Community Safety &
Correctional Services (MCSCS).\[20\] In the province of
[Alberta](../Page/Alberta.md "wikilink"), private investigators are
licensed and regulated by the Alberta Justice and Solicitor
General.\[21\] Similar licensing requirements apply in other [provinces
and territories of
Canada](../Page/provinces_and_territories_of_Canada.md "wikilink"). As
per the [Ontario](../Page/Ontario.md "wikilink") text of the
\[<https://www.ontario.ca/laws/statute/05p3> Private Security and
Investigative Services Act of 2005\], private investigators are
forbidden from referring to themselves as
[detective](../Page/detective.md "wikilink") or [private
detective](../Page/private_detective.md "wikilink"). In order to become
a licensed private investigator, you must be 18 years of age or older in
[Ontario](../Page/Ontario.md "wikilink") (in other [Provinces and
territories of
Canada](../Page/Provinces_and_territories_of_Canada.md "wikilink") the
eligible age to work may be higher); have a clean criminal record or
obtain a waiver; and submit a correctly completed application for a
license. You are required to complete 50-hours of basic training with an
accredited source such as a university, college, or through private
agencies licensed to administer the course. Upon completion of basic
training, individuals are required to write and pass the basic test to
obtain a private investigator's license.

## Global Networks

There are a number of global networks which helps Private Investigators
to stay connected, share resources and carry out work across borders.
Some of these are [World Association of
Detectives](../Page/World_Association_of_Detectives.md "wikilink") and
The Association of British Investigators. These organisations promote
ethical practices in the profession of Private Investigator or security
service throughout the world.

## Fiction

The PI genre in fiction dates to [Edgar Allan
Poe](../Page/Edgar_Allan_Poe.md "wikilink"), who created the character
[C. Auguste Dupin](../Page/C._Auguste_Dupin.md "wikilink") in the 1840s.
Dupin, an amateur crime-solver residing in Paris, appeared in three Poe
stories.

## Notable private investigators

### In reality

  - [Rick Crouch](../Page/Rick_Crouch.md "wikilink")
  - [Charles Frederick
    Field](../Page/Charles_Frederick_Field.md "wikilink")
  - [Dashiell Hammett](../Page/Dashiell_Hammett.md "wikilink") (also a
    notable author of detective fiction)
  - [Anthony Pellicano](../Page/Anthony_Pellicano.md "wikilink")
  - [Allan Pinkerton](../Page/Allan_Pinkerton.md "wikilink")
  - [Justin Hopson](../Page/Justin_Hopson.md "wikilink")
  - [Daniel Ribacoff](../Page/Daniel_Ribacoff.md "wikilink")

### In fiction

Where the characters below do not meet the strict criteria of a private
investigator (i.e. available for hire) it is noted in brackets.

  - [Sherlock Holmes](../Page/Sherlock_Holmes.md "wikilink")
  - [L. Lawliet](../Page/L._Lawliet.md "wikilink")
  - [Feluda](../Page/Feluda.md "wikilink")
  - [Byomkesh Bakshi](../Page/Byomkesh_Bakshi.md "wikilink")
  - [Bhaduri Moshai](../Page/Bhaduri_Moshai.md "wikilink")
  - [Three Investigators](../Page/Three_Investigators.md "wikilink")
    (Amateur juvenile detectives)
  - [Miss Marple](../Page/Miss_Marple.md "wikilink") (Amateur detective)
  - [Mitin Masi](../Page/Mitin_Masi.md "wikilink")
  - [Harry Dresden](../Page/Harry_Dresden.md "wikilink")
  - [Auguste Dupin](../Page/Auguste_Dupin.md "wikilink") (Amateur
    [Gentleman detective](../Page/Gentleman_detective.md "wikilink"))
  - [Nancy Drew](../Page/Nancy_Drew.md "wikilink") (Amateur juvenile
    detective)
  - [Mike Hammer](../Page/Mike_Hammer.md "wikilink")
  - [The Hardy Boys](../Page/The_Hardy_Boys.md "wikilink") (Amateur
    juvenile detectives)
  - [Laura Holt](../Page/Remington_Steele.md "wikilink")
  - [Parashor Barma](../Page/Parashor_Barma.md "wikilink")
  - [Jessica Jones](../Page/Jessica_Jones.md "wikilink")
  - [Thomas Magnum](../Page/Thomas_Magnum.md "wikilink")
  - [Joe Mannix](../Page/Joe_Mannix.md "wikilink")
  - [Philip Marlowe](../Page/Philip_Marlowe.md "wikilink")
  - [Veronica Mars](../Page/Veronica_Mars.md "wikilink")
  - [Kinsey Millhone](../Page/Kinsey_Millhone.md "wikilink")
  - [Adrian Monk](../Page/Adrian_Monk.md "wikilink")
  - [Hercule Poirot](../Page/Hercule_Poirot.md "wikilink")
  - [Jim
    Rockford](../Page/Jim_Rockford_\(television_character\).md "wikilink")
  - [Mma Precious Ramotswe](../Page/Mma_Precious_Ramotswe.md "wikilink")
  - [Ezekiel "Easy"
    Rawlins](../Page/Ezekiel_"Easy"_Rawlins.md "wikilink")
  - [Simon & Simon](../Page/Simon_&_Simon.md "wikilink")
  - [Sam & Max](../Page/Sam_&_Max.md "wikilink")
  - [Sam Spade](../Page/Sam_Spade.md "wikilink")
  - [Kiriti Roy](../Page/Kiriti_Roy.md "wikilink")
  - [Shawn Spencer](../Page/Shawn_Spencer.md "wikilink")
  - [Spenser](../Page/Spenser_\(character\).md "wikilink")
  - [Cormoran Strike](../Page/Cormoran_Strike.md "wikilink")
  - [Varg Veum](../Page/Varg_Veum.md "wikilink")
  - [V. I. Warshawski](../Page/V._I._Warshawski.md "wikilink")
  - [Nero Wolfe](../Page/Nero_Wolfe.md "wikilink")
  - [Angel (Buffy the Vampire
    Slayer)](../Page/Angel_\(Buffy_the_Vampire_Slayer\).md "wikilink")
  - [The Bloodhound
    Gang](../Page/The_Bloodhound_Gang_\(TV_series\).md "wikilink")
    (Amateur juvenile detectives)
  - [Tex Murphy](../Page/Tex_Murphy.md "wikilink")
  - [Hetty Wainthropp](../Page/Hetty_Wainthropp.md "wikilink")
  - [Rip Kirby](../Page/Rip_Kirby.md "wikilink")

## See also

  - [Bounty hunter](../Page/Bounty_hunter.md "wikilink")
  - [Detective](../Page/Detective.md "wikilink")
      - [Hotel detective](../Page/Hotel_detective.md "wikilink")
  - [Insurance
    investigator](../Page/Insurance_investigator.md "wikilink")
  - [Mystery film](../Page/Mystery_film.md "wikilink")
  - [Private police](../Page/Private_police.md "wikilink")

## References

## External links

  - [Bureau of Labor Statistics
    information](http://www.bls.gov/ooh/protective-service/private-detectives-and-investigators.htm)

[\*](../Category/Private_detectives_and_investigators.md "wikilink")
[Category:Occupations](../Category/Occupations.md "wikilink")
[Category:偵探](../Category/偵探.md "wikilink")
[Category:職業](../Category/職業.md "wikilink")

1.  [Historique des détectives et enquêteurs privés et grandes dates de
    la profession](http://ufedp.online.fr/historique-detectives.htm)  –
    "Le Bureau des Renseignements Universels pour le commerce et
    l'Industrie”

2.

3.
4.

5.
6.  5 U.S. Code 3108; Public Law 89-554, 80 Stat. 416 (1966); ch. 208
    (5th par. under "Public Buildings"), 27 Stat. 591 (1893). The U.S.
    Court of Appeals for the Fifth Circuit, in *U.S. ex rel. Weinberger*
    v. *Equifax*, 557 F.2d 456 (5th Cir. 1977), *cert. denied*, 434 U.S.
    1035 (1978), held that "The purpose of the Act and the legislative
    history reveal that an organization was 'similar' to the Pinkerton
    Detective Agency only if it offered for hire mercenary,
    quasi-military forces as strikebreakers and armed guards. It had the
    secondary effect of deterring any other organization from providing
    such services lest it be branded a 'similar organization.'" 557 F.2d
    at 462; *see also*

7.

8.
9.

10.

11.
12.  Security (manpower) licence
    |url=<https://www.qld.gov.au/law/laws-regulated-industries-and-accountability/queensland-laws-and-regulations/regulated-industries-and-licensing/regulated-industries-licensing-and-legislation/security-industry-regulation/get-a-security-licence/security-manpower-licence/apply-for-a-private-investigator-licence>
    |website=www.qld.gov.au |language=en}}

13.

14.

15.

16.

17.

18.

19.  License for PI Jobs and Training School
    Requirements|website=privateinvestigatoredu.org|language=en-US|access-date=2018-08-30}}

20.

21.