**秋山依里**（），原藝名**秋山奈奈**，[日本](../Page/日本.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")、[演員及](../Page/演員.md "wikilink")[歌手](../Page/歌手.md "wikilink")。出身自[東京](../Page/東京.md "wikilink")，隸屬[BUGSY公司](../Page/BUGSY.md "wikilink")（）。[童星出身](../Page/童星.md "wikilink")，自幼經常擔任時裝雜誌模特兒。後來出演[特攝作品](../Page/特攝.md "wikilink")《[假面騎士響鬼](../Page/假面騎士響鬼.md "wikilink")》女角**天美晶**，人氣始增。身高155cm。

## 個人簡介

**秋山奈奈**本身是一名[童星模特兒](../Page/童星.md "wikilink")，因相貌可愛而被發掘，開始為一些少女時裝雜誌充當模特兒。2005年參與特攝作品《[假面騎士響鬼](../Page/假面騎士響鬼.md "wikilink")》，出演**威吹鬼**的女弟子**天美晶**，大受歡迎，因而為人所熟悉。

2006年度高中畢業，不斷接拍廣告與及電視演出，知名度逐步提升。同年開始踏足歌唱之途，推出個人單曲作品。目前秋山奈奈正以**演員**為事業重心，同時亦以**歌手**及**水著偶像**的身份活躍於藝能界。擅長[書法](../Page/書法.md "wikilink")、演奏[單簧管與及](../Page/單簧管.md "wikilink")[舞蹈](../Page/舞蹈.md "wikilink")，喜歡吃[甜和](../Page/甜.md "wikilink")[辣的東西](../Page/辣.md "wikilink")，家裡養了一隻名為「モモ（[桃](../Page/桃.md "wikilink")）」的貓。患有[哮喘](../Page/哮喘.md "wikilink")。

已於2010年7月和相差三歲以上的男性結婚，並於2011年4月生子。消息於2012年11月2日於自己的部落格公開，並且同時宣布改藝名為「秋山依里」。\[1\]

## 演出紀錄

### 電視劇

  - 假面騎士響鬼（2005年 - 2006年） - 飾演：天美あきら
  - [佐藤四姊妹](../Page/佐藤四姊妹.md "wikilink")（2005年） - 飾演：佐藤奈々
  - [恋する日曜日第](../Page/恋する日曜日.md "wikilink")12回（2005年） - 飾演：常盤ハル
  - [東京少女之回転少女](../Page/東京少女.md "wikilink")（2006年） - 飾演：井之原凜子
  - [死神的歌谣](../Page/死神的歌谣.md "wikilink")（2007年） - 客串演出：藤浦トマト
  - [恋する日曜日](../Page/恋する日曜日.md "wikilink")（第3輯）（2007年） - 飾演：山中きのこ
  - [探偵学園Q第](../Page/探偵学園Q.md "wikilink")8話（2007年） - 飾演：帆華
  - [コスプレ幽霊 紅蓮女](../Page/紅蓮女.md "wikilink")（2008年） - 飾演：本間響子
  - [音女](../Page/音女.md "wikilink")（2008年6月10日） - 飾演：佳乃
  - [手機搜查官7](../Page/手機搜查官7.md "wikilink")（2008年8月6日） - 客串演出：五十嵐陽子　
  - [貓街](../Page/貓街.md "wikilink")（2008年9月26日） - 客串演出：惠美
  - [假面騎士DECADE](../Page/假面騎士DECADE.md "wikilink")（2009年） -
    客串演出：天美あきら、假面騎士天鬼

### 電台

  - 秋山奈奈的（2006年7月） -　擔任DJ

  - （2006年10月 - 2007年3月） -　擔任助手

  - 「秋山奈奈的Cafe de nana」（2007年7月）-　逢星期二深夜

### 電影

  - 《[假面騎士響鬼與七人的戰鬼](../Page/假面騎士響鬼與七人的戰鬼.md "wikilink")》（2005年9月3日上映） -
    飾演　鈴
  - 《BS-i Creator Box - 恋する日曜日～アニー》 - 飾演　常盤ハル
  - 《[學校的階梯](../Page/學校的階梯.md "wikilink")》（2007年4月28日上映） - 飾演　井筒奈美
  - 《ガチバン》（2008年7月26日上映） - 飾演 北白川アヤ
  - 《Fure Fure 少女》（預計2008年9月上映）\[2\] - 飾演　奈津子
  - 《コードブレイカー507 上海圓舞曲》（預計2009年[春季上映](../Page/春季.md "wikilink")）
  - 《Departed to the future》 - 飾演　KOTOKO（預計2009年3月25日上映）

### DVD

  - DEATH FILE（2006年） - 飾演　木內葉子
  - DEATH FILE2（2007年） - 飾演　木內葉子

|                    |                              |                    |                                                 |                           |                                   |
| ------------------ | ---------------------------- | ------------------ | ----------------------------------------------- | ------------------------- | --------------------------------- |
|                    | <small>名稱</small>            | <small>發賣日</small> | <small>規格</small>                               | <small>生產編號</small>       | <small>ISBN</small>               |
| <small>1st</small> | **pupil**                    | 2006年5月27日         | <small>[DVD](../Page/DVD.md "wikilink")</small> | <small>WBDV-0010</small>  | <small>ISBN 4-8470-2934-8</small> |
| <small>2nd</small> | **Trip to…**                 | 2007年3月14日         | <small>DVD</small>                              | <small> WBDV-0018</small> | <small>ISBN 4-8470-2994-3</small> |
| <small>3rd</small> | **Light Bright**             | 2007年12月19日        | <small>DVD</small>                              | <small>SSBX-2237</small>  | <small>ISBN</small>               |
| <small>1st</small> | **StilL ProgresS DVD vol.1** | 2006年7月26日         | <small>[DVD](../Page/DVD.md "wikilink")</small> | <small>VIBL-324</small>   | <small>ISBN</small>               |
| <small>2nd</small> | **StilL ProgresS DVD vol.2** | 2007年3月21日         | <small>DVD</small>                              | <small>VIBL-378</small>   | <small>ISBN</small>               |
| <small>3rd</small> | **StilL ProgresS DVD vol.3** | 2007年10月24日        | <small>DVD</small>                              | <small>VIBL-398</small>   | <small>ISBN</small>               |

### 舞台演出

  - [身邊的守護神](../Page/身邊的守護神.md "wikilink")（2008年2月1日～7日預定）

### 動畫配音

  - 2008年

<!-- end list -->

  - 《[屍姬](../Page/屍姬.md "wikilink") 赫》（**星村真姬那**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - 《[屍姬](../Page/屍姬.md "wikilink") 玄》（**星村真姬那**）

### 廣告

  - [日產汽車](../Page/日產汽車.md "wikilink")「NISSAN 70th 新聞廣告」（2003年8月 - 12月）
  - [住友林業](../Page/住友林業.md "wikilink")「理想篇」「見習篇」（2003年10月 - 2004年9月）
  - ネスレ日本「キットカット・車站編」（2004年10月 - 12月）
  - [花王](../Page/花王株式會社.md "wikilink")「エコナ」（2007年4月 - ）

### CD

<table>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p><small>發賣日</small></p></td>
<td><p><small>名稱</small></p></td>
<td><p><small>規格</small></p></td>
<td><p><small>生產編號</small></p></td>
</tr>
<tr class="even">
<td><p><small>1st</small></p></td>
<td><p>2006年7月26日</p></td>
<td><p><strong>夜明け前</strong></p></td>
<td><p><small>12 cm<a href="../Page/CD.md" title="wikilink">CD</a></small></p></td>
<td><p><small>VICL-36088</small></p></td>
</tr>
<tr class="odd">
<td><p><small>2nd</small></p></td>
<td><p>2006年11月22日</p></td>
<td><p><strong>オレンジ色</strong></p></td>
<td><p><small>12 cm<a href="../Page/CD.md" title="wikilink">CD</a></small></p></td>
<td><p><small>VICL-36193（初回盤）</small><br />
<small>VICL-36194（通常盤）</small></p></td>
</tr>
<tr class="even">
<td><p><small>3rd</small></p></td>
<td><p>2007年2月14日</p></td>
<td><p><strong>さよならとはじまり</strong></p></td>
<td><p><small>12 cm<a href="../Page/CD.md" title="wikilink">CD</a></small></p></td>
<td><p><small>VICL-36206（初回盤）</small><br />
<small>VICL-36207（通常盤）</small></p></td>
</tr>
<tr class="odd">
<td><p><small>4th</small></p></td>
<td><p>2007年9月19日</p></td>
<td><p><strong>同じ星</strong></p></td>
<td><p><small>12 cm<a href="../Page/CD.md" title="wikilink">CD</a></small></p></td>
<td><p><small>VICL-36339</small></p></td>
</tr>
<tr class="even">
<td><p><small>5th</small></p></td>
<td><p>2008年10月1日</p></td>
<td><p><strong>空を泳ぐさかな</strong></p></td>
<td><p><small>12 cm<a href="../Page/CD.md" title="wikilink">CD</a></small></p></td>
<td><p><small>VICL-36463</small></p></td>
</tr>
</tbody>
</table>

## 備註

## 外部連結

  - [ABPinc MEMBER
    秋山奈奈官方網頁](https://web.archive.org/web/20080105105117/http://www.abp-inc.co.jp/nana_akiyama.html)
    - ABPinc
  - [xxx](http://ameblo.jp/1989x12x26/) - 秋山奈奈部落格（2010年9月啟用）
  - [StilL ProgresS instant memory from nana by
    Ameba](http://ameblo.jp/still-progress/) - 秋山奈奈部落格（2008年4月啟用）
  - [StilL ProgresS+ ～instant memory from
    \*nana\*～](https://web.archive.org/web/20070211225258/http://blog.jvcmusic.co.jp/blog/akiyama-nana/)
    - 秋山奈奈部落格（2008年4月以前）
  - [秋山奈奈個人網頁](http://www.jvcmusic.co.jp/nana/) - Victor Entertainment

[Category:日本電影演員](../Category/日本電影演員.md "wikilink")
[Category:日本寫真偶像](../Category/日本寫真偶像.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本電視演員](../Category/日本電視演員.md "wikilink")
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:日本女性模特兒](../Category/日本女性模特兒.md "wikilink")
[Category:Pichilemon模特兒](../Category/Pichilemon模特兒.md "wikilink")
[Category:勝利娛樂旗下藝人](../Category/勝利娛樂旗下藝人.md "wikilink")

1.  <http://ameblo.jp/1989x12x26/entry-11394436260.html>
2.  [電影官方網頁](http://fure-fure.jp/)