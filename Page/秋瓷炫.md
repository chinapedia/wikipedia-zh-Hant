**秋瓷炫**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")，\[1\]2006年憑《》獲韩国电影[大钟奖最佳女新人奖和](../Page/大钟奖.md "wikilink")[釜山电影节影评人协会最佳女配角奖等各大獎項](../Page/釜山电影节.md "wikilink")，2009年憑《[美人圖](../Page/美人圖：私情畫慾.md "wikilink")》獲大鐘獎和[青龍獎最佳女配角提名](../Page/青龍獎.md "wikilink")。2011年主演《[回家的誘惑](../Page/回家的誘惑.md "wikilink")》，創下高收視率。

## 經歷

### 早年

秋瓷炫出生於韓國[大邱](../Page/大邱.md "wikilink")，幼時妹妹溺水身亡造成家庭陰影。\[2\]17歲出道，開始接拍廣告。18歲父母因感情失和離異，媽媽移居[日本](../Page/日本.md "wikilink")，秋瓷炫則一個人居住在[首爾](../Page/首爾.md "wikilink")，進入[檀國大學戲劇電影系就讀](../Page/檀國大學.md "wikilink")。2004年與[彭于晏合作](../Page/彭于晏.md "wikilink")《戀香》，飾演外表堅強，內心缺乏安全感的尹香之。2006年為了詮釋電影《》中的吸毒患者，秋瓷炫在兩個禮拜內成功減重8公斤，並憑藉該角色拿下多項大獎。

### 中國發展

2006年接演[古龍小說改編之電視劇](../Page/古龍.md "wikilink")《[大旗英雄傳](../Page/大旗英雄傳.md "wikilink")》，出飾輕功卓絕的女主水靈光。該劇由[游建鳴製片](../Page/游建鳴.md "wikilink")，深受好評，翌年並獲得[新浪網](../Page/新浪網.md "wikilink")「最佳古裝劇獎」。秋瓷炫表示她在韓國時，就經常看中國的[武俠劇](../Page/武俠.md "wikilink")，這個角色實現了她做俠女的飛天夢想，而此劇也是她與游建鳴首次合作。2007年再次詮釋古龍筆下的女角石觀音，演繹癡情偏激的女魔頭，得到觀眾肯定。2011年，在《[回家的誘惑](../Page/回家的誘惑.md "wikilink")》中，展現個性反差截然不同的演技。《回家的誘惑》以其優異的收視率（最高5.19%），成為當時熱議的話題。同年，與強視傳媒簽約，宣布將事業重心移至中國。

## 个人生活

2015年9月16日公开与中国男演员[-{于}-曉光正在交往](../Page/于晓光.md "wikilink")，两人因拍摄《铁血英雄路》认识，后逐渐发展为恋人关系。2017年7月10日在韩国[SBS节目](../Page/SBS_\(韩国\).md "wikilink")《[同床异梦2，你是我的命运](../Page/同床異夢2_-_你是我的命運.md "wikilink")》中爆出已经于2017年1月18日两人领证结婚。\[3\]2017年10月，秋瓷炫在《同床異夢2
-
你是我的命運》節目上公開宣布已懷孕2個月。2018年6月1日在韩国首尔生下一子，其生下的儿子取名为“大海”\[4\]。4日卻傳出秋瓷炫突然出現產後癲癇症狀，被送到急診室進行急救。\[5\]\[6\]

## 影視作品

### 電視劇（韩国）

|       |                                    |                                                     |         |
| ----- | ---------------------------------- | --------------------------------------------------- | ------- |
| 年分    | 電視台                                | 電視劇名稱                                               | 角色      |
| 1996年 | SBS                                | 《[成长的感觉18岁](../Page/成长的感觉18岁.md "wikilink")》（）      | 明珠      |
| 1997年 | KBS                                | 《[锦绣年华](../Page/锦绣年华.md "wikilink")》                |         |
| 1998年 | MBC                                | 《[長男](../Page/長男_\(韓國電視劇\).md "wikilink")》          |         |
| 1999年 | MBC                                | 《最后的战争》(婚姻战争、邂逅)                                    | 瓷炫      |
| 1999年 | SBS                                | 《[KAIST科技大学2](../Page/KAIST_\(電視劇\).md "wikilink")》 | 秋瓷炫     |
| 2000年 | MBC                                | 《[为了你](../Page/为了你.md "wikilink")》                  |         |
| 2001年 | SBS                                | 单元剧《[男与女](../Page/男与女.md "wikilink")》               |         |
| 2001年 | SBS                                | 《[外出](../Page/外出.md "wikilink")》                    | 兰英      |
| 2001年 | SBS                                | 《[情伤最是重逢时](../Page/情伤最是重逢时.md "wikilink")》          |         |
| 2002年 | SBS                                | 《[開朗少女成功記](../Page/開朗少女成功記.md "wikilink")》          | 宋宝贝     |
| 2003年 | SBS                                | 《[破晓之家](../Page/破晓之家.md "wikilink")》                |         |
| 2003年 | SBS                                | 《[狎鷗亭宗家](../Page/狎鷗亭宗家.md "wikilink")》              | 白瓷炫     |
| 2004年 | MBC                                | 《[天生缘分](../Page/天生缘分.md "wikilink")》                |         |
| 2004年 | KBS                                | 《[OH\!必勝奉順英](../Page/OH!必勝奉順英.md "wikilink")》       | 許松子     |
| 2005年 | OCN                                | 《[家族戀愛史](../Page/家族戀愛史.md "wikilink")》              | 善雅      |
| 2010年 | MBC                                | 《[被稱為神的男子](../Page/被稱為神的男子.md "wikilink")》          | 徐美秀/崔强熙 |
| 2019年 | [JTBC](../Page/JTBC.md "wikilink") | 《[美麗的世界](../Page/美麗的世界.md "wikilink")》              | 姜仁荷     |

### 電視劇（中國大陸、台灣）

|                                              |                                                     |                                                     |                                           |
| -------------------------------------------- | --------------------------------------------------- | --------------------------------------------------- | ----------------------------------------- |
| 年分                                           | 名稱                                                  | 角色                                                  | 备注                                        |
| 2004年                                        | 《[戀香](../Page/戀香.md "wikilink")》                    | 尹香之                                                 | 台韓合拍                                      |
| 2006年                                        | 《[大旗英雄傳](../Page/大旗英雄传_\(2007年电视剧\).md "wikilink")》 | [水靈光](../Page/水靈光.md "wikilink")                    |                                           |
| 2007年                                        | 《[楚留香傳奇](../Page/楚留香传奇_\(电视剧\).md "wikilink")》      | [石觀音](../Page/石觀音.md "wikilink")                    |                                           |
| 《[讓愛化作珍珠雨](../Page/讓愛化作珍珠雨.md "wikilink")》   | 貝心如                                                 |                                                     |                                           |
| 2011年                                        | 《[回家的誘惑](../Page/回家的誘惑.md "wikilink")》              | 高珊珊/林品如                                             | 中國版《[妻子的诱惑](../Page/妻子的诱惑.md "wikilink")》 |
| 2012年                                        | 《[從將軍到士兵](../Page/從將軍到士兵.md "wikilink")》            | 關碧雲                                                 |                                           |
| 《[新烏龍山剿匪記](../Page/新烏龍山剿匪記.md "wikilink")》   | 四丫頭                                                 |                                                     |                                           |
| 《[木府风云](../Page/木府风云.md "wikilink")》         | 阿勒邱                                                 |                                                     |                                           |
| 2013年                                        | 《[狐仙](../Page/狐仙_\(电视剧\).md "wikilink")》            | [聶小倩](../Page/聶小倩.md "wikilink")                    |                                           |
| 《[舞乐传奇](../Page/舞乐传奇.md "wikilink")》         | 夜莎罗                                                 |                                                     |                                           |
| 2014年                                        | 《[长安三怪探](../Page/长安三怪探.md "wikilink")》              | 韋若昭                                                 |                                           |
| 《[秀秀的男人](../Page/秀秀的男人.md "wikilink")》       | 秀秀                                                  |                                                     |                                           |
| 2015年                                        | 《[南侨机工英雄传](../Page/南侨机工英雄传.md "wikilink")》          | 郁兰亭/郁雪晴                                             | 又名《铁血英雄路》                                 |
| 《[最后一战](../Page/最后一战_\(电视剧\).md "wikilink")》 | 諶劍玉                                                 |                                                     |                                           |
| 《[恋上黑天使](../Page/恋上黑天使.md "wikilink")》       | 叶曼凌                                                 | 原名《恋上你,爱上我》                                         |                                           |
| 《猎虎》                                         | 韩潇雨                                                 | 原名《猎虎1946》, 2012年拍攝                                 |                                           |
| 2016年                                        | 《[幸福在一起](../Page/幸福在一起.md "wikilink")》              | 高真真                                                 |                                           |
| 2017年                                        | 《[華麗上班族](../Page/華麗上班族_\(電視劇\).md "wikilink")》      | 張威                                                  | [越南首播](../Page/越南.md "wikilink")          |
| 《[向幸福奔跑](../Page/向幸福奔跑.md "wikilink")》       | 杨虹                                                  | \[7\][韓國首播](../Page/韓國.md "wikilink")，又名《麻辣女友的幸福时光》 |                                           |
|                                              |                                                     |                                                     |                                           |

### 電影

|       |                                                         |     |
| ----- | ------------------------------------------------------- | --- |
| 年分    | 電影名稱                                                    | 角色  |
| 1996年 | 《朴峰坤离家事件》（） ...  \[8\]\[9\]                             |     |
| 2005年 | 《》                                                      |     |
| 2005年 | 《》                                                      |     |
| 2006年 | 《》（）\[10\]（臺灣：正邪無間） \[11\]\[12\]                        | 智英  |
| 2008年 | 《[美人圖](../Page/美人圖_\(電影\).md "wikilink")》               | 雪花  |
| 2009年 | 《[失蹤](../Page/失蹤_\(電影\).md "wikilink")》（）\[13\]         | 贤贞  |
| 2010年 | 《》（）... Seong-chan's mother (cameo)                     |     |
| 2010年 | 《[幻想剧场](../Page/幻想剧场.md "wikilink")》（）\[14\] - 《》\[15\] |     |
| 2010年 | 《[无法忍受](../Page/无法忍受.md "wikilink")》（）\[16\]            | 芷欣  |
| 2014年 | 《[全城通緝](../Page/全城通緝.md "wikilink")》                    | 林嵐  |
| 2017年 | 《[遊戲規則](../Page/遊戏规则_\(2017年电影\).md "wikilink")》        | 蓝若云 |
|       |                                                         |     |

### 音樂電影

|       |                                  |                                  |    |                                                                           |
| ----- | -------------------------------- | -------------------------------- | -- | ------------------------------------------------------------------------- |
| 年度    | 歌手                               | 歌曲                               | 導演 | 合作演員                                                                      |
| 2014年 | [姚貝娜](../Page/姚貝娜.md "wikilink") | [麗江之戀](../Page/麗江.md "wikilink") | 童年 | [朱曉漁](../Page/朱曉漁.md "wikilink")、-{于}-榮光、[金素妍](../Page/金素妍.md "wikilink") |

## 綜藝／訪談節目

|                          |                                                          |       |            |
| ------------------------ | -------------------------------------------------------- | ----- | ---------- |
| 年分                       | 節目名稱                                                     | 電視台   | 演出型態       |
| 2011年3月6日                | 《[給力星期天](../Page/給力星期天.md "wikilink")》                   | 湖南衛視  | 特別嘉賓       |
| 2011年3月19日               | 《[快樂大本營](../Page/快樂大本營.md "wikilink")》                   | 湖南衛視  | 特別嘉賓       |
| 2011年4月30日               | 《[快樂大本營](../Page/快樂大本營.md "wikilink")》                   | 湖南衛視  | 特別嘉賓       |
| 2011年5月7日                | 《[非常靜距離](../Page/非常靜距離.md "wikilink")》                   | 安徽衛視  | 特別訪問       |
| 2012年6月24日               | 《[影視俱樂部](../Page/影視俱樂部.md "wikilink")》                   | 央視    | 特別訪問       |
| 2012年7月1日                | 《[影視俱樂部](../Page/影視俱樂部.md "wikilink")》                   | 央視    | 特別訪問       |
| 2014年1月5日                | 《[影視俱樂部](../Page/影視俱樂部.md "wikilink")》                   | 央視    | 特別訪問       |
| 2014年1月12日               | 《[影視俱樂部](../Page/影視俱樂部.md "wikilink")》                   | 央視    | 特別訪問       |
| 2014年10月10日\~12月19日（每週五） | 《[明星到我家](../Page/明星到我家.md "wikilink")》                   | 江蘇衛視  | 固定出演       |
| 2015年9月13日               | 《[你吃飯了嗎](../Page/吃好過好之法您吃了嗎.md "wikilink")》              | 韓國SBS | 特別嘉賓       |
| 2015年9月17日               | 《[你吃飯了嗎](../Page/吃好過好之法您吃了嗎.md "wikilink")》              | 韓國SBS | 特別嘉賓       |
| 2017年7月10日\~2018年3月26日   | 《[同床異夢2 - 你是我的命運](../Page/同床異夢2_-_你是我的命運.md "wikilink")》 | 韓國SBS | E1-E38,E52 |
| 2017年8月15日               | 《深夜TV演藝》                                                 | 韓國SBS | 特別嘉賓       |

## 獎項

<table>
<thead>
<tr class="header">
<th><p>年分</p></th>
<th><p>大獎</p></th>
<th><p>獎項</p></th>
<th><p>作品</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2006年</p></td>
<td></td>
<td><p>最佳女新人奖</p></td>
<td><p>《生死決斷》（臺灣：正邪無間）</p></td>
<td><p>[17]</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>韓國電影大獎</p></td>
<td><p>最佳女配角奖</p></td>
<td><p>《生死決斷》</p></td>
<td><p></p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/釜山國際電影節.md" title="wikilink">釜山電影節影評人協會</a></p></td>
<td><p>最佳女配角奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>电影部门<br />
（）</p></td>
<td><p>优秀演员奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>韩国电影<a href="../Page/大钟奖.md" title="wikilink">大钟奖</a></p></td>
<td><p>最佳女新人奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>韩国电影导演评鉴</p></td>
<td><p>最佳女新人奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/百想艺术大赏.md" title="wikilink">百想艺术大赏</a></p></td>
<td><p>最佳女新人奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>韩国电影<a href="../Page/青龍電影獎.md" title="wikilink">青龙奖</a></p></td>
<td><p>最佳女新人奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>韩国电影青龙奖</p></td>
<td><p>最佳女配角奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/亚洲电影促进联盟.md" title="wikilink">亚洲电影促进联盟</a><br />
（舊譯「亚洲影评人协会」）</p></td>
<td><p>最佳女配角奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>亚洲电影促进联盟</p></td>
<td><p>最佳新人奖</p></td>
<td><p>《生死決斷》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>韩国电影大赏</p></td>
<td><p>最佳女配角奖</p></td>
<td><p>《美人图》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p>韩国电影青龙奖</p></td>
<td><p>最佳女配角奖</p></td>
<td><p>《美人图》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>韩国电影大钟奖</p></td>
<td><p>最佳女配角奖</p></td>
<td><p>《美人图》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>乐视盛典</p></td>
<td><p>年度电视剧最佳女演员奖</p></td>
<td><p>《回家的诱惑》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p>优酷影视指数盛典</p></td>
<td><p>开年人气偶像奖</p></td>
<td><p>《回家的诱惑》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>光影星播客母亲节关爱女性健康公益晚会</p></td>
<td><p>“慈善明星”证书</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p>安徽卫视亚洲偶像盛典</p></td>
<td><p>亚洲偶像最受欢迎海外演员奖</p></td>
<td><p>《回家的诱惑》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>央视中国电视剧年度明星盛典</p></td>
<td><p>最佳国际合作奖</p></td>
<td><p>《木府风云》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/大田电视剧节.md" title="wikilink">大田电视剧节</a></p></td>
<td><p>成就奖</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>第8屆<a href="../Page/首爾國際电视节.md" title="wikilink">首爾國際电视节</a></p></td>
<td><p>最佳女演員奖</p></td>
<td><p>《木府风云》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>第10屆<a href="../Page/首爾國際电视节.md" title="wikilink">首爾國際电视节</a></p></td>
<td><p>mango TV 人氣奖</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 單曲

| 年度    | 曲名    | 備註             |
| ----- | ----- | -------------- |
| 2015年 | 請全部留下 | 《戀上黑天使》主題曲（女版） |

## 廣告代言

  - 2012年至今，伊貝詩代言人
  - 2015年，韓國Coex Mall代言人

## 公益活動

  - 2009年 乐行天下慈善义卖
  - 2011年 粉红的康乃馨: 关爱女性健康公益晚会

## 頒獎活動/典禮

  - 2008年12月4日，擔任[MBC第](../Page/文化廣播_\(韓國\).md "wikilink")7届最佳女新人獎頒獎人
  - 2009年11月6日，擔任第46届韩国电影[大鐘奖最佳女配角頒獎人](../Page/大鐘奖.md "wikilink")
  - 2015年9月10日，擔任第10届[首尔国际电视节最优秀长篇电视剧作品奖](../Page/首尔国际电视节.md "wikilink")、最优秀迷你剧作品奖和最优秀短片作品奖頒獎人
  - 2015年10月10日，擔任第20届[釜山国际电影节闭幕式主持人](../Page/釜山国际电影节.md "wikilink")
    （与朴星雄共同主持）
  - 2015年12月2日，擔任[Mnet亞洲音樂大獎](../Page/Mnet亞洲音樂大獎.md "wikilink")（MAMA）最佳男子團體和最佳女子團體頒獎人
  - 2017年12月31日，擔任SBS演藝大賞主持人(與全炫茂及李尚敏共同主持)

## 參考資料

## 外部連結

  -
  -
  -
  -
  -
  -
[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[Category:韓國電影演員](../Category/韓國電影演員.md "wikilink")
[Category:大鐘獎獲獎者](../Category/大鐘獎獲獎者.md "wikilink")
[Category:檀國大學校友](../Category/檀國大學校友.md "wikilink")
[Category:在中國的韓國人](../Category/在中國的韓國人.md "wikilink")
[Category:大邱廣域市出身人物](../Category/大邱廣域市出身人物.md "wikilink")
[Category:秋姓](../Category/秋姓.md "wikilink")

1.  演藝 – 韓國Daum 영화
2.
3.
4.
5.
6.
7.
8.  [1996-09-21 박봉곤 가출사건 , The adventure of Mrs.
    Park](http://movie.daum.net/moviedetail/moviedetailMain.do?movieId=114)
9.  ；[2014-02-21
    archive02](https://archive.is/moviedaumnet/movieperson/Summary.do?personId=18925)
10. [2006-04-26 사생결단 死生決斷 Bloody
    Tie](http://movie.daum.net/moviedetail/moviedetailMain.do?movieId=41275)
11.
12.
13. [2009-03-19
    실종](http://movie.daum.net/moviedetail/moviedetailMain.do?movieId=45801)
14. [2011-03-17 환상극장 ,Fantastic
    Theater](http://movie.daum.net/moviedetail/moviedetailMain.do?movieId=55037)
15. [허기 (2010) ,The
    Famished](http://movie.daum.net/moviedetail/moviedetailMain.do?movieId=56753)
16. [2010-10-21 참을 수
    없는](http://movie.daum.net/moviedetail/moviedetailMain.do?movieId=52667)
17.