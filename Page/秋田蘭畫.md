[Naotake1.jpg](https://zh.wikipedia.org/wiki/File:Naotake1.jpg "fig:Naotake1.jpg")藏\]\]

**秋田蘭畫**，是指[日本](../Page/日本.md "wikilink")[江戶時代的](../Page/江戶時代.md "wikilink")[繪畫](../Page/繪畫.md "wikilink")[類型之一](../Page/類型.md "wikilink")。此派主要的畫家是[久保田藩](../Page/久保田藩.md "wikilink")（也稱秋田藩）的藩主及藩士，也被稱為**秋田派**。此派畫作揉合了西洋畫的構圖技法及日本傳統的畫材，是一種日西合壁的繪畫。

此派的誕生是在[安永年間](../Page/安永_\(年號\).md "wikilink")（1772年－1781年）於久保田藩成立，但由於無後繼者，在[天明年間](../Page/天明.md "wikilink")（1781年－1789年）即已廢亡。但是，這個畫派極端的[透視法對後世的](../Page/透視.md "wikilink")[浮世繪有很大的影響](../Page/浮世繪.md "wikilink")。代表性的畫家有[小田野直武](../Page/小田野直武.md "wikilink")（1750年－1780年）、藩主[佐竹曙山](../Page/佐竹曙山.md "wikilink")（1748年－1785年）及[佐竹義躬](../Page/佐竹義躬.md "wikilink")（1749年－1800年）。而將秋田蘭畫發揚光大的推手是與小田野直武同鄉的[日本畫家](../Page/日本畫.md "wikilink")[平福百穗](../Page/平福百穗.md "wikilink")。他在1930年（[昭和](../Page/昭和.md "wikilink")5年），在名為《日本洋畫曙光》（）的大型版畫集中介紹了秋田蘭畫。此外，由於百穗同時也是[阿羅羅木派的歌人](../Page/阿羅羅木派.md "wikilink")，他也做了兩首[短歌來歌詠秋田蘭畫](../Page/短歌.md "wikilink")。

## 參考文獻

  - 《》，武塙林太郎，[秋田魁新報社](../Page/秋田魁新報社.md "wikilink")，1989年7月，ISBN
    4-87-020070-8
  - 《》，《日本美術館》叢書，，[小學館](../Page/小學館.md "wikilink")，1997年10月，ISBN
    4-09-699701-3
  - 〈〉，成澤勝嗣，[秋田市立千秋美術館](../Page/秋田市立千秋美術館.md "wikilink")『』，2007年9月
  - 〈〉，，秋田市立千秋美術館『』，2007年9月
  - 《》，太田桃介，《秋田大百科事典》，秋田魁新報社編，1981年9月，ISBN 4-87-020007-4
  - 《》，[高階秀爾監修](../Page/高階秀爾.md "wikilink")，武塙林太郎、[養老孟司](../Page/養老孟司.md "wikilink")、[芳賀徹著](../Page/芳賀徹.md "wikilink")，筑摩書房，1996年12月，ISBN
    4-48-085729-X

[Category:日本艺术](../Category/日本艺术.md "wikilink")
[Category:江戶時代文化](../Category/江戶時代文化.md "wikilink")
[Category:秋田縣歷史](../Category/秋田縣歷史.md "wikilink")
[Category:久保田藩](../Category/久保田藩.md "wikilink")
[Category:秋田縣文化](../Category/秋田縣文化.md "wikilink")