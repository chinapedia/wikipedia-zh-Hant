**秋葵**（[学名](../Page/学名.md "wikilink")：**）亦称**黄秋葵**、**咖啡黃葵**、**补肾草**，其果实常被称为**羊角豆**、**潺茄**。性喜温暖，原产地为[非洲西部](../Page/非洲西部.md "wikilink")、[埃塞俄比亚附近以及亚洲热带](../Page/埃塞俄比亚.md "wikilink")。

## 形态

[Ladies'_Finger_BNC.jpg](https://zh.wikipedia.org/wiki/File:Ladies'_Finger_BNC.jpg "fig:Ladies'_Finger_BNC.jpg")
[OkraLeafFruitFlower-GomboFeuilleFruitFleur.jpg](https://zh.wikipedia.org/wiki/File:OkraLeafFruitFlower-GomboFeuilleFruitFleur.jpg "fig:OkraLeafFruitFlower-GomboFeuilleFruitFleur.jpg")
秋葵为一年或多年生[草本植物](../Page/草本植物.md "wikilink")，能长到2米高。叶10到20厘米长，掌形分裂，有5到7个[裂片](../Page/裂片.md "wikilink")，有硬毛。花直径4到8厘米，花瓣由白到黄，花瓣根部有红色或紫色斑点，生在主枝叶腋间。结长形[蒴果](../Page/蒴果.md "wikilink")，先端尖，五角或六角形，有毛，内含多颗[种子](../Page/种子.md "wikilink")。

[Red_and_green_okra.jpg](https://zh.wikipedia.org/wiki/File:Red_and_green_okra.jpg "fig:Red_and_green_okra.jpg")\]\]

## 参考文献

  - 《台灣蔬果實用百科第一輯》，薛聰賢 著，薛聰賢出版社，2001年

## 外部連結

  - [黄秋葵,
    huangqiukui](http://www.huangqiukui.com/huangqiukui/huangqiukui9.html)
    黄秋葵栽培技术

[Category:果菜類](../Category/果菜類.md "wikilink")
[esculentus](../Category/秋葵属.md "wikilink")