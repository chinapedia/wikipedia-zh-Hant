**科佩尔**（[斯洛文尼亚语](../Page/斯洛文尼亚语.md "wikilink")：**Koper**；[意大利语](../Page/意大利语.md "wikilink")：**Capodistria**），位于[斯洛文尼亚西南部与](../Page/斯洛文尼亚.md "wikilink")[意大利的边境地区](../Page/意大利.md "wikilink")，[亚得里亚海北端](../Page/亚得里亚海.md "wikilink")，是斯洛文尼亚全国唯一的商业港口。该市全境约311平方公里，人口47539人，斯洛文尼亚语和意大利语同为该市官方语言。

科佩尔早在[希腊化时代便已建城](../Page/希腊化时代.md "wikilink")，[希腊语称之为Aegida](../Page/希腊语.md "wikilink")，其[拉丁语名称Capris更为流行](../Page/拉丁语.md "wikilink")，今日Koper一词便是Capris的变音。568年，附近[罗马帝国城市](../Page/罗马帝国.md "wikilink")[的里雅斯特的大量市民为躲避](../Page/的里雅斯特.md "wikilink")[伦巴德人入侵而逃来该市](../Page/伦巴德人.md "wikilink")，使得该市开始繁荣。当时，该市处于[东罗马帝国治下](../Page/东罗马帝国.md "wikilink")，其名字也为纪念[查士丁尼二世而改为](../Page/查士丁尼二世.md "wikilink")“查士丁诺波尔”。不久以后，该地被并入[伦巴底王国](../Page/倫巴底王國.md "wikilink")，后又随着伦巴底被兼并入[法兰克王国](../Page/法兰克王国.md "wikilink")。

932年，科佩尔开始与[威尼斯建立起稳定的贸易关系](../Page/威尼斯.md "wikilink")。虽然如此，当威尼斯和[神圣罗马帝国发生冲突时](../Page/神圣罗马帝国.md "wikilink")，科佩尔却站在帝国一边。因此，[康拉德二世于](../Page/康拉德二世.md "wikilink")1035年授予科佩尔自治市镇的地位。但是，1232年，科佩尔被划入了[阿奎莱拉大主教区](../Page/阿奎萊亞.md "wikilink")，1278年随之并入威尼斯共和国，被更名为“Caput
Histriae”，意为“[伊斯特利亚之都](../Page/伊斯特利亚.md "wikilink")”，今日Capodistria一词便来自于此。

[哈布斯堡家族夺得神圣罗马帝国皇位后](../Page/哈布斯堡家族.md "wikilink")，科佩尔作为的里雅斯特的一部分，被划成[奥地利的领地](../Page/奥地利.md "wikilink")。[一战之后](../Page/一战.md "wikilink")，被奥地利割让给意大利。[二战之后](../Page/二战.md "wikilink")，科佩尔作为[第里雅斯特自由區的B区一部分](../Page/第里雅斯特自由區.md "wikilink")，1954年被正式划入[南斯拉夫](../Page/南斯拉夫.md "wikilink")，其意大利裔居民多半迁走。

1991年，随着斯洛文尼亚的独立，科佩尔成为其唯一的商业港口。

## 姐妹城市

  - [薩馬拉](../Page/薩馬拉.md "wikilink")

## 参考资料

[Category:斯洛文尼亚城市](../Category/斯洛文尼亚城市.md "wikilink")
[Category:地中海港口](../Category/地中海港口.md "wikilink")