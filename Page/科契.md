**科契**\[1\]（**Kochi**；旧名**Cochin**，译作**科钦**）是[印度](../Page/印度.md "wikilink")[喀拉拉邦最大的城市和主要的港口](../Page/喀拉拉邦.md "wikilink")。“科契”一名来自[马拉雅拉姆语](../Page/马拉雅拉姆语.md "wikilink")“Kochazhi”，意指小湖泊。[葡萄牙人取名](../Page/葡萄牙.md "wikilink")**科欣**（Cochin），1996年改回古名**科契**（Kochi）。

科契素有“阿拉伯海的王后”之称，位于[喀拉拉邦的](../Page/喀拉拉邦.md "wikilink")[埃尔讷古勒姆县](../Page/埃尔讷古勒姆县.md "wikilink")，在[锡鲁万纳塔普拉姆城北约](../Page/锡鲁万纳塔普拉姆.md "wikilink")220公里。昔日的科契曾包括、[科契堡](../Page/科契堡.md "wikilink")、等岛群。今日的科契包括埃尔讷古勒姆市、科契古城、贡巴兰吉和外围岛屿。科契以其天然港闻名，曾经是几个世纪中印度[香料贸易的中心](../Page/香料.md "wikilink")。科契现有一个[海军基地和一个](../Page/海军基地.md "wikilink")[机场可通印度各大城市](../Page/机场.md "wikilink")。威灵东岛是个大型人工岛，横跨岛上有一大型[造船厂](../Page/造船厂.md "wikilink")，生产[印度海军使用的](../Page/印度海军.md "wikilink")[巡洋舰和](../Page/巡洋舰.md "wikilink")[航空母舰](../Page/航空母舰.md "wikilink")。

[国际胡椒交易所设在科契](../Page/国际胡椒交易所.md "wikilink")。

## 交通

  - 科契有公路、铁路、水路和空路与印度及世界各地相通。
  - [科钦国际机场离城](../Page/科钦国际机场.md "wikilink")25公里。
  - 科契近郊有两个火车站（和），可通印度各地。

## 人口

，来自印度各地区，[马拉雅拉姆语为通用语言](../Page/马拉雅拉姆语.md "wikilink")，英语是商业语言。

## 经济

科契的经济除[造船业和](../Page/造船业.md "wikilink")[渔业外](../Page/渔业.md "wikilink")，以服务性行业为主，主要的服务性行业包括黄金零售业、服装零售业、海产、香料出口业、信息技术业、旅游业、医疗等。

## 历史和遗迹

[明朝](../Page/明朝.md "wikilink")[永乐年间](../Page/永乐.md "wikilink")[郑和下西洋](../Page/郑和下西洋.md "wikilink")，曾多次访问**科契**，先后随[郑和出访的](../Page/郑和.md "wikilink")[马欢](../Page/马欢.md "wikilink")、[费信](../Page/费信.md "wikilink")、[巩珍三人都将见闻纪录著书](../Page/巩珍.md "wikilink")，他们分别撰写的《[瀛涯胜览](../Page/瀛涯胜览.md "wikilink")》、《[星槎胜览](../Page/星槎胜览.md "wikilink")》、《[西洋番国志](../Page/西洋番国志.md "wikilink")》各有专篇纪录科契国。

  - 科契国王[可也里信佛教](../Page/可也里.md "wikilink")。
  - 科契出产[胡椒](../Page/胡椒.md "wikilink")、[米](../Page/米.md "wikilink")、[豆](../Page/豆.md "wikilink")、牛、羊鸡、鸭等；不出产小麦、驴、鹅。
  - 科契国的富商称为“哲地”，哲地收买本地土产的胡椒交换从[阿拉伯商人贩卖的珍珠](../Page/阿拉伯.md "wikilink")、珊瑚和香料，等候[郑和宝船或别国船只到来交易](../Page/郑和.md "wikilink")。
  - [永乐三年国王](../Page/永乐.md "wikilink")[可亦里遣使](../Page/可亦里.md "wikilink")[完者塔儿朝贡](../Page/完者塔儿.md "wikilink")。
  - 永乐十年国王可亦里遣使请封国内大山。
  - 永乐十四年，[明成祖诏封](../Page/明成祖.md "wikilink")[可亦里为科契国国王](../Page/可亦里.md "wikilink")，诏封科契国内东大山为“镇国山”，并赐科契御制碑。《科契御制碑文》：“截彼高山，作镇海邦。吐烟出云，为下国洪庞。时其雨旸，肃其烦熇，作彼丰穰，祛彼氛妖。庇于斯民，靡灾靡涔。室家胥庆，优游卒岁。山之崭矣，海之深矣。勒此铭诗，相为终始。”

1503年至1663年科契先被[葡萄牙统治](../Page/葡萄牙.md "wikilink")，随后是[荷兰人](../Page/荷兰.md "wikilink")。

在中科契割让给[英国以交换](../Page/英国.md "wikilink")[邦加島](../Page/邦加島.md "wikilink")。

麻坦切里半岛上的科契堡是科契古城，有许多历史古迹，包括[中国渔网](../Page/中国渔网.md "wikilink")、[麻坦切里宫和](../Page/麻坦切里宫.md "wikilink")[圣十字圣殿主教座堂](../Page/圣十字圣殿主教座堂_\(科契\).md "wikilink")。
[KochiFishingNet.jpg](https://zh.wikipedia.org/wiki/File:KochiFishingNet.jpg "fig:KochiFishingNet.jpg")

## 参考文獻

  - （明）[马欢原著](../Page/马欢.md "wikilink") 万明校注 明钞本
    《[瀛涯胜览](../Page/瀛涯胜览.md "wikilink")》校注 海洋出版社 2005
    ISBN 978-7-5027-6378-7
  - （明）[巩珍著](../Page/巩珍.md "wikilink")《[西洋番国志](../Page/西洋番国志.md "wikilink")》，中华书局向达校注，ISBN
    978-7-101-02025-0
  - （明）[黄省曾著](../Page/黄省曾.md "wikilink")
    《[西洋朝贡典录校注](../Page/西洋朝贡典录.md "wikilink")》中华书局
    ISBN 978-7-101-02029-8

## 參考資料

[科契](../Category/科契.md "wikilink")
[Category:喀拉拉邦城镇](../Category/喀拉拉邦城镇.md "wikilink")
[Category:阿拉伯海沿海城市](../Category/阿拉伯海沿海城市.md "wikilink")
[Category:印度港口](../Category/印度港口.md "wikilink")
[Category:中外交通史地名](../Category/中外交通史地名.md "wikilink")

1.  [國家教育研究院](http://terms.naer.edu.tw/detail/2999474/)