**科学引文索引**（、[缩写](../Page/缩写.md "wikilink")：）是由[美国](../Page/美国.md "wikilink")[科学資訊研究所](../Page/科学資訊研究所.md "wikilink")（Institute
for Scientific
Information，简称ISI）于1960年上线投入使用的一部期刊[文献检索工具](../Page/文献检索.md "wikilink")，其出版形式包括印刷版期刊和光盘版及联机数据库。科学引文索引由[科睿唯安公司](../Page/科睿唯安.md "wikilink")（Clarivate
Analytics）运营。

## 影响

科学引文索引以[布拉德福](../Page/布拉德福.md "wikilink")（S. C.
Bradford）[文献离散律理论](../Page/文献离散律理论.md "wikilink")、以加菲尔德（[Eugene
Garfield](../Page/Eugene_Garfield.md "wikilink")）[引文分析理论为主要基础](../Page/引文分析理论.md "wikilink")，通过论文的被引用频次等的统计，对学术期刊和科研成果进行多方位的评价研究，从而评判一个国家或地区、科研单位、个人的科研产出绩效，来反映其在国际上的学术水平。因此，SCI是目前国际上被公认的**最具权威**的[科技文献检索工具](../Page/科技文献检索.md "wikilink")。

科学引文索引以其独特的引证途径和综合全面的科学数据，通过大量的引文进行统计，然后得出某期刊某论文在某学科内的[影响因子](../Page/影响因子.md "wikilink")、被引频次、[即时指数等量化指标来对期刊](../Page/即时指数.md "wikilink")、论文等进行排行，被引频次高,说明该论文在它所研究的领域里产生了巨大的影响、被国际同行重视、学术水平高。由于SCI收录的论文主要是[自然科学的基础研究领域](../Page/自然科学.md "wikilink")，所以SCI指标主要适用于评价基础研究的成果。而基础研究的主要成果的表现形式是学术论文，所以，如何评价基础研究成果也就常常简化为如何评价论文所承载的内容对科学知识进展的影响。

科学引文索引是当今世界上最著名的检索性刊物之一，也是[文献计量学和](../Page/文献计量学.md "wikilink")[科学计量学的重要工具](../Page/科学计量学.md "wikilink")。通过引文检索功能可查找相关研究课题早期、当时和最近的学术文献，同时获取论文摘要；可以看到所引用参考文献的记录、被引用情况及相关文献的记录。

## SCI报道内容

  - [农业](../Page/农业.md "wikilink")
  - [生物学](../Page/生物学.md "wikilink")
  - [环境科学](../Page/环境科学.md "wikilink")
  - [工程技术](../Page/工程技术.md "wikilink")
  - [应用科学](../Page/应用科学.md "wikilink")
  - [医学与](../Page/医学.md "wikilink")[生命科学](../Page/生命科学.md "wikilink")
  - [物理学](../Page/物理学.md "wikilink")
  - [化学](../Page/化学.md "wikilink")
  - [经济学](../Page/经济学.md "wikilink")

## 参见

  - [社会科学引文索引](../Page/社会科学引文索引.md "wikilink")(SSCI)，创始于1956年含括1700种期刊。
  - [艺术人文引文索引](../Page/艺术人文引文索引.md "wikilink")(AHCI), 创始于1975年含括1150种期刊。
  - [工程索引](../Page/工程索引.md "wikilink")(EI)。
  - [影响因子](../Page/影响因子.md "wikilink")

## 延伸閱讀

  -
  -
  -
  -
  -
## 外部链接

  - [加菲爾德表示:不能以SCI論文數量評價科學水平](http://scitech.people.com.cn/BIG5/10052660.html)
  - [International Science Index](http://www.waset.org/Publications/)
  - [Introduction to SCI](http://scientific.thomson.com/products/wos/)
  - [Master journal list](http://science.thomsonreuters.com/mjl/)
  - [Chemical Information Sources/ Author and Citation
    Searches](https://en.wikibooks.org/wiki/Chemical_Information_Sources/Author_and_Citation_Searches).
    on WikiBooks.
  - [Cited Reference Searching: An
    Introduction](http://scientific.thomson.com/tutorials/citedreference/crs1.htm).
    Thomson Reuters.
  - [Chemistry Citation
    Index](http://www.chinweb.com/cgi-bin/chemport/getfiler.cgi?ID=k4l7vyYF5FimYvScsOm3pxWVmEhBoH0ZuYgxjLdKBfqdmURDHLrjuVv78i16JLPX&VER=E).
    Chinweb.

[Category:文献学](../Category/文献学.md "wikilink")
[Category:文献检索数据库](../Category/文献检索数据库.md "wikilink")
[Category:线上数据库](../Category/线上数据库.md "wikilink")
[Category:湯森路透](../Category/湯森路透.md "wikilink")