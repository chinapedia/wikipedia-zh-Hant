**科尔沁右翼中旗**（）位于[内蒙古自治区东北部](../Page/内蒙古自治区.md "wikilink")，地处[大兴安岭南麓](../Page/大兴安岭.md "wikilink")[科尔沁草原腹地](../Page/科尔沁草原.md "wikilink")，是[中華人民共和國](../Page/中華人民共和國.md "wikilink")[內蒙古自治區](../Page/內蒙古自治區.md "wikilink")[興安盟轄下的一個](../Page/興安盟.md "wikilink")[旗](../Page/旗.md "wikilink")。全旗辖16个[苏木镇](../Page/苏木_\(行政区划\).md "wikilink")、工作部，面積為15613平方公里，人口25.1萬，其中，[蒙古族人口占总人口的](../Page/蒙古族.md "wikilink")84％，是全区乃至全国[蒙古族人口比例最高的](../Page/蒙古族.md "wikilink")[少数民族聚居旗](../Page/中国少数民族.md "wikilink")\[1\]。該旗政府駐於白音胡碩鎮，郵政編碼029400\[2\]。

## 簡介

科尔沁右翼中旗，簡稱科右中旗。該旗[畜牧業極為發達](../Page/畜牧業.md "wikilink")。

## 行政区划

\[3\] 。

## 参考文献

{{-}}

[科尔沁右翼中旗](../Category/科尔沁右翼中旗.md "wikilink")
[兴安](../Category/内蒙古自治区的旗.md "wikilink")
[旗](../Category/兴安县级行政区.md "wikilink")

1.
2.
3.