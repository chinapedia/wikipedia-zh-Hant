**科布雷罗阿体育俱乐部**（**Club de Deportes
Cobreloa**）是[智利足球俱乐部](../Page/智利.md "wikilink")，俱乐部来自[阿塔卡马沙漠中的矿业城市](../Page/阿塔卡马沙漠.md "wikilink")[卡拉马](../Page/卡拉马.md "wikilink")，球队是智利最富有竞争力的俱乐部之一。

不像其他的大球会的悠久历史，科布雷罗阿建立于1977年1月7日。俱乐部从其自身的建立到进入智利顶级足球联赛只用了很短的时间，球队建立的第二年就取得了晋级甲级联赛的资格直到现今。

可能没有其他俱乐部的历史像科布雷罗阿一样在国际赛场上如此迅速的创造成绩。1981年球队建队四年后就打入了[南美解放者杯的决赛](../Page/南美解放者杯.md "wikilink")，但是决赛中输给了[巴西的](../Page/巴西.md "wikilink")[弗拉门戈](../Page/弗拉门戈体育俱乐部.md "wikilink")。科布雷罗阿在之后一年的南美解放者杯中再次闯入决赛，可是这次在决赛中再次失利，输给了[乌拉圭球会](../Page/乌拉圭.md "wikilink")[佩纳罗尔](../Page/佩纳罗尔竞技俱乐部.md "wikilink")。科布雷罗阿在1987年也闯入了解放者杯的半决赛。

球队的主场球衣色为全橙色。

俱乐部名字科布雷罗阿(Cobreloa)来自于球队所在的地区，那就是世界上最大的铜矿产地洛阿省(El
Loa)，而科布雷(Cobre)是[西班牙语铜的意思](../Page/西班牙语.md "wikilink")，两词组合就形成了「科布雷罗阿」这个名字。

## 球队荣誉

### 国内荣誉

  - **[智利足球甲级联赛](../Page/智利足球甲级联赛.md "wikilink")**
      - **冠军 (8次):** 1980, 1982, 1985, 1988, 1992, 2003 (春季), 2003 (秋季),
        2004 (秋季)
  - **[智利杯赛](../Page/智利杯赛.md "wikilink")**
      - **冠军 (1次)**: 1986年

### 国际荣誉

  - **[南美解放者杯](../Page/南美解放者杯.md "wikilink")**
      - **亚军 (2次):** 1981年, 1982年

## 知名球员

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/Armando_Alarcon.md" title="wikilink">Armando Alarcon</a></p></li>
<li><p><a href="../Page/Julio_César_Baldivieso.md" title="wikilink">Julio César Baldivieso</a></p></li>
<li><p><a href="../Page/Juan_Covarrubias.md" title="wikilink">Juan Covarrubias</a></p></li>
<li><p><a href="../Page/Enzo_Escobar.md" title="wikilink">Enzo Escobar</a></p></li>
<li><p><a href="../Page/Patricio_Galaz.md" title="wikilink">Patricio Galaz</a></p></li>
<li><p><a href="../Page/Eduardo_Gomez.md" title="wikilink">Eduardo Gomez</a></p></li>
<li><p><a href="../Page/Rubén_Gómez.md" title="wikilink">Rubén Gómez</a></p></li>
<li><p><a href="../Page/Nicolas_Hernández.md" title="wikilink">Nicolas Hernández</a></p></li>
<li><p><a href="../Page/Juan_Carlos_Letelier.md" title="wikilink">Juan Carlos Letelier</a></p></li>
<li><p><a href="../Page/Victor_Merello.md" title="wikilink">Victor Merello</a></p></li>
<li><p><a href="../Page/Washington_Olivera.md" title="wikilink">Washington Olivera</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/Hector_Puebla.md" title="wikilink">Hector Puebla</a></p></li>
<li><p><a href="../Page/阿歷斯·山齊士.md" title="wikilink">阿歷斯·山齊士</a></p></li>
<li><p><a href="../Page/Jorge_Luis_Siviero.md" title="wikilink">Jorge Luis Siviero</a></p></li>
<li><p><a href="../Page/Mario_Benavides_Soto.md" title="wikilink">Mario Soto</a></p></li>
<li><p><a href="../Page/Hugo_Tabilo.md" title="wikilink">Hugo Tabilo</a></p></li>
<li><p><a href="../Page/Nelson_Tapia.md" title="wikilink">Nelson Tapia</a></p></li>
<li><p><a href="../Page/Carlos_Tejas.md" title="wikilink">Carlos Tejas</a></p></li>
<li><p><a href="../Page/Marcelo_Trobbiani.md" title="wikilink">Marcelo Trobbiani</a></p></li>
<li><p><a href="../Page/Dario_Verón.md" title="wikilink">Dario Verón</a></p></li>
<li><p><a href="../Page/Osvaldo_Canobbio.md" title="wikilink">Osvaldo Canobbio</a></p></li>
<li><p><a href="../Page/Marco_Antonio_Figueroa.md" title="wikilink">Marco Antonio Figueroa</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 官方赞助

[129 px](https://zh.wikipedia.org/wiki/File:KelmeLogo.PNG "fig:129 px")

  - [卡尔美-Kelme](../Page/:en:Kelme_\(company\).md "wikilink")

## 外部链接

  - [Official site](http://www.cdcobreloa.cl)

[Category:智利足球俱乐部](../Category/智利足球俱乐部.md "wikilink")
[Category:1977年建立的足球俱樂部](../Category/1977年建立的足球俱樂部.md "wikilink")