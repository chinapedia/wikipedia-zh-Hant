[Kotor-Position.PNG](https://zh.wikipedia.org/wiki/File:Kotor-Position.PNG "fig:Kotor-Position.PNG")
**科托爾**
（；）是[黑山共和國的一個海岸城市](../Page/黑山共和國.md "wikilink")，位於[科托爾灣最深入之處](../Page/科托爾灣.md "wikilink")，是**科托爾區**
（面積335平方公里）首府。2011年人口961人 （區人口22,601人）。

1900年時[天主教徒仍然佔當地人口多數](../Page/天主教.md "wikilink")
，今日多數居民信奉[東正教](../Page/東正教.md "wikilink")。[天主教科托爾教區仍以此為其駐地](../Page/天主教科托爾教區.md "wikilink")。

是黑山著名的旅遊勝地。[科托尔港自然与历史文化区是](../Page/科托尔港自然与历史文化区.md "wikilink")[世界遺產](../Page/世界遺產.md "wikilink")。


[K](../Category/黑山城市.md "wikilink")
[Category:地中海沿海城市](../Category/地中海沿海城市.md "wikilink")
[Category:黑山世界遺產](../Category/黑山世界遺產.md "wikilink")