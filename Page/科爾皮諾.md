[Spb_kolpinsky.svg](https://zh.wikipedia.org/wiki/File:Spb_kolpinsky.svg "fig:Spb_kolpinsky.svg")
[Troitsky-sobor_Izhora_river_and_Bridge.jpg](https://zh.wikipedia.org/wiki/File:Troitsky-sobor_Izhora_river_and_Bridge.jpg "fig:Troitsky-sobor_Izhora_river_and_Bridge.jpg")，[伊佐拉河](../Page/伊佐拉河.md "wikilink")，和河上的橋樑\]\]
[Coat_of_Arms_of_Kolpino_1999.png](https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Kolpino_1999.png "fig:Coat_of_Arms_of_Kolpino_1999.png")
[Flag_of_Kolpino_(St_Petersburg).png](https://zh.wikipedia.org/wiki/File:Flag_of_Kolpino_\(St_Petersburg\).png "fig:Flag_of_Kolpino_(St_Petersburg).png")
[Coat_of_Arms_of_Kolpino_(St_Petersburg).png](https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Kolpino_\(St_Petersburg\).png "fig:Coat_of_Arms_of_Kolpino_(St_Petersburg).png")
**科爾皮諾**（，）是[俄羅斯](../Page/俄羅斯.md "wikilink")[聖彼得堡直轄市](../Page/聖彼得堡.md "wikilink")[科爾皮諾區下轄的一座城市](../Page/科爾皮諾區.md "wikilink")，位於市中心東南的[伊佐拉河](../Page/伊佐拉河.md "wikilink")（[涅瓦河的支流](../Page/涅瓦河.md "wikilink")）畔，東南距聖彼得堡26km。2010年人口138,013人；2002年136,632人；1972年81,000人；1897年8,076人。

科爾皮諾建於1722年，並于1912年設市。傳統上是一個鐵工業城市，曾是主要的俄羅斯皇室所屬的[鋼鐵廠之一](../Page/鋼鐵廠.md "wikilink")。亦曾為[俄羅斯海軍的](../Page/俄羅斯海軍.md "wikilink")[鑄鐵廠](../Page/鑄鐵廠.md "wikilink")。今有[伊佐拉造船廠](../Page/伊佐拉造船廠.md "wikilink")，本市很多人工作於此。每年的5月22日均會有大量的朝聖者前往本市的[三一大教堂朝覲](../Page/三一大教堂.md "wikilink")[聖尼古拉的聖像](../Page/聖尼古拉.md "wikilink")。

隨著[衛國戰爭的開始科爾皮諾市民們組成了民兵的一部份](../Page/衛國戰爭.md "wikilink")，1941年8月24日至9月4日之間，工人們組成了伊佐拉營。因前線就在科爾皮諾附近，該城便暴露在敵軍炮火之下（至1944年，科爾皮諾的房屋從2183座減少至327座）。科爾皮諾附近地區及其主幹道遭受了140,939發子彈及436枚空投彈的火力。據不完全統計，戰爭中因炮彈或飢餓而喪生的人有4,600人，且不計陣亡于前線者。至1944年1月1日科爾皮諾僅剩下2196名居民。隨著戰爭的結束，撤離及參軍者漸漸返回。至1945年1月1日，人口達7404人，下一年則達到了8914人。

在一幢新住宅樓的建設中，科爾皮諾發現了大量墓葬，共埋有888名紅軍官兵，他們殁于1941年。該墓葬位於Very Slutskoy街道上。

## 姐妹城市

  - [芬蘭](../Page/芬蘭.md "wikilink")[勞馬](../Page/勞馬.md "wikilink")

  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")[淮南市](../Page/淮南市.md "wikilink")

## 參考資料

  -
## 外部連結

  - [科爾皮諾政府官網](http://kolpino-city.ru/)

[К](../Category/聖彼得堡下轄市鎮.md "wikilink")