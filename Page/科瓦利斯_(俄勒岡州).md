**科瓦利斯**（
）是[美国](../Page/美国.md "wikilink")[俄勒冈州中西部的一个小型城市](../Page/俄勒冈州.md "wikilink")，為[本頓縣的縣治](../Page/本頓縣_\(俄勒岡州\).md "wikilink")，中文又称**谷心镇**。因[威拉米特河的支流](../Page/威拉米特河.md "wikilink")[玛丽河流经此地而被称为](../Page/玛丽河.md "wikilink")**玛丽威尔**（**Marysville**），1853年后才改称科瓦利斯（来自[拉丁语](../Page/拉丁语.md "wikilink")*cor
vallis*，意为山谷之心）。

該城市在[2010年美国人口普查显示科瓦利斯现有](../Page/2010年美国人口普查.md "wikilink")[人口约](../Page/人口.md "wikilink")54462。[俄勒冈州立大学主校區坐落于科瓦利斯](../Page/俄勒冈州立大学.md "wikilink")。[惠普公司的一個主要](../Page/惠普公司.md "wikilink")[打印机研发中心以及消耗品生产工厂也位于本城](../Page/打印机.md "wikilink")。

## 地理方位

科瓦利斯位于[北纬](../Page/北纬.md "wikilink")44°34'15"[西经](../Page/西经.md "wikilink")123°16'34"(44.570780,
-123.275998)，平均[海拔](../Page/海拔.md "wikilink")72[米](../Page/米.md "wikilink")。

## 参考文献

[C](../Category/俄勒冈州城市.md "wikilink")