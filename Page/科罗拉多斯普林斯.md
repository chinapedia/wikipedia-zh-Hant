**科羅拉多泉**（）是[美国](../Page/美国.md "wikilink")[科羅拉多州的第二大](../Page/科羅拉多州.md "wikilink")、美國第49大城市，也是[艾爾帕索縣的首府](../Page/艾爾帕索縣_\(科羅拉多州\).md "wikilink")。根據[美國人口普查局](../Page/美國人口普查局.md "wikilink")2005年的估計，科羅拉多泉市约有人口36萬9815人。它位于科罗拉多州最大城市[丹佛约](../Page/丹佛.md "wikilink")100公里的南方，距離科羅拉多州地理中心東方約101[公里](../Page/公里.md "wikilink")（63[英里](../Page/英里.md "wikilink")）。城市平均海拔超過1[英哩許多](../Page/英哩.md "wikilink")，為1839[公尺](../Page/公尺.md "wikilink")（6035[英呎](../Page/英呎.md "wikilink")），也是一個「哩高城」。整個城市位於著名的[派克斯峰山腳](../Page/派克斯峰.md "wikilink")，[洛磯山脈的東部邊緣](../Page/洛磯山脈.md "wikilink")。

科羅拉多泉擁有一個溫和的冬天，1月平均最高與最低溫度分別為-10 °C/5.5 °C(14 °F/42 °F)，7月夏天則為12.7 °C/29.4 °C(55 °F/85 °F)。大量積雪的情況在市中心並不常見，10月底到3/4月之間偶爾幾天會有暴風雪與冰點以下的氣溫。科羅拉多泉是美國打雷最頻繁的地方之一，[尼古拉·特斯拉因為這個現象而在這設立他的一個實驗室](../Page/尼古拉·特斯拉.md "wikilink")。科羅拉多泉史上最高溫度為1874年6月7日記錄的38.3 °C(101 °F)，最低溫度則為1883年1月20日的-35.5 °C(-32 °F)。

科羅拉多泉附近有眾多與[空防相關的](../Page/空防.md "wikilink")[軍事設施](../Page/軍事設施.md "wikilink")，包括[彼得森空軍基地](../Page/彼得森空軍基地.md "wikilink")、[西瑞佛空軍基地](../Page/西瑞佛空軍基地.md "wikilink")、[北美空防司令部總部等](../Page/北美空防司令部.md "wikilink")。市内的主要大學有[科羅拉多學院](../Page/科羅拉多學院.md "wikilink")、[科羅拉多泉科羅拉多大學](../Page/科羅拉多泉科羅拉多大學.md "wikilink")（University
of Colorado, Colorado Springs）、與[美國空軍官校](../Page/美國空軍官校.md "wikilink")。

## 参见

  - [夏延山综合设施](../Page/夏延山综合设施.md "wikilink")

## 参考文献

## 外部链接

  - [City of Colorado Springs website](http://www.springsgov.com/)
  - [Colorado Springs Fine Arts
    Center](http://www.csfineartscenter.org/)
  - [Colorado Springs Travel Information: Colorado Springs Convention &
    Visitors Bureau](http://www.VisitCOS.com/)
  - [Early Capitol and Legislative Assembly
    Locations](http://www.colorado.gov/dpa/doit/archives/cap/locate.htm)
  - [Garden of the
    Gods](http://www.gardenofgods.com/yourvisit/special_events/index_277.cfm)

[Category:科罗拉多州城市](../Category/科罗拉多州城市.md "wikilink")
[Category:科羅拉多州縣城](../Category/科羅拉多州縣城.md "wikilink")
[Category:派克斯峰](../Category/派克斯峰.md "wikilink")
[Category:1871年建立的聚居地](../Category/1871年建立的聚居地.md "wikilink")
[Category:1871年科羅拉多領地建立](../Category/1871年科羅拉多領地建立.md "wikilink")