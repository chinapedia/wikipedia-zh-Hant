[Koch_curve.svg](https://zh.wikipedia.org/wiki/File:Koch_curve.svg "fig:Koch_curve.svg")
[KochFlake.svg](https://zh.wikipedia.org/wiki/File:KochFlake.svg "fig:KochFlake.svg")
[Von_Koch_curve.gif](https://zh.wikipedia.org/wiki/File:Von_Koch_curve.gif "fig:Von_Koch_curve.gif")

**科赫曲線**是一種[-{A](../Page/分形.md "wikilink")。其形態似[雪花](../Page/雪花.md "wikilink")，又稱**科赫雪花、雪花曲線**。其[豪斯多夫維是](../Page/豪斯多夫維.md "wikilink")\(\log 4/\log 3\)。

它最早出現在[海里格·冯·科赫的論文](../Page/海里格·冯·科赫.md "wikilink")《關於一條連續而無切線，可由初等幾何構作的曲線》（1904年，法語原題：*Sur
une courbe continue sans tangente, obtenue par une construction
géométrique élémentaire*）。

科赫曲線是[de Rham曲線的特例](../Page/de_Rham曲線.md "wikilink")。

給定線段AB，科赫曲線可以由以下步驟生成：

1.  將線段分成三等份（AC,CD,DB）
2.  以CD為底，向外（內外隨意）畫一個等邊[三角形DMC](../Page/三角形.md "wikilink")
3.  將線段CD移去
4.  分別對AC,CM,MD,DB重複1\~3。

科赫雪花是以等邊三角形三邊生成的科赫曲線組成的。科赫雪花的面積是
\(\frac{2\sqrt{3}(s^2)}{5}\)，其中\(s\)是原來三角形的邊長。每條科赫曲線的長度是無限大，它是[連續而](../Page/連續.md "wikilink")[無處可微的曲線](../Page/無處可微.md "wikilink")。

## 記錄

以[L系統](../Page/L系統.md "wikilink")：

  -
    字符 : F
    常數 : +, −
    公理 : F++F++F
    規則:
    F → F−F++F−F

<!-- end list -->

  - F ：向前
  - \- ：左轉60°
  - \+ ：右轉60°

## [Logo源碼](../Page/Logo語言.md "wikilink")

`rt 30 koch 100`.

`to koch :x`
`  repeat 3 [triline :x rt 120]`
`end`
`to triline :x`
`  if :x < 1 [fd :x] [triline :x/3 lt 60 triline :x/3 rt 120 triline :x/3 lt 60 triline :x/3]`
`end`

[Category:分形曲线](../Category/分形曲线.md "wikilink")