**科迪·莊臣**（**Frode
Johnsen**，）是一名[挪威](../Page/挪威.md "wikilink")[足球員](../Page/足球.md "wikilink")，現在於[日本職業足球聯賽球隊](../Page/日本職業足球聯賽.md "wikilink")[清水心跳中效力](../Page/清水心跳.md "wikilink")。

莊臣的球員生涯於*Skotfoss
IL*開始，但直至1999年[奧特格寧蘭收購之前都未曾在](../Page/奧特格寧蘭.md "wikilink")
[挪威足球超級聯賽](../Page/挪威足球超級聯賽.md "wikilink")
的比賽中上陣，當時莊臣已經25歲了。2000年，莊臣轉會到[洛辛堡](../Page/洛辛堡.md "wikilink")，用來代替[約翰·卡維](../Page/約翰·卡維.md "wikilink")。莊臣身高1[米](../Page/米.md "wikilink")88，所以他有著良好的頂上功夫。2005年，[皇家馬略卡和](../Page/皇家馬略卡.md "wikilink")[布拉格斯巴達都對莊臣有興趣](../Page/布拉格斯巴達足球俱樂部.md "wikilink")。

2006年7月，莊臣離開[挪威](../Page/挪威.md "wikilink")，轉到[日本加盟](../Page/日本.md "wikilink")[名古屋鯨魚](../Page/名古屋鯨魚.md "wikilink")，並簽了18個月的[合約](../Page/合約.md "wikilink")。2006年7月29日，他第一次替球隊上陣，對手是[千葉市原](../Page/千葉市原.md "wikilink")，莊臣打進了2球，助球隊以3-2險勝對手。

莊臣在成為球員之前，正在修讀作為[警察的課程](../Page/警察.md "wikilink")，可是在加盟[洛辛堡時已經放棄了](../Page/洛辛堡.md "wikilink")。

## 外部連結

  - [Player profile from
    RBKweb](http://www.rosenborg.info/player/fjohnsen)
  - [Player profile from RBKweb
    (nor)](http://www.rbkweb.no/spiller/fjohnsen)
  - [Player profile from
    Playerhistory.com](http://www.playerhistory.com/player/3427/)

[Category:挪威足球運動員](../Category/挪威足球運動員.md "wikilink")
[Category:洛辛堡球員](../Category/洛辛堡球員.md "wikilink")
[Category:名古屋鯨魚球員](../Category/名古屋鯨魚球員.md "wikilink")
[Category:清水心跳球員](../Category/清水心跳球員.md "wikilink")
[Category:日職球員](../Category/日職球員.md "wikilink")
[Category:奧特足球會球員](../Category/奧特足球會球員.md "wikilink")
[Category:挪超球員](../Category/挪超球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:日本外籍足球運動員](../Category/日本外籍足球運動員.md "wikilink")