**英國秘密情報局**（，[縮寫](../Page/縮寫.md "wikilink")：*SIS*），通稱「-{zh-hant:軍情六處;zh-hans:军情六处}-」（[英文](../Page/英文.md "wikilink")：，縮寫：*MI6*），是[英國對外的](../Page/英國.md "wikilink")[情報機構](../Page/情報機構.md "wikilink")，於1909年成立，負責在海外進行[間諜工作](../Page/間諜.md "wikilink")。自1569年[伊丽莎白一世时期英国](../Page/伊丽莎白一世.md "wikilink")[国务大臣](../Page/国务大臣.md "wikilink")[弗朗西斯·沃尔辛厄姆爵士创建](../Page/弗朗西斯·沃尔辛厄姆.md "wikilink")「英国保密局」后，曾几度变更其机构形式，直到1912年由[曼斯菲尔德·卡明海军准将组建成现在这样的机构](../Page/曼斯菲尔德·史密斯-卡明.md "wikilink")。

1930－1940年代，它被公认为世界上效能最高的情报机构。在[纳粹德国侵略歐洲時](../Page/纳粹德国.md "wikilink")，它在[欧洲](../Page/欧洲.md "wikilink")，[南美洲以及](../Page/南美洲.md "wikilink")[亚洲的部分區域從事諜報任務](../Page/亚洲.md "wikilink")（MI6的名称就是在這個時期出現的，因為當時是「英國軍事情報局第六處」）。当美国参加第二次世界大战时，MI6曾帮助[美國](../Page/美國.md "wikilink")[戰略情報局](../Page/戰略情報局.md "wikilink")（後改组為[中央情報局](../Page/中央情報局.md "wikilink")）培訓情報人员。從那時起，這兩個機構之間开始了合作關係。並與[KGB](../Page/KGB.md "wikilink")，[CIA](../Page/CIA.md "wikilink")，[摩薩德齊名世界四大情報單位](../Page/摩薩德.md "wikilink")。从1950年代中期开始，关于[苏联间谍自](../Page/苏联.md "wikilink")1930年代起滲透組織内部的醜聞敗露，引起人們震驚。

英国的新聞報導受管制透露MI6的工作情况和與該部門有關的新聞，英國政府直到1994年才正式承認其存在\[1\]。因为该局有权根据政府保密法审查与其工作相关的新闻，故其内部运作仍然相当神秘，即使是MI6的局长名字，除了有限的范围，甚至在MI6向其提供情报的[英国外交部裏也很少有人知道](../Page/英国外交部.md "wikilink")。[007系列電影則以該處為主要題材](../Page/007.md "wikilink")。

## 其他分部

隶属于或名义上由[英国外交與联-{邦}-事务部](../Page/英国外交部.md "wikilink")（外交部）使用的建筑包括：

  - [汉斯洛普公园](../Page/汉斯洛普公园.md "wikilink")（Hanslope
    Park）：郊区的[米尔顿凯恩斯](../Page/米尔顿凯恩斯足球俱乐部.md "wikilink")大楼，隶属于外交部和英国情报网络.\[2\]
  - [蒙克顿堡](../Page/蒙克顿堡.md "wikilink")（Fort
    Monckton）：[拿破仑战争时期堡垒](../Page/拿破仑战争.md "wikilink")，1880年代重建，现为SIS野外行动训练中心。\[3\]

## 英国秘密情报局历任局长（Chiefs）

  - 1909–1923: [曼斯菲尔德·史密斯-卡明爵士](../Page/曼斯菲尔德·史密斯-卡明.md "wikilink")（Sir
    Mansfield Smith-Cumming）,
  - 1923–1939: [休·辛克莱尔海军上将爵士](../Page/休·辛克莱尔.md "wikilink")（Admiral Sir
    Hugh Sinclair）,
  - 1939–1952: [斯图尔特·孟席斯少将爵士](../Page/斯图尔特·孟席斯.md "wikilink")
  - 1953–1956: [约翰·亚历山大·辛克莱尔爵士](../Page/约翰·亚历山大·辛克莱尔.md "wikilink")（Sir
    John Alexander Sinclair）,
  - 1956–1968: [理查德·怀特爵士](../Page/迪克·怀特.md "wikilink")（Sir Richard
    White）,
  - 1968–1973: [约翰·伦尼爵士](../Page/约翰·伦尼（MI6官员）.md "wikilink")（Sir John
    Rennie）,
  - 1973–1978: [莫里斯·奥德菲尔德爵士](../Page/莫里斯·奥德菲尔德.md "wikilink")（Sir
    Maurice Oldfield）,
  - 1979–1982: [迪克·弗兰克斯爵士](../Page/迪克·弗兰克斯.md "wikilink")（Sir Dick
    Franks）,
  - 1982–1985: [科林·菲格雷斯爵士](../Page/科林·菲格雷斯.md "wikilink")（Sir Colin
    Figures）,
  - 1985–1989: [克里斯托弗·柯温爵士](../Page/克里斯托弗·柯温.md "wikilink")（Sir
    Christopher Curwen）,
  - 1989–1994: [科林·麦科尔爵士](../Page/科林·麦科尔.md "wikilink")（Sir Colin
    McColl）,
  - 1994–1999: [戴维·斯佩丁爵士](../Page/戴维·斯佩丁.md "wikilink")（Sir David
    Spedding）,
  - 1999–2004: [理查德·迪尔洛夫爵士](../Page/理查德·迪尔洛夫.md "wikilink")（Sir Richard
    Dearlove）,
  - 2004–2009: [约翰·斯卡莱特爵士](../Page/约翰·斯卡莱特.md "wikilink")（Sir John
    Scarlett）,
  - 2009–2014: [约翰·索厄斯爵士](../Page/约翰·索厄斯.md "wikilink")（Sir John Sawers）
  - 2014年至今: [阿莱克斯·杨格](../Page/阿莱克斯·杨格.md "wikilink")（Alex Younger）

## 相關名人

[Bundesarchiv_Bild_135-S-06-07-32,_Tibetexpedition,_Junger_Engländer.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_135-S-06-07-32,_Tibetexpedition,_Junger_Engländer.jpg "fig:Bundesarchiv_Bild_135-S-06-07-32,_Tibetexpedition,_Junger_Engländer.jpg")，德國著名的[鳥類學家](../Page/鳥類學.md "wikilink")在[西藏](../Page/西藏.md "wikilink")[亞東縣拍攝](../Page/亞東縣.md "wikilink")\]\]

  - 「[劍橋五傑](../Page/劍橋五傑.md "wikilink")」，冷戰時期的一個蘇聯間諜圈，包括[金·費爾比](../Page/金·費爾比.md "wikilink")
      - [安東尼·布朗特](../Page/安東尼·布朗特.md "wikilink")（Anthony
        Blunt，化名：Johnson），MI5官員和蘇聯特工
      - [蓋·伯吉斯](../Page/蓋·伯吉斯.md "wikilink")（Guy
        Burgess，化名：Hicks），SIS官員和蘇聯特工
      - [約翰·凱恩克羅斯](../Page/約翰·凱恩克羅斯.md "wikilink")（John
        Cairncross，化名：Liszt），SIS官員和蘇聯特工
      - [唐納·麥克林](../Page/唐納·麥克林_\(間諜\).md "wikilink")（Donald
        Maclean，化名：Homer），SIS官員和蘇聯特工
      - [金·費爾比](../Page/金·費爾比.md "wikilink")（Kim Philby，化名：Stanley）,
        SIS官員 、MI6官員和蘇聯特工
  - [大衛·康威爾](../Page/約翰·勒卡雷.md "wikilink")（即[約翰·勒卡雷](../Page/約翰·勒卡雷.md "wikilink")），作家，前SIS官員
  - [查爾斯·卡明](../Page/查爾斯·卡明.md "wikilink")（Charles Cumming），作家
  - [保羅·達克斯](../Page/保羅·達克斯.md "wikilink")（Paul Dukes），SIS官員和作家
  - [伊恩·弗萊明](../Page/伊恩·弗萊明.md "wikilink")（Ian
    Fleming），作家，前[海軍情報局（NID）官員](../Page/英國海軍情報局.md "wikilink")
  - [格雷厄姆·格林](../Page/格雷厄姆·格林.md "wikilink")，作家，前SIS官員
  - [拉爾夫·伊澤德](../Page/拉爾夫·伊澤德.md "wikilink")（Ralph
    Izzard），記者，作家，前[海軍情報局（NID）官員](../Page/英國海軍情報局.md "wikilink")
  - [Horst Kopkow](../Page/Horst_Kopkow.md "wikilink"), SS官員，二戰後在SIS工作
  - [亞歷克·利馬斯](../Page/亞歷克·利馬斯.md "wikilink")（Alec
    Leamas），[約翰·勒卡雷小說](../Page/約翰·勒卡雷.md "wikilink")《[柏林碟影](../Page/柏林碟影.md "wikilink")》（*Spy
    Who Came in from the
    Cold*，1963年）中的角色，1965年由[李察·波頓](../Page/李察·波頓.md "wikilink")（Richard
    Burton）主演拍成電影
  - [西德尼·雷利](../Page/西德尼·雷利.md "wikilink")，王牌間諜（*Ace of Spies*）, worked
    for SIS and others, Intelligence mentor of Sir Henry Osborne Oswald
  - [Alex
    Rider](../Page/Alex_Rider.md "wikilink"),英國作家[安東尼·赫洛維兹小說](../Page/安東尼·赫洛維兹.md "wikilink")[Alex
    Rider
    series中的特工人物](../Page/Alex_Rider_series.md "wikilink")，很不情願的為MI6工作
      - [Alan
        Blunt](../Page/Alan_Blunt.md "wikilink"),英國作家[安東尼·赫洛維兹小說](../Page/安東尼·赫洛維兹.md "wikilink")[Alex
        Rider
        Series中的人物](../Page/Alex_Rider_Series.md "wikilink")，MI6特别行動處首腦
      - [Mrs. Jones](../Page/Mrs._Jones.md "wikilink"),
        MI6特别行動處副首腦，[Alan
        Blunt的頂頭上司](../Page/Alan_Blunt.md "wikilink")
      - [Ian Rider](../Page/Ian_Rider.md "wikilink"), MI6特工，Alex
        Rider的叔叔和John Rider的弟弟
      - [John Rider](../Page/John_Rider_\(Alex_Rider\).md "wikilink"),
        MI6特工，Alex Rider的父親和Ian Rider的兄弟
  - [Krystyna Skarbek](../Page/Krystyna_Skarbek.md "wikilink"),特工
  - [George Smiley](../Page/George_Smiley.md "wikilink"),
    [約翰·勒卡雷小說和電影中的人物](../Page/約翰·勒卡雷.md "wikilink")，為MI6工作
  - [Richard
    Tomlinson](../Page/Richard_Tomlinson.md "wikilink"),作家，前SIS官員
  - [Valentine Vivian](../Page/Valentine_Vivian.md "wikilink"),
    SIS副局長和防碟五處首腦
  - [加雷斯·威廉姆斯](../Page/加雷斯·威廉姆斯死亡事件.md "wikilink")（Gareth
    Williams），由國家通信情報局GCHQ借調到軍情六處，2010年因未知原因死亡

## 参考文献

### 引用

### 来源

  - Aldrich, Richard J.（2006）, *The Hidden Hand: Britain, America and
    Cold War Secret Intelligence*, London: John Murray ISBN
    978-1-58567-274-5
  - Davies, Philip H.J.（2004）. *MI6 and the Machinery of Spying* London:
    Frank Cass, ISBN 978-0-7146-8363-8（h/b）
  - Davies, Philip H.J.（2005）'The Machinery of Spying Breaks Down' in
    *Studies in Intelligence* Summer 2005 Declassified Edition.
  - Dorril, Stephen (2001) *MI6: Fifty Years of Special Operations*
    London: Fourth Estate, ISBN 978-1-85702-701-3
  - Humphreys, Rob (1999) *London: The Rough Guide*, Rough Guides, ISBN
    978-1-85828-404-0
  - Jeffery, Keith (2010) *MI6: The History of the Secret Intelligence
    Service 1909–1949* London: Bloomsbury, ISBN 978-0-7475-9183-2
  - Judd, Alan (1999) *The quest for C : Sir Mansfield Cumming and the
    founding of the British Secret Service,* London: HarperCollins, ISBN
    978-0-00-255901-0
  - Seeger, Kirsten Olstrup (2008) 'Friendly Fire'（DK）ISBN
    978-87-7799-193-6. A biography of the author's father who was a
    member of the Danish resistance during the Second World War.
  - Smith, Michael (2010) *SIX: A History of Britain's Secret
    Intelligence Service Pt 1 Murder and Mayhem 1909–1939*, London:
    Dialogue, ISBN 978-1-906447-00-7
  - Smiley, Colonel David (1994). *Irregular Regular*. Norwich: Editions
    Michael Russell. ISBN 978-0-85955-202-8. An autobiography of a
    British officer, honorary colonel of the Royal Horse Guards, David
    de Crespigny Smiley LVO, OBE, MC, who served in the [Special
    Operations
    Executive](../Page/Special_Operations_Executive.md "wikilink")
    during World War II（[Albania](../Page/Albania.md "wikilink"),
    [Thailand](../Page/Thailand.md "wikilink")）and was a MI6 officer
    after war（Poland, Malta, Oman, Yemen）. Translated in French by
    Thierry Le Breton, *Au cœur de l’action clandestine. Des Commandos
    au MI6*, L’Esprit du Livre Editions, France, 2008 ISBN
    978-2-915960-27-3
  - Tomlinson, Richard; Nick Fielding (2001). *The Big Breach: From Top
    Secret to Maximum Security*. Mainstream Publishing ISBN
    978-1-903813-01-0
  - Walton, Calder (2012). *Empire of Secrets*. London: Harperpress.
    ISBN 978-0-00-745796-0

## 外部链接

  - [部门官方网站](http://www.sis.gov.uk)}

  - [UK Intelligence Community On
    Line](https://web.archive.org/web/20090225140651/http://www.intelligence.gov.uk/)

  - from the [Foreign and Commonwealth
    Office](../Page/Foreign_and_Commonwealth_Office.md "wikilink")'s
    website

  - [BBC interview with MI6
    spy.](http://www.bbc.co.uk/blogs/theoneshow/consumer/2008/09/23/exclusive-justin-at-spy-hq.html)
    BBC's *The One* show presenter interviews MI6 spy

## 参见

  - [軍情五處](../Page/軍情五處.md "wikilink")
  - [秘密情報局E中隊](../Page/秘密情報局E中隊.md "wikilink")
  - [詹姆斯·邦德](../Page/詹姆斯·邦德.md "wikilink")，代号**007**，畅销小说、电影和电子游戏系列中的主角，MI6特工
  - 《》，[BBC](../Page/BBC.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")

{{-}}

[Category:英国情报机构](../Category/英国情报机构.md "wikilink")
[Category:特務組織](../Category/特務組織.md "wikilink")
[MI6](../Category/1909年建立政府機構.md "wikilink")

1.
2.
3.