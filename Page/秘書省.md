**祕書省**，又名**蘭臺**、**麟臺**，[祕書監源設於](../Page/祕書監.md "wikilink")[東漢桓帝](../Page/東漢.md "wikilink")，屬[太常寺](../Page/太常寺.md "wikilink")，三國[曹魏時曾屬](../Page/曹魏.md "wikilink")[少府](../Page/少府.md "wikilink")，西晉初裁撤，西晉末復置秘书寺，[南朝梁後改为省](../Page/梁_\(南朝\).md "wikilink")，置监、丞等官；[唐代祕书省领太史](../Page/唐代.md "wikilink")、著作二局，曾改称兰台、麟台；北宋經籍圖書歸[祕閣](../Page/祕閣.md "wikilink")，祕書僅掌祭祀祝版。[宋神宗](../Page/宋神宗.md "wikilink")[元豐改制後](../Page/元豐改制.md "wikilink")，祕書省職事恢復。[元代并入](../Page/元代.md "wikilink")[翰林國史院](../Page/翰林國史院.md "wikilink")，明代因之歸屬[翰林院](../Page/翰林院.md "wikilink")。

唐代為六省之一（[三省只是其中的權力核心](../Page/三省.md "wikilink")，實際共有六省），工作性質是管理皇家[圖書館](../Page/圖書館.md "wikilink")，掌邦國經籍圖書之事。

祕書省內設[秘書監](../Page/秘書監.md "wikilink")（皇家圖書館館長，從三品）一人，[祕書少監](../Page/祕書少監.md "wikilink")（副長官，又名蘭臺侍郎，從四品）二人，[祕書丞一人](../Page/祕書丞.md "wikilink")，[祕書郎四人](../Page/祕書郎.md "wikilink")、[校書郎八人](../Page/校書郎.md "wikilink")、[正字四人](../Page/正字_\(官员\).md "wikilink")，[主事一人](../Page/主事.md "wikilink")，[令史四人](../Page/令史.md "wikilink")、書令史九人、典書四人、楷書十人、亭長六人、掌固八人、熟紙匠十人、裝潢匠十人、筆匠六人等共八十八人。

《正字通》从示从必。俗从禾作秘，譌。

## 参考文献

  - Huiping Pang (彭慧萍), “Liangsong gongtingshuhua chucangzhidu zhibian:
    yi Mige wei hexin zhi jiancang jizhiyanjiu”
    兩宋宮廷書畫儲藏制度之變：以秘閣為核心之鑒藏機制研究
    \[A Study of the System for Authenticating and Collecting Works of
    Art Centered on the Palace Library (Mige) of the Southern Song
    Dynasty\], Gugong Bowuyuan Yuankan \[Palace Museum Journal\], 1
    (2005), 12-40.
  - Huiping Pang (彭慧萍), “Cunshi shuhua suoqian Shangshushengyin kao”
    存世書畫所鈐尚書省印考 \[Analysis of the authenticity of the eight
    seals of “Department of State Affairs”\], Wenwu \[Cultural Relics\],
    11 (2008), 77-93.

## 参见

  - [国家图书馆](../Page/国家图书馆.md "wikilink")

[Category:秘书省](../Category/秘书省.md "wikilink")