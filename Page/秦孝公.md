**秦孝公**（），《[越绝书](../Page/越绝书.md "wikilink")》作**秦平王**，《[史记索隐](../Page/史記索隱.md "wikilink")》记载名**渠梁**。[战国时期](../Page/戰國.md "wikilink")[秦国君主](../Page/秦国.md "wikilink")，[秦献公之子](../Page/秦献公.md "wikilink")，前362年－前338年在位，在位共24年。

秦孝公在位期间致力于恢复[秦穆公时的霸业](../Page/秦穆公.md "wikilink")，他因此颁布著名的求贤令，任用[商鞅进行变法](../Page/商鞅.md "wikilink")，将秦国改造成为富裕强大之国，为秦国将来兼并六国，统一[中国奠定了基础](../Page/中国.md "wikilink")。\[1\]

## 生平

### 继位

秦孝公于秦献公四年正月庚寅（正月初九，[儒略历前](../Page/儒略历.md "wikilink")382年12月6日）出生。前362年，其父秦献公去世后，秦孝公继位。秦孝公继位时年仅21岁。《史记·卷五·秦本纪》：（秦献公）二十四年，献公卒，子孝公立，年已二十一岁矣。

早在秦孝公出生前，秦国经历了四代亂政（[秦厲公](../Page/秦厲公.md "wikilink")、[秦躁公](../Page/秦躁公.md "wikilink")、[秦簡公](../Page/秦簡公.md "wikilink")、[秦出子](../Page/秦出子.md "wikilink")），国力大为削弱。[魏国趁秦国政局不稳之机夺取了](../Page/魏国_\(战国\).md "wikilink")[河西地区](../Page/西河郡.md "wikilink")（今[山西](../Page/山西省.md "wikilink")、[陕西两省间](../Page/陕西省.md "wikilink")[黄河南段以西地区](../Page/黄河.md "wikilink")）。秦孝公之父秦献公继位后，安定边境，迁都[栎阳](../Page/栎阳.md "wikilink")（今陕西省[渭南市](../Page/渭南市.md "wikilink")[富平县东南](../Page/富平县.md "wikilink")），并且数次东征，想要收复河西失地，无奈愿望没有实现便去世。\[2\]

### 求贤令

秦孝公继位时与[齐威王](../Page/齐威王.md "wikilink")、[楚宣王](../Page/楚宣王.md "wikilink")、[魏惠王](../Page/魏惠王.md "wikilink")、[燕文公](../Page/燕后文公.md "wikilink")、[韩昭侯](../Page/韩昭侯.md "wikilink")、[赵成侯并立](../Page/趙成侯.md "wikilink")。当时[黄河和](../Page/黄河.md "wikilink")[殽山以东的](../Page/崤.md "wikilink")[战国六雄已经形成](../Page/戰國.md "wikilink")，[淮河](../Page/淮河.md "wikilink")、[泗水之间有十多个小国](../Page/泗水.md "wikilink")。周王室势力衰微，诸侯间用武力相互征伐吞并。战国六雄中，[楚国](../Page/楚国.md "wikilink")、魏国与秦国接壤。魏国占有原本属于秦国的河西地区，从[郑县](../Page/华县.md "wikilink")（今陕西省[渭南市](../Page/渭南市.md "wikilink")[华州区](../Page/华州区.md "wikilink")）沿[洛河北上修筑](../Page/洛河.md "wikilink")[长城](../Page/长城.md "wikilink")。楚国自[汉中郡往南](../Page/汉中郡.md "wikilink")，占有[巫郡](../Page/巫黔郡.md "wikilink")和[黔中郡](../Page/黔中郡.md "wikilink")。秦国地处偏僻的[雍州](../Page/雍州_\(古代\).md "wikilink")，不参加[中原各国诸侯的盟会](../Page/中原.md "wikilink")，被诸侯们疏远，像对待夷狄一样对待。秦孝公继位后以恢复秦穆公时期的霸业为己任，广施恩德，救济孤寡，招募战士，明确论功行赏的法令，并在国内颁布了著名的求贤令，命[国人大臣献富国强兵之策](../Page/国人.md "wikilink")，全文如下：

同年，秦孝公两路出兵，向东围攻[陕城](../Page/陕城.md "wikilink")（今陕西省[韩城市](../Page/韩城市.md "wikilink")），向西进攻[西戎](../Page/西戎.md "wikilink")，斩杀西戎獂王。\[3\]而赵、韩两国也趁秦孝公继位未稳之机率军攻打秦国。\[4\]

前360年，[周显王派使臣送来祭肉](../Page/周显王.md "wikilink")。\[5\]

### 商鞅变法

#### 商鞅入秦

[卫国人商鞅在](../Page/卫国.md "wikilink")[公叔痤死后听闻秦孝公的求贤令](../Page/公叔痤.md "wikilink")，便攜帶李悝的《[法經](../Page/法經.md "wikilink")》投奔秦国，通過秦孝公的宠臣[景監見孝公](../Page/景監.md "wikilink")。商鞅第一次用[帝道游说秦孝公](../Page/帝道.md "wikilink")，孝公听后直打瞌睡并通过景监指责商鞅是个狂妄之徒，不可任用。五日后，商鞅再次会见秦孝公，用[王道之术游说](../Page/王道_\(儒家思想\).md "wikilink")，孝公不能接受并再次通过景监责备商鞅。商鞅第三次会见秦孝公时用[霸道之术游说](../Page/霸道.md "wikilink")，获得孝公的肯定但没有被采用，但商鞅此时已领会孝公心中的意图。最后商鞅见孝公时畅谈富国强兵之策，孝公听时十分入迷，膝盖不知不觉向商鞅挪动，二人畅谈数日毫无倦意。景监不得其解，向商鞅询问缘由。商鞅说秦孝公意图在当今争霸天下，所以对耗时太长才能取得成效的帝道、王道学说不感兴趣。\[6\]\[7\]

#### 垦草令

在商鞅的劝说下，秦孝公决定在秦国国内进行变法，但变法遭到以[甘龙](../Page/甘龙.md "wikilink")、[杜挚为代表的守旧派的反对](../Page/杜挚.md "wikilink")，双方产生激烈的争论。变法之争结束后，秦孝公于前359年命商鞅在秦国国内颁布《[垦草令](../Page/垦草令.md "wikilink")》，作为全面变法的序幕。主要内容有：刺激农业生产、抑制商业发展、重塑社会价值观，提高农业的社会认知度、削弱贵族、官吏的特权，让国内贵族加入到农业生产中、实行统一的税租制度以及其他措施。

#### 第一次变法

《垦草令》在秦国成功实施后，秦孝公于前356年任命商鞅为[左庶长](../Page/左庶长.md "wikilink")，在秦国国内实行第一次变法。\[8\]主要内容为：改革户籍制度，实行[什伍连坐法](../Page/什伍连坐法.md "wikilink")、明令军法，奖励军功、废除[世卿世禄制度](../Page/世袭.md "wikilink")、建立[二十等军功爵制](../Page/二十等爵.md "wikilink")、奖励耕织，重农抑商，严惩私斗、改法为律，制定[秦律和推行小家庭制](../Page/秦律.md "wikilink")。

经过第一次变法后，秦国国力开始强大。前358年，秦国在[西山](../Page/西山_\(古代地名\).md "wikilink")（今[河南省](../Page/河南省.md "wikilink")[熊耳山以西](../Page/熊耳山.md "wikilink")）击败[韩国](../Page/韩国_\(战国\).md "wikilink")。\[9\]前357年，楚宣王派[右尹黑来迎娶秦孝公的女儿](../Page/右尹.md "wikilink")，与秦国联姻。\[10\]前355年，秦孝公与魏惠王在[杜平](../Page/杜平_\(古代地名\).md "wikilink")（今陕西省[澄城县东](../Page/澄城县.md "wikilink")）会盟，结束了秦国长期不与中原诸侯会盟的局面。\[11\]

#### 第二次变法

[咸阳](../Page/咸阳市.md "wikilink")（今陕西省[咸阳市东北](../Page/咸阳市.md "wikilink")）位于[关中平原中部](../Page/关中平原.md "wikilink")，北依高原，南临[渭河](../Page/渭河.md "wikilink")，顺渭河而下可直入黄河，[终南山与渭河之间可直通](../Page/终南山.md "wikilink")[函谷关](../Page/函谷关.md "wikilink")。为便于向函谷关以东发展，秦孝公于前350年命商鞅征调士卒，按照[鲁国](../Page/鲁国.md "wikilink")、卫国的国都规模修筑冀阙宫廷，营造新都，并于次年将国都从栎阳迁至咸阳，同时命商鞅在秦国国内进行第二次变法。\[12\]\[13\]主要内容为：开阡陌封疆，废[井田](../Page/井田.md "wikilink")，制[辕田](../Page/辕田.md "wikilink")，允许土地私有及买卖、推行县制、加收[口赋](../Page/人头税.md "wikilink")、统一[度量衡](../Page/度量衡.md "wikilink")、燔诗书而明法令，塞私门之请，禁游宦之民和执行分户令。

经过两次变法后的秦国国力强大，百姓家家富裕充足。秦国人路不拾遗，山中没有盗贼。人民勇于为国家打仗，怯于私斗，乡村、城镇秩序安定。\[14\]周显王派使臣赐予秦孝公霸主的称号，诸侯各国都派使者前来祝贺。\[15\]前348年，韩昭侯亲自前往秦国，与秦孝公签订停战盟约。\[16\]前342年，秦孝公派[太子骃(驷)率领西戎九十二国朝见周显王](../Page/秦惠文王.md "wikilink")，显示了秦国西方霸主的地位。\[17\]

### 河西之战

收复河西失地，恢复秦穆公时期的霸业是秦献公、秦孝公两代国君的愿望。早在秦献公时期，秦国与魏国为争夺河西地区发生多次战争，其中秦国在[洛阴](../Page/洛阴.md "wikilink")（今陕西省[大荔县西南](../Page/大荔县.md "wikilink")）、[石门山](../Page/石門山.md "wikilink")（今山西省[运城市西南](../Page/运城市.md "wikilink")）、[少梁](../Page/韩城市.md "wikilink")（今陕西省韩城市西南）三场战役中取得胜利。\[18\]\[19\]\[20\]经过商鞅两次变法后的秦国国力强大，具备收复河西地区的能力。

#### 元里之战

前354年，[赵国入侵魏国的盟国卫国](../Page/赵国.md "wikilink")，夺取漆及[富丘](../Page/富丘.md "wikilink")（均在今河南省[长垣县](../Page/长垣县.md "wikilink")）两地。\[21\]此举招致魏国的干涉，魏国出兵助卫，包围赵都[邯郸](../Page/邯郸市.md "wikilink")（今[河北省](../Page/河北省.md "wikilink")[邯郸市](../Page/邯郸市.md "wikilink")）。\[22\]秦孝公趁魏军主力出击之机，派军队偷袭魏国，进攻魏河西长城重要据点[元里](../Page/元里.md "wikilink")（今陕西省澄城县南），大败魏军，歼灭守军七千人，并占领少梁。\[23\]同年，秦孝公命[公孙壮率军攻打韩国](../Page/公孙壮.md "wikilink")，包围[焦城](../Page/焦城.md "wikilink")（今河南省[三门峡市西](../Page/三门峡市.md "wikilink")），但没有攻克，占领[上枳](../Page/上枳.md "wikilink")、[安陵](../Page/安陵.md "wikilink")（今河南省[鄢陵县北](../Page/鄢陵县.md "wikilink")）、[山氏](../Page/山氏.md "wikilink")（今河南省[新郑市东北](../Page/新郑市.md "wikilink")）并筑城，插入魏、韩两国交界地区。\[24\]

#### 安邑、固阳之战

前353年，[赵成侯派使者向](../Page/赵成侯.md "wikilink")[齐国求援](../Page/齐国.md "wikilink")，\[25\][齐威王派兵分两路救援赵国](../Page/齐威王.md "wikilink")，一路齐军与[宋国](../Page/宋国.md "wikilink")[景㪨](../Page/景㪨.md "wikilink")、卫国[公孙仓所率部队會合](../Page/公孙仓.md "wikilink")，围攻魏国的[襄陵](../Page/襄陵.md "wikilink")（今河南省[睢县](../Page/睢县.md "wikilink")）。\[26\]同年，魏军主力攻破赵国首都邯郸，但在[桂陵之战被另一路由](../Page/桂陵之战.md "wikilink")[田忌](../Page/田忌.md "wikilink")、[孙膑所率领的齐国军队击败](../Page/孙膑.md "wikilink")。\[27\]楚宣王也派大将[景舍率兵救援赵国](../Page/景舍.md "wikilink")，夺取魏国[睢水](../Page/睢水.md "wikilink")、[濊水之间的土地](../Page/濊水.md "wikilink")。\[28\]秦孝公趁魏国国内空虚之机，于前352年任命商鞅为[大良造](../Page/大良造.md "wikilink")，率兵长驱直入，包围魏国并占领魏国旧都[安邑](../Page/安邑.md "wikilink")（今山西省[夏县西北](../Page/夏县.md "wikilink")）。\[29\]魏惠王急忙派军队在[上郡要地](../Page/上郡.md "wikilink")[固阳](../Page/固阳_\(古代地名\).md "wikilink")（即定阳，今陕西省[延安市东](../Page/延安市.md "wikilink")）以东修建[崤山长城](../Page/崤山长城.md "wikilink")（东南起崤山，西北至黄河），以阻止秦军的进攻。\[30\]前351年，商鞅又率兵包围并占领固阳。\[31\]

#### 彤地会盟

前352年，魏惠王调用韩国的军队击败包围襄陵的齐、宋、卫联军，齐国被迫请楚国大将景舍出面调停，各国休战。\[32\]前351年，魏惠王与[赵成侯在](../Page/赵成侯.md "wikilink")[漳河边结盟](../Page/漳河.md "wikilink")，撤出赵国首都邯郸。\[33\]魏国陆续与各国和谈后，集中兵力向西反攻，夺回安邑并包围固阳。\[34\]为争取时间在国内进行第二次变法，秦孝公于前350年与魏惠王在[彤地](../Page/彤地.md "wikilink")（今陕西省渭南市华州区西南）会盟修好，缓和两国紧张的关系。\[35\]

#### 逢泽会盟

魏惠王以朝见周天子为名，召集[泗上十二诸侯举行会盟](../Page/泗上十二诸侯.md "wikilink")，图谋攻秦。秦孝公下令加强防守，并采纳商鞅“尊魏为王”的策略来改变魏国进攻秦国的意图。秦孝公于前344年派商鞅游说魏惠王，劝说他除了号令宋、卫、邹、鲁等十二个小国外，还要向北联合[燕国](../Page/燕国.md "wikilink")，向东攻打齐国，迫使赵国屈服；向西联合秦国，向南攻打楚国，迫使韩国屈服，这样霸业可成。商鞅还建议魏惠王顺从天下之志，先行称王，再图霸业。魏惠王听从商鞅的游说开始称王，按照天子的规格大建宫室，制作丹衣和九施、七星之旗，并召集各小国参加[逢泽](../Page/逢泽.md "wikilink")（今河南省[开封市南](../Page/开封市.md "wikilink")）会盟，秦[公子少官和](../Page/公子少官.md "wikilink")[赵肃侯](../Page/赵肃侯.md "wikilink")也应邀参加，诸侯会盟后又前往朝见周天子。魏惠王僭越礼制的行为引起齐、楚等国的愤怒，诸侯纷纷倒向齐国。\[36\]\[37\]\[38\]

#### 西鄙之战

前343年，秦国在[武城](../Page/武城县_\(秦代\).md "wikilink")（今陕西省渭南市华州区东）筑城。\[39\]

前341年，魏国在[马陵之战遭受齐国重创](../Page/馬陵之戰.md "wikilink")，主将[庞涓自杀](../Page/庞涓.md "wikilink")，[太子申被俘](../Page/太子申.md "wikilink")。商鞅趁机对秦孝公说：“秦国和魏国的关系，就像人得了心腹疾病，不是魏国兼并秦国，就是秦国吞并魏国。魏国地处山岭险要的西部，占据崤山以东，与秦国以黄河为界。形势有利时就向西侵犯秦国，不利时就向东扩展领地。如今君上圣明贤能，秦国繁荣昌盛。而魏国刚刚被齐国击败，可以趁此良机攻打魏国。魏国抵挡不住秦国的攻势，必然要向东撤退。魏国一向东撤退，秦国就占据黄河和崤山险固的地势，向东可以控制各国诸侯，这可是一统天下的帝王基业啊！”秦孝公采纳商鞅的建议，决定趁魏国实力尚未恢复之机，大举攻魏。

前341年，秦国联合齐、赵两国攻打魏国。同年九月，秦孝公派商鞅进攻魏河东，魏派[公子卬迎战](../Page/公子卬_\(魏国\).md "wikilink")。两军对峙时，商鞅派使者送信给公子卬，说：“我当初与公子相处的很快乐，如今你我成了敌对两国的将领，不忍心相互攻击，我可以与公子当面相见，订立盟约，痛痛快快地喝几杯然后各自撤兵，让秦魏两国相安无事。”公子卬赴会时被商鞅埋伏的甲士俘虏，商鞅趁机攻击魏军，魏军大败。魏惠王被迫割让河西部分土地求和，商鞅因战功获封於商-{於}-郡十五邑，号为商君。\[40\]\[41\]

前338年，秦再次攻魏，在[岸门](../Page/岸门.md "wikilink")（今山西省[河津市南](../Page/河津市.md "wikilink")）击败魏军，俘其主将[魏错](../Page/魏错.md "wikilink")。\[42\]同年，秦国联合[大荔戎包围了魏国的](../Page/大荔戎.md "wikilink")[郃阳](../Page/郃阳.md "wikilink")（今陕西省[合阳县东南](../Page/合阳县.md "wikilink")）。\[43\]

### 去世

前338年，秦孝公病危，《[战国策](../Page/战国策.md "wikilink")》记载秦孝公想传位于商鞅，商鞅推辞不接受。\[44\]五个月后，秦孝公去世，葬于[弟圉](../Page/弟圉.md "wikilink")，其子[秦惠文王继位](../Page/秦惠文王.md "wikilink")。\[45\]商鞅因被[公子虔诬陷谋反](../Page/公子虔.md "wikilink")，战败死于彤地\[46\]，其尸身被带回咸阳，处以[车裂后示众](../Page/車裂.md "wikilink")。\[47\]

## 名言

代立不忘社稷，君之道也；錯法務明主長，臣之行也。

今吾欲變法以治，更禮以教百姓，恐天下之議我也。

## 其他

  - 前361年，西方出现[彗星](../Page/彗星.md "wikilink")。\[48\]
  - 前346年，桃树李树在冬天开花。\[49\]
  - 前341年，马生出了人。\[50\]

## 评价

[西汉的](../Page/西汉.md "wikilink")[贾谊在](../Page/贾谊.md "wikilink")《[过秦论](../Page/过秦论.md "wikilink")》中评价秦孝公：秦孝公据崤函之固，拥雍州之地，君臣固守，以窥周室，有席卷天下、包举宇内、囊括四海之意，并吞八荒之心。当是时，商君佐之，内立法度，务耕织，修守战之备；外连衡而斗诸侯。于是秦人拱手而取西河之外。\[51\]

## 影视形象

1999年上映的长篇电视剧《[东周列国战国篇](../Page/东周列国战国篇.md "wikilink")》中，秦孝公由[陈强饰演](../Page/陈强_\(演员\).md "wikilink")。\[52\]2009年首播的电视剧《[大秦帝国](../Page/大秦帝国.md "wikilink")·第一部·裂变》中，秦孝公作为本剧的主人公之一，由[侯勇饰演](../Page/侯勇.md "wikilink")。\[53\]

## 注释

<div class="references-medium">

</div>

## 参考资料

## 紀年

| 秦孝公                            | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             | 6年                             | 7年                             | 8年                             | 9年                             | 10年                            |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前361年                          | 前360年                          | 前359年                          | 前358年                          | 前357年                          | 前356年                          | 前355年                          | 前354年                          | 前353年                          | 前352年                          |
| [干支](../Page/干支.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") |
| 秦孝公                            | 11年                            | 12年                            | 13年                            | 14年                            | 15年                            | 16年                            | 17年                            | 18年                            | 19年                            | 20年                            |
| 西元                             | 前351年                          | 前350年                          | 前349年                          | 前348年                          | 前347年                          | 前346年                          | 前345年                          | 前344年                          | 前343年                          | 前342年                          |
| [干支](../Page/干支.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") |
| 秦孝公                            | 21年                            | 22年                            | 23年                            | 24年                            |                                |                                |                                |                                |                                |                                |
| 西元                             | 前341年                          | 前340年                          | 前339年                          | 前338年                          |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") |                                |                                |                                |                                |                                |                                |

[Category:秦國君主](../Category/秦國君主.md "wikilink")

1.  《盐铁论·卷二·非鞅》：秦任商君，国以富强，其后卒并六国而成帝业。

2.  《史记·卷五·秦本纪》：会往者厉、躁、简公、出子之不宁，国家内忧，未遑外事，三晋攻夺我先君河西地，诸侯卑秦、丑莫大焉。献公即位，镇抚边境，徙治栎阳，且欲东伐，复缪公之故地，修缪公之政令。寡人思念先君之意，常痛于心。

3.  《史记·卷五·秦本纪》：孝公元年，河山以东强国六，与齐威、楚宣、魏惠、燕悼（应为燕文公）、韩哀、赵成侯并。淮泗之间小国十余。楚、魏与秦接界。魏筑长城，自郑滨洛以北，有上郡。楚自汉中，南有巴（应为巫郡）、黔中。周室微，诸侯力政，争相并。秦僻在雍州，不与中国诸侯之会盟，夷翟遇之。孝公于是布惠，振孤寡，招战士，明功赏…于是乃出兵东围陕城，西斩戎之獂王。

4.  《史记·卷四十三·赵世家》：（赵成侯）十四年，（赵）与韩攻秦。

5.  《史记·卷十五·六国年表》：（秦孝公）二年，天子致胙。

6.  《魏书·卷一百一十一·刑罚志》：商君以法经六篇，入说于秦，议参夷之诛，连相坐之法。

7.  《史记·卷六十八·商君列传》：公叔既死，公孙鞅闻秦孝公下令国中求贤者，将修缪公之业，东复侵地，乃遂西入秦，因孝公宠臣景监以求见孝公。孝公既见卫鞅，语事良久，孝公时时睡，弗听。罢而孝公怒景监曰：“子之客妄人耳，安足用邪！”景监以让卫鞅。卫鞅曰：“吾说公以帝道，其志不开悟矣。”后五日，复求见鞅。鞅复见孝公，益愈，然而未中旨。罢而孝公复让景监，景监亦让鞅。鞅曰：“吾说公以王道而未入也。请复见鞅。”鞅复见孝公，孝公善之而未用也。罢而去。孝公谓景监曰：“汝客善，可与语矣。”鞅曰：“吾说公以霸道，其意欲用之矣。诚复见我，我知之矣。”卫鞅复见孝公。公与语，不自知厀之前於席也。语数日不厌。景监曰：“子何以中吾君？吾君之驩甚也。”鞅曰：“吾说君以帝王之道比三代，而君曰：‘久远，吾不能待。且贤君者，各及其身显名天下，安能邑邑待数十百年以成帝王乎？’故吾以彊国之术说君，君大说之耳。然亦难以比德於殷周矣。”

8.  《史记·卷五·秦本纪》：卒用鞅法，百姓苦之；居三年，百姓便之。乃拜鞅为左庶长。

9.  《史记·卷十五·六国年表》：韩昭侯元年（应为五年），秦败我西山。

10. 《史记·卷十五·六国年表》：（楚宣王）十三年，君（应为右）尹黑迎女秦。

11. 《史记·卷十五·六国年表》：（秦孝公）七年，与魏王会杜平。

12. 《史记·卷五·秦本纪》：（秦孝公）十二年，作为咸阳，筑冀阙，秦徙都之。

13. 《史记·卷六十八·商君列传》：大筑冀阙，营如鲁卫矣。

14. 《史记·卷六十八·商君列传》：行之十年，秦民大说，道不拾遗，山无盗贼，家给人足。民勇于公战，怯于私斗，乡邑大治。

15. 《史记·卷五·秦本纪》：（秦孝公）十九年，天子致伯。二十年，诸侯毕贺。

16. 《史记·卷十五·六国年表》：韩昭侯十一年（应为十五年），昭侯如秦。

17. 《后汉书·卷八十七·西羌传》：时秦孝公雄强，威服羌戎。孝公使太子骃(驷)率戎狄九十二国朝周显王。

18. 《史记·卷十五·六国年表》：（秦献公）十九年，败韩、魏洛阴。

19. 《史记·卷十五·六国年表》：（秦献公）二十一年，章蟜与晋战于石门，斩首六万。

20. 《史记·卷四十三·赵世家》：（赵成侯）十三年，秦献公使庶长国伐魏少梁。

21. 《古本竹书纪年·魏纪》：梁惠成王十六年，邯郸伐卫 ，取漆富丘，城之。

22. 《史记·卷四十三·赵世家》：（赵成侯）二十一年，魏围我邯郸。

23. 《史记·卷十五·六国年表》：（秦孝公）八年，与魏战元里，斩首七千，取少梁。

24. 《古本竹书纪年·魏纪》：梁惠成王十六年，秦公孙壮率师伐郑，围焦城，不克。秦公孙壮率师城上枳、安陵、山氏。

25. 《战国策·卷八·齐策一·邯郸之难》：邯郸之难，赵求救于齐。

26. 《古本竹书纪年·魏纪》：梁惠成王十七年，宋景㪨、卫公孙仓会齐师，围我襄陵。

27. 《史记·卷十五·六国年表》：魏惠王十八年（应为十七年），邯郸降。齐败我桂陵。

28. 《战国策·卷十四·楚策一·邯郸之难》：楚因使景舍起兵救赵。邯郸拔，楚取睢、濊之间。

29. 《史记·卷五·秦本纪》：（秦孝公）十年，卫鞅为大良造，将兵围魏安邑，降之。

30. 《史记·卷十五·六国年表》：（魏惠王）十九年（应为十八年），筑长城，塞固阳。

31. 《史记·卷十五·六国年表》：（秦孝公）十一年，卫鞅围固阳，降之。

32. 《古本竹书纪年·魏纪》：（梁惠成王）十八年， 惠成王以韩师败诸侯师于襄陵。齐侯使楚景舍来求成。公会齐、宋之围。

33. 《史记·卷四十三·赵世家》：（赵成侯）二十四年，魏归我邯郸，与魏盟漳水上。

34.
35. 《史记·卷四十四·魏世家》：（魏惠王）二十一年（应为二十年），与秦会彤。

36. 《战国策·卷十二·齐策五·苏秦说齐闵王》：昔者魏王拥土千里，带甲三十六万，其强而拔邯郸，西围定阳，又从十二诸侯朝天子，以西谋秦。秦王恐之，寝不安席，食不甘味，令于境内，尽堞中为战具，竟为守备，为死士置将，以待魏氏。卫鞅谋于秦王曰：‘夫魏氏其功大，而令行于天下，有十二诸侯而朝天子，其与必众。故以一秦而敌大魏，恐不如。王何不使臣见魏王，则臣请必北魏矣。’秦王许诺。卫鞅见魏王曰：‘大王之功大矣，令行于天下矣。今大王之所从十二诸侯，非宋、卫也，则邹、鲁、陈、蔡，此固大王之所以鞭箠使也，不足以王天下。大王不若北取燕，东伐齐，则赵必从矣；西取秦，南伐楚，则韩必从矣。大王有伐齐、楚心，而从天下之志，则王业见矣。大王不如先行王服，然后图齐、楚。’魏王说于卫鞅之言也，故身广公宫，制丹衣柱，建九斿，从七星之旟。此天子之位也，而魏王处之。于是齐、楚怒，诸侯奔齐。

37. 《战国策·卷六·秦策四·或为六国说秦王》：魏伐邯郸，因退为逢泽之遇，乘夏车，称夏王，朝为天子，天下皆从。

38. 《史记·卷五·秦本纪》：（秦孝公）二十年（应为十八年），诸侯毕贺。秦使公子少官率师会诸侯逢泽，朝天子。

39. 《史记·卷十五·六国年表》：（秦孝公）十九年，城武城。

40. 《古本竹书纪年·魏纪》：（魏惠王）二十九年五月，齐田朌伐我东鄙；九月，秦卫鞅伐我西鄙；十月，邯郸伐我北鄙。王攻卫鞅，我师败绩。

41. 《史记·卷六十八·商君列传》：其明年，齐败魏兵于马陵，虏其太子申，杀将军庞涓。其明年，卫鞅说孝公曰：“秦之与魏，譬若人之有腹心疾，非魏并秦，秦即并魏。何者？魏居岭厄之西，都安邑，与秦界河而独擅山东之利。利则西侵秦，病则东收地。今以君之贤圣，国赖以盛。而魏往年大破于齐，诸侯畔之，可因此时伐魏。魏不支秦，必东徙。东徙，秦据河山之固，东乡以制诸侯，此帝王之业也。孝公以为然，使卫鞅将而伐魏。魏使公子卬将而击之。军既相距，卫鞅遗魏将公子卬书曰：“吾始与公子驩，今俱为两国将，不忍相攻，可与公子面相见，盟，乐饮而罢兵，以安秦魏。”魏公子卬以为然。会盟已，饮，而卫鞅伏甲士而袭虏魏公子卬，因攻其军，尽破之以归秦。魏惠王兵数破于齐秦，国内空，日以削，恐，乃使使割河西之地献于秦以和…卫鞅既破魏还，秦封之-{於}-、商十五邑，号为商君。

42. 《史记·卷五·秦本纪》：（秦孝公）二十四年，与晋战雁门（应为岸门），虏其将魏错。

43. 《史记·卷十五·六国年表》：（秦孝公）二十四年，秦与大荔围合阳。

44. 《战国策·卷三·卫鞅亡魏入秦》：孝公行之八年，疾且不起，欲传商君，辞不受。

45. 《史记·卷六·秦始皇本纪》：孝公享国二十四年。葬弟圉。生惠文王。

46. 《[史記](../Page/史記.md "wikilink")》卷十五《[六國年表](../Page/六國年表.md "wikilink")》記載為「彤地」，《史记·卷六十八·商君列传》記載為「郑黾池」

47. 《史记·卷六十八·商君列传》：后五月而秦孝公卒，太子立。公子虔之徒告商君欲反…商君既复入秦，走商邑，与其徒属发邑兵北出击郑。秦发兵攻商君，杀之于郑黾池。秦惠王车裂商君以徇。

48. 《史记·卷十五·六国年表》：秦孝公元年，彗星见西方。

49. 《史记·卷六·秦始皇本纪》：孝公立十六年。时桃李冬华。

50. 《史记·卷十五·六国年表》：（秦孝公）二十一年，马生人。

51. 见 《过秦论·上篇》。

52.

53.