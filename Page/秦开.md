**秦開**（）是[戰國時](../Page/戰國.md "wikilink")[燕国](../Page/燕国.md "wikilink")[昭王時代的將軍](../Page/燕昭王.md "wikilink")。[荆轲副將](../Page/荆轲.md "wikilink")[秦舞陽的祖父](../Page/秦舞陽.md "wikilink")。

## 生平

秦開是燕國將軍。[前3世纪初時被送往](../Page/前3世纪.md "wikilink")[东胡成為](../Page/东胡.md "wikilink")[人質](../Page/人質.md "wikilink")。由於得到東胡的信任，秦開通曉東胡的民情風俗及其虛實。[燕昭王即位後](../Page/燕昭王.md "wikilink")，秦開逃歸燕國。
前300年大破东胡，迫使東胡北退千餘-{里}-。之後曾渡過[遼水進攻](../Page/遼水.md "wikilink")[箕子朝鮮](../Page/箕子朝鮮.md "wikilink")，直達[滿潘汗](../Page/滿潘汗.md "wikilink")（今[朝鲜](../Page/朝鲜.md "wikilink")[清川江](../Page/清川江.md "wikilink")）為界，據有今[遼寧全境](../Page/遼寧.md "wikilink")，並開辟[辽东](../Page/辽东.md "wikilink")，置[上谷](../Page/上谷郡.md "wikilink")、[漁陽](../Page/漁陽郡.md "wikilink")、[右北平](../Page/右北平郡.md "wikilink")、[遼西](../Page/遼西郡.md "wikilink")、[遼東五郡](../Page/遼東郡.md "wikilink")，築[燕長城](../Page/燕長城.md "wikilink")。《[史記·匈奴傳](../Page/:s:史記/卷110.md "wikilink")》有其事蹟。再往後紀錄不詳。

[中國文物局於](../Page/中國文物局.md "wikilink")2000年在[辽宁省](../Page/辽宁省.md "wikilink")[葫蘆島市](../Page/葫蘆島市.md "wikilink")[建昌縣](../Page/建昌縣.md "wikilink")[西碱厂乡東大杖子村](../Page/西碱厂乡.md "wikilink")\[1\]的[大凌河戰國墓地群發掘出](../Page/大凌河.md "wikilink")[積石木槨墳](../Page/積石木槨墳.md "wikilink")、青銅短劍及燕式陶器\[2\]，當中包括秦開的遺物。

[秦舞阳是秦開之孙](../Page/秦舞阳.md "wikilink")，曾隨著[荊軻](../Page/荊軻.md "wikilink")[行刺](../Page/行刺.md "wikilink")[秦始皇](../Page/秦始皇.md "wikilink")。

## 注釋

## 參看

  - [秦舞陽](../Page/秦舞陽.md "wikilink")
  - [荆轲](../Page/荆轲.md "wikilink")

## 外部链接

  -
[Category:春秋战国军事人物](../Category/春秋战国军事人物.md "wikilink")
[Category:辽宁](../Category/辽宁.md "wikilink")

1.
2.