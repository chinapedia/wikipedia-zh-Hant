**秦皇岛市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[河北省下辖的](../Page/河北省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于河北省东端，是[京哈铁路上重要城市之一](../Page/京哈铁路.md "wikilink")。靠在[渤海湾边上](../Page/渤海湾.md "wikilink")，西距首都[北京](../Page/北京市.md "wikilink")280公里，距[天津](../Page/天津市.md "wikilink")220公里，西南距省会[石家庄](../Page/石家庄市.md "wikilink")483公里。以[秦始皇求仙驻跸而得名](../Page/秦始皇.md "wikilink")。

秦皇岛是全国首批14个[沿海开放城市之一](../Page/中華人民共和國沿海開放城市.md "wikilink")，中国北方重要的对外贸易口岸，国务院批准的全国甲级旅游城市。著名的风景区[山海关](../Page/山海关.md "wikilink")、[北戴河坐落于此](../Page/北戴河区.md "wikilink")。

## 历史

[商](../Page/商朝.md "wikilink")[周时期为](../Page/周朝.md "wikilink")[孤竹国中心区域](../Page/孤竹国.md "wikilink")，[春秋时期](../Page/春秋时期.md "wikilink")[晋灭](../Page/晋国.md "wikilink")[肥](../Page/肥國_\(春秋\).md "wikilink")，肥子逃奔[燕国](../Page/燕国.md "wikilink")，燕封肥子在此地建肥子国。[战国时期](../Page/战国.md "wikilink")，此地属[燕国](../Page/燕国.md "wikilink")[辽西郡](../Page/辽西郡.md "wikilink")。[秦](../Page/秦朝.md "wikilink")[汉时期这里是东巡朝拜和兵家必经之地](../Page/汉朝.md "wikilink")。[秦始皇第四次出巡到](../Page/秦始皇.md "wikilink")[碣石](../Page/碣石镇.md "wikilink")，刻碣石门。并派燕人卢生、韩终、侯公、石生等方士入海求仙人和不死之药，秦皇岛由此得名。[汉武帝东巡观海](../Page/汉武帝.md "wikilink")，到碣石筑汉武台，并在此用兵攻朝鲜[衛氏王朝](../Page/卫满朝鲜.md "wikilink")，把[北戴河金山嘴作为屯粮城](../Page/北戴河区.md "wikilink")。[曹操率兵北伐](../Page/曹操.md "wikilink")[乌桓](../Page/乌桓.md "wikilink")，取道[渤海之滨](../Page/渤海.md "wikilink")，望临碣石，赋《观沧海》诗。

[Qinhuangdao_1908_AMS-WO.jpg](https://zh.wikipedia.org/wiki/File:Qinhuangdao_1908_AMS-WO.jpg "fig:Qinhuangdao_1908_AMS-WO.jpg")
[隋](../Page/隋朝.md "wikilink")[唐时期](../Page/唐朝.md "wikilink")，这里是抵御关外[突厥](../Page/突厥人.md "wikilink")、[契丹的战略要地](../Page/契丹人.md "wikilink")，[唐玄宗派](../Page/唐玄宗.md "wikilink")[史思明统平州太守](../Page/史思明.md "wikilink")，兼统卢龙军使。[元朝](../Page/元朝.md "wikilink")[忽必烈将中书省平滦路设在此地](../Page/忽必烈.md "wikilink")，后改为[永平府](../Page/永平府.md "wikilink")，[明朝曾在此设立盐署](../Page/明朝.md "wikilink")。1381年，[明太祖朱元璋派开国元勋中山王](../Page/朱元璋.md "wikilink")[徐达主持修建了](../Page/徐达.md "wikilink")[山海关关城](../Page/山海关.md "wikilink")。明[嘉靖十四年](../Page/嘉靖.md "wikilink")（1535年）版《山海关志》最早记有这一地名：“秦皇岛，城西南25里，又入海1里。”1644年，明王朝覆灭，[李自成带兵](../Page/李自成.md "wikilink")4万余众到山海关征讨[吴三桂](../Page/吴三桂.md "wikilink")，山海关守将吴三桂投降[多尔衮](../Page/多尔衮.md "wikilink")，开关引清军15万同[李自成大战](../Page/李自成.md "wikilink")，这就是改变中国历史命运的“石河大战”，经三天苦战，农民军终因兵寡失利，退至境内卢龙县（永平府），重整军队再战，大败，李自成逃回京城。清王朝从此入主[中原](../Page/中原.md "wikilink")。

[清王朝统治时期](../Page/清朝.md "wikilink")，在山海关设立臨榆县。1898年，秦皇岛港启锚开运，[津榆铁路](../Page/津榆铁路.md "wikilink")（天津－临榆）通车。同年，清政府正式将北戴河开辟为“各国人士避暑地”。[民国初期](../Page/中華民國.md "wikilink")，秦皇岛属[直隶省渤海道](../Page/直隶省.md "wikilink")，外国军队、牧师开始进入秦皇岛境内。

1948年，[解放军入驻秦皇岛](../Page/中国人民解放军.md "wikilink")；1949年设市；1978年秦皇岛作为首批全国沿海开放城市。秦皇岛作为[北京2008奥运会协办城市成功举办奥运赛事](../Page/2008年夏季奥林匹克运动会.md "wikilink")。

## 地理

秦皇岛市地处河北省东北部，北纬39°24′\~ 40°37′，东经118°33′\~
119°51′。南面频临[渤海](../Page/渤海.md "wikilink")，北面背靠[燕山](../Page/燕山.md "wikilink")，东接[辽宁省](../Page/辽宁省.md "wikilink")[葫芦岛市](../Page/葫芦岛市.md "wikilink")，西邻[唐山](../Page/唐山市.md "wikilink")、[承德两市](../Page/承德市.md "wikilink")，距首都[北京约](../Page/北京市.md "wikilink")280公里。市境地势北高南低。

秦皇岛气候类型属于半湿润[温带季风气候](../Page/温带季风气候.md "wikilink")，受海洋影响较大。

## 政治

### 现任领导

<table>
<caption>秦皇岛市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党秦皇岛市委员会.md" title="wikilink">中国共产党<br />
秦皇岛市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/秦皇岛市人民代表大会.md" title="wikilink">秦皇岛市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/秦皇岛市人民政府.md" title="wikilink">秦皇岛市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议秦皇岛市委员会.md" title="wikilink">中国人民政治协商会议<br />
秦皇岛市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/孟祥伟.md" title="wikilink">孟祥伟</a>[1]</p></td>
<td><p><a href="../Page/刘辰彦.md" title="wikilink">刘辰彦</a>[2]</p></td>
<td><p><a href="../Page/张瑞书.md" title="wikilink">张瑞书</a>[3]</p></td>
<td><p><a href="../Page/郝占敏.md" title="wikilink">郝占敏</a>[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/赵县.md" title="wikilink">赵县</a></p></td>
<td><p>河北省<a href="../Page/元氏县.md" title="wikilink">元氏县</a></p></td>
<td><p>河北省<a href="../Page/献县.md" title="wikilink">献县</a></p></td>
<td><p>河北省<a href="../Page/临城县.md" title="wikilink">临城县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2015年9月</p></td>
<td><p>2016年2月</p></td>
<td><p>2015年2月</p></td>
<td><p>2016年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖4个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")、1个[自治县](../Page/自治县.md "wikilink")：

  - 市辖区：[海港区](../Page/海港区_\(秦皇岛市\).md "wikilink")、[山海关区](../Page/山海关区.md "wikilink")、[北戴河区](../Page/北戴河区.md "wikilink")、[抚宁区](../Page/抚宁区.md "wikilink")
  - 县：[昌黎县](../Page/昌黎县.md "wikilink")、[卢龙县](../Page/卢龙县.md "wikilink")
  - 自治县：[青龙满族自治县](../Page/青龙满族自治县.md "wikilink")

另外，秦皇岛市设有[国家级](../Page/国家级经济技术开发区.md "wikilink")[秦皇岛经济技术开发区](../Page/秦皇岛经济技术开发区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>秦皇岛市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>130300</p></td>
</tr>
<tr class="odd">
<td><p>130302</p></td>
</tr>
<tr class="even">
<td><p>130303</p></td>
</tr>
<tr class="odd">
<td><p>130304</p></td>
</tr>
<tr class="even">
<td><p>130306</p></td>
</tr>
<tr class="odd">
<td><p>130321</p></td>
</tr>
<tr class="even">
<td><p>130322</p></td>
</tr>
<tr class="odd">
<td><p>130324</p></td>
</tr>
<tr class="even">
<td><p>注：海港区珠江道街道、山海关区船厂路街道、渤海乡实际由秦皇岛经济技术开发区管辖。</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>秦皇岛市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>秦皇岛市</p></td>
<td><p>2987605</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>海港区</p></td>
<td><p>765254</p></td>
<td><p>25.61</p></td>
</tr>
<tr class="even">
<td><p>山海关区</p></td>
<td><p>178769</p></td>
<td><p>5.98</p></td>
</tr>
<tr class="odd">
<td><p>北戴河区</p></td>
<td><p>85647</p></td>
<td><p>2.87</p></td>
</tr>
<tr class="even">
<td><p>青龙满族自治县</p></td>
<td><p>496726</p></td>
<td><p>16.63</p></td>
</tr>
<tr class="odd">
<td><p>昌黎县</p></td>
<td><p>559697</p></td>
<td><p>18.73</p></td>
</tr>
<tr class="even">
<td><p>抚宁县</p></td>
<td><p>517073</p></td>
<td><p>17.31</p></td>
</tr>
<tr class="odd">
<td><p>卢龙县</p></td>
<td><p>384439</p></td>
<td><p>12.87</p></td>
</tr>
<tr class="even">
<td><p>注：秦皇岛经济技术开发区的常住人口为105155人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/中华人民共和国第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")2987605人\[8\]，同[第五次全国人口普查相比](../Page/中华人民共和国第五次全国人口普查.md "wikilink")，十年共增加202580人，增长7.27%。年平均增长率为0.70%。其中，男性人口为1516194人，占50.75；女性人口为1471411人，占49.25。总人口性别比（以女性为100）为103.04。0－14岁人口为425790人，占14.25；15－64岁人口为2279262人，占76.29；65岁及以上人口为282553人，占9.46。

## 交通

[Qinhuangdao_Railway_Station_Platforms.jpg](https://zh.wikipedia.org/wiki/File:Qinhuangdao_Railway_Station_Platforms.jpg "fig:Qinhuangdao_Railway_Station_Platforms.jpg")[津秦客專](../Page/津秦客运专线.md "wikilink")\]\]
[秦皇岛火车站为](../Page/秦皇岛站.md "wikilink")[京哈铁路](../Page/京哈铁路.md "wikilink")、[大秦铁路和](../Page/大秦铁路.md "wikilink")[津山铁路的沿线车站](../Page/津山铁路.md "wikilink")，中国首条快速客运专线——[秦沈客运专线起点](../Page/秦沈客运专线.md "wikilink")。该线于2003年10月12日正式运营。2013年12月，[津秦客运专线也投入运营](../Page/津秦客运专线.md "wikilink")。

[秦皇岛北戴河机场位于](../Page/秦皇岛北戴河机场.md "wikilink")[昌黎县](../Page/昌黎县.md "wikilink")，于2016年3月31日正式启用。其有来往[石家庄](../Page/石家庄.md "wikilink")、[成都和](../Page/成都.md "wikilink")[上海的航班](../Page/上海.md "wikilink")。

[秦皇岛港是中国最大的能源输出港口](../Page/秦皇岛港.md "wikilink")，是中国山西煤炭外运的主要港口之一，是天然不冻良港。

## 工业

主要工业有[玻璃](../Page/玻璃.md "wikilink")、[造船](../Page/造船.md "wikilink")、[冶金以及](../Page/冶金.md "wikilink")[汽车配件](../Page/汽车.md "wikilink")。

## 旅遊

[Shanhaiguan.jpg](https://zh.wikipedia.org/wiki/File:Shanhaiguan.jpg "fig:Shanhaiguan.jpg")\]\]
著名的旅遊勝地[北戴河](../Page/北戴河区.md "wikilink")、[南戴河](../Page/南戴河街道.md "wikilink")、[昌黎](../Page/昌黎县.md "wikilink")[黃金海岸和天下第一關](../Page/黃金海岸_\(昌黎\).md "wikilink")[山海关以及青龙满族自治县的祖山自然风景区](../Page/山海关.md "wikilink")。

知名的旅游景点有：[天下第一关](../Page/山海关.md "wikilink")，[老龙头](../Page/老龙头.md "wikilink")，[孟姜女庙](../Page/孟姜女庙.md "wikilink")，[角山](../Page/角山.md "wikilink")，[鸽子窝](../Page/鸽子窝.md "wikilink")，[老虎石](../Page/老虎石.md "wikilink")，[联峰山](../Page/联峰山.md "wikilink")，[海洋馆](../Page/海洋馆.md "wikilink")，[翡翠岛及](../Page/翡翠岛.md "wikilink")2014年12月落成的[中國夢碑](../Page/中國夢碑.md "wikilink")\[9\]等。

## 教育

  - [燕山大学](../Page/燕山大学.md "wikilink")
  - [中国环境管理干部学院](../Page/中国环境管理干部学院.md "wikilink")
  - [河北科技师范学院](../Page/河北科技师范学院.md "wikilink")
  - [东北大学秦皇岛分校](../Page/东北大学秦皇岛分校.md "wikilink")
  - [东北石油大学秦皇岛分校](../Page/东北石油大学.md "wikilink")
  - [河北农业大学秦皇岛校区](../Page/河北农业大学.md "wikilink")（海洋学院）
  - [河北外国语职业学院](../Page/河北外国语职业学院.md "wikilink")
  - [秦皇岛职业技术学院](../Page/秦皇岛职业技术学院.md "wikilink")
  - [河北建材职业技术学院](../Page/河北建材职业技术学院.md "wikilink")
  - [秦皇岛市第一中学](../Page/秦皇岛市第一中学.md "wikilink")
  - [华北理工大学秦皇岛分院](../Page/华北理工大学.md "wikilink")（[秦皇岛市卫生学校](../Page/秦皇岛市卫生学校.md "wikilink")）
  - [秦皇岛华讯计算机学校](../Page/秦皇岛华讯计算机学校.md "wikilink")

## 其他

历史文献记载表明，最早这里曾是文明昌盛的[孤竹古国](../Page/孤竹.md "wikilink")，被[孔子尊为](../Page/孔子.md "wikilink")“古之贤人”的[伯夷](../Page/伯夷.md "wikilink")、[叔齐](../Page/叔齊.md "wikilink")，就生长于此。

## 友好城市

  - [首爾特別市](../Page/首爾.md "wikilink")[江東區](../Page/江東區_\(首爾\).md "wikilink")

  - [馬爾凱](../Page/马尔凯大区.md "wikilink")[佩萨罗](../Page/佩萨罗.md "wikilink")

  - [忠清南道](../Page/忠清南道.md "wikilink")[瑞山市](../Page/瑞山市.md "wikilink")

  - [俄亥俄州](../Page/俄亥俄州.md "wikilink")[托萊多](../Page/托莱多_\(俄亥俄州\).md "wikilink")

  - [夏威夷州](../Page/夏威夷州.md "wikilink")[檀香山](../Page/檀香山_\(夏威夷州\).md "wikilink")

  - [富山縣](../Page/富山縣.md "wikilink")[富山市](../Page/富山市.md "wikilink")

  - [京都府](../Page/京都府.md "wikilink")[宮津市](../Page/宮津市.md "wikilink")

  - [加利西亚](../Page/加利西亚_\(西班牙\).md "wikilink")[卢戈](../Page/卢戈_\(西班牙\).md "wikilink")

  - [北海道](../Page/北海道.md "wikilink")[苫小牧市](../Page/苫小牧市.md "wikilink")

## 参考文献

## 外部链接

  - [秦皇岛新闻网](http://www.qhdxw.com/)
  - [秦皇岛市信息港](http://www.qhd.com.cn/)
  - [秦皇岛市政府门户网站](http://www.qhd.gov.cn/)

## 参见

  - [山海关](../Page/山海关.md "wikilink")
  - [渤海湾](../Page/渤海湾.md "wikilink")
  - [秦始皇](../Page/秦始皇.md "wikilink")
  - [孤竹国](../Page/孤竹国.md "wikilink")

{{-}}

[Category:河北地级市](../Category/河北地级市.md "wikilink")
[秦皇岛](../Category/秦皇岛.md "wikilink")
[Category:京津冀城市群](../Category/京津冀城市群.md "wikilink")
[冀](../Category/中国大城市.md "wikilink")
[Category:渤海沿海城市](../Category/渤海沿海城市.md "wikilink")
[冀](../Category/国家园林城市.md "wikilink")
[5](../Category/全国文明城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.