**秦鳴雷**（），[字](../Page/表字.md "wikilink")**子豫**，[號](../Page/號.md "wikilink")**華峰**，[浙江](../Page/浙江.md "wikilink")[臨海城關人](../Page/臨海.md "wikilink")，[明朝政治人物](../Page/明朝.md "wikilink")、狀元。

## 生平

[嘉靖二十三年](../Page/嘉靖.md "wikilink")（1544年）狀元，原取[吳情](../Page/吳情.md "wikilink")，但其名不為[明世宗所喜](../Page/明世宗.md "wikilink")，並曰“無情岂宜居第一？”，且许多地方正值大旱，加上明世宗夢見雷聲，因此“睹其名大喜”，遂亲擢秦鸣雷为第一。授[翰林院](../Page/翰林院.md "wikilink")[修撰](../Page/修撰.md "wikilink")，參修《國史》和《會典》。[嘉靖二十九年](../Page/嘉靖.md "wikilink")（1550年）升[左春坊](../Page/左春坊.md "wikilink")[谕德](../Page/谕德.md "wikilink")，[嘉靖三十一年](../Page/嘉靖.md "wikilink")（1542年）主[乡试](../Page/乡试.md "wikilink")，晋[南京](../Page/南京.md "wikilink")[国子监祭酒](../Page/国子监祭酒.md "wikilink")。[嘉靖四十四年](../Page/嘉靖.md "wikilink")（1565年）主[会试](../Page/会试.md "wikilink")，取[陈有年](../Page/陈有年.md "wikilink")、[王一鹗](../Page/王一鹗.md "wikilink")、[王锡爵等](../Page/王锡爵.md "wikilink")，后皆成為知名[公卿](../Page/公卿.md "wikilink")、[宰輔](../Page/宰輔.md "wikilink")，[嘉靖四十五年](../Page/嘉靖.md "wikilink")（1566年）改任[南京](../Page/南京.md "wikilink")[吏部左侍郎](../Page/吏部左侍郎.md "wikilink")，校《[永乐大典](../Page/永乐大典.md "wikilink")》。[隆庆五年](../Page/隆庆.md "wikilink")（1571年）官至[南京吏部尚书兼](../Page/南京吏部尚书.md "wikilink")[翰林学士](../Page/翰林学士.md "wikilink")。[万历元年](../Page/万历.md "wikilink")（1573年）正月十八日，[兵科](../Page/兵科.md "wikilink")[给事中](../Page/给事中.md "wikilink")[赵思诚上疏弹劾秦鸣雷](../Page/赵思诚.md "wikilink")“只知嗜酒、不顾廉节”，疏下[吏部](../Page/吏部.md "wikilink")，遂令其[致仕](../Page/致仕.md "wikilink")。居家二十年。[万历二十一年](../Page/万历.md "wikilink")（1593年）卒，葬於临海东乡两头门，著有《谈盗》、《倚云楼稿》、《清风亭》等。

## 墓葬

  - 秦鳴雷墓\[1\]
  - 秦鳴雷狀元第

## 參考文獻

  - [秦鳴雷](http://www.bjdclib.com/subdb/exam/examperson/200908/t20090818_21958.html)
    北京市東城區圖書館

{{-}}

[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:明朝翰林院修撰](../Category/明朝翰林院修撰.md "wikilink")
[Category:明朝南京國子監祭酒](../Category/明朝南京國子監祭酒.md "wikilink")
[Category:南京吏部侍郎](../Category/南京吏部侍郎.md "wikilink")
[Category:南京吏部尚書](../Category/南京吏部尚書.md "wikilink")
[Category:台州人](../Category/台州人.md "wikilink")
[M](../Category/秦姓.md "wikilink")

1.  [明嘉靖状元秦鸣雷墓的前世今生](http://lhnews.zjol.com.cn/lhnews/system/2015/06/25/019473718.shtml)
    临海新闻网