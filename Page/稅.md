**税**（又稱**税赋**、**稅負**、**稅捐**、**租稅**等）是指[政府](../Page/政府.md "wikilink")（或与政府等价的[实体](../Page/实体.md "wikilink")，如[教会](../Page/教会.md "wikilink")、[部落首领](../Page/部落.md "wikilink")）向[纳税人](../Page/纳税人.md "wikilink")（个人或企業法人）强制征收的貨幣或資源。税由[法律强制力保证](../Page/法律.md "wikilink")，抗拒或延遲纳税的人会受到法律惩罚。税收可以分为直接税和间接税，或所得稅和財產稅和消費稅，税收的形式可以是[货币或](../Page/货币.md "wikilink")[劳动](../Page/劳动.md "wikilink")。少數國家完全不用納稅，像[阿拉伯聯合大公國](../Page/阿拉伯聯合大公國.md "wikilink")\[1\]。

依稅法繳納的金額稱為「稅金」。依據不同課稅對象、或是不同法律授權、或是不同納稅人可劃分為不同的分類，稱為税种或稅目。政府依法對民間收取稅收的行為稱為課稅；個人或企業向政府繳納稅金的行為稱為納稅。政府要求納稅人在繳稅期限後繳足應納稅金稱為補稅，政府退還溢收稅金稱為退稅。

## 概述

政府有多种取得收入的方式，常见的有：通过提供服务收取费用，如[公立学校的学费](../Page/公立学校.md "wikilink")；通过[铸币或](../Page/铸币.md "wikilink")[印钞取得收入](../Page/印钞.md "wikilink")；通过民众的自愿捐助（捐款给公立学校和[博物馆](../Page/博物馆.md "wikilink")）；通过收取[罚款](../Page/罚款.md "wikilink")；通过发行[国债](../Page/国债.md "wikilink")；通过[没收私人财产](../Page/没收.md "wikilink")；通过税收。税收是政府或统治者取得收入最重要的方式。在[经济学家看来](../Page/经济学.md "wikilink")，税收是按照一定的规则非惩罚性地强制性地将资源从私人转移到政府的方式。虽然宏观上税收相当于“购买”了政府的服务和[福利](../Page/福利.md "wikilink")，但微观上税收不是交易，纳税人并不会因为纳税得到直接的好处。

在现代税收系统中，税以金钱的形式缴纳。征税和政府开销(政府支出)是现代国家政治和经济事务的核心。税收由专门的政府机构负责徵收和統籌支配，如中国的[国税局](../Page/国家税务总局.md "wikilink")/地税局、美国的[美国国家税务局](../Page/美国国家税务局.md "wikilink")（IRS）、英国的[稅務海關總署](../Page/稅務海關總署.md "wikilink")（HMRC）。欠税的纳税人会受到法律制裁，如罚款、[没收财产](../Page/没收.md "wikilink")、[刑事处罚](../Page/刑事处罚.md "wikilink")。

## 税的目的和效果

政府通过税收得到的金钱可以用于各项事业，如维持政府运作（包括供养统治者和他们的仆人）、[战争](../Page/战争.md "wikilink")、保证法律实施、维持社会秩序、保护财产、基础设施建设、维持经济体系（法偿、保障合同）、[公共服务](../Page/公共服务.md "wikilink")、社会工程、社会福利、偿还国债和[利息](../Page/利息.md "wikilink")。常见的公共服务包括建立[教育](../Page/教育.md "wikilink")、[医疗](../Page/医疗.md "wikilink")、[养老](../Page/养老.md "wikilink")、[失业救济](../Page/失业救济.md "wikilink")、[公共交通](../Page/公共交通.md "wikilink")、能源供应、[供水](../Page/供水.md "wikilink")、废物废水处理系统。殖民地时期宗主国也用税收将附属地的经济货币化。

政府设置不同的税种和税率，对公民的各种活动收税，从而实现资源在不同个体或阶级之间的再分配。历史上很长时期，[贵族要靠下层阶级的税收来供养](../Page/贵族.md "wikilink")。现代文明社会中，政府向工作的人收税以建立社会福利体系，来帮助穷人、残疾人、老人。税收也用于对外援助和军事打击，刺激或调整经济（属于政府的财税政策，包括税收减免、退税、多课税）、调整消费结构、就业结构。

国家的税收往往体现了国家或当权者的价值。通过调整税收种类、起征点和税率，政府有意识地实现了资源再分配。在[民主国家](../Page/民主国家.md "wikilink")，权力机构受民众[选举的制约](../Page/选举.md "wikilink")，因此税收能体现公众的共同愿望。在[极权国家等非民主国家](../Page/极权国家.md "wikilink")，民众没有发言权，因此税收一般只反映当权者的意愿。

征税过程必然会损耗社会资源，包括征税和执法过程中消耗的人力。而且税收会阻碍一些民间交易。有些税种被要求“专款专用”，如对[酒类收税以治疗对酒上瘾的患者](../Page/酒.md "wikilink")。财政部不喜欢这种方式，因为将降低了他们使用资金的自由度。一些经济学家认为这种规定是多余的，因为钱的来源不同，但使用效果都是一样的。

不同经济学派对税收观点不同。[新古典主义经济学家认为税收扭曲了市场](../Page/新古典主义.md "wikilink")，降低了市场效率，但反对者称政府维护了市场秩序，因而提高了市场效率。[古典自由主義经济学家认为除了維護基本的國防和執法](../Page/古典自由主義.md "wikilink")、消防以外，几乎所有的税都不该存在。因为强制征收的过程侵犯了人对自身财务的所有权，因此等同于奴役。[无政府资本主义者持有更强烈的抗税观念](../Page/无政府资本主义.md "wikilink")，认为一切社会服务均应由需要者自愿购买。

税收還有以下的目的和效果：

  - 矯正外部性，例如課徵[能源稅](../Page/能源.md "wikilink")、[污染稅](../Page/污染.md "wikilink")
  - 穩定經濟，減輕景氣循環波動
  - 促進成長，例如獎勵[投資](../Page/投資.md "wikilink")、[儲蓄](../Page/儲蓄.md "wikilink")
  - 寓禁於徵（尤其是國民健康方面），課徵菸品健康捐。丹麥於2011年10月開始徵收[肥胖稅](../Page/肥胖.md "wikilink")。

## 歷史

在西元前2,500年前的[古埃及](../Page/古埃及.md "wikilink")[古王國時期就已經有完整的徵稅系統](../Page/古王國時期.md "wikilink")，是人類歷史中最早的賦稅系統\[2\]。

[路史曾經記載](../Page/路史.md "wikilink")「神農之時，民為賦，二十而一。」是[中國有徵收稅收最早的傳說](../Page/中國.md "wikilink")。[西周時](../Page/西周.md "wikilink")，土地為[貴族所有](../Page/貴族.md "wikilink")，貴族將土地依[井田制劃分為](../Page/井田制.md "wikilink")[公田與](../Page/公田.md "wikilink")[私田](../Page/私田.md "wikilink")，公田的耕種收穫為貴族所有，而私田的收穫則可由耕種的庶民保留。西元前685年，[齊國的](../Page/齊國.md "wikilink")[管仲主張](../Page/管仲.md "wikilink")「相地而衰徵」，是[東周時最早提出土地私有](../Page/東周.md "wikilink")、對農業耕地實物徵稅的政策。《[左傳](../Page/左傳.md "wikilink")》「宣公十五年秋，初稅畝。」則是史書中記載[魯國正式實施](../Page/魯國.md "wikilink")（西元前594年）對耕地徵稅的紀錄。

歷史上對於稅賦的名稱還有很多種，包括[貢納](../Page/貢納.md "wikilink")、[租庸調](../Page/租庸調.md "wikilink")、[厘金都是某種稅賦的稱呼](../Page/厘金.md "wikilink")。然而古時有[徭役這種以付出體力](../Page/徭役.md "wikilink")、勞力的義務，有時在租稅改革時，也會以「力役折錢」的方式，以稅收來取代應該付出勞力的義務，這時雖然是以「徭」或「役」為名，其實仍為稅賦。有時也會有臨時因為[軍費開支較高而課徵新稅](../Page/軍費.md "wikilink")，則會以「某餉」為名，這本質也是稅賦。

## 租稅課徵原則

不同的學者對於租稅應如何課徵，有不同的看法：

### 亞當·斯密

[亞當·斯密提出了以下的](../Page/亞當·斯密.md "wikilink")「租稅四原則」（Four Canons）\[3\]：

  - 公平原則（按國民經濟能力課稅）
  - 確定原則（時間確定、地點確定、方式確定）
  - 便利原則（納稅手續）
  - 經濟原則（最小稽徵費用原則）

### 華格納

不同於[古典自由經濟學派的](../Page/古典自由主义.md "wikilink")[最小政府觀點](../Page/小政府主義.md "wikilink")，[阿道夫·華格納提出較積極的課稅原則以配合政府日益增加的公共開支](../Page/阿道夫·華格納.md "wikilink")，稱為「四大九小原則」\[4\]：

  - 財政政策原則
      - 稅收充分原則
      - 稅收彈性原則
  - 國民經濟原則
      - 選擇正確稅原則（所得、消費、財產）
      - 選擇適當稅目原則
  - 社會正義原則
      - 普遍原則（不分階級、職業、性別、地域）
      - 平等原則（累進、量能）
  - 稅務行政原則
      - 明確原則
      - 便利原則
      - 省費原則

## 賦稅理論

### 拉弗曲线

[Laffer_Curve.png](https://zh.wikipedia.org/wiki/File:Laffer_Curve.png "fig:Laffer_Curve.png")

[拉弗曲线是經濟學中](../Page/拉弗曲线.md "wikilink")，在表示各種可能稅率下，稅率和政府稅收收入之間關係的曲線，由[美国经济学家](../Page/美国.md "wikilink")[阿瑟·拉弗](../Page/阿瑟·拉弗.md "wikilink")（Arthur
Laffer）提出。用來表示稅收收入的彈性（因著稅率變化造成稅收收入的變化），是一個[思想實驗下的結果](../Page/思想實驗.md "wikilink")。先考慮稅率是0%和100%的極端例子，若稅率0%時，很明顯的不會有稅收收入，拉弗曲线假設在稅率100%時，其稅收收入也是零。因納稅人的收入完全用來納稅，理性的納稅人不會有意願工作來賺取收入。若稅率0%及100%時都沒有稅收收入，根據[极值定理](../Page/极值定理.md "wikilink")，中間至少有一點的稅收收入為最大值。拉弗曲线一般會描述為一個符合以下說明的圖：稅率0%，稅收收入零，稅率漸漸變大，稅收收入漸漸增加，到達稅收收入的極值，之後稅收收入隨稅率增加而漸漸減少，最後稅率100%，稅收收入零。

拉弗曲线的一個潛在結論是當稅率高過一定值，提高稅率反而會使稅收收入下降。不過針對特定經濟體的拉弗曲线都只能用來估計，而且有時會有很大的爭議。中有統計各經濟體在最大稅收收入下的稅率，各經濟體的差異很大
\[5\]。

### 最适课税

大部份政府的稅收收入都大於稅制非扭曲時的稅收。理論是經濟學中的一個分支，研究需如何調整稅制，才能使[无谓损失降至最小](../Page/无谓损失.md "wikilink")，或是產生最大的[社会福利](../Page/社会福利.md "wikilink")\[6\]。[拉姆齊問題就是設法使无谓损失降至最小](../Page/拉姆齊問題.md "wikilink")。无谓损失和貨物對需求及供給的[弹性有關](../Page/弹性_\(经济学\).md "wikilink")，因此將最高的稅率放在需求及供給彈性最小的稅率，即可讓无谓损失降至最小。有些經濟學家想整合最适课税及，設法將社会福利用經濟學的方式表現出來。若個體有收入[報酬遞減的情形](../Page/報酬遞減.md "wikilink")，則對社會最理想的稅率是累進稅率。[詹姆斯·莫理斯的最佳收入稅理論是有關最佳累進稅率的理論](../Page/詹姆斯·莫理斯.md "wikilink")。過去這些年有許多政治經濟學家討論過最适课税理論是否有效\[7\]。

### 稅率

稅率常會以百分比來表示，不過在討論稅率時，常會出現二個不同的定義方式：邊際稅率及有效稅率。有效稅率是稅金金額除以應稅收入或交易金額的百分比，而邊際稅率是應稅收入或交易金額若再多一元，對應稅額增加的金額和增加的應稅收入或交易金額（即1元）的百分比。

因為有些稅捐會以累進稅率的方式計算，不同的應稅收入或交易金額會有不同的稅率，因此上述二種稅率會有所不同。例如應稅收入在零至五萬元時，（邊際）稅率5%，應稅收入在五萬至十萬元時，（邊際）稅率10%，應稅收入在十萬元以上時，（邊際）稅率15%，納稅人的應稅收入為$175,000，需支付稅金$18,750元。

  -
    稅金計算
      -
        (0.05\*50,000) + (0.10\*50,000) + (0.15\*75,000) = 18,750

<!-- end list -->

  -
    有效稅率為10.7%:
      -
        18,750/175,000 = 0.107

<!-- end list -->

  -
    邊際稅率為15%

## 各經濟體現行稅制概況

[Unternehmenssteuersätze_EU25.png](https://zh.wikipedia.org/wiki/File:Unternehmenssteuersätze_EU25.png "fig:Unternehmenssteuersätze_EU25.png")各國所得稅率\]\]
[Coin_fiscal_OCDE.png](https://zh.wikipedia.org/wiki/File:Coin_fiscal_OCDE.png "fig:Coin_fiscal_OCDE.png")

### 中華民國 （臺灣）

[中華民國在](../Page/中華民國.md "wikilink")[國民政府撥遷至](../Page/國民政府.md "wikilink")[臺灣後](../Page/臺灣.md "wikilink")，改革由[中華民國大陸時期稅制](../Page/中華民國大陸時期.md "wikilink")，並經1950年、1958年、1968年、1986年四次改革，逐步形成「直接稅類」和「間接稅類」兩大部分包括[所得稅](../Page/所得稅.md "wikilink")、、[營業稅爲主體的現行複合稅體系](../Page/營業稅.md "wikilink")。另外在臺灣、[澎湖](../Page/澎湖.md "wikilink")、[金門](../Page/金門.md "wikilink")、[馬祖等自由地區現行租稅制度依據財政收支劃分法](../Page/馬祖.md "wikilink")，也另可分為[國稅和](../Page/国家税.md "wikilink")[地方稅兩大類](../Page/地方税.md "wikilink")。

#### 以稅負轉嫁分類

  - [直接稅](../Page/直接稅.md "wikilink")：[所得稅](../Page/所得稅.md "wikilink")、[遺産稅](../Page/遺産稅.md "wikilink")、、[土地稅](../Page/土地稅.md "wikilink")、[房屋稅](../Page/房屋稅.md "wikilink")、[契稅等組成](../Page/契稅.md "wikilink")；
  - [間接稅](../Page/間接稅.md "wikilink")：、加值型及非加值型[營業稅](../Page/營業稅.md "wikilink")、[關稅](../Page/关税.md "wikilink")、[證券交易稅](../Page/證券交易稅.md "wikilink")、[印花稅](../Page/印花稅.md "wikilink")、[使用牌照稅](../Page/使用牌照稅.md "wikilink")、。

#### 以課稅主體分類

  - [國稅是屬於中央政府可支用的稅收](../Page/國稅.md "wikilink")，包括有九種：[關稅](../Page/關稅.md "wikilink")、[礦區稅](../Page/礦區稅.md "wikilink")、[所得稅](../Page/所得稅.md "wikilink")、[遺産稅](../Page/遺産稅.md "wikilink")、、、[證券交易稅](../Page/證券交易稅.md "wikilink")、[期貨交易稅](../Page/期貨交易稅.md "wikilink")、[營業稅](../Page/營業稅.md "wikilink")、[菸酒稅](../Page/菸酒稅.md "wikilink")。
  - [地方稅是屬於地方政府可支用的稅收](../Page/地方稅.md "wikilink")，包括直轄市及縣（市）稅，共有八種：[印花稅](../Page/印花稅.md "wikilink")、[使用牌照稅](../Page/使用牌照稅.md "wikilink")、[地價稅](../Page/地價稅.md "wikilink")、[田賦](../Page/田賦.md "wikilink")（於1987年停徵）、[土地增值稅](../Page/土地增值稅.md "wikilink")、[房屋稅](../Page/房屋稅.md "wikilink")、[契稅](../Page/契稅.md "wikilink")、。

### 中華人民共和國

[中華人民共和國自](../Page/中華人民共和國.md "wikilink")1994年税制改革，根据这次[税制改革](../Page/中国1994年税制改革.md "wikilink")，形成共计23个税种\[8\]。

中華人民共和國对于税收的定义是：[政府為了提供公共服務及公共财政](../Page/政府.md "wikilink")，依照[法律規定](../Page/法律.md "wikilink")，对个人或民間企業（法人）无偿征收貨幣或資源的总称。「税制」即指税收制度，由[纳税人](../Page/纳税人.md "wikilink")、[课税对象](../Page/课税对象.md "wikilink")、[税目](../Page/税目.md "wikilink")、[税率](../Page/税率.md "wikilink")、纳税环节、纳税期限、计税依据、减免税和违章处理等要素构成。经过多次调整，目前有18个税种。

#### 按课税对象區分

  - [流转税](../Page/流转税.md "wikilink")：包含[增值税](../Page/增值税.md "wikilink")、[消費稅](../Page/消費稅.md "wikilink")、[關稅](../Page/關稅.md "wikilink")。
  - [所得稅](../Page/所得稅.md "wikilink")：包含[企业所得税和](../Page/企业所得税.md "wikilink")[个人所得税](../Page/个人所得税.md "wikilink")。
  - [財產稅](../Page/財產稅.md "wikilink")：包含[房产税](../Page/房产税.md "wikilink")、[车船使用税](../Page/车船使用税.md "wikilink")、[船舶吨税](../Page/船舶吨税.md "wikilink")
  - [環境影響稅](../Page/環境影響稅.md "wikilink")：[资源税](../Page/资源税.md "wikilink")、[城镇土地使用税](../Page/城镇土地使用税.md "wikilink")、[房产税](../Page/房产税.md "wikilink")、[烟叶税以及](../Page/烟叶税.md "wikilink")[环境保护税](../Page/环境保护税.md "wikilink")。
  - [行為稅](../Page/行為稅.md "wikilink")：[城市维护建设税](../Page/城市维护建设税.md "wikilink")、[耕地占用税](../Page/耕地占用税.md "wikilink")、[车辆购置税](../Page/车辆购置税.md "wikilink")、[契税以及](../Page/契税.md "wikilink")[印花税等等](../Page/印花税.md "wikilink")。

#### 按征收管理体系區分

  - [工商税](../Page/工商税.md "wikilink")
  - [关税](../Page/关税.md "wikilink")
  - [农业税](../Page/农业税.md "wikilink")：[农业税于](../Page/农业税.md "wikilink")2005年12月29日公布废止，另外除[烟叶以外的](../Page/烟草.md "wikilink")[农业特产税](../Page/农业特产税.md "wikilink")，于2006年2月17日全部废止，牧业税全部免征。

#### 按税收收入支配权限區分

  - [中央税](../Page/中央税.md "wikilink")（国家税）
  - [地方税](../Page/地方税.md "wikilink")
  - [中央地方共享税](../Page/中央地方共享税.md "wikilink")

### 香港

[香港的税收](../Page/香港.md "wikilink")，与其它国家、地区的税种相比，具有种类较少且大多为[直接税](../Page/直接税.md "wikilink")，分为四大类即[所得税](../Page/所得税.md "wikilink")、[财产税](../Page/财产税.md "wikilink")、[消费税和](../Page/消费税.md "wikilink")[行为税](../Page/行为税.md "wikilink")。

  - [所得税](../Page/所得税.md "wikilink")：包含[物业税](../Page/物业税.md "wikilink")、[薪俸税](../Page/薪俸税.md "wikilink")、[利得税](../Page/利得税.md "wikilink")
  - 財產稅：[差饷及](../Page/差饷.md "wikilink")[地租](../Page/地租.md "wikilink")（過去有[遺產稅](../Page/遺產稅.md "wikilink")，現已取消）
  - 行为税：[博彩税](../Page/博彩税.md "wikilink")、[飞机乘客离境税](../Page/飞机乘客离境税.md "wikilink")、[印花税](../Page/印花税.md "wikilink")、[酒店房租税](../Page/酒店房租税.md "wikilink")、[汽车首次登记税以及](../Page/汽车首次登记税.md "wikilink")[专利和](../Page/专利.md "wikilink")[特权税](../Page/特权税.md "wikilink")。
  - [消费税和](../Page/消费税.md "wikilink")[关税](../Page/关税.md "wikilink")：香港為[自由港](../Page/自由港.md "wikilink")，絕大多數貨品不徵收關稅，但會對极少数物品，主要是[奢侈品收消费税](../Page/奢侈品.md "wikilink")，分别为[酒精](../Page/酒精.md "wikilink")、[碳氢油](../Page/碳氢油.md "wikilink")（石油制品）、[甲醇](../Page/甲醇.md "wikilink")、[烟草四大类物品](../Page/烟草.md "wikilink")。

### 富人稅

  - 課富人稅國家：[法國曾在](../Page/法國.md "wikilink")2012年提出，後來在2015年取消\[9\]。

## 相關條目

  - [一條鞭法](../Page/一條鞭法.md "wikilink")

  - [火耗歸公](../Page/火耗.md "wikilink")

  - [皇糧國稅](../Page/皇糧國稅.md "wikilink")

  - [均田制](../Page/均田制.md "wikilink")

  -
  -
  - [沒收](../Page/沒收.md "wikilink")

  -
  - [赤字](../Page/赤字.md "wikilink")

  - [国际税务](../Page/国际税务.md "wikilink")

  -
  -
  - [价格上限](../Page/价格上限.md "wikilink")

  - [价格下限](../Page/价格下限.md "wikilink")

  - [革命稅](../Page/革命稅.md "wikilink")

  - [單一稅](../Page/單一稅.md "wikilink")

  -
  - [避稅港](../Page/避稅港.md "wikilink")

  -
  -
  -
  - [避稅及逃稅](../Page/避稅及逃稅.md "wikilink")

  - [美國海外帳戶稅收遵從法](../Page/美國海外帳戶稅收遵從法.md "wikilink")

  - [共同申報準則](../Page/共同申報準則.md "wikilink")

## 参考資料

## 外部链接

  - 中国大陸：[国家税务总局](http://www.chinatax.gov.cn/)
  - 臺灣：[全國法規資料庫](http://law.moj.gov.tw/)
  - 臺灣：[財政部稅務入口網](https://web.archive.org/web/20090330143946/http://www.etax.nat.gov.tw/wSite/dp?mp=1)
  - [財團法人中華民國稅務基金會－兩稅合一](http://www.123.org.tw/docs/p0901.php)

[Category:財稅](../Category/財稅.md "wikilink")

1.

2.  [Taxes in the Ancient
    World](http://www.upenn.edu/almanac/v48/n28/AncientTaxes.html),
    University of Pennsylvania Almanac, *Vol. 48, No. 28, April 2, 2002*

3.

4.
5.

6.

7.

8.  [大陸租稅實務](http://www.opens.com.tw/china/chs04.htm#01)

9.