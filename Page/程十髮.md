**程十髮**（），原名**程潼**，[字](../Page/表字.md "wikilink")**十髮**，[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")[松江人](../Page/松江區.md "wikilink")，[中国](../Page/中国.md "wikilink")[画家](../Page/画家.md "wikilink")。

## 生平

  - 程生於上海[松江縣城外岳庙镇莫家巷](../Page/松江.md "wikilink")，1938年进入[上海美术专科学校学习](../Page/上海美术专科学校.md "wikilink")[绘画](../Page/绘画.md "wikilink")。
  - [中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，程进入华东[人民美术出版社任美术创作员](../Page/人民美术出版社.md "wikilink")，创作了大量的[连环画作品](../Page/连环画.md "wikilink")。
  - 1957年，参与筹建[上海中国画院](../Page/上海中国画院.md "wikilink")，转向专门[国画创作](../Page/国画.md "wikilink")，1984年出任上海中国画院[院长](../Page/院长.md "wikilink")。
  - 2005年8月9日因小[疝气手術住進](../Page/疝气.md "wikilink")[華東醫院](../Page/華東醫院.md "wikilink")。2007年7月18日晚上6點58分因多個器官衰竭病逝於上海華東醫院，終年86歲。
  - 程十髮已婚，有兩子；大子[程助](../Page/程助.md "wikilink")，次子[程多多](../Page/程多多.md "wikilink")。

## 家世

程十-{髮}-1921年4月10日（农历三月初三）诞生于[上海市](../Page/上海.md "wikilink")[松江县城西门外岳庙镇莫家弄](../Page/松江.md "wikilink")。父亲程欣木，取嘉善女子丁织勤为妻，生育一子，取名程潼。小名“美孙”，取祖父程子美之孙意。程氏祖籍[金山區](../Page/金山區_\(上海市\).md "wikilink")[枫泾镇](../Page/枫泾镇.md "wikilink")，祖上世代行医，积善，在地方上历受称道。

古代有一種度量衡單位叫「-{髮}-」和「程」，《說文解字》釋曰：「程，晶也。十-{髮}-為程，十程為分，十分為寸。」「程十-{髮}-」這名字就是依此據姓取名，體現文字上的機趣。

## 参考资料

  - 上海文新網
    [程十髮專題](https://web.archive.org/web/20070927215309/http://www.news365.com.cn/wxzt/wx_wenhua/ghdscsf/)
  - [国画大师程十髮病逝](http://www.zaobao.com/zg/zg070719_502.html)，聯合早報2007年7月19日
  - [追忆程十髮先生二三事](http://news.qq.com/a/20070718/003301.htm)，東方網2007年7月18日
  - [程十髮艺术进程年表](https://web.archive.org/web/20070927215447/http://blog.news365.com.cn/sp1/chengzhu/094308638.shtml),news365.com2007年7月18日

[Category:中华人民共和国画家](../Category/中华人民共和国画家.md "wikilink")
[Category:上海美术专科学校校友](../Category/上海美术专科学校校友.md "wikilink")
[Category:松江人](../Category/松江人.md "wikilink")
[Shi十髮](../Category/程姓.md "wikilink")