**程姓**是[中文的](../Page/中文.md "wikilink")[姓氏之一](../Page/姓氏.md "wikilink")，在[百家姓中排名](../Page/百家姓.md "wikilink")193。据2006年的统计，程姓的人口在[中国大陆排名](../Page/中国大陆.md "wikilink")33位。(韓音
Jeong, Jung)

## 起源

1.  出自风姓，以国为姓，是重和黎的后裔。
2.  以地名为姓。
3.  出自商、周之际的伯符之后。
4.  出自[姬姓](../Page/姬姓.md "wikilink")，是[荀氏后裔](../Page/荀氏.md "wikilink")，以邑为姓而改。\[1\]
5.  别的民族和姓氏改为程姓。

## 名人

### 歷史人物

  - [程郑](../Page/程郑.md "wikilink")：春秋晋国“六卿”之一
  - [程婴](../Page/程婴.md "wikilink")：春秋時期晋国义士
  - [程邈](../Page/程邈.md "wikilink")：秦朝 隶书创造者
  - [程不识](../Page/程不识.md "wikilink")：西汉時期戍边将领
  - [程銀](../Page/程銀.md "wikilink")：東漢末年關中軍閥
  - [程昱](../Page/程昱.md "wikilink")：三國時期曹魏【謀士】
  - [程普](../Page/程普.md "wikilink")：三国时期孙吴【将领】
  - [程憘](../Page/程憘.md "wikilink")：字申伯，西晋广平侯，[征北将军](../Page/征北将军.md "wikilink")、青州刺史
  - [程雄](../Page/程雄.md "wikilink")：字长思，西晋初建，任[宁西将军](../Page/宁西将军.md "wikilink")、陈留太守、豫州刺史，封山阳侯
  - [程猛](../Page/程猛.md "wikilink")：字景陵，[晋惠帝永康中任](../Page/晋惠帝.md "wikilink")[宁远将军](../Page/宁远将军.md "wikilink")、浮氏令
  - [程丰](../Page/程丰.md "wikilink")：字庆云，[晋怀帝永嘉元年中任](../Page/晋怀帝.md "wikilink")[宁朔将军](../Page/宁朔将军.md "wikilink")、魏郡太守，[晋愍帝时除上党太守](../Page/晋愍帝.md "wikilink")，成为上党人。生七子：程荫、程稚等。
  - [程瑕](../Page/程瑕.md "wikilink")：程丰之弟，赵明帝[石勒的](../Page/石勒.md "wikilink")[司徒](../Page/司徒.md "wikilink")。
  - 程稚，赵建平中任凌江将军、白马令，生五子：程绠、程景、程洛、程阳、程周。
  - 程绠，[后燕建兴二年](../Page/后燕.md "wikilink")（387年）任驾部郎中、代郡太守、贵乡侯。
  - 程周，上党太守，[慕容超命为功曹](../Page/慕容超.md "wikilink")。东燕建兴七年（392年）除镇军将军、常山太守。子程蒲，北魏武安令；孙高都令程芒、假魏郡太守程信、假赵郡太守程鞞。
  - 程哲，字子贤，山西省[长治市西南袁家漏村有](../Page/长治市.md "wikilink")《赠代郡太守程哲碑》，大魏天平元年（公元534年）次甲寅十一月庚辰朔三日壬午造讫。子赠陈郡太守程永。
  - [程咬金](../Page/程咬金.md "wikilink")：唐朝将领
  - [程颐](../Page/程颐.md "wikilink")：北宋理学家
  - [程颢](../Page/程颢.md "wikilink")：北宋理学家
  - [程大昌](../Page/程大昌.md "wikilink")：南宋学者
  - [程伟元](../Page/程伟元.md "wikilink")：清朝作家
  - [程德全](../Page/程德全.md "wikilink")：清末民初时期政治家
  - [程长庚](../Page/程长庚.md "wikilink")：清初戏曲家、京剧创始人
  - [程砚秋](../Page/程砚秋.md "wikilink")：中国近代京剧艺术家

### 現代名人

  - [程虹](../Page/程虹.md "wikilink")：中国国务院总理[李克强夫人](../Page/李克强.md "wikilink")，[首都经济贸易大学外语系教授](../Page/首都经济贸易大学.md "wikilink")
  - [程菲](../Page/程菲.md "wikilink")：中国体操运动员
  - [程抱一](../Page/程抱一.md "wikilink")：法国华裔作家、书法家、诗人
  - [程思远](../Page/程思远.md "wikilink")：中国政治人物
  - [程月如](../Page/程月如.md "wikilink")：香港國語電影演員，藝名林黛
  - [程潜](../Page/程潜.md "wikilink")：中国政治人物
  - [程文意](../Page/程文意.md "wikilink")：香港配音員，以《數碼暴龍》系列成名。
  - [程懋筠](../Page/程懋筠.md "wikilink")：中国音乐家
  - [程張迎](../Page/程張迎.md "wikilink")：新界東區議員、前區域市政局議員、香港教育家
  - [程千帆](../Page/程千帆.md "wikilink")：中国文史学家
  - [程章灿](../Page/程章灿.md "wikilink")：中国文史学家
  - [程十髮](../Page/程十髮.md "wikilink")：中國畫家
  - [程建人](../Page/程建人.md "wikilink")：中華民國前外交官、前立法委員
  - [程仁宏](../Page/程仁宏.md "wikilink")：監察委員，消費者文教基金會前任董事長
  - [程小東](../Page/程小東.md "wikilink")：香港著名導演、武術指導
  - [程翔](../Page/程翔.md "wikilink")：香港記者、時事評論員
  - [程可為](../Page/程可為.md "wikilink"):香港無線電視演員
  - [程介南](../Page/程介南.md "wikilink")：香港教育家
  - [程介明](../Page/程介明.md "wikilink")：香港教育家
  - [程振鵬](../Page/程振鵬.md "wikilink")：香港電台DJ
  - [程旭辉](../Page/程旭辉.md "wikilink")：新加坡男演员
  - [程瀟](../Page/程瀟.md "wikilink") : 藝人行于韓國
  - [程维](../Page/程维.md "wikilink") ：滴滴出行创始人
  - [程池](../Page/程池.md "wikilink") ：作曲家，音乐制作人。
  - [程永华](../Page/程永华.md "wikilink")：中国驻日本大使

### 虛構人物

  - [程靈素](../Page/程靈素.md "wikilink")：[飛狐外傳中的女主角](../Page/飛狐外傳.md "wikilink")，[胡斐的義妹](../Page/胡斐.md "wikilink")
  - [程英](../Page/程英.md "wikilink")：[神鵰俠侶人物](../Page/神鵰俠侶.md "wikilink")
  - [程瑤迦](../Page/程瑤迦.md "wikilink")：[射雕英雄傳](../Page/射雕英雄傳.md "wikilink")、[神鵰俠侶人物](../Page/神鵰俠侶.md "wikilink")
  - [程蝶衣](../Page/程蝶衣.md "wikilink") :
    [霸王別姬人物](../Page/霸王別姬.md "wikilink")
  - [程能文](../Page/程能文.md "wikilink") : 安徽安庆
  - [程子榮](../Page/程子榮.md "wikilink") : 負能量魔人
  - 程美： 中國電視劇[浪花一朵朵的女三主角](../Page/浪花一朵朵.md "wikilink")

## 注释

[C程](../Category/漢字姓氏.md "wikilink") [\*](../Category/程姓.md "wikilink")

1.