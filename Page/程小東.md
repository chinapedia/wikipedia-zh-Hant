**程小東**（），原名**程冬兒**，原籍[安徽省](../Page/安徽省.md "wikilink")[壽縣](../Page/壽縣.md "wikilink")，香港著名[導演和](../Page/導演.md "wikilink")[武术指导](../Page/武术指导.md "wikilink")。

## 生平

程小東乃前[香港](../Page/香港.md "wikilink")[導演](../Page/導演.md "wikilink")[程剛的](../Page/程剛.md "wikilink")[兒子](../Page/兒子.md "wikilink")，香港東方戲劇學院出身，曾熟習[中國武術](../Page/中國武術.md "wikilink")[北派](../Page/中國武術門派.md "wikilink")。他早於17歲時已出任[龍虎武師](../Page/龍虎武師.md "wikilink")，三年後在电视台任[武術指導](../Page/武術指導.md "wikilink")，曾任職[麗的電視](../Page/麗的電視.md "wikilink")、[佳藝電視](../Page/佳藝電視.md "wikilink")、[無綫電視等](../Page/無綫電視.md "wikilink")。

1972年進軍[香港電影](../Page/香港電影.md "wikilink")，曾為多部影片設計動作，包括《[十四女英豪](../Page/十四女英豪.md "wikilink")》、《[第一类型危险](../Page/第一类型危险.md "wikilink")》、《[新蜀山剑侠](../Page/新蜀山剑侠.md "wikilink")》、《[刀马旦](../Page/刀马旦.md "wikilink")》、《[英雄本色](../Page/英雄本色.md "wikilink")》。他早於1983年已執導電影，1987年憑《[倩女幽魂](../Page/倩女幽魂_\(1987年電影\).md "wikilink")》获1987年法国第十六届科幻影展评委会特别大獎。同年他憑《[奇缘](../Page/奇缘.md "wikilink")》获[香港電影金像奖最佳武术指导](../Page/香港電影金像奖.md "wikilink")。1989年，[程小东偕同](../Page/程小东.md "wikilink")[香港著名媒体人](../Page/香港.md "wikilink")[甘国亮](../Page/甘国亮.md "wikilink")、[导演](../Page/导演.md "wikilink")[徐克等团队到](../Page/徐克.md "wikilink")[北京联同](../Page/北京.md "wikilink")[张艺谋和](../Page/张艺谋.md "wikilink")[巩俐拍摄电影](../Page/巩俐.md "wikilink")《[古今大战秦俑情](../Page/古今大战秦俑情.md "wikilink")》，这是[香港电影人较早期与](../Page/香港电影人.md "wikilink")[内地电影人的合作](../Page/内地电影人.md "wikilink")，程小东更憑《古今大战秦俑情》獲得[香港金像奖](../Page/香港金像奖.md "wikilink")[最佳武术指导提名](../Page/最佳武术指导.md "wikilink")。

程小东的动作设计注重气氛营造，能製造出一種气魄宏大、凌厉壮观的动作场面。其设计风格飘逸、潇洒，善于处理高来高去、凌空蹈虚的动作，因之他製作的作品以古裝片最為成功。

## 獎項

最佳動作指導獎

  - [金馬獎](../Page/金馬獎.md "wikilink") (1992)
  - [香港電影金像獎](../Page/香港電影金像獎.md "wikilink") (1987, 1989)

## 影视作品

### 导演作品

| 上映年份 | 片名                                             | 主演                                                                                                                | 备注                                         |
| ---- | ---------------------------------------------- | ----------------------------------------------------------------------------------------------------------------- | ------------------------------------------ |
| 1983 | 《生死决》                                          |                                                                                                                   |                                            |
| 1986 | 《奇缘》                                           |                                                                                                                   | 第6屆香港金像奖最佳动作指导奖                            |
| 1987 | [倩女幽魂](../Page/倩女幽魂_\(1987年電影\).md "wikilink") |                                                                                                                   |                                            |
| 1989 | [秦俑](../Page/秦俑.md "wikilink")                 |                                                                                                                   |                                            |
| 1990 | [倩女幽魂2之人間道](../Page/倩女幽魂2之人間道.md "wikilink")   |                                                                                                                   |                                            |
| 1991 | [倩女幽魂Ⅲ：道道道](../Page/倩女幽魂Ⅲ：道道道.md "wikilink")   |                                                                                                                   |                                            |
| 1992 | [笑傲江湖II東方不敗](../Page/笑傲江湖II東方不敗.md "wikilink") |                                                                                                                   |                                            |
| 1992 | [鹿鼎記](../Page/鹿鼎記_\(1992年電影\).md "wikilink")   |                                                                                                                   | 与[王晶联合导演](../Page/王晶_\(導演\).md "wikilink") |
| 1993 | [現代豪俠傳](../Page/現代豪俠傳.md "wikilink")           |                                                                                                                   | 与[杜琪峰联合导演](../Page/杜琪峰.md "wikilink")      |
| 1996 | [冒險王](../Page/冒險王_\(電影\).md "wikilink")        | [李連杰](../Page/李連杰.md "wikilink")、[金城武](../Page/金城武.md "wikilink")                                                 |                                            |
| 2002 | [赤裸特工](../Page/赤裸特工.md "wikilink")             | [吳彥祖](../Page/吳彥祖.md "wikilink")、[Maggie Q](../Page/Maggie_Q.md "wikilink")、[安雅](../Page/安雅_\(演員\).md "wikilink") |                                            |
| 2008 | [江山美人](../Page/江山美人_\(2008年電影\).md "wikilink") |                                                                                                                   |                                            |
| 2011 | [白蛇傳說](../Page/白蛇傳說.md "wikilink")             |                                                                                                                   |                                            |

### 動作指導作品

| 上映年份 | 片名                                                | 導演                                | 備注                                                |
| ---- | ------------------------------------------------- | --------------------------------- | ------------------------------------------------- |
| 1983 | [射雕英雄傳](../Page/射鵰英雄傳_\(1983年電視劇\).md "wikilink") |                                   |                                                   |
|      | [神雕俠侶](../Page/神鵰俠侶_\(1983年電視劇\).md "wikilink")   |                                   |                                                   |
|      | 俠盜風流                                              |                                   |                                                   |
|      | 天蠶變                                               |                                   |                                                   |
|      | 大內群英                                              |                                   |                                                   |
|      | 天龍訣                                               |                                   |                                                   |
|      | 沈勝衣                                               |                                   |                                                   |
|      | 浣花洗劍錄                                             |                                   |                                                   |
| 1986 | [刀馬旦](../Page/刀馬旦_\(電影\).md "wikilink")           | [徐克](../Page/徐克.md "wikilink")    |                                                   |
| 1990 | [笑傲江湖](../Page/笑傲江湖_\(1990年電影\).md "wikilink")    | [胡金銓](../Page/胡金銓.md "wikilink")  | 第10屆香港電影金獎最佳武術指導獎                                 |
| 1991 | [帶子洪郎](../Page/帶子洪郎.md "wikilink")                | [陳木勝](../Page/陳木勝.md "wikilink")  |                                                   |
| 1992 | [審死官](../Page/審死官_\(1992年電影\).md "wikilink")      | [杜琪峰](../Page/杜琪峰.md "wikilink")  |                                                   |
| 1992 | [新龍門客棧](../Page/新龍門客棧.md "wikilink")              | [李惠民](../Page/李惠民.md "wikilink")  | 第29屆金馬獎最佳武術指導獎（與[元彬分享](../Page/元彬.md "wikilink")） |
| 1992 | [鹿鼎記2神龍教](../Page/鹿鼎記2神龍教.md "wikilink")          | 王晶                                |                                                   |
| 1993 | [東方三俠](../Page/東方三俠.md "wikilink")                | [杜琪峰](../Page/杜琪峰.md "wikilink")  |                                                   |
| 1995 | [大話西游之月光寶盒](../Page/大話西游之月光寶盒.md "wikilink")      | [劉鎮偉](../Page/劉鎮偉.md "wikilink")  |                                                   |
| 1995 | [西遊記大結局之仙履奇緣](../Page/西遊記大結局之仙履奇緣.md "wikilink")  | 劉鎮偉                               |                                                   |
| 2002 | [英雄](../Page/英雄_\(電影\).md "wikilink")             | [張藝謀](../Page/張藝謀.md "wikilink")  | 第22屆香港金像獎最佳動作指導獎                                  |
| 2004 | [十面埋伏](../Page/十面埋伏_\(電影\).md "wikilink")         | 張藝謀                               |                                                   |
| 2005 | [中華英雄](../Page/中華英雄_\(2005年電視劇\).md "wikilink")   |                                   |                                                   |
| 2006 | [滿城盡帶黃金甲](../Page/滿城盡帶黃金甲.md "wikilink")          | 張藝謀                               |                                                   |
| 2007 | [投名狀](../Page/投名狀.md "wikilink")                  | [陳可辛](../Page/陳可辛.md "wikilink")  |                                                   |
| 2009 | [追捕](../Page/追捕_\(2010年電視劇\).md "wikilink")       | [林龍](../Page/林龍.md "wikilink")、郝琳 |                                                   |
| 2010 | [未來警察](../Page/未來警察.md "wikilink")                | 王晶                                |                                                   |
| 2010 | [大笑江湖](../Page/大笑江湖.md "wikilink")                | [朱延平](../Page/朱延平.md "wikilink")  |                                                   |

## 外部链接

  -
  -
  - [程小東的影視作品](http://www.dianying.com/ft/person/ChengXiaodong)

  - [Kung Fu Cinema
    interview](http://www.kungfucinema.com/articles/2004-06-22-01.htm)

  - [HK cinemagic entry](http://www.hkcinemagic.com/en/people.asp?id=32)

  -
  -
  -
  -
  -
[category:香港动作指导](../Page/category:香港动作指导.md "wikilink")

[C](../Category/寿县人.md "wikilink") [C](../Category/香港電影導演.md "wikilink")
[Category:香港電影金像獎最佳動作設計得主](../Category/香港電影金像獎最佳動作設計得主.md "wikilink")
[C](../Category/金馬獎技術獎項獲獎者.md "wikilink")
[S](../Category/程姓.md "wikilink")
[Category:香港电影金像奖获得者](../Category/香港电影金像奖获得者.md "wikilink")