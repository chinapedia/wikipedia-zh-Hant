**程德全**（），[字](../Page/表字.md "wikilink")**纯如**，[号](../Page/号.md "wikilink")**雪楼**，法名**寂照**。[四川省](../Page/四川省.md "wikilink")[夔州府](../Page/夔州府.md "wikilink")[云阳县人](../Page/云阳县.md "wikilink")。[清朝末年曾任](../Page/清朝.md "wikilink")[黑龙江将军](../Page/黑龙江将军.md "wikilink")，[中华民国初期任](../Page/中华民国.md "wikilink")[江苏都督](../Page/江苏都督.md "wikilink")、[南京临时政府内务总长](../Page/南京临时政府.md "wikilink")。后出家为[佛教僧人](../Page/佛教.md "wikilink")。

## 生平

[Cheng_Dequan3.jpg](https://zh.wikipedia.org/wiki/File:Cheng_Dequan3.jpg "fig:Cheng_Dequan3.jpg")
程德全是[廪贡生出身](../Page/廪贡生.md "wikilink")。光緒十六年（1890年），入[国子监学习](../Page/国子监.md "wikilink")。1891年，他到黑龙江[瑷辉入](../Page/瑷辉.md "wikilink")[黑龙江副都统](../Page/黑龙江副都统.md "wikilink")[文全幕](../Page/文全.md "wikilink")。后又入[黑龙江将军](../Page/黑龙江将军.md "wikilink")[依克唐阿幕](../Page/依克唐阿.md "wikilink")。1894年，被保奏以[知县发派](../Page/知县.md "wikilink")[安徽](../Page/安徽.md "wikilink")。[光绪二十四年](../Page/光绪.md "wikilink")（1898年），经[黑龙江将军](../Page/黑龙江将军.md "wikilink")[寿山奏调重回](../Page/壽山_\(清朝\).md "wikilink")[黑龙江省](../Page/黑龙江省.md "wikilink")，後任黑龙江将军衙门营务处总办。[庚子事变](../Page/庚子事变.md "wikilink")，[俄军入侵](../Page/俄罗斯帝国.md "wikilink")[齐齐哈尔](../Page/齐齐哈尔.md "wikilink")，寿山自杀，程德全曾单骑入俄营同[额林干夫议和](../Page/额林干夫.md "wikilink")，被俄军扣押，后获释。光绪二十九年（1903年），署[齐齐哈尔副都统](../Page/齐齐哈尔副都统.md "wikilink")，光绪三十一年（1905年）署理[黑龙江将军](../Page/黑龙江将军.md "wikilink")，开[东三省汉人任将军之首例](../Page/东三省.md "wikilink")。1907年，署理[黑龙江巡抚](../Page/黑龙江巡抚.md "wikilink")。在黑龙江期间，他与[达桂奏准将黑龙江省土地全部开禁](../Page/达桂.md "wikilink")。光绪三十四年（1908年），因病开缺。

[宣统元年](../Page/宣统.md "wikilink")（1909年）复被起用，任[奉天巡抚](../Page/奉天巡抚.md "wikilink")。1910年，改任[江苏巡抚](../Page/江苏巡抚.md "wikilink")。
[Cheng_Dequan2.jpg](https://zh.wikipedia.org/wiki/File:Cheng_Dequan2.jpg "fig:Cheng_Dequan2.jpg")
[辛亥革命爆发后](../Page/辛亥革命.md "wikilink")，程德全在江苏省城[苏州](../Page/苏州.md "wikilink")，经过[张一麐劝说](../Page/张一麐.md "wikilink")，于1911年11月5日宣布江苏[独立](../Page/独立.md "wikilink")，自任[江苏都督](../Page/江苏都督.md "wikilink")，成为第一名参加革命的清朝疆吏。[民国](../Page/民国纪年.md "wikilink")[元年](../Page/元年.md "wikilink")（1912年）1月3日，[南京临时政府成立](../Page/南京临时政府.md "wikilink")，他被任命为[南京临时政府内务总长](../Page/南京临时政府.md "wikilink")。同日，他和脱离了[中国同盟会的](../Page/中国同盟会.md "wikilink")[章炳麟](../Page/章炳麟.md "wikilink")（章太炎）、[張謇共同组织了](../Page/張謇.md "wikilink")[中华民国联合会](../Page/中华民国联合会.md "wikilink")（后来改为[统一党](../Page/统一党_\(中国\).md "wikilink")）。1912年4月，[袁世凯任](../Page/袁世凯.md "wikilink")[中华民国临时大总统后](../Page/中华民国临时大总统.md "wikilink")，他被任命为江苏都督。同年5月，统一党和[民社等合组](../Page/民社.md "wikilink")[共和党](../Page/共和党_\(中国\).md "wikilink")，程德全因与章太炎不和而离党。

1913年3月，他曾亲赴上海处理“[宋教仁被刺案](../Page/宋教仁.md "wikilink")”，主张按法律程序解决。1913年7月[二次革命中](../Page/二次革命.md "wikilink")，他宣布江苏独立，并被推举为南军司令，但他弃职逃遁[上海](../Page/上海.md "wikilink")。同年9月，[二次革命失败](../Page/二次革命.md "wikilink")，他辞去江苏都督。此后他住在上海，以礼佛为事。1917年退出政界。1926年，受戒于[常州天宁寺](../Page/常州天宁寺.md "wikilink")，[法名](../Page/法名.md "wikilink")**寂照**。1930年5月29日于[上海圆寂](../Page/上海.md "wikilink")。享年71岁。

## 遗迹

今[苏州城外](../Page/苏州.md "wikilink")[寒山寺](../Page/寒山寺.md "wikilink")，有“古寒山寺”四个大字为其墨迹。[齐齐哈尔市](../Page/齐齐哈尔市.md "wikilink")[龙沙公园内建有](../Page/龙沙公园.md "wikilink")《清云阳程公以身御难碑》。

## 著作

著有《程中丞奏稿》、《抚江文牍》等。

## 軼聞

1945年1月，銀幣收藏家吳詩錦發表文章認為[袁世凱像中華民國開國紀念幣上面的頭像實為程德全](../Page/袁世凱.md "wikilink")，引起半個多世紀爭論\[1\]。

## 参考文献

  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>

{{-}}

<table>
<thead>
<tr class="header">
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>　（<a href="../Page/北京政府.md" title="wikilink">北京政府</a>） </p></td>
</tr>
</tbody>
</table>

[Category:黑龍江將軍](../Category/黑龍江將軍.md "wikilink")
[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:中華民國佛教出家众](../Category/中華民國佛教出家众.md "wikilink")
[Category:辛亥革命人物](../Category/辛亥革命人物.md "wikilink")
[Category:云阳人](../Category/云阳人.md "wikilink")
[D德](../Category/程姓.md "wikilink")
[Category:清朝奉天巡撫](../Category/清朝奉天巡撫.md "wikilink")

1.  [大胡子开国纪念币之考](http://www.cguardian.com/images/Portals/0/pdf/2010/2010%E5%B9%B4%E7%AC%AC2%E6%9C%9F/%E5%A4%A7%E8%83%A1%E5%AD%90%E5%BC%80%E5%9B%BD%E7%BA%AA%E5%BF%B5%E5%B8%81%E4%B9%8B%E8%80%83.pdf)，鉴容，嘉德通讯
    2012年第2期，170-171頁