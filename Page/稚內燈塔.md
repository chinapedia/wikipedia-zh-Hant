{{Infobox_lighthouse |lighthouse_name= 稚內燈塔 |image =
[Wakkanai_lighrhouse.JPG](https://zh.wikipedia.org/wiki/File:Wakkanai_lighrhouse.JPG "fig:Wakkanai_lighrhouse.JPG")
|location =
[北海道](../Page/北海道.md "wikilink")[稚内市野寒布](../Page/稚内市.md "wikilink")
|established = 1900年 |type = 有人駐守 |status = 仍在營運|dominion =
[日本海上保安廳](../Page/日本海上保安廳.md "wikilink") |p.s. =
**稚内燈塔**為[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[稚內市](../Page/稚內市.md "wikilink")[野寒布岬建立之](../Page/野寒布岬.md "wikilink")[燈塔](../Page/燈塔.md "wikilink")。與[宗谷岬燈塔及對岸國境](../Page/宗谷岬燈塔.md "wikilink")[樺太的燈塔是同様功能](../Page/庫頁島.md "wikilink")，是一座守護著為[國際海峽](../Page/國際海峽.md "wikilink")[宗谷海峡航路上的重要燈塔](../Page/拉彼魯茲海峽.md "wikilink")。進一步，被選為「[日本燈塔50選](../Page/日本燈塔50選.md "wikilink")」之一。由燈塔南延到西側的[海岸線](../Page/海岸線.md "wikilink")，被指定劃入[利尻禮文佐呂別國立公園範圍](../Page/利尻禮文佐呂別國立公園.md "wikilink")。從宗谷海峽的方向可以望見[利尻島](../Page/利尻島.md "wikilink")・[禮文島等風景名勝之地](../Page/禮文島.md "wikilink")。

## 歷史

  -
    1900年（[明治](../Page/明治.md "wikilink")33年）12月10日初代稚内燈塔點燈。（現在的[稚內分屯地内](../Page/稚內分屯地.md "wikilink")）
    1966年（昭和41年）1月10日第二代稚内燈塔移轉到現在的位置。

## 燈塔結構與行政諸元

### 燈塔結構諸元

  - 航路標識番號：0510 \[F6109\]。
  - 塔身塗色・構造：塔身在白色基質上塗6條赤横帯，塔形為水泥混凝土造。
  - 燈器：LB-90型燈器。
  - [燈質](../Page/燈質.md "wikilink")：群閃白光（毎14秒內、隔6秒(暗)閃2次(明)）。
  - 實効[光度](../Page/光度.md "wikilink")：32萬[cd](../Page/坎德拉.md "wikilink")。
  - [光程](../Page/光程.md "wikilink")：18.0[浬](../Page/浬.md "wikilink")＜約33[km](../Page/km.md "wikilink")＞。
  - [明弧](../Page/明弧.md "wikilink")：從6度至315度。
  - 塔高＜地上～塔頂＞：42.7[m](../Page/m.md "wikilink")。（全[日本排名第](../Page/日本.md "wikilink")2位）
  - 燈高＜平均海面～燈火＞：42.1m。
  - 燈塔座標：[北緯](../Page/緯度.md "wikilink")45[度](../Page/度.md "wikilink")26分58.51秒、[東經](../Page/經度.md "wikilink")141度38分42.82秒。

### 燈塔行政諸元

  - 管轄機關：[日本海上保安廳](../Page/日本海上保安廳.md "wikilink")[第1管區海上保安本部](../Page/第1管區海上保安本部.md "wikilink")。
  - 初點燈：1900年（[明治](../Page/明治.md "wikilink")33年）12月10日。
  - 所在地：[北海道](../Page/北海道.md "wikilink")[稚內市野寒布](../Page/稚內市.md "wikilink")。

## 附屬施設

  - [霧信號所](../Page/霧信號所.md "wikilink")（隔膜式霧笛(Diaphragm horn)：毎30秒吹鳴2回）

## 交通路線

  - 從[稚內港旁](../Page/稚內港.md "wikilink")[JR稚內站前的汽車招呼站](../Page/稚內站.md "wikilink")（由[JR稚內站徒歩走](../Page/稚內站.md "wikilink")5分鐘）出發，搭乘[宗谷汽車](../Page/宗谷汽車.md "wikilink")1系統市内線往「野寒布」方向。在「野寒布」站下車（所需要時間約15分鐘），下車後從汽車招呼站徒歩走5分鐘即可抵達稚内燈塔（位於「[稚內市立野寒布寒流水族館](../Page/稚內市立野寒布寒流水族館.md "wikilink")」與「[稚內市青少年科學館](../Page/稚內市青少年科學館.md "wikilink")」旁）。

## 關連項目

  - [燈塔](../Page/燈塔.md "wikilink")
  - [日本燈塔50選](../Page/日本燈塔50選.md "wikilink")
  - [野寒布岬](../Page/野寒布岬.md "wikilink")
  - [宗谷海峽](../Page/拉彼魯茲海峽.md "wikilink")

## 參考文獻

<div class="references-small">

1.  週刊紙『海上保安新聞』 財団法人海上保安協会 （木曜日発行、正確には月4回刊）
2.  季刊誌『かいほジャーナル』　財団法人海上保安協会 （1月・4月・7月・10月発行）

</div>

## 外部連結

  - [稚内海上保安部](http://www.kaiho.mlit.go.jp/01kanku/wakkanai/)

  - [第一管区海上保安本部 Japan 1st Regional Coast Guard
    Headquarters](http://www.kaiho.mlit.go.jp/01kanku/)

  - [ノシャップ岬と稚内灯台（北海道）](https://web.archive.org/web/20080725114518/http://gauss0jp.hp.infoseek.co.jp/wakkanai.htm)

  - [灯台用語集Ｆ](http://gauss0jp.hp.infoseek.co.jp/ftyougo0.htm)

  - [「灯台のことなら」　社団法人　燈光会](http://www.tokokai.org/)

[category:日本燈塔](../Page/category:日本燈塔.md "wikilink")

[Category:北海道建築物](../Category/北海道建築物.md "wikilink")
[Category:稚内市](../Category/稚内市.md "wikilink")