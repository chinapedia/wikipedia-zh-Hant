**穆罕默德·安瓦尔·萨达特**（，），前[埃及總統](../Page/埃及總統.md "wikilink")。

萨达特出生於[米努夫省迈特阿布库姆村](../Page/米努夫省.md "wikilink")（Mit Abu
Al-Kum），有13个兄弟姐妹。1936年進入[开罗军事学院学习](../Page/开罗.md "wikilink")。1939年秘密建立“自由军官”小组，因从事反[英活动](../Page/英國.md "wikilink")，曾两次被捕入狱。1952年7月23日，参加[纳赛尔领导推翻](../Page/纳赛尔.md "wikilink")[法鲁克王朝的](../Page/法鲁克王朝.md "wikilink")[七月革命](../Page/埃及七月革命.md "wikilink")。[埃及共和国成立后](../Page/埃及共和国.md "wikilink")，曾於1964年至1966年及1969年到1970年間，两次任職[副总统](../Page/副总统.md "wikilink")。

1970年，纳赛尔逝世，萨达特继任总统。1973年10月，[埃及和](../Page/埃及.md "wikilink")[叙利亚一起发动了](../Page/叙利亚.md "wikilink")[第四次中东战争](../Page/第四次中东战争.md "wikilink")（又称[十月战争](../Page/十月战争.md "wikilink")），摧毁了[以色列的巴列夫防线](../Page/以色列.md "wikilink")。1978年9月，在[美国参与下](../Page/美国.md "wikilink")，萨达特與[以色列总理](../Page/以色列总理.md "wikilink")[贝京在](../Page/梅纳赫姆·贝京.md "wikilink")[華盛頓特區签署](../Page/華盛頓特區.md "wikilink")[戴维营協議](../Page/戴维营協議.md "wikilink")，因而获得[诺贝尔和平奖](../Page/诺贝尔和平奖.md "wikilink")。

[Nasrcity.jpg](https://zh.wikipedia.org/wiki/File:Nasrcity.jpg "fig:Nasrcity.jpg")
1981年10月6日，萨达特在开罗举行庆祝[贖罪日戰爭胜利八周年的](../Page/贖罪日戰爭.md "wikilink")[阅兵儀式上](../Page/阅兵.md "wikilink")身亡。他的國葬於10月10日舉行，遺體安葬於位於他遇刺地方對面的**無名英雄紀念碑**；此處及當年遇刺的檢閱台，後來成為遊客必到的埃及旅遊景點。

沙達特繼任總統之初經常睡不著，三更半夜仍在寢室走來走去不睡；沙達特的妻子問「你怎麼了」，沙達特回答「我想起納赛尔這二十年所發生的事睡不著」，沙達特的妻子告訴他「忘掉納赛尔吧，想想你自己的事」。日後台灣發生[二月政爭時](../Page/二月政爭.md "wikilink")，[中華民國總統](../Page/中華民國總統.md "wikilink")[李登輝精神壓力大而在許多夜晚都睡不著](../Page/李登輝.md "wikilink")，即以沙達特的處境比擬當時他的心境。\[1\]

## 文獻

  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [Anwar Sadat Official
    Page](http://www.facebook.com/Muhammed.Anwar.Elsadat)

  - [Official website](http://www.anwarsadat.org/)

  - [محمد انور السادات , الصفحه الرئيسية](http://www.anwarelsadat.com/)

  - [Bibliotheca Alexandrina, Front Page](http://sadat.bibalex.org/)

  - [El-Sadat.info](http://www.elsadat.info/)

  - [Anwar Sadat Chair for Peace and
    Development](http://www.sadat.umd.edu/) at the [University of
    Maryland](../Page/University_of_Maryland.md "wikilink")

  - [*Remarks at the Presentation Ceremony for the Presidential Medal of
    Freedom -
    March 26, 1984*](http://www.reagan.utexas.edu/archives/speeches/1984/32684a.htm)

  -
  -
  -
  -
  - [Video of Sadat's
    assassination](http://www.youtube.com/watch?v=nwQL3N57TQE)

  - [Al Jazeera Video detailed coverage of the cause of Sadat's
    Assassination](https://web.archive.org/web/20120217200916/http://video.google.ca/videoplay?docid=-5277109171595100145&q=sadat%2F)


  - [Free Egyptians Point of View About Sadat's
    Assassination](https://web.archive.org/web/20050924111107/http://www.angelfire.com/art3/eg05/killingSadat.htm)


  -
  -
[Category:诺贝尔和平奖获得者](../Category/诺贝尔和平奖获得者.md "wikilink")
[Category:埃及諾貝爾獎獲得者](../Category/埃及諾貝爾獎獲得者.md "wikilink")
[Category:埃及总统](../Category/埃及总统.md "wikilink")
[Category:埃及總理](../Category/埃及總理.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")
[Category:贖罪日戰爭人物](../Category/贖罪日戰爭人物.md "wikilink")
[Category:埃及反共主義者](../Category/埃及反共主義者.md "wikilink")
[Category:與納粹德國合作的中東人](../Category/與納粹德國合作的中東人.md "wikilink")
[Category:遇刺身亡的总统](../Category/遇刺身亡的总统.md "wikilink")
[Category:埃及遇刺身亡者](../Category/埃及遇刺身亡者.md "wikilink")
[Category:在埃及被謀殺身亡者](../Category/在埃及被謀殺身亡者.md "wikilink")
[Category:埃及穆斯林](../Category/埃及穆斯林.md "wikilink")
[Category:蘇丹裔埃及人](../Category/蘇丹裔埃及人.md "wikilink")
[Category:時代年度風雲人物](../Category/時代年度風雲人物.md "wikilink")
[Category:軍人出身的總統](../Category/軍人出身的總統.md "wikilink")
[Category:亞歷山大大學校友](../Category/亞歷山大大學校友.md "wikilink")

1.  [鄒景雯採訪](../Page/鄒景雯.md "wikilink")，《李登輝執政告白實錄》，臺北：印刻出版，2001年5月。頁76-77。