**穆罕默德·纳吉布拉·阿赫马德扎伊**（，），[阿富汗政治家](../Page/阿富汗.md "wikilink")、[共产主义者](../Page/共产主义.md "wikilink")，出生在[喀布尔](../Page/喀布尔.md "wikilink")。他在1986年至1992年期间担任[苏联傀儡政权](../Page/苏联.md "wikilink")[阿富汗民主共和国最高领导人](../Page/阿富汗民主共和国.md "wikilink")。1992年，反政府军攻入喀布尔，纳吉布拉被迫下台，躲藏在联合国驻喀布尔办事处避难。1996年塔利班攻入喀布尔之后，将纳吉布拉酷刑处死。

## 生平

纳吉布拉是[普什图人](../Page/普什图人.md "wikilink")，出生在[喀布尔](../Page/喀布尔.md "wikilink")，其祖父是部族长老，父亲是驻[巴基斯坦](../Page/巴基斯坦.md "wikilink")[白沙瓦的领事](../Page/白沙瓦.md "wikilink")。幼年时随父亲在白沙瓦居住12年，50年代回国上学。1965年加入[阿富汗人民民主党](../Page/阿富汗人民民主党.md "wikilink")。1975年毕业于[喀布尔大学医学院](../Page/喀布尔大学.md "wikilink")。1977年任党中央委员，成为该党的干部。翌年阿富汗人民民主党发动政变，推翻总统[穆罕默德·达乌德汗](../Page/穆罕默德·达乌德汗.md "wikilink")，他成为领导人之一，但由于在政治斗争中站错队伍而失势，贬为驻[伊朗大使](../Page/伊朗.md "wikilink")。他短暂地在[德黑兰供职之后](../Page/德黑兰.md "wikilink")，便流亡欧洲。

1979年，[苏联](../Page/苏联.md "wikilink")[侵略阿富汗](../Page/阿富汗战争_\(1979年\).md "wikilink")，杀害总统[哈菲佐拉·阿明](../Page/哈菲佐拉·阿明.md "wikilink")。纳吉布拉回到阿富汗，被苏联人扶植为国家情报总局局长（[KHAD](../Page/KHAD.md "wikilink")），掌管国家安全机构。在此期间，他奉苏联之命，模仿苏联[克格勃的模式](../Page/克格勃.md "wikilink")，逮捕、审问和杀害了数万阿富汗人民，血债累累，深受阿富汗人民痛恨，呼为“屠夫”。1981年升官任中央政治局委员。1983年晋升为中将。1985年，中央书记[巴布拉克·卡尔迈勒失势](../Page/巴布拉克·卡尔迈勒.md "wikilink")，他继任中央书记。1986年1月，任革命委员会主席团委员，5月出任人民民主党中央委员会总书记，12月出任革命委员会主席团主席，成为国家元首。

苏联军队自1989年从阿富汗撤军，阿富汗随即爆发内战，失去靠山的共产政权统治岌岌可危。苏联陷入政局动荡中，无暇顾及阿富汗，切断了对阿富汗共产政权的支持。此时[圣战者的势力逼近喀布尔](../Page/圣战者.md "wikilink")，他于1992年4月16日宣布向[圣战者投降](../Page/圣战者.md "wikilink")，并辞去总统职务。他试图逃离喀布尔，但遭到[阿布都·拉希德·杜斯塔姆将军的阻挠](../Page/阿布都·拉希德·杜斯塔姆.md "wikilink")，最终逃进联合国驻喀布尔办事处避难。

1996年9月26日，信奉伊斯兰原教旨主义的[塔利班攻入](../Page/塔利班.md "wikilink")[喀布尔](../Page/喀布尔.md "wikilink")，取得了政权。雖然納吉布拉和從小就結識的[艾哈迈德·沙阿·马苏德是政敵](../Page/艾哈迈德·沙阿·马苏德.md "wikilink")，但馬蘇德仍兩次提供機會幫纳吉布拉逃离喀布尔。纳吉布拉因相信塔利班会饶他一命，不会杀害他而拒绝了这个提议。1996年9月27日，塔利班士兵闖入联合国驻喀布尔办事处，将纳吉布拉从里面拖出来，指责他信奉[无神论](../Page/无神论.md "wikilink")、背弃伊斯兰教、违反神的意愿、使阿富汗陷入混乱，将他当众[阉割](../Page/阉割.md "wikilink")，\[1\]然后被拴在一辆卡车后面拖着游街示众直至死去，年仅49岁。他的血腥的尸体被悬挂在红绿灯上示众以昭示“一个新时代的来临”，他的嘴里被塞满了[钞票](../Page/钞票.md "wikilink")，所有手指都被砍掉，塔利班士兵将[雪茄插在他手指的位置](../Page/雪茄.md "wikilink")。\[2\]
他的兄弟Shahpur
Ahmadzai遭到同样的酷刑。\[3\]起初纳吉布拉和Ahmadzai因为他们的“罪行”，不允许举行伊斯兰葬礼，后来尸体通过[红十字国际委员会被交给](../Page/红十字国际委员会.md "wikilink")[帕克蒂亚省自己的部落安葬](../Page/帕克蒂亚省.md "wikilink")\[4\]。

国际社会，特别是[穆斯林世界](../Page/穆斯林世界.md "wikilink")，对此酷刑进行了广泛谴责。联合国发表声明，处死纳吉布拉加以谴责，并声称谋杀将进一步破坏阿富汗的稳定。塔利班的回应是宣布对[阿布都·拉希德·杜斯塔姆](../Page/阿布都·拉希德·杜斯塔姆.md "wikilink")，马苏德和[布尔汉努丁·拉巴尼的死刑宣判](../Page/布尔汉努丁·拉巴尼.md "wikilink")\[5\]。[印度一直支持纳吉布拉](../Page/印度.md "wikilink")，此时强烈谴责对他的公开处死，并开始支持马苏德的[拯救阿富汗全国统一伊斯兰阵线](../Page/北方聯盟_\(阿富汗\).md "wikilink")，试图遏制塔利班的崛起\[6\]。

## 参考资料

[Category:阿富汗总统](../Category/阿富汗总统.md "wikilink")
[Category:阿富汗外交官](../Category/阿富汗外交官.md "wikilink")
[Category:被處決的阿富汗人](../Category/被處決的阿富汗人.md "wikilink")
[Category:酷刑受害者](../Category/酷刑受害者.md "wikilink")
[Category:被處決的總統](../Category/被處決的總統.md "wikilink")
[Category:阿富汗普什图族人](../Category/阿富汗普什图族人.md "wikilink")
[Category:喀布爾大學校友](../Category/喀布爾大學校友.md "wikilink")
[Category:喀布尔人](../Category/喀布尔人.md "wikilink")
[Category:被塔利班殺害的人物](../Category/被塔利班殺害的人物.md "wikilink")

1.

2.

3.
4.

5.
6.  Pigott, Peter. [*Canada in Afghanistan: The War So
    Far*.](http://books.google.com/books?id=XaQPHbHb_fkC&pg=PA54&lpg=PA54&dq=Mohammad+Najibullah+delhi&source=bl&ots=P99ziKKf3-&sig=9msSFGE-gWwa8jtpVoHSC9g-_A4&hl=en&ei=qsMZStLsHpeWMZKphJoP&sa=X&oi=book_result&ct=result&resnum=4)
    Toronto: Dundurn Press Ltd, 2007. ISBN 978-1-55002-674-0, ISBN
    978-1-55002-674-0. P. 54.