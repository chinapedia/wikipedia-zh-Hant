[Hanging_Gardens_of_Babylon.jpg](https://zh.wikipedia.org/wiki/File:Hanging_Gardens_of_Babylon.jpg "fig:Hanging_Gardens_of_Babylon.jpg")
[Ogrody_semiramidy.jpg](https://zh.wikipedia.org/wiki/File:Ogrody_semiramidy.jpg "fig:Ogrody_semiramidy.jpg")
**巴比倫空中花园**（[古希腊文](../Page/古希腊文.md "wikilink")：οἱ \[τῆς Σεμιράμιδος\]
Κῆποι Κρεμαστοὶ
Βαβυλώνιοι）是[古代世界七大奇迹之一](../Page/古代世界七大奇迹.md "wikilink")，又称**悬园**。

## 概論

相傳在前6世纪由[新巴比伦王国的](../Page/新巴比伦王国.md "wikilink")[尼布甲尼撒二世在](../Page/尼布甲尼撒二世.md "wikilink")[巴比伦城为其患思鄉病的王妃安美依迪絲](../Page/巴比伦城.md "wikilink")（Amyitis）修建的。现已不存在，也是目前唯一一個位置尚未明確得知的遺蹟。空中花园据说采用立体造园手法，将[花园放在四层平台之上](../Page/花园.md "wikilink")，由瀝青及磚塊建成，平台由25米高的柱子支撑，并且有灌溉系统，奴隸不停地推動連繫著齒輪的把手。园中种植各种花草树木，远看犹如花园悬在半空中。

空中花園這名字純粹是出自對[希臘文paradeisos一字的意譯](../Page/希臘文.md "wikilink")，而不是吊於空中的[花園](../Page/花園.md "wikilink")。但就字面原意paradeisos直譯應作「梯形高台」，所以推測「空中花園」實際上就是建築在「梯形高台」上的花園。[希臘文paradeisos](../Page/希臘文.md "wikilink")（空中花園）後來蛻變為英文paradise（[天堂](../Page/天堂.md "wikilink")）。
[缩略图](https://zh.wikipedia.org/wiki/File:Gardens_of_Ninevah.png "fig:缩略图")
然而所有巴比倫文獻中並未有任何這座壯觀建築的紀錄顯得不尋常，加上長達數十年的考古都無法發掘到位置，使得更多人認為這只是虛構傳說。2010年後一個契機讓新理論誕生，一批[冷戰年代早期的美國日冕衛星照片解密](../Page/冷戰.md "wikilink")，學者發現當時的[伊拉克衛星照上有一條](../Page/伊拉克.md "wikilink")60英里的古運河遺跡，部分地段深度還超過蘇伊士運河，認為這是一個國家力量的產物，位於古[亞述境內](../Page/亞述.md "wikilink")，今日該遺跡已經因為破壞與[農業革命變成不明顯而未被發現](../Page/農業革命.md "wikilink")，但在1960年的冷戰早期卻明顯存在所以被拍攝到，加州理工大學古埃及學家AMANDA博士等人\[1\]於是提出一個新理論認為巴比倫空中花園實際上位於巴比倫以北300英里之外的亞述首都[尼尼微](../Page/尼尼微.md "wikilink")，亞述王西拿基立（704-681BC）建立於現代城市[摩蘇爾附近的](../Page/摩蘇爾.md "wikilink")[亞述首都](../Page/亞述.md "wikilink")[尼尼微](../Page/尼尼微.md "wikilink")\[2\]。其建造者是亞述王[西拿基立](../Page/西拿基立.md "wikilink")，而不是巴比倫的[尼布甲尼撒二世](../Page/尼布甲尼撒二世.md "wikilink")，應該更正為**亞述空中花园**。
[缩略图](https://zh.wikipedia.org/wiki/File:Hanging_Gardens_of_Babylon_.gif "fig:缩略图")
其主要理由如下\[3\]：

  - 因為巴比倫空中花园一詞其實是數百年後的產物，而當時的羅馬與[希臘等歐洲知識中心的人其實多數分不清亞述與巴比倫的差別](../Page/希臘.md "wikilink")，可能因此而誤植於較有名的巴比倫國。且亞述國王在西元前689年征服巴比倫後，亞述首都尼尼微曾被稱為「新巴比倫」。
  - 亞述人其實是美索布達米亞平原上當時唯一擅長修建水利設施的人，巴比倫人並不擅長，而常理推論空中花園的構想與實踐可能出現於擅長水利的工程師。
  - 當時該地區只有巴比倫和亞述兩大國有能力修建，所以若真有花園存在那答案只能這兩者二選一。
  - 只有該條古代水利設施是整個區域中唯一在當年代可大量供水給空中花园的，而巴比倫境內全無此類遺跡。\[4\]
  - 尼尼微古壁畫上曾發掘一個類似高台花園的圖片雕刻。\[5\]

## 参见

  - [悬空寺](../Page/悬空寺.md "wikilink")
  - [天空之城](../Page/天空之城.md "wikilink")
  - [隔火層](../Page/隔火層.md "wikilink")

## 現代建築元素

[201806_1000_Trees_(Hanging_Gradens_of_Babylon).jpg](https://zh.wikipedia.org/wiki/File:201806_1000_Trees_\(Hanging_Gradens_of_Babylon\).jpg "fig:201806_1000_Trees_(Hanging_Gradens_of_Babylon).jpg")（又名1000
Trees）\]\]
現代不少[大廈](../Page/大廈.md "wikilink")、[屋苑都採用空中花園建築元素](../Page/屋苑.md "wikilink")，又名為平台花園、天臺花園或者花園平台等。

## 參考文獻

<references/>

## 外部参考

  - [新华网 -
    世界著名古城遗址巴比伦及巴比伦空中花园](http://news.xinhuanet.com/world/2013-05/11/c_115728214.htm)
  - [巴比倫空中花園](http://www.nownews.com/n/2013/11/28/1031563)
  - [根據石刻復原的亞述空中花園電腦模型](https://www.youtube.com/watch?v=7yyZ6UFtoHo)

[Category:古代世界七大奇迹](../Category/古代世界七大奇迹.md "wikilink")
[Category:土木工程](../Category/土木工程.md "wikilink")
[Category:皇家园林](../Category/皇家园林.md "wikilink")
[Category:巴比倫](../Category/巴比倫.md "wikilink")

1.  Dalley, Stephanie. The Mystery of the Hanging Garden of Babylon; an
    elusive World Wonder traced, Oxford University Press (2013). ISBN
    978-0-19-966226-5. The quotations in this section are the
    translations of the author and are reproduced with the permission of
    OUP.

2.  Dalley, Stephanie, (2013) *The Mystery of the Hanging Garden of
    Babylon: an elusive World Wonder traced,* Oxford University Press.
    ISBN 978-0-19-966226-5

3.
4.  [The Hanging Gardens of …
    Nineveh?](http://news.nationalgeographic.com/news/2013/13/130531-babylon-hanging-gardens-nineveh-seven-wonders/)

5.  [空中花園確實存在　但不在巴比倫?](http://www.ettoday.net/news/20130510/204924.htm)