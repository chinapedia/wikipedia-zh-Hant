[Airguitar1.jpg](https://zh.wikipedia.org/wiki/File:Airguitar1.jpg "fig:Airguitar1.jpg")
**空气吉他社**是一种以模仿[摇滚乐或](../Page/摇滚乐.md "wikilink")[重金属音乐的](../Page/重金属音乐.md "wikilink")[电吉他](../Page/电吉他.md "wikilink")[独奏部分进行的社團](../Page/独奏.md "wikilink")，弹奏者往往会以[节拍强烈的电吉他音乐配合夸张的弹奏动作和](../Page/节拍.md "wikilink")[对嘴来进行表演](../Page/对嘴.md "wikilink")。

## 起源

[Ian_Gillan_playing_air_guitar.jpg](https://zh.wikipedia.org/wiki/File:Ian_Gillan_playing_air_guitar.jpg "fig:Ian_Gillan_playing_air_guitar.jpg")主唱的即兴表演\]\]
一般认为，空气吉他的表演方式最初是在1970年代的重金属迷之间开始盛行，在[铁娘子乐队发行的](../Page/铁娘子乐队.md "wikilink")《早期》（）[DVD中](../Page/DVD.md "wikilink")，乐队的前任和现任成员声称，乐队的狂热追随者带着“自制[木吉他](../Page/吉他.md "wikilink")”到演奏会现场进行即兴的模仿表演。最终，这股风气传染到其他的追随者身上，并从演奏木吉他改变成演奏空气吉他。

然而，真正将空气吉他带上舞台的是在1969年的[伍茲塔克音樂節上](../Page/伍茲塔克音樂節.md "wikilink")，[喬·庫克被发现在演唱](../Page/喬·庫克.md "wikilink")《在朋友们的帮助下》（）时，将弹奏吉他的动作带入他的表演当中。

另外，早在此的12年前，[钻石乐队在电视节目中演出](../Page/钻石乐队.md "wikilink")《爱的音讯》（）一曲时，[贝斯手](../Page/贝斯手.md "wikilink")[比尔·雷德也有弹奏空气吉他的动作](../Page/比尔·雷德.md "wikilink")，因此可以相信早在1957年以前，就已有乐队将弹奏空气吉他带入表演当中。

## 竞赛

有组织的空气吉他比赛在许多国家皆有举办，最早的空气吉他比赛是在1994年在[英国举行](../Page/英国.md "wikilink")，而从1996年开始配合[芬兰](../Page/芬兰.md "wikilink")[奥卢音乐映像节举办的空气吉他世界大赛在](../Page/奥卢音乐映像节.md "wikilink")2007年度吸引了来自[美国](../Page/美国.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[英国](../Page/英国.md "wikilink")、[德国](../Page/德国.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[法国](../Page/法国.md "wikilink")、[奥地利](../Page/奥地利.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[希腊](../Page/希腊.md "wikilink")、[日本](../Page/日本.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[新西兰](../Page/新西兰.md "wikilink")、[芬兰和](../Page/芬兰.md "wikilink")[西班牙等十七个国家的参赛者参与](../Page/西班牙.md "wikilink")。\[1\]

### 世界大赛

全称为**世界空气吉他锦标赛**（）的空气吉他大赛目前是由[世界空气吉他锦标赛网络进行管理](../Page/世界空气吉他锦标赛网络.md "wikilink")，在1996年开始举办时，原只不过是[芬兰](../Page/芬兰.md "wikilink")[奥卢音乐映像节的一部份](../Page/奥卢音乐映像节.md "wikilink")，发展至今吸引了来自世界各地的参赛者参与，主要裁判包括有芬兰的著名吉他手[尤哈·托维能](../Page/尤哈·托维能.md "wikilink")，奖品则是由[皇后乐队的](../Page/皇后乐队.md "wikilink")[布赖恩·梅赞助的](../Page/布赖恩·梅.md "wikilink")[VOX布莱恩](../Page/VOX.md "wikilink")·梅特制功率放大器和名为[芬兰飞人的特制吉他](../Page/芬兰飞人.md "wikilink")。

空气吉他世界锦标赛采用类似[花样滑冰的](../Page/花样滑冰.md "wikilink")6.0比分系统，其规则如下：

  - 每个参赛者分两个回合弹奏，每个回合限时1分钟：
      - 第一回合：参赛者弹奏自选的音乐，音乐被编辑成约60秒的长度；
      - 第二回合：参赛者弹奏主办单位指定的音乐，通常曲目予先没有宣布，因此参加者必须[即兴创作](../Page/即兴创作.md "wikilink")。
  - 参赛者必须是单独进行弹奏，不允许使用真实的[乐器和](../Page/乐器.md "wikilink")[乐队伴奏](../Page/乐队.md "wikilink")；声援者可以给予参赛者一些明确的引导，但必须在弹奏开始前离开场地。
  - 参赛者呈现的必须是空气吉他（也既是说空气鼓、空气钢琴或其他乐器是不被允许的），那可以是原音吉他、电子吉他或两者皆可。
  - 通常，参赛者并没有服装上的限制，并且被鼓励采取可以增加表演感染力的装扮和物品；不过，所有真正的音乐设备（如：[调音台](../Page/调音台.md "wikilink")、[功率放大器等](../Page/功率放大器.md "wikilink")）是被禁止采用的，可以代替[吉他拨子的器材也不例外](../Page/拨子.md "wikilink")。
  - 裁判通常包括著名的[音乐家或音乐评论家](../Page/音乐家.md "wikilink")。
  - 每位裁判给予参赛者4.0至6.0的评分，最高和最低的评分将会被舍弃，其他的评分总和最高者为优胜。
  - 评分的标准如下：
      - **独创性** - 参赛者除了单纯的模仿以外，能不能演绎出个人风格；
      - **音乐感** - 参赛者所选的曲子是否切合参赛者本身的形象；
      - **精准度** - 参赛者弹奏的真实度，节拍、指法和拨弦的准确度；
      - **舞台感** - 参赛者与群众之间的互动；是否表现出怯场或迟疑；
      - **印象分** - 一般来说，参赛者的魅力和感染力越大的话，印象分也就越高；
      - **空气感** - 这是一个主观的标准，纯粹考验参赛者除了机械式的模仿外，能不能演绎出“摇滚精神”。

### 历年优胜者

|                                                                                     历年优胜者                                                                                     |
| :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                                                                                      年度                                                                                       |
|                                                                                     2009年                                                                                     |
| [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg") 安德鲁·莱兹（Andrew "William Ocean" Litz） |
|                                                                                     2008年                                                                                     |
|                                                                                     2007年                                                                                     |
|                                                                                     2006年                                                                                     |
|                                                                                     2005年                                                                                     |
|                                                                                     2004年                                                                                     |
|          [Flag_of_New_Zealand.svg](https://zh.wikipedia.org/wiki/File:Flag_of_New_Zealand.svg "fig:Flag_of_New_Zealand.svg") 塔尔坎·凯斯（Tarquin "The Tarkness" Keys）           |
|                                                                                     2003年                                                                                     |
|                                                                                     2002年                                                                                     |
|                        [Flag_of_Canada.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Canada.svg "fig:Flag_of_Canada.svg") 安德鲁·布克莱斯（Andrew Buckles）                        |
|                                                                                     2001年                                                                                     |
|                                                                                     2000年                                                                                     |
|                                                                                     1999年                                                                                     |
|                                                                                     1998年                                                                                     |
|                                                                                     1997年                                                                                     |
|                                                                                     1996年                                                                                     |
|                      [Flag_of_Finland.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Finland.svg "fig:Flag_of_Finland.svg") 佩特里·海基能（Petri Heikkinen）                       |

### 国家大赛

世界空气吉他锦标赛网络目前配合25个[国家设立了国家空气吉他锦标赛](../Page/国家.md "wikilink")，各国首先在国内通过地区预赛决定出预赛优胜者，再经过决赛遴选出决赛优胜者，决赛优胜者将会成为国家代表参加世界空气吉他锦标赛。

国家大赛采用与世界大赛相同的规则，不过个别国家大赛往往也会附设几个条件，包括：

  - 参赛者必须是本国籍者；
  - 预赛优胜者在各预赛中选出并自动晋级决赛；
  - 决赛优胜者将会成为国家代表出赛世界空气吉他锦标赛；
  - 优胜者有义务协助推动空气吉他活动。

|                                                                                            各国家赛事                                                                                             |
| :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                                                                                              地区                                                                                              |
|                                    [Flag_of_Finland.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Finland.svg "fig:Flag_of_Finland.svg") 芬兰世界空气吉他锦标赛                                     |
|                    [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg") 英国空气吉他锦标赛                    |
|                                     [Flag_of_Austria.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Austria.svg "fig:Flag_of_Austria.svg") 奥地利空气吉他锦标赛                                     |
|                                 [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") 澳大利亚空气吉他锦标赛                                  |
|                              [Flag_of_New_Zealand.svg](https://zh.wikipedia.org/wiki/File:Flag_of_New_Zealand.svg "fig:Flag_of_New_Zealand.svg") 新西兰空气吉他锦标赛                               |
|                                       [Flag_of_Norway.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg "fig:Flag_of_Norway.svg") 挪威空气吉他锦标赛                                       |
|                     [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg") 美国空气吉他锦标赛                      |
|                         [Flag_of_the_Netherlands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Netherlands.svg "fig:Flag_of_the_Netherlands.svg") 荷兰空气吉他锦标赛                         |
|                                     [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg") 德国空气吉他锦标赛                                      |
|                                        [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg") 日本空气吉他锦标赛                                         |
|                                        [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") 意大利空气吉他锦标赛                                        |
|                                      [Flag_of_Canada.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Canada.svg "fig:Flag_of_Canada.svg") 加拿大空气吉他锦标赛                                       |
|                                       [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") 法国空气吉他锦标赛                                       |
|                                       [Flag_of_Greece.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Greece.svg "fig:Flag_of_Greece.svg") 希腊空气吉他锦标赛                                       |
|                               [Flag_of_Switzerland.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Switzerland.svg "fig:Flag_of_Switzerland.svg") 瑞士空气吉他锦标赛                                |
|                                      [Flag_of_Mexico.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Mexico.svg "fig:Flag_of_Mexico.svg") 墨西哥空气吉他锦标赛                                       |
|                                    [Flag_of_Estonia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Estonia.svg "fig:Flag_of_Estonia.svg") 爱沙尼亚空气吉他锦标赛                                     |
|                                    [Flag_of_Romania.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Romania.svg "fig:Flag_of_Romania.svg") 罗马尼亚空气吉他锦标赛                                     |
|                                      [Flag_of_Russia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Russia.svg "fig:Flag_of_Russia.svg") 俄罗斯空气吉他锦标赛                                       |
| [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg") 中国空气吉他锦标赛 |
|                                       [Flag_of_Brazil.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Brazil.svg "fig:Flag_of_Brazil.svg") 巴西空气吉他锦标赛                                       |
|                                    [Flag_of_Thailand.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Thailand.svg "fig:Flag_of_Thailand.svg") 泰国空气吉他锦标赛                                    |
|                                       [Flag_of_Taiwan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Taiwan.svg "fig:Flag_of_Taiwan.svg") 台湾空气吉他锦标赛                                       |
|                                        [Flag_of_Kenya.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kenya.svg "fig:Flag_of_Kenya.svg") 肯尼亚空气吉他锦标赛                                        |
|                             [Flag_of_South_Africa.svg](https://zh.wikipedia.org/wiki/File:Flag_of_South_Africa.svg "fig:Flag_of_South_Africa.svg") 南非空气吉他锦标赛                              |

### 台湾大赛

于2007年在[台湾](../Page/台湾.md "wikilink")[金山乡举办的](../Page/金山乡.md "wikilink")[OKGO冲浪音乐节除了有](../Page/冲浪音乐节.md "wikilink")[冲浪](../Page/冲浪.md "wikilink")、[电子音乐](../Page/电子音乐.md "wikilink")、[摇滚](../Page/摇滚.md "wikilink")[乐团和](../Page/乐团.md "wikilink")[嘻哈乐团的表演以外](../Page/嘻哈.md "wikilink")，也特别举行了一场空气吉他比赛。連結為獲得評審團特別獎的參賽者。http://www.youtube.com/watch?v=PE3dFUCA7S4\[2\]

## 革新技术及相关产品

在2005年，来自[赫尔辛基理工大学的学生开发了一种能够将手部动作转换成吉他声音的系统](../Page/赫尔辛基理工大学.md "wikilink")，并以此系统制造了一具多功能吉他，该系统包括了一双明亮的[手套和一台](../Page/手套.md "wikilink")[红外线照相机](../Page/红外线照相机.md "wikilink")，照相机通过确认手套之间的距离并综合弹奏者的手部动作形成一个[音调](../Page/音调.md "wikilink")，目前系统展示在[赫尔辛基科学中心](../Page/赫尔辛基科学中心.md "wikilink")。\[3\]

在2006年11月，[澳大利亚政府的](../Page/澳大利亚政府.md "wikilink")[联邦科学及工业研究组织的研究员公告他们发展了一种不需要经过人为控制](../Page/CSIRO.md "wikilink")，能够感应人体动作的精密纺织品控制计算机，并且宣布将它制为一件空气吉他[衬衣](../Page/衬衣.md "wikilink")。\[4\]\[5\]

在2008年3月，[加利福尼亚的Jada](../Page/加利福尼亚.md "wikilink")
Toys生产了一种名为空气吉他摇滚乐手（）的[专利](../Page/专利.md "wikilink")[玩具](../Page/玩具.md "wikilink")，玩者将一个[皮带扣戴在腰间](../Page/皮带.md "wikilink")，再使用一个有[磁性的拨子在皮带扣前做出弹奏动作](../Page/磁性.md "wikilink")，吉他音乐便会由一个便携式的[扬声器发出](../Page/扬声器.md "wikilink")。

### 日本玩具商品

日本参赛者大地洋辅于2006年赢的世界空气吉他大赛优胜时，瞬间成为了日本国内的热门话题，于是[玩具制造商趁此机会将一系列模仿空气吉他的产品推出市场](../Page/玩具.md "wikilink")：

  - 空气吉他PRO（[Takara Tomy](../Page/Takara_Tomy.md "wikilink")）
      -
        产品保留了吉他琴颈的部份，它能够在琴弦应在相应的位置发出一条[红外线](../Page/红外线.md "wikilink")，当以弹奏吉他的方式隔断红外线时，内置的[扬声器就会发出声音](../Page/扬声器.md "wikilink")。\[6\]
  - 空气吉他尤克里里琴Lilo & Stitch（Takara Tomy）
      -
        与空气吉他PRO相似，只不过将造型转换成[烏克麗麗琴](../Page/烏克麗麗.md "wikilink")。\[7\]
  - 摇滚之魂（）
      -
        产品造型参造[吉他拨子](../Page/拨子.md "wikilink")，通过按动上面的按钮发出音乐。\[8\]
  - 空气音乐家（）
      -
        只要将产品戴在手腕上，然后将手摆动就能够发出音乐。\[9\]
  - 空气吉他（u-mate）
      -
        这个产品是制成吉他形状的[透明](../Page/透明.md "wikilink")[气垫](../Page/气垫.md "wikilink")，其实无法发出任何声音，有Type-L和Type-V两款造型。\[10\]
  - 直感吉他（[TAITO](../Page/TAITO.md "wikilink")）
      -
        这是一款对应[NTT
        DoCoMo](../Page/NTT_DoCoMo.md "wikilink")[直感手机的](../Page/直感手机.md "wikilink")[游戏](../Page/手机游戏.md "wikilink")，只要把手在[屏幕上下摆动就能够发出吉它的声音](../Page/屏幕.md "wikilink")，另外还有一款相同原理的直感鼓游戏。\[11\]

## 脚注

## 外部链接

  - 竞赛

<!-- end list -->

  - [世界空气吉他锦标赛](http://www.airguitarworldchampionships.com/)
  - [世界空气吉他锦标赛联盟](https://web.archive.org/web/20080221223745/http://www.airguitarworldchampionship.org/)
  - [美国空气吉他锦标赛](http://www.airguitarusa.com/)
  - [Air Guitar UK
    Championships](https://web.archive.org/web/20070216235714/http://www.airguitaruk.com/)
  - [UK Air Guitar Championships](http://www.ukairguitar.com/)
  - [日本空气吉他锦标赛](http://airguitar.jp/)
  - [德国空气吉他联盟](http://www.germanairguitarfederation.de/)

<!-- end list -->

  - 其他

<!-- end list -->

  - [Virtual Air
    Guitar](https://web.archive.org/web/20110720182611/http://airguitar.tml.hut.fi/)

[Category:吉他](../Category/吉他.md "wikilink")
[Category:表演藝術](../Category/表演藝術.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.

9.

10.

11.