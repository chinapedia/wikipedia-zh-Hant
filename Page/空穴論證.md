**空穴論證**（）在[廣義相對論中是一](../Page/廣義相對論.md "wikilink")[悖論](../Page/悖論.md "wikilink")，曾經在[阿爾伯特·愛因斯坦往](../Page/阿爾伯特·愛因斯坦.md "wikilink")[場方程式前進的路上帶來疑惑與困擾](../Page/愛因斯坦場方程式.md "wikilink")。此一論證與「[流形實體論](../Page/流形實體論.md "wikilink")」（manifold
substantialism）相對抗；流形本質論為一個將[時空中](../Page/時空.md "wikilink")[事件的](../Page/事件.md "wikilink")[流形視為一實體](../Page/流形.md "wikilink")（substance）之學理，其存在獨立於其內所含[物質](../Page/物質.md "wikilink")。

## 愛因斯坦的空穴論證

[廣義協變性指出自然定律在所有](../Page/廣義協變性.md "wikilink")[參考系中必須相同](../Page/參考系.md "wikilink")，因此也在所有座標系統中相同。這個原則是[阿爾伯特·愛因斯坦在選擇重力理論的場方程式的一個準據](../Page/阿爾伯特·愛因斯坦.md "wikilink")。在1913年，愛因斯坦在建構[廣義相對論的過程中](../Page/廣義相對論.md "wikilink")，他了解到他所發現的一些事物相當驚人，這些是廣義協變性的直接結果。他將這些困擾展示為所謂的「空穴論證」。

## 參考文獻

  - [阿爾伯特·愛因斯坦](../Page/阿爾伯特·愛因斯坦.md "wikilink"), H. A. Lorentz, H.
    Weyl, and H. Minkowski, *[The Principle of
    relativity](../Page/The_Principle_of_relativity.md "wikilink")*
    (1916).

  - [Carlo Rovelli](../Page/Carlo_Rovelli.md "wikilink"), Quantum
    Gravity, Published by Cambridge University Press Year=2004 ID=ISBN
    978-0-521-83733-0

  - Norton, John, [The Hole
    Argument](http://plato.stanford.edu/archives/spr2004/entries/spacetime-holearg),
    The Stanford Encyclopedia of Philosophy (Spring 2004 Edition),
    Edward N. Zalta (ed.)

  - [Iftime,
    Mihaela](http://en.wikipedia.org/wiki/User:Mihaela_Iftime#Mihaela_Iftime)
    and [Stachel, John](../Page/Stachel,_John.md "wikilink"), "The Hole
    Argument for Covariant Theories", in GRG Springer (2006), Vol.38, No
    8, 1241-1252; e-print available as
    [gr-qc/0512021](http://arxiv.org/abs/gr-qc/0512021)

  - See *section 13.6*.

  - \`\`Physics Meets Philosophy at the Planck Scale'' (Cambridge
    University Press).

  - [Joy
    Christian](http://www.perimeterinstitute.ca/index.php?option=com_content&task=view&id=30&Itemid=72&e=Long%20Term%20Visitors&cat_id=453&cat_table=3&e=Joy%20Christian%20&f=3&name=Joy%20Christian%20&resident_id=4625),
    *Why the Quantum Must Yield to Gravity*, e-print available as
    [gr-qc/9810078](http://arxiv.org/abs/gr-qc/9810078). Appears in
    \`\`Physics Meets Philosophy at the Planck Scale'' (Cambridge
    University Press).

  - Carlo Rovelli and [Marcus Gaul](../Page/Marcus_Gaul.md "wikilink"),
    *Loop Quantum Gravity and the Meaning of Diffeomorphism Invariance*,
    e-print available as
    [gr-qc/9910079](http://arxiv.org/abs/gr-qc/9910079).

  - Alan Macdonald, *[Einstein's hole
    argument](https://archive.is/20130223160058/http://link.aip.org/link/?AJPIAS/69/223/1)*
    American Journal of Physics (Feb 2001) Vol 69, Issue 2, pp. 223-225.

[K](../Category/廣義相對論.md "wikilink")