以下是世界各地**[空軍](../Page/空軍.md "wikilink")**的列表。

## 非洲

  - [阿爾及利亞空軍](../Page/阿爾及利亞空軍.md "wikilink")

<!-- end list -->

  - [埃及空軍](../Page/埃及空軍.md "wikilink")

<!-- end list -->

  - [加納空軍](../Page/加納空軍.md "wikilink")

<!-- end list -->

  - [南非空軍](../Page/南非空軍.md "wikilink")

## 亞洲

  - [中国人民解放军空军](../Page/中国人民解放军空军.md "wikilink")

<!-- end list -->

  - [中華民國空軍](../Page/中華民國空軍.md "wikilink")

<!-- end list -->

  - [阿布哈兹空軍](../Page/阿布哈兹空軍.md "wikilink")

<!-- end list -->

  - [日本航空自衛隊](../Page/日本航空自衛隊.md "wikilink")

<!-- end list -->

  - [印度空軍](../Page/印度空軍.md "wikilink")

<!-- end list -->

  - [伊拉克空軍](../Page/伊拉克空軍.md "wikilink")

<!-- end list -->

  - [約旦皇家空軍](../Page/約旦皇家空軍.md "wikilink")

<!-- end list -->

  - [大韓民國空軍](../Page/大韓民國空軍.md "wikilink")

<!-- end list -->

  - [朝鮮人民軍空軍](../Page/朝鮮人民軍空軍.md "wikilink")

<!-- end list -->

  - [巴基斯坦空軍](../Page/巴基斯坦空軍.md "wikilink")

<!-- end list -->

  - [新加坡空军](../Page/新加坡空军.md "wikilink")

<!-- end list -->

  - [泰國皇家空軍](../Page/泰國皇家空軍.md "wikilink")

<!-- end list -->

  - [土耳其空軍](../Page/土耳其空軍.md "wikilink")

## 大洋洲

  - [澳大利亞皇家空軍](../Page/澳大利亞皇家空軍.md "wikilink")

<!-- end list -->

  - [紐西蘭皇家空軍](../Page/紐西蘭皇家空軍.md "wikilink")

## 歐洲

  - [俄羅斯空軍](../Page/俄羅斯空軍.md "wikilink")

<!-- end list -->

  - [阿布哈兹空軍](../Page/阿布哈兹空軍.md "wikilink")

<!-- end list -->

  - [亞美尼亞空軍](../Page/亞美尼亞空軍.md "wikilink")

<!-- end list -->

  - [奧地利空軍](../Page/奧地利空軍.md "wikilink")

<!-- end list -->

  - [比利時空軍](../Page/比利時空軍.md "wikilink")

<!-- end list -->

  - [保加利亞空軍](../Page/保加利亞空軍.md "wikilink")

<!-- end list -->

  - [丹麥空軍](../Page/丹麥空軍.md "wikilink")

<!-- end list -->

  - [西班牙空軍](../Page/西班牙空軍.md "wikilink")

<!-- end list -->

  - [愛沙尼亞空軍](../Page/愛沙尼亞空軍.md "wikilink")

<!-- end list -->

  - [芬蘭空軍](../Page/芬蘭空軍.md "wikilink")

<!-- end list -->

  - [法國空軍](../Page/法國空軍.md "wikilink")

<!-- end list -->

  - [德國空軍](../Page/德國空軍.md "wikilink")

<!-- end list -->

  - [匈牙利空軍](../Page/匈牙利空軍.md "wikilink")

<!-- end list -->

  - [冰島空軍](../Page/冰島空軍.md "wikilink")

<!-- end list -->

  - [意大利空軍](../Page/意大利空軍.md "wikilink")

<!-- end list -->

  - [荷蘭空軍](../Page/荷蘭空軍.md "wikilink")

<!-- end list -->

  - [挪威皇家空軍](../Page/挪威皇家空軍.md "wikilink")

<!-- end list -->

  - [波蘭空軍](../Page/波蘭空軍.md "wikilink")

<!-- end list -->

  - [葡萄牙空軍](../Page/葡萄牙空軍.md "wikilink")

<!-- end list -->

  - [羅馬尼亞空軍](../Page/羅馬尼亞空軍.md "wikilink")

<!-- end list -->

  - [瑞士空軍](../Page/瑞士空軍.md "wikilink")

<!-- end list -->

  - [瑞典空軍](../Page/瑞典空軍.md "wikilink")

<!-- end list -->

  - [英國皇家空軍](../Page/英國皇家空軍.md "wikilink")

<!-- end list -->

  - [烏克蘭空軍](../Page/烏克蘭空軍.md "wikilink")

## 南美洲

  - [阿根廷空軍](../Page/阿根廷空軍.md "wikilink")

<!-- end list -->

  - [巴西空軍](../Page/巴西空軍.md "wikilink")

<!-- end list -->

  - [哥倫比亞空軍](../Page/哥倫比亞空軍.md "wikilink")

<!-- end list -->

  - [厄瓜多爾空軍](../Page/厄瓜多爾空軍.md "wikilink")

<!-- end list -->

  - [秘魯空軍](../Page/秘魯空軍.md "wikilink")

<!-- end list -->

  - [烏拉圭空軍](../Page/烏拉圭空軍.md "wikilink")

<!-- end list -->

  - [委內瑞拉空軍](../Page/委內瑞拉空軍.md "wikilink")

## 北美洲

  - [古巴空軍](../Page/古巴空軍.md "wikilink")

<!-- end list -->

  - [墨西哥空軍](../Page/墨西哥空軍.md "wikilink")

<!-- end list -->

  - [美國空軍](../Page/美國空軍.md "wikilink")

## 其他地方

[蘇聯](../Page/蘇聯.md "wikilink")

  - [蘇聯空軍](../Page/蘇聯空軍.md "wikilink")（已解散）

## 外部連結

  - [Cocardes du monde entier - Roundels of the
    World](http://cocardes.monde.online.fr/)
  - <http://www.xairforces.com>

{{-}}

[\*](../Category/各国空军.md "wikilink")
[空军列表](../Category/空军列表.md "wikilink")