[JGSDF_22nd_Inf._official.jpg](https://zh.wikipedia.org/wiki/File:JGSDF_22nd_Inf._official.jpg "fig:JGSDF_22nd_Inf._official.jpg")一支突击小队\]\]
[Commando_jauber1.jpg](https://zh.wikipedia.org/wiki/File:Commando_jauber1.jpg "fig:Commando_jauber1.jpg")\]\]

**突擊隊**（），又稱**特攻隊**，是一種精英的[輕裝步兵或是](../Page/輕裝步兵.md "wikilink")[特種部隊](../Page/特種部隊.md "wikilink")。在美國陸軍中的[遊騎兵](../Page/美國陸軍遊騎兵.md "wikilink")，就是一種突擊兵，因此也被稱為[遊騎兵](../Page/遊騎兵.md "wikilink")。一般來說，突擊隊是有能力在發動大規模的攻勢中，作牽頭作用，並且可以在限定時間內，入侵[距離與](../Page/距離.md "wikilink")[範圍比一般單位表現較為傑出](../Page/範圍.md "wikilink")。人員能夠有此表現，是因為有多樣化的特殊[技能](../Page/技能.md "wikilink")、[武器與](../Page/武器.md "wikilink")[裝備](../Page/裝備.md "wikilink")，尤其是在與一般單位比較下，能夠[傘降](../Page/傘降.md "wikilink")、[攀冰](../Page/攀冰.md "wikilink")、及[岸灘作戰等](../Page/岸灘作戰.md "wikilink")。

## 歷史

突擊隊的原文本來指的是**一支軍事化部隊**，像是[陸軍的](../Page/陸軍.md "wikilink")[團](../Page/團.md "wikilink")，而不是一個隊員或行動名詞；但是現在也可以指[單兵或](../Page/單兵.md "wikilink")[突襲的](../Page/襲擊.md "wikilink")[軍事行動](../Page/軍事行動.md "wikilink")，原因是多年來，大眾多多少少誤解了新聞播報這個名詞時未有加以解釋而誤解了。

### 17世紀

或[南非荷蘭語的](../Page/南非荷蘭語.md "wikilink")可能產生於非洲殖民時期。[荷蘭人](../Page/荷蘭人.md "wikilink")[揚·范里貝克作為](../Page/揚·范里貝克.md "wikilink")[荷蘭殖民地的統治長官](../Page/荷蘭殖民地.md "wikilink")，於[南非](../Page/南非.md "wikilink")[開普敦向脫離](../Page/開普敦.md "wikilink")[荷兰东印度公司的](../Page/荷兰东印度公司.md "wikilink")[自由布尔人發布了](../Page/布尔人.md "wikilink")《》，准許這些布尔人武裝，條件是放棄土地分配與農耕[權力](../Page/權力.md "wikilink")，後來這些被要求協助維持殖民地的[治安](../Page/治安.md "wikilink")。在這點，與19世紀中，[德克薩斯共和國的遊騎兵相似](../Page/德克薩斯共和國.md "wikilink")。

後來這樣的法制消失了，不過在[布尔人侵入南部非洲的同時](../Page/布尔人.md "wikilink")，布尔人社區與農莊也組成自保。

19世紀末，[第二次布爾戰爭時](../Page/第二次布爾戰爭.md "wikilink")，75,000名[阿非利卡人對抗相對強大的](../Page/阿非利卡人.md "wikilink")[英國軍隊](../Page/英國軍隊.md "wikilink")（約450,000，包括了[澳大利亞](../Page/澳大利亞.md "wikilink")、[紐西蘭和](../Page/紐西蘭.md "wikilink")[加拿大的增援部隊](../Page/加拿大.md "wikilink")）時，對英國軍隊發動的[游擊戰讓](../Page/游擊戰.md "wikilink")[英國人頭痛不已](../Page/英國人.md "wikilink")。

### 第二次世界大戰

1940年，德國動用70名[士兵成功](../Page/士兵.md "wikilink")[偷襲了比利時埃本·艾美爾要塞](../Page/埃本-埃美爾要塞戰役.md "wikilink")。當時比利士的防守士兵有約800名。就此事件表明，經過特殊訓練，以偷襲和奇襲的方式，一小股力量就可以戰勝幾倍於自己的力量或數量，並且爲主力部隊完成任務及掃清障礙。

### 1945年至現代

## 参见

  - [襲擊](../Page/襲擊.md "wikilink")

  - [美國陸軍遊騎兵](../Page/美國陸軍遊騎兵.md "wikilink")

  - [英國皇家海軍陸戰隊](../Page/英國皇家海軍陸戰隊.md "wikilink")

  - [越南共和國別動軍](../Page/越南共和國別動軍.md "wikilink")

  -
  - [別動隊](../Page/別動隊.md "wikilink")

[he:קומנדו](../Page/he:קומנדו.md "wikilink")

[Category:军种](../Category/军种.md "wikilink")