**突尼斯國家足球隊**（）是[突尼斯的國家男子足球隊](../Page/突尼斯.md "wikilink")，由[突尼西亞足球協會負責管轄](../Page/突尼西亞足球協會.md "wikilink")。

## 歷史

突尼西亞曾經4度晉級世界盃決賽週，第一次在[1978年](../Page/1978年世界盃足球賽.md "wikilink")，是當時世界盃唯一的非洲區代表，最後突尼斯於分組賽以第三名未能晉級下一圈。90年代末開始，突尼斯已經穩固在非洲區內的實力，並且於1998年晉級20年來從未參加的世界盃決賽週，其後2002年及2006年都可以取得出線名額，三次參加決賽週同樣分組賽出局。2004年更以主辦國身分首次奪得[非洲國家盃榮譽](../Page/非洲國家盃.md "wikilink")。

## 世界盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>總排名</p></th>
<th><p>場數</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
<td><p><em>退出</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
<td><p>小組賽</p></td>
<td><p>9</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
<td><p>小組賽</p></td>
<td><p>26</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
<td><p>小組賽</p></td>
<td><p>29</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
<td><p>小組賽</p></td>
<td><p>24</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年</a></p></td>
<td><p>小組賽</p></td>
<td><p>24</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2022年世界盃足球賽.md" title="wikilink">2022年</a></p></td>
<td><p>待定</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
<td><p>5/21</p></td>
<td></td>
<td><p>15</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>9</p></td>
<td><p>13</p></td>
<td><p>25</p></td>
</tr>
</tbody>
</table>

## 洲際國家盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>場次</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1992年洲際國家盃.md" title="wikilink">1992年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1995年洲際國家盃.md" title="wikilink">1995年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1997年洲際國家盃.md" title="wikilink">1997年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1999年洲際國家盃.md" title="wikilink">1999年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2001年洲際國家盃.md" title="wikilink">2001年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2003年洲際國家盃.md" title="wikilink">2003年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2005年洲際國家盃.md" title="wikilink">2005年</a></p></td>
<td><p>第一圈</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年洲際國家盃.md" title="wikilink">2009年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年洲際國家盃.md" title="wikilink">2013年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2017年洲際國家盃.md" title="wikilink">2017年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
<td><p>1/10</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>5</p></td>
</tr>
</tbody>
</table>

## 非洲國家盃成績

<table style="width:100%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 13%" />
<col style="width: 10%" />
<col style="width: 13%" />
<col style="width: 13%" />
<col style="width: 10%" />
<col style="width: 13%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th></th>
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th></th>
<th><p>年份</p></th>
<th><p>最終成績</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1957年非洲國家盃.md" title="wikilink">1957年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p><a href="../Page/1976年非洲國家盃.md" title="wikilink">1976年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1994年非洲國家盃.md" title="wikilink">1994年</a></p></td>
<td><p>第一圈</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1959年非洲國家盃.md" title="wikilink">1959年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p><a href="../Page/1978年非洲國家盃.md" title="wikilink">1978年</a></p></td>
<td><p>殿軍</p></td>
<td><p><a href="../Page/1996年非洲國家盃.md" title="wikilink">1996年</a></p></td>
<td><p>亞軍</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1962年非洲國家盃.md" title="wikilink">1962年</a></p></td>
<td><p>季軍</p></td>
<td><p><a href="../Page/1980年非洲國家盃.md" title="wikilink">1980年</a></p></td>
<td><p><em>退出</em></p></td>
<td><p><a href="../Page/1998年非洲國家盃.md" title="wikilink">1998年</a></p></td>
<td><p>八強</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1963年非洲國家盃.md" title="wikilink">1963年</a></p></td>
<td><p>|第一圈</p></td>
<td><p><a href="../Page/1982年非洲國家盃.md" title="wikilink">1982年</a></p></td>
<td><p>第一圈</p></td>
<td><p><a href="../Page/2000年非洲國家盃.md" title="wikilink">2000年</a></p></td>
<td><p>殿軍</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1965年非洲國家盃.md" title="wikilink">1965年</a></p></td>
<td><p>亞軍</p></td>
<td><p><a href="../Page/1984年非洲國家盃.md" title="wikilink">1984年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/2002年非洲國家盃.md" title="wikilink">2002年</a></p></td>
<td><p>第一圈</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年非洲國家盃.md" title="wikilink">1968年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1986年非洲國家盃.md" title="wikilink">1986年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/2004年非洲國家盃.md" title="wikilink">2004年</a></p></td>
<td><p><strong>冠軍</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1970年非洲國家盃.md" title="wikilink">1970年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p><a href="../Page/1988年非洲國家盃.md" title="wikilink">1988年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/2006年非洲國家盃.md" title="wikilink">2006年</a></p></td>
<td><p>八強</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1972年非洲國家盃.md" title="wikilink">1972年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p><a href="../Page/1990年非洲國家盃.md" title="wikilink">1990年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/2008年非洲國家盃.md" title="wikilink">2008年</a></p></td>
<td><p>八強</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年非洲國家盃.md" title="wikilink">1974年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p><a href="../Page/1992年非洲國家盃.md" title="wikilink">1992年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/2010年非洲國家盃.md" title="wikilink">2010年</a></p></td>
<td><p>第一圈</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 近賽成績

### [2012年非洲國家盃](../Page/2012年非洲國家盃.md "wikilink")

  - 分組賽C組

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>+4</p></td>
<td><p><strong>9</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>3</p></td>
<td><p>+1</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
<td><p>−1</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>5</p></td>
<td><p>−4</p></td>
<td><p><strong>0</strong></p></td>
</tr>
</tbody>
</table>

-----

-----

  - 半準決賽

### [2014年世界盃](../Page/2014年世界盃足球賽.md "wikilink")

  - [外圍賽第二輪B組](../Page/2014年世界盃外圍賽非洲區第二輪#B組.md "wikilink")

## 著名球員

  - [查迪](../Page/查迪.md "wikilink") (Radhi Jaïdi)
  - [查比斯](../Page/查比斯.md "wikilink") (Hatem Trabelsi)
  - [保尼積爾](../Page/保尼積爾.md "wikilink")(Ali Boumnijel)

## 外部連結

  - [突尼斯國際賽成績(RSSSF)](http://www.rsssf.com/tablest/tun-intres.html)

[Category:突尼西亞國家足球隊](../Category/突尼西亞國家足球隊.md "wikilink")
[Category:非洲足球代表隊](../Category/非洲足球代表隊.md "wikilink")