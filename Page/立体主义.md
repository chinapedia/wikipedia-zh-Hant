[Juan_Gris_-_Portrait_of_Pablo_Picasso_-_Google_Art_Project.jpg](https://zh.wikipedia.org/wiki/File:Juan_Gris_-_Portrait_of_Pablo_Picasso_-_Google_Art_Project.jpg "fig:Juan_Gris_-_Portrait_of_Pablo_Picasso_-_Google_Art_Project.jpg")作品*畢卡索畫像*（1912，油畫）\]\]
**立體主義**（）是[前衛藝術運動的一個流派](../Page/前衛藝術運動.md "wikilink")，對[二十世紀初期的歐洲繪畫與雕塑帶來革命](../Page/二十世紀.md "wikilink")。

## 內容

立體主義的藝術家追求碎裂、解析、重新組合的形式，形成分離的畫面──以許多組合的碎片型態為藝術家們所要展現的目標。藝術家以許多的角度來描寫對象物，將其置於同一個畫面之中，以此來表達對象物最為完整的形象。物體的各個角度交錯疊放造成了許多的垂直與平行的線條角度，散亂的陰影使立體主義的畫面沒有傳統西方繪畫的透視法造成的三度空間錯覺，且刻意反抗艺术模仿生活主流思想。背景與畫面的主題交互穿插，讓立體主義的畫面創造出一個二度空間的繪畫特色。

## 歷史

立體主義開始於1906年，由[喬治·布拉克與](../Page/喬治·布拉克.md "wikilink")[帕布羅·畢卡索所建立](../Page/帕布羅·畢卡索.md "wikilink")，當時他們居住在[法國](../Page/法國.md "wikilink")[巴黎的蒙馬特區](../Page/巴黎.md "wikilink")。他們於1907年相識\[1\]。到1914年[第一次世界大戰爆發之前](../Page/第一次世界大戰.md "wikilink")，兩人一直非常親近地在一起工作。

法國的藝評人路易·沃克塞尔於1908年批評布拉克的作品「愛思塔克之屋」Houses at L'Estaque
為一堆立方體組成。在此之後，這「立體主義」被廣泛地拿來使用，但作為創始者的布拉克與畢卡索，則是相當地少用。在布拉克的作品之中，房屋的構建、形狀與樹木的色彩都極大地反映出[塞尚對其的影響](../Page/塞尚.md "wikilink")，直到1907年畢加索的《[亞維農的少女](../Page/亞維農的少女.md "wikilink")》之後「立體主義」才煥發出新的光彩。

立體主義起於自巴黎的[蒙馬特](../Page/蒙馬特.md "wikilink")，由聚集在賽納河左岸[蒙帕納斯地區的藝術家們傳播開來](../Page/蒙帕納斯.md "wikilink")，並由畫商Henry
Kahnweiler所發起。立體主義很快地受到歡迎，在1910年時，藝評者已將由布拉克與畢卡索影響的藝術家們稱做「立體學派」。然而，許多認為自己是立體主義的畫家，在藝術風格的表現上和布拉克或畢卡索的表現大異其趣，在1920年以前，他們就已經走向了明顯歧異的道路。比如[奧費主義即是立體主義的一個分支](../Page/奧費主義.md "wikilink")，其中包括[纪尧姆·阿波利奈尔](../Page/纪尧姆·阿波利奈尔.md "wikilink")、羅伯·德洛內，[馬塞爾·杜象](../Page/馬塞爾·杜象.md "wikilink")，[费尔南·莱热等著名畫家](../Page/费尔南·莱热.md "wikilink")\[2\]。

1910-1912年的立體主義被稱為是「分析型立體主義」Analytical
Cubism,此時畢加索與布拉克的風格與手法及其相像，以至於後人難以區分。分析性立體主義側重於分析、解剖物體型態，兩位藝術家傾向於直角與直線的表現形式。物體大而顏色敦厚，向畫布外擴張。畢加索與布拉克經常運用到象徵性主題與字母，例如樂器、水瓶、玻璃、報紙與人臉等。

立體主義在[二十世紀的最初十年影響了全歐洲的藝術家](../Page/二十世紀.md "wikilink")，並激發了一連串的藝術改革運動，如[未來主義](../Page/未來主義.md "wikilink")、[結構主義及](../Page/結構主義.md "wikilink")[表現主義等等](../Page/表現主義.md "wikilink")。

不斷尋求創新的藝術家，布拉克與畢卡索，尋找新的畫中主題及空間的表達模式。他們受到了[保羅·塞尚](../Page/保羅·塞尚.md "wikilink")、[喬治·秀拉](../Page/喬治·秀拉.md "wikilink")、伊伯利亞雕塑、[非洲部落藝術](../Page/非洲.md "wikilink")（即使布拉克駁斥這種說法），以及[野獸派的影響](../Page/野獸派.md "wikilink")。

## 對亚洲畫家的影響

民國時期留學來自法國的中國學生當中有不少畫家受到立體主義等的現代藝術潮流影響，代表人物包括後來在[浙江美術學院執教的](../Page/中国美术学院.md "wikilink")[方幹民](../Page/方幹民.md "wikilink")\[3\]、[林風眠和](../Page/林风眠.md "wikilink")[吳大羽](../Page/吳大羽.md "wikilink")，以及他們的學生[趙無極](../Page/趙無極.md "wikilink")、[朱德群和](../Page/朱德群.md "wikilink")[吳冠中等](../Page/吳冠中.md "wikilink")，和倪贻德、赵兽等。[方幹民著名作品](../Page/方幹民.md "wikilink")《秋曲》（1934）、《白鴿》（1935）和《節日裡的西湖》（1935）便是以立方主義為基礎的創作\[4\]\[5\]。

## 參考

  - [方幹民](../Page/方幹民.md "wikilink")
  - [畢卡索](../Page/巴勃羅·畢卡索.md "wikilink")

## 參考資料

[Category:藝術運動](../Category/藝術運動.md "wikilink")
[Category:現代主義](../Category/現代主義.md "wikilink")

1.  [Christopher Green, 2009, Cubism, MoMA, Grove Art Online, Oxford
    University
    Press](http://www.moma.org/collection/details.php?theme_id=10068&displayall=1#skipToContent)
2.  [MoMA collection Cubism, details, Oxford University
    Press](http://www.moma.org/collection/details.php?theme_id=10068&section_id=T020541)
3.
4.
5.