**立方晶系**，也叫**等轴晶系**，它有4个三重[对称轴以及](../Page/对称轴.md "wikilink")3个互相垂直的4次[对称轴或者](../Page/对称轴.md "wikilink")3个相互垂直的二重[对称轴](../Page/对称轴.md "wikilink")。其中的3个互相垂直的4次[对称轴或者](../Page/对称轴.md "wikilink")3个相互垂直的二重[对称轴是晶体结晶轴](../Page/对称轴.md "wikilink")。轴角α=β=γ=90<sup>o</sup>，轴单位a=b=c。所以凡是等轴晶系的晶体在各个方向上的性质——[光学性质](../Page/光学性质.md "wikilink")、电磁性、[折射率都相同](../Page/折射率.md "wikilink")。即具有所谓各向同性。

|  |
|  |
|  |
|  |
|  |

## 分類

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>點群</p></th>
<th><p>例</p></th>
<th><p><a href="../Page/空間群.md" title="wikilink">空間群</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>名稱</p></td>
<td><p><a href="../Page/赫尔曼–莫甘记号.md" title="wikilink">國際標記法</a></p></td>
<td><p>[1]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>195–197</p></td>
<td><p>四分半面晶族(Tetartoidal)</p></td>
<td><p><em>T</em></p></td>
<td><p>23</p></td>
</tr>
<tr class="odd">
<td><p>198–199</p></td>
<td><p>P2<sub>1</sub>3</p></td>
<td></td>
<td><p>I2<sub>1</sub>3</p></td>
</tr>
<tr class="even">
<td><p>200–205</p></td>
<td><p>偏方二十四面晶族(Diploidal)</p></td>
<td><p><em>T<sub>h</sub></em></p></td>
<td><p>(=m)</p></td>
</tr>
<tr class="odd">
<td><p>205–206</p></td>
<td><p>Pa</p></td>
<td></td>
<td><p>Ia</p></td>
</tr>
<tr class="even">
<td><p>207–211</p></td>
<td><p>五角二十四面晶族(Gyroidal)</p></td>
<td><p><em>O</em></p></td>
<td><p>432</p></td>
</tr>
<tr class="odd">
<td><p>212–214</p></td>
<td><p>P4<sub>3</sub>32, P4<sub>1</sub>32</p></td>
<td></td>
<td><p>I4<sub>1</sub>32</p></td>
</tr>
<tr class="even">
<td><p>215–217</p></td>
<td><p>六四面體晶族(Hextetrahedral)</p></td>
<td><p><em>T<sub>d</sub></em></p></td>
<td><p>3m</p></td>
</tr>
<tr class="odd">
<td><p>218–220</p></td>
<td><p>P3n</p></td>
<td><p>F3c</p></td>
<td><p>I3d</p></td>
</tr>
<tr class="even">
<td><p>221–230</p></td>
<td><p>六八面體晶族(Hexoctahedral)</p></td>
<td><p><em>O<sub>h</sub></em></p></td>
<td><p>(=mm)</p></td>
</tr>
</tbody>
</table>

## 參見

  - [晶體結構](../Page/晶體結構.md "wikilink")

[Category:立方晶系](../Category/立方晶系.md "wikilink")

1.  又名向夫立符號