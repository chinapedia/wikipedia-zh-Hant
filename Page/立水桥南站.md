**立水桥南站**位于[北京市](../Page/北京市.md "wikilink")[朝阳区](../Page/朝阳区.md "wikilink")，是[北京地铁](../Page/北京地铁.md "wikilink")[5号线的一个车站](../Page/北京地铁5号线.md "wikilink")。車站色調為白色。

## 位置

这个站安立路北端西侧，北侧就是[立水桥和](../Page/立水桥.md "wikilink")[立水西桥](../Page/立水西桥.md "wikilink")。

## 出口

这座车站共有3个出口：A（西北），C（东南），D（西南）

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>黄金苑</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>中国环境科学研究院</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>领地大厦</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 结构

这座车站为高架车站，采用侧式站台设计。

<table>
<tbody>
<tr class="odd">
<td><p><strong>地上三层</strong></p></td>
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
</tr>
<tr class="even">
<td><p>北</p></td>
<td><p>列车往方向 <small>（） <small></small></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>列车往<a href="../Page/宋家庄站_(北京市).md" title="wikilink">宋家庄方向</a> <small>（）</small> 南</p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地上二层</strong></p></td>
<td><p>站厅层</p></td>
</tr>
<tr class="even">
<td><p><strong>地面</strong></p></td>
<td><p>出入口</p></td>
</tr>
</tbody>
</table>

## 临近车站

[Category:北京市朝阳区地铁车站](../Category/北京市朝阳区地铁车站.md "wikilink")
[Category:2007年啟用的鐵路車站](../Category/2007年啟用的鐵路車站.md "wikilink")