《**站台**》，中國電影[导演](../Page/导演.md "wikilink")[贾樟柯的第二部长篇故事片电影](../Page/贾樟柯.md "wikilink")，[2000年出品](../Page/2000年电影.md "wikilink")，被誉为“平民化的史诗作品”。影片采用的语言是[晋语](../Page/晋语.md "wikilink")、[普通话](../Page/普通话.md "wikilink")、[河南](../Page/河南.md "wikilink")[安阳方言](../Page/安阳.md "wikilink")（王宏伟）。片長154分鐘。片名来自刘克的歌曲《站台》。这首歌曲在片中后期几次出现。

这部影片以贾樟柯的故乡[山西](../Page/山西.md "wikilink")[汾阳为背景](../Page/汾阳.md "wikilink")，讲述了几个歌舞演员从1970年代末到1990年代初的生活。贾樟柯通过几个小城镇人在这大约十年间的平淡生活，展示了现代经济中心的中国是如何从[文革后中国转型的](../Page/文革.md "wikilink")，以及这种转型对草根阶层的冲击。

## 故事梗概

文化大革命后的中国，在山西汾阳，一个文工团在酝酿改革。[手风琴手崔明亮爱上了朗诵](../Page/手风琴.md "wikilink")、舞蹈演员尹瑞娟，但尹瑞娟没有接受他作为正式男朋友。另一对情侣张军和钟萍则发生了性行为，钟萍怀孕。文工团被承包之后，改名“[深圳霹雳柔波歌舞团](../Page/深圳.md "wikilink")”，招收了一些新演员，四处表演。尹瑞娟则转到税务部门工作。最终，崔和尹终成眷属。

## 创作人员

  - 导演：[贾樟柯](../Page/贾樟柯.md "wikilink")
  - 编剧：[贾樟柯](../Page/贾樟柯.md "wikilink")

## 演员名单

  - 崔明亮：[王宏伟](../Page/王宏伟.md "wikilink")
  - 尹瑞娟：[赵　涛](../Page/赵涛.md "wikilink")
  - 钟　萍：[杨荔钠](../Page/杨荔钠.md "wikilink")
  - 张　军：梁景东
  - 韩三明：[韩三明](../Page/韩三明.md "wikilink")

## 时代表现

贾樟柯经常通过背景中的广播、电视、音乐、电影等来表现人物所处的时代和环境。在这部影片中也不例外。按照在影片中出现的顺序，如下这些事件在背景中被呈现。有些顺序和历史相比有少许差错：

1.  歌曲《[火车向着韶山跑](../Page/火车向着韶山跑.md "wikilink")》、“[农业学大寨](../Page/农业学大寨.md "wikilink")”口号
2.  [印度电影](../Page/印度.md "wikilink")《[流浪者](../Page/流浪者_\(印度電影\).md "wikilink")》，1970年代末期引进中国
3.  [刘兰芳播讲的](../Page/刘兰芳.md "wikilink")[评书](../Page/评书.md "wikilink")《[岳飞传](../Page/岳飞传.md "wikilink")》
4.  [喇叭裤](../Page/喇叭裤.md "wikilink")
5.  [李谷一的歌曲](../Page/李谷一.md "wikilink")《[绒花](../Page/绒花.md "wikilink")》（电影《[小花](../Page/小花.md "wikilink")》插曲，1979年）
6.  [邓丽君的歌曲](../Page/邓丽君.md "wikilink")《美酒加咖啡》
7.  [美国电视系列剧](../Page/美国.md "wikilink")《[加里森敢死队](../Page/加里森敢死队.md "wikilink")》（1979年开播）
8.  [中越战争中的](../Page/中越战争.md "wikilink")[法卡山前线新闻](../Page/法卡山.md "wikilink")（1979年之后）
9.  歌曲《[年轻的朋友来相会](../Page/年轻的朋友来相会.md "wikilink")》
10. “[四个现代化](../Page/四个现代化.md "wikilink")”口号
11. [轻音乐的流行](../Page/轻音乐.md "wikilink")
12. 平反[刘少奇的新闻](../Page/刘少奇.md "wikilink")（1980年2月[中共十一届五中全会](../Page/中共十一届五中全会.md "wikilink")）
13. [苏晓明的歌曲](../Page/苏晓明.md "wikilink")《[军港之夜](../Page/军港之夜.md "wikilink")》
14. [李谷一的歌曲](../Page/李谷一.md "wikilink")《[边疆的泉水清又纯](../Page/边疆的泉水清又纯.md "wikilink")》（电影《[黑三角](../Page/黑三角.md "wikilink")》，1978年）
15. [台湾歌手](../Page/台湾.md "wikilink")[张帝的歌曲](../Page/张帝.md "wikilink")、手提式[录音机](../Page/录音机.md "wikilink")
16. [香港歌曲](../Page/香港.md "wikilink")《[成吉思汗](../Page/成吉思汗.md "wikilink")》、[迪斯科](../Page/迪斯科.md "wikilink")
17. [烫发](../Page/烫发.md "wikilink")
18. 红棉牌[吉他的流行](../Page/吉他.md "wikilink")
19. 1984年[中华人民共和国国庆](../Page/中华人民共和国.md "wikilink")35周年大[阅兵](../Page/阅兵.md "wikilink")
20. 电视片《[河殇](../Page/河殇.md "wikilink")》（1988年）
21. 广播里的通缉令，学潮运动（1989年）
22. [刘克的歌曲](../Page/刘克.md "wikilink")《站台》
23. 香港歌手[张明敏的歌曲](../Page/张明敏.md "wikilink")《[我的中国心](../Page/我的中国心.md "wikilink")》（1984年）
24. [台湾歌手](../Page/台湾.md "wikilink")[苏芮的歌曲](../Page/苏芮.md "wikilink")《[是否](../Page/是否.md "wikilink")》
25. 中国的热播电视连续剧《[渴望](../Page/渴望.md "wikilink")》（1990年）

## 获奖

## 奖项

<table style="width:120%;">
<colgroup>
<col style="width: 20%" />
<col style="width: 18%" />
<col style="width: 14%" />
<col style="width: 65%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>類別</p></th>
<th><p>被提名者</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/威尼斯电影节.md" title="wikilink">威尼斯电影节</a></p></td>
<td><p><a href="../Page/金狮奖.md" title="wikilink">金狮奖</a></p></td>
<td><p>站台</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>亚洲最佳影片奖</p></td>
<td><p>站台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/弗利伯格国际电影节.md" title="wikilink">弗利伯格国际电影节</a></p></td>
<td><p><a href="../Page/堂吉诃德.md" title="wikilink">堂吉诃德奖</a></p></td>
<td><p>站台</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>費比西奖</p></td>
<td><p>站台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南村三大洲电影节.md" title="wikilink">南村三大洲电影节</a></p></td>
<td><p>最佳影片</p></td>
<td><p>站台</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳导演</p></td>
<td><p><a href="../Page/賈樟柯.md" title="wikilink">賈樟柯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新加坡国际电影节.md" title="wikilink">新加坡国际电影节</a></p></td>
<td><p>最佳亚洲故事片</p></td>
<td><p>站台</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>青年电影奖</p></td>
<td><p>站台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布宜诺斯艾利斯.md" title="wikilink">布宜诺斯艾利斯国际独立影片节</a></p></td>
<td><p>最佳影片</p></td>
<td><p>站台</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>电影手册</p></td>
<td><p>十大佳片</p></td>
<td><p>站台</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部参考

  - [《站台》](http://ent.sina.com.cn/m/c/f/zhtai/index.html)

[Category:贾樟柯电影](../Category/贾樟柯电影.md "wikilink")
[Z站](../Category/2000年電影.md "wikilink")
[Z站](../Category/中国电影作品.md "wikilink")