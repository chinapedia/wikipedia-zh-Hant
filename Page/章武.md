**章武**（221年四月-223年四月）是[三国时期](../Page/三国.md "wikilink")[蜀汉的君主汉昭烈帝](../Page/蜀汉.md "wikilink")[刘备的](../Page/刘备.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。这也是蜀汉政权的第一个年号。

221年四月刘备在[成都称帝](../Page/成都.md "wikilink")，建立蜀汉政权，继承[汉朝正统](../Page/汉朝.md "wikilink")。章武三年四月，刘备病死[白帝城](../Page/白帝城.md "wikilink")。五月漢懷帝[刘禅即位](../Page/刘禅.md "wikilink")，改元[建兴元年](../Page/建兴_\(刘禅\).md "wikilink")。

國號意思 章：樂竟為一章。十，數之終也。 武：定功戢兵，止戈為武。

## 纪年

<table>
<thead>
<tr class="header">
<th><p>章武</p></th>
<th><p>元年</p></th>
<th><p>二年</p></th>
<th><p>三年</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/公元纪年.md" title="wikilink">公元</a></p></td>
<td><p>221年</p></td>
<td><p>222年</p></td>
<td><p>223年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/干支纪年.md" title="wikilink">干支</a></p></td>
<td><p><a href="../Page/辛丑.md" title="wikilink">辛丑</a></p></td>
<td><p><a href="../Page/壬寅.md" title="wikilink">壬寅</a></p></td>
<td><p><a href="../Page/癸卯.md" title="wikilink">癸卯</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曹魏.md" title="wikilink">曹魏</a></p></td>
<td><p><a href="../Page/黄初.md" title="wikilink">黄初二年</a></p></td>
<td><p>三年</p></td>
<td><p>四年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/东吴.md" title="wikilink">东吴</a></p></td>
<td><p><a href="../Page/建安.md" title="wikilink">建安二十六年</a></p></td>
<td><p>二十七<br />
<a href="../Page/黄武.md" title="wikilink">黄武元年</a></p></td>
<td><p>二年</p></td>
</tr>
</tbody>
</table>

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")

[Category:蜀汉年号](../Category/蜀汉年号.md "wikilink")
[Z章](../Category/3世纪年号.md "wikilink")