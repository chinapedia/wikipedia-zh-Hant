**童寯**\[1\]（），字**伯潜**。[辽宁](../Page/辽宁.md "wikilink")[沈阳人](../Page/沈阳.md "wikilink")，[满族](../Page/满族.md "wikilink")。中國[建筑学家](../Page/建筑学.md "wikilink")，建筑教育家。中国近代造园理论研究的开拓者，中国近代建筑理论研究的开拓者之一。在《建筑五宗师》书中与[刘敦桢](../Page/刘敦桢.md "wikilink")、[吕彦直](../Page/吕彦直.md "wikilink")、[梁思成](../Page/梁思成.md "wikilink")、[杨廷宝合称](../Page/杨廷宝.md "wikilink")“建筑五宗师”。\[2\]

## 简历

  - 1900年10月2日出生于[奉天省城附近东台子村](../Page/奉天.md "wikilink")。
  - 1910年9月入[奉天省立第一小学](../Page/奉天省立第一小学.md "wikilink")，1917月年入[奉天省立第一中学](../Page/奉天省立第一中学.md "wikilink")。
  - 1921年7月中学毕业后在[天津新学书院专修英语](../Page/天津.md "wikilink")。
  - 1921年同时考取两所大学，但仍就读于[北平](../Page/北平.md "wikilink")[清华留美预备学校](../Page/清华留美预备学校.md "wikilink")。
  - 1925年毕业于清华学校高等科。同年秋，公费留学[美国](../Page/美国.md "wikilink")[宾夕法尼亚大学建筑系](../Page/宾夕法尼亚大学.md "wikilink")，曾与[杨廷宝](../Page/杨廷宝.md "wikilink")（杨为师兄，时已毕业，但与童常有往来）、[梁思成](../Page/梁思成.md "wikilink")
    [陈植等同窗学习](../Page/陈植_\(建筑师\).md "wikilink")。在校期间，先后获全美大学生设计竞赛一、二等奖牌各一枚。
  - 1928年冬，以3年修满6年全部学分，获得建筑学硕士学位，提前毕业。此后在[费城](../Page/费城.md "wikilink")、[纽约两地建筑师事务所实习](../Page/纽约.md "wikilink")、工作各一年。
  - 1930年春，赴[欧洲英](../Page/欧洲.md "wikilink")、法、德、意、瑞士、比、荷等国考察建筑，经东欧回国。
  - 1930年秋回国，任[沈阳](../Page/沈阳.md "wikilink")[东北大学建筑系教授](../Page/东北大学.md "wikilink")，1931年6月，[梁思成赴京](../Page/梁思成.md "wikilink")“中国营造学社”任职，童继任系主任。
  - 1931年，[九一八事变后建筑系解散](../Page/九一八事变.md "wikilink")，与[赵深](../Page/赵深.md "wikilink")、[陈植在](../Page/陈植.md "wikilink")[上海共同组建](../Page/上海.md "wikilink")[华盖建筑师事务所](../Page/华盖建筑师事务所.md "wikilink")。抗战爆发后1938年在[重庆](../Page/重庆.md "wikilink")、[贵阳设华盖建筑师事务所分所](../Page/贵阳.md "wikilink")。
  - 1944年，应[刘敦桢之邀抵重庆](../Page/刘敦桢.md "wikilink")，任[中央大学建筑系教授](../Page/國立中央大學.md "wikilink")，授课之余继续华盖建筑师事务所建筑师业务；抗战胜利后迁回南京。
  - 1949年，中央大学改名[南京大学后](../Page/南京大学.md "wikilink")，专任[南京大学建筑系教授](../Page/南京大学.md "wikilink")。1952年南京大学建筑系等工学院系科独立组建[南京工学院](../Page/南京工学院.md "wikilink")（1988年改名[东南大学](../Page/东南大学.md "wikilink")），任南京工学院建筑系教授。
  - 1983年3月28日逝世于[南京](../Page/南京.md "wikilink")，享年83岁。

## 主要设计作品（其中有与合伙人合作工程）

1932—1952年间，童寯主持或参加的工程项目有100多项，代表作品有：

  - 1932—1933年：
      - [南京宁海路新住宅区私人住宅](../Page/南京.md "wikilink")
      - 南京[原国民政府外交部大楼及官舍](../Page/国民政府外交部旧址.md "wikilink")
      - 南京[下关电厂](../Page/下关电厂.md "wikilink")
      - [南京首都饭店](../Page/首都饭店旧址.md "wikilink")
      - 上海[恒利银行](../Page/恒利银行.md "wikilink")
      - 南京中山陵园[孙科住宅](../Page/孙科公馆旧址_\(南京\).md "wikilink")
      - 上海[大上海大戏院](../Page/大上海大戏院.md "wikilink")
  - 1934年，上海金城大戏院，南京[卢树森住宅](../Page/卢树森.md "wikilink")，南京陵园[张治中住宅](../Page/张治中.md "wikilink")，[何建住宅](../Page/何建.md "wikilink")，上海公寓数幢。
  - 1935年，上海浙江兴业银行，南京金城银行别墅。
  - 1936年，南京陵园中山文化教育馆，南京[中央博物院](../Page/南京博物院.md "wikilink")（？）保管库。
  - 1937年，南京水晶台地质矿产陈列馆，[地质调查所](../Page/南京地质调查所陈列室旧址.md "wikilink")，[原国民政府资源委员会办公楼](../Page/国民政府资源委员会旧址.md "wikilink")、图书馆，南京白下路原国民党政府审计部办公楼。
  - 1938年，[四川](../Page/四川.md "wikilink")[资中酒精厂](../Page/资中.md "wikilink")，[重庆炼铜厂](../Page/重庆.md "wikilink")。
  - 1939年，[贵阳花溪清华中学](../Page/贵阳.md "wikilink")，大夏大学教舍。
  - 1941年，贵阳花溪贵筑县政府，清华中学。
  - 1942年，贵阳贵州省儿童图书馆。
  - 1943年，贵阳民众教育馆。
  - 1946年，南京下关工人福利社，南京小营航空工业局。
  - 1947年，南京[孝陵卫政治大学校舍](../Page/孝陵卫.md "wikilink")、宿舍，南京高楼门公路总局。
  - 1948年，南京[萨家湾交通银行](../Page/萨家湾.md "wikilink")，江南铁路公司建设大楼。
  - 1952年，上海[杨树浦电力学校教室](../Page/杨树浦电力学校.md "wikilink")、礼堂等。

## 主要论著

  - 著作：《[江南园林志](../Page/江南园林志.md "wikilink")》《新建筑与流派》《苏联建筑》《造园史纲》《日本近现代建筑》《建筑科技沿革》《[近百年西方建筑史](../Page/近百年西方建筑史.md "wikilink")》《中国园林对东西方的影响》
  - 论文：《外中分割》《随园考》《北京长春园西洋建筑》《北平两塔寺》《中国建筑的特点》《我国公共建筑外观的检讨》《外国纪念建筑史话》
  - 英文论著：《Chinese Gardens》（中国园林）、《Foreign Influence in Chinese
    Architecture》（中国建筑的外来影响）《Architectural Chronicle》《Glimpses of
    Gardens in Easter China》
  - 画册：《童寯画选》、《童寯素描选》等
  - 《童寯文集》四卷

## 参考来源

　　 [category:中華民國建築師](../Page/category:中華民國建築師.md "wikilink")
[category:清华大学校友](../Page/category:清华大学校友.md "wikilink")

[Category:中華人民共和國建築學家](../Category/中華人民共和國建築學家.md "wikilink")
[Category:中華人民共和國建築師](../Category/中華人民共和國建築師.md "wikilink")
[Category:國立中央大學教授](../Category/國立中央大學教授.md "wikilink")
[教T童](../Category/东南大学教师.md "wikilink")
[Category:東北大學教授](../Category/東北大學教授.md "wikilink")
[Category:賓夕法尼亞大學校友](../Category/賓夕法尼亞大學校友.md "wikilink")
[Category:國立清華大学校友](../Category/國立清華大学校友.md "wikilink")
[Category:沈阳人](../Category/沈阳人.md "wikilink")
[Category:滿族人](../Category/滿族人.md "wikilink")
[Jun寯](../Category/童姓.md "wikilink")

1.  寯，音同俊。英文Chuin Tung
2.  杨永生 《建筑五宗师》 2005 百花文艺出版社 ISBN 7530642294