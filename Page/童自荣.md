**童自榮**（），[回族](../Page/回族.md "wikilink")，[江苏](../Page/江苏省.md "wikilink")[丹阳人](../Page/丹阳市.md "wikilink")。是一位著名的[中国](../Page/中国.md "wikilink")[配音演员](../Page/配音演员.md "wikilink")。

## 生平

他1966年畢業於[上海戲劇學院](../Page/上海戲劇學院.md "wikilink")，自1973年開始在[上海电影译制片厂從事電影及電視等配音工作](../Page/上海电影译制片厂.md "wikilink")，2004年1月8日退休，以其独特的声音广为大陆影迷熟知。

## 作品

### 外国电影

  -
    《[佐罗](../Page/佐罗.md "wikilink")》（[佐罗](../Page/佐罗.md "wikilink")）
    《[三十九级台阶](../Page/三十九级台阶.md "wikilink")》
    《[铁面人](../Page/铁面人.md "wikilink")》
    《[茜茜公主](../Page/茜茜公主_\(電影\).md "wikilink")》
    《[黑郁金香](../Page/黑郁金香.md "wikilink")》
    《[砂器](../Page/砂器.md "wikilink")》
    《[追捕](../Page/追捕_\(电影\).md "wikilink")》
    《[苔丝](../Page/苔丝.md "wikilink")》
    《[国家利益](../Page/国家利益.md "wikilink")》
    《[虎口脱险](../Page/虎口脱险.md "wikilink")》
    《[木棉袈裟](../Page/木棉袈裟.md "wikilink")》
    《[蒲田进行曲](../Page/蒲田进行曲.md "wikilink")》
    《[胜利大逃亡](../Page/胜利大逃亡.md "wikilink")》
    《[华丽家族](../Page/华丽家族.md "wikilink")》
    《[未来世界](../Page/未来世界.md "wikilink")》
    《》
    《[海的女儿](../Page/海的女儿.md "wikilink")》
    《[悲惨世界](../Page/悲惨世界.md "wikilink")》
    《[孤星血泪](../Page/孤星血泪.md "wikilink")》
    《[海狼](../Page/海狼.md "wikilink")》
    《[尼罗河上的惨案](../Page/尼罗河上的惨案.md "wikilink")》
    《[阳光下的罪恶](../Page/阳光下的罪恶.md "wikilink")》
    《[莫扎特](../Page/莫扎特.md "wikilink")》
    《[莫斯科之恋](../Page/莫斯科之恋.md "wikilink")》
    《[加里森敢死队](../Page/加里森敢死队.md "wikilink")》
    《[伦敦上空的鹰](../Page/伦敦上空的鹰.md "wikilink")》
    《[霹雳天使](../Page/霹雳天使.md "wikilink")》
    《[哈利波特与魔法师](../Page/哈利波特与魔法师.md "wikilink")》
    《[哈利波特与密室](../Page/哈利波特与密室.md "wikilink")》
    《[绝唱](../Page/绝唱.md "wikilink")》
    《[哑女](../Page/哑女.md "wikilink")》
    《[夜茫茫](../Page/夜茫茫.md "wikilink")》
    《[云中漫步](../Page/云中漫步.md "wikilink")》
    《[现代启示录](../Page/现代启示录.md "wikilink")》
    《[高卢英雄大战凯撒大帝](../Page/高卢英雄大战凯撒大帝.md "wikilink")》
    《[玩具總動員](../Page/玩具總動員.md "wikilink")》
    《[玩具總動員2](../Page/玩具總動員2.md "wikilink")》
    《[玩具總動員3](../Page/玩具總動員3.md "wikilink")》

### 中国电影

  -
    《成吉思汗》（[桑昆](../Page/桑昆.md "wikilink")）
    《[西游记之大圣归来](../Page/西游记之大圣归来.md "wikilink")》（山妖之王[混沌](../Page/混沌.md "wikilink")）

## 外部連結

  -
  -
[T](../Page/category:中國配音員.md "wikilink")
[T](../Page/category:上海戏剧学院校友.md "wikilink")
[T](../Page/category:丹阳人.md "wikilink")
[Z自荣](../Page/category:童姓.md "wikilink")
[T](../Page/category:回族人.md "wikilink")