**童黨**通常是由一群青少年[三五成群聚集一起](../Page/三五成群.md "wikilink")，作出吸煙、叫罵、吸毒、打架等[反社會行為](../Page/反社會行為.md "wikilink")。這些行為多具滋擾性，且影響社會秩序、人身及財產安全。他們一般具[反社會或反叛傾向](../Page/反社會.md "wikilink")，部份更有[黑社會背景](../Page/黑社會.md "wikilink")。這亦造成一個[社會問題](../Page/社會.md "wikilink")，在香港常被作為[電視及](../Page/電視.md "wikilink")[電影常用的題材](../Page/電影.md "wikilink")。

## 童黨的心態

童黨一般具反叛性格，並對家長和學校的管教反感。他們大多缺少家庭溫暖，加入童黨成為了獲得認同的途徑，因而作出反叛的行為。童黨成員男性一般佔比例較多，但近年明顯多了女性參與。

## 相關的電影列表

  - 《[三五成群](../Page/三五成群_\(電影\).md "wikilink")》（[秀茂坪童黨燒屍案電影版](../Page/秀茂坪童黨燒屍案.md "wikilink")）
  - [郭富城的](../Page/郭富城.md "wikilink")《[童黨之街頭霸王](../Page/童黨之街頭霸王.md "wikilink")》
  - [劉國昌的](../Page/劉國昌.md "wikilink")《[童黨](../Page/童黨_\(電影\).md "wikilink")》
  - 《[牯岭街少年杀人事件](../Page/牯岭街少年杀人事件.md "wikilink")》
  - [劉國昌的](../Page/劉國昌.md "wikilink")《[圍城](../Page/圍城.md "wikilink")》
  - 《[慈雲山十三太保](../Page/慈雲山十三太保.md "wikilink")》

## 相關的電台節目

  - 《[危險人物](../Page/危險人物_\(電台節目\).md "wikilink")》之[寶馬山雙屍案](../Page/寶馬山雙屍案.md "wikilink")

## 外部链接

  - [青少年與童黨問題](https://web.archive.org/web/20091023172005/http://www.youthvoice.hk/gang/index.html)
  - [香港《立法會會議紀錄1999年6月2日》此文件的第5849頁有討論童黨問題](http://www.legco.gov.hk/yr98-99/chinese/counmtg/hansard/990602fc.pdf)
  - 在2004年7月中，體重逾200磅的18歲少女**劉美英**，遭童黨虐殺案，[9名童黨毒打少女10小時至死](http://news.xinhuanet.com/tai_gang_ao/2006-06/02/content_4637641.htm)
  - [2007年8月25日玉蓮臺童黨殺人案](https://web.archive.org/web/20081202150745/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20081115&sec_id=4104&subsec_id=12740&art_id=11850428)
  - 2007年[牛頭角](../Page/牛頭角.md "wikilink")[玉蓮臺](../Page/玉蓮臺.md "wikilink")[胡羽熹案，童黨罪成判刑](https://web.archive.org/web/20090220055816/http://hk.news.yahoo.com/article/090217/4/aqhq.html)
  - [海淀艺校课堂事件](http://tech.sina.com.cn/i/2007-05-29/07571532967.shtml)

[Category:犯罪社會学](../Category/犯罪社會学.md "wikilink")
[Category:社會問題](../Category/社會問題.md "wikilink")
[Category:兒童](../Category/兒童.md "wikilink")
[Category:青春期](../Category/青春期.md "wikilink")