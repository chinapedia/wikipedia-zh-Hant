**端平**（1234年—1236年）是[宋理宗赵昀的第三个年号](../Page/宋理宗.md "wikilink")。[南宋使用这个年号共三年](../Page/南宋.md "wikilink")。

## 纪年

| 端平                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 1234年                          | 1235年                          | 1236年                          |
| [干支](../Page/干支纪年.md "wikilink") | [甲午](../Page/甲午.md "wikilink") | [乙未](../Page/乙未.md "wikilink") | [丙申](../Page/丙申.md "wikilink") |

## 改元

## 大事记

  - [端平元年](../Page/1234年.md "wikilink")[農歷正月初十](../Page/2月9日.md "wikilink")——[南宋与](../Page/南宋.md "wikilink")[蒙古联军攻克](../Page/蒙古帝国.md "wikilink")[蔡州](../Page/蔡州_\(隋朝\).md "wikilink")，[金朝滅亡](../Page/金朝.md "wikilink")。
  - [端平元年農歷六月至八月](../Page/1234年.md "wikilink")——南宋「[端平入洛](../Page/端平入洛.md "wikilink")」，被蒙古軍大敗而回。

## 出生

  -
## 去世

  - [端平元年](../Page/1234年.md "wikilink")[農歷正月初十](../Page/2月9日.md "wikilink")——[金哀宗](../Page/金哀宗.md "wikilink")、[金末帝](../Page/金末帝.md "wikilink")，金朝末代皇帝

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [天兴](../Page/天兴_\(金哀宗\).md "wikilink")（1232年-1234年）：[金朝皇帝](../Page/金朝.md "wikilink")[金哀宗完颜守緒的年号](../Page/金哀宗.md "wikilink")
      - [盛昌](../Page/盛昌.md "wikilink")（1234年）：金朝皇帝[金末帝完顏承麟的年号](../Page/金末帝.md "wikilink")（[李兆洛](../Page/李兆洛.md "wikilink")《纪元编》以**盛昌**作为末帝的年号，但没有史料证据。）
      - [天應政平](../Page/天應政平.md "wikilink")（1232年至1254年）：陳朝—陳日煚之年號
      - [天福](../Page/天福_\(四條天皇\).md "wikilink")（1233年四月十五日至1234年十一月五日）：日本[四條天皇年號](../Page/四條天皇.md "wikilink")
      - [文曆](../Page/文曆.md "wikilink")（1234年十一月五日至1235年九月十九日）：日本[四條天皇年號](../Page/四條天皇.md "wikilink")
      - [嘉禎](../Page/嘉禎.md "wikilink")（1235年九月十九日至1238年十一月二十三日）：日本[四條天皇年號](../Page/四條天皇.md "wikilink")

[Category:南宋年号](../Category/南宋年号.md "wikilink")
[Category:13世纪中国年号](../Category/13世纪中国年号.md "wikilink")
[Category:1220年代中国政治](../Category/1220年代中国政治.md "wikilink")
[Category:1230年代中国政治](../Category/1230年代中国政治.md "wikilink")