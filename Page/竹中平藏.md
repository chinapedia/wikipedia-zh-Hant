**竹中平藏**（
），生於[日本](../Page/日本.md "wikilink")[和歌山縣](../Page/和歌山縣.md "wikilink")[和歌山市](../Page/和歌山市.md "wikilink")，經濟學者與政治人物。是[小泉政權重要的閣員之一](../Page/小泉純一郎.md "wikilink")。

## 生平

[一橋大學畢業](../Page/一橋大學.md "wikilink")，後來再以『日本経済の国際化と企業投資』為題取得[大阪大學經濟學博士](../Page/大阪大學.md "wikilink")。[慶應義塾大学教授](../Page/慶應義塾大学.md "wikilink")。[關西大學教授](../Page/关西大学.md "wikilink")。曾任参議院議員（1期）；内閣府特命擔當大臣（金融擔當、經濟財政政策擔當）任內推動[金融再生計劃](../Page/金融再生計劃.md "wikilink")；[郵政民營化担当大臣](../Page/郵政民營化.md "wikilink")；[總務大臣等職務](../Page/總務大臣.md "wikilink")。

## 主张

竹中平藏在接受《[南风窗](../Page/南风窗.md "wikilink")》采访时表示希望中国政府今后应该更多放权给市场，减少对于企业的补贴并且尽快处理不良债务问题。“中国政府应该乘着现在经济成长势头还算不错的时期，赶紧着手于这个问题。还是那句话，越快越好！”\[1\]。

## 著作

### 译作

  - 竹中平藏著；朱瑀等译《大家的经济学》（中国海关出版社出版，2004年5月）
  - 竹中平藏著；王泺译《明天的经济学》（中国海关出版社出版，2004年5月）
  - 竹中平藏著；林光江译《竹中平藏解读日本经济与改革》（新华出版社，2010年7月）
  - 竹中平藏，船桥洋一编著；林光江等译《日本3.11大地震的启示 复合型灾害与危机管理》（新华出版社，2012年3月）
  - 竹中平藏著；周维宏，刘彬洁译者《阻碍日本复兴的30个谎言》（东方出版社，2013年1月）
  - 竹中平藏著；范薇等译《读懂改革逻辑 竹中平藏的实践经济学》（浙江大学出版社，2014年9月）

## 脚注

<references />

[Category:和歌山縣出身人物](../Category/和歌山縣出身人物.md "wikilink")
[Category:二戰後日本政治人物](../Category/二戰後日本政治人物.md "wikilink")
[Category:日本總務大臣](../Category/日本總務大臣.md "wikilink")
[Category:經濟財政政策擔當大臣](../Category/經濟財政政策擔當大臣.md "wikilink")
[Category:金融擔當大臣](../Category/金融擔當大臣.md "wikilink")
[Category:第一次小泉內閣閣僚](../Category/第一次小泉內閣閣僚.md "wikilink")
[Category:第二次小泉內閣閣僚](../Category/第二次小泉內閣閣僚.md "wikilink")
[Category:第三次小泉內閣閣僚](../Category/第三次小泉內閣閣僚.md "wikilink")
[Category:日本自由民主黨黨員](../Category/日本自由民主黨黨員.md "wikilink")
[Category:日本參議院議員
2004–2010](../Category/日本參議院議員_2004–2010.md "wikilink")
[Category:日本经济学家](../Category/日本经济学家.md "wikilink")
[Category:關西大學教師](../Category/關西大學教師.md "wikilink")
[Category:一橋大學校友](../Category/一橋大學校友.md "wikilink")
[Category:大阪大學校友](../Category/大阪大學校友.md "wikilink")
[Category:比例區選出日本參議院議員](../Category/比例區選出日本參議院議員.md "wikilink")

1.  胡万程，尤一唯，王家远“处理债务风险，中国越快越好！”《[南风窗](../Page/南风窗.md "wikilink")》2018年第11期。