[Chuk_Yuen_North_Estate_201403.jpg](https://zh.wikipedia.org/wiki/File:Chuk_Yuen_North_Estate_201403.jpg "fig:Chuk_Yuen_North_Estate_201403.jpg")
[Chuk_Yuen_North_Estate_Koi_Pond_and_Pavilion.jpg](https://zh.wikipedia.org/wiki/File:Chuk_Yuen_North_Estate_Koi_Pond_and_Pavilion.jpg "fig:Chuk_Yuen_North_Estate_Koi_Pond_and_Pavilion.jpg")
[Chuk_Yuen_North_Estate_Football_Field.jpg](https://zh.wikipedia.org/wiki/File:Chuk_Yuen_North_Estate_Football_Field.jpg "fig:Chuk_Yuen_North_Estate_Football_Field.jpg")
[Chuk_Yuen_North_Estate_Gym_Zone_and_Playground_(brighter).jpg](https://zh.wikipedia.org/wiki/File:Chuk_Yuen_North_Estate_Gym_Zone_and_Playground_\(brighter\).jpg "fig:Chuk_Yuen_North_Estate_Gym_Zone_and_Playground_(brighter).jpg")
**竹園北邨**（英語：**Chuk Yuen North
Estate**）是[香港的租者置其屋計劃屋邨](../Page/香港.md "wikilink")，位於[九龍東](../Page/九龍東.md "wikilink")[黃大仙北面](../Page/黃大仙_\(香港地方\).md "wikilink")，[穎竹街至](../Page/穎竹街.md "wikilink")[沙田坳道之間](../Page/沙田坳道.md "wikilink")。本邨於1999年在[租者置其屋計劃](../Page/租者置其屋.md "wikilink")（第2期）把單位出售給所屬居民，現已成立竹園北邨業主立案法團，而該邨為[防止虐待兒童會市區總部](../Page/防止虐待兒童會.md "wikilink")。

## 屋邨資料

### 樓宇

| 樓宇名稱         | 樓宇類型                             | 落成年份  |
| ------------ | -------------------------------- | ----- |
| 榕園樓（第一期第11座） | [Y3型](../Page/Y3型.md "wikilink") | 1987年 |
| 橡園樓（第一期第12座） |                                  |       |
| 桐園樓（第一期第13座） |                                  |       |
| 梅園樓（第一期第14座） |                                  |       |
| 松園樓（第二期第8座）  | 1988年                            |       |
| 柏園樓（第二期第9座）  |                                  |       |
| 蕙園樓（第二期第10座） |                                  |       |
| 桃園樓（第二期第15座） | 1989年                            |       |

（註：因發展計劃內的第三期第16座已經轉作居者有其屋計劃屋苑[鵬程苑的關係](../Page/鵬程苑.md "wikilink")，部分座號被略去。上述座號是以房屋署Estate
Property 的紀錄為依歸。）

### 設施

  - [竹園廣場](../Page/竹園廣場.md "wikilink")

Chuk Yuen North Estate Basketball Court (brighter).jpg|籃球場 Chuk Yuen
Sports Centre (brighter).jpg|竹園體育館

## 教育及福利設施

### 幼稚園

  - [聖公會慈光堂柯佩璋幼稚園](http://www.skhklcopc.edu.hk)（1988年創辦）（位於桐園樓）
  - [嗇色園主辦可德幼稚園](../Page/嗇色園.md "wikilink")（1989年創辦）（位於松園樓）

<!-- end list -->

  - *已結束*

<!-- end list -->

  - [嗇色園主辦可慈幼兒園](../Page/嗇色園.md "wikilink")（位於榕園樓A翼地下）

### 綜合家居照顧服務

  - [香港聖公會黃大仙綜合家居照顧服務隊](http://www1.skhwc.org.hk/zh-hant/services/main/4/?sub_id=84)（位於榕園樓地下）

### 長者地區中心

  - [香港聖公會竹園馬田法政牧師長者綜合服務中心](http://www1.skhwc.org.hk/zh-hant/services/main/4/?sub_id=69)（位於蕙園樓地下）
  - 香港聖公會竹園馬田法政牧師長者綜合服務中心 - 榕園聚（位於榕園樓地下）

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/竹園邨巴士總站.md" title="wikilink">竹園邨巴士總站</a>／<a href="../Page/竹園道.md" title="wikilink">竹園道</a></dt>

</dl>
<dl>
<dt><a href="../Page/穎竹街.md" title="wikilink">穎竹街</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 區議會分區

竹園邨劃分三個選區，[竹園南邨劃分為竹園南選區](../Page/竹園南邨.md "wikilink")，竹園北邨（蕙園、榕園、橡園、桐園、桃園、梅園）劃分為竹園北選區，竹園北邨（松園、柏園）劃分為翠竹及鵬程選區。竹園北選區的民選議員為[陽光勵進會創會社長](../Page/陽光勵進會.md "wikilink")[丁志威](../Page/丁志威.md "wikilink")，2012年前則為政黨[社會民主連線前主席兼](../Page/社會民主連線.md "wikilink")[五區公投聯合行動委員會副召集人](../Page/五區公投.md "wikilink")[陶君行](../Page/陶君行.md "wikilink")，他於1991年-2011年間擔任竹園區區議員。竹園南選區的民選議員為[民協的](../Page/民協.md "wikilink")[許錦成](../Page/許錦成.md "wikilink")
。翠竹及鵬程選區的民選議員為陳英。

## 參考資料

## 相關網頁

  - [房委會竹園北邨資料](http://www.housingauthority.gov.hk/b5/interactivemap/estate/0,,3-347-12_4819,00.html)

[Category:黃大仙](../Category/黃大仙.md "wikilink")
[Category:租者置其屋計劃屋邨](../Category/租者置其屋計劃屋邨.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")
[Category:竹園](../Category/竹園.md "wikilink")