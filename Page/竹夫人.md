**竹夫人**是一种[東亞傳統的消暑用具](../Page/東亞.md "wikilink")，又稱**竹夹膝**、**竹姬**、**青奴**、**竹奴**、**竹妃**，一般由[竹或](../Page/竹.md "wikilink")[竹篾制成](../Page/竹篾.md "wikilink")。在现在的[中国已不常见](../Page/中国.md "wikilink")，在現代的[韩国](../Page/韩国.md "wikilink")、[日本及](../Page/日本.md "wikilink")[東南亞依然是常见的居家用具](../Page/東南亞.md "wikilink")。

## 制作

竹夫人一般是由[竹条编织成四周漏空](../Page/竹.md "wikilink")、上下封闭的笼状物体，或用一段竹子雕刻镂空，并打通中间的节制作而成，一般都不长过1米。人们在夏天暑热时候，可以怀抱一个竹夫人，起到通气降暑的作用。

## 历史和流传

据传竹夫人起于[唐代](../Page/唐代.md "wikilink")，最初名为“竹夹膝”、“竹姬”，直到宋代始称“竹夫人”。后流传至日本、[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[越南等地](../Page/越南.md "wikilink")，朝鮮有[高麗王朝已有竹夫人的文獻記載](../Page/高麗.md "wikilink")。後來再傳到[東南亞](../Page/東南亞.md "wikilink")，成為當地人日常生活用品之一。由於竹夫人製作成本低，一些窮人就以製作竹夫人為生。

中國到了[文化大革命时期](../Page/文化大革命.md "wikilink")，被列入“[四旧](../Page/四旧.md "wikilink")”而不准使用，遂逐渐在[中國大陆地区失传](../Page/中國大陆.md "wikilink")。此外，由於[風扇](../Page/風扇.md "wikilink")、[空調的普及](../Page/空調.md "wikilink")，很多地方已經比從前少用竹夫人。

## 相關創作

### 詩詞

### 其他

《[红楼梦](../Page/红楼梦.md "wikilink")》（甲辰、程本）中薛寶釵作诗迷：「有眼无珠腹内空，荷花出水喜相逢；梧桐叶落分离别，恩爱夫妻不到冬。」

[小池一夫原著的](../Page/小池一夫.md "wikilink")[日本漫畫](../Page/日本漫畫.md "wikilink")《[修羅雪姬](../Page/修羅雪姬.md "wikilink")》及分別於1973年及2001年上映的改編同名[日本電影中](../Page/日本電影.md "wikilink")，有一名女角是以編織竹夫人為生。

## 外部链接

  - [韩国人夏天离不开竹夫人](https://web.archive.org/web/20080216073621/http://tour.cyol.com/content/2007-03/13/content_1698270.htm)

[Category:東亞傳統家具](../Category/東亞傳統家具.md "wikilink")
[Category:東南亞文化](../Category/東南亞文化.md "wikilink")
[Category:生活用品](../Category/生活用品.md "wikilink")
[Category:竹製品](../Category/竹製品.md "wikilink")
[Category:寢具](../Category/寢具.md "wikilink")