**竹柏**（學名：*Nageia
nagi*），別名**南攻竹柏**、**臺灣竹柏**、**恆春竹柏**、**山杉**、**竹葉柏**。葉脈平行似竹葉而得名，材質似杉木，故有**山杉**之稱。竹柏是有名的景觀樹與行道樹種，為常綠喬木，高可達20米，病蟲害為[橙帶藍尺蛾](../Page/橙帶藍尺蛾.md "wikilink")。

## 特徵

樹幹通直，高可達30公尺，樹皮黑褐色，光滑，外皮常片狀剝落，樹枝斜上昇或水平開展。
葉卵狀披針形，單葉，對生或近似對生，長3至9公分，寬0.8至2.5公分，葉面革質光滑，質地厚，表面濃綠色，背面淡綠色，有多條平行细[葉脈](../Page/葉脈.md "wikilink")，無中脈，葉揉碎後有[番石榴氣味](../Page/番石榴.md "wikilink")。雌雄異株；雄花毬葇荑狀，雌花單生於葉腋。
種子球形，直徑約1至1.5公分，種子表面微帶白粉狀，外種皮青藍色，成熟後轉深紫色。內種皮茶褐色，果柄光滑。種實可榨油。雄球花為穗狀圓柱形，單生於葉腋，多呈分枝狀，長1.8至2.5厘米，總梗粗而短，基部有少數三角狀苞片，雌球花為單生葉腋，較少成對腋生，基部有數枚苞片，開花後與[羅漢松不同](../Page/羅漢松.md "wikilink")，竹柏種托已經退化，苞片並不會肥大成肉質種托。\[1\]
\[2\]

## 習性

竹柏生長緩慢，喜温暖环境，不耐湿亦不耐旱，稍耐寒。為中性偏陰樹種，幼株極為耐陰，成年後則可接受較強的光照。\[3\]

## 繁殖

竹柏可以利用播種和扦插繁殖。播種可將竹柏種子除去外種皮，泡水一天，將浮於水面的不良種子剔除，沉於水的種子即可播種，
一年四季皆可播種，但春夏氣溫較溫暖可加速發芽。\[4\]

## 用途

竹柏生長緩慢，質地堅重，遇火時不易燃燒，木材可供建築、雕刻之用，樹皮可用作提煉染料之用。竹柏邊材淡黃白色，心材色暗，條紋理直，硬度適中，比重0.47至0.53，容易加工，耐久度高。是建築、造船、家具、器具及工藝用材優良的用材。種仁油供食用及工業用油。\[5\]

## 其他

為[台灣](../Page/台灣.md "wikilink")[新竹縣之縣樹](../Page/新竹縣.md "wikilink")。\[6\]

## 分佈

竹柏分佈於[中國東南部低](../Page/中國.md "wikilink")、中海拔常綠闊葉樹森林，最高達海拔1600米之高山地帶，[台灣](../Page/台灣.md "wikilink")(1906年首度引進種植)，[日本](../Page/日本.md "wikilink")、[琉球](../Page/琉球.md "wikilink")、[屋久島及](../Page/屋久島.md "wikilink")[九州](../Page/九州.md "wikilink")、[四國等地](../Page/四國.md "wikilink")。在台灣之原生分布於北部中低海拔[文山地區及南部](../Page/文山區.md "wikilink")[恒春半島之](../Page/恒春半島.md "wikilink")[北大武山區](../Page/北大武山.md "wikilink")。\[7\]

## 現況

竹柏被[IUCN紅色名錄](../Page/IUCN紅色名錄.md "wikilink")[近危物種](../Page/近危.md "wikilink")，在1996年時已評估竹柏在台灣為[極危物種](../Page/極危.md "wikilink")（CR），園藝貿易的非法挖掘亦導致竹柏減少。\[8\]

## 相關條目

橙帶藍尺蛾

## 參見

## 外部連結

  - [竹柏种子](http://www.zhongzi8.com/priceShow.asp?id=457)
  - [台北植物園-植物資料庫(竹柏)](http://tpbg.tfri.gov.tw/plants/plants_info.php?rid=341)
  - [竹柏
    Zhubai](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00814)
    藥用植物圖像資料庫 (香港浸會大學中醫藥學院)
  - [台灣大百科全書 竹柏](http://nrch.culture.tw/twpedia.aspx?id=21503)
  - [竹柏Nageia nagi (Thunb.) O.Ktze. Nagai
    podocarpus](http://kent-garden.myweb.hinet.net/Nagai%20podocarpus.htm)

[category:松柏門](../Page/category:松柏門.md "wikilink")
[category:松柏目](../Page/category:松柏目.md "wikilink")
[category:羅漢松科](../Page/category:羅漢松科.md "wikilink")
[category:竹柏屬](../Page/category:竹柏屬.md "wikilink")

[Category:雌雄異株植物](../Category/雌雄異株植物.md "wikilink")
[Category:观叶植物](../Category/观叶植物.md "wikilink")
[Category:樹](../Category/樹.md "wikilink")
[Category:植物保護](../Category/植物保護.md "wikilink")
[Category:台灣植物](../Category/台灣植物.md "wikilink")
[Category:中國植物](../Category/中國植物.md "wikilink")
[Category:日本植物](../Category/日本植物.md "wikilink")

1.  [中國高等植物圖鑑-竹柏Podocarpus nagi (Thunb.) Zoll. et
    Mor.](http://www.eflora.cn/sptujian/%E7%AB%B9%E6%9F%8F)
2.  [國立台北大學樹種介紹](http://120.126.133.241/energymanage/TREE/tree_zhubai.html)

3.  [南仁山森林不同耐陰性樹苗光合作用性狀及其對光亮的可塑性](http://www.cnps.org.tw/Upload/Files/2012-03/201203201547539092514.pdf)

4.  [珍古德協會](http://www.goodall.org.tw/about_greenthumb/pgcontent/tree/27.html)
5.  [中國植物誌-竹柏](http://www.eflora.cn/spzwz/%E7%AB%B9%E6%9F%8F)
6.  [新竹縣官方網站](http://www.taiwantrip.com.tw/Scene/Index/?id=130)
7.
8.  [Nageia
    nagi](http://www.iucnredlist.org/details/46347417/0)，載[IUCN紅色名錄](../Page/IUCN紅色名錄.md "wikilink")，2013年11月15日查閱