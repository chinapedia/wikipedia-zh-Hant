**笛吹市**（）為[山梨縣](../Page/山梨县.md "wikilink")[甲府盆地中央地區的一個市](../Page/甲府盆地.md "wikilink")。成立于2004年10月12日，由[東八代郡與](../Page/東八代郡.md "wikilink")[東山梨郡](../Page/東山梨郡.md "wikilink")6町村合併而成。
[Isawa_Spa_Town_Aug.2013.JPG](https://zh.wikipedia.org/wiki/File:Isawa_Spa_Town_Aug.2013.JPG "fig:Isawa_Spa_Town_Aug.2013.JPG")

## 地理

  - 山：御坂山塊、秩父山地
  - 河川：[笛吹川](../Page/笛吹川.md "wikilink")、日川、金川、淺川、境川
  - 湖沼：

<!-- end list -->

  - 土地利用状況
      - 住宅地：14.1km²（8.5％）
      - 農用地：37.1km²（22.5％）
      - 森林等：83.8km²（50.9％）

### 相鄰的自治体

  - [甲府市](../Page/甲府市.md "wikilink")
  - [山梨市](../Page/山梨市.md "wikilink")
  - [甲州市](../Page/甲州市.md "wikilink")
  - [大月市](../Page/大月市.md "wikilink")
  - [南都留郡](../Page/南都留郡.md "wikilink")[富士河口湖町](../Page/富士河口湖町.md "wikilink")

## 历史

  - 2004年10月12日 -
    [東八代郡](../Page/東八代郡.md "wikilink")[石和町](../Page/石和町.md "wikilink")、[御坂町](../Page/御坂町.md "wikilink")、[一宮町](../Page/一宮町.md "wikilink")、[八代町](../Page/八代町.md "wikilink")、[境川村及](../Page/境川村.md "wikilink")[東山梨郡春日居町合併](../Page/東山梨郡.md "wikilink")，笛吹市成立。
  - 2006年8月1日 - 東八代郡[蘆川村編入](../Page/蘆川村.md "wikilink")。

## 經濟

### 主要產業

  - 農業
      - [果樹](../Page/果樹.md "wikilink")（[桃](../Page/桃.md "wikilink")、[葡萄](../Page/葡萄.md "wikilink")、[柿子](../Page/柿.md "wikilink")）
      - [花卉](../Page/花.md "wikilink")（[玫瑰](../Page/玫瑰.md "wikilink")、[菊](../Page/菊花.md "wikilink")）
  - 觀光
      - [石和温泉](../Page/石和温泉.md "wikilink")
  - 酒造
      - [甲州葡萄酒](../Page/日本的葡萄酒.md "wikilink")

### 產業人口

  -
    （2000年）

<!-- end list -->

  - 總計：38,725人
  - 第1產業：8,046人（20.8％）
  - 第2產業：9,582人（24.7％）
  - 第3產業：21,025人（54.3％）

-----

  -