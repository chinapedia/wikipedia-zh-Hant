**笛木優子**（），[日本](../Page/日本.md "wikilink")[女演員](../Page/女演員.md "wikilink")。在日本拍過幾部電影及電視劇，便來到[韓國發展](../Page/韓國.md "wikilink")，用「****（柳敏）」這個藝名\[1\]。2014年被曝与[张佑赫交往数年](../Page/张佑赫.md "wikilink")\[2\]\[3\]。

## 作品列表

### 電視劇

  - 2000年：[CX](../Page/富士電視台.md "wikilink")《》
  - 2001年：[CX](../Page/富士電視台.md "wikilink")《》
  - 2001年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[悄悄愛上妳](../Page/悄悄愛上妳.md "wikilink")》
  - 2002年：[KBS](../Page/韓國放送公社.md "wikilink")《[一起結婚吧](../Page/一起結婚吧.md "wikilink")》
  - 2003年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[All In
    真愛賭注](../Page/All_In_真愛賭注.md "wikilink")》（）
  - 2003年：MBC《[烈愛無間道](../Page/烈愛無間道.md "wikilink")》（）
  - 2003年：SBS《[狎鷗亭宗家](../Page/狎鷗亭宗家.md "wikilink")》（）
  - 2004年：SBS《[玻璃畫](../Page/玻璃畫.md "wikilink")》
  - 2005年：SBS《[不良主婦](../Page/不良主婦.md "wikilink")》（）
  - 2006年：CX《[-{zh-hans:甜心空姐;zh-hk:我要做空姐;zh-tw:空姐特訓班;}-](../Page/ATTENTION_PLEASE#「ATTENTION_PLEASE」2006年電視劇版.md "wikilink")》
    飾 麻生 薰
  - 2007年：[EX](../Page/朝日電視台.md "wikilink")《》飾 槇村隆子
  - 2009年：[TBS](../Page/TBS電視台.md "wikilink")《[RESCUE〜特別高度救助隊](../Page/RESCUE〜特別高度救助隊.md "wikilink")》飾
    水野明日香
  - 2009年：EX《[恋爱小说家·伊崎龙之介](../Page/恋爱小说家·伊崎龙之介.md "wikilink")》飾 伊崎／藤崎
  - 2009年：KBS《[IRIS](../Page/IRIS_\(電視劇\).md "wikilink")》飾
    日本内閣情報調査室國際部　佐藤江梨子
  - 2009年：TBS《》飾 小林朝美
  - 2010年：TBS《》飾 杉本美春
  - 2010年：TBS《》飾 劍持紗江子警部補
  - 2010年：EX《》飾 吉崎仁美
  - 2010年：SBS《[人生多美麗](../Page/人生多美麗.md "wikilink")》飾 柳彩英
  - 2012年：TBS《[浪花少年偵探團](../Page/浪花少年偵探團.md "wikilink")》飾 朝倉町子
  - 2013年：KBS《[IRIS 2](../Page/IRIS_2.md "wikilink")》飾 佐藤江梨子
  - 2013年：EX《[天氣姐姐](../Page/天氣姐姐_\(2013年電視劇\).md "wikilink")》飾 原口蘭
  - 2013年：EX《》飾 鐮田匠子
  - 2013年：EX《[Doctor-X～外科醫·大門未知子～2](../Page/Doctor-X～外科醫·大門未知子～.md "wikilink")》飾
    照井珠緒
  - 2014年：TBS《[出色的選TAXI](../Page/出色的選TAXI.md "wikilink")》飾
    野野山明步─第三話Guest
  - 2014年：NHK《[軍師官兵衛](../Page/軍師官兵衛.md "wikilink")》飾 御鮮（おせん）
  - 2015年：CX《[殘念丈夫](../Page/殘念丈夫.md "wikilink")》飾 大石香織
  - 2016年：[讀賣電視台](../Page/讀賣電視台.md "wikilink")《》飾 安住紗那

### 電影

  - 2001年：《[新雪國](../Page/新雪國.md "wikilink")》()
  - 2001年：《[The Firefly](../Page/The_Firefly.md "wikilink")》
  - 2003年：《[Jump](../Page/Jump.md "wikilink")》
  - 2004年：《[The Hotel Venus](../Page/The_Hotel_Venus.md "wikilink")》
  - 2005年：《[青燕](../Page/青燕.md "wikilink")》()
  - 2006年：《[公寓](../Page/公寓_\(電影\).md "wikilink")》()（客串）
  - 2007年：《[特別市的人們](../Page/特別市的人們.md "wikilink")》()
  - 2011年：《[朋友 Mooncake](../Page/朋友_Mooncake.md "wikilink")》
  - 2011年：《[家门的荣耀4](../Page/家门的荣耀4.md "wikilink")》()
  - 2012年：《[家门的荣耀5](../Page/家门的荣耀5.md "wikilink")》

## 獎項

  - 2003年：SBS演技大賞──新人賞

## 参考

## 外部連結

  -
[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:在韓國的日本人](../Category/在韓國的日本人.md "wikilink")
[Category:埼玉县出身人物](../Category/埼玉县出身人物.md "wikilink")
[Category:玉川学园女子短期大学校友](../Category/玉川学园女子短期大学校友.md "wikilink")
[Category:梨花女子大學校友](../Category/梨花女子大學校友.md "wikilink")
[Category:奧斯卡傳播所屬藝人](../Category/奧斯卡傳播所屬藝人.md "wikilink")

1.
2.
3.