**笠原 拓巳**（**かさはら たくみ**,
）是[童星](../Page/童星.md "wikilink")[talent](../Page/talent.md "wikilink")。屬於[放映新社的一員](../Page/放映新社.md "wikilink")。身高160公分。血型是B型。左撇子。暱稱是**笠ピー**。出身於[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")。

## 人物

  - 特技是足球、跳舞（ＨｉｐＨｏｐ）、以及能讓他大量流汗的事（？）。
  - 還不能一個人洗澡的樣子。（2005年4月時）
  - 2005年4月開始在[天才兒童MAX裡作為](../Page/天才兒童MAX.md "wikilink")[TV戰士而經常性演出中](../Page/TV戰士.md "wikilink")。
  - 2005年開始的時候、他的臉頰圓圓膨膨的,很是豐滿可愛、但是不知道是不是因為在[紙飛機達陣賽與主演的](../Page/天てれゲームゾーン.md "wikilink")[天TV連續劇](../Page/天TV連續劇.md "wikilink")「**走れ！拓巳**(跑啊\!拓巳)」和字面上意思相同的一直跑步的關係,變的瘦多了,也增加了可愛的感覺。
  - 最近似乎變得帥多了 。
  - [天才兒童max的幸運物的存在](../Page/天才兒童max.md "wikilink")。坦率的反應也讓人覺得快樂與可愛。
  - 他很會流汗、這幾乎可以算得上是一種特技、在[紙飛機達陣賽中他還準備了](../Page/天てれゲームゾーン.md "wikilink")5件替換用的襯衫。
  - 在競賽中幾乎完全幫不上忙、但是在復活優勝的第三回合賽中、作為[ジョーキマホーンズ・ワルブル的一員首次達陣](../Page/ジョーキマホーンズ・ワルブル.md "wikilink"),為優勝作出了貢獻。（在2006年度中,他在K-2比賽中做為サイキョウ・シャカリキ的隊長,以K-1出場為目標,但在首場比賽敗退。）
  - 魅力點是高亢的聲音與像嬰兒一樣圓圓滾滾的手指。
  - 無法好好拿筷子,而是用握住的,但在[天TV任務裡已經完美的克服](../Page/天TV任務.md "wikilink")。
  - 在[天TV任務中沒成功的單槓翻轉](../Page/天TV任務.md "wikilink")、在之後的現場直播完美的達成、演出了5部[天TV連續劇的作品](../Page/天TV連續劇\(2005年度\).md "wikilink"),是[TV戰士中最多的](../Page/TV戰士.md "wikilink"),非常活躍,令人難以想像他是個新人。
  - 在2005年夏季公演的設定中,稱呼前輩[de・Lencquesaing望為](../Page/de・Lencquesaing望.md "wikilink")「おやびん」,仰慕他的地方也很多、似乎是真的非常喜歡他。
  - 另外也稱呼[村田Chihiro為](../Page/村田Chihiro.md "wikilink")「姐姐(あねさん)」。
  - 一些人對於他特技的舞蹈（ＨｉｐＨｏｐ）有著疑問、但是他確實在[ドレミノテレビ裡拚命地](../Page/ドレミノテレビ.md "wikilink")？輕快跳著舞。
  - \-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-是他事務所的前輩,也是原[TV戰士](../Page/TV戰士.md "wikilink")。
  - 在2005年11月舉行的NHK[教育博覽會](../Page/教育博覽會.md "wikilink")2005（天才兒童MAX特別演唱會）中與原TV戰士的[井出卓也一起作為來賓而演出](../Page/井出卓也.md "wikilink")。
  - 在[快樂星期四！進行的企劃](../Page/天才兒童MAX#優克迪花園.md "wikilink")「エア祭り！」中,表演了虛擬空中保齡球。
  - 在2006年度的『[這裡是「週刊優克迪爾」編輯部](../Page/天才兒童MAX的單元#這裡是「週刊優克迪爾」編輯部.md "wikilink")』中,以「時代は硬派！！(時代是硬派的\!\!)」為題,表示這時代是"硬派"(在此為"身體僵硬"之意)的時代,與[日向滉一一起展示身體僵硬可以保持姿勢正確](../Page/日向滉一.md "wikilink"),而且衣服也不容易皺...等種種好處。

## 主要演出作品

### テレビ

  - [ドレミノテレビ](../Page/ドレミノテレビ.md "wikilink")（[NHK](../Page/日本放送協會.md "wikilink")）2003
    regular
  - [親と子のＴＶスクール](../Page/親と子のＴＶスクール.md "wikilink")（NHK）
  - [ほんとにあった怖い話](../Page/ほんとにあった怖い話.md "wikilink")（[富士電視台](../Page/富士電視台.md "wikilink")）ほん怖メンバー
  - [天才兒童MAX](../Page/天才兒童MAX.md "wikilink")（NHK）2005～TV戰士regular
  - [宇宙船蘇菲亞號的冒險](../Page/宇宙船蘇菲亞號的冒險.md "wikilink")（天才兒童MAX外傳）」（NHK）2005年

### 廣告

  - 「バーモントカレー」（[ハウス](../Page/ハウス.md "wikilink")）

### 電影

  - [タナカヒロシのすべて](../Page/タナカヒロシのすべて.md "wikilink") 2005

## 外部連結

  - [官方個人簡介](https://web.archive.org/web/20060911082322/http://www.houeishinsha.com/profile/kasahara-takumi.htm)
  - [放映新社](http://www.houeishinsha.com/)

[Category:TV戰士](../Category/TV戰士.md "wikilink")