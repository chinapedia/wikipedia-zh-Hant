[Hazard_F.svg](https://zh.wikipedia.org/wiki/File:Hazard_F.svg "fig:Hazard_F.svg")的符號。\]\]
在一种认知体系中，**符号**是指代一定意义的意象，可以是图形图像、文字组合，也可以是声音信号、建筑造型，甚至可以是一种思想文化、一个时事人物。例如“=”在数学中是等价的符号，“[紫禁城](../Page/紫禁城.md "wikilink")”在政治上是中国古代[皇权的象征](../Page/皇权.md "wikilink")。总的來說，符號的功能是携带和传达意义。\[1\]它具有极为广泛的含义，来自于规定或约定俗成。
以传播学的视角来看，麦克卢汉在《理解媒介》一书中提出，媒介即讯息。信息是符号和意义的统一体，符号是信息的外在形式或物质载体。结构语言学家索绪尔在《普通语言学教程》中界定为能指和所指。\[2\]英国学者特伦斯·霍克斯认为“任何事物只要它独立存在，并和另一事物有联系，而且可以被‘解释’，那么它的功能就是符号。”\[3\]

## 符號研究和應用

  - [符號學研究符號與符號系統](../Page/符號學.md "wikilink")，而[語意學則研究詞語的意義](../Page/語意學.md "wikilink")。
  - 優秀的[文學作品可以利用文字](../Page/文學.md "wikilink")、用語和情境，超越字面的解釋，引發讀者的想像和情感；[符號文學評論則利用符號學理論](../Page/符號文學評論.md "wikilink")，對文學進行研究。
  - [煉金術使用了大量的符號](../Page/煉金術.md "wikilink")，記錄精神和化學過程。到近代不斷演化及變化，形成現今[化學的](../Page/化學.md "wikilink")[化學符號](../Page/化學符號.md "wikilink")。
  - [宗教和](../Page/宗教.md "wikilink")[形而上學的文獻利用了很多](../Page/形而上學.md "wikilink")[秘教符號](../Page/秘教.md "wikilink")。
  - [佛洛依德的](../Page/佛洛依德.md "wikilink")[精神分析學與](../Page/精神分析學.md "wikilink")[榮格的](../Page/榮格.md "wikilink")[分析心理學認為](../Page/分析心理學.md "wikilink")，[夢有符號象徵的意義](../Page/夢.md "wikilink")。

## [文字符号](../Page/文字符号.md "wikilink")

  - [自源文字](../Page/自源文字.md "wikilink")
      - [楔形文字](../Page/楔形文字.md "wikilink")
  - [字母系統](../Page/字母系統.md "wikilink")
      - [字母](../Page/字母.md "wikilink")
  - [數字](../Page/數字.md "wikilink")
      - [羅馬數字](../Page/羅馬數字.md "wikilink")
  - [拉丁语](../Page/拉丁语.md "wikilink")
  - [漢字](../Page/漢字.md "wikilink")、[漢語](../Page/漢語.md "wikilink")
      - [甲骨文](../Page/甲骨文.md "wikilink")
      - [注音符號](../Page/注音符號.md "wikilink")
      - [拼音 (消歧義)](../Page/拼音_\(消歧義\).md "wikilink")
  - [字符](../Page/字符.md "wikilink")
      - [國際音標](../Page/國際音標.md "wikilink")

## [标点符号](../Page/标点符号.md "wikilink")

  - [?](../Page/?.md "wikilink")（問號）/
    [\!](../Page/!.md "wikilink")（感歎號）/
    [。](../Page/。.md "wikilink")（句號）/
    [&](../Page/&.md "wikilink")（與及）/
    [，](../Page/，.md "wikilink")（逗號）

## [数学符号](../Page/数学符号.md "wikilink")

  - [=](../Page/等于.md "wikilink")（等号）/
    [+](../Page/加法.md "wikilink")（加号）/
    [-](../Page/减法.md "wikilink")（减号）/
    [×](../Page/乘法.md "wikilink")（乘号）/
    [÷](../Page/除法.md "wikilink")（除号）

[≠](../Page/≠.md "wikilink") [≤](../Page/不等.md "wikilink")
[≥](../Page/≥.md "wikilink") [\<](../Page/不等號.md "wikilink")
[\>](../Page/不等號.md "wikilink") [≡](../Page/≡.md "wikilink")
[≈](../Page/≈.md "wikilink") [≅](../Page/≅.md "wikilink")
[∝](../Page/∝.md "wikilink") [°](../Page/°.md "wikilink")
[′](../Page/角分符號.md "wikilink") [″](../Page/角分符號.md "wikilink")
[∴](../Page/∴.md "wikilink") [∵](../Page/∵.md "wikilink")
[−](../Page/−.md "wikilink") [×](../Page/×.md "wikilink")
[÷](../Page/÷.md "wikilink") [±](../Page/±.md "wikilink")
[⊥](../Page/⊥.md "wikilink") [⊕](../Page/⊕.md "wikilink")
[⊗](../Page/⊗.md "wikilink") [∗](../Page/∗.md "wikilink")
[…](../Page/….md "wikilink") [½](../Page/½.md "wikilink")
[²](../Page/².md "wikilink") [³](../Page/立方數.md "wikilink")
[∂](../Page/∂.md "wikilink") [∫](../Page/數學符號表.md "wikilink")
[∑](../Page/∑.md "wikilink") [∞](../Page/∞.md "wikilink")
[Π](../Page/Π.md "wikilink") [√](../Page/√.md "wikilink")
[∇](../Page/∇.md "wikilink") [=](../Page/=.md "wikilink")
[←](../Page/←.md "wikilink") [→](../Page/→.md "wikilink")
[↔](../Page/↔.md "wikilink") [⇐](../Page/⇐.md "wikilink")
[⇒](../Page/⇒.md "wikilink") [⇔](../Page/⇔.md "wikilink")
[⌈](../Page/⌈.md "wikilink") [⌉](../Page/⌉.md "wikilink")
[⌊](../Page/⌊.md "wikilink") [⌋](../Page/⌋.md "wikilink")
[¬](../Page/¬.md "wikilink") [∧](../Page/∧.md "wikilink")
[∨](../Page/∨.md "wikilink") [∃](../Page/∃.md "wikilink")
[∀](../Page/∀.md "wikilink") [∈](../Page/∈.md "wikilink")
[∉](../Page/元素_\(數學\).md "wikilink")
[∋](../Page/元素_\(數學\).md "wikilink")
[∅](../Page/∅.md "wikilink") [⊆](../Page/⊆.md "wikilink")
[⊇](../Page/子集.md "wikilink") [⊃](../Page/子集.md "wikilink")
[⊂](../Page/⊂.md "wikilink") [⊄](../Page/子集.md "wikilink")
[∪](../Page/∪.md "wikilink") [∩](../Page/∩.md "wikilink")

[123](../Page/阿拉伯數字.md "wikilink")

  - [旗幟識別符號](../Page/旗幟學#旗幟識別符號（FIS）.md "wikilink")

## 天文學

  - [天文符號](../Page/天文符號.md "wikilink")

## [占星學](../Page/占星學.md "wikilink")

  - [星座符號](../Page/西洋占星學.md "wikilink")

## [性別](../Page/性別.md "wikilink")

  - [性別符號](../Page/性別符號.md "wikilink")

## 金融貨幣

  - [貨幣符號](../Page/貨幣符號.md "wikilink")

## [網路文化](../Page/網路文化.md "wikilink")

  - [網路語言](../Page/網路語言.md "wikilink")
  - [火星文](../Page/火星文.md "wikilink")
  - [表情符號](../Page/表情符號.md "wikilink")

## 其它

在人类文化中出现过的符号。

  - [♠](../Page/♠.md "wikilink") - [♣](../Page/♣.md "wikilink") -
    [♦](../Page/♦.md "wikilink") - [♥](../Page/♥.md "wikilink") -
    [卍](../Page/卍.md "wikilink") - [十字](../Page/十字.md "wikilink") -
    [☭](../Page/锤子与镰刀.md "wikilink")（锤子与镰刀）

## 参看

  - [記號](../Page/記號.md "wikilink")
  - [符号学](../Page/符号学.md "wikilink")
  - [符号学家](../Page/符号学家.md "wikilink")
  - [文字](../Page/文字.md "wikilink")
  - [語言](../Page/語言.md "wikilink")
  - [图腾](../Page/图腾.md "wikilink")
  - [標誌](../Page/標誌.md "wikilink")
  - [棋譜](../Page/棋譜.md "wikilink")
  - [字母频率](../Page/字母频率.md "wikilink")

## 外部連結

  - [symbols.com](http://www.symbols.com/)
  - [Symbols meanings for hundreds of
    symbols](http://livingartsoriginals.com/)
  - [Symbolism Wiki](../Page/wikia:symbolism.md "wikilink")
  - [Symbols & signs](http://www.digiden.nl/en/symbols-and-signs/)
  - [Ancient Symbolism](http://www.ancient-symbols.com/)
  - [Numericana](http://www.numericana.com/answer/symbol.htm)
  - [Forbidden symbols - Signs and symbols of cults, gangs and secret
    societies](http://www.forbiddensymbols.com)
  - [symbols, signs, logos, and their
    meanings](http://www.wikisymbol.com)

[符號](../Category/符號.md "wikilink")
[Category:传播学](../Category/传播学.md "wikilink")

1.  《传播学教程 第二版》，郭庆光 著，中国人民大学出版社34页
2.  《传播学教程 第二版》郭庆光 著，中国人民大学出版社，35页
3.  特伦斯·霍克斯 《结构主义与符号学》，上海译文出版社，132页