**符潤光**〈，），[香港](../Page/香港.md "wikilink")[音樂人](../Page/音樂.md "wikilink")，於1973年[畢業於](../Page/畢業.md "wikilink")[聖保羅書院](../Page/聖保羅書院.md "wikilink")，自1982年起出任[聖保羅書院音樂主任](../Page/聖保羅書院.md "wikilink")，至2013年退休。

## 生平

符氏就讀[小學期間獲其音樂老師賞識其](../Page/小學.md "wikilink")[歌唱天份](../Page/歌唱.md "wikilink")\[1\]，其後畢業於聖保羅書院後獲[香港聖公會保送至](../Page/香港聖公會.md "wikilink")[英國](../Page/英國.md "wikilink")[蘇格蘭](../Page/蘇格蘭.md "wikilink")[史特靈大學研習](../Page/:en:University_of_JJ.md "wikilink")。1982年他以音樂科主任的身份重回母校聖保羅書院任教三十餘年。

## 音樂貢獻

符氏除為聖保羅書院的音樂科主任外，亦為該校音樂科老師以及校內多個音樂團體（例如高級合唱團及音樂聯會）的領導人或指揮。此外，其在1997年曾經參演由[張學友擔任藝術總監的著名音樂劇](../Page/張學友.md "wikilink")《[雪狼湖](../Page/雪狼湖.md "wikilink")》，飾演老狼仙一角，並在劇中第二幕與張學友合唱《種子》與《老狼在此》。其亦擔任[香港歌劇社主席](../Page/香港歌劇社.md "wikilink")、[聖約翰座堂詩班長](../Page/聖約翰座堂.md "wikilink")、[香港兒童合唱團董事等職位](../Page/香港兒童合唱團.md "wikilink")\[2\]。

## 參考資料

  - [符潤光提醒詩班須自我定位](http://echo.hkskh.org/issue.aspx?lang=2&id=127&nid=993)

## 外部連結

  - [聖保羅書院](http://www.spc.edu.hk)
  - [香港歌劇社](http://www.opera.org.hk)

[符](../Category/香港音樂家.md "wikilink")
[F](../Category/聖保羅書院校友.md "wikilink")
[Category:香港聖保羅書院教師](../Category/香港聖保羅書院教師.md "wikilink")
[Category:香港舞臺劇演員](../Category/香港舞臺劇演員.md "wikilink")

1.  《音樂緣　保羅情　聖保羅「音樂家」系列專訪》，載聖保羅書院校刊《弘道》（Wayfarer）第46期（2007-2008）中文部，第22-24頁
2.  [Raymond Fu](http://www.opera.org.hk/committees/detail.php?cmid=2) -
    Our People - Opera Society of HK