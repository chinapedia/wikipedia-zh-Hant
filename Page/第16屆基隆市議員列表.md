以下列出[台灣](../Page/台灣.md "wikilink")**[基隆市](../Page/基隆市.md "wikilink")[議會第](../Page/基隆市議會.md "wikilink")16屆[議員](../Page/議員.md "wikilink")**名單。

本屆議會共有**31個席次**。全市七個[行政區中](../Page/市轄區.md "wikilink")，[仁愛區有](../Page/仁愛區.md "wikilink")4席、[中正區有](../Page/中正區_\(基隆市\).md "wikilink")5席、[信義區有](../Page/信義區_\(基隆市\).md "wikilink")4席、[中山區有](../Page/中山區_\(基隆市\).md "wikilink")4席、[安樂區有](../Page/安樂區.md "wikilink")7席、[暖暖區有](../Page/暖暖區.md "wikilink")3席、[暖暖區有](../Page/暖暖區.md "wikilink")4席、[七堵區有席](../Page/七堵區.md "wikilink")，另有[平地原住民](../Page/臺灣原住民.md "wikilink")1席；[政黨席次分配方面](../Page/政黨.md "wikilink")，[中國國民黨有](../Page/中國國民黨.md "wikilink")22席、[民主進步黨有](../Page/民主進步黨.md "wikilink")6席、[親民黨有](../Page/親民黨.md "wikilink")2席、[無黨籍有](../Page/無黨籍.md "wikilink")1席。

由於原任議長[張通榮於](../Page/張通榮.md "wikilink")2007年5月起轉任[基隆市市長](../Page/基隆市市長.md "wikilink")，市議會於同年5月31日進行議長補選，結果由[張芳麗議員當選](../Page/張芳麗.md "wikilink")，並於當日立即宣示就職，張芳麗議員也成為基隆市議會自1950年成立以來的第一位[女性議長](../Page/女性.md "wikilink")。

## 政黨席次

<table style="width:55%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 13%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>政黨</p></th>
<th><p>當選席次</p></th>
<th><p>備注 |-0</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p>20席</p></th>
<th style="text-align: center;"></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>6席</p></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td></td>
<td><p>4席</p></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Independent_candidate_icon_(TW).svg" title="fig:Independent_candidate_icon_(TW).svg">Independent_candidate_icon_(TW).svg</a> <a href="../Page/無黨籍.md" title="wikilink">無黨籍</a></p></td>
<td><p>3席</p></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 選區

### 第一選區

<table style="width:60%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選區</p></th>
<th><p>政黨</p></th>
<th><p>姓名</p></th>
<th><p>備註 |-0</p></th>
<th style="text-align: center;"><p>第一選區</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p><a href="../Page/呂美玲.md" title="wikilink">呂美玲</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/呂美玲.md" title="wikilink">呂美玲</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/張通榮.md" title="wikilink">張通榮</a></p></td>
<td><p>原任議長，後<a href="../Page/2007年基隆市市長補選.md" title="wikilink">當選並轉任</a><a href="../Page/基隆市市長.md" title="wikilink">基隆市市長</a></p></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/詹春陽.md" title="wikilink">詹春陽</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/謝守男.md" title="wikilink">謝守男</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/藍敏煌.md" title="wikilink">藍敏煌</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 第二選區

<table style="width:60%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選區</p></th>
<th><p>政黨</p></th>
<th><p>姓名</p></th>
<th><p>備註 |-0</p></th>
<th style="text-align: center;"><p>第二選區</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p><a href="../Page/何淑萍_(台灣).md" title="wikilink">何淑萍</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/張漢土.md" title="wikilink">張漢土</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/陳東財.md" title="wikilink">陳東財</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Independent_candidate_icon_(TW).svg" title="fig:Independent_candidate_icon_(TW).svg">Independent_candidate_icon_(TW).svg</a> <a href="../Page/無黨籍.md" title="wikilink">無黨籍</a></p></td>
<td><p><a href="../Page/韓良圻.md" title="wikilink">韓良圻</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 第三選區

<table style="width:60%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選區</p></th>
<th><p>政黨</p></th>
<th><p>姓名</p></th>
<th><p>備註 |-0</p></th>
<th style="text-align: center;"><p>第三選區</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p><a href="../Page/曾水源.md" title="wikilink">曾水源</a></p></th>
<th><p>副議長</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/張芳麗.md" title="wikilink">張芳麗</a></p></td>
<td><p>議長</p></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/莊榮欽.md" title="wikilink">莊榮欽</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/游祥耀.md" title="wikilink">游祥耀</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 第四選區

<table style="width:60%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選區</p></th>
<th><p>政黨</p></th>
<th><p>姓名</p></th>
<th><p>備註 |-0</p></th>
<th style="text-align: center;"><p>第四選區</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p><a href="../Page/宋瑋莉.md" title="wikilink">宋瑋莉</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/施世明.md" title="wikilink">施世明</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/張錦煌.md" title="wikilink">張錦煌</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/楊石城.md" title="wikilink">楊石城</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 第五選區

<table style="width:60%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選區</p></th>
<th><p>政黨</p></th>
<th><p>姓名</p></th>
<th><p>備註 |-0</p></th>
<th style="text-align: center;"><p>第五選區</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p><a href="../Page/沈義傳.md" title="wikilink">沈義傳</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/林弘和.md" title="wikilink">林弘和</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/俞?發.md" title="wikilink">俞?發</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/秦鉦.md" title="wikilink">秦鉦</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/莊錦田.md" title="wikilink">莊錦田</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/蔡適應.md" title="wikilink">蔡適應</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/鄭林清良.md" title="wikilink">鄭林清良</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 第六選區

<table style="width:60%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選區</p></th>
<th><p>政黨</p></th>
<th><p>姓名</p></th>
<th><p>備註 |-0</p></th>
<th style="text-align: center;"><p>第六選區</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p><a href="../Page/陳江山.md" title="wikilink">陳江山</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/黃景泰.md" title="wikilink">黃景泰</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Independent_candidate_icon_(TW).svg" title="fig:Independent_candidate_icon_(TW).svg">Independent_candidate_icon_(TW).svg</a> <a href="../Page/無黨籍.md" title="wikilink">無黨籍</a></p></td>
<td><p><a href="../Page/鄭怡信.md" title="wikilink">鄭怡信</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 第七選區

<table style="width:60%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選區</p></th>
<th><p>政黨</p></th>
<th><p>姓名</p></th>
<th><p>備註 |-0</p></th>
<th style="text-align: center;"><p>第七選區</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p><a href="../Page/高辜惠珍.md" title="wikilink">高辜惠珍</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/張耿輝.md" title="wikilink">張耿輝</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/陳金樹.md" title="wikilink">陳金樹</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Independent_candidate_icon_(TW).svg" title="fig:Independent_candidate_icon_(TW).svg">Independent_candidate_icon_(TW).svg</a> <a href="../Page/無黨籍.md" title="wikilink">無黨籍</a></p></td>
<td><p><a href="../Page/蔡旺璉.md" title="wikilink">蔡旺璉</a></p></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 第八選區

<table style="width:60%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選區</p></th>
<th><p>政黨</p></th>
<th><p>姓名</p></th>
<th><p>備註 |-0</p></th>
<th style="text-align: center;"><p>第八選區</p></th>
<th style="text-align: center;"></th>
<th style="text-align: center;"><p><a href="../Page/馬賢生.md" title="wikilink">馬賢生</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [中華民國行政院中央選舉委員會](http://www.cec.gov.tw)
  - [中央選舉委員會 -
    選舉資料庫](https://web.archive.org/web/20040908044807/http://210.69.23.140/cec/cechead.asp)
  - [基隆市議會全球資訊](http://www.kmc.gov.tw)

[\*16](../Category/基隆市議員.md "wikilink")
[基隆市議員016](../Category/台灣人列表.md "wikilink")