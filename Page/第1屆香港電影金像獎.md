**第1屆香港電影金像獎**於1982年3月9日在[香港藝術中心壽臣劇院舉行](../Page/香港藝術中心.md "wikilink")，由[香港電台與](../Page/香港電台.md "wikilink")[電影雙周刊聯合主辦](../Page/電影雙周刊.md "wikilink")。

獎項分為兩大組別，一組是五項最受影評人推薦的演員、編劇、導演及電影；另一組是頒發該年度的十大華語片及外語片。

## 得獎名單

### 最佳電影

| 獲獎電影                               |
| ---------------------------------- |
| 《[父子情](../Page/父子情.md "wikilink")》 |

### 最佳導演

| 獲獎者                              | 獲獎電影                               |
| -------------------------------- | ---------------------------------- |
| [方育平](../Page/方育平.md "wikilink") | 《[父子情](../Page/父子情.md "wikilink")》 |

### 最佳編劇

| 獲獎者                              | 獲獎電影                                   |
| -------------------------------- | -------------------------------------- |
| [張堅庭](../Page/張堅庭.md "wikilink") | 《[胡越的故事](../Page/胡越的故事.md "wikilink")》 |

### 最佳男主角

| 獲獎者                              | 獲獎電影                                 |
| -------------------------------- | ------------------------------------ |
| [許冠文](../Page/許冠文.md "wikilink") | 《[摩登保鑣](../Page/摩登保鑣.md "wikilink")》 |

### 最佳女主角

| 獲獎者                              | 獲獎電影                                    |
| -------------------------------- | --------------------------------------- |
| [惠英紅](../Page/惠英紅.md "wikilink") | 《[長輩](../Page/長輩_\(電影\).md "wikilink")》 |

### 十大華語片

1.  《[父子情](../Page/父子情.md "wikilink")》

2.  《[胡越的故事](../Page/胡越的故事.md "wikilink")》

3.  《[邊緣人](../Page/邊緣人_\(電影\).md "wikilink")》

4.  《[天雲山傳奇](../Page/天雲山傳奇.md "wikilink")》

5.  《歸心似箭》

6.  《鬼馬智多星》

7.  《[鬼打鬼](../Page/鬼打鬼.md "wikilink")》

8.  《[忌廉溝鮮奶](../Page/忌廉溝鮮奶_\(電影\).md "wikilink")》

9.  《[摩登保鑣](../Page/摩登保鑣.md "wikilink")》

10. 《舞廳》

### 十大外語片

1.  《[普通人](../Page/普通人.md "wikilink")》

2.  《[狂牛](../Page/狂牛.md "wikilink")》

3.  《》

4.  《》

5.  《[神劍](../Page/神劍.md "wikilink")》

6.  《[象人](../Page/象人_\(電影\).md "wikilink")》

7.  《》

8.  《》

9.  《》

10. 《[爵士春秋](../Page/爵士春秋.md "wikilink")》

## 相關條目

  - [香港電影金像獎](../Page/香港電影金像獎.md "wikilink")
  - [金馬獎](../Page/金馬獎.md "wikilink")

## 參考資料

## 外部連結

  - 第一屆香港電影金像獎得獎名單（[新版](http://www.hkfaa.com/winnerlist01.html)·[舊版](http://www.hkfaa.com/history/list_01.html)）

[Category:香港電影金像獎](../Category/香港電影金像獎.md "wikilink")
[Category:1981年電影](../Category/1981年電影.md "wikilink")
[港](../Category/1982年電影獎項.md "wikilink")