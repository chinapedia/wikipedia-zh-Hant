**第2周期元素**是[元素周期表中第二行](../Page/元素周期表.md "wikilink")（即[周期](../Page/周期_\(化学\).md "wikilink")）的[元素](../Page/元素.md "wikilink")。列表如下：

|                                  |                                  |
| -------------------------------- | -------------------------------- |
| [S區塊](../Page/S區塊.md "wikilink") | [P區塊](../Page/P區塊.md "wikilink") |



| nowrap|[原子序](../Page/原子序.md "wikilink") | nowrap|[元素符號](../Page/元素符號.md "wikilink") | 元素名稱                                    | nowrap|[族](../Page/族.md "wikilink")             | nowrap|[元素分區](../Page/元素分區.md "wikilink") | [電子排布](../Page/電子排布.md "wikilink") | [電子層](../Page/電子層.md "wikilink") | 備註      |
| --------------------------------------- | ----------------------------------------- | --------------------------------------- | ----------------------------------------------- | ----------------------------------------- | ---------------------------------- | -------------------------------- | ------- |
| 3                                       | Li                                        | **[鋰](../Page/鋰.md "wikilink")**        | [1族](../Page/鹼金屬.md "wikilink")、s<sup>1</sup>   | [s區](../Page/s區塊.md "wikilink")           | \[He\]2s<sup>1</sup>               | 2, 1                             |         |
| 4                                       | Be                                        | **[鈹](../Page/鈹.md "wikilink")**        | [2族](../Page/鹼土金屬.md "wikilink")、s<sup>2</sup>  | s區                                        | \[He\]2s<sup>2</sup>               | 2, 2                             |         |
| 5                                       | B                                         | **[硼](../Page/硼.md "wikilink")**        | [13族](../Page/硼族元素.md "wikilink")、p<sup>1</sup> | [p區](../Page/p區塊.md "wikilink")           | \[He\]2s<sup>2</sup>2p<sup>1</sup> | 2, 3                             | 第一個p區元素 |
| 6                                       | C                                         | **[碳](../Page/碳.md "wikilink")**        | [14族](../Page/碳族元素.md "wikilink")、p<sup>2</sup> | p區                                        | \[He\]2s<sup>2</sup>2p<sup>2</sup> | 2, 4                             |         |
| 7                                       | N                                         | **[氮](../Page/氮.md "wikilink")**        | [15族](../Page/氮族元素.md "wikilink")、p<sup>3</sup> | p區                                        | \[He\]2s<sup>2</sup>2p<sup>3</sup> | 2, 5                             |         |
| 8                                       | O                                         | nowrap|**[氧](../Page/氧.md "wikilink")** | [16族](../Page/氧族元素.md "wikilink")、p<sup>4</sup> | p區                                        | \[He\]2s<sup>2</sup>2p<sup>4</sup> | 2, 6                             |         |
| 9                                       | F                                         | **[氟](../Page/氟.md "wikilink")**        | [17族](../Page/鹵素.md "wikilink")、p<sup>5</sup>   | p區                                        | \[He\]2s<sup>2</sup>2p<sup>5</sup> | 2, 7                             |         |
| 10                                      | Ne                                        | **[氖](../Page/氖.md "wikilink")**        | [18族](../Page/稀有氣體.md "wikilink")、p<sup>6</sup> | p區                                        | \[He\]2s<sup>2</sup>2p<sup>6</sup> | 2, 8                             |         |

[周](../Category/元素周期表.md "wikilink")
[\*](../Category/第2周期元素.md "wikilink")