**第43届柏林影展**在[柏林當地時間](../Page/柏林.md "wikilink")1993年2月11日到2月22日舉行。本屆[柏林電影節評審團主席是](../Page/柏林電影節.md "wikilink")[中國](../Page/中國.md "wikilink")[導演](../Page/導演.md "wikilink")[張藝謀](../Page/張藝謀.md "wikilink")，金熊獎則由[台灣導演](../Page/台灣.md "wikilink")[李安及中國導演](../Page/李安.md "wikilink")[謝飛共同獲得](../Page/謝飛.md "wikilink")。

## 參展影片

以下電影參與角逐金熊獎：

<table style="width:85%;">
<colgroup>
<col style="width: 25%" />
<col style="width: 17%" />
<col style="width: 21%" />
<col style="width: 21%" />
</colgroup>
<thead>
<tr class="header">
<th><p>中文/英文片名</p></th>
<th><p>導演</p></th>
<th><p>出品国</p></th>
<th><p>演員</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夢遊亞利桑那.md" title="wikilink">夢遊亞利桑那</a></p></td>
<td><p><a href="../Page/艾米爾·庫斯杜力卡.md" title="wikilink">艾米爾·庫斯杜力卡</a></p></td>
<td></td>
<td><p><a href="../Page/強尼·戴普.md" title="wikilink">強尼·戴普</a>、<a href="../Page/傑瑞·路易斯.md" title="wikilink">傑瑞·路易斯</a>、<a href="../Page/費·唐娜薇.md" title="wikilink">費·唐娜薇</a>、<a href="../Page/莉莉·泰勒.md" title="wikilink">莉莉·泰勒</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美好年代.md" title="wikilink">美好年代</a></p></td>
<td><p><a href="../Page/佛南多·楚巴.md" title="wikilink">佛南多·楚巴</a></p></td>
<td></td>
<td><p><a href="../Page/潘妮洛普·克魯茲.md" title="wikilink">潘妮洛普·克魯茲</a>、<a href="../Page/費爾南·戈斯麥.md" title="wikilink">費爾南·戈斯麥</a>、Michel Galabru</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/背叛.md" title="wikilink">背叛</a></p></td>
<td><p><a href="../Page/弗朗斯·薇茲.md" title="wikilink">弗朗斯·薇茲</a></p></td>
<td></td>
<td><p>Renée Soutendijk</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Die_Denunziantin.md" title="wikilink">Die Denunziantin</a></p></td>
<td><p><a href="../Page/湯瑪斯·米切利斯.md" title="wikilink">湯瑪斯·米切利斯</a></p></td>
<td></td>
<td><p>Katharina Thalbach</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香魂女.md" title="wikilink">香魂女</a></p></td>
<td><p><a href="../Page/謝飛.md" title="wikilink">謝飛</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Hoppá.md" title="wikilink">Hoppá</a></p></td>
<td><p><a href="../Page/Gyula_Maár.md" title="wikilink">Gyula Maár</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Jimmy_Hoffa.md" title="wikilink">Jimmy Hoffa</a></p></td>
<td><p><a href="../Page/丹尼·狄維托.md" title="wikilink">丹尼·狄維托</a></p></td>
<td></td>
<td><p><a href="../Page/傑克·尼克遜.md" title="wikilink">傑克·尼克遜</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/喜宴.md" title="wikilink">喜宴</a></p></td>
<td><p><a href="../Page/李安.md" title="wikilink">李安</a></p></td>
<td></td>
<td><p><a href="../Page/郎雄.md" title="wikilink">郎雄</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/少年維特的煩惱.md" title="wikilink">少年維特的煩惱</a></p></td>
<td><p><a href="../Page/賈克·杜瓦雍.md" title="wikilink">賈克·杜瓦雍</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Kærlighedens_smerte.md" title="wikilink">Kærlighedens smerte</a></p></td>
<td><p><a href="../Page/尼尔斯·马尔姆洛斯.md" title="wikilink">尼尔斯·马尔姆洛斯</a>（Nils Malmros）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Das_Leben,_wie_von_Agfa_bezeugt.md" title="wikilink">Das Leben, wie von Agfa bezeugt</a></p></td>
<td><p><a href="../Page/Assi_Dayan.md" title="wikilink">Assi Dayan</a></p></td>
<td></td>
<td><p><a href="../Page/姬拉·艾瑪戈.md" title="wikilink">姬拉·艾瑪戈</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Love_Field.md" title="wikilink">Love Field</a></p></td>
<td><p><a href="../Page/強納森·凱普蘭.md" title="wikilink">強納森·凱普蘭</a></p></td>
<td></td>
<td><p><a href="../Page/蜜雪兒菲佛.md" title="wikilink">蜜雪兒菲佛</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑潮.md" title="wikilink">黑潮</a></p></td>
<td><p><a href="../Page/史派克·李.md" title="wikilink">史派克·李</a></p></td>
<td></td>
<td><p><a href="../Page/丹佐·華盛頓.md" title="wikilink">丹佐·華盛頓</a>、<a href="../Page/安琪拉·貝瑟.md" title="wikilink">安琪拉·貝瑟</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Patul_conjugal.md" title="wikilink">Patul conjugal</a></p></td>
<td><p><a href="../Page/Mircea_Daneliuc.md" title="wikilink">Mircea Daneliuc</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小启示录.md" title="wikilink">小启示录</a>（Petite Apocalypse）</p></td>
<td><p><a href="../Page/科斯塔·加夫拉斯.md" title="wikilink">科斯塔·加夫拉斯</a></p></td>
<td></td>
<td><p>André Dussollier、Pierre Arditi、Jiří Menzel</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Die_Russlandaffäre.md" title="wikilink">Die Russlandaffäre</a></p></td>
<td><p><a href="../Page/Morten_Arnfred.md" title="wikilink">Morten Arnfred</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Samba_Traoré.md" title="wikilink">Samba Traoré</a></p></td>
<td><p><a href="../Page/伊德沙·屋德瑞古.md" title="wikilink">伊德沙·屋德瑞古</a>（Idrissa Ouédraogo）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Sankofa.md" title="wikilink">Sankofa</a></p></td>
<td><p><a href="../Page/海尔·格里玛.md" title="wikilink">海尔·格里玛</a>（Haile Gerima）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Die_Sonne_der_Wachenden.md" title="wikilink">Die Sonne der Wachenden</a></p></td>
<td><p>Temur Babluani</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Tagebuch_einer_Manie.md" title="wikilink">Tagebuch einer Manie</a></p></td>
<td><p><a href="../Page/馬可·菲萊利.md" title="wikilink">馬可·菲萊利</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Telefrafisten.md" title="wikilink">Telefrafisten</a></p></td>
<td><p><a href="../Page/埃里克·古斯达夫逊.md" title="wikilink">埃里克·古斯达夫逊</a>（Erik Gustavson）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/玩具兵團.md" title="wikilink">玩具兵團</a></p></td>
<td><p><a href="../Page/巴瑞·李文森.md" title="wikilink">巴瑞·李文森</a></p></td>
<td></td>
<td><p><a href="../Page/羅賓·威廉斯.md" title="wikilink">羅賓·威廉斯</a>、Michael Gambon、Joan Cusack</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/一個好人(1993年電影).md" title="wikilink">一個好人</a></p></td>
<td><p><a href="../Page/德特福·巴克.md" title="wikilink">德特福·巴克</a></p></td>
<td></td>
<td><p>Joachim Król、Horst Krause、Sophie Rois</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賽門花園.md" title="wikilink">賽門花園</a></p></td>
<td><p><a href="../Page/安德魯·柏金.md" title="wikilink">安德魯·柏金</a></p></td>
<td></td>
<td><p><a href="../Page/夏綠蒂·甘絲柏格.md" title="wikilink">夏綠蒂·甘絲柏格</a></p></td>
</tr>
</tbody>
</table>

## 獲獎名單

[Michelle_Pfeiffer_1994.jpg](https://zh.wikipedia.org/wiki/File:Michelle_Pfeiffer_1994.jpg "fig:Michelle_Pfeiffer_1994.jpg")
本屆柏林電影節頒獎典禮於2月22日舉行。

  - 正式競賽長片
      - [金熊獎](../Page/金熊獎.md "wikilink")：《[囍宴](../Page/喜宴_\(电影\).md "wikilink")》（[台灣](../Page/台灣.md "wikilink")[李安執導](../Page/李安.md "wikilink")）/《[香魂女](../Page/香魂女.md "wikilink")》（[中國](../Page/中國.md "wikilink")[謝飛執導](../Page/謝飛.md "wikilink")）
      - 評審團大獎銀熊獎： [艾米爾·庫斯杜力卡](../Page/艾米爾·庫斯杜力卡.md "wikilink")
        《[夢遊亞利桑那](../Page/夢遊亞利桑那.md "wikilink")》
      - 最佳導演銀熊獎：[安德魯·柏金](../Page/安德魯·柏金.md "wikilink")（Andrew Birkin）
        《[賽門花園](../Page/賽門花園.md "wikilink")》
      - 最佳男演員銀熊獎：[丹佐·華盛頓](../Page/丹佐·華盛頓.md "wikilink") 《*Malcolm X*》
      - 最佳女演員銀熊獎：[蜜雪兒菲佛](../Page/蜜雪兒菲佛.md "wikilink")
        《[槍聲響起](../Page/槍聲響起.md "wikilink")》

<!-- end list -->

  - 其他獎項
      - 最佳劇情片[泰迪熊獎](../Page/泰迪熊獎.md "wikilink")：《[維根斯坦](../Page/維根斯坦.md "wikilink")》（[德里克·加曼執導](../Page/德里克·加曼.md "wikilink")）
      - 最佳紀錄片泰迪熊獎：《[銀色之戀](../Page/銀色之戀.md "wikilink")》（[湯姆·加斯林執導](../Page/湯姆·加斯林.md "wikilink")）
      - 榮譽金熊獎：[葛雷哥萊·畢克](../Page/葛雷哥萊·畢克.md "wikilink")、[比利·懷德](../Page/比利·懷德.md "wikilink")
      - 藍天使獎：《[少年維特的煩惱](../Page/少年維特的煩惱.md "wikilink")》（[賈克·杜瓦雍執導](../Page/賈克·杜瓦雍.md "wikilink")）
      - 柏林金相機獎：[鞏俐](../Page/鞏俐.md "wikilink")、[茱莉葉·畢諾許](../Page/茱莉葉·畢諾許.md "wikilink")、[柯琳娜·哈佛克](../Page/柯琳娜·哈佛克.md "wikilink")

## 評審團

  - [張藝謀](../Page/張藝謀.md "wikilink")，中國導演
  - [布羅克·彼得斯](../Page/布羅克·彼得斯.md "wikilink")，美國演員
  - [蘇珊·史翠絲堡](../Page/蘇珊·史翠絲堡.md "wikilink")（Susan Strasberg），美國演員
  - [法蘭克·貝爾](../Page/法蘭克·貝爾.md "wikilink")（Frank Beyer），德國導演

## 外部連結

  - [柏林影展官方網站](http://www.berlinale.de/en/HomePage.html)

[43](../Category/柏林电影节.md "wikilink")
[Category:1993年電影](../Category/1993年電影.md "wikilink")