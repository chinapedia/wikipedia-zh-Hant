**第44軍**為[大日本帝國陸軍之一](../Page/大日本帝國陸軍.md "wikilink")[軍](../Page/軍_\(軍隊\).md "wikilink")。

1941年7月以**關東防衛軍**為基礎編制成軍，編入[關東軍](../Page/關東軍.md "wikilink")，佈署在[新京地區](../Page/新京.md "wikilink")。1945年5月30日改編為第44軍，編入[第3方面軍](../Page/第3方面軍_\(日本陸軍\).md "wikilink")[作戰序列](../Page/作戰序列.md "wikilink")，佈署在南[滿洲地區](../Page/滿洲國.md "wikilink")，進行與[蘇聯紅軍交戰任務](../Page/蘇聯紅軍.md "wikilink")。駐守[奉天直至終戰](../Page/瀋陽市.md "wikilink")。

## 軍概要

### 第44軍

  - [通稱號](../Page/通稱號.md "wikilink")：遠征
  - 編成時期：[昭和](../Page/昭和.md "wikilink")20年5月30日
  - 最終位置：[奉天](../Page/奉天.md "wikilink")
  - 上級部隊：[第3方面軍](../Page/第3方面軍_\(日本陸軍\).md "wikilink")

### 歷代司令官

#### 關東防衛軍司令官

  - [山下奉文](../Page/山下奉文.md "wikilink") 中將：1941年7月17日 -
  - [草場辰巳](../Page/草場辰巳.md "wikilink") 中將：1941年11月6日 - 1942年12月21日
  - [木下敏](../Page/木下敏.md "wikilink") 中將：1942年12月22日 -
  - [吉田悳](../Page/吉田悳.md "wikilink") 中將：1943年12月7日 -
  - [本鄉義夫](../Page/本鄉義夫.md "wikilink") 中將：1945年3月1日 - 1945年5月30日

#### 第44軍司令官

  - 本鄉義夫 中將：1945年5月30日 -

### 歴代參謀長

#### 關東防衛軍參謀長

  - [吉岡安直](../Page/吉岡安直.md "wikilink") 少將：1941年7月17日 –
  - [田坂専一](../Page/田坂専一.md "wikilink") 少將：1942年7月1日 –
  - [田村浩](../Page/田村浩.md "wikilink") 少將：1944年5月16日 –
  - [小畑信良](../Page/小畑信良.md "wikilink") 少將：1944年10月2日 – 1945年5月30日

#### 第44軍參謀長

  - 小畑信良少將：1945年5月30日 –

### 最終時

  - 司令官 本鄉義夫 中將
  - 參謀長 小畑信良 少將
  - 高級參謀 梅里助就 中佐
  - 高級副官 岡野董 中佐
  - 兵器部長 竹林熊雄 大佐
  - 軍醫部長 井原愛雄 軍醫大佐
  - 獸醫部長 脇坂賢吉 獸醫大佐
  - 法務部長 池田武雄 法務大佐

### 戰闘序列解除時之隷下部隊

  - [第63師團](../Page/第63師團.md "wikilink")
  - [第107師團](../Page/第107師團.md "wikilink")
  - [第117師團](../Page/第117師團.md "wikilink")
  - 獨立戰車第9旅團

## 關連項目

  - [軍事單位](../Page/軍事單位.md "wikilink")
  - [大日本帝國陸軍師團列表](../Page/大日本帝國陸軍師團列表.md "wikilink")
  - [第50軍](../Page/第50軍_\(日本陸軍\).md "wikilink")

## 參考文獻

  - [秦郁彦編](../Page/秦郁彦.md "wikilink")《[日本陸海軍總合事典](../Page/日本陸海軍總合事典.md "wikilink")》第2版、[東京大學出版會](../Page/東京大學出版會.md "wikilink")、2005年。
  - 外山操・森松俊夫編著《[帝国陸軍編制總覽](../Page/帝国陸軍編制總覽.md "wikilink")》[芙蓉書房出版](../Page/芙蓉書房出版.md "wikilink")、1987年。

## 外部連結

  -
  - [帝國陸軍～その制度と人事～](https://web.archive.org/web/20080411094145/http://imperialarmy.hp.infoseek.co.jp/)

  - [日本陸海軍事典](https://web.archive.org/web/20100516180653/http://homepage1.nifty.com/kitabatake/rikukaiguntop.html)

[Category:軍 (日本陸軍)](../Category/軍_\(日本陸軍\).md "wikilink")