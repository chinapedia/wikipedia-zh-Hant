[←第48回](../Page/第48回日劇學院賞名單.md "wikilink") - **第49回** -
[第50回→](../Page/第50回日劇學院賞名單.md "wikilink")

**第49回日劇學院賞**，針對2006年4月到6月在[日本播出的連續劇做出投票與評比](../Page/日本.md "wikilink")。

## 最優秀作品賞

1.  **[醫龍](../Page/醫龍.md "wikilink") Team Medical Dragon**
2.  [詐欺獵人](../Page/詐欺獵人.md "wikilink")
3.  [垃圾律師](../Page/垃圾律師.md "wikilink")
4.  [愛上恐龍妹](../Page/愛上恐龍妹.md "wikilink")
5.  [街頭律師](../Page/街頭律師.md "wikilink")

<!-- end list -->

  - 讀者票：1.詐欺獵人；2.愛上恐龍妹；3.醫龍
  - 記者票：1.垃圾律師；2.醫龍；3.愛上恐龍妹
  - 評審票：1.醫龍；2.街頭律師；3.垃圾律師

## 主演男優賞

1.  **[豐川悅司](../Page/豐川悅司.md "wikilink")（[垃圾律師](../Page/垃圾律師.md "wikilink")）**
2.  [山下智久](../Page/山下智久.md "wikilink")（[詐欺獵人](../Page/詐欺獵人.md "wikilink")）
3.  [坂口憲二](../Page/坂口憲二.md "wikilink")（[醫龍](../Page/醫龍.md "wikilink")）

<!-- end list -->

  - 讀者票：1.山下智久（詐欺獵人）；2.豐川悅司（垃圾律師）；3.坂口憲二（醫龍）
  - 記者票：1.豐川悅司（垃圾律師）；2.坂口憲二（醫龍）；3.[稲垣吾郎](../Page/稲垣吾郎.md "wikilink")（愛上恐龍妹）
  - 評審票：1.豐川悅司（垃圾律師）；2.坂口憲二（醫龍）；3.山下智久（詐欺獵人）

## 主演女優賞

1.  **[天海祐希](../Page/天海祐希.md "wikilink")（[主播台女王](../Page/主播台女王.md "wikilink")）**
2.  [上戶彩](../Page/上戶彩.md "wikilink")（[空姐特訓班](../Page/空姐特訓班.md "wikilink")）
3.  [江角真紀子](../Page/江角真紀子.md "wikilink")（[街頭律師](../Page/街頭律師.md "wikilink")）

<!-- end list -->

  - 讀者票：1.天海祐希（主播台女王）；2.上戶彩（空姐特訓班）；3.[深田恭子](../Page/深田恭子.md "wikilink")（[富豪刑事](../Page/富豪刑事.md "wikilink")）
  - 記者票：1.天海祐希（主播台女王）；2.上戶彩（空姐特訓班）；3.江角真紀子（街頭律師）
  - 評審票：1.天海祐希（主播台女王）；2.江角真紀子（街頭律師）；3.上戶彩（空姐特訓班）

## 助演男優賞

1.  **[伊藤英明](../Page/伊藤英明.md "wikilink")（[垃圾律師](../Page/垃圾律師.md "wikilink")）**
2.  [古田新太](../Page/古田新太.md "wikilink")（[辣妹掌門人](../Page/辣妹掌門人.md "wikilink")）
3.  [小池徹平](../Page/小池徹平.md "wikilink")（[醫龍](../Page/醫龍.md "wikilink")）

<!-- end list -->

  - 讀者票：1.伊藤英明（垃圾律師）；2.[錦戸亮](../Page/錦戸亮.md "wikilink")（空姐特訓班）；3.[山崎努](../Page/山崎努.md "wikilink")（詐欺獵人）
  - 記者票：1.伊藤英明（垃圾律師）；2.小池徹平（醫龍）；3.[古田新太](../Page/古田新太.md "wikilink")（[辣妹掌門人](../Page/辣妹掌門人.md "wikilink")）
  - 評審票：1.伊藤英明（垃圾律師）；2.古田新太（辣妹掌門人）；3.[北村一輝](../Page/北村一輝.md "wikilink")（醫龍）、[阿部貞夫](../Page/阿部貞夫.md "wikilink")（醫龍）、[山本耕史](../Page/山本耕史.md "wikilink")（街頭律師）、山崎努（詐欺獵人）

## 助演女優賞

1.  **[堀北真希](../Page/堀北真希.md "wikilink")（[詐欺獵人](../Page/詐欺獵人.md "wikilink")）**
2.  [村上知子](../Page/村上知子.md "wikilink")（[愛上恐龍妹](../Page/愛上恐龍妹.md "wikilink")）
3.  [矢田亞希子](../Page/矢田亞希子.md "wikilink")（[主播台女王](../Page/主播台女王.md "wikilink")）

<!-- end list -->

  - 讀者票：1.堀北真希（詐欺獵人）；2.村上知子（愛上恐龍妹）；3.矢田亞希子（主播台女王）
  - 記者票：1.堀北真希（詐欺獵人）；2.[戶田惠梨香](../Page/戶田惠梨香.md "wikilink")（辣妹掌門人）；3.村上知子（愛上恐龍妹）
  - 評審票：1.村上知子（愛上恐龍妹）；2.堀北真希（詐欺獵人）；3.矢田亞希子（主播台女王）

## 監督賞

1.  **[久保田哲史等](../Page/久保田哲史.md "wikilink")（[醫龍](../Page/醫龍.md "wikilink")
    Team Medical Dragon）**
2.  [今井夏木](../Page/今井夏木.md "wikilink")、[酒井聖博](../Page/酒井聖博.md "wikilink")、[竹村謙太郎](../Page/竹村謙太郎.md "wikilink")、[森嶋正也](../Page/森嶋正也.md "wikilink")（[垃圾律師](../Page/垃圾律師.md "wikilink")）

## 腳本賞

1.  **[井上由美子](../Page/井上由美子.md "wikilink")（[街頭律師](../Page/街頭律師.md "wikilink")）**
2.  [荒井修子](../Page/荒井修子.md "wikilink")、[瀧本智行](../Page/瀧本智行.md "wikilink")（[垃圾律師](../Page/垃圾律師.md "wikilink")）

## 新人俳優賞

1.  **[村上知子](../Page/村上知子.md "wikilink")（[愛上恐龍妹](../Page/愛上恐龍妹.md "wikilink")）**
2.  [星野亞希](../Page/星野亞希.md "wikilink")（[垃圾律師](../Page/垃圾律師.md "wikilink")）

## 音樂賞

1.  **[河野伸](../Page/河野伸.md "wikilink")、[澤野弘之](../Page/澤野弘之.md "wikilink")（[醫龍](../Page/醫龍.md "wikilink")
    Team Medical Dragon）**
2.  [住友紀人](../Page/住友紀人.md "wikilink")（[愛上恐龍妹](../Page/愛上恐龍妹.md "wikilink")）

## 片頭賞

  - **[空姐特訓班](../Page/空姐特訓班.md "wikilink")**

[49](../Category/日劇學院賞.md "wikilink")
[Category:2006年日本](../Category/2006年日本.md "wikilink")
[Category:2006年电视奖项](../Category/2006年电视奖项.md "wikilink")