[Cannes_2006.jpg](https://zh.wikipedia.org/wiki/File:Cannes_2006.jpg "fig:Cannes_2006.jpg")電影《[花樣年華](../Page/花樣年華.md "wikilink")》的劇照。
\]\]
**第59屆戛纳电影节**于[2006年](../Page/2006年电影.md "wikilink")5月17日（法国当地时间）至5月28日舉行。本屆[坎城影展由](../Page/坎城影展.md "wikilink")[凡松·卡塞](../Page/凡松·卡塞.md "wikilink")（Vincent
Cassel）主持，開幕片是[美國片](../Page/美國.md "wikilink")《[达芬奇密码](../Page/达芬奇密码_\(电影\).md "wikilink")》，閉幕片是[法國片](../Page/法國.md "wikilink")《[尋愛之路](../Page/尋愛之路.md "wikilink")》。本屆評審團主席是[香港導演](../Page/香港.md "wikilink")[王家衛](../Page/王家衛.md "wikilink")，是[坎城影展有史以來第一位亞洲裔評審團主席](../Page/坎城影展.md "wikilink")。[娄烨导演的](../Page/娄烨.md "wikilink")《[颐和园](../Page/颐和园_\(电影\).md "wikilink")》是本屆[坎城影展上亚洲地区唯一入围竞赛单元的影片](../Page/坎城影展.md "wikilink")。

## 評審團

### 競賽單元

  - [王家衛](../Page/王家衛.md "wikilink")：導演。(評審團主席)

  - [莫妮卡·貝魯奇](../Page/莫妮卡·貝魯奇.md "wikilink")：演員。

  - [海伦娜·博纳姆·卡特](../Page/海伦娜·博纳姆·卡特.md "wikilink")：演員。

  - ：導演。

  - [章子怡](../Page/章子怡.md "wikilink")：演員。

  - [山繆·傑克森](../Page/山繆·傑克森.md "wikilink")：演員。

  - ：導演。

  - [蒂姆·罗斯](../Page/蒂姆·罗斯.md "wikilink")：演員。

  - [艾利亞·蘇雷曼](../Page/艾利亞·蘇雷曼.md "wikilink")：導演、演員。

## 官方單元

### 競賽片

<table style="width:146%;">
<colgroup>
<col style="width: 17%" />
<col style="width: 25%" />
<col style="width: 21%" />
<col style="width: 14%" />
<col style="width: 68%" />
</colgroup>
<thead>
<tr class="header">
<th><p>中文片名</p></th>
<th><p>原文片名</p></th>
<th><p>導演</p></th>
<th><p>國家或地區</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《<a href="../Page/玩美女人.md" title="wikilink">玩美女人</a>》</p></td>
<td><p><em>Volver</em></p></td>
<td><p><a href="../Page/佩德羅·阿莫多瓦.md" title="wikilink">佩德羅·阿莫多瓦</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Red Road</em></p></td>
<td><p><a href="../Page/安德莉亞·阿諾德.md" title="wikilink">安德莉亞·阿諾德</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>La Raison du plus faible</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Indigènes</em></p></td>
<td></td>
<td><p>、、、</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Crónica de una fuga</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>İklimler</em></p></td>
<td><p><a href="../Page/努里·比格·錫蘭.md" title="wikilink">努里·比格·錫蘭</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/凡爾賽拜金女.md" title="wikilink">凡爾賽拜金女</a>》</p></td>
<td><p><em>Marie Antoinette</em></p></td>
<td><p><a href="../Page/蘇菲亞·柯波拉.md" title="wikilink">蘇菲亞·柯波拉</a></p></td>
<td><p>、、</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/青春向前行.md" title="wikilink">青春向前行</a>》</p></td>
<td><p><em>Juventude em Marcha</em></p></td>
<td><p><a href="../Page/佩德羅·科斯塔.md" title="wikilink">佩德羅·科斯塔</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/羊男的迷宮.md" title="wikilink">羊男的迷宮</a>》</p></td>
<td><p><em>El Laberinto del Fauno</em></p></td>
<td><p><a href="../Page/葛雷摩·戴托羅.md" title="wikilink">吉勒摩·戴托羅</a></p></td>
<td><p>、</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Flandres</em></p></td>
<td><p><a href="../Page/布魯諾·杜蒙.md" title="wikilink">布魯諾·杜蒙</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Selon Charlie</em></p></td>
<td><p><a href="../Page/妮可·賈西亞.md" title="wikilink">妮可·賈西亞</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Quand j'étais chanteur</em></p></td>
<td><p><a href="../Page/札維耶·賈諾利.md" title="wikilink">札維耶·賈諾利</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/火線交錯.md" title="wikilink">火線交錯</a>》</p></td>
<td><p><em>Babel</em></p></td>
<td><p><a href="../Page/阿利安卓·崗札雷·伊納利圖.md" title="wikilink">阿利安卓·崗札雷·伊納利圖</a></p></td>
<td><p>、、</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Laitakaupungin valot</em></p></td>
<td><p><a href="../Page/阿基·郭利斯馬基.md" title="wikilink">阿基·郭利斯馬基</a></p></td>
<td><p>、、</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/南方傳奇.md" title="wikilink">南方傳奇</a>》</p></td>
<td><p><em>Southland Tales</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/快餐國家.md" title="wikilink">快餐國家</a>》</p></td>
<td><p><em>Fast Food Nation</em></p></td>
<td><p><a href="../Page/理查德·林克莱特.md" title="wikilink">李察·林克雷特</a></p></td>
<td><p>、</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/風吹稻浪.md" title="wikilink">吹動大麥的風</a>》</p></td>
<td><p><em>The Wind That Shakes the Barley</em></p></td>
<td><p><a href="../Page/肯·洛區.md" title="wikilink">肯·洛區</a></p></td>
<td><p>、、、、、</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/頤和園_(電影).md" title="wikilink">頤和園</a>》</p></td>
<td></td>
<td><p><a href="../Page/婁燁.md" title="wikilink">婁燁</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Il caimano</em></p></td>
<td><p><a href="../Page/南尼·莫莱蒂.md" title="wikilink">南尼·莫瑞提</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>L'amico di famiglia</em></p></td>
<td><p><a href="../Page/保羅·索倫蒂諾.md" title="wikilink">保羅·索倫蒂諾</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 一種注目

  - Nikolay KHOMERIKI - **977** (5月27日)
  - Djamshed USMONOV - **POUR ALLER AU CIEL, IL FAUT MOURIR** (5月23日)
  - Rabah AMEUR-ZAÏMECHE - **BLED NUMBER ONE** (5月20日)
  - Catalin MITULESCU - **CUM MI-AM PETRECUT SFARSITUL LUMII**
    《我如何慶祝世界末日》(5月23日)
  - Jacques FIESCHI - **LA CALIFORNIE** (5月26日)
  - [彭氏兄弟](../Page/彭氏兄弟.md "wikilink") - 《鬼域》（Re-Cycle）(5月27日)
  - Paz ENCINA - **HAMACA PARAGUAYA** (5月18日)
  - [王超](../Page/王超.md "wikilink") -
    《[江城夏日](../Page/江城夏日.md "wikilink")》（Luxury
    Car）(5月22日)
  - Patrick GRANDPERRET - **MEURTRIÈRES** (5月21日)
  - 聯合執導 - **PARIS, JE T'AIME**
    《[巴黎我愛你](../Page/巴黎我愛你.md "wikilink")》(5月18日)
  - Marco BELLOCCHIO - **IL REGISTA DI MATRIMONI** (5月20日)
  - Manuel HUERGA - **SALVADOR** (5月23日)
  - Richard LINKLATER - **A SCANNER DARKLY**
    《[心機掃瞄](../Page/心機掃瞄.md "wikilink")》(5月25日)
  - Garin NUGROHO - **SERAMBI** (5月21日)
  - Paul GOLDMAN - **LE FEU SOUS LA PEAU** (5月24日)
  - György PÁLFI - **TAXIDERMIE** (5月19日)
  - Rolf DE HEER - **TEN CANOES** (5月19日)
  - Denis DERCOURT - **LA TOURNEUSE DE PAGES**
    《[情謎變奏曲](../Page/情謎變奏曲.md "wikilink")》(5月19日)
  - Murali K. THALLURI - **2h37** (5月26日)
  - YOON Jong-Bin - **THE UNFORGIVEN** 《兄弟之情斷背未滿》(5月18日)
  - Stefan FALDBAKKEN - **Uro** (5月22日)
  - Francisco VARGAS - **EL VIOLIN** 《小提琴革命曲》(5月24日)
  - Kristijonas VILDZIUNAS - **TOI ETRE MOI** (5月25日)
  - Slawomir FABICKI - **L'HOMME DE MAIN** (5月25日)

## 獎項

本屆[坎城影展頒獎典禮於](../Page/坎城影展.md "wikilink")5月28日舉行。

### 競賽片

  - [金棕櫚獎](../Page/金棕櫚獎.md "wikilink")：[肯·洛區](../Page/肯·洛區.md "wikilink")
    - 《[吹動大麥的風](../Page/吹動大麥的風.md "wikilink")》
  - [評審團大獎](../Page/评审团大奖_\(戛纳电影节\).md "wikilink")：[布魯諾·杜蒙](../Page/布魯諾·杜蒙.md "wikilink")
    - 《》
  - [最佳導演獎](../Page/最佳導演獎_\(坎城影展\).md "wikilink"):：[阿利安卓·崗札雷·伊納利圖](../Page/阿利安卓·崗札雷·伊納利圖.md "wikilink")
    - 《[火線交錯](../Page/火線交錯.md "wikilink")》
  - [最佳劇本獎](../Page/坎城影展最佳劇本獎.md "wikilink")：[佩德羅·阿莫多瓦](../Page/佩德羅·阿莫多瓦.md "wikilink")
    - 《[玩美女人](../Page/玩美女人.md "wikilink")》
  - [最佳男演員獎](../Page/最佳男演員獎_\(坎城影展\).md "wikilink")：[賈梅·德布茲](../Page/賈梅·德布茲.md "wikilink")、[沙米·納西利](../Page/沙米·納西利.md "wikilink")、、、
    - 《》
  - [最佳女演員獎](../Page/最佳女演员奖_\(戛纳电影节\).md "wikilink")：[潘妮洛普·克魯茲](../Page/潘妮洛普·克魯茲.md "wikilink")、[卡門·莫拉](../Page/卡門·莫拉.md "wikilink")、蘿拉·杜納斯、查斯·拉姆普雷芙、布蘭卡·波提羅、尤漢娜·柯博
    - 《[玩美女人](../Page/玩美女人.md "wikilink")》
  - [評審團獎](../Page/坎城影展評審團獎.md "wikilink")：[安德莉亞·阿諾德](../Page/安德莉亞·阿諾德.md "wikilink")
    - 《》

### 競賽短片

  - [短片金棕櫚獎](../Page/短片金棕櫚獎.md "wikilink")：[挪威](../Page/挪威.md "wikilink")，Bobbie
    PEERS - **SNIFFER**
  - 特別提名：[法國](../Page/法國.md "wikilink")，Florence MIAILHE - **CONTE DE
    QUARTIER**

### 一種注目

  - 一種注目獎：[中國](../Page/中國.md "wikilink")，[王超](../Page/王超.md "wikilink")（WANG
    Chao）《[江城夏日](../Page/江城夏日.md "wikilink")》
  - 評審團特別獎：[澳大利亞](../Page/澳大利亞.md "wikilink")，Rolf DE HEER - **TEN
    CANOES**
  - 最佳男演員獎：[墨西哥](../Page/墨西哥.md "wikilink")，Don Angel TAVIRA 在 Francisco
    VARGAS - **EL VIOLIN** 《小提琴革命曲》
  - 最佳女演員獎：[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")，Dorotheea PETRE 在 Catalin
    MITULESCU - **CUM MI-AM PETRECUT SFARSITUL LUMII** 《我如何慶祝世界末日》
  - 評審團主席獎：[法國](../Page/法國.md "wikilink")，Patrick GRANDPERRET -
    **MEURTRIÈRES**

### [導演雙週](../Page/導演雙週.md "wikilink")

  - 藝術論文獎：[義大利](../Page/義大利.md "wikilink")，Kim ROSSI STUART -
    《[屋頂上的童年時光](../Page/屋頂上的童年時光.md "wikilink")》（**ANCHE
    LIBERO VA BENE**）
  - 年輕視野獎：[美國](../Page/美國.md "wikilink")，Julia LOKTEV - 《日夜日夜》（**DAY
    NIGHT, DAY NIGHT**）

### 國際影評人週

  - 評審團大獎：[法國](../Page/法國.md "wikilink")，Emmanuel BOURDIEU -
    《邪惡友誼》（**LES AMITIÉS MALÉFIQUES**）
  - 年輕視野獎：[巴西](../Page/巴西.md "wikilink")，Kirill MIKHANOVSKY - **SONHOS
    DE PEIXE**

### 金攝影機獎

金攝影機獎：[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")，Corneliu PORUMBOIU
–《[有還是沒有？](../Page/有還是沒有？.md "wikilink")》（**A FOST SAU
N-A FOST ?**）

### 電影協會

  - 第一名：[阿根廷](../Page/阿根廷.md "wikilink")，Gustavo RIET - **GE & ZETA**
  - 第二名：[德國](../Page/德國.md "wikilink")，Stefan MUELLER - **MR. SCHWARTZ,
    MR. HAZEN & MR. HORLOCKER**
  - 第三名：[美國](../Page/美國.md "wikilink")，Sian HEDER - **MOTHER** 及
    [匈牙利](../Page/匈牙利.md "wikilink")，Ágnes KOCSIS - **A VÍRUS** 並列

## 外部链接

  - [官方网站](http://www.festival-cannes.fr/index.php?langue=6002)

[59](../Category/戛纳电影节.md "wikilink")
[Category:2006年法国](../Category/2006年法国.md "wikilink")
[\*](../Category/2006年電影.md "wikilink")
[Category:2006年5月](../Category/2006年5月.md "wikilink")