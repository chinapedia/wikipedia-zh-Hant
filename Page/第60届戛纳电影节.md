**第60屆戛纳電影節**於[2007年](../Page/2007年电影.md "wikilink")5月16日（法國當地時間）至5月27日舉行。本屆[戛纳影展由](../Page/戛纳影展.md "wikilink")[黛安·克魯格](../Page/黛安·克魯格.md "wikilink")（Diane
Kruger）主持，開幕片是[香港導演](../Page/香港.md "wikilink")[王家衛](../Page/王家衛.md "wikilink")（前一年的[康城影展評審團主席](../Page/康城影展.md "wikilink")）的《[藍莓之夜](../Page/藍莓之夜.md "wikilink")》，閉幕片是[加拿大導演](../Page/加拿大.md "wikilink")[丹尼斯·阿坎德](../Page/丹尼斯·阿坎德.md "wikilink")（Denys
Arcand）的《黑暗年代》。本屆評審團主席是[英國導演](../Page/英國導演.md "wikilink")[斯蒂芬·弗里爾斯](../Page/斯蒂芬·弗里爾斯.md "wikilink")（Stephen
Frears）。

## 得獎名單

本屆[坎城影展頒獎典禮於](../Page/坎城影展.md "wikilink")5月27日舉行。

### 正式競賽長片

  -   - [金棕櫚獎](../Page/金棕櫚獎.md "wikilink")：《[4个月3星期零2天](../Page/4个月3星期零2天.md "wikilink")》
      - 評審團大獎：《[殯之森](../Page/殯之森.md "wikilink")》
      - 最佳導演：[朱利安·許納貝](../Page/朱利安·許納貝.md "wikilink")
        -《[潛水鐘與蝴蝶](../Page/潛水鐘與蝴蝶.md "wikilink")》
      - 最佳劇本：法提·阿金 - 《天堂邊緣》
      - 最佳女演員：[全道嬿](../Page/全道嬿.md "wikilink")－[李滄東](../Page/李滄東.md "wikilink")《[密陽](../Page/密陽.md "wikilink")》
      - 最佳男演員：[康斯坦丁·拉朗尼柯](../Page/康斯坦丁·拉朗尼柯.md "wikilink")（Konstantin
        LAVRONENKO）－安德烈·薩金塞夫 《[放逐](../Page/放逐（俄國片）.md "wikilink")》
      - 評審團獎：《[我在伊朗長大](../Page/我在伊朗長大.md "wikilink")》、《[寂靜之光](../Page/寂靜之光.md "wikilink")》並列
      - 六十週年紀念獎：[葛斯·范·桑](../Page/葛斯·范·桑.md "wikilink")
        -《[迷幻公園](../Page/迷幻公園_\(電影\).md "wikilink")》
      - 金攝影機：Etgar KERET 跟 Shira GEFFEN 的《蛇髮女妖》（**MEDUZOT**）；特別提名：Anton
        CORBIJN 的 《控制》 （**CONTROL**）

### 一種注目

  -   - 最佳影片：Cristian NEMESCU - **CALIFORNIA DREAMIN' (NESFARSIT)**
        《加州夢》（未完成）
      - 評審團特別獎：Valeria BRUNI-TEDESCHI - **LE RÊVE DE LA NUIT D'AVANT**
        《前一夜的夢》
      - 評審團心動獎：Eran KOLIRIN - 《樂隊造訪》

## 評審團

### 正式競賽長片評審團

  - 評審團主席：Stephen FREARS
    ─[斯蒂芬·弗里爾斯](../Page/斯蒂芬·弗里爾斯.md "wikilink")，[英國導演](../Page/英國.md "wikilink")。
  - [張曼玉](../Page/張曼玉.md "wikilink")（Maggie
    CHEUNG），[香港女演員](../Page/香港.md "wikilink")。
  - Toni COLLETTE
    ──[東妮·寇雷特](../Page/東妮·寇雷特.md "wikilink")，[澳大利亞女演員](../Page/澳大利亞.md "wikilink")。
  - Maria DE MEDEIROS
    ──[瑪莉亞·德·梅德羅斯](../Page/瑪莉亞·德·梅德羅斯.md "wikilink")，[葡萄牙女演員兼導演](../Page/葡萄牙.md "wikilink")。
  - Sarah POLLEY
    ──[莎拉·波利](../Page/莎拉·波利.md "wikilink")，[加拿大女演員兼導演](../Page/加拿大.md "wikilink")。
  - Marco BELLOCCHIO
    ──[馬可·貝洛凱歐](../Page/馬可·貝洛凱歐.md "wikilink")，[義大利男導演](../Page/義大利.md "wikilink")。
  - Orhan PAMUK ──
    [奧罕·帕慕克](../Page/奧罕·帕慕克.md "wikilink")，[土耳其男作家](../Page/土耳其.md "wikilink")。
  - Michel PICOLLI
    ──[米歇·皮柯里](../Page/米歇·皮柯里.md "wikilink")，[法國男演員兼導演](../Page/法國.md "wikilink")。
  - Abderrahmane SISSAKO
    ──[阿布代拉曼·西沙可](../Page/阿布代拉曼·西沙可.md "wikilink")，[茅利塔尼亞男導演](../Page/茅利塔尼亞.md "wikilink")。

### 一種注目評審團

  - 評審團主席：Pascale FERRAN
    ──[帕思卡兒·費宏](../Page/帕思卡兒·費宏.md "wikilink")，[法國女導演](../Page/法國.md "wikilink")
  - Jasmine TRINCA
    ──[亞思敏·特林卡](../Page/亞思敏·特林卡.md "wikilink")，[義大利女演員](../Page/義大利.md "wikilink")
  - Cristi PUIU
    ──[克里斯提·普優](../Page/克里斯提·普優.md "wikilink")，[羅馬尼亞男導演](../Page/羅馬尼亞.md "wikilink")
  - Kent JONES ──肯特·瓊斯，[美國男影評人](../Page/美國.md "wikilink")
  - Bian QIN

## 參展影片

  - 開幕片：《[藍莓之夜](../Page/藍莓之夜.md "wikilink")》（*My Blueberry
    Nights*），[王家衛執導](../Page/王家衛.md "wikilink")。

### 正式競賽長片

<table style="width:74%;">
<colgroup>
<col style="width: 22%" />
<col style="width: 22%" />
<col style="width: 18%" />
<col style="width: 11%" />
</colgroup>
<thead>
<tr class="header">
<th><p>中文片名</p></th>
<th><p>原文片名</p></th>
<th><p>導演</p></th>
<th><p>國家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《<a href="../Page/4個月3星期零2天.md" title="wikilink">4月3週又2天</a>》</p></td>
<td><p><em>4 luni, 3 săptămâni şi 2 zile</em></p></td>
<td><p><a href="../Page/克里斯汀·穆基.md" title="wikilink">克里斯汀·穆基</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Александра</em></p></td>
<td><p><a href="../Page/亞歷山大·蘇古諾夫.md" title="wikilink">亞歷山大·蘇古諾夫</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Изгнание</em></p></td>
<td><p><a href="../Page/安德烈·薩金塞夫.md" title="wikilink">安德烈·薩金塞夫</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>숨</em></p></td>
<td><p><a href="../Page/金基德.md" title="wikilink">金基德</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Death Proof</em></p></td>
<td><p><a href="../Page/昆汀·塔倫提諾.md" title="wikilink">昆汀·塔倫提諾</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/潛水鐘與蝴蝶_(電影).md" title="wikilink">潛水鐘與蝴蝶</a>》</p></td>
<td><p><em>Le scaphandre et le papillon</em></p></td>
<td><p><a href="../Page/朱利安·許納貝.md" title="wikilink">朱利安·許納貝</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/天堂邊緣.md" title="wikilink">天堂邊緣</a>》</p></td>
<td><p><em>Auf der anderen Seite</em></p></td>
<td><p><a href="../Page/法提·阿金.md" title="wikilink">法提·阿金</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Import Export</em></p></td>
<td><p><a href="../Page/伍瑞克·賽德爾.md" title="wikilink">伍瑞克·賽德爾</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/情慾2重奏.md" title="wikilink">情慾2重奏</a>》</p></td>
<td><p><em>Une vieille maîtresse</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Les chansons d'amour</em></p></td>
<td><p><a href="../Page/克里斯多福·歐諾黑.md" title="wikilink">克里斯多福·歐諾黑</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>A londoni férfi</em></p></td>
<td><p><a href="../Page/貝拉·塔爾.md" title="wikilink">貝拉·塔爾</a></p></td>
<td><p>、、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>殯の森</em></p></td>
<td><p><a href="../Page/河瀨直美.md" title="wikilink">河瀨直美</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/我的藍莓夜.md" title="wikilink">我的藍莓夜</a>》</p></td>
<td><p><em>My Blueberry Nights</em></p></td>
<td><p><a href="../Page/王家衛.md" title="wikilink">王家衛</a></p></td>
<td><p>、、</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/險路勿近.md" title="wikilink">險路勿近</a>》</p></td>
<td><p><em>No Country for Old Men</em></p></td>
<td><p><a href="../Page/科恩兄弟.md" title="wikilink">科恩兄弟</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/迷幻公園_(電影).md" title="wikilink">迷幻公園</a>》</p></td>
<td><p><em>Paranoid Park</em></p></td>
<td><p><a href="../Page/葛斯·范·桑.md" title="wikilink">葛斯·范·桑</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/茉莉人生.md" title="wikilink">茉莉人生</a>》</p></td>
<td><p><em>Persepolis</em></p></td>
<td><p><a href="../Page/瑪嘉·莎塔碧.md" title="wikilink">瑪嘉·莎塔碧</a>、</p></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Завет</em></p></td>
<td><p><a href="../Page/艾米爾·庫斯杜力卡.md" title="wikilink">艾米爾·庫斯杜力卡</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/密陽_(電影).md" title="wikilink">密陽</a>》</p></td>
<td><p><em>밀양</em></p></td>
<td><p><a href="../Page/李滄東.md" title="wikilink">李滄東</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/寂靜之光.md" title="wikilink">寂靜之光</a>》</p></td>
<td><p><em>Stellet Lijcht</em></p></td>
<td><p><a href="../Page/卡洛斯·雷加達斯.md" title="wikilink">卡洛斯·雷加達斯</a></p></td>
<td><p>、、、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Tehilim</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>We Own the Night</em></p></td>
<td><p><a href="../Page/傑姆士·葛瑞.md" title="wikilink">傑姆士·葛瑞</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/索命黃道帶.md" title="wikilink">索命黃道帶</a>》</p></td>
<td><p><em>Zodiac</em></p></td>
<td><p><a href="../Page/大卫·芬奇.md" title="wikilink">大卫·芬奇</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 一種注目

  - [侯孝賢](../Page/侯孝賢.md "wikilink")（HOU, Hsiao Hsien ）-
    《[紅氣球之旅](../Page/紅氣球之旅.md "wikilink")》
  - [瓦莱丽亚·布诺妮-泰特琪](../Page/瓦莱丽亚·布诺妮-泰特琪.md "wikilink") (Valeria
    BRUNI-TEDESCHI) – **LE RÊVE DE LA NUIT D'AVANT** 《前一夜的夢》
  - Carmen CASTILLO – **CALLE SANTA FE** 《聖塔菲路》
  - Lee Isaac CHUNG – **MUNYURANGABO** 《自由日》
  - [刁亦男](../Page/刁亦男.md "wikilink")（DIAO Yinan）
    –《[夜行列車](../Page/夜行列車.md "wikilink")》（NIGHT
    TRAIN）
  - Lola DOILLON – **ET TOI T'ES SUR QUI ?** 《你咧你支持誰？》
  - Enrique FERNÁNDES 及 César CHARLONE – **EL BAÑO DEL PAPA** 《教宗之浴》
  - Eran KOLIRIN – **BIKUR HATIZMORET** 《樂隊造訪》
  - Harmony KORINE – **MISTER LONELY** 《寂寞先生》
  - Kadri KÕUSAAR – **MAGNUS** 《馬努斯》
  - [李楊](../Page/李楊.md "wikilink")（LI Yang）
    –《[盲山](../Page/盲山.md "wikilink")》
  - Daniele LUCHETTI – **MIO FRATELLO È FIGLIO UNICO** 《我弟弟是獨生子》
  - Cristian NEMESCU – **CALIFORNIA DREAMIN' (NESFARSIT)** 《加州夢》（未完成）
  - Jaime ROSALES – **LA SOLEDAD** 《孤獨》
  - [巴贝·施洛德](../Page/巴贝·施洛德.md "wikilink")(Barbet SCHROEDER) –
    **L'AVOCAT DE LA TERREUR** 《恐怖律師》
  - Céline SCIAMMA – **NAISSANCE DES PIEUVRES** 《章魚的誕生》
  - Robert THALHEIM – **AM ENDE KOMMEN TOURISTEN** 《然後是觀光客》
  - Ekachai UEKRONGTHAM – **KUAILE GONGCHANG** 《快樂工廠》

## 外部連結

  - [康城影展官方網站](http://www.festival-cannes.fr/)

[60](../Category/戛纳电影节.md "wikilink")
[Category:2007年法国](../Category/2007年法国.md "wikilink")
[\*](../Category/2007年电影.md "wikilink")
[Category:2007年5月](../Category/2007年5月.md "wikilink")