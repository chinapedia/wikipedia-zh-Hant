**第6方面軍**為[大日本帝國陸軍中之一](../Page/大日本帝國陸軍.md "wikilink")[方面軍](../Page/方面軍.md "wikilink")。[太平洋戰爭末期統統括在](../Page/太平洋戰爭.md "wikilink")[華中方面之各軍而編成之方面軍](../Page/華中.md "wikilink")。
[昭和](../Page/昭和.md "wikilink")19年（1944年）8月25日軍部下令編入編成後[中国派遣軍的](../Page/中国派遣軍.md "wikilink")[作戰序列](../Page/作戰序列.md "wikilink")。終戰時駐在[漢口](../Page/漢口.md "wikilink")。

## 概要

  - 通稱號：統
  - 編成時期：昭和19年8月25日
  - 最終位置：[漢口](../Page/漢口.md "wikilink")
  - 上級部隊：[中国派遣軍](../Page/中国派遣軍.md "wikilink")

## 歷代司令官

  - [岡村寧次大將](../Page/岡村寧次.md "wikilink")
    [昭和](../Page/昭和.md "wikilink")19年8月25日 - 昭和19年11月22日
  - [岡部直三郎大將](../Page/岡部直三郎.md "wikilink") 昭和19年11月22日 - 終戰(昭和20年9月)

## 歷代参謀長

  - [宮崎周一少將](../Page/宮崎周一.md "wikilink")
    [昭和](../Page/昭和.md "wikilink")19年8月25日 - 昭和19年12月14日
  - [唐川安夫少將](../Page/唐川安夫.md "wikilink") 昭和19年12月14日 - 昭和20年4月23日
  - [中山貞武少將](../Page/中山貞武.md "wikilink") 昭和20年4月23日 - 終戰

### 參謀副長

  - [天野正一少將](../Page/天野正一.md "wikilink")
    [昭和](../Page/昭和.md "wikilink")19年8月25日 - 昭和20年2月12日
  - [福富伴藏少將](../Page/福富伴藏.md "wikilink") 昭和20年2月12日 - 終戰

## 終戰時之隷下部隊

  - [第11軍](../Page/第11軍_\(日本陸軍\).md "wikilink")
      - [第58師團](../Page/第58師團.md "wikilink")
      - 獨立混成第22旅團
      - 獨立混成第88旅團

<!-- end list -->

  - [第20軍](../Page/第20軍_\(日本陸軍\).md "wikilink")
      - [第64師團](../Page/第64師團.md "wikilink")
      - [第68師團](../Page/第68師團.md "wikilink")
      - [第116師團](../Page/第116師團.md "wikilink")
      - 獨立混成第81旅團
      - 獨立混成第82旅團
      - 獨立混成第86旅團
      - 獨立混成第87旅團
      - 第2独立警備隊

<!-- end list -->

  - [第132師團](../Page/第132師團.md "wikilink")
  - 獨立混成第17旅團
  - 獨立混成第83旅團
  - 獨立混成第84旅團
  - 獨立混成第85旅團
  - 獨立歩兵第5旅團
  - 獨立歩兵第6旅團
  - 獨立歩兵第11旅團
  - 獨立歩兵第12旅團

## 關連項目

  - [軍事單位](../Page/軍事單位.md "wikilink")
  - [第18方面軍](../Page/第18方面軍_\(日本陸軍\).md "wikilink")
  - [第5方面軍](../Page/第5方面軍_\(日本陸軍\).md "wikilink")
  - [第8方面軍](../Page/第8方面軍_\(日本陸軍\).md "wikilink")

## 參考文獻

  - [秦郁彦編](../Page/秦郁彦.md "wikilink")『[日本陸海軍總合事典](../Page/日本陸海軍總合事典.md "wikilink")』第2版、[東京大學出版會](../Page/東京大學出版會.md "wikilink")、2005年。
  - 外山操・森松俊夫編著『[帝國陸軍編制總覽](../Page/帝國陸軍編制總覽.md "wikilink")』[芙蓉書房出版](../Page/芙蓉書房出版.md "wikilink")、1987年。

## 外部連結

  -
  - [帝國陸軍～その制度と人事～](https://web.archive.org/web/20080411094145/http://imperialarmy.hp.infoseek.co.jp/)

  - [日本陸海軍事典](https://web.archive.org/web/20100516180653/http://homepage1.nifty.com/kitabatake/rikukaiguntop.html)

[Category:方面軍 (日本陸軍)](../Category/方面軍_\(日本陸軍\).md "wikilink")