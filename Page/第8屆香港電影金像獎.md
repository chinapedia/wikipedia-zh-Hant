**第8屆香港電影金像獎**於1989年4月9日晚上在[香港會議展覽中心舉行](../Page/香港會議展覽中心.md "wikilink")，由[電影雙週刊與](../Page/電影雙週刊.md "wikilink")[香港影業協會聯合主辦](../Page/香港影業協會.md "wikilink")、[香港電影導演會協辦](../Page/香港電影導演會.md "wikilink")，提名及獲獎名單如下。

## 提名及獲獎名單

### 最佳電影

<table style="width:80%;">
<colgroup>
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</p></td>
<td><ul>
<li>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</li>
<li>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
<li>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
<li>《<a href="../Page/童黨.md" title="wikilink">童黨</a>》</li>
<li>《<a href="../Page/雞同鴨講.md" title="wikilink">雞同鴨講</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳導演

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/關錦鵬.md" title="wikilink">關錦鵬</a></p></td>
<td><p>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</p></td>
<td><ul>
<li><a href="../Page/王家衛.md" title="wikilink">王家衛</a>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
<li><a href="../Page/張之亮.md" title="wikilink">張之亮</a>《<a href="../Page/中國最後一個太監.md" title="wikilink">中國最後一個太監</a>》</li>
<li><a href="../Page/劉國昌.md" title="wikilink">劉國昌</a>《<a href="../Page/童黨.md" title="wikilink">童黨</a>》</li>
<li><a href="../Page/羅啟銳.md" title="wikilink">羅啟銳</a>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</li>
<li><a href="../Page/關錦鵬.md" title="wikilink">關錦鵬</a>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳編劇

<table style="width:80%;">
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1：<a href="../Page/聶宏風.md" title="wikilink">聶宏風</a>、<a href="../Page/邵國華.md" title="wikilink">邵國華</a>、<a href="../Page/陳嘉上.md" title="wikilink">陳嘉上</a>、<a href="../Page/葉廣儉.md" title="wikilink">葉廣儉</a><br />
2：<a href="../Page/邱戴安平.md" title="wikilink">邱戴安平</a>、<a href="../Page/李碧華.md" title="wikilink">李碧華</a></p></td>
<td><p>1：《<a href="../Page/三人世界.md" title="wikilink">三人世界</a>》<br />
2：《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</p></td>
<td><ul>
<li><a href="../Page/羅啟銳.md" title="wikilink">羅啟銳</a>、<a href="../Page/張婉婷.md" title="wikilink">張婉婷</a>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</li>
<li><a href="../Page/聶宏風.md" title="wikilink">聶宏風</a>、<a href="../Page/邵國華.md" title="wikilink">邵國華</a>、<a href="../Page/陳嘉上.md" title="wikilink">陳嘉上</a>、<a href="../Page/葉廣儉.md" title="wikilink">葉廣儉</a>《<a href="../Page/三人世界.md" title="wikilink">三人世界</a>》</li>
<li><a href="../Page/方令正.md" title="wikilink">方令正</a>《<a href="../Page/中國最後一個太監.md" title="wikilink">中國最後一個太監</a>》</li>
<li>方令正《<a href="../Page/我愛太空人.md" title="wikilink">我愛太空人</a>》</li>
<li><a href="../Page/邱戴安平.md" title="wikilink">邱戴安平</a>、<a href="../Page/李碧華.md" title="wikilink">李碧華</a>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳男主角

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/洪金寶.md" title="wikilink">洪金寶</a></p></td>
<td><p>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</p></td>
<td><ul>
<li><a href="../Page/洪金寶.md" title="wikilink">洪金寶</a>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</li>
<li><a href="../Page/莫少聰.md" title="wikilink">莫少聰</a>《<a href="../Page/中國最後一個太監.md" title="wikilink">中國最後一個太監</a>》</li>
<li><a href="../Page/許冠文.md" title="wikilink">許冠文</a>《<a href="../Page/雞同鴨講.md" title="wikilink">雞同鴨講</a>》</li>
<li><a href="../Page/張國榮.md" title="wikilink">張國榮</a>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
<li><a href="../Page/劉德華.md" title="wikilink">劉德華</a>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳女主角

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a></p></td>
<td><p>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</p></td>
<td><ul>
<li><a href="../Page/恬妞.md" title="wikilink">恬妞</a>《<a href="../Page/我愛太空人.md" title="wikilink">我愛太空人</a>》</li>
<li><a href="../Page/張曼玉.md" title="wikilink">張曼玉</a>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
<li><a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
<li><a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>《<a href="../Page/三人世界.md" title="wikilink">三人世界</a>》</li>
<li><a href="../Page/繆騫人.md" title="wikilink">繆騫人</a>《<a href="../Page/繼續跳舞.md" title="wikilink">繼續跳舞</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳男配角

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/張學友.md" title="wikilink">張學友</a></p></td>
<td><p>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</p></td>
<td><ul>
<li><a href="../Page/午馬.md" title="wikilink">午馬</a>《<a href="../Page/中國最後一個太監.md" title="wikilink">中國最後一個太監</a>》</li>
<li><a href="../Page/周星馳.md" title="wikilink">周星馳</a>《<a href="../Page/霹靂先鋒.md" title="wikilink">霹靂先鋒</a>》</li>
<li><a href="../Page/張學友.md" title="wikilink">張學友</a>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
<li><a href="../Page/曾志偉.md" title="wikilink">曾志偉</a>《<a href="../Page/我愛太空人.md" title="wikilink">我愛太空人</a>》</li>
<li><a href="../Page/萬梓良.md" title="wikilink">萬梓良</a>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳女配角

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/李麗蕊.md" title="wikilink">李麗蕊</a></p></td>
<td><p>《<a href="../Page/學校風雲.md" title="wikilink">學校風雲</a>》</p></td>
<td><ul>
<li><a href="../Page/李麗蕊.md" title="wikilink">李麗蕊</a>《<a href="../Page/學校風雲.md" title="wikilink">學校風雲</a>》</li>
<li><a href="../Page/金燕玲.md" title="wikilink">金燕玲</a>《<a href="../Page/我要逃亡.md" title="wikilink">我要逃亡</a>》</li>
<li><a href="../Page/劉美君.md" title="wikilink">劉美君</a>《<a href="../Page/法中情.md" title="wikilink">法中情</a>》</li>
<li><a href="../Page/繆騫人.md" title="wikilink">繆騫人</a>《<a href="../Page/我愛太空人.md" title="wikilink">我愛太空人</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳新演員

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/吳大維.md" title="wikilink">吳大維</a></p></td>
<td><p>《<a href="../Page/今夜星光燦爛.md" title="wikilink">今夜星光燦爛</a>》</p></td>
<td><ul>
<li><a href="../Page/吳大維.md" title="wikilink">吳大維</a>《<a href="../Page/今夜星光燦爛.md" title="wikilink">今夜星光燦爛</a>》</li>
<li><a href="../Page/吳君如.md" title="wikilink">吳君如</a>《<a href="../Page/霸王花.md" title="wikilink">霸王花</a>》</li>
<li><a href="../Page/何沛東.md" title="wikilink">何沛東</a>《<a href="../Page/童黨.md" title="wikilink">童黨</a>》</li>
<li><a href="../Page/周星馳.md" title="wikilink">周星馳</a>《<a href="../Page/霹靂先鋒.md" title="wikilink">霹靂先鋒</a>》</li>
<li><a href="../Page/周慧敏.md" title="wikilink">周慧敏</a>《<a href="../Page/三人世界.md" title="wikilink">三人世界</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳攝影

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鍾志文.md" title="wikilink">鍾志文</a></p></td>
<td><p>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</p></td>
<td><ul>
<li><a href="../Page/鍾志文.md" title="wikilink">鍾志文</a>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</li>
<li><a href="../Page/劉偉強.md" title="wikilink">劉偉強</a>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
<li><a href="../Page/黃仲標.md" title="wikilink">黃仲標</a>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
<li><a href="../Page/鮑德熹.md" title="wikilink">鮑起鳴</a>、<a href="../Page/潘恒生.md" title="wikilink">潘恒生</a>《<a href="../Page/喜寶.md" title="wikilink">喜寶</a>》</li>
<li>黃仲標《<a href="../Page/群鶯亂舞.md" title="wikilink">群鶯亂舞</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳剪接

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/張耀宗.md" title="wikilink">張耀宗</a></p></td>
<td><p>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</p></td>
<td><ul>
<li><a href="../Page/余純.md" title="wikilink">余純</a>，<a href="../Page/鄺志良.md" title="wikilink">鄺志良</a>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</li>
<li><a href="../Page/蔣彼得.md" title="wikilink">蔣彼得</a>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
<li><a href="../Page/張耀宗.md" title="wikilink">張耀宗</a>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
<li><a href="../Page/火山.md" title="wikilink">火山</a>、<a href="../Page/郭強.md" title="wikilink">郭強</a>、<a href="../Page/劉國昌.md" title="wikilink">劉國昌</a>《<a href="../Page/童黨.md" title="wikilink">童黨</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳美術指導

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/張叔平.md" title="wikilink">張叔平</a></p></td>
<td><p>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</p></td>
<td><ul>
<li><a href="../Page/黃仁逵.md" title="wikilink">黃仁逵</a>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</li>
<li><a href="../Page/李仁港.md" title="wikilink">李仁港</a>《<a href="../Page/今夜星光燦爛.md" title="wikilink">今夜星光燦爛</a>》</li>
<li><a href="../Page/張叔平.md" title="wikilink">張叔平</a>《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
<li><a href="../Page/朴若木.md" title="wikilink">朴若木</a>、<a href="../Page/馬光榮.md" title="wikilink">馬光榮</a>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳動作指導

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/成家班.md" title="wikilink">成家班</a></p></td>
<td><p>《<a href="../Page/警察故事續集.md" title="wikilink">警察故事續集</a>》</p></td>
<td><ul>
<li><a href="../Page/劉家班.md" title="wikilink">劉家班</a>《<a href="../Page/老虎出更.md" title="wikilink">老虎出更</a>》</li>
<li><a href="../Page/洪家班.md" title="wikilink">洪家班</a>、<a href="../Page/成家班.md" title="wikilink">成家班</a>《<a href="../Page/飛龍猛將.md" title="wikilink">飛龍猛將</a>》</li>
<li><a href="../Page/成家班.md" title="wikilink">成家班</a>《<a href="../Page/警察故事續集.md" title="wikilink">警察故事續集</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳電影配樂

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/黎小田.md" title="wikilink">黎小田</a></p></td>
<td><p>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</p></td>
<td><ul>
<li><a href="../Page/盧冠廷.md" title="wikilink">盧冠廷</a>《<a href="../Page/七小福_(1988年電影).md" title="wikilink">七小福</a>》</li>
<li><a href="../Page/鍾定一.md" title="wikilink">鍾定一</a>《<a href="../Page/三人世界.md" title="wikilink">三人世界</a>》</li>
<li>鍾定一《<a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a>》</li>
<li><a href="../Page/黎小田.md" title="wikilink">黎小田</a>《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》</li>
<li><a href="../Page/羅永暉.md" title="wikilink">羅永暉</a>《<a href="../Page/群鶯亂舞.md" title="wikilink">群鶯亂舞</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳電影歌曲

<table style="width:80%;">
<colgroup>
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>胭脂扣《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》<br />
作曲：<a href="../Page/黎小田.md" title="wikilink">黎小田</a><br />
填詞：<a href="../Page/鄧景生.md" title="wikilink">鄧景生</a><br />
主唱：<a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a></p></td>
<td><ul>
<li>大丈夫日記《<a href="../Page/大丈夫日記.md" title="wikilink">大丈夫日記</a>》<br />
作曲／填詞：<a href="../Page/黃霑.md" title="wikilink">黃霑</a><br />
主唱：<a href="../Page/周潤發.md" title="wikilink">周潤發</a></li>
<li>故園風雪後《<a href="../Page/今夜星光燦爛.md" title="wikilink">今夜星光燦爛</a>》<br />
作曲：<a href="../Page/林子祥.md" title="wikilink">林子祥</a><br />
填詞：<a href="../Page/陳少琪.md" title="wikilink">陳少琪</a><br />
主唱：林子祥</li>
<li>胭脂扣《<a href="../Page/胭脂扣.md" title="wikilink">胭脂扣</a>》<br />
作曲：<a href="../Page/黎小田.md" title="wikilink">黎小田</a><br />
填詞：<a href="../Page/鄧景生.md" title="wikilink">鄧景生</a><br />
主唱：<a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a></li>
<li>夜溫柔《<a href="../Page/群鶯亂舞.md" title="wikilink">群鶯亂舞</a>》<br />
作曲：<a href="../Page/羅永暉.md" title="wikilink">羅永暉</a><br />
填詞：<a href="../Page/林振強.md" title="wikilink">林振強</a><br />
主唱：<a href="../Page/何嘉麗_(歌手).md" title="wikilink">何嘉麗</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 十大華語片

1.  《[八星报喜](../Page/八星报喜.md "wikilink")》

2.  《[警察故事續集](../Page/警察故事續集.md "wikilink")》

3.  《[飛龍猛將](../Page/飛龍猛將.md "wikilink")》

4.  《[雞同鴨講](../Page/雞同鴨講.md "wikilink")》

5.  《老虎出更》

6.  《[富贵再逼人](../Page/富贵再逼人.md "wikilink")》

7.  《三人世界》

8.  《[公子多情](../Page/公子多情.md "wikilink")》

9.  《[最佳損友](../Page/最佳損友.md "wikilink")》

10. 《[大丈夫日记](../Page/大丈夫日记.md "wikilink")》

### 十大外語片

1.  /  / 《[末代皇帝](../Page/末代皇帝_\(电影\).md "wikilink")》

2.  《》

3.  《[終極警探](../Page/終極警探.md "wikilink")》

4.  《[龙猫](../Page/龙猫.md "wikilink")》

5.  《[風之谷](../Page/風之谷_\(電影\).md "wikilink")》

6.  《[致命的吸引力](../Page/致命的吸引力.md "wikilink")》

7.  《》

8.  《[飛進未來](../Page/飛進未來.md "wikilink")》

9.  《[过关斩将](../Page/过关斩将.md "wikilink")》

10. / 《[烈血焚城](../Page/烈血焚城.md "wikilink")》

## 獎項統計

|        |                                                                                                                                                                                                                                                                                                                                                             |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **數目** | **電影名稱**                                                                                                                                                                                                                                                                                                                                                    |
| **獲獎** |                                                                                                                                                                                                                                                                                                                                                             |
| 7      | [胭脂扣](../Page/胭脂扣.md "wikilink")                                                                                                                                                                                                                                                                                                                            |
| 2      | [七小福](../Page/七小福_\(1988年電影\).md "wikilink")、[旺角卡門](../Page/旺角卡門.md "wikilink")                                                                                                                                                                                                                                                                             |
| 1      | [三人世界](../Page/三人世界.md "wikilink")、[學校風雲](../Page/學校風雲.md "wikilink")、[今夜星光燦爛](../Page/今夜星光燦爛.md "wikilink")、[警察故事續集](../Page/警察故事續集.md "wikilink")                                                                                                                                                                                                         |
| **提名** |                                                                                                                                                                                                                                                                                                                                                             |
| 10     | [旺角卡門](../Page/旺角卡門.md "wikilink")、[胭脂扣](../Page/胭脂扣.md "wikilink")                                                                                                                                                                                                                                                                                         |
| 8      | [七小福](../Page/七小福_\(1988年電影\).md "wikilink")                                                                                                                                                                                                                                                                                                                |
| 4      | [童黨](../Page/童黨.md "wikilink")、[中國最後一個太監](../Page/中國最後一個太監.md "wikilink")、[三人世界](../Page/三人世界.md "wikilink")、[我愛太空人](../Page/我愛太空人.md "wikilink")                                                                                                                                                                                                           |
| 3      | [今夜星光燦爛](../Page/今夜星光燦爛.md "wikilink")、[群鶯亂舞](../Page/群鶯亂舞.md "wikilink")                                                                                                                                                                                                                                                                                   |
| 2      | [雞同鴨講](../Page/雞同鴨講.md "wikilink")、[霹靂先鋒](../Page/霹靂先鋒.md "wikilink")                                                                                                                                                                                                                                                                                       |
| 1      | [繼續跳舞](../Page/繼續跳舞.md "wikilink")、[學校風雲](../Page/學校風雲.md "wikilink")、[我要逃亡](../Page/我要逃亡.md "wikilink")、[法中情](../Page/法中情.md "wikilink")、[霸王花](../Page/霸王花.md "wikilink")、[喜寶](../Page/喜寶.md "wikilink")、[老虎出更](../Page/老虎出更.md "wikilink")、[飛龍猛將](../Page/飛龍猛將.md "wikilink")、[警察故事續集](../Page/警察故事續集.md "wikilink")、[大丈夫日記](../Page/大丈夫日記.md "wikilink") |
|        |                                                                                                                                                                                                                                                                                                                                                             |

## 相關條目

  - [香港電影金像獎](../Page/香港電影金像獎.md "wikilink")
  - [金馬獎](../Page/金馬獎.md "wikilink")

## 參考資料

## 外部連結

  - 第八屆香港電影金像獎得獎名單（[新版](http://www.hkfaa.com/winnerlist08.html)·[舊版](http://www.hkfaa.com/history/list_08.html)）

[Category:香港電影金像獎](../Category/香港電影金像獎.md "wikilink")
[Category:1988年电影](../Category/1988年电影.md "wikilink")
[港](../Category/1989年電影獎項.md "wikilink")