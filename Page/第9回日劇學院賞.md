[←第8回](../Page/第8回日劇學院賞.md "wikilink") - **第9回** -
[第10回→](../Page/第10回日劇學院賞.md "wikilink")

**第9回[日劇學院賞](../Page/日劇學院賞.md "wikilink")**是針對1996年4到6月在[日本播出的春季檔連續劇做出投票與評比](../Page/日本.md "wikilink")，這一季一共有19部連續劇被提名。

## 得獎名單

### 最優秀作品賞

1.  **[長假](../Page/長假_\(電視劇\).md "wikilink")**
2.  [白色之戀2](../Page/白色之戀2.md "wikilink")
3.  [醜小鴨](../Page/醜小鴨_\(日劇\).md "wikilink")
4.  [徬徨少年時](../Page/徬徨少年時.md "wikilink")
5.  [深深的沉睡\~誰在我心中\~](../Page/深深的沉睡~誰在我心中~.md "wikilink")

### 主演男優賞

1.  **[木村拓哉](../Page/木村拓哉.md "wikilink")**（長假）
2.  [本木雅弘](../Page/本木雅弘.md "wikilink")（[與你相遇](../Page/與你相遇.md "wikilink")）
3.  [岸谷五朗](../Page/岸谷五朗.md "wikilink")（醜小鴨）

### 主演女優賞

1.  **[山口智子](../Page/山口智子.md "wikilink")**（長假）
2.  [大竹忍](../Page/大竹忍.md "wikilink")（深深的沉睡\~誰在我心中\~）
3.  [菅野美穗](../Page/菅野美穗.md "wikilink")（[變身](../Page/變身.md "wikilink")）

### 助演男優賞

1.  **[竹野內豐](../Page/竹野內豐.md "wikilink")**（長假）
2.  [陣內孝則](../Page/陣內孝則.md "wikilink")（[勝利女神](../Page/勝利女神.md "wikilink")）
3.  [根津甚八](../Page/根津甚八_\(演員\).md "wikilink")（徬徨少年時）

### 助演女優賞

1.  **[稻森泉](../Page/稻森泉.md "wikilink")**（長假）
2.  [瀨戶朝香](../Page/瀨戶朝香.md "wikilink")（等你說愛我）
3.  [松隆子](../Page/松隆子.md "wikilink")（長假）

### 主題歌賞

1.  **[射亂Q](../Page/射亂Q.md "wikilink")：いいわけ**（等你說愛我）
2.  [久保田利伸](../Page/久保田利伸.md "wikilink")&[娜歐蜜·坎貝爾](../Page/娜歐蜜·坎貝爾.md "wikilink")：LA·LA·LA
    LOVE SONG（長假）
3.  [松山千春](../Page/松山千春.md "wikilink")：忘不了你（醜小鴨）

### 其他

  - **新人俳優賞：松隆子**（長假）
  - **最佳服裝賞：山口智子與她的造型設計師犬走比佐乃女士**（長假）
  - **腳本賞：[北川悅吏子](../Page/北川悅吏子.md "wikilink")**
  - **監督賞：光野道夫、石阪理江子**（等你說愛我）
  - **攝影賞：[透明人間](../Page/透明人間_\(電視劇\).md "wikilink")**
  - **卡司賞：長假**
  - **片頭賞：長假**
  - **電視週刊特別賞：[烈火消防隊的美術人員](../Page/烈火消防隊.md "wikilink")**

## 讀者投票結果

### 作品賞

#### 男性票

1.  長假
2.  徬徨少年時
3.  勝利女神
4.  醜小鴨
5.  [委託龍馬！](../Page/委託龍馬！.md "wikilink")
6.  等你說愛我
7.  透明人間
8.  與你相逢
9.  變身
10. [心思增加](../Page/心思增加.md "wikilink")

#### 女性票

1.  長假
2.  醜小鴨
3.  等你說愛我
4.  委託龍馬！
5.  徬徨少年時
6.  心思增加
7.  變身
8.  勝利女神
9.  烈火消防隊
10. 與你相逢

### 主演男優賞

1.  木村拓哉（長假）
2.  岸谷五朗（醜小鴨）
3.  [堂本剛](../Page/堂本剛.md "wikilink")（徬徨少年時）
4.  [中居正廣](../Page/中居正廣.md "wikilink")（勝利女神）
5.  本木雅弘（與你相逢）
6.  [濱田雅功](../Page/濱田雅功.md "wikilink")（委託龍馬！）
7.  [堂本光一](../Page/堂本光一.md "wikilink")（徬徨少年時）
8.  [香取慎吾](../Page/香取慎吾.md "wikilink")（透明人間）
9.  [中井貴一](../Page/中井貴一.md "wikilink")（等你說愛我）
10. [明石家秋刀魚](../Page/明石家秋刀魚.md "wikilink")（增加心思）

### 主演女優賞

1.  山口智子（長假）
2.  菅野美穗（變身）
3.  [鶴田真由](../Page/鶴田真由.md "wikilink")（與你相逢）
4.  [田中美佐子](../Page/田中美佐子.md "wikilink")（等你說愛我）
5.  大竹忍（深深的沉睡\~誰在我心中\~）
6.  [石田百合子](../Page/石田百合子.md "wikilink")（[結婚喲](../Page/結婚喲.md "wikilink")）
7.  [大原麗子](../Page/大原麗子.md "wikilink")（[牙醫麗子](../Page/牙醫麗子.md "wikilink")）

### 助演男優賞

1.  竹野內豐（長假）
2.  陣內孝則（勝利女神）
3.  [段田安則](../Page/段田安則.md "wikilink")（與你相逢）
4.  [反町隆史](../Page/反町隆史.md "wikilink")（委託龍馬！）
5.  [西村雅彥](../Page/西村雅彥.md "wikilink")（委託龍馬！）
6.  根津甚八（徬徨少年時）
7.  [草剪剛](../Page/草剪剛.md "wikilink")（結婚喲）
8.  [河相我聞](../Page/河相我聞.md "wikilink")（醜小鴨）
9.  [椎名桔平](../Page/椎名桔平.md "wikilink")（等你說愛我）
10. [岡田義德](../Page/岡田義德.md "wikilink")（變身）

### 助演女優賞

1.  稻森泉（長假）
2.  [常盤貴子](../Page/常盤貴子.md "wikilink")（醜小鴨）
3.  瀨戶朝香（等你說愛我）
4.  [奧菜惠](../Page/奧菜惠.md "wikilink")（徬徨少年時）
5.  松隆子（長假）
6.  [深津繪里](../Page/深津繪里.md "wikilink")（透明人間）
7.  [松雪泰子](../Page/松雪泰子.md "wikilink")（勝利女神）
8.  [緒川珠樹](../Page/緒川珠樹.md "wikilink")（委託龍馬！）
9.  [中谷美紀](../Page/中谷美紀.md "wikilink")（[青春白皮書](../Page/青春白皮書.md "wikilink")）
10. [手塚理美](../Page/手塚理美.md "wikilink")（增加心思）

### 主題歌賞

1.  久保田利伸&娜歐蜜·坎貝爾：[LA·LA·LA LOVE
    SONG](../Page/LA·LA·LA_LOVE_SONG.md "wikilink")（長假）
2.  射亂Q：いいわけ（等你說愛我）
3.  [近畿小子](../Page/近畿小子.md "wikilink")：FRIENDS（徬徨少年時）
4.  松山千春：忘不了你（醜小鴨）
5.  [UFLUFS](../Page/UFLUFS.md "wikilink")：萬歲～最喜歡你～（勝利女神）
6.  H Jungle with t：FRIENDSHIP（委託龍馬！）
7.  [比吉斯](../Page/比吉斯.md "wikilink")：五月夏日（徬徨少年時）
8.  [南方之星](../Page/南方之星.md "wikilink")：[愛的言靈 ～Spiritual
    Message](../Page/愛的言靈_～Spiritual_Message.md "wikilink")（透明人間）
9.  [大浦龍宇一](../Page/大浦龍宇一.md "wikilink")：夏日午後（與你相逢）
10. [老鷹合唱團](../Page/老鷹合唱團.md "wikilink")：加州旅館（增加心思）

[0](../Category/日劇學院賞.md "wikilink")
[Category:1996年日本](../Category/1996年日本.md "wikilink")
[Category:1996年电视奖项](../Category/1996年电视奖项.md "wikilink")