**第一代羅素伯爵約翰·羅素**，[KG](../Page/嘉德勳章.md "wikilink")，[GCMG](../Page/GCMG.md "wikilink")，[PC](../Page/英國樞密院.md "wikilink")（**John
Russell, 1st Earl
Russell**，），活躍於19世紀中期的[英國](../Page/英國.md "wikilink")[輝格黨及](../Page/輝格黨.md "wikilink")[自由黨政治家](../Page/英國自由黨.md "wikilink")，曾任[英國首相](../Page/英國首相.md "wikilink")，於1861年以前以**約翰·羅素勳爵**（**Lord
John
Russell**）為其通稱。他的孫子[伯特蘭·羅素是著名的哲學家](../Page/伯特蘭·羅素.md "wikilink")、1950年的[諾貝爾文學獎得主](../Page/諾貝爾文學獎.md "wikilink")。\[1\]\[2\]

## 生平

1792年，出生于威斯敏斯特的赫特福德街

1813年，进入国会。

1835年，担任内政国务秘书。

1846年，第一次出任首相。

1847年，在工厂推行每日十小时的工时

1848年，在工厂推行建立公共健康委员会。

1865年，第二次出任首相。

1878年，去世。

## 亲属

孙子[伯特兰·罗素是著名的哲学家](../Page/伯特兰·罗素.md "wikilink")、于1950年获得诺贝尔文学奖。

## 参考文献

[Category:英国首相](../Category/英国首相.md "wikilink")
[Category:英國外相](../Category/英國外相.md "wikilink")
[Category:聯合王國伯爵](../Category/聯合王國伯爵.md "wikilink")
[Category:嘉德騎士](../Category/嘉德騎士.md "wikilink")
[Category:愛丁堡大學校友](../Category/愛丁堡大學校友.md "wikilink")
[Category:皇家统计学会会长](../Category/皇家统计学会会长.md "wikilink")
[Category:阿伯丁大学教师](../Category/阿伯丁大学教师.md "wikilink")

1.  \[John Russell, 1st Earl Russell | prime minister of United Kingdom
    <https://www.britannica.com/biography/John-Russell-1st-Earl-Russell>\]
2.  \[History of Lord John Russell, 1st Earl Russell - GOV.UK
    <https://www.gov.uk/government/history/past-prime-ministers/lord-john-russell-1st-earl-russell>\]