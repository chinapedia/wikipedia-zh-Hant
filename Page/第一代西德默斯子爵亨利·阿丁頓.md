**第一代西德默斯子爵亨利·阿丁頓**，[PC](../Page/英國樞密院.md "wikilink")（**Henry Addington,
1st Viscount
Sidmouth**，），[英國](../Page/英國.md "wikilink")[托利黨](../Page/托利黨.md "wikilink")[政治家](../Page/政治家.md "wikilink")，1801年至1804年出任[英國首相](../Page/英國首相.md "wikilink")。\[1\]\[2\]

## 生平

1757年，出生于伦敦的医生家庭。

1789年，担任下议院议长。

1801年，出任首相兼财政大臣。

1802年，与拿破仑法国缔结《亚眠和约》。

1804年，出任枢密院院长。

1812年，出任内政大臣。

1844年，去世。

## 参考文献

[Category:英国首相](../Category/英国首相.md "wikilink")
[Category:英國財政大臣](../Category/英國財政大臣.md "wikilink")
[Category:牛津大学布雷齐诺斯学院校友](../Category/牛津大学布雷齐诺斯学院校友.md "wikilink")
[Category:聯合王國子爵](../Category/聯合王國子爵.md "wikilink")
[Category:英國下議院議長](../Category/英國下議院議長.md "wikilink")
[Category:温切斯特公学校友](../Category/温切斯特公学校友.md "wikilink")

1.  [History of Henry Addington 1st Viscount Sidmouth -
    GOV.UK](https://www.gov.uk/government/history/past-prime-ministers/henry-addington-1st-viscount-sidmouth)
2.  [Henry Addington Viscount Sidmouth |
    Encyclopedia.com](https://www.encyclopedia.com/people/history/british-and-irish-history-biographies/henry-addington-viscount-sidmouth)