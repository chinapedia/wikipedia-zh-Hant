**第一次伊松佐河之役**（）發生於1915年6月23日至7月7日，在[意大利](../Page/意大利.md "wikilink")[伊松佐河附近爆發](../Page/伊松佐河.md "wikilink"),
當時二十萬名意軍先發動進攻, 試圖驅逐駐守在伊松佐河和附近山區的十萬名[奥匈帝国军队](../Page/奥匈帝国.md "wikilink"),
但意軍的炮火未能充份掩護意軍, 令在高地的奧軍成功守住防線。

## 過程

1915年5月23日，[意大利王国向](../Page/意大利王國_\(1861年–1946年\).md "wikilink")[奧匈帝國宣戰](../Page/奧匈帝國.md "wikilink")。[意軍統帥部計劃在北部交通不便的](../Page/意軍.md "wikilink")[阿爾卑斯山區實施防禦](../Page/阿爾卑斯山.md "wikilink")，而在距[塞爾維亞較近且利於進攻的東部邊境向](../Page/塞爾維亞.md "wikilink")[伊松佐河地區發動大規模進攻](../Page/伊松佐河.md "wikilink")。[奧軍因主力部隊陷於](../Page/奧軍.md "wikilink")[俄國戰場](../Page/俄國戰場.md "wikilink")，決定在[義大利方向實施防禦](../Page/義大利.md "wikilink")，重點防禦[伊松佐河地區](../Page/伊松佐河.md "wikilink")。戰前，奧軍控制伊松佐河所有渡口和東岸的[巴因西扎高地及](../Page/巴因西扎高地.md "wikilink")[卡爾索高地](../Page/卡爾索高地.md "wikilink")，並在[托爾明諾和](../Page/托爾明諾.md "wikilink")[戈里齊亞築有堅固的橋頭陣地](../Page/戈里齊亞.md "wikilink")。

5月23日～6月22日，意軍經邊境作戰擊退伊松佐河西岸的奧軍；6月23日發動戰役，企圖奪占的[里雅斯特](../Page/里雅斯特.md "wikilink")。總參謀長[路易吉·卡多爾納指揮意第](../Page/路易吉·卡多爾納.md "wikilink")2、第3集團軍（19個師近20萬人），分三路進攻伊松佐河東岸奧軍陣地：左路攻擊[托爾明諾和](../Page/托爾明諾.md "wikilink")[蒙特內羅山](../Page/蒙特內羅山.md "wikilink")，切斷[戈里齊亞至](../Page/戈里齊亞.md "wikilink")[維也納的鐵路線](../Page/維也納.md "wikilink")，阻擊奧軍援兵；中路主攻戈里齊亞橋頭陣地；右路進攻卡爾索高地，切斷海岸鐵路線。同時，意海軍封鎖的里雅斯特的海上交通。奧軍[斯韋托扎爾·博羅埃維奇指揮](../Page/斯韋托扎爾·博羅埃維奇.md "wikilink")13個師約10萬人，堅守東岸的[要塞和](../Page/要塞.md "wikilink")[高地](../Page/高地.md "wikilink")。意軍強渡伊松佐河後，向奧軍陣地發起全面突擊。奧軍憑險據守，以猛烈的火力和頑強的反衝擊擊退意軍多次強攻。7月7日，意軍進攻受挫。

[Category:1915年一战战役](../Category/1915年一战战役.md "wikilink")
[Category:奥匈帝国战役](../Category/奥匈帝国战役.md "wikilink")
[Category:意大利王国战役](../Category/意大利王国战役.md "wikilink")
[Category:伊松佐河戰役](../Category/伊松佐河戰役.md "wikilink")
[Category:1915年6月](../Category/1915年6月.md "wikilink")
[Category:1915年7月](../Category/1915年7月.md "wikilink")