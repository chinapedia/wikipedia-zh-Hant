[MiG-25_fig2agrau_USAF.jpg](https://zh.wikipedia.org/wiki/File:MiG-25_fig2agrau_USAF.jpg "fig:MiG-25_fig2agrau_USAF.jpg")为典型的第三代战机\]\]
[F-4g-69-0212-81st-tactical-fighter-squadron.jpg](https://zh.wikipedia.org/wiki/File:F-4g-69-0212-81st-tactical-fighter-squadron.jpg "fig:F-4g-69-0212-81st-tactical-fighter-squadron.jpg")鬼怪II型戰鬥機\]\]
**第三代戰機**是1972年後主要出現的[戰鬥機](../Page/戰鬥機.md "wikilink")，其特色為中低空機動靈活性高、配備先進[雷達設備](../Page/雷達.md "wikilink")、加強[導彈應用](../Page/導彈.md "wikilink")（例如進行視距外作戰(BVR)）等。

## 特徵

第三代戰鬥機出現於1960年代，這個階段將先前累積的使用經驗已經各種試驗的結果加以整合。許多高速飛行時的現象和控制問題獲得相當程度的解決，高後掠角度的機翼設計已經不受到青睞，三角翼和幾何可變機翼與後掠角度小於45度的[梯形翼成為設計的主流](../Page/梯形翼.md "wikilink")。發動機的輸出透過耐高溫特殊材料和冷卻技術而更上一層樓。[雷達與各類](../Page/雷達.md "wikilink")[航電逐漸成熟與複雜化](../Page/航電.md "wikilink")，機鼻進氣口幾乎完全被放棄，以配合大型雷達天線的安裝需求，而這個需求使得飛機的大小和成本迅速高漲。

第三代戰鬥機將[空對空飛彈作為標準武裝之一](../Page/空對空飛彈.md "wikilink")，並且在[越戰](../Page/越戰.md "wikilink")、[六日戰爭與](../Page/六日戰爭.md "wikilink")[印巴邊界衝突當中使用](../Page/印巴邊界衝突.md "wikilink")，這些使用經驗對於[飛彈系統本身多是負面的評價](../Page/飛彈.md "wikilink")，不過飛彈的重要性以及發展的方向逐漸在各國受到重視，打下日後的基礎。

前兩代的發展當中，單一用途的[攔截機與](../Page/攔截機.md "wikilink")[戰鬥轟炸機進展至此](../Page/戰鬥轟炸機.md "wikilink")，受惠於各項系統的進步，尤其是雷達與航電的功能以及效能，使得第三代的戰鬥機開始趨向多任務，多用途的路線。此代的戰機以[F-4](../Page/F-4.md "wikilink")、[F-5](../Page/F-5.md "wikilink")、[MiG-25](../Page/MiG-25.md "wikilink")
為主要代表。

## 戰鬥機種

  -   - [幻影F1型战斗机](../Page/幻象F1型戰鬥機.md "wikilink")
      - [超級軍旗攻擊機](../Page/超級軍旗攻擊機.md "wikilink") <small>(海軍)</small>

<!-- end list -->

  -   - [HESA Azarakhsh](../Page/HESA_Azarakhsh.md "wikilink")

<!-- end list -->

  -   - [幼獅戰鬥機](../Page/幼獅戰鬥機.md "wikilink")

<!-- end list -->

  - /

      - [F-104星式戰鬥機](../Page/F-104星式戰鬥機.md "wikilink")

<!-- end list -->

  -   - [三菱F-1戰鬥機](../Page/三菱F-1戰鬥機.md "wikilink")

<!-- end list -->

  -   - [殲-8](../Page/殲-8.md "wikilink")

  -   - [Atlas Cheetah](../Page/Atlas_Cheetah.md "wikilink")

<!-- end list -->

  -   - [米格-25戰鬥機](../Page/米格-25戰鬥機.md "wikilink")
      - [Su-15攔截機](../Page/Su-15攔截機.md "wikilink")
      - [Su-17攻擊機](../Page/Su-17攻擊機.md "wikilink")
      - [Tupolev Tu-28](../Page/Tupolev_Tu-28.md "wikilink")

<!-- end list -->

  -   - [Saab 37 Viggen](../Page/Saab_37_Viggen.md "wikilink")

<!-- end list -->

  -   - [海鷂戰鬥攻擊機](../Page/海鷂戰鬥攻擊機.md "wikilink") <small>(海軍)</small>

<!-- end list -->

  - /

      - [AV-8獵鷹II式攻擊機](../Page/AV-8獵鷹II式攻擊機.md "wikilink")
        <small>(海軍)</small>

<!-- end list -->

  -   - [F-4鬼怪II戰鬥機](../Page/F-4鬼怪II戰鬥機.md "wikilink")
      - [F-5自由鬥士戰鬥機](../Page/F-5自由鬥士戰鬥機.md "wikilink")

### 已被取消的機型

  -   - [Dassault Mirage F2](../Page/Dassault_Mirage_F2.md "wikilink")
        <small>(1966年6月12日首飛)</small>
      - [Dassault Mirage G](../Page/Dassault_Mirage_G.md "wikilink")
        <small>(1967年11月18日首飛)</small>
      - [Dassault Mirage
        IIIV](../Page/Dassault_Mirage_IIIV.md "wikilink")
        <small>(1967年11月18日首飛)</small>

<!-- end list -->

  -   - [殲-12](../Page/殲-12.md "wikilink")
        <small>(1970年12月26日首飛)</small>

<!-- end list -->

  -   - [Project Sabre
        II](../Page/Project_Sabre_II.md "wikilink")<small>(Project
        Abandoned)</small>

<!-- end list -->

  -   - [Mikoyan-Gurevich
        Ye-8](../Page/Mikoyan-Gurevich_Ye-8.md "wikilink")
        <small>(1962年9月11日首飛)</small>
      - [Mikoyan-Gurevich
        Ye-150/Ye-151/Ye-152](../Page/Mikoyan-Gurevich_Ye-150_family.md "wikilink")
        <small>(1959年7月10日首飛)</small>

<!-- end list -->

  -   - [Hawker P.1121](../Page/Hawker_P.1121.md "wikilink")
        <small>(未曾建造)</small>
      - [Hawker Siddeley
        P.1154](../Page/Hawker_Siddeley_P.1154.md "wikilink")
        <small>(未曾建造)</small>

<!-- end list -->

  -   - [Bell D-188A](../Page/Bell_D-188A.md "wikilink")
        <small>(未曾建造)</small>
      - [Douglas F5D
        Skylancer](../Page/Douglas_F5D_Skylancer.md "wikilink")
        <small>(1956年4月21日首飛)</small>
      - [Douglas F6D
        Missileer](../Page/Douglas_F6D_Missileer.md "wikilink")
        <small>(未曾建造)</small>
      - [General Dynamics/Grumman
        F-111B](../Page/General_Dynamics/Grumman_F-111B.md "wikilink")
        <small>(1965年5月18日首飛)</small>
      - [Grumman XF12F](../Page/Grumman_G-118.md "wikilink")
        <small>(未曾建造)</small>
      - [X-27試驗機](../Page/X-27試驗機.md "wikilink") <small>(未曾建造)</small>
      - [YF-12戰鬥機](../Page/YF-12戰鬥機.md "wikilink")
        <small>(1963年8月7日首飛)</small>
      - [XF-108輕劍戰鬥機](../Page/XF-108輕劍戰鬥機.md "wikilink")
        <small>(未曾建造)</small>
      - [XFV-12戰鬥機](../Page/XFV-12戰鬥機.md "wikilink")
        <small>(不能飛行)</small>
      - [Vought XF8U-3 Crusader
        III](../Page/Vought_XF8U-3_Crusader_III.md "wikilink")
        <small>(1958年6月2日首飛)</small>

  -   - [EWR VJ 101](../Page/EWR_VJ_101.md "wikilink")
        <small>(1963年4月10日首飛)</small>
      - [VFW VAK 191B](../Page/VFW_VAK_191B.md "wikilink")
        <small>(1971年9月20日首飛)</small>

[Category:战斗机](../Category/战斗机.md "wikilink")