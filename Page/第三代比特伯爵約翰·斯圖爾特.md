**约翰·斯图尔特，第三代比特伯爵**，[KG](../Page/嘉德勋章.md "wikilink")，[PC](../Page/英國樞密院.md "wikilink")（，），[苏格兰贵族](../Page/苏格兰.md "wikilink")，[乔治三世时的](../Page/乔治三世_\(英国\).md "wikilink")[英国首相](../Page/英国首相.md "wikilink")(1762–1763)。

## 生平

1713年，出生在爱丁堡的议会广场。

1737年，在伊顿公学受教育。

1723年，继承了伯爵爵位。

1737年，进入上议院。

1754年，迁居伦敦。

1762年，出任首相。

1763年，辞职。

1792年，去世。

## 子女

1.  玛丽·克里奇顿-斯图尔特 (b. c. 1741年) 1761年9月7日嫁[詹姆斯·劳赛尔
    (伦斯达尔伯爵)](../Page/詹姆斯·劳赛尔_\(伦斯达尔伯爵\).md "wikilink")
2.  [約翰·克里奇頓-斯圖爾特，第一代比特侯爵](../Page/約翰·克里奇頓-斯圖爾特，第一代比特侯爵.md "wikilink")
    (1744年6月30日–1814年11月16日)
3.  安娜·克里奇顿-斯图尔特 (b. c. 1745年) 1764年7月2日嫁[休·佩西
    (诺森伯兰公爵)](../Page/休·佩西_\(诺森伯兰公爵\).md "wikilink").
4.  詹姆斯·阿奇博尔德·斯图尔特-沃特利-麦肯齐 (1747年9月19日–1818年3月1日)
5.  简·克里奇顿-斯图尔特 (b. c. 1748年–1828年2月28日)
    1768年2月1日嫁[乔治·马戛尔尼](../Page/乔治·马戛尔尼.md "wikilink")。
6.  查理·克里奇顿-斯图尔特爵士 (1753年1月–1801年5月25日)
7.  Most Rev. [威廉·克里奇顿-斯图尔特](../Page/威廉·克里奇顿-斯图尔特.md "wikilink"),
    [Archbishop of Armagh (Church of
    Ireland)](../Page/Archbishops_of_Armagh_-_Primate_of_All_Ireland_\(Church_of_Ireland\).md "wikilink")
    (1755年3月–1822年3月6日)
8.  卡萝琳·斯图尔特夫人 (before 1763年–1813年1月) 1778年1月1日嫁[约翰·道森
    (波塔林顿伯爵)](../Page/约翰·道森_\(波塔林顿伯爵\).md "wikilink").

## 参见

  - [七年战争](../Page/七年战争.md "wikilink")

## 参考文献

  - ; cited as *ButeODNB*.

## 外部链接

  - [More about John Stuart, Earl of
    Bute](https://web.archive.org/web/20080609014928/http://www.pm.gov.uk/output/Page169.asp)
    on the Downing Street website.
  - [The Age of George III: The ministry of John Stuart, third Earl of
    Bute](http://www.historyhome.co.uk/c-eight/ministry/butemin.htm)
  - [1911 Encyclopædia Britannica/Bute, John Stuart, 3rd Earl
    of](../Page/s:1911_Encyclopædia_Britannica/Bute,_John_Stuart,_3rd_Earl_of.md "wikilink")

[Category:英国首相](../Category/英国首相.md "wikilink")
[Category:蘇格蘭伯爵](../Category/蘇格蘭伯爵.md "wikilink")
[Category:嘉德騎士](../Category/嘉德騎士.md "wikilink")