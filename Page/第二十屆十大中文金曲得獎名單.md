**第二十屆十大中文金曲得獎名單**

  - 十大中文金曲
      - 《我們的主題曲》*（主唱：[鄭秀文](../Page/鄭秀文.md "wikilink")
        作曲：[吳國敬](../Page/吳國敬.md "wikilink")
        填詞：[黃偉文](../Page/黃偉文.md "wikilink")）*
      - 《愛的呼喚》*（主唱：[郭富城](../Page/郭富城.md "wikilink")
        作曲：[譚國政](../Page/譚國政.md "wikilink")
        填詞：[小美](../Page/小美.md "wikilink")）*
      - 《[不老的傳說](../Page/不老的傳說.md "wikilink")》*（主唱：[張學友](../Page/張學友.md "wikilink")
        作曲：Dick Lee 填詞：[古倩敏](../Page/古倩敏.md "wikilink")）*
      - 《明知故犯》*（主唱：[許美靜](../Page/許美靜.md "wikilink")
        作曲：[陳佳明](../Page/陳佳明.md "wikilink")
        填詞：[林夕](../Page/林夕.md "wikilink")）*
      - 《星夢情真》*（主唱：[陳慧琳](../Page/陳慧琳.md "wikilink")
        作曲：[雷頌德](../Page/雷頌德.md "wikilink")
        填詞：[黃偉文](../Page/黃偉文.md "wikilink")）*
      - 《只要為我愛一天》*（主唱：[黎明](../Page/黎明.md "wikilink")
        作曲：[雷頌德](../Page/雷頌德.md "wikilink")
        填詞：[林夕](../Page/林夕.md "wikilink")）*
      - 《歡樂今宵》*（主唱：[古巨基](../Page/古巨基.md "wikilink")
        作曲：[黃丹儀](../Page/黃丹儀.md "wikilink")
        填詞：[黃偉文](../Page/黃偉文.md "wikilink")）*
      - 《中國人》*（主唱：[劉德華](../Page/劉德華.md "wikilink")
        作曲：[陳耀川](../Page/陳耀川.md "wikilink")
        填詞：[李安修](../Page/李安修.md "wikilink")）*
      - 《你快樂所以我快樂》*（主唱：[王菲](../Page/王菲.md "wikilink")
        作曲：[張亞東](../Page/張亞東.md "wikilink")
        填詞：[林夕](../Page/林夕.md "wikilink")）*
      - 《[愛是永恆](../Page/愛是永恆.md "wikilink")》*（主唱：[張學友](../Page/張學友.md "wikilink")
        作曲：Dick Lee 填詞：[林振強](../Page/林振強.md "wikilink")）*
  - 最佳中文（流行）歌曲獎
      - 《原來只要共你活一天》*（[陳少琪](../Page/陳少琪.md "wikilink")）*
  - 最佳中文（流行）歌詞獎
      - 《約定》*（[林夕](../Page/林夕.md "wikilink")）*
  - 十大優秀流行歌手大獎
      - *[王菲](../Page/王菲.md "wikilink")　[鄭秀文](../Page/鄭秀文.md "wikilink")　[彭羚](../Page/彭羚.md "wikilink")　[許志安](../Page/許志安.md "wikilink")　[劉德華](../Page/劉德華.md "wikilink")　[張學友](../Page/張學友.md "wikilink")　[鄭伊健](../Page/鄭伊健.md "wikilink")　[黎明](../Page/黎明.md "wikilink")　[郭富城](../Page/郭富城.md "wikilink")　[古巨基](../Page/古巨基.md "wikilink")*
  - 最有前途新人獎
      - 金獎：*[謝霆鋒](../Page/謝霆鋒.md "wikilink")*
      - 銀獎：*[張惠妹](../Page/張惠妹.md "wikilink")*
      - 銅獎：*[林海峰](../Page/林海峰_\(香港\).md "wikilink")*
      - 優異獎：*[Dry](../Page/Dry.md "wikilink") /
        [譚小環](../Page/譚小環.md "wikilink") /
        [陳小春](../Page/陳小春.md "wikilink")*
  - 最佳原創歌曲獎
      - 《我有我天地》*（[彭羚](../Page/彭羚.md "wikilink")）*
      - 《與我常在》*（[陳奕迅](../Page/陳奕迅.md "wikilink")）*
  - 最佳改編歌曲獎
      - 《明知故犯》*（[許美靜](../Page/許美靜.md "wikilink")）*
  - 優秀國語歌曲獎
      - 金獎：《中國人》*（[劉德華](../Page/劉德華.md "wikilink")）*
      - 銀獎：《想和你去吹吹風》*（[張學友](../Page/張學友.md "wikilink")）*
      - 銅獎：《心太軟》*（[任賢齊](../Page/任賢齊.md "wikilink")）*
      - 銅獎：《相思無用》*（[鄭中基](../Page/鄭中基.md "wikilink")）*
  - 全年最高銷量歌手大獎
      - *[鄭秀文](../Page/鄭秀文.md "wikilink")　[張學友](../Page/張學友.md "wikilink")　[郭富城](../Page/郭富城.md "wikilink")　[黎明](../Page/黎明.md "wikilink")　[鄭伊健](../Page/鄭伊健.md "wikilink")*
  - 全年最高銷量冠軍歌手大獎
      - *[張學友](../Page/張學友.md "wikilink")*
  - 全年銷量冠軍大獎
      - 《[不老的傳說](../Page/不老的傳說.md "wikilink")》*（[張學友](../Page/張學友.md "wikilink")）*
  - 飛躍大獎
      - 男歌手
          - 金獎：*[郭富城](../Page/郭富城.md "wikilink")*
          - 銀獎：*[古巨基](../Page/古巨基.md "wikilink")*
          - 銅獎：*[陳曉東](../Page/陳曉東.md "wikilink")*
      - 女歌手
          - 金獎：*[鄭秀文](../Page/鄭秀文.md "wikilink")*
          - 銀獎：*[王菲](../Page/王菲.md "wikilink")*
          - 銅獎：*[陳慧琳](../Page/陳慧琳.md "wikilink")*
  - 全球華人至尊金曲
      - 《只要為我愛一天》*（[黎明](../Page/黎明.md "wikilink")）*
  - 金曲廿載榮譽大獎
      - 《帝女花》

## 得奖名单

[第20](../Category/十大中文金曲.md "wikilink")