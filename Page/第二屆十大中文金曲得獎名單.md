第二屆十大中文金曲得獎名單

  - 十首中文金曲獲獎名單排名不分前後。

| **排名** | **歌曲**                               | **作曲**                             | **填詞**                                                             | **主唱**                           |
| ------ | ------------------------------------ | ---------------------------------- | ------------------------------------------------------------------ | -------------------------------- |
| **1**  | 《天蠶變》                                | [黎小田](../Page/黎小田.md "wikilink")   | [盧國沾](../Page/盧國沾.md "wikilink")                                   | [關正傑](../Page/關正傑.md "wikilink") |
| **2**  | 《加價熱潮》                               | De Knight-Freedman                 | [許冠傑](../Page/許冠傑.md "wikilink")/ [黎彼得](../Page/黎彼得.md "wikilink") | [許冠傑](../Page/許冠傑.md "wikilink") |
| **3**  | 《好歌獻給你》                              | [馬飼野康](../Page/馬飼野康.md "wikilink") | [鄭國江](../Page/鄭國江.md "wikilink")                                   | [羅文](../Page/羅文.md "wikilink")   |
| **4**  | 《[春雨彎刀](../Page/春雨彎刀.md "wikilink")》 | [顧嘉煇](../Page/顧嘉煇.md "wikilink")   | [鄧偉雄](../Page/鄧偉雄.md "wikilink")                                   | [甄妮](../Page/甄妮.md "wikilink")   |
| **5**  | 《陌上歸人》                               | [馮添枝](../Page/馮添枝.md "wikilink")   | [鄭國江](../Page/鄭國江.md "wikilink")                                   | [區瑞強](../Page/區瑞強.md "wikilink") |
| **6**  | 《茫茫路》                                | [顧嘉煇](../Page/顧嘉煇.md "wikilink")   | [盧國沾](../Page/盧國沾.md "wikilink")                                   | [張德蘭](../Page/張德蘭.md "wikilink") |
| **7**  | 《眼淚為你流》                              | [陳百強](../Page/陳百強.md "wikilink")   | [鄭國江](../Page/鄭國江.md "wikilink")                                   | [陳百強](../Page/陳百強.md "wikilink") |
| **8**  | 《楚留香》                                | [顧嘉煇](../Page/顧嘉煇.md "wikilink")   | [鄧偉雄](../Page/鄧偉雄.md "wikilink") / [黃霑](../Page/黃霑.md "wikilink")  | [鄭少秋](../Page/鄭少秋.md "wikilink") |
| **9**  | 《像白雲像清風》                             | [顧嘉煇](../Page/顧嘉煇.md "wikilink")   | [盧國沾](../Page/盧國沾.md "wikilink")                                   | [汪明荃](../Page/汪明荃.md "wikilink") |
| **10** | 《橄欖樹》                                | [李泰祥](../Page/李泰祥.md "wikilink")   | [三毛](../Page/三毛_\(作家\).md "wikilink")                              | [齊豫](../Page/齊豫.md "wikilink")   |

align=center style="background:\#BFD7FF"|**十大中文金曲**:***十首優秀中文金曲名單***

[第02](../Category/十大中文金曲.md "wikilink")