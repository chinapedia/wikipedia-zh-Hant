**第五套人民币**由[中国人民银行在](../Page/中国人民银行.md "wikilink")[中华人民共和国建国](../Page/中华人民共和国.md "wikilink")50周年之际，于1999年10月1日起，分批发行。该套[人民币共八种面额](../Page/人民币.md "wikilink")：100元、50元、20元、10元、5元、1元（纸币、硬币）、5角（硬币）、1角（硬币）。相比前几套人民币，第五套人民币取消了二角及二元的票面，增加了二十元票面，硬币取消了前几套人民币硬币正面的[国徽图案](../Page/中华人民共和国国徽.md "wikilink")，面额则从背面改到正面。该套人民币与[第四套人民币相比](../Page/第四套人民币.md "wikilink")，加强了防伪措施，增强了机读性能。纸币和硬币中“中国人民银行”六字是由[马文蔚所书写](../Page/马文蔚.md "wikilink")。
票面上一方[印章为](../Page/印章.md "wikilink")“行长之章”。纸币背面印有用[汉语拼音](../Page/汉语拼音.md "wikilink")、[蒙古文](../Page/蒙古文.md "wikilink")、[维吾尔文](../Page/维吾尔文.md "wikilink")、[藏文](../Page/藏文.md "wikilink")、[壮文书写的](../Page/壮文.md "wikilink")“中国人民银行”与其面额的字样。壮文改为1982年的方案。票面正面增加了[盲文](../Page/盲文.md "wikilink")。[第五套人民币的彩稿设计工作则全部是由印钞造币企业的专业设计人员承担完成](../Page/第五套人民币.md "wikilink")。

## 票面特征

第五套人民币，各面额钞票的正面一律采用[毛泽东的头像](../Page/毛泽东.md "wikilink")，头像作者为[刘文西](../Page/刘文西.md "wikilink")，底衬[花卉图案](../Page/花卉.md "wikilink")，分别是[梅花](../Page/梅花.md "wikilink")、[菊花](../Page/菊花.md "wikilink")、[荷花](../Page/荷花.md "wikilink")、[月季花](../Page/月季花.md "wikilink")、[水仙花](../Page/水仙花.md "wikilink")、[兰花的图案](../Page/兰花.md "wikilink")，各面额钞票的钞票的背面主景分别是[人民大会堂](../Page/人民大会堂.md "wikilink")、[布达拉宫](../Page/布达拉宫.md "wikilink")、[桂林山水](../Page/桂林山水.md "wikilink")、[长江三峡](../Page/长江三峡.md "wikilink")、[泰山](../Page/泰山.md "wikilink")、[杭州西湖的图案](../Page/杭州西湖.md "wikilink")。

## 各种面值人民币的发行时间

  - 1999年10月1日：发行了1999年版100元纸币；
  - 2000年10月16日：发行了1999年版20元纸币、1元和1角以铝合金为金属的硬币；
  - 2001年9月1日：发行了1999年版50元、10元纸币；
  - 2002年11月18日：发行了1999年版5元纸币、5角硬币；
  - 2004年7月30日：发行了1999年版1元纸币；
  - 2005年8月31日：发行了2005年版5元、10元、20元、50元、100元纸币和1角以不锈钢为金属的硬币；
  - 2015年11月12日：发行了2015年版100元纸币。

## 改版

[中国人民银行](../Page/中国人民银行.md "wikilink")2005年8月30日召开新闻发布会称，经[国务院批准](../Page/国务院.md "wikilink")，人民银行对第五套人民币（1999年版）的生产工艺、技术进行了提高。改进、提高后的2005年版第五套人民币100元、50元、20元、10元、5元纸币和1角硬币于8月31日发行流通。

此次改版，主要是統一防伪系统，改版後双色横排号码、白水印、全息磁性开窗安全线、票背雕刻凹版印刷成為自5元至100元共通防伪措施；隐形面额数字觀察方式改變，观察更加方便，效果更加明显；红、蓝彩色纤维取消，被凹印手感线纹代替。

2004年发行的1元纸币事實上具有實驗性質，隐形面额数字為新式觀察方式，不具有红、蓝彩色纤维而是凹印手感线纹，因此以防伪措施來看1元纸币屬於2005版而非1999版（但票背仍印「1999年」）

人民银行在2015年8月10日召开新闻发布会称，于本年11月12日發行2015年版人民币100元。

## 防伪特征

[RMB_fangwei.png](https://zh.wikipedia.org/wiki/File:RMB_fangwei.png "fig:RMB_fangwei.png")公布资料绘制\]\]
第五套人民币主要有以下防伪特征（详见右图）：

  - 所有面額皆有

<!-- end list -->

  - 手工雕刻头像
  - 胶印缩微文字
  - 四周花纹对接
  - 雕刻凹版印刷（正面）
  - 凹印手感线纹（1元及2005版5元、10元、20元、50元、100元）
  - 双色横排号码（1元、5元、10元、20元及2005版50元、100元）
  - 固定位置水印（1元、5元、10元及20元為花卉，50元及100元為人像）
  - 隐形面额数字（新式，1元及2005版5元、10元、20元、50元、100元）
  - 有色荧光图案（正面）（特定波长的紫外光下始可見）（黄色荧光的面额数字）
  - [圆圈星座防伪技术](../Page/圆圈星座防伪技术.md "wikilink")（正面面額及固定位置水印之間的一堆圓圈）（1元及2005版5元、10元、20元、50元、100元）
  - 鈔票序號（沒有「V」字）

<!-- end list -->

  - 部分面額採用

<!-- end list -->

  - 白水印（5元、10元及2005版20元、50元、100元）
  - 黃、蓝彩色纤维（5元、10元、20元、50元及100元）（特定波长的紫外光照射下始可見）
  - 光变油墨面额数字（50元及100元）
  - 彩虹过渡印刷（100元）
  - 雕刻凹版印刷（背面）（5元、10元、50元、100元及2005版20元）
  - 全息磁性开窗安全线（5元、10元及2005版20元、50元、100元）
  - 阴阳互补对印图案（10元、50元、100元及2005版20元）
  - 有色荧光图案（背面）（特定波长的紫外光下）（20元為绿色荧光图案、50元显现图案、100元為桔黄色、其他面額待查）

<!-- end list -->

  - 1999版採用，2005版不採用

<!-- end list -->

  - 红、蓝彩色纤维（5元、10元、20元、50元及100元）（2005版不採用）
  - 磁性缩微文字安全线（50元及100元）（2005版不採用）
  - 明暗之间安全线（20元）（2005版不採用）
  - 橫豎異色雙號碼（50元及100元）（2005版不採用）
  - 隐形面额数字（5元、10元、20元、50元、100元）（2005版不採用）

## 紙币

<table>
<tbody>
<tr class="odd">
<td><p><strong>币值</strong></p></td>
<td><p><strong>图案</strong></p></td>
<td><p><strong>说明</strong></p></td>
</tr>
<tr class="even">
<td><p>1元</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bh1.JPG" title="fig:Bh1.JPG">Bh1.JPG</a><a href="https://zh.wikipedia.org/wiki/File:Bh2.JPG" title="fig:Bh2.JPG">Bh2.JPG</a></p></td>
<td><p>规格为：130×63mm。正面为<a href="../Page/毛泽东.md" title="wikilink">毛泽东像</a>，兰花底衬；背面图案为<a href="../Page/杭州西湖.md" title="wikilink">杭州西湖</a>（<a href="../Page/三潭印月.md" title="wikilink">三潭印月</a>）图，橄榄绿色调。该币于2004年7月30日发行。<br />
<strong>另</strong>：虽然1元紙幣的防伪设计屬於2005年版而非1999年版，但票背仍印「1999年」。</p></td>
</tr>
<tr class="odd">
<td><p>5元</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:5-2005-b.jpg" title="fig:5-2005-b.jpg">5-2005-b.jpg</a><a href="https://zh.wikipedia.org/wiki/File:5-2005-a.jpg" title="fig:5-2005-a.jpg">5-2005-a.jpg</a></p></td>
<td><p>规格为：135×63mm。正面为<a href="../Page/毛泽东.md" title="wikilink">毛泽东像</a>，水仙花底衬；背面图案为<a href="../Page/泰山.md" title="wikilink">泰山</a>（五嶽獨尊石）图，紫色调。该币于2002年11月18日发行。<br />
<strong>另</strong>：2005年8月31日发行了2005年版，图案与颜色与1999年版相同，只是改变部分防伪设计。1999年版于2018年4月1日起只收不付。</p></td>
</tr>
<tr class="even">
<td><p>10元</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:10-2005-b.jpg" title="fig:10-2005-b.jpg">10-2005-b.jpg</a><a href="https://zh.wikipedia.org/wiki/File:10-2005-a.jpg" title="fig:10-2005-a.jpg">10-2005-a.jpg</a></p></td>
<td><p>规格为：140×70mm。正面为<a href="../Page/毛泽东.md" title="wikilink">毛泽东像月季花底衬</a>；背面图案为<a href="../Page/长江三峡.md" title="wikilink">长江三峡</a>（瞿塘峽夔門）图，蓝黑色调。该币于2001年9月1日发行。<br />
<strong>另</strong>：2005年8月31日发行了2005年版，图案与颜色与1999年版相同，只是改变部分防伪设计。1999年版于2018年4月1日起只收不付。</p></td>
</tr>
<tr class="odd">
<td><p>20元</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:20-2005-b.jpg" title="fig:20-2005-b.jpg">20-2005-b.jpg</a><a href="https://zh.wikipedia.org/wiki/File:20-2005-a.jpg" title="fig:20-2005-a.jpg">20-2005-a.jpg</a></p></td>
<td><p>规格为：145×70mm。正面为<a href="../Page/毛泽东.md" title="wikilink">毛泽东像</a>，荷花底衬；背面图案为<a href="../Page/桂林山水.md" title="wikilink">桂林山水</a>（陽朔興坪元寶山，灕江）图，棕色调。该币于2000年10月16日发行。<br />
<strong>另</strong>：2005年8月31日发行了2005年版，图案与颜色与1999年版相同，只是改变部分防伪设计。1999年版于2018年4月1日起只收不付。</p></td>
</tr>
<tr class="even">
<td><p>50元</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:50-2005-b.jpg" title="fig:50-2005-b.jpg">50-2005-b.jpg</a><a href="https://zh.wikipedia.org/wiki/File:50-2005-a.jpg" title="fig:50-2005-a.jpg">50-2005-a.jpg</a></p></td>
<td><p>规格为：150×70mm。正面为<a href="../Page/毛泽东.md" title="wikilink">毛泽东像</a>，菊花底衬；背面图案为<a href="../Page/布达拉宫.md" title="wikilink">布达拉宫图</a>，绿色调。该币于2001年9月1日发行。<br />
<strong>另</strong>：2005年8月31日发行了2005年版，图案与颜色与1999年版相同，只是改变部分防伪设计。1999年版于2018年4月1日起只收不付。</p></td>
</tr>
<tr class="odd">
<td><p>100元</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:100-2005-b.jpg" title="fig:100-2005-b.jpg">100-2005-b.jpg</a><a href="https://zh.wikipedia.org/wiki/File:100-2005-a.jpg" title="fig:100-2005-a.jpg">100-2005-a.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:100-2015-b.jpg" title="fig:100-2015-b.jpg">100-2015-b.jpg</a><a href="https://zh.wikipedia.org/wiki/File:100-2015-a.jpg" title="fig:100-2015-a.jpg">100-2015-a.jpg</a></p></td>
<td><p>规格为：155×77mm。正面为<a href="../Page/毛泽东.md" title="wikilink">毛泽东像</a>，梅花底衬；背面图案为<a href="../Page/人民大会堂.md" title="wikilink">人民大会堂图</a>，红色调。该币于1999年10月1日发行。<br />
<strong>另</strong>：2005年8月31日发行了2005年版，图案与颜色与1999年版相同，只是改变部分防伪设计。<br />
<strong>另</strong>：2015年11月12日发行了2015年版，颜色与1999年版相同，图案（<a href="../Page/毛泽东.md" title="wikilink">毛泽东像</a>）中的<a href="../Page/毛泽东.md" title="wikilink">毛泽东的耳朵變小</a>，並改变和加入部分防伪设计，以及恢復了1999年版的豎號碼設計。1999年版于2018年4月1日起只收不付。</p></td>
</tr>
</tbody>
</table>

## 硬币

|        |                                                                                                                                                                                  |                                                                                                                                                                                                                                                                                                               |
| ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **币值** | **图案**                                                                                                                                                                           | **说明**                                                                                                                                                                                                                                                                                                        |
| 1角     | [Cr01f.jpg](https://zh.wikipedia.org/wiki/File:Cr01f.jpg "fig:Cr01f.jpg")[Cr01r.jpg](https://zh.wikipedia.org/wiki/File:Cr01r.jpg "fig:Cr01r.jpg")                               | 直径为：19mm。正面為行名、面额及年号；背面为[兰花圖案及行名](../Page/兰花.md "wikilink")[汉语拼音](../Page/汉语拼音.md "wikilink")。色泽为[铝](../Page/铝.md "wikilink")[白色](../Page/白色.md "wikilink")，材质为[铝合金](../Page/铝合金.md "wikilink")，币外缘为圆柱面。該幣于2000年10月16日發行。                                                                                     |
| 1角     | [1jiao2005a.JPG](https://zh.wikipedia.org/wiki/File:1jiao2005a.JPG "fig:1jiao2005a.JPG")[1jiao2005b.JPG](https://zh.wikipedia.org/wiki/File:1jiao2005b.JPG "fig:1jiao2005b.JPG") | 直径为：19mm。正面為行名、面额及年号；背面为[兰花圖案及行名](../Page/兰花.md "wikilink")[汉语拼音](../Page/汉语拼音.md "wikilink")。色泽为[钢](../Page/钢.md "wikilink")[白色](../Page/白色.md "wikilink")，材质为[不锈钢](../Page/不锈钢.md "wikilink")，币外缘为圆柱面。該幣为1999版1角的改版，于2005年8月31日發行。                                                                          |
| 5角     | [Cr05f.jpg](https://zh.wikipedia.org/wiki/File:Cr05f.jpg "fig:Cr05f.jpg")[Cr05r.jpg](https://zh.wikipedia.org/wiki/File:Cr05r.jpg "fig:Cr05r.jpg")                               | 直径为：20.5mm。正面為行名、面额及年号；背面为[荷花圖案及行名](../Page/荷花.md "wikilink")[汉语拼音](../Page/汉语拼音.md "wikilink")。色泽为[金](../Page/金色.md "wikilink")[黄色](../Page/黄色.md "wikilink")，材质为[钢芯镀](../Page/钢.md "wikilink")[铜](../Page/铜.md "wikilink")[合金](../Page/合金.md "wikilink")，币外缘为间断丝齿，共有六个丝齿段，每个丝齿段有八个齿距相等的丝齿。該幣于2002年11月18日發行。 |
| 1元     | [Cr1f.jpg](https://zh.wikipedia.org/wiki/File:Cr1f.jpg "fig:Cr1f.jpg")[Cr1r.jpg](https://zh.wikipedia.org/wiki/File:Cr1r.jpg "fig:Cr1r.jpg")                                     | 直径为：25mm。正面為行名、面额及年号；背面为[菊花圖案及行名](../Page/菊花.md "wikilink")[汉语拼音](../Page/汉语拼音.md "wikilink")。色泽为[镍](../Page/镍.md "wikilink")[白色](../Page/白色.md "wikilink")，材质为[钢芯镀](../Page/钢.md "wikilink")[镍](../Page/镍.md "wikilink")，币外缘为圆柱面，并印有“[RMB](../Page/RMB.md "wikilink")”字符标记。該幣于2000年10月16日發行。                 |

### 曾发行的

[Chinese_20_renminbi_yuan_location.jpg](https://zh.wikipedia.org/wiki/File:Chinese_20_renminbi_yuan_location.jpg "fig:Chinese_20_renminbi_yuan_location.jpg")

#### 1角

  - 铝合金：1999-2003年（2004年无发行）
  - 不锈钢：2005-2016年

#### 5角

  - 2002-2018年

#### 1元

  - 1999-2018年

## 参考文献

  -
  -
  -
## 外部連結

{{-}}