**第五次反法同盟**（War of the Fifth
Coalition），1809年[普魯士](../Page/普魯士.md "wikilink")、[奧地利等國聯合第五次反法同盟](../Page/奧地利.md "wikilink")，奧地利偷袭法國在德國的領土，[拿破仑不等](../Page/拿破仑.md "wikilink")[西班牙戰事結束](../Page/半島戰爭.md "wikilink")，匆匆率兵回國，東征奧地利，奧地利軍隊雖然一開始取得優勢，但後來情勢逆轉，奧地利被迫簽訂《維也納和約》，再一次割地求和。

## 註解

<references/>

[category:拿破崙戰爭](../Page/category:拿破崙戰爭.md "wikilink")