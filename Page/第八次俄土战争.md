**第八次俄土战争**发生于1828年至1829年间，此时正值希腊人摆脱土耳其统治的[希腊独立战争](../Page/希腊独立战争.md "wikilink")，
[俄国趁火打劫](../Page/俄国.md "wikilink")，[法国也支持希腊](../Page/法蘭西王國.md "wikilink")，英国诗人[拜伦率领一支志愿军赴希腊作战](../Page/拜伦.md "wikilink")，[土耳其与埃及联军战败](../Page/土耳其.md "wikilink")。1829年9月，俄土双方签定《[亚得里亚堡和约](../Page/亚得里亚堡和约.md "wikilink")》，土耳其向俄国割让[外高加索沿海的领土](../Page/外高加索.md "wikilink")。1832年，土耳其被迫承认希腊独立。

## 参见

  - [俄土战争](../Page/俄土战争.md "wikilink")

[Category:俄土战争](../Category/俄土战争.md "wikilink")