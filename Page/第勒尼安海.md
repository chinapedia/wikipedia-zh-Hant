[Locatie_Tyrreense_Zee.PNG](https://zh.wikipedia.org/wiki/File:Locatie_Tyrreense_Zee.PNG "fig:Locatie_Tyrreense_Zee.PNG")
**第勒尼安海**（或译**提雷尼亞海**；[意大利語](../Page/意大利語.md "wikilink")：Mar
Tirreno；）是[地中海的一部分](../Page/地中海.md "wikilink")，位於[意大利半島西面](../Page/意大利半島.md "wikilink")。海域被意大利的[薩丁島](../Page/薩丁島.md "wikilink")、[西西里島](../Page/西西里島.md "wikilink")、[利古里亞](../Page/利古里亞.md "wikilink")、[托斯卡納](../Page/托斯卡納.md "wikilink")、[拉齐奥](../Page/拉齐奥.md "wikilink")、[坎帕尼亞](../Page/坎帕尼亞.md "wikilink")、[卡拉布里亞及法國的](../Page/卡拉布里亞.md "wikilink")[科西嘉島包圍著](../Page/科西嘉島.md "wikilink")。

## 名字由來

第勒尼安海的名字是來自[意大利的原住民族](../Page/意大利.md "wikilink")[埃特魯斯坎](../Page/伊特拉斯坎文明.md "wikilink")（Etruscans），傳說中這個民族在他們的王子Tyrrhenus帶領下由[呂底亞遷移到今天](../Page/呂底亞.md "wikilink")[托斯卡納一帶](../Page/托斯卡納.md "wikilink")。

## 地理

由於第勒尼安海位於歐非[斷層線附近](../Page/斷層.md "wikilink")，因此海底下有山脈及火山，海底最深處達3785米。

## 出入口

  - [墨西拿海峽](../Page/墨西拿海峽.md "wikilink")--位於[西西里島與](../Page/西西里島.md "wikilink")[意大利靴形](../Page/意大利.md "wikilink")[半島的腳趾之間](../Page/亞平寧半島.md "wikilink")，寬度只有3公里。
  - 科西嘉海峽—位於[科西嘉島與](../Page/科西嘉島.md "wikilink")[意大利](../Page/意大利.md "wikilink")[托斯卡納之間](../Page/托斯卡納.md "wikilink")，寬80公里。
  - [博尼法喬海峽](../Page/博尼法喬海峽.md "wikilink")--位於[科西嘉島與](../Page/科西嘉島.md "wikilink")[薩丁島之間](../Page/薩丁島.md "wikilink")，寬11公里。
  - \-{[西西里海峽](../Page/西西里海峽.md "wikilink")}---位於[西西里島與](../Page/西西里島.md "wikilink")[突尼西亞之間](../Page/突尼西亞.md "wikilink")，寬160公里。
  - [薩丁島與](../Page/薩丁島.md "wikilink")[突尼西亞之間的地中海海域](../Page/突尼西亞.md "wikilink")，寬200公里。

## 島嶼

  - [托斯卡納對出的](../Page/托斯卡納.md "wikilink")[托斯卡納群島](../Page/托斯卡納群島.md "wikilink")（L'arcipelago
    toscano）
      - Capraia
      - Pianosa
      - Giannutri
      - [基度山](../Page/基度山.md "wikilink")（Monte Cristo）
      - [吉廖島](../Page/吉廖島.md "wikilink")（Isola del Giglio）
      - [艾爾巴島](../Page/艾爾巴島.md "wikilink")（Elba）

<!-- end list -->

  - [拉丁姆對出的](../Page/拉丁姆.md "wikilink")[庞廷群岛](../Page/庞廷群岛.md "wikilink")（L'arcipelago
    pontino）
      - Ponza
      - Palmarola
      - Gavi
      - Zannone
      - Ventotene
      - Santo Stefano

<!-- end list -->

  - [西西里](../Page/西西里.md "wikilink")[米拉佐](../Page/米拉佐.md "wikilink")（Milazzo）對出的[伊奧利亞群島](../Page/伊奧利亞群島.md "wikilink")（Aeolian
    Islands, Isole Eolie）
      - [斯特龍博利島](../Page/斯特龍博利島.md "wikilink")（Stromboli）
      - 阿利庫迪島（Alicudi）
      - 菲里庫迪島（Filicudi）
      - [巴西盧佐島](../Page/巴西盧佐島.md "wikilink")（Basiluzzo）
      - [利帕里島](../Page/利帕里島.md "wikilink")（Lipari）
      - [薩利納島](../Page/薩利納島.md "wikilink")（Salina）
      - [武爾卡諾島](../Page/武爾卡諾島.md "wikilink")（Vulcano）
      - [帕納雷阿島](../Page/帕納雷阿島.md "wikilink")（Panarea）

<!-- end list -->

  - [西西里](../Page/西西里.md "wikilink")[特拉帕尼](../Page/特拉帕尼.md "wikilink")（Trapani）對出的[埃加迪群島](../Page/埃加迪群島.md "wikilink")（Isloe
    Egadi）
      - 萊萬佐島（Levanzo）
      - 法維尼亞納島（Favignana）
      - 馬雷蒂莫島（Marettimo）

<!-- end list -->

  - [那不勒斯灣的島嶼](../Page/那不勒斯灣.md "wikilink")
      - [卡普里島](../Page/卡普里島.md "wikilink")（Capri）
      - [普羅奇達](../Page/普羅奇達.md "wikilink")（Procida）
      - [伊斯基亞](../Page/伊斯基亞.md "wikilink")（Ischia）

[Category:地中海](../Category/地中海.md "wikilink")
[Category:陆缘海](../Category/陆缘海.md "wikilink")
[Category:欧洲地理](../Category/欧洲地理.md "wikilink")
[Category:義大利地理](../Category/義大利地理.md "wikilink")