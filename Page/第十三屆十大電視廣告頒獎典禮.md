**《第十三屆十大電視廣告頒獎典禮》（[英文](../Page/英文.md "wikilink")：）**，於2007年5月6日浸會大學會堂舉行，主持為[朱茵](../Page/朱茵.md "wikilink")、[陳啟泰](../Page/陳啟泰.md "wikilink")、[袁文傑](../Page/袁文傑.md "wikilink")、[張嘉倫](../Page/張嘉倫.md "wikilink")，目的為表揚2006年1月1日至2006年12月31日播出的[香港](../Page/香港.md "wikilink")[電視](../Page/電視.md "wikilink")[廣告](../Page/廣告.md "wikilink")，是2007年[廣告界的其中一項盛事](../Page/廣告.md "wikilink")。

## 得獎名單

  - 最高榮譽大獎
      - [菓汁先生](../Page/菓汁先生.md "wikilink") －先生話篇

<!-- end list -->

  - 十大最受歡迎電視廣告
      - [可口可樂](../Page/可口可樂.md "wikilink") －全城喝球
      - [國泰航空](../Page/國泰航空.md "wikilink") －手足情深
      - [Tempo](../Page/Tempo.md "wikilink") －分手篇
      - [麥當勞板燒雞腿飽](../Page/麥當勞.md "wikilink") －怪獸篇
      - [Visa卡](../Page/Visa.md "wikilink")
        －[袋鼠篇](../Page/袋鼠.md "wikilink")
      - [海洋公園](../Page/海洋公園.md "wikilink")
        －[水母](../Page/水母.md "wikilink")[萬花筒](../Page/萬花筒.md "wikilink")
      - [菓汁先生](../Page/菓汁先生.md "wikilink") －先生話篇
      - [地鐵公司](../Page/地鐵公司.md "wikilink") －請勿衝門篇
      - [Tempo](../Page/Tempo.md "wikilink") －海邊拍照篇
      - [益力多](../Page/益力多.md "wikilink") －划艇篇

<!-- end list -->

  - 最受歡迎電視廣告男演員大獎
      - [位元堂](../Page/位元堂.md "wikilink")
        －優質[中藥篇](../Page/中藥.md "wikilink")
        ([位元堂](../Page/位元堂.md "wikilink")[中醫](../Page/中醫.md "wikilink")[師傅](../Page/師傅.md "wikilink"))

<!-- end list -->

  - 最受歡迎電視廣告女演員大獎
      - [恒基兆業](../Page/恒基兆業.md "wikilink")[比華利山別墅](../Page/比華利山別墅.md "wikilink")
        －豪門望族 ([環球小姐](../Page/環球小姐.md "wikilink"))

<!-- end list -->

  - 最受歡迎電視廣告明星大獎
      - [麥當勞板燒雞腿飽](../Page/麥當勞.md "wikilink") －怪獸篇
        ([陳奕迅](../Page/陳奕迅.md "wikilink"))

<!-- end list -->

  - 最可愛電視廣告小演員大獎
      - [菓汁先生](../Page/菓汁先生.md "wikilink") －先生話篇 (佻皮小學生)

<!-- end list -->

  - 最受歡迎電視廣告歌曲大獎
      - [One2Free](../Page/One2Free.md "wikilink")[音樂頻道](../Page/音樂.md "wikilink")
        －追逐篇 (愛得太遲 ──[古巨基](../Page/古巨基.md "wikilink"))

<!-- end list -->

  - 最深刻印象電視廣告大獎
      - [地鐵](../Page/地鐵.md "wikilink") －請勿衝門篇

<!-- end list -->

  - 最受年青人歡迎電視廣告大獎
      - [PCCW mobile](../Page/電訊盈科流動通訊.md "wikilink")
        －[MOOV樂手機](../Page/MOOV.md "wikilink")

<!-- end list -->

  - 最具健康活力電視廣告大獎
      - [醫之選強化](../Page/醫之選.md "wikilink")[關節配方](../Page/關節.md "wikilink")
        －舞動香江篇

<!-- end list -->

  - 最具創意服務電視廣告大獎
      - [1010](../Page/1010.md "wikilink")
        [3G隨身TV](../Page/3G.md "wikilink") －簡約篇

<!-- end list -->

  - 最溫馨電視廣告大獎
      - [新世界傳動網](../Page/新世界傳動網.md "wikilink")
        －媽媽話音[短訊篇](../Page/短訊.md "wikilink")

<!-- end list -->

  - 最受歡迎公益電視廣告大獎
      - [香港吸煙與健康委員會](../Page/香港吸煙與健康委員會.md "wikilink") －二手香口膠

## 相關連結

  - [亞洲電視](../Page/亞洲電視.md "wikilink")
  - [十大電視廣告頒獎典禮](../Page/十大電視廣告頒獎典禮.md "wikilink")

## 外部連結

  - [第十三屆十大電視廣告頒獎典禮官方網頁](https://web.archive.org/web/20071126100718/http://www.hkatv.com/special_events/07/tvc_award_2007/)

[Category:十大電視廣告頒獎典禮](../Category/十大電視廣告頒獎典禮.md "wikilink")