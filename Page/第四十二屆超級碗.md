**第四十二屆超級碗**（英語：**Super Bowl
XLII**）是[國家美式足球聯盟第四十二屆](../Page/國家美式足球聯盟.md "wikilink")[超級碗比賽](../Page/超級碗.md "wikilink")，比賽球隊是[新英格蘭愛國者及](../Page/新英格蘭愛國者.md "wikilink")[紐約巨人](../Page/紐約巨人.md "wikilink")，於美國東部時間2月3日下午6時半\[1\]，在[亞利桑那州](../Page/亞利桑那州.md "wikilink")[格蘭岱爾](../Page/格蘭岱爾.md "wikilink")[鳳凰城大學球場舉行](../Page/鳳凰城大學球場.md "wikilink")。最终纽约巨人队凭借终场前35秒的一次成功[达阵以](../Page/达阵.md "wikilink")17-14赢得超级碗冠军。

## 轉播

  - 美國：[霍士電視台](../Page/福克斯广播公司.md "wikilink")

## 表演嘉賓

  - 國歌：[喬丁·斯帕克斯](../Page/喬丁·斯帕克斯.md "wikilink")
  - 半場表演：[Tom Petty and The
    Heartbreakers](../Page/Tom_Petty.md "wikilink")

## 詳細賽果

## 参考文献

## 外部連結

  - [官方網站](http://www.superbowl.com)

[42](../Category/超級碗.md "wikilink")
[Category:2008年美国体育](../Category/2008年美国体育.md "wikilink")
[Category:2008年美国电视](../Category/2008年美国电视.md "wikilink")

1.  <http://www.azsuperbowl.com/game_day.aspx>