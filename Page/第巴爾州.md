<div class="notice metadata" id="disambig">

[Disambig.svg](https://zh.wikipedia.org/wiki/File:Disambig.svg "fig:Disambig.svg")<small>本條目記敘的是**迪勃拉州**。其他同名的行政區尚有[第巴爾區](../Page/第巴爾區.md "wikilink")。</small>

</div>

**第巴爾州**（[阿爾巴尼亞語](../Page/阿爾巴尼亞語.md "wikilink")：Dibër）位於[阿爾巴尼亞中北部](../Page/阿爾巴尼亞.md "wikilink")，由[-{zh-hans:布尔奇察区;
zh-hant:布爾基則區;}-](../Page/布爾基則區.md "wikilink")、[第巴爾區](../Page/第巴爾區.md "wikilink")、[馬蒂區所組成](../Page/馬蒂區.md "wikilink")，[-{zh-hans:爱尔巴桑州;
zh-hant:艾巴申州;}-](../Page/艾巴申州.md "wikilink")、[庫克斯州](../Page/庫克斯州.md "wikilink")、[-{zh-hans:莱什州;
zh-hant:列澤州;}-](../Page/列澤州.md "wikilink")、[-{zh-hans:斯库台州;
zh-hant:士科德州;}-相鄰](../Page/士科德州.md "wikilink")，該州東部邊境與[北馬其頓共和國接壤](../Page/北馬其頓共和國.md "wikilink")。