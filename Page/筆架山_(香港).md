**筆架山**（），亦作**畢架山**，舊名**煙墩山**，是[香港一個](../Page/香港.md "wikilink")[山峰](../Page/山峰.md "wikilink")。筆架山位於[石硤尾](../Page/石硤尾.md "wikilink")、[大窩坪以北](../Page/大窩坪.md "wikilink")，高海拔457米。筆架山屬於[獅子山郊野公園範圍](../Page/獅子山郊野公園.md "wikilink")，山上設有[民航處](../Page/民航處.md "wikilink")[雷達站](../Page/雷達.md "wikilink")（早於1978年連同[柏架山及](../Page/柏架山.md "wikilink")[大帽山航空雷達站一併啟用](../Page/大帽山.md "wikilink")），負責協助機場指揮航空交通\[1\]，並設有行車路（[龍欣道](../Page/龍欣道.md "wikilink")）方便維修員上山。而筆架山西南山腳一帶，則被開發成為[高尚住宅區](../Page/高尚住宅.md "wikilink")。

[筆架山隧道貫通筆架山連接北九龍和](../Page/筆架山隧道.md "wikilink")[新界東](../Page/新界東.md "wikilink")，共有兩條管道。首條管道原為20世紀初[九廣鐵路興建時開鑿](../Page/九廣鐵路.md "wikilink")，名為，1980年代因九廣鐵路鋪設雙軌及進行電氣化，而在隧道西側開鑿一條新的管道，而新筆架山隧道現為[港鐵公司管理](../Page/港鐵公司.md "wikilink")，舊有隧道則被改為[煤氣及深港](../Page/煤氣.md "wikilink")[液化天然氣輸送管道](../Page/液化天然氣.md "wikilink")。

## 地理

筆架山東面為[獅子山](../Page/獅子山_\(香港\).md "wikilink")，西北面為[尖山](../Page/尖山_\(香港\).md "wikilink")，西南面則為[鴉巢山](../Page/鴉巢山.md "wikilink")，此外山峰附近有[鐵路坳和](../Page/鐵路坳.md "wikilink")[九龍坳](../Page/九龍坳.md "wikilink")。筆架山以北是[新界東](../Page/新界東.md "wikilink")[沙田區的](../Page/沙田區.md "wikilink")[地龍口](../Page/地龍口.md "wikilink")，以南則是近石硤尾的[大窩坪](../Page/大窩坪.md "wikilink")。

## 名稱

筆架山因其形狀與古時放[毛筆的筆架相似](../Page/毛筆.md "wikilink")，因而得名。現在很多人普遍都將其寫為畢架山。

至於英文名「」，源於煙墩山。煙墩山的位置與明清史籍中的九龍墩台相近，爲用作燒草放煙發出訊號的海防設施，故得其名，但該墩台具體位置已不可考。

## 住宅區

[Beacon_Hill_Apartments_201612.jpg](https://zh.wikipedia.org/wiki/File:Beacon_Hill_Apartments_201612.jpg "fig:Beacon_Hill_Apartments_201612.jpg")
因擁有整個[九龍半島](../Page/九龍半島.md "wikilink")、[香港島北部和](../Page/香港島.md "wikilink")[維多利亞港的優美景觀](../Page/維多利亞港.md "wikilink")，筆架山山腳已被開發為[高級住宅區](../Page/高級住宅.md "wikilink")。住宅區共分為兩部份，其一指[深水埗區](../Page/深水埗區.md "wikilink")[大窩坪](../Page/大窩坪.md "wikilink")[南昌街的](../Page/南昌街.md "wikilink")[畢架山花園及](../Page/畢架山花園.md "wikilink")[帝景峰](../Page/帝景峰.md "wikilink")，另一則指[九龍城區](../Page/九龍城區.md "wikilink")[歌和老街及](../Page/歌和老街.md "wikilink")[聯合道以北一帶](../Page/聯合道.md "wikilink")（包括[廣播道](../Page/廣播道.md "wikilink")「五台山」）。

窩打老道及歌和老街處的筆架山環境清靜，近[義本道曾有](../Page/義本道.md "wikilink")[九龍仔軍人宿舍](../Page/九龍仔軍人宿舍.md "wikilink")，後來地皮被[長江實業地產收購並興建](../Page/長江實業地產.md "wikilink")[畢架山一號](../Page/畢架山一號.md "wikilink")。

### 學校

而山上有一間[英基學校協會的國際學校](../Page/英基學校協會.md "wikilink")，於[安域道及義德道的交界](../Page/安域道.md "wikilink")，名為[畢架山學校](../Page/畢架山學校.md "wikilink")。

### 醫院

[香港浸信會醫院](../Page/香港浸信會醫院.md "wikilink")（，簡稱浸會醫院）是[香港一間全科](../Page/香港.md "wikilink")[基督教醫院](../Page/基督教.md "wikilink")，位於筆架山[窩打老道](../Page/窩打老道.md "wikilink")222號，約在[窩打老道及](../Page/窩打老道.md "wikilink")[聯合道交界](../Page/聯合道.md "wikilink")，成立於1963年，為[香港浸信會聯會社會服務機構之一](../Page/香港浸信會聯會.md "wikilink")。

[無綫電視劇集](../Page/無綫電視.md "wikilink")《[天幕下的戀人](../Page/天幕下的戀人.md "wikilink")》曾於畢架山學校及[朗德道取景](../Page/朗德道.md "wikilink")。

## 區內街道

筆架山的主要通道有義德道。山上主要道路名為筆架山道，而以下為其他小路及掘頭路。

  - [和域道](../Page/和域道.md "wikilink")
  - [安域道](../Page/安域道.md "wikilink")
  - [義本道](../Page/義本道.md "wikilink")
  - [朗德道](../Page/朗德道.md "wikilink")

而筆架山隧道東面的[碧景林路在](../Page/碧景林路.md "wikilink")[畢架山一號興建後變成了私家路](../Page/畢架山一號.md "wikilink")。

## 交通

[Moonbeam_Terrace_Bus_Stop.jpg](https://zh.wikipedia.org/wiki/File:Moonbeam_Terrace_Bus_Stop.jpg "fig:Moonbeam_Terrace_Bus_Stop.jpg")

山下的主要道路有[七號幹線](../Page/七號幹線.md "wikilink")[龍翔道](../Page/龍翔道.md "wikilink")、[歌和老街](../Page/歌和老街.md "wikilink")、[南昌街](../Page/南昌街.md "wikilink")、[達之路和](../Page/達之路.md "wikilink")[大埔道](../Page/大埔道.md "wikilink")。

  - 筆架山（義德道）住宅區

如果要前往港島區可乘搭免費穿梭巴士從山上往[樂富](../Page/樂富.md "wikilink")，或是乘搭[專線小巴](../Page/專線小巴.md "wikilink")29B往[九龍塘站](../Page/九龍塘站_\(香港\).md "wikilink")；

而往新界則相對方便得多，可往映月台巴士站乘搭多線巴士往[上水](../Page/上水.md "wikilink")、[粉嶺](../Page/粉嶺.md "wikilink")、[大埔](../Page/大埔_\(香港\).md "wikilink")、[馬鞍山及](../Page/馬鞍山_\(香港市鎮\).md "wikilink")[沙田](../Page/沙田_\(香港\).md "wikilink")。

  - 筆架山山頂

從[龍翔道可經](../Page/龍翔道.md "wikilink")[龍欣道前往筆架山山頂](../Page/龍欣道.md "wikilink")。另外，[麥理浩徑第五段也經過接近山頂的地方](../Page/麥理浩徑.md "wikilink")，[三義徑](../Page/三義徑.md "wikilink")、[貴妃徑亦可接駁](../Page/貴妃徑.md "wikilink")[麥理浩徑第五段](../Page/麥理浩徑.md "wikilink")，[戰地遺跡徑則可通往二戰時期](../Page/戰地遺跡徑.md "wikilink")[醉酒灣防線於畢架山上建造的多個機鎗堡](../Page/醉酒灣防線.md "wikilink")、座標石和地洞等古蹟，其中一個座標石是全香港最大的。\[2\]

## 註釋

## 參考資料

<div class="references-small">

<references />

</div>

{{-}}  {{-}}

[Category:香港山峰](../Category/香港山峰.md "wikilink")
[Category:九龍城區](../Category/九龍城區.md "wikilink")
[Category:深水埗區](../Category/深水埗區.md "wikilink")
[Category:沙田區](../Category/沙田區.md "wikilink")
[Category:具特殊科學價值地點](../Category/具特殊科學價值地點.md "wikilink")
[Category:新九龍](../Category/新九龍.md "wikilink")

1.  [筆架山進場著陸二次監察雷達站](http://www.flickr.com/photos/minghong/3337101727/)
2.  [郊野樂行：戰地遺跡徑](http://hiking.gov.hk/chi/trail_list/other_route/war_relics_trail/introduction.htm/)