**-{zh:等离子体;zh-cn:等离子体;zh-tw:電漿;zh-hk:等離子體;}-**（又稱**-{zh:电浆;zh-cn:电浆;zh-tw:等離子體;zh-hk:電漿;}-**）是在[固態](../Page/固態.md "wikilink")、[液態和](../Page/液態.md "wikilink")[氣態以外的第四大](../Page/氣態.md "wikilink")[物質狀態](../Page/物質狀態.md "wikilink")，其特性與前三者截然不同。

氣體在高溫或強[電磁場下](../Page/電磁場.md "wikilink")，會變為等離子體。在這種狀態下，氣體中的原子會擁有比正常更多或更少的[電子](../Page/電子.md "wikilink")，從而形成陰離子或陽離子，即帶負電荷或正電荷的粒子。\[1\]氣體中的任何[共價鍵也會分離](../Page/共價鍵.md "wikilink")。\[2\]

由於等離子體含有許多[載流子](../Page/載流子.md "wikilink")，因此它能夠[導電](../Page/導電.md "wikilink")，對電磁場也有很強的反應。和氣體一樣，等離子體的形狀和體積並非固定，而是會根據容器而改變；但和氣體不一樣的是，在磁場的作用下，它會形成各種結構，例如絲狀物、圓柱狀物和[雙層等](../Page/雙層_\(等離子體\).md "wikilink")。

等離子體是宇宙[重子物質最常見的形態](../Page/重子.md "wikilink")，其中大部分存在於稀薄的[星系際空間](../Page/外层空间#星系際空間.md "wikilink")（特別是[星系團內介質](../Page/星系團內介質.md "wikilink")）和[恆星之中](../Page/恆星.md "wikilink")。\[3\]

## 性質

[plasma_fountain.gif](https://zh.wikipedia.org/wiki/File:plasma_fountain.gif "fig:plasma_fountain.gif")。在地球大氣和太陽風的相互作用下，氧、氦、氫離子會從南北極地區上空向太空噴射。北極上方暗黃色的是氣體向外流失的區域，而綠色的則是[北極光](../Page/北極光.md "wikilink")，即等離子體返回大氣釋放能量的區域。\[4\]\]\]

### 定義

等離子體是由未結合離子所組成的電中性物質，其中陰離子和陽離子的總電荷約等於零。雖然這些離子不相互結合，但這並不意味著它們不受到力的影響：等離子體中的每顆帶電粒子都受到其他粒子移動時產生的電磁場的影響。\[5\]\[6\]等離子體的定義有三個重要部分：\[7\]\[8\]

1.  **等離子體近似**：帶電粒子之間的距離必須足夠接近，使得每顆粒子都能夠影響許多鄰近的粒子，而不是只影響最接近的粒子，從而產生集體性效應。只有當每顆帶電粒子的影響範圍內都有平均超過一顆帶電粒子，等離子體近似才是有效的。這一影響範圍稱為德拜球，是一個半徑為[德拜長度的球體空間](../Page/德拜長度.md "wikilink")。德拜球內的粒子數量稱為[等離子參數](../Page/等離子參數.md "wikilink")，由希臘字母Λ表示。
2.  **體積相互作用**：相對等離子體的整體大小來說，德拜長度必須很短。這意味著相互作用主要在等離子體的體積內部，而不是它的邊緣上。若符合這個準則，則等離子體可視為準中性。
3.  **等離子體頻率**：電子和電子之間的碰撞頻率必須比電子和中性粒子之間的碰撞頻率高得多。若滿足此條件，則靜電效應會比普通氣體動力學效應強得多。

### 參數的範圍

電漿的參數可以在數個[數量級之間變化](../Page/數量級.md "wikilink")，但在參數上顯然不同的電漿，卻有相當類似的性質（參考），下表只考慮傳統帶正負電的電漿，不考慮特殊的[夸克-膠子漿](../Page/夸克-膠子漿.md "wikilink")。
等離子體的各種參數可以有跨越幾個數量級的數值範圍。不過，有的等離子體雖然在參數上顯然不同，但性質卻十分相似。以下只考慮普通的原子等離子體，不考慮[夸克-膠子漿這類奇異現象](../Page/夸克-膠子漿.md "wikilink")。

<table>
<caption>等離子體參數的典型範圍</caption>
<thead>
<tr class="header">
<th><p>特性</p></th>
<th><p>地球上的等離子體</p></th>
<th><p>地球外的等離子體</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>大小</strong></p></td>
<td><p>10<sup>−6</sup> <a href="../Page/公尺.md" title="wikilink">m</a>（實驗室中的等離子體）<br />
10<sup>2</sup> m（閃電）</p></td>
<td><p>10<sup>−6</sup> m（航天器護套）<br />
10<sup>25</sup> m（星系際星雲）</p></td>
</tr>
<tr class="even">
<td><p><strong>壽命</strong></p></td>
<td><p>10<sup>−8</sup>至10<sup>−6</sup> <a href="../Page/秒.md" title="wikilink">s</a>（激光產生的等離子體）<br />
10<sup>7</sup> s（熒光燈）</p></td>
<td><p>10<sup>1</sup> s（太陽耀斑）<br />
10<sup>17</sup> s（星系際等離子體）</p></td>
</tr>
<tr class="odd">
<td><p><strong>密度</strong></p></td>
<td><p>10<sup>7</sup> m<sup>−3</sup>至<br />
10<sup>32</sup> m<sup>−3</sup>（慣性約束等離子體）</p></td>
<td><p>1 m<sup>−3</sup>（星系際介質）<br />
10<sup>30</sup> m<sup>−3</sup>（恆星核心）</p></td>
</tr>
<tr class="even">
<td><p><strong>溫度</strong></p></td>
<td><p>~0 <a href="../Page/開爾文.md" title="wikilink">K</a>（非中性結晶等離子體[9]）<br />
10<sup>8</sup> K（核聚變磁性等離子）</p></td>
<td><p>10<sup>2</sup> K（極光）<br />
10<sup>7</sup> K（太陽核心）</p></td>
</tr>
<tr class="odd">
<td><p><strong>磁場</strong></p></td>
<td><p>10<sup>−4</sup> <a href="../Page/特斯拉.md" title="wikilink">T</a>（實驗室中的等離子體）<br />
10<sup>3</sup> T（脈衝功率等離子體）</p></td>
<td><p>10<sup>−12</sup> T（星系際介質）<br />
10<sup>11</sup> T（接近中子星）</p></td>
</tr>
</tbody>
</table>

### 電離度

[電離是等離子體存在的必要條件](../Page/電離.md "wikilink")。「等離子體密度」通常指的是「電子密度」，也就是每單位體積中的自由電子數量。[電離度指的是等離子體中電子數比正常更多或更少的原子所佔的比例](../Page/電離度.md "wikilink")，這主要受溫度影響。就算氣體粒子中只有1%是電離粒子，這一氣體也會表現出等離子體的一些特性，例如會受磁場影響、能夠導電等等。電離度\(\alpha\)的明確定義是\(\alpha = \frac{n_i}{n_i + n_n}\)，其中\(n_i\)是離子的數量密度（每單位體積中的數量），而\(n_n\)是中性原子的數量密度。電子密度\(n_e\)與電離度的關係是\(n_e = \langle Z\rangle n_i\)，其中\(\langle Z\rangle\)是離子的平均電荷態。

### 溫度

等離子體的溫度籠統地來說代表了每顆粒子的平均動能，一般用[開爾文或](../Page/開爾文.md "wikilink")[電子伏特來量度](../Page/電子伏特.md "wikilink")。要維持等離子體的電離狀態，一般需要較高的溫度。[薩哈電離方程說明](../Page/薩哈電離方程.md "wikilink")，[電子溫度與](../Page/電子溫度.md "wikilink")[電離能之比決定了等離子體的電離度](../Page/電離能.md "wikilink")（密度也有較弱的影響）。在低溫下，離子和電子會互相結合，形成結合態，即原子，\[10\]等離子態也會因此最終變為氣體。

在大多數情況下，等離子體中的電子很接近[熱平衡](../Page/熱平衡.md "wikilink")，所以電子溫度有良好的定義。在[紫外線](../Page/紫外線.md "wikilink")、高能粒子或強[電場等的影響下](../Page/電場.md "wikilink")，電子的能量分佈和[麦克斯韦-玻尔兹曼分布會有較大的偏離](../Page/麦克斯韦-玻尔兹曼分布.md "wikilink")，但儘管如此，電子溫度仍然具有良好定義。由於質量相差懸殊，所以電子和電子之間比電子和離子或中性原子之間更快地達到熱平衡。因此，離子溫度和電子溫度可以有巨大的差異（通常前者更低）。這種情況在弱電離等離子體中尤為常見，其中的離子一般接近[室溫](../Page/室溫.md "wikilink")。

#### 高溫與低溫等離子體

等離子體可以根據其電子、離子和中性粒子的相對溫度歸為兩類──高溫等離子體和低溫等離子體。在高溫等離子體中，電子、離子和中性粒子處於同一溫度，即熱平衡；在低溫等離子體中，電子有較高的溫度，而離子和中性粒子的溫度則比電子低很多，有時甚至接近室溫。\[11\]

#### 完全與非完全電離

等離子體可以根據電離程度分為冷、熱兩種。熱等離子體中的粒子幾乎完全電離，而冷等離子體中則只有小部分電離粒子（比如1%）。「冷」、「熱」等離子體在不同文獻中可能會有不同的含義。就算是在所謂的「冷」等離子體中，電子溫度也可以達到幾千攝氏度。

### 等離子體電勢

[Bliksem_in_Assen.jpg](https://zh.wikipedia.org/wiki/File:Bliksem_in_Assen.jpg "fig:Bliksem_in_Assen.jpg")是地球表面常見的等離子體現象。每次閃電一般在1億[伏特電壓下釋放出](../Page/伏特.md "wikilink")30,000[安培](../Page/安培.md "wikilink")，同時放出[可見光](../Page/可見光.md "wikilink")、[無線電波](../Page/無線電波.md "wikilink")、[X光乃至](../Page/X光.md "wikilink")[伽馬射線](../Page/伽馬射線.md "wikilink")。\[12\]閃電中的等離子體溫度可達到28000 K，電子密度可超過。\]\]

帶電粒子間的空間內的電勢稱為「等離子體電勢」或「空間電勢」。不過由於[德拜鞘層的緣故](../Page/德拜鞘層.md "wikilink")，如果往等離子體中插入電極，所測量的電勢一般都會比等離子體電勢低很多。等離子體是良好的導電體，所以其內部的電場很小。從而有「準中性」這一重要的概念，即：在足夠大的範圍內，等離子體中的陽離子和陰離子有近乎相同的密度（\(n_e = \langle Z\rangle n_i\)）；在[德拜長度尺度上](../Page/德拜長度.md "wikilink")，則會有不均勻的電荷分佈。在產生[雙層的特殊情況下](../Page/雙層_\(等離子體\).md "wikilink")，電荷分離的尺度可以是德拜長度的數十倍。

要得出電勢和電場的大小，單單靠測量淨[電荷密度是不足夠的](../Page/電荷密度.md "wikilink")。一種常見的做法是假設電子滿足[玻爾茲曼關係](../Page/玻爾茲曼關係.md "wikilink")：

\[n_e \propto e^{e\Phi/k_BT_e}.\]

對等號兩邊求導，可得出從密度計算電場的公式：

\[\vec{E} = (k_BT_e/e)(\nabla n_e/n_e).\]

等離子體也有可能不是準中性的，例如電子束就只含陰離子。非中性等離子體一般密度都非常低，或體積非常小，否則[靜電力的會使等離子體自相排斥並消散](../Page/靜電力.md "wikilink")。

在[天體物理學所研究的等離子體中](../Page/天體物理學.md "wikilink")，[德拜屏蔽會避免電場在大尺度上](../Page/電場屏蔽.md "wikilink")（超過德拜長度）影響等離子體。但是，等離子體中的帶電粒子會產生[磁場](../Page/磁場.md "wikilink")，並受磁場的影響。這有可能導致高度複雜的現象，例如形成雙層──電荷間分離數十個德拜長度。等離子體在外部和內部磁場影響下的動力學現象，是[磁流體力學的研究對象](../Page/磁流體力學.md "wikilink")。

### 磁化

當等離子體的自身磁場足以影響帶電粒子的運動時，就可稱之為「磁化等離子體」。常用的量化條件是，某粒子在與其他粒子碰撞之前，要在磁場內迴旋至少一圈，即：\(\omega_{\mathrm{ce}} / v_{\mathrm{coll}} > 1\)，其中\(\omega_{\mathrm{ce}}\)是電子迴轉頻率，\(v_{\mathrm{coll}}\)是電子碰撞率。一種較常見的情況是，等離子體中的電子是磁化的，陽離子則不是。磁化等離子體不具各向同性：它在平行和垂直於磁場的方向上有不同的性質。雖然等離子體自身的電場很小，但在磁場中運動的等離子體也會產生電場：\(\mathbf{E} = -v\times\mathbf{B}\)（其中\(\mathbf{E}\)是電場，\(\mathbf{v}\)是速度，\(\mathbf{B}\)是磁場）。這一電場不受[德拜鞘層影響](../Page/德拜鞘層.md "wikilink")。\[13\]

### 等離子體和氣體的比較

等離子體實質上是電離的氣體，但也往往被視為固體、液體和氣體以外的第四大[物質狀態](../Page/物質狀態.md "wikilink")，\[14\]\[15\]並與其他低能量[相態分隔開來](../Page/相態.md "wikilink")。雖然它和氣體一樣沒有固定的形狀和體積，但是兩者間仍有以下若干不同之處。

| 性質                               | 氣體                                                                                                                                                                                                                   | 等離子體                                                                                                                                                                                                                                                         |
| -------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [電導率](../Page/電導率.md "wikilink") | **非常低**：例如[空氣是良好的](../Page/空氣.md "wikilink")[絕緣體](../Page/絕緣體.md "wikilink")，但在[電場強度超過](../Page/電場.md "wikilink") \(3 \times 10^6\) [V](../Page/伏特.md "wikilink")\(/\)[m](../Page/公尺.md "wikilink") 時會分解成等離子體。\[16\] | **通常非常高**：在許多應用中，可假設等離子體的電導率為無限大。                                                                                                                                                                                                                            |
| 不同性質粒子                           | **1種**：所有氣體粒子的行為類似，都受[重力及其他粒子](../Page/重力.md "wikilink")[碰撞的影響](../Page/碰撞.md "wikilink")。                                                                                                                           | **2至3種**：[電子](../Page/電子.md "wikilink")、[離子](../Page/離子.md "wikilink")、[質子和](../Page/質子.md "wikilink")[中子可以從電荷的正負和大小來區別](../Page/中子.md "wikilink")，所以在許多情況下行為都會有差異，例如有不同的速度和溫度等。這能產生一些特殊的[波和](../Page/等離子波.md "wikilink")[不穩定性](../Page/不穩定性.md "wikilink")。 |
| 速度分佈                             | **[麦克斯韦-玻尔兹曼分布](../Page/麦克斯韦-玻尔兹曼分布.md "wikilink")**：粒子間的碰撞會使所有氣體粒子符合馬克士威分佈，其中速度較高的粒子非常少。                                                                                                                            | **通常非馬克士威分佈**：熱等離子體的碰撞交互作用不強，而外力可以導致等離子體遠遠偏離局部平衡，並產生一組速度特別高的粒子。                                                                                                                                                                                              |
| 交互作用                             | **成雙**：交互作用-{局限}-於兩顆粒子之間的碰撞，三顆粒子間的碰撞是極為罕見的。                                                                                                                                                                          | **集體**：粒子和粒子可以在較大的距離上通過電磁力相互影響，所以會產生波以及其他有組織性的運動。                                                                                                                                                                                                            |

## 常見的等離子體

[Plasma_Cutter.jpg](https://zh.wikipedia.org/wiki/File:Plasma_Cutter.jpg "fig:Plasma_Cutter.jpg")，其中的等離子體是由[電弧對噴出的氣體加熱產生](../Page/電弧.md "wikilink")。\]\]

等離子體從質量和體積上都是宇宙中最常見的物質[相態](../Page/相態.md "wikilink")。\[17\]大部分來自太空的可見光都源於恆星，而恆星是由等離子體所組成，其溫度所對應的輻射含較強的可見光。更宏觀地來看，宇宙絕大部分普通物質（即[重子物質](../Page/重子.md "wikilink")）都位於[星系際空間](../Page/外层空间#星系際空間.md "wikilink")，同樣是由等離子體組成，其溫度則高得多，主要輻射[X-射線](../Page/X-射線.md "wikilink")。儘管如此，如果納入普通物質以外所有類型的能量，那麼在全宇宙的總能量密度中，就有96%不屬於普通物質（進而也不是等離子體），而是[冷暗物質和](../Page/冷暗物質.md "wikilink")[暗能量](../Page/暗能量.md "wikilink")。\[18\]

1937年，[漢尼斯·阿爾文論證](../Page/漢尼斯·阿爾文.md "wikilink")，如果宇宙充斥著等離子體，這些物質就會產生電流，從而產生星系尺度上的磁場。\[19\]在獲得[諾貝爾物理學獎後](../Page/諾貝爾物理學獎.md "wikilink")，他又強調：

> 要了解某個等離子體區域內的各種現象，既要測繪出磁場，又要測繪出電場和電流。太空中佈滿了縱橫交錯的電流網絡，能夠在大尺度乃至非常大尺度上傳遞能量和動量。這些電流往往會縮成絲狀或表面電流，後者很有可能會使太空──星際和星系際空間──形成一種胞狀結構。\[20\]

[太陽和其他恆星一樣是由等離子體所組成](../Page/太陽.md "wikilink")。\[21\]其最外層稱為[日冕](../Page/日冕.md "wikilink")，是溫度約為10<sup>6</sup> [K的等離子體](../Page/開爾文.md "wikilink")，從太陽表面開始向整個太陽系擴張，充斥[行星際空間](../Page/行星際物質.md "wikilink")，並止於[日球層頂](../Page/日球層頂.md "wikilink")。\[22\]在日球層頂以外，也充斥著等離子體[星際介質](../Page/星際物質.md "wikilink")。連無法直接觀測的[黑洞相信也是通過吸入吸積盤中的等離子體而壯大的](../Page/黑洞.md "wikilink")，\[23\]而且和由發光等離子體所組成的[相對論性噴流有緊密的聯繫](../Page/相對論性噴流.md "wikilink")，\[24\]如延伸5千光年之遙的[室女A星系噴流](../Page/室女A星系#噴流.md "wikilink")。\[25\]

等離子體中如果有塵粒，淨負電荷會積累在塵粒上，這些塵粒的性質類似於質量很大的陰離子，且可以視為等離子體的一個組成部分。 \[26\]\[27\]

<table>
<caption>常見的等離子體</caption>
<thead>
<tr class="header">
<th><p>人造</p></th>
<th><p>地球上</p></th>
<th><p>太空中</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/等離子顯示器.md" title="wikilink">等離子顯示器</a>，如電視螢幕。</li>
<li><a href="../Page/熒光燈.md" title="wikilink">熒光燈</a>（低能量光源）、<a href="../Page/霓虹燈.md" title="wikilink">霓虹燈</a>。[28]</li>
<li>火箭排氣口、<a href="../Page/離子推進器.md" title="wikilink">離子推進器</a></li>
<li><a href="../Page/航天器.md" title="wikilink">航天器在</a><a href="../Page/進入大氣.md" title="wikilink">進入大氣時的</a><a href="../Page/防熱盾.md" title="wikilink">防熱盾前方</a></li>
<li>電暈放電<a href="../Page/臭氧.md" title="wikilink">臭氧發生器</a></li>
<li><a href="../Page/聚變能.md" title="wikilink">聚變能研究</a></li>
<li><a href="../Page/弧光燈.md" title="wikilink">弧光燈</a>、<a href="../Page/電弧焊機.md" title="wikilink">電弧焊機和</a><a href="../Page/等離子炬.md" title="wikilink">等離子炬等所產生的</a><a href="../Page/電弧.md" title="wikilink">電弧</a></li>
<li><a href="../Page/等離子燈.md" title="wikilink">等離子燈</a></li>
<li><a href="../Page/特斯拉線圈.md" title="wikilink">特斯拉線圈所產生的電弧</a></li>
<li><a href="../Page/半導體器件製造.md" title="wikilink">製造半導體器件時用到的等離子</a>，包括<a href="../Page/反應離子刻蝕.md" title="wikilink">反應離子刻蝕</a>、<a href="../Page/濺鍍.md" title="wikilink">濺鍍</a>、<a href="../Page/等離子清洗.md" title="wikilink">等離子清洗</a>、<a href="../Page/等離子體增強化學氣相沉積.md" title="wikilink">等離子體增強化學氣相沉積等過程</a>。</li>
<li><a href="../Page/激光.md" title="wikilink">激光產生的等離子體</a>，在高能激光和物質反應時出現。</li>
<li><a href="../Page/感應耦合等離子.md" title="wikilink">感應耦合等離子</a>，用於可見<a href="../Page/光譜法.md" title="wikilink">光譜法和</a><a href="../Page/質譜法.md" title="wikilink">質譜法</a>，一般在<a href="../Page/氬氣.md" title="wikilink">氬氣中形成</a>。</li>
<li>磁誘導等離子體，通過微波產生，用於諧振耦合法。</li>
<li><a href="../Page/靜電.md" title="wikilink">靜電火花</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/閃電.md" title="wikilink">閃電</a></li>
<li><a href="../Page/聖艾爾摩之火.md" title="wikilink">聖艾爾摩之火</a></li>
<li><a href="../Page/中高層大氣放電.md" title="wikilink">中高層大氣放電</a></li>
<li><a href="../Page/精靈_(閃電).md" title="wikilink">精靈</a></li>
<li><a href="../Page/電離層.md" title="wikilink">電離層</a></li>
<li><a href="../Page/等離子層.md" title="wikilink">等離子層</a></li>
<li><a href="../Page/極光.md" title="wikilink">極光</a></li>
<li>某些<a href="../Page/火焰.md" title="wikilink">火焰</a>[29]</li>
<li><a href="../Page/等離子體噴泉.md" title="wikilink">等離子體噴泉</a>，又稱極風</li>
</ul></td>
<td><ul>
<li><a href="../Page/太陽.md" title="wikilink">太陽及其他</a><a href="../Page/恆星.md" title="wikilink">恆星</a></li>
<li><a href="../Page/太陽風.md" title="wikilink">太陽風</a></li>
<li><a href="../Page/行星際物質.md" title="wikilink">行星際物質</a></li>
<li><a href="../Page/星際物質.md" title="wikilink">星際物質</a></li>
<li><a href="../Page/外层空间#星系際空間.md" title="wikilink">星系際物質</a></li>
<li><a href="../Page/木衛一.md" title="wikilink">木衛一和</a><a href="../Page/木星.md" title="wikilink">木星之間的</a><a href="../Page/流量管.md" title="wikilink">流量管</a></li>
<li><a href="../Page/吸積盤.md" title="wikilink">吸積盤</a></li>
<li><a href="../Page/星雲.md" title="wikilink">星雲</a></li>
<li><a href="../Page/彗尾.md" title="wikilink">彗星離子尾</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 複雜現象

雖然用以描述等離子體的式子相對簡單，但是等離子體的各種現象卻是錯綜複雜的。這種從簡單物理模型中出現不可預見行為的現象，正是[複雜系統的特性](../Page/複雜系統.md "wikilink")。此類系統從某種意義上處於有序和無序間的邊界上，無法用簡單[光滑的數學函數或純粹的隨機過程來描述](../Page/光滑.md "wikilink")。等離子體結構的特點在於，形狀尖銳，在空間上斷斷續續（即特徵間的距離大於特徵本身的大小），甚或產生[分形](../Page/分形.md "wikilink")。不少現象最早是在實驗室中觀察到的，之後，天文物理學者又發現其廣泛存在於宇宙之中。

### 成絲

[白克蘭電流是一種絲弦狀結構](../Page/白克蘭電流.md "wikilink")，\[30\]可見於[等離子燈](../Page/等離子燈.md "wikilink")、[極光](../Page/極光.md "wikilink")、\[31\][閃電](../Page/閃電.md "wikilink")、\[32\][電弧](../Page/電弧.md "wikilink")、[太陽耀斑](../Page/太陽耀斑.md "wikilink")、\[33\][超新星遺跡等的等離子現象](../Page/超新星遺跡.md "wikilink")。\[34\]弦中的電流密度更高，在磁場的影響下會產生磁繩結構。\[35\]標準大氣壓下的高功率微波分解也會造成絲狀結構的形成。\[36\]

高功率激光脈衝的自我聚焦效應也會產生絲狀等離子體。在高功率下，折射率的非線性部分變得重要。因為激光束的中心比外圍更亮，所以中心的折射率會比外圍更高，使得激光進一步聚焦。亮度峰值（福照度）因此增加，並使激光束產生等離子體。等離子體的折射率低於1，會使激光束發散。在自我聚焦效應和等離子體發散效應之間的相互作用下，等離子體形成絲狀，其長度短至[微米](../Page/微米.md "wikilink")，長至[公里](../Page/公里.md "wikilink")。\[37\]這樣產生的絲狀等離子體的特點是離子密度低，這是由於電離電子有發散的作用。\[38\]

### 激波和雙層

當激波（移動）或[雙層](../Page/雙層_\(等離子體\).md "wikilink")（靜止）這些薄片結構存在的情況下，等離子體的性質從薄片的一邊到另一邊可以有急劇的變化（在幾個[德拜長度以內](../Page/德拜長度.md "wikilink")）。雙層之中的局部電荷分離使雙層內部有較大的電勢差異，但在雙層以外不產生任何電場。這可以分隔開雙層兩邊性質不同的等離子體，並使離子和電子加速。

### 電場和電路

等離子體的準中性意味著，等離子體中的任何電流都必須形成迴路。這種迴路同樣遵守[基爾霍夫電路定律](../Page/基爾霍夫電路定律.md "wikilink")，並具有[電阻和](../Page/電阻.md "wikilink")[電感](../Page/電感.md "wikilink")。一般來說，等離子體迴路都必須當做強耦合系統，即某一區域的性質受整個迴路的影響。強耦合性加上非線性會產生複雜的現象。這些迴路中儲存著磁能，一旦迴路受到破壞，例如因等離子體不穩定性，這一能量將會以加熱和加速的形式釋放出來。[日冕中的加熱現象通常就是以此為解釋的](../Page/日冕.md "wikilink")。等離子體電流，特別是磁場對齊的電流（一般稱為[白克蘭電流](../Page/白克蘭電流.md "wikilink")），也出現在地球極光和絲狀等離子體中。

### 胞狀結構

等離子體中所形成的高梯度薄片可以分隔開磁化強度、密度、溫度等性質不同的區域，形成胞狀結構，如[磁層](../Page/磁層.md "wikilink")、[太陽圈和](../Page/太陽圈.md "wikilink")[太陽圈電流片等](../Page/太陽圈電流片.md "wikilink")。[漢尼斯·阿爾文曾寫道](../Page/漢尼斯·阿爾文.md "wikilink")：「從宇宙學的觀點來看，太空研究中最重要的新發現莫過於宇宙的胞狀結構。在原位測量方法能夠研究的一切宇宙範圍內，無一不有『胞壁』。這些帶電流的薄片把太空分割成磁化強度、密度、溫度等等性質各異的區域。」\[39\]

### 臨界電離速度

當等離子體和中性氣體之間達到一定的相對速度時，就會發生失控的電離反應，這一臨界速度稱為[臨界電離速度](../Page/臨界電離速度.md "wikilink")。臨界電離過程可以將快速流動氣體的動能轉化為電離能和等離子體熱能，適用範圍廣泛。臨界現象會產生空間或時間上急劇變化的結構，是複雜系統的一個典型特徵。

### 超冷等離子體

超冷等離子體的製備過程如下。[磁光阱先將中性](../Page/磁光阱.md "wikilink")[原子降溫至](../Page/原子.md "wikilink")1 [mK以下](../Page/開爾文.md "wikilink")，再用另一個激光束把僅僅足夠的能量傳給原子的最外層電子，使其脫離原子的束縛。超冷等離子體的優勢在於，其初始條件能夠很好地設定及調整，包括大小和電子溫度。通過調整用於電離的激光的波長，便能控制逃逸電子的動能。這一動能是由激光脈衝的頻寬決定的，最低可達0.1
K。電離後產生的離子一開始會保留中性原子原來的溫度，但溫度會因為所謂的亂度加熱效應而迅速升高。此類非平衡超冷等離子體會快速地演變，並展現出各種有趣的現象。\[40\]

### 非中性等離子體

等離子體的導電性以及電場強度和範圍意味著，在足夠大的體積內，正負電荷大體相等，是為準中性。當等離子體含有過高的淨電荷密度，甚至完全以單種帶電粒子組成時，就稱為[非中性等離子體](../Page/非中性等離子.md "wikilink")。電場在這種等離子體中的作用是舉足輕重的。例子有：帶電粒子束、[彭寧離子阱中的電子雲以及](../Page/彭寧離子阱.md "wikilink")[正子等離子體等等](../Page/正子.md "wikilink")。\[41\]

### 塵埃等離子

[塵埃等離子體含有細小的帶電塵粒](../Page/塵埃等離子體.md "wikilink")，通常存在於太空之中。塵粒能積累較高的電荷，並相互影響。實驗室中的塵埃等離子體又稱「複雜等離子體」。\[42\]

### 不可滲透等離子體

不可滲透等離子體是一種熱等離子體，它對於氣體和冷等離子體的性質如同不可滲透的固體，而且能夠受別的物質推挪。以漢尼斯·阿爾文為首的研究組曾經在1960至1970年代短暫地研究不可滲透等離子體，試圖在[核聚變反應中用它來隔開聚變等離子體和反應爐壁](../Page/核聚變.md "wikilink")。\[43\]然而他們不久後發現，這種組態下的外部磁場會使等離子體產生所謂的扭折不穩定性，導致熱量過多地向爐壁流失。\[44\]

2013年，一組材料科學家宣稱，他們不用[磁約束](../Page/磁約束聚變.md "wikilink")，只用一層超高壓力低溫氣體，成功地生成穩定的不可滲透等離子體。雖然由於高壓的關係無法通過光譜法取得等離子體的性質，但從等離子體對各種[納米結構](../Page/納米結構.md "wikilink")[合成過程的間接影響可以清晰看出](../Page/化學合成.md "wikilink")，這種約束方法是有效的。他們還發現，在維持不滲透性幾十秒後，等離子體和氣體的界面會篩選離子，這有可能引起第二種加熱模式（稱為粘性加熱）。這種模式意味著，反應會有不同的動力學特性，並會產生複雜的[納米材料](../Page/納米材料.md "wikilink")。\[45\]

## 數學描述

[Magnetic_rope.svg](https://zh.wikipedia.org/wiki/File:Magnetic_rope.svg "fig:Magnetic_rope.svg")，其中有自我束緊的複雜磁場線和電流路徑。圖中帶箭頭的線同時代表電流和磁場線，由內之外（即紅、藍、綠）強度降低。\[46\]\]\]
要完全描述等離子體的狀態，原則上須要寫下所有粒子的位置和速度，並計算出等離子體範圍內的電磁場。不過這種繁複的做法一般是不切實際的，在現實中也不可能測量出每顆粒子的動態。所以，等離子體物理學家通常會運用簡化的模型，這些模型可分為以下兩大類。

### 流體模型

流體模型利用光滑的量來描述等離子體，如密度和某位置周圍的平均速度（參見[等離子體參數](../Page/等離子體參數.md "wikilink")）。簡單的流體模型有[磁流體力學](../Page/磁流體力學.md "wikilink")，它結合[麥克斯韋方程組和](../Page/麥克斯韋方程組.md "wikilink")[納維-斯托克斯方程組](../Page/納維-斯托克斯方程.md "wikilink")，並把等離子體視為遵守這套方程組的單一流體。再推廣一步，有將離子和電子分開描述的雙流體模型。當碰撞頻率足夠高，使等離子體的速度分佈近似[麦克斯韦-玻尔兹曼分布時](../Page/麦克斯韦-玻尔兹曼分布.md "wikilink")，流體模型就相對準確。由於流體模型通常把等離子體描述成每個空間位置具有某特定溫度的單一的流，因此無法描述等離子體束或雙層這類速度隨空間改變的結構，以及任何波粒效應。

### 動力學模型

動力學模型描述等離子體中每一點的速度分佈函數，所以無須假設麥克斯韋方程組。在無碰撞等離子體中，往往需要此類模型。動力學模型有兩種：第一種在速度和位置上設下格子，並在格子上表示光滑化的分佈函數；另一種稱為「胞中粒子」方法，它通過追蹤一大群單獨粒子的軌跡來描述動力學狀態。動力學模型的計算密集度一般比流體模型更高。[弗拉索夫方程式能夠描述帶電粒子與電磁場發生相互作用的系統的動力學狀態](../Page/弗拉索夫方程式.md "wikilink")。

在磁化等離子體中，[陀螺動力學方法可以大大降低一個完全使用動力學模型的模擬的計算密集度](../Page/迴旋動理學.md "wikilink")。

## 人造等離子體

多數人造等離子體是通過對氣體增加電磁場產生的。實驗室或工業產生的等離子體一般根據以下各項標準分類：

  - 所用的能源類型──直流電、射頻源、微波源等等
  - 能源的操作壓力──真空（小於10 [mTorr](../Page/托.md "wikilink")，1
    [Pa](../Page/帕斯卡.md "wikilink")）、中等壓力（約1 Torr，100
    Pa）、大氣壓力（760 Torr，100 kPa）
  - 等離子體的電離度──完全電離、部分電離、弱電離
  - 等離子體組成部分的溫度關係──熱等離子體（\(T_e = T_i = T_{gas}\)）、冷等離子體（\(T_e \gg T_i = T_{gas}\)）
  - 生成等離子體所用的電極構造
  - 等離子體粒子的磁化強度──完全磁化（離子和電子都受磁場束縛在[拉莫軌道上](../Page/拉莫頻率.md "wikilink")）、部分磁化（只有電子受磁場束縛）、非磁化（磁場太弱，無法把粒子束縛在軌道上，但仍能產生[洛倫茲力](../Page/洛倫茲力.md "wikilink")）

### 等離子體的製備

[Simple_representation_of_a_discharge_tube_-_plasma.png](https://zh.wikipedia.org/wiki/File:Simple_representation_of_a_discharge_tube_-_plasma.png "fig:Simple_representation_of_a_discharge_tube_-_plasma.png")
[Plasma_jacobs_ladder.jpg](https://zh.wikipedia.org/wiki/File:Plasma_jacobs_ladder.jpg "fig:Plasma_jacobs_ladder.jpg")在空氣中產生的等離子體\]\]
等離子的製備方法有許多種，但生成和維持都需要能量的輸入。\[47\]對介電氣體或其他流體（絕緣體）施加電壓，產生的[電場會把負電荷拉向](../Page/電場.md "wikilink")[陽極](../Page/陽極.md "wikilink")，而把正電荷拉向[陰極](../Page/陰極.md "wikilink")。\[48\]當電壓不斷增加，[電極化會對材料施加應力](../Page/電極化.md "wikilink")，直到超過其[介電極限](../Page/介電強度.md "wikilink")。這時發生[電擊穿現象](../Page/電擊穿.md "wikilink")，釋放[電弧](../Page/電弧.md "wikilink")，使絕緣材料電離，變為導電體。其背後的原理是：初始電離所釋放的電子，在每次撞擊中性原子時，都會再釋放一顆電子，如此類推，迅速產生一連串的連鎖電離反應。\[49\]

#### 電弧

[Cascade-process-of-ionization.svg](https://zh.wikipedia.org/wiki/File:Cascade-process-of-ionization.svg "fig:Cascade-process-of-ionization.svg")
[Townsend_Discharge.svg](https://zh.wikipedia.org/wiki/File:Townsend_Discharge.svg "fig:Townsend_Discharge.svg")

當電流密度及電離度達到一定的程度，兩個電極之間就會形成發光的[電弧](../Page/電弧.md "wikilink")。這是一種空間上連續的放電現象，類似於[閃電](../Page/閃電.md "wikilink")。電弧的連續軌跡上的[電阻會產生熱量](../Page/電阻.md "wikilink")，進而分解更多的氣體分子，使更多的原子電離（電離度取決於溫度），氣體如此逐漸變為熱等離子體。熱等離子體處於[熱平衡](../Page/熱平衡.md "wikilink")，也就是說，電子和質量大的粒子（原子、分子和離子）溫度相近。這是因為，在熱等離子體形成的時候，電子所接收的[電能會因電子數量龐大及流動性強而迅速分散](../Page/電能.md "wikilink")，再通過[彈性碰撞](../Page/彈性碰撞.md "wikilink")（即不喪失任何能量）傳遞給大質量粒子\[50\]。

### 工業及商業用等離子體

由於等離子體的溫度和密度範圍極廣，所以能應用在許多學術研究、科技及工業範疇中。工業用途有：工業及萃取[冶金學](../Page/冶金學.md "wikilink")、\[51\][等離子體噴塗等表面處理法](../Page/等離子體噴塗.md "wikilink")、微電子學蝕刻法、\[52\]金屬切割\[53\]和[焊接等](../Page/焊接.md "wikilink")。日常用途有汽車排氣淨化和[熒光燈等](../Page/熒光燈.md "wikilink")。\[54\]另外還有[航空航天工程中的](../Page/航空航天工程.md "wikilink")[超音速燃燒衝壓發動機](../Page/超音速燃燒衝壓發動機.md "wikilink")。\[55\]

#### 低密度放電

  - [發光放電等離子體](../Page/發光放電.md "wikilink")：最常見的等離子體之一，通過在兩個金屬電極間施加直流電或低頻無線電波（低於100 kHz）電場產生。[熒光燈所含的就是這種等離子體](../Page/熒光燈.md "wikilink")。\[56\]
  - [容性耦合等離子體](../Page/容性耦合等離子體.md "wikilink")：類似於發光放電等離子體，但由高頻無線電波電場產生，頻率通常為[13.56 MHz](../Page/ISM頻段.md "wikilink")。兩者差異在於，容性耦合等離子體的鞘層強度低很多。廣泛應用於[集成電路產業](../Page/集成電路.md "wikilink")，作等離子體蝕刻及等離子增強化學氣相沉積。\[57\]
  - 多級弧等離子體源：能製造低溫（約1 eV）高密度等離子體的儀器。
  - [電感耦合等離子體](../Page/電感耦合等離子體.md "wikilink")：性質和應用範疇類似於容性耦合等離子體，但產生原理是在容器外繞上線圈，使容器內形成等離子體。\[58\]
  - 波加熱等離子體：一般在無線電波頻段，這點類似於電感及容性耦合等離子體。例子有[螺旋波等離子體源和](../Page/螺旋波等離子體源.md "wikilink")[電子迴旋共振等](../Page/電子迴旋共振.md "wikilink")。\[59\]

#### 大氣壓力

  - [電弧](../Page/電弧.md "wikilink")：高溫、高功率放電現象，可用各種電源產生，常用於[冶金過程](../Page/冶金.md "wikilink")，如對含Al<sub>2</sub>O<sub>3</sub>的礦物進行冶煉，產生[鋁](../Page/鋁.md "wikilink")。
  - [電暈放電](../Page/電暈放電.md "wikilink")：低溫放電現象，在高壓電極的尖端形成，常用於[臭氧產生器和除塵器](../Page/臭氧.md "wikilink")。
  - [介質阻擋放電](../Page/介質阻擋放電.md "wikilink")：低溫放電現象，在高壓的細小間隙內形成，其中有絕緣塗層避免等離子體成為電弧。這種現象在工業中的用途與電暈放電相似，常被人們誤稱為電暈放電。應用還包括紡織物的幅處理，\[60\]有助染料、膠水等物質黏合在紡織物表面上。\[61\]
  - 電容放電：低溫等離子體。產生方法是，一個電極接上無線電波頻率電源（[13.56 MHz](../Page/ISM頻段.md "wikilink")），另一電極接地，兩極相距約1 cm。用[惰性氣體如氦](../Page/稀有气体.md "wikilink")、氬可以使這種放電現象穩定化。\[62\]
  - 壓電直接放電等離子體：低溫等離子體，在壓電變壓器的高壓端形成。這種低溫等離子體製備方法特別適用於不具備單獨高壓電源的高效、細小設備。

## 歷史

[威廉·克魯克斯在](../Page/威廉·克魯克斯.md "wikilink")1879年在他所研製的[克魯克斯管中首次發現等離子體](../Page/克魯克斯管.md "wikilink")，他稱之為「發光物質」。\[63\][約瑟夫·湯姆森在](../Page/約瑟夫·湯姆森.md "wikilink")1897年研究出克魯克斯管中所含的「陰極射線」物質的真實性質。\[64\][歐文·朗繆爾在](../Page/歐文·朗繆爾.md "wikilink")1928年創造了「plasma」一詞，現成為等離子體在歐洲各語言中的名稱，\[65\]源於[希臘文的](../Page/希臘文.md "wikilink")「πλάσμα」（模塑成型之物）。這樣命名，可能是因為克魯克斯管中的發光體會自行改變成管的形狀。\[66\]朗繆爾描述道：

> 除了在電極附近有含極少電子的鞘層以外，電離氣體含有大體相同數量的離子和電子，所以整體空間內的電荷很小。這一離子和電子的電荷達到平衡的空間，我們稱之為「plasma」。\[67\]

中文對「plasma」一詞的翻譯有二：取其最早的含義（整體保持電中性的電離物質），有中國大陸所用的「等離子-{}-體」；台灣則稱「電-{}-漿」。直至今天，一些不符合原先電中性定義的物質也會被人們稱為等離子-{}-體，如非中性等離子體和[夸克-膠子等離子-{}-體等](../Page/夸克-膠子等離子體.md "wikilink")。有中國大陸的物理學家對這一歷史遺留的尷尬翻譯表示質疑。\[68\]

## 參見

  - [等離子炬](../Page/等離子炬.md "wikilink")
  - [雙極性擴散](../Page/雙極性擴散.md "wikilink")

<!-- end list -->

  - [等離子體參數](../Page/等離子體參數.md "wikilink")

<!-- end list -->

  - [磁流體力學](../Page/磁流體力學.md "wikilink")
  - [電場屏蔽](../Page/電場屏蔽.md "wikilink")

<!-- end list -->

  - [等粒子物理重要著作列表](../Page/物理學重要著作列表#等粒子物理.md "wikilink")

<!-- end list -->

  - [夸克-膠子漿](../Page/夸克-膠子漿.md "wikilink")
  - [空間物理學](../Page/空間物理學.md "wikilink")

<!-- end list -->

  - [等離子顯示屏](../Page/等離子顯示屏.md "wikilink")
  - [極光](../Page/極光.md "wikilink")

## 注釋

## 參考資料

## 外部連結

  - [麻省理工學院I. H.
    Hutchinson：等離子體概論](http://silas.psfc.mit.edu/introplasma/index.html)
  - [怎樣在你的微波爐裡用葡萄製造發光的等離子體](http://c3po.barnesos.net/homepage/lpl/grapeplasma/)|[更多視頻](http://stewdio.org/plasma/)
  - [OpenPIC3D：用3D混合胞中粒子模型模擬等離子體動力學](http://comphys.narod.ru)
  - [Plasma Formulary
    Interactive](http://plasma-gate.weizmann.ac.il/pf/)（根據用戶輸入的等離子體參數繪圖的線上工具）

[\*](../Category/等离子体.md "wikilink")
[Category:物质状态](../Category/物质状态.md "wikilink")
[Category:基本物理概念](../Category/基本物理概念.md "wikilink")
[Category:电导体](../Category/电导体.md "wikilink")
[Category:气体](../Category/气体.md "wikilink")
[Category:天体物理学](../Category/天体物理学.md "wikilink")

1.

2.
3.
4.  Plasma fountain
    [Source](http://pwg.gsfc.nasa.gov/istp/news/9812/solar1.html), press
    release: [Solar Wind Squeezes Some of Earth's Atmosphere into
    Space](http://pwg.gsfc.nasa.gov/istp/news/9812/solarwind.html)

5.

6.

7.

8.

9.  參見：[加州大學聖迭戈分校的非中性等離子體研究組](http://sdphca.ucsd.edu/)

10.

11. von Engel, A. and Cozens, J.R. (1976) "Flame Plasma" in *Advances in
    electronics and electron physics*, L. L. Marton (ed.), Academic
    Press, ISBN 978-0-12-014520-1,
    [p. 99](https://books.google.com/books?id=0Mndi2cCMuUC&lpg=PA99)

12. 參見：[Flashes in the Sky: Earth's Gamma-Ray Bursts Triggered by
    Lightning](http://www.nasa.gov/vision/universe/solarsystem/rhessi_tgf.html)

13. Richard Fitzpatrick, *Introduction to Plasma Physics*, [Magnetized
    plasmas](http://farside.ph.utexas.edu/teaching/plasma/lectures/node10.html)

14. Yaffa Eliezer, Shalom Eliezer, *The Fourth State of Matter: An
    Introduction to the Physics of Plasma*, Publisher: Adam Hilger,
    1989, ISBN 978-0-85274-164-1, 226 pages, page 5

15.

16.

17. 一種常見的說法是，可見宇宙中超過99%的物質都是等離子體。見：和等。

18.
19.

20.

21.
22.

23. Mészáros, Péter (2010) *The High Energy Universe: Ultra-High Energy
    Events in Astrophysics and Cosmology*, Publisher    Cambridge
    University Press, ISBN 978-0-521-51700-3,
    [p. 99](https://books.google.com/books?id=NXvE_zQX5kAC&lpg=PA99&dq=%22Black%20hole%22%20plasma%20acreting&pg=PA99).

24. Raine, Derek J. and Thomas, Edwin George (2010) *Black Holes: An
    Introduction*, Publisher: Imperial College Press, ISBN
    978-1-84816-382-9,
    [p. 160](https://books.google.com/books?id=O3puAMw5U3UC&lpg=PA160)

25. Nemiroff, Robert and Bonnell, Jerry (11 December 2004) [Astronomy
    Picture of the Day](http://apod.nasa.gov/apod/ap041211.html),
    nasa.gov

26.

27.

28. [IPPEX Glossary of Fusion
    Terms](http://ippex.pppl.gov/fusion/glossary.html) . Ippex.pppl.gov.
    Retrieved on 2011-11-19.

29. "[Plasma and Flames – The Burning
    Question](http://www.plasmacoalition.org/plasma_writeups/flame.pdf)",
    from the Coalition for Plasma Science, retrieved 8 November 2012

30.

31.

32.

33.

34. . The University of Arizona

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46. [Evolution of the Solar
    System](http://history.nasa.gov/SP-345/ch15.htm#250)*, 1976*

47.
48.

49.
50.
51.

52.

53.

54.

55.

56.

57.

58.

59.

60.

61.

62.

63. 克魯克斯曾於1879年8月22日在[謝菲爾德對英國科學促進協會](../Page/謝菲爾德.md "wikilink")（British
    Association for the Advancement of
    Science）演講。[1](http://www.worldcatlibraries.org/wcpa/top3mset/5dcb9349d366f8ec.html)
    [2](http://www.tfcbooks.com/mall/more/315rm.htm)

64. 在1897年4月30日對[英國皇家科學研究所的晚間演講上首次公佈](../Page/英國皇家科學研究所.md "wikilink")，並發佈於：

65.

66.

67.
68.