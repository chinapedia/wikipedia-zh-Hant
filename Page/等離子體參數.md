[Magnetic_rope.svg](https://zh.wikipedia.org/wiki/File:Magnetic_rope.svg "fig:Magnetic_rope.svg")的[磁場](../Page/磁場.md "wikilink")。\]\]
**等離子體參數**就是一系列決定[電漿性質的參數](../Page/電漿.md "wikilink")。一般來說是以[厘米-克-秒制來當作參數的基本單位](../Page/厘米-克-秒制.md "wikilink")，但是[溫度卻是以](../Page/溫度.md "wikilink")[電子伏特](../Page/電子伏特.md "wikilink")(eV)當作單位，而[質量則是以](../Page/質量.md "wikilink")[質子](../Page/質子.md "wikilink")[質量](../Page/質量.md "wikilink")(*μ
= mi / mp* )的倍數當作單位。在這裡，''
K*是指[波長](../Page/波長.md "wikilink")、Z是指荷電狀態、*k''是指[波茲曼常數](../Page/波茲曼常數.md "wikilink")、γ是指[絕熱指數而Λ](../Page/絕熱指數.md "wikilink")
是指[库仑碰撞](../Page/库仑碰撞.md "wikilink")。[電漿簡單的說就是一群由不同的](../Page/電漿.md "wikilink")[離子構成的](../Page/離子.md "wikilink")，也被被稱為[物質的第四態](../Page/物質.md "wikilink")。它也可以看成一群[粒子的系統](../Page/粒子.md "wikilink")，因此可以以[統計的方式研究它](../Page/統計學.md "wikilink")。

## 基本的等離子體參數

以下是一些基本的離子體參數:

### 關於頻率

  - **電子迴轉頻率**:

\[\omega_{ce} = eB/m_ec = 1.76 \times 10^7 B \mbox{rad/s} \,\]

  - **離子迴轉頻率**

\[\omega_{ci} = eB/m_ic = 9.58 \times 10^3 Z \mu^{-1} B \mbox{rad/s} \,\]

  - **電子等離子體頻率**

\[\omega_{pe} = (4\pi n_ee^2/m_e\varepsilon_0)^{1/2} = 199.98\times n_e^{1/2} \mbox{rad/s}\]

  - **離子等離子體頻率**:

\[\omega_{pi} = (4\pi n_iZ^2e^2/m_i)^{1/2} = 1.32 \times 10^3 Z \mu^{-1/2} n_i^{1/2} \mbox{rad/s}\]

  - **低電子陷阱率**

\[\nu_{Te} = (eKE/m_e)^{1/2} = 7.26 \times 10^8 K^{1/2} E^{1/2} \mbox{s}^{-1} \,\]

  - **低離子陷阱率**:

\(\nu_{Ti} = (ZeKE/m_i)^{1/2} = 1.69 \times 10^7 Z^{1/2} K^{1/2} E^{1/2} \mu^{-1/2} \mbox{s}^{-1} \,\)

  - **電子碰撞率**:

\(\nu_e = 2.91 \times 10^{-6} n_e\,\ln\Lambda\,T_e^{-3/2} \mbox{s}^{-1}\)

  - **離子碰撞率**:

\(\nu_i = 4.80 \times 10^{-8} Z^4 \mu^{-1/2} n_i\,\ln\Lambda\,T_i^{-3/2} \mbox{s}^{-1}\)

### 關於長度

  - ''':

\[\Lambda_e= \sqrt{\frac{h^2}{2\pi m_ekT_e}}= 6.919\times 10^{-8}\,T_e^{-1/2}\,\mbox{cm}\]

  - **classical distance of closest approach**

\[e^2/kT=1.44\times10^{-7}\,T^{-1}\,\mbox{cm}\]

  - **電子迴轉半徑**:

\[r_e = v_{Te}/\omega_{ce} = 2.38\,T_e^{1/2}B^{-1}\,\mbox{cm}\]

  - **離子迴轉半徑**:

\[r_i = v_{Ti}/\omega_{ci} = 1.02\times10^2\,\mu^{1/2}Z^{-1}T_i^{1/2}B^{-1}\,\mbox{cm}\]

  - **電漿吸收深度**

\[c/\omega_{pe} = 5.31\times10^5\,n_e^{-1/2}\,\mbox{cm}\]

  - **[德拜長度](../Page/德拜長度.md "wikilink")**

\[\lambda_D = (kT/4\pi ne^2)^{1/2} = 7.43\times10^2\,T^{1/2}n^{-1/2}\,\mbox{cm}\]

### 關於速率

  - **電子平均速率**:

\[v_{Te} = (kT_e/m_e)^{1/2} = 4.19\times10^7\,T_e^{1/2}\,\mbox{cm/s}\]

  - **離子平均速率** :

\[v_{Ti} = (kT_i/m_i)^{1/2} = 9.79\times10^5\,\mu^{-1/2}T_i^{1/2}\,\mbox{cm/s}\]

  - **離子聲速**

\[c_s = (\gamma ZkT_e/m_i)^{1/2} = 9.79\times10^5\,(\gamma ZT_e/\mu)^{1/2}\,\mbox{cm/s}\]

  - **[阿爾文速率](../Page/漢尼斯·阿爾文.md "wikilink")**:

\[v_A = B/(4\pi n_im_i)^{1/2} = 2.18\times10^{11}\,\mu^{-1/2}n_i^{-1/2}B\,\mbox{cm/s}\]

### 其他

  - 質子質量/電子質量的根號

\[(m_e/m_p)^{1/2} = 2.33\times10^{-2} = 1/42.9 \,\]

  - [德拜球裡的粒子數量](../Page/德拜球.md "wikilink")

\[(4\pi/3)n\lambda_D^3 = 1.72\times10^9\,T^{3/2}n^{-1/2}\]

  - [阿爾文速率](../Page/漢尼斯·阿爾文.md "wikilink")/光速

\[v_A/c = 7.28\,\mu^{-1/2}n_i^{-1/2}B\]

  - 電子電漿/電子等離子體頻率

\[\omega_{pe}/\omega_{ce} = 3.21\times10^{-3}\,n_e^{1/2}B^{-1}\]

  - 離子電漿/離子等離子體頻率

\[\omega_{pi}/\omega_{ci} = 0.137\,\mu^{1/2}n_i^{1/2}B^{-1}\]

[Category:電漿體物理學](../Category/電漿體物理學.md "wikilink")