**筒蛇科**（[學名](../Page/學名.md "wikilink")：**''Aniliidae**''）又名**圓環蛇科**，是[蛇亞目下的一個](../Page/蛇亞目.md "wikilink")[單型科](../Page/單型.md "wikilink")。\[1\]\[2\]主要品種是分佈在[南美洲一帶的](../Page/南美洲.md "wikilink")[筒蛇](../Page/筒蛇.md "wikilink")。\[3\]筒蛇身上有蛇類退化了的[盆骨痕跡](../Page/盆骨.md "wikilink")，其[泄殖腔是突出而可被觀察的](../Page/泄殖腔.md "wikilink")；牠們屬於[卵胎生生物](../Page/卵胎生.md "wikilink")，主要進食[兩棲類及其它](../Page/兩棲類.md "wikilink")[爬蟲類的動物](../Page/爬蟲類.md "wikilink")。目前筒蛇科下有兩個品種已被確認。\[4\]

## 行為學描述

筒蛇主要分佈於[南美洲](../Page/南美洲.md "wikilink")[亞馬遜雨林](../Page/亞馬遜雨林.md "wikilink")、[圭亞那及](../Page/圭亞那.md "wikilink")[千里達及托巴哥一帶](../Page/千里達及托巴哥.md "wikilink")，體型普通，平均身長只有70[公分](../Page/公分.md "wikilink")。牠們是[卵胎生的蛇類](../Page/卵胎生.md "wikilink")，主要進食[甲蟲](../Page/鞘翅目.md "wikilink")、[蚓螈](../Page/無足目.md "wikilink")、小型洞棲蛇、[魚類及](../Page/魚類.md "wikilink")[蛙類等動物](../Page/蛙類.md "wikilink")。

## 解剖學描述

筒蛇身型筆直，身體各部位直徑相若，[尾部短小](../Page/尾巴.md "wikilink")。體紋呈[紅色或](../Page/紅色.md "wikilink")[黑色](../Page/黑色.md "wikilink")，細小的[眼睛藏於](../Page/眼睛.md "wikilink")[頭部的巨鱗之下](../Page/頭部.md "wikilink")。

## 分類學描述

该科蛇共有1属：

  - *Anilius* [筒蛇屬](../Page/筒蛇屬.md "wikilink")

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：筒蛇](https://web.archive.org/web/20090514074817/http://www.jcvi.org/reptiles/families/aniliidae.php)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:筒蛇科](../Category/筒蛇科.md "wikilink")

1.

2.

3.

4.