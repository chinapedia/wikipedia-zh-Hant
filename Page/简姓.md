**簡姓**是一個[中文姓氏](../Page/中文姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》中排名第382位。相关的姓有[台湾和](../Page/台湾.md "wikilink")[閩南一帶的](../Page/閩南.md "wikilink")[張簡姓](../Page/張簡姓.md "wikilink")，于[元末](../Page/元.md "wikilink")[明初](../Page/明.md "wikilink")[洪武四年辛亥时](../Page/洪武.md "wikilink")[漳州府](../Page/漳州.md "wikilink")[簡德潤入贅張姓進興家](../Page/簡德潤.md "wikilink")，希望將張簡兩姓均有子嗣可傳，和張家商量後，決定生子姓張簡以傳後人。

## 起源

  - 出自[姬姓](../Page/姬姓.md "wikilink")。以谥号为姓。[春秋时](../Page/春秋.md "wikilink")，[晋国有大夫](../Page/晋国.md "wikilink")[狐鞫居](../Page/狐鞫居.md "wikilink")，他的祖先是[周武王之子](../Page/周武王.md "wikilink")[唐叔虞的支裔](../Page/唐叔虞.md "wikilink")，狐鞫居的封邑在续，死后谥为续简伯，他的后代便以其谥号为姓，称简姓。
  - 出自[耿姓](../Page/耿姓.md "wikilink")。为[三国时](../Page/三国.md "wikilink")[蜀国](../Page/蜀国.md "wikilink")[简雍之后](../Page/简雍.md "wikilink")。简雍本姓耿，幽州人读"耿"与"简"同音，遂变为简姓。
  - 出自[检姓](../Page/检姓.md "wikilink")。[汉代时有句章尉检其明](../Page/汉代.md "wikilink")，因避讳而改姓简。

## 分布

中國大陸[江西](../Page/江西.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[广东](../Page/广东.md "wikilink")、[海南](../Page/海南.md "wikilink")、[四川](../Page/四川.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[云南](../Page/云南.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[福建與](../Page/福建.md "wikilink")[香港](../Page/香港.md "wikilink")、[台灣比较多](../Page/台灣.md "wikilink")。

## 台灣簡姓源流略述

## 知名人物

  - 古代

<!-- end list -->

  - [簡卿](../Page/簡卿.md "wikilink")：西漢武帝時（公元前140－87年間），山東東平人，以經學享譽天下。

  - [簡雍](../Page/簡雍.md "wikilink")：涿郡人，字憲和，少與先主劉備善，從至荊州，為從事中郎，往來使命，先主圍成都，入城說劉璋歸命，拜昭德將軍，性簡傲跌宕，滑譏諷諫言，為劉備所重視。

  -
  - 簡一山：五代後梁時，避契丹寇患，由范陽之涿洲，官由嶺外，旅南雄，漸至廣州，南漢時命子文會卜居黎涌鄉。

  - [簡正理](../Page/簡正理.md "wikilink")：宋代御史。居官廉介，以儒術飭吏治，時譽翕然。

  - 簡世傑：進賢人，自伯俊，隆興進士，任清江司理參事，范成大辟入幕府，改知蒲圻縣，不設科條，民不忍犯，遷知賀州卒。

  - 簡克己：南海人，少師事張栻，得其傳，退歸杜門，以真知實踐為事公，務啟迪後進，士無少長，咸稱簡先生。

  - [簡而廉](../Page/簡而廉.md "wikilink")：明代孝子。通五經，舉孝行，以明經任臨利訓導，著有《孝經解》。

  - [簡朝亮](../Page/簡朝亮.md "wikilink")：字季紀（1851－1933），號竹居，清末順德人。1875年求學於廣東名儒朱九江。研習經史、性理、詞章之學。後以講學著述為主。有《朱先生講學記》、《尚書集注述疏》、《論語集注補正述疏》。（見《中國近現代人名大辭典》、《民國人物小傳》）

<!-- end list -->

  - 當代

<!-- end list -->

  - [簡悅威](../Page/簡悅威.md "wikilink")：華人醫學家
  - [簡松年](../Page/簡松年.md "wikilink")：香港新界東區議員、前區域市政局議員
  - [簡悅強](../Page/簡悅強.md "wikilink")：前香港行政立法兩局首席非官守議員
  - [簡福飴](../Page/簡福飴.md "wikilink")：前香港特別行政區全國人大代表
  - [簡炳墀](../Page/簡炳墀.md "wikilink")：前香港冠軍練馬師
  - [簡而清](../Page/簡而清.md "wikilink")：香港作家
  - [簡慕華](../Page/簡慕華.md "wikilink")：香港女演員
  - [簡淑兒](../Page/簡淑兒.md "wikilink")：香港女演員
  - [簡大獅](../Page/簡大獅.md "wikilink")：台灣抗日民軍首領，台灣台北人。他對日軍侵佔台灣十分憤慨，並在台北聚眾起義，多次給予日軍以沉重的打擊。後日本政府勾結清朝官吏將他殺害
  - [簡吉](../Page/簡吉.md "wikilink")：台灣的社會運動者與政治人物，台灣日治時期台灣共產黨（台共）黨員
  - [簡又新](../Page/簡又新.md "wikilink")：前中華民國外交部長
  - [簡太郎](../Page/簡太郎.md "wikilink")：前中華民國內政部政務次長
  - [簡鳳君](../Page/簡鳳君.md "wikilink")：台灣第三屆《超級星光大道》歌唱選秀節目奪下殿軍
  - [簡筑翎](../Page/簡筑翎.md "wikilink")：藝名蝴蝶姐姐，臺灣藝人
  - [簡嫚書](../Page/簡嫚書.md "wikilink")：台灣新生代女演員
  - [簡佩玲](../Page/簡佩玲.md "wikilink")：台灣攝影師
  - [簡余晏](../Page/簡余晏.md "wikilink")：前台北市議員，曾任臺北市政府觀光傳播局局長
  - [簡肇棟](../Page/簡肇棟.md "wikilink")：前中華民國立法委員
  - [簡媜](../Page/簡媜.md "wikilink")：台灣當代散文作家
  - [簡福疆](../Page/簡福疆.md "wikilink")：鳳凰衛視新聞主播
  - [簡廷芮](../Page/簡廷芮.md "wikilink")：台灣女藝人，匿稱普普，組合Dears成員
  - [簡沛恩](../Page/簡沛恩.md "wikilink")：台灣女演員
  - [簡懿佳](../Page/簡懿佳.md "wikilink")：台灣體育女主播

[J簡](../Category/漢字姓氏.md "wikilink")
[Category:朝鮮語姓氏](../Category/朝鮮語姓氏.md "wikilink")
[\*](../Category/簡姓.md "wikilink")