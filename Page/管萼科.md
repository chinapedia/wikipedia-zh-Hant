**管萼科**又名[管萼木科或](../Page/管萼木科.md "wikilink")[木乃耳科](../Page/木乃耳科.md "wikilink")，共有9[属](../Page/属.md "wikilink")29[种](../Page/种.md "wikilink")，全部分布在[非洲最南端的](../Page/非洲.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")。

本科[植物为常绿小](../Page/植物.md "wikilink")[灌木](../Page/灌木.md "wikilink")；单[叶对生](../Page/叶.md "wikilink")，革质，有或无托叶；[花生长在枝条顶端](../Page/花.md "wikilink")，[花萼](../Page/花萼.md "wikilink")4数，有[颜色](../Page/颜色.md "wikilink")，管状，形成类似[花冠的形状](../Page/花冠.md "wikilink")；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")。

## 属

  - [巧玲木属](../Page/巧玲木属.md "wikilink") *Brachysiphon*
  - [叉苞木属](../Page/叉苞木属.md "wikilink") *Endonema*
  - [胭苞木属](../Page/胭苞木属.md "wikilink") *Glischrocolla*
  - [管萼木属](../Page/管萼木属.md "wikilink") *Penaea*
  - [瑞龙木属](../Page/瑞龙木属.md "wikilink") *Saltera*
  - [丽龙木属](../Page/丽龙木属.md "wikilink") *Sonderothamnus*
  - [肋柱木属](../Page/肋柱木属.md "wikilink") *Stylapterus*

### 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[管萼科](http://delta-intkey.com/angio/www/penaeace.htm)
  - [APG网站中的管萼科](http://www.mobot.org/MOBOT/Research/APWeb/orders/myrtalesweb2.htm)
  - [NCBI中的管萼科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=40022)

[\*](../Category/管萼科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")