**箱魨科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[魨形目的其中一科](../Page/魨形目.md "wikilink")。会发出狗叫声\[1\]。

## 分類

**箱魨科**下分12個屬，如下：

### 三棱角箱魨屬(*Acanthostracion*)

  - [幾內亞三棱角箱魨](../Page/幾內亞三棱角箱魨.md "wikilink")(*Acanthostracion
    guineensis*)
  - [背棘三棱角箱魨](../Page/背棘三棱角箱魨.md "wikilink")(*Acanthostracion
    notacanthus*)
  - [多角三棱角箱魨](../Page/多角三棱角箱魨.md "wikilink")(*Acanthostracion
    polygonia*)
  - [四角三棱角箱魨](../Page/四角三棱角箱魨.md "wikilink")(*Acanthostracion
    quadricornis*)

### 粒突六稜箱魨屬(*Anoplocapros*)

  - [似杏粒突六棱箱魨](../Page/似杏粒突六棱箱魨.md "wikilink")(*Anoplocapros
    amygdaloides*)
  - [無刺粒突六棱箱魨](../Page/無刺粒突六棱箱魨.md "wikilink")(*Anoplocapros inermis*)
  - [白帶粒突六棱箱魨](../Page/白帶粒突六棱箱魨.md "wikilink")(*Anoplocapros
    lenticularis*)
  - [壯體粒突六稜箱魨](../Page/壯體粒突六稜箱魨.md "wikilink")(*Anoplocapros robustus*)

### 六棱箱魨屬(*Aracana*)

  - [金黃六棱箱魨](../Page/金黃六棱箱魨.md "wikilink")(*Aracana aurita*)
  - [麗飾六棱箱魨](../Page/麗飾六棱箱魨.md "wikilink")(*Aracana ornata*)

### 羊箱魨屬(*Caprichthys*)

  - [羊箱魨](../Page/羊箱魨.md "wikilink")(*Caprichthys gymnura*)

### 棘棱六棱箱魨屬(*Capropygia*)

  - [棘棱六棱箱魨](../Page/棘棱六棱箱魨.md "wikilink")(*Capropygia unistriata*)

### 棘箱魨屬(*Kentrocapros*)

  -   - [棘箱魨](../Page/棘箱魨.md "wikilink")(*Kentrocapros aculeatus*)
      - [窄鰓棘箱魨](../Page/窄鰓棘箱魨.md "wikilink")(*Kentrocapros eco*)
      - [黃紋棘箱魨](../Page/黃紋棘箱魨.md "wikilink")(*Kentrocapros
        flavofasciatus*)：又稱黃帶棘箱魨。

### 棱箱魨屬(*Lactophrys*)

  - [斑點稜箱魨](../Page/斑點稜箱魨.md "wikilink")(*Lactophrys bicaudalis*)
  - [棱箱魨](../Page/棱箱魨.md "wikilink")(*Lactophrys trigonus*)

### 角箱魨屬(*Lactoria*)

  - [角箱魨](../Page/角箱魨.md "wikilink")(*Lactoria cornuta*)
  - [棘背角箱魨](../Page/棘背角箱魨.md "wikilink")(*Lactoria diaphana*)
  - [福氏角箱魨](../Page/福氏角箱魨.md "wikilink")(*Lactoria fornasini*)：又稱線紋角箱魨。
  - [復活節島角箱魨](../Page/復活節島角箱魨.md "wikilink")(*Lactoria paschae*)

### 箱魨屬(*Ostracion*)

  - [粒突箱魨](../Page/粒突箱魨.md "wikilink")(*Ostracion cubicus*)
  - [藍尾箱魨](../Page/藍尾箱魨.md "wikilink")(*Ostracion cyanurus*)
  - [無斑箱魨](../Page/無斑箱魨.md "wikilink")(*Ostracion immaculatus*)
  - [米點箱魨](../Page/米點箱魨.md "wikilink")(*Ostracion meleagris*)：又稱白點箱魨。
  - [吻鼻箱魨](../Page/吻鼻箱魨.md "wikilink")(*Ostracion
    rhinorhynchos*)：又稱突吻箱魨。
  - [藍帶箱魨](../Page/藍帶箱魨.md "wikilink")(*Ostracion solorensis*)
  - [粗皮箱魨](../Page/粗皮箱魨.md "wikilink")(*Ostracion trachys*)
  - [惠氏箱魨](../Page/惠氏箱魨.md "wikilink")(*Ostracion whitleyi*)

### 副棘箱魨屬(*Paracanthostracion*)

  - [林氏副棘箱魨](../Page/林氏副棘箱魨.md "wikilink")(*Paracanthostracion
    lindsayi*)

### 多板箱魨屬(*Polyplacapros*)

  -   - [多板箱魨](../Page/多板箱魨.md "wikilink")(*Polyplacapros tyleri*)

### 糙身箱魨屬(*Rhinesomus*)

  - [三棱糙身箱魨](../Page/三棱糙身箱魨.md "wikilink")(*Rhinesomus triqueter*)

### 尖鼻箱魨屬(*Rhynchostracion*)

  - [尖鼻箱魨](../Page/尖鼻箱魨.md "wikilink")(*Rhynchostracion nasus*)

### 三棱箱魨屬(*Tetrosomus*)

  - [雙峰三稜箱魨](../Page/雙峰三稜箱魨.md "wikilink")(*Tetrosomus
    concatenatus*)：又稱雙峰真三棱箱魨。
  - [駝背三棱箱魨](../Page/駝背三棱箱魨.md "wikilink")(*Tetrosomus
    gibbosus*)：又稱駝背真三稜箱魨。
  - [賴普三棱箱魨](../Page/賴普三棱箱魨.md "wikilink")(*Tetrosomus
    reipublicae*)：又稱賴普真三棱箱魨。
  - [北美三棱箱魨](../Page/北美三棱箱魨.md "wikilink")(*Tetrosomus
    stellifer*)：又稱北美真三棱箱魨。

[\*](../Category/箱魨科.md "wikilink")

1.