**節尾猴**（学名 *Callimico
goeldii*），为[卷尾猴科](../Page/卷尾猴科.md "wikilink")[節尾猴屬下唯一一种](../Page/節尾猴屬.md "wikilink")，是一种产于[南美](../Page/南美.md "wikilink")[亚马逊河上游地区的小型猴](../Page/亚马逊河.md "wikilink")，主要分布于[玻利维亚](../Page/玻利维亚.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[哥伦比亚](../Page/哥伦比亚.md "wikilink")、[厄瓜多尔和](../Page/厄瓜多尔.md "wikilink")[秘鲁](../Page/秘鲁.md "wikilink")。

節尾猴体色为黑色或黑褐色。体长约22厘米，尾长25-30厘米。

節尾猴主要在茂密的矮灌木地区觅食，湿季以水果、昆虫、蜘蛛、蜥蜴、蛙和蛇为食；干季则以蘑菇为食。一般六只左右组成一个群体，通过叫声进行联系。

## 参考

<references/>

[Category:卷尾猴科](../Category/卷尾猴科.md "wikilink")