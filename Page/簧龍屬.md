**簧龍屬**（[學名](../Page/學名.md "wikilink")：*Calamosaurus*）是[虛骨龍類的一個](../Page/虛骨龍類.md "wikilink")[屬小型](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，化石發現於[英國](../Page/英國.md "wikilink")[懷特島郡的](../Page/懷特島郡.md "wikilink")[威塞克斯組](../Page/威塞克斯組.md "wikilink")，地質年代屬於[下白堊紀的](../Page/下白堊紀.md "wikilink")[巴列姆階](../Page/巴列姆階.md "wikilink")。牠的[化石是兩節](../Page/化石.md "wikilink")[頸椎](../Page/頸椎.md "wikilink")（編號BMNH
R901），是由[威廉·達爾文·福克斯](../Page/威廉·達爾文·福克斯.md "wikilink")（William Darwin
Fox）所發掘的。

## 歷史及分類

在1889年，[理查德·莱德克](../Page/理查德·莱德克.md "wikilink")（Richard
Lydekker）在將[福克斯發現的化石重新歸類](../Page/威廉·達爾文·福克斯.md "wikilink")、編號時，發現這些骨頭與[虛骨龍的相似性](../Page/虛骨龍.md "wikilink")，並命名為[福氏簧椎龍](../Page/簧椎龍.md "wikilink")（*Calamospondylus
foxi*）\[1\]。但是，福克斯在1866年已使用了簧椎龍這個名字\[2\]。里德克於是在1891年將之更名為[福氏簧龍](../Page/簧龍.md "wikilink")（*Calamosaurus
foxi*）\[3\]。同時，里德克將一條[脛骨](../Page/脛骨.md "wikilink")（編號BMNH
R186）暫時性地編入於此屬，而這條脛骨可能是屬於[虛骨龍類的基底物種](../Page/虛骨龍類.md "wikilink")，類似[美頜龍科](../Page/美頜龍科.md "wikilink")\[4\]。

由於簧龍的化石稀少，簧龍受到的關注很少。雖然簧龍與簧椎龍沒有任何可以比較的化石部份\[5\]，但牠在分類上仍經常被認為是簧椎龍的[異名](../Page/異名.md "wikilink")，造成分類上的困難\[6\]\[7\]\[8\]。目前簧龍被認為有可能是虛骨龍類恐龍\[9\]，但也有研究認為簧龍是[獸腳亞目的一個](../Page/獸腳亞目.md "wikilink")[疑名](../Page/疑名.md "wikilink")\[10\]\[11\]。

## 古生物學

簧龍是種基礎需骨龍類恐龍，可能是小型、敏捷的雙足[肉食性](../Page/肉食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。簧龍的身長約為3-5米長，根據[頸椎的大小](../Page/頸椎.md "wikilink")，可推測其頭部很小\[12\]。

## 參考資料

  - Naish, D. 2011. Theropod dinosaurs. In Batten, D. J. (ed.) English
    Wealden Fossils. The Palaeontological Association (London),
    pp. 526–559.

[Category:虛骨龍類](../Category/虛骨龍類.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.  Lydekker. R. (1889). On a coelurid dinosaur from the Wealden.
    *Geological Magazine* **6**:119-121.

2.  Fox, W.D. in Anonymous. (1866) Another Wealden reptile. *Athenaeum*
    **2014**:740.

3.  Lydekker. R. (1891). On certain ornithosaurian and dinosaurian
    remains. *Quartely Journal of the Geological Society of London*
    **47**:41-44.

4.  Naish, D., Hutt, S., and Martill, D.M. (2001). Saurischian dinosaurs
    2: theropods. In: Martill, D.M., and Naish, D. (eds.). *Dinosaurs of
    the Isle of Wight.* The Palaeontological Association:London,
    242-309. ISBN 0-901707-72-2

5.
6.  Swinton, W.E. (1936). The dinosaurs of the Isle of Wight.
    *Proceedings of the Geologists' Association* **47**(3):204-220.

7.  Romer, A.S. (1956). *Osteology of the Reptiles.* University of
    Chicago Press:Chicago, 1-772. ISBN 0-89464985-X

8.  Steel, R. (1970). Part 14. Saurischia. *Handbuch der
    Paläoherpetologie/Encyclopedia of Paleoherpetology.* Part 14.
    Gustav Fischer Verlag:Stuttgart, 1-87.

9.
10. Norman, D.B. (1990). Problematic theropoda: "coelurosaurs". In:
    Weishampel, D.B., Dodson, P., and Osmólska, H. (eds.). *The
    Dinosauria*. University of California Press:Berkeley, 280-305. ISBN
    0-520-06727-4.

11. Holtz Jr., T.R., Molnar, R.E., and Currie, P.J. (2004). Basal
    Tetanurae. In: Weishampel, D.B., Dodson, P., and Osmólska, H.
    (eds.). *The Dinosauria* (second edition). University of California
    Press:Berkeley, 71-110. ISBN 0-520-24209-2.

12.