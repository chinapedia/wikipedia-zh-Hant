**籍贯**主要指自己[家族開始發生繁衍的主要地域](../Page/家族.md "wikilink")。[中國古代所謂籍贯通常指](../Page/中國古代.md "wikilink")[父親與](../Page/父親.md "wikilink")[祖父的長居地](../Page/祖父.md "wikilink")，大至[國或](../Page/國.md "wikilink")[省](../Page/省.md "wikilink")，小至[縣](../Page/縣.md "wikilink")[市或](../Page/市.md "wikilink")[村](../Page/村.md "wikilink")、[里等聚落均可以成为籍贯](../Page/里.md "wikilink")。

## 词源由来

[中國歷史古代](../Page/中國歷史.md "wikilink")，「籍」是指一个人的[家庭对](../Page/家庭.md "wikilink")[朝廷负担的](../Page/朝廷.md "wikilink")[徭役种类](../Page/徭役.md "wikilink")，也就是指其所从事之[职业](../Page/职业.md "wikilink")，如“盐户”（专门为[政府煮製](../Page/政府.md "wikilink")[食盐的](../Page/食盐.md "wikilink")）、“军户”（专门为[政府服](../Page/政府.md "wikilink")[兵役的](../Page/兵役.md "wikilink")）等。[南北朝诗歌](../Page/南北朝.md "wikilink")《[木兰诗](../Page/木兰诗.md "wikilink")》中[木兰家就是军户](../Page/木兰.md "wikilink")，所以“昨日见军帖，[可汗大点兵](../Page/可汗.md "wikilink")，军书十二卷，卷卷有爷名。”同一种户役的人户都编入一份册籍。

「贯」指一个人生長的所在，如“乡贯”、“里贯”。《[隋书](../Page/隋书.md "wikilink")·[经籍志](../Page/经籍志.md "wikilink")》“其无贯之人，不乐州县编户者，谓之浮浪人。”
[白居易](../Page/白居易.md "wikilink")《新丰折臂翁》诗：“翁云贯属新丰县，生逢圣代无征战。”

「籍贯」合在一起，指一个人生長的地點（贯），與徭役种类（籍）的登记文件。《[魏书](../Page/魏书.md "wikilink")·景穆十二王列传》：“太兴弟遥，
……迁[冀州](../Page/冀州.md "wikilink")[刺史](../Page/刺史.md "wikilink")。遥以诸胡先无籍贯，奸良莫辨，悉令造籍。”不是说[胡人没有生長的地點](../Page/胡人.md "wikilink")，而是並未[登記](../Page/登記.md "wikilink")。《魏书·[宦官列传](../Page/宦官.md "wikilink")》：“石荣籍贯兵伍，……”即其籍编于军队。

从[魏晋时期开始](../Page/魏晋.md "wikilink")，[政府对籍贯加强掌控](../Page/政府.md "wikilink")，以避免徭役和[赋税的流失](../Page/赋税.md "wikilink")。《[魏书](../Page/魏书.md "wikilink")·食货志》：“自昔以来，诸州户口，籍贯不实。”指出当时籍贯的漏洞。

## 各地情況

在[中華人民共和國政府治理的](../Page/中華人民共和國政府.md "wikilink")[中國大陸](../Page/中國大陸.md "wikilink")、[英國管治時期的](../Page/港英.md "wikilink")[香港](../Page/香港.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")1980年代以前的臺灣，籍貫是很多文件表格中須要填寫的欄目，如同[性别](../Page/性别.md "wikilink")、[年齡](../Page/年齡.md "wikilink")、[學歷](../Page/學歷.md "wikilink")、[職業等](../Page/職業.md "wikilink")。

### 中國大陸

今日中國大陸的籍貫填寫，按照[中國共產黨](../Page/中國共產黨.md "wikilink")[中央委員會](../Page/中國共產黨中央委員會.md "wikilink")[組織部及](../Page/中共中央組織部.md "wikilink")[國家檔案局](../Page/國家檔案局.md "wikilink")1991年頒布的《幹部檔案工作條例》中規定：籍貫填寫黨員本人的祖居地（指祖父的長期居住地）在上述規定中，雖然沒有對「長期」兩字作進一步的詮釋，但總的時間概念還是明確的。舉例來說，[中國共產黨黨員甲的父親從世代居住的上海來北京工作](../Page/中國共產黨.md "wikilink")，此後甲的父親一直在京居住，那麼北京就是其父的長期居住地。甲在填寫籍貫時，他的籍貫就是祖父的長期居住地上海，而不是北京。而甲的子女再填寫籍貫時，子女的籍貫就是北京了，除此之外，行政區劃的變動也需要照新的規劃填寫。\[1\]

針對非[共產黨黨員的一般民眾](../Page/共產黨.md "wikilink")，由公安部和各级公安机关文件规范籍贯填法。公安部公通字\[1995\]91号文件《关于启用新的常住人口登记表和居民户口簿有关事项的通知》（1995年12月19日）附件三《常住人口登记表和居民户口簿填写说明》：公民的籍贯填写本人祖父的居住地（户口所在地）。城市填至区或不设区的市，农村填至县，但须冠以省、自治区、直辖市的名称或通用简称。
弃婴，如果籍贯不详，应将收养人籍贯或收养机构所在地作为其籍贯。
外国人经批准加入中华人民共和国国籍的，填写其入籍前所在国家的名称。祖父去世的，填写祖父去世时的户口所在地；祖父未落常住户口的，填写祖父应落常住户口的的地方；公民登记籍贯后，祖父又迁移户口的，该公民的籍贯不再随之更改。

### 香港

在[香港](../Page/香港.md "wikilink")，自從1997年[香港主權移交中華人民共和國之後](../Page/香港主權移交.md "wikilink")，網上學校行政及管理系統已不再需要學生填寫籍貫，而只需要學生填寫出生地。如果需要填寫籍貫，但不確定時，可填寫祖父的出生地。

### 中華民國

1931年[中華民國的](../Page/中華民國.md "wikilink")《戶籍法》中有對本籍登記的規定\[2\]；臺灣、澎湖自[日治時代起將原有的漢人籍貫分類體系納入公文檔案](../Page/日治時代.md "wikilink")\[3\]。1992年[中華民國政府為解決](../Page/中華民國政府.md "wikilink")[省籍情結與混淆問題](../Page/省籍情結.md "wikilink")，修訂《戶籍法》之中的本籍登記改為出生地登記。\[4\]

### 日本

## 類似概念

**籍貫**与[出生地](../Page/出生地.md "wikilink")、[故乡](../Page/故乡.md "wikilink")、[祖籍](../Page/祖籍.md "wikilink")、[戶籍的意義不尽相同](../Page/戶籍.md "wikilink")。

祖籍通常是追認極遙遠的[祖先生長地](../Page/祖先.md "wikilink")，籍貫則是[父親與](../Page/父親.md "wikilink")[祖父的長居之地](../Page/祖父.md "wikilink")，戶籍則是其本人現在登記於[政府的居住地](../Page/政府.md "wikilink")[地址](../Page/地址.md "wikilink")。

如逝於[中南海的](../Page/中南海.md "wikilink")[毛澤東](../Page/毛澤東.md "wikilink")，由於遠祖**毛太華**避戰亂，從[江西](../Page/江西.md "wikilink")[吉水遷徙到](../Page/吉水.md "wikilink")[湖南](../Page/湖南.md "wikilink")[韶山](../Page/韶山.md "wikilink")\[5\]，故祖籍乃[江西](../Page/江西.md "wikilink")[吉水](../Page/吉水.md "wikilink")，籍貫是[湖南](../Page/湖南.md "wikilink")[韶山](../Page/韶山.md "wikilink")，戶籍為[北京](../Page/北京.md "wikilink")[西城](../Page/西城.md "wikilink")。

另外[明朝抗](../Page/明朝.md "wikilink")[倭名將](../Page/倭.md "wikilink")[俞大猷](../Page/俞大猷.md "wikilink")，因先祖**俞敏**跟從[明太祖征戰四方](../Page/明太祖.md "wikilink")，以開國功臣襲[泉州衛百戶官](../Page/泉州衛.md "wikilink")，便從家鄉[直隸](../Page/南直隶.md "wikilink")（南直隸）[霍丘落戶當地](../Page/霍丘.md "wikilink")，故祖籍是[直隸](../Page/南直隶.md "wikilink")[霍丘](../Page/霍丘.md "wikilink")，籍貫為[福建](../Page/福建.md "wikilink")[泉州](../Page/泉州.md "wikilink")。\[6\]

## 参考文献

## 参见

  - [祖籍](../Page/祖籍.md "wikilink")
  - [户籍](../Page/户籍.md "wikilink")
  - [郡望](../Page/郡望.md "wikilink")

[category:社會制度](../Page/category:社會制度.md "wikilink")

1.
2.  《戶籍法》，中華民國20年12月12日
3.  [日據時期戶籍檔案之建立與應用](https://www.archives.gov.tw/Download_File.ashx?id=5728)，[國家發展委員會檔案管理局](../Page/國家發展委員會檔案管理局.md "wikilink")
4.  [國民身分證沿革：國民身分證樣本展出文件。](http://files.dahr.taipei/ct.asp?xItem=971761&ctNode=21683&mp=100044)，臺北市大安區戶政事務所檔案管理知識網
5.  [千年古磚印證毛澤東祖籍在江西](http://news.sohu.com/19/53/news201115319.shtml)，中新社2002年5月27日
6.  《都督俞公生祠记》
    载“原籍直隶凤阳霍丘县人，世泉州卫前所百户，以魁武科授正千户，累迁都督同知，虚江其别号云。岁嘉靖甲子冬十月之吉，赐进士出身、南京户部、山东清吏司主事、同安南洲[许廷用撰](../Page/许廷用.md "wikilink")。”