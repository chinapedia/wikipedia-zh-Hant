**米勒古蜥屬**（屬名：*Milleretta*）是種小型、快速移動的[爬行動物](../Page/爬行動物.md "wikilink")，屬於[米勒古蜥科](../Page/米勒古蜥科.md "wikilink")，生存於晚[二疊紀](../Page/二疊紀.md "wikilink")[長興階的](../Page/長興階.md "wikilink")[南非](../Page/南非.md "wikilink")，約2億5000萬年前。米勒古蜥的化石是被發現於[巴爾福組](../Page/巴爾福組.md "wikilink")（Balfour
Formation）地層\[1\]。

米勒古蜥的身長約60公分，外表類似[蜥蜴](../Page/蜥蜴.md "wikilink")。米勒古蜥通常被認為是[食蟲性動物](../Page/食蟲性.md "wikilink")\[2\]，而且牠們可能有準確的聽力。米勒古蜥屬只有發現一個種。米勒古蜥曾經被認為是[雙孔亞綱的祖先](../Page/雙孔亞綱.md "wikilink")，但現在被認為較接近副爬行動物\[3\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [The DINOSAUR ENCYCLOPAEDIA on
    Millaretta.](https://web.archive.org/web/20070927223537/http://www.dinoruss.org/de_4/5c5d95a.htm)
  - [另一個米勒古蜥重建圖(德文)](https://web.archive.org/web/20070703131951/http://leute.server.de/frankmuster/M/Milleretta.htm)
  - [頭顱骨照片](https://web.archive.org/web/20080516085552/http://www.gps.caltech.edu/~rkopp/photos/SAfrica2002/Pages/154.html)

[Category:無孔亞綱](../Category/無孔亞綱.md "wikilink")
[Category:二疊紀爬行動物](../Category/二疊紀爬行動物.md "wikilink")

1.

2.

3.