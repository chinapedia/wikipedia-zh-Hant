[thumb](../Page/圖像:Near_East_1400_BCE.png.md "wikilink")
**米坦尼**（）是[美索不达米亚的古老國家](../Page/美索不达米亚.md "wikilink")，由[胡里特人](../Page/胡里特人.md "wikilink")（Hurrians）在大約前1500年所建立，領土範圍在現在的[美索不達米亞](../Page/美索不達米亞.md "wikilink")，於前14世紀國力達到頂峰，當時佔有目前的[土耳其東南部](../Page/土耳其.md "wikilink")、[敘利亞北部與](../Page/敘利亞.md "wikilink")[伊拉克北部](../Page/伊拉克.md "wikilink")，首都[瓦舒戛尼](../Page/瓦舒戛尼.md "wikilink")（Washukanni）的位置則尚未確定。在[亞述的國王](../Page/亞述.md "wikilink")[亞述尼拉里五世時期](../Page/亞述尼拉里五世.md "wikilink")，米坦尼被亞述人所攻陷，成為亞述王國的一部份。

## 历史

米坦尼王国大约於公元前1550年－前1350年存在，前後約200年。与埃及新王国时期的[第十八王朝](../Page/第十八王朝.md "wikilink")（約公元前1550年－1295年）相对应。

[Mitanni_map.png](https://zh.wikipedia.org/wiki/File:Mitanni_map.png "fig:Mitanni_map.png")

## 米坦尼王表

| 統治者                                                   |                                           |
| ----------------------------------------------------- | ----------------------------------------- |
| [舒塔尔那一世](../Page/舒塔尔那一世.md "wikilink")（Shuttarna I）   | [基尔塔](../Page/基尔塔.md "wikilink")（Kirta）之子 |
| [帕拉塔尔那](../Page/帕拉塔尔那.md "wikilink")（Parattarna）      |                                           |
| [萨乌什塔塔](../Page/萨乌什塔塔.md "wikilink")（Saushtatar）      | [帕尔萨塔塔之子](../Page/帕尔萨塔塔.md "wikilink")    |
| [阿尔塔塔玛](../Page/阿尔塔塔玛.md "wikilink")（Artatama）        |                                           |
| [舒塔尔那二世](../Page/舒塔尔那二世.md "wikilink")（Shuttarna II）  |                                           |
| [阿尔塔舒玛拉](../Page/阿尔塔舒玛拉.md "wikilink")（Artashumara）   |                                           |
| [图什拉塔](../Page/图什拉塔.md "wikilink")（Tushratta）         |                                           |
| [阿尔塔塔玛二世](../Page/阿尔塔塔玛二世.md "wikilink")（Artatama II） |                                           |
| [沙提瓦扎](../Page/沙提瓦扎.md "wikilink")（Shattiwaza）        |                                           |
| [舒塔尔那三世](../Page/舒塔尔那三世.md "wikilink")（Shuttarna III） |                                           |
| [沙图瓦拉](../Page/沙图瓦拉.md "wikilink")（Shattuara）         |                                           |
| [瓦萨沙塔](../Page/瓦萨沙塔.md "wikilink")（Wasashatta）        |                                           |
| [沙图瓦拉二世](../Page/沙图瓦拉二世.md "wikilink")（Shattuara II）  |                                           |

\[1\]

## 参见

  - [美索不达米亚](../Page/美索不达米亚.md "wikilink")

## 注释

## 参考书籍

  - <cite id="CITEREF中华世纪坛世界艺术馆2007"></cite>

## 外部連結

  - [Mitanni](http://www.livius.org/mi-mn/mitanni/mitanni.html)
  - [古代埃及文明与西亚文明的碰撞与融合](https://web.archive.org/web/20160617204623/http://sohac.nenu.edu.cn/kyss/asia/bbs/baogao/6.htm)

[Category:两河流域](../Category/两河流域.md "wikilink")
[Category:已不存在的亞洲國家](../Category/已不存在的亞洲國家.md "wikilink")
[Category:中东历史](../Category/中东历史.md "wikilink")
[Category:已不存在的帝國](../Category/已不存在的帝國.md "wikilink")
[Category:已不存在的國家](../Category/已不存在的國家.md "wikilink")

1.