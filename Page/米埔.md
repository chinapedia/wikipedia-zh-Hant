**米埔內-{后}-海灣拉姆薩爾國際重要濕地**（），在當地稱為**米埔自然護理區**（****），是一片位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗區米埔一帶的](../Page/元朗區.md "wikilink")[濕地](../Page/濕地.md "wikilink")。米埔以[候鳥](../Page/候鳥.md "wikilink")、自然護理區和[紅樹林而聞名](../Page/紅樹林.md "wikilink")。於冬季，濕地有不少來自[中國北部及](../Page/中國北部.md "wikilink")[西伯利亞的候鳥在此過冬](../Page/西伯利亞.md "wikilink")。

護理區範圍包括[大榔基](../Page/大榔基.md "wikilink")、[石山和](../Page/石山.md "wikilink")[尖鼻咀一帶的濕地](../Page/尖鼻咀.md "wikilink")，當地亦是[深圳河](../Page/深圳河.md "wikilink")、[山貝河及](../Page/山貝河.md "wikilink")[天水圍渠的](../Page/天水圍渠.md "wikilink")[河口](../Page/河口.md "wikilink")，佔地約1,540[公頃](../Page/公頃.md "wikilink")\[1\]。米埔自然護理區本身是一個禁區，[邊界道路以外更屬於](../Page/邊界道路.md "wikilink")[邊境禁區](../Page/香港邊境禁區.md "wikilink")，該地可說是雙重禁區\[2\]。

米埔濕地被紀錄於《[國際重要濕地名錄](../Page/國際重要濕地名錄.md "wikilink")》中。

## 歷史

[Mai_Po_Marshes_Plaque.jpg](https://zh.wikipedia.org/wiki/File:Mai_Po_Marshes_Plaque.jpg "fig:Mai_Po_Marshes_Plaque.jpg")
[Mai_Po_Marshes_Gei_wai_view1_2016.jpg](https://zh.wikipedia.org/wiki/File:Mai_Po_Marshes_Gei_wai_view1_2016.jpg "fig:Mai_Po_Marshes_Gei_wai_view1_2016.jpg")
[Mai_Po_Marshes_Gei_wai_boardwalk_2016.jpg](https://zh.wikipedia.org/wiki/File:Mai_Po_Marshes_Gei_wai_boardwalk_2016.jpg "fig:Mai_Po_Marshes_Gei_wai_boardwalk_2016.jpg")
米埔一帶從前是一個養殖[蝦隻的地方](../Page/蝦.md "wikilink")，在濕地中使用[基圍進行養殖](../Page/基圍.md "wikilink")。1950年[韓戰爆發](../Page/韓戰.md "wikilink")，當時的[香港政府將香港跟中國大陸接壤的土地劃為](../Page/香港殖民地時期#香港政府.md "wikilink")[邊境禁區](../Page/香港邊境禁區.md "wikilink")，當中包括米埔濕地。這限制了其發展之餘，米埔濕地的自然生態環境一直都並無得到任何形式的保護。直到[英國於](../Page/英國.md "wikilink")1976年加入《[濕地公約](../Page/濕地公約.md "wikilink")》（拉姆薩爾公約），其條款於1979年延伸至當時為[殖民地的](../Page/殖民地.md "wikilink")[香港](../Page/香港英治時期.md "wikilink")，同時香港政府將米埔濕地列為「[具特殊科學價值地點](../Page/具特殊科學價值地點.md "wikilink")」，予以保護。1984年香港政府將米埔濕地交予[世界自然基金香港分會管理](../Page/世界自然基金会.md "wikilink")，並藉此推行環境保護及教育工作。1995年，米埔濕地正式根據國際濕地公約列為國際重要濕地。

后海灣濕地位於香港西北，是生物多樣性最豐富之一的亞洲濕地。濕地佔地2,700公頃，為多種物種提供棲身之所，更是遷徙性候鳥的天堂。

世界自然基金會香港分會自1983年管理米埔自然保護區（佔后海灣380公頃土地），一直在保護區內進行研究和增設教育設施，同時推出多項保育工作維持濕地。保護區極具生物多樣性，擁有豐富的動植物種，遍佈6種濕地，包括魚塘、基圍、潮間帶泥灘、紅樹林、蘆葦叢及淡水池塘。世界自然基金會將竭盡所能管理米埔濕地，保護這片具重要生態價值的地方。

1990年代開始，由於[深圳](../Page/深圳.md "wikilink")[-{后}-海灣沿岸的工業發展](../Page/后海灣.md "wikilink")，加上深方在對岸（今後海企業總部基地及[深圳灣口岸](../Page/深圳灣口岸.md "wikilink")）進行填海工程，米埔濕地的生態或會受到損害。不過到了1990年代後期，由於[羅湖區的地產物業極之興旺](../Page/羅湖區.md "wikilink")，發展商家亦意識到毗鄰米埔濕地的[深圳灣濕地所提供的大自然景觀有助提升物業價值](../Page/深圳灣濕地.md "wikilink")，因此都積極保護當地的生態。

1995年9月4日，內后海灣1,500公頃的土地獲《拉姆薩爾公約》劃為「國際重要濕地」。

劃為[拉姆薩爾濕地](../Page/拉姆薩爾濕地.md "wikilink")，讓香港尚存最大面積的濕地得以保存。根據拉姆薩爾濕地劃分的管理區，米埔自然保護區屬生物多樣性管理區，為區內的野生生物帶來莫大裨益，同時在教育、休閒及保育等活動方面，為本地社群作出貢獻。

## 水文

米埔濕地是元朗盆地內一個天然淺水河口濕地，平均水深約2.9米，潮差中位值為1.4米。內后海灣是香港與深圳水流和沉積物的匯流處，潮間帶泥灘由沉積物沖積而成，土質主要為幼細淤泥\[3\]。

香港屬[亞熱帶季風氣候](../Page/亞熱帶季風氣候.md "wikilink")，每年均受季候風影響，降雨主要集中在4月至9月。潮間帶水域的鹽度隨著季節變化，趨勢十分明顯。在晚春至夏季期間，由於雨水增加，水的鹽度會大幅下降，有時甚至幾乎變成淡水，夏季將結束時，鹽度則會回升，到了秋冬/初春乾燥季節則達到全年最高水平。

這片濕地有助紓減新界西北部的水浸問題，而紅樹林則有穩定后海灣海岸的價值。

## 特色

[Mai_Po_Nature_Reserve_Gei_wai_pano_201611.jpg](https://zh.wikipedia.org/wiki/File:Mai_Po_Nature_Reserve_Gei_wai_pano_201611.jpg "fig:Mai_Po_Nature_Reserve_Gei_wai_pano_201611.jpg")全景\]\]

米埔濕地每年有超過60,000隻的水鳥過冬，候鳥品種包括[勺咀鷸](../Page/勺咀鷸.md "wikilink")、[小青腳鷸](../Page/小青腳鷸.md "wikilink")、[半蹼鷸](../Page/半蹼鷸.md "wikilink")、[灰尾鷸](../Page/灰尾鷸.md "wikilink")、[黑嘴鷗及](../Page/黑嘴鷗.md "wikilink")[黑臉琵鷺](../Page/黑臉琵鷺.md "wikilink")。其中米埔濕地錄得黑臉琵鷺的數目更佔全球總數的25%。這些候鳥主要在[春季和](../Page/春季.md "wikilink")[秋季出現](../Page/秋季.md "wikilink")，有達30,000隻禽鳥同一時間棲身於米埔濕地的泥灘，以作長途遷徙旅程的中途歇息之處。

此外，米埔濕地也有許多野生生物在此定居，包括超過400種[昆蟲](../Page/昆蟲.md "wikilink")、300種[雀鳥](../Page/雀鳥.md "wikilink")、18种[哺乳动物](../Page/哺乳动物.md "wikilink")、21种[爬行动物](../Page/爬行动物.md "wikilink")、90種[海洋](../Page/海洋.md "wikilink")[無脊椎動物及](../Page/無脊椎動物.md "wikilink")50種[蝴蝶](../Page/蝴蝶.md "wikilink")。而米埔濕地的潮間帶[紅樹林適應當地海岸生長](../Page/紅樹林.md "wikilink")，並提供落葉給水生動物維生，而面積更為廣東地區第1、[中國第](../Page/中國.md "wikilink")6。

[世界自然基金會香港分會在](../Page/世界自然基金會.md "wikilink")2015年5月至2016年12月，於米埔及[后海灣](../Page/后海灣.md "wikilink")[拉姆薩爾濕地內進行](../Page/拉姆薩爾濕地.md "wikilink")[生物多樣性普查](../Page/生物多樣性普查.md "wikilink")，發現逾2,050種生物，包括416種[雀鳥](../Page/雀鳥.md "wikilink")、33種[哺乳類](../Page/哺乳類.md "wikilink")、8種[兩棲類動物](../Page/兩棲類動物.md "wikilink")、22種[爬行類動物](../Page/爬行類動物.md "wikilink")、54種魚、11種蝦
、40種蟹
、逾100種[蜘蛛](../Page/蜘蛛.md "wikilink")、155種[蜂類](../Page/蜂.md "wikilink")、51種[蜻蜓及](../Page/蜻蜓.md "wikilink")[豆娘](../Page/豆娘.md "wikilink")、316種[蛾](../Page/蛾.md "wikilink")、逾15種[蟻](../Page/蟻.md "wikilink")、逾400種[鞘翅目](../Page/鞘翅目.md "wikilink")([甲蟲](../Page/甲蟲.md "wikilink"))以及323種植物。當中更發現一種懷疑是全球新品種的[異足蛛屬](../Page/異足蛛屬.md "wikilink")[蜘蛛](../Page/蜘蛛.md "wikilink")，有待專家進一步核實\[4\]\[5\]\[6\]\[7\]\[8\]。

而米埔濕地的中心地區則保留了24個傳統基圍蝦塘，位於潮間區的淺水地帶。基圍蝦塘現時仍維持營運，成為[華南地區僅存的同類作業](../Page/華南地區.md "wikilink")。每年4月至10月是基圍蝦塘的作業期，蝦苗來自由后海灣沖入基圍的小蝦，以蝦塘內的[浮游生物維生](../Page/浮游生物.md "wikilink")。到了每年11月至3月期間，基圍蝦塘會被輪流放乾，露出水面的泥濘及大量魚類，成為各種以捕食魚類為生的鳥類（包括[蒼鷺](../Page/蒼鷺.md "wikilink")、[白鷺和](../Page/白鷺.md "wikilink")[黑臉琵鷺](../Page/黑臉琵鷺.md "wikilink")）的覓食和棲息地點。而這種獨特的養蝦方式有利於米埔濕地的生態價值，對米埔濕地的濕地資源有[生生不息的作用](../Page/生生不息.md "wikilink")，有利於該處的[可持續發展](../Page/可持續發展.md "wikilink")，並充份利用米埔濕地的天然生產能力，讓人與濕地的各種生物和諧共處。

<File:Mai> Po Marshes Nature Reserve Entrance Gate 2016.jpg|禁區警告閘門
<File:Mai> Po Nature Reserve 02.jpg|小徑 <File:Mai> Po Marshes Gei wai
bridge 2016.jpg|基圍小橋 Mai Po Marshes Bird watching hide 2016.jpg|觀鳥屋
<File:Mai> Po Marshes ponds for farmers 2016.jpg|魚塘 Mai Po Marshes
Mangrove boardwalk bridge 2016.jpg|紅樹林 <File:MaiPo> bridge.JPG|浮橋
<File:Mai> Po Wildlife Education Centre 2016.jpg|野生生物教育中心 <File:Mai> Po
Wildlife Education Centre interior 2016.jpg|野生生物教育中心內貌
[File:MaiPo1.jpg|滿塘荷花](File:MaiPo1.jpg%7C滿塘荷花)

## 米埔觀鳥賽

米埔觀鳥賽在每年的3月舉行，是一個國際性的觀鳥賽事。參賽者為世界各地的觀鳥高手，是世界知名的頂級觀鳥賽事之一。

## 參觀方法

根據《野生動物保護條例》（第170章），米埔自然保護區是一個「限制進入或處於其內的地區」，以便對保護區內野生動植物的干擾減至最低。參觀者需要持有由漁農自然護理署署長發出的有效「進入米埔沼澤許可證」方可進入米埔自然保護區範圍。世界自然基金會香港分會可為申請人申請通行證，申請手續一般需時四星期，每張個人通行證的行政費為港幣100元\[9\]。

米埔濕地每年約有40,000參觀人次，其中包括約10,000名學生。學生參觀項目於逢星期一至星期五舉行，費用由[教育局全數資助](../Page/香港教育局.md "wikilink")，而每年的中小學校參觀活動接近400個。於逢星期六、日及公眾假期（農曆新年除外），米埔濕地則開放給公眾人士參觀，世界自然基金會香港分會並會安排自然導賞員作講解，一個三小時的導賞活動收費每位70港元（包括代辨[禁區通行證](../Page/邊境禁區通行證.md "wikilink")）\[10\]。

參觀名額是相當有限，尤其是於候鳥出沒的高峰季節。

<File:Mai> Po Marshes Reception 2016.jpg|米埔訪客中心 <File:Mai> Po Wildlife
Education Centre view 2016.jpg|遠望野生生物教育中心 <File:Outing> tour.jpg|導賞團

## 交通

米埔濕地並沒有任何[公共交通工具直達](../Page/公共交通.md "wikilink")。最接近的公共交通工具是行經[青山公路的](../Page/青山公路.md "wikilink")[九龍巴士76K線](../Page/九龍巴士76K線.md "wikilink")，來往[元朗](../Page/元朗.md "wikilink")[朗屏邨至](../Page/朗屏邨.md "wikilink")[上水](../Page/上水.md "wikilink")，但乘客於米埔村外的巴士站下車後要再沿[-{担}-竿洲路步行前往](../Page/担竿洲路.md "wikilink")。

前往米埔濕地的人士亦可於[西鐵綫](../Page/西鐵綫.md "wikilink")[元朗站](../Page/元朗站_\(西鐵綫\).md "wikilink")、[錦上路站或](../Page/錦上路站.md "wikilink")[東鐵綫](../Page/東鐵綫.md "wikilink")[上水站轉乘](../Page/上水站.md "wikilink")[的士前往](../Page/的士.md "wikilink")\[11\]。

## 區議會議席分佈

由於米埔鄉村人口不足以成為一個區議會選區，所以往往跟鄰近的[牛潭尾](../Page/牛潭尾.md "wikilink")、[新田](../Page/新田.md "wikilink")、[落馬洲鄉村範圍劃為同一個選區](../Page/落馬洲.md "wikilink")，為方便比較，以下列表主要以[青山公路米埔段為範圍](../Page/青山公路#.E7.B1.B3.E5.9F.94.E6.AE.B5.md "wikilink")，而1982至1990年，區內被劃入[元朗北郊選區](../Page/新田_\(選區\).md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/青山公路#.E7.B1.B3.E5.9F.94.E6.AE.B5.md" title="wikilink">青山公路米埔段沿線</a>（包括<a href="../Page/加州花園.md" title="wikilink">加州花園及</a><a href="../Page/加州豪園.md" title="wikilink">加州豪園</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 参考文献

## 外部連結

  - [世界自然基金香港分會](http://www.wwf.org.hk/chi/maipo/)
  - [元朗區議會](https://web.archive.org/web/20060524091925/http://www.districtcouncils.gov.hk/yl/chinese/welcome.htm)
  - [元朗區分界圖](http://www.eac.gov.hk/pdf/distco/maps/dc2003m.pdf)
  - [元朗新市鎮](https://web.archive.org/web/20050828145113/http://www.cedd.gov.hk/tc/about/achievements/regional/regi_yuenlong.htm)
  - [天水圍新市鎮](https://web.archive.org/web/20050829183006/http://www.cedd.gov.hk/tc/about/achievements/regional/regi_tinshuiwai.htm)
  - [元朗網站 Yuen Long Special](http://www.yl.com.hk)
  - [YL.hk-討論區](https://web.archive.org/web/20070429010201/http://www.yl.hk/forum/)
  - [元朗遊記 The Incredible Journey of Yuen
    Long](https://web.archive.org/web/20170630235514/http://www.go2yl.com/)
  - [元朗居民手冊2004](https://web.archive.org/web/20070712180452/http://www.yuenlonginfo.com/)

## 參見

  - [湿地](../Page/湿地.md "wikilink")
      - [湿地公约](../Page/湿地公约.md "wikilink")（拉姆薩爾公約）
      - [中国湿地](../Page/中国湿地.md "wikilink")
          - [中华人民共和国国际重要湿地列表](../Page/中华人民共和国国际重要湿地列表.md "wikilink")
  - [世界自然基金会](../Page/世界自然基金会.md "wikilink")
  - [香港生態](../Page/香港生態.md "wikilink")
  - [塱原](../Page/塱原.md "wikilink")
  - [-{后}-海灣](../Page/后海灣.md "wikilink")
  - [紅樹林](../Page/紅樹林.md "wikilink")

{{-}}

[米埔](../Category/米埔.md "wikilink")
[Category:具特殊科學價值地點](../Category/具特殊科學價值地點.md "wikilink")
[Category:香港濕地](../Category/香港濕地.md "wikilink")
[Category:香港生態](../Category/香港生態.md "wikilink")

1.

2.

3.
4.

5.

6.

7.

8.

9.

10.

11. 《[世界自然基金會網頁：如何抵達米埔自然保護區？](http://www.wwf.org.hk/chi/maipo/publicvisit/visitmap.php)
    》