**米奇·查理斯·曼托**（**Mickey Charles
Mantle**，）是纽约扬基球员，1974年被選入[名人堂](../Page/棒球名人堂.md "wikilink")。

他生涯18個球季都效力於[紐約洋基隊](../Page/紐約洋基.md "wikilink")，贏得3座[美聯](../Page/美國聯盟.md "wikilink")[MVP以及入選](../Page/美國職棒大聯盟最有價值球員獎.md "wikilink")16次明星賽。曼托參加過12次[世界大賽並擁有](../Page/世界大賽.md "wikilink")7枚世界大賽冠軍戒指。1995年曼托因肝癌去世，享年63歲。

## 大聯盟生涯

## 退休

## 家庭問題

## 榮譽

## 相關條目

  - [50全壘打俱樂部](../Page/50全壘打俱樂部.md "wikilink")
  - [500全壘打俱樂部](../Page/500全壘打俱樂部.md "wikilink")

## 外部連結

  - [7: *The Mickey Mantle Novel*](http://www.mantlebook.com/) by Peter
    Golenbock
  - [Downloadable audio interview with sportswriter Peter Golenbock
    (*Dynasty, Bats, Balls, Guidry, The Bronx Zoo*) on his controversial
    book *7: The Mickey Mantle
    Novel*](http://www.mrmedia.com/2007/03/fridays-with-mr-media-peter-golenbock7.html)
  - [Mantle
    photo](https://web.archive.org/web/20080827212605/http://www.thesportgallery.com/products/rfk.html)
    Mantle with Senator Robert Kennedy on Mickey Mantle Day, September
    18, 1965
  - [Mantle-Maris
    photo](https://web.archive.org/web/20080827160705/http://www.thesportgallery.com/products/limited2/mantle-maris.html)
    Mantle with Roger Maris in the summer of '61.
  - [Mickey
    Mantle](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=1239)
    at [Findagrave.com](../Page/Findagrave.com.md "wikilink")

[Category:美國棒球選手](../Category/美國棒球選手.md "wikilink")
[Category:紐約洋基隊球員](../Category/紐約洋基隊球員.md "wikilink")
[Category:美國職棒大聯盟金手套獎得主](../Category/美國職棒大聯盟金手套獎得主.md "wikilink")
[Category:500全壘打俱樂部](../Category/500全壘打俱樂部.md "wikilink")
[Category:美國聯盟打擊王](../Category/美國聯盟打擊王.md "wikilink")
[Category:美國聯盟全壘打王](../Category/美國聯盟全壘打王.md "wikilink")
[Category:美國聯盟打點王](../Category/美國聯盟打點王.md "wikilink")
[Category:罹患肝癌逝世者](../Category/罹患肝癌逝世者.md "wikilink")