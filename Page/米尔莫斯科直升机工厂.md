**米爾莫斯科直升机工厂**（[俄语](../Page/俄语.md "wikilink")：****）由[苏联航空工程师](../Page/苏联.md "wikilink")[米哈伊尔·米尔始建](../Page/米哈伊尔·米尔.md "wikilink")，主要生产米系列直升机。

## 米系列直升機

[SM-1_RB4.jpg](https://zh.wikipedia.org/wiki/File:SM-1_RB4.jpg "fig:SM-1_RB4.jpg")
[MIL_Mi-4_HOUND.png](https://zh.wikipedia.org/wiki/File:MIL_Mi-4_HOUND.png "fig:MIL_Mi-4_HOUND.png")
[Mi-26.jpg](https://zh.wikipedia.org/wiki/File:Mi-26.jpg "fig:Mi-26.jpg")

  - [Mi-1](../Page/Mi-1直升机.md "wikilink"), 1948年 - 輕型多用途直升機
  - [Mi-2](../Page/Mi-2直升机.md "wikilink"), 1965年 - 輕型多用途直升機
  - [Mi-3](../Page/Mi-3直升机.md "wikilink"), 1960年代 - 輕型通用直升機
  - [Mi-4](../Page/Mi-4直昇机.md "wikilink"), 1955年 - 運輸和反潜直升機
  - [V-5](../Page/V-5直升机.md "wikilink"), 1950年代後期 - 中型單渦輪軸發動機運輸直升機
  - [Mi-6](../Page/Mi-6直昇机.md "wikilink"), 1957年 - 重型運輸直升機
  - [V-7](../Page/V-7直升机.md "wikilink"), 1950年代後期 - 實驗型四座直升機
  - [Mi-8](../Page/Mi-8直升机.md "wikilink"), 1968年 - 多用途直升機
  - [Mi-10](../Page/Mi-10直升机.md "wikilink"), 1962-1963年 -
    skycrane，機輪支架可延長
  - [V-12](../Page/V-12直升机.md "wikilink"), 1960年代後期 - 實驗重型直升機
  - [Mi-14](../Page/Mi-14直昇机.md "wikilink"), 1978年 - 反潜直升機
  - [V-16](../Page/V-16直升机.md "wikilink"), 1960年代後期 - 重型貨物/運輸直升機
  - [Mi-17](../Page/Mi-17直昇机.md "wikilink"), 1974年 - 運輸直升機
  - [Mi-18](../Page/Mi-8直升机.md "wikilink"), 只有原型
  - [Mi-20](../Page/Mi-20直升机.md "wikilink"), 超輕型直升機
  - [Mi-22](../Page/Mi-22直升机.md "wikilink"), 計劃中,未建成
  - [Mi-24](../Page/Mi-24雌鹿直升机.md "wikilink"), 1978年 - 重型戰鬥直升機
  - [Mi-25](../Page/Mi-24直升机.md "wikilink"), Mi-24 的出口版
  - [Mi-26](../Page/Mi-26直升机.md "wikilink"), 世界上最大的直升機
  - [Mi-28](../Page/Mi-28攻击直升机.md "wikilink"), 1984 - 戰鬥直升機
  - [Mi-30](../Page/Mi-30直升机.md "wikilink"), 垂直起飛的飛機,計劃中,未建成
  - [Mi-32](../Page/Mi-32直升机.md "wikilink"), 超重型直升機,三轉子渦輪軸發動機,未建成
  - [Mi-34](../Page/Mi-34直升机.md "wikilink"), 1986年 - 輕型直升機
  - [Mi-35](../Page/Mi-24直升机.md "wikilink"), Mi-24的出口版
  - [Mi-36](../Page/Mi-36直升机.md "wikilink"), 只有計劃
  - [Mi-38](../Page/Mi-38直升机.md "wikilink"), 2000年 - 多用途直升機
  - [Mi-40](../Page/Mi-40直升机.md "wikilink"), 計劃中,未建成
  - [Mi-42](../Page/Mi-42直升机.md "wikilink"), 計劃中,未建成
  - [Mi-44](../Page/Mi-44直升机.md "wikilink"), 計劃中,未建成
  - [Mi-46](../Page/Mi-46直升机.md "wikilink"), 計劃中,未建成
  - [Mi-54](../Page/Mi-54直升机.md "wikilink"), 2010年 - 多用途直升機
  - [Mi-60](../Page/Mi-60直升机.md "wikilink"), 計劃中,未建成
  - [Mi-115](../Page/Mi-60直升机.md "wikilink"), 計劃中,未建成
    ([Mi-60](../Page/Mil_Mi-60.md "wikilink"))
  - [Mi-171](../Page/Mi-171直昇機.md "wikilink"), Mi-17的版本
  - [Mi-172](../Page/Mi-17直升机.md "wikilink"), Mi-17的版本
  - [Mi-234](../Page/Mi-34直升机.md "wikilink"),
    [Mi-34的版本](../Page/Mi-34.md "wikilink")
  - [Mi-X1](../Page/Mi-X1直升机.md "wikilink"), 計劃中,未建成

<center>

<File:RU035> 09.jpg|Mi-1 <File:RU036> 09.jpg|Mi-4 <File:RU037>
09.jpg|Mi-8 <File:RU038> 09.jpg|Mi-34 <File:RU039> 09.jpg|Mi-28

</center>

## 參見

  - [俄罗斯飞机制造厂列表](../Page/俄罗斯飞机制造厂列表.md "wikilink")

## 外部链接

  - [Official web
    site](https://web.archive.org/web/20031220102215/http://www.mi-helicopter.ru/)
  - <https://web.archive.org/web/20040101214137/http://www.luftfahrtmuseum.com/htmd/dth/mil.htm>
  - [The Creative Work of Mil Moscow Helicopter
    Plant](https://web.archive.org/web/20070929131747/http://avia.russian.ee/helix/mil/index.html)

[Category:俄羅斯飛機製造商](../Category/俄羅斯飛機製造商.md "wikilink")
[Category:米爾設計局](../Category/米爾設計局.md "wikilink")