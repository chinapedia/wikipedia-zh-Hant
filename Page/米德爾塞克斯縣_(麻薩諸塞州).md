**米德爾塞克斯縣**（）是[美國](../Page/美國.md "wikilink")[麻薩諸塞州東北部的一個](../Page/麻薩諸塞州.md "wikilink")[縣](../Page/郡.md "wikilink")，北鄰[-{zh-cn:新罕布什尔;zh-tw:新罕布夏;zh-hk:新罕布什爾;}-州](../Page/新罕布什爾州.md "wikilink")。面積2,195平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口1,465,396人，為全州人口最多的縣。縣治有二：[劍橋和](../Page/剑桥_\(马萨诸塞州\).md "wikilink")[羅威爾](../Page/洛厄尔_\(马萨诸塞州\).md "wikilink")（Lowell），惟自1997年起已無縣政府。

成立於1643年5月10日，是該州原始的縣之一。縣名來自[英國](../Page/英國.md "wikilink")[米德爾塞克斯郡](../Page/米德爾塞克斯郡.md "wikilink")。

[M](../Category/马萨诸塞州行政区划.md "wikilink")
[Category:马萨诸塞州米德尔塞克斯县](../Category/马萨诸塞州米德尔塞克斯县.md "wikilink")
[Category:大波士顿](../Category/大波士顿.md "wikilink")