**米拉**（[土耳其语](../Page/土耳其语.md "wikilink")：Myra）[土耳其历史上的古城](../Page/土耳其.md "wikilink")；现名[代姆雷](../Page/代姆雷.md "wikilink")。位于今土耳其[安塔利亚省境内](../Page/安塔利亚省.md "wikilink")。

[古罗马](../Page/古罗马.md "wikilink")[地理学家](../Page/地理学.md "wikilink")[斯特拉波在其著作中最早提到米拉](../Page/斯特拉波.md "wikilink")，说她是[吕基亚](../Page/吕基亚.md "wikilink")（[安纳托利亚一地区](../Page/安纳托利亚.md "wikilink")，后成为[罗马帝国的](../Page/罗马帝国.md "wikilink")[行省](../Page/罗马行省.md "wikilink")）的一个大城。该城居住的主要是[希腊移民](../Page/希腊.md "wikilink")，他们崇拜[阿耳忒弥斯](../Page/阿耳忒弥斯.md "wikilink")。当地的原住民是[吕基亚人](../Page/吕基亚人.md "wikilink")，他们约在前1世纪被[希腊人取代了](../Page/希腊人.md "wikilink")。希腊-罗马时代的遗迹大多被河流带来的泥沙冲积所掩盖，只有[卫城等少数建筑得到发掘](../Page/卫城.md "wikilink")。1840年，欧洲旅行家查尔斯·费劳斯在古城附近的峭壁上发现了两座古吕基亚人开凿的石墓。

米拉在公元初曾是小亚细亚的大城市。[使徒](../Page/使徒.md "wikilink")[圣保罗曾在该城的港口换乘船舶](../Page/保罗.md "wikilink")。[狄奥多西二世皇帝在位时期](../Page/狄奥多西二世.md "wikilink")（408年～450年），米拉成为[东罗马帝国吕基亚省的首府](../Page/东罗马帝国.md "wikilink")。809年，米拉被[阿拉伯人攻占](../Page/阿拉伯人.md "wikilink")，从此走向衰落。[阿历克塞一世皇帝](../Page/阿历克塞一世.md "wikilink")（1081年～1118年在位）曾将该城从[突厥人手中夺回](../Page/突厥人.md "wikilink")，但随着拜占庭本身的衰微，米拉终于成为[穆斯林的地盘](../Page/穆斯林.md "wikilink")。

米拉是著名的[基督教](../Page/基督教.md "wikilink")[圣人](../Page/基督教圣人.md "wikilink")[圣尼古拉担任](../Page/圣尼古拉.md "wikilink")[主教的城市](../Page/主教.md "wikilink")。当地现存的圣尼古拉教堂大约建于8世纪。1863年[俄国](../Page/俄国.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")[亚历山大二世下令重修该教堂](../Page/亚历山大二世_\(俄国\).md "wikilink")，但最终未能完工。1963年起，对该[教堂进行了考古发掘](../Page/教堂.md "wikilink")。

## 外部链接

  - [Notes on Myra at Turkish Ministry of Culture
    site](https://web.archive.org/web/20060501090840/http://www.kultur.gov.tr/portal/arkeoloji_en.asp?belgeno=1899)
  - [Virtual tour of the ancient
    city](http://www.apeskov.ru/images/stories/Lycia/demre.html)
  - [Tripod site about
    Myra](http://ancientneareast.tripod.com/Myra.html)
  - [Sites in
    Myra](https://web.archive.org/web/20040202135906/http://www.lycianturkey.com/lycian_sites/myra.htm)
  - [Myra Guide and Photo Album](http://www.anatolia.luwo.be/myra.htm)

[Category:土耳其地理](../Category/土耳其地理.md "wikilink")
[Category:土耳其历史](../Category/土耳其历史.md "wikilink")