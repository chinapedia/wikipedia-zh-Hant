**米歇尔·弗朗索瓦·普拉蒂尼**（），），生于[法国](../Page/法国.md "wikilink")[洛林](../Page/洛林.md "wikilink")，昵称“足球场上的拿破仑大帝”、已退役的法国足球运动员，世界足球史上最佳法国传奇巨星之一，与[齐达内并列为法国历史上最佳足球巨星](../Page/cmn:齐内丁·齐达内.md "wikilink")。

2004年，他位列球王[比利評選的](../Page/比利.md "wikilink")125位最出色的在世球星（FIFA
100）中。通常认为普拉蒂尼是20世紀80年代最出色的[中場球員](../Page/中場.md "wikilink")，曾是[歐洲足協主席](../Page/歐洲足協.md "wikilink")\[1\]及[法國足球總會副會長](../Page/法國足球總會.md "wikilink")。

## 足球生涯

柏天尼早在七十年代展開球員生涯，曾效力[法國甲組聯賽球會](../Page/法國甲組聯賽.md "wikilink")[南錫和](../Page/南锡足球俱乐部.md "wikilink")[聖伊天](../Page/圣艾蒂安足球俱乐部.md "wikilink")。1984年協助[法國國家隊奪得](../Page/法國國家足球隊.md "wikilink")[歐洲國家盃冠軍](../Page/歐洲國家盃.md "wikilink")，三年間三度當選[歐洲足球先生](../Page/歐洲足球先生.md "wikilink")。八十年代的柏天尼，與[居里斯](../Page/阿蘭·吉雷瑟.md "wikilink")、[泰簡拿和](../Page/讓·蒂加納.md "wikilink")[費南迪斯組成的黃金中場](../Page/路易斯·費爾南德斯.md "wikilink")，一度威震全球，1986年世界杯與[巴西的八強比賽](../Page/巴西國家足球隊.md "wikilink")，成為世界杯史上最經典的比賽之一，該場比賽法國勝出，再一次打入四強。

## 退役後

退役後的柏天尼曾擔任過國家隊主帥一職，在1992年歐洲國家杯僅能在首圈畢業。但柏天尼一直都是法國足球總會副會長，在1985年獲得[法國榮譽軍團勳章的騎士勳章](../Page/法國榮譽軍團勳章.md "wikilink")，1988年再贏得軍官勳章。[2006年世界杯更為國際足協出任技術小組主席](../Page/2006年世界杯.md "wikilink")，可見地位超然。

柏天尼在位歐洲足協主席期間，多次被指針對[英格蘭](../Page/英格蘭足球代表隊.md "wikilink")。如[2012–13年歐洲冠軍聯賽](../Page/2012–13年歐洲冠軍聯賽.md "wikilink")16強第二回合[曼聯對](../Page/曼聯.md "wikilink")[皇家馬德里](../Page/皇家馬德里.md "wikilink")，當時為比賽第56分鐘，[蘭尼將對手剷跌](../Page/蘭尼.md "wikilink")。主球證卡基亞毫不猶豫地將蘭尼直接[紅牌趕出場](../Page/紅牌.md "wikilink")，但慢鏡顯示蘭尼有明顯的收腳動作，而且蘭尼只是向球的方向剷去。連一向喜歡揶揄對手的皇馬主帥[摩連奴都覺得蘭尼絕對不應該拿紅牌](../Page/摩連奴.md "wikilink")，更直斥球證破壞了整場比賽。但柏天尼之後卻力撐球證卡基亞，認爲其沒錯，並直言蘭尼的紅牌是咎由自取。之後在2013年尾的[世界盃抽籤](../Page/2014年世界盃足球賽.md "wikilink")，因爲法國是歐洲球隊進軍世界盃中排名最低，而法國身處的歐洲檔次則有9支球隊（每個檔次必須有8支球隊），因此法國理應被調去只得7支球隊的檔次。可能預知法國會進入死亡之組，所以盛傳柏天尼從中作梗，要求國際足協不要將法國調入其他檔次。結果英格蘭被編入死亡之組（對手有烏拉圭、意大利及哥斯達黎加），而[法國則被編入相對輕鬆的小組](../Page/法國國家足球隊.md "wikilink")（對手有瑞士、厄瓜多爾及洪都拉斯）。雖然英格蘭被編入死亡之組與柏天尼沒有直接關係，但大部分英格蘭球迷均歸咎於柏天尼從後搞小動作致使法國能夠逃出生天而不被編入死亡之組。結果一如外界所料，英格蘭分組墊底出局，法國分組首名出線。而事件的另一受害者[意大利也在分組賽最後一場輸給烏拉圭後出局](../Page/意大利國家足球隊.md "wikilink")。

另一方面，他也被指有多次偏幫自己國家聯賽球隊的嫌疑，最著名例子為2011年的歐聯[里昂對](../Page/奥林匹克里昂.md "wikilink")[薩格勒布戴拿模的比賽](../Page/薩格勒布戴拿模.md "wikilink")，里昂以7:1大勝對手，當時有記者拍到有里昂球員鬼鬼祟祟地拍對方球員，懷疑打假球，柏天尼承諾會徹查，但結果最後還是不了了之。

## 效力球隊

  - 法國[奎史德](../Page/US_Quiestede.md "wikilink")（1963年－1965年；學徒）
  - 法國[AS祖夫](../Page/AS_Joeuf.md "wikilink")（1966年－1972年；學徒）
  - 法國[南錫](../Page/南锡足球俱乐部.md "wikilink")（1973年－1979年）
  - 法國[-{zh-hans:圣艾蒂安;zh-hk:聖伊天;zh-tw:聖艾蒂安;}-](../Page/圣艾蒂安足球俱乐部.md "wikilink")（1979年－1982年）
  - [意大利](../Page/意大利.md "wikilink")[祖雲達斯](../Page/祖雲達斯.md "wikilink")（1982年－1987年）

## 榮譽

### 球會榮譽

  - [法國甲組聯賽](../Page/法甲.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")：聖伊天(1981)
  - [法國杯冠軍](../Page/法國杯.md "wikilink")：南錫(1978)、聖伊天(1981、1982)
  - [意大利杯冠軍](../Page/意大利杯.md "wikilink")：祖雲達斯(1983)
  - [意大利甲組聯賽神射手](../Page/意大利甲組聯賽.md "wikilink")：祖雲達斯(1983、1984、1985)
  - [歐洲聯賽冠軍杯](../Page/歐洲聯賽冠軍杯.md "wikilink")：祖雲達斯(1985)
  - [歐洲杯賽冠軍杯](../Page/歐洲杯賽冠軍杯.md "wikilink")：祖雲達斯(1984)

### 國際賽榮譽

  - : 殿軍

  - [1984年欧洲足球锦标赛](../Page/1984年欧洲足球锦标赛.md "wikilink"): 冠軍

  - : 季軍

柏天尼共代表法國國家隊上陣72場比賽，取得41個入球，並49次擔任隊長。世界杯共取得5個入球，一度是繼在世界杯射入13球的[方丹後](../Page/朱斯特·方丹.md "wikilink")，在世界杯入球最多的法國球員。今被現役射手[亨利以](../Page/蒂埃里·亨利.md "wikilink")6球打破。

## 教練生涯

  - [1992年歐洲國家盃](../Page/1992年歐洲國家盃.md "wikilink"): 首圈

## 参考資料

## 外部連結

[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:1986年世界盃足球賽球員](../Category/1986年世界盃足球賽球員.md "wikilink")
[Category:1982年世界盃足球賽球員](../Category/1982年世界盃足球賽球員.md "wikilink")
[Category:1978年世界盃足球賽球員](../Category/1978年世界盃足球賽球員.md "wikilink")
[Category:1984年歐洲國家盃球員](../Category/1984年歐洲國家盃球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")
[Category:1992年歐洲國家盃主教練](../Category/1992年歐洲國家盃主教練.md "wikilink")
[Category:法國國家足球隊主教練](../Category/法國國家足球隊主教練.md "wikilink")
[Category:南錫球員](../Category/南錫球員.md "wikilink")
[Category:聖伊天球員](../Category/聖伊天球員.md "wikilink")
[Category:祖雲達斯球員](../Category/祖雲達斯球員.md "wikilink")
[Category:法國足球主教練](../Category/法國足球主教練.md "wikilink")
[Category:歐洲足球先生](../Category/歐洲足球先生.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英格蘭足球名人堂男球員](../Category/英格蘭足球名人堂男球員.md "wikilink")
[Category:法乙球員](../Category/法乙球員.md "wikilink")
[Category:法国荣誉军团勋章持有人](../Category/法国荣誉军团勋章持有人.md "wikilink")
[Category:法国之最](../Category/法国之最.md "wikilink")
[Category:法国体育](../Category/法国体育.md "wikilink")
[Category:洛林人](../Category/洛林人.md "wikilink")
[Category:默爾特-摩澤爾省人](../Category/默爾特-摩澤爾省人.md "wikilink")
[Category:義大利裔法國人](../Category/義大利裔法國人.md "wikilink")
[Category:歐洲足球協會聯盟主席](../Category/歐洲足球協會聯盟主席.md "wikilink")
[Category:FIFA 100](../Category/FIFA_100.md "wikilink")

1.  [柏天尼獲選歐洲足協主席](http://news.bbc.co.uk/sport2/hi/football/europe/6284365.stm)