**米歇爾‧魏特曼**（Michael
Wittmann）（），[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")[納粹德國活躍的](../Page/納粹德國.md "wikilink")[虎式坦克指揮官](../Page/虎式坦克.md "wikilink")。

## 生平

出生於巴伐利亞的Oberpfalz(英譯Upper
Palatinate)的Vogelthal，父親約翰‧魏特曼是一位[農民](../Page/農民.md "wikilink")。1934年10月30日入伍，成為[德國國防軍第](../Page/德國.md "wikilink")19步兵團的一員。1936年9月30日退伍並在之後的1937年4月5日加入[武裝黨衛隊](../Page/武裝黨衛隊.md "wikilink")“[阿道夫·希特勒護衛隊](../Page/阿道夫·希特勒.md "wikilink")”（Leibstandarte
SS Adolf Hitler，簡稱LSSAH）92團第一連。

1937年加入了LSSAH第17連（裝甲偵查連）。1938年夏季該部縮編，1939年9月，魏特曼作為LSSAH偵查部隊的軍官指揮SdKfz
232参加[波蘭戰役](../Page/波蘭戰役.md "wikilink")。

1944年[盟軍登陸](../Page/盟軍.md "wikilink")[法國](../Page/法國.md "wikilink")[諾曼第](../Page/諾曼第.md "wikilink")。米歇爾‧魏特曼臨危受命，指揮[虎式坦克對](../Page/虎式坦克.md "wikilink")[盟軍發動突襲](../Page/盟軍.md "wikilink")。他利用“攻击后立刻改变位置”的策略，一日之內總共击毁盟軍大小[坦克](../Page/坦克.md "wikilink")、[裝甲車](../Page/裝甲車.md "wikilink")、軍車51辆，其中坦克23辆。创下了单人一日击毁军用车辆数目最多的纪录，这项纪录至今未被打破。但现在已有证据证明魏并非始终都是单车进攻，且他本人的座车连同多辆虎式也均被击毁。

著名战例：[维莱博卡日战斗是於诺曼第之戰役后](../Page/维莱博卡日战斗.md "wikilink")，魏特曼所在部队奉命对卡昂附近盟军进行反击，其带领虎式坦克车组脱离主力部队，孤车进入维莱博卡日镇进行侦查。利用迂回战术，良好的射击技巧，击毁英军[谢尔曼](../Page/M4谢尔曼.md "wikilink")、[萤火虫](../Page/雪曼萤火虫.md "wikilink")、[克伦威尔坦克和补给车辆共](../Page/克伦威尔坦克.md "wikilink")50多辆，成为世界坦克史上的经典战例。魏特曼于8月8日死于英军的萤火虫或加拿大军的多辆谢尔曼近距射击，虽有说法称是受空袭而死，但已被NO
HOLDING BACK一书证實為伪造。

## 外部連結

  - [魏特曼之死](http://v.youku.com/v_show/id_XMjg1NTYxNDM2.html)

[Category:德國軍事人物](../Category/德國軍事人物.md "wikilink")
[Category:德國第二次世界大戰人物](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:武装党卫军军官](../Category/武装党卫军军官.md "wikilink")