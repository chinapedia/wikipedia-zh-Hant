[米篩目.JPG](https://zh.wikipedia.org/wiki/File:米篩目.JPG "fig:米篩目.JPG")米篩目\]\]

**米篩目**，[台灣話](../Page/台灣話.md "wikilink"):
Bí-thai-ba̍k，[客家話](../Page/客家話.md "wikilink"):Mi-chi-mok
又作-{zh:**銀針粉**、**老鼠粄**或称**老鼠粉**;zh-hans:**银针粉**、**老鼠粄**或称**老鼠粉**;zh-cn:**银针粉**或称**老鼠粉**;zh-hk:**老鼠粄**或**老鼠粉**;zh-sg:**老鼠粄**或**银针粉**;zh-tw:**老鼠粄**、**老鼠粉**或**銀針粉**;}-，常被誤稱為**米苔目**，[客家傳統米製麵條](../Page/客家.md "wikilink")，一般認為起源於廣東梅州，常見於[福建](../Page/中國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[台灣等](../Page/台灣.md "wikilink")[國家和地區](../Page/國家.md "wikilink")。

在台灣20到30年代的農業社會，凡遇稻穀成熟的季節家家戶戶安排收割日期，主事的主人便會在早晚兩次的休息時間準備各種點心，米篩目是當時所普遍食用的傳統米食\[1\]，是一種[客家](../Page/客家.md "wikilink")[粉條](../Page/粉條.md "wikilink")，主要成份為[米漿](../Page/米漿.md "wikilink")[蒸成凝塊](../Page/蒸.md "wikilink")，再以數十小孔洞[鐵器透過凝塊形成長圓條形類似麵形](../Page/鐵器.md "wikilink")，易於[消化食用](../Page/消化.md "wikilink")，老少咸宜。

## 名稱由來

米篩目源於[廣東](../Page/廣東.md "wikilink")[梅州](../Page/梅州.md "wikilink")[大埔一帶](../Page/大埔縣.md "wikilink")，因為兩端尖，形似[老鼠](../Page/老鼠.md "wikilink")，[客家人慣稱粉為](../Page/客家人.md "wikilink")「粄」，因此稱為**老鼠粄**。

後來傳至[台灣及](../Page/台灣.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")，當地[客家人稱為](../Page/客家人.md "wikilink")**米-{}-篩目**，是指製造時把粉團經過篩子般的擦板，從洞眼（目）中搓出粉條。由於[閩南語篩子音像](../Page/閩南語.md "wikilink")《米苔》，在台灣坊間又常作**米苔目**\[2\]\[3\]。

於[馬來西亞称老鼠](../Page/馬來西亞.md "wikilink")-{}-粉，也作**米台目**。

老鼠粄也傳至[香港](../Page/香港.md "wikilink")，因「老鼠」之名不雅，當地人以粉條兩端尖，狀似銀[針](../Page/針.md "wikilink")，稱為銀針-{}-粉。

## 吃法

### 傳統吃法

  - 湯粉：煮湯加肉絲、香菇絲、蝦米、[油蔥](../Page/油蔥.md "wikilink")、蔬菜即是鹹食、熱食
  - 炒粉：把各種配料與粉一起炒食

### 新式吃法

加糖水、冰塊或挫冰即是爽口甜食，流行於[台灣](../Page/台灣.md "wikilink")（即粉條）。

## 參考資料

  - [老鼠-{}-粉情結 ◎
    林金城](http://www.got1mag.com/blogs/kimcherng.php/2007/01/13/e_efncs_a_cm)

[Category:米製麵條](../Category/米製麵條.md "wikilink")
[Category:福建食品](../Category/福建食品.md "wikilink")
[Category:台灣麵條](../Category/台灣麵條.md "wikilink")
[Category:客家食品](../Category/客家食品.md "wikilink")
[Category:廣東食品](../Category/廣東食品.md "wikilink")
[Category:香港粉麵](../Category/香港粉麵.md "wikilink")
[Category:馬來西亞食品](../Category/馬來西亞食品.md "wikilink")

1.
2.
3.