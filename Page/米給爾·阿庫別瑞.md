**米給爾·阿庫別瑞**（，），墨西哥理論物理學家。

米給爾·阿庫別瑞1964年出生於[墨西哥首都](../Page/墨西哥.md "wikilink")[墨西哥市](../Page/墨西哥市.md "wikilink")，1988年和1990年先後獲得[墨西哥国立自治大学物理學](../Page/墨西哥国立自治大学.md "wikilink")[Licentiate與碩士學位](../Page/Licentiate.md "wikilink")。1990年搬到[英國](../Page/英國.md "wikilink")[威爾士](../Page/威爾士.md "wikilink")[加的夫以就讀](../Page/加的夫.md "wikilink")[卡迪夫大学的研究所](../Page/卡迪夫大学.md "wikilink")，並在1994年拿到[博士學位](../Page/博士學位.md "wikilink")。1996年他離開[威爾斯](../Page/威爾斯.md "wikilink")，来到[德國](../Page/德國.md "wikilink")[波茲坦包括有重力物理研究的](../Page/波茲坦.md "wikilink")[普朗克研究院工作](../Page/普朗克研究院.md "wikilink")。在那里，他開發了新的[數值方法來描繪](../Page/數值方法.md "wikilink")[黑洞](../Page/黑洞.md "wikilink")。自2002年起，他在[墨西哥國立自治大學](../Page/墨西哥國立自治大學.md "wikilink")（National
Autonomous University of Mexico,
UNAM）的核子科學研究所工作，在那裡他研究了[數值化相對論](../Page/數值化相對論.md "wikilink")——致力於運用[電腦來建構與解出由](../Page/電腦.md "wikilink")[阿爾伯特·愛因斯坦首先提出的物理方程式](../Page/阿爾伯特·愛因斯坦.md "wikilink")。

## 阿庫別瑞引擎

阿庫別瑞最有名的事蹟是於1994年提出以他為名的[阿庫別瑞引擎](../Page/阿庫別瑞引擎.md "wikilink")——[理論物理中的一項方案](../Page/理論物理.md "wikilink")，使得[超光速航行變得可能](../Page/超光速航行.md "wikilink")。並且不會違反[廣義相對論中](../Page/廣義相對論.md "wikilink")「沒有物體可以[局域地比光速還快](../Page/局域.md "wikilink")」。

## 著作

## 相關條目

  - [阿庫別瑞引擎](../Page/阿庫別瑞引擎.md "wikilink")
  - [曲速引擎](../Page/曲速引擎.md "wikilink")
  - [數值化相對論](../Page/數值化相對論.md "wikilink")

[Category:墨西哥物理學家](../Category/墨西哥物理學家.md "wikilink")
[Category:墨西哥城人](../Category/墨西哥城人.md "wikilink")
[Category:墨西哥國立自治大學校友](../Category/墨西哥國立自治大學校友.md "wikilink")
[Category:卡迪夫大學校友](../Category/卡迪夫大學校友.md "wikilink")