[PigsBloodCake.jpg](https://zh.wikipedia.org/wiki/File:PigsBloodCake.jpg "fig:PigsBloodCake.jpg")
**米血糕**，簡稱為**米血**，是指以[糯米和](../Page/糯米.md "wikilink")[動物血](../Page/动物血液.md "wikilink")（[豬](../Page/猪血.md "wikilink")、[鴨](../Page/鴨血.md "wikilink")、[雞](../Page/鸡血.md "wikilink")、[鵝](../Page/鹅血.md "wikilink")）所做成的一種傳統米食[糕點](../Page/糕點.md "wikilink")，發源自台灣。依製成原料的不同，多為雞、[鴨血糕與](../Page/鴨血糕.md "wikilink")[豬血糕](../Page/豬血糕.md "wikilink")\[1\]，再加上糯米的種類搭配，在口感上會有些差異。

米血糕是發源自 [台灣](../Page/台灣.md "wikilink")
的一種米食糕點。華人多以米食為主，常利用[蒸](../Page/蒸.md "wikilink")、煮方式，改變米食型態。[鴨肉滋陰補虛](../Page/鴨.md "wikilink")，昔日[農家在殺鴨之後](../Page/農家.md "wikilink")，基於節儉美德，不忍心倒掉鴨血，故以食器盛米和鴨血，蒸熟後沾醬食用。後來流傳至民間，漸成為平民小吃，又名「鴨血糕」。然而鴨肉價高，養育費時，鴨血供不應求，而[雞血卻又不易凝固](../Page/雞.md "wikilink")，於是漸以[豬血取代鴨血](../Page/豬.md "wikilink")，成為「**豬血糕**」。因此，米血糕是鴨血糕與豬血糕的原稱，但不能以米血糕名稱得知究竟是鴨血糕或是豬血糕。

## 鴨血糕

**鴨血糕**，為食材的一種，由通常用新鮮的鴨血加入[糯米](../Page/糯米.md "wikilink")、鹽及其它材料，然後蒸熟成為凝固的塊狀。常作為[火鍋或](../Page/火鍋.md "wikilink")[滷味的食材](../Page/滷味.md "wikilink")。

## 臺灣豬血糕

豬血糕又稱為米血或米血糕，具有兩種意義，一種是作為食品原料的豬血糕；另一種則是用[豬](../Page/豬.md "wikilink")[血加上](../Page/血.md "wikilink")[糯米等食材製成的小吃](../Page/糯米.md "wikilink")，名稱依照種類的不同可以是「豬血糕」、「花生米血」或「花生豬血糕」。

有的小吃攤直接用豬血糕稱呼，有的則是稱為花生豬血糕，是將蒸好的條狀豬血糕沾上醬油後，依照顧客的需求有不同的做法。如南部吃法是加甜辣醬、醬油膏、薑絲，而北部吃法是加上厚重的[花生粉及少量](../Page/花生.md "wikilink")[香菜](../Page/香菜.md "wikilink")。插上竹籤後，直接拿在手上食用，是臺灣[夜市中不可或缺的](../Page/夜市.md "wikilink")[小吃之一](../Page/小吃.md "wikilink")。

另一種選擇是[鹽酥雞攤位](../Page/鹽酥雞.md "wikilink")，豬血糕則是直接丟入油鍋炸，吃之前切塊，然後依照顧客的要求撒上椒鹽粉，辣粉，等等。有些地方的攤位是用鴨血糕當作食材，但是人們往往直接稱呼為豬血糕。

米血糕常見於路邊攤，蒸熟後多以沾醬、花生粉或[香菜對外販售](../Page/香菜.md "wikilink")，做為主食或零食均可。[秋](../Page/秋.md "wikilink")[冬期間](../Page/冬.md "wikilink")，米血糕也是熱門[火鍋料首選之一](../Page/火鍋.md "wikilink")，唯不能久煮，易黏鍋底、焦味四起。

2009年臺灣豬血糕被[英國旅游網站](../Page/英國.md "wikilink")“VirtualTourist.com”的會員和編輯評為[全球十大怪食之首](../Page/全球十大怪食.md "wikilink")，被外國游客認為比蟲子更加噁心、恐怖\[2\]。

## 製作

豬血糕通常用新鮮的豬血加入[糯米](../Page/糯米.md "wikilink")、[食鹽及其它材料](../Page/食鹽.md "wikilink")，然後蒸熟成為凝固的塊狀，口感比一般的糯米糕還硬一些。也有使用[鴨血製作的血糕](../Page/鴨血.md "wikilink")，通常較硬，比較適合烹煮。

## 作為食材

與[糯米腸一樣](../Page/糯米腸.md "wikilink")，都可以當成[米飯的替代品](../Page/米飯.md "wikilink")。可以切成小塊當成[火鍋料](../Page/火鍋.md "wikilink")，或是切成條狀後成為[滷味](../Page/滷味.md "wikilink")、[關東煮的材料](../Page/關東煮.md "wikilink")。

米血糕的料理方式很多元，包括煮、蒸、滷、炸、炒、三杯等做法，在台灣攤車常見的做法是「花生米血糕」，將米血糕切成大而薄狀，以蒸的方式煮熟，插上竹籤、浸泡在醬汁中後，再裹上花生粉、灑香菜葉食用\[3\]。

除了上述吃法外，米血糕也是[火鍋店的常見配料](../Page/火鍋.md "wikilink")；有些名餐廳會將米血糕製成創意料理。還有一種常見的食用方式，是放進[麻油雞或](../Page/麻油雞.md "wikilink")[薑母鴨的湯中](../Page/薑母鴨.md "wikilink")，此時米血糕會再吸收湯汁的油脂和香味，形成獨特風味。

## 參見

  - [血腸](../Page/血腸.md "wikilink")
  - [豬紅](../Page/豬紅.md "wikilink")
  - [全球十大怪食](../Page/全球十大怪食.md "wikilink")

## 參考來源

[糯](../Category/台灣米飯類食品.md "wikilink")
[Category:中式糕點](../Category/中式糕點.md "wikilink")
[Category:糯米食品](../Category/糯米食品.md "wikilink")
[Category:動物血液食品](../Category/動物血液食品.md "wikilink")
[Category:泉州小吃](../Category/泉州小吃.md "wikilink")

1.  [〈台北都會〉早期米血糕 現今豬血糕](http://news.ltn.com.tw/news/local/paper/480405)
    過去農業社會，勤儉的農家宰殺雞、鴨食用之餘，會在一碗米或糯米中加入雞、鴨的血攪和，凝固就是「米血糕」...鄭永豐說，隨著時代進步，收集雞、鴨的血做米血糕，實在不符合經濟效益，因此許多業者都改用豬血
2.  [全球10大怪食
    豬血糕排第一，蘋果日報](http://tw.nextmedia.com/applenews/article/art_id/31945384/IssueID/20090916)
3.