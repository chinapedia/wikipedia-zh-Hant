**米诺环素**又称“**二甲胺四环素**”或“**美满环素**”，是一种广谱抗菌的[四环素类抗生素](../Page/四环素类抗生素.md "wikilink")。它能与[tRNA结合](../Page/tRNA.md "wikilink")，从而达到抑菌的效果。米诺环素比同类药物具有更广的抗菌谱，具有抑菌活性。因为其[半衰期较长](../Page/半衰期.md "wikilink")，米诺环素的血药浓度是其他同类型药物高2至4倍。

## 适应症

米诺环素主要用于治疗[痤疮和其他](../Page/痤疮.md "wikilink")[皮肤](../Page/皮肤.md "wikilink")[感染](../Page/感染.md "wikilink")，由于半衰期长，给药次数较其他药物少，使用方便。
尽管米诺环素具有广谱抗菌作用，但由于其[副作用](../Page/副作用.md "wikilink")，应用受到很大限制。

## 副作用

  - [菌群失调](../Page/菌群失调.md "wikilink")
  - [消化道反应](../Page/消化道.md "wikilink")
  - [肝损害](../Page/肝.md "wikilink")
  - [肾损害](../Page/肾.md "wikilink")
  - 影响[牙齿和](../Page/牙齿.md "wikilink")[骨发育](../Page/骨.md "wikilink")
  - [过敏反应](../Page/过敏反应.md "wikilink")

</div>

[Category:四环素类抗生素](../Category/四环素类抗生素.md "wikilink")