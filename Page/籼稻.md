**秈稻**（）（[学名](../Page/学名.md "wikilink")：）或**秈米**\[1\]，亦有稱**絲苗米**（港澳）或**在來米**（台灣），是[稻米的一個](../Page/稻米.md "wikilink")[亞種](../Page/亞種.md "wikilink")。秈稻，又稱[旱稻](../Page/旱稻.md "wikilink")，生長適合長日照，比粳稻耐熱耐旱，米粒長但粘性差，与[粳稻相比](../Page/粳稻.md "wikilink")，米粒中[蛋白質和](../Page/蛋白質.md "wikilink")[直链澱粉含量均较高](../Page/直链澱粉.md "wikilink")\[2\]。生長期短，可以[輪作](../Page/輪作.md "wikilink")，產量高。傳統上，華中、華南、台灣、印度及中南半島出產的米為秈米，而中國淮河以北、西南高海拔地區及日本、朝鮮等地所種植的稻米品種為[粳米](../Page/粳稻.md "wikilink")。在[長江流域可進行稻](../Page/長江.md "wikilink")[麥輪作](../Page/麥.md "wikilink")，在華南地區可實行一年兩熟甚至三熟。著名的有[泰國茉莉香米和](../Page/泰國.md "wikilink")[增城絲苗](../Page/增城.md "wikilink")。

早期台灣原住民栽種的稻米是以小米為主，直到[明鄭](../Page/明鄭.md "wikilink")[鄭成功來到台灣](../Page/鄭成功.md "wikilink")、發現台灣很適合栽種[水稻](../Page/水稻.md "wikilink")，才開始由中國大陸引進台灣大量栽種，因此開啟台灣稻米王國的開始。但是台灣稻米的基礎工程奠定在[日治時代](../Page/日治時代.md "wikilink")，當時日本政府以「工業日本、農業台灣」的殖民策略投入經營台灣的[蔗糖與稻米產業](../Page/蔗糖.md "wikilink")，大力建設[嘉南平原的](../Page/嘉南平原.md "wikilink")[水利工程](../Page/水利工程.md "wikilink")，並將所生產的蔗糖及稻米運回日本，但是當時台灣所種植的稻米品種為米粒較長的秈米，日本人稱之為「在來米」意指台灣在地本來就有的米，口感比[日本人習慣的粳米差](../Page/日本人.md "wikilink")，因此日本人育種出了適合台灣耕種的粳米品種[蓬萊米](../Page/蓬萊米.md "wikilink")。
近來台灣育種出台中10號，其為秈稻品種，口感卻接近粳米。

## 圖片區

[File:JasmineRice.jpg|茉莉香米](File:JasmineRice.jpg%7C茉莉香米) / 絲苗米(Jasmine)
<File:Brun> ris.jpg|Basmati

## 参考文獻

[Category:粮食](../Category/粮食.md "wikilink")
[Category:稻屬](../Category/稻屬.md "wikilink")
[Category:谷物](../Category/谷物.md "wikilink")

1.  ["秈"字,異體字解說 教育部異體字字典/臺灣/中華民國
    擷取](http://dict.variants.moe.edu.tw/yitib/frb/frb03086.htm)
2.