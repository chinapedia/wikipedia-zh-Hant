[CookedCellophaneNoodles.jpg](https://zh.wikipedia.org/wiki/File:CookedCellophaneNoodles.jpg "fig:CookedCellophaneNoodles.jpg")
**粉-{}-絲**是[東亞常見的](../Page/東亞.md "wikilink")[食品之一](../Page/食品.md "wikilink")，往往又叫做**粉条**、**粉条丝**、**冬-{}-粉**（主要在[台灣](../Page/台灣.md "wikilink")），日本稱**春雨**，[朝鮮半島稱](../Page/朝鮮半島.md "wikilink")**唐麵**，[越南稱](../Page/越南.md "wikilink")**麵**。粉丝通常是以[綠豆製成](../Page/綠豆.md "wikilink")，也可由[玉米淀粉或者](../Page/玉米淀粉.md "wikilink")[地瓜淀粉制作](../Page/太白粉.md "wikilink")。绿豆中的[直链淀粉最多](../Page/直链淀粉.md "wikilink")，口感好，长时间烹饪易碎；地瓜淀粉制成的粉丝更适合进行炖菜烹饪。粉絲的口感滑腻，類似細[麵條狀](../Page/麵.md "wikilink")，乾燥後販賣，食用前最好先泡水讓它柔軟，粉絲的直径一般在0.5毫米左右，這也是它有「絲」之名的由來。在台灣，最著名的粉絲是[中農粉絲。](../Page/中農粉絲.md "wikilink")

除了以[綠豆為原料的粉絲外](../Page/綠豆.md "wikilink")，[地瓜](../Page/地瓜.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[高粱](../Page/高粱.md "wikilink")、[豌豆](../Page/豌豆.md "wikilink")、[蠶豆](../Page/蠶豆.md "wikilink")、[馬鈴薯等](../Page/馬鈴薯.md "wikilink")[雜糧粉絲亦有生產](../Page/雜糧.md "wikilink")，尤以地瓜粉絲為多。絲條細勻，光潔度高，透明度強，質地韌柔，潔白，在水中浸泡48小時不變色，不發脹，食用爽口，能做多種中西菜餚，是家庭及飲食業界的熱煮涼拌之佳品。

## 来源

关于龙口粉丝的来源，实际上是因为[龙口自古就是一个](../Page/龙口.md "wikilink")[港口](../Page/港口.md "wikilink")，在古代，粉丝大多是由[山東省](../Page/山東省.md "wikilink")[烟台](../Page/烟台.md "wikilink")、[招远生产](../Page/招远.md "wikilink")，经[龙口港](../Page/龙口港.md "wikilink")，转运到世界各地，再加上当时没有[品牌意识](../Page/品牌.md "wikilink")，所以龙口粉丝就这样叫了出去。龙口粉丝以招远的品质为最好，同时，又以招远的[张星镇最为出名](../Page/张星镇.md "wikilink")。

龙口粉丝是中国的传统特产之一，其生产历史悠久。据史料记载，龙口粉丝已有三百多年的历史，最早产地是[招远](../Page/招远.md "wikilink")，以后逐渐发展到龙口、[蓬莱](../Page/蓬莱.md "wikilink")、[莱州](../Page/莱州.md "wikilink")、[栖霞](../Page/栖霞.md "wikilink")、[莱阳](../Page/莱阳.md "wikilink")、[海阳等地](../Page/海阳.md "wikilink")。龙口粉丝的出口最早可追溯到一百多年前，一九一六年龙口港开埠后，直接把粉丝运往香港和东南亚各国，这时招远、龙口生产的粉丝，绝大多数卖给龙口粉丝庄，龙口成为粉丝的集散地，因而得名龙口粉丝。因其原料好，气候适宜，加工精细，因而产品质量优异，被称为“粉丝之冠”。

除了山東的龍口粉絲以外，其他地方亦有生產粉絲。例如：台灣最早的發展為[中農粉絲](../Page/中農粉絲.md "wikilink")、最為著名的爲龍口粉絲；而在日本的僧人都會吃粉絲，而當地的粉絲有60%都是來自[奈良縣的](../Page/奈良縣.md "wikilink")[櫻井市及](../Page/櫻井市.md "wikilink")[御所市所出產的粉絲](../Page/御所市.md "wikilink")。不過，製造這些粉絲的原材料綠豆依然是從中國進口的。

## 食品安全的顾虑

粉絲為消費者經常食用的食品，經常出現在[火鍋或夏日的](../Page/火鍋.md "wikilink")[涼拌料理中](../Page/涼拌.md "wikilink")，但製作的過程若添加含明礬的[膨松剂](../Page/膨松剂.md "wikilink")，若經常食用，長久下來則有攝入多量鋁的風險，建議消費者仍需注意，認清產品資訊\[1\]\[2\]。
中國龍口地區所產的粉絲，聲稱已不再用明礬做添加劑。

## 料理

  - [螞蟻上樹](../Page/螞蟻上樹.md "wikilink")
  - [豬腸冬粉](../Page/豬腸冬粉.md "wikilink")
  - [八寶冬粉](../Page/八寶冬粉.md "wikilink")
  - [鵝肉冬粉](../Page/鵝肉冬粉.md "wikilink")
  - [鍋燒冬粉](../Page/鍋燒冬粉.md "wikilink")

## 參看

  - [純綠豆冬粉](https://www.youtube.com/watch?v=ORXm3w_NXMU)
  - [米線](../Page/米線.md "wikilink")
  - [米粉](../Page/米粉.md "wikilink")

## 参考文献

[粉絲](../Category/粉絲.md "wikilink")

1.
2.