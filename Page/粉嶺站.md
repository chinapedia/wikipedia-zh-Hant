}} {{ multiple image | align = right | direction = vertical | width =
220 | image1 = Fanling Station 2013 part2.JPG | caption1 = 車站外觀 }}
**粉嶺站**（）是位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[北區](../Page/北區_\(香港\).md "wikilink")[粉嶺的](../Page/粉嶺.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[東鐵綫一個地面車站](../Page/東鐵綫.md "wikilink")，它與[沙田站同是香港兩個最早通車](../Page/沙田站.md "wikilink")，而且現時仍然提供鐵路服務的車站。

## 結構

### 樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>U1</strong></p></td>
<td><p>大堂</p></td>
<td><p>出入口、客務中心、商店、自助服務、洗手間、免費Wi-Fi熱點</p></td>
</tr>
<tr class="even">
<td><p><strong>G</strong></p></td>
<td><p>側式月台，開左邊門</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>東鐵綫往<a href="../Page/紅磡站.md" title="wikilink">紅磡</a><small>（<a href="../Page/太和站_(香港).md" title="wikilink">太和</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>側式月台，開左邊門</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td><p>C出入口、客務中心</p></td>
<td></td>
</tr>
</tbody>
</table>

寒

### 大堂

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Fanling Station 2017 03 part2.jpg | caption1 = 南面大堂 }}
粉嶺站大堂位於月台之上，因此大堂中央為非收費區的出入口，收費區及閘機皆分為南北兩面，大堂北面收費區為舊大堂，並設有[洗手間及售票處](../Page/廁所.md "wikilink")（[兩鐵合併後改稱](../Page/兩鐵合併.md "wikilink")「客務中心」）；與[上水站未擴建前的大堂設計相似](../Page/上水站_\(香港\).md "wikilink")。而南面的收費區則為後期加建部份，原來並不能與北面收費區互通，必需利用月台往返南北面大堂的收費區；現已再加建為完全貫通。

大堂內有不同類型的商店，例如[便利店](../Page/便利商店.md "wikilink")、[零食店](../Page/零食.md "wikilink")、[麵包糕餅店](../Page/麵包糕餅店.md "wikilink")、[涼茶專賣店及外賣](../Page/涼茶.md "wikilink")[壽司店等](../Page/壽司.md "wikilink")。此外，也有不少自助服務設施供乘客使用，包括[自動櫃員機](../Page/自動櫃員機.md "wikilink")、[自動售賣機](../Page/自動販賣機.md "wikilink")\[1\]以及[香港郵政](../Page/香港郵政.md "wikilink")[郵箱](../Page/郵箱.md "wikilink")\[2\]\[3\]。另外，大堂非付費區設有「[會員服務站](../Page/會員服務站.md "wikilink")」，供乘客享用港鐵友禮會的會員優惠\[4\]。

在[互聯網服務上](../Page/互聯網.md "wikilink")，粉嶺站大堂及月台均設有由[電訊盈科提供的](../Page/電訊盈科.md "wikilink")[Wi-Fi熱點](../Page/Wi-Fi.md "wikilink")\[5\]，乘客可在候車的時候透過任何配備Wi-Fi功能的電子器材存取互聯網。

[Fanling_Station_2017_12_part5.jpg](https://zh.wikipedia.org/wiki/File:Fanling_Station_2017_12_part5.jpg "fig:Fanling_Station_2017_12_part5.jpg")

### 月台

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Fanling Station 2018 11 part1.jpg | caption1 = 1號月台﹙往羅湖／落馬洲方向﹚
| image2 = Fanling Station 2018 11 part2.jpg | caption2 = 2號月台﹙往紅磡方向﹚ }}
粉嶺站設有2個[側式月台](../Page/側式月台.md "wikilink")，位於地面層，並無安裝[月台幕門](../Page/月台幕門.md "wikilink")。粉嶺站月台中央設有扶手電梯連接路軌之上的大堂，而在月台兩旁均設有不少緊急出口供乘客在緊急時疏散。值得一提的是，粉嶺站月台藝術設計並不如大多東鐵線車站般僅採用單一歷史建築元素，而是同時選用了粉嶺區內兩條原居民圍村的元素，前方有三個圓形圖案的是[粉嶺正圍的圍門](../Page/粉嶺圍.md "wikilink")，後方寫有「老圍」二字的則是[龍躍頭老圍的圍門](../Page/老圍_\(龍躍頭\).md "wikilink")，粉嶺彭氏和龍躍頭鄧氏皆爲[新界五大族](../Page/新界五大氏族.md "wikilink")，是區內規模最大的兩個圍村族群。

[Fanling_Station_2017_12_part3.jpg](https://zh.wikipedia.org/wiki/File:Fanling_Station_2017_12_part3.jpg "fig:Fanling_Station_2017_12_part3.jpg")
[Fanling_Station_2017_12_part4.jpg](https://zh.wikipedia.org/wiki/File:Fanling_Station_2017_12_part4.jpg "fig:Fanling_Station_2017_12_part4.jpg")

### 出入口

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Fanling Station 2017 03 part3.jpg | caption1 = A出入口 | image2 =
Fanling Station Exit C 201504.jpg | caption2 = C出入口 | image3 = Fanling
Staton Bicycle Parking 201504.jpg | caption3 = A2出口外設單車停泊處 }}
粉嶺站並設有5個出入口，其中A、B出入口皆連接車站南北大堂。另外，往[紅磡的](../Page/紅磡站.md "wikilink")2號月台則另設C出入口連接粉嶺車站路，方便乘客由其他交通工具轉乘東鐵綫前往[九龍](../Page/九龍.md "wikilink")。各個出入口均設有指示牌顯示出入口周邊的建築物及設施列表。

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>大堂</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/北區政府合署.md" title="wikilink">北區政府合署</a>、<a href="../Page/粉嶺法院大樓.md" title="wikilink">粉嶺法院大樓</a>、<a href="../Page/粉嶺裁判法院.md" title="wikilink">粉嶺裁判法院</a>、<a href="../Page/粉嶺遊樂場.md" title="wikilink">粉嶺遊樂場</a>、<a href="../Page/聯和墟.md" title="wikilink">聯和墟</a>、上水分區警署</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/粉嶺名都.md" title="wikilink">粉嶺名都</a>、<a href="../Page/明愛粉嶺陳震夏中學.md" title="wikilink">明愛粉嶺陳震夏中學</a>、<a href="../Page/祥華邨.md" title="wikilink">祥華邨</a>、<a href="../Page/粉嶺中心.md" title="wikilink">粉嶺中心</a>、<a href="../Page/粉嶺官立小學.md" title="wikilink">粉嶺官立小學</a>、<a href="../Page/東華三院李嘉誠中學.md" title="wikilink">東華三院李嘉誠中學</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>小巴站、的士站</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/蓬瀛仙館.md" title="wikilink">蓬瀛仙館</a>、<a href="../Page/碧湖花園.md" title="wikilink">碧湖花園</a>、<a href="../Page/粉嶺救恩書院.md" title="wikilink">粉嶺救恩書院</a>、<a href="../Page/嘉福邨.md" title="wikilink">嘉福邨</a>、<a href="../Page/嘉盛苑.md" title="wikilink">嘉盛苑</a>、<a href="../Page/景盛苑.md" title="wikilink">景盛苑</a>、<a href="../Page/香港觀宗寺.md" title="wikilink">觀宗寺</a>、百福花園、<a href="../Page/聖芳濟各書院.md" title="wikilink">聖芳濟各書院</a>、<a href="../Page/華明邨.md" title="wikilink">華明邨</a>、<a href="../Page/華心邨.md" title="wikilink">華心邨</a>、<a href="../Page/欣盛苑.md" title="wikilink">欣盛苑</a>、<a href="../Page/花都廣場.md" title="wikilink">花都廣場</a>、<a href="../Page/牽晴間.md" title="wikilink">牽晴間</a>、<a href="../Page/中華基督教會基新中學.md" title="wikilink">中華基督教會基新中學</a></p></td>
</tr>
<tr class="even">
<td><p>2號月台（地面）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>粉嶺政府合署、祥華邨、粉嶺中心、粉嶺官立小學、前粉嶺女童院、粉嶺法院大樓、粉嶺裁判法院、粉嶺遊樂場、<a href="../Page/粉嶺游泳池.md" title="wikilink">粉嶺游泳池</a>、粉嶺名都、聯和墟、<a href="../Page/五旬節靳茂生小學.md" title="wikilink">五旬節靳茂生小學</a>、上水分區警署</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 接駁交通

粉嶺站設於粉嶺市中心，接駁交通十分完善。車站旁設有[公共運輸交匯處](../Page/公共運輸交匯處.md "wikilink")，內有[專線小巴站及](../Page/專線小巴.md "wikilink")[的士站](../Page/的士站.md "wikilink")。到2013年起，A2出口外設「雙層單車泊架」系統，方便居民利用[單車代步](../Page/單車.md "wikilink")。在[百和路亦有一](../Page/百和路.md "wikilink")[巴士站](../Page/巴士站.md "wikilink")，方便乘客轉乘其他公共交通工具前往各區。

<table>
<thead>
<tr class="header">
<th><p>接駁交通列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<ul>
<li><a href="../Page/蓬瀛仙館.md" title="wikilink">蓬瀛仙館</a>，<a href="../Page/百和路.md" title="wikilink">百和路巴士站</a></li>
</ul>
<ul>
<li>粉嶺車站路巴士站</li>
</ul>
<ul>
<li>粉嶺名都/<a href="../Page/祥華邨.md" title="wikilink">祥華邨祥頌樓巴士站</a></li>
</ul>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl>
<ul>
<li>粉嶺車站路小巴總站</li>
</ul>
<ul>
<li>蓬瀛仙館，百和路小巴站</li>
</ul>
<dl>
<dt><a href="../Page/香港的士.md" title="wikilink">的士</a></dt>

</dl>
<ul>
<li>市區的士站</li>
<li>新界的士站</li>
</ul></td>
</tr>
</tbody>
</table>

## 利用狀況

{{ original research | 章節 }}
粉嶺站附近有多個大型屋苑及公共屋邨，加上粉嶺南和聯和墟一帶居民皆依賴鐵路出入市區，所以其使用量一直高企。在[清明和](../Page/清明.md "wikilink")[重陽節期間](../Page/重陽節.md "wikilink")，九巴更提供接駁巴士76S前往和合石墳場，而本站亦鄰近[蓬瀛仙館](../Page/蓬瀛仙館.md "wikilink")，令其使用量更進一步增加。

並與[上水站一樣](../Page/上水站.md "wikilink")，憑一般成人[八達通卡由港鐵各站](../Page/八達通.md "wikilink")（[東鐵綫新界路段及](../Page/東鐵綫.md "wikilink")[馬鞍山綫除外](../Page/馬鞍山綫.md "wikilink")）乘搭東鐵綫普通等往來[羅湖或](../Page/羅湖站_\(香港\).md "wikilink")[落馬洲](../Page/落馬洲站.md "wikilink")，在粉嶺站出閘再入閘繼續餘下旅程，也可節省3.9元車資。

## 歷史

[粉嶺車站.jpg](https://zh.wikipedia.org/wiki/File:粉嶺車站.jpg "fig:粉嶺車站.jpg")

### 沙頭角支線轉車站

1910年[九廣鐵路英段通車時已經設有粉嶺站](../Page/九廣鐵路.md "wikilink")，其後[九廣鐵路局興建連接粉嶺站至沙頭角站長約](../Page/九廣鐵路局.md "wikilink")11.67公里的[沙頭角支線](../Page/沙頭角支線.md "wikilink")。沙頭角支線在1912年4月1日通車，共設5個站，路線以沿現今[沙頭角公路](../Page/沙頭角公路.md "wikilink")，由粉嶺站經[洪嶺站](../Page/洪嶺站.md "wikilink")、禾坑站、石涌凹站至沙頭角站。兩線採用不同寬度的路軌和火車，故兩線路軌不能連接。沙頭角支線的粉嶺站和九廣英段的粉嶺站往九龍方向的一邊呈90°交錯。由於走線相近的沙頭角公路於1927年開通，令原有的沙頭角鐵路支線處於虧蝕狀態，沙頭角鐵路支線遂於1928年4月1日停止行駛，粉嶺站變回一個普通車站。

### 和合石支線轉車站

第2次世界大戰之後，[和合石墳場及火葬場啟用](../Page/和合石墳場.md "wikilink")。為方便日常運送屍體往當地的火葬場及墳場，及於每年的[清明節和](../Page/清明節.md "wikilink")[重陽節運送前往當地](../Page/重陽節.md "wikilink")[掃墓的乘客](../Page/掃墓.md "wikilink")，九廣鐵路局於1949年興建[和合石支線](../Page/和合石支線.md "wikilink")。1978年，為配合粉嶺新市鎮的發展，和合石村南移，支線亦停止運作。

### 九廣鐵路電氣化

[Fanling_Station.jpg](https://zh.wikipedia.org/wiki/File:Fanling_Station.jpg "fig:Fanling_Station.jpg")
1983年7月15日粉嶺站成為九廣鐵路電氣化最後一期的其中一個車站。

#### 舊月台

<table>
<tbody>
<tr class="odd">
<td><p><strong>P</strong></p></td>
</tr>
<tr class="even">
<td><p>側式月台</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>側式月台</p></td>
</tr>
</tbody>
</table>

### 擴建及翻新

為應付使用此站的乘客數量大幅增加，九鐵於1998年為車站進行擴建工程。新大堂部份位於A、B出入口南面，於1999年啟用，新大堂增設4道扶手電梯、12部出入閘機、客務中心（非收費區及收費區，目前已停用）及5
間非收費區店舖。

到了2001年，原有80年代建成的大堂亦進行重舖地台工程，感覺較現代化。

### 擴闊月台

2001年，九鐵為車站南行頭尾部份月台進行擴闊工程，令人流更暢通。

### 翻新月台

兩鐵合併後，港鐵為東鐵綫各車站月台進行較低成本的翻新工程，以遮掩東鐵綫車站月台殘舊的觀感。工程包括重新為各東鐵綫車站配主題色，並加入車站代表植物，為月台橫樑及柱子翻上月台配色的油漆，貼上印有車站主題植物的圖案、車站附近建築物、車站名書法字的牆紙，加設直立港鐵式站牌、路線圖、指示牌、告示等等。本站被配色為淺綠色，主題植物為馬蹄蘭，街景為粉嶺老圍。

### 翻新大堂

{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Fanling Station Concourse 2008.jpg | caption1 = 翻新前的北面大堂（2008年）
}}
[港鐵公司於](../Page/港鐵公司.md "wikilink")2008年引入嶄新的建築理念－「融入大自然」，作為東鐵綫大型車站改善工程的設計方向。繼[大埔墟](../Page/大埔墟站.md "wikilink")、[旺角東及](../Page/旺角東站.md "wikilink")[沙田站的大型翻新工程於竣工後](../Page/沙田站.md "wikilink")，東鐵綫的上水及粉嶺站亦正進行翻新工程。港鐵公司於2013年進行粉嶺站大堂翻新工程，包括：

  - 翻新公共廁所及客務中心；
  - 增加商店數目。商店總面積由454平方米擴充至549平方米；
  - 優化指示牌和廣告牌佈局；
  - 美化大堂天花、牆身及地板。

改善工程於2015年完成。\[6\]

## 軼聞

經典電影《[賭神](../Page/賭神.md "wikilink")》中，賭神在乘火車中途險遭伏擊，伏擊手便是在粉嶺站上車。\[7\]

## 未來發展

### 未來樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>U1</strong></p></td>
<td><p>大堂</p></td>
<td><p>出入口、客務中心、商店、自助服務、洗手間、免費Wi-Fi熱點</p></td>
</tr>
<tr class="even">
<td><p><strong>G</strong></p></td>
<td><p>側式月台</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>東鐵綫往<a href="../Page/金鐘站.md" title="wikilink">金鐘</a><small>（<a href="../Page/太和站_(香港).md" title="wikilink">太和</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>側式月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td><p>C出入口、客務中心</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [港鐵公司－粉嶺站街道圖](http://www.mtr.com.hk/archive/ch/services/maps/fan.pdf)
  - [港鐵公司－粉嶺站位置圖](http://www.mtr.com.hk/archive/ch/services/layouts/fan.pdf)
  - [港鐵公司－粉嶺站列車服務時間](http://www.mtr.com.hk/archive/ch/services/timetables/fan.pdf)

[Category:粉嶺](../Category/粉嶺.md "wikilink")
[Category:前九廣東鐵車站](../Category/前九廣東鐵車站.md "wikilink")
[Category:1910年啟用的鐵路車站](../Category/1910年啟用的鐵路車站.md "wikilink")
[Category:東鐵綫車站](../Category/東鐵綫車站.md "wikilink")
[Category:以綠色為主題的港鐵站](../Category/以綠色為主題的港鐵站.md "wikilink")
[Category:北區鐵路車站 (香港)](../Category/北區鐵路車站_\(香港\).md "wikilink")

1.
2.

3.

4.

5.

6.

7.  [映城之戀（四）︰沙田站——由郊遊勝地到新城市](https://www.thenewslens.com/article/30005)