[HK_Wan_Chai_North_灣仔北_Grand_Hyatt_Hotel_保利集團_Poly_Auction_preview_exhibits_拍賣_Moutai_貴州茅台酒_Maotai_大麯白酒_Sept_2017_IX1_06.jpg](https://zh.wikipedia.org/wiki/File:HK_Wan_Chai_North_灣仔北_Grand_Hyatt_Hotel_保利集團_Poly_Auction_preview_exhibits_拍賣_Moutai_貴州茅台酒_Maotai_大麯白酒_Sept_2017_IX1_06.jpg "fig:HK_Wan_Chai_North_灣仔北_Grand_Hyatt_Hotel_保利集團_Poly_Auction_preview_exhibits_拍賣_Moutai_貴州茅台酒_Maotai_大麯白酒_Sept_2017_IX1_06.jpg")、[小麦和水为原料制成的](../Page/小麦.md "wikilink")[贵州茅台酒](../Page/贵州茅台酒公司.md "wikilink")\]\]
**粮食酒**是用[粮食酿造的](../Page/粮食.md "wikilink")[酒](../Page/酒.md "wikilink")，用粮食造酒要比用其他原料复杂，因为酿造[果酒利用葡萄糖](../Page/果酒.md "wikilink")，一般[微生物就可以发酵](../Page/微生物.md "wikilink")，酿造[啤酒](../Page/啤酒.md "wikilink")、[威士忌等利用大麦种子自己产生的](../Page/威士忌.md "wikilink")[酶将](../Page/酶.md "wikilink")[淀粉转化成](../Page/淀粉.md "wikilink")[麦芽糖后发酵](../Page/麦芽糖.md "wikilink")。而直接利用粮食，首先要把澱粉转化成糖，这不是一般微生物可以作到的，需要[霉菌介入](../Page/霉菌.md "wikilink")，但一般霉菌在分解淀粉的同时会产生有害人体的毒性物质，必须筛选、驯化霉菌，就是製麯\[1\]。

酿造粮食酒的方法首先是[中国人发明的](../Page/中国.md "wikilink")，中国古代不出产大批、易储存的水果，最好的水果只有桃和梨，不能浪费在酒上。利用粮食酿酒根据传说最早是[禹的妻子](../Page/禹.md "wikilink")[仪狄发明的](../Page/仪狄.md "wikilink")，为了宣扬禹的“明君”形象，历史书说禹“饮而甘之”，然后说以后这种东西必将导致亡国，“遂远仪狄”\[2\]。

粮食酒常用的原料是[糯米和](../Page/糯米.md "wikilink")[黍](../Page/黍.md "wikilink")。江南的香糯米和[卫辉路](../Page/卫辉路.md "wikilink")[辉州](../Page/辉州.md "wikilink")（今河南辉县）出产的[苏门糯米](../Page/苏门糯米.md "wikilink")，都是酿酒的好材料\[3\]。粮食酒原来都是放在罈中边酿造边贮存，喝时开罈，筛除酵母渣滓。所以古代喝酒也叫“筛酒”，直到[清朝后期才有](../Page/清朝.md "wikilink")[蒸馏酒出现](../Page/蒸馏酒.md "wikilink")（一說[元朝時即已出現](../Page/元朝.md "wikilink")），改为以小盅喝烈酒，一般为65度，最高达67度。最近幾十年才出现30—40度的低度蒸馏酒。

用粮食酿酒的方法后来逐渐传入中国的邻国，[朝鲜](../Page/朝鲜.md "wikilink")、[日本](../Page/日本.md "wikilink")、[越南用大米酿酒](../Page/越南.md "wikilink")，[俄罗斯用](../Page/俄罗斯.md "wikilink")[小麦](../Page/小麦.md "wikilink")、[马铃薯酿造](../Page/马铃薯.md "wikilink")[伏特加](../Page/伏特加.md "wikilink")，直到目前世界上也只有这些国家可以用粮食直接酿酒。各种粮食酒都带有粮食品种特有的味道，如大米酒就有一些米糠味，经过几千年的摸索，中国人发现只有[高粱酿造的酒没有任何干扰味道](../Page/高粱.md "wikilink")，因此中国的著名[白酒几乎都主要是用高粱酿造的](../Page/白酒.md "wikilink")。

## 参考

[Category:酒](../Category/酒.md "wikilink")

1.
2.
3.