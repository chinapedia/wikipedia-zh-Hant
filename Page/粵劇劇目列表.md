**[粵劇劇目](../Page/粵劇.md "wikilink")/曲目列表**（*漢語拼音序列*） __NOTOC__
[A](../Page/#A.md "wikilink") [B](../Page/#B.md "wikilink")
[C](../Page/#C.md "wikilink") [D](../Page/#D.md "wikilink")
[E](../Page/#E.md "wikilink") [F](../Page/#F.md "wikilink")
[G](../Page/#G.md "wikilink") [H](../Page/#H.md "wikilink")
[I](../Page/#I.md "wikilink") [J](../Page/#J.md "wikilink")
[K](../Page/#K.md "wikilink") [L](../Page/#L.md "wikilink")
[M](../Page/#M.md "wikilink") [N](../Page/#N.md "wikilink")
[O](../Page/#O.md "wikilink") [P](../Page/#P.md "wikilink")
[Q](../Page/#Q.md "wikilink") [R](../Page/#R.md "wikilink")
[S](../Page/#S.md "wikilink") [T](../Page/#T.md "wikilink")
[U](../Page/#U.md "wikilink") [V](../Page/#V.md "wikilink")
[W](../Page/#W.md "wikilink") [X](../Page/#X.md "wikilink")
[Y](../Page/#Y.md "wikilink") [Z](../Page/#Z.md "wikilink")

-----

## B

  - 《[白兔會](../Page/白兔會.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[白蛇傳](../Page/白蛇傳.md "wikilink")》
  - 《[白蛇新傳](../Page/白蛇新傳.md "wikilink")》（編劇：[葉紹德等](../Page/葉紹德.md "wikilink")）
  - 《[白門樓](../Page/白門樓.md "wikilink")》
  - 《[班定遠平西域](../Page/班定遠平西域.md "wikilink")》
    （編劇：[梁啟超](../Page/梁啟超.md "wikilink")）
  - 《[百里奚會妻](../Page/百里奚會妻.md "wikilink")》
  - 《[別館盟心](../Page/別館盟心.md "wikilink")》
  - 《[琵琶行](../Page/琵琶行.md "wikilink")》
  - 《[琵琶怨](../Page/琵琶怨.md "wikilink")》
  - 《[寶蓮燈](../Page/寶蓮燈.md "wikilink")》（場次：仙凡相會...）
  - 《[寶劍重揮萬丈虹](../Page/寶劍重揮萬丈虹.md "wikilink")》（編劇：[潘一帆](../Page/潘一帆.md "wikilink")）
  - 《[碧海狂僧](../Page/碧海狂僧.md "wikilink")》（編劇：[陳冠卿](../Page/陳冠卿.md "wikilink")）
  - 《[伯牙碎琴](../Page/伯牙碎琴.md "wikilink")》
  - 《[枇杷花下結新知](../Page/枇杷花下結新知.md "wikilink")》

## C

  - 《[長阪坡](../Page/長阪坡.md "wikilink")》
  - 《[穿金寶扇](../Page/穿金寶扇.md "wikilink")》編劇：唐滌生
  - 《[曹操與楊修](../Page/曹操與楊修.md "wikilink")》粵劇編劇：秦中英
  - 《[曹操下皖城](../Page/曹操下皖城.md "wikilink")》
  - 《[釵頭鳳](../Page/釵頭鳳.md "wikilink")》
  - 《[陳橋兵變](../Page/陳橋兵變.md "wikilink")》

## D

  - 《[大鬧廣昌隆](../Page/大鬧廣昌隆.md "wikilink")》
  - 《[大鬧養閑堂](../Page/大鬧養閑堂.md "wikilink")》
  - 《[大鬧御勾欄](../Page/大鬧御勾欄.md "wikilink")》
  - 《[大破朱仙鎮](../Page/大破朱仙鎮.md "wikilink")》
  - 《[狄青夜闖三關](../Page/狄青夜闖三關.md "wikilink")》-平子喉對唱曲
  - 《[狄青與襄陽公主](../Page/狄青與襄陽公主.md "wikilink")》
  - 《[帝女花](../Page/帝女花.md "wikilink")》（場次:
    樹盟、香劫、乞屍、庵遇、相認、迎鳳、上表、香夭）（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[竇娥冤](../Page/竇娥冤.md "wikilink")》
    （又名《[六月飞霜](../Page/六月飞霜.md "wikilink")》或《[六月雪](../Page/六月雪.md "wikilink")》）（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[蝶影紅梨記](../Page/蝶影紅梨記.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[定軍山](../Page/定軍山.md "wikilink")》
  - 《[黛玉歸天](../Page/黛玉歸天.md "wikilink")》
  - 《[貂蟬拜月](../Page/貂蟬拜月.md "wikilink")》
  - 《[單刀會](../Page/單刀會.md "wikilink")》
  - 《[刁蠻宮主戇駙馬](../Page/刁蠻宮主戇駙馬.md "wikilink")》
  - 《[多情君瑞俏紅娘](../Page/多情君瑞俏紅娘.md "wikilink")》（編劇：[秦中英](../Page/秦中英.md "wikilink")）

## F

  - 《[風雨同舟](../Page/風雨同舟.md "wikilink")》
  - 《[鳳閣恩仇未了情](../Page/鳳閣恩仇未了情.md "wikilink")》（場次：送別...）
  - 《[鳳儀亭](../Page/鳳儀亭.md "wikilink")》
  - 《[鳳儀亭訴苦](../Page/鳳儀亭訴苦.md "wikilink")》
  - 《[粉面十三郎](../Page/粉面十三郎.md "wikilink")》
  - 《[非夢奇緣](../Page/非夢奇緣.md "wikilink")》（編劇：《[靳夢萍](../Page/靳夢萍.md "wikilink")》）<sup>[1](https://web.archive.org/web/20070929065401/http://yamkimfai.net/film/law21.htm)</sup>

## G

  - 《[關公送嫂](../Page/關公送嫂.md "wikilink")》
  - 《[觀音得道，香花山大賀壽](../Page/觀音得道，香花山大賀壽.md "wikilink")》（傳統例戲）
  - 《[閨留學廣](../Page/閨留學廣.md "wikilink")》
  - 《[孤寒種懺悔](../Page/孤寒種懺悔.md "wikilink")》
  - 《[桂枝告狀](../Page/桂枝告狀.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）

## H

  - 《[火網梵宮十四年](../Page/火網梵宮十四年.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")，主題曲：[吳一嘯](../Page/吳一嘯.md "wikilink")）
  - 《[火燒博望坡](../Page/火燒博望坡.md "wikilink")》
  - 《[火燒赤壁](../Page/火燒赤壁.md "wikilink")》
  - 《[花月影](../Page/花月影.md "wikilink")》
  - 《[花田八喜](../Page/花田八喜.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[花蕊夫人](../Page/花蕊夫人.md "wikilink")》
  - 《[花染狀元紅](../Page/花染狀元紅.md "wikilink")》
  - 《[活命金牌](../Page/活命金牌.md "wikilink")》（編劇：[蘇翁](../Page/蘇翁.md "wikilink")）
  - 《[紅了櫻桃碎了心](../Page/紅了櫻桃碎了心.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[紅樓夢](../Page/紅樓夢.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[胡不歸](../Page/胡不歸.md "wikilink")》（編劇：[馮志芬](../Page/馮志芬.md "wikilink")）
  - 《[魂夢繞山河](../Page/魂夢繞山河.md "wikilink")》（撰曲：[阮眉](../Page/阮眉.md "wikilink")）
  - 《[漢武帝夢會衛夫人](../Page/漢武帝夢會衛夫人.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[黃蕭養回頭](../Page/黃蕭養回頭.md "wikilink")》
  - 《[黃鶴樓](../Page/黃鶴樓.md "wikilink")》
  - 《[黃飛虎反五關](../Page/黃飛虎反五關.md "wikilink")》
  - 《[華容道](../Page/華容道.md "wikilink")》（古老排場）

## J

  - 《[鏡花緣](../Page/鏡花緣.md "wikilink")》
  - 《[荊釵記](../Page/荊釵記.md "wikilink")》
  - 《[荊軻剌秦皇](../Page/荊軻剌秦皇.md "wikilink")》
  - 《[佳偶天成](../Page/佳偶天成.md "wikilink")》
  - 《[舉獅觀圖](../Page/舉獅觀圖.md "wikilink")》
  - 《[蔣幹盜書](../Page/蔣幹盜書.md "wikilink")》

## K

  - 《[客途秋恨](../Page/客途秋恨.md "wikilink")》（南音）
  - 《[苦鳳鶯憐](../Page/苦鳳鶯憐.md "wikilink")》
  - 《[開叉](../Page/開叉.md "wikilink")》

## L

  - 《[六國大封相](../Page/六國大封相.md "wikilink")》（開台例戲）（編劇：[劉華東](../Page/劉華東.md "wikilink")）
  - 《[李後主](../Page/李後主_\(粵劇\).md "wikilink")》（編劇：[葉紹德](../Page/葉紹德.md "wikilink")）
  - 《[李密陳情](../Page/李密陳情.md "wikilink")》
  - 《[柳毅傳書](../Page/柳毅傳書.md "wikilink")》（內地版本編劇：[譚青霜](../Page/譚青霜.md "wikilink")/[陳冠卿](../Page/陳冠卿.md "wikilink")，首演：[羅家寶](../Page/羅家寶.md "wikilink")
    [林小群](../Page/林小群.md "wikilink")，香港版本的編劇是[葉紹德](../Page/葉紹德.md "wikilink")，首演：[雛鳳鳴粵劇團](../Page/雛鳳鳴粵劇團.md "wikilink")）
  - 《[洛水情夢](../Page/洛水情夢.md "wikilink")》
  - 《[荔枝換絳桃](../Page/荔枝換絳桃.md "wikilink")》/《[九天玄女](../Page/九天玄女.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[梁祝](../Page/梁祝.md "wikilink")》
  - 《[梁祝恨史](../Page/梁祝恨史.md "wikilink")》（編劇：[潘一帆](../Page/潘一帆.md "wikilink")）
  - 《[攔江截斗](../Page/攔江截斗.md "wikilink")》
  - 《[落霞孤鶩](../Page/落霞孤鶩.md "wikilink")》
  - 《[樂毅下齊城](../Page/樂毅下齊城.md "wikilink")》
  - 《[拉郎配](../Page/拉郎配.md "wikilink")》

## M

  - 《[牡丹亭驚夢](../Page/牡丹亭驚夢.md "wikilink")》（場次：遊園驚夢、寫真、幽媾、回生、探親會母、拷元、圓駕）（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[穆桂英大破洪州](../Page/穆桂英大破洪州.md "wikilink")》
  - 《[穆桂英下山](../Page/穆桂英下山.md "wikilink")》
  - 《[盲公問米](../Page/盲公問米.md "wikilink")》
  - 《[夢會太湖](../Page/夢會太湖.md "wikilink")》（編劇：[陳自強](../Page/陳自強.md "wikilink")）
  - 《[夢斷香銷四十年](../Page/夢斷香銷四十年.md "wikilink")》（編劇：[陳冠卿](../Page/陳冠卿.md "wikilink")）

## N

  - 《[南宋鴛鴦鏡](../Page/南宋鴛鴦鏡.md "wikilink")》（編劇：[盧山](../Page/盧山.md "wikilink")）
  - 《[南唐殘夢](../Page/南唐殘夢.md "wikilink")》

## P

  - 《[平貴別窰](../Page/平貴別窰.md "wikilink")》
  - 《[蟠龍令](../Page/蟠龍令.md "wikilink")》（編劇：[蘇翁](../Page/蘇翁.md "wikilink")）

## Q

  - 《[妻嬌郎更嬌](../Page/妻嬌郎更嬌.md "wikilink")》
  - 《[秦香蓮](../Page/秦香蓮.md "wikilink")》
  - 《[泣荊花](../Page/泣荊花.md "wikilink")》
  - 《[乾坤鏡](../Page/乾坤鏡.md "wikilink")》（編劇：[楊智深](../Page/楊智深.md "wikilink")）
  - 《[清宮遺恨](../Page/清宮遺恨.md "wikilink")》
  - 《[情僧偷到瀟湘館](../Page/情僧偷到瀟湘館.md "wikilink")》
  - 《[情醉王大儒](../Page/情醉王大儒.md "wikilink")》（編劇：[秦中英](../Page/秦中英.md "wikilink")）

## R

  - 《[睿王與莊妃](../Page/睿王與莊妃.md "wikilink")》
  - 《[戎馬金戈萬里情](../Page/戎馬金戈萬里情.md "wikilink")》（編劇：[潘焯](../Page/潘焯.md "wikilink")）

## S

  - 《[三笑姻緣](../Page/三笑姻緣.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[三英戰呂布](../Page/三英戰呂布.md "wikilink")》
  - 《[三打王英](../Page/三打王英.md "wikilink")》
  - 《[三審玉堂春](../Page/三審玉堂春.md "wikilink")》
  - 《[三娘教子](../Page/三娘教子.md "wikilink")》
  - 《[三盜九龍杯](../Page/三盜九龍杯.md "wikilink")》
  - 《[三借芭蕉扇](../Page/三借芭蕉扇.md "wikilink")》
  - 《[三氣白牡丹](../Page/三氣白牡丹.md "wikilink")》
  - 《[三伯爵](../Page/三伯爵.md "wikilink")》
  - 《[三祭鐵丘墳](../Page/三祭鐵丘墳.md "wikilink")》
  - 《[三擒三放](../Page/三擒三放.md "wikilink")》
  - 《[三官堂(陳世美)](../Page/三官堂\(陳世美\).md "wikilink")》
  - 《[三看御妹](../Page/三看御妹.md "wikilink")》
  - 《[山東響馬](../Page/山東響馬.md "wikilink")》
  - 《[山鄉風雲](../Page/山鄉風雲.md "wikilink")》
  - 《[搜書院](../Page/搜書院.md "wikilink")》（編劇：[楊子靜](../Page/楊子靜.md "wikilink")）
  - 《[隋宮十載菱花夢](../Page/隋宮十載菱花夢.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[獅吼記](../Page/獅吼記.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[審死官](../Page/審死官.md "wikilink")》
  - 《[蘇武牧羊](../Page/蘇武牧羊.md "wikilink")》
  - 《[水淹七軍](../Page/水淹七軍.md "wikilink")》
  - 《[沙陀借兵](../Page/沙陀借兵.md "wikilink")》
  - 《[雙仙拜月亭](../Page/雙仙拜月亭.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[顺治与董鄂妃](../Page/顺治与董鄂妃.md "wikilink")》（編劇：《[李悦强](../Page/李悦强.md "wikilink")》）

## T

  - 《[踏雪尋梅](../Page/踏雪尋梅.md "wikilink")》（撰曲：[蔡衍棻](../Page/蔡衍棻.md "wikilink")）
  - 《[唐明皇與楊貴妃](../Page/唐明皇與楊貴妃.md "wikilink")》、《[唐宮恨史](../Page/唐宮恨史.md "wikilink")》（場次：《[七月七日長生殿](../Page/七月七日長生殿.md "wikilink")》）
  - 《[唐伯虎點秋香](../Page/唐伯虎點秋香.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[桃花扇](../Page/桃花扇.md "wikilink")》（香港版本編劇：楊智深）
  - 《[桃花仙子](../Page/桃花仙子.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")，電影版編劇：[潘焯](../Page/潘焯.md "wikilink")）
  - 《[鐵馬銀婚](../Page/鐵馬銀婚.md "wikilink")》（編劇：[蘇翁](../Page/蘇翁.md "wikilink")）

## W

  - 《[文姬歸漢](../Page/文姬歸漢.md "wikilink")》（編導：[龍圖](../Page/龍圖.md "wikilink")，撰曲:
    [蘇翁](../Page/蘇翁.md "wikilink")）
  - 《[萬世流芳張玉喬](../Page/萬世流芳張玉喬.md "wikilink")》（編劇：[簡又文](../Page/簡又文.md "wikilink")/[唐滌生](../Page/唐滌生.md "wikilink")，首演：[芳艷芬](../Page/芳艷芬.md "wikilink")
    [陳錦棠](../Page/陳錦棠.md "wikilink")）
  - 《[萬惡淫為首](../Page/萬惡淫為首.md "wikilink")》
  - 《[無情寶劍有情天](../Page/無情寶劍有情天.md "wikilink")》（場次：...營房相會...）（編劇：[徐子郎](../Page/徐子郎.md "wikilink")，改編：[蘇翁](../Page/蘇翁.md "wikilink")）
  - 《[舞台春色](../Page/舞台春色.md "wikilink")》
  - 《[五郎出家](../Page/五郎出家.md "wikilink")》
  - 《[五郎救弟](../Page/五郎救弟.md "wikilink")》（大喉平喉對唱曲）
  - 《[五虎平南(段紅玉)](../Page/五虎平南\(段紅玉\).md "wikilink")》
  - 《[王老虎搶親](../Page/王老虎搶親.md "wikilink")》
  - 《王寶釧》 李少芸編撰

## X

  - 《[西樓錯夢](../Page/西樓錯夢.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[西河會](../Page/西河會.md "wikilink")》
  - 《[倩女離魂](../Page/倩女離魂.md "wikilink")》
  - 《[新白金龍](../Page/新白金龍.md "wikilink")》
  - 《[俏潘安](../Page/俏潘安.md "wikilink")》(編劇：[葉紹德](../Page/葉紹德.md "wikilink"))
  - 《[香羅塚](../Page/香羅塚.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[血染海棠紅](../Page/血染海棠紅.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[血濺烏紗](../Page/血濺烏紗.md "wikilink")》（編劇：[楊子靜等](../Page/楊子靜.md "wikilink")）

## Y

  - 《[燕子樓](../Page/燕子樓.md "wikilink")》
  - 《[雅雀如何作鳳凰](../Page/雅雀如何作鳳凰.md "wikilink")》
  - 《[夜吊秋喜](../Page/夜吊秋喜.md "wikilink")》
  - 《[夜吊白芙蓉](../Page/夜吊白芙蓉.md "wikilink")》（古腔粵曲）
  - 《[夜戰馬超](../Page/夜戰馬超.md "wikilink")》（霸腔對唱曲）
  - 《[夜斬龍詛](../Page/夜斬龍詛.md "wikilink")》
  - 《[一曲琵琶動漢皇](../Page/一曲琵琶動漢皇.md "wikilink")》、《[寶劍重揮萬丈虹](../Page/寶劍重揮萬丈虹.md "wikilink")》（編劇：[潘一帆](../Page/潘一帆.md "wikilink")）
  - 《一代名花花濺淚》 唐滌生編撰
  - 《一枝紅艷露凝香》 唐滌生編撰
  - 《一入侯門深似海》 唐滌生編撰
  - 《[英烈劍中劍](../Page/英烈劍中劍.md "wikilink")》（編劇：[御香梵山](../Page/御香梵山.md "wikilink")）
  - 《[英雄叛國](../Page/英雄叛國.md "wikilink")》（編劇：[秦中英等](../Page/秦中英.md "wikilink")）
  - 《[胭脂巷口故人來](../Page/胭脂巷口故人來.md "wikilink")》（編劇:
    [唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[陰陽判](../Page/陰陽判.md "wikilink")》（編劇：《[楊智深](../Page/楊智深.md "wikilink")》）
  - 《[燕歸人未歸](../Page/燕歸人未歸.md "wikilink")》（編劇：[南海十三郎](../Page/南海十三郎.md "wikilink")）
  - 《[岳飛班師](../Page/岳飛班師.md "wikilink")》
  - 《[月下釋貂蟬](../Page/月下釋貂蟬.md "wikilink")》
  - 《[玉面參軍](../Page/玉面參軍.md "wikilink")》
  - 《[楊二舍化緣](../Page/楊二舍化緣.md "wikilink")》（編劇：[秦中英](../Page/秦中英.md "wikilink")）

## Z

  - 《[再世紅梅記](../Page/再世紅梅記.md "wikilink")》（編劇：[唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[張羽煮海](../Page/張羽煮海.md "wikilink")》（香港版本編劇：[楊智深](../Page/楊智深.md "wikilink")）
  - 《[醉打金枝](../Page/醉打金枝.md "wikilink")》（編劇：[蘇翁](../Page/蘇翁.md "wikilink")）
  - 《[醉打韓通](../Page/醉打韓通.md "wikilink")》
  - 《[醉斬鄭恩](../Page/醉斬鄭恩.md "wikilink")》（斬二王排場）
  - 《[章台柳](../Page/章台柳.md "wikilink")》（編劇：[蘇翁](../Page/蘇翁.md "wikilink")）
  - 《[摘纓會](../Page/摘纓會.md "wikilink")》（編劇：[蘇翁](../Page/蘇翁.md "wikilink")）
  - 《[賊王子巧遇情僧](../Page/賊王子巧遇情僧.md "wikilink")》
  - 《[紫釵記](../Page/紫釵記.md "wikilink")》（場次:
    燈街拾翠、花院盟香、陽關折柳、凍賣珠釵、吞釵拒婚、花前遇俠、劍合釵圓、節鎮宣恩/論理爭夫）（編劇:
    [唐滌生](../Page/唐滌生.md "wikilink")）
  - 《[紫鳳樓](../Page/紫鳳樓.md "wikilink")》（撰曲:
    [楊石渠](../Page/楊石渠.md "wikilink")）
  - 《[趙子龍催歸](../Page/趙子龍催歸.md "wikilink")》
  - 《[趙氏孤兒](../Page/趙氏孤兒.md "wikilink")》
  - 《[朱買臣](../Page/朱買臣.md "wikilink")》
  - 《[朱弁回朝](../Page/朱弁回朝.md "wikilink")》（內地版本編劇：[秦中英](../Page/秦中英.md "wikilink")，首演：[陳笑風](../Page/陳笑風.md "wikilink")，香港版本編劇：[葉紹德](../Page/葉紹德.md "wikilink")，首演：[林家聲](../Page/林家聲.md "wikilink")）
  - 《[周姑娘放腳](../Page/周姑娘放腳.md "wikilink")》
  - 《[昭君出塞](../Page/昭君出塞.md "wikilink")》（編劇：[楊子靜等](../Page/楊子靜.md "wikilink")）
  - 《[左慈戲曹](../Page/左慈戲曹.md "wikilink")》
  - 《[賊王子](../Page/賊王子.md "wikilink")》

### 羅家英主演的新編粵劇

  - 《[南宋鴛鴦鏡](../Page/南宋鴛鴦鏡.md "wikilink")》（編劇：[盧丹](../Page/盧丹.md "wikilink")）
  - 《[曹操與楊修](../Page/曹操與楊修.md "wikilink")》、《[三鎖玉郎心](../Page/三鎖玉郎心.md "wikilink")》、《[狀元打更](../Page/狀元打更.md "wikilink")》、《[糟糠情](../Page/糟糠情.md "wikilink")》（編劇：[秦中英](../Page/秦中英.md "wikilink")）
  - 《[英雄叛國](../Page/英雄叛國.md "wikilink")》（原著為莎士比亞名劇《[馬克白](../Page/馬克白.md "wikilink")》）（編劇：[羅家英](../Page/羅家英.md "wikilink")、[秦中英](../Page/秦中英.md "wikilink")、[溫誌鵬](../Page/溫誌鵬.md "wikilink")）\[1\]
  - 《[章台柳](../Page/章台柳.md "wikilink")》、《[活命金牌](../Page/活命金牌.md "wikilink")》（《[劈陵救母](../Page/劈陵救母.md "wikilink")》改編）、《[鐵馬銀婚](../Page/鐵馬銀婚.md "wikilink")》、《[狄青與襄陽公主](../Page/狄青與襄陽公主.md "wikilink")》、《[蟠龍令](../Page/蟠龍令.md "wikilink")》（編劇：[蘇翁](../Page/蘇翁.md "wikilink")）\[2\]

### 楊智深新編粵劇

  - 《[張羽煮海](../Page/張羽煮海.md "wikilink")》（首演：[龍貫天](../Page/龍貫天.md "wikilink")、[南鳳](../Page/南鳳.md "wikilink")）
  - 《[陰陽判](../Page/陰陽判.md "wikilink")》（首演：[龍貫天](../Page/龍貫天.md "wikilink")、[陳好逑](../Page/陳好逑.md "wikilink")、[尹飛燕](../Page/尹飛燕.md "wikilink")）
  - 《[桃花扇](../Page/桃花扇.md "wikilink")》（首演：[文千歲](../Page/文千歲.md "wikilink")、[陳好逑](../Page/陳好逑.md "wikilink")）
  - 《[乾坤鏡](../Page/乾坤鏡.md "wikilink")》（首演：[李龍](../Page/李龍.md "wikilink")、[尹飛燕](../Page/尹飛燕.md "wikilink")）

### 英語粵劇

  - 《[佳偶兵戎](../Page/佳偶兵戎.md "wikilink")》（編劇：[黃展華](../Page/黃展華.md "wikilink")，20世紀90年代，[謝雪心與一眾香港官員在](../Page/謝雪心.md "wikilink")[香港大會堂演出](../Page/香港大會堂.md "wikilink")。）
  - 《[清宮遺恨](../Page/清宮遺恨.md "wikilink")》、《[白蛇傳](../Page/白蛇传.md "wikilink")》之遊湖、《[醉打金枝](../Page/醉打金枝.md "wikilink")》、《[雅雀如何作鳳凰](../Page/雅雀如何作鳳凰.md "wikilink")》

### 馬來語粵劇

  - 《[拾玉镯](../Page/拾玉镯.md "wikilink")》

## 參考資料

<div class="references-small">

<references />

</div>

[\*](../Page/category:粵劇.md "wikilink")

1.  [香港中文大學音樂系戲曲資料中心：《英雄叛國》簡介](http://corp.mus.cuhk.edu.hk/Newsletter6/Hero.html)

2.  [大公報：名劇展演回顧蘇翁作品](http://www.takungpao.com/inc/print_me.asp?url=/news/2004-12-15/MF-341995.htm&date=2004-12-15)