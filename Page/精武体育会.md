**精武體操學校**是1910年由革命黨人倡議在上海所成立的一所小武館。由[農葝蓀](../Page/農葝蓀.md "wikilink")（又名農竹）任校長。[霍元甲被邀在任主要教員](../Page/霍元甲.md "wikilink")，可惜開學後3個月後即逝世。後來[霍元甲被塑造為](../Page/霍元甲.md "wikilink")[民族英雄](../Page/民族英雄.md "wikilink")，以宣傳反對日本及俄國的入侵意圖。

清末民初，由熱血年輕商人[陳公哲等人响应同盟会会员](../Page/陳公哲.md "wikilink")[陳其美提出的短期内训练出有强健体魄](../Page/陳其美.md "wikilink")，又有军事技能的青年计划而创立，後來發展至世界各地的**精武體育會**。

## 历史

### 精武体操會

1909年初、[陳其美與](../Page/陳其美.md "wikilink")[宋教仁](../Page/宋教仁.md "wikilink")、[譚人鳳](../Page/譚人鳳.md "wikilink")、[杨谱生等在上海](../Page/杨谱生.md "wikilink")[天宝栈组织江浙](../Page/天宝栈.md "wikilink")[同盟會](../Page/同盟會.md "wikilink")，从事革命活动。他们考虑到武装起义，需要大批军事人才。20岁的陳公哲響應計畫、與農葝蓀（竹）及[陳铁生等開始組織精武体操會](../Page/陳铁生.md "wikilink")。

1909年3月，[陳公哲與](../Page/陳公哲.md "wikilink")[農葝蓀邀请](../Page/農葝蓀.md "wikilink")[霍元甲和武術教師](../Page/霍元甲.md "wikilink")[劉振聲](../Page/劉振聲.md "wikilink")、[趙漢傑等從天津來到上海](../Page/趙漢傑.md "wikilink")。時值英國大力士[奧皮音在四川北路的阿波羅影院表演大力戲](../Page/奧皮音.md "wikilink")。展開了[霍元甲在静安寺旁的张园擂台比武事件](../Page/霍元甲.md "wikilink")。

1909年夏[霍元甲數人從](../Page/霍元甲.md "wikilink")[農葝蓀家移居到滬寧鐵路北站旱橋西一里許之闸北王家宅](../Page/農葝蓀.md "wikilink")（今交通路会文路附近），即精武體操會的校址。王家宅為舊式兩廂一廳設計，土堂瓦屋，外有院落，可作為操場，月租十四元。集資得來百多元，購置了幾件刀槍，隨來隨教，借收學費為生計。

[精武体操會于](../Page/精武体操會.md "wikilink")1910年6月1日开学，由[霍元甲出任总教习](../Page/霍元甲.md "wikilink")。[農葝蓀出任校长](../Page/農葝蓀.md "wikilink")。會的办学宗旨以“爱国、修身、正义、助人”作為精武精神。

早期教員除[霍元甲外](../Page/霍元甲.md "wikilink")，只有[劉振聲](../Page/劉振聲.md "wikilink")、[趙漢傑與](../Page/趙漢傑.md "wikilink")[張富猷數人](../Page/張富猷.md "wikilink")。由於[精武體操會成立不久](../Page/精武體操會.md "wikilink")，霍元甲去世，他親傳學生只有『[捻手拳](../Page/捻手拳.md "wikilink")』傳世。所教的有[撲打](../Page/撲打.md "wikilink")、[節拳](../Page/節拳.md "wikilink")、[潭腿及一般器械等](../Page/潭腿.md "wikilink")。[1](https://web.archive.org/web/20060113151442/http://www.fschinwoo.com/cw/chinwoo80.htm)

### 上海精武體育會

1916年[陳公哲与](../Page/陳公哲.md "wikilink")[姚蟾伯](../Page/姚蟾伯.md "wikilink")（江苏吴县人）、[卢炜昌](../Page/卢炜昌.md "wikilink")（中山人）在上海成立精武体育会，4月6日迁入倍开尔路73号（现杨浦区惠民路379弄）在[陈公哲宅地上建造的新會館](../Page/陈公哲.md "wikilink")，1921年[陈公哲又将隔壁倍开尔路](../Page/陈公哲.md "wikilink")75号的四层自有房屋献给精武体育会，1923年又在福德里建造精武中央大礼堂，即总会会所。他們是精武第一批学员，毕业于高级班。是精武會的核心领导，有“精武三公司”之称。

1916年11月5日，[孙中山先生亲自会见精武体育会的全体会员](../Page/孙中山.md "wikilink")。[孙中山曾于](../Page/孙中山.md "wikilink")1916年精武会举办6周年纪念会时，应其邀请亲临祝贺。

1919年，精武体育会在上海举行成立十周年纪念活动时，[孙中山先生亲筆题赠](../Page/孙中山.md "wikilink")“尚武精神”匾额，并担任该会的[名誉会长](../Page/名誉会长.md "wikilink")，还为该会特刊《精武本纪》撰写了序文。

[ChenGongZheHouse.jpg](https://zh.wikipedia.org/wiki/File:ChenGongZheHouse.jpg "fig:ChenGongZheHouse.jpg")捐献的住宅\]\]

### 基本十套

[精武体育会集合各家同源的观点](../Page/精武体育会.md "wikilink")，不争门户短长，熔各派武术。先后聘请了[黄河](../Page/黄河.md "wikilink")、[长江](../Page/长江.md "wikilink")、[珠江流域各派](../Page/珠江.md "wikilink")[武术名家于一堂](../Page/武术名家.md "wikilink")。编制了“[精武基本十套](../Page/精武基本十套.md "wikilink")”，成为精武武术的基本教材，即[潭腿](../Page/潭腿.md "wikilink")、[功力拳](../Page/功力拳.md "wikilink")、[节拳](../Page/节拳.md "wikilink")、[大战拳](../Page/大战拳.md "wikilink")、[套拳](../Page/套拳.md "wikilink")、[接潭腿](../Page/接潭腿.md "wikilink")、[单刀串枪](../Page/单刀串枪.md "wikilink")、[群羊棍](../Page/群羊棍.md "wikilink")、[八卦刀](../Page/八卦刀.md "wikilink")、[五虎枪](../Page/五虎枪.md "wikilink")。

學员“必须熟悉此十种方法及他技”。定期(每年或二年)举行运动会，进行“精武基本十套的集体大会操表演。

### 其他

精武体育会不以門派區分，而是以地域區分。分為黃河流域派、長江流域派及珠江流域派。

精武体育会促发了[中华民国期间](../Page/中华民国.md "wikilink")[中国](../Page/中国.md "wikilink")[武林各派的联合](../Page/武林.md "wikilink")，[全国武术协会](../Page/全国武术协会.md "wikilink")（National
Martial Arts Institutes）由此创立。

[抗战时期](../Page/抗战.md "wikilink")，精武体育会投身救亡运动，[第二次國共內戰时期和](../Page/第二次國共內戰.md "wikilink")[文革时期](../Page/文革.md "wikilink")，精武体育会曾经停办，后恢复。

1990年12月17日，上海精武体育会改名为[上海精武体育总会](http://www.chinwoo.org.cn)。

## 张园擂台與西洋力士

根據[陳公哲的記載說](../Page/陳公哲.md "wikilink")：-
當年有西洋力士[奥皮音來華在上海阿波罗影戏院表演大力戲](../Page/奥皮音.md "wikilink")，包括展露肌肉健美，表演舉重、拉車，靠神力賣藝，就像馬戲表演。革命黨人在報章宣傳說西洋力士指中國人是[東亞病夫](../Page/東亞病夫.md "wikilink")。於是[同盟会会员](../Page/同盟会.md "wikilink")[陈其美](../Page/陈其美.md "wikilink")、[农劲荪和成立精武体操会的五金行经理](../Page/农劲荪.md "wikilink")[陈公哲等人开会商讨](../Page/陈公哲.md "wikilink")，安排在上海靜安寺路之[张氏莼味园搭擂台](../Page/张氏莼味园.md "wikilink")，由当时在天津[農勁蓀所开药栈当伙计的](../Page/農勁蓀.md "wikilink")[霍元甲及武術教師](../Page/霍元甲.md "wikilink")[劉振聲主持](../Page/劉振聲.md "wikilink")，聲稱已經相約大力士[奧皮音來比武](../Page/奧皮音.md "wikilink")，但[奧皮音臨時爽約](../Page/奧皮音.md "wikilink")，於是提議在來賓中找人比賽。這時有個叫「東海趙」請纓上台，與\[\[霍元甲之徒\[\[劉振聲|霍元甲之徒[劉振聲交手](../Page/劉振聲.md "wikilink")，為劉打擊跌於地上。後有又「海門張」挑戰，[劉振聲復與他不分勝負](../Page/劉振聲.md "wikilink")。第二天[霍元甲親自登台比武](../Page/霍元甲.md "wikilink")，“右手执张臂，出左手揽其腰间，轻轻抱起，张某两足离地，霍将其置于地上”。不出一个回合，打敗了「海門張」。這日觀衆約有千人。擂台比武後，[霍元甲開始出名](../Page/霍元甲.md "wikilink")，後來被安排在『[精武體操學校](../Page/精武體操學校.md "wikilink")』任職主教席。

## 霍元甲逝世

精武体操学校开学仅三個月，[霍元甲即英年早逝](../Page/霍元甲.md "wikilink")。

[霍元甲原患有咯血病](../Page/霍元甲.md "wikilink")，在[農竹](../Page/農葝蓀.md "wikilink")（農葝蓀）家中居住時病徵不時發作。霍元甲自稱少年時候練習[氣功不得其法](../Page/氣功.md "wikilink")，閉氣太久導致體內氧氣耗盡，二氣化碳積聚使肺部微血管爆裂，遂形成咯血病症。又因曾咯血導致面色蠟黃，故有黃面虎之稱。

自遷入王家宅（精武體操會）後，霍先生病情急轉，送入新閘路中國[紅十字會醫院醫治兩星期後病逝](../Page/紅十字會.md "wikilink")，移厝於[河北會館](../Page/河北會館.md "wikilink")，時值1910年陰曆八月間。越一年運柩北返，精武會以「成仁取義」作挽。[霍元甲來滬前後僅六個月](../Page/霍元甲.md "wikilink")，享年四十三歲。

## 精武体育会与精武门

精武体育会的宗旨是弘扬中华[武术](../Page/武术.md "wikilink")，主张摒弃门户之见，因此精武体育会本身从来没有“精武门”的叫法。[2](https://web.archive.org/web/20060225050206/http://www.chinwoo.org.cn/news0.asp?id=18)

但社会上有将精武体育会称作精武门的，[李小龙曾主演电影](../Page/李小龙.md "wikilink")《[精武门](../Page/精武门_\(电影\).md "wikilink")》，使得这个名字更加广为传播。后来的影星们，例如[李连杰和](../Page/李连杰.md "wikilink")[甄子丹](../Page/甄子丹.md "wikilink")，也多次演出关于精武门的虚构的電影和電視劇，如佳藝電視的《精武門》、1994年的《[精武英雄](../Page/精武英雄.md "wikilink")》、1995年的《[精武門](../Page/精武門_\(電視劇\).md "wikilink")》、2010年的《[精武風雲·陳真](../Page/精武風雲·陳真.md "wikilink")》。

## 參见

  - [霍元甲](../Page/霍元甲.md "wikilink")
  - [陳真](../Page/陳真.md "wikilink")
  - [中國武術](../Page/中國武術.md "wikilink")
  - [中國武術門派](../Page/中國武術門派.md "wikilink")

## 參考書籍

  - 《中國近百年螳螂拳術史述論稿》，黃漢超著，香港天地圖書2002年出版。
  - 《精武50年武术发展史》，陳公哲1957年著。

## 外部链接

  - [上海精武体育总会](http://www.chinwoo.org.cn)
  - [馬來西亞雪隆精武體育會 Malaysia Chin Woo Athletic Association Selangor & Kuala
    Lumpur](http://www.chinwoo.org.my/cn/home.php)
  - [新加坡精武体育会Singapore Chin Woo Athletic
    Association](https://web.archive.org/web/20090723113515/http://sg.geocities.com/sg_chinwoo)
  - [Chin Woo
    Italia](http://www.chinwooitalia.altervista.org/index.html)
  - [World Chin Woo Federation](http://www.chinwoo.com)
  - [Italian Chin Woo Athletic Association](http://www.chinwoo.com)
  - [美國國家精武總會USA Chin Woo](http://www.chinwoo.com/USA)
  - [全球精武會索引Directory of World Chin Woo
    organizations](http://www.chinwoo.com/directory.htm)

[Category:中国武术组织](../Category/中国武术组织.md "wikilink")
[Category:1910年建立](../Category/1910年建立.md "wikilink")