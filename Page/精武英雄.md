是1994年拍摄的一部香港功夫片，[陈嘉上导演](../Page/陈嘉上.md "wikilink")，[袁和平任武术指导](../Page/袁和平.md "wikilink")，[李连杰主演](../Page/李连杰.md "wikilink")。

该片是[李小龙](../Page/李小龙.md "wikilink")1972年的经典影片《[精武门](../Page/精武門_\(電影\).md "wikilink")》的新版本，李连杰扮演影片中的男主角[陈真](../Page/陈真.md "wikilink")。几段精心设计的打鬥场面都相当精彩，激动人心，充分显示了他精湛的武术功底，为李连杰巅峰时期的经典之作。

## 剧情

一代武术宗师[霍元甲在与日本人比武时被人下毒身亡](../Page/霍元甲.md "wikilink")，其在日本留学的弟子陈真闻讯后返回中国，协助师父之子霍廷恩主持精武门的业务，不料却引起了他的妒忌，师兄弟之间引发权力之争。随着陈真对师傅死因的调查，隐藏在事件背后的更大的阴谋渐渐浮现出来……

## 主演

  - [李连杰](../Page/李连杰.md "wikilink") 饰 [陈真](../Page/陈真.md "wikilink")

  - [钱小豪](../Page/钱小豪.md "wikilink") 饰 霍廷恩

  - 饰 山田光子

  - [蔡少芬](../Page/蔡少芬.md "wikilink") 饰 晓红（素兰）

  - [倉田保昭](../Page/倉田保昭.md "wikilink") 饰 船越文夫

  - [周比利](../Page/周比利.md "wikilink") 饰 藤田冈

  - 饰 日本領事

  - [秦沛](../Page/秦沛.md "wikilink") 饰 [農勁蓀](../Page/農勁蓀.md "wikilink")

  - [黃新](../Page/黃新.md "wikilink") 饰 廚師

  - [彭子晴](../Page/彭子晴.md "wikilink") 飾 [陳真師妹](../Page/陳真.md "wikilink")

  - [樓學賢](../Page/樓學賢.md "wikilink") 飾 芥川龍一

  - [盧慶輝](../Page/盧慶輝.md "wikilink") 飾 虹口道場的武生

## 得獎紀錄

  - [第14屆香港電影金像獎](../Page/1995年香港電影金像獎.md "wikilink")

:\*入圍最佳動作指導 - [袁和平](../Page/袁和平.md "wikilink")

## 外部連結

  - {{@movies|fRcmb4043515}}

  -
  -
  -
  -
  -
  -
[Category:陳嘉上電影](../Category/陳嘉上電影.md "wikilink")
[4](../Category/1990年代香港電影作品.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:1990年代動作片](../Category/1990年代動作片.md "wikilink")
[Category:功夫片](../Category/功夫片.md "wikilink")
[Category:日本背景電影](../Category/日本背景電影.md "wikilink")
[Category:重拍电影](../Category/重拍电影.md "wikilink")