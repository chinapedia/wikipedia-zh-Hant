**糖廠街**是[香港](../Page/香港.md "wikilink")[東區的一條街道](../Page/東區_\(香港\).md "wikilink")，位於[鰂魚涌近](../Page/鰂魚涌.md "wikilink")[太古坊一帶](../Page/太古坊.md "wikilink")。由於該處[酒吧林立](../Page/酒吧.md "wikilink")，又有「**東區[蘭桂坊](../Page/蘭桂坊.md "wikilink")**」或「**小蘭桂坊**」\[1\]之稱。

糖廠街是一條T字型的道路，西接[英皇道](../Page/英皇道.md "wikilink")，北接[海堤街](../Page/海堤街.md "wikilink")，南接[濱海街](../Page/濱海街.md "wikilink")。該道路因昔日座落於當地的[太古糖廠而得名](../Page/太古糖廠.md "wikilink")。

## 參見

  - [銅鑼灣](../Page/銅鑼灣.md "wikilink")[糖街](../Page/糖街.md "wikilink")

## 環境

<File:Tong> Chong Street View1.jpg|糖廠街地下設有不少露天食肆 <File:1999年香港糖厂街太古坊>
TAIKOO PLACE, HK - panoramio.jpg|糖厂街 (1999年) <File:HK> Quarry Bay Taikoo
Place Tong Chong Street Allen Jones Sculptor City Shadow I
Security.JPG|[當代藝術](../Page/當代藝術.md "wikilink")[Allen
Jones作品](../Page/:en:Allen_Jones_\(sculptor\).md "wikilink")
<File:Tong> Chong Street and King's Road intersection (Hong
Kong).jpg|位於[英皇道](../Page/英皇道.md "wikilink")（左方）與糖廠街（右方）交界的英皇大樓

## 參考資料

<div class="references-small">

<references />

</div>

[Category:鰂魚涌街道](../Category/鰂魚涌街道.md "wikilink")

1.  [香港自遊樂在18區 - 東區](http://www.gohk.gov.hk/chi/tours/east.html)