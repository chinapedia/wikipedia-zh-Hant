《**糖果子彈 A Lollypop or A
Bullet**》是日本[直木賞作家](../Page/直木賞.md "wikilink")[櫻庭一樹的暗黑青春小說](../Page/櫻庭一樹.md "wikilink")。被譽為櫻庭一樹的原點之作。日本最初於2004年在[富士見Mystery文庫以](../Page/富士見Mystery文庫.md "wikilink")[輕小說形式出版文庫本](../Page/輕小說.md "wikilink")，插圖作者為むー。2007年2月由富士見書房發行刪除插圖的單行本，09又由角川書店再度以一般小說的形式文庫化。台灣與香港地區則由[台灣角川發行中文版](../Page/台灣角川.md "wikilink")。目前另外傳《暴君》、《脂肪遊戲》，收錄在[井上雅彦編纂的](../Page/井上雅彦.md "wikilink")《異形Collection》中，尚無中譯。

由[杉基鮭魚子作畫的漫畫版](../Page/杉基鮭魚子.md "wikilink")，\[1\]在日本的[月刊Dragon
Age](../Page/月刊Dragon_Age.md "wikilink")2007年2月號開始連載。隨後出版單行本，中文版由台灣角川代理，於2010年1月29日出版。

## 故事

住在偏僻鄉村的山田渚，是個「想早點變成大人」的女國中生。在她的學校中，有個自稱為人魚的少女，從東京轉學來的海野藻屑。當兩人的交往漸漸密切時，藻屑卻失蹤了……

## 登場人物

  -
    與哥哥和母親一起住在鄉村的少女。是個現實主義者，希望早點畢業能夠出社會。自稱所追求的是實彈。
  -
    山田渚的同班同學，是個轉學生。[僕娘](../Page/僕娘.md "wikilink")。自稱是[人魚](../Page/人魚.md "wikilink")，常常拿著礦泉水猛灌。被山田渚喻為糖果子彈。
  -
    山田渚的哥哥，是個絕世美少年與[隱蔽青年](../Page/隱蔽青年.md "wikilink")。渚稱他為貴族。能夠冷靜地旁觀各種事物。
  -
    海野藻屑的父親，雖然當年是個當紅歌手，但行為怪異、理念偏差。
  - 花名島正太
    山田渚的同班同學，對海野藻屑有好感的小平頭。
  - 映子
    山田渚的同班同學，好事，討人喜歡，好奇心旺盛。

## 已出版的書籍

### 小說版

  - 《糖果子彈 A Lollypop or A Bullet》ISBN 9861741623（中文）ISBN 4829162767（日文）
      - 第1章 我跟糖果子彈合不來
      - 第2章 我和糖果子彈獨處
      - 終章　再也見不到糖果子彈了

### 漫畫版

中文版由台灣角川代理。

  - 《糖果子彈 A Lollypop or A Bullet（上）》ISBN 4048541714 （日文）
  - 《糖果子彈 A Lollypop or A Bullet（下）》ISBN 4048541722 （日文）

## 參考資料

  - 《糖果子彈 A Lollypop or A Bullet》

[Category:日本小說](../Category/日本小說.md "wikilink")
[Category:富士見Mystery文庫](../Category/富士見Mystery文庫.md "wikilink")
[Category:月刊Dragon Age](../Category/月刊Dragon_Age.md "wikilink")
[Category:鳥取縣背景作品](../Category/鳥取縣背景作品.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")

1.