[Adenosin.svg](https://zh.wikipedia.org/wiki/File:Adenosin.svg "fig:Adenosin.svg")與[腺嘌呤之間的垂直線為配醣鍵](../Page/腺嘌呤.md "wikilink")。\]\]
[Glycoside_general.svg](https://zh.wikipedia.org/wiki/File:Glycoside_general.svg "fig:Glycoside_general.svg")作為糖的部分，糖苷键位於X元素與葡萄糖間\]\]
**糖苷键\[1\]**（，旧称**配糖键**）是指特定類型的[化學鍵](../Page/化學鍵.md "wikilink")，連接[糖苷分子中的非糖部分](../Page/糖苷.md "wikilink")（即[苷元](../Page/苷元.md "wikilink")）與糖基，或者糖基与糖基。含有配糖鍵的物質稱為[糖苷](../Page/糖苷.md "wikilink")（或配糖體）。

根據與糖基異頭碳原子相連的原子的不同，糖苷鍵一般可分為氧苷鍵、氮苷鍵、硫苷鍵和碳苷鍵等。右圖中[核糖與](../Page/核糖.md "wikilink")[腺嘌呤之間的糖苷鍵是氮苷鍵](../Page/腺嘌呤.md "wikilink")。

## 參見

  - [糖苷水解酶](../Page/糖苷水解酶.md "wikilink")（Glycoside hydrolases）

## 外部連結

  - [Definition of glycosides](http://goldbook.iupac.org/G02661.html),
    from the [IUPAC](../Page/IUPAC.md "wikilink") Compendium of Chemical
    Terminology, the "[Gold Book](../Page/Gold_Book.md "wikilink")"

## 參考文獻

  - Varki A *et al.* *Essentials of Glycobiology.* Cold Spring Harbor
    Laboratory Press; 1999.
    \[<http://www.ncbi.nlm.nih.gov/books/bv.fcgi?call=bv.View>..ShowTOC\&rid=glyco.TOC\&depth=10
    Searchable online version\]

[Category:化学键](../Category/化学键.md "wikilink")
[Category:糖苷](../Category/糖苷.md "wikilink")
[Category:糖类](../Category/糖类.md "wikilink")
[Category:糖化学](../Category/糖化学.md "wikilink")

1.  [1](http://term.gov.cn/pages/homepage/result2.jsp?id=333382&subid=10001798&subject=%E7%B3%96%E7%B1%BB&subsys=%E7%94%9F%E7%89%A9%E5%8C%96%E5%AD%A6%E4%B8%8E%E5%88%86%E5%AD%90%E7%94%9F%E7%89%A9%E5%AD%A6)