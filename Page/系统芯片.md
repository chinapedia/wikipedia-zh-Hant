**系统芯片**（，[縮寫](../Page/縮寫.md "wikilink")：**SoC**）是一个将[電腦或其他](../Page/電腦.md "wikilink")[电子](../Page/电子学.md "wikilink")[系统集成到单一芯片的](../Page/系统.md "wikilink")[集成电路](../Page/集成电路.md "wikilink")\[1\]。系统芯片可以处理[数字信号](../Page/数字信号.md "wikilink")、[模拟信号](../Page/模拟信号.md "wikilink")、[混合信号甚至更高频率的信号](../Page/混合信号集成电路.md "wikilink")。系统芯片常常應用在[嵌入式系统中](../Page/嵌入式系统.md "wikilink")\[2\]。系统芯片的集成规模很大，一般达到几百万门到几千万门。\[3\]

尽管微控制器通常只有不到100
kB的[随机存取存储器](../Page/随机存取存储器.md "wikilink")，但是事实上它是一种简易的、功能弱化的单芯片系统，而“系统芯片”这个术语常被用来指功能更加强大的处理器，这些处理器可以运行[Windows和](../Page/Microsoft_Windows.md "wikilink")[Linux的某些版本](../Page/Linux.md "wikilink")。系统芯片更强的功能要求它具备外部存储芯片，例如有的系统芯片配备了[闪存](../Page/闪存.md "wikilink")。系统芯片往往可以连接额外的[外部设备](../Page/外部设备.md "wikilink")。系统芯片对半导体器件的[集成规模提出了更高的要求](../Page/超大规模集成电路.md "wikilink")。为了更好地执行更复杂的任务，一些系统芯片采用了多个处理器核心。

## 基本架构

[ARMSoCBlockDiagram.png](https://zh.wikipedia.org/wiki/File:ARMSoCBlockDiagram.png "fig:ARMSoCBlockDiagram.png")
典型的系统芯片具有以下部分：

  - 至少一个[微控制器或](../Page/微控制器.md "wikilink")[微处理器](../Page/微处理器.md "wikilink")、[数字信号处理器](../Page/数字信号处理器.md "wikilink")，但是也可以有多个中央控制核心
  - [記憶體则可以是](../Page/記憶體.md "wikilink")[唯讀記憶體](../Page/唯讀記憶體.md "wikilink")、[随机存取存储器](../Page/随机存取存储器.md "wikilink")、[EEPROM和](../Page/EEPROM.md "wikilink")[闪存中的一种或多种](../Page/闪存.md "wikilink")
  - 用于提供[时间脉冲信号的](../Page/定時器訊號.md "wikilink")[振盪器和](../Page/振盪器.md "wikilink")[锁相环电路](../Page/锁相环.md "wikilink")
  - 由[计数器和计时器](../Page/计数器.md "wikilink")、电源电路组成的[外部设备](../Page/外部设备.md "wikilink")
  - 不同标准的[连线接口](../Page/电子连接器.md "wikilink")，如[通用串行总线](../Page/通用串行总线.md "wikilink")、[火线](../Page/IEEE_1394.md "wikilink")、[以太网](../Page/以太网.md "wikilink")、[通用异步收发和](../Page/UART.md "wikilink")[序列周邊介面等](../Page/序列周邊介面.md "wikilink")
  - 用于在[数字信号和](../Page/数字信号.md "wikilink")[模拟信号之间转换的](../Page/模拟信号.md "wikilink")[模拟数字转换器和](../Page/模拟数字转换器.md "wikilink")[数字模拟转换器](../Page/数字模拟转换器.md "wikilink")
  - 电压调理电路以及[稳压器](../Page/稳压器.md "wikilink")

数据的流动主要借助了系统中的[I/O总线](../Page/I/O总线.md "wikilink")，例如[安謀國際科技公司的](../Page/安謀國際科技.md "wikilink")[高级微控制器总线架构](../Page/高级微控制器总线架构.md "wikilink")。采用[DMA控制器](../Page/直接記憶體存取.md "wikilink")，则可以使得外部数据直接被传送到存储器，无需经过[中央处理器](../Page/中央处理器.md "wikilink")，这可以大大改善数据吞吐的效率。

## 设计流程

[SoCDesignFlow.svg](https://zh.wikipedia.org/wiki/File:SoCDesignFlow.svg "fig:SoCDesignFlow.svg")
一个完整系统芯片由硬件和软件两部分组成，其中软件用于控制硬件部分的[控制器](../Page/微控制器.md "wikilink")、[微处理器或](../Page/微处理器.md "wikilink")[数字信号处理器核心以及](../Page/数字信号处理器.md "wikilink")[外部设备和接口](../Page/外部设备.md "wikilink")。系统芯片的设计流程主要是其硬件和软件的设计。

由于系统芯片的集成度已经达到数百万门，工程师必须尽可能采取可重用的设计思路。大部分的系统芯片都使用了预定义的半导体知识产权核（[IP核](../Page/IP核.md "wikilink")，包括软核、硬核和固核），以可重用设计的方式来完成快速设计。与以往的[集成电路设计相比](../Page/集成电路设计.md "wikilink")，可重用设计要求设计人员的工作更加标准化，例如规范的代码书写风格等等。设计人员需要关注硬件[驱动程序的实现](../Page/驱动程序.md "wikilink")，从而实现具体的功能。[协议栈是一个重要的概念](../Page/协议栈.md "wikilink")，它与诸如[通用串行总线的接口的工业标准有关](../Page/通用串行总线.md "wikilink")。设计人员通常使用[计算机辅助工程工具来把已经设计](../Page/计算机辅助工程.md "wikilink")（或者购买）的核连接在一起，这时[集成开发环境可以被用来整合包含不同子功能的模块](../Page/集成开发环境.md "wikilink")。

设计的芯片在被送到工厂进行[硬件工艺制造之前](../Page/半导体器件制造.md "wikilink")，设计人员会采取不同方式对芯片的逻辑功能进行[验证](../Page/功能验证.md "wikilink")。功能验证的重要性丝毫不亚于[集成电路设计](../Page/集成电路设计.md "wikilink")，对于现代的超大规模集成电路，这一步骤在整个设计周期中将花费相当的时间和金錢。\[4\]为了应对芯片极高的复杂程度，类似[SystemVerilog](../Page/SystemVerilog.md "wikilink")、[SystemC](../Page/SystemC.md "wikilink")、e验证语言和[OpenVera的](../Page/OpenVera.md "wikilink")[硬件验证语言逐渐变得流行](../Page/硬件验证语言.md "wikilink")。在验证阶段，系统软件的[程序错误可以被反馈到设计人员那里](../Page/程序错误.md "wikilink")，以便进行针对性的修正。

工程师通常会使用精心设计的[仿真器或者在通用的](../Page/仿真器.md "wikilink")[现场可编程逻辑门阵列](../Page/现场可编程逻辑门阵列.md "wikilink")（FPGA）上运行程序，来测试之前进行的系统级、行为级（或用另一个术语[寄存器传输级](../Page/寄存器传输级.md "wikilink")，即RTL）的设计代码，这一步的目的是在设计项目在进行最后的硬件生产（[投片](../Page/下線.md "wikilink")）之前，其软、硬件的功能、性能得到最后的确认，并改正所有功能、时序、功耗上的错误。

其中，使用现场可编程逻辑门阵列构建产品原型的工作方式可以让工程师评估、测试各种刺激（stimulus）施加在系统时，系统的运行状态。相关的[电子设计自动化工具包括Certus](../Page/电子设计自动化.md "wikilink")\[5\]，它可以被用来分析、检测系统设计的寄存器传输级代码，监视其中的变量和信号在整个运行过程中的变化。

在功能验证过程结束之后，工程师还会采取[计算机辅助工程的方式完成](../Page/计算机辅助工程.md "wikilink")[布局](../Page/布局_\(集成电路\).md "wikilink")、[布线流程](../Page/布线_\(集成电路\).md "wikilink")，这一步他们需要关注何种布局布线方式可以尽可能地减少连线之间的信号干扰和延迟，功率也是另外一个考虑的重点。

## 参考文献

## 相关条目

  - [集成电路设计](../Page/集成电路设计.md "wikilink")
  - [电子设计自动化](../Page/电子设计自动化.md "wikilink")
  - [集成电路](../Page/集成电路.md "wikilink")、[超大规模集成电路](../Page/超大规模集成电路.md "wikilink")
  - [嵌入式系统](../Page/嵌入式系统.md "wikilink")
  - [Raspberry Pi](../Page/Raspberry_Pi.md "wikilink")

[Category:電子設計](../Category/電子設計.md "wikilink")
[Category:微技术](../Category/微技术.md "wikilink")
[系統單晶片](../Category/系統單晶片.md "wikilink")
[Raspberry_Pi](../Category/Raspberry_Pi.md "wikilink")

1.
2.
3.
4.
5.