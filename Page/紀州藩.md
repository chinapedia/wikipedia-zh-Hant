**紀州藩**（）是日本[江戶時代的一個](../Page/江戶時代.md "wikilink")[藩](../Page/藩.md "wikilink")，又稱為**紀伊藩**、**和歌山藩**。位於[紀伊國](../Page/紀伊國.md "wikilink")，藩廳是[和歌山城](../Page/和歌山城.md "wikilink")（今[和歌山縣](../Page/和歌山縣.md "wikilink")[和歌山市](../Page/和歌山市.md "wikilink")）。藩主是[紀州德川家](../Page/紀州德川家.md "wikilink")，與[水戶藩及](../Page/水戶藩.md "wikilink")[尾張藩合稱御三家](../Page/尾張藩.md "wikilink")，[石高](../Page/石高.md "wikilink")55萬石。

## 藩史

[Kioi-Kii-Hyoushiki.JPG](https://zh.wikipedia.org/wiki/File:Kioi-Kii-Hyoushiki.JPG "fig:Kioi-Kii-Hyoushiki.JPG")。\]\]
[Wakayamajyoudaitensyu.JPG](https://zh.wikipedia.org/wiki/File:Wakayamajyoudaitensyu.JPG "fig:Wakayamajyoudaitensyu.JPG")\]\]

安土桃山時代，由[桑山氏管治](../Page/桑山氏.md "wikilink")，1601年被移封到大和國[新庄藩](../Page/新庄藩.md "wikilink")。紀州藩最初的藩主是[淺野氏](../Page/淺野氏.md "wikilink")，淺野氏統治期間，曾經在[大坂之役期間爆發](../Page/大坂之役.md "wikilink")[紀州一揆](../Page/紀州一揆.md "wikilink")，淺野軍採取措施鎮壓\[1\]。1619年，[廣島藩的](../Page/廣島藩.md "wikilink")[福島正則因得罪幕府而移封到其他領地](../Page/福島正則.md "wikilink")，淺野氏[長晟移封到廣島藩](../Page/淺野長晟.md "wikilink")，新入藩的是[家康十男](../Page/德川家康.md "wikilink")[賴宣](../Page/德川賴宣.md "wikilink")，石高是55萬5千石。

賴宣著手進行領地改革招募不少浪人為藩工作，改建和歌山城，解決浪人過剩的問題，又實行地士制度，對[國人實行懷柔政策](../Page/國人.md "wikilink")。1651年[由井正雪曾偽造賴宣的文書](../Page/由井正雪.md "wikilink")，但被幕府發現，正雪最後被處死。

在第三任藩主[綱教和第四任藩主](../Page/德川綱教.md "wikilink")[賴織先後上任不久而過身](../Page/德川賴織.md "wikilink")。由[賴方上任](../Page/德川吉宗.md "wikilink")，上任後改名為吉宗。吉宗再次整頓領地，簡化藩政、將10萬兩葬儀綱教及賴織的費用還給幕府\[2\]，停止發行藩札等措施。1716年，第七任將軍[家繼](../Page/德川家繼.md "wikilink")8歲病逝，幕府失去了家康的直系子孫（不過[館林藩主](../Page/館林藩.md "wikilink")[松平清武是家康直系子孫](../Page/松平清武.md "wikilink")，但因為年老而且本人無意成為將軍），幕府決定讓吉宗成為幕府將軍。

第八代藩主[重倫](../Page/德川重倫.md "wikilink")30歲被幕府革職，被迫隱居。第九代藩主[治貞實行節儉政策](../Page/德川治貞.md "wikilink")，與[熊本藩的](../Page/熊本藩.md "wikilink")[細川重賢共稱為](../Page/細川重賢.md "wikilink")「**紀州的麒麟**、肥後的鳳凰」\[3\]。

第十三代藩主[慶福](../Page/德川家茂.md "wikilink")（後來改名為家茂）與[一橋派的鬥爭取得勝利](../Page/一橋派.md "wikilink")，成為了幕府第十四代將軍。江戶幕府滅亡後，藩士[津田出及](../Page/津田出.md "wikilink")[陸奧宗光實行藩政改革](../Page/陸奧宗光.md "wikilink")，構思徵兵令，設置軍務局。影響了明治政府的徵兵方針。

1871年[廢藩置縣時成為了](../Page/廢藩置縣.md "wikilink")[和歌山縣](../Page/和歌山縣.md "wikilink")，紀伊國東部部份領地成為三重縣的領地。而藩主/知事[德川茂承獲明治政府頒發侯爵](../Page/德川茂承.md "wikilink")。

## 藩主

### 淺野家

外樣37萬6千石

1.  [淺野幸長](../Page/淺野幸長.md "wikilink")（1601年-1613年）
2.  [淺野長晟](../Page/淺野長晟.md "wikilink")（1613年-1619年）

### 紀州德川家

親藩55萬5千石

1.  [德川賴宣](../Page/德川賴宣.md "wikilink")（1619年-1667年）
2.  [德川光貞](../Page/德川光貞.md "wikilink")（1667年-1698年）
3.  [德川綱教](../Page/德川綱教.md "wikilink")（1698年-1705年）
4.  [德川賴職](../Page/德川賴職.md "wikilink")（1705年）
5.  [德川吉宗](../Page/德川吉宗.md "wikilink")（1705年-1716年）
6.  [德川宗直](../Page/德川宗直.md "wikilink")（1716年-1757年）
7.  [德川宗將](../Page/德川宗將.md "wikilink")（1757年-1765年）
8.  [德川重倫](../Page/德川重倫.md "wikilink")（1765年-1775年）
9.  [德川治貞](../Page/德川治貞.md "wikilink")（1775年-1789年）
10. [德川治寶](../Page/德川治寶.md "wikilink")（1789年-1824年）
11. [德川齊順](../Page/德川齊順.md "wikilink")（1824年-1846年）
12. [德川齊彊](../Page/德川齊彊.md "wikilink")（1846年-1849年）
13. [德川慶福](../Page/德川慶福.md "wikilink")（1849年-1858年）
14. [德川茂承](../Page/德川茂承.md "wikilink")（1858年-1869年）

## 支城

  - [松坂城](../Page/松坂城.md "wikilink")
  - [田丸城](../Page/田丸城.md "wikilink")

## 家老

  - 附家老

<!-- end list -->

  -
    [安藤家](../Page/三河安藤氏.md "wikilink")（1868年設立[紀伊田邊藩獨立](../Page/紀伊田邊藩.md "wikilink")）
    [水野家](../Page/水野氏.md "wikilink")（1868年設立[紀伊新宮藩獨立](../Page/紀伊新宮藩.md "wikilink")）

<!-- end list -->

  - 城代家老

<!-- end list -->

  -
    [久野氏](../Page/久野氏.md "wikilink")

<!-- end list -->

  - 一門家老

<!-- end list -->

  -
    [安房正木氏](../Page/安房正木氏.md "wikilink")

## 支藩

  - [伊予國](../Page/伊予國.md "wikilink")[西條藩](../Page/西條藩.md "wikilink")：初代藩主是[德川賴宣三男](../Page/德川賴宣.md "wikilink")[賴純](../Page/松平賴純.md "wikilink")。

## 參考資料

<div class="references-small">

參考：

<references />

連結：

1.  [和歌山藩](http://www.asahi-net.or.jp/~me4k-skri/han/kinki/wakayama.html)

</div>

[Category:藩](../Category/藩.md "wikilink")
[\*](../Category/紀州藩.md "wikilink")
[Category:紀伊國](../Category/紀伊國.md "wikilink")
[Category:淺野氏](../Category/淺野氏.md "wikilink")
[Category:紀州德川家](../Category/紀州德川家.md "wikilink")

1.  [紀州一揆](http://tikugo.cool.ne.jp/osaka/kassen/kishu-k.html)
2.  [德川吉宗](http://www.geocities.jp/masu741223jp/history6.htm)
3.  [九代治貞](http://www.chohoji.or.jp/tokugawa/han-9.htm)