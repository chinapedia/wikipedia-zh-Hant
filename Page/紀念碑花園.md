**紀念碑花園**（），是位於[澳門](../Page/澳門.md "wikilink")[氹仔的](../Page/氹仔.md "wikilink")[花園](../Page/公園.md "wikilink")。公園內豎立了一個紀念碑，紀念葡萄牙戰船「第二瑪琍亞」號(戰船葡文名稱"D.
Maria II",
即"[瑪麗亞二世](../Page/瑪麗亞二世.md "wikilink")")爆炸事件之罹難人士。顧名思義，這個花園是因此紀念碑而得名。

## 地理位置

紀念碑花園位於澳門氹仔的西北馬路和盧廉若馬路交界，依山而建，面積約只有800平方公尺。鄰近為[氹仔隧道](../Page/氹仔隧道.md "wikilink")，在[氹仔炮台旁邊](../Page/氹仔炮台.md "wikilink")。

## 歷史

[Jardim_do_Monumento2.jpg](https://zh.wikipedia.org/wiki/File:Jardim_do_Monumento2.jpg "fig:Jardim_do_Monumento2.jpg")
[Jardim_do_Monumento3.JPG](https://zh.wikipedia.org/wiki/File:Jardim_do_Monumento3.JPG "fig:Jardim_do_Monumento3.JPG")
紀念碑花園由[海島市市政廳興建](../Page/海島市市政廳.md "wikilink")，於1991年3月正式開放。紀念碑建於1851年，乃紀念葡萄牙戰船「第二瑪麗亞」號爆炸事件中的罹難者。

爆炸事件發生於1850年10月29日，當日凌晨2時，葡萄牙戰船「瑪麗亞二世」[巡洋艦從](../Page/巡洋艦.md "wikilink")[葡萄牙駛至澳門停泊在](../Page/葡萄牙.md "wikilink")[氹仔碼頭](../Page/氹仔碼頭.md "wikilink")。當時葡萄牙人正慶祝[葡萄牙女王](../Page/葡萄牙君主.md "wikilink")[瑪麗亞二世誕辰紀念日](../Page/瑪麗亞二世.md "wikilink")，戰船突然在一聲巨響中被炸得粉碎，附近停泊的船隻也同受波及，釀成231人喪生\[1\]，包括190名[水手](../Page/水手.md "wikilink")、船長及約40名在船上觀禮的[中國人](../Page/中國人.md "wikilink")，約90人生還，至今仍為澳門史上最多人命傷亡的爆炸事件。

## 參考文獻

[Category:澳門公園](../Category/澳門公園.md "wikilink")
[Category:澳門康樂設施](../Category/澳門康樂設施.md "wikilink")

1.