[Josiah_Warren.jpg](https://zh.wikipedia.org/wiki/File:Josiah_Warren.jpg "fig:Josiah_Warren.jpg")
**約書亞·沃倫**（**Josiah
Warren**，）是[美國的](../Page/美國.md "wikilink")[個人無政府主義者](../Page/個人無政府主義.md "wikilink")、[發明家](../Page/發明家.md "wikilink")、和[作家](../Page/作家.md "wikilink")。他被廣泛視為是美國的第一名[無政府主義者](../Page/無政府主義.md "wikilink")，而他在1833年開始出版的期刊《The
Peaceful
Revolutionist》則是第一本由無政府主義者定期出版的期刊。他被[班傑明·塔克稱為是第一名批評](../Page/班傑明·塔克.md "wikilink")[資本主義的](../Page/資本主義.md "wikilink")[社會主義始祖](../Page/社會主義.md "wikilink")（值得注意的是，當時「社會主義」一詞的涵義與現代所指反私人財產的社會主義完全不同）[1](http://praxeology.net/BT-SSA.htm)，選擇以[無政府和個人主義的方式來對抗獨裁主義形式的社會主義](../Page/無政府.md "wikilink")。塔克在他的論文集《Instead
of a
Book》中紀念沃倫道：「我的朋友和老師…他的教導成為我生命中所見的第一盞光明」。塔克稱沃倫是「第一名提倡並且公式化了無政府主義的原則的人。」\[1\]

在1825年，沃伦參與了[罗伯特·欧文所領導的移民社區實驗](../Page/罗伯特·欧文.md "wikilink")，試圖建立一個和諧的集體主義社區，這個實驗後來以失敗告終。在他對於實驗失敗的檢討結論中，他激烈的主張應該改採個人主義和私人財產的制度，並且大力提倡個人的[消極自由](../Page/两种自由概念.md "wikilink")：

沃倫宣稱：「只要去除了政府，我們便排除了對於人類權利的最大威脅。」\[2\]

沃倫也創造了「成本即為價格限制」這一名言，他認為將一件商品的價格調高超過生產的成本是不道德的。他依據這個理論創辦了一個實驗性的商店，稱之為「辛辛那提時代商店」（Cincinnati
Time
Store），以協助勞動的證明票證作為貨幣進行貿易。這個實驗商店最後證明相當成功並且持續營業了三年，直到後來沃倫進一步創建了更大的互助主義實驗性社區為止。

## 参考文献

## 參見

  - [無政府主義](../Page/無政府主義.md "wikilink")
  - [個人無政府主義](../Page/個人無政府主義.md "wikilink")
  - [美國個人無政府主義](../Page/美國個人無政府主義.md "wikilink")
  - [互助主義](../Page/互助主義.md "wikilink")
  - [班傑明·塔克](../Page/班傑明·塔克.md "wikilink")

## 外部連結

  - [沃倫的兒子George W.
    Warren所著的傳記](http://faculty.evansville.edu/ck6/bstud/warren.html)
  - \[[http://raforum.apinc.org/article.php3?id_article=169約書亞·沃倫的"宣言](http://raforum.apinc.org/article.php3?id_article=169約書亞·沃倫的%22宣言)"\]
  - [*Equitable Commerce* 由
    約書亞·沃倫](https://web.archive.org/web/20040719123958/http://www.blancmange.net/tmh/pdf/jwarren.pdf)
  - [*Plan of the Cincinnati Labor for Labor Store* 由
    約書亞·沃倫](https://web.archive.org/web/20020825042655/http://www.crispinsartwell.com/warren.htm)
  - [*True Civilization* 由
    約書亞·沃倫](http://dwardmac.pitzer.edu/anarchist_archives/bright/warren/truecivtoc.html)
  - [*Josiah Warren: The First American Anarchist*, 由 William
    Bailie](https://web.archive.org/web/20120204155505/http://libertarian-labyrinth.org/warren/1stAmAnarch.pdf)
  - ['' Josiah Warren and the Sovereignty of the Individual'' 由 Ann
    Caldwell Butler](http://www.mises.org/journals/jls/4_4/4_4_8.pdf)
  - [2](https://web.archive.org/web/20050205061103/http://www.blackcrayon.com/people/warren/)
  - [3](http://www.famousamericans.net/josiahwarren/)

{{-}}

[約書亞·沃倫](../Category/無政府主義者.md "wikilink")
[約書亞·沃倫](../Category/個人無政府主義者.md "wikilink")
[約書亞·沃倫](../Category/美国哲学家.md "wikilink")
[Category:罹患水腫病逝世者](../Category/罹患水腫病逝世者.md "wikilink")

1.  (Liberty XIV (December, 1900):1)
2.  (Equitable Commerce)