[Joseph_Leidy.jpg](https://zh.wikipedia.org/wiki/File:Joseph_Leidy.jpg "fig:Joseph_Leidy.jpg")
**約瑟夫·萊迪**（）是一位美国[古生物学家](../Page/古生物学.md "wikilink")。

## 生平

萊迪首先任[宾夕法尼亚大学](../Page/宾夕法尼亚大学.md "wikilink")[解剖学教授](../Page/解剖学.md "wikilink")，后来任[斯沃斯莫尔学院](../Page/斯沃斯莫尔学院.md "wikilink")[自然历史教授](../Page/自然历史.md "wikilink")。在他1869年发表的书《》（达科塔州和内布拉斯加州灭绝动物）中他描写了许多[北美洲灭绝的种类](../Page/北美洲.md "wikilink")。

比如他描写了[威廉·帕克·福克尔在](../Page/威廉·帕克·福克尔.md "wikilink")[新澤西州找到的第一具几乎完整的](../Page/新澤西州.md "wikilink")[恐龙骨骼并将它命名为](../Page/恐龙.md "wikilink")[鸭嘴龙](../Page/鸭嘴龙.md "wikilink")。

萊迪在[寄生虫学中也有所贡献](../Page/寄生虫学.md "wikilink")。1846年他认识到[旋毛虫病是由生肉或者没有煮熟的肉中的](../Page/旋毛虫病.md "wikilink")[寄生虫](../Page/寄生虫.md "wikilink")[旋毛虫导致的](../Page/旋毛虫.md "wikilink")。

萊迪在[查尔斯·达尔文之前就已经怀疑环境对生物有影响](../Page/查尔斯·达尔文.md "wikilink")。达尔文发表了他的理论后萊迪立刻就成为达尔文的支持者。因此他的批评者说他是一个[无神论者](../Page/无神论.md "wikilink")。

1879年他发表了《》（北美洲淡水根足类动物）

## 外部链接

  - Leidy was the first president of the [American Association of
    Anatomists](http://www.anatomy.org) (1888-1889)

  -
  - Chapman Henry C. 1891. [*Memoir of Joseph Leidy, M.D.,
    LL.D.*](https://archive.org/details/memoirofjosephle00chaprich)
    Proceedings of the Academy of Natural Sciences of America, Academy
    of Natural Sciences, Philadelphia.

  - Persifor Frazer. 1892 (January). [*Joseph Leidy, M.D.,
    LL.D*](https://archive.org/details/josephleidymdlld00frazrich). The
    American Geologist, Philadelphia.

  - [The Joseph Leidy on-line exhibit at the Academy of Natural
    Sciences](https://web.archive.org/web/20061008183044/http://www.ansp.org/museum/leidy/index.php)

  - [The Joseph Leidy Microscopy
    Portal](http://www.xmission.com/~psneeley/Personal/Leidy.htm)

  -
  - View works by [Joseph
    Leidy](http://biodiversitylibrary.org/creator/1271) online at the
    Biodiversity Heritage Library.

  - [National Academy of Sciences Biographical
    Memoir](http://www.nasonline.org/publications/biographical-memoirs/memoir-pdfs/leidy-joseph.pdf)

[Category:美国古生物学家](../Category/美国古生物学家.md "wikilink")
[Category:美国法医科学家](../Category/美国法医科学家.md "wikilink")
[Category:德国裔美国人](../Category/德国裔美国人.md "wikilink")
[Category:科学插画家](../Category/科学插画家.md "wikilink")
[Category:宾夕法尼亚大学校友](../Category/宾夕法尼亚大学校友.md "wikilink")
[Category:宾夕法尼亚大学教师](../Category/宾夕法尼亚大学教师.md "wikilink")
[Category:斯沃斯莫尔学院教师](../Category/斯沃斯莫尔学院教师.md "wikilink")
[Category:费城人](../Category/费城人.md "wikilink")