**約翰內斯·布拉斯科維茨**（[德语](../Page/德语.md "wikilink")：**Johannes
Blaskowitz**，)[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[納粹德國的一名](../Page/納粹德國.md "wikilink")[大將](../Page/大將.md "wikilink")，曾經勸阻[希特勒勿濫殺](../Page/希特勒.md "wikilink")[猶太人導致仕途不順](../Page/猶太人.md "wikilink")；後出任西戰場駐法國G集團軍司令，[諾曼底登陸登陸後戰敗被革職](../Page/諾曼底登陸.md "wikilink")，於1944年8月底再派任為駐[荷蘭司令官](../Page/荷蘭.md "wikilink")，1945年3月「德國保衛戰」之[魯爾突圍戰受](../Page/魯爾突圍戰.md "wikilink")[莫德爾元帥指揮領導](../Page/莫德爾.md "wikilink")，卻早在3月中旬領軍7萬自「魯登道夫大橋」先行逃逸，導致[瓦爾特·莫德爾在西線陷入無退路孤軍奮戰到自盡](../Page/瓦爾特·莫德爾.md "wikilink")；戰後以「戰犯」罪名問審，卻未留遺囑自盡獄裡，部份史家相信他是被[親衛隊](../Page/親衛隊.md "wikilink")[暗殺](../Page/暗殺.md "wikilink")。

## 早年

布拉斯科維茨誕生於东普鲁士的Wehlau（魏劳，今[茲納緬斯克](../Page/茲納緬斯克.md "wikilink"))，是一位路德教派牧師的兒子；

1894年他到科斯林（現在波蘭[科沙林](../Page/科沙林.md "wikilink")）唸軍官學校，後來又到[柏林著名的正統](../Page/柏林.md "wikilink")「[普魯士軍官團](../Page/普魯士.md "wikilink")」搖籃「大光野」軍校[Berlin
Lichterfelde](../Page/Berlin_Lichterfelde.md "wikilink")；
1899年他開始他的戰場生涯於[東普魯士](../Page/東普魯士.md "wikilink")(今天[波蘭](../Page/波蘭.md "wikilink"))的[奧斯特魯達當陸軍團長](../Page/奧斯特魯達.md "wikilink")。

[第一次世界大戰爆發](../Page/第一次世界大戰.md "wikilink")，他先在東戰場作戰，後又改派西戰場，最後到德國「參謀本部」當參謀軍官；戰爭結束[威瑪共和時代他繼續在](../Page/威瑪共和.md "wikilink")「德國國防軍」內任職。

## 波蘭

第二次世界大戰開始1939年[波蘭戰役](../Page/波蘭戰役.md "wikilink")，他是德軍第8軍軍長，後於1939年10月20日升任為德軍駐波蘭總司令；
(1940年1月「回憶錄」)

布拉斯科維茨口無遮攔、形同天真無畏的反政策批評流至[希特勒耳邊也激怒了希特勒](../Page/希特勒.md "wikilink")；1940年5月14日他在波蘭統治者身份被希特勒拉掉，換成黨衛軍大將[漢斯·法郎克](../Page/漢斯·法郎克.md "wikilink")。

## 1944年後「西戰場」作戰

他發表聲明：抗議[親衛隊在波蘭殘害](../Page/親衛隊.md "wikilink")[猶太人的暴行](../Page/猶太人.md "wikilink")，隨即在1944年9月末德軍G集團軍群司令職位被撤職；但是又在同年12月24日復職。1945年1月28日他被改派為德軍H集團軍群司令，此集團軍群於1945年4月改編組為德軍駐荷蘭集團軍群。

## 戰後

布拉斯科維茨被美軍逮捕，並準備接受[紐倫堡審判](../Page/紐倫堡審判.md "wikilink")，卻在1948年2月5日在獄中自裁身亡，一般認為是被混入獄中的[親衛隊謀殺](../Page/親衛隊.md "wikilink")；惟此說尚待證實。

## 陸軍生涯

### 歷任官階

  - 預官: 3月 02, 1901
  - [少尉](../Page/少尉.md "wikilink"): 1月 27, 1902
  - [中尉](../Page/中尉.md "wikilink"): 1月 27, 1910
  - [上尉](../Page/上尉.md "wikilink"): 2月 17, 1914
  - [少校](../Page/少校.md "wikilink"): 2月 01, 1922
  - [中校](../Page/中校.md "wikilink"): 4月 06, 1926
  - [上校](../Page/上校.md "wikilink"): 10月 01, 1929
  - [少將](../Page/少將.md "wikilink"): 10月 01, 1932
  - [中將](../Page/中將.md "wikilink"): 12月 01, 1933
  - [上將](../Page/上將.md "wikilink"): 8月 01, 1936
  - [大將](../Page/大將.md "wikilink"): 10月 01, 1939

### 軍人獎勳章

  - [鐵十字勳章](../Page/鐵十字勳章.md "wikilink") 二級 (1914) 一級 (1915)
  - [銀質交棒扣鐵十字勳章](../Page/鐵十字勳章.md "wikilink") 二級(1939) 一級(1939)
  - [騎士鐵十字勳章](../Page/鐵十字勳章.md "wikilink") (1939)
  - [橡葉騎士鐵十字勳章](../Page/鐵十字勳章.md "wikilink") (1944)
  - [橡葉騎士帶寶劍鐵十字勳章](../Page/鐵十字勳章.md "wikilink") (1945)
  - 「Sudetenland 勳章」 (1938) with [Prague Castle
    bar](../Page/Sudetenland_Medal.md "wikilink") (1938)
  - 「[重傷勳章](../Page/重傷勳章.md "wikilink")」- 1918款式 (?)
  - 光榮十字勳章 (1934)
  - [巴伐利亞之戰功十字勳章](../Page/巴伐利亞.md "wikilink") 佩劍第4級 (1916)
  - 「德國十字勳章」銀質 (1943)
  - [普魯士騎士十字勳章](../Page/普魯士.md "wikilink") of the [Royal House Order of
    Hohenzollern佩寶劍](../Page/Royal_House_Order_of_Hohenzollern.md "wikilink")(1917)
  - [巴登騎士十字勳章二級of](../Page/巴登.md "wikilink") the [Order of the Zähringen
    Lion](../Page/Order_of_the_Zähringen_Lion.md "wikilink") with
    Swords(1915)
  - [Oldenburg](../Page/Oldenburg.md "wikilink") [Friedrich August
    Cross](../Page/Friedrich_August_Cross.md "wikilink") Second (1916)
    and First (1916) Classes
  - 戰功勳章 二級 (?) 一級(?)
  - 「義大利皇冠勳章」大十字 (1941)
  - 「新德軍」讚賞獎章 (1939年9月27日)

## 參閱

  - [波希米亞與](../Page/波希米亞.md "wikilink")[摩拉維亞統治者名冊](../Page/摩拉維亞.md "wikilink")

## 注釋

<div class="references-small">

  - **Berger**, Florian, *Mit Eichenlaub und Schwertern. Die
    höchstdekorierten Soldaten des Zweiten Weltkrieges*. Selbstverlag
    Florian Berger, 2006. ISBN 3-9501307-0-5.
  - **von Blaskowitz**, Johannes - *German reaction to the invasion of
    southern France* - (ASIN B0007K469O) - Historical Division,
    Headquarters, United States Army, Europe, Foreign Military Studies
    Branch, 1945
  - **von Blaskowitz**, Johannes - *Answers to questions directed to
    General Blaskowitz* - (ASIN B0007K46JY) - Historical Division,
    Headquarters, United States Army, Europe, Foreign Military Studies
    Branch, 1945
  - **Fellgiebel**, Walther-Peer. *Die Träger des Ritterkreuzes des
    Eisernen Kreuzes 1939-1945*. Friedburg, Germany: Podzun-Pallas,
    2000. ISBN 3-7909-0284-5.
  - **Giziowski**, Richard - *The Enigma of General Blaskowitz*
    (Hardcover) (ISBN 0-7818-0503-1) - Hippocrene Books, November 1996
  - Information on his death - *[The New York
    Times](../Page/The_New_York_Times.md "wikilink")*, Feb 6, 1948, p.13
  - Information on his death - *[The
    Times](../Page/The_Times.md "wikilink")*, Feb 8, 1948, p. 3

</div>

[Category:德國軍事人物](../Category/德國軍事人物.md "wikilink")
[Category:橡葉騎士佩寶劍鐵十字勳章人物](../Category/橡葉騎士佩寶劍鐵十字勳章人物.md "wikilink")
[Category:德國大將](../Category/德國大將.md "wikilink")
[Category:德國自殺者](../Category/德國自殺者.md "wikilink")
[Category:東普魯士人](../Category/東普魯士人.md "wikilink")