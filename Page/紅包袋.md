[Laisee.jpg](https://zh.wikipedia.org/wiki/File:Laisee.jpg "fig:Laisee.jpg")
[McDonalds_red_packet.JPG](https://zh.wikipedia.org/wiki/File:McDonalds_red_packet.JPG "fig:McDonalds_red_packet.JPG")
**紅包袋**是包著金錢作為[禮物送人的包裝](../Page/禮物.md "wikilink")[封套](../Page/封套.md "wikilink")，-{zh-cn:粤语称**利是封**;zh-hk:國語稱**紅包袋**;zh-tw:粵語稱**利是封**;}-。相傳送[紅包的習俗](../Page/紅包.md "wikilink")，在很久以前就流行。

紅包袋在[唐代時已經出現](../Page/唐代.md "wikilink")，當時是用手工織的布袋做“封面”，僅限於[宮廷及官方使用](../Page/宮廷.md "wikilink")，民間則以寓意吉祥的鮮[紅色紙](../Page/紅色.md "wikilink")，包著一張寫滿祝福字句的字條，送給親朋好友，以表心意。

到了三百年前的[清代](../Page/清代.md "wikilink")，人們則改用一張大約是正方形的紅紙，包裹[銅錢或](../Page/銅錢.md "wikilink")[銀錢](../Page/銀錢.md "wikilink")，封作“利市”。而第一代的印刷紙質紅包袋，約於1900年，[印刷術開始廣爲所用時](../Page/印刷術.md "wikilink")，才得以問世。當時的所謂紅包袋，做法非常簡單，只以紅紙印上黃油，再於未乾的黃油上綴上金粉，效果就如現今燙了金字的紅包袋般，而圖案則多以簡單爲主，再配以吉利的字句。

近年的紅包袋印刷精美，除了傳統的紅色外，還有金色的封套，上面的圖案亦種類繁多，除了平面印刷和燙金印刷的吉祥圖案、生肖圖案外，還有立體雷射印刷和卡通人物圖案的。

由於收紅包的人在在拆開紅包袋後，往往就會把紅包袋棄掉，每年浪費了不少[紙張](../Page/紙張.md "wikilink")，因此有[環保人士提出循環再用紅包袋](../Page/環保.md "wikilink")。除了留待明年春節或需要時再用來封紅包外，還可以做一些賀年飾物，例如裝飾用的假[爆竹](../Page/爆竹.md "wikilink")、[魚形或球形的](../Page/魚.md "wikilink")[吊飾等](../Page/吊飾.md "wikilink")。

## 其他地方的紅包袋

[越南](../Page/越南.md "wikilink")、[琉球紅包袋與中國的樣式相同](../Page/琉球.md "wikilink")，[日本的紅包袋稱為](../Page/日本.md "wikilink")“”，傳統上是白色的，而近年也有其他顏色出现。[朝鮮人則會用白信封裝壓歲錢](../Page/朝鮮族.md "wikilink")，因為他們把[白色視為純潔的象徵](../Page/白色.md "wikilink")。

## 參見

  - [禮封](../Page/禮封.md "wikilink")
  - [紅包](../Page/紅包.md "wikilink")
  - [壓歲錢](../Page/壓歲錢.md "wikilink")

## 參考資料

  - [清朝利是封炫耀達官權勢
    民國利是封印滿商品廣告](http://gocn.southcn.com/xqjj/gxfw/200506020031.htm)

[Category:封套](../Category/封套.md "wikilink")
[Category:红包](../Category/红包.md "wikilink")