**紅檜**（[學名](../Page/學名.md "wikilink")：**）
是[柏科](../Page/柏科.md "wikilink")[扁柏屬大喬木](../Page/扁柏屬.md "wikilink")，又稱「台灣紅檜」、松蘿、薄皮、水古杉、Benihi。1896年11月，[竹山撫墾署長](../Page/竹山撫墾署.md "wikilink")[齊藤音作夥同林學博士](../Page/齊藤音作.md "wikilink")[本多靜六組團搶攻](../Page/本多靜六.md "wikilink")[玉山](../Page/玉山.md "wikilink")，誤登最難攻頂的[玉山東峰](../Page/玉山東峰.md "wikilink")，當時係取道[東埔](../Page/東埔.md "wikilink")、[觀高](../Page/觀高.md "wikilink")、[八通關](../Page/八通關.md "wikilink")，因而本多氏也帶回植物採集史上，第一份紅檜標本，且運至日本東京帝大，由松村任三教授以「福爾摩沙」拉丁語化，於1901年命名為台灣紅檜。然而，本多靜六也成為第一位主張開採檜木林的日本學者，十餘年後的1910年，阿里山區正式伐木集材\[1\]。由於人類的濫伐，已經成為[瀕危物種](../Page/瀕危物種.md "wikilink")。與[日本花柏為近緣種](../Page/日本花柏.md "wikilink")。

## 形态

  - 常绿大乔木。
  - 幹皮灰紅色至紅褐色，縱向淺溝裂，長片條狀剝落，有時具方形鱗片；外皮纖維質，斷面深紅褐色；新生周皮極顯著，鮮紫紅色；內皮纖維質，緂紅色，刀削後漸變為橘黃色。
  - 葉先端漸尖形成銳形，中葉橫斷面船形，中肋兩面凸起，側葉則為V形。
  - [毬果橢圓形](../Page/毬果.md "wikilink")，長7\~9毫米，鱗片10\~13；每一果鱗有種子1\~2。
  - 幼苗有多數長約1cm之線形初生葉遺留苗莖。

## 分佈

  - 本種為[台灣特有種](../Page/台灣特有種.md "wikilink")，全島山區皆曾有分布，常與[台灣扁柏形成混交林](../Page/台灣扁柏.md "wikilink")：分布最低至海拔1,050m（台灣[北插天山](../Page/北插天山.md "wikilink")），最盛處在海拔1,800\~2,500m間，已知最高齡為4100歲的[眠月神木](../Page/眠月神木.md "wikilink")。由於檜木為上好的木材，日治時期與國民政府時期皆大量開發檜木林，其中日治砍伐材積最大的一年為1942年，達48萬9728立方公尺\[2\]；1945年後至1991年正式宣布「全面禁伐天然林」之前，[國民政府檜木生產最高的一年為](../Page/國民政府.md "wikilink")1964年，達40萬2440立方公尺\[3\]。因飽受長期伐木開發，天然紅檜巨木所剩稀少，巨木林群幾乎已被砍伐殆盡。目前所見的紅檜巨木，多是在林場之中，當時被認定生長不良、不利於開採或時間不足等其他因素未被砍伐的樹材。

## 其它

  - [阿里山神木](../Page/阿里山神木.md "wikilink")、[鹿林神木](../Page/鹿林神木.md "wikilink")、[赫威神木皆為本種](../Page/赫威神木.md "wikilink")。
  - 生長緩慢，比較於扁柏樹性偏陽，佔台灣[針葉樹材蓄積之第二位](../Page/針葉樹.md "wikilink")，為主要造林樹種之一。
  - 心材帶淡紅色罕黃色(所謂「反種紅檜」)，無辣味，優良之建築加工用材。
  - 是建材中木質最優者，被列為一級木材，其他同等之樹材為：[扁柏](../Page/扁柏.md "wikilink")、[紫杉](../Page/紫杉.md "wikilink")、[花紋樟樹](../Page/花紋樟.md "wikilink")，有著連[紅豆杉都無法比擬價值](../Page/紅豆杉.md "wikilink")

## 註釋

## 参考文献

  -
## 註釋

  - [台灣扁柏](../Page/台灣扁柏.md "wikilink")

## 外部來源

  - [紅檜](http://knowledge.teldap.tw/knowledge/embed/holding-embed.php?entryId=13561)
    － 台灣多樣性知識網
  - [世界遺產－台灣檜木](https://www.youtube.com/watch?v=YpDdT8t3h5I&feature=youtu.be)
    － 農業委員會行政院
  - [林務局 李桃生
    農業100年精華－百年林業承先啟後](http://comedu.just.edu.tw/ezfiles/31/1031/attach/38/pta_9186_1037633_85300.pdf)
  - [李依陵、黃建中、何幸霖－林務局所藏日治與戰後林業檔案簡介](http://www.ith.sinica.edu.tw/quarterly_download.php?name=06+%E6%9D%8E%E4%BE%9D%E9%99%B5.pdf&filename=128505963612.pdf)
  - [第六十三集　自然篇　山林景觀 (一) 森林風華 (台灣真紀錄 \~
    三大林場的開拓史話)](http://i46552.myweb.hinet.net/9401/026.htm)
    － 嘉義林管處志工網頁

[Category:扁柏属](../Category/扁柏属.md "wikilink")
[Cf](../Category/臺灣特有植物.md "wikilink")
[Category:瀕危物種](../Category/瀕危物種.md "wikilink")
[Category:植物保護](../Category/植物保護.md "wikilink")

1.  搶救檜木林運動的價值依據-陳玉峰[1](http://twstudy.sinica.edu.tw/~ngo/forest/cypress/value.htm)

2.  [台湾林業統計.
    昭和17年，p.92(單位為石，相當於0.278立方公尺)](http://dl.ndl.go.jp/info:ndljp/pid/1124803)
3.  [諸葛正：鹿港木工藝產業木材用料的演變，p.9](http://www.jodesign.org.tw/index.php/JODesign/article/viewFile/645/292)