[RedDwarfNASA.jpg](https://zh.wikipedia.org/wiki/File:RedDwarfNASA.jpg "fig:RedDwarfNASA.jpg")
[RedDwarfNASA-hue-shifted.jpg](https://zh.wikipedia.org/wiki/File:RedDwarfNASA-hue-shifted.jpg "fig:RedDwarfNASA-hue-shifted.jpg")筆下的**紅矮星**，也是太陽附近最常見的恆星類型\]\]
**紅矮星**，也就是**M型主序星**（MV），根據[赫羅圖](../Page/赫羅圖.md "wikilink")，「紅矮星」在眾多處於主序階段的[恆星當中](../Page/恆星.md "wikilink")，其大小及[溫度均相對較小和低](../Page/溫度.md "wikilink")，在[光譜分類方面屬於M型](../Page/恒星光谱分类.md "wikilink")，它們在恆星中的數量較多，大多數紅矮星的直徑及質量均低於太陽的三分之一，表面溫度也低於3,500
[K](../Page/热力学温标.md "wikilink")。釋出的光也比太陽弱得多，有時更可低於太陽光度的萬分之一。又由於內部的[氫元素核聚變的速度緩慢](../Page/氫.md "wikilink")，因此它們也擁有較長的壽命。质量低于0.35太阳质量的红矮星会有充分的对流，[氦元素会在恒星内部均匀分布](../Page/氦.md "wikilink")，而不会在核心累积，紅矮星不會膨脹成[紅巨星](../Page/紅巨星.md "wikilink")，而逐步收縮，直至氫氣耗盡。
\[1\]它们会保持稳定的光度和光谱持续数千亿年，由于现在宇宙的年龄有限，还没有红矮星发展到之后的阶段。

[96235main_compjpg.jpg](https://zh.wikipedia.org/wiki/File:96235main_compjpg.jpg "fig:96235main_compjpg.jpg")

此外人們又發現，不含「金屬」的紅矮星只佔很少（**在天文學裡，「金屬」是指氫和氦以外的重元素**），而根據「[大爆炸](../Page/大爆炸理论.md "wikilink")」理論的預測，第一代恆星應只擁有氫、氦及[鋰元素](../Page/鋰.md "wikilink")，如果這些早期恆星包括紅矮星，這些「純正」的紅矮星至今天定能繼續觀測得到，而事實卻不然，含有「金屬」的恆星佔了紅矮星的大多數。因此在宇宙形成時，能發光的第一代恆星定擁有超高質量，它們擁有極短壽命，在經過超新星爆發後，重元素得以產生，成為形成低質量恆星的所需物質。

宇宙眾多恆星中，大約73%左右為紅矮星，佔了大多數，是宇宙中最常見的星體。\[2\]
在我們的銀河系中紅矮星也是大多數，离太阳最近的65颗恒星中有50颗是红矮星。\[3\]例如離太陽最近的恆星，[半人馬座的](../Page/半人馬座.md "wikilink")[南門二](../Page/南門二.md "wikilink")[比鄰星](../Page/比鄰星.md "wikilink")，便是一顆紅矮星，其光譜分類為M5，視星等11.0。

至2005年，人們首度在紅矮星身上，發現有[太陽系外行星圍繞旋轉](../Page/太陽系外行星.md "wikilink")，第一顆行星的質量與[海王星差不多](../Page/海王星.md "wikilink")，日距約為600萬公里（0.04天文單位），其表面溫度約為攝氏150°C。2006年，人們又發現一顆與[土星差不多的行星繞著另一顆紅矮星旋轉](../Page/土星.md "wikilink")，這顆行星的日距為3.9億公里（2.6天文單位），表面溫度為攝氏零下220°C。不過因為適居帶距離太近，红矮星適不適合生命的發展常引起爭論。

## 參見

  - [赫羅圖](../Page/赫羅圖.md "wikilink")
  - [紅巨星](../Page/紅巨星.md "wikilink")
  - [白矮星](../Page/白矮星.md "wikilink")
  - [棕矮星](../Page/棕矮星.md "wikilink")
  - [太陽伴星](../Page/太陽伴星.md "wikilink")

## 引用

[紅矮星](../Category/紅矮星.md "wikilink")
[Category:恆星類型](../Category/恆星類型.md "wikilink")
[Category:矮恒星](../Category/矮恒星.md "wikilink")

1.
2.  [宇宙中超3/4恒星均为红矮星](http://space.kexue.com/2014/0306/37903.html), 科学网,
    2014-03-06 09:39:11
3.  [临近恒星列表](../Page/临近恒星列表.md "wikilink")