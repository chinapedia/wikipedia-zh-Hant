**红肉**是指的是在烹飪前呈現岀紅色的[肉](../Page/肉.md "wikilink")，具體來說、[牛肉](../Page/牛肉.md "wikilink")、[羊肉](../Page/羊肉.md "wikilink")、、[兔肉等](../Page/兔肉.md "wikilink")[哺乳動物的肉都是紅肉](../Page/哺乳動物.md "wikilink")。有些肉類，如豬肉，是用營養學定義的紅肉，而采用普通的定義則為[白肉](../Page/白肉.md "wikilink")。

## 原因

紅肉的顔色來自於[哺乳動物肉中含有的](../Page/哺乳動物.md "wikilink")[肌紅蛋白](../Page/肌紅蛋白.md "wikilink")。肌紅蛋白是一種蛋白質，能夠將[氧傳送至動物的肌肉中去](../Page/氧.md "wikilink")。
適合利用脂肪作為能量來源，收縮速度慢但適合長時間運動。
烹飪好後的食物的顔色不能作為判斷是否紅肉的標準。不管牛肉做成什麼顔色都是紅肉；同樣，豬肉雖在烹飪時變為白色，也是紅肉。

相反的，[鳥類](../Page/鳥類.md "wikilink")（[雞](../Page/雞.md "wikilink")、[鴨](../Page/鴨.md "wikilink")、[鵝](../Page/鵝.md "wikilink")、[火雞等](../Page/火雞.md "wikilink")）、[魚](../Page/魚.md "wikilink")、[爬行動物](../Page/爬行動物.md "wikilink")、[兩棲動物](../Page/兩棲動物.md "wikilink")、甲殼類動物（[蝦](../Page/蝦.md "wikilink")[蟹等](../Page/蟹.md "wikilink")）
或雙殼類動物（[牡蠣](../Page/牡蠣.md "wikilink")、[蛤蜊等](../Page/蛤蜊.md "wikilink")）等非[哺乳動物的肉都不是紅肉](../Page/哺乳動物.md "wikilink")（可以算作[白肉](../Page/白肉.md "wikilink")）。儘管如[鮭魚肉是紅色](../Page/鮭魚.md "wikilink")，也不能算作紅肉。

## 營養

很多營養專家都認為其他肉比紅肉要[健康](../Page/健康.md "wikilink")，因為紅肉中含有很高的[飽和脂肪](../Page/飽和脂肪.md "wikilink")。有一些研究表明紅肉在[直腸癌的形成中起了很大作用](../Page/直腸癌.md "wikilink")。

然而紅肉中有豐富的[鐵](../Page/鐵.md "wikilink")，[素食主義者和不進食紅肉的人應該多吃含鐵豐富的食物](../Page/素食主義者.md "wikilink")。紅肉中也含有豐富的[蛋白質](../Page/蛋白質.md "wikilink")、[鋅](../Page/鋅.md "wikilink")、[煙酸](../Page/煙酸.md "wikilink")、維生素B<sub>12</sub>、[硫胺](../Page/硫胺.md "wikilink")、[核黃素和](../Page/核黃素.md "wikilink")[磷等](../Page/磷.md "wikilink")。

在2012年，[哈佛大學最新的統計數據顯示](../Page/哈佛大學.md "wikilink")，為了減少癌症及心血管疾病的風險，每人每天的紅肉攝取量應該低於42g，其原因是高量的[飽和脂肪與](../Page/飽和脂肪.md "wikilink")[膽固醇](../Page/膽固醇.md "wikilink")，而加工肉品（包含由白肉為原料的加工肉品）的攝取量則要更低，因為加工肉品高[鹽高脂肪又有](../Page/鹽.md "wikilink")[硝等致癌物質](../Page/硝.md "wikilink")。\[1\]

紅肉的主要[健康風險被認為是來自於飽和脂肪與膽固醇](../Page/健康.md "wikilink")（主要出現在肥肉）、加工（高鹽及添加致癌物質）、[燒烤](../Page/燒烤.md "wikilink")、及缺乏[ω-3脂肪酸](../Page/ω-3脂肪酸.md "wikilink")（可以另外補充）；而紅肉生產商一直表示，去除可見脂肪的精瘦紅肉，其膽固醇與飽和脂肪含量只會略高於精瘦的白肉，因此精瘦的紅肉及白肉在控制心血管疾病上幾乎無差別；美國心臟協會前會長也表示，控制[心血管疾病的飲食中可以包含精瘦紅肉](../Page/心血管疾病.md "wikilink")，而且其安全攝取量是高於哈佛大學的統計數據。只是沒有足夠證據顯示，相較於白肉、紅肉沒有其他明顯的[健康風險](../Page/健康.md "wikilink")，例如一些研究懷疑，紅肉含有的[血紅素鐵是明顯的](../Page/血紅素鐵.md "wikilink")[健康風險](../Page/健康.md "wikilink")。

對於精瘦紅肉是否[健康的問題](../Page/健康.md "wikilink")，美國「自然醫學雜誌」的文章中指出，紅肉的瘦肉中所含的左旋肉鹼在被腸道中的細菌分解後將導致更高的膽固醇，增加罹患[心臟病的風險](../Page/心臟病.md "wikilink")。肉鹼在腸道中被分解為一種氣體，經過[肝臟後轉化為一種名為](../Page/肝臟.md "wikilink")[氧化三甲胺的化學物質](../Page/氧化三甲胺.md "wikilink")，這種物質和血管中的脂肪之間有著密切關聯，因此食用過量紅肉對[健康有害](../Page/健康.md "wikilink")。\[2\]

## 人体健康

由于脂肪含量、加工处理上的差异，不同的红肉对[健康也有不同的影响](../Page/健康.md "wikilink")\[3\]。食用加工红肉与死亡率增加有很强的关联性，其中，心血管疾病及癌症是主要死因。\[4\]
亦有证据表明食用未加工红肉可能对人体[健康造成负面影响](../Page/健康.md "wikilink")。\[5\]

### 癌症

流行病学研究发现，随着加工红肉摄入量的增加，患大肠癌的风险也增加。而白肉（如鸡肉）则没有表现出这种关联。\[6\]\[7\]
[世界癌症研究基金会](../Page/世界癌症研究基金会.md "wikilink")（WCRF）和[美国癌症研究协会](../Page/美国癌症研究协会.md "wikilink")（AICR）认定食用红肉可增加患[大肠癌的风险](../Page/大肠癌.md "wikilink")。\[8\]在[英国](../Page/英国.md "wikilink")，约21%的大肠癌都与食用红肉有关。\[9\]WCRF建议每周摄入的红肉不超过300克（烹煮后的质量），其中“应尽可能少”。\[10\]

已有证据显示，摄入红肉可能增加罹患以下癌症的风险：[食道癌](../Page/食道癌.md "wikilink")、[胰腺癌](../Page/胰腺癌.md "wikilink")、[胃癌](../Page/胃癌.md "wikilink")、[子宫内膜癌](../Page/子宫内膜癌.md "wikilink")、[肺癌](../Page/肺癌.md "wikilink")、[膀胱癌](../Page/膀胱癌.md "wikilink")。\[11\]\[12\]\[13\]\[14\]\[15\]

2010年的一项研究显示，没有充分证据能够表明食用红肉会增加[乳癌及](../Page/乳癌.md "wikilink")[前列腺癌的风险](../Page/前列腺癌.md "wikilink")。\[16\]\[17\]

2015年10月26日，[世界卫生组织的](../Page/世界卫生组织.md "wikilink")[国际癌症研究机构发表报告称](../Page/国际癌症研究机构.md "wikilink")，食用[加工](../Page/食品加工.md "wikilink")[肉类](../Page/肉类.md "wikilink")（如[培根](../Page/培根.md "wikilink")、[香肠](../Page/香肠.md "wikilink")、[火腿](../Page/火腿.md "wikilink")、[热狗](../Page/热狗.md "wikilink")）和红肉与某些[癌症存在关联](../Page/癌症.md "wikilink")。\[18\]\[19\]\[20\]

## 参考文献

## 參見

  - [肉](../Page/肉.md "wikilink")
  - [白肉](../Page/白肉.md "wikilink")
  - [加工肉品](../Page/加工肉品.md "wikilink")

[sv:Kött\#Rött och vitt
kött](../Page/sv:Kött#Rött_och_vitt_kött.md "wikilink")

[Category:肉類](../Category/肉類.md "wikilink")
[Category:营养学术语](../Category/营养学术语.md "wikilink")

1.  [每天吃紅肉
    增加早死風險](http://www.libertytimes.com.tw/2012/new/mar/14/today-int9.htm?Slots=BInt)

2.  [紅肉損[健康](../Page/健康.md "wikilink")
    左旋肉鹼是元凶](http://life.chinatimes.com/LifeContent/1404/20130505000617.html)

3.  [一幅圖睇晒患癌風險
    多食腸仔火腿最危險？](https://topick.hket.com/article/1499533?lcc=at)，香港經濟日報，2016-09-09

4.

5.

6.

7.

8.
9.

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.