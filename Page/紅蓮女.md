《**紅蓮女**》是從2008年1月11日起，在[東京電視臺的](../Page/東京電視臺.md "wikilink")[電視劇24中所播放的節目](../Page/電視劇24.md "wikilink")。

## 演出人員

### 主要演員

  - 邊倉史代/紅蓮女：[高部あい](../Page/高部あい.md "wikilink")
  - 剣塚小五郎：[進藤學](../Page/進藤學.md "wikilink")
  - 都市傳説同好會(TDD)成員
      - 武藤英麻：[薗田杏奈](../Page/薗田杏奈.md "wikilink")
      - 神田步美：[中濱奈美子](../Page/中濱奈美子.md "wikilink")
      - 後藤佐織：[中村静香](../Page/中村静香.md "wikilink")
      - 本間響子：[秋山奈奈](../Page/秋山奈奈.md "wikilink")
      - 中川美雪：[高山侑子](../Page/高山侑子.md "wikilink")
      - 一條舞衣：[加藤みづき](../Page/加藤みづき.md "wikilink")
  - 尾崎留美：[夏目奈奈](../Page/夏目奈奈.md "wikilink")
  - 關谷：[片桐龍次](../Page/片桐龍次.md "wikilink")
  - 中鉢大輝：[熊井幸平](../Page/熊井幸平.md "wikilink")
  - 今井真司：[杉村蟬之介](../Page/杉村蟬之介.md "wikilink")
  - こごろー：[我修院達也](../Page/我修院達也.md "wikilink")（声）

### 副標．客串

  - 第1話『』
      - 北澤勇介/裂口女：[府川亮](../Page/府川亮.md "wikilink")

<!-- end list -->

  - 第2話『』
      - 受驗君：[小林正寛](../Page/小林正寛.md "wikilink")
      - 百合子：[重廣禮香](../Page/重廣禮香.md "wikilink")
      - 計程車司機：[春海四方](../Page/春海四方.md "wikilink")

<!-- end list -->

  - 第3話『』
      - 鈴木進/チェーンメール男：[加治將樹](../Page/加治將樹.md "wikilink")

<!-- end list -->

  - 第4話『』
      - 白衣男(売人)：[つまみ枝豆](../Page/つまみ枝豆.md "wikilink")
      - 黒服男(売人)：[ブッチャーブラザーズ](../Page/ブッチャーブラザーズ.md "wikilink")
      - 護士(売人)：[佐野大樹](../Page/佐野大樹.md "wikilink")

<!-- end list -->

  - 第5話『』
      - ドール：[峯岸みなみ](../Page/峯岸みなみ.md "wikilink")
      - アキラ：[椿隆之](../Page/椿隆之.md "wikilink")

<!-- end list -->

  - 第6話『』
      - ベッドの下の男(空巢)：[はなわ](../Page/はなわ.md "wikilink")
      - 少女時代の史代：[優希](../Page/優希.md "wikilink")

<!-- end list -->

  - 第7話『』\[1\]
      - 前園コージ：[柳ユーレイ](../Page/柳ユーレイ.md "wikilink")
      - サツキ：[金子さやか](../Page/金子さやか.md "wikilink")
      - 日向：[倉貫匡弘](../Page/倉貫匡弘.md "wikilink")

<!-- end list -->

  - 第8話『』
      - 後藤田瑞穂：[中島史恵](../Page/中島史恵.md "wikilink")
      - 史代(少女時代)：[優希](../Page/優希.md "wikilink")
      - 珠代：[寉岡萌希](../Page/寉岡萌希.md "wikilink")
      - 計程車司機：[春海四方](../Page/春海四方.md "wikilink")

<!-- end list -->

  - 第9話『』
      - 森岡：[近江谷太朗](../Page/近江谷太朗.md "wikilink") …
        オフ会主催者で「都市伝説ずびずばー」の管理人「どんだけパパイヤ」
      - 謎の男：[梨本謙次郎](../Page/梨本謙次郎.md "wikilink") … 吉本有香の父親
      - 村田/男 紅蓮女B：[アサカ コウギ](../Page/アサカ_コウギ.md "wikilink") … 中鉢の友人
      - ミホリン(声)：[宮後由美](../Page/宮後由美.md "wikilink") … 中鉢の彼女

<!-- end list -->

  - 第10話『』
      - 珠代/セーラーさん：[寉岡萌希](../Page/寉岡萌希.md "wikilink") …
        史代の姉。生日是3月11日。17歳の誕生日が命日。
      - 史代(少女時代)：[優希](../Page/優希.md "wikilink")
      - 伝説マニア：[石井智也](../Page/石井智也.md "wikilink")

## 工作人員

  - 原作：上甲宣之
  - 腳本：[田中江里夏](../Page/田中江里夏.md "wikilink")、[酒井直行](../Page/酒井直行.md "wikilink")、[吉澤智子](../Page/吉澤智子.md "wikilink")
  - プロデュサー：阿部神二（東京電視臺）、森田昇（東京電視臺）、大野秀樹（5年D組）、里内英司（5年D組）
  - 導演：[阿部雄一](../Page/阿部雄一.md "wikilink")、[木内健人](../Page/木内健人.md "wikilink")、[日暮謙](../Page/日暮謙.md "wikilink")
  - 音樂：[EUGENE](../Page/EUGENE.md "wikilink")
  - 音樂協力：[東京電視臺ミュージック](../Page/東京電視臺ミュージック.md "wikilink")
  - 企畫協力：[オスカープロモーション](../Page/オスカープロモーション.md "wikilink")
  - 製作：東京電視臺、[5年D組](../Page/5年D組.md "wikilink")

## 外部連結

  - [官方網站](http://www.tv-tokyo.co.jp/guren/index.html)

## 參考文獻

[Category:電視劇24](../Category/電視劇24.md "wikilink")
[Category:奇幻愛情電視劇](../Category/奇幻愛情電視劇.md "wikilink")
[Category:日本浪漫喜劇電視劇](../Category/日本浪漫喜劇電視劇.md "wikilink")
[Category:日本奇幻劇](../Category/日本奇幻劇.md "wikilink")
[Category:日本恐怖劇](../Category/日本恐怖劇.md "wikilink")
[Category:日本小說改編電視劇](../Category/日本小說改編電視劇.md "wikilink")
[Category:2008年日本電視劇集](../Category/2008年日本電視劇集.md "wikilink")
[Category:变身女主角](../Category/变身女主角.md "wikilink")

1.  番組公式サイト上では**「紅蓮女、テレビで暴かれる！」**となっている