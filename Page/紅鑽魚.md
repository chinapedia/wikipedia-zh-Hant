**紅鑽魚**（[学名](../Page/学名.md "wikilink")：），又稱**大口濱鯛**、**紅肉鑽**、**紅雞仔**、**濱鯛**，[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[鱸亞目](../Page/鱸亞目.md "wikilink")[笛鯛科的其中一個](../Page/笛鯛科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚廣泛分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[東非](../Page/東非.md "wikilink")、[紅海](../Page/紅海.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[波斯灣](../Page/波斯灣.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[波斯灣](../Page/波斯灣.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[印度](../Page/印度.md "wikilink")、[安達曼海](../Page/安達曼海.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[越南](../Page/越南.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[中國](../Page/中國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[琉球群島](../Page/琉球群島.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[所羅門群島](../Page/所羅門群島.md "wikilink")、[諾魯](../Page/諾魯.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[萬納杜](../Page/萬納杜.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[夏威夷等海域](../Page/夏威夷.md "wikilink")。

## 深度

水深90至400公尺。

## 特徵

本魚體延長呈紡錘形，標準體長約為體高之3.35倍。兩眼間隔平扁。眼前方無溝槽。下頜突出於上頜；上頜骨末端延伸至眼中部的下方；上頜骨被鱗。上下頜具多列細齒，外列齒擴大；上下頜前端具2至4犬齒；鋤骨具窄的弧狀齒帶。體被中大型櫛鱗，背鰭及臀鰭上均裸露無鱗；側線完全且平直。魚體為鮮豔的玫瑰紅，腹側為淺紅色或銀白色，背鰭、尾鰭、吻、頷、眼、口內、鰓孔內皆為紅色，背鰭硬棘10枚，軟條11枚；臀鰭硬棘3枚，軟條8枚；胸鰭長約等於頭長；尾鰭深分叉，下葉比較長。其明顯特徵為沿著側線有條黃色縱帶自鰓蓋後方延伸到尾鰭基部。體長可達127公分。

## 生態

本魚棲息在深溝、大凹坑或斜坡地，離底5至8公尺處洄游，係貼地吃餌，喜向下俯衝食餌。主要攝食[魚類及大型無脊椎動物如](../Page/魚類.md "wikilink")[烏賊](../Page/烏賊.md "wikilink")、[蝦及](../Page/蝦.md "wikilink")[蟹等](../Page/蟹.md "wikilink")。

## 經濟利用

重要之食用魚，味美肉鮮，適宜做[生魚片](../Page/生魚片.md "wikilink")、砂鍋魚頭，炸烤等各式烹煮方法。

## 参考文献

  -
## 扩展阅读

[Category:食用鱼](../Category/食用鱼.md "wikilink")