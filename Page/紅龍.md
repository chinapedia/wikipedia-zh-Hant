是一部2002年上映的美國[心理恐懼電影](../Page/心理學恐懼.md "wikilink")，改編[湯瑪斯·哈里斯的同名小說](../Page/湯瑪斯·哈里斯.md "wikilink")。本片為《[1987大懸案](../Page/1987大懸案.md "wikilink")》的翻拍，是《[沉默的羔羊](../Page/沉默的羔羊.md "wikilink")》同系列小說之第一部改編而成。本片由[布萊特·拉特納執導](../Page/布萊特·拉特納.md "wikilink")，[艾德華·諾頓](../Page/艾德華·諾頓.md "wikilink")、[安東尼·霍普金斯領銜主演](../Page/安東尼·霍普金斯.md "wikilink")，描述殺人狂醫師[漢尼拔·萊克特在遇上克麗絲](../Page/漢尼拔·萊克特.md "wikilink")·史達琳探員前的故事。本片是《[沉默的羔羊](../Page/沉默的羔羊.md "wikilink")》及《[沉默的殺機](../Page/沉默的殺機.md "wikilink")》的前傳。

## 劇情

故事開頭是年輕的FBI探員威爾·葛拉漢為了調查連續殺人案而前來拜訪合作夥伴：漢尼拔·萊克特醫生，卻無意間發現漢尼拔就是那個連續殺人-{}-魔，葛拉漢被刺成重傷後仍不斷掙扎，在失去意識前對漢尼拔開槍，漢尼拔因此被捕。雖然威爾平安獲救，卻由於龐大的精神壓力而決定辭職，與妻兒住在佛羅里達的馬拉松。

數年後，葛拉漢的前上司瑞拔·麥克連前來拜訪，希望威爾能重出江湖協助逮捕新的連續殺人狂「牙仙」。

## 角色

  - [艾德華·諾頓](../Page/艾德華·諾頓.md "wikilink") 飾

  - [安東尼·霍普金斯](../Page/安東尼·霍普金斯.md "wikilink") 飾
    [漢尼拔·萊克特](../Page/漢尼拔·萊克特.md "wikilink")

  - [雷夫·范恩斯](../Page/雷夫·范恩斯.md "wikilink") 飾

  - [哈維·凱托](../Page/哈維·凱托.md "wikilink") 飾

  - [艾蜜莉·華森](../Page/艾蜜莉·華森.md "wikilink") 飾 瑞拔·麥克連（Reba McClane）

  - [瑪麗-露易斯·帕克](../Page/瑪麗-露易斯·帕克.md "wikilink") 飾 莫莉·葛拉漢（Molly Graham）

  - [菲力普·西蒙·霍夫曼](../Page/菲力普·西蒙·霍夫曼.md "wikilink") 飾

  - 飾 Ralph Mandy

  - 飾

  - 飾 Lloyd Bowman

  - [弗蘭基·費森](../Page/弗蘭基·費森.md "wikilink") 飾 Barney Matthews

  - 飾 Josh Graham

  - [艾倫·鮑絲汀](../Page/艾倫·鮑絲汀.md "wikilink") 飾 Grandma Dolarhyde的聲音（未掛名）

## 回應

《紅龍》一片在票房上很成功，北美賺進92,930,005美元，影評則不一，許多人認為比起《[1987大懸案](../Page/1987大懸案.md "wikilink")》還是遜色，不過知名影評[羅傑·艾伯特則相當的喜歡重製片](../Page/羅傑·艾伯特.md "wikilink")。爛番茄網站的新鮮度則有69%，比起《1987大懸案》的94%遜色不少，而且在權威網站[IMDb上](../Page/IMDb.md "wikilink")，紅龍一片和《1987大懸案》都得到7.2的分數。

## 外部連結

  - [官方網站](http://www.reddragonmovie.com/)

  -
[Category:2002年电影](../Category/2002年电影.md "wikilink")
[Category:美國恐怖片](../Category/美國恐怖片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國重拍電影](../Category/美國重拍電影.md "wikilink")
[Category:環球影業電影](../Category/環球影業電影.md "wikilink")
[Category:連環殺手題材電影](../Category/連環殺手題材電影.md "wikilink")
[Category:米高梅電影](../Category/米高梅電影.md "wikilink")
[Category:人吃人題材電影](../Category/人吃人題材電影.md "wikilink")
[Category:驚悚小說改編電影](../Category/驚悚小說改編電影.md "wikilink")
[Category:漢尼拔·萊克特](../Category/漢尼拔·萊克特.md "wikilink")