**納弗沙島**（、[海地](../Page/海地.md "wikilink")[克里奧爾語](../Page/克里奧爾語.md "wikilink")：**Lanavaz**）是[加勒比海無人居住的小](../Page/加勒比海.md "wikilink")[島](../Page/島.md "wikilink")，是[美國的](../Page/美國.md "wikilink")[非建制領土](../Page/美国非建制屬地.md "wikilink")，[海地亦宣稱所有](../Page/海地.md "wikilink")。

## 地理

納弗沙島面積大約5.2平方公里（2平方英里）。島的[經度和](../Page/經度.md "wikilink")[緯度是西經](../Page/緯度.md "wikilink")75°2'，北緯18°25'。

納弗沙島的[地形大多是裸露的](../Page/地形.md "wikilink")[岩石](../Page/岩石.md "wikilink")，但有足夠的[草地支持](../Page/草地.md "wikilink")[山羊的糧食](../Page/山羊.md "wikilink")。

[Category:无人岛](../Category/无人岛.md "wikilink")
[Category:有爭議的島嶼](../Category/有爭議的島嶼.md "wikilink")
[Category:美國本土外小島](../Category/美國本土外小島.md "wikilink")
[Category:海地島嶼](../Category/海地島嶼.md "wikilink")
[Category:大安的列斯群島](../Category/大安的列斯群島.md "wikilink")
[Category:美國爭議地區](../Category/美國爭議地區.md "wikilink")
[Category:海地爭議地區](../Category/海地爭議地區.md "wikilink")
[Category:海地－美國關係](../Category/海地－美國關係.md "wikilink")