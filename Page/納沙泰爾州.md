**納沙泰爾**([法語](../Page/法語.md "wikilink")、[意大利語](../Page/意大利語.md "wikilink")、[羅曼語](../Page/羅曼語.md "wikilink"):Neuchâtel;
[德語](../Page/德語.md "wikilink"):Neuenburg)是一個位於[瑞士西部的州](../Page/瑞士.md "wikilink")，在法語區內。人口167,500，首府設於[納沙泰爾市](../Page/納沙泰爾.md "wikilink")。

## 地理

納沙泰爾州在[汝拉山脈的中部](../Page/汝拉山脈.md "wikilink")，州的東北面是[伯爾尼州](../Page/伯爾尼州.md "wikilink")，西北面與[法國接壤](../Page/法國.md "wikilink")，東南面是[納沙泰爾湖](../Page/納沙泰爾湖.md "wikilink")，[沃州在西南方](../Page/沃州.md "wikilink")。州內主要水系有南方的[納沙泰爾湖及北方的](../Page/納沙泰爾湖.md "wikilink")[杜布河](../Page/杜河.md "wikilink")(River
Doubs)。

州內分為三個主要地區。**Vignoble區**位於[納沙泰爾湖邊](../Page/納沙泰爾湖.md "wikilink")，名字的來由是區內有很多葡萄園。**Les
Vallées區**包含州內兩個最大的山谷--[魯茲谷](../Page/魯茲谷.md "wikilink")(Val de
Ruz)及[特拉瓦谷](../Page/特拉瓦谷.md "wikilink")(Val de
Travers)，兩個山谷都位於海拔700米高。**納沙泰爾山區**(Montagnes
Neuchâteloises)海拔由900至1065米，為州內地勢最高的地區。[拉紹德封](../Page/拉紹德封.md "wikilink")(La
Chaux-de-Fonds)，[力洛克](../Page/力洛克.md "wikilink")(Le
Locle)及-{[拉布雷維訥](../Page/拉布雷維訥.md "wikilink")}-(La
Brévine)位於此區的山谷內。

## 歷史

納沙泰爾州(Neuchâtel)的名字可追溯至古羅馬時代的拉丁名Novum
Castellum，即**新堡**之意。歷史上經歷多番政權更迭。在1032年，勃艮第的[魯道夫三世在遺囑上提及過納沙泰爾](../Page/魯道夫三世.md "wikilink")。孔特‧烏里希‧馮‧芬尼斯王朝在1034年接管了[納沙泰爾城和周邊地區](../Page/納沙泰爾.md "wikilink")，在1373年勢力已擴展至整個州的範圍。1405年，[伯爾尼與](../Page/伯爾尼.md "wikilink")[納沙泰爾建立聯盟](../Page/納沙泰爾.md "wikilink")。之後[納沙泰爾落入](../Page/納沙泰爾.md "wikilink")[弗萊堡大公手中](../Page/弗萊堡.md "wikilink")，在1504年又被[法國的奧爾良](../Page/法國.md "wikilink")-隆格維爾王朝佔領。

[法國牧師威廉](../Page/法國.md "wikilink")‧法路爾(Guillaume
Farel)在1530年把[宗教改革的思想傳到納沙泰爾地區](../Page/宗教改革.md "wikilink")。奧爾良-隆格維爾王朝倒台後，納沙泰爾落入[普魯士國王](../Page/普魯士.md "wikilink")[腓得烈一世手中](../Page/腓特烈一世_\(普鲁士\).md "wikilink")。[普魯士統治此區域至](../Page/普魯士.md "wikilink")1848年(在1806至1814年間成為貝爾蒂埃的大公國)。

1815年納沙泰爾正式加入[瑞士邦聯](../Page/瑞士#拿破崙時期.md "wikilink")，那是[瑞士第一次允許非共和政體的州加入](../Page/瑞士.md "wikilink")。1848年發生的和平[革命最終令納沙泰爾實現共和](../Page/革命.md "wikilink")。

## 經濟

[Lac_de_Neuchatel_depuis_la_Neuchatel_(ville_suisse)_dscn0660.jpg](https://zh.wikipedia.org/wiki/File:Lac_de_Neuchatel_depuis_la_Neuchatel_\(ville_suisse\)_dscn0660.jpg "fig:Lac_de_Neuchatel_depuis_la_Neuchatel_(ville_suisse)_dscn0660.jpg")
[納沙泰爾湖邊是著名的產酒區](../Page/納沙泰爾湖.md "wikilink")，山谷上的居民主要飼養[牛隻及出產](../Page/牛.md "wikilink")[牛奶](../Page/牛奶.md "wikilink")，而產自納沙泰爾的[馬匹也有很好的名聲](../Page/馬.md "wikilink")。除[農業外](../Page/農業.md "wikilink")，納沙泰爾亦生產[鐘錶](../Page/鐘錶.md "wikilink")、精密機械及[微型晶片](../Page/積體電路.md "wikilink")。

## 人口

絕大多數居民說[法語](../Page/法語.md "wikilink")，三分之二信奉[新教](../Page/新教.md "wikilink")，三分之一信奉[天主教](../Page/天主教.md "wikilink")。

## 飲食文化

[Swiss_fondue_2.jpg](https://zh.wikipedia.org/wiki/File:Swiss_fondue_2.jpg "fig:Swiss_fondue_2.jpg")\]\]
[乳酪火鍋是納沙泰爾出名的食品](../Page/乳酪火鍋.md "wikilink")。

在纳沙泰尔州，九月底举行为期三天的[葡萄酒节](../Page/葡萄酒.md "wikilink")，是[瑞士全國同類活動中規模最大的](../Page/瑞士.md "wikilink")。庆祝活动中，最引人注目的是复杂精细的花车游行。

## 行政區域

共分6區轄下有62市:

### 區

  - 納沙泰爾區(Neuchâtel)--首府[納沙泰爾市](../Page/納沙泰爾.md "wikilink")(Neuchâtel)
  - 布德利區(Boudry)--首府布德利(Boudry)
  - 魯茲谷區(Val-de-Ruz)--首府錫尼埃(Cernier)
  - 特拉瓦谷區(Val-de-Travers)--首府莫蒂埃(Môtiers)
  - 索迪芳區(La Chaux-de-Fonds)--首府索迪芳(La Chaux-de-Fonds)
  - 洛可勒區(Le Locle)--首府洛可勒(Le Locle)

### 市

<table>
<tbody>
<tr class="odd">
<td><h4 id="納沙泰爾區">納沙泰爾區</h4>
<ul>
<li><a href="../Page/科爾諾_(納沙泰爾州).md" title="wikilink">科爾諾</a></li>
<li><a href="../Page/克雷謝_(納沙泰爾州).md" title="wikilink">克雷謝</a></li>
<li><a href="../Page/恩熱.md" title="wikilink">恩熱</a></li>
<li><a href="../Page/豪泰里夫.md" title="wikilink">豪泰里夫</a></li>
<li><a href="../Page/勒朗德龍.md" title="wikilink">勒朗德龍</a></li>
<li><a href="../Page/利涅爾_(納沙泰爾州).md" title="wikilink">利涅爾</a></li>
<li><a href="../Page/拉登_(瑞士).md" title="wikilink">拉登</a></li>
<li><strong><a href="../Page/納沙泰爾.md" title="wikilink">納沙泰爾市</a></strong></li>
<li><a href="../Page/聖布萊斯.md" title="wikilink">聖布萊斯</a></li>
</ul>
<h4 id="索迪芳區">索迪芳區</h4>
<ul>
<li><strong><a href="../Page/拉紹德封.md" title="wikilink">拉紹德封</a></strong></li>
<li><a href="../Page/拉薩涅.md" title="wikilink">拉薩涅</a></li>
<li><a href="../Page/萊普朗謝特.md" title="wikilink">萊普朗謝特</a></li>
</ul></td>
<td><h4 id="布德利區">布德利區</h4>
<ul>
<li><a href="../Page/貝魏.md" title="wikilink">貝魏</a></li>
<li><strong><a href="../Page/布德里.md" title="wikilink">布德里</a></strong></li>
<li><a href="../Page/布羅-德蘇.md" title="wikilink">布羅-德蘇</a></li>
<li><a href="../Page/科塞勒-科芒德雷什.md" title="wikilink">科塞勒-科芒德雷什</a></li>
<li><a href="../Page/科泰洛.md" title="wikilink">科泰洛</a></li>
<li><a href="../Page/弗雷森.md" title="wikilink">弗雷森</a></li>
<li><a href="../Page/戈吉耶.md" title="wikilink">戈吉耶</a></li>
<li><a href="../Page/Milvignes.md" title="wikilink">Milvignes</a></li>
<li><a href="../Page/芒塔爾謝茲.md" title="wikilink">芒塔爾謝茲</a></li>
<li><a href="../Page/佩瑟_(納沙泰爾州).md" title="wikilink">佩瑟</a></li>
<li><a href="../Page/羅什福爾_(納沙泰爾州).md" title="wikilink">羅什福爾</a></li>
<li><a href="../Page/聖歐班索熱.md" title="wikilink">聖歐班索熱</a></li>
<li><a href="../Page/沃馬居.md" title="wikilink">沃馬居</a></li>
</ul></td>
<td><h4 id="洛可勒區">洛可勒區</h4>
<ul>
<li><a href="../Page/布羅-普蘭博.md" title="wikilink">布羅-普蘭博</a></li>
<li><a href="../Page/拉布雷維訥.md" title="wikilink">拉布雷維訥</a></li>
<li><a href="../Page/拉紹迪米利厄.md" title="wikilink">拉紹迪米利厄</a></li>
<li><a href="../Page/萊瑟納-佩基尼奧.md" title="wikilink">萊瑟納-佩基尼奧</a></li>
<li><strong><a href="../Page/力洛克.md" title="wikilink">力洛克</a></strong></li>
<li><a href="../Page/萊布雷內.md" title="wikilink">萊布雷內</a></li>
<li><a href="../Page/萊蓬德馬爾泰勒.md" title="wikilink">萊蓬德馬爾泰勒</a></li>
</ul>
<h4 id="魯茲谷區">魯茲谷區</h4>
<ul>
<li><strong><a href="../Page/Val-de-Ruz.md" title="wikilink">Val-de-Ruz</a></strong></li>
<li><a href="../Page/瓦朗然.md" title="wikilink">瓦朗然</a></li>
</ul>
<h4 id="特拉瓦谷區">特拉瓦谷區</h4>
<ul>
<li><a href="../Page/拉科特奧費.md" title="wikilink">拉科特奧費</a></li>
<li><a href="../Page/萊韋里耶爾.md" title="wikilink">萊韋里耶爾</a></li>
<li><strong><a href="../Page/瓦勒德特拉費.md" title="wikilink">瓦勒德特拉費</a></strong></li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [納沙泰爾州官方網站](http://www.ne.ch/)
  - [官方統計數字](http://www.statistik.admin.ch/stat_ch/ber00/ekan_ne.htm)
  - ["Watch Valley"的旅行資訊](http://www.watchvalley.ch)
  - [非官方的納沙泰爾州網站](https://web.archive.org/web/20050924163612/http://www.neuch.ch/)

[Category:瑞士行政區劃](../Category/瑞士行政區劃.md "wikilink")