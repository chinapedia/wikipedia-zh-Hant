**C線第八大道慢車**（），又稱**紐約地鐵C線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的一條[地鐵](../Page/地鐵.md "wikilink")，全長19英哩\[1\]。由於該線使用[IND第八大道線通過](../Page/IND第八大道線.md "wikilink")[曼哈頓主要地區](../Page/曼哈頓.md "wikilink")，因此其路線徽號為藍色\[2\]。

C線列車除深夜外任何時候都營運，來往[華盛頓高地的](../Page/華盛頓高地_\(曼哈頓\).md "wikilink")[168街與](../Page/168街車站_\(IND第八大道線\).md "wikilink")的[尤克利德大道](../Page/尤克利德大道車站_\(IND福爾頓街線\).md "wikilink")，停靠沿線全部車站。在深夜時段，日間時以快車營運的[A線列車以慢車營運整條C線](../Page/紐約地鐵A線.md "wikilink")，停靠所有車站。

C線繁忙時段列車過去以慢車營運[IND匯集線往](../Page/IND匯集線.md "wikilink")[布朗克斯的](../Page/布朗克斯.md "wikilink")[貝德福德公園林蔭路與皇后區](../Page/貝德福德公園林蔭路車站_\(IND匯集線\).md "wikilink")[洛克威公園-海灘116街](../Page/洛克威公園-海灘116街車站_\(IND洛克威線\).md "wikilink")。

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p><a href="../Page/R1_(紐約地鐵車輛).md" title="wikilink">R1</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a></p>
</div></td>
<td></td>
</tr>
</tbody>
</table>

</div>

## 歷史

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p>1967-1979年使用（圓形）</p>
</div></td>
</tr>
</tbody>
</table>

</div>

C線與CC線列車自1933年7月1日開始營運於新的[IND匯集線](../Page/IND匯集線.md "wikilink")。

1976年8月27日，C線行駛慢車取代[E線](../Page/紐約地鐵E線.md "wikilink")，自福爾頓街駛至洛克威公園，成為當時唯一橫跨四區的地鐵路線。

1986年將雙字代表慢車的措施取消，CC線易名H線。

1998年3月1日，B線與C線北訖站對調，C線北訖站改為168街車站。

1999年起，C線行駛慢車至尤克利德大道。

2001年[911事件後](../Page/911事件.md "wikilink")，C線暫停服務（於9月21日復駛），停駛期間中央公園西路的慢車由[A](../Page/紐約地鐵A線.md "wikilink")/[D線取代](../Page/紐約地鐵D線.md "wikilink")，[E線延駛至尤克利德大道](../Page/紐約地鐵E線.md "wikilink")。

2005年1月23日，欽伯斯街車站號誌控制室發生火災，最初評估需要幾年的時間才能恢復，但受損設備尚可使用，於是在4月21日恢復正常行駛。

## 路線

### 服務形式

以下表格顯示C線所使用路線：\[3\]

| 路段                                                | 起點                                                        | 終點                                                    | 軌道 |
| ------------------------------------------------- | --------------------------------------------------------- | ----------------------------------------------------- | -- |
| [IND第八大道線](../Page/IND第八大道線.md "wikilink")        | [168街](../Page/168街車站_\(IND第八大道線\).md "wikilink")         | [堅尼街](../Page/堅尼街車站_\(IND第八大道線\).md "wikilink")       | 慢車 |
| [錢伯斯街](../Page/錢伯斯街車站_\(IND第八大道線\).md "wikilink") | [高街](../Page/高街車站_\(IND第八大道線\).md "wikilink")             | 全部                                                    |    |
| [IND福爾頓街線](../Page/IND福爾頓街線.md "wikilink")        | [傑伊街-都會科技](../Page/傑伊街-都會科技車站_\(IND福爾頓街線\).md "wikilink") | [尤克利德大道](../Page/尤克利德大道車站_\(IND福爾頓街線\).md "wikilink") | 慢車 |

### 車站

更詳細的車站列表參見上方列出的路線。

<table style="width:154%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 30%" />
<col style="width: 36%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-C.svg" title="fig:NYCS-bull-trans-C.svg">NYCS-bull-trans-C.svg</a></p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/曼哈頓.md" title="wikilink">曼哈頓</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND第八大道線.md" title="wikilink">第八大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/168街車站_(IND第八大道線).md" title="wikilink">168街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/163街-阿姆斯特丹大道車站_(IND第八大道線).md" title="wikilink">163街-阿姆斯特丹大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/155街車站_(IND第八大道線).md" title="wikilink">155街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/145街車站_(IND第八大道線).md" title="wikilink">145街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND匯集線.md" title="wikilink">IND匯集線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/135街車站_(IND第八大道線).md" title="wikilink">135街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/125街車站_(IND第八大道線).md" title="wikilink">125街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/116街車站_(IND第八大道線).md" title="wikilink">116街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/教堂公園道-110街車站_(IND第八大道線).md" title="wikilink">教堂公園道-110街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/103街車站_(IND第八大道線).md" title="wikilink">103街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/96街車站_(IND第八大道線).md" title="wikilink">96街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/86街車站_(IND第八大道線).md" title="wikilink">86街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/81街-自然歷史博物館車站_(IND第八大道線).md" title="wikilink">81街-自然歷史博物館</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/72街車站_(IND第八大道線).md" title="wikilink">72街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/59街-哥倫布圓環車站_(IND第八大道線).md" title="wikilink">59街-哥倫布圓環</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/50街車站_(IND第八大道線).md" title="wikilink">50街</a></p></td>
<td></td>
<td><p> ↓</p></td>
<td><p>（<a href="../Page/IND皇后林蔭路線.md" title="wikilink">IND皇后林蔭路線</a>）</p></td>
<td><p>只限南行可無障礙通行</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/42街-航港局客運總站車站_(IND第八大道線).md" title="wikilink">42街-航港局客運總站</a></p></td>
<td></td>
<td></td>
<td><p>nowrap | <br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）<br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>）<br />
（<a href="../Page/42街接駁線.md" title="wikilink">42街接駁線</a>）<br />
，<a href="../Page/時報廣場-42街車站.md" title="wikilink">時報廣場-42街</a></p></td>
<td><p><a href="../Page/紐新航港局客運總站.md" title="wikilink">紐新航港局客運總站</a><br />
</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/34街-賓州車站_(IND第八大道線).md" title="wikilink">34街-賓州車站</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
<a href="../Page/美鐵.md" title="wikilink">美鐵</a>、<a href="../Page/長島鐵路.md" title="wikilink">長島鐵路及</a><a href="../Page/新澤西通勤鐵路.md" title="wikilink">新澤西交通</a>，<a href="../Page/賓夕法尼亞車站_(紐約市).md" title="wikilink">賓州車站</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/23街車站_(IND第八大道線).md" title="wikilink">23街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/14街車站_(IND第八大道線).md" title="wikilink">14街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/西四街-華盛頓廣場車站_(IND第八大道線).md" title="wikilink">西四街-華盛頓廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/第九街車站_(PATH).md" title="wikilink">第九街</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/泉街車站_(IND第八大道線).md" title="wikilink">泉街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/堅尼街車站_(IND第八大道線).md" title="wikilink">堅尼街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/錢伯斯街車站_(IND第八大道線).md" title="wikilink">錢伯斯街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>，<a href="../Page/公園廣場車站_(IRT百老匯-第七大道線).md" title="wikilink">公園廣場</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>，<a href="../Page/科特蘭街車站_(BMT百老匯線).md" title="wikilink">科特蘭街</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/世界貿易中心車站_(PATH).md" title="wikilink">世界貿易中心</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/福爾頓街車站_(IND第八大道線).md" title="wikilink">福爾頓街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）<br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>）<br />
（<a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a>）<br />
連接（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>），經，<a href="../Page/科特蘭街車站_(BMT百老匯線).md" title="wikilink">科特蘭街</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/高街車站_(IND第八大道線).md" title="wikilink">高街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/IND福爾頓街線.md" title="wikilink">福爾頓街線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/傑伊街-都會科技車站_(IND福爾頓街線).md" title="wikilink">傑伊街-都會科技</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT第四大道線.md" title="wikilink">BMT第四大道線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/霍伊特-歇爾美角街車站_(IND福爾頓街線).md" title="wikilink">霍伊特-歇爾美角街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND跨城線.md" title="wikilink">IND跨城線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/拉法葉大道車站_(IND福爾頓街線).md" title="wikilink">拉法葉大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/克林頓-華盛頓大道車站_(IND福爾頓街線).md" title="wikilink">克林頓-華盛頓大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/富蘭克林大道車站_(IND福爾頓街線).md" title="wikilink">富蘭克林大道</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/BMT富蘭克林大道線.md" title="wikilink">BMT富蘭克林大道線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/諾斯特蘭大道車站_(IND福爾頓街線).md" title="wikilink">諾斯特蘭大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>，<a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/京斯頓-羅普大道車站_(IND福爾頓街線).md" title="wikilink">京斯頓-羅普大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>巴士往<a href="../Page/約翰·甘迺迪國際機場.md" title="wikilink">JFK機場</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/猶提卡大道車站_(IND福爾頓街線).md" title="wikilink">猶提卡大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/勞夫大道車站_(IND福爾頓街線).md" title="wikilink">勞夫大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/洛克威大道車站_(IND福爾頓街線).md" title="wikilink">洛克威大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/百老匯交匯車站_(IND福爾頓街線).md" title="wikilink">百老匯交匯</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT牙買加線.md" title="wikilink">BMT牙買加線</a>）<br />
（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/自由大道車站_(IND福爾頓街線).md" title="wikilink">自由大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/凡希克凌大道車站_(IND福爾頓街線).md" title="wikilink">凡希克凌大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/牧者大道車站_(IND福爾頓街線).md" title="wikilink">牧者大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/尤克利德大道車站_(IND福爾頓街線).md" title="wikilink">尤克利德大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 備註

<references />

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20061022175312/http://mta.info/nyct/service/cline.htm)
  - [C線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit -
    C線時刻表（PDF）](https://web.archive.org/web/20060921231502/http://www.mta.info/nyct/service/pdf/tccur.pdf)

[C](../Category/紐約地鐵服務路線.md "wikilink")

1.
2.

3.