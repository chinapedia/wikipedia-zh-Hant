**G線跨城線慢車**（），又稱**紐約地鐵G線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的[地鐵系統](../Page/地鐵.md "wikilink")，全長11.4英哩\[1\]。由於該線使用[IND跨城線](../Page/IND跨城線.md "wikilink")，因此其路線徽號為淺綠色\[2\]。

G線任何時候都營運，來往[長島市的](../Page/長島市.md "wikilink")[法庭廣場與](../Page/法庭廣場車站_\(IND跨城線\).md "wikilink")的[教堂大道](../Page/教堂大道車站_\(IND卡爾弗線\).md "wikilink")，停靠全線所有車站。G線是唯一不停靠[曼哈頓的非接駁路線](../Page/曼哈頓.md "wikilink")。此線經常受到服務干擾、班次較疏、甚少免費轉乘其他路線、載客量低，引致途經的鄰里的居民和政客經常批抨此線。自2000年代起，G線也進行了一定改善工程，包括延長至[布魯克林和全線沿途聽證會以解決G線列車的問題](../Page/布魯克林.md "wikilink")。

G線列車在[皇后區停靠兩個車站](../Page/皇后區.md "wikilink")：都位於長島市的法庭廣場與[21街](../Page/21街車站_\(IND跨城線\).md "wikilink")。2010年以前，G線服務所有法庭廣場至[森林小丘的](../Page/森林小丘.md "wikilink")[71大道的](../Page/森林小丘-71大道車站_\(IND皇后林蔭路線\).md "wikilink")[IND皇后林蔭路線車站](../Page/IND皇后林蔭路線.md "wikilink")。1939年至1940年，G線同時用作現已拆除的以接駁[1939年紐約世界博覽會](../Page/1939年紐約世界博覽會.md "wikilink")。從1976年至2009年，G線在[史密斯-第九街進行](../Page/史密斯-第九街車站_\(IND卡爾弗線\).md "wikilink")。

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p><a href="../Page/R1_(紐約地鐵車輛).md" title="wikilink">R1</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a>（不久後換成<a href="http://nycsubway.org/perl/show?4793">1</a>）</p>
</div></td>
</tr>
</tbody>
</table>

</div>

## 歷史

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p>1967-1979年使用（圓形）</p>
</div></td>
</tr>
</tbody>
</table>

</div>

1933年8月19日，開行皇后廣場車站至納蘇大道車站的G線列車。

1937年7月1日，IND跨區線通車至卑爾根街車站並與[BMT克佛線相連](../Page/BMT克佛線.md "wikilink")，G線南訖站為史密斯街-第九街，列車沿皇后大道往七十一大道-州陸大道-森林高地車站而行。

1968年7月，列車延駛至教堂大道車站，線列車亦於[IND克佛線行快車](../Page/IND克佛線.md "wikilink")。但因許多在IND克佛線慢車車站上車的乘客會轉乘至[曼哈頓](../Page/曼哈頓.md "wikilink")，線服務於1976年8月部份停駛。

1987年5月24日，、兩線對調[皇后區的訖站](../Page/皇后區.md "wikilink")，線列車也於夜晚、凌晨及週末駛至皇后廣場車站。

1990年9月30日線於凌晨取代線，延駛至牙買加-179街車站。

1997年5月，因為[63街隧道](../Page/63街隧道.md "wikilink")（連接[IND63街線與](../Page/IND63街線.md "wikilink")[IND皇后大道線](../Page/IND皇后大道線.md "wikilink")）的興建工程，線於夜晚、凌晨及週末訖於長島市-法庭廣場車站。

2001年12月16日63街隧道通車，法庭廣場車站變成線中午與尖峰時段的北訖站，取代沿皇后大道行駛的線，其他時段經由63街隧道延駛IND皇后大道線。

## 路線

### 服務形式

G線列車在任何時候都在同一條路線上使用相同的服務形式。\[3\]

| 路線                                       | 起點                                               | 終點                                               | 軌道 |
| ---------------------------------------- | ------------------------------------------------ | ------------------------------------------------ | -- |
| [IND跨城線](../Page/IND跨城線.md "wikilink")   | [法庭廣場](../Page/法庭廣場車站_\(IND跨城線\).md "wikilink")  | [霍伊特-歇爾美角街](../Page/霍伊特-歇爾美角街車站.md "wikilink")   | 全部 |
| [IND卡爾弗線](../Page/IND卡爾弗線.md "wikilink") | [卑爾根街](../Page/卑爾根街車站_\(IND卡爾弗線\).md "wikilink") | [教堂大道](../Page/教堂大道車站_\(IND卡爾弗線\).md "wikilink") | 慢車 |

### 車站

更詳細的車站列表參見上方列出的路線。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-G.svg" title="fig:NYCS-bull-trans-G.svg">NYCS-bull-trans-G.svg</a></p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/皇后區.md" title="wikilink">皇后區</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND跨城線.md" title="wikilink">跨城線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/法庭廣場車站_(IND跨城線).md" title="wikilink">法庭廣場</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p>（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>），<a href="../Page/法庭廣場車站_(IRT法拉盛線).md" title="wikilink">法庭廣場</a><br />
（<a href="../Page/IND皇后林蔭路線.md" title="wikilink">IND皇后林蔭路線</a>，<a href="../Page/法庭廣場-23街車站_(IND皇后林蔭路線).md" title="wikilink">法庭廣場-23街</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/21街車站_(IND跨城線).md" title="wikilink">21街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/綠點大道車站_(IND跨城線).md" title="wikilink">綠點大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/納蘇大道車站_(IND跨城線).md" title="wikilink">納蘇大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/羅里默街／大都會大道車站.md" title="wikilink">大都會大道</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>，<a href="../Page/羅里默街／大都會大道車站.md" title="wikilink">羅里默街</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/百老匯車站_(IND跨城線).md" title="wikilink">百老匯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/法拉盛大道車站_(IND跨城線).md" title="wikilink">法拉盛大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/默特爾-威爾勞夫比大道車站_(IND跨城線).md" title="wikilink">默特爾-威爾勞夫比大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/貝德福德-諾斯特蘭大道車站_(IND跨城線).md" title="wikilink">貝德福德-諾斯特蘭大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/卡拉森大道車站_(IND跨城線).md" title="wikilink">卡拉森大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/克林頓-華盛頓大道車站_(IND跨城線).md" title="wikilink">克林頓-華盛頓大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/福爾頓街車站_(IND跨城線).md" title="wikilink">福爾頓街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/霍伊特-歇爾美角街車站_(IND跨城線).md" title="wikilink">霍伊特-歇爾美角街</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IND福爾頓街線.md" title="wikilink">IND福爾頓街線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/IND卡爾弗線.md" title="wikilink">卡爾弗線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/卑爾根街車站_(IND卡爾弗線).md" title="wikilink">卑爾根街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/卡爾洛街車站_(IND卡爾弗線).md" title="wikilink">卡爾洛街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/史密斯-第九街車站_(IND卡爾弗線).md" title="wikilink">史密斯-第九街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/第四大道／第九街車站.md" title="wikilink">第四大道</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT第四大道線.md" title="wikilink">BMT第四大道線</a>，<a href="../Page/第四大道／第九街車站.md" title="wikilink">第九街</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第七大道車站_(IND卡爾弗線).md" title="wikilink">第七大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/15街-展望公園車站_(IND卡爾弗線).md" title="wikilink">15街-展望公園</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/漢密爾頓堡公園道車站_(IND卡爾弗線).md" title="wikilink">漢密爾頓堡公園道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/教堂大道車站_(IND卡爾弗線).md" title="wikilink">教堂大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注释

## 参考文献

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20061028201725/http://mta.info/nyct/service/gline.htm)
  - [G線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit -
    G線時刻表（PDF）](https://web.archive.org/web/20060921230345/http://www.mta.info/nyct/service/pdf/tgcur.pdf)

[G](../Category/紐約地鐵服務路線.md "wikilink")
[G](../Category/布魯克林.md "wikilink")
[G](../Category/皇后區.md "wikilink")

1.
2.

3.