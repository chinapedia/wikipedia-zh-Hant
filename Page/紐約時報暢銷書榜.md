**《紐約時報》暢銷書榜**（）是美國最具代表性的[暢銷書排行榜之一](../Page/暢銷書.md "wikilink")\[1\]\[2\]，每週在星期日版《[紐約時報](../Page/紐約時報.md "wikilink")》隨報附送的《[紐約時報書評](../Page/紐約時報書評.md "wikilink")》雜誌（The
New York Times Book Review）刊載，而雜誌則同時接受單獨訂閱。暢銷書榜自1942年4月9日開始，一直刊載至今。

## 暢銷書排名榜

暢銷書排名榜並非由刊載此榜的書評部門制定，而是由紐約時報「新聞調查」（）部門的編輯負責。榜單上排名是根據全國抽樣調查的大小型書店以及批發商所提供的每周銷售報告制定而成。由於零售數據比起批發數據更能反映出真實的銷售情形，因此時報人員調查不少書店。\[3\]某些上榜的書本旁邊會加上一個[劍標型](../Page/劍標.md "wikilink")（†）符號，表示一些書店大量訂購此書。

排名榜的制定方法一直是個商業秘密。根據作家Edwin Diamond在其著作Behind the
Times指出，自1992年起，時報的調查對象涵蓋全國3000多家書店，以及合共多達28000個包括百貨公司和超級市場等銷售點的批發商。\[4\]

暢銷書榜共分為[小說及非小說兩個類別](../Page/小說.md "wikilink")，各類別分別羅列出排行頭10名或頭20名的著作，全部排名則上載至書評的網頁。由於大量如勵志書籍等提供生活意見的書籍進佔非小說類榜單，因此自1984年起，有關方面決定另外推出「Advice,
How-to and
Miscellaneous」「勵志·工具及其他書籍排行榜」。\[5\]小說《[哈利·波特](../Page/哈利·波特.md "wikilink")》推出後，曾高踞排名榜首位多個星期，惹來出版商投訴。有見及此，時報於2000年7月推出「兒童文學暢銷書榜」（）。\[6\]另外，2007年9月23日一期開始，時報把平裝（[Paperback](../Page/:en:Paperback.md "wikilink")）小說排行榜分拆成Trade
Fiction和Mass Fiction兩個項目，根據編輯的解釋是:「這樣有助讀者更留意雜誌經常評論的文學小說及短篇故事」。\[7\]

## 分析

曾研究[精裝版小說銷路的](../Page/精裝版.md "wikilink")[斯坦福商学院副教授Alan](../Page/斯坦福商学院.md "wikilink")
T.
Sorensen指出，大部分購買書本的人都似乎十分看重紐約時報的暢銷書榜。因此，對一些缺乏知名度的作家或新作家來說，暢銷書榜對他們的著作多少具有宣傳效用。相反，對一些如[Danielle
Steel和](../Page/:en:Danielle_Steel.md "wikilink")[約翰·葛里遜等暢銷書作家來說](../Page/約翰·葛里遜.md "wikilink")，上榜與否對他們小說的銷路影響不大。他又指，暢銷書榜刊出後對上榜書籍的銷售情況影響有限，它未必會使書本賣得更好，但可能會拖慢銷路逐漸下降的情形。\[8\]

## 相關條目

  - [畅销书列表](../Page/畅销书列表.md "wikilink")
  - [List of bestselling novels in the United
    States](../Page/:en:List_of_bestselling_novels_in_the_United_States.md "wikilink")
  - [Bestseller](../Page/:en:Bestseller.md "wikilink")

## 参考文献

## 外部連結

  - [紐約時報暢銷書榜](http://www.nytimes.com/pages/books/bestseller/)
  - [過往紐約時報暢銷書榜資料](http://www.hawes.com/pastlist.htm)
  - [Previous Fiction \#1 Best
    Sellers](http://www.hawes.com/no1_f_d.htm)
  - [Previous Non-Fiction \#1 Best
    Sellers](http://www.hawes.com/no1_nf_d.htm)
  - [Controversy Regarding New Children's
    List](https://web.archive.org/web/20090504135643/http://archive.salon.com/mwt/feature/2000/08/16/bestseller/index.html)

[銷](../Category/書籍列表.md "wikilink")

1.  [John Bear](../Page/:en:John_Bear.md "wikilink"), *The \#1 New York
    Times Best Seller: intriguing facts about the 484 books that have
    been \#1 New York Times bestsellers since the first list, 50 years
    ago*, Berkeley: Ten Speed Press, 1992.
2.  Rep. [Billy Tauzin](../Page/:en:Billy_Tauzin.md "wikilink") (R-LA),
    Chairman, Subcommittee on Telecommunications, Trade, and Consumer
    Protection, said "the *New York Times* best-seller list is widely
    considered to be one of the most authoritative lists of which books
    are selling the most in American bookstores" during his [Opening
    Statement for Hearing on
    H.R. 1858](http://www.techlawjournal.com/cong106/database/19990615tau.htm)
    on 1999年6月15日
3.  ["Blatty Sue Times on Best-Seller
    List"](http://query.nytimes.com/gst/fullpage.html?res=9B03E0D61438F93AA1575BC0A965948260),
    from *[The New York
    Times](../Page/:en:The_New_York_Times.md "wikilink")*,August 29,
    1983
4.  Edwin Diamond (1995). *Behind the Times: Inside the New New York
    Times*.
    [Page 364](http://books.google.com/books?vid=ISBN0226144720&id=LAfZ-Y13obwC&pg=PA364&lpg=PA364&ots=XQAwzfMECJ&dq=%22New+York+Times+Best+Seller+list%22&sig=xSluZ32O22b7mQFid3CISEltXDU#PPA364,M1)
5.  *New York Times Book Review*, "TBR: Inside the list". February 24,
    2008. page 26.
6.  [Bestseller
    Math](http://www.riverdeep.net/current/2001/11/111201t_top10.jhtml).
    November 12, 2001.
7.  *The New York Times Book Review*, "Up Front", pg.4 - September 23,
    2007
8.  ["Readers Tap Best-Seller List for New
    Authors"](http://www.gsb.stanford.edu/news/bmag/sbsm0502/research_sorensen_consumers.shtml)
    , *Stanford Business Magazine*, February 2005. Last accessed
    December 2006. See also Alan T. Sorensen, *Bestseller Lists and
    Product Variety: The Case of Book Sales*, May 2004.