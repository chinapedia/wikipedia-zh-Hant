**素叻他尼府**\[1\]（，），是[泰国南部各府份当中](../Page/泰国.md "wikilink")[人口最多的一府](../Page/人口.md "wikilink")。簡稱**素叻府**（），華人稱之為**萬倫府**。

她位于[马来亚半岛东海岸](../Page/马来亚半岛.md "wikilink")，濒临[泰国湾](../Page/泰国湾.md "wikilink")。「素叻他尼」在[梵文中的意思是](../Page/梵文.md "wikilink")「好人居住的城市」。该称号由[泰国国王](../Page/泰国国王.md "wikilink")[拉玛六世于](../Page/拉玛六世.md "wikilink")1915年7月册封给素叻府的，名字的由来是[印度](../Page/印度.md "wikilink")[马哈拉施特拉邦的同名城市](../Page/马哈拉施特拉邦.md "wikilink")。一个月之后，拉瑪六世又再度下詔将流经该府城的河流命名为[塔彼河](../Page/塔彼河.md "wikilink")（[Tapi
river](../Page/Tapi_river.md "wikilink")），恰恰也是流經那个印度城市里的河流名字。

## 地理位置

素叻府与以下府份相接邻（以顺时针为序）：[那空是贪玛叻府](../Page/那空是贪玛叻府.md "wikilink")、[甲米府](../Page/甲米府.md "wikilink")、[攀牙府](../Page/攀牙府.md "wikilink")、[拉廊府](../Page/拉廊府.md "wikilink")、[春蓬府](../Page/春蓬府.md "wikilink")。
流经素叻府的最大河流为[塔彼河](../Page/塔彼河.md "wikilink")（แม่น้ำตาปี，Tapi
river），地理上塔彼河流域平原位於府的正中央。
素叻他尼府城距离[泰国](../Page/泰国.md "wikilink")[首都](../Page/首都.md "wikilink")[曼谷](../Page/曼谷.md "wikilink")462公里，有现代化的公路和铁路相通。

[Thailand_Mueang_Surat.png](https://zh.wikipedia.org/wiki/File:Thailand_Mueang_Surat.png "fig:Thailand_Mueang_Surat.png")

离岸有一些面积较大的岛屿，如：[阁沙梅岛](../Page/阁沙梅岛_\(素叻他尼府\).md "wikilink")（Koh
Samui，苏梅岛）、[閣帕岸島](../Page/閣帕岸島.md "wikilink")（Koh
Pha-ngan）、[閣道島](../Page/閣道島.md "wikilink")（Koh
Tao，龜島）、[红统群岛](../Page/红统群岛.md "wikilink")（Koh Ang
Thong，该岛包括素叻府中最大的[红统海洋公园](../Page/红统海洋公园.md "wikilink")--[Koh Ang Thong
marine national
park](../Page/Koh_Ang_Thong_marine_national_park.md "wikilink")）。

## 人口

素叻府人口为965,800人（2007年最新统计数字），为泰国全境76府中第22位，泰国南部第1位。

素叻府内最大的城市是素叻府城，今天人口大约有125,900。

## 素叻经济

[Surat_Thani_shrine.jpg](https://zh.wikipedia.org/wiki/File:Surat_Thani_shrine.jpg "fig:Surat_Thani_shrine.jpg")
in Surat Thani\]\]
根据2005年泰国经济调查，当年素叻府的[GPP](../Page/GPP.md "wikilink")（即总初始生产值，Gross
Primary
production）是940亿[泰铢](../Page/泰铢.md "wikilink")，折算为25.41亿[美元](../Page/美元.md "wikilink")。人均GPP为97,936泰铢即2,647美元。
主要农作物为[椰子和](../Page/椰子.md "wikilink")[红毛丹](../Page/红毛丹.md "wikilink")。素叻府有特别的[猴子训练学校](../Page/猴子.md "wikilink")，训练它们爬到高高的椰子树上把椰子拔下来，可称作此地一特色。

红毛丹于1926年由一个叫K·王的马来亚华侨引进，最早种植于素叻府纳森村（Ban Na
San）。每年八月府城内外举行红毛丹节，节日中的最大盛况就是[他彼河上的竞赛彩船](../Page/他彼河.md "wikilink")（Float
on the Tapi River）。

## 府徽

素叻府府徽展示一座庙宇，这座庙座落于素叻府差也城（Chaiya），是府中年代最古老的庙宇，相信已经有一千二百年历史。

素叻府的旗帜也展示同一庙宇，旗色是上红下黄。

府花的泰文名为： Bua Phut（学名为：Rafflesia kerrii）

府树的泰文名为：Ton Kiam（学名为：Cotylelobium melanoxylon）

素叻府标语为：เมืองร้อยเกาะ เงาะอร่อย หอยใหญ่ ไข่แดง แหล่งธรรมะ

英文译文：

  -
    City of 100 islands, delicious rambutan, big shells and red eggs,
    center of Buddhism.
    Red eggs are a local culinary speciality of pickled duck eggs, while
    the big shells refer to the plenty of seafood available. Center of
    Buddhism refers to the pagoda of Chaiya.

中文译文：尚无。

## 行政与管理

素叻府行政上划分为18个县（Amphoe）和一个次级县（King
Amphoe），又进一步被划分为131个区（Tambon）及1028个村（Muban）。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>中文名</p></th>
<th><p>泰文名</p></th>
<th><p>英文名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>เมืองสุราษฎร์ธานี</p></td>
<td><p>Mueang Surat Thani</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>กาญจนดิษฐ์</p></td>
<td><p>Kanchanadit</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>ดอนสัก</p></td>
<td><p>Don Sak</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/阁沙梅岛_(素叻他尼府).md" title="wikilink">阁沙梅县</a></p></td>
<td><p>เกาะสมุย</p></td>
<td><p>Ko Samui</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/閣帕岸島.md" title="wikilink">阁帕岸县</a></p></td>
<td><p>เกาะพะงัน</p></td>
<td><p>Ko Pha-ngan</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>ไชยา</p></td>
<td><p>Chaiya</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>ท่าชนะ</p></td>
<td><p>Tha Chana</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>คีรีรัฐนิคม</p></td>
<td><p>Khiri Rat Nikhom</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>บ้านตาขุน</p></td>
<td><p>Ban Ta Khun</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>พนม</p></td>
<td><p>Phanom</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>ท่าฉาง</p></td>
<td><p>Tha Chang</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>บ้านนาสาร</p></td>
<td><p>Ban Na San</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>บ้านนาเดิม</p></td>
<td><p>Ban Na Doem</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>เคียนซา</p></td>
<td><p>Khian Sa</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>เวียงสระ</p></td>
<td><p>Wiang Sa</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>พระแสง</p></td>
<td><p>Phrasaeng</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>พุนพิน</p></td>
<td><p>Phunphin</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td></td>
<td><p>ชัยบุรี</p></td>
<td><p>Chai Buri</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p>วิภาวดี</p></td>
<td><p>Vibhavadi</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Amphoe_Surat_Thani.svg" title="fig:Amphoe_Surat_Thani.svg">Amphoe_Surat_Thani.svg</a></p></td>
</tr>
</tbody>
</table>

## 交通

  - [素叻國際機場](../Page/素叻國際機場.md "wikilink")
  - [蘇梅機場](../Page/蘇梅機場.md "wikilink")

## 參考資料

## 外部链接

  - 泰国王家旅游局府素叻他尼府专页

<!-- end list -->

  - [素叻他尼府政府官方网站（泰文）](http://www.suratthani.go.th)

<!-- end list -->

  - 素叻他尼府地图、府徽、邮政编码图

[素叻他尼府](../Category/素叻他尼府.md "wikilink")
[Category:泰國南部](../Category/泰國南部.md "wikilink")

1.