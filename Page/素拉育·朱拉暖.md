**素拉育·朱拉暖**（；），1943年出生于[泰國](../Page/泰國.md "wikilink")[佛丕府](../Page/佛丕府.md "wikilink")，曾任陆军总司令，退役后任[泰国枢密院大臣](../Page/泰国枢密院.md "wikilink")。2006年10月1日，他經[颂提将军发动](../Page/颂提.md "wikilink")[军事政变後的泰国管理改革委员会任命为](../Page/2006年泰國軍事政變.md "wikilink")[泰国首相](../Page/泰国首相.md "wikilink")。

## 军旅生涯

  - 1965年参军，服役于陆军部队，并加入泰国特种部队，指挥泰国在[柬埔寨的秘密行动](../Page/柬埔寨.md "wikilink")。
  - 1992年，担任泰国特种作战司令部司令。
  - 1998年，晋升为陆军总司令。
  - 2003年，退役。退役前曾短暂担任泰軍最高司令。

## 文职生涯

  - 2003年11月，受[泰国国王御令担任枢密院大臣](../Page/蒲美蓬.md "wikilink")。
  - 2006年10月1日，受泰王御令，出任[泰国临时首相](../Page/泰國首相.md "wikilink")。

## 相关链接

  - 泰国国会：[1](http://www.parliament.go.th/)
  - [2006年泰國軍事政變](../Page/2006年泰國軍事政變.md "wikilink")

[Category:泰国总理](../Category/泰国总理.md "wikilink")
[Category:靠政變上台的領導人](../Category/靠政變上台的領導人.md "wikilink")
[Category:泰國政治人物](../Category/泰國政治人物.md "wikilink")
[Category:泰國軍事人物](../Category/泰國軍事人物.md "wikilink")
[Category:泰國佛教徒](../Category/泰國佛教徒.md "wikilink")
[Category:軍人出身的政府首腦](../Category/軍人出身的政府首腦.md "wikilink")