**索伦托**（Sorrento，；[那不勒斯语](../Page/那不勒斯语.md "wikilink")：Surriento；又译“苏莲托”）是[意大利](../Page/意大利.md "wikilink")[坎帕尼亚区下的一个市镇](../Page/坎帕尼亚.md "wikilink")，人口約為16,500人。索倫托位于[索伦托半岛北岸](../Page/索伦托半岛.md "wikilink")，濒[那不勒斯湾](../Page/那不勒斯湾.md "wikilink")，北距[那不勒斯](../Page/那不勒斯.md "wikilink")27公里，是一個觀光勝地。

## 概况

索伦托为[意大利南部城镇](../Page/意大利.md "wikilink")，属[坎帕尼亚区](../Page/坎帕尼亚.md "wikilink")。意大利名曲《》（Torna
a Surriento，作词，作曲。词、曲作者是兄弟。）中作者所向往的即是这里。

城筑于海滨的峭壁上，为橘、柠檬、油橄榄与桑等树丛所围绕，为一景色绮丽的游览区。主产葡萄酒与檄榄油，捕鱼亦较重要。细木镶嵌、花边等手工艺品著名。有十四世纪修道院与中世纪雕刻、绘画艺术。

[Sorrento_from_Piazza_Tasso.jpg](https://zh.wikipedia.org/wiki/File:Sorrento_from_Piazza_Tasso.jpg "fig:Sorrento_from_Piazza_Tasso.jpg")看阶梯下方从小港进城的道路\]\]
[SORVESUV.JPG](https://zh.wikipedia.org/wiki/File:SORVESUV.JPG "fig:SORVESUV.JPG")与[维苏威火山的景色](../Page/维苏威火山.md "wikilink")\]\]

## 景点

  - [阿马尔菲海岸](../Page/阿马尔菲海岸.md "wikilink")
  - [大港](../Page/大港_\(索伦托\).md "wikilink")（Marina Grande）
  - 小港（Marina Piccola）
  - 市政公园（*Villa
    communale*），可观赏[那不勒斯湾与](../Page/那不勒斯湾.md "wikilink")[维苏威火山的景色](../Page/维苏威火山.md "wikilink")
  - [塔索广场](../Page/塔索广场_\(索伦托\).md "wikilink")，索伦托的中央广场
  - Museo della tarsia lignea
  - [科雷亚莱博物馆](../Page/科雷亚莱博物馆.md "wikilink")\[1\]
  - [圣凯撒略街](../Page/圣凯撒略街.md "wikilink"), 索伦托的主要购物街
  - [索伦托圣斐理伯圣雅各伯主教座堂](../Page/索伦托圣斐理伯圣雅各伯主教座堂.md "wikilink")，14世纪，立面重建于1924年，11世纪的大门来自[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")\[2\]
  - [圣腓力和巴科洛堂](../Page/圣腓力和巴科洛堂.md "wikilink")
  - [圣方济各堂及修道院](../Page/圣方济各堂_\(索伦托\).md "wikilink")，14世纪
  - *Punta del Capo*罗马废墟

## 外部連結

  - [WikiMapia](http://www.wikimapia.org/#lat=40.6299787&lon=14.3699455&z=14&l=4&m=a&v=2)
  - [Sorrento Events](http://eventssorrento.com/)

[S](../Category/那不勒斯城市.md "wikilink")
[索倫托](../Category/索倫托.md "wikilink")

1.  [museocorreale.it](http://www.museocorreale.it/)
2.  [cattedralesorrento.it](http://www.cattedralesorrento.it/)