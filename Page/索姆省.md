**索姆省**（）是[法國](../Page/法國.md "wikilink")[上法兰西大区所轄的](../Page/上法兰西大区.md "wikilink")[省份](../Page/省_\(法国\).md "wikilink")，位于法国北部，由境内的[索姆河得名](../Page/索姆河.md "wikilink")。該省編號為80。

## 历史

[第一次世界大战中著名的](../Page/第一次世界大战.md "wikilink")[索姆河战役就发生于此](../Page/索姆河战役.md "wikilink")。

## 城市

| 排名 | 市镇                                 | 区                                    | 人口      |
| -- | ---------------------------------- | ------------------------------------ | ------- |
| 1  | [亚眠](../Page/亚眠.md "wikilink")     | [亚眠区](../Page/亚眠区.md "wikilink")     | 132,699 |
| 2  | [阿布维尔](../Page/阿布维尔.md "wikilink") | [阿布维尔区](../Page/阿布维尔区.md "wikilink") | 23,821  |
| 3  | [阿尔贝](../Page/阿尔贝.md "wikilink")   | [佩罗讷区](../Page/佩罗讷区.md "wikilink")   | 10,054  |
| 4  | [佩罗讷](../Page/佩罗讷.md "wikilink")   | [佩罗讷区](../Page/佩罗讷区.md "wikilink")   | 7,702   |
| 5  | [杜朗](../Page/杜朗.md "wikilink")     | [亚眠区](../Page/亚眠区.md "wikilink")     | 6,497   |

**索姆省人口最多的市镇**\[1\]

## 参考资料

[Category:法国省份](../Category/法国省份.md "wikilink")
[S](../Category/上法蘭西大區.md "wikilink")

1.