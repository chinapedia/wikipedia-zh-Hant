**索尔·A·图科斯基**（，），[美国物理学家](../Page/美国.md "wikilink")，自2007年4月3日起担任[康奈尔大学的物理系主任](../Page/康奈尔大学.md "wikilink")。图科斯基是研究[数值相对论](../Page/数值相对论.md "wikilink")（运用[计算机的数值方法求解](../Page/计算机.md "wikilink")[爱因斯坦场方程](../Page/爱因斯坦场方程.md "wikilink")）的先驱之一。

## 学术生涯

他于[二十世纪](../Page/二十世纪.md "wikilink")[七十年代师从](../Page/七十年代.md "wikilink")[加州理工学院的](../Page/加州理工学院.md "wikilink")[基普·索恩教授](../Page/基普·索恩.md "wikilink")，其后进入康奈尔大学任教，1983年起获得康奈尔大学的[汉斯·贝特物理与天体物理学教授称号](../Page/汉斯·贝特.md "wikilink")。主要研究领域为[广义相对论和相对论天体物理学](../Page/广义相对论.md "wikilink")、数值相对论、[黑洞与](../Page/黑洞.md "wikilink")[中子星物理](../Page/中子星.md "wikilink")、[计算物理等](../Page/计算物理.md "wikilink")，主要成果为在二十世纪七十年代创立的[图科斯基方程](../Page/图科斯基方程.md "wikilink")，这是在[克尔度规框架下求解有微扰场作用的爱因斯坦场方程的基本数值方法](../Page/克尔度规.md "wikilink")。

目前他的研究小组重点研究运用数值方法求解有可能被[激光干涉引力波天文台](../Page/激光干涉引力波天文台.md "wikilink")（）和[激光干涉空间天线](../Page/激光干涉空间天线.md "wikilink")（）探测到的[引力波的波形](../Page/引力波.md "wikilink")。同时图科斯基也是在[数学](../Page/数学.md "wikilink")、计算物理和[工程领域影响力很大的著名丛书](../Page/工程.md "wikilink")《[数值算法](../Page/数值算法.md "wikilink")》系列（）的作者之一。

## 参考和外部链接

  - [Saul
    Teukolsky在康奈尔大学物理系的介绍网页](http://www.physics.cornell.edu/people/faculty/?page=website/faculty&action=show/id=44)

[T](../Category/美国物理学家.md "wikilink")
[T](../Category/金山大学校友.md "wikilink")
[T](../Category/加州理工學院校友.md "wikilink")