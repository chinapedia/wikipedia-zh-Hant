<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

**索恩一世**（**Thorin
I**），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·瑞爾·托爾金](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")（J.R.R.
Tolkien）的史詩式奇幻小說《[魔戒三部曲](../Page/魔戒三部曲.md "wikilink")》中的虛構人物。

他是都靈子民的國王，放棄了古老的家園[凱薩督姆](../Page/凱薩督姆.md "wikilink")（Khazad-dûm），遷到[伊魯伯](../Page/孤山_\(托爾金小說\).md "wikilink")（Erebor）建立了山下王國。

## 名字

**索恩**（Thráin）一名來自於現實中的北歐神話詩歌*Voluspa*，有著「堅毅」*resolute*的意思。\[1\]

## 總覽

索恩一世是[矮人](../Page/矮人_\(中土大陸\).md "wikilink")（Dwarves）。他的父親是[耐恩一世](../Page/耐恩一世.md "wikilink")（Náin
I）。不清楚他有沒有任何兄弟姊妹。他有一位兒子，[索林](../Page/索林一世.md "wikilink")（Thorin）。\[2\]

他是[都靈子民的國王](../Page/都靈_\(中土大陸\).md "wikilink")，繼承了父祖在凱薩督姆的統治。在他祖父統治末年，矮人因為想挖取更多[秘銀](../Page/秘銀.md "wikilink")（Mithril）而不慎喚醒了潛伏在迷霧山脈深處的[炎魔](../Page/都靈的剋星.md "wikilink")（Barlog），並殺了他的祖父和父親，逼使他帶著都靈一族的矮人逃離凱薩督姆。此後他率領族人遷至孤山，建立了山下王國（Kingdom
under the Mountain）。

索恩一世生於第三紀元1934年，死於2190年，享年256歲。\[3\]

## 生平

索恩一世生於[第三紀元](../Page/第三紀元.md "wikilink")1934年，\[4\]應該是在凱薩督姆出生。1981年，炎魔殺害他的父親耐恩一世，\[5\]他繼承統領凱薩督姆的矮人的責任。但此時矮人已經無力驅逐敵人，最終索恩一世唯有率族人逃離自己的家園。\[6\]

十八年以後（1999年），都靈一族遷移至伊魯伯，建立了山下王國。\[7\]他們在伊魯伯挖掘到[家傳寶鑽](../Page/家傳寶鑽.md "wikilink")（Arkenstone），\[8\]並逐漸回復實力和財富。第三紀元2190年，索恩一世應該是因自然原因而逝世，\[9\]結束了209年的領導，由他的兒子索林繼位。

## 參考

  - 《[魔戒三部曲](../Page/魔戒三部曲.md "wikilink")》及其附錄

## 資料來源

[en:List of Middle-earth Dwarves\#Thráin
I](../Page/en:List_of_Middle-earth_Dwarves#Thráin_I.md "wikilink")
[pl:Thráin I](../Page/pl:Thráin_I.md "wikilink")

[Category:中土大陸的角色](../Category/中土大陸的角色.md "wikilink")
[Category:中土大陸的矮人](../Category/中土大陸的矮人.md "wikilink")
[Category:魔戒三部曲中的人物](../Category/魔戒三部曲中的人物.md "wikilink")

1.  [Thain's Book: Thráin
    I](http://www.tuckborough.net/dwarves.html#Thráin1)

2.
3.
4.
5.
6.
7.  《[魔戒三部曲](../Page/魔戒三部曲.md "wikilink")》 2001年 聯經初版翻譯 附錄二編年史

8.
9.