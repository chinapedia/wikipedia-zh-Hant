**索斯诺维茨**（[波兰语](../Page/波兰语.md "wikilink")：****）是[波兰南部的一座城市](../Page/波兰.md "wikilink")，与[卡托维茨相邻](../Page/卡托维茨.md "wikilink")，附近在采矿和工业区。该市为县级市，是[西里西亚省和](../Page/西里西亚省.md "wikilink")[上西里西亚工业区最大的城市之一](../Page/上西里西亚工业区.md "wikilink")，2006年人口225,202。

该市名称又常拼作“Sosnowietz”、“Sosnowitz”、“Sosnovitz”（[意第绪语](../Page/意第绪语.md "wikilink")）、“Sosnovyts”、“Sosnowyts”、“Sosnovytz”、“Sosnowytz”、“Sosnovets”（[俄语](../Page/俄语.md "wikilink")）或“Sosnovetz”。

## 文化設施

  - Teatr Zagłębia
  - Sosnowieckie Centrum Sztuki - 藝術中心
  - Energetyczne Centrum Kultury - 文化中心
  - Egzotarium - 植物園

## 公園和自然保護區

  - Sielecki - 歷史悠久的城堡公園
  - Dietla - 歷史悠久的公園
  - Srodula -
  - Torfowisko Bory - 自然保護區

## 旅遊和古蹟

  - 城堡 Sielecki
  - 宮 Deitl
  - 宮 Schöen
  - 新宮 Schöen
  - 福音派教會
  - 東正教會

## 友好城市

  - [法国](../Page/法国.md "wikilink")[莱米罗](../Page/莱米罗.md "wikilink")

  - [罗马尼亚](../Page/罗马尼亚.md "wikilink")[苏恰瓦](../Page/苏恰瓦.md "wikilink")

  - [法国](../Page/法国.md "wikilink")[鲁贝](../Page/鲁贝.md "wikilink")

  - [匈牙利](../Page/匈牙利.md "wikilink")[科马罗姆](../Page/科马罗姆.md "wikilink")

## 外部链接

  - [索斯诺维茨官方网站](http://www.sosnowiec.pl/)

[Sosnowiec](../Category/波兰城市.md "wikilink")