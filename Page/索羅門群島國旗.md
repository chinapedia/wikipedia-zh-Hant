**[所罗门群岛国旗](../Page/所罗门群岛.md "wikilink")**呈横长方形，长与宽之比为9:5。旗地由淺藍、綠色兩個三角形構成。一道黃色寬條從靠旗桿一邊的下旗角與旗地右上角連接。靠旗桿一邊的淺藍色部分有五顆白色的五角星，上、下排各兩顆，中間為一顆。

淺藍色象徵[藍天和環繞索羅門群島的](../Page/藍天.md "wikilink")[海洋](../Page/海洋.md "wikilink")，黄色代表太阳，綠色象徵覆蓋這個島國的[森林](../Page/森林.md "wikilink")。五顆星象徵組成這個島國的五個區域島群，即東、西、中央、馬萊塔和外圍的島群\[1\]。

<File:Civil> Ensign of the Solomon
Islands.svg|[FIAV_000100.svg](https://zh.wikipedia.org/wiki/File:FIAV_000100.svg "fig:FIAV_000100.svg")
民船旗 比例: 1:2 <File:Government> Ensign of the Solomon
Islands.svg|[FIAV_000010.svg](https://zh.wikipedia.org/wiki/File:FIAV_000010.svg "fig:FIAV_000010.svg")
政府船旗 比例: 1:2 <File:Naval> Ensign of the Solomon
Islands.svg|[FIAV_000001.svg](https://zh.wikipedia.org/wiki/File:FIAV_000001.svg "fig:FIAV_000001.svg")
軍艦旗 比例: 1:2 <File:Customs> Ensign of the Solomon
Islands.svg|[FIAV_000010.svg](https://zh.wikipedia.org/wiki/File:FIAV_000010.svg "fig:FIAV_000010.svg")
海关旗 比例: 1:2

## 参考文献

[S](../Category/国旗.md "wikilink")
[Category:索羅門群島](../Category/索羅門群島.md "wikilink")
[Category:1977年面世的旗幟](../Category/1977年面世的旗幟.md "wikilink")

1.