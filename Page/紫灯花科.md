**紫灯花科**只有十几[属](../Page/属.md "wikilink")，全部生长在[美洲](../Page/美洲.md "wikilink")。

一般分类学家以前把本[科各属](../Page/科.md "wikilink")[植物都分到](../Page/植物.md "wikilink")[百合科内](../Page/百合科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其单独列为一个科](../Page/APG_分类法.md "wikilink")，放在[天门冬目中](../Page/天门冬目.md "wikilink")，2003年经过修订的[APG
II
分类法认为其仍然可以选择性地和](../Page/APG_II_分类法.md "wikilink")[天门冬科合并](../Page/天门冬科.md "wikilink")。

## 属

  - *[Androstephium](../Page/Androstephium.md "wikilink")*
  - *[Bessera](../Page/Bessera.md "wikilink")* (*Behria*)
  - *[Bloomeria](../Page/Bloomeria.md "wikilink")*
  - *[Brodiaea](../Page/Brodiaea.md "wikilink")*
  - *[Dandya](../Page/Dandya.md "wikilink")*
  - *[Dichelostemma](../Page/Dichelostemma.md "wikilink")*
    (*Brevoortia*)
  - *[Milla](../Page/Milla.md "wikilink")* (*Diphalangium*)
  - *[Petronymphe](../Page/Petronymphe.md "wikilink")*
  - *[Triteleia](../Page/Triteleia.md "wikilink")* (*Hesperoscordium*,
    *Themis*)
  - *[Triteleiopsis](../Page/Triteleiopsis.md "wikilink")*

## 参考文献

  - Michael F. Fay and Mark W. Chase. 1996. Resurrection of Themidaceae
    for the Brodiaea alliance, and Recircumscription of Alliaceae,
    Amaryllidaceae and Agapanthoideae. *Taxon* 45: 441-451
    ([abstract](http://links.jstor.org/sici?sici=0040-0262\(199608\)45%3A3%3C441%3AROTFTB%3E2.0.CO%3B2-M#abstract))

## 外部链接

:\*
[NCBI中的紫灯花科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=158486&lvl=3&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/紫灯花科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")