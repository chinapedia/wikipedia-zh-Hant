**紫羅蘭屬**（[学名](../Page/学名.md "wikilink")：）是[十字花科下的一个](../Page/十字花科.md "wikilink")[属](../Page/属.md "wikilink")，为[一年生至](../Page/一年生.md "wikilink")[多年生](../Page/多年生.md "wikilink")[草本](../Page/草本.md "wikilink")[植物](../Page/植物.md "wikilink")，有时为[亚灌木状](../Page/亚灌木.md "wikilink")。该[属約有](../Page/属.md "wikilink")48
\[1\] 至50個種。\[2\]\[3\]
分布于[地中海区](../Page/地中海.md "wikilink")、[欧洲](../Page/欧洲.md "wikilink")、[亚洲及](../Page/亚洲.md "wikilink")[南非](../Page/南非.md "wikilink")。\[4\]

## 種

[Matthiola_longipetala.jpg](https://zh.wikipedia.org/wiki/File:Matthiola_longipetala.jpg "fig:Matthiola_longipetala.jpg")*\]\]
[Ponta_de_São_Lourenço_(north)_-_May_2007.jpg](https://zh.wikipedia.org/wiki/File:Ponta_de_São_Lourenço_\(north\)_-_May_2007.jpg "fig:Ponta_de_São_Lourenço_(north)_-_May_2007.jpg")*\]\]
[Matthiola_tricuspidata2.jpg](https://zh.wikipedia.org/wiki/File:Matthiola_tricuspidata2.jpg "fig:Matthiola_tricuspidata2.jpg")''\]\]

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><em><a href="../Page/Matthiola_acaulis.md" title="wikilink">Matthiola acaulis</a></em></li>
<li><em><a href="../Page/Matthiola_adenophora.md" title="wikilink">Matthiola adenophora</a></em></li>
<li><em><a href="../Page/Matthiola_afghanica.md" title="wikilink">Matthiola afghanica</a></em></li>
<li><em><a href="../Page/Matthiola_albicaulis.md" title="wikilink">Matthiola albicaulis</a></em></li>
<li><em><a href="../Page/Matthiola_alyssifolia.md" title="wikilink">Matthiola alyssifolia</a></em></li>
<li><em><a href="../Page/Matthiola_anchoniifolia.md" title="wikilink">Matthiola anchoniifolia</a></em></li>
<li><em><a href="../Page/Matthiola_anchusoides.md" title="wikilink">Matthiola anchusoides</a></em></li>
<li><em><a href="../Page/Matthiola_angulosa.md" title="wikilink">Matthiola angulosa</a></em></li>
<li><em><a href="../Page/Matthiola_annua.md" title="wikilink">Matthiola annua</a></em></li>
<li><em><a href="../Page/Matthiola_anoplia.md" title="wikilink">Matthiola anoplia</a></em></li>
<li><em><a href="../Page/Matthiola_arabica.md" title="wikilink">Matthiola arabica</a></em></li>
<li><em><a href="../Page/Matthiola_arborescens.md" title="wikilink">Matthiola arborescens</a></em></li>
<li><em><a href="../Page/Matthiola_aspera.md" title="wikilink">Matthiola aspera</a></em></li>
<li><em><a href="../Page/Matthiola_australis.md" title="wikilink">Matthiola australis</a></em></li>
<li><em><a href="../Page/Matthiola_bicornis.md" title="wikilink">Matthiola bicornis</a></em></li>
<li><em><a href="../Page/Matthiola_biennis.md" title="wikilink">Matthiola biennis</a></em></li>
<li><em><a href="../Page/Matthiola_boissieri.md" title="wikilink">Matthiola boissieri</a></em></li>
<li><em><a href="../Page/Matthiola_bolleana.md" title="wikilink">Matthiola bolleana</a></em></li>
<li><em><a href="../Page/Matthiola_bucharica.md" title="wikilink">Matthiola bucharica</a></em></li>
<li><em><a href="../Page/Matthiola_caboverdeana.md" title="wikilink">Matthiola caboverdeana</a></em></li>
<li><em><a href="../Page/Matthiola_capiomontiana.md" title="wikilink">Matthiola capiomontiana</a></em></li>
<li><em><a href="../Page/Matthiola_carnica.md" title="wikilink">Matthiola carnica</a></em></li>
<li><em><a href="../Page/Matthiola_caspica.md" title="wikilink">Matthiola caspica</a></em></li>
<li><em><a href="../Page/Matthiola_chenopodifolia.md" title="wikilink">Matthiola chenopodifolia</a></em></li>
<li><em><a href="../Page/Matthiola_chenopodiifolia.md" title="wikilink">Matthiola chenopodiifolia</a></em></li>
<li><em><a href="../Page/Matthiola_chorassanica.md" title="wikilink">Matthiola chorassanica</a></em></li>
<li><em><a href="../Page/Matthiola_circinnata.md" title="wikilink">Matthiola circinnata</a></em></li>
<li><em><a href="../Page/Matthiola_clausonis.md" title="wikilink">Matthiola clausonis</a></em></li>
<li><em><a href="../Page/Matthiola_codringtonii.md" title="wikilink">Matthiola codringtonii</a></em></li>
<li><em><a href="../Page/Matthiola_coronopifolia.md" title="wikilink">Matthiola coronopifolia</a></em></li>
<li><em><a href="../Page/Matthiola_crassifolia.md" title="wikilink">Matthiola crassifolia</a></em></li>
<li><em><a href="../Page/Matthiola_crucigera.md" title="wikilink">Matthiola crucigera</a></em></li>
<li><em><a href="../Page/Matthiola_czerniakowskae.md" title="wikilink">Matthiola czerniakowskae</a></em></li>
<li><em><a href="../Page/Matthiola_daghestanica.md" title="wikilink">Matthiola daghestanica</a></em></li>
<li><em><a href="../Page/Matthiola_damascena.md" title="wikilink">Matthiola damascena</a></em></li>
<li><em><a href="../Page/Matthiola_deflexa.md" title="wikilink">Matthiola deflexa</a></em></li>
<li><em><a href="../Page/Matthiola_dentata.md" title="wikilink">Matthiola dentata</a></em></li>
<li><em><a href="../Page/Matthiola_deserticola.md" title="wikilink">Matthiola deserticola</a></em></li>
<li><em><a href="../Page/Matthiola_dimolehensis.md" title="wikilink">Matthiola dimolehensis</a></em></li>
<li><em><a href="../Page/Matthiola_dimorpha.md" title="wikilink">Matthiola dimorpha</a></em></li>
<li><em><a href="../Page/Matthiola_dumulosa.md" title="wikilink">Matthiola dumulosa</a></em></li>
<li><em><a href="../Page/Matthiola_elliptica.md" title="wikilink">Matthiola elliptica</a></em></li>
<li><em><a href="../Page/Matthiola_erlangeriana.md" title="wikilink">Matthiola erlangeriana</a></em></li>
<li><em><a href="../Page/Matthiola_euboea.md" title="wikilink">Matthiola euboea</a></em></li>
<li><em><a href="../Page/Matthiola_exigua.md" title="wikilink">Matthiola exigua</a></em></li>
<li><em><a href="../Page/Matthiola_farinosa.md" title="wikilink">Matthiola farinosa</a></em></li>
<li><em><a href="../Page/Matthiola_fasciculata.md" title="wikilink">Matthiola fasciculata</a></em></li>
<li><em><a href="../Page/Matthiola_fenestralis.md" title="wikilink">Matthiola fenestralis</a></em></li>
<li><em><a href="../Page/Matthiola_fischeri.md" title="wikilink">Matthiola fischeri</a></em></li>
<li><em><a href="../Page/Matthiola_flavida.md" title="wikilink">Matthiola flavida</a></em></li>
<li><em><a href="../Page/Matthiola_fragrans.md" title="wikilink">Matthiola fragrans</a></em></li>
<li><em><a href="../Page/Matthiola_fruticulosa.md" title="wikilink">Matthiola fruticulosa</a></em></li>
<li><em><a href="../Page/Matthiola_ghorana.md" title="wikilink">Matthiola ghorana</a></em></li>
<li><em><a href="../Page/Matthiola_glabra.md" title="wikilink">Matthiola glabra</a></em></li>
<li><em><a href="../Page/Matthiola_glabrata.md" title="wikilink">Matthiola glabrata</a></em></li>
<li><em><a href="../Page/Matthiola_glandulosa.md" title="wikilink">Matthiola glandulosa</a></em></li>
<li><em><a href="../Page/Matthiola_glutinosa.md" title="wikilink">Matthiola glutinosa</a></em></li>
<li><em><a href="../Page/Matthiola_graeca.md" title="wikilink">Matthiola graeca</a></em></li>
<li><em><a href="../Page/Matthiola_graminea.md" title="wikilink">Matthiola graminea</a></em></li>
<li><em><a href="../Page/Matthiola_hesperoides.md" title="wikilink">Matthiola hesperoides</a></em></li>
<li><em><a href="../Page/Matthiola_humilis.md" title="wikilink">Matthiola humilis</a></em></li>
<li><a href="../Page/紫罗兰.md" title="wikilink">紫罗兰</a> <em>Matthiola incana</em></li>
<li><em><a href="../Page/Matthiola_integrifolia.md" title="wikilink">Matthiola integrifolia</a></em></li>
<li><em><a href="../Page/Matthiola_kralikii.md" title="wikilink">Matthiola kralikii</a></em></li>
<li><em><a href="../Page/Matthiola_lacera.md" title="wikilink">Matthiola lacera</a></em></li>
<li><em><a href="../Page/Matthiola_leprosa.md" title="wikilink">Matthiola leprosa</a></em></li>
<li><em><a href="../Page/Matthiola_linearis.md" title="wikilink">Matthiola linearis</a></em></li>
<li><em><a href="../Page/Matthiola_littorea.md" title="wikilink">Matthiola littorea</a></em></li>
<li><em><a href="../Page/Matthiola_livida.md" title="wikilink">Matthiola livida</a></em></li>
<li><em><a href="../Page/Matthiola_lonchophora.md" title="wikilink">Matthiola lonchophora</a></em></li>
<li><em><a href="../Page/Matthiola_longipetala.md" title="wikilink">Matthiola longipetala</a></em></li>
</ul></td>
<td></td>
<td><ul>
<li><em><a href="../Page/Matthiola_lunata.md" title="wikilink">Matthiola lunata</a></em></li>
<li><em><a href="../Page/Matthiola_macranica.md" title="wikilink">Matthiola macranica</a></em></li>
<li><em><a href="../Page/Matthiola_macropetala.md" title="wikilink">Matthiola macropetala</a></em></li>
<li><em><a href="../Page/Matthiola_maderensis.md" title="wikilink">Matthiola maderensis</a></em></li>
<li><em><a href="../Page/Matthiola_maroccana.md" title="wikilink">Matthiola maroccana</a></em></li>
<li><em><a href="../Page/Matthiola_masguindali.md" title="wikilink">Matthiola masguindali</a></em></li>
<li><em><a href="../Page/Matthiola_montana.md" title="wikilink">Matthiola montana</a></em></li>
<li><em><a href="../Page/Matthiola_myrtodes.md" title="wikilink">Matthiola myrtodes</a></em></li>
<li><em><a href="../Page/Matthiola_nana.md" title="wikilink">Matthiola nana</a></em></li>
<li><em><a href="../Page/Matthiola_nudicaulis.md" title="wikilink">Matthiola nudicaulis</a></em></li>
<li><em><a href="../Page/Matthiola_obovata.md" title="wikilink">Matthiola obovata</a></em></li>
<li><em><a href="../Page/Matthiola_odoratissima.md" title="wikilink">Matthiola odoratissima</a></em></li>
<li><em><a href="../Page/Matthiola_ovatifolia.md" title="wikilink">Matthiola ovatifolia</a></em></li>
<li><em><a href="../Page/Matthiola_oxyceras.md" title="wikilink">Matthiola oxyceras</a></em></li>
<li><em><a href="../Page/Matthiola_oxyceratia.md" title="wikilink">Matthiola oxyceratia</a></em></li>
<li><em><a href="../Page/Matthiola_oyensis.md" title="wikilink">Matthiola oyensis</a></em></li>
<li><em><a href="../Page/Matthiola_parviflora.md" title="wikilink">Matthiola parviflora</a></em></li>
<li><em><a href="../Page/Matthiola_parvifolia.md" title="wikilink">Matthiola parvifolia</a></em></li>
<li><em><a href="../Page/Matthiola_patens.md" title="wikilink">Matthiola patens</a></em></li>
<li><em><a href="../Page/Matthiola_perennis.md" title="wikilink">Matthiola perennis</a></em></li>
<li><em><a href="../Page/Matthiola_perpusilla.md" title="wikilink">Matthiola perpusilla</a></em></li>
<li><em><a href="../Page/Matthiola_persica.md" title="wikilink">Matthiola persica</a></em></li>
<li><em><a href="../Page/Matthiola_phlox.md" title="wikilink">Matthiola phlox</a></em></li>
<li><em><a href="../Page/Matthiola_porphyrantha.md" title="wikilink">Matthiola porphyrantha</a></em></li>
<li><em><a href="../Page/Matthiola_prostrata.md" title="wikilink">Matthiola prostrata</a></em></li>
<li><em><a href="../Page/Matthiola_provincialis.md" title="wikilink">Matthiola provincialis</a></em></li>
<li><em><a href="../Page/Matthiola_pseudoxyceras.md" title="wikilink">Matthiola pseudoxyceras</a></em></li>
<li><em><a href="../Page/Matthiola_pumila.md" title="wikilink">Matthiola pumila</a></em></li>
<li><em><a href="../Page/Matthiola_pumilio.md" title="wikilink">Matthiola pumilio</a></em></li>
<li><em><a href="../Page/Matthiola_puntensis.md" title="wikilink">Matthiola puntensis</a></em></li>
<li><em><a href="../Page/Matthiola_revoluta.md" title="wikilink">Matthiola revoluta</a></em></li>
<li><em><a href="../Page/Matthiola_rivae.md" title="wikilink">Matthiola rivae</a></em></li>
<li><em><a href="../Page/Matthiola_robusta.md" title="wikilink">Matthiola robusta</a></em></li>
<li><em><a href="../Page/Matthiola_rostrata.md" title="wikilink">Matthiola rostrata</a></em></li>
<li><em><a href="../Page/Matthiola_runcinata.md" title="wikilink">Matthiola runcinata</a></em></li>
<li><em><a href="../Page/Matthiola_rupestris.md" title="wikilink">Matthiola rupestris</a></em></li>
<li><em><a href="../Page/Matthiola_sabauda.md" title="wikilink">Matthiola sabauda</a></em></li>
<li><em><a href="../Page/Matthiola_saxatilis.md" title="wikilink">Matthiola saxatilis</a></em></li>
<li><em><a href="../Page/Matthiola_scapifera.md" title="wikilink">Matthiola scapifera</a></em></li>
<li><em><a href="../Page/Matthiola_serica.md" title="wikilink">Matthiola serica</a></em></li>
<li><em><a href="../Page/Matthiola_simplicicaulis.md" title="wikilink">Matthiola simplicicaulis</a></em></li>
<li><em><a href="../Page/Matthiola_sinuata.md" title="wikilink">Matthiola sinuata</a></em></li>
<li><em><a href="../Page/Matthiola_smithii.md" title="wikilink">Matthiola smithii</a></em></li>
<li><em><a href="../Page/Matthiola_songarica.md" title="wikilink">Matthiola songarica</a></em></li>
<li><em><a href="../Page/Matthiola_spathulata.md" title="wikilink">Matthiola spathulata</a></em></li>
<li><em><a href="../Page/Matthiola_stelligera.md" title="wikilink">Matthiola stelligera</a></em></li>
<li><em><a href="../Page/Matthiola_stenopetala.md" title="wikilink">Matthiola stenopetala</a></em></li>
<li><em><a href="../Page/Matthiola_stephanos-mariae.md" title="wikilink">Matthiola stephanos-mariae</a></em></li>
<li><em><a href="../Page/Matthiola_stoddarti.md" title="wikilink">Matthiola stoddarti</a></em></li>
<li><em><a href="../Page/Matthiola_stylosa.md" title="wikilink">Matthiola stylosa</a></em></li>
<li><em><a href="../Page/Matthiola_subglabra.md" title="wikilink">Matthiola subglabra</a></em></li>
<li><em><a href="../Page/Matthiola_superba.md" title="wikilink">Matthiola superba</a></em></li>
<li><em><a href="../Page/Matthiola_tatarica.md" title="wikilink">Matthiola tatarica</a></em></li>
<li><em><a href="../Page/Matthiola_taurica.md" title="wikilink">Matthiola taurica</a></em></li>
<li><em><a href="../Page/Matthiola_telum.md" title="wikilink">Matthiola telum</a></em></li>
<li><em><a href="../Page/Matthiola_tenella.md" title="wikilink">Matthiola tenella</a></em></li>
<li><em><a href="../Page/Matthiola_tenera.md" title="wikilink">Matthiola tenera</a></em></li>
<li><em><a href="../Page/Matthiola_thessala.md" title="wikilink">Matthiola thessala</a></em></li>
<li><em><a href="../Page/Matthiola_tianschanica.md" title="wikilink">Matthiola tianschanica</a></em></li>
<li><em><a href="../Page/Matthiola_tomentosa.md" title="wikilink">Matthiola tomentosa</a></em></li>
<li><em><a href="../Page/Matthiola_torulosa.md" title="wikilink">Matthiola torulosa</a></em></li>
<li><em><a href="../Page/Matthiola_tricuspidata.md" title="wikilink">Matthiola tricuspidata</a></em></li>
<li><em><a href="../Page/Matthiola_tristis.md" title="wikilink">Matthiola tristis</a></em></li>
<li><em><a href="../Page/Matthiola_trojana.md" title="wikilink">Matthiola trojana</a></em>[5]</li>
<li><em><a href="../Page/Matthiola_undulata.md" title="wikilink">Matthiola undulata</a></em></li>
<li><em><a href="../Page/Matthiola_valesiaca.md" title="wikilink">Matthiola valesiaca</a></em></li>
<li><em><a href="../Page/Matthiola_vallesiaca.md" title="wikilink">Matthiola vallesiaca</a></em></li>
<li><em><a href="../Page/Matthiola_varia.md" title="wikilink">Matthiola varia</a></em></li>
<li><em><a href="../Page/Matthiola_viburnodes.md" title="wikilink">Matthiola viburnodes</a></em></li>
<li><em><a href="../Page/Matthiola_vulgaris.md" title="wikilink">Matthiola vulgaris</a></em></li>
</ul>
<p> </p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  -
[Category:十字花科](../Category/十字花科.md "wikilink")
[紫罗兰属](../Category/紫罗兰属.md "wikilink")

1.  Jaén-Molina, R., et al. (2009). [The molecular phylogeny of
    *Matthiola* R. Br.(Brassicaceae) inferred from ITS sequences, with
    special emphasis on the Macaronesian
    endemics.](http://www.bioclimac.com/mbdna/images/stories/caratulas-publication/04.pdf)
     *Molecular Phylogenetics and Evolution* 53(3) 972-81.
2.  [*Matthiola*.](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=119870)
    Flora of China.
3.  Sanchez, J. L., et al. (2005). [Genetic differentiation of three
    species of *Matthiola* (Brassicaceae) in the Sicilian insular
    system.](http://www.bioclimac.com/mbdna/images/stories/descargas-generales/sicilian%20matthiola.pdf)
     *Plant Systematics and Evolution* 253(1-4) 81-93.
4.
5.  Dirmenci, T., et al. (2006). [A new species of *Matthiola* R.
    Br.(Brassicaceae) from
    Turkey.](http://onlinelibrary.wiley.com/doi/10.1111/j.1095-8339.2006.00518.x/abstract)
    *Botanical Journal of the Linnean Society* 151(3) 431-35.