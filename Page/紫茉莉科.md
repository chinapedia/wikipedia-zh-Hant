**紫茉莉科**（[学名](../Page/学名.md "wikilink")：）包括33[属约](../Page/属.md "wikilink")290[种](../Page/种.md "wikilink")，大部分分布在[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，[美洲品种最多](../Page/美洲.md "wikilink")，少数也生活在[温带地区](../Page/温带.md "wikilink")，[中国原生的有有](../Page/中国.md "wikilink")2属，7种，但也有引进种。许多属的[花粉颗粒非常大](../Page/花粉.md "wikilink")（大于100
[微米](../Page/微米.md "wikilink")）。

本科[植物有](../Page/植物.md "wikilink")[草本](../Page/草本.md "wikilink")、[灌木和](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")，也有[藤本植物](../Page/藤本.md "wikilink")。单互生或对生，全缘，无托叶；[花辐射对称](../Page/花.md "wikilink")，两性或很少单性，常围以有颜色的苞片组成的总苞；[花萼](../Page/花萼.md "wikilink")[花冠状](../Page/花冠.md "wikilink")，[花瓣缺](../Page/花瓣.md "wikilink")；[果实为](../Page/果实.md "wikilink")[瘦果](../Page/瘦果.md "wikilink")，有棱或有翅。有多种本[科植物品种是被各地引进的观赏](../Page/科.md "wikilink")[花卉](../Page/花卉.md "wikilink")，如[小葉九重葛](../Page/小葉九重葛.md "wikilink")
（*Bougainvillea glabra*）、[紫茉莉](../Page/紫茉莉.md "wikilink")（*Mirabilis
jalapa*）等。

## 属

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><em><a href="../Page/Abronia_(plant).md" title="wikilink">Abronia</a></em></li>
<li><em><a href="../Page/Acleisanthes.md" title="wikilink">Acleisanthes</a></em></li>
<li><em><a href="../Page/Allionia.md" title="wikilink">Allionia</a></em></li>
<li><em><a href="../Page/Ammocodon.md" title="wikilink">Ammocodon</a></em></li>
<li><em><a href="../Page/Andradea.md" title="wikilink">Andradea</a></em></li>
<li><em><a href="../Page/Anulocaulis.md" title="wikilink">Anulocaulis</a></em></li>
<li><em><a href="../Page/Belemia.md" title="wikilink">Belemia</a></em></li>
<li><a href="../Page/黄细心属.md" title="wikilink">黄细心属</a> <em>Boerhavia</em></li>
<li><a href="../Page/叶子花属.md" title="wikilink">叶子花属</a> <em>Bougainvillea</em></li>
<li><em><a href="../Page/Caribea.md" title="wikilink">Caribea</a></em></li>
<li><em><a href="../Page/Cephalotomandra.md" title="wikilink">Cephalotomandra</a></em></li>
</ul></td>
<td><ul>
<li><em><a href="../Page/Colignonia.md" title="wikilink">Colignonia</a></em></li>
<li><a href="../Page/粘腺果属.md" title="wikilink">粘腺果属</a> <em>Commicarpus</em></li>
<li><em><a href="../Page/Cryptocarpus.md" title="wikilink">Cryptocarpus</a></em></li>
<li><em><a href="../Page/Cuscatlania.md" title="wikilink">Cuscatlania</a></em></li>
<li><em><a href="../Page/Cyphomeris.md" title="wikilink">Cyphomeris</a></em></li>
<li><em><a href="../Page/Gaupira.md" title="wikilink">Gaupira</a></em></li>
<li><em><a href="../Page/Grajalesia.md" title="wikilink">Grajalesia</a></em></li>
<li><em><a href="../Page/Izabalaea.md" title="wikilink">Izabalaea</a></em></li>
<li><em><a href="../Page/Leucaster.md" title="wikilink">Leucaster</a></em></li>
<li><a href="../Page/紫茉莉属.md" title="wikilink">紫茉莉属</a> <em>Mirabilis</em></li>
<li><em><a href="../Page/Neea.md" title="wikilink">Neea</a></em></li>
</ul></td>
<td><ul>
<li><em><a href="../Page/Neeopsis.md" title="wikilink">Neeopsis</a></em></li>
<li><a href="../Page/夜茉莉属.md" title="wikilink">夜茉莉属</a> <em>Nyctaginia</em></li>
<li><em><a href="../Page/Okenia.md" title="wikilink">Okenia</a></em></li>
<li><em><a href="../Page/Phaeoptilum.md" title="wikilink">Phaeoptilum</a></em></li>
<li><a href="../Page/腺果藤属.md" title="wikilink">腺果藤属</a> <em>Pisonia</em></li>
<li><em><a href="../Page/Pisoniella.md" title="wikilink">Pisoniella</a></em></li>
<li><em><a href="../Page/Ramisia.md" title="wikilink">Ramisia</a></em></li>
<li><em><a href="../Page/Reichenbachia.md" title="wikilink">Reichenbachia</a></em></li>
<li><em><a href="../Page/Salpianthus.md" title="wikilink">Salpianthus</a></em></li>
<li><em><a href="../Page/Selinocarpus.md" title="wikilink">Selinocarpus</a></em></li>
<li><em><a href="../Page/Tripterocalyx.md" title="wikilink">Tripterocalyx</a></em></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

  - Levin, 2000, Phylogenetic relationships within Nyctaginaceae tribe
    Nyctagineae: Evidence from nuclear and chloroplast genomes.
    Systematic Botany 24(4) 738-750. (Subscription req.)

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[紫茉莉科](http://delta-intkey.com/angio/www/nyctagin.htm)
  - [《北美植物》中的紫茉莉科](http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=10617)
  - [NCBI分类中的紫茉莉科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=3536&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL分类中的紫茉莉科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Nyctaginaceae)
  - [国际植物名称索引](http://www.ipni.org/index.html)

[\*](../Category/紫茉莉科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")