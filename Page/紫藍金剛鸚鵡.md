**紫藍金剛鸚鵡**（[學名](../Page/學名.md "wikilink")：**）是[金刚鹦鹉属中](../Page/金刚鹦鹉属.md "wikilink")，以至到[鹦形目中體型最大的種](../Page/鹦形目.md "wikilink")，但牠們的體重不及[新西蘭的](../Page/新西蘭.md "wikilink")[鴞鸚鵡](../Page/鴞鸚鵡.md "wikilink")。牠們獨特的體型、顏色和行为，使其成為金剛鸚鵡屬中最受自然學家注目的品種，不幸的是，寵物飼養者也同樣喜愛紫藍金剛鸚鵡。[鸟类贸易的需求對野生鸚鵡的數目造成严重的威胁](../Page/鸟类贸易.md "wikilink")。

## 分布地區

紫藍金剛鸚鵡的主要族群已知分布於[南美洲三個地區](../Page/南美洲.md "wikilink")：[巴西南部](../Page/巴西.md "wikilink")、[玻利维亚東部及](../Page/玻利维亚.md "wikilink")[巴拉圭北部](../Page/巴拉圭.md "wikilink")。零星及較小的族群可能散落於附近一帶。牠們首先被發現於[熱帶雨林及棕櫚](../Page/熱帶雨林.md "wikilink")[沼澤](../Page/沼澤.md "wikilink")，但牠們多喜愛活動於開闊的樹林，避開終年潮濕的森林，多棲息於棕櫚林中。

## 外形特征

紫藍金剛鸚鵡的羽毛是純藍色，接近靛藍色。牠們的喙呈黑色，下喙的後端及眼周有黃色的裸露[皮肤](../Page/皮肤.md "wikilink")。不像金剛鸚鵡屬內其他的品種，紫藍金剛鸚鵡眼睛周圍沒有白色而無羽毛的皮膚。紫藍金剛鸚鵡的性别基本上無法從外表上分辨，但雌性的一般是較為纖細。紫藍金剛鸚鵡的體長可達1[米](../Page/米.md "wikilink")，重達1.4至1.7公斤。翼展達1.3至1.5米。其強而有力的[喙可輕易地於短時間內拆毀鳥籠的鐵枝](../Page/喙.md "wikilink")，也可咬開堅果及種子，甚至[椰子](../Page/椰子.md "wikilink")。

## 习性

牠們的食物主要是[果實及](../Page/果實.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")。有八個品種的[棕櫚科植物是牠們主要的食糧](../Page/棕榈科.md "wikilink")。

此雀鳥於樹洞內築巢，一窩約2或3顆蛋，但通常只有一隻會存活，原因是父母只會盡力照顧第一隻孵出的幼鳥，後來孵出的因無法和兄弟姊妹爭食物，通常會死於脱水、體重下降等，而且[烏鴉](../Page/烏鴉.md "wikilink")、[犀鳥和一些](../Page/犀鳥.md "wikilink")[哺乳動物會把蛋吃掉](../Page/哺乳動物.md "wikilink")，幼鳥與父母生活約半年後便會離開親鳥，七歲後會成熟並開始繁殖。

## 瀕臨絕種及保育

紫藍金剛鸚鵡受鸟类贸易的威胁十分严重，1980年代有至少10,000隻被從野外捕捉，當中50%流入巴西市場。1983年至1984年間，超過2,500隻被運離巴拉圭的城市Bahía
Negra，1980年代末再有600隻流失。亦有當地人獵殺紫藍金剛鸚鵡為食物及其羽毛，不過，活捉價值更高。在黑市上，紫蓝金刚鹦鹉的交易价格高达在2万美金之多。

栖息地的破坏也威胁着紫蓝金刚鹦鹉的生存。在[亞馬遜雨林](../Page/亞馬遜雨林.md "wikilink")，牧草場及水力發電計劃破壞了紫藍金剛鸚鵡的生境。在[潘塔納爾](../Page/潘塔納爾濕地.md "wikilink")，只有5%的[無瓣海桑樹](../Page/無瓣海桑樹.md "wikilink")（*S.
apetala*）適合紫藍金剛鸚鵡築巢的樹洞，幼樹被牲畜吃掉或被火災燒掉。

紫藍金剛鸚鵡被列入[華盛頓公約附錄一](../Page/瀕臨絕種野生動植物國際貿易公約.md "wikilink")，表示其瀕臨絕種，國際間的交易被禁止，擁有潘塔納爾土地的人士亦禁止設置陷阱捕捉。現時，野生的紫藍金剛鸚鵡估計只剩不足3,000隻，保育工作刻不容緩。

## 媒體

### 相片

<File:Anodorhynchus>
hyacinthinus1.jpg|紫藍金剛鸚鵡於[迪士尼動物王國](../Page/迪士尼動物王國.md "wikilink")
<File:Anodorhynchus> hyacinthinus2.jpg <File:2005-04-03-paradisio>
ara-hyacinthe-4.jpg

### 影片

## 參考資料

## 延伸閱讀

  - [Araproject](http://www.araproject.nl)
  - 國際鳥盟（）（2004）[](https://web.archive.org/web/20050829015122/http://www.redlist.org/search/details.php?species=1314)。2006年[世界自然保護聯盟紅色名錄的受威脅物種](../Page/世界自然保護聯盟紅色名錄.md "wikilink")，資料包括此物種居住範圍的地圖，並解釋為何被編入瀕危級別
  - del Hoyo et al., 1997. *Handbook of the Birds of the World*. Vol. 4.
  - [Hyacinth Macaw - a closeup on its endangered
    status](http://www.bagheera.com/inthewild/van_anim_macaw.htm)
  - [Hyacinth Macaw entry from Earlham
    College](http://www.earlham.edu/~chickha/hyacinth/hyacinth.htm)
  - [How the Hyacinth Macaw got its Markings - a folk
    tale](https://web.archive.org/web/20060628025905/http://www.realmacaw.com/pages/hycolor.html)

[Category:琉璃金剛鸚鵡屬](../Category/琉璃金剛鸚鵡屬.md "wikilink")
[Category:生物之最](../Category/生物之最.md "wikilink")
[Category:玻利維亞動物](../Category/玻利維亞動物.md "wikilink")
[Category:巴西鳥類](../Category/巴西鳥類.md "wikilink")
[Category:巴拉圭動物](../Category/巴拉圭動物.md "wikilink")
[Category:能言鳥類](../Category/能言鳥類.md "wikilink")