**紫金县**（传统外文：）位于[中国](../Page/中国.md "wikilink")[广东省中部](../Page/广东省.md "wikilink")、[东江中游东岸](../Page/东江.md "wikilink")，属于[河源市管辖](../Page/河源市.md "wikilink")，东边与广东省[五华县相邻](../Page/五华县.md "wikilink")，南边与广东省[惠东县](../Page/惠东县.md "wikilink")、[陆河县相邻](../Page/陆河县.md "wikilink")，西边与广东省[博罗县](../Page/博罗县.md "wikilink")、[惠州市](../Page/惠州市.md "wikilink")[惠城区相邻](../Page/惠城区.md "wikilink")，北边与广东省[东源县相邻](../Page/东源县.md "wikilink")。

## 行政区划

2005年5月辖20个镇：**[紫城](../Page/紫城.md "wikilink")**、[中坝](../Page/中坝.md "wikilink")、**[龙窝](../Page/龙窝.md "wikilink")**、[九和](../Page/九和.md "wikilink")、[上义](../Page/上义.md "wikilink")、**[蓝塘](../Page/蓝塘.md "wikilink")**、[凤安](../Page/凤安.md "wikilink")、[义容](../Page/义容.md "wikilink")、**[古竹](../Page/古竹.md "wikilink")**、**[临江](../Page/临江.md "wikilink")**、[柏埔](../Page/柏埔.md "wikilink")、[黄塘](../Page/黄塘.md "wikilink")、[敬梓](../Page/敬梓.md "wikilink")、[乌石](../Page/乌石.md "wikilink")、[水墩](../Page/水墩.md "wikilink")、[南岭](../Page/南岭.md "wikilink")、[苏区](../Page/苏区.md "wikilink")、[瓦溪](../Page/瓦溪.md "wikilink")、[附城](../Page/附城.md "wikilink")、[好义](../Page/好义.md "wikilink")。县政府驻地在[紫城](../Page/紫城.md "wikilink")。（**粗体**为中心镇\[1\]）

## 历史

[明朝](../Page/明朝.md "wikilink")[隆庆三年正月辛未](../Page/隆庆.md "wikilink")（1569年2月12日）置永安县。

民国3年（1914年），改名为紫金县，因县城的紫金山而得名。

## 民族

紫金县以[汉族](../Page/汉族.md "wikilink")[客家人为主](../Page/客家人.md "wikilink")。

## 语言

全县通用[客家话](../Page/客家话.md "wikilink")，属纯客家县。

## 经济

2007年，全县的[地区生产总值为](../Page/地区生产总值.md "wikilink")46.7亿元，工农业生产总值为55.21亿元。

## 旅游

比较著名的景点有：紫金观、庙子石南母寺、 越王石、道姑岩、武顿山、白溪水库风景区、九和温泉（御临门温泉度假村）、天字嶂。

## 交通

全县通车总里程1960公里。

## 名人

  - [張美妮](../Page/張美妮.md "wikilink")
  - [彭新為](../Page/彭新為.md "wikilink")

## 参考资料

## 外部链接

  - [紫金政府网](http://www.zijin.gov.cn/gov/index.htm)

[紫金县](../Category/紫金县.md "wikilink") [县](../Category/河源区县.md "wikilink")
[河源](../Category/广东省县份.md "wikilink")

1.  [河源市行政区划一览表](http://www.heyuan.gov.cn/T/2009-12-23/Article_31853_1.shtml)
    ，河源市政府门户网站，2009-12-11，2010年7月11日查阅