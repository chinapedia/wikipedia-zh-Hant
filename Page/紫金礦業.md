**紫金礦業集團股份有限公司**（，，簡稱**紫金礦業**）是一家在[香港交易所上市的](../Page/香港交易所.md "wikilink")[工業公司](../Page/工業.md "wikilink")，主要業務是從事[黃金及其他](../Page/黃金.md "wikilink")[金屬的勘探](../Page/金屬.md "wikilink")、開採、生產、冶煉及销售，主要資產為[中國](../Page/中國.md "wikilink")[福建省](../Page/福建省.md "wikilink")[紫金山的](../Page/紫金山.md "wikilink")[金礦](../Page/金礦.md "wikilink")，為中國最大[金礦企業](../Page/礦業.md "wikilink")。2011年，紫金矿业实现销售收入397.63億元，利润总额57.13億元（人民幣），资产总额296.46亿元，实现和保持了净利润两位数增长，2009年12月31日公司市值达1277亿元，跻身英国《金融时报》2009年度全球500强企业（市值）排行榜第243名\[1\]
紫金矿业的主营业务集中于黄金、铜等有色金属的开发。紫金矿业下属核心企业紫金山金矿是国内单体矿山保有可利用储量最大、采选规模最大、黄金产量最大、经济效益最好的黄金矿山，也是中国唯一的一座世界级金矿。\[2\]

2007年，紫金礦業分別收購了英國蒙特瑞科公司（Monterrico Metals PLC）及CBML（Commonwealth &
BritishMinerals
Limited）的股權獲得[秘魯和](../Page/秘魯.md "wikilink")[塔吉克的礦產](../Page/塔吉克.md "wikilink")。\[3\]

2012年5月31日全購澳洲金礦公司諾頓金田（Norton
Gold）餘下83.02%股權，每股收購價0.25澳元，較諾頓金田停牌前溢價19%，總交易金額1.8億澳元（約13.6億港元）。諾頓金田主要資產是澳洲Paddington金礦項目，年產黃金4.67噸

2012年9月29日國家統計局前局長[邱曉華獲委任](../Page/邱曉華.md "wikilink")「紫金礦業」的[副董事長](../Page/副董事長.md "wikilink")。

## 污染事件

2010年7月3日，紫金礦業位於[福建](../Page/福建.md "wikilink")[上杭的紫金山銅礦濕法廠的水池發生滲漏](../Page/上杭.md "wikilink")，約9,100[立方米含有銅酸性的](../Page/立方米.md "wikilink")[污水進入](../Page/污水.md "wikilink")[汀江](../Page/汀江.md "wikilink")，造成過千[噸魚中毒死亡](../Page/噸.md "wikilink")。\[4\]

2010年9月20日夜间至21日白天，信宜紫金矿业所属的[广东](../Page/广东.md "wikilink")[信宜市银岩锡矿矿区高旗岭尾矿库初期坝因强降雨后水位急剧上升](../Page/信宜市.md "wikilink")，导致坝体右侧决口，造成28人死亡或失踪。\[5\]

## 参考文献

## 外部連結

  - [紫金礦業](http://www.zjky.cn/)

## 参见

  - [2008年云南文山警民冲突](../Page/2008年云南文山警民冲突.md "wikilink")

{{-}}

[Category:香港上市工業公司](../Category/香港上市工業公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:前恒生中國企業指數成份股](../Category/前恒生中國企業指數成份股.md "wikilink")
[Category:金屬公司](../Category/金屬公司.md "wikilink")
[Category:龙岩公司](../Category/龙岩公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")

1.  [紫金礦業IPO成功過會，打破1元面值創新模式](http://www.hkwarrant.cn/hkspage/2007012/027/blog493624bd01008as8html.htm)
2.  [紫金矿业（2899.HK），国际矿业托拉斯雏形乍现](http://www.create.hk/archives/183)
3.  [中國黃金生產巨頭將攜鉅資進軍中亞金礦](http://www.xj.xinhuanet.com/2007-06/28/content_10424016.htm)
4.  [紫金礦業毒水殺1980噸魚
    釀生態災難](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20100713&sec_id=15335&subsec_id=15336&art_id=14233022)
    蘋果日報，2010年7月13日
5.  [信宜紫金溃坝事件：28人死亡或失踪](http://www.infzm.com/content/50538)
    [南方周末](../Page/南方周末.md "wikilink")，2010年9月27日