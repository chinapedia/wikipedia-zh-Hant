**紫錦魚**又稱**紫衣葉鯛**，俗名四齒、礫仔、紫衣、貓仔魚、汕冷仔為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[隆頭魚科的其中一](../Page/隆頭魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[東非](../Page/東非.md "wikilink")、[南非](../Page/南非.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[葛摩共和國](../Page/葛摩共和國.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[印度](../Page/印度.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國](../Page/中國.md "wikilink")[南海](../Page/南海.md "wikilink")、[越南](../Page/越南.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")'[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[羅得豪島](../Page/羅得豪島.md "wikilink")、[帕拉群島](../Page/帕拉群島.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[索羅門群島](../Page/索羅門群島.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[法屬玻里尼西亞](../Page/法屬玻里尼西亞.md "wikilink")、[夏威夷群島](../Page/夏威夷群島.md "wikilink")、[復活節島](../Page/復活節島.md "wikilink")、[美屬薩摩亞](../Page/美屬薩摩亞.md "wikilink")、[西薩摩亞](../Page/西薩摩亞.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[吐瓦魯](../Page/吐瓦魯.md "wikilink")、[吉里巴斯](../Page/吉里巴斯.md "wikilink")、[東加](../Page/東加.md "wikilink")、[諾魯等海域](../Page/諾魯.md "wikilink")。

## 深度

水深0.5至5公尺。

## 特徵

本魚體稍長且側扁。吻部短；上下頜具一列尖齒，前方各具
2犬齒，無後犬齒。雌魚體呈黑褐色，頭部較深，背鰭在第一到第三棘間有一黑斑，背鰭和臀鰭紅色具一黃色縱帶，尾鰭赭色。雄魚體青綠色，頭部有朱紅色塊斑，體背具黑褐色縱帶，背鰭、臀鰭和體側具紅色縱帶，體側並有一些斷續紅色橫紋。尾鰭截形呈黑褐色，末緣則呈青綠色。體被大形圓鱗，鰓蓋上部稍具小鱗片外，頭部無鱗，背鰭前之胸部被較小鱗，頸部裸出；腹鰭具鞘鱗。背鰭硬棘8枚、背鰭軟條12至14枚、臀鰭硬棘3枚、臀鰭軟條10至12枚。體長可達35公分。

## 生態

本魚棲息於礁台、礁緣或礫石區，以[海膽](../Page/海膽.md "wikilink")、[甲殼類](../Page/甲殼類.md "wikilink")、[多毛類為食](../Page/多毛類.md "wikilink")。

## 經濟利用

食用性魚類，可[清蒸加豬油食用](../Page/清蒸.md "wikilink")，肉細味淡，並無異味。另外體色鮮豔，也適合水族觀賞的魚類。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[purpureum](../Category/錦魚屬.md "wikilink")