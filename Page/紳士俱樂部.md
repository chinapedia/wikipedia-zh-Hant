[Reform_Club._Upper_level_of_the_saloon._From_London_Interiors_(1841).jpg](https://zh.wikipedia.org/wiki/File:Reform_Club._Upper_level_of_the_saloon._From_London_Interiors_\(1841\).jpg "fig:Reform_Club._Upper_level_of_the_saloon._From_London_Interiors_(1841).jpg")（Reform
Club）\]\]
**紳士俱樂部**（又稱**紳士會所**；[英文](../Page/英文.md "wikilink")：**gentlemen's
club**），顧名思義，原為[英格蘭](../Page/英格蘭.md "wikilink")[上流社會男會員而設](../Page/上流社會.md "wikilink")\[1\]，是採用會員制的私人[俱樂部](../Page/俱樂部.md "wikilink")（又稱[會所](../Page/會所.md "wikilink")），標榜[社交](../Page/社交.md "wikilink")、[飲食和](../Page/飲食.md "wikilink")[博弈等吸引力](../Page/博弈.md "wikilink")。

然而，時至今日，紳士俱樂部大多已放寬了對會員[性別和](../Page/性別.md "wikilink")[社會地位的限制](../Page/社會地位.md "wikilink")。除[英國外](../Page/英國.md "wikilink")，其他[國家亦有不少著名的紳士俱樂部](../Page/國家.md "wikilink")。

在[日本](../Page/日本.md "wikilink")（[日語稱](../Page/日語.md "wikilink")****）和[美國](../Page/美國.md "wikilink")，「紳士俱樂部」一詞常用作[脫衣舞](../Page/脫衣舞.md "wikilink")[夜總會的](../Page/夜總會.md "wikilink")[委婉](../Page/委婉語.md "wikilink")[代名詞](../Page/代名詞.md "wikilink")。這種用法在英國亦開始流行起來，聲色場所[連鎖店](../Page/連鎖店.md "wikilink")，如[Stringfellows和](../Page/:en:Stringfellows.md "wikilink")[Spearmint
Rhino正正以此招徠](../Page/:en:Spearmint_Rhino.md "wikilink")。

## 歷史

紳士俱樂部最早成立於[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")[西區](../Page/西區_\(倫敦\).md "wikilink")（West
End）。時至今日，[聖詹姆斯](../Page/聖詹姆斯.md "wikilink")（St.
James's）地區有時仍稱為「俱樂部街」（clubland）。在18世紀的倫敦，俱樂部在個別階層成了[咖啡廳的替代角色](../Page/咖啡廳.md "wikilink")\[2\]，其影響力更在19世紀末發展至高峰。一般而言，俱樂部是由一群相熟的人組成，大家一起消遣，分享興趣。當中，[博弈是重要的活動之一](../Page/博弈.md "wikilink")，一般以[紙牌遊戲為主](../Page/紙牌遊戲.md "wikilink")，其他活動則因應會員在[政治](../Page/政治.md "wikilink")、[文學](../Page/文學.md "wikilink")、[體育等範疇的興趣而定](../Page/體育.md "wikilink")。此外，會員還有其他的消遺方式。很多時候，來自相同[軍隊分部或](../Page/軍隊.md "wikilink")[大學背景的人](../Page/大學.md "wikilink")，他們會較容易交往。

一些舊式的俱樂部是極具[貴族氣派的](../Page/貴族.md "wikilink")，後來這種經營模式的俱樂部日漸普及。\[3\]19世紀末以前，男士只要表明可靠的「[紳士](../Page/紳士.md "wikilink")」地位資格，就可獲俱樂部承認；除非情況很極端，譬如申請者性格、行為惹人討厭，或者「不愛交際」（「unclubbable」，\[4\]。此詞由著名文人[塞繆爾·約翰遜首先使用](../Page/塞繆爾·約翰遜.md "wikilink")）。此舉是為了吸納有收入的專業人士，如[醫生](../Page/醫生.md "wikilink")、[律師](../Page/律師.md "wikilink")、[議員和](../Page/議員.md "wikilink")[法官等](../Page/法官.md "wikilink")\[5\]。
[Whitehouse_With_Lights_Auckland.jpg](https://zh.wikipedia.org/wiki/File:Whitehouse_With_Lights_Auckland.jpg "fig:Whitehouse_With_Lights_Auckland.jpg")
[Garrick_Club_King_St_Covent_Garden_Illustrated_London_News_1864.jpg](https://zh.wikipedia.org/wiki/File:Garrick_Club_King_St_Covent_Garden_Illustrated_London_News_1864.jpg "fig:Garrick_Club_King_St_Covent_Garden_Illustrated_London_News_1864.jpg")
[Metro_Club_60_St_jeh.jpg](https://zh.wikipedia.org/wiki/File:Metro_Club_60_St_jeh.jpg "fig:Metro_Club_60_St_jeh.jpg")
要注意的是，紳士俱樂部並不標榜[大眾](../Page/大眾.md "wikilink")[娛樂](../Page/娛樂.md "wikilink")（如[音樂表演](../Page/音樂.md "wikilink")，或諸如此類），而是以[社交](../Page/社交.md "wikilink")、[飲食和](../Page/飲食.md "wikilink")[博弈等作賣點](../Page/博弈.md "wikilink")。紳士俱樂部是男士會員的「第二個家」，他們可以放鬆心情、結交朋友，玩[室內遊戲](../Page/室內遊戲.md "wikilink")，或者飲食，不受拘束。在裝潢華麗的環境下，俱樂部的服務對象是以有穩定收入的[上流和中上流社會男士為主](../Page/上流社會.md "wikilink")。較高級的俱樂部會聘請[建築師建造](../Page/建築師.md "wikilink")，以其考究的內部陳設\[6\]，與當時的鄉間[別墅一較高下](../Page/別墅.md "wikilink")。俱樂部亦是男士的便利收容所，藉此躲避女性親屬。不少會員把大多數時間花費在俱樂部，一些甚至在此投宿，流連至通宵達旦。

19-20世紀，階級規定漸漸放寬起來，自20世紀末起，一些紳士俱樂部更開放予[女性](../Page/女性.md "wikilink")。她們光顧之餘，也可以申請做會員。這在一定程度上維持了會員級別制度，同時亦犧牲了俱樂部和男士的隱私權。\[7\]\[8\]

## 現況

正值傳統紳士俱樂部不再受歡迎、不復影響力之際，不少在近年起死回生。然而，高級俱樂部和普通俱樂部之間到底有何差異，人們是不以為然的。實際上，前者仍然極其強調顧客的身分、地位，只會招特符合會籍的會員。申請者如果在入會名單上等候了很久，這意味着申請被拒。他的推薦人亦會被要求放棄會籍，因為他帶來了不受歡迎的申請者。

現時紳士俱樂部偏佈全球，在[英聯邦國家或地區和](../Page/英聯邦.md "wikilink")[美國](../Page/美國.md "wikilink")（特別是[英語圈](../Page/英語圈.md "wikilink")）佔主導地位。不少俱樂部會採取互惠的經營策略，為來自外地俱樂部的會員遊客提供殷勤招待。

在[英國](../Page/英國.md "wikilink")，尤其是[倫敦](../Page/倫敦.md "wikilink")，舊式紳士俱樂部過渡到新式俱樂部，或類似的私人會員俱樂部，如[Groucho俱樂部](../Page/:en:Groucho_Club.md "wikilink")、[蘇豪之家](../Page/蘇豪之家.md "wikilink")（Soho
House）及[Home
House](../Page/:en:Home_House.md "wikilink")。它們都提供相似的設施，如優雅的環境、餐飲供應、場地租賃，以至住宿服務。\[9\]

### 英國

[倫敦大概有](../Page/倫敦.md "wikilink")25間有名氣的紳士俱樂部，例子有[Athenaeum和](../Page/:en:_Athenaeum_Club,_London.md "wikilink")[White's](../Page/:en:White's.md "wikilink")。其他有分量的俱樂部，如[遊艇俱樂部](../Page/遊艇.md "wikilink")（又稱遊艇會），以其特色另覓客源。在[商業利益的前提下](../Page/商業.md "wikilink")，它們大多不會考慮個體間的共同興趣，以求吸引更多會員。

倫敦的紳士俱樂部是不准談公事的。\[10\]然而，在[英國和](../Page/英國.md "wikilink")[世界各地](../Page/世界.md "wikilink")，愈來愈多[政界和](../Page/政治.md "wikilink")[商界人士利用俱樂部討論](../Page/商業.md "wikilink")[時事](../Page/時事.md "wikilink")。譬如，倫敦的[聯邦俱樂部](../Page/聯邦俱樂部.md "wikilink")（Commonwealth
Club）曾經招待過前[英國首相](../Page/英國首相.md "wikilink")[貝理雅](../Page/貝理雅.md "wikilink")、前[尼日利亞](../Page/尼日利亞.md "wikilink")[總統](../Page/總統.md "wikilink")[奧巴桑喬及前](../Page/奧盧塞貢·奧巴桑喬.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")[首相](../Page/首相.md "wikilink")[霍華德等具影響力的人](../Page/約翰·霍華德.md "wikilink")，他們均在那裏發過言。然而，不少制度較完善的大型俱樂部曾一度嚴厲禁止公眾討論。\[11\]

#### 會籍規定

一些俱樂部有嚴格的入會規定，以下是一些例子：

  - [倫敦的](../Page/倫敦.md "wikilink")[Caledonian俱樂部規定](../Page/:en:Caledonian_Club.md "wikilink")，申請者須「擁有直系蘇格蘭血統，換句話說，血統要上承自[蘇格蘭父母或祖父母](../Page/蘇格蘭.md "wikilink")」，或者「獲委員會判定為與蘇格蘭有密切聯係」。\[12\]

<!-- end list -->

  - [旅客俱樂部](../Page/:en:Travellers_Club.md "wikilink")，自1819年創立以來，拒絕「未離開過[不列顛群島](../Page/不列顛群島.md "wikilink")，即從[倫敦起計](../Page/倫敦.md "wikilink")，至少方圓五百[英里距離](../Page/英里.md "wikilink")」的人士入會。\[13\]

<!-- end list -->

  - [哈佛校友會](../Page/哈佛校友會.md "wikilink")（Harvard
    Club）開放予所有與[哈佛大學有關係的人士](../Page/哈佛大學.md "wikilink")。

<!-- end list -->

  - [革新俱樂部](../Page/革新俱樂部.md "wikilink")（Reform
    Club）要求申請者表態支持《[1832年改革法案](../Page/1832年改革法案.md "wikilink")》。該俱樂部從1981年起接受女會員。\[14\]

### 美國

[San_Francisco_-_Crazy_Horse.jpg](https://zh.wikipedia.org/wiki/File:San_Francisco_-_Crazy_Horse.jpg "fig:San_Francisco_-_Crazy_Horse.jpg")
[美國大多數重要](../Page/美國.md "wikilink")[城市至少設有一間傳統紳士俱樂部](../Page/城市.md "wikilink")。紳士俱樂部在[東岸的舊城市較為流行](../Page/美國東岸.md "wikilink")，如[紐約市](../Page/紐約市.md "wikilink")（當地有最多著名的俱樂部）、[費城](../Page/費城.md "wikilink")、[波士頓及](../Page/波士頓.md "wikilink")[首都](../Page/首都.md "wikilink")[華盛頓](../Page/華盛頓哥倫比亞特區.md "wikilink")。美國很多俱樂部與[倫敦的舊式俱樂部建立了互惠關係](../Page/倫敦.md "wikilink")，[世界各地一些俱樂部亦然](../Page/世界.md "wikilink")。美國一些俱樂部可追溯至18-19世紀，其悠久歷史與[英格蘭的相差不遠](../Page/英格蘭.md "wikilink")。\[15\]

### 澳洲

[澳洲的](../Page/澳洲.md "wikilink")[悉尼和](../Page/悉尼.md "wikilink")[墨爾本有很多紳士俱樂部](../Page/墨爾本.md "wikilink")，如[澳洲俱樂部](../Page/澳洲俱樂部.md "wikilink")（Australian
Club）、[墨爾本俱樂部](../Page/墨爾本俱樂部.md "wikilink")（Melbourne
Club）、[Weld俱樂部](../Page/:en:Weld_Club.md "wikilink")、Athenaeum俱樂部（相當於[倫敦的同名俱樂部](../Page/倫敦.md "wikilink")）及Savage俱樂部。

### 南非

[南非的](../Page/南非.md "wikilink")[約翰內斯堡](../Page/約翰內斯堡.md "wikilink")[商業區是](../Page/商業.md "wikilink")[Rand
Club的所在地](../Page/:en:Rand_Club.md "wikilink")。

## 參見

  - [俱樂部](../Page/俱樂部.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [中國九大頂級富豪俱樂部排行](https://web.archive.org/web/20120404221829/http://news.xinhuanet.com/fortune/2006-04/04/content_4383493.htm)，[新華網](../Page/新華網.md "wikilink")
  - [上海拒絕《花花公子》開設俱樂部](http://news.bbc.co.uk/chinese/trad/hi/newsid_4070000/newsid_4079100/4079137.stm)，[BCC新聞網](../Page/BCC.md "wikilink")，2004年12月8日

[Category:組織](../Category/組織.md "wikilink")
[私人俱樂部](../Category/私人俱樂部.md "wikilink")

1.  [保守的古怪規矩
    英國俱樂部不歡迎女人](http://www.people.com.cn/BIG5/guoji/25/95/20020327/696018.html)

2.  [俱樂部的歷史](http://www2.wunan.com.tw/download/preview/1l38.pdf)

3.
4.  《[牛津英語詞典](../Page/牛津英語詞典.md "wikilink")》 Probably 1764

5.
6.
7.
8.
9.
10. [紳士俱樂部](http://travel.tvb.com/unitedkingdom/travel_8.html)

11.
12. [Membership](http://www.caledonianclub.com/membership/)，Caledonian俱樂部官方網站

13. [Travellers
    Club](http://www.victorianlondon.org/entertainment/travellersclub.htm)，Victorian
    London網站

14. [英最傲慢俱樂部，入會要等半年](http://gb.cri.cn/9523/2006/07/11/421@1127615.htm)

15.