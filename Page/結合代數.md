在[數學裡](../Page/數學.md "wikilink")，**結合代數**是指一[向量空間](../Page/向量空間.md "wikilink")(或更一般地，一[模](../Page/模.md "wikilink"))，其允許向量有具[分配律和](../Page/分配律.md "wikilink")[結合律的乘法](../Page/結合律.md "wikilink")。因此，它為一特殊的[代數](../Page/多元環.md "wikilink")。結合代數，是一種代數系統，類似於群、環、域，而更接近於環。仿照由實數來構造複數的方法，可用複數來構造新的數。

## 定義

一於[體](../Page/域_\(數學\).md "wikilink")*K*上的結合代數*A*的定義為一於*K*上的向量空間，其*K*-[雙線性映射](../Page/雙線性映射.md "wikilink")*A*
× *A* → *A* 具有結合律：

  - 對任何於*A*內的*x*、*y*和*z*，(*x y*) *z* = *x* (*y z*)。

此乘法的雙線性性質可表示成

  - 對任何於*A*內的*x*、*y*和*z*，满足结合律: (*x* + *y*) *z* = *x z* + *y z*；
  - 對任何於*A*內的*x*、*y*及於*K*的*a*，满足分配律: *x* (*y* + *z*) = *x y* + *x z*；
  - 對任何於*A*內的*x*、*y*及於*K*內的*a*，满足结合律 *a* (*x y*) = (*a* *x*) *y* = *x*
    (*a* *y*)。

當*A*含有單位元，即元素1使得對任一於*A*內的*x*，1*x* = *x*1 =
*x*，則稱*A*為*具一的結合代數*或**[單作結合代數](../Page/單作.md "wikilink")**。
此一代數為一個[環](../Page/環.md "wikilink")，且包含所以體*K*內的元素*a*，由*a*1相連接。

上述的定義沒有任何改變地廣義化成了於[可交換環](../Page/可交換環.md "wikilink")*K*上的代數(除了*K*-線性空間被稱做[模而非向量空間之外](../Page/模.md "wikilink"))。詳述請見[代數
(環論)](../Page/代數_\(環論\).md "wikilink")。

於一體*K*上的結合代數*A*的*維度*為其*K*-向量空間的[維度](../Page/維度_\(向量空間\).md "wikilink")。

## 例子

  - 其元素為體*K*的*n×n*[方陣形成了一於](../Page/方陣.md "wikilink")*K*上的單作結合代數。
  - [複數形成了於](../Page/複數.md "wikilink")[實數上的二維單作結合代數](../Page/實數.md "wikilink")。
  - [四元數形成了於實數上的四維單作結合代數](../Page/四元數.md "wikilink")(但不為一複數上的代數，因為複數和四元數不可交換)。
  - 實係數[多項式形成了一於實數上的單作結合代數](../Page/多項式.md "wikilink")。
  - 給定一[巴拿赫空間](../Page/巴拿赫空間.md "wikilink")*X*，其[連續線性算子](../Page/連續線性算子.md "wikilink")
    *A* : *n* →
    *X*形成了一單作結合代數(以算子複合做為乘法)；事實上，這是一個[巴拿赫代數](../Page/巴拿赫代數.md "wikilink")。
  - 給定一[拓撲空間](../Page/拓撲空間.md "wikilink")*X*，於*X*上的連續實(複)值函數形成了一單作結合代數；這裡，加法和乘法是對函數的各點相加和相乘。
  - 一非單作的結合代數為所有*x*趨向無限時的[極限為零的函數](../Page/極限.md "wikilink")*f*: **R** →
    **R**所組成的集合。
  - [克理福代數也是結合代數的一種](../Page/克理福代數.md "wikilink")，在[幾何和](../Page/幾何.md "wikilink")[物理上都很有用](../Page/物理.md "wikilink")。
  - 局部有限[偏序集合的](../Page/偏序關係.md "wikilink")[相交代數為一](../Page/相交代數.md "wikilink")[組合數學內的單作結合代數](../Page/組合數學.md "wikilink")。

## [代數同態](../Page/代數同態.md "wikilink")

若*A*和*B*為體*K*上的結合代數，*[代數同態](../Page/代數同態.md "wikilink")* *h*: *A* →
*B*則是一*K*-[線性映射](../Page/線性算子.md "wikilink")，其對任何於*A*內的*x*、*y*，會有*h*(*xy*)
= *h*(*x*)
*h*(*y*)的關係。加上[態射的概念](../Page/態射.md "wikilink")，於*K*上的結合代數組成的類便成了一[範疇](../Page/範疇.md "wikilink")。

舉個例子，設*A*為所有實值連續函數**R** →
**R**所組成的代數，及*B*=**R**，這兩者都是於**R**上的代數，且其每一連續函數*f*指定至數字*f*(0)的映射會是個由*A*至*B*的代數同態。

## 免指標標記法

前面所述之結合代數的定義，其結合律的定義是對*A*的所有元素而定的。但有時不涉及*A*內元素的結合律定義會較方便。
這可以由下列方法作到。一定義成在一向量空間*A*內映射*M*的代數：

\[M: A \times A \rightarrow A\] 其為結合代數當*M*有下面性質：

\[M \circ (\mbox {Id} \times M) = M \circ (M \times \mbox {Id})\]
其中，符號\(\circ\)表示函數的[複合](../Page/複合函數.md "wikilink")，而Id則為[恆等函數](../Page/恆等函數.md "wikilink")：對所有於*A*內的*x*，\(Id(x)=x\)。要了解其定義是等價的，只需要知道上述式子的兩邊都是三個引數的函數。例如，式子左邊為

\[( M \circ (\mbox {Id} \times M)) (x,y,z) = M (x, M(y,z))\]

類似地，一單作結合代數可以以單位映射\(\eta: K \rightarrow A\)來定義，其性質如下：

\[M \circ (\mbox {Id} \times \eta ) = s = M \circ (\eta \times \mbox {Id})\]
其中，單位映射η將*K*內的元素*k*映射至*A*內的元素*k1*，這裡*1*是*A*的[單位元](../Page/單位元.md "wikilink")。映射*s*只是個純量乘積：\(s:K\times A \rightarrow A\)。

## 廣義化

## 共代數

## 表示

## 參考

  - Ross Street, *[Quantum Groups: an entrée to modern
    algebra](https://web.archive.org/web/20050825034431/http://www-texdev.ics.mq.edu.au/Quantum/Quantum.ps)*
    (1998). *(Provides a good overview of index-free notation)*

[J](../Category/抽象代数.md "wikilink")