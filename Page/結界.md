**结界**（巴利語：baddha-sīmā\[1\]；梵語：sīmā-bandha，或bandhaya-sīman，音譯畔陀也死曼\[2\]）是[佛教术语](../Page/佛教术语.md "wikilink")，原為[僧伽在](../Page/僧伽.md "wikilink")[布薩等集聚一處時](../Page/布薩.md "wikilink")，隨處劃定一定之界區，限定僧侶活動的範圍，以免僧眾動輒違犯別眾、離宿、宿煮等過失\[3\]。巴利律注中提到，界分為「已結之界」(baddha-sīmā)和「未結之界」(abaddha-sīmā)兩類\[4\]，近於[南山律中所稱之](../Page/南山律.md "wikilink")「作法界」和「自然界」\[5\]。

後來因[真言宗的發展](../Page/真言宗.md "wikilink")，於修[密教法時](../Page/密教.md "wikilink")，為防止魔障侵入，劃一定之地區，以保護道場與行者，稱為結界、結護。
結界為具有一定法力效力的范围，其作用通常是保护性的。即堅固所住地之地結印（金剛橛）與四方設柵以防他人侵入之四方結印（金剛牆），俟[本尊入](../Page/本尊.md "wikilink")[道場後](../Page/道場.md "wikilink")，在[虛空張網覆道場上](../Page/虛空.md "wikilink")，令入其中者無障難，即結虛空網印（金剛網）；又在道場四方設柵，周圍繞以火焰護衛之，即結火院印（金剛炎）。

佛教修行者通过遵循一定的仪式可以构造结界。例如按照《[大悲心陀罗尼经](../Page/大悲心陀罗尼经.md "wikilink")》第二十一节：

## 动漫

在一些[动漫作品中](../Page/动漫.md "wikilink")，结界的概念被拓展，也通常具有可见的[边界和范围](../Page/边界.md "wikilink")。

## 外部連結

  - \[<http://211.72.15.72/huayen/public/preview.php?main=007&sub=28&id=872>|
    大華嚴寺佛学名词\]

[Category:佛教術語](../Category/佛教術語.md "wikilink")

1.

2.  《大毘盧遮那成佛經疏》：「畔陀也死曼……上句結下句界也。此意-{云}-一切方處所結界也」

3.  《四分律行事鈔資持記》：「釋結界篇。結謂白二（白二[羯磨](../Page/羯磨.md "wikilink")）限約，即能被之法。界，謂分隔彼此，即所加之處。」

4.
5.