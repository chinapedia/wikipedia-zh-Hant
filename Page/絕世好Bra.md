是於2001年上映，由[陳慶嘉](../Page/陳慶嘉.md "wikilink")、[梁柏堅聯合導演的](../Page/梁柏堅.md "wikilink")[香港電影](../Page/香港電影.md "wikilink")，[陳慶嘉](../Page/陳慶嘉.md "wikilink")、[錢小蕙編劇及監製](../Page/錢小蕙.md "wikilink")。由[劉青雲](../Page/劉青雲.md "wikilink")、[古天樂](../Page/古天樂.md "wikilink")、[劉嘉玲](../Page/劉嘉玲.md "wikilink")、[梁詠琪联合主演](../Page/梁詠琪.md "wikilink")\[1\]。

## 故事大綱

講述一家世界級的[胸圍公司過去一直都只僱用女性僱員](../Page/胸罩.md "wikilink")，但在面對同行競爭愈來愈激烈之時，她們首次僱用了兩位男性作首席設計師，務求在產品上能夠有突破性的發展。故事講述Wayne（[古天樂飾](../Page/古天樂.md "wikilink")）及強尼（[劉青雲飾](../Page/劉青雲.md "wikilink")）兩位本來對胸圍一無所知的大男人，在加入了這一家胸圍公司之後，除了學會有關胸圍的一切以外，還學曉了真正的[愛與](../Page/愛.md "wikilink")[關懷](../Page/關懷.md "wikilink")。

## 劇情簡介

日本著名胸圍公司的香港區總裁Samantha破例聘請了Johnny和Wayne兩名男設計師。他們的上司高級設計師Lena認為他們根本無法勝任這份工作，因此常常刁難他們。公司要求他們設計出一款“絕世好Bra”，可是多次失敗。

在工作的過程中，Johnny和Wayne分別與Smantha和Lena產生了感情，最後他們也在各自女友的幫忙下，才最終把“絕世好Bra”設計成功。

## 角色列表

|                                                                    |               |
| ------------------------------------------------------------------ | ------------- |
| **演員**                                                             | **角色**        |
| [劉青雲](../Page/劉青雲.md "wikilink")                                   | 強尼            |
| [古天樂](../Page/古天樂.md "wikilink")                                   | Wayne         |
| [劉嘉玲](../Page/劉嘉玲.md "wikilink")                                   | Samantha      |
| [梁詠琪](../Page/梁詠琪.md "wikilink")                                   | Lena          |
| [李珊珊](../Page/李珊珊_\(藝員\).md "wikilink")                            | Candy         |
| [青山知可子](../Page/青山知可子.md "wikilink")                               | 日本總公司老闆Kanako |
| [樋口明日嘉](../Page/樋口明日嘉.md "wikilink")                               | Suki          |
| [卓韻芝](../Page/卓韻芝.md "wikilink")                                   | Gigi          |
| [谷祖琳](../Page/谷祖琳.md "wikilink")                                   | Alex          |
| [Rosemary Vandebrouck](../Page/Rosemary_Vandebrouck.md "wikilink") | Elieen        |
| [馮德倫](../Page/馮德倫.md "wikilink")                                   | Fung          |
| [莫文蔚](../Page/莫文蔚.md "wikilink")                                   | Shirley       |
| [譚耀文](../Page/譚耀文.md "wikilink")                                   | Ali Bra Bra   |
| [鄒凱光](../Page/鄒凱光.md "wikilink")                                   | 戀物狂           |
| [余翠芝](../Page/余翠芝.md "wikilink")                                   | 售貨員           |
| [陳霽平](../Page/陳霽平.md "wikilink")                                   | Jolene        |
| [陳慧儀](../Page/陳慧儀.md "wikilink")                                   | Louis女友       |
| [田蕊妮](../Page/田蕊妮.md "wikilink")                                   | 場地負責人         |
| [陳志健](../Page/陳志健.md "wikilink")                                   | 非禮青年          |
| [夏永康](../Page/夏永康.md "wikilink")                                   | 導演            |
| [林超賢](../Page/林超賢.md "wikilink")                                   | 酒保            |

## 電影歌曲

|        |        |                                  |
| ------ | ------ | -------------------------------- |
| **曲別** | **歌名** | **演唱者**                          |
| 主題曲    | 《貼心》   | [梁詠琪](../Page/梁詠琪.md "wikilink") |

## 參看

  - 《[絕世好B](../Page/絕世好B.md "wikilink")》
  - 《[絕世好賓](../Page/絕世好賓.md "wikilink")》

## 参考资料

## 外部链接

  -
  - {{@movies|flhk60236956}}

  -
  -
  -
  -
  -
  -
  - [HK cinemagic
    entry](http://www.hkcinemagic.com/en/movie.asp?id=2713)

[1](../Category/2000年代香港電影作品.md "wikilink")
[Category:香港浪漫喜剧片](../Category/香港浪漫喜剧片.md "wikilink")

1.  <http://ent.qq.com/d/movie/8/7704/>