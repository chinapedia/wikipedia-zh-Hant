**綜藝節目**是一種[娛樂性的](../Page/娛樂.md "wikilink")[節目形式](../Page/節目.md "wikilink")，通常包含了許多不同性質和元素的演出，例如音樂與搞笑等類型。過往通常在[電視上播出](../Page/電視.md "wikilink")，而大部分的綜藝節目會邀請現場[觀眾參加](../Page/觀眾.md "wikilink")[錄影](../Page/錄影.md "wikilink")，但也有現場實況播出。

常見的綜藝節目主要分為四大類型：綜合經典（亦即大型傳統綜藝節目）、[遊戲](../Page/遊戲節目.md "wikilink")、[益智](../Page/益智節目.md "wikilink")、[談話](../Page/談話節目.md "wikilink")。

隨著時代演進，綜藝節目含括的不同元素衍生出各種子類型和變化創新，各子類型開枝散葉、持續發展。

## 歷史

[台灣和](../Page/台灣.md "wikilink")[香港的綜藝節目發展多時](../Page/香港.md "wikilink")，早期比較有名的節目如《[群星會](../Page/群星會.md "wikilink")》、《[歡樂今宵](../Page/歡樂今宵.md "wikilink")》等。

而目前世界[健力士紀錄](../Page/健力士紀錄.md "wikilink")，全球最長壽的電視綜藝節目是[香港無綫電視](../Page/香港無綫電視.md "wikilink")（TVB）製作的《[歡樂今宵](../Page/歡樂今宵.md "wikilink")》，從1967年啟播，一直到1994年結束，及後1997年至2000年短暫復播，2007年製作特輯《[再會歡樂今宵](../Page/再會歡樂今宵.md "wikilink")》。2017年適逢無綫電視開播50週年和紀念《歡樂今宵》50週年，再製作特輯，名為《[我愛EYT](../Page/我愛EYT.md "wikilink")》\[1\]\[2\]。

## 種類

  - 談話節目
  - 音樂節目
  - 外景節目
  - 旅遊節目
  - 美食節目
  - 整人節目
  - 問答類節目
  - 遊戲類節目
  - 選秀類節目
  - 模仿類節目
  - 短劇類節目
  - 聯誼速配節目
  - 真人實境節目
  - 深夜成人節目
  - 文藝晚會

## 經典節目

  - 1962年《[群星會](../Page/群星會.md "wikilink")》
  - 1962年《[傅培梅時間](../Page/傅培梅時間.md "wikilink")》
  - 1965年《[錦繡河山](../Page/錦繡河山.md "wikilink")》
  - 1965年《[五燈獎](../Page/五燈獎.md "wikilink")》
  - 1967年《[歡樂今宵](../Page/歡樂今宵.md "wikilink")》
  - 1970年《[歡樂假期](../Page/歡樂假期.md "wikilink")》
  - 1973年《[分秒世界](../Page/分秒世界.md "wikilink")》
  - 1978年《[一道彩虹](../Page/一道彩虹.md "wikilink")》
  - 1979年《[綜藝100](../Page/綜藝100.md "wikilink")》
  - 1980年《[臨風高歌](../Page/臨風高歌.md "wikilink")》
  - 1981年《[雙星報喜](../Page/雙星報喜_\(華視\).md "wikilink")》
  - 1982年《[我愛紅娘](../Page/我愛紅娘.md "wikilink")》
  - 1983年《[大家一起來](../Page/大家一起來.md "wikilink")》
  - 1984年《[黃金拍檔](../Page/黃金拍檔.md "wikilink")》
  - 1985年《[豬哥亮歌廳秀](../Page/豬哥亮歌廳秀.md "wikilink")》
  - 1985年《[強棒出擊](../Page/強棒出擊.md "wikilink")》
  - 1985年《[歡樂週末派](../Page/歡樂週末派.md "wikilink")》
  - 1986年《[連環泡](../Page/連環泡.md "wikilink")》
  - 1986年《[鑽石舞台](../Page/鑽石舞台.md "wikilink")》
  - 1986年《[金舞台](../Page/金舞台.md "wikilink")》
  - 1987年《[頑皮家族](../Page/頑皮家族.md "wikilink")》
  - 1987年《[天天開心](../Page/天天開心.md "wikilink")》
  - 1988年《[百戰百勝](../Page/百戰百勝.md "wikilink")》
  - 1988年《[女丑劇場](../Page/女丑劇場.md "wikilink")》
  - 1988年《[歡樂一百點](../Page/歡樂一百點.md "wikilink")》
  - 1989年《[好彩頭](../Page/好彩頭.md "wikilink")》
  - 1989年《[來電五十](../Page/來電五十.md "wikilink")》
  - 1989年《[八千里路雲和月](../Page/八千里路雲和月.md "wikilink")》
  - 1991年《[玫瑰之夜](../Page/玫瑰之夜.md "wikilink")》
  - 1991年《[綜藝萬花筒](../Page/綜藝萬花筒.md "wikilink")》
  - 1993年《[龍兄虎弟](../Page/龍兄虎弟.md "wikilink")》
  - 1994年《[超級星期天](../Page/超級星期天.md "wikilink")》
  - 1995年《[完全娛樂](../Page/完全娛樂.md "wikilink")》
  - 1995年《[獎門人電視遊戲節目系列](../Page/獎門人電視遊戲節目系列.md "wikilink")》
  - 1995年《[天天樂翻天](../Page/天天樂翻天.md "wikilink")》
  - 1995年《[黃金夜總會](../Page/黃金夜總會.md "wikilink")》
  - 1995年《[苦苓晚點名](../Page/苦苓晚點名.md "wikilink")》
  - 1995年《[黃金傳奇](../Page/黃金傳奇.md "wikilink")》
  - 1996年《[我猜我猜我猜猜猜](../Page/我猜我猜我猜猜猜.md "wikilink")》
  - 1996年《[學校沒教的事](../Page/學校沒教的事.md "wikilink")》
  - 1996年《[非常男女](../Page/非常男女.md "wikilink")》
  - 1996年《[紅白勝利](../Page/紅白勝利.md "wikilink")》
  - 1997年《[快樂大本營](../Page/快樂大本營.md "wikilink")》
  - 1997年《[全能綜藝通](../Page/全能綜藝通.md "wikilink")》
  - 1997年《[台灣紅不讓](../Page/台灣紅不讓.md "wikilink")》
  - 1997年《[台灣探險隊](../Page/台灣探險隊.md "wikilink")》
  - 1997年《[娛樂百分百](../Page/娛樂百分百.md "wikilink")》
  - 1997年《[天才Bang\!Bang\!Bang\!](../Page/天才Bang!Bang!Bang!.md "wikilink")》
  - 1997年《[世界非常奇妙](../Page/世界非常奇妙.md "wikilink")》
  - 1997年《[電玩快打](../Page/電玩快打.md "wikilink")》
  - 1998年《[戀愛講義](../Page/戀愛講義.md "wikilink")》
  - 1998年《[電視笑話冠軍](../Page/電視笑話冠軍.md "wikilink")》
  - 1999年《[綜藝旗艦 Hello Jacky\!](../Page/綜藝旗艦_Hello_Jacky!.md "wikilink")》
  - 1999年《[Jacky show](../Page/Jacky_show.md "wikilink")》
  - 2000年《[周日八點黨](../Page/周日八點黨.md "wikilink")》
  - 2000年《[超級大富翁](../Page/超級大富翁.md "wikilink")》
  - 2000年《[美鳳有約](../Page/美鳳有約.md "wikilink")》
  - 2001年《[綜藝大集合](../Page/綜藝大集合.md "wikilink")》
  - 2001年《[智者生存](../Page/智者生存.md "wikilink")》
  - 2001年《[歡喜玉玲龍](../Page/歡喜玉玲龍.md "wikilink")》
  - 2001年《[TV三賤客](../Page/TV三賤客.md "wikilink")》
  - 2002年《[綜藝大哥大](../Page/綜藝大哥大.md "wikilink")》
  - 2002年《[生活智慧王](../Page/生活智慧王.md "wikilink")》
  - 2002年《[少年特攻隊](../Page/少年特攻隊.md "wikilink")》
  - 2002年《[兩代電力公司](../Page/兩代電力公司.md "wikilink")》
  - 2003年《[快樂星期天](../Page/快樂星期天.md "wikilink")》
  - 2003年《[綜藝最愛憲](../Page/綜藝最愛憲.md "wikilink")》
  - 2003年《[女人我最大](../Page/女人我最大.md "wikilink")》
  - 2003年《[冷知識轟趴](../Page/冷知識轟趴.md "wikilink")》
  - 2003年《[黃金七秒半](../Page/黃金七秒半.md "wikilink")》
  - 2004年《[康熙來了](../Page/康熙來了.md "wikilink")》
  - 2004年《[全民大悶鍋](../Page/全民大悶鍋.md "wikilink")》
  - 2004年《[全能估價王](../Page/全能估價王.md "wikilink")》
  - 2004年《[命運好好玩](../Page/命運好好玩.md "wikilink")》
  - 2005年《[國光幫幫忙](../Page/國光幫幫忙.md "wikilink")》
  - 2005年《[麻辣天后宮](../Page/麻辣天后宮.md "wikilink")》
  - 2005年《[歡樂幸運星](../Page/歡樂幸運星.md "wikilink")》
  - 2005年《[小氣大財神](../Page/小氣大財神.md "wikilink")》
  - 2005年《[齊天大勝](../Page/齊天大勝.md "wikilink")》
  - 2005年《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》
  - 2006年《[模范棒棒堂](../Page/模范棒棒堂.md "wikilink")》
  - 2006年《[哈林國民學校](../Page/哈林國民學校.md "wikilink")》
  - 2006年《[好色男女](../Page/好色男女.md "wikilink")》
  - 2006年《[天才衝衝衝](../Page/天才衝衝衝.md "wikilink")》
  - 2006年《[型男大主廚](../Page/型男大主廚.md "wikilink")》
  - 2006年《[分手擂台](../Page/分手擂台.md "wikilink")》
  - 2006年《[美女廚房](../Page/美女廚房.md "wikilink")》
  - 2007年《[超級星光大道](../Page/超級星光大道.md "wikilink")》
  - 2007年《[大學生了沒](../Page/大學生了沒.md "wikilink")》
  - 2007年《[硬是要鬥牛](../Page/硬是要鬥牛.md "wikilink")》
  - 2007年《[食尚玩家](../Page/食尚玩家.md "wikilink")》
  - 2007年《[超級偶像](../Page/超級偶像.md "wikilink")》
  - 2007年《[今晚誰當家](../Page/今晚誰當家.md "wikilink")》
  - 2008年《[POWER星期天](../Page/POWER星期天.md "wikilink")》
  - 2008年《[百萬小學堂](../Page/百萬小學堂.md "wikilink")》
  - 2008年《[百萬大歌星](../Page/百萬大歌星.md "wikilink")》
  - 2008年《[王牌大賤諜](../Page/王牌大賤諜.md "wikilink")》
  - 2009年《[豬哥會社](../Page/豬哥會社.md "wikilink")》
  - 2009年《[爸媽囧很大](../Page/爸媽囧很大.md "wikilink")》
  - 2009年《[挑戰101](../Page/挑戰101.md "wikilink")》
  - 2009年《[打擊出去](../Page/打擊出去.md "wikilink")》
  - 2009年《[WTO姐妹會](../Page/WTO姐妹會.md "wikilink")》
  - 2010年《[一袋女王](../Page/一袋女王.md "wikilink")》
  - 2010年《[超級巨星紅白藝能大賞](../Page/超級巨星紅白藝能大賞.md "wikilink")》
  - 2010年《[小燕之夜](../Page/小燕之夜.md "wikilink")》
  - 2010年《[瘋神無雙](../Page/瘋神無雙.md "wikilink")》
  - 2011年《[上班這黨事](../Page/上班這黨事.md "wikilink")》
  - 2012年《[超級模王大道](../Page/超級模王大道.md "wikilink")》
  - 2012年《[女人要有錢](../Page/女人要有錢.md "wikilink")》
  - 2013年《[綜藝大熱門](../Page/綜藝大熱門.md "wikilink")》
  - 2013年《[金頭腦](../Page/金頭腦.md "wikilink")》
  - 2014年《[二分之一強](../Page/二分之一強.md "wikilink")》
  - 2014年《[綜藝玩很大](../Page/綜藝玩很大.md "wikilink")》
  - 2014年《[一字千金](../Page/一字千金.md "wikilink")》
  - 2016年《[小明星大跟班](../Page/小明星大跟班.md "wikilink")》
  - 2016年《[大腦先生](../Page/大腦先生.md "wikilink")》

## 參考來源

## 參看

  - [長壽節目](../Page/長壽節目.md "wikilink")
  - [台灣綜藝節目列表](../Page/台灣綜藝節目列表.md "wikilink")
  - [中國大陸綜藝節目列表](../Page/中國大陸綜藝節目列表.md "wikilink")

[綜藝節目](../Category/綜藝節目.md "wikilink")

1.
2.