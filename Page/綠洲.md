[Libyen-oase1.jpg](https://zh.wikipedia.org/wiki/File:Libyen-oase1.jpg "fig:Libyen-oase1.jpg")的利比亞部份的綠洲。\]\]
**綠洲**是一個地理名詞，是指被[沙漠地形包圍的環境裏](../Page/沙漠.md "wikilink")、一塊有[植被覆蓋的孤立肥沃地區](../Page/植被.md "wikilink")。通常會造成綠洲的原因都是因為此地點有終年不斷的水源供應，常見的水源來源是[地下水泉湧或人工鑿](../Page/地下水.md "wikilink")[井來](../Page/井.md "wikilink")[灌溉](../Page/灌溉.md "wikilink")，水在遠方的降雨區降到地面後潛入地底，通過透水的地下[砂岩層穿過沙漠地帶](../Page/砂岩.md "wikilink")，在綠洲處返回地表附近而能被使用到。

綠洲對於沙漠地區的生活是非常重要的，不只是大部分的沙漠居民都是圍繞在綠洲地帶生活，往來的商旅與貿易網往往也都是沿著綠洲發展起來，因為綠洲是重要的食物與水之補給站。盤據[非洲大陸北部中央的](../Page/非洲.md "wikilink")[撒哈拉沙漠裏面](../Page/撒哈拉沙漠.md "wikilink")，有百分之六十以上的人口都是居住在零星分佈於沙漠中的綠洲地帶。中國[烏鞘嶺以西](../Page/烏鞘嶺.md "wikilink")、北山和[祁連山之間的](../Page/祁連山.md "wikilink")[河西走廊](../Page/河西走廊.md "wikilink")，有大量綠洲，“一城山光，半城塔影，葦溪連片，古剎處處”，就是在描寫有稱“金張掖”或“甘洲”之稱的[張掖](../Page/張掖.md "wikilink")，漢朝即有[額濟納綠洲](../Page/額濟納.md "wikilink")，[河西走廊的綠洲唯一水源是來自](../Page/河西走廊.md "wikilink")[祁連山春融的雪水](../Page/祁連山.md "wikilink")。

## 世界知名綠洲

### 亞洲

  - [新疆](../Page/新疆.md "wikilink")[吐魯番窪地](../Page/吐魯番.md "wikilink")，[中國](../Page/中國.md "wikilink")
  - [月牙泉綠洲](../Page/月牙泉.md "wikilink")，中國[敦煌附近](../Page/敦煌.md "wikilink")，已緩慢消失
  - [哈爾湖](../Page/哈爾湖.md "wikilink")（Khar
    Nlia），位於蒙古西部[科布多的](../Page/科布多.md "wikilink")[大湖盆地](../Page/大湖盆地.md "wikilink")（Great
    Lakes Basin）區
  - 大衛河沙漠綠洲 （Nahal David
    Oasis）、[恩戈地](../Page/恩戈地.md "wikilink")，[以色列](../Page/以色列.md "wikilink")
  - [蓋提夫綠洲](../Page/蓋提夫.md "wikilink")（Qatif
    oasis），[沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")
  - 赫勒堡綠洲（Nakhl Fort oases），[阿曼](../Page/阿曼.md "wikilink")

### 歐洲

  - 赫度布雷林達綠洲 （Herðubreiðarlindir），[冰島](../Page/冰島.md "wikilink")

### 美洲

[Fish_Springs_Utah.jpg](https://zh.wikipedia.org/wiki/File:Fish_Springs_Utah.jpg "fig:Fish_Springs_Utah.jpg")\]\]

  - 馬倫豪沙漠湖綠洲（Maranhao desert lake），[巴西](../Page/巴西.md "wikilink")
  - 瓦卡奇納綠洲（Huacachina oasis），[秘魯](../Page/秘魯.md "wikilink")

### 非洲

  - 廷吉爾綠洲（Tinerhir oasis），[摩洛哥](../Page/摩洛哥.md "wikilink")
  - 姆扎卜綠洲（M'zab oasis），[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")
  - 舍比凱綠洲（Chebika Oasis, Tunisia），[突尼斯](../Page/突尼斯.md "wikilink")
  - 烏拜里綠洲（Ubari Oasis），[利比亞](../Page/利比亞.md "wikilink")
  - 提米亞綠洲（Saharan oasis of Timia），[尼日爾](../Page/尼日爾.md "wikilink")

[\*](../Category/綠洲.md "wikilink")