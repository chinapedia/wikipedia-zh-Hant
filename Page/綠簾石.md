**綠簾石**，一種[矽酸鹽礦物](../Page/矽酸鹽.md "wikilink")，化學式為Ca<sub>2</sub>(Al,
Fe)<sub>3</sub>(SiO<sub>4</sub>)<sub>3</sub>(OH)，常以[柱狀出現於自然界](../Page/柱.md "wikilink")，[單斜晶系](../Page/單斜晶系.md "wikilink")，屬於從[火成岩變質而成的](../Page/火成岩.md "wikilink")[變質岩](../Page/變質岩.md "wikilink")，[硬度為](../Page/硬度.md "wikilink")6.5，[比重為](../Page/比重.md "wikilink")3.4，[密度為](../Page/密度.md "wikilink")3.25-3.45，擁有[玻璃光澤](../Page/玻璃光澤.md "wikilink")，顏色為[綠色](../Page/綠色.md "wikilink")、[棕色](../Page/棕色.md "wikilink")、[灰色或近乎](../Page/灰色.md "wikilink")[黑色的深色](../Page/黑色.md "wikilink")。

## 參考文獻

  - [:en:epidote](../Page/:en:epidote.md "wikilink")
  - [:ja:緑簾石](../Page/:ja:緑簾石.md "wikilink")

[Lv4](../Category/矿物.md "wikilink")
[Category:地質學](../Category/地質學.md "wikilink")
[Category:礦物小作品](../Category/礦物小作品.md "wikilink")
[Category:含鈣礦物](../Category/含鈣礦物.md "wikilink")
[Category:含鋁礦物](../Category/含鋁礦物.md "wikilink")
[Category:含鐵礦物](../Category/含鐵礦物.md "wikilink")
[Category:矽酸鹽礦物](../Category/矽酸鹽礦物.md "wikilink")