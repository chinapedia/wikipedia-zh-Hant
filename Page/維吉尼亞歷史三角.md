**維吉尼亞歷史三角**位於[美國的](../Page/美國.md "wikilink")[維吉尼亞半島包括了](../Page/維吉尼亞半島.md "wikilink")[詹姆斯鎮](../Page/詹姆斯鎮.md "wikilink")、[約克鎮和](../Page/約克鎮.md "wikilink")[威廉斯堡三個殖民地社區](../Page/威廉斯堡.md "wikilink")。這三個殖民地社區形成[殖民地大路](../Page/殖民地大路.md "wikilink")。

## 殖民地大路簡介

[殖民地大路把受保護的三個](../Page/殖民地大路.md "wikilink")[維吉尼亞的](../Page/維吉尼亞.md "wikilink")[殖民地社區連接起來](../Page/殖民地社區.md "wikilink")，當中還有一條供遊客欣賞風景和田園的車行道。車行道經常有[野生生物和](../Page/野生生物.md "wikilink")[水鳥沿車行道行走](../Page/水鳥.md "wikilink")(或橫過它)。

在大路附近的[詹姆斯河和](../Page/詹姆斯河.md "wikilink")[約克河末端](../Page/約克河.md "wikilink")，有一些家庭允許他們的孩子用麵包餵海鷗。[殖民地大路是免費的](../Page/殖民地大路.md "wikilink")。

## 詹姆斯鎮的渡輪簡介

詹姆斯鎮的(四個)渡輪(的當中一個)會經過[維吉尼亞州際高速公路](../Page/維吉尼亞.md "wikilink")10|VA
10,和31|VA 31再由蘇格蘭碼頭抵達玻璃議院點(Glass House
Point)。當乘客橫渡時，他們必須留下他們的車。詹姆斯鎮的輪渡服務是免費的。

## 詹姆斯鎮簡介

1607年，第一個[英國在](../Page/英國.md "wikilink")[美國建立的新世界就是](../Page/美國.md "wikilink")[詹姆斯鎮](../Page/詹姆斯鎮.md "wikilink")。今天，您能參觀[詹姆斯鎮節日公園和詹姆斯鎮海島](../Page/詹姆斯鎮節日公園.md "wikilink")。村莊和殖民地堡壘的休閒，和當前的工作是進行中的考古學站點。

在詹姆斯鎮的二個主要區域分別是前Jamestown節日公園，(一個活生生的歷史博物館\!)
公園裏有由維吉尼亞聯邦管理的複製船。除了詹姆斯鎮海島外，當地還有極具歷史價值的詹姆斯鎮。

## 威廉斯堡簡介

1699年，根據[威廉與瑪麗學院學生的建議](../Page/威廉與瑪麗學院.md "wikilink")，[維吉尼亞的郡首府從](../Page/維吉尼亞.md "wikilink")[詹姆斯鎮遷往中央種植園](../Page/詹姆斯鎮.md "wikilink")。為了紀念英皇
[威廉三世](../Page/威廉三世.md "wikilink")，中央種植園後改名為[威廉斯堡](../Page/威廉斯堡.md "wikilink")。

從1780年起，幾乎150年來，[威廉斯堡成了一個被遺忘了的小鎮](../Page/威廉斯堡.md "wikilink")。直到20世紀初期在牧師博士Goodwin,
Bruton Parish Church, Standard Oil等人的努力下，
[威廉斯堡不但被保存下來](../Page/威廉斯堡.md "wikilink")，許多18世紀殖民時代的輝煌的建築都被恢復和重建，而且看起來挺真實,很有特色。它就是[殖民地威廉斯堡](../Page/殖民地威廉斯堡.md "wikilink")，現今是一個普遍的旅遊目的地。

## 約克鎮簡介

維吉尼亞歷史三角的第三點是約克鎮，英國Cornwallis
將軍投降的地方，結束了[美國革命](../Page/美國革命.md "wikilink")。

## 外部連結

  - [威廉與瑪麗學院](http://www.wm.edu)
  - [殖民地威廉斯堡官方網站](http://www.history.org)
  - [APVA web site for the Jamestown Rediscovery
    project](https://web.archive.org/web/20090416033022/http://www.apva.org/jr.html)
  - [1](http://www.historicjamestowne.org/詹姆斯鎮)
      - [我們何現在在何處發掘?](http://www.historicjamestowne.org/the_dig/)
  - [Jamestown 2007 Celebration](http://www.jamestown2007.org/)
  - [Jamestown Settlement and Yorktown Victory
    Center](http://www.historyisfun.org/)
  - [Virtual Jamestown](http://www.virtualjamestown.org/)
  - [National Park Service: Jamestown National Historic
    Site](http://www.nps.gov/jame/)
  - [York County Virginia Local Government](http://www.yorkcounty.gov/)
  - [Williamsburg Area Convention and Visitors Bureau - The Official
    Website](http://www.visitwilliamsburg.com/)

[category:美國歷史](../Page/category:美國歷史.md "wikilink")

[Category:威廉斯堡](../Category/威廉斯堡.md "wikilink")
[Category:維吉尼亞州歷史](../Category/維吉尼亞州歷史.md "wikilink")