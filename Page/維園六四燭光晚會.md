[21st_anniversary_candlelight_vigil_8.jpg](https://zh.wikipedia.org/wiki/File:21st_anniversary_candlelight_vigil_8.jpg "fig:21st_anniversary_candlelight_vigil_8.jpg")
[Victoria_Park_June_4th_Vigil_in_2018.jpg](https://zh.wikipedia.org/wiki/File:Victoria_Park_June_4th_Vigil_in_2018.jpg "fig:Victoria_Park_June_4th_Vigil_in_2018.jpg")

**維園六四燭光晚會**是[香港悼念](../Page/香港.md "wikilink")[六四天安門事件死難者的年度活動](../Page/六四天安門事件.md "wikilink")，1990年6月4日起每年由[香港市民支援愛國民主運動聯合會](../Page/香港市民支援愛國民主運動聯合會.md "wikilink")（支聯會）舉辦，於[維多利亞公園的硬地](../Page/維多利亞公園.md "wikilink")[足球場舉行](../Page/足球場.md "wikilink")。該晚會是目前世界上規模最大的六四事件悼念活動，每年參加人數为数万至十数万人不等。

## 晚會內容

晚會程序包括誦讀六四死難者及離世死難者家屬名單、致悼辭、[默哀](../Page/默哀.md "wikilink")、向[紀念碑獻](../Page/紀念碑.md "wikilink")[花及](../Page/花.md "wikilink")[鞠躬](../Page/鞠躬.md "wikilink")、播放民運人士及[天安門母親的](../Page/天安門母親運動.md "wikilink")[訪問](../Page/訪問.md "wikilink")[影片等等](../Page/影片.md "wikilink")。參與者歌唱富有民運色彩的[六四歌曲](../Page/六四歌曲.md "wikilink")，包括《[血染的風-{采}-](../Page/血染的風采.md "wikilink")》、《[自由花](../Page/自由花.md "wikilink")》及《[歷史的傷口](../Page/歷史的傷口.md "wikilink")》等等。

2001年的晚會上，[支聯會青年組](../Page/支聯會青年組.md "wikilink")\[1\]宣佈籌備成立\[2\]，其後每年支青組均會在晚會上讀出宣言或表演[話劇及歌唱等](../Page/話劇.md "wikilink")\[3\]，以示「接好民主棒」。

## 訴求

[22º_aniversário_de_Massacre_de_4_de_Junho_2225.JPG](https://zh.wikipedia.org/wiki/File:22º_aniversário_de_Massacre_de_4_de_Junho_2225.JPG "fig:22º_aniversário_de_Massacre_de_4_de_Junho_2225.JPG")[西洋菜街上的粉筆字](../Page/西洋菜街.md "wikilink")\]\]
除了平反[八九民運外](../Page/八九民運.md "wikilink")，晚會還提出其他訴求，包括「追究屠城責任」、「釋放民運人士」、「結束[一黨專政](../Page/一黨專政.md "wikilink")」、「建設民主中國」等等。在2003年，因著當時的政治氣氛，加入「反對[香港基本法第二十三條](../Page/香港基本法.md "wikilink")」、「還政於民」等訴求。2006年起加入「支持維權」等等。而在2010年，即六四事件21周年，加入「釋放[劉曉波](../Page/劉曉波.md "wikilink")，支持[憲章](../Page/零八憲章.md "wikilink")」及「反對打壓」兩個主題。最近的一次即2018年，晚会呼吁释放[王全璋](../Page/王全璋.md "wikilink")、声援[709事件中被捕维权律师](../Page/709事件.md "wikilink")。此外，晚会也喊出了释放刘晓波的妻子[刘霞的口号](../Page/刘霞.md "wikilink")。

## 歷年參加人數

<table>
<colgroup>
<col style="width: 80%" />
<col style="width: 70%" />
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 26%" />
<col style="width: -315%" />
</colgroup>
<thead>
<tr class="header">
<th><p>六四事件週年</p></th>
<th><p>年份</p></th>
<th style="text-align: right;"><p>大會公佈人數</p></th>
<th style="text-align: right;"><p>警方公佈人數</p></th>
<th style="text-align: right;"><p>籌得款項<br />
（港元）[4]</p></th>
<th><p>大會年度主題</p></th>
<th style="text-align: right;"><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><span style="display:none">0</span>1週年</p></td>
<td><p>1990年</p></td>
<td style="text-align: right;"><p>150,000</p></td>
<td style="text-align: right;"><p>80,000</p></td>
<td style="text-align: right;"></td>
<td><p>釋放民運人士、平反八九民運、追究屠城責任、結束一黨專政、建設民主中國</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td><p><span style="display:none">0</span>2週年</p></td>
<td><p>1991年</p></td>
<td style="text-align: right;"><p>100,000</p></td>
<td style="text-align: right;"><p>60,000</p></td>
<td style="text-align: right;"><p>（沒有紀錄）</p></td>
<td><p>釋放民運人士</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p><span style="display:none">0</span>3週年</p></td>
<td><p>1992年</p></td>
<td style="text-align: right;"><p>80,000</p></td>
<td style="text-align: right;"><p>28,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>439,000</p></td>
<td><p>釋放民運人士</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td><p><span style="display:none">0</span>4週年</p></td>
<td><p>1993年</p></td>
<td style="text-align: right;"><p>40,000</p></td>
<td style="text-align: right;"><p>12,000</p></td>
<td style="text-align: right;"><p>$3,300,000</p></td>
<td><p>釋放民運人士</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p><span style="display:none">0</span>5週年</p></td>
<td><p>1994年</p></td>
<td style="text-align: right;"><p>40,000</p></td>
<td style="text-align: right;"><p>12,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>480,000</p></td>
<td><p>我會來</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td><p><span style="display:none">0</span>6週年</p></td>
<td><p>1995年</p></td>
<td style="text-align: right;"><p>35,000</p></td>
<td style="text-align: right;"><p>16,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>569,000</p></td>
<td><p>平反六四</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p><span style="display:none">0</span>7週年</p></td>
<td><p>1996年</p></td>
<td style="text-align: right;"><p>45,000</p></td>
<td style="text-align: right;"><p>16,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,0</span>74,200</p></td>
<td><p>跨越九七</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td><p><span style="display:none">0</span>8週年</p></td>
<td><p>1997年</p></td>
<td style="text-align: right;"><p>55,000</p></td>
<td style="text-align: right;"></td>
<td style="text-align: right;"><p>$1,974,000</p></td>
<td><p>戰鬥到底</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p><span style="display:none">0</span>9週年</p></td>
<td><p>1998年</p></td>
<td style="text-align: right;"><p>40,000</p></td>
<td style="text-align: right;"><p>16,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>676,000</p></td>
<td><p>平反六四</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td><p>10週年</p></td>
<td><p>1999年</p></td>
<td style="text-align: right;"><p>70,000</p></td>
<td style="text-align: right;"><p>（沒有公佈）</p></td>
<td style="text-align: right;"><p>$1,255,000</p></td>
<td><p>毋忘六四十周年 邁向民主新世紀</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p>11週年</p></td>
<td><p>2000年</p></td>
<td style="text-align: right;"><p>45,000</p></td>
<td style="text-align: right;"><p>（沒有公佈）</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>833,000</p></td>
<td><p>薪火相傳</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td><p>12週年</p></td>
<td><p>2001年</p></td>
<td style="text-align: right;"><p>48,000</p></td>
<td style="text-align: right;"><p>（沒有公佈）</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>736,000</p></td>
<td><p>教育下一代 接好民主棒</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p>13週年</p></td>
<td><p>2002年</p></td>
<td style="text-align: right;"><p>45,000</p></td>
<td style="text-align: right;"><p>（沒有公佈）</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>603,000</p></td>
<td><p>年青一代 齊來參與 認識歷史 毋忘六四</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td><p>14週年</p></td>
<td><p>2003年</p></td>
<td style="text-align: right;"><p>50,000</p></td>
<td style="text-align: right;"><p>（沒有公佈）</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>760,000</p></td>
<td><p>毋忘六四 反對廿三</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p>15週年</p></td>
<td><p>2004年</p></td>
<td style="text-align: right;"><p>82,000</p></td>
<td style="text-align: right;"><p>48,000</p></td>
<td style="text-align: right;"><p>$1,125,000</p></td>
<td><p>平反六四 還政於民</p></td>
<td style="text-align: right;"><p><a href="../Page/0708年雙普選.md" title="wikilink">0708年雙普選被否決</a></p></td>
</tr>
<tr class="even">
<td><p>16週年</p></td>
<td><p>2005年</p></td>
<td style="text-align: right;"><p>45,000</p></td>
<td style="text-align: right;"><p>22,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,0</span>64,000</p></td>
<td><p>以史為鑑 平反六四</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p>17週年</p></td>
<td><p>2006年</p></td>
<td style="text-align: right;"><p>44,000</p></td>
<td style="text-align: right;"><p>19,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>617,000</p></td>
<td><p>平反六四 支持維權</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td><p><a href="../Page/六四事件十八週年.md" title="wikilink">18週年</a></p></td>
<td><p>2007年</p></td>
<td style="text-align: right;"><p>55,000</p></td>
<td style="text-align: right;"><p>27,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>826,000</p></td>
<td><p>平反六四 支持維權</p></td>
<td style="text-align: right;"><p>早前<a href="../Page/民建聯.md" title="wikilink">民建聯黨魁</a><a href="../Page/馬力.md" title="wikilink">馬力發表六四言論</a></p></td>
</tr>
<tr class="odd">
<td><p>19週年</p></td>
<td><p>2008年</p></td>
<td style="text-align: right;"><p>48,000</p></td>
<td style="text-align: right;"><p>18,000</p></td>
<td style="text-align: right;"><p>$<span style="display:none">0,</span>683,000</p></td>
<td><p>同一世界 同一人權 同一夢想 平反六四</p></td>
<td style="text-align: right;"><p>大會將紀念六四結合對<a href="../Page/汶川地震.md" title="wikilink">汶川地震遇難者的哀悼</a>。<a href="../Page/中國中央電視台.md" title="wikilink">中國中央電視台則報導為</a>「紀念四川地震活動」。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/六四事件二十週年.md" title="wikilink">20週年</a></p></td>
<td><p>2009年</p></td>
<td style="text-align: right;"></td>
<td style="text-align: right;"><p>62,800</p></td>
<td style="text-align: right;"><p>$2,100,000</p></td>
<td><p>毋忘六四 繼承英烈志 薪火相傳 接好民主棒</p></td>
<td style="text-align: right;"><p>早前特首<a href="../Page/曾蔭權.md" title="wikilink">曾蔭權</a>、<a href="../Page/陳一諤.md" title="wikilink">陳一諤</a>、<a href="../Page/呂智偉.md" title="wikilink">呂智偉</a>、<a href="../Page/詹培忠.md" title="wikilink">詹培忠等發表六四言論</a><br />
當晚集會人數暴增，是繼1990年以來首次場內參加人數達15萬；同時亦開展連續6年超過15萬人參加的紀錄<br />
連同未能進場市民計算，有20萬人，成為包括場外人數在內的最高紀錄</p></td>
</tr>
<tr class="odd">
<td><p>21週年</p></td>
<td><p>2010年</p></td>
<td style="text-align: right;"><p>150,000[5]</p></td>
<td style="text-align: right;"><p>113,000</p></td>
<td style="text-align: right;"><p>$1,410,000</p></td>
<td><p>毋忘六四 薪火相傳 平反六四 堅持到底；反對政治檢控 抗議政治打壓；釋放劉曉波 支持零八憲章<br />
（增加「釋放劉曉波 支持<a href="../Page/零八憲章.md" title="wikilink">零八憲章</a>」和「反對政治打壓」主題）</p></td>
<td style="text-align: right;"><p>六四晚會之前的幾天，支聯會<a href="../Page/民主女神像.md" title="wikilink">民主女神像遭警方沒收</a><br />
警方估計參加人數創歷年新高</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/六四事件二十二週年.md" title="wikilink">22週年</a></p></td>
<td><p>2011年</p></td>
<td style="text-align: right;"><p>150,000[6]</p></td>
<td style="text-align: right;"><p>77,000</p></td>
<td style="text-align: right;"><p>$1,310,000</p></td>
<td><p>平反六四 革命尚未成功 建設民主 同志仍須努力</p></td>
<td style="text-align: right;"><p><a href="../Page/支聯會.md" title="wikilink">支聯會前主席</a><a href="../Page/司徒華.md" title="wikilink">司徒華逝世後首次晚會</a><br />
中共為防<a href="../Page/茉莉花革命.md" title="wikilink">茉莉花革命爆發而打壓</a><a href="../Page/維權.md" title="wikilink">維權人士</a></p></td>
</tr>
<tr class="odd">
<td><p>六四事件二十三週年|23週年</p></td>
<td><p>2012年</p></td>
<td style="text-align: right;"><p>180,000</p></td>
<td style="text-align: right;"><p>85,000</p></td>
<td style="text-align: right;"><p>$2,323,000</p></td>
<td><p>毋忘六四傳真相 民主潮流不可擋</p></td>
<td style="text-align: right;"><p>被坦克輾斷雙腿的民運人士<a href="../Page/方政.md" title="wikilink">方政到場參與</a><br />
警方開放<a href="../Page/維多利亞公園.md" title="wikilink">維多利亞公園足球場</a>、草坪、音樂亭、籃球場及<a href="../Page/告士打道.md" title="wikilink">告士打道一條行車線予公眾集會</a><br />
大會公佈場內人數破紀錄</p></td>
</tr>
<tr class="even">
<td><p>六四事件二十四週年|24週年</p></td>
<td><p>2013年</p></td>
<td style="text-align: right;"><p>150,000</p></td>
<td style="text-align: right;"><p>54,000</p></td>
<td style="text-align: right;"><p>$1,600,000</p></td>
<td><p>愛國愛民 香港精神 平反六四 永不放棄<br />
（後刪去「愛國愛民，香港精神」）</p></td>
<td style="text-align: right;"><p>維園的六四燭光集會原定於晚上8時開始，但在開始前15分鐘突然下大雨，因此晚會押後開始<br />
然後由於雨勢過大，維園出現積水；再加上音響設備故障，因此集會進行約半小時後，大會宣佈提前結束<br />
<a href="../Page/尖沙咀六四集會.md" title="wikilink">尖沙咀同時舉辦了六四集會</a>，人數在200至1000人之間</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/六四事件二十五週年.md" title="wikilink">25週年</a></p></td>
<td><p>2014年</p></td>
<td style="text-align: right;"><p>180,000[7]</p></td>
<td style="text-align: right;"><p>99,500 [8][9]</p></td>
<td style="text-align: right;"><p>$1,700,000[10]</p></td>
<td><p>平反六四 戰鬥到底</p></td>
<td style="text-align: right;"><p>內地維權人士<a href="../Page/滕彪.md" title="wikilink">滕彪</a>、「<a href="../Page/坦克人.md" title="wikilink">坦克人</a>」攝影師到場參與<br />
按大會公佈，參加人數平2012年創下的場內人數最高紀錄<br />
<a href="../Page/維多利亞公園.md" title="wikilink">維多利亞公園足球場</a>、草坪、音樂亭及籃球場開放予集會使用<br />
<a href="../Page/尖沙咀六四集會.md" title="wikilink">尖沙咀亦舉辦六四集會</a>，以「本土、民主、反共」為主題，人數在3000至7000人之間。</p></td>
</tr>
<tr class="even">
<td><p>26週年</p></td>
<td><p>2015年</p></td>
<td style="text-align: right;"><p>135,000[11]</p></td>
<td style="text-align: right;"><p>46,600[12]</p></td>
<td style="text-align: right;"><p>$1,340,000[13]</p></td>
<td><p>全民團結爭民主 平反六四一起撐[14]</p></td>
<td style="text-align: right;"><p>2008年以來集會人數首次低於15萬。</p></td>
</tr>
<tr class="odd">
<td><p>27週年</p></td>
<td><p>2016年</p></td>
<td style="text-align: right;"><p>125,000[15]</p></td>
<td style="text-align: right;"><p>21,800[16]</p></td>
<td style="text-align: right;"><p>$1,740,000[17]</p></td>
<td><p>平反六四 停止濫捕 結束專政 力爭民主[18]</p></td>
<td style="text-align: right;"><p><a href="../Page/五區六四集會.md" title="wikilink">五區同時舉辦了六四集會</a>，以「本土、民主、反共、建國」為主題，人數在1000至5000人之間。</p></td>
</tr>
<tr class="even">
<td><p>28週年</p></td>
<td><p>2017年</p></td>
<td style="text-align: right;"><p>110,000[19]</p></td>
<td style="text-align: right;"><p>18,000[20]</p></td>
<td style="text-align: right;"><p>$1,410,000[21]</p></td>
<td><p>平反六四 結束專政 [22]</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="odd">
<td><p>29週年</p></td>
<td><p>2018年</p></td>
<td style="text-align: right;"><p>115,000[23]</p></td>
<td style="text-align: right;"><p>17,000[24]</p></td>
<td style="text-align: right;"><p>$1,480,000[25]</p></td>
<td><p>悼六四 抗威權</p></td>
<td style="text-align: right;"></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td style="text-align: right;"></td>
<td style="text-align: right;"></td>
<td style="text-align: right;"></td>
<td></td>
<td style="text-align: right;"></td>
</tr>
</tbody>
</table>

### 人數爭議

於2009年的燭光晚會，支聯會估計約有15萬人出席，[香港警務處指有](../Page/香港警務處.md "wikilink")6萬2千8百人，兩者數據差距超過一半。支聯會副主席[蔡耀昌解釋](../Page/蔡耀昌.md "wikilink")，警務處曾經向他們表示6個足球場連通道若然爆滿，可以容納接近10萬人，連同草地、籃球場和廣場皆爆滿，支聯會於是估算總數為有15萬。

《[星島日報](../Page/星島日報.md "wikilink")》根據場地[面積及每人佔用面積推斷計算](../Page/面積.md "wikilink")，得出出席人數介乎7萬至10萬。《星島日報》以科學化方式作出推斷計算，當晚出席者坐滿維多利亞公園6個足球場（包括四周通道），從[地圖上作出量度](../Page/地圖.md "wikilink")，6個足球場連同四周及中間通道，最寬鬆估計面積約為4萬[平方米](../Page/平方米.md "wikilink")，假設每人以坐姿算佔用空間為1平方米，坐滿6個足球場就有4萬人；假若以每人佔用0.7平方米計算，則坐滿6個足球場就有5萬7千人。連面積相若於3個足球場草地、[籃球場及](../Page/籃球場.md "wikilink")[噴水池所在的](../Page/噴水池.md "wikilink")[廣場](../Page/廣場.md "wikilink")，寬鬆估計總共最多有5個足球場的面積，合共總共參加人數應該介乎7萬3千至10萬4千人。

向來有就[七一遊行人數進行](../Page/七一遊行.md "wikilink")[統計的](../Page/統計.md "wikilink")[香港大學統計及精算系高級講師](../Page/香港大學.md "wikilink")[葉兆輝認同上述的推斷計算方法](../Page/葉兆輝.md "wikilink")，表示一般計算一個人坐着佔用空間相當於1平方米，以0.7平方米計算，場面已經非常擁擠；他表示難以看到6個足球場連通道能夠容納10萬人的可能性；香港大學社會科學研究中心主任[白景崇根據](../Page/白景崇.md "wikilink")[香港有線新聞於集會四周多角度](../Page/香港有線新聞.md "wikilink")[攝影的影片](../Page/攝影.md "wikilink")，大部分人手持1枝蠟燭，少部分手持兩支，少部分沒有，亦估計人數為介乎7至10萬；他認為警務處低估人數，主辦單位高估人數。

源自運輸機構消息透露，警務處於晚會舉行前晚曾經向運輸機構提供的人數預算與公開統計數字為一致。警務處消息人士稱，過去曾經在維多利亞公園足球場做實驗，安排警務人員企立或坐下，以計算該小幅面積在鬆動、適中及擠逼的情況下所可以容納的人數，再計算足球場的面積，以推斷估算若以站立姿勢計算，一個足球場最多可以容納約9千人，如果坐下計算，則最多可以容納約7千人\[26\]。

## 歷年參加情況

### 2003年

2003年6月4日晚上，六四事件14週年，大約50,000名香港人參加支聯會主辦的[燭光集會悼念活動](../Page/燭光.md "wikilink")，主題是「反對23，毋忘六四」，並反對[基本法第23條立法](../Page/香港基本法第二十三條.md "wikilink")。[支聯會主席](../Page/支聯會.md "wikilink")[司徒華表示](../Page/司徒華.md "wikilink")，即使有關法例通過，集會也不會停辦。一些以往沒有出席集會的香港人，也破例參加。群眾坐滿了維多利亞公園5個足球場。支聯會播放了民運人士[丁子霖和學運領袖](../Page/丁子霖.md "wikilink")[王丹的講話](../Page/王丹.md "wikilink")。在晚會開始前，香港的[天主教團體在維多利亞公園的另一角落](../Page/天主教.md "wikilink")，舉行[祈禱會](../Page/祈禱.md "wikilink")，有300多人參加。祈禱會名為「民主中國」，主題是「傾聽良心的呼聲，舞動生命的熱情」。時任天主教香港教區主教[陳日君樞機](../Page/陳日君.md "wikilink")，為[中國大陸的人民祈福](../Page/中國.md "wikilink")，也表達了他對中國大陸民主改革的盼望\[27\]。

### 2004年

2004年6月4日，估計有八萬多人參加了[維多利亞公園舉行的](../Page/維多利亞公園.md "wikilink")「六四」15週年紀念晚會。是1993年以來人數最多的一次。上百名參加「香港自由行」的大陸民眾在香港參加被[中共政府禁止的](../Page/中共.md "wikilink")「六四」紀念活動，創下了歷年的最高紀錄。組織者說收到了許多來自中國大陸的人民幣捐款。一共收到了大約4,000元人民幣捐款，遠比他們在過去舉辦的「六四」晚會多，過去只是收到一、兩百元人民幣。自1989年以來，受訪者平均參加了5.8次「六四」燭光晚會；4日晚參加集會的15歲或以上市民中，29%屬於首次參加，12%每次都有參加；而回歸以來每次都有參加者，則佔22%。

### 2006年

2006年六四燭光晚會，有44,000人參加，參加人數和往年類似。
根據[香港大學民意計劃](../Page/香港大學.md "wikilink")，在六四事件過去了17年後，仍然有53%的市民，認為當年北京學生做法正確。有56%的市民，依然認為北京當局要平反六四。[支聯會近年的口號是](../Page/支聯會.md "wikilink")「教育新一代，接好民主棒」。

### 2008年

2008年「六四」前夕，港大的民意調查顯示支持平反六四的市民人數略有下降，部分原因被認為是[北京奧運宣傳帶來的國家認同感及對](../Page/北京奧運.md "wikilink")5月[汶川大地震的同情](../Page/汶川大地震.md "wikilink")。燭光晚會本身亦結合了悼念地震遇難者的內容，且主辦方稱所有現場捐款將交予香港紅十字會支持地震災區重建。許多市民對於燭光晚會熱情不減，當晚主辦方在現場宣佈參與晚會人數超過48,000人。當年的燭光晚會在次日被[CCTV網站報導](../Page/CCTV.md "wikilink")，描述為「哀悼地震遇難同胞」，報導甚至有「向烈士紀念碑獻花」的內容，但有關報導在9日已被刪除。\[28\]

### 2009年

2009年是六四事件的二十週年，參與人數比往年多出很多。晚會的口號是「毋忘六四．繼承英烈志，薪火相傳．接好民主棒」。根據[支聯會的數字](../Page/支聯會.md "wikilink")，是年共有150,000人，警方的數字則為62,800人。晚會當晚除六個足球場及草地足球場，人群亦站滿大部份的維多利亞公園通道，而在維多利亞公園外的天后及銅鑼灣地區，亦都站滿了準備入場的市民。\[29\]港大民意網站亦有為晚會的人數作出估計，人數約為108,000至132,000人。\[30\]

[Szeto_Wah_at_21st_anniversary_candlelight_vigil.jpg](https://zh.wikipedia.org/wiki/File:Szeto_Wah_at_21st_anniversary_candlelight_vigil.jpg "fig:Szeto_Wah_at_21st_anniversary_candlelight_vigil.jpg")最後一次出席維園六四燭光晚會。\]\]

### 2010年

六四事件二十一周年，期間支聯會指香港受到不斷的政治打壓。晚會出席人數與2009年一樣，為150,000人，而且尚未計算未能入場的市民\[31\]。警方公佈人數亦達113,000人，為警方歷年最高數字。晚會加入了「釋放[劉曉波](../Page/劉曉波.md "wikilink")，支持[零八憲章](../Page/零八憲章.md "wikilink")」和「反對政治打壓」兩個主題。

[蘋果日報報導該年警方的人流控制措施與往年不同](../Page/蘋果日報_\(香港\).md "wikilink")，指出警方以特別劃出一條緊急通道為由封閉一條行人道，導致通往維園的[天后入口收窄](../Page/天后.md "wikilink")，造成樽頸位，引起市民鼓譟。此外有支聯會義工表示該年警方嚴格執行禁止工作人員或市民從主要入口進入維園，以往十分少見。報導綜合指出警方有意阻撓市民參與晚會。\[32\]

### 2011年

2011年1月2日，長期帶領香港人支持並推動中國民主運動的民主派領袖[司徒華病逝](../Page/司徒華.md "wikilink")，他生前定下六四事件二十二周年及辛亥革命一百週年的主題「平反六四，革命尚未成功；建設民主，同志仍須努力」。大會宣布晚會進場人數是150,000人，但由於警方在維園還未滿座、晚會還未開始就阻撓市民進入維園，有些市民未能進場，李卓人表示一定會交涉。雖然警方截龍阻止市民進入維園，但是他們稱有77,000人在場。\[33\]由於該年晚會事前曾舉辦「廣場的日與夜」學生營，因此晚會現場掛有一幅當年北京學生在[天安門廣場集會的印畫布幕](../Page/天安門廣場.md "wikilink")，參與晚會的人就彷彿置身在當年廣場的集會上\[34\]。

晚會先播出支聯會主席司徒華的錄影。這次參與悼念活動的，很多都是在六四事件後才出生的，顯示年青一代願意關心[中國大陸的民主民生發展](../Page/中國大陸.md "wikilink")。

### 2012年

2012年「六四」前夕，當年被坦克輾斷雙腿的民運人士[方政從美國順利入境香港](../Page/方政.md "wikilink")\[35\]\[36\]，到場參與晚會。當晚8時前，參與市民已坐滿所有6個足球場、草地及音樂亭，晚會亦因人潮進入其他通道需時而順延20分鐘開始\[37\]。支聯會公佈參加人數為180,000人，破歷年紀錄；而警方則估計最高峰時參加人數為85,000人\[38\]。方政致辭時表示，見到維園的燭光，感到非常震撼和感動，並感謝港人23年來的堅持。支聯會主席[李卓人致辭時批評候任行政長官](../Page/李卓人.md "wikilink")[梁振英是](../Page/梁振英.md "wikilink")「『六·四』變色龍」\[39\]。

### 2013年

[2013_Hong_Kong_Event_Remembering_the_1989_June_4th_Massacre_in_Beijing,_China_香港維園六四晚會.jpg](https://zh.wikipedia.org/wiki/File:2013_Hong_Kong_Event_Remembering_the_1989_June_4th_Massacre_in_Beijing,_China_香港維園六四晚會.jpg "fig:2013_Hong_Kong_Event_Remembering_the_1989_June_4th_Massacre_in_Beijing,_China_香港維園六四晚會.jpg")

由於大雨影響，現場音響器材受大雨影响而无法正常運作，為防漏電危險，烛光晚会在进行了大约半小时后，宣佈提前结束。[支联会宣布](../Page/支联会.md "wikilink")，当晚共有150,000人出席；而警方称有约54,000人参加\[40\]\[41\]。

### 2014年

2014年為六四事件[25週年](../Page/六四事件二十五周年.md "wikilink")，六四集會的人數創了歷年的新高。集會前席，由於多個團體宣布會在不同的地方舉行集會，如尖沙咀、上水及[英國駐香港總領事館](../Page/英國駐香港總領事館.md "wikilink")\[42\]，[支聯會主席](../Page/支聯會.md "wikilink")[李卓人一度擔心會削弱悼念六四的效果](../Page/李卓人.md "wikilink")\[43\]。然而，警方公布的維園晚會參與人數是99,500人，是歷年第二高的人數，主辦單位公布的數字為180,000人\[44\]。另外，尖沙咀舉辦的[六四集會亦有不少的民眾參與](../Page/尖沙咀六四集會.md "wikilink")，大會宣布參與人數為7,000人，警方則估計有3,060人出席\[45\]。

### 2015年

由於本土意識增長，2015年晚會的主題變成「立足本土守住香港」。是年，學聯在遭受多間大學退出學聯後決定首次不以學聯名義參與晚會，港大學生會則於港大舉行晚會、而熱血公民與普羅政治學苑則舉行「遍地開花64晚會」，舉辦向政改三人組畫像掟雞蛋等活動以紀念六四。大會宣佈集會人數為135,000人，而警方則表示約有三至四萬人參加集會。
經歷2014年[雨傘革命](../Page/雨傘革命.md "wikilink")，2015年晚會滲入[雨傘革命元素](../Page/雨傘革命.md "wikilink")，包括播放播放「傘捕者」家人心聲。

### 2016年

这年是六四事件二十七周年，主題为“哀悼民運死難同胞，繼承烈士民主遺志”。由于大学学生会另外举行六四论坛，令出席晚会的人数大减。支联会宣布大约有十二万五千人出席晚會，是自二零零九年六四事件二十周年以來八年的新低，警方则认为有二萬一千八百人出席晚会。\[46\]支聯會主席何俊仁對有逾十萬人參與集會非常感動，反映港人的堅持及原則、意志及勇氣，認為港人能堅持廿七年，是為歷史創造了一項紀錄，很值得驕傲。同一时间，烛光晚会举行期间有数名[香港民族陣綫](../Page/香港民族陣綫.md "wikilink")、[學生動源成員手持](../Page/學生動源.md "wikilink")[香港旗及](../Page/香港旗.md "wikilink")「[香港獨立](../Page/香港獨立.md "wikilink")」直幡並带著口罩的人，企图衝上大台，被現場的糾察按在地上，而其中[冼偉賢冲破防线](../Page/冼偉賢.md "wikilink")，走到台上高呼[香港獨立口號](../Page/香港獨立.md "wikilink")，最后被在場義工按在地上后抬離大台。

### 2017年

六四事件二十八週年，支聯會在維多利亞公園舉行集會悼念六四死難者及要求平反六四。香港各大專院校學生會表明不出席支聯會的活動。由於本土意識抬頭，香港年輕人更關心本土的政制發展，支聯會宣稱要代表中國人要求平反六四，在香港已不合時宜，香港各大專院校不會再進行六四悼念活動。[香港大學自行舉辦研討會](../Page/香港大學.md "wikilink")，[中文大學表明不會出席悼念活動](../Page/中文大學.md "wikilink")，並批評支聯會利用六四撈取政治利益，並表示中大更關心香港本土歷史。[嶺南大學](../Page/嶺南大學.md "wikilink")、[教育大學](../Page/教育大學.md "wikilink")、[公開大學](../Page/公開大學.md "wikilink")、[珠海學院及](../Page/珠海學院.md "wikilink")[恆生管理學院](../Page/恆生管理學院.md "wikilink")，在公開大學校園合辦研討會，但不會有悼念活動，[浸會大學學生會則直接指出六四事件是鄰國的事](../Page/浸會大學.md "wikilink")，如同韓國的光州事件不值得香港人特別紀念\[47\]。

## 軼聞

香港首任行政長官[董建華曾經於](../Page/董建華.md "wikilink")[香港回歸前後和](../Page/香港回歸.md "wikilink")[司徒華傾談](../Page/司徒華.md "wikilink")，希望對方別再舉辦「六四」活動，司徒華當時卻只回應了一句，指對方太不了解他。\[48\]

## 晚會相集

Candlelight Vigil for June 4 Massacre 2007 - 001.JPG|2007年的六四燭光晚會
Candlelight Vigil for June 4 Massacre 2007 -
006.JPG|2007年的六四燭光晚會，參加者手持[蠟燭唱民運歌曲](../Page/蠟燭.md "wikilink")
Candlelight Vigil for June 4 Massacre 2007 -
004.JPG|2007年的六四燭光晚會的司令台，寫有「平反六四，支持維權」的標語
2008june4.JPG|2008年的六四燭光晚會，結合了對[汶川地震遇難者的哀悼](../Page/汶川地震.md "wikilink")
Candlelight Vigil for June 4 Massacre 2009.JPG|2009年的六四燭光晚會，主辦單位自製的烈士碑
Victoria Park Hong Kong Tiananmen Vigil 2009.jpg|2009年的六四燭光晚會：六四廿年 薪火相傳
21st anniversary of the June 4th incident in Victoria
Park.jpg|2010年六四燭光晚會
People_in_HK_memory64_voa6.jpg|2012年六四燭光晚會 2013.6.4 Hong Kong
Event Remembering the 1989 June 4th Massacre in Beijing, China
香港維園六四晚會.jpg|2013年六四紀念碑 香港六四天安門大屠殺24周年紀念 Remembering the
24th Anniversary of 1989 June 4th Tianamen Massacre in Hong
Kong.jpg|2013年悼念圖樣 Candlelight vigil for the 26th anniversary of the
June 4th incident in Victoria Park, Hong Kong.JPG|2015年六四燭光晚會

## 參考文獻

## 外部連結

  - [香港市民支援愛國民主運動聯合會官方網頁](http://alliance.org.hk/)

  - [「六四」弔唁冊](https://web.archive.org/web/20090529011907/http://www.alliance.org.hk/sign64/)

  - [「六四」二十週年網站](https://web.archive.org/web/20090523122748/http://www.alliance.org.hk/64/6420/)

  -
## 參看

  - [六四襟章](../Page/六四襟章.md "wikilink")
  - [民主歌聲獻中華](../Page/民主歌聲獻中華.md "wikilink")
  - [全球華人大遊行](../Page/全球華人大遊行.md "wikilink")
  - [尖沙咀六四集會](../Page/尖沙咀六四集會.md "wikilink")
  - [六四事件二十五周年](../Page/六四事件二十五周年.md "wikilink")

{{-}}

[Category:六四事件](../Category/六四事件.md "wikilink")
[Category:香港民主運動](../Category/香港民主運動.md "wikilink")
[Category:香港維多利亞公園](../Category/香港維多利亞公園.md "wikilink")
[Category:集會](../Category/集會.md "wikilink")

1.  [六四青年網](http://youth.alliance.org.hk/)

2.  [齊來參加「支青組」](http://www.hkptu.org/ptu/director/pubdep/ptunews/424/424p203.htm)，教協報，424期，2001年10月22日

3.  [「六四」十八週年燭光悼念集會- 程序](http://www.alliance.org.hk/64/6418/index.php)
    ，「六四」18週年專頁，香港市民支援愛國民主運動聯合會

4.  明報20090606:燭光晚會籌210萬破紀錄

5.  尚未計算未能進場的市民

6.  尚未計算未能進場的市民

7.  [香港支聯會稱18萬人出席「六四」燭光晚會](http://www.bbc.co.uk/zhongwen/trad/china/2014/06/140604_64_alliance_hk_vigil.shtml)

8.  [維園十萬燭光悼念六四](https://hk.news.yahoo.com/%E7%B6%AD%E5%9C%92%E5%8D%81%E8%90%AC%E7%87%AD%E5%85%89%E6%82%BC%E5%BF%B5%E5%85%AD%E5%9B%9B-215839391.html)

9.  [六四25周年纪念活动
    维园集会高峰9万人](http://www.nanzao.com/sc/hk/28823/liu-si-25zhou-nian-ji-nian-huo-dong-wei-yuan-ji-hui-gao-feng-9mo-ren)

10.

11. [大會宣布13.5萬人出席維園六四晚會](http://topick.hket.com/article/611350/%E5%A4%A7%E6%9C%83%E5%AE%A3%E5%B8%8313.5%E8%90%AC%E4%BA%BA%E5%87%BA%E5%B8%AD%E7%B6%AD%E5%9C%92%E5%85%AD%E5%9B%9B%E6%99%9A%E6%9C%83)

12.

13.

14. [支聯會宣佈六四燭光晚會主題](http://cablenews.i-cable.com/webapps/news_video/index.php?news_id=458845)

15.

16.
17.

18.

19.

20.
21.

22.

23.

24.

25.
26. [學者指六球場難容十萬人
    《星島日報》 2009年6月6日](http://std.stheadline.com/archive/fullstory.asp?id=20090606a03)

27.

28. [香港四萬市民燭光晚會哀悼地震遇難同胞(圖)](http://sports.cctv.com/20080605/105378_1.shtml)（[网上截图](http://hrichina.org/sites/default/files/oldsite/fs/downloadables/pdf/2008.06.05_cctv_64.PDF)）

29.

30.

31. 根據晚會當日李卓人公佈警方的通知，在臨結束前，警方需要封閉糖街及內告士打道，以容納等候進場的市民。

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48. 《鏗鏘集》 6/6/2010 華叔您好