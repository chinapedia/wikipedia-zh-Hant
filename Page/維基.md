[Wikipedia_screenshot_on_Nexus_9_20141121.jpg](https://zh.wikipedia.org/wiki/File:Wikipedia_screenshot_on_Nexus_9_20141121.jpg "fig:Wikipedia_screenshot_on_Nexus_9_20141121.jpg")
**維基**一詞出自「[Wikipedia](../Page/Wikipedia.md "wikilink")」的中文譯名「[維基百科](../Page/維基百科.md "wikilink")」，是[維基媒體基金會](../Page/維基媒體基金會.md "wikilink")（）的一個[商標](../Page/商標.md "wikilink")，用於所有由維基媒體基金會營運、使用[Wiki引擎發展的中文站點上](../Page/Wiki引擎.md "wikilink")。「維」字意思為繫物的大[繩](../Page/繩.md "wikilink")，也可作[網的解釋](../Page/網.md "wikilink")，可以引申為[網際網路](../Page/網際網路.md "wikilink")，「基」是指事物的根本，或是建築物的底座。「維基百科」合起來可引申為網際網路中裝載人類基礎知識的[百科全書](../Page/百科全書.md "wikilink")，是一種採用[Wiki協作系統技術的線上百科](../Page/Wiki.md "wikilink")。

除了維基百科外，其他以「維基」這個商標命名的網站還包括[維基詞典](../Page/維基詞典.md "wikilink")、[維基教科書](../Page/維基教科書.md "wikilink")、[維基語錄](../Page/維基語錄.md "wikilink")、[維基共享資源](../Page/維基共享資源.md "wikilink")、[維基新聞](../Page/維基新聞.md "wikilink")、[維基文庫](../Page/維基文庫.md "wikilink")、[維基物種](../Page/維基物種.md "wikilink")、[維基數據](../Page/維基數據.md "wikilink")、[維基導遊及](../Page/維基導遊.md "wikilink")[元維基](../Page/元維基.md "wikilink")。「維基百科」這個譯名最早是由[中文維基百科使用者ghyll](../Page/中文維基百科.md "wikilink")（現Mountain）於2002年11月28日時提出，並由中文維基百科社群十餘位早期參與者於2003年10月21日經投票產生。

現在“維基”一詞亦可指作Wiki的中文翻譯，以及基於使用Wiki引擎的網站。

## Wiki與維基

由於維基媒體基金會早期並未留意到商標問題，隨著維基百科的知名度和影響力日增，連帶提升社會大眾接觸與使用[Wiki引擎來參與協作的機會](../Page/Wiki引擎.md "wikilink")，「維基」一詞也被用廣泛使用作「wiki」的譯名，如：《[维基经济学](../Page/维基经济学.md "wikilink")》、[台灣棒球維基館](../Page/台灣棒球維基館.md "wikilink")、[量子化學維基](https://web.archive.org/web/20060622102850/http://wiki.quantumchemistry.net/index.php/%E9%A6%96%E9%A1%B5)等等。目前「維基」為維基媒體基金會的在台湾注册的商標。\[1\]\[2\]Wiki的中文譯名除**維基**外尚有**圍紀**、**快紀**、**維客**、**共筆**等，但現時尚未有統一。

## Wikileaks與維基

專門公開來自匿名來源和網絡洩露的文檔之網站[Wikileaks](../Page/Wikileaks.md "wikilink")，被中文媒體譯作維基解密、維基揭密及維基洩密。正因如此，部份中文媒體甚至將「維基」一詞作為Wikileaks的簡稱\[3\]。事實上，維基媒體基金會及其旗下網站維基百科，與WikiLeaks沒有任何關係。

## 相關條目

  - [維基媒體基金會](../Page/維基媒體基金會.md "wikilink")
  - [维基百科](../Page/维基百科.md "wikilink")
  - [Wiki](../Page/Wiki.md "wikilink")
  - [Wiki引擎](../Page/Wiki引擎.md "wikilink")
  - [Wiki農場](../Page/Wiki農場.md "wikilink")

## 参考文献

## 外部链接

  - [Wikipedia:Wikipedia的中文名](../Page/Wikipedia:Wikipedia的中文名.md "wikilink")
  - [Wikipedia:Wikipedia的中文名/投票結果](../Page/Wikipedia:Wikipedia的中文名/投票結果.md "wikilink")

[Category:Wiki](../Category/Wiki.md "wikilink")
[Category:维基百科](../Category/维基百科.md "wikilink")

1.  [「維基」在台灣的商標註冊資料](http://tmsearch.tipo.gov.tw/RAVS/wfm20103.jsp?Applno=096031048&ExamNo=01313993)
2.  [維基百科創始人訪華接受中文商標捐贈](http://tech.sina.com.cn/i/2009-09-15/13523439436.shtml)．[新浪科技](../Page/新浪.md "wikilink")，2009-9-15
3.  例子包括《明報》：[維基指泰國官員憂王儲登基](https://web.archive.org/web/20101219205541/http://hk.news.yahoo.com/article/101216/4/lslj.html)、《星島日報》：[「維基」爆暗殺利特維年科內情](http://hk.news.yahoo.com/article/101212/3/lq1v.html)、路透社譯稿：[維基：美圖阻止委國取得俄飛彈](http://hk.news.yahoo.com/article/101212/21/lq5p.html)、法新社譯稿：[專家：挺維基攻擊不算網路戰爭](http://hk.news.yahoo.com/article/101212/8/lq57.html)等等