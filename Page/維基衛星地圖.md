**WikiMapia**是結合[Google
Maps及](../Page/Google_Maps.md "wikilink")[Wiki引擎的計劃](../Page/Wiki引擎.md "wikilink")，目標是「描述地球表面」。由和Evgeniy
Saveliev發起。他們自稱是受到 Google Maps 及維基百科啟發的。

在這個網站上，使用者可以在Google
Maps提供的地圖上標定一塊矩形區域，稱之為「hotspot」。使用者可替hotspot加上一段文字資訊。這段文字包含三個部分：標題、內容、標籤。三個部分都開放給不特定使用者編輯。網站並且保存編輯歷史。網站還替同一塊hotspot提供了多語言版本功能，也就是說同一塊hotspot可以有多種語言的文字資訊。hotspot的形狀是矩形，容許的最大邊長是20公里左右。網站也提供一個提報刪除某塊hotspot的超連結。

網站也提供標籤搜尋功能。針對某些標籤作搜尋時，使用者可以在自己的地圖上只顯示有這些標籤的hotspot。

版權方面，WikiMapia原先遵從[Google
Maps之使用條款](../Page/Google_Maps.md "wikilink")，2012年起宣布改用[姓名標示-相同方式分享
(CC-BY-SA)](../Page/创作共用.md "wikilink")。

## 类似项目

  - [EEmap](../Page/EEmap.md "wikilink")
  - [Google Earth](../Page/Google_Earth.md "wikilink")
  - [OpenStreetMap](../Page/開放街圖.md "wikilink")
  - [Placeopedia](../Page/Placeopedia.md "wikilink")
  - WikiMapia
  - [Windows Live Local](../Page/Windows_Live_Local.md "wikilink")
  - [World Wind](../Page/World_Wind.md "wikilink")
  - [Yandex地图编辑器](../Page/Yandex地图编辑器.md "wikilink")

## 参考文献

## 外部連結

  - [WikiMapia](http://www.wikimapia.org)

{{-}}

[Category:地理信息系统](../Category/地理信息系统.md "wikilink")
[Category:电子地图](../Category/电子地图.md "wikilink")
[Category:协作项目](../Category/协作项目.md "wikilink")
[Category:Wiki社群](../Category/Wiki社群.md "wikilink")
[Category:2006年建立的网站](../Category/2006年建立的网站.md "wikilink")
[Category:众包](../Category/众包.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")