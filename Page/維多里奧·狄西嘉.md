**維多里奧·狄西嘉**（[義大利語](../Page/義大利語.md "wikilink")：**Vittorio De
Sica**，）是一位[義大利](../Page/義大利.md "wikilink")[導演與](../Page/導演.md "wikilink")[演員](../Page/演員.md "wikilink")，也是電影史上一位相當重要的導演，四次[奥斯卡最佳外语片获奖导演](../Page/奥斯卡最佳外语片.md "wikilink")。

## 生平

## 作品

  - 1946年：《》(Sciuscià)
  - 1948年：《[單車失竊記](../Page/單車失竊記.md "wikilink")》(Ladri di biciclette)
  - 1951年：《[慈航普渡](../Page/慈航普渡.md "wikilink")》(Miracolo a Milano)
  - 1952年：《[風燭淚](../Page/風燭淚.md "wikilink")》(Umberto D.)
  - 1953年：《[終點站](../Page/終點站.md "wikilink")》(Stazione Termini)
  - 1963年：《[昨日今日明日](../Page/昨日今日明日.md "wikilink")》(Ieri, oggi e domani)
  - 1964年：《[意大利式婚姻](../Page/意大利式婚姻.md "wikilink")》(Matrimonio
    all'italiana)
  - 1967年：《[七段情](../Page/七段情.md "wikilink")》(Woman Times Seven)
  - 1968年：《[淚灑相思地](../Page/淚灑相思地.md "wikilink")》(A Place for Lovers)
  - 1971年：《[費尼茲花園](../Page/費尼茲花園.md "wikilink")》(Il Giardino dei
    Finzi-Contini)

## 外部連結

  -
[Category:義大利導演](../Category/義大利導演.md "wikilink")
[Category:義大利演員](../Category/義大利演員.md "wikilink")
[Category:意大利电影演员](../Category/意大利电影演员.md "wikilink")
[Category:柏林影展獲獎者](../Category/柏林影展獲獎者.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:奥斯卡最佳外语片获奖导演](../Category/奥斯卡最佳外语片获奖导演.md "wikilink")
[Category:20世纪电影导演](../Category/20世纪电影导演.md "wikilink")
[Category:意大利罗马天主教徒](../Category/意大利罗马天主教徒.md "wikilink")