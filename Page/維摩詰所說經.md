《**維摩詰所說經**》（****・・・****
ゆいまきょう／ヴィマラキールティ・ニルデーシャ・スートラ)。漢文大藏經中曾翻譯七遍，而目前僅剩三種版本：三國時代之支謙譯本／魏晉南北朝時之鳩摩羅什譯本／初唐朝時之三藏法師譯本。目前流通的版本中，由[**鳩摩羅什**所翻譯的版本為](../Page/鸠摩罗什.md "wikilink")《**維摩詰所說經**》最為通行，而有**[玄奘](../Page/玄奘.md "wikilink")**的版本則名為《**不可思議解脫經**》或《**淨名經**》，是[初期](../Page/初期佛教.md "wikilink")[大乘佛教](../Page/大乘佛教.md "wikilink")[經典於](../Page/佛经.md "wikilink")[吐蕃僧諍記於吐蕃王朝的三耶寺中](../Page/堪布摩诃衍.md "wikilink")[支那的](../Page/支那.md "wikilink")[大乘和尚和](../Page/大乘和尚.md "wikilink")[天竺的](../Page/天竺.md "wikilink")[寂護論師和其弟子](../Page/寂護.md "wikilink")[蓮花戒論師的激變過程](../Page/蓮花戒.md "wikilink")，共歷時二年\[1\]。本部經典的發生地點為今日的藏區，也就是吐蕃王朝的第一間中印風格的佛教寺院，更可以追溯到[阿含經中的周利槃陀伽在](../Page/阿含經.md "wikilink")[大乘佛法傳至東方藥師琉璃佛國的中華疆域之具體菩薩相](../Page/大乘佛法.md "wikilink")。

本经典的梵文本於西元1999年於[布达拉宫中发现](../Page/布达拉宫.md "wikilink")，被判定为12世纪（1140年）的手抄本，由[聖嚴法師的母校大正大學的團隊發現](../Page/釋聖嚴.md "wikilink")。

在中國的甘肅省的[敦煌石窟中](../Page/敦煌石窟.md "wikilink")，有許多維摩詰的變經圖畫，而以前在吐蕃統治敦煌時期時僧人也以演繹的方式來吸引觀眾，可以說是佛法世俗與大眾化的開端和鼻祖。本經也是少數在法會時不使用，但反而以文學作品流傳於世的佛教文學經典，胡適將它說是「最美的佛典文學」。

『維摩』代表「無垢」，為鳩摩羅什的音譯，而後玄奘所翻譯的『無垢』則為「維摩」意譯。羅什所翻經名中之『詰』字在梵文為
“Nirdésa"，意在「解說」和「宣說」，英文應可翻譯為
"EXPOUND"，而玄奘則就用『說』字來翻譯。就三種版本中，只有羅什將佛經的主角歸於維摩詰本身，而支謙和玄奘還是將佛經的主角放在佛陀身上。

### 佛經之主要內容與意義

經中的主角人物是以[居士身份出身的](../Page/居士.md "wikilink")[維摩詰菩薩](../Page/維摩詰菩薩.md "wikilink")：維摩詰居士的家財萬貫，平常救助貧民、布施僧侶，樂善好施；而且不執著於外相，為了度化眾生，維摩詰居士可以向天神天魔說法，也可以向王公貴族說法，甚至在[妓院](../Page/妓院.md "wikilink")、[赌場向貪歡求樂的鄉民说法](../Page/赌場.md "wikilink")。經文主要講述了發生在毗耶離的一次法會，敘述維摩詰菩薩與諸位[菩薩和](../Page/菩薩.md "wikilink")[聲聞](../Page/聲聞.md "wikilink")[羅漢滿智慧和譬喻的問答](../Page/羅漢.md "wikilink")。

## 影響

《维摩诘经》中的故事性很強，例如[天女散花](../Page/天女散花.md "wikilink")、请饭香土等等，故事人物鲜活，想象奇迥，富于文学趣味。用浅近的方法引生大众的信仰，是大乘经典的一大特色。南北朝时期的文人学士最愛[清谈與](../Page/清谈.md "wikilink")《维摩经》\[2\]，六朝志怪作品深受《维摩诘经》中佛教教义、文学手法之影响，大量反映恶报、地狱等场面。[僧肇在读](../Page/僧肇.md "wikilink")《[道德经](../Page/道德经.md "wikilink")》有
“美则美矣，然期神冥累之方，犹未尽善也”之感叹，后来读到《维摩诘经》，決定出家。《高僧传》记载[支遁在山阴讲](../Page/支遁.md "wikilink")《维摩经》。\[3\][王安石作过一首](../Page/王安石.md "wikilink")《读〈维摩经〉有感》的诗：“身如泡沫亦如风，刀割香涂共一空。宴坐世间观此理，维摩虽病有神通。”中唐著名詩人[王維](../Page/王維.md "wikilink")，從小便因其母信佛的影響，成年後將自己的字就是**「摩詰」**，故後世也稱[王維為](../Page/王維.md "wikilink")**王摩詰**。

《维摩诘经》的“不二法门”和思想，深深影響了[禅宗的](../Page/禅宗.md "wikilink")“不二”思想。即所謂的“动静不二，真妄不二，维摩明一切法皆入不二门。”\[4\]《维摩诘经》中的许多典故，多变成禅宗公案。宋代以後的士子多好禅学，常講《[金剛经](../Page/金剛经.md "wikilink")》、《[楞严经](../Page/楞严经.md "wikilink")》和《[圆觉经](../Page/圆觉经.md "wikilink")》。自此《维摩诘经》的影响變小。\[5\]

### 各種語言中的維摩詰經

[Old_Tibetan_Chronicle,_Pelliot_tibétain_1287,_p._21._Vimalakīrti_Nirdeśa_Sūtra_in_Chinese.jpg](https://zh.wikipedia.org/wiki/File:Old_Tibetan_Chronicle,_Pelliot_tibétain_1287,_p._21._Vimalakīrti_Nirdeśa_Sūtra_in_Chinese.jpg "fig:Old_Tibetan_Chronicle,_Pelliot_tibétain_1287,_p._21._Vimalakīrti_Nirdeśa_Sūtra_in_Chinese.jpg")》寫本反面的維摩詰經片段\]\]
[維摩詰經在敦煌寫本中也有殘卷，但由於殘破不全，只留了二巻](http://www.fgu.edu.tw/~cbs/pdf/人間佛教新一卷二期/a11.pdf)。目前一般認為有二種藏文的版本流傳於世間，但是如果包括東部木板刻本和西部手寫書本的話，則可稱有三種版本。

## 佛經版本

維摩詰經約在西元100年前後，開始在印度流傳。在西元183年，由支謙首次漢譯。

目前中文的版本有三種版本，分別由三國[支謙](../Page/支謙.md "wikilink")／[羅什](../Page/鳩摩羅什.md "wikilink")／[玄奘大師的版本](../Page/玄奘.md "wikilink")，其他版本則不明或未發現。

《維摩詰所說經》曾有七種[漢文譯本](../Page/漢文.md "wikilink")，現存三種\[6\]，分別是

1.  [三國時期](../Page/三國.md "wikilink")[吳](../Page/東吳.md "wikilink")[支謙](../Page/支謙.md "wikilink")《佛說維摩詰經》三卷\[7\]
2.  [後秦](../Page/後秦.md "wikilink")[鳩摩羅什](../Page/鳩摩羅什.md "wikilink")
    《維摩詰所說經》三卷\[8\]
3.  [唐朝](../Page/唐朝.md "wikilink")[玄奘](../Page/玄奘.md "wikilink") 《說無垢稱經》
    六卷 \[9\]

其中鳩摩羅什的譯本流傳最廣、歷久不衰，收于大正藏第14册。

於清代時，乾隆大藏經有參考藏文版本，收藏於乾隆大藏經（俗稱中文龍藏經，以免與康熙版藏文龍藏經混淆）：

1.  [龍藏經](../Page/龍藏經.md "wikilink")《[維摩詰所說大乘經](https://drive.google.com/file/d/1YgdjWfBiubEQdn-pEHcRTG1VJDj0t7gD/view?usp=sharing)》三卷

東晉的[僧肇](../Page/僧肇.md "wikilink")、[南朝梁的](../Page/梁_\(南朝\).md "wikilink")[智顗](../Page/智顗.md "wikilink")（天台智者）、[慧遠](../Page/慧遠.md "wikilink")、[隋末](../Page/隋朝.md "wikilink")[唐初的](../Page/唐朝.md "wikilink")[嘉祥吉藏](../Page/嘉祥吉藏.md "wikilink")、唐朝[慈恩窺基都曾為此經做註記](../Page/慈恩窺基.md "wikilink")。在日本，有聖德太子等人著評論。

無論如何，這本經典可以稱為藏傳佛教寧瑪教派的基本論述，也普遍對於中國佛教有顯著的影響，但藏文版本與玄奘大師的版本更為相近。

維摩經在禪宗與淨土宗中，都有极为重要的地位，而在藏傳佛教中也是[丹珠爾的首卷中](../Page/丹珠爾.md "wikilink")。其中最重要的思想主要有三：藏傳寧瑪教的大圓滿思想、印度初期大乘思想的開播、和男女合一與福彗雙全之禪淨方便與般若思路。

[星雲大師](https://www.youtube.com/watch?v=teb2U4SNdpU)甚至於於星雲大師全集第七本提到：我將每一個人都當作維摩居士。因此我們可以總結：《維摩詰經》對於漢傳佛教與人間佛教的關係不須否認，王維等唐代詩人皆以維摩居士為榜樣。

## 藏文版佛經

8世紀譯經僧法性戒所翻譯，名為《聖無垢稱所說經》（आर्यविमलकीर्तिनिर्देशो नाम
महायानसूत्रम्，Āryavimalakīrtinirdeśo Nāma
Mahāyānasūtram）。目前收錄於德格版、北京版、奈塘版西藏大藏經中。

敦煌殘本，譯者不詳，南無藥師琉璃光如來。

本經在藏傳佛教[大藏經當中](../Page/大藏經.md "wikilink")，則稱為《**無垢經**／དྲི་མ་མེད་པ》，表示藏傳佛教的禪密淵源。該經典共計三卷(fasicles)十四品(chapters)，經名則以[維摩詰居士命名](../Page/維摩詰菩薩.md "wikilink")。最通行的版本由[姚秦](../Page/姚秦.md "wikilink")[三藏法師](../Page/三藏法師.md "wikilink")[鳩摩羅什譯](../Page/鳩摩羅什.md "wikilink")。它以辯論（類似臨濟的頓悟禪法）的方式來詳細說明大乘佛教與[小乘佛教在教義上的分別](../Page/小乘佛教.md "wikilink")，集中討論[不二論](../Page/不二論.md "wikilink")。\[10\]維摩詰居士是從東方妙喜國來的，金栗如來的轉世，現菩薩身。目前研究維摩經典，屬於[佛光大學最為興盛](../Page/佛光大學.md "wikilink")，由萬金川院長領導。就目前研究显示，此经被翻译为汉文和藏文，于清朝出现于康熙龙藏经，之后出现于乾隆大藏经中则出现了汉藏融合版本，但仍然挂名为鸠摩罗什所翻译。

### 梵文本

1981年，[西藏高等研究中央學院出版了由藏文版](../Page/西藏高等研究中央學院.md "wikilink")《維摩詰經》翻譯為梵文的*Vimalakirtinirdesasutra*。1999年，日本[大正大學](../Page/大正大學.md "wikilink")[高橋尚夫教授](../Page/高橋尚夫.md "wikilink")，在中國政府允許下，在[西藏](../Page/西藏.md "wikilink")[布達拉宮進行文獻考查時](../Page/布達拉宮.md "wikilink")，在《智光明莊嚴經》抄寫本中，發現現存最早的《維摩詰經》梵文寫本，並由大正大學梵語佛典研究會出版《梵漢藏対照『維摩経』》（2004）、《梵文維摩経－ボタラ宮所藏写本につく校訂》（2006）。

## 相關條目

  - [維摩詰菩薩](../Page/維摩詰菩薩.md "wikilink")

## 參考文獻

## 外部連結

  - 陸揚：〈[論《維摩詰經》和淨土思想在中國中古社會之關係](http://www.chibs.edu.tw/ch_html/LunCong/099/p207-220.htm)〉。
  - [Multilingual edition of Vimalakīrtinirdeśasūtra in the Bibliotheca
    Polyglotta](https://www2.hf.uio.no/polyglotta/index.php?page=volume&vid=37)
  - [文本對勘與漢譯佛典的語言研究：以《維摩經》為例](http://enlight.lib.ntu.edu.tw/FULLTEXT/JR-BM054/bm054397035.pdf)

<!-- end list -->

  -
[Category:經集部](../Category/經集部.md "wikilink")

1.
2.  《鲁迅全集》第五卷

3.  《高僧传》卷四

4.  《古尊宿语录》卷三三。

5.  [《六祖壇經箋註》，[丁福保居士著](../Page/丁福保.md "wikilink")](http://book.bfnn.org/books2/1295.htm#a06)

6.  林怡君，《維摩詰經》研究 ，國語文學系國語文教學碩士班論文，國立臺南大學，2008。

7.  [CBETA T14 No. 474
    《佛說維摩詰經》](http://www.cbeta.org/result/T14/T14n0474.htm)


8.  [CBETA T14 No. 475
    《維摩詰所說經》](http://cbeta.org/result/T14/T14n0475.htm)

9.  [CBETA T14 No. 476
    《說無垢稱經》](http://www.cbeta.org/result2/T14/T14n0476.htm)


10. 徐燕枝，《維摩詰經》空觀與菩薩行之研究 ，宗教學系研究所碩士論文，私立輔仁大學，2012。