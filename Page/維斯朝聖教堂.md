**維斯教堂**（**Wieskirche**，全称**Wallfahrtskirche zum Gegeißelten Heiland auf
der
Wies**，意为“維斯受鞭打的救世主朝圣教堂”），位于[德国](../Page/德国.md "wikilink")[巴伐利亚州](../Page/巴伐利亚州.md "wikilink")[施泰因加登](../Page/施泰因加登.md "wikilink")（Steingaden）镇维斯区，於1745年至1754年由[约翰·巴普蒂斯特·齐默尔曼](../Page/约翰·巴普蒂斯特·齐默尔曼.md "wikilink")（Johann
Baptist
Zimmermann，1680年1月3日—1758年3月2日）和[多米尼库斯·齐默尔曼](../Page/多米尼库斯·齐默尔曼.md "wikilink")（Dominikus
Zimmermann，1685年6月30日—1766年11月16日）兄弟设计建造，他们都是德国[洛可可风格的画家和建筑师](../Page/洛可可.md "wikilink")。維斯教堂采用了當時流行的[洛可可設計](../Page/洛可可.md "wikilink")，因相传教堂内的救世主像曾落泪而成为圣地，是一座雄伟的[朝圣教堂](../Page/朝圣.md "wikilink")，其室內裝潢相當出色，1983年被列为[世界文化遗产](../Page/世界文化遗产.md "wikilink")。

## 历史

[Wies_altar.jpg](https://zh.wikipedia.org/wiki/File:Wies_altar.jpg "fig:Wies_altar.jpg")维斯教堂中供奉的是一尊受鞭打的[救世主雕像](../Page/救世主.md "wikilink")，由帕特尔·马格努斯·施特劳布（Pater
Magnus Straub）和卢卡斯·施魏格尔（Lukas
Schweiger）兄弟于1730年在上巴伐利亚的施泰因加登修道院雕刻完成。传说1738年6月14日，维斯的一位名叫玛丽亚·洛吕（Maria
Lory）的女农民在救世主雕像的眼睛中看到了几滴眼泪，消息传开以后，很快引来了[礼拜和](../Page/礼拜.md "wikilink")[朝圣的人潮](../Page/朝圣.md "wikilink")，人们蜂拥前往一睹传说中流泪的救世主雕像，为了满足人们的需求，维斯建造起了一座小型的[礼拜堂](../Page/礼拜堂.md "wikilink")，它就是维斯教堂的前身。

1745年至1754年，齐默尔曼兄弟在Abt Marinus II.
Mayer的带领下建造了现在这座[洛可可风格的维斯教堂](../Page/洛可可.md "wikilink")，[圣坛上的画作是由](../Page/圣坛.md "wikilink")[慕尼黑的宫廷画师巴尔塔萨](../Page/慕尼黑.md "wikilink")·奥古斯特·阿尔布雷希特（Balthasar
August
Albrecht）创作的；四位西方世界伟大[神学家希尔尼玛斯](../Page/神学家.md "wikilink")（Hieronymus）、[安波罗修](../Page/安波罗修.md "wikilink")、[圣奥古斯丁和](../Page/圣奥古斯丁.md "wikilink")[额我略一世的雕像](../Page/额我略一世.md "wikilink")，是[蒂罗尔画家安东](../Page/蒂罗尔.md "wikilink")·施图尔姆（Anton
Sturm）的成熟期作品；教堂的[管风琴造于](../Page/管风琴.md "wikilink")1957年。

随着巴伐利亚地区[政教分离的开始](../Page/政教分离.md "wikilink")，19世纪初维斯教堂曾被下令准备拍卖和拆除掉，被当地的农民抢救了下来，也使得朝圣能够继续。

## 文化

[Wieskirche1998.jpg](https://zh.wikipedia.org/wiki/File:Wieskirche1998.jpg "fig:Wieskirche1998.jpg")
1983年维斯教堂入选[世界文化遗产名录](../Page/世界文化遗产.md "wikilink")，1985年至1991年花费了1060万马克进行整修。现在每年有超过一百万人来到这里，教堂内经常性地举宗教音乐会。5月1日是维斯每年朝圣的开始，而每年的6月14日和接下来的星期日则是“耶稣的眼泪”节，以纪念救世主雕像落泪奇迹的发生和维斯圣地的建立。

## 参考文献

<div class="references-small">

  - Deutsche UNESCO-Kommission e.V.: *[Wallfahrtskirche "Die
    Wies"](http://www.unesco.de/295.html?&L=0)*.
  - UNESCO: *[Advisory Body
    Evaluation](http://whc.unesco.org/archive/advisory_body_evaluation/271.pdf)*.
  - Wieskirche.de: *[Wallfahrtskirche zum Gegeißelten Heiland auf der
    Wies](http://www.wieskirche.de/)*.

</div>

## 外部連結

  - [維斯教堂官方网站](http://www.wieskirche.de/)

[Category:德国天主教堂](../Category/德国天主教堂.md "wikilink")
[W](../Category/德国世界遗产.md "wikilink")