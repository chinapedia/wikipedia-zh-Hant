**部落客**（**Blogger**）一般是指經營[部落格](../Page/部落格.md "wikilink")（**Blog**）的人，在台灣只要於[痞客邦
PIXNET](../Page/痞客邦.md "wikilink")、[隨意窩
Xuite](../Page/隨意窩_Xuite日誌.md "wikilink")、[Blogger(service)](../Page/Blogger.md "wikilink")...等免費**BSP**網誌平台註冊帳戶，或使用[WordPress系統自架網站](../Page/WordPress.md "wikilink")，就是一名「**部落客**」**作者**，除了一部份人寫興趣外，依工作型態可分為**全職部落客**與**兼職部落客**；依主題又可分為**美食部落客**、**旅遊部落客**、**親子部落客**、**美妝部落客**、**3C部落客**...等，與[YouTuber](../Page/YouTuber.md "wikilink")、[IG客](../Page/IG客.md "wikilink")、[臉書客](../Page/臉書.md "wikilink")、[網紅](../Page/网红.md "wikilink")、[直播主](../Page/直播.md "wikilink")...等同屬於[自媒體的一種](../Page/自媒体.md "wikilink")。

## 百大部落客

然而只要有**部落格**帳號就可以撰寫文章，並非所有「**部落客**」**作者**都能定時更新或提供有價值的網誌內容，因此[痞客邦社群影響力](https://pixranking.events.pixnet.net/)就成為指標，以搜尋力(Search)、擴散力(Share)、人氣力(Publicity)、互動力(Engagement)
、號召力(Drive)...等五力進行分析，於每月一號提供台灣人氣 **Blogger**
排行榜（又稱為「**百大部落客**」），分為[痞客邦排行榜](https://pixranking.events.pixnet.net/ranking/#blogpage)（站內）與[聯盟排行榜](https://pixranking.events.pixnet.net/personalrank)（自架站），除了供網友參考外，也提供給業界廠商挑選「**部落客**」[業配的依據](../Page/業配.md "wikilink")。

1.  [百大部落客名單](https://pixranking.events.pixnet.net/personalrank)

## 參見

  - [部落格](../Page/網誌.md "wikilink")
  - [痞客邦 PIXNET](../Page/痞客邦.md "wikilink")
  - [隨意窩 Xuite](../Page/隨意窩_Xuite日誌.md "wikilink")
  - [Blogger (service)](../Page/Blogger.md "wikilink")
  - [WordPress](../Page/WordPress.md "wikilink")
  - [自媒體](../Page/自媒体.md "wikilink")

[\*](../Page/分類:部落客.md "wikilink")

[ja:ブロガー](../Page/ja:ブロガー.md "wikilink")
[ko:블로거닷컴](../Page/ko:블로거닷컴.md "wikilink")
[nl:Blogger](../Page/nl:Blogger.md "wikilink")

[Category:網站](../Category/網站.md "wikilink")