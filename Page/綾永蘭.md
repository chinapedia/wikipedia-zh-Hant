**綾永蘭**（，）是一位[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")，出身於[福井縣](../Page/福井縣.md "wikilink")[福井市](../Page/福井市.md "wikilink")。曾以**奈月蘭**（）為名擔任[Visco的](../Page/Visco_\(遊戲公司\).md "wikilink")[PlayStation
2](../Page/PlayStation_2.md "wikilink")[遊戲軟體](../Page/遊戲軟體.md "wikilink")《[élan](../Page/élan.md "wikilink")》（）系列的[人物設定](../Page/人物設定.md "wikilink")。

在《[純情房東俏房客](../Page/純情房東俏房客.md "wikilink")》連載時擔任[赤松健的助手](../Page/赤松健.md "wikilink")，在《[我永遠的聖誕老人](../Page/我永遠的聖誕老人.md "wikilink")》[OVA擔任人物原案協力](../Page/OVA.md "wikilink")。正式以[漫畫家的身分出道後](../Page/漫畫家.md "wikilink")，曾為赤松健設計《[魔法老師](../Page/魔法老師.md "wikilink")》的幾名人物（[綾瀨夕映](../Page/綾瀨夕映.md "wikilink")、[椎名櫻子](../Page/椎名櫻子.md "wikilink")、[犬上小太郎](../Page/犬上小太郎.md "wikilink")、菲特·亞維路克斯、阿爾比雷歐·伊馬、近衛詠春（青年期）等）。夫婿是赤松健的主要助手[MAGI](../Page/MAGI.md "wikilink")（）。

## 作品

### 漫畫

  - 《[R.O.D](../Page/R.O.D.md "wikilink") -READ OR
    DREAM-》（[集英社](../Page/集英社.md "wikilink")《[Ultra
    Jump](../Page/Ultra_Jump.md "wikilink")》連載）
  - 《》（[講談社](../Page/講談社.md "wikilink")《》連載）

### 遊戲軟體

  - 《élan》（Visco發行）

## 外部連結

  - 綾永蘭個人部落格：[PIROSHIKIGUMI](http://members.jcom.home.ne.jp/0724236901/)
  - MAGI個人網站：[MAGI\`s HOMEPAGE](http://hwm5.gyao.ne.jp/magipon/)

[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:日本女性漫画家](../Category/日本女性漫画家.md "wikilink")
[Category:福井縣出身人物](../Category/福井縣出身人物.md "wikilink")