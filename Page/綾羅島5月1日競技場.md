[Arirang1.jpg](https://zh.wikipedia.org/wiki/File:Arirang1.jpg "fig:Arirang1.jpg")
[View_Pyongyang_11.JPG](https://zh.wikipedia.org/wiki/File:View_Pyongyang_11.JPG "fig:View_Pyongyang_11.JPG")

**綾羅島5月1日競技場**或**5月1日競技場**（）、**五一体育场**，是位於[朝鮮](../Page/朝鮮民主主義人民共和國.md "wikilink")[平壤的](../Page/平壤.md "wikilink")[體育場](../Page/體育場.md "wikilink")，啟用於1989年5月1日，是为了迎接在1989年7月1日开幕的[世界青少年和學生節](../Page/世界青年与学生联欢节.md "wikilink")，名稱來自於該體育場的所在地：[大同江中的](../Page/大同江.md "wikilink")[綾羅島](../Page/綾羅島.md "wikilink")、以及[國際勞動節的日期](../Page/國際勞動節.md "wikilink")：5月1日。座位總數達150,000席\[1\]\[2\]，是全世界[容納人數最多的體育場](../Page/體育場容納人數一覽表.md "wikilink")，其扇形頂部是整個建築的最大特色。草地面積達到22,500平方米（242,200平方[呎](../Page/呎.md "wikilink")），場區總面積更超過207,000平方米（2百20萬平方[呎](../Page/呎.md "wikilink")）。

與球場的體育賽事相比，慶祝[金日成生辰的太陽節](../Page/金日成.md "wikilink")、[阿里郎節表演活動](../Page/阿里郎節.md "wikilink")、以及[朝鮮建國日的活動更為有名](../Page/朝鮮建國日.md "wikilink")。在2002年5月，一場宏大而精巧的舞蹈在球場舉行，目的是慶祝已故建國領導人[金日成的誕辰](../Page/金日成.md "wikilink")，以及前任領導人[金正日的](../Page/金正日.md "wikilink")60周年大壽。活動包括了100,000名穿著奇裝異服的參加者，這一人數竟然比觀眾還多一倍。\[3\]

球場亦是金正日於2000年接待[美國國務卿](../Page/美國國務卿.md "wikilink")[馬德琳·奧爾布賴特的場所](../Page/馬德琳·奧爾布賴特.md "wikilink")。馬德琳·奧爾布賴特是[美國政府中造訪朝鮮的最高層級官員](../Page/美國政府.md "wikilink")。

球場入場人數最多的比賽是一場[職業摔角](../Page/職業摔角.md "wikilink")，於1995年4月29日舉行，總共有190,000人入場觀看。

## 參考資料

## 外部链接

  - [WorldStadiums.com
    entry](http://www.worldstadiums.com/stadium_pictures/asia/north_korea/pyongyang_may_day.shtml)

[Category:朝鮮體育場地](../Category/朝鮮體育場地.md "wikilink")
[Category:朝鮮足球場](../Category/朝鮮足球場.md "wikilink")
[Category:平壤市建築物](../Category/平壤市建築物.md "wikilink")
[Category:亚洲的世界之最](../Category/亚洲的世界之最.md "wikilink")
[Category:1989年完工體育場館](../Category/1989年完工體育場館.md "wikilink")

1.
2.  [www.fussballtempel.net](http://www.fussballtempel.net/stadienallgemein.html)
3.