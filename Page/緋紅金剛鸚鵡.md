**绯红金刚鹦鹉**，又名**五彩金剛鸚鵡**（學名**）屬於[鹦鹉科中的](../Page/鹦鹉科.md "wikilink")[金剛鸚鵡屬](../Page/金剛鸚鵡屬.md "wikilink")。分布范围包括[墨西哥南部地区到](../Page/墨西哥.md "wikilink")[巴拿马及](../Page/巴拿马.md "wikilink")[南美洲南部的](../Page/南美洲.md "wikilink")[热带](../Page/热带.md "wikilink")[森林到](../Page/森林.md "wikilink")[玻利维亚东部](../Page/玻利维亚.md "wikilink")。是金刚鹦鹉中分布范围最广的一种。

体长约85厘米，体重1000克。寿命约50年。绯红金刚鹦鹉从额到枕部、颈、背部和尾巴的羽毛都是绯红色。只有翅膀上覆盖着黄色羽毛，以及深蓝色的初、次级[飞羽](../Page/飞羽.md "wikilink")。[喙的上部为肉色](../Page/喙.md "wikilink")，下部铅灰色，[趾铅蓝色](../Page/趾.md "wikilink")。
绯红金刚鹦鹉与[绿翅金刚鹦鹉非常相似](../Page/绿翅金刚鹦鹉.md "wikilink")，最大的不同之处在于：绿翅金刚鹦鹉翅膀上红色与蓝色交界处是绿色，而绯红则为黄色。

受到[鸟类贸易的影响](../Page/鸟类贸易.md "wikilink")，绯红金刚鹦鹉已列入[华盛顿公约一级濒危绝种名单](../Page/华盛顿公约.md "wikilink")。

## 习性及保育

绯红金刚鹦鹉在野外时喜欢跟[蓝黄金刚鹦鹉及](../Page/蓝黄金刚鹦鹉.md "wikilink")[绿翅金刚鹦鹉作伴](../Page/绿翅金刚鹦鹉.md "wikilink")，性格活泼，较为吵闹及喜欢沐浴，需要较大的空间飞翔。[喙的力量强大](../Page/喙.md "wikilink")，需要关在金属笼子中。语言学习能力并不强。主要食物为[果仁](../Page/果仁.md "wikilink")，种子。

绯红金刚鹦鹉3岁便可以[繁殖](../Page/繁殖.md "wikilink")，[繁殖季节从每年的](../Page/繁殖季节.md "wikilink")11月开始到来年5月结束。一窝产卵1到2颗，[孵化期约](../Page/孵化期.md "wikilink")28天，幼鸟重量只有20多克，但满月时便可达560克。

## 媒体

[File:Guacamayas.ogg|绯红金刚鹦鹉与](File:Guacamayas.ogg%7C绯红金刚鹦鹉与)[军舰金刚鹦鹉](../Page/军舰金刚鹦鹉.md "wikilink")

## 外部链接

  - [Scarlet Macaw](http://www.camelot-macaws.com/Scarlet-Macaw.htm)

[AM](../Category/IUCN無危物種.md "wikilink")
[AM](../Category/鹦鹉科.md "wikilink")
[Category:能言鳥類](../Category/能言鳥類.md "wikilink")