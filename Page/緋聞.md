**緋聞**（），又稱**桃色新聞**或**粉紅色新聞**，是指與[愛情](../Page/愛情.md "wikilink")、[性相關的](../Page/性愛.md "wikilink")[消息](../Page/消息.md "wikilink")。「緋」字本義是「[紅色](../Page/紅色.md "wikilink")」，紅色在[華人地區本有喜慶的含意](../Page/華人.md "wikilink")，「緋聞」本身是指值得喜慶的事情，引申為男女情愛的消息，性質屬於[中性](../Page/中性.md "wikilink")，不一定是[醜聞](../Page/醜聞.md "wikilink")，但因為涉及敏感[關係](../Page/關係.md "wikilink")，所以常會對當事人有負面影響。

由於不少社會傳統上均對男女交往、戀愛等有一定的禁忌和限制，因此戀愛、曖昧關係常成為[人際關係中茶餘飯後的話題](../Page/人際關係.md "wikilink")，在任何社交圈子均可能有緋聞產生。如[中小學生在](../Page/中小學生.md "wikilink")[學校裡常會指某些男女](../Page/學校.md "wikilink")[同學互相傾慕](../Page/同學.md "wikilink")，而實際上當事人未必有這樣的想法，甚至彼此之間不熟稔甚至無甚接觸，這類緋聞往往令當事人[尷尬](../Page/尷尬.md "wikilink")，而談論者則從開別人[玩笑感受到樂趣](../Page/玩笑.md "wikilink")。

在[辦公室政治裡](../Page/辦公室政治.md "wikilink")，緋聞除了是茶餘飯後的[閒聊話題外](../Page/閒聊.md "wikilink")，還可以用作[抹黑或攻擊對手的](../Page/抹黑.md "wikilink")[工具](../Page/工具.md "wikilink")。由於有些人認為戀愛會影響[工作](../Page/工作.md "wikilink")，而[辦公室戀愛又可能造成工作上的](../Page/辦公室戀愛.md "wikilink")[利益衝突](../Page/利益衝突.md "wikilink")，因此緋聞當事人的[事業發展可能會受到影響](../Page/事業.md "wikilink")。

在現代[娛樂圈](../Page/娛樂圈.md "wikilink")，緋聞常被用作[宣傳工具](../Page/宣傳.md "wikilink")，為[流行文化中的現實](../Page/流行文化.md "wikilink")[藝人做配對](../Page/藝人.md "wikilink")，引發[觀眾立體的聯想](../Page/觀眾.md "wikilink")，目的為成功建立[公眾形象](../Page/公眾形象.md "wikilink")。緋聞分為正面也有負面兩種。例如某[電視劇熱播](../Page/電視劇.md "wikilink")，[單身的男女](../Page/單身.md "wikilink")[主角拍劇日久生情](../Page/主角.md "wikilink")，受到[視迷祝福](../Page/愛好者.md "wikilink")。但已婚或已有穩定伴侶的藝人與他人傳出緋聞，常會令[家庭觀眾反感](../Page/家庭觀眾.md "wikilink")，導致當事人被[公司](../Page/公司.md "wikilink")[冷處理等](../Page/冷處理.md "wikilink")，都是緋聞。緋聞未必是真實，也未必是純[虛構](../Page/虛構.md "wikilink")，必定要有娛樂性，否則[傳聞流傳不會擴大](../Page/傳聞.md "wikilink")，深遠。

[政治人物的緋聞也可能影響公眾對他們的觀感](../Page/政治人物.md "wikilink")，如果傳緋聞的對象背景涉及政治、經濟的利益關係，常會被人質疑有利益輸送。此外，已婚的政治人物與其他人傳出緋聞，可能會變成[性醜聞](../Page/性醜聞.md "wikilink")。

[公眾人物或會有](../Page/公眾人物.md "wikilink")「人言可畏」的壓力，緋聞甚至引發[自殺](../Page/自殺.md "wikilink")[悲劇](../Page/悲劇.md "wikilink")。

## 緋聞媒體

  - [八卦雜誌](../Page/八卦雜誌.md "wikilink")
  - [網路討論區](../Page/網路論壇.md "wikilink")
  - 耳語相傳（[:en:Speech
    communication](../Page/:en:Speech_communication.md "wikilink")）
  - [短訊](../Page/短訊.md "wikilink")

## 參见

  - [醜聞](../Page/醜聞.md "wikilink")
  - [外遇](../Page/外遇.md "wikilink")
  - [八卦新闻](../Page/八卦新闻.md "wikilink")

[Category:傳播學](../Category/傳播學.md "wikilink")
[Category:新聞](../Category/新聞.md "wikilink")
[Category:人際關係](../Category/人際關係.md "wikilink")