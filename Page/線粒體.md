\-{zh-tw:[Mitochondrion_(standalone_version)-zh-hant.svg](https://zh.wikipedia.org/wiki/File:Mitochondrion_\(standalone_version\)-zh-hant.svg "fig:Mitochondrion_(standalone_version)-zh-hant.svg");zh-hk:[Mitochondrion_(standalone_version)-zh-hant.svg](https://zh.wikipedia.org/wiki/File:Mitochondrion_\(standalone_version\)-zh-hant.svg "fig:Mitochondrion_(standalone_version)-zh-hant.svg");zh-hans:[Mitochondrion_(standalone_version)-zh-hans.svg](https://zh.wikipedia.org/wiki/File:Mitochondrion_\(standalone_version\)-zh-hans.svg "fig:Mitochondrion_(standalone_version)-zh-hans.svg")}-
[Mitochondrie.svg](https://zh.wikipedia.org/wiki/File:Mitochondrie.svg "fig:Mitochondrie.svg")

-{zh-cn:**线粒体**;zh-tw:**粒線體**;zh-hk:**線粒體**}-（mitochondrion）是一种存在于大多数[真核细胞中的由两层膜包被的](../Page/真核细胞.md "wikilink")[细胞器](../Page/细胞器.md "wikilink")，\[1\]直径在0.5到10微米左右。除了[溶组织内阿米巴](../Page/溶组织内阿米巴.md "wikilink")、[篮氏贾第鞭毛虫以及几种](../Page/篮氏贾第鞭毛虫.md "wikilink")[微孢子虫外](../Page/微孢子虫.md "wikilink")，大多数真核细胞或多或少都拥有[线粒体](../Page/线粒体.md "wikilink")，但它们各自拥有的线粒体在大小、数量及外观等方面上都有所不同。\[2\]这种细胞器拥有自身的[遗传物质和遗传体系](../Page/遗传物质.md "wikilink")，但因其[基因组大小有限](../Page/基因组.md "wikilink")，所以线粒体是一种[半自主细胞器](../Page/半自主胞器.md "wikilink")。线粒体是细胞内[氧化磷酸化和合成](../Page/氧化磷酸化.md "wikilink")[三磷酸腺苷](../Page/三磷酸腺苷.md "wikilink")（ATP）的主要场所，为细胞的活动提供了化学[能量](../Page/化学能.md "wikilink")，所以有“細胞的發電站”（the
powerhouse of the
cell）之称。\[3\]除了为细胞供能外，线粒体还参与诸如[细胞分化](../Page/细胞分化.md "wikilink")、[细胞信息传递和](../Page/细胞信息传递.md "wikilink")[细胞凋亡等过程](../Page/细胞凋亡.md "wikilink")，并拥有调控[细胞生长和](../Page/细胞生长.md "wikilink")[细胞周期的能力](../Page/细胞周期.md "wikilink")。\[4\]

[英文中的](../Page/英文.md "wikilink")“线粒体”（*mitochondrion*，[复数形式为](../Page/复数_\(语言学\).md "wikilink")“*mitochondria*”）一词是由[希腊语中的](../Page/希腊语.md "wikilink")“[线](../Page/线.md "wikilink")”（“*μίτος*”或“*mitos*”）和“[颗粒](../Page/颗粒.md "wikilink")”（“*χονδρίον*”或“*chondrion*”）组合而成的。在“线粒体”这一名称出现前后，“粒体”
“球状体”等众多名字曾先后或同时被使用。\[5\]\[6\]

## 概况

  - 大小

线粒体的直径一般为0.5-1.0[μm](../Page/微米.md "wikilink")，长1.5-3.0μm，在[光学显微镜下可见](../Page/光学显微镜.md "wikilink")。\[7\]在[动物细胞中](../Page/动物.md "wikilink")，线粒体大小受细胞代谢水平限制。\[8\]不同组织在不同条件下可能产生[体积异常膨大的线粒体](../Page/体积.md "wikilink")，称为“”（megamitochondria）：[胰脏外分泌细胞中可长达](../Page/胰脏.md "wikilink")10-20μm；[神经元胞体中的线粒体尺寸差异很大](../Page/神经元.md "wikilink")，有的也可能长达10μm；[人类](../Page/人类.md "wikilink")[成纤维细胞的线粒体则更长](../Page/成纤维细胞.md "wikilink")，可达40μm。\[9\]有研究表明在低[氧气](../Page/氧气.md "wikilink")[分压的](../Page/气压.md "wikilink")[环境中](../Page/环境.md "wikilink")，某些如[烟草的](../Page/烟草.md "wikilink")[植物的线粒体能可逆地变为巨线粒体](../Page/植物.md "wikilink")，长度可达80μm，并形成网络。\[10\]

  - 形状

线粒体一般呈短棒状或圆球状，但因生物种类和生理状态而异，还可呈环状、线状、哑铃状、分杈状、扁盘状或其它形状。[成型蛋白](../Page/成型蛋白.md "wikilink")（shape-forming
protein）介导线粒体以不同方式与周围的[细胞骨架接触或在线粒体的两层膜间形成不同的连接可能是线粒体在不同细胞中呈现出不同形态的原因](../Page/细胞骨架.md "wikilink")。\[11\]

  - 数量

不同[生物的不同](../Page/生物.md "wikilink")[组织中线粒体数量的差异是巨大的](../Page/组织.md "wikilink")。\[12\]有许多细胞拥有多达数千个的线粒体（如[肝脏细胞中有](../Page/肝脏.md "wikilink")1000-2000个线粒体），而一些细胞则只有一个线粒体（如[酵母菌细胞的大型分支线粒体](../Page/酵母菌.md "wikilink")）。大多数[哺乳动物的成熟](../Page/哺乳动物.md "wikilink")[红细胞不具有线粒体](../Page/红细胞.md "wikilink")。\[13\]一般来说，细胞中线粒体数量取决于该细胞的[代谢水平](../Page/代谢.md "wikilink")，代谢活动越旺盛的细胞线粒体越多。

  - 分布

线粒体分布方向与[微管一致](../Page/微管.md "wikilink")，\[14\]通常分布在细胞功能旺盛的区域：如在[肾脏细胞中靠近](../Page/肾脏.md "wikilink")[微血管](../Page/微血管.md "wikilink")，呈平行或栅状排列；在肠表皮细胞中呈两极分布，集中在顶端和基部；在[精子中分布在](../Page/精子.md "wikilink")[鞭毛中区](../Page/鞭毛.md "wikilink")。在[卵母细胞体外培养中](../Page/卵母细胞.md "wikilink")，随着细胞逐渐成熟，线粒体会由在细胞周边分布发展成均匀分布。\[15\]\[16\]线粒体在细胞质中能以微管为导轨、由[马达蛋白提供动力向功能旺盛的区域迁移](../Page/马达蛋白.md "wikilink")。

  - 组成

线粒体的化学组分主要包括[水](../Page/水.md "wikilink")、[蛋白质和](../Page/蛋白质.md "wikilink")[脂质](../Page/脂质.md "wikilink")，此外还含有少量的[辅酶等小分子及](../Page/辅酶.md "wikilink")[核酸](../Page/核酸.md "wikilink")。蛋白质占线粒体干重的65-70%。线粒体中的蛋白质既有可溶的也有不溶的。可溶的蛋白质主要是位于线粒体基质的[酶和膜的](../Page/酶.md "wikilink")[外周蛋白](../Page/外周蛋白.md "wikilink")；不溶的蛋白质构成膜的本体，其中一部分是[镶嵌蛋白](../Page/镶嵌蛋白.md "wikilink")，也有一些是酶。线粒体中脂类主要分布在两层膜中，占干重的20-30%。在线粒体中的磷脂占总脂质的3/4以上。同种生物不同组织线粒体膜中[磷脂的量相对稳定](../Page/磷脂.md "wikilink")。\[17\]含丰富的[心磷脂和较少的](../Page/心磷脂.md "wikilink")[胆固醇是线粒体在组成上与细胞其他膜结构的明显差别](../Page/胆固醇.md "wikilink")。

  - 结构

线粒体由外至内可划分为[线粒体外膜](../Page/线粒体外膜.md "wikilink")（OMM）、[线粒体膜间隙](../Page/线粒体膜间隙.md "wikilink")、[线粒体内膜](../Page/线粒体内膜.md "wikilink")（IMM）和[线粒体基质四个功能区](../Page/线粒体基质.md "wikilink")。处于线粒体外侧的膜彼此[平行](../Page/平行.md "wikilink")，都是典型的[单位膜](../Page/单位膜.md "wikilink")。其中，线粒体外膜较光滑，起细胞器界膜的作用；线粒体内膜则向内皱褶形成[线粒体嵴](../Page/线粒体嵴.md "wikilink")，负担更多的生化反应。这两层膜将线粒体分出两个区室，位于两层[线粒体膜之间的是线粒体膜间隙](../Page/线粒体膜.md "wikilink")，被线粒体内膜包裹的是线粒体基质。

## 发现及研究的历史

[Kolliker2.jpg](https://zh.wikipedia.org/wiki/File:Kolliker2.jpg "fig:Kolliker2.jpg")，瑞士科学家，线粒体的发现者。\]\]
线粒体的研究是从19世纪50年代末开始的。

1857年，[瑞士](../Page/瑞士.md "wikilink")[解剖学家及](../Page/解剖学.md "wikilink")[生理学家](../Page/生理学.md "wikilink")[阿尔伯特·冯·科立克在肌肉细胞中发现了颗粒状结构](../Page/阿尔伯特·冯·科立克.md "wikilink")。\[18\]另外的一些科学家在其他细胞中也发现了同样的结构，证实了科立克的发现。[德国](../Page/德国.md "wikilink")[病理学家及](../Page/病理学.md "wikilink")[组织学家](../Page/组织学.md "wikilink")[理查德·阿尔特曼将这些颗粒命名为](../Page/理查德·阿尔特曼.md "wikilink")“原生粒”（bioblast）并于1886年发明了一种鉴别这些颗粒的染色法。阿尔特曼猜测这些颗粒可能是共生于细胞内的独立生活的细菌。\[19\]

1898年，德国科学家[卡尔·本达因这些结构时而呈线状时而呈颗粒状](../Page/卡尔·本达.md "wikilink")\[20\]，所以用[希腊语中](../Page/希腊语.md "wikilink")“线”和“颗粒”对应的两个词——“*mitos*”和“*chondros*”——组成“*mitochondrion*”来为这种结构命名，这个名称被沿用至今。\[21\]一年后，[美国](../Page/美国.md "wikilink")[化学家](../Page/化学家.md "wikilink")[莱昂诺尔·米歇利斯开发出用具有](../Page/莱昂诺尔·米歇利斯.md "wikilink")[还原性的](../Page/还原性.md "wikilink")[健那绿染液为线粒体染色的方法](../Page/健那绿染液.md "wikilink")，并推断线粒体参与某些[氧化反应](../Page/氧化反应.md "wikilink")。\[22\]这一方法于1900年公布，并由美国[细胞学家](../Page/细胞学.md "wikilink")[埃德蒙·文森特·考德里推广](../Page/埃德蒙·文森特·考德里.md "wikilink")。\[23\]德国[生物化学家](../Page/生物化学.md "wikilink")[奥托·海因里希·沃伯格成功完成线粒体的粗提取且分离得到一些催化与氧有关的反应的](../Page/奥托·海因里希·沃伯格.md "wikilink")[呼吸酶](../Page/呼吸酶.md "wikilink")，并提出这些[酶能被](../Page/酶.md "wikilink")[氰化物](../Page/氰化物.md "wikilink")（如[氢氰酸](../Page/氢氰酸.md "wikilink")）抑制的猜想。\[24\]

[英国](../Page/英国.md "wikilink")[生物学家](../Page/生物学.md "wikilink")[大卫·基林在](../Page/大卫·基林.md "wikilink")1923年至1933年这十年间对线粒体内的[氧化还原链](../Page/氧化还原链.md "wikilink")（redox
chain）的物质基础进行探索，辨别出反应中的[电子载体](../Page/电子载体.md "wikilink")——[细胞色素](../Page/细胞色素.md "wikilink")。\[25\]

[沃伯格于](../Page/奥托·海因里希·瓦尔堡.md "wikilink")1931年因“发现呼吸酶的性质及作用方式”被授予[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。\[26\]

## 结构

\-{zh-cn:[Animal_mitochondrion_diagram_zh_ml.svg](https://zh.wikipedia.org/wiki/File:Animal_mitochondrion_diagram_zh_ml.svg "fig:Animal_mitochondrion_diagram_zh_ml.svg");
zh-tw:[Animal_mitochondrion_diagram_zh_tw.svg](https://zh.wikipedia.org/wiki/File:Animal_mitochondrion_diagram_zh_tw.svg "fig:Animal_mitochondrion_diagram_zh_tw.svg");
zh-hk:[Animal_mitochondrion_diagram_zh_hk.svg](https://zh.wikipedia.org/wiki/File:Animal_mitochondrion_diagram_zh_hk.svg "fig:Animal_mitochondrion_diagram_zh_hk.svg")}-

### 外膜

线粒体外膜是位于线粒体最外围的一层单位膜，厚度约为6-7nm。其中磷脂与蛋白质的[质量为](../Page/质量.md "wikilink")0.9:1，与真核细胞[细胞膜的同一](../Page/细胞膜.md "wikilink")[比例相近](../Page/比例.md "wikilink")。线粒体外膜中酶的含量相对较少，其标志酶为[单胺氧化酶](../Page/单胺氧化酶.md "wikilink")。线粒体外膜包含称为“[孔蛋白](../Page/孔蛋白.md "wikilink")”的[整合蛋白](../Page/整合蛋白.md "wikilink")，其内部通道宽約2-3nm，这使线粒体外膜对[分子量小于](../Page/分子量.md "wikilink")5000[Da的](../Page/原子量单位.md "wikilink")[分子完全通透](../Page/分子.md "wikilink")。分子量大于上述限制的分子则需拥有一段特定的信号序列以供识别并通过[外膜转运酶](../Page/外膜转运酶.md "wikilink")（translocase
of the outer
membrane，TOM）的[主动运输来进出线粒体](../Page/主动运输.md "wikilink")。\[27\]

线粒体外膜主要参与诸如[脂肪酸链延伸](../Page/脂肪酸.md "wikilink")、[肾上腺素](../Page/肾上腺素.md "wikilink")[氧化以及](../Page/氧化.md "wikilink")[色氨酸](../Page/色氨酸.md "wikilink")[生物降解等](../Page/生物降解.md "wikilink")[生化反应](../Page/生化反应.md "wikilink")，它也能同时对那些将在[线粒体基质中进行彻底氧化的](../Page/线粒体基质.md "wikilink")[物质先行初步分解](../Page/物质.md "wikilink")。[细胞凋亡过程中](../Page/细胞凋亡.md "wikilink")，线粒体外膜对多种存在于线粒体膜间隙中的蛋白的通透性增加，\[28\]使致死性蛋白进入细胞质基质，促进了细胞凋亡。\[29\]高分辨三维X射线摄影可见内质网及线粒体之间的有20%膜是紧密接触的，\[30\]在这些接触位点上线粒体外膜与[内质网膜通过某些蛋白质相连](../Page/内质网膜.md "wikilink")，形成称为“[线粒体结合内质网膜](../Page/线粒体结合内质网膜.md "wikilink")”（mitochondria-associated
ER-membrane，MAM）的结构。该结构在[脂质的相互交换和线粒体与内质网间的](../Page/脂质.md "wikilink")[钙离子信号传导等过程中都有重要作用](../Page/钙离子信号传导.md "wikilink")。\[31\]

### 膜间隙

线粒体膜间隙是线粒体外膜与线粒体内膜之间的空隙，宽约6-8nm，其中充满无定形[液体](../Page/液体.md "wikilink")。由于线粒体外膜含有孔蛋白，通透性较高，而线粒体内膜通透性较低，所以线粒体膜间隙内容物的组成与细胞质基质十分接近，含有众多生化反应[底物](../Page/底物.md "wikilink")、可溶性的酶和[辅助因子等](../Page/辅助因子.md "wikilink")。线粒体膜间隙中还含有比细胞质基质中[浓度更高的](../Page/浓度.md "wikilink")[腺苷酸激酶](../Page/腺苷酸激酶.md "wikilink")、[单磷酸激酶和](../Page/单磷酸激酶.md "wikilink")[二磷酸激酶等](../Page/二磷酸激酶.md "wikilink")[激酶](../Page/激酶.md "wikilink")，其中腺苷酸激酶是线粒体膜间隙的标志酶。线粒体膜间隙中存在的蛋白质可统称为“[线粒体膜间隙蛋白质](../Page/线粒体膜间隙蛋白质.md "wikilink")”，这些蛋白质全部在细胞质基质中合成。\[32\]

### 内膜

线粒体内膜是位于线粒体外膜内侧、包裹着线粒体基质的单位膜。线粒体内膜中蛋白质与磷脂的质量比约为0.7:0.3，并含有大量的心磷脂（心磷脂常为[细菌细胞膜的成分](../Page/细菌.md "wikilink")\[33\]）。线粒体内膜的某些部分会向线粒体基质折叠形成线粒体嵴。线粒体内膜的标志酶是[细胞色素氧化酶](../Page/细胞色素氧化酶.md "wikilink")。

线粒体内膜含有[電子傳遞鏈](../Page/電子傳遞鏈.md "wikilink")（ETC）以及比外膜更多的蛋白质（超过151种，约占线粒体所含所有蛋白质的五分之一），所以承担着更复杂的生化反应。存在于线粒体内膜中的几类蛋白质主要负责以下生理过程：特异性载体运输[磷酸](../Page/磷酸.md "wikilink")、[谷氨酸](../Page/谷氨酸.md "wikilink")、[鸟氨酸](../Page/鸟氨酸.md "wikilink")、各种[离子及](../Page/离子.md "wikilink")[核苷酸等](../Page/核苷酸.md "wikilink")[代谢](../Page/代谢.md "wikilink")[产物和中间产物](../Page/产物_\(化学\).md "wikilink")；[内膜转运酶](../Page/内膜转运酶.md "wikilink")（translocase
of the inner
membrane，TIM）运输蛋白质；参与[氧化磷酸化中的](../Page/氧化磷酸化.md "wikilink")[氧化还原反应](../Page/氧化还原反应.md "wikilink")；参与ATP的合成；控制线粒体的分裂与融合。\[34\]

### 嵴

线粒体嵴简称“嵴”，是线粒体内膜向线粒体基质折褶形成的一种结构。线粒体嵴的形成增大线粒体内膜的[表面积](../Page/表面积.md "wikilink")。在不同种类的细胞中，线粒体嵴的数目、形态和排列方式可能有较大差别。线粒体嵴主要有几种排列方式，分别称为“片状嵴”（lamellar
cristae）、“管状嵴”（tubular cristae）和“泡状嵴”（vesicular
cristae）。\[35\]片状排列的线粒体嵴主要出现在[高等动物](../Page/高等动物.md "wikilink")[细胞的线粒体中](../Page/细胞.md "wikilink")，这些片状嵴多数[垂直于线粒体](../Page/垂直.md "wikilink")[长轴](../Page/长轴.md "wikilink")；管状排列的线粒体嵴则主要出现在[原生动物和](../Page/原生动物.md "wikilink")[植物细胞的线粒体中](../Page/植物.md "wikilink")。有研究发现，[睾丸间质细胞中既存在层状嵴也存在管状嵴](../Page/睾丸间质.md "wikilink")。\[36\]线粒体嵴上有许多有柄小球体，即[线粒体基粒](../Page/线粒体基粒.md "wikilink")，基粒中含有[ATP合酶](../Page/ATP合酶.md "wikilink")，能利用[呼吸链产生的](../Page/呼吸链.md "wikilink")[能量合成三磷酸腺苷](../Page/能量.md "wikilink")。所以需要较多能量的细胞，线粒体嵴的数目一般也较多。但某些形态特殊的线粒体嵴由于没有ATP合酶，所以不能合成ATP。\[37\]

### 基质

线粒体基质是线粒体中由线粒体内膜包裹的内部空间，其中含有参与[三羧酸循环](../Page/三羧酸循环.md "wikilink")、脂肪酸氧化、[氨基酸](../Page/氨基酸.md "wikilink")[降解等生化反应的酶等众多蛋白质](../Page/降解.md "wikilink")，所以较细胞质基质黏稠。\[38\]\[39\][苹果酸脱氢酶是线粒体基质的标志酶](../Page/苹果酸脱氢酶.md "wikilink")。线粒体基质中一般还含有线粒体自身的[DNA](../Page/DNA.md "wikilink")（即[线粒体DNA](../Page/线粒体DNA.md "wikilink")）、[RNA和](../Page/RNA.md "wikilink")[核糖体](../Page/核糖体.md "wikilink")（即[线粒体核糖体](../Page/线粒体核糖体.md "wikilink")）。

线粒体DNA是线粒体中的[遗传物质](../Page/遗传物质.md "wikilink")，呈双链环状，并可与多种蛋白质结合成高度紧密的[线粒体拟核](../Page/线粒体拟核.md "wikilink")。一个线粒体中可有一个或数个线粒体DNA分子。线粒体RNA是线粒体DNA的[表达产物](../Page/基因表达.md "wikilink")，[RNA编辑也普遍存在于线粒体RNA中](../Page/RNA编辑.md "wikilink")，是线粒体产生功能蛋白所必不可少的过程。\[40\]线粒体核糖体是存在于线粒体基质内的一种[核糖体](../Page/核糖体.md "wikilink")，负责完成线粒体内进行的[翻译工作](../Page/翻译_\(遗传学\).md "wikilink")。线粒体核糖体的[沉降系数介干](../Page/沉降系数.md "wikilink")55[S](../Page/斯维德伯格.md "wikilink")-56S之间。一般的线粒体核糖体由[28S核糖体亚基](../Page/28S核糖体亚基.md "wikilink")（[小亚基](../Page/核糖体小亚基.md "wikilink")）和[39S核糖体亚基](../Page/39S核糖体亚基.md "wikilink")（[大亚基](../Page/核糖体大亚基.md "wikilink")）组成。\[41\]在这类核糖体中，rRNA约占25%，[核糖体蛋白质约占](../Page/核糖体蛋白质.md "wikilink")75%。线粒体核糖体是已发现的蛋白质含量最高的一类核糖体。线粒体基质中存在的蛋白质统称为“[线粒体基质蛋白质](../Page/线粒体基质蛋白质.md "wikilink")”，包括[DNA聚合酶](../Page/DNA聚合酶.md "wikilink")、[RNA聚合酶](../Page/RNA聚合酶.md "wikilink")、[柠檬酸合成酶以及](../Page/柠檬酸合成酶.md "wikilink")[三羧酸循环酶系中的酶类](../Page/三羧酸循环酶系.md "wikilink")。大部分线粒体基质蛋白是由核基因编码的。线粒体基质蛋白不一定只在线粒体基质中[表达](../Page/基因表达.md "wikilink")，它们也可以在线粒体外表达。\[42\]

## 功能

### 能量转化

线粒体是真核生物进行氧化代谢的部位，是[糖类](../Page/糖类.md "wikilink")、[脂肪和](../Page/脂肪.md "wikilink")[氨基酸最终氧化释放能量的场所](../Page/氨基酸.md "wikilink")。线粒体负责的最终氧化的共同途径是[三羧酸循环与](../Page/三羧酸循环.md "wikilink")[氧化磷酸化](../Page/氧化磷酸化.md "wikilink")，分别对应[有氧呼吸的第二](../Page/有氧呼吸.md "wikilink")、三阶段。\[43\]细胞质基质中完成的糖酵解和在线粒体基质中完成的三羧酸循环在会产[还原型烟酰胺腺嘌呤二核苷酸](../Page/还原型烟酰胺腺嘌呤二核苷酸.md "wikilink")（reduced
nicotinarnide adenine
dinucleotide，NADH）和[还原型黄素腺嘌呤二核苷酸](../Page/还原型黄素腺嘌呤二核苷酸.md "wikilink")（reduced
flavin adenosine
dinucleotide，FADH<sub>2</sub>）等高能分子，而氧化磷酸化这一步骤的作用则是利用这些物质还原[氧气释放能量合成ATP](../Page/氧气.md "wikilink")。在有氧呼吸过程中，1分子[葡萄糖经过糖酵解](../Page/葡萄糖.md "wikilink")、三羧酸循环和氧化磷酸化将能量释放后，可产生30-32分子ATP（考虑到将NADH运入线粒体可能需消耗2分子ATP）。\[44\]如果细胞所在环境缺氧，则会转而进行无氧呼吸。此时，糖酵解产生的丙酮酸便不再进入线粒体内的三羧酸循环，而是继续在细胞质基质中反应（被NADH还原成[乙醇或](../Page/乙醇.md "wikilink")[乳酸等](../Page/乳酸.md "wikilink")[发酵产物](../Page/发酵.md "wikilink")），但不产生ATP。所以在无氧呼吸过程中，1分子葡萄糖只能在第一阶段产生2分子ATP。

#### 三羧酸循環

糖酵解中生成的每分子[丙酮酸会被](../Page/丙酮酸.md "wikilink")[主动运输转运穿过线粒体膜](../Page/主动运输.md "wikilink")。进入线粒体基质后，丙酮酸会被氧化，并与[辅酶A结合生成CO](../Page/辅酶A.md "wikilink")<sub>2</sub>、[还原型辅酶Ⅰ和](../Page/还原型辅酶Ⅰ.md "wikilink")[乙酰辅酶A](../Page/乙酰辅酶A.md "wikilink")。乙酰辅酶A是三羧酸循环（也称为「柠檬酸循环」或「Krebs循环」）的初级底物。参与该循环的酶除位于线粒体内膜的[琥珀酸脱氢酶外都游离于线粒体基质中](../Page/琥珀酸脱氢酶.md "wikilink")。\[45\]在三羧酸循环中，每分子乙酰辅酶A被氧化的同时会产生起始电子传递链的还原型辅因子（包括3分子NADH和1分子FADH<sub>2</sub>）以及1分子[三磷酸鸟苷](../Page/三磷酸鸟苷.md "wikilink")（GTP）。

#### 氧化磷酸化

\-{zh-cn:[Electron_transport_chain_zh_ml.svg](https://zh.wikipedia.org/wiki/File:Electron_transport_chain_zh_ml.svg "fig:Electron_transport_chain_zh_ml.svg");
zh-tw:[Electron_transport_chain_zh_tw.svg](https://zh.wikipedia.org/wiki/File:Electron_transport_chain_zh_tw.svg "fig:Electron_transport_chain_zh_tw.svg");
zh-hk:[Electron_transport_chain_zh_hk.svg](https://zh.wikipedia.org/wiki/File:Electron_transport_chain_zh_hk.svg "fig:Electron_transport_chain_zh_hk.svg")}-
NADH和FADH<sub>2</sub>等具有还原性的分子（在细胞质基质中的还原当量可从由[逆向转运蛋白构成的](../Page/逆向转运蛋白.md "wikilink")[苹果酸-天冬氨酸穿梭系统或通过](../Page/苹果酸-天冬氨酸穿梭.md "wikilink")[磷酸甘油穿梭作用进入电子传递链](../Page/磷酸甘油穿梭作用.md "wikilink")）在电子传递链里面经过几步反应最终将氧气还原并释放能量，其中一部分能量用于生成ATP，其余则作为[热能散失](../Page/热能.md "wikilink")。在线粒体内膜上的酶复合物（[NADH-泛醌还原酶](../Page/NADH-泛醌还原酶.md "wikilink")、[泛醌-细胞色素c还原酶](../Page/泛醌-细胞色素c还原酶.md "wikilink")、[细胞色素c氧化酶](../Page/细胞色素c氧化酶.md "wikilink")）利用过程中释放的能量将质子逆浓度梯度泵入线粒体膜间隙。虽然这一过程是高效的，但仍有少量电子会过早地还原氧气，形成[超氧化物等](../Page/超氧化物.md "wikilink")[活性氧](../Page/活性氧.md "wikilink")（ROS），这些物质能引起[氧化应激反应使线粒体性能发生衰退](../Page/氧化应激反应.md "wikilink")。\[46\]

当质子被泵入线粒体膜间隙后，线粒体内膜两侧便建立起了电化学梯度，质子就会有顺浓度梯度扩散的趋势。质子唯一的扩散通道是[ATP合酶](../Page/ATP合酶.md "wikilink")（呼吸链复合物V）。当质子通过复合物从膜间隙回到线粒体基质时，电势能被ATP合酶用于将ADP和磷酸合成ATP。这个过程被称为“[化学渗透](../Page/化学渗透.md "wikilink")”，是一种[协助扩散](../Page/协助扩散.md "wikilink")。彼得·米切尔就因为提出了这一假说而获得了1978年[诺贝尔奖](../Page/诺贝尔奖.md "wikilink")。1997年诺贝尔奖获得者[保罗·博耶和](../Page/保罗·博耶.md "wikilink")[約翰·沃克阐明了ATP合酶的机制](../Page/約翰·沃克.md "wikilink")。

### 储存钙离子

[Chondrocyte-_calcium_stain.jpg](https://zh.wikipedia.org/wiki/File:Chondrocyte-_calcium_stain.jpg "fig:Chondrocyte-_calcium_stain.jpg")中线粒体（字母M附近的黑斑）经钙离子染料染色后得到的电镜照片。\]\]
线粒体可以储存钙离子，可以和内质网、[细胞外基质等结构协同作用](../Page/细胞外基质.md "wikilink")，\[47\]从而控制细胞中的钙离子浓度的动态平衡。\[48\]线粒体迅速吸收钙离子的能力使其成为细胞中钙离子的缓冲区。\[49\]在线粒体内膜膜电位的驱动下，钙离子可由存在于线粒体内膜中的[单向运送体输送进入线粒体基质](../Page/单向运送体.md "wikilink")；\[50\]排出线粒体基质时则需要[钠-钙交换蛋白的辅助或通过](../Page/钠-钙交换蛋白.md "wikilink")[钙诱导钙释放](../Page/钙诱导钙释放.md "wikilink")（calcium-induced-calcium-release，CICR）机制。\[51\]在钙离子释放时会引起伴随着较大膜电位变化的“钙波”（calcium
wave），能激活某些[第二信使系统蛋白](../Page/第二信使系统.md "wikilink")，协调诸如[突触中](../Page/突触.md "wikilink")[神经递质的释放及](../Page/神经递质.md "wikilink")[内分泌细胞中](../Page/内分泌细胞.md "wikilink")[激素的分泌](../Page/激素.md "wikilink")。线粒体也参与细胞凋亡时的[钙离子信号转导](../Page/钙离子信号转导.md "wikilink")。\[52\]

### 其他功能

除了合成ATP为细胞提供能量等主要功能外，线粒体还承担了许多其他生理功能。

  - 调节[膜电位并控制](../Page/膜电位.md "wikilink")[细胞程序性死亡](../Page/细胞程序性死亡.md "wikilink")：\[53\]当线粒体内膜与外膜接触位点处生成了由[己糖激酶](../Page/己糖激酶.md "wikilink")（细胞质基质蛋白）、[外周苯并二氮受体和](../Page/外周苯并二氮受体.md "wikilink")[电压依赖阴离子通道](../Page/电压依赖阴离子通道.md "wikilink")（线粒体外膜蛋白）、[肌酸激酶](../Page/肌酸激酶.md "wikilink")（线粒体膜间隙蛋白）、[ADP-ATP载体](../Page/ADP-ATP载体.md "wikilink")（线粒体内膜蛋白）和[亲环蛋白D](../Page/亲环蛋白D.md "wikilink")（线粒体基质蛋白）等多种蛋白质组成的[通透性转变孔道](../Page/通透性转变孔道.md "wikilink")（PT孔道）后，会使线粒体内膜通透性提高，引起线粒体跨膜电位的耗散，从而导致细胞凋亡。\[54\]线粒体膜通透性增加也能使[诱导凋亡因子](../Page/诱导凋亡因子.md "wikilink")（AIF）等分子释放进入细胞质基质，破坏细胞结构。\[55\]

<!-- end list -->

  - 细胞增殖与细胞代谢的调控；
  - 合成胆固醇及某些[血红素](../Page/血红素.md "wikilink")\[56\]。

线粒体的某些功能只有在特定的组织细胞中才能展现。例如，只有肝脏细胞中的线粒体才具有对[氨气](../Page/氨气.md "wikilink")（蛋白质代谢过程中产生的废物）造成的毒害[解毒的功能](../Page/解毒.md "wikilink")。\[57\]

## 起源假说

对于线粒体的起源有两种假说，分别为[内共生假说与](../Page/内共生假说.md "wikilink")[非内共生假说](../Page/非内共生假说.md "wikilink")：

### 内共生假说

该假说认为线粒体起源于被另一个细胞吞噬的线粒体祖先——[原线粒体](../Page/原线粒体.md "wikilink")——一种能进行三羧酸循环和电子传递的[革兰氏阴性菌](../Page/革兰氏阴性菌.md "wikilink")。这种[好氧细菌是变形菌门下的一个分支](../Page/好氧细菌.md "wikilink")，与[立克次氏体有密切关系](../Page/立克次氏体.md "wikilink")。原线粒体被吞噬后，并没有被[消化](../Page/消化.md "wikilink")，而是与[宿主细胞形成了](../Page/宿主.md "wikilink")[共生关系](../Page/互利共生.md "wikilink")——[寄主可以从](../Page/寄主.md "wikilink")[宿主处获得更多](../Page/宿主.md "wikilink")[营养](../Page/营养.md "wikilink")，而宿主则可使用寄主产生的能量——这种关系增加了细胞的竞争力，使其可以适应更多的生存环境。在长期对寄主和宿主都有利的[互利共生中](../Page/互利共生.md "wikilink")，原线粒体逐渐演变形成了线粒体，使宿主细胞中进行的糖酵解和原线粒体中进行的三羧酸循环和氧化磷酸化成功耦合。\[58\]有研究认为，这种共生关系大约发生在17亿年以前，\[59\]，与进化趋异产生真核生物和古细菌的时期几乎相同。\[60\]但线粒体与真核生物细胞核出现的先后关系仍存在争议。\[61\]

现已发现支持内共生学说的证据包括：

1.  线粒体有內膜，拥有自己的DNA，其形状与细菌的[环状DNA类似](../Page/环状DNA.md "wikilink")；
2.  线粒体的DNA上编码了在线粒体中表达的特定蛋白质；
3.  线粒体的遗传密码与变形菌门细菌的遗传密码更为相似\[62\]；
4.  线粒体核糖体不论在大小还是在结构上都与细菌[70S核糖体较为相似](../Page/70S核糖体.md "wikilink")，而与真核细胞的[80S核糖体差异较大](../Page/80S核糖体.md "wikilink")。\[63\]\<\!--

\--\>

### 非内共生假说

非内共生假说又称为“细胞分化学说”，认为线粒体的发生是由细胞膜或内质网膜等[生物膜系统中的膜结构演变而来的](../Page/生物膜系统.md "wikilink")。非内共生学说有几种模型，主流的模型认为在细胞进化的最初阶段，[原核细胞基因组复制后并不伴有典型的](../Page/原核细胞.md "wikilink")[无丝分裂](../Page/无丝分裂.md "wikilink")，而是[拟核附近的细胞膜内陷形成双层膜](../Page/拟核.md "wikilink")，将其中一个基因组包围、隔离，进而发生细胞分裂。未分裂出来的子细胞则缓慢演化为细胞核、线粒体和叶绿体等高度特化的[细胞结构](../Page/细胞结构.md "wikilink")。

## 遗传学

### 基因组

线粒体的基因组中基因的数量很少，规模远小于细菌基因组。但内共生学说认为线粒体源于被吞噬的细菌，那么两者基因组规模应该较为相似。为了解释这一现象，有猜想认为原线粒体的基因除了丢失了一些外，大部分转移到了宿主细胞的细胞核中，\[64\]所以核基因编码了在超过98%的线粒体表达内的蛋白质。\[65\]某些线粒体中不含DNA的生物（如[隐孢子虫等](../Page/隐孢子虫.md "wikilink")）的mtDNA可能已完全丢失或整合入[核DNA中](../Page/核DNA.md "wikilink")。\[66\][线粒体DNA](../Page/线粒体DNA.md "wikilink")（mtDNA）在线粒体中有2-10个备份，\[67\]呈双链环状（但也有呈线状的特例存在\[68\]）。mtDNA长度一般为几万至数十万[碱基对](../Page/碱基对.md "wikilink")，[人类mtDNA的长度为](../Page/人类.md "wikilink")16,569[bp](../Page/碱基对.md "wikilink")，\[69\]拥有有37个[基因](../Page/基因.md "wikilink")，编码了两种[rRNA](../Page/rRNA.md "wikilink")（[12S
rRNA和](../Page/线粒体12S_rRNA.md "wikilink")[16S
rRNA](../Page/线粒体16S_rRNA.md "wikilink")）、22种[tRNA](../Page/tRNA.md "wikilink")（同样转运20种[标准氨基酸](../Page/标准氨基酸.md "wikilink")，只是[亮氨酸和](../Page/亮氨酸.md "wikilink")[丝氨酸都有两种对应的tRNA](../Page/丝氨酸.md "wikilink")）以及13种[多肽](../Page/多肽.md "wikilink")（呼吸链复合物Ⅰ、Ⅲ、Ⅳ、Ⅴ的[亚基](../Page/蛋白质亚基.md "wikilink")）。\[70\]mtDNA的长度和线粒体基因组的大小因物种而异，表一列出了几种[模式生物mtDNA的长度](../Page/模式生物.md "wikilink")：

| 生物                                     | [学名](../Page/双名法.md "wikilink") | mtDNA长度（bp）               |
| -------------------------------------- | ------------------------------- | ------------------------- |
| [芽殖酵母](../Page/芽殖酵母.md "wikilink")     | *Saccharomyces cerevisiae*      | 85779<ref>{{cite journal  |
| [裂殖酵母](../Page/裂殖酵母.md "wikilink")     | *Schizosaccharomyces pombe*     | 19431<ref>{{cite journal  |
| [拟南芥](../Page/拟南芥.md "wikilink")       | *Arabidopsis thaliana*          | 366924<ref>{{cite journal |
| [水稻](../Page/水稻.md "wikilink")         | *Oryza sativa*                  | 490520<ref>{{cite journal |
| [秀丽隐杆线虫](../Page/秀丽隐杆线虫.md "wikilink") | *Caenorhabditis elegans*        | 13794<ref>{{cite journal  |
| [黑腹果蝇](../Page/黑腹果蝇.md "wikilink")     | *Drosophila melanogaster*       | 19517<ref>{{cite journal  |
| [非洲爪蟾](../Page/非洲爪蟾.md "wikilink")     | *Xenopus laevis*                | 17553<ref>{{cite journal  |
| [小鼠](../Page/小鼠.md "wikilink")         | *Mus musculus*                  | 16300<ref>{{cite journal  |

表一：几种模式生物mtDNA的长度

mtDNA利用率极高，线粒体基因组各基因之间排列十分紧凑，部分区域还可能出现重叠（即前一个基因的最后一段碱基与下一个基因的第一段碱基相衔接）。人类mtDNA中[基因间隔区总共只有](../Page/基因间隔区.md "wikilink")87bp，占mtDNA总长的0.5%。\[71\]mtDNA的两条[DNA单链均有编码功能](../Page/DNA单链.md "wikilink")，其中重链编码两个rRNA、12个mRNA和14个tRNA；轻链编码一个mRNA和8个tRNA。\[72\]mtDNA一般没有[内含子](../Page/内含子.md "wikilink")（如人类的mtDNA等），\[73\]但也已发现某些[真核生物的mtDNA拥有内含子](../Page/真核生物.md "wikilink")，\[74\]这些生物包括：[盘基网柄菌](../Page/盘基网柄菌.md "wikilink")\[75\]等[原生生物](../Page/原生生物.md "wikilink")\[76\]和酵母菌（其\<span
title="细胞色素氧化酶c亚基Ⅰ"{{\#ifeq:|no| | style="border-bottom:1px dotted"
}}\>OXi3</span>基因有9个内含子）\[77\]。这些mtDNA中的内含子在基因转录产物的加工和翻译中可能有一定功能。\[78\]

线粒体基因组通常都是存在于同一个mtDNA分子中，但少数生物的线粒体基因组却分别储存在多个不同的mtDNA中。例如，人虱的线粒体基因组就分开储藏于18个长约3-4kb的微型环状DNA中，每个DNA分子只分配到了1-3个基因。\[79\]这些微型环状DNA之间也存在着同源或非同源的[基因重组现象](../Page/基因重组.md "wikilink")，但成因未知。\[80\]

### 遗传密码

线粒體中拥有一套独特的[遗传系统](../Page/遗传系统.md "wikilink")。在进行[人类線粒體遗传学研究时](../Page/人类線粒體遗传学.md "wikilink")，人们确认線粒體的[遗传密码与](../Page/遗传密码.md "wikilink")[通用遗传密码也有些许差异](../Page/通用遗传密码.md "wikilink")。\[81\]自从上述发现证明并不只存在单独的一种遗传密码之后，许多有轻微不同的遗传密码都陆续被发现。\[82\]在線粒體的遗传密码中最常见的差异是：AUA由[異白胺酸变为](../Page/異白胺酸.md "wikilink")[甲硫氨酸的](../Page/甲硫氨酸.md "wikilink")[密码子](../Page/密码子.md "wikilink")、UGA由终止密码子变为[色氨酸的密码子](../Page/色氨酸.md "wikilink")、AGA和AGG由[精氨酸的密码子变为终止密码子](../Page/精氨酸.md "wikilink")（[植物等生物的线粒体遗传密码另有差异](../Page/植物.md "wikilink")，参见表二）。\[83\]此外，也有某些特例是只涉及终止密码子的，在[山羊支原体线粒体遗传密码的UGA由终止密码子变为色氨酸的密码子](../Page/山羊支原体.md "wikilink")，而且使用频率比UGG更高；\[84\][四膜虫線粒體遗传密码里只有UGA一种终止密码子](../Page/四膜虫.md "wikilink")，其UAA和UAG由终止密码子变为[谷氨酰胺的密码子](../Page/谷氨酰胺.md "wikilink")；而[游仆虫線粒體遗传密码里则只有UAA和UAG两种终止密码子](../Page/游仆虫.md "wikilink")，其UGA由终止密码子变为[半胱氨酸的密码子](../Page/半胱氨酸.md "wikilink")。\[85\]通过線粒體遗传密码和通用遗传密码的对比，可以推导出遗传密码演化过程的可能模式。\[86\]

|                                |                                |                                      |
| :----------------------------: | :----------------------------: | :----------------------------------: |
|              密码子               |              通用密码              |               線粒體遗传密码                |
| [真菌](../Page/真菌.md "wikilink") | [植物](../Page/植物.md "wikilink") | [无脊椎动物](../Page/无脊椎动物.md "wikilink") |
|              UGA               |             终止密码子              |                 色氨酸                  |
|              AUA               |              异亮氨酸              |                 甲硫氨酸                 |
|              CUA               |              亮氨酸               |                 苏氨酸                  |
|            AGA、AGG             |              精氨酸               |                 精氨酸                  |

表二：線粒體遗传密码与通用遗传密码的差异

### 分裂与融合

线粒体的融合是与分裂协同进行的，过程高度保守，需要在多种蛋白质的精确调控下完成。\[87\]两者一般保持[动态平衡](../Page/动态平衡.md "wikilink")，这种平衡对维持线粒体正常的形态、分布和功能十分重要。线粒体融合与分裂间的失衡可产生巨型线粒体，这种过大的线粒体常见于病变的肝细胞、恶性[营养不良患者的胰脏细胞和](../Page/营养不良.md "wikilink")[白血病患者](../Page/白血病.md "wikilink")[骨髓的](../Page/骨髓.md "wikilink")[巨噬细胞中](../Page/巨噬细胞.md "wikilink")。\[88\]分裂异常会导致线粒体破碎，而融合异常则会导致线粒体形态延长，两者都会影响线粒体的功能。\[89\]分裂与融合活动异常的线粒体膜电位通常会降低，并最终经线粒体[自噬作用清除](../Page/自噬作用.md "wikilink")。

线粒体的分裂在真核细胞内经常发生。为了保证在细胞发生分裂后每个[子细胞都能继承](../Page/子细胞.md "wikilink")[母细胞的线粒体](../Page/母细胞.md "wikilink")，母细胞中的线粒体在一个[细胞周期需要至少复制一次](../Page/细胞周期.md "wikilink")。即使是在不再分裂的细胞内，线粒体为了填补已老化的线粒体造成的空缺也需要进行分裂。\[90\]的线粒体以与细菌的[无丝分裂类似的方式进行增殖](../Page/无丝分裂.md "wikilink")，可细分为三种模式：\[91\]

1.  间壁分离（见于部分动物和植物线粒体）：线粒体内部首先由内膜形成隔，随后外膜的一部分内陷，插入到隔的双层膜之间，将线粒体一分为二。
2.  收缩分离（见于[蕨类植物和酵母菌线粒体](../Page/蕨类植物.md "wikilink")）：线粒体中部先缢缩同时向两端不断拉长然后一分为二。
3.  出芽分离（见于[藓类植物和酵母菌线粒体](../Page/藓类植物.md "wikilink")）：线粒体上先出现小芽，小芽脱落后成长、发育为成熟线粒体。

线粒体的融合也是细胞中的基本事件，对线粒体正常功能的发挥具有非常重要的作用。人类细胞需要通过线粒体融合的互补作用来抵抗[衰老](../Page/衰老.md "wikilink")；酵母细胞线粒体融合发生障碍会引起呼吸链缺陷。\[92\]线粒体间的融合需在一种分子量约为800kDa的蛋白质[复合物](../Page/复合物.md "wikilink")——“[融合装置](../Page/融合装置.md "wikilink")”（fusion
machinery）的介导下进行，\[93\]该过程可大致分为四个步骤：锚定、外膜融合、内膜融合以及基质内含物融合。\[94\]

### 群体遺傳學

因为mtDNA几乎不发生基因重组，所以[遗传学家长期将其作为研究群体遗传学与](../Page/遗传学家.md "wikilink")[进化生物学的信息来源](../Page/进化生物学.md "wikilink")。\[95\]所有mtDNA是以单一单元（[单体型](../Page/单体型.md "wikilink")）进行遗传的（而不像[细胞核中的DNA储存在多个](../Page/细胞核.md "wikilink")[染色体中](../Page/染色体.md "wikilink")），它们在[亲本与](../Page/亲本.md "wikilink")[子代之间的传递关系并不复杂](../Page/子代.md "wikilink")，因此不同个体间mtDNA的联系便可以利用[系统发生树来表现](../Page/系统发生树.md "wikilink")。\[96\]而从这些系统发生树的形态中人们可以得知[种群的进化史](../Page/种群.md "wikilink")。[人类进化遗传学中运用](../Page/人类进化遗传学.md "wikilink")[分子钟技术推算出了](../Page/分子钟.md "wikilink")[线粒体夏娃最晚出现的时间](../Page/线粒体夏娃.md "wikilink")\[97\]（这个成果被认为是人类由[非洲](../Page/非洲.md "wikilink")[单地起源的有力依据](../Page/单地起源说.md "wikilink")\[98\]）是利用mtDNA研究群体遗传学的典型例子。另外一个例子是对[尼安德特人](../Page/尼安德特人.md "wikilink")[骨骼](../Page/骨骼.md "wikilink")[化石中mtDNA测序](../Page/化石.md "wikilink")。该测序的结果显示，尼安德特人与[解剖学意义上的](../Page/解剖学.md "wikilink")[现代人在mtDNA序列上有较大差异](../Page/现代人.md "wikilink")，说明两者间缺乏基因交流。\[99\]虽然mtDNA在遗传学研究中占据了重要地位，但是mtDNA序列中的信息只能反映所考察的群体中的[雌性成员的演化进程](../Page/雌性.md "wikilink")，而不能代表整个种群。这一缺陷需要由对父系遗传序列（如[Y染色体上的](../Page/Y染色体.md "wikilink")[非重组区](../Page/非重组区.md "wikilink")）的测序弥补。\[100\]广义上来说，只有既考虑了mtDNA又考虑了核DNA的遗传学研究才能为种群的进化史提供全面的线索。\[101\]

## 机能障碍与疾病

正常細胞含數個至千餘個相同的粒線體，如細菌大小。研究證實，在老人身上，其身體細胞內粒線體的含量有明顯減少。粒線體負責製造腺苷三磷酸ATP，如同發電機一般，是身體能量的來源，其在轉換為ATP能量的過程需動用電子傳遞。如果沒有正確捕捉到電子，逸出的電子會與氧分子結合成超氧自由基，很容易破壞[鹼基而造成粒線體DNA突變](../Page/鹼基.md "wikilink")，進而累積一些細胞的衰老或疾病因子，像是一些老年疾病：糖尿病、心臟病、[關節炎等](../Page/關節炎.md "wikilink")，都與粒線體DNA變異有關。

## 粒線體缺陷疾病

線粒體病（mitochondrial disorders）是遺傳缺損引起線粒體代謝酶缺陷，致使ATP合成障礙、能量來源不足導致的一組異質性病變。

線粒體是密切與能量代謝相關的細胞器，無論是細胞的存活（[氧化磷酸化](../Page/氧化磷酸化.md "wikilink")）和[細胞死亡](../Page/細胞死亡.md "wikilink")（凋亡）均與線粒體功能有關，特別是呼吸鏈的氧化磷酸化異常與許多人類疾病有關。

Luft等（1962）首次報道一例[線粒體肌病](../Page/線粒體肌病.md "wikilink")，生化研究證實為氧化磷酸化脫耦聯引起。Anderson（1981）測定人類線粒體DNA（mtDNA）全長序列，Holt（1988）首次發現線粒體病患者mtDNA缺失，證實mtDNA突變是人類疾病的重要病因，建立了有別于傳統[孟德爾遺傳的線粒體遺傳新概念](../Page/孟德爾遺傳.md "wikilink")。

根據線粒體病變部位不同可分為：

1\.[線粒體肌病](../Page/線粒體肌病.md "wikilink")（mitochondrial
myopathy）線粒體病變侵犯[骨骼肌為主](../Page/骨骼肌.md "wikilink")。

2\.[線粒體腦肌病](../Page/線粒體腦肌病.md "wikilink")（mitochondrial
encephalomyopathy）病變同時侵犯骨骼肌和[中樞神經系統](../Page/中樞神經系統.md "wikilink")。

3.線粒體腦病病變侵犯中樞神經系統為主。

## 参见

  - [內共生理論](../Page/內共生理論.md "wikilink")
  - [呼吸作用](../Page/呼吸作用.md "wikilink")
  - [化学渗透假说](../Page/化学渗透假说.md "wikilink")
  - [叶绿体](../Page/叶绿体.md "wikilink")
  - [线粒体病](../Page/线粒体病.md "wikilink")
  - [线粒体DNA](../Page/线粒体DNA.md "wikilink")
  - [糖酵解](../Page/糖酵解.md "wikilink")
  - [线粒体遗传学](../Page/线粒体遗传学.md "wikilink")
  - [线粒体核糖体](../Page/线粒体核糖体.md "wikilink")
  - [紡錘核移植](../Page/紡錘核移植.md "wikilink")（Spindle transfer/spindle
    nuclear transfer）
  - [線粒體病](../Page/線粒體病.md "wikilink")（Mitochondrial disease）

## 注释

<references group="注"/>

## 参考文献

## 外部連結

  - [线粒体研究](https://web.archive.org/web/20080821114550/http://www.mitochondrial.net/)（mitochondrial.net）
  - [线粒体：功能决定结构](https://web.archive.org/web/20100125062948/http://www.cytochemistry.net/Cell-biology/mitoch1.htm)（cytochemistry.net）
  - [MIP，线粒体生理学学会](http://www.mitophysiology.org/)
  - [密西根大学提供的结构研究](../Page/密西根大学.md "wikilink")：
      - [线粒体外膜蛋白的3维结构](http://opm.phar.umich.edu/localization.php?localization=Mitochondrial%20outer%20membrane)
      - [线粒体内膜蛋白的3维结构](http://opm.phar.umich.edu/localization.php?localization=Mitochondrial%20inner%20membrane)
  - [阿拉巴马大学提供的](../Page/阿拉巴马大学.md "wikilink")[线粒体相关链接](https://web.archive.org/web/20090418174136/http://bama.ua.edu/~hsmithso/class/bsc_495/mito-plastids/mito_web.html)
  - [美因茨大学提供的](../Page/美因茨大学.md "wikilink")[线粒体图册](http://www.uni-mainz.de/FB/Medizin/Anatomie/workshop/EM/EMMitoE.html)
  - [威斯康星大学提供的](../Page/威斯康星大学.md "wikilink")[线粒体蛋白间的合作关系研究资料](http://www.mitoproteins.org)
  - Cell Centered
    Database中的[线粒体资料](http://ccdb.ucsd.edu/sand/main?stype=lite&keyword=mitochondrion&Submit=Go&event=display&start=1)
  - [圣地亚哥州立大学提供的](../Page/圣地亚哥州立大学.md "wikilink")[线粒体电子断层扫描重建资料](http://www.sci.sdsu.edu/TFrey/MitoMovie.htm)
  - 大鼠肝脏线粒体的低温电子断层扫描[视频](https://web.archive.org/web/20031211051649/http://www.wadsworth.org/databank/electron/cryomito_dis2.html)

[线粒体](../Category/线粒体.md "wikilink")

1.
2.
3.
4.
5.
6.  这些现在已不再继续使用的名称包括：*blepharoblast*、*condriokont*、*chondriomite*、*chondrioplast*、*chondriosome*、*chondrioshere*、*filum*、*fuchsinophilic
    granule*、*interstitial
    body*、*körner*、*fädenkörner*、*mitogel*、*parabasal
    body*、*plasmasome*、*plastochondria*、*plastome*、*sphereoplast*和*vermicle*等（按首[字母在](../Page/字母.md "wikilink")[英文字母表中的顺序排列](../Page/英文字母.md "wikilink")），其中“*chondriosome*”（可译为“颗粒体”）直至1982年仍见诸[欧洲各国的](../Page/欧洲.md "wikilink")[科学](../Page/科学.md "wikilink")[文献](../Page/文献.md "wikilink")。
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20. 现在人们已经了解前人观察细胞时发现线粒体呈现不同形状的原因：一方面，线粒体自身形状多变；另一方面，制备[切片时切割细胞的角度不同也会影响到](../Page/切片.md "wikilink")[显微镜下观察到的线粒体的形状](../Page/显微镜.md "wikilink")。
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.
38. 线粒体基质中每1[μ](../Page/微.md "wikilink")[L的水](../Page/升.md "wikilink")[溶解了约](../Page/溶解.md "wikilink")1.25[mg的蛋白质](../Page/毫克.md "wikilink")，而细胞质基质中每1μL的水中只溶解了约0.26mg蛋白质，所以线粒体基质较细胞质基质黏稠。
39.
40.
41.
42.
43. 有氧呼吸第一阶段对应的是糖酵解，是糖类经过一系列[酶促反应产生丙酮酸的过程](../Page/酶促反应.md "wikilink")。该过程在细胞质基质内完成，能释放少量能量。
44.
45.
46.
47.
48.
49.
50.
51.
52.
53.
54.
55.
56.
57.
58.
59.
60.
61.
62.
63.
64.
65.
66.
67.
68.
69.
70.
71.
72.
73.
74.
75.
76.
77.
78.
79.
80.
81.
82.
83.
84.
85.
86.
87.
88.
89.
90.
91.
92.
93.
94.
95.
96.
97.
98.
99.
100.
101.