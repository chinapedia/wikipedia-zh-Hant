**線西鄉**位於[臺灣](../Page/臺灣.md "wikilink")[彰化縣西北部](../Page/彰化縣.md "wikilink")，西濱[臺灣海峽](../Page/臺灣海峽.md "wikilink")，是[彰化縣內面積最小的漁業鄉鎮](../Page/彰化縣.md "wikilink")。

## 行政區

  - **2013.5**

<table>
<thead>
<tr class="header">
<th><p>村名</p></th>
<th><p>鄰數</p></th>
<th><p>戶數</p></th>
<th><p>面積<br />
<small>（km²）</small></p></th>
<th><p>人口</p></th>
<th><p>人口密度<br />
<small>（人/km²）</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>線西村</p></td>
<td><p>23</p></td>
<td><p>674</p></td>
<td><p>2.34</p></td>
<td><p>2,742</p></td>
<td><p>1,171.8</p></td>
</tr>
<tr class="even">
<td><p>德興村</p></td>
<td><p>13</p></td>
<td><p>382</p></td>
<td><p>1.48</p></td>
<td><p>1,586</p></td>
<td><p>1,071.62</p></td>
</tr>
<tr class="odd">
<td><p>頂犁村</p></td>
<td><p>23</p></td>
<td><p>753</p></td>
<td><p>2.73</p></td>
<td><p>3,031</p></td>
<td><p>1,110.26</p></td>
</tr>
<tr class="even">
<td><p>下犁村</p></td>
<td><p>17</p></td>
<td><p>561</p></td>
<td><p>2.42</p></td>
<td><p>2,304</p></td>
<td><p>952.01</p></td>
</tr>
<tr class="odd">
<td><p>塭仔村</p></td>
<td><p>15</p></td>
<td><p>375</p></td>
<td><p>4.36</p></td>
<td><p>1,585</p></td>
<td><p>363.53</p></td>
</tr>
<tr class="even">
<td><p>溝內村</p></td>
<td><p>11</p></td>
<td><p>371</p></td>
<td><p>2.82</p></td>
<td><p>1,474</p></td>
<td><p>522.7</p></td>
</tr>
<tr class="odd">
<td><p>寓埔村</p></td>
<td><p>22</p></td>
<td><p>715</p></td>
<td><p>5.45</p></td>
<td><p>2,796</p></td>
<td><p>513.03</p></td>
</tr>
<tr class="even">
<td><p>頂-{庄}-村</p></td>
<td><p>17</p></td>
<td><p>376</p></td>
<td><p>1.87</p></td>
<td><p>1,529</p></td>
<td><p>817.65</p></td>
</tr>
<tr class="odd">
<td><p><strong>線西鄉</strong></p></td>
<td><p><strong>141</strong></p></td>
<td><p><strong>4,207</strong></p></td>
<td><p><strong>18.0856</strong></p></td>
<td><p><strong>17,047</strong></p></td>
<td><p><strong>942.57</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 歷任鄉長

<table>
<thead>
<tr class="header">
<th><p>任次</p></th>
<th><p>姓名</p></th>
<th><p>就任時間</p></th>
<th><p>卸任時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>黃秀傳</p></td>
<td><p>1951年7月2日</p></td>
<td><p>1953年7月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>黃秀傳</p></td>
<td><p>1953年7月1日</p></td>
<td><p>1956年7月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>黃秀傳</p></td>
<td><p>1956年7月1日</p></td>
<td><p>1960年1月17日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>黃　霧</p></td>
<td><p>1960年1月17日</p></td>
<td><p>1964年3月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>黃　霧</p></td>
<td><p>1964年3月1日</p></td>
<td><p>1968年3月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>黃秀禎</p></td>
<td><p>1968年3月1日</p></td>
<td><p>1973年4月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>黃共仙</p></td>
<td><p>1973年4月1日</p></td>
<td><p>1977年12月30日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>楊焜輝</p></td>
<td><p>1977年12月30日</p></td>
<td><p>1982年3月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>許清圭</p></td>
<td><p>1982年3月1日</p></td>
<td><p>1986年3月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>許德和</p></td>
<td><p>1986年3月1日</p></td>
<td><p>1990年3月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>黃榮吉</p></td>
<td><p>1990年3月1日</p></td>
<td><p>1994年3月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>黃正義</p></td>
<td><p>1994年3月1日</p></td>
<td><p>1998年3月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>黃正義</p></td>
<td><p>1998年3月1日</p></td>
<td><p>2000年1月6日</p></td>
<td><p>任內因病逝世</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>黃漢鉛</p></td>
<td><p>2000年4月1日</p></td>
<td><p>2002年3月1日</p></td>
<td><p>籍，補選上任</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>黃漢鉛</p></td>
<td><p>2002年3月1日</p></td>
<td><p>2002年12月19日</p></td>
<td><p>籍[1]。2002年11月12日，台中高分院依重傷害等罪將黃漢鉛判刑3年6月，因其擔任縣議員期間被控為樁腳處理債務時黃夫婦及其子，造成其子左眼受傷，另又依妨害公務罪將黃漢鉛判刑八個月，因黃向警方關說賭博電玩案被拒，以穢言辱罵員警[2][3]。2002年12月13日，台中高分院駁回黃某關於妨害公務罪的上訴[4]。2002年12月19日依法解除其鄉長職務[5]。</p></td>
</tr>
<tr class="even">
<td><p>代理</p></td>
<td><p>李俊德</p></td>
<td><p>2002年12月19日</p></td>
<td><p>2003年3月</p></td>
<td><p>縣政府指派</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>黃弘耀</p></td>
<td><p>2003年3月</p></td>
<td><p>2006年3月1日</p></td>
<td><p>籍，2003年2月16日補選鄉長，線西鄉公所秘書黃弘耀一人參選並當選[6]。</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>黃弘耀</p></td>
<td><p>2006年3月1日</p></td>
<td><p>2007年1月24日</p></td>
<td><p>籍，黃弘耀當選後被檢舉於選舉前與有意參選鄉長的鄉民代表會主席蘇賜木達成協議，承諾若當選鄉長，將於任期第4年聘蘇某擔任鄉公所秘書，並將鄉政主導權交予蘇某全權運作，讓其有機會博取鄉民好評、提高聲望，以便蘇某可以競選下屆鄉長，並給付500萬元作為蘇某退選之代價[7]。2007年1月24日，台中高分院二審駁回黃弘耀涉嫌競選期間「搓圓仔湯」案之上訴，維持一審當選無效之判決[8][9]，黃弘耀遭解職</p></td>
</tr>
<tr class="odd">
<td><p>代理</p></td>
<td><p>陳瑞霞</p></td>
<td><p>2007年2月6日</p></td>
<td><p>2007年4月</p></td>
<td><p>縣政府指派</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>蔡麗娟</p></td>
<td><p>2007年4月</p></td>
<td><p>2010年3月1日</p></td>
<td><p>籍，2007年4月15日補選鄉長，遭解職的鄉長黃弘耀的妻子蔡麗娟、以及黃寶川等2人參選[10]，由蔡麗娟當選[11]。</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>蔡麗娟</p></td>
<td><p>2010年3月1日</p></td>
<td><p>2014年12月25日</p></td>
<td><p>籍</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>蘇賜木</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>，上任前病逝[12]</p></td>
</tr>
<tr class="odd">
<td><p>代理</p></td>
<td><p>蘇韋峻</p></td>
<td><p>2014年12月25日</p></td>
<td><p>2015年3月6日</p></td>
<td><p>縣府指派代理，鄉長當選人蘇賜木之子[13]。</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>黃浚豪</p></td>
<td><p>2015年3月6日</p></td>
<td><p>-</p></td>
<td><p>，原名黃漢鉛，2015年2月7日補選鄉長，以3040票當選[14]。</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p>蘇韋峻</p></td>
<td><p>2018年12月25日</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 人口

## 警政治安

  - [彰化縣警察局和美分局](http://www.chpb.gov.tw/part_net.php?group_item=5)　Tel：(04)755-2031
      - 線西分駐所：彰化縣線西鄉寓埔村中央路二段65號　Tel：(04)758-2247

## 交通

### 公路

#### 省道

  - [省道](https://zh.wikipedia.org/wiki/File:TW_PHW17.svg "fig:省道")[台17線](../Page/台17線.md "wikilink")
  - [省道](https://zh.wikipedia.org/wiki/File:TW_PHW61.svg "fig:省道")[台61線](../Page/台61線.md "wikilink")（西濱快速公路）
      - 線西交流道（169）

#### 縣道

  - [縣道](https://zh.wikipedia.org/wiki/File:TW_CHW138.svg "fig:縣道")[縣道138號](../Page/縣道138號.md "wikilink")：<font size= -1>線西鄉－[和美鎮](../Page/和美鎮.md "wikilink")－[彰化市](../Page/彰化市.md "wikilink")</font>

## 教育

### 國民中學

  - [彰化縣立線西國民中學](http://www.hhjh.chc.edu.tw/)

### 國民小學

  - [彰化縣線西鄉線西國民小學](http://www.sces.chc.edu.tw/)
  - [彰化縣線西鄉曉陽國民小學](http://www.syes.chc.edu.tw/~infor/)

## 旅遊

[塭仔漁港.jpg](https://zh.wikipedia.org/wiki/File:塭仔漁港.jpg "fig:塭仔漁港.jpg")

  - [蛤蜊兵營](../Page/蛤蜊兵營.md "wikilink")
  - 慶安水道河濱公園
  - 彰濱[肉粽角](../Page/肉粽角.md "wikilink")
  - 塭仔漁港
  - 林家馬場
  - 線西濱海騎馬俱樂部
  - 冬季油麻花季
  - 藥上農莊
  - [台灣優格餅乾學院](http://www.cookieschool.com.tw/)
  - 白馬酒莊
  - [興麥蛋捲烘焙王國觀光工廠](https://www.ximai.com.tw/zh/news.html)

## 參考文獻

## 外部連結

  - [線西鄉公所](http://town.chcg.gov.tw/xianxi)
  - [滄海遺珠…線西海濱風光](http://library.taiwanschoolnet.org/cyberfair2005/hhjh2004306/index.htm)

[Category:彰化縣行政區劃](../Category/彰化縣行政區劃.md "wikilink")
[線西鄉](../Category/線西鄉.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.