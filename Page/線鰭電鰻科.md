**線鰭電鰻科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[電鰻目的其中一科](../Page/電鰻目.md "wikilink")。

## 分類

**線鰭電鰻科**下分5個屬，如下：

### 喉電鰻屬（*Archolaemus*）

  - [巴西喉電鰻](../Page/巴西喉電鰻.md "wikilink")（*Archolaemus blax*）

### 圓電鰻屬（*Distocyclus*）

  - [錐吻圓電鰻](../Page/錐吻圓電鰻.md "wikilink")（*Distocyclus conirostris*）
  - [戈氏圓電鰻](../Page/戈氏圓電鰻.md "wikilink")（*Distocyclus goajira*）

### 埃氏電鰻屬（*Eigenmannia*）

  - [亨堡埃氏電鰻](../Page/亨堡埃氏電鰻.md "wikilink")（*Eigenmannia humboldtii*）
  - [緣邊埃氏電鰻](../Page/緣邊埃氏電鰻.md "wikilink")（*Eigenmannia limbata*）
  - [大鱗埃氏電鰻](../Page/大鱗埃氏電鰻.md "wikilink")（*Eigenmannia macrops*）
  - [小口埃氏電鰻](../Page/小口埃氏電鰻.md "wikilink")（*Eigenmannia microstoma*）
  - [暗色埃氏電鰻](../Page/暗色埃氏電鰻.md "wikilink")（*Eigenmannia nigra*）
  - [三線埃氏電鰻](../Page/三線埃氏電鰻.md "wikilink")（*Eigenmannia trilineata*）
  - [巴西埃氏電鰻](../Page/巴西埃氏電鰻.md "wikilink")（*Eigenmannia vicentespelaea*）
  - [青色埃氏電鰻](../Page/青色埃氏電鰻.md "wikilink")（*Eigenmannia virescens*）

### 賈皮河電鰻屬(*Japigny*)

  - [基氏賈皮河電鰻](../Page/基氏賈皮河電鰻.md "wikilink")(*Japigny kirschbaum*)

### 棒電鰻屬（*Rhabdolichops*）

  - [頭孔棒電鰻](../Page/頭孔棒電鰻.md "wikilink")（*Rhabdolichops caviceps*）
  - [埃氏棒電鰻](../Page/埃氏棒電鰻.md "wikilink")（*Rhabdolichops eastwardi*）
  - [琥珀棒電鰻](../Page/琥珀棒電鰻.md "wikilink")（*Rhabdolichops electrogrammus*）
  - [朱氏棒電鰻](../Page/朱氏棒電鰻.md "wikilink")（*Rhabdolichops jegui*）
  - [倫氏棒電鰻](../Page/倫氏棒電鰻.md "wikilink")（''Rhabdolichops lundbergi ''）
  - [巴西棒電鰻](../Page/巴西棒電鰻.md "wikilink")（''Rhabdolichops navalha ''）
  - [淺黑棒電鰻](../Page/淺黑棒電鰻.md "wikilink")（*Rhabdolichops nigrimans*）
  - [史氏棒電鰻](../Page/史氏棒電鰻.md "wikilink")（*Rhabdolichops stewarti*）
  - [特氏棒電鰻](../Page/特氏棒電鰻.md "wikilink")（*Rhabdolichops troscheli*）
  - [贊氏棒電鰻](../Page/贊氏棒電鰻.md "wikilink")（*Rhabdolichops zareti*）

### 線鰭電鰻屬（*Sternopygus*）

  - [等唇線鰭電鰻](../Page/等唇線鰭電鰻.md "wikilink")（*Sternopygus aequilabiatus*）
  - [砂棲線鰭電鰻](../Page/砂棲線鰭電鰻.md "wikilink")（*Sternopygus arenatus*）
  - [鞍斑線鰭電鰻](../Page/鞍斑線鰭電鰻.md "wikilink")（*Sternopygus astrabes*）
  - [亞馬遜河線鰭電鰻](../Page/亞馬遜河線鰭電鰻.md "wikilink")（*Sternopygus branco*）
  - [卡氏線鰭電鰻](../Page/卡氏線鰭電鰻.md "wikilink")（*Sternopygus castroi*）
  - [大尾線鰭電鰻](../Page/大尾線鰭電鰻.md "wikilink")（*Sternopygus macrurus*）
  - [鈍吻線鰭電鰻](../Page/鈍吻線鰭電鰻.md "wikilink")（*Sternopygus obtusirostris*）
  - [委內瑞拉線鰭電鰻](../Page/委內瑞拉線鰭電鰻.md "wikilink")（*Sternopygus pejeraton*）
  - [巴西線鰭電鰻](../Page/巴西線鰭電鰻.md "wikilink")（*Sternopygus xingu*）

## 參考資料

1.  [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[\*](../Category/胸肛魚科.md "wikilink")