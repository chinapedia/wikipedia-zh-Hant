**緣禾**是一個與[沮渠氏](../Page/沮渠氏.md "wikilink")[北凉和](../Page/北凉.md "wikilink")[闞姓](../Page/闞姓.md "wikilink")[高昌政權有關的](../Page/高昌.md "wikilink")[年號](../Page/年號.md "wikilink")，不见于史籍\[1\]。

來自[甘肅的石刻有](../Page/甘肅.md "wikilink")“……緣禾三年歲次甲戌……”字樣\[2\]。[吐魯番](../Page/吐魯番.md "wikilink")[出土文書中有缺少](../Page/吐魯番出土文書.md "wikilink")[干支的](../Page/干支.md "wikilink")“……緣禾五年……”、“……緣禾六年……”字樣\[3\]。不同意見認爲[沮渠蒙遜的](../Page/沮渠蒙遜.md "wikilink")[義和年號和](../Page/義和.md "wikilink")[沮渠牧犍的](../Page/沮渠牧犍.md "wikilink")[永和年號都可能是緣禾之錯誤](../Page/承和_\(沮渠牧犍\).md "wikilink")\[4\]。根據石刻的紀年所得知緣禾元年的[壬申干支都與義和](../Page/壬申.md "wikilink")、永和或承和干支不合\[5\]。

北涼奉夏國的[承光年號時](../Page/承光_\(赫连昌\).md "wikilink")，曾用了同[韻意義相近的](../Page/韻.md "wikilink")**陽**字代替了**光**字，做成**承陽**年號。同理，學界認爲沮渠氏北凉奉[北魏正朔時](../Page/北魏.md "wikilink")，遵照諧音，將“[延和](../Page/延和_\(北魏\).md "wikilink")”和“[太延](../Page/太延.md "wikilink")”分別改作“**緣禾**”和“[太緣](../Page/太緣.md "wikilink")”\[6\]\[7\]。

至於北魏的延和年號出現了三年而北涼的卻有緣禾五、六年可能是北涼不嚴格跟隨北魏紀年的緣故\[8\]。

## 參考文獻

<references/>

[category:北凉年号](../Page/category:北凉年号.md "wikilink")
[category:高昌年号](../Page/category:高昌年号.md "wikilink")
[category:北魏年号](../Page/category:北魏年号.md "wikilink")

[Category:有爭議的年號](../Category/有爭議的年號.md "wikilink")

1.
2.

3.

4.
5.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 978-7-101-02512-5

6.
7.  施新榮（2003年），《國内20世纪以來的高昌史研究綜述》，歐亞學研究網：<http://www.eurasianhistory.com/data/articles/l02/578.html>，[UTC時間](../Page/UTC.md "wikilink")2006年1月19日
    03:53更新。

8.