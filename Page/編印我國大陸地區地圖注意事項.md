[ROC_Administrative_Subdivisions_zh-hant.svg](https://zh.wikipedia.org/wiki/File:ROC_Administrative_Subdivisions_zh-hant.svg "fig:ROC_Administrative_Subdivisions_zh-hant.svg")

**編印我國大陸地區地圖注意事項**是[中華民國內政部根據](../Page/中華民國內政部.md "wikilink")《[水陸地圖審查條例](../Page/水陸地圖審查條例.md "wikilink")》，為規範並審查國內印行之[中華民國地圖而制訂的](../Page/中華民國.md "wikilink")[行政命令](../Page/行政命令.md "wikilink")。該條令於1988年（[民國](../Page/民國.md "wikilink")77年）8月27日由內政部訂頒，後幾經修正，並於2002年（民國91年）11月28日更名為「**編印大陸地區地圖注意事項**」，最後於2004年（民國93年）11月9日由時任內政部長的[蘇嘉全簽署部令廢止](../Page/蘇嘉全.md "wikilink")。而實際上官方早在1990年代後期就已不再發行「中華民國全圖」。\[1\]

## 民國91年條文

## 民國93年條文

## 中華民國全圖

## 參見

  - [原中華民國法理疆域行政區劃](../Page/原中華民國法理疆域行政區劃.md "wikilink")
  - [外蒙古獨立](../Page/外蒙古獨立.md "wikilink")

## 注释

## 外部链接

  - [內政部地政司地政法規檢索](http://www.land.moi.gov.tw/law/chhtml/index.asp)：法規沿革
  - [教育部秘書室](http://140.111.1.192/secretary/history-publicnews/85/8504/decree.htm)：85年資料
  - [教育部秘書室](http://140.111.1.192/secretary/history-publicnews/86/8608/decree.htm)：86年資料
  - [司法院法學資料檢索](http://nwjirs.judicial.gov.tw/change/200210/10857.html)：91年資料
  - [司法院法學資料檢索](http://nwjirs.judicial.gov.tw/change/200212/11581.html)：91年資料
  - [司法院法學資料檢索](http://nwjirs.judicial.gov.tw/change/200411/27363.html)：93年資料

[Category:臺灣地圖](../Category/臺灣地圖.md "wikilink")
[Category:中華民國法規命令](../Category/中華民國法規命令.md "wikilink")
[Category:中华民国内政部历史](../Category/中华民国内政部历史.md "wikilink")
[Category:臺灣已廢止法律](../Category/臺灣已廢止法律.md "wikilink")
[Category:1988年台灣建立](../Category/1988年台灣建立.md "wikilink")
[Category:2004年台灣废除](../Category/2004年台灣废除.md "wikilink")

1.  內政部函：「一、本部前曾於民國87年出版「中華民國全圖」，後因考量銷售量偏低、**圖資內容不符現況，目前已停止銷售**。」