**緬甸陸龜**（[学名](../Page/学名.md "wikilink")：）又名**黃頭陸龜**或**黃頭象龜**，为[陆龟科](../Page/陆龟科.md "wikilink")[印支陸龜屬的](../Page/印支陸龜屬.md "wikilink")[爬行动物](../Page/爬行动物.md "wikilink")。分布于[印度](../Page/印度.md "wikilink")、[尼泊尔](../Page/尼泊尔.md "wikilink")、经[孟加拉](../Page/孟加拉.md "wikilink")、[缅甸到](../Page/缅甸.md "wikilink")[印度支那](../Page/印度支那.md "wikilink")、[马来西亚半岛以及](../Page/马来西亚半岛.md "wikilink")[中国大陆的](../Page/中国大陆.md "wikilink")[广西等地](../Page/广西.md "wikilink")，多栖息于热带和亚热带地区的山地、丘陵中。该物种的模式产地在缅甸[若开邦](../Page/若开邦.md "wikilink")。\[1\]

其頭部、鼻孔四周均呈淡黃色，上颌缘有齿状突起。背甲很高且与腹甲相连，长约26厘米。背甲绿黄色，有不规则的黑斑；腹甲黄色。四肢粗壮呈柱状，长有发达的鳞片；指、趾间均无[蹼](../Page/蹼.md "wikilink")。生活在陆地草丛中，以[植物幼苗等为食](../Page/植物.md "wikilink")。

## 參考文獻

[category:中國爬行動物](../Page/category:中國爬行動物.md "wikilink")
[category:尼泊爾動物](../Page/category:尼泊爾動物.md "wikilink")
[category:孟加拉動物](../Page/category:孟加拉動物.md "wikilink")
[category:印度爬行動物](../Page/category:印度爬行動物.md "wikilink")
[category:緬甸動物](../Page/category:緬甸動物.md "wikilink")
[category:寮國動物](../Page/category:寮國動物.md "wikilink")
[category:泰國動物](../Page/category:泰國動物.md "wikilink")
[category:柬埔寨動物](../Page/category:柬埔寨動物.md "wikilink")
[category:越南動物](../Page/category:越南動物.md "wikilink")
[category:馬來西亞動物](../Page/category:馬來西亞動物.md "wikilink")

[Category:印支陸龜屬](../Category/印支陸龜屬.md "wikilink")
[Category:云南爬行动物](../Category/云南爬行动物.md "wikilink")

1.