[Colonial_Williamsburg_Governors_Palace_Front_Dscn7232.jpg](https://zh.wikipedia.org/wiki/File:Colonial_Williamsburg_Governors_Palace_Front_Dscn7232.jpg "fig:Colonial_Williamsburg_Governors_Palace_Front_Dscn7232.jpg")
[Backpalace_Williamsburg_Virginia.jpg](https://zh.wikipedia.org/wiki/File:Backpalace_Williamsburg_Virginia.jpg "fig:Backpalace_Williamsburg_Virginia.jpg")

**總督府**是[維吉尼亞](../Page/維吉尼亞.md "wikilink")[州長的](../Page/州長.md "wikilink")[宮殿](../Page/宮殿.md "wikilink")，一棟兩層樓的紅磚房，最初是維吉尼亞的皇家州長們的家，位於[威廉斯堡的格洛斯特公爵街](../Page/威廉斯堡.md "wikilink")。它是[殖民地威廉斯堡第二大的建築](../Page/殖民地威廉斯堡.md "wikilink")，另一是[國會大廈](../Page/國會大廈.md "wikilink")。

在殖民期間(1699-1780)，威廉斯堡是郡首府，總督府成了皇家州長的官邸。原本的總督府要用上16年修建，在1722年建成。

[美國革命戰爭期間](../Page/美國革命戰爭.md "wikilink")，在州長[杰斐逊的敦促下](../Page/托马斯·杰斐逊.md "wikilink")，維吉尼亞首府被遷往[里士满
(维吉尼亚州)](../Page/里士满_\(维吉尼亚州\).md "wikilink")，因為傑弗遜擔心脆弱的威廉斯堡擋不住[英國的攻擊](../Page/英國.md "wikilink")。不過，在革命戰爭期間，許多重要的大會都是在威廉斯堡舉行。在1781年12月22日，總督府和很多主要大廈被火毀壞了，其他的，大都在[美國內戰期間被拆毀了](../Page/美國內戰.md "wikilink")。

經過[古德溫](../Page/古德溫.md "wikilink")[牧師](../Page/牧師.md "wikilink")[博士的努力](../Page/博士.md "wikilink")，和[石油大王洛克菲勒](../Page/石油.md "wikilink")、John
D. Rockefeller Jr.及他妻子Abby Aldrich
Rockefeller提供的資助，這些華麗的宮殿在20世紀初期大都被重建。開放日是在1934年4月23日。

[Category:威廉斯堡](../Category/威廉斯堡.md "wikilink")
[Category:維吉尼亞州歷史](../Category/維吉尼亞州歷史.md "wikilink")