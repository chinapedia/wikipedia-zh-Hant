**織田秀信**（（[天正](../Page/天正.md "wikilink")8年—[慶長](../Page/慶長.md "wikilink")10年[五月初八](../Page/五月初八.md "wikilink")））是[日本](../Page/日本.md "wikilink")[安土桃山時代武將](../Page/安土桃山.md "wikilink")，洗禮名**彼得**。祖父為[織田信長](../Page/織田信長.md "wikilink")，父為[織田信忠](../Page/織田信忠.md "wikilink")。[關原之戰時反抗](../Page/關原之戰.md "wikilink")[德川家康](../Page/德川家康.md "wikilink")，被[流放至死](../Page/流放.md "wikilink")。

## 簡介

[織田信忠長子](../Page/織田信忠.md "wikilink")，母親為[側室](../Page/側室.md "wikilink")[鹽川長滿](../Page/鹽川長滿.md "wikilink")（伯耆守）之女，不過在《[西山家文言覺書秘傳錄](../Page/西山家文言覺書秘傳錄.md "wikilink")》則記載是信忠與他的未婚妻[松姬所生](../Page/武田松姬.md "wikilink")，但是[三方原之戰取消信忠與松姬的婚姻當時](../Page/三方原之戰.md "wikilink")，松姬才11歲，此說荒誕，幼名為**三法師**。

[1580年出生](../Page/1580年.md "wikilink")，由於當時信忠的正室死前並未生下兒子，因此秀信並不是信忠的嫡長子，而是庶長子。[1582年在](../Page/1582年.md "wikilink")[本能寺之變期間](../Page/本能寺之變.md "wikilink")，留在[岐阜](../Page/岐阜城.md "wikilink")，在[前田玄以及木下某保護下](../Page/前田玄以.md "wikilink")，到[清洲城避難](../Page/清洲城.md "wikilink")。在[清洲會議中](../Page/清洲會議.md "wikilink")，[羽柴秀吉力挺三法師為織田家繼承人](../Page/羽柴秀吉.md "wikilink")，結果三法師獲得[近江国](../Page/近江国.md "wikilink")[坂田郡](../Page/坂田郡.md "wikilink")3萬石領地，由[堀秀政代管](../Page/堀秀政.md "wikilink")。[1584年三法師遷移到](../Page/1584年.md "wikilink")[丹羽長秀的](../Page/丹羽長秀.md "wikilink")[坂本城](../Page/坂本城.md "wikilink")。

[1588年](../Page/1588年.md "wikilink")（天正16年）在歧阜城元服，名為**三郎秀信**，就任從四位下官職。4月[後陽成天皇前往](../Page/後陽成天皇.md "wikilink")[聚樂第時在聚樂亭行幸記以三郎侍從秀信朝臣記載](../Page/聚樂第.md "wikilink")。在當時列席人士官位排第五，與[前田利長](../Page/前田利長.md "wikilink")、[豐臣秀勝](../Page/豐臣秀勝.md "wikilink")、[結城秀康並列](../Page/結城秀康.md "wikilink")。

[1590年在](../Page/1590年.md "wikilink")[進攻小田原列入第六番隊](../Page/小田原征伐戰.md "wikilink")，是堀秀政隊中左備大將指揮鐵砲隊\[1\]。[1592年](../Page/1592年.md "wikilink")（[文祿元年](../Page/文祿.md "wikilink")）[豐臣秀勝死後獲得歧阜](../Page/豐臣秀勝.md "wikilink")13萬石領地，將部份織田家的舊家臣再次召集。1592年跟隨[長岡忠興出征朝鮮](../Page/細川忠興.md "wikilink")。翌年返回日本，與秀吉會面後，獲贈羽柴姓在當時的史書中有岐阜中納言名字。

秀信信奉[天主教](../Page/天主教.md "wikilink")（[吉利支丹](../Page/吉利支丹.md "wikilink")），[1597年與弟](../Page/1597年.md "wikilink")[秀則一起](../Page/織田秀則.md "wikilink")[洗禮](../Page/洗禮.md "wikilink")。秀信在領地內建天主教[教堂](../Page/教堂.md "wikilink")，不過沒有一面傾向支持天主教，亦保護[佛寺](../Page/佛寺.md "wikilink")、[神道寺廟](../Page/神道教.md "wikilink")。

[1600年爆發](../Page/1600年.md "wikilink")[關原之戰](../Page/關原之戰.md "wikilink")，在此之前秀信加入西軍，秀信拜見[豐臣秀賴獲得](../Page/豐臣秀賴.md "wikilink")200枚[黃金及](../Page/黃金.md "wikilink")2000至3000[石兵糧](../Page/石.md "wikilink")。三成以[美濃及](../Page/美濃國.md "wikilink")[尾張兩國領地利誘秀信加入西軍](../Page/尾張.md "wikilink")，三成則派遣家臣[柏木彦右衛門及](../Page/柏木彦右衛門.md "wikilink")[河瀨左馬之助支援戰事](../Page/河瀨左馬之助.md "wikilink")。面對德川軍的先遣部隊向[岐阜進軍](../Page/岐阜城.md "wikilink")，織田軍迎擊，雙方在[木曾川及](../Page/木曾川.md "wikilink")[米野交戰](../Page/米野.md "wikilink")（[米野會戰](../Page/米野會戰.md "wikilink")），織田軍戰敗退守[岐阜](../Page/岐阜城.md "wikilink")。織田軍在防守歧阜城時仍然奮戰，但是雙方兵力差距太多，織田軍亦告戰敗。而另一方面[郡上八幡城城主](../Page/郡上八幡城.md "wikilink")[稻葉貞通也因敗勢而投降](../Page/稻葉貞通.md "wikilink")。其後原本秀信與弟秀則打算[切腹自殺](../Page/切腹.md "wikilink")，卻被[福島正則以](../Page/福島正則.md "wikilink")「怎麼說到底您是信長公的嫡孫，領地與性命願用自己（正則本人）的戰功換取」一言阻止。

戰後秀信的13萬石領地被沒收，[流放到](../Page/流放.md "wikilink")[高野山](../Page/高野山.md "wikilink")，五年後病死，也有自殺說的說法，[法名](../Page/法名.md "wikilink")「大善院圭嚴松貞」。

## 逸聞

  - 因秀信容貌酷似祖父信長，於關原會戰時身穿西洋盔甲作戰的模樣被東軍將領比喻為「信長公再世」。
  - 秀信時代岐阜領内並無發生任何大規模的[一揆眾動亂](../Page/一揆.md "wikilink")，並持續執行信長所創的[樂市制度](../Page/樂市.md "wikilink")，相當用心經營水運貿易使民生優渥，可見其政治才能之優秀。

## 家臣

  - [百百綱家](../Page/百百綱家.md "wikilink") -
    家老，秀信的輔佐人，曾試圖阻止秀信加入西軍，未果後來擔任米野會戰的總大將。米野會戰結束後仕於[山內一豐](../Page/山內一豐.md "wikilink")。
  - [木造長政](../Page/木造長政.md "wikilink") -
    家老，與綱家一同建議秀信別加入西軍，戰敗後仕於[福島正則](../Page/福島正則.md "wikilink")。
  - [稻葉貞通](../Page/稻葉貞通.md "wikilink") -
    原織田家舊臣，曾根城主。於天正二十年（[1592年](../Page/1592年.md "wikilink")）成為秀信直屬家臣，擔任家老。
  - [飯沼長實](../Page/飯沼長實.md "wikilink") -
    [池尻城主](../Page/池尻城.md "wikilink")，[岐阜四天王之一](../Page/岐阜四天王.md "wikilink")。朝鮮出兵時擔任秀信屬下並立下眾多戰功，岐阜城攻防戰途中由於火藥庫爆炸遭炸傷，不治身亡。
  - 織田兵部 - 織田家一門眾之一人。
  - [飯沼長資](../Page/飯沼長資.md "wikilink") -
    岐阜四天王之一，長實之子，武藝出眾。米野會戰時擔任先鋒，以寡兵大戰[池田輝政](../Page/池田輝政.md "wikilink")，兵敗陣亡。

## 參考文獻

<div class="references-small">

<references />

</div>

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**織田秀信**  |-    |-

[Hidenobu](../Category/勝幡織田氏.md "wikilink")
[Hidenobu](../Category/吉利支丹.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")

1.  《[寛政重修諸家譜](../Page/寛政重修諸家譜.md "wikilink")》