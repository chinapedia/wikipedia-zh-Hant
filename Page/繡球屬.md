**繡球屬**（[学名](../Page/学名.md "wikilink")：**）是[绣球花科的一個屬](../Page/绣球花科.md "wikilink")，均為落叶[灌木](../Page/灌木.md "wikilink")，花几乎全为无性花，所谓的“花”只是[萼片而已](../Page/萼片.md "wikilink")。早期“花”为白色，后变为蓝色或粉红色。

其性喜阴湿，且对[二氧化硫等有害气体抗性强](../Page/二氧化硫.md "wikilink")。不可接受过强之直射阳光，否则叶片易灼伤。绣球花的花色可随土壤的[pH值而改变](../Page/pH值.md "wikilink")。若在酸性土壤種植（pH值比7小），花色是藍色；若在中性土壤種植（pH值大約等於7），花色是乳白色；若在鹼性土壤種植（pH值比7大），花色是紅或紫。因此可通过调节土壤的pH值来改变花色。可用[扦插法繁殖](../Page/扦插法.md "wikilink")。

其全株有毒，误食会导致[疝痛](../Page/疝痛.md "wikilink")、[腹痛](../Page/腹痛.md "wikilink")、[腹泻](../Page/腹泻.md "wikilink")、[呼吸急促](../Page/呼吸急促.md "wikilink")、[呕吐](../Page/呕吐.md "wikilink")、[便血等中毒症状](../Page/便血.md "wikilink")。

## [花語](../Page/花語.md "wikilink")

  - 希望、家族的團聚

## 如何改變花色

### 藍色

[Hortensia-1.jpg](https://zh.wikipedia.org/wiki/File:Hortensia-1.jpg "fig:Hortensia-1.jpg")
[Bigleaf_Hydrangea_Hydrangea_macrophylla_'Tokyo_Delight'_Pink_3008px.jpg](https://zh.wikipedia.org/wiki/File:Bigleaf_Hydrangea_Hydrangea_macrophylla_'Tokyo_Delight'_Pink_3008px.jpg "fig:Bigleaf_Hydrangea_Hydrangea_macrophylla_'Tokyo_Delight'_Pink_3008px.jpg")
[Bigleaf_Hydrangea_Hydrangea_macrophylla_'Tokyo_Delight'_Flowers_3008px.jpg](https://zh.wikipedia.org/wiki/File:Bigleaf_Hydrangea_Hydrangea_macrophylla_'Tokyo_Delight'_Flowers_3008px.jpg "fig:Bigleaf_Hydrangea_Hydrangea_macrophylla_'Tokyo_Delight'_Flowers_3008px.jpg")

  - 夏秋時期，每週施用一次稀釋[硫化鐵溶液](../Page/硫化鐵.md "wikilink")（1000倍以上），當花萼細胞鐵元素增加，花色便會漸漸轉為藍色。
  - 植株旁埋幾根生鏽鐵釘。
  - 換盆時混入少量[硫化鋁或](../Page/硫化鋁.md "wikilink")[硫化鎂](../Page/硫化鎂.md "wikilink")。

### 紅色

換盆時壤土依盆土多寡加入少於1/3茶匙以下的[碳酸鈣](../Page/碳酸鈣.md "wikilink")（即[石灰石](../Page/石灰石.md "wikilink")）的粉末。

## 醫學用途

  - 治療[瘧疾](../Page/瘧疾.md "wikilink")

## [种](../Page/物种.md "wikilink")

  - [狭瓣八仙花](../Page/狭瓣八仙花.md "wikilink") *Hydrangea angustipetala*
  - [冠盖绣球](../Page/冠盖绣球.md "wikilink") *Hydrangea anomala*
  - [马桑绣球](../Page/马桑绣球.md "wikilink") *Hydrangea aspera*
  - [东陵绣球](../Page/东陵绣球.md "wikilink") *Hydrangea bretschneideri*
  - [珠光绣球](../Page/珠光绣球.md "wikilink") *Hydrangea candida*
  - [尾叶绣球](../Page/尾叶绣球.md "wikilink") *Hydrangea caudatifolia*
  - [中国绣球](../Page/中国绣球.md "wikilink") *Hydrangea chinensis*
  - [毡毛绣球](../Page/毡毛绣球.md "wikilink") *Hydrangea coacta*
  - [酥醪绣球](../Page/酥醪绣球.md "wikilink") *Hydrangea coenobialis*
  - [西南绣球](../Page/西南绣球.md "wikilink") *Hydrangea davidii*
  - [盘果绣球](../Page/盘果绣球.md "wikilink") *Hydrangea discocarpa*
  - [银针绣球](../Page/银针绣球.md "wikilink") *Hydrangea dumicola*
  - [光柄绣球](../Page/光柄绣球.md "wikilink") *Hydrangea glabripes*
  - [粉背绣球](../Page/粉背绣球.md "wikilink") *Hydrangea glaucophylla*
  - [细枝绣球](../Page/细枝绣球.md "wikilink") *Hydrangea gracilis*
  - [微绒绣球](../Page/微绒绣球.md "wikilink") *Hydrangea heteromalla*
  - [白背绣球](../Page/白背绣球.md "wikilink") *Hydrangea hypoglauca*
  - [全缘绣球](../Page/全缘绣球.md "wikilink") *Hydrangea integrifolia*
  - [蝶萼绣球](../Page/蝶萼绣球.md "wikilink") *Hydrangea kawakamii*
  - [粤西绣球](../Page/粤西绣球.md "wikilink") *Hydrangea kwangsiensis*
  - [广东绣球](../Page/广东绣球.md "wikilink") *Hydrangea kwangtungensis*
  - [狭叶绣球](../Page/狭叶绣球.md "wikilink") *Hydrangea lingii*
  - [临桂绣球](../Page/临桂绣球.md "wikilink") *Hydrangea linkweiensis*
  - [长翅绣球](../Page/长翅绣球.md "wikilink") *Hydrangea longialata*
  - [长叶绣球](../Page/长叶绣球.md "wikilink") *Hydrangea longifolia*
  - [莼兰绣球](../Page/莼兰绣球.md "wikilink") *Hydrangea longipes*
  - [大果绣球](../Page/大果绣球.md "wikilink") *Hydrangea macrocarpa*
  - [绣球](../Page/绣球_\(植物\).md "wikilink") *Hydrangea macrophylla*
  - [大瓣绣球](../Page/大瓣绣球.md "wikilink") *Hydrangea macrosepala*
  - [灰绒绣球](../Page/灰绒绣球.md "wikilink") *Hydrangea mandarinorum*
  - [莽山绣球](../Page/莽山绣球.md "wikilink") *Hydrangea mangshanensis*
  - [白绒绣球](../Page/白绒绣球.md "wikilink") *Hydrangea mollis*
  - [倒卵绣球](../Page/倒卵绣球.md "wikilink") *Hydrangea obovatifolia*
  - [水亚木](../Page/水亚木.md "wikilink") *Hydrangea paniculata*
  - [乐思绣球](../Page/乐思绣球.md "wikilink") *Hydrangea rosthornii*
  - [圆叶绣球](../Page/圆叶绣球.md "wikilink") *Hydrangea rotundifolia*
  - [紫彩绣球](../Page/紫彩绣球.md "wikilink") *Hydrangea sargentiana*
  - [泽八绣球](../Page/泽八绣球.md "wikilink") *Hydrangea serrata f.
    acuminata*（为 *H. serrata*的变型）
  - [上思绣球](../Page/上思绣球.md "wikilink") *Hydrangea shaochingii*
  - [柳叶绣球](../Page/柳叶绣球.md "wikilink") *Hydrangea stenophylla*
  - [蜡莲绣球](../Page/蜡莲绣球.md "wikilink") *Hydrangea strigosa*
  - [松潘绣球](../Page/松潘绣球.md "wikilink") *Hydrangea sungpanensis*
  - [独龙绣球](../Page/独龙绣球.md "wikilink") *Hydrangea taronensis*
  - [柔毛绣球](../Page/柔毛绣球.md "wikilink") *Hydrangea villosa*
  - [紫叶绣球](../Page/紫叶绣球.md "wikilink") *Hydrangea vinicolor*
  - [挂苦绣球](../Page/挂苦绣球.md "wikilink") *Hydrangea xanthoneura*
  - [浙皖绣球](../Page/浙皖绣球.md "wikilink") *Hydrangea zhewanensis*

## 外部链接

  -
  - [Flora of China:
    *Hydrangea*](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=115977)

  - [Flora of Nepal: *Hydrangea* species
    list](http://www.efloras.org/florataxon.aspx?flora_id=110&taxon_id=115977)

  - [USDA Plant Profile:
    *Hydrangea*](http://plants.usda.gov/java/profile?symbol=HYDRA)

  - [*Hydrangea arborescens* images at
    bioimages.vanderbilt.edu](http://www.cas.vanderbilt.edu/bioimages/species/frame/hyar.htm)

  - [Hydrangea - Selecting Shrubs for Your
    Home](http://www.urbanext.uiuc.edu/shrubselector/search_results.cfm?q=hydrangea)
    (University of Illinois Extension)

  - [Hydrangea Thoughts
    I](https://web.archive.org/web/20070927110701/http://lakecounty.typepad.com/life_in_lake_county/2007/07/hydrangea-thoug.html)
    - Informative but non-scholarly essay on Hydrangea (Culture, History
    and Etymology).

  - [繡球花
    Xiuqiuhua](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00941)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[绣球属](../Category/绣球属.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")
[Category:有毒植物](../Category/有毒植物.md "wikilink")