《**繩之於法**》是[香港電台電視部與](../Page/香港電台電視部.md "wikilink")[皇家香港警務處](../Page/皇家香港警務處.md "wikilink")[警察公共關係科合製的](../Page/警察公共關係科.md "wikilink")[現場直播](../Page/現場直播.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")，在[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台播出](../Page/翡翠台.md "wikilink")，1985年10月27日開播，1997年3月28日停播\[1\]。

## 歷史

《繩之於法》仿傚[英國廣播公司第一台節目](../Page/英國廣播公司第一台.md "wikilink")《Crimewatch
UK》，每月最後一個[星期日](../Page/星期日.md "wikilink")19:30在翡翠台播出一次，播放時間原為報道香港輕微罪案的同類節目《[警訊](../Page/警訊.md "wikilink")》的播出時間（為晚上[黃金時間](../Page/黃金時間.md "wikilink")），由[張瑪莉和](../Page/張瑪莉.md "wikilink")[楊子江擔任](../Page/楊子江_\(香港\).md "wikilink")[主持](../Page/主持.md "wikilink")。節目特色是[攝影棚設有](../Page/攝影棚.md "wikilink")[電話熱線](../Page/電話.md "wikilink")「3-388288」，10位[警務人員穿](../Page/警務人員.md "wikilink")[西裝或警察](../Page/西裝.md "wikilink")[制服](../Page/制服.md "wikilink")、坐在主持群座位後方10台[電話機前接聽電話](../Page/電話機.md "wikilink")，電話熱線在節目播出時到23:30運作。節目內容主要集中於香港嚴重罪案的消息，並在[罪案現場錄製案件重演以讓市民更深入了解案情](../Page/罪案現場.md "wikilink")，案件重演多數拍得十分恐怖。如有屍體發現或兇殺案之死者不明時，更會直接播出未經[馬賽克處理的屍體照片](../Page/馬賽克_\(影像處理\).md "wikilink")，例如1985年[寶馬山雙屍案](../Page/寶馬山雙屍案.md "wikilink")；令到目擊罪案發生的觀眾在觀看節目時，一旦勾起相關罪案的回憶，可以立即致電熱線，為警方提供破案的線索。

1989年12月，《繩之於法》探討有關濫用[精神科藥物的問題](../Page/精神科藥物.md "wikilink")，[毒品調查科總參事](../Page/毒品調查科.md "wikilink")[曾蔭培講解香港濫用精神科藥物的情況以及](../Page/曾蔭培.md "wikilink")[毒品對](../Page/毒品.md "wikilink")[青少年的禍害](../Page/青少年.md "wikilink")，[禁毒常務委員會代表詳述精神科藥物對身體健康的影響](../Page/禁毒常務委員會.md "wikilink")\[2\]。

1995年，[香港電台舉辦](../Page/香港電台.md "wikilink")《繩之於法》播映十週年慶祝會。1996年，有鑑於香港的整體罪案數字不斷下降，香港電台決定將《繩之於法》與《警訊》合併，由警務人員接聽電話的熱線亦取消；不過，市民仍可透過警察舉報罪案熱線「（852）2527
7177」或者致電到就近[警署舉報罪案](../Page/警署.md "wikilink")。

1997年3月28日，《繩之於法》在翡翠台播出最後一集，為[外景拍攝加中文](../Page/外景拍攝.md "wikilink")[字幕播映](../Page/字幕.md "wikilink")，楊子江主持，在香港電台電視大廈對面錄製開場，帶領觀眾回顧《繩之於法》歷史。

## 參看

  - [警訊](../Page/警訊.md "wikilink")

## 注釋

[Category:香港電台電視節目](../Category/香港電台電視節目.md "wikilink")
[Category:警察公共關係科](../Category/警察公共關係科.md "wikilink")

1.  [楊江59](http://www.ck2i.com/j105c/index.php)，內有節錄1997年3月28日《繩之於法》最後一集。本集開場，楊子江說，他記得《繩之於法》開播日期是1985年10月27日。
2.  [香港電台1980年代經典重溫頻道](http://www.rthk.org.hk/classicschannel/tv_80.htm)，內有《繩之於法》該集完整節目。