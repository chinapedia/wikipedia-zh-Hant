**纓翅目**（**Thysanoptera**）是[昆虫纲的一目](../Page/昆虫纲.md "wikilink")，通稱为**薊馬**。

小型而细长的昆蟲，體型小約1mm左右，一般黄褐或黑色；眼睛发达；[翅膀狭长](../Page/翅膀.md "wikilink")，具有少数或无翅脉，翅缘扁长，不少種類的翅膀外緣有排列整齊的長細毛，所以稱為纓翅目；尾缺须。变态为渐进变态。

薊馬有左右不对称的銼吸式口器，食性相當廣泛，包括動物、植物與花粉、蕈類等等，有不少的種類是重要的農業[害蟲](../Page/害蟲.md "wikilink")，不過也有些種類獵食小型昆蟲或者螨類，目前紀錄約有5000多種。

## 社會行為

許多種類的纓翅目物種會在植物上取食或產卵時形成[癭](../Page/癭.md "wikilink")。一些的物種例如：** 屬\[1\]與**
屬\[2\]，在癭中形成[真社會性結構](../Page/真社會性.md "wikilink")，類似於[螞蟻](../Page/螞蟻.md "wikilink")，包含了具有繁殖能力的[后蟲以及沒有繁殖能力的士兵階級](../Page/蟻后.md "wikilink")（相當於蟻群中的兵蟻）\[3\]\[4\]\[5\]。

## 參考文獻

[Category:纓翅目](../Category/纓翅目.md "wikilink")
[Category:害蟲](../Category/害蟲.md "wikilink")

1.
2.
3.
4.
5.