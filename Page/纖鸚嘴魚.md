**纖鸚嘴魚**（[学名](../Page/学名.md "wikilink")：），又名**纖鸚鯉**，俗名鸚哥、蠔魚、臊魚，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[鸚哥魚科的其中一](../Page/鸚哥魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[東非](../Page/東非.md "wikilink")、[南非](../Page/南非.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[越南](../Page/越南.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[尼可巴群島](../Page/尼可巴群島.md "wikilink")、[安達曼群島](../Page/安達曼群島.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[索羅門群島](../Page/索羅門群島.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[紐西蘭北部](../Page/紐西蘭.md "wikilink")、[復活節島](../Page/復活節島.md "wikilink")、[夏威夷群島](../Page/夏威夷群島.md "wikilink")、[法屬玻里尼西亞等海域](../Page/法屬玻里尼西亞.md "wikilink")。

## 深度

水深3至10公尺。

## 特徵

本魚具有癒合齒板，上顎較下顎稍長，較細長的體型和角度較小的頭部輪廓。體延長而略側扁。吻圓鈍；前額不突出。上咽骨每側有咽頭齒三列，下咽骨之生齒面寬度大於長度。成體的雄魚和雌魚的差別在於身體布滿藍色的小斑點。背鰭硬棘9至10枚、背鰭軟條10枚、臀鰭硬棘3枚、臀鰭軟條9枚、脊椎骨25枚。體長可達35公分。

## 生態

本魚不經歷性別轉變。主要棲息在[潟湖](../Page/潟湖.md "wikilink")、海灣水域。以海藻及海草為食。

## 經濟利用

食用魚類，但不具經濟價值，一般作[清蒸](../Page/清蒸.md "wikilink")、[紅燒或煮味增湯](../Page/紅燒.md "wikilink")。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[vaigiensis](../Category/纖鸚嘴魚屬.md "wikilink")