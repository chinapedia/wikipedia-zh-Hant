**红楼梦**可以指：

## 小说

  - [红楼梦](../Page/红楼梦.md "wikilink")：中国古典长篇[章回小说](../Page/章回小说.md "wikilink")，[中國四大小說之一](../Page/中國四大小說.md "wikilink")，作者[曹雪芹](../Page/曹雪芹.md "wikilink")（前八十回）。

## 衍生作品

### 電視劇（無綫電視）

  - [紅樓夢
    (1975年電視劇)](../Page/紅樓夢_\(1975年電視劇\).md "wikilink")：香港[無綫電視劇集](../Page/電視廣播有限公司.md "wikilink")《[民間傳奇](../Page/民間傳奇.md "wikilink")》單元之一，1975年播出，主演[伍衛國](../Page/伍衛國.md "wikilink")、[汪明荃](../Page/汪明荃.md "wikilink")、[呂有慧](../Page/呂有慧.md "wikilink")。

### 電視劇（佳藝電視）

  - [紅樓夢
    (1977年電視劇)](../Page/紅樓夢_\(1977年電視劇\).md "wikilink")：1977年香港電視劇，[佳藝電視製作](../Page/佳藝電視.md "wikilink")，主演伍衛國、[毛舜筠](../Page/毛舜筠.md "wikilink")、[米雪](../Page/米雪.md "wikilink")。

### 電視劇（台灣）

  - [紅樓夢
    (1978年電視劇)](../Page/紅樓夢_\(1978年電視劇\).md "wikilink")：1978年台灣[華視電視劇](../Page/華視.md "wikilink")，主演[龍隆](../Page/龍隆.md "wikilink")、[程秀瑛](../Page/程秀瑛.md "wikilink")、[范丹鳳](../Page/范丹鳳.md "wikilink")。
  - [紅樓夢
    (1983年電視劇)](../Page/紅樓夢_\(1983年電視劇\).md "wikilink")：1983年台灣華視電視劇，主演[李陸齡](../Page/李陸齡.md "wikilink")、[趙永馨](../Page/趙永馨.md "wikilink")、[陳琪](../Page/陳琪_\(台灣演員\).md "wikilink")。
  - [紅樓夢
    (1996年電視劇)](../Page/紅樓夢_\(1996年電視劇\).md "wikilink")：1996年台灣華視電視劇，主演[鍾本偉](../Page/鍾本偉.md "wikilink")、[張玉嬿](../Page/張玉嬿.md "wikilink")、[鄒琳琳](../Page/鄒琳琳.md "wikilink")。

### 電視劇（中國大陸）

  - [红楼梦
    (1987年电视剧)](../Page/红楼梦_\(1987年电视剧\).md "wikilink")：1987年中国大陆电视剧，导演[王扶林](../Page/王扶林.md "wikilink")，主演[欧阳奋强](../Page/欧阳奋强.md "wikilink")、[陈晓旭](../Page/陈晓旭.md "wikilink")、[张莉](../Page/张莉_\(演员\).md "wikilink")。
  - [红楼梦
    (越剧电视剧)](../Page/红楼梦_\(越剧电视剧\).md "wikilink")：2002年中国大陆越剧电视剧，主演[钱惠丽](../Page/钱惠丽.md "wikilink")、[余彬](../Page/余彬.md "wikilink")、[赵海英](../Page/赵海英.md "wikilink")。
  - [红楼梦
    (2010年电视剧)](../Page/红楼梦_\(2010年电视剧\).md "wikilink")：2010年中国大陆电视剧，[北京电视台制作](../Page/北京电视台.md "wikilink")，导演[李少红](../Page/李少红.md "wikilink")，主演[杨洋](../Page/杨洋_\(1991年\).md "wikilink")、[蒋梦婕](../Page/蒋梦婕.md "wikilink")、[李沁](../Page/李沁.md "wikilink")、[于小彤](../Page/于小彤.md "wikilink")、[姚笛](../Page/姚笛_\(演员\).md "wikilink")、[白冰](../Page/白冰.md "wikilink")。

### 電影

  - [紅樓夢
    (1944年電影)](../Page/紅樓夢_\(1944年電影\).md "wikilink")：1944年中國電影，上海中華電影出品，主演[袁美雲](../Page/袁美雲.md "wikilink")、[周璇](../Page/周璇.md "wikilink")、[王丹鳳](../Page/王丹鳳.md "wikilink")。
  - [紅樓夢
    (1962年電影)](../Page/紅樓夢_\(1962年電影\).md "wikilink")：1962年香港[邵氏電影](../Page/邵氏.md "wikilink")，主演[任潔](../Page/任潔.md "wikilink")、[樂蒂](../Page/樂蒂.md "wikilink")、[丁紅](../Page/丁紅.md "wikilink")。
  - [红楼梦
    (1962年越剧电影)](../Page/红楼梦_\(1962年越剧电影\).md "wikilink")：1962年中国大陆越剧电影，主演[王文娟](../Page/王文娟.md "wikilink")、[徐玉兰](../Page/徐玉兰.md "wikilink")、[吕瑞英](../Page/吕瑞英.md "wikilink")。
  - [金玉良緣紅樓夢](../Page/金玉良緣紅樓夢.md "wikilink")：1977年香港邵氏電影，主演[林青霞](../Page/林青霞.md "wikilink")、[張艾嘉](../Page/張艾嘉.md "wikilink")、米雪。
  - [紅樓春上春](../Page/紅樓春上春.md "wikilink")：1978年香港三級電影，主演[張國榮](../Page/張國榮.md "wikilink")、[黃杏秀](../Page/黃杏秀.md "wikilink")。
  - [新紅樓夢](../Page/新紅樓夢.md "wikilink")：1978年台灣黃梅戲電影，主演[凌波](../Page/凌波.md "wikilink")、[周芝明](../Page/周芝明.md "wikilink")、[李菁](../Page/李菁.md "wikilink")。
  - [红楼梦
    (1989年电影)](../Page/红楼梦_\(1989年电影\).md "wikilink")：1989年中国大陆系列电影，共8集，主演[夏钦](../Page/夏钦.md "wikilink")、[陶慧敏](../Page/陶慧敏.md "wikilink")、[傅艺伟](../Page/傅艺伟.md "wikilink")、[刘晓庆](../Page/刘晓庆.md "wikilink")。
  - [红楼梦
    (2007年越剧电影经典版)](../Page/红楼梦_\(2007年越剧电影经典版\).md "wikilink")：2007年中国大陆越剧电影，主演[郑国凤](../Page/郑国凤.md "wikilink")、[王志萍](../Page/王志萍.md "wikilink")、[金静](../Page/金静.md "wikilink")。
  - [红楼梦
    (2007年越剧电影交响版)](../Page/红楼梦_\(2007年越剧电影交响版\).md "wikilink")：2007年中国大陆越剧电影，主演[赵志刚](../Page/赵志刚.md "wikilink")、[方亚芬](../Page/方亚芬.md "wikilink")、陶慧敏。
  - [紅樓夢
    (2018年電影)](../Page/紅樓夢_\(2018年電影\).md "wikilink")：2018年台灣電影，主演[利晴天](../Page/利晴天.md "wikilink")、[紀言愷](../Page/紀言愷.md "wikilink")、[林冠宇](../Page/林冠宇.md "wikilink")、[陳彥名](../Page/陳彥名.md "wikilink")。

### 戲曲

  - [红楼梦
    (越剧)](../Page/红楼梦_\(越剧\).md "wikilink")：中国剧作家[徐进于](../Page/徐进.md "wikilink")1958年创作的经典越剧剧目，曾多次被改编为越剧电影。
  - [红楼梦 (黄梅戏)](../Page/红楼梦_\(黄梅戏\).md "wikilink")：黄梅戏剧目。
  - [红楼梦 (昆曲)](../Page/红楼梦_\(昆曲\).md "wikilink")：昆曲剧目。

### 歌剧

  - [红楼梦
    (歌剧)](../Page/红楼梦_\(歌剧\).md "wikilink")：朝鲜[血海歌剧团创作的歌剧](../Page/血海歌剧团.md "wikilink")。

### 現代舞

  - [紅樓夢
    (現代舞)](../Page/紅樓夢_\(現代舞\).md "wikilink")：台灣[雲門舞集舞蹈作品](../Page/雲門舞集.md "wikilink")，[林懷民於](../Page/林懷民.md "wikilink")1983年編舞，2005年封箱演出。

### 游戏

  - [红楼梦
    (游戏)](../Page/红楼梦_\(游戏\).md "wikilink")：北京娱乐通公司开发的一款[恋爱冒险游戏](../Page/恋爱冒险游戏.md "wikilink")。
  - [紅樓夢之十二金釵(游戏)](../Page/紅樓夢之十二金釵\(游戏\).md "wikilink")：智冠科技所研發的一款[恋爱冒险游戏](../Page/恋爱冒险游戏.md "wikilink")。

### 海选比赛

  - [红楼梦中人](../Page/红楼梦中人.md "wikilink")：北京电视台为选拔2010版《红楼梦》演员而举办的大型选秀比赛。

## 参见

  - [红楼梦影视列表](../Page/红楼梦影视列表.md "wikilink")