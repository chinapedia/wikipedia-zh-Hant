**巴西龟''
（[学名](../Page/学名.md "wikilink")：**），也叫**紅耳彩龜**、**紅耳泥龜**、**巴西龟'''，是[彩龟属](../Page/彩龟属.md "wikilink")[彩龟的亚种之一](../Page/彩龟.md "wikilink")，是一种水龟。

雖然紅耳龜又名巴西龜，但其原產地並非位於[巴西](../Page/巴西.md "wikilink")，而是生存于[北美](../Page/北美.md "wikilink")[密西西比河及](../Page/密西西比河.md "wikilink")[格兰德河流域](../Page/格兰德河.md "wikilink")。现已被不少家庭当作[宠物来养殖或觀賞用寵物](../Page/宠物.md "wikilink")，但仍有餐廳拿來做為食用動物料理。

[黄腹彩龟及](../Page/黄腹彩龟.md "wikilink")[坎伯兰彩龟是其同种近亲](../Page/坎伯兰彩龟.md "wikilink")。
談到巴西龜，也許有人自然而然就聯想到牠的產地應該來自巴西，可是實際上巴西龜的產地來自美國，而且牠真正的名字也不叫「巴西龜」，應該叫做「密西西比紅耳龜」

## 分布

本种原生于[美國及](../Page/美國.md "wikilink")[墨西哥的](../Page/墨西哥.md "wikilink")[密西西比河](../Page/密西西比河.md "wikilink")、[格蘭德河水系](../Page/格蘭德河.md "wikilink")，随着宠物贸易被人类引入到世界各地，现在包括[歐洲](../Page/歐洲.md "wikilink")、[中美洲](../Page/中美洲.md "wikilink")、[南美洲](../Page/南美洲.md "wikilink")、[北亚](../Page/北亚.md "wikilink")、[南亚](../Page/南亚.md "wikilink")、[中国大陆](../Page/中国大陆.md "wikilink")、[台湾](../Page/台湾.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[夏威夷](../Page/夏威夷.md "wikilink")、[南非等非原生地均有其野化种群](../Page/南非.md "wikilink")。

## 特徵

幼體體色為鮮豔的翠綠色，殼上也有鮮明漂亮的紋路，隨著[年齡的增長](../Page/年齡.md "wikilink")，體色會漸漸變為暗沉的墨綠色，殼上紋路也會消失殆盡，成龜殼色基本上與[斑龜相同](../Page/斑龜.md "wikilink")。成龟[龜甲長](../Page/龜甲.md "wikilink")28[公分](../Page/公分.md "wikilink")，[外眼角的部分有](../Page/外眼角.md "wikilink")[紡錘形的](../Page/紡錘形.md "wikilink")[紅橘色](../Page/紅橘色.md "wikilink")[斑紋](../Page/斑紋.md "wikilink")，[腹甲每](../Page/腹甲.md "wikilink")[甲板有](../Page/甲板.md "wikilink")1對或2對[眼狀斑](../Page/眼狀斑.md "wikilink")。

## 生態

在[河川](../Page/河川.md "wikilink")、湖沼和[濕地廣泛地生活](../Page/濕地.md "wikilink")，喜好日光浴。

食性為[雜食性](../Page/雜食性.md "wikilink")，成龜較偏肉食性，故性情較兇猛。

[卵生](../Page/卵生.md "wikilink")，1次產2到25個蛋。红耳龟是受欢迎的宠物，由于被释放到野外，已经在世界上许多地方建立族群。它是发现在淡水与[半淡咸水流域](../Page/半淡咸水.md "wikilink"),
包括海岸的沼泽池塘而且被认为与本土水生的海龟竞争。它是[杂食性动物](../Page/雜食動物.md "wikilink")，会吃[昆虫](../Page/昆虫.md "wikilink")、[小龙虾](../Page/小龙虾.md "wikilink")、[虾](../Page/虾.md "wikilink")、[蠕虫](../Page/蠕虫.md "wikilink")、[蜗牛](../Page/蜗牛.md "wikilink")、[两栖动物与小](../Page/两栖动物.md "wikilink")[鱼以及](../Page/鱼.md "wikilink")[水生植物](../Page/水生植物.md "wikilink")。

壽命為15至25年，飼養的好可以活更久。

## 圖片

## 相關条目

  - [寵物龜](../Page/寵物龜.md "wikilink")

## 參考文献

## 外部链接

  - [烏龜介紹](https://web.archive.org/web/20081202155209/http://dns.hsps.tpc.edu.tw/~an115/join8~6.htm)

[Category:墨西哥爬行动物](../Category/墨西哥爬行动物.md "wikilink")
[Category:寵物龜](../Category/寵物龜.md "wikilink")
[Category:入侵爬行動物种](../Category/入侵爬行動物种.md "wikilink")
[Category:美國爬行動物](../Category/美國爬行動物.md "wikilink")
[Category:彩龟](../Category/彩龟.md "wikilink")