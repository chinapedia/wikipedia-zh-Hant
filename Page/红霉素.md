**红霉素**（）是一种[大环内酯类的](../Page/大环内酯.md "wikilink")[抗生素](../Page/抗细菌药.md "wikilink")\[1\]，可用於治療[呼吸道感染](../Page/呼吸道感染.md "wikilink")、、衣原體菌感染（），以及[梅毒](../Page/梅毒.md "wikilink")
。本品也可用於預防新生兒的\[2\]。紅黴素可以用於改善胃輕癱（）的症狀\[3\]。本品可透過靜脈注射或口服給藥\[4\]
。新生兒有時會給予含有紅黴素的眼藥水，以避免新生兒結膜炎（）\[5\]。

常見副作用包含腹絞痛、嘔吐，以及腹瀉等
。更嚴重者可能引發[偽膜性結腸炎](../Page/偽膜性結腸炎.md "wikilink")、肝臟疾病、[長QT症](../Page/長QT症.md "wikilink")，以及[過敏反應](../Page/過敏.md "wikilink")
。[青黴素過敏者通常可安全服用本品](../Page/青黴素.md "wikilink")\[6\]。[妊娠期間用藥目前顯示安全](../Page/妊娠.md "wikilink")\[7\]。一般認為[哺乳期間用藥為安全](../Page/母乳餵養.md "wikilink")，但有研究顯示，未滿兩周的新生兒若服用本品，或母體於哺乳期間用藥，可能會增加罹患[幽門狹窄的風險](../Page/幽門狹窄.md "wikilink")\[8\]\[9\]\[10\]。紅黴素屬於[大環內酯類抗生素](../Page/大環內酯.md "wikilink")，會抑制細菌的蛋白質合成\[11\]。

紅黴素於1952年首次由紅黴素放線菌（）中純化出來\[12\]\[13\]。本品列名於[世界卫生组织基本药物标准清单之中](../Page/世界卫生组织基本药物标准清单.md "wikilink")，為基礎公衛體系必備藥物之一\[14\]。該藥屬於[學名藥](../Page/學名藥.md "wikilink")，價格不貴\[15\]。本品在[发展中国家的每錠批發價約於](../Page/发展中国家.md "wikilink")
0.03 至 0.06 [USD之間](../Page/美元.md "wikilink")\[16\]。

## 发展历史

## 作用机理

红霉素可以透过[细菌](../Page/细菌.md "wikilink")[细胞膜](../Page/细胞膜.md "wikilink")，在接近供位（**P**位）与细菌[核糖体的](../Page/核糖体.md "wikilink")[50S亚基成可逆性结合](../Page/50S亚基.md "wikilink")，阻断[转移核糖核酸](../Page/转移核糖核酸.md "wikilink")（t-RNA）结合在**P**位上，同时也阻断[多肽链自受位](../Page/多肽.md "wikilink")（**A**位）至**P**位的转移，从而抑制细菌[蛋白质合成](../Page/蛋白质.md "wikilink")。

## 适应症

适应症为[细菌](../Page/细菌.md "wikilink")[感染](../Page/感染.md "wikilink")。抗菌范围与[青黴素相似](../Page/青黴素.md "wikilink")；主要用于对青霉素已经产生耐药性的[葡萄球菌](../Page/葡萄球菌.md "wikilink")、[链球菌](../Page/链球菌.md "wikilink")、[腹泻等胃肠道反应](../Page/腹泻.md "wikilink")。

## 不良反应

  - 可能发生胃肠道不良反应，如腹泻、恶心、呕吐等
  - 肝中毒（少见）
  - 过敏反应

## 药代动力学参数

## 參見

  - [白凡士林](../Page/白凡士林.md "wikilink")

## 參考文獻

  - British National Formulary "BNF 49" March 2005.
  - Mims C, Dockrell HM, Goering RV, Roitt I, Wakelin D, Zuckerman M.
    Chapter 33: Attacking the Enemy: Antimicrobial Agents and
    Chemotherapy: Macrolides. In: Medical Microbiology (3rd Edition).
    London: Mosby Ltd; 2004. p 489
  - Hunt,Ch.M., Watkins, P.B., Saenger, P., Stave, G.M., Barlascini, N.,
    Watlington, Ch.O., Wright, J.T., Guzelian, P.S., (1992)
    Heterogeneity of CYP3A isoforms metabolizing erythromycin and
    cortisol. Clinical Pharmacology and Therapeutics 51: 18-23

## 外部連結

  - [Erythromycin bound to
    proteins](http://www.ebi.ac.uk/pdbe-srv/PDBeXplore/ligand/?ligand=ERY)
    in the [PDB](../Page/蛋白質資料庫.md "wikilink")
  - [U.S.
    Patent 2,653,899](http://v3.espacenet.com/origdoc?DB=EPODOC&IDX=US2653899&F=0&QPN=US2653899)
  - [E.E.S. (Erythromycin Ethylsuccinate) Drug Information: Uses, Side
    Effects, Drug Interactions and Warnings at
    RxList](http://www.rxlist.com/erythromycin-ethylsuccinate-drug.htm)

{{-}}

[Category:醇](../Category/醇.md "wikilink")
[Category:胺](../Category/胺.md "wikilink")
[Category:醚](../Category/醚.md "wikilink")
[酮](../Category/酮.md "wikilink")
[Category:礼来公司](../Category/礼来公司.md "wikilink")
[Category:大环内酯类抗生素](../Category/大环内酯类抗生素.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.
2.
3.

4.
5.

6.

7.

8.
9.

10.

11.
12.
13.

14.

15.

16.