**紅頸袋鼠**（[学名](../Page/学名.md "wikilink")：**）为[袋鼠科](../Page/袋鼠科.md "wikilink")[大袋鼠属的一种](../Page/大袋鼠属.md "wikilink")，是一种中等體型的[袋鼠](../Page/袋鼠科.md "wikilink")，生活在較溫和及肥沃的[澳洲東部](../Page/澳洲.md "wikilink")。

## 名称

牠們是被俗称为[小袋鼠](../Page/小袋鼠.md "wikilink")（wallaby）之中最大的其中一种，故有時也被俗称為[袋鼠](../Page/袋鼠.md "wikilink")（kangaroo）。

## 特征

雄性重於20公斤，體長90厘米。

紅頸袋鼠的特徵是其黑色的鼻子及爪子，上唇的白紋，灰色的毛皮及兩肩之間的紅點。

## 分布

紅頸袋鼠棲息澳洲東部海岸及高地之間的叢林及[硬葉植物森林](../Page/硬葉植物.md "wikilink")，即由[昆士蘭的](../Page/昆士蘭.md "wikilink")[羅克漢普頓至](../Page/羅克漢普頓.md "wikilink")[南澳洲州](../Page/南澳洲州.md "wikilink")、[塔斯曼尼亞及](../Page/塔斯曼尼亞.md "wikilink")[巴斯海峽的大部份島嶼上](../Page/巴斯海峽.md "wikilink")。

在塔斯曼尼亞，[新南威爾士東北部及昆士蘭海岸](../Page/新南威爾士.md "wikilink")，紅頸袋鼠的數目在過往30年暴升，原因是被獵殺的危機減少，及消除後的森林變成適合牠們晚間覓食的草地並有草叢作日間遮蔭。但是在[維多利亞州](../Page/維多利亞州.md "wikilink")，不知為何牠們卻不怎麼普及。

## 习性

紅頸袋鼠很多時間都是獨自生活的，但仍有較稀疏的群族，往往都有共同哺食的地方。牠們於晚間覓食，尤其是在陰天或近黃昏時份，吃接近遮蔽處的草。

紅頸袋鼠有兩個[亞種](../Page/亞種.md "wikilink")。塔斯曼尼亞的[斑氏小袋鼠](../Page/斑氏小袋鼠.md "wikilink")（*M.
r.
rufogriseus*）較為細小，有較長及粗糙的毛，於[夏天末](../Page/夏天.md "wikilink")（約2月至4月）繁殖。雌性的斑氏小袋鼠若非在正常季節有孕，則會在8個月後誕下幼獸。牠們可以生活在[人類的附近](../Page/人類.md "wikilink")，曾發現牠們在都市的草坪上覓食。另一個亞種是大陸上的*M.
r. banksianus*，全年都僧繁殖。有趣的是飼養下的紅頸袋鼠有穩定的飲食時間。

## 外來引進

[Red_necked_wallaby444.jpg](https://zh.wikipedia.org/wiki/File:Red_necked_wallaby444.jpg "fig:Red_necked_wallaby444.jpg")

在[英國](../Page/英國.md "wikilink")[阿蓋爾-比特的](../Page/阿蓋爾-比特.md "wikilink")[洛蒙德湖有小數群族的紅頸袋鼠](../Page/洛蒙德湖.md "wikilink")。於1975年，在[溫比思奈得野生動物園有兩對紅頸袋鼠](../Page/溫比思奈得野生動物園.md "wikilink")，至1993年，其數量就上升至26隻。在[坎布里亞的](../Page/坎布里亞.md "wikilink")[峰區及](../Page/峰區.md "wikilink")[東蘇塞克斯的](../Page/東蘇塞克斯.md "wikilink")[亞士頓森林都有指曾出現小量的紅頸袋鼠](../Page/亞士頓森林.md "wikilink")。牠們是於1900年代引進的，但估計現時已經在當地[滅絕](../Page/滅絕.md "wikilink")。

於1870年，有幾隻[小袋鼠由](../Page/小袋鼠.md "wikilink")[塔斯曼尼亞運送往](../Page/塔斯曼尼亞.md "wikilink")[新西蘭的](../Page/新西蘭.md "wikilink")[基督城](../Page/基督城.md "wikilink")。牠們包括了兩頭雌性及一頭雄性，後來被放生。於1874年在[獵人山有牠們的蹤跡](../Page/獵人山.md "wikilink")，經年後牠們大幅的增加。現時估計小袋鼠成為了獵人山的住客，在約350000公頃的地方生活。有些人因牠們數量的增加而認為牠們是害蟲。\[1\]

## 參考文献

[Category:大袋鼠属](../Category/大袋鼠属.md "wikilink")

1.  [Waimate District
    Council](http://www.waimatedc.govt.nz/waimate/attractions/wallabies.htm)