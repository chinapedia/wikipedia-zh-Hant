**约恩·莱克·约翰森**（，），以聞名，是一位[挪威人](../Page/挪威.md "wikilink")（父亲为挪威人，母亲是波兰人）。他擅长于对数据格式的[逆向工程](../Page/逆向工程.md "wikilink")。其最著名的事蹟是参与开发了[DeCSS软件](../Page/DeCSS.md "wikilink")，其可破解DVD的[防拷保護系统](../Page/防拷.md "wikilink")（content-scrambling
system）。约恩是一个自学的软件工程师，他在高中第一年即辍学以投入更多时间在DeCSS项目上。他目前居住在[美国](../Page/美国.md "wikilink")。

## DeCSS的起訴

2001年時，年僅十八歲的约翰森與其他兩名未公開的人士一起编写了DeCSS，破解了DVD的防拷保護。隔年他被挪威[檢察官起訴](../Page/檢察官.md "wikilink")，不過兩度都獲判無罪。

## 其他參加的项目

  - 2003年，约翰森釋出了[开源的](../Page/开源.md "wikilink")[QTFairUse](../Page/QTFairUse.md "wikilink")，可用來讀出[QuickTime](../Page/QuickTime.md "wikilink")[AAC串流並存至檔案](../Page/AAC.md "wikilink")
  - 2004年，约翰森成為一個[VideoLAN的开发者](../Page/VideoLAN.md "wikilink")。
  - 2005年3月18日，约翰森與Travis Watkins、Cody
    Brocious釋出了[PyMusique](../Page/PyMusique.md "wikilink")，破解了iTunes的[DRM保護](../Page/DRM.md "wikilink")。
  - 2006年，约翰森表示他打算破解下一代DVD的防拷技術[AACS](../Page/AACS.md "wikilink")
  - 2007年，约翰森使得[iPhone不需要](../Page/iPhone.md "wikilink")[AT\&T的啟動便可以使用無線上網](../Page/AT&T.md "wikilink")（WiFi）以及[iPod的功能](../Page/iPod.md "wikilink")。
  - 2008年2月2日，约翰森发布了[doubleTwist应用程序](../Page/doubleTwist.md "wikilink")。

## 外部連結

  - [约恩·约翰森的个人博客](http://nanocr.eu/)

  - Electronic Frontier Norway's link collection on the Jon Johansen
    case: [Complete (Norwegian)
    version](http://www.efn.no/jonjohansen.html), [English version
    (links only)](http://www.efn.no/jonjohansen-en.html)

  - [DVD Jon releases program to bypass iTunes
    DRM](http://apple.slashdot.org/article.pl?sid=05/03/18/1324235)

  - [Interview with DVD Jon](http://www.slyck.com/news.php?story=733) –
    From [slyck.com](../Page/slyck.com.md "wikilink")

  - [CNN
    interview](http://archives.cnn.com/2000/TECH/computing/01/31/johansen.interview.idg/)

  - [Jon Lech Johansen talks to
    DVDfuture](http://www.dvdfuture.com/features.php?id=13)

  -
  - [Wired News: DVD Jon Lands Dream Job
    Stateside](https://web.archive.org/web/20060108092734/http://www.wired.com/news/technology/0%2C1282%2C69257%2C00.html?tw=wn_story_page_prev2)

  - [Hacker Claims to Have Cracked iPod, iTunes (UCF Central Florida
    Future
    article)](https://web.archive.org/web/20071019024741/http://media.www.centralfloridafuture.com/media/storage/paper174/news/2006/10/25/News/Hacker.Claims.To.Have.Cracked.Ipod.Itunes.Playback.Restrictions-2399180.shtml)

[Category:挪威程式設計師](../Category/挪威程式設計師.md "wikilink")
[Category:黑客](../Category/黑客.md "wikilink")
[Category:现代密码学家](../Category/现代密码学家.md "wikilink")