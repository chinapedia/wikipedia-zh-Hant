[Baptism_Site.jpg](https://zh.wikipedia.org/wiki/File:Baptism_Site.jpg "fig:Baptism_Site.jpg")的[伯大尼遗迹](../Page/伯大尼遗迹.md "wikilink")，[施洗约翰为人施行](../Page/施洗约翰.md "wikilink")[浸礼的地方](../Page/浸礼.md "wikilink")\]\]

**約旦河**（；）是[西亚地区的一条](../Page/西亚.md "wikilink")[河流](../Page/河流.md "wikilink")，发源于[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[叙利亚](../Page/叙利亚.md "wikilink")，流經[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")、[约旦](../Page/约旦.md "wikilink")，注入[死海](../Page/死海.md "wikilink")，全长251公里。在[亚伯拉罕诸教历史上](../Page/亚伯拉罕诸教.md "wikilink")，被奉为最神圣的河流之一\[1\]；而在现代，其珍貴的[水資源也成为该地区紛爭的一個因素](../Page/水資源.md "wikilink")。

## 物理特征

### 支流

  - [哈斯巴尼河](../Page/哈斯巴尼河.md "wikilink")（；），来自[黎巴嫩](../Page/黎巴嫩.md "wikilink")
  - [巴尼亞斯河](../Page/巴尼亞斯河.md "wikilink")（；），发源于[黑门山山麓巴尼亞斯的泉水](../Page/黑门山.md "wikilink")
  - [但河](../Page/但河.md "wikilink")（；），也发源于黑门山下
  - Iyon河（；），发源于黎巴嫩。

### 流程

約旦河在最初的75公里中落差甚大，河水湍急，注入沼泽化的Hula湖，略低于[海平面](../Page/海平面.md "wikilink")。流出该湖后，进一步下降大约25公里，汇入[加利利海](../Page/加利利海.md "wikilink")。在下游河段，河流的比降减小，河道也变得蜿蜒曲折，最后注入低于海平面大约400米的内陆湖──[死海](../Page/死海.md "wikilink")。在最后一段，还有2条主要的[支流从东侧汇入](../Page/支流.md "wikilink")：[耶尔穆克河和](../Page/耶尔穆克河.md "wikilink")[扎卡河](../Page/扎卡河.md "wikilink")（Jabbok）。

在加利利海以北的河段，位于以色列境内，构成[戈兰高地的西部边界](../Page/戈兰高地.md "wikilink")。在加利利海以南的河段，则构成[约旦王国](../Page/约旦王国.md "wikilink")（东侧）和以色列控制的[約旦河西岸](../Page/約旦河西岸.md "wikilink")（西侧）的边界。

## 人类影响

1964年，[以色列开始修建](../Page/以色列.md "wikilink")[水坝](../Page/水坝.md "wikilink")，从约旦河水的主要来源加利利海引水
，也是在1964年，约旦修建一条水渠，从约旦河的另一条主要支流耶尔穆克河引水。叙利亚也建造了[水库拦截耶尔穆克河河水](../Page/水库.md "wikilink")。[环保主义者谴责](../Page/环保主义.md "wikilink")[以色列](../Page/以色列.md "wikilink")、[约旦和](../Page/约旦.md "wikilink")[叙利亚大规模损害约旦河的生态系统](../Page/叙利亚.md "wikilink")。\[2\]

在现代，约旦河水量的70%到90%都被人类使用，流量大为减少。由于这个原因以及死海的高蒸发率，死海正在收缩。死海南端较浅的水域已经全部干涸，在现代已经变为盐滩。

2006年9月，出现了污染物问题：在下游，未处理的污水注入河道。约旦河上游靠近加利利海的一小段，仍然保持原始状态。大部分污染出现在下游的60英里河段
-
从加利利海到死海的曲折河段。环保主义者声称约旦河的[生态系统已经几乎被摧毁](../Page/生态系统.md "wikilink")，救援工作可能需要数十年。\[3\]
2007年，[地球之友中东宣布约旦河是世界上](../Page/地球之友.md "wikilink")100个濒危的生态地点之一，部分归因于以色列和邻近的阿拉伯国家之间缺乏合作。\[4\]

### 重要性

对于这片干燥的地区，约旦河河水是极其重要的资源，也是[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[叙利亚](../Page/叙利亚.md "wikilink")、[约旦](../Page/约旦.md "wikilink")、[以色列和](../Page/以色列.md "wikilink")[巴勒斯坦之间争议的焦点](../Page/巴勒斯坦.md "wikilink")。

### 交通

连接以色列南北两端的以色列90号公路位于约旦河西岸，与之平行。

## 圣经意义

### 希伯来圣经

《[圣经](../Page/圣经.md "wikilink")》中，约旦河被描述为滋润了一个肥沃的大平原，由于植被繁茂，被称为“[耶和华的花园](../Page/耶和华.md "wikilink")”（《[创世记](../Page/创世记.md "wikilink")》）。在《[希伯来圣经](../Page/希伯来圣经.md "wikilink")》中对约旦河没有集中的描述，只是分散见于各卷
[雅各渡过约旦河及其支流](../Page/雅各.md "wikilink")[Jabbok](../Page/Jabbok.md "wikilink")（今Al-Zarḳa)，为了到达[Haran](../Page/Haran.md "wikilink")
(，）。约旦河也是一条分界线：[以色列十二支派中的](../Page/以色列十二支派.md "wikilink")[流便支派和](../Page/流便支派.md "wikilink")[迦得支派两个支派和](../Page/迦得支派.md "wikilink")[玛拿西支派这半个支派定居在约旦河以东](../Page/玛拿西支派.md "wikilink")（《[民数记](../Page/民数记.md "wikilink")》），而其他九个半支派則由[约书亚率领](../Page/约书亚.md "wikilink")，定居在约旦河以西（,
passim）。

在[耶利哥的对面](../Page/耶利哥.md "wikilink")，称为“耶利哥的约旦河”（;
）。约旦河有许多浅滩，其中之一是以许多[以法莲人在此被](../Page/以法莲人.md "wikilink")[耶弗他所屠杀而著称](../Page/耶弗他.md "wikilink")（[士师记](../Page/士师记.md "wikilink")）。而在伯巴拉附近的浅滩，[基甸曾在那里伏击](../Page/基甸.md "wikilink")[米甸人](../Page/米甸人.md "wikilink")（）。在约旦河的平原，在疏割和撒拉但之间，则是[所罗门铸造其铜器的场地](../Page/所罗门.md "wikilink")（[列王纪上](../Page/列王纪上.md "wikilink")）。

在《圣经》中，约旦河是数次[神迹发生的地点](../Page/神迹.md "wikilink")，第一次奇迹发生在耶利哥附近，[以色列人在约书亚带领下渡过约旦河](../Page/以色列人.md "wikilink")（）。后来其中两个半支派定居在约旦河以东，在河岸上建造大型祭坛，见证他们和其他支派（,
, et
seq.）。[以利亚用自己的外衣击打河水](../Page/以利亚.md "wikilink")，水就左右分开，他和[以利沙两人走干地过了约旦河](../Page/以利沙.md "wikilink")（，）。以利沙在约旦河完成了另外2个神迹：让[乃缦在约旦河水中沐浴](../Page/乃缦.md "wikilink")7次，治愈他的麻风病；将一根木头抛在河中，使落水的斧头浮出水面（；）。

### 新约圣经

《[新约圣经](../Page/新约圣经.md "wikilink")》记载，[施洗约翰为悔改者在约旦河施行](../Page/施洗约翰.md "wikilink")[浸礼](../Page/浸礼.md "wikilink")\[5\]（[马太福音](../Page/马太福音.md "wikilink");
[马可福音](../Page/马可福音.md "wikilink"); [路加福音](../Page/路加福音.md "wikilink");
[约翰福音](../Page/约翰福音.md "wikilink")）。而在（）则详细记载此事发生在伯大尼（Bethabara）。[耶稣也来到这里](../Page/耶稣.md "wikilink")[受约翰的浸](../Page/耶稣受浸.md "wikilink")（;
; ,
)。也是在约旦河，施洗约翰宣称耶稣是[上帝的儿子和](../Page/上帝.md "wikilink")[羔羊](../Page/羔羊.md "wikilink")（）。[以赛亚對](../Page/以赛亚.md "wikilink")[弥赛亚的](../Page/弥赛亚.md "wikilink")[预言提過约旦河](../Page/预言.md "wikilink")（），并在得到复述。

耶稣曾数次渡过约旦河（；），信徒们也渡过约旦河来听他讲道，以及治愈他们的疾病（；）。当敌人试图抓捕他时，耶稣前往约旦河外，施洗约翰起初施行浸礼的地方避难（）。

## 图集

<center>

<File:Aerial> jordan.jpg|大裂谷北段 <File:Jordan> River.jpg|约旦河 <File:Yarden>
0182.JPG |<File:Route> ninety.jpg|路标

</center>

## 参考文献

## 外部链接

  - [SMART - Multilateral project for sustainable water management in
    the lower Jordan Valley](http://www.iwrm-smart.org/)
  - [约旦河和死海地图：以及上尉指挥W.F.林奇的政党的路线美国海军](http://www.wdl.org/zh/item/148)

[Category:亞洲跨國河流](../Category/亞洲跨國河流.md "wikilink")
[約旦河](../Category/約旦河.md "wikilink")
[Category:內流河](../Category/內流河.md "wikilink")
[Category:約旦河流](../Category/約旦河流.md "wikilink")
[Category:巴勒斯坦河流](../Category/巴勒斯坦河流.md "wikilink")
[Category:敘利亞河流](../Category/敘利亞河流.md "wikilink")
[Category:以色列河流](../Category/以色列河流.md "wikilink")
[Category:以色列-約旦邊界](../Category/以色列-約旦邊界.md "wikilink")
[Category:以色列-敘利亞邊界](../Category/以色列-敘利亞邊界.md "wikilink")
[Category:約旦-巴勒斯坦邊界](../Category/約旦-巴勒斯坦邊界.md "wikilink")
[Category:希伯來聖經地理](../Category/希伯來聖經地理.md "wikilink")
[Category:圣经地名](../Category/圣经地名.md "wikilink")
[Category:希伯来圣经中的地名](../Category/希伯来圣经中的地名.md "wikilink")
[Category:希伯来圣经中的城市](../Category/希伯来圣经中的城市.md "wikilink")
[Category:摩西五经中的城市](../Category/摩西五经中的城市.md "wikilink")
[Category:施洗约翰](../Category/施洗约翰.md "wikilink")
[Category:耶稣受浸](../Category/耶稣受浸.md "wikilink")
[Category:肥沃月彎](../Category/肥沃月彎.md "wikilink")
[Category:東非大裂谷](../Category/東非大裂谷.md "wikilink")

1.

2.
3.
4.  "Endangered Jordan",*Dateline World Jewry*, World Jewish
    Congress，2007年9月

5.  Cf.