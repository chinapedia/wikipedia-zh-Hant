**约瑟夫·里欧·曼凯维奇**（Joseph Leo
Mankiewicz，），生于[宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink")[威尔克斯-巴里](../Page/威尔克斯-巴里_\(宾夕法尼亚州\).md "wikilink")，[美国](../Page/美国.md "wikilink")[剧作家](../Page/剧作家.md "wikilink")，[导演](../Page/导演.md "wikilink")，[制片人](../Page/制片人.md "wikilink")。他于1949年凭借《[三妻艳史](../Page/三妻艳史.md "wikilink")》（A
Letter to Three
Wives）获得第22屆[奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")，次年又以《[彗星美人](../Page/彗星美人.md "wikilink")》（All
About
Eve）再度获得该奖项，并获得[最佳改编剧本奖](../Page/奥斯卡最佳改编剧本奖.md "wikilink")。約瑟夫·孟威茲成為繼[約翰·福特](../Page/约翰·福特.md "wikilink")（第13屆、第14屆）之後，史上第2位「連莊」奧斯卡最佳導演獎的得主。

## 作品集

### 导演

  - 1944年 《[王国的钥匙](../Page/王国的钥匙.md "wikilink")》（The Keys of the
    Kingdom）
  - 1946年 《[惊魂骇魄](../Page/惊魂骇魄.md "wikilink")》（Somewhere in the Night）
  - 1947年 《[寡妇情深结鬼缘](../Page/寡妇情深结鬼缘.md "wikilink")》（The Ghost and Mrs.
    Muir）
  - 1949年 《[三妻艳史](../Page/三妻艳史.md "wikilink")》（A Letter to Three Wives）
  - 1949年 《[陌生人之屋](../Page/陌生人之屋.md "wikilink")》（House of Strangers）
  - 1950年 《[彗星美人](../Page/彗星美人.md "wikilink")》（All About Eve）
  - 1951年 《[人言可畏](../Page/人言可畏.md "wikilink")》（People Will Talk）
  - 1953年 《[恺撒大帝](../Page/恺撒大帝.md "wikilink")》（Julius Caesar）
  - 1954年 《[赤足天使](../Page/赤足天使.md "wikilink")》（The Barefoot Contessa）
  - 1955年 《[红男绿女](../Page/红男绿女.md "wikilink")》（Guys and Dolls）
  - 1958年 《[沉静的美国人](../Page/沉静的美国人.md "wikilink")》（The Quiet American）
  - 1959年 《[夏日痴魂](../Page/夏日痴魂.md "wikilink")》（Suddenly, Last Summer）

### 编剧

  - 1949年 《[三妻艳史](../Page/三妻艳史.md "wikilink")》（A Letter to Three Wives）
  - 1950年 《[彗星美人](../Page/彗星美人.md "wikilink")》（All About Eve）

## 外部链接

  -
  -
  - [约瑟夫·曼凯维奇作品集](http://www.sensesofcinema.com/2005/great-directors/mankiewicz/)

[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:奧斯卡最佳導演獎獲獎者](../Category/奧斯卡最佳導演獎獲獎者.md "wikilink")
[Category:奧斯卡最佳改編劇本獲獎者](../Category/奧斯卡最佳改編劇本獲獎者.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:賓夕法尼亞州人](../Category/賓夕法尼亞州人.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")