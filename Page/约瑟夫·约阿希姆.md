**约瑟夫·约阿希姆**（，），[匈牙利写法为](../Page/匈牙利.md "wikilink")**约阿希姆·约瑟夫**（**Joachim
József**），[匈牙利](../Page/匈牙利.md "wikilink")[小提琴演奏家](../Page/小提琴.md "wikilink")、作曲家、指挥家和音乐教育家，[德国小提琴学派在](../Page/德国小提琴学派.md "wikilink")19世纪后半期的领袖人物。

## 早年

约阿希姆出生于现属于[奥地利的Kittsee小城](../Page/奥地利.md "wikilink")，父母是匈牙利[犹太人](../Page/犹太人.md "wikilink")，1833年随全家移居[佩斯](../Page/佩斯.md "wikilink")，幼年时即跟从Stanislaus
Serwaczynski（其后成为[亨里克·维尼亚夫斯基的老师](../Page/亨里克·维尼亚夫斯基.md "wikilink")）学习[小提琴](../Page/小提琴.md "wikilink")。1839年赴[维也纳学习音乐](../Page/维也纳.md "wikilink")。不久，随堂姐芳妮·维特根斯坦（哲学家[路德维希·维特根斯坦的祖母](../Page/路德维希·维特根斯坦.md "wikilink")）移居[莱比锡](../Page/莱比锡.md "wikilink")，在[门德尔松的指导下学习小提琴演奏](../Page/门德尔松.md "wikilink")。1844年，在[伦敦非常成功地演出了](../Page/伦敦.md "wikilink")[贝多芬的](../Page/贝多芬.md "wikilink")[小提琴协奏曲](../Page/贝多芬小提琴协奏曲.md "wikilink")，从此成名。

## 成名后

门德尔松逝世后，约阿希姆留在莱比锡跟随德国小提琴学派大师[费迪南德·大卫继续其演奏生涯](../Page/费迪南德·大卫.md "wikilink")。1848年，[李斯特定居](../Page/弗兰兹·李斯特.md "wikilink")[魏玛](../Page/魏玛.md "wikilink")，发愿要将其建成[德国音乐的](../Page/德国.md "wikilink")“[雅典](../Page/雅典.md "wikilink")”。约阿希姆立即脱离保守气氛浓厚的莱比锡，投奔李斯特。很快，李斯特就召集了一批革新气息浓厚的年轻音乐家。

1852年，约阿希姆又脱离李斯特，移居[汉诺威](../Page/汉诺威.md "wikilink")，同[罗伯特·舒曼](../Page/罗伯特·舒曼.md "wikilink")、[克拉拉·舒曼和](../Page/克拉拉·舒曼.md "wikilink")[布拉姆斯等人建立了友谊](../Page/布拉姆斯.md "wikilink")。1863年成婚。1865年，辞去在[汉诺威王室中的供职](../Page/汉诺威王国.md "wikilink")，以抗议王室对一位犹太乐师的[种族歧视](../Page/种族歧视.md "wikilink")。

1866年，约阿希姆移居[柏林](../Page/柏林.md "wikilink")，创建了当时首屈一指的“姚阿幸弦乐四重奏乐团”。1884年同妻子离婚，原因是他怀疑妻子与布拉姆斯的出版商有染，这导致他与布拉姆斯反目。

1869年，姚阿幸於柏林為當時的普魯士王朝建立柏林皇家音樂院（為柏林藝術大學音樂系的前身），伴隨著孟德爾頌門徒以及布拉姆斯好友的名號，成就他成為最有名、地位也最崇高的演奏家，此後的四十年為他所建立的音樂學院努力。

1907年，约阿希姆在柏林去世。

## 音乐创作

约阿希姆早年曾希望朝[作曲家方向发展](../Page/作曲家.md "wikilink")，《哈姆雷特序曲》等作品接近李斯特的风格，而之后的作品则主要为小提琴而作，擅于发挥高超的演奏技巧，并结合匈牙利民族风格。

## 参见

  - [古典音乐作曲家列表](../Page/古典音乐作曲家列表.md "wikilink")

## 外部链接

  - [Joseph Joachim's autograph and handwritten note to Marinanne
    Scharwenka](http://www.flickr.com/photos/mscharwenka/392901517/)(Violinist
    and wife of Philipp Scharwenka)
  - [Bach Adagio g-minor played by Joseph
    Joachim 1904](http://www.youtube.com/watch?v=i3wysuAIDGc)

[Category:匈牙利作曲家](../Category/匈牙利作曲家.md "wikilink")
[Category:匈牙利犹太人](../Category/匈牙利犹太人.md "wikilink")
[Category:浪漫主义作曲家](../Category/浪漫主义作曲家.md "wikilink")
[Category:匈牙利小提琴家](../Category/匈牙利小提琴家.md "wikilink")
[Category:犹太音乐家](../Category/犹太音乐家.md "wikilink")
[Category:儿童古典音乐家](../Category/儿童古典音乐家.md "wikilink")