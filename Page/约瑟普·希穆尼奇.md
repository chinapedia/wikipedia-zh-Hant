**约瑟普·希穆尼奇**（[克罗地亚语](../Page/克罗地亚语.md "wikilink")：****，），是一名在[澳洲出生的](../Page/澳洲.md "wikilink")[克罗地亚](../Page/克罗地亚.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，司職[後衛](../Page/後衛_\(足球\).md "wikilink")，现效力于[克罗地亚足球甲级联赛球會](../Page/克罗地亚足球甲级联赛.md "wikilink")[薩格勒布戴拿模](../Page/萨格勒布迪纳摩足球俱乐部.md "wikilink")。

## 生平

### 球會

施蒙歷父親是居於[波斯尼亞的](../Page/波斯尼亞.md "wikilink")[克羅地亞人](../Page/克羅地亞.md "wikilink")，其後移居[澳洲](../Page/澳洲.md "wikilink")。他於[澳洲首都](../Page/澳洲.md "wikilink")[堪培拉出生](../Page/堪培拉.md "wikilink")，早期於接受足球訓練，之後加入澳洲聯賽球隊[墨尔本武士](../Page/墨尔本武士足球俱乐部.md "wikilink")。1998年施蒙歷離開澳洲，往歐洲發展事業，加入[德甲球隊](../Page/德甲.md "wikilink")[汉堡](../Page/汉堡足球俱乐部.md "wikilink")。2000年轉投[柏林赫塔](../Page/柏林赫塔足球俱乐部.md "wikilink")，效力長達8個年頭。

2009年6月30日，尚餘兩年合約在身的施蒙歷引用轉出條款，以700萬[歐元加盟大肆擴軍的](../Page/歐元.md "wikilink")[賀芬咸](../Page/霍芬海姆足球俱乐部.md "wikilink")，簽約至2012年夏季\[1\]。2011年8月31日在賀芬咸淪為板凳球員的施蒙歷獲球會放行，免費轉投祖家的[薩格勒布戴拿模](../Page/萨格勒布迪纳摩足球俱乐部.md "wikilink")\[2\]。

### 國家隊

儘管施蒙歷於澳洲出生及長大，於澳洲政府資助的接受足球訓練，但他仍希望代表祖國[克羅地亞效力](../Page/克羅地亞.md "wikilink")。2001年10月10日，施蒙歷首次代表[克羅地亞國家隊上陣](../Page/克羅地亞國家足球隊.md "wikilink")，對[韓國](../Page/韓國國家足球隊.md "wikilink")。

施蒙歷最令人印象深刻的是[2006年世界盃的](../Page/2006年世界盃.md "wikilink")「三黃一紅」事件，該場[克羅地亞對](../Page/克羅地亞國家足球隊.md "wikilink")[澳洲的賽事由](../Page/澳洲國家足球隊.md "wikilink")[英格蘭球證](../Page/英格蘭.md "wikilink")[普爾執法](../Page/葛瑞姆·波爾.md "wikilink")，施蒙歷在61分鐘及90分鐘領兩面[黃牌](../Page/黃牌.md "wikilink")，但普爾並未亮出[紅牌把施蒙歷逐出球場](../Page/紅牌.md "wikilink")，更在3分鐘後再向他發出黃牌，才把他逐出球場。

2013年11月19日，在2:0擊敗[冰島的](../Page/冰島國家足球隊.md "wikilink")[世界盃附加賽結束後](../Page/2014年世界盃外圍賽歐洲區第二輪.md "wikilink")，拿著[麥克風四度帶領球迷高呼](../Page/麥克風.md "wikilink")[二次大戰時的](../Page/二次大戰.md "wikilink")[克羅埃西亞法西斯政權使用的口號](../Page/克羅埃西亞獨立國.md "wikilink")。[國際足聯紀律小組就此召開會議](../Page/國際足聯.md "wikilink")，裁定他歷違反紀律條文，被禁賽10場國際賽，這將使他不能參加2014年的[巴西世界盃足球賽](../Page/2014年世界盃足球賽.md "wikilink")3場、[2016年歐洲國家盃外圍賽](../Page/2016年歐洲國家盃外圍賽.md "wikilink")7場比賽。他喊的是[克羅埃西亞語](../Page/克羅埃西亞語.md "wikilink")"za
dom\!"，意思是「為了祖國」，觀眾回應克羅埃西亞語"spremni\!"─「準備好了」\[3\]。

## 參考資料

## 外部链接

  - [Hertha Berlin profile](http://www.hertha.de/index.php?id=1266)
  - [Oz Football
    profile](http://www.ozfootball.net/ark/Players/S/SE.html)

[Category:克羅地亞裔澳大利亞人](../Category/克羅地亞裔澳大利亞人.md "wikilink")
[Category:克罗地亚足球运动员](../Category/克罗地亚足球运动员.md "wikilink")
[Category:漢堡球员](../Category/漢堡球员.md "wikilink")
[Category:哈化柏林球員](../Category/哈化柏林球員.md "wikilink")
[Category:賀芬咸球員](../Category/賀芬咸球員.md "wikilink")
[Category:萨格勒布迪纳摩球员](../Category/萨格勒布迪纳摩球员.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:克甲球員](../Category/克甲球員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:FIFA世纪俱乐部](../Category/FIFA世纪俱乐部.md "wikilink")

1.
2.
3.  [Croatia's Josip Simunic banned from World Cup for 'pro-Nazi' chants
    -
    CNN.com](http://edition.cnn.com/2013/12/16/sport/football/simunic-nazi-chant-ban-football/)