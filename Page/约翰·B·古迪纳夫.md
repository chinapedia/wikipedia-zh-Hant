**约翰·巴尼斯特·古迪纳夫**（，），[美国](../Page/美国.md "wikilink")[固体物理学家](../Page/固体物理学.md "wikilink")，是二次電池產業的重要學者。他目前是美國[德州大學奧斯汀分校的](../Page/德州大學奧斯汀分校.md "wikilink")[機械工程和](../Page/機械工程.md "wikilink")[材料科學教授](../Page/材料科學.md "wikilink")。\[1\]

2014年，[美国国家工程院公认古迪纳夫](../Page/美国国家工程院.md "wikilink")、、和[吉野彰为現代](../Page/吉野彰.md "wikilink")[锂离子电池所做的先驱性和领先性的基础工作](../Page/锂离子电池.md "wikilink")\[2\]。

## 成就

[Inorganic-chemistry-lab-Oxford-plaque.jpg](https://zh.wikipedia.org/wiki/File:Inorganic-chemistry-lab-Oxford-plaque.jpg "fig:Inorganic-chemistry-lab-Oxford-plaque.jpg")設置的[藍色牌匾](../Page/藍色牌匾.md "wikilink")，紀念古迪纳夫等人在此發現[锂离子电池的](../Page/锂离子电池.md "wikilink")[陰極材質](../Page/陰極.md "wikilink")[鈷酸鋰](../Page/鈷酸鋰.md "wikilink")（LiCoO2）。\]\]
1980年，古迪纳夫在英國[牛津大學招攬了日本學者](../Page/牛津大學.md "wikilink")[水島公一等人](../Page/水島公一.md "wikilink")，共同發現[鋰離子電池的](../Page/鋰離子電池.md "wikilink")[陰極材質](../Page/陰極.md "wikilink")[鈷酸鋰](../Page/鈷酸鋰.md "wikilink")（LiCoO2）。\[3\]\[4\]\[5\]

1983年，古迪纳夫、M.Thackeray等人发现[锰尖晶石是优良的正极材料](../Page/锰.md "wikilink")，\[6\]。[锰尖晶石具有低价](../Page/锰.md "wikilink")、稳定和优良的导电、导锂性能。其分解温度高，且氧化性远低于钴酸锂，即使出现短路、过充电，也能够避免了燃烧、爆炸的危险。雖然純錳尖晶石随充放电循環会變衰弱，但這是可以通过材料的化學改性克服的。\[7\]
截至2013年[錳尖晶石用於商業电池](../Page/錳.md "wikilink")。\[8\]

1989年，古迪纳夫、A.Manthiram发现采用[聚电解质](../Page/聚电解质.md "wikilink")(例如，[硫酸鹽](../Page/硫酸鹽.md "wikilink"))的正极将产生更高的电压，原因是聚电解质的[电磁感应效应](../Page/电磁感应.md "wikilink")。\[9\]

此外，他還與日本學者[金森順次郎](../Page/金森順次郎.md "wikilink")（第13代[大阪大學總長](../Page/大阪大學.md "wikilink")）共同提出「古迪纳夫-金森法則」（[Goodenough-Kanamori
rules](../Page/:en:Superexchange.md "wikilink")）

## 專利訴訟

[磷酸鋰鐵](../Page/磷酸鋰鐵.md "wikilink")[LFP正極材料的專利起源來自](../Page/LFP.md "wikilink")[德州大學](../Page/德州大學.md "wikilink")，真正發現者便是其奧斯丁學區的機械工程教授古迪纳夫博士。

古迪纳夫在1996年取得專利並在1997年1月生效，但是，原告方需要支付的訴訟帳單遠高於H-Q已經對外授權所取得的權利金總額，而且，古迪纳夫和德州大學也未收到任何權利金。

德州大學技術商業化推展處總監（director of the university’s Office Technology
Commercialization）Neil
Iscoe曾在美國接受媒體訪問時說，H-Q需要付出的律師費用超過1,000萬美元，但是，他們至今收到的授權費只有100萬美元。

古迪纳夫他在接受媒體訪問時說，若他沒告訴H-Q有關[Black &
Decker計畫使用](../Page/Black_&_Decker.md "wikilink")[A123
Systems電池的事情](../Page/A123_Systems.md "wikilink")，這些訴訟就不會發生了。他希望一切問題可以在他離開人世前可以妥善解決，而且，他也會把這些損害賠償費用與權利金捐給慈善單位。

## 荣誉

古迪纳夫教授是[美国国家工程学院](../Page/美国国家工程学院.md "wikilink")，[美国国家科学院](../Page/美国国家科学院.md "wikilink")，[法国科学院和西班牙皇家學會的院士成员](../Page/法国科学院.md "wikilink")。他撰寫了550多篇文章，85本書的章節和評論，五本書，其中包括兩個開創性著作，*Magnetism
and the Chemical Bond* (1963) 和 *Les oxydes des metaux de transition*
(1973)。古迪纳夫是一個2009年[恩里科·費米獎](../Page/恩里科·費米獎.md "wikilink")（Enrico Fermi
Award）的共同獲得者。這個總統大獎是一種美國政府最古老和最負盛名的獎励，並有一個37.5萬美元的獎金。他在2010年當選為英國[皇家學會外籍會員](../Page/皇家學會.md "wikilink")。\[10\]在2013年2月1日，古迪纳夫获得[美國國家科學獎章](../Page/美國國家科學獎章.md "wikilink")。\[11\]

## 出版

  - Lightfoot, P.; Pei, S. Y.; Jorgensen, J. D.; Manthiram, A.; Tang, X.
    X. & J. B. Goodenough. ["Excess Oxygen Defects in Layered
    Cuprates"](http://www.osti.gov/cgi-bin/rd_accomplishments/display_biblio.cgi?id=ACC0329&numPages=8&fp=N),
    [Argonne National
    Laboratory](../Page/Argonne_National_Laboratory.md "wikilink"),
    University of Texas-Austin, Materials Science Laboratory [United
    States Department of
    Energy](../Page/United_States_Department_of_Energy.md "wikilink"),
    [National Science
    Foundation](../Page/National_Science_Foundation.md "wikilink"),
    (September 1990).
  - Argyriou, D. N.; Mitchell, J. F.; Chmaissem, O.; Short, S.;
    Jorgensen, J. D. & J. B. Goodenough. ["Sign Reversal of the Mn-O
    Bond Compressibility in
    La<sub>1.2</sub>Sr<sub>1.8</sub>Mn<sub>2</sub>O<sub>7</sub> Below
    T<sub>C</sub>: Exchange Striction in the Ferromagnetic
    State"](http://www.osti.gov/cgi-bin/rd_accomplishments/display_biblio.cgi?id=ACC0328&numPages=17&fp=N),
    [Argonne National
    Laboratory](../Page/Argonne_National_Laboratory.md "wikilink"),
    University of Texas-Austin, Center for Material Science and
    Engineering [United States Department of
    Energy](../Page/United_States_Department_of_Energy.md "wikilink"),
    [National Science
    Foundation](../Page/National_Science_Foundation.md "wikilink"),
    Welch Foundation, (March 1997).
  - Goodenough, J. B.; Abruna, H. D. & M. V. Buchanan. ["Basic Research
    Needs for Electrical Energy Storage. Report of the Basic Energy
    Sciences Workshop on Electrical Energy Storage,
    April 2-4, 2007"](http://www.osti.gov/cgi-bin/rd_accomplishments/display_biblio.cgi?id=ACC0330&numPages=186&fp=N),
    [United States Department of
    Energy](../Page/United_States_Department_of_Energy.md "wikilink"),
    (April 4, 2007).

## 參見

  - [鋰電池](../Page/鋰電池.md "wikilink")

  - [水島公一](../Page/水島公一.md "wikilink")

  - [吉野彰](../Page/吉野彰.md "wikilink")

  -
  -
## 参考

[Category:1922年出生](../Category/1922年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[G](../Category/美国物理学家.md "wikilink")
[G](../Category/德州大學奧斯汀分校教師.md "wikilink")
[G](../Category/美国国家工程院院士.md "wikilink")
[G](../Category/耶魯大學校友.md "wikilink")
[G](../Category/芝加哥大學校友.md "wikilink")
[G](../Category/麻省理工學院教師.md "wikilink")
[G](../Category/法蘭西科學院院士.md "wikilink")
[G](../Category/皇家学会外籍会员.md "wikilink")
[G](../Category/美国国家科学院院士.md "wikilink")
[Category:日本国际奖获得者](../Category/日本国际奖获得者.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:恩里科·費米獎獲獎者](../Category/恩里科·費米獎獲獎者.md "wikilink")
[Category:富兰克林研究所本杰明·富兰克林奖章获得者](../Category/富兰克林研究所本杰明·富兰克林奖章获得者.md "wikilink")
[Category:查尔斯·斯塔克·德雷珀奖获得者](../Category/查尔斯·斯塔克·德雷珀奖获得者.md "wikilink")

1.
2.  ["Lithium Ion Battery Pioneers Receive Draper Prize, Engineering’s
    Top
    Honor"](http://www.utexas.edu/news/2014/01/06/goodenough-wins-highest-engineering-honor/),
    University of Texas, 6 January 2014
3.  K. Mizushima, P.C. Jones, P.J. Wiseman, J.B. Goodenough, LixCoO2
    (0\<x\<-1): A new cathode material for batteries of high energy
    density, Materials Research Bulletin, 15 (6), Jun 1980, 783-789.
4.  欧州特許 EP17400B1, J. B. Goodenough, K. Mizushima, P. J. Wiseman.
5.  米国特許4357215 J.B. Goodenough and K. Mizushima.
6.
7.
8.  Voelcker, John (September 2007). [Lithium Batteries Take to the
    Road](http://www.spectrum.ieee.org/sep07/5490/2) . IEEE Spectrum.
    Retrieved 15 June 2010.
9.
10.
11.