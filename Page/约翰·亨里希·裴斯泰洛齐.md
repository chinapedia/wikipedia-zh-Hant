**约翰·海因里希·裴斯泰洛齐**（**Johann Heinrich
Pestalozzi**，）是[瑞士](../Page/瑞士.md "wikilink")[教育家和教育改革家](../Page/教育家.md "wikilink")。
要素教育思想的主要代表人物，被尊为西方“教圣”、欧洲“平民教育之父”。

## 生平

1746年1月12日，裴斯泰洛齐出生在[苏黎世的一个医生家庭](../Page/苏黎世.md "wikilink")。父亲早逝，由寡母和女仆巴贝丽抚育长大。在[苏黎世大学他结交了拉瓦特尔和改革党](../Page/苏黎世大学.md "wikilink")。他的早年投身于提高人民生活条件的计划。他朋友布伦奇利的死使他从政治转向献身教育。他二十三岁结婚，在新庄买了一块废地，试图种植[茜草](../Page/茜草.md "wikilink")。裴斯泰洛齐丝毫不懂商业，计划失败。

在他开放农场办学校之前；写了《隐士的黄昏》（1780年），
接下来又完成了代表作，《林哈德和葛笃德》（1781年），描述一个善良而专心的女人所作渐进的改革，先是一家，然后是全村。这本书在[德国广受欢迎](../Page/德国.md "wikilink")，裴斯泰洛齐的名字不再默默无闻。

1775年在新莊(New hof)自費設立貧民學校，收容孤兒50餘人。
1798年法国侵略瑞士，显示出他真正的英雄品格。许多孩童离开翁特瓦尔登州没有父母、家、食物和住处，裴斯泰洛齐於詩塘(Starz)一个荒废的修道院成立孤兒院，收容孤兒尽力教养他们。在冬季他亲自用极大的热诚照料他们，但1799年6月该建筑被法国人征作医院，他的控诉遭到驱散。

1801年，裴斯泰洛齐写了《葛笃德如何教育她的子女》一书，介绍他的教育理念。他的方法是从易到难。开始于观察，然后是知觉、讲述，再然后是测量、绘画、写作、数字和计算。
1799年他在[布格多夫建立一所学校](../Page/布格多夫.md "wikilink")，直到1804年。1802年，他受托前往
[巴黎](../Page/巴黎.md "wikilink")，尽最大努力使[拿破仑关心国民教育体系](../Page/拿破仑.md "wikilink")；但这位伟大的征服者说他不会为字母自找麻烦。

1805年，他搬到[纳沙泰尔湖边的](../Page/纳沙泰尔湖.md "wikilink")[伊韦尔东](../Page/伊韦尔东.md "wikilink")，二十年之久按部就班地完成他的工作。所有对教育感兴趣的人士参观。他受到[洪堡和](../Page/洪堡.md "wikilink")[费希特的赞扬](../Page/费希特.md "wikilink")。他的学生包括[亚兰·卡甸](../Page/亚兰·卡甸.md "wikilink")、[冉绍尔](../Page/冉绍尔.md "wikilink")、[德尔布吕克](../Page/德尔布吕克.md "wikilink")、[布洛赫曼](../Page/布洛赫曼.md "wikilink")、[李特尔](../Page/李特尔.md "wikilink")、[福禄贝尔和](../Page/福禄贝尔.md "wikilink")[泽勒](../Page/泽勒.md "wikilink")。

大约1815年学校教师中爆发了纠纷，裴斯泰洛齐的最后十年是疲惫和悲伤交替。1825年他回到出生地新庄；完成他最后的著作《天鹅之歌》后，他死于[布鲁克](../Page/布鲁克.md "wikilink")。正如他所说，他一生真正的作品不是在布格多夫或伊韦尔东，而是他实践的教育原则，发展观察力，全人训练，以同情的方式对待学生，他曾在斯坦兹劳动六个月。他对教育的几乎所有分支都有极其深远的影响，他的影响力还远远没有用尽。

被稱為平民教育之父。

死後墓誌銘被尊稱「人中之神，神中之人」。

裴斯泰洛齐全集1819年在[斯图加特出版](../Page/斯图加特.md "wikilink")。

## 参考文献

  - Biber, George Eduard. *Henry Pestalozzi and his Plan of Education*.
    Orig. pub. London: John Souter, School Library, 1831. Repub. ISBN
    1-85506-272-0. Among the earliest and probably the most influential
    19th-century account of Pestalozzi's work in English, this was
    widely read in America (for instance, by [Bronson
    Alcott](../Page/Bronson_Alcott.md "wikilink") and [Ralph Waldo
    Emerson](../Page/Ralph_Waldo_Emerson.md "wikilink")) and in England.
    Contains translated excerpts from many of Pestalozzi's works.

  - Silber, Kate. *Pestalozzi: The Man and his Work*. London: Routledge
    and Kegan Paul, 1960. ISBN 0-7100-2118-6. Written by a
    German-speaking lifelong Pestalozzi scholar, this remains the most
    recent complete biography in English.

  -
[Category:1746年出生](../Category/1746年出生.md "wikilink")
[Category:1827年逝世](../Category/1827年逝世.md "wikilink")
[Category:瑞士教育學家](../Category/瑞士教育學家.md "wikilink")
[Category:苏黎世大学校友](../Category/苏黎世大学校友.md "wikilink")
[Category:瑞士新教徒](../Category/瑞士新教徒.md "wikilink")