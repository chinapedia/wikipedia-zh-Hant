**亨德里克·约翰内斯·“约翰”·克鲁伊夫**（， (,
），荷蘭著名足球員及足球教練。是世界球壇中的傳奇巨星之一。他在2004年票選[最偉大的荷蘭人當中](../Page/最偉大的荷蘭人.md "wikilink")，排名第六，次於排名第五的[伊拉斯謨](../Page/伊拉斯謨.md "wikilink")。

2016年3月24日因[肺癌逝世](../Page/肺癌.md "wikilink")，終年68歲。

## 生涯

### 球員時代

球員時代的克魯伊夫出身於荷蘭著名球會阿賈克斯，司職中鋒，因搶截積極，盤帶技術皆是頂級水準，速度快，故有「飛人」稱號。

他是荷蘭[全能足球的代表人物](../Page/全能足球.md "wikilink")，曾三次奪得[歐洲足球先生](../Page/歐洲足球先生.md "wikilink")（1971年、1973年、1974年）。在[阿賈克斯時已三次率队夺得](../Page/阿賈克斯.md "wikilink")[歐洲聯賽冠軍杯](../Page/欧洲俱乐部冠军杯.md "wikilink")，其中1972年一屆決賽由阿賈克斯對[意甲的](../Page/意甲.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")，便憑克魯伊夫的兩個入球，以2-0贏出。當時外界都認為荷蘭的全能足球戰勝了意大利的防守足球。

1974年荷蘭國家隊教練[-{zh-hans:米歇尔斯;
zh-hk:米高斯;}-挑選克魯伊夫出戰](../Page/里努斯·米歇尔斯.md "wikilink")[世界杯](../Page/世界杯足球賽.md "wikilink")。荷蘭隊擊敗多個強勁對手後進入決賽，最後1:2不敵東道主聯邦德國，獲亞軍。之前一年（1973年），克魯伊夫已轉會至加泰隆尼亞的[巴塞罗那](../Page/巴塞罗那俱乐部.md "wikilink")，協助巴塞隆拿贏得當季的[西甲聯賽冠軍](../Page/西甲.md "wikilink")。而克魯伊夫的助陣，也為阿賈克斯球員在巴塞羅那奠定地位，以後有不少阿賈克斯球員都效力過巴塞羅那，如[羅納德·科曼](../Page/羅納德·科曼.md "wikilink")、[克鲁伊维特等等](../Page/克鲁伊维特.md "wikilink")。

克鲁伊夫早於1978年退出荷蘭國家隊，1984年宣佈退役。他為國家隊出賽48場，共取得33個入球。

### 執教生涯

克魯伊夫在退出球員生涯後成為了教練，先在1986－1988年間執教母會[阿贾克斯](../Page/阿贾克斯.md "wikilink")。而他亦與巴塞羅那相當有緣，自1988年起便執教巴塞羅那。自1991年起，克魯伊夫帶領巴塞羅那連贏四屆[西甲冠軍](../Page/西甲.md "wikilink")，並在1992年率隊贏得球會史上第一個[歐冠杯](../Page/歐冠杯.md "wikilink")。當時的巴塞遂被稱為「夢之隊」，即使到近年，不少巴塞球迷仍相當尊敬克魯伊夫，認為他是球會歷來最出色教頭之一。

克魯伊夫離開巴塞源於與當時主席努涅斯不和，曾宣稱在努涅斯当政期间绝不回球會，不过同时也宣称永不执教其他球队。
隨後努涅斯下台，由加斯帕特接任主席，但當政間巴塞一度淪落至只能打[联盟杯的地步](../Page/联盟杯.md "wikilink")，而其主席职位亦在选举中失去。得到克鲁伊夫支持的[拉波尔塔在此次选举中获胜](../Page/祖安·拿樸達.md "wikilink")，担任巴萨主席一职。

此後克魯伊夫虽未直接回到巴塞，然而拉波尔塔在任期间诸多重要决策背后均能看到其影子，比如两任主教练[弗兰克·里杰卡尔德和](../Page/弗兰克·里杰卡尔德.md "wikilink")[何塞普·瓜迪奥拉的上任](../Page/何塞普·瓜迪奥拉.md "wikilink")。此二人上任皆出乎当时一般舆论预料之外，不过；前者获得了巴塞史上第二座欧冠奖杯，而后者則获得了巴塞史上第三座和第四座欧冠奖杯。

克魯伊夫对巴塞的影响力并不仅如此。近年來巴塞阵容中有許多球员，诸如[梅西](../Page/利昂内尔·梅西.md "wikilink")、[哈維](../Page/哈維·埃爾南德斯.md "wikilink")、[伊涅斯塔](../Page/伊涅斯塔.md "wikilink")、[普約爾](../Page/普約爾.md "wikilink")、[域陀·華迪斯](../Page/域陀·華迪斯.md "wikilink")、[布斯克茨](../Page/塞爾希奧·布斯克茨.md "wikilink")、[佩德羅均是出自其自身的青训体系](../Page/佩德羅·羅德里格斯·雷德斯馬.md "wikilink")，而这个体系正式由克魯伊夫于二十年前奠下根基。此外，克鲁伊夫当时所带来的433控球进攻的打法，亦一直持续到了现在。诚如巴塞一代中场核心哈维所言，巴塞的打法自二十年前克鲁伊夫之后便再未有大的变更。这套打法不仅被巴塞成年队所执行，亦在巴塞各级青年队当中推行，这也使得巴塞青年队每名球员卒上一队便能迅速适应打法融入球队。而这套打法推行的20多年间，巴塞获得了11次联赛冠军，和4次欧洲冠军杯冠军。其成功之处可见一斑。

克魯伊夫的兒子[祖迪·告魯夫亦是足球員](../Page/祖迪·告魯夫.md "wikilink")，曾經在多間大球會包括巴塞隆拿、[曼聯及](../Page/曼聯.md "wikilink")[阿拉維斯效力](../Page/阿拉維斯足球俱樂部.md "wikilink")。在巴塞隆拿時，克魯伊夫是其教練。

## 所屬球會

[Johan_Cruijff_golfer_cropped.jpg](https://zh.wikipedia.org/wiki/File:Johan_Cruijff_golfer_cropped.jpg "fig:Johan_Cruijff_golfer_cropped.jpg")以外，[高爾夫球是克魯伊夫最喜愛的運動](../Page/高爾夫球.md "wikilink")\]\]

  - 1964年—1973年：阿賈克斯（荷蘭）
  - 1973年—1978年：巴塞隆拿（加泰隆尼亞）
  - 1979年：[洛杉磯阿茲特克](../Page/洛杉磯阿茲特克.md "wikilink")（Los Angeles
    Aztecs）（[美國](../Page/美國.md "wikilink")）
  - 1980年—1981年：[華盛頓外交官隊](../Page/華盛頓外交官隊.md "wikilink")（Washington
    Diplomats）（美國）
  - 1981年：[利雲特](../Page/利雲特.md "wikilink")（西班牙）
  - 1981年—1983年：阿賈克斯（荷蘭）
  - 1983年—1984年：[费耶诺德](../Page/费耶诺德.md "wikilink")（荷蘭）

## 外部連結

  - [經典告魯夫—運動鞋品牌](http://www.cruyffclassics.com/)
  - [UEFA.com：荷蘭金童](http://www.uefa.com/uefa/history/associationweeks/association=95/newsId=215769.html)

[Category:1974年世界盃足球賽球員](../Category/1974年世界盃足球賽球員.md "wikilink")
[Category:1976年歐洲國家盃球員](../Category/1976年歐洲國家盃球員.md "wikilink")
[Category:荷兰足球运动员](../Category/荷兰足球运动员.md "wikilink")
[Category:阿積士球員](../Category/阿積士球員.md "wikilink")
[Category:巴塞隆拿球員](../Category/巴塞隆拿球員.md "wikilink")
[Category:利雲特球員](../Category/利雲特球員.md "wikilink")
[Category:飛燕諾球員](../Category/飛燕諾球員.md "wikilink")
[Category:荷蘭足球主教練](../Category/荷蘭足球主教練.md "wikilink")
[Category:阿積士主教練](../Category/阿積士主教練.md "wikilink")
[Category:巴塞隆拿主教練](../Category/巴塞隆拿主教練.md "wikilink")
[Category:西甲主教練](../Category/西甲主教練.md "wikilink") [Category:FIFA
100](../Category/FIFA_100.md "wikilink")
[Category:歐洲足球先生](../Category/歐洲足球先生.md "wikilink")
[Category:阿姆斯特丹人](../Category/阿姆斯特丹人.md "wikilink")
[Category:欧冠冠军主教练](../Category/欧冠冠军主教练.md "wikilink")
[Category:罹患癌症逝世者](../Category/罹患癌症逝世者.md "wikilink")