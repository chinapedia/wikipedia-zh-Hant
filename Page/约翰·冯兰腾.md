**约翰·冯兰腾**（**Johan Vonlanthen
Benavidez**，）是一名[瑞士足球運動員](../Page/瑞士.md "wikilink")，可擔任[前鋒和](../Page/前鋒_\(足球\).md "wikilink")[進攻中場](../Page/中場.md "wikilink")，因嚴重傷病已退役。

2004年6月6日，禾蘭芬首次代表[瑞士國家足球隊對](../Page/瑞士國家足球隊.md "wikilink")[列支敦士登](../Page/列支敦士登國家足球隊.md "wikilink")，他於81分鐘後備入替[亞歷山大·費爾](../Page/亞歷山大·費爾.md "wikilink")。[2004年歐洲國家盃](../Page/2004年歐洲國家盃.md "wikilink")，禾蘭芬以18歲之齡參加，是賽事中其中一個最年輕的球員。2004年6月21日，禾蘭芬成為賽事中最年輕的進球者，打破了4日前由[朗尼所創的紀錄](../Page/朗尼.md "wikilink")\[1\]。

## 參考資料

## 外部連結

  - [禾蘭芬官方網頁](http://www.johanvonlanthen.ch/)
  - [News about Vonlanthen in the Swiss
    Newspapers](http://www.pressetrends.com/s/johann-vonlanthen)

[Category:哥倫比亞足球運動員](../Category/哥倫比亞足球運動員.md "wikilink")
[Category:瑞士足球運動員](../Category/瑞士足球運動員.md "wikilink")
[Category:PSV燕豪芬球員](../Category/PSV燕豪芬球員.md "wikilink")
[Category:年輕人球員](../Category/年輕人球員.md "wikilink")
[Category:布雷西亞球員](../Category/布雷西亞球員.md "wikilink")
[Category:NAC比達球員](../Category/NAC比達球員.md "wikilink")
[Category:薩爾斯堡紅牛球員](../Category/薩爾斯堡紅牛球員.md "wikilink")
[Category:蘇黎世球員](../Category/蘇黎世球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:奧超球員](../Category/奧超球員.md "wikilink")
[Category:瑞士超聯球員](../Category/瑞士超聯球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")

1.  <http://www.guardian.co.uk/football/2008/may/27/switzerland.euro2008groupa?gusrc=rss&feed=football>