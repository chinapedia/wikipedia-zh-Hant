**约翰·海密许·华生**醫生（，1852年7月7日-？），是[阿瑟·柯南·道尔爵士笔下](../Page/阿瑟·柯南·道尔.md "wikilink")《[福尔摩斯探案集](../Page/福尔摩斯探案.md "wikilink")》中的[虚构人物](../Page/虚构人物.md "wikilink")，書中資料顯示他生於1852年7月7日。

華生曾以[英國陸軍](../Page/英國陸軍.md "wikilink")[軍醫的身分派往](../Page/軍醫.md "wikilink")[阿富汗作戰](../Page/阿富汗.md "wikilink")，後來因傷[退役](../Page/退役.md "wikilink")，軍階為[上尉](../Page/上尉.md "wikilink")。华生在结婚前一直与[福尔摩斯合租](../Page/歇洛克·福尔摩斯.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")[贝克街](../Page/贝克街.md "wikilink")[221号B的房子](../Page/貝克街221號B.md "wikilink")；而华生與[瑪麗·摩斯坦结婚后搬離貝克街](../Page/瑪麗·摩斯坦.md "wikilink")，成立[私人診所開業](../Page/私人診所.md "wikilink")。华生不仅是福尔摩斯的助手，还是福尔摩斯[破案过程的记录者](../Page/破案.md "wikilink")（福尔摩斯的“[包斯威尔](../Page/詹姆士·包斯威爾.md "wikilink")”）。几乎所有的福尔摩斯故事都是由华生叙述的；不过福尔摩斯经常批评华生以[小说手法描写破案过程](../Page/小说.md "wikilink")，而不是客观科学地呈现事实。

约翰·华生被多部电影、电视、广播和视频游戏频繁诠释，飾演过該角的演员包括、、、[-{zh-hans:裘德·洛;zh-tw:裘德·洛;zh-hk:祖迪·羅;}-以及](../Page/裘德·洛.md "wikilink")[马丁·弗里曼](../Page/马丁·弗里曼.md "wikilink")。

## 生平

约翰·华生於1852年出生。1878年获得[伦敦大学](../Page/伦敦大学.md "wikilink")[医学博士学位](../Page/医学博士.md "wikilink")。接受军医訓練，结訓後派往[阿富汗擔任随军医生](../Page/阿富汗.md "wikilink")。

1880年华生因肩傷從軍中退伍，回到伦敦後，在朋友史坦福介紹下结识了夏洛克·福尔摩斯。兩人合租貝克街221號B，[房東是](../Page/房東.md "wikilink")[哈德森太太](../Page/哈德森太太.md "wikilink")。

1889年，华生在《[四簽名](../Page/四簽名.md "wikilink")》一案中與[瑪麗·摩斯坦相識並結婚](../Page/瑪麗·摩斯坦.md "wikilink")，搬離貝克街。

## 改編作品

  - 《[名偵探福爾摩斯](../Page/名偵探福爾摩斯.md "wikilink")》，配音員：[富田耕生](../Page/富田耕生.md "wikilink")。
  - 《[-{zh-hk:神探福爾摩斯; zh-tw:福爾摩斯;
    zh-cn:大侦探福尔摩斯;}-](../Page/福爾摩斯_\(2009年電影\).md "wikilink")》及《[-{zh-hk:神探福爾摩斯：詭影遊戲;
    zh-tw:福爾摩斯：詭影遊戲;
    zh-cn:大侦探福尔摩斯2：诡影游戏;}-](../Page/福爾摩斯：詭影遊戲.md "wikilink")》，演員：[-{zh-hans:裘德·洛;zh-tw:裘德·洛;zh-hk:祖迪·羅;}-](../Page/裘德·洛.md "wikilink")。
  - 《[-{zh-cn:神探夏洛克;zh-tw:新世紀福爾摩斯;zh-hk:新福爾摩斯;}-](../Page/新世紀福爾摩斯.md "wikilink")》電視劇，演員：[-{zh-cn:马丁·弗里曼;
    zh-tw:馬丁·費里曼; zh-hk:馬田·費曼;}-](../Page/馬丁·費里曼.md "wikilink")。\[1\]
  - 《[-{zh-hans:基本演绎法; zh-tw:福爾摩斯與華生;
    zh-hk:福爾摩斯新傳;}-](../Page/基本演繹法.md "wikilink")》，演員：[劉玉玲](../Page/劉玉玲.md "wikilink")。該片中將華生改為女性\[2\]，因此姓名亦更為瓊安·華生（Joan
    Watson）

## 影響

华生醫生的角色對後續的[推理小說敘事產生了影響](../Page/推理小說.md "wikilink")，如[阿嘉莎·克莉絲蒂筆下的角色](../Page/阿嘉莎·克莉絲蒂.md "wikilink")（[赫丘勒·白羅的夥伴](../Page/赫丘勒·白羅.md "wikilink")）明顯就跟华生醫生十分類似，都需要偵探向他解釋所有的訊息\[3\]。

## 參考資料

<references />

## 外部連結

  - [约翰·华生](http://www.bbc.co.uk/programmes/profiles/3vT16nZ3kntNN2z8ShLphSf/dr-john-watson)在BBC《新世紀福爾摩斯》官網上的頁面
  - [约翰·华生的部落格](http://www.johnwatsonblog.co.uk/)，影集《新世紀福爾摩斯》相關網站

[Category:福爾摩斯人物](../Category/福爾摩斯人物.md "wikilink")
[Category:虛構醫生](../Category/虛構醫生.md "wikilink")
[Category:虛構英國軍人](../Category/虛構英國軍人.md "wikilink")
[Category:虛構英格蘭人](../Category/虛構英格蘭人.md "wikilink")
[Category:虚构神枪手和狙击手](../Category/虚构神枪手和狙击手.md "wikilink")
[Category:虛構作家](../Category/虛構作家.md "wikilink")

1.
2.
3.