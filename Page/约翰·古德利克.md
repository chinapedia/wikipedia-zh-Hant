**约翰·古德利克**（，），英籍荷兰裔天文学家，在[变星领域做出了突出的贡献](../Page/变星.md "wikilink")。

## 生平

古德利克1764年出生于[荷兰](../Page/荷兰.md "wikilink")，幼年时因发烧导致聋哑。他在少年时就对星空产生了深厚的兴趣。1782年冬季，不满20岁的古德利克持续观测了位于[英仙座的变星](../Page/英仙座.md "wikilink")[大陵五](../Page/大陵五.md "wikilink")（英仙座β星），测算出它的光变周期为2天20小时49分8秒，并且提出了大陵五的光变机理，认为大陵五是由一亮一暗两颗恒星组成的双星系统，两颗星相互环绕运转，当两颗星相互遮掩时，总体亮度就变暗。1888年德国天文学家[沃格耳使用分光法证实了古德利克这个设想](../Page/沃格耳.md "wikilink")。这种双星现在叫做[食双星](../Page/食双星.md "wikilink")，或食双星。古德利克1783年5月向[英国皇家学会报告了他的理论](../Page/英国皇家学会.md "wikilink")，并因此获得英国皇家学会的科普利奖章。

接下来的两年里，古德利克又先后发现两颗变星：[天琴座β](../Page/天琴座β.md "wikilink")（中文名渐台二）和[仙王座δ](../Page/仙王座δ.md "wikilink")（中文名造父一）。古德利克正确地指出仙王座δ星是一颗本身光度就在变化的真正意义上的变星。以仙王座δ星为代表的一大类恒星称为造父变星。

1786年，古德利克当选英国皇家学会会员。两个星期后，未满22岁古德利克因过度劳累去世。为纪念这位聋哑天文学家，古德利克去世后[约克大学的](../Page/約克大學_\(英國\).md "wikilink")[古德利克学院以他的名字命名](../Page/古德利克学院.md "wikilink")。

## 参阅

  - [食双星](../Page/食双星.md "wikilink")
  - [造父变星](../Page/造父变星.md "wikilink")

[分类:英国天文学家](../Page/分类:英国天文学家.md "wikilink")

[Category:18世纪英国人](../Category/18世纪英国人.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")