**約翰·巴比羅利**爵士，[CH](../Page/名譽勳位.md "wikilink")（，），[英國](../Page/英國.md "wikilink")[指揮家和](../Page/指揮家.md "wikilink")[大提琴演奏家](../Page/大提琴.md "wikilink")，曾領導[倫敦交響管弦樂團和](../Page/倫敦交響管弦樂團.md "wikilink")[倫敦愛樂管弦樂團](../Page/倫敦愛樂管弦樂團.md "wikilink")，還有許多別的[管弦樂團](../Page/管弦樂團.md "wikilink")。

## 早年（1899年─1937年）

巴比羅利出生在倫敦的音樂家庭，父親和叔叔都在萊斯特廣場帝國劇院(Leicester Square
Empire)等歌劇院的交響樂團當[小提琴手](../Page/小提琴.md "wikilink")，也曾經由指揮[托斯卡尼尼帶領在](../Page/托斯卡尼尼.md "wikilink")[米蘭的斯卡拉劇院](../Page/米蘭.md "wikilink")(La
Scala)演出。年輕的巴比羅利，就這樣注定了要成為提琴手，英國音樂專家，和意大利歌劇愛好者。

他在倫敦交響管弦樂團作年輕的大提琴手時，曾製作了一些錄音，其中主要是[艾爾加的大提琴協奏曲的第一場演出](../Page/艾爾加.md "wikilink")，以及不久後他作獨奏的同一作品的第二場演出。1920年代，他轉向指揮工作，成立了一支室內管弦樂團，為[國家留聲機協會](../Page/國家留聲機協會.md "wikilink")（National
Gramophone Society）錄製新作品，包括艾爾加的Introduction and
Allegro，這可能使[HMV在艾爾加逝世前避開這首作品](../Page/HMV.md "wikilink")。

巴比羅利為人熟知，因為他能在短時間的通知內準備，確保演出水準。1930年代，他與[倫敦交響管弦樂團和](../Page/倫敦交響管弦樂團.md "wikilink")[倫敦愛樂管弦樂團錄製很多作品](../Page/倫敦愛樂管弦樂團.md "wikilink")，並且和傑出獨奏家如Fritz
Kreisler、Heifetz和Rubinstein合作，指揮樂團伴奏協奏曲。他的很多演出錄音至今仍是經典。

## 紐約愛樂指揮（1937年─1942年）

1937年巴比羅利的事業有成功突破，他獲得邀請接替托斯卡尼尼為[紐約愛樂管弦樂團的指揮](../Page/紐約愛樂管弦樂團.md "wikilink")，當上了這極高榮譽的職位。他在五個樂季的指揮，從現存的錄音看是音樂的成就。話雖如此，他卻持續受到紐約尖銳的媒體批評，尤其是討厭英國的樂評人[Olin
Downes](../Page/Olin_Downes.md "wikilink")。

## 後期的工作（1942年─1970年）

1942年巴比羅利受邀續約，但如此他就要入籍美國，可是他不想入籍。這時他接到邀請擔任[曼徹斯特的哈雷管弦樂團](../Page/曼徹斯特.md "wikilink")(Hallé
Orchestra)的主指揮，從此改變了他的事業。

哈雷管弦樂團一度安排與BBC共用樂手，挽救他們度過不景氣時代。可是隨著他們的音樂會演出範圍趨向多樣，這安排顯得不適合而須終止，同時也要聘請一位頂尖指揮家。只有4位前共用樂手選擇加入哈雷管弦樂團，因此巴比羅利新上任，就要在數星期內重整樂團。他熱心投入這份任務。他的「新哈雷」可以在戰時曼徹斯特灌錄的[阿诺德·巴克斯和](../Page/阿诺德·巴克斯.md "wikilink")[沃恩-威廉斯的交響樂錄音聽到](../Page/沃恩-威廉斯.md "wikilink")。

巴比羅利指揮樂團達25年，在許多城市演出，也於[切爾滕納姆節](../Page/切爾滕納姆節.md "wikilink")(Cheltenham
Festival)首演了許多新作。他也指揮BBC和其他倫敦交響樂團的音樂會和灌錄工作。他晚年時他更新了與[EMI的聯繫](../Page/EMI.md "wikilink")，為他發行了一個時代的高品質演出錄音，其中有很多一直保持發行。

巴比羅利詮釋艾爾加、[沃恩-威廉斯和](../Page/沃恩-威廉斯.md "wikilink")[馬勒很著名](../Page/古斯塔夫·馬勒.md "wikilink")，另外他也詮釋了[舒伯特](../Page/舒伯特.md "wikilink")、[貝多芬](../Page/貝多芬.md "wikilink")、[威爾第和](../Page/威爾第.md "wikilink")[普契尼的作品](../Page/普契尼.md "wikilink")。他也堅定支持英國作曲家的新作品，他對此的提倡可以媲美[Adrian
Boult和](../Page/Adrian_Boult.md "wikilink")[Henry
Wood](../Page/Henry_Wood.md "wikilink")。

## 傳記

Reid, Charles "John Barbirolli: a biography" Taplinger Pub. Co
(1971)ISBN 0800844084

## 參考资料

  - [BBC Profile of Sir John
    Barbirolli](https://web.archive.org/web/20050318021707/http://www.bbc.co.uk/music/profiles/barbirolli.shtml)
  - [Encyclopedia.com Brief
    Biography](https://web.archive.org/web/20050317035211/http://www.encyclopedia.com/html/B/Barbirol.asp)

## 外部链接

  - [Sir John Barbiorlli Memorial
    Foundation](http://www.royalphilharmonicsociety.org.uk/johnbarbirolli.htm)

[Category:英国指挥家](../Category/英国指挥家.md "wikilink")
[Category:英國音樂家](../Category/英國音樂家.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:名譽勳位成員](../Category/名譽勳位成員.md "wikilink")