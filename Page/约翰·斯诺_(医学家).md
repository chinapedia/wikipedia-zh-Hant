**约翰·斯诺**（，），[英国](../Page/英国.md "wikilink")[内科医生](../Page/内科.md "wikilink")，因在[1854年宽街霍乱爆发事件研究中作出重大贡献](../Page/1854年宽街霍乱爆发事件.md "wikilink")，被认为是[麻醉学和](../Page/麻醉学.md "wikilink")[公共卫生医学的开拓者](../Page/公共卫生医学.md "wikilink")。

## 早年生活与教育

斯诺生于1813年3月15日，[英格兰](../Page/英格兰.md "wikilink")[约克郡](../Page/约克郡.md "wikilink")，为威廉和弗兰西斯·斯诺的长子。斯诺早年生活在约克郡最穷的地方——北街（North
Street）。因为临近[乌兹河](../Page/乌兹河.md "wikilink")，那里经常面临着水浸的危险。斯诺的父亲在当地的一个煤矿工作。斯诺小时候便在约克郡一个中世纪的教堂接受施洗。
[allsaintsnorthstreetyork.jpg](https://zh.wikipedia.org/wiki/File:allsaintsnorthstreetyork.jpg "fig:allsaintsnorthstreetyork.jpg")
14岁的时候，斯诺到William Hardcastle当徒弟，William Hardcastle是斯诺叔叔Charles
Empson的朋友，一个著名的[内科医生](../Page/内科.md "wikilink")。而Charles
Empsom和[罗伯特 史蒂芬孙](../Page/罗伯特_史蒂芬孙.md "wikilink")（Robert
Stephenson）是同学，正是这种关系，斯诺获得了离开家乡很远的这份学徒工作。接下来在1833年到1836年，斯诺不断到煤矿当实习医生，先在[Burnopfield、](../Page/:en:Burnopfield.md "wikilink")[达拉谟](../Page/达拉谟.md "wikilink")，再在
[Pateley
Bridge](../Page/:en:Pateley_Bridge.md "wikilink")。1836年10月，斯诺到伦敦Hunterian
school of medicine in Great Windmill
Street学习医学。一年后，他开始在[西敏市医院](../Page/西敏市医院.md "wikilink")（
[Westminster Hospital](../Page/:en:Westminster_Hospital.md "wikilink")）
工作，并且在来年5月2日成为
[英国伦敦皇家外科医学院的成员](../Page/英国伦敦皇家外科医学院.md "wikilink")。经过努力，他1844年12月毕业于[伦敦大学](../Page/伦敦大学.md "wikilink")。1850年又被吸收为[英国伦敦皇家内科医学院成员](../Page/英国伦敦皇家内科医学院.md "wikilink")。

## 对麻醉学的研究

约翰·斯诺是第一个研究计算[麻醉药](../Page/麻醉药.md "wikilink")——[乙醚用量的医学家](../Page/乙醚.md "wikilink")。他还发现，[氯仿可以用作](../Page/氯仿.md "wikilink")[麻醉剂](../Page/麻醉剂.md "wikilink")。当[维多利亚女王在](../Page/维多利亚女王.md "wikilink")1853和1857生產她最后两个孩子[利奥波德王子和](../Page/利奥波德王子_\(奥尔巴尼公爵\).md "wikilink")[貝翠絲公主的时候](../Page/貝翠絲公主_\(英國\).md "wikilink")，斯诺大胆地将[氯仿用于](../Page/氯仿.md "wikilink")[维多利亚女王身上以减轻她生孩子时的痛苦](../Page/维多利亚女王.md "wikilink")，\[1\]这获得了大众对麻醉药广泛的认可。斯诺于1847年发表了一篇关于乙醚的论文——《关于对乙醚蒸汽吸入研究》（On
the Inhalation of the Vapor of
Ether），更多的研究论文在他死后发表——1858的《关于氯仿和其他麻醉剂药效和施用研究》（On
Chloroform and Other Anaesthetics, and Their Action and
Administration*.*）

## 对霍乱的研究

[Snow-cholera-map-1.jpg](https://zh.wikipedia.org/wiki/File:Snow-cholera-map-1.jpg "fig:Snow-cholera-map-1.jpg")
在斯诺生活的年代，对霍乱的起因的主流意见是空气污染论（认为霍乱像[黑死病一样通过空气传播](../Page/黑死病.md "wikilink")）。另一方意见是未被广泛接受病菌学说。斯诺并不清楚究竟霍乱是通过哪种途径传播的，但是，经过1832年斯诺与巴尼澳霍乱研究提出的证据，使他相信[霍乱的传播并不归咎于吸入了被](../Page/霍乱.md "wikilink")“污染”的空气。而是因严重的水污染病菌传播。他1849年开始通过他的论文——《霍乱传递方式研究》（On
the Mode of Communication of
Cholera）宣传他的理论，并于1855年出版了第二版。在第二版中，他详细介绍了1854年英国伦敦西敏市霍乱爆发时水源在病菌传播中所起的媒介作用。他通过与当地居民交流和仔细分析，将污染源锁定在布劳德大街（现[布劳维克大街](../Page/:en:Broadwick_Street.md "wikilink")）的公用[抽水机上](../Page/抽水机.md "wikilink")。虽然通过[化学分析及](../Page/化学.md "wikilink")[显微镜观察](../Page/显微镜.md "wikilink")[抽水机水源](../Page/抽水机.md "wikilink")[样本并未得出确凿的结论](../Page/样本.md "wikilink")，但是他对[霍乱传播方式研究却足以令人信服](../Page/霍乱.md "wikilink")，并成功说服当地市政将抽水机手柄移走。各大报纸将其视为霍乱病的终结，然而根据斯诺自己的解释，霍乱發病率在此前可能已经大幅度下降。接下来，斯诺使用一张地图来阐明霍乱是如何集中于抽水机旁的。他同时将[统计学应用于其对于水质和霍乱个案联系的研究中](../Page/统计学.md "wikilink")。他表示，正是因为公司从被污染的[泰晤士河部份取水](../Page/泰晤士河.md "wikilink")，导致了霍乱發病率的提高。斯诺的研究可以说是[公共卫生学历史上一重大事件](../Page/公共卫生学.md "wikilink")。

## 政治上的论战

在霍乱病魔逐渐被制服之时，政府却将[布劳德大街](../Page/布劳德大街.md "wikilink")[抽水机的手柄重新安装上](../Page/抽水机.md "wikilink")。他们装腔作势，并且开始反对斯诺关于霍乱病传播的理论，这在大众中引起不满。

今天主管[公共卫生的官员意识到](../Page/公共卫生.md "wikilink")，改革者经常卷入政治斗争。在一年一度的关于抽水机手柄讲座演讲举行期间，[约翰斯诺协会的成员们将一个抽水机手柄移走再重新安装上](../Page/约翰斯诺协会.md "wikilink")，以表当涉及公共卫生时的不断挑战。

## 晚年生活

斯诺一生并未结婚，是一个[素食主义者和](../Page/素食主义.md "wikilink")[禁酒主义者](../Page/禁酒主义.md "wikilink")。他一生相信饮用经过煮沸的[纯水能延年益寿](../Page/纯水.md "wikilink")。

斯诺在1858年6月10日工作时，经历了一场大病。\[2\]他并没有康复过来，于1858年6月16日与世长辞，葬于
[布朗普顿公墓](../Page/布朗普顿公墓.md "wikilink")。\[3\]

## 纪念

[JohnSnowBrompton.jpg](https://zh.wikipedia.org/wiki/File:JohnSnowBrompton.jpg "fig:JohnSnowBrompton.jpg")
[John_Snow_memorial_and_pub.jpg](https://zh.wikipedia.org/wiki/File:John_Snow_memorial_and_pub.jpg "fig:John_Snow_memorial_and_pub.jpg")

现在，在原布劳德大街（现布劳维克大街）安装[抽水机的地方](../Page/抽水机.md "wikilink")，有一块纪念约翰·斯诺与他在1854年对[霍乱研究的牌匾](../Page/霍乱.md "wikilink")，在一起的还有一个被移走了手柄的抽水机，旁边镶嵌有红色的[花岗石](../Page/花岗石.md "wikilink")。以约翰·斯诺命名的、隶属于[杜伦大学的](../Page/杜伦大学.md "wikilink")[约翰斯诺学院成立於](../Page/约翰斯诺学院.md "wikilink")2001年。而在2003年举行的调查中，约翰·斯诺被选为[英国历史上最伟大的](../Page/英国历史.md "wikilink")[内科医生](../Page/内科.md "wikilink")。

## 参见

  - [倫敦蘇豪區](../Page/倫敦蘇豪區.md "wikilink")
  - [霍乱](../Page/霍乱.md "wikilink")
  - [1854年宽街霍乱爆发事件](../Page/1854年宽街霍乱爆发事件.md "wikilink")

## 参考资料

<small>

  - Peter Vinten-Johansen *et al.*, *Cholera, Chloroform, and the
    Science of Medicine: A Life of John Snow*. OUP, 2003. ISBN
    978-0-19-513544-2
  - Edward Tufte, *Visual Explanations*, chapter 2. Graphics Press,
    1997. ISBN 978-0-9613921-2-3
  - T. W. Körner, *The Pleasures of Counting*, chapter 1. CUP 1996. ISBN
    978-0-521-56823-4
  - Steven Berlin Johnson, *The Ghost Map: The Story of London's Most
    Terrifying Epidemic - and How it Changed Science, Cities and the
    Modern World* (2006) ISBN 978-1-59448-925-9
  - Shapin, Steven. (2006, November 6) Electronic version. [Sick City:
    Maps and mortality in the time of
    cholera](http://www.newyorker.com/printables/critics/061106crbo_books).
    The New Yorker. Retrieved November 10, 2006.
  - Dr. Robert D. Morris, *The Blue Death*, Harper Collins, 2007. ISBN
    978-0-06-073089-5</small>

## 外部链接

  - [UCLA site devoted to the life of Dr. John
    Snow](http://www.ph.ucla.edu/epi/snow.html)
  - [Myth and reality regarding the Broad Street
    pump](http://www.ph.ucla.edu/epi/snow/mapmyth/mapmyth.html)
  - [John Snow Society](http://www.johnsnowsociety.org/)
  - [Source for Snow's letter to the Editor of the Medical Times and
    Gazette](http://www.ph.ucla.edu/epi/snow/choleragoldensquare.html)

[Category:英国医学家](../Category/英国医学家.md "wikilink")
[Category:麻醉师](../Category/麻醉师.md "wikilink")

1.
2.
3.