**约翰·海斯伯特·阿兰·海廷加**（，），前[荷蘭職業足球員](../Page/荷蘭.md "wikilink")，司職[後衛](../Page/後衛_\(足球\).md "wikilink")，職業生涯有一半以上時間效力[阿積士](../Page/阿積士.md "wikilink")。

## 生平

### 球會

海廷加是[阿積士青訓人物之一](../Page/阿積士.md "wikilink")，早於2001年新球季開始時已登場，起先上陣15場。其後因傷患休養達半年，至2003-04年球季才奠定正選位置，單在復出第一季已上陣26場。海廷加除了可司職中堅外也可扮演右閘。他在阿積士的最佳成績，乃2004年[荷甲聯賽冠軍](../Page/荷甲.md "wikilink")。2008年起已轉投[西班牙球會](../Page/西班牙.md "wikilink")[馬德里體育會](../Page/馬德里體育會.md "wikilink")。

在[西甲僅逗留了一季](../Page/西甲.md "wikilink")，於2009年9月1日轉會窗關閉前成功加盟[英超](../Page/英超.md "wikilink")[愛華頓](../Page/埃弗顿足球俱乐部.md "wikilink")，轉會費初步為600萬[英鎊](../Page/英鎊.md "wikilink")，可增加到700萬英鎊，簽約五年\[1\]。

### 國家隊

海廷加早在2004年入選國家隊，首場比賽乃2004年2月18日對陣美國的友誼賽。[2004年歐洲國家杯荷蘭打了五場比賽](../Page/2004年歐洲國家杯.md "wikilink")，他上陣了其中三場。其後荷蘭教練[雲巴士頓執掌國家隊](../Page/雲巴士頓.md "wikilink")，海廷加成為雲巴士頓的麾下人物之一。

海廷加随队参加了[2006年世界杯](../Page/2006年世界杯足球赛.md "wikilink")，但在八分之一决赛中输给[葡萄牙队](../Page/葡萄牙國家足球隊.md "wikilink")，未能进入八强。2010年，海廷加参加了[2010年世界杯](../Page/2010年世界杯足球赛.md "wikilink")，并帮助球队打入了决赛，但在决赛的加时赛部分的第109分钟，其因为累计两张黄牌而被罚下场，荷兰队的大门随后与在第117分钟被[伊涅斯塔攻破](../Page/伊涅斯塔.md "wikilink")，最终荷兰队痛失冠军。

## 榮譽

### [荷兰国家队](../Page/荷兰国家队.md "wikilink")

  - [世界杯亚军](../Page/世界杯足球赛.md "wikilink")：[2010年](../Page/2010年世界杯足球赛.md "wikilink")

### 阿積士

  - [荷甲聯賽冠軍](../Page/荷甲.md "wikilink")：2001/02年、2003/04年；
  - [荷蘭盃](../Page/荷蘭盃.md "wikilink")：2002年、2006年、2007年；
  - [告魯夫盾](../Page/告魯夫盾.md "wikilink")：2003年、2006年、2007年、2008年；

## 參考資料

## 外部連結

  -
  - [Johnny Heitinga - Official
    website](http://www.heitingaofficial.com/)

  - [Profile](http://www.wereldvanoranje.nl/profielen/profiel.php?id=472)

  - [Voetbal International website and 2007/2008 presentation
    magazine](http://www.vi.nl)

[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:荷蘭國家足球隊球員](../Category/荷蘭國家足球隊球員.md "wikilink")
[Category:荷蘭足球運動員](../Category/荷蘭足球運動員.md "wikilink")
[Category:阿積士球員](../Category/阿積士球員.md "wikilink")
[Category:馬德里體育會球員](../Category/馬德里體育會球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:富咸球員](../Category/富咸球員.md "wikilink")
[Category:哈化柏林球員](../Category/哈化柏林球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:荷蘭旅外足球運動員](../Category/荷蘭旅外足球運動員.md "wikilink")
[Category:在英國的荷蘭人](../Category/在英國的荷蘭人.md "wikilink")
[Category:在西班牙的荷蘭人](../Category/在西班牙的荷蘭人.md "wikilink")
[Category:印度尼西亞裔荷蘭人](../Category/印度尼西亞裔荷蘭人.md "wikilink")
[Category:南荷蘭省人](../Category/南荷蘭省人.md "wikilink")

1.