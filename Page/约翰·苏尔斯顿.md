**约翰·愛德華·苏尔斯顿**爵士，[CH](../Page/名譽勳位.md "wikilink")，[FRS](../Page/皇家學會院士.md "wikilink")（，），[英国科学家](../Page/英国.md "wikilink")，因发现器官发育和细胞程序性细胞死亡（[细胞程序化凋亡](../Page/细胞凋亡.md "wikilink")）的遗传调控机理，与[悉尼·布伦纳](../Page/悉尼·布伦纳.md "wikilink")、[H·罗伯特·霍维茨一起获得](../Page/H·罗伯特·霍维茨.md "wikilink")2002年[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。他是[曼彻斯特大学](../Page/曼彻斯特大学.md "wikilink")[科学，伦理与创新研究所的主席](../Page/科学，伦理与创新研究所.md "wikilink")\[1\]\[2\]\[3\]\[4\]\[5\]\[6\]。

苏尔斯顿出生于剑桥\[7\]\[8\]\[9\]\[10\]，父亲是[法政牧師Arthur](../Page/法政牧師.md "wikilink")
Edward Aubrey Sulsto和母亲是Josephine Muriel Frearson (née
Blocksidge)\[11\]\[12\]。

## 参考资料

## 外部链接

  - [诺贝尔官方网站约翰·E·苏尔斯顿自传](http://nobelprize.org/nobel_prizes/medicine/laureates/2002/sulston-autobio.html)

[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:英国科学家](../Category/英国科学家.md "wikilink")
[Category:名譽勳位成員](../Category/名譽勳位成員.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:欧洲分子生物学组织会员](../Category/欧洲分子生物学组织会员.md "wikilink")
[Category:劍橋大學彭布羅克學院校友](../Category/劍橋大學彭布羅克學院校友.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.

9.

10.

11.
12.