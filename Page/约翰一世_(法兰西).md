**（遗腹子）约翰一世**（，），名义上的法蘭西國王和[納瓦拉](../Page/納瓦拉王國.md "wikilink")[國王](../Page/國王.md "wikilink")（自出生至去世）。

约翰为[卡佩王朝国王](../Page/卡佩王朝.md "wikilink")[路易十世的遗腹子](../Page/路易十世.md "wikilink")，母亲是[匈牙利公主克莱芒](../Page/匈牙利.md "wikilink")。实际上，约翰并没有实际统治法蘭西，因为婴儿在出生数日后即夭折了。但约翰一世仍被视为合法的法蘭西國王。他的遺体被安葬于[圣但尼修道院](../Page/圣但尼修道院.md "wikilink")。

基于《[薩利克法典](../Page/薩利克繼承法.md "wikilink")》，法蘭西贵族们否决了约翰同父异母的姐姐[让娜的继承权](../Page/让娜_\(法兰西\).md "wikilink")，而让约翰的叔叔[腓力五世继承了王位](../Page/腓力五世_\(法兰西\).md "wikilink")。一直有传言说腓力五世谋杀了婴儿國王约翰一世，或是用一死婴将其掉包。

[Jean_Ier_Bier.jpg](https://zh.wikipedia.org/wiki/File:Jean_Ier_Bier.jpg "fig:Jean_Ier_Bier.jpg")

1358年，[意大利](../Page/意大利.md "wikilink")[佛罗伦萨一个名叫尼欧](../Page/佛罗伦萨.md "wikilink")·巴廖尼（意大利语：Baglioni）的人自称是约翰一世的继承人，并且试图维护自己的权利，但最后他在[普罗旺斯被捕](../Page/普罗旺斯.md "wikilink")，并被[囚禁](../Page/囚禁.md "wikilink")，于1363年去世。不过最近的一本书揭示了这个流言，制造了这个谣言，目的是为了加强他在罗马教廷继承法国王位的合法性和权利，他和尼欧·巴廖尼在1354年会面，但不久之后克拉·迪佐遇刺身亡，尼欧·巴廖尼等了两年克拉·迪佐给他的报酬，他到了匈牙利，面见了国王[拉约什一世](../Page/拉约什一世.md "wikilink")（的侄子），在法庭上，[拉约什一世承认了尼欧](../Page/拉约什一世.md "wikilink")·巴廖尼是克莱门斯和路易十世的儿子。不久后他去[阿维尼翁见教宗](../Page/阿维尼翁.md "wikilink")[诺森六世](../Page/诺森六世.md "wikilink")，但诺森六世拒绝承认他的继位合法性，经过多次尝试失败后，他在[那不勒斯被捕](../Page/那不勒斯.md "wikilink")，并于1363年去世。

[J](../Category/法国君主.md "wikilink") [J](../Category/纳瓦拉君主.md "wikilink")
[J](../Category/卡佩王朝.md "wikilink") [J](../Category/幼君.md "wikilink")