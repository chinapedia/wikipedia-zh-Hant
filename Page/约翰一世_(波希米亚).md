[Le_Boucq_-_Jean_de_Luxembourg_(1296-1346),_roi_de_Bohême.jpg](https://zh.wikipedia.org/wiki/File:Le_Boucq_-_Jean_de_Luxembourg_\(1296-1346\),_roi_de_Bohême.jpg "fig:Le_Boucq_-_Jean_de_Luxembourg_(1296-1346),_roi_de_Bohême.jpg")
**卢森堡的约翰**（[捷克语](../Page/捷克语.md "wikilink")：Jan
Lucemburský；[德语](../Page/德语.md "wikilink")：Johann von
Luxemburg，）[波希米亚国王](../Page/波希米亚.md "wikilink")（1311年－1346年在位）。

## 生平

卢森堡的约翰是[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")[亨利七世之子](../Page/亨利七世_\(神圣罗马帝国\).md "wikilink")，生于[卢森堡](../Page/卢森堡.md "wikilink")。1310年，他在父皇的支持下获得对全部捷克地区的统治权，并成为[卢森堡伯爵和波希米亚国王](../Page/卢森堡伯爵.md "wikilink")。1311年2月7日，他在[布拉格加冕](../Page/布拉格.md "wikilink")。

约翰在[奥地利公爵](../Page/奥地利.md "wikilink")[腓特烈三世和](../Page/腓特烈三世_\(奥地利\).md "wikilink")[上巴伐利亚公爵](../Page/上巴伐利亚.md "wikilink")[路易四世](../Page/路易四世_\(神圣罗马帝国\).md "wikilink")（即皇帝路易四世）争夺神圣罗马帝国皇位的斗争中支持后者，并参加了决定性的[米尔多夫战役](../Page/米尔多夫战役.md "wikilink")（1322年）。此役之后，路易成为无可争议的皇帝。然而不久，约翰与路易的关系即告破裂。

1333年至1335年，卢森堡的约翰代表[圭尔夫派](../Page/圭尔夫派.md "wikilink")（教皇派）在[意大利作战](../Page/意大利.md "wikilink")。1340年起，约翰双目失明。1346年路易四世去世后，约翰的儿子当选为神圣罗马帝国皇帝[查理四世](../Page/查理四世_\(神圣罗马帝国\).md "wikilink")。

约翰在[百年战争中支持](../Page/百年战争.md "wikilink")[法国](../Page/法国.md "wikilink")。1346年，他在援助法国国王[腓力六世抵御](../Page/腓力六世_\(法兰西\).md "wikilink")[英格兰国王](../Page/英格兰.md "wikilink")[爱德华三世入侵时阵亡于](../Page/爱德华三世.md "wikilink")[克雷西战役](../Page/克雷西战役.md "wikilink")。

在约翰统治时期，波希米亚从[波兰手中夺取了](../Page/波兰.md "wikilink")[上卢日策和](../Page/上卢日策.md "wikilink")[西里西亚](../Page/西里西亚.md "wikilink")。

2005年6月捷克票選「[最偉大的捷克人](../Page/最偉大的捷克人.md "wikilink")」（*Největší
Čech*）中，他排名第74。

## 外部链接

  - [History of some of John's 4 resting places as of 1913, when this
    book was written. He is now in a
    fifth.](https://archive.org/stream/cu31924028319949/cu31924028319949_djvu.txt)
  - [Brief history of Czech
    lands](https://web.archive.org/web/20120612014408/http://users.ox.ac.uk/~tayl0010/history.htm)
  - [Social History in Bohemia during the 13th into the 14th
    century](http://www.academia.edu/1512598/Konflikt_i_pojednanie_w_spoleczenstwie_sredniowiecznym._Przypadek_Fryderyka_z_Schoenburga_i_biskupa_olomunieckiego_Dytryka_1285_)

[Category:捷克君主](../Category/捷克君主.md "wikilink")
[Category:卢森堡伯爵](../Category/卢森堡伯爵.md "wikilink")
[Category:卢森堡王朝](../Category/卢森堡王朝.md "wikilink")
[Category:比利时裔卢森堡人](../Category/比利时裔卢森堡人.md "wikilink")
[Category:荷兰裔卢森堡人](../Category/荷兰裔卢森堡人.md "wikilink")
[Category:在法国的卢森堡人](../Category/在法国的卢森堡人.md "wikilink")
[Category:卢森堡裔捷克人](../Category/卢森堡裔捷克人.md "wikilink")
[Category:比利时裔捷克人](../Category/比利时裔捷克人.md "wikilink")
[Category:荷兰裔捷克人](../Category/荷兰裔捷克人.md "wikilink")
[Category:巴黎人](../Category/巴黎人.md "wikilink")
[Category:莎士比亚男性角色](../Category/莎士比亚男性角色.md "wikilink")
[Category:盲人王室和贵族](../Category/盲人王室和贵族.md "wikilink")
[Category:葬于卢森堡圣母主教座堂](../Category/葬于卢森堡圣母主教座堂.md "wikilink")