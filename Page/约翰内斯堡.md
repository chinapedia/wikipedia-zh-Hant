**約翰尼斯堡**（[南非語](../Page/南非語.md "wikilink")、英語：Johannesburg；[科薩語](../Page/科薩語.md "wikilink")：eGoli；[祖魯語](../Page/祖魯語.md "wikilink")：iGoli），简称**约堡**（）\[1\]，是[南非最大的城市與經濟](../Page/南非.md "wikilink")、文化中心，同時也是為[世界上最大的50個城市之一](../Page/按人口排列的世界城市地區列表.md "wikilink")\[2\]。約翰尼斯堡為[豪登省首府和最大城市](../Page/豪登省.md "wikilink")，也是該省最富裕的地區\[3\]。雖然約翰內斯堡不是[南非三個首都城市之一](../Page/南非#政治和政府.md "wikilink")，但它是的所在地。這城市位於富含礦物的[維瓦特斯蘭山脈中](../Page/維瓦特斯蘭.md "wikilink")，是世界上最大規模[黃金和](../Page/黃金.md "wikilink")[鑽石貿易的中心](../Page/鑽石.md "wikilink")。

這座大都市是列出的[全球城市之一](../Page/全球城市.md "wikilink")。2011年，約翰內斯堡市的人口為4,434,827，成為南非人口最多的城市\[4\]。同年，人口為7,860,781\[5\]。但都會區的面積達1,645平方公里（635平方英里），較其他主要城市大，所以[人口密度為較適中的](../Page/人口密度.md "wikilink")2,364/平方公里（6,120平方英里）。

這城市成立於1886年，當時在農場發現了黃金。由於[威特沃特斯蘭德發現了極大規模的金礦](../Page/威特沃特斯蘭德.md "wikilink")，這座城市通常被形容為現代的[黃金國](../Page/黃金國.md "wikilink")\[6\]。該名稱源於參與建立城市的三名男子中的一人或全部。在十年來，人口增長達到10萬。

從20世紀70年代末到1994年，[索韋托是一個獨立的城市](../Page/索韋托.md "wikilink")，現在則為約翰內斯堡的一部分。索韋托最初是“**So**uth-**We**stern
**To**wnships”的首字母縮寫，其起源於約翰內斯堡郊區的一系列定居點，主要由來自黃金採礦業的非洲本地工人居住。索韋托雖然最終被併入約翰內斯堡，但已經被分開作為黑人的住宅區，他們不被允許居住在約翰內斯堡。主要由講[英語的](../Page/英語.md "wikilink")[印度裔南非人居住](../Page/印度人.md "wikilink")。根據南非白人政府的[種族隔離政策](../Page/南非種族隔離.md "wikilink")，這些地區被指定為非白人地區。

## 历史

最初围绕在约翰内斯堡的人主要是[闪族部落](../Page/闪族.md "wikilink")，13世纪开始，说[班图语的群落开始从非洲中心地带向该地区迁徙并侵占了闪族土著的土地](../Page/班图语.md "wikilink")。到了[十八世纪中叶](../Page/十八世纪.md "wikilink")，该地区充满了大量[索托-茨瓦纳社群](../Page/索托-茨瓦纳.md "wikilink")。作为班图语群落的一支后裔，他们其实是西方今天的[博茨瓦纳到南方](../Page/博茨瓦纳.md "wikilink")[莱索托地区势力的延伸](../Page/莱索托.md "wikilink")，北至[佩迪](../Page/佩迪.md "wikilink")（Pedi）地区。

索托-茨瓦纳的石头遗迹表明，城镇和乡村就曾分布在现在[德兰士瓦的某些地区和约翰内斯堡市区周围](../Page/德兰士瓦.md "wikilink")，索托-茨瓦纳群体在该地区发展起了农业，蓄养牛群、绵羊和山羊，并曾大量开采冶炼[铜矿](../Page/铜矿.md "wikilink")、[铁矿和](../Page/铁矿.md "wikilink")[锡](../Page/锡.md "wikilink")。根据1960年代的大量记录和评估，索托-茨瓦纳群体大致处于[铁器时代晚期](../Page/铁器时代.md "wikilink")。

[荷兰探险者](../Page/荷兰.md "wikilink")（[南非荷兰语和](../Page/南非荷兰语.md "wikilink")[荷兰语中称](../Page/荷兰语.md "wikilink")“先驱”，Voortrekkers，意为“敢先伸头者”）最早在19世纪早期到达该地区。与[索托-茨瓦纳合作赶走了](../Page/索托-茨瓦纳.md "wikilink")[马特贝莱](../Page/马特贝莱.md "wikilink")（Matebele），[1830年早期在](../Page/1830.md "wikilink")[比勒陀利亚和](../Page/比勒陀利亚.md "wikilink")[勒斯腾堡建立了殖民地](../Page/勒斯腾堡.md "wikilink")，宣布拥有后来成为[南非共和国一部分的约翰内斯堡的主权](../Page/南非共和国.md "wikilink")。南非共和国的非正式名字是[德兰士瓦共和国](../Page/德兰士瓦共和国.md "wikilink")。

1976年6月16日，南非白人当局在城西南25公里的[索韦托](../Page/索韦托.md "wikilink")（黑人最集中的隔离区），血腥镇压了[索維托起義](../Page/索維托起義.md "wikilink")。

## 人口統計

总人口达3,225,812左右，其中半数以上都是[黑人](../Page/黑人.md "wikilink")。

黑人佔人口的73％，其次是白人16％，有色人種6％，亞洲人佔4％。 42％的人口在24歲以下，而6％的人口是60歲以上。 37％的城市居民失業。
91％的失業人員是黑人。婦女佔勞動人口的43％。
19％的經濟活躍人士在批發和零售業工作，18％在金融，房地產和商業服務，17％在社區，社會和個人服務，12％在製造業。只有0.7％在採礦業工作。

約翰內斯堡29％的成年人從高中畢業。 14％有高等教育（大學或技術學校）。 7％的居民完全是文盲。 15％有小學教育。

34％使用公共交通工具上班或上學。 32％步行到工作或學校。 還有34％使用私人交通工具上班或上學。

## 治安

南非治安欠佳，入室[打劫](../Page/打劫.md "wikilink")、拦路[抢劫](../Page/抢劫.md "wikilink")、[凶杀](../Page/凶杀.md "wikilink")[强奸等案件时有发生](../Page/强奸.md "wikilink")。[受害者不仅是普通民众](../Page/受害者.md "wikilink")，一些[政府要员](../Page/政府.md "wikilink")、知名人士和[外国使领馆人员也遭抢劫](../Page/外国使领馆人员.md "wikilink")。

## 交通

[捷運系統僅存於城內少數舊市區](../Page/捷運系統.md "wikilink")，新的鐵路線路則仍興建當中。

## 在約翰內斯堡的大學

  - [金山大學](../Page/金山大學.md "wikilink")
  - [約翰內斯堡大學](../Page/約翰內斯堡大學.md "wikilink")（University of Johannesburg）

### 參考

  - [南非教育部公布的部分學校名單](../Page/南非教育部公布的部分學校名單.md "wikilink")

## 主要建築物

### 卡爾登中心

[卡爾登中心是位於中央車站](../Page/卡爾登中心.md "wikilink")600公尺處的50層大樓，地下一、二層是購物中心，頂樓有瞭望台，也有餐廳和特產品店。

### 市政廳

位於中央車站南方約500公尺，1913年建成，有一可容納2000人的音樂廳，會定期舉行交響樂演奏會。

### 史垂頓電信塔

聳立於約翰尼斯堡街上的圓柱形建築物，高269m，是非洲最高的建築物，是非洲國內外通訊系統的中心，塔頂附近有6層樓高的遼望台，可在豪華的迴轉餐廳內眺望風景和用餐，但現在不對外開放。

### 鐵路博物館

[博物館利用過去的約翰尼斯堡建立的](../Page/博物館.md "wikilink")，與現在的中央車站相鄰，館內展示精緻的火車模型和圖片和使用過貨車的零件。

### 南非洲蘭德大學

本校位於市區西北，在1968年是南非最新的大學，有五個[學院](../Page/學院.md "wikilink")，每個學院色彩都不一樣。

### 班蘇山攝影博物館

在中央車站北方2公里處，保存南非發展的歷史照片，也收藏具有歷史性的照相機和照相器材。

### 約翰內斯堡國際機場

[約翰內斯堡國際機場是南非和非洲最繁忙機場](../Page/約翰內斯堡國際機場.md "wikilink")。

[Coat_of_Arms_of_Johannesburg.svg](https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Johannesburg.svg "fig:Coat_of_Arms_of_Johannesburg.svg")

## 画廊

  - [高画质约翰内斯堡城市景观，Paul Saad作品](https://www.flickr.com/photos/kartaba/)

<File:Johannesburg> From M2 Highway Looking North.jpg|thumb|Johannesburg
From M2 Highway Looking North, photos by [Paul
Saad](https://secure.flickr.com/photos/kartaba/) <File:Johannesburg> CBD
From M2 Highway.jpg|thumb|Johannesburg CBD From M2 Highway, photos by
[Paul Saad](https://secure.flickr.com/photos/kartaba/)

## 氣候

## 姐妹城市

约翰内斯堡姐妹城市有：

  - [阿克拉](../Page/阿克拉.md "wikilink");\[7\]

  - [亚的斯亚贝巴](../Page/亚的斯亚贝巴.md "wikilink");\[8\]

  - [伯明翰](../Page/伯明翰.md "wikilink");\[9\]

  - [金沙萨](../Page/金沙萨.md "wikilink");\[10\]

  - [伦敦](../Page/伦敦.md "wikilink");\[11\]

  - [马托拉 (莫桑比克)](../Page/马托拉_\(莫桑比克\).md "wikilink");\[12\]

  - [纽约](../Page/纽约.md "wikilink");\[13\]\[14\]

  - [臺北](../Page/台北市.md "wikilink");

  - [马恩河谷省](../Page/马恩河谷省.md "wikilink");\[15\]

  - [温得和克](../Page/温得和克.md "wikilink").\[16\]

## 参考来源

## 外部連結

  - [约翰内斯堡市政府](http://www.joburg.org.za)

[J](../Category/南非城市.md "wikilink")
[Category:南非之最](../Category/南非之最.md "wikilink")
[J](../Category/豪登省.md "wikilink")
[约翰内斯堡](../Category/约翰内斯堡.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.
10.
11.
12.
13.
14.

15.
16.