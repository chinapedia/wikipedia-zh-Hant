[Johnston_Atoll_on_the_globe_(small_islands_magnified)_(Polynesia_centered).svg](https://zh.wikipedia.org/wiki/File:Johnston_Atoll_on_the_globe_\(small_islands_magnified\)_\(Polynesia_centered\).svg "fig:Johnston_Atoll_on_the_globe_(small_islands_magnified)_(Polynesia_centered).svg")
**约翰斯顿环礁**（），位於北[太平洋中部](../Page/太平洋.md "wikilink")，是[波利尼西亚群岛的組成部分之一](../Page/波利尼西亚群岛.md "wikilink")，属于[美国](../Page/美国.md "wikilink")[无建制领土](../Page/无建制领土.md "wikilink")。东北距[夏威夷](../Page/夏威夷.md "wikilink")[檀香山](../Page/檀香山.md "wikilink")1328公里，有重要战略地位。

## 地理

[Johnston_Atoll.svg](https://zh.wikipedia.org/wiki/File:Johnston_Atoll.svg "fig:Johnston_Atoll.svg")
约翰斯顿环礁主要由[约翰斯顿岛](../Page/约翰斯顿岛.md "wikilink")（Johnston
Island）、[沙岛](../Page/沙岛.md "wikilink")（Sand
Island）和[北岛](../Page/北岛.md "wikilink")、[东岛两个人工小岛屿组成](../Page/东岛.md "wikilink")。面积共2.8平方公里，海岸线长约10公里。气候干燥，常年有东北[信风](../Page/信风.md "wikilink")。岛上地势平坦，陆地最高点为海平面以上5米，无淡水资源。

## 历史

  - 1807年，英国海军舰长[查尔斯·约翰斯顿发现该环礁的主岛](../Page/查尔斯·约翰斯顿.md "wikilink")（即约翰斯顿岛），后该岛以他的名字命名。
  - 1858年，[夏威夷王国和](../Page/夏威夷王国.md "wikilink")[美国对该地区的主权发生争议](../Page/美国.md "wikilink")。
  - 1898年，美国吞并夏威夷王国，约翰斯顿环礁正式归属美国。
  - 1934年，划归[美国海军管辖](../Page/美国海军.md "wikilink")，并在主岛上修建了海军基地。
  - 1941年，约翰斯顿环礁宣布成为美国海军防务区，建立海军航空兵站。
  - 1948年，划归[美国空军管辖](../Page/美国空军.md "wikilink")。

约翰斯顿环礁在1950年代至1960年代曾为为[核武器试验区和飞机加油站](../Page/核武器.md "wikilink")。之后到2000年，为美国[化学武器的储存及处理地](../Page/化学武器.md "wikilink")。该岛曾由美国太平洋空军[希卡姆空军基地](../Page/希卡姆空军基地.md "wikilink")（Hickam
AFB）和[内政部鱼类和野生动物服务机构管理](../Page/美國內政部.md "wikilink")。岛上有不到800名美国军事人员和[承包商生活](../Page/承包商.md "wikilink")。2010年，[希卡姆空军基地與](../Page/希卡姆空军基地.md "wikilink")[珍珠港合併](../Page/珍珠港.md "wikilink")，島上軍事設施已經廢棄。


[Category:玻里尼西亞](../Category/玻里尼西亞.md "wikilink")
[Category:美國本土外小島](../Category/美國本土外小島.md "wikilink")
[Category:美國保護區](../Category/美國保護區.md "wikilink")
[Category:美国环礁](../Category/美国环礁.md "wikilink")
[Category:美国无人岛](../Category/美国无人岛.md "wikilink")
[Category:太平洋环礁](../Category/太平洋环礁.md "wikilink")
[Category:太平洋无人岛](../Category/太平洋无人岛.md "wikilink")
[Category:大洋洲环礁](../Category/大洋洲环礁.md "wikilink")
[Category:大洋洲无人岛](../Category/大洋洲无人岛.md "wikilink")
[Category:美國核試驗地點](../Category/美國核試驗地點.md "wikilink")
[Category:约翰斯顿环礁](../Category/约翰斯顿环礁.md "wikilink")
[Category:2009年設立的保護區](../Category/2009年設立的保護區.md "wikilink")