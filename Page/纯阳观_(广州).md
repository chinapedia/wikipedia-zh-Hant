**纯阳观**位于[中国](../Page/中国.md "wikilink")[广州市](../Page/广州市.md "wikilink")[海珠区](../Page/海珠区.md "wikilink")[五凤村的](../Page/五凤村.md "wikilink")[漱珠岗上](../Page/漱珠岗.md "wikilink")，是一座[道教的](../Page/道教.md "wikilink")[宫观](../Page/宫观.md "wikilink")。

## 建筑

该观坐[北向](../Page/北.md "wikilink")[南](../Page/南.md "wikilink")，依漱珠岗而建，占地1万多[平方米](../Page/平方米.md "wikilink")。原有山门、灵官殿、大殿、毕绝先师亭、南雪祠、清献祠、松枝馆、拜亭、东西廊房、步云亭、东西客厅、左右巡廊、库房、怡云轩、朝斗台等建筑，现仅存山门、纯阳殿、拜亭、朝斗台、放生池及[李明彻墓等](../Page/李明彻.md "wikilink")。在山门的石额上刻有以[篆书写成](../Page/篆书.md "wikilink")“纯阳观”三字，为[两广总督](../Page/两广总督.md "wikilink")[阮元手笔](../Page/阮元.md "wikilink")，两侧石刻有对联：灵山松径古，道岸石门高，为[潘仕成手书](../Page/潘仕成.md "wikilink")。过山门后沿楼梯而上是纯阳殿，纯阳殿为观内主要建筑，正中供奉纯阳祖师[吕洞宾金身塑像](../Page/吕洞宾.md "wikilink")。殿后有朝斗台，朝斗台为[花岗岩石砌筑](../Page/花岗岩.md "wikilink")，平面呈长方形，为李明彻天象及朝斗的地方，是现时广州地区现仅存的古代[观象台](../Page/观象台.md "wikilink")。在纯阳殿前是拜亭，拜亭前是放生池，池的右侧有一块大石，石上刻着“漱石”二字。

## 历史

该观建于[清朝](../Page/清朝.md "wikilink")[道光四年](../Page/道光.md "wikilink")（1824年），至道光九年(1829年)竣工，共耗资白银7600多两。是清代[道士](../Page/道士.md "wikilink")[李明彻为祭祀](../Page/李明彻.md "wikilink")[唐代](../Page/唐代.md "wikilink")[道士](../Page/道士.md "wikilink")[吕洞宾而兴建](../Page/吕洞宾.md "wikilink")，道观的名称出自吕洞宾之号。1963年被公布为市级[文物保护单位](../Page/文物保护单位.md "wikilink")，但[文化大革命期间](../Page/文化大革命.md "wikilink")，受到严重破坏，所有建筑被外单位占用。1986年政府收回大殿，恢复宗教活动，至1992年5月将整个漱珠岗收回。1988年[香港的道教团体](../Page/香港.md "wikilink")（香港圆玄学院、蓬莱仙馆等）捐资重修了大殿、灵官殿、拜亭、山门等。

## 外部链接

  - [第46期 纯阳观
    一个人的天象台和一个城市的天空](https://web.archive.org/web/20140513174828/http://www.gzlib.gov.cn/shequ_info/ndgz/NDGZDetail.do?id=18148)
    - 《[南方都市報](../Page/南方都市報.md "wikilink")》

[Category:广东清代建筑](../Category/广东清代建筑.md "wikilink")
[C纯](../Category/广州市文物保护单位.md "wikilink")
[C纯](../Category/广州道观.md "wikilink")
[C纯](../Category/海珠区.md "wikilink")
[Category:呂祖廟](../Category/呂祖廟.md "wikilink")