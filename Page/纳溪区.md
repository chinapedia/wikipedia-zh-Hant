**纳溪区**是[四川省](../Page/四川省.md "wikilink")[泸州市下辖的一个](../Page/泸州市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，位于[四川盆地南缘](../Page/四川盆地.md "wikilink")，[长江与](../Page/长江.md "wikilink")[永宁河交汇外](../Page/永宁河_\(长江上游支流\).md "wikilink")，[泸州市的中部](../Page/泸州.md "wikilink")。全区幅员面积1150.6平方公里。纳溪位于因[三国时期](../Page/三国.md "wikilink")“蛮夷纳贡出此溪”而得名，又名云溪。[南宋理宗绍定五年](../Page/南宋.md "wikilink")（公元1232年）建县，(1996年)撤县建区，东连[合江县](../Page/合江县.md "wikilink")，南接[叙永县](../Page/叙永县.md "wikilink")，西界[江安县](../Page/江安县.md "wikilink")，北邻[江阳区](../Page/江阳区.md "wikilink")。地理坐标东经105
°09′-- 105 °37′，
北纬28°02′14″—28°26′53″，属[亚热带湿润性季风气候区](../Page/亚热带湿润性季风气候区.md "wikilink")，四季分明，气候温和，雨量充沛。年总降雨量1129.1毫米；年平均气温18.6℃，年极端最高气温为43.2℃，年极端最低气温为0.9℃；年日照1443.9小时。

纳溪东西宽41千米，南北长46千米，全区幅员面积1150平方公里，辖大渡口、新乐、棉花坡、龙车、丰乐、白节、天仙、渠坝、护国、打古、上马、合面12个镇、安富、永宁、东升3个街道，199个村（社区），是“中国白酒酒庄文化服务综合标准化示范区”、“中国最佳生态旅游示范区”、“中国特早茶之乡”、“中国特色竹乡”、“中国民间文化艺术之乡”、“全国生态文明先进区”、“全国绿化造林先进区”。

## 历史

  - [春秋战国时](../Page/春秋战国.md "wikilink")，属[巴国巴郡地](../Page/巴国.md "wikilink")。
  - [西汉至](../Page/西汉.md "wikilink")[西晋本江阳县地](../Page/西晋.md "wikilink")，成汉（304—347年）时江阳郡为夷僚（少数民族）所据，江阳郡寄武阳县地（今[井研县南](../Page/井研县.md "wikilink")）,后寄治武阳县治（今[彭山县东北](../Page/彭山县.md "wikilink")），称西江阳郡，后复故土。
  - [南北朝为](../Page/南北朝.md "wikilink")[绵水县地](../Page/绵水县.md "wikilink")。
  - [隋分属](../Page/隋.md "wikilink")[泸州](../Page/泸州.md "wikilink")、[江安](../Page/江安.md "wikilink")、[汉安等县地](../Page/汉安.md "wikilink")。
  - [唐属](../Page/唐.md "wikilink")[江安](../Page/江安.md "wikilink")、[泾南县地](../Page/泾南县.md "wikilink")。
  - [北宋仁宗皇祜三年](../Page/北宋.md "wikilink")（1051年）置纳溪寨，
  - [南宋理宗绍定五年](../Page/南宋.md "wikilink")（1232年）升[纳溪寨为](../Page/纳溪.md "wikilink")[纳溪县](../Page/纳溪县.md "wikilink")。先属[泸州](../Page/泸州.md "wikilink")，后属[江安州](../Page/江安.md "wikilink")。
  - [元](../Page/元.md "wikilink")、[明](../Page/明.md "wikilink")、[清三代均属](../Page/清.md "wikilink")[泸州管辖](../Page/泸州.md "wikilink")。
  - [民国元年](../Page/民国.md "wikilink")（1912年），属[川南军政府](../Page/川南军政府.md "wikilink")，后属下[川南道](../Page/川南道.md "wikilink")、[永宁道](../Page/永宁道.md "wikilink")。民国十八年（1929年）撤销永宁道，直属民国[四川省政府](../Page/四川省政府.md "wikilink")。民国二十四年（1935年），隶属[第七区行政督察专员公署](../Page/第七区行政督察专员公署.md "wikilink")。
  - (1949年12月3日)，[纳溪解放](../Page/纳溪.md "wikilink")，12月12日成立[纳溪县人民政府](../Page/纳溪县人民政府.md "wikilink")，隶属[泸县专署](../Page/泸县专署.md "wikilink")。
  - (1952年)改属[隆昌专署](../Page/隆昌专署.md "wikilink")。
  - (1953年)初属[泸州专署](../Page/泸州专署.md "wikilink")。
  - (1960年)，改属[宜宾专区](../Page/宜宾专区.md "wikilink")。
  - (1983年6月)建立省辖[泸州市](../Page/泸州市.md "wikilink")，[纳溪县划归](../Page/纳溪县.md "wikilink")[泸州市管辖](../Page/泸州市.md "wikilink")。
  - (1996年7月1日)，[纳溪撤县建区](../Page/纳溪.md "wikilink")，为[泸州市纳溪区](../Page/泸州市.md "wikilink")，结束了[纳溪建县](../Page/纳溪.md "wikilink")764年历史。

## 地理

  - 纳溪区东连[合江县](../Page/合江.md "wikilink")，南接[叙永县](../Page/叙永.md "wikilink")、[兴文县](../Page/兴文.md "wikilink")，西连[江安县](../Page/江安.md "wikilink")，北邻泸州市[江阳区](../Page/江阳.md "wikilink")。
  - 地理坐标东经105°09′—105°37′，北纬28°02′14″—28°26′53″，东西宽41千米，南北长46千米，
  - 纳溪南高北低，平坝、丘陵、低山兼有，海拔在230米—963.2米之间，全区最高点在打鼓镇普照山，海拔963.2米。
  - 属[亚热带湿润季风气候区](../Page/亚热带湿润季风气候.md "wikilink")，四季分明，气候温和，雨量充沛。年降水量1306毫米，年均温度18℃，年日照1220小时。

## 人口

  - 截至2011年末，纳溪区[人口](../Page/人口.md "wikilink")48.26万人，其中非农业人口9.34
    万人。[人口自然增长率](../Page/人口自然增长率.md "wikilink")2.47‰。（为2011年末数据）
  - 境内人口以[汉族为主](../Page/汉族.md "wikilink")，占总人口99%以上，[少数民族有](../Page/少数民族.md "wikilink")[苗族](../Page/苗族.md "wikilink")、[回族](../Page/回族.md "wikilink")、[彝族](../Page/彝族.md "wikilink")、[壮族](../Page/壮族.md "wikilink")、[满族等](../Page/满族.md "wikilink")17个少数民族700余人。（为2011年末数据）

## 行政区划

下辖2个[街道办事处](../Page/街道办事处.md "wikilink")、12个[镇](../Page/行政建制镇.md "wikilink")：

。

## 经济

  - [GDP完成](../Page/GDP.md "wikilink")25.1亿元，三次产业比为23.3：45.1：31.6。财政收入9562万元，农民人均纯收入2613元。（2003年数据）

## 交通

  - 内河航运：[长江](../Page/长江.md "wikilink")、[永宁河](../Page/永宁河_\(长江上游支流\).md "wikilink")
  - 高速公路：2011年，纳溪区公路通车总里程1118.4公里。有（G76）[隆纳高速公路](../Page/隆纳高速公路.md "wikilink")、（G93）[泸宜高速公路](../Page/泸宜高速公路.md "wikilink")、[泸纳高等级公路](../Page/泸纳高等级公路.md "wikilink")。
  - 其他公路：纳溪城区距[泸州](../Page/泸州.md "wikilink")14公里距[泸州港集装箱多用途码头](../Page/泸州港集装箱多用途码头.md "wikilink")20公里[321国道北接](../Page/321国道.md "wikilink")[泸州江阳区](../Page/泸州江阳区.md "wikilink")，南接[叙永县](../Page/叙永县.md "wikilink")，为[西南出海大通道在](../Page/西南出海大通道.md "wikilink")[纳溪境内一段](../Page/纳溪.md "wikilink")，长34.8千米。
  - 铁路：[隆叙铁路](../Page/隆叙铁路.md "wikilink")、[隆纳铁路](../Page/隆纳铁路.md "wikilink")
  - 航空：[泸州机场位于离本区约](../Page/泸州机场.md "wikilink")8KM的蓝田坝。

## 旅游

  - [倒流河风景区](../Page/倒流河.md "wikilink")
  - [护国岩石刻蔡锷将军题字石刻](../Page/护国岩石刻.md "wikilink")
  - [天仙洞天仙硐](../Page/天仙洞.md "wikilink")，又名天仙洞，国家AAAA级景区，位于四川泸州纳溪天仙镇境内，321国道与纳黔高速旁、泸纳高等级公路纳溪段终点处，地理坐标位于东经105°23′－105°28′，北纬28°38′－28°43′，距泸州市中心19公里，距蓝田机场15公里，距纳溪区中心9公里，交通便利，已开通景区直通车。最高海拔482米，最低海拔245.1米，相对高差236.9米。景区于1996年5月正式接待游客，2000年被泸州市人民政府批准为市级风景名胜区，2003年被四川省人民政府批准为省级风景名胜区。2013年成功创建成为国家AAAA级景区。
  - [普照山避暑山庄](../Page/普照山.md "wikilink")，风景区。位于打鼓镇以北8公里路程。
  - [铜鼓寺位于纳溪区新乐镇铜鼓村](../Page/铜鼓寺.md "wikilink")，为唐代寺庙，驱车大渡镇方向5公里左右。
  - [护国战争纪念馆位于](../Page/护国战争纪念馆.md "wikilink")[泸州市纳溪区](../Page/泸州市.md "wikilink")[棉花坡镇的](../Page/棉花坡镇.md "wikilink")[护国战争纪念馆原属于地主庄园](../Page/护国战争纪念馆.md "wikilink")[陶家大院原](../Page/陶家大院.md "wikilink")[护国战争](../Page/护国战争.md "wikilink")[棉花破战役指挥部](../Page/棉花破战役.md "wikilink")。

## 相关链接

  - [中国纳溪门户网站](http://www.naxi.gov.cn/)

## 参考文献

[纳溪区](../Page/category:纳溪区.md "wikilink")
[泸州](../Page/category:四川市辖区.md "wikilink")
[区](../Page/category:泸州区县.md "wikilink")