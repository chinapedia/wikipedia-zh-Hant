**纽约公共图书馆**（**New York Public Library**,
**NYPL**）是[美国主要](../Page/美国.md "wikilink")[图书馆系统之一](../Page/图书馆.md "wikilink")，和[布魯克林公共圖書館系统](../Page/布魯克林公共圖書館.md "wikilink")、[皇后圖書館系统一起组成](../Page/皇后圖書館.md "wikilink")[纽约市的三大公共图书馆系统](../Page/纽约市.md "wikilink")。

纽约公共图书馆总部的主体建筑位于[第五大道](../Page/第五大道.md "wikilink")，由于藏有《[古腾堡圣经](../Page/古腾堡圣经.md "wikilink")》和[牛顿的](../Page/艾萨克·牛顿.md "wikilink")《[自然哲学的数学原理](../Page/自然哲学的数学原理.md "wikilink")》，它被认为是世界著名的图书馆之一。

## 历史

[NYC_Public_Library_postcard_1920.jpg](https://zh.wikipedia.org/wiki/File:NYC_Public_Library_postcard_1920.jpg "fig:NYC_Public_Library_postcard_1920.jpg")
[new_york_public_library_1948.jpg](https://zh.wikipedia.org/wiki/File:new_york_public_library_1948.jpg "fig:new_york_public_library_1948.jpg")
19世纪末，[纽约市只有两座对外开放的图书馆](../Page/纽约市.md "wikilink")：由阿斯特遗赠40万[美元建造的阿斯特图书馆](../Page/美元.md "wikilink")，1849年正式开放；由藏书家莱努克斯建造的莱努克斯图书馆，1880年开放。

1886年，蒂尔登遗赠240万美元，遗嘱要求在纽约建造一座图书馆，他的遗嘱执行人比格娄律师建议和阿斯特图书馆、莱努克斯图书馆合并，成立一个“蒂尔登、阿斯特、莱努克斯基金会”，基金会于1895年5月23日成立。1901年[卡内基也捐赠了](../Page/卡内基.md "wikilink")520万美元，并且合并了纽约巡回图书馆。市政府承担了维护和运转费用，因此纽约公共图书馆成为政府和私人慈善机构共同合组的、典型美国方式运营的图书馆。

图书馆主体是科研图书馆，位于[曼哈顿第五大道](../Page/曼哈顿.md "wikilink")40街和42街之间，1911年5月23日落成，第二天开放，门前有两个石雕卧狮，命名为“阿斯特狮”和“莱努克斯狮”，后来又俗称为“阿斯特先生”和“莱努克斯夫人”（虽然两只都是雄狮）。在[大萧条时期](../Page/大萧条时期.md "wikilink")，纽约市长[拉瓜地亚为了鼓励市民战胜](../Page/拉瓜地亚.md "wikilink")[经济危机](../Page/经济危机.md "wikilink")，将这两座石狮取名为“忍耐”和“坚强”，现在纽约市民只是根据它们的位置俗称左面的（北方）为“上城”（居住区），右面的为“下城”（金融区）。

主要阅览室是315号房间，90.5[米长](../Page/米.md "wikilink")，23.8米宽，高15.8米，四外都是开放式书架，长窗，房顶有大吊灯。室内长型阅览桌和舒服的椅子，桌上有铜台灯。装备有[电子计算机可以和](../Page/电子计算机.md "wikilink")[因特网联接](../Page/因特网.md "wikilink")。所有一切都是免费的，有许多著名作家在这里搜集资料，在大萧条时期，也有许多人在这里进行相当大学教育的自学。

1980年代，图书馆扩建了12,000[平方米藏书面积](../Page/平方米.md "wikilink")，是在地下，占据了图书馆西面的伯里扬公园的地下部分，上面仍然是公园。

## 分馆

纽约公共图书馆除了主馆外还有85个分馆，分散在布朗士区、曼哈顿和斯塔滕岛（里士满），分馆主要提供文学作品和基础科研书籍，其中有4个属于特殊科研分馆：人文和社会科学图书馆；表演艺术图书馆（在[林肯中心](../Page/林肯中心.md "wikilink")）；[黑人文化图书馆](../Page/黑色人种.md "wikilink")（在绍姆贝格中心）；工商科学图书馆。还有三个流动图书馆。

## 虚构作品中

许多[电影都以纽约公共图书馆做背景](../Page/电影.md "wikilink")，电影《[-{zh-hans:蜘蛛侠;
zh-hant:蜘蛛人}-](../Page/蜘蛛人.md "wikilink")》、《[-{zh-hans:捉鬼队;
zh-hant:魔鬼剋星}-](../Page/魔鬼剋星.md "wikilink")》、《[-{zh-hans:后天;zh-hk:明日之後;zh-tw:明天過後;}-](../Page/明天過後.md "wikilink")》、《[-{zh-hans:蒂凡尼早餐;
zh-hant:第凡內早餐}-](../Page/第凡內早餐.md "wikilink")》、《[愛情第二章](../Page/愛情第二章.md "wikilink")》（*Chapter
Two*）、《[紐約大逃亡](../Page/紐約大逃亡.md "wikilink")》、《[新绿野仙踪](../Page/新绿野仙踪.md "wikilink")》都应用了图书馆的环境。

## 外部链接

  - [图书馆网站](https://www.nypl.org/node/107747)
  - [图书馆网站](http://www.nypl.org/)
  - [科研图书馆网站](http://www.nypl.org/research/)

{{-}}

[Category:曼哈顿博物馆](../Category/曼哈顿博物馆.md "wikilink")
[Category:紐約市公共圖書館系統](../Category/紐約市公共圖書館系統.md "wikilink")
[Category:曼哈顿建筑物](../Category/曼哈顿建筑物.md "wikilink")