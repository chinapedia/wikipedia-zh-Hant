[The_United_Nations_Secretariat_Building.jpg](https://zh.wikipedia.org/wiki/File:The_United_Nations_Secretariat_Building.jpg "fig:The_United_Nations_Secretariat_Building.jpg"),
[纽约市](../Page/纽约市.md "wikilink")[曼哈頓](../Page/曼哈頓.md "wikilink")。\]\]
[NYSCapitolPanorama.jpg](https://zh.wikipedia.org/wiki/File:NYSCapitolPanorama.jpg "fig:NYSCapitolPanorama.jpg")，1899年落成的[紐約州議會大廈](../Page/紐約州議會大廈.md "wikilink")。\]\]
[ChryslerBuildingMidtownManhattanNewYorkCity.jpg](https://zh.wikipedia.org/wiki/File:ChryslerBuildingMidtownManhattanNewYorkCity.jpg "fig:ChryslerBuildingMidtownManhattanNewYorkCity.jpg"),
[纽约市](../Page/纽约市.md "wikilink")。\]\]
[chinatown_manhattan_2009.JPG](https://zh.wikipedia.org/wiki/File:chinatown_manhattan_2009.JPG "fig:chinatown_manhattan_2009.JPG")。由華人在纽约構成,同時是紐約最迅速增長的人口。\]\]
[Whiteface_Mountain_from_Lake_Placid_Airport.JPG](https://zh.wikipedia.org/wiki/File:Whiteface_Mountain_from_Lake_Placid_Airport.JPG "fig:Whiteface_Mountain_from_Lake_Placid_Airport.JPG")。\]\]
[Statue_of_Liberty_from_base.jpeg](https://zh.wikipedia.org/wiki/File:Statue_of_Liberty_from_base.jpeg "fig:Statue_of_Liberty_from_base.jpeg")。\]\]
[DSCN4390_americanfalls_e.jpg](https://zh.wikipedia.org/wiki/File:DSCN4390_americanfalls_e.jpg "fig:DSCN4390_americanfalls_e.jpg")。\]\]
[Unisfera_Flushing.jpg](https://zh.wikipedia.org/wiki/File:Unisfera_Flushing.jpg "fig:Unisfera_Flushing.jpg")。\]\]
[WesthamptonInnerWaterway.jpg](https://zh.wikipedia.org/wiki/File:WesthamptonInnerWaterway.jpg "fig:WesthamptonInnerWaterway.jpg")。\]\]
[Cornell_University,_Ho_Plaza_and_Sage_Hall.jpg](https://zh.wikipedia.org/wiki/File:Cornell_University,_Ho_Plaza_and_Sage_Hall.jpg "fig:Cornell_University,_Ho_Plaza_and_Sage_Hall.jpg"),
[伊萨卡](../Page/伊萨卡_\(纽约州\).md "wikilink")。\]\]
[Buffharbor_view.jpg](https://zh.wikipedia.org/wiki/File:Buffharbor_view.jpg "fig:Buffharbor_view.jpg")濱的[水牛城](../Page/水牛城.md "wikilink")。\]\]
[OneWorldTradeCenter.jpg](https://zh.wikipedia.org/wiki/File:OneWorldTradeCenter.jpg "fig:OneWorldTradeCenter.jpg"),
[曼哈顿下城](../Page/曼哈顿下城.md "wikilink")。\]\]
[Rochester_Eastman_Theatre_-_Exterior.jpg](https://zh.wikipedia.org/wiki/File:Rochester_Eastman_Theatre_-_Exterior.jpg "fig:Rochester_Eastman_Theatre_-_Exterior.jpg")，。\]\]
[June03_007.jpg](https://zh.wikipedia.org/wiki/File:June03_007.jpg "fig:June03_007.jpg")。\]\]
**纽约州**（），[暱稱](../Page/美國各州暱稱列表.md "wikilink")**帝國州**（Empire
State），是[美国](../Page/美国.md "wikilink")[东北部的一个](../Page/美國東北部.md "wikilink")[州](../Page/州.md "wikilink")，首府是[奧爾巴尼](../Page/奧爾巴尼.md "wikilink")，由[上州](../Page/紐約上州.md "wikilink")、[紐約市及郊縣](../Page/紐約市.md "wikilink")（下州）所組成。

纽约州西部及北部与[加拿大接壤](../Page/加拿大.md "wikilink")，东邻[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")、[康涅狄格州](../Page/康涅狄格州.md "wikilink")、[佛蒙特州](../Page/佛蒙特州.md "wikilink")（即“[新英格兰三州](../Page/新英格兰.md "wikilink")”）和大西洋，南接[宾夕法尼亚州和](../Page/宾夕法尼亚州.md "wikilink")[新泽西州](../Page/新泽西州.md "wikilink")，西北部連接[五大湖之一的](../Page/五大湖.md "wikilink")[安大略湖](../Page/安大略湖.md "wikilink")。通常人们说起纽约州时不能像称呼美国绝大多数州那样省略最后的“州”（State）字，因为简单地说纽约多指[纽约市](../Page/纽约市.md "wikilink")。

## 历史

纽约州原为印第安人居住地，並于1609至1664年間为[荷兰所占](../Page/荷兰.md "wikilink")，称为**新尼德兰**。1664年來自英国約克郡的人占领后，為紀念約克公爵而定名为「新約克郡」，故稱[紐約](../Page/紐約.md "wikilink")（New
York）。1777年4月20日独立，1788年加入联邦，成为美国第十一个州。

## 地理

纽约州总面积为141,300平方公里，在全美名列第27位。相鄰地區自順時針方向依序為北美[五大湖區](../Page/五大湖區.md "wikilink")（由[伊利湖及與](../Page/伊利湖.md "wikilink")[安大略湖連結的](../Page/安大略湖.md "wikilink")[尼亞加拉河](../Page/尼亞加拉河.md "wikilink")）；[恰普蘭湖](../Page/恰普蘭湖.md "wikilink")；[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")（[聖勞倫斯河](../Page/聖勞倫斯河.md "wikilink")）及[魁北克省](../Page/魁北克省.md "wikilink")；新英格蘭三州（[佛蒙特州](../Page/佛蒙特州.md "wikilink")、[麻薩諸塞州及](../Page/麻薩諸塞州.md "wikilink")[康乃狄克州](../Page/康乃狄克州.md "wikilink")）；大西洋；[新澤西州及](../Page/新澤西州.md "wikilink")[賓夕法尼亞州](../Page/賓夕法尼亞州.md "wikilink")）。

紐約市在紐約州的南端，包含了[長島](../Page/長島.md "wikilink")，[哈德遜河谷的南部及新澤西州北部](../Page/哈德遜河.md "wikilink")，其為人口稠密地帶的中心點。此人口稠密地帶由北起[波士頓南至](../Page/波士頓.md "wikilink")[維吉尼亞州的](../Page/維吉尼亞州.md "wikilink")[華盛頓特區稱為](../Page/華盛頓特區.md "wikilink")“波士頓
-
華盛頓城市帶”。紐約市所轄的5個區當中有四個區位於[哈德遜河南端的](../Page/哈德遜河.md "wikilink")3個島上，分別為[曼哈頓島](../Page/曼哈頓島.md "wikilink")、[史坦頓島及長島的](../Page/史坦頓島.md "wikilink")[布魯克林區及](../Page/布魯克林區.md "wikilink")[皇后區](../Page/皇后區.md "wikilink")。

紐約州一共有62個[郡](../Page/郡.md "wikilink")。請詳見：[纽约州行政区划](../Page/纽约州行政区划.md "wikilink")。

這其中，有5個郡與[紐約市的](../Page/紐約市.md "wikilink")5個[區基本一致](../Page/區.md "wikilink")，所以這5個郡都沒有起作用的郡級政府，只有一些區級官員，紐約市被認為是這5個郡（區）的郡城：紐約郡（[曼哈頓](../Page/曼哈頓.md "wikilink")）、金斯郡（[布魯克林](../Page/布魯克林.md "wikilink")）、布朗克斯郡（[布朗克斯](../Page/布朗克斯.md "wikilink")）、里奇蒙郡（[史泰登島](../Page/史泰登島.md "wikilink")）和皇后郡（[皇后區](../Page/皇后區.md "wikilink")）。

## 经济

在17至19世纪时，纽约州一直是美国经济最强州。2011年，纽约州[GDP达到](../Page/GDP.md "wikilink")12,000亿美元，为全美第3位（仅次于[加利福尼亚州和](../Page/加利福尼亚州.md "wikilink")[德克萨斯州](../Page/德克萨斯州.md "wikilink")）。

## 人口

根据[美国人口调查局的统计数据显示](../Page/美国人口调查局.md "wikilink")，至2011年7月1日，纽约州人口达到19,465,197人。与[2010年美国人口普查时相比增长了](../Page/2010年美国人口普查.md "wikilink")0.45%，在全美各州中继续保持第3位（仅次于加州和德州）。与其它的州相比，纽约州人口高度城市化，92%的人口在城市中居住。另外，纽约州也保持着较高的人口[迁移率](../Page/迁移.md "wikilink")，从2000年至2005年间，从纽约州迁移至[佛罗里达州的人口数超过其它任何一州迁至另一州的人数](../Page/佛罗里达州.md "wikilink")。不过，纽约州仍然是国际上最大的[移民目的地之一](../Page/移民.md "wikilink")（仅2008年就达420万），在各州中仅次于[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")。绝大多数外来移民都安置在[纽约市及其周边地区](../Page/纽约市.md "wikilink")，而纽约市作为美国最大的城市，人口数已超过810万，比第2位[洛杉矶市多出一倍以上](../Page/洛杉矶市.md "wikilink")。紐約州有3.8%的人口是[LGBT人士](../Page/LGBT.md "wikilink")。從中國人的構成在纽约和[布魯克林最迅速增長的人口](../Page/布魯克林.md "wikilink")。

## 政治

近25年來，纽约州一直偏向支持民主黨，是民主黨的重要根據地。自1988年歷屆美國總統大選，紐約州一直支持民主黨總統候選人。

在州政府層面，自1975年以來民主黨一直控制州眾議院，而共和黨則控制參議院。民主黨過去在州長選舉中多次獲勝，1975－1994年及2007年至今一直由民主黨主政，現任州長為[安德魯·庫默](../Page/安德魯·庫默.md "wikilink")。

## 宗教

紐約州[紐約市](../Page/紐約市.md "wikilink")[曼哈頓的](../Page/曼哈頓.md "wikilink")[華盛頓高地](../Page/華盛頓高地.md "wikilink")（Washington
Heights）有天主教[聖心傳教女修會創始人](../Page/聖心傳教女修會.md "wikilink")[卡布里尼修女](../Page/卡布里尼修女.md "wikilink")（Saint
Frances Xavier Cabrini，又稱為Mother
Cabrini）的神龕與墓地。卡布里尼修女是美國第一位受到封號的修女，同時也是一位移民。

下列是紐約州人口信仰比例。\[1\]

  - 31% [天主教徒](../Page/天主教徒.md "wikilink")
  - 27% 無宗教信仰者
  - 26% [新教徒](../Page/新教徒.md "wikilink")
  - 7% [猶太教](../Page/猶太教.md "wikilink")
  - 2% [伊斯蘭教](../Page/伊斯蘭教.md "wikilink")
  - 1% [佛教徒](../Page/佛教.md "wikilink")

紐約州新教徒中，各教義派所占比例不同，其中以[浸禮會](../Page/浸禮會.md "wikilink")（Baptist）最高（8%）、其次為[衛理公會派](../Page/衛理公會派.md "wikilink")（Methodist，6%）、接著是[路德會](../Page/路德會.md "wikilink")（Lutheran，3%）。

## 重要城市

  - [纽约市](../Page/纽约市.md "wikilink")
  - [羅徹斯特](../Page/羅徹斯特_\(紐約州\).md "wikilink")（Rochester）:屬於[門羅郡](../Page/門羅縣_\(紐約州\).md "wikilink")，也是郡政府所在城市。
  - [水牛城](../Page/布法罗_\(纽约州\).md "wikilink")（Buffalo）:是[紐約州第二大城市](../Page/紐約州.md "wikilink")（僅次於[紐約市](../Page/紐約市.md "wikilink")）、隸屬於[伊利郡](../Page/伊利縣_\(紐約州\).md "wikilink")，也是該郡的首府。對岸為[加拿大的](../Page/加拿大.md "wikilink")[伊利堡鎮](../Page/伊利堡.md "wikilink")。
  - [奥本尼](../Page/奥本尼.md "wikilink")（Albany）:是[紐約州的州政府所在城市](../Page/紐約州.md "wikilink")，同時也是紐約州[奧巴尼郡郡治所在地](../Page/奧爾巴尼縣_\(紐約州\).md "wikilink")
  - [雪城](../Page/锡拉丘兹_\(纽约州\).md "wikilink")（Syracuse）

## 教育

  - 州立大學

<!-- end list -->

  - [纽约州立大学](../Page/纽约州立大学.md "wikilink")（State University of New
    York，簡稱為SUNY）有70多個校區，較大的是：
      - [紐約州立大學奧本尼分校](../Page/紐約州立大學奧本尼分校.md "wikilink")（Albany）
      - [紐約州立大學賓漢頓分校](../Page/宾汉顿大学.md "wikilink")（Binghamton）
      - [紐約州立大學水牛城分校](../Page/紐約州立大學水牛城分校.md "wikilink")（Buffalo）
      - [紐約州立大學石溪分校](../Page/石溪大学.md "wikilink")（Stony Brook）

<!-- end list -->

  - 位於紐約市

<!-- end list -->

  - [哥倫比亞大學](../Page/哥倫比亞大學.md "wikilink")（Columbia University）
  - [柏納德學院](../Page/柏納德學院.md "wikilink")（Barnard College）
  - [紐約市立大學](../Page/紐約市立大學.md "wikilink")（The City University of New
    York）
  - [福坦莫大學](../Page/福坦莫大學.md "wikilink")（Fordham University）
  - [茱利亞音樂學院](../Page/茱利亞音樂學院.md "wikilink")（Juilliard School of Music）
  - [曼尼斯音乐学院](../Page/曼尼斯音乐学院.md "wikilink")（Mannes College of Music）
  - [曼哈頓學院](../Page/曼哈頓學院.md "wikilink")（Manhattan College）
  - [紐約大學](../Page/紐約大學.md "wikilink")（New York University）
  - [佩斯大學](../Page/佩斯大學.md "wikilink")（Pace University）
  - [普瑞特藝術學院](../Page/普瑞特藝術學院.md "wikilink")（Pratt College）
  - [聖若望大學](../Page/聖若望大學.md "wikilink")（St. John's University）
  - [莎拉劳伦斯学院](../Page/莎拉劳伦斯学院.md "wikilink")（Sara Lawrence College）
  - [葉史瓦大學](../Page/葉史瓦大學.md "wikilink")（Yeshiva University）

<!-- end list -->

  - 私立大學

<!-- end list -->

  - [克拉克森大学](../Page/克拉克森大学.md "wikilink")（Clarkson University）
  - [哥倫比亞大學](../Page/哥倫比亞大學.md "wikilink")（Columbia University）
  - [科爾傑大學](../Page/科爾傑大學.md "wikilink")（Colgate University）
  - [康乃尔大学](../Page/康乃尔大学.md "wikilink")（Cornell University）
  - [伊萨卡学院](../Page/伊萨卡学院.md "wikilink") (Ithaca College)\]
  - [紐約大學](../Page/紐約大學.md "wikilink")（New York University）
  - [柯柏聯盟學院](../Page/柯柏聯盟學院.md "wikilink")（The Cooper Union for the
    Advancement of Science and Art）
  - [-{zh-cn:伦斯勒;zh-tw:壬色列;}-理工學院](../Page/壬色列理工學院.md "wikilink")（Rensselaer
    Polytechnic Institute）
  - [羅徹斯特理工学院](../Page/羅徹斯特理工学院.md "wikilink")（Rochester Institute of
    Technology）
  - [羅徹斯特大學](../Page/羅徹斯特大學.md "wikilink")（University of Rochester）
  - [史基德摩爾學院](../Page/史基德摩爾學院.md "wikilink")（Skidmore College）
  - [雪城大學](../Page/雪城大學.md "wikilink")（Syracuse University）
  - [艾道菲大學](../Page/艾道菲大學.md "wikilink")（Adelphi University）
  - [弗沙學院](../Page/弗沙學院.md "wikilink")（Vassar College）
  - [库克学院](../Page/库克学院.md "wikilink")（keuka college）

<!-- end list -->

  - 联邦政府下属学院

<!-- end list -->

  - [美国军事学院](../Page/西点军校.md "wikilink")（西點軍校，The United States Military
    Academy at West Point）
  - [國王角美國商船學院](../Page/國王角美國商船學院.md "wikilink")（United States Merchant
    Marine Academy at Kings Point）

## 交通

### 鐵路運輸

紐約州的鐵路客運主要是由[美鐵](../Page/美鐵.md "wikilink")（Amtrak）所提供。紐約市有全美最大的鐵路使用率，其中包括[紐約地鐵](../Page/紐約地鐵.md "wikilink")、[大都會北方鐵路](../Page/大都會北方鐵路.md "wikilink")、以及[長島鐵路](../Page/長島鐵路.md "wikilink")。

### 主要機場

  - [甘迺迪國際機場](../Page/甘迺迪國際機場.md "wikilink")（John F. Kennedy
    International Airport,
    JFK/KJFK）：是[達美航空的轉運中心](../Page/達美航空.md "wikilink")
  - [拉瓜地亞機場](../Page/拉瓜地亞機場.md "wikilink")（LaGuardia Airport, LGA/KLGA）

### 重要高速公路

紐約州境內有數個主要的高速公路系統，包含規模最大的[紐約州收費公路](../Page/紐約州收費公路.md "wikilink")（New
York State Thruway），與數條州際公路：

  - 南北向：
      - [81號州際公路](../Page/81號州際公路.md "wikilink")（I-81）
      - [87號州際公路](../Page/87號州際公路.md "wikilink")（I-87）
      - [95號州際公路](../Page/95號州際公路.md "wikilink")（I-95）

Belt pkwy

  - 東西向：
      - [78號州際公路](../Page/78號州際公路.md "wikilink")（I-78）
      - [84号州际公路](../Page/84号州际公路_\(宾夕法尼亚州至马萨诸塞州\).md "wikilink")（I-84）
      - [86號州際公路](../Page/86號州際公路.md "wikilink")（I-86）
      - [88號州際公路](../Page/88號州際公路_\(東段\).md "wikilink")（I-88）

278號公路Bq and SI expressway

## 職業運動

### 美式足球

  - [NFL](../Page/NFL.md "wikilink")
      - [紐約巨人](../Page/紐約巨人.md "wikilink")（New York
        Giants）：雖然名義上是紐約州的球隊，但主球場實際上是在[紐澤西州境內](../Page/紐澤西州.md "wikilink")。
      - [紐約噴射機](../Page/紐約噴射機.md "wikilink")（New York Jets）：主球場同樣是在紐澤西州。
      - [水牛城比爾](../Page/水牛城比爾.md "wikilink")（Buffalo Bills）

### 棒球

  - [大聯盟](../Page/美國職棒大聯盟.md "wikilink")
      - [紐約洋基](../Page/紐約洋基.md "wikilink")（New York Yankees）
      - [紐約大都會](../Page/紐約大都會.md "wikilink")（New York Mets）
  - [小聯盟](../Page/小聯盟.md "wikilink")
      - 水牛城野牛（Buffalo
        Bisons）：3A級國際聯盟，母隊為[克里夫蘭印地安人](../Page/克里夫蘭印地安人.md "wikilink")。
      - 羅徹斯特紅翼（Rochester Red
        Wings）：3A級國際聯盟，母隊為[明尼蘇達雙城](../Page/明尼蘇達雙城.md "wikilink")。
      - 雪城天領（Syracuse Sky
        Chiefs）：3A級國際聯盟，母隊為[多倫多藍鳥](../Page/多倫多藍鳥.md "wikilink")。
      - 賓漢頓大都會（Binghampton Mets）：2A級東方聯盟，母隊為紐約大都會。
      - 史坦頓島洋基（Staten Island Yankees）：短期1A級紐約－賓夕弗尼亞聯盟，母隊為紐約洋基。
      - 布魯克林旋風（Brooklyn Cyclones）：短期1A級紐約－賓夕弗尼亞聯盟，母隊為紐約大都會。
      - 安尼安塔老虎（Oneonta
        Tigers）：短期1A級紐約－賓夕弗尼亞聯盟，母隊為[底特律老虎](../Page/底特律老虎.md "wikilink")。
      - 奧本雙日（Auburn Doubledays）：短期1A級紐約－賓夕弗尼亞聯盟，母隊為多倫多藍鳥。
      - 三城谷貓（Tri-City
        Valleycats）：短期1A級紐約－賓夕弗尼亞聯盟，母隊為[休士頓太空人](../Page/休士頓太空人.md "wikilink")。
      - 哈德森谷叛徒（Hudson Valley
        Renegades）：短期1A級紐約－賓夕弗尼亞聯盟，母隊為[坦帕灣光芒](../Page/坦帕灣光芒.md "wikilink")。
      - 巴塔維亞髒狗（Batavia
        Muckdogs）：短期1A級紐約－賓夕弗尼亞聯盟，母隊為[費城費城人](../Page/費城費城人.md "wikilink")。
      - 詹姆斯鎮擾亂者（Jamestown
        Jammers）：短期1A級紐約－賓夕弗尼亞聯盟，母隊為[佛羅里達馬林魚](../Page/佛羅里達馬林魚.md "wikilink")。

### 足球

  - [MLS](../Page/美國職業足球大聯盟.md "wikilink")
      - [纽约红牛](../Page/纽约红牛.md "wikilink")（New York Red Bulls）
      - [紐約城](../Page/紐約城足球俱樂部.md "wikilink")（New York City FC）

### 籃球

  - [NBA](../Page/NBA.md "wikilink")
      - [紐約尼克](../Page/紐約尼克.md "wikilink")（New York Knicks）
      - [布鲁克林籃網](../Page/布鲁克林籃網.md "wikilink")（Brooklyn Nets）
  - [NCAA](../Page/NCAA.md "wikilink")
      - 雪城大學隊（Syracuse University）
  - [WNBA](../Page/WNBA.md "wikilink")
      - [紐約自由人](../Page/紐約自由人.md "wikilink")（New York Liberty）

### 冰球

  - [NHL](../Page/NHL.md "wikilink")
      - [紐約島人](../Page/紐約島人.md "wikilink")（New York Islanders）
      - [紐約遊騎兵](../Page/紐約遊騎兵.md "wikilink")（New York Rangers）
      - [水牛城軍刀](../Page/水牛城軍刀.md "wikilink")（Buffalo Sabres）
  - NHL小联盟
      - Albany River Rats
  - [NCAA](../Page/NCAA.md "wikilink")
      - 壬色列理工大學工程師隊（Rensselaer Polytechnic Institute Engineers）
      - 康乃爾大學大紅隊（Cornell Big Red）

## 其他

  - 州花：[玫瑰](../Page/玫瑰.md "wikilink")
  - 州鸟：[知更鸟](../Page/知更鸟.md "wikilink")

### [HDI](../Page/HDI.md "wikilink")([人類發展指數](../Page/人類發展指數.md "wikilink"))

0.956（）

## 參考文獻

## 外部链接

  - [纽约州政府](http://www.ny.gov/)

  - [纽约州长](http://www.governor.ny.gov/)

  - [纽约州参议院](https://web.archive.org/web/20110811200332/http://www.nysenate.gov/front)

  -
  - [纽约州历史协会](http://www.nysha.org/)

  - [纽约州文化教育中心](https://web.archive.org/web/20110818152504/http://www.oce.nysed.gov/ctl/index.htm)

  - [USGS geographic resources of New
    York](http://www.usgs.gov/state/state.asp?State=NY)

  - [纽约大学](http://www.nyu.edu/)

  - [CUNY](http://www1.ccny.cuny.edu/)

  - [纽约州立大学](http://www.suny.edu/)

  - [纽约信息](http://mynewyorkcityconcierge.com/)

{{-}}

[Category:美国州份](../Category/美国州份.md "wikilink")
[紐約州](../Category/紐約州.md "wikilink")
[Category:1788年美國建立](../Category/1788年美國建立.md "wikilink")

1.  [Religious composition of adults in New
    York](http://www.pewforum.org/religious-landscape-study/state/new-york/),
    Pew Research Center