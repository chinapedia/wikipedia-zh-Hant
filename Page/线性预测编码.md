**线性预测编码**（）是主要用于[音频信号处理与](../Page/音频信号处理.md "wikilink")[语音处理中根据](../Page/语音处理.md "wikilink")[线性预测模型的信息用](../Page/线性预测.md "wikilink")[压缩形式表示](../Page/数据压缩.md "wikilink")[数字](../Page/数字.md "wikilink")[语音](../Page/语音.md "wikilink")[信号](../Page/信号.md "wikilink")[谱包络](../Page/谱包络.md "wikilink")（spectral
envelope）的工具。它是最有效的语音分析技术之一，也是低位速下编码方法高质量语音最有用的方法之一，它能够提供非常精确的语音参数预测。

## 综述

线性预测编码的基本思想是：「一个语音取样的现在值可以用若干个语音取样过去值的加权线性组合来逼近」。在线性组合中的加权系数称为预测器系数。通过使实际语音抽样和线性预测抽样之间差值的平方和达到最小值，能够决定唯一的一组预测器系数。

线性预测编码通过估计共振峰、剔除它们在语音信号中的作用、估计保留的蜂鸣音强度与频率来分析语音信号。剔除共振峰的过程称为逆滤波，经过这个过程剩余的信号称为残余信号（residue）。

描述峰鸣强度与频率、共鸣峰、残余信号的数字可以保存、发送到其它地方。线性预测编码通过逆向的过程合成语音信号：使用蜂鸣参数与残余信号生成源信号、使用共振峰生成表示声道的滤波器，源信号经过滤波器的处理就得到语音信号。

由于语音信号是一种非平稳的时变信号又同时具有短时平稳性，这个过程是在一段段的语音信号帧上进行处理的。通常每秒30到50帧的速度就能对可理解的信号进行很好的压缩。

## 线性预测编码的早期历史

根据[斯坦福大学Robert](../Page/斯坦福大学.md "wikilink") M.
Gray的说法，线性预测编码起源于1966年，当时[日本電信電話公社的S](../Page/日本電信電話公社.md "wikilink").
Saito和F.
Itakura描述了一种自动音素识别的方法，这种方法第一次使用了针对语音编码的[最大似然估计实现](../Page/最大似然估计.md "wikilink")。1967年，John
Burg略述了[最大熵的实现方法](../Page/最大熵.md "wikilink")。1969年Itakura与Saito提出了部分相关（partial
correlation）的概念, May Glen Culler提议进行实时语音压缩，B. S.
Atal在[美国声学协会年会上展示了一个LPC语音编码器](../Page/美国声学协会.md "wikilink")。1971年[Philco-Ford展示了使用](../Page/Philco-Ford.md "wikilink")16位LPC硬件的实时LPC并且卖出了四个。

1972年[美國國防部](../Page/美國國防部.md "wikilink")[國防高等研究計劃署](../Page/國防高等研究計劃署.md "wikilink")（DARPA）的[Bob
Kahn与](../Page/Bob_Kahn.md "wikilink")[林肯實驗室](../Page/林肯實驗室.md "wikilink")（Lincoln
Laboratory, LL）的Jim Forgie，以及[BBN科技的Dave](../Page/BBN科技.md "wikilink")
Walden开始了语音信息包的第一次开发，这最终带来了[Voice over
IP技术](../Page/VoIP.md "wikilink")。根据林肯實驗室的非正式历史资料记载，1973年Ed
Hofstetter实现了第一个2400位/秒的实时LPC。1974年，第一个双向实时LPC语音包通信在Culler-Harrison与林肯實驗室之间通过ARPANET以3500位/秒的速度实现。1976年，第一次LPC会议通过[ARPANET使用](../Page/ARPANET.md "wikilink")[Network
Voice
Protocol在Culler](../Page/Network_Voice_Protocol.md "wikilink")-Harrison、ISI、SRI与LL之间以3500位/秒的速度实现。最后在1978年，BBN的Vishwanath
et al.开发了第一个变速LPC算法。

## 线性预测编码系数表示

线性预测编码经常用来传输频谱包络信息，这样它就可以容忍传输误差。由于直接传输滤波器系数（参见[线性预测中系数定义](../Page/线性预测.md "wikilink")）对于误差非常敏感，所以人们不希望直接传输滤波器系数。换句话说，一个小的误差不会扭曲整个频谱或使整个频谱质量下降，但是一个小的误差可能使预测滤波器变得不稳定。

有许多更加高级的表示方法，如[对数面积比](../Page/对数面积比.md "wikilink")（log area
ratio，LAR）、[线谱对](../Page/线谱对.md "wikilink")（line spectral
pairs，LSP）分解以及[反射系数等](../Page/反射系数.md "wikilink")。在这些方法中，LSP由于它能够保证预测器的稳定性、并且小的系数偏差带来的谱误差也是局部的这些特性，所以得到了广泛应用。

## 应用

线性预测编码通常用于语音的重新合成，它是电话公司使用的声音压缩格式，如[GSM标准就在使用这种格式](../Page/GSM.md "wikilink")。它还用作[安全无线通信中的格式](../Page/安全.md "wikilink")，在安全的无线通信中，声音必须进行[数字化](../Page/数字化.md "wikilink")、[加密然后通过狭窄的语音信道传输](../Page/加密.md "wikilink")。

线性预测编码合成也可以用于构建[声音合成器](../Page/声音合成器.md "wikilink")，乐器用作从歌手声音预测得到的时变滤波器的激励信号，这在[电子音乐中有一定的流行](../Page/电子音乐.md "wikilink")。

1980年流行的[Speak &
Spell教育玩具中使用了一个](../Page/Speak_&_Spell.md "wikilink")10阶的线性预测编码。

在[FLAC音频编解码器中使用了](../Page/FLAC.md "wikilink")0到4阶的线性预测编码预测器。

## 参考文献

  - [Robert M. Gray, IEEE Signal Processing Society, Distinguished
    Lecturer Program](http://www-ee.stanford.edu/~gray/dl.html)

## 参见

  - [赤池信息量準則](../Page/赤池信息量準則.md "wikilink")（Akaike information
    criterion）
  - [音频压缩](../Page/音频压缩.md "wikilink")

## 外部連結

  - [实时LPC分析与综合学习软件](http://soundlab.cs.princeton.edu/software/rt_lpc/)
  - [HawkVoice开源LPC软件与API](https://web.archive.org/web/20050519013950/http://www.hawksoft.com/hawkvoice/)
  - [使用线性预测编码（LPC）的语音编码](https://web.archive.org/web/20060524125030/http://www.dspexperts.com/dsp/projects/lpc/)DSP
    experts.com

[Category:语音编解码器](../Category/语音编解码器.md "wikilink")
[Category:音频编解码器](../Category/音频编解码器.md "wikilink")
[Category:有损压缩算法](../Category/有损压缩算法.md "wikilink")