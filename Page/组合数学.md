广义的**组合数学**（）就是[离散数学](../Page/离散数学.md "wikilink")，狭义的**组合数学**是[组合计数](../Page/组合计数.md "wikilink")、[图论](../Page/图论.md "wikilink")、[代数结构](../Page/代数结构.md "wikilink")、[数理逻辑等的总称](../Page/数理逻辑.md "wikilink")。但这只是不同学者在叫法上的区别。总之，组合数学是一门研究可數或离散对象的科学。随着[计算机科学的日益发展](../Page/计算机科学.md "wikilink")，组合数学的重要性也日渐凸显，因为计算机科学的核心内容是使用[算法处理](../Page/算法.md "wikilink")[离散数据](../Page/离散数据.md "wikilink")。

狭义的组合数学主要研究满足一定条件的组态（也称组合模型）的存在、计数以及构造等方面的问题。
组合数学的主要内容有[组合计数](../Page/组合计数.md "wikilink")、[组合设计](../Page/组合设计.md "wikilink")、[组合矩阵](../Page/组合矩阵.md "wikilink")、[组合优化](../Page/组合优化.md "wikilink")（[最佳組合](../Page/最佳組合.md "wikilink")）等。

## 组合数学中的著名问题

  - 計算一些物品在特定條件下分組的方法數目。這些是關於[排列](../Page/排列.md "wikilink")、[組合和](../Page/組合.md "wikilink")[整數分拆的](../Page/整數分拆.md "wikilink")。
  - [地图着色问题](../Page/四色定理.md "wikilink")：对世界地图着色，每一個国家使用一种颜色。如果要求相邻国家的颜色相异，是否总共只需四种颜色？這是[圖論的問題](../Page/圖論.md "wikilink")。
  - [船夫过河问题](../Page/狼、羊、菜问题.md "wikilink")：船夫要把一匹狼、一只羊和一棵白菜运过河。只要船夫不在场，羊就会吃白菜、狼就会吃羊。船夫的船每次只能运送一种东西。怎样把所有东西都运过河？這是[線性規劃的問題](../Page/線性規劃.md "wikilink")。
  - [中国邮差问题](../Page/中国邮差问题.md "wikilink")：由中国组合数学家[管梅谷教授提出](../Page/管梅谷.md "wikilink")。邮递员要穿过城市的每一条路至少一次，怎样行走走过的路程最短？这不是一个[NP完全问题](../Page/NP完全.md "wikilink")，存在多项式复杂度算法：先求出度为奇数的点，用匹配算法算出这些点间的连接方式，然后再用[欧拉路径算法求解](../Page/欧拉路径.md "wikilink")。這也是[圖論的問題](../Page/圖論.md "wikilink")。
  - [任务分配问题](../Page/任务分配问题.md "wikilink")（也称[分配问题](../Page/分配问题.md "wikilink")）：有一些员工要完成一些任务。各个员工完成不同任务所花费的时间都不同。每个员工只分配一项任务。每项任务只被分配给一个员工。怎样分配员工与任务以使所花费的时间最少？這是[線性規劃的問題](../Page/線性規劃.md "wikilink")。
  - 如何構造[幻方](../Page/幻方.md "wikilink")。
    [幻方為一方陣](../Page/幻方.md "wikilink")，填入不重複之[自然數](../Page/自然數.md "wikilink")，並使其中每一縱列、橫列、對角線內數字之總和皆相同。

## 排列

从\(n\)个元素中取出\(k\)个元素，\(k\)个元素的排列數量為：

\[P_k^n = \frac{n!}{(n-k)!}\]

以[賽馬為例](../Page/賽馬.md "wikilink")，有8匹马参加比赛，玩家需要在彩票上填入前三胜出的马匹的号码，從8匹馬中取出3匹馬來排前3名，排列數量為：

\[P_3^8 =\frac{8!}{(8-3)!}=8\times7\times6=336\]

因为一共存在336种可能性，因此玩家在一次填入中中奖的概率应该是：

\[P = \frac{1}{336}= 0.00298\]

不過，中國大陸的教科書則是把從n取k的情況記作\(P^k_n\)或\(A^k_n\)（A代表Arrangement，即排列\[1\]）。

上面的例子是建立在取出元素不重複出現狀況。

從\(n\)个元素中取出\(k\)个元素，\(k\)个元素可以重复出现，這排列數量為：

\[U_k^n = n^k\]\[2\]

以[四星彩為例](../Page/中華民國公益彩券.md "wikilink")，10個數字取4個數字，因可能重複所以排列數量為：

\[U_4^{10}=10^4=10000\]

这时的一次性添入中奖的概率就应该是：

\[P=\frac{1}{10000}=0.0001\]

## 组合

和排列不同的是，组合取出元素的顺序不考虑。

从\(n\)个元素中取出\(k\)个元素，\(k\)个元素的组合數量为：

\[C_k^n ={n \choose k} = \frac{P_k^n}{k!} = \frac{n!}{k!(n-k)!}\]

不過，中國大陸的教科書則是把從n取k的情況記作\(C^k_n\)\[3\]。

以[六合彩為例](../Page/六合彩.md "wikilink")。在六合彩中从49顆球中取出6顆球的组合數量为：

\[C_{6}^{49}  = {49 \choose 6} = \frac{49!}{6!43!} = 13983816\]

如同排列，上面的例子是建立在取出元素不重複出現狀況。

从\(n\)个元素中取出\(k\)个元素，\(k\)個元素可以重複出現，這组合數量为：

\[H_k^n = C_{k}^{n+k-1}\]

以取色球為例，每種顏色的球有無限多顆，從8種色球中取出5顆球，這組合數量為：

\[H_5^8 = C_{5}^{8+5-1} = C_5^{12} = \frac{12!}{5!7!} = 792\]

因為組合數量公式特性，重複組合轉換成組合有另一種公式為：

\[H_k^n=C_k^{n+k-1}=\frac{(n+k-1)!}{k!(n-1)!}=C_{n-1}^{n+k-1}\]

另外\(H_k^n\)也可以記為\(F_k^n\)\[4\]

\[F_k^n = H_k^n\]

## 总结

<table>
<tbody>
<tr class="odd">
<td><p><span class="math inline"><em>n</em></span>中取<span class="math inline"><em>k</em></span></p></td>
<td><p>直線排列<br />
（考慮順序）</p></td>
<td><p>环状排列</p></td>
<td><p>组合<br />
（不考慮順序）</p></td>
</tr>
<tr class="even">
<td><p>不重复出现<br />
（不放回去）</p></td>
<td><p><span class="math inline">$P^n_k=\frac{n!}{(n-k)!}$</span><br />
</p></td>
<td><p><span class="math inline">$\frac{n!}{k \cdot (n-k)!}$</span><br />
</p></td>
<td><p><span class="math inline">$C^n_k=\frac{n!}{k! \cdot (n-k)!}$</span><br />
</p></td>
</tr>
<tr class="odd">
<td><p>可重复出现<br />
（再放回去）</p></td>
<td><p><span class="math inline"><em>U</em><sub><em>k</em></sub><sup><em>n</em></sup> = <em>n</em><sup><em>k</em></sup></span><br />
</p></td>
<td><p><span class="math inline">$\frac{\sum_{r|k}(r \cdot \varphi(r) \cdot n^{\frac{k}{r}})}{k}$</span><br />
</p></td>
<td><p><span class="math inline">$H^n_k=\frac{(n+k-1)!}{k! \cdot (n-1)!}$</span><br />
</p></td>
</tr>
</tbody>
</table>

## 参见

  - [阶乘](../Page/阶乘.md "wikilink")
  - [阶乘符号](../Page/阶乘符号.md "wikilink")
  - [排列](../Page/排列.md "wikilink")

## 參考文獻

## 外部連結

  - [The Combinatorics Net](http://www.combinatorics.net/)
  - [Electronic Journal of Combinatorics](http://www.combinatorics.org/)
  - [点算的奥秘](http://chowkafat.net/Mathtopic.html#Enumeration)

[\*](../Category/组合数学.md "wikilink")
[Category:离散数学](../Category/离散数学.md "wikilink")

1.
2.   OCLC:44527392
3.
4.   OCLC:44527392