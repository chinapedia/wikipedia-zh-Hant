**组织行为学**（），是通过研究一定[组织体系内人的](../Page/组织.md "wikilink")[心理和行为表现及其规律](../Page/心理.md "wikilink")，从而提高预测、引导和控制人的行为的能力，以实现组织既定目标的一種[社會科学](../Page/社會科学.md "wikilink")。\[1\]

## 從眾

[個體在行為上的調適](../Page/個體.md "wikilink")，以符合[團體的規範](../Page/團體.md "wikilink")。[團體的從眾壓力](../Page/从众效应.md "wikilink")，是否對個別成員的判斷力和態度產生影響？經由[所罗门·阿希](../Page/所罗门·阿希.md "wikilink")（Solomon
Asch）的實驗，結論是：團體規範會壓迫成員要順從（conformity）。因為我們希望成為團體的一份子，並避免與他人有顯著的不同，所以捨棄自己的主見而附應團體的意見。

從眾行為可以用[社會資訊處理理論](../Page/社會資訊處理理論.md "wikilink")（Social Information
Processing
Theory）來解釋，這個理論是由[傑勒德·R·塞蘭尼克](../Page/傑勒德·R·塞蘭尼克.md "wikilink")（Gerald
R. Salancik）和[傑弗瑞·菲弗](../Page/傑弗瑞·菲弗.md "wikilink")（Jeffrey
Pfeffer）在1978年提出，基本的前提是說人是一個[有機體](../Page/有機體.md "wikilink")，會根據所處的社會背景（social
context）和情境（situation）的狀況，當成一個[資訊來處理](../Page/資訊.md "wikilink")，然後調適其個人的態度、行為和信念等等。我們也可以這樣說，人們會使用社會環境中他人的價值、規範、期望及行為結果等等資訊，用以引導個人的行為。

因為社會資訊影響一個人的程度可能是不同的，塞蘭尼克和菲弗又分為三個不同程度的作用，稱之為影響的程序（processes of
influence），分別為遵守（compliance）、認同（identification）和內化（internalization）。

## 管理角色

1960年代末期，MIT的研究生Henry
Mintzberg針對5名主管，進行一項縝密的研究，以了解管理者的工作內容。根據他的觀察，總結出管理者扮演的角色可分為十種，而角色之間的相關程度高。這十種角色可分三類：人際性、資訊性以及決策性角色。

1.  人際性角色（Interpersonal Roles）

所有管理者多多少少都要執行一些儀式或象徵性的工作。例如，校長在畢業典禮上頒發證書，公司主管招待前來參訪的學生，他們都是在扮演主管（figurehead）的角色。管理者還有領導者（leadership）角色，包含招募、訓練、激勵或懲處員工。第三個角色是連絡者（liaison），與他人聯繫以取得資訊。例如，業務經理透過公司品管部門得到一些情報時，就是運用了內在連絡關係；業務經理若是從其他公司探到一些消息，則是運用了外在連絡關係。

1.  資訊性角色（Informational Roles）

所有管理者都會從外界的組織或組織內部蒐集資訊。管理者藉由傳媒或與他人交談，以了解一般大眾的偏好或競爭對手的活動資訊，這是偵查者（monitor）。把手邊得到的資訊傳送給組織其他成員，這時就是傳達者（disseminator）。此外，管理著也扮演發言人（spokesperson），代表組織向外界發表意見。

1.  決策性角色（Decisional Roles）

管理者提出並監督可以改善組織績效的新方案，是企業家角色（entrepreneur）。在清道夫角色（disturbance
handlers），管理者採取因應措施，排除未預期的困難阻礙。作為一個資源分派者（resource
allocators）管理者要負責調度人力、物力及財務資源。最後一個是協商者（negotiator），管理者必須和其他單位討論與協調，替自己的單位爭取福利。

| 角色    | 描述                  |
| ----- | ------------------- |
| 主管    | 成為組織象徵，負法律與社會責任     |
| 領導者   | 負責激勵與引導部屬           |
| 連絡者   | 建立向外蒐集情報之網路         |
| 偵查者   | 像中樞神經般，廣泛收集組織內外的資料  |
| 傳達者   | 將從各種管道獲得的資訊，傳送給其他成員 |
| 發言人   | 對外宣告組織計畫、政策、行動或成果   |
| 企業家   | 檢視組織與環境中的機會，以適時發動變革 |
| 清道夫   | 排除組織遭遇的重大且未預期的阻礙    |
| 資源分派者 | 決定或核准組織重大決策         |
| 協商者   | 在重大協商中代表組織          |

  - H.Mintzberg,''The Nature of Work ''（Upper Saddle River,NJ:Prentice
    Hall,1973）.

## 角色認同

個人的態度和實際行為能與角色一致，就構成了角色認同（role
identity）。當人們發現情境及其要求起了明顯的變化時，他們有迅速變換角色的能力。例如：當[工會幹事升任為領班時](../Page/工會.md "wikilink")，我們可以發現在幾個月內，他的態度會從傾向於工會轉為傾向管理當局。

## 參見

  - [工業與組織心理學](../Page/工業與組織心理學.md "wikilink")
  - [人力資源管理學](../Page/人力資源管理學.md "wikilink")
  - [人力資本](../Page/人力資本.md "wikilink")
  - [勞工](../Page/勞工.md "wikilink")
  - [心理學](../Page/心理學.md "wikilink")
  - [社會學](../Page/社會學.md "wikilink")
  - [社會心理學](../Page/社會心理學.md "wikilink")
  - [人類學](../Page/人類學.md "wikilink")
  - [政治學](../Page/政治學.md "wikilink")
  - [行政學](../Page/行政學.md "wikilink")
  - [人事行政](../Page/人事行政.md "wikilink")
  - [管理学](../Page/管理学.md "wikilink")
  - [經濟學](../Page/經濟學.md "wikilink")
  - [企业资源](../Page/企业资源.md "wikilink")

## 参考文献

[组织行为学](../Category/组织行为学.md "wikilink")
[Category:管理学](../Category/管理学.md "wikilink")
[Category:人力資源](../Category/人力資源.md "wikilink")
[Category:心理学](../Category/心理学.md "wikilink")

1.  [組織行為學](http://source.eol.cn/gjpxw/landa/zzxwx/m01/select1.htm)