**细胞分化**（），是[发育生物学的研究课题之一](../Page/发育生物学.md "wikilink")，指的是在[多细胞生物中](../Page/多细胞生物.md "wikilink")，一个[干细胞在分裂的时候](../Page/干细胞.md "wikilink")，其子细胞的[基因表达受到调控](../Page/基因表达.md "wikilink")，例如[DNA甲基化](../Page/DNA甲基化.md "wikilink")，变成不同細胞类型的过程。类如[全能](../Page/全能性.md "wikilink")（）的[受精卵在分裂到一定程度时](../Page/受精卵.md "wikilink")，其子细胞就会开始向特定的方向分化，形成[胎儿的](../Page/胎儿.md "wikilink")[肌肉](../Page/肌肉.md "wikilink")，[骨骼](../Page/骨骼.md "wikilink")，[毛发等器官](../Page/毛发.md "wikilink")。分化后的[细胞在其结构](../Page/细胞.md "wikilink")，功能上都会出现差异，而且成为了所谓的“[单能性](../Page/单能性.md "wikilink")”细胞（），就是其只能分裂得出同等细胞类型的子细胞\[1\]。但是所有这些子细胞的[基因组](../Page/基因组.md "wikilink")（）却是与“祖宗”的[干细胞一样的](../Page/干细胞.md "wikilink")\[2\]。研究细胞分化，对理解[疾病的发生](../Page/疾病.md "wikilink")，如[癌症的出现有着重要意义](../Page/癌症.md "wikilink")\[3\]。

## 分化与特化

**分化**\[4\]是指[分生组织細胞發育成](../Page/分生组织.md "wikilink")[細胞](../Page/細胞.md "wikilink")、[组织](../Page/组织.md "wikilink")、[器官](../Page/器官.md "wikilink")、乃至整个[个体](../Page/生物.md "wikilink")，或者由其幼年至成熟的过程中，在[生理上的](../Page/生理学.md "wikilink")、[形态上的改变](../Page/形态学.md "wikilink")。此现象通常伴随着**特化**的现象。

**特化**
(specialization)\[5\]：由于功能、潜能、适应力等方面的限制，导致[细胞](../Page/细胞.md "wikilink")、[组织](../Page/组织.md "wikilink")、[器官](../Page/器官.md "wikilink")、乃至整个[个体的结构上的改变](../Page/生物.md "wikilink")，使得个体能针对某种功能具有更大的效益。这种**特化**的功能，在[植物有时是可逆的](../Page/植物.md "wikilink")，有时则不可逆。

## 参考文献

<div class="references-small">

<references />

</div>

## 參見

  - [細胞潛能](../Page/細胞潛能.md "wikilink")

## 外部链接

  - [使用microarray研究胚胎脑垂体的细胞分化过程](http://physiolgenomics.physiology.org/cgi/content/abstract/00248.2005v1?etoc)
  - [细胞分化，小提琴走调了](http://www.sciencedirect.com/science?_ob=ArticleURL&_udi=B6VRT-4BDJHY9-8&_user=963894&_coverDate=01%2F06%2F2004&_rdoc=1&_fmt=&_orig=search&_sort=d&view=c&_acct=C000049504&_version=1&_urlVersion=0&_userid=963894&md5=ce62fab6d98df9e5e40579409142c179)
  - [细胞生长，分化和基因表达的调控](https://web.archive.org/web/20070630201604/http://www.niehs.nih.gov/emfrapid/html/Symposium1/3apndx2.html)

[Category:细胞过程](../Category/细胞过程.md "wikilink")
[Category:發育生物學](../Category/發育生物學.md "wikilink")

1.

2.

3.

4.  \[Esau, K. 1977.\]， Anatomy of seed plants. NY: John Wiley and Sons.
    p. 508, 524.

5.