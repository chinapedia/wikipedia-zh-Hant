**绍柳斯·什托姆贝尔加斯**（，），生于[苏联](../Page/苏联.md "wikilink")[立陶宛加盟共和国](../Page/立陶宛.md "wikilink")[克莱佩达](../Page/克莱佩达.md "wikilink")，前[立陶宛职业篮球运动员和立陶宛国家男子篮球队成员](../Page/立陶宛.md "wikilink")，司职[小前锋](../Page/小前锋.md "wikilink")。

在萨乌留斯11年的国家队生涯中总共为立陶宛国家男子篮球队出场97次得到1036分，他帮助国家队在1996年、2000年两次夺得[奥运会铜牌以及](../Page/奥运会.md "wikilink")[雅典欧洲篮球锦标赛的银牌](../Page/雅典.md "wikilink")，萨乌留斯还担任过国家队的队长。

NBA著名中锋[姚明回忆到](../Page/姚明.md "wikilink")，当他只有16岁时萨乌留斯是当时[中国CBA联赛中](../Page/中国男子篮球职业联赛.md "wikilink")[上海大鲨鱼队的外援](../Page/上海大鲨鱼篮球俱乐部.md "wikilink")，他以出色的球技成为联赛最受关注的球员之一，也是姚明当时的偶像。如今萨乌留斯仍被广泛认定为是CBA联赛有史以来最出色的外援之一。

## 成就/荣誉

  - 立陶宛国家男子篮球队成员 1994年至2004年
  - [欧洲篮球锦标赛](../Page/欧洲篮球锦标赛.md "wikilink") (亚军) 1997年, 1999年,
    2001年；(冠军) 2003年
  - [奥运会](../Page/奥运会.md "wikilink") 1996年, 2000年, 2004年
  - [友好运动会](../Page/友好运动会.md "wikilink") 1998年
  - 入选立陶宛LKL联赛全明星赛 1996年, 2003年
  - 欧洲篮球网（Eurobasket.com）评选出的立陶宛全明星第一阵容 1996年
  - 立陶宛LKL联赛 冠军 1998年, 1999年
  - NEBL 冠军 1999年
  - 欧洲篮球联赛 冠军 萨基列斯考那斯 (Žalgiris Kaunas BC) in 1999年
  - 入选欧洲全明星赛- 柏林 1998年
  - 意大利杯亚军 2000年
  - 西班牙ACB联赛全明星赛 2001年
  - ULEB联赛 欧洲联赛 亚军 2001年
  - 西班牙ACB联赛 四强 2001年
  - 土耳其篮球联赛 常规赛冠军 2002年
  - 土耳其杯 冠军 2002年
  - 欧洲联赛 冠军 2004年

萨乌留斯曾经效力过的俱乐部：[考那斯KK阿特勒塔斯](../Page/考那斯.md "wikilink") (KK Atletas),
[上海大鲨鱼队](../Page/上海大鲨鱼篮球俱乐部.md "wikilink")
([中国](../Page/中国.md "wikilink")), 萨基列斯考那斯 (Žalgiris Kaunas BC),
维鲁斯博洛尼亚 (Virtus Bologna), 塔乌陶瓷俱乐部 (TAU Cerámica), 埃菲斯皮尔森 (Efes Pilsen
S.K.), UNICS喀山 (UNICS Kazan), 费内巴切 (Fenerbahçe Ülkerspor)。

## 外部链接

  - [萨乌留斯CBA月薪一千美金
    雅典队长现在国内养老](http://www.tianshannet.com.cn/special/content/2006-12/28/content_1555432.htm)
    2006年12月28日
  - [Saulius Štombergas -
    EUROBASKET.LT](http://www.eurobasket.lt/?players.id.164)
    ([立陶宛语](../Page/立陶宛语.md "wikilink"))
  - [www.answers.com萨乌留斯·斯汤博加斯](http://www.answers.com/topic/saulius-tombergas)


[Category:立陶宛男子篮球运动员](../Category/立陶宛男子篮球运动员.md "wikilink")
[Category:中国篮球联赛外援](../Category/中国篮球联赛外援.md "wikilink")
[Category:立陶宛奧林匹克運動會銅牌得主](../Category/立陶宛奧林匹克運動會銅牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2000年夏季奥林匹克运动会奖牌得主](../Category/2000年夏季奥林匹克运动会奖牌得主.md "wikilink")
[Category:1996年夏季奥林匹克运动会篮球运动员](../Category/1996年夏季奥林匹克运动会篮球运动员.md "wikilink")
[Category:2000年夏季奧林匹克運動會籃球運動員](../Category/2000年夏季奧林匹克運動會籃球運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會籃球運動員](../Category/2004年夏季奧林匹克運動會籃球運動員.md "wikilink")
[Category:奧林匹克運動會籃球獎牌得主](../Category/奧林匹克運動會籃球獎牌得主.md "wikilink")