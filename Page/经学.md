**經學** ()
在中文语境里一般特指研究[儒家经典](../Page/儒家.md "wikilink")，解释其字面意義、阐明其蕴含义理的學問。经学是中国古代学术的主体，仅《[四库全书](../Page/四库全书.md "wikilink")》经部就收录了经学著作一千七百七十三部、二万零四百二十七卷。经学保存了大量珍贵的史料，是[儒家学说的核心组成部分](../Page/儒家学说.md "wikilink")。

## 經學的內容

經學研究的工作，主要就是解釋[經書](../Page/十三经.md "wikilink")。所謂「注」，就是對經書原文翻譯解釋，但有些注因為太簡要或年代久遠，因此後人為注再作解釋，稱作「疏」，以此類推後面還有「解」、「集解」、「正義」共五層，都是希望了解經書所欲表達意義的考證行為，但春秋時期的經書內文用字過於簡化，故難免會帶有考證者強烈主觀意識或穿鑿附會的解釋。

自漢武帝獨尊儒術以來研究經學成為最重要的學術活動，加上對通曉經書的人多有禮遇，使投入經學研究的人更多。在[東漢時因此有](../Page/東漢.md "wikilink")「遺子金滿盈，不如教子一經」的說法。《[漢書](../Page/漢書.md "wikilink")‧藝文志》中，五經與儒家著作仍分列在兩個類別，[六朝時](../Page/六朝.md "wikilink")，逐漸產生從七部圖書到四部的過渡，到了《[隋書](../Page/隋書.md "wikilink")‧經籍志》，正式把當時的學術按「經、史、子、集」區分為四種，即以經學為首，這種分類方式，一直到[清代仍為人所接受](../Page/清代.md "wikilink")。

由於古代知識分子數量稀少，故社會地位很高，也是地方民意的重要來源，歷代政權靠武力取得「[法統](../Page/法統.md "wikilink")」之後，均希望能進一步取得“[道统](../Page/道统.md "wikilink")”，意為得到知識分子在價值觀上的認同，增進社會安定。一個最典型的例子就是清朝，異民族要突破「華夷」的思想並不容易，他們以政治力積極運作，如[文字獄](../Page/文字獄.md "wikilink")）、開科取士、獎勵學術、編纂《[四庫全書](../Page/四庫全書.md "wikilink")》、《[明史](../Page/明史.md "wikilink")》、《[大義觉迷錄](../Page/大義觉迷錄.md "wikilink")》），對知識份子軟硬兼施消磨精力以減少反抗。

士人要施展政治抱負，一方面要取得法統上政治力的支持，另一方面也要尋求道统上的立論依據，因此往往透過對經典的詮釋活動，來影響執政者的意識型態，執政者也樂於親近知識份子換取禮賢愛才的美名，故經學成為執政者與士人互動的政治媒介。

## 經學的產生

所谓[儒家经典](../Page/儒家.md "wikilink")，现在一般是指[儒学](../Page/儒学.md "wikilink")[十三经](../Page/十三经.md "wikilink")，亦即《[周易](../Page/周易.md "wikilink")》、《[尚書](../Page/尚書_\(經\).md "wikilink")》、《[诗经](../Page/诗经.md "wikilink")》、《[周礼](../Page/周礼.md "wikilink")》、《[儀禮](../Page/儀禮.md "wikilink")》、《[禮記](../Page/禮記.md "wikilink")》、《[春秋左传](../Page/春秋左传.md "wikilink")》、《[春秋公羊传](../Page/春秋公羊传.md "wikilink")》、《[春秋穀梁传](../Page/春秋穀梁传.md "wikilink")》、《[论语](../Page/论语.md "wikilink")》、《[孝经](../Page/孝经.md "wikilink")》、《[尔雅](../Page/尔雅.md "wikilink")》、《[孟子](../Page/孟子.md "wikilink")》。但早期的儒家经典并不是这十三经。

[春秋末年](../Page/春秋.md "wikilink")（公元前六世纪至五世纪），儒家的创始人[孔子在长期的政治活动失败后](../Page/孔子.md "wikilink")，返回故乡[鲁国](../Page/鲁国.md "wikilink")，编订和整理了一些传统文献，形成了[六经](../Page/六经.md "wikilink")。[司马迁在](../Page/司马迁.md "wikilink")《[史记](../Page/史记.md "wikilink")‧孔子世家》裡指出，孔子编辑了《书》，删定了《诗》，编订了《礼》和《乐》，作了《易》的一部分，并根据鲁国的史料创作了《春秋》。（关于六经是否是孔子所作，长期以来一直有争议。不過一致的方向是同意孔子主導了編輯地位，原始文本則非孔子原作。）自此以后，儒生们就以六经为课本学习儒家思想。在[春秋战国时期](../Page/春秋战国.md "wikilink")，六经就已被儒生公认为宝典。

经学产生于[西汉](../Page/西汉.md "wikilink")。[秦代即设有](../Page/秦代.md "wikilink")[博士官](../Page/博士.md "wikilink")，以「博學之[士](../Page/士.md "wikilink")」為用。由於[秦始皇采纳](../Page/秦始皇.md "wikilink")[李斯的建议的](../Page/李斯.md "wikilink")[焚书坑儒](../Page/焚书坑儒.md "wikilink")，将全国图书以及学术集中到[咸阳城](../Page/咸阳.md "wikilink")，秦亡後，[项羽焚烧](../Page/项羽.md "wikilink")[咸阳](../Page/咸阳.md "wikilink")，致使大量[先秦典籍消失於歷史舞台](../Page/先秦.md "wikilink")，六经除了《[易经](../Page/易经.md "wikilink")》因是[占卜之書而不必毀除之外](../Page/占卜.md "wikilink")，其它幾未能幸免于难。汉代起初，高祖[劉邦并不重视这些儒家经典](../Page/劉邦.md "wikilink")，从[漢文帝](../Page/漢文帝.md "wikilink")[漢景帝时期开始展开了大量的献书和古籍收集工作](../Page/漢景帝.md "wikilink")，部分年长的秦博士和其他儒生，或默诵已遭焚毁的经典而口述謄抄，或把[秦时](../Page/秦.md "wikilink")[書記於身邊的典籍重新拿出](../Page/書記.md "wikilink")，用當年通用的[隸書使之传世](../Page/隸書.md "wikilink")。因为文字、传述和解释体系的不同，产生了不同的学派，但其版本上则基本相同，后来统称为[今文经](../Page/今文经.md "wikilink")。

[汉景帝末年](../Page/汉景帝.md "wikilink")[鲁恭王兴建王府](../Page/鲁恭王.md "wikilink")，拆[孔子](../Page/孔子.md "wikilink")[門第](../Page/門第.md "wikilink")，从孔宅墙中发现《[孝經](../Page/孝經.md "wikilink")》、《[詩經](../Page/詩經.md "wikilink")》等一批经書；[汉武帝时](../Page/汉武帝.md "wikilink")，[河间献王劉德从民间收集了大批的古典文献](../Page/河间献王.md "wikilink")，其中最重要的就是《[周官](../Page/周官.md "wikilink")》，皆收入秘府（即官方皇家圖書館）；[汉宣帝时又有河内女子坏老屋](../Page/汉宣帝.md "wikilink")，得几篇《尚书》。这些出土的文献都是用[战国](../Page/战国.md "wikilink")[古文字书写](../Page/古文.md "wikilink")，与通行的[五经相比](../Page/五经.md "wikilink")，不仅篇数、字数不同，而且内容上也有相当差异，此后即统称为[古文经](../Page/古文经.md "wikilink")。

[汉武帝即位后](../Page/汉武帝.md "wikilink")，为了适应大一统的政治局面和加强中央集权统治，实行了[董仲舒所倡之](../Page/董仲舒.md "wikilink")「罢黜百家、独尊儒术」，改變博士原有制度，增設弟子員，有五经[博士之說](../Page/博士.md "wikilink")。从此儒学独尊，由於樂已無書，《诗》、《书》、《礼》、《易》、《春秋》五经超出了一般典籍的地位，成为崇高的法定经典，也成为士子必读的经典。汉代儒生们即以传习、解释[五经为主业](../Page/五经.md "wikilink")。自此经学正式宣告诞生，可以将经学视为先秦原初儒学的继承和发展。

## 經學的發展

### 两汉经学

[汉武帝时立五经博士](../Page/汉武帝.md "wikilink")，每一经都置若干博士，博士下又有弟子。博士与弟子传习经书，分成若干“师说”，也就是若干流派。武帝时的五经博士共有七家。武帝以后经学日益兴盛，博士的数量也逐渐增加。到了东[汉光武帝时期](../Page/汉光武帝.md "wikilink")，确定了十四家博士。据《[后汉书](../Page/后汉书.md "wikilink")·儒林列传》记载：“于是立五经博士，各以家法教授，《易》有施、孟、梁丘、京氏，《尚书》欧阳、大、小夏侯，《诗》，齐、鲁、韩，《礼》，大、小戴，《春秋》严、颜，凡十四博士。”这十四家都属于[今文经学](../Page/今文经学.md "wikilink")，其官学地位一直保持到东汉末年。

今文经学的特点是微言大義地阐发说明[孔子的思想](../Page/孔子.md "wikilink")，继承和发扬[儒家学说](../Page/儒家.md "wikilink")。今文经学以《[春秋](../Page/春秋_\(史書\).md "wikilink")》为孔子为万世立法的“元经”，其主流就是“[春秋公羊学](../Page/春秋公羊学.md "wikilink")”。公羊学即为《[春秋公羊传](../Page/春秋公羊传.md "wikilink")》里所阐发的微言大义，主要包括大一统、大居正、[大复仇](../Page/大复仇.md "wikilink")、通三统、统三世、更化改制、兴礼诛贼等。汉武帝时期出现了为大一统政治提供了完整的理论体系的公羊学大师[董仲舒和善于把公羊学理论运用于现实政治中的政治家](../Page/董仲舒.md "wikilink")[公孙弘](../Page/公孙弘.md "wikilink")，经过一代代今文经学学者的推阐与实践，以公羊学为代表的今文经学深受汉朝皇帝的重视，始终在汉朝政治中处于主导地位。

今文经学发展到西汉后期，出现了两种趋势：一方面由于董仲舒对于公羊学中灾异、符瑞、天人感应的阐发，今文经学由此逻辑发展的后果即是[谶纬泛滥](../Page/谶纬.md "wikilink")，再加之统治者的迷信与提倡，经学逐渐[神学化](../Page/神学.md "wikilink")；另一方面由于今文经学继承了较多的原初儒学的色彩，其理论内在地包含着对现实的批判，从而越来越不能为逐渐加强的君主专制所容忍。在这种情况下，自西汉中期开始就已经在民间传授的[古文经学兴起](../Page/古文经学.md "wikilink")。

古文经学所依据的经书一般都是西汉中期以后在民间发现的古书，以[魯恭王發](../Page/魯恭王.md "wikilink")[孔子宅之經書為主](../Page/孔子.md "wikilink")，因其是用[战国](../Page/战国.md "wikilink")[六國及以前的](../Page/六國.md "wikilink")[古文字所书写](../Page/古文.md "wikilink")，故称之为[古文经](../Page/古文经.md "wikilink")。古文经学与今文经学并不仅仅是文字篇章的差异，主要在于它们对经书的解释与治学方法的不同。今文经学认为孔子是“为汉制法”的“[素王](../Page/素王.md "wikilink")”，而古文经学认为孔子只是古典文献的整理保存者，是一位“述而不作、信而好古”的先师；今文经学认为六经都是孔子所作，是孔子政治思想所托，其中有许多微言大义，而古文经学则认为六经是上古文化典章制度与圣君贤相政治格言的记录；今文经学注重微言大义，古文经学注重对经文本义的理解和典章制度的阐明。如果说今文经学关注的重心在于政治哲学与历史哲学的话，那么自西汉后期开始与之针锋相对的古文经学所关注的重心就是历史史料学与语言学。

古文经学的兴起最早起自《[春秋谷梁传](../Page/春秋谷梁传.md "wikilink")》，西汉后期曾被立为博士。在[王莽当政时期](../Page/王莽.md "wikilink")，[刘歆极力鼓吹古文经学](../Page/刘歆.md "wikilink")，并使之立为[新朝的博士](../Page/新朝.md "wikilink")。[东汉时期](../Page/东汉.md "wikilink")，古文经学虽然一直没有被立为博士，属于民间学说，但是其影响力越来越大，逐步超出并压倒了今文经学。由于今文经学发展后期日趋繁琐，例如“曰若稽古”四个字可以解释十万字，又有所谓“师法”“家法”的束缚，再加之其与[谶纬纠缠过深](../Page/谶纬.md "wikilink")，使得人们逐渐遗弃了今文经学。而古文经学一来较少受“师法”“家法”的制约，较为自由也较为简明；二来与谶纬瓜葛较少，较为理性；三来其放弃了今文经学的批判性，对君主专制的维护更有优势，所以在今古文经学的长期斗争中，古文经学取得了最后的胜利。东汉的古文经学大师有[贾逵](../Page/贾逵.md "wikilink")、[许慎](../Page/许慎.md "wikilink")、[马融](../Page/马融.md "wikilink")、[服虔](../Page/服虔.md "wikilink")、[卢植等](../Page/卢植.md "wikilink")，弟子众多，影响很大。而今文经学只有[何休取得较大的成就](../Page/何休.md "wikilink")，他的《[春秋公羊解诂](../Page/春秋公羊解诂.md "wikilink")》是唯一一部完整流传至今的今文经。

在今古文经学的长期争辩过程中，互相也在逐渐地渗透，互相融合。东汉初年（公元79年）召开的[白虎观会议就是一个官方召开的企图弥合今古文经学异同的重要的学术会议](../Page/白虎观会议.md "wikilink")。会议的成果由[班固写成](../Page/班固.md "wikilink")《[白虎通德论](../Page/白虎通義.md "wikilink")》，又称《白虎通义》，简称《白虎通》一书。《白虎通》是以今文经学为基础，初步实现了经学的统一。东汉末年，古文经学的集大成者[郑玄](../Page/郑玄.md "wikilink")，网罗众家、遍注群经，对今古文经学进行了全面总结，自成一家之言。郑玄以古文经学为基础，但又能吸收今文经学中的优点，态度严谨，实事求是，无征不信，从而超过了前人。自此以后郑学兴盛，这不仅标志着今古文经学之争的终结，也标志着汉代经学的衰亡，之后今文经学也随之消失。

[汉朝是经学最为昌盛的时代](../Page/汉朝.md "wikilink")，朝野内外诵读经书蔚然成风，《[汉书](../Page/汉书.md "wikilink")·韦贤传》引民间谚语说“遗子黄金满赢，不如一经”。汉朝的“以经义决狱”是汉朝经学与王朝政治相结合的一大特色，也是汉朝经学繁盛的一大标志。儒生通过司法实践并官学私学教育，移风易俗，把经学思想深深的植入了普通民众之中。

### 魏晋南北朝经学

魏晋南北朝时期是经学由衰落走向分离的时期。在[曹魏时期](../Page/曹魏.md "wikilink")，出现了王学与郑学之争。王学，是指[王肅所创立的经学体系](../Page/王肅_\(三國\).md "wikilink")。王肃是[司马炎的外祖父](../Page/司马炎.md "wikilink")，所以王学获得了司马氏的支持，他注解的《尚书》、《诗》、《论语》、三《礼》和《左氏春秋》以及其父所作的《易传》都被列为官学。王学和郑学之间的纷争，并不是纯粹的学术争论，而带有强烈的政治斗争的意味。这场纷争同时也标志着两汉经学的衰落。

魏晋时期在经学取得成就较大的还有[王弼](../Page/王弼.md "wikilink")、[何晏等](../Page/何晏.md "wikilink")。王弼注《周易》，摆脱了汉代用“象数”和谶纬解说《周易》的老路，开创了用义理、思辨哲学解说《周易》的新路，这是经学史上一次重大变革。何晏所作《论语集解》收集了汉以来各家之说，对后世影响很大。这一时期经学的特点是经学逐渐[玄学化](../Page/玄学.md "wikilink")。

[南北朝时期经学也随着政治上的南北对立而分立为南学和北学](../Page/南北朝.md "wikilink")。据《北史·儒林传》记载，南学《周易》尊王弼，摒弃象数、发挥义理，《尚书》流行《孔传古文尚书》，《左传》盛行[杜预撰](../Page/杜预.md "wikilink")《春秋左传集解》；北学《周易》、《尚书》主郑玄，《左传》主[服虔](../Page/服虔.md "wikilink")。“南人简约，得其英华；北学深芜，穷其枝叶。”从学术风格上讲，南学受玄学和[佛学影响比较大](../Page/佛学.md "wikilink")，能博取众家之长，又喜标新立异，反映了其哲学思辨能力的提高，而北学受北方游牧民族质朴风尚的影响，保持了汉朝经学以章句训诂为宗的特点。

### 隋唐經學

经学由汉而唐，有古今文学，郑学、王学，南学、北学之争。[唐代則基於取士的需要](../Page/唐代.md "wikilink")，以國家的力量來推行經學，[孔穎達的](../Page/孔穎達.md "wikilink")《[五經正義](../Page/五經正義.md "wikilink")》是這時代的代表著作，同時也是鄭玄以來漢學的總結與高峰。它的编纂一方面成为士人的教科书，另一方面则象徵着政府在圣统上的合法性建立，影响了後来明代《[五经大全](../Page/五经大全.md "wikilink")》、《[永乐大典](../Page/永乐大典.md "wikilink")》以及清代《[四库全书](../Page/四库全书.md "wikilink")》等等政府主导下的经典编辑，至唐後五代之世，宋代晁迥之前，無人異詞。这个时期的经学也进入了韩国以及日本，成为诸国所仿效的法典，日本天皇更运用经学确立了他的政治法统地位。

### 宋朝經學

[宋代](../Page/宋代.md "wikilink")[理學興起](../Page/理學.md "wikilink")，自[晁迥之後](../Page/晁迥.md "wikilink")，理學家們以重新詮釋古代經典的方式，以疑經、改經、刪經來進行回歸先秦經典的活動，闡發他們的主張，或保守、或激進。此時期，出現以《論語》、《孟子》加上從禮記中抽出《大學》、《中庸》合稱的「四書」，因為被界定為還原聖人思想的需要而被重新定位，成为超越五經的思想著作。

### 明朝經學

[明代延续了宋代的理学路线](../Page/明代.md "wikilink")，一方面政府编纂官方版经典文本，另一方面南方的经学力量逐渐抬头，例如[王阳明即是最为重要的明代理学家](../Page/王阳明.md "wikilink")。明末经学家几乎都带有王阳明式的豪气，纷纷组织学社，发动朝野[清议](../Page/清议.md "wikilink")、舆论、弹劾，与腐败的政府、宦官对抗，形成激烈的流血党争，种下明朝灭亡的内部因子。另外一部份的明朝流亡者、海盗、商人与士人，以日本为重要根据地，进行活动，连带将日本的经学进一步推展，例如孔子、孟子、王阳明皆被各粝与幕府所尊崇、[朱舜水以](../Page/朱舜水.md "wikilink")《春秋》为核心的[尊王攘夷思想](../Page/尊王攘夷.md "wikilink")，影响了[水户学的政治论调](../Page/水户学.md "wikilink")，进而在[明治维新引起了重大的思想浪潮](../Page/明治维新.md "wikilink")。

### 清朝經學

[清代初期受到](../Page/清代.md "wikilink")[明朝灭亡的影响](../Page/明朝.md "wikilink")，士人开始思考[王阳明路线的弊病](../Page/王阳明.md "wikilink")，另一方面因为[清朝](../Page/清朝.md "wikilink")[皇帝实施](../Page/皇帝.md "wikilink")[文字狱](../Page/文字狱.md "wikilink")、编纂《[四库全书](../Page/四库全书.md "wikilink")》、《[明史](../Page/明史.md "wikilink")》等[思想控制手段](../Page/思想控制.md "wikilink")，經學中较不介入政治的的[实学与](../Page/实学.md "wikilink")[考据路线](../Page/考据.md "wikilink")，遂特別發達，尤其重視以大量的古代典籍以及[文字學](../Page/文字學.md "wikilink")、[聲韻學](../Page/聲韻學.md "wikilink")、[訓詁學等方式來研究經書](../Page/訓詁學.md "wikilink")，甚至進一步考證某些經書的真偽、划分学术流派，可以说，清代初期的经学思想是被明朝遗老们如[黄宗羲](../Page/黄宗羲.md "wikilink")、[顾炎武等人所影响主导的](../Page/顾炎武.md "wikilink")。清末基於时代需求，主张激进改革的[公羊家大盛](../Page/公羊家.md "wikilink")，其中又以[常州学派的](../Page/常州学派.md "wikilink")[康有为](../Page/康有为.md "wikilink")、[梁启超等最为有力活跃的人物](../Page/梁启超.md "wikilink")。

[皮锡瑞在](../Page/皮锡瑞.md "wikilink")《[经学历史](../Page/经学历史.md "wikilink")》中曾说明清代经学“凡三变”，清初，以[宋学为主](../Page/宋学.md "wikilink")；[乾隆以后](../Page/乾隆.md "wikilink")，[许](../Page/許慎.md "wikilink")[郑之学大明](../Page/鄭玄.md "wikilink")，是为专门汉学。嘉道以后，“汉十四博士今文说，自[魏](../Page/魏.md "wikilink")、[晋沦亡千余年](../Page/晋.md "wikilink")，至今日而复明。实能述[伏](../Page/伏生.md "wikilink")、[董之遗文](../Page/董仲舒.md "wikilink")，寻[武](../Page/漢武帝.md "wikilink")、[宣之绝轨](../Page/漢宣帝.md "wikilink")。是为[西汉今文之学](../Page/西汉.md "wikilink")。学愈进而愈古，义愈推而愈高；屡迁而返其初，一变而至于道。”又說：“[乾](../Page/乾隆.md "wikilink")[嘉以后](../Page/嘉慶.md "wikilink")，[阳湖庄氏乃讲今文之学](../Page/阳湖庄氏.md "wikilink")，[孔广森治](../Page/孔广森.md "wikilink")《[公羊春秋](../Page/公羊春秋.md "wikilink")》，[孙星衍于](../Page/孙星衍.md "wikilink")《[尚书](../Page/尚书.md "wikilink")》兼治今、古文，[陈乔枞治](../Page/陈乔枞.md "wikilink")《[今文尚书](../Page/今文尚书.md "wikilink")》、齐、鲁、韩三家《[诗](../Page/诗.md "wikilink")》，[魏源作](../Page/魏源.md "wikilink")《[书古微](../Page/书古微.md "wikilink")》、《[诗古微](../Page/诗古微.md "wikilink")》、《[公羊古微](../Page/公羊古微.md "wikilink")》，[凌曙作](../Page/凌曙.md "wikilink")《[公羊礼证](../Page/公羊礼证.md "wikilink")》、《[春秋繁露注](../Page/春秋繁露注.md "wikilink")》，[陈立作](../Page/陈立.md "wikilink")《[公羊义疏](../Page/公羊义疏.md "wikilink")》，王馆长(按，指时任[湖南师范馆馆长的](../Page/湖南师范馆馆长.md "wikilink")[王先谦](../Page/王先谦.md "wikilink"))作《[三家诗义疏](../Page/三家诗义疏.md "wikilink")》，已成《[周南](../Page/周南.md "wikilink")》、《[召南](../Page/召南.md "wikilink")》、《[邶风](../Page/邶风.md "wikilink")》，锡瑞作《[今文尚书考证](../Page/今文尚书考证.md "wikilink")》、《[尚书大传疏证](../Page/尚书大传疏证.md "wikilink")》。”

兩廣總督[阮元輯](../Page/阮元.md "wikilink")《[皇清經解](../Page/皇清經解.md "wikilink")》，收七十三家，記書一百八十八種，凡一千四百卷。此書是匯集[儒家經學經解之大成](../Page/儒家.md "wikilink")，是對乾嘉學術的一次全面總結。

### 民國以後的經學

到[民國時代以後](../Page/民國.md "wikilink")，由於大量的西学、政治运动取代了原来的经学思想，兩者的衝擊之中，產生了諸多主張，一般而言，全面排斥西學的想法已經不復存在，但仍存有「[中國文化本位](../Page/十教授宣言.md "wikilink")」與[全盤西化的路線爭執](../Page/全盤西化.md "wikilink")。[胡適在](../Page/胡適.md "wikilink")〈論六經不夠作領袖人才的來源〉一文就說：“儒家經典中，除《論》《孟》及《禮記》之一部分外，皆古史料而已。”\[1\]，胡適因把《詩經》當成文學作品，不是一部經典\[2\]。古代經書的權威遂下降，經學也就逐漸式微，民國二十年代，[何鍵](../Page/何鍵.md "wikilink")、[陳濟棠等倡議學校恢復經學課程](../Page/陳濟棠.md "wikilink")，遭到不少反對。

## 参看

  - [西漢經學傳承列表](../Page/西漢經學傳承列表.md "wikilink")
  - [马王堆帛书](../Page/马王堆帛书.md "wikilink")
  - [易學](../Page/易學.md "wikilink")

## 注釋

<references/>

## 參考書目

  - [本田成之](../Page/本田成之.md "wikilink")：《中國經學史》（上海：上海書店出版社，2001）。
  - [徐復觀](../Page/徐復觀.md "wikilink")：《中國經學史的基礎》（台北：台灣學生書局，1982）。
  - 李学勤：〈[国学与经学的几个问题](http://www.nssd.org/articles/article_read.aspx?id=21507433)〉。
  - [艾爾曼](../Page/艾爾曼.md "wikilink")（Benjamin
    Elman）：〈[清代科舉與經學的關係](https://www.princeton.edu/~elman/documents/Civil%20Examinations%20and%20Classicism)〉。
  - 羅志田：〈[清季民初經學的邊緣化與史學的走向中心](http://ccsdb.ncl.edu.tw/ccs/image/01_015_002_01_01.pdf)〉。

[Category:經學](../Category/經學.md "wikilink")

1.  《胡適文存》第四集
2.  《胡適文存》第四集，卷四〈談談詩經〉