**經濟**一詞用於統稱一定範圍（國家、區域⋯⋯等）內，組織一切[生產](../Page/生產.md "wikilink")、分配、[流通](../Page/流通.md "wikilink")、[消費活動和關係的](../Page/消費.md "wikilink")[系統](../Page/系統.md "wikilink")。另一涵意是隐藏著不平等的欲望交换，經世濟民則是和諧地分配不平等化。而研究經濟問題、探討經濟發展規律、解釋經濟現象成因的[社會科學](../Page/社會科學.md "wikilink")，即稱為[經濟學](../Page/經濟學.md "wikilink")。

## 詞源

### 英文

英文中****源自古希腊语****（家政术）。****为家庭的意思，****是方法或者习惯的意思。因此，其本来含义是指治理家庭[财物的方法](../Page/财物.md "wikilink")，到了近代扩大为治理国家的范围，为了区别于之前的用法也被称为[政治经济学](../Page/政治经济学.md "wikilink")。这个名称后来被[马歇尔改回](../Page/阿尔弗莱德·马歇尔.md "wikilink")[经济学](../Page/经济学.md "wikilink")。到了现代，如果单称经济学的话，是在政治经济学或者更广的层面来考虑经济，因此一般在指经济学的时候，经济学与政治经济学是同义的。

### 中文

古時以「**食貨**」一詞泛指經濟財政之事\[1\]，例如[漢書以降歷代史書中的](../Page/漢書.md "wikilink")「[食貨志](../Page/食貨志.md "wikilink")」記載了各時代的經濟現況（漢書之前的[史記則名之為](../Page/史記.md "wikilink")「平準書」）\[2\]。

至於**经济**的辭源出自[东晋時代](../Page/东晋.md "wikilink")[葛洪](../Page/葛洪.md "wikilink")《[抱朴子](../Page/抱朴子.md "wikilink")・内篇》中的“经世济俗”，意为治理天下，救济百姓。[隋朝](../Page/隋朝.md "wikilink")[王通在](../Page/王通_\(隋朝\).md "wikilink")《[文中子](../Page/文中子.md "wikilink")・礼乐篇》则提出了“经济”一词：“皆有经济之道，谓**经國济民**”。后人遂将“经济”一词作为“经國济民”的省略语，作为“政治统治”和“社会管理”的同义词。
[清末](../Page/清.md "wikilink")[戊戌政变后](../Page/戊戌政变.md "wikilink")，改革[科举制度](../Page/科举.md "wikilink")，开“经济特科”，就是沿用了“经济”的本义。

[明](../Page/明朝.md "wikilink")[清之际](../Page/清朝.md "wikilink")，政治秩序的巨大变动使得以[王夫之](../Page/王夫之.md "wikilink")、[顧炎武](../Page/顧炎武.md "wikilink")、[黄梨洲等为代表的一部分儒家学者开始理论反思](../Page/黄梨洲.md "wikilink")，批判[程朱理学和](../Page/程朱理学.md "wikilink")[心学末流](../Page/心学.md "wikilink")，提出学术要“经世致用”的口号。这种学术思想传播到了[日本](../Page/日本.md "wikilink")，影响了日本[江户时代的学术](../Page/江户时代.md "wikilink")，“经世论”开始在一些学者中流行。

但江户后期至明治时期，欧美思潮涌入日本。[神田孝平首先在](../Page/神田孝平.md "wikilink")《经济小学》一書中用经济一词翻译英文的“political
economy”\[3\]，然而也有人認為是[福澤諭吉首先使用這樣的译法](../Page/福澤諭吉.md "wikilink")\[4\]。這段時期，日本学者开始用经济一词指代因货币经济发展而带来的种种活动，并有了“经济指维系社会生活所必须的生产、消费、交易等活动”的説法。

此類与金钱、财物等实际问题相关的定义逐渐在日本流行，但這種用法與明清时期中文语境中“经济”的原始用法不同。當時日本亦有另一種译法“資生”，但是並未被普遍接受。而後用「經濟」一詞翻譯英文的「」的这种译法又被[梁啟超引入汉语](../Page/梁啟超.md "wikilink")，也取代了[嚴復在](../Page/嚴復.md "wikilink")《[原富](../Page/原富.md "wikilink")》中的译法“計學”或是傳統的“生計學”、“平準學”，并进而在整个[汉字文化圈内逐渐取代了经济一词的原本含义](../Page/汉字文化圈.md "wikilink")。

反對[和製漢語者彭文祖在](../Page/和製漢語.md "wikilink")《盲人瞎马之新名词》
寫到，反對英文的**economy**翻譯成**经济**，認為是中國人瞎眼盲從。\[5\]

### 其他用法

经济是人类对有限资源做最适分配的行为。

现代经济伴随着[货币](../Page/货币.md "wikilink")。当中被[生产](../Page/生产.md "wikilink")、[流通](../Page/流通.md "wikilink")、[交换的物品被称为](../Page/交换.md "wikilink")[商品](../Page/商品.md "wikilink")，其中既包括苹果和衣服等有形的物品，也包括法律咨询、[邮政递送等无形的](../Page/邮政.md "wikilink")[服务](../Page/服务.md "wikilink")，还有[证券等的权利](../Page/证券.md "wikilink")，以及信息。

### 劫富濟貧爭議

  - [河北](../Page/河北.md "wikilink")[燕子李三善長搶劫並散布財物給貧困者](../Page/燕子李三.md "wikilink")，但其行為經辯護律師的文件發現其大部分財物都用在自身的揮霍，只有少量的財物才進行佈施，顯然和其個人主張目的有所出入。另外[蘇聯](../Page/蘇聯.md "wikilink")[大清洗想要藉由沒收富人的財物來重新分配](../Page/大清洗.md "wikilink")，結果造成社會極端的不安，搶到財物的人們又為了對方身上的財物彼此相互爭鬥，造成社會動盪及大量的傷亡。\[6\]

## 國家

### 中國

## 经济活动

  - [生产](../Page/生产.md "wikilink")（[国内生产总值](../Page/国内生产总值.md "wikilink")、[国民生产总值](../Page/国民生产总值.md "wikilink")）
      - [消費](../Page/消費.md "wikilink")、[投資](../Page/投資.md "wikilink")、[貿易](../Page/貿易.md "wikilink")、[入口](../Page/入口.md "wikilink")、[出口](../Page/出口贸易.md "wikilink")、[收入分配](../Page/收入分配.md "wikilink")

## 经济用语

  - [需求](../Page/需求.md "wikilink") -
    [供给](../Page/供给.md "wikilink")（[供给和需求](../Page/供给和需求.md "wikilink")）
  - [有效需求](../Page/有效需求.md "wikilink")
  - [民需](../Page/民需.md "wikilink")・[军需](../Page/军需.md "wikilink")・[军需产业](../Page/军需产业.md "wikilink")
  - [批发](../Page/批发.md "wikilink") - [零售](../Page/零售.md "wikilink")
  - [销售](../Page/销售.md "wikilink")
  - [宣传](../Page/宣传.md "wikilink")
  - [营销](../Page/营销.md "wikilink")
  - [经济周期](../Page/经济周期.md "wikilink")
  - [产业](../Page/产业.md "wikilink")
  - [资本](../Page/资本.md "wikilink") -
    [投资](../Page/投资.md "wikilink")・[股票](../Page/股票.md "wikilink")\*
    [劳动](../Page/劳动.md "wikilink") - [雇用](../Page/雇用.md "wikilink")
  - [货币](../Page/货币.md "wikilink") - [利息](../Page/利息.md "wikilink")
  - [通货膨胀](../Page/通货膨胀.md "wikilink") -
    [通货紧缩](../Page/通货紧缩.md "wikilink")
      - [滞胀](../Page/滞胀.md "wikilink")
  - [帕雷托法则](../Page/帕雷托法则.md "wikilink")
  - [马太效应](../Page/马太效应.md "wikilink")
  - [蝴蝶效应](../Page/蝴蝶效应.md "wikilink")
  - 经济体制
      - [市场经济](../Page/市场经济.md "wikilink")
      - [计划经济](../Page/计划经济.md "wikilink")
      - [混合经济](../Page/混合经济.md "wikilink")
  - [垄断](../Page/垄断.md "wikilink")
      - [卡特尔](../Page/卡特尔.md "wikilink")
      - [康采恩](../Page/康采恩.md "wikilink")
      - [托拉斯](../Page/托拉斯.md "wikilink")
      - [辛迪加](../Page/辛迪加.md "wikilink")
  - [金本位](../Page/金本位.md "wikilink")
  - [泡沫经济](../Page/泡沫经济.md "wikilink")
  - [倒金字塔式經濟](../Page/倒金字塔式經濟.md "wikilink")
  - [浮动汇率制](../Page/浮动汇率制.md "wikilink") -
    [固定汇率制](../Page/固定汇率制.md "wikilink")
  - [兑换纸币](../Page/兑换纸币.md "wikilink")
  - [股份公司](../Page/股份公司.md "wikilink")・[合资公司](../Page/合资公司.md "wikilink")・[有限公司](../Page/有限公司.md "wikilink")
  - [金融](../Page/金融.md "wikilink")
  - [金融机构](../Page/金融机构.md "wikilink")
      - [商业银行](../Page/商业银行.md "wikilink")
      - [投资银行](../Page/投资银行.md "wikilink")
  - [中央银行](../Page/中央银行.md "wikilink")
  - [股票](../Page/股票.md "wikilink")
  - [债券](../Page/债券.md "wikilink")
  - [期货](../Page/期货.md "wikilink")
  - [经济学途径](../Page/经济学途径.md "wikilink")

## 参考文献

## 外部链接

## 参见

  - [經濟體](../Page/經濟體.md "wikilink")
  - [经济学](../Page/经济学.md "wikilink")
  - [产业](../Page/产业.md "wikilink")、[贸易](../Page/贸易.md "wikilink")、[金融](../Page/金融.md "wikilink")
  - [政治](../Page/政治.md "wikilink")、[社会](../Page/社会.md "wikilink")、[文化](../Page/文化.md "wikilink")

[经济](../Category/经济.md "wikilink")
[Category:经济学](../Category/经济学.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")

1.  [中華民國教育部重編國語辭典「食貨」詞條](http://dict.revised.moe.edu.tw/cgi-bin/newDict/dict.sh?cond=%25AD%25B9%25B3f&pieceLen=50&fld=1&cat=&ukey=1193231764&serial=4&recNo=0&op=f&imgFont=1)
2.  [中華民國教育部重編國語辭典「食貨志」詞條](http://dict.revised.moe.edu.tw/cgi-bin/newDict/dict.sh?cond=%25AD%25B9%25B3f&pieceLen=50&fld=1&cat=&ukey=1193231764&serial=3&recNo=1&op=f&imgFont=1)
3.
4.
5.
6.  \[[http://www.epochtimes.com/b5/16/5/30/n7945307.htm\]馮學榮：劫富濟貧為甚麼不對](http://www.epochtimes.com/b5/16/5/30/n7945307.htm%5D馮學榮：劫富濟貧為甚麼不對)
    人氣: 874 【字號】 大 中 小 更新: 2016-05-30 5:38 PM 標籤: 劫富濟貧, 蘇聯