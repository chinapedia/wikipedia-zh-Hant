**结婚纪念日**是夫妇两人结婚纪念的日子，通常以「日期」為紀念節點。例如2009年1月15日結婚，那每年的1月15日就是該夫婦兩人的結婚紀念日。

近現代，部分人们在结婚周年的时候，采用特定的礼物或名称来纪念他们的[婚姻](../Page/婚姻.md "wikilink")。例如[禧年就發表過](../Page/禧年_\(犹太节日\).md "wikilink")25周年[银婚](../Page/银.md "wikilink")，30周年[珍珠婚](../Page/珍珠.md "wikilink")，35周年：[珊瑚婚](../Page/珊瑚.md "wikilink")，40周年[红宝石婚](../Page/红宝石.md "wikilink")，45周年：[蓝宝石婚](../Page/蓝宝石.md "wikilink")，50周年：[金婚](../Page/金.md "wikilink")，55周年：[祖母绿婚及](../Page/祖母绿.md "wikilink")60周年[钻石婚的說法](../Page/钻石.md "wikilink")。

## 德语区的節日紀念特定物说法

在[德语区](../Page/德语.md "wikilink")，人们不仅为庆祝结婚周年，甚至还庆祝半年或四分之一年。为此，人们还设定了各种新奇的礼物。

  - 0 - [绿色或](../Page/绿色.md "wikilink")[白色](../Page/白色.md "wikilink")
  - ½ - [梦](../Page/梦.md "wikilink")
  - ¾ - [啤酒](../Page/啤酒.md "wikilink")
  - 1 - [纸](../Page/纸.md "wikilink")
  - 2 - [棉花](../Page/棉花.md "wikilink")
  - 3 -
    [皮革](../Page/皮革.md "wikilink")，[水果](../Page/水果.md "wikilink")，[冷杉](../Page/冷杉.md "wikilink")，[小麦](../Page/小麦.md "wikilink")
  - 4 -
    [丝绸](../Page/丝绸.md "wikilink")，[琥珀](../Page/琥珀.md "wikilink")，[亚麻](../Page/亚麻.md "wikilink")，[葡萄酒](../Page/葡萄酒.md "wikilink")，[蜡](../Page/蜡.md "wikilink")
  - 5 - [木头](../Page/木头.md "wikilink")：

<!-- end list -->

  -
    木头雕像表示婚姻进入稳定期。
      - 有孩子：[勤奋](../Page/勤奋.md "wikilink")
      - 没有孩子：[牛](../Page/牛.md "wikilink")，[橡胶](../Page/橡胶.md "wikilink")，[风向袋](../Page/风向袋.md "wikilink")（windsock）

<!-- end list -->

  - 6 - [锡](../Page/锡.md "wikilink")，[糖](../Page/糖.md "wikilink")
  - 6¼ - [羊肉](../Page/羊肉.md "wikilink")
  - 7 -
    [铜](../Page/铜.md "wikilink")，[羊毛](../Page/羊毛.md "wikilink")，[黄铜](../Page/黄铜.md "wikilink")
  - 8 -
    薄[金属](../Page/金属.md "wikilink")，[玉米片](../Page/玉米.md "wikilink")，[镍](../Page/镍.md "wikilink")，[石头](../Page/石头.md "wikilink")，[陶器](../Page/陶器.md "wikilink")，[盐](../Page/盐.md "wikilink")，[青铜](../Page/青铜.md "wikilink")
  - 9 -
    [陶器](../Page/陶器.md "wikilink")，[瓷器](../Page/瓷器.md "wikilink")，[玻璃](../Page/玻璃.md "wikilink")，[水晶](../Page/水晶.md "wikilink")，[牛](../Page/牛.md "wikilink")，[水](../Page/水.md "wikilink")，[牧草](../Page/牧草.md "wikilink")
  - 10 - [玫瑰](../Page/玫瑰.md "wikilink")，木头，青铜，玻璃，薄金属，锡，牛

<!-- end list -->

  -
    适合[爱情之花](../Page/爱情.md "wikilink")：玫瑰的日子。
    在德国北部，亲友和邻居会在结婚十年的夫妻的家门口装饰花环。

<!-- end list -->

  - 11 -
    [钢](../Page/钢.md "wikilink")，[珊瑚](../Page/珊瑚.md "wikilink")，[忏悔](../Page/忏悔.md "wikilink")
  - 12 - 镍，丝绸，泥土，亚麻
  - 12½ - [欧芹](../Page/欧芹.md "wikilink")（parsley），青铜，紫罗兰
  - 13 -
    [紫罗兰](../Page/紫罗兰.md "wikilink")（violets），[百合](../Page/百合.md "wikilink")，盐，花边
  - 14 -
    [象牙](../Page/象牙.md "wikilink")，[蓝色](../Page/蓝色.md "wikilink")，[玛瑙](../Page/玛瑙.md "wikilink")
  - 15 -
    水晶，[衣服](../Page/衣服.md "wikilink")，[抹布](../Page/抹布.md "wikilink")（rags），玻璃，瓶子
  - 16 - [蓝宝石](../Page/蓝宝石.md "wikilink")
  - 17 - [兰花](../Page/兰花.md "wikilink")
  - 18 - [绿松石](../Page/绿松石.md "wikilink")（turquoise）
  - 19 -
    [印花棉布](../Page/印花棉布.md "wikilink")（Cretonne），[珍珠蚌](../Page/珍珠蚌.md "wikilink")（mother-of
    pearl）
  - 20 - [瓷器](../Page/瓷器.md "wikilink")，[菊花](../Page/菊花.md "wikilink")
  - 21 -
    [书](../Page/书.md "wikilink")，[猫眼石](../Page/猫眼石.md "wikilink")（opal）
  - 22 - 青铜，[电气石](../Page/电气石.md "wikilink")（tourmaline）
  - 23 -
    [绿玉](../Page/绿玉.md "wikilink")（beryl），[钛](../Page/钛.md "wikilink")
  - 24 - [缎子](../Page/缎子.md "wikilink")（satin），丝绸
  - 25 - [银](../Page/银.md "wikilink")

<!-- end list -->

  -
    婚姻已经走过四分之一个[世纪](../Page/世纪.md "wikilink")。这一天，所有的东西都装饰成银色，代表婚姻将更加持久。

<!-- end list -->

  - 26 - [玉](../Page/玉.md "wikilink")，[橡树](../Page/橡树.md "wikilink")
  - 27 -
    [黄麻](../Page/黄麻.md "wikilink")（jute），[桃花心木](../Page/桃花心木.md "wikilink")（mahogany）
  - 28 - [丁香](../Page/丁香.md "wikilink")，镍
  - 29 -
    [天鹅绒](../Page/天鹅绒.md "wikilink")（velvet），[乌木](../Page/乌木.md "wikilink")（ebony）
  - 30 - [珍珠](../Page/珍珠.md "wikilink")

<!-- end list -->

  -
    一年又一年，就像串起来的珍珠：因此，丈夫会送给他的妻子一条珍珠项链。

<!-- end list -->

  - 31 -
    [羊皮](../Page/羊皮.md "wikilink")（basan），[椴树](../Page/椴树.md "wikilink")
  - 32 -
    [肥皂](../Page/肥皂.md "wikilink")，[青金石](../Page/青金石.md "wikilink")，[铜](../Page/铜.md "wikilink")
  - 33 - [斑岩](../Page/斑岩.md "wikilink")（porphyry），锡
  - 33½ - [大蒜](../Page/大蒜.md "wikilink")，[蔬菜](../Page/蔬菜.md "wikilink")
  - 34 - [琥珀](../Page/琥珀.md "wikilink")，[祖母绿](../Page/祖母绿.md "wikilink")
  - 35 -
    [帆布](../Page/帆布.md "wikilink")（canvas），珊瑚，玉，亚麻，衣服，[红宝石](../Page/红宝石.md "wikilink")
  - 36 -
    [月亮](../Page/月亮.md "wikilink")，[棉布](../Page/棉布.md "wikilink")（muslin），祖母绿
  - 37 - [孔雀石纸](../Page/孔雀石.md "wikilink")
  - 37½ - [铝](../Page/铝.md "wikilink")
  - 38 - [火](../Page/火.md "wikilink")，[水银](../Page/水银.md "wikilink")
  - 39 -
    [太阳](../Page/太阳.md "wikilink")，[绉纱](../Page/绉纱.md "wikilink")（crepe）
  - 40 - 红宝石，[石榴石](../Page/石榴石.md "wikilink")（garnet）

<!-- end list -->

  -
    四十年的婚姻也没有磨灭爱情之火：因此，结婚戒指上需要添上一枚红宝石。

<!-- end list -->

  - 41 -
    [桦树](../Page/桦树.md "wikilink")（birch），[铁](../Page/铁.md "wikilink")
  - 42 - 石榴石，珍珠蚌
  - 43 -
    [铅](../Page/铅.md "wikilink")，[法兰绒](../Page/法兰绒.md "wikilink")（flannel）
  - 44 -
    [恒星](../Page/恒星.md "wikilink")，[黄玉](../Page/黄玉.md "wikilink")（topaz）
  - 45 -
    黄铜，蓝宝石，[火绒草](../Page/火绒草.md "wikilink")（edelweiss），[铂](../Page/铂.md "wikilink")，电气石
  - 46 -
    [熏衣草](../Page/熏衣草.md "wikilink")（lavender），[大理石](../Page/大理石.md "wikilink")
  - 47 - [开司米](../Page/开司米.md "wikilink")（cashmere），蓝宝石
  - 48 -
    [紫水晶](../Page/紫水晶.md "wikilink")（amethyst），[王冠](../Page/王冠.md "wikilink")（diadem）
  - 49 -
    [天王星](../Page/天王星.md "wikilink")，[雪松](../Page/雪松.md "wikilink")（cedar）
  - 50 - [金](../Page/金.md "wikilink")

<!-- end list -->

  -
    婚姻变得如此珍贵，就像金子一样闪闪发光。通常，人们将再进行一次宗教仪式。

<!-- end list -->

  - 51 - [柳树](../Page/柳树.md "wikilink")
  - 52 - 黄玉
  - 53 - [天王星](../Page/天王星.md "wikilink")
  - 54 - [宙斯](../Page/宙斯.md "wikilink")
  - 55 - 蓝宝石，铂，[宝石](../Page/宝石.md "wikilink")，祖母绿
  - 60 - [钻石](../Page/钻石.md "wikilink")

<!-- end list -->

  -
    钻石象征了爱情，力量，和不朽的光辉。

<!-- end list -->

  - 61 - [榆树](../Page/榆树.md "wikilink")
  - 62 - [绿玉](../Page/绿玉.md "wikilink")（aquamarine）
  - 63 - 水银
  - 64 - [天堂](../Page/天堂.md "wikilink")
  - 65 - 铁

<!-- end list -->

  -
    婚姻已经经历了大约人的一生：这需要铁一般的意志。

<!-- end list -->

  - 67½ - 石头
  - 70 - 铂，铜

<!-- end list -->

  -
    神给予了这对夫妻非常罕有的一生。

<!-- end list -->

  - 72½ - 宝石

<!-- end list -->

  -
    悠长的一生，荣辱与共，就要结束了。其价值超过了任何贵重的宝石。

<!-- end list -->

  - 75 - [权杖](../Page/权杖.md "wikilink")（crown
    jewels），[镭](../Page/镭.md "wikilink")，石头，[雪花石膏](../Page/雪花石膏.md "wikilink")（alabaster），铁
  - 80 - 橡树
  - 100 - 天堂

## 俄罗斯結婚特定代表物

1.  结婚当日：绿婚（新婚夫妇须在庭院 中种一小树，像征期待爱情之树开花结果。俄语中的“绿”，有“年轻”或“不成熟”之意）；
2.  1年：花布婚（Ситцевая свадьба，新婚夫妇互赠花布、手帕，像征今后生活如花似锦）；
3.  3年：皮婚（Кожаная свадьба，表示爱情经受住考验，具备一定韧性）；
4.  5年：木婚（Деревянная свадьба，人们向夫妇赠送 各种木制礼品，像征婚姻犹如幼苗长成林木，祝愿爱情 如木材般坚韧巩固）；
5.  6年半：锌婚（Цинковая свадьба，像征婚姻象锌制品一样经常“开光”，赠以锌制品）；
6.  7年：铜婚（Медная свадьба，夫妻交换铜币）；
7.  8年：白铁婚（Жестяная свадьба，亲友赠以白铁炊具）；
8.  9年：锡婚；
9.  10年：玫瑰婚（Розовая свадьба，以玫瑰花代表纯洁坚贞的爱情）；
10. 15年：水晶婚（Хрустальная свадьба）；
11. 20年：陶瓷婚（Фарфоровая свадьб）；
12. 25年：银婚（Серебряная свадьба）；
13. 30年：珍珠婚（Жемчужная свадьба）；
14. 35年：亚麻婚（Полотняная(льняная) свадьба）；
15. 37年半：铝婚（Алюминевая свадьба）；
16. 40年：红宝石婚（Рубиновая свадьба）；
17. 50年：金婚（Золотая свадьба）；
18. 60年：钻石婚（Бриллиантовая свадьба）；
19. 65年：铁婚（Железная свадьба）；
20. 67年半：石婚（Каменная свадьба）；
21. 70年：福婚；
22. 75年：王冠婚（Коронная свадьба）；
23. 100年：红婚（Красная свадьба）。

## 參見

  - [重諧花燭](../Page/重諧花燭.md "wikilink")

[Category:婚姻](../Category/婚姻.md "wikilink")
[Category:周年紀念](../Category/周年紀念.md "wikilink")