**结晶水**是以中性水分子形式参加到[晶体结构中去的一定量的水](../Page/晶体结构.md "wikilink")；在[晶格中占有一定的位置](../Page/晶格.md "wikilink")，水分子数量与[矿物的其他成分之间常呈简单比例](../Page/矿物.md "wikilink")。

不同的含水化合物有特定的脱水温度，绝大部分伴有显著的吸热效应；[土壤中土粒所含的结晶水](../Page/土壤.md "wikilink")，不能直接参加土壤中进行的物理作用，也不能被植物直接吸收。

[晶体从](../Page/晶体.md "wikilink")[溶液当中](../Page/溶液.md "wikilink")[结晶时所析出时结合一定的数目的](../Page/结晶.md "wikilink")[水分子](../Page/水.md "wikilink")，它原本是和其他[化合物以分子的型態接和在一起的水](../Page/化合物.md "wikilink")，因此不具有水的特性，這類的晶體有[水合氧化鐵](../Page/水合氧化鐵.md "wikilink")、[氯化亞鈷](../Page/氯化亞鈷.md "wikilink")、[硫酸銅等](../Page/硫酸銅.md "wikilink")。

## 結晶水列表

<table>
<thead>
<tr class="header">
<th><p>化學式</p></th>
<th><p>配合物</p></th>
<th><p>結晶水數目</p></th>
<th><p>特點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/氯化釩.md" title="wikilink">VCl<sub>3</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[VCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]<sup>+</sup></p></td>
<td><p>二</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化溴.md" title="wikilink">VBr<sub>3</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[VBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]<sup>+</sup></p></td>
<td><p>二</p></td>
<td><p>溴化物和氯化物通常類似</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化碘.md" title="wikilink">VI<sub>3</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>[V(H<sub>2</sub>O)<sub>6</sub>]<sup>3+</sup></p></td>
<td><p>沒有</p></td>
<td><p>iodide competes<br />
poorly with water</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化鉻.md" title="wikilink">CrCl<sub>3</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[CrCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]<sup>+</sup></p></td>
<td><p>二</p></td>
<td><p>dark green isomer</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化鉻.md" title="wikilink">CrCl<sub>3</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>[CrCl(H<sub>2</sub>O)<sub>5</sub>]<sup>2+</sup></p></td>
<td><p>一</p></td>
<td><p>blue-green isomer</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化鉻.md" title="wikilink">CrCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>trans-[CrCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>molecular</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化鉻.md" title="wikilink">CrCl<sub>3</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>[Cr(H<sub>2</sub>O)<sub>6</sub>]<sup>3+</sup></p></td>
<td><p>沒有</p></td>
<td><p>violet isomer</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化錳.md" title="wikilink">MnCl<sub>2</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[MnCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>二</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化錳.md" title="wikilink">MnCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>cis-[MnCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>note cis<br />
molecular</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溴化錳.md" title="wikilink">MnBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>cis-[MnBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>note cis<br />
molecular</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化錳.md" title="wikilink">MnCl<sub>2</sub>(H<sub>2</sub>O)<sub>2</sub></a></p></td>
<td><p>trans-[MnCl<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>polymeric with<br />
bridging chloride</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溴化錳.md" title="wikilink">MnBr<sub>2</sub>(H<sub>2</sub>O)<sub>2</sub></a></p></td>
<td><p>trans-[MnBr<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>polymeric with<br />
bridging bromide</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化鐵.md" title="wikilink">FeCl<sub>2</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[FeCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>二</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化鐵.md" title="wikilink">FeCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>trans-[FeCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>molecular</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/溴化鐵.md" title="wikilink">FeBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>trans-[FeBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>molecular</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化鐵.md" title="wikilink">FeCl<sub>2</sub>(H<sub>2</sub>O)<sub>2</sub></a></p></td>
<td><p>trans-[FeCl<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>polymeric with<br />
bridging chloride</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化鈷.md" title="wikilink">CoCl<sub>2</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[CoCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>二</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溴化鈷.md" title="wikilink">CoBr<sub>2</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[CoBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>二</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/溴化鈷.md" title="wikilink">CoBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>trans-[CoBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>molecular</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化鈷.md" title="wikilink">CoCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>cis-[CoCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>note: cis<br />
molecular</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化鈷.md" title="wikilink">CoCl<sub>2</sub>(H<sub>2</sub>O)<sub>2</sub></a></p></td>
<td><p>trans-[CoCl<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>polymeric with<br />
bridging chloride</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化鈷.md" title="wikilink">CoBr<sub>2</sub>(H<sub>2</sub>O)<sub>2</sub></a></p></td>
<td><p>trans-[CoBr<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>polymeric with<br />
bridging bromide</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化鎳.md" title="wikilink">NiCl<sub>2</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[NiCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>二</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化鎳.md" title="wikilink">NiCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>cis-[NiCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>note: cis<br />
molecular</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/溴化鎳.md" title="wikilink">NiBr<sub>2</sub>(H<sub>2</sub>O)<sub>6</sub></a></p></td>
<td><p>trans-[NiBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>]</p></td>
<td><p>二</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化鎳.md" title="wikilink">NiCl<sub>2</sub>(H<sub>2</sub>O)<sub>2</sub></a></p></td>
<td><p>trans-[NiCl<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>]</p></td>
<td><p>沒有</p></td>
<td><p>polymeric with<br />
bridging chloride</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/氯化銅.md" title="wikilink">CuCl<sub>2</sub>(H<sub>2</sub>O)<sub>2</sub></a></p></td>
<td><p>[CuCl<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>]<sub>2</sub></p></td>
<td><p>沒有</p></td>
<td><p>tetragonally distorted<br />
two long Cu-Cl distances</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/氯化銅.md" title="wikilink">CuBr<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub></a></p></td>
<td><p>[CuBr<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>]<sub>n</sub></p></td>
<td><p>二</p></td>
<td><p>tetragonally distorted<br />
two long Cu-Br distances</p></td>
</tr>
</tbody>
</table>

## 参见

  - [水合物](../Page/水合物.md "wikilink")

[category:水](../Page/category:水.md "wikilink")

[Category:固体化学](../Category/固体化学.md "wikilink")