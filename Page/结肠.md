[Gray1223.png](https://zh.wikipedia.org/wiki/File:Gray1223.png "fig:Gray1223.png")、[肝臟](../Page/肝臟.md "wikilink")（紅）、[胃和](../Page/胃.md "wikilink")[大腸](../Page/大腸.md "wikilink")（藍）\]\]

**結腸**，中國古稱**回腸**\[1\]，是大多數[脊椎動物](../Page/脊椎動物.md "wikilink")[消化系統的最後一部分](../Page/消化系統.md "wikilink")，在將固體廢物排出體外前吸收[水和](../Page/水.md "wikilink")[鹽](../Page/鹽.md "wikilink")。結腸中未吸收的廢物也在[微生物](../Page/微生物.md "wikilink")（主要是[細菌](../Page/細菌.md "wikilink")）的幫助下[發酵](../Page/發酵.md "wikilink")。在食品和營養物質的吸收方面，結腸不像[小腸](../Page/小腸.md "wikilink")，較不重要，然而，結腸吸收水分，[鉀和一些脂溶性](../Page/鉀.md "wikilink")[維生素](../Page/維生素.md "wikilink")\[2\]。在[哺乳動物中](../Page/哺乳動物.md "wikilink")，結腸包括四個部分：[升結腸](../Page/升結腸.md "wikilink")，[橫結腸](../Page/橫結腸.md "wikilink")，[降結腸和](../Page/降結腸.md "wikilink")[乙狀結腸](../Page/乙狀結腸.md "wikilink")（“近端結腸”通常是指升結腸和橫結腸）。結腸是[大腸中從](../Page/大腸.md "wikilink")[盲腸到](../Page/盲腸.md "wikilink")[直腸的一段](../Page/直腸.md "wikilink")\[3\]。

## 解剖學

結腸位於腹腔或是其後的後腹膜。結腸在這些部份的位置是固定的。結腸外部有三條名為大腸條帶（taenia
coli）的[平滑肌分別附在升結腸](../Page/平滑肌.md "wikilink")、橫結腸、降結腸與乙狀結腸上。由於這三條平滑肌比結腸短，使結腸外型局部膨大和環形的縮窄交互間隔。這三條不隨意肌收縮使大腸緩緩蠕動。

### 血液和淋巴循環

結腸的[動脈](../Page/動脈.md "wikilink")[血液供應來自腸系膜上動脈](../Page/血液.md "wikilink")（Superior
mesenteric artery）和腸系膜下動脈（Inferior mesenteric
artery）的分支機構。這兩個系統之間通過“邊緣動脈”流動，它的整個長度的結腸平行。從歷史上看，它已被認為是Riolan弧，或蜿蜒的腸系膜動脈（Moskowitz發現），是一個可變的血管，連接腸系膜上動脈與腸系膜下動脈。然而，最近的研究與改進成像技術質疑血管是否實際存在，與一些專家呼籲從未來的醫學文獻取消這兩個名稱。

[靜脈回流與腸動脈供血相似](../Page/靜脈.md "wikilink")，腸系膜下靜脈流入脾靜脈，腸系膜上靜脈加入脾靜脈形成[肝門靜脈](../Page/肝門靜脈.md "wikilink")，然後進入[肝臟](../Page/肝臟.md "wikilink")。

整個結腸和直腸近端三分之二的[淋巴液流至腹主動脈旁](../Page/淋巴液.md "wikilink")[淋巴結](../Page/淋巴結.md "wikilink")，然後排入乳糜池
（cisterna
chyli）。餘下的直腸和[肛門的淋巴液可以循同樣的路線](../Page/肛門.md "wikilink")，或者流入內部髂或體表腹股溝淋巴結。

### 結構分部

[Dickdarm-Schema.svg](https://zh.wikipedia.org/wiki/File:Dickdarm-Schema.svg "fig:Dickdarm-Schema.svg")

#### 升結腸

[盲腸向上的延續](../Page/盲腸.md "wikilink")，移行於橫結腸，在[腹腔的右側](../Page/腹腔.md "wikilink")，[人類的升結腸自人體的右下腹往斜後方上升](../Page/人類.md "wikilink")，直到[肝臟下緣](../Page/肝臟.md "wikilink")，形成直角朝左側水平橫結腸前進，全長約25厘米\[4\]
。

#### 橫結腸

前接升結腸，後續降結腸。[人類橫結腸前接升結腸成直角朝左側水平前進](../Page/人類.md "wikilink")，此轉彎區稱為肝彎(Hepatic
Flexure)，往左方橫走，橫過左上腹，在[脾的下方直角轉灣往下](../Page/脾.md "wikilink")，此直角轉灣區稱為脾彎(Splenic
Flexure)。橫結腸前方由大網膜掛在[胃下](../Page/胃.md "wikilink")，後方由橫結腸系膜連於腹後壁。

#### 降結腸

上接橫結腸，下續乙狀結腸。人類降結腸自左上腹往下，在將靠近[骨盆時轉向內側](../Page/骨盆.md "wikilink")。降結腸的功能是儲存食物廢棄部分排入[直腸](../Page/直腸.md "wikilink")。

#### 乙狀結腸

人類乙狀結腸上承接自降結腸，在[骨盆腔內](../Page/骨盆腔.md "wikilink")，往下走朝左彎，為最彎曲的一段結腸。乙狀結腸壁[肌肉發達](../Page/肌肉.md "wikilink")，收縮增加結腸內的壓力，推動[糞便移動到](../Page/糞便.md "wikilink")[直腸](../Page/直腸.md "wikilink")。

#### 冗餘結腸

有少數人結腸特別長，多繞一圈。這種情況，被稱為冗餘結腸，通常健康上沒有直接的影響，但少數​​發生腸扭轉導致阻塞，必須立即就醫\[5\]。這些人做[大腸鏡時會遇到困難](../Page/大腸鏡.md "wikilink")，在某些情況下不可能做大腸鏡。特殊的專門大腸鏡有助於克服這個問題\[6\]。

### 水份吸收

結腸水份吸收需要克服水份的[擴散作用](../Page/擴散作用.md "wikilink")。長期的滲透壓差（Standing
gradient
osmosis）是一個描述腸內水份的重吸收要克服腸內水份的[擴散作用的術語](../Page/擴散作用.md "wikilink")。這[高滲液體的滲透壓驅動水通過](../Page/高张.md "wikilink")[緊密連接和鄰近的細胞](../Page/緊密連接.md "wikilink")[渗透到外側間隙](../Page/渗透.md "wikilink")，然後依次穿過基底膜和進入毛細血管。

## 功能

在不同的生物體之間的結腸的功能有差異。結腸主要是負責儲存廢物，回收水，保持水分平衡，吸收一些[維生素](../Page/維生素.md "wikilink")，如[维生素K](../Page/维生素K.md "wikilink")，並提供輔助菌群發酵的位置。

食糜達到結腸的時候，大部分的營養物質和90％的水已經被人體​​吸收。在這時，剩下的是一些[電解質如](../Page/電解質.md "wikilink")[鈉](../Page/鈉.md "wikilink")，[鎂](../Page/鎂.md "wikilink")，[氯以及攝入食物中不能消化的部分](../Page/氯.md "wikilink")（例如，攝入的[直鏈澱粉的很大一部分](../Page/直鏈澱粉.md "wikilink")，迄今尚未消化的[蛋白質](../Page/蛋白質.md "wikilink")，以及主要是可溶性或不溶性的[碳水化合物的](../Page/碳水化合物.md "wikilink")[膳食纖維](../Page/膳食纖維.md "wikilink")）。由於通過大腸的的肌肉移動食糜，剩餘的水大部分被吸收，而食糜混有粘液和細菌（稱為腸道菌群），成為[糞便](../Page/糞便.md "wikilink")。當糞便進入升結腸時尚算是[液體](../Page/液體.md "wikilink")。結腸肌肉將含水量高的糞便向前移動，並慢慢地吸收所有多餘的水份。當糞便進入降結腸時已成為半固態\[7\]。其中[細菌分解食物纖維為自己的養料](../Page/細菌.md "wikilink")，產生[醋酸](../Page/醋酸.md "wikilink")，[丙酸和](../Page/丙酸.md "wikilink")[丁酸等副產品](../Page/丁酸.md "wikilink")，這又是滋養結腸內壁細胞的養份。[蛋白質無法在此消化](../Page/蛋白質.md "wikilink")。對於[人類來說](../Page/人類.md "wikilink")，也許百分之10未消化的碳水化合物也因此成為可用的養分。這是一種[共生關係的一個例子](../Page/共生.md "wikilink")，並每天提供身體約100[卡路里](../Page/卡路里.md "wikilink")。大腸不產生[消化酶](../Page/消化酶.md "wikilink")
-
化學性消化是食糜到達大腸前在[小腸完成](../Page/小腸.md "wikilink")。結腸的[pH值在](../Page/pH值.md "wikilink")5.5和7之間（微酸性至中性）。其他動物，包括[猿和其他](../Page/猿.md "wikilink")[靈長類動物有比例較大的結腸](../Page/靈長類.md "wikilink")，使牠們能從植物材料得到更多的養分，所以牠們飲食中植物材料比例能夠比人類高\[8\]。

## 結腸疾病

  - [腸道血管發育不良](../Page/腸道血管發育不良.md "wikilink")
  - [闌尾炎](../Page/闌尾炎.md "wikilink")
  - 慢性功能性腹痛
  - 結腸炎
  - [大腸癌](../Page/大腸癌.md "wikilink")
  - [便秘](../Page/便秘.md "wikilink")
  - [克隆氏症](../Page/克隆氏症.md "wikilink")
  - [腹瀉](../Page/腹瀉.md "wikilink")
  - 憩室
  - 先天性巨結腸症（aganglionosis）
  - 腸梗阻
  - [腸套疊](../Page/腸套疊.md "wikilink")
  - 腸易激綜合症
  - [息肉](../Page/息肉.md "wikilink")（另見大腸息肉）
  - [偽膜性結腸炎](../Page/偽膜性結腸炎.md "wikilink")
  - 潰瘍性結腸炎
  - 中毒性巨結腸

## 外部链接

  - [格雷解剖学Online](https://web.archive.org/web/20070402113234/http://education.yahoo.com/reference/gray/subjects/subject)

[he:המעי הגס\#הכרכשת](../Page/he:המעי_הגס#הכרכשת.md "wikilink")
[hu:Vastagbél](../Page/hu:Vastagbél.md "wikilink")
[is:Ristill](../Page/is:Ristill.md "wikilink")
[ku:Zeblot](../Page/ku:Zeblot.md "wikilink")
[nn:Tjukktarmen\#Colon](../Page/nn:Tjukktarmen#Colon.md "wikilink")
[ta:பெருங்குடல்](../Page/ta:பெருங்குடல்.md "wikilink")
[th:ลำไส้ใหญ่](../Page/th:ลำไส้ใหญ่.md "wikilink")

[Category:解剖学](../Category/解剖学.md "wikilink")
[Category:腸臟](../Category/腸臟.md "wikilink")
[Category:腹部](../Category/腹部.md "wikilink")

1.  《[靈樞](../Page/靈樞.md "wikilink")．腸胃》：「**回腸**當臍，在環回周葉積當下，回運環反十六曲。大四寸，徑一寸寸之少半，長二丈一尺。[廣腸傳脊](../Page/結腸.md "wikilink")，以受回腸，左環葉脊，上下辟，大八寸，徑二寸寸之大半，長二尺八寸。」
2.  [Colon Function And Health
    Information](http://www.colonfunction.com/) Retrieved on 2010-01-21
3.  <http://definr.com/large+intestine>
4.  <http://wiki.answers.com/Q/What_are_the_lengths_of_the_different_parts_of_the_colon>
5.
6.   *Note:Single use PDF copy provided free by [Blackwell
    Publishing](../Page/Blackwell_Publishing.md "wikilink") for purposes
    of Wikipedia content enrichment.*
7.  [The Function Of The
    Colon](http://ccnt.hsc.usc.edu/colorectal/functioncolon.aspx)
    Retrieved on 2010-01-21
8.  [Function Of The Large
    Intestine](http://jamesallred.com/function-of-the-colon)  Retrieved
    on 2010-01-21