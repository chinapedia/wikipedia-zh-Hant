**绥阳县**在[中华人民共和国](../Page/中华人民共和国.md "wikilink")[贵州省北部](../Page/贵州省.md "wikilink")、[芙蓉江上游](../Page/芙蓉江.md "wikilink")，是[遵义市下属的一个](../Page/遵义市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。

## 历史

隋朝时，置绥阳县，后废。[明朝政府在](../Page/明朝政府.md "wikilink")[播州之役获胜](../Page/播州之役.md "wikilink")，消灭[播州杨氏土司政权后](../Page/播州杨氏.md "wikilink")，于[万历二十九年](../Page/万历.md "wikilink")（1601年）四月设置[遵义军民府](../Page/遵义军民府.md "wikilink")。同时以旧绥阳县地，置绥阳县。时绥阳县属遵义府下的[真安州](../Page/真安州.md "wikilink")，亦属[四川省](../Page/四川承宣布政使司.md "wikilink")\[1\]。

## 行政区划

下辖13个[镇](../Page/镇.md "wikilink")、2个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 县情简介

縣境內有[寬闊水國家級自然保護區](../Page/貴州寬闊水國家級自然保護區.md "wikilink")。本地[特产有](../Page/特产.md "wikilink")[朝天椒和空心银丝面](../Page/朝天椒.md "wikilink")。绥阳县有[绥遵高速公路与遵义市区连接](../Page/绥遵高速公路.md "wikilink")。

[Zunyi-suiyang-pano-2009.2.24.jpg](https://zh.wikipedia.org/wiki/File:Zunyi-suiyang-pano-2009.2.24.jpg "fig:Zunyi-suiyang-pano-2009.2.24.jpg")
[Guizhou_Suiyang_Pano_from_Yankong.jpg](https://zh.wikipedia.org/wiki/File:Guizhou_Suiyang_Pano_from_Yankong.jpg "fig:Guizhou_Suiyang_Pano_from_Yankong.jpg")

## 注释

## 外部链接

  - [绥阳县政府网站](http://www.suiyang.gov.cn/)

[绥阳县](../Category/绥阳县.md "wikilink")
[县](../Category/遵义区县市.md "wikilink")
[遵义](../Category/贵州县份.md "wikilink")

1.  《[明史](../Page/明史.md "wikilink")·卷四十三·志第十九》◎地理四○四川、江西
    四川......遵义军民府，元播州宣慰司......万历二十九年四月改置遵义军民府。领州一，县四。遵义......真安州，元珍州思宁长官司。明玉珍改真州。洪武十七年置真州长官司。万历二十九年四月改置。南有芙蓉江，自乌江分流，东北入於黔江。又有三江，东南流合於虎溪，亦注於黔江。西南距府二百里。领县二：绥阳，府东北。万历二十九年四月以旧绥阳县地置。东有水德江，亦曰涪江，亦曰小乌江，流入彭水县界。仁怀......