**维也纳**（[英语](../Page/英语.md "wikilink")：****）是[美国](../Page/美国.md "wikilink")[西弗吉尼亚州](../Page/西弗吉尼亚州.md "wikilink")[伍德县](../Page/伍德县_\(西弗吉尼亚州\).md "wikilink")[俄亥俄河畔的一座城市](../Page/俄亥俄河.md "wikilink")，属于[帕克斯堡的一个郊区](../Page/帕克斯堡_\(西弗吉尼亚州\).md "wikilink")，2000年人口10,861，是帕克斯堡-[玛丽埃塔](../Page/玛丽埃塔_\(俄亥俄州\).md "wikilink")-维也纳[都会区的第三大城市](../Page/帕克斯堡-玛丽埃塔-维也纳都会区.md "wikilink")。该城在1935年设市，根据[弗吉尼亚州的](../Page/弗吉尼亚州.md "wikilink")[同名城市命名](../Page/维也纳_\(弗吉尼亚州\).md "wikilink")。

## 外部链接

[Category:西弗吉尼亚州城市](../Category/西弗吉尼亚州城市.md "wikilink") [Category:伍德县
(西弗吉尼亚州)](../Category/伍德县_\(西弗吉尼亚州\).md "wikilink")
[Category:帕克斯堡-玛丽埃塔-维也纳都会区](../Category/帕克斯堡-玛丽埃塔-维也纳都会区.md "wikilink")