**维伦多尔夫的维纳斯（Venus of
Willendorf）**，一座11.1厘米（4又3/8英寸）高的女性小[雕塑](../Page/雕塑.md "wikilink")，1908年出土于[考古学家](../Page/考古学家.md "wikilink")在[奥地利的维伦多尔夫村](../Page/奥地利.md "wikilink")（Willendorf）附近一处[旧石器时代遗址的考古发掘中](../Page/旧石器时代.md "wikilink")。\[1\]雕像由一块产于外地的、带有[红赭色彩的](../Page/红赭色.md "wikilink")[鲕粒](../Page/鲕石.md "wikilink")[石灰石](../Page/石灰石.md "wikilink")[雕刻而成](../Page/雕刻.md "wikilink")，年代距今约2.2-2.4万年。现为[维也纳](../Page/维也纳.md "wikilink")[自然史博物馆的收藏品之一](../Page/自然史博物馆_\(维也纳\).md "wikilink")。

## 年代

1990年，借助[地层的重新分析修订](../Page/地层.md "wikilink")，一般估计该雕塑雕刻于西元前22,000至24,000年。\[2\]关于其起源、创作方法和文化含义人们几乎一无所知。

## 描述

[VenusWillendorf.jpg](https://zh.wikipedia.org/wiki/File:VenusWillendorf.jpg "fig:VenusWillendorf.jpg")

名字中的[维纳斯并非指其是一](../Page/维纳斯.md "wikilink")[现实主义雕塑](../Page/现实主义.md "wikilink")，而是对妇女形象的理想化描绘。雕像的[阴户](../Page/阴户.md "wikilink")、[乳房以及肿胀的下腹令人印象深刻](../Page/乳房.md "wikilink")，这些特征都与旺盛的[生育能力密切相关](../Page/生育能力.md "wikilink")。引起鲜明对比的是雕像短小的四肢，和手臂折于乳房前，雕像的脸部无法看见，她的头部为编制物，许多眼睛或是某种头饰所环绕。

雕像的昵称，拿这个肥硕身材的小雕像与经典的「维纳斯」雕像进行了对比，这导致了现代一些分析家的反对。克里斯托弗·维特科姆（Christopher
Witcombe）曾指出，「使用讽刺性的身份将这座小雕像鉴定为‘维纳斯’至少满足了某些对远古，对妇女和对某些趣味的假定感觉」（，[链接](http://witcombe.sbc.edu/willendorf/)）。同时，另一位当时的专家勉强将她鉴定为一位在[老欧洲的旧石器时代](../Page/老欧洲文化.md "wikilink")[地球之母女神](../Page/地球之母.md "wikilink")。一些人认为，她的肥硕身材是代表了其在猎人社会中较高的身份地位，另外她明显强盛的生育能力可能也是安全和成功的标志。

雕像的腿部并未设计为能让雕像站立的形态，因此有人推测雕像是用于手持而非简单放置观赏之用。以致一些考古学家认为她并非是[生殖女神而仅仅是一个带来好运的符咒](../Page/生殖女神.md "wikilink")。更有人认为其可能是设计作用于插入阴道，以乞求带来旺盛的[生殖力](../Page/生殖力.md "wikilink")。

## 其他

在此雕塑发现及命名後，又发现了多个类似或其他形式的艺术品。它们全部都被认为与相关。

## 參考文獻

## 外部链接

  - [Christopher L. C. E. Witcombe,
    “史前妇女：维伦多夫的维纳斯”](http://witcombe.sbc.edu/willendorf/)

[Category:奧地利雕塑作品](../Category/奧地利雕塑作品.md "wikilink")
[Category:石雕像](../Category/石雕像.md "wikilink")
[Category:欧洲史前艺术](../Category/欧洲史前艺术.md "wikilink")
[Category:欧洲旧石器时代](../Category/欧洲旧石器时代.md "wikilink")

1.
2.  [Venus of
    Willendorf](http://witcombe.sbc.edu/willendorf/willendorfdiscovery.html)
    Christopher L. C. E. Witcombe, 2003.