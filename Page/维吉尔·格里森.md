**维吉尔·伊万·“加斯”·格里森**（，），前[美國空軍](../Page/美國空軍.md "wikilink")[中校及](../Page/中校.md "wikilink")[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")，执行过[水星-红石4号](../Page/水星-红石4号.md "wikilink")、[双子星座3号以及](../Page/双子星座3号.md "wikilink")[阿波罗1号任务](../Page/阿波罗1号.md "wikilink")。1967年1月27日，格里森与[爱德华·怀特和](../Page/爱德华·怀特.md "wikilink")[罗杰·查菲在](../Page/罗杰·查菲.md "wikilink")[阿波罗1号的一次例行测试中因舱内大火牺牲](../Page/阿波罗1号.md "wikilink")。

## 外部链接

  - [美国国家航空航天局关于格里森的介绍](http://www.jsc.nasa.gov/Bios/htmlbios/grissom-vi.html)

[Category:美國韓戰軍事人物](../Category/美國韓戰軍事人物.md "wikilink")
[Category:双子星座计划](../Category/双子星座计划.md "wikilink")
[Category:阿波罗计划](../Category/阿波罗计划.md "wikilink")
[Category:美国试飞员](../Category/美国试飞员.md "wikilink")
[Category:國會太空榮譽勳章獲得者](../Category/國會太空榮譽勳章獲得者.md "wikilink")
[Category:美国殉职者](../Category/美国殉职者.md "wikilink")
[Category:美国空军中校](../Category/美国空军中校.md "wikilink")
[Category:普渡大學校友](../Category/普渡大學校友.md "wikilink")
[Category:印第安納州人](../Category/印第安納州人.md "wikilink")
[Category:水星计划7人](../Category/水星计划7人.md "wikilink")
[Category:阿波羅1號](../Category/阿波羅1號.md "wikilink")