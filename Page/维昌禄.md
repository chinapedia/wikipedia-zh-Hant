<table>
<tbody>
<tr class="odd">
<td><p>{{Infobox Person</p></td>
<td><p>name = 维昌禄<br />
<br />
</p></td>
<td><p>image = <a href="https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_137-032800,_Bischof_Georg_Weig.jpg" title="fig:Bundesarchiv_Bild_137-032800,_Bischof_Georg_Weig.jpg">Bundesarchiv_Bild_137-032800,_Bischof_Georg_Weig.jpg</a></p></td>
<td><p>caption =</p></td>
<td><p>birth_date = 1883年12月14日</p></td>
<td><p>birth_place = <a href="../Page/贝拉茨豪森.md" title="wikilink">贝拉茨豪森</a></p></td>
<td><p>death_date = 1941年10月3日</p></td>
<td><p>death_place = <a href="../Page/青岛.md" title="wikilink">青岛</a></p></td>
<td><p>occupation = 罗马天主教主教</p></td>
<td><p>spouse = 无</p></td>
<td><p>parents = }}</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

**维昌禄**（拉丁语：**Episcopus Georgius Weig, S.V.D.**，德语：**Bischof Georg Weig,
S.V.D.**，），[罗马天主教](../Page/罗马天主教.md "wikilink")[主教](../Page/主教.md "wikilink")，[圣言会会士](../Page/圣言会.md "wikilink")。

1883年12月14日，维昌禄出生于[德国](../Page/德国.md "wikilink")[巴伐利亚](../Page/巴伐利亚王国.md "wikilink")[雷根斯堡附近的小城](../Page/雷根斯堡.md "wikilink")[贝拉茨豪森](../Page/贝拉茨豪森.md "wikilink")。他先在[西里西亚读文学](../Page/西里西亚.md "wikilink")，后去莫德林读神学，1907年2月10日（23岁）在莫德林晋升[神甫](../Page/神甫.md "wikilink")。1908年9月15日（24岁），维昌禄来华传教，在兖州修院教书，1913年任兖州小修院院长，1915年任兖州大修院院长。

1925年，罗马教廷从兖州代牧区划出胶县、高密、即墨、诸城、日照、临沂、郯城、费县、蒙阴、沂水、莒县11个县成立[青岛监牧区](../Page/天主教青岛教区.md "wikilink")，3月18日（41岁）任命维昌禄为首任监牧主教。1928年6月14日升格为青岛代牧区主教，同年9月23日在兖州由罗马教廷驻华公使主教[刚恒毅祝圣](../Page/刚恒毅.md "wikilink")。

维昌禄主教在任期间，修建了宏伟的[罗曼复兴式](../Page/罗曼复兴式建筑.md "wikilink")[圣弥额尔主教座堂](../Page/圣弥爱尔大教堂.md "wikilink")，1932年动工，1934年10月落成。

1941年10月3日，维昌禄在青岛逝世，年57岁，葬于主教座堂西侧的墓穴内。

## 相关条目

  - [圣弥额尔主教座堂](../Page/圣弥爱尔大教堂.md "wikilink")
  - [天主教青岛教区](../Page/天主教青岛教区.md "wikilink")
  - [安治泰](../Page/安治泰.md "wikilink")
  - [韩宁镐](../Page/韩宁镐.md "wikilink")
  - [白明德](../Page/白明德.md "wikilink")

[Category:来华圣言会传教士](../Category/来华圣言会传教士.md "wikilink")
[Category:德国天主教传教士](../Category/德国天主教传教士.md "wikilink")