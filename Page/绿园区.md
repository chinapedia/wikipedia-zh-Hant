**园区**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[吉林省](../Page/吉林省.md "wikilink")[长春市下辖的](../Page/长春市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，位于长春市西部。

## 行政区划

下辖7个街道、1个镇、2个乡，26个行政村、205个居民委员会：

  - 街道办事处：[春城街道](../Page/春城街道.md "wikilink")、[普阳街道](../Page/普阳街道.md "wikilink")、[正阳街道](../Page/正阳街道.md "wikilink")、[东风街道](../Page/东风街道.md "wikilink")、[锦程街道](../Page/锦程街道.md "wikilink")、[铁西街道](../Page/铁西街道.md "wikilink")、[青年路街道](../Page/青年路街道.md "wikilink")。
  - 镇：[合心镇](../Page/合心镇.md "wikilink")、[西新镇](../Page/西新镇.md "wikilink")、[城西镇](../Page/城西镇.md "wikilink")。

## 知名人物

  - [王洪文](../Page/王洪文.md "wikilink")，中共领导人

## 参考文献

[绿园区](../Category/绿园区.md "wikilink")
[区](../Category/长春区县市.md "wikilink")
[长春](../Category/吉林市辖区.md "wikilink")