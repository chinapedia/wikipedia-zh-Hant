**绿林起义**指中國古代[新莽末的](../Page/新朝.md "wikilink")[农民起义](../Page/农民起义.md "wikilink")。[天凤四年](../Page/天凤.md "wikilink")（公元17年）新市（今[湖北省](../Page/湖北省.md "wikilink")[京山县](../Page/京山县.md "wikilink")）人[王匡](../Page/王匡_\(更始\).md "wikilink")、[王凤组织了以](../Page/王凤_\(更始\).md "wikilink")[绿林山为根据地的](../Page/绿林山.md "wikilink")[武装起义](../Page/武装起义.md "wikilink")，史称“[绿林军](../Page/绿林军.md "wikilink")”。[地皇三年](../Page/地皇_\(王莽\).md "wikilink")（22年）二王分兵，遂又称被为[下江兵和](../Page/下江兵.md "wikilink")[新市兵](../Page/新市兵.md "wikilink")。次年，义军攻占[昆阳](../Page/昆阳.md "wikilink")（今[河南](../Page/河南省.md "wikilink")[叶县](../Page/叶县.md "wikilink")）、定陵（今河南[舞阳](../Page/舞阳.md "wikilink")）、郾县（今河南[郾城](../Page/郾城.md "wikilink")）等地。[王莽乃派](../Page/王莽.md "wikilink")[王寻](../Page/王寻.md "wikilink")、[王邑率军镇压](../Page/王邑.md "wikilink")，但在[昆阳之战遭到惨败](../Page/昆阳之战.md "wikilink")。义军乘胜攻[洛阳](../Page/洛阳.md "wikilink")、[长安](../Page/长安.md "wikilink")。长安被迅速攻占后，王莽被杀。后绿林军首领[刘玄称帝](../Page/汉更始帝.md "wikilink")。公元25年[赤眉军立](../Page/赤眉军.md "wikilink")[刘盆子为帝](../Page/刘盆子.md "wikilink")，随后击败了绿林军。

## 参见

  - [绿林军](../Page/绿林军.md "wikilink")
  - [赤眉军](../Page/赤眉军.md "wikilink")
  - [赤眉起义](../Page/赤眉起义.md "wikilink")

## 参考资料

[Category:新朝民变](../Category/新朝民变.md "wikilink")
[Category:荆门历史](../Category/荆门历史.md "wikilink")
[Category:京山市](../Category/京山市.md "wikilink")