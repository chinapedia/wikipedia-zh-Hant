**绿翅金刚鹦鹉**([学名](../Page/学名.md "wikilink")：**Ara
chloroptera**英文名：**red-and-green macaw**)
该种分布在中美洲部分地区，以及南美北部的[哥伦比亚](../Page/哥伦比亚.md "wikilink")、[委内瑞拉](../Page/委内瑞拉.md "wikilink")、[巴西北部](../Page/巴西.md "wikilink")、[玻利维亚和](../Page/玻利维亚.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")。在外形上绿翅与[绯红金刚鹦鹉很接近](../Page/绯红金刚鹦鹉.md "wikilink")。最大的区别在于绯红身上的颜色排布为红黄蓝，该种为红绿蓝。因此也有将该种称为红绿金刚鹦鹉的习惯。

## 习性及保育

成年身长九十厘米左右。[寿命约五十年](../Page/寿命.md "wikilink")。它的叫声极大，但是并不十分刺耳。
繁殖期从4月开始，在[繁殖期间具有攻击性](../Page/繁殖期.md "wikilink")，每窝产2-3颗[蛋](../Page/蛋.md "wikilink")，但并不是全部[受精](../Page/受精.md "wikilink")。26日后孵化，至3个月后[羽化完全](../Page/羽化.md "wikilink")。

## 參考文獻

[Category:鹦鹉科](../Category/鹦鹉科.md "wikilink")
[Category:能言鳥類](../Category/能言鳥類.md "wikilink")