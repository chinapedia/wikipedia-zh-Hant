**绿萝**（[学名](../Page/学名.md "wikilink")：），又名**黄金葛**、**[万年青](../Page/万年青_\(消歧义\).md "wikilink")**，属[天南星科](../Page/天南星科.md "wikilink")[麒麟叶属](../Page/麒麟叶属.md "wikilink")，原产于[法属玻里尼西亚的](../Page/法属玻里尼西亚.md "wikilink")[莫雷阿岛](../Page/莫雷阿岛.md "wikilink")，并随着人类引进而在[东南亚](../Page/东南亚.md "wikilink")、[南亚](../Page/南亚.md "wikilink")、[华南](../Page/华南.md "wikilink")、[所罗门群岛](../Page/所罗门群岛.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[中美洲](../Page/中美洲.md "wikilink")[西印度群岛等地的热带雨林中归化](../Page/西印度群岛.md "wikilink")。为[多年生](../Page/多年生植物.md "wikilink")[木质](../Page/木质.md "wikilink")[藤本植物](../Page/藤本植物.md "wikilink")。喜荫蔽及潮湿的环境，现在在家庭中常作为[观叶植物栽培](../Page/观叶植物.md "wikilink")。由于其是攀缘植物的特点，适宜[吊盆栽植或植於](../Page/吊盆.md "wikilink")[图腾柱上](../Page/图腾柱.md "wikilink")。

## 形态

绿萝属高大藤本，靠茎部攀墙，节间具纵向槽状；较多分枝，枝悬垂。幼枝呈鞭状，细长，粗3-4毫米，节间长15-20厘米；叶柄长8-10厘米，两侧鞘状部分达顶部；鞘状部分呈皮革质感，下部每侧宽近1厘米，向上渐狭；下部叶片大，长5-10厘米，上部的长6-8厘米，纸质感，宽卵形，短渐尖，叶基部呈心形，宽6.5厘米。成熟枝上叶柄粗壮，长30-40厘米，枝基部稍大，上部关节长2.5一3厘米、稍肥厚，腹面具宽槽，叶鞘长，叶片薄皮革质感，翠绿色，通常特别是叶面有多数不规则的纯黄色斑块，不等边的卵形或卵状长圆形，前端短渐尖，叶基部深心形，长32-45厘米，宽24-36厘米，1级侧叶脉8-9对，稍粗，两面略隆起，与中肋成70°-80°{{\#tag:ref|\[<http://www.cvh.org.cn/lsid/index.php?lsid=urn:lsid:cvh.org.cn:names:cnpc_4989&vtype=img,spm,ref,link>,
绿萝\] 中国数字植物标本馆|name=|group=}}。

本种开花极为罕见，1962年首次记录到本种开花，依据该年的记录，其[肉穗花序的形态与](../Page/肉穗花序.md "wikilink")[麒麟叶极度相似](../Page/麒麟叶.md "wikilink")，甚至因此一度被植物学家归类为麒麟叶的栽培型，但其全株成长形态与麒麟叶仍有显著差异，才将其保留为独立物种。但自此以后，无论野外或栽培环境中一直未再发现其开花。直到2016年在《[自然](../Page/自然_\(期刊\).md "wikilink")》期刊发表的一份研究发现其罕开花的原因：本种由于基因缺损，不能产生足够的[植物激素](../Page/植物激素.md "wikilink")——[赤霉素来激活其开花程序](../Page/赤霉素.md "wikilink")。研究者通过外施赤霉素，终于成功诱导绿萝开花。

## 毒性

[Raphides_from_variegated_ivy.jpg](https://zh.wikipedia.org/wiki/File:Raphides_from_variegated_ivy.jpg "fig:Raphides_from_variegated_ivy.jpg")
绿萝的毒性来自于其汁液中的，这也是许多[天南星科植物的特征](../Page/天南星科.md "wikilink")。这些不易溶解于水的[草酸钙针晶体会对进食者的口腔](../Page/草酸钙.md "wikilink")、食道等软组织细胞带来物理性损伤，因此人或动物误食绿萝的枝叶，可以引起口腔刺痛、呕吐、吞咽困难等等。皮肤接触其汁液也会产生刺痛感等反应。\[1\]\[2\]\[3\]\[4\]

## 入侵物种

[Epipremnum_aureum_in_Udawattakele.jpg](https://zh.wikipedia.org/wiki/File:Epipremnum_aureum_in_Udawattakele.jpg "fig:Epipremnum_aureum_in_Udawattakele.jpg")
绿萝作为观赏植物被人类带到世界各地，但在一些非原生地的热带雨林，因缺乏天敌，绿萝可以变成[入侵物种](../Page/入侵物种.md "wikilink")。例如，在[斯里兰卡](../Page/斯里兰卡.md "wikilink")，绿萝在当地雨林中过度生长，进而干扰当地生态平衡\[5\]。

[美国农业部于](../Page/美国农业部.md "wikilink")1999年将其列入“入侵物种”名录之中\[6\]。

## 參考文獻

## 外部链接

  -
[aureum](../Category/麒麟叶属.md "wikilink")
[Category:观叶植物](../Category/观叶植物.md "wikilink")
[Category:爬藤植物](../Category/爬藤植物.md "wikilink")
[Category:法属波利尼西亚植物](../Category/法属波利尼西亚植物.md "wikilink")

1.
2.  [Outbreak of Food-borne Illness Associated with Plant Material
    Containing
    Raphides](http://informahealthcare.com/doi/abs/10.1081/CLT-44721).
    Informa Healthcare.
3.  ASPCA (2009). Taro. ASPCA official website: Animal Poison Control
    Center. Retrieved on 7 December 2009 from:
    <http://www.aspca.org/pet-care/poison-control/plants/taro-1.html>
4.  Wrongdiagnosis.com (2009). Plant poisoning – Calcium oxalate
    crystals. Wrongdiagnosis.com retrieved on 7 December 2009 from:
    <http://www.wrongdiagnosis.com/p/plant_poisoning_calcium_oxalate_crystals/intro.htm>
5.
6.  USDA Natural Resources Conservation Service). [Plants
    Profile](http://plants.usda.gov/java/profile?symbol=EPPI)