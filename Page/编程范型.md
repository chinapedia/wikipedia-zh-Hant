**编-{}-程范型**、**编-{}-程范式**或**程式-{}-設計法**（），（**範**即模範、典範之意，範式即模式、方法），是一类典型的编程风格，是指从事[软件工程的一类典型的风格](../Page/软件工程.md "wikilink")（可以对照[方法学](../Page/方法学.md "wikilink")）。如：[函數式編程](../Page/函數式編程.md "wikilink")、[程序編程](../Page/程序編程.md "wikilink")、[面向对象编程](../Page/面向对象编程.md "wikilink")、[指令式编程等等為不同的编程范型](../Page/指令式编程.md "wikilink")。

编程范型提供了（同时决定了）[程序员对](../Page/程序员.md "wikilink")[程序执行的看法](../Page/程序.md "wikilink")。例如，在[面向对象编程中](../Page/面向对象编程.md "wikilink")，程序员认为程序是一系列相互作用的对象，而在[函数式编程中一个程序会被看作是一个无状态的函数计算的序列](../Page/函数式编程.md "wikilink")。

正如[软件工程中不同的群体会提倡不同的](../Page/软件工程.md "wikilink")「方法学」一样，不同的[编程语言也会提倡不同的](../Page/编程语言.md "wikilink")「编程范型」。一些语言是专门为某个特定的范型设计的（如[Smalltalk和](../Page/Smalltalk.md "wikilink")[Java支持面向对象编程](../Page/Java.md "wikilink")，而[Haskell和](../Page/Haskell.md "wikilink")[Scheme则支持函数式编程](../Page/Scheme.md "wikilink")），同时还有另一些语言支持多种范型（如[Ruby](../Page/Ruby.md "wikilink")、[Common
Lisp](../Page/Common_Lisp.md "wikilink")、[Python和](../Page/Python.md "wikilink")[Oz](../Page/Oz.md "wikilink")）。

很多编程范型已经被熟知他们禁止使用哪些技术，同时允许使用哪些。例如，纯粹的[函数式编程不允许有副作用](../Page/函数式编程.md "wikilink")\[1\]；[结构化编程不允许使用](../Page/结构化编程.md "wikilink")[goto](../Page/goto.md "wikilink")。可能是因为这个原因，新的范型常常被那些习惯于较早的风格的人认为是教条主义或过分严格。然而，这样避免某些技术反而更加证明了关于程序正确性——或仅仅是理解它的行为——的法则，而不用限制程序语言的一般性。

编程范型和编程语言之间的关系可能十分复杂，由于一个编程语言可以支持[多种范型](../Page/多范型.md "wikilink")。例如，[C++设计时](../Page/C++.md "wikilink")，支持[过程化编程](../Page/过程化编程.md "wikilink")、[面向对象编程以及](../Page/面向对象编程.md "wikilink")[泛型编程](../Page/泛型编程.md "wikilink")。然而，设计师和程序员们要考虑如何使用这些范型元素来构建一个程序。一个人可以用C++写出一个完全过程化的程序，另一个人也可以用C++写出一个纯粹的面向对象程序，甚至还有人可以写出杂揉了两种范型的程序。

## 例子

  - [结构化编程對比](../Page/结构化编程.md "wikilink")[非结构化编程](../Page/非结构化编程.md "wikilink")
  - [命令式编程對比](../Page/命令式编程.md "wikilink")[宣告式编程](../Page/宣告式编程.md "wikilink")
  - [消息传递编程對比](../Page/消息传递编程.md "wikilink")[命令式编程](../Page/命令式编程.md "wikilink")
  - [程序編程對比](../Page/程序編程.md "wikilink")[函数式编程](../Page/函数式编程.md "wikilink")
  - [Value-level
    programming對比](../Page/Value-level_programming.md "wikilink")[Function-level
    programming](../Page/Function-level_programming.md "wikilink")
  - [流程驱动编程對比](../Page/流程驱动编程.md "wikilink")[事件驱动编程](../Page/事件驱动编程.md "wikilink")
  - [纯量编程对比](../Page/纯量编程.md "wikilink")[阵列编程](../Page/阵列编程.md "wikilink")
  - [基于类编程对比](../Page/基于类编程.md "wikilink")[基于原型编程](../Page/基于原型编程.md "wikilink")（在[面向对象编程的上下文中](../Page/面向对象编程.md "wikilink")）
  - [Rule-based
    programming對比](../Page/Rule-based_programming.md "wikilink")[Constraint
    programming](../Page/Constraint_programming.md "wikilink")（在[逻辑编程的上下文中](../Page/逻辑编程.md "wikilink")）
  - [基于组件编程](../Page/Software_componentry.md "wikilink")（如[OLE](../Page/Object_linking_and_embedding.md "wikilink")）
  - [面向方面编程](../Page/面向方面编程.md "wikilink")（如[AspectJ](../Page/AspectJ.md "wikilink")）
  - [符号式编程](../Page/符号式编程.md "wikilink")（如[Mathematica](../Page/Mathematica.md "wikilink")）
  - [面向表格编程](../Page/面向表格编程.md "wikilink")（如[Microsoft
    FoxPro](../Page/Microsoft_FoxPro.md "wikilink")）
  - [管道编程](../Page/管道编程.md "wikilink")（如Unix命令中的[管道_(Unix)](../Page/管道_\(Unix\).md "wikilink")）
  - [Post-object
    programming](../Page/Post-object_programming.md "wikilink")
  - [面向主题编程](../Page/面向主题编程.md "wikilink")
  - [自省编程或称反射编程](../Page/自省编程.md "wikilink")

## 參考文献

## 参见

  - [Ars based programming](../Page/Ars_based_programming.md "wikilink")
  - [Memetics](../Page/Memetics.md "wikilink")

{{-}}

[編程典範](../Category/編程典範.md "wikilink")

1.  [F\# 程式設計入門
    (1)](http://msdn.microsoft.com/zh-tw/library/dd252673.aspx)：在表示式（expression）內不可以造成值的改變。