**缨子木科**也叫[燧体木科](../Page/燧体木科.md "wikilink")、[穗籽木科](../Page/穗籽木科.md "wikilink")、[流苏亮籽科或](../Page/流苏亮籽科.md "wikilink")[流苏子科](../Page/流苏子科.md "wikilink")，只有1[属](../Page/属.md "wikilink")2[种](../Page/种.md "wikilink")，生长在[北美洲](../Page/北美洲.md "wikilink")，都是[灌木](../Page/灌木.md "wikilink")，其[种子的假种皮碎裂后呈流苏状](../Page/种子.md "wikilink")。

*Crossosoma californicum* [Nutt.](../Page/Thomas_Nuttall.md "wikilink")
生长在[美国](../Page/美国.md "wikilink")[加利福尼亚州的帕洛斯](../Page/加利福尼亚州.md "wikilink")-弗德斯半岛、圣卡塔利纳岛、圣克莱门特岛和[墨西哥的瓜达卢佩岛](../Page/墨西哥.md "wikilink")。

*Crossosoma bigelovii* [S.Wats.](../Page/Sereno_Watson.md "wikilink")
生长在[美国](../Page/美国.md "wikilink")[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")、[内华达州](../Page/内华达州.md "wikilink")、[亚利桑那州和](../Page/亚利桑那州.md "wikilink")[墨西哥的加利福尼亚半岛的](../Page/墨西哥.md "wikilink")[沙漠地带](../Page/沙漠.md "wikilink")。

1981年的[克朗奎斯特分类法将本](../Page/克朗奎斯特分类法.md "wikilink")[科列入](../Page/科.md "wikilink")[蔷薇目](../Page/蔷薇目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其直接列在](../Page/APG_分类法.md "wikilink")[蔷薇分支之下](../Page/蔷薇分支.md "wikilink")，没有放入任何一[目](../Page/目.md "wikilink")，2003年经过修订的[APG
II
分类法新设立了一个](../Page/APG_II_分类法.md "wikilink")[缨子木目](../Page/缨子木目.md "wikilink")，包括本科、[旌节花科和](../Page/旌节花科.md "wikilink")[省沽油科以及一些独立的](../Page/省沽油科.md "wikilink")[属](../Page/属.md "wikilink")。

## 外部链接

  - [*C. bigelovii*
    介绍](http://ucjeps.berkeley.edu/cgi-bin/get_JM_treatment.pl?Crossosoma+bigelovii)
  - [*C. californica*
    介绍](http://ucjeps.berkeley.edu/cgi-bin/get_JM_treatment.pl?Crossosoma+californica)

[\*](../Category/缨子木科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")