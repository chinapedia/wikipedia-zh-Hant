**网页代理服务器**（[英文](../Page/英文.md "wikilink")：****），又称为**在-{}-线代理**或**線-{}-上代理**。网页代理是一种在[网页上运行的](../Page/网页.md "wikilink")[代理服务器程序](../Page/代理服务器.md "wikilink")，其不用任何设置，输入[网址选择好代理服务器便可以访问的优点已经成为时下最流行的代理访问方式](../Page/网址.md "wikilink")。网页代理是常见的一种代理程序。网页代理给客户端提供远程网站上的网页和文件的[高速缓存](../Page/高速缓存.md "wikilink")，使客户端可以更加快速安全的浏览远程网站。一些网页代理网站保持每天更新以保证速度。

## 運作原理

**网页代理**原理是接收到[浏览器对远程网站的浏览需求时候](../Page/浏览器.md "wikilink")（浏览器客户端提交网址的时候），代理程序开始在当前服务器寻找远程网站的[缓存网页和网站](../Page/缓存网页.md "wikilink")，找到目标网站后，代理程序马上将网站数据返回到用户的浏览器客户端。如果当前服务器没有该远程服务器的缓存，代理程序则会自动读取远程网站，将远程网站的资料提交给客户端，同时将资料缓存以提供给下一次的浏览需求。代理程序会根据缓存的时间、大小和提取记录自动删除缓存。删除的方法有两种，一种是删除保存最久的资料，一种是删除最少提取的缓存。这两种方法也可以结合使用。

**网页代理**使用不同的复杂的脚本，以便绕过[过滤器和](../Page/过滤器.md "wikilink")[防火墙来访问被屏蔽或封锁的网站](../Page/防火墙.md "wikilink")，用户使用在线代理服务非常简单，不需要设置浏览器，也不需要安装额外的软件，只要访问在线代理网站，然后输入要访问网站的网址，然后就可以享受免费的代理服务。在[中国大陆](../Page/中国大陆.md "wikilink")，有部分网站被屏蔽掉，网页代理访问可以实现浏览。

## 网页代理的优点

网页代理是一种具有匿名代理服务器作用的网页代理，在服务器运行在线代理程序,可以以此服务器作为跳板来上网浏览，可以隐藏自己的真实[IP和信息](../Page/IP地址.md "wikilink")，不但上网更安全，更引起多重帳戶使用者的青睞。

网页代理也可以过滤远程网站的内容。一些审查软件也跟网页代理差不多，将一些网站内容过滤掉。出于某些目的或保护，有些网页代理程序也会将网站的内容重新编排。比如，Skweezer可以将网页重新编排，以提供给手机和[PDA浏览](../Page/PDA.md "wikilink")。网络管理员也经常使用代理程序来预防病毒和反动网站内容。

这些程序通常是使用[PHP或者](../Page/PHP.md "wikilink")[CGI编程语言编写的](../Page/CGI.md "wikilink")。[CGI编写的代理程序通常被用来突破公司或学校的网络封锁](../Page/CGI.md "wikilink")。通过[CGI编写的代理程序可以隐藏客户端的IP](../Page/CGI.md "wikilink")，因此用户可以实现匿名访问远程网站。[PHP编写的代理程序采用](../Page/PHP.md "wikilink")[64位加密技术](../Page/Base64.md "wikilink")，通过HTTP访问类使用fsockopen函数访问目标[URL](../Page/URL.md "wikilink")，然后处理一下得到的[HTML代码](../Page/HTML.md "wikilink")，将其中的各个链接加上前缀，以便让浏览器仍然通过代理程序访问各个图片和[CSS](../Page/CSS.md "wikilink")、[Javascript文件](../Page/Javascript.md "wikilink")。

通过一些网页代理下载软件也可以过滤掉一些含有病毒和木马的危险文件，这可以使浏览体验更安全。

## 网页代理的缺点

由于网络原因，网页代理很多时候都无法使用。

  - 在中国，即使使用网络代理也会过滤关键字，成为审查对象，同样也无法访问许多网站。
  - 网页代理虽然可以访问大部分网站，但是并不能保证所有的网站都是可以访问的。\[1\]
  - 有一些网页代理也支持安全连接（[HTTPS](../Page/HTTPS.md "wikilink")），但是最好不要用网页代理访问涉及个人隐私的网站。
  - 部分**網頁代理**會禁止[中國](../Page/中國.md "wikilink")[IP訪問](../Page/IP.md "wikilink")，出現「Are
    you from China?」等訊息。
  - 使用**網頁代理**的速度會比正常的上網慢，即使代理網站的效率很高。
  - **網頁代理**會消耗很大的流量，所以提供**網頁代理**的網站很容易出現流量用完或不穩定的狀況。
  - 由於架設**網頁代理**最主要的目的是賺錢、支付[網頁寄存費用](../Page/網頁寄存.md "wikilink")，**網頁代理**網站會刊登[廣告](../Page/廣告.md "wikilink")。
  - 使用**網頁代理**時，較進階的如[AJAX將不會正常](../Page/AJAX.md "wikilink")，另外[Yahoo\!知識+的申請加入知識團](../Page/Yahoo!知識+.md "wikilink")、補充內容、發表意見、交付投票等項目，**網頁代理**也辦不到。\[2\]
  - 使用**網頁代理**時會有樣式無法顯示或顯示異常等狀況\[3\]。

## 使用方法

[使用網頁代理瀏覽網頁代理這個條目的畫面.jpg](https://zh.wikipedia.org/wiki/File:使用網頁代理瀏覽網頁代理這個條目的畫面.jpg "fig:使用網頁代理瀏覽網頁代理這個條目的畫面.jpg")瀏覽本頁面的畫面。\]\]

1.  代理网站的输入框中输入目标网址。
2.  選擇是否去除[JavaScript](../Page/JavaScript.md "wikilink")、[Cookies](../Page/Cookie.md "wikilink")、標題、[Meta資訊等](../Page/Meta.md "wikilink")，以及是否用[Base64或](../Page/Base64.md "wikilink")[ROT13加密網頁](../Page/ROT13.md "wikilink")。
3.  提交表單。

## 使用技巧

  - 先在本机设置代理IP，再使用网页代理会比较快，而且IP无法被侦测。

## 相关破网软件(工具)

主要的破网软件有[世界通](../Page/世界通.md "wikilink")、[火凤凰](../Page/火凤凰_\(软件\).md "wikilink")、[动态网](../Page/动态网.md "wikilink")([自由门](../Page/自由门.md "wikilink")
[逍遥游](../Page/逍遥游.md "wikilink")
[动网通](../Page/动网通.md "wikilink"))、[无界网](../Page/无界网络.md "wikilink")([无界浏览](../Page/无界浏览.md "wikilink"))、[花园网](../Page/花园网.md "wikilink")([GTunnel](../Page/GTunnel.md "wikilink"))与[Tor系列](../Page/Tor.md "wikilink")、[赛风](../Page/赛风.md "wikilink")、[JAP](../Page/JAP.md "wikilink")、[Freedur等](../Page/Freedur.md "wikilink")。

主要的VPN代理服务器有[proXPN](../Page/proXPN.md "wikilink")、[IPjetable](../Page/IPjetable.md "wikilink")、[VPNPOP](../Page/VPNPOP.md "wikilink")、[Tenacy
VPN](../Page/Tenacy_VPN.md "wikilink")、[MacroVPN](../Page/MacroVPN.md "wikilink")、[VPN98](../Page/VPN98.md "wikilink")、[Loki
Network Project](../Page/Loki_Network_Project.md "wikilink")
、[FreeVPN等](../Page/FreeVPN.md "wikilink")。

主要的SSH代理服务器有
[sshs](../Page/sshs.md "wikilink")、[aoyoussh](../Page/aoyoussh.md "wikilink")
其他的例如[Flexamail](../Page/Flexamail.md "wikilink")(电子邮件代理)

## 网页代理的種類

  - [PHProxy](../Page/PHProxy.md "wikilink")
  - [Glype](../Page/Glype.md "wikilink")
  - [ASProxy](../Page/ASProxy.md "wikilink")

## 註解

## 外部链接

  - \[//tools.ietf.org/html/draft-luotonen-web-proxy-tunneling-01
    Tunneling TCP based protocols through Web proxy servers\]
  - [Apollo Web
    Proxy](https://web.archive.org/web/20090420012217/http://quick-proxy.appspot.com/)

[Category:互联网](../Category/互联网.md "wikilink")

1.  部分代理會出現**網址中包含mail，故無法瀏覽**的錯誤訊息(缺圖)。
2.  申請加入知識團、補充內容、發表意見完全失敗，交付投票則是確認送出後出現**參數錯誤**的訊息。若發現有**網頁代理**已經可以辦到，請擴充本條目！
3.  如圖片[:<File:使用網頁代理瀏覽網頁代理這個條目的畫面.jpg>](../Page/:File:使用網頁代理瀏覽網頁代理這個條目的畫面.jpg.md "wikilink")，「條目」、「討論」等以及左邊維基百科的圖片皆發生走位。