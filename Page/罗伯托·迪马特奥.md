**罗伯托·迪马特奥**（，），乃一名在[瑞士出生的](../Page/瑞士.md "wikilink")[義大利](../Page/義大利.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，曾代表過[意大利國家隊出戰國際賽](../Page/意大利國家足球隊.md "wikilink")。退役後擔任[領隊](../Page/教練.md "wikilink")，曾執教[米尔顿凯恩斯](../Page/米尔顿凯恩斯足球俱乐部.md "wikilink")、[西布朗和](../Page/西布罗姆维奇足球俱乐部.md "wikilink")[車路士](../Page/切尔西足球俱乐部.md "wikilink")、他是車路士歷史上唯一奪得[欧洲冠军联赛冠军的領隊](../Page/2012年歐洲聯賽冠軍盃決賽.md "wikilink")。

2014年10月7日，迪馬堤奧獲委任為德甲[史浩克04的新教練](../Page/史浩克04.md "wikilink")。

## 生平

### 球員時期

迪馬堤奧出生於[瑞士的一個](../Page/瑞士.md "wikilink")[意大利人家庭](../Page/意大利.md "wikilink")，因而順理成章在瑞士展開其足球員生涯。司職[中場中路的他](../Page/中場.md "wikilink")，早年效力過多家瑞士足球會，贏得過1993年瑞士聯賽冠軍，並獲得瑞士聯賽最佳球員獎。

1993年他南下意大利投身[意甲聯賽](../Page/意甲.md "wikilink")，目的地正是首都球會[拉素](../Page/拉素體育會.md "wikilink")。由於其意大利人背景，他得以於1994年代表[意大利國家隊出賽](../Page/意大利國家足球隊.md "wikilink")。他效力四年間球會成績平平，1996年更被賣往[英超聯賽球會](../Page/英超.md "wikilink")[車路士](../Page/切尔西足球俱乐部.md "wikilink")，轉會費為490萬[英鎊](../Page/英鎊.md "wikilink")。於是拉素便成為這名意大利人平生唯一效力的意大利球會。

迪馬堤奧在車路士效力期間，為球會贏得過數項盃賽冠軍。1997年，他協助車路士贏得[英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")，而他在當屆決賽甫開賽43秒便攻入一球，是當時足總杯史上最快的入球。1998年車路士憑著足總盃冠軍身份闖入[歐洲超级盃决赛](../Page/歐洲超级盃.md "wikilink")，最後亦奪冠而回。[1998年世界杯迪馬堤奧得以隨意大利國家隊出賽](../Page/1998年世界盃足球賽.md "wikilink")，惟自此再沒有入選。

2000年車路士再度贏得足總盃冠軍，當屆迪馬堤奧曾一度受傷患影響，但仍能及時復出，並在決賽取得入球取勝。但翌季他卻再受傷患困擾，一度養傷達年半，結果他只能選擇於2002年2月退役，時年32歲。

### 領隊時期

2008年7月2日在40多份申請中脫穎而出，獲聘為[英格蘭足球甲級聯賽球隊](../Page/英格蘭足球甲級聯賽.md "wikilink")[米尔顿凯恩斯的新領隊](../Page/米尔顿凯恩斯足球俱乐部.md "wikilink")\[1\]。由於球隊表現出色，在新年前高據第二位，迪馬堤奧獲延長合約到2011年\[2\]。

2009年6月30日轉投剛降級[英冠的](../Page/英冠.md "wikilink")[西布朗](../Page/西布罗姆维奇足球俱乐部.md "wikilink")，代替離隊返回[蘇格蘭的](../Page/蘇格蘭.md "wikilink")[托尼·莫布雷](../Page/托尼·莫布雷.md "wikilink")（Tony
Mowbray）之職，簽訂一年滾動合約\[3\]，帶領球隊首季即能取得聯賽亞軍及直接升班重返[英超](../Page/英超.md "wikilink")。

2010年8月14日[英超揭幕戰迪馬堤奧帶領西布朗重返](../Page/英超.md "wikilink")[史丹福橋球場挑戰衛冕冠軍](../Page/史丹福橋球場.md "wikilink")[車路士](../Page/切尔西足球俱乐部.md "wikilink")，卻以0-6敗陣\[4\]。但球隊隨即反彈，更於9月份取得3勝1和不敗佳績，高據聯賽榜第6位，獲選「英超9月最佳領隊」\[5\]。其後西布朗成績急速下滑，2011年2月6日於球隊最近18戰13負後，迪馬堤奧被解除領隊職務\[6\]。

2011年6月29日迪馬堤奧重返[車路士成為新任領隊](../Page/切尔西足球俱乐部.md "wikilink")[安德烈·維拉斯-波亞斯的教練團成員之一](../Page/安德烈·維拉斯-波亞斯.md "wikilink")，出任助理領隊一職\[7\]。

2012年3月4日車路士透過官網宣佈[安德烈·維拉斯-波亞斯被解僱](../Page/安德烈·維拉斯-波亞斯.md "wikilink")，原助理教練迪馬堤奧擔任臨時[車路士主帥直至季尾](../Page/切尔西足球俱乐部.md "wikilink")\[8\]。

2012年4月18日，在[欧冠四強首回合主場以](../Page/欧冠.md "wikilink")1比0擊敗[巴塞罗那](../Page/巴塞隆拿足球會.md "wikilink")。\[9\]

2012年4月25日，在[欧洲冠军联赛四強次回合客場以](../Page/欧洲冠军联赛.md "wikilink")2比2战平[巴塞罗那](../Page/巴塞罗那足球俱乐部.md "wikilink")，以总比分3比2晋级决赛。

2012年5月5日，帶領車路士以2比1擊敗利物浦，奪得2012年[英格蘭足總盃冠軍](../Page/英格蘭足總盃.md "wikilink")。

2012年5月19日，帶領車路士遠征慕尼黑安聯球場，挑戰東道主[拜仁](../Page/拜仁慕尼黑.md "wikilink")，出戰[2012年歐洲聯賽冠軍盃決賽](../Page/2012年歐洲聯賽冠軍盃決賽.md "wikilink")。結果雙方戰罷百二分鐘，仍未分勝負，終於要以互射[十二碼定勝](../Page/十二碼.md "wikilink")。車路士在首先射失的劣勢下，卻奇蹟地在最後兩輪逆轉，終以
**4:3** （總比數
5:4）獲勝，奪下球隊首個歐洲冠軍，迪馬堤奧旋即成為球隊英雄，亦成為首位車路士領隊奪得[欧冠](../Page/2011/12賽季歐洲聯賽冠軍盃.md "wikilink")。

2012年6月13日，正式成为[切尔西一线队主教练](../Page/切尔西.md "wikilink")。在同年11月21日，陷入小低潮的切尔西聯賽四輪不勝及歐冠近乎出局的情況下被閃電革職，成為該球季首位被革職的英超教練。

## 榮譽

### 球員

  - [瑞士聯賽冠軍](../Page/瑞士足球超级联赛.md "wikilink")：1992/93年；
  - 瑞士聯賽最佳球員：1993年；
  - [英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")：1997年、2000年；
  - [英格蘭聯賽盃](../Page/英格蘭聯賽盃.md "wikilink")：1998年；
  - [英格蘭社區盾](../Page/英格蘭社區盾.md "wikilink")：2000年；
  - [歐洲盃賽冠軍盃](../Page/歐洲盃賽冠軍盃.md "wikilink")：1998年；
  - [歐洲超級盃](../Page/歐洲超級盃.md "wikilink")：1998年；

### 領隊

西布罗姆维奇

  - [英格兰足球甲级联赛亚军](../Page/英格兰足球甲级联赛.md "wikilink"): 2009/2010年
  - 英超每月最佳領隊：2010年9月；

切尔西

  - [英格兰足总杯冠军](../Page/英格兰足总杯.md "wikilink"): 2011/2012年
  - [欧洲冠军联赛冠军](../Page/欧洲冠军联赛.md "wikilink"): 2011/2012年

## 參考資料

## 外部連結

  - [Di Matteo at
    Chelsea](https://web.archive.org/web/20100818034810/http://www.chelseafc.com/page/Legends_Details/0,,10268~1961478,00.html)
  - [MK Dons linked with Di
    Matteo](http://www.skysports.com/story/0,19528,11095_3759521,00.html)
  - [MK Dons
    profile](https://web.archive.org/web/20090805041451/http://www.mkdons.com/page/FirstTeamStaff/0,,10420,00.html)

[Category:義大利裔瑞士人](../Category/義大利裔瑞士人.md "wikilink")
[Category:意大利足球運動員](../Category/意大利足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:蘇黎世球員](../Category/蘇黎世球員.md "wikilink")
[Category:阿勞球員](../Category/阿勞球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:瑞士超聯球員](../Category/瑞士超聯球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:意大利足球主教練](../Category/意大利足球主教練.md "wikilink")
[Category:米爾頓凱恩斯領隊](../Category/米爾頓凱恩斯領隊.md "wikilink")
[Category:西布朗領隊](../Category/西布朗領隊.md "wikilink")
[Category:車路士領隊](../Category/車路士領隊.md "wikilink")
[Category:英格蘭足球聯賽領隊](../Category/英格蘭足球聯賽領隊.md "wikilink")
[Category:英超領隊](../Category/英超領隊.md "wikilink")
[Category:欧冠冠军主教练](../Category/欧冠冠军主教练.md "wikilink")

1.   [Di Matteo appointed MK Dons
    boss](http://news.bbc.co.uk/sport2/hi/football/teams/m/milton_keynes_dons/7484484.stm)
2.   [Di Matteo agrees Dons
    extension](http://news.bbc.co.uk/sport2/hi/football/teams/m/milton_keynes_dons/7804639.stm)，2008年12月30日，[BBC](../Page/BBC.md "wikilink")
    Sport，於2008年12月31日查閱
3.
4.
5.
6.
7.
8.
9.