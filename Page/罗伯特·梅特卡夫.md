**罗伯特·梅蘭克頓·梅特卡夫**（，
），暱稱**鮑伯·梅特卡夫**（）出生于[紐約](../Page/紐約.md "wikilink")[布魯克林](../Page/布魯克林.md "wikilink")。美國科技先驅，發明了[乙太網路](../Page/乙太網路.md "wikilink")，成立[3Com且制定了](../Page/3Com.md "wikilink")[梅特卡夫定律](../Page/梅特卡夫定律.md "wikilink")。

## 生平

在1973年，梅特卡夫在[Xerox
PARC工作時](../Page/Xerox_PARC.md "wikilink")，他發明了乙太網路，一個可以在短矩離使得電腦可以互相連通的標準。在1979年，梅特卡夫離開PARC，成立3Com，一家電腦網路設備的製造商。在1980年，以發展區域網路，特別是乙太網，的貢獻，他得到由[計算機協會所頒發的](../Page/計算機協會.md "wikilink")[葛麗絲·霍普獎](../Page/葛麗絲·霍普.md "wikilink")。在1990年，梅特卡夫從3Com退休並且開始一段為期10年的的發行者和網路權威者的生涯，為InfoWorld撰寫網路專欄。他於2001年成為資本投資家，現在是Polaris
Venture Partners的一員。 He is a director of PopTech, an executive
technology conference he cofounded in 1997.為什麼突然變成英文? 你搞得我好亂啊～～

他以兩個學士學位畢業於[麻省理工學院](../Page/麻省理工學院.md "wikilink")，一個是[電機工程學位](../Page/電機工程學.md "wikilink")，另一個麻省理工學院[史隆商學院的工業管理學位](../Page/史隆商學院.md "wikilink")。他在[哈佛大學以一篇有關於](../Page/哈佛大學.md "wikilink")[封包交換的論文取得博士學位](../Page/封包交換.md "wikilink")。那篇論文實際上是他在MIT為一個名為MAC的計劃工作時寫好的。

在2005年3月14日，美國布希總統在白宮慶典時頒給他全國科技獎章，以表揚他創新乙太網路及將其標準化及商業通用化上的領導地位。根據IDC，在2004年全世界上有2億個新的乙太網路的埠被製造出貨。


[Category:美国计算机科学家](../Category/美国计算机科学家.md "wikilink")
[Category:美國電機工程師](../Category/美國電機工程師.md "wikilink")
[Category:美国国家工程院院士](../Category/美国国家工程院院士.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")
[Category:乙太網路](../Category/乙太網路.md "wikilink")
[Category:布魯克林區出身人物](../Category/布魯克林區出身人物.md "wikilink")
[Category:IEEE亚历山大·格雷厄姆·贝尔奖章获得者](../Category/IEEE亚历山大·格雷厄姆·贝尔奖章获得者.md "wikilink")