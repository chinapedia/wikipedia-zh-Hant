**羅保銘**，[天津人](../Page/天津.md "wikilink")。男，汉族。1969年5月参加工作，1971年8月加入[中國共產黨](../Page/中國共產黨.md "wikilink")。[天津师范专科学校中文系中文专业毕业](../Page/天津师范专科学校.md "wikilink")，[南開大學明清史專業畢業](../Page/南開大學.md "wikilink")，[在職研究生學歷](../Page/在職研究生學歷.md "wikilink")，碩士學位。[第十五](../Page/中国共产党第十五届中央委员会候补委员列表.md "wikilink")、[十六届中共中央候补委员](../Page/中国共产党第十六届中央委员会候补委员列表.md "wikilink")，[第十七](../Page/中国共产党第十七届中央委员会委员列表.md "wikilink")、[十八届中央委员](../Page/中国共产党第十八届中央委员会委员列表.md "wikilink")。曾任[海南省省長](../Page/海南省.md "wikilink")、[中共海南省委书记](../Page/中共海南省委.md "wikilink")。

## 履历

1969年5月，任[內蒙古生產建設兵團班長](../Page/內蒙古.md "wikilink")、排長、副指導員。1973年12月，任天津市無線電元件三廠工人、車間黨支部副書記。1978年11月，在[天津師範專科學校中文系中文專業學習](../Page/天津師範專科學校.md "wikilink")。1981年9月，任[共青團天津市委青工部幹部](../Page/共青團.md "wikilink")。1984年3月，任共青團天津市委研究室主任。1984年11月，任共青團天津市委副書記。1985年11月，任共青團天津市委書記(其間：1987年9月至1988年1月，在[中共中央黨校學習](../Page/中共中央黨校.md "wikilink"))。

1992年4月，任中共天津市[大港區委副書記](../Page/大港區.md "wikilink")、代區長（1993年3月当选區長）。1994年10月，任中共天津市大港區委書記、區長(1991年9月至1995年1月，在南開大學明清史專業读在職研究生，獲碩士學位)。1995年7月，任天津市商業委員會主任、中共天津市委商工委副書記。1997年10月，任中共天津市委常委、天津市商業委員會主任。1997年12月，任中共天津市委常委、宣傳部部長。

2001年7月，任中共[海南省委副書記](../Page/海南省.md "wikilink")。2002年5月，任中共海南省委副書記、宣傳部部長。2002年9月，任中共海南省委副書記。2007年1月，任中共海南省委副書記，海南省人民政府副省長、代省長（2007年2月当选省長）。2011年8月，任中共海南省委书记，海南省人大常委会主任，海南省军区党委第一书记。2017年4月1日，卸任中共海南省委书记\[1\]。

2017年4月，任[全国人民代表大会华侨委员会副主任委员](../Page/全国人民代表大会华侨委员会.md "wikilink")\[2\]。

## 参考文献

## 参考资料

  - [罗保铭:守护好生态"金饭碗"](http://www.ce.cn/xwzx/gnsz/gdxw/201307/28/t20130728_24611104.shtml)
    中国经济网 2014-05-12

{{-}}

[B](../Category/羅姓.md "wikilink")
[Category:天津人](../Category/天津人.md "wikilink")
[Category:天津师范大学校友](../Category/天津师范大学校友.md "wikilink")
[Category:南开大学校友](../Category/南开大学校友.md "wikilink")
[Category:中共十六大代表](../Category/中共十六大代表.md "wikilink")
[Category:中共十七大代表](../Category/中共十七大代表.md "wikilink")
[Category:中共十八大代表](../Category/中共十八大代表.md "wikilink")
[Category:中国共产党第十五届中央委员会候补委员](../Category/中国共产党第十五届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十六届中央委员会候补委员](../Category/中国共产党第十六届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中国共产党第十八届中央委员会委员](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:第十届全国人大代表](../Category/第十届全国人大代表.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:中國共產主義青年團天津市委員會書記](../Category/中國共產主義青年團天津市委員會書記.md "wikilink")
[Category:大港區區長](../Category/大港區區長.md "wikilink")
[Category:中共大港區委書記](../Category/中共大港區委書記.md "wikilink")
[Category:天津市商业委员会主任](../Category/天津市商业委员会主任.md "wikilink")
[Category:中共天津市委常委](../Category/中共天津市委常委.md "wikilink")
[Category:中共天津市委宣傳部部長](../Category/中共天津市委宣傳部部長.md "wikilink")
[Category:中共海南省委副書記](../Category/中共海南省委副書記.md "wikilink")
[Category:中共海南省委宣傳部部長](../Category/中共海南省委宣傳部部長.md "wikilink")
[Category:中華人民共和國海南省省長](../Category/中華人民共和國海南省省長.md "wikilink")
[Category:中共海南省委书记](../Category/中共海南省委书记.md "wikilink")
[Category:海南省人大常委会主任](../Category/海南省人大常委会主任.md "wikilink")

1.
2.