**罗孝全**（，），[基督教](../Page/基督教.md "wikilink")[新教](../Page/新教.md "wikilink")[美南浸信会传教士](../Page/美南浸信会.md "wikilink")。

1802年，罗孝全出生于[美國](../Page/美國.md "wikilink")[田納西州](../Page/田納西州.md "wikilink")[索姆奈县](../Page/索姆奈县_\(田纳西州\).md "wikilink")，就读于[南卡罗来纳州Furman](../Page/南卡罗来纳州.md "wikilink")
Academy后[按立为](../Page/按立.md "wikilink")[牧师](../Page/牧师.md "wikilink")。1837年自费来中国，初在澳门向麻疯病患者传教。1841年加入[美国浸信会差会](../Page/美国浸信会.md "wikilink")，1842年，他成为第一位长期居留香港的新教传教士。当年6月，他为第一名中国信徒施浸。1844年到廣州，他是第一个搬到广州商馆区以外居住的新教传教士。1845年美国浸会分裂，他属于美南浸信会差会。

[太平天國的創立者](../Page/太平天國.md "wikilink")[洪秀全在](../Page/洪秀全.md "wikilink")1847年3-5月來到羅孝全在廣州的禮拜堂，在那裡学习《[圣经](../Page/圣经.md "wikilink")》，曾要求受洗，羅孝全不同意洪秀全對以前大病時所見「異象」的見解，拒絕為他施洗。洪秀全4個月後離開。

在洪秀全未建立太平天國前，羅孝全對此人頗有微言，但1852年當羅孝全知道當年的洪秀全已經成為太平天國領袖後，態度旋即改變，他們希望西方諸國能接受他，以便基督教能在太平天國境內傳播，他更形容洪秀全「比我想像還要帥，聲音極好聽」。

他多次想進入太平天國境內傳教，一直未能成事，直到1860年，太平軍攻佔[蘇州](../Page/蘇州.md "wikilink")，逼近上海，外人進入太平天國統治區較以前容易。羅孝全從南方經上海到蘇州，10月到天京，住在-{干}-王[洪仁玕的府第](../Page/洪仁玕.md "wikilink")。

洪秀全召見他，授予通事官領袖一職，封接天義。羅孝全亦一反當地清朝與西方已制定的禮儀，向洪秀全下跪叩拜，但他事後說自己只是聽見鑼鼓大作，一時不知如何反應，才跪了下來。初期兩人關係尚算良好，羅亦被授以外交權力，管理太平天國與洋人的事務。

不過，羅孝全的上海之行，最大目標是要糾正洪秀全對基督的概念，但洪秀全借基督教而自創拜上帝教與西方基督教本質上完全不同。羅孝全發現他一直推崇的太平天國，事實上只是洪秀全借上帝之名行神權獨裁統治，1862年1月忿而到停泊在長江邊的英國船尋求庇護，離開[天京](../Page/天京.md "wikilink")。

離開後，羅孝全第三次改變態度，隨即在《[北華捷報](../Page/北華捷報.md "wikilink")》撰文批評洪秀全是「狂人，全不適合統治，政府全無組織」，但當時洋人對羅孝全並不同情，其中《北華捷報》在報道他離開南京時，戲稱他是「浦東大主教閣下」、「冒牌的[狄奧根尼](../Page/狄奧根尼.md "wikilink")」，甚至笑他快要成為傳教士的[柏拉圖](../Page/柏拉圖.md "wikilink")。羅孝全本身人緣亦不好，其美國妻子與兩名孩子堅持留在美國，與他貌合神離；他的華籍助手一個死去，另一個卻受不住他而離開；而他亦試過因廣州的房子被毀，經年不斷打官司要求清政府賠錢，最終官府不勝其擾，決定賠償2500美元，成為一時趣聞。　

有说羅孝全后来1866年回到美國，也有说羅孝全最后一次离开金陵后就回广州继续传道行医\[1\]，1871年因早先在澳门感染的[麻瘋病并发症去世](../Page/麻瘋病.md "wikilink")。

## 著作

  - 《真理之教》1840年，澳门
  - 《问答俗话》1840年 澳门
  - 《救世主耶稣新遗诏书》1840 澳门
  - 《路加福音传注释》1860 广州
  - 《家用良药》 广州
  - 《耶稣圣经》

## 參考文献

<references />

  - 羅爾綱《太平天國史》卷四十二、七十七。

## 外部链接

  - [茅家琦：洪秀全和罗孝全](http://jds.cass.cn/Article/20070802160949.asp)

{{-}}

[L](../Category/在华基督教传教士.md "wikilink")
[L](../Category/清朝基督教新教人物.md "wikilink")
[L](../Category/田納西州人.md "wikilink")
[L](../Category/广州历史人物.md "wikilink")
[Category:太平天國人物](../Category/太平天國人物.md "wikilink")

1.  《历代寓穗名流》 李小松作 南粤出版社出版 ISBN 962.04.0434.3