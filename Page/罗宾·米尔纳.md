**亞瑟·約翰·羅賓·葛瑞爾·米爾納**（，），生于英国[普利茅斯](../Page/普利茅斯.md "wikilink")，[计算机科学家](../Page/计算机科学.md "wikilink")。1991年获得[图灵奖](../Page/图灵奖.md "wikilink")。他是英国皇家学会成员，ACM会士。

## 生平

出生于一个军人家庭，早年就读于[伊顿公学](../Page/伊顿公学.md "wikilink")。后加入英国皇家工程兵部队，获得[少尉军衔](../Page/少尉.md "wikilink")。1952年入读[剑桥大学国王学院](../Page/剑桥大学国王学院.md "wikilink")，1957年毕业，之后的第一个工作是教师，然后在Ferranti公司当程序员。此后他进入学术界，先后在[伦敦城市大学](../Page/伦敦城市大学.md "wikilink")，Swansea大学，斯坦福大学任职。1973年回到英国[爱丁堡大学](../Page/爱丁堡大学.md "wikilink")，在爱丁堡大学任职期间，他开发了函数式编程语言，[ML](../Page/ML.md "wikilink")，并和他的同事一起完成了LCF的开发。在离开爱丁堡前，罗宾·米尔纳向现在的爱丁堡大学信息学院（[:爱丁堡大学信息学院](../Page/:爱丁堡大学信息学院.md "wikilink"))提供了一笔捐款并成立了每年一次在爱丁堡大学信息学院举行的以他名字命名的罗宾·米尔纳演讲（[1](http://www.lfcs.inf.ed.ac.uk/events/milner-lecture/index.html))，被邀请的演讲者都是对理论计算机科学有重大贡献的学者。1995年，罗宾·米尔纳回到母校剑桥大学任教，并担任剑桥大学计算机实验室主任。

2010 年3月20日卒于英国剑桥。

## 學術貢獻

在計算機科學裡，米爾納主要有三大貢獻。他開發了其中一個最早的[自動定理證明工具](../Page/自動定理證明.md "wikilink")——[LCF](../Page/LCF.md "wikilink")。

他另一項主要工作是[并发理论](../Page/并发理论.md "wikilink")（concurrency
theory），他提出了许多被广泛研究的并发计算模型：[CCS](../Page/CCS.md "wikilink")、[pi演算](../Page/pi演算.md "wikilink")。

## 外部連結及參考

  - [A review of "Proof, Language, and
    Interaction"](https://web.archive.org/web/20050204172933/http://north.ecc.edu/alsani/ct01\(1-4\)/msg00041.html),
    a book on computer science dedicated to Milner and covering many
    areas of his work.
  - [Milner在劍橋的首頁](http://www.cl.cam.ac.uk/users/rm135/)
  - [訪問](https://web.archive.org/web/20050109091702/http://www.dcs.qmul.ac.uk/~martinb/interviews/milner/);
    Martin Berger; 2003年9月3日

[M](../Category/英国计算机科学家.md "wikilink")
[M](../Category/图灵奖获得者.md "wikilink")
[Category:程式語言設計者](../Category/程式語言設計者.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:劍橋大學國王學院校友](../Category/劍橋大學國王學院校友.md "wikilink")