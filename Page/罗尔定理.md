以法国数学家[米歇尔·罗尔命名的](../Page/米歇尔·罗尔.md "wikilink")**罗尔中值定理**（）是[微分学中一条重要的定理](../Page/微分学.md "wikilink")，是三大[微分中值定理之一](../Page/微分中值定理.md "wikilink")，叙述如下：如果[函数](../Page/函数.md "wikilink")\(f(x)\)满足

1.  在闭[区间](../Page/区间.md "wikilink")\([a,b]\)上[连续](../Page/连续.md "wikilink")；
2.  在开区间\((a,b)\)内可导；
3.  在区间端点处的函数值相等，即\(f(a)=f(b)\)，

那么在\((a,b)\)内至少有一点\(\xi (a<\xi<b)\)，使得\(f^\prime(\xi)=0\)\[1\]。

## 证明

[RTCalc.svg](https://zh.wikipedia.org/wiki/File:RTCalc.svg "fig:RTCalc.svg")
首先，因为\(f\)在闭区间\([a,b]\)上连续，根据[极值定理](../Page/极值定理.md "wikilink")，\(f\)在\([a,b]\)上有[最大值和](../Page/最大值.md "wikilink")[最小值](../Page/最小值.md "wikilink")。如果最大值和最小值都在端点\(a\)或\(b\)处取得，由于\(f(a)=f(b)\)，\(f\)显然是一个[常数函数](../Page/常数函数.md "wikilink")。那么对于任一点\(\xi \in (a,b)\)，我们都有\(f^\prime(\xi)=0\)。

现在假设\(f\)在\(\xi\in (a,b)\)处取得最大值。我们只需证明\(f\)在该点[导数为零](../Page/导数.md "wikilink")。

取\(x\in (a,\xi)\)，由最大值定义\(f(\xi)\geq f(x)\)，那么\(\frac{f(x)-f(\xi)}{x-\xi}\geq 0\)。令\(x\rightarrow \xi^-\)，则\(\lim_{x\rightarrow \xi^-} \frac{f(x)-f(\xi)}{x-\xi}\geq 0\)。因为\(f\)在\(\xi\)处可导，所以我们有\(f'(\xi)\geq 0\)。

取\(x\in (\xi,b)\)，那么\(\frac{f(x)-f(\xi)}{x-\xi}\leq 0\)。这时令\(x\rightarrow \xi^+\)，则有\(\lim_{x\rightarrow \xi^+} \frac{f(x)-f(\xi)}{x-\xi}\leq 0\)，所以\(f'(\xi)\leq 0\)。

于是，\(f'(\xi)=0\)。

\(f\)在\(\xi\in(a,b)\)处取得最小值的情况同理。

## 例子

### 第一个例子

[semicircle.svg](https://zh.wikipedia.org/wiki/File:semicircle.svg "fig:semicircle.svg")

考虑函数

\[f(x)=\sqrt{r^2-x^2},\quad x\in[-r,r]\]。

（其中*r* \> 0。）它的图像是中心位于原点的半圆。这个函数在闭区间\[−*r*,*r*\]内连续，在开区间(−*r*,*r*)内可导（但在终点−*r*和*r*处不可导）。由于*f*(−*r*) =
*f*(*r*)，因此根据罗尔定理，存在一个导数为零的点。

### 第二个例子

[Absolute_value.svg](https://zh.wikipedia.org/wiki/File:Absolute_value.svg "fig:Absolute_value.svg")

如果函数在区间内的某个点不可导，则罗尔定理的结论不一定成立。对于某个*a* \> 0，考虑[绝对值函数](../Page/绝对值.md "wikilink")：

\[f(x) = |x|,\qquad x\in[-a,a]\]。

那么*f*(−*a*) =
*f*(*a*)，但−*a*和*a*之间不存在导数为零的点。这是因为，函数虽然是连续的，但它在点*x* = 0不可导。注意*f*的导数在*x* = 0从-1变为1，但不取得值0。

## 推广形式

第二个例子表明罗尔定理下面的一般形式：

考虑一个实数，f(x)是在闭区间\[*a*,*b*\]上的连续函数，并满足f(a) =
f(b).如果对开区间(*a*,*b*)内的任意*x*，右极限

\[f'(x+):=\lim_{h \to 0^+}\frac{f(x+h)-f(x)}{h}\]

而左极限

\[f'(x-):=\lim_{h \to 0^-}\frac{f(x+h)-f(x)}{h}\]

在[扩展的实数轴](../Page/扩展的实数轴.md "wikilink")\[−∞,∞\]上存在，那么开区间(*a*,*b*)内就存在*c*使得这两个极限

\(f'(c+)\quad\)和\(\quad f'(c-)\)

中其中一个≥ 0，另一个≤ 0（在扩展的实数轴上）。如果对任何*x*左极限和右极限都相同，那么它们对*c*也相等，于是在*c*处*f*的导函数存在且等于零。

## 参见

  - [中值定理](../Page/中值定理.md "wikilink")

## 参考文献

## 外部链接

  -
[Category:微积分](../Category/微积分.md "wikilink")
[L](../Category/数学定理.md "wikilink")

1.