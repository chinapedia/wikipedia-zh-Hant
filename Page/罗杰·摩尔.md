**罗杰·乔治·摩尔**爵士，[KBE](../Page/KBE.md "wikilink")（，\[1\]）是一名[英格蘭](../Page/英格蘭.md "wikilink")[演員](../Page/演員.md "wikilink")，因他溫柔和風趣的作風而著名。他又因曾演出兩個虛構的英國動作英雄而出名，分別為[電視節目](../Page/電視節目.md "wikilink")《》（*The
Saint*，1962年至1969年）中的角色[賽門·鄧普勒和在](../Page/賽門·鄧普勒.md "wikilink")1973年至1985年成為[史恩·康納萊的繼承人](../Page/史恩·康納萊.md "wikilink")，於[詹姆士·龐德這套成功的系列電影中飾演](../Page/詹姆士·龐德.md "wikilink")[詹姆士·龐德](../Page/詹姆士·龐德.md "wikilink")。從1991年開始成為[聯合國兒童基金會大使](../Page/聯合國兒童基金會.md "wikilink")。

## 逝世

摩尔的家人於2017年5月23日宣布，摩尔在瑞士死於[癌症](../Page/癌症.md "wikilink")\[2\]\[3\]。

## 電視

  - 七海遊俠  (1962) - 賽門·鄧普勒 ()

## 電影作品

  - (1977)

  - (1980)

  - [炮彈飛車](../Page/炮彈飛車.md "wikilink") (1981)

  - [粉紅豹之探長的詛咒](../Page/粉紅豹之探長的詛咒.md "wikilink") (1983) -克魯索探長

  - [神鬼至尊](../Page/神鬼至尊.md "wikilink")

### 詹姆士·龐德

  - [生死關頭](../Page/生死關頭.md "wikilink") (1973)
  - [金鎗人](../Page/金鎗人.md "wikilink") (1974)
  - [海底城](../Page/海底城.md "wikilink") (1977)
  - [太空城](../Page/太空城.md "wikilink") (1979)
  - [最高機密](../Page/最高機密.md "wikilink") (1981)
  - [八爪女](../Page/八爪女.md "wikilink") (1983)
  - [雷霆殺機](../Page/雷霆殺機.md "wikilink") (1985)

## 参考资料

## 外部連結

  - [Sir Roger Moore @ Fan Site](http://www.rogermoore.prv.pl/) - best
    source of information about Sir Roger Moore's film and TV work on
    the net

  -
  - [Sir Roger Moore's Official Website](http://www.roger-moore.com/) -
    contains articles on Roger's work with UNICEF as well as his
    entertainment career

  - [Classic Movies (1939–1969): Roger
    Moore](https://web.archive.org/web/20060228213523/http://www.thegoldenyears.org/rmoore.html)

  - [Blog about Roger Moore](http://roger-moore.skynetblogs.be)

[Moore, Roger](../Category/英國電影演員.md "wikilink") [Moore,
Roger](../Category/KBE勳銜.md "wikilink") [Moore,
Roger](../Category/CBE勳銜.md "wikilink")
[M](../Category/動作片演員.md "wikilink")
[Category:詹姆士·龐德演員](../Category/詹姆士·龐德演員.md "wikilink")
[Category:法國藝術及文學勳章持有人](../Category/法國藝術及文學勳章持有人.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:倫敦人](../Category/倫敦人.md "wikilink")
[Category:罹患癌症逝世者](../Category/罹患癌症逝世者.md "wikilink")
[Category:20世纪演员](../Category/20世纪演员.md "wikilink")

1.
2.
3.  [Family tweet re death of Sir Roger
    Moore](https://twitter.com/i/web/status/867005447018086400),
    twitter.com; accessed 23 May 2017.