**罗杰·大卫·科恩伯格**（，），[美国](../Page/美国.md "wikilink")[生物化学家](../Page/生物化学家.md "wikilink")，[斯坦福大学](../Page/斯坦福大学.md "wikilink")[结构生物学](../Page/结构生物学.md "wikilink")[教授](../Page/教授.md "wikilink")。因其对“[真核转录的](../Page/真核转录.md "wikilink")[分子基础所作的研究](../Page/分子基础.md "wikilink")”而荣获2006年[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")\[1\]。他的父亲[阿瑟·科恩伯格也是斯坦福大学的教授](../Page/阿瑟·科恩伯格.md "wikilink")，并且是1959年[诺贝尔生理学或医学奖获得者](../Page/诺贝尔生理学或医学奖.md "wikilink")。

## 生平

1947年出生于[密苏里州](../Page/密苏里州.md "wikilink")[圣路易市](../Page/圣路易市.md "wikilink")。1967年，科恩伯格毕业于[哈佛大学](../Page/哈佛大学.md "wikilink")，获得[学士学位](../Page/学士.md "wikilink")，1972年于[斯坦福大学获得](../Page/斯坦福大学.md "wikilink")[博士学位](../Page/博士.md "wikilink")。曾经在位于[剑桥的](../Page/剑桥.md "wikilink")[英国医学研究理事会](../Page/英国医学研究理事会.md "wikilink")[分子生物学实验室做博士后研究](../Page/分子生物学实验室.md "wikilink")。在担任斯坦福大学教授之前，他曾经任教于哈佛医学院生物化学系。他對「[真核生物的](../Page/真核生物.md "wikilink")[DNA](../Page/DNA.md "wikilink")[轉錄機制](../Page/轉錄.md "wikilink")」鑽研超過三十年，累積的研究成果對於瞭解[幹細胞的分化](../Page/幹細胞.md "wikilink")、人類疾病的機制等重要醫學問題，提供重要基礎。

科恩伯格是[美国国家科学院](../Page/美国国家科学院.md "wikilink")（）和[美国艺术与科学研究院](../Page/美国艺术与科学研究院.md "wikilink")（）[院士](../Page/院士.md "wikilink")。

## 外部链接

  - [罗杰·科恩伯格 Roger Kornberg](http://www.chem-station.com/cn/?p=1796)
  - [在斯坦福大学的简介](http://med.stanford.edu/profiles/frdActionServlet?choiceId=printerprofile&fid=4308)
  - [罗杰·科恩伯格实验室的网站](http://kornberg.stanford.edu/)
  - [瑞典皇家科学院的新闻稿](http://www.kva.se/KVA_Root/eng/_press/detail.asp?NewsId=860)

## 参考资料

[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:美國生物化学家](../Category/美國生物化学家.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:欧洲分子生物学组织会员](../Category/欧洲分子生物学组织会员.md "wikilink")
[Category:史丹佛大學醫學院教師](../Category/史丹佛大學醫學院教師.md "wikilink")
[Category:霍维茨奖获得者](../Category/霍维茨奖获得者.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")

1.  <http://www.ch.ntu.edu.tw/nobel/2006.html>