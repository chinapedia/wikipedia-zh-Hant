**罗森海姆**（Rosenheim）位于上巴伐利亚的阿尔卑斯山麓地区，[因河与](../Page/因河.md "wikilink")[芒法尔河交汇处](../Page/芒法尔河.md "wikilink")，是[德国](../Page/德国.md "wikilink")[巴伐利亚州](../Page/巴伐利亚州.md "wikilink")[上巴伐利亚行政区的直辖市](../Page/上巴伐利亚行政区.md "wikilink")，是巴伐利亚州东南部文化和经济中心城市，工农业均很发达，特别是以木材工业著称。

## 历史

公元前15年罗马人在此建立了一个兵站。由于[因河上交通的发展](../Page/因河.md "wikilink")，中世纪时这里成为船夫们的定居点。1328年正式获得了“市场城市”地位。1864年该城被巴伐利亚国王[路德维希二世接管](../Page/路德维希二世.md "wikilink")。纳粹时期该城的犹太人遭到了严重的迫害。盟军进攻德国时对该城进行了猛烈的轰炸。

## 人物

  -
    [戈林](../Page/戈林.md "wikilink")，[纳粹空军元帅](../Page/纳粹.md "wikilink")。
    [汉斯-乌尔里希·鲁德尔](../Page/汉斯-乌尔里希·鲁德尔.md "wikilink")，[纳粹王牌飞行员](../Page/纳粹.md "wikilink")。
    [埃德蒙德·斯托伊贝](../Page/埃德蒙德·斯托伊贝.md "wikilink")，政治家。
    [拉尔斯·本德](../Page/拉尔斯·本德.md "wikilink")，足球运动员。
    [斯文·本德](../Page/斯文·本德.md "wikilink")，足球运动员。

## 教育

[罗森海姆应用技术大学是德南地区尤以木材](../Page/罗森海姆应用技术大学.md "wikilink")（Holztechnik）和经济工程（Wirtschaftsingenieurwesen）专业著称的高等学府。

[R](../Category/巴伐利亚州市镇.md "wikilink")