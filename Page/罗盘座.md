**羅盤座**（）是在南天的一個小且暗淡的星座。它的名稱源自[拉丁文的](../Page/拉丁文.md "wikilink")[水手的指南針](../Page/指南針.md "wikilink")（參考英文時不要與[圓規座混淆](../Page/圓規座.md "wikilink")）。在北緯53度以南的地區可以完整的看見這個星座，在黃昏觀看的最佳時間在每年的一月到三月。

羅盤座是[尼可拉·路易·拉卡伊在](../Page/尼可拉·路易·拉卡伊.md "wikilink")18世紀首度介紹的，它稱之為**船用羅盤（）**，但是後來名稱被縮短了。這個星座的位置靠近古老的[南船座](../Page/南船座.md "wikilink")（*阿戈爾號*），天文學家[約翰·赫歇爾在](../Page/約翰·赫歇爾.md "wikilink")19世紀建議重新命名為[船桅座](../Page/船桅座.md "wikilink")（），但是沒有被接受
。

## 有趣的星

  - [罗盘座ε星](../Page/罗盘座ε.md "wikilink")：这是一个[双星系统](../Page/双星.md "wikilink")。两颗[子星的](../Page/子星.md "wikilink")[星等分别是](../Page/星等.md "wikilink")+5.5和+9.5，从[地球上看相距](../Page/地球.md "wikilink")17.8[弧秒](../Page/弧秒.md "wikilink")。
  - [罗盘座T](../Page/罗盘座T.md "wikilink")：这是一颗[再发新星](../Page/再发新星.md "wikilink")，它的星等通常约为+14，但在爆发时星等可达到+7，爆发时间可能持续100[日或更长](../Page/日.md "wikilink")，最近一次爆发是在1966年。

## 在大眾文化

  - 動漫[聖鬥士星矢：失去的畫布](../Page/聖鬥士星矢_THE_LOST_CANVAS_冥王神話.md "wikilink")，羅盤座被描述為臘斯克（），[山羊座](../Page/聖鬥士星矢_THE_LOST_CANVAS_冥王神話#黃金聖鬥士.md "wikilink")
    “艾爾熙德”（El Cid）的學生。
  - 動漫玩具[爆旋陀螺鋼鐵戰魂斯奧](../Page/爆旋陀螺.md "wikilink")‧阿比士擁有的烈焰羅盤，為羅盤座。

## 參考資料

  - Source of values: <http://www.astro.wisc.edu/~dolan/>
  - Ian Ridpath and Wil Tirion (2007). *Stars and Planets Guide*,
    Collins, London. ISBN 978-0007251209. Princeton University Press,
    Princeton. ISBN 978-0691135564.

## 外部連結

  - [Star Tales – Pyxis](http://www.ianridpath.com/startales/pyxis.htm)

[Luo2](../Category/星座.md "wikilink")
[罗盘座](../Category/罗盘座.md "wikilink")