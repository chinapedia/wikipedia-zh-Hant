**罗莎琳·萨斯曼·耶洛**（，），美国[医学物理学家](../Page/醫學物理.md "wikilink")，因开发多肽类激素的[放射免疫分析法](../Page/放射免疫分析.md "wikilink")，而与[罗歇·吉耶曼和](../Page/罗歇·吉耶曼.md "wikilink")[安德鲁·沙利共同获得](../Page/安德鲁·沙利.md "wikilink")1977年[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。她是自[格蒂·科里之后第二个获得](../Page/格蒂·科里.md "wikilink")[诺贝尔生理学或医学奖的美国女性](../Page/诺贝尔生理学或医学奖.md "wikilink")。\[1\]

## 奖项

1975年耶洛和Berson共同获得。第二年，耶洛成为第一个获得[拉斯克基礎醫學研究獎的女性](../Page/拉斯克基礎醫學研究獎.md "wikilink")。

1977年她与与[罗歇·吉耶曼和](../Page/罗歇·吉耶曼.md "wikilink")[安德鲁·沙利共同获得](../Page/安德鲁·沙利.md "wikilink")[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")，获奖理由是研发了[肽类激素的](../Page/肽类激素.md "wikilink")[放射免疫分析技术](../Page/放射免疫分析.md "wikilink")，是一种在无须采用生物测定方法的情况下用于检测[抗原](../Page/抗原.md "wikilink")（例如，[血清之中](../Page/血清.md "wikilink")[激素的水平](../Page/激素.md "wikilink")）的实验室测定方法。该技术也可以用于识别激素相关的健康问题。此外，也可用于检测血液中的许多外来物质包括某些癌症。最后，该技术可用于测量抗生素的有效性和药物的剂量水平。\[2\]

1978年耶洛被[美国文理科学院选为院士](../Page/美国文理科学院.md "wikilink")\[3\]\[4\]，1988年，获得[美国国家科学奖章](../Page/美国国家科学奖章.md "wikilink")。

## 参见

  - [諾貝爾獎女性得主列表](../Page/諾貝爾獎女性得主列表.md "wikilink")
  - [猶太人諾貝爾獎得主列表](../Page/猶太人諾貝爾獎得主列表.md "wikilink")

## 参考资料

## 延伸阅读

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部链接

  - [诺贝尔官方网站罗莎琳·萨斯曼·耶洛自传](http://nobelprize.org/nobel_prizes/medicine/laureates/1977/yalow-autobio.html)

[category:諾貝爾生理學或醫學獎獲得者](../Page/category:諾貝爾生理學或醫學獎獲得者.md "wikilink")

[Category:美國醫學家](../Category/美國醫學家.md "wikilink")
[Category:美國生物物理學家](../Category/美國生物物理學家.md "wikilink")
[Category:美國物理學家](../Category/美國物理學家.md "wikilink")
[Category:猶太科學家](../Category/猶太科學家.md "wikilink")
[Category:美国女性科学家](../Category/美国女性科学家.md "wikilink")
[Category:女性物理學家](../Category/女性物理學家.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:猶太諾貝爾獎獲得者](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:女性諾貝爾獎獲得者](../Category/女性諾貝爾獎獲得者.md "wikilink")
[Category:拉斯克基礎醫學研究獎得主](../Category/拉斯克基礎醫學研究獎得主.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:伊利諾大學厄巴納－香檳分校校友](../Category/伊利諾大學厄巴納－香檳分校校友.md "wikilink")
[Category:亨特學院校友](../Category/亨特學院校友.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")

1.  [Obituary in *The
    Telegraph*](http://www.telegraph.co.uk/news/obituaries/8553122/Rosalyn-Yalow.html)
2.
3.
4.