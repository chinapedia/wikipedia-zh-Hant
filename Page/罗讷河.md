**罗讷河**（；[普罗旺斯語](../Page/普罗旺斯語.md "wikilink")：**Roun**；,  -
都从[拉丁语](../Page/拉丁语.md "wikilink")**Rhodanus**来）是[欧洲主要](../Page/欧洲.md "wikilink")[河流之一](../Page/河流.md "wikilink")。

## 名称由来

罗讷河这个名称的起源和含义还有争议。凯尔特起源说称**Rhodanus**或**Rodanus**来自**Rhôdan**，意为“转向的”或者“奔跑的”；但希腊起源说的支持者[老普林尼在他](../Page/老普林尼.md "wikilink")《[自然史](../Page/博物志_\(老普林尼\).md "wikilink")》一书中认为罗讷这个名称来自**Rhoda**或**Rhodanusia**，一个罗得斯岛人曾经在河流入海口处建造的殖民地。

## 地理

[Rhoneglacier.JPG](https://zh.wikipedia.org/wiki/File:Rhoneglacier.JPG "fig:Rhoneglacier.JPG")
[Rhone_vs2.jpg](https://zh.wikipedia.org/wiki/File:Rhone_vs2.jpg "fig:Rhone_vs2.jpg")

### 河流

罗讷河源于[瑞士](../Page/瑞士.md "wikilink")[瓦莱州海拔](../Page/瓦莱州.md "wikilink")1753米的[圣哥达峰](../Page/圣哥达峰.md "wikilink")[罗讷冰川的融化](../Page/罗讷冰川.md "wikilink")。在[馬蒂尼附近](../Page/馬蒂尼，瑞士.md "wikilink")，罗讷河水流变急，并成为一条向西南方向流过冰川峡谷的大河。其后，罗讷河向西北流出[阿尔卑斯山脉并投入](../Page/阿尔卑斯山脉.md "wikilink")[日内瓦湖](../Page/日内瓦湖.md "wikilink")（法语：[莱曼湖](../Page/莱曼湖.md "wikilink")）。[日内瓦湖以平均每秒](../Page/日内瓦湖.md "wikilink")570立方米的速度\[1\]向法国境内输出水流。

通过弯弯曲曲的河道流到[里昂](../Page/里昂.md "wikilink")，并在那里与[索恩河汇合之后](../Page/索恩河.md "wikilink")，罗讷河继续向南流，穿过[阿尔卑斯山和](../Page/阿尔卑斯山.md "wikilink")[中央高原之间的平原地带](../Page/中央高原.md "wikilink")。

在[阿尔勒](../Page/阿尔勒.md "wikilink")，年均流速为每秒2300\[2\]立方米的罗讷河分为[大罗讷河和](../Page/大罗讷河.md "wikilink")[小罗讷河两支](../Page/小罗讷河.md "wikilink")，并形成[卡马尔格](../Page/卡马尔格.md "wikilink")[三角洲](../Page/三角洲.md "wikilink")，投入地中海。

### 主要支流及相关河流

#### 瑞士

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/Vispa.md" title="wikilink">Vispa</a></li>
<li><a href="../Page/Navisence.md" title="wikilink">Navisence</a></li>
<li><a href="../Page/Borgne.md" title="wikilink">Borgne</a></li>
<li><a href="../Page/Dranse_(Valais).md" title="wikilink">Dranse</a></li>
<li><a href="../Page/Vièze.md" title="wikilink">Vièze</a></li>
<li><a href="../Page/Grande-Eau.md" title="wikilink">Grande-Eau</a></li>
<li><a href="../Page/Veveyse.md" title="wikilink">Veveyse</a></li>
<li><a href="../Page/Venoge.md" title="wikilink">Venoge</a></li>
</ul></td>
</tr>
</tbody>
</table>

#### 法国

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/阿尔沃河.md" title="wikilink">阿尔沃河</a></li>
<li>le <a href="../Page/Fier_(rivière).md" title="wikilink">Fier</a></li>
<li><a href="../Page/安河.md" title="wikilink">安河</a></li>
<li><a href="../Page/索恩河.md" title="wikilink">索恩河</a></li>
<li><a href="../Page/Gier.md" title="wikilink">Gier</a></li>
<li><a href="../Page/Cance.md" title="wikilink">Cance</a></li>
<li><a href="../Page/Cèze_(rivière).md" title="wikilink">Cèze</a></li>
<li><a href="../Page/Galaure.md" title="wikilink">Galaure</a></li>
<li><a href="../Page/Doux_(rivière).md" title="wikilink">Doux</a></li>
<li><a href="../Page/伊泽尔河.md" title="wikilink">伊泽尔河</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/Eyrieux.md" title="wikilink">Eyrieux</a></li>
<li><a href="../Page/德隆河.md" title="wikilink">德隆河</a></li>
<li><a href="../Page/Ouvèze_Rive_droite.md" title="wikilink">Ouvèze Rive droite</a></li>
<li><a href="../Page/阿尔代什河.md" title="wikilink">阿尔代什河</a></li>
<li><a href="../Page/迪朗斯河.md" title="wikilink">迪朗斯河</a></li>
<li><a href="../Page/艾格河.md" title="wikilink">艾格河</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/Ouvèze_Rive_gauche.md" title="wikilink">Ouvèze Rive gauche</a></li>
<li><a href="../Page/Gardon_(rivière).md" title="wikilink">Gardon</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 主要城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/锡永.md" title="wikilink">锡永</a></li>
<li><a href="../Page/蒙特勒.md" title="wikilink">蒙特勒</a>（<a href="../Page/日內瓦湖.md" title="wikilink">日內瓦湖边</a>）</li>
<li><a href="../Page/洛桑.md" title="wikilink">洛桑</a>（<a href="../Page/日內瓦湖.md" title="wikilink">日內瓦湖边</a>）</li>
<li><a href="../Page/埃維昂萊班.md" title="wikilink">埃維昂萊班</a>（<a href="../Page/日內瓦湖.md" title="wikilink">日內瓦湖边</a>）</li>
<li><a href="../Page/道农雷班.md" title="wikilink">道农雷班</a>（<a href="../Page/日內瓦湖.md" title="wikilink">日內瓦湖边</a>）</li>
</ul></td>
<td><ul>
<li><a href="../Page/日内瓦.md" title="wikilink">日内瓦</a></li>
<li><a href="../Page/里昂.md" title="wikilink">里昂</a></li>
<li><a href="../Page/维埃纳.md" title="wikilink">维埃纳</a></li>
<li><a href="../Page/图尔农.md" title="wikilink">图尔农</a></li>
<li><a href="../Page/瓦伦斯.md" title="wikilink">瓦伦斯</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/蒙泰利马尔.md" title="wikilink">蒙泰利马尔</a></li>
<li><a href="../Page/奥兰治.md" title="wikilink">奥兰治</a></li>
<li><a href="../Page/阿维尼翁.md" title="wikilink">阿维尼翁</a></li>
<li><a href="../Page/博凯尔.md" title="wikilink">博凯尔</a></li>
<li><a href="../Page/塔拉斯孔.md" title="wikilink">塔拉斯孔</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/阿尔勒.md" title="wikilink">阿尔勒</a></li>
<li><a href="../Page/罗讷圣路易港.md" title="wikilink">罗讷圣路易港</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 历史

罗讷河从远古时期便是一条重要的商道，连接着欧洲北部及[地中海](../Page/地中海.md "wikilink")。

[二战](../Page/二战.md "wikilink")1942年[義大利軍队進駐](../Page/意大利王國.md "wikilink")[隆河以東的法國領土](../Page/隆河.md "wikilink")，此河在1942年至1943年為[納粹德國和義大利的边界](../Page/納粹德國.md "wikilink")。

## 航运

罗讷河是一条内河航行要道，将众多工业城市，例如[阿尔勒](../Page/阿尔勒.md "wikilink")、[阿维尼翁](../Page/阿维尼翁.md "wikilink")、[瓦伦斯](../Page/瓦伦斯.md "wikilink")、[维埃纳或](../Page/维埃纳.md "wikilink")[里昂](../Page/里昂.md "wikilink")，与[地中海港口](../Page/地中海.md "wikilink")[福斯](../Page/福斯.md "wikilink")、[马赛](../Page/马赛.md "wikilink")、[赛特连接着](../Page/赛特.md "wikilink")。罗讷河从[索恩河汇合点到](../Page/索恩河.md "wikilink")[地中海都按五级水路发展](../Page/地中海.md "wikilink")。同样改造为[运河的](../Page/运河.md "wikilink")[索恩河将罗讷河上的港口连接到Villefranche](../Page/索恩河.md "wikilink"),
Macon and
Chalons这些城市。小型船-{只}-可以继续向西北，北或东北行驶，通过中部-卢瓦尔-布里亚尔运河和庐安运河到达[塞纳河](../Page/塞纳河.md "wikilink")，通过马恩-索恩运河进入马恩河，通过孚日运河（原"东部运河-南支"）进入摩澤爾河或通过罗讷-莱茵运河进入[莱茵河](../Page/莱茵河.md "wikilink")。

罗讷河因其强大水流而闻名：水流速度可达到每小时10公里，使得航行有一定难度。十道河闸每日从早上5点到晚上9点运行，管制着运河的通船。\[3\].

## 水文资料

### 水文周期

罗讷河的水文周期是秋天和春天达到高峰，秋天由于雨水，春天由于冰雪的融化。冬天水流平和而夏季水流达到最低点。

[博凯尔的平均水流量为每秒](../Page/博凯尔.md "wikilink")1650立方米。一旦水流量超过每秒5000立方米，罗讷河被认为泛滥。
有档案以来最高值纪录于2003年12月在博凯尔达到每秒13000立方米[1](https://web.archive.org/web/20051017103717/http://www.ens-lsh.fr/geoconfluence/doc/transv/Risque/RisqueDoc.htm)，高于1840年和1856年11月的两次洪水。历史上最大的一次洪水有可能是1548年甚至是580年的那次。[世纪大洪水则被认为能够达到每秒](../Page/世纪大洪水.md "wikilink")14000立方米以上（在每秒14000与16000立方米之间）。

罗讷河三角洲经过18世纪到19世纪几次大洪水逐渐有了变化。

## 氣候

冬春兩季有乾冷強烈的風沿河谷向南吹，被稱作[密斯托拉風](../Page/密斯托拉風.md "wikilink")，密斯托拉風在隆河河谷一代最為明顯，常造成作物的災害。

## 艺术

[Starry_Night_Over_the_Rhone.jpg](https://zh.wikipedia.org/wiki/File:Starry_Night_Over_the_Rhone.jpg "fig:Starry_Night_Over_the_Rhone.jpg")》\]\]
[梵高曾画过](../Page/梵高.md "wikilink")《[罗讷河上的星月夜](../Page/罗讷河上的星月夜.md "wikilink")》

## 参考

<div class="references-small">

<references/>

</div>

## 参见

  - [罗讷省](../Page/罗讷省.md "wikilink")
  - [罗讷河谷](../Page/罗讷河谷.md "wikilink")（葡萄酒产区）

## 外部链接

  - [罗讷河网站](https://web.archive.org/web/20061006083715/http://pelo.ton-hebergement-gratuit.com/)
  - [罗讷河官方网站](https://web.archive.org/web/20060812122123/http://www.fleuverhone.com/)
  - [实况水文数据](http://www.rdbrmc.com/hydroreel2/)
  - [具体数据](https://web.archive.org/web/20060618172500/http://www.eptb-rhone.fr/sitsemt/c02aa.html)
  - [法国的水路](http://www.discoverfrance.net/France/DF_waterways.shtml)

[Category:法國河流](../Category/法國河流.md "wikilink")
[Category:瑞士河流](../Category/瑞士河流.md "wikilink")
[Category:歐洲跨國河流](../Category/歐洲跨國河流.md "wikilink")

1.  [罗讷河：一条严重人类利用的水资源的生态复原](http://www.iucn.org/themes/wani/flow/cases/France.pdf)
2.  [罗讷河：一条严重人类利用的水资源的生态复原](http://www.iucn.org/themes/wani/flow/cases/France.pdf)
3.  [NoorderSoft水路数据库](http://www.noordersoft.com/indexen.html)