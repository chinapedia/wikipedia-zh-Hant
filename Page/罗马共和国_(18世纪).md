18世纪的**罗马共和国**是在1798年到1799年存在过的一个短命的國家。

1798年3月7日，法国入侵罗马的教皇国，建立了新的羅馬共和國，部分原因是为了报复前一年谋杀法国的将军马图林·伦纳德。

教皇[庇护六世被押解到法国](../Page/庇护六世.md "wikilink")，死于1799年。1799年，蒂贝里纳共和国（Tiberina
Republic）并入到了罗马共和国中。

1800年6月，[教皇国重新建立](../Page/教皇国.md "wikilink")。

## 参见

  - [罗马共和国 (19世纪)](../Page/罗马共和国_\(19世纪\).md "wikilink")

[Category:義大利歷史](../Category/義大利歷史.md "wikilink")
[Category:已不存在的歐洲國家](../Category/已不存在的歐洲國家.md "wikilink")
[Category:已不存在的共和國](../Category/已不存在的共和國.md "wikilink")
[Category:短命國家](../Category/短命國家.md "wikilink")