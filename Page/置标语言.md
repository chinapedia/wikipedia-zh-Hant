**置标语言**（也称**-{zh-hans:置标语言; zh-hant:置標語言}-**、**-{zh-hans:标记语言;
zh-hant:標記語言}-**、**-{zh-hans:标志语言; zh-hant:標誌語言}-**、**-{zh-hans:标识语言;
zh-hant:標識語言}-**、markup
language）是一種将文本（）以及文本相关的其他信息结合起来，展现出关于文档结构和数据处理细节的计算机文字编码。与文本相关的其他信息（包括例如文本的结构和表示信息等）与原来的文本结合在一起，但是使用标记（markup）进行标识。当今广泛使用的置标语言是[超文本置标语言](../Page/超文本置标语言.md "wikilink")（，）和[可扩展置标语言](../Page/可扩展置标语言.md "wikilink")（，）。置标语言广泛应用于[网页和](../Page/网页.md "wikilink")[网络应用程序](../Page/网络应用程序.md "wikilink")。标记最早用于出版业，是作者、编辑以及出版商之间用于描述出版作品的排版格式所使用的。

## 标志语言的分类

标志语言通常可以分为三类：表示性的、过程性的以及描述性的。

### 表示性的标志语言

**表示性的标志语言（Presentational
markup）**是在编码过程中，标记文档的结构信息。例如，在[文本文件中](../Page/文本.md "wikilink")，文件的标题可能需要用特定的格式表示（居中，放大等），这样我们就需要标记文件的标题。字处理以及[桌面出版产品有时候能够自动推断出这类的结构信息](../Page/桌面出版.md "wikilink")，但是绝大多数的，像Wiki这样的纯文本编辑器还不能解决这个问题。

### 过程性标识

**过程性标志语言（Procedural
markup）**一般都專門於文字的表達，但通常对于文本编辑者可见，并且能够被软件依其出现顺序依次解读。为了格式化一个标题，在标题文本之前，会紧接着插入一系列的格式标识符，以指示计算机转换到居中的显示模式，同时加大及加粗字体。在标题文本之后，会紧接缀上格式中止标识；对于更高级的系统宏命令或者[堆栈模式会让这一过程的实现方式更加丰富](../Page/堆栈.md "wikilink")。大多数情况下，过程性标志能力包含有一个[Turing-complete编程语言](../Page/Turing-complete.md "wikilink")。过程性标志语言的范例有：[nroff](../Page/nroff.md "wikilink"),
[troff](../Page/troff.md "wikilink"), [TeX](../Page/TeX.md "wikilink"),
[Lout以及](../Page/Lout.md "wikilink")[PostScript](../Page/PostScript.md "wikilink")。过程性标志语言被广泛应用在专业出版领域，专业的出版商会根据要求使用不同的指标语言以达到出版要求。

### 描述性标识

**描述性标识（Descriptive
Markup）**也称**通用标识**，所描述的是文件的内容或结构，而不是文件的显示外观或样式，制定SGML的基本思想就是把文档的内容与样式分开，XML、SGML都是典型的通用标识语言。

## 历史

“标识（markup）”这个词来源自传统出版业的“标记”一个[手稿](../Page/手稿.md "wikilink")，也就是在原稿的边缘加注一些符号来指示[打印上的要求](../Page/打印.md "wikilink")。长久以来，这个工作都是由专门的人（"markup
men"）以及校对人来进行，对原稿标志出使用什么样的[字体](../Page/字体.md "wikilink")，字型以及字号，然后再将原稿交给其它人进行手工的[排版工作](../Page/排版.md "wikilink")。

### GenCode

### TeX

**TeX**是一个由美国电脑教授[高德纳](../Page/高德纳.md "wikilink")（Donald E.
Knuth）编写的功能强大的排版软件。它在学术界十分流行，特别是[数学](../Page/数学.md "wikilink")、[物理学和](../Page/物理学.md "wikilink")[计算机科学界](../Page/计算机科学.md "wikilink")。TeX被普遍认为是一个很好的排版工具，特别是在处理复杂的数学公式时。利用诸如是[LaTeX等终端软件](../Page/LaTeX.md "wikilink")，TeX就能够排版出精美的文本。

### SGML

[SGML是一种专门的标记语言](../Page/SGML.md "wikilink")，被用作编写《牛津英语词典》的电子版本。由于SGML的复杂，导致难以普及。

### HTML

超文件标识语言（英文：HyperText Markup
Language，简称为HTML）是为[网页创建和其它可在](../Page/网页.md "wikilink")[网页浏览器中看到的信息设计的一种标志语言](../Page/网页浏览器.md "wikilink")。

### XML

可扩展标识语言（eXtensible Markup
Language，简称XML），又称可扩展标记语言，是一种标志语言。标志指计算机所能理解的信息符号，通过此种标记，计算机之间可以处理包含各种信息的文章等。

### XHTML

可延伸超文本标识语言（eXtensible HyperText Markup
Language，XHTML），是一种标志语言，表现方式与超文本标志语言（HTML）类似，不过语法上更加严格。

### 其它基于XML的应用

还有其它一些基于XML的应用，比如[RDF](../Page/RDF.md "wikilink")、[XForms](../Page/XForms.md "wikilink")、[DocBook](../Page/DocBook.md "wikilink")、[SOAP以及](../Page/SOAP.md "wikilink")[Web
Ontology
Language](../Page/Web_Ontology_Language.md "wikilink")（OWL）。具体可以参见[XML标记语言列表](../Page/XML标记语言列表.md "wikilink").

## 特征

## 其他应用

  - [XAML](../Page/XAML.md "wikilink")（Extensible Application Markup
    Language），基於XML語言，在微軟[WPF](../Page/WPF.md "wikilink")（Windows
    Presentation Foundation）中使用。

## 参考文献

  - [TEI
    guidelines](https://web.archive.org/web/20070905132641/http://www.tei-c.org/Guidelines2/index.html)
  - [Markup systems and the future of scholarly text
    processing](http://xml.coverpages.org/coombs.html) by James H.
    Coombs, Allen H. Renear, and Steven J. DeRose. Originally published
    in the November 1987
    [CACM](../Page/Communications_of_the_ACM.md "wikilink"), and
    reprinted several times in other forums, this article introduced
    many of the concepts now used in discussing markup languages, and
    lays out the basic arguments for the superior usability of
    descriptive markup.

## 外部連結

  - [World Wide Web Consortium (W3C)](http://www.w3.org/)
  - [International Organization for Standardization
    (ISO)](http://www.iso.org/)

## 參見

  - [層疊樣式表](../Page/層疊樣式表.md "wikilink")（CSS）
  - [轻量级标记语言](../Page/轻量级标记语言.md "wikilink")
  - [用戶介面標記語言](../Page/用戶介面標記語言.md "wikilink")
  - [可縮放向量圖形](../Page/可縮放向量圖形.md "wikilink")（SVG）
  - [矢量電氣圖標記語言](../Page/矢量電氣圖標記語言.md "wikilink")
  - [標記語言列表](../Page/標記語言列表.md "wikilink")
  - [程式設計語言](../Page/程式設計語言.md "wikilink")：與標記語言相對，有邏輯判斷功能，可對[計算機](../Page/計算機.md "wikilink")（）進行操作，以達程式設計目的
  - [YAML](../Page/YAML.md "wikilink")：一種類似的標記語言
  - [維基文本](../Page/維基文本.md "wikilink")

{{-}}

[Category:计算机语言](../Category/计算机语言.md "wikilink")
[标记语言](../Category/标记语言.md "wikilink")