**羅伯·威爾森**（Robert
Wilson，）出生於[美國](../Page/美國.md "wikilink")[德克薩斯州](../Page/德克薩斯州.md "wikilink")[瓦克市](../Page/瓦克市.md "wikilink")，是一位國際知名的美國[戲劇](../Page/戲劇.md "wikilink")[導演和](../Page/導演.md "wikilink")[舞台設計師](../Page/舞台設計.md "wikilink")。最知名的戲劇作品為《沙灘上的愛因斯坦》（*Einstein
on the
Beach*），此劇被譽為[後現代主義中的戲劇代表作品](../Page/後現代主義.md "wikilink")。音樂由[極簡主義音樂家](../Page/極簡主義.md "wikilink")[菲利普·格拉斯](../Page/菲利普·格拉斯.md "wikilink")（Philip
Glass）所創作。

## 生平

羅伯·威爾森在德州長大，青少年時期有說話困難的毛病，是一位舞蹈家Byrd
Hoffman所治好的，大學時就讀[德州大學奧斯丁分校](../Page/德州大學奧斯丁分校.md "wikilink")，主修工商管理。在1963年搬到[紐約市](../Page/紐約市.md "wikilink")[布魯克林區](../Page/布魯克林區.md "wikilink")，就讀[普瑞特藝術學院](../Page/普瑞特藝術學院.md "wikilink")[建築系](../Page/建築.md "wikilink")。1964年曾在[巴黎學過繪畫](../Page/巴黎.md "wikilink")，1965年開始學習編舞，1969年正式推出自己的戲劇作品《西班牙國王》，1976年以《沙灘上的愛因斯坦》一舉成名，1993年獲得[威尼斯雙年展中的](../Page/威尼斯雙年展.md "wikilink")[金獅獎](../Page/金獅獎.md "wikilink")，作品是裝置藝術《床上的愛麗絲》（Alice
in bed）。目前羅伯·威爾森有自己的劇場，名字為「意象劇場」（Theatre of Images）。

## 作品

  - 《西班牙國王》（*The King of Spain*，1969年）
  - 《佛洛依得的生命與時光》（*The Life and Times of Sigmund Freud*，1969年）
  - 《聾人一瞥》 （*Deafman Glance*，與一起創作，1971年）
  - 《KA山與Guardenia台地》（*KA MOUNTain and GUARDenia Terrace*，1972年）
  - 《史達林的生命與時光》（*The Life and Times of Joseph Stalin*，1973年）
  - 《維多利亞女皇的信》（*A Letter from Queen Victoria*，1974年）
  - 《沙灘上的愛因斯坦》（*Einstein on the Beach*，與菲力普·葛拉斯合作，1976年）
  - 《死亡毀滅與底特律》 （*Death Destruction & Detroit*，1979年）
  - 《死亡毀滅與底特律二》 （*Death Destruction & Detroit II*，1987年）
  - 《黑騎士：十二顆神奇子彈》（*The Black Rider: The Casting of the Twelve Magic
    Bullets*，和[威廉·柏洛茲與](../Page/威廉·柏洛茲.md "wikilink")[汤姆·威茨共同創作](../Page/汤姆·威茨.md "wikilink")，
    1990年）
  - 《愛麗絲》（*Alice*，音樂劇，與汤姆·威茨共同創作，1992年）
  - 《胡錫傳》（*Woyzeck*，劇本，與汤姆·威茨共同創作，2002年）
  - *Jean de La Fontaine's The Fables*，2004年

## 外部連結

  - [羅伯威爾森官方網站](http://www.robertwilson.com)－英文
  - [英國Design
    Museum的介紹](http://www.designmuseum.org/designerex/robert-wilson.htm)－英文

[category:美國劇作家](../Page/category:美國劇作家.md "wikilink")

<a id="TOP">A</a>

[Category:普瑞特艺术学院校友](../Category/普瑞特艺术学院校友.md "wikilink")