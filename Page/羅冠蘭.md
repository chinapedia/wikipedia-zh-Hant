**羅冠蘭**（**Lo Koon
Lan**，），現為[香港演藝學院副教授](../Page/香港演藝學院.md "wikilink")，主要從事教育工作及[舞台劇創作](../Page/舞台劇.md "wikilink")，戲劇研究範圍包括[寫實主義及](../Page/寫實主義.md "wikilink")[荒誕劇](../Page/荒诞派戏剧.md "wikilink")，亦為電影及電視演員。前[香港話劇團首席演員](../Page/香港話劇團.md "wikilink")，曾參與及演出超過一百部舞台劇，均擔演主要角色。羅氏致力推動戲劇治療，主張戲劇除了藝術創作、娛樂外，有其社會功能。曾於[香港浸會大學及](../Page/香港浸會大學.md "wikilink")[香港大學主持自我尋找](../Page/香港大學.md "wikilink")（Self-discovery）工作坊。1994年獲選為[香港十大傑出青年](../Page/香港十大傑出青年.md "wikilink")。

## 教育

羅冠蘭是香港演藝界[學院派的代表人物](../Page/學院派.md "wikilink")。1972年畢業於[聖公會諸聖中學](../Page/聖公會諸聖中學.md "wikilink")\[1\]，後畢業於[香港浸會學院](../Page/香港浸會學院.md "wikilink")（今香港浸會大學）歷史系、[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")[密德薩斯大學](../Page/密德薩斯大學.md "wikilink")（）東西方戲劇研究[碩士](../Page/碩士.md "wikilink")，[香港大學專業進修學院戲劇研究學士後文憑](../Page/香港大學專業進修學院.md "wikilink")，並修畢[羅富國教育學院](../Page/羅富國教育學院.md "wikilink")（歷史及地理系）證書課程，以及英國倫敦[珊薩密學院](../Page/珊薩密學院.md "wikilink")（）「行為及戲劇治療」證書課程。

羅冠蘭一向致力於戲劇表演的教育工作，曾經為[香港大學](../Page/香港大學.md "wikilink")、[香港浸會大學](../Page/香港浸會大學.md "wikilink")、[香港城市大學等設計和主持戲劇創作及](../Page/香港城市大學.md "wikilink")“自我尋找”訓練課程，曾為香港[無線電視藝員訓練班導師及顧問](../Page/無線電視藝員訓練班.md "wikilink")，以及多間電影公司之演員訓練班導師，“中國星”電影公司演員訓練班校長，並曾於[香港科技大學任教](../Page/香港科技大學.md "wikilink")“”
課程。現為[香港演艺学院副教授](../Page/香港演艺学院.md "wikilink")，“[冠作坊](../Page/冠作坊.md "wikilink")”（Koon
Workshop）董事及課程設計導師。

## 演藝工作

羅冠蘭於香港浸會學院歷史學系畢業後，曾在多間大學出席研究課程。1982年加入[香港話劇團](../Page/香港話劇團.md "wikilink")，後晉升為首席演員，亦多次獲得舞台劇演員獎。
她曾參演舞台劇超過100套，演出超過2,000多場次，亦多次被邀請於中國內地及海外如[美加](../Page/美加.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[台湾等地演出舞台劇](../Page/台湾.md "wikilink")。曾於美國[紐約The](../Page/紐約.md "wikilink")
Circle Repertory實驗室學習，期間參演由奧斯卡影帝威廉赫特（William Hurt）導演之舞台劇《如此姊妹》（Those
Inconvenient Sisters）。其他演出主要担演主角，如：《心靈病房》教授Vivian
Bearing、《海鷗》之妮娜、《三姊妹》之瑪莎、《六個尋找劇作家的角色》之繼女、莎剧《奧賽羅》（Othello）之Desdemona、《無事生非》之Beatrice、《羅密歐與茱麗葉》之茱麗葉、《安提崗妮》之安提崗妮、《聖女貞德》之貞德、《女店主》之女店主米蘭多琳娜、《北京人》之曾思懿、《雷雨》之繁漪及《我和春天有個約會》之金露露等等。及後，舞台剧《我和春天有個約會》被拍成同名電影，其演出的角色金露露獲第14屆香港電影金像獎最佳女配角。

1990年代開始業餘參與拍攝電影及電視，亦多次被提名香港電影金像奬及提名台灣[金馬獎](../Page/金馬獎.md "wikilink")。1994年起在無綫演出了多部劇集，演技都獲得肯定，在《烈火雄心》飾演群姐，《[天子尋龍](../Page/天子尋龍.md "wikilink")》中飾演[武則天](../Page/武則天.md "wikilink")、《[學警雄心](../Page/學警雄心.md "wikilink")》中的陳燕蘭、《[阿旺新傳](../Page/阿旺新傳.md "wikilink")》中的何麗莎、《[秀才愛上兵](../Page/秀才愛上兵.md "wikilink")》的女俠游玉蘭等都較為人熟知。

2016年拍攝電影《[寵我](../Page/寵我.md "wikilink")》，飾演媽咪一角，獲得多個國際電影節及影展肯定，並為羅氏帶來十個最佳女主角及一個最佳女配角等獎項。

## 演出作品

### 電影

|                                                            |                                                |             |
| ---------------------------------------------------------- | ---------------------------------------------- | ----------- |
| **首播**                                                     | **劇名**                                         | **角色**      |
| 1987年                                                      | [你OK我OK](../Page/你OK我OK.md "wikilink")         |             |
| 1988年                                                      | [鸡同鸭讲](../Page/鸡同鸭讲.md "wikilink")             | 猩嫂          |
| [繼續跳舞](../Page/繼續跳舞.md "wikilink")                         |                                                |             |
| 1990年                                                      | [新半斤八兩](../Page/新半斤八兩.md "wikilink")           | 蘭姐          |
| [廟街皇后](../Page/廟街皇后.md "wikilink")                         | 燕萍                                             |             |
| 1992年                                                      | [不文骚](../Page/不文骚.md "wikilink")               |             |
| 1993年                                                      | [紙盒藏屍之公審](../Page/紙盒藏屍之公審.md "wikilink")       | 主控官         |
| 1994年                                                      | [燈籠](../Page/燈籠_\(電影\).md "wikilink")          |             |
| [伴我同行](../Page/伴我同行.md "wikilink")                         |                                                |             |
| [記得香蕉成熟時II之初戀情人](../Page/記得香蕉成熟時II之初戀情人.md "wikilink")     | 江普照                                            |             |
| [重案實錄](../Page/重案實錄.md "wikilink")                         |                                                |             |
| [我和春天有個約會](../Page/我和春天有個約會_\(電影\).md "wikilink")          | 金露露                                            |             |
| 1995年                                                      | [炸彈情人](../Page/炸彈情人.md "wikilink")             |             |
| [影子敵人](../Page/影子敵人.md "wikilink")                         |                                                |             |
| [女人四十](../Page/女人四十.md "wikilink")                         | 姨姨                                             |             |
| [人間有情](../Page/人間有情.md "wikilink")                         |                                                |             |
| [搶閘媽咪](../Page/搶閘媽咪.md "wikilink")                         |                                                |             |
| 1996年                                                      | [港督最後一個保鏢](../Page/港督最後一個保鏢.md "wikilink")     | 鍾方安心        |
| [浪漫風暴](../Page/浪漫風暴.md "wikilink")                         | Ken母                                           |             |
| [天才與白痴](../Page/天才與白痴_\(1996年電影\).md "wikilink")           | 妙蓮                                             |             |
| 1997年                                                      | [奪舍](../Page/奪舍_\(電影\).md "wikilink")          | 雞妻          |
| [夜半2點鐘](../Page/夜半2點鐘.md "wikilink")                       |                                                |             |
| [97家有喜事](../Page/97家有喜事.md "wikilink")                     | 丁太／九嫂                                          |             |
| [我愛廚房](../Page/我愛廚房.md "wikilink")                         | Chika                                          |             |
| [完全結婚手冊](../Page/完全結婚手冊.md "wikilink")                     |                                                |             |
| 1998年                                                      | [全職大盜](../Page/全職大盜.md "wikilink")             |             |
| [陽性反應](../Page/陽性反應.md "wikilink")                         |                                                |             |
| [驚天大賊王](../Page/驚天大賊王.md "wikilink")                       | 鍾方安心                                           |             |
| [愛情傳真](../Page/愛情傳真.md "wikilink")                         |                                                |             |
| [失業特工隊](../Page/失業特工隊.md "wikilink")                       | 導師                                             |             |
| 1999年                                                      | [愛滋初體驗](../Page/愛滋初體驗.md "wikilink")           |             |
| [新家法](../Page/新家法.md "wikilink")                           |                                                |             |
| [皇家香港警察的最後一夜之一體兩旗](../Page/皇家香港警察的最後一夜之一體兩旗.md "wikilink") | 新妻                                             |             |
| [黑道風雲之收數王](../Page/黑道風雲之收數王.md "wikilink")                 | 陳小姐                                            |             |
| 2000年                                                      | [少年十五十六時](../Page/少年十五十六時.md "wikilink")       |             |
| [順其自然](../Page/順其自然.md "wikilink")                         |                                                |             |
| 2001年                                                      | [有人說愛我](../Page/有人說愛我.md "wikilink")           |             |
| [童黨2001](../Page/童黨2001.md "wikilink")                     | 俊母                                             |             |
| [英雄人物](../Page/英雄人物.md "wikilink")                         |                                                |             |
| [愛情白麵包](../Page/愛情白麵包.md "wikilink")                       |                                                |             |
| [飛哥傳奇](../Page/飛哥傳奇.md "wikilink")                         |                                                |             |
| [老夫子2001](../Page/老夫子2001.md "wikilink")                   | 譚例珠                                            |             |
| 2002年                                                      | [反收數特遣隊](../Page/反收數特遣隊.md "wikilink")         |             |
| [賭神之神](../Page/賭神之神.md "wikilink")                         |                                                |             |
| [星星與月亮](../Page/星星與月亮.md "wikilink")                       |                                                |             |
| 2003年                                                      | [六樓后座](../Page/六樓后座.md "wikilink")             | 增值導師 Ms.Kim |
| [我要結婚](../Page/我要結婚.md "wikilink")                         |                                                |             |
| [失鎗72小時](../Page/失鎗72小時.md "wikilink")                     | 成嫂                                             |             |
| [夜間管理員](../Page/夜間管理員.md "wikilink")                       |                                                |             |
| 2004年                                                      | [婚前殺行爲](../Page/婚前殺行爲.md "wikilink")           | 祖母親         |
| [絕世好賓](../Page/絕世好賓.md "wikilink")                         | 蘭姐                                             |             |
| [鬼馬狂想曲](../Page/鬼馬狂想曲.md "wikilink")                       | 問答遊戲主持                                         |             |
| 2006年                                                      | [左麟右李之我愛醫家人](../Page/左麟右李之我愛醫家人.md "wikilink") |             |
| 2013年                                                      | [葉問：終極一戰](../Page/葉問：終極一戰.md "wikilink")       | 耀華妻         |
| 2014年                                                      | [Z風暴](../Page/Z風暴.md "wikilink")               |             |
| 2017年                                                      | [寵我](../Page/寵我.md "wikilink")                 | 媽咪          |

### 電視劇（[香港電台](../Page/香港電台.md "wikilink")）

| 首播    | 剧名                                                                                           | 角色    |
| ----- | -------------------------------------------------------------------------------------------- | ----- |
| 1991年 | [《小說家族：人間有情 - 杜國威》](http://podcast.rthk.hk/podcast/item_epi.php？pid=737&lang=zh-CN&id=46699) | 陳瑩    |
| 1993年 | [《驕陽歲月：女巫的另一面》](https://www.youtube.com/watch?v=1saIk6AOyC4)                                 | 鄭雅儀母親 |

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/翡翠台.md" title="wikilink">翡翠台首播</a></p></th>
<th><p>劇名</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1994年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>11月21日-1995年1月13日</p></td>
<td><p><a href="../Page/笑看風雲.md" title="wikilink">笑看風雲</a></p></td>
<td><p>李玉芬（媽記）</p></td>
</tr>
<tr class="odd">
<td><p><strong>1995年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>5月1日-5月26日</p></td>
<td><p><a href="../Page/包青天_(無綫電視劇集).md" title="wikilink">包青天之親子情仇</a></p></td>
<td><p>杜慧瓊</p></td>
</tr>
<tr class="odd">
<td><p><strong>1997年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>9月1日-10月24日</p></td>
<td><p><a href="../Page/刑事偵緝檔案III.md" title="wikilink">刑事偵緝檔案III</a></p></td>
<td><p>朱秀筠（Deborah）</p></td>
</tr>
<tr class="odd">
<td><p><strong>1998年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6月1日-7月31日</p></td>
<td><p><a href="../Page/鹿鼎記_(1998年電視劇).md" title="wikilink">鹿鼎記</a></p></td>
<td><p><a href="../Page/韋春花.md" title="wikilink">韋春花</a></p></td>
</tr>
<tr class="odd">
<td><p>10月12日-12月6日</p></td>
<td><p><a href="../Page/烈火雄心_(電視劇).md" title="wikilink">烈火雄心</a></p></td>
<td><p>郭麗群</p></td>
</tr>
<tr class="even">
<td><p><strong>1999年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8月9日-9月3日</p></td>
<td><p><a href="../Page/雙面伊人.md" title="wikilink">雙面伊人</a></p></td>
<td><p>阮玉嫻</p></td>
</tr>
<tr class="even">
<td><p>9月13日-10月8日</p></td>
<td><p><a href="../Page/人龍傳說.md" title="wikilink">人龍傳說</a></p></td>
<td><p>刑湘媚</p></td>
</tr>
<tr class="odd">
<td><p><strong>2000年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2月14日-3月10日</p></td>
<td><p><a href="../Page/緣份無邊界.md" title="wikilink">緣份無邊界</a></p></td>
<td><p>談鳳凰</p></td>
</tr>
<tr class="odd">
<td><p>11月20日-2001年1月12日</p></td>
<td><p><a href="../Page/妙手仁心II.md" title="wikilink">妙手仁心II</a></p></td>
<td><p>莫雪芳</p></td>
</tr>
<tr class="even">
<td><p><strong>2001年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>12月24日-2002年1月18日</p></td>
<td><p><a href="../Page/勇探實錄.md" title="wikilink">勇探實錄</a></p></td>
<td><p>廖秀珠</p></td>
</tr>
<tr class="even">
<td><p><strong>2002年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9月16日-11月15日</p></td>
<td><p><a href="../Page/流金歲月_(香港電視劇).md" title="wikilink">流金歲月</a></p></td>
<td><p>法　官</p></td>
</tr>
<tr class="even">
<td><p><strong>2003年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1月27日-2月21日</p></td>
<td><p><a href="../Page/天子尋龍.md" title="wikilink">天子尋龍</a></p></td>
<td><p><a href="../Page/武則天.md" title="wikilink">武則天</a></p></td>
</tr>
<tr class="even">
<td><p>10月27日-12月20日</p></td>
<td><p><a href="../Page/衝上雲霄.md" title="wikilink">衝上雲霄</a></p></td>
<td><p>陳玉琴</p></td>
</tr>
<tr class="odd">
<td><p>電視電影</p></td>
<td><p><a href="../Page/懸案追兇.md" title="wikilink">懸案追兇</a></p></td>
<td><p>霍陳桂枝</p></td>
</tr>
<tr class="even">
<td><p><strong>2004年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4月3日-5月15日</p></td>
<td></td>
<td><p>張秀蘭</p></td>
</tr>
<tr class="even">
<td><p>4月26日-5月21日</p></td>
<td><p><a href="../Page/下一站彩虹.md" title="wikilink">下一站彩虹</a></p></td>
<td><p>丁麗嫦</p></td>
</tr>
<tr class="odd">
<td><p>6月21日-7月17日</p></td>
<td><p><a href="../Page/隔世追兇.md" title="wikilink">隔世追兇</a></p></td>
<td><p>梁妙蘭／梁少芬</p></td>
</tr>
<tr class="even">
<td><p>10月3日-2005年1月3日</p></td>
<td><p><a href="../Page/赤沙印記@四葉草.2.md" title="wikilink">赤沙印記@四葉草.2</a></p></td>
<td><p>方陳貴妹</p></td>
</tr>
<tr class="odd">
<td><p>12月27日-2005年1月29日</p></td>
<td><p><a href="../Page/水滸無間道.md" title="wikilink">水滸無間道</a></p></td>
<td><p>萬麗冰</p></td>
</tr>
<tr class="even">
<td><p><strong>2005年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4月11日-5月24日</p></td>
<td><p><a href="../Page/學警雄心.md" title="wikilink">學警雄心</a></p></td>
<td><p>陳燕蘭</p></td>
</tr>
<tr class="even">
<td><p>4月16日</p></td>
<td><p><a href="../Page/奇幻潮.md" title="wikilink">奇幻潮之我愛你</a></p></td>
<td><p>Joanna母</p></td>
</tr>
<tr class="odd">
<td><p>6月6日-7月1日</p></td>
<td><p><a href="../Page/情迷黑森林.md" title="wikilink">情迷黑森林</a></p></td>
<td><p>姿整四娘</p></td>
</tr>
<tr class="even">
<td><p>6月22日-7月16日</p></td>
<td><p><a href="../Page/奪命真夫.md" title="wikilink">奪命真夫</a></p></td>
<td><p>法　官</p></td>
</tr>
<tr class="odd">
<td><p>10月24日-12月6日</p></td>
<td><p><a href="../Page/阿旺新傳.md" title="wikilink">阿旺新傳</a></p></td>
<td><p>何麗莎</p></td>
</tr>
<tr class="even">
<td><p>11月28日-12月30日</p></td>
<td><p><a href="../Page/本草藥王.md" title="wikilink">本草藥王</a></p></td>
<td><p>張　嬌</p></td>
</tr>
<tr class="odd">
<td><p><strong>2006年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1月2日-1月27日</p></td>
<td><p><a href="../Page/謎情家族.md" title="wikilink">謎情家族</a></p></td>
<td><p>陳麗雲</p></td>
</tr>
<tr class="odd">
<td><p>2月20日-3月17日</p></td>
<td><p><a href="../Page/天幕下的戀人.md" title="wikilink">天幕下的戀人</a></p></td>
<td><p>吳碧姬</p></td>
</tr>
<tr class="even">
<td><p>6月13日-7月8日</p></td>
<td><p><a href="../Page/心慌·心郁·逐個捉.md" title="wikilink">心慌·心郁·逐個捉</a></p></td>
<td><p>紀賢淑</p></td>
</tr>
<tr class="odd">
<td><p>8月28日-10月6日</p></td>
<td><p><a href="../Page/鳳凰四重奏.md" title="wikilink">鳳凰四重奏</a></p></td>
<td><p>譚　卿</p></td>
</tr>
<tr class="even">
<td><p><strong>2007年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2月8日-2月17日</p></td>
<td><p><a href="../Page/紅衣手記.md" title="wikilink">紅衣手記</a></p></td>
<td><p>郁魯芬</p></td>
</tr>
<tr class="even">
<td><p>6月4日-7月13日</p></td>
<td><p><a href="../Page/學警出更.md" title="wikilink">學警出更</a></p></td>
<td><p>陳燕蘭</p></td>
</tr>
<tr class="odd">
<td><p>6月11日-7月6日</p></td>
<td><p><a href="../Page/緣來自有機.md" title="wikilink">緣來自有機</a></p></td>
<td><p>王淑娟</p></td>
</tr>
<tr class="even">
<td><p>7月9日-8月3日</p></td>
<td><p><a href="../Page/強劍.md" title="wikilink">強劍</a></p></td>
<td><p>成大娘</p></td>
</tr>
<tr class="odd">
<td><p>10月8日-11月24日</p></td>
<td><p><a href="../Page/通天干探.md" title="wikilink">通天干探</a></p></td>
<td><p>林泳梅</p></td>
</tr>
<tr class="even">
<td><p><strong>2008年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1月21日-2月15日</p></td>
<td><p><a href="../Page/秀才愛上兵.md" title="wikilink">秀才愛上兵</a></p></td>
<td><p>游玉蘭</p></td>
</tr>
<tr class="even">
<td><p><strong>2009年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5月4日-6月5日</p></td>
<td><p><a href="../Page/老婆大人II.md" title="wikilink">老婆大人II</a></p></td>
<td><p>王　官</p></td>
</tr>
<tr class="even">
<td><p><strong>2011年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2月21日-4月2日</p></td>
<td><p><a href="../Page/Only_You_只有您.md" title="wikilink">Only You 只有您</a></p></td>
<td><p>陳　太</p></td>
</tr>
<tr class="even">
<td><p>'''拍攝中</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/牛下女高音.md" title="wikilink">牛下女高音</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 海外电视剧

| 播出时间                     | 电视台                                        | 剧名                                 | 角色  | 备注 |
| ------------------------ | ------------------------------------------ | ---------------------------------- | --- | -- |
| 2017年10月6日 - 2017年12月29日 | [八度空间](../Page/八度空间_\(电视台\).md "wikilink") | [逆光成长](../Page/逆光成长.md "wikilink") | 苏嫦妹 |    |
|                          |                                            |                                    |     |    |

## 獎項

<table style="width:143%;">
<colgroup>
<col style="width: 75%" />
<col style="width: 21%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>作品</p></th>
<th><p>角色</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份不詳</p></td>
<td><p>香港戲劇協會</p></td>
<td><p>十年傑出成就獎</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>香港舞台劇界</p></td>
<td><p>專業精神大獎</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香港舞台劇獎.md" title="wikilink">香港舞台劇獎</a></p></td>
<td><p>最佳女主角（喜，鬧劇）</p></td>
<td><p>《<a href="../Page/橫衝直撞偷錯情.md" title="wikilink">橫衝直撞偷錯情</a>》</p></td>
<td><p>-</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香港舞台劇獎.md" title="wikilink">香港舞台劇獎</a></p></td>
<td><p>最佳女主角（悲，正劇）</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1988</p></td>
<td><p>香港藝術家年獎</p></td>
<td><p>舞台演員獎</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p><a href="../Page/國際青年商會香港總會.md" title="wikilink">國際青年商會香港總會</a></p></td>
<td><p><a href="../Page/香港十大傑出青年.md" title="wikilink">香港十大傑出青年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第31屆金馬獎.md" title="wikilink">第31屆金馬獎</a></p></td>
<td><p><a href="../Page/金馬獎獎項列表#.E6.9C.80.E4.BD.B3.E5.A5.B3.E9.85.8D.E8.A7.92.md" title="wikilink">最佳女配角</a></p></td>
<td><p>《<a href="../Page/記得香蕉成熟時Ⅱ之初戀情人.md" title="wikilink">記得香蕉成熟時Ⅱ之初戀情人</a>》</p></td>
<td><p>江普照</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香港舞台劇獎.md" title="wikilink">香港舞台劇獎</a></p></td>
<td><p>最佳女配角（悲，正劇）</p></td>
<td><p>《<a href="../Page/芭芭拉少校.md" title="wikilink">芭芭拉少校</a>》</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/1995年香港電影金像獎.md" title="wikilink">第14屆香港電影金像獎</a></p></td>
<td><p><a href="../Page/香港電影金像獎最佳女配角.md" title="wikilink">最佳女配角</a></p></td>
<td><p>《<a href="../Page/我和春天有個約會_(電影).md" title="wikilink">我和春天有個約會</a>》</p></td>
<td><p>金露露</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
<td><p><a href="../Page/1996年香港電影金像獎.md" title="wikilink">第15屆香港電影金像獎</a></p></td>
<td><p><a href="../Page/香港電影金像獎最佳女配角.md" title="wikilink">最佳女配角</a></p></td>
<td><p>《<a href="../Page/女人四十.md" title="wikilink">女人四十</a>》</p></td>
<td><p>姨姨</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第32屆金馬獎.md" title="wikilink">第32屆金馬獎</a></p></td>
<td><p><a href="../Page/金馬獎獎項列表#.E6.9C.80.E4.BD.B3.E5.A5.B3.E9.85.8D.E8.A7.92.md" title="wikilink">最佳女配角</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1996年香港電影金像獎.md" title="wikilink">第15屆香港電影金像獎</a></p></td>
<td><p><a href="../Page/香港電影金像獎最佳女配角.md" title="wikilink">最佳女配角</a></p></td>
<td><p>《<a href="../Page/人間有情.md" title="wikilink">人間有情</a>》</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p><a href="../Page/1997年香港電影金像獎.md" title="wikilink">第16屆香港電影金像獎</a></p></td>
<td><p><a href="../Page/香港電影金像獎最佳女配角.md" title="wikilink">最佳女配角</a></p></td>
<td><p>《<a href="../Page/港督最后一個保鑣.md" title="wikilink">港督最后一個保鑣</a>》</p></td>
<td><p>鍾方安心</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/2003年度萬千星輝賀台慶.md" title="wikilink">萬千星輝頒獎典禮2003</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《<a href="../Page/天子尋龍.md" title="wikilink">天子尋龍</a>》</p></td>
<td><p>武則天</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/智勇新警界.md" title="wikilink">智勇新警界</a>》</p></td>
<td><p>方宜嬌</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/2004年度萬千星輝賀台慶.md" title="wikilink">萬千星輝頒獎典禮2004</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《<a href="../Page/隔世追兇.md" title="wikilink">隔世追兇</a>》</p></td>
<td><p>梁妙蘭</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/2005年度萬千星輝賀台慶.md" title="wikilink">萬千星輝頒獎典禮2005</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《<a href="../Page/水滸無間道.md" title="wikilink">水滸無間道</a>》</p></td>
<td><p>萬麗冰</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/學警雄心.md" title="wikilink">學警雄心</a>》</p></td>
<td><p>陳燕蘭</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/阿旺新傳.md" title="wikilink">阿旺新傳</a>》</p></td>
<td><p>何麗莎</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p><a href="../Page/聖地牙哥獨立電影節_The_IndieFEST_Film_Awards.md" title="wikilink">聖地牙哥獨立電影節 The IndieFEST Film Awards</a></p></td>
<td><p><a href="../Page/最佳女主角.md" title="wikilink">最佳女主角</a></p></td>
<td><p>《<a href="../Page/寵我.md" title="wikilink">寵我</a>》</p></td>
<td><p>媽咪</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/景琛國際電影節_Depth_of_Field_International_Film_Festival.md" title="wikilink">景琛國際電影節 Depth of Field International Film Festival</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界電影大獎_World_Film_Awards.md" title="wikilink">世界電影大獎 World Film Awards</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洛杉磯獨立電影節大獎_Los_Angeles_Independent_Film_Festival_Awards.md" title="wikilink">洛杉磯獨立電影節大獎 Los Angeles Independent Film Festival Awards</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/峇里電影大獎_Bali_Film_Awards.md" title="wikilink">峇里電影大獎 Bali Film Awards</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛爾蘭Elevation獨立電影大獎_Elevation_Indie_Film_Awards.md" title="wikilink">愛爾蘭Elevation獨立電影大獎 Elevation Indie Film Awards</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奧克蘭國際電影節_Auckland_International_Film_Festival.md" title="wikilink">奧克蘭國際電影節 Auckland International Film Festival</a></p></td>
<td><p>最佳女配角</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考

## 外部連結

  - [舞台天后羅冠蘭](http://club.yule.sohu.com/yulemain.php?c=95&b=old&a=52497)

[Category:香港十大傑出青年](../Category/香港十大傑出青年.md "wikilink")
[Koon](../Category/羅姓.md "wikilink")
[Category:20世紀演員](../Category/20世紀演員.md "wikilink")
[Category:21世紀演員](../Category/21世紀演員.md "wikilink")
[Category:香港舞臺劇演員](../Category/香港舞臺劇演員.md "wikilink")
[Category:香港電視女演員](../Category/香港電視女演員.md "wikilink")
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:無綫電視女藝員](../Category/無綫電視女藝員.md "wikilink")
[Category:無綫電視藝員訓練班](../Category/無綫電視藝員訓練班.md "wikilink")
[Category:香港甘草演員](../Category/香港甘草演員.md "wikilink")
[Category:香港戲劇導演](../Category/香港戲劇導演.md "wikilink")
[Category:聖公會諸聖中學校友](../Category/聖公會諸聖中學校友.md "wikilink")
[Category:香港浸會大學校友](../Category/香港浸會大學校友.md "wikilink")
[Category:香港大學專業進修學院校友](../Category/香港大學專業進修學院校友.md "wikilink")
[Category:米德爾塞克斯大學校友](../Category/米德爾塞克斯大學校友.md "wikilink")

1.  [聖公會諸聖中學60周年校慶特刊，2012年](http://library.skhasms.edu.hk/11-12SM/saint_60th.pdf)