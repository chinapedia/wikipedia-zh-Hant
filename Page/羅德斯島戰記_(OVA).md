《**羅德斯島戰記**》[OVA版](../Page/OVA.md "wikilink")，是在1990年至1991年間由[角川書店](../Page/角川書店.md "wikilink")、[丸紅](../Page/丸紅.md "wikilink")、[東京放送](../Page/東京放送.md "wikilink")、[角川
Media
Office所聯合製作](../Page/MediaWorks.md "wikilink")，並在2006年推出[復刻版](../Page/復刻版.md "wikilink")[DVD](../Page/DVD.md "wikilink")。全篇共13集，其中前8集的内容取材自[水野良](../Page/水野良.md "wikilink")《[羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink")》第一卷《灰色的魔女》，後5集取材自《羅德斯島戰記》第三卷《火龍山的魔龍》。

## 工作人員

  -
    製作總指揮：[角川歷彥](../Page/角川歷彥.md "wikilink")
    原作：水野良、安田均
    企劃：田宮武
    角色設定：[結城信輝](../Page/結城信輝.md "wikilink")
    音樂：荻田光雄
    系列構成：渡邊麻實

## 聲優

  -
    帕恩：[草尾毅](../Page/草尾毅.md "wikilink")
    蒂德莉特：[冬馬由美](../Page/冬馬由美.md "wikilink")
    基姆：[坂口芳貞](../Page/坂口芳貞.md "wikilink")
    埃德：[山口勝平](../Page/山口勝平.md "wikilink")
    史連：[田中秀幸](../Page/田中秀幸.md "wikilink")
    伍德傑克：[若本規夫](../Page/若本規夫.md "wikilink")
    渥德：[大木民夫](../Page/大木民夫.md "wikilink")
    法恩國王：[阪脩](../Page/阪脩.md "wikilink")
    貝魯特皇帝：[石田太郎](../Page/石田太郎.md "wikilink")
    卡修國王：[池田秀一](../Page/池田秀一.md "wikilink")
    阿修拉姆：[神谷明](../Page/神谷明.md "wikilink")
    巴格納德：[青野武](../Page/青野武.md "wikilink")
    琵洛提絲：[玉川紗己子](../Page/玉川紗己子.md "wikilink")
    卡拉：[榊原良子](../Page/榊原良子.md "wikilink")
    希莉絲：[高山南](../Page/高山南.md "wikilink")
    歐爾森：[速水獎](../Page/速水獎.md "wikilink")
    旁白：[永井一郎](../Page/永井一郎.md "wikilink")

## 音樂

  - 片頭曲：「Adesso e Fortuna ～ 炎與永遠」

<!-- end list -->

  -
    作詞：[新居昭乃](../Page/新居昭乃.md "wikilink")
    作曲：新居昭乃
    編曲：荻田光雄
    主唱：Sherry ([加藤泉](../Page/加藤泉.md "wikilink"))

<!-- end list -->

  - 片尾曲：「風のファンタジア」

<!-- end list -->

  -
    作詞：伊藤薰
    作曲：伊藤薰
    編曲：荻田光雄
    主唱：Sherry ([加藤泉](../Page/加藤泉.md "wikilink"))

<!-- end list -->

  - 插曲：「風と鳥と空」

<!-- end list -->

  -
    作詞：新居昭乃
    作曲：新居昭乃
    編曲：荻田光雄
    主唱：新居昭乃

## 各篇標題

  -
    第01話：通往傳説的序章
    第02話：火炎的出發
    第03話：黒衣的騎士
    第04話：灰色的魔女
    第05話：砂漠的國王
    第06話：暗黑王的劍
    第07話：英雄戰爭
    第08話：戰士的鎮魂歌
    第09話：支配的王錫
    第10話：火龍山的魔龍
    第11話：魔導師的野心
    第12話：決戰！暗黒之島
    第13話：灼熱的大地

## 各地播映期間

  - [中國電視公司](../Page/中國電視公司.md "wikilink")：1997年2月2日至1997年4月27日在[台灣首播](../Page/台灣.md "wikilink")。

## 相關條目

  - [羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink")

[category:1990年日本OVA動畫](../Page/category:1990年日本OVA動畫.md "wikilink")

[Category:TBS動畫](../Category/TBS動畫.md "wikilink")
[Category:中視外購動畫](../Category/中視外購動畫.md "wikilink")
[Category:羅德斯島戰記](../Category/羅德斯島戰記.md "wikilink")