**羅理基博士大馬路**
（****）是一條位於[澳門半島](../Page/澳門半島.md "wikilink")[新口岸北部邊緣的道路](../Page/新口岸.md "wikilink")，依[東望洋山山勢而建](../Page/東望洋山.md "wikilink")。西起葡京路、賈羅布大馬路、嘉思欄馬路之交界，往東北方至[回力球場後右轉至](../Page/回力球場_\(澳門\).md "wikilink")[友誼大馬路止](../Page/友誼大馬路.md "wikilink")，全長1470米。

以第109任[澳門總督](../Page/澳門總督.md "wikilink")[羅理基命名](../Page/羅理基.md "wikilink")。周圍早期是菜地。東西兩則在1980年代開始發展。中間路段南側建成了[何賢公園](../Page/何賢公園.md "wikilink")
（中間為何賢公園迴旋處，下有行車隧道，上有[中葡友好紀念物](../Page/中葡友好紀念物.md "wikilink")「東方拱門」）。[松山隧道與](../Page/松山隧道.md "wikilink")[南光大廈之間的土地一直未有開發](../Page/南光大廈.md "wikilink")。惟2006年政府廢除原有的對東望洋山周圍建築高度的限制，觸發了社會對政府保育政策緩慢的不滿。

## 重要地點

[Arco_Oriente.jpg](https://zh.wikipedia.org/wiki/File:Arco_Oriente.jpg "fig:Arco_Oriente.jpg")
[MacauPawnShops2.jpg](https://zh.wikipedia.org/wiki/File:MacauPawnShops2.jpg "fig:MacauPawnShops2.jpg")

### 北面 （西至東）

  - [澳門培道中學](../Page/澳門培道中學.md "wikilink")
  - 南光大廈 （[澳門電台和澳門中國旅行社所在](../Page/澳門電台.md "wikilink")）
  - [聖羅撒英文中學](../Page/聖羅撒女子中學.md "wikilink")
  - [中央人民政府駐澳門特別行政區聯絡辦公室](../Page/中央人民政府駐澳門特別行政區聯絡辦公室.md "wikilink")
  - [松山隧道出口行車天橋](../Page/松山隧道.md "wikilink")

### 南面 （西至東）

  - 富豪酒店
  - [治安警察局](../Page/治安警察局.md "wikilink")
  - [澳門中華總商會大樓](../Page/澳門中華總商會.md "wikilink")
  - 龍成大廈
  - 駐澳解放軍大廈
  - [澳門理工學院](../Page/澳門理工學院.md "wikilink")
  - [澳門綜藝館](../Page/澳門綜藝館.md "wikilink")
  - 外交部駐澳公署
  - [皇家金堡酒店](../Page/皇家金堡酒店.md "wikilink")
  - 回力球場

## 街景

<center>

{{ gallery | width = 230 | <File:Avenida> Dr. Rodrigo Rodrigues
01.jpg|鄰近[葡京酒店](../Page/葡京酒店.md "wikilink") | <File:Avenida> Dr.
Rodrigo Rodrigues 02.jpg|中聯辦門外 }}

</center>

[Category:澳門街道](../Category/澳門街道.md "wikilink")
[Category:冠以人名的澳門道路](../Category/冠以人名的澳門道路.md "wikilink")