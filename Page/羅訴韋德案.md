**罗诉韦德案**（），又译为**露對威德案**，是[美國聯邦最高法院於](../Page/美國聯邦最高法院.md "wikilink")1973年對於婦女墮胎權以及[隱私權的重要案例](../Page/隱私權.md "wikilink")，對於婦女墮胎的問題，美國聯邦最高法院承認婦女的墮胎權，受到憲法隱私權的保護。此外最高法院對墮胎權的限制應採取嚴格審查標準，並提出「三階段標準」。最高法院的判決至今仍受到美國的社會爭議，部分[反墮胎團體一直爭取推翻判決](../Page/反墮胎.md "wikilink")，而支持墮胎權的人士則要求維持最高法院的判決。判決後各州均制定不同的法律，惟限制不一。

## 事實

1969年8月，美國德州的女服務生Norma
McCorvey意外怀孕想[堕胎](../Page/堕胎.md "wikilink")，她的朋友建议她謊稱遭到[強暴](../Page/強姦.md "wikilink")，以合法堕胎，因为德州法律規定被性侵可以合法墮胎。然而因为没有警方報告證明其遭到性侵，所以这个办法没有成功。于是，她去了一家地下堕胎诊所，但发现该诊所已经被警察查封。

1970年，律师Linda Coffee和Sarah Weddington为McCorvey（化名珍妮·羅，Jane
Roe），起诉代表德州的[达拉斯县司法长官亨利](../Page/达拉斯.md "wikilink")·韋德，指控德州禁止墮胎的法律，侵犯了她的「隱私權」。地方法院判決，該法侵犯了原告，受[美國憲法第九修正案所保障的權利](../Page/美國憲法第九修正案.md "wikilink")，但是沒有對德州的反墮胎法律提出[禁制令](../Page/禁制令.md "wikilink")（injunction），Roe向美國聯邦最高法院[上訴](../Page/上訴.md "wikilink")。

1973年1月22日，聯邦最高法院於1973年以7比2的比數，認定德州刑法限制婦女墮胎權的規定，違反[美國憲法第十四修正案](../Page/美國憲法第十四修正案.md "wikilink")「[正當法律程序](../Page/正當法律程序.md "wikilink")」條款。

## 爭議

  - 婦女「墮胎權」是否為「隱私權」所保障? 隱私權的憲法基礎條文為何?
  - 未出生的[胎兒](../Page/胎兒.md "wikilink")，是否受憲法第14條修正案「正當法律程序」條款保障?
  - 德州禁止墮胎法律，是否違反憲法第14條修正案「正當法律程序」條款?

## 理由

隱私權的保護範圍，包含婦女的墮胎權。不過值得注意的是，法院並未指出隱私權的憲法根據為何。

憲法上的人應該僅適用於[出生之後](../Page/出生.md "wikilink")，至於未出生胎兒（unborn
child），並非憲法增修條文第14條中所稱的「人」(person)。

法院對婦女墮胎權的保護，採取「嚴格審查」(Strict Scrutiny)
標準，州可以主張，基於保障懷孕婦女的身體健康、維持醫療標準、以及對於未出生胎兒的生命權等具有「重要利益」（important
interest），當此種利益已達「不可抗拒利益」(compelling interest)
的程度時，州可以[立法的方式](../Page/立法.md "wikilink")，對於婦女的墮胎權作一定的限制。

法院提出「三階段標準」，認為在婦女的懷孕期間，可以分為三個階段(trimester)。在懷孕前三個月(第1到第12周)，由於胎兒不具有「母體外存活性」
(viability)，所以孕婦可在與醫生討論之後自行決定是否墮胎；懷孕三個月後，政府得限制墮胎，但是只限以保護孕婦健康為必要；在胎兒具有母體外存活性(第24到28周)之後，政府保護潛在生命的利益，達到了不可抗拒利益的程度，除非母親的生命或健康遭遇危險，否則政府禁止墮胎。

德州禁止墮胎的法規，並未因婦女懷孕期間的不同，而訂定不同的審查標準，違反憲法增修條文第14條「[正當法律程序](../Page/正當法律程序.md "wikilink")」的規定。

## 結論

原判決部分維持，部分廢棄。美國各州議會在反墮胎團體的推動下，通過各種在最高法院判決下限制墮胎的法律。

## 參見

  - [岡薩雷斯訴卡哈特案](../Page/岡薩雷斯訴卡哈特案.md "wikilink")

  -
## 参考文献

  - 屠振宇
    著：《[從墮胎案件看美國司法審查標準](https://web.archive.org/web/20080423190219/http://www.jcrb.com/200802/ca680632.htm)》，中國民商法律網，2008年.
  - 陳愛娥 著：《憲法對未出生胎兒的保護》，《政大法學論》第58期，頁67，1997年12月.

[Category:美國最高法院案例](../Category/美國最高法院案例.md "wikilink")
[Category:美國人權](../Category/美國人權.md "wikilink")
[Category:得克萨斯州历史](../Category/得克萨斯州历史.md "wikilink")
[Category:1973年美国](../Category/1973年美国.md "wikilink")
[Category:美国堕胎](../Category/美国堕胎.md "wikilink")