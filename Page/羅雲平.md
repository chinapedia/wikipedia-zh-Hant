**羅雲平**，()，生於[安東省](../Page/安東省.md "wikilink")[鳳城](../Page/鳳城.md "wikilink")，工科教授、[中華民國政治人物](../Page/中華民國.md "wikilink")、教育家。

## 大事紀

[哈爾濱工業大學畢業後前往](../Page/哈爾濱工業大學.md "wikilink")[德國](../Page/德國.md "wikilink")[漢諾威高等工科大學取得工學博士](../Page/漢諾威.md "wikilink")

  - 1939年9月回到[中國](../Page/中國.md "wikilink")，在上海[同濟大學做工學院教授](../Page/同濟大學.md "wikilink")
  - 1944年轉往[國立中央大學工學院](../Page/國立中央大學工學院.md "wikilink")
  - 1946年出任教育部[東北區教育輔導委員會校院接收委員](../Page/中國東北地區.md "wikilink")，9月任[中國長春鐵路總管局技術室正工程師兼技術室主任](../Page/中國長春鐵路.md "wikilink")
  - 1948年4月任東北政務委員會文化處處長，5月出任[長春大學校長](../Page/長春大學.md "wikilink")
  - 1949年5月到台灣，在[台南市台灣省立工學院](../Page/台南市.md "wikilink")（今[國立成功大學](../Page/國立成功大學.md "wikilink")）擔任教授
  - 1956年8月出任成功大學教授兼工學院院長
  - 1959年1月轉任[教育部高等教育司司長](../Page/教育部.md "wikilink")
  - 1964年1月接任國立成功大學校長
  - 1971年3月出任[中華民國教育部部長](../Page/中華民國教育部.md "wikilink")
  - 1972年5月卸任後改為擔任[國立中興大學校長](../Page/國立中興大學.md "wikilink")。
  - 1981年退休，受聘[行政院顧問](../Page/行政院.md "wikilink")。

## 外部連結

  - [教育部部史－歷屆部長 羅雲平](http://history.moe.gov.tw/minister.asp?id=75)
  - [國立中興大學校史館-興湖紀事](https://web.archive.org/web/20121105025400/http://archive.nchu.edu.tw/joomla/index.php/zh)

|- |colspan="3"
style="text-align:center;"|**[國立成功大學](../Page/國立成功大學.md "wikilink")**
|-    |- |colspan="3"
style="text-align:center;"|**[National_Chung_Hsing_University.svg](https://zh.wikipedia.org/wiki/File:National_Chung_Hsing_University.svg "fig:National_Chung_Hsing_University.svg")[國立中興大學](../Page/國立中興大學.md "wikilink")**
|-     |- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")
[行政院](../Page/行政院.md "wikilink")** |-

[category:中華民國教育部部長](../Page/category:中華民國教育部部長.md "wikilink")

[Category:中國教育家](../Category/中國教育家.md "wikilink")
[Category:臺灣教育工作者](../Category/臺灣教育工作者.md "wikilink")
[Category:國立成功大學校長](../Category/國立成功大學校長.md "wikilink")
[Category:國立中興大學校長](../Category/國立中興大學校長.md "wikilink")
[工](../Category/國立中央大學教授.md "wikilink")
[Category:南京大學教席學者](../Category/南京大學教席學者.md "wikilink")
[Category:同济大学教授](../Category/同济大学教授.md "wikilink")
[Category:丹東人](../Category/丹東人.md "wikilink")
[Category:台灣戰後東北移民](../Category/台灣戰後東北移民.md "wikilink")
[Category:羅姓](../Category/羅姓.md "wikilink")