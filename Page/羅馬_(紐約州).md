**羅馬**（Rome, New
York）是[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[奧奈達縣的一座城市](../Page/奧奈達縣_\(紐約州\).md "wikilink")，2000年人口34,950人。

地名來自於[義大利的首都](../Page/義大利.md "wikilink")[羅馬](../Page/羅馬.md "wikilink")。

## 外部链接

  - [官方網頁](http://romenewyork.com)

[R](../Category/紐約州城市.md "wikilink")