《**罗马假日**》（）是1953年由[派拉蒙公司拍摄的浪漫爱情片](../Page/派拉蒙公司.md "wikilink")，由[道爾頓·莊柏编剧](../Page/道爾頓·莊柏.md "wikilink")，[威廉·惠勒导演](../Page/威廉·惠勒.md "wikilink")。故事讲述了一位[欧洲某](../Page/欧洲.md "wikilink")[公国的公主与一个](../Page/公国.md "wikilink")[美国](../Page/美国.md "wikilink")[记者之间在](../Page/记者.md "wikilink")[意大利](../Page/意大利.md "wikilink")[罗马一天之内发生的浪漫故事](../Page/罗马.md "wikilink")。罗马的[特萊維噴泉](../Page/特萊維噴泉.md "wikilink")（Fontana
di Trevi）、[西班牙廣場](../Page/西班牙廣場.md "wikilink")（Piazza di
Spagna）、[真理之口與](../Page/真理之口.md "wikilink")[台伯河等著名景点均有在影片中出现](../Page/台伯河.md "wikilink")。

本片获得了当年[奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")（[奥黛丽·赫本](../Page/奥黛丽·赫本.md "wikilink")）、[最佳服装设计奖（黑白片）](../Page/奥斯卡最佳服装设计奖.md "wikilink")（伊蒂絲·海德，
Edith
Head）、[最佳剧本奖（劇情片）奖项](../Page/奥斯卡最佳剧本奖.md "wikilink")。同时也获得了[最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")（艾迪·亞伯特，Eddie
Albert）、[最佳艺术指导奖——布景装置（黑白片）](../Page/奥斯卡最佳艺术指导奖.md "wikilink")、[最佳摄影奖（黑白片）](../Page/奥斯卡最佳摄影奖.md "wikilink")、[最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")、[最佳剪接奖与](../Page/奥斯卡最佳剪接奖.md "wikilink")[最佳影片奖的提名](../Page/奥斯卡最佳影片奖.md "wikilink")。

在1970年代，很多人建议拍摄公主与记者重逢的续集，然而，这个建议始终没有实现。原剧则在1980年代早期被改编成电视剧播出。1999年被[美国国会图书馆典藏](../Page/美国国会图书馆.md "wikilink")。该片是[奥黛丽·赫本擔綱女主角的第一部电影](../Page/奥黛丽·赫本.md "wikilink")，她也因此片获得[奥斯卡金像奖最佳女主角奖](../Page/奥斯卡金像奖.md "wikilink")，[金球奖最佳女主角奖](../Page/金球奖.md "wikilink")。

## 剧情

[Gregory_Peck,_Audrey_Hepburn_and_Eddie_Albert_in_Roman_Holiday_trailer.jpg](https://zh.wikipedia.org/wiki/File:Gregory_Peck,_Audrey_Hepburn_and_Eddie_Albert_in_Roman_Holiday_trailer.jpg "fig:Gregory_Peck,_Audrey_Hepburn_and_Eddie_Albert_in_Roman_Holiday_trailer.jpg")

安（[奥黛丽·赫本饰](../Page/奥黛丽·赫本.md "wikilink")）是一位[皇室的](../Page/皇室.md "wikilink")[公主](../Page/公主.md "wikilink")（片中並没有提及具体国家）。她正在对欧洲的一些首都进行公开的访问，包括[罗马](../Page/罗马.md "wikilink")。一天夜晚，她对她第二天将要进行的非常紧张的公务感到难以承受而变得[情绪失控](../Page/情绪失控.md "wikilink")。她的医生给她注射了一针[镇静剂来帮助睡眠](../Page/镇静剂.md "wikilink")，但是公主悄悄地离开了她所在的[大使馆](../Page/大使馆.md "wikilink")，希望自己来体验真正的[罗马](../Page/罗马.md "wikilink")。

不久，镇静剂开始发挥效力，公主在路边的一张长椅上睡着了，并被一位[美国](../Page/美国.md "wikilink")[记者乔伊](../Page/记者.md "wikilink")·布莱德利所发现。由于双方并不认识，他给了她一些钱让她坐[的士回家](../Page/的士.md "wikilink")。但是非常睏倦的「安雅·史密斯」（她本人如此自稱）拒绝合作。最终，出于安全考虑，乔伊决定让她在自己的[公寓度过一晚](../Page/公寓.md "wikilink")。一开始，他对她王室一般的举止感到非常好笑，但是当发现她占据了自己的床铺，乔伊感到很不高兴。他没有叫醒她就直接把她移到一把躺椅上。第二天早上，乔伊发现自己起晚了。慌忙中他冲出门去上班，没有顾及她。

当他的[主编韓納西責问他为什么遲到](../Page/主编.md "wikilink")，乔伊撒了谎。他声称自己已经参加过了为公主召开的[记者见面会](../Page/记者见面会.md "wikilink")。当乔伊开始有头有脸地编造所谓的见面会上的细节时，韓納西告诉他根本就没有什么记者见面会，因为公主突然「生病」了，原来准备进行的见面会也告取消。乔伊看到了一张公主的照片，并认出了她就是昨天晚上自己遇到的那个年轻女子。最后，乔伊和韓納西打赌说他能够写出一篇有关公主的独家报道，以逃过主编的斥责。

乔伊意识到著名的公主在他的房间中，他仿佛感到自己面对的是一笔飞来横财。他小心地隐藏自己媒体记者的身份，主动提出带安雅在罗马参观。后来当他遇到自己的朋友，照相师歐文·拉多維奇时，事情变得更加顺利，因为他的朋友对暗中拍摄的本领掌握得非常娴熟。这时安雅却不愿意让乔伊陪同自己而独自离开。

享受着这样的自由（平时她总是有永远没有尽头的公务与访问），安雅心血来潮地在一家[理发店把她的头发剪短](../Page/理发店.md "wikilink")。乔伊悄悄地跟着她，并且「意外地」再次遇到她。他们用了一天时间在罗马周游，去了很多景点。在[真理之口](../Page/真理之口.md "wikilink")，他们看到了一个据说能够咬下说谎者的手指的石雕。当乔伊把他的手拿出来时，他把手藏在了袖子里使之看起来像是被咬掉了。当安雅大声惊叫的时候，他却把手重新从袖子中拿出了并大笑。后来，安雅告诉乔伊她一直以来的一个梦想，那就是做一个普通的女孩，过一种常人的生活，而不需要承当作为公主那么深重的责任。那天晚上，当两个人在一只小船上跳舞时，八名特工发现了公主，想强行将公主带走。但在一阵混乱中，乔伊和安雅巧妙地逃脱。在这短短的一天里，两个人逐渐坠入爱河，但安雅知道双方的关系是不可能持续的。在极度的痛苦中，她和乔伊道别，并且回到了大使馆。自始至终，她都没有主动说出自己真实的身份。

在这天所发生的一連串事件中，韓納西逐渐了解到公主并不是像大使馆声称的那样生病了，而是[失踪](../Page/失踪.md "wikilink")。想到乔伊和他打的那个赌，他立刻怀疑乔伊知道公主在哪里，并且试图让乔伊说出来，但是乔伊始终说自己对此一无所知。照顾到乔伊对公主的感情，歐文最终勉强同意不再出售自己所暗中拍摄的公主照片。

第二天，安公主出现在了推迟举行的新闻发布会上，并且对乔伊和歐文的出现感到惊讶。歐文用打火机状的袖珍照相机拍了一张照片，而这正是他在前一天秘密拍摄公主照片的方法。歐文将这些照片还给了公主，當作「公主在罗马的回忆照片」。乔伊间接地让公主知道，她的秘密将永远不会外洩。而公主，当回答记者的提问：「公主殿下旅途中最欣赏哪一个城市？」的问题时，在一串外交辞令之后，终于流露出自己的真情：

"Rome, by all means, Rome. I will cherish my visit here in memory, as
long as I live\! "

「罗马，当然是罗马。我会用我的一生来珍藏在这座城市里度过的每一分钟。」

新闻发布会结束了，公主离开了大厅。只有乔伊默默地站着，深沉地守著會目送公主離開的約定。

## 角色

[缩略图](https://zh.wikipedia.org/wiki/File:Audrey_Hepburn_and_Gregory_Peck_on_Vespa_in_Roman_Holiday_trailer.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Audrey_Hepburn_in_Roman_Holiday_trailer.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Gregory_Peck_in_Roman_Holiday_trailer.jpg "fig:缩略图")

  - [格里高利·派克](../Page/格里高利·派克.md "wikilink")- Joe Bradley
  - [奧黛麗·赫本](../Page/奧黛麗·赫本.md "wikilink") - Princess Ann
  - Eddie Albert - Irving Radovich
  - Hartley Power - Hennessy, 报社主编
  - Harcourt Williams - 大使
  - Margaret Rawlings - Countess Vereberg, 侍女
  - Tullio Carminati - General Provno
  - Paolo Carlini - Mario Delani
  - Claudio Ermelli - Giovanni
  - Paola Borboni - the Charwoman
  - Laura Solari - Secretary

## 荣誉

  - 获奖

<!-- end list -->

  - 奧斯卡最佳女主角
  - 奥斯卡最佳服裝設計（黑白片）
  - [英国电影学院奖最佳英国女演员](../Page/英国电影学院奖.md "wikilink")
  - [金球奖最佳剧情片女主角](../Page/金球奖.md "wikilink")
  - [纽约影评人协会最佳女主角](../Page/纽约影评人协会.md "wikilink")
  - [美国编剧工会奖最佳美国喜剧片剧本](../Page/美国编剧工会奖.md "wikilink")

<!-- end list -->

  - 美国电影学会

<!-- end list -->

  - [AFI百年百大爱情电影](../Page/AFI百年百大爱情电影.md "wikilink")：第四位
  - [AFI十大类型十大佳片](../Page/AFI十大类型十大佳片.md "wikilink")：浪漫喜剧片第四位

## 外部链接

  -
  -
  -
  -
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1953年电影](../Category/1953年电影.md "wikilink")
[Category:黑白電影](../Category/黑白電影.md "wikilink")
[Category:美國浪漫喜劇片](../Category/美國浪漫喜劇片.md "wikilink")
[Category:公主主角題材電影](../Category/公主主角題材電影.md "wikilink")
[Category:威廉·惠勒电影](../Category/威廉·惠勒电影.md "wikilink")
[Category:羅馬背景電影](../Category/羅馬背景電影.md "wikilink")
[Category:羅馬取景電影](../Category/羅馬取景電影.md "wikilink")
[Category:倫敦取景電影](../Category/倫敦取景電影.md "wikilink")
[Category:身分差異戀情題材電影](../Category/身分差異戀情題材電影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")
[Category:奧斯卡最佳女主角獲獎電影](../Category/奧斯卡最佳女主角獲獎電影.md "wikilink")
[Category:奥斯卡最佳服装设计奖获奖电影](../Category/奥斯卡最佳服装设计奖获奖电影.md "wikilink")
[Category:金球獎最佳戲劇類女主角獲獎電影](../Category/金球獎最佳戲劇類女主角獲獎電影.md "wikilink")