{{ 足球聯賽資料 | name = 羅馬尼亞足球甲級聯賽 | another_name = Liga I | image = |
pixels = | country =  | confed =
[UEFA](../Page/歐洲足球協會聯盟.md "wikilink")（[歐洲](../Page/歐洲.md "wikilink")）
| first = 1909年 | teams = 14 隊 | relegation =
[羅馬尼亞足球乙級聯賽](../Page/羅馬尼亞足球乙級聯賽.md "wikilink")
| level = 第 1 級 | domestic_cup = [羅馬尼亞盃](../Page/羅馬尼亞盃.md "wikilink")
[羅馬尼亞超級盃](../Page/羅馬尼亞超級盃.md "wikilink") | international_cup =
[歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")
[歐霸盃](../Page/歐足聯歐洲聯賽.md "wikilink") | season = 2017–18 | champions =
[克盧日](../Page/CFR克盧日足球俱樂部.md "wikilink") | most_successful_club =
[布加勒斯特星隊](../Page/布加勒斯特星足球俱樂部.md "wikilink") | no_of_titles = 26 次 |
website = [羅甲官網](http://www.lpf.ro/) | current = }}

**羅馬尼亞足球甲級聯賽**是[羅馬尼亞足球協會](../Page/羅馬尼亞足球協會.md "wikilink")（LPF）组织的职业[足球联赛](../Page/足球.md "wikilink")，通常简称「**羅甲**」，是羅馬尼亞足球賽事中最高水平者。

羅馬尼亞聯賽肇始於1909年，其間經過多次改組，才變成現時的規模。目前羅馬尼亞甲組聯賽隊伍共有 18
支，聯賽大抵是每一支球會跟另外十七隊進行主場、作客的比賽。聯賽成績以積分計算，最高積分者會是冠軍；而成績最差四隊則要降級。由2005–06年球季開始，聯賽冠、亞軍會取得參與[歐洲聯賽冠軍杯資格](../Page/歐洲聯賽冠軍杯.md "wikilink")，而第三名則有參與[歐洲足協盃資格](../Page/歐洲足協盃.md "wikilink")。

羅甲聯賽基本上是由兩支球會所壟斷──[布加勒斯特戴拿模和](../Page/布加勒斯特迪納摩足球俱樂部.md "wikilink")[布加勒斯特星隊](../Page/布加勒斯特星足球俱樂部.md "wikilink")。近年兩大俱樂部以外的勢力也逐漸興起。在[羅馬尼亞共產主義時代](../Page/羅馬尼亞社會主義共和國.md "wikilink")，迪納摩受到[秘密警察的支持](../Page/秘密警察.md "wikilink")；而星隊則有軍隊的支持。其中[布加勒斯特星隊取得過](../Page/布加勒斯特星足球俱樂部.md "wikilink")
23
次聯賽冠軍，乃近代羅甲拿得最多冠軍的球隊。而[布加勒斯特戴拿模也獲得過](../Page/布加勒斯特迪納摩足球俱樂部.md "wikilink")
18 次聯賽錦標。而2010–11賽季的聯賽冠軍是[加拉治](../Page/加拉茨足球俱樂部.md "wikilink")。

## 參賽球隊

2018–19年羅馬尼亞足球甲級聯賽參賽隊伍共有 14 支。

| 中文名稱                                            | 英文名稱                          | 所在城市                                     | 上季成績     |
| ----------------------------------------------- | ----------------------------- | ---------------------------------------- | -------- |
| [克盧日](../Page/CFR克盧日足球俱樂部.md "wikilink")        | CFR Cluj                      | [克盧日-納波卡](../Page/克盧日-納波卡.md "wikilink") | 第 1 位    |
| [布加勒斯特星隊](../Page/布加勒斯特星足球俱樂部.md "wikilink")    | FC Steaua Bucuresti           | [布加勒斯特](../Page/布加勒斯特.md "wikilink")     | 第 2 位    |
| [卡拉奧華大學](../Page/卡拉奧華大學體育會.md "wikilink")       | CS Universitatea Craiova      | [克拉約瓦](../Page/克拉約瓦.md "wikilink")       | 第 3 位    |
| [維托魯干斯坦達](../Page/康斯坦察維托足球俱樂部.md "wikilink")    | FC Viitorul Constanta         | [康斯坦察](../Page/康斯坦察.md "wikilink")       | 第 4 位    |
| [艾斯特拉](../Page/阿斯特拉足球俱樂部.md "wikilink")         | FC Astra Giurgiu              | [久爾久](../Page/久爾久.md "wikilink")         | 第 5 位    |
| [CSMP拉斯](../Page/拉斯理工體育會.md "wikilink")         | CSM Politehnica Iași          | [雅西](../Page/雅西.md "wikilink")           | 第 6 位    |
| [布加勒斯特戴拿模](../Page/布加勒斯特迪納摩足球俱樂部.md "wikilink") | FC Dinamo Bucuresti           | [布加勒斯特](../Page/布加勒斯特.md "wikilink")     | 第 7 位    |
| [博托沙尼](../Page/博托沙尼足球俱樂部.md "wikilink")         | FC Botosani                   | [博托沙尼](../Page/博托沙尼.md "wikilink")       | 第 8 位    |
| [施佩斯](../Page/施佩斯體育會.md "wikilink")             | ACS Sepsi OSK Sfântu Gheorghe | [聖格奧爾基](../Page/聖格奧爾基.md "wikilink")     | 第 9 位    |
| [加斯梅登](../Page/加斯梅登體育會.md "wikilink")           | CS Gaz Metan Mediaș           | [梅迪亞什](../Page/梅迪亞什.md "wikilink")       | 第 10 位   |
| [康戈迪亞齊安](../Page/基亞日納足球俱樂部.md "wikilink")       | CS Concordia Chiajna          | [基亞日納](../Page/基亞日納.md "wikilink")       | 第 11 位   |
| [禾倫達利](../Page/禾倫達利足球會.md "wikilink")           | FC Voluntari                  | [沃倫塔里](../Page/沃倫塔里.md "wikilink")       | 第 12 位   |
| [杜拿尼亞](../Page/杜拿尼亞足球會.md "wikilink")           | FC Dunărea Călărași           | [克勒拉希](../Page/克勒拉希.md "wikilink")       | 乙組，第 1 位 |
| [靴曼斯塔特](../Page/靴曼斯塔特足球會.md "wikilink")         | FC Hermannstadt               | [錫比烏](../Page/錫比烏.md "wikilink")         | 乙組，第 2 位 |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[1\]

## 球會奪冠次數

  - **粗體** – 代表該球隊於頂級聯賽角逐
  - *斜體* – 代表該球隊於已解散

<table>
<thead>
<tr class="header">
<th><p>球會</p></th>
<th><p>冠軍次數</p></th>
<th><p>冠軍年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/布加勒斯特星足球俱樂部.md" title="wikilink">布加勒斯特星隊</a></strong></p></td>
<td><center>
<p>26</p></td>
<td><p>1951, 1952, 1953, 1956, 1960, 1961, 1968, 1976, 1978, 1985,<br />
1986, 1987, 1988, 1989, 1993, 1994, 1995, 1996, 1997, 1998,<br />
2001, 2005, 2006, 2013, 2014, 2015</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/布加勒斯特迪納摩足球俱樂部.md" title="wikilink">布加勒斯特戴拿模</a></strong></p></td>
<td><center>
<p>18</p></td>
<td><p>1955, 1962, 1963, 1964, 1965, 1971, 1973, 1975, 1977, 1982,<br />
1983, 1984, 1990, 1992, 2000, 2002, 2004, 2007</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/布加勒斯特維納斯體育會.md" title="wikilink">布加勒斯特維納斯</a></em></p></td>
<td><center>
<p>8</p></td>
<td><p>1920, 1921, 1929, 1932, 1934, 1937, 1939, 1940</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/UTA阿拉德足球會.md" title="wikilink">UTA阿拉德</a></p></td>
<td><center>
<p>6</p></td>
<td><p>1947, 1948, 1950, 1954, 1969, 1970</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/蒂米索拉捷尼素.md" title="wikilink">捷尼素</a></em></p></td>
<td><center>
<p>6</p></td>
<td><p>1922, 1923, 1924, 1925, 1926, 1927</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡拉奧華大學體育會.md" title="wikilink">卡拉奧華大學</a></p></td>
<td><center>
<p>4</p></td>
<td><p>1974, 1980, 1981, 1991</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/普羅耶什蒂佩特羅魯足球俱樂部.md" title="wikilink">比杜路爾</a></strong></p></td>
<td><center>
<p>4</p></td>
<td><p>1930, 1958, 1959, 1966</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蒂米索拉利班斯亞足球會.md" title="wikilink">利班斯亞</a></p></td>
<td><center>
<p>4</p></td>
<td><p>1933, 1935, 1936, 1938</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/CFR克盧日足球俱樂部.md" title="wikilink">克盧日</a></strong></p></td>
<td><center>
<p>4</p></td>
<td><p>2008, 2010, 2012, 2018</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布加勒斯特迅速足球俱樂部.md" title="wikilink">布加勒斯特迅速</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1967, 1999, 2003</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/布加勒斯特高倫天拿體育會.md" title="wikilink">高倫天拿</a></em></p></td>
<td><center>
<p>2</p></td>
<td><p>1913, 1914</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿格斯比斯特足球會.md" title="wikilink">阿格斯</a></p></td>
<td><center>
<p>2</p></td>
<td><p>1972, 1979</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/布加勒斯特奧林比亞.md" title="wikilink">布加勒斯特奧林比亞</a></em></p></td>
<td><center>
<p>2</p></td>
<td><p>1910, 1911</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/奧拉迪亞體育會.md" title="wikilink">奧拉迪亞</a></em></p></td>
<td><center>
<p>1</p></td>
<td><p>1949</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/普洛耶什蒂聯足球會.md" title="wikilink">普洛耶什蒂聯隊</a></em></p></td>
<td><center>
<p>1</p></td>
<td><p>1912</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/普洛耶什蒂柏高華.md" title="wikilink">柏高華</a></em></p></td>
<td><center>
<p>1</p></td>
<td><p>1916</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/布拉索夫高迪亞.md" title="wikilink">布拉索夫高迪亞</a></em></p></td>
<td><center>
<p>1</p></td>
<td><p>1928</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/列斯達足球會.md" title="wikilink">列斯達</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1931</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/烏爾濟切尼烏尼雷亞足球會.md" title="wikilink">烏尼雷亞</a></em></p></td>
<td><center>
<p>1</p></td>
<td><p>2009</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/布加勒斯特羅馬奧阿美利加.md" title="wikilink">羅馬奧阿美利加</a></em></p></td>
<td><center>
<p>1</p></td>
<td><p>1915</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/布加勒斯特烏尼雷亞三色旗.md" title="wikilink">烏尼雷亞三色旗</a></em></p></td>
<td><center>
<p>1</p></td>
<td><p>1941</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/加拉茨足球俱樂部.md" title="wikilink">加拉治</a></strong></p></td>
<td><center>
<p>1</p></td>
<td><p>2011</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/阿斯特拉足球俱樂部.md" title="wikilink">艾斯特拉</a></strong></p></td>
<td><center>
<p>1</p></td>
<td><p>2016</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/康斯坦察維托足球俱樂部.md" title="wikilink">維托魯干斯坦達</a></strong></p></td>
<td><center>
<p>1</p></td>
<td><p>2017</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [LPF.ro](http://www.lpf.ro/)
  - [UEFA](http://www.uefa.com/memberassociations/association=rou/domesticleague/index.html)

## 參考

[Category:羅馬尼亞足球賽事](../Category/羅馬尼亞足球賽事.md "wikilink")

1.