**羊可立**（），[字](../Page/表字.md "wikilink")**子豫**，[号](../Page/号.md "wikilink")**崧原**，[河南](../Page/河南.md "wikilink")[汝阳城南十五里角羊村人](../Page/汝阳.md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[直隸](../Page/南直隸.md "wikilink")[山阳](../Page/山阳.md "wikilink")。明朝政治人物。

## 生平

羊可立之父羊杰，[字](../Page/表字.md "wikilink")**士英**，[号](../Page/号.md "wikilink")**南溪**，學者稱**南溪先生**。可立自幼多才，篤志好學。[隆慶四年](../Page/隆慶.md "wikilink")（1570年）中[舉人](../Page/舉人.md "wikilink")，[萬曆五年](../Page/萬曆.md "wikilink")（1577年），登進士\[1\]\[2\]\[3\]。初授官[山西](../Page/山西.md "wikilink")[安邑](../Page/安邑.md "wikilink")（今[山西](../Page/山西.md "wikilink")[運城](../Page/運城.md "wikilink")）[縣令](../Page/縣.md "wikilink")。擢为[云南道](../Page/云南.md "wikilink")[监察御史](../Page/监察御史.md "wikilink")。

[萬曆十年](../Page/萬曆.md "wikilink")（1583年）[張居正死後不久](../Page/張居正.md "wikilink")，可立與[江西道](../Page/江西.md "wikilink")[御史](../Page/御史.md "wikilink")[李植](../Page/李植.md "wikilink")、[山東道](../Page/山東.md "wikilink")[監察御史江東之等攻訐張居正與](../Page/監察御史.md "wikilink")[馮保](../Page/馮保.md "wikilink")「交結恣橫」、「寶藏逾天府」。羊可立又彈劾張構陷[遼王](../Page/遼王.md "wikilink")，同時遼妃上書為遼王辯冤，並說遼府家產無數，全入張家。皇帝下令籍張居正家。[万历十二年](../Page/万历.md "wikilink")（1585年）制定《盐法条例》。擢升[太常卿](../Page/太常卿.md "wikilink")。後为权贵忌恨，貶[大理](../Page/大理.md "wikilink")[评事](../Page/评事.md "wikilink")。[万历二十三年](../Page/万历.md "wikilink")（1595年）一病不起，祀入[乡贤祠](../Page/乡贤祠.md "wikilink")。

## 著作

著有《濯缨亭集》、《中台三疏》、《筠州诗卷》、《瑞阳日记》、《羊氏家乘》等。

## 家族

曾祖父[羊友](../Page/羊友.md "wikilink")；祖父[羊文慶](../Page/羊文慶.md "wikilink")，曾任壽官；父親[羊傑](../Page/羊傑.md "wikilink")，曾任知縣。前母方氏；前母苑氏；盛氏\[4\]。

## 参考文献

[Category:明朝安邑縣知縣](../Category/明朝安邑縣知縣.md "wikilink")
[Category:明朝監察御史](../Category/明朝監察御史.md "wikilink")
[Category:明朝太常寺卿](../Category/明朝太常寺卿.md "wikilink")
[Category:明朝大理寺評事](../Category/明朝大理寺評事.md "wikilink")
[Category:汝陽人](../Category/汝陽人.md "wikilink")
[K](../Category/羊姓.md "wikilink")

1.
2.
3.
4.