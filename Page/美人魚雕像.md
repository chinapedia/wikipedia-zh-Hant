[Copenhagen_-_the_little_mermaid_statue_-_2013.jpg](https://zh.wikipedia.org/wiki/File:Copenhagen_-_the_little_mermaid_statue_-_2013.jpg "fig:Copenhagen_-_the_little_mermaid_statue_-_2013.jpg")
**美人魚雕像**位於[丹麥](../Page/丹麥.md "wikilink")[哥本哈根](../Page/哥本哈根.md "wikilink")[長堤公園的港口岩石上](../Page/長堤公園.md "wikilink")，是哥本哈根著名的觀光景點之一。

美人魚雕像在1909年時受到[嘉士伯創辦人的兒子卡爾](../Page/嘉士伯.md "wikilink")·雅布克森（Carl
Jacobsen）的委託，因為他以童話故事為題材的芭蕾舞感到著迷。於是在1912年，丹麥雕塑家[愛德華·艾瑞克森根據](../Page/愛德華·艾瑞克森.md "wikilink")[安徒生童話並以妻子愛琳](../Page/安徒生童話.md "wikilink")·埃里克森（Eline
Eriksen）為藍本，雕塑了這尊[美人魚雕像](../Page/美人魚.md "wikilink")。美人魚雕像與位於[紐西蘭](../Page/紐西蘭.md "wikilink")[內皮爾的](../Page/內皮爾_\(紐西蘭\).md "wikilink")[帕尼亞像](../Page/帕尼亞像.md "wikilink")（Pania
of the Reef）有一些類似，而且美人魚的故事也與帕尼亞傳說有些相似。

美人魚雕像於1913年8月23日正式公開。第一次參訪的旅客基本上會對這座雕像的大小感到驚訝，因為美人魚雕像高度只有1.25公尺，重量則為175公斤。2010年小美人鱼铜像首次远渡重洋，在[上海世界博览会](../Page/上海世界博览会.md "wikilink")[丹麦馆现身](../Page/中国2010年上海世界博览会丹麦馆.md "wikilink")。在小美人鱼返回丹麦之前，其原址上将展放中国艺术家[艾未未的影视艺术作品](../Page/艾未未.md "wikilink")。\[1\]

## 破坏

小美人魚雕像在历史上曾遭多次故意毁坏。她曾被砍头两次，断臂一次，被从岩石底座上抛入大海，蒙上穆斯林妇女的面纱女袍，被動保人士潑漆。\[2\]

## 參見

  - [2010年上海世博会丹麦馆](../Page/中国2010年上海世界博览会丹麦馆.md "wikilink")
  - [美人魚](../Page/美人魚_\(童話\).md "wikilink")

## 参考文献

## 外部連結

  - [The Little Mermaid. Photo gallery from Denmark. Hans Christian
    Andersen
    Information](http://www.hcandersen-homepage.dk/?page_id=14955)
  - [Little Mermaid
    Statue](https://web.archive.org/web/20052623042200/http://www.isidore-of-seville.com/mermaids/24.html)
    thumbnail photo gallery, from [Mermaids on the
    Web](http://www.isidore-of-seville.com/mermaids/)
  - [The Little
    Mermaid](http://www.panoramas.dk/the-little-mermaid/the_little_mermaid.html)
    360 degree Quicktime VR panorama from Copenhagen

[Category:丹麥雕塑作品](../Category/丹麥雕塑作品.md "wikilink")
[Category:哥本哈根旅游](../Category/哥本哈根旅游.md "wikilink")
[Category:曾被毀壞的藝術作品](../Category/曾被毀壞的藝術作品.md "wikilink")
[Category:人魚題材作品](../Category/人魚題材作品.md "wikilink")

1.  [丹麦国宝小美人鱼参展上海世博会](http://www1.voanews.com/chinese/news/20100326-DEMARK-MERMAID-CHINA-89260602.html)

2.