**美北浸礼会差会**（)是[美国浸礼会和](../Page/美国浸礼会.md "wikilink")[美北浸礼会的海外传教机构](../Page/美北浸礼会.md "wikilink")。

1814年美国浸礼会成立差会。1845年，美国的浸信会分裂成[美南浸信会和](../Page/美南浸信会.md "wikilink")[美北浸礼会](../Page/美北浸礼会.md "wikilink")。

## 在中国

1836年，美国浸礼会派遣传教士来华，首先在[葡屬澳门](../Page/葡屬澳门.md "wikilink")（1837年）及[英屬香港](../Page/英屬香港.md "wikilink")（1841年）落脚。

### 岭东(华南)

  - 传教站：[汕頭](../Page/汕頭.md "wikilink")（1860年）;
    [潮州](../Page/潮州.md "wikilink")（1894年）;
    [嘉应](../Page/梅州.md "wikilink")（1890年）;[揭阳](../Page/揭阳.md "wikilink")（1895年）；[潮阳](../Page/潮阳.md "wikilink")；河婆(揭西)；黄冈[饶平](../Page/饶平.md "wikilink")（1893年）；江西[长宁](../Page/长宁.md "wikilink")（[寻乌](../Page/寻乌.md "wikilink")，1912年）;
  - 教堂：礐石教堂，揭阳北门外教堂，嘉应东门礼拜堂。
  - 学校：角光中学，嘉应广益中学。
  - 医院：汕头益世医院，潮阳潮光医院，揭阳真理医院，河婆大同医院。

### 浙沪(华东)

  - 传教站：[宁波](../Page/宁波.md "wikilink")（1843年）;[绍兴](../Page/绍兴.md "wikilink")（1869年）；[金华](../Page/金华.md "wikilink")（1883年）;
    [湖州](../Page/湖州.md "wikilink")（1888年）;[杭州](../Page/杭州.md "wikilink")（1899年）;[上海](../Page/上海.md "wikilink")
    （1907年）;
  - 教堂：杭州东街路民众堂，绍兴东街大坊口真神堂，宁波真神堂(西门)、真神堂(北郊、后归浸会中学使用)；金华真神堂，上海文监师路真神堂
  - 学校：上海[沪江大学](../Page/沪江大学.md "wikilink")，杭州蕙兰中学，宁波浙东中学(合办)，绍兴越光中学，
  - 医院：宁波北门[华美医院](../Page/宁波第二医院.md "wikilink")，绍兴南街福康医院，金华福音医院

### 四川(华西)

  - 传教站：叙府[宜宾](../Page/宜宾.md "wikilink")（1890年）;
    嘉定[乐山](../Page/乐山.md "wikilink")（1894年）;
    雅州[雅安](../Page/雅安.md "wikilink")（1894年）;[成都](../Page/成都.md "wikilink")（1894年）
  - 教堂：成都南打金街教堂，东大街分堂
  - 学校：成都[华西协和大学](../Page/华西协和大学.md "wikilink")，
  - 医院：雅安仁德医院，叙府明德女医院

## 著名人物

  - [叔未士](../Page/叔未士.md "wikilink")（Shuck, Jehu Lewis）

## 参考文献

<references />

  -
## 参见

  - [美北浸礼会在华传教士列表](../Page/美北浸礼会在华传教士列表.md "wikilink")

{{-}}

[Category:美北浸礼会](../Category/美北浸礼会.md "wikilink")
[Category:基督教在华差会](../Category/基督教在华差会.md "wikilink")