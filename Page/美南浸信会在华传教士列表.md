|                             |                                    |             |             |            |                                                                                              |
| --------------------------- | ---------------------------------- | ----------- | ----------- | ---------- | -------------------------------------------------------------------------------------------- |
| **姓名**                      | **中文姓名**                           | **出生**      | **逝世**      | **抵达中国**   | **驻扎地点**                                                                                     |
| A. B. Cabaniss              |                                    |             |             |            | [上海](../Page/上海.md "wikilink")                                                               |
| Tarleton Perry Crawford     | [高第丕](../Page/高第丕.md "wikilink")   | 1821年5月8日   | 1902年       | 1852年      | [山东](../Page/山东.md "wikilink")，[上海](../Page/上海.md "wikilink")                                |
| Thomas T. Devan             | 德万                                 |             |             |            | [香港](../Page/香港.md "wikilink")                                                               |
| James T. Dickenson          |                                    |             |             |            |                                                                                              |
| Charles Washington Gaillard | 基律                                 |             |             |            | [澳门](../Page/澳门.md "wikilink")、[广东](../Page/广东.md "wikilink")                                |
| Rosewell Hobart Graves      | [纪好弼](../Page/纪好弼.md "wikilink")   |             |             |            | [广州](../Page/广州.md "wikilink")                                                               |
| J.B.Hartwell                | 海雅西                                |             |             |            | [山东](../Page/山东.md "wikilink")                                                               |
| J. L. Holmes                | 花雅各                                |             |             |            | [山东](../Page/山东.md "wikilink")                                                               |
| Sophie Stephens Lanneau     |                                    |             |             |            |                                                                                              |
| Lottie Moon                 | [慕拉第](../Page/慕拉第.md "wikilink")   | 1840年12月12日 | 1912年12月24日 | 1873年      | [山东](../Page/山东.md "wikilink")                                                               |
| Cicero Washington Pruitt    | 蒲其维                                |             |             |            | [山东](../Page/山东.md "wikilink")                                                               |
| Anna Seward Pruitt          | 蒲安娜                                |             |             |            | [山东](../Page/山东.md "wikilink")                                                               |
| Ida Pruitt                  | 蒲爱德                                |             |             |            | [山东](../Page/山东.md "wikilink")                                                               |
| Alanson Reed                |                                    |             |             |            |                                                                                              |
| Issachar Jacox Roberts      | [罗孝全](../Page/罗孝全.md "wikilink")   | 1802年       | 1871年       | 1837年      | [澳门](../Page/澳门.md "wikilink")、[广州](../Page/广州.md "wikilink")、[南京](../Page/南京.md "wikilink") |
| John Griffith Schilling     |                                    |             |             |            | [香港](../Page/香港.md "wikilink")                                                               |
| Henrietta Hall Shuck        | [叔何显理](../Page/叔何显理.md "wikilink") | 1817年10月28日 | 1844年11月27日 | 1836年9月17日 | [澳门](../Page/澳门.md "wikilink")、[香港](../Page/香港.md "wikilink")                                |
| Jehu Lewis Shuck            | [叔未士](../Page/叔未士.md "wikilink")   | 1812年9月4日   | 1863年       | 1836年9月17日 | [澳门](../Page/澳门.md "wikilink")、[香港](../Page/香港.md "wikilink")、[广州](../Page/广州.md "wikilink") |
| Matthew Tyson Yates         | [晏玛太](../Page/晏玛太.md "wikilink")   | 1819年       | 1888年       | 1847年      | [上海](../Page/上海.md "wikilink")                                                               |
|                             |                                    |             |             |            |                                                                                              |

## 参见

  - [美南浸信会](../Page/美南浸信会.md "wikilink")

[美南浸信会在华传教士](../Category/美南浸信会在华传教士.md "wikilink")
[Category:基督教新教在华传教士列表](../Category/基督教新教在华传教士列表.md "wikilink")