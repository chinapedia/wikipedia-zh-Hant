此处收列了[美国历史主要事件的时间表](../Page/美国历史.md "wikilink")。

## 大事年表

  - [殖民地时期美国历史年表](../Page/殖民地时期美国历史年表.md "wikilink")

<!-- end list -->

  - [美国历史 (1776年-1849年)](../Page/美国历史_\(1776年-1849年\).md "wikilink")

<!-- end list -->

  - [美国历史 (1849年-1865年)](../Page/美国历史_\(1849年-1865年\).md "wikilink")

<!-- end list -->

  - [美国历史 (1865年-1918年)](../Page/美国历史_\(1865年-1918年\).md "wikilink")

<!-- end list -->

  - [美国历史 (1918年-1945年)](../Page/美国历史_\(1918年-1945年\).md "wikilink")

<!-- end list -->

  - [美国历史 (1945年-1964年)](../Page/美国历史_\(1945年-1964年\).md "wikilink")

<!-- end list -->

  - [美国历史 (1964年-1980年)](../Page/美国历史_\(1964年-1980年\).md "wikilink")

<!-- end list -->

  - [美国历史 (1980年至今)](../Page/美国历史_\(1980年至今\).md "wikilink")

## 分类历史索引

  - [美国人口史](../Page/美国人口史.md "wikilink")
      - [美国移民史](../Page/美国移民史.md "wikilink")

<!-- end list -->

  - [美国经济史](../Page/美国经济史.md "wikilink")
      - 金融体系：[美国第一银行](../Page/美国第一银行.md "wikilink")、[美国第二银行](../Page/美国第二银行.md "wikilink")、[美联储](../Page/美联储.md "wikilink")、[布雷顿森林体系](../Page/布雷顿森林体系.md "wikilink")
      - 经济事件：[大萧条](../Page/大萧条.md "wikilink")、[石油危机](../Page/石油危机.md "wikilink")、[次贷危机](../Page/次贷危机.md "wikilink")

<!-- end list -->

  - [美国政治史](../Page/美国政治史.md "wikilink")
      - 建国：[独立宣言](../Page/独立宣言.md "wikilink")、[美国开国元勋](../Page/美国开国元勋.md "wikilink")、[美国宪法历史](../Page/美国宪法历史.md "wikilink")
      - 主要政党：[聯邦黨](../Page/聯邦黨.md "wikilink")、[民主共和党](../Page/民主共和党.md "wikilink")、[民主党历史](../Page/民主党_\(美国\)#歷史.md "wikilink")、[共和党历史](../Page/共和党_\(美国\)#歷史.md "wikilink")
      - 政治制度：[众议院](../Page/众议院.md "wikilink")、[参议院](../Page/参议院.md "wikilink")、[选举人团](../Page/选举人团.md "wikilink")

<!-- end list -->

  - [美国外交史](../Page/美国外交史.md "wikilink")
      - 外交政策：[门罗主义](../Page/门罗主义.md "wikilink")、[大棒政策](../Page/大棒政策.md "wikilink")。[杜鲁门主义](../Page/杜鲁门主义.md "wikilink")
      - 外交事件：[香蕉戰爭](../Page/香蕉戰爭.md "wikilink")。[雅尔塔会议](../Page/雅尔塔会议.md "wikilink")。[厨房辩论](../Page/厨房辩论.md "wikilink")、[古巴导弹危机](../Page/古巴导弹危机.md "wikilink")

<!-- end list -->

  - [美国军事史](../Page/美国军事史.md "wikilink")
      - [美國領土變遷](../Page/美國領土變遷.md "wikilink")
      - 近代：[1812年战争](../Page/1812年战争.md "wikilink")、[美墨戰爭](../Page/美墨戰爭.md "wikilink")；[美西战争](../Page/美西战争.md "wikilink")（古巴）、[美菲战争](../Page/美菲战争.md "wikilink")（菲律宾）
      - 现代：[一战](../Page/一战.md "wikilink")、[二战](../Page/二战.md "wikilink")（[太平洋战争](../Page/太平洋战争.md "wikilink")、[诺曼底登陆](../Page/诺曼底登陆.md "wikilink")）。[朝鲜战争](../Page/朝鲜战争.md "wikilink")、[猪湾事件](../Page/猪湾事件.md "wikilink")、[越南战争](../Page/越南战争.md "wikilink")。[海湾战争](../Page/海湾战争.md "wikilink")、[伊拉克战争](../Page/伊拉克战争.md "wikilink")

<!-- end list -->

  - [美国科技史](../Page/美国科技史.md "wikilink")

<!-- end list -->

  - [美国文化史](../Page/美国文化史.md "wikilink")
      - [美国文学](../Page/美国文学.md "wikilink")
      - [迷失的一代](../Page/迷失的一代.md "wikilink")

[美國歷史年表](../Category/美國歷史年表.md "wikilink") [United
States](../Category/各國年表.md "wikilink")