**美国国家侦察局**（National Reconnaissance
Office，**NRO**），位于[美国](../Page/美国.md "wikilink")[维吉尼亚州](../Page/维吉尼亚州.md "wikilink")，是美国的16个情报机构之一；负责为[美国政府设计](../Page/美国政府.md "wikilink")、组装并发射侦察[卫星](../Page/卫星.md "wikilink")。\[1\]
并协调、收集和分析从[中央情报局以及军事机构的](../Page/中央情报局.md "wikilink")[航天飞机](../Page/航天飞机.md "wikilink")、[卫星收集到的情报](../Page/卫星.md "wikilink")。\[2\]
该机构得到国家侦察计划（外国情报收集计划的一部分）的拨款。该机构是[国防部的组成部分之一](../Page/美国国防部.md "wikilink")。

国侦局和相关[太空](../Page/太空.md "wikilink")、情报机构密切合作，包括[美国国家安全局](../Page/美国国家安全局.md "wikilink")（NSA）、[美国国家地理空间情报局](../Page/美国国家地理空间情报局.md "wikilink")（NGA）、[中央情报局](../Page/中央情报局.md "wikilink")（CIA）、[美国国防高级研究计划局](../Page/國防高等研究計劃署.md "wikilink")（DARPA）、[美国战略司令部](../Page/美国战略司令部.md "wikilink")、[海军研究实验室等高级机构](../Page/美国海军研究实验室.md "wikilink")。负责运作在世界各地的地面站，用于收集并发布来自侦察卫星的情报。

## 历史

国侦局成立于1961年9月6日，旨在提高国家卫星侦察系统。1958年2月[苏联发射](../Page/苏联.md "wikilink")[人造地球卫星1号后](../Page/人造地球卫星1号.md "wikilink")，在[艾森豪威尔支持下成立](../Page/艾森豪威尔.md "wikilink")，当飞行员[弗朗西斯·加里·鲍尔斯驾驶的](../Page/弗朗西斯·加里·鲍尔斯.md "wikilink")[U-2侦察机在](../Page/U-2侦察机.md "wikilink")1960年5月1日被击落后，该机构显得更加重要。

国侦局的第一项照片侦察卫星计划被成为[“日冕”计划](../Page/日冕_\(卫星\).md "wikilink")，根据1995年2月的解密资料，“日冕”计划存在于1960年8月到1972年5月，尽管首次试飞是在1959年2月。“日冕计划”的卫星所释放的摄影太空舱（有时用多个）由军方的飞船来校正。最成功的一次，对发现者13号的校正于1960年8月12日进行。第一张来自太空的图片在六天之后问世，分辨度达8米，后来被改善至2米。一张照片平均能显示大约16到190千米的范围。日冕的最后一次任务（第145次）在1972年5月25日进行。此项任务的最后一张照片拍摄于1972年5月31日。

从1962年5月到1964年8月，国侦局主持了Argon计划中的12次地图绘制任务，但只有7次成功。
1963年，国侦局开始用更高的分辨率进行Lanyard计划中的地图绘制部分。

国侦局1972年之后的任务仍然是机密，之前计划的剩余部分也不再为公众所知晓。国防部副部长在[中央情报局局长的建议下于](../Page/中央情报局.md "wikilink")1992年9月18日证实了国侦局的存在。

1995年，一篇《[华盛顿邮报](../Page/华盛顿邮报.md "wikilink")》文章指出，在[中情局](../Page/中央情报局.md "wikilink")、[五角大楼和](../Page/五角大楼.md "wikilink")[国会不知情的情况下](../Page/美国国会.md "wikilink")，国侦局秘密保留了10亿至17亿[美元的未尽专款](../Page/美元.md "wikilink")。中情局接到投诉，国侦局在一年前从秘密预算中拿出私留3亿美元在维吉尼亚州的尚蒂伊建造一个新的总部，因此介入国侦局的预算调查。[美国科学家联合会获得的一份建筑许可中的](../Page/美国科学家联合会.md "wikilink")[蓝图则泄露了国侦局秘密新总部的情况](../Page/蓝图.md "wikilink")。在[九一一后该蓝图便被列为机密](../Page/九一一袭击事件.md "wikilink")。国侦局挪用资金被证实。发起这项调查的前中情局首席[律师傑佛瑞](../Page/律师.md "wikilink")·史密斯（Jeffrey
Smith）表示：“我们的调查显示，国侦局数量惊人的‘私房钱’已经积蓄多年了。”

1999年，国侦局和[波音公司开始着手名为](../Page/波音公司.md "wikilink")“Future Imagery
Architecture”的项目，即开发新一代的成像卫星。[纽约时报在](../Page/纽约时报.md "wikilink")2007年11月11日发表的一篇调查报告指出：根据国侦局的记录，自2002年开始该项目进度就远远落后于计划，并可能超出预算20至30亿美元。美国政府开始施压，提出应努力完成该项目。但两年之后，因新增的评审小组和数十亿美元的额外开销，该计划被迫中止。纽约时报报道：“也许50年来最引人瞩目和最昂贵的失败就是美国的间谍卫星项目”
\[3\]

2008年1月，美国政府宣布，国侦局的一颗[侦察卫星将在数月后无法挽回地坠入](../Page/侦察卫星.md "wikilink")[大气层](../Page/大气层.md "wikilink")。
卫星观测爱好者们称这颗卫星可能是[洛克希德马丁制造的USA](../Page/洛克希德马丁.md "wikilink")-193，在2006年12月进入轨道后不久就失效了。\[4\]
2008年2月14日，五角大楼称与其任卫星坠入大气层，不如用[海军](../Page/美国海军.md "wikilink")[巡洋舰的](../Page/巡洋舰.md "wikilink")[导弹将其击落](../Page/导弹.md "wikilink")。\[5\]
此次击落行动最终在2008年2月21日进行。\[6\]

## 组织

国侦局是[国防部的一部分](../Page/美国国防部.md "wikilink")，国侦局局长由[国防部长在](../Page/美国国防部长.md "wikilink")[美国国家情报总监的同意下任命](../Page/美国国家情报总监.md "wikilink")，不需要国会的许可。按照惯例，该职位由[空军副部长或部长助理来担任](../Page/美国空军.md "wikilink")，
但Donald
Kerr在2005年7月被任命为国侦局局长是个例外。国侦局的员工来自中情局、[国安局](../Page/美国国家安全局.md "wikilink")、军队和民间国防承包商。

国侦局由下列机构组成：信号情报处、通讯处、图像情报处和技术处。\[7\]

## “战略游戏部门”

2002年，一本安全会议的宣传手册中提到，国侦局有个“战略游戏部门”，由John Fulton负责训练中情局的工作人员。\[8\]

## 宇宙飞船

国侦局的宇宙飞船有：

  -   - [KH-5](../Page/KH-5.md "wikilink")—*Argon*（1961年－1962年）
      - [KH-6](../Page/KH-6.md "wikilink")—*Lanyard*（1963年）
      - [KH-7](../Page/KH-7.md "wikilink")—*Gambit*（1963年－1967年）
      - [KH-8](../Page/KH-8.md "wikilink")—*Gambit*（1966年－1984年）
      - [KH-9](../Page/KH-9.md "wikilink")—*Hexagon* 和 *Big
        Bird*（1971年－1986年）
      - [KH-10](../Page/Manned_Orbiting_Laboratory.md "wikilink")—*Dorian*（已停止）
      - [KH-11](../Page/KH-11.md "wikilink")—*Crystal* 和
        *Kennan*（1976年－1988年）
      - [KH-12](../Page/KH-12.md "wikilink")—*Ikon* 和 *Improved
        Crystal*（1990年－？）
      - [KH-13](../Page/KH-13.md "wikilink")—（1999年－？）

  - [*Samos*](../Page/Samos_\(衛星\).md "wikilink")— 成像卫星（1960年－1962年）

  - [*Poppy*](../Page/Poppy_\(衛星\).md "wikilink")（1960年－1961年）

  - [*Jumpseat*](../Page/Jumpseat_\(衛星\).md "wikilink")（1971-1983） 和
    [Trumpet](../Page/Trumpet_\(衛星\).md "wikilink")（1994-1997）
    [SIGINT](../Page/SIGINT.md "wikilink")

  - [*Lacrosse*/*Onyx*](../Page/Lacrosse_\(衛星\).md "wikilink")—
    成像[雷达](../Page/雷达.md "wikilink")（1988年－）

  - [*Canyon*](../Page/Canyon_\(衛星\).md "wikilink")（1968-1977）,
    *[Vortex/Chalet](../Page/Vortex/Chalet.md "wikilink")*（1978–1989）
    和*[Mercury](../Page/Mercury_\(衛星\).md "wikilink")*（1994-1998）—[SIGINT](../Page/SIGINT.md "wikilink")
    包括 [COMINT](../Page/COMINT.md "wikilink")

  - [Rhyolite/Aquacade](../Page/Rhyolite/Aquacade.md "wikilink")（1970–1978）,
    [Magnum/Orion](../Page/Magnum_\(衛星\).md "wikilink")（1985–1990） 和
    [Mentor](../Page/Mentor_\(衛星\).md "wikilink")（1995-2003）—[SIGINT](../Page/SIGINT.md "wikilink")

  - [*Quasar*](../Page/Quasar_\(衛星\).md "wikilink")、[communications
    relay](../Page/Repeater.md "wikilink")

  - [Misty](../Page/Misty_\(衛星\).md "wikilink")/[Zirconic](../Page/Zirconic.md "wikilink")

## 參考資料

## 參看

  - [间谍卫星](../Page/间谍卫星.md "wikilink")
  - [骗局](../Page/骗局_\(小说\).md "wikilink")（[丹布朗](../Page/丹布朗.md "wikilink")[小说](../Page/小说.md "wikilink")）

## 外部連結

  - [国家侦察局官方網站](http://www.nro.gov)
  - [國家安全記錄：国家侦察局解密](http://www.gwu.edu/~nsarchiv/NSAEBB/NSAEBB35/)（National
    Security Archive: The NRO Declassified）
  - [国家侦察局解密備忘錄](http://www.fas.org/irp/nro/dod091802.html)（Memo of
    Declassification of NRO）
  - [Additional
    國家偵查局資訊](http://www.fas.org/irp/nro/)，來自[美國科學家聯盟](../Page/美國科學家聯盟.md "wikilink")
  - [美國的祕密間諜衛星花了你數十億元，但卻連發射都不成功](http://www.usnews.com/usnews/news/articles/030811/11nro.htm/)，刊登於
    U.S News 和 World Report，2003年8月11日，Douglas Pasternak 撰寫
  - [Agency planned exercise on Sept. 11 built around a plane crashing
    into a
    building](http://www.boston.com/news/packages/sept11/anniversary/wire_stories/0903_plane_exercise.htm)，來自
    [Boston.com](http://www.boston.com)

[Category:美國國防部](../Category/美國國防部.md "wikilink")
[N](../Category/美國情報機構.md "wikilink")

1.
2.  "[NRO Provides Support to the
    Warfighters](http://www.nro.gov/PressReleases/prs_rel18.html) "
3.
4.
5.
6.
7.
8.  *[America's Leadership
    Challenge](http://webarchive.org/web/20030212092040/http://www.nlsi.net/hs-alc-info.htm)*
    (pre-event publicity pamphlet for National Law Enforcement And
    Security Institute \[NLSI\] conference "Homeland Security: America's
    Leadership Challenge", Sept. 6, 2002).