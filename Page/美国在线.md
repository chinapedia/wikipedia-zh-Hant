[America_Online_logo.svg](https://zh.wikipedia.org/wiki/File:America_Online_logo.svg "fig:America_Online_logo.svg")

**美国在线**（**AOL
Inc.**，前身為：，），著名的[互联网服务提供者](../Page/互联网服务提供者.md "wikilink")，現為電信商[威訊旗下](../Page/威訊.md "wikilink")[Oath公司的子公司](../Page/Oath.md "wikilink")\[1\]\[2\]。

## 历史

今天的美国在线，源自20世纪80年代的一家计算机服务公司**Control
Video**\[3\]，该公司一度陷入财政危机，后来一家電腦公司要为它的Commodore商务机用户提供互联网接入服务，Control
Video公司提供了此服务，并摆脱财务困境，后改名为**Quantum Computer
Services**。Quantum为Commodore商务机设计的拨号网络[Q-link于](../Page/Q-link.md "wikilink")1985年上市\[4\]，与[Tandy公司合作的](../Page/Tandy.md "wikilink")[PC-link也在](../Page/PC-link.md "wikilink")1989年上市。1989年10月，公司将[America
Online服务引入](../Page/America_Online.md "wikilink")[Macintosh和Aplle](../Page/Macintosh.md "wikilink")Ⅱ，其[DOS版本则于](../Page/DOS.md "wikilink")1991年2月上市。

后来[苹果公司撤除交易](../Page/苹果公司.md "wikilink")，Quantum决定将其所有網路服务重新命名为Online
American，后改名为America Online（由Case举办的一次征名大赛而来）\[5\]\[6\]。

  - 1998年，收购[CompuServe和](../Page/CompuServe.md "wikilink")[ICQ](../Page/ICQ.md "wikilink")。
  - 1998年11月，收购[網景](../Page/網景.md "wikilink")\[7\]。
  - 2000年，美国在线和[时代华纳](../Page/时代华纳.md "wikilink")（）宣布计划合并，2001年1月11日该交易被[联邦贸易委员会](../Page/联邦贸易委员会.md "wikilink")（）证实。合并及以后的运作的信息见[时代华纳](../Page/时代华纳.md "wikilink")。
  - 2009年12月9日，正式与时代华纳分离，成立独立公司并将标识改为「Aol.」。美国在线期望通过此举，由网络接入商转型成为广告提供商和原创内容提供商。在与时代华纳合并期间，美国在线总市值缩水98%\[8\]。
  - 2011年2月8日，美國線上以3億1千5百萬美元收購[哈芬登郵報](../Page/哈芬登郵報.md "wikilink")\[9\]。
  - 2013年8月8日，AOL宣佈以4.05億美元收購[網路廣告交易平台Adap](../Page/網路廣告.md "wikilink").tv。Adap.tv是一家幫助企業出售和購買在線視頻廣告的公司。AOL收購Adap.tv的交易價格中，包括3.22億美元現金及約8300萬美元的AOL股票。
  - 2015年5月12日，[威訊宣佈將以每股](../Page/威訊.md "wikilink")50美元、即總額44億美元收購AOL，這項交易將於2015年夏季完成\[10\]。

## 参考文献

## 外部链接

  - [地理坐标](../Page/地理坐标.md "wikilink")：

  -
  - [A Visual Archive of America Online, Versions 1.1
    – 3.0](http://14forums.blogspot.com/2013/06/14forums.html)

{{-}}

[Category:ISP](../Category/ISP.md "wikilink")
[Category:美國線上公司](../Category/美國線上公司.md "wikilink")
[Category:再次上市公司企業](../Category/再次上市公司企業.md "wikilink")
[Category:1983年成立的公司](../Category/1983年成立的公司.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.  [AOL Celebrates Day One as an Independent
    Company](http://corp.aol.com/2009/12/10/aol-celebrates-day-one-as-an-independent-company/).
    AOL Press Release. Retrieved on April 9, 2012.

9.  [AOL buys Huffington Post: the beginning of the
    end?](http://www.guardian.co.uk/media/pda/2011/feb/07/aol-buys-huffington-post).
    Guardian. Retrieved on July 8, 2011.

10.