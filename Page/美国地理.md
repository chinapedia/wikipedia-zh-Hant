|                                                                                               |
| :-------------------------------------------------------------------------------------------: |
| [USA_topo_en.jpg](https://zh.wikipedia.org/wiki/File:USA_topo_en.jpg "fig:USA_topo_en.jpg") |
|                               <small>**美國本土48個州分的地勢圖**</small>                                |
|                                            **面積**                                             |
|                                            **總計**                                             |
|                                            **陸域**                                             |
|                                            **水域**                                             |
|                                            **緯度**                                             |
|                                            **經度**                                             |
|                                            **國界**                                             |
|                             **[加拿大](../Page/加拿大.md "wikilink")**                              |
|                             **[墨西哥](../Page/墨西哥.md "wikilink")**                              |
|                                            **海岸線**                                            |
|                                           **領海主張**                                            |
|                                            **鄰接區**                                            |
|                                           **經濟海域**                                            |
|                                            **領海**                                             |

**美國地理**

**[美國](../Page/美國.md "wikilink")**位於[西半球](../Page/西半球.md "wikilink")，由[美國本土](../Page/美國本土.md "wikilink")、[阿拉斯加州及](../Page/阿拉斯加州.md "wikilink")[夏威夷州三個部分所組成](../Page/夏威夷州.md "wikilink")：美國本土包含[北美洲](../Page/北美洲.md "wikilink")48個相鄰的[州份](../Page/美國州份.md "wikilink")；阿拉斯加州是位在北美洲最西北部的[半島](../Page/半島.md "wikilink")；夏威夷州則是坐落於[太平洋的](../Page/太平洋.md "wikilink")[群島](../Page/群島.md "wikilink")。此外，太平洋和[加勒比海上還有](../Page/加勒比海.md "wikilink")[波多黎各等](../Page/波多黎各.md "wikilink")[美國領土](../Page/美國領土.md "wikilink")。在地理意義上，「美國」一詞普遍指的是美國大陸，而不包括阿拉斯加州、夏威夷州、波多黎各、[關島和](../Page/關島.md "wikilink")[美屬維京群島](../Page/美屬維京群島.md "wikilink")\[1\]。美國本土分別與[加拿大和](../Page/加拿大.md "wikilink")[墨西哥相鄰](../Page/墨西哥.md "wikilink")，並隔海與[俄羅斯](../Page/俄羅斯.md "wikilink")、[古巴](../Page/古巴.md "wikilink")、[巴哈馬對望](../Page/巴哈馬.md "wikilink")。

## 面積

美國的[總面積](../Page/國家面積列表.md "wikilink")（包含水域及陸域）大小和[中國相去不遠](../Page/中國.md "wikilink")，依計算入未實際管轄的主張領土範圍與否，為全球第三或第四大的國家；兩國皆僅次於[俄羅斯和](../Page/俄羅斯.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")，但列於[巴西之前](../Page/巴西.md "wikilink")\[2\]。若只考慮陸域面積，美國則是世界第三大國家，依序亞於俄羅斯和中國，加拿大位居第四\[3\]。美國與中國領土面積大小的季殿之爭取決於兩大因素：（一）中國對[阿克賽欽和](../Page/阿克賽欽.md "wikilink")[喀喇崑崙走廊領土主張的合法性](../Page/喀喇崑崙走廊.md "wikilink")。上述地區同時也是印度的主張領土，故未列入計算；（二）美國計算自身國土表面積的方式。自[世界概況初次發布以來](../Page/世界概況.md "wikilink")，[中央情報局](../Page/中央情報局.md "wikilink")（CIA）對美國總面積大小便有過數次更動\[4\]。1989年至1996年，美國總面積明列為9,372,610平方公里（僅包含陸域和內陸水域）。1997年變更至9,629,091平方公里（加入計算五大湖區和沿海水域），2004年至9,631,418平方公里，2006年至9,631,420平方公里，以及2007年至9,826,630平方公里（加入計算海域）。現今，美國總面積大小共有三方說法：中央情報局的世界概況為9,826,675平方公里\[5\]，[聯合國統計部門為](../Page/聯合國.md "wikilink")9,629,091平方公里\[6\]，《[大英百科全書](../Page/大英百科全書.md "wikilink")》則為9,522,055平方公里\[7\]。

## 參考文獻

## 外部連結

  - [USGS: Tapestry of Time and
    Terrain](https://web.archive.org/web/20030209053037/http://tapestry.usgs.gov/)
  - [United States Geological Survey](http://www.usgs.gov/) - 免費航空地圖
  - [National Atlas of the United States of
    America](https://web.archive.org/web/20081205020547/http://www.nationalatlas.gov/)

[美国地理](../Category/美国地理.md "wikilink")

1.
2.  參見：[國家面積列表](../Page/國家面積列表.md "wikilink")
3.  雅虎（Yahoo's）國家陸域面積圖表（資料來源：CIA世界概況）：http://education.yahoo.com/reference/factbook/countrycompare/area/3d.html;_ylt=As1XMsN8kgSx746VWazy_s7PecYF

4.  .
5.
6.
7.