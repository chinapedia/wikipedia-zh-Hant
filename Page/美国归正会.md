**美国归正会**（Reformed Church in
America，RCA）是美国的一个[基督新教教派](../Page/基督新教.md "wikilink")，属于[改革宗](../Page/改革宗.md "wikilink")（喀爾文宗，Calvinist），[教會體制採](../Page/教會體制.md "wikilink")[長老制](../Page/教政體制#.E9.95.B7.E8.80.81.E5.88.B6.md "wikilink")，起源于[荷兰](../Page/荷兰.md "wikilink")，曾经称为**荷兰归正会**（Dutch
Reformed
Church），屬於[喀爾文主義教派](../Page/喀爾文主義.md "wikilink")。在美国和加拿大拥有30万教徒。总部位于[大急流城](../Page/大急流城_\(密西根州\).md "wikilink")。

美国归正会參與[美国基督教会联合会](../Page/美国基督教会联合会.md "wikilink")（National Council of
Churches）、[世界基督教会联合会](../Page/世界基督教会联合会.md "wikilink")（World Council of
Churches）和，一部分的會堂則加入[National Association of
Evangelicals](../Page/National_Association_of_Evangelicals.md "wikilink").

美国归正会接受《》、《》與《[海德堡探題](../Page/海德堡探題.md "wikilink")》等[宗教改革運動時期的](../Page/宗教改革運動.md "wikilink")[喀爾文宗文獻](../Page/喀爾文宗.md "wikilink")，以及該教會在當代的信仰宣言：Our
Song of Hope(1978)，和反對種族隔離的(1982)\[1\]。

美国归正会和[美國福音路德教會](../Page/美國福音路德教會.md "wikilink")、美國[聯合基督教會](../Page/聯合基督教會.md "wikilink")、[美國長老教會達成](../Page/美國長老教會.md "wikilink")[共融](../Page/共融.md "wikilink")，不過在2009年時教會內的保守派有撤銷的呼聲。在2014年美国归正会與達成結盟關係\[2\]。

## 历史

归正会是[北美洲最古老的](../Page/北美洲.md "wikilink")[新教教会之一](../Page/新教.md "wikilink")。早期[荷兰移民在](../Page/荷兰.md "wikilink")[新尼德兰](../Page/新尼德兰.md "wikilink")（New
Netherland）举行宗教集会 1628年，[Jonas
Michaelius在](../Page/Jonas_Michaelius.md "wikilink")[新阿姆斯特丹](../Page/新阿姆斯特丹.md "wikilink")[纽约市东](../Page/纽约市.md "wikilink")22街25号归正会大厦（Reformed
Church
Building）组织了[荷兰归正会](../Page/荷兰归正会.md "wikilink")。在[纽约市有](../Page/纽约市.md "wikilink")4座教堂：华盛顿堡教堂（Fort
Washington Collegiate Church）、中教堂（Middle Collegiate Church）、大理石教堂（Marble
Collegiate Church），和西区教堂（West End Collegiate Church）。

归正会是新尼德兰的官方宗教。尽管英国人在1664年夺取了这个殖民地，所有归正会神职人员仍在[荷兰受训练](../Page/荷兰.md "wikilink")，直到1764年，归正会的礼拜一直使用[荷兰语](../Page/荷兰语.md "wikilink")。

该教派建立了[纽约州和](../Page/纽约州.md "wikilink")[新泽西许多古老的殖民地教堂](../Page/新泽西.md "wikilink")；19世纪中叶来自[荷兰的新移民](../Page/荷兰.md "wikilink")，又使得该教派在中西部得到发展。在密歇根州的荷兰（Holland）建立了[Hope
学院和](../Page/Hope_学院.md "wikilink")[西部神学院](../Page/西部神学院.md "wikilink")，在[Pella,
Iowa建立了](../Page/Pella,_Iowa.md "wikilink")[中央学院](../Page/中央学院.md "wikilink")。1857年密歇根的一批荷兰移民由[Gijsbert
Haan领头](../Page/Gijsbert_Haan.md "wikilink")，从归正会分裂出去，组织了[Christian
Reformed
Church](../Page/Christian_Reformed_Church.md "wikilink")，其他人也效仿他。第二次世界大战以后该教派扩展到加拿大。从1949年到1958年，该教派在非荷兰人的郊区社区建立了120个教堂。

## 名人

  - ，[美国参议员](../Page/美國參議院.md "wikilink")

  - ，神学家

  - ，佈道家

  - [西奥多·罗斯福](../Page/西奥多·罗斯福.md "wikilink")，美国第26任总统

  - [菲力·斯凱勒](../Page/菲力·斯凱勒.md "wikilink")，美国革命领袖

  - [羅伯特·舒樂](../Page/羅伯特·舒樂.md "wikilink")，佈道家

  - [马丁·范布伦](../Page/马丁·范布伦.md "wikilink")，美国第8任总统

## 大学和神学院

  - 大学

<!-- end list -->

  - ，位於

  - ，位於[密歇根州](../Page/密歇根州.md "wikilink")

  - ，位於[-{zh-cn:艾奥瓦;zh-hk:艾奧瓦;zh-tw:愛荷華;zh-sg:艾奥瓦;zh-mo:愛阿華;}-州](../Page/愛荷華州.md "wikilink")[-{zh-cn:奥兰治城;zh-tw:橙鎮;}-](../Page/橙鎮_\(愛荷華州\).md "wikilink")

<!-- end list -->

  - [神學院](../Page/神學院.md "wikilink")\[3\]

<!-- end list -->

  - [新Brunswick 神学院](../Page/新Brunswick_神学院.md "wikilink")（New Brunswick
    Theological
    Seminary），位於[新泽西州](../Page/新泽西州.md "wikilink")[-{zh-cn:新不伦瑞克;zh-hk:紐賓士域;zh-tw:新布藍茲維;}-](../Page/新不伦瑞克_\(新泽西州\).md "wikilink")

  - ，位於[密歇根州](../Page/密歇根州.md "wikilink")

## 在华传教

归正会最初加入[美国公理会差会进行海外传教](../Page/美国公理会差会.md "wikilink")。1857年才成立了独立差会。那一年，[约翰·打马字](../Page/约翰·打马字.md "wikilink")（J.V.N.
Talmage）在中国厦门将公理会1842年开始的工作移交给归正会。

归正会在中国传教的区域始终局限在[福建南部](../Page/福建.md "wikilink")。先后建立的传教站有5个:[厦门](../Page/厦门.md "wikilink")(1842)；[漳州](../Page/漳州.md "wikilink")(1853)；小溪(平和，1876)；同安(1886)和龙岩。除了建立教堂以外，还建立了厦门救世医院、[寻源中学](../Page/寻源中学.md "wikilink")（[林语堂的母校](../Page/林语堂.md "wikilink")，先在厦门，后迁漳州）等附属机构，并参与创办[福建协和大学](../Page/福建协和大学.md "wikilink")。

## 参考文献

## 资源

  - M. G. Hansen,归正会在荷兰, 1340–1840 (1884)
  - J. J. Birch, The Pioneering Church in the Mohawk Valley (1955)

## 外部链接

  - [官方网站](http://www.rca.org/)

## 参见

  - [美国归正会在华传教士列表](../Page/美国归正会在华传教士列表.md "wikilink")
  - [荷兰归正会](../Page/荷兰归正会.md "wikilink")（Dutch Reformed Church）

[Category:美国基督教新教组织](../Category/美国基督教新教组织.md "wikilink")
[Category:歸正宗](../Category/歸正宗.md "wikilink")

1.
2.
3.  [RCA - About Us: Educational
    Institutions](http://www.rca.org/NETCOMMUNITY/Page.aspx?pid=425&srcid=2225)