**美国数学协会**（， 缩写作
**MAA**）是一個關注[大學](../Page/大學.md "wikilink")[數學教育的專業團體](../Page/數學教育.md "wikilink")，成員涵盖各[學院和](../Page/學院.md "wikilink")[高校教師](../Page/高校.md "wikilink")、畢業及未畢業的大學生，多为[數學家](../Page/數學家.md "wikilink")、[科學家](../Page/科學家.md "wikilink")。美国数学协会總部位於[華盛頓市](../Page/華盛頓市.md "wikilink")。

它贊助每屆的[威廉·羅威爾·普特南數學競賽](../Page/威廉·羅威爾·普特南數學競賽.md "wikilink")，也曾舉行一些小型但不算瑣碎的活動，例如使用[樂高磚塊建造數學表面](../Page/樂高.md "wikilink")。

## 出版物

MAA亦是著名的專業及普及數學書籍出版商，以數學家和教育家為對象的期刊。主要有：

  - [美国数学月刊](../Page/美国数学月刊.md "wikilink")（American Mathematical
    Monthly）

  - （Mathematics Magazine）

  - （College Mathematics Journal）

  - （Math Horizons）

  -
## 地区划分

MAA由29个分区组成:\[1\]

  - 东南地区
  - 西南地区
  - 中北部地区
  - 东北部地区
  - 英特蒙顿地区（intermountain）
  - Seaways

<!-- end list -->

  - [宾夕法尼亚州东部及](../Page/宾夕法尼亚州.md "wikilink")[特拉华州地区](../Page/特拉华州.md "wikilink")（Eastern
    Pennsylvania Delaware, EPADEL）
  - [佛罗里达州地区](../Page/佛罗里达州.md "wikilink")
  - [伊利诺斯州地区](../Page/伊利诺斯州.md "wikilink")
  - [印第安那州地区](../Page/印第安那州.md "wikilink")
  - [衣阿华州地区](../Page/衣阿华州.md "wikilink")
  - [堪萨斯州地区](../Page/堪萨斯州.md "wikilink")
  - [肯塔基州地区](../Page/肯塔基州.md "wikilink")
  - [路易斯安那州](../Page/路易斯安那州.md "wikilink")/[密西西比州地区](../Page/密西西比州.md "wikilink")
  - [马里兰州](../Page/马里兰州.md "wikilink")-[哥伦比亚特区](../Page/哥伦比亚特区.md "wikilink")-[弗吉尼亚州地区](../Page/弗吉尼亚州.md "wikilink")
  - [纽约都会区](../Page/纽约都会区.md "wikilink")
  - [密西根州地区](../Page/密西根州.md "wikilink")
  - [密苏里州地区](../Page/密苏里州.md "wikilink")
  - [内布拉斯加州](../Page/内布拉斯加州.md "wikilink") -
    [南达科他州东南部地区](../Page/南达科他州.md "wikilink")
  - [新泽西州地区](../Page/新泽西州.md "wikilink")(或稱「[紐澤西州](../Page/紐澤西州.md "wikilink")」)
  - [加利福尼亚州北部](../Page/加利福尼亚州.md "wikilink") -
    [内华达州](../Page/内华达州.md "wikilink")-[夏威夷群岛地区](../Page/夏威夷群岛.md "wikilink")
  - [俄亥俄州地区](../Page/俄亥俄州.md "wikilink")
  - [奧克拉荷马州](../Page/奧克拉荷马州.md "wikilink")-[阿肯色州地区](../Page/阿肯色州.md "wikilink")
  - [加利福尼亚州南部](../Page/加利福尼亚州.md "wikilink") -
    [内华达州地区](../Page/内华达州.md "wikilink")
  - [德克萨斯州地区](../Page/德克萨斯州.md "wikilink")
  - [威斯康辛州地区](../Page/威斯康辛州.md "wikilink")
  - [太平洋西北部地区地区](../Page/太平洋.md "wikilink")
  - [洛磯山地区](../Page/洛磯山.md "wikilink")
  - [阿利根尼山脉地区](../Page/阿利根尼山脉.md "wikilink")（Allegheny Mountain）

## 参考文献

## 外部連結

  - [MAA官方網站](http://www.maa.org)

  - [MAA書單（來自美國數學學會）](http://www.kolmogorov.com/Book_List.html)

  - [Convergence](http://mathdl.maa.org/convergence/1/),
    MAA关于数学历史和数学教育的杂志

## 參見

  - [數學學會列表](../Page/數學學會列表.md "wikilink")

{{-}}

[Category:數學組織](../Category/數學組織.md "wikilink")
[美国数学协会](../Category/美国数学协会.md "wikilink")
[Category:華盛頓哥倫比亞特區組織](../Category/華盛頓哥倫比亞特區組織.md "wikilink")
[Category:1915年建立的組織](../Category/1915年建立的組織.md "wikilink")

1.