**美国联邦法院**（Federal judiciary of the United
States）是根據[美國憲法和](../Page/美國憲法.md "wikilink")[美國法律成立的](../Page/美國法律.md "wikilink")[法院](../Page/法院.md "wikilink")。其中[美國憲法只指明要成立](../Page/美國憲法.md "wikilink")[最高法院](../Page/美國最高法院.md "wikilink")，其餘法院由[美國國會授權成立](../Page/美國國會.md "wikilink")。

美国联邦法院是[美國聯邦政府的一部份](../Page/美國聯邦政府.md "wikilink")，可分為[普通法院和](../Page/普通法院.md "wikilink")[專門法院](../Page/專門法院.md "wikilink")。普通法院分為三级，从下到上分别是：

  - [地方法院](../Page/美国联邦地方法院.md "wikilink")
  - [上诉法院](../Page/美国联邦上诉法院.md "wikilink")（除了[美国联邦巡回区上诉法院](../Page/美国联邦巡回区上诉法院.md "wikilink")）
  - [最高法院](../Page/美国最高法院.md "wikilink")

專門法院有：

  - [破產法庭](../Page/美國破產法庭.md "wikilink")

  -
  - [國際貿易法庭](../Page/美國國際貿易法庭.md "wikilink")

  - [美国联邦权利申诉法院](../Page/美国联邦权利申诉法院.md "wikilink")

  - [美国軍事上诉法院](../Page/美国軍事上诉法院.md "wikilink")

  - [美国联邦巡回区上诉法院](../Page/美国联邦巡回区上诉法院.md "wikilink")

  - [美国退伍军人权利上诉法院](../Page/美国退伍军人权利上诉法院.md "wikilink")

附属机构有：

  - [联邦司法中心](../Page/联邦司法中心.md "wikilink")
  - [美国裁决委员会](../Page/美国裁决委员会.md "wikilink")

## 延伸閱讀

  - [Federal Court Concepts, Georgia
    Tech](http://www.catea.gatech.edu/grade/legal/)
  - [Creating the Federal Judicial
    System](https://www.fjc.gov/content/creating-federal-judicial-system-third-edition-0)
  - [Debates on the Federal Judiciary: A Documentary
    History](https://purl.fdlp.gov/GPO/gpo41680)
  - [History of the Courts of the Federal
    Judiciary](http://www.fjc.gov/history/home.nsf)
  - [CourtWEB, Online Federal Court Opinions Information
    System](http://courtweb.pamd.uscourts.gov/courtweb/CourtWeb.aspx)

## 参见

  - [美国联邦法官](../Page/美国联邦法官.md "wikilink")

  - [美國法院](../Page/美國法院.md "wikilink")

  -
## 外部連結

  -
[美国联邦法院](../Category/美国联邦法院.md "wikilink")