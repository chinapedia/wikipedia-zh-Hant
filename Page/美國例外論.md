**美國例外論**（），又譯**美國卓異主義**、**美國例外主義**、**美式例外主義**，一種理論與意識形態，認為[美利堅合眾國是個獨特的國家](../Page/美利堅合眾國.md "wikilink")，與其他國家完全不同。为[亞歷西斯·托克維里於](../Page/亞歷西斯·托克維里.md "wikilink")1831年所杜撰之詞句。有观点认为，美國例外論實質上已經成爲美國淩駕于[國際法之上](../Page/國際法.md "wikilink")，推行全球[霸權主義和](../Page/霸權主義.md "wikilink")[利己主義](../Page/利己主義.md "wikilink")，干涉其他國家内政的藉口和思想根源。\[1\]

美國例外論的概念為，美利堅合眾國與[美國人在世界上地位獨特](../Page/美國人.md "wikilink")，是[世界上第一個](../Page/世界.md "wikilink")、也是獨一無二，以[自由](../Page/自由.md "wikilink")、[個人主義](../Page/個人主義.md "wikilink")、法律面前人人[平等](../Page/平等.md "wikilink")、[自由放任](../Page/自由放任.md "wikilink")[資本主義等思想為建國基礎的](../Page/資本主義.md "wikilink")[國家](../Page/國家.md "wikilink")；[人民特別富裕幸福](../Page/人民.md "wikilink")，國家特別穩定強盛，並在世界上領導與保衛自由潮流，因此獨特優越，具有其他國家無可比擬之處；因其為人類提供機會與希望，由注重於人身與經濟自由的憲政理想所治理，衍生出獨一無二的公私利益平衡。美利堅合眾國有多項特徵在[政治學上獨一無二](../Page/政治學.md "wikilink")，如反專制反獨裁的傳統、[個人主義](../Page/個人主義.md "wikilink")、各[社會主義政黨不成氣候](../Page/社會主義.md "wikilink")、[美洲大陸與世界其他地區在地理上的區隔](../Page/美洲.md "wikilink")、受到[宗教](../Page/宗教.md "wikilink")——尤其是[基督教](../Page/基督教.md "wikilink")——大力影響。這些特徵和開發程度相近的[西歐與](../Page/西歐.md "wikilink")[斯堪的那維亞半島](../Page/斯堪的那維亞半島.md "wikilink")（[北歐](../Page/北歐.md "wikilink")）國家、[共產國家或影響](../Page/共產國家.md "wikilink")[拉丁美洲的](../Page/拉丁美洲.md "wikilink")[馬克思主義國家完全不同](../Page/馬克思主義.md "wikilink")。

## 在歷史上的來龍去脈

「美國例外主義」一詞通常意指在大眾文化中，用以善意解釋美國成功理由與方法的神話。此概念在本質上主張，對「自由戰勝暴政」的「審慎選擇」乃理所當然，亦為美國社會『成功』發展之核心理由。此一觀點承受若干異議，懷疑其不過是地域性社會民俗發展趨勢又一例證。依此觀點，美國例外主義為眾多民族優越運動之一。

以下列示此概念之起源，及其隨著美國之變遷而產生用法上之演變。

### 來自清教徒的根源

美國在[殖民地時期最早的意識形態為在](../Page/殖民地.md "wikilink")[新英格蘭墾殖的](../Page/新英格蘭.md "wikilink")[清教徒之](../Page/清教徒.md "wikilink")[新教教義](../Page/新教.md "wikilink")。許多清教徒，連同[阿米紐派教徒](../Page/阿米紐派教徒.md "wikilink")（Arminian），在嚴格的[宿命論與鬆散的](../Page/宿命.md "wikilink")[天意](../Page/天意.md "wikilink")（Divine
Providence）論中，保持中間立場。他們相信[上帝與其有約](../Page/上帝.md "wikilink")，並揀選他們領導地球上其餘國家。清教徒領袖之一，[約翰·溫梭普](../Page/約翰·溫梭普.md "wikilink")（John
Winthrop），將此理念表述於其[嶺上之都](../Page/嶺上之都.md "wikilink")（City on a
Hill）的修辭隱喻中─即新英格蘭的清教徒社會須成為世上其他國家之模範社區。這篇修辭隱喻廣為例外主義支持者所引述。

雖然新英格蘭地區的清教徒式世界觀其後戲劇性地改變，而與之歧異的基督教式傳統在殖民地中部與南部又極具權威，清教徒式深沉的[道學價值融入國家性格遠逾一世紀之久](../Page/道學.md "wikilink")；雖有爭議，但其價值觀可說綿延至今。雖然美國例外主義於今在性質上主要植基於[世俗](../Page/世俗.md "wikilink")，部份仍是源自美國清教徒之根本。[宗教右派](../Page/宗教右派.md "wikilink")，如[福音教派與承襲近似清教徒生活準則的](../Page/福音教派.md "wikilink")[基本教義派等](../Page/基本教義派.md "wikilink")，今為例外主義之主要支持者。

### 美國獨立革命與共和主義

[美國獨立革命為另一則廣受引述為美國例外主義發展史里程碑的事件](../Page/美國獨立革命.md "wikilink")。革命中的[知識份子們](../Page/知識份子.md "wikilink")（著[常識一書的](../Page/常識.md "wikilink")[湯瑪斯·潘恩可為代表](../Page/湯瑪斯·潘恩.md "wikilink")）首次表述美國並非歐洲之延伸，而是一個新天地，是一個潛力無限、機會無窮的國家，卻受發展瞠乎其後的不列顛母國所傷害。如此情懷引領鼓吹革命的高知識份子們懷想美國例外主義，緊密連繫[共和主義](../Page/共和主義.md "wikilink")─即主權在民，不在世襲的統治階級。

### 移民與可用資源

[亞歷西斯·托克維里到訪時](../Page/亞歷西斯·托克維里.md "wikilink")，美國正處於空前增長。19世紀時的美利堅合眾國，因其無限[移民政策](../Page/移民.md "wikilink")、地大物博以及土地獎勵（land
incentivization）方案，看似世無其匹。雖說這些多為久遠的過去，美國境內的公意通常仍將其與[愛國主義與](../Page/愛國主義.md "wikilink")[民族主義相結合](../Page/民族主義.md "wikilink")；許多人抱持觀念，本國今日之舉世無倫，來自過去之作為。其他國家，包含[澳大利亞與](../Page/澳大利亞.md "wikilink")[加拿大在內](../Page/加拿大.md "wikilink")，直至不具經濟利益時方纔實行無限移民政策，及獎勵土地探勘。

或有人將此措詞與民主黨於[傑克遜民主時期所用之](../Page/傑克遜民主.md "wikilink")[昭昭天命一詞相互連繫](../Page/昭昭天命.md "wikilink")。該詞語聲稱佔有北美洲全境為神賜之權利。

又或有人將此措詞與[唯物主義以及](../Page/唯物主義.md "wikilink")[消費主義相互連繫](../Page/消費主義.md "wikilink")。對抱持如此觀點者而言，廣博程度史無前例的自然資源，以及努力善用如此資源之熱情，為美國例外主義之根源。

### 在政治上的穩定性

許多人視美國自1789年開國以來，依[美國憲法於各地構建政府之事實為優異非凡](../Page/美國憲法.md "wikilink")。然而，這項事實亦可視之為例外主義『自身存在之解釋』（*explanation
for the existence*）。美國文教神話因缺乏與其他政府形式互動之經歷，無可避免地與本國政府相互聯繫。

### 冷戰

美國例外主義於[冷戰期間經](../Page/冷戰.md "wikilink")[大眾媒體廣為報導](../Page/大眾媒體.md "wikilink")，成為[美式生活方式所代表的](../Page/美式生活方式.md "wikilink")[自由與](../Page/自由.md "wikilink")[共產主義](../Page/共產主義.md "wikilink")[暴政之爭](../Page/暴政.md "wikilink")。曾用以區分美國與19世紀歐洲列強之屬性，其殘留之情懷，乃轉用於區分（由美國領導的）資本主義民主政體與共產國家。

## 美國例外主義之論戰

信服美國例外主義者論斷，在許多方面，美利堅合眾國不僅與其所從出之歐洲世界大有不同，亦與全球其他國家相異；而美國例外主義一詞並非暗指美國之優越性，這點對其十分重要。例如說，美國最具特色的表徵包含了奴隸制度之遺留，以及南方的種族隔離。V.O.
Key在其《南方的政府與民族政策》（*Southern Politics in State and
Nation*，1951）中論說，美國整個南方的政策即使在美式體制中亦屬『卓然有異』。其論述僅在於標明奴隸制度之遺留，而非讚許。

### 共和政體的性格與國家地位之概念

美國例外主義支持者們論斷，美利堅合眾國之獨特性在於其為一系列[共和主義之理念所形塑而成](../Page/共和主義.md "wikilink")，非由共有傳承之種族，或-{zh-hans:[精英](../Page/精英主義.md "wikilink");zh-hant:[精英](../Page/精英主義.md "wikilink");zh-tw:[菁英](../Page/精英主義.md "wikilink");}-統治者們奠基。[亞伯拉罕·林肯總統於其](../Page/亞伯拉罕·林肯.md "wikilink")[蓋茨堡演說中表述](../Page/蓋茨堡演說.md "wikilink")，美國『於自由中孕育，並致力於凡人生而平等之念。』依此觀點，身為美國人無可避免須愛護並捍衛自由平等。如此這般，美國於境外廣為推行這些理念，尤其是在兩次世界大戰與[冷戰期間](../Page/冷戰.md "wikilink")。批評家論斷，美國於這些衝突時期的各項政策多受經濟上與軍事上之自私自利所鼓動；大多數觀察家則承認，理想主義性格與自私自利同時存在，而程度互有不同。實際上，美國雖可說是第一個，卻非唯一一個，由諸般崇高理想所形塑之共和政體，[巴西與](../Page/巴西.md "wikilink")[法蘭西共和國可為例證](../Page/法蘭西共和國.md "wikilink")。

始自[聯邦主義體系](../Page/聯邦主義.md "wikilink")，以及[分權與制衡](../Page/分權與制衡.md "wikilink")（checks
and
balances），為美利堅合眾國國體特徵。分權制衡旨在防止任何個人、團體、地區、或政府部門變得太有權勢。若干美國例外主義者論斷，此種不信任集權的體系，連同其附屬系統，在于防止美國承受[多數暴力](../Page/多數暴力.md "wikilink")，使美國公民得以生活在一個律法反映公民價值之地；其結果使全國各地的法律處處不同。與全國性的價值觀相較，若干州的州法可能較為激進，而另外若干州又可能更為保守。例如，在[美國最高法院於](../Page/美國最高法院.md "wikilink")2003年對雞姦除罪化，影響遍及全國之前，持[自由主義的](../Page/自由主義.md "wikilink")[佛蒙特州便領先全國](../Page/佛蒙特州.md "wikilink")，將[公民結合](../Page/公民結合.md "wikilink")（即同性戀婚姻）合法化。批評者則一向認為，此種體系不過是將國家凌駕於各州的權力轉為以州府的權力凌駕於下轄的區域性實體。為求平衡，美國的政治體制（帶有疑義地）容許區域性統治，而制止[單一體制國家](../Page/單一體制國家.md "wikilink")（unitary
state）所行的中央統治。

### 機會與-{zh-hans:精英;zh-hant:精英;zh-tw:菁英;}-階級

美利堅合眾國的綽號為『機會之地』（Land of
Opportunity），其社會層級不像他國嚴密死板，亦無[貴族體制](../Page/貴族.md "wikilink")。美國人傾向於相信，強力的[工作倫理與堅毅的個人性格才是成功關鍵](../Page/工作倫理.md "wikilink")，而非出身高貴，或交對了朋友。

### 政治權利

一般共通看法為，美利堅合眾國之獨一無二在於其肇建伊始即確保國民之[公民權](../Page/公民權.md "wikilink")─如[言論自由](../Page/言論自由.md "wikilink")、[投票權](../Page/投票權.md "wikilink")、[無罪推定等](../Page/無罪推定.md "wikilink")，以及對這些公民權之尊重，為美國政治文化中獨有的堅實要素。批評家則論斷，這些公民權來自英格蘭，後擴及聯合王國全境，終於廣至全大英帝國，而與美利堅合眾國之所有並無不同；美國不過將這些權利以突出自身優越性的一紙文書合併。實際上，雖有爭議，美國在實行這些公民權與政治權利上落後於其他西方民主國家；而美國憲法，即使到了今天，亦未包含投票權。參見[選舉權](../Page/選舉權.md "wikilink")。

### 開疆拓土的精神（Frontier spirit）

美國例外主義之支持者們多主張，開拓者們（追隨[斐德列克·透納](../Page/斐德列克·透納.md "wikilink")（Frederick
Jackson Turner）之[開拓論點](../Page/開拓論點.md "wikilink")（Frontier
Thesis））創出了『美國精神』或『美國認同』，國家民族的活力由粗礪的環境條件中誕生。其他邊界綿長的國家，如[俄國](../Page/俄國.md "wikilink")、加拿大與澳大利亞，並不容許個別的拓荒先驅者定區於邊境，亦未經歷相同的心理衝擊與文化衝突。

### 美國獨立革命

[美國獨立戰爭為例外主義者們之意識型態領域](../Page/美國獨立戰爭.md "wikilink")。獨立革命中的知識份子們，如[湯瑪斯·潘恩](../Page/湯瑪斯·潘恩.md "wikilink")，爭議性地將美國形塑為與其歐洲祖先根本不同的國家，創建出世人所知的君主立憲跟民主共和這兩個型態的現代[自由民主體制](../Page/自由民主.md "wikilink")。其他國家之革命則無此特性─英國的革命（[英國內戰](../Page/英國內戰.md "wikilink")）發生於美國獨立革命之前一個世紀，其結果產生了[君主立憲](../Page/君主立憲.md "wikilink")。[法國大革命則爭議性地帶來現代自由民主體制其中之一的](../Page/法國大革命.md "wikilink")(民主共和體制)。

## 參見

  - [民族優越感](../Page/民族優越感.md "wikilink")
  - [沙文主義](../Page/沙文主義.md "wikilink")
  - [白人的负担](../Page/白人的负担.md "wikilink")
  - [昭昭天命](../Page/昭昭天命.md "wikilink")
  - [反美主義](../Page/反美主義.md "wikilink")
  - [價值觀外交](../Page/價值觀外交.md "wikilink")

## 參考资料

## 擴展閱讀

  -
  -
  -
  -
  - Byron E. Shafer, *\* Is America Different? : A New Look at American
    Exceptionalism* (1991)

**例外主義與美國勞工運動**

:\* Glickstein, Jonathan A. *American Exceptionalism, American Anxiety:
Wages, Competition, And Degraded Labor In The Antebellum United States*
(2002)

:\**Against Exceptionalism: Class Consciousness and the American Labor
Movement, 1790-1820*, 26 Int'l Lab. & Working Class History 1 (1984)

:\* [Voss, Kim. *The Making of American Exceptionalism: The Knights of
Labor and Class Formation in the Nineteenth
Century*（1993）](http://www.questia.com/PM.qst?a=o&d=103669154)

**歐美之別**

:\*

**美國的"邊境焦慮"**

:\*

:\*

  - [additional online scholarly
    sources](http://www.questia.com/library/history/united-states-history/american-exceptionalism.jsp)

## 外部連結

  - [美式教條：是否至關緊要？應該改變嗎？](https://web.archive.org/web/20040803210716/http://www.foreignaffairs.org/19960301fareviewessay4193/michael-lind/the-american-creed-does-it-matter-should-it-change.html)
    摘要：[Seymour Martin
    Lipset](../Page/Seymour_Martin_Lipset.md "wikilink")
    解釋美利堅合眾國何以卓異。Michael J. Sandel
    則以國家性的病態行為讉責利己主義的傳統，並說美國應回到新英格蘭城鎮。美國既非卓異，也不必回來。
    [1](https://web.archive.org/web/20040803210716/http://www.foreignaffairs.org/19960301fareviewessay4193/michael-lind/the-american-creed-does-it-matter-should-it-change.html)
  - [有權與眾不同](https://web.archive.org/web/20060520055837/http://www.opendemocracy.net/debates/article-3-115-2032.jsp)
    [Grover Norquist](../Page/Grover_Norquist.md "wikilink") 與 [Will
    Hutton](../Page/Will_Hutton.md "wikilink") 激辯。
  - [美式清教徒主義之宗教特徵](https://web.archive.org/web/20061013164919/http://www.holysmoke.org/sdhok/hum12.htm)
    By [美國人權組織](../Page/美國人權組織.md "wikilink")（American Humanist
    Association）執行董事，[The Humanist](../Page/The_Humanist.md "wikilink")
    總編，[Frederick Edwords](../Page/Frederick_Edwords.md "wikilink") 著

[Category:美国政治哲学](../Category/美国政治哲学.md "wikilink")
[Category:美國文化](../Category/美國文化.md "wikilink")
[Category:反美情緒](../Category/反美情緒.md "wikilink")
[Category:民族優越感](../Category/民族優越感.md "wikilink")
[Category:美國外交](../Category/美國外交.md "wikilink")
[Category:历史理论](../Category/历史理论.md "wikilink")
[Category:政治理論](../Category/政治理論.md "wikilink")

1.  *Foreword: on American Exceptionalism; Symposium on Treaties,
    Enforcement, and U.S. Sovereignty*, Stanford Law Review, 2003-05-01,
    Pg. 1479