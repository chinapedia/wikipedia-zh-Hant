根据人口密度的大小顺序将[美国](../Page/美国.md "wikilink")50个州依次顺序排列。表中的数字的单位是人/每平方千米。数据的来源是[美国人口调查局](../Page/美国人口调查局.md "wikilink")2000年的统计。
[Map_of_states_showing_population_density_in_2013.svg](https://zh.wikipedia.org/wiki/File:Map_of_states_showing_population_density_in_2013.svg "fig:Map_of_states_showing_population_density_in_2013.svg")

|        |                                        |          |
| ------ | -------------------------------------- | -------- |
| **名次** | **州别**                                 | **人口密度** |
| 1      | [紐泽西州](../Page/紐泽西州.md "wikilink")     | 438.00   |
| 2      | [罗德岛州](../Page/罗德岛州.md "wikilink")     | 387.35   |
| 3      | [马萨诸塞州](../Page/马萨诸塞州.md "wikilink")   | 312.68   |
| 4      | [康乃狄克州](../Page/康乃狄克州.md "wikilink")   | 271.40   |
| 5      | [马里兰州](../Page/马里兰州.md "wikilink")     | 209.23   |
| 6      | [纽约州](../Page/纽约州.md "wikilink")       | 155.18   |
| 7      | [德拉瓦州](../Page/德拉瓦州.md "wikilink")     | 154.87   |
| 8      | [佛罗里达州](../Page/佛罗里达州.md "wikilink")   | 114.43   |
| 9      | [俄亥俄州](../Page/俄亥俄州.md "wikilink")     | 107.05   |
| 10     | [宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink") | 105.80   |
| 11     | [伊利诺州](../Page/伊利诺州.md "wikilink")     | 86.27    |
| 12     | [加利福尼亚州](../Page/加利福尼亚州.md "wikilink") | 83.85    |
| 13     | [夏威夷州](../Page/夏威夷州.md "wikilink")     | 72.83    |
| 14     | [弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")   | 69.03    |
| 15     | [密歇根州](../Page/密歇根州.md "wikilink")     | 67.55    |
| 16     | [印第安纳州](../Page/印第安纳州.md "wikilink")   | 65.46    |
| 17     | [北卡罗莱纳州](../Page/北卡罗莱纳州.md "wikilink") | 63.80    |
| 18     | [佐治亚州](../Page/佐治亚州.md "wikilink")     | 54.59    |
| 19     | [田纳西州](../Page/田纳西州.md "wikilink")     | 53.29    |
| 20     | [新罕布什尔州](../Page/新罕布什尔州.md "wikilink") | 53.20    |
| 21     | [南卡罗莱纳州](../Page/南卡罗莱纳州.md "wikilink") | 51.45    |
| 22     | [路易斯安那州](../Page/路易斯安那州.md "wikilink") | 39.61    |
| 23     | [肯塔基州](../Page/肯塔基州.md "wikilink")     | 39.28    |
| 24     | [威斯康辛州](../Page/威斯康辛州.md "wikilink")   | 38.13    |
| 25     | [华盛顿州](../Page/华盛顿州.md "wikilink")     | 34.20    |
| 26     | [阿拉巴马州](../Page/阿拉巴马州.md "wikilink")   | 33.84    |
| 27     | [密苏里州](../Page/密苏里州.md "wikilink")     | 31.36    |
| 28     | [德克萨斯州](../Page/德克萨斯州.md "wikilink")   | 30.75    |
| 29     | [西维吉尼亚州](../Page/西维吉尼亚州.md "wikilink") | 29.00    |
| 30     | [佛蒙特州](../Page/佛蒙特州.md "wikilink")     | 25.41    |
| 31     | [明尼苏达州](../Page/明尼苏达州.md "wikilink")   | 23.86    |
| 32     | [密西西比州](../Page/密西西比州.md "wikilink")   | 23.42    |
| 33     | [爱荷华州](../Page/爱荷华州.md "wikilink")     | 20.22    |
| 34     | [阿肯色州](../Page/阿肯色州.md "wikilink")     | 19.82    |
| 35     | [奥克拉荷马州](../Page/奥克拉荷马州.md "wikilink") | 19.40    |
| 36     | [亚利桑那州](../Page/亚利桑那州.md "wikilink")   | 17.43    |
| 37     | [科罗拉多州](../Page/科罗拉多州.md "wikilink")   | 16.01    |
| 38     | [缅因州](../Page/缅因州.md "wikilink")       | 15.95    |
| 39     | [俄勒冈州](../Page/俄勒冈州.md "wikilink")     | 13.76    |
| 40     | [堪萨斯州](../Page/堪萨斯州.md "wikilink")     | 12.69    |
| 41     | [犹他州](../Page/犹他州.md "wikilink")       | 10.50    |
| 42     | [内布拉斯加州](../Page/内布拉斯加州.md "wikilink") | 8.60     |
| 43     | [内华达州](../Page/内华达州.md "wikilink")     | 7.03     |
| 44     | [爱德荷州](../Page/爱德荷州.md "wikilink")     | 6.04     |
| 45     | [新墨西哥州](../Page/新墨西哥州.md "wikilink")   | 5.79     |
| 46     | [南达科他州](../Page/南达科他州.md "wikilink")   | 3.84     |
| 47     | [北达科他州](../Page/北达科他州.md "wikilink")   | 3.59     |
| 48     | [蒙大拿州](../Page/蒙大拿州.md "wikilink")     | 2.39     |
| 49     | [怀俄明州](../Page/怀俄明州.md "wikilink")     | 1.96     |
| 50     | [阿拉斯加州](../Page/阿拉斯加州.md "wikilink")   | 0.42     |

[人口密度](../Category/美國各州相關列表.md "wikilink")
[Category:美國人口](../Category/美國人口.md "wikilink")
[Category:人口列表](../Category/人口列表.md "wikilink")