以下是[美國各](../Page/美國.md "wikilink")[州的州](../Page/美國州份.md "wikilink")[議會大廈列表](../Page/議會.md "wikilink")。

| 州府                                              | 議會大廈                                           | 建造年份                                                                                                                |
| ----------------------------------------------- | ---------------------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| [蒙哥馬利](../Page/蒙哥馬利_\(阿拉巴馬州\).md "wikilink")    | [阿拉巴馬州議會大廈](../Page/阿拉巴馬州議會大廈.md "wikilink")   | 1851年                                                                                                               |
| [朱諾](../Page/朱諾_\(阿拉斯加\).md "wikilink")         | [阿拉斯加州議會大廈](../Page/阿拉斯加州議會大廈.md "wikilink")   | 1929年－1931年                                                                                                         |
| [鳳凰城](../Page/鳳凰城_\(亞利桑那州\).md "wikilink")      | [亞利桑那州議會大廈](../Page/亞利桑那州議會大廈.md "wikilink")   | 1899年－1900年                                                                                                         |
| [小岩城](../Page/小岩城_\(阿肯色州\).md "wikilink")       | [阿肯色州議會大廈](../Page/阿肯色州議會大廈.md "wikilink")     | 1895年－1915年                                                                                                         |
| [沙加緬度](../Page/沙加緬度_\(加利福尼亞州\).md "wikilink")   | [加利福尼亞州議會大廈](../Page/加利福尼亞州議會大廈.md "wikilink") | 1860年－1874年                                                                                                         |
| [丹佛](../Page/丹佛_\(科羅拉多州\).md "wikilink")        | [科羅拉多州議會大廈](../Page/科羅拉多州議會大廈.md "wikilink")   | 1886年－1907年                                                                                                         |
| [哈特福](../Page/哈特福_\(康乃狄克州\).md "wikilink")      | [康乃狄克州議會大廈](../Page/康乃狄克州議會大廈.md "wikilink")   | 1872年－1879年                                                                                                         |
| [多佛](../Page/多佛_\(特拉华州\).md "wikilink")         | [德拉瓦州議會大廈](../Page/德拉瓦州議會大廈.md "wikilink")     | 1933年                                                                                                               |
| [塔拉哈西](../Page/塔拉哈西_\(佛羅里達州\).md "wikilink")    | [佛羅里達州議會大廈](../Page/佛羅里達州議會大廈.md "wikilink")   | 1973年－1977年                                                                                                         |
| [亞特蘭大](../Page/亞特蘭大_\(喬治亞州\).md "wikilink")     | [喬治亞州議會大廈](../Page/喬治亞州議會大廈.md "wikilink")     | 1883年－1889年                                                                                                         |
| [檀香山](../Page/檀香山.md "wikilink")                | [夏威夷州議會大廈](../Page/夏威夷州議會大廈.md "wikilink")     | 1960年－1969年                                                                                                         |
| [博伊西](../Page/博伊西.md "wikilink")                | [愛達荷州議會大廈](../Page/愛達荷州議會大廈.md "wikilink")     | 1905年－1913年, 1919年－1920年 (增建側翼)                                                                                     |
| [斯普林菲爾德](../Page/斯普林菲爾德_\(伊利諾州\).md "wikilink") | [伊利諾州議會大廈](../Page/伊利諾州議會大廈.md "wikilink")     | 1867年－1876年 (設計), 1884年－1887年 (建造)                                                                                  |
| [印第安納波利斯](../Page/印第安納波利斯.md "wikilink")        | [印第安納州議會大廈](../Page/印第安納州議會大廈.md "wikilink")   | 1877年－1887年[1](https://web.archive.org/web/20060824053803/http://www.in.gov/statehouse/tour/WebPageRenovation1.pdf) |
| [德梅因](../Page/德梅因_\(愛荷華州\).md "wikilink")       | [愛荷華州議會大廈](../Page/愛荷華州議會大廈.md "wikilink")     | 1870年－1886年                                                                                                         |
| [托皮卡](../Page/托皮卡_\(堪薩斯州\).md "wikilink")       | [堪薩斯州議會大廈](../Page/堪薩斯州議會大廈.md "wikilink")     | 1866年－1873年 (東側翼), 1879－[1881](../Page/1881.md "wikilink") (西側翼), 1884年－1906年 (中央)                                  |
| [法蘭克福](../Page/法蘭克福_\(肯塔基州\).md "wikilink")     | [肯塔基州議會大廈](../Page/肯塔基州議會大廈.md "wikilink")     | 1905年－1910年                                                                                                         |
| [巴頓魯治](../Page/巴頓魯治_\(路易西安納州\).md "wikilink")   | [路易斯安那州議會大廈](../Page/路易斯安那州議會大廈.md "wikilink") | 1930年－1932年                                                                                                         |
| [奧古斯塔](../Page/奧古斯塔_\(緬因州\).md "wikilink")      | [緬因州議會大廈](../Page/緬因州議會大廈.md "wikilink")       | 1828年－1832年, 1889年－1891年 (增建), 1909年－1911年 (增建)                                                                     |
| [安那波利斯](../Page/安那波利斯_\(馬里蘭州\).md "wikilink")   | [馬里蘭州議會大廈](../Page/馬里蘭州議會大廈.md "wikilink")     | 1772年－1779年                                                                                                         |
| [波士頓](../Page/波士頓.md "wikilink")                | [马萨諸塞州議會大廈](../Page/马萨諸塞州議會大廈.md "wikilink")   | 1795年－1798年                                                                                                         |
| [蘭辛](../Page/蘭辛_\(密西根州\).md "wikilink")         | [密西根州議會大廈](../Page/密西根州議會大廈.md "wikilink")     | 1871年－1878年                                                                                                         |
| [聖保羅](../Page/聖保羅_\(明尼蘇達州\).md "wikilink")      | [明尼蘇達州議會大廈](../Page/明尼蘇達州議會大廈.md "wikilink")   | 1893年－1905年                                                                                                         |
| [傑克遜](../Page/傑克遜_\(密西西比州\).md "wikilink")      | [密西西比州議會大廈](../Page/密西西比州議會大廈.md "wikilink")   | 1901年－1903年                                                                                                         |
| [傑佛遜市](../Page/傑佛遜市.md "wikilink")              | [密蘇里州議會大廈](../Page/密蘇里州議會大廈.md "wikilink")     | 1911年－1917年                                                                                                         |
| [海倫娜](../Page/海倫娜_\(蒙大拿州\).md "wikilink")       | [蒙大拿州議會大廈](../Page/蒙大拿州議會大廈.md "wikilink")     | 1896年－1902年, 1909年－1912年 (側翼增建)                                                                                     |
| [林肯](../Page/林肯_\(內布拉斯加州\).md "wikilink")       | [內布拉斯加州議會大廈](../Page/內布拉斯加州議會大廈.md "wikilink") | 1919年－1932年                                                                                                         |
| [卡森城](../Page/卡森城_\(內華達州\).md "wikilink")       | [內華達州議會大廈](../Page/內華達州議會大廈.md "wikilink")     | 1869年－1871年                                                                                                         |
| [康科德](../Page/康科德_\(新罕布夏州\).md "wikilink")      | [新罕布夏州議會大廈](../Page/新罕布夏州議會大廈.md "wikilink")   | 1815年－1818年                                                                                                         |
| [翠頓](../Page/翠頓_\(紐澤西州\).md "wikilink")         | [紐澤西州議會大廈](../Page/紐澤西州議會大廈.md "wikilink")     | 1792年                                                                                                               |
| [聖大非](../Page/聖大非_\(新墨西哥州\).md "wikilink")      | [新墨西哥州議會大廈](../Page/新墨西哥州議會大廈.md "wikilink")   | 1964年－1966年                                                                                                         |
| [奧本尼](../Page/奧本尼_\(紐約州\).md "wikilink")        | [紐約州議會大廈](../Page/紐約州議會大廈.md "wikilink")       | 1867年－1875年                                                                                                         |
| [羅利](../Page/羅利_\(北卡羅來納州\).md "wikilink")       | [北卡羅來納州議會大廈](../Page/北卡羅來納州議會大廈.md "wikilink") | 1833年 (Completed 1840 per [North Carolina](../Page/North_Carolina.md "wikilink") page)                              |
| [俾斯麥](../Page/俾斯麥_\(北達科他州\).md "wikilink")      | [北達科他州議會大廈](../Page/北達科他州議會大廈.md "wikilink")   | 1920年－1924年, 1931年－1934年 (辦公大樓及側翼)                                                                                  |
| [哥倫布](../Page/哥倫布_\(俄亥俄州\).md "wikilink")       | [俄亥俄州議會大廈](../Page/俄亥俄州議會大廈.md "wikilink")     | 1838年－1840年                                                                                                         |
| [奧克拉荷馬市](../Page/奧克拉荷馬市.md "wikilink")          | [奧克拉荷馬州議會大廈](../Page/奧克拉荷馬州議會大廈.md "wikilink") | 1914年－1917年, 2000年-2002年 ([圓頂](../Page/圓頂.md "wikilink"))                                                           |
| [塞勒姆](../Page/塞勒姆_\(俄勒冈州\).md "wikilink")       | [俄勒冈州议会大厦](../Page/俄勒冈州议会大厦.md "wikilink")     | 1935年，1977年（側翼）                                                                                                     |
| [哈里斯堡](../Page/哈里斯堡.md "wikilink")              | [賓夕法尼亞州議會大廈](../Page/賓夕法尼亞州議會大廈.md "wikilink") | 1898年－1902年/1903年                                                                                                   |
| [普洛威頓斯](../Page/普洛威頓斯.md "wikilink")            | [羅德島州議會大廈](../Page/羅德島州議會大廈.md "wikilink")     | 1895年－1904年                                                                                                         |
| [哥倫比亞](../Page/哥倫比亞_\(南卡羅來納州\).md "wikilink")   | [南卡羅來納州議會大廈](../Page/南卡羅來納州議會大廈.md "wikilink") | 1854年－1865年                                                                                                         |
| [皮爾](../Page/皮爾_\(南達科他州\).md "wikilink")        | [南達科他州議會大廈](../Page/南達科他州議會大廈.md "wikilink")   | 1905年－1911年                                                                                                         |
| [納西維爾](../Page/納西維爾_\(田納西州\).md "wikilink")     | [田納西州議會大廈](../Page/田納西州議會大廈.md "wikilink")     | 1845年－1854年                                                                                                         |
| [奧斯汀](../Page/奧斯汀_\(德克薩斯州\).md "wikilink")      | [德克薩斯州議會大廈](../Page/德克薩斯州議會大廈.md "wikilink")   | 1881年－1888年                                                                                                         |
| [鹽湖城](../Page/鹽湖城.md "wikilink")                | [猶他州議會大廈](../Page/猶他州議會大廈.md "wikilink")       | 1911年－1916年                                                                                                         |
| [蒙特佩利爾](../Page/蒙特佩利爾_\(佛蒙特州\).md "wikilink")   | [佛蒙特州議會大廈](../Page/佛蒙特州議會大廈.md "wikilink")     | 1834年－1836年                                                                                                         |
| [里奇蒙](../Page/里奇蒙_\(維吉尼亞州\).md "wikilink")      | [維吉尼亞州議會大廈](../Page/維吉尼亞州議會大廈.md "wikilink")   | 1785年－1790年, 1904年－1906年 (側翼)                                                                                       |
| [奧林匹亞](../Page/奧林匹亞_\(華盛頓州\).md "wikilink")     | [華盛頓州議會大廈](../Page/華盛頓州議會大廈.md "wikilink")     | 1919年－1928年 (議會大樓)                                                                                                  |
| [查爾斯頓](../Page/查爾斯頓_\(西維吉尼亞州\).md "wikilink")   | [西維吉尼亞州議會大廈](../Page/西維吉尼亞州議會大廈.md "wikilink") | 1924年－1932年                                                                                                         |
| [麥迪遜](../Page/麥迪遜_\(威斯康辛州\).md "wikilink")      | [威斯康辛州議會大廈](../Page/威斯康辛州議會大廈.md "wikilink")   | 1906年－1917年                                                                                                         |
| [夏延](../Page/夏延_\(懷俄明州\).md "wikilink")         | [懷俄明州議會大廈](../Page/懷俄明州議會大廈.md "wikilink")     | 1886年－1890年, 1915年－1917年 (參議院及眾議院建築)                                                                                |

## 各州州議會大廈圖像

<File:Sacramento_Capitol_2013.jpg>|

<center>

[加利福尼亚州](../Page/加利福尼亞州議會大廈.md "wikilink")

</center>

<File:Colo> state capitol.jpg|

<center>

[科羅拉多州](../Page/科羅拉多州議會大廈.md "wikilink")

</center>

<File:Connecticut> State Capitol, February 24, 2008.jpg|

<center>

[康乃狄克州](../Page/康乃狄克州議會大廈.md "wikilink")

</center>

<File:Tallahassee> Old and New Capitols 3.jpg|

<center>

[佛羅里達州](../Page/佛羅里達州議會大廈.md "wikilink")

</center>

<File:GeorgiaCapitol01.jpg>|

<center>

[喬治亞州](../Page/喬治亞州議會大廈.md "wikilink")

</center>

<File:Boise-State-Capitol0601.jpg>|

<center>

[愛達荷州](../Page/愛達荷州議會大廈.md "wikilink")

</center>

<File:Illinoiscapitol2.jpg>|

<center>

[伊利諾州](../Page/伊利諾州議會大廈.md "wikilink")

</center>

<File:Indiana> State Capitol rect pano.jpg|

<center>

[印第安納州](../Page/印第安納州議會大廈.md "wikilink")

</center>

<File:Iowa> capitol.jpg|

<center>

[愛荷華州](../Page/愛荷華州議會大廈.md "wikilink")

</center>

<File:Topeka> capital.jpg|

<center>

[堪薩斯州](../Page/堪薩斯州議會大廈.md "wikilink")

</center>

<File:KY_State_Capitol.jpg>|

<center>

[肯塔基州](../Page/肯塔基州議會大廈.md "wikilink")

</center>

<File:Louisiana> State Capitol.jpg|

<center>

[路易斯安那州](../Page/路易斯安那州議會大廈.md "wikilink")

</center>

<File:Maine> state capitol.jpg|

<center>

[緬因州](../Page/緬因州議會大廈.md "wikilink")

</center>

<File:MassStateHouse.jpg>|

<center>

[麻塞諸塞州](../Page/马萨諸塞州議會大廈.md "wikilink")

</center>

<File:Minnesota> State Capitol.jpg|

<center>

[明尼蘇達州](../Page/明尼蘇達州議會大廈.md "wikilink")

</center>

<File:MissouriCapitol.jpg>|

<center>

[密蘇里州](../Page/密蘇里州議會大廈.md "wikilink")

</center>

<File:Helena_capitol.jpg>|

<center>

[蒙大拿州](../Page/蒙大拿州議會大廈.md "wikilink")

</center>

<File:NavadaCapitolCarsonCity.jpg>|

<center>

[內華達州](../Page/內華達州議會大廈.md "wikilink")

</center>

<File:NewMexicoCapitolSantaFe.jpg>|

<center>

[新墨西哥州](../Page/新墨西哥州議會大廈.md "wikilink")

</center>

<File:NDCapitol.jpg>|

<center>

[北達科他州](../Page/北達科他州議會大廈.md "wikilink")

</center>

<File:NYSCapitolPanorama.jpg>|

<center>

[紐約州](../Page/紐約州議會大廈.md "wikilink")

</center>

<File:Oregon> State Capitol.jpg|

<center>

[奧勒岡州](../Page/奧勒岡州議會大廈.md "wikilink")

</center>

<File:Oklahoma> State Capitol.jpg|

<center>

[奧克拉荷馬州](../Page/奧克拉荷馬州議會大廈.md "wikilink")

</center>

<File:PAState> Capitol Back a Bit.JPG|

<center>

[賓夕法尼亞州](../Page/賓夕法尼亞州議會大廈.md "wikilink")

</center>

<File:Rhode> Island State Capitol (north facade).jpg|

<center>

[羅德島州](../Page/羅德島州議會大廈.md "wikilink")

</center>

<File:SCCapitol0270.jpg>|

<center>

[南卡羅來納州](../Page/南卡羅來納州議會大廈.md "wikilink")

</center>

<File:PierreSD> Capitol.jpg|

<center>

[南達科他州](../Page/南達科他州議會大廈.md "wikilink")

</center>

<File:Texas> capitol day.jpg|

<center>

[德克薩斯州](../Page/德克薩斯州議會大廈.md "wikilink")

</center>

<File:Slccapitol.jpg>|

<center>

[猶他州](../Page/猶他州議會大廈.md "wikilink")

</center>

<File:Montpelier_vermont_state_house_20.jpg>|

<center>

[佛蒙特州](../Page/佛蒙特州議會大廈.md "wikilink")

</center>

<File:Richmond> Virginia Capitol.jpg|

<center>

[維吉尼亞州](../Page/維吉尼亞州議會大廈.md "wikilink")

</center>

<File:Washington> State Capitol Legislative Building.jpg|

<center>

[華盛頓州](../Page/華盛頓州議會大廈.md "wikilink")

</center>

<File:Wisconsin> State Capitol.jpg|

<center>

[威斯康辛州](../Page/威斯康辛州議會大廈.md "wikilink")

</center>

<File:WV> Capitolbuilding.jpg|

<center>

[西維吉尼亞州](../Page/西維吉尼亞州議會大廈.md "wikilink")

</center>

|

<center>

[阿拉斯加州](../Page/阿拉斯加州議會大廈.md "wikilink")

</center>

<File:Hawaii> State Capitol, Honolulu.jpg|

<center>

[夏威夷州](../Page/夏威夷州議會大廈.md "wikilink")

</center>

<File:Mississippi> New State Capitol Building in Jackson.jpg|

<center>

[密西西比州](../Page/密西西比州議會大廈.md "wikilink")

</center>

<File:New> Jersey State House.jpg|

<center>

[紐澤西州](../Page/紐澤西州議會大廈.md "wikilink")

</center>

<File:Wyoming> Capitol Exterior.jpg|

<center>

[懷俄明州](../Page/懷俄明州議會大廈.md "wikilink")

</center>

<File:2006> 09 19 - Annapolis - Sunset over State House.JPG|

<center>

[馬里蘭州](../Page/馬里蘭州議會大廈.md "wikilink")

</center>

## 外部链接

  - [Cupolas of Capitalism State Capitol Building
    Histories](http://www.cupola.com/html/bldgstru/statecap/cap01.htm).
    *Cupola.com*.

[州](../Category/美國各州議會大廈.md "wikilink")
[州](../Category/美國各州相關列表.md "wikilink")