**國家航空諮詢委員會**（，縮寫：NACA）是於1915年3月3日成立的[美國聯邦政府機構](../Page/美國聯邦政府.md "wikilink")，負責[航空科學研究的執行](../Page/航空科學.md "wikilink")、促進與制度化。由於進入[太空競賽時代](../Page/太空競賽.md "wikilink")，美國聯邦政府於1958年10月1日解散NACA，並將其設備、資產與人員轉移至新創立的[國家航空暨太空總署](../Page/美国国家航空航天局.md "wikilink")。NACA的讀音是字母單獨發音，而不是[首字母縮略字的發音方式](../Page/首字母縮略字.md "wikilink")。

NACA的名字在汽車界與航空界仍然很常見，例如汽車的進氣導管有稱為NACA
duct（導管）的，或是飛機上常用的[NACA翼型](../Page/NACA翼型.md "wikilink")、NACA
cowling（整流罩）等仍被用於新設計上。

## 源起

[NACA_seal.jpg](https://zh.wikipedia.org/wiki/File:NACA_seal.jpg "fig:NACA_seal.jpg")
NACA開始於一個促進產官學界就戰爭相關計劃進行合作的緊急措施，並且參考了幾個歐洲國家的相似組織，例如法國（L'Établissement
Central de l'Aérostation Militaire，現在的Office National d'Études et de
Recherches Aérospatiales）、德國（Aerodynamical Laboratory of the University
of Göttingen）、或俄國（Aerodynamic Institute of
Koutchino），影響最大的則是連名字都幾乎一樣的英國「航空諮詢委員會」（Advisory
Committee for Aeronautics）。

1912年12月，時任美國總統的[威廉·霍华德·塔夫脱指派華盛頓](../Page/威廉·霍华德·塔夫脱.md "wikilink")[卡內基學會](../Page/安德鲁·卡耐基.md "wikilink")（Carnegie
Institution of Washington）總裁的Robert S. Woodward主持國家航空動力實驗室委員會（National
Aerodynamical Laboratory Commission），相關法規在1913年1月於參眾兩院提出，但在投票中失利而未獲核准。

Charles D.
Walcott──於1907年到1927年擔任[史密森尼学会會長](../Page/史密森尼学会.md "wikilink")──接收其成果，於1915年1月，參議員Benjamin
R. Tillman與眾議員Ernest W.
Roberts提出與Walcott的概述相同的方案。此一委員會的功能是「管理與指引飛行問題的科學研究並包含實務解決方案的看法，與判定必須實驗驗證、討論其結果及實際應用的問題」。時任助理海軍部長的[小羅斯福曾寫道他](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")「衷心地贊同」此一法案所根基的原則。之後Walcott提出加入海軍撥款書的建議。

NACA設立案通過差點錯過了加入1915年3月海軍撥款書追加條款，委員會的12人──均為無給──得到編列了每年5000元的預算。

[威爾遜總統在第](../Page/伍德罗·威尔逊.md "wikilink")63屆參議院的最後一天簽署了法案，國家諮詢委員會正式成立。參議院的說明書敘述：「……管理與指引飛行問題的科學研究並包含實務解決方案的看法……是國家航空諮詢委員會的任務……」

## NACA的研究

## 轉換為NASA

### 太空科技特別委員會

1957年11月21日，時任NACA主任的[Hugh
Dryden](../Page/Hugh_Dryden.md "wikilink")，成立了太空科技特別委員會。由於其主席[Guyford
Stever的緣故](../Page/Guyford_Stever.md "wikilink")，該委員會也被暱稱為「Stever委員會」，任務是在聯邦政府的各個部門、私營公司以及美國的大學和NACA的目標之間居中協調，利用各家的專長來開發太空計劃。

當時任職於[陸軍彈道飛彈署的](../Page/陸軍彈道飛彈署.md "wikilink")[華納·馮·布朗博士已經準備好於](../Page/華納·馮·布朗.md "wikilink")1956年將[木星-C探空火箭發射升空](../Page/木星-C探空火箭.md "wikilink")，而後卻又推遲；而當時的[蘇聯政府又將在](../Page/蘇聯.md "wikilink")1957年10月時發射[史普尼克一號衛星](../Page/史普尼克一號.md "wikilink")。

基於以上種種，Dryden在1958年1月14日發布了「國家太空科技研究計劃」，當中聲明：

## NASA諮詢協調會

1958年NASA成立後NACA也隨之解散，而其研究機構包括Ames研究中心、Lewis研究中心、蘭利航空實驗室等，也與美國陸軍及海軍等的一些單位一起併入新部門。1967年，國會指示NASA成立航太安全諮詢小組（Aerospace
Safety Advisory Panel，ASAP），以提供太空計劃的安全性與風險的評估建議。另外還有太空計劃諮詢會議（Space
Program Advisory Council）及研究與科技諮詢會議（Research and Technology Advisory
Council）等機構。1977年，這些機構全部整合為NASA諮詢委員會（NASA Advisory
Council，NAC），繼承了NACA的功能。 \[1\]

## NACA風洞列表

NACA的第一個[風洞在](../Page/風洞.md "wikilink")1920年6月11日正式落成於蘭利，這是現在許多著名的NACA與NASA風洞中的第一個。雖然這個風洞並不獨特或先進，它仍讓NACA的工程師與科學家們能夠發展與測試最新最先進的空氣動力學概念，並提昇未來的風洞設計。

1.  5ft [Atmospheric wind
    tunnel](../Page/Subsonic_and_transonic_wind_tunnel#Subsonic_tunnel.md "wikilink")
    (1920)
2.  Variable density wind tunnel (1922)
3.  Propeller research tunnel (1927)
4.  11in [High speed wind
    tunnel](../Page/Subsonic_and_transonic_wind_tunnel#Transonic_tunnel.md "wikilink")
    (1928)
5.  5ft [Vertical wind
    tunnel](../Page/Vertical_wind_tunnel.md "wikilink") (1929)
6.  7x10ft [Atmospheric wind
    tunnel](../Page/Subsonic_and_transonic_wind_tunnel#Subsonic_tunnel.md "wikilink")
    (1930)
7.  Full scale 30x60ft [Atmospheric wind
    tunnel](../Page/Subsonic_and_transonic_wind_tunnel#Subsonic_tunnel.md "wikilink")
    (1931)

## NACA主席列表

1.  [William F. Durand](../Page/William_F._Durand.md "wikilink")
    ([史丹福大學](../Page/史丹福大學.md "wikilink")) (1917–1918)
2.  [John R. Freeman](../Page/John_R._Freeman.md "wikilink")
    ([consultant](../Page/consultant.md "wikilink")) (1918–1919)
3.  [Charles Doolittle
    Walcott](../Page/Charles_Doolittle_Walcott.md "wikilink")
    ([Smithsonian
    Institution](../Page/Smithsonian_Institution.md "wikilink"))
    (1920–1927)
4.  [Joseph Sweetman Ames](../Page/Joseph_Sweetman_Ames.md "wikilink")
    ([Johns Hopkins
    University](../Page/Johns_Hopkins_University.md "wikilink"))
    (1927–1939)
5.  [Vannevar Bush](../Page/Vannevar_Bush.md "wikilink") ([Carnegie
    Institution](../Page/Carnegie_Institution_of_Washington.md "wikilink"))
    (1940–1941)
6.  [Jerome C. Hunsaker](../Page/Jerome_C._Hunsaker.md "wikilink")
    ([空軍](../Page/美國空軍.md "wikilink")、[麻省理工學院](../Page/麻省理工學院.md "wikilink"))
    (1941–1956)
7.  [James H. Doolittle](../Page/James_H._Doolittle.md "wikilink")
    ([硯殼石油](../Page/硯殼石油.md "wikilink")) (1957–1958)

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - The NASA Technical Reports Server provides access to a
    [collection](http://ntrs.nasa.gov/search.jsp?Ne=2&N=17&Ns=PublicationYear%7c0)
    of 14,469 NACA documents dating from 1917.
  - [U.S. Centennial of Flight Commission - The National Advisory
    Committee for Aeronautics
    (NACA)](https://web.archive.org/web/20080430115833/http://www.centennialofflight.gov/essay/Evolution_of_Technology/NACA/Tech1.htm)
  - [More information on NACA airfoil
    series](http://www.aerospaceweb.org/question/airfoils/q0041.shtml)
  - [From Engineering Science to Big Science - The NACA and NASA Collier
    Trophy Research Project Winners, Edited by Pamela E.
    Mack](http://history.nasa.gov/SP-4219/Contents.html)

## 延伸閱讀

  - [John Henry, et al. '' Orders of Magnitude: A History of the NACA
    and
    NASA, 1915-1990''](http://www.hq.nasa.gov/office/pao/History/SP-4406/cover.html).
  - [Alex Roland. *Model Research: The National Advisory Committee for
    Aeronautics, 1915-1958.*](http://history.nasa.gov/SP-4103)
  - [Michael H. Gorn, *Expanding the envelope - Flight Research at NACA
    and
    NASA.*](https://web.archive.org/web/20070928044802/http://www.kentuckypress.com/viewbook.cfm?Category_ID=1&Group=60&ID=1004)

[Category:美國航空](../Category/美國航空.md "wikilink")
[Category:航天机构](../Category/航天机构.md "wikilink")
[Category:美国国家航空航天局](../Category/美国国家航空航天局.md "wikilink")
[Category:1915年建立](../Category/1915年建立.md "wikilink")
[Category:1958年廢除](../Category/1958年廢除.md "wikilink")
[Category:美國已廢止政府機構](../Category/美國已廢止政府機構.md "wikilink")

1.