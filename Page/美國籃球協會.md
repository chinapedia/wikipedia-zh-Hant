1967年成立的**美国篮球协会**（**American Basketball Association**，或简写成
**ABA**）曾经是美国的一个主要篮球联赛，
1976年被[NBA联盟兼并](../Page/NBA.md "wikilink")。与1999年新建立的[美国篮球协会
(21世纪)并无关联](../Page/美国篮球协会_\(21世纪\).md "wikilink")。

## 历史

ABA创建于1967年，和1946年成立的美国NBA联盟互相竞争，1976年NBA与ABA合并。合并时投资者可以用比建立新的NBA球队便宜一半的价格买下原ABA球队。最终，四支ABA球队被吸纳参加了原来的NBA联盟，包括[新泽西籃网队](../Page/新泽西籃网队.md "wikilink")（旧称纽约籃网队），[丹佛金塊队](../Page/丹佛金塊队.md "wikilink")，[印第安纳溜馬队和](../Page/印第安纳溜馬队.md "wikilink")[圣安东尼奥马刺队](../Page/圣安东尼奥马刺队.md "wikilink")。另外两支队伍，肯塔基上校队(Kentucky
Colonels)和圣路易斯灵魂队(Spirits of St. Louis)在合并大潮的冲击下宣布解散。此外，弗吉尼亚绅士队(Virginia
Squires)在合并前一个月停止运营，因而错过了合并的机遇。

ABA联盟比NBA联盟更开放，进攻更华丽。进攻时间为30秒（NBA为24秒），并率先引入三分线。ABA联盟采用红色、蓝色和白色组成的彩色球，而不是NBA使用的传统褐色皮质篮球。ABA的这些特点曾经逐渐的吸引许多球迷，但是始终由于缺乏电视转播合同，和之前遗留下来的财政损失，ABA最终难以为继。在她存在的最后一年，也就是1976年，ABA在丹佛率先举办了如今在NBA整个赛季被球迷津津乐道的灌篮大赛和全明星週。

ABA对NBA的影响很大，如三分线、全明星週、和灌篮大赛都成为NBA的一部分。ABA用更高的工资吸引NBA的球员、裁判和教练，使得NBA的工资水平随之提高，合并后NBA也保持了该工资水平。也使得NBA进入了北加州、弗吉尼亚州和肯塔基州。

前NBA巨星[乔治·麦肯是ABA的第一任主席](../Page/乔治·麦肯.md "wikilink")。同时也是他使用了三分线和ABA联盟的商标，一个红白蓝色相间的篮球。

### ABA球队名单

以粗體標記的隊伍目前仍然存在于[NBA联盟中](../Page/NBA.md "wikilink")。箭號表示球隊的遷移。

  - [安那罕伙伴队(Anaheim Amigos)→洛杉矶星队(Los Angeles
    Stars)](../Page/洛杉矶星队.md "wikilink")→[犹他星队](../Page/犹他星队.md "wikilink")
  - **[达拉斯灌木队(Dallas Chaparrals)/德克萨斯灌木队(Texas Chaparrals) --\>
    圣安东尼奥马刺队](../Page/圣安东尼奥马刺队.md "wikilink")**
  - **[丹佛火箭/金塊队](../Page/丹佛掘金队.md "wikilink")**
  - [休士頓小牛队 --\>
    卡罗莱纳美洲狮队](../Page/卡罗莱纳美洲狮队.md "wikilink")→[聖路易精神隊](../Page/聖路易精神隊.md "wikilink")
      - 犹他洛基山队（圣路易斯灵魂队原本迁移到[盐湖城后改名](../Page/盐湖城.md "wikilink")。但是这个球队实际上没有参加过一场的常规赛比赛，就因为ABA和NBA合并而解散）
  - **[印第安那溜馬队](../Page/印第安那溜馬队.md "wikilink")**
  - [肯塔基上校队](../Page/肯塔基上校队.md "wikilink")
  - [新奥尔良海盗队](../Page/新奥尔良海盗队.md "wikilink")(New Orleans
    Buccaneers)→[孟菲斯职业/圆扁帽/乐曲队](../Page/孟菲斯乐曲队.md "wikilink")
      - [巴尔的摩急先锋队/蟹爪队](../Page/巴尔的摩蟹爪队.md "wikilink")（孟菲斯乐曲队迁去了新城市且取了新队名。蟹爪队进行过几场表演赛但在常规赛之前球队解散了。）
  - [明尼苏达马斯基队 --\> 迈阿密佛罗里达人队/佛罗里达人队](../Page/佛罗里达人队.md "wikilink")
  - **[新泽西美国人 -\> 纽約籃网队](../Page/新泽西网.md "wikilink")**
  - [奥克兰橡树队](../Page/奥克兰橡树队.md "wikilink")→[华盛顿首都队(Washington Capitals)
    --\> 弗吉尼亚绅士队(Virginia Squires)](../Page/弗吉尼亚绅士队.md "wikilink")
  - [匹兹堡风笛手队 --\> 明尼苏达风笛手队(Minnesota Pipers) --\>
    匹兹堡风笛手队/秃鹫队(Piper/Condors)](../Page/匹兹堡秃鹫队.md "wikilink")
  - [圣迭戈征服者队/帆船队(Conquistadors/Sails)](../Page/圣迭戈征服者队.md "wikilink")

### ABA著名球星

  - **[朱利叶斯·欧文](../Page/朱利叶斯·欧文.md "wikilink")**
    ([弗吉尼亚绅士队](../Page/弗吉尼亚绅士队.md "wikilink") 和
    [纽约网队](../Page/新泽西网.md "wikilink")，以罚球线起跳扣篮而闻名世界，他被尊崇为"J博士")。
  - **[路易·丹皮尔](../Page/路易·丹皮尔.md "wikilink")**
    ([肯塔基上校队](../Page/肯塔基上校队.md "wikilink"))，
    全联盟的得分王
  - **[梅尔·丹尼尔斯](../Page/梅尔·丹尼尔斯.md "wikilink")**
    ([印第安那溜馬队](../Page/印第安那溜馬队.md "wikilink"))
  - **[比利·坎宁安](../Page/比利·坎宁安.md "wikilink")**
    ([卡罗莱纳美洲狮队](../Page/卡罗莱纳美洲狮队.md "wikilink"))
  - **[里克·巴里](../Page/里克·巴里.md "wikilink")**
    ([奥克兰橡树队](../Page/奥克兰橡树队.md "wikilink"))
    (以超高的罚球命中率著称。他的倒马桶式罚球姿势颇为传奇).
  - **[康尼·霍金斯](../Page/康尼·霍金斯.md "wikilink")**
    ([匹兹堡风笛手队](../Page/匹兹堡风笛手队.md "wikilink"))
  - **[大卫·托马斯](../Page/大卫·托马斯_\(篮球运动员\).md "wikilink")**
    ([丹佛金塊](../Page/丹佛金塊.md "wikilink")，曾经一场独得73分)
  - **[乔治·"冰人"·格温](../Page/乔治·格温.md "wikilink")**
    ([圣安东尼奥马刺](../Page/圣安东尼奥马刺.md "wikilink"))
  - **[乔治·麦金尼斯](../Page/乔治·麦金尼斯.md "wikilink")**
    ([印第安那溜馬](../Page/印第安那溜馬.md "wikilink"))
  - **[斯宾瑟·海伍德](../Page/斯宾瑟·海伍德.md "wikilink")**
    ([丹佛火箭](../Page/丹佛金塊队.md "wikilink"))
  - **[阿堤斯·基尔莫勒](../Page/阿堤斯·基尔莫勒.md "wikilink")**
    ([肯塔基上校队](../Page/肯塔基上校队.md "wikilink"))
  - **[丹·伊塞尔](../Page/丹·伊塞尔.md "wikilink")**
    ([肯塔基上校队](../Page/肯塔基上校队.md "wikilink")，[丹佛金塊](../Page/丹佛金塊.md "wikilink"))
  - **[摩西·马龙](../Page/摩西·马龙.md "wikilink")**
    (他1974选秀大会中被[洛杉矶星队选中](../Page/洛杉矶星队.md "wikilink")，他从1976年进入ABA联盟直到1995年从NBA退役)。
  - **[鲍比·琼斯](../Page/鲍比·琼斯.md "wikilink")**
    ([卡罗莱纳美洲狮队](../Page/卡罗莱纳美洲狮队.md "wikilink"))
  - **[马克·卡尔文](../Page/马克·卡尔文.md "wikilink")**
    ([丹佛火箭](../Page/丹佛金塊队.md "wikilink"))
  - **[杰米·琼斯 (篮球运动员)](../Page/杰米·琼斯_\(篮球运动员\).md "wikilink")**
    ([犹他星队](../Page/犹他星队.md "wikilink"))
  - **[唐·布斯](../Page/唐·布斯.md "wikilink")**
    ([印第安那溜馬](../Page/印第安那溜馬.md "wikilink"))
  - **[罗杰·布朗](../Page/罗杰·布朗.md "wikilink")**
    ([印第安那溜馬](../Page/印第安那溜馬.md "wikilink"))
  - **[唐尼·弗里曼](../Page/唐尼·弗里曼.md "wikilink")**
    ([印第安那溜馬](../Page/印第安那溜馬.md "wikilink"))
  - **[拉里·琼斯](../Page/拉里·琼斯.md "wikilink")**
    ([丹佛火箭](../Page/丹佛金塊队.md "wikilink"))
  - **[查理·斯科特](../Page/查理·斯科特.md "wikilink")**
    ([弗吉尼亚绅士队](../Page/弗吉尼亚绅士队.md "wikilink"))
  - **[拉尔夫·桑普森](../Page/拉尔夫·桑普森.md "wikilink")**
    ([丹佛火箭](../Page/丹佛金塊队.md "wikilink"))

### 历届ABA联盟总冠军

| <font color="#000000">年份 | 冠军球队                                          | 比分    | 亚军球队                                     |
| :----------------------- | --------------------------------------------- | ----- | ---------------------------------------- |
| 1967年-1968年              | **[匹兹堡风笛手队](../Page/匹兹堡风笛手队.md "wikilink")**  | 4 - 3 | [新奥尔良海盗队](../Page/新奥尔良海盗队.md "wikilink") |
| 1968年-1969年              | **[奥克兰橡树队](../Page/奥克兰橡树队.md "wikilink")**    | 4 - 1 | [印第安那溜馬](../Page/印第安那溜馬.md "wikilink")   |
| 1969年-1970年              | **[印第安那溜馬](../Page/印第安那溜馬.md "wikilink")**    | 4 - 2 | [洛杉矶星队](../Page/洛杉矶星队.md "wikilink")     |
| 1970年-1971年              | **[犹他星队](../Page/犹他星队.md "wikilink")**        | 4 - 3 | [肯塔基上校队](../Page/肯塔基上校队.md "wikilink")   |
| 1971年-1972年              | **[印第安那溜馬](../Page/印第安那溜馬.md "wikilink")**    | 4 - 2 | [纽约籃网队](../Page/紐泽西籃网.md "wikilink")     |
| 1972年-1973年              | **[印第安那溜馬](../Page/印第安那溜馬.md "wikilink")**    | 4 - 3 | [肯塔基上校队](../Page/肯塔基上校队.md "wikilink")   |
| 1973年-1974年              | **[纽约网队](../Page/纽约网队.md "wikilink")**        | 4 - 1 | [犹他星队](../Page/犹他星队.md "wikilink")       |
| 1974年-1975年              | '''[肯塔基上校队](../Page/肯塔基上校队.md "wikilink") ''' | 4 - 1 | [印第安那溜馬](../Page/印第安那溜馬.md "wikilink")   |
| 1975年-1976年              | **[纽约籃网队](../Page/纽约籃网队.md "wikilink")**      | 4 - 2 | [丹佛金塊](../Page/丹佛金塊.md "wikilink")       |

## 外部链接

  - [回忆中的ABA](https://web.archive.org/web/20081107214515/http://www.remembertheaba.com/)

[Category:美国篮球](../Category/美国篮球.md "wikilink")