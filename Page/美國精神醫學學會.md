**美國精神醫學學會**（）是[美國精神科醫生的專業組織](../Page/美國.md "wikilink")，在行內具有全球性的影響力。現有約38000名會員，大部份在[美國](../Page/美國.md "wikilink")。其出版刊物包括學術期刊以及小冊子以外，最為人熟悉的就是《[精神疾病診斷與統計手冊](../Page/精神疾病診斷與統計手冊.md "wikilink")》，簡稱「DSM」。這本手冊是美國主要診斷[精神疾病的重要標準](../Page/精神疾病.md "wikilink")，亦是全世界許多國家在診斷[精神疾病上的重要參考](../Page/精神疾病.md "wikilink")。

其簡稱「APA」會被人誤會成[美國心理學會](../Page/美國心理學會.md "wikilink")（）。

## 註釋

## 外部連結

  - [美國精神醫學學會網頁](http://www.psych.org/)
  - [Paul Lowinger
    papers](http://hdl.library.upenn.edu/1017/d/ead/upenn_rbml_MsColl635),
    Kislak Center for Special Collections, Rare Books and Manuscripts,
    University of Pennsylvania. Paul Lowinger was a psychiatrist and
    founder of the Institute of Social Medicine and Community Health,
    and his papers are largely concerned with his work as a psychiatrist
    and activist, with significant portions devoted to his work with the
    American Psychiatric Association.

[Category:精神病学组织](../Category/精神病学组织.md "wikilink")
[Category:臨床心理學](../Category/臨床心理學.md "wikilink")
[Category:美國精神醫學學會](../Category/美國精神醫學學會.md "wikilink")
[Category:科学协会](../Category/科学协会.md "wikilink")
[Category:美国医学协会](../Category/美国医学协会.md "wikilink")