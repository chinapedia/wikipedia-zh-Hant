**美國銀行大廈**（）位於[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")，是為[世界貿易中心倒塌後](../Page/世界貿易中心.md "wikilink")[美國繼](../Page/美國.md "wikilink")[西爾斯大樓及](../Page/西爾斯大樓.md "wikilink")[帝國大廈後](../Page/帝國大廈.md "wikilink")[第三高的](../Page/摩天大樓列表.md "wikilink")[摩天大廈](../Page/摩天大廈.md "wikilink")，於全世界摩天大樓排名第13，高366米（1,200英尺），樓高54層，已於2009年完工。

## 歷史

美國銀行大廈於2004年開始動工，2008年完成並於同年開放。
[BankOfAmericaTowerSite.jpg](https://zh.wikipedia.org/wiki/File:BankOfAmericaTowerSite.jpg "fig:BankOfAmericaTowerSite.jpg")
[Bank_of_america_tower_sept_2007.jpg](https://zh.wikipedia.org/wiki/File:Bank_of_america_tower_sept_2007.jpg "fig:Bank_of_america_tower_sept_2007.jpg")

## 參見

  - [美國銀行中心](../Page/美國銀行中心.md "wikilink")
  - [美國銀行廣場](../Page/美國銀行廣場.md "wikilink")

## 参考文献

{{-}}

[Category:曼哈頓摩天大樓](../Category/曼哈頓摩天大樓.md "wikilink")
[Category:超過350米高的摩天大樓](../Category/超過350米高的摩天大樓.md "wikilink")
[Category:2009年完工建築物](../Category/2009年完工建築物.md "wikilink")