<table>
<thead>
<tr class="header">
<th><p><a href="../Page/美國電影.md" title="wikilink">{{color</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:United_States_film.png" title="fig:United_States_film.png">United_States_film.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年代電影.md" title="wikilink">{{color</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2000年美國電影列表.md" title="wikilink">2000</a> <a href="../Page/2001年美國電影列表.md" title="wikilink">2001</a> <a href="../Page/2002年美國電影列表.md" title="wikilink">2002</a> <a href="../Page/2003年美國電影列表.md" title="wikilink">2003</a> <a href="../Page/2004年美國電影列表.md" title="wikilink">2004</a><br />
<a href="../Page/2005年美國電影列表.md" title="wikilink">2005</a> <a href="../Page/2006年美國電影列表.md" title="wikilink">2006</a> <a href="../Page/2007年美國電影列表.md" title="wikilink">2007</a> <a href="../Page/2008年美國電影列表.md" title="wikilink">2008</a> <a href="../Page/2009年美國電影列表.md" title="wikilink">2009</a></p></td>
</tr>
</tbody>
</table>

本表列出[美國電影工業所出產的電影作品](../Page/美國電影.md "wikilink")。因影片數量龐大，由2000年代美國電影開始分門別類，依年代排序另分出條目列表，再按電影片名英文原名字母順序排列。注意：此列表剛剛開始建設。

## 2000年代

  - **[2000年美國電影列表](../Page/2000年美國電影列表.md "wikilink")**
  - **[2001年美國電影列表](../Page/2001年美國電影列表.md "wikilink")**
  - **[2002年美國電影列表](../Page/2002年美國電影列表.md "wikilink")**
  - **[2003年美國電影列表](../Page/2003年美國電影列表.md "wikilink")**
  - **[2004年美國電影列表](../Page/2004年美國電影列表.md "wikilink")**
  - **[2005年美國電影列表](../Page/2005年美國電影列表.md "wikilink")**
  - **[2006年美國電影列表](../Page/2006年美國電影列表.md "wikilink")**
  - **[2007年美國電影列表](../Page/2007年美國電影列表.md "wikilink")**
  - **[2008年美國電影列表](../Page/2008年美國電影列表.md "wikilink")**
  - **[2009年美國電影列表](../Page/2009年美國電影列表.md "wikilink")**

__NOTOC__ [A](../Page/#A.md "wikilink")
[B](../Page/#B.md "wikilink") [C](../Page/#C.md "wikilink")
[D](../Page/#D.md "wikilink") [E](../Page/#E.md "wikilink")
[F](../Page/#F.md "wikilink") [G](../Page/#G.md "wikilink")
[H](../Page/#H.md "wikilink") [I](../Page/#I.md "wikilink")
[J](../Page/#J.md "wikilink") [K](../Page/#K.md "wikilink")
[L](../Page/#L.md "wikilink") [M](../Page/#M.md "wikilink")
[N](../Page/#N.md "wikilink") [O](../Page/#O.md "wikilink")
[P](../Page/#P.md "wikilink") [Q](../Page/#Q.md "wikilink")
[R](../Page/#R.md "wikilink") [S](../Page/#S.md "wikilink")
[T](../Page/#T.md "wikilink") [U](../Page/#U.md "wikilink")
[V](../Page/#V.md "wikilink") [W](../Page/#W.md "wikilink")
[X](../Page/#X.md "wikilink") [Y](../Page/#Y.md "wikilink")
[Z](../Page/#Z.md "wikilink")

## A

  - [初恋的回忆](../Page/初恋的回忆.md "wikilink") ( A Walk to Remember, 2002)
  - [复仇者](../Page/复仇者.md "wikilink")（The Avengers）
  - [安娜与国王](../Page/安娜与国王.md "wikilink")（Anna and the King）
  - [安妮·霍尔](../Page/安妮·霍尔.md "wikilink")（Annie Hall）
  - [阿凡达](../Page/阿凡达.md "wikilink")（Avatar）
  - [美国风情画](../Page/美国风情画.md "wikilink")（American Graffiti）
  - [西線無戰事](../Page/西線無戰事_\(電影\).md "wikilink")（All Quiet on the Western
    Front）
  - [美国甜心](../Page/美国甜心.md "wikilink")（American Sweethearts）
  - [桃色公寓](../Page/桃色公寓.md "wikilink")（The Apartment）
  - [莫扎特](../Page/莫扎特_\(电影\).md "wikilink")（Amadeus）
  - [异形](../Page/异形_\(电影\).md "wikilink")/系列（Alien）
  - [彗星美人](../Page/彗星美人.md "wikilink")（All About Eve）
  - [人工智能](../Page/人工智能_\(电影\).md "wikilink")（A.I. Artificial
    Intelligence）
  - [边城英烈传](../Page/边城英烈传.md "wikilink") (The Alamo)

-----

## B

  - [回到未來](../Page/回到未來.md "wikilink") (Back to the Future)
  - [回到未來II](../Page/回到未來II.md "wikilink") (Back to the Future II)
  - [回到未來III](../Page/回到未來III.md "wikilink") (Back to the Future III)
  - [男孩别哭](../Page/男孩别哭.md "wikilink")（Boys Don't Cry）
  - [沙滩](../Page/沙滩.md "wikilink")（The Beach）
  - [宾漢](../Page/宾漢.md "wikilink")（Ben-Hur）
  - [廊桥遗梦](../Page/廊桥遗梦.md "wikilink")（Bridges of Madison County）
  - [边缘日记](../Page/边缘日记.md "wikilink")（The Basketball Diaries）
  - [标心者](../Page/标心者.md "wikilink") ( A life less ordinary)
  - [-{zh-hans:不死劫;
    zh-hant:驚心動魄;}-](../Page/不死劫.md "wikilink")（Unbreakable）
  - [断背山](../Page/断背山.md "wikilink")（Brokeback Mountain）
  - [蝙蝠侠](../Page/蝙蝠侠.md "wikilink")（Batman）
  - [宝贝儿](../Page/宝贝儿.md "wikilink") (Baby Doll)
  - [奔腾年代](../Page/奔腾年代.md "wikilink")（Seabiscuit）
  - [黑鹰坠落](../Page/黑鹰坠落.md "wikilink")（Black Hawk Down）
  - [一個國家的誕生](../Page/一個國家的誕生.md "wikilink")(The Birth of a Nation)
  - [美丽心灵](../Page/美丽心灵.md "wikilink")/[有你終身美麗](../Page/有你終身美麗.md "wikilink")
    (A Beautiful Mind)
  - [变人](../Page/变人.md "wikilink") (Bicentennial Man)
  - [兵人](../Page/兵人.md "wikilink") (Soldier)
  - [变相怪杰](../Page/变相怪杰.md "wikilink") (The Mask)
  - [冰冻蜘蛛](../Page/冰冻蜘蛛.md "wikilink") (Ice Spiders)
  - [北西北](../Page/北西北.md "wikilink") (North by Northwes)
  - [蓝丝绒](../Page/蓝丝绒.md "wikilink") (Blue Velvet)
  - [雌雄大盗](../Page/雌雄大盗.md "wikilink") (Bonnie and Clyde)
  - [-{zh-hans:生于7月4日; zh-hant:7月4日誕生;}-](../Page/生于7月4日.md "wikilink")
    (Born on the Fourth of July)
  - [第凡內早餐](../Page/第凡內早餐.md "wikilink") (Breakfast at Tiffany's)
  - [人人都是约翰·马尔科维奇](../Page/人人都是约翰·马尔科维奇.md "wikilink") (Being John
    Malkovich)
  - [小鹿斑比](../Page/小鹿斑比.md "wikilink") (Bambi)

-----

## C

  - [北非諜影](../Page/北非諜影.md "wikilink")（*Casablanca*, 1942）
  - [苹果酒屋法则](../Page/苹果酒屋法则.md "wikilink")（The Cider House Rules, 1999）
  - [埃及艳后](../Page/埃及艳后_\(电影\).md "wikilink") (Cleopatra, 1963)
  - [赤潮](../Page/赤潮.md "wikilink") (Red Tide, 2011)
  - [浓情巧克力](../Page/浓情巧克力.md "wikilink") (Chocolat, 2000)
  - [霹雳娇娃](../Page/霹雳娇娃.md "wikilink") (Charlie's Angels, 2000)
  - [哭泣游戏](../Page/哭泣游戏.md "wikilink")（*The Crying Game*, 1992）
  - [虫虫危机](../Page/虫虫危机.md "wikilink") (A Bug's Life, 1998)
  - [城市之光](../Page/城市之光.md "wikilink") (City Lights, 1931)
  - [柯莱利上尉的曼陀铃](../Page/柯莱利上尉的曼陀铃.md "wikilink") (Captain Corelli's
    Mandolin, 2001)
  - [重归天堂](../Page/重归天堂.md "wikilink")
  - [蠢货](../Page/蠢货_\(电影版\).md "wikilink") (Jackass, 2002)
  - [擒凶記](../Page/擒凶記.md "wikilink") (The Man Who Knew Too Much, 1956)
  - [赤膽屠龍](../Page/赤膽屠龍.md "wikilink") (Rio Bravo, 1959)
  - [唐人街](../Page/唐人街_\(电影\).md "wikilink") (Chinatown, 1974)
  - [大國民](../Page/大國民.md "wikilink") (Citizen Kane, 1941)
  - [冷山](../Page/冷山.md "wikilink") (Cold Mountain, 2003)
  - [发条桔子](../Page/发条桔子.md "wikilink") (A Clockwork Orange, 1972)

-----

## D

  - [最後一封情書](../Page/最後一封情書.md "wikilink") (Dear John, 2010)
  - [小飞象](../Page/小飞象.md "wikilink") (Dumbo, 1941)
  - [阿呆与阿瓜](../Page/阿呆与阿瓜.md "wikilink") (Dumb and Dumber, 1994)
  - [死囚168小时/越過死亡線](../Page/死囚168小时/越過死亡線.md "wikilink") (Dead Man
    Walking, 1995)
  - [日瓦戈医生/齊瓦哥醫生](../Page/日瓦戈医生/齊瓦哥醫生.md "wikilink") (Doctor Zhivago,
    1957)
  - [安妮日记](../Page/安妮的日記_\(電影\).md "wikilink") (The Diary of Anne Frank,
    1959)
  - [刀锋战士3：三位一体](../Page/刀锋战士3：三位一体.md "wikilink") (Blade - Trilogy)
  - [谍影疑云](../Page/谍影疑云.md "wikilink") (North by Northwest, 1959)
  - [大卫与丽莎](../Page/大卫与丽莎.md "wikilink") (David and Lisa, 1962)
  - [大河恋](../Page/大河恋.md "wikilink") (A River Runs Through It, 1992)
  - [东方西方](../Page/东方西方.md "wikilink") (East/West, 2000)
  - [断头谷](../Page/断头谷.md "wikilink") (Heads will Roll, 1999)
  - [后天/明天過後](../Page/后天/明天過後.md "wikilink") (The Day After Tomorrow,
    1994)
  - [大毒枭/一世狂野](../Page/大毒枭/一世狂野.md "wikilink") (Blow, 2001)
  - [灯塔惊魂记](../Page/灯塔惊魂记.md "wikilink") (Cruel And Unusual , 2001)
  - \[\[盜墓迷城1,2,外傳:蠍子王傳奇\]/神鬼傳奇\] (Scorpion King, 2002)
  - [狙击电话亭](../Page/狙击电话亭.md "wikilink") (Phone Booth, 2002)
  - [第六日/魔鬼複製人](../Page/第六日/魔鬼複製人.md "wikilink") (The 6th Day, 2000)
  - [第四类接触](../Page/第四类接触.md "wikilink") (The Fourth Kind, 2009)
  - [巅峰极限/垂直极限](../Page/巅峰极限/垂直极限.md "wikilink") （Vertical Limit， 2000）
  - [鲨滩/夺命狂鲨(港)/绝鲨岛(台)](../Page/鲨滩/夺命狂鲨\(港\)/绝鲨岛\(台\).md "wikilink")
    （The Shallows， 2016）
  - [达芬奇密码](../Page/达芬奇密码.md "wikilink")（The Da Vinci Code， 2006）
  - [碟中谍 / 职业特工队(港) /
    不可能的任务(台)](../Page/碟中谍_/_职业特工队\(港\)_/_不可能的任务\(台\).md "wikilink")
    （Mission: Impossible， 1996）
  - [碟中谍2 / 职业特工队2(港) /
    不可能的任务2(台)](../Page/碟中谍2_/_职业特工队2\(港\)_/_不可能的任务2\(台\).md "wikilink")
    （Mission: Impossible II， 2000）
  - [碟中谍3 / 职业特工队3(港) /
    不可能的任务3(台)](../Page/碟中谍3_/_职业特工队3\(港\)_/_不可能的任务3\(台\).md "wikilink")
    （Mission: Impossible III， 2006）
  - [碟中谍4](../Page/碟中谍4.md "wikilink") / 职业特工队：鬼影约章(港) /
    [不可能的任务：鬼影行动](../Page/不可能的任务：鬼影行动.md "wikilink")(台)（Mission:
    Impossible - Ghost Protocol， 2011）
  - [碟中谍5：神秘国度](../Page/碟中谍5：神秘国度.md "wikilink") / 职业特工队5：叛逆帝国(港) /
    [不可能的任务：失控国度](../Page/不可能的任务：失控国度.md "wikilink")(台)（Mission:
    Impossible - Rogue Nation， 2015）
  - [電話情殺案](../Page/電話情殺案.md "wikilink")（Dial M for Muder, 1954）
  - [奇爱博士](../Page/奇爱博士.md "wikilink")（Dr. Strangelove, 1964）
  - [猎鹿人 / 越战猎鹿人(台) /
    猎鹿者(港)](../Page/猎鹿人_/_越战猎鹿人\(台\)_/_猎鹿者\(港\).md "wikilink")（The
    Deer Hunter, 1978）
  - [双重赔偿 / 双重保险 / 双重生活](../Page/双重赔偿_/_双重保险_/_双重生活.md "wikilink")
    (Double Indemnity, 1944)

-----

## E

  - [艾德·伍德 / 艾活传 / 艾得伍德](../Page/艾德·伍德_/_艾活传_/_艾得伍德.md "wikilink")（Ed
    Wood, 1994）
  - [20段恋爱真相 / 泡妞20段 /
    20段恋爱真相](../Page/20段恋爱真相_/_泡妞20段_/_20段恋爱真相.md "wikilink")
    ( 20 Dates， 1998)
  - [灵魂战车 / 恶灵骑士 / 幽灵骑士](../Page/灵魂战车_/_恶灵骑士_/_幽灵骑士.md "wikilink")
    (Ghost Rider, 2007)
  - [驱魔人/大法師](../Page/驱魔人/大法師.md "wikilink")（The Exorcist, 1973）
  - [逍遥骑士](../Page/逍遥骑士.md "wikilink")（The Easy Rider, 1969）
  - [英倫情人](../Page/英倫情人.md "wikilink")（The English Patient, 1996）
  - [E.T.外星人](../Page/E.T.外星人.md "wikilink")/[外星人](../Page/外星人.md "wikilink")（E.T.
    The Extra-Terrestial, 1982）
  - [剪刀手爱德华](../Page/剪刀手爱德华.md "wikilink")（Edward Scissorhands, 1990）
  - [太阳帝国](../Page/太阳帝国.md "wikilink")（Empire of the Sun, 1987）
  - [第三類接觸](../Page/第三類接觸_\(電影\).md "wikilink")（Encounters of the Third
    Kind, 1977）
  - [伊丽莎白2：黄金时代](../Page/伊丽莎白2：黄金时代.md "wikilink")（Elizabeth: The Golden
    Age, 2007）

-----

## F

  - [第五元素](../Page/第五元素.md "wikilink")（The Fifth Element, 1997）
  - [阿甘正传](../Page/阿甘正传.md "wikilink")（Forrest Gump, 1994）
  - [-{zh-hans:海底总动员;zh-hk:海底奇兵;zh-tw:海底總動員;}-](../Page/海底总动员.md "wikilink")（Finding
    Nemo, 2003）
  - [变蝇人](../Page/变蝇人.md "wikilink")（The Fly, 1986）
  - [第一滴血](../Page/第一滴血.md "wikilink")（First Blood,
    1982）/[第二滴血](../Page/第二滴血.md "wikilink")/[第三滴血](../Page/第三滴血.md "wikilink")/[第四滴血](../Page/第四滴血.md "wikilink")
  - [浮生若夢](../Page/浮生若夢.md "wikilink")(You Can't Take It with You, 1938)
  - [年度人物 / 风云人物 / 年度风云人物](../Page/年度人物_/_风云人物_/_年度风云人物.md "wikilink")
    (Man of the Year, 2006)
  - [屠出地狱/開膛手](../Page/屠出地狱/開膛手.md "wikilink")（From Hell, 2011）
  - [富貴浮雲](../Page/富貴浮雲.md "wikilink") (Aristocrats, 1999)
  - [神奇四侠/驚奇4超人](../Page/神奇四侠/驚奇4超人.md "wikilink")（The Fantastic Four,
    2015）
  - [母牛总动员 / 牧场是我家 /
    放牛吃草](../Page/母牛总动员_/_牧场是我家_/_放牛吃草.md "wikilink")(Home
    on the Range, 2004)
  - [好莱坞有间怪酒店/瘋狂終結者](../Page/好莱坞有间怪酒店/瘋狂終結者.md "wikilink")（Four Rooms,
    1995）
  - [疯狂与美丽](../Page/疯狂与美丽.md "wikilink")(Crazy/Beautiful, 2001)
  - [秘书 / 怪ㄎㄚ情缘 /
    风流老板俏秘书](../Page/秘书_/_怪ㄎㄚ情缘_/_风流老板俏秘书.md "wikilink")(Secretary,
    2002)
  - [法国贩毒网2 / 霹雳神探续集 /
    密探霹雳火续集](../Page/法国贩毒网2_/_霹雳神探续集_/_密探霹雳火续集.md "wikilink")(French
    Connection II, 1975)
  - [法戈](../Page/法戈.md "wikilink")（Fargo, 1996）
  - [-{zh-hans:咪走堂; zh-hant:蹺課天才;}-](../Page/咪走堂.md "wikilink")（Ferris
    Bueller's Day Off, 1986）
  - [乱世忠魂](../Page/乱世忠魂.md "wikilink")（From Here to Eternity, 1953）
  - [科学怪人](../Page/科学怪人.md "wikilink")（Mary Shelley's Frankenstein,
    1994）
  - [搏击俱乐部](../Page/搏击俱乐部.md "wikilink")（Fight Club, 1999）

-----

## G

  - [乱世佳人](../Page/乱世佳人_\(电影\).md "wikilink")（Gone with the Wind, 1939）
  - [人鬼情未了/第六感生死戀](../Page/人鬼情未了/第六感生死戀.md "wikilink")（Ghost, 2003）
  - [黑暗物质\*黄金罗盘](../Page/黑暗物质*黄金罗盘.md "wikilink")（The Golden Compass,
    2007）
  - [愤怒的葡萄](../Page/愤怒的葡萄.md "wikilink")（The Grapes of Wrath, 1939）
  - [毕业生](../Page/毕业生_\(电影\).md "wikilink")（The Graduate, 1967）
  - [教父第1至3集](../Page/教父_\(电影\).md "wikilink")（The Godfather, 1972,
    1974, 1990）
  - [桂河大桥](../Page/桂河大桥.md "wikilink") (The Bridge on the River Kwai,
    1957)
  - [关山飞渡](../Page/关山飞渡.md "wikilink") (Stagecoach, 1939)
  - [公主日记](../Page/公主日记.md "wikilink")（The Princess Diaries, 2001）
  - [失恋排行榜 / 高保真 / 高度忠诚](../Page/失恋排行榜_/_高保真_/_高度忠诚.md "wikilink") (High
    Fidelity, 2000)
  - [葛底士堡](../Page/葛底士堡.md "wikilink")/蓋茨堡戰役 (Gettysburg, 1993)
  - [光荣战役 / 光荣 / Slava](../Page/光荣战役_/_光荣_/_Slava.md "wikilink")（Glory,
    1989）
  - [过关斩将 / 过关斩将 / 魔鬼阿诺](../Page/过关斩将_/_过关斩将_/_魔鬼阿诺.md "wikilink") (The
    Running Man, 1987)
  - [鬼雾](../Page/鬼雾.md "wikilink") (The Fog, 2005)
  - [哥斯拉](../Page/哥斯拉.md "wikilink")（Godzilla）/1 2
  - [隔山有眼](../Page/隔山有眼.md "wikilink") (The Hills Have Eyes, 2006)
  - [鬼娃新娘](../Page/鬼娃新娘.md "wikilink") (Bride of Chucky, 1998)
  - [葛倫米勒傳](../Page/葛倫米勒傳.md "wikilink") (The Glenn Miller Story, 1953)

## H

  - [西部開拓史](../Page/西部開拓史.md "wikilink")（How the West Was Won, 1962）
  - [花木兰](../Page/花木兰_\(1998年电影\).md "wikilink")（Mulan, 1998）
  - [幻想曲](../Page/幻想曲.md "wikilink") (Fantasia, 1940)
  - [绿巨人 (电影)](../Page/绿巨人_\(电影\).md "wikilink") (Hulk, 2003)
  - [好莱坞式结局](../Page/好莱坞式结局.md "wikilink") (Hollywood Ending, 2002)
  - [汉尼拔 / 沉默的羔羊](../Page/汉尼拔_/_沉默的羔羊.md "wikilink") (Hannibal, 2001)
  - [航运新闻 / 真情快递 / 笔下有晴天](../Page/航运新闻_/_真情快递_/_笔下有晴天.md "wikilink")
    (The Shipping News, 2001)
  - [海上钢琴师](../Page/海上钢琴师.md "wikilink") (The Legend of 1900, 1998)
  - [黄金时代](../Page/黄金时代.md "wikilink") (The Best Years of Our Lives,
    1946)
  - [后窗](../Page/后窗.md "wikilink") (Rear Window, 1954)
  - [后窗](../Page/后窗.md "wikilink") (Rear Window, 1958)
  - [虎豹小霸王](../Page/虎豹小霸王.md "wikilink") (Butch Cassidy and the Sundance
    Kid, 1969)
  - [虎胆龙威 / 終極警探](../Page/虎胆龙威_/_終極警探.md "wikilink") (Die Hard, 1988)
  - [虎胆龙威2 / 終極警探2](../Page/虎胆龙威2_/_終極警探2.md "wikilink") (Die Hard 2,
    1990)
  - [虎胆龙威3 / 終極警探3](../Page/虎胆龙威3_/_終極警探3.md "wikilink") (Die Hard :
    With a Vengeance, 1995)
  - [虎胆龙威4 / 終極警探4](../Page/虎胆龙威4_/_終極警探4.md "wikilink") (Live Free or
    Die Hard, 2007)
  - [虎胆龙威5 / 終極警探5](../Page/虎胆龙威5_/_終極警探5.md "wikilink") (A Good Day to
    Die Hard, 2013)
  - [虎！虎！虎！/ 偷袭珍珠港](../Page/虎！虎！虎！/_偷袭珍珠港.md "wikilink") ( Tora\! Tora\!
    Tora\! 1970)
  - [黑狱亡魂/ 第三个人 / 第三者](../Page/黑狱亡魂/_第三个人_/_第三者.md "wikilink") (The
    Third Man, 1949)
  - [魂归离恨天/ 咆哮山庄](../Page/魂归离恨天/_咆哮山庄.md "wikilink") (Wuthering Heights,
    1939)
  - [好家伙 / 盗亦有盗(港) /
    四海好家伙(台)](../Page/好家伙_/_盗亦有盗\(港\)_/_四海好家伙\(台\).md "wikilink")
    (Goodfellas, 1990)
  - [黑超特警組 / MIB星际战警 1, 2,
    3](../Page/黑超特警組_/_MIB星际战警_1,_2,_3.md "wikilink")
    ( Men in Black I, II, III, 1997, 2002, 2012)
  - [华纳巨星总动员](../Page/华纳巨星总动员.md "wikilink") ( Looney Tunes: Back in
    Action , 2003)
  - [騎兵隊](../Page/騎兵隊.md "wikilink") (The Horse Soldiers, 1959)
  - [滑板公园](../Page/滑板公园.md "wikilink") (Ken Park, 2002)
  - [环游地球80天](../Page/环游地球80天.md "wikilink") (Around the World in 80
    Days, 2004)
  - [蝗恐之灾 / 蝗虫毁灭日](../Page/蝗恐之灾_/_蝗虫毁灭日.md "wikilink") (Locusts, 2005)
  - [黑洞频率](../Page/黑洞频率.md "wikilink") (Frequency, 2000)
  - [ID4星际终结者(台) / 天煞-地球反击战(港) /
    地球捍卫战](../Page/ID4星际终结者\(台\)_/_天煞-地球反击战\(港\)_/_地球捍卫战.md "wikilink")
    (Independence Day, 1996)
  - [黑洞表面 / 撕裂地平线](../Page/黑洞表面_/_撕裂地平线.md "wikilink") (Event Horizon,
    1997)
  - [火星任务](../Page/火星任務_\(2000年電影\).md "wikilink") (Mission to Mars,
    2000)
  - [透明人 / 透明人魔 / 隐形人](../Page/透明人_/_透明人魔_/_隐形人.md "wikilink") (Hollow
    Man, 2000)
  - [火星怪物](../Page/火星怪物.md "wikilink")
  - [黑夜传说 / 决战异世界(台)](../Page/黑夜传说_/_决战异世界\(台\).md "wikilink")
    (Underworld, 2003)
  - [黑夜传说2 / 决战异世界2(台)](../Page/黑夜传说2_/_决战异世界2\(台\).md "wikilink")
    (Underworld: Evolution, 2006)
  - [黑夜传说前传：狼族再起 /
    决战异世界前传：鬼哭狼嚎(台)](../Page/黑夜传说前传：狼族再起_/_决战异世界前传：鬼哭狼嚎\(台\).md "wikilink")
    (Underworld: Rise of the Lycans, 2009)
  - [黑夜传说4：觉醒 /
    决战异世界：未来复苏(台)](../Page/黑夜传说4：觉醒_/_决战异世界：未来复苏\(台\).md "wikilink")
    (Underworld: Awakening, 2012)
  - [黑夜传说5：血战 /
    决战异世界：弑血之战(台)](../Page/黑夜传说5：血战_/_决战异世界：弑血之战\(台\).md "wikilink")
    (Underworld: Blood Wars, 2016)
  - [007黑日危机](../Page/007黑日危机.md "wikilink") (The World Is Not Enough,
    1999)
  - [核弹列车](../Page/核弹列车.md "wikilink") (Atomic Train, 1999)
  - [核爆地心](../Page/核爆地心.md "wikilink")
  - [陨石噩梦 / 轰天浩劫](../Page/陨石噩梦_/_轰天浩劫.md "wikilink") (Comet Impact,
    2008)
  - [蝴蝶效应](../Page/蝴蝶效应.md "wikilink") (The Butterfly Effect, 2004)
  - [黃巾騎兵隊 / 她披上了黄丝带](../Page/黃巾騎兵隊_/_她披上了黄丝带.md "wikilink") (She Wore a
    Yellow Ribbon, 1949)
  - [紅河](../Page/紅河.md "wikilink") (Red River, 1948)
  - [黃昏之戀 / 午后之爱 / 巴黎春恋 /
    下午的爱情](../Page/黃昏之戀_/_午后之爱_/_巴黎春恋_/_下午的爱情.md "wikilink")
    (Love in the Afternoon, 1957)
  - [捍卫战士(台) / 壮志凌云](../Page/捍卫战士\(台\)_/_壮志凌云.md "wikilink") (Top Gun,
    1986)

-----

## I

  - [天羅盜網](../Page/天羅盜網.md "wikilink")/[偷天换日](../Page/偷天换日.md "wikilink")（The
    Italian Job, 2003）
  - [超人特攻队](../Page/超人特攻队.md "wikilink")/[超人总动员](../Page/超人总动员.md "wikilink")/[超人家族](../Page/超人家族.md "wikilink")/[超人特工队](../Page/超人特工队.md "wikilink")（The
    Incredibles, 2004）
  - [冰河世纪](../Page/冰河世纪.md "wikilink")（Ice Age）
  - [-{zh-hans:我是山姆; zh-hant:他不笨，他是我爸爸;}-](../Page/我是山姆.md "wikilink")（I
    Am Sam）
  - [鐵甲奇俠 / 鋼鐵人 (電影)](../Page/鐵甲奇俠_/_鋼鐵人_\(電影\).md "wikilink") (Iron
    Man, 2008)

-----

## J

  - [大白鯊](../Page/大白鲨_\(电影\).md "wikilink")（Jaws, 1975 )
  - [家族企业](../Page/家族企业.md "wikilink") (Family Business, 1989)
  - [精神病人](../Page/精神病人.md "wikilink")/[精神病患者](../Page/精神病患者.md "wikilink")/[触目惊心](../Page/触目惊心.md "wikilink")/[驚魂記](../Page/驚魂記.md "wikilink")
    ( Psycho, 1960 )
  - [巨人传 / 巨人](../Page/巨人传_/_巨人.md "wikilink") ( Giant, 1956 )
  - [爵士歌手 / 爵士歌王 / 歌场孝子](../Page/爵士歌手_/_爵士歌王_/_歌场孝子.md "wikilink") ( The
    Jazz Singer, 1927 )
  - [-{zh-hans:角斗士;zh-hk:帝國驕雄;zh-tw:神鬼戰士;}-](../Page/角斗士_\(电影\).md "wikilink")
    ( Gladiator, 2000 )
  - [-{zh-hans:加勒比海盗; zh-hant:神鬼奇航;}- / 加勒比海盗1：黑珍珠号的诅咒 / 神鬼奇航：鬼盗船魔咒(台) /
    魔盗王：决战鬼盗船 / 海盗船](../Page/加勒比海盜.md "wikilink") ( Pirates of the
    Caribbean: The Curse of the Black Pearl, 2003 )
  - [加勒比海盗2：聚魂棺](../Page/加勒比海盗2：聚魂棺.md "wikilink") /
    [加勒比海盜：决战魔盜王(港)](../Page/加勒比海盜：决战魔盜王\(港\).md "wikilink")
    / [加勒比海盗：神鬼奇航2(台)](../Page/加勒比海盗：神鬼奇航2\(台\).md "wikilink") /
    [加勒比海盗2：亡灵宝藏](../Page/加勒比海盗2：亡灵宝藏.md "wikilink") (
    Pirates of the Caribbean: Dead Man's Chest, 2006 )
  - [加勒比海盗3：世界的尽头](../Page/加勒比海盗3：世界的尽头.md "wikilink") /
    [加勒比海盜：魔盜王终极之战(港)](../Page/加勒比海盜：魔盜王终极之战\(港\).md "wikilink")
    / [神鬼奇航：世界的尽头(台)](../Page/神鬼奇航：世界的尽头\(台\).md "wikilink") /
    [加勒比海盗3：死亡荣誉](../Page/加勒比海盗3：死亡荣誉.md "wikilink") (
    Pirates of the Caribbean: At World's End, 2007 )
  - [加勒比海盗4：惊涛怪浪](../Page/加勒比海盗4：惊涛怪浪.md "wikilink") /
    [加勒比海盗：魔盗狂潮(港)](../Page/加勒比海盗：魔盗狂潮\(港\).md "wikilink")
    / [加勒比海盗 神鬼奇航：幽灵海(台)](../Page/加勒比海盗_神鬼奇航：幽灵海\(台\).md "wikilink") /
    [加勒比海盗4：陌生的潮汐](../Page/加勒比海盗4：陌生的潮汐.md "wikilink") ( Pirates of
    the Caribbean: On Stranger Tides, 2011 )
  - [加勒比海盗5：死无对证](../Page/加勒比海盗5：死无对证.md "wikilink") /
    [加勒比海盗：恶灵启航(港)](../Page/加勒比海盗：恶灵启航\(港\).md "wikilink")
    / [加勒比海盗 神鬼奇航：死无对证(台)](../Page/加勒比海盗_神鬼奇航：死无对证\(台\).md "wikilink") /
    [加勒比海盗5：亡灵的缄默](../Page/加勒比海盗5：亡灵的缄默.md "wikilink") /
    [加勒比海盗5：亡者无言](../Page/加勒比海盗5：亡者无言.md "wikilink")
    / [加勒比海盗5：死人不会告密](../Page/加勒比海盗5：死人不会告密.md "wikilink") ( Pirates of
    the Caribbean: Dead Men Tell No Tales, 2017 )
  - [吉他杀手](../Page/吉他杀手.md "wikilink")
  - [惊变28天/28天毁灭倒数(台)/28日后(港)](../Page/惊变28天/28天毁灭倒数\(台\)/28日后\(港\).md "wikilink")
    ( 28 Days Later, 2002 )
  - [机械公敌](../Page/机械公敌.md "wikilink")/1 2
  - [极度深寒 / 怪从深海来 / 深海拦截：大海怪 / 大海怪 /
    深海浮出](../Page/极度深寒_/_怪从深海来_/_深海拦截：大海怪_/_大海怪_/_深海浮出.md "wikilink")（
    Deep Rising, 1998 )
  - [绝地战警/ 坏小子 / 重案梦幻组](../Page/绝地战警/_坏小子_/_重案梦幻组.md "wikilink") ( Bad
    Boys, 1995)
  - [机器战警](../Page/机器战警.md "wikilink")/系列
  - [记忆裂痕/ 致命报酬(港/台) /
    空头支票](../Page/记忆裂痕/_致命报酬\(港/台\)_/_空头支票.md "wikilink")(
    Paycheck , 2003 )
  - [寂静岭/ 沉默之丘(台) / 鬼魅山房(港) /
    哑巴山](../Page/寂静岭/_沉默之丘\(台\)_/_鬼魅山房\(港\)_/_哑巴山.md "wikilink")
    ( Silent Hill , 2006 )

-----

## K

  - [金刚](../Page/金剛_\(2005年電影\).md "wikilink")（King Kong, 2005）
  - [隔世情缘 / 穿越時空愛上你 / 隔世俏佳人 /
    凯特和利奥波德](../Page/隔世情缘_/_穿越時空愛上你_/_隔世俏佳人_/_凯特和利奥波德.md "wikilink")
    （Kate & Leopold, 2001）
  - [狂恋大提琴/无情荒地有琴天(台) / 她比烟花寂寞 / 希拉里与杰基 / 希拉莉和积琪琳 /
    中断的琴声](../Page/狂恋大提琴/无情荒地有琴天\(台\)_/_她比烟花寂寞_/_希拉里与杰基_/_希拉莉和积琪琳_/_中断的琴声.md "wikilink")
    （Hilary and Jackie, 1998）
  - [哭泣的男人/ 縱情四海 / 哭泣的恋人](../Page/哭泣的男人/_縱情四海_/_哭泣的恋人.md "wikilink")（The
    Man Who Cried, 2000）
  - [K-19：寡妇制造者/ 轰天潜舰 /
    哈里逊福特K19](../Page/K-19：寡妇制造者/_轰天潜舰_/_哈里逊福特K19.md "wikilink")（K-19:
    The Widowmaker, 2002）
  - [狂蟒之灾/ 巨蟒 / 大蟒蛇：神出鬼没](../Page/狂蟒之灾/_巨蟒_/_大蟒蛇：神出鬼没.md "wikilink")
    （Anaconda , 1997）
  - [蛞蝓之灾](../Page/蛞蝓之灾.md "wikilink")（Slugs muerte viscosa , 1988）
  - [殺死比爾/ 追杀比尔(台) / 标杀令(港) /
    杀死比尔：第一卷](../Page/殺死比爾/_追杀比尔\(台\)_/_标杀令\(港\)_/_杀死比尔：第一卷.md "wikilink")
    （Kill Bill: Vol. 1, 2003）

-----

## L

| 原名                    | 台灣譯名                                          | 香港譯名                                      | 國內譯名                                         | 年份   |
| --------------------- | --------------------------------------------- | ----------------------------------------- | -------------------------------------------- | ---- |
| *L.A. Confidential*   | \-{[鐵面特警隊](../Page/鐵面特警隊.md "wikilink")}-     | \-{[幕後嫌疑犯](../Page/幕後嫌疑犯.md "wikilink")}- | \-{[洛城机密](../Page/洛城机密.md "wikilink")}-      | 1997 |
| *Lady and the Tramp*  | \-{[小姐與流氓](../Page/小姐與流氓.md "wikilink")}-     | \-{[小姐與流氓](../Page/小姐與流氓.md "wikilink")}- | \-{[小姐与流浪汉](../Page/小姐与流浪汉.md "wikilink")}-  | 1955 |
| *Legally Blonde*      | \-{[金法尤物](../Page/金法尤物.md "wikilink")}-       | \-{[律政俏佳人](../Page/律政俏佳人.md "wikilink")}- |                                              | 2001 |
| *Lilo & Stitch*       | \-{[星際寶貝](../Page/星際寶貝.md "wikilink")}-       | \-{[扮嘢小魔星](../Page/扮嘢小魔星.md "wikilink")}- | \-{[星际宝贝](../Page/星际宝贝.md "wikilink")}-      | 2002 |
| *The Lion King*       | \-{[獅子王](../Page/獅子王.md "wikilink")}-         | \-{獅子王}-                                  | \-{[狮子王](../Page/狮子王.md "wikilink")}-        | 1994 |
| *The Little Mermaid*  | \-{[小美人魚](../Page/小美人魚.md "wikilink")}-       | \-{[小魚仙](../Page/小魚仙.md "wikilink")}-     | \-{[小美人鱼](../Page/小美人鱼.md "wikilink")}-      | 1989 |
| *The Little Princess* | [小公主](../Page/小公主.md "wikilink")              |                                           |                                              |      |
| *Lolita*              | \-{[一樹梨花壓海棠](../Page/一樹梨花壓海棠.md "wikilink")}- | \-{一樹梨花壓海棠}-                              | \-{[洛丽塔](../Page/洛丽塔_\(电影\).md "wikilink")}- | 1962 |
| *The Longest Day*     | \-{[最長的一日](../Page/最長的一日.md "wikilink")}-     | \-{[碧血長天](../Page/碧血長天.md "wikilink")}-   | \-{[最长的一日](../Page/最长的一日.md "wikilink")}-    | 1962 |
| *The Lord of War*     | \-{[戰爭之王](../Page/戰爭之王.md "wikilink")}-       | \-{[軍火之王](../Page/軍火之王.md "wikilink")}-   | \-{[战争之王](../Page/战争之王.md "wikilink")}-      | 2005 |
| *Lost in Space*       | [LIS太空號](../Page/LIS太空號.md "wikilink")        | [迷失太空](../Page/迷失太空.md "wikilink")        | [迷失太空](../Page/迷失太空.md "wikilink")           |      |
| *Lost in Translation* | \-{[愛情，不用翻譯](../Page/愛情，不用翻譯.md "wikilink")}- | \-{[迷失東京](../Page/迷失東京.md "wikilink")}-   | \-{[迷失东京](../Page/迷失东京.md "wikilink")}-      | 2004 |
|                       |                                               |                                           |                                              |      |

-----

## M

  - [-{zh-hans:黑客帝国;zh-hk:廿二世紀杀人网络;zh-tw:駭客任務;}-](../Page/黑客帝国.md "wikilink")（The
    Matrix, 1999）
  - [黑客帝国2 - 重装上阵](../Page/黑客帝国2_-_重装上阵.md "wikilink")（The Matrix
    Reloaded, 2003）
  - [黑客帝国3 - 矩阵革命 /
    骇客任务完结篇：最后战役](../Page/黑客帝国3_-_矩阵革命_/_骇客任务完结篇：最后战役.md "wikilink")（The
    Matrix Revolutions, 2003）
  - [神秘河/ 悬河杀机 / 神秘之河 / 神秘河流 / 杀人约会 /
    神秘悬河](../Page/神秘河/_悬河杀机_/_神秘之河_/_神秘河流_/_杀人约会_/_神秘悬河.md "wikilink")（Mystic
    River, 2003）
  - [铁面人](../Page/铁面人.md "wikilink")（The Man in the Iron Mask）
  - [迷魂記](../Page/迷魂記.md "wikilink") (Vertigo, 1958)
  - [蝴蝶君](../Page/蝴蝶君.md "wikilink")（M. Butterfly）
  - [我的希腊婚礼](../Page/我的希腊婚礼.md "wikilink")（My Big Fat Greek Wedding）
  - [史密斯遊美京](../Page/史密斯遊美京.md "wikilink")（Mr. Smith Goes to Washington）
  - [美人記](../Page/美人記.md "wikilink") (Notorious, 1946)
  - [美国往事](../Page/美国往事.md "wikilink") (Once Upon a Time in America,
    1984)
  - [码头风云](../Page/码头风云.md "wikilink") (On the Waterfront, 1954)
  - [美好的生活](../Page/美好的生活.md "wikilink")
  - [美梦成真](../Page/美梦成真.md "wikilink")
  - [麻辣女王](../Page/麻辣女王.md "wikilink")
  - [木乃伊](../Page/木乃伊_\(电影\).md "wikilink")
  - [美国美人](../Page/美国美人.md "wikilink")
  - [-{zh-hans:我最好朋友的婚礼;
    zh-hant:新娘不是我;}-](../Page/我最好朋友的婚礼.md "wikilink")
  - [猫鼠游戏](../Page/猫鼠游戏.md "wikilink")
  - [美少女啦啦队](../Page/美少女啦啦队.md "wikilink")
  - [马耳他之鹰](../Page/马耳他之鹰.md "wikilink")
  - [摩登时代](../Page/摩登时代.md "wikilink")
  - [谍影迷魂](../Page/谍影迷魂.md "wikilink")
  - [怪兽电力公司](../Page/怪兽电力公司.md "wikilink")
  - [缺席的人](../Page/缺席的人.md "wikilink")
  - [午夜牛郎](../Page/午夜牛郎.md "wikilink")
  - [蒙面侠－佐罗](../Page/蒙面侠－佐罗.md "wikilink")　The Mask of Zorro
  - [-{zh-hans:佐罗的面具; zh-hant:蒙面俠蘇洛;}-](../Page/佐罗的面具.md "wikilink")
  - [穆赫兰大道](../Page/穆赫兰大道.md "wikilink")
  - [迷霧驚魂](../Page/迷霧驚魂.md "wikilink")
  - [明日帝国](../Page/明日帝国.md "wikilink")
  - [魔窟](../Page/魔窟.md "wikilink")
  - [猛鬼夜惊魂](../Page/猛鬼夜惊魂.md "wikilink")
  - [马达加斯加](../Page/马达加斯加.md "wikilink")
  - [冒险游戏](../Page/冒险游戏.md "wikilink")
  - [叛舰喋血记](../Page/叛舰喋血记.md "wikilink")
  - [红磨坊](../Page/红磨坊.md "wikilink")
  - [死囚之舞](../Page/死囚之舞.md "wikilink")
  - [关键报告](../Page/关键报告.md "wikilink")

-----

## N

  - [博物馆惊魂夜 / 博物馆之夜 / 博物馆奇妙夜 /
    翻生侏罗馆](../Page/博物馆惊魂夜_/_博物馆之夜_/_博物馆奇妙夜_/_翻生侏罗馆.md "wikilink")
    ( Night at the Museum, 2006 )
  - [女佣变凤凰 / 曼哈顿灰姑娘 /
    五星级恋人](../Page/女佣变凤凰_/_曼哈顿灰姑娘_/_五星级恋人.md "wikilink")
    ( Maid in Manhattan, 2002 )
  - [纽约沙发 / 巴黎情人 /
    沙发上的心理医生](../Page/纽约沙发_/_巴黎情人_/_沙发上的心理医生.md "wikilink")
    ( Un divan à New York, 1996 )
  - [怒海争锋:极地征伐 / 怒海争锋：极地远征 / 怒海争锋 /
    军天勇将：战海豪情](../Page/怒海争锋:极地征伐_/_怒海争锋：极地远征_/_怒海争锋_/_军天勇将：战海豪情.md "wikilink")
    ( Master and Commander: The Far Side of the World , 2003 )
  - [牛头](../Page/牛头.md "wikilink")
  - [怒火之花/ 愤怒的葡萄 / 愤怒的葡萄园](../Page/怒火之花/_愤怒的葡萄_/_愤怒的葡萄园.md "wikilink") (
    The Grapes of Wrath, 1940 )
  - [天生杀人狂 / 闪灵杀手 / 天生杀手](../Page/天生杀人狂_/_闪灵杀手_/_天生杀手.md "wikilink") (
    Natural Born Killers, 1994 )

-----

## O

  - [飛越瘋人院 / 飞越杜鹃窝 / 飞越喜鹊巢](../Page/飛越瘋人院_/_飞越杜鹃窝_/_飞越喜鹊巢.md "wikilink")
    ( One Flew Over the Cuckoo's Nest, 1975 )
  - [11罗汉/ 瞒天过海(台) / 盗海豪情(港) / 十一王牌 / 赌场劫案 / 盗海11侠 /
    江洋大盗](../Page/11罗汉/_瞒天过海\(台\)_/_盗海豪情\(港\)_/_十一王牌_/_赌场劫案_/_盗海11侠_/_江洋大盗.md "wikilink")
    ( Ocean's Eleven, 2001 )

-----

## P

  - [小飞侠](../Page/小飞侠.md "wikilink")（Peter Pan）
  - [-{zh-hans:低俗小说;zh-hk:危險人物
    (電影);zh-tw:黑色追緝令;}-](../Page/低俗小说.md "wikilink")（Pulp
    Fiction）
  - [漂流之心](../Page/漂流之心.md "wikilink")
  - [驚魂記](../Page/驚魂記.md "wikilink")/[触目惊心](../Page/触目惊心.md "wikilink")（Psycho）
  - [-{zh-hans:费城故事;
    zh-hant:費城;}-](../Page/费城故事_\(1993年\).md "wikilink")（Philadelphia）
  - [完美风暴](../Page/完美风暴.md "wikilink")（The Perfect Storm）
  - [費城故事](../Page/費城故事.md "wikilink")（Philadelphia Story）
  - [人猿星球](../Page/人猿星球.md "wikilink")（Planet of the Apes）
  - [巴顿将军](../Page/巴顿将军.md "wikilink")（Patton）
  - [风中奇缘](../Page/风中奇缘.md "wikilink")（Pocahontas）
  - [木偶奇遇记](../Page/木偶奇遇记.md "wikilink")（Pinoccio）

-----

## Q

  - [鹅毛笔](../Page/鹅毛笔_\(电影\).md "wikilink")（Quills）
  - [全金属外壳](../Page/全金属外壳.md "wikilink")
  - [情陷撒哈拉](../Page/情陷撒哈拉.md "wikilink")
  - [蜻蜓](../Page/蜻蜓.md "wikilink")
  - [情有独钟](../Page/情有独钟.md "wikilink")
  - [前进巴格达](../Page/前进巴格达.md "wikilink")
  - [千年痴情](../Page/千年痴情.md "wikilink")
  - [群尸玩过界](../Page/群尸玩过界.md "wikilink")

-----

## R

  - [夺宝奇兵](../Page/夺宝奇兵.md "wikilink")（Raiders of the Lost Ark）
  - [罗密欧与朱丽叶](../Page/罗密欧与朱丽叶_\(电影\).md "wikilink")（Romeo and Juliet）
  - [日正当中](../Page/日正当中.md "wikilink")
  - [與魔鬼共騎](../Page/與魔鬼共騎.md "wikilink")
  - [罗马假日](../Page/罗马假日_\(电影\).md "wikilink")（Roman Holiday）
  - [洛基](../Page/洛基.md "wikilink")（Rocky）
  - [愤怒的公牛](../Page/愤怒的公牛.md "wikilink")（Raging Bull）
  - [奪魂索](../Page/奪魂索.md "wikilink")（Rope）

-----

## S

  - [白雪公主](../Page/白雪公主.md "wikilink")（Snow White）
  - [钢琴师](../Page/钢琴师.md "wikilink")（Shine）
  - [熱情如火](../Page/熱情如火.md "wikilink")/[热情似火](../Page/热情似火.md "wikilink")（Some
    Like It Hot）
  - [星尘](../Page/星尘.md "wikilink")（Stardust）
  - [-{zh-hans:星际旅行;zh-hk:星空奇遇記;zh-tw:星艦奇航記;}-系列电影](../Page/星际旅行.md "wikilink")
  - [七年之痒](../Page/七年之痒.md "wikilink")（The Seven Year Itch）
  - [-{zh-hans:音乐之声;zh-hk:仙樂飄飄處處聞;zh-tw:真善美;}-](../Page/仙樂飄飄處處聞.md "wikilink")（The
    Sound of Music）
  - [龍鳳配](../Page/龍鳳配.md "wikilink")（Sabrina）
  - [第六感](../Page/第六感.md "wikilink")（Sixth Sense）
  - [沉默的羔羊](../Page/沉默的羔羊.md "wikilink")（The Silence of the Lambs）
  - [星际之门系列](../Page/星际之门.md "wikilink")/[星际传奇系列](../Page/星际传奇.md "wikilink")（Stargate）
  - [日落大道](../Page/日落大道_\(電影\).md "wikilink")（Sunset Boulevard）
  - [超人](../Page/超人.md "wikilink")（Superman）
  - [七宗罪](../Page/七宗罪.md "wikilink")（Seven）
  - [爱德华大夫](../Page/爱德华大夫.md "wikilink")（*Spellbound*）
  - [四眼天鸡](../Page/四眼天鸡.md "wikilink")
  - [时光倒流七十年](../Page/时光倒流七十年.md "wikilink")（Somewhere in Time）
  - [-{zh-hans:肖申克的救赎;zh-hk:月黑高飛;zh-tw:刺激1995;}-](../Page/肖申克的救赎.md "wikilink")（Shawshank
    Redemption）
  - [莎翁情史](../Page/莎翁情史.md "wikilink")（Shakespeare in Love）
  - [扫荡三人组](../Page/扫荡三人组.md "wikilink")
  - [-{zh-hans:蜘蛛侠;zh-hk:蜘蛛俠;zh-tw:蜘蛛人;}-](../Page/蜘蛛人.md "wikilink")（Spiderman）
  - [-{zh-hans:临时特工; zh-hant:神鬼拍檔;}-](../Page/临时特工.md "wikilink")
  - [-{zh-hans:生死时速; zh-tw:捍衛戰警;
    zh-hk:生死時速;}-](../Page/生死时速.md "wikilink")（Speed）
  - [十日拍拖手册](../Page/十日拍拖手册.md "wikilink")
  - [索拉里斯星](../Page/索拉里斯星_\(2002年电影\).md "wikilink")（Solaris）
  - [星球大战](../Page/星球大战.md "wikilink")（Star Wars）
  - [闪灵](../Page/闪灵.md "wikilink")（The Shining）
  - [圣女贞德](../Page/圣女贞德.md "wikilink")
  - [沙雾之宅](../Page/沙雾之宅.md "wikilink")
  - [史前巨鳄](../Page/史前巨鳄.md "wikilink")
  - [辛德勒的名单](../Page/辛德勒的名单.md "wikilink")（Schindler's List）
  - [史前一万年](../Page/史前一万年.md "wikilink")
  - [十三凶间](../Page/十三凶间.md "wikilink")
  - [深海圓疑](../Page/深海圓疑.md "wikilink")（*Sphere*）
  - [深渊](../Page/深渊.md "wikilink")
  - [山崩地裂](../Page/山崩地裂.md "wikilink")
  - [时间机器](../Page/时间机器.md "wikilink")
  - [雙虎屠龍](../Page/雙虎屠龍.md "wikilink")
  - [俠骨柔情](../Page/俠骨柔情.md "wikilink")
  - [生死戀](../Page/生死戀.md "wikilink")
  - [勝利之歌](../Page/勝利之歌.md "wikilink")
  - [雷霆救兵](../Page/雷霆救兵.md "wikilink")（Saving Private Ryan）

-----

## T

  - [星银岛](../Page/星银岛.md "wikilink")（Treasure Planet）
  - [变形金刚](../Page/变形金刚.md "wikilink")（Transformers）
  - [玩具总动员](../Page/玩具总动员.md "wikilink")（Toy Story）
  - [玩具总动员2](../Page/玩具总动员2.md "wikilink")（Toy Story 2）
  - [出租车司机](../Page/出租车司机_\(电影\).md "wikilink")（Taxi Driver）
  - [古墓丽影](../Page/古墓丽影.md "wikilink")/1 2
  - [-{zh-hans:杀死一只知更鸟; zh-hant:梅崗城故事;}-](../Page/怪屋疑雲.md "wikilink")（To
    Kill a Mockingbird）
  - [2001太空漫遊](../Page/2001太空漫遊.md "wikilink")
  - [十戒](../Page/十戒.md "wikilink")
  - [剃刀边缘](../Page/剃刀边缘.md "wikilink")
  - [天生一对](../Page/天生一对.md "wikilink")
  - [天使之城](../Page/天使之城_\(1998年電影\).md "wikilink")
  - [天才游戏](../Page/天才游戏.md "wikilink")
  - [十二猴子](../Page/十二猴子.md "wikilink")
  - [泰坦尼克号](../Page/泰坦尼克号_\(1997年电影\).md "wikilink")
  - [太阳浴血记](../Page/太阳浴血记.md "wikilink")
  - [特洛伊](../Page/特洛伊.md "wikilink")
  - [-{zh-hans:天才里普雷先生; zh-hant:天才雷普利;}-](../Page/天才里普雷先生.md "wikilink")
  - [巴拿马裁缝](../Page/巴拿马裁缝.md "wikilink")
  - [太空拖拉库](../Page/太空拖拉库.md "wikilink")
  - [太阳危机](../Page/太阳危机.md "wikilink")
  - [糖人](../Page/糖人.md "wikilink")
  - [偷窥](../Page/偷窥.md "wikilink")
  - [铁血战士](../Page/铁血战士.md "wikilink")
  - [天外来菌](../Page/天外来菌.md "wikilink")
  - [烈日風暴](../Page/烈日風暴.md "wikilink")

-----

## V

| 原名               | 台灣譯名                               | 香港譯名                                    | 國內譯名                               | 年份   |
| ---------------- | ---------------------------------- | --------------------------------------- | ---------------------------------- | ---- |
| *Van Helsing*    | [凡赫辛](../Page/凡赫辛.md "wikilink")   | [狙魔人](../Page/狙魔人_\(電影\).md "wikilink") | [范海辛](../Page/范海辛.md "wikilink")   | 2004 |
| *Vertical Limit* | [巔峰極限](../Page/巔峰極限.md "wikilink") | [終極天險](../Page/終極天險.md "wikilink")      | [垂直极限](../Page/垂直极限.md "wikilink") | 2000 |

-----

## W

  - [世界中心](../Page/世界中心.md "wikilink")
  - [风语者](../Page/风语者.md "wikilink")
  - [西城故事](../Page/西城故事_\(电影\).md "wikilink")
  - [白色夹竹桃](../Page/白色夹竹桃.md "wikilink")
  - [闻香识女人](../Page/闻香识女人.md "wikilink")
  - [万世流芳](../Page/万世流芳.md "wikilink")
  - [温柔的杀我](../Page/温柔的杀我.md "wikilink")
  - [我愛上流](../Page/我愛上流.md "wikilink")
  - [维克多](../Page/维克多.md "wikilink")
  - [乌鸦](../Page/乌鸦.md "wikilink")
  - [万劫之境](../Page/万劫之境.md "wikilink")
  - [无线广播网](../Page/无线广播网.md "wikilink")
  - [无人地带](../Page/无人地带.md "wikilink")
  - [未来水世界](../Page/未来水世界.md "wikilink")
  - [绿野仙踪](../Page/绿野仙踪.md "wikilink")
  - [蘇絲黃的世界](../Page/蘇絲黃的世界.md "wikilink")
  - [狼人](../Page/狼人.md "wikilink")
  - [小熊维尼相关电影](../Page/小熊维尼.md "wikilink")

-----

## X

  - [熊的传说](../Page/熊的传说.md "wikilink")
  - [香港女伯爵](../Page/香港女伯爵.md "wikilink")
  - [心灵访客](../Page/心灵访客.md "wikilink")
  - [训练日](../Page/训练日.md "wikilink")
  - [现代启示录](../Page/现代启示录.md "wikilink")
  - [眩晕](../Page/眩暈_\(小說\).md "wikilink")
  - [项链事件](../Page/项链事件.md "wikilink")
  - [X战警](../Page/X战警.md "wikilink")
  - [星河战队](../Page/星河战队.md "wikilink")
  - [小岛惊魂](../Page/小岛惊魂.md "wikilink")
  - [搜索者](../Page/搜索者.md "wikilink")

## Y

  - [遗言](../Page/遗言.md "wikilink")
  - [一小时快照](../Page/一小时快照.md "wikilink")
  - [雨人](../Page/雨人.md "wikilink")
  - [月之全蚀](../Page/月之全蚀.md "wikilink")
  - [云上的日子](../Page/云上的日子.md "wikilink")
  - [一夜情](../Page/一夜情.md "wikilink")
  - [永不妥协](../Page/永不妥协.md "wikilink")
  - [-{zh-hant:英雄本色; -zh-hans:勇敢的心;}-](../Page/勇敢的心.md "wikilink")

<!-- end list -->

  - [丫丫神圣秘密](../Page/丫丫神圣秘密.md "wikilink")
  - [一言不发](../Page/一言不发.md "wikilink")
  - [一切从心开始](../Page/一切从心开始.md "wikilink")
  - [野战陆军医院](../Page/野战陆军医院.md "wikilink")
  - [欲望号街车](../Page/欲望号街车.md "wikilink")
  - [雨中曲](../Page/雨中曲.md "wikilink")

<!-- end list -->

  - [一夜风流](../Page/一夜风流.md "wikilink")
  - [一个国家的诞生](../Page/一个国家的诞生.md "wikilink")
  - [一个美国人在巴黎](../Page/一个美国人在巴黎.md "wikilink")
  - [窈窕淑女](../Page/窈窕淑女.md "wikilink")
  - [与狼共舞](../Page/与狼共舞.md "wikilink")
  - [原野奇侠](../Page/原野奇侠.md "wikilink")
  - [-{zh-hans:野战排; zh-hant:前進高棉;}-](../Page/野战排.md "wikilink")
  - [野鸭汤](../Page/野鸭汤.md "wikilink")
  - [伊丽莎白](../Page/伊丽莎白.md "wikilink")
  - [游泳池谋杀案](../Page/游泳池谋杀案.md "wikilink")
  - [幽灵船](../Page/幽灵船.md "wikilink")
  - [夜空](../Page/夜空.md "wikilink")
  - [银河飞将](../Page/银河飞将.md "wikilink")
  - [眼镜蛇与斧头党](../Page/眼镜蛇与斧头党.md "wikilink")
  - [要塞風雲](../Page/要塞風雲.md "wikilink")
  - [育嬰奇譚](../Page/育嬰奇譚.md "wikilink")
  - [約克軍曹](../Page/約克軍曹.md "wikilink")
  - [螢光幕後](../Page/螢光幕後.md "wikilink")

-----

## Z

  - [战争与和平](../Page/战争与和平.md "wikilink")
  - [走出非洲](../Page/走出非洲.md "wikilink")
  - [紫醉金迷](../Page/紫醉金迷.md "wikilink")
  - [-{zh:cn:拯救大兵瑞恩; zh-hant:搶救雷恩大兵;}-](../Page/拯救大兵瑞恩.md "wikilink")
  - [针锋相对](../Page/针锋相对.md "wikilink")
  - [着魔](../Page/着魔.md "wikilink")
  - [指环王](../Page/魔戒#電影.md "wikilink")
  - [择日再死](../Page/择日再死.md "wikilink")
  - [珍珠港](../Page/珍珠港_\(电影\).md "wikilink")
  - [追魂交易](../Page/追魂交易_\(電影\).md "wikilink")
  - [侏罗纪公园](../Page/侏罗纪公园.md "wikilink")
  - [浴血金沙](../Page/浴血金沙.md "wikilink")
  - [芝加哥](../Page/芝加哥_\(电影\).md "wikilink")
  - [致命ID](../Page/致命ID.md "wikilink")
  - [再造战士](../Page/再造战士.md "wikilink")
  - [捉賊記](../Page/捉賊記.md "wikilink")
  - [壯志凌雲](../Page/壯志凌雲_\(1957年電影\).md "wikilink")
  - [戰地軍魂](../Page/戰地軍魂.md "wikilink")
  - [日落黃沙](../Page/日落黃沙.md "wikilink")

__NOTOC__

[A](../Page/#A.md "wikilink") [B](../Page/#B.md "wikilink")
[C](../Page/#C.md "wikilink") [D](../Page/#D.md "wikilink")
[E](../Page/#E.md "wikilink") [F](../Page/#F.md "wikilink")
[G](../Page/#G.md "wikilink") [H](../Page/#H.md "wikilink")
[I](../Page/#I.md "wikilink") [J](../Page/#J.md "wikilink")
[K](../Page/#K.md "wikilink") [L](../Page/#L.md "wikilink")
[M](../Page/#M.md "wikilink") [N](../Page/#N.md "wikilink")
[O](../Page/#O.md "wikilink") [P](../Page/#P.md "wikilink")
[Q](../Page/#Q.md "wikilink") [R](../Page/#R.md "wikilink")
[S](../Page/#S.md "wikilink") [T](../Page/#T.md "wikilink")
[U](../Page/#U.md "wikilink") [V](../Page/#V.md "wikilink")
[W](../Page/#W.md "wikilink") [X](../Page/#X.md "wikilink")
[Y](../Page/#Y.md "wikilink") [Z](../Page/#Z.md "wikilink")

## 外部連結

  - 《[美國電影](http://www.imdb.com/Sections/Countries/USA/)》在[網際網路電影資料庫](../Page/網際網路電影資料庫.md "wikilink")（IMDb）

{{-}}

[美国电影列表](../Category/美国电影列表.md "wikilink")
[\*](../Category/美國電影作品.md "wikilink")
[Category:美国文化列表](../Category/美国文化列表.md "wikilink")
[Category:列表索引](../Category/列表索引.md "wikilink")