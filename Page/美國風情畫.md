《**美國風情畫**》（*American
Graffiti*）是一部1973年美國[喜劇音樂片](../Page/喜劇片.md "wikilink")，為導演[喬治·盧卡斯初試影壇清新佳作](../Page/喬治·盧卡斯.md "wikilink")；講述[美國北](../Page/美國.md "wikilink")[加州一群](../Page/加州.md "wikilink")17、18歲中學生在已畢業、暑假結束前最後幾天之聚會後，便各奔前程，臨別依依之故事；片中可見年輕時的[哈里遜·福特及當時仍是演員的](../Page/哈里遜·福特.md "wikilink")《[達文西密碼](../Page/達文西密碼.md "wikilink")》導演[朗·霍華](../Page/朗·霍華.md "wikilink")；並被[AFI百年百大電影列為第](../Page/AFI百年百大電影.md "wikilink")62名，此部電影意義在於它標誌美國青少年「校園電影」類型起先第一部。

## 外部連結

  - [Kip Pullman's American Graffiti
    Site](https://www.webcitation.org/query?id=1256534609802674&url=www.geocities.com/kippullman)

  -
  -
  -
  - [Petaluma's Salute to American
    Graffiti](http://www.americangraffiti.net)

  - [Kip Pullman's American Graffiti
    Page](https://www.webcitation.org/query?id=1256534609802674&url=www.geocities.com/kippullman)

  - [American
    Graffiti](http://www.lucasfilm.com/films/other/graffiti.html.html)''
    Official Lucasfilm site

[Category:1973年電影](../Category/1973年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1970年代喜劇片](../Category/1970年代喜劇片.md "wikilink")
[Category:美國喜劇劇情片](../Category/美國喜劇劇情片.md "wikilink")
[Category:有關市郊區的電影](../Category/有關市郊區的電影.md "wikilink")
[Category:加利福尼亞州背景電影](../Category/加利福尼亞州背景電影.md "wikilink")
[Category:1960年代背景電影](../Category/1960年代背景電影.md "wikilink")
[Category:盧卡斯影業電影](../Category/盧卡斯影業電影.md "wikilink")
[Category:乔治·卢卡斯电影](../Category/乔治·卢卡斯电影.md "wikilink")