**美扭椎龍屬**（[學名](../Page/學名.md "wikilink")：*Eustreptospondylus*）又名**優椎龍**，是[斑龍科下的一](../Page/斑龍科.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生活於[侏羅紀中期](../Page/侏羅紀.md "wikilink")[卡洛維階的](../Page/卡洛維階.md "wikilink")[英格蘭南部](../Page/英格蘭.md "wikilink")，距今約1億6500萬至1億6100萬年前，當時[歐洲還是一些分散的島嶼](../Page/歐洲.md "wikilink")。牠是雙足的[肉食性恐龍](../Page/肉食性.md "wikilink")，有堅實的尾巴。牠是典型的[獸腳亞目恐龍](../Page/獸腳亞目.md "wikilink")：有強壯的後肢、直立的姿勢及小型的前肢。頭顱骨有空腔，可減輕重量，手部有三根指頭。美扭椎龍的唯一標本是個未完全成長個體，身長約4.63公尺\[1\]。

屬名在拉丁文意為「優美的彎曲脊椎骨」，指的是化石最初發現時的脊椎排列方式

## 歷史

[Eustrept1DB1.jpg](https://zh.wikipedia.org/wiki/File:Eustrept1DB1.jpg "fig:Eustrept1DB1.jpg")為食\]\]
[Eustreptospondylus_skeleton_(incomplete_skull).JPG](https://zh.wikipedia.org/wiki/File:Eustreptospondylus_skeleton_\(incomplete_skull\).JPG "fig:Eustreptospondylus_skeleton_(incomplete_skull).JPG")
美扭椎龍是由[理查德·歐文](../Page/理查德·歐文.md "wikilink")（Richard
Owen）於1841年首先描述，被認為是[斑龍的一個新](../Page/斑龍.md "wikilink")[物種](../Page/物種.md "wikilink")，並命名為居氏斑龍（*Megalosaurus
cuvieri*）。這個標本是在[英格蘭](../Page/英格蘭.md "wikilink")[牛津市北部](../Page/牛津市.md "wikilink")，[牛津黏土的一個磚窯中發現](../Page/牛津黏土.md "wikilink")，但曾一度遺失。在1964年，[艾力克·沃克](../Page/艾力克·沃克.md "wikilink")（Alick
Walker）從找回的[化石與其描述比較](../Page/化石.md "wikilink")，發現牠應該是另一個[屬](../Page/屬.md "wikilink")，並將之命名為**牛津美扭椎龍**（*E.
oxoniensis*）\[2\]。在被命名為美扭椎龍前，這個標本曾被編入[扭椎龍中](../Page/扭椎龍.md "wikilink")，並命名為居氏扭椎龍（*Streptospondylus
cuvieri*）。

在2000年，有研究人員發現美扭椎龍的[骨盆與](../Page/骨盆.md "wikilink")[大龍的只有很小差別](../Page/大龍.md "wikilink")\[3\]，故牠們被認為其實是屬於同一屬的，名為牛津大龍（*Magnosaurus
oxoniensis*）\[4\]。

目前只有發現一個美扭椎龍的化石，而且是在海相的沉積層中發現的，科學家們推論牠們的屍體是從河流沖刷到海洋中，該化石在死亡時可能還未成年\[5\]。

## 大眾文化

美扭椎龍出現於[BBC的電視節目](../Page/BBC.md "wikilink")《[與恐龍共舞](../Page/與恐龍共舞.md "wikilink")》（*Walking
with
Dinosaurs*）的第三集中，一隻在岸邊的美扭椎龍遭到躍出海面的[滑齒龍所吞食](../Page/滑齒龍.md "wikilink")；在該集最後，一隻巨大年老年滑齒龍遭暴風雨沖上陸地擱淺，死後遺體遭島嶼上的美扭椎龍吃食。節目中亦有對美扭椎龍吃食[海龜遺骸及捕食](../Page/海龜.md "wikilink")[喙嘴翼龍的描述](../Page/喙嘴翼龍.md "wikilink")。

美扭椎龍也出現於電視節目《[遠古入侵](../Page/遠古入侵.md "wikilink")》的周邊小說《*Fire and
Water*》，美扭椎龍被描繪成侏儸紀中期的頂級掠食動物，曾經游泳到小型島嶼。

## 參考資料

<div class="references-small">

<references>

</references>

  - Haines, Tim & Chambers, Paul. (2006) The Complete Guide to
    Prehistoric Life. Canada: Firefly Books Ltd.
  - <http://www.oum.ox.ac.uk/learning/pdfs/dinosaur.pdf>

</div>

[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:斑龍超科](../Category/斑龍超科.md "wikilink")

1.
2.
3.
4.
5.