**美普思科技有限公司**（， ）
是一家[CPU生產商](../Page/CPU.md "wikilink")，主要研發[MIPS架構的處理器](../Page/MIPS架構.md "wikilink")。總部位于[美國](../Page/美國.md "wikilink")。

**MIPS**自己只进行CPU的設計，之後把設計方案授權給客戶，使得客戶能够製造出高性能的CPU。

## 產品

  - MIPS® 核心
      - [32位元及](../Page/32位元.md "wikilink")[64位元CPU](../Page/64位元.md "wikilink")
  - MIPS® 架構
  - 軟體工具
  - 訓練課程

## 歷史

  - 1984年，由[史丹福大學教授](../Page/史丹福大學.md "wikilink")[約翰·軒尼詩與他的團隊一同創立](../Page/約翰·軒尼詩.md "wikilink")。
  - 1992年－MIPS科技有限公司成立。
  - 1998年－MIPS科技[股票在](../Page/股票.md "wikilink")[美國](../Page/美國.md "wikilink")[納斯達克股票交易所公開上市](../Page/納斯達克股票交易所.md "wikilink")。
  - 2007年8月16日－MIPS科技宣佈，[中国科学院计算技术研究所的](../Page/中国科学院计算技术研究所.md "wikilink")[龙芯中央处理器获得其处理器IP的全部](../Page/龙芯.md "wikilink")[专利和](../Page/专利.md "wikilink")[总线](../Page/总线.md "wikilink")、[指令集](../Page/指令集.md "wikilink")[授权](../Page/授权.md "wikilink")。
  - 2007年12月20日－MIPS科技宣佈，[揚智科技已取得其針對先進](../Page/揚智科技.md "wikilink")[多媒體所設計的可定製化](../Page/多媒體.md "wikilink")[系統單晶片](../Page/系統單晶片.md "wikilink")（SoC）核心「MIPS32
    24KEc Pro」授權。
  - 2012年11月5日—MIPS科技宣布，向AST出售498项专利后，被[Imagination
    Technologies](../Page/Imagination_Technologies.md "wikilink")[并购](../Page/并购.md "wikilink")\[1\]。

## 外部連結

<references />

  - [MIPS Technologies美國官方網站](http://www.mips.com/)
  - [MIPS
    Technologies台灣官方網站](https://web.archive.org/web/20060820194918/http://www.mips.com.tw/)
  - [MIPS科技中國官方網站](http://www.mips.com.cn/)

[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[M](../Category/森尼韋爾公司.md "wikilink")

1.  <http://www.mips.com/news-events/newsroom/newsindex/index.dot?id=76254>