[Mizuno-Crista.jpg](https://zh.wikipedia.org/wiki/File:Mizuno-Crista.jpg "fig:Mizuno-Crista.jpg")[大阪市住之江區的總部](../Page/大阪市.md "wikilink")\]\]
[2006Taipei101RunUp_Opening-10.jpg](https://zh.wikipedia.org/wiki/File:2006Taipei101RunUp_Opening-10.jpg "fig:2006Taipei101RunUp_Opening-10.jpg")」開幕致詞\]\]
**美津濃**（；）是日本最大的[體育用品及運動服裝製造商](../Page/體育用品.md "wikilink")。

1906年在[大阪創業以來](../Page/大阪.md "wikilink")，贊助過許多體育活動，小至學校的運動會，大至以[奧林匹克運動會為首的各種國際大會均有廣泛的合作](../Page/奧林匹克運動會.md "wikilink")。在世界上為屈指可數的體育用品製造大廠。

各種項目的體育用品、運動服裝都有製造販賣。而其中[棒球及](../Page/棒球.md "wikilink")[排球更是有相當多的贊助選手](../Page/排球.md "wikilink")，具有較高的市場佔有率.1993年[J聯盟創立時全聯盟所有球隊的球衣都是採用美津濃製造的](../Page/J聯盟.md "wikilink")。2008年時，[大阪櫻花](../Page/大阪櫻花.md "wikilink")、[德島漩渦](../Page/德島漩渦.md "wikilink")、[廣島三箭](../Page/廣島三箭.md "wikilink")、[愛媛FC及](../Page/愛媛FC.md "wikilink")[福岡黃蜂等五隊採用美津濃的球衣](../Page/福岡黃蜂.md "wikilink")。

現時，美津濃為[日本女子排球隊](../Page/日本女子排球隊.md "wikilink")、[日本棒球代表隊](../Page/日本棒球代表隊.md "wikilink")（男/女子）及[日本壘球代表隊提供全隊服裝](../Page/日本壘球代表隊.md "wikilink")。

美津濃在台灣的商品以慢跑鞋最為知名。

## 歷史

美津濃是由[水野利八於](../Page/水野利八.md "wikilink")1906年4月1日創辦，總部位於[日本](../Page/日本.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[住之江区](../Page/住之江区.md "wikilink")。会长[水野正人及社长](../Page/水野正人.md "wikilink")[水野明人都是水野家族中人](../Page/水野明人.md "wikilink")。

  - 事業内容：運動用品、運動服裝等各種運動相關物品的製造、批發、販賣。

## 名稱由來

創辦人水野利八是出身於[岐阜縣](../Page/岐阜縣.md "wikilink")（美濃）[大垣](../Page/大垣市.md "wikilink")，美津濃(Mizuno)就是由水野的羅馬拼音mizuno和美濃的[漢字所構成的](../Page/漢字.md "wikilink")。

## 贊助活動

  - 美津濃高爾夫職業公開賽 、美津濃高爾夫職業錦標賽
  - [台北101國際登高賽](../Page/台北101國際登高賽.md "wikilink")

## 贊助隊伍

  - 棒球
      - 國家隊：[日本棒球代表隊](../Page/日本棒球代表隊.md "wikilink")、[中華成棒代表隊](../Page/中華成棒代表隊.md "wikilink")
      - 俱樂部：[統一獅](../Page/統一獅.md "wikilink")、[兄弟象](../Page/兄弟象.md "wikilink")
  - 籃球
      - 俱樂部：[豐田汽車Alvark](../Page/豐田汽車Alvark.md "wikilink")、[JAL
        Rabbits](../Page/JAL_Rabbits.md "wikilink")
  - 排球
      - 國家隊：[日本女子排球隊](../Page/日本女子排球隊.md "wikilink")、[美國排球代表隊](../Page/美國排球代表隊.md "wikilink")
      - 俱樂部：[NEC紅火箭](../Page/NEC紅火箭.md "wikilink")、[久光製藥Springs](../Page/久光製藥Springs.md "wikilink")、[岡山海鷗](../Page/岡山海鷗.md "wikilink")、[武富士Bamboos](../Page/武富士Bamboos.md "wikilink")、[三得利Sunbirds](../Page/三得利Sunbirds.md "wikilink")、[豐田合成Trfuerza](../Page/豐田合成Trfuerza.md "wikilink")
  - 橄欖球
      - 俱樂部：[NEC綠火箭](../Page/NEC綠火箭.md "wikilink")
  - 曲棍球
      - 國家隊：[日本曲棍球代表隊](../Page/日本曲棍球代表隊.md "wikilink")
  - 田徑
      - [香港業餘田徑總會](../Page/香港業餘田徑總會.md "wikilink")
  - 其他
      - [香港殘疾人奧委會暨傷殘人士體育協會](../Page/香港殘疾人奧委會暨傷殘人士體育協會.md "wikilink")
      - [中國香港體育協會暨奧林匹克委員會](../Page/中國香港體育協會暨奧林匹克委員會.md "wikilink")
        （[2004年雅典奧運](../Page/2004年雅典奧運.md "wikilink")）

## 贊助選手

  - 棒球：[鈴木一朗](../Page/鈴木一朗.md "wikilink")、[松井秀喜](../Page/松井秀喜.md "wikilink")、[岡島秀樹](../Page/岡島秀樹.md "wikilink")、[小笠原道大](../Page/小笠原道大.md "wikilink")、[姜建銘](../Page/姜建銘.md "wikilink")、[林威助](../Page/林威助.md "wikilink")、[中田翔](../Page/中田翔.md "wikilink")
  - 足球：[阿部勇樹](../Page/阿部勇樹.md "wikilink")、[永井雄一郎](../Page/永井雄一郎.md "wikilink")、[加地亮](../Page/加地亮.md "wikilink")、[本田圭佑](../Page/本田圭佑.md "wikilink")、[冈崎慎司](../Page/冈崎慎司.md "wikilink")
  - 游泳：[北島康介](../Page/北島康介.md "wikilink")、[武田美保](../Page/武田美保.md "wikilink")、[入江陵介](../Page/入江陵介.md "wikilink")、[寺川綾](../Page/寺川綾.md "wikilink")
  - 田徑：[室伏廣治](../Page/室伏廣治.md "wikilink")、[室伏由佳](../Page/室伏由佳.md "wikilink")、[末續慎吾](../Page/末續慎吾.md "wikilink")、[內藤真人](../Page/內藤真人.md "wikilink")
  - 排球：[越川優](../Page/越川優.md "wikilink")、[高橋美雪](../Page/高橋美雪.md "wikilink")、[杉山祥子](../Page/杉山祥子.md "wikilink")、[木村沙織](../Page/木村沙織.md "wikilink")、[落合真理](../Page/落合真理.md "wikilink")
  - 乒乓球：[福原愛](../Page/福原愛.md "wikilink")、[馬琳](../Page/馬琳.md "wikilink")、[石川佳純](../Page/石川佳純.md "wikilink")
  - 體操：[富田洋之](../Page/富田洋之.md "wikilink")、[內村航平](../Page/內村航平.md "wikilink")
  - 柔道：[谷亮子](../Page/谷亮子.md "wikilink")、[鈴木桂治](../Page/鈴木桂治.md "wikilink")、[上野雅惠](../Page/上野雅惠.md "wikilink")、[野村忠宏](../Page/野村忠宏.md "wikilink")
  - 籃球：[原田裕花](../Page/原田裕花.md "wikilink")
  - 壘球：[上野由岐子](../Page/上野由岐子.md "wikilink")
  - 羽球:
    [早川賢一](../Page/早川賢一.md "wikilink")、[奧原希望](../Page/奧原希望.md "wikilink")

## 游泳隊

英文: mizuno swim team
成員:[寺川綾](../Page/寺川綾.md "wikilink")、[種田惠](../Page/種田惠.md "wikilink")

## 市場主要競爭對手

美津濃的市場對手有[Adidas](../Page/Adidas.md "wikilink")、[Asics](../Page/Asics.md "wikilink")、[New
Balance](../Page/New_Balance.md "wikilink")、[NIKE](../Page/NIKE.md "wikilink")、[Puma](../Page/Puma.md "wikilink")、[Under
Armour等](../Page/Under_Armour.md "wikilink")。

## 参考资料

## 外部連結

  - [ミズノ](https://www.mizuno.jp)（日本官網）
  - [美津浓中国官网](http://www.mizuno.com.cn)
  - [美津濃台灣官網](http://www.mizuno.com.tw)

[Category:1906年成立的公司](../Category/1906年成立的公司.md "wikilink")
[Category:日本服装公司](../Category/日本服装公司.md "wikilink")
[Category:日本鞋业公司](../Category/日本鞋业公司.md "wikilink")
[Category:運動用品製造商](../Category/運動用品製造商.md "wikilink")
[Category:日本品牌](../Category/日本品牌.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")
[Category:高爾夫球具生產商](../Category/高爾夫球具生產商.md "wikilink")