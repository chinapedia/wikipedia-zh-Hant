**美洲假吸血蝠**（[学名](../Page/学名.md "wikilink")：**），属[哺乳綱](../Page/哺乳綱.md "wikilink")[翼手目](../Page/翼手目.md "wikilink")[葉口蝠科](../Page/葉口蝠科.md "wikilink")，也是**美洲假吸血蝠屬**的唯一[物种](../Page/物种.md "wikilink")，棲息於[墨西哥](../Page/墨西哥.md "wikilink")、[中美洲及](../Page/中美洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")。其同科近亲有[窄齒長舌蝠屬](../Page/窄齒長舌蝠屬.md "wikilink")（窄齒長舌蝠）、[纓唇蝠屬](../Page/纓唇蝠屬.md "wikilink")（纓唇蝠）、[圓耳蝠屬](../Page/圓耳蝠屬.md "wikilink")（圓耳蝠）等之數種。

美洲假吸血蝠為[新大陸上最大型的蝙蝠](../Page/新大陸.md "wikilink")，同時也是最大型的[肉食性蝙蝠](../Page/肉食性.md "wikilink")，翼展可達
。具有粗壯的頭顱與牙齒，可以透過強大的[咬合力殺死獵物](../Page/咬合力.md "wikilink")，多半以[鳥類](../Page/鳥類.md "wikilink")、[囓齒類](../Page/囓齒類.md "wikilink")、[昆蟲或其他](../Page/昆蟲.md "wikilink")[蝙蝠為食](../Page/蝙蝠.md "wikilink")。

和其他大部分的蝙蝠不同，美洲假吸血蝠為[單配偶制](../Page/單配偶制.md "wikilink")，群體由一頭成年雄性與一頭成年雌性及牠們的後代所組成，雄性負責將食物帶回巢穴餵養群體，多半位於之中。由於數量稀少加上[棲息地破壞的威脅](../Page/棲息地破壞.md "wikilink")，美洲假吸血蝠已被[國際自然保護聯盟標為](../Page/國際自然保護聯盟.md "wikilink")[近危物種](../Page/近危物種.md "wikilink")。

## 描述

美洲假吸血蝠為[新大陸上最大型的蝙蝠](../Page/新大陸.md "wikilink")，同時也是世界上最大行的肉食性蝙蝠\[1\]。翼展可達
\[2\] ，[前臂長度為](../Page/前臂.md "wikilink") \[3\]，拇指長
\[4\]，上面具有類似[貓的彎曲利爪](../Page/貓.md "wikilink")\[5\]。全身體長可達
，體重約為 \[6\]\[7\]。

總體來說，美洲假吸血蝠的頭顱骨類似於縮小版的[犬科或](../Page/犬科.md "wikilink")[熊科頭顱骨](../Page/熊科.md "wikilink")\[8\]

美洲假吸血蝠的腦相對於身體來說偏大（1:67），其甚至較貓狗還高\[9\]\[10\]。

## 参考文献

[Category:葉口蝠科](../Category/葉口蝠科.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.

2.

3.

4.

5.
6.

7.
8.

9.

10.