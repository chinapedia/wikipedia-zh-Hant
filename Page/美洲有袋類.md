**美洲有袋類**，即**美洲有袋超目**（[学名](../Page/学名.md "wikilink")：），几乎所有生活在[美洲的](../Page/美洲.md "wikilink")[有袋類都属于这总目](../Page/有袋類.md "wikilink")，只有[微兽目除外](../Page/微兽目.md "wikilink")。

現時仍未確定美洲有袋類究竟是否[單系群](../Page/單系群.md "wikilink")，即牠們是否都有同一個祖先。其下的[鼩負鼠目與](../Page/鼩負鼠目.md "wikilink")[澳洲有袋類較](../Page/澳洲有袋類.md "wikilink")[負鼠目更為接近](../Page/負鼠目.md "wikilink")。

美洲有袋類與其他有袋類的分野是基於[分子生物學的數據](../Page/分子生物學.md "wikilink")，而牠們的長吻亦是其特徵。另外，由於只有美洲有袋類的[精子會組合成一對](../Page/精子.md "wikilink")，故此亦有爭議牠們是否屬於有袋類。

美洲有袋類的[齒列是](../Page/齒列.md "wikilink")[門齒](../Page/門齒.md "wikilink")5/5、[犬齒](../Page/犬齒.md "wikilink")1/1、[前臼齒](../Page/前臼齒.md "wikilink")3/3及[臼齒](../Page/臼齒.md "wikilink")4/4，可見牠們的上下顎有相同的齒列。

## 分佈及棲息地

美洲有袋類只分佈在[美洲中](../Page/美洲.md "wikilink")，差不多所有都是在[中美洲及](../Page/中美洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")，當中只有[北美負鼠在](../Page/北美負鼠.md "wikilink")[北美洲定居](../Page/北美洲.md "wikilink")。牠們的棲息地很多樣化，由潮濕草原的[熱帶](../Page/熱帶.md "wikilink")[森林至乾旱地區也有牠們的蹤影](../Page/森林.md "wikilink")。有些[物種生活在高達](../Page/物種.md "wikilink")[海拔](../Page/海拔.md "wikilink")3000米，也有些物種生活在[海平面](../Page/海平面.md "wikilink")。

## 趨同演化

美洲有袋類下的[鼩負鼠目發展出與](../Page/鼩負鼠目.md "wikilink")[老鼠相似的](../Page/老鼠.md "wikilink")[生態位](../Page/生態位.md "wikilink")。除了[哺乳動物中的](../Page/哺乳動物.md "wikilink")[食蟲目外](../Page/食蟲目.md "wikilink")，牠們的一些[物種與](../Page/物種.md "wikilink")[勞亞獸總目都有相似的形態](../Page/勞亞獸總目.md "wikilink")。另外其下已[滅絕的](../Page/滅絕.md "wikilink")[袋劍虎屬亦是](../Page/袋劍虎屬.md "wikilink")[肉食性動物的](../Page/肉食性.md "wikilink")[趨同演化例子](../Page/趨同演化.md "wikilink")。

## 歷史

[美洲有袋類與其近親的](../Page/美洲.md "wikilink")[澳洲有袋類於](../Page/澳洲.md "wikilink")1億2800萬年前至6170萬年前之間被分隔，相信牠們約在7500萬年前的[下白堊紀開始分成兩個分支](../Page/下白堊紀.md "wikilink")。

[南美洲約於](../Page/南美洲.md "wikilink")3500萬年前與[南極洲分離](../Page/南極洲.md "wikilink")，所有生活在南極洲的有袋類因寒冷的天氣而消失。而在南美洲的有袋類亦因[巴拿馬地峽的形成而進入](../Page/巴拿馬地峽.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")，与来自北美且拥有相同[生態位的](../Page/生態位.md "wikilink")[胎盘类发生竞争](../Page/胎盘类.md "wikilink")，大多数有袋类在竞争中不及现今的胎盘类而消失，當中只有[北美負鼠生存至今](../Page/北美負鼠.md "wikilink")。

## 分類

美洲有袋類其下有兩個[目](../Page/目.md "wikilink")，共有約90個[物種及](../Page/物種.md "wikilink")22個[屬](../Page/屬.md "wikilink")：

  - [負鼠目](../Page/負鼠目.md "wikilink")：包括生活在[北美洲的](../Page/北美洲.md "wikilink")[北美負鼠約有](../Page/北美負鼠.md "wikilink")80個物種。牠們有真正的袋供幼獸成長。牠們的重量可達5公斤。

<!-- end list -->

  - [鼩負鼠目](../Page/鼩負鼠目.md "wikilink")：共下有5類。牠們沒有真正的袋，而只是[皮膚上的褶層](../Page/皮膚.md "wikilink")。牠們只有幾厘米長，以[昆蟲及其他細小的](../Page/昆蟲.md "wikilink")[齧齒目為食物](../Page/齧齒目.md "wikilink")。

另外，約於200-300萬年前[滅絕的](../Page/滅絕.md "wikilink")[袋犬目亦屬於此類](../Page/袋犬目.md "wikilink")。

[Category:有袋類](../Category/有袋類.md "wikilink")
[Category:美洲有袋類](../Category/美洲有袋類.md "wikilink")