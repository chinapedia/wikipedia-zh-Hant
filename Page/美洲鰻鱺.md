**美洲鰻鱺**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鰻鱺目](../Page/鰻鱺目.md "wikilink")[鰻鱺亞目](../Page/鰻鱺亞目.md "wikilink")[鰻鱺科的其中一](../Page/鰻鱺科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於西[大西洋](../Page/大西洋.md "wikilink")，包括[拉布拉多半島](../Page/拉布拉多半島.md "wikilink")、[美國](../Page/美國.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[西印度群島](../Page/西印度群島.md "wikilink")、[加勒比海等海域](../Page/加勒比海.md "wikilink")。但數量不多。

## 深度

水深0至464公尺。

## 特徵

**美洲鰻鱺**（American eel, Anguilla
rostrata）為一種在[北美東岸發現](../Page/北美.md "wikilink")，降河迴游產卵（catadromous）[魚類](../Page/魚類.md "wikilink")。牠有一個[蛇形身體及一個細小尖銳的頭部](../Page/蛇.md "wikilink")。背部為棕色而腹部為茶黃色。牠的牙齒銳利但沒[腹鰭](../Page/腹鰭.md "wikilink")（pelvic
fin）。牠與[歐洲鰻鱺十分類似](../Page/歐洲鰻鱺.md "wikilink")，但兩者的[染色體及](../Page/染色體.md "wikilink")[脊椎骨](../Page/脊椎骨.md "wikilink")（vertebra）數目都不相同。前者脊椎骨數103至111；後者為110至119。體長可達152公分。

## 生態

[rostrata.jpg](https://zh.wikipedia.org/wiki/File:rostrata.jpg "fig:rostrata.jpg")
[雌性美洲鰻鱺在](../Page/雌性.md "wikilink")[鹽](../Page/鹽.md "wikilink")[水](../Page/水.md "wikilink")[產卵](../Page/產卵.md "wikilink")（Spawn
(biology)），並用9至10個星期令[蛋孵出](../Page/蛋.md "wikilink")。年幼鰻魚孵出後向北美移動，進入淡水系統後長成。雌性鰻魚可以每年生下4百萬浮起的蛋，但很多時在產卵後便死亡。鰻魚喜愛[淡水](../Page/淡水.md "wikilink")，可以在[大西洋岸邊發現](../Page/大西洋.md "wikilink")，包括[切薩皮克灣及](../Page/切薩皮克灣.md "wikilink")[哈德遜河](../Page/哈德遜河.md "wikilink")。鰻魚喜愛晚間獵食，在日間則在泥土、沙或砂礫中隱藏。

## 經濟價值

美洲鰻鱺在北美東岸及她們經過的河流有非常高的經濟重要性。她們被漁民捕獲、賣出、進食或作為寵物。美洲鰻鱺透過進食死魚、[無脊椎動物](../Page/無脊椎動物.md "wikilink")、[臭屍](../Page/臭屍.md "wikilink")（carrion）、[昆蟲的途徑去幫助大西洋沿岸的](../Page/昆蟲.md "wikilink")[生態系統](../Page/生態系統.md "wikilink")。在非常飢餓的情況下，她們會進食同[科的動物](../Page/科.md "wikilink")。

## 捕獲鰻魚

雖然很多垂釣者因為鰻魚的蛇狀外表而卻步，鰻魚實際上是好魚。她們通常被垂釣者釣其他魚類時捕獲到。最重的美洲鰻鱺的世界紀錄9.25磅。

## 圖庫

<File:Anguilla> rostrata.jpg <File:Rostrataluk.jpg> <File:Anguilla>
rostrata 2.jpg

## 参考文献

  -
  -
## 外部連結

  - [The American Eel, an Endangered
    Species?](https://web.archive.org/web/20080216025558/http://www.glooskapandthefrog.org/eel%20challenge.htm)
  - [ESA protection](http://www.fws.gov/northeast/ameel/index.html)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[rostrata](../Category/鰻鱺屬.md "wikilink")
[Category:大西洋鱼类](../Category/大西洋鱼类.md "wikilink")