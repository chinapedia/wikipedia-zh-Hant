**美濃加茂市**（）為日本[岐阜縣南部的](../Page/岐阜縣.md "wikilink")[市](../Page/市.md "wikilink")。誕生于1954年（昭和29年）4月1日
，由太田町、古井町、山之上村、蜂屋村、加茂野村、伊深村、三和村、下米田村、和知村合併而成。現任市長為渡邊直由。

## 地理

### 相鄰的自治体

  - [關市](../Page/關市.md "wikilink")、[可兒市](../Page/可兒市.md "wikilink")
  - [加茂郡](../Page/加茂郡.md "wikilink")[七宗町](../Page/七宗町.md "wikilink")、[坂祝町](../Page/坂祝町.md "wikilink")、[富加町](../Page/富加町.md "wikilink")、[川邊町](../Page/川邊町.md "wikilink")、[八百津町](../Page/八百津町.md "wikilink")
  - [可兒郡](../Page/可兒郡.md "wikilink")[御嵩町](../Page/御嵩町.md "wikilink")

## 巴西学校

２校有

## 外部連結

  - [日本昭和村](http://www.nihon-showamura.co.jp/)