出生於[美國](../Page/美國.md "wikilink")[田納西州](../Page/田納西州.md "wikilink")，拥有[法国](../Page/法国.md "wikilink")、[爱尔兰和](../Page/爱尔兰.md "wikilink")[印第安人血統](../Page/印第安人.md "wikilink")。是[美國知名女](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")，因为拍摄電影《[变形金刚](../Page/变形金刚_\(2007年电影\).md "wikilink")》和電視[情景喜劇](../Page/情景喜劇.md "wikilink")《希望和信仰》而出名。她被[英國男士](../Page/英國.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")《[男人幫](../Page/男人幫.md "wikilink")》選為2008及2009年度「百大性感美女」第一名\[1\]\[2\]\[3\]。

## 生平

1986年，美瑾·霍絲生在[美國](../Page/美國.md "wikilink")[田納西州](../Page/田納西州.md "wikilink")[羅恩郡](../Page/罗恩县_\(田纳西州\).md "wikilink")[橡樹嶺](../Page/橡樹嶺_\(田納西州\).md "wikilink")，擁有[法國人](../Page/法蘭西人.md "wikilink")、[愛爾蘭人](../Page/愛爾蘭人.md "wikilink")、[印第安人血統](../Page/印第安人.md "wikilink")。她小時家境並不富裕，父母離婚後與母親和繼父一起居住，對她管教頗為嚴格要她不得交男友，唸天主教學校，家中還有一位[姐姐](../Page/姐姐.md "wikilink")\[4\]。她在5歲起學習[舞蹈和](../Page/舞蹈.md "wikilink")[戲劇](../Page/戲劇.md "wikilink")。10歲時她移居至[佛羅里達州](../Page/佛羅里達州.md "wikilink")[聖露西港](../Page/聖露西港_\(佛羅里達州\).md "wikilink")，在學校常被其他女生排擠，所以她說她只有一位女性朋友。

2010年1月，她與足球巨星[基斯坦奴·朗拿度一同取代](../Page/基斯坦奴·朗拿度.md "wikilink")[大衛·碧咸夫婦](../Page/大衛·碧咸.md "wikilink")，成為奢侈時尚品牌[喬治·阿瑪尼最新的形象代言人](../Page/喬治·阿瑪尼.md "wikilink")。
梅根成名後也曾自己出錢拍公益廣告片批評[加州政府刪減教育預算減少教師人數](../Page/加州.md "wikilink")。

## 个人生活

2010年6月24日她与美国演员[布莱恩·奥斯汀·格林于](../Page/布莱恩·奥斯汀·格林.md "wikilink")[夏威夷举行私人婚礼](../Page/夏威夷.md "wikilink")，结为夫妻。

2012年9月27日生下了第一個兒子。

2014年2月14日生下了第二個兒子，此時已拍完2014年電影《[忍者龜：變種世代](../Page/忍者龜：變種世代.md "wikilink")》。

## 作品

### 电影

[Megan_Fox_at_Jennifer's_Body_TIFF.jpg](https://zh.wikipedia.org/wiki/File:Megan_Fox_at_Jennifer's_Body_TIFF.jpg "fig:Megan_Fox_at_Jennifer's_Body_TIFF.jpg")\]\]
[Megan_Fox_and_Shia_Labeouf_promoting_Transformers_in_Paris.jpg](https://zh.wikipedia.org/wiki/File:Megan_Fox_and_Shia_Labeouf_promoting_Transformers_in_Paris.jpg "fig:Megan_Fox_and_Shia_Labeouf_promoting_Transformers_in_Paris.jpg")在[巴黎宣传](../Page/巴黎.md "wikilink")《[变形金刚2](../Page/變形金剛：復仇之戰.md "wikilink")》\]\]
[Megan_Fox_at_Transformers_2_Press_Conference_in_Paris_-_1.jpg](https://zh.wikipedia.org/wiki/File:Megan_Fox_at_Transformers_2_Press_Conference_in_Paris_-_1.jpg "fig:Megan_Fox_at_Transformers_2_Press_Conference_in_Paris_-_1.jpg")在[巴黎宣传](../Page/巴黎.md "wikilink")《[变形金刚2](../Page/變形金剛：復仇之戰.md "wikilink")》\]\]

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2001</p></td>
<td><p>夏日戀曲<br />
Holiday in the Sun</p></td>
<td><p>布兰娜·华莱士<br />
Brianna Wallace</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p>絕地戰警2<br />
Bad Boys II</p></td>
<td><p>在瀑布下的比基尼女子<br />
Bikini Kid in Under Waterfall</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p>高校天后<br />
Confessions of a Teenage Drama Queen</p></td>
<td><p>卡拉<br />
Carla Santini</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
Crimes of Fashion</p></td>
<td><p>坎迪斯<br />
Candace</p></td>
<td><p><a href="../Page/電視電影.md" title="wikilink">電視電影</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/变形金刚_(2007年电影).md" title="wikilink">变形金刚</a><br />
Transformers</p></td>
<td><p>蜜琪·班斯<br />
Mikaela Banes</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>如何眾叛親離<br />
How to Lose Friends &amp; Alienate People</p></td>
<td><p>苏菲<br />
Sophie Maes</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>辣得要命<br />
Jennifer's Body</p></td>
<td><p>珍妮弗·查克<br />
Jennifer Check</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/变形金刚：復仇之戰.md" title="wikilink">变形金刚：復仇之戰</a><br />
Transformers: Revenge of the Fallen</p></td>
<td><p>蜜琪·班斯<br />
Mikaela Banes</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/西部英雄约拿·哈克斯.md" title="wikilink">西部英雄约拿·哈克斯</a><br />
Jonah Hex</p></td>
<td><p>赖丽<br />
Lilah</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p>激情遊戲<br />
Passion Play</p></td>
<td><p>莉莉<br />
Lily Luster</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p>老友有喜<br />
Friends with Kids</p></td>
<td><p>玛丽·简<br />
Mary Jane</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大鈍裁者.md" title="wikilink">大鈍裁者</a><br />
The Dictator</p></td>
<td><p>她自己</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>四十不惑<br />
This Is Forty</p></td>
<td><p>黛丝<br />
Desi</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/忍者龜：變種世代.md" title="wikilink">忍者龜：變種世代</a><br />
Teenage Mutant Ninja Turtles</p></td>
<td><p>艾波·歐尼爾<br />
April O'Neil</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p><a href="../Page/忍者龜：破影而出.md" title="wikilink">忍者龜：破影而出</a><br />
Teenage Mutant Ninja Turtles: Out of the Shadows</p></td>
<td><p>艾波·歐尼爾<br />
April O'Neil</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p><br />
Zeroville</p></td>
<td><p>Soledad Paladin</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2019</p></td>
<td><p><a href="../Page/長沙裡9.15.md" title="wikilink">長沙裡9.15</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 电视剧

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>劇名</p></th>
<th><p>角色</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2003</p></td>
<td><p>同屋姐妹<br />
What I Like About You</p></td>
<td><p>香农<br />
Shannon</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/好汉两个半.md" title="wikilink">好汉两个半</a><br />
Two and a Half Men</p></td>
<td><p>鲁登斯<br />
Prudence</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>帮助<br />
The Help</p></td>
<td><p>卡桑德拉·里奇韦<br />
Cassandra Ridgeway</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>希望和信仰<br />
Hope &amp; Faith</p></td>
<td><p>Sydney Shanowski</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p>真人秀：埋葬的生活<br />
The Buried Life</p></td>
<td><p>她自己<br />
Herself</p></td>
<td></td>
</tr>
</tbody>
</table>

### 音乐剧

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>劇名</p></th>
<th><p>角色</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2010</p></td>
<td><p>爱你说谎的方法<br />
Love the Way You Lie</p></td>
<td><p>女友<br />
The Girlfriend</p></td>
<td></td>
</tr>
</tbody>
</table>

### 廣告

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>廣告名</p></th>
<th><p>角色</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/決勝時刻：魅影.md" title="wikilink">決勝時刻：魅影 (真人廣告)</a><br />
Call Of Duty：Ghost</p></td>
<td></td>
<td><p>客串</p></td>
</tr>
</tbody>
</table>

## 奖项

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>作品</p></th>
<th><p>奖</p></th>
<th><p>类别</p></th>
<th><p>结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2005</p></td>
<td><p>《希望和信仰》</p></td>
<td><p><a href="../Page/青年艺术家奖.md" title="wikilink">青年艺术家奖</a></p></td>
<td><p>电视系列最佳表演 - 最受支持的女演员</p></td>
<td><p>rowspan="6" </p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p>《变形金刚》</p></td>
<td><p><a href="../Page/青少年选择奖.md" title="wikilink">青少年选择奖</a></p></td>
<td><p>动作冒险电影最佳演员</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>突破性的女性</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>突出表现</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>英国国家电影奖</p></td>
<td><p>最佳表演的女演员</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/MTV電影大獎.md" title="wikilink">MTV电影大奖</a></p></td>
<td><p>最佳突破表演奖</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>无</p></td>
<td><p>青少年选择奖</p></td>
<td><p>最性感女星</p></td>
<td><p>rowspan="4" [5]</p></td>
</tr>
<tr class="even">
<td><p>《变形金刚：復仇之戰》</p></td>
<td><p>夏季电影女性明星</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>尖叫奖[6]</p></td>
<td><p>最佳科幻类女演员</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《变形金刚：復仇之戰》视频游戏</p></td>
<td><p>视频游戏奖</p></td>
<td><p>最佳女性表现奖</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p>《辣得要命》和《变形金刚：堕落者的复仇》</p></td>
<td><p>第30届金酸莓奖[7]</p></td>
<td><p>2009年最差女演员</p></td>
<td><p>rowspan="4" </p></td>
</tr>
<tr class="even">
<td><p>《变形金刚：復仇之戰》</p></td>
<td><p>2009年最差银幕搭档</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>尼克儿童国际频道孩子选择奖</p></td>
<td><p>最受欢迎电影女演员</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《辣得要命》</p></td>
<td><p>MTV电影大奖</p></td>
<td><p>最雷人时刻奖</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>青少年选择奖</p></td>
<td><p>最性感女星</p></td>
<td><p>rowspan="2" </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>恐怖和惊悚片电影演员</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p>《疤面鬼煞手》</p></td>
<td><p><a href="../Page/金酸莓奖.md" title="wikilink">金酸莓奖</a></p></td>
<td><p>最差女演员</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最差银幕搭档</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部連結

  -
  -
  -
  - [美瑾·霍絲访谈](http://www.blender.com/guide/articles.aspx?ID=2714&src=dx1)

  -
  -
  -
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:美國基督徒](../Category/美國基督徒.md "wikilink")
[Category:美國女性模特兒](../Category/美國女性模特兒.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:美國女配音演員](../Category/美國女配音演員.md "wikilink")
[Category:雙性戀演員](../Category/雙性戀演員.md "wikilink")
[Category:LGBT模特兒](../Category/LGBT模特兒.md "wikilink")

1.  2008年4月25日《[明報](../Page/明報.md "wikilink")》A25頁《Megan Fox全球性感之最》
2.  《男人幫》英國版官網：[100sexiest
    - 01](http://www.fhm.com/site/100sexiest/topten/one.aspx)
3.  是次投票英文原名「100 Sexiest
    Women」，中文譯名源自《[男人幫國際中文版](http://www.fhm.com.tw/index.aspi)》。
4.
5.
6.  [Bloody-Disgusting.com](http://www.bloody-disgusting.com/news/17742),
    TV: Scream 2009 Award Winners Announced
7.