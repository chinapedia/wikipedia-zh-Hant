**美禰郡**是過去山口縣轄下的一[郡](../Page/郡.md "wikilink")，已於2008年3月21日因轄下的兩町與[美禰市](../Page/美禰市.md "wikilink")[合併後](../Page/市町村合併.md "wikilink")，轄下無管轄町村而同時廢除。

廢除時，管轄的町村包括：

  - [秋芳町](../Page/秋芳町.md "wikilink")
  - [美東町](../Page/美東町.md "wikilink")

過去美禰郡的範圍相當於現在[美禰市的大部分地區](../Page/美禰市.md "wikilink")。

## 历史

[明治維新後因大嶺煤礦的](../Page/明治維新.md "wikilink")[無煙媒及](../Page/無煙媒.md "wikilink")[石灰石的開採](../Page/石灰石.md "wikilink")，使得此地發展為工業城市。

### 沿革

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，**美禰郡**下轄：伊佐村、大嶺村、[東厚保村](../Page/東厚保村.md "wikilink")、[西厚保村](../Page/西厚保村.md "wikilink")、[於福村](../Page/於福村.md "wikilink")、大田村、[赤鄉村](../Page/赤鄉村.md "wikilink")、[綾木村](../Page/綾木村.md "wikilink")、[真長田村](../Page/真長田村.md "wikilink")、[秋吉村](../Page/秋吉村.md "wikilink")、[岩永村](../Page/岩永村.md "wikilink")、[別府村](../Page/別府村.md "wikilink")、[共和村](../Page/共和村.md "wikilink")。（13村）
  - 1923年8月1日：大田村改制為[大田町](../Page/大田町.md "wikilink")。（1町12村）
  - 1924年1月1日：伊佐村改制為[伊佐町](../Page/伊佐町.md "wikilink")。（2町11村）
  - 1939年5月1日：大嶺村改制為[大嶺町](../Page/大嶺町.md "wikilink")。（3町10村）
  - 1954年3月31日：伊佐町、大嶺町、東厚保村、西厚保村、於福村與[豐浦郡豐田前町](../Page/豐浦郡.md "wikilink")[合併為](../Page/市町村合併.md "wikilink")[美禰市](../Page/美禰市.md "wikilink")，並脫離美禰郡的範圍。（1町7村）
  - 1954年10月1日：大田町、赤鄉村、綾木村、真長田村合併為[美東町](../Page/美東町.md "wikilink")。（1町4村）
  - 1955年4月1日：秋吉村、岩永村、別府村、共和村合併為[秋芳町](../Page/秋芳町.md "wikilink")。（2町）
  - 2008年3月21日：美禰市與美東町、秋芳町合併為新設置的美禰市，同時美禰郡因無管轄町村而廢除。\[1\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>伊佐村</p></td>
<td><p>1924年1月1日<br />
改制為伊佐町</p></td>
<td><p>1954年3月31日<br />
美禰市</p></td>
<td><p>2008年3月21日<br />
美禰市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大嶺村</p></td>
<td><p>1939年5月1日<br />
改制為大嶺町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>東厚保村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>西厚保村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>於福村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大田村</p></td>
<td><p>1923年8月1日<br />
改制為大田町</p></td>
<td><p>1954年10月1日<br />
美東町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>赤鄉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>綾木村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>真長田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>秋吉村</p></td>
<td><p>1955年4月1日<br />
秋芳町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>岩永村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>別府村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>共和村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

[Category:長門國](../Category/長門國.md "wikilink")

1.