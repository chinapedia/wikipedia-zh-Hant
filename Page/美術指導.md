**美術指導**是在[電影](../Page/電影.md "wikilink")、[電視或是](../Page/電視.md "wikilink")[劇院舞台上負責為戲劇表演來設計](../Page/舞台.md "wikilink")[佈景的人](../Page/背景.md "wikilink")。由他所統籌規劃的布景，必須與這齣戲的其他所有元素（如：人物服裝、道具、劇情背景等）相吻合，才能讓表演的視覺風格達到統一\[1\]。

## 概要

美術指導通常專門決定一段演出從頭到尾的燈光、攝影、特效、服裝、道具等的互相搭配，並負責設計整體表演的視覺風格。這個職位雖然是屬於較居幕後的表演工作者，但他們的決策深深影響了這部[電影](../Page/電影.md "wikilink")、這齣戲劇、這部[動畫的視覺品質及整體氣氛](../Page/動畫.md "wikilink")，因此在前期製作上仍具有重要的地位。一位深具經驗與水準的美術指導，通常需要累積多元化的素養及經驗，這其中包含了各個風格的建築設計、各個年代的服裝認識、各種類型的繪圖技巧等等。最後，他也必須對製作預算經費精打細算，加上他對於攝影、燈光、特效、剪接的知識，才能順利將一個合年代、合地點、合人物、合事件的表演佈景完美呈現出來。

## 日本動畫的「美術指導」

美術指導在日文中稱為『**美術監督**』，[日本動畫業界中的美術監督和電影](../Page/日本動畫.md "wikilink")、戲劇方面的美術監督在工作內容上有明顯的差異。早期是出現了所謂**美術設定**(或是**美術設計**)這個專門負責決定一部動畫背景細部設計的職稱，透過美術設定，將可以大略呈現該動畫內容的世界觀。20世紀後的現在，由於電腦等數位媒體工具的普及，所以美術設計師可以將實際風景照片掃描進電腦，並加工處理成更豐富多元的動畫背景。此時的美術設定師也開始被賦予更專業的職稱：『**[背景設計](../Page/背景設計.md "wikilink")**』。

日本動畫的美術監督，通常由一些具有一定經驗與能力的美術背景設計公司人員所擔任，只有少數的監督是從[動畫師或是](../Page/動畫師.md "wikilink")[製作進行等其他部門被拔擢出來的](../Page/製作進行.md "wikilink")。前者例如[京都動畫的](../Page/京都動畫.md "wikilink")****，後者例如會社的**土橋誠**。

## 知名日本動畫美術監督

  - 西田稔：電影[追殺比爾劇中的動畫橋段](../Page/追殺比爾.md "wikilink")。
  - 池信孝：[PERFECT
    BLUE](../Page/PERFECT_BLUE.md "wikilink")、[千年女優](../Page/千年女優.md "wikilink")、[東京教父](../Page/東京教父.md "wikilink")、[妄想代理人](../Page/妄想代理人.md "wikilink")、[PAPRIKA](../Page/PAPRIKA.md "wikilink")，大多是[今敏的作品](../Page/今敏.md "wikilink")。
  - [池田繁美](../Page/池田繁美.md "wikilink")：[機動戰士V
    GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")、[犬夜叉](../Page/犬夜叉.md "wikilink")、[地球防衛少年](../Page/地球防衛少年.md "wikilink")、[無限的未知](../Page/無限的未知.md "wikilink")、[惑星奇航](../Page/惑星奇航.md "wikilink")。
  - [男鹿和雄](../Page/男鹿和雄.md "wikilink")：[妖獸都市動畫版](../Page/妖獸都市.md "wikilink")、[平成狸合戰](../Page/平成狸合戰.md "wikilink")。
  - [小倉宏昌](../Page/小倉宏昌.md "wikilink")：[王立宇宙軍](../Page/王立宇宙軍.md "wikilink")、[機動警察劇場版1與2](../Page/機動警察.md "wikilink")、[獸兵衛忍風帖](../Page/獸兵衛忍風帖.md "wikilink")、[攻殼機動隊劇場版](../Page/攻殼機動隊_\(電影\).md "wikilink")、[人狼
    JIN-ROH](../Page/人狼_JIN-ROH.md "wikilink")、[×××HOLiC](../Page/×××HOLiC.md "wikilink")。
  - 川本征平：[大雄的恐龍](../Page/大雄的恐龍.md "wikilink")、[大雄與鐵人兵團](../Page/大雄與鐵人兵團.md "wikilink")、[大雄的大魔境](../Page/大雄的大魔境.md "wikilink")，多為[哆啦A夢動畫作品](../Page/哆啦A夢_\(電視動畫\).md "wikilink")。
  - [窪田忠雄](../Page/窪田忠雄.md "wikilink")：[銀河鐵道999](../Page/銀河鐵道999.md "wikilink")、[聖鬥士星矢](../Page/聖鬥士星矢.md "wikilink")、[美少女戰士](../Page/美少女戰士.md "wikilink")、[櫻花大戰OVA第3期、第4期](../Page/櫻花大戰系列.md "wikilink")。
  - 小林七郎：[橙路](../Page/橙路.md "wikilink")、[愛的魔法](../Page/愛的魔法.md "wikilink")、[交響情人夢](../Page/交響情人夢.md "wikilink")、[幸運女神](../Page/幸運女神.md "wikilink")、[SIMOUN](../Page/SIMOUN.md "wikilink")、[君吻](../Page/君吻.md "wikilink")。
  - 中村光毅：[科學小飛俠](../Page/科學小飛俠.md "wikilink")、[風之谷](../Page/風之谷.md "wikilink")、[機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")、[逮捕令](../Page/逮捕令#電視動畫.md "wikilink")。
  - 山本二三：[未來少年科南](../Page/未來少年科南.md "wikilink")、[螢火蟲之墓](../Page/螢火蟲之墓.md "wikilink")。

## 知名日本動畫美術設計公司

  - Studio風雅：[龍龍與忠狗](../Page/世界名作劇場.md "wikilink")、[王立宇宙軍](../Page/王立宇宙軍.md "wikilink")、[亞基拉](../Page/亞基拉.md "wikilink")、[庫洛魔法使](../Page/庫洛魔法使.md "wikilink")、[交響詩篇](../Page/交響詩篇.md "wikilink")、[企業傭兵](../Page/企業傭兵.md "wikilink")。
  - Studio美峰：[魔法騎士雷阿斯](../Page/魔法騎士雷阿斯.md "wikilink")、[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")、[電影少女](../Page/電影少女.md "wikilink")、[BLOOD:THE
    LAST VAMPIRE](../Page/血戰.md "wikilink")。
  - 小林Production：小林七郎創設的美術公司。
  - 草薙：OVA版R.O.D、[星空的邂逅](../Page/星空的邂逅.md "wikilink")、[星空的邂逅2](../Page/星空的邂逅2.md "wikilink")、[鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")、[雙戀](../Page/雙戀.md "wikilink")

## 相關條目

  - [背景設計](../Page/背景設計.md "wikilink")
  - [動畫術語](../Page/動畫術語.md "wikilink")
  - [藝術指導](../Page/藝術指導.md "wikilink")

## 參考資料

## 參考文獻

  -
  -
  - Ede, Laurie N. (2010). *British film design: a history*.
    I.B.Tauris.ISBN 978-1-84-885108-5

  - Hans-Jürgen Tast (ed.) *ANTON WEBER (1904-1979) - Filmarchitekt bei
    der UFA* (Schellerten 2005) ISBN 3-88842-030-X;

  -
[美術指導](../Category/美術指導.md "wikilink") [P](../Category/動畫.md "wikilink")
[P](../Category/電影製作.md "wikilink") [P](../Category/戲劇.md "wikilink")
[P](../Category/动画术语.md "wikilink") [P](../Category/動畫製作.md "wikilink")
[Category:設計](../Category/設計.md "wikilink")

1.