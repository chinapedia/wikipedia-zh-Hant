**美西战争**，又稱**西美戰爭**（，），是1898年[美国为了夺取](../Page/美国.md "wikilink")[西班牙的](../Page/西班牙.md "wikilink")[美洲殖民地进而控制](../Page/美洲殖民地.md "wikilink")[加勒比海而发动的](../Page/加勒比海.md "wikilink")[战争](../Page/战争.md "wikilink")。

## 簡介

19世紀末，[古巴民眾開始反抗](../Page/古巴.md "wikilink")[西班牙暴虐的統治](../Page/西班牙.md "wikilink")，但官方進行殘酷的軍事鎮壓，又株連不少民眾，波及美國[僑民](../Page/僑民.md "wikilink")。消息傳來，[美國人相當憤慨](../Page/美國人.md "wikilink")，又加上[美國政府覬覦](../Page/美國政府.md "wikilink")[加勒比海已久](../Page/加勒比海.md "wikilink")，情勢緊張。

美國政府派军舰[緬因號至古巴保護僑民](../Page/緬因號戰艦_\(ACR-1\).md "wikilink")，卻在1898年2月15日於[哈瓦那近海爆炸沉没](../Page/哈瓦那.md "wikilink")。爆炸的威力巨大，几乎炸掉前侧三分之一的船体，其余的残骸迅速沉入海面。此事件造成266人死亡，其中绝大多数为士兵，爆炸时全舰[军官几乎都在舰上](../Page/军官.md "wikilink")，只有两名军官在岸上。对此次爆炸事件的起因及经过至今没有明确的调查结果，但在当时美方认定是西班牙所为，并授意新闻媒体煽动，激起美国报刊强烈反彈和國內民眾的愤怒。

1898年3月27日，美国通过驻西班牙公使，提出要求西班牙在古巴停火和取消[集中营等条件](../Page/集中营.md "wikilink")。西班牙为了避免对美作战，于4月9日宣布休战。但[美国国会发布决议](../Page/美国国会.md "wikilink")：「承认古巴独立，要求[西班牙軍隊撤出古巴](../Page/西班牙軍隊.md "wikilink")。同时授予总统使用武力的权力，并宣告美国无意兼併古巴。」4月22日，[美国海军封锁古巴港口](../Page/美国海军.md "wikilink")。[诺希维尔号军舰捕获到一艘西班牙商船](../Page/诺希维尔号.md "wikilink")。4月24日，西班牙向美国[宣战](../Page/宣战.md "wikilink")，美国於次日宣战。

战争在古巴、[波多黎各和](../Page/波多黎各.md "wikilink")[呂宋同时进行](../Page/呂宋.md "wikilink")。开战后，美海军部副部长[西奥多·罗斯福](../Page/西奥多·罗斯福.md "wikilink")（後來的[美國總統](../Page/美國總統.md "wikilink")，俗稱「老羅斯福」）辞去职位，组建志愿军第一志愿骑兵团前往参战。

在古巴，老羅斯福率第一志愿骑兵团（即[莽骑兵](../Page/莽骑兵.md "wikilink")）节节获胜，击败了西班牙在古巴的陆军一部，从而让战争的陆上形势对美国有利。西班牙海军上将[帕斯夸爾·塞韋拉指挥的加勒比海舰队在古巴圣地亚哥港被美国彻底摧毁](../Page/帕斯夸爾·塞韋拉.md "wikilink")。[圣地亚哥市向](../Page/聖地亞哥·德·古巴.md "wikilink")将军投降。

在呂宋，美国海军准将[喬治·杜威的舰队在](../Page/喬治·杜威.md "wikilink")中占领[菲律宾](../Page/菲律宾.md "wikilink")[马尼拉](../Page/马尼拉.md "wikilink")。同时，从[香港出发的美国舰队歼灭了驻守在菲律宾马尼拉港的西班牙舰队](../Page/香港.md "wikilink")。两国于12月10日在[法国](../Page/法国.md "wikilink")[巴黎签订](../Page/巴黎.md "wikilink")《巴黎和约》。根据和约，西班牙完全放弃古巴，[割让波多黎各和](../Page/割让.md "wikilink")[关岛等原](../Page/关岛.md "wikilink")[殖民地给美国](../Page/殖民地.md "wikilink")，西班牙自此完全喪失美洲殖民地。此外，西班牙須以2000万[美元的代价](../Page/美元.md "wikilink")，把[呂宋](../Page/呂宋.md "wikilink")（菲律宾）賣给美国。

战争过程中，[威廉·赫斯特的](../Page/威廉·赫斯特.md "wikilink")《新闻早报》由于采用大量离奇手法报道战争过程而一举成名，赫斯特本人也曾作出親自带领记者持槍俘虏西班牙海軍士兵的奇闻。1898年美國軍艦被炸時，他指示派往古巴的記者：「你製造新聞，我製造戰爭」，稱為「[煽色腥新聞](../Page/黃色新聞.md "wikilink")」。

## 参考文献

## 参见

  - [世界戰爭列表](../Page/世界戰爭列表.md "wikilink")
  - [黄色新闻](../Page/黄色新闻.md "wikilink")

{{-}}

[美西戰爭](../Category/美西戰爭.md "wikilink")
[Category:西班牙战争](../Category/西班牙战争.md "wikilink")
[Category:美国战争](../Category/美国战争.md "wikilink")
[Category:西班牙－美國關係](../Category/西班牙－美國關係.md "wikilink")
[Category:古美關係](../Category/古美關係.md "wikilink")
[Category:美菲關係](../Category/美菲關係.md "wikilink")
[Category:1898年衝突](../Category/1898年衝突.md "wikilink")
[Category:古巴历史](../Category/古巴历史.md "wikilink")
[Category:菲律宾历史](../Category/菲律宾历史.md "wikilink")
[Category:波多黎各历史](../Category/波多黎各历史.md "wikilink")