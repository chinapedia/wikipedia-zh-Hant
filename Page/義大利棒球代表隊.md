**義大利棒球代表隊**，是代表[義大利在國際棒球賽事中出賽的國家代表隊](../Page/義大利.md "wikilink")。

## 簡介

在[2006年世界棒球經典賽當中](../Page/2006年世界棒球經典賽.md "wikilink")，最引人注目的就是義大利隊當中，其中大多數選手都並非義大利籍，30人的名單當中有23人都是美國出生，這對於國際賽事當中算是相當少見的事，原因在於世界棒球經典賽徵召規則當中，選手的父母或祖父母有一人是該國籍就可以代表該國徵召入隊。義大利代表隊與委內瑞拉、多明尼加、澳洲同組，第一場雖然以優勢的輕易打倒澳洲，但是後兩場紛紛輸給了委內瑞拉以及多明尼加兩隊，無緣複賽。

## 名單

**教練**

  - [Matt Galante](../Page/Matt_Galante.md "wikilink")

**投手**

  - [Phil Barzilla](../Page/Phil_Barzilla.md "wikilink") -
    [圓石城快車](../Page/圓石城快車.md "wikilink") -
    [休士頓太空人小聯盟球隊](../Page/休士頓太空人.md "wikilink")
  - [Riccardo De Santis](../Page/Riccardo_De_Santis.md "wikilink") -
    [Prink Grosseto](../Page/Prink_Grosseto.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Lenny DiNardo](../Page/Lenny_DiNardo.md "wikilink") -
    [奧克蘭運動家](../Page/奧克蘭運動家.md "wikilink")
  - [Tony Fiore](../Page/Tony_Fiore.md "wikilink") -
    [托利多泥母雞](../Page/托利多泥母雞.md "wikilink") -
    [底特律老虎小聯盟球隊](../Page/底特律老虎.md "wikilink")
  - [Mike Gallo](../Page/Mike_Gallo.md "wikilink") -
    [休士頓太空人](../Page/休士頓太空人.md "wikilink")
  - [Jason Grilli](../Page/Jason_Grilli.md "wikilink") -
    [底特律老虎](../Page/底特律老虎.md "wikilink")
  - [Todd Incantalupo](../Page/Todd_Incantalupo.md "wikilink") -
    [Italeri Bologna](../Page/Italeri_Bologna.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Marc Lamacchia](../Page/Marc_Lamacchia.md "wikilink") -
    [貝克斯菲爾德火焰](../Page/貝克斯菲爾德火焰.md "wikilink") -
    [德州遊騎兵小聯盟球隊](../Page/德州遊騎兵.md "wikilink")
  - [Alessandro Maestri](../Page/Alessandro_Maestri.md "wikilink") -
    [樹城老鷹](../Page/樹城老鷹.md "wikilink") -
    [芝加哥小熊小聯盟球隊](../Page/芝加哥小熊.md "wikilink")
  - [John Mangieri](../Page/John_Mangieri.md "wikilink") - [T\&A San
    Marino](../Page/T&A_San_Marino.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Dan Miceli](../Page/Dan_Miceli.md "wikilink") -
    [坦帕灣魔鬼魚](../Page/坦帕灣魔鬼魚.md "wikilink")
  - [Fabio Milano](../Page/Fabio_Milano.md "wikilink") - [Italeri
    Bologna](../Page/Italeri_Bologna.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Kasey Olenberger](../Page/Kasey_Olenberger.md "wikilink") -
    [鹽湖城蜜蜂](../Page/鹽湖城蜜蜂.md "wikilink") -
    [洛杉磯天使小聯盟球隊](../Page/洛杉磯天使.md "wikilink")
  - [David Rollandini](../Page/David_Rollandini.md "wikilink") - [Prink
    Grosseto](../Page/Prink_Grosseto.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")

**捕手**

  - [Matt Ceriani](../Page/Matt_Ceriani.md "wikilink") - [Caffè Danesi
    Nettuno](../Page/Caffè_Danesi_Nettuno.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Thomas Gregorio](../Page/Thomas_Gregorio.md "wikilink") -
    [聖安東尼奧任務](../Page/聖安東尼奧任務.md "wikilink") -
    [西雅圖水手小聯盟球隊](../Page/西雅圖水手.md "wikilink")
  - [Mike Piazza](../Page/Mike_Piazza.md "wikilink") -
    [奧克蘭運動家](../Page/奧克蘭運動家.md "wikilink")

**內野手**

  - [Davide Dallospedale](../Page/Davide_Dallospedale.md "wikilink") -
    [Italeri Bologna](../Page/Italeri_Bologna.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Tony Giarratano](../Page/Tony_Giarratano.md "wikilink") -
    [伊利海狼](../Page/伊利海狼.md "wikilink") -
    [底特律老虎小聯盟球隊](../Page/底特律老虎.md "wikilink")
  - [Claudio Liverziani](../Page/Claudio_Liverziani.md "wikilink") -
    [Italeri Bologna](../Page/Italeri_Bologna.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Frank Menechino](../Page/Frank_Menechino.md "wikilink") -
    [路易維爾蝙蝠](../Page/路易維爾蝙蝠.md "wikilink") -
    [辛辛那提紅人小聯盟球隊](../Page/辛辛那提紅人.md "wikilink")
  - [Jairo Ramos Gizzi](../Page/Jairo_Ramos_Gizzi.md "wikilink") -
    [Prink Grosseto](../Page/Prink_Grosseto.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Mark Saccomanno](../Page/Mark_Saccomanno.md "wikilink") -
    [基督聖體市鐵勾](../Page/基督聖體市鐵勾.md "wikilink") -
    [休士頓太空人小聯盟球隊](../Page/休士頓太空人.md "wikilink")
  - [Jack Santora](../Page/Jack_Santora.md "wikilink") -
    [紐維克熊](../Page/紐維克熊.md "wikilink") -
    [獨立聯盟](../Page/獨立聯盟.md "wikilink")
  - [Vince Sinisi](../Page/Vince_Sinisi.md "wikilink") -
    [佛利斯柯義勇騎兵](../Page/佛利斯柯義勇騎兵.md "wikilink") -
    [德州遊騎兵小聯盟球隊](../Page/德州遊騎兵.md "wikilink")

**外野手**

  - [James Buccheri](../Page/James_Buccheri.md "wikilink") - [Telemarket
    Rimini](../Page/Telemarket_Rimini.md "wikilink") -
    [義大利棒球聯賽](../Page/義大利棒球聯賽.md "wikilink")
  - [Frank Catalanotto](../Page/Frank_Catalanotto.md "wikilink") -
    [多倫多藍鳥](../Page/多倫多藍鳥.md "wikilink")
  - [Dustin Delucchi](../Page/Dustin_Delucchi.md "wikilink") -
    [莫比爾海灣熊](../Page/莫比爾海灣熊.md "wikilink") -
    [聖地牙哥教士小聯盟球隊](../Page/聖地牙哥教士.md "wikilink")
  - [Val Pascucci](../Page/Val_Pascucci.md "wikilink") -
    [千葉羅德海洋](../Page/千葉羅德海洋.md "wikilink") -
    [日本職棒](../Page/日本職棒.md "wikilink")
  - [Peter Zoccolillo](../Page/Peter_Zoccolillo.md "wikilink") -
    [科羅拉多洛磯小聯盟球隊](../Page/科羅拉多洛磯.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[Category:義大利體育](../Category/義大利體育.md "wikilink")
[Category:國家棒球代表隊](../Category/國家棒球代表隊.md "wikilink")