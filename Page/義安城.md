**義安城**（[英语](../Page/英语.md "wikilink")：**Ngee Ann
City**），是[新加坡最大的](../Page/新加坡.md "wikilink")[商业](../Page/商业.md "wikilink")[购物中心之一](../Page/商場.md "wikilink")，位於[烏節路](../Page/烏節路.md "wikilink")391號，現時由[升禧環球信託持有](../Page/升禧環球信託.md "wikilink")。內有百多家[零售商品名店](../Page/零售.md "wikilink")、[餐廳及](../Page/餐廳.md "wikilink")[新加坡國家圖書館分館](../Page/新加坡國家圖書館.md "wikilink")。义安城是由[高岛屋和义安城两间百货连成一座巨型购物中心](../Page/高岛屋.md "wikilink")，日式商品齐全，其中拥有东南亚最大的[路易·威登旗舰店以及占地面积达到](../Page/路易·威登.md "wikilink")40,000[平方英尺的](../Page/平方英尺.md "wikilink")[东南亚最大书店](../Page/东南亚.md "wikilink")[纪伊国屋书店](../Page/纪伊国屋书店.md "wikilink")。

义安城內的名店有：[纪伊国屋](../Page/纪伊国屋.md "wikilink")（Books
Kinokuniya）、[博柏利](../Page/博柏利.md "wikilink")（Burberry）、[卡地亚](../Page/卡地亚.md "wikilink")（Cartier）、[香奈兒](../Page/香奈兒.md "wikilink")（Chanel）、[范思哲](../Page/范思哲.md "wikilink")（Gianni
Versace）、[雨果博斯](../Page/雨果博斯.md "wikilink")（HUGO
BOSS）、[路易·威登](../Page/路易·威登.md "wikilink")（Louis
Vuitton）、[萬寶龍](../Page/萬寶龍.md "wikilink")（Mont
Blanc）、[蒂芙尼公司](../Page/蒂芙尼公司.md "wikilink")（Tiffany
& Co.）等。

## 交通

  - [新加坡地铁](../Page/新加坡地铁.md "wikilink")[乌节站](../Page/乌节地铁站.md "wikilink")（**<font color=#FF0000>[MRT南北线](../Page/新加坡地铁南北线.md "wikilink")</font>**NS22
    Orchard）

## 著名租户

[Ngee_Ann_City_Orchard_Road2.jpg](https://zh.wikipedia.org/wiki/File:Ngee_Ann_City_Orchard_Road2.jpg "fig:Ngee_Ann_City_Orchard_Road2.jpg")
[Ngee_Ann_City_Orchard_Road.jpg](https://zh.wikipedia.org/wiki/File:Ngee_Ann_City_Orchard_Road.jpg "fig:Ngee_Ann_City_Orchard_Road.jpg")
[Ngee_Ann_City,_Fountain,_Dec_05.JPG](https://zh.wikipedia.org/wiki/File:Ngee_Ann_City,_Fountain,_Dec_05.JPG "fig:Ngee_Ann_City,_Fountain,_Dec_05.JPG")
[Ngee_Ann_City,_Takashimaya_Square,_Dec_05.JPG](https://zh.wikipedia.org/wiki/File:Ngee_Ann_City,_Takashimaya_Square,_Dec_05.JPG "fig:Ngee_Ann_City,_Takashimaya_Square,_Dec_05.JPG")
这里罗列了在义安城的著名商户。[高岛屋不在其列](../Page/高岛屋.md "wikilink")，其佔用了地下一层到4楼。

### 地下二层

  - [堡獅龍](../Page/堡獅龍.md "wikilink")

  -
  - [佐丹奴](../Page/佐丹奴.md "wikilink")

  - [摩斯漢堡](../Page/摩斯漢堡.md "wikilink")

  - [彪马](../Page/彪马.md "wikilink")

  - [胡椒厨房](../Page/胡椒厨房.md "wikilink")(Pepper Lunch)

  - [皇家运动屋](../Page/皇家运动屋.md "wikilink")(Royal Sporting House)

  - [U2](../Page/U2_\(服饰\).md "wikilink")

  - [屈臣氏](../Page/屈臣氏.md "wikilink")

  - [吉野家](../Page/吉野家.md "wikilink")

### 地下一层

  - A/X [Armani Exchange](../Page/Armani_Exchange.md "wikilink")
  - [肯德基](../Page/肯德基.md "wikilink") ([高岛屋内](../Page/高岛屋.md "wikilink"))
  - [Mango](../Page/Mango_\(clothing\).md "wikilink")
  - [麦当劳](../Page/麦当劳.md "wikilink") ([高岛屋内](../Page/高岛屋.md "wikilink"))
  - [添柏岚](../Page/添柏岚.md "wikilink")
  - [Zara](../Page/飒拉.md "wikilink")

### 一楼

  - [宝格丽](../Page/宝格丽.md "wikilink")（Bulgari，[高岛屋内](../Page/高岛屋.md "wikilink")）

  - [思琳](../Page/思琳.md "wikilink")（Celine）

  - [香奈儿](../Page/香奈儿.md "wikilink")（Chanel）

  - [克里斯汀·迪奥](../Page/克里斯汀·迪奥_\(品牌\).md "wikilink")（Dior）

  - [登喜路](../Page/登喜路公司.md "wikilink")（Dunhill）

  - [芬迪](../Page/芬迪.md "wikilink")（Fendi）

  -
  - [雨果博斯](../Page/雨果博斯.md "wikilink")（Hugo Boss）

  - [罗威](../Page/罗威.md "wikilink")（Loewe）

  - [路易·威登](../Page/路易·威登.md "wikilink")（Louis Vuitton）

  - [蒂梵尼](../Page/蒂梵尼.md "wikilink")（Tiffany & Co.）

### 二楼

  - [Armani Collezioni](../Page/Armani_Collezioni.md "wikilink")
    ([高岛屋内](../Page/高岛屋.md "wikilink"))
  - [宝格丽](../Page/宝格丽.md "wikilink")(Bulgari)([高岛屋内](../Page/高岛屋.md "wikilink"))
  - [卡地亚](../Page/卡地亚.md "wikilink")
  - 翡翠食饮上海菜餐厅
  - [杰尼亚](../Page/杰尼亚.md "wikilink")(Ermenegildo Zegna)
  - [蒂梵尼](../Page/蒂梵尼.md "wikilink")(Tiffany & Co.)
  - 新加坡歌剧画廊(Opera Gallery)

### 三楼

  - [纪伊国屋](../Page/纪伊国屋.md "wikilink")
  - [高尔夫屋](../Page/皇家运动屋.md "wikilink")
  - [Hugo Boss](../Page/Hugo_Boss.md "wikilink")

### 四楼

  - Art-Friend首饰店
  - 翡翠皇宫酒家
  - 新加坡Imperial Treasure潮州菜

### 五楼

  - [最好电器](../Page/最好电器.md "wikilink")（Best Denki）
  - [新加坡国家图书馆分馆](../Page/新加坡国家图书馆.md "wikilink")(library@orchard)

## 参考资料

  - *[海峡时报](../Page/海峡时报.md "wikilink")*, "Ngee Ann City comes alive",
    1993年8月7日
  - Tan Sung, "Takashimaya ready to face sluggish sector",
    *[海峡时报](../Page/海峡时报.md "wikilink")*, 1993年8月6日

<references />

## 外部參考

  - [义安城官方网站](https://web.archive.org/web/20070524120630/http://www.ngeeann.com.sg/webtop/chinese/property.phtml)
  - [义安城介绍](https://web.archive.org/web/20061231222844/http://www.singapore.com.tw/Nas/paperShow.asp?actions=AfterPaper&Class=4&ID=41)

[Category:新加坡旅遊景點](../Category/新加坡旅遊景點.md "wikilink")
[Category:新加坡商場](../Category/新加坡商場.md "wikilink")
[Category:1993年完工建築物](../Category/1993年完工建築物.md "wikilink")
[Category:烏節路](../Category/烏節路.md "wikilink")