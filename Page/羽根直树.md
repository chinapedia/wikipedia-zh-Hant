**羽根直树**（），[日本著名](../Page/日本.md "wikilink")[围棋棋手](../Page/围棋.md "wikilink")，[日本棋院中部本部职业九段](../Page/日本棋院.md "wikilink")，其父为著名棋手[羽根泰正九段](../Page/羽根泰正.md "wikilink")，其妻為[松岡秀樹九段的妹妹](../Page/松岡秀樹.md "wikilink")，也是職業棋士，其女[羽根彩夏亦是職業棋士](../Page/羽根彩夏.md "wikilink")，為日本圍棋史上首次祖孫三代同時為現役棋士。

## 生平

羽根出生于[三重县](../Page/三重县.md "wikilink")[志摩市](../Page/志摩市.md "wikilink")，从小跟随父亲学棋，1991年入段，2002年升为九段。他曾三次夺得[天元战冠军](../Page/天元战_\(日本围棋\).md "wikilink")（2001、2002、2003），2003年0比2负于[李昌镐获](../Page/李昌镐.md "wikilink")[春兰杯亚军](../Page/春兰杯.md "wikilink")，两次夺得[棋圣战冠军](../Page/棋圣战.md "wikilink")（2004、2005），2004年阿含桐山杯冠军，2006年夺得NHK杯，2008年三连败后四连胜，惊天大逆转击败[高尾绅路](../Page/高尾绅路.md "wikilink")，首次夺得[本因坊](../Page/本因坊.md "wikilink")，与[张栩](../Page/张栩.md "wikilink")、[山下敬吾和](../Page/山下敬吾.md "wikilink")[高尾绅路并成为](../Page/高尾绅路.md "wikilink")“平成四天王”或“若手四天王”。

羽根直树从2005年开始连续4届出席[农心杯](../Page/农心杯.md "wikilink")，2005年胜[姜东润](../Page/姜东润.md "wikilink")、[王垚](../Page/王垚.md "wikilink")，2006年胜[王磊](../Page/王磊.md "wikilink")、[曹薰铉](../Page/曹薰铉.md "wikilink")，2007年胜[彭荃](../Page/彭荃.md "wikilink")。

## 頭銜

  - [棋聖](../Page/棋聖.md "wikilink") 2期（第28、29期）
  - [本因坊](../Page/本因坊#本因坊戦.md "wikilink") 2期（第63、64期）
  - [天元](../Page/天元.md "wikilink") 3期（第27〜29期） 3連覇
  - [碁聖](../Page/碁聖#碁聖戦.md "wikilink") 1期（第36期）

### 一般棋戦

  - [NEC杯](../Page/NEC杯.md "wikilink")（第28期）
  - [阿含桐山杯](../Page/阿含桐山杯.md "wikilink")2回 （第11,16期）
  - [NHK杯](../Page/囲碁の時間#NHK杯テレビ囲碁トーナメント.md "wikilink") （第53回）
  - [王冠戦](../Page/王冠戦.md "wikilink") 11期 （第40,43〜45,48〜50,52〜55期）
  - [新鋭トーナメント戦](../Page/新鋭トーナメント戦.md "wikilink") （第26回）

### 佳績

  - [春蘭杯世界圍棋選手権戦準優勝](../Page/春兰杯世界职业围棋锦标赛.md "wikilink") （第4回）

  - 棋聖戦挑戦者決定戦出場 （第31期）

  - [名人挑戦者](../Page/名人.md "wikilink") （第37期）

  - 本因坊挑戦者 （第66期）

  - [王座挑戦者](../Page/王座.md "wikilink") （第59期）

  - 準優勝（第15回）

## 年表

<table>
<thead>
<tr class="header">
<th></th>
<th><p>棋聖</p></th>
<th><p>十段</p></th>
<th><p><a href="../Page/本因坊.md" title="wikilink">本因坊</a></p></th>
<th><p><a href="../Page/碁聖.md" title="wikilink">碁聖</a></p></th>
<th><p>名人</p></th>
<th><p>王座</p></th>
<th><p>天元</p></th>
<th><p>棋道賞</p></th>
<th><p>賞金対局料</p></th>
<th><p>備考</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>棋聖戦<br />
1-3月</p></td>
<td><p>十段戦<br />
3-4月</p></td>
<td><p>本因坊戦<br />
5-7月</p></td>
<td><p>碁聖戦<br />
6-8月</p></td>
<td><p>名人戦<br />
9-11月</p></td>
<td><p>王座戦<br />
10-12月</p></td>
<td><p>天元戦<br />
10-12月</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>新人</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>勝</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>勝</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>連</p></td>
<td><p>1454 (13位)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>style="background-color:#ffffbc"|4位</p></td>
<td></td>
<td></td>
<td></td>
<td><p>1944 (11位)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td></td>
<td></td>
<td><p>4位</p></td>
<td></td>
<td><p>style="background-color:#ffffbc"|陥落</p></td>
<td></td>
<td><p>style="background-color:#ffcccc| <a href="../Page/柳時熏.md" title="wikilink">柳時熏</a></p></td>
<td><p>優 勝 対</p></td>
<td><p>3567 (6位)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>2位</p></td>
<td></td>
<td><p>陥落</p></td>
<td></td>
<td></td>
<td></td>
<td><p>style="background-color:#ffcccc| <a href="../Page/趙善津.md" title="wikilink">趙善津</a></p></td>
<td></td>
<td><p>3272 (6位)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>2位</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>style="background-color:#ffcccc| <a href="../Page/山下敬吾.md" title="wikilink">山下敬吾</a></p></td>
<td><p>優</p></td>
<td><p>4120 (5位)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>style="background-color:#ffcccc| 山下敬吾</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>style="background-color:#c1ffff| 山下敬吾</p></td>
<td></td>
<td><p>6783 (2位)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>style="background-color:#ffcccc| <a href="../Page/結城聡.md" title="wikilink">結城聡</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>優</p></td>
<td><p>5565 (2位)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>style="background-color:#c1ffff| 山下敬吾</p></td>
<td></td>
<td><p>プレーオフ</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>4649 (4位)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>1位</p></td>
<td></td>
<td><p>陥落</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2027 (8位)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p>4位</p></td>
<td></td>
<td><p>style="background-color:#ffcccc| <a href="../Page/高尾紳路.md" title="wikilink">高尾紳路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>優</p></td>
<td><p>4817 (3位)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>3位</p></td>
<td></td>
<td><p>style="background-color:#ffcccc| 高尾紳路</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>7179 (2位)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>3位</p></td>
<td></td>
<td><p>style="background-color:#c1ffff| 山下敬吾</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2807 (5位)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p>3位</p></td>
<td></td>
<td><p>style="background-color:#c1ffff| 山下敬吾</p></td>
<td><p>style="background-color:#ffcccc| <a href="../Page/坂井秀至.md" title="wikilink">坂井秀至</a></p></td>
<td><p>style="background-color:#ffffbc"|プレーオフ</p></td>
<td><p>style="background-color:#c1ffff| <a href="../Page/張栩.md" title="wikilink">張栩</a></p></td>
<td></td>
<td><p>優</p></td>
<td><p>4299 (4位)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p>3位</p></td>
<td></td>
<td><p>陥落</p></td>
<td><p>style="background-color:#c1ffff| <a href="../Page/井山裕太.md" title="wikilink">井山裕太</a></p></td>
<td><p>style="background-color:#c1ffff| 山下敬吾</p></td>
<td></td>
<td></td>
<td></td>
<td><p>3288 (5位)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>2位</p></td>
<td></td>
<td></td>
<td></td>
<td><p>style="background-color:#ffffbc"|4位</p></td>
<td></td>
<td></td>
<td></td>
<td><p>1405 (7位)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>4位</p></td>
<td></td>
<td></td>
<td></td>
<td><p>style="background-color:#ffffbc"|5位</p></td>
<td></td>
<td></td>
<td></td>
<td><p>1347 (8位)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>陥落</p></td>
<td></td>
<td></td>
<td></td>
<td><p>style="background-color:#ffffbc"|陥落</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部链接

  - [日本棋院羽根直樹介绍](http://www.nihonkiin.or.jp/player/htm/ki000307.htm)

[Category:日本围棋棋手](../Category/日本围棋棋手.md "wikilink")
[Category:三重县出身人物](../Category/三重县出身人物.md "wikilink")
[Category:本因坊](../Category/本因坊.md "wikilink")