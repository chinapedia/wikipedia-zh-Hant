[Tsui_Lam_Estate_Amphitheater.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Estate_Amphitheater.jpg "fig:Tsui_Lam_Estate_Amphitheater.jpg")
[Tsui_Lam_Estate_Sculpture.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Estate_Sculpture.jpg "fig:Tsui_Lam_Estate_Sculpture.jpg")
[Tsui_Lam_Community_Hall.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Community_Hall.jpg "fig:Tsui_Lam_Community_Hall.jpg")
[Tsui_Lam_Estate_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Estate_Basketball_Court.jpg "fig:Tsui_Lam_Estate_Basketball_Court.jpg")
[Tsui_Lam_Estate_Volleyball_Court.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Estate_Volleyball_Court.jpg "fig:Tsui_Lam_Estate_Volleyball_Court.jpg")
[Tsui_Lam_Estate_Playground.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Estate_Playground.jpg "fig:Tsui_Lam_Estate_Playground.jpg")
[King_Ming_Court_Landscape.jpg](https://zh.wikipedia.org/wiki/File:King_Ming_Court_Landscape.jpg "fig:King_Ming_Court_Landscape.jpg")
[Tsui_Lam_Estate_Opening_Plaque.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Estate_Opening_Plaque.jpg "fig:Tsui_Lam_Estate_Opening_Plaque.jpg")[衛奕信爵士於](../Page/衛奕信.md "wikilink")1989年11月13日主持開幕\]\]
[Tsui_Lam_Estate_Car_Park_Roof.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Estate_Car_Park_Roof.jpg "fig:Tsui_Lam_Estate_Car_Park_Roof.jpg")
[Tsui_Lam_Estate_Car_Park.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Estate_Car_Park.jpg "fig:Tsui_Lam_Estate_Car_Park.jpg")

**翠林邨**（英語：**Tsui Lam
Estate**）為[香港](../Page/香港.md "wikilink")[將軍澳](../Page/將軍澳.md "wikilink")[翠林西北的](../Page/翠林.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，屬於[香港房屋委員會下的屋邨](../Page/香港房屋委員會.md "wikilink")。並在1988年落成，是繼[寶林邨後的第二條公共屋邨](../Page/寶林邨.md "wikilink")，也是[將軍澳新市鎮唯一不是建於填海區的公共屋邨](../Page/將軍澳新市鎮.md "wikilink")\[1\]。2005年8月於租者置其屋計劃最後一期把邨內部份單位出售予租戶，已成立業主立案法團管理屋邨事務。

**景明苑**（英語：**King Ming
Court**）是一個[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")，在翠林邨旁邊，共有三座樓宇，在1988年落成。

## 位置及歷史

翠林邨和景明苑位於將軍澳[寶琳區西的山上](../Page/寶琳.md "wikilink")，與[康盛花園相望](../Page/康盛花園.md "wikilink")。位於[翠琳路及](../Page/翠琳路.md "wikilink")[寶琳北路的交界上](../Page/寶琳路.md "wikilink")。

在1985年，將軍澳發展第一期的填海區工程完成。1988年將軍澳第一個公共屋邨寶林邨正式落成，其後在山上的翠林邨也落成了。因寶林邨有沉降的問題，所以部份居民被安排移至山上的翠林邨，因此此邨漸有生氣。

同年景明苑亦落成入伙，由於景明苑和翠林邨只是一條馬路之隔，而且和翠林邨一樣都是居高臨下，與相鄰的寶林邨有一段距離，故居民日常購物和出外均依賴翠林邨的設施。

## 屋邨資料

### 翠林邨

| 樓宇名稱       | 樓宇類型                             | 落成年份 |
| ---------- | -------------------------------- | ---- |
| 彩林樓        | [Y2型](../Page/Y2型.md "wikilink") | 1988 |
| 輝林樓        |                                  |      |
| 雅林樓        |                                  |      |
| 欣林樓        |                                  |      |
| 碧林樓（連接秀林樓） | [新長型](../Page/新長型.md "wikilink") |      |
| 秀林樓（連接碧林樓） |                                  |      |
| 康林樓（連接安林樓） |                                  |      |
| 安林樓（連接康林樓） |                                  |      |
|            |                                  |      |

Tsui Lam Estate.jpg|翠林邨東北面 Tsui Lam Estate 2013 11 part2.JPG|行人天橋 Tsui
Lam Estate Playground (2).jpg|兒童遊樂場（2） Tsui Lam Estate Pavilion.jpg|涼亭
Tsui Lam Estate Gym Zone.jpg|健體區 Tsui Lam Estate Gym Zone (2).jpg|健體區（2）
Tsui Lam Estate Pebble Walking Trail.jpg|卵石路步行徑

### 景明苑

| 樓宇名稱\[2\] | 樓宇類型                               | 落成年份 |
| --------- | ---------------------------------- | ---- |
| 曦景閣       | [新十字型](../Page/新十字型.md "wikilink") | 1988 |
| 暉景閣       |                                    |      |
| 旭景閣       |                                    |      |
|           |                                    |      |

King Ming Court after renovation.jpg|翻新後的景明苑 King Ming 2013 11
part2.JPG|翻新前的景明苑 King Ming Court renovation works.jpg|景明苑翻新工程 King Ming
Court Playground.jpg|兒童遊樂場 King Ming Court Covered Walkway.jpg|有蓋行人通道
King Ming Court Chess Zone.jpg|棋藝區 King Ming Court Gym Zone.jpg|健體區

### 公共設施

#### 翠林商場

[Tsui_Lam_Square_Entrance_and_5th_Floor.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Square_Entrance_and_5th_Floor.jpg "fig:Tsui_Lam_Square_Entrance_and_5th_Floor.jpg")
[Tsui_Lam_Square_Ground_Floor.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Square_Ground_Floor.jpg "fig:Tsui_Lam_Square_Ground_Floor.jpg")
翠林商場曾為[領展房地產投資信託基金持有](../Page/領展房地產投資信託基金.md "wikilink")，總樓面約8.77萬方呎，另有711個車位。2014年9月領展推出招標，由林子峰和陳秉志合資財團以6.5億元投得，並於2015年斥資近1億元進行翻新工程，多間店鋪因無法承擔貴租而被迫結業。

翠林商場及街市翻新後分別改稱為翠林新城（Tsui Lam Square）及匯翠坊（Tsui Lam Market
Place，由外判商管理），五樓及六樓出租給酒樓及聯誼會。\[3\]

商場地下即一樓是[巴士總站](../Page/翠林巴士總站.md "wikilink")；二樓是街市（曾由[領展管理](../Page/領展.md "wikilink")）並連接[景明苑](../Page/景明苑.md "wikilink")；三樓是商店及連接停車場，四樓不設[電梯連接](../Page/電梯.md "wikilink")，是商場辦事處、議員譚領律的辦公室及[大家樂的雅座](../Page/大家樂.md "wikilink")（已關閉）；五樓是商場骨幹，所有商場的商店是連接所有樓宇的集中地。該層亦設酒樓。商舖以連鎖店為主，包括優品360°、Union和[萬寧等](../Page/萬寧.md "wikilink")。

不過現時商場管理每況愈下，西貢區議員譚領律稱翻新初期仍有大量推廣活動，但自2018年起，場內的冷氣和扶手設備損壞兩個月也未有人修理，令商戶生意慘淡。\[4\]居民批評出入非常不便。

Tsui Lam Square 2nd Floor.jpg|2樓 Tsui Lam Square 3rd Floor.jpg|3樓 Tsui
Lam Square 5th Floor (2).jpg|5樓 Tsui Lam Square 6th Floor.jpg|6樓 Sau Lam
House Shops.jpg|秀林樓商店

  - 翻新前的翠林商場

Tsui Lam 1.jpg|地下正門 Tsui Lam Road.jpg|翻油前的外貌 Tsui Lam Shopping
Centre.JPG|5樓

#### 匯翠坊

[Tsui_Lam_Market_Place.jpg](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Market_Place.jpg "fig:Tsui_Lam_Market_Place.jpg")
[Tsui_Lam_Market.JPG](https://zh.wikipedia.org/wiki/File:Tsui_Lam_Market.JPG "fig:Tsui_Lam_Market.JPG")
翠林街市自2014年易手後，管理公司大幅加租，多間經營近27年的商戶被拒絕承租和無法負擔昂貴租金，被迫結業和退休。街坊批評管理公司做法逼使商戶結業。\[5\]街市翻新後外判予「好眼光有限公司」承包管理。當中魚檔和豬肉檔疑為「好眼光」公司屬下經營。現時街市人流不多，至少有6間店舖空置中。

Tsui Lam Market Place (2).jpg|餐廳及水果檔 Tsui Lam Market Place
(3).jpg|凍肉及海味雜貨檔 Tsui Lam Market Place (4).jpg|小食檔及五金店 Tsui
Lam Market Place (5).jpg|燒臘及糕點檔 Tsui Lam Market Place Entrance.jpg|入口

  - [翠林體育館](../Page/翠林體育館.md "wikilink")
  - 翠林[停車場](../Page/停車場.md "wikilink")
  - 翠林社區會堂
      - 連接商場二至四樓。
  - 康林及安林[停車場](../Page/停車場.md "wikilink")

## 教育及福利設施

### 幼稚園

  - [明愛翠林幼兒學校](http://tlns.caritas.org.hk)（1989年創辦）（位於碧林樓306-313號）
  - [翠林邨浸信會幼稚園](http://www.tlebk.edu.hk)（1989年創辦）（位於雅林樓地下）

<!-- end list -->

  - *已結束*

<!-- end list -->

  - 香港互勵會聖嘉諾幼稚園（1988年創辦）（位於彩林樓地下）

### 小學

  - [拔萃女小學](../Page/拔萃女小學.md "wikilink")（2009年4月-2011年8月為臨時校舍）\[6\]

### 部分已停辦的小學

  - [葛量洪校友會將軍澳學校](../Page/葛量洪校友會將軍澳學校.md "wikilink")（於政府殺校時已停用，現為青年學院）
  - [香港道教聯合會湯鄧淑芳紀念學校](../Page/香港道教聯合會湯鄧淑芳紀念學校.md "wikilink")（於政府殺校時已停用，2009年4月-2011年8月為[拔萃女小學臨時校舍](../Page/拔萃女小學.md "wikilink")，現為[香港中文大學專業進修學院將軍澳教學中心](../Page/香港中文大學專業進修學院.md "wikilink")）

### 特殊學校

  - [匡智翠林晨崗學校](../Page/匡智翠林晨崗學校.md "wikilink")

### 職業訓練學校

  - [青年學院](../Page/青年學院.md "wikilink")

### 專上教育

  - [香港中文大學專業進修學院](../Page/香港中文大學專業進修學院.md "wikilink")

### 長者鄰舍中心

  - [保良局黃祐祥紀念耆暉中心](http://elderly.poleungkuk.org.hk/tc/page.aspx?pageid=54)（位於欣林樓地下A翼109-116室）

### 綜合家居照顧服務

  - [明愛將軍澳綜合家居照顧服務](https://www.caritasse.org.hk/services/main/enhancedho/tkoih.html)（位於碧林樓208-210室）

### 長者日間護理中心

  - [靈實翠林老人日間活動中心](https://www.hohcs.org.hk/content_34.html)（位於康林樓306室）
  - [基督教家庭服務中心翠林長者日間護理中心](http://www.cfsc.org.hk/tlde/)（位於彩林樓地下101,
    108-116室）

### 安老院

  - [耆康會東蓮覺苑護理安老院](http://www.sage.org.hk/Service/UnitServe/ElderlyHomes/tlkycaCenter.aspx)（位於安林樓4樓）

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/翠林巴士總站.md" title="wikilink">翠林巴士總站</a></dt>

</dl>
<dl>
<dt><a href="../Page/寶琳北路.md" title="wikilink">寶琳北路</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 本邨區議員

雖然翠林邨和景明苑只是一條馬路之隔，但卻屬於西貢區議會內不同選區。翠林邨屬Q14 翠林選區，而景明苑屬Q13
康景選區（**康**指康盛花園，**景**指景明苑）。

### Q16翠林

  - 石志強（），1996-2007
  - [譚領律](../Page/譚領律.md "wikilink")（[公民力量](../Page/公民力量.md "wikilink")），2008-今

### Q15康景

  - [羅祥國](../Page/羅祥國.md "wikilink")（[公民力量](../Page/公民力量.md "wikilink")），1996-2007
  - [林少忠](../Page/林少忠.md "wikilink")（→[新民主同盟](../Page/新民主同盟.md "wikilink")），2008-今

## 參考

<div class="references-small">

<references />

</div>

## 外部連結

  - [房委會翠林邨資料](http://www.housingauthority.gov.hk/b5/interactivemap/estate/0,,3-347-8_4923,00.html)
  - 香港租者置其屋末期9月推出
    [大紀元](http://www.epochtimes.com/b5/5/8/29/n1033916.htm)

[en:Public housing estates in Tseung Kwan O\#Tsui Lam
Estate](../Page/en:Public_housing_estates_in_Tseung_Kwan_O#Tsui_Lam_Estate.md "wikilink")

[Category:將軍澳](../Category/將軍澳.md "wikilink")
[Category:租者置其屋計劃屋邨](../Category/租者置其屋計劃屋邨.md "wikilink")
[Category:西貢區商場](../Category/西貢區商場.md "wikilink")
[Category:1988年完工建築物](../Category/1988年完工建築物.md "wikilink")

1.  《西貢歷史與風物》， 馬木池、張兆和、黃永豪、廖迪生、劉義章、蔡志祥 著，西頁區議會 出版，2003年9月，第69頁。ISBN
    988-97421-1-X。
2.  [景明苑](http://www.housingauthority.gov.hk/b5/interactivemap/court/0,,3-0-8_5609,00.html)
3.  [聯誼會5倍租進駐領展前商場 佔翠林商場三成半樓面
    擬設24小時麻將耍樂](http://www.mpfinance.com/fin/daily2.php?node=1468869320628&issue=20160719)
    明報，2016年7月19日
4.
5.
6.  <http://www.dgjs.edu.hk/information.html>  (2009/4/17)