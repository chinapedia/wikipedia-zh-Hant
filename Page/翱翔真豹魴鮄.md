**翱翔真豹魴鮄**（[学名](../Page/学名.md "wikilink")：**）為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮋形目](../Page/鮋形目.md "wikilink")[飛角魚亞目](../Page/飛角魚亞目.md "wikilink")[飛角魚科的其中一](../Page/飛角魚科.md "wikilink")[種](../Page/種.md "wikilink")，是[熱帶至](../Page/熱帶.md "wikilink")[溫帶的](../Page/溫帶.md "wikilink")[魚類](../Page/魚類.md "wikilink")，分佈在[大西洋的兩岸](../Page/大西洋.md "wikilink")，北至[北卡羅萊納州及南至](../Page/北卡羅萊納州.md "wikilink")[巴西](../Page/巴西.md "wikilink")，及由[英倫海峽至](../Page/英倫海峽.md "wikilink")[安哥拉](../Page/安哥拉.md "wikilink")。

翱翔真豹魴鮄的[胸鰭十分大及像扇子](../Page/胸鰭.md "wikilink")，前六枝棘分開像細小的鰭葉。牠們亦會在沙灘、泥漿或石澗出沒，以胸鰭下的部份來尋找食物。牠們主要是吃[底棲區的](../Page/底棲區.md "wikilink")[甲殼類](../Page/甲殼類.md "wikilink")，尤其是[蟹](../Page/蟹.md "wikilink")、[蛤蜊及細小的魚類](../Page/蛤蜊.md "wikilink")。牠們並沒有毒腺，可做為食用魚及觀賞魚。

## 外部連結

  - <http://www.gma.org/fogm/Dactylopterus_volitans.htm>

  - <http://filaman.ifm-geomar.de/Summary/SpeciesSummary.php?id=1021>

  -
[Category:食用魚](../Category/食用魚.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[Dactylopterus](../Category/飛角魚科.md "wikilink")
[Category:單種屬魚類](../Category/單種屬魚類.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")