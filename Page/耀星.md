**耀星**是一種[變星](../Page/變星.md "wikilink")，它可以不可預知的在數分鐘內戲劇性的急遽增光，有時在幾分鐘內的改變會大於幾個[星等以上](../Page/星等.md "wikilink")，並持續幾分鐘到幾小時後又慢慢復原。它被認為與[太陽閃焰類似](../Page/太陽閃焰.md "wikilink")，是由於在[恆星大氣層內的](../Page/恆星.md "wikilink")[磁重聯](../Page/磁重聯.md "wikilink")。亮度的增加跨越了整個[光譜](../Page/電磁頻譜.md "wikilink")，從[X射線到](../Page/X射線.md "wikilink")[無線電波](../Page/無線電波.md "wikilink")。第一批耀星（[天鵝座V1396和](../Page/天鵝座V1396.md "wikilink")[顯微鏡座AT](../Page/顯微鏡座AT.md "wikilink")）
是在1924年發現的；然而，最著名的耀星是在1948年發現的''[鯨魚座UV](../Page/鯨魚座UV.md "wikilink")
*。如今，相似的耀星在變星目錄上，像是[變星總表都被分類為](../Page/變星總表.md "wikilink")*鯨魚座UV*型[變星](../Page/變星.md "wikilink")（使用上縮寫為*UV''）。耀斑可以隔幾天就發生\[1\]
，或是頻率非常低，像[巴納德星](../Page/巴納德星.md "wikilink")。

雖然最近的研究表明質量更小的[棕矮星也可能發生閃焰](../Page/棕矮星.md "wikilink")，但大多數的耀星都是暗淡的[紅矮星](../Page/紅矮星.md "wikilink")。質量更大的[獵犬座RS型變星](../Page/獵犬座RS型變星.md "wikilink")（RS
CVn）已知也是耀星，但據了解這些閃焰是由聯星系統中的伴星造成的[磁場糾纏誘發的](../Page/磁場.md "wikilink")。此外，也觀察到9顆類似[太陽的恆星曾經歷閃焰的事件](../Page/太陽.md "wikilink")\[2\]。曾經有建議指出在類似RS
CVn變星誘發閃焰的機制，是有看不見的，大小類似木星的行星，在一個緊密的軌道上繞著恆星運轉\[3\]。目前在太陽系附近已發現近100顆耀星。

## 鄰近的耀星

本質上，耀星都很暗淡，只能發現距離地球1,000[光年以內的](../Page/光年.md "wikilink")\[4\]。

### 半人馬座比鄰星

距離最接近太陽的鄰居，半人馬座[比鄰星](../Page/比鄰星.md "wikilink")，是一顆經由恆星磁場的作用，會隨機增加亮度的耀星\[5\]。恆星的磁場是由恆星本身的[對流產生的](../Page/對流.md "wikilink")，並由此造成閃焰的活動，生成的[X射線總輻射量與太陽產生的相似](../Page/X射線.md "wikilink")\[6\]。

### 沃夫359

[沃夫359是另一顆鄰近的焰星](../Page/沃夫359.md "wikilink")（2.39 ±
0.01秒差距）。[沃夫359也稱為格利澤](../Page/沃夫359.md "wikilink")406或獅子座CN，它是[光譜類型為M](../Page/光譜類型.md "wikilink")6.5，輻射出X射線的[紅矮星](../Page/紅矮星.md "wikilink")\[7\]。它是[鯨魚座UV型的耀星](../Page/魯坦726-8.md "wikilink")\[8\]，並且有相對較高的閃焰頻率。

它的平均磁場強度大約為（），但是在短短的6小時的時間尺度內就會有很大的差異\[9\]。相較之下，[太陽的磁場強度平均只有](../Page/太陽.md "wikilink")（），但是在[太陽黑子活動的區域可以達到](../Page/太陽黑子.md "wikilink")
()\[10\]。

### 巴納德星

[巴納德星是距離太陽第二近的恆星](../Page/巴納德星.md "wikilink")，它也被懷疑是一顆焰星。

### TVLM513-46546

質量非常小的[TVLM513-46546](../Page/TVLM513-46546.md "wikilink")，只比紅矮星的質量下限略重一些。

## 相關條目

  - [太陽閃焰](../Page/太陽閃焰.md "wikilink")
  - [超級閃焰](../Page/超級閃焰.md "wikilink")
  - [變星](../Page/變星.md "wikilink")

## 參考資料

## 外部連結

  - [UV Ceti and the flare stars, Autumn 2003 Variable Star Of The
    Season](http://www.aavso.org/vsots_uvcet) prepared by Dr. Matthew
    Templeton, AAVSO(www.aavso.org)
  - [Stellar
    Flares](http://www.ucm.es/info/Astrof/invest/actividad/flares.html)
    - D. Montes, UCM.

[Category:耀星](../Category/耀星.md "wikilink")
[Category:变星](../Category/变星.md "wikilink")
[Category:恆星類型](../Category/恆星類型.md "wikilink")

1.  <http://www.solstation.com/stars/alp-cent3.htm>
2.
3.
4.
5.
6.
7.
8.
9.
10.