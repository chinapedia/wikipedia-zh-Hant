**老约瑟夫·帕特里克·“乔”·肯尼迪**（**Joseph Patrick "Joe" Kennedy,
Sr.**，1888年9月6日[波士顿](../Page/波士顿_\(马萨诸塞州\).md "wikilink")
-
1969年11月18日。[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[海厄尼斯](../Page/海厄尼斯_\(马萨诸塞州\).md "wikilink")），[美国商人](../Page/美国.md "wikilink")、政治家，[美国民主党成员](../Page/美国民主党.md "wikilink")，曾任[美国驻英国大使](../Page/美国驻英国大使.md "wikilink")（1938年-1940年）、[美国证券交易委员会主席](../Page/美国证券交易委员会.md "wikilink")（1934年-1935年）。
约瑟夫是[肯尼迪家族的第二代成员](../Page/肯尼迪家族.md "wikilink")，[派屈克·J·甘迺迪的長子](../Page/派屈克·J·甘迺迪.md "wikilink")。

## 婚姻与家庭

1914年，约瑟夫与[波士顿市长](../Page/波士顿市长.md "wikilink")[约翰·F·菲茨杰拉德之女](../Page/约翰·F·菲茨杰拉德.md "wikilink")[罗丝·菲茨杰拉德结婚](../Page/罗丝·肯尼迪.md "wikilink")，婚后共育有四儿五女。

  - [小约瑟夫·P·肯尼迪](../Page/小约瑟夫·P·肯尼迪.md "wikilink")（1915年7月25日-1944年8月12日）：第二次世界大战中牺牲
  - [约翰·F·肯尼迪](../Page/约翰·F·肯尼迪.md "wikilink")（1917年5月29日-1963年11月22日）：美国第35任总统
  - 罗斯玛丽（1918年9月13日-2005年1月7日）
  - 凯瑟琳（1920年2月20日-1948年5月13日）
  - 尤妮斯（1921年7月10日-2009年8月11日）
  - 帕特里夏（1923年5月6日-2006年9月17日）
  - [罗伯特·F·肯尼迪](../Page/罗伯特·F·肯尼迪.md "wikilink")（1925年11月20日-1968年6月6日）：美国司法部部长、参议员
  - [珍妮·肯尼迪·史密斯](../Page/珍妮·肯尼迪·史密斯.md "wikilink")（1928年2月20日）：美国驻爱尔兰大使
  - [泰德·甘迺迪](../Page/泰德·甘迺迪.md "wikilink")（1932年2月22日-2009年8月25日）：美國聯邦參議員

## 外部链接

  - [Joe Kennedy's Political Influence](http://www.ytedk.com/jfk.htm)
  - [The Kennedys - PBS
    Special](http://www.pbs.org/wgbh/amex/kennedys/peopleevents/p_joe.html)
  - [Kennedy's Legacy at the
    SEC](http://www.businessweek.com/bwdaily/dnflash/may2003/nf20030529_7026.htm)
  - [Biography of Joseph P. Kennedy and his early life and
    education](http://xroads.virginia.edu/~UG03/omara-alwala/JPKennedy.html)

[Category:1888年出生](../Category/1888年出生.md "wikilink")
[Category:1969年逝世](../Category/1969年逝世.md "wikilink")
[Category:美国驻英国大使](../Category/美国驻英国大使.md "wikilink")
[Category:美国证券交易委员会主席](../Category/美国证券交易委员会主席.md "wikilink")
[Category:肯尼迪家族](../Category/肯尼迪家族.md "wikilink")
[Category:美国房地产商](../Category/美国房地产商.md "wikilink")