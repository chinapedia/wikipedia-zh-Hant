[TIGER_CITY.jpg](https://zh.wikipedia.org/wiki/File:TIGER_CITY.jpg "fig:TIGER_CITY.jpg")
**老虎城購物中心**（**Tiger
City**）是一間位於[臺灣](../Page/臺灣.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[西屯區的都會型](../Page/西屯區.md "wikilink")[購物中心](../Page/購物中心.md "wikilink")，規模屬於中型購物中心（營業面積約8,200坪），由[日本建築師](../Page/日本.md "wikilink")[倉持光男負責整體空間](../Page/倉持光男.md "wikilink")、景觀的規劃與設計，2002年1月開業。鄰近[臺中國家歌劇院](../Page/臺中國家歌劇院.md "wikilink")、[秋紅谷公園與](../Page/秋紅谷公園.md "wikilink")[台中林酒店](../Page/台中林酒店.md "wikilink")。

## 歷史

[Baskin-Robbins_TigerCity_Taichung.jpg](https://zh.wikipedia.org/wiki/File:Baskin-Robbins_TigerCity_Taichung.jpg "fig:Baskin-Robbins_TigerCity_Taichung.jpg")\]\]

  - 2002年1月，斥資35億元興建的**老虎城購物中心**正式啟用。
  - 2006年，老虎城計畫在本館旁斥資26億元打造一座「摩天娛樂城」，其中5億元用來打造高105公尺的[摩天輪](../Page/摩天輪.md "wikilink")，其餘21億元則興建3000坪的購物商場，原預計2008年完工\[1\]，後來擴店計畫取消，老虎城二期用地改由其他建商興建商辦大樓\[2\]。
  - 2009年2月14日，老虎城前廣場啟用台灣首座[自行車機械](../Page/自行車.md "wikilink")[停車塔](../Page/停車塔.md "wikilink")\[3\]（現已拆除內部停車設備，改為街邊專門店）。
  - 2016年10月13日，台中國際動畫影展於台中老虎城(TIGER CITY)威秀影城登場\[4\]。

## 樓層介紹

  - 7F威秀影城GOLD CLASS
  - 6F-4F威秀影城
  - 3F戶外運動
  - 2F時尚設計
  - 1F國際名品
  - B1親子生活
  - B2主題娛樂/停車場
  - B3停車場

## 主力核心店

  - [威秀影城](../Page/威秀影城.md "wikilink")
  - [ZARA](../Page/颯拉.md "wikilink")
  - [玩具反斗城](../Page/玩具反斗城.md "wikilink")
  - [CLUB SEGA](../Page/SEGA.md "wikilink")

## 營業時間

  - 週日至週四 11:00\~22:00
  - 週五至週六及國定假日前一天11:00\~23:00

特殊營業時間除外

## 參考資料

## 外部連結

  - [老虎城購物中心](http://www.tigercity.com.tw/)

  -
  - [台灣公司資料](http://company.g0v.ronny.tw/id/70754305)

[T](../Category/台中市商場.md "wikilink") [市](../Category/西屯區.md "wikilink")
[Category:2002年成立的公司](../Category/2002年成立的公司.md "wikilink")
[Category:2001年完工建築物](../Category/2001年完工建築物.md "wikilink")

1.  [105公尺 全國最高摩天輪
    落腳老虎城](http://news.ltn.com.tw/news/local/paper/90541/print)，自由時報，2006-09-06
2.  [台中市區少見百億個案　國際級商辦「鼎盛BHW」開工動土](http://history.n.yam.com/taiwanhot/society/201202/20120205186250.html)，台灣好新聞，2012-02-05
3.  [首見單車停車塔
    收費10元起](http://www.appledaily.com.tw/appledaily/article/headline/20090215/31394500)，蘋果日報，2009年02月15日
4.  [台中國際動畫影展10/13登場　播映200場精選動畫](http://www.nownews.com/n/2016/09/22/2246739)今日新聞(NOWnews)，2016-09-22