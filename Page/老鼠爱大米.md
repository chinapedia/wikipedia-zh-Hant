《**老鼠爱大米**》，是一首於2004年12月至2005年在网络上迅速走红的[國語](../Page/國語.md "wikilink")[网络歌曲](../Page/网络歌曲.md "wikilink")。这首歌由[杨臣刚作词作曲](../Page/杨臣刚.md "wikilink")，[王啟文](../Page/王啟文.md "wikilink")[主唱](../Page/主唱.md "wikilink")。後被[香香](../Page/香香_\(歌手\).md "wikilink")、[郭美美](../Page/郭美美_\(歌手\).md "wikilink")、[胡杨林等人](../Page/胡杨林.md "wikilink")[翻唱](../Page/翻唱.md "wikilink")，杨臣刚本人亦有演唱过此歌。這首歌的[旋律簡單](../Page/旋律.md "wikilink")，琅琅上口，[歌詞也相當白話](../Page/歌詞.md "wikilink")，尤其是以「[老鼠喜愛](../Page/老鼠.md "wikilink")[大米](../Page/大米.md "wikilink")」來比喻對於[情人的喜愛](../Page/情人.md "wikilink")，生動活潑地表現出網路族群的[想像力](../Page/想像力.md "wikilink")，可能是其迅速走紅的原因。

## 其他版本

  - [粵語版](../Page/粵語.md "wikilink")「老鼠愛大米」，[Twins](../Page/Twins.md "wikilink")（[粵語](../Page/粵語.md "wikilink")、[國語](../Page/國語.md "wikilink")）、[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")（國語）主唱
  - [英語版](../Page/英語.md "wikilink")「'Cause I Love
    You」，North（越南裔、菲律賓裔澳大利亞人組合）主唱
  - [英語版](../Page/英語.md "wikilink")「Mouse love rice」，San Panith（泰國人）主唱
  - [日語版](../Page/日語.md "wikilink")「」，[美山加戀](../Page/美山加戀.md "wikilink")（2006年）、[下川美娜](../Page/下川美娜.md "wikilink")（2009年）主唱
  - [韓語版](../Page/韓語.md "wikilink")「」，主唱
  - [越南語版](../Page/越南語.md "wikilink")「」（），主唱
  - [高棉語版](../Page/高棉語.md "wikilink")「Mouse love rice」，Preap Sovath主唱

## 歌曲試聽

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
[Category:2004年歌曲](../Category/2004年歌曲.md "wikilink")
[Category:2005年歌曲](../Category/2005年歌曲.md "wikilink")
[Category:2006年歌曲](../Category/2006年歌曲.md "wikilink")
[Category:中文流行歌曲](../Category/中文流行歌曲.md "wikilink")
[Category:愛情題材歌曲](../Category/愛情題材歌曲.md "wikilink")