**考夫曼縣**（**Kaufman County,
Texas**）位[美國](../Page/美國.md "wikilink")[德克薩斯州東北部的一個縣](../Page/德克薩斯州.md "wikilink")。面積2,090平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口71,313人。縣治[考夫曼](../Page/考夫曼_\(德克薩斯州\).md "wikilink")（Kaufman）。

成立於1848年2月26日，縣政府成立於同年8月7日。縣名紀念第一位代表德州的[猶太人](../Page/猶太人.md "wikilink")[眾議員](../Page/美國眾議院.md "wikilink")[大衛·S·考夫曼](../Page/大衛·S·考夫曼.md "wikilink")（David
Spangler Kaufman）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[K](../Category/得克萨斯州行政区划.md "wikilink")
[Category:1848年建立的聚居地](../Category/1848年建立的聚居地.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.