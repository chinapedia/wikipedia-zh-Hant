**考拉克区**（[法语](../Page/法语.md "wikilink")：****）是[塞内加尔的一个](../Page/塞内加尔.md "wikilink")[行政区](../Page/塞内加尔行政区划.md "wikilink")，首府[考拉克是](../Page/考拉克.md "wikilink")[萨卢姆河的一座港口城市](../Page/萨卢姆河.md "wikilink")。该区接壤[冈比亚](../Page/冈比亚.md "wikilink")，是[达喀尔和](../Page/达喀尔.md "wikilink")[班珠尔之间旅程的一个常见停留点](../Page/班珠尔.md "wikilink")。

考拉克位于一个农业地区，是[花生交易和出口中心](../Page/花生.md "wikilink")，市内有一家大型花生油工厂。此外，酿酒、制革、棉花和鱼类加工也是当地重要的工业活动。

考拉克区下设、[考拉克](../Page/考拉克省.md "wikilink")（）、和三省。

[K](../Category/塞内加尔行政区.md "wikilink")