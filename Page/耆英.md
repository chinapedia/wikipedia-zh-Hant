**耆英**（），[爱新觉罗氏](../Page/爱新觉罗氏.md "wikilink")，[字](../Page/表字.md "wikilink")**介春**，[清朝](../Page/清朝.md "wikilink")[宗室](../Page/清宗室.md "wikilink")[正蓝旗人](../Page/正蓝旗.md "wikilink")，官至[两广总督](../Page/两广总督.md "wikilink")。[鸦片战争期间多次任钦差大臣代表清廷与](../Page/鸦片战争.md "wikilink")[英国和谈](../Page/英国.md "wikilink")，后因[谈判失利而被](../Page/谈判.md "wikilink")[咸豐帝赐死](../Page/咸豐帝.md "wikilink")。

## 經歷

以[荫生授](../Page/荫生.md "wikilink")[宗人府](../Page/宗人府.md "wikilink")[主事](../Page/主事.md "wikilink")。

1838年，任[盛京将军](../Page/盛京将军.md "wikilink")。

1842年3月[奕经在](../Page/奕经.md "wikilink")[浙江战败](../Page/浙江.md "wikilink")，清政府命耆英署理[杭州将军](../Page/杭州.md "wikilink")。4月，他被任命为钦差大臣，同[伊里布一起赴浙江向英军求和](../Page/伊里布.md "wikilink")。8月，英军闯入[南京](../Page/南京.md "wikilink")[下关](../Page/下关.md "wikilink")[长江江面](../Page/长江.md "wikilink")，耆英同伊里布赶奔南京，跟英国代表[砵甸乍谈判](../Page/砵甸乍.md "wikilink")，签订了中英《[南京条约](../Page/南京条约.md "wikilink")》。

1843年，耆英再任钦差大臣，与英国签订《[中英五口通商章程](../Page/中英五口通商章程.md "wikilink")》和《[虎门条约](../Page/虎门条约.md "wikilink")》。

[Formal_Reception_of_Keying.jpg](https://zh.wikipedia.org/wiki/File:Formal_Reception_of_Keying.jpg "fig:Formal_Reception_of_Keying.jpg")
1844年，耆英任[两广总督兼办通商事务](../Page/两广总督.md "wikilink")，与[美国签订了](../Page/美国.md "wikilink")《[望厦条约](../Page/望厦条约.md "wikilink")》，与[法国签订了](../Page/法国.md "wikilink")《[黄埔条约](../Page/黄埔条约.md "wikilink")》，与[瑞典签署了](../Page/瑞典.md "wikilink")《[中瑞广州条约](../Page/中瑞广州条约.md "wikilink")》。

1858年[第二次鸦片战争期间](../Page/第二次鸦片战争.md "wikilink")，耆英被派赴[天津协助](../Page/天津.md "wikilink")[桂良与](../Page/桂良.md "wikilink")[英法联军交涉](../Page/英法联军.md "wikilink")，由于英军在占领广州期间查获两广总督衙门大量档案文件，发现耆英在上报朝廷时对英国言辞不恭，因此拒绝与其交谈\[1\]。桂良请耆英回京，咸丰帝大怒，下狱议罪，拟绞监候，[肃顺上疏立即正法](../Page/肃顺.md "wikilink")，[咸丰帝赐其自尽](../Page/咸丰帝.md "wikilink")。

## 世系

  - 七世祖清顯祖[塔克世](../Page/塔克世.md "wikilink")
  - 六世祖诚毅勇壮贝勒[穆尔哈齐](../Page/穆尔哈齐.md "wikilink")
  - 五世祖
  - 高祖父
  - 曾祖父
  - 祖父[炳文](../Page/炳文.md "wikilink")
  - 父亲[禄康](../Page/禄康.md "wikilink")
  - 耆英
  - 子
      - 长子[庆锡](../Page/庆锡.md "wikilink")
      - 长子[庆贤](../Page/庆贤.md "wikilink")

## 以他命名的事物

  - [耆英號](../Page/耆英號.md "wikilink")，一艘[中國帆船](../Page/中國帆船.md "wikilink")，曾創下中國帆船航海最遠的紀錄

## 参考文献

  - 《[清史稿](../Page/清史稿.md "wikilink")》卷三百七十·列傳一百五十七·宗室耆英傳

## 外部連結

  - [香港艺术馆藏画](http://hkmasvr.lcsd.gov.hk/HKMACS_DATA/web/Object.nsf/0/8510cd400023a73648257068000c4902)：香港總督[德庇時與耆英會面圖](../Page/戴維斯_\(總督\).md "wikilink")

[Category:清朝宗人府理事官](../Category/清朝宗人府理事官.md "wikilink")
[Category:內閣侍讀學士](../Category/內閣侍讀學士.md "wikilink")
[Category:清朝太僕寺少卿](../Category/清朝太僕寺少卿.md "wikilink")
[Category:內閣學士](../Category/內閣學士.md "wikilink")
[Category:鑲白旗蒙古副都統](../Category/鑲白旗蒙古副都統.md "wikilink")
[Category:總管圓明園事務大臣](../Category/總管圓明園事務大臣.md "wikilink")
[Category:護軍營護軍統領](../Category/護軍營護軍統領.md "wikilink")
[Category:理藩院右侍郎](../Category/理藩院右侍郎.md "wikilink")
[Category:清朝兵部侍郎](../Category/清朝兵部侍郎.md "wikilink")
[Category:總管內務府大臣](../Category/總管內務府大臣.md "wikilink")
[Category:清朝工部侍郎](../Category/清朝工部侍郎.md "wikilink")
[Category:清朝戶部侍郎](../Category/清朝戶部侍郎.md "wikilink")
[Category:步軍營左翼總兵](../Category/步軍營左翼總兵.md "wikilink")
[Category:清朝禮部尚書](../Category/清朝禮部尚書.md "wikilink")
[Category:兼管奉宸院大臣](../Category/兼管奉宸院大臣.md "wikilink")
[Category:清朝內大臣](../Category/清朝內大臣.md "wikilink")
[Category:清朝工部尚書](../Category/清朝工部尚書.md "wikilink")
[Category:清朝太子三師](../Category/清朝太子三師.md "wikilink")
[Category:清朝戶部尚書](../Category/清朝戶部尚書.md "wikilink")
[Category:熱河都統](../Category/熱河都統.md "wikilink")
[Category:盛京將軍](../Category/盛京將軍.md "wikilink")
[Category:廣州將軍](../Category/廣州將軍.md "wikilink")
[Category:清朝兩江總督](../Category/清朝兩江總督.md "wikilink")
[Category:清朝兩廣總督](../Category/清朝兩廣總督.md "wikilink")
[Category:清朝協辦大學士](../Category/清朝協辦大學士.md "wikilink")
[Category:崇文門監督](../Category/崇文門監督.md "wikilink")
[Category:清朝文淵閣大學士](../Category/清朝文淵閣大學士.md "wikilink")
[Category:清朝工部員外郎](../Category/清朝工部員外郎.md "wikilink")
[Category:清朝自殺人物](../Category/清朝自殺人物.md "wikilink")
[Category:滿洲正藍旗人](../Category/滿洲正藍旗人.md "wikilink")
[Category:清朝宗室](../Category/清朝宗室.md "wikilink")
[Category:第一次鴉片戰爭人物](../Category/第一次鴉片戰爭人物.md "wikilink")
[Category:鑲藍旗滿洲副都統](../Category/鑲藍旗滿洲副都統.md "wikilink")
[Category:鑲黃旗漢軍副都統](../Category/鑲黃旗漢軍副都統.md "wikilink")
[Category:正紅旗滿洲副都統](../Category/正紅旗滿洲副都統.md "wikilink")
[Category:鑲黃旗滿洲副都統](../Category/鑲黃旗滿洲副都統.md "wikilink")
[Category:鑲黃旗漢軍都統](../Category/鑲黃旗漢軍都統.md "wikilink")
[Category:鑲白旗漢軍都統](../Category/鑲白旗漢軍都統.md "wikilink")
[Category:鑲紅旗滿洲都統](../Category/鑲紅旗滿洲都統.md "wikilink")
[Category:鑲白旗滿洲都統](../Category/鑲白旗滿洲都統.md "wikilink")
[Category:鑲藍旗滿洲都統](../Category/鑲藍旗滿洲都統.md "wikilink")
[Category:鑲黃旗蒙古都統](../Category/鑲黃旗蒙古都統.md "wikilink")

1.  [郭廷以](../Page/郭廷以.md "wikilink")《近代中国史纲》记载：“事后（桂良）与耆英相对而泣北京以耆英与英人有旧，原望他在此次交涉中能起作用，不料反遭[李泰国](../Page/李泰国.md "wikilink")、[威妥玛当面斥责](../Page/威妥玛.md "wikilink")，说是以往为他所愚。桂良、[花沙纳怕他有碍和局](../Page/花沙纳.md "wikilink")，请他回京。咸丰帝罪以从前办理夷务不善，以致酿成今日钜祸，迫令自尽。按1844年1月23日，耆英曾有驾驭夷人情形奏片，广州失陷后，原件为英人所得。威妥玛谓其中对英人多辱骂贱薄之语。[俄理范](../Page/俄理范.md "wikilink")（L.
    Oliphant）的《额尔金出使中国日本记》，附有译文，谓此奏片系1850年底所上，[马士](../Page/马士.md "wikilink")（H.
    B. Morse）的《中华帝国国际关系史》，加以肯定，谓事在耆英降调以前，均误。”