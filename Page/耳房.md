[Erfang_in_Sanshan_An,_Badachu,_Beijing.jpg](https://zh.wikipedia.org/wiki/File:Erfang_in_Sanshan_An,_Badachu,_Beijing.jpg "fig:Erfang_in_Sanshan_An,_Badachu,_Beijing.jpg")[八大处](../Page/八大处.md "wikilink")[三山庵正殿的耳房](../Page/三山庵.md "wikilink")\]\]
**耳房**是指[中国传统建筑设计中在主](../Page/中国传统建筑.md "wikilink")[房屋旁边加盖的小房屋](../Page/房屋.md "wikilink")，其高度和体积小于主房，犹如在主房两侧的[耳朵](../Page/耳朵.md "wikilink")，故名耳房。

在[四合院中](../Page/四合院.md "wikilink")，[正房两侧可建耳房](../Page/正房.md "wikilink")，\[1\]
[厢房也可设耳房](../Page/厢房.md "wikilink")，有的建成平顶，称为“[盝顶](../Page/盝顶.md "wikilink")”。\[2\]。耳房也可建在[城楼上](../Page/城楼.md "wikilink")，例如[天安门城楼两侧就建有耳房](../Page/天安门.md "wikilink")。\[3\]

## 注释

<div class="references-small">

<references />

</div>

[Category:中国古建筑名词](../Category/中国古建筑名词.md "wikilink")
[Category:房屋](../Category/房屋.md "wikilink")

1.  [耳房](http://www.zhongguowenhua.org/%E6%B0%91%E4%BF%97/%E5%9B%9B%E5%90%88%E9%99%A2/%E8%80%B3%E6%88%BF.html)
    ，中国文化网
2.  [厢房](http://www.zhongguowenhua.org/%E6%B0%91%E4%BF%97/%E5%9B%9B%E5%90%88%E9%99%A2/%E5%8E%A2%E6%88%BF.html)
    ，中国文化网。
3.  [耳房](http://www.tiananmen.org.cn/capinfo_product/NC_Foreground/NCFore_cms/list.asp?id=168&column_id=316&column_layer=040316&title_name=%E8%80%B3%E6%88%BF)
    ，载于“[天安门地区管理委员会](../Page/天安门.md "wikilink")”网页2002年7月19日。