**耳面-{A|zh-hans:症;zh-hant:症}-候群**（，亦作**無臉症**或**無下巴症候群**）是一種非常罕見的[先天缺陷](../Page/先天缺陷.md "wikilink")\[1\]。根據《[ICD-10](../Page/ICD-10.md "wikilink")》，這種疾病是一種[先天畸形](../Page/先天畸形.md "wikilink")、變形或[染色體異常疾病](../Page/染色體異常.md "wikilink")\[2\]，現時全世界就只有兩面患者\[3\]。患者天生沒有下巴，臉部的下半部嚴重塌陷。因此，患者不能自行進食，需要靠[胃喉來餵食](../Page/胃喉.md "wikilink")；不能自行呼吸，而利用[呼吸機](../Page/呼吸機.md "wikilink")()來協助呼吸；所以亦不能說話，要依靠電子儀器來發聲\[4\]。現時的解決辦法，是從患者的[髖骨取出一部份來重建下巴](../Page/髖骨.md "wikilink")。方法是：先取出部份髖骨，然後移植到背部的皮膚下，以便新骨長出神經和軟骨組織，然後才可以移殖到患者的頭部，用來重建下顎\[5\]。

## 著名患者

  - [阿倫·多爾蒂](../Page/阿倫·多爾蒂.md "wikilink") (Alan Doherty)
  - [朱丽安娜](../Page/朱丽安娜.md "wikilink")
  - [山川記代香](../Page/山川記代香.md "wikilink")

## 參看

  - [Treacher Collins症候群](../Page/Treacher_Collins症候群.md "wikilink")

## 參考

## 外部連結

  - <http://www.julianawetmore.net>
  - <http://www.foxnews.com/story/0,2933,279034,00.html>
  - [南方新闻网：国际新闻：美两岁半女孩天生无脸
    看上去像外星人](http://www.southcn.com/news/international/pic2/200511290661.htm)
  - [夠中國2006 -\> 新聞區 -\> 無顎少年
    重建下巴](https://web.archive.org/web/20070929002501/http://www.gochina7.net/2006/simple/index.php?t21313.html)

[Category:症候群](../Category/症候群.md "wikilink")

1.  [Teen Born Without Lower Face Finds Hope In
    NYC](http://wcbstv.com/local/local_story_159162133.html)

2.  《[ICD-10](../Page/ICD-10.md "wikilink")》

3.  《[香港蘋果日報](../Page/香港蘋果日報.md "wikilink")》引述《[美國霍士新聞頻道](../Page/美國霍士新聞頻道.md "wikilink")》，2007年6月13日號，A28版。

4.
5.