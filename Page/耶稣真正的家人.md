**耶稣真正的家人**是在《[新约圣经](../Page/新约圣经.md "wikilink")》中和记载的一个[耶稣的比喻](../Page/耶稣的比喻.md "wikilink")，與[耶穌的血親作對比](../Page/耶穌的血親.md "wikilink")。在早期的希臘文[圣经抄本中](../Page/圣经抄本.md "wikilink")，並沒有出現這個段落，許多學者認為這個段落是比較晚才被加進《馬太福音》與《馬可福音》的。

## 圣经译文

这两处记载在各种中英文[《圣经》译本中差异很小](../Page/圣经译本.md "wikilink")\[1\]。

在《[多馬福音](../Page/多馬福音.md "wikilink")》中，也有類似的記載。

## 解释

### 天主教观点

耶穌沒有廢除親屬的關係，只是指奉行天主旨意的人，獲得了更尊高的地位，成了耶穌的真親屬。\[2\]

### 东正教观点

### 新教观点

新教圣经学者普遍认为这里是要表明耶稣放弃了祂和犹太人肉身的关系。犹太人弃绝基督达到极峰，以致基督完全放弃他们，与基督隔绝了。此時，他們開始與基督決裂，而與基督隔絕了。（）

### 其他觀點

這個段落可能是被用來減低[耶穌的血親](../Page/耶穌的血親.md "wikilink")，如[公義者雅各等人在教會及信仰中的影響力](../Page/公義者雅各.md "wikilink")。

## 参考文献

## 参见

  - [耶稣的谱系](../Page/耶稣的谱系.md "wikilink")、[圣家族](../Page/圣家族.md "wikilink")、[耶穌的血親](../Page/耶穌的血親.md "wikilink")
  - [十诫](../Page/十诫.md "wikilink")、[盐和光](../Page/盐和光.md "wikilink")
  - [基督徒](../Page/基督徒.md "wikilink")、[假先知](../Page/假先知.md "wikilink")

{{-}}

{{@Bible|马太福音|12|46-50}} {{@Bible|马可福音|3|31-35}} {{@Bible|多马福音}}

[J](../Category/新约圣经中的人.md "wikilink")
[+](../Category/耶稣的家人.md "wikilink")
[J](../Category/对观福音.md "wikilink")

1.  、，中文[和合本](../Page/和合本.md "wikilink")、[新译本](../Page/新译本.md "wikilink")、[吕振中译本](../Page/吕振中译本.md "wikilink")、[恢复本](../Page/恢复本.md "wikilink")，英文[钦定本](../Page/钦定本.md "wikilink")、[美国标准本](../Page/美国标准本.md "wikilink")、[达秘译本](../Page/达秘译本.md "wikilink")、[杨氏直译本](../Page/杨氏直译本.md "wikilink")
2.  《[聖經思高本](../Page/聖經思高本.md "wikilink")》注釋