**耶罗尼米斯·博斯**（**Hieronymus
Bosch**，；[荷蘭語](../Page/荷蘭語.md "wikilink")：**Jheronimus
Bosch**），原名**耶罗恩·安东尼松·范·阿肯**（Jeroen Anthoniszoon van
Aken；荷蘭語：jeˈɾoːnimʏs vɑn ˈaːkə(n)），又名**耶罗恩·博斯**（Jeroen
Bosch）；[1452年](../Page/1452年.md "wikilink")－[1516年](../Page/1516年.md "wikilink")[8月](../Page/8月.md "wikilink")）是一位[十五至](../Page/十五世紀.md "wikilink")[十六世紀的多產](../Page/十六世紀.md "wikilink")[荷蘭畫家](../Page/荷蘭.md "wikilink")。他多數的畫作多在描繪[罪惡與人類道德的沉淪](../Page/原罪.md "wikilink")。博斯以[惡魔](../Page/惡魔.md "wikilink")、半人半獸甚至是[機械的形象來表現人的邪惡](../Page/機械.md "wikilink")。他的圖畫複雜，有高度的原創性、想像力，並大量使用各式的[象徵與](../Page/象徵.md "wikilink")[符號](../Page/符號.md "wikilink")，其中有些甚至在他的時代中也非常晦澀難解。博斯被認為是20世紀的[超現實主義的啟發者之一](../Page/超現實主義.md "wikilink")。

他的真名是**耶罗尼米斯（或“耶罗恩”）·范·阿肯**（Jheronimus（或Jeroen） van
Aken），意思是「[亞琛來的人](../Page/亞琛.md "wikilink")」。他在一部份畫作上署名Bosch（[荷蘭文](../Page/荷蘭文.md "wikilink")，音近[英文Boss](../Page/英文.md "wikilink")），取自他的出生地[斯海爾托亨博斯](../Page/斯海爾托亨博斯.md "wikilink")。在[西班牙文中他則多被稱為El](../Page/西班牙文.md "wikilink")
Bosco。

博斯出生於繪畫世家，他的雙親分別是荷蘭與[德國人](../Page/德國.md "wikilink")。他大部分的人生都在[斯海爾托亨博斯渡過](../Page/斯海爾托亨博斯.md "wikilink")，這是十五世紀當時[布拉班特](../Page/北布拉班特省.md "wikilink")（今荷蘭南部）一個熱鬧的城市。1463年時，約13歲的他可能曾目睹在當地發生的嚴重[火災](../Page/火災.md "wikilink")。不久之後他成為知名的畫家，甚至曾接到海外的委託。1488年他加入了[圣母兄弟會](../Page/圣母兄弟會.md "wikilink")，一個極端保守的信仰組織，由40位斯海爾托亨博斯當地有權勢的市民，以及[歐洲各地](../Page/歐洲.md "wikilink")7000多名的會員組成。

## 風格

[The_Garden_of_Earthly_Delights_by_Bosch_High_Resolution.jpg](https://zh.wikipedia.org/wiki/File:The_Garden_of_Earthly_Delights_by_Bosch_High_Resolution.jpg "fig:The_Garden_of_Earthly_Delights_by_Bosch_High_Resolution.jpg")[普拉多博物館的](../Page/普拉多博物館.md "wikilink")《[人間樂園](../Page/人間樂園.md "wikilink")》，是波希1503年至1504年間的作品。\]\]
[Hieronymus_Bosch_-_The_Garden_of_Earthly_Delights_-_Hell.jpg](https://zh.wikipedia.org/wiki/File:Hieronymus_Bosch_-_The_Garden_of_Earthly_Delights_-_Hell.jpg "fig:Hieronymus_Bosch_-_The_Garden_of_Earthly_Delights_-_Hell.jpg")
[Jheronimus_Bosch_050.jpg](https://zh.wikipedia.org/wiki/File:Jheronimus_Bosch_050.jpg "fig:Jheronimus_Bosch_050.jpg")'',
ca 1500–10, 华盛顿[国家艺术馆](../Page/国家艺术馆.md "wikilink")\]\]
博斯製作了多幅[三联畫](../Page/三联畫.md "wikilink")──繪在三片接合起來的木質[屏風上的畫作](../Page/屏風.md "wikilink")，其中最有名的是《[人间乐园](../Page/人间乐园.md "wikilink")》（亦作《塵世樂園》\[1\]）。這件三連畫的左幅，描繪了[樂園中的](../Page/失樂園.md "wikilink")[亞當與](../Page/亞當.md "wikilink")[夏娃與眾多奇妙的生物](../Page/夏娃.md "wikilink")；中幅以大量裸身的人體、巨大的[水果和](../Page/水果.md "wikilink")[鳥類描寫人間的樂園](../Page/鳥類.md "wikilink")；右幅則是[地獄的情境](../Page/地獄.md "wikilink")，充斥著大量造型奇幻的獄卒，以各式怪異的酷刑逞罰罪人。三件畫作合起時，觀賞者可見[上帝創造地球的](../Page/上帝.md "wikilink")[灰色裝飾畫](../Page/灰色裝飾畫.md "wikilink")。

這些畫作有一層較粗糙的顏料表層，與傳統[弗拉芒風格](../Page/弗拉芒.md "wikilink")，以平滑的表面修飾人為的不自然的手法大異其趣。

到了晚年時，博斯的風格已有所轉變，改以描繪大型、接近觀賞者的人物為表現方式。代表作是《戴刺冠的基督》（Christ Crowned with
Thorns）。

博斯從未在畫作上註明日期，也僅在部分作品上簽名（某些簽名則被認為並非本人）；總括來說，目前確認出自博斯之手的畫作，僅有25幅。[西班牙國王](../Page/西班牙國王.md "wikilink")[腓力二世在博斯死後收藏了他的大部分作品](../Page/腓力二世_\(西班牙\).md "wikilink")，因此目前西班牙[馬德里的](../Page/馬德里.md "wikilink")[普拉多美術館收藏了博斯最多的作品](../Page/普拉多美術館.md "wikilink")，包括《人間樂園》。

稍晚期的弗拉芒畫家[老彼得·布呂赫爾受博斯影響](../Page/老彼得·布呂赫爾.md "wikilink")，其作品風格與博斯相當近似，如1562年之《[死亡的勝利](../Page/死亡的勝利.md "wikilink")》。

## 作品列表

博斯的各式畫作可能有不同的譯名。在此採用英文名稱；連結將連入英文維基百科。

### 已完成的三聯畫（Triptych）

  - *[Haywain](../Page/:en:Haywain_\(painting\).md "wikilink")*
    (1500–1515)
  - 《[人間樂園](../Page/人間樂園.md "wikilink")》（作于1490-1510年间）
  - 《聖安東尼的誘惑》（*[The Temptation of St.
    Anthony](../Page/:en:The_Temptation_of_St._Anthony.md "wikilink")*）
  - 《最後的審判》（*[The Last
    Judgement](../Page/:en:The_Last_Judgement_\(Bosch\).md "wikilink")*，1482年）
  - 《東方三賢者的朝拜》（*[The Epiphany (The Adoration of the
    Magi)](../Page/:en:The_Epiphany_\(Bosch\).md "wikilink")*，1510年）
  - *[The Crucifixion of St
    Julia](../Page/:en:The_Crucifixion_of_St_Julia.md "wikilink")*

### 未完成的三聯畫

  - *[Christ Carrying the
    Cross](../Page/:en:Christ_Carrying_the_Cross_\(Madrid_version\).md "wikilink")*
    （Madrid 版本）
  - *[Christ Carrying the
    Cross](../Page/:en:Christ_Carrying_the_Cross_\(1515-16\).md "wikilink")*
    （1515-1516）
  - ''[The Last
    Judgement](../Page/:en:The_Last_Judgement_\(Bosch_triptych_fragment\).md "wikilink")
    （1506-08）
  - 記註為1494年後；曾為三連畫但現今其他部分已佚失：
      - *[The Ship of
        Fools](../Page/:en:Ship_of_Fools_\(painting\).md "wikilink")*
      - *[Allegory of Gluttony and
        Lust](../Page/:en:Allegory_of_Gluttony_and_Lust.md "wikilink")*
      - *[Death of the
        Miser](../Page/:en:Death_of_the_Miser.md "wikilink")*

### 油彩畫

  - *[Adoration of the
    Child](../Page/:en:Adoration_of_the_Child.md "wikilink")*
  - *[Christ Carrying the
    Cross](../Page/:en:Christ_Carrying_the_Cross_\(1480s\).md "wikilink")*
    (1480s)
  - *[Christ Crowned with
    Thorns](../Page/:en:Christ_Crowned_with_Thorns_\(1495-1500\).md "wikilink")*
    (1495-1500)
  - *[Christ Crowned with
    Thorns](../Page/:en:Christ_Crowned_with_Thorns_\(El_Escorial_version\).md "wikilink")*
    (El Escorial 版本)
  - ''[Crucifixion With a
    Donor](../Page/:en:Crucifixion_With_a_Donor.md "wikilink") (1483後)
  - *[Death of the
    Reprobate](../Page/:en:Death_of_the_Reprobate.md "wikilink")*
  - *[Ecce
    Homo](../Page/:en:Ecce_Homo_\(Hieronymus_Bosch\).md "wikilink")*
    (1476後)
  - *[Ecce Homo](../Page/:en:Ecce_Homo_\(1490s\).md "wikilink")* (1490s)
  - *[St. Jerome at
    Prayer](../Page/:en:St._Jerome_at_Prayer.md "wikilink")* (c. 1505)
  - *[St. Christopher Carrying the Christ
    Child](../Page/:en:St._Christopher_Carrying_the_Christ_Child.md "wikilink")*
    (1490–1500)
  - *[St. John the Baptist in the
    Wilderness](../Page/:en:St._John_the_Baptist_in_the_Wilderness.md "wikilink")*
    (1495後)
  - *[St. John the Evangelist on
    Patmos](../Page/:en:St._John_the_Evangelist_on_Patmos.md "wikilink")*
    (1490後)
  - *[The
    Conjurer](../Page/:en:The_Conjurer_\(painting\).md "wikilink")*
    (1500s)
  - *[The Extraction of the Stone of Madness (The Cure of
    Folly)](../Page/:en:The_Extraction_of_the_Stone_of_Madness_\(The_Cure_of_Folly\).md "wikilink")*
    (1500後)
  - *[The Marriage Feast at
    Cana](../Page/:en:The_Marriage_Feast_at_Cana.md "wikilink")* (1500)
  - *[The Seven Deadly Sins and the Four Last
    Things](../Page/:en:The_Seven_Deadly_Sins_and_the_Four_Last_Things.md "wikilink")*
    (1500–1510)
  - *[The Wayfarer](../Page/:en:The_Wayfarer.md "wikilink")*
  - 1490後收藏於[威尼斯總督府的四張系列木板畫](../Page/總督宮_\(威尼斯.md "wikilink")：
      - *[Ascent of the
        Blessed](../Page/:en:Ascent_of_the_Blessed.md "wikilink")*
      - *[Terrestrial
        Paradise](../Page/:en:Terrestrial_Paradise_\(Bosch\).md "wikilink")*
      - *[Fall of the
        Damned](../Page/:en:Fall_of_the_Damned.md "wikilink")*
      - *[Hell](../Page/:en:Hell_\(Bosch\).md "wikilink")*

### 素描

  - [Animal
    Studies](../Page/:en:Hieronymus_Bosch_drawings#Animal_Studies.md "wikilink")
  - [Beehive and
    Witches](../Page/:en:Hieronymus_Bosch_drawings#Beehive_and_Witches.md "wikilink")
  - [Beggars](../Page/:en:Hieronymus_Bosch_drawings#Beggars.md "wikilink")
  - [Beggars and
    Cripples](../Page/:en:Hieronymus_Bosch_drawings#Beggars_and_Cripples.md "wikilink")
  - [Christ Carrying The
    Cross](../Page/:en:Hieronymus_Bosch_drawings#Christ_Carrying_The_Cross.md "wikilink")
  - [Death of the
    Miser](../Page/:en:Hieronymus_Bosch_drawings#Death_of_the_Miser.md "wikilink")
  - [Group of Male
    Figures](../Page/:en:Hieronymus_Bosch_drawings#Group_of_Male_Figures.md "wikilink")
  - [Mary and John at the Foot of the
    Cross](../Page/:en:Hieronymus_Bosch_drawings#Mary_and_John_at_the_Foot_of_the_Cross.md "wikilink")
  - [Nest of
    Owls](../Page/:en:Hieronymus_Bosch_drawings#Nest_of_Owls.md "wikilink")
  - [Portrait of Hieronymus
    Bosch](../Page/:en:Hieronymus_Bosch_drawings#Portrait_of_Hieronymus_Bosch.md "wikilink")
  - [Scenes in
    Hell](../Page/:en:Hieronymus_Bosch_drawings#Scenes_in_Hell.md "wikilink")
  - [Studies](../Page/:en:Hieronymus_Bosch_drawings#Studies.md "wikilink")
  - [Studies of
    Monsters](../Page/:en:Hieronymus_Bosch_drawings#Studies_of_Monsters.md "wikilink")
  - [Temptation of St
    Anthony](../Page/:en:Hieronymus_Bosch_drawings#Temptation_of_St_Anthony.md "wikilink")
  - [The
    Entombment](../Page/:en:Hieronymus_Bosch_drawings#The_Entombment.md "wikilink")
  - [The Hearing Forest and the Seeing
    Field](../Page/:en:Hieronymus_Bosch_drawings#The_Hearing_Forest_and_the_Seeing_Field.md "wikilink")
  - [The Ship of
    Fools](../Page/:en:Hieronymus_Bosch_drawings#The_Ship_of_Fools.md "wikilink")
  - [The Ship of Fools in
    Flames](../Page/:en:Hieronymus_Bosch_drawings#The_Ship_of_Fools_in_Flames.md "wikilink")
  - [Tree-Man](../Page/:en:Hieronymus_Bosch_drawings#Tree-Man.md "wikilink")
  - [Two Caricatured
    Heads](../Page/:en:Hieronymus_Bosch_drawings#Two_Caricatured_Heads.md "wikilink")
  - [Two
    Monsters](../Page/:en:Hieronymus_Bosch_drawings#Two_Monsters.md "wikilink")
  - [Two
    Witches](../Page/:en:Hieronymus_Bosch_drawings#Two_Witches.md "wikilink")
  - [Witches](../Page/:en:Hieronymus_Bosch_drawings#Witches.md "wikilink")

## 瑣事

在2004年「[最偉大的荷蘭人](../Page/最偉大的荷蘭人.md "wikilink")（De Grootste
Nederlander）」中，博斯居於第63位。\[2\]

## 大眾文化

  - 在作家[邁克爾‧康奈利的小說中](../Page/邁克爾‧康奈利.md "wikilink")，主角之名*Hieronymus
    Harry Bosch*即是以博斯為靈感。此外，在康奈利2001年的作品「*A Darkness More Than
    Night*」情節中，博斯的畫作有重要的份量。
  - 童書繪本「*Pish, Posh, Said Hieronymus
    Bosch*」中，博斯畫作中的各種生物，在一位畫家與他的女友家裡變成活生生的生命。
  - 香港小說家[董啟章在他的](../Page/董啟章.md "wikilink")《自然史三部曲》系列小說的《時間繁史‧啞瓷之光》裡有講述關於博斯（在小說中譯作「波殊」）的畫作的情節。在題為〈E=mc²〉的小說章節裡，小說人物卉茵透過自身的家庭悲劇來詮釋博斯的其中一幅畫作《The
    Ship of Fools》的含意。\[3\]

## 參考資料

<references/>

## 外部連結

  - [Religious
    Paintings](https://web.archive.org/web/20061127234250/http://riri.essortment.com/heironymousbosc_pam.htm)
  - [High quality Bosch
    images](http://www.wga.hu/html/b/bosch/index.html)
  - [Hieronymus Bosch at
    Artcyclopedia](http://www.artcyclopedia.com/artists/bosch_hieronymus.html)
  - [Hieronymus Bosch at Olga's
    Gallery](http://www.abcgallery.com/B/bosch/bosch.html)
  - [Hieronymus Bosch "Between Heaven and Hell" in the "A World History
    of
    Art"](http://www.all-art.org/early_renaissance/bosch01biography.html)
  - [a painting selection of Hieronymus
    Bosch](https://web.archive.org/web/20070313173125/http://www.reproarte.com/painter/Hieronymus_Bosch/index.html)
  - [Hieronymus Bosch Gallery at
    ibiblio](http://www.ibiblio.org/wm/paint/auth/bosch/)
  - [Bosch
    Universe](https://web.archive.org/web/20070219102705/http://www.boschuniverse.org/index.cfm)
  - [Hieronymus Bosch Action
    Figures](http://www.3d-mouseion.com/engels/bosch_eng.htm)
  - [new interpretation of The Garden of
    Delights](http://elboscoblog.blogspot.com/)
  - [VA Tech English Dept. Project - Details Hieronymous Bosch paintings
    and allows for close
    examination](http://athena.english.vt.edu/~baugh/bosch/boschmain.htm)

[Category:1450年出生](../Category/1450年出生.md "wikilink")
[Category:1516年逝世](../Category/1516年逝世.md "wikilink")
[Category:弗拉芒画派](../Category/弗拉芒画派.md "wikilink")
[Category:荷蘭画家](../Category/荷蘭画家.md "wikilink")
[Category:文藝復興畫家](../Category/文藝復興畫家.md "wikilink")
[Category:斯海尔托亨博斯人](../Category/斯海尔托亨博斯人.md "wikilink")

1.  [大英簡明百科](http://sc.hrd.gov.tw/ebintra/Content.asp?Query=&ContentID=3496)
2.
3.  《時間繁史‧啞瓷之光》（上），2007年，麥田出版，144-148頁。