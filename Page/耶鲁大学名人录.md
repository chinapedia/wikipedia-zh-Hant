**耶鲁人（Yalies）**指与[耶鲁大学具有某种关系的个人](../Page/耶鲁大学.md "wikilink")，其中包括校友、教职员工和其他人员。以下是著名的耶鲁人的名单。

## 诺贝尔奖获得者

  - [乔治·阿克罗夫](../Page/乔治·阿克罗夫.md "wikilink")（George Akerlof），（2001年经济学獎）
  - [雷蒙德·戴维斯](../Page/雷蒙德·戴维斯.md "wikilink")（Raymond Davis）, (2002年物理学獎)
  - [约翰·埃弗·恩德斯](../Page/约翰·埃弗·恩德斯.md "wikilink")（John F. Enders）,
    (1954年生理学/医学獎)
  - [约翰·芬博士](../Page/约翰·芬.md "wikilink")（John Fenn \[Ph.D.\]),
    (2002年化学獎)
  - [默里·盖尔曼-默里](../Page/默里·盖尔曼-默里.md "wikilink")（Murray Gell-Mann）,
    (1969年物理学獎)
  - [阿尔佛雷德·G·吉尔曼](../Page/阿尔佛雷德·G·吉尔曼.md "wikilink")（Alfred G. Gilman）,
    (1994年生理学/医学獎)
  - [欧内斯特·劳伦斯博士](../Page/欧内斯特·劳伦斯.md "wikilink")（Ernest Lawrence
    \[(Ph.D.)\]）, (1939年物理学獎).
    （[劳伦斯利弗莫尔国家实验室和](../Page/劳伦斯利弗莫尔国家实验室.md "wikilink")[劳伦斯伯克利国家实验室便是以他的名字命名的](../Page/劳伦斯伯克利国家实验室.md "wikilink")）。
  - [乔舒亚·莱德伯格博士](../Page/乔舒亚·莱德伯格.md "wikilink")（Joshua Lederberg
    \[Ph.D.\]）, (1958年生理学/医学獎)
  - [大卫·李博士](../Page/大卫·李.md "wikilink")（David Lee \[Ph.D.\] 1959年),
    (1996年物理学獎)
  - [辛克莱·刘易斯](../Page/辛克莱·刘易斯.md "wikilink")（Sinclair Lewis）, (1930年文学獎)
  - [昂萨格·拉斯博士](../Page/昂萨格·拉斯.md "wikilink")（Lars Onsager \[Ph.D.\]）,
    (1968年化学獎)
  - [迪金森·理查兹](../Page/迪金森·理查兹.md "wikilink")（Dickinson Richards）,
    (1956年生理学/医学獎)
  - [威廉·威克瑞](../Page/威廉·威克瑞.md "wikilink")（William Vickrey）, (1996年经济学獎)
  - [乔治·惠普尔](../Page/乔治·惠普尔.md "wikilink")（George Whipple）,
    (1934年生理学/医学獎)
  - [埃里克·维斯查斯](../Page/埃里克·维斯查斯.md "wikilink")（Eric Wieschaus） (Ph.D.),
    (1995年生理学/医学獎)
  - [保罗·克鲁格曼](../Page/保罗·克鲁格曼.md "wikilink")（Paul Krugman），（2008年经济学獎）

## 发明家和艺术家

  - [沃尔特·坎普](../Page/沃尔特·坎普.md "wikilink")（Walter Camp）,"
    [美式橄榄球之父](../Page/美式橄榄球.md "wikilink")"
  - [弗朗西斯·S·柯林斯博士](../Page/弗朗西斯·S·柯林斯.md "wikilink")（Francis S. Collins
    \[Ph.D.\]), [人类基因计划研究主任](../Page/人类基因计划.md "wikilink")
  - [李·德福雷斯特](../Page/李·德福雷斯特.md "wikilink"),
    [三级真空管发明者](../Page/三级真空管.md "wikilink")
  - [W·爱德华兹·德明博士](../Page/W·爱德华兹·德明.md "wikilink")（W. Edwards Deming
    \[Ph.D.\]), "全名质量管理" ([TQM](../Page/全名质量管理.md "wikilink")) 大师
  - [欧文·费希尔博士](../Page/欧文·费希尔.md "wikilink") （Irving Fisher \[Ph.D.\]）,
    经济学家t, "[货币主义之父](../Page/货币主义.md "wikilink")"
  - [约西亚·威拉德·吉布斯](../Page/约西亚·威拉德·吉布斯.md "wikilink")（Josiah Willard
    Gibbs）, 数学家, 物理化学家, 热力学家,
    因发现“[吉布斯现象](../Page/吉布斯现象.md "wikilink")”出名
  - [格雷斯·霍珀博士](../Page/格雷斯·霍珀.md "wikilink")（Grace Hopper \[Ph. D.\]）,
    计算机编程语言[COBOL的发明人](../Page/COBOL.md "wikilink")
  - [Art Laffer](../Page/Art_Laffer.md "wikilink"), 经济学家, best known for
    the "[拉弗曲线](../Page/拉弗曲线.md "wikilink")"
  - [保罗·D·迈克尔德莱蒂](../Page/保罗·D·迈克尔德莱蒂.md "wikilink")（Paul D. MacCready）,
    "Engineer of the Century," won the Kremer Prize for first
    human-powered flying machine, pioneer in solar-powered flight
  - [Saunders MacLane](../Page/Saunders_MacLane.md "wikilink"),
    mathematician, one of the founders of "[category
    theory](../Page/category_theory.md "wikilink")"
  - [Jordan Mechner](../Page/Jordan_Mechner.md "wikilink"),
    [videogame](../Page/videogame.md "wikilink") developer, created
    *[Prince of Persia](../Page/Prince_of_Persia.md "wikilink")*
  - [Stanley Milgram](../Page/Stanley_Milgram.md "wikilink"),
    psychologist, [Milgram
    experiment](../Page/Milgram_experiment.md "wikilink"), coined the
    concept "[six degrees of
    separation](../Page/six_degrees_of_separation.md "wikilink")"
  - [薩繆爾·摩爾斯](../Page/薩繆爾·摩爾斯.md "wikilink"), 电报先驱,
    [摩尔斯电码发明人](../Page/摩尔斯电码.md "wikilink")
  - [John Ousterhout](../Page/John_Ousterhout.md "wikilink"),
    计算机编程语言[Tcl的发明人](../Page/Tcl.md "wikilink")
  - [Ronald Rivest](../Page/Ronald_Rivest.md "wikilink"), 计算机科学家,
    [RSA加密中的R即代表其人](../Page/RSA.md "wikilink"), 2002年
    [图灵奖获得者](../Page/图灵奖.md "wikilink")
  - [Eli Whitney](../Page/Eli_Whitney.md "wikilink"),
    [轧花机发明人](../Page/轧花机.md "wikilink")
  - [詹天佑](../Page/詹天佑.md "wikilink"),"鐵路工程師" ，在1881年畢業於[Sheffield
    Scientific
    School](../Page/Sheffield_Scientific_School.md "wikilink")，亦為京张铁路总工程师

## 企业家 & CEO

  - [鄧文迪](../Page/鄧文迪.md "wikilink"),
    ([企業管理碩士](../Page/企業管理碩士.md "wikilink"))
    美國知名企業家，前任[新聞集團副總](../Page/新聞集團.md "wikilink")、現任耶魯大學管理學院顧問委員。
  - [Robert M. Bass](../Page/Robert_M._Bass.md "wikilink"),
    ([文學學士](../Page/文學學士.md "wikilink")－1971年) [Keystone
    Inc.的董事長](../Page/Keystone_Inc..md "wikilink")。
  - [John Thomas
    Daniels](../Page/John_Thomas_Daniels.md "wikilink")，[Archer
    Daniels
    Midland創辦人](../Page/Archer_Daniels_Midland.md "wikilink")。（本公司是農業產品加工和發酵科技的全球領先者）
  - [西奥·艾普斯坦](../Page/西奥·艾普斯坦.md "wikilink")，[波士顿红袜执行副总裁](../Page/波士顿红袜.md "wikilink")、总经理。
  - [Robert Glaser](../Page/Robert_Glaser.md "wikilink"), (B.A. & M.A.)
    [RealNetworks的創辦人與](../Page/RealNetworks.md "wikilink")[CEO](../Page/CEO.md "wikilink")。
  - [Bing
    Gordon](../Page/Bing_Gordon.md "wikilink")，[美商藝電](../Page/美商藝電.md "wikilink")（EA）的創辦人之一，擔任執行副總裁、首席創意總監。
  - [Roberto
    Goizueta](../Page/Roberto_Goizueta.md "wikilink")，[可口可樂的前](../Page/可口可樂.md "wikilink")[CEO](../Page/CEO.md "wikilink")([艾文理大學的商學院是以其為名](../Page/艾文理大學.md "wikilink")。)
  - [Charles B.
    Johnson](../Page/Charles_B._Johnson.md "wikilink")，[富蘭克林坦伯頓投資公司的主席](../Page/富蘭克林坦伯頓.md "wikilink")。
  - [Mitch Kapor](../Page/Mitch_Kapor.md "wikilink")，[Kapor
    Enterprises的出資者](../Page/Kapor_Enterprises.md "wikilink")，[Lotus
    Software的前創辦人與](../Page/Lotus_Software.md "wikilink")[CEO](../Page/CEO.md "wikilink")。
  - [Herbert Kohler](../Page/Herbert_Kohler.md "wikilink")，[Kohler
    Company的董事長與主席](../Page/Kohler_Company.md "wikilink")。
  - [Clarence
    King](../Page/Clarence_King.md "wikilink")，[美國地質調查局的創建者之一](../Page/美國地質調查局.md "wikilink")。
  - [Edward Lampert](../Page/Edward_Lampert.md "wikilink")[ESL
    Investments的創辦人與主席](../Page/ESL_Investments.md "wikilink")。
  - [John Franklyn
    Mars](../Page/John_Franklyn_Mars.md "wikilink")，[Mars,
    Incorporated的CEO](../Page/Mars,_Incorporated.md "wikilink")
  - [羅伯·摩斯](../Page/羅伯·摩斯.md "wikilink")，20世紀中期[紐約市的建築業大王](../Page/紐約市.md "wikilink")。
  - [Gifford Pinchot](../Page/Gifford_Pinchot.md "wikilink")，[United
    States Forest
    Service的創辦人](../Page/United_States_Forest_Service.md "wikilink")。
  - [Robert Sargent Shriver
    III](../Page/Robert_Sargent_Shriver_III.md "wikilink")，[巴爾的摩金鶯隊的擁有者之一](../Page/巴爾的摩金鶯.md "wikilink")。
  - [Timothy Perry
    Shriver](../Page/Timothy_Perry_Shriver.md "wikilink")，[特殊奧林匹克運動會的CEO](../Page/特殊奧林匹克運動會.md "wikilink")。
  - [Frederick W.
    Smith](../Page/Frederick_W._Smith.md "wikilink")，[聯邦快遞的創辦人與](../Page/聯邦快遞.md "wikilink")[CEO](../Page/CEO.md "wikilink")
  - [Harold
    Stanley](../Page/Harold_Stanley.md "wikilink")，[摩根士丹利的創辦人](../Page/摩根士丹利.md "wikilink")
  - [Richard Thalheimer](../Page/Richard_Thalheimer.md "wikilink")，[The
    Sharper
    Image的創辦人與](../Page/The_Sharper_Image.md "wikilink")[CEO](../Page/CEO.md "wikilink")。
  - [胡安·特里普](../Page/胡安·特里普.md "wikilink")，[泛美航空的創辦人與](../Page/泛美航空.md "wikilink")[CEO](../Page/CEO.md "wikilink")
  - [Frederick E.
    Weyerhaeuser](../Page/Frederick_E._Weyerhaeuser.md "wikilink")，[Weyerhaeuser](../Page/Weyerhaeuser.md "wikilink")（紙張生產公司）創辦人。

## 学术界名人

  - [Richard H.
    Brodhead](../Page/Richard_H._Brodhead.md "wikilink")，[杜克大學校長](../Page/杜克大學.md "wikilink")。
  - [Alan
    Dershowitz](../Page/Alan_Dershowitz.md "wikilink")，[哈佛大學法律教授](../Page/哈佛大學.md "wikilink")。
  - [Jonathan
    Dickinson](../Page/Jonathan_Dickinson.md "wikilink")，[普林斯頓大學創辦人](../Page/普林斯頓大學.md "wikilink")。
  - [Daniel Coit
    Gilman](../Page/Daniel_Coit_Gilman.md "wikilink")，[約翰·霍普金斯大學第一任校長](../Page/約翰·霍普金斯大學.md "wikilink")。
  - [William Rainey
    Harper](../Page/William_Rainey_Harper.md "wikilink")，[芝加哥大學第一任校長](../Page/芝加哥大學.md "wikilink")。
  - [Lawrence
    Lessig](../Page/Lawrence_Lessig.md "wikilink")，[史丹佛大學法律教授](../Page/史丹佛大學.md "wikilink")。
  - [Reinhold Niebuhr](../Page/Reinhold_Niebuhr.md "wikilink")
    (神學院)，作家、[神學家](../Page/神學家.md "wikilink")。
  - [Camille Paglia](../Page/Camille_Paglia.md "wikilink")
    (Ph.D.)，*[Sexual
    Personae](../Page/Sexual_Personae.md "wikilink")*的作者
  - [Benjamin Spock](../Page/Benjamin_Spock.md "wikilink") (醫學院)，嬰兒權威。
  - [Andrew Dickson
    White](../Page/Andrew_Dickson_White.md "wikilink")，[康乃爾大學第一任校長](../Page/康乃爾大學.md "wikilink")。
  - [容閎](../Page/容閎.md "wikilink")，第一位獲得美國大學學位的中國學生。

## [美国总统](../Page/美国总统.md "wikilink")&[副总统](../Page/美国副总统.md "wikilink")

  - 第27任美国总统
    (1909年-1913年)-[威廉·霍华德·塔夫脱](../Page/威廉·霍华德·塔夫脱.md "wikilink"),美国[最高法院首席大法官](../Page/美国首席大法官.md "wikilink")(1921年-1930年)
  - 第38任美国总统 (1974年-1977年)-[杰拉尔德·福特](../Page/杰拉尔德·福特.md "wikilink")
    ([法学院](../Page/耶鲁法学院.md "wikilink"))
  - 第41任美国总统 (1989年-1993年)-[乔治·H·W·布什](../Page/乔治·H·W·布什.md "wikilink")
  - 第42任美国总统 (1993年-2001年)-[比尔·克林顿](../Page/比尔·克林顿.md "wikilink") (法学院)
  - 第43任美国总统 (2001年-2009年)-[乔治·W·布什](../Page/乔治·W·布什.md "wikilink")
  - 美国副总统 (2001年-2009年)-[理察·切尼](../Page/理察·切尼.md "wikilink")

## 法学界 & 政坛名人

  - [江宜樺](../Page/江宜樺.md "wikilink")，前[行政院院長](../Page/中華民國行政院.md "wikilink")
  - [迪安·艾奇遜](../Page/迪安·艾奇遜.md "wikilink")，前[美國國務卿](../Page/美國國務卿.md "wikilink")
  - [Cecilia Altonaga](../Page/Cecilia_Altonaga.md "wikilink"), 联邦法官
  - [阿什克羅夫特](../Page/約翰·阿什克羅夫特.md "wikilink")，前[美国司法部长](../Page/美国司法部长.md "wikilink")、[美国参议员](../Page/美國參議院.md "wikilink")（1993年—2001年）、曾擔任[密苏里州州长](../Page/密苏里州.md "wikilink")（1985年—1993年）。
  - [David Boies](../Page/David_Boies.md "wikilink"),
    著名律师（曾經歷[微軟反托辣斯案](../Page/微軟.md "wikilink")、[布希與](../Page/布希.md "wikilink")[高爾選舉訴訟](../Page/高爾.md "wikilink")、[Napster與](../Page/Napster.md "wikilink")[美國唱片業協會之爭](../Page/美國唱片業協會.md "wikilink")。)
  - [小威廉·F·巴克利](../Page/小威廉·F·巴克利.md "wikilink")，政治權威人士。
  - [McGeorge Bundy](../Page/McGeorge_Bundy.md "wikilink")，前內閣閣員。
  - [Edmund Gerald "Jerry" Brown,
    Jr.](../Page/Jerry_Brown.md "wikilink")
    (法學院)，[奧克蘭市長](../Page/奧克蘭_\(加利福尼亞州\).md "wikilink")(1999年-迄今)、[加州州長](../Page/加州.md "wikilink")
    (1975年-1983年)。
  - [卡爾·卡斯滕斯](../Page/卡爾·卡斯滕斯.md "wikilink")，[德國總統](../Page/德國總統.md "wikilink")（1979年—1984年）
  - [希拉蕊·柯林頓](../Page/希拉蕊·柯林頓.md "wikilink")
    (法學院)，[2016年美國總統選舉](../Page/2016年美國總統選舉.md "wikilink")[民主黨](../Page/民主黨_\(美國\).md "wikilink")[總統候選人](../Page/美國總統.md "wikilink")、前[第一夫人](../Page/第一夫人.md "wikilink")、前[美國國務卿](../Page/美國國務卿.md "wikilink")
  - [Mark
    Dayton](../Page/Mark_Dayton.md "wikilink")，[美國參議員](../Page/美國參議員.md "wikilink")（[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")，2000年至今)
  - [Howard
    Dean](../Page/Howard_Dean.md "wikilink")，[佛蒙特州州長](../Page/佛蒙特州.md "wikilink")
    (1991年-2003年)，[美國](../Page/美國.md "wikilink")2004年[民主黨](../Page/民主黨.md "wikilink")[候選人](../Page/候選人.md "wikilink")。
  - [William H.
    Donaldson](../Page/William_H._Donaldson.md "wikilink")，[美國證券交易委員會主席](../Page/美國證券交易委員會.md "wikilink")
    (2003年至今)，創辦人為Donaldson、Lufkin 與 Jenrette
  - [David Gergen](../Page/David_Gergen.md "wikilink")，政治權威人士
  - [Nathan Hale](../Page/Nathan_Hale.md "wikilink")，愛國列士，曾說出名言："
    我只後悔自己只有一條命為國家犧牲。"（原文："I only regret that I have but
    one life to lose for my country."
  - [吉姆·杰福兹](../Page/吉姆·杰福兹.md "wikilink")，[美國參議員](../Page/美國參議員.md "wikilink")（[佛蒙特州](../Page/佛蒙特州.md "wikilink")，1989年－2007年)
  - [約翰·克里](../Page/約翰·克里.md "wikilink")，前[美國國務卿](../Page/美國國務卿.md "wikilink")、[2004年美國總統選舉民主黨候選人](../Page/2004年美國總統選舉.md "wikilink")，[美國參議員](../Page/美國參議員.md "wikilink")（[麻薩諸塞州](../Page/麻薩諸塞州.md "wikilink")，1985年-迄今)
  - [Tony
    Knowles](../Page/Tony_Knowles_\(politician\).md "wikilink")，[阿拉斯加州州長](../Page/阿拉斯加州.md "wikilink")
    (1994-2002年)
  - [Joseph
    Lieberman](../Page/Joseph_Lieberman.md "wikilink")，[美國參議員](../Page/美國參議員.md "wikilink")（[康乃狄克州](../Page/康乃狄克州.md "wikilink")，1989年至今)
  - [駱家輝](../Page/駱家輝.md "wikilink")，前[美國駐華大使](../Page/美國駐華大使.md "wikilink")、前[美國商務部長](../Page/美國商務部長.md "wikilink")、前[華盛頓州州長](../Page/華盛頓州.md "wikilink")
  - [John
    Negroponte](../Page/John_Negroponte.md "wikilink")，[美國駐](../Page/美國.md "wikilink")[伊拉克大使](../Page/伊拉克.md "wikilink")
    (2004年至今)
  - [George
    Pataki](../Page/George_Pataki.md "wikilink")，[紐約州州長](../Page/紐約州.md "wikilink")
    (1995年至今)
  - [Clark T. Randt,
    Jr.](../Page/Clark_T._Randt,_Jr..md "wikilink")，前[美國駐](../Page/美國.md "wikilink")[中國大使](../Page/中國.md "wikilink")
  - [Sargent
    Shriver](../Page/Sargent_Shriver.md "wikilink")，[和平工作隊的主要組織人與第一任主任](../Page/和平工作隊.md "wikilink")。[Eunice
    Kennedy的丈夫](../Page/Eunice_Kennedy.md "wikilink")，[Maria
    Shriver的父親](../Page/Maria_Shriver.md "wikilink")，([Maria
    Shriver是位記者](../Page/Maria_Shriver.md "wikilink")，也是[加州現任州長](../Page/加州.md "wikilink")[阿諾·史瓦辛格的妻子](../Page/阿諾·史瓦辛格.md "wikilink"))。
  - [Potter
    Stewart](../Page/Potter_Stewart.md "wikilink")，最高法院法官(1958年-1991年)
  - [Robert
    Taft](../Page/Bob_Taft.md "wikilink")，[俄亥俄州州長](../Page/俄亥俄州.md "wikilink")(1999年至今)
  - [Clarence Thomas](../Page/Clarence_Thomas.md "wikilink") (J.D.
    1974，)，最高法院法官 (1991年至今)
  - [Byron White](../Page/Byron_White.md "wikilink")，最高法院法官(1962年-1993年)
  - [Pete
    Wilson](../Page/Pete_Wilson.md "wikilink")，[加州州長](../Page/加州.md "wikilink")
    (1991年-1999年)
  - [Ernesto
    Zedillo](../Page/Ernesto_Zedillo.md "wikilink")，[墨西哥總統](../Page/墨西哥.md "wikilink")
    (1994年-2000年)
  - [鐘文耀](../Page/鐘文耀.md "wikilink")(並未畢業)，[清朝一位傑出的外交官](../Page/清朝.md "wikilink")，在[耶魯大學就學時期也是傑出的舵手](../Page/耶魯大學.md "wikilink")。
  - [郭正亮](../Page/郭正亮.md "wikilink")，[政治學](../Page/政治學.md "wikilink")[博士](../Page/博士.md "wikilink")，[中華民國現任](../Page/中華民國.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。
  - [林佳龍](../Page/林佳龍.md "wikilink")，[政治學](../Page/政治學.md "wikilink")[博士](../Page/博士.md "wikilink")，[中華民國現任](../Page/中華民國.md "wikilink")[中華民國交通部部長](../Page/中華民國交通部部長.md "wikilink")、前任[立法委員](../Page/立法委員.md "wikilink")、前任[總統府副秘書長](../Page/中華民國總統府.md "wikilink")。

## 历史人文、音乐艺术界名人

  - [Hiram
    Bingham](../Page/Hiram_Bingham.md "wikilink")，發現了位於[秘魯的](../Page/秘魯.md "wikilink")[马丘比丘](../Page/马丘比丘.md "wikilink")。
  - [哈羅德·布魯姆](../Page/哈羅德·布魯姆.md "wikilink")，美國文學評論家。
  - [James Fenimore
    Cooper](../Page/James_Fenimore_Cooper.md "wikilink")，[最後的莫希干人](../Page/最後的莫希干人.md "wikilink")（The
    Last of the Mohicans）作者。
  - [查理斯·艾伍士](../Page/查理斯·艾伍士.md "wikilink")，[古典音樂作曲家](../Page/古典音樂.md "wikilink")。
  - [John Knowles](../Page/John_Knowles.md "wikilink")，[A Separate
    Peace的作者](../Page/A_Separate_Peace.md "wikilink")。
  - [林璎](../Page/林璎.md "wikilink")，建筑师，[越战纪念墙的设计者](../Page/越战纪念墙.md "wikilink")
  - [亨利·魯斯](../Page/亨利·魯斯.md "wikilink")，[時代
    (雜誌)的創辦人之一](../Page/時代_\(雜誌\).md "wikilink")。
  - [David
    McCullough](../Page/David_McCullough.md "wikilink")，知名的歷史學家，曾兩度獲得[普利茲獎](../Page/普利茲獎.md "wikilink")，最知名的著作是關於美國總統生平的[哈瑞·S·杜魯門與](../Page/哈瑞·S·杜魯門.md "wikilink")[约翰·亚当斯](../Page/约翰·亚当斯.md "wikilink")。
  - [Camille Paglia](../Page/Camille_Paglia.md "wikilink")，文化評論家與女性主義學者。
  - [Cole Porter](../Page/Cole_Porter.md "wikilink")，作曲家
  - [埃羅·沙里寧](../Page/埃羅·沙里寧.md "wikilink")，建築師
  - [Garry
    Trudeau](../Page/Garry_Trudeau.md "wikilink")，創作[Doonesbury的漫畫家](../Page/Doonesbury.md "wikilink")
  - [諾亞·韋伯斯特](../Page/諾亞·韋伯斯特.md "wikilink")，[韋氏英文字典的作者](../Page/韋氏英文字典.md "wikilink")。
  - [Thornton
    Wilder](../Page/Thornton_Wilder.md "wikilink")，劇作家，以劇本*[Our
    Town](../Page/Our_Town.md "wikilink")*獲得[普立茲獎](../Page/普立茲獎.md "wikilink")。
  - [Naomi
    Wolf](../Page/Naomi_Wolf.md "wikilink")，[女性主義作家](../Page/女性主義.md "wikilink")
  - [Tom Wolfe](../Page/Tom_Wolfe.md "wikilink") (PhD)，新聞記者。書籍[The Right
    Stuff與](../Page/The_Right_Stuff.md "wikilink")[The Bonfire of the
    Vanities的作者](../Page/The_Bonfire_of_the_Vanities.md "wikilink")。
  - [鮑勃·伍德沃德](../Page/鮑勃·伍德沃德.md "wikilink")，新聞記者。也是獲得[普立茲獎的書籍](../Page/普立茲獎.md "wikilink")[All
    the President's
    Men的作者之一](../Page/All_the_President's_Men.md "wikilink")。
  - [李名覺](../Page/李名覺.md "wikilink")，著名[舞台設計師](../Page/舞台設計.md "wikilink")
  - [黄自](../Page/黄自.md "wikilink")，中国作曲家

## 影坛名人

  - [安琪拉·巴塞特](../Page/安琪拉·巴塞特.md "wikilink")（Angela Bassett），演员
  - [Jennifer
    Beals](../Page/Jennifer_Beals.md "wikilink")，女演員，最知名的作品為*[閃舞](../Page/閃舞.md "wikilink")（Flashdance）*
  - [Henry Bean](../Page/Henry_Bean.md "wikilink")，電影劇作家，曾導演店映*[The
    Believer](../Page/The_Believer.md "wikilink")*
  - [Jordana
    Brewster](../Page/Jordana_Brewster.md "wikilink")，演员。在電影*[The
    Fast and the
    Furious](../Page/The_Fast_and_the_Furious.md "wikilink")（台灣譯：玩命關頭）*中演出Mia。
  - [Bruce Cohen](../Page/Bruce_Cohen.md "wikilink")，電影製片，曾因1999年的電影
    *[American
    Beauty](../Page/American_Beauty.md "wikilink")（台灣譯：美國心玫瑰情）*獲得[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")。
  - [Michael
    Cimino](../Page/Michael_Cimino.md "wikilink")，[奧斯卡金像獎得獎導演](../Page/奧斯卡金像獎.md "wikilink")
  - [珍妮佛·康納莉](../Page/珍妮佛·康納莉.md "wikilink")，[奧斯卡金像獎獲獎女演員](../Page/奧斯卡金像獎.md "wikilink")
  - [克萊兒·丹妮絲](../Page/克萊兒·丹妮絲.md "wikilink")（Claire
    Danes），女演員。曾在電影*[終結者第三集](../Page/終結者.md "wikilink")*中演出，也在導演[Baz
    Luhrmann的電影](../Page/Baz_Luhrmann.md "wikilink")*[羅密歐與茱麗葉](../Page/羅密歐與茱麗葉.md "wikilink")*中與[李奧納多·狄卡皮歐演出對手戲](../Page/李奧納多·狄卡皮歐.md "wikilink")。
  - [茱蒂·福斯特](../Page/茱蒂·福斯特.md "wikilink")，[奧斯卡金像獎獲獎女演員與導演](../Page/奧斯卡金像獎.md "wikilink")。在本校拿到[文學學士的學位](../Page/文學學士.md "wikilink")，拿到*[拉丁文學位榮譽](../Page/拉丁文學位榮譽.md "wikilink")*的極優異評等成績。
  - [Paul Giamatti](../Page/Paul_Giamatti.md "wikilink") (MFA,
    1989年)，男演員。曾在2003年的電影*[American
    Splendor](../Page/American_Splendor.md "wikilink")*中演出"Harvey Pekar"
    一角。
  - [David Alan Grier](../Page/David_Alan_Grier.md "wikilink"), 喜劇演員
  - [Kathryn Hahn](../Page/Kathryn_Hahn.md "wikilink")，演员
  - [George Roy
    Hill](../Page/George_Roy_Hill.md "wikilink")，[奧斯卡金像獎得獎導演](../Page/奧斯卡金像獎.md "wikilink")。
  - [荷莉·杭特](../Page/荷莉·杭特.md "wikilink")（Holly
    Hunter），[奧斯卡金像獎獲獎女演員](../Page/奧斯卡金像獎.md "wikilink")。
  - [勞埃德·考夫曼](../Page/勞埃德·考夫曼.md "wikilink")(Lloyd Kaufman)，著名獨立製片人
  - [Elia Kazan](../Page/Elia_Kazan.md "wikilink"),
    [奥斯卡金像奖獲獎](../Page/奥斯卡金像奖.md "wikilink")
    director
  - [Phil LaMarr](../Page/Phil_LaMarr.md "wikilink")
    (1989年)，喜劇演员。在電影[黑色追緝令中演出被主角](../Page/黑色追緝令.md "wikilink")[約翰·屈伏塔射中頭部的角色](../Page/約翰·屈伏塔.md "wikilink")
    "Marvin"。
  - [Ron
    Livingston](../Page/Ron_Livingston.md "wikilink")，男演員，最廣為人之的作品是在電視影集*[慾望城市](../Page/慾望城市.md "wikilink")*中演出"Jack
    Berger"一角。
  - [Frances McDormand](../Page/Frances_McDormand.md "wikilink")
    (MFA)，女演員
  - [保羅·紐曼](../Page/保羅·紐曼.md "wikilink")（Paul
    Newman），[奧斯卡金像獎獲獎演員](../Page/奧斯卡金像獎.md "wikilink")，經典性格巨星。
  - [愛德華·諾頓](../Page/愛德華·諾頓.md "wikilink") (1991年)，演员。
  - [Bronson Pinchot](../Page/Bronson_Pinchot.md "wikilink") (1981年)，演员
  - [Vincent Price](../Page/Vincent_Price.md "wikilink")，演员
  - [Gene Siskel](../Page/Gene_Siskel.md "wikilink") (1967年)，影評人
  - [Todd Solondz](../Page/Todd_Solondz.md "wikilink")，導演。作品有*[Welcome
    to the
    Dollhouse](../Page/Welcome_to_the_Dollhouse.md "wikilink")*與*[Happiness](../Page/Happiness.md "wikilink")*。
  - [奧利佛·史東](../Page/奧利佛·史東.md "wikilink")（Oliver
    Stone），[奧斯卡金像獎獲獎導演](../Page/奧斯卡金像獎.md "wikilink")
  - [梅丽·史翠普](../Page/梅丽·史翠普.md "wikilink") (MFA),
    [奧斯卡金像獎獲獎女藝人](../Page/奧斯卡金像獎.md "wikilink")。[穿著Prada的惡魔中演出上司角色](../Page/穿著Prada的惡魔.md "wikilink")。
  - [John Turturro](../Page/John_Turturro.md "wikilink") (MFA)，演員
  - [萨姆·沃特斯](../Page/萨姆·沃特斯.md "wikilink")（Sam Waterston），演员
  - [雪歌妮·薇佛](../Page/雪歌妮·薇佛.md "wikilink")（Sigourney
    Weaver），(MFA)。女演员。知名作品為[異形
    (電影)](../Page/異形_\(電影\).md "wikilink")
  - [Jennifer
    Westfeldt](../Page/Jennifer_Westfeldt.md "wikilink")，女演員，曾創作電影(*[Kissing
    Jessica Stein](../Page/Kissing_Jessica_Stein.md "wikilink")*)的劇本。

## 电视名人

  - [Dick Cavett](../Page/Dick_Cavett.md "wikilink")，電視名人（擁有自己的脫口秀）
  - [安德森·库珀](../Page/安德森·库珀.md "wikilink")，美國[CNN知名節目](../Page/CNN.md "wikilink")
    "Anderson Cooper 360"的主播。
  - [Bill Corbett](../Page/Bill_Corbett.md "wikilink") (DRA
    1989年)，演員、作家。在*[Mystery Science Theater
    3000](../Page/Mystery_Science_Theater_3000.md "wikilink")*中扮演[Crow
    T. Robot一角](../Page/Crow_T._Robot.md "wikilink")。
  - [大衛·杜考夫尼](../Page/大衛·杜考夫尼.md "wikilink")，(M.A. 英國文學)
    ，電視影集*[X檔案](../Page/X檔案.md "wikilink")*的男主角。
  - [Dick
    Ebersol](../Page/Dick_Ebersol.md "wikilink")，[國家廣播公司](../Page/國家廣播公司.md "wikilink")（NBC）運動頻道總裁，促成*[Saturday
    Night Live](../Page/Saturday_Night_Live.md "wikilink")（週末夜現場）*的推手。
  - [Sara
    Gilbert](../Page/Sara_Gilbert.md "wikilink")，女演員。最為人知的演出是在[情境喜劇](../Page/情境喜劇.md "wikilink")*[Roseanne](../Page/Roseanne.md "wikilink")*中演出"Darlene
    Conner"一角。
  - [Michael Gross](../Page/Michael_Gross.md "wikilink") (DRA
    1973年)，男演員。最為人知的角色是在*[Family
    Ties](../Page/Family_Ties.md "wikilink")*中扮演[Michael J.
    Fox的父親](../Page/Michael_J._Fox.md "wikilink") "Steven Keaton"
    。
  - [Leo
    Laporte](../Page/Leo_Laporte.md "wikilink")，在[TechTV主持](../Page/TechTV.md "wikilink")
    "The Screen Savers" 。
  - [Ari Meyers](../Page/Ari_Meyers.md "wikilink"), played Emma McArdle
    on *[Kate & Allie](../Page/Kate_&_Allie.md "wikilink")*
  - [Chris Noth](../Page/Chris_Noth.md "wikilink") (MFA), plays "Mr.
    Big" on *[慾望城市](../Page/慾望城市.md "wikilink")*
  - [Stone
    Phillips](../Page/Stone_Phillips.md "wikilink")，[NBC電視台新聞主播](../Page/NBC.md "wikilink")
  - [Robert Picardo](../Page/Robert_Picardo.md "wikilink")，
    *[星艦奇航記：重返地球](../Page/星艦奇航記：重返地球.md "wikilink")*中演出"總醫官的角色
  - [David Hyde
    Pierce](../Page/David_Hyde_Pierce.md "wikilink")，男演員。最知名的角色是在
    "Dr. Niles Crane" 中演出[Frasier一角](../Page/Frasier.md "wikilink")。
  - [Steve
    Skrovan](../Page/Steve_Skrovan.md "wikilink")，[情境喜劇](../Page/情境喜劇.md "wikilink")[人人都愛雷蒙德的監製](../Page/人人都愛雷蒙德.md "wikilink")。
  - [Ben Stein](../Page/Ben_Stein.md "wikilink") (法律)，經濟學家，主持節目 "Win Ben
    Stein's Money."
  - [Ming
    Tsai](../Page/Ming_Tsai.md "wikilink")，擔任[公共廣播協會頻道的節目](../Page/公共廣播協會.md "wikilink")\[\[chef
    on "East Meets West with Ming Tsai" 的主廚。
  - [Margaret Warner](../Page/Margaret_Warner.md "wikilink")，*The News
    Hour with [Jim
    Lehrer](../Page/Jim_Lehrer.md "wikilink")*的資深特派員(主播群之一)，這是一個每週於[公共廣播協會頻道播出的國際新聞節目](../Page/公共廣播協會.md "wikilink")。
  - [Henry Winkler](../Page/Henry_Winkler.md "wikilink")
    (MFA)，男演員。最知名的角色是在*[Happy
    Days](../Page/Happy_Days.md "wikilink")*裡演出"[Fonzie](../Page/Fonzie.md "wikilink")"。
  - [Sam Tsui](../Page/Sam_Tsui.md "wikilink") youtuber
  - [Kurt Hugo Schneider](../Page/Kurt_Hugo_Schneider.md "wikilink")
    youtuber

## 文学中的人物

  - "[Charles Montgomery
    Burns](../Page/C._M._Burns.md "wikilink")"，1914年。在知名的電視卡通影集[辛普森一家中](../Page/辛普森一家.md "wikilink")，是
    [Springfield Nuclear
    Powerplant的擁有者](../Page/Springfield_Nuclear_Powerplant.md "wikilink")。
  - "Linus
    Larrabee"，電影*[Sabrina](../Page/Sabrina.md "wikilink")*（龍鳳配）的主角，在1954年的版本是由[亨弗萊·鮑嘉飾演](../Page/亨弗萊·鮑嘉.md "wikilink")，1995年版本則是由[哈里森·福特飾演](../Page/哈里森·福特.md "wikilink")。
  - "Dink Stover"，1911年[Owen
    Johnson的](../Page/Owen_Johnson.md "wikilink")*Stover at Yale*的主角。
  - "Rory Gilmore"，影集[Gilmore
    Girls中的主角](../Page/Gilmore_Girls.md "wikilink")。
  - "Tom
    Buchanan"，[佛蘭西斯·史考特·基·費茲傑羅的](../Page/佛蘭西斯·史考特·基·費茲傑羅.md "wikilink")*[大亨小傳](../Page/大亨小傳.md "wikilink")*中的一角。
  - "Nick
    Carraway"，[佛蘭西斯·史考特·基·費茲傑羅的](../Page/佛蘭西斯·史考特·基·費茲傑羅.md "wikilink")*[大亨小傳](../Page/大亨小傳.md "wikilink")*中的解說員。

## 著名教授

从耶鲁大学毕业的教授用*斜体*.

  - *[哈羅德·布魯姆](../Page/哈羅德·布魯姆.md "wikilink")*
    ([Ph.D.](../Page/Ph.D..md "wikilink") 1955年)，作家與評論家，書籍 "Genius" 的作者。
  - [Yung-Chi (Tommy)
    Cheng](../Page/Yung-Chi_\(Tommy\)_Cheng.md "wikilink")（鄭永齊），[藥理學家](../Page/藥理學.md "wikilink")，[AIDS藥品](../Page/AIDS.md "wikilink")3TC的發明者，[3TC一般人熟悉的名稱是](../Page/3TC.md "wikilink")[Epivir](../Page/Epivir.md "wikilink")。
  - [Paul Kennedy](../Page/Paul_Kennedy.md "wikilink")，歷史學家。"The Rise
    and Fall of the Great Powers" 的作者。
  - [John
    Gaddis](../Page/John_Gaddis.md "wikilink")，歷史學家，[冷戰的專家](../Page/冷戰.md "wikilink")。
  - *[David
    Gelernter](../Page/David_Gelernter.md "wikilink")*(1976年)，電腦科學家，[Linda](../Page/Linda.md "wikilink")[程式語言的創作人之一](../Page/程式語言.md "wikilink")。
  - [Paul
    Hudak](../Page/Paul_Hudak.md "wikilink")，電腦科學家。他最知名的成就是－[Haskell](../Page/Haskell.md "wikilink")[程式語言](../Page/程式語言.md "wikilink")。書籍
    "The Haskell School of Expression" 的作者。
  - [本華·曼德博](../Page/本華·曼德博.md "wikilink")(Benoit
    Mandelbrot)，以[碎形几何学聞名的数学家](../Page/碎形几何学.md "wikilink")。
  - [William
    Prusoff](../Page/William_Prusoff.md "wikilink")，[藥理學家](../Page/藥理學.md "wikilink")。[AIDS的藥物](../Page/AIDS.md "wikilink")[d4T發明者](../Page/d4T.md "wikilink")。[d4T一般人熟悉的名稱是](../Page/d4T.md "wikilink")[Zerit](../Page/Zerit.md "wikilink")。
  - [Robert
    Shiller](../Page/Robert_Shiller.md "wikilink")，經濟學家。書籍"[Irrational
    Exuberance](../Page/Irrational_Exuberance_\(book\).md "wikilink")"的作者，以他發明的[心理學理論而聞名](../Page/心理學.md "wikilink")。
  - [史景迁](../Page/史景迁.md "wikilink")（Jonathan Spence），历史学家與作者。中國歷史權威。
  - [拉明·珊拿](../Page/拉明·珊拿.md "wikilink")-
    但以理·威利斯·詹姆斯[宣教和](../Page/宣教.md "wikilink")[世界基督教](../Page/世界基督教.md "wikilink")[教授](../Page/教授.md "wikilink") （英語：Daniel
    Willis James Professor of Missions and World Christianity）和歷史教授。

## 参考

[\*](../Category/耶鲁大学校友.md "wikilink")
[Category:耶鲁大学相关列表](../Category/耶鲁大学相关列表.md "wikilink")