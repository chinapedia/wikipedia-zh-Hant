**耿伯軒**（Po-Hsuan
Keng，﹞[台灣的棒球選手](../Page/台灣.md "wikilink")，暱稱為「**耿胖**」。曾到[美國職棒](../Page/美國職棒.md "wikilink")[多倫多藍鳥隊小聯盟發展](../Page/多倫多藍鳥.md "wikilink")，回台後也曾效力於[中華職棒](../Page/中華職棒.md "wikilink")[Lamigo桃猿](../Page/Lamigo桃猿.md "wikilink")、[中信兄弟隊](../Page/中信兄弟.md "wikilink")，守備位置是[投手](../Page/投手.md "wikilink")。

目前則擔任少棒隊教練、[美國職棒](../Page/美國職棒.md "wikilink")[聖地牙哥教士隊球探](../Page/聖地牙哥教士.md "wikilink")，以及[FOX體育台](../Page/FOX體育台.md "wikilink")[MLB轉播球評](../Page/美國職棒大聯盟.md "wikilink")。近期則幫教士隊簽下第一位亞洲業餘球員[宋文華](../Page/宋文華.md "wikilink")。\[1\]

## 經歷

  - [台北市福林國小少棒隊](../Page/台北市.md "wikilink")
  - [台北市華興中學青少棒隊](../Page/臺北市私立華興高級中學.md "wikilink")
  - [台北市華興中學青棒隊](../Page/台北市.md "wikilink")
  - [台灣體院](../Page/台灣體院.md "wikilink")（富邦公牛）棒球隊
  - [美國職棒](../Page/美國職棒.md "wikilink")[多倫多藍鳥](../Page/多倫多藍鳥.md "wikilink")（[小聯盟](../Page/小聯盟.md "wikilink")１Ａ－[小聯盟高階](../Page/小聯盟.md "wikilink")１Ａ）(2005年－2008年)
  - [中華職棒](../Page/中華職棒.md "wikilink")[La
    New熊隊](../Page/La_New熊.md "wikilink")（2009年－2011年1月5日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[Lamigo桃猿隊](../Page/Lamigo桃猿.md "wikilink")（2011年1月6日－2013年1月31日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[兄弟象隊](../Page/兄弟象.md "wikilink")（2013年2月1日－2013年12月3日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[中信兄弟隊](../Page/中信兄弟.md "wikilink")（2013年12月3日－2015年12月7日）

## 特殊事蹟

  - [總冠軍](../Page/中華職棒總冠軍賽.md "wikilink")：2012
  - 多倫多藍鳥隊新人聯盟明星球員
  - 2010年4月20日發生遭燙水燙至跨下以及重要部位的意外，需要兩週的時間休養。
  - 2013年1月31日，被[Lamigo桃猿球團外加三十萬元新台幣球團交易至](../Page/Lamigo桃猿.md "wikilink")[兄弟象](../Page/兄弟象.md "wikilink")，而猿隊獲得[王英山](../Page/王英山.md "wikilink")。

## 職棒生涯記錄

| 年度    | 球隊                                         | 出賽  | 先發 | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死 | 三振 | 責失 | 投球局數  | 自責分率  | 被上壘率 |
| ----- | ------------------------------------------ | --- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | ----- | ----- | ---- |
| 2009年 | [La new熊](../Page/La_new熊.md "wikilink")   | 55  | 0  | 5  | 3  | 12 | 1  | 0  | 0  | 19 | 25 | 25 | 61.1  | 3.67  | 1.21 |
| 2010年 | [La new熊](../Page/La_new熊.md "wikilink")   | 51  | 0  | 4  | 5  | 8  | 10 | 0  | 0  | 24 | 26 | 17 | 47.0  | 3.26  | 1.45 |
| 2011年 | [Lamigo桃猿](../Page/Lamigo桃猿.md "wikilink") | 24  | 0  | 2  | 1  | 6  | 1  | 0  | 0  | 12 | 9  | 12 | 19.1  | 5.59  | 2.02 |
| 2012年 | [Lamigo桃猿](../Page/Lamigo桃猿.md "wikilink") | 12  | 0  | 1  | 1  | 3  | 0  | 0  | 0  | 8  | 4  | 8  | 10.2  | 6.75  | 1.88 |
| 2013年 | [兄弟象](../Page/兄弟象.md "wikilink")           | 18  | 0  | 0  | 0  | 1  | 0  | 0  | 0  | 14 | 8  | 9  | 21.1  | 3.80  | 1.64 |
| 2014年 | [中信兄弟](../Page/中信兄弟.md "wikilink")         | 34  | 0  | 0  | 1  | 6  | 0  | 0  | 0  | 8  | 14 | 12 | 27.0  | 4.00  | 1.22 |
| 2015年 | [中信兄弟](../Page/中信兄弟.md "wikilink")         | 10  | 0  | 0  | 1  | 0  | 0  | 0  | 0  | 1  | 7  | 8  | 5.1   | 13.50 | 2.81 |
| 合計    | 7年                                         | 204 | 0  | 12 | 12 | 36 | 12 | 0  | 0  | 92 | 87 | 91 | 192.0 | 4.27  | 1.48 |

## 參考資料

## 外部連結

[K](../Category/台灣棒球選手.md "wikilink")
[K](../Category/中華青棒隊球員.md "wikilink")
[K](../Category/中華成棒隊球員.md "wikilink")
[K](../Category/多倫多藍鳥隊球員.md "wikilink")
[K](../Category/Lamigo桃猿隊球員.md "wikilink")
[K](../Category/兄弟象隊球員.md "wikilink")
[K](../Category/中信兄弟隊球員.md "wikilink")
[K](../Category/台灣旅美棒球選手.md "wikilink")
[K](../Category/台灣旅外歸國棒球選手.md "wikilink")
[K](../Category/國立臺灣體育學院校友.md "wikilink")
[K](../Category/台北市人.md "wikilink")
[K](../Category/耿姓.md "wikilink")

1.  [「菜鳥球探」耿伯軒跨出第一步
    推宋文華圓夢](http://udn.com/news/story/9687/1805437-%E3%80%8C%E8%8F%9C%E9%B3%A5%E7%90%83%E6%8E%A2%E3%80%8D%E8%80%BF%E4%BC%AF%E8%BB%92%E8%B7%A8%E5%87%BA%E7%AC%AC%E4%B8%80%E6%AD%A5-%E6%8E%A8%E5%AE%8B%E6%96%87%E8%8F%AF%E5%9C%93%E5%A4%A2)