[March_of_the_Volunteers.png](https://zh.wikipedia.org/wiki/File:March_of_the_Volunteers.png "fig:March_of_the_Volunteers.png")
**聂耳**（），原名**聂守信**，字**子义**（亦作**紫艺**），[云南](../Page/云南.md "wikilink")[玉溪人](../Page/玉溪.md "wikilink")，[中國](../Page/中國.md "wikilink")[音乐家](../Page/音乐家.md "wikilink")，主要从事包括电影插曲在内的流行音乐、中国民族音乐的创作，[中华人民共和国](../Page/中华人民共和国.md "wikilink")[国歌](../Page/国歌.md "wikilink")《[义勇军进行曲](../Page/义勇军进行曲.md "wikilink")》的[作曲者](../Page/作曲家.md "wikilink")。聶耳也被稱為[中華人民共和國新音樂運動的先驅](../Page/中華人民共和國.md "wikilink")\[1\]。

## 生平介绍

聂耳是[云南](../Page/云南.md "wikilink")[玉溪人](../Page/玉溪.md "wikilink")，出生于[昆明一個貧苦的](../Page/昆明.md "wikilink")[中醫家庭](../Page/中醫.md "wikilink")。从小喜爱[音乐](../Page/音乐.md "wikilink")，1918年就读于昆明师范附属小学。利用课余时间，聂耳自学了[笛子](../Page/笛子.md "wikilink")、[二胡](../Page/二胡.md "wikilink")、[三弦和](../Page/三弦.md "wikilink")[月琴等乐器](../Page/月琴.md "wikilink")，并开始担任学校“儿童乐队”的指挥。1922年，聂耳进入私立求实小学高级部，1925年考取云南省立第一联合中学插班生。

少年的聂耳曾虚心向一位木匠学习吹笛，并尊称木匠是他的第一位吹笛老师，奠定了他的音乐基础。

1927年毕业于云南省立第一联合中学，并进入云南省立第一师范学校。在校期间参与了学生组织“读书会”的活动，并与友人组织“九九音乐社”，经常参加校内外的演出活动。在这期间，聶耳还自学了[小提琴和](../Page/小提琴.md "wikilink")[钢琴](../Page/钢琴.md "wikilink")。

1930年夏天，聂耳从云南省立师范学校毕业，因参加反政府活动而列入“黑名单”，为此离开云南至上海。

1930年7月，初抵上海的聂耳进入昆明云丰商行所设的“云丰申庄”干活，并居住于同春里86号（现[公平路](../Page/公平路.md "wikilink")185弄）[亭子间](../Page/亭子间.md "wikilink")。

1931年春，云丰申庄关门，1931年4月聂耳考入[黎锦晖主办的](../Page/黎锦晖.md "wikilink")“明月歌舞剧社”，任小提琴手。1932年7月发表《中国歌舞短论》，并因批评黎氏被迫离团。1932年11月进入[联华影业公司工作](../Page/联华影业公司.md "wikilink")，参加“苏联之友社”音乐小组，并组织“中国新兴音乐研究会”，参加左翼戏剧家联盟音乐组。这一时期，聂耳与电影界的袁牧之、王人美等结识，为他们创作了大量电影音乐，并向王人美的哥哥王人艺学习小提琴。

1933年，聂耳由劇作家[田汉介紹加入](../Page/田汉.md "wikilink")[中国共产党](../Page/中国共产党.md "wikilink")。

1934年4月聂耳加入百代唱片公司主持音乐部工作，同时建立百代国乐队（又名“森森国乐队”）。这也是聂耳最多产的一年。1935年1月聂耳任联华二厂音乐部主任。1935年2月，[田汉为电影](../Page/田汉.md "wikilink")《[風雲兒女](../Page/風雲兒女.md "wikilink")》创作的主题歌（即日后的《[义勇军进行曲](../Page/义勇军进行曲.md "wikilink")》）歌词从监狱传出。聂耳听说后主动要求谱曲。

為免被捕，聶耳按照中共的決定離開[上海](../Page/上海.md "wikilink")，拟取道[日本赴](../Page/日本.md "wikilink")[苏联](../Page/苏联.md "wikilink")。1935年4月18日抵[东京后又修改了](../Page/东京.md "wikilink")《風雲兒女》主题歌曲谱的初稿并三次修改歌词，一并寄回中国。7月17日，在[神奈川县](../Page/神奈川县.md "wikilink")[藤澤市](../Page/藤澤市.md "wikilink")游泳时，不幸溺水身亡，年僅23歲。关于聂耳的死因，至今没有一个确切的说法，当时日本法医鉴定结论为溺死，官方的说法是聂耳在日本游泳时溺水身亡\[2\]，但也有被日本特务暗害\[3\]，突发[心脏病溺水猝逝等说法](../Page/心脏病.md "wikilink")\[4\]。

## 主要作品

<div style="float: right; margin: 0 0 1em; border: 1px solid #AAA;">

</div>

聂耳一生共创作37首乐曲，都是在他去世前不到两年的时间里所写的，其中反映工人阶级生活和斗争的歌曲占有较大比重。聂耳经常与[田汉合作](../Page/田汉.md "wikilink")。除《[义勇军进行曲](../Page/义勇军进行曲.md "wikilink")》外，聂耳的代表作品还有《[毕业歌](../Page/毕业歌.md "wikilink")》、《前进歌》、《[大路歌](../Page/大路_\(1935年电影\).md "wikilink")》、《[开路先锋](../Page/大路_\(1935年电影\).md "wikilink")》、《码头工人歌》、《新女性》、《飞花歌》、《塞外村女》、《铁蹄下的歌女》、《告别南洋》、《[梅娘曲](../Page/梅娘曲.md "wikilink")》、《[卖报歌](../Page/卖报歌.md "wikilink")》、歌剧《[扬子江暴风雨](../Page/扬子江暴风雨.md "wikilink")》及民族器乐曲《翠湖春晓》、《山国情侣》、《[金蛇狂舞](../Page/金蛇狂舞.md "wikilink")》等。

## 聂耳墓

[Monument_of_NIE_ER_at_KUGENUMA_Beach_2018-9-16.jpg](https://zh.wikipedia.org/wiki/File:Monument_of_NIE_ER_at_KUGENUMA_Beach_2018-9-16.jpg "fig:Monument_of_NIE_ER_at_KUGENUMA_Beach_2018-9-16.jpg")
[Nie_Er_Tomb_Kunming_2009_07.jpg](https://zh.wikipedia.org/wiki/File:Nie_Er_Tomb_Kunming_2009_07.jpg "fig:Nie_Er_Tomb_Kunming_2009_07.jpg")

### 日本藤泽

為了紀念游泳時不幸溺水身亡的聶耳，日本當地的有志之士於1954年修建了聶耳紀念碑，兩年後遭颱風襲擊損毀，1965年重新修建了聶耳墓，1985年日本當地政府出資修繕了聶耳墓，並新建了聶耳紀念碑，由當時的[藤澤市市長葉山峻題寫碑文](../Page/藤澤市.md "wikilink")。聶耳墓位於[江之島海濱浴場最西端的鵠沼海岸](../Page/江之島.md "wikilink")。碑文陳述了聶耳對音樂事業所作的貢獻，表達對其英年早逝的惋惜，最後祝願聶耳生前的微笑能成為中日世代友好的基石。

### 云南昆明

在聂耳的出生地云南昆明，亦有一座聂耳墓，位于昆明市[西山森林公园](../Page/西山森林公园.md "wikilink")，为[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。昆明与藤泽两市也于1981年缔结为友好城市。\[5\]

## 参考资料

## 外部链接

  - [聂耳纪念馆](https://web.archive.org/web/20110812152423/http://www.nie-er.org/)

  - [聂耳墓](https://web.archive.org/web/20070927014009/http://www.city.fujisawa.kanagawa.jp/shougai/page100072.shtml)

[category:藝人出身的政治人物](../Page/category:藝人出身的政治人物.md "wikilink")

[Category:國歌作曲人](../Category/國歌作曲人.md "wikilink")
[Category:中華民國作曲家](../Category/中華民國作曲家.md "wikilink")
[作](../Category/中华人民共和国国歌.md "wikilink") [Category:中国共产党党员
(1933年入党)](../Category/中国共产党党员_\(1933年入党\).md "wikilink")
[Category:死在日本的中国人](../Category/死在日本的中国人.md "wikilink")
[Category:溺死者](../Category/溺死者.md "wikilink")
[Category:昆明人](../Category/昆明人.md "wikilink")
[Category:玉溪人](../Category/玉溪人.md "wikilink")
[E耳](../Category/聂姓.md "wikilink")
[Category:中国电影音乐作曲家](../Category/中国电影音乐作曲家.md "wikilink")
[Category:葬于昆明](../Category/葬于昆明.md "wikilink")

1.  [中央電視台中文國際頻道](../Page/中央電視台中文國際頻道.md "wikilink")
2.
3.
4.
5.