**联邦科学与工业研究组织**（，[縮寫](../Page/縮寫.md "wikilink")：****）前身是于1926年成立的**科学与工业顾问委员会**（），是[澳大利亞聯邦最大的国家级科技研究机构](../Page/澳大利亞聯邦.md "wikilink")，主要角色是通过科学研究和发展，为[澳大利亚联邦政府提供新的科学途径](../Page/澳大利亚联邦政府.md "wikilink")，以造福于澳大利亚社会，提高经济效益和社会效益。联邦科学与工业研究组织[總部座落在](../Page/總部.md "wikilink")[澳洲首都特區](../Page/澳洲首都特區.md "wikilink")[坎培拉市](../Page/坎培拉.md "wikilink")[坎贝尔的](../Page/坎贝尔_\(澳大利亚首都特区\).md "wikilink")[澳洲戰爭紀念館旁](../Page/澳洲戰爭紀念館.md "wikilink")。

联邦科学与工业研究组织有逾6,600名员工，在[澳大利亚](../Page/澳大利亚.md "wikilink")、[法国及](../Page/法国.md "wikilink")[墨西哥拥有逾](../Page/墨西哥.md "wikilink")50座[研究站](../Page/研究站.md "wikilink")。

## 主要的研究成果

[CSIRO.jpg](https://zh.wikipedia.org/wiki/File:CSIRO.jpg "fig:CSIRO.jpg")[坎培拉市中心旁的坎普贝尔](../Page/坎培拉.md "wikilink")(Campbell)\]\]
[CSIRO_Darwin.jpg](https://zh.wikipedia.org/wiki/File:CSIRO_Darwin.jpg "fig:CSIRO_Darwin.jpg")\]\]
[CSIRO_Marine_and_Atmospheric_Research_Hobart.jpg](https://zh.wikipedia.org/wiki/File:CSIRO_Marine_and_Atmospheric_Research_Hobart.jpg "fig:CSIRO_Marine_and_Atmospheric_Research_Hobart.jpg")\]\]

发明了[原子吸收光谱法](../Page/原子吸收光谱法.md "wikilink")，开发了世界上第一种[塑料钞票](../Page/塑料钞票.md "wikilink")，发明了[航空驱虫剂](../Page/航空驱虫剂.md "wikilink")，[基因粘贴技术](../Page/基因粘贴.md "wikilink")，在澳大利亚推广了一系列[有害生物控制制度](../Page/有害生物控制.md "wikilink")，例如，利用[多发性粘液瘤](../Page/多发性粘液瘤.md "wikilink")（Myxomatosis）和[兔杯状病毒病](../Page/兔杯状病毒病.md "wikilink")（Rabbit
calicivirus）来控制澳大利亚过多的兔子。联邦科学与工业研究组织在[資訊及通訊科技方面的研究也取得了一系列成果](../Page/資訊及通訊科技.md "wikilink")，例如[Panoptic搜索引擎](../Page/Panoptic.md "wikilink")（现改称[Funnelback](../Page/Funnelback.md "wikilink")）、[votApedia电话问卷系统和](../Page/votApedia.md "wikilink")[Annodex视频内容标注系统](../Page/Annodex.md "wikilink")。

在2005年10月，[自然杂志发表了关于联邦科学与工业研究组织的科学家开发了一种技术](../Page/自然杂志.md "wikilink")，可以从[节肢弹性蛋白](../Page/节肢弹性蛋白.md "wikilink")（resilin）中提取出一种“近乎完美”的橡胶。节肢弹性蛋白是一种极富弹性的蛋白质，[跳蚤超凡的弹跳能力和很多昆虫的飞行能力都来源于这种蛋白质的作用](../Page/跳蚤.md "wikilink")。

### 802.11 & Wifi

近来，联邦科学与工业研究组织采取了一系列行动来保护其1996年取得的用于[无线局域网标准的底层技术专利](../Page/无线局域网.md "wikilink")，该技术专利跟[IEEE目前的](../Page/IEEE.md "wikilink")802.11a与802.11g无线标准有相当密切关系，现在已经广泛使用在几乎所有品牌的[笔记本电脑上](../Page/笔记本电脑.md "wikilink")。联邦科学与工业研究组织已经在美国采取一系列法律行动，以达到迫使包括[微软](../Page/微软.md "wikilink")，[苹果电脑和](../Page/苹果电脑.md "wikilink")[戴尔电脑在内的](../Page/戴尔电脑.md "wikilink")100多家美国公司缴纳专利使用费的目的。

## 主要科研项目

2014年列举的十大主要科研项目：\[1\]

  - [食品](../Page/食品.md "wikilink")&[营养](../Page/营养.md "wikilink")
  - [天文与](../Page/天文.md "wikilink")[空间科学](../Page/空间科学.md "wikilink")（包括[澳大利亚国家无线电天文台](../Page/澳大利亚国家无线电天文台.md "wikilink")）
  - [农业](../Page/农业.md "wikilink")
  - [生物安全](../Page/生物安全.md "wikilink")
  - [能源](../Page/能源.md "wikilink")
  - [矿产资源](../Page/矿产资源.md "wikilink")
  - [海洋](../Page/海洋.md "wikilink")&[大气](../Page/大气.md "wikilink")
  - [土地](../Page/土地.md "wikilink")&[水](../Page/水.md "wikilink")
  - [数字信息](../Page/数字信息.md "wikilink")[生产力与服务](../Page/生产力.md "wikilink")
  - [制造](../Page/制造.md "wikilink")

## 研究团体和分类

大类：

  - 天文与空间科学学院（包括澳洲国立无线电天文台）
  - 地球科学与资源工程
  - 能源科技
  - 食品及营养科学
  - 信息与计算机技术中心
  - 土地和水
  - 畜牧业
  - 海洋与大气研究
  - 材料科学与工程（包括之前的分子和卫生技术）
  - 数学，信息与统计
  - 处理科学与工程学院
  - 种植业
  - 生态系统的科学（包括昆虫学）
  - 恩瑟斯 - 林业和森林产品
  - 澳大利亚食品科学 - 与维多利亚州政府合作项目（也被称为“CSIRO的食品及营养科学部”）
  - 澳大利亚电子健康研究中心 - 与昆士兰州政府合作项目
  - 位于智利的卓越中心
  - 澳大利亚同步加速器
  - 堪培拉深空通讯基地（CDSCC） - 与美国航天局(NASA)合作

## 研究部门的分布

研究部门散布于澳洲全国：

  - [首都特区](../Page/澳大利亞首都特區.md "wikilink")：
      - 机构总部(Corporate Centre)
      - 黑山实验室(Black Mountain Laboratories)
      - [澳洲国立大学](../Page/澳洲国立大学.md "wikilink")
      - 堪培拉深层空间通讯基地(Canberra Deep Space Communication Complex)
      - 探索中心 (Discovery Centre)
      - 金宁德拉试验站(Ginninderra Experiment Station)
      - 甘加林家园(Gungahlin Homestead)
      - 亚勒兰拉(Yarralumla)

<!-- end list -->

  - [维多利亚州](../Page/维多利亚州.md "wikilink")：
      - 澳大利亚动物健康实验室(Australian Animal Health Laboratory)
      - 动物健康设施(Animal Health Facility)
      - 食品及营养科学(Food & Nutritional Sciences)
      - 帕克维利实验室(Parkville Laboratory)
      - 克莱顿实验室(Clayton Laboratories)
      - 阿斯宾代尔实验室(Aspendale Laboratories)
      - 海特实验室(Highett Laboratories)
      - 贝尔蒙特(Belmont)
      - 科研出版(CSIRO Publishing)

<!-- end list -->

  - [新南威尔士州](../Page/新南威尔士州.md "wikilink")：
      - 澳大利亚棉花研究小组Australian Cotton Research Unit (Myall Vale
      - 纽卡斯尔能源中心(Energy Centre)
      - FD麦克马斯特实验室(FD McMaster Laboratory)
      - 格里菲斯实验室(Griffith Laboratory)
      - 林德菲尔德实验室(Lindfield Laboratories)
      - 卢卡斯高地科学与技术中心（Lucas Heights Science and Technology Centre）
      - 北莱德(North Ryde)
      - 帕克斯天文台(Parkes Observatory)
      - [保罗怀尔德天文台](../Page/保罗怀尔德天文台.md "wikilink")
      - 无线电物理实验室(Radio Physics Laboratory)

<!-- end list -->

  - [昆士兰州](../Page/昆士兰州.md "wikilink")：
      - 农业生产系统的研究单位(Agricultural Production Systems Research Unit)
      - 阿瑟顿实验室(Atherton Laboratory)
      - 澳大利亚热带森林研究所(Australian Tropical Forest Institute)
      - 澳大利亚热带科学与创新园(Australian Tropical Sciences and Innovation
        Precinct)
      - 贝尔蒙研究站(Belmont Research Station)
      - 克利夫兰实验室，摩顿湾(Cleveland Laboratory, Moreton Bay)
      - 生态科学园(EcoSciences Precinct)
      - 食品及营养科学(Food and Nutritional Sciences)
      - 北昆士兰科技教育中心尔(North Queensland Science Education Centre)
      - 昆士兰生物科学园(Queensland Bioscience Precinct)
      - 昆士兰州的高级技术中心(Queensland Centre for Advanced Technologies)
      - 伍德斯托克 - 兰斯站(Woodstock - Lansdown Station)

等。

## 参考资料

## 外部链接

  - [官方網頁](http://www.csiro.au)
  - [CNetNews - Wi-Fi标准面临专利威胁
    冲击设备消费电子厂商](http://www.cnetnews.com.cn/news/tel/story/0,3800050315,39551017,00.htm)

[Category:國家科學院](../Category/國家科學院.md "wikilink")
[Category:坎培拉](../Category/坎培拉.md "wikilink")
[Category:1926年建立的组织](../Category/1926年建立的组织.md "wikilink")

1.