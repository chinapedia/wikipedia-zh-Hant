[Stade_Geoffroy-Guichard_-_Saint-Etienne_(10-11-2013).jpg](https://zh.wikipedia.org/wiki/File:Stade_Geoffroy-Guichard_-_Saint-Etienne_\(10-11-2013\).jpg "fig:Stade_Geoffroy-Guichard_-_Saint-Etienne_(10-11-2013).jpg")
**聖伊天體育會**（，，簡稱**A.S. Saint-Étienne**
或[首字母縮略字](../Page/首字母縮略字.md "wikilink")**ASSE**）是[法國的一家](../Page/法國.md "wikilink")[體育會](../Page/體育會.md "wikilink")，位處法國中东部[卢瓦尔省首府的](../Page/卢瓦尔省.md "wikilink")[聖德田市](../Page/聖德田.md "wikilink")。聖伊天乃法國一家最具歷史兼最成功的體育會，曾10奪法國甲組聯賽冠軍。

## 球會歷史

圣艾蒂安競技俱乐部是在1919年由一家雜貨店連鎖店的僱員成立。當時球會只是採用雜貨店招牌作名稱，因法國足總規例所限，1920年改稱 Amical
Sporting Club。球會名稱經過多次更改，最於1933年正式稱為**聖伊天體育會**。

聖伊天首個[法甲聯賽冠軍是在](../Page/法甲聯賽.md "wikilink")1957-58年獲得，翌年出席歐洲賽事。但之後要到1964年才再奪法甲冠軍。1967至1970年間聖伊天更連續四屆贏法甲聯賽冠軍，和兩屆法國杯冠軍。踏入七十年代聖伊天再踏上高峰，在1974年和1975年兩屆再度成為聯賽和杯賽雙料冠軍；1976年不但聯賽衛冕成功，在[歐冠杯更闖入決賽](../Page/歐冠杯.md "wikilink")，只是不敵當時的歐冠杯冠軍[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")。至於第十個聯賽冠軍則是在1981年取得。

在1982年一單財務醜聞導致球隊的衰敗。當時的會長羅傑·羅什（Roger
Rocher）被迫離開球隊而且在監牢中度過好幾個月。自此以後，球隊不但一個冠軍也沒取得，大部分時間只能在甲組下游，甚或在乙組中渡過。直到2004年奪得乙組冠軍後再升回頂級聯賽，隨即獲得聯賽第六位，取得[圖圖盃的參賽資格](../Page/圖圖盃.md "wikilink")，再次進軍歐洲比賽。

## 球會榮譽

  - **[法國甲組足球聯賽](../Page/法國甲組足球聯賽.md "wikilink")**
      - 冠軍（10）：1957、1964、1967、1968、1969、1970、1974、1975、1976、1981
      - 亞軍（3）：1946、1971、1982

<!-- end list -->

  - **[法國乙組足球聯賽](../Page/法國乙組足球聯賽.md "wikilink")**
      - 冠軍（3）：1963、1999、2004
      - 亞軍（2）：1938、1986

<!-- end list -->

  - **[法國盃](../Page/法國盃.md "wikilink")**
      - 冠軍（6）：1962、1968、1970、1974、1975、1977
      - 亞軍（3）：1960、1981、1982

<!-- end list -->

  - **[法國聯賽盃](../Page/法國聯賽盃.md "wikilink")**
      - 冠軍（1）：2013

<!-- end list -->

  - **[歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")**
      - 亞軍（1）：1976

## 著名球星

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/威利·萨尼奥尔.md" title="wikilink">威利·萨尼奥尔</a> (Willy Sagnol)</li>
<li><a href="../Page/Alex_Dias_de_Almeida.md" title="wikilink">亚历克斯</a> (Alex)</li>
<li><a href="../Page/艾梅·雅凯.md" title="wikilink">艾梅·雅凯</a> (Aime Jacquet)</li>
<li><a href="../Page/洛朗·布兰克.md" title="wikilink">白蘭斯</a>（Laurent Blanc）</li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><a href="../Page/Vladimir_Durković.md" title="wikilink">杜高域</a> (Vladimir Durković)</li>
<li><a href="../Page/雅克·桑蒂尼.md" title="wikilink">雅克·桑蒂尼</a>（Jacques Santini）</li>
<li><a href="../Page/多米尼克·巴特纳伊.md" title="wikilink">多米尼克·巴特纳伊</a>（Dominique Bathenay）</li>
<li><a href="../Page/多米尼克·罗歇托.md" title="wikilink">洛捷圖</a>（Dominique Rocheteau）</li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><a href="../Page/米歇尔·普拉蒂尼.md" title="wikilink">-{zh-hans:米歇尔·普拉蒂尼; zh-hk:米高·柏天尼; zh-tw:米歇爾·普拉蒂尼;}-</a></li>
<li><a href="../Page/约翰尼·雷普.md" title="wikilink">立普</a>（Johnny Rep）</li>
<li><a href="../Page/罗杰·米拉.md" title="wikilink">米拿</a>（Roger Milla）</li>
<li><a href="../Page/卢波米尔·莫拉维茨克.md" title="wikilink">卢波米尔·莫拉维茨克</a>（Lubomir Moravčík）</li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><a href="../Page/格雷戈里·库佩.md" title="wikilink">哥柏</a>（Grégory Coupet）</li>
<li><a href="../Page/皮埃爾-埃默里克·奧巴梅揚.md" title="wikilink">奧巴美揚</a>（Pierre-Emerick Aubameyang）</li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [聖伊天官方網站](http://www.asse.fr)

[分類:1919年建立的足球俱樂部](../Page/分類:1919年建立的足球俱樂部.md "wikilink")

[S](../Category/法國足球俱樂部.md "wikilink")
[Category:1919年法國建立](../Category/1919年法國建立.md "wikilink")