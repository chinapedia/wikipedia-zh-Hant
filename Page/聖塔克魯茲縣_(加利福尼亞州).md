**圣克鲁斯縣**（[英文](../Page/英文.md "wikilink")：**Santa Cruz
County**）是[美國](../Page/美國.md "wikilink")[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[太平洋岸邊的一縣](../Page/太平洋.md "wikilink")，成立於1850年，是加州最早成立的縣之一。位於[舊金山灣區的南方](../Page/舊金山灣區.md "wikilink")。聖塔克魯茲縣佔據了[蒙特瑞灣](../Page/蒙特瑞灣.md "wikilink")（Monterey
Bay）的北岸，[蒙特瑞縣則位於东南岸](../Page/蒙特雷縣_\(加利福尼亞州\).md "wikilink")。根據[2010年美国人口普查](../Page/2010年美国人口普查.md "wikilink")，聖塔克魯茲縣共有26萬2382人居住在此。縣府所在地為[聖塔克魯茲市](../Page/聖塔克魯茲_\(加利福尼亞州\).md "wikilink")。圣塔克鲁兹县的名称来源于[西班牙语](../Page/西班牙语.md "wikilink")“聖十字架”（"Holy
Cross"）的意思。该县被群山从北面半包围，南面与西面是海，拥有[地中海式气候](../Page/地中海式气候.md "wikilink")，夏季少雨却多雾，冬季多雨而湿润。由于地处海边，它的夏天要比山北面另一头的[圣克拉拉县凉爽得多](../Page/圣克拉拉县.md "wikilink")。其依山傍水的地理位置使得圣塔克鲁兹县成为加州较为有名的度假与冲浪胜地。

## 行政区划

圣塔克鲁兹境內有四个已建制的城市，以及27个未建制社区。

| 城市名称                                                          | 位置                                                                                                                                                                                                                                                                                                                                                       | 图片                                                                                              | 人口     | 面积（平方千米） |
| ------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- | ------ | -------- |
| [卡皮托拉 (Capitola)](../Page/卡皮托拉_\(加利福尼亚州\).md "wikilink")      | [Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Capitola_Highlighted.svg](https://zh.wikipedia.org/wiki/File:Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Capitola_Highlighted.svg "fig:Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Capitola_Highlighted.svg")                 | [Capitola_smt.jpg](https://zh.wikipedia.org/wiki/File:Capitola_smt.jpg "fig:Capitola_smt.jpg") | 9,918  | 4.340    |
| [圣塔克鲁兹 (Santa Cruz)](../Page/圣塔克鲁兹_\(加利福尼亚州\).md "wikilink")  | [Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Santa_Cruz_Highlighted.svg](https://zh.wikipedia.org/wiki/File:Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Santa_Cruz_Highlighted.svg "fig:Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Santa_Cruz_Highlighted.svg")          | [200px](../Page/file:_Downtown_santa_cruz,_cropped.jpg.md "wikilink")                           | 59,946 | 40.996   |
| [斯科茨谷 (Scotts Valley)](../Page/斯科茨谷_\(加利福尼亚州\).md "wikilink") | [Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Scotts_Valley_Highlighted.svg](https://zh.wikipedia.org/wiki/File:Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Scotts_Valley_Highlighted.svg "fig:Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Scotts_Valley_Highlighted.svg") | [200px](../Page/file:_Scotts_Valley_CA_city_hall.jpg.md "wikilink")                             | 11,580 | 11.900   |
| [沃森维尔 (Watsonville)](../Page/沃森维尔_\(加利福尼亚州\).md "wikilink")   | [Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Watsonville_Highlighted.svg](https://zh.wikipedia.org/wiki/File:Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Watsonville_Highlighted.svg "fig:Santa_Cruz_County_California_Incorporated_and_Unincorporated_areas_Watsonville_Highlighted.svg")        | [200px](../Page/file:DowntownWatsonville.JPG.md "wikilink")                                     | 51,199 | 17.569   |

## 人口

[2010年美国人口普查报告该县有人口](../Page/2010年美国人口普查.md "wikilink")262,382人。种族基本构成为72.5%白人、1.1%黑人、0.9%北美原住民、4.2%亚洲人、0.1%太平洋岛屿居民、16.5%其他种族、4.7%同时来自两个或更多的种族。墨西哥以及拉美裔有32.0%。

2000年，该县每户收入中位数为$53,998，每家庭收入中位数为$61,941。人均收入$26,396。6.7%的家庭和11.9%的人口处于[贫穷线下](../Page/贫穷线.md "wikilink")，包括12.50%的未成年人和6.3%的65岁以上老人。

该县居民教育水平较高。38.3%的25岁以上居民拥有至少一个学士学位。比加州平均的29.5%、美国平均的27.2%都要高出一些。\[1\]

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/2010年美国人口普查.md" title="wikilink">2010年美国人口普查</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>'''</p>
<center>
<p>县'''</p>
</center></td>
</tr>
<tr class="even">
<td><p><strong>圣塔克鲁兹县</strong></p></td>
</tr>
<tr class="odd">
<td><p>'''</p>
<center>
<p><a href="../Page/城市.md" title="wikilink">城市</a>'''</p>
</center></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/卡皮托拉_(加利福尼亚州).md" title="wikilink">卡皮托拉 (Capitola)</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/Santa_Cruz,_California.md" title="wikilink">圣塔克鲁兹 (Santa Cruz)</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/Scotts_Valley,_California.md" title="wikilink">斯科茨谷 (Scotts Valley)</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/Watsonville,_California.md" title="wikilink">沃森维尔 (Watsonville)</a></strong></p></td>
</tr>
</tbody>
</table>

## 参考文献

[Santa Cruz](../Category/加利福尼亞州行政區劃.md "wikilink")
[Category:旧金山湾区行政区划](../Category/旧金山湾区行政区划.md "wikilink")

1.