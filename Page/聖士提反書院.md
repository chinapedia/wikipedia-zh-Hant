**聖士提反書院**（）是[香港一所](../Page/香港.md "wikilink")[基督教](../Page/基督教.md "wikilink")[直資男女中學](../Page/直接資助計劃.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區_\(香港\).md "wikilink")[赤柱](../Page/赤柱.md "wikilink")[東頭灣道](../Page/東頭灣道.md "wikilink")22號，佔地[面積約](../Page/面積.md "wikilink")\[1\]，為[香港面積最大的一所中學](../Page/香港.md "wikilink")，亦是香港少數[寄宿學校之一](../Page/寄宿學校.md "wikilink")，多座校舍被列為歷史建築。學校於1903年創校，當時只有6名宿生和1名走讀生就讀\[2\]，曾享有東方[伊頓公學的美譽](../Page/伊頓公學.md "wikilink")。2014至2015年度，學校有914名學生就讀\[3\]。現任校長為楊清女士。聖士提反書院使用[英文作為主要](../Page/英文.md "wikilink")[教學語言](../Page/教學語言.md "wikilink")，除[中國語文科](../Page/中國語文.md "wikilink")、[中國歷史科](../Page/中國歷史.md "wikilink")、[普通話科](../Page/普通話.md "wikilink")、家政科、[視覺藝術科及](../Page/視覺藝術.md "wikilink")[體育科外](../Page/體育.md "wikilink")，其餘所有科目均使用[英語作為教學語言](../Page/香港英文授課中學.md "wikilink")。

## 歷史

十九世紀末至二十世紀初的中國因為科技與及思想上的落後而飽受西方列強欺壓，一群香港知識份子深明救國需由教育入手，遂計畫建立一所書院。1901年，包括[何啟等八位香港商人及知識份子致函殖民地總督建議成立一所英文書院為本地華人子弟提供教育](../Page/何啟.md "wikilink")，這促成了1903年聖士提反書院的創立。建校之時，校舍位於[西營盤](../Page/西營盤.md "wikilink")[般咸道今](../Page/般咸道.md "wikilink")[香港大學所在地](../Page/香港大學.md "wikilink")，並於1924年至1929年短暫遷往[薄扶林](../Page/薄扶林.md "wikilink")。1923年，書院獲得政府批出[赤柱一塊地皮以建立新校舍](../Page/赤柱.md "wikilink")（今書院所在地），1928年4月27日舉行動土儀式，邀得當時的殖民地總督[金文泰爵士出席](../Page/金文泰.md "wikilink")。1930年，學校主樓建成，由署理[港督](../Page/港督.md "wikilink")[修頓爵士揭幕](../Page/修頓.md "wikilink")。\[4\]

1941年12月8日日軍侵港，學校大樓被港府改作緊急軍事醫院。12月24日，駐港英軍東旅撤往赤柱，並於赤柱建立最後三條防線，其中第二條防線橫穿書院。書院一帶曾爆發激戰。12月25日早上日軍攻佔聖士提反書院後，發生屠殺傷兵及醫務人員事件，此次事件為[聖士提反書院大屠殺](../Page/聖士提反書院大屠殺.md "wikilink")。當時約150至200多名日軍闖進軍事醫院，屠殺事件中最少有60多名人員被殺害，包括56位負傷臥床的英軍及加拿大軍人，以至部分醫護及學校職工都被刺刀殺死，包括時任中文科主任譚長萱先生。此為[香港保衛戰中最後及最大一宗屠殺事件](../Page/香港保衛戰.md "wikilink")\[5\]。

[香港保衛戰期間學校兩位外籍員工靳約翰及翟阿瑟都參加了](../Page/香港保衛戰.md "wikilink")[香港義勇軍對抗日軍](../Page/香港義勇軍團.md "wikilink")，翟阿瑟約於1941年12月19日的港島戰事中被殺，遺體大概被埋於[畢架山某處](../Page/畢架山.md "wikilink")。靳約翰先生則被俘為囚，於1944年1月4日死於[深水埗集中營](../Page/深水埗集中營.md "wikilink")，二人現於[赤柱軍人墳場立有衣物塚](../Page/赤柱軍人墳場.md "wikilink")。

在隨後的[三年零八個月中](../Page/三年零八個月.md "wikilink")，日軍徵用書院校舍作[拘留營](../Page/赤柱拘留營.md "wikilink")，收容近千名英軍及加拿大士兵。小學部曾短暫用作拘禁俘虜，但不久轉為守衛的營房。當時日本容許戰俘作有限度地的營房自管，戰俘在校內的大禮堂舉行不同形式的娛樂，如電影，話劇、音樂會及宗教活動，亦成為營中學生初、高中教育場地。

根據[葉靈鳳著作香島滄桑錄記載](../Page/葉靈鳳.md "wikilink")，英屬香港旗幟[香港旗是一個英國人在香港被日本人佔領時期](../Page/香港旗.md "wikilink")，在[赤柱戰犯集中營內所繪](../Page/赤柱拘留營.md "wikilink")，經過[倫敦](../Page/倫敦.md "wikilink")[英國紋章院修改後於](../Page/英國紋章院.md "wikilink")1959年1月21日頒布成為[香港盾徽成為戰後香港的象徵](../Page/香港盾徽.md "wikilink")。

日本戰敗後，由於校舍損毀嚴重，書院未有馬上恢復運作，在維修的同時政府租用了校舍作為臨時警察訓練學校。書院於1947年重新運作至今。聖士提反書院於1968年起由男校轉為男女校，並由1970年起轉為津貼中學。2008年起由津貼學校轉為[直資學校](../Page/直資.md "wikilink")。

### 校董會

  - [錢果豐博士](../Page/錢果豐.md "wikilink")（主席） -
    [港鐵前非執行主席](../Page/港鐵.md "wikilink")、[恆生銀行非執行董事長](../Page/恆生銀行.md "wikilink")
  - [董建成先生](../Page/董建成.md "wikilink")（副主席） -
    [東方海外（國際）有限公司主席兼](../Page/東方海外.md "wikilink")[行政總裁](../Page/行政總裁.md "wikilink")、[國泰航空獨立](../Page/國泰航空.md "wikilink")[非執行董事](../Page/非執行董事.md "wikilink")、[全國政協副主席兼前行政長官](../Page/全國政協副主席.md "wikilink")[董建華之弟](../Page/董建華.md "wikilink")
  - [練松柏律師](../Page/練松柏.md "wikilink")（榮譽主席）
  - [梁貫成教授](../Page/梁貫成.md "wikilink")（校監） -
    聖士提反書院校監、[香港大學](../Page/香港大學.md "wikilink")[教育學院教授](../Page/教育學院.md "wikilink")、全球數學教育界最高榮譽的2013年[費萊登特爾獎得主](../Page/費萊登特爾獎.md "wikilink")，亦是首位亞洲學者獲頒此項國際殊榮
  - 王怡瑞先生（義務司庫）
  - 蔡潘少芬女士
  - [鄭慕智博士](../Page/鄭慕智.md "wikilink") -
    [香港電視網絡非執行董事](../Page/香港電視網絡.md "wikilink")、香港[立法局前議員](../Page/立法局.md "wikilink")、[香港董事學會榮譽會長及榮譽主席](../Page/香港董事學會.md "wikilink")
  - 陶榮博士
  - 區愷慈律師
  - 管浩鳴法政牧師 - [香港聖公會教省秘書長](../Page/香港聖公會.md "wikilink")
  - 袁彌望女士 - 聖士提反書院舊生會主席
  - 陳鐘亮先生
  - 楊清女士 - 聖士提反書院校長
  - 衞燕華博士 - [聖士提反書院附屬小學校長](../Page/聖士提反書院附屬小學.md "wikilink")

\[6\]

### 歷任校長及舍監

聖士提反書院由創校至今一直是一所寄宿學校，故此校長亦兼任舍監。

  - 1903年至1914年：The Ven. E.J. Barnett（班納會吏長）\[7\]
  - 1914年至1915年：The Revd. A.D. Stewart（史超活牧師）\[8\]
  - 1915年至1928年：The Revd. W.H. Hewitt（侯活牧師）\[9\]
  - 1928年至1953年：Canon E.W.L. Martin（馬田法政牧師）\[10\]\[11\]
  - 1953年至1955年：The Revd. F.R. Myhill（米希爾牧師）\[12\]
  - 1955年至1956年：Rev. R.W. Bowie（署理）
  - 1956年至1958年：Mr. C.T. Priestley（費至理先生）\[13\]
  - 1958年至1965年：Mr. J.R.F. Melluish（麥鱗書先生）\[14\]
  - 1965年至1973年：The Revd. R.B. Handforth（韓復士牧師）\[15\]
  - *1973年至 1974年：Ven. W.N. Cheung（署理）*
  - 1974年至1999年：Mr. Luke J.P. Yip（葉敬平先生）\[16\]
  - 1999年至2004年：Mr. D.R. Too（朱業桐先生）\[17\]
  - 2005年至2011年：Dr. Louise Y.S. Law（羅懿舒博士）\[18\]
  - 2011年至今：Ms. Carol C. Yang（楊清女士）\[19\]

## 校訓

篤信多能（；）

## 校園

聖士提反書院校園佔地甚廣，擁有多座不同的校舍及設施。\[20\]\[21\]

主要建築物有：
[St._Stephen's_College_-_School_House.JPG](https://zh.wikipedia.org/wiki/File:St._Stephen's_College_-_School_House.JPG "fig:St._Stephen's_College_-_School_House.JPG")

1.  書院大樓
      - 分為東翼（代號：EW）與西翼（代號：WW），同建於1928年至1929年間，1930年由時任[香港護督](../Page/香港護督.md "wikilink")[修頓舉行開幕儀式](../Page/修頓.md "wikilink")\[22\]，是校舍最先建成的大樓之一，以[裝飾藝術風格建造](../Page/裝飾藝術.md "wikilink")。
    <!-- end list -->
    1.  東翼一樓為男生宿舍，二樓為教職員宿舍，露台名為莫幹生先生紀念樓\[23\]，地下設有教員室五、教員室六、職業及升學支援中心及[音樂室](../Page/音樂.md "wikilink")。
    2.  西翼一樓為女生宿舍，地下設有教員室二、教員室三、小書齋、非教學職員室、印刷室、教員室四及教職員電腦室。
    3.  兩翼連接處設有九十周年校慶王澤森[圖書館](../Page/圖書館.md "wikilink")（代號：LB）及鍾普洋科技輔助學習中心。
          - 圖書館於1990年代設立，現址前身是禮堂，並於1994年遷至現址。圖書館面積近8000平方尺，分為兩層，藏書近6萬冊，地下設有15部電腦，供同學使用。
          - 鍾普洋科技輔助學習中心為於圖書館內二樓，於1990年代末設立，另設14部電腦，於2013年9月起不再設有電腦，改為自修間隔桌。
    <!-- end list -->
      - 學校大樓已在2011年12月17日被列為[法定古蹟](../Page/法定古蹟.md "wikilink")。
2.  馬田宿舍（前稱宿舍、代號：M）
      - 建於1929年。馬田宿舍樓高三層，地下為教職員宿舍、走讀生飯堂及洗衣房，一樓為男生宿舍，二樓為教職員宿舍，混合了歐洲不同的建築特色。馬田宿舍外設有一個籃球場及一個綜合網球及排球場。
      - 馬田宿舍已在2010年4月16日被列為[二級歷史建築](../Page/二級歷史建築.md "wikilink")。
3.  第八座／舊實驗室\[24\]
      - 建於1930年\[25\]，現為教職員宿舍。
      - 舊實驗室已在2010年4月16日被列為[二級歷史建築](../Page/二級歷史建築.md "wikilink")。
4.  一至五號屋（Bungalow No.
    1-5）[聖士提反書院5號平房.JPG](https://zh.wikipedia.org/wiki/File:聖士提反書院5號平房.JPG "fig:聖士提反書院5號平房.JPG")
      - 同建於1931年，一號屋為職員宿舍，二號屋為校長居所，三號屋改建為文物館，四號屋為文物學習室，五號屋為牧師居所。
      - 一至五號屋已在2010年4月16日全被列為[二級歷史建築](../Page/二級歷史建築.md "wikilink")。
5.  小教堂與副堂[聖士提反書院聖士提反堂.JPG](https://zh.wikipedia.org/wiki/File:聖士提反書院聖士提反堂.JPG "fig:聖士提反書院聖士提反堂.JPG")
      - 同建於1950年，位於全校最高處。1950年3月4日由[何明華會督揭幕](../Page/何明華會督.md "wikilink")。建造小教堂是用來紀念在日佔時期在聖士提反書院（時為[赤柱拘留營的一部分](../Page/赤柱拘留營.md "wikilink")）逝世的人。小教堂門上的彩繪玻璃由曲氏夫婦於1995年捐贈，描繪了日治時期拘留營的艱苦歲月。小教堂內有聖士提反人像的彩繪玻璃及來自聖芭芭拉小教堂的碑石紀念在[香港保衛戰陣亡的](../Page/香港保衛戰.md "wikilink")[皇家砲兵團和](../Page/皇家砲兵團.md "wikilink")[里斯本丸號沉沒事件中遇難的士兵](../Page/里斯本丸.md "wikilink")。
      - 小教堂已在2010年1月22日被列為[三級歷史建築](../Page/三級歷史建築.md "wikilink")。
6.  北屋
      - 建於1952年\[26\]，於2005年重新裝修。北屋樓高兩層，地下為女生宿舍及教職員宿舍，一樓為女生宿舍。北屋外設有一個籃場、三個網球場及燒烤場。
7.  教學樓（代號：CR）
      - 建於1964年\[27\]，樓高三層，共有一間[設計與科技工場及二十三間課室](../Page/設計與科技.md "wikilink")。傳聞因發生錯誤導致教學樓前後調轉了興建。\[28\]
      - 教學樓已在2010年1月22日被列為[三級歷史建築](../Page/三級歷史建築.md "wikilink")。
8.  伍華堂（代號：NH）[HKSSC_Ng_Wah_Hall.jpeg](https://zh.wikipedia.org/wiki/File:HKSSC_Ng_Wah_Hall.jpeg "fig:HKSSC_Ng_Wah_Hall.jpeg")
      - 建於1970年\[29\]。伍華堂樓高兩層，地下有五至七號琴室、一間音樂協會室、一間運動用品倉和一間健身室，一樓為禮堂。
      - 伍華堂已在2010年1月22日被列為[三級歷史建築](../Page/三級歷史建築.md "wikilink")。
9.  一至四號琴室、大提琴室及敲擊樂室
      - 位於伍華堂旁，於2010年翻新。
10. 鄧肇堅運動場（代號：F）
      - 俗稱大球場(Big
        Field)，建於1920年代，為防[瘧疾感染](../Page/瘧疾.md "wikilink")，填平大片沼澤而建成，分別於1975年\[30\]及2010年重鋪草地。日佔期間，運動場被轉作菜田，為戰俘提供食物。1975年獲[鄧肇堅爵士資助重新鋪草並改名為鄧肇堅運動場](../Page/鄧肇堅.md "wikilink")。鄧肇堅運動場設有一組長三百五十米的六線跑步徑、一個標準足球場（可分成兩個小型足球場）、一個射箭場及一個鉛球場。
11. 科藝樓（代號：SR）
      - 建於1980年\[31\]，由何添樓、李嘉誠樓、包兆龍樓及鄧肇堅堂各層互相連接組成。[Tang_Shiu_Kin_Hall,_St._Stephen's_College,_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Tang_Shiu_Kin_Hall,_St._Stephen's_College,_Hong_Kong.jpg "fig:Tang_Shiu_Kin_Hall,_St._Stephen's_College,_Hong_Kong.jpg")
    <!-- end list -->
    1.  何添樓由兩幢各成三角形的大樓組成，樓高三層，地下設有一間[視聽影院](../Page/視聽影院.md "wikilink")、兩間[物理](../Page/物理.md "wikilink")[實驗室及](../Page/實驗室.md "wikilink")[電腦室二](../Page/電腦.md "wikilink")，一樓設有一間[多媒體學習室](../Page/多媒體.md "wikilink")、一間[伺服器室及兩間](../Page/伺服器.md "wikilink")[生物](../Page/生物.md "wikilink")[實驗室](../Page/實驗室.md "wikilink")，二樓為兩間[化學](../Page/化學.md "wikilink")[實驗室](../Page/實驗室.md "wikilink")。
    2.  李嘉誠樓樓高兩層，地下為兩間[綜合科學](../Page/科學.md "wikilink")[實驗室](../Page/實驗室.md "wikilink")，一樓設有一間[地理室及](../Page/地理.md "wikilink")[電腦室一](../Page/電腦.md "wikilink")，頂層天台為開放空間。
    3.  包兆龍樓樓高兩層，地下為一間[視覺藝術室](../Page/視覺藝術.md "wikilink")、一間[陶瓷室](../Page/陶瓷.md "wikilink")，一樓設有兩間[家政室](../Page/家政.md "wikilink")，頂層天台為開放空間。
    4.  鄧肇堅堂（代號：TH/T）於2002年加設空氣調節系統。地下為禮堂入口，一樓設有控制室。為學校之室內[體育館及舉行](../Page/體育館.md "wikilink")[早會和畢業禮的禮堂](../Page/早會.md "wikilink")。
    <!-- end list -->
      - 科藝樓地下對開之空地設有鐘樓。禮堂旁設有半個籃球場。
      - 設計者[何弢博士因此建築而獲](../Page/何弢.md "wikilink")1982[香港建築師學會](../Page/香港建築師學會.md "wikilink")[周年年獎優異獎](../Page/周年年獎優異獎.md "wikilink")。
12. 王澤森水運中心（代號：WS）
      - 建於1982年\[32\]，建有一個室外六線五十米游泳池（五十米乘十四米）、能容納近千人的看台、男更衣室、女更衣室、控制室及泵房。更衣室於2011年翻新，並同時在看台無上蓋部分加上帆布遮擋太陽。
13. 柯俊文樓
      - 建於1982年\[33\]，樓高兩層。地下為小食部，一樓為教職員飯堂及宿生飯堂。
14. 葉敬平樓（前稱行政樓）
      - 建於1985年\[34\]，於2000年為紀念葉敬平校長對學校的貢獻更改為現稱\[35\]。地下設有教員室一，一樓設有校務處、校長室、三個副校長室、校長秘書室及醫療室。
15. 梁球鋸學生中心（代號：SC）
      - 建於1988年\[36\]。學生中心樓高四層，地下為簿房，一樓設有學長室、[學生會室及校園電視台室](../Page/學生會.md "wikilink")，二樓為舊生會室、家長教師會室、[社工室](../Page/社工.md "wikilink")、中文學會室及攝影學會室，三樓設有三間課室。
16. 百周年紀念大樓（代號：CB）
      - 為慶祝學校成立一百周年興建。大樓建於2006年，於2009年至2010年間加建一層及改動原有間隔以增加課室數目，2015年於低層地下再加建課室。大樓現樓高四層，地下設有一間學生活動室、兩間課室、一間電腦輔助學習室、一間電腦輔助學習預備室及一間語言室，一樓設有一間會議室、一間多用途室及五間課室，二樓為八間課室，地下低層為兩間課室及有蓋操場，有數張乒乓球桌及數部足球機。綠化天台設有[太陽能電池板](http://www.sunnyportal.com/Templates/PublicPageOverview.aspx?plant=57fd9ff0-22a9-4365-81be-9c383d85304f)，為大樓部分照明設施供電。百周年紀念大樓旁設有百周年紀念花園，設有噴水池及校徽形狀的紀念塔。
17. College House A,
    B\[37\]（代號：CH）[College_House_of_St._Stephen's_College.JPG](https://zh.wikipedia.org/wiki/File:College_House_of_St._Stephen's_College.JPG "fig:College_House_of_St._Stephen's_College.JPG")
      - 建於2011年，於2011年11月啟用。大樓樓高四層，左右兩邊分別為男生宿舍和女生宿舍，連接處為升降機大堂及職員宿舍。

校園內每個房間均有代號以方便學生及職員。

### 康體設施

  - 標準[足球場](../Page/足球場.md "wikilink")1個／小型足球場2個
  - 籃球場3個
  - 綜合網球／排球場1個
  - 網球場3個
  - 室外六線五十米游泳池1個（與小學部共用）
  - 室內體育館（包括室內籃球場1個／室內排球場1個／室內羽毛球場3個）
  - 三百五十米六線跑步徑
  - 射箭場1個
  - 鉛球場1個
  - 綜合鐵餅／鏈球場1個
  - 標槍場1個
  - 健身室1間
  - 越野跑（Cross-Country）賽道1條
  - 燒烤場

### 文物徑

**聖士提反書院文物徑**（St. Stephen's College Heritage Trail）

  - 校方於2008年12月18日設立文物徑，是香港首個在學校校園內的[文物徑](http://www.ssc.edu.hk/ssctrail/chi/index.html)。
  - 校方於2008年同時創立文物保育及推廣學會（Cultural Heritage and Promotion
    Club），訓練導賞員帶領公眾遊覽文物徑，學會已於2013年更名為文物學會（Heritage
    Society）。
  - 文物徑於2011年末推出[Android應用程式](http://play.google.com/store/apps/details?id=edu.ssc.heritage.trail)。

## 社聯

聖士提反書院為了令學生在比賽中培養出學生的群體意識和積極進取的精神，把學生共分為六社，每位學生於入學時都被隨機編入六社其中一社。部分社名的來源乃為紀念歷任校長而設定：

  - 班納社（Barnett）（<span style="color: orange;">●</span> 橙）為 The Ven. E.
    J. Barnett；
  - 學堂社（College）（<span style="color: yellow;">●</span> 黃）；
  - 侯活社（Hewitt）（<span style="color: red;">●</span> 紅）為 The Revd. W. H.
    Hewitt；
  - 馬田社（Martin）（<span style="color: blue;">●</span> 藍）為 Canon E. W. L.
    Martin；
  - 費至理社（Priestley）（<span style="color: green;">●</span> 綠）為 Mr. C. T.
    Priestley；
  - 史超域社（Stewart）（<span style="color: purple;">●</span> 紫）為 The Revd. A.
    D. Stewart。

註：馬田社從前是使用藍色，80年代因運動服脫色而轉為白色，並已於2013年度轉回使用藍色。

### 社際比賽

曾舉辦的社際比賽包括：

  - 游泳比賽
  - 越野跑比賽
  - 陸運會
  - 閱讀比賽
  - 球類運動比賽
  - 常識問答比賽

## 課程

書院作為香港的一所本地中學，過往一直跟隨香港政府政策沿用英制的[3223學制](../Page/三二二三學制.md "wikilink")，提供三年初中、兩年高中、兩年預科合共七年的中學教育。
隨着香港政府推行教育改革，學校在2009-2010學年在中四級開始逐步落實[334學制](../Page/三三四學制.md "wikilink")，轉為提供三年初中及三年高中，合共六年的中學教育。
與此同時，[國際文憑大學預科課程亦於](../Page/國際文憑大學預科課程.md "wikilink")2014-2015學年開始逐步實施。校方期望此課程能為同學提供本地課程外的另一選擇，提供一年銜接課程及兩年的預科課程。

### 初中課程

中一至中三的中文、英文科課堂會每兩班分成三小組上課，所有班別的電腦科和電腦及資訊科技科課堂均會每班分為兩小組上課，實行小班教學。
中一、中二的設計與科技科及家政科課程只有一個學期的長度，每班分成兩小組個上一科，下學期便會對調。
中一至中三的普通話科則視乎老師人手情況而由校方於開學時決定有否需要獨立設堂教授，如有需要則從中文科抽取時間編配，如無需要則於中文科課堂上教授。
因應[直接資助計劃的實施](../Page/直接資助計劃.md "wikilink")，學校在初中將每級學生分成六班上課。

  - 中一：1C、1D、1H、1J、1L、1Y
  - 中二：2C、2D、2H、2J、2L、2Y
  - 中三：3C、3D、3H、3J、3L、3Y

六個英文字母分別代表六種素質，分別為Courage（勇氣）、Diligence（勤勉）、Honesty（誠實）、Justice（正義）、Loyalty（忠誠）及Youthfulness（青春）。
以下為2015至2016學年各班級資料：

| 中一、中二                                    |
| ---------------------------------------- |
| 考試科目                                     |
| [中國語文](../Page/中國語文.md "wikilink")       |
| [英國語文及文學](../Page/英國語文及文學.md "wikilink") |
| [數學](../Page/數學.md "wikilink")           |
| [綜合科學](../Page/科學.md "wikilink")         |
| [綜合人文](../Page/綜合人文.md "wikilink")       |
| [中國歷史](../Page/中國歷史.md "wikilink")       |
| 非考試科目                                    |
| [普通話](../Page/普通話.md "wikilink")         |
| [宗教（基督教）](../Page/宗教.md "wikilink")      |
| [普通電腦](../Page/電腦.md "wikilink")         |
| [設計與科技](../Page/設計與科技.md "wikilink")     |
| [視覺藝術](../Page/視覺藝術.md "wikilink")       |
| [音樂](../Page/音樂.md "wikilink")           |
| [體育](../Page/體育.md "wikilink")           |

| 中三                                       |
| ---------------------------------------- |
| 考試科目                                     |
| [中國語文](../Page/中國語文.md "wikilink")       |
| [英國語文及文學](../Page/英國語文及文學.md "wikilink") |
| [數學](../Page/數學.md "wikilink")           |
| [生物](../Page/生物.md "wikilink")           |
| [化學](../Page/化學.md "wikilink")           |
| [物理](../Page/物理.md "wikilink")           |
| [地理](../Page/地理.md "wikilink")           |
| [中國歷史](../Page/中國歷史.md "wikilink")       |
| [通識教育](../Page/通識教育.md "wikilink")       |
| [歷史](../Page/歷史.md "wikilink")           |
| 非考試科目                                    |
| [電腦及資訊科技](../Page/電腦及資訊科技.md "wikilink") |
| [普通話](../Page/普通話.md "wikilink")         |
| [宗教（基督教）](../Page/宗教.md "wikilink")      |
| [視覺藝術](../Page/視覺藝術.md "wikilink")       |
| [音樂](../Page/音樂.md "wikilink")           |
| [體育](../Page/體育.md "wikilink")           |

### 334學制新高中課程

[334學制新高中課程根據中三學業成績輔助選課組合分為四班](../Page/三三四學制.md "wikilink")，高中人數較多的班別會在語文科課堂分成兩小組上課；數學科和通識教育科則會以每班分為兩組、每兩班分為三組或每三班分為五組的模式上課。英文字母含義與初中相同。以下為各班級資料：

<table>
<thead>
<tr class="header">
<th></th>
<th><p>C</p></th>
<th><p>D</p></th>
<th><p>H</p></th>
<th><p>J</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>必修科目</p></td>
<td><p><a href="../Page/中國語文.md" title="wikilink">中國語文</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英國語文.md" title="wikilink">英國語文</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/數學.md" title="wikilink">數學</a> (必修部分)</p></td>
<td><p><a href="../Page/數學.md" title="wikilink">數學</a>（必修部分，另可選修延伸部分M1或M2）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/通識教育.md" title="wikilink">通識教育</a>（英文或中文作答）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>選修科目一</p></td>
<td><p><a href="../Page/化學.md" title="wikilink">化學</a> /</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中國歷史.md" title="wikilink">中國歷史</a>／<a href="../Page/物理.md" title="wikilink">物理</a>／<a href="../Page/經濟.md" title="wikilink">經濟</a></p></td>
<td><p><a href="../Page/生物.md" title="wikilink">生物</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>選修科目二</p></td>
<td><p><a href="../Page/物理.md" title="wikilink">物理</a>/ <a href="../Page/企業、會計與財務概論.md" title="wikilink">企業、會計與財務概論</a> / <a href="../Page/歷史.md" title="wikilink">歷史</a></p></td>
<td><p><a href="../Page/化學.md" title="wikilink">化學</a></p></td>
<td><p><a href="../Page/化學.md" title="wikilink">化學</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>選修科目三</p></td>
<td><p><a href="../Page/生物.md" title="wikilink">生物</a>／</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/經濟.md" title="wikilink">經濟</a>／<a href="../Page/資訊及通訊科技.md" title="wikilink">資訊及通訊科技</a>／<a href="../Page/視覺藝術.md" title="wikilink">視覺藝術</a> / <a href="../Page/地理.md" title="wikilink">地理</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>非考試科目</p></td>
<td><p>宗教（基督教）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>音樂</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>體育</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 國際文憑大學預科課程

[國際文憑大學預科課程](../Page/國際文憑大學預科課程.md "wikilink")（International
Baccalaureate Diploma
Programme，IBDP）為期兩年，相等於新高中中五及中六兩級，課程受[國際文憑組織而非](../Page/國際文憑組織.md "wikilink")[香港政府](../Page/香港.md "wikilink")[教育局管理](../Page/教育局.md "wikilink")。校方期望此課程能為同學提供本地課程外的另一選擇。因應[國際文憑大學預科課程只是為期兩年](../Page/國際文憑大學預科課程.md "wikilink")，校方對修讀課程的學生提供一個相等於中四級的一年制[國際文憑大學預科課程銜接課程](../Page/國際文憑大學預科課程.md "wikilink")，以協助同學適應[國際文憑大學預科課程以論文及討論為主的教學方式](../Page/國際文憑大學預科課程.md "wikilink")。

## 刊物

聖士提反書院出版了多種不同的刊物，包括：

  - 校刊
      - 《鐘聲》（Chimes）（每五年兩刊）（1909年\[38\] 至今）
  - 家長教師會（Parent-Teacher Association）
      - 《會訊》（一年三刊）（1996年\[39\] 至今）
  - 學生報（Students' Magazine）
      - 《子衿》（Euditio）（2016年\[40\] 至今）
  - 學生會
      - 《采茞》\[41\]（年刊）
      - 《提聞》（2010至2011年度學生會 (Alpha) 季刊）（2010年 至 2011年）
      - 《紙兩張》（2011至2012年度學生會 (SOAR) 季刊）（2011年 至 2012年）
      - 《回聲》（2012至2013年度學生會 (ECHO) 半年刊）（2012年 至 2013年）
  - 中文學會
      - 《思藻》（季刊）（2010年 至今)
  - [英文學會](http://sites.google.com/a/ssces.org/www)（English Society）
      - 《St. Stephen's College Times》（半年刊）（2008年 至今）
  - 數學學會（Mathematics Society）
      - 《Mathematics Challengers Paper》（2010年 至 2011年）
  - 音樂學會（Music Society）
      - 《妙愫》（MUSO Monthly）（2012年）

## 小學部

聖士提反書院附屬小學（St. Stephen's College Preparatory
School）乃香港聖士提反書院的屬下小學。聖士提反書院附屬小學於1938年建校，初時只有一座包含課室、宿舍及飯堂的建築物。二次大戰時期，聖士提反書院小學部及中學部雙雙被日軍佔據作戰俘營。學校於1947年重新運作，並增建一座新校舍。1950年代學校再加建一座男生宿舍，並於1960年代起開始招收女宿生。2013
- 2014年度有18班共576人。

## 公開考試佳績

2014年DSE文憑試放榜中，該校出產一位狀元。

## 香港傑出學生選舉

直至2018年（第33屆），在[香港傑出學生選舉共出產](../Page/香港傑出學生選舉.md "wikilink")1名傑出學生。

## 爭議

2016年，與之毗鄰的附屬小學因推出面額達200萬的定期票據計劃，讓購買人可提名一名學童優先入讀該校小一，該學童將於中六畢業時獲退回面額，但不保證必獲取錄，一度成為社會熱話，不少家長憂學校成為「貴族學校」。\[42\]

2017年3月28日，校內多間更衣室及課室被發現裝設了偷拍鏡頭，校方於同日報警求助，並透過內聯網電郵向家長強調「絕不姑息容忍偷拍行為」。警方調查後拘捕7名15至16歲男學生，案件交西區重案組接手追查。\[43\]

## 電視節目、電影取景

聖士提反書院環境優美，是多個廣告、電影、電視節目的拍攝場地。

### 電視節目

  - 《[荳芽夢](../Page/荳芽夢.md "wikilink")》（[無綫電視劇集](../Page/無綫電視.md "wikilink")，1981年）
  - 《[好時光](../Page/好時光.md "wikilink")》（[香港電台兒童劇集](../Page/香港電台.md "wikilink")，1981-1982年首播）
  - 《[晴天雨天孩子天](../Page/晴天雨天孩子天.md "wikilink")》（[香港電台兒童劇集](../Page/香港電台.md "wikilink")）
  - 《蝴蝶飛》（[香港電台](../Page/香港電台.md "wikilink")）
  - 《[屋簷下](../Page/屋簷下.md "wikilink")》1982
    《[當時年少](../Page/當時年少.md "wikilink")》（[香港電台劇集](../Page/香港電台.md "wikilink")，1982年首播）
  - 《[青出於藍](../Page/青出於藍_\(電視劇\).md "wikilink")》（[無綫電視劇集](../Page/無綫電視.md "wikilink")，2004年9月13日至2004年10月22日首播）
  - 《[香港築跡](../Page/香港築跡.md "wikilink")》第13集（[無綫電視節目](../Page/無綫電視.md "wikilink")，2010年10月3日首播）
  - 《[保育好風光](../Page/保育好風光.md "wikilink")》（[路訊通節目](../Page/路訊通.md "wikilink")，2012年首播）
  - 《[DiY2K](../Page/DiY2K.md "wikilink")》（[香港電台劇集](../Page/香港電台.md "wikilink")，2012年9月25日至2012年12月18日首播）
  - 《[都市閒情](../Page/都市閒情.md "wikilink")》《運動通識站》環節（[無綫電視節目](../Page/無綫電視.md "wikilink")，2013年2月28日首播）
  - 《[香港故事](../Page/香港故事.md "wikilink")》第24輯《[百年樹人](../Page/香港故事.md "wikilink")》第4集（[香港電台節目](../Page/香港電台.md "wikilink")，2013年12月23日首播）
  - 《[我阿媽係黑玫瑰](../Page/我阿媽係黑玫瑰.md "wikilink")》（[香港電視劇集](../Page/香港電視.md "wikilink")，2015年2月9日至2015年2月19日首播）
  - 《[末日+5](../Page/末日+5.md "wikilink")》（[香港電視劇集](../Page/香港電視.md "wikilink")，2015年6月15日至2015年6月19日首播）
  - 《[香港歷史系列III](../Page/香港歷史系列III.md "wikilink")》（[香港電台節目](../Page/香港電台.md "wikilink")，2015年7月首播）
  - 《[同2047特首上學去](../Page/同2047特首上學去.md "wikilink")》（[ViuTV節目](../Page/ViuTV.md "wikilink")，2016年5月21日至6月19日首播）

### 電影

  - 《[失業生](../Page/失業生.md "wikilink")》（1982年）
  - 《[仙樂飄飄](../Page/仙樂飄飄.md "wikilink")》（1995年8月16日上映）
  - 《[天作之盒](../Page/天作之盒.md "wikilink")》（2004年4月8日上映）
  - 《[地獄第19層](../Page/地獄第19層.md "wikilink")》（2007年9月6日上映）
  - 《[歲月神偷](../Page/歲月神偷.md "wikilink")》（2010年3月11日上映）
  - 《[雛妓](../Page/雛妓_\(2015年電影\).md "wikilink")》（2015年3月5日上映）
  - 《[追龍](../Page/追龍_\(電影\).md "wikilink")》（2017年9月28日上映）
  - 《[空手道](../Page/空手道_\(電影\).md "wikilink")》（2017年12月2日上映）
  - 《[棟篤特工](../Page/棟篤特工.md "wikilink")》（2018年2月15日上映）
  - 《[某日某月](../Page/某日某月.md "wikilink")》（2018年5月31日上映）

## 著名校友

### 政界

  - [董建成](../Page/董建成.md "wikilink") -
    聖士提反書院校董會副主席、[東方海外(國際)有限公司主席兼行政總裁](../Page/東方海外.md "wikilink")、前[國泰航空獨立](../Page/國泰航空.md "wikilink")[非執行董事](../Page/非執行董事.md "wikilink")、前[行政長官](../Page/行政長官.md "wikilink")[董建華之弟](../Page/董建華.md "wikilink")
  - [霍震霆](../Page/霍震霆.md "wikilink") -
    [國際奧委會委員](../Page/國際奧委會.md "wikilink")、[港協暨奧委會會長](../Page/中國香港體育協會暨奧林匹克委員會.md "wikilink")、前[立法會議員（體育、演藝、文化及出版界）](../Page/第四屆香港立法會.md "wikilink")、已故[全國政協副主席](../Page/全國政協副主席.md "wikilink")[霍英東長子](../Page/霍英東.md "wikilink")
  - [傅秉常](../Page/傅秉常.md "wikilink") -
    1919年[巴黎和會中國代表團秘書](../Page/巴黎和會.md "wikilink")、曾任孫中山秘書、國民政府首屆立法委員暨外交委員會委員、民法修訂委員會召集人、前[中華民國駐](../Page/中華民國.md "wikilink")[蘇聯大使](../Page/蘇聯.md "wikilink")、[司法院副院長等](../Page/司法院.md "wikilink")。
  - [陳偉業](../Page/陳偉業.md "wikilink") -
    前[立法會議員（新界西）](../Page/第五屆香港立法會.md "wikilink")、[人民力量黨團召集人](../Page/人民力量.md "wikilink")
  - [袁彌明](../Page/袁彌明.md "wikilink") -
    [人民力量前主席](../Page/人民力量.md "wikilink")、曾參選[2012年香港立法會選舉並成功令名單首位的](../Page/2012年香港立法會選舉.md "wikilink")[陳志全（慢必）當選](../Page/陳志全.md "wikilink")
  - [吳文遠](../Page/吳文遠.md "wikilink") -
    [社會民主連線主席](../Page/社會民主連線.md "wikilink")、香港[政治人物](../Page/政治人物.md "wikilink")、[社會活動家](../Page/社會運動.md "wikilink")
  - [王澤森博士](../Page/王澤森.md "wikilink") -
    已故立法局議員、香港慈善家、[新法書院創校人](../Page/新法書院.md "wikilink")
  - [李紹鴻醫生](../Page/李紹鴻.md "wikilink")<span style="font-size:smaller;">，[ISO](../Page/帝國服務勳章.md "wikilink")</span>
    -
    公共衛生、傳染病和醫療行政專家，1988年11月至1989年3月署任香港政府醫務衛生署署長，1989年4月至1994年6月任首位衛生署署長，前[香港中文大學賽馬會公共衛生及基層醫療學院院長](../Page/香港中文大學.md "wikilink")。
  - [梁耀祖律師](../Page/梁耀祖.md "wikilink") -
    已故[中西區](../Page/中西區_\(香港\).md "wikilink")[區議員](../Page/區議員.md "wikilink")，曾贏得「社區律師」的美譽
  - [鄭達鴻](../Page/鄭達鴻_\(議員\).md "wikilink") -
    [公民黨青年公民主席](../Page/公民黨_\(香港\).md "wikilink")、港島支部副主席
  - [陳裕財](../Page/陳裕財.md "wikilink")（Bhichai Rattakul） -
    [泰國前副總理](../Page/泰國.md "wikilink")、曾擔任[國際扶輪](../Page/國際扶輪.md "wikilink")2002-03年度社長
  - [梁敬國](../Page/梁敬國.md "wikilink") -
    前[商務及經濟發展局副局長](../Page/商務及經濟發展局.md "wikilink")（1983年中七）
  - [胡穗珊](../Page/胡穗珊.md "wikilink") -
    前[工黨主席](../Page/工黨_\(香港\).md "wikilink")、[新婦女協進會計劃統籌](../Page/新婦女協進會.md "wikilink")、前[民間監察世貿聯盟幹事](../Page/民間監察世貿聯盟.md "wikilink")、[職工盟飲食及酒店業職工總會組織幹事](../Page/職工盟.md "wikilink")
  - [晏陽初](../Page/晏陽初.md "wikilink")- 近代著名平民教育家

### 商界

  - [錢果豐博士](../Page/錢果豐.md "wikilink") -
    聖士提反書院校董會主席、[港鐵前主席](../Page/港鐵.md "wikilink")、[恆生銀行](../Page/恆生銀行.md "wikilink")[非執行董事兼董事長](../Page/非執行董事.md "wikilink")、[滙豐銀行董事](../Page/滙豐銀行.md "wikilink")、[九龍倉集團董事](../Page/九龍倉集團.md "wikilink")
  - [鄧肇堅爵士](../Page/鄧肇堅.md "wikilink")，<span style="font-size:smaller;">[KStJ](../Page/聖約翰美德勳章.md "wikilink")</span>
    -
    已故香港富商、慈善家、[九巴創辦人](../Page/九龍巴士（1933）有限公司.md "wikilink")、前董事局主席及前總監督，[東華醫院前主席](../Page/東華醫院.md "wikilink")、[保良局前主席](../Page/保良局.md "wikilink")
  - [鍾普洋](../Page/鍾普洋.md "wikilink") -
    [香港敦豪國際快運](../Page/香港敦豪國際快運.md "wikilink")
    （[DHL](../Page/DHL.md "wikilink")）公司董事局主席及創辦人
  - [吳光正](../Page/吳光正.md "wikilink") -
    [九龍倉集團前主席](../Page/九龍倉集團.md "wikilink")、[香港貿易發展局前主席](../Page/香港貿易發展局.md "wikilink")、[醫院管理局前主席](../Page/醫院管理局.md "wikilink")
  - [鄒文懷](../Page/鄒文懷.md "wikilink") -
    [嘉禾電影公司創辦人](../Page/橙天嘉禾.md "wikilink")、主席兼執行董事，香港電影大亨
  - [伍經衡](../Page/伍經衡.md "wikilink")（Richard Eng） -
    大型補習社[遵理學校創辦人](../Page/遵理學校.md "wikilink")、大股東及名師，藝人[伍詠薇之兄](../Page/伍詠薇.md "wikilink")
  - [周錫年爵士](../Page/周錫年.md "wikilink") -
    香港富商、華人領袖、首位[香港華人](../Page/香港華人.md "wikilink")[耳鼻喉科](../Page/耳鼻喉科學.md "wikilink")[醫生](../Page/醫生.md "wikilink")，曾任[華人銀行](../Page/華人銀行.md "wikilink")、[九龍巴士](../Page/九龍巴士.md "wikilink")、[牛奶公司董事長](../Page/牛奶公司.md "wikilink")，[香港工業總會前主席](../Page/香港工業總會.md "wikilink")、[香港貿易發展局的首任主席](../Page/香港貿易發展局.md "wikilink")，另一校友[周埈年之堂弟](../Page/周埈年.md "wikilink")
  - [周埈年爵士](../Page/周埈年.md "wikilink") -
    香港富商、[廣東信託銀行董事長](../Page/廣東信託銀行.md "wikilink")，曾任[潔淨局局紳及](../Page/潔淨局.md "wikilink")[立法局非官守議員](../Page/立法局.md "wikilink")，先後獲港府委任為立法局及[行政局的](../Page/行政局.md "wikilink")[首席非官守議員](../Page/首席非官守議員.md "wikilink")，另一校友[周錫年之堂兄](../Page/周錫年.md "wikilink")
  - [彭文堅](../Page/彭文堅.md "wikilink")：前無綫電視及亞洲電視藝員、[中國融保金融集團創辦人](../Page/中國融保金融集團.md "wikilink")、主席及執行董事

### 專業界

  - [葉問](../Page/葉問.md "wikilink") - 1917年18歲入學、詠春拳一代宗師
  - [莫樹錦醫生](../Page/莫樹錦.md "wikilink") -
    [香港中文大學醫學院臨床腫瘤學系教授](../Page/香港中文大學.md "wikilink")、中大癌症病人針灸中心主管、[廣東省人民醫院名譽教授](../Page/廣東省人民醫院.md "wikilink")
  - [梁貫成教授](../Page/梁貫成.md "wikilink") -
    聖士提反書院校監、[香港大學](../Page/香港大學.md "wikilink")[教育學院教授](../Page/教育學院.md "wikilink")、全球數學教育界最高榮譽的2013年[費萊登特爾獎得主](../Page/費萊登特爾獎.md "wikilink")，亦是首位亞洲學者獲頒此項國際殊榮
  - [區慶祥法官](../Page/區慶祥.md "wikilink") -
    [高等法院](../Page/香港高等法院.md "wikilink")[原訟法庭法官](../Page/香港高等法院原訟法庭.md "wikilink")
  - [彭耀鴻](../Page/彭耀鴻.md "wikilink") -
    [資深大律師](../Page/資深大律師.md "wikilink")
  - [趙家寶](../Page/趙家寶.md "wikilink") -
    2014年[世界咖啡師大賽](../Page/世界咖啡師大賽.md "wikilink")（[World
    Barista
    Championship](../Page/World_Barista_Championship.md "wikilink")）亞軍
  - [區永谷](../Page/區永谷.md "wikilink") - 醫生 - 心臟移植外科醫生 (瑪麗醫院心胸外科).
  - [陳沐文](../Page/陳沐文.md "wikilink") -
    [香港建築師學會會長](../Page/香港建築師學會.md "wikilink")

### 演藝界

  - [袁彌明](../Page/袁彌明.md "wikilink") -
    2005年[香港小姐競選](../Page/香港小姐.md "wikilink")「旅遊大使獎」得主並位居決賽頭五名

  - [森美](../Page/森美.md "wikilink") -
    [香港商業電台](../Page/香港商業電台.md "wikilink")[DJ](../Page/DJ.md "wikilink")、著名[司儀及](../Page/司儀.md "wikilink")[電視節目主持](../Page/電視節目主持.md "wikilink")

  - [莫樹錦醫生](../Page/莫樹錦.md "wikilink") - 電視節目主持、雜誌專欄作者

  - [王愛倫](../Page/王愛倫.md "wikilink") -
    1985年[香港小姐季軍](../Page/香港小姐.md "wikilink")

  - [謝立文](../Page/謝立文.md "wikilink") -
    《[麥兜](../Page/麥兜.md "wikilink")》系列作者，亦是另一作者[麥家碧的丈夫](../Page/麥家碧.md "wikilink")；故事中的校長、黎根等角色均按照[謝立文就讀時的校長](../Page/謝立文.md "wikilink")[葉敬平的樣子繪畫](../Page/聖士提反書院#.E6.AD.B7.E4.BB.BB.E6.A0.A1.E9.95.B7.E5.8F.8A.E8.88.8D.E7.9B.A3.md "wikilink")

  - [何崇志博士](../Page/何崇志.md "wikilink") -
    香港[作曲家](../Page/作曲家.md "wikilink")、《[麥兜](../Page/麥兜.md "wikilink")》系列歌曲作詞家及[編劇](../Page/編劇.md "wikilink")

  - [汪溟曦](../Page/汪溟曦.md "wikilink") -
    [香港電台記者](../Page/香港電台.md "wikilink")，[無綫電視前記者](../Page/無綫電視.md "wikilink")

  - [唐素琪](../Page/唐素琪.md "wikilink") -
    在任職[模特兒期間參與](../Page/模特兒.md "wikilink")[無綫電視節目](../Page/無綫電視.md "wikilink")《[美女廚房](../Page/美女廚房.md "wikilink")》及《[學廚出餐](../Page/學廚出餐.md "wikilink")》，獲著名音樂人[雷頌德賞識加入樂壇](../Page/雷頌德.md "wikilink")

  - \- 演員，作品包括《[安信兄弟](../Page/安信信貸.md "wikilink")》廣告（1998年畢業）

  - [葉宇澄](../Page/葉宇澄.md "wikilink") -
    歌手，於EMI卡拉OK大賽勝出而入行。現時於[商業電台工作](../Page/商業電台.md "wikilink")，主持雷霆881節目《書．情．歌》。

  - [周智立](../Page/周智立.md "wikilink") - 功夫新星

  - [鍾健強](../Page/鍾健強.md "wikilink") -
    全港第一本夜遊指南《豪情夜生活》的創刊人，事蹟於2003年被拍成電影《[豪情](../Page/豪情.md "wikilink")》，由[古天樂飾演其角色](../Page/古天樂.md "wikilink")

  - [張智軒](../Page/張智軒.md "wikilink") - 演員

  - [何艷娟](../Page/何艷娟.md "wikilink") -
    [2014年度香港小姐季軍](../Page/2014年度香港小姐競選.md "wikilink")

  - [何廣沛](../Page/何廣沛.md "wikilink") -
    [無綫電視藝人](../Page/無綫電視.md "wikilink")

  - [黃鳳瓊](../Page/黃鳳瓊.md "wikilink") - 演員

  - [吳耀漢](../Page/吳耀漢.md "wikilink") - 演員

  - [區偉麟](../Page/區偉麟.md "wikilink") - 前無綫電視男藝員

  - [汪琳](../Page/汪琳.md "wikilink") - 前香港無綫電視藝員

  - [王若琪](../Page/王若琪.md "wikilink") - 歌手

  - [關信培](../Page/關信培.md "wikilink") - 香港資深演藝界工作者、導師、主持、舞台劇演員、監製及配音員

### 體育界

  - [溫健儀](../Page/溫健儀.md "wikilink") -
    香港[田徑運動員](../Page/田徑.md "wikilink")，六項香港紀錄保持者，另一校友[畢國偉的妻子](../Page/畢國偉.md "wikilink")
  - [畢國偉](../Page/畢國偉.md "wikilink") -
    香港田徑運動員，前一百米香港紀錄保持者，另一校友女飛人[溫健儀的丈夫](../Page/溫健儀.md "wikilink")
  - [黃耀信](../Page/黃耀信.md "wikilink") -
    退役足球員，曾效力80年代班霸[精工及香港隊](../Page/精工.md "wikilink")
  - [蔣偉洪](../Page/蔣偉洪.md "wikilink") - 香港田徑運動員，前一百米香港紀錄保持者
  - [林鼎智](../Page/林鼎智.md "wikilink") - 香港飛鏢運動員，奪得多項國際飛鏢賽冠軍
  - [陳國培](../Page/陳國培.md "wikilink") -
    香港業餘足球員，現效力聖士提反舊生會足球隊，其座右銘爲：「足球，就是一種態度。」

### 文化及新聞界

  - [周思中](../Page/周思中.md "wikilink") - 文化人
  - [李憬仁](../Page/李憬仁.md "wikilink") - 武俠小說作家
  - [梁寶華](../Page/梁寶華.md "wikilink") -
    [香港教育大學文化與創意藝術學系教授及系主任與博文與社會科學學院副院長](../Page/香港教育大學.md "wikilink")（質素保證及提升）
  - [張正甫](../Page/張正甫_\(香港\).md "wikilink") - 香港資深傳媒人

## 交通

<div class="NavFrame" width="100%">

<div class="NavHead">

[大巴士](../Page/大巴士公司.md "wikilink")

</div>

<div class="NavContent">

|                                       |                                        |   |                                    |           |
| ------------------------------------- | -------------------------------------- | - | ---------------------------------- | --------- |
| [赤柱線](../Page/大巴士公司#路線.md "wikilink") | [中環七號碼頭](../Page/中環七號碼頭.md "wikilink") | ↺ | [赤柱廣場](../Page/赤柱廣場.md "wikilink") | 綠線，每天袛開六班 |

</div>

</div>

## 參考資料

<div class="references-column">

<references/>

</div>

## 外部連結

  -
  - [聖士提反書院文物經 網站](http://www.ssc.edu.hk/ssctrail/chi/index.html)

  - [聖士提反書院家長教師會 網站](http://s07.ssc.edu.hk/pta/Default.aspx)

  - [聖士提反書院舊生會 網站（英文）](http://www.sscaa.org/)

  - [聖士提反書院文物經
    iOS應用程式](http://itunes.apple.com/us/app/ssc-heritage-trail/id562894088)

  - [聖士提反書院文物經
    Android應用程式](http://play.google.com/store/apps/details?id=edu.ssc.heritage.trail)

  - [TVB節目《香港築跡》介紹聖士提反書院文物經](http://programme.tvb.com/lifestyle/hkartchitecture/i_info/109641/1/883)

[Category:香港教育小作品](../Category/香港教育小作品.md "wikilink")
[聖](../Category/香港聖公會中學.md "wikilink")
[Category:赤柱](../Category/赤柱.md "wikilink")
[Category:香港直資學校](../Category/香港直資學校.md "wikilink")
[S](../Category/香港南區中學.md "wikilink")
[Category:香港一級歷史建築](../Category/香港一級歷史建築.md "wikilink")
[Category:1903年創建的教育機構](../Category/1903年創建的教育機構.md "wikilink")
[Category:香港西式建築](../Category/香港西式建築.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")

1.  [St Stephen’s College School Report
    (2013-2014)](http://www.ssc.edu.hk/images/document/SchoolReport2013to14.pdf)

2.
3.
4.
5.  《香港戰地指南(1941)》，高添強 著，三聯書店 出版，1995年7月，第100頁。ISBN 962-04-1278-8

6.
7.  [班納會吏長](http://www.ssc.edu.hk/photoalbum/album223/1P1_VEN_ARCHDEACON_BARNETT)，聖士提反書院網站

8.  [史超活牧師](http://www.ssc.edu.hk/photoalbum/album18/1914_1915_Rev_A_D_Stewart)，聖士提反書院網站

9.  [侯活牧師](http://www.ssc.edu.hk/photoalbum/album18/1915_1928_Rev_W_H_Hewitt_MA_BD_Warden)，聖士提反書院網站

10.
11. [馬田法政牧師](http://www.ssc.edu.hk/photoalbum/album18/1928_1953_Rev_Canon_E_W_L_Martin)，聖士提反書院網站

12. [米希爾牧師](http://www.ssc.edu.hk/photoalbum/album18/1953_1955_The_Rev_F_R_Myhill_Warden)，聖士提反書院網站

13. [費至理](http://www.ssc.edu.hk/photoalbum/album18/1956_1958_Mr_C_T_Priestley)，聖士提反書院網站

14. [麥鱗書](http://www.ssc.edu.hk/photoalbum/album18/1958_1965_Mr_J_R_E_Melluish)，聖士提反書院網站

15. [韓復士牧師](http://www.ssc.edu.hk/photoalbum/album18/1965_1973_Rev_R_B_Handforth)，聖士提反書院網站

16. [葉敬平](http://www.ssc.edu.hk/photoalbum/album18/1974_1999_Mr_Luke_J_P_Yip)，聖士提反書院網站

17. [朱業桐](http://www.ssc.edu.hk/photoalbum/album18/2000_present_Principal)，聖士提反書院網站

18. [羅懿舒博士](http://www.ssc.edu.hk/photoalbum/album18/2005_Speech_Day_044)，聖士提反書院網站

19.
20.
21. [校園地圖](http://www.ssc.edu.hk/images/campus/campus_map.png)，聖士提反書院網站

22. 《[鐘聲](../Page/聖士提反書院#刊物.md "wikilink")》一百周年版，2003年。

23.
24.
25.
26.
27.
28. [SSC secrets
    專頁](http://www.facebook.com/SSCsecret/posts/499100836823984)，Facebook

29.
30.
31.
32.
33.
34.
35.
36.
37. 「學堂宿舍」為College House的非正式中文名稱，名稱只是常見於學校部分文件，而英文名稱則為正式名稱

38.
39. [家長教師會網站](http://s07.ssc.edu.hk/pta/Newsletters.aspx)

40. [《子衿》第一期創刊號](http://www.ssc.edu.hk/images/document/StudentsMagazine/eruditio_issue_1.pdf)

41. 采茞為學生會刊物。「茞」是白茞，是一種美麗的香草；而「采」通「採」，即採集；意思是刊物採集了學校裏美好的回憶。采茞粵音一般被錯讀作「采茞(coi<sup>2</sup>
    zi<sup>2</sup>)」，但正確的讀音實為「采茞(coi<sup>2</sup>
    coi<sup>2</sup>)」。[茞的粵音](http://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/search.php?q=%D3%B2)

42.

43.