**聖白議會**（*White Council*），或稱**智者會議**（*Council of the
Wise*）是作家[J·R·R·托爾金筆下由](../Page/J·R·R·托爾金.md "wikilink")[埃斯塔力與](../Page/巫師_\(中土大陸\).md "wikilink")[精靈組成的組織](../Page/精靈_\(中土大陸\).md "wikilink")，議會在[第三紀元成立](../Page/第三紀元.md "wikilink")，用以對付[多爾哥多妖術師](../Page/多爾哥多.md "wikilink")。

聖白議會[議長是白袍](../Page/議長.md "wikilink")[薩魯曼](../Page/薩魯曼.md "wikilink")，其他成員包括灰袍[甘道夫](../Page/甘道夫.md "wikilink")、褐袍[瑞達加斯特](../Page/瑞達加斯特.md "wikilink")、造船者[瑟丹](../Page/瑟丹.md "wikilink")、[凱蘭崔爾女王](../Page/凱蘭崔爾.md "wikilink")、[半精靈](../Page/半精靈.md "wikilink")[愛隆](../Page/愛隆.md "wikilink")。

原先凱蘭崔爾女王屬意由甘道夫成為議長，但被甘道夫拒絕。後來薩魯曼背叛後，甘道夫成為新一任議長。

## 歷史

[缩略图](https://zh.wikipedia.org/wiki/File:Dol_guldur.jpg "fig:缩略图")的死靈法師。\]\]
根據小說《[魔戒](../Page/魔戒.md "wikilink")》的[附錄](../Page/魔戒附錄.md "wikilink")，聖白議會共招開過四次會議。

第三紀元2850年，成員甘道夫於多爾哥多確認其主人為重生的黑暗魔君[索倫後](../Page/索倫_\(魔戒\).md "wikilink")，他便於次年的會議上敦促其他成員向多爾哥多發動攻擊，然而此一訴求遭議長薩魯曼拒絕，還嘲笑吸食[菸草的甘道夫](../Page/菸草.md "wikilink")。

2941年，即為[孤山遠征的同年](../Page/孤山_\(托爾金小說\).md "wikilink")，甘道夫再次於會議上呼籲攻擊多爾哥多，這次薩魯曼同意了；於是聖白議會成員們向多爾哥多進攻，但早有預謀的索倫藉機逃到了老巢[魔多](../Page/魔多.md "wikilink")。\[1\]

在2953年的最後一次會議上，討論的主題主要聚焦於[統御魔戒上](../Page/統御魔戒.md "wikilink")，薩魯曼堅稱索倫的[至尊魔戒已透過](../Page/至尊魔戒.md "wikilink")[安都因河流入](../Page/安都因河.md "wikilink")[大海](../Page/大海.md "wikilink")。\[2\]

## 改編描述

在魔戒前傳電影《[哈比人：意外旅程](../Page/哈比人：意外旅程.md "wikilink")》裡，聖白議會在[瑞文戴爾招開了一次會議](../Page/瑞文戴爾.md "wikilink")\[3\]，與會者有甘道夫、薩魯曼、凱蘭崔爾和愛隆。在會議上討論的主題包括了甘道夫支持[索林·橡木盾收復](../Page/索林·橡木盾.md "wikilink")[孤山遠征的理由](../Page/孤山_\(托爾金小說\).md "wikilink")：防止[索倫及惡龍](../Page/索倫_\(魔戒\).md "wikilink")[史矛革邪惡勢力的威脅](../Page/史矛革.md "wikilink")，但薩魯曼對此不屑一顧。甘道夫還在會議上出示了來自多爾哥多的[魔窟劍](../Page/魔窟劍.md "wikilink")。

《[哈比人：五軍之戰](../Page/哈比人：五軍之戰.md "wikilink")》裡，甘道夫被囚於多爾哥多，聖白議會其餘成員來到多爾哥多對抗九[戒靈](../Page/戒靈.md "wikilink")，瑞達加斯特則負責救走甘道夫。凱蘭崔爾女王獨自一人用[她的水晶瓶將索倫與九名戒靈驅離](../Page/凱蘭崔爾的水晶瓶.md "wikilink")，本來愛隆欲進一步追擊以徹底消滅索倫，但薩魯曼阻止了他並說「索倫留給我」。這一幕暗示了薩魯曼後來的背叛\[4\]。

## 參見

  - 《[哈比人歷險記](../Page/哈比人歷險記.md "wikilink")》
  - 《[魔戒](../Page/魔戒.md "wikilink")》
  - [愛隆會議](../Page/愛隆會議.md "wikilink")
  - [魔戒遠征隊](../Page/魔戒遠征隊.md "wikilink")

## 參考資料

<references />

[WC](../Category/中土大陸.md "wikilink")
[WC](../Category/虛構組織.md "wikilink")

1.
2.
3.
4.