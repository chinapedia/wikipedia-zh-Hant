[HK_Garden_Road_PeakTramTerminal.jpg](https://zh.wikipedia.org/wiki/File:HK_Garden_Road_PeakTramTerminal.jpg "fig:HK_Garden_Road_PeakTramTerminal.jpg")
**聖約翰大廈**（**St. John's
Building**），位於[香港](../Page/香港.md "wikilink")[港島](../Page/香港島.md "wikilink")[中環](../Page/中環.md "wikilink")[花園道](../Page/花園道.md "wikilink")33號，[香港上海大酒店有限公司屬下出租物業](../Page/香港上海大酒店.md "wikilink")。[山頂纜車](../Page/山頂纜車.md "wikilink")[中環總站位於聖約翰大廈地下](../Page/花園道站.md "wikilink")。它是一座商業大廈，1983年落成並於同年獲得香港建築師學會銀牌奬，聖約翰大廈由關吳黃建築師事務所設計，內有[比利時及](../Page/比利時.md "wikilink")[盧森堡駐港總領事館](../Page/盧森堡.md "wikilink")。西望可見[美國駐香港總領事館](../Page/美國駐香港總領事館.md "wikilink")，在[911事件之後保安森嚴](../Page/911事件.md "wikilink")。

聖約翰大廈門外有一座[噴水池](../Page/噴水池.md "wikilink")。每逢假日或[中秋節等節日](../Page/中秋節.md "wikilink")，等候乘搭山頂纜車的乘客會沿聖約翰大廈外排隊。

## 歷史

  - 1980年代進行重建。
  - 2001年12月15日聖約翰大廈[董建華競選辦公室首日開幕](../Page/董建華.md "wikilink")。
  - 2005年12月17日[反世貿示威者包括韓國農民一百人遊行到聖約翰大廈](../Page/世界貿易組織第六次部長級會議.md "wikilink")，向[歐盟駐港辦事處請願](../Page/歐盟駐港辦事處.md "wikilink")。

## 景點

  - [香港動植物公園](../Page/香港動植物公園.md "wikilink")
  - [香港公園](../Page/香港公園.md "wikilink")
  - [聖約翰座堂](../Page/聖約翰座堂.md "wikilink")
  - [維多利亞港](../Page/維多利亞港.md "wikilink")

## 道路

  - [花園道](../Page/花園道.md "wikilink")
  - [紅棉路](../Page/紅棉路.md "wikilink")
  - [金鐘道](../Page/金鐘道.md "wikilink")
  - [堅尼地道](../Page/堅尼地道.md "wikilink")
  - [麥當奴道](../Page/麥當奴道.md "wikilink")
  - [上亞厘畢道](../Page/上亞厘畢道.md "wikilink")

## 學校

  - [聖保羅男女中學](../Page/聖保羅男女中學.md "wikilink")
  - [聖若瑟書院](../Page/聖若瑟書院.md "wikilink")

## 交通

  - 山頂纜車
  - [巴士](../Page/巴士.md "wikilink")
  - [電車](../Page/電車.md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[金鐘站](../Page/金鐘站.md "wikilink")

## 外部参考

  - [香港上海大酒店：聖約翰大廈](https://web.archive.org/web/20070826220827/http://www.hshgroup.com/officeDetails.asp?rtid=76&cid=50&id=275)
  - [比利時註港領事館地址：香港中環花園道33號聖約翰大廈9字樓](https://web.archive.org/web/20060524061158/http://www.y28travel.com/travel8.htm)

[Category:香港上海大酒店](../Category/香港上海大酒店.md "wikilink")
[Category:香港中環寫字樓](../Category/香港中環寫字樓.md "wikilink")
[Category:中西區寫字樓 (香港)](../Category/中西區寫字樓_\(香港\).md "wikilink")
[Category:金鐘](../Category/金鐘.md "wikilink")