[Saint.john.ambulance.london.arp.jpg](https://zh.wikipedia.org/wiki/File:Saint.john.ambulance.london.arp.jpg "fig:Saint.john.ambulance.london.arp.jpg")的聖約翰[救護車](../Page/救護車.md "wikilink")\]\]
**聖約翰救護機構**（），是一個起源於[英國的法定國際性慈善救援組織](../Page/英國.md "wikilink")，設有

  - **聖約翰救傷會**（）：負責訓練課程及教學。
  - **聖約翰救傷隊**（）：執行[急救](../Page/急救.md "wikilink")、護理及有關服務。

聖約翰救護機構起源自1099年的[耶路撒冷](../Page/耶路撒冷.md "wikilink")，其後於英國多個[殖民地設立分部](../Page/大英帝國.md "wikilink")。聖約翰救護機構的前身是[中世紀中](../Page/中世紀.md "wikilink")[三大騎士團之一](../Page/三大騎士團.md "wikilink")[醫院騎士團](../Page/醫院騎士團.md "wikilink")（現稱[馬爾他騎士團](../Page/馬爾他騎士團.md "wikilink")）。後來因[欧洲](../Page/欧洲.md "wikilink")[宗教改革](../Page/宗教改革.md "wikilink")（）的關係，聖約翰組織從馬爾他騎士團總部獨立出來。\[1\]。

聖約翰救護機構的上級組織为[最受尊崇的耶路撒冷聖約翰](../Page/圣约翰勋章.md "wikilink")（），隸屬于。

## 設有聖約翰救護機構的國家及地區

  -   -
      -
      -
      -
      -
      -
  - — [马来西亚圣约翰救护机构](../Page/马来西亚圣约翰救护机构.md "wikilink")

  -
  -
  -
  - — [香港聖約翰救護機構](../Page/香港聖約翰救護機構.md "wikilink")

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 参考文献

## 外部連結

  - [聖約翰救護機構英國總部](http://www.sja.org.uk/)

## 參見

  - [医院骑士团](../Page/医院骑士团.md "wikilink")
  - [马耳他骑士团](../Page/马耳他骑士团.md "wikilink")
  - [三大骑士团](../Page/三大骑士团.md "wikilink")
  - [十字军东征](../Page/十字军东征.md "wikilink")
  - [宗教改革](../Page/宗教改革.md "wikilink")

[Category:國際醫療與健康組織](../Category/國際醫療與健康組織.md "wikilink")
[Category:医院骑士团](../Category/医院骑士团.md "wikilink")

1.  [St John Ambulance's
    history](http://www.sja.org.uk/sja/about-us/our-history.aspx)