**現代中文譯本**（，[縮寫为](../Page/縮寫.md "wikilink")****），是[聯合聖經公會於](../Page/聯合聖經公會.md "wikilink")1979年出版的《[聖經](../Page/聖經.md "wikilink")》[汉语译本](../Page/聖經汉语譯本.md "wikilink")，又於1997年又參考教牧之意見，出版《現代中文譯本修訂版》。它主要對象是剛接觸《聖經》的讀者。此譯本的翻譯工作開始於1971年，由許牧世教授、駱維仁博士、周聯華博士、王成章博士和焦明女士等人所翻譯。

聯合聖經公會鑑於[華人最常用的](../Page/華人.md "wikilink")《[聖經和合本](../Page/聖經_\(和合本\).md "wikilink")》的一些字句，對於某些讀者（尤其是初信或教外的）而言暗晦難明、生硬拗口，甚至容易造成誤解；這是由於《[和合本](../Page/聖經_\(和合本\).md "wikilink")》的語言屬於[白話文運動的早期](../Page/白話文.md "wikilink")，許多語詞用法已與[現代汉语](../Page/現代汉语.md "wikilink")[白話文的使用習慣有不少差距](../Page/白話文.md "wikilink")。此外，《和合本》是以1885年出版的《英國修訂本》(English
Revised Version) 為翻譯藍本，
並參考《[詹姆士王](../Page/詹姆士一世.md "wikilink")[欽定本](../Page/欽定本.md "wikilink")》（King
James
Version），而不是直接從[希伯來文或](../Page/希伯來文.md "wikilink")[希臘文的原文翻譯](../Page/希臘文.md "wikilink")；如此從英文二次轉譯的過程中，難免出現錯譯或漏譯的情形。再者，當時《和合本》根據的原文抄本，無論從數量或素質而言都不能與現存的抄本與[考古資料相比](../Page/考古.md "wikilink")。因應不少教內有識人士呼籲重譯聖經的聲音，聯合聖經公會遂決定推出符合現代白話文，通順易懂、又具時代特色的新版《聖經》。

## 翻譯參考文本

  - [初稿](../Page/初稿.md "wikilink")：《》 (Today's English Version)
  - [舊約](../Page/舊約.md "wikilink")：《基托爾氏[希伯來文聖經](../Page/希伯來文.md "wikilink")》（Kittel
    Biblia Hebraica）1952 年第三版
  - [新約](../Page/新約.md "wikilink")：《[希臘文](../Page/希臘文.md "wikilink")[新約聖經](../Page/新約.md "wikilink")》（Greek
    New Testament）1968 年第二版、1975 年第三版 - 聯合聖經公會
  - 在舊約翻譯中參考《[七十士譯本](../Page/七十士譯本.md "wikilink")》（Septuagint）

## 特色

[Today's_Chinese_Version_Bible_Cover.jpg](https://zh.wikipedia.org/wiki/File:Today's_Chinese_Version_Bible_Cover.jpg "fig:Today's_Chinese_Version_Bible_Cover.jpg")出版的「現代中文譯本」聖經\]\]

  - 譯文忠實地傳達原文的意思。
  - 現代中文譯本的翻譯原則不同於傳統的「形式對應」，而是「功能對等」（functional equivalence, 或dynamic
    equivalence），即強調兩個內涵：意義相符和效果相等。或說以「意義相符、效果相等」為原則翻譯\[1\]。
  - 顧及「聽」和「讀」的需要。使人一讀就懂，一聽就明。
  - 將《[和合本](../Page/聖經_\(和合本\).md "wikilink")》一些過時的譯名，改為符合[現代人所熟悉通用之現代譯名](../Page/現代人.md "wikilink")。例如「該撒」改為「[凱撒](../Page/凱撒.md "wikilink")」（[羅馬](../Page/羅馬.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")）、「丟斯」改為「[宙斯](../Page/宙斯.md "wikilink")」、「[希利尼人](../Page/希利尼人.md "wikilink")」改為「[希臘人](../Page/希臘人.md "wikilink")」、「伯拉河」改為「[幼發拉底河](../Page/幼發拉底河.md "wikilink")」……等。
  - 將《[和合本](../Page/聖經_\(和合本\).md "wikilink")》一些意思已與現代[白話不同的語彙](../Page/白話.md "wikilink")，改為更符合[現代人習慣的用語](../Page/現代人.md "wikilink")。例如「教訓人」改為「教導人」，「論到」改為「談到」，疑問句末的「麼」改成「嗎」……等。
  - 度量衡使用國際通用的公制。如「寬十肘」改為「寬五公尺」、「申初」改為「下午三點鐘」。
  - 減少使用音譯的詞彙。如「拉加」改為「廢物」、「瑪門」改為「錢財」。

## 影響

現代中文譯本文字較[和合本更符合現代漢語使用習慣](../Page/和合本.md "wikilink")，也參考較新[聖經考古學成果](../Page/聖經考古學.md "wikilink")，以及對照[聯合聖經公會審訂的](../Page/聯合聖經公會.md "wikilink")[古希伯來文](../Page/古希伯來文.md "wikilink")、[希臘文標準版](../Page/希臘文.md "wikilink")[新舊約聖經的原文來修正了](../Page/聖經.md "wikilink")[和合本的一些錯誤翻譯](../Page/和合本.md "wikilink")，不少教友都會拿現代中文譯本作為比對的參考，以幫助理解經文。但一般而言，由於[官話版](../Page/官話.md "wikilink")[和合本](../Page/和合本.md "wikilink")[聖經的歷史悠久](../Page/聖經.md "wikilink")，已在教友間產生了根深柢固的權威性，現代中文譯本在[教會中依舊無法取代](../Page/教會.md "wikilink")[和合本的地位](../Page/和合本.md "wikilink")。

## 爭議

此譯本的說：“[摩西的](../Page/摩西.md "wikilink")[法律因](../Page/摩西律法.md "wikilink")[人性的軟弱而不能成就的](../Page/人性.md "wikilink")，上帝卻親自成就了。上帝差遣自己的兒子，使他有了跟我們人相同的罪性，為要宣判人性裏面的罪，把罪除去。”有人認為此節譯文容易令人誤解它在說[耶穌基督有](../Page/耶穌基督.md "wikilink")[罪性](../Page/原罪.md "wikilink")\[2\]，與一般看似與[基督教正統信仰矛盾](../Page/基督教.md "wikilink")。後來在1997年的修訂版加入註解（「使他有了跟我們人相同的罪性」或譯「取了跟我們相似的罪的肉身」），以解釋原文的意思。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 許牧世：《經與譯經》（香港：基督教文藝出版社，1983）
  - 趙維本：《譯經溯源──現代五大中文聖經翻譯史》（香港：中國神學研究院，1993）

<!-- end list -->

  - 论文

<!-- end list -->

  - 林草原：〈忠信與操縱――當代基督教《聖經》中文譯本研究〉（香港：嶺南大學哲學碩士論文，2003）

<!-- end list -->

  - 网页

<!-- end list -->

  - [中文聖經譯本流源](http://ctestimony.org/0203/zwsj.htm)

## 外部連結

  - [現代中文譯本閱讀與查詢](http://cb.fhl.net/)
  - [現代中文譯本介紹—台灣聖經公會](http://www.biblesociety-tw.org/translation/Todays%20Chinese%20Version%20Revised.html)
  - [《聖經中文譯本編年紀》(聖經．中文．翻譯)](http://www.translatebible.com/chinese-bible-translation-timeline.html)

## 参见

  - [圣经汉语译本](../Page/圣经汉语译本.md "wikilink")

{{-}}

[Category:圣经汉语译本](../Category/圣经汉语译本.md "wikilink")
[Category:1979年建立](../Category/1979年建立.md "wikilink")

1.  [現代中文譯本聖經](http://www.bstwn.org/translation/Todays%20Chinese%20Version%20Revised.html)
2.  [主耶穌有罪性嗎？](http://www.chineseministries.com/books/wrong.htm#w49)