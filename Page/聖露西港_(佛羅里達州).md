**圣露西港** （Port St. Lucie,
Florida）是[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[聖盧西亞縣的一個城市](../Page/聖盧西亞縣.md "wikilink")，位於[佛羅里達半島東部](../Page/佛羅里達半島.md "wikilink")[印第安河畔](../Page/印第安河.md "wikilink")。面積198.6平方公里。2000年人口88,769人，2006年人口143,868人。\[1\]

1961年4月27日建市。

## 参考文献

<div class="references-small">

<references />

</div>

[P](../Category/佛羅里達州城市.md "wikilink")
[Category:1958年佛羅里達州建立](../Category/1958年佛羅里達州建立.md "wikilink")
[Category:1958年建立的聚居地](../Category/1958年建立的聚居地.md "wikilink")

1.  [Port St. Lucie, Florida - Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=Search&geo_id=16000US1245975&_geoContext=01000US%7C04000US12%7C16000US1245975&_street=&_county=Port+St.+Lucie+city&_cityTown=Port+St.+Lucie+city&_state=04000US12&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=160&_submenuId=population_0&ds_name=null&_ci_nbr=null&qr_name=&reg=%3Anull&_keyword=&_industry=)