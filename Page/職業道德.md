**職業道德**，是指被普遍認為是從事職業的人士應該遵守的道德規範。在[職業上](../Page/職業.md "wikilink")，不能用不正當的手法去謀取利益、不接受不應接受的[利益](../Page/利益.md "wikilink")、不能洩漏[工作上的隱私](../Page/工作.md "wikilink")、在面對[僱主或上司不合理或非法的要求時](../Page/僱主.md "wikilink")，必須本著[良知拒絕](../Page/良知.md "wikilink")。

面對欠缺職業道德的個案，應該予以除了「口頭警告」、「內部處分」之外的制裁，例如[立法禁止](../Page/立法.md "wikilink")。

## 例子

  - [社工](../Page/社工.md "wikilink")、[教師](../Page/教師.md "wikilink")、[律師](../Page/律師.md "wikilink")、[會計師](../Page/會計師.md "wikilink")、[醫師](../Page/醫師.md "wikilink")、[護士等人員因業務上可能持有客戶資料](../Page/護士.md "wikilink")，須保守因業務上而得知的他人私隱。
  - [足球運動員在](../Page/足球運動員.md "wikilink")[足球比賽中](../Page/足球比賽.md "wikilink")，不可以玩[假摔](../Page/插水_\(足球術語\).md "wikilink")；棒球球員不可受威脅或利誘而[打假球](../Page/打假球.md "wikilink")。
  - [地產代理](../Page/地產代理.md "wikilink")、[經紀不可以抬高價格](../Page/經紀人.md "wikilink")、[詐騙客人](../Page/詐騙.md "wikilink")。
  - [旅行社](../Page/旅行社.md "wikilink")、[領隊](../Page/領隊.md "wikilink")、[導遊不可以強迫遊客](../Page/導遊.md "wikilink")[定點購物](../Page/定點購物.md "wikilink")。
  - [公務員](../Page/公務員.md "wikilink")、[董事](../Page/董事.md "wikilink")、[職員等不可以](../Page/職員.md "wikilink")[貪污收受利益](../Page/貪污.md "wikilink")。
  - [的士](../Page/的士.md "wikilink")[司機不可以無理拒載](../Page/司機.md "wikilink")、兜路、濫收車資。
  - [政治家要](../Page/政治家.md "wikilink")[申報利益](../Page/申報利益.md "wikilink")，不可接受[黑金](../Page/黑金.md "wikilink")[捐款](../Page/贊助.md "wikilink")。

## 另見

  - [新教倫理](../Page/新教倫理.md "wikilink")

## 外部連結

  - [Miula Business
    Review](https://web.archive.org/web/20070504231357/http://blog.miula.cc/archives/date/2007/04/01/)

  - [香港廉政公署 - 防貪資料](http://www.icac.org.hk/big5/0/1/10/14/12052.html)

  - [André Gorz,Critique of Economic Reason,chapter 3:Crisis of
    Work,Gallilé,1989](http://www.antenna.nl/~waterman/gorz.html)

[Category:職業](../Category/職業.md "wikilink")
[Category:道德](../Category/道德.md "wikilink")
[Category:倫理學](../Category/倫理學.md "wikilink")