**肃北蒙古族自治县**（[蒙古語](../Page/蒙古語.md "wikilink")：）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省](../Page/甘肃省.md "wikilink")[酒泉市下属的一个](../Page/酒泉市.md "wikilink")[自治县](../Page/自治县.md "wikilink")。

面积6.93万平方公里，约占甘肃省总面积的14%，人口1.18万人；是甘肃省人均占有面积最大的县份之一。

肃北蒙古族自治县位于甘肃省[河西走廊西段的南北两侧](../Page/河西走廊.md "wikilink")，周边与1个国家，3个省区，8个县市接壤，是一个以[蒙古族为主体的少数民族自治县](../Page/蒙古族.md "wikilink")，也是甘肃省唯一的边境县。

## 历史

早在春秋、魏晋时期就有先民居住，西晋已设县，之后历朝都在这里设镇置县；民国25年（1937年）设[肃北设治局](../Page/肃北设治局.md "wikilink")，1950年7月22日被[中国共产党占领](../Page/中国共产党.md "wikilink")，7月29日正式建立肃北蒙古族自治县。

## 行政区划

下辖2个[镇](../Page/镇.md "wikilink")、2个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 外部链接

  - [新华网甘肃频道肃北县分站](http://www.gs.xinhuanet.com/dfpd/subeix/index.htm)

## 参考文献

{{-}}

[肃北县](../Category/肃北县.md "wikilink")
[县/自治县](../Category/酒泉区县市.md "wikilink")
[Category:甘肃省民族自治县](../Category/甘肃省民族自治县.md "wikilink")
[甘](../Category/中国蒙古族自治县.md "wikilink")