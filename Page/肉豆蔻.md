**肉豆**（），又名**肉蔻**、**肉果**、**玉果**、**麻醉果**，是一種重要的香料、藥用植物。生長於[熱帶地區的](../Page/熱帶.md "wikilink")[常綠植物](../Page/常綠植物.md "wikilink")，常見於[東南亞](../Page/東南亞.md "wikilink")、[澳洲及加勒比海地区](../Page/澳洲.md "wikilink")，尤以[印度尼西亚和](../Page/印度尼西亚.md "wikilink")[格林纳达产量最大](../Page/格林纳达.md "wikilink")。肉豆蔻[果實是可制两种常見](../Page/果實.md "wikilink")[香料](../Page/香料.md "wikilink")：**豆蔻核仁**（nutmeg）和**肉豆蔻皮**（mace），用於食品、菜餚的調味，其果仁可製作香精油。肉豆蔻含有（myristicin），能够产生兴奋及致幻作用。如服用过量，可产生幻覺甚至昏迷现象。

[Myristica_fragrans_trunk_W_IMG_2464.jpg](https://zh.wikipedia.org/wiki/File:Myristica_fragrans_trunk_W_IMG_2464.jpg "fig:Myristica_fragrans_trunk_W_IMG_2464.jpg")
[Nutmeg_on_Tree.jpg](https://zh.wikipedia.org/wiki/File:Nutmeg_on_Tree.jpg "fig:Nutmeg_on_Tree.jpg")
[muscade.jpg](https://zh.wikipedia.org/wiki/File:muscade.jpg "fig:muscade.jpg")
[Muskatnuss.jpg](https://zh.wikipedia.org/wiki/File:Muskatnuss.jpg "fig:Muskatnuss.jpg")
[NuezMoscada.jpg](https://zh.wikipedia.org/wiki/File:NuezMoscada.jpg "fig:NuezMoscada.jpg")
[Fleur_de_muscade.jpg](https://zh.wikipedia.org/wiki/File:Fleur_de_muscade.jpg "fig:Fleur_de_muscade.jpg")
[Nutmeg.jpg](https://zh.wikipedia.org/wiki/File:Nutmeg.jpg "fig:Nutmeg.jpg")

## 物種

本屬包含約100個物種，包括：

  - [银肉豆蔻](../Page/银肉豆蔻.md "wikilink") *Myristica argentea*
  - [肉荳蔲](../Page/肉荳蔲.md "wikilink") *Myristica fragrans*
  - *Myristica inutilis*
  - [印度肉豆蔻](../Page/印度肉豆蔻.md "wikilink") *Myristica malabarica*
  - *Myristica macrophylla*
  - *Myristica otoba*
  - *Myristica platysperma*

## 形態特徵

小[喬木](../Page/喬木.md "wikilink")，高可達10公尺。枝條纖細，略具毛。[葉柄](../Page/葉.md "wikilink")6-12公厘，葉橢圓形，長4-8公分，近革質，基部圓葉端尖，約6-10對側[脈](../Page/葉脈.md "wikilink")。

雄[花序短](../Page/花序.md "wikilink")，約2.5-5公分，4至8朵[花](../Page/花.md "wikilink")（或更多）構成[圓錐花序](../Page/圓錐花序.md "wikilink")。[花被片](../Page/花被.md "wikilink")3（或4）片，長約5-7公厘。[雄蕊](../Page/雄蕊.md "wikilink")9-12枚，基部合生。雌花序單一朵花或多朵花構成。雌花之花被片長6公厘。[子房橢圓狀](../Page/子房.md "wikilink")，密生鏽毛；[花柱極短](../Page/花柱.md "wikilink")，[柱頭](../Page/柱頭.md "wikilink")2，極小。\[1\]

[果實](../Page/果實.md "wikilink")1-2枚，橘色或黃色，梨形或近圓形，直徑3.5-5公分。[種子橢圓形](../Page/種子.md "wikilink")，2-3
× ca. 2公分。[假種皮紅色](../Page/假種皮.md "wikilink")，不規則深裂。子葉短，彎曲。

## 分布

原產於[印尼](../Page/印尼.md "wikilink")[摩鹿加群島](../Page/摩鹿加群島.md "wikilink")，廣泛栽培於[熱帶地區](../Page/熱帶地區.md "wikilink")。[廣東](../Page/廣東.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[雲南有栽培](../Page/雲南.md "wikilink")。\[2\]

## 历史

肉豆蔻原產[摩鹿加群島](../Page/摩鹿加群島.md "wikilink")，是聞名於世的[香料](../Page/香料.md "wikilink")。幾百年前，中世紀的人們把肉豆蔻視為珍寶。1284年的[英國](../Page/英國.md "wikilink")，0.5千克肉豆蔻花的價值與3頭羊差不多，只有富人才拿得出錢來買這種名貴的香料。肉豆蔻成了地位的象徵。

葡萄牙1513年抵[摩鹿加群島的主島](../Page/摩鹿加群島.md "wikilink")：[馬來群島](../Page/馬來群島.md "wikilink")，建立了自己的要塞和貿易基地。1517年至[澳門](../Page/澳門.md "wikilink")，當時主要的交易項目：產自摩鹿加群島之[班達群島的](../Page/班達群島.md "wikilink")[丁香](../Page/丁香.md "wikilink")、肉荳蔻、荳蔻皮
(mace)。

## 利用

本種是著名的[香料](../Page/香料.md "wikilink")**豆蔻核仁**（nutmeg）和**肉豆蔻皮**來源，並作藥用。[種子含](../Page/種子.md "wikilink")40-73%[脂肪](../Page/脂肪.md "wikilink")，並作工業使用。植物的其他部位亦作藥用，如[痢疾](../Page/痢疾.md "wikilink")、[風濕痛](../Page/風濕痛.md "wikilink")，並可做[驅蟲劑](../Page/驅蟲劑.md "wikilink")。\[3\]

可抗發炎、抗菌、抗病毒和抗寄生蟲劑。對於胃腸和呼吸的疾病有益。治療癌症、克隆氏症(局部性迴腸炎)、感冒和流行性感冒、糖尿病、月經問題和更年期症狀和肌肉疼痛、痙攣有效。

### 食用

肉豆蔻的[乙醇提取物具有抗真菌和抗微生物作用](../Page/乙醇.md "wikilink")，其萜類成分有抗菌作用。
肉豆蔻油對腸胃道有局部刺激作用，少量能促進胃液分泌及腸蠕動，大量則呈抑制作用。
肉豆蔻油為一種強大的單胺氧化酶抑制劑，有顯著的麻醉性能。
肉豆蔻衣中分離所得的肉豆蔻醚和脫氧二異丁香油酚，可通過直接清除自由基和活性氧而抑制肝中脂質過氧化作用。
肉豆蔻醚對人的大腦有中度興奮作用，肉豆蔻對正常人有致幻作用。

### 毒性

從肉豆蔻油中分離得到的活性成分為肉豆蔻醚、欖香素和異欖香素，肉豆蔻醚有致幻作用，攝取過量的肉豆蔻會出現明顯的中毒症狀：時間和空間定向錯誤，超越實際，視聽幻覺和其他幻覺，如浮動、飛行、手足離體等。

## 參考文獻

## 外部連結

  - [肉豆蔻
    Roudoukou](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01351)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [肉豆蔻
    Roudoukou](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00201)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [肉豆蔻 Rou Dou
    Kou](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00516)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

  - [黃樟素
    Safrole](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00344)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [Antifungal Properties of Nutmeg Essential
    oil](https://web.archive.org/web/20050527134308/http://pages.unibas.ch/mdpi/ecsoc-3/d0002/d0002.html)

  - [肉豆蔻之魅
    凡高死因：恶魔之灵苦艾酒？](https://web.archive.org/web/20091007175423/http://news.xinhuanet.com/shuhua/2007-04/16/content_5981052.htm)新华网

[樹](../Category/藥用植物.md "wikilink")
[Category:肉豆蔻属](../Category/肉豆蔻属.md "wikilink")
[Category:肉豆蔻科属](../Category/肉豆蔻科属.md "wikilink")
[Category:印度尼西亚植物](../Category/印度尼西亚植物.md "wikilink")
[Category:香料](../Category/香料.md "wikilink")
[Category:致幻剂](../Category/致幻剂.md "wikilink")
[Category:单胺氧化酶抑制剂](../Category/单胺氧化酶抑制剂.md "wikilink")
[Category:波多黎各食品原料](../Category/波多黎各食品原料.md "wikilink")

1.
2.
3.