**肉身佛**，[佛教用語](../Page/佛教.md "wikilink")，是[佛教高僧大德成就的一種境界](../Page/佛教.md "wikilink")，死亡後肉身仍然可以不腐壞；如中国的禅宗高僧[惠能大师](../Page/惠能.md "wikilink")。其不腐坏的肉身称作**全身舍利**、**不腐肉身**、**肉身菩薩**、**金刚不坏之身**等，若塑成佛像奉祀者，稱為**真身塑像**；[日本將肉身不腐等同於](../Page/日本.md "wikilink")[即身成佛](../Page/即身成佛.md "wikilink")，故也稱為**即身佛**（）。
[The_Mumified_Munk.jpg](https://zh.wikipedia.org/wiki/File:The_Mumified_Munk.jpg "fig:The_Mumified_Munk.jpg")南部[蘇梅島的寺廟](../Page/蘇梅島.md "wikilink")。\]\]

## 概论

肉身佛，是指高僧大德成就者在死亡後，在没有外力的影响下，遗体不会自然地腐化，而是呈现[木乃伊化](../Page/木乃伊.md "wikilink")，或不作任何变化就像人生前的模样，甚至会呈现金光色，被称作[紫磨金身](../Page/紫磨金身.md "wikilink")，此是佛、菩萨、阿罗汉圣人的境界。[惠能是中國佛教歷史上第一個肉身不腐的高僧](../Page/惠能.md "wikilink")，被稱為“肉身菩薩”。

中國佛教中，以[禪宗以及在九華山中較多出現肉身不腐的高僧](../Page/禪宗.md "wikilink")。[禪宗主張](../Page/禪宗.md "wikilink")[即身成佛](../Page/即身成佛.md "wikilink")，肉身不腐可以作為成道的表現；九華山則是有[缸葬傳統](../Page/缸葬.md "wikilink")，僧人[圓寂後](../Page/圓寂.md "wikilink")，葬入大缸中，不腐化者則作為肉身菩薩供奉。

同樣的現象在[道教則稱為](../Page/道教.md "wikilink")**仙蛻**，道教認為靈魂得道後離開身體，[遺體如同](../Page/遺體.md "wikilink")[蟬蛻一般可以保存](../Page/蟬蛻.md "wikilink")。

## 效仿

肉身不腐的境界在[中國等](../Page/中國.md "wikilink")[漢傳佛教地區](../Page/漢傳佛教.md "wikilink")，尤其[日本](../Page/日本.md "wikilink")，引起了一些不健康的效仿。有些宗派的僧人在預感不久將要離世時，會先以打坐的姿勢坐下，戒除吃[米](../Page/米.md "wikilink")、[麥](../Page/麥.md "wikilink")、[豆類等高](../Page/豆類.md "wikilink")[澱粉食品](../Page/澱粉.md "wikilink")，限制自己只可以吃[果仁](../Page/果仁.md "wikilink")、野果超過一千天，然后以樹皮、樹葉为食一千天，目的是慢慢把僧人的脂肪和水份減少，以減慢死後屍身腐爛的速度。同时僧人也服用有毒的油桐树的樹汁制成的茶，进一步催吐，排除体液，并且进一步[防腐](../Page/防腐.md "wikilink")。經過幾年的折磨，僧人會以打坐方式住在地底，透過伸出地上的竹筒呼吸，僧人手握鈴，以鈴聲告訴別人他還活著，並且[念佛誦經](../Page/念佛.md "wikilink")，然後斷水[絕食](../Page/絕食.md "wikilink")，將自己活活餓死，地面上的弟子們聽不到鈴聲，他們把竹筒取走，封好墓穴，三年後把遺體作成[木乃伊](../Page/木乃伊.md "wikilink")，就是「即身成佛」。雖然不少僧人嘗試此法「成佛」，但不少還是失敗，屍身腐壞、要經過防腐儀式再安葬。能夠保存遺體，避免腐壞的「即身成佛」是極少數。

這一方法的存在被許多[唯物論者作為否認所有肉身佛的證據](../Page/唯物論.md "wikilink")。

## 著名肉身菩萨

[Huineng.jpg](https://zh.wikipedia.org/wiki/File:Huineng.jpg "fig:Huineng.jpg")，在[廣東省](../Page/廣東省.md "wikilink")[韶關市](../Page/韶關市.md "wikilink")。\]\]
《[壇經](../Page/壇經.md "wikilink")·大師事略》說，[劉宋](../Page/劉宋.md "wikilink")[三藏法師](../Page/三藏法師.md "wikilink")[求那跋陀羅懸](../Page/求那跋陀羅.md "wikilink")[記](../Page/授记.md "wikilink")[禪宗六祖](../Page/禪宗.md "wikilink")[惠能大師為](../Page/惠能.md "wikilink")“肉身菩薩”。惠能入灭後果然肉身不腐，成爲一道奇观，是謂“中國歷史上首尊肉身菩萨”\[1\]。從此，但凡成就肉身不腐者，多稱爲“肉身菩薩”。惠能肉身的奇跡，引得信徒争先礼拜，传说还有新罗僧人欲夺走惠能肉身。在“[文化大革命](../Page/文化大革命.md "wikilink")”中，其肉身遭到[红卫兵的破坏](../Page/红卫兵.md "wikilink")，据[野史披露](../Page/野史.md "wikilink")，红卫兵砸破了惠能肉身，见到其中內臟骨頭完好不腐\[2\]\[3\]。

[中國佛教史上著名的肉身菩薩](../Page/中國佛教.md "wikilink")，近代以前除了[六祖惠能](../Page/六祖惠能.md "wikilink")，還有[石頭希遷](../Page/石頭希遷.md "wikilink")、九華山[金喬覺](../Page/金喬覺.md "wikilink")、[海玉法師](../Page/释海玉.md "wikilink")、[无瑕和尚](../Page/无瑕和尚.md "wikilink")、[憨山德清禪師等等](../Page/憨山德清.md "wikilink")，近代則有[慈航法師](../Page/慈航法師.md "wikilink")、[月溪法師等等](../Page/月溪法師.md "wikilink")。而[日本](../Page/日本.md "wikilink")[山形縣及](../Page/山形縣.md "wikilink")[新潟縣不少寺廟中都有被供奉的肉身菩薩](../Page/新潟縣.md "wikilink")。[南傳上座部佛教也有不少肉身不腐的高僧](../Page/南傳上座部佛教.md "wikilink")，如缅甸高僧（U
Sunlun
Sayadaw），公认的[阿罗汉成就者](../Page/阿罗汉.md "wikilink")\[4\]，於1952年[入滅而肉身不腐](../Page/入滅.md "wikilink")。另外，在[藏傳佛教也存在肉身不腐的現象](../Page/藏傳佛教.md "wikilink")，如2015年新聞報道稱，蒙古乌兰巴托發現一具200年前的僧人的不腐遺體，但信徒指他可能還活著，只是進入了冥想狀態\[5\]。

2015年3月在匈牙利博物馆展出一尊千年佛像，因佛像内藏有一名高僧的遗骸而受关注。肉身坐佛展出后引发争议，福建省文物部门经初步确认，认为这尊“肉身坐佛”应是福建省大田县吴山乡阳春村1995年被盗的[章公祖师像](../Page/释普照.md "wikilink")。中国大陆国家文物局已经启动追讨程序\[6\]。

## 參見

  - [肉身得道](../Page/肉身得道.md "wikilink")
  - [尸解](../Page/尸解.md "wikilink")
  - [柯象](../Page/柯象.md "wikilink")
  - [聖髑](../Page/聖髑.md "wikilink")
  - [般涅槃](../Page/般涅槃.md "wikilink")

## 參考資料

  - [中國十大肉身菩薩排行榜](http://www.epochtimes.com/b5/7/7/24/n1781454.htm)
  - [肉身菩薩 隱身內埔50年](http://news.ltn.com.tw/news/society/paper/811928)

## 註釋

[Category:佛教術語](../Category/佛教術語.md "wikilink")

1.  [泰国出现“肉身佛”\!高僧圆寂100天尸身不腐](https://fo.ifeng.com/a/20171101/44738843_0.shtml)：“唐朝的惠能大师是中国历史上出现的第一尊肉身菩萨。”
2.  [南华寺六祖慧能真身考.徐恒彬](http://blog.sina.com.cn/s/blog_754bdb10010152y5.html)
3.  [六祖惠能真身文革惨遭开膛
    习仲勋下死令保护-凤凰佛教网](https://fo.ifeng.com/news/detail_2013_12/05/31820985_0.shtml)
4.  （Jack Kornfield）. 《Living Dharma: Teachings and Meditation
    Instructions from Twelve Theravada Masters》. Shambala Publications,
    1996. Chapter 6
5.  [蒙古200岁和尚肉身出土
    有专家认为或还活着](https://fo.ifeng.com/a/20151223/41528625_0.shtml)
6.  [匈牙利展出肉身坐佛突然被撤
    疑为中国文物.人民网](http://world.people.com.cn/n/2015/0323/c157278-26732527.html)