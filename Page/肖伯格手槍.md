**肖伯格手槍**（Laumann
Pistol），在1892年，[奥地利人](../Page/奥地利.md "wikilink")[约瑟·劳曼](../Page/约瑟·劳曼.md "wikilink")（Joseph
Laumann）发明了历史上第一支[半自动手枪](../Page/半自动手枪.md "wikilink")，由制造，採用[後座作用系統](../Page/後座作用.md "wikilink")，發射[7.8
x
19R手枪彈](../Page/7.8_x_19R.md "wikilink")，彈夾供彈。因为他在申請登記[专利](../Page/专利.md "wikilink")，喜欢签上肖伯格兄弟公司的名字，所以又被称为**劳曼手枪**\[1\]，但這種手槍後來未被推广使用，而是後來由[德國人製作的手槍產生較大的影響](../Page/德國.md "wikilink")\[2\]。

## 參考

  - [手枪](../Page/手枪.md "wikilink")
  - [半自動手枪](../Page/半自動手枪.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

[Category:半自動手槍](../Category/半自動手槍.md "wikilink")
[Category:奧匈帝國槍械](../Category/奧匈帝國槍械.md "wikilink")
[Category:奧地利槍械](../Category/奧地利槍械.md "wikilink")

1.  [bookrags.com](http://www.bookrags.com/research/pistol-woi/)
2.  <http://gxgc.ahlib.com/datalib/Weapon/2006/2006_11/weapon.2006-10-24.9771719335>