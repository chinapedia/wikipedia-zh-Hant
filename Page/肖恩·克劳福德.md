**肖恩·克劳福德**（**Shawn
Crawford**，）是一名[美国男子田径运动员](../Page/美国.md "wikilink")，主攻短跑项目。曾获得2004年雅典奥林匹克运动会男子200米项目金牌。在北京奥运，克劳福德原本获得第4名，后来由于原第2名和第3名选手均因犯规而被取消成绩，他递补获得银牌。

## 主要成绩

  - [2001年世界田径锦标赛](../Page/2001年世界田径锦标赛.md "wikilink")：200米银牌
  - [2001年世界室内田径锦标赛](../Page/2001年世界室内田径锦标赛.md "wikilink")：200米金牌
  - [2004年世界室内田径锦标赛](../Page/2004年世界室内田径锦标赛.md "wikilink")：60米银牌
  - [2004年夏季奥林匹克运动会](../Page/2004年夏季奥林匹克运动会.md "wikilink")：200米金牌、4×100米接力银牌

## 參考資料

## 外部連結

  -
[Category:美國短跑運動員](../Category/美國短跑運動員.md "wikilink")
[Category:美國奧林匹克運動會金牌得主](../Category/美國奧林匹克運動會金牌得主.md "wikilink")
[Category:美國奧林匹克運動會銀牌得主](../Category/美國奧林匹克運動會銀牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會田徑運動員](../Category/2004年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會田徑運動員](../Category/2008年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:美國奧運田徑運動員](../Category/美國奧運田徑運動員.md "wikilink")
[Category:非洲裔美國田徑運動員](../Category/非洲裔美國田徑運動員.md "wikilink")
[Category:南卡羅來納州人](../Category/南卡羅來納州人.md "wikilink")
[Category:男子短跑運動員](../Category/男子短跑運動員.md "wikilink")
[Category:世界田徑錦標賽獎牌得主](../Category/世界田徑錦標賽獎牌得主.md "wikilink")
[Category:奥林匹克运动会田径金牌得主](../Category/奥林匹克运动会田径金牌得主.md "wikilink")
[Category:奥林匹克运动会田径银牌得主](../Category/奥林匹克运动会田径银牌得主.md "wikilink")