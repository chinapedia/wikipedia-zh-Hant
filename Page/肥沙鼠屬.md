**肥沙鼠屬**（學名***Psammomys***）是鼠科中的一屬，包含下列物種：

  - [肥沙鼠](../Page/肥沙鼠.md "wikilink") (*Psammomys obesus*)
  - [瘦沙鼠](../Page/瘦沙鼠.md "wikilink") (*Psammomys vexillaris*)

## 參考文獻

  - Musser, G. G. and M. D. Carleton. 2005. Superfamily Muroidea. Pp.
    894-1531 *in* Mammal Species of the World a Taxonomic and Geographic
    Reference. D. E. Wilson and D. M. Reeder eds. Johns Hopkins
    University Press, Baltimore.

[Category:肥沙鼠屬](../Category/肥沙鼠屬.md "wikilink")