[Tualetsapo.jpg](https://zh.wikipedia.org/wiki/File:Tualetsapo.jpg "fig:Tualetsapo.jpg")
[Decorative_Soaps.jpg](https://zh.wikipedia.org/wiki/File:Decorative_Soaps.jpg "fig:Decorative_Soaps.jpg")
-{H|zh-hans:湿;zh-hant:溼}-
**肥皂（英語：Soap）**，又名香皂，是用作個人[清潔用品的](../Page/清潔用品.md "wikilink")[表面活性劑](../Page/表面活性劑.md "wikilink")，通常以[固體塊狀的形式存在](../Page/固體.md "wikilink")。

## 歷史

古代不管是東西方，最早的洗滌成份不外乎都是[碳酸鈉和](../Page/碳酸鈉.md "wikilink")[碳酸鉀](../Page/碳酸鉀.md "wikilink")。前者為天然湖礦產品，後者就是[草木灰的主要洗滌成分](../Page/草木灰.md "wikilink")。

肥皂的發明據傳是[地中海東岸的](../Page/地中海.md "wikilink")[腓尼基人](../Page/腓尼基人.md "wikilink")。有傳說指，公元前7世紀古埃及皇宮中的一名腓尼基廚師可能是发明了肥皂的人\[1\]
：

  -
    该厨师不意将一罐[食用油打翻](../Page/食用油.md "wikilink")，为避免被他人发现，便用灶爐裏的草木灰覆盖，并将这些浸透有油脂的草木灰捧到室外扔掉。为了洗净髒手，他将手放到了水中。发现只用轻搓幾下，便将手上的油腻洗掉了，甚至还洗掉了以前的老[污垢](../Page/污垢.md "wikilink")。厨师于是让其他的厨师也用这种物质洗手，发现的确将手洗得更干净。到了后来，[法老王也知道了這個秘密](../Page/法老王.md "wikilink")，也讓廚師做些拌了油的草木灰供他洗手用。

然而據至目前為此的考古發現，類似肥皂的物品可以追溯至西元前29世紀的[古巴比倫](../Page/古巴比倫.md "wikilink")\[2\]。而西元前23世紀時，在[巴比倫的](../Page/巴比倫.md "wikilink")[泥板上已有記載由水](../Page/泥板.md "wikilink")，鹼和肉桂油組成的肥皂配方。[艾德溫·史密斯紙草文稿表明](../Page/艾德溫·史密斯紙草文稿.md "wikilink")[古埃及人在西元前](../Page/古埃及人.md "wikilink")15世紀，結合動物、植物油和鹼性鹽打造皂類物質定期洗澡。
雖然肥皂很早就發明，但塊狀肥皂卻是奢侈品，直到十九世紀才廣泛由一般大眾使用\[3\]。

### 中國

在還沒有發明肥皂以前，中國北方的人都用[豬的](../Page/豬.md "wikilink")[胰臟](../Page/胰臟.md "wikilink")、[板油以及](../Page/豬油.md "wikilink")[碱](../Page/碱.md "wikilink")，搗一搗，放著晒乾幾天就可以拿來洗東西了，稱作“**胰子**”。\[4\]也有人們用清水來浸草木灰，過濾以後，用這種物質來洗衣服。之外還使用含有[皂苷的植物提取物](../Page/皂苷.md "wikilink")，如[皂角等](../Page/皂角.md "wikilink")。另外古時候的人們用水和木屑來擦洗皮膚，然後再抹油滋潤受摩擦的皮膚，這是他們用來洗澡的成分。

### 日本

日本肥皂寫作「石鹸」(せっけん)，也作「石鹼」。

### 中东地区

在12世纪的伊斯兰教文件描述了制造肥皂的过程。文件中提及了关键的原料，碱。在13世纪，伊斯兰教世界的肥皂生产几乎进入了工业化时期。其原料是由纳布卢斯（Nablus），菲斯（Fes），大马士革（Damascus），阿勒颇（Aleppo）等地提供的。

### 西方

查理曼來到羅馬了解到肥皂的好處，回到法蘭克做推廣；在此之前，西方僅限於[西西里地區還使用](../Page/西西里.md "wikilink")。\[5\]在1662年[英國化學家](../Page/英國.md "wikilink")[仲恩及](../Page/仲恩.md "wikilink")[巴默將原本蓋蘭的肥皂改良了以後](../Page/巴默.md "wikilink")，肥皂清潔物品的效果也有好一點。法國化學家[歇夫爾也將仲恩及巴默改良的肥皂再深入加以延伸](../Page/歇夫爾.md "wikilink")，成為用牛油碱化原理製作肥皂的第一人，也是製造現代肥皂重要的一大步。

在4000多年前，在古希臘一個叫做[勒斯波斯的小島](../Page/勒斯波斯.md "wikilink")，當地的人用動物來祭天，由於燒動物的時候要用到木材，木材的灰燼和動物的脂肪混合後產生了一種類似肥皂的東西，大雨把這些東西沖刷到當地婦女用來洗衣服的河流，她們發現衣服洗的更乾淨了。

西元前3000年前，[美索不達米亞人](../Page/美索不達米亞人.md "wikilink")([兩河文化](../Page/兩河文化.md "wikilink"))發現，植物燃燒後的灰燼和油混合後，可以清除污垢。從義大利[龐貝城廢墟中](../Page/龐貝城.md "wikilink")，挖掘出來的肥皂工廠，便可以知道古羅馬已經開始使用肥皂了。[布路蘭是發明由食鹽製作碳酸鈉肥皂的第一人](../Page/布路蘭.md "wikilink")，也是肥皂歷史中，很重要的一人。

## 成分

主要成分都是[硬脂酸鈉](../Page/硬脂酸鈉.md "wikilink")，其分子式是[C](../Page/碳.md "wikilink")<sub>17</sub>[H](../Page/氫.md "wikilink")<sub>35</sub>C[OO](../Page/氧.md "wikilink")[Na](../Page/鈉.md "wikilink")。如果在裡面加進[精油](../Page/精油.md "wikilink")、[香料和染料](../Page/香料.md "wikilink")，就做成既有顏色，又有香味的香皂了；如果往裡面加點[藥物](../Page/藥物.md "wikilink")（如[硼酸或](../Page/硼酸.md "wikilink")[石炭酸](../Page/石炭酸.md "wikilink")），即成藥皂。肥皂加入香料雖然可增添香氣，但部分人士或會因為香料引起皮膚敏感，所以不是每個人都適合使用。

例如：[油脂](../Page/油脂.md "wikilink")+[氫氧化鈉](../Page/氫氧化鈉.md "wikilink")+[水](../Page/水.md "wikilink")
= 肥皂+[甘油](../Page/甘油.md "wikilink")

## 去污原理

[Sodium_stearate.png](https://zh.wikipedia.org/wiki/File:Sodium_stearate.png "fig:Sodium_stearate.png")的键线式。左为疏水端，右为亲水端\]\]
肥皂泡沫愈多，去汙效果愈好，這是因為肥皂分子([硬脂酸](../Page/硬脂酸.md "wikilink")[根](../Page/原子團.md "wikilink"))就在泡沫裡。肥皂[分子結構可以分成二個部分](../Page/分子結構.md "wikilink")。一端是帶[電荷呈極性的COO](../Page/電荷.md "wikilink")-（親水部位，即**亲水端**），另一端為非極性的碳鏈（親油部位，即**疏水端**）。肥皂能破壞[水的表面](../Page/水.md "wikilink")[張力](../Page/張力.md "wikilink")，當肥皂分子進入水中時，具有極性的親水部位，會破壞水分子間的吸引力而使水的表面張力降低，使水分子平均地分配在待清洗的[衣物或](../Page/衣物.md "wikilink")[皮膚表面](../Page/皮膚.md "wikilink")。肥皂的親油部位，深入油汙，而親水部位溶於水中，此結合物經攪動後形成較小的油滴，其表面布滿肥皂的親油部位，而不會重新聚在一起成大油污。此過程（又稱[乳化](../Page/乳化.md "wikilink")）重複多次，則所有油污均會變成非常微小的油滴溶於水中，可被輕易地沖洗乾淨。

## 使用

清潔身體：把要清潔的地方用水濕透，之後把肥皂塗在那裡，用手搓洗，就能清潔。

清潔衣服：把衣服用水濕透，把肥皂塗在衣服有污垢处，再把衣服互相搓揉，就能使衣服潔淨。

起霧：使用乾肥皂塗抹在鏡子上，再等肥皂乾掉，即能防霧。（亦可用浓稠肥皂水涂抹）

## 變形

  - 精皂花

<!-- end list -->

  -
    一種用香皂片壓模產生，經組合為玫瑰花瓣的皂花。帶有香皂的香氣，可以作為洗滌之用途。可在婚禮送禮使用或當作情人節禮物。原稱為皂花或花皂。

## 参照

  - [手工皂](../Page/手工皂.md "wikilink")：以[橄欖油](../Page/橄欖油.md "wikilink")，棕櫚油，椰子油等，加上[氫氧化鈉溶液](../Page/氫氧化鈉.md "wikilink")、精油或[香精等制成的香皂](../Page/香精.md "wikilink")。
  - [不锈钢皂](../Page/不锈钢皂.md "wikilink")：一种使用[不锈钢制成的肥皂狀金屬塊](../Page/不锈钢.md "wikilink")。在流動溫水中放手內揉搓，有去除手上魚腥味的功能。
  - [皂基](../Page/皂基.md "wikilink")：型為長條狀，方便自製香皂的基礎皂，但品質遠不如手工皂。
  - [肥皂粉](../Page/肥皂粉.md "wikilink")：俗稱[洗衣粉](../Page/洗衣粉.md "wikilink")。
  - [沐浴乳](../Page/沐浴乳.md "wikilink")：調配的方法與人工香皂雷同，所用的化學物品則是[氫氧化鉀](../Page/氫氧化鉀.md "wikilink")，成品則成濃稠的乳狀。
  - [沐浴露](../Page/沐浴露.md "wikilink")：是液體的肥皂或清潔液。時尚所至，沐浴露直接成為肥皂的[替代品](../Page/替代品.md "wikilink")。甚至有些[酒店](../Page/酒店.md "wikilink")[套房只有沐浴露](../Page/套房.md "wikilink")，而沒有肥皂供應。
  - [無患子是古代的主要清潔劑之一](../Page/無患子.md "wikilink")，一直在亞洲各地區用到20世紀初。

## 参考资料

<div class="references-small">

<references />

</div>

[Category:盐](../Category/盐.md "wikilink")
[Category:肥皂](../Category/肥皂.md "wikilink")
[Category:阴离子表面活性剂](../Category/阴离子表面活性剂.md "wikilink")
[Category:皮肤养护](../Category/皮肤养护.md "wikilink")
[Category:清潔](../Category/清潔.md "wikilink")

1.  [新华网广东频道 -
    歪打正着](http://www.gd.xinhuanet.com/newscenter/swnews/2004-08/16/content_2691862.htm)

2.
3.  [藝術與建築索引典—肥皂](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300014329)
    於2010年11月4日查閱
4.
5.