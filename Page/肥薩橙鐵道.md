[thumb柴油客車](../Page/file:HSOR-115.jpg.md "wikilink")，115號車，2010年時攝於[鹿兒島中央車站內](../Page/鹿兒島中央車站.md "wikilink")。\]\]
[thumb當地出身的漫畫家](../Page/file:HSOR-103.JPG.md "wikilink")[江口壽史設計](../Page/江口壽史.md "wikilink")，並配合當地的四季風景照片作為水俁市的觀光推廣。\]\]
**肥薩橙鐵道股份有限公司**\[1\]（），簡稱**肥薩橙鐵道**，是[日本](../Page/日本.md "wikilink")[九州地區的](../Page/九州.md "wikilink")[鐵路運輸業者](../Page/鐵路運輸.md "wikilink")。2002年時由[熊本縣和](../Page/熊本縣.md "wikilink")[鹿兒島縣沿線共](../Page/鹿兒島縣.md "wikilink")9個自治體（地方政府），連同[日本貨物鐵道](../Page/日本貨物鐵道.md "wikilink")（JR貨物）共10個單位聯合出資成立，肥薩橙鐵道是家以經營原本[鹿兒島本線於](../Page/鹿兒島本線.md "wikilink")[八代至](../Page/八代站.md "wikilink")[川內間的鐵路路線為主要營業項目的](../Page/川內站_\(鹿兒島縣\).md "wikilink")[第三部門公司](../Page/日本第三部門鐵道.md "wikilink")。

## 歷史

1991年時由於[九州新幹線鹿兒島線即將開工](../Page/九州新幹線.md "wikilink")，意味著原本一度是九州西岸南北向主幹線的鹿兒島本線將失去幹線的重要性，而有被廢線的可能。為了維持沿線城鎮的發展，在原本擁有鹿兒島本線經營權的[九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")（JR九州），與鐵道沿線熊本縣與鹿兒島縣境內9個縣級、市級與町級自治體的協調之下，決議由JR九州撥讓出既有的路線與沿線設施，交由新成立的第三部門業者（也就是日後的肥薩橙鐵道公司）經營，來作為允許九州新幹線開工興建的交換條件（根據日本相關法規，新鐵道路線的開通必須獲得平行路線沿線自治體的全部首肯，才能開始修築）。

2002年，九州新幹線鹿兒島線南段（[新八代至](../Page/新八代車站.md "wikilink")[鹿兒島中央段](../Page/鹿兒島中央站.md "wikilink")）通車在即，9個縣市鎮自治體與原本和JR九州共用鹿兒島本線的JR貨物合資21億2千萬[日圓](../Page/日圓.md "wikilink")，正式在該年10月31日成立肥薩橙鐵道公司。公司總部設於熊本縣[八代市萩原町](../Page/八代市.md "wikilink")1丁目1番地。2004年3月13日，九州新幹線啟用。JR九州正式將鹿兒島本線八代～川內段的經營轉交肥薩橙鐵道公司，而該路段即被命名為「[肥薩橙鐵道線](../Page/肥薩橙鐵道線.md "wikilink")」開始營運。

### 年表

  - 1991年：以鹿兒島本線八代～川内間改為第三部門方式維持營運作為交換條件，鹿兒島本線沿線自治體同意九州新幹線開始施工。
  - 2002年10月31日：肥薩橙鐵道公司正式成立。
  - 2004年3月13日：九州新幹線開始營運，肥薩橙鐵道也正式自九州旅客鐵道繼承八代至川内之間路段的營運權。
  - 2005年3月1日：新設[田浦御立岬公園車站](../Page/田浦御立岬公園車站.md "wikilink")（），是該公司成立後第一個新設的車站。

## 股東

採第三部門（非營利法人）方式經營的肥薩橙鐵道共擁有10個股東，其中9個是具有官方地位的地方自治體，而唯一的1個民營公司股東則是日本貨物鐵道。這10個股東分別為：

  - [熊本縣](../Page/熊本縣.md "wikilink")
      - [八代市](../Page/八代市.md "wikilink")
      - [津奈木町](../Page/津奈木町.md "wikilink")
      - [蘆北町](../Page/蘆北町.md "wikilink")
      - [水俁市](../Page/水俁市.md "wikilink")
  - [鹿兒島縣](../Page/鹿兒島縣.md "wikilink")
      - [出水市](../Page/出水市.md "wikilink")
      - [阿久根市](../Page/阿久根市.md "wikilink")
      - [薩摩川内市](../Page/薩摩川内市.md "wikilink")
  - [日本貨物鐵道](../Page/日本貨物鐵道.md "wikilink")

## 命名、企業識別標誌和吉祥物

肥薩橙鐵道的公司名稱全都取材於沿線當地的風土民情，其中「肥」與「薩」分別代表附近地區的古代國名——[肥後國與](../Page/肥後國.md "wikilink")[薩摩國](../Page/薩摩國.md "wikilink")——的首字，再加上沿線與風光明媚的[海岸線連在一起的](../Page/海岸線.md "wikilink")[蜜柑等](../Page/蜜柑.md "wikilink")[柑橘類水果產地](../Page/柑橘.md "wikilink")，組合成非常具有地方味道的公司命名。

至於由[建築家](../Page/建築師.md "wikilink")[川西康之所設計的企業識別標誌](../Page/川西康之.md "wikilink")，則是以兩個[-{zh-hans:橙;zh-hk:橙;zh-tw:柳橙;}-排成像是火車車輪般的形狀作為基礎](../Page/橙.md "wikilink")，中間的葉子則是依照沿線各車站的位置排列，並採用綠色的配色以表達該公司認為鐵道運輸應能與[環境友善並存的理念](../Page/環境友善.md "wikilink")。

肥薩橙鐵道的[吉祥物有六個](../Page/吉祥物.md "wikilink")，是以水果為題材的，分別是[金橘](../Page/金橘.md "wikilink")、[溫州](../Page/溫州.md "wikilink")[橘](../Page/橘.md "wikilink")、[蘆橘](../Page/蘆橘.md "wikilink")、甘夏、[文旦和晚白](../Page/文旦.md "wikilink")[柚](../Page/柚.md "wikilink")。

## 路線

<table>
<thead>
<tr class="header">
<th><p>中文線名</p></th>
<th><p>日文線名</p></th>
<th><p>起點</p></th>
<th><p>終點</p></th>
<th><p>距離（公里）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/肥薩橙鐵道線.md" title="wikilink">肥薩橙鐵道線</a></p></td>
<td></td>
<td><p><a href="../Page/八代站.md" title="wikilink">八代</a></p></td>
<td><p><a href="../Page/川內站_(鹿兒島縣).md" title="wikilink">川內</a></p></td>
<td><p>116.9</p></td>
</tr>
</tbody>
</table>

肥薩橙鐵道公司目前只有一條路線，稱為[肥薩橙鐵道線](../Page/肥薩橙鐵道線.md "wikilink")，營運區間介於熊本縣境內的八代與鹿兒島縣境內的川内之間，共116.9公里長。但為了方便列車接駁，部分列車會駛入目前還是由九州旅客鐵道經營的新八代（九州新幹線和鹿兒島本線交會站）與[隈之城](../Page/隈之城車站.md "wikilink")（鹿兒島本線）站內。

肥薩橙鐵道線全線皆採[電氣化設置](../Page/電氣化鐵路.md "wikilink")，但因[交流電列車的造價太高](../Page/交流電.md "wikilink")，不符合成本效益關係，該公司本身卻只使用[柴油車作為營運車輛](../Page/柴油車.md "wikilink")。之所以保留路線的電氣化，主要是因應共用同路線的日本貨物鐵道列車通過時的供電需要。

肥薩橙鐵道線全線共設有28個車站，但其中只有10個車站設有站務人員看管，其餘的18個站全為無人車站。

## 使用車輛

肥薩橙鐵道公司目前擁有19輛由[新潟運輸系統製造的](../Page/新潟運輸系統.md "wikilink")[柴油車](../Page/柴油車.md "wikilink")，其中17輛是一般的動力客車，2輛是活動專用車。使用車輛共可分成三種:[橘子號](../Page/橘子.md "wikilink")、[故鄉號和](../Page/故鄉.md "wikilink")[一般車輛](../Page/普通.md "wikilink")。平常大都採用單輛[一人控制的方式營運](../Page/一人控制.md "wikilink")，但必要時最多可以組成三輛一列的編組來行駛。最高營運車速為95公里/小時。

## 相關條目

  - [鹿兒島本線](../Page/鹿兒島本線.md "wikilink")
  - [九州新幹線](../Page/九州新幹線.md "wikilink")
  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")

## 外部連結

  - [肥薩橙鐵道日文網站](http://www.hs-orange.com)

## 參考資料

[肥薩橙鐵道](../Category/肥薩橙鐵道.md "wikilink")
[Category:日本鐵路公司](../Category/日本鐵路公司.md "wikilink")
[Category:第三部門鐵路公司](../Category/第三部門鐵路公司.md "wikilink")
[Category:熊本縣鐵路](../Category/熊本縣鐵路.md "wikilink")
[Category:鹿兒島縣鐵路](../Category/鹿兒島縣鐵路.md "wikilink")
[Category:熊本縣公司](../Category/熊本縣公司.md "wikilink")

1.  [1](http://www.hs-orange.com/t-chinese/common/img/top/pdf/leaflet_t-chinese01.pdf)
     根據官方中文宣傳刊物所標示的名稱。