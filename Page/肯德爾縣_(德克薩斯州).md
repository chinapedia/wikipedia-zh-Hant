**肯德爾县**（**Kendall County,
Texas**）是位於[美國](../Page/美國.md "wikilink")[德克薩斯州中南部的一個縣](../Page/德克薩斯州.md "wikilink")。面積1,717平方公里。根據[美國2000年人口普查估計](../Page/美國2000年人口普查.md "wikilink")，共有人口23,743人。縣治[伯尼](../Page/伯尼.md "wikilink")（Boerne）。

成立於1862年1月10日，縣政府成立於同年2月18日。縣名紀念[美墨戰爭時期記者喬治](../Page/美墨戰爭.md "wikilink")·W·肯德爾\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[K](../Category/得克萨斯州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.