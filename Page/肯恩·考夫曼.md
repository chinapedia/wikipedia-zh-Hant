**肯恩·考夫曼** ()
是一位[美国](../Page/美国.md "wikilink")[作家](../Page/作家.md "wikilink")、[艺术家](../Page/艺术家.md "wikilink")、[鸟类专家](../Page/鸟类专家.md "wikilink")、[博物学家和](../Page/博物学家.md "wikilink")[环保论者](../Page/环保论者.md "wikilink")，以写有几种流行的[野外手册](../Page/野外手册.md "wikilink")
(如[北美的鸟类](../Page/北美.md "wikilink")、蝴蝶) 而著称。

考夫曼生于[印第安纳州的](../Page/印第安纳州.md "wikilink")[南本德](../Page/南本德.md "wikilink")，6岁时就开始观鸟，16岁时受观鸟开拓者[罗杰·托瑞·彼得森的启发](../Page/罗杰·托瑞·彼得森.md "wikilink")，他放弃了高中学业，而开始搭车在北美寻鸟。3年后，也就是1973年，他建立了大部分北美鸟类的记录
(671)，在此记录种，过去不被鸟类研究视作北美的地区，比如[下加利福尼亞州](../Page/下加利福尼亞州.md "wikilink")，也被归入了北美，此后渐被采纳。他的跨国观鸟之旅，行程8000多英里，后被收入回忆录
*Kingbird Highway*。

此后，他致力于对鸟类野外手册的更新和扩展。1992年，他被[美国观鸟协会授予Ludlow](../Page/美国观鸟协会.md "wikilink")
Griscom Award 。

考夫曼现居住于[俄亥俄州的Rocky](../Page/俄亥俄州.md "wikilink") Ridge。

## 书目

  - *Kaufman Focus Guides: Birds of North America*
  - *Kaufman Focus Guides: Butterflies of North America* (with Jim P.
    Brock)
  - *Kingbird Highway* ISBN 0-395-77398-9
  - *Lives of American Birds*
  - *The Peterson Guide to Advanced Birding*

## 外部链接

  - [肯恩·考夫曼的自然网站](http://www.kknature.com/)
  - [肯恩·考夫曼访谈](http://www.birdwatching.com/tips/kkaufman.html)，birdwatching.com

[Kaufman, Kenn](../Category/美国鸟类学家.md "wikilink") [Kaufman,
Kenn](../Category/鸟类艺术家.md "wikilink")
[Category:美国作家](../Category/美国作家.md "wikilink")
[Category:印第安納州人](../Category/印第安納州人.md "wikilink")