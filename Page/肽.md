[Tetrapeptide_structural_formulae_v.1.png](https://zh.wikipedia.org/wiki/File:Tetrapeptide_structural_formulae_v.1.png "fig:Tetrapeptide_structural_formulae_v.1.png")-[甘氨酸](../Page/甘氨酸.md "wikilink")-[絲胺酸](../Page/絲胺酸.md "wikilink")-[丙胺酸](../Page/丙胺酸.md "wikilink"))
<span style="color:green;">**綠色**</span> 標示 氨基端
([<span style="color:green;">**L-纈氨酸**</span>](../Page/纈胺酸.md "wikilink"))
<span style="color:blue;">**藍色**</span> 標示 羧基端
([<span style="color:blue;">**L-丙氨酸**</span>](../Page/丙胺酸.md "wikilink")).\]\]
[1EDN_human_endothelin1_02.png](https://zh.wikipedia.org/wiki/File:1EDN_human_endothelin1_02.png "fig:1EDN_human_endothelin1_02.png")
[Alamethicin.png](https://zh.wikipedia.org/wiki/File:Alamethicin.png "fig:Alamethicin.png")
[Protein_primary_structure.svg](https://zh.wikipedia.org/wiki/File:Protein_primary_structure.svg "fig:Protein_primary_structure.svg")
**肽**（，來自希臘文的“消化”），旧称**-{胜}-**\[1\]，即**胜肽**，又稱**縮氨酸**，是天然存在的小生物分子，介於[胺基酸和](../Page/胺基酸.md "wikilink")[蛋白質之間的物質](../Page/蛋白質.md "wikilink")。

由於胺基酸的分子最小，蛋白質最大，而它們則是氨基酸單體組成的短鏈，由肽（[酰胺](../Page/酰胺.md "wikilink")）鍵連接。當一個氨基酸的羧基基團與另一個氨基酸的氨基反應時，形成該共價化學鍵。肽由氨基酸組成的短鏈是精準的蛋白質片段，其分子只有[纳米般大小](../Page/纳米.md "wikilink")，腸胃、血管及肌膚皆極容易吸收。二胜肽（簡稱二肽），就是由二個胺基酸組成的蛋白質片段，兩個或以上的胺基酸脫水縮合形成若干個[肽鍵從而組成一個肽](../Page/肽鍵.md "wikilink")，多個肽進行多級折叠就組成一個蛋白質分子。蛋白質有時也稱為“多肽”。

## 分類

常見的有**[二肽](../Page/二肽.md "wikilink")**（）、**[三肽](../Page/三肽.md "wikilink")**（），甚至**多肽**（）等，而2－20肽屬於**寡肽**（），20－50肽屬於**多肽**，通常十肽以下者較具[醫藥及](../Page/醫藥.md "wikilink")[商業實用性](../Page/商業.md "wikilink")。

## 水解法

肽可由膳食蛋白質（）通過化學方法水解出來，也可以人工方法取得。係由兩個或以上的胺基酸（）聚合所構成，在細胞生理及代謝功能的調節上甚為重要。肽大多性质不稳定，长期贮存宜防潮，放在4°C以下的地方。

## 用途

活性肽與營養、[荷爾蒙](../Page/荷爾蒙.md "wikilink")、[酵素抑制](../Page/酵素.md "wikilink")、調節[免疫](../Page/免疫.md "wikilink")、抗菌、抗病毒\[2\]、抗[氧化有非常緊密關係](../Page/氧化.md "wikilink")。

[Tetrapeptide_structural_formulae_v.1.png](https://zh.wikipedia.org/wiki/File:Tetrapeptide_structural_formulae_v.1.png "fig:Tetrapeptide_structural_formulae_v.1.png")

## 結構

**肽**是由[氨基酸的](../Page/氨基酸.md "wikilink")[胺基](../Page/胺基.md "wikilink")（-NH<sub>2</sub>）和[羧基](../Page/羧基.md "wikilink")（-COOH）脱水缩合形成[肽键後](../Page/肽键.md "wikilink")，形成的鏈狀分子。

肽鍵是由一個胺基酸與次一胺基酸的胺基，行脫水縮合反應而成的-CO-NH-鍵，具有雙鍵的性質，與鄰近共六個原子在同一平面上，因此C-N不可自由轉動，肽鍵是構成蛋白質架構的連繫帶。

对于含[半胱氨酸](../Page/半胱氨酸.md "wikilink")，[甲硫氨酸或](../Page/甲硫氨酸.md "wikilink")[色氨酸的肽](../Page/色氨酸.md "wikilink")，脱氧缓冲剂对其溶解必不可少，这类肽易于空气中氧化。含[谷氨酰胺或](../Page/谷氨酰胺.md "wikilink")[天冬酰胺的多肽也容易降解](../Page/天冬酰胺.md "wikilink")。多肽在-20℃很稳定，特别是冷冻干燥并保存在干燥器中，在将它们暴露于空气之前，冷冻干燥多肽可以放于室温。

## 參考資料

[肽](../Category/肽.md "wikilink")

1.  音shēng，不是“-{勝}-”的简化字。见《现代汉语词典》释义。
2.  [科學人-分秒不鬆懈的人體免疫防禦系統](http://sa.ylib.com/saeasylearn/saeasylearnshow.asp?FDocNo=1884&CL=87)