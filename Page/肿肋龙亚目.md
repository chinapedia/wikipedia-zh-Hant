**腫肋龍亞目**（Pachypleurosauria）是種原始的[鰭龍超目](../Page/鰭龍超目.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")，外表稍微地類似水生[蜥蜴](../Page/蜥蜴.md "wikilink")，只生存於[三疊紀](../Page/三疊紀.md "wikilink")。牠們的體型修長，體型從20公分到1公尺長，並擁有小頭部、長頸部、鰭狀肢、長尾巴。牠們的肢帶（Limb
Girdles）縮小許多，所以這群動物不太可能移動到陸地上。牠們的頜部前部佈滿釘狀牙齒，顯示這群動物以[魚類為生](../Page/魚類.md "wikilink")。
[Pachypleurosaurus.jpg](https://zh.wikipedia.org/wiki/File:Pachypleurosaurus.jpg "fig:Pachypleurosaurus.jpg")\]\]
腫肋龍亞目被歸類於[幻龍目](../Page/幻龍目.md "wikilink")。在有些[親緣分支分類法裡](../Page/親緣分支分類法.md "wikilink")，腫肋龍類被認為是[真鰭龍類的姐妹](../Page/真鰭龍類.md "wikilink")[分類單元](../Page/分類單元.md "wikilink")，真鰭龍類演化支包括幻龍類與[蛇頸龍類](../Page/蛇頸龍類.md "wikilink")。

## 參考資料

## 參考文獻

  - Michael J. Bemton (2004), *Vertebrate Paleontology*, 3rd ed.
    Blackwell Science Ltd
    [classification](https://web.archive.org/web/20060613012442/http://www.blackwellpublishing.com/book.asp?ref=0632056371&site=1)
  - Robert L. Carroll (1988), *Vertebrate Paleontology and Evolution*,
    WH Freeman & Co.
  - Oliver Rieppel, (2000), Sauropterygia I, placodontia,
    pachypleurosauria, nothosauroidea, pistosauroidea: In: Handbuch der
    Palaoherpetologie, part 12A, 134pp. Verlag Dr. Friedrich Pfeil
    [Table of contents](http://www.pfeil-verlag.de/07pala/d2_78d.html)

## 外部連結

  - [Lepidosauromorpha:
    Pachypleurosauridae](https://web.archive.org/web/20060222052942/http://www.palaeos.com/Vertebrates/Units/220Lepidosauromorpha/220.200.html)
    - Palaeos

[\*](../Category/幻龍目.md "wikilink")