**胡安·泰利·特里普**（，），航空業先鋒和企業家。

## 生平

1921年特里普於[耶魯大學畢業後](../Page/耶魯大學.md "wikilink")，開始在[華爾街工作](../Page/華爾街.md "wikilink")，但他很快便對工作失去興趣。在繼承一筆遺產後，便轉往紐約航空（New
York Airways）工作，該公司為一些權貴富豪提供空中接送服務。

特里普與一些耶魯大學的富有同學投資了一間名為殖民空運（Colonial Air
Transport）的航空公司。又創立了美州航空公司（Aviation
Company of the
Americas），這家公司後來參與了[泛美航空的業務發展](../Page/泛美航空.md "wikilink")。泛美航空首班航班於1927年10月28日由[佛羅里達州](../Page/佛羅里達州.md "wikilink")[基韋斯特啟航前往](../Page/基韋斯特.md "wikilink")[哈瓦那](../Page/哈瓦那.md "wikilink")。其後，特里普購入了[中國航空公司以提供](../Page/中國航空公司.md "wikilink")[中國國內的航空服務](../Page/中國.md "wikilink")。30年代時，泛美航空成為首家橫越太平洋的航空公司。[二戰期間](../Page/二戰.md "wikilink")，特里普的航空公司不斷擴展，是少數未有受戰爭嚴重影響的航空公司。

特里浦還曾贊助第一個不落地飛越大西洋的[林白和太空](../Page/林白.md "wikilink")[火箭製造者](../Page/火箭.md "wikilink")[高達德](../Page/高達德.md "wikilink")，使之無後顧之憂。
特里普更為航空業界帶來很多革新。他很快便認定了[噴射機時代的降臨](../Page/噴氣式飛機.md "wikilink")，訂購了一些[波音707和](../Page/波音707.md "wikilink")[麥道](../Page/麥克唐納-道格拉斯公司.md "wikilink")[DC-8客機](../Page/DC-8.md "wikilink")。泛美航空首班噴射機航班於1958年10月由波音707執行，由[紐約前往](../Page/紐約.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。新的噴射客機令泛美航空能夠以較低的票價搭載更多乘客。

1965年，特里普要求當時掌管波音的[比爾·阿倫](../Page/比爾·阿倫.md "wikilink")（Bill
Allen）研製一種較[波音707更大的客機](../Page/波音707.md "wikilink")，而該新型客機就是後來聞名世界的[波音747](../Page/波音747.md "wikilink")，而泛美航空就是波音747的[首名客戶](../Page/啟始客戶.md "wikilink")。特里普起初認為波音747最終會由當時研製中的超音速客機取代，並轉為貨機使用。但後來[協和式客機和](../Page/協和式客機.md "wikilink")[圖-144均未能達到要求](../Page/圖-144.md "wikilink")。

特里普於1968年離任泛美航空總裁一職。

1985年，[美國總統](../Page/美國總統.md "wikilink")[隆納·雷根追授特里普](../Page/隆納·雷根.md "wikilink")[總統自由勳章](../Page/總統自由勳章.md "wikilink")。

特里普在[娛樂大亨中的角色由](../Page/娛樂大亨.md "wikilink")[艾力克·包德溫飾演](../Page/艾力克·包德溫.md "wikilink")。

## 外部連結

  - [Time 100: profile of Juan
    Trippe](http://www.time.com/time/time100/builder/profile/trippe.html)
  - [Biography resource dedicated to Juan
    Trippe](https://web.archive.org/web/20070310223833/http://www.biographyshelf.com/juan_trippe_biography.html)

[Category:美國企业家](../Category/美國企业家.md "wikilink")
[Category:航空企业家](../Category/航空企业家.md "wikilink")
[Category:耶鲁大学校友](../Category/耶鲁大学校友.md "wikilink")