**胡家惠**（），[預科就讀於](../Page/預科.md "wikilink")[遵理學校](../Page/遵理學校.md "wikilink")，参加2002年度[香港小姐競選](../Page/香港小姐競選.md "wikilink")，參選編號是15號，被傳媒形容為「女版[許愿](../Page/許愿.md "wikilink")」（即其貌不揚）。在不備受看好的情況下，憑著決賽當晚出色的問答表現進入五強，繼而奪得季軍，被傳媒視爲當年最大的冷門。其後代表[香港参加](../Page/香港.md "wikilink")2002年[國際小姐賽並獲得友誼小姐](../Page/国际小姐.md "wikilink")。卸任後她沒有簽約[無綫電視而離開娛樂圈](../Page/香港無綫電視.md "wikilink")。

## 参加竞赛

  - 2002年[香港小姐](../Page/香港小姐.md "wikilink")
  - 2002年[国际小姐](../Page/国际小姐.md "wikilink")

## 奖项

  - 2002年[香港小姐季军](../Page/香港小姐.md "wikilink")
  - 2002年[国际小姐友谊小姐](../Page/国际小姐.md "wikilink")

{{-}}

[Category:2002年度香港小姐競選參賽者](../Category/2002年度香港小姐競選參賽者.md "wikilink")
[Category:香港順德人](../Category/香港順德人.md "wikilink")
[K](../Category/胡姓.md "wikilink")