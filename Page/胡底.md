**胡底**，原名**胡百昌**，又名**胡北风**，化名**胡马**等，[安徽](../Page/安徽.md "wikilink")[舒城人](../Page/舒城.md "wikilink")，[中国共产党早期著名谍报人员](../Page/中国共产党.md "wikilink")、[中国电影明星](../Page/中国.md "wikilink")。

1925年胡在[北京](../Page/北京.md "wikilink")[中国大学求学期间加入中国共产党](../Page/中国大学.md "wikilink")。1926年同[钱壮飞等人合拍中国第一部黑白武侠故事片](../Page/钱壮飞.md "wikilink")《燕山侠隐》，1927年遭[张作霖通缉](../Page/张作霖.md "wikilink")，改名胡北风，南下避难[上海](../Page/上海.md "wikilink")。到上海后，加入[昆仑电影制片厂](../Page/昆仑电影制片厂.md "wikilink")，作为男主角出演《盘丝洞》《昆仑大盗》等武侠电影，被誉为“东方[范明克](../Page/范明克.md "wikilink")”。
[Hu_Di.jpg](https://zh.wikipedia.org/wiki/File:Hu_Di.jpg "fig:Hu_Di.jpg")
1929年，胡与[李克农结识](../Page/李克农.md "wikilink")，恢复了与共产党组织的联系，并介绍钱壮飞与李见面。后钱受[中共中央特科委派](../Page/中共中央特科.md "wikilink")，赴[南京打入](../Page/南京.md "wikilink")[徐恩曾组建的](../Page/徐恩曾.md "wikilink")[中国国民党特务组织](../Page/中国国民党.md "wikilink")[中央组织部党务调查科](../Page/中统.md "wikilink")。胡底为免暴露身份，前往南京隐居于钱家。

1930年，受钱壮飞委托，在南京[丹凤街开设](../Page/丹凤街.md "wikilink")“民智通讯社”，明裡为国民党服务，暗里作为共产党的联络据点。当年年底，又被派往[天津开设](../Page/天津.md "wikilink")“长江通讯社”，为共产党在天津打开了工作局面。当时，中共中央决定将打入国民党内部卧底的李克农、钱壮飞、胡底三人组成党小组，后[周恩来称誉他们为](../Page/周恩来.md "wikilink")“[龙潭三杰](../Page/龙潭三杰.md "wikilink")”。

1931年4月，中共重要领导人[顾顺章叛变](../Page/顾顺章.md "wikilink")，钱壮飞为营救中共在上海的组织而暴露身份。胡底与李克农等人被迫转移去[中央苏区](../Page/中央苏区.md "wikilink")。在苏区期间，胡底与钱壮飞等人一方面负责政治保卫工作，一方面又组建了[八一剧团](../Page/八一剧团.md "wikilink")。胡底创作并出演了一批作品，为鼓舞士气起到了良好效果。1932年底，胡底出任[中华苏维埃共和国政治保卫局局长](../Page/中华苏维埃共和国.md "wikilink")。

1934年，参加[长征](../Page/长征.md "wikilink")。1935年6月12日，[红一方面军和](../Page/红一方面军.md "wikilink")[红四方面军在](../Page/红四方面军.md "wikilink")[四川](../Page/四川.md "wikilink")[懋功会师](../Page/懋功.md "wikilink")。会师后[红军重组领导体系](../Page/红军.md "wikilink")，胡底随同[朱德等人跟随](../Page/朱德.md "wikilink")[张国焘率领的红四方面军活动](../Page/张国焘.md "wikilink")。8月，张国焘另立党中央，带领红四方面军南下，与红一方面军脱离。胡底反对张国焘的分裂活动，于9月被张国焘派人勒死。

1945年[中共七大决定追认为](../Page/中共七大.md "wikilink")[革命烈士](../Page/革命烈士.md "wikilink")。

## 参见

  - [李克农](../Page/李克农.md "wikilink")
  - [钱壮飞](../Page/钱壮飞.md "wikilink")

[Category:中华民国大陆时期电影演员](../Category/中华民国大陆时期电影演员.md "wikilink")
[Category:中共中央特科人物](../Category/中共中央特科人物.md "wikilink")
[Category:中华民国大陆时期中共烈士](../Category/中华民国大陆时期中共烈士.md "wikilink")
[Category:中国共产党党员
(1925年入党)](../Category/中国共产党党员_\(1925年入党\).md "wikilink")
[Category:被中共政权处决者](../Category/被中共政权处决者.md "wikilink")
[Category:六安人](../Category/六安人.md "wikilink")
[D底](../Category/胡姓.md "wikilink")