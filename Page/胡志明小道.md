[HoCMT.png](https://zh.wikipedia.org/wiki/File:HoCMT.png "fig:HoCMT.png")
[Defoliation_agent_spraying.jpg](https://zh.wikipedia.org/wiki/File:Defoliation_agent_spraying.jpg "fig:Defoliation_agent_spraying.jpg")直升機正在向叢林噴灑脫葉劑(1969年)。\]\]
**胡志明小道**（）又稱**胡志明小徑**（），[越南亦稱](../Page/越南.md "wikilink")「**長山道**」（）是1959年由[北越國家主席](../Page/越南社會主義共和國主席.md "wikilink")[胡志明下令开闢支援南方作战的通道](../Page/胡志明.md "wikilink")，繞道[老撾](../Page/老撾.md "wikilink")、[柬埔寨境內](../Page/柬埔寨.md "wikilink")，可從北部榮市經過中部非軍事區通往南方戰場，故稱為“胡志明小道”。这也使[越战扩大至](../Page/越战.md "wikilink")[老撾](../Page/老撾.md "wikilink")、[柬埔寨境內](../Page/柬埔寨.md "wikilink")。

1959年6月10日，[越南人民軍首次通过](../Page/越南人民軍.md "wikilink")“胡志明小道”向南方运送武器装备。“胡志明小道”共有5条主路、29条支路，還有其它捷道、旁門左道，总长近2万公里。1960年美軍終於發現胡志明小道，調派T-28教练机展開偵察，1964年胡志明又對小道展開擴建工作，可以行驶卡车，還建立地下兵营、倉庫、油庫等。

1964年底美軍B-26轟炸机開始轟炸胡志明小道，並以UC-123飛机噴灑脫葉毒劑，一直持續到1965年几乎每天都有美方軍機往老挝進行轰炸，造成北越軍重大的傷亡。面对疯狂的空袭，“胡志明小道”的运输车必須夜间行驶。从1964年到1967年六年之間，美军總共出动战机18万多架次空袭“胡志明小道”，造成胡志明部队約4.6万车辆毁坏，但中國仍大量提供胡志明部隊軍需，使得美軍的轟炸終始無法奏效。在此期間共有132架美军战机被擊落。

1968年初，美軍在胡志明小道投下20多萬枚電子感測器，為美軍轟炸機提供了準確的資訊，北越軍開始以牛群、猩猩、猴子作為干擾，使得美國計劃再度落空。1971年“胡志明小道”每星期約有9000輛車次通過，1972年戰事已經接近尾聲，美國實施[碘化銀](../Page/碘化銀.md "wikilink")[人造雨](../Page/人造雨.md "wikilink")，形成狂暴雨，小徑泥濘難行，通車數量降至900輛。1973年1月27日美越簽署停戰協定，一直到戰爭結束，美國始終無法根除胡志明小道的問題，使得北越軍的作戰物資始終源源不絕。

## 参看

  - [越南战争](../Page/越南战争.md "wikilink")
  - [橙劑](../Page/橙劑.md "wikilink")
  - [補給線](../Page/補給線.md "wikilink")

[Category:越南戰爭遺跡](../Category/越南戰爭遺跡.md "wikilink")
[HZMXD](../Category/越南歷史.md "wikilink")
[HZMXD](../Category/越南地理.md "wikilink")
[Category:跨境行动](../Category/跨境行动.md "wikilink")