**胡春华**（
），原名**王春华**，乳名**红兵**，汉族，[湖北](../Page/湖北省.md "wikilink")[五峰人](../Page/五峰土家族自治县.md "wikilink")，中国[政治家](../Page/政治家.md "wikilink")。[北京大学中文系毕业](../Page/北京大学中国语言文学系.md "wikilink")，[中共中央党校](../Page/中共中央党校.md "wikilink")[在職研究生学历](../Page/在職研究生.md "wikilink")。1983年4月加入[中国共产党](../Page/中国共产党.md "wikilink")，是中共第十七届、十八届、十九届中央委员，十八届、十九届[中共中央政治局委员](../Page/中共中央政治局.md "wikilink")，现任[国务院副总理](../Page/国务院副总理.md "wikilink")（排名第三）。曾担任[中国共青团中央书记处第一书记](../Page/中国共产主义青年团.md "wikilink")、[河北省省长](../Page/河北省.md "wikilink")、[中共内蒙古自治区党委书记](../Page/中国共产党内蒙古自治区委员会.md "wikilink")、[中共广东省委书记等职](../Page/中国共产党广东省委员会.md "wikilink")。\[1\]\[2\]

## 早年生活

1963年4月，胡春华出生在[湖北省](../Page/湖北省.md "wikilink")[宜昌市](../Page/宜昌市.md "wikilink")[五峰土家族自治县马岩墩村的一个农民家庭](../Page/五峰土家族自治县.md "wikilink")。1979年，他以全县[文科](../Page/文科.md "wikilink")“[状元](../Page/状元.md "wikilink")”的优异成绩考入[北京大学中文系](../Page/北京大学.md "wikilink")，成为五峰县历史上第一个考入北京大学的学生，现任国务院总理李克强当时在[北京大学法学系读书](../Page/北京大学.md "wikilink")。1983年6月大学毕业之际，胡春华主动要求去[西藏工作](../Page/西藏自治区.md "wikilink")。在[人民大会堂召开毕业生大会上](../Page/人民大会堂.md "wikilink")，作为毕业生代表，胡春华表示正是有了“[改革开放](../Page/改革开放.md "wikilink")”的政策，才改变了自己的家乡，一个曾经闭塞的内陆[少数民族地区](../Page/少数民族.md "wikilink")，同时也改变了自己的命运。所以他下决心去西藏工作，帮助少数民族地区也能实现现代化\[3\]，这件事情在当时引起了轰动，《[人民日报](../Page/人民日报.md "wikilink")》也对此事进行了报导\[4\]。

## 从政经历

胡春华的从政经历创造了中共政坛的多项纪录，而他的升迁路线则与“[中国共青团](../Page/中国共产主义青年团.md "wikilink")”分不开，因此被外界视为“[團派](../Page/團派.md "wikilink")”一员。有媒体总结如下：他于1990年2月，不足27岁，就出任[副厅级官职的](../Page/中华人民共和国公务员.md "wikilink")[共青团](../Page/中国共产主义青年团.md "wikilink")[西藏自治区委副书记](../Page/西藏自治区.md "wikilink")；1997年12月，34岁，升任[副部级的共青团中央书记处书记](../Page/中华人民共和国公务员.md "wikilink")、兼[全国青联副主席](../Page/中华全国青年联合会.md "wikilink")；2006年11月，43岁，晋升为[正部级官员](../Page/中华人民共和国公务员.md "wikilink")，出任[共青团中央书记处第一书记](../Page/中国共产主义青年团中央委员会.md "wikilink")；2008年4月，45岁的胡春华成为当时[中国最年轻的](../Page/中华人民共和国.md "wikilink")[省级行政区领导人](../Page/中华人民共和国省级行政区领导人.md "wikilink")\[5\]。2009年11月胡春华接替[储波任](../Page/储波.md "wikilink")[中共内蒙古自治区党委书记](../Page/中国共产党内蒙古自治区委员会.md "wikilink")\[6\]。2010年1月24日，当选为内蒙古人大常委会主任。2012年12月18日，接替[汪洋出任广东省党委书记](../Page/汪洋_\(政治人物\).md "wikilink")。

### 初到西藏

1983年8月，胡春华入藏，并先后在[共青团](../Page/中国共产主义青年团.md "wikilink")[西藏自治区委组织部](../Page/西藏自治区.md "wikilink")，《西藏青年报》社，西藏饭店工作；1987年出任共青团西藏自治区委副书记。1992年3月，胡春华获得外放机会，到西藏[林芝地区行政公署任副专员](../Page/林芝市.md "wikilink")（相当于副市长）；九个月后，他又回到共青团西藏自治区委，并升任书记；1995年，又外放至[山南地区行政公署任专员](../Page/山南市.md "wikilink")（相当于市长），时年32岁\[7\]。值得注意的是，在1988年到1992年底的这段时间，担任[中共西藏自治区委书记的](../Page/中国共产党西藏自治区委员会.md "wikilink")，正是也曾担任过共青团领导的[胡锦涛](../Page/胡锦涛.md "wikilink")\[8\]。

后来，胡春华在1996年9月到1997年7月间，回到[北京](../Page/北京市.md "wikilink")，在[中共中央党校培训部一年制中青班学习](../Page/中共中央党校.md "wikilink")。学习结束后四个月（即，1997年11月），胡春华就被调入[共青团中央工作](../Page/中国共产主义青年团.md "wikilink")，从而结束了他在西藏工作的第一阶段，共十四年。

### 二次进藏

在共青团中央期间，胡春华一直担任共青团中央书记处书记兼[全国青联副主席](../Page/中华全国青年联合会.md "wikilink")；同时还在[中共中央党校](../Page/中共中央党校.md "wikilink")[研究生院](../Page/研究所.md "wikilink")[在職研究生班世界经济专业学习](../Page/在職研究生.md "wikilink")。四年团中央工作经历之后，2001年胡春华再次进藏，历任[中共西藏自治区党委常委兼秘书长](../Page/中国共产党西藏自治区委员会.md "wikilink")，自治区党委副书记，[自治区政府常务副主席](../Page/西藏自治区人民政府.md "wikilink")，自治区党校校长等职。

经过五年历练之后2006年12月，胡春华又调回共青团中央，接替[周强出任团中央第一书记](../Page/周强.md "wikilink")。

### 任职河北

2008年3月，[中共中央宣布任命胡春华任](../Page/中国共产党中央委员会.md "wikilink")[中共河北省委副书记](../Page/中国共产党河北省委员会.md "wikilink")；并提名[河北省人民政府省长候选人](../Page/河北省人民政府.md "wikilink")\[9\]。4月，被河北省人大常委會任命為河北省副省長、代省長。2009年1月12日，正式当选[河北省人民政府](../Page/河北省人民政府.md "wikilink")[省長](../Page/河北行政长官列表.md "wikilink")。在胡春华就职河北不久便发生了李家窪煤礦特別重大礦難瞞報事件。紧接着又发生了震惊全球、影响深远的[三鹿奶粉事件](../Page/2008年中国奶制品污染事件.md "wikilink")，有网站认为事件最初被河北省政府掩盖因而抨击胡春華\[10\]。胡春华在三鹿事件处理过程时任“三鹿奶粉重大安全事故应急处置领导小组组长”，在2008年9月16日[石家庄公安局视察侦办情况时称](../Page/石家庄市.md "wikilink")：“对那些坑害消费者、坑害奶农的不法分子一定要严厉打击、依法严惩”\[11\]。并在2008年12月7日在“石家庄市建设食品安全放心城市万人承诺签名活动仪式”带头签名\[12\]。又《[文汇报](../Page/文汇报_\(香港\).md "wikilink")》报道称胡春华通过信息公开、严格执法，逐步化解了毒奶粉事件给社会带来的不良影响\[13\]。

### 内蒙古时期

2009年11月，中共中央调胡春华接替[储波](../Page/储波.md "wikilink")，担任[中共内蒙古自治区党委书记](../Page/中国共产党内蒙古自治区委员会.md "wikilink")；次年1月，又当选内蒙古自治区人大常委会主任\[14\]。
在内蒙古任职期间，内蒙古发生了因牧民被卡车轧死，爆发大规模的抗议。事件发生后，美国之音称内蒙古部分地区实行了戒严\[15\]。有美国网站报道认为，内蒙古压死牧民事件本可以通过有效疏导化解，但因胡春华误判形势，过于偏执于事件背后所谓的“蒙独”因素，进而施行铁腕镇压，因为其思想僵化反而扩大了“[蒙独](../Page/內蒙古獨立運動.md "wikilink")”因子\[16\]。而中国大陆《内蒙古日报》则报道，胡春华称：“最近发生在[锡盟的两起案件](../Page/锡林郭勒盟.md "wikilink")，性质恶劣，民愤极大。自治区党委、政府对这两起案件高度重视，态度非常明确，坚决依法从重从快，予以严惩”。又这篇报道称这两起案件的凶手都已经被批准逮捕，司法部门正在审理中\[17\]。

又根据《[文汇报](../Page/文汇报_\(香港\).md "wikilink")》的报道，胡春华在内蒙古把扶贫开发当作头号民生工程，2011年，内蒙古全区各级财政民生投入1,956亿元，较上年增加300亿元，主要民生领域的财政支出增长都在20%以上\[18\]。

## 進入中央

### 进入政治局

2012年11月15日，在[中共十八届一中全會上](../Page/中国共产党第十八届中央委员会第一次全体会议.md "wikilink")，胡春华晋升[中共中央政治局委員](../Page/中国共产党中央政治局.md "wikilink")\[19\]。在本届25人的政治局中，他和[孙政才都是](../Page/孙政才.md "wikilink")15名新成员之一，《[纽约时报](../Page/纽约时报.md "wikilink")》评论称这被视为他们正走在通往政治局核心层的路上，有年龄优势的他们很有机会在2017年召开的[中共十九大上进入](../Page/中国共产党第十九次全国代表大会.md "wikilink")[中共中央政治局常委会](../Page/中国共产党中央政治局常务委员会.md "wikilink")。而如成功晋身常委，将使他们有机会在2022年[中共二十大](../Page/中共二十大.md "wikilink")[习近平和](../Page/习近平.md "wikilink")[李克强卸任之后](../Page/李克强.md "wikilink")，竞争党内最高领导的职务，成为[中共第六代領導人](../Page/中国共产党集体领导制度.md "wikilink")\[20\]。然而胡春华并没有在十九大上成功晋升[中央政治局常委](../Page/中央政治局常委.md "wikilink")，[孙政才更是在](../Page/孙政才.md "wikilink")2017年7月因为严重违纪接受组织审查。

### 主政广东

2012年12月18日，49岁的胡春华接替即将在次年3月担任[国务院副总理的](../Page/中华人民共和国国务院副总理.md "wikilink")[汪洋](../Page/汪洋_\(政治人物\).md "wikilink")，出任[中共广东省委书记](../Page/中国共产党广东省委员会.md "wikilink")，成为广东改革开放以来最年轻的省委书记。2013年8月28日，胡春华在[广东省各](../Page/广东省.md "wikilink")[民主党派负责人暑期座谈会上称](../Page/民主党派.md "wikilink")：“要始终坚持中国共产党的领导……要坚定走[中国特色社会主义政治发展道路](../Page/中国特色社会主义.md "wikilink")，引导广大成员坚持走中国特色社会主义政治发展道路，自觉抵制西方政治制度和政党制度的渗透影响，在实践中增强对社会主义制度的自信和认同”\[21\]。2016年6月到9月，[陆丰市](../Page/陆丰市.md "wikilink")[东海镇](../Page/东海镇.md "wikilink")[烏坎村党支部书记兼](../Page/烏坎村.md "wikilink")[村民委员会主任](../Page/村民委员会.md "wikilink")[林祖恋被抓最后获刑](../Page/林祖恋.md "wikilink")，乌坎村村民为了争取土地权益并要求释放林祖恋长期连续进行抗议，2016年9月13日凌晨，大批警察闯入村内进行抓人，双方发生流血冲突，大约70个村民被抓。\[22\]\[23\]据[BBC报道](../Page/英国广播公司.md "wikilink")，本次镇压行动是省委书记胡春华直接下令的，目的是证明自己有临危不乱的能力，以便在2017年的[中共十九大上能够顺利成为](../Page/中国共产党第十九次全国代表大会.md "wikilink")[政治局常委](../Page/中国共产党中央政治局常务委员会.md "wikilink")；而中共高层认为，如何处理[乌坎问题将是考验胡春华能否入常的一个重要因素](../Page/烏坎事件.md "wikilink")\[24\]。2017年5月26日连任中共广东省委书记、常委。2017年10月28日，不再兼任广东省委委员、常委、书记\[25\]，由第十九届中央政治局委员李希兼任广东省委书记。

### 入常失败

先前一直有传胡春华是[中共十九届](../Page/中国共产党第十九届中央委员会.md "wikilink")[中央政治局常委的热门人选](../Page/中国共产党中央政治局常务委员会.md "wikilink")，但从[中共十九届一中全会公布的名单显示](../Page/中国共产党第十九届中央委员会第一次全体会议.md "wikilink")，胡没有入选常委，仅继续留任[政治局委员](../Page/中国共产党中央政治局.md "wikilink")。\[26\]\[27\]有年龄和资历优势的胡春华最终不能当选政治局常委，可能意味着他失去[第六代接班人的资格](../Page/中国共产党集体领导制度.md "wikilink")，加上另一位拥有接班优势的[孙政才已被开除公职和党籍](../Page/孙政才.md "wikilink")，在没有接班人选的情况下，[习近平有很大机会在](../Page/习近平.md "wikilink")2022年的[中共二十大上连任第三届](../Page/中共二十届一中全会.md "wikilink")[总书记和](../Page/中国共产党中央委员会总书记.md "wikilink")[军委主席](../Page/中国共产党中央军事委员会主席.md "wikilink")，打破[改革开放以来形成的](../Page/改革开放.md "wikilink")[任期制](../Page/任期制.md "wikilink")。\[28\]

### 当选副总理

2018年3月19日，根据[国务院总理](../Page/中华人民共和国国务院总理.md "wikilink")[李克强提名](../Page/李克强.md "wikilink")\[29\]，胡春华在[第十三届全国人民代表大会第一次会议第七次全体会议上被任命为](../Page/第十三届全国人民代表大会.md "wikilink")[国务院副总理](../Page/国务院副总理.md "wikilink")，在四位副总理中排名第三，主管[三农](../Page/三农.md "wikilink")\[30\]、扶贫\[31\]、商务、贸易\[32\]工作。

## 家族

## 参考文献

## 外部链接

  - [胡春华-中国党政领导干部资料库](https://web.archive.org/web/20160817002415/http://gbzl.people.com.cn/grzy.php?id=121000089)
  - [胡春华同志简历](http://district.ce.cn/newarea/sddy/201212/18/t20121218_23953037.shtml)
    中国经济网 2014-05-11

{{-}}

[Category:五峰人](../Category/五峰人.md "wikilink")
[C春](../Category/胡姓.md "wikilink")
[C春](../Category/王姓.md "wikilink") [Category:中国共产党党员
(1983年入党)](../Category/中国共产党党员_\(1983年入党\).md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:中国共产主义青年团第一书记](../Category/中国共产主义青年团第一书记.md "wikilink")
[H](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中共内蒙古自治区党委书记](../Category/中共内蒙古自治区党委书记.md "wikilink")
[Category:内蒙古自治区人大常委会主任](../Category/内蒙古自治区人大常委会主任.md "wikilink")
[H](../Category/中国共产党第十八届中央政治局委员.md "wikilink")
[Category:中共广东省委书记](../Category/中共广东省委书记.md "wikilink")
[Category:中国共产党第十九届中央政治局委员](../Category/中国共产党第十九届中央政治局委员.md "wikilink")
[Category:中国共产党第十九届中央委员会委员](../Category/中国共产党第十九届中央委员会委员.md "wikilink")
[Category:中国共产党第十八届中央委员会委员](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:第13届国务院常务会议组成人员](../Category/第13届国务院常务会议组成人员.md "wikilink")
[Category:第13届国务院副总理](../Category/第13届国务院副总理.md "wikilink")
[Category:全国青联副主席](../Category/全国青联副主席.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.

9.

10.

11.

12.

13.

14.

15.

16.

17.

18.
19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.