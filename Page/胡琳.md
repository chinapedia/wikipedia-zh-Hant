**胡琳**（，），[香港女艺人](../Page/香港.md "wikilink")，2006年出道，2010年曾用[藝名](../Page/藝名.md "wikilink")“小琳”，但後期仍使用胡琳發表專輯。2017年首次參与电視剧《[溏心风暴3](../Page/溏心风暴3.md "wikilink")》。

## 个人簡历

胡琳[祖籍](../Page/祖籍.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[台山市](../Page/台山市.md "wikilink")，18歲時到[美國](../Page/美國.md "wikilink")[紐約學習唱歌](../Page/紐約.md "wikilink")，在那兒住了8年，後來回港當歌手\[1\]。她自幼喜愛以[R\&B為主的](../Page/R&B.md "wikilink")[流行音樂](../Page/流行音樂.md "wikilink")，於紐約讀書期間亦深受當地的[爵士和](../Page/爵士樂.md "wikilink")[騷靈音樂影響](../Page/騷靈.md "wikilink")，因此其歌曲風格帶有以上多種元素。

胡琳於2006年出道，曲風以爵士樂及R\&B為主，至今發行過不少個人專輯，包括全[英文經典爵士樂專輯](../Page/英文.md "wikilink")《Bianca
Sings Timeless》。曾獲頒獎項包括2006年[IFPI最暢銷本地女新人](../Page/IFPI.md "wikilink")。

胡琳曾於紐約多間著名學院修讀演藝課程，包括[纽约电影学院](../Page/纽约电影学院.md "wikilink")、HB Acting
Studio, New
York，及著名[爵士鋼琴大師](../Page/爵士鋼琴.md "wikilink")的[聲樂訓練課程](../Page/聲樂.md "wikilink")。她亦於接受過多種[舞蹈訓練](../Page/舞蹈.md "wikilink")，並跟隨國際知名導師Eugene
Katsevman和Maria
Manusova專攻國際[拉丁舞](../Page/拉丁舞.md "wikilink")。胡琳常與紐約著名爵士鋼琴家。

由於在紐約讀書期間深受當地的爵士和騷靈音樂影響，2006年胡琳回港開始發展其歌唱事業，更將爵士和騷靈曲風帶進其廣東歌曲中，塑造了極具個人特式的曲風，得到廣大的音響發燒樂迷，及追求高質素音樂的知音者推崇，2010年推出的發燒天碟《Jazz
Them
Up》專輯，不論口碑及銷量一致得到非常高的評價；2011年於[香港演藝學院舉行一連兩場](../Page/香港演藝學院.md "wikilink")《Bianca
LIVE\! with New York Jazz Cat》音樂會，叫好叫座，更被業界及樂迷譽為新一代的「爵士小天后」。

2012年，胡琳共推出了兩張唱片，一張為全新國語大碟《Déjà Vu
夢空間》，另一張為翻唱[鄧麗君歌曲的專輯](../Page/鄧麗君.md "wikilink")《Bianca
Sings Tess》，並在年末於[香港文化中心舉行了一連兩場](../Page/香港文化中心.md "wikilink")《Bianca Wu
胡琳 Nice & Easy Concert》音樂會。

2014年初，胡琳延續2010年推出的發燒天碟《Jazz Them
Up》專輯的理念，推出另一張翻唱[四大天王歌曲的專輯](../Page/四大天王.md "wikilink")《Jazz
Them Up Once More》，首日公開發售已迅速被買光，各大唱片公司均需補充訂貨。

2016年，她首度參與《歌词大师卢国沾作品演唱會》，与前辈[麦洁文](../Page/麦洁文.md "wikilink")（Kitman）同台演出；其后更举行个人演唱會，憑着细膩的聲線及出色的歌艺而广为人知。

## 唱片

### 派台歌曲成績

<table>
<thead>
<tr class="header">
<th><p><strong>派台歌曲成績</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>大碟</p></td>
</tr>
<tr class="even">
<td><p><strong>2005年</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Love_Notes.md" title="wikilink">Love Notes</a></p></td>
</tr>
<tr class="even">
<td><p>下不為例</p></td>
</tr>
<tr class="odd">
<td><p><strong>2006年</strong></p></td>
</tr>
<tr class="even">
<td><p>Love Notes</p></td>
</tr>
<tr class="odd">
<td><p>自由像你</p></td>
</tr>
<tr class="even">
<td><p>手．心</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Sings_Timeless.md" title="wikilink">Sings Timeless</a></p></td>
</tr>
<tr class="even">
<td><p>綠野聲踪</p></td>
</tr>
<tr class="odd">
<td><p>綠野聲-{踪}-</p></td>
</tr>
<tr class="even">
<td><p><strong>2007年</strong></p></td>
</tr>
<tr class="odd">
<td><p>綠野聲踪</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Feel_So_Good_(胡琳專輯).md" title="wikilink">Feel So Good</a></p></td>
</tr>
<tr class="odd">
<td><p>他</p></td>
</tr>
<tr class="even">
<td><p>趁墟</p></td>
</tr>
<tr class="odd">
<td><p>間尺</p></td>
</tr>
<tr class="even">
<td><p><strong>2008年</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Still..._A_Wonderful_World.md" title="wikilink">Still... A Wonderful World</a></p></td>
</tr>
<tr class="even">
<td><p>一個人</p></td>
</tr>
<tr class="odd">
<td><p>東風</p></td>
</tr>
<tr class="even">
<td><p><strong>2009年</strong></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Jun_K.md" title="wikilink">Jun K</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><strong>2010年</strong></p></td>
</tr>
<tr class="odd">
<td><p>Jazz Them Up</p></td>
</tr>
<tr class="even">
<td><p>他的女人</p></td>
</tr>
<tr class="odd">
<td><p>Oh 夜</p></td>
</tr>
<tr class="even">
<td><p><strong>2011年</strong></p></td>
</tr>
<tr class="odd">
<td><p>Bianca Live</p></td>
</tr>
<tr class="even">
<td><p><strong>2012年</strong></p></td>
</tr>
<tr class="odd">
<td><p>Déjà Vu 夢空間</p></td>
</tr>
<tr class="even">
<td><p>Déjà Vu</p></td>
</tr>
<tr class="odd">
<td><p>天窗</p></td>
</tr>
<tr class="even">
<td><p>Sings Tess</p></td>
</tr>
<tr class="odd">
<td><p><strong>2013年</strong></p></td>
</tr>
<tr class="even">
<td><p>Sings Tess</p></td>
</tr>
<tr class="odd">
<td><p><strong>2014年</strong></p></td>
</tr>
<tr class="even">
<td><p>Jazz Them Up Once More</p></td>
</tr>
<tr class="odd">
<td><p>月半彎</p></td>
</tr>
<tr class="even">
<td><p>第四晚心情</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/The_Fifth_Season.md" title="wikilink">The Fifth Season</a></p></td>
</tr>
<tr class="even">
<td><p>被窩</p></td>
</tr>
<tr class="odd">
<td><p>冬至</p></td>
</tr>
<tr class="even">
<td><p><strong>2015年</strong></p></td>
</tr>
<tr class="odd">
<td><p>The Fifth Season</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/All_of_Me_(胡琳專輯).md" title="wikilink">All of Me</a></p></td>
</tr>
<tr class="odd">
<td><p>放空</p></td>
</tr>
<tr class="even">
<td><p><strong>2016年</strong></p></td>
</tr>
<tr class="odd">
<td><p>Body n' Soul Concert</p></td>
</tr>
<tr class="even">
<td><p>人魚之歌<br />
（Body and Soul Live Version）</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Moments_(胡琳專輯).md" title="wikilink">Moments</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2017年</strong></p></td>
</tr>
<tr class="even">
<td><p>Moments</p></td>
</tr>
<tr class="odd">
<td><p><strong>2018年</strong></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>Moments</p></td>
</tr>
<tr class="even">
<td><p>Moments</p></td>
</tr>
</tbody>
</table>

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

  - （\*）表示仍在榜上

### 音樂專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/Love_Notes.md" title="wikilink">Love Notes</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>2006年5月4日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>自由像-{祢}-</li>
<li>手·心</li>
<li>一杯新鮮的愛（Pop Jazz Version）</li>
<li>小心輕放</li>
<li>下不為例</li>
<li>害怕回家</li>
<li>煙灰</li>
<li>什麼不同</li>
<li>一杯新鮮的愛（Ballad Version）</li>
<li>喵喵（國語）</li>
<li>翅膀（國語）</li>
<li>The Girl From Ipanema</li>
<li>Over The Rainbow</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>一杯新鮮的愛 MV</li>
<li>小心輕放 MV</li>
<li>害怕回家 MV</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/Sings_Timeless.md" title="wikilink">Sings Timeless</a><small><strong>（翻唱專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>2006年7月25日</p></td>
<td style="text-align: left;"><ol>
<li>Our Love Is Here To Stay</li>
<li>Windmills Of Your Mind</li>
<li>I've Got You Under My Skin</li>
<li>不了情</li>
<li>The Girl From Ipanema</li>
<li>Let's fall In Love</li>
<li>Autumn Leaves</li>
<li>They Can't Take That Away From Me</li>
<li>The Very Thought Of You</li>
<li>I Remember You</li>
<li>Over The Rainbow</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/Love_Notes_+_Sings_Timeless.md" title="wikilink">Love Notes + Sings Timeless</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>2006年7月25日</p></td>
<td style="text-align: left;"><p><strong>CD: Sings Timeless</strong></p>
<ol>
<li>Our Love Is Here To Stay</li>
<li>Windmills Of Your Mind</li>
<li>I've Got You Under My Skin</li>
<li>不了情</li>
<li>The Girl From Ipanema</li>
<li>Let's fall In Love</li>
<li>Autumn Leaves</li>
<li>They Can't Take That Away From Me</li>
<li>The Very Thought Of You</li>
<li>I Remember You</li>
<li>Over The Rainbow</li>
</ol>
<p><strong>CD: Sings Timeless</strong></p>
<ol>
<li>自由像-{祢}-</li>
<li>手·心</li>
<li>一杯新鮮的愛（Pop Jazz Version）</li>
<li>小心輕放</li>
<li>下不為例</li>
<li>害怕回家</li>
<li>煙灰</li>
<li>什麼不同</li>
<li>一杯新鮮的愛（Ballad Version）</li>
<li>喵喵（國語）</li>
<li>翅膀（國語）</li>
<li>The Girl From Ipanema</li>
<li>Over The Rainbow</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>一杯新鮮的愛 MV</li>
<li>小心輕放 MV</li>
<li>害怕回家 MV</li>
</ol></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p><a href="../Page/綠野聲踪.md" title="wikilink">綠野聲-{踪}-</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/愛貝克思.md" title="wikilink">Avex Asia</a></p></td>
<td style="text-align: left;"><p>2006年12月4日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>綠野聲-{踪}-</li>
<li>一輩子的寶貝</li>
<li>情人哲學</li>
<li>Have Yourself a Merry Little Christmas</li>
<li>人人美麗</li>
<li>因爲愛你</li>
<li>認錯</li>
<li>手·心</li>
<li>自由像-{祢}-</li>
<li>怕醜</li>
<li>一杯新鮮的愛（Pop Jazz Version）</li>
<li>認錯（Jazz Version）</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>綠野聲-{踪}- MV</li>
<li>認錯 MV</li>
<li>一杯新鮮的愛 MV</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p><a href="../Page/Feel_So_Good_(胡琳專輯).md" title="wikilink">Feel So Good</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/東亞唱片_(集團).md" title="wikilink">東亞唱片</a></p></td>
<td style="text-align: left;"><p>2007年9月13日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>人魚之歌（Acoustic Version）</li>
<li>間尺</li>
<li>趁墟</li>
<li>快樂頌</li>
<li>唞唞</li>
<li>他</li>
<li>人魚之歌（Soul Version）</li>
<li>我願意（Live 現場版）</li>
<li>Don't know why（Live 現場版）</li>
<li>春風吹 x Lovin' you（Live 現場版）</li>
<li>Light my fire（Live 現場版）</li>
<li>Can't give you anything but my love（Live 現場版）</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>趁墟 MV</li>
<li>人魚之歌 MV</li>
<li>他 MV</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>5th</p></td>
<td style="text-align: left;"><p><a href="../Page/Still..._A_Wonderful_World.md" title="wikilink">Still... A Wonderful World</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/發記唱片.md" title="wikilink">發記唱片</a></p></td>
<td style="text-align: left;"><p>2008年8月12日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>What a wonderful world</li>
<li>東風</li>
<li>一對一</li>
<li>畢加索</li>
<li>別再纏住我</li>
<li>一個人</li>
<li>Mr. So</li>
<li>香薰</li>
<li>I remember you</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>畢加索</li>
<li>一個人</li>
<li>東風</li>
<li>香薰</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>6th</p></td>
<td style="text-align: left;"><p><a href="../Page/Jazz_Them_Up.md" title="wikilink">Jazz Them Up</a><small><strong>（翻唱專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/BMA_Records.md" title="wikilink">BMA Records</a></p></td>
<td style="text-align: left;"><p>2010年8月10日</p></td>
<td style="text-align: left;"><ol>
<li>我是不是該安靜的走開</li>
<li>暗裡著迷</li>
<li>Oh!夜</li>
<li>忘情水</li>
<li>你是我的女人</li>
<li>頭髮亂了</li>
<li>哪有一天不想你</li>
<li>他的女人</li>
<li>每天愛你多一些</li>
<li>我為何讓你走</li>
<li>我是不是該安靜的走開（Reverb）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>7th</p></td>
<td style="text-align: left;"><p><a href="../Page/Déjà_vu_夢空間.md" title="wikilink">Déjà vu 夢空間</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/BMA_Records.md" title="wikilink">BMA Records</a></p></td>
<td style="text-align: left;"><p>2012年6月28日</p></td>
<td style="text-align: left;"><ol>
<li>白鍵上的黑鍵</li>
<li>再見愛</li>
<li>睜開眼</li>
<li>每一次想念</li>
<li>Déjà Vu</li>
<li>不被探險的世界</li>
<li>現在我都懂了</li>
<li>雨停</li>
<li>尋找旅程</li>
<li>巴厘時間</li>
<li>Bend Or Break</li>
<li>House of the Rising Sun</li>
<li>天窗</li>
<li>醒來</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>8th</p></td>
<td style="text-align: left;"><p><a href="../Page/Sings_Tess.md" title="wikilink">Sings Tess</a><small><strong>（翻唱專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/BMA_Records.md" title="wikilink">BMA Records</a></p></td>
<td style="text-align: left;"><p>2012年11月9日</p></td>
<td style="text-align: left;"><ol>
<li>我只在乎你</li>
<li>遇見舊情人</li>
<li>夜來香</li>
<li>千言萬語</li>
<li>難忘的初戀情人</li>
<li>小村之戀</li>
<li>雪中情</li>
<li>再見我的愛人</li>
<li>海韻</li>
<li>獨上西樓</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>9th</p></td>
<td style="text-align: left;"><p><a href="../Page/Jazz_Them_Up_Once_More.md" title="wikilink">Jazz Them Up Once More</a><br />
<small><strong>（翻唱專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/大名娛樂.md" title="wikilink">大名娛樂</a></p></td>
<td style="text-align: left;"><p>2014年1月16日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>愛不完</li>
<li>月半彎</li>
<li>敬啟者之愛是全奉獻</li>
<li>傻癡癡</li>
<li>第四晚心情</li>
<li>繼續談情</li>
<li>餓狼傳說</li>
<li>夏日傾情</li>
<li>如果這是情</li>
<li>分手總要在雨天</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>愛不完 MV</li>
<li>月半彎 MV</li>
<li>第四晚心情 MV</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>10th</p></td>
<td style="text-align: left;"><p><a href="../Page/The_Fifth_Season.md" title="wikilink">The Fifth Season</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/發記唱片.md" title="wikilink">發記唱片</a></p></td>
<td style="text-align: left;"><p>2014年11月28日</p></td>
<td style="text-align: left;"><ol>
<li>被窩</li>
<li>春眠不覺曉</li>
<li>滲</li>
<li>金色之夏</li>
<li>藍</li>
<li>一葉知秋</li>
<li>秋光乍洩</li>
<li>冬至</li>
<li>真相</li>
<li>空空</li>
<li>一葉知秋（結他版）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>11th</p></td>
<td style="text-align: left;"><p><a href="../Page/All_of_Me_(胡琳專輯).md" title="wikilink">All of Me</a></p></td>
<td style="text-align: left;"><p>新曲+精選</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a></p></td>
<td style="text-align: left;"><p>2015年8月5日</p></td>
<td style="text-align: left;"><p><strong>Disc 1</strong></p>
<ol>
<li>綠野聲-{踪}-</li>
<li>東風</li>
<li>畢加索</li>
<li>自由像袮</li>
<li>他</li>
<li>現在我都懂了</li>
<li>白鍵上的黑鍵</li>
<li>快樂頌</li>
<li>一杯新鮮的愛（Pop Jazz Version）</li>
<li>手･心</li>
</ol>
<p><strong>Disc 2</strong></p>
<ol>
<li>Mr. So</li>
<li>香薫</li>
<li>人魚之歌（Soul Version）</li>
<li>一對一</li>
<li>一個人</li>
<li>野地戀人（Live Version）</li>
<li>間尺</li>
<li>醒來</li>
<li>放空</li>
<li>思城</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>12th</p></td>
<td style="text-align: left;"><p><a href="../Page/Me_And_You.md" title="wikilink">Me And You</a><small><strong>（翻唱專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a></p></td>
<td style="text-align: left;"><p>2017年5月19日</p></td>
<td style="text-align: left;"><ol>
<li>Bye Bye Blackbird</li>
<li>Just the Two of Us</li>
<li>Mr. Magic</li>
<li>The way You make Me Feel</li>
<li>Famous blue raincoat</li>
<li>The wicked game</li>
<li>Leaving on a Jet plane（林一峰合唱）</li>
<li>It had to be You</li>
<li>Just the Way You Look Tonight</li>
<li>冷雨夜</li>
<li>愛變了這世界襯衣（林一峰合唱）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>13th</p></td>
<td style="text-align: left;"><p><a href="../Page/Moments_(胡琳專輯).md" title="wikilink">Moments</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a></p></td>
<td style="text-align: left;"><p>2018年4月6日</p></td>
<td style="text-align: left;"><ol>
<li>青玉案</li>
<li>浮生若夢</li>
<li>盡情投</li>
<li>黑白</li>
<li>赤兔</li>
<li>自遊</li>
<li>床上戲</li>
<li>一天也不能少</li>
<li>願望樹</li>
<li>時光</li>
<li>樹屋</li>
</ol></td>
</tr>
</tbody>
</table>

### 音樂會專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/Bianca_Live.md" title="wikilink">Bianca Live</a></p></td>
<td style="text-align: left;"><p>2CD+DVD</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/BMA_Records.md" title="wikilink">BMA Records</a></p></td>
<td style="text-align: left;"><p>2011年6月30日</p></td>
<td style="text-align: left;"><p><strong>CD 1</strong></p>
<ol>
<li>Overture - Band</li>
<li>東風</li>
<li>暗裡著迷</li>
<li>Fly me to the moon</li>
<li>They can't take that away from me</li>
<li>Night and Day</li>
<li>他的女人</li>
<li>味道</li>
<li>Oh!夜</li>
<li>哪有一天不想你</li>
<li>花田錯/春風吹</li>
<li>Bianca Wu monolog</li>
<li>手·心（胡琳/林一峰）</li>
<li>Autumn Leaves（林一峰）</li>
</ol>
<p><strong>CD 2</strong></p>
<ol>
<li>Intermission - Band</li>
<li>The Shadow Of Your Smile</li>
<li>I've got you under my skin</li>
<li>Kiss Me</li>
<li>一個人</li>
<li>Nobody</li>
<li>忘情水</li>
<li>Human Nature</li>
<li>野地戀人</li>
<li>我為何讓你走</li>
<li>追</li>
<li>頭髮亂了</li>
<li>月亮代表我的心</li>
<li>每天愛你多一些</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>Overture - Band</li>
<li>東風</li>
<li>暗裡著迷</li>
<li>Fly me to the moon</li>
<li>They can't take that away from me</li>
<li>Night and Day</li>
<li>他的女人</li>
<li>味道</li>
<li>Oh!夜</li>
<li>哪有一天不想你</li>
<li>花田錯/春風吹</li>
<li>Bianca Wu monolog</li>
<li>手·心（胡琳/林一峰）</li>
<li>Autumn Leaves（林一峰）</li>
<li>Intermission - Band</li>
<li>The Shadow Of Your Smile</li>
<li>I've got you under my skin</li>
<li>Kiss Me</li>
<li>一個人</li>
<li>Nobody</li>
<li>忘情水</li>
<li>Human Nature</li>
<li>野地戀人</li>
<li>我為何讓你走</li>
<li>追</li>
<li>頭髮亂了</li>
<li>月亮代表我的心</li>
<li>每天愛你多一些</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/Nice_&amp;_Easy_Concert_Live.md" title="wikilink">Nice &amp; Easy Concert Live</a></p></td>
<td style="text-align: left;"><p>2CD+DVD</p></td>
<td style="text-align: left;"><p><a href="../Page/LMC_Music.md" title="wikilink">LMC Music</a><br />
<a href="../Page/BMA_Records.md" title="wikilink">BMA Records</a></p></td>
<td style="text-align: left;"><p>2013年6月3日</p></td>
<td style="text-align: left;"><p><strong>CD 1</strong></p>
<ol>
<li>Have Yourself A Merry Little Christmas</li>
<li>醒來</li>
<li>你是我的女人</li>
<li>傷痕</li>
<li>我只在乎你</li>
<li>The end of the world</li>
<li>Nice n' easy</li>
<li>It don't mean a thing</li>
<li>Miss you night and day</li>
<li>遇見舊情人</li>
</ol>
<p><strong>CD 2</strong></p>
<ol>
<li>哪有一天不想你</li>
<li>一杯新鮮的愛</li>
<li>夜來香</li>
<li>I need to know</li>
<li>Game of love</li>
<li>東風</li>
<li>畢加索</li>
<li>綠野聲-{踪}-</li>
<li>True Colours</li>
<li>獨上西樓</li>
<li>千言萬語</li>
<li>Last Christmas</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>Have Yourself A Merry Little Christmas</li>
<li>醒來</li>
<li>你是我的女人</li>
<li>傷痕</li>
<li>我只在乎你</li>
<li>The end of the world</li>
<li>Nice n' easy</li>
<li>It don't mean a thing</li>
<li>Miss you night and day</li>
<li>遇見舊情人</li>
<li>哪有一天不想你</li>
<li>一杯新鮮的愛</li>
<li>夜來香</li>
<li>I need to know</li>
<li>Game of love</li>
<li>東風</li>
<li>畢加索</li>
<li>綠野聲-{踪}-</li>
<li>True Colours</li>
<li>獨上西樓</li>
<li>千言萬語</li>
<li>Last Christmas</li>
</ol></td>
</tr>
</tbody>
</table>

### 演唱會

  - 2008年：胡琳 Wonderful World Concert
    （11月16日）（[九龍灣國際展貿中心](../Page/九龍灣國際展貿中心.md "wikilink")）
  - 2011年：Bianca LIVE\! with the New York Jazz
    Cats（1月8日及9日）（[香港演藝學院](../Page/香港演藝學院.md "wikilink")）
  - 2012年：[Philips](../Page/Philips.md "wikilink") Fidelio presents: 胡琳
    Bianca Wu Nice& Easy Concert with the New York Jazz
    Cats（12月18日及19日）（[香港文化中心](../Page/香港文化中心.md "wikilink")）
  - 2015年：胡琳Bianca Wu: Body and Soul 演唱會
    （12月18日、19、20日）（[香港演藝學院](../Page/香港演藝學院.md "wikilink")）
  - 2018年：[Audi](../Page/Audi.md "wikilink") Hong Kong 呈獻：胡琳 shining
    moment演唱會2018（5月26日及27日）（[九龍灣國際展貿中心](../Page/九龍灣國際展貿中心.md "wikilink")）

### 音樂劇

2008年

  - [綠野仙蹤](../Page/綠野仙蹤.md "wikilink") 飾 Dorothy（女主角）
  - [樹盟](../Page/樹盟.md "wikilink") 飾
    [長平公主](../Page/長平公主.md "wikilink")（女主角）

2017年

  - 《[風雲5D](../Page/風雲5D.md "wikilink")》 飾 楚楚（女主角）

## 電視劇

### [無綫電視](../Page/無綫電視.md "wikilink")

|        |                                      |               |        |
| ------ | ------------------------------------ | ------------- | ------ |
| **首播** | **劇名**                               | **角色**        | **性質** |
| 2017年  | [溏心風暴3](../Page/溏心風暴3.md "wikilink") | Daphne        | 女配角    |
| 未播映    | [解決師](../Page/解決師.md "wikilink")     | 關芷林（Dr. Kwan） | 女配角    |

## 獎項

### 2006年度

  - [新城勁爆頒獎禮2006](../Page/2006年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆新登場女歌手
  - [RoadShow至尊音樂頒獎禮2006](../Page/2006年度RoadShow至尊音樂頒獎禮得獎名單.md "wikilink")
    - 至尊潛力新人
  - [IFPI香港唱片銷量大獎2006](../Page/IFPI香港唱片銷量大獎頒獎禮2006.md "wikilink") -
    最暢銷本地女新人

### 2007年度

  - [新城勁爆頒獎禮2007](../Page/2007年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆跳舞歌曲《趁墟》
  - [RoadShow至尊音樂頒獎禮2007](../Page/2007年度RoadShow至尊音樂頒獎禮得獎名單.md "wikilink")
    - RoadShow至尊舞台演繹《趁墟》

### 2008年度

  - [新城勁爆頒獎禮2008](../Page/2008年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆歌曲《一個人》
  - [新城勁爆頒獎禮2008](../Page/2008年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆跳舞歌曲《一個人》

### 2009年度

  - 國際華語音樂聯盟華語音樂盛典頒獎禮 - 最愛流行爵士歌手奬
  - [新城國語力頒獎禮2009](../Page/2009年度新城國語力頒獎禮得獎名單.md "wikilink") -
    新城國語力歌曲《尋愛旅程》

### 2010年度

  - [新城國語力頒獎禮2010](../Page/2010年度新城國語力頒獎禮得獎名單.md "wikilink") -
    新城國語力歌曲《我是不是該安靜的走開》
  - [新城國語力頒獎禮2010](../Page/2010年度新城國語力頒獎禮得獎名單.md "wikilink") -
    新城國語力熱爆K歌《我是不是該安靜的走開》
  - [新城勁爆頒獎禮2010](../Page/2010年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆流行爵士歌手
  - [新城勁爆頒獎禮2010](../Page/2010年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆專輯《Jazz
    Them Up Once More》

### 2011年度

  - [新城國語力頒獎禮2011](../Page/2011年度新城國語力頒獎禮得獎名單.md "wikilink") -
    新城國語力流行爵士歌手

### 2012年度

  - [新城國語力頒獎禮2012](../Page/2012年度新城國語力頒獎禮得獎名單.md "wikilink") -
    新城國語力專輯《Déjà Vu 夢空間》
  - [新城國語力頒獎禮2012](../Page/2012年度新城國語力頒獎禮得獎名單.md "wikilink") -
    新城國語力流行爵士歌手
  - [2012年度TVB8金曲榜頒獎典禮](../Page/2012年度TVB8金曲榜頒獎典禮得獎名單.md "wikilink") -
    TVB8金曲榜最佳唱作歌手獎 銅獎
  - [新城勁爆頒獎禮2012](../Page/2012年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆流行爵士專輯《Déjà Vu 夢空間》
  - [新城勁爆頒獎禮2012](../Page/2012年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆亞洲流行爵士歌手大獎

### 2014年度

  - [新城勁爆頒獎禮2014](../Page/2014年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆女歌手
  - [新城勁爆頒獎禮2014](../Page/2014年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆發燒音樂專輯《[The Fifth
    Season](../Page/The_Fifth_Season.md "wikilink")》
  - [2014年度勁歌金曲頒獎典禮](../Page/2014年度勁歌金曲得獎名單.md "wikilink") - 勁歌金曲獎《愛不完》

## 參考資料

## 外部連結

  -
  -
  -
  -
  - [大名娛樂網頁](http://www.bighonor.com.hk/artist_d.php?nid=3&id=100)

  - [B blog - Bianca](http://hk.myblog.yahoo.com/biancawuhk)（並不活躍）

[胡渭康](../Page/胡渭康.md "wikilink") {{\!}}
[Choco](../Page/Choco.md "wikilink") {{\!}} 胡琳 {{\!}}
[劉振宇](../Page/劉振宇.md "wikilink"){{\!}}
[劉凱欣](../Page/劉凱欣.md "wikilink") |group2 =
[名將音樂](../Page/名將音樂_\(香港\).md "wikilink") |list2 =
[戴夢夢](../Page/戴夢夢.md "wikilink") {{\!}}
[胡渭康](../Page/胡渭康.md "wikilink") {{\!}}
[Choco](../Page/Choco.md "wikilink") {{\!}} 胡琳 |group3 =
[廣州大名娛樂](../Page/廣州大名娛樂.md "wikilink") |list3 =
[張曼莉](../Page/張曼莉.md "wikilink") |below = }}

</center>

[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:台山人](../Category/台山人.md "wikilink")
[L琳](../Category/胡姓.md "wikilink")
[Category:女性爵士歌手](../Category/女性爵士歌手.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")

1.