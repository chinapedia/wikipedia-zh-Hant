**胡蘿蔔**（[學名](../Page/學名.md "wikilink")：），又称**紅蘿蔔**、**番蘿蔔**、**丁香蘿蔔**\[1\]、**胡芦菔金**、**赤珊瑚**、**黄根**、**甘筍**\[2\]、**金筍**\[3\]、**紅菜頭**（[臺語](../Page/臺語.md "wikilink")、[閩南語](../Page/閩南語.md "wikilink")、[潮汕话](../Page/潮汕话.md "wikilink")、[客家語](../Page/客家語.md "wikilink")）等，是[繖形科](../Page/繖形科.md "wikilink")[胡蘿蔔属](../Page/胡蘿蔔属.md "wikilink")[二年生植物](../Page/二年生植物.md "wikilink")，以呈肉質的[根作](../Page/根.md "wikilink")[蔬菜食用](../Page/蔬菜.md "wikilink")。

## 名字由来

[Daucus_carota_flowers,_peen_'Napoli'_(1).jpg](https://zh.wikipedia.org/wiki/File:Daucus_carota_flowers,_peen_'Napoli'_\(1\).jpg "fig:Daucus_carota_flowers,_peen_'Napoli'_(1).jpg")
胡萝蔔原产于[亚洲的西南部](../Page/亚洲.md "wikilink")，[阿富汗为最早](../Page/阿富汗.md "wikilink")[演化中心](../Page/演化.md "wikilink")，栽培历史在两千年以上。公元10世纪从[伊朗引入](../Page/伊朗.md "wikilink")[欧洲大陆](../Page/欧洲.md "wikilink")，15世纪见于[英国](../Page/英国.md "wikilink")，发展成欧洲生态型，尤以[地中海沿岸种植最多](../Page/地中海.md "wikilink")。16世纪传入[美洲](../Page/美洲.md "wikilink")。

约在13世纪，胡萝蔔从[伊朗引入](../Page/伊朗.md "wikilink")[中国](../Page/中国.md "wikilink")，发展成中国生态型\[4\]，根据[李时珍](../Page/李时珍.md "wikilink")《[本草纲目](../Page/本草纲目.md "wikilink")》的记载，因为从胡地传来，味道像[萝卜](../Page/萝卜.md "wikilink")，故名「胡萝蔔」。中国的胡萝蔔以[山东](../Page/山东.md "wikilink")、[河南](../Page/河南.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[云南等省份种植最多](../Page/云南.md "wikilink")。胡萝蔔于16世纪从中国传入[日本](../Page/日本.md "wikilink")，當時漢字名為「[人參](../Page/人蔘.md "wikilink")」。

## 形态

[Carrots_of_many_colors.jpg](https://zh.wikipedia.org/wiki/File:Carrots_of_many_colors.jpg "fig:Carrots_of_many_colors.jpg")
胡萝蔔为两年生草本植物，有9对[染色体](../Page/染色体.md "wikilink")，属半耐寒性长日照植物，喜冷凉气候。三回羽状全裂叶，丛生于单生的短缩茎上。顶端各着生一复伞形[花序](../Page/花序.md "wikilink")，有糙硬毛，花朵为白色或淡红色，花期5至7月。异花传粉。双悬果，果实圆卵形。肉质根有长筒、短筒、长圆锥及短圆锥等不同形状，白、黄、橙、橙红、青、紫等不同颜色。

## 栽培

胡萝蔔一般在[秋季播種冬季收获](../Page/秋.md "wikilink")，不过也可以进行反季節栽培，也就是春天播種夏天收获。主要是看所在地域，因地制宜。胡萝蔔肉质根在[摄氏温标](../Page/摄氏温标.md "wikilink")时发育良好，发芽温度为摄氏温标。由于胡萝蔔[根系发达](../Page/根.md "wikilink")，保持土质疏松对于胡萝蔔的生长有利，适宜种植在沙质土壤之中。栽培的前期不能過量施水，以防地面部分生长过度。后期则要保证供水充足以使肉质根能够充分发育\[5\]。优秀的胡萝蔔栽培种在合适条件下，最高[亩产可达](../Page/亩.md "wikilink")6000[公斤](../Page/公斤.md "wikilink")（每[公顷](../Page/公顷.md "wikilink")90[吨](../Page/吨.md "wikilink")）其常见病虫害包括黑斑病、\[6\]\[7\]\[8\]、根结[线虫病](../Page/线虫.md "wikilink")、\[9\]\[10\]\[11\]\[12\]\[13\]、胡萝蔔花叶病等\[14\]。

## 營養

[缩略图](https://zh.wikipedia.org/wiki/File:How_to_cut_a_carrot.webm "fig:缩略图")

胡萝蔔[营养丰富](../Page/营养.md "wikilink")。人们一般食用其肉质根，有时也食用胡萝蔔叶。胡萝蔔种子内含有挥发性油。胡萝蔔根可以直接生食，也可加工成块、丁、丝同其他食材一同烹饪。胡萝蔔汁也是一种常见的胡萝蔔加工制成品，胡萝蔔汁或胡萝蔔泥也可和面粉或米粉等淀粉类物质混合做成糕饼类食物\[15\]。还可[醃制](../Page/醃.md "wikilink")、[醬渍](../Page/醬.md "wikilink")、制干或作[饲料](../Page/饲料.md "wikilink")。其风味主要来自于[萜烯类物质](../Page/萜烯.md "wikilink")，该物质味道较为独特，并非所有人都能接受。

胡萝蔔的营养成分中，最重要的就是因其得名的[胡蘿蔔素](../Page/胡蘿蔔素.md "wikilink")，胡萝蔔根内含有[α](../Page/Α-胡萝卜素.md "wikilink")、[β](../Page/Β-胡萝卜素.md "wikilink")（大多藏在外皮）、γ、ε-胡萝蔔素和番茄烃、六氢番茄烃等[類胡蘿蔔素](../Page/類胡蘿蔔素.md "wikilink")，胡萝蔔素有治疗[夜盲症](../Page/夜盲症.md "wikilink")、保护[呼吸道和促进儿童生长等功能](../Page/呼吸道.md "wikilink")。此外，胡萝蔔还含较多的维生素，[钙](../Page/钙.md "wikilink")、[磷](../Page/磷.md "wikilink")、[铁等](../Page/铁.md "wikilink")[矿物质](../Page/矿物质.md "wikilink")，淀粉、[纤维素等糖类物质](../Page/纤维素.md "wikilink")。

## 文化

有些时候，胡萝蔔被视作是赏赐的代名词。\[16\]如政治用语[胡蘿蔔加大棒](../Page/胡蘿蔔加大棒.md "wikilink")，意思为赏赐与威逼共同使用，以使对手同意自己的要求。在流行文化中（原来在西方最普遍），胡萝蔔被视为[兔子最喜欢的食物](../Page/兔.md "wikilink")（但其實是錯誤的），这一组合如同[老鼠与](../Page/鼠.md "wikilink")[奶酪一样](../Page/乾酪.md "wikilink")，经常用在[寓言](../Page/寓言.md "wikilink")、[笑話甚至](../Page/笑話.md "wikilink")[管理学中](../Page/管理学.md "wikilink")。

## 不同名稱的使用

在香港不同種的紅蘿蔔會分別命名為紅蘿蔔或甘筍。紅蘿蔔個子比較大，纖維較粗，生食時口感差，適合用來煲湯；反之甘筍個子較小，纖維比較幼細，可供生食，如做沙律，但卻不宜煲湯。

## 參考來源

### 注释

### 書目

  -
### 資料庫

  -
  -
  -
  -
## 外部連結

  - \-{zh-hans:; zh-hant:;}-

[Category:可食用伞形科](../Category/可食用伞形科.md "wikilink")
[Category:植物与传粉者](../Category/植物与传粉者.md "wikilink")
[Category:根菜類](../Category/根菜類.md "wikilink")

1.
2.  香港區分不同種胡蘿蔔
3.  [《海南植物志》](http://gjk.scib.ac.cn/hnzwz/hnzwzfile1-4/3-182.pdf)
4.
5.  [胡萝蔔栽培技术](http://shucai.ag365.com/shucai/huluobo/2006/2006051947236.html)
6.
7.
8.
9.
10.
11. [軟腐病 -
    植物保護圖鑑系列 6](https://www.baphiq.gov.tw/Publish/plant_protect_pic_6/orchidPDF/02-6.pdf)
12.
13.
14. [胡萝蔔主要病害及其防治技术](http://shucai.ag365.com/shucai/huluobo/2006/2006051947532.html)
15. [胡萝蔔饼——韩式风味](http://www.xiaochu.cn/html/caipu/shijie/hanguoliaoli/2009/0824/5561.html)

16.