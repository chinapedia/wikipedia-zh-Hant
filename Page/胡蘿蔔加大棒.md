[Carrot_and_stick.svg](https://zh.wikipedia.org/wiki/File:Carrot_and_stick.svg "fig:Carrot_and_stick.svg")
**胡蘿蔔加大棒**（****），或譯**紅蘿蔔加巨棒**，是[美國](../Page/美國.md "wikilink")[總統](../Page/美國總統.md "wikilink")[西奥多·罗斯福](../Page/西奥多·罗斯福.md "wikilink")（老羅斯福）的一種觀點。是一種以“獎勵”（[胡蘿蔔](../Page/胡蘿蔔.md "wikilink")）與“懲罰”（“[大棒政策](../Page/大棒政策.md "wikilink")”），同時進行的一種策略。一個流行的說法是，老羅斯福的這句名言。據此，[美國政府多次干涉](../Page/美國政府.md "wikilink")[拉丁美洲](../Page/拉丁美洲.md "wikilink")[國家的](../Page/國家.md "wikilink")[內政問題](../Page/內政.md "wikilink")。

老羅斯福在1901年參觀[明尼蘇達州州博覽會時演說](../Page/明尼蘇達州州博覽會.md "wikilink")：「温言在口，大棒在手，你可以走得更遠。」\[1\]成為一句名言，後被改編為「紅蘿蔔加巨棒」。

世界上有很多知名政治家有同類型的名句，亦稱**「[獨裁者的恩威並用](../Page/獨裁者.md "wikilink")」**，如[德意志帝國的](../Page/德意志帝國.md "wikilink")「鐵血宰相」[俾斯麥稱](../Page/俾斯麥.md "wikilink")「**麵包與鞭**」，[臺灣日治時期](../Page/臺灣日治時期.md "wikilink")，[臺灣總督府](../Page/臺灣總督府.md "wikilink")[民政長官](../Page/民政長官.md "wikilink")[後藤新平則把此策略稱為](../Page/後藤新平.md "wikilink")「**糖飴與鞭**」，一面以重罰整治[異議人士](../Page/異議人士.md "wikilink")，另一面用心於臺灣內政，同時展現出威信與親和力。\[2\]

“胡蘿蔔加大棒”一詞最早在1948年12月11日《[经济学人](../Page/经济学人.md "wikilink")》發表，後收錄於《[牛津英語詞典](../Page/牛津英語詞典.md "wikilink")》增訂版，附圖有一隻[驢和胡蘿蔔](../Page/驢.md "wikilink")。

## 參見

  - [門羅主義](../Page/門羅主義.md "wikilink")

## 外部連結

  - Paul Brians, Department of English, Washington State University
    [“Carrot on a stick” vs. “the carrot or the
    stick.”](https://web.archive.org/web/20071010000915/http://www.wsu.edu/~brians/errors/carrot.html)

[Category:外交政策](../Category/外交政策.md "wikilink")

1.
2.