**胡蜂亞科**是[胡蜂科中的一類](../Page/胡蜂科.md "wikilink")，是以[女王蜂為中心的](../Page/女王蜂.md "wikilink")[真社會性](../Page/真社會性.md "wikilink")（亦作[完全社會性](../Page/完全社會性.md "wikilink")）[昆蟲](../Page/昆蟲.md "wikilink")，是這些昆蟲當中數量最大和最為人所知，當中包括了[黃蜂](../Page/黃蜂屬.md "wikilink")、[黑胡蜂及](../Page/黑胡蜂屬.md "wikilink")[虎頭蜂三大家族](../Page/虎頭蜂屬.md "wikilink")。
另一種比較少人認識的[夜胡蜂](../Page/夜胡蜂屬.md "wikilink")，生活於[東南亞](../Page/東南亞.md "wikilink")，是一種[夜行動物](../Page/夜行動物.md "wikilink")。
[古胡蜂屬是本亞科的一個已滅絕品種](../Page/古胡蜂屬.md "wikilink")，出現於[北美洲](../Page/北美洲.md "wikilink")[科羅拉多州的](../Page/科羅拉多州.md "wikilink")[始新世](../Page/始新世.md "wikilink")[化石記錄](../Page/化石.md "wikilink")。
整體來說，全球除了[南極洲以外](../Page/南極洲.md "wikilink")，皆可找到本亞科物種的踪影。一方面當然因為本亞科的多個屬都是[入侵性很強的物種](../Page/入侵物种.md "wikilink")，一旦把牠們引入牠們生活的範圍以外，很快就會成為重要的蟲患；二來本物種的天敵亦比較少：為了防範[入侵者而演化出有毒的](../Page/入侵者.md "wikilink")[尾針](../Page/尾針.md "wikilink")，螫人時會產生刺痛感，也有人因為對[毒液中含有的](../Page/毒液.md "wikilink")[毒蛋白](../Page/毒蛋白.md "wikilink")[過敏而](../Page/過敏.md "wikilink")[休克](../Page/休克.md "wikilink")[死亡](../Page/死亡.md "wikilink")。
[Palaeovespa_florissantia.jpg](https://zh.wikipedia.org/wiki/File:Palaeovespa_florissantia.jpg "fig:Palaeovespa_florissantia.jpg")的一個物種。\]\]

## 參考資料

## 外部連結

[Category:Biological pest control
wasps](../Category/Biological_pest_control_wasps.md "wikilink")
[胡蜂亞科](../Category/胡蜂亞科.md "wikilink")
[Category:有毒昆蟲](../Category/有毒昆蟲.md "wikilink")