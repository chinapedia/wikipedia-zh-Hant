**胡錦**（），[香港早期](../Page/香港.md "wikilink")[華語片](../Page/華語片.md "wikilink")[艷星](../Page/艷星.md "wikilink")，代表作出演[李翰祥系列](../Page/李翰祥.md "wikilink")[风月片](../Page/色情片.md "wikilink")，《[風流韻事](../Page/風流韻事.md "wikilink")》、《[捉奸趣事](../Page/捉奸趣事.md "wikilink")》、《[金瓶雙艷](../Page/金瓶雙艷.md "wikilink")》、《[軍閥趣史](../Page/軍閥趣史.md "wikilink")》等百餘片電影。

## 生平

胡錦生於[江西](../Page/江西.md "wikilink")，自小跟母親[馬驪珠學](../Page/馬驪珠.md "wikilink")[京戲](../Page/京戲.md "wikilink")，演過[電視劇及](../Page/電視劇.md "wikilink")[電視](../Page/電視.md "wikilink")[平劇等](../Page/平劇.md "wikilink")。[中國文化大學](../Page/中國文化大學.md "wikilink")[家政系肄業後](../Page/家政.md "wikilink")，1969年，被[李翰祥](../Page/李翰祥.md "wikilink")[導演引導投入](../Page/導演.md "wikilink")[電影](../Page/電影.md "wikilink")，在[邵氏兄弟](../Page/邵氏兄弟.md "wikilink")10多年，拍攝有過百部[邵氏電影](../Page/邵氏電影.md "wikilink")。
\[1\]\[2\]

1974年，胡錦以電影《雪花片片》（）榮獲第二十屆[亞洲影展演技優異女主角獎](../Page/亞洲影展.md "wikilink")，參加過不少大型[舞台劇例如](../Page/舞台劇.md "wikilink")《[遊園驚夢](../Page/遊園驚夢.md "wikilink")》、《[蝴蝶夢](../Page/蝴蝶夢.md "wikilink")》、[白先勇作品和音樂劇](../Page/白先勇.md "wikilink")《[梁祝](../Page/梁祝.md "wikilink")40》。

1990年，胡錦轉職幕後，擔任電視製作人，製作《[芙蓉鎮](../Page/芙蓉鎮_\(電視劇\).md "wikilink")》、《[愛在他鄉](../Page/愛在他鄉_\(中視電視劇\).md "wikilink")》、《[紙婚](../Page/紙婚.md "wikilink")》等膾炙人心的連續劇。胡錦的胞弟[胡鈞](../Page/胡鈞_\(台灣演員\).md "wikilink")、[胡銘亦為演員](../Page/胡銘.md "wikilink")。

## 近況

胡錦旅居[美國](../Page/美國.md "wikilink")[舊金山](../Page/舊金山.md "wikilink")，其夫婿是前[臺灣電視公司副總經理](../Page/臺灣電視公司.md "wikilink")[顧安生](../Page/顧安生.md "wikilink")。1999年發現得患[乳癌](../Page/乳癌.md "wikilink")，成為[美國防癌協會](../Page/美國防癌協會.md "wikilink")[義工](../Page/義工.md "wikilink")；同期，策劃[紐約](../Page/紐約.md "wikilink")、[舊金山](../Page/舊金山.md "wikilink")、[洛杉磯等地之跨世紀](../Page/洛杉磯.md "wikilink")[慈善](../Page/慈善.md "wikilink")[晚會](../Page/音樂會.md "wikilink")。

在2001年至2003年期間，與[凌波舉行多場](../Page/凌波.md "wikilink")「[梁祝](../Page/梁祝.md "wikilink")」[音樂](../Page/音樂.md "wikilink")[演唱會](../Page/演唱會.md "wikilink")。2005年7月22日，於[台灣北中南開始舉行](../Page/台灣.md "wikilink")「梁祝」音樂劇及[黃梅調精選演唱會](../Page/黃梅調.md "wikilink")。

2011年7月底，於[台北](../Page/台北.md "wikilink")[國父紀念館參與](../Page/國父紀念館.md "wikilink")[白先勇短篇小說所改編的舞臺劇](../Page/白先勇.md "wikilink")《[遊園驚夢](../Page/遊園驚夢.md "wikilink")》演出，飾演「天辣椒」蔣碧月。

## 作品

  - 電影參演

<!-- end list -->

  - 《[風流韻事](../Page/風流韻事.md "wikilink")》(1973年)
  - 《[七十二家房客](../Page/七十二家房客_\(1973年電影\).md "wikilink")》(1973年)
  - 《[聲色犬馬](../Page/聲色犬馬.md "wikilink")》(1974年)
  - 《[金瓶雙艷](../Page/金瓶雙艷.md "wikilink")》飾[潘金蓮](../Page/潘金蓮.md "wikilink")(1974年)
  - 《[煙雨](../Page/煙雨.md "wikilink")》(1975年)
  - 《[捉姦趣事](../Page/捉姦趣事.md "wikilink")》(1975年)
  - 《[金玉良缘红楼梦](../Page/金玉良缘红楼梦.md "wikilink")》飾[王熙凤](../Page/王熙凤.md "wikilink")(1977年)
  - 《[鬼叫春](../Page/鬼叫春.md "wikilink")》(1979年)
  - 《[軍閥趣史](../Page/軍閥趣史.md "wikilink")》(1979年)

<!-- end list -->

  - 電視劇參演

<!-- end list -->

  - 台灣電視公司1983年連續劇《[比翼雙雙飛](../Page/比翼雙雙飛.md "wikilink")》
  - [中國電視公司](../Page/中國電視公司.md "wikilink")1984年連續劇《[鹿鼎記](../Page/鹿鼎記.md "wikilink")》飾毛東珠、太-{后}-
  - 中國電視公司1985年連續劇《[牽情](../Page/牽情.md "wikilink")》
  - 中國電視公司1986年連續劇《[揚子江風雲](../Page/揚子江風雲.md "wikilink")》飾卓寡婦
  - 中國電視公司1987年連續劇《[珍珠傳奇](../Page/珍珠傳奇.md "wikilink")》飾[楊貴妃](../Page/楊貴妃.md "wikilink")
  - 中國電視公司1987年連續劇《[靈山神箭](../Page/靈山神箭.md "wikilink")》飾丁羞花
  - 中國電視公司1987年連續劇《[長江一號](../Page/長江一號.md "wikilink")》飾卓秀珍
  - 中國電視公司1992年連續劇《[財神爺報到](../Page/財神爺報到.md "wikilink")》飾精靈王
  - 中國電視公司1994年連續劇《[楊乃武與小白菜](../Page/楊乃武與小白菜_\(1994年電視劇\).md "wikilink")》飾粉菊花
  - 中華電視公司1994年連續劇《[七俠五義](../Page/七俠五義.md "wikilink")》飾媒婆

<!-- end list -->

  - 製作人

<!-- end list -->

  - 台灣電視公司1989年連續劇《[芙蓉鎮](../Page/芙蓉鎮_\(電視劇\).md "wikilink")》
  - 台灣電視公司1990年連續劇《[龍城風雲](../Page/龍城風雲.md "wikilink")》
  - 台灣電視公司1994年連續劇《[歲月的眼睛](../Page/歲月的眼睛.md "wikilink")》
  - 中國電視公司1995年連續劇《[愛在他鄉](../Page/愛在他鄉_\(中視電視劇\).md "wikilink")》

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -
  -
  -
  -
  -
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:亞太影展獲獎者](../Category/亞太影展獲獎者.md "wikilink")
[Category:台灣電視製作人](../Category/台灣電視製作人.md "wikilink")
[Category:江西人](../Category/江西人.md "wikilink")
[J](../Category/胡姓.md "wikilink")
[Category:中國文化大學校友](../Category/中國文化大學校友.md "wikilink")

1.  [胡錦主演的《金瓶雙艷》](http://paper.wenweipo.com/2007/04/18/RW0704180001.htm)，
    [香港電影金像獎主席文雋](../Page/香港電影金像獎.md "wikilink")[流鼻血](../Page/流鼻血.md "wikilink")
2.  [香港邵氏著名艷星胡錦](http://www.hq.xinhuanet.com/audio/2005-02/22/content_3756165.htm)
     - [新華網](../Page/新華網.md "wikilink")