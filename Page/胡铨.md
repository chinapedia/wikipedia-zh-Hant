[_Tianya_Haijiao_094.JPG](https://zh.wikipedia.org/wiki/File:_Tianya_Haijiao_094.JPG "fig:_Tianya_Haijiao_094.JPG")[天涯海角](../Page/天涯海角.md "wikilink")。\]\]
**胡铨**（），[字](../Page/表字.md "wikilink")**邦衡**，[号](../Page/号.md "wikilink")**澹庵**，[諡号](../Page/諡号.md "wikilink")**忠简**，[吉州](../Page/吉州.md "wikilink")[庐陵](../Page/庐陵.md "wikilink")（今[江西](../Page/江西.md "wikilink")[吉安](../Page/吉安市.md "wikilink")）人。[南宋政治家](../Page/南宋.md "wikilink")、文學家。與[歐陽修](../Page/歐陽修.md "wikilink")、[楊邦乂](../Page/楊邦乂.md "wikilink")、[周必大](../Page/周必大.md "wikilink")、[楊萬里](../Page/楊萬里.md "wikilink")、[文天祥](../Page/文天祥.md "wikilink")，合稱廬陵「五忠一節」。\[1\]與[李綱](../Page/李綱.md "wikilink")、[趙鼎](../Page/趙鼎.md "wikilink")、[李光並稱](../Page/李光.md "wikilink")「南宋四名臣」。

## 生平

北宋建中元年(1101年)，六月初三日生於宋吉州廬陵薌城(今江西省吉安市青原區值夏鎮道院)。自幼聰慧，隨[蕭楚習讀](../Page/蕭楚.md "wikilink")《[春秋](../Page/春秋.md "wikilink")》。

南宋[建炎二年](../Page/建炎.md "wikilink")（1128年），[進士及第](../Page/進士.md "wikilink")，该科由高宗亲自策试，胡铨以万言策上，甚为轰动。紹興五年(1135年)[金太宗死](../Page/金太宗.md "wikilink")，[金國內部調整](../Page/金國.md "wikilink")，宋高宗重用[秦檜](../Page/秦檜.md "wikilink")，一再主張議和。紹興七年（1137年）在[枢密院编修官任上](../Page/枢密院.md "wikilink")，反对与[金朝议和](../Page/金朝.md "wikilink")，上书请斩主和派[秦桧](../Page/秦桧.md "wikilink")、[王倫](../Page/王倫.md "wikilink")、[孫近三奸臣](../Page/孫近.md "wikilink")，停止「和議」，北上抗金。他說：「奈何以祖宗之天下為金虜之天下，以祖宗之位為金虜藩臣之位！」「此膝一屈不可復伸，國勢陵夷不可復振」，「臣備員樞屬，義不與檜等共載天，區區之心，願斷三人頭，竿之藁街」，「不然，臣有赴東海而死，寧能處小朝廷求活耶？」\[2\]秦檜見書，即以“狂妄上书，语多凶悖，意在鼓众，劫持朝廷”之罪，欲將胡銓除名[編管](../Page/編管.md "wikilink")（類似開除[公職](../Page/公職.md "wikilink")、下放[勞動改造](../Page/勞動改造.md "wikilink")），朝中大臣多救之，秦檜迫於公論，才未除名而貶官於邊遠地區，輾轉任[福州簽判](../Page/福州.md "wikilink")。但胡詮反對的這次和議，事實上讓[南宋收回了](../Page/南宋.md "wikilink")[陝西](../Page/陝西.md "wikilink")、[河南等失土](../Page/河南.md "wikilink")，胡銓的言論近於偏激迂腐。绍兴十二年（1142年）最終的[紹興和議時](../Page/紹興和議.md "wikilink")，胡铨又在[广州再次上书谴责秦桧](../Page/广州.md "wikilink")，以“饰非横议”之名编管新州（今[广东](../Page/广东.md "wikilink")[新兴县](../Page/新兴县.md "wikilink")）。绍兴十八年（1148年）再谪吉阳军（今[海南](../Page/海南.md "wikilink")[崖县](../Page/崖县.md "wikilink")）。绍兴二十五年（1155年）秦桧死后，方得以复官。

紹興三十二年（1162年），六月高宗趙構傳位太子趙昚，胡銓復職奉議郎、知饒州。帝召他入對，胡銓上奏修德、結民、練兵、觀釁四事。帝說：「朕很早就聽說你耿直誠實。」因任命他為吏部郎官。

隆興元年（1163年），遷任秘書少監、擢任起居郎。針對自隆興北伐失敗後朝中的議和之風，多次上表予以反對。後历任国史院编修、工部员外郎、权兵部侍郎、[端明殿学士等职](../Page/端明殿.md "wikilink")，积极反对与金朝议和，并曾一度亲自领兵抗金。[乾道七年](../Page/乾道.md "wikilink")（1171年），任宝文阁待制，留经筵。後辞官回乡，

[淳熙七年庚子](../Page/淳熙.md "wikilink")（1180年），五月月二十九日卒于故里，卒諡忠簡。

## 著作

著有《澹庵集》一百卷，已散佚，现存词作不多，多为慷慨激昂之语，开[辛弃疾等人之先河](../Page/辛弃疾.md "wikilink")。后世多景仰其事迹，多有胡氏家族尊其为始祖。

## 傳記

《宋史·卷三百七十四·列傳第一百三十三》

## 注釋

[H](../Category/宋朝詞人.md "wikilink")
[H](../Category/宋朝政治人物.md "wikilink")
[Q](../Category/胡姓.md "wikilink")
[Category:吉安人](../Category/吉安人.md "wikilink")

1.  因六人出身廬陵(吉安)，諡號分別為文忠、忠襄、忠簡、文節、文烈。
2.  《宋史》卷三七四《胡銓傳》、《[古文觀止](../Page/古文觀止.md "wikilink")》收錄為〈[戊午上高宗封事](../Page/s:戊午上高宗封事.md "wikilink")〉