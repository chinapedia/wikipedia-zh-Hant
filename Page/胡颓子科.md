**胡颓子科**共有3[属约](../Page/属.md "wikilink")50余[种](../Page/种.md "wikilink")，主要分布在北半球[温带和](../Page/温带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")。[中国有](../Page/中国.md "wikilink")2属41种。

本[科植物有](../Page/科.md "wikilink")[乔木也有](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，枝端和叶被有银色或金褐色的盾状鳞片或星状毛，单[叶互生或对生](../Page/叶.md "wikilink")，无托叶；[花两性或单性](../Page/花.md "wikilink")，无[花瓣](../Page/花瓣.md "wikilink")，花被2-4裂；[果实为瘦果或坚果](../Page/果实.md "wikilink")，包藏在肉质花被中。大部分品种是耐干旱[植物](../Page/植物.md "wikilink")，但也有部分品种耐潮湿的气候。其中[沙棘](../Page/沙棘.md "wikilink")（*Hippophae
rhamnoidese* L.）是优良的固沙植物品种。

1981年的[克朗奎斯特分类法将本科分入](../Page/克朗奎斯特分类法.md "wikilink")[山龙眼目](../Page/山龙眼目.md "wikilink")，2003年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
II
分类法将其列入](../Page/APG_II_分类法.md "wikilink")[蔷薇目](../Page/蔷薇目.md "wikilink")。

## 参考文献

## 外部链接

  - [胡颓子科分类](https://web.archive.org/web/20070208174956/http://www.cc.jyu.fi/~pamakine/biologia/elaeagnaceae/)

[\*](../Category/胡颓子科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")