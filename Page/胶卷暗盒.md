[Filmcassettes.jpg](https://zh.wikipedia.org/wiki/File:Filmcassettes.jpg "fig:Filmcassettes.jpg")、[爱迪飒](../Page/爱迪飒.md "wikilink")、[密诺斯](../Page/密诺斯.md "wikilink")\]\]
[Minox_film_cassette.JPG](https://zh.wikipedia.org/wiki/File:Minox_film_cassette.JPG "fig:Minox_film_cassette.JPG")
[Steky_cassettes.jpg](https://zh.wikipedia.org/wiki/File:Steky_cassettes.jpg "fig:Steky_cassettes.jpg")
[Golden_Ricoh_16_cassette.jpg](https://zh.wikipedia.org/wiki/File:Golden_Ricoh_16_cassette.jpg "fig:Golden_Ricoh_16_cassette.jpg")
[Meopta_Mikroma_cassette.jpg](https://zh.wikipedia.org/wiki/File:Meopta_Mikroma_cassette.jpg "fig:Meopta_Mikroma_cassette.jpg")
[Mikroma_cassette_with_case.jpg](https://zh.wikipedia.org/wiki/File:Mikroma_cassette_with_case.jpg "fig:Mikroma_cassette_with_case.jpg")
[Photavit_cassette_vs_regular_35mm_film_cassette.jpg](https://zh.wikipedia.org/wiki/File:Photavit_cassette_vs_regular_35mm_film_cassette.jpg "fig:Photavit_cassette_vs_regular_35mm_film_cassette.jpg")
**胶卷暗盒**是能够装在[照相机上直接提供胶卷](../Page/照相机.md "wikilink")、胶片的储存器，简称[暗盒](../Page/暗盒.md "wikilink")，多用于大型照相机胶片、玻璃片，35毫米胶卷和微型摄影胶卷。暗盒有一次性和多次性两类。

在底片[冲洗后剩下的胶卷暗盒也是一種蒐藏品](../Page/冲洗.md "wikilink")，熱愛此道的蒐藏家會蒐集各種不同廠牌的暗盒或是在世界各地發行的暗盒，可以像蒐集[郵票](../Page/郵票.md "wikilink")、[錢幣一樣賞玩品評](../Page/錢幣.md "wikilink")，不過由於膠捲暗盒的種類並不像郵票、錢幣這麼多，蒐集者也屬於小眾，所以保值性也較不佳。此外，也有人蒐集膠捲暗盒來當作[積木](../Page/積木.md "wikilink")。

## 功能

暗盒的暗盒的功能有四\[1\]：

1.  保护胶卷不暴光
2.  保护胶卷不沾尘土
3.  能装入照相机中拍照时输送胶片\[2\]
4.  一卷胶片拍完后回收胶片。

## 种类和形式

暗盒有单腔腔式、双腔式两种。多数用[塑料制成](../Page/塑料.md "wikilink")，也有少数用[黄铜等金属制成](../Page/黄铜.md "wikilink")。

  - [微型相机胶卷暗盒](../Page/微型相机.md "wikilink")：
      - 一次性塑料双腔暗盒，只可使用一次，不能重覆使用：[柯达](../Page/柯达.md "wikilink")110型、[祿萊](../Page/祿萊.md "wikilink")126型。
      - 多次性塑料单腔暗盒 用于：[特熙纳TESSINA](../Page/特熙纳.md "wikilink") 35，TESSINA
        35L；[祿萊16](../Page/祿萊16.md "wikilink")、[爱迪飒16](../Page/爱迪飒.md "wikilink")、[爱迪飒16M](../Page/爱迪飒.md "wikilink")、[爱迪飒16MB](../Page/爱迪飒.md "wikilink")、Franka16、Alka16。
      - 多次性塑料双腔暗盒 [美能达](../Page/美能达.md "wikilink")（Minolta
        QT），[密诺斯](../Page/密诺斯.md "wikilink")8x11型。
      - 多次性金属双腔暗盒 Golden Ricoh
      - 多次性金属双暗盒 Steky,Meopta Mikroma
      - 多次性黄铜双腔暗盒：[密诺斯](../Page/密诺斯.md "wikilink")8x11型。
  - [先進攝影系統暗盒](../Page/先進攝影系統.md "wikilink")：一次性塑料单腔暗盒
  - [35毫米相机胶卷暗盒](../Page/35毫米相机.md "wikilink")：
      - 一次性金属单腔暗盒：用于绝大多数35毫米胶卷，不能打开，不能重复使用。
      - 多次性金属单腔暗盒：用于1970年前35毫米胶卷，能打开，因此可以重复使用，后废。今日，少数摄影器材制造商仍出售原装无胶卷在内的多次性金属单腔暗盒，以供摄影者从[长装胶卷分截成](../Page/长装胶卷.md "wikilink")24、36幅胶卷。
      - 多次性塑料单腔暗盒：供摄影者从[长装胶卷分截用](../Page/长装胶卷.md "wikilink")。

[FILCA.jpeg](https://zh.wikipedia.org/wiki/File:FILCA.jpeg "fig:FILCA.jpeg")
[Contaflex_SuperB_film_back.JPG](https://zh.wikipedia.org/wiki/File:Contaflex_SuperB_film_back.JPG "fig:Contaflex_SuperB_film_back.JPG")的可卸双暗盒\]\]

  -   - 多次性黄铜单腔[活门暗盒](../Page/活门暗盒.md "wikilink")：[莱卡螺丝卡照相机](../Page/莱卡.md "wikilink")、早期莱卡M照相机与早期[康泰时照相机有多次性黄铜单腔活门暗盒](../Page/康泰时.md "wikilink")；莱卡活门暗盒不能用于康泰时照相机，康泰时活门暗盒不能用于莱卡照相机。
      - 多次性金属双腔活门暗盒，用于早期[康泰时照相机](../Page/康泰时.md "wikilink")

  - [康泰单反照相机可卸双暗盒](../Page/康泰单反照相机.md "wikilink")

  - 大型照相机、玻璃板、[胶片暗盒](../Page/胶片暗盒.md "wikilink")。用木材或金属制造，扁长方形，有单面双面两种。

<!-- end list -->

  - [可卸暗盒](../Page/可卸暗盒.md "wikilink")
      - 大型照相机用可卸胶卷暗盒，用120胶卷，带 6x4.5、6x9、6x12厘米框。

      -
## 构造

## 参考文献

<references />

## 注释

[category:摄影](../Page/category:摄影.md "wikilink")

[Category:底片](../Category/底片.md "wikilink")

1.  Photography For Dummies p39
2.  大型照相机中胶片不移动