**能丹國家公園**，為[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）未成立的[國家公園之一](../Page/國家公園.md "wikilink")，於1998年由[埔里地方人士提議設置](../Page/埔里鎮.md "wikilink")，原本預定於2000年成立，由於受到當地[布農族人及居民強烈反彈而中止](../Page/布農族.md "wikilink")。\[1\]\[2\]\[3\]

能丹國家公園的名稱，由[中央山脈的](../Page/中央山脈.md "wikilink")[能高山](../Page/能高山.md "wikilink")、[丹大山兩大山系而來](../Page/丹大山.md "wikilink")，預定範圍位於[台灣中部的中高海拔山區](../Page/中台灣.md "wikilink")，北臨[太魯閣國家公園](../Page/太魯閣國家公園.md "wikilink")、南接[玉山國家公園](../Page/玉山國家公園.md "wikilink")，面積超過10萬公頃\[4\]。主要涵蓋[南投縣](../Page/南投縣.md "wikilink")[仁愛鄉](../Page/仁愛鄉.md "wikilink")、[信義鄉](../Page/信義鄉_\(臺灣\).md "wikilink")、[花蓮縣](../Page/花蓮縣.md "wikilink")[秀林鄉](../Page/秀林鄉.md "wikilink")、[萬榮鄉等原屬](../Page/萬榮鄉.md "wikilink")[泰雅族與](../Page/泰雅族.md "wikilink")[布農族的祖傳生存領域](../Page/布農族.md "wikilink")。其中大部分的預定範圍在2000年由[農委會林務局公告為](../Page/行政院農業委員會林務局.md "wikilink")[丹大野生動物重要棲息環境](../Page/丹大野生動物重要棲息環境.md "wikilink")，面積達109,952公頃\[5\]。

## 參考資料

[Category:未成立的台灣國家公園](../Category/未成立的台灣國家公園.md "wikilink")
[Category:南投縣地理](../Category/南投縣地理.md "wikilink")
[Category:花蓮縣地理](../Category/花蓮縣地理.md "wikilink")

1.
2.
3.
4.
5.