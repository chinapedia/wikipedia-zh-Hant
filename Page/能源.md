[Energy-production-World2.png](https://zh.wikipedia.org/wiki/File:Energy-production-World2.png "fig:Energy-production-World2.png")
[Energy_corridor_from_Niagara_Falls,_near_Buffalo_NY._IMG_0636.jpg](https://zh.wikipedia.org/wiki/File:Energy_corridor_from_Niagara_Falls,_near_Buffalo_NY._IMG_0636.jpg "fig:Energy_corridor_from_Niagara_Falls,_near_Buffalo_NY._IMG_0636.jpg")
[Gent_-_Electrabel_Rodenhuize_1.jpg](https://zh.wikipedia.org/wiki/File:Gent_-_Electrabel_Rodenhuize_1.jpg "fig:Gent_-_Electrabel_Rodenhuize_1.jpg")
[CPCC_Tamsui_Station_20070805.jpg](https://zh.wikipedia.org/wiki/File:CPCC_Tamsui_Station_20070805.jpg "fig:CPCC_Tamsui_Station_20070805.jpg")
使用**能源**通過控制和適應環境使它在人類社會裡成為一個關鍵的發展。在任何一個社會都無法避免管理能源的使用。在[工業化國家裡](../Page/工業化.md "wikilink")，能源資源的發展在[農業](../Page/農業.md "wikilink")、[運輸](../Page/運輸.md "wikilink")、[垃圾收集](../Page/垃圾收集.md "wikilink")、[信息技術和](../Page/信息技術.md "wikilink")[通訊是成為發達社會的先決條件](../Page/通訊.md "wikilink")。自從[工業革命後](../Page/工業革命.md "wikilink")，能源的使用越來越多，同時也帶來一些嚴重的問題，其中一些，如[全球暖化對目前全世界有潛在嚴重的風險](../Page/全球暖化.md "wikilink")。另外由於[經濟活動](../Page/經濟活動.md "wikilink")，如[製造業和](../Page/製造業.md "wikilink")[運輸業的密集](../Page/運輸業.md "wikilink")，能源效率﹑依賴﹑安全和價格等的問題也令人關注。

在人類社會背景下的能源資源:能源資源作為**能源**的同義詞，一般來說常指物質，例如[燃料](../Page/燃料.md "wikilink")，[石油加工產品和](../Page/石油.md "wikilink")[電力](../Page/電力.md "wikilink")。這些都是可利用的能源來源，因為它們可以很容易地轉化為其他為特定的用處種類的能源。

在自然界中，能源可以採取幾種不同的形式存在：[熱](../Page/熱.md "wikilink")，[電](../Page/電.md "wikilink")，[輻射](../Page/輻射.md "wikilink")，[化學能等](../Page/化學能.md "wikilink")。許多這些形式可以很容易轉化為另一種的幫助下，如利用裝置；從[化學能到](../Page/化學能.md "wikilink")[電能使用的](../Page/電能.md "wikilink")[電池](../Page/電池.md "wikilink")。但我們大多數現有的能源來自於[太陽](../Page/太陽.md "wikilink")。巨大潛在的能源闡述可由著名的公式
= <sup>2</sup>所表示。
現代能源一般可分為兩類：[可再生能源和](../Page/可再生能源.md "wikilink")[可替代能源](../Page/可替代能源.md "wikilink")。

## 能源

能源資源的生產與消費对于世界經濟非常重要。無論是生產商品，提供運輸，使電腦和其他設備正常運作，所有的經濟活動都需要能源資源，可以說是工業時代重要[生產資料之一](../Page/生產資料.md "wikilink")。

## 能源與環境

能源資源的消耗需要資源，並且會對[環境具有影響](../Page/環境.md "wikilink")。許多[發電廠燃燒](../Page/發電廠.md "wikilink")[煤炭](../Page/煤炭.md "wikilink")、[石油或](../Page/石油.md "wikilink")[天然氣來發電能作為能源的需求](../Page/天然氣.md "wikilink")。雖然燃燒這些[化石燃料可以馬上供應電力](../Page/化石燃料.md "wikilink")，但是會產生的空氣污染物包括[二氧化碳](../Page/二氧化碳.md "wikilink")（CO
2），[二氧化硫和](../Page/二氧化硫.md "wikilink")[三氧化硫](../Page/三氧化硫.md "wikilink")（SOX）和[氮氧化物](../Page/氮氧化物.md "wikilink")（NOx）。二氧化碳是一種重要的溫室氣體而被認為對[全球暖化影響的加快負起責任](../Page/全球暖化.md "wikilink")。燃燒礦物燃料發電也釋放微量金屬如鈹，鎘，鉻，銅，錳，汞，鎳，銀到環境中，也作為污染物。
因此一些環保人士提倡使用[可再生能源](../Page/可再生能源.md "wikilink")，普遍認為，最有效的辦法就是[保護環境](../Page/保護環境.md "wikilink")，[節約能源避免擴大使用能源](../Page/節約能源.md "wikilink")，從而導致環境被破壞。

## 能源危機

因為能源供應短缺或是價格上漲而影響經濟。這通常涉及到石油，電力或其他自然資源的短缺。[經濟和](../Page/經濟.md "wikilink")[政治不穩定也可能導致](../Page/政治.md "wikilink")[能源危機](../Page/能源危機.md "wikilink")，像是1973年的[石油危機和](../Page/石油危機.md "wikilink")1979年的[石油危機](../Page/石油危機.md "wikilink")。當全球石油開採率達到最高（即[石油峰值](../Page/石油峰值.md "wikilink")）可能會促成另一[能源危機](../Page/能源危機.md "wikilink")。

### 國際能源機構

1974年成立的[發達國家能源論壇性組織](../Page/發達國家能源論壇性組織.md "wikilink")。由[澳大利亞](../Page/澳大利亞.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[法國](../Page/法國.md "wikilink")、[德國等](../Page/德國.md "wikilink")23個國家所組成。成員國政府在發生[石油危機中](../Page/石油危機.md "wikilink")，合作協調能源政策，分享能源供應。並借發展[替代能源](../Page/替代能源.md "wikilink")，解決能源結構中的供需問題和環境問題。

### 傳統能源類別及其限制

  - [不可再生能源](../Page/不可再生能源.md "wikilink") -
    [石油](../Page/石油.md "wikilink")、[天然氣](../Page/天然氣.md "wikilink")、[核能](../Page/核能.md "wikilink")……等依賴有總量限制的來源
  - [可再生能源](../Page/可再生能源.md "wikilink") -
    [水力](../Page/水力.md "wikilink")、[風力](../Page/風力.md "wikilink")、[太陽能](../Page/太陽能.md "wikilink")……等無限制能量來源

#### [核能的發展](../Page/核能.md "wikilink")

核能發電是核能的主要應用方面，1950年代末至80年代中是世界[核電開始快速發展的時期](../Page/核電.md "wikilink")。人們利用[核裂變釋放出的熱量進行供熱及發電的技術](../Page/核裂變.md "wikilink")。核子工業會產生[放射性廢料](../Page/放射性廢料.md "wikilink")，低階放射性廢料要監測約300年，待其放射性逐漸降低後可以確定其安全穩定性；而放射性較強的高階放射性廢料，則需要搭配較長時間的妥善管理及處置，以待其放射性衰減到與背景輻射相當的程度。\[1\]

#### [新能源和](../Page/新能源.md "wikilink")[可再生能源](../Page/可再生能源.md "wikilink")

[太陽能](../Page/太陽能.md "wikilink")、[風能](../Page/風能.md "wikilink")、[地熱能](../Page/地熱能.md "wikilink")、[生物質能](../Page/生物質能.md "wikilink")、[潮汐能](../Page/潮汐能.md "wikilink")、[水能的總稱](../Page/水能.md "wikilink")。除生物質能外，均為[非燃燒能源](../Page/非燃燒能源.md "wikilink")，又稱為[清潔能源或](../Page/清潔能源.md "wikilink")[綠色能源](../Page/綠色能源.md "wikilink")。

#### 能源的来源

广义上来讲能源来自三个途径：1、太阳光辐射。该途径通过太阳光、植物能、化石燃料、地表温差、月球反光、水汽蒸发循环的方式体现在地球表层。来自太阳光辐射的能源总量是有限的，目前社会对该类能源的提取超过了能源充能的效率；2、地球自转运动。该途径通常包括地热、磁、以及风能、水流、潮汐等通常意义上的可再生能源。该詠翔属于地球突变过程中的韋璁耗散，偶尔也表现为爆发性的火山喷发或者地震之类破坏性的情形。；3、核聚变、核裂变。该途径包括传统意义上的可再生能源。在产生副产品的热能的同时也会产生辐射能。

## 參看

  - [各国每年人均能量消耗列表](../Page/各国每年人均能量消耗列表.md "wikilink")

  - [可再生能源商业化](../Page/可再生能源商业化.md "wikilink")

  - [燃烧](../Page/燃烧.md "wikilink")

  - [中華人民共和國能源政策](../Page/中華人民共和國能源政策.md "wikilink")

  -
  - [各國核能發展](../Page/各國核能發展.md "wikilink")

## 參考文獻

## 外部链接

  - [绿色和平：掀起能源革命，遏制全球气候变化](http://www.greenpeace.org/china/zh/news/energy-revolution)

[ca:Economia del sector
energètic](../Page/ca:Economia_del_sector_energètic.md "wikilink")
[cs:Energetika](../Page/cs:Energetika.md "wikilink") [es:Energía
(tecnología)](../Page/es:Energía_\(tecnología\).md "wikilink")
[fr:Énergie (secteur
économique)](../Page/fr:Énergie_\(secteur_économique\).md "wikilink")
[hu:Energetika](../Page/hu:Energetika.md "wikilink")
[pl:Energetyka](../Page/pl:Energetyka.md "wikilink")
[ro:Energetică](../Page/ro:Energetică.md "wikilink") [ru:Энергия
(общество)](../Page/ru:Энергия_\(общество\).md "wikilink")
[sk:Energetika](../Page/sk:Energetika.md "wikilink")
[sl:Energetika](../Page/sl:Energetika.md "wikilink")
[sr:Енергетика](../Page/sr:Енергетика.md "wikilink")
[sv:Energisystem](../Page/sv:Energisystem.md "wikilink")
[uk:Енергетика](../Page/uk:Енергетика.md "wikilink")

[\*](../Category/能源.md "wikilink")
[Category:可持續發展目標7](../Category/可持續發展目標7.md "wikilink")

1.  Vandenbosch, Robert, and Susanne E. Vandenbosch. 2007. *Nuclear
    waste stalemate*. Salt Lake City: University of Utah Press, 21.