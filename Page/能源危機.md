**能源危機**是指因为[能源供应短缺或是价格上涨而影响](../Page/能源.md "wikilink")[经济](../Page/经济.md "wikilink")，通常涉及到[石油](../Page/石油.md "wikilink")，[电力或其他](../Page/电力.md "wikilink")[自然资源的短缺](../Page/自然资源.md "wikilink")。能源危机通常会使得经济休克。很多突如其来的经济衰退通常就是由能源危机引起的。事实上，电力的生产价格的上涨导致生产成本的增加。[汽车或其他交通工具所使用的石油产品价格的上涨降低了消费者的信心和增加了他们的开销](../Page/汽车.md "wikilink")。

## 起因

市场经济的能源价格受供求关系的影响，而供求关系中的供或求的变化都可以导致能源价格的突然变化。

虽然一些能源危机是由于市场应对短缺的价格调节产生。但是在有些情况下，危机可能是市场的流通不畅而导致。
[Imported_Crude_Oil_as_a_Percent_of_US_Consumption_1950-2003.jpg](https://zh.wikipedia.org/wiki/File:Imported_Crude_Oil_as_a_Percent_of_US_Consumption_1950-2003.jpg "fig:Imported_Crude_Oil_as_a_Percent_of_US_Consumption_1950-2003.jpg")
[Brent_Spot_monthly.svg](https://zh.wikipedia.org/wiki/File:Brent_Spot_monthly.svg "fig:Brent_Spot_monthly.svg")
[P1020770.JPG](https://zh.wikipedia.org/wiki/File:P1020770.JPG "fig:P1020770.JPG")

### 石油供应

石油的[供应大致上由一些拥有大量容易抽取的石油储藏的国家所控制](../Page/供应.md "wikilink")，包括[阿拉伯联合酋长国](../Page/阿拉伯联合酋长国.md "wikilink")、[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[科威特和](../Page/科威特.md "wikilink")[委内瑞拉](../Page/委内瑞拉.md "wikilink")。

主要的产油国成立了[石油输出国组织](../Page/石油输出国组织.md "wikilink")，控制了全球石油出口的大部分产量，对世界油价具有强大的“[杠杆](../Page/杠杆.md "wikilink")”作用。

### 煤炭产量

中国的煤炭产量预计将在2027年达到峰值，其峰值产量为51亿吨。\[1\]

## 历史上的能源危机

  - [1973年能源危机](../Page/第一次石油危机.md "wikilink")：原因為石油输出的[阿拉伯国家不满西方国家支持](../Page/阿拉伯国家.md "wikilink")[以色列而采取石油禁运](../Page/以色列.md "wikilink")。

  - [1979年能源危机](../Page/第二次石油危机.md "wikilink")：原因為[伊朗革命爆发](../Page/伊朗革命.md "wikilink")。

  - [1990年石油价格暴涨](../Page/第三次石油危机.md "wikilink")：原因為[波斯灣战争](../Page/波斯灣战争.md "wikilink")。

  - ：原因為投機炒作及[美元貶值](../Page/美元.md "wikilink")。

  -
  -
## 未来可替代的能源

目前主要的替代能源有：[核能](../Page/核子動力.md "wikilink")、[水力发电](../Page/水力发电.md "wikilink")、[太阳能](../Page/太阳能.md "wikilink")、[生物能](../Page/生物能.md "wikilink")、[潮汐能](../Page/潮汐能.md "wikilink")、[地熱能和](../Page/地熱能.md "wikilink")[风能等](../Page/风能.md "wikilink")。但是迄今为止只有[地熱能](../Page/地熱能.md "wikilink")、[水力发电和](../Page/水力发电.md "wikilink")[核能有明顯的功效](../Page/核子動力.md "wikilink")。

## 参见

  - [哈伯特顶点](../Page/哈伯特顶点.md "wikilink")
  - [Scientific American: The End of Cheap
    Oil](http://dieoff.com/page140.pdf)
  - [頁岩油](../Page/頁岩油.md "wikilink")

## 参考资料

<references/>

[能源危机](../Page/category:能源危机.md "wikilink")

[Category:自然资源冲突](../Category/自然资源冲突.md "wikilink")

1.  [中国2027年将迎来煤炭峰值](http://www.forbeschina.com/review/201107/0010468.shtml)
    ，福布斯中文网，2011年7月6日