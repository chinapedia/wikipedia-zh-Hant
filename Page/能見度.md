[Beijing_smog_comparison_August_2005.png](https://zh.wikipedia.org/wiki/File:Beijing_smog_comparison_August_2005.png "fig:Beijing_smog_comparison_August_2005.png")彌漫（右）的[北京](../Page/北京.md "wikilink")\]\]
**能見度**又稱**可見度**，指觀察者離物體多遠時仍然可以清楚看見該物體。[氣象學中](../Page/氣象學.md "wikilink")，能見度被定義為[大氣的](../Page/大氣.md "wikilink")[透明度](../Page/透明度.md "wikilink")，因此在氣象學裏，同一空氣的能見度在白天和晚上是一樣的。能見度的[單位一般為](../Page/單位.md "wikilink")[米或](../Page/米.md "wikilink")[公里](../Page/公里.md "wikilink")。能見度對於[航空](../Page/航空.md "wikilink")、[航海和陸上運輸都非常重要](../Page/航海.md "wikilink")。

## 能見度的高低

能見度通常要好，看的公里數遠，需要有下列因素：

1.  剛下過[雨](../Page/雨.md "wikilink")（通常是夏季[午後雷陣雨](../Page/午後雷陣雨.md "wikilink")：或[颱風過後](../Page/颱風.md "wikilink")「也須視情況而定」）
2.  [大氣擴散條件良好](../Page/大氣擴散條件.md "wikilink")，[懸浮微粒與污染物較容易擴散](../Page/懸浮微粒.md "wikilink")
3.  [風向與](../Page/風向.md "wikilink")[地形配合](../Page/地形.md "wikilink")
4.  [風速](../Page/風速.md "wikilink")
5.  颱風來臨前（視情況而定：但機會相對颱風剛過的高）

而低能見度出現的因素與時間，可能有下列因素所致：

1.  大氣擴散條件不好，懸浮微粒與污染物不容易擴散，天空上方容易形成污染層，並可能伴隨光化污染，二次污染
2.  下雨中
3.  有[霾的時間](../Page/霾.md "wikilink")
4.  [逆溫](../Page/逆溫.md "wikilink")

## 能見度的報告

能見度的呈現是按照[世界氣象組織的數值調整指引](../Page/世界氣象組織.md "wikilink")。

| 儀器量度所得的能見度   | 數值向下調整的規則 |
| ------------ | --------- |
| 100米 - 4999米 | 100米      |
| 5公里 - 30公里   | 1公里       |
| 31公里 - 50公里  | 5公里       |

例1.量度所得的能見度是1690米，報告為1600米；

例2.量度所得的能見度是9900米，報告為9公里；

例3.量度所得的能見度是39公里，報告為35公里\[1\]。

## 資料來源

## 外部連結

  - [1](http://taqm.epa.gov.tw/taqm/zh-tw/default.aspx)，[中華民國](../Page/中華民國.md "wikilink")[環保署](../Page/環保署.md "wikilink")[空氣品質監測網](../Page/空氣品質.md "wikilink")
  - [2](http://www.weather.gov.hk/wxinfo/ts/index_c_vis.htm)，[香港天文台分區天氣](../Page/香港天文台.md "wikilink")
    能見度

## 參見

[能见度](../Category/能见度.md "wikilink")
[Category:基本气象概念与现象](../Category/基本气象概念与现象.md "wikilink")

1.