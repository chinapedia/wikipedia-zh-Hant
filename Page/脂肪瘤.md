**脂肪瘤（）**是一种良性[肿瘤](../Page/肿瘤.md "wikilink")，由[脂肪组成](../Page/脂肪.md "wikilink")，多发生在[四肢](../Page/四肢.md "wikilink")、[躯干的](../Page/躯干.md "wikilink")[皮下](../Page/皮肤.md "wikilink")，多呈扁圆形，用[手稍微用力压其基部](../Page/手.md "wikilink")，可见分叶形态。

脂肪瘤一般无痛感，但發炎時會十分疼痛。但有多发性圆形或卵圆形结节状脂肪瘤按压有痛感，又称为痛性脂肪瘤或多发性脂肪瘤。

脂肪瘤可发生在任何年龄层，但通常好发于40-60岁的人群。

脂肪瘤不同于[脂肪肉瘤](../Page/脂肪肉瘤.md "wikilink")，后者为恶性。

## 治療

### 手术治疗

Image:Lipoma 05.jpg|脂肪瘤的[X光图](../Page/X光.md "wikilink") Image:Lipoma
03.jpg|术中照片 Image:Lipoma
04.jpg|脂肪瘤移除后的手术区域。箭头所指为被脂肪瘤压迫的[正中神经](../Page/正中神经.md "wikilink")。
Image:Lipoma 06.jpg|被移除的脂肪瘤
(8 cm × 6 cm × 3 cm)

<File:Breast> Lipoma
102050296.jpg|乳房脂肪瘤的[超声图像](../Page/超声图像.md "wikilink")
<File:Arm> mri.jpg|[MRI图像显示出手臂上的脂肪瘤](../Page/MRI.md "wikilink")
<File:Medical> X-Ray imaging TPH07
nevit.jpg|[X光片显示出脂肪瘤](../Page/X光.md "wikilink")
<File:Lipoma> from thigh.jpg|这个脂肪瘤从一位39岁男性患者的大腿上移除。移除时测得直径约为10 cm。
[File:Fibrolipoma.JPG|背部上方肩膀正下方的成纤维脂肪瘤，形状类似中等大小的葡萄干](File:Fibrolipoma.JPG%7C背部上方肩膀正下方的成纤维脂肪瘤，形状类似中等大小的葡萄干)。

## 引用

## 外部連接

  - [Illustration](https://web.archive.org/web/20060109042809/http://pathweb.uchc.edu/eAtlas/Bone/706.htm)
    from University of Connecticut Health Center
  - [Esophageal
    Lipomatosis](http://rad.usuhs.edu/medpix/master.php3?mode=slide_sorter&pt_id=11517&quiz=#top)
    MedPix Images from Uniformed Services University of the Health
    Sciences
  - [Lipoma
    images](https://web.archive.org/web/20030223054237/http://dermatlas.med.jhmi.edu/derm/result.cfm?Diagnosis=-1623404710)
    from DermAtlas
  - humpath
    [\#2626](http://www.humpath.com/spip.php?page=article&id_article=2626)
  - [List of possible treatment
    options](http://www.lipomaboard.com/treatments-cures-f3/here-your-lipoma-treatment-options-far-t284.html)

[Category:皮膚和皮下生長](../Category/皮膚和皮下生長.md "wikilink")
[Category:软组织疾病](../Category/软组织疾病.md "wikilink")
[Category:良性腫瘤](../Category/良性腫瘤.md "wikilink")