[Chylomicron.svg](https://zh.wikipedia.org/wiki/File:Chylomicron.svg "fig:Chylomicron.svg")）**
**ApoA**、**ApoB**、**ApoC**和ApoE**：[载脂蛋白](../Page/载脂蛋白.md "wikilink")；**T**：[三酰甘油](../Page/三酸甘油酯.md "wikilink")）；**C**：[胆固醇](../Page/胆固醇.md "wikilink")；绿色：[磷脂](../Page/磷脂.md "wikilink")\]\]

**脂蛋白**是一种含有[蛋白质以及可以与蛋白质水结合的](../Page/蛋白质.md "wikilink")[脂类的](../Page/脂类.md "wikilink")[生物化学组合体](../Page/生物化学.md "wikilink")。许多[酶](../Page/酶.md "wikilink")、[载体](../Page/载体.md "wikilink")、结构蛋白、[抗原](../Page/抗原.md "wikilink")、[黏附素和](../Page/黏附素.md "wikilink")[毒素都是脂蛋白](../Page/毒素.md "wikilink")。其中例子包括：可以载着脂肪在血液中流动的[高密度与](../Page/高密度脂蛋白.md "wikilink")[低密度脂蛋白](../Page/低密度脂蛋白.md "wikilink")、[线粒体与](../Page/线粒体.md "wikilink")[叶绿体上的](../Page/叶绿体.md "wikilink")[跨膜蛋白以及细菌脂蛋白](../Page/跨膜蛋白.md "wikilink")\[1\]。

## 功能

脂蛋白颗粒的目的是在体内的[血液中转运](../Page/血液.md "wikilink")[脂类](../Page/脂类.md "wikilink")（例如[胆固醇](../Page/胆固醇.md "wikilink")）。

所有[细胞都使用及依赖](../Page/细胞.md "wikilink")[脂肪和](../Page/脂肪.md "wikilink")[胆固醇作为构造多种](../Page/胆固醇.md "wikilink")[生物膜的建筑材料](../Page/生物膜.md "wikilink")，细胞使用生物膜以控制内部水、内部水溶物以及组织内部结构和蛋白质酶促系统。

脂蛋白颗粒中磷脂、胆固醇和载脂蛋白的[亲水基团面向外边](../Page/亲水.md "wikilink")。这样的表征使它们得以溶解于基于盐水的血池之中。而甘油三酯脂肪以及胆固醇酯则存在于脂蛋白的内部，磷脂单层和[载脂蛋白将之与水相屏蔽起来](../Page/载脂蛋白.md "wikilink")。

与脂蛋白颗粒表面蛋白之间的相互作用类型有：①与血液中的酶相互作用、②它们之间相互作用③与细胞表面的特异性蛋白结合以决定到底是甘油三酸酯还是胆固醇将会添加到脂蛋白颗粒上面或是从脂蛋白上面移除。

就[粥样斑的形成与发展的过程而言是与衰退过程截然相反的](../Page/粥样斑.md "wikilink")，其主要问题出在胆固醇的转运模式上，而不是胆固醇浓度上。

### 跨膜脂蛋白

脂类常常是复合体不可或缺的一部分，尽管它们看似本身没有催化活性。为了将跨膜脂蛋白与和它结合的[生物膜相分开](../Page/生物膜.md "wikilink")，就常常需要一些[去垢剂](../Page/去垢剂.md "wikilink")（一些脂类）。

## 分类

### 按密度分

脂蛋白常以以下方式分类，下面以体积从大到小和密度从小到大将他们列出。如果脂蛋白所含的脂肪比蛋白质更多的话，他们就会比较大且密度较小。它们是基于[电泳以及](../Page/电泳.md "wikilink")[差速离心数据而分类的](../Page/差速离心.md "wikilink")。

  - [乳糜微粒将](../Page/乳糜微粒.md "wikilink")[甘油三酯](../Page/甘油三酯.md "wikilink")（脂肪）从[肠脏带往](../Page/肠脏.md "wikilink")[肝脏](../Page/肝脏.md "wikilink")、[骨骼肌和](../Page/骨骼肌.md "wikilink")[脂肪组织](../Page/脂肪组织.md "wikilink")。
  - [极低密度脂蛋白](../Page/极低密度脂蛋白.md "wikilink")（VLDL）将（新合成的）[甘油三酯从肝脏带往脂肪组织](../Page/甘油三酯.md "wikilink")。
  - [中间密度脂蛋白](../Page/中间密度脂蛋白.md "wikilink")（IDL）的密度和大小介于极低密度脂蛋白与低密度脂蛋白之间。在血液中常检测不到他们的存在。
  - [低密度脂蛋白](../Page/低密度脂蛋白.md "wikilink")（LDL）将胆固醇从肝脏带到身体细胞中。低密度脂蛋白有时也叫做“[坏胆固醇](../Page/坏胆固醇.md "wikilink")”脂蛋白。
  - [高密度脂蛋白](../Page/高密度脂蛋白.md "wikilink")（HDL）从身体组织中收集胆固醇并将它们带回肝脏。高密度脂蛋白有时也叫做“[好胆固醇](../Page/好胆固醇.md "wikilink")”脂蛋白。

| 脂蛋白\[2\]     |
| ------------ |
| **密度**（克/毫升） |
| \>1.063      |
| 1.019～1.063  |
| 1.006～1.019  |
| 0.95～1.006   |
| \<0.95       |

### α与β

根据[血清蛋白电泳中的蛋白质分类](../Page/血清蛋白电泳.md "wikilink")，也可以将脂蛋白分为“α”和“β”类，这一术语有时用于描述诸如[无β脂蛋白血症之类的脂类紊乱](../Page/无β脂蛋白血症.md "wikilink")。

### 脂蛋白（a）

在心血管病诊断测试中，脂蛋白（a）——Lp(a)含量在：

  -
    \< 14 mg/dL :正常
    14-19 mg/dL : ?
    \> 19 mg/dL :高危

有些做法可以降低其含量：多做[有氧运动](../Page/有氧运动.md "wikilink")、服用[烟酸](../Page/烟酸.md "wikilink")、服用[阿司匹林以及服用](../Page/阿司匹林.md "wikilink")[没药脂](../Page/没药脂.md "wikilink")\[3\]。

## 代谢

在体内处理脂蛋白的过程被称为“脂蛋白代谢”。它分为两套途径，分别是外源性途径和内源性途径，大多数是基于所讨论的脂蛋白是主要是来自于膳食（外源性途径）或是由肝脏所产生（内源性途径）

### 外源性途径

[小肠膜](../Page/小肠.md "wikilink")[上皮细胞很容易地从富于营养的食物中吸收脂质](../Page/上皮细胞.md "wikilink")。这些脂质，包括[甘油三酯](../Page/甘油三酯.md "wikilink")、磷脂和胆固醇，被[载脂蛋白B-48组装进](../Page/载脂蛋白B-48.md "wikilink")[乳糜微粒之中](../Page/乳糜微粒.md "wikilink")。这些新生的乳糜微粒被肠上皮细胞分泌到[淋巴循环之中](../Page/淋巴系统.md "wikilink")，此过程在很大程度上依赖于载脂蛋白B-48。当它们在淋巴管中循环时，这些新生的乳糜微粒绕过肝脏循环并且通过[胸导管排放到血流之中](../Page/胸导管.md "wikilink")。

在血液中，高密度脂蛋白颗粒将[载脂蛋白C-II与](../Page/载脂蛋白C-II.md "wikilink")[载脂蛋白E送给新生的乳糜微粒](../Page/载脂蛋白E.md "wikilink")；现在，乳糜微粒可以被看作为成熟型的了。借助载脂蛋白C-II的作用，成熟的乳糜微粒激活一种称为[脂蛋白脂肪酶](../Page/脂蛋白脂肪酶.md "wikilink")（LPL）的酶，这种酶位于血管[内皮细胞膜上](../Page/内皮细胞.md "wikilink")。脂蛋白脂肪酶催化[甘油三酯](../Page/甘油三酯.md "wikilink")（即[甘油共价结合在](../Page/甘油.md "wikilink")[脂肪酸上](../Page/脂肪酸.md "wikilink")）的[水解](../Page/水解.md "wikilink")，最终从乳糜微粒中释放出[甘油以及](../Page/甘油.md "wikilink")[脂肪酸](../Page/脂肪酸.md "wikilink")。甘油和脂肪酸可以被吸收到周缘组织中，特别是[脂肪和](../Page/脂肪.md "wikilink")[肌肉之中](../Page/肌肉.md "wikilink")，以作为能量和储备之用。

被水解的乳糜微粒可以被叫做乳糜微粒残体。乳糜微粒残体会继续循环下去，直到在载脂蛋白E的帮助下与[乳糜微粒残体受体相互作用为止](../Page/乳糜微粒残体受体.md "wikilink")，这些受体多数见于肝脏之中。该相互作用会导致乳糜微粒残体被[胞吞](../Page/胞吞.md "wikilink")，它接下来会被[溶体水解](../Page/溶体.md "wikilink")。溶体水解会释放甘油和脂肪酸进入细胞中，细胞会将其作为产能用或储存起来以备后用。

### 内源性途径

肝脏是脂蛋白的另一个重要来源，主要是极低密度脂蛋白。甘油三酯和胆固醇被[载脂蛋白B-100组装起来形成极低密度脂蛋白颗粒](../Page/载脂蛋白B-100.md "wikilink")。新生的极低密度脂蛋白颗粒通过依赖与载脂蛋白B-100的途径被释放进血流之中。

在乳糜微粒代谢之中，极低密度脂蛋白颗粒中的[载脂蛋白C-II与](../Page/载脂蛋白C-II.md "wikilink")[载脂蛋白E是从高密度脂蛋白颗粒中索取的](../Page/载脂蛋白E.md "wikilink")。一旦被装载上载脂蛋白C-II与载脂蛋白E，新生极低密度脂蛋白颗粒就可以被认为是成熟的了。

与乳糜微粒相同，极低密度脂蛋白颗粒也参加循环并且也被位于血管内皮细胞上的脂蛋白脂肪酶水解。载脂蛋白C-II激活脂蛋白脂肪酶，致使极低密度脂蛋白颗粒的水解并释放出甘油和脂肪酸。这些产物可以被周缘组织从血液中吸收，主要是脂肪和肌肉。被水解的极低密度脂蛋白颗粒现在叫做极低密度脂蛋白残体或称为[中间密度脂蛋白](../Page/中间密度脂蛋白.md "wikilink")（IDL）。极低密度脂蛋白残体可以继续参与循环，并且通过载脂蛋白E与残体受体相互作用，被肝脏吸收以及被[肝酯酶进一步水解](../Page/肝酯酶.md "wikilink")。

它被肝酯酶水解后会产生甘油和脂肪酸，留下中间密度脂蛋白残体，称作[低密度脂蛋白](../Page/低密度脂蛋白.md "wikilink")（LDL），其中有相对较高的胆固醇含量。低密度脂蛋白继续循环并被肝脏以及周缘组织吸收。通过[低密度脂蛋白受体与低密度脂蛋白颗粒表面的载脂蛋白B](../Page/低密度脂蛋白受体.md "wikilink")-100或E之间的相互作用，低密度脂蛋白与目标组织发生结合。通过[胞吞作用被吸收进去](../Page/胞吞作用.md "wikilink")，被内化的低密度脂蛋白颗粒在溶酶体中被水解，释放出主要成分为胆固醇的脂类。

## 参考文献

## 外部链接

  - [Database of bacterial lipoproteins at
    mrc-lmb.cam.ac.uk](http://www.mrc-lmb.cam.ac.uk/genomes/dolop/)

  - [Overview and diagram at
    washington.edu](http://courses.washington.edu/conj/membrane/lipoprotein.htm)

  - [Lipoprotein research at the Medical University of
    Vienna](http://www.lipoprotein.at)

  - [Lipoprotein assembly at
    wisc.edu](http://www.biochem.wisc.edu/attie/research/researchLDLR.html)

  - [Lipoprotein circulation at
    purdue.edu](https://web.archive.org/web/20081222064637/http://people.pharmacy.purdue.edu/~dallen/lipo.html)

  -
  -
## 参见

  - [载脂蛋白](../Page/载脂蛋白.md "wikilink")
  - [脂锚定蛋白](../Page/脂锚定蛋白.md "wikilink")
  - [变异型心绞痛胆固醇试验](../Page/变异型心绞痛胆固醇试验.md "wikilink")

{{-}}

[he:כולסטרול\#ליפופרוטאינים](../Page/he:כולסטרול#ליפופרוטאינים.md "wikilink")

[脂蛋白](../Category/脂蛋白.md "wikilink")
[Category:脂类](../Category/脂类.md "wikilink")

1.  [mrc-lmb.cam.ac.uk](http://www.mrc-lmb.cam.ac.uk/genomes/dolop/)
2.  Biochemistry 2nd Ed. 1995 Garrett & Grisham
3.  Beyond Cholesterol, Julius Torelli MD, 2005 ISBN 0-312-34863-0 p.91