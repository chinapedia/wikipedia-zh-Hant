[Common_lipids_lmaps.png](https://zh.wikipedia.org/wiki/File:Common_lipids_lmaps.png "fig:Common_lipids_lmaps.png")\[1\]和[油酸](../Page/油酸.md "wikilink")\[2\]。中央為[三酸甘油酯](../Page/三酸甘油酯.md "wikilink")，由[丙三醇](../Page/丙三醇.md "wikilink")（又稱甘油）為主幹，旁接[油酸](../Page/油酸.md "wikilink")、[硬脂酸和](../Page/硬脂酸.md "wikilink")[棕櫚酸鏈構成](../Page/棕櫚酸.md "wikilink")。圖片下方是[磷脂醯膽鹼](../Page/磷脂醯膽鹼.md "wikilink")，一種常見的[磷脂](../Page/磷脂.md "wikilink")\[3\]。\]\]

**脂類**（英語：**Lipid**），又稱**脂質**，这是一类不溶于水而易溶于脂肪溶剂（醇、醚、氯仿、苯）等非极性有机溶剂，由[脂肪酸与](../Page/脂肪酸.md "wikilink")[醇作用脱水缩合生成的](../Page/醇.md "wikilink")[酯及其衍生物统称为脂类](../Page/酯.md "wikilink")，其中包括[脂肪](../Page/脂肪.md "wikilink")、[蠟](../Page/蠟.md "wikilink")、[类固醇](../Page/类固醇.md "wikilink")、脂溶性[維生素](../Page/維生素.md "wikilink")（如維生素A，D，E和K）\[4\]、、[二酸甘油酯](../Page/二酸甘油酯.md "wikilink")、[磷脂等](../Page/磷脂.md "wikilink")。它的主要生理功能包括儲存能量、構成[細胞膜以及](../Page/細胞膜.md "wikilink")[膜的訊息傳導等](../Page/信號分子.md "wikilink")\[5\]\[6\]。如今，脂类已经被用于[美容和](../Page/美容.md "wikilink")[食品工业](../Page/食品工业.md "wikilink")，以及[纳米技术](../Page/纳米技术.md "wikilink")\[7\]。

脂質可以廣義定義為[疏水性或](../Page/疏水性.md "wikilink")[雙親性小分子](../Page/两亲分子.md "wikilink")；某些脂質因為其雙親性的特質（兼具親水性與疏水性），能在水溶液環境中形成[囊泡](../Page/囊泡.md "wikilink")、[脂質體或膜等構造](../Page/脂質體.md "wikilink")。生物體內的脂質完全或部分源自兩種截然不同的生物次單元：[酮酸基與](../Page/酮.md "wikilink")[異戊二烯](../Page/異戊二烯.md "wikilink")\[8\]。由此，脂質可以概分為八類：[脂肪酸](../Page/脂肪酸.md "wikilink")、[甘油酯](../Page/甘油酯.md "wikilink")、[甘油磷脂](../Page/甘油磷脂.md "wikilink")、[鞘脂](../Page/鞘脂.md "wikilink")（神經脂質）、、[聚酮类](../Page/聚酮.md "wikilink")（由酮乙基次單元聚合而成）、固醇脂类，以及孕烯醇酮脂类（由[異戊二烯次單元縮合聚合而成](../Page/異戊二烯.md "wikilink")）\[9\]\[10\]。

脂類常被視為是[脂肪的同義詞](../Page/脂肪.md "wikilink")，但脂肪只是一種稱為[三酸甘油脂的脂類](../Page/三酸甘油脂.md "wikilink")。脂類也包括[脂肪酸及其衍生物](../Page/脂肪酸.md "wikilink")，包括單酸甘油酯、二酸甘油酯、磷脂等，也包括其他含有[固醇的](../Page/固醇.md "wikilink")[代謝產物](../Page/代謝產物.md "wikilink")，像是[膽固醇](../Page/膽固醇.md "wikilink")\[11\]。雖然人類和其他動物有許多不同的代謝方式，可以切斷脂肪鏈及合成脂質，不過仍有一些必需脂質無法自行合成，需要在食物中攝取。

有生物以前脂質的化學反應，以及[原始生命體的形成](../Page/原始生命體.md "wikilink")，現已認為是[生命起源模型中的關鍵](../Page/生命起源.md "wikilink")。

## 分类

### 脂肪酸类

[Prostacyclin-2D-skeletal.png](https://zh.wikipedia.org/wiki/File:Prostacyclin-2D-skeletal.png "fig:Prostacyclin-2D-skeletal.png")（一种[前列腺素](../Page/前列腺素.md "wikilink")，类二十烷酸脂肪酸的一个例子）\]\]
[Leukotriene_B4.svg](https://zh.wikipedia.org/wiki/File:Leukotriene_B4.svg "fig:Leukotriene_B4.svg")（一种[白三烯](../Page/白三烯.md "wikilink")，类二十烷酸脂肪酸的一个例子）\]\]

[脂肪酸](../Page/脂肪酸.md "wikilink")，或是脂質中的脂肪酸殘留部份，是由[乙醯輔酶A和](../Page/乙醯輔酶A.md "wikilink")[丙二醯輔酶A及](../Page/丙二醯輔酶A.md "wikilink")[甲基丙二酸單醯輔酶A合成的許多不同種類的分子](../Page/甲基丙二酸單醯輔酶A.md "wikilink")，合成的反應稱為[脂肪酸合成](../Page/脂肪酸合成.md "wikilink")\[12\]\[13\]。脂肪酸是由尾端為[羧酸官能基的](../Page/羧酸.md "wikilink")[碳鏈組成](../Page/脂肪族化合物.md "wikilink")，因此分子會有有[極性且](../Page/極性.md "wikilink")[親水的一端](../Page/親水.md "wikilink")，另一端則是非極性且[疏水的](../Page/疏水性.md "wikilink")。脂肪酸結構是生物脂質中最基本的結構，常用來建構更複雜脂質\[14\]。碳鏈長度一般介於4到24個碳之間\[15\]，可能是飽和化合物或是[不饱和化合物](../Page/不饱和化合物.md "wikilink")，也可能連結其他含有[氧](../Page/氧.md "wikilink")、[鹵素](../Page/鹵素.md "wikilink")、[氮或是](../Page/氮.md "wikilink")[硫的](../Page/硫.md "wikilink")[官能基](../Page/官能基.md "wikilink")。若脂肪酸中含有雙鍵，則可能會有順式及反式的[顺反异构](../Page/顺反异构.md "wikilink")，對有很大的影響。顺式的雙鍵會使碳鏈彎曲，若是分子中有多個雙鍵，反應會更明顯。18個碳的[亚麻酸中有三個雙鍵](../Page/亚麻酸.md "wikilink")，是植物的类囊体膜中最豐富的脂肪酸酰基链，因此在環境低溫時，仍可以使囊膜有高度的流動性\[16\]
。大部份天然的（有雙鍵的）脂肪酸是順式的，不過有些天然的脂肪酸是反式的，而人工氫化的脂肪和油類也是反式的\[17\]。

在生物中重要的脂肪酸包括主要衍生自[花生四烯酸的](../Page/花生四烯酸.md "wikilink")[类花生酸](../Page/类花生酸.md "wikilink")，另一種為[二十碳五烯酸](../Page/二十碳五烯酸.md "wikilink")（EPA），包括[前列腺素](../Page/前列腺素.md "wikilink")、[白三烯](../Page/白三烯.md "wikilink")、[血栓素等](../Page/血栓素.md "wikilink")。[二十二碳六烯酸](../Page/二十二碳六烯酸.md "wikilink")（DHA）對生物體也相當的重要，尤其是在生物的視覺上\[18\]\[19\]。其他重要的脂肪酸類脂質包括脂肪酸酯及脂肪酸胺，脂肪酸酯包括重要的生物化學中間產物，例如、及脂肪酸[硫酯](../Page/硫酯.md "wikilink")[辅酶A衍生物](../Page/辅酶A.md "wikilink")、脂肪酸[硫酯](../Page/硫酯.md "wikilink")[醯基載體蛋白衍生物](../Page/醯基載體蛋白.md "wikilink")、及脂肪酸肉碱。脂肪酸胺包括，例如[大麻素中的神經傳導物質](../Page/大麻素.md "wikilink")[花生四烯酸乙醇胺](../Page/花生四烯酸乙醇胺.md "wikilink")\[20\]。

### 甘油酯类

[Fat_triglyceride_shorthand_formula.PNG](https://zh.wikipedia.org/wiki/File:Fat_triglyceride_shorthand_formula.PNG "fig:Fat_triglyceride_shorthand_formula.PNG");
右边从上到下：[棕榈酸](../Page/棕榈酸.md "wikilink")，[油酸](../Page/油酸.md "wikilink")，[α-亚麻酸](../Page/Α-亞麻酸.md "wikilink")。\]\]

甘油酯中包括、[二酸及](../Page/二酸甘油酯.md "wikilink")[三酸甘油酯](../Page/三酸甘油酯.md "wikilink")\[21\]，分別是甘油和一、二、三個脂肪酸形成的酯類，其中最為人知的是三酸甘油酯，其中甘油
的三個羥基和脂肪酸反應，多半會是三種不同的脂肪酸。動物會用脂質儲存能量，而這些脂質也會儲存在動物的[脂肪組織中](../Page/脂肪組織.md "wikilink")。在代謝脂肪時三酸甘油酯的酯鍵會斷裂，分解為甘油和脂肪酸\[22\]。

甘油酯类中的化合物還包括甘油葡糖苷（glycosylglycerol），是甘油和[單醣由](../Page/單醣.md "wikilink")[糖苷键鍵結的化合物](../Page/糖苷键.md "wikilink")，例如在植物薄膜中常見的二半乳糖基二脂酰甘油（digalactosyldiacylglycerol）\[23\]，或是哺乳類[精子中常見的](../Page/精子.md "wikilink")\[24\]。

### 甘油磷脂类

[Phosphatidyl-ethanolamine.svg](https://zh.wikipedia.org/wiki/File:Phosphatidyl-ethanolamine.svg "fig:Phosphatidyl-ethanolamine.svg")\]\]

甘油磷脂一般簡稱為[磷脂](../Page/磷脂.md "wikilink")，是含有磷酸的脂類，出現在自然界及細胞的[磷脂雙分子層中](../Page/磷脂雙分子層.md "wikilink")\[25\]，和[新陳代謝和](../Page/新陳代謝.md "wikilink")[細胞信號傳送有關](../Page/細胞信號傳送.md "wikilink")\[26\]。神經組織（包括大腦）含有大量的磷脂，其成份的改變意味著有可能有神經的病變\[27\]。磷脂可以分為兩類，[真核生物及](../Page/真核生物.md "wikilink")[細菌中的磷脂](../Page/細菌.md "wikilink")，其極性的分子團連結在甘油的*sn*-3位上，而[古菌中的磷脂](../Page/古菌.md "wikilink")，其極性的分子團連結在甘油的*sn*-1位上\[28\]。

[生物膜中常見的磷脂有](../Page/生物膜.md "wikilink")[磷脂醯膽鹼](../Page/磷脂醯膽鹼.md "wikilink")（也稱為PC、GPCho或[卵磷脂](../Page/卵磷脂.md "wikilink")）、[磷脂酰乙醇胺](../Page/磷脂酰乙醇胺.md "wikilink")（PE或GPEtn）及[磷脂絲胺酸](../Page/磷脂絲胺酸.md "wikilink")（也稱為PS或GPSer）。磷脂除了作為細胞膜的主要成份，以及結合細胞內或細胞間蛋白質外。有些真核生物细胞中的磷脂是細胞膜衍生的[第二信使系統或是其前驅體](../Page/第二信使系統.md "wikilink")，這類磷脂有及[磷脂酸](../Page/磷脂酸.md "wikilink")\[29\]。一般而言甘油的一或兩個羥基會連接長鏈的脂肪酸，不過也有連接烷基或是1Z-烯基（的磷脂，例如古菌中的二烷基醚变体\[30\]。

### 鞘脂类

[Sphingomyelin-horizontal-2D-skeletal.png](https://zh.wikipedia.org/wiki/File:Sphingomyelin-horizontal-2D-skeletal.png "fig:Sphingomyelin-horizontal-2D-skeletal.png").\]\]

[鞘脂是一組複雜化合物的統稱](../Page/鞘脂.md "wikilink")\[31\]，有共同的鞘氨醇碱（sphingoid
base）骨架，是由[絲胺酸和長脂肪鏈的醯基輔酶A](../Page/絲胺酸.md "wikilink")[從頭合成](../Page/從頭合成.md "wikilink")，之後轉換為[神經醯胺](../Page/神經醯胺.md "wikilink")、磷鞘脂、糖鞘脂和其它化合物。哺乳動物的鞘氨醇碱一般是指[鞘氨醇](../Page/鞘氨醇.md "wikilink")。[神經醯胺是常見的鞘氨醇碱衍生物](../Page/神經醯胺.md "wikilink")，有一個連接[酰胺基的脂肪酸](../Page/酰胺.md "wikilink")。其脂肪酸多半是飽和脂肪酸或是單元不飽和脂肪酸，碳鏈長度約為16至26個碳原子\[32\]

哺乳類體內的鞘脂主要以[鞘磷脂為主](../Page/鞘磷脂.md "wikilink")\[33\]．而昆蟲體內則主要是磷酸乙醇胺神经酰胺\[34\]，真菌體內有植物神經磷酸肌醇及含有[甘露糖的鞘脂](../Page/甘露糖.md "wikilink")\[35\]。[糖鞘脂是鞘脂和糖以](../Page/糖鞘脂.md "wikilink")[糖苷键連結的化合物](../Page/糖苷键.md "wikilink")，例如構造簡單的[腦苷脂以及較複雜的](../Page/腦苷脂.md "wikilink")[神經節苷脂](../Page/神經節苷脂.md "wikilink")。

### 固醇类

固醇包括[膽固醇及其衍生物](../Page/膽固醇.md "wikilink")，以及甘油磷脂和鞘磷脂是組成生物膜的重要成份\[36\]。固醇都有相同的四環結構，是身體中的[激素及](../Page/激素.md "wikilink")[細胞信號傳送](../Page/細胞信號傳送.md "wikilink")，有著不同的角色。18個碳的固醇包括[雌激素](../Page/雌激素.md "wikilink")，C19的固醇包括[雄激素](../Page/雄激素.md "wikilink")，例如[睾酮及](../Page/睾酮.md "wikilink")[雄甾酮](../Page/雄甾酮.md "wikilink")。C21的固醇包括[孕激素](../Page/孕激素.md "wikilink")、[糖皮質激素及](../Page/糖皮質激素.md "wikilink")[鹽皮質激素](../Page/鹽皮質激素.md "wikilink")\[37\]。包括許多不同形式的[維生素D](../Page/維生素D.md "wikilink")，其特徵是固醇主結構中B環的开环\[38\]其他的固醇有[胆汁酸及其共軛鹼](../Page/胆汁酸.md "wikilink")\[39\]，是哺乳類氧化膽固醇後的衍生物，在肝臟中生成。植物中的固醇稱為[植物固醇](../Page/植物固醇.md "wikilink")，例如[β-谷固醇](../Page/β-谷固醇.md "wikilink")、[豆固醇及](../Page/豆固醇.md "wikilink")[菜籽固醇](../Page/菜籽固醇.md "wikilink")，後者也是判斷[藻類生長的](../Page/藻類.md "wikilink")[生物標記](../Page/生物標記.md "wikilink")\[40\]。真菌細胞膜中主要的固醇為[麥角固醇](../Page/麥角固醇.md "wikilink")\[41\]。

### 異戊烯醇脂类

酯是由五碳及合成，主要是透過[甲羥戊酸路徑](../Page/甲羥戊酸.md "wikilink")\[42\]。簡單的类异戊二烯是由C5單元的連續加成所形成，依照[萜烯的數量來分類](../Page/萜烯.md "wikilink")。超過40個碳的萜稱為多萜。[類胡蘿蔔素是重要的簡單类异戊二烯](../Page/類胡蘿蔔素.md "wikilink")，是[抗氧化剂](../Page/抗氧化剂.md "wikilink")，也是[維生素A的前驅體](../Page/維生素A.md "wikilink")\[43\]。另一種重要的分子是[醌及](../Page/醌.md "wikilink")[对苯二酚](../Page/对苯二酚.md "wikilink")\[44\]。[維生素E](../Page/維生素E.md "wikilink")、[維生素K及](../Page/維生素K.md "wikilink")[辅酶Q10也屬於這一類](../Page/辅酶Q10.md "wikilink")。原核生物會合成聚異戊二烯醇（[细菌萜醇](../Page/细菌萜醇.md "wikilink")），連接在氧原子上的終端異戊二烯是未飽和的，而動物產生的聚異戊二烯醇（）其終端異戊二烯已被還原\[45\]。

### 醣脂类

[Kdo2-lipidA.png](https://zh.wikipedia.org/wiki/File:Kdo2-lipidA.png "fig:Kdo2-lipidA.png")殘基以藍色表示，Kdo殘基以紅色表示，[醯基為黑色](../Page/醯基.md "wikilink")，而[磷酸鹽為綠色](../Page/磷酸鹽.md "wikilink")\]\]

醣脂是指脂肪酸直接連結到[醣的骨架](../Page/醣.md "wikilink")，產生和雙層脂膜相容的結構。由[單糖取代了](../Page/單糖.md "wikilink")[甘油酯和](../Page/甘油酯.md "wikilink")[磷脂中甘油的骨架角色](../Page/磷脂.md "wikilink")。最常見的醣脂是[脂质A的前體](../Page/脂质A.md "wikilink")，是[革兰氏阴性菌中](../Page/革兰氏阴性菌.md "wikilink")[脂多糖之成份之一](../Page/脂多糖.md "wikilink")。典型的脂质A分子有葡萄糖胺[雙醣](../Page/雙醣.md "wikilink")，是加了七個脂肪酸鏈的衍生物。[大腸桿菌生長需要的最小多醣脂為Kdo](../Page/大腸桿菌.md "wikilink")<sub>2</sub>-Lipid
A，是葡萄糖胺的六酰化二糖，其中有二個糖基化的3-脱氧-D-甘露-2-辛酮糖（Kdo）殘基\[46\]。

### 聚酮类

[聚酮是由](../Page/聚酮.md "wikilink")[乙醯基及](../Page/乙醯基.md "wikilink")[丙醯輔酶A的子單位組成](../Page/丙醯輔酶A.md "wikilink")，藉由經典的酶聚合的產物
。其中包括大量動物、植物、細菌、真菌及海洋生物的[次級代謝產物及](../Page/次級代謝產物.md "wikilink")[天然產物](../Page/天然產物.md "wikilink")，在結構上有很大的不同\[47\]\[48\]。
許多聚酮是有環的分子，其主結構經[醣基化](../Page/醣基化.md "wikilink")、[甲基化](../Page/甲基化.md "wikilink")、[羥基化](../Page/羥基化.md "wikilink")、[氧化或是其他化學反應](../Page/氧化.md "wikilink")。許多常用的[抗菌藥](../Page/抗菌藥.md "wikilink")、及抗癌藥物是聚酮或其衍生物，例如[红霉素](../Page/红霉素.md "wikilink")、[四環素類抗生素](../Page/四環素類抗生素.md "wikilink")、[阿佛菌素及抗腫瘤的](../Page/阿佛菌素.md "wikilink")\[49\]。

## 生物功能

### 生物膜

真核細胞用生物膜分隔成數個[細胞器](../Page/細胞器.md "wikilink")，各自有不同的生物機能。[甘油磷脂是](../Page/甘油磷脂.md "wikilink")[生物膜的主要成份](../Page/生物膜.md "wikilink")，像[細胞膜和細胞器的](../Page/細胞膜.md "wikilink")，動物細胞是由細胞膜分隔細胞內和細胞外的環境。甘油磷脂是[兩親分子](../Page/兩親分子.md "wikilink")，分子中同時具有[親水性及](../Page/親水性.md "wikilink")[親脂性的基團](../Page/親脂性.md "wikilink")，其中以甘油為中心，藉由酯鍵連結到二個脂肪基的親脂性「尾巴」，另外一個酯鍵連結到一個磷酸的親水性「頭」。生物膜主要是以甘油磷脂為主，但也有一些沒有甘油的脂類，像[鞘磷脂](../Page/鞘磷脂.md "wikilink")、[膽固醇](../Page/膽固醇.md "wikilink")\[50\]。在植物及藻類中，
缺少磷酸基的磺酸基异鼠李糖基二脂酰基甘油（sulfoquinovosyldiacylglycerol）是葉綠體以其他有關細胞器膜的主要成份，也是高等植物、藻類及一些細菌的光合組織中最豐富的脂類。

植物的類囊體膜含有形成非雙層膜的單半乳糖甘油二酯（MGDG），且是其中比例最多的脂質，其中也有少量的磷脂。而葉綠體類囊體膜中用磁共振及電子顯微鏡也發現有動態的脂質雙層膜基質\[51\]。

雙層膜發現有高度的[雙折射](../Page/雙折射.md "wikilink")，可以用及[圓二色性來量測雙層膜的規則性或變型程度](../Page/圓二色性.md "wikilink")。

[Phospholipids_aqueous_solution_structures.svg](https://zh.wikipedia.org/wiki/File:Phospholipids_aqueous_solution_structures.svg "fig:Phospholipids_aqueous_solution_structures.svg")的[自組織](../Page/自組織.md "wikilink")，分別是球型的[脂質體](../Page/脂質體.md "wikilink")（liposome）、[膠束](../Page/膠束.md "wikilink")（micelle）及[脂質雙分子層](../Page/磷脂雙分子層.md "wikilink")（lipid
bilayer）\]\]

生物膜是種的[磷脂雙分子層](../Page/磷脂雙分子層.md "wikilink")，若[磷脂是在水溶液的環境中](../Page/磷脂.md "wikilink")，磷脂雙分子層的形成是能量考量偏好的過程\[52\]。這稱為[疏水效應](../Page/疏水效應.md "wikilink")。在水溶液中，磷脂極性的頭朝向極性的水分子，而疏水的尾巴減少對水的接觸，彼此距離更加緊密，形成[囊泡](../Page/囊泡.md "wikilink")。依脂質[浓度的不同](../Page/臨界膠束濃度.md "wikilink")，會生成[脂質體](../Page/脂質體.md "wikilink")、[膠束](../Page/膠束.md "wikilink")（micelle）及[脂質雙分子層](../Page/磷脂雙分子層.md "wikilink")。也觀察到有其他聚合的形式，都是[兩親分子的](../Page/兩親分子.md "wikilink")的一部份，這是[生物物理學學術研究的主題之一](../Page/生物物理學.md "wikilink")\[53\]\[54\]。在極性介質中生成[脂質體及](../Page/脂質體.md "wikilink")[膠束的過程稱為](../Page/膠束.md "wikilink")[疏水效應](../Page/疏水效應.md "wikilink")\[55\]。當在極性環境中溶解兩親性或是親脂性的物質，因此這些分子的極性分子（例如水溶液中的水）會更加有序
。所以在水溶液的環境中，在親脂性分子附近會有有序的結構\[56\]。

脂類形成[原始生命體生物膜的過程](../Page/原始生命體.md "wikilink")，是[生命起源的關鍵步驟](../Page/生命起源.md "wikilink")。

### 能量儲存

動植物體內的三酸甘油酯儲存在[脂肪組織內](../Page/脂肪組織.md "wikilink")，是動植物的主要能量來源之一。脂肪細胞設計為可連續生成或分解三酸甘油酯，而其分解主要是透過由荷爾蒙驅動的[脂酶來啟動](../Page/脂酶.md "wikilink")\[57\]。脂肪酸的完整氧化可以產生高熱量，約為9 kcal/g，而[醣和](../Page/醣.md "wikilink")[蛋白質氧化只能產生](../Page/蛋白質.md "wikilink")4 kcal/g的熱量，鳥類之所以可以在不進食的條件下長期間飛行，就是利用體內三酸甘油酯儲存的熱量\[58\]。

### 信號傳送

最近幾年的研究發現是[細胞信號傳送中基本的一部份](../Page/細胞信號傳送.md "wikilink")\[59\]\[60\]。脂質信號傳送可以由[G蛋白偶聯受體或是](../Page/G蛋白偶聯受體.md "wikilink")[核受體啟動](../Page/核受體.md "wikilink")，而且已發現許多不同種類的脂質是信號分子或是[第二信使系統的一部份](../Page/第二信使系統.md "wikilink")\[61\]。這類脂質包括，由神經酰胺衍生的鞘脂，是鈣調節\[62\]、細胞生長及凋亡有關的信息分子\[63\]，二酸甘油酯（DAG）及磷酸（PIPs）和[蛋白激酶C以鈣來引導的活化有關](../Page/蛋白激酶C.md "wikilink")\[64\]，[前列腺素是一種脂肪酸衍生的類二十烷酸](../Page/前列腺素.md "wikilink")，和[炎症和](../Page/炎症.md "wikilink")[免疫有關](../Page/免疫.md "wikilink")\[65\]，甾體荷爾蒙包括[雌激素](../Page/雌激素.md "wikilink")、[睾酮及](../Page/睾酮.md "wikilink")[皮質醇](../Page/皮質醇.md "wikilink")，調節像生殖、代謝及血壓等機能，像25-羟基胆固醇等是[肝X受體的](../Page/肝X受體.md "wikilink")[激動劑](../Page/激動劑.md "wikilink")\[66\]。

### 其他功能

脂溶性維生素（如[維生素A](../Page/維生素A.md "wikilink")、[D](../Page/維生素D.md "wikilink")、[E](../Page/維生素E.md "wikilink")、[K](../Page/維生素K.md "wikilink")）是萜烯的脂質，是人體必需的營養素，儲存在肝臟及脂肪組織中，有許多不同的功能。[肉鹼和脂肪酸的運輸及代謝時](../Page/肉鹼.md "wikilink")，進出線粒體有關，其中會進行[β-氧化](../Page/β-氧化.md "wikilink")\[67\]。在運輸[寡糖進出細胞膜的過程中](../Page/寡糖.md "wikilink")，聚異戊二烯和其磷酸化的衍生物也起到重要的作用的。聚異戊二烯醇的磷酸糖及二磷酸糖在細胞質外的糖基化反應、細胞外的生物多醣合成（例如細菌進行的[肽聚醣聚合](../Page/肽聚醣.md "wikilink")）及真核蛋白質的N-[糖基化中都有其作用](../Page/糖基化.md "wikilink")\[68\]\[69\]。[心磷脂是一種含有四個酰基及三個甘油基團的甘油酰磷脂](../Page/心磷脂.md "wikilink")，在線粒體內膜中相當豐富\[70\]\[71\]\[72\]。一般認為他們可以活化和[氧化磷酸化有關的酶](../Page/氧化磷酸化.md "wikilink")\[73\]。脂質也是形成甾體荷爾蒙的原料\[74\]。

## 代謝體學

人類和其他動物食物常見的脂質有動物及植物的三酸甘油酯、甾醇，和生物膜的磷脂。脂質代謝的過程可以合成及降解儲存的脂質，並產生個別組織需要的結構性及機能性的脂質。

### 生物合成

動物若攝取了過量的[醣類](../Page/醣類.md "wikilink")，過量的醣類會轉換為三酸甘油酯，過程中包括由乙酰辅酶A合成脂肪酸，以及將脂肪酸酯化形成三酸甘油酯，後者稱為\[75\]。[脂肪酸合酶合成脂肪酸的過程是先聚合](../Page/脂肪酸合酶.md "wikilink")，再還原乙酰辅酶A單元。脂肪酸中的酰鏈是在一連串的反應中延長，一開始先加入乙酰基，還原後得到醇類，[脫水得到烯類](../Page/脫水.md "wikilink")，再還原後得到烷類。生物合成脂肪酸的酶分為二類，在動物及真菌中，脂肪酸的合成反應是由單一的多功能蛋白質實現\[76\]，而在植物[色素體及細菌體內是由不同的酶分工進行](../Page/色素體.md "wikilink")\[77\]\[78\]。脂肪酸會轉換為三酸甘油酯，包裹在[脂蛋白中](../Page/脂蛋白.md "wikilink")，並在[肝臟中釋出](../Page/肝臟.md "wikilink")。

[不飽和脂肪酸的生成需要](../Page/不飽和脂肪酸.md "wikilink")反應，在脂肪醯基中引入雙鍵酸。在人體身內，[硬脂酸透過](../Page/硬脂酸.md "wikilink")會變成[油酸](../Page/油酸.md "wikilink")，是單元不飽和脂肪酸。但人體組織無法生成有二個雙鍵的[亚油酸及三個雙鍵的](../Page/亚油酸.md "wikilink")[Α-亞麻酸](../Page/Α-亞麻酸.md "wikilink")，因此這些多元不飽和脂肪酸需在飲食中攝取，稱為[必需脂肪酸](../Page/必需脂肪酸.md "wikilink")\[79\]。

三酸甘油酯的生成是在[內質網中進行](../Page/內質網.md "wikilink")，其中在乙酰辅酶A中的酰基轉換為甘油-3-磷酸及二酸甘油酯中的羥基\[80\]。

[萜烯和](../Page/萜烯.md "wikilink")[類萜](../Page/類萜.md "wikilink")（如[類胡蘿蔔素](../Page/類胡蘿蔔素.md "wikilink")）的生成是由[異戊二烯單元的組合和修飾](../Page/異戊二烯.md "wikilink")，異戊二烯單元是由活性的前驅體及提供\[81\]。前驅體的生成方式有許多種：在動物及古菌中會透過[甲羟戊酸途径](../Page/甲羟戊酸途径.md "wikilink")，由乙酰辅酶A產生這些化合物\[82\]，而在植物和細菌中用[丙酮酸及](../Page/丙酮酸.md "wikilink")[甘油醛3-磷酸來產生](../Page/甘油醛3-磷酸.md "wikilink")\[83\]\[84\]。會用到這些化合物的一種重要反應為[甾體生成反應](../Page/甾體.md "wikilink")，其中會結合異戊二烯單元，生成[鲨烯](../Page/鲨烯.md "wikilink")，再摺疊產生甾體環，生成[羊毛固醇](../Page/羊毛固醇.md "wikilink")\[85\]。羊毛固醇可以生成像[膽固醇及](../Page/膽固醇.md "wikilink")[麥角固醇等固醇](../Page/麥角固醇.md "wikilink")\[86\]\[87\]。

### 降解

脂肪酸的代謝是透過在[粒線體或](../Page/粒線體.md "wikilink")/及[過氧化體中進行的](../Page/過氧化體.md "wikilink")[β-氧化反應](../Page/β-氧化.md "wikilink")，產生[乙醯輔酶A](../Page/乙醯輔酶A.md "wikilink")。大部份的情形中，脂肪酸氧化的機制類似脂肪酸合成的逆反應。在[脱氢](../Page/脱氢.md "wikilink")、[水合及](../Page/水合.md "wikilink")[氧化反應後](../Page/氧化.md "wikilink")，脂肪酸會脫落二個碳，藉由產生[酮酸](../Page/酮酸.md "wikilink")。乙醯輔酶A最後會由[三羧酸循環及](../Page/三羧酸循環.md "wikilink")[電子傳遞鏈](../Page/電子傳遞鏈.md "wikilink")，轉換為[三磷酸腺苷](../Page/三磷酸腺苷.md "wikilink")（ATP）、CO<sub>2</sub>及H<sub>2</sub>O。

若體內沒有葡萄糖或是含量不多時，三羧酸循環可以由乙醯輔酶A開始，並且分解脂肪以產生能量。

脂肪酸棕櫚酸酯在完全氧化後，可產生對應106個ATP的能量\[88\]。不飽和脂肪酸及奇數鏈長的脂肪酸需要額外的酶反應才能降解。

## 营养和健康

大部份食物中的脂質是三酸甘油酯、甾醇和磷脂。若食物中有一些脂質，有助於脂溶性的[維生素](../Page/維生素.md "wikilink")（如[維生素A](../Page/維生素A.md "wikilink")、[D](../Page/維生素D.md "wikilink")、[E](../Page/維生素E.md "wikilink")、[K](../Page/維生素K.md "wikilink")）及[類胡蘿蔔素的吸收](../Page/類胡蘿蔔素.md "wikilink")\[89\]。人類和其他哺乳類因為無法合成一些特定的脂肪酸，需要藉由食物攝取，稱為[必需脂肪酸](../Page/必需脂肪酸.md "wikilink")，例如[ω-6脂肪酸的](../Page/ω-6脂肪酸.md "wikilink")[亚油酸及ω](../Page/亚油酸.md "wikilink")-3脂肪酸的[α-亞麻酸](../Page/α-亞麻酸.md "wikilink")\[90\]
。上述兩種脂肪酸都是18個碳的[多元不飽和脂肪酸](../Page/多元不飽和脂肪.md "wikilink")，但雙鍵的數量和位置有所不同。大部份的[植物油含有大量的亚油酸](../Page/植物油.md "wikilink")，像是[紅花油](../Page/紅花.md "wikilink")、[葵花籽油及](../Page/葵花籽油.md "wikilink")[玉米油等](../Page/玉米油.md "wikilink")。α-亞麻酸則主要是在植物的葉子及以一些特定的種子、核果及豆類中，例如[亞麻](../Page/亞麻.md "wikilink")、[油菜籽](../Page/油菜籽.md "wikilink")、[核桃及](../Page/核桃.md "wikilink")[大豆](../Page/大豆.md "wikilink")\[91\]。[魚油中有大量長鏈的](../Page/魚油.md "wikilink")[ω-3脂肪酸](../Page/ω-3脂肪酸.md "wikilink")，例如[二十碳五烯酸](../Page/二十碳五烯酸.md "wikilink")（EPA）和[二十二碳六烯酸](../Page/二十二碳六烯酸.md "wikilink")（DHA）。\[92\]許多研究顯示攝取ω-3脂肪酸對於嬰兒發展、癌症及[心血管疾病的預防](../Page/心血管疾病.md "wikilink")，以及像抑鬱症，注意力缺陷多動障礙和癡呆等精神疾病的的預防都有幫助\[93\]\[94\]。相反的，攝取由植物油[部份氫化產生的](../Page/氫化.md "wikilink")[反式脂肪是可能造成心血管疾病的危險因子](../Page/反式脂肪.md "wikilink")\[95\]\[96\]\[97\]。

許多研究指出每日脂肪的攝取量和肥胖症\[98\]\[99\]及糖尿病\[100\]\[101\]的風險有正相關。不過也有許多研究指出脂肪的攝取量和這些疾病沒有相關性，這些研究包括針對約五萬名婦女為期八年的饮食调整试验、護士健康研究以及衛生專業人員的隨訪研究等\[102\]\[103\]\[104\]。這些研究認為熱量中來自脂肪的比例和癌症、心臟疾病和體重的增加沒有關係。營養系的網站Nutrition
Source總結了飲食中總脂肪量對人體的影響：「詳細的研究（其中大部份是在哈佛進行）指出，飲食中總脂肪量和體重的變化或是疾病沒有關係。」\[105\]。

## 相關條目

  -
  - [必需脂肪酸](../Page/必需脂肪酸.md "wikilink")

  -
  -
  -
  - [脂蛋白](../Page/脂蛋白.md "wikilink")

  - [蛋白質脂類相互作用](../Page/蛋白質脂類相互作用.md "wikilink")

  -
## 参考文献

## 外部連結

  - 入門

<!-- end list -->

  - [脂質相關網站列表](http://www.cyberlipid.org/cyberlip/link0041.htm)
  - [天然脂質學入口網站](http://www.lipidmaps.org/) - 近期脂質研究之摘要
  - [脂質資料庫](https://web.archive.org/web/20060414162931/http://www.lipidlibrary.co.uk/)
    - 脂質化學與生物化學之文獻
  - [Cyberlipid.org](http://www.cyberlipid.org/) - 脂質相關歷史與資源
  - [電腦模擬分子](http://www.fos.su.se/~sasha/Lipid_membranes.html) - 膜脂模型
  - [脂質、膜和囊泡運輸](https://web.archive.org/web/20050621082405/http://www.biochemweb.org/lipids_membranes.shtml)
    - 生物化學和細胞生物學的虛擬圖書館

<!-- end list -->

  - 命名

<!-- end list -->

  - [IUPAC命名法：脂質](http://www.chem.qmul.ac.uk/iupac/lipid/)
  - [IUPAC詞彙表：脂質類分子](http://www.chem.qmul.ac.uk/iupac/class/lipid.html)

<!-- end list -->

  - 数据库

<!-- end list -->

  - [LIPID MAPS](http://www.lipidmaps.org/data/databases.html) -
    脂質及其相關之基因/蛋白質資料
  - [LipidBank](http://lipidbank.jp/) - 脂質與其相關性質、光譜數據及文獻
  - [LIPIDAT](http://www.lipidat.tcd.ie/) - 磷脂和相關熱力學資訊

{{-}}

[脂類](../Category/脂類.md "wikilink")

1.

2.  Stryer *et al.*, p. 328.

3.  Stryer *et al.*, p. 330.

4.

5.

6.

7.

8.

9.
10.

11.

12.

13.

14.

15.

16. YashRoy R.C. (1987) 13-C NMR studies of lipid fatty acyl chains of
    chloroplast membranes. *Indian Journal of Biochemistry and
    Biophysics*, vol. 24(6), pp.
    177-178.https://www.researchgate.net/publication/230822408_13-C_NMR_studies_of_lipid_fatty_acyl_chains_of_chloroplast_membranes?ev=prf_pub

17.

18.

19.

20.

21.

22. [van Holde and Mathews](../Page/#Holde.md "wikilink"), p. 630–31.

23.

24.

25.

26.

27.

28.

29. [van Holde and Mathews](../Page/#Holde.md "wikilink"), p. 844.

30.

31. Merrill AH, Sandhoff K. (2002). ["Sphingolipids: metabolism and cell
    signaling"](http://bio.ijs.si/~krizaj/group/Predavanja%202011/Biochemistry%20Lipids%20Lipoproteins%20and%20Membranes/14.pdf),
    Ch. 14 in *New Comprehensive Biochemistry: Biochemistry of Lipids,
    Lipoproteins, and Membranes*, Vance, D.E. and Vance, J.E., eds.
    Elsevier Science, NY, ISBN 978-0-12-182212-5.

32. [Devlin](../Page/#Devlin.md "wikilink"), pp. 421–22.

33.

34.

35.

36.

37. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), p. 749.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), pp. 329–331

51. YashRoy R.C. (1990) Magnetic resonance studies of the dynamic
    organisation of lipids of chloroplast membranes. *Journal of
    Biosciences*, vol. 15(4), pp.
    281-288.https://www.researchgate.net/publication/225688482_Magnetic_resonance_studies_of_dynamic_organisation_of_lipids_in_chloroplast_membranes?ev=prf_pub

52. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), pp. 333–34.

53.

54.

55.

56.

57.

58. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), p. 619.

59.

60.

61.

62.

63.

64.

65.

66.

67.

68.

69.

70.

71.

72.

73.

74. [Steroids](http://www.elmhurst.edu/~chm/vchembook/556steroids.html)
    . Elmhurst.edu. Retrieved on 2013-10-10.

75. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), p. 634.

76.

77.

78.

79. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), p. 643.

80. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), pp. 733–39.

81.

82.

83.
84.

85.

86.
87.

88. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), pp. 625–26.

89. [Bhagavan](../Page/#Bhagavan.md "wikilink"), p. 903.

90. [Stryer *et al.*](../Page/#Stryer.md "wikilink"), p. 643.

91.

92. [Bhagavan](../Page/#Bhagavan.md "wikilink"), p. 388.

93.

94.

95.

96.

97.

98.

99.

100.

101.

102.

103.

104.

105.