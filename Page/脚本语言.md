**脚本语言**（）是为了缩短传统的「编写、编译、链接、运行」（edit-compile-link-run）过程而创建的计算机[编程语言](../Page/编程.md "wikilink")。早期的脚本语言经常被称为[批处理语言或](../Page/批处理.md "wikilink")[工作控制语言](../Page/工作控制语言.md "wikilink")。一个脚本通常是解释运行而非编译。脚本語言通常都有簡單、易學、易用的特性，目的就是希望能讓[程式設計師快速完成](../Page/程式設計師.md "wikilink")[程式的編寫工作](../Page/计算机程序.md "wikilink")。而[巨集語言則可視為脚本語言的分支](../Page/巨集語言.md "wikilink")，兩者也有實質上的相同之處。

虽然许多脚本语言都超越了计算机简单任务自动化的领域，成熟到可以编写精巧的程序，但仍然还是被称为脚本。几乎所有计算机系统的各个层次都有一种脚本语言。包括操作系统层，如[计算机游戏](../Page/计算机游戏.md "wikilink")，[网络应用程序](../Page/网络应用程序.md "wikilink")，字处理文档，网络[软件等](../Page/软件.md "wikilink")。在许多方面，高级[编程语言和](../Page/编程语言.md "wikilink")**脚本语言**之间互相交叉，二者之间没有明确的界限。

## 起源

脚本語言是一种[電腦程式語言](../Page/電腦程式語言.md "wikilink")，因此也能讓開發者藉以編寫出讓電腦聽命行事的程式。以簡單的方式快速完成某些複雜的事情通常是創造腳本語言的重要原則，基於這項原則，使得腳本語言通常比[C語言](../Page/C語言.md "wikilink")、[C++語言或](../Page/C++語言.md "wikilink")
[Java之類的](../Page/Java.md "wikilink")[系統程式語言要簡單容易](../Page/系統程式語言.md "wikilink")，也讓腳本語言另有一些屬於腳本語言的特性：\[1\]

  - 語法和結構通常比較簡單
  - 學習和使用通常比較簡單
  - 通常以容易修改程式的「直譯」作為執行方式，而不需要「編譯」
  - 程式的開發產能優於執行效能

一个脚本可以使得本来要用[键盘进行的相互式操作自动化](../Page/键盘.md "wikilink")。一个[Shell脚本主要由原本需要在](../Page/Shell.md "wikilink")[命令行输入的命令组成](../Page/命令行.md "wikilink")，或在一个[文本编辑器中](../Page/文本编辑器.md "wikilink")，用户可以使用脚本来把一些常用的操作组合成一组序列。主要用来书写这种脚本的语言叫做脚本语言。很多脚本语言实际上已经超过简单的用户命令序列的指令，还可以编写更复杂的程序。

## 定义

计算机语言是为了各种目的和任务而开发的，一个常见任务就是把各种不同的已有组件连接起来以完成相关任务。大多脚本语言共性是：良好的快速开发，有效率的执行，解释而非编译执行，和其它语言编写的程序组件之间通信功能很强大。

许多脚本语言用来执行一次性任务，尤其是系统管理方面。它可以把服务组件粘合起来，因此被广泛用于GUI创建或者命令行，[操作系统通常提供一些默认的脚本语言](../Page/操作系统.md "wikilink")，即通常所谓shell脚本语言。

脚本通常以文本（如[ASCII](../Page/ASCII.md "wikilink")）保存，只在被调用时进行解释或编译。

有些脚本是为了特定领域设计的，但通常脚本都可以写更通用的脚本。在大型项目中经常把脚本和其它低级编程语言一起使用，各自发挥优势解决特定问题。脚本经常用于设计互动通信，它有许多可以单独执行的命令，可以做很高级的操作，（如在传统的[Unix
shell](../Page/Unix_shell.md "wikilink")（sh）中，大多操作就是程序本身。）

这些高级命令简化了代码编写过程。诸如内存自动管理和溢出检查等性能问题可以不用考虑。在更低级或非脚本语言中，[内存及变量管理和数据结构等耗费人工](../Page/内存.md "wikilink")，为解决一个给定问题需要大量代码，当然这样能够获得更为细致的控制和优化。脚本缺少优化程序以提速或者降低[内存的伸缩性](../Page/内存.md "wikilink")。

综上所述，脚本编程速度更快，且脚本文件明显小于如同类C程序文件。这种灵活性是以执行效率为代价的。脚本通常是解释执行的，速度可能很慢，且运行时更耗内存。在很多案例中，如编写一些数十行的小脚本，它所带来的编写优势就远远超过了运行时的劣势，尤其是在当前程序员工资趋高和硬件成本趋低时。

然而，在脚本和传统编程语言之间的界限越来越模糊，尤其是在一系列新语言及其集成常出现时。在一些脚本语言中，有经验的程序员可以进行大量优化工作。在大多现代系统中通常有多种合适的脚本语言可以选择，所以推荐使用多种语言（包括C或者汇编语言）编写一种脚本。

## 脚本语言分类

### 工作控制语言和shell

此类脚本用于自动化工作控制，即启动和控制系统程序的行为。大多的脚本语言解释器也同时是命令行界面，如[Unix
shell和MS](../Page/Unix_shell.md "wikilink")-DOS
COMMAND.COM。其他如AppleScript，可以为系统增加脚本环境，但没有命令行界面。

具体包括：

  - [4DOS](../Page/4DOS.md "wikilink")
  - [4NT](../Page/4NT.md "wikilink") / [Take
    Command](../Page/Take_Command.md "wikilink")
  - [AppleScript](../Page/AppleScript.md "wikilink")
  - [ARexx](../Page/ARexx.md "wikilink")（Amiga Rexx）
  - [bash](../Page/bash.md "wikilink")
  - [csh](../Page/C_shell.md "wikilink")
  - [DCL](../Page/DIGITAL_Command_Language.md "wikilink")
  - [JCL](../Page/Job_Control_Language.md "wikilink")
  - [ksh](../Page/Korn_shell.md "wikilink")
  - [Cmd.exe
    batch](../Page/Cmd.exe.md "wikilink")（[Windows](../Page/Microsoft_Windows.md "wikilink"),
    [OS/2](../Page/OS/2.md "wikilink")）
  - [Command.com
    batch](../Page/Command.com.md "wikilink")（[DOS](../Page/DOS.md "wikilink")）
  - [REXX](../Page/REXX.md "wikilink")
  - [tcsh](../Page/tcsh.md "wikilink")
  - [sh](../Page/Unix_shell.md "wikilink")
  - [Winbatch](../Page/Winbatch.md "wikilink")
  - [Windows PowerShell](../Page/Windows_PowerShell.md "wikilink")
  - [Windows Script Host](../Page/Windows_Script_Host.md "wikilink")
  - [zsh](../Page/Z_shell.md "wikilink")

### GUI脚本

[GUI出现带来一种专业的控制计算机的脚本语言](../Page/GUI.md "wikilink")。它在用户和图形界面，[菜单](../Page/選單.md "wikilink")，按钮等之间互动。它经常用来自动化重复性动作，或设置一个标准状态。理论上它可以用来控制运行于基于GUI的计算机上的所有[应用程序](../Page/应用程序.md "wikilink")，但实际上这些语言是否被支持还要看应用程序和操作系统本身。当通过键盘进行互动时，这些语言也被称为巨集语言。

具体包括：

  - [AutoHotkey](../Page/AutoHotkey.md "wikilink")
  - [AutoIt](../Page/AutoIt.md "wikilink")
  - [Expect](../Page/Expect.md "wikilink")

### 应用程序定制的脚本语言

许多大型的应用程序都包括根据用户需求而定制的惯用脚本语言。同样地，许多电脑游戏系统使用一种自定义脚本语言来表现NPC（non-player
character,non-playable character,non-player class）和游戏环境的预编程动作。

此类语言通常是为一个单独的应用程序所设计，虽然它们貌似一些通用语言（如QuakeC, modeled after C），但它们有自定义的功能。

具体包括：

  - [Action Code Script](../Page/Action_Code_Script.md "wikilink")
  - [ActionScript](../Page/ActionScript.md "wikilink")
  - [AutoLISP](../Page/AutoLISP.md "wikilink")
  - [BlobbieScript](../Page/BlobbieScript.md "wikilink")
    [1](http://www.wocmud.org/Carnage/blobbieScript/)
  - [Emacs Lisp](../Page/Emacs_Lisp.md "wikilink")
  - [Game Maker Language](../Page/Game_Maker_Language.md "wikilink")
  - [HyperTalk](../Page/HyperTalk.md "wikilink")
  - [IPTSCRAE](../Page/IPTSCRAE.md "wikilink")
  - [IRC script](../Page/IRC_script.md "wikilink")
  - [Lingo](../Page/Lingo.md "wikilink")
  - [Matlab Embedded
    Language](../Page/Matlab_Embedded_Language.md "wikilink")
  - [Maya Embedded
    Language](../Page/Maya_Embedded_Language.md "wikilink")
  - [mIRC script](../Page/mIRC_script.md "wikilink")
  - [NWscript](../Page/NWscript.md "wikilink")
  - [QuakeC](../Page/QuakeC.md "wikilink")
  - [UnrealScript](../Page/UnrealScript.md "wikilink")
  - [Visual Basic for
    Applications](../Page/Visual_Basic_for_Applications.md "wikilink")
  - [VBScript](../Page/VBScript_programming_language.md "wikilink")
  - [ZZT-oop](../Page/ZZT-oop.md "wikilink")

### [WEB编程脚本](../Page/WEB.md "wikilink")

应用程序定制的脚本语言中有一种重要的类别，用于提供WEB页面的自定义功能。它专业处理互联网通信，使用[网页浏览器作为用户界面](../Page/网页浏览器.md "wikilink")。当然，大多现代WEB编程语言都比较强大可以做一些通用编程。

具体包括：

  - [ColdFusion](../Page/ColdFusion.md "wikilink")（Application Server）
  - [Lasso](../Page/Lasso_programming_language.md "wikilink")
  - [Miva](../Page/Miva.md "wikilink")
  - [SMX](../Page/SMX.md "wikilink")
  - [IPTSCRAE](../Page/IPTSCRAE.md "wikilink")-*一些网络开发团队用于创建论坛的工具*

### 文本处理语言

处理基于文本的记录是脚本语言最早的用处之一。如Unix's
awk最早是设计来帮助系统管理员处理调用[UNIX基于文本的配置和LOG文件](../Page/UNIX.md "wikilink")。Perl最早是用来产生报告的，现在它已经成了全面成熟的语言。

具体包括：

  - [Awk](../Page/Awk.md "wikilink")
  - [Perl](../Page/Perl.md "wikilink")
  - [sed](../Page/sed.md "wikilink")
  - [XSLT](../Page/XSL_Transformations.md "wikilink")

### 通用动态语言

一些语言，比如Perl，从一门脚本语言发展成了更通用的编程语言。由于“解释执行，内存管理，动态”等特性，它们仍被称为脚本语言。但它们已经用于应用程序编写，用户也不把它们看作脚本语言。

  - [APL](../Page/APL語言.md "wikilink")
  - [Dao](../Page/道语言.md "wikilink")
  - [Dylan](../Page/Dylan.md "wikilink")
  - [Groovy](../Page/Groovy.md "wikilink")
  - [Lua](../Page/Lua.md "wikilink")
  - [MUMPS](../Page/MUMPS.md "wikilink")（M）
  - [newLISP](../Page/newLISP.md "wikilink")
  - [Nuva](../Page/Nuva.md "wikilink")
  - [Perl](../Page/Perl.md "wikilink")
  - [PHP](../Page/PHP.md "wikilink")
  - [Python](../Page/Python.md "wikilink")
  - [Ruby](../Page/Ruby.md "wikilink")
  - [Scheme](../Page/Scheme.md "wikilink")
  - [Smalltalk](../Page/Smalltalk.md "wikilink")
  - [SuperCard](../Page/SuperCard.md "wikilink")
  - [Tcl](../Page/Tcl.md "wikilink")（Tool command language）

### 扩展/可嵌入语言

少数的语言被设计通过嵌入应用程序来取代应用程序定制的脚本语言。开发者（如使用C等其它系统语言）包入使脚本语言可以控制应用程序的hook。这些语言和应用程序定制的脚本语言是同种用途，但优点在于可以在应用程序之间传递一些技能。

具体包括：

  - [Ch](../Page/Ch_interpreter.md "wikilink")（C/C++ interpreter）
  - [Dao](../Page/道语言.md "wikilink")
  - [ECMAScript亦称](../Page/ECMAScript.md "wikilink")[DMDScript](../Page/DMDScript.md "wikilink"),
    [JavaScript](../Page/JavaScript.md "wikilink")
  - [GameMonkeyScript](../Page/GameMonkey_Script.md "wikilink")
  - [Guile](../Page/Guile.md "wikilink")
  - [ICI](../Page/ICI_programming_language.md "wikilink")
  - [Lua](../Page/Lua.md "wikilink")
  - [RBScript](../Page/RBScript.md "wikilink")（REALbasic Script）
  - [Squirrel](../Page/Squirrel_programming_language.md "wikilink")
  - [Tcl](../Page/Tcl.md "wikilink")
  - [Z-Script](../Page/Z_Script.md "wikilink")

JavaScript直到现在仍然是网页浏览器内的主要编程语言，它的ECMAScript标准化保证了它成为流行的通用嵌入性语言。

Tcl作为一种扩展性语言而创建，但更多地被用作通用性语言，就如同Python, Perl, Ruby一样。

### 其它

  - [BeanShell](../Page/BeanShell.md "wikilink")（scripting for Java）
  - [CobolScript](../Page/CobolScript.md "wikilink")
  - [Escapade (server side scripting)](../Page/Escapade.md "wikilink")
  - [Euphoria](../Page/Euphoria_programming_language.md "wikilink")
  - [F-Script](../Page/F-Script_programming_language.md "wikilink")
  - [Ferite](../Page/Ferite.md "wikilink")
  - [Groovy](../Page/Groovy.md "wikilink")
  - [Gui4Cli](../Page/Gui4Cli.md "wikilink")
  - [Io](../Page/Io_programming_language.md "wikilink")
  - [KiXtart](../Page/KiXtart.md "wikilink")
  - [Mondrian](../Page/Mondrian_programming_language.md "wikilink")
  - [Object REXX](../Page/ObjectRexx.md "wikilink")
  - [Pike](../Page/Pike_programming_language.md "wikilink")
  - [Pliant](../Page/Pliant.md "wikilink")
  - [REBOL](../Page/REBOL.md "wikilink")
  - [ScriptBasic](../Page/ScriptBasic.md "wikilink")
  - [Shorthand Language](../Page/Shorthand_Language.md "wikilink")
  - [Simkin](../Page/Simkin_\(language\).md "wikilink")
  - [Sleep](../Page/Sleep_programming_language.md "wikilink")
  - [StepTalk](../Page/StepTalk.md "wikilink")
  - [Visual DialogScript](../Page/Visual_DialogScript.md "wikilink")

## 常见的脚本语言

  - [C Shell](../Page/C_Shell.md "wikilink")
  - [JavaScript](../Page/JavaScript.md "wikilink")
  - [Nuva](../Page/Nuva编程语言.md "wikilink")
  - [Perl](../Page/Perl.md "wikilink")
  - [PHP](../Page/PHP.md "wikilink")
  - [Python](../Page/Python.md "wikilink")
  - [Ruby](../Page/Ruby.md "wikilink")
  - [Tcl](../Page/Tcl.md "wikilink")

## 参见

[Domain-specific programming
language](../Page/Domain-specific_programming_language.md "wikilink")

## 参考文献

## 外部链接

  - [A study of the Script-Oriented Programming (SOP) suitability of
    selected
    languages](http://merd.sourceforge.net/pixel/language-study/scripting-language/)
    – from The Scriptometer.
  - [Hotscripts.com](http://www.hotscripts.com) -A collection of many
    scripts written in an assortment of languages
  - [OpenSourceScripts.com](http://www.opensourcescripts.com) -A
    collection of open source scripts
  - [A Slightly Skeptical View on Scripting
    Languages](http://www.softpanorama.org/Articles/a_slightly_skeptical_view_on_scripting_languages.shtml)
    by Dr. Nikolai Bezroukov
  - [Scripting: Higher Level Programming for the 21st
    Century](http://home.pacbell.net/ouster/scripting.html) by John K.
    Ousterhout
  - [Are Scripting Languages Any Good? A Validation of Perl, Python,
    Rexx, and Tcl against C, C++, and Java
    (PDF)](http://page.mi.fu-berlin.de/~prechelt/Biblio/jccpprt2_advances2003.pdf)—2003
    study
  - [Free Classified ASP Script Site
    Software](http://canadaclassify.com)
  - [學習Shell
    Scripts](http://linux.vbird.org/linux_basic/0340bashshell-scripts.php)鳥哥的Linux私房菜

{{-}}

[脚本语言](../Category/脚本语言.md "wikilink")

1.