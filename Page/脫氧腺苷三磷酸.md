**去氧腺苷三磷酸**（**Deoxyadenosine
triphosphate**，**dATP**）是一種[去氧核苷酸三磷酸](../Page/去氧核苷酸三磷酸.md "wikilink")（dNTP），結構與[腺苷三磷酸](../Page/腺苷三磷酸.md "wikilink")（ATP）相似，但少了一個位於[五碳糖](../Page/五碳糖.md "wikilink")2號碳上的[-OH基](../Page/羥基.md "wikilink")，取而代之的是單獨的[氫原子](../Page/氫.md "wikilink")。若移去接在五碳糖3號碳上的氧原子，則會產生[ddATP](../Page/ddATP.md "wikilink")。此外，dATP是[DNA聚合酶在](../Page/DNA聚合酶.md "wikilink")[DNA複製過程中](../Page/DNA複製.md "wikilink")，用來合成DNA長鏈的原料之一。
















[Category:核苷酸](../Category/核苷酸.md "wikilink")