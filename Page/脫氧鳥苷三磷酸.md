**去氧鳥苷三磷酸**（****；****）是[核苷三磷酸的一種](../Page/核苷三磷酸.md "wikilink")，與[鳥苷三磷酸相似](../Page/鳥苷三磷酸.md "wikilink")，但少了一個氧原子。是合成DNA的原料之一。


[Category:核苷酸](../Category/核苷酸.md "wikilink")