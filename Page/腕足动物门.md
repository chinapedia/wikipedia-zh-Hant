**腕足动物门**（[學名](../Page/學名.md "wikilink")：）是[动物界的一个](../Page/动物界.md "wikilink")[门](../Page/門_\(生物\).md "wikilink")，屬於[底栖](../Page/底棲生物.md "wikilink")、有一對硬壳的[触手冠](../Page/觸手冠動物.md "wikilink")[海产动物](../Page/海.md "wikilink")。但與[雙殼類動物不同的是](../Page/雙殼類.md "wikilink")：其殼是上、下開合，而不是左、右開合。[鉸位在後背部](../Page/鉸齒.md "wikilink")，而前方可開合作捕食或防御。牠們自[寒武紀開始演化](../Page/寒武紀.md "wikilink")，现存的种类多分布在高[纬度的冷水区](../Page/纬度.md "wikilink")。

## 語源

腕足動物的學名*brachiopod*源於[古希臘語](../Page/古希臘語.md "wikilink")，由「手臂」（）與「足」（）兩部份組成。

## 特征

介壳两枚，大小相等或不等，掩盖背腹两面；介壳的形状和饰纹以及内部器官的构造，是鉴定腕足纲属、种的依据；身体柔软、左右对称；头顶有突出部；上生许多触手，称为“腕足”；消化道呈U字形弯曲，常缺少肛门；具有体腔和后肾。

## 分類

[Pygites_diphyoides_(d'Orbigny).jpg](https://zh.wikipedia.org/wiki/File:Pygites_diphyoides_\(d'Orbigny\).jpg "fig:Pygites_diphyoides_(d'Orbigny).jpg")[穆爾西亞](../Page/穆爾西亞.md "wikilink")[塞埃欣的](../Page/塞埃欣.md "wikilink")[早白堊世](../Page/早白堊世.md "wikilink")[豪特里維期地層出土的](../Page/豪特里維期.md "wikilink")*Pygites
diphyoides* ([d'Orbigny](../Page/d'Orbigny.md "wikilink"),
1849)化石。這件[穿孔贝目化石的特色是其殼上位於中央的孔洞](../Page/穿孔贝目.md "wikilink")。\]\]
腕足动物的分類有其獨特的不同之處：現時腕足動物的現存物種與化石物種有着不同的特色。其化石物種的外形變化很大，但其外殼只有少量特徵；而現存物種的外殼變化不大，反而其軟體及外殼均各有特徵。若我們只專注於其中一類特徵，均不能把另一類好好的分類。本門動物也經歷了顯著的[趨同進化和](../Page/趨同進化.md "wikilink")[逆轉演化](../Page/逆轉演化.md "wikilink")（這使較新的組別損失了原來在中間組別出現的特色，又或回復了原來只在較舊的組別出現的特點）。因此，有些分類學的學者認為現時我們仍然未成熟到可以具體把[目以上的分類確定](../Page/目_\(生物\).md "wikilink")，建議只適宜透過自底向上的方法，先確定物種的種屬，然後再把他們分成組。不過，亦有學者不同意這個觀點，認為現時的
然而，其他分類學家相信，物種中某些模式的特點已足夠穩定，值得作更高級別的分類，儘管大家可能對更高級別的分類有不同的看法。以下為現時較多人同意的分類法。

### 目

以下為腕足動物門以下的[目](../Page/目_\(生物\).md "wikilink")：

  - [舌形貝型亞門](../Page/舌形貝型亞門.md "wikilink")（Linguliformea）
      - [舌形貝綱](../Page/舌形貝綱.md "wikilink")（Lingulata）
          - †[頂孔貝目](../Page/頂孔貝目.md "wikilink")（Acrotretida，又名[乳房贝目](../Page/乳房贝目.md "wikilink")）
          - [舌形貝目](../Page/舌形貝目.md "wikilink")（Linguilida）
          - †[管洞貝目](../Page/管洞貝目.md "wikilink")（Siphonotretida）
      - †[神父貝綱](../Page/神父貝綱.md "wikilink")（Paterinata）
          - †[神父贝目](../Page/神父贝目.md "wikilink")（Paterinida）
  - [髑髅贝型亚门](../Page/髑髅贝型亚门.md "wikilink")（Craniiformea）
      - [頭殻綱](../Page/頭殻綱.md "wikilink")（Craniata，又名[髑髏貝綱](../Page/髑髏貝綱.md "wikilink")、[顱形貝綱](../Page/顱形貝綱.md "wikilink")）
          - [頭殻目](../Page/頭殻目.md "wikilink")（Craniida，又名[髑髏貝目](../Page/髑髏貝目.md "wikilink")、[顱形貝目](../Page/顱形貝目.md "wikilink")）
          - †[擬髑髏貝目](../Page/擬髑髏貝目.md "wikilink")（Craniopsida，又名[擬顱形貝目](../Page/擬顱形貝目.md "wikilink")）
          - †[三分貝目](../Page/三分貝目.md "wikilink")（Trimerellida）
  - [小嘴貝型亞門](../Page/小嘴貝型亞門.md "wikilink")（Rhynchonelliformea）
      - [小嘴貝綱](../Page/小嘴貝綱.md "wikilink")（Rhynchonellata）
          - †[前正形貝目](../Page/前正形貝目.md "wikilink")（Protorthida）
          - †[正形贝目](../Page/正形贝目.md "wikilink")（Orthida）
          - †[五房贝目](../Page/五房贝目.md "wikilink")（Pentamerida）
          - †[无洞贝目](../Page/无洞贝目.md "wikilink")（Atrypida，已廢棄的[並系群分類](../Page/並系群.md "wikilink")）
          - [小嘴贝目](../Page/小嘴贝目.md "wikilink")（Rhynchonellida）
          - †[石燕贝目](../Page/石燕贝目.md "wikilink")（Spiriferida）
          - †[無窗貝目](../Page/無窗貝目.md "wikilink")（Athyridida）
          - †[莢采貝目](../Page/莢采貝目.md "wikilink")（Spiriferinida）
          - [穿孔贝目](../Page/穿孔贝目.md "wikilink")（Terebratulida）
          - [鞘貝目](../Page/鞘貝目.md "wikilink")（Thecideida）
      - †[扭月貝綱](../Page/扭月貝綱.md "wikilink")（Strophomenata）
          - †[扭月贝目](../Page/扭月贝目.md "wikilink")（Strophomenida）
          - †[长身贝目](../Page/长身贝目.md "wikilink")（Productida）
          - †[伯灵贝目](../Page/伯灵贝目.md "wikilink")（Billingsellida）
          - †[直形貝目](../Page/直形貝目.md "wikilink")（Orthotetida）
      - †[智利貝綱](../Page/智利貝綱.md "wikilink")（Chileata）
          - †[智利貝目](../Page/智利貝目.md "wikilink")（Chileida）
          - †目 Dictyonellida
      - †[庫圖貝綱](../Page/庫圖貝綱.md "wikilink")（Kutorginata）
          - †[庫圖貝目](../Page/庫圖貝目.md "wikilink")（Kutorginida）
      - †[小圓貨貝綱](../Page/小圓貨貝綱.md "wikilink")（Obolellata）
          - †[小圆货贝目](../Page/小圆货贝目.md "wikilink")（Obolellida）
          - †目 Naukatida

## 註釋

## 參考資料

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  -
  -
<!-- end list -->

  -
  -
<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 延伸閱讀

  -
## 外部連結

  - [UC-Berkeley Museum of
    Paleontology](http://www.ucmp.berkeley.edu/brachiopoda/brachiopoda.html)
  - [Palaeos
    Brachiopoda](https://web.archive.org/web/20060101163750/http://www.palaeos.com/Invertebrates/Lophotrochozoa/Brachiopoda/E0A0E0Brachiopoda.htm)
  - [BrachNet](http://paleopolis.rediris.es/BrachNet/)
  - [Information from the Kansas Geological
    Survey](https://web.archive.org/web/20080705173419/http://www.kgs.ku.edu/Extension/fossils/brachiopod.html)
  - [site of
    R.Filippi](http://perso.wanadoo.fr/jean-ours.filippi/brach/anglais/poursavoirplusang22.html)
  - [Brachiopoda World
    Database](http://paleopolis.rediris.es/Brachiopoda_Phoronida_databases/)

[Category:动物](../Category/动物.md "wikilink")
[Category:无脊椎动物](../Category/无脊椎动物.md "wikilink")
[Category:冠輪動物](../Category/冠輪動物.md "wikilink")
[\*](../Category/腕足动物门.md "wikilink")