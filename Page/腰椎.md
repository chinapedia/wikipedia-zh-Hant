**腰椎**（）在[腰部底下有](../Page/腰.md "wikilink")5塊，用以支持[背部](../Page/背.md "wikilink")。

## 附件圖片

## 参见

  - [人体骨骼列表](../Page/人体骨骼列表.md "wikilink")

## 註釋

## 參考文獻

  -
## 外部連結

  -
  -
  - [Back Pain Medical Journal for
    Patients](http://www.spine-health.com)

  - [Lower back pain
    information](http://www.medicinenet.com/low_back_pain/article.htm)



[category:解剖学](../Page/category:解剖学.md "wikilink")

[Category:骨骼](../Category/骨骼.md "wikilink")