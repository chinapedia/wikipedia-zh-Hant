**腾讯QQ**是[腾讯公司于](../Page/腾讯.md "wikilink")1999年2月11日推出的一款的[多平台](../Page/操作系统.md "wikilink")[即时通信软件](../Page/即时通信软件.md "wikilink")，从支持文字、语音和视频聊天，发展到带有文件共享、网络硬盘、邮箱、游戏、论坛，甚至是網購、租房與找工作等等廣泛服务的平台。

## 历史

[50px](../Page/文件:OICQLogo.gif.md "wikilink")
1998年11月7日，[腾讯公司成立](../Page/腾讯公司.md "wikilink")\[1\]，并于1998年11月7日和1999年1月26日分别注册了[域名](../Page/域名.md "wikilink")“oicq.com”和“oicq.net”\[2\]。1999年2月10日，腾讯正式推出其第一款[即时通信软件](../Page/即时通信软件.md "wikilink")“**OPEN-ICQ**”，简称**OICQ**，版本号为“99a
Build
1005”，基本功能有网络寻呼机、公共[聊天室](../Page/聊天室.md "wikilink")、传输文件\[3\]\[4\]。

到同年8、9月，腾讯收到[美国在线公司投诉](../Page/美国在线公司.md "wikilink")，指“oicq.com”和“oicq.net”这两个域名含有“[ICQ](../Page/ICQ.md "wikilink")”字样为侵犯其[知识产权](../Page/知识产权.md "wikilink")，并要求腾讯将域名免费转让给他们；当时的[ICQ是美国在线公司的一个即时通信软件](../Page/ICQ.md "wikilink")\[5\]。2000年3月3日，[美国仲裁论坛受理这两个域名的争议书](../Page/美国仲裁论坛.md "wikilink")，并致函通知腾讯向仲裁论坛提交域名争议答辩书，腾讯虽经多次交涉，但最终仲裁论坛于3月21日判定域名应当转让给美国在线。为避免同类事件的发生，腾讯于2000年4月启用新域名“tencent.com”和“tencent.net”，并将“OICQ”更名为“腾讯QQ”。2002年12月，腾讯向[中国国际经济贸易仲裁委员会域名争议解决中心提出对](../Page/中国国际经济贸易仲裁委员会.md "wikilink")“qq.com.cn”的域名投诉争议，但因该域名的注册时间早于腾讯提出“QQ”名称，最终投诉未果。到了2003年3月，腾讯以11万美元（包括1万美元的律师费）的价格从原持有人
罗伯特·亨茨曼手里购得域名“qq.com”，并开始使用“qq.com”作为腾讯QQ的主要域名\[6\]。
[Tencent_QQ.svg](https://zh.wikipedia.org/wiki/File:Tencent_QQ.svg "fig:Tencent_QQ.svg")
2000年6月21日，移动QQ首次集成于[移动电话的](../Page/移动电话.md "wikilink")[STK卡中](../Page/STK卡.md "wikilink")，用户可以执行基本的聊天功能，如收发信息、查询用户信息\[7\]。2003年8月17日，腾讯推出“[QQ游戏](../Page/QQ游戏.md "wikilink")”，并在11月推出的QQ2003III
Beta1里面集成QQ游戏\[8\]。2003年9月，腾讯推出企业级实时通信产品“腾讯通”（RTX），而在同年12月15日，腾讯又推出另外一款可与QQ互联互通的即时通信软件“[Tencent
Messenger](../Page/Tencent_Messenger.md "wikilink")”（简称TM），该软件则侧重于办公时或朋友间的通信\[9\]。

QQ推出接近两年后（2001年2月10日），腾讯QQ最高同时在线用户数达到100万；在4年后（2005年2月16日）达到1000万；5年后（2010年3月5日）达到1亿\[10\]\[11\]。

2000年12月，[广州市](../Page/广州市.md "wikilink")[东利行企业发展有限公司与腾讯签署QQ动漫画合作协议](../Page/东利行企业发展有限公司.md "wikilink")，独家拓展QQ动漫画领域，同年9月取得腾讯QQ企鹅商标和QQ形象版权的中国地区唯一使用权，创立Q-Gen品牌并开发、生产、销售QQ形象系列产品，这些产品包括服饰、公仔玩具、宣传品、文具、会员卡和电脑周边产品。Q-Gen是英文“Q-Generation”（QQ一代）的缩写。第一家Q-Gen专卖店于2001年10月5日在广州市[北京路步行街开业](../Page/北京路步行街.md "wikilink")，到2005年，在中国大陆内已开设超过200家连锁专卖店\[12\]。2005年，腾讯授权[三斯达鞋业有限公司开发出一系列QQ运动鞋](../Page/三斯达鞋业有限公司.md "wikilink")\[13\]。

2003年2月前，腾讯网（tencent.com）为腾讯OICQ/QQ软件的主页，之后为腾讯公司的官方网站；qq.com自2003年启用后则作为一个[综合网站出现](../Page/综合网站.md "wikilink")，除提供腾讯QQ软件的服务外，还提供[新闻](../Page/新闻.md "wikilink")、[娱乐](../Page/娱乐.md "wikilink")、[财经](../Page/财经.md "wikilink")、[游戏](../Page/游戏.md "wikilink")、[购物等多种服务](../Page/购物.md "wikilink")。根据[Alexa](../Page/Alexa.md "wikilink")[数据](http://www.alexa.com/siteinfo/qq.com)显示，现在腾讯网（qq.com）在全球[互联网流量排名位列第](../Page/互联网.md "wikilink")7位，在[中国位列第二](../Page/中国.md "wikilink")，仅次于[百度](../Page/百度.md "wikilink")。

2013年3月29日腾讯发布基于Facebook平台的QQ Chat。

### 官方版本

官方版本的腾讯QQ可以分为电脑版本\[14\]\[15\]和手机版本\[16\]。

<table>
<thead>
<tr class="header">
<th></th>
<th><p>运行平台</p></th>
<th><p>名称</p></th>
<th></th>
<th><p>发布日期</p></th>
<th><p>语言</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/电脑.md" title="wikilink">电脑</a></p></td>
<td><p><a href="../Page/Windows.md" title="wikilink">Windows</a></p></td>
<td></td>
<td><p>9.1.1</p></td>
<td></td>
<td><p><a href="../Page/简体中文.md" title="wikilink">简体中文</a></p></td>
<td><p>最早期的QQ于1999年2月11日推出，当时被命名为OICQ。之后QQ版本按年份命名，如QQ2000、QQ2001、QQ2002、QQ2003、QQ2010、QQ2011、QQ2012、QQ2013等。2014年1月7日起，新版QQ命名为QQ5.0，更换版本命名方式。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2.3.2</p></td>
<td></td>
<td><p><a href="../Page/简体中文.md" title="wikilink">简体中文</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>定位为企业办公用户</p></td>
<td><p>2.11</p></td>
<td></td>
<td><p><a href="../Page/英语.md" title="wikilink">英语</a>、<a href="../Page/法语.md" title="wikilink">法语</a>、<a href="../Page/德语.md" title="wikilink">德语</a>、<a href="../Page/西班牙语.md" title="wikilink">西班牙语</a>、<a href="../Page/日语.md" title="wikilink">日语</a>、<a href="../Page/韩语.md" title="wikilink">韩语</a>、<a href="../Page/繁体中文.md" title="wikilink">繁体中文</a></p></td>
<td><p>也叫QQ国际版。第一个QQ国际版Beta 1于2009年9月21日发布。而目前已知的第一个QQ英文版是Tencent QQ English Version 2000b Build 0710B。Beta 1-3是在QQ2009 SP2的基础上开发的，但大部分额外功能（如<a href="../Page/QQ空间.md" title="wikilink">QQ空间</a>、<a href="../Page/QQ游戏.md" title="wikilink">QQ游戏</a>、<a href="../Page/QQ音乐.md" title="wikilink">QQ音乐</a>）就没有被集成[17][18]。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>7.9</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>轻快简洁，高效沟通。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1.89</p></td>
<td></td>
<td><p>简体中文</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Preview2</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>TM是<a href="../Page/Tencent.md" title="wikilink">Tencent</a> Messenger的简称，是腾讯公司推出的一款面向个人的即时通讯软件，能够与QQ互联互通，具有无广告、抗骚扰、安静高效的特点，风格简约清新，侧重在办公环境中使用。[19]。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>5.4</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>Light设计风格，轻快、简洁、个性化</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>4.8</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>在安装了此版本之后再进行覆盖安装非Q+版的时候，Q+程序不会被卸载</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>QQ2012Beta2</p></td>
<td></td>
<td><p>繁体中文</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>v1.0 beta</p></td>
<td></td>
<td><p>简体中文</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>正式版</p></td>
<td></td>
<td><p>简体中文</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2011</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/泰语.md" title="wikilink">泰语</a></p></td>
<td><p>Sanook QQ为Sanook网站运营的腾讯QQ的本地化版本，Sanook QQ提供聊天、交友和QQ秀等服务。Sanook为Naspers Limited的全资子公司，而Naspers Limited为腾讯控股主要股东<a href="../Page/南非.md" title="wikilink">南非</a><a href="../Page/米拉德国际控股集团.md" title="wikilink">米拉德国际控股集团的控股公司</a>。[20]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Beta</p></td>
<td></td>
<td><p>英语</p></td>
<td><p>ibibo Messenger为腾讯QQ在<a href="../Page/印度.md" title="wikilink">印度的本地化版本</a>，由米拉德国际控股集团旗下的印度ibibo公司推出。[21]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2010</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/越南语.md" title="wikilink">越南语</a></p></td>
<td><p>Zing Chat为腾讯授权<a href="../Page/VinaGame.md" title="wikilink">VinaGame公司旗下的</a><a href="../Page/Zing.md" title="wikilink">Zing公司推出的越南语版QQ本地化即时通信软件</a>。Zing Chat在外观上没有任何腾讯的商标，甚至连腾讯QQ的形象企鹅也改为鸭子，但在风格和功能上与QQ十分类似。但是，其文件属性等字串中有腾讯及QQ信息残留，软件架构、目录结构也与QQ极为类似[22]。2007年，VinaGame将QQ重新包装并改名为Zing Chat，进入越南市场。两年后，Zing Chat在越南打败了<a href="../Page/雅虎通.md" title="wikilink">雅虎通</a>，成为越南用户数最多的即时通信软件[23]。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>2005 Beta2</p></td>
<td></td>
<td></td>
<td><p>QQ Italy是腾讯與<a href="http://www.sonky.com/qqitaly/">SonkySoft</a>合作推出的、基于简体中文版翻译的即时通信软件，但SonkySoft于2006年11月1日开始停止对Beta 2和之前的版本的支持服务[24]，2009年之后更停止下载，转而链接到QQ国际版的官方网站imqq.com。此外还有基于Luma QQ的QQ Italy for iPhone（最新版本为1.12.13.14，发布于2008年12月13日）[25]。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>英语</p></td>
<td><p>由米拉德国际控股集团旗下的<a href="../Page/MWeb.md" title="wikilink">MWeb公司推出</a>，不能与中国用户使用的QQ互通，已停止開發，被QQ国际版取代[26]。 &lt;!--"| <a href="../Page/电脑.md" title="wikilink">电脑</a></p></td>
<td><p>rowspan=" windows的QQ這裡結束了--&gt;</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Mac.md" title="wikilink">Mac</a></p></td>
<td></td>
<td><p>5.5.1</p></td>
<td></td>
<td><p>简体中文、英文</p></td>
<td><p>QQ for Mac的第一个版本为QQ for Mac 1.0 Preview 1，于2008年1月28日在腾讯的体验中心网站上正式推出[27]。腾讯表示为了追求稳定性能以及兼容性，试用版本没有承载过多功能[28]。除基本的聊天功能外，最新版还可以捉屏、发图片、传输文件、自定义头像等[29]。运行环境：Mac OS X v10.7 (Lion)或更高版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1.3.2</p></td>
<td></td>
<td><p>简体中文</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Linux.md" title="wikilink">Linux</a></p></td>
<td></td>
<td><p>1.0.2</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>QQ for Linux于2008年7月31日推出，且直至1.0 Beta 1版较Windows版本只有最基本的聊天功能；QQ for Linux支持<a href="../Page/32位.md" title="wikilink">32位和</a><a href="../Page/64位.md" title="wikilink">64位处理器的Linux系统</a>，如<a href="../Page/SuSE.md" title="wikilink">SuSE</a>、<a href="../Page/Ubuntu.md" title="wikilink">Ubuntu</a>、<a href="../Page/Fedora.md" title="wikilink">Fedora</a>、<a href="../Page/Lemote_Loonux.md" title="wikilink">Lemote Loonux</a>[30]。有用户报告QQ for Linux在好友列表的加载上存在一些问题，如不能正确显示好友数量、不能显示一部分未上线好友[31]，此外该QQ版本还与使用较为广泛的IBus输入平台（Ubuntu 10.04默认）相冲突，可能会造成聊天窗口一输入中文即出错退出。2010年12月22日更新的QQ For Linux 1.02版本，好友列表不能正确显示的问题得到了解决，并能更好的支持群聊天功能，但其他问题仍然没有明显改进[32]。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Web.md" title="wikilink">Web</a></p></td>
<td></td>
<td><p>3.0</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>Q+ Web即网页版QQ，旧称WebQQ，可在多种<a href="../Page/浏览器.md" title="wikilink">浏览器下进行聊天</a>，而无需下载和安装任何QQ软件，这样就可以部分解决跨平台聊天的问题。第一代WebQQ的第一个版本于2006年9月发布。WebQQ刚推出时功能较为简单，只能进行基本的聊天。[33]。第二代WebQQ于2010年9月14日发布，第二代除了包含第一代QQ的所有功能之外，还增加了皮肤设置、好友管理、云输入法（即不用输入法软件而直接在网页上输入中文）、应用管理器等。相对于第一代，第二代实现窗口式页面管理，模拟<a href="../Page/操作系统.md" title="wikilink">操作系统环境</a>[34]。第三代WebQQ新增多桌面操作、讨论组、Q+和音视频功能[35]。2011年10月起更名为Q+ Web[36]，新增云存储功能，改进应用管理功能，并允许用户将好友头像拖动到Q+ Web的桌面[37]。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Facebook.md" title="wikilink">Facebook</a></p></td>
<td></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>简体中文</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/手机.md" title="wikilink">手机</a></p></td>
<td><p><a href="../Page/Android.md" title="wikilink">Android</a></p></td>
<td></td>
<td><p>7.9.0</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>手机QQ Android版第一版于2010年3月30日推出[38]。运行环境：Android 4.0.3 及以上。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3.5.0.660</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适合低配置、小屏幕的安卓设备使用。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3.0.1</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>推荐配置：10寸分辨率1280*800 Android3.0以上；基本配置：7寸以上分辨率大于1024*600 Android2.2以上。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>2.2.2</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>推荐配置：7寸分辨率1024*600 Android2.2以上；基本配置：分辨率大于800*480 Android2.0及以上。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>5.2.0.6068</p></td>
<td></td>
<td><p>繁体中文、英语、日语、韩语、法语、西班牙语</p></td>
<td><p>QQ International的安卓版本。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/iPhone.md" title="wikilink">iPhone</a>,<a href="../Page/iPad.md" title="wikilink">iPad</a>,<a href="../Page/iPod_Touch.md" title="wikilink">iPod Touch</a></p></td>
<td></td>
<td><p>7.9.0</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>第一款iPhone版手机QQ于2008年10月6日在<a href="../Page/iTunes.md" title="wikilink">iTunes推出</a>[39]。兼容性：需要 iOS 5.0 或更高版本。[40]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>7.1.0</p></td>
<td></td>
<td><p>简体中文、英语</p></td>
<td><p>兼容性：需要 iOS 8.0 或更高版本。与 iPad 兼容。[41]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>4.6.12</p></td>
<td></td>
<td><p>簡體中文、繁体中文、英语、法语、日语、韩语、德語、西班牙语</p></td>
<td><p>QQ International的iPhone版本。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Phone.md" title="wikilink">Windows Phone</a></p></td>
<td></td>
<td><p>3.6</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>系统要求：Windows Phone 7.1[42]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>5.4</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>表情商店：免费表情，乐趣聊天,通用架构：Windows平板&amp;手机，同步更新</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Java.md" title="wikilink">Java</a></p></td>
<td></td>
<td><p>Beta1Build0016</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>运行环境：KJava2.0平台。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/S60.md" title="wikilink">S60</a></p></td>
<td></td>
<td><p>Beta1 Build0753</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适配机型：N70/N72/N90</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Beta1 Build0752</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适合其他S60V2手机</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>2013.0.0.1718</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>手机QQ S60第三版于2007年6月28日正式推出</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Beta3 Build1220</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>手机QQ S60版第五版于2010年6月3日正式推出 [43]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Beta2Build1202</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适配机型：Nokia N8/E7-00/C7-00/C6-01/500/603/700</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Mobile.md" title="wikilink">Windows Mobile</a></p></td>
<td></td>
<td><p>正式版 Build0094</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>正式版 Build0096</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/MTK.md" title="wikilink">MTK</a></p></td>
<td></td>
<td><p>Beta1 Build1022</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>该版本自带浏览器。</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Beta1 Build0020</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>该版本为腾讯推出的MTK平台的精简版，适用于小内存山寨机，支持后台，但去除了浏览器。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黑莓.md" title="wikilink">黑莓</a>、RIM</p></td>
<td></td>
<td><p>Beta2</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魅族M8.md" title="wikilink">魅族M8</a></p></td>
<td></td>
<td><p>V1.0</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Palm.md" title="wikilink">Palm</a></p></td>
<td></td>
<td><p>palm QQ 测试版</p></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>通用版（<a href="../Page/Java.md" title="wikilink">Java</a>）</p></td>
<td></td>
<td><p>正式版Build0337</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适用于性能较好的手机</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Build066</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适用于性能一般的手机</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适用于性能较弱的手机</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Beta1Build0051</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适用于普通触屏手机</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Beta2Build0301</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>适用于全触屏手机</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>通用版（<a href="../Page/S60.md" title="wikilink">S60</a>）</p></td>
<td></td>
<td><p>正式版 Build070</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>不支持Nokia N70/N72机型</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>该版本支持操作过程的语音提示功能，可以帮助盲人和低视力用户实现手机QQ的操作。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>通用版（<a href="../Page/Windows_Mobile.md" title="wikilink">Windows Mobile</a>）</p></td>
<td></td>
<td><p>Build093</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>WM触屏</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>build021</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>WM非触屏</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Build093</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>PPC触屏</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>SP QVGA 240*320分辨率</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td></td>
<td><p>简体中文</p></td>
<td><p>SP非触屏、适用于Windows Mobile 2003平台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<div style="text-align:right">

<small>：近期有更新；：已有一段时间没有更新，但官方未宣布停止更新；：已宣布停止更新。</small>

</div>

### 非官方版本

2001年，腾讯首次在QQ中集成[广告](../Page/广告.md "wikilink")\[44\]，而在QQ2005
Beta3版之前，TT浏览器被捆绑在QQ的安装程序且为默认安装\[45\]。很多早期的第三方QQ版本（如[珊瑚虫QQ](../Page/珊瑚虫QQ.md "wikilink")、木子版QQ）的设计初衷是屏蔽QQ的广告和一些不必要的[软件和插件](../Page/软件.md "wikilink")，而且还为QQ增加很多新功能，如显示[IP地址](../Page/IP地址.md "wikilink")、查看好友是否隐身等。而包含显示IP地址功能的第三方QQ版本常被称为“IPQQ”。

<table>
<thead>
<tr class="header">
<th></th>
<th><p>运行平台</p></th>
<th><p>名称</p></th>
<th></th>
<th></th>
<th><p>语言</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>基于官方客户端组件的非通讯协议实现</p></td>
<td><p><a href="../Page/Windows.md" title="wikilink">Windows</a></p></td>
<td></td>
<td><p>珊瑚虫QQ 2007 5.0c正式版</p></td>
<td><p>2007年7月26日</p></td>
<td><p>简体中文</p></td>
<td><p>珊瑚虫QQ的功能特点是“去广告、显IP”。2001年，珊瑚虫QQ首次推出；2003年，珊瑚虫作者在腾讯的维权行动中被迫写下保证书，停止QQ的开发一年。2004到2006年间，珊瑚虫QQ发展、壮大，其用户数达到四千万。2007年8月，珊瑚虫作者因涉嫌侵犯著作权罪被捕，之后珊瑚虫工作室网站被封，珊瑚虫QQ的开发宣告终止。[46]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>腾讯QQ 2004木子版V1.0</p></td>
<td><p>2004年4月29日</p></td>
<td><p>简体中文</p></td>
<td><p>木子工作室于2002年发布“腾讯QQ2000c 1230b木子增强版”，特点是在腾讯QQ的基础上加上“去广告、除TT浏览器”的功能。2003年6月，腾讯警告木子工作室可能会对其采取法律行动，其后，木子工作室停止开发木子版QQ。[47]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>腾讯QQ彩虹显IP显隐身V2.71</p></td>
<td><p>2008年11月14日</p></td>
<td><p>简体中文</p></td>
<td><p>由51.com开发，特色功能是“显示IP地址、隐身好友，隐身好友蓝名显示、靠前排序”，于2008年12月停止开发[48]。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>腾讯QQ2008 Final飘云V4.90</p></td>
<td><p>2008年7月17日</p></td>
<td><p>简体中文</p></td>
<td><p>2004年9月，飘云QQ的作者在“QQ 2004奥运特别版”的基础上，以木子版QQ的开发原理创作了“木子继承版”，后来被称为飘云版QQ。特点是“去广告与显IP”。在2007年珊瑚虫QQ作者被抓后，飘云版QQ的作者宣布退出开发[49]，但是到了2008年，新版的飘云版QQ流出。[50]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>赛博QQ3.0</p></td>
<td><p>2010年9月19日</p></td>
<td><p>简体中文</p></td>
<td><p>主要功能是“显IP”，支持QQ2009、2010。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>NTRQQ3.4天兔台风纪念版（内部测试版为NTRQQ3.5Beta4端午测试版）</p></td>
<td><p>2013年9月22日（内部测试版发布日期为2014年6月2日）</p></td>
<td><p>简体中文</p></td>
<td><p>功能是“显IP”，“去广告”等，支持QQ2009-5.2（内部测试版支持QQ5.3-5.5），TM2008-2013，繁体版，国际版。[51]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>其他</p></td>
<td><p>除上述第三方QQ版本外，还有狂人、传美、阿瑞斯（AresCN）、快乐无极、海峰版等。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>通过<a href="../Page/逆向工程.md" title="wikilink">逆向工程</a><a href="../Page/开源.md" title="wikilink">开源性质的</a><a href="../Page/通讯协议.md" title="wikilink">通讯协议实现</a></p></td>
<td><p><a href="../Page/跨平台.md" title="wikilink">跨平台</a></p></td>
<td></td>
<td><p>2.10.0; libqq 0.71</p></td>
<td></td>
<td></td>
<td><p>前称“<a href="../Page/Gaim.md" title="wikilink">Gaim</a>”，采用<a href="../Page/GTK+.md" title="wikilink">GTK+</a>，支持多平台、多协议的即时通信软件，从2.0版本到2.7内置支持QQ通讯协议，但是后来链接不了[52]。现在可用第三方插件：libqq[53]。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2006 Milestone 2</p></td>
<td><p>2006年1月22日</p></td>
<td><p>多语言</p></td>
<td><p>用<a href="../Page/Java.md" title="wikilink">Java及</a><a href="../Page/SWT.md" title="wikilink">SWT实现的QQ客户端核心库及用户友好的客户端</a>。2006年开始停止开发[54]。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>0.7</p></td>
<td><p>2006年5月10日</p></td>
<td><p><a href="../Page/英语.md" title="wikilink">英语</a></p></td>
<td><p>基于<a href="../Page/LumaQQ.md" title="wikilink">LumaQQJava核心库的命令行版本的QQ第三方客户端</a>[55]。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Net::OICQ 1.6</p></td>
<td><p>2007年6月16日</p></td>
<td><p><a href="../Page/简体中文.md" title="wikilink">简体中文</a>、英语</p></td>
<td><p>Net::OICQ是一个<a href="../Page/Perl.md" title="wikilink">Perl的面向对象的模块</a>，包含连接QQ服务器的基本方法，其内部分程序能实现基本的QQ功能[56]。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>1.02beta</p></td>
<td><p>2012年</p></td>
<td><p><a href="../Page/简体中文.md" title="wikilink">简体中文</a></p></td>
<td><p>iQQ是一项开源项目，源码托管在google code上，基于腾讯WebQQ 3.0网络协议。<a href="https://code.google.com/p/iqq/">项目</a>使用Java语言跨平台开发，作者基于Linux（Ubuntu 12.04）系统，使用IDE NetBeans开发。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows.md" title="wikilink">Windows</a></p></td>
<td></td>
<td><p>MirandaQQ2 0.2.0.68</p></td>
<td><p>2010年1月6日</p></td>
<td><p>中文、英语</p></td>
<td><p>在<a href="../Page/Miranda_IM.md" title="wikilink">Miranda IM中通过添加支持QQ协议的插件实现</a>：MirandaQQ（OpenQ Version、libeva Version）已停止开发[57]，目前可用有MirandaQQ2、MirandaQQ3[58]。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Linux.md" title="wikilink">Linux</a></p></td>
<td></td>
<td><p>0.4.1</p></td>
<td><p>2006年1月24日</p></td>
<td><p>简体中文</p></td>
<td><p>Linux <a href="../Page/KDE.md" title="wikilink">KDE下兼容QQ即时通讯协议的客户端</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2.32.0.1</p></td>
<td><p>2010年10月4日</p></td>
<td><p>多语言</p></td>
<td><p><a href="../Page/Ubuntu.md" title="wikilink">Ubuntu和</a><a href="../Page/Fedora.md" title="wikilink">Fedora目前默认的即时通讯软件的聊天程序</a>。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Mac.md" title="wikilink">Mac</a></p></td>
<td></td>
<td><p>1.5b7; AdiumQQ 0.6</p></td>
<td></td>
<td></td>
<td><p>多协议的即时通信软件，从1.0版本到1.4内置支持QQ通讯协议，但是后来链接不了。现在可用第三方插件：AdiumQQ[59]。</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1.0 Final (v226)</p></td>
<td><p>2004年12月27日</p></td>
<td><p>简体中文</p></td>
<td><p>isQ为第一个在Mac上实现与QQ进行即时通讯的聊天程序[60]。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<div style="text-align:right">

<small>：近期有更新；：已有一段时间没有更新，但官方未宣布停止更新；：已宣布停止更新。</small>

</div>

## 功能和服务

### QQ账号

用户可以通过QQ号码、[电子邮箱地址](../Page/电子邮箱.md "wikilink")、手机号码登录腾讯QQ。

可选登录方式（均需与一个QQ号码绑定后才可使用）：

  - 电子邮箱 （该方式随QQ2007正式版发布而加入）
  - 手机号码 （该方式于2012年增加）

QQ号码由[数字组成](../Page/数字.md "wikilink")，在1999年，即QQ刚推出不久时，其长度为5位数\[61\]，而目前通过免费注册的QQ号码长度已超过10位数。QQ号码分为免费的“普通号码”、付费的“QQ靓号”和“QQ行号码”，包含某种特定寓意（如[生日](../Page/生日.md "wikilink")、手机号码）或重复数字的号码通常作为靓号在QQ号码商城出售。目前售价最高的QQ号码是由腾讯公司总裁[马化腾在](../Page/马化腾.md "wikilink")[淘宝网举办的](../Page/淘宝网.md "wikilink")[南亚海啸赈灾义卖活动中通过](../Page/南亚海啸.md "wikilink")[竞拍方式最终以](../Page/拍卖.md "wikilink")26.02万元的价格出售的QQ号码“88888”\[62\]\[63\]。

腾讯在其
《QQ号码规则》规定“QQ账号使用权仅属于初始申请注册人，禁止赠与、借用、租用、转让或售卖”\[64\]。2005年3月至7月期间，一名腾讯职员与另外一人合谋通过内部窃取他人QQ号码出售获利，最终两人以侵犯他人通信自由罪被判各[拘役六个月](../Page/拘役.md "wikilink")\[65\]。同时，根据协议，腾讯有权回收QQ号码，除由于非法转售QQ号码而被回收外，回收的对象还包括3个月内没有登录纪录的普通QQ号码，自关停后一个月内没有及时续费的付费号码，非法抢注的、用于[灌水或群发](../Page/灌水.md "wikilink")[广告的号码](../Page/广告.md "wikilink")\[66\]。

### QQ群

[QQ群是由是一个聚集一定数量QQ用户的长期稳定的公共](../Page/QQ群.md "wikilink")[聊天室](../Page/聊天室.md "wikilink")，最早见于QQ2000c
Build0825版本。QQ群成员可以通过文字、[语音](../Page/语音.md "wikilink")、和视频进行聊天，在群空间内也可以通过群论坛、群相册、群共享文件等方式进行交流。创建QQ群的人叫做群主，能委任群成员为管理员，并且有全部权利解散群，群主和管理员可以添加、删除群成员（群成员也可以要求其他人加入，但需要管理员或者群主同意）和撤回成员消息。相对于QQ群，群[讨论组适合少人的文字图片聊天](../Page/讨论组.md "wikilink")，最早见于QQ2004
Preview\[67\]。QQ群与[QQ校友里的班级绑定后](../Page/QQ校友.md "wikilink")，QQ群随即变为班级群，群内的成员名字默认显示为其在QQ校友注册的姓名，同时共享空间也会被置换成QQ校友的共享空间。现QQ群共享空间更名为群文件，空间随群人数变动。QQ群拥有群视频，群签到，群论坛，群文件，群相册，群调查等等数十项应用。\[68\]。

<table>
<caption>QQ群和群讨论组功能对比[69]</caption>
<thead>
<tr class="header">
<th><p>分类</p></th>
<th><p>创建条件</p></th>
<th><p>创建数量上限</p></th>
<th></th>
<th></th>
<th></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>500</p></td>
<td><p>500人10G，200人2G</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>千人群</p></td>
<td><p>vip级</p></td>
<td><p>年费vip6级可创建2个，vip7级可创建3个</p></td>
<td><p>1000</p></td>
<td><p>10G</p></td>
<td><p>最多20</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>两千人群</p></td>
<td><p>仅SVIP级可以创建</p></td>
<td><p>SVIP级可建立1个，年费SVIP6可建立2个，年费SVIP8可以建立3个</p></td>
<td><p>2000</p></td>
<td><p>10G</p></td>
<td><p>最多20</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>群讨论组</p></td>
<td><p>不限</p></td>
<td><p>无上限</p></td>
<td><p>50</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
</tbody>
</table>

2016年1月20日腾讯开始测试万人群\[70\]，2017年7月企业、机构可申请5000人认证群费用为300元一年\[71\]

#### 匿名聊天

手机QQ5.1及以上版本支持，电脑QQ版本6.3及以上。该功能允许在QQ群内匿名发送消息，其他人不知道匿名聊天者的身份，该功能可以被群主或管理员开启或关闭。\[72\]

#### 聊天记录

另外，腾讯QQ也被认为会记录与监视用户对电脑的操作行为和聊天记录，腾讯也承认会按主管部门要求做一些处理，检测消息中的敏感信息，并可能因此封禁用户账号。\[73\]

### QQ秀

**QQ秀**是[腾讯推出的为](../Page/腾讯.md "wikilink")[QQ服务的一项商业化增值服务](../Page/腾讯QQ.md "wikilink")，大部分需要使用[Q币购买](../Page/Q币.md "wikilink")，这项功能能让用户更显示出个性。\[74\]

#### 厘米秀

厘米秀，是手机QQ6.2.2版本推出的重要功能，相当于将PC端的QQ秀移动至手机端，2016年1月26号，随着手Q6.2.2版本的更新，手Q官方发放了1万2千个安卓版手Q厘米秀的内测资格。

  - 资料卡和点赞

资料卡功能暂时受限\[75\]

  - 徽章

### 会员与等级制

腾讯推出的会员制和等级制是其[商业化尝试之一](../Page/商业.md "wikilink")，用户通过有偿方式，在登录、附加功能等项目上拥有更多的权限和便捷。

<table>
<caption>腾讯QQ的等级系统</caption>
<thead>
<tr class="header">
<th><p>增值服务<br />
名称</p></th>
<th><p>服务领域</p></th>
<th><p>最高<br />
级别</p></th>
<th><p>费用</p></th>
<th><p>增值服务<br />
推出日期</p></th>
<th><p>服务领域<br />
推出日期</p></th>
<th><p>等级系统<br />
推出日期</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>QQ</p></td>
<td><p>不限</p></td>
<td><p>—</p></td>
<td></td>
<td></td>
<td></td>
<td><p>第一个开始计算在线时长的版本是QQ2004II Beta1。自在线时长功能发布后，不少人为了能使自QQ等级快速升级，24小不关电脑保持QQ在线，俗称“挂QQ”，当时部分声音质疑这样做会浪费国家能源资源；而由于“挂QQ”的盛行，有人就做出了“QQ挂机王”的收费软件、免费“挂QQ”的网站[76]。其后，腾讯推出多项措施，包括于2005年8月15日开始实施的新的在线等级计算办法，由以往的以小时改为以“活跃天”计算，即每天登录两小时记一天，小于半小时不算，而在两者之间则记半天[77]。2011年11月16日，QQ等级加速新规则发布，与之前的加速规则相比，加速更灵活，同时特权更丰富。新规则分为基础加速和服务加速，而且更注重QQ的活跃程度（在线时长、在线方式、与好友或群友的互动）、连续使用，以及使用Q+的情况。</p></td>
</tr>
<tr class="even">
<td><p>QQ会员</p></td>
<td><p>会员</p></td>
<td><p>VIP7</p></td>
<td><p>10元/月</p></td>
<td></td>
<td></td>
<td><p>2006年6月15日</p></td>
<td><p>在2002年之前，QQ会员称为QQ俱乐部。</p></td>
</tr>
<tr class="odd">
<td><p>QQ超级会员</p></td>
<td><p>会员</p></td>
<td><p>SVIP8</p></td>
<td><p>20元/月</p></td>
<td><p>2013年6月24日</p></td>
<td><p>2013年6月24日</p></td>
<td><p>2015年4月1日</p></td>
<td><p>在2015年4月1日前，超级会员最高等级为SVIP7</p></td>
</tr>
<tr class="even">
<td><p>黄钻</p></td>
<td><p>QQ空间</p></td>
<td><p>Lv8</p></td>
<td><p>10元/月），超级黄钻20元/月</p></td>
<td><p>2005年12月23日</p></td>
<td><p>2005年6月6日</p></td>
<td></td>
<td><p>黄钻适用于QQ空间，提供更多空间装扮功能和相册空间[78]。</p></td>
</tr>
<tr class="odd">
<td><p>红钻</p></td>
<td><p>QQ秀</p></td>
<td><p>Lv7</p></td>
<td><p>10元/月</p></td>
<td><p>2004年8月20日</p></td>
<td><p>2001年</p></td>
<td></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>绿钻</p></td>
<td><p>QQ音乐</p></td>
<td><p>Lv8</p></td>
<td><p>10元/月</p></td>
<td><p>2005年9月1日</p></td>
<td><p>2004年7月12日</p></td>
<td><p>2008年4月2日</p></td>
<td><p>在QQ2007 Beta3发布时，原QQ音乐VIP改称为绿钻。绿钻的额外功能和服务有正版<a href="../Page/音乐.md" title="wikilink">音乐下载</a>、高清<a href="../Page/音乐视频.md" title="wikilink">音乐视频观看</a>、提供<a href="../Page/表演.md" title="wikilink">演出折扣</a>[79]。</p></td>
</tr>
<tr class="odd">
<td><p>蓝钻</p></td>
<td><p>QQ游戏</p></td>
<td><p>Lv7</p></td>
<td><p>10元/月，藍鑽豪華版15元/月</p></td>
<td><p>2004年7月1日[80]</p></td>
<td><p>2003年8月18日</p></td>
<td><p>2008年11月14日</p></td>
<td><p>原称超级玩家。在QQ游戏中，蓝钻用户较普通用户有更多高级功能，如在游戏中可以请某人离开[81]。</p></td>
</tr>
<tr class="even">
<td><p>黑钻</p></td>
<td><p>地下城与勇士</p></td>
<td><p>Lv7</p></td>
<td><p>20元/月</p></td>
<td><p>2008年6月19日</p></td>
<td><p>2008年6月19日</p></td>
<td><p>2009年5月1日[82]</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>紫钻</p></td>
<td></td>
<td><p>Lv6</p></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>紫钻分为炫舞紫钻（20元/月）、飞车紫钻、音速紫钻、QQ堂紫钻。</p></td>
</tr>
<tr class="even">
<td><p>粉钻（已停止开通）</p></td>
<td><p>QQ宠物</p></td>
<td><p>Lv7</p></td>
<td><p>10元/月</p></td>
<td><p>2007年4月5日[83]</p></td>
<td><p>2005年6月13日</p></td>
<td><p>2010年11月16日</p></td>
<td><p>成长加成、免费吃洗看病复活、每天/周/月礼包、离线挂机、购物打折、粉钻标志、贴心宝贝。2018年09月30日，由于QQ宠物下线，故腾讯暂停了此服务的开通</p></td>
</tr>
<tr class="odd">
<td><p>寻仙VIP</p></td>
<td><p>QQ寻仙</p></td>
<td><p>—</p></td>
<td><p>20元/月</p></td>
<td><p>2009年4月28日</p></td>
<td><p>2008年10月27日</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>CF会员</p></td>
<td><p>穿越火线</p></td>
<td><p>—</p></td>
<td><p>30元/月</p></td>
<td><p>2010年5月19日</p></td>
<td><p>2008年7月25日</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>读书VIP</p></td>
<td><p>腾讯读书</p></td>
<td><p>—</p></td>
<td><p>10元/月</p></td>
<td><p>2007年5月</p></td>
<td><p>2004年9月</p></td>
<td><p>—</p></td>
<td><p>QQ会员可以通过特别通道开通每月5元</p></td>
</tr>
<tr class="even">
<td><p>超级QQ</p></td>
<td><p><a href="../Page/手机.md" title="wikilink">手机</a></p></td>
<td><p>黄金8级</p></td>
<td><p>10元/月</p></td>
<td><p>—</p></td>
<td><p>2006年11月</p></td>
<td><p>2008年3月10日</p></td>
<td><p>分为短信版与预付费版，两者均为10元/月，两者区别是预付费版没有短信的功能。按运营商不同，短信版可能为15元/月。超级QQ已于2015年升级为QQ会员，正式关闭开通通道。[84]。</p></td>
</tr>
</tbody>
</table>

#### QQ等级

在2004年10月腾讯公司推出了QQ“在線时长等级服务”，QQ用戶透過長期在線，便能提升其在QQ中的等級。随着等級的增加可以解锁一些特权。但也增加了挂机行为。QQ等级由用户积累的QQ活跃天数决定。2014年，QQ等级加速规则再次修改，新规则分为**基础加速**、**成长加速**和**服务加速**。

### 轉賬和紅包

要更新至最新的手机QQ5.4版本\[85\]\[86\]。2018年1月8日起，qq红包做出服务变更，如果用户将qq钱包余额提现到[银行卡上](../Page/银行卡.md "wikilink")，将收取服务费。每位用户获得1000元的免费提现额度，超过将被收取0.1%的费用，每次提现至少收取1角钱。其他支付功能，如红包和转账保持免费。

## 相关事件

## 参考资料

<div class='references-small'>

</div>

## 外部链接

  - [腾讯QQ下载](https://im.qq.com/download/)

[Q](../Category/即时通讯软件.md "wikilink")
[Category:中国驰名商标](../Category/中国驰名商标.md "wikilink")
[Category:腾讯QQ](../Category/腾讯QQ.md "wikilink")

1.  [腾讯·公司信息](http://www.tencent.com/zh-cn/at/rm/2003.shtml)
    .\[2010-9-23\]

2.  [腾讯高价海外回购域名秘密重铸QQ品牌战略](http://news.chinabyte.com/110/1659110.shtml).ChinaByte\[2003-3-25\]

3.  黄孟复.[中国民营企业自主创新调查](http://books.google.com.hk/books?id=gr_sWBs78ssC&pg=PA394)\[M\].北京：中华工商联合出版社,
    2007:394.

4.  [QQ版本发展历程](http://im.qq.com/culture/version.shtml).QQ官方网站\[2010-9-23\]

5.
6.
7.
8.  北方网.[升级神速！腾讯QQ2003III
    beta1抢鲜下载](http://it.enorth.com.cn/system/2003/11/18/000670241.shtml).\[2003-11-18\]

9.
10. 腾讯网.[QQ同时在线历程：2010年3月5日最高在线破1亿](http://tech.qq.com/a/20100305/000559.htm).\[2010-3-5\]

11. QQ官方网站.[QQ发展大事记](http://im.qq.com/culture/progress.shtml).\[2010-9-24\]

12. 姜明媚.[QQ：互联网起家的品牌](http://yule.sohu.com/20050802/n226536247.shtml)。搜狐娱乐\[2005-8-2\]

13. 刘德良.[向下，向下，再向下](http://column.iresearch.cn/u/liudeliang/archives/2007/8926.shtml)。艾瑞网\[2007-10-22\]

14. [QQ所有版本](http://im.qq.com/l).QQ官方网站\[2017-01-01\]

15. [腾讯博客](http://webqq.qzone.qq.com/).WebQQ官方博客\[2010-9-23\]

16. [手机QQ官网](http://im.qq.com/mobileqq/)

17. 腾讯网.[中西合璧：QQ国际版尝鲜体验](http://xian.qq.com/a/20091016/000035.htm).\[2009-10-16\]

18. 东北网.[交友国际化QQ International
    Beta1发布](http://it.dbw.cn/system/2009/09/27/052131546.shtml).\[2009-9-27\]

19. [腾讯TM](http://im.qq.com/tm/2013).\[2011-4-28\]

20. 华军资讯.[腾讯将收购泰国门户网站Sanook 49.9%股份](http://news.newhua.com/news/2010/0827/100769.shtml).
    \[2010-8-27\]

21. [ibibo.About Us](http://www.ibibo.com/aboutus/main.php).
    \[2010-9-26\]

22. 百度空间.[QQ也有盗版？越南ZingChat雷人！开心农场也被山寨！](http://hi.baidu.com/dekaron/blog/item/7fca642320335a4593580775.html).\[2009-12-17\]

23. 《中国企业家》.[腾讯国际化全局](http://tech.qq.com/a/20100512/000284.htm).\[2010-5-12\]

24. [Internet Archive Wayback Machine.Search
    Result](https://web.archive.org/web/20070116110204/http://www.sonky.com/qqitaly/).\[2010-9-26\]

25. Sonky.[QQITALY for Iphone](http://www.sonky.com/qqitaly/iphone/) .
    \[2010-9-26\]

26. 企奥网.[南非版QQ测评-斗鸡眼的小企鹅](http://www.qqaomi.com/2010/06/%E4%BC%81%E5%A5%A5%E5%8D%97%E9%9D%9E%E7%89%88qq%E6%B5%8B%E8%AF%84/)
    .\[2010-6-9\]

27. 太平洋电脑网.[试用QQ for
    Mac:原来如此简单](http://www.pconline.com.cn/pcedu/qq/zhixun/0801/1216405.html).
    \[2008-1-28\]

28. 新浪.[腾讯正式推出首款苹果版QQ软件](http://tech.sina.com.cn/i/2008-01-28/18442001949.shtml).\[2008-1-28\]

29. QQ for
    Mac.[当前版本功能介绍](http://im.qq.com/qq/mac/download.shtml).\[2010-9-25\]

30. QQ for Linux.[一般问题解答](http://im.qq.com/qq/linux/help.shtml).
    \[2010-9-25\]

31. 腾讯客服.[QQ软件常见问题 - Linux
    QQ问题](http://service.qq.com/category/1008_1.html) .\[2010-9-25\]

32. Ubuntu之家.[QQ FOR
    LINUX将要更新了？](http://www.ubuntuhome.com/qq-for-linux-will-update.html)
    .

33. 太平洋电脑网.[出门在外就用它：WebQQ网页聊天初体验](http://pcedu.pconline.com.cn/qq/jiqiao/0904/1621706.html).\[2009-4-16\]

34. 太平洋电脑网.[腾讯WebOS新布局WebQQ 2.0亮点深度体验](http://pcedu.pconline.com.cn/pingce/pingcenormal/1009/2220885.html).\[2010-9-14\]

35. 驱动之家MyDrivers.[更像OS腾讯WebQQ 3.0新版上线](http://tech.163.com/digi/11/0422/07/727QIRJ600162OUT.html).
    \[2011-4-22\]

36. [Q+ Web - 搜搜百科](http://baike.soso.com/v43691303.htm)

37. [Q+官方网站-产品中心](http://www.qplus.com/productForWeb.shtml)

38. 91手机.\[<http://android.91.com/content/2010-03-30/20100330075812461.shtml>,
    droid腾讯官方QQ上线\]. \[2010-3-30\]

39. 南方报业网.\[<http://nf.nfdaily.cn/nanfangdaily/21cn/200909070079.asp>,
    iPhone创富志\].\[2009-9-7\]

40. [iTunes 的 App Store
    中的“QQ”](https://itunes.apple.com/cn/app/qq-2011/id444934666?mt=8)

41. [1](https://itunes.apple.com/am/app/qq-hd/id453718989?mt=8)

42. 聊天升级：原创表情、图文消息展示，聊天草稿，收图升级：2G/3G下聊天缩略图自动下载，下载原图到本地，查找联系人升级：按邮箱查找人，关键词查找群，群成员新增搜索，临时会话升级：群临时支持发图，新增从讨论组发起临时会话，通过链接加入讨论组，性能升级：支持好友24小时消息漫游，联网优化，修复其他程序bug.

43. [S60V5腾讯官网下载](http://mobile.qq.com/2010/v5手机QQ)

44. 天极网.[腾讯QQ七年成长历程回顾](http://soft.yesky.com/lianluo/201/2170201.shtml).\[2005-10-27\]

45. 腾讯.[QQ2005
    Beta3](http://im.qq.com/qq/2005/beta3/index_d.shtml).\[2010-10-1\]

46.

47. 太平洋电脑网.[乌云密布！腾讯QQ的天空不需要彩虹？](http://pcedu.pconline.com.cn/softnews/gdpl/0811/1480093_1.html).\[2008-11-17\]

48. 搜狐网.[51.com中止彩虹QQ开发维护内部业务重组](http://it.sohu.com/20081217/n261251511.shtml).\[2008-12-17\]

49. cnBeta.[告别飘云\!飘云QQ核心技术人员正式声明退出开发](http://www.cnbeta.com/articles/40844.htm)
    . \[2007-10-11\]

50. 华军资讯网.[飘云重拾QQ复制陈寿福珊瑚虫的疯狂？](http://news.newhua.com/news1/net/2008/610/08610153028C9C52FGE95CCFAA0AAK9KK1EFJ4G45GA1JAH3HDG1CID2.html)
    .\[2008-6-10\]

51. <http://bbs.ntrqq.net/forum.php?mod=forumdisplay&fid=15&filter=typeid&typeid=47>

52. Pidgin.[FullChangeLog](http://developer.pidgin.im/wiki/FullChangeLog).\[2010-9-27\]

53. [libqq](http://code.google.com/p/libqq-pidgin/).

54. LumaQQ.[Java
    Edition](http://lumaqq.linuxsir.org/main/LumaQQJavaEdition.html)
    .\[2010-9-27\]

55. TextQQ.[Project
    Info](http://cosoft.org.cn/projects/textqq/).\[2010-9-27\]

56. Perl OICQ.[Perl OICQ](http://perl-oicq.sourceforge.net/).
    \[2010-9-27\]

57. Miranda IM.[MirandaQQ (libeva
    Version) 0.1.0.27](http://addons.miranda-im.org/details.php?action=viewfile&id=2107)
    .\[2010-10-1\]

58. Miranda
    IM.[MirandaQQ2 0.2.0.68](http://addons.miranda-im.org/details.php?action=viewfile&id=3348).\[2010-10-1\]

59. AdiumQQ
    github.com/worr/AdiumQQ/（adiumxtras.com/index.php?a=xtras\&xtra_id=8226）

60. 渤麦堂.[isQ开发小组](http://www.sinomac.com/isQ/index.php) .\[2010-10-1\]

61. 杨子晚报.[网友追捧最牛QQ号](http://epaper.yangtse.com/yzwb/2009-10/09/content_14179623.htm)
    . \[2009-10-9\]

62. 中国江西省广播电视今视网.[腾讯五位至尊QQ号码“88888”拍出天价26.02万](http://www.jxgdw.com/news/shxw/2005-01-31/3000028795.html)
    .\[2007-12-23\]

63.

64. 腾讯.[软件许可及服务协议](http://freeqqm.qq.com/agreement.html).\[\]

65. 徐强.[全国首宗盗卖QQ号码案宣判定侵犯通信自由罪](http://www.chinacourt.org/public/detail.php?id=192244)。中国法院网\[2006-1-13\]

66. 硅谷动力.[深入了解：腾讯回收QQ号码原则](http://www.enet.com.cn/article/2006/0123/A20060123496230.shtml)
    .\[2006-1-23\]

67. 腾讯.[QQ群组](http://service.qq.com/category/18_1.html) .\[2010-10-4\]

68. 腾讯.[校友班级和QQ群问题](http://service.qq.com/category/1154_1.html) .
    \[2010-10-4\]

69.
70. [传QQ在内测万人群](http://tech.163.com/16/0406/08/BJV4O6EA00094OE0.html)

71. [QQ认证群功能升级：申请成功立即升级为5000人群](http://tech.sina.com.cn/roll/2017-07-15/doc-ifyiaewh9137183.shtml)

72.

73.

74.

75.

76. 新浪.[QQ挂级：浪费能源谁心疼](http://finance.sina.com.cn/roll/20050802/0903244132.shtml).\[2005-8-2\]

77. 腾讯.[QQ在线计划说明](http://im.qq.com/qq/mo.shtml?time.htm).\[2010-10-6\]

78. 腾讯.[黄钻等级体系](http://beta-imgcache.qzone.qq.com/qzone/mall/v3/vip/portal/series.htm).
    \[2010-10-4\]

79. 腾讯.[绿钻](http://vip.music.qq.com/v2/privileges/index.html).\[2010-10-5\]

80. 太平洋电脑网.[QQ蓝钻将推出等级体系](http://www.pconline.com.cn/pcedu/qq/zhixun/0811/1473924.html).\[2008-11-11\]

81. [蓝钻贵族使用帮助](http://minigame.qq.com/gamevip/help.html) 。蓝钻贵族FAQ
    \[2011-11-28\]

82. 腾讯.[黑钻等级规则](http://dnf.qq.com/comm-htdocs/vip/dnf/power_8.htm).\[2010-10-5\]

83. 腾讯.[购Q宠粉钻送礼包限时优惠火爆抢购](http://games.qq.com/a/20070412/000251.htm).
    \[2007-4-12\]

84.

85.

86.