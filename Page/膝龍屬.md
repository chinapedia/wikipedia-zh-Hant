**膝龍屬**（[學名](../Page/學名.md "wikilink")：*Genusaurus*）意為「膝蓋[蜥蜴](../Page/蜥蜴.md "wikilink")」，是[下白堊紀的一](../Page/下白堊紀.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。膝龍是屬於[角鼻龍下目](../Page/角鼻龍下目.md "wikilink")，並可能是[食肉牛龍的近親](../Page/食肉牛龍.md "wikilink")。膝龍的[化石被發現於](../Page/化石.md "wikilink")[法國](../Page/法國.md "wikilink")。膝龍的身長估計為9.8公尺\[1\]，重量約1.5公噸\[2\]。膝龍的生存年代約是[阿普第階](../Page/阿普第階.md "wikilink")，約1億2000萬到1億1000萬年前\[3\]。

[模式種是](../Page/模式種.md "wikilink")*G. sisteronis*，是在1995年根據部份骨骼而描述、命名的。

## 參考文獻

<div class="references-small">

<references />

  - Accarie, H., B. Beaudoin , J. Dejax, G. Fries, J.C. Michard, and P.
    Taquet. (1995). "Découverte d'un Dinosaure Théropode nouveau
    (*Genusaurus sisteronis n. g., n. sp*.) dans l'Albien marin de
    Sisteron (Alpes de Haute-Provence, France) et extension au Crétace
    inférieur de la lignée ceratosaurienne". *Compte rendu hebdomadaire
    des séances de l'Académie des Sciences Paris*. 320(2):327-334.

</div>

## 外部連結

  - <https://web.archive.org/web/20090628102404/http://www.thescelosaurus.com/ceratosauria.htm>
  - <http://web.me.com/dinoruss/de_4/5a648e1.htm>

[Category:阿貝力龍超科](../Category/阿貝力龍超科.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")

1.

2.

3.