**膳食纖維**是指不能由人體[消化道](../Page/消化道.md "wikilink")[酶分解的植物源食物成分](../Page/酶.md "wikilink")，主要是[多醣类及](../Page/多醣.md "wikilink")[木質素](../Page/木質素.md "wikilink")。可分为两类：

  - 可溶性纤维：可溶于水，吸收水分后成为凝胶状半流体，在结肠中细菌作用下易于发酵生成气体与生理活性副产物，是[益生元](../Page/益生元.md "wikilink")。某些可溶性纤维阻止肠粘膜粘连与潜在致病细菌迁移（translocation）因此能调理肠道炎症，这种效果称为contrabiotic。\[1\]\[2\]
  - 不可溶性纤维：不溶于水，是新陈代谢惰性，提供充盈（bulking），可以是不发酵\[3\]，例如[木质素能够改变可溶性膳食纤维的吸收速率](../Page/木质素.md "wikilink")；\[4\]也可以使[益生元且在大肠中发酵](../Page/益生元.md "wikilink")，如抗性淀粉。\[5\]填充纤维在通过消化道时吸收水分促进[排便](../Page/排便.md "wikilink")。\[6\]

膳食纤维能够改变[胃肠道内物质的性质](../Page/胃肠道.md "wikilink")，改变其他营养物与化学物质的吸收方式。

在[消化系統中有吸收](../Page/消化系統.md "wikilink")[水份的作用](../Page/水.md "wikilink")。膳食纖維能增加[腸道及](../Page/腸道.md "wikilink")[胃內的食物體積](../Page/胃.md "wikilink")，可增加飽足感；又能促進腸胃蠕動，可舒解[便秘](../Page/便秘.md "wikilink")；同時膳食纖維也能吸附腸道中的有害物質以便排出。过量摄入纤维素可以减少食物通过肠道所需时间，导致机体不能吸收其他[营养物质](../Page/营养物质.md "wikilink")。因为[钙](../Page/钙.md "wikilink")，[铁](../Page/铁.md "wikilink")，[锌和](../Page/锌.md "wikilink")[镁在肠道中共用转运体](../Page/镁.md "wikilink")，所以其中一种[矿物质大量消耗会使转运体系饱和](../Page/矿物质.md "wikilink")，从而减少其他矿物质的转运\[7\]，一般建議量為每日攝取20-30克。

膳食纖維主要是非[澱粉](../Page/澱粉.md "wikilink")[多糖的多種](../Page/多糖.md "wikilink")[植物物質](../Page/植物.md "wikilink")，包括[纖維素](../Page/纖維素.md "wikilink")、[木質素](../Page/木質素.md "wikilink")、、、抗性[糊精](../Page/糊精.md "wikilink")、[蠟](../Page/蠟.md "wikilink")、[甲殼質](../Page/甲殼質.md "wikilink")、[果膠](../Page/果膠.md "wikilink")、、[菊糖和](../Page/菊糖.md "wikilink")[低聚糖等](../Page/低聚糖.md "wikilink")。\[8\]

水溶性膳食纖維的功能：

  - 延緩胃排空
  - 防止血糖急劇上升
  - 降低血膽固醇

非水溶性膳食纖維的功能：

  - 增加糞便體積
  - 促進腸道蠕動
  - 预防便秘和痔疮
  - 預防憩室炎及大腸癌

來源：

  - 粗粮
  - 亚麻籽
  - 豆类
  - 地瓜
  - 青菜
  - 水果
  - 食用菌
  - 膳食补充剂

## 食物中的膳食纤维

下表来自USDA food groups/subgroups\[9\]

| 食物类别                        | 每份          | 纤维 g/份 |
| --------------------------- | ----------- | ------ |
| 水果                          | 0.5 cup     | 1.1    |
| 暗绿蔬菜（Dark-green vegetables） | 0.5 cup     | 6.4    |
| 橙色蔬菜（Orange vegetables）     | 0.5 cup     | 2.1    |
| 熟后豆子（Cooked dry beans）      | 0.5 cup     | 8.0    |
| 含淀粉蔬菜（Starchy vegetables）   | 0.5 cup     | 1.7    |
| 其他蔬菜                        | 0.5 cup     | 1.1    |
| 全麦（Whole grain）             | 28 g (1 oz) | 2.4    |
| 肉类                          | 28 g (1 oz) | 0.1    |

## 参考文献

## 參見

  - [纖維](../Page/纖維.md "wikilink")

{{-}}

[Category:營養物質](../Category/營養物質.md "wikilink")
[Category:纤维](../Category/纤维.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.
9.  Fiber data derived from USDA National Nutrient Database for Standard
    Reference, Release 17.