[Chinesesausageunpackaged.jpg](https://zh.wikipedia.org/wiki/File:Chinesesausageunpackaged.jpg "fig:Chinesesausageunpackaged.jpg")
**臘腸**是一種[臘味食品](../Page/臘味.md "wikilink")，是[中國江浙](../Page/中國.md "wikilink")、[四川及](../Page/四川.md "wikilink")[广东等南方地區常見的食物](../Page/广东.md "wikilink")。傳統的臘腸是以[豬肉注入利用動物腸臟製作的](../Page/豬肉.md "wikilink")[腸衣](../Page/香腸.md "wikilink")，再天然風乾製成，但現在工廠生產的臘腸，腸衣多以人工方法製造，並以熱風乾燥來提高生產效率。臘腸不但可單獨食用，也是製作多種粵式食品的食材。

## 製作

臘腸的材料包括豬肉和腸衣\[1\]。先將豬肉剁碎，再按照所需的肥瘦肉比例加以混和，並加入[鹽](../Page/鹽.md "wikilink")、[糖和](../Page/糖.md "wikilink")[醬油等](../Page/醬油.md "wikilink")[調味料](../Page/調味料.md "wikilink")，然後把肉泥灌入腸衣內，再經過壓縮，並把肉腸用绳子分截成所需長度后截断，於其中一端打結，另一端加上吊掛用的繩子，再採取天然風乾或熱風乾燥的方法，使臘腸[脫水以便保存](../Page/脫水.md "wikilink")。

## 用途

臘腸與其他臘味相比，因為臘腸的豬肉餡料有腸衣包覆，豬肉的油脂不易流出。在廣東和香港，臘腸是製作臘味飯的重要材料，原條臘腸被放在[米飯上煮熟](../Page/米飯.md "wikilink")。煮熟後的臘腸油份飽滿，當切開或咬開後，充滿臘味氣味的油脂便流出來，使米飯都帶有臘味的味道。臘腸卷是以完條臘腸製作的包子。臘腸可切片與其他食材一起作為餸菜，臘腸也是製作多種粵式食品的材料，包括[生炒糯米飯](../Page/生炒糯米飯.md "wikilink")、[糯米雞和](../Page/糯米雞.md "wikilink")[蘿蔔糕等](../Page/蘿蔔糕.md "wikilink")。

## 健康問題

因為臘腸是利用豬肉製成的加工食品，一般消費者難以得知其原料來源和製作方法是否符合衛生要求。過去便有商家被揭發使用死因不明的豬隻作為材料，又有商家為了使臘腸變得美觀吸引買家，而加入不可食用的[染料或大量](../Page/染料.md "wikilink")[防腐劑](../Page/防腐劑.md "wikilink")。

香港[食物環境衞生署於](../Page/食物環境衞生署.md "wikilink")2006年9月14日公布抽取作[化學及](../Page/化學.md "wikilink")[微生物化驗測試的賀年食品結果](../Page/微生物.md "wikilink")，發現一款澳門製「鳳城切肉腸」的樣本，含有不准使用的染色料[若丹明B](../Page/若丹明B.md "wikilink")，俗稱「花紅粉」\[2\]。能令胸口作悶、[嘔吐及影響](../Page/嘔吐.md "wikilink")[中樞神經](../Page/中樞神經.md "wikilink")。

2013年1月，[浙江省有商家被揭發使用死因不明的豬隻製造臘腸](../Page/浙江省.md "wikilink")，所製成的「毒臘腸」多達7500公斤，並銷往多個鄰近省市。該商家於8月23日被判刑\[3\]。

## 參見

  - [臘味](../Page/臘味.md "wikilink")
  - [香腸](../Page/香腸.md "wikilink")

## 參考資料

<references />

[Category:香腸](../Category/香腸.md "wikilink")
[Category:廣東食品](../Category/廣東食品.md "wikilink")
[Category:香港食品](../Category/香港食品.md "wikilink")
[Category:臘味](../Category/臘味.md "wikilink")

1.  [手切臘腸嚐真味](http://hk.apple.nextmedia.com/supplement/food/art/20131213/18548418)，蘋果日報，2013年12月13日
2.  [含「若丹明B」 惡心嘔吐
    奇華臘腸致癌回收](http://the-sun.on.cc/channels/news/20060115/20060115024257_0000.html)，太陽報，2006-1-15
3.  [黑心老板用病死猪肉制售7500多斤“毒腊肠”](http://news.qq.com/a/20140823/015995.htm)，腾讯网，2014-8-23