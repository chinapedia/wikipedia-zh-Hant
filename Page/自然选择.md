[Darwin's_finches.jpeg](https://zh.wikipedia.org/wiki/File:Darwin's_finches.jpeg "fig:Darwin's_finches.jpeg")（Galápagos
Islands）擁有13個[雀鳥物種](../Page/雀鳥.md "wikilink")（Darwin's
finches），他們相當親近，主要的特徵差異在於鳥喙的形狀。不同種類的鳥喙皆與合適於牠們的[食性](../Page/食性.md "wikilink")。[達爾文認為](../Page/查爾斯·達爾文.md "wikilink")，這是自然選擇造成的[演化後果](../Page/演化.md "wikilink")。\]\]

**自然选择**（，傳統上也譯為**天擇**）指生物的遺傳特徵在生存競爭中，由於具有某種優勢或某種劣勢，因而在生存能力上產生差異，並進而導致繁殖能力的差異，使得這些特徵被保存或是淘汰。自然選擇則是[演化的主要機制](../Page/演化.md "wikilink")，經過自然選擇而能夠稱成功生存，稱為「[適應](../Page/適應.md "wikilink")」。自然選擇是唯一可以解釋生物適應環境的機制。

這個理論最早是由[英國](../Page/英國.md "wikilink")[生物學家](../Page/生物學家.md "wikilink")[查爾斯·達爾文在](../Page/查爾斯·達爾文.md "wikilink")1859年出版的《[物種起源](../Page/物種起源.md "wikilink")》中提出，其於早年在[加拉巴哥群島觀察了數種動物後發現](../Page/加拉巴哥群島.md "wikilink")，島上很少有與鄰近大陸相似的物種，並且還演化出許多獨有物種，如巨型的[加拉巴哥象龜](../Page/加拉巴哥象龜.md "wikilink")，達爾文於開始以為，島上的鷽鳥應與[南美洲發現的為同種](../Page/南美洲.md "wikilink")，經研究，十三種燕雀中只有一種是與其大陸近親類似的，其餘皆或多或少發生了演化現象，他們爲了適應島上的生存環境，改變了鳥[喙的大小](../Page/喙.md "wikilink")。

## 自然選擇的因素

因為生物族群呈[指數成長](../Page/指數成長.md "wikilink")，但資源有限，因此不是每個個體都能存活。影響存活率的因素包括無機環境，如溫度、溼度、日照、空氣；以及生物因子，如掠食者、共生、種內競爭、食物、疾病等等。個體之間存在的[個體差異](../Page/個體差異.md "wikilink")（[變異](../Page/變異.md "wikilink")）使每個個體的存活率不同，如果個體可以耐乾旱、在空中飛、在海裡呼吸、用爪子掠食、抵抗病原、製造工具、或是合作獵食等等，就會有不同的適應力，因此個體間的存活率不同。這些個體差異如果可以遺傳，則會造成演化。

## 類別

[Genetic_Distribution.svg](https://zh.wikipedia.org/wiki/File:Genetic_Distribution.svg "fig:Genetic_Distribution.svg")

  - [定向選擇](../Page/定向選擇.md "wikilink")（Directional
    selection）：往某一方向偏離平均的個體存活率最高，例如較白的北極熊毛。
  - [穩定化選擇](../Page/穩定化選擇.md "wikilink")（stabilizing selection）：
  - [破壞選擇](../Page/破壞選擇.md "wikilink")（Disruptive
    selection）：性狀位在中間的個體存活率最低，例如環境中有黑色和白色石頭時，灰色的蛾無保護色。可能導致[種化](../Page/種化.md "wikilink")。

[性擇和](../Page/性擇.md "wikilink")[生殖選擇和](../Page/生殖選擇.md "wikilink")[人擇有時被視為廣意的自然選擇的次分類](../Page/人擇.md "wikilink")。狹義的自然選擇（又稱生態選擇）指個體在自然環境中存活率的差異，性擇是取得交配對象的能力差異，而生殖選擇則是交配後生育後代的數目差異。[人工選擇則將自然選擇概念應用在受人類圈養的生物上](../Page/人工選擇.md "wikilink")，例如家畜、寵物與農作物的育種。

## 單位

最容易觀察到的自然選擇是作用在[個體上](../Page/個體.md "wikilink")，依據個體之間性狀的不同。

因為是基因帶有個體的大多數資訊但不能自我複製，George C.
Williams和[理察·道金斯主张以基改為單位的演化觀點](../Page/理察·道金斯.md "wikilink")。個體只是基因為了自我複製所製造的「機器」。這種觀點在解釋[轉座子和](../Page/轉座子.md "wikilink")[親緣選擇上較為方便](../Page/親緣選擇.md "wikilink")。

在[癌症的例子中](../Page/癌症.md "wikilink")，基因突變造成細胞的性狀差異，引發在細胞層次的自然選擇。

自然選擇作用在族群上時，稱為[族群選擇](../Page/族群選擇.md "wikilink")（group
selection）。族群選擇在自然界中的普遍程度仍有爭議。

自然選擇也可能運作在物種的層次。當一個物種中的不同族群因為自然選擇而產生[生物分類學上的差異時](../Page/生物分類學.md "wikilink")，則稱為「[物種形成](../Page/物種形成.md "wikilink")」；若是族群因為不受自然選擇青睞而導致族群規模縮小進而消失，則稱為「[滅絕](../Page/滅絕.md "wikilink")」，物種形成和滅絕的速率決定一個支系的適應力。

## 参考文献

## 延伸閱讀

  - For technical audiences

<!-- end list -->

  -
  -
  - [Popper, Karl](../Page/Karl_Popper.md "wikilink") (1978) *Natural
    selection and the emergence of mind.* Dialectica 32:339-55. See
    [1](http://mertsahinoglu.com/research/karl-popper-on-the-scientific-status-of-darwins-theory-of-evolution/)

  - [Sober, Elliott](../Page/Elliott_Sober.md "wikilink") (1984) *The
    Nature of Selection: Evolutionary Theory in Philosophical Focus.*
    University of Chicago Press.

  - [Williams, George C.](../Page/George_C._Williams.md "wikilink")
    (1966) *[Adaptation and Natural Selection: A Critique of Some
    Current Evolutionary
    Thought](../Page/Adaptation_and_Natural_Selection.md "wikilink").*
    Oxford University Press.

  - [Williams, George C.](../Page/George_C._Williams.md "wikilink")
    (1992) *Natural Selection: Domains, Levels and Challenges.* Oxford
    University Press.

<!-- end list -->

  - For general audiences

<!-- end list -->

  - [Dawkins, Richard](../Page/Richard_Dawkins.md "wikilink") (1996)
    *[Climbing Mount
    Improbable](../Page/Climbing_Mount_Improbable.md "wikilink").*
    Penguin Books, ISBN 978-0-670-85018-1.
  - [Dennett, Daniel](../Page/Daniel_Dennett.md "wikilink") (1995)
    *[Darwin's Dangerous Idea: Evolution and the Meanings of
    Life](../Page/Darwin's_Dangerous_Idea.md "wikilink").* Simon &
    Schuster ISBN 978-0-684-82471-0.
  - [Gould, Stephen Jay](../Page/Stephen_Jay_Gould.md "wikilink") (1997)
    *Ever Since Darwin: Reflections in Natural History.* Norton, ISBN
    978-0-393-06425-4.
  - [Jones, Steve](../Page/Steve_Jones_\(biologist\).md "wikilink")
    (2001) *Darwin's Ghost: The Origin of Species Updated.* Ballantine
    Books ISBN 978-0-345-42277-4. Also published in Britain under the
    title *Almost Like a Whale: The Origin of Species Updated.*
    Doubleday. ISBN 978-1-86230-025-5.
  - [Lewontin, Richard](../Page/Richard_Lewontin.md "wikilink") (1978)
    *Adaptation.* Scientific American 239:212-30
  - [Mayr, Ernst](../Page/Ernst_Mayr.md "wikilink") (2001) *What
    Evolution Is*. Weidenfeld & Nicolson, London. ISBN 978-0-297-60741-0
  - [Weiner, Jonathan](../Page/Jonathan_Weiner.md "wikilink") (1994)
    *[The Beak of the
    Finch](../Page/The_Beak_of_the_Finch.md "wikilink"): A Story of
    Evolution in Our Time.* Vintage Books, ISBN 978-0-679-73337-9.

<!-- end list -->

  - Historical

<!-- end list -->

  -
  - Kohm M (2004) *A Reason for Everything: Natural Selection and the
    English Imagination.* London: Faber and Faber. ISBN
    978-0-571-22392-3. For review, see
    [2](http://human-nature.com/nibbs/05/wyhe.html) van Wyhe J (2005)
    *Human Nature Review* 5:1-4

## 外部連結

  - [*On the Origin of Species* by Charles
    Darwin](http://www.literature.org/authors/darwin-charles/the-origin-of-species/chapter-04.html)
    – Chapter 4, Natural Selection
  - [Natural
    Selection](https://web.archive.org/web/20080703021425/http://www.wcer.wisc.edu/NCISLA/MUSE/naturalselection/index.html)-
    Modeling for Understanding in Science Education, University of
    Wisconsin
  - [Natural
    Selection](http://evolution.berkeley.edu/evolibrary/search/topicbrowse2.php?topic_id=53)
    from University of Berkeley education website
  - [T. Ryan Gregory: Understanding Natural Selection: Essential
    Concepts and Common
    Misconceptions](http://www.springerlink.com/content/2331741806807x22/fulltext.html)
    Evolution: Education and Outreach
  - [ENCODE threads
    explorer](http://www.nature.com/encode/#/threads/impact-of-evolutionary-selection-on-functional-regions)
    Impact of functional information on understanding variation. [Nature
    (journal)](../Page/Nature_\(journal\).md "wikilink")

{{-}}

[Category:演化生物學](../Category/演化生物學.md "wikilink")
[Category:自然史](../Category/自然史.md "wikilink")