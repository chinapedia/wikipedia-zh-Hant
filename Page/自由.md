**自由**（[和製漢語](../Page/和製漢語.md "wikilink")：，）是一个政治哲学中的概念，意即人类可以自我支配，凭借自由意志而行动。

學術上存在對自由概念的不同見解，在对个人与社会的关系认识上有所不同。自由包括各国宪法规定的言论信息自由和新闻自由，當然還有諸多的自由，例如：思想自由、宗教信仰自由等等。法律概念上則以憲法來保障人的自由權，並同時賦予其國民守護享有各該憲法保障之各種自由權利之義務。

自由，在民主政治中又体现为一种精神：就是对什么是正确不那么确定，但是會守護那有選擇之自由的精神。

## 定义

自由（）有多种含义：

1.  意指由[憲法或](../Page/憲法.md "wikilink")[基本法所保障的一種權利或自由權](../Page/基本法.md "wikilink")，包括政府在內任何人都不可以不起訴或以莫須有的罪名而拘捕扣留任何人。\[1\]
2.  把個人從某些非政府的[私權力中解放](../Page/私權力.md "wikilink")，
    如[奴隸主](../Page/奴隸主.md "wikilink")、[父母](../Page/父母.md "wikilink")、[丈夫](../Page/丈夫.md "wikilink")、[軍閥](../Page/軍閥.md "wikilink")、[地主或](../Page/地主.md "wikilink")[獨裁政府等](../Page/獨裁政府.md "wikilink")。
3.  盡可能地以法律保障與賦以相對應之義務，讓最大多數人擁有說出屬於他們的言論之自由。([言論自由](../Page/言論自由.md "wikilink"))
4.  [自律](../Page/自律.md "wikilink")（）意义下的自由。[康德奧側在此意义上使用自由一词](../Page/伊曼努尔·康德.md "wikilink")。
5.  是人在自己所拥有的领域自主追求自己设定目标的[权利](../Page/权利.md "wikilink")。
6.  在不妨碍他人同等自由的前提下个人运用自身和自身所具有的能力(如财产)从事一切活动的权利。
7.  不干涉別人的私人生活，只要不危害旁人就不應當禁制，如個人的[私有財產](../Page/私有財產.md "wikilink")、[私隱](../Page/私隱.md "wikilink")、[性取向](../Page/性取向.md "wikilink")、[信仰](../Page/信仰.md "wikilink")、[政治主張](../Page/政治.md "wikilink")、[嗜好](../Page/嗜好.md "wikilink")，但對於怎樣界定有嚴重的分歧，這也是到底放縱和自律的定義標準問題。
8.  把被[外國所](../Page/外國.md "wikilink")[侵略中解放](../Page/侵略.md "wikilink")[殖民地或](../Page/殖民地.md "wikilink")[領土](../Page/領土.md "wikilink")，恢復[獨立或歸還原有國家的](../Page/獨立.md "wikilink")。
9.  不具有[性別意識](../Page/性別意識.md "wikilink")、思想，認為[性別是對自己和](../Page/性別.md "wikilink")[人身自由的一種限制](../Page/人身自由.md "wikilink")。

[Eugène_Delacroix_-_La_liberté_guidant_le_peuple.jpg](https://zh.wikipedia.org/wiki/File:Eugène_Delacroix_-_La_liberté_guidant_le_peuple.jpg "fig:Eugène_Delacroix_-_La_liberté_guidant_le_peuple.jpg")名画：[自由引导人民](../Page/自由引导人民.md "wikilink")\]\]
而[法国大革命纲领性文件](../Page/法国大革命.md "wikilink")《[人权宣言](../Page/人权和公民权宣言.md "wikilink")》中，对自由的定义为：
絕對的自由在理論上可能存在，但由於[社會是由人與人所組成](../Page/社會.md "wikilink")，自由不僅是個人的議題，而是社會中各個主體之間彼此互相界定的程度，因此托马斯·杰斐逊認為个人的自由受制于他人的同等的自由。進而有人認為自由與責任相關，有相關之自由即應負相關之責任。自由的边界是人权，自由止于人权。

自由是[政治哲学的核心概念](../Page/政治哲学.md "wikilink")。自由也是一种社会概念。自由是社会人的权利。与自由相对的，是[奴役](../Page/奴役.md "wikilink")。

[孫中山多次在演講中引述](../Page/孫中山.md "wikilink")[彌爾的話指出](../Page/约翰·斯图尔特·密尔.md "wikilink")，一個人的自由，以不侵犯他人的自由為範圍，才是真自由。如果侵犯他人的範圍，便是不自由\[2\]。

[第二次世界大战中](../Page/第二次世界大战.md "wikilink")（1941年1月6日），美国总统[罗斯福在国情咨文中提出了著名的](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")“[四大自由](../Page/四大自由.md "wikilink")”\[3\]。

简略来说，这四大自由便是[言论自由](../Page/言論自由.md "wikilink")、[宗教自由](../Page/宗教自由.md "wikilink")、[免于匮乏的自由和](../Page/免于匮乏的自由.md "wikilink")[免于恐惧的自由](../Page/免於恐懼的自由.md "wikilink")。

[联合国](../Page/联合国.md "wikilink")[世界人权宣言重申了这四大自由的精神](../Page/世界人权宣言.md "wikilink")：「人人享有言論和信仰自由並免予恐懼和匱乏」（《[世界人權宣言](../Page/s:世界人權宣言.md "wikilink")》）

20世纪下半叶，[以賽亞·伯林开始用](../Page/以賽亞·伯林.md "wikilink")“两种自由”的概念来划分自由：“[消极自由](../Page/消极自由.md "wikilink")”和“[积极自由](../Page/积极自由.md "wikilink")”。他认为，积极自由是指人在“主动”意义上的自由，即作为[主体的人做的决定和选择](../Page/主体.md "wikilink")，均基于自身的主动意志而非任何外部力量。当一个人是自主的或自决的，他就处于“积极”自由的状态之中。这种自由是“去做……的自由”。而消极自由指的是在“被动”意义上的自由。即人在意志上不受他人的强制，在行为上不受他人的干涉，也就是“免于强制和干涉”的状态。

## 词源

现代意义上的自由一词是从日本引入的，日本在翻译外国文献时首次使用「自由」一词来对应「Liberty」。在翻译的争议问题上，[严复先後用過](../Page/严复.md "wikilink")「自由」、「自繇」（繇、由二字相通）、「群己」等譯名。然而最终流传下来爲民众所接受的，还是「自由」一词。

## 參見

  - [人权](../Page/人权.md "wikilink")
  - [自由主义](../Page/自由主义.md "wikilink")
  - [政治自由](../Page/政治自由.md "wikilink")
  - [公民自由](../Page/公民自由.md "wikilink")
  - [人身自由](../Page/自由人.md "wikilink")
  - [出入境自由](../Page/出入境自由.md "wikilink")
  - [集會自由](../Page/集會自由.md "wikilink")
  - [結社自由](../Page/結社自由.md "wikilink")
  - [言論自由](../Page/言論自由.md "wikilink")
  - [生存自由](../Page/無條件基本收入.md "wikilink")
  - [自由意志](../Page/自由意志.md "wikilink")
  - [思想自由](../Page/思想自由.md "wikilink")
  - [宗教自由](../Page/宗教自由.md "wikilink")
  - [学术自由](../Page/学术自由.md "wikilink")
  - [自由软件](../Page/自由软件.md "wikilink")
  - [新闻自由](../Page/新闻自由.md "wikilink")
      - [无国界记者](../Page/无国界记者.md "wikilink")
  - [信息自由](../Page/信息自由.md "wikilink")
      - [连接自由](../Page/连接自由.md "wikilink")
  - [戀愛自由](../Page/自由戀愛.md "wikilink")
  - [自由貿易](../Page/自由貿易.md "wikilink")
  - [自由经济](../Page/自由经济.md "wikilink")
      - [经济自由度指数](../Page/经济自由度指数.md "wikilink")
  - [自由市](../Page/利伯维尔.md "wikilink")
  - [無性別](../Page/無性別.md "wikilink")

## 参考文献

### 引用

### 来源

  - 林毓生：[〈哈耶克論自由的創造力〉](http://www.cenet.org.cn/article.asp?articleid=75791)
  - 林毓生：〈[哈耶克與博蘭尼在法治觀念上論自由的洞見](http://hkmag.crntt.com/crn-webapp/mag/docDetail.jsp?coluid=25&docid=103182923&page=1)〉

## 外部链接

  -
{{-}}

[Category:人權](../Category/人權.md "wikilink")
[自由權](../Category/自由權.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")
[Category:社会概念](../Category/社会概念.md "wikilink")
[Category:自由主义理论](../Category/自由主义理论.md "wikilink")

1.  [藝術與建築索引典—自由](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300055497)
    ，於2011年4月11日查閱
2.
3.
    EDSITEment|accessdate=2017-08-28|work=edsitement.neh.gov|language=en}}