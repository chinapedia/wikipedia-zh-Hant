[thumb](../Page/file:Freehugs_sydney2004.jpg.md "wikilink")
**自由拥抱运动**，或稱**免费拥抱运动**，[中国大陆稱為](../Page/中国大陆.md "wikilink")**抱抱團**，是指在主动上街[拥抱陌生人的一个活動](../Page/拥抱.md "wikilink")。原自於[澳大利亚人](../Page/澳大利亚.md "wikilink")[Juan
Mann提出的](../Page/Juan_Mann.md "wikilink")“Free Hugs”，以拒绝冷漠，通过拥抱向陌生人传递温暖。

## 各地免费擁抱運動

### 台灣

[台灣也出现过多次免费拥抱活动](../Page/台灣.md "wikilink")，如2012年12月31日跨年開始持續每年舉辦free
hugs跨年活動

### 中國

  - [长沙](../Page/长沙.md "wikilink")：在才子豪的发动下，抱抱团在长沙成立，并率先在2006年10月21日下午在[黄兴南路商业步行街开始了第一次](../Page/黄兴南路商业步行街.md "wikilink")“免费拥抱”的活动。
  - [北京](../Page/北京.md "wikilink")：从天涯社区看到长沙“抱抱团”的公益活动后，几名北京的年轻人也开始在[王府井开始免费拥抱活动](../Page/王府井.md "wikilink")。但是仅进行了1个小时，“北京抱抱团”拥抱活动成员被警方带到派出所「教育」。

### 澳门

[澳门也有抱抱团活动的踪迹](../Page/澳门.md "wikilink")。2013年3月，[澳门大学的五十多名学生和校友连续两天在新马路](../Page/澳门大学.md "wikilink")、[大三巴一带进行免费擁抱活动](../Page/大三巴.md "wikilink")。\[1\]

### 韩国

[韩国首都](../Page/韩国.md "wikilink")[首尔出现过多次免费拥抱活动](../Page/首尔.md "wikilink")，如2011年12月28日\[2\]和2012年4月7日\[3\]均有游客目击抱抱团活动。

## 参考资料

## 外部链接

  - [台灣跨年FREE
    HUGS的“就是愛擁抱”](https://www.facebook.com/%E5%B0%B1%E6%98%AF%E6%84%9B%E6%93%81%E6%8A%B1-197154690725872/)
  - [长沙“抱抱团”街上送拥抱
    专家表示应健康引导](http://hn.rednet.cn/c/2006/10/30/1016388.htm)
  - [北京“抱抱团”街头“拥抱陌生人”](http://news.163.com/06/1029/03/2UISBNNN0001124J.html)
  - [YouTube上的原始视频](http://www.youtube.com/watch?v=vr3x_RRJdd4)
  - [新浪“抱抱团”专题](http://bbs.sina.com.cn/zt/w/baobaotuan/)
  - [2011香港平安夜FREEHUGS視頻](http://www.youtube.com/watch?v=gZIXtHiyDYY)

[Category:抱](../Category/抱.md "wikilink")
[Category:社會運動](../Category/社會運動.md "wikilink")

1.
2.
3.