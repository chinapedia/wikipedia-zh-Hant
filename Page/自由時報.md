\- 567993600 ) / 86400 + 0.5 ) round 0}}號（{{ \#time: Y }}年{{ \#time: M
}}{{ \#time: j }}日） |ceased publication = |language =
[正體中文](../Page/繁体中文.md "wikilink") |price =
[新臺幣](../Page/新臺幣.md "wikilink")10元 |headquarters =
[臺北市](../Page/臺北市.md "wikilink")[內湖區港墘里](../Page/內湖區.md "wikilink")020鄰瑞光路399號、405號
|circulation = 562,393份
（2016年7月1日至9月30日）\[1\] |ISSN = |website = [自由時報](http://www.ltn.com.tw/)
|政治立场=獨派、親綠 }}

《**自由時報**》（原名《**自由日報**》，於1987年改名）是一家創立於1980年4月17日的[臺湾平面](../Page/台湾.md "wikilink")[报纸媒体](../Page/報紙.md "wikilink")。發行者全名為「**自由時報企業股份有限公司**」。

## 沿革

[Union_Commercial_Building_20160305.jpg](https://zh.wikipedia.org/wiki/File:Union_Commercial_Building_20160305.jpg "fig:Union_Commercial_Building_20160305.jpg")南京東路分行。\]\]
[Liberty_Times_Printing_Plant_and_Union_Building_Liberty_Plaza_2011-02-08a.jpg](https://zh.wikipedia.org/wiki/File:Liberty_Times_Printing_Plant_and_Union_Building_Liberty_Plaza_2011-02-08a.jpg "fig:Liberty_Times_Printing_Plant_and_Union_Building_Liberty_Plaza_2011-02-08a.jpg")
[Liberty_Times_head_office_20150926.jpg](https://zh.wikipedia.org/wiki/File:Liberty_Times_head_office_20150926.jpg "fig:Liberty_Times_head_office_20150926.jpg")
[Liberty_Times_Printing_Plant_flagpoles_2011-07-01.jpg](https://zh.wikipedia.org/wiki/File:Liberty_Times_Printing_Plant_flagpoles_2011-07-01.jpg "fig:Liberty_Times_Printing_Plant_flagpoles_2011-07-01.jpg")，中華民國國旗右方旗幟為《自由時報》旗，基座刻《自由時報》標誌與林榮三所題「自由時報」四字。\]\]

### 前身

  - 1946年12月12日以《**臺東導報**》為名創刊，是[三民主義青年團臺東分團機關的刊物](../Page/三民主義青年團.md "wikilink")。
  - 1948年12月12日，由於經營情況不佳，在[臺東縣](../Page/臺東縣.md "wikilink")[國大代表](../Page/國大代表.md "wikilink")[陳振宗等地方人士支持下](../Page/陳振宗.md "wikilink")，《臺東導報》申請改為《**臺東新報**》，陳振宗出任發行人，正式發行日刊報紙。
  - 1950年10月11日，由於虧損太大，《臺東導報》宣佈停刊。
  - 1952年7月12日，經[中國國民黨臺東縣黨部主任委員](../Page/中國國民黨.md "wikilink")[吳若萍出面主持](../Page/吳若萍.md "wikilink")，《臺東導報》才得以復刊；但因銷售範圍僅限於[花蓮與](../Page/花蓮縣.md "wikilink")[臺東兩縣](../Page/臺東縣.md "wikilink")，缺乏[廣告收入](../Page/廣告.md "wikilink")，經營困難，勉強維持到1961年1月1日，終於宣告停刊。
  - 1961年1月1日，《臺東導報》停刊後迅速轉手，買主易名為《**遠東日報**》重新發刊。
  - 1978年初，經營權再轉移，《遠東日報》改名為《**自強日報**》，此次轉手發行地也從臺東遷往[彰化](../Page/彰化縣.md "wikilink")。
  - 1980年4月17日，《自強日報》被以[新臺幣四千萬元轉賣給](../Page/新臺幣.md "wikilink")[林榮三的](../Page/林榮三.md "wikilink")[聯邦企業集團](../Page/聯邦企業集團.md "wikilink")

<!-- end list -->

  - 《**自由日報**》（1981年1月1日～1987年9月）

<!-- end list -->

  - 1981年1月1日，《自強日報》改名為《**自由日報**》，正式成為[中臺灣地區之地方報紙](../Page/中臺灣.md "wikilink")。
  - 1986年9月15日，《自由日報》獲准將總部遷至臺北縣新莊市（今[新北市](../Page/新北市.md "wikilink")[新莊區](../Page/新莊區.md "wikilink")）。

<!-- end list -->

  - 《**自由時報**》（1987年9月～今）

<!-- end list -->

  - 1987年9月，再度更名為《**自由時報**》，並開始積極往全國性報紙之規模發展。
  - 1989年，總部自[新莊遷至](../Page/新莊區.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[南京東路二段](../Page/南京東路.md "wikilink")137號1樓，位於「聯邦商業大樓」。
  - 1992年舉辦「12週年回饋讀者，6000兩黃金大贈獎」，自此真正崛起，後兩年還有送驕車、送別墅等史無前例的抽獎活動，知名度幾乎壓倒傳統《[聯合報](../Page/聯合報.md "wikilink")》和《[中國時報](../Page/中國時報.md "wikilink")》\[2\]。
  - 1995年4月，《自由時報》在[美國](../Page/美國.md "wikilink")[大紐約地區發行的](../Page/大紐約地區.md "wikilink")[華文姐妹報](../Page/華文.md "wikilink")《美東自由時報》創刊，與《自由時報》是[代理關係](../Page/代理.md "wikilink")，創辦人是[江蕙美](../Page/江蕙美.md "wikilink")。2007年1月，[民進黨人士與美國親](../Page/民進黨.md "wikilink")[綠臺灣僑民領袖共](../Page/泛綠聯盟.md "wikilink")50多人籌措捐助12萬[美元給當時已面臨財務危機的](../Page/美元.md "wikilink")《美東自由時報》。2008年6月14日，由於不敵財務危機、無法拓展讀者群與廣告主，《美東自由時報》宣佈停刊\[3\]。
  - 1995年起《自由時報》將贈報作為常規發行手段，據傳有時達總發行量30%（當時他報通常占1%至3%），當時臺灣唯一的媒體接觸率指標—SRT的媒體閱讀率大調查中，《自由時報》很快超過《聯合報》和《中國時報》\[4\]。
  - 1999年8月末，《自由時報》開始在每週一至週五清晨，向[臺北市各](../Page/臺北市.md "wikilink")[高中](../Page/高中.md "wikilink")、[高職供應大量完整版的報紙](../Page/高職.md "wikilink")，免費供應各校師生，由班級幹部於晨間拿教學日誌時帶回各教室，同時期還有同屬泛三重幫[宏國集團的](../Page/宏國集團.md "wikilink")《[大成報](../Page/大成報.md "wikilink")》也搭上順風車進入校園，類似行銷手法一般稱為「免費報」。這是臺灣報社首次使用「免費報」來提高「閱報率」。但大部份「免費報」的發送也僅止於公共場所由人力向不特定人派發，以校園為發放重點的較少。其後《[中國時報](../Page/中國時報.md "wikilink")》及《[聯合報](../Page/聯合報.md "wikilink")》也想以類似手法，在[捷運車站外或學校贈送報紙提高閱報率](../Page/捷運.md "wikilink")\[5\]。

### 現況

2005年5月16日，《自由時報》總部遷至[臺北市](../Page/臺北市.md "wikilink")[內湖區瑞光路](../Page/內湖區.md "wikilink")399號1樓，位於「聯邦大樓自由廣場」內。「聯邦大樓自由廣場」樓高15層，總高度110公尺，佔地7000[坪](../Page/坪.md "wikilink")，建築面積超過2萬坪；《自由時報》印刷廠則設於「聯邦大樓自由廣場」隔壁的405號，樓高6層。

2012年10月，《自由時報》推出電子報手機版本（m.ltn.com.tw）和電腦版短網址（ltn.com.tw）\[6\]。

2013年2月10日，《自由時報》副總編輯兼政治新聞群組執行長[胡文輝升任總編輯](../Page/胡文輝.md "wikilink")，原總編輯[陳進榮與](../Page/陳進榮.md "wikilink")[俞國基共同擔任副社長](../Page/俞國基.md "wikilink")\[7\]。

2014年4月8日，《自由時報》正式將官方網站（自由時報電子報）域名改為「www.ltn.com.tw」，並推出新版電子報\[8\]。

2015年4月8日，《[新新聞](../Page/新新聞.md "wikilink")》引述一位曾經參與[臺北市政府勞動局媒體勞動檢查相關會議的人說](../Page/臺北市政府勞動局.md "wikilink")，他常聽《自由時報》記者不滿該報「每天寫[人權](../Page/人權.md "wikilink")，自己卻沒[勞權](../Page/勞權.md "wikilink")」：該報沒有[工會](../Page/工會.md "wikilink")，員工被資方以「花錢支持老闆」為由強迫訂報；而且雖然該報記者休假制度為隔周休二日，但每逢每個月第五周的應有休假日，該報記者都被長官要求「自行吸收，繼續支持老闆」\[9\]。2015年6月5日，[自由時報企業工會取得](../Page/自由時報企業工會.md "wikilink")[臺北市政府](../Page/臺北市政府.md "wikilink")〈臺北市工會登記證書〉，發表聲明宣布正式成立，首任理事長為臺北市政記者[郭安家](../Page/郭安家.md "wikilink")\[10\]。

2015年11月28日，創辦人林榮三因肝腫瘤併發心肺衰竭於臺大醫院離世；2005年林榮三把《自由時報》交給次子林鴻邦以後，雖然淡出編務，但[社論等內容仍要親自過目](../Page/社論.md "wikilink")\[11\]。

2017年7月1日，《自由時報》執行副總編輯鄒景雯升任總編輯，原總編輯胡文輝升任副社長。

2018年7月15日，文化週報改為刊在A15版，出刊1頁，生活週報恢復為週六和週日出刊。

### 政治立場

[李登輝執政時期](../Page/李登輝.md "wikilink")，《自由時報》偏向[國民黨本土派的立場](../Page/國民黨.md "wikilink")；2000年以後，《自由時報》偏向[民進黨與](../Page/民進黨.md "wikilink")[泛綠政黨的立場](../Page/泛綠.md "wikilink")。\[12\]。

## 發行情況

[Liberty_Times_and_Apple_Daily_Taiwan_coverage_on_Lee_Ming-cheh's_wife_Lee_Ching-yu_protest_against_her_husband_detained_by_China_20170414.jpg](https://zh.wikipedia.org/wiki/File:Liberty_Times_and_Apple_Daily_Taiwan_coverage_on_Lee_Ming-cheh's_wife_Lee_Ching-yu_protest_against_her_husband_detained_by_China_20170414.jpg "fig:Liberty_Times_and_Apple_Daily_Taiwan_coverage_on_Lee_Ming-cheh's_wife_Lee_Ching-yu_protest_against_her_husband_detained_by_China_20170414.jpg")
[自由時報報紙販賣機_20080728.jpg](https://zh.wikipedia.org/wiki/File:自由時報報紙販賣機_20080728.jpg "fig:自由時報報紙販賣機_20080728.jpg")》採用的金雨企業報紙販賣機\]\]
2007年，《自由時報》在「傳統三大報」（《聯合報》、《中國時報》、《中央日報》）中首先加入[中華民國發行公信會](../Page/中華民國發行公信會.md "wikilink")（ROC-ABC）進行有費報發行量稽核（ROC-ABC在稽核發行量時不列計「免費報」，即免費供不特定人士自由取閱的自由時報報紙）\[13\]，稽核報告證明於2013年4月－6月平均每日有費報紙發行量為628,295份，2017年第四季(10-12月)平均每日有費報紙發行量為533,159份。

## 姊妹報

  - 《[臺北時報](../Page/台北時報.md "wikilink")》（*Taipei Times*）：於1999年6月1日創刊。

## 報版

  - A疊：焦點新聞
      - 生活
      - 社會
      - 文教
      - 國際新聞
      - 國際財經
      - 地方
      - 文化週報（A15版）:週日、週一出刊
      - 自由廣場
  - B疊：體育（原為S疊）
  - C疊：聚富（週一為「財經週報」）
  - D疊：影視消費（包含節目表）
  - E疊：生活週報（逢週末出刊）
  - G疊：分類廣告

## 投資事業

### 轉投資

  - 人力銀行網站：Yes123求職網。

### 失敗的轉投資

  - 電視臺：2006年申請籌設三個電視頻道——自由新聞臺、自由電視臺及自由news頻道，並獲[國家通訊傳播委員會核發執照](../Page/國家通訊傳播委員會.md "wikilink")，但因未在期限前開播，執照已經失效。

## 民調中心

2010年4月28日，據《[今日新聞網](../Page/今日新聞網.md "wikilink")》報導稱，針對自由時報對雙英辯ECFA的民調，國民黨立委[林郁方表示](../Page/林郁方.md "wikilink")，《自由時報》該講出民調中心設在哪，不能「只在此山中，雲深不知處。」國民黨文傳會主委[蘇俊賓更直言](../Page/蘇俊賓.md "wikilink")「為何久久才做一次民調，一做就嚇死人？」。中天記者還直接打電話到《自由時報》，希望能轉到「民調中心」，但無奈電話轉來轉去就是轉不到民調中心，電話那頭的小姐要記者稍晚找一位「編政組的蘇組長」\[14\]。同日，時事評論員[董智森在](../Page/董智森.md "wikilink")[TVBS頻道節目](../Page/TVBS頻道.md "wikilink")《[新聞夜總會](../Page/新聞夜總會.md "wikilink")》中質疑，《自由時報》「假的民調」、「他民調中心只有一個人，自己在那邊編數字」。後來，《自由時報》向法院提告，要求董智森登報道歉並相自由時報賠償新臺幣150萬元。

翌日(2010年4月29日)，《自由時報》記者蘇永耀、田世昊在《自由時報》發表一份標題為「中時又來了
ECFA民調結果不同就質疑本報」的聲明，其聲稱是源由於2006年高雄市長選舉時，自由時報在選前所做並發布的民調，與選舉結果十分接近，精準預測高雄市長的選舉結果，中國時報發布的民調，則與選舉結果差很大，該文稱「自己不準卻反而質疑本報」來回應中國時報「真的這麼準？自由時報應公布民調方法」。

2012年7月13日，中華民國最高法院認定，《自由時報》應比照其他媒體進行民調的嚴謹程度才符合民眾期待，但《自由時報》坦承沒有常設民調中心，當時《中時》也曾用報導質疑《自由時報》民調真實性；而董智森詢問新聞同業，得到答案都是《自由》無常設民調中心，已盡查證責任，因此對可受公評的《自由時報》民調真假一事發表評論，並非惡意侵害《自由時報》聲譽，故判[董智森勝訴](../Page/董智森.md "wikilink")，免賠《自由時報》，定讞。\[15\]。

## 相關爭議

### 改圖

  - 2005年10月18日，《自由時報》財經版頭版（C1版）引用[法新社提供的一張](../Page/法新社.md "wikilink")[證券公司大廳中](../Page/證券公司.md "wikilink")[股民看報紙的照片](../Page/股民.md "wikilink")；但原始照片中，股民所看的報紙應是《聯合報》，《自由時報》卻將報頭變造為《自由時報》報頭。《聯合報》曾刊登2005年10月17日原版報紙的照片，與《自由時報》版照片對照，證明《自由時報》變造報頭\[16\]。

### 南線專案

### 盧貝松狗仔爭議事件

  - 2013年10月26日，《自由時報》專案組（[狗仔隊](../Page/狗仔隊.md "wikilink")）偷拍[盧貝松電影](../Page/盧貝松.md "wikilink")《[露西](../Page/露西_\(電影\).md "wikilink")》在台北市[三軍總醫院汀州院區的拍攝工作](../Page/三軍總醫院.md "wikilink")\[17\]，引發保鑣阻擋及盧貝松不滿\[18\]。兩天後《自由時報》專案組批評保鑣暴力阻擋該報記者拍攝，並批評[台北市電影委員會沒有做好協調](../Page/台北市電影委員會.md "wikilink")，並批評沒有「拍電影的就比較偉大，就可以暴力侵犯也在工作的媒體記者」這回事\[19\]同日，《中國時報》引述《露西》台灣工作人員說法，《自由時報》狗仔隊於本月26日傍晚守在三軍總醫院汀州院區正對面的大樓屋頂拍攝，後來佯稱「包包被黑人保鑣摔在地上」報警處理\[20\]

## 注释

## 参考文献

  - [自由時報「全民關心台灣經濟」系列社論](http://old.ltn.com.tw/2001/new/mar/31/r-editorial.htm)
  - [史明](../Page/史明.md "wikilink")：[《自由時報》「不要怕臺灣民族主義」](http://news.ltn.com.tw/news/opinion/paper/18529).自由電子報
    自由評論網.2005-05-13
  - 礙呆丸社社長.[《自由時報》新聞照片造假
    王效蘭憑空消失](http://blog.udn.com/hsiangsho/1509956).ㄞˋ呆丸社
    - udn部落格.2008-01-04

## 外部連結

  - [自由時報電子報](http://www.ltn.com.tw/)

  -
## 參見

  - [林榮三](../Page/林榮三.md "wikilink")
  - [聯邦企業集團](../Page/聯邦企業集團.md "wikilink")
  - [臺灣報紙列表](../Page/台灣報紙列表.md "wikilink")
  - [臺灣媒體亂象](../Page/台灣媒體亂象.md "wikilink")

{{-}}

[自由時報](../Category/自由時報.md "wikilink")
[Category:臺灣中文報紙](../Category/臺灣中文報紙.md "wikilink")
[Category:台灣新聞網站](../Category/台灣新聞網站.md "wikilink")
[Category:中文新聞網站](../Category/中文新聞網站.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")
[Category:1980年台灣建立](../Category/1980年台灣建立.md "wikilink")
[Category:1980年建立的出版物](../Category/1980年建立的出版物.md "wikilink")

1.  [《自由時報》有費報紙發行量稽核報告](https://www.abc.org.tw/upload/report/a24ee-2016.12.02-2016-7-9-.pdf)

2.

3.  黃兆平.[經營13年
    美東自由時報今最後一天出報](http://epochtimes.com/b5/8/6/14/n2154858.htm).[中央通訊社](../Page/中央通訊社.md "wikilink").2008-06-14

4.
5.  林照真.[調查的迷思Ⅲ：破解閱讀率　誰讓報紙重量不重質](http://web.cc.ntnu.edu.tw/~t10021/21.doc).[天下雜誌](../Page/天下雜誌.md "wikilink")309期

6.  高旻均.[自由電子報推出手機版本](http://mol.mcu.edu.tw/show_2009.php?nid=150370).銘報.2012-10-04


7.

8.  巫其倫.[LTN自由時報改版上路
    強化網站完整性](http://mol.mcu.edu.tw/show_2009.php?nid=161486).銘報.2014-04-17


9.

10.

11.

12.
13. 根據[財團法人中華民國發行公信會](../Page/財團法人中華民國發行公信會.md "wikilink")[**2013年4-6月**發行量稽核報告出爐](http://www.abc.org.tw/img/m2/自由時報2013年4-6月發行量稽核報告-1.pdf)


14.

15.

16.

17.

18.

19.

20.