[Freie_Demokratische_Partei_(Logo,_2001-2013).svg](https://zh.wikipedia.org/wiki/File:Freie_Demokratische_Partei_\(Logo,_2001-2013\).svg "fig:Freie_Demokratische_Partei_(Logo,_2001-2013).svg")
**自由民主党**（，缩写为），1968年–2001年期间缩写为，是[德国的一个](../Page/德国.md "wikilink")[经济自由主义政党](../Page/经济自由主义.md "wikilink")，是德国政坛中历史比较悠久的政党之一。

## 深入剖析

### 党纲

威斯巴登基本原则确定了德国自由民主党当前的基本路线。

#### 经济政策

在[德国聯邦议会中](../Page/德国聯邦议会.md "wikilink")，自由民主党是最为旗帜鲜明地主张[经济自由主义的政党](../Page/经济自由主义.md "wikilink")，有时候也被政治对手斥之为[新自由主义的代理人](../Page/新自由主义.md "wikilink")。自由民主党认为[全球化是大势所趋](../Page/全球化.md "wikilink")，对于德国是机会多于威胁。德国应该顺应时代潮流，加快进行政治、经济和社会的改革，以便在[全球化的时代保持竞争力](../Page/全球化.md "wikilink")。自由民主党改革纲领的核心目标是改善[投资环境](../Page/投资环境.md "wikilink")，从而创造更多的就业机会。简化各种官僚机构和规章制度，改革劳资关系的传统。通过削简各种财政补助平衡预算，减少赤字。在税务制度上，敦促对世界第一庞杂的德国[税法进行大刀阔斧的简化](../Page/税法.md "wikilink")，将个人的[收入税简化成三个等级的](../Page/收入税.md "wikilink")[税率](../Page/税率.md "wikilink")，分布为（15%,
25%,
35%）。不但坚决反对提高[增值税](../Page/增值税.md "wikilink")，而且建议通过全面减税来刺激经济增长。对于社会福利制度，目前由国家财政支持的社会福利名目繁多，自由民主党主张将它们合并成单一的[国民补助金](../Page/国民补助金.md "wikilink")，同时推广各种靠资本运作的私营福利实体，比如商业养老保险，补充甚至逐步取代目前的国家社会福利体系。

#### 社会政策

根据自民党的理念，最大的威胁莫过于无所不在的[国家和以](../Page/国家.md "wikilink")[平均主义为基础的社会模式](../Page/平均主义.md "wikilink")。“捍卫和扩大个人自由”是自由民主党的根本目标，进而提出了“更少国家，够用就好！”的口号，力求减少国家对公民的自由和私人生活的干涉。自民党支持[政教分離](../Page/政教分離.md "wikilink")，以各种扩大个人自由的社会改革方案，提倡[教会要完全被排除在国家体制之外](../Page/教会.md "wikilink")，各个政党应该和宗教团体保持相当的距离。国家反过来应该保护各种支持基本法和民法、尊重民主秩序的宗教团体。

#### 内政

对于传统法律中侵害个人自由的部分，FDP持反对态度。比较有争议的例如“扩大窃听范围法”:
1995年自民黨曾经发起过一次党内表决，三分之二的党员表示赞成“扩大窃听范围法”，
直接导致了持反对态度的联邦司法部长Sabine
Leutheusser-Schnarrenberger辞职。1998年包括自民黨代表的多数联邦议会议员投票通过了“扩大窃听范围法”，一批持反对态度的自民黨員向联邦宪法法院提出诉讼，2004年联邦宪法法院最终判决“扩大窃听范围法”违宪。同时自民黨反对长期储存个人的电话和网络通讯记录。

#### 教育政策

自民黨力求加强学龄前教育。从四周岁起儿童就应该强制接受语言能力测试，语言能力较弱的——大部分是来自移民家庭的孩子将及时在学前语言强化班里提高语言能力。从3周岁开始到学前班其间，每个孩子都有权免费在幼儿园得到一个位置。从小学一年级开始，所有儿童都应该接受各种形式的外语教育。普及全日制小学，推行12年制基础教育，也是自民黨教育方针之一。自民黨支持中小学实行“小学－初中－高中”三级体制，反对13年的“一体化学校”，这样学生能够根据自己的能力和兴趣获得最适合自己的教育环境。同时反对“德语书写标准改革”，为此自民黨[2005年德国联邦选举的竞选纲领就是采用旧的书写标准](../Page/2005年德国联邦选举.md "wikilink")。自民黨主张对高等教育收取学费，用来支持学校建设。废除对科学研究起限制作用的法律，加强德国作为高科技国家的地位。

#### 欧洲政策

自民黨将自己定位为一个推进欧洲一体化的政党，目标是实现一个在政治上一体化，而且有着共同对内对外安全防务政策的欧洲。对目前的[欧盟宪法草案应该进行](../Page/欧盟宪法.md "wikilink")[全民公决](../Page/全民公决.md "wikilink")。统一的[欧盟应该在政治上能够高效率做出决断](../Page/欧盟.md "wikilink")，并且切实贯彻执行。自民黨原则上支持[土耳其加入欧盟](../Page/土耳其.md "wikilink")，当时当前进行的入盟谈判不应该预先设定任何结果。考虑到未来土耳其可能无法最终实现欧盟的基本条件，或者欧盟本身没有能力继续接纳一个大国作为新成员。谈判的最终目标除了土耳其作为欧盟的全权成员，议程中应该包括其他备用方案。谈判过程中一直要明确的是，欧盟自身的健康成长是要优先考虑的问题。

#### 党内派别

根据个人的兴趣和某些重要问题上的立场不同，自民党员和拥护者乐于将自己称为民权自由主义者和经济自由主义者。比如Sabine
Leutheusser-Schnarrenberger, Gerhart Rudolf Baum和Burkhard
Hirsch主要致力于法制国家和加强民权，而Otto Graf Lambsdorff, Rainer
Brüderle, Carl-Ludwig Thiele和Hermann Otto
Solms等则是经济自由主义的代表人物。这些团体一般被外界称为党内不同的阵营或者派别，当然党内不同派别并没有原则上分歧，而是所关心问题的领域和侧重点不同。

## 历史

1948年12月11日，德国自由民主党正式成立，是由9个1945年形成的自由党合并而成。而这9个自由党则是从1933年被纳粹取缔的德国人民党（DVP）和德国民主党（DDP）的残存份子而组成。自由民主党的首任主席是[特奥多尔·豪斯](../Page/特奥多尔·豪斯.md "wikilink")，原来曾是DDP成员，后来又曾成为DVP成员。1990年德国统一之后，原东德地区的部分自由主义政党组织也加入进德国自由民主党。自民黨在聯邦德國時期，長期參與聯合政府。

[2009年德国联邦议院选举](../Page/2009年德国联邦议院选举.md "wikilink")，自民黨議席由61席增加至93席，其後聯合基民盟組建中右翼聯合政府，直至2013年。

## 各州情况

<table>
<thead>
<tr class="header">
<th><p>联邦州</p></th>
<th><p>主席</p></th>
<th><p><strong><small>成员</strong></p></th>
<th><p><strong><small>Kreisverbände</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Birgit_Homburger.md" title="wikilink">Birgit Homburger</a></p></td>
<td><p>7.100</p></td>
<td><p>37</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Sabine_Leutheusser-Schnarrenberger.md" title="wikilink">Sabine Leutheusser-Schnarrenberger</a></p></td>
<td><p>4.500</p></td>
<td><p>61</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Markus_Löning.md" title="wikilink">Markus Löning</a></p></td>
<td><p>2.700</p></td>
<td><p>12</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Heinz_Lanfermann.md" title="wikilink">Heinz Lanfermann</a></p></td>
<td><p>1.600</p></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Peter_Bollhagen.md" title="wikilink">Peter Bollhagen</a></p></td>
<td><p>400</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Leif_Schrader.md" title="wikilink">Leif Schrader</a></p></td>
<td><p>1.300</p></td>
<td><p>23</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Jörg-Uwe_Hahn.md" title="wikilink">Jörg-Uwe Hahn</a></p></td>
<td><p>6.300</p></td>
<td><p>26</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Hans_Kreher.md" title="wikilink">Hans Kreher</a></p></td>
<td><p>1.200</p></td>
<td><p>17</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Walter_Hirche.md" title="wikilink">Walter Hirche</a></p></td>
<td><p>6.500</p></td>
<td><p>41</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Andreas_Pinkwart.md" title="wikilink">Andreas Pinkwart</a></p></td>
<td><p>17.000</p></td>
<td><p>50</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Rainer_Brüderle.md" title="wikilink">Rainer Brüderle</a></p></td>
<td><p>5.100</p></td>
<td><p>25</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Christoph_Hartmann.md" title="wikilink">Christoph Hartmann</a></p></td>
<td><p>1.300</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Holger_Zastrow.md" title="wikilink">Holger Zastrow</a></p></td>
<td><p>2.500</p></td>
<td><p>22</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Cornelia_Pieper.md" title="wikilink">Cornelia Pieper</a></p></td>
<td><p>2.300</p></td>
<td><p>20</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Jürgen_Koppelin.md" title="wikilink">Jürgen Koppelin</a></p></td>
<td><p>2.500</p></td>
<td><p>15</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Uwe_Barth.md" title="wikilink">Uwe Barth</a></p></td>
<td><p>2.200</p></td>
<td><p>18</p></td>
</tr>
</tbody>
</table>

  - 自由民主党在柏林地区没有党组织，具体工作由该地区的协会组织进行管理。

## 選舉

### 聯邦議會

<table>
<thead>
<tr class="header">
<th><p>選舉</p></th>
<th><p>領袖</p></th>
<th><p>政黨名單</p></th>
<th><p>席次</p></th>
<th><p>政府</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>#</p></td>
<td><p>%</p></td>
<td><p>#</p></td>
<td><p>±</p></td>
<td><p>Position</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/West_German_federal_election,_1949.md" title="wikilink">1949</a></p></td>
<td><p><a href="../Page/Franz_Blücher.md" title="wikilink">Franz Blücher</a></p></td>
<td><p>2,829,920</p></td>
<td><p>11.9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/West_German_federal_election,_1953.md" title="wikilink">1953</a></p></td>
<td><p><a href="../Page/Franz_Blücher.md" title="wikilink">Franz Blücher</a></p></td>
<td><p>2,629,163</p></td>
<td><p>9.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/West_German_federal_election,_1957.md" title="wikilink">1957</a></p></td>
<td><p><a href="../Page/Reinhold_Maier.md" title="wikilink">Reinhold Maier</a></p></td>
<td><p>2,307,135</p></td>
<td><p>7.7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/West_German_federal_election,_1961.md" title="wikilink">1961</a></p></td>
<td><p><a href="../Page/Erich_Mende.md" title="wikilink">Erich Mende</a></p></td>
<td><p>4,028,766</p></td>
<td><p>12.8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/West_German_federal_election,_1965.md" title="wikilink">1965</a></p></td>
<td><p><a href="../Page/Erich_Mende.md" title="wikilink">Erich Mende</a></p></td>
<td><p>3,096,739</p></td>
<td><p>9.5</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/West_German_federal_election,_1969.md" title="wikilink">1969</a></p></td>
<td><p><a href="../Page/瓦尔特·谢尔.md" title="wikilink">瓦尔特·谢尔</a></p></td>
<td><p>1,903,422</p></td>
<td><p>5.8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1972年西德聯邦選舉.md" title="wikilink">1972</a></p></td>
<td><p><a href="../Page/瓦尔特·谢尔.md" title="wikilink">瓦尔特·谢尔</a></p></td>
<td><p>3,129,982</p></td>
<td><p>8.4</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/West_German_federal_election,_1976.md" title="wikilink">1976</a></p></td>
<td><p><a href="../Page/汉斯-迪特里希·根舍.md" title="wikilink">汉斯-迪特里希·根舍</a></p></td>
<td><p>2,995,085</p></td>
<td><p>7.9</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/West_German_federal_election,_1980.md" title="wikilink">1980</a></p></td>
<td><p><a href="../Page/汉斯-迪特里希·根舍.md" title="wikilink">汉斯-迪特里希·根舍</a></p></td>
<td><p>4,030,999</p></td>
<td><p>10.6</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/West_German_federal_election,_1983.md" title="wikilink">1983</a></p></td>
<td><p><a href="../Page/汉斯-迪特里希·根舍.md" title="wikilink">汉斯-迪特里希·根舍</a></p></td>
<td><p>2,706,942</p></td>
<td><p>6.9</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/West_German_federal_election,_1987.md" title="wikilink">1987</a></p></td>
<td><p><a href="../Page/马丁·班格曼.md" title="wikilink">马丁·班格曼</a></p></td>
<td><p>3,440,911</p></td>
<td><p>9.1</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/German_federal_election,_1990.md" title="wikilink">1990</a></p></td>
<td><p><a href="../Page/Otto_Graf_Lambsdorff.md" title="wikilink">Otto Graf Lambsdorff</a></p></td>
<td><p>5,123,233</p></td>
<td><p>11.0</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/German_federal_election,_1994.md" title="wikilink">1994</a></p></td>
<td><p><a href="../Page/克劳斯·金克尔.md" title="wikilink">克劳斯·金克尔</a></p></td>
<td><p>3,258,407</p></td>
<td><p>6.9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/German_federal_election,_1998.md" title="wikilink">1998</a></p></td>
<td><p><a href="../Page/Wolfgang_Gerhardt.md" title="wikilink">Wolfgang Gerhardt</a></p></td>
<td><p>3,080,955</p></td>
<td><p>6.2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/German_federal_election,_2002.md" title="wikilink">2002</a></p></td>
<td><p><a href="../Page/基多·威斯特威勒.md" title="wikilink">基多·威斯特威勒</a></p></td>
<td><p>3,538,815</p></td>
<td><p>7.4</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2005年德国联邦议院选举.md" title="wikilink">2005</a></p></td>
<td><p><a href="../Page/基多·威斯特威勒.md" title="wikilink">基多·威斯特威勒</a></p></td>
<td><p>4,648,144</p></td>
<td><p>9.8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年德國聯邦議院選舉.md" title="wikilink">2009</a></p></td>
<td><p><a href="../Page/基多·威斯特威勒.md" title="wikilink">基多·威斯特威勒</a></p></td>
<td><p>6,316,080</p></td>
<td><p>14.6</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年德国联邦议院选举.md" title="wikilink">2013</a></p></td>
<td><p><a href="../Page/菲利普·罗斯勒.md" title="wikilink">菲利普·罗斯勒</a></p></td>
<td><p>2,083,533</p></td>
<td><p>4.8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2017年德国联邦议院选举.md" title="wikilink">2017</a></p></td>
<td><p><a href="../Page/克里斯蒂安·林德納.md" title="wikilink">克里斯蒂安·林德納</a></p></td>
<td><p>4,997,178</p></td>
<td><p>10.7</p></td>
<td></td>
</tr>
</tbody>
</table>

### 歐洲議會

<table>
<thead>
<tr class="header">
<th><p>Election year</p></th>
<th><p># of<br />
overall votes</p></th>
<th><p>% of<br />
overall vote</p></th>
<th><p># of<br />
overall seats won</p></th>
<th><p>+/–</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/European_Parliament_election,_1979_(West_Germany).md" title="wikilink">1979</a></p></td>
<td><p>1,662,621</p></td>
<td><p>5.9 (#4)</p></td>
<td></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/European_Parliament_election,_1984_(West_Germany).md" title="wikilink">1984</a></p></td>
<td><p>1,192,624</p></td>
<td><p>4.8 (#5)</p></td>
<td></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/European_Parliament_election,_1989_(West_Germany).md" title="wikilink">1989</a></p></td>
<td><p>1,576,715</p></td>
<td><p>5.6 (#6)</p></td>
<td></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/European_Parliament_election,_1994_(Germany).md" title="wikilink">1994</a></p></td>
<td><p>1,442,857</p></td>
<td><p>4.1 (#6)</p></td>
<td></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/European_Parliament_election,_1999_(Germany).md" title="wikilink">1999</a></p></td>
<td><p>820,371</p></td>
<td><p>3.0 (#6)</p></td>
<td></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/European_Parliament_election,_2004_(Germany).md" title="wikilink">2004</a></p></td>
<td><p>1,565,431</p></td>
<td><p>6.1 (#6)</p></td>
<td></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/European_Parliament_election,_2009_(Germany).md" title="wikilink">2009</a></p></td>
<td><p>2,888,084</p></td>
<td><p>11.0 (#4)</p></td>
<td></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p>'''<a href="../Page/European_Parliament_election,_2014_(Germany).md" title="wikilink">2014</a></p></td>
<td><p>986,253</p></td>
<td><p>3.3 (#7)</p></td>
<td></td>
<td><p>9</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 地方議會

<table>
<thead>
<tr class="header">
<th><p>State Parliament</p></th>
<th><p>Election year</p></th>
<th><p># of<br />
overall votes</p></th>
<th><p>% of<br />
overall vote</p></th>
<th><p>Seats</p></th>
<th><p>Government</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>#</p></td>
<td><p>±</p></td>
<td><p>Position</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Landtag_of_Baden-Württemberg.md" title="wikilink">Baden-Württemberg</a></p></td>
<td><p><a href="../Page/Baden-Württemberg_state_election,_2016.md" title="wikilink">2016</a></p></td>
<td><p>445,430</p></td>
<td><p>8.3 (#5) </p></td>
<td></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴伐利亚州议会.md" title="wikilink">Bavaria</a></p></td>
<td><p><a href="../Page/Bavarian_state_election,_2013.md" title="wikilink">2013</a></p></td>
<td><p>389,584</p></td>
<td><p>3.3 (#5) </p></td>
<td></td>
<td><p>16</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Abgeordnetenhaus_of_Berlin.md" title="wikilink">Berlin</a></p></td>
<td><p><a href="../Page/Berlin_state_election,_2016.md" title="wikilink">2016</a></p></td>
<td><p>109,431</p></td>
<td><p>6.7 (#6) </p></td>
<td></td>
<td><p>12</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Landtag_of_Brandenburg.md" title="wikilink">Brandenburg</a></p></td>
<td><p><a href="../Page/Brandenburg_state_election,_2014.md" title="wikilink">2014</a></p></td>
<td><p>14,389</p></td>
<td><p>1.5 (#7) </p></td>
<td></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Bürgerschaft_of_Bremen.md" title="wikilink">Bremen</a></p></td>
<td><p><a href="../Page/Bremen_state_election,_2015.md" title="wikilink">2015</a></p></td>
<td><p>76,754</p></td>
<td><p>6.5 (#5) </p></td>
<td></td>
<td><p>6</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Hamburg_Parliament.md" title="wikilink">Hamburg</a></p></td>
<td><p><a href="../Page/Hamburg_state_election,_2015.md" title="wikilink">2015</a></p></td>
<td><p>262,157</p></td>
<td><p>7.4 (#5) </p></td>
<td></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑森州议会.md" title="wikilink">Hesse</a></p></td>
<td><p><a href="../Page/Hesse_state_election,_2013.md" title="wikilink">2013</a></p></td>
<td><p>157,354</p></td>
<td><p>5.0 (#5) </p></td>
<td></td>
<td><p>14</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Landtag_of_Lower_Saxony.md" title="wikilink">Lower Saxony</a></p></td>
<td><p><a href="../Page/Lower_Saxony_state_election,_2013.md" title="wikilink">2013</a></p></td>
<td><p>354,971</p></td>
<td><p>9.9 (#4) </p></td>
<td></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Landtag_of_Mecklenburg-Vorpommern.md" title="wikilink">Mecklenburg-Vorpommern</a></p></td>
<td><p><a href="../Page/Mecklenburg-Vorpommern_state_election,_2016.md" title="wikilink">2016</a></p></td>
<td><p>24,475</p></td>
<td><p>3.0 (#6) </p></td>
<td></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Landtag_of_North_Rhine-Westphalia.md" title="wikilink">North Rhine-Westphalia</a></p></td>
<td><p><a href="../Page/North_Rhine-Westphalia_state_election,_2017.md" title="wikilink">2017</a></p></td>
<td><p>1,065,307</p></td>
<td><p>12.6 (#3) </p></td>
<td></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Landtag_of_Rhineland-Palatinate.md" title="wikilink">Rhineland-Palatinate</a></p></td>
<td><p><a href="../Page/Rhineland-Palatinate_state_election,_2016.md" title="wikilink">2016</a></p></td>
<td><p>132,262</p></td>
<td><p>6.2 (#4) </p></td>
<td></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Landtag_of_the_Saarland.md" title="wikilink">Saarland</a></p></td>
<td><p><a href="../Page/Saarland_state_election,_2017.md" title="wikilink">2017</a></p></td>
<td><p>17,419</p></td>
<td><p>3.3 (#6) </p></td>
<td></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Landtag_of_the_Free_State_of_Saxony.md" title="wikilink">Saxony</a></p></td>
<td><p><a href="../Page/Saxony_state_election,_2014.md" title="wikilink">2014</a></p></td>
<td><p>61,847</p></td>
<td><p>3.8 (#7) </p></td>
<td></td>
<td><p>14</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Landtag_of_Saxony-Anhalt.md" title="wikilink">Saxony-Anhalt</a></p></td>
<td><p><a href="../Page/Saxony-Anhalt_state_election,_2016.md" title="wikilink">2016</a></p></td>
<td><p>54,525</p></td>
<td><p>4.9 (#6) </p></td>
<td></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Landtag_of_Schleswig-Holstein.md" title="wikilink">Schleswig-Holstein</a></p></td>
<td><p><a href="../Page/Schleswig-Holstein_state_election,_2017.md" title="wikilink">2017</a></p></td>
<td><p>105,770</p></td>
<td><p>11.5 (#4) </p></td>
<td></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Landtag_of_Thuringia.md" title="wikilink">Thuringia</a></p></td>
<td><p><a href="../Page/Thuringian_state_election,_2014.md" title="wikilink">2014</a></p></td>
<td><p>23,352</p></td>
<td><p>2.5 (#7) </p></td>
<td></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 人物

### 曾參與聯合政府的自由民主黨成员

[Otto-Graf-Lambsdorff.jpg](https://zh.wikipedia.org/wiki/File:Otto-Graf-Lambsdorff.jpg "fig:Otto-Graf-Lambsdorff.jpg")
[Wolfgang_Gerhardt.jpg](https://zh.wikipedia.org/wiki/File:Wolfgang_Gerhardt.jpg "fig:Wolfgang_Gerhardt.jpg")

  - [Martin Bangemann](../Page/马丁·班格曼.md "wikilink")，经济部长（1984－1988）
  - [Josef Ertl](../Page/Josef_Ertl.md "wikilink")，食品农业和森林部长（1969－1983）
  - [Hans
    Friderichs](../Page/Hans_Friderichs.md "wikilink")，经济部长（1972－1977）
  - [Hans-Dietrich
    Genscher内务部长](../Page/汉斯-迪特里希·根舍.md "wikilink")（1969－1974），外交部长（1974－1992）
  - [Helmut
    Haussmann](../Page/Helmut_Haussmann.md "wikilink")，经济部长（1988－1991）
  - [Klaus
    Kinkel](../Page/克劳斯·金克尔.md "wikilink")，司法部长（1991－1992），国防部长（1992－1998）
  - [Otto Graf
    Lambsdorff](../Page/Otto_Graf_Lambsdorff.md "wikilink")，经济部长（1977－1984）
  - [Sabine
    Leutheusser-Schnarrenberger](../Page/Sabine_Leutheusser-Schnarrenberger.md "wikilink")，司法部长（1992－1996）
  - [Jürgen W.
    Möllemann](../Page/Jürgen_W._Möllemann.md "wikilink")，教育和研究部长（1987－1991），经济部长（1991－1993）
  - [Günter
    Rexrodt](../Page/Günter_Rexrodt.md "wikilink")，经济部长（1993－1998）
  - [Walter Scheel](../Page/瓦尔特·谢尔.md "wikilink")，经济合作部长（1961－1966）
  - [Edzard
    Schmidt-Jortzig](../Page/Edzard_Schmidt-Jortzig.md "wikilink")，司法部长（1996－1998）

### 著名成员

  - [Rudolf
    Augstein](../Page/鲁道夫·奥格斯坦.md "wikilink")，常年担任新闻杂志*[明镜周刊](../Page/明镜周刊.md "wikilink")*主编
  - [Hans-Artur
    Bauckhage](../Page/Hans-Artur_Bauckhage.md "wikilink")，莱茵普法兹经济部长（1998始）
  - [Thomas Bach](../Page/托马斯·巴赫.md "wikilink")，运动员，世界冠军
  - [Karl-Hermann
    Flach](../Page/Karl-Hermann_Flach.md "wikilink")，常务秘书（1971–73）,Initiator
    der [Freiburger Thesen](../Page/Freiburger_Thesen.md "wikilink")
  - [Wolfgang Haußmann](../Page/Wolfgang_Haußmann.md "wikilink"), Von
    1953–1966 Baden-Württemberg司法部长, Mitbegründer des Widerstandskreis
    „Rettet Stuttgart“.
  - [Hermann
    Höpker-Aschoff](../Page/Hermann_Höpker-Aschoff.md "wikilink")，[Parlamentarischer
    Rat成员](../Page/Parlamentarischer_Rat.md "wikilink")，首任[德国联邦宪法法庭大法官](../Page/德国联邦宪法法庭.md "wikilink")（1951–54）
  - [Emilie
    Kiep-Altenloh](../Page/Emilie_Kiep-Altenloh.md "wikilink")，汉堡Senatorin
    1953–61 (u. a. für Jugend und Soziales), Namensgeberin der
    gleichnamigen Stiftung in [汉堡](../Page/汉堡.md "wikilink")
  - [Marie-Elisabeth
    Lüders](../Page/Marie-Elisabeth_Lüders.md "wikilink")，女权主义者,
    Alterspräsidentin des Bundestages（1953–61）
  - [Reinhold Maier](../Page/Reinhold_Maier.md "wikilink"),
    1952年Baden-Württemberg首位州长, 1957–60联邦主席, 1960–71荣誉主席
  - [Helmut Markwort](../Page/Helmut_Markwort.md "wikilink"),
    *[焦点杂志](../Page/焦点杂志.md "wikilink")*主编
  - [Robert Philipp Nöll von der
    Nahmer](../Page/Robert_Philipp_Nöll_von_der_Nahmer.md "wikilink")，金融学家
  - [Karl-Heinz Paqué](../Page/Karl-Heinz_Paqué.md "wikilink"),
    Sachsen-Anhalt金融部长（seit 2002）

## 参考文献

  - Lothar Albertin: *联邦共和国的政治自由主义*Sammlung Vandenhoeck, Göttingen 1980,
    ISBN 3-525-01324-8
  - Daniel Elfendahl: *Richtungskämpfe im parteipolitischen
    Liberalismus: Die FDP zwischen 1948 und 1972.*
    [Ruhr-Universität](../Page/Ruhr-Universität.md "wikilink"), Bochum
    2003
  - [Karl-Hermann Flach](../Page/Karl-Hermann_Flach.md "wikilink"):
    *自由主义还有一个机会. Eine Streitschrift*.美茵河畔法兰克福1971, ISBN
    3-596-22040-8
  - [Hans-Dietrich Genscher](../Page/汉斯-迪特里希·根舍.md "wikilink"): *Nur ein
    Ortswechsel? Eine Zwischenbilanz der Berliner Republik.* Hohenheim
    Verlag, 2002, ISBN 3-89850-074-8
  - Jörg Michael Gutscher: *FDP的发展自创建至1961年*Hain出版社, Meisenheim am Glan
    1967
  - Dieter Hein: *Zwischen liberaler Milieupartei und nationaler
    Sammlungsbewegung. FDP创建、发展和结构1945–1949.* Droste出版社，杜塞尔多夫1985, ISBN
    3-7700-5127-0
  - Karl Holl, Günter Trautmann, Hans Vorländer: *社会自由主义*Vandenhoeck &
    Ruprecht，哥廷根1986, ISBN 3-525-01333-7
  - Reinhart Hübsch, Jürgen Frölich: *Deutsch-deutscher Liberalismus im
    Kalten Krieg.自由主义德国政治1945–1970.*波斯坦1997
  - Heino Kaack: *Zur Geschichte und Programmatik der Freien
    Demokratischen Partei*. Verlag Anton Hain, Meisenheim am Glan 1976,
    ISBN 3-445-01380-2
  - Kurt J. Körper: *FDP. Bilanz der Jahre 1960 – 1966. Braucht
    Deutschland eine liberale Partei?* Köln 1968.
  - [Otto Graf
    Lambsdorff](../Page/Otto_Graf_Lambsdorff.md "wikilink")（Hrsg）:*Freiheit
    und soziale Verantwortung. Grundsätze liberaler Sozialpolitik.* FAZ
    Verlag, Frankfurt 2001, ISBN 3-89843-041-3
  - Dieter Langewiesche: *Liberalismus in Deutschland.* Suhrkamp, 1988,
    ISBN 3-518-11286-4
  - Marco Michel: *Die Bundestagswahlkämpfe der FDP 1949–2002.* Vs
    Verlag für Sozialwissenschaften, 2004, ISBN 3-531-14180-5
  - Andreas Morgenstern: *Die FDP in der parlamentarischen Opposition
    1966–69. Wandel zu einer „Reformpartei“.* Tectum-Verlag, 2004, ISBN
    3-8288-8670-1
  - Ralph Raico: *Die Partei der Freiheit. Studien zur Geschichte des
    deutschen Liberalismus.* Lucius & Lucius, Stuttgart 1999, ISBN
    3-8282-0042-7.
  - [John Rawls](../Page/约翰·罗尔斯.md "wikilink"): *Politischer
    Liberalismus.* Suhrkamp, 2003, ISBN 3-518-29242-0
  - [John Rawls](../Page/约翰·罗尔斯.md "wikilink"): *Die Idee des
    politischen Liberalismus.* Suhrkamp, 1994, ISBN 3-518-28723-0
  - Theo Rütten: *Der deutsche Liberalismus 1945 bis 1955. Deutschland-
    und Gesellschaftspolitik der ost- und westdeutschen Liberalen in der
    Entstehungsphase der beiden deutschen Staaten.* Nomos Verlag,
    Baden-Baden 1984
  - [Walter Scheel](../Page/瓦尔特·谢尔.md "wikilink")、[Otto Graf
    Lambsdorff](../Page/Otto_Graf_Lambsdorff.md "wikilink"): *Freiheit
    in Verantwortung, Deutscher Liberalismus seit 1945.* Bleicher 1988,
    ISBN 3-88350-047-X
  - Mathias Siekmeier: *Restauration oder Reform. Die FDP in den
    sechziger Jahren.* Janus Verlag, Köln 1998, ISBN 3-922977-51-0
  - [Guido Westerwelle](../Page/基多·威斯特威勒.md "wikilink"): *Neuland. Die
    Zukunft des deutschen Liberalismus.* ECON, München 1999, ISBN
    3-612-26658-6
  - Rüdiger Zülch: *从FDP到F.D.P. –德国政党系统第三股力量*波恩1973

## 注釋

## 外部链接

  - [官方网站](http://www.fdp.de/)
  - [Umfangreiches Verzeichnis von
    FDP-Weblinks](http://www.fdp-liberalweb.de/)
  - [自由主义的历史和FDP](http://www.fdp-bundesverband.de/grundsaetzliches/geschichte.phtml)
    *（FDP-Bundesverband）*
  - [威斯巴登Grundsätze](http://www.fdp-bundesverband.de/grundsaetzliches/wiesbaden.phtml)
  - [Historische Wahlplakate](http://www.gfid.de/fdp/)
  - [Udo
    Leuschner,FDP历史](http://www.udo-leuschner.de/liberalismus/fdp0.htm)
    *（Umfangreiche, teils kritische Darstellung）*
  - [自由政治](https://web.archive.org/web/20130926204118/http://www.politik-fuer-die-freiheit.de/)
    *（1945年后德国自由主义概览）*
  - [FDP的2005年德国联邦议会大选中文竞选纲领](http://www.fdp-bundesverband.de/files/363/FDP-Kurzwahlprogramm_chinesisch.pdf)
    *（FDP当时主要的政治、经济和社会改革纲领）*
  - [德国概况](http://www.bowang.info/)

[Category:德国政党](../Category/德国政党.md "wikilink")
[Category:自由主義政黨](../Category/自由主義政黨.md "wikilink")
[Category:保守自由主義政黨](../Category/保守自由主義政黨.md "wikilink")