**自由港**（，又稱**自由貿易園區**、**自由口岸**）是一种以經營[貿易為主的經濟特區](../Page/貿易.md "wikilink")，可自由進行貨物起卸、搬運、轉口和加工、長期儲存的[港口區域](../Page/港口.md "wikilink")。自由港內的國外貨物，可免征[關稅和不需經](../Page/關稅.md "wikilink")[海關人員檢查](../Page/海關.md "wikilink")。自由港主要從事[轉口貿易](../Page/轉口.md "wikilink")，但一些還會進行加工、旅遊和服務等業務。

[Ddm_2004_027_Kaohsiung_Harbor.jpg](https://zh.wikipedia.org/wiki/File:Ddm_2004_027_Kaohsiung_Harbor.jpg "fig:Ddm_2004_027_Kaohsiung_Harbor.jpg")自由區\]\]
最早的自由港出現於歐洲，13世纪[法蘭西王国已開闢](../Page/法蘭西王国.md "wikilink")[馬賽港為](../Page/馬賽港.md "wikilink")[自由貿易園區](../Page/自由貿易園區.md "wikilink")。1547年，[西班牙王國的](../Page/西班牙王國.md "wikilink")[錫耶納共和國正式將](../Page/錫耶納共和國.md "wikilink")[热那亚湾的](../Page/热那亚湾.md "wikilink")[里南那港定名为世界上第一个自由港](../Page/里南那港.md "wikilink")。其後，為了擴大對外的國際貿易，一些歐洲國家便陸續將一些港口城市開闢自由港。至今，因應全球的貿易活動與經濟發展，自由港的數量已上升至130多個。

自由港與[保税港區相似](../Page/保税港區.md "wikilink")，其不同在於貿易優惠措施空间範圍上的不同而已。

## 全球自由港

### [非洲](../Page/非洲.md "wikilink")

####

[蒙羅維亞自由港](../Page/蒙羅維亞自由港.md "wikilink")

####

  - [塞得港](../Page/塞得港.md "wikilink")
  - [蘇伊士運河貨櫃碼頭](../Page/蘇伊士運河貨櫃碼頭.md "wikilink")

####

  - [馬薩瓦自由港及國際機場](../Page/馬薩瓦國際機場.md "wikilink")
  - [阿薩布自由港及國際機場](../Page/阿薩布國際機場.md "wikilink")

####

  - [丹吉尔出口免稅區](../Page/丹吉尔出口免稅區.md "wikilink")

####

  - [路易斯自由港](../Page/路易港.md "wikilink")

####

  - [恩納港油氣自由貿易區](../Page/恩納港油氣自由貿易區.md "wikilink")

### [亞洲](../Page/亞洲.md "wikilink")

####

  - [麦纳麦](../Page/麦纳麦.md "wikilink")

####

  - [新加坡港](../Page/新加坡港.md "wikilink")

####

  - [仁川](../Page/仁川.md "wikilink")
  - [釜山](../Page/釜山.md "wikilink")

####

  - [基隆港自由貿易區](../Page/基隆港.md "wikilink")
  - [蘇澳港自由貿易區](../Page/蘇澳港.md "wikilink")
  - [臺北港自由貿易區](../Page/臺北港.md "wikilink")
  - [臺中港自由貿易區](../Page/臺中港.md "wikilink")
  - [安平港自由貿易區](../Page/安平港.md "wikilink")
  - [高雄港自由貿易區](../Page/高雄港.md "wikilink")
  - [桃園國際機場自由貿易區](../Page/桃園國際機場.md "wikilink")

#### [大陸](../Page/大陸.md "wikilink")

  - [上海自由贸易区](../Page/上海自由贸易区.md "wikilink")
  - [天津自由貿易區](../Page/天津自由貿易區.md "wikilink")
  - [福建自由貿易區](../Page/福建自由貿易區.md "wikilink")
  - [广东自由贸易区](../Page/广东自由贸易区.md "wikilink")

####

  - [香港](../Page/香港.md "wikilink")

####

  - [澳門](../Page/澳門.md "wikilink")

####

  - [班加羅爾](../Page/班加羅爾.md "wikilink")
  - [孟買](../Page/孟買.md "wikilink")
  - [果阿](../Page/果阿.md "wikilink")
  - [加爾各答](../Page/加爾各答.md "wikilink")
  - [喀拉拉邦](../Page/喀拉拉邦.md "wikilink")

####

  - [巴淡](../Page/巴淡.md "wikilink")

####

  - [克什姆](../Page/克什姆.md "wikilink")
  - [恰巴哈尔](../Page/恰巴哈尔.md "wikilink")
  - [基什岛](../Page/基什岛.md "wikilink")
  - [帕尔斯能源经济特区](../Page/帕尔斯能源经济特区.md "wikilink")
  - [阿尔万德自由貿易區](../Page/阿尔万德自由貿易區.md "wikilink")

####

  - [埃拉特](../Page/埃拉特.md "wikilink")

####

  - [長崎](../Page/長崎.md "wikilink")
  - [新潟](../Page/新潟.md "wikilink")

####

  - [贝鲁特港](../Page/贝鲁特港.md "wikilink")
  - [黎波里港](../Page/黎波里港.md "wikilink")

####

  - [檳城](../Page/檳城.md "wikilink") 1969前
  - [浮罗交怡](../Page/浮罗交怡.md "wikilink") 1987后
  - [纳闽岛](../Page/纳闽岛.md "wikilink") 1956后 \[1\]

####

  - [卡拉奇](../Page/卡拉奇.md "wikilink")(卡拉奇並非自由港)
  - [瓜达尔港](../Page/瓜达尔港.md "wikilink") (www.gwadarport.gov.pk)

####

  - [The Freeport Area of
    Bataan](http://en.wikipedia.org/wiki/Freeport_Area_of_Bataan)
  - [苏比克湾自由區](../Page/苏比克湾自由區.md "wikilink")
  - [克拉克自由港區](../Page/克拉克自由港區.md "wikilink")
  - [三宝颜市經濟特區](../Page/三宝颜市經濟特區.md "wikilink") \[2\]

####

  - [梅爾辛](../Page/梅爾辛.md "wikilink")
  - [伊兹密尔](../Page/伊兹密尔.md "wikilink")
  - [泰基爾達](../Page/泰基爾達.md "wikilink")

####

  - [阿布達比](../Page/阿布達比.md "wikilink")
  - [杜拜](../Page/杜拜.md "wikilink")
      - Abu Dhabi Airport Free Zone (ADAFZ)
      - ADPC - Khalifa Port and Industrial Zone (KPIZ)
      - Ahmed Bin Rashid Free Zone (ABRFZ)
      - Ajman Free Zone (AFZ)
      - [Creative City](../Page/Creative_City.md "wikilink")
      - Dubai Academic City
      - Dubai Airport Free Zone (DAFZ)
      - Dubai Biotechnology & Research Park (DuBiotech)
      - Dubai Car and Automotive City Free Zone (DUCAMZ)
      - Dubai Gold and Diamond Park
      - [Dubai Multi Commodities Centre Free
        Zone](../Page/Dubai_Multi_Commodities_Centre_Free_Zone.md "wikilink")
      - [Dubai Internet City](../Page/Dubai_Internet_City.md "wikilink")
      - [Dubai Media City](../Page/Dubai_Media_City.md "wikilink")
      - [Dubai Studio City](../Page/Dubai_Studio_City.md "wikilink")
      - Dubai Techno Park
      - Dubai Technology and Media Free Zone (TECOM)
      - [International Media Production
        Zone](../Page/International_Media_Production_Zone.md "wikilink")
      - [Dubai Knowledge
        Village](../Page/Dubai_Knowledge_Village.md "wikilink")
      - Dubai Logistics City
      - [Dubai Healthcare
        City](../Page/Dubai_Healthcare_City.md "wikilink")
      - Dubai Industrial City (DIC)
      - Dubai International Academic City
      - [迪拜国际金融中心](../Page/迪拜国际金融中心.md "wikilink") Dubai International
        Financial Centre (DIFC)
      - [DuBiotech](../Page/DuBiotech.md "wikilink")
      - Economic Zones World (EZW)
      - Fujairah Creative City
      - Fujairah Free Zone (FFZ)
      - [Dubai Outsource
        Zone](../Page/Dubai_Outsource_Zone.md "wikilink")
      - Dubai Silicon Oasis (DSO)
      - [Jebel Ali Free Zone](../Page/Jebel_Ali_Free_Zone.md "wikilink")
        (JAFZA)
      - [Hamriyah Free Zone](../Page/Hamriyah_Free_Zone.md "wikilink")
      - Higher Corporation for Specialized Economic Zones (HCSEZ) -
        ZonesCorp
      - Industrial City of Abu Dhabi
      - [JLT Free Zone](../Page/JLT_Free_Zone.md "wikilink") Dubai
      - RAK Investment Authority Free Zone (RAKIA FZ)
      - [Ras Al Khaimah Free Trade
        Zone](../Page/Ras_Al_Khaimah_Free_Trade_Zone.md "wikilink")
      - [Ras Al Khaimah IT
        Park](../Page/Ras_Al_Khaimah_IT_Park.md "wikilink")
      - [Ras Al Khaimah Media Free
        Zone](../Page/Ras_Al_Khaimah_Media_Free_Zone.md "wikilink")
      - Sharjah Airport International Free Zone (SAIF)
      - [twofour54](../Page/twofour54.md "wikilink") Abu Dhabi
      - Virtuzone Free Zone

###

####

####

  - [維也納](../Page/維也納.md "wikilink")

####

  - [Brest FEZ](../Page/Brest_FEZ.md "wikilink")
  - [Grodno FEZ](../Page/Grodno_FEZ.md "wikilink")

####

  - [扎達爾](../Page/扎達爾.md "wikilink")

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - Freeport of [Copenhagen](../Page/Copenhagen.md "wikilink")
    (*Københavns Frihavn*)

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - Free zone of [Lappeenranta](../Page/Lappeenranta.md "wikilink")
    (*Lappeenrannan Vapaa-alue*)
  - Freeport of [Hanko](../Page/Hanko.md "wikilink") (*Hangon
    Vapaasatama*)

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - Free Zone of [Le Verdon](../Page/Le_Verdon-sur-Mer.md "wikilink") -
    Port de [Bordeaux](../Page/Bordeaux.md "wikilink") (*Zone franche du
    Verdon — Port de Bordeaux*)

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - [不萊梅市自由港](../Page/不萊梅市.md "wikilink") (*Freihafen Bremen*),
    1888−2007
  - [不萊梅港自由港](../Page/不萊梅港.md "wikilink")(*Freihafen Bremerhaven*)
  - [库克斯哈文自由港](../Page/库克斯哈文.md "wikilink") (*Freihafen Cuxhaven*),
    since 1896
  - [代根多夫自由港](../Page/代根多夫.md "wikilink") (*Freihafen Deggendorf*),
    since 1989
  - [杜伊斯堡自由港](../Page/杜伊斯堡.md "wikilink") (*Freihafen Duisburg*), since
    1989
  - [漢堡自由港](../Page/漢堡.md "wikilink") (*Freihafen Hamburg*), 1888–2012

####

  - [Adjara](../Page/Adjara.md "wikilink") autonomous republic
      - [Batumi](../Page/Batumi.md "wikilink"), 1878-1886 (then
        [Russia](../Page/Russia.md "wikilink"))

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - Free zone of [Piraeus](../Page/Piraeus.md "wikilink")
  - Free zone of [Thessaloniki](../Page/Thessaloniki.md "wikilink")
  - Free zone of [Heraklion](../Page/Heraklion.md "wikilink")
  - Free zone of [Evros](../Page/Evros.md "wikilink")
    ([Debzos](https://web.archive.org/web/20110627075437/http://debzos.com/))

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - [Port of Cork](../Page/Port_of_Cork.md "wikilink") Free Port
  - [Shannon Free Zone](../Page/Shannon_Free_Zone.md "wikilink")

####

  - [Isle of Man Airport](../Page/Isle_of_Man_Airport.md "wikilink")
    (Ballasala)

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - [Livorno](../Page/Livorno.md "wikilink"), 1675–1860
  - Free Zone of [Venice](../Page/Venice.md "wikilink") (*Porto franco
    di Venezia*)
  - [Aosta Valley](../Page/Aosta_Valley.md "wikilink")
  - [Campione d'Italia](../Page/Campione_d'Italia.md "wikilink")
  - [Livigno](../Page/Livigno.md "wikilink")
  - [Port of Trieste](../Page/Port_of_Trieste.md "wikilink")

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - [Malta Freeport](../Page/Malta_Freeport.md "wikilink") \[3\]

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - Free port of [Riga](../Page/Riga.md "wikilink")
  - [Free port of
    Ventspils](../Page/Free_port_of_Ventspils.md "wikilink")

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - Free [Port of Klaipėda](../Page/Port_of_Klaipėda.md "wikilink")

####

  - Famagusta Free Zone
  - Gemikonağı Free Zone

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - Free Zone of [Madeira](../Page/Madeira.md "wikilink") - Caniçal
    (*Zona franca da Madeira - Caniçal*)

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - [Port of Constanţa](../Page/Port_of_Constanţa.md "wikilink"),
    January 2007

####

  - [Nakhodka](../Page/Nakhodka.md "wikilink")
  - [Vladivostok](../Page/Vladivostok.md "wikilink"), 1861–1909

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - Free zone of [Barcelona](../Page/Barcelona.md "wikilink") (*[Zona
    franca de
    Barcelona](../Page/Zona_franca_de_Barcelona.md "wikilink")*)
  - Free zone of [Cádiz](../Page/Cádiz.md "wikilink") (*Zona franca de
    Cádiz*)
  - Free zone of [Vigo](../Page/Vigo.md "wikilink") (*Zona franca de
    Vigo*)
  - Free zone of [Las Palmas de Gran
    Canaria](../Page/Las_Palmas_de_Gran_Canaria.md "wikilink") (*Zona
    franca de Las Palmas de Gran Canaria*)
  - ([Ceuta](../Page/Ceuta.md "wikilink") and
    [Melilla](../Page/Melilla.md "wikilink") are not Free Ports or Free
    zones because they are part of Spain, but not part of the European
    union for customs and excises)

####

([歐盟成員](../Page/歐盟.md "wikilink"))

  - [Marstrand](../Page/Marstrand.md "wikilink"), 18th century
  - [Saint-Barthélemy](../Page/Saint-Barthélemy.md "wikilink"),
    1785–1878
  - Stockholms frihamn, 1919–1995
  - Frihamnen, Göteborg, 1922-

####

  - La Praille free port, Geneva\[4\]
  - [Geneva Cointrin International
    Airport](../Page/Geneva_Cointrin_International_Airport.md "wikilink")
    free port, Geneva\[5\]
  - Magazzini Generali con Punto Franco SA, Chiasso and Stabio, free
    port and bonded whorehouse, [1](http://www.puntofranco.com)

####

  - free port and free economic zone
    [Odessa](../Page/Odessa.md "wikilink")
      - 1819-1858
      - Trade sea port of [Odessa](../Page/Odessa.md "wikilink"),
        January 1, 2000 for 25 years

####

([歐盟成員](../Page/歐盟.md "wikilink"))\[6\]

  - [Liverpool](../Page/Liverpool.md "wikilink") Free Zone
  - [Prestwick Airport](../Page/Glasgow_Prestwick_Airport.md "wikilink")
    Free Zone
  - [Southampton](../Page/Southampton.md "wikilink") Free Zone
  - [Tilbury](../Page/Tilbury.md "wikilink") Free Zone
  - Port of [Sheerness](../Page/Sheerness.md "wikilink") Free Zone

### [美洲](../Page/美洲.md "wikilink")

  - [Zona Franca](http://es.mercatrade.com/) of Latin America

#### [阿根廷](../Page/阿根廷.md "wikilink")

  - Zona Franca [La Plata](../Page/La_Plata.md "wikilink")
  - Polo Industrial Zona Franca [General
    Pico](../Page/General_Pico.md "wikilink"), La Pampa \[7\]

#### [百慕達群島](../Page/百慕達群島.md "wikilink")

  - Free port of Hamilton Harbour,
    [汉密顿自由港](../Page/Hamilton,_Bermuda.md "wikilink")

#### [巴西](../Page/巴西.md "wikilink")

  - [瑪瑙斯自由貿易區](../Page/瑪瑙斯.md "wikilink")

#### [智利](../Page/智利.md "wikilink")

  - [亚利卡](../Page/Arica,_Chile.md "wikilink")
  - [伊基克](../Page/Iquique.md "wikilink")
  - [盆他安那斯](../Page/Punta_Arenas.md "wikilink")

#### [哥倫比亞](../Page/哥倫比亞.md "wikilink")

  - [Zona Franca
    [Bogota](../Page/Bogota.md "wikilink")](https://web.archive.org/web/20120603041600/http://www.zonafrancabogota.com/)

#### [多米尼加共和國](../Page/多米尼加共和國.md "wikilink")

  - Mega Port of Punta Caucedo

#### [尼加拉瓜](../Page/尼加拉瓜.md "wikilink")

  - [Managua](../Page/Managua.md "wikilink") [Zona Franca (Free
    Port)](http://www.czf.com.ni/)

#### [巴拿馬](../Page/巴拿馬.md "wikilink")

  - [Zona Libre de Colón](http://www.zonalibredecolon.com.pa/)
  - [Zona Libre Colon](http://es.mercatrade.com/)

#### [烏拉圭](../Page/烏拉圭.md "wikilink")

  - [Carrasco International Airport (Free
    Airport)](http://www.tcu.com.uy/)
  - Zona Franca de [Montevideo](../Page/Montevideo.md "wikilink")
  - Zona Franca Colonia
  - Zona Franca Rivera

#### [美國](../Page/美國.md "wikilink")

  - [US Commerce Department (International Trade Administration): List
    of U.S. Foreign-Trade
    Zones](http://ia.ita.doc.gov/ftzpage/letters/ftzlist-map.html)
  - A [foreign trade zone](../Page/foreign_trade_zone.md "wikilink")
    (FTZ) is a high security, access restricted, Customs privileged,
    C-TPAT Supply Chain Best Practice facility where merchandise both
    Domestic and Foreign may be Admitted for purposes of storage,
    manipulation, manufacturing, destruction, exhibition, or temporary
    removal duty-free. Duty, certain user fees and taxes are only
    assessed on merchandise that is Entered into the U.S. commerce for
    consumption. Goods that are transferred out of the FTZ for
    exportation abroad are exempt from duty, taxes, and user fees. FTZs
    are considered to be outside the main U.S. [customs
    territory](../Page/customs_territory.md "wikilink").
  - [United States Virgin
    Islands](../Page/United_States_Virgin_Islands.md "wikilink")

#### [委內瑞拉](../Page/委內瑞拉.md "wikilink")

  - Free port of [Isla Margarita](../Page/Isla_Margarita.md "wikilink")
    (*Puerto Libre de Margarita*)
  - Free zone of the [Paraguaná
    Peninsula](../Page/Paraguaná_Peninsula.md "wikilink") (*Zona Franca
    de la Península de Paraguaná*)
  - Free zone of [Santa Elena de
    Uairén](../Page/Santa_Elena_de_Uairén.md "wikilink") (*Zona Franca
    de Santa Elena de Uairén*)

## 參見

  - [轉口港](../Page/轉口港.md "wikilink")
  - [自由贸易区](../Page/自由贸易区.md "wikilink")

## 參考來源

  - [临港经济与辽宁中部城市群](http://gov.finance.sina.com.cn/zsyz/2005-05-20/60310.html)

[Category:港口](../Category/港口.md "wikilink")
[Category:货运](../Category/货运.md "wikilink") [Free
ports](../Category/國際稅務.md "wikilink")

1.  <http://hids.arkib.gov.my/en/peristiwa/-/asset_publisher/WAhqbCYR9ww2/content/labuan-diisytiharkan-sebagai-pelabuhan-bebas/pop_up?_101_INSTANCE_WAhqbCYR9ww2_viewMode=print>

2.

3.  [Malta Freeport](http://www.freeport.com.mt)

4.  [Ports Francs et Entrepôts de Genève
    SA](http://www.geneva-freeports.ch/)

5.
6.  [Free Zones; HMRC
    Reference:Notice 334](http://customs.hmrc.gov.uk/channelsPortalWebApp/channelsPortalWebApp.portal?_nfpb=true&_pageLabel=pageLibrary_ShowContent&id=HMCE_CL_000241&propertyType=document#P7_42)

7.  [Sitio Oficial Zona Franca La Pampa](http://www.zflapampa.com.ar)