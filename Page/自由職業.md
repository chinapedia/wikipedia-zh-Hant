**自由職業**（，**自由工作者**或簡稱**自由業**）較早以前是指自由[作家](../Page/作家.md "wikilink")、自由[記者等](../Page/記者.md "wikilink")，沒有隸屬於任何[公司或和特定](../Page/公司.md "wikilink")[公司簽訂專屬](../Page/公司.md "wikilink")[契約的](../Page/契約.md "wikilink")[職業型態](../Page/職業.md "wikilink")。[派遣職員因為和派遣公司簽訂契約](../Page/派遣.md "wikilink")，所以和自由職業有別。而現在自由職業已較無特定領域分別。

因應世界的社會與經濟的變遷，正職工作者也在非上班時間成為了自由職業，並使用各種接案平台來增加收入，也因為每個自由工作者的專門領域可能不只一項，因而衍伸出了「斜槓經濟」一詞。

## 語源

自由职业的英語語源「**Free
lance**」要追遡到[中世紀的](../Page/中世紀.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，[國王與](../Page/國王.md "wikilink")[貴族每遇到戰爭都會和](../Page/貴族.md "wikilink")[傭兵團簽立雇用契約](../Page/傭兵.md "wikilink")，其中有不屬於傭兵團的[士兵](../Page/士兵.md "wikilink")。當時的[槍騎兵多會附帶從屬的](../Page/槍騎兵.md "wikilink")[歩兵或](../Page/歩兵.md "wikilink")[弓兵](../Page/弓兵.md "wikilink")，因此簽契約時是以[槍的隻數計算成為一個戰鬥單位](../Page/槍.md "wikilink")，「**Freelance**」從此就被使用為表示尚未和敵方勢力簽約（**FREE**）的戰鬥單位（**LANCE**）之單字。當時意指士兵的「**free
lancer**」到了[近代後](../Page/近代.md "wikilink")，轉變為表示脫離組織工作的狀態，自由契約的「free」則是指[政治](../Page/政治.md "wikilink")[立場自由](../Page/立場.md "wikilink")。「*Free
lance*」這個字第一次出現在1819年由蘇格蘭小說《[撒克遜英雄傳](../Page/撒克遜英雄傳.md "wikilink")》（*Ivanhoe*）中\[1\]。

\[2\]

## 自由契約職業的例子

  - 自由[作家](../Page/作家.md "wikilink")
  - 自由[記者](../Page/記者.md "wikilink")
  - [自由撰稿人](../Page/自由撰稿人.md "wikilink")
  - [漫畫家](../Page/漫畫家.md "wikilink")
  - [動畫師之類的動畫製作者](../Page/動畫師.md "wikilink")
  - [插畫家](../Page/插畫家.md "wikilink")
  - [-{zh-hans:程序员; zh-hant:程式設計師;}-](../Page/程序员.md "wikilink")（部份）
  - [演員](../Page/演員.md "wikilink")（部份）
  - [主持人](../Page/主持人.md "wikilink")（部份）
  - [配音員](../Page/配音員.md "wikilink")、[聲優](../Page/聲優.md "wikilink")（部份）
  - [口譯](../Page/口譯.md "wikilink")
  - [翻譯](../Page/翻譯.md "wikilink")
  - [Youtuber](../Page/Youtuber.md "wikilink")

## 相關條目

  - [契約自由原則](../Page/契約自由原則.md "wikilink")
  - [勞動者](../Page/勞動.md "wikilink")
      - [上班族](../Page/上班族.md "wikilink")
      - [企業家](../Page/企業家.md "wikilink")
      - [藍領階級](../Page/藍領階級.md "wikilink")
      - [白領階級](../Page/白領階級.md "wikilink")
      - [人力派遣](../Page/人力派遣.md "wikilink")
  - [-{zh-hans:外包;zh-hk:外判;zh-tw:外包;}-](../Page/外判.md "wikilink")
  - [居家就業](../Page/居家就業.md "wikilink")
  - [網路個體戶](../Page/網路個體戶.md "wikilink")
  - 接案平台
  - 斜槓經濟

## 註釋

## 參考

1\.
[最齊全香港攝影師](https://freehunter.hk/freelancers/%E6%94%9D%E5%BD%B1%E5%B8%AB/)

2\.
[香港影片拍攝師](https://freehunter.hk/freelancers/%E5%BD%B1%E7%89%87%E6%8B%8D%E6%94%9D%E5%B8%AB/)

3\.
[香港平面設計師](https://freehunter.hk/freelancers/%E5%B9%B3%E9%9D%A2%E8%A8%AD%E8%A8%88%E5%B8%AB/)

4\. [【搵外快必讀】2017香港10大 Freelance
工作，可以搵幾多錢？](https://www.hellotoby.com/blog/%E3%80%90%E6%90%B5%E5%A4%96%E5%BF%AB%E5%BF%85%E8%AE%80%E3%80%912017%E9%A6%99%E6%B8%AF10%E5%A4%A7-freelance-%E5%B7%A5%E4%BD%9C%EF%BC%8C%E5%8F%AF%E4%BB%A5%E6%90%B5%E5%B9%BE%E5%A4%9A%E9%8C%A2%EF%BC%9F/)

5\. [【搵外快必讀】做 Freelance
的10大貼士](http://hellotoby.com/blog/%E3%80%90%E6%90%B5%E5%A4%96%E5%BF%AB%E5%BF%85%E8%AE%80%E3%80%91%E5%81%9A-freelance-%E7%9A%8410%E5%A4%A7%E8%B2%BC%E5%A3%AB/)

6\. [Freelancer
該提服務五大靈感](https://nt150.com/5%E7%A8%AE%E7%B0%A1%E6%98%93%E6%8E%A5%E6%A1%88%E6%96%B9%E5%BC%8F/)

7\. [个人导师如何使用 aBingGo 揾外快！](https://abinggo.org/centerpage/)

[Category:職業](../Category/職業.md "wikilink")

1.  [「Freelance」一字的由來](http://wemjob.com/2014/03/03/%E4%BB%80%E9%BA%BC%E6%98%AF-freelance-freelance-%E4%B8%80%E5%AD%97%E7%9A%84%E7%94%B1%E4%BE%86/)
    。
2.
    aBingGo|date=2019-01-15|newspaper=aBingGo|accessdate=2019-01-15|language=en-US}}