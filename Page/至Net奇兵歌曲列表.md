在[至NET奇兵中](../Page/至NET奇兵.md "wikilink")，有很多首不同的歌曲，這條目是列出[至NET奇兵中的歌曲](../Page/至NET奇兵.md "wikilink")。

## 主題曲

### Un Monde Sans Danger

Un Monde Sans
Danger是[至NET奇兵的法文版主題曲](../Page/至NET奇兵.md "wikilink")，由[Franck
Keller及](../Page/Franck_Keller.md "wikilink")[Ygal
Amar作曲](../Page/Ygal_Amar.md "wikilink")，主唱為[Julien
Lamassonne](../Page/Julien_Lamassonne.md "wikilink")，而[至NET奇兵動畫的法文版中播放的片頭曲是這首主題曲的濃縮版](../Page/至NET奇兵.md "wikilink")。

### A World Without Danger

A World Without
Danger是[至NET奇兵的英文版主題曲](../Page/至NET奇兵.md "wikilink")，由[Franck
Keller及](../Page/Franck_Keller.md "wikilink")[Ygal
Amar作曲](../Page/Ygal_Amar.md "wikilink")，主唱及作詞為[Noam
Kaniel](../Page/Noam_Kaniel.md "wikilink")，而[至NET奇兵動畫的英文版中播放的片頭曲是這首主題曲的濃縮版](../Page/至NET奇兵.md "wikilink")。

## 片尾曲

### 第一輯片尾曲

[至NET奇兵動畫第一輯的片尾曲是一首大約有](../Page/至NET奇兵.md "wikilink")10秒的音樂，而這首音樂是「Un
Monde Sans Danger」及「A World Without Danger」的純音樂濃縮版。

### 第二至四輯片尾曲

[至NET奇兵動畫第二至四輯的片尾曲是一首大約有](../Page/至NET奇兵.md "wikilink")30秒的音樂，而這首音樂是「S'envoler」及「Break
Away」的純音樂濃縮版。

## 其他歌曲

### Code Lyoko Featuring Subdigitals

「Code Lyoko Featuring
Subdigitals」是[至NET奇兵的唱片集](../Page/至NET奇兵.md "wikilink")，共有12首歌曲，是[門士戈夫為](../Page/門士戈夫.md "wikilink")[Subdigitals這隊](../Page/Subdigitals.md "wikilink")[至NET奇兵動畫中的虛構樂隊請來音樂人製作的唱片集](../Page/至NET奇兵.md "wikilink")，由Debra
Reynolds和Noam Kaniel所主唱。
這唱片集先在2006年11月20日於[法國以法文推出](../Page/法國.md "wikilink"),
其後在2007年6月19日於[美國以英文推出](../Page/美國.md "wikilink")。而這唱片集內，其中一首名叫「Planet
Net」的歌曲配有一套音樂影片用來推廣這首音樂，另外[至NET奇兵的主題曲](../Page/至NET奇兵.md "wikilink")「Un
Monde Sans Danger」及「A World Without Danger」也包含在這唱片集中。而「Planet
Net」及「S'envoler/Break
Away」曾於[至NET奇兵第](../Page/至NET奇兵.md "wikilink")89集「音樂淨化野獸」中變為濃縮版作插曲使用。

「Code Lyoko Featuring Subdigitals」法英文歌名：

<table>
<tbody>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>歌曲編號</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>法文歌名</strong></p>
</div></td>
<td><div style="text-align: center;">
<p><strong>英文歌名</strong></p>
</div></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>Planet Net</p></td>
<td><p>Planet Net</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>Ouvre Les Yeux</p></td>
<td><p>Angle of Mine</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>Technoïde</p></td>
<td><p>School Is Out</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>D'ici Et Ailleurs</p></td>
<td><p>Virtual World</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>Ensemble</p></td>
<td><p>Time to Cry</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>Sauver Le Monde</p></td>
<td><p>Secret Life</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>Rodéo</p></td>
<td><p>Suring in Cyberspace</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>La Tribu</p></td>
<td><p>Mother Earth</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>S'en Aller</p></td>
<td><p>Get Away</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>Bienvenue</p></td>
<td><p>World with My Eyes</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>S'envoler</p></td>
<td><p>Break Away</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>Un Monde Sans Danger</p></td>
<td><p>A World Without Danger</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>Planet Net（音樂影片）</p></td>
<td><p>Planet Net（音樂影片）</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

-----

[Category:至Net奇兵](../Category/至Net奇兵.md "wikilink")