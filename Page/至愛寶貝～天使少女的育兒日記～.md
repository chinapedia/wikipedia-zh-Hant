《**至愛寶貝～天使少女的育兒日記～**》（）是[日本漫畫家](../Page/日本.md "wikilink")[愛本瑞穗的](../Page/愛本瑞穗.md "wikilink")[漫畫作品](../Page/漫畫.md "wikilink")。2005年開始在「BE.LOVE」（[講談社](../Page/講談社.md "wikilink")）連載，至2008年1月己有6卷單行本發售。這套漫畫以[智障人士育兒為題材](../Page/智障人士.md "wikilink")，內容主要描寫一般人對有關智障人士育兒的偏見。故事一次連載四集，每隔2至3個月刊行一次。漫畫中文版在[台灣由](../Page/台灣.md "wikilink")[東立出版社所代理](../Page/東立出版社.md "wikilink")。

自2008年1月17日開始，該故事改編成電視劇在日本[TBS系星期四晚上](../Page/TBS系.md "wikilink")10時放映，由[香里奈主演](../Page/香里奈.md "wikilink")。

## 故事簡介

主角福原柚子本身患有輕度弱智的，在工作中心與澤田草介相識，並因此懷有身孕。就在這時，草介卻遭遇車禍離世。周圍的人雖然認為智障者是沒有照顧兒子的能力，但柚子仍決心生下女兒並撫養她。從此，柚子就開始其初為人母的生活。

## 登場人物

### 柚子及其家人

  - 福原 柚子
    故事主角，患有輕度弱智，與草介生下女兒葵。及後開始其作為人母的生活。

<!-- end list -->

  - 福原 葵
    柚子與草介所生的女兒。在福原家中養育。至最新的第6卷，葵正在上小學。

<!-- end list -->

  - 福原 蓮
    柚子之弟。

## 單行本

  - 第1巻 ISBN 4-06-319166-4
  - 第2巻 ISBN 4-06-319181-8
  - 第3巻 ISBN 4-06-319199-0
  - 第4巻 ISBN 4-06-319207-5

## 電視劇

2008年1月17日開始，日本TBS電視台在逢星期四22:00～22:54中播放改編自這套漫畫的電視劇，劇名為『最喜歡你\!\!』（日文原名：），由香里奈主演，初回劇集播放時間延長10分鐘至23:04，今次是香里奈在電視劇中首次擔任女主角，由於角色關係，她的長髮也因此而剪短。

### 演員

  - 福原柚子：[香里奈](../Page/香里奈.md "wikilink")、[松元環季](../Page/松元環季.md "wikilink")（孩提時代）
  - 福原葵（）：[星野莉音](../Page/星野莉音.md "wikilink")、[松本春姫](../Page/松本春姫.md "wikilink")、[佐佐木麻緒](../Page/佐佐木麻緒.md "wikilink")
  - 福原蓮：[平岡祐太](../Page/平岡祐太.md "wikilink")
  - 澤田琴音：[福田沙紀](../Page/福田沙紀.md "wikilink")

<!-- end list -->

  -
    電視劇原創人物。草介的親妹。自幼雙親離世，在孤兒園長大。忽然離開了孤兒園，來到了福原家，成為一位協助柚子的角色．

<!-- end list -->

  - 安西真紀：[紺野真昼](../Page/紺野真昼.md "wikilink")

<!-- end list -->

  -
    工作中心蒲公英的職員。

<!-- end list -->

  - 藤川夏梅：[臼田麻美](../Page/臼田麻美.md "wikilink")
  - 岡本拓也：[蓮ハルク](../Page/蓮ハルク.md "wikilink")

<!-- end list -->

  -
    工作中心支援工作的職員。

<!-- end list -->

  - 田中桃花：[近野成美](../Page/近野成美.md "wikilink")

<!-- end list -->

  -
    [和菓子屋之主人](../Page/和菓子屋.md "wikilink")・清一郎之娘。

<!-- end list -->

  - 笹岡俊哉：[田中幸太朗](../Page/田中幸太朗.md "wikilink")（第2話\~）

<!-- end list -->

  -
    福原葵在學的幼稚園教師。

<!-- end list -->

  - 真田香織：[小川奈那](../Page/小川奈那.md "wikilink")（第2話\~）

<!-- end list -->

  -
    福原葵的在學幼稚園教師。

<!-- end list -->

  - 野村さん：[MEGUMI](../Page/MEGUMI.md "wikilink")（第3話\~）
  - 博美：[眞野裕子](../Page/眞野裕子.md "wikilink")（第2話\~）
  - 早苗：[寶積有香](../Page/寶積有香.md "wikilink")（第2話\~）
  - [土肥美緒](../Page/土肥美緒.md "wikilink")（第3話\~）

<!-- end list -->

  -
    福原葵在學的幼稚園的母親們。

<!-- end list -->

  - 戸川：[春田純一](../Page/春田純一.md "wikilink")（第3話）

<!-- end list -->

  -
    音樂公司的董事。

<!-- end list -->

  - 公司店長：[岡山肇](../Page/岡山肇.md "wikilink")（第4話）
      -
        [小野寺昭](../Page/小野寺昭.md "wikilink")
  - 田中清一郎：[相島一之](../Page/相島一之.md "wikilink")

<!-- end list -->

  -
    母・美代子在職的[和菓子屋的老闆](../Page/和菓子屋.md "wikilink")。

<!-- end list -->

  - 勝川節子：[余貴美子](../Page/余貴美子.md "wikilink")

<!-- end list -->

  -
    保健師

<!-- end list -->

  - 下柳園長：[音無美紀子](../Page/音無美紀子.md "wikilink")

<!-- end list -->

  -
    福原葵在學的幼稚園園長。

<!-- end list -->

  - 澤田草介：[中村俊介](../Page/中村俊介.md "wikilink")（特別出演）
  - 福原美代子：[岸本加世子](../Page/岸本加世子.md "wikilink")
  - 為福原葵作三個月後檢查的醫生：[津田健次郎](../Page/津田健次郎.md "wikilink")（第1話）

### 副題・收視率

<table>
<thead>
<tr class="header">
<th><p>各話</p></th>
<th><p>播放日</p></th>
<th><p>副標</p></th>
<th><p>原文副題</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1話</p></td>
<td><p>|2008年1月17日</p></td>
<td><p>我成為媽媽</p></td>
<td></td>
<td><p>10.9%</p></td>
</tr>
<tr class="even">
<td><p>第2話</p></td>
<td><p>|2008年1月24日</p></td>
<td><p>我要養育小葵!</p></td>
<td></td>
<td><p>11.5%</p></td>
</tr>
<tr class="odd">
<td><p>第3話</p></td>
<td><p>|2008年1月31日</p></td>
<td><p>我想要朋友</p></td>
<td></td>
<td><p><span style="color: red;">9.1%</span> |-align=center -&gt;</p></td>
</tr>
<tr class="even">
<td><p><strong>平均收視率</strong></p></td>
<td><p><strong>11.45%</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 主題曲

  - [melody.](../Page/melody..md "wikilink")「[遙花～はるか～](../Page/遥花〜はるか〜.md "wikilink")」

### 插曲

  - [彌生](../Page/彌生_\(歌手\).md "wikilink")「途切れない愛のメッセージ」

### 獎項

  - [第56回日劇學院賞最佳女主角](../Page/第56回日劇學院賞.md "wikilink")：香里奈

### 工作人員

  - 監修：[社会福祉法人全日本手をつなぐ育成会](../Page/全日本手をつなぐ育成会.md "wikilink")
  - 腳本：[篠崎繪里子](../Page/篠崎繪里子.md "wikilink")、[渡辺千穂](../Page/渡辺千穂.md "wikilink")
  - 導演：[竹村健太郎](../Page/竹村健太郎.md "wikilink")、[堀英樹](../Page/堀英樹.md "wikilink")、[塚原亞由子](../Page/塚原亞由子.md "wikilink")
  - プロデューサー：[山本和夫](../Page/山本和夫_\(テレビプロデューサー_1955年生\).md "wikilink")、[川西琢](../Page/川西琢.md "wikilink")
  - 音樂：[近藤由紀夫](../Page/近藤由紀夫.md "wikilink")、[小西香葉](../Page/小西香葉.md "wikilink")
  - 技術協力：[フォーチュン](../Page/フォーチュン_\(テレビ技術会社\).md "wikilink")
  - 美術協力：[アックス](../Page/アックス_\(会社\).md "wikilink")
  - 企畫協力：[ドラマデザイン社](../Page/ドラマデザイン社.md "wikilink")
  - 制作：[ドリマックス・テレビジョン](../Page/ドリマックス・テレビジョン.md "wikilink")、TBS
    *[人](../Page/ジ～ン.md "wikilink")*

## 外部連結

  - [電視劇網站](http://www.tbs.co.jp/daisuki2008/)

[Category:TBS週四晚間十點連續劇](../Category/TBS週四晚間十點連續劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:2008年日本電視劇集](../Category/2008年日本電視劇集.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:日本漫畫改編日本電視劇](../Category/日本漫畫改編日本電視劇.md "wikilink")
[Category:發展障礙題材作品](../Category/發展障礙題材作品.md "wikilink")
[Category:育兒題材作品](../Category/育兒題材作品.md "wikilink")
[Category:篠崎繪里子劇本作品](../Category/篠崎繪里子劇本作品.md "wikilink")
[Category:BE·LOVE](../Category/BE·LOVE.md "wikilink")