《**致命摇篮**》（）是一部2003年美國[動作片](../Page/動作片.md "wikilink")，由[安德瑞·巴寇亞克執導](../Page/安德瑞·巴寇亞克.md "wikilink")，[华纳兄弟公司发行](../Page/华纳兄弟.md "wikilink")。主要演员有[李連杰](../Page/李連杰.md "wikilink")、厄爾西蒙斯、[胡凯莉](../Page/胡凯莉.md "wikilink")、[馬克·德可斯可](../Page/馬克·德可斯可.md "wikilink")。

## 劇情

一個由六人組成的偷竊集團如往常一樣去偷竊東西來賣，沒想到偷到貌似黑鑽石的東西卻是由[台灣所研發出來的核子武器](../Page/台灣.md "wikilink")。這些黑鑽石當年被一名台灣安全局的情報人員林（馬克·德可斯可
飾）所偷走帶到[美國](../Page/美國.md "wikilink")。而[台灣為了要追回這一批黑鑽石](../Page/台灣.md "wikilink")，特地派了一名台灣安全局情報人員蘇黨恆（李連杰
飾）到[美國調查](../Page/美國.md "wikilink")。最後，蘇成功的追回了黑鑽石。

## 花絮

  - 為避免[敏感議題](../Page/台灣問題.md "wikilink")，本片中「蘇」使用的識別證上，使用修改過的[梅花旗作為台灣的地區識別](../Page/梅花旗.md "wikilink")

## 主要演員

  - [李連杰](../Page/李連杰.md "wikilink") 飾 蘇黨恆
  - 厄爾西蒙斯 飾 安東尼斐特
  - 蓋布里爾憂尼恩 飾 戴瑞兒
  - [胡凱莉](../Page/胡凱莉.md "wikilink") 飾 蘇娜
  - [馬克·德可斯可](../Page/馬克·德可斯可.md "wikilink") 飾 林
  - 安東尼安德森 飾 湯米
  - 湯姆阿諾 飾 阿齊

## 外部链接

  -
  -
  -
  -
  -
  -
  - {{@movies|fenC40306685}}

  -
  -
[Category:2003年電影](../Category/2003年電影.md "wikilink")
[Category:美国电影作品](../Category/美国电影作品.md "wikilink")
[Category:美國動作片](../Category/美國動作片.md "wikilink")
[Category:安德瑞·巴寇亞克電影](../Category/安德瑞·巴寇亞克電影.md "wikilink")