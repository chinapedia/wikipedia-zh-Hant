本文敘述**[省轄市](../Page/省轄市.md "wikilink")**時期的**臺中市**。臺中於[日治時期](../Page/台灣日治時期.md "wikilink")1920年實施[市制](../Page/臺灣市制.md "wikilink")，為[臺中州之](../Page/臺中州.md "wikilink")[州轄市](../Page/州轄市.md "wikilink")。1945年[中華民國接管臺灣後](../Page/臺灣戰後時期.md "wikilink")，改設置隸屬[臺灣省之](../Page/臺灣省.md "wikilink")[省轄市](../Page/市_\(中華民國\).md "wikilink")，2010年12月25日[五都升格案生效](../Page/2010年中華民國縣市改制直轄市.md "wikilink")，與[臺中縣合併改制成為](../Page/臺中縣.md "wikilink")[直轄市](../Page/直轄市_\(中華民國\).md "wikilink")。

## 歷史

臺中市別名「**文化城**」。[清朝之前](../Page/清朝.md "wikilink")，臺中市為[平埔族群之中的](../Page/平埔族.md "wikilink")[巴則海族與](../Page/巴則海族.md "wikilink")[巴布薩族的生活區域](../Page/巴布薩族.md "wikilink")，主要以狩獵維生。[清治末期](../Page/台灣清治時期.md "wikilink")，臺灣中部的要邑位在半線（今日的[彰化市](../Page/彰化市.md "wikilink")），直至1890年[臺灣巡撫](../Page/臺灣巡撫.md "wikilink")[劉銘傳在當時東大墩街南側的](../Page/劉銘傳.md "wikilink")「橋仔頭」聚落（或稱橋仔圖、橋孜圖；今[國光路到](../Page/國光路_\(台中\).md "wikilink")[臺中路之間](../Page/臺中路.md "wikilink")）附近始建[八卦形](../Page/八卦.md "wikilink")[城池及大量官署建築物](../Page/城池.md "wikilink")（現存北門城樓、考場），預備作為[臺灣建省之後的](../Page/福建臺灣省.md "wikilink")[省會](../Page/省城.md "wikilink")。但後任巡撫[邵友濂終止此計畫](../Page/邵友濂.md "wikilink")，省城改設在[臺北](../Page/臺北.md "wikilink")；但所累積的建設成果是[清治時期全臺營建之城池中規模最大者](../Page/台灣清治時期.md "wikilink")。

1896年[日治時期](../Page/台灣日治時期.md "wikilink")，[臺灣總督府將原臺灣省臺灣府改為](../Page/臺灣總督府.md "wikilink")[臺中縣](../Page/臺中縣.md "wikilink")（今之[雲林縣](../Page/雲林縣.md "wikilink")、[彰化縣](../Page/彰化縣.md "wikilink")、[南投縣](../Page/南投縣.md "wikilink")、[苗栗縣](../Page/苗栗縣.md "wikilink")、[臺中市](../Page/臺中市.md "wikilink")），「臺中」之稱始於此；1901年苗栗、雲林、彰化、南投等地拆出，設[臺中廳](../Page/臺中廳.md "wikilink")。1899年，[日本當局實施](../Page/日本.md "wikilink")[市街改正計畫](../Page/市街改正.md "wikilink")
，整治河流與設計棋盤狀道路（現今中區、東區），將臺中興建成為日治時期新興的現代化都市。1909年，彰化廳併入臺中廳。1920年，[總督府實施](../Page/總督府.md "wikilink")「五州二廳」制度，再將臺中廳與[南投廳合併為](../Page/南投廳.md "wikilink")[臺中州](../Page/臺中州.md "wikilink")，再將現今臺中市的東、南、西、北、中等五[區區域組成](../Page/市轄區.md "wikilink")[州轄市級的](../Page/州轄市.md "wikilink")[臺中市](../Page/臺中市_\(州轄市\).md "wikilink")，隸屬於臺中州，此為臺中設「市」之始。

在[二戰後臺中市由中華民國政府接收](../Page/二戰.md "wikilink")，並改為省轄市。1947年2月1日，[臺灣省行政長官公署將臺中縣](../Page/臺灣省行政長官公署.md "wikilink")[北屯](../Page/北屯區.md "wikilink")、[西屯](../Page/西屯區.md "wikilink")、[南屯等三個鄉劃入臺中市管轄](../Page/南屯區.md "wikilink")。2006年6月《[遠見雜誌](../Page/遠見雜誌.md "wikilink")》發表的「2008年縣市總體競爭力調查」，臺中市在總表現中排名全國第五\[1\]。

2003年，臺中市的總人口數突破100萬\[2\]，要求在中部唯一大都會再設一處直轄市的聲浪四起，甚至成為總統候選人的政見。但由於直轄市門檻自100萬人改為125萬人，故於2009年6月通過臺中市縣合併改制直轄市案、翌年[正式實施](../Page/2010年中華民國縣市改制直轄市.md "wikilink")，完成。

<File:Taichung> City Map by US
Army.jpg|[日治時期末期的臺中市街圖](../Page/臺灣日治時期.md "wikilink")。可見當時該市的街道，主呈棋盤狀(為今臺中市中區和其臨近區域)。
<File:Er> nd 1013
Taichung.jpg|日治時期[臺中州廳建築](../Page/臺中州廳.md "wikilink")（右圖為今貌）
[File:Taichung_City_Hall.JPG|今日的](File:Taichung_City_Hall.JPG%7C今日的)[臺中州廳為臺中市市定古蹟](../Page/臺中州廳.md "wikilink")
<File:Taisho-cho> Street, Taichu on postcard.jpg|日治時期臺中市大正町通，現今的中正路。

## 地理

[Lyu_Chuan.jpg](https://zh.wikipedia.org/wiki/File:Lyu_Chuan.jpg "fig:Lyu_Chuan.jpg")

  - 地形水文

臺中市位於臺灣本島西側的中部。地形上，臺中市中心位於台中盆地，四周環山，[大坑風景區內的](../Page/大坑風景區.md "wikilink")[頭嵙山](../Page/頭嵙山.md "wikilink")（[海拔](../Page/海拔.md "wikilink")859[公尺](../Page/公尺.md "wikilink")）為市區最高峰（[日治時期臺中市轄區最高峰為](../Page/日治時期.md "wikilink")[台中公園的砲台山](../Page/台中公園.md "wikilink")）。平日若天氣晴朗，則可自[大肚山等臺中周邊的山俯瞰整個臺中盆地](../Page/大肚山.md "wikilink")。

其中，臺中市區被多條溪流穿過，大型的溪流由東至西有[廍子溪](../Page/廍子溪.md "wikilink")、[大里溪](../Page/大里溪.md "wikilink")、[旱溪](../Page/旱溪.md "wikilink")、[筏子溪](../Page/筏子溪.md "wikilink")，小型（或為區域排水系統）則有[綠川](../Page/綠川.md "wikilink")、[柳川](../Page/柳川.md "wikilink")、[梅川](../Page/梅川.md "wikilink")、[麻園頭溪](../Page/麻園頭溪.md "wikilink")、[楓樹腳溪](../Page/楓樹腳溪.md "wikilink")、[潮洋溪](../Page/潮洋溪.md "wikilink")、[南屯溪](../Page/南屯溪.md "wikilink")（犁頭店溪）、[土庫溪](../Page/土庫溪.md "wikilink")、[大坑溪](../Page/大坑溪_\(臺中市\).md "wikilink")、橫坑溪、濁水坑溪、清水坑溪、新庄仔溪、北坑溪。其中以綠川、柳川、梅川、麻園頭溪四者被稱作「[臺中市四大河川](../Page/Template:臺中市四大河川.md "wikilink")」。[日治時期時](../Page/臺灣日治時期.md "wikilink")，[臺灣總督府仿照](../Page/臺灣總督府.md "wikilink")[京都市的都市計畫](../Page/京都市.md "wikilink")（[市區改正](../Page/市區改正.md "wikilink")）來規劃臺中，因而整頓了四區內的四條排水系統，至今仍可由柳川兩岸的柳樹一窺[鴨川的意象](../Page/鴨川_\(淀川水系\).md "wikilink")。臺中市的氣候受到這些流經的河川調節，顯得更為怡人。

在行政區上，臺中市四周皆與[臺中縣接壤](../Page/臺中縣.md "wikilink")，其東側與[臺中縣](../Page/臺中縣.md "wikilink")[太平市及](../Page/太平區_\(臺中市\).md "wikilink")[新社鄉相鄰](../Page/新社區.md "wikilink")；西緣[臺中縣](../Page/臺中縣.md "wikilink")[沙鹿鎮](../Page/沙鹿區.md "wikilink")、[龍井鄉及](../Page/龍井區.md "wikilink")[大肚鄉](../Page/大肚區.md "wikilink")；南鄰[臺中縣](../Page/臺中縣.md "wikilink")[烏日鄉及](../Page/烏日區.md "wikilink")[-{大里}-市](../Page/大里區.md "wikilink")，北邊則是[臺中縣](../Page/臺中縣.md "wikilink")[豐原市](../Page/豐原區.md "wikilink")、[潭子鄉與](../Page/潭子區.md "wikilink")[大雅鄉](../Page/大雅區.md "wikilink")。

  - 氣候

属于[副热带季风气候](../Page/副热带季风气候.md "wikilink")。

## 行政

### 歷任市長

### 行政區劃

| 行政區                                    | 面積（單位：[km<sup>2</sup>](../Page/平方公里.md "wikilink")） | [Taichung_Districts.png](https://zh.wikipedia.org/wiki/File:Taichung_Districts.png "fig:Taichung_Districts.png") |
| -------------------------------------- | --------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------- |
| [西區](../Page/西區_\(臺中市\).md "wikilink") | 5.7042                                              |                                                                                                                   |
| [北區](../Page/北區_\(臺中市\).md "wikilink") | 6.9376                                              |                                                                                                                   |
| [中區](../Page/中區_\(臺中市\).md "wikilink") | 0.8803                                              |                                                                                                                   |
| [東區](../Page/東區_\(臺中市\).md "wikilink") | 9.2855                                              |                                                                                                                   |
| [南區](../Page/南區_\(臺中市\).md "wikilink") | 6.8101                                              |                                                                                                                   |
| [南屯區](../Page/南屯區.md "wikilink")       | 31.2578                                             |                                                                                                                   |
| [西屯區](../Page/西屯區.md "wikilink")       | 39.8467                                             |                                                                                                                   |
| [北屯區](../Page/北屯區.md "wikilink")       | 62.7034                                             |                                                                                                                   |
| 總計                                     | 163.4256                                            |                                                                                                                   |

## 象徵

[Taichung_City_Government_symbol_2010.svg](https://zh.wikipedia.org/wiki/File:Taichung_City_Government_symbol_2010.svg "fig:Taichung_City_Government_symbol_2010.svg")[湖心亭為設計概念的](../Page/湖心亭.md "wikilink")[臺中市政府府徽](../Page/臺中市政府.md "wikilink")，於2007年8月1日啟用。\]\]

  - 市徽：於[日治時期](../Page/台灣日治時期.md "wikilink")1921年（大正十年）配合[臺中市役所落成](../Page/臺中市役所.md "wikilink")，經公開徵選再由[市協議會](../Page/協議會_\(台灣日治時期\).md "wikilink")（[市議會前身](../Page/臺中市議會.md "wikilink")）議員投票決定而產生的，在[臺灣總督府鐵道部臺北鐵道工務課的中村與松所設計的作品作為市章](../Page/台灣總督府鐵道部.md "wikilink")。今日[臺中公園砲台山望月亭](../Page/臺中公園.md "wikilink")、[臺中放送局的建築上亦可見到](../Page/台中放送局.md "wikilink")。
  - 市樹：[黑板樹](../Page/黑板樹.md "wikilink")，[張子源市長任內於](../Page/張子源.md "wikilink")1988年經市民票選訂為市樹後遂進行大量栽種。然而黑板樹為外來種，生長過快且枝幹鬆散。每逢颱風造成殘枝遍佈，氣根也破壞建築地基。2006年，[臺中市政府宣佈將以](../Page/臺中市政府.md "wikilink")20種[行道樹取代](../Page/行道樹.md "wikilink")，並移植既有24處街廓之黑板樹\[3\]。黑板樹將於2009年陸續移植至[南屯區](../Page/南屯區.md "wikilink")[嶺東苗圃附近](../Page/嶺東苗圃.md "wikilink")，市政府規劃的樹木銀行內\[4\]。
  - 市花：[長壽花](../Page/長壽花.md "wikilink")
  - 市鳥：[小白鷺](../Page/小白鷺.md "wikilink")

## 交通

<File:Wenxin> and
Taichungang.JPG|臺中市的高汽機車使用率，導致各重要幹道交會處車流量十分大，亦經常[壅塞](../Page/塞車.md "wikilink")。圖為[臺中港路及](../Page/臺中港路.md "wikilink")[文心路路口](../Page/文心路.md "wikilink")
<File:Construction> plan of the Taichung life circle road
system.JPG|[特三號道路是](../Page/臺中生活圈特三號道路.md "wikilink")「臺中生活道路系統建設計畫」其中一項建設
<File:Sanmin> Road in
Taichung.JPG|三民路三段，由於其行經[中友百貨](../Page/中友百貨.md "wikilink")（道路左側）及[一中商圈](../Page/一中商圈.md "wikilink")，往來人車眾多
<File:Taichung> Railway Station.JPG|臺中火車站（舊站） <File:Geya> Bus
Transportation 779-FB 20060627.jpg|[巨業交通](../Page/巨業交通.md "wikilink")68路
<File:Taichung> Bus 082-FC
20060721.jpg|停在[臺中技術學院外的](../Page/臺中技術學院.md "wikilink")[臺中客運](../Page/臺中客運.md "wikilink")

### 公路

  - 市區道路

臺中市的都市道路規劃呈蜘蛛網狀分布，是由數條放射狀的連外幹道（[台中港路](../Page/台中港路.md "wikilink")、[大雅路等](../Page/大雅路.md "wikilink")）及環繞市區的環狀幹道（[忠明路](../Page/忠明路.md "wikilink")、[文心路](../Page/文心路.md "wikilink")、[環中路](../Page/環中路.md "wikilink")）所組成的。近年來臺中市的人口激增，以及臺中市的汽機車使用率居全台之冠，使得每逢[假日或](../Page/假日.md "wikilink")[上下班尖峰時間](../Page/通勤.md "wikilink")，市區內各主要道路常有[塞車情形出現](../Page/塞車.md "wikilink")。為解決臺中市日趨嚴重的[塞車問題](../Page/塞車.md "wikilink")，臺中市政府提出多個解決方案，例如「部分路口禁止左轉」、「增加[公車路線](../Page/公車.md "wikilink")」
、 「[台中港路取消停車格](../Page/台中港路.md "wikilink")」 、
「增加路口員警數量」及「興建[捷運系統](../Page/台中捷運.md "wikilink")」等，以減少臺中市區的車流量。

  - 省、縣道

<!-- end list -->

  - ：[烏日鄉](../Page/烏日區.md "wikilink")−市內−[大雅路](../Page/大雅路.md "wikilink")

  - ：[大里市](../Page/大里區.md "wikilink")−市內−[潭子鄉](../Page/潭子區.md "wikilink")

  - ：市內−[龍井鄉](../Page/龍井區.md "wikilink")(台灣大道)

  - ：[中投公路](../Page/中投公路.md "wikilink")

  - ：[中彰快速道路](../Page/中彰快速道路.md "wikilink")

  - ：[大雅鄉](../Page/大雅區.md "wikilink")−市內−[烏日鄉](../Page/烏日區.md "wikilink")

  - ：[大雅鄉](../Page/大雅區.md "wikilink")−市內−[烏日鄉](../Page/烏日區.md "wikilink")

  - ：[新社鄉](../Page/新社區.md "wikilink")−市內−[太平市](../Page/太平區_\(臺中市\).md "wikilink")

  - ：[大肚鄉](../Page/大肚區.md "wikilink")−市內−[太平市](../Page/太平區_\(臺中市\).md "wikilink")

<!-- end list -->

  - 高速公路

<!-- end list -->

  - （中山高速公路）

      - [大雅交流道](../Page/大雅交流道.md "wikilink")（中清路交流道）
      - [台中交流道](../Page/台中交流道.md "wikilink")（中港路交流道）
      - [南屯交流道](../Page/南屯交流道.md "wikilink")（五權西路交流道）

### 大眾運輸

  - 鐵路

<!-- end list -->

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[臺中線](../Page/臺中線.md "wikilink")
      - [太原車站](../Page/太原車站.md "wikilink")
      - [臺中車站](../Page/臺中車站.md "wikilink")：特等站，臺中市主要火車站。附近有許多連絡[中台灣各地的客運路線](../Page/中台灣.md "wikilink")。1960年之前有糖業鐵路（[中南線](../Page/中南線.md "wikilink")）通往霧峰、草屯、南投。
      - [大慶車站](../Page/大慶車站.md "wikilink")（未來將與捷運綠線共站）
  - [台灣糖業鐵路](../Page/台灣糖業鐵路.md "wikilink")[中南線](../Page/中南線.md "wikilink")（廢止）

<!-- end list -->

  - 捷運

根據省政府時期的省住都處所規劃，初步建議之路網由紅、藍、綠、橘四條路線組成，路網總長約98.7公里，共設64座車站，預估2021年尖峰小時站間最高運量約為122,500人次，藍線採用高運量系統，綠線與橘線採用中運量捷運系統，紅線為[台鐵捷運化之密集區間車](../Page/台鐵捷運化.md "wikilink")，預估總建造成本為2300億元。

最後規劃各路線如下：

  - [紅線](../Page/台中捷運紅線.md "wikilink")：台鐵高架捷運化
  - [橘線](../Page/台中捷運橘線.md "wikilink")：
  - [藍線](../Page/台中捷運藍線.md "wikilink")：
  - [綠線](../Page/台中捷運綠線.md "wikilink")：分為[烏日文心北屯線及](../Page/烏日文心北屯線.md "wikilink")[彰化延伸線](../Page/彰化延伸線.md "wikilink")

<!-- end list -->

  - 公車客運

<!-- end list -->

  - 台中火車站前：此區位於[建國路與](../Page/建國路.md "wikilink")[綠川東路上](../Page/綠川東路.md "wikilink")，為大台中都會區國道客運、跨縣市公車、市公車路網匯集點
      - 客運公司：[國光客運](../Page/國光客運.md "wikilink")、[統聯客運](../Page/統聯客運.md "wikilink")、[台中客運](../Page/台中客運.md "wikilink")、[豐原客運](../Page/豐原客運.md "wikilink")、[巨業客運](../Page/巨業交通.md "wikilink")、[仁友客運](../Page/仁友客運.md "wikilink")
  - \-{干}-城轉運站：此區位於雙十路上，可轉搭國道客運或往[南投的班車](../Page/南投市.md "wikilink")
      - 客運公司：[全航客運](../Page/全航客運.md "wikilink")、[南投客運](../Page/南投客運.md "wikilink")、[彰化客運](../Page/彰化客運.md "wikilink")、[員林客運](../Page/員林客運.md "wikilink")
  - [朝馬轉運站](../Page/朝馬.md "wikilink")：此區位於[台中港路上](../Page/台中港路.md "wikilink")（台中交流道以東），可轉搭國道客運或往[沙鹿](../Page/沙鹿區.md "wikilink")、[台中港的班車](../Page/台中港.md "wikilink")
      - 客運公司：[國光客運](../Page/國光客運.md "wikilink")、[統聯客運](../Page/統聯客運.md "wikilink")、[台中客運](../Page/台中客運.md "wikilink")、[和欣客運](../Page/和欣客運.md "wikilink")、[飛狗巴士](../Page/飛狗巴士.md "wikilink")、[阿羅哈客運](../Page/阿羅哈客運.md "wikilink")
  - [中港轉運站](../Page/中港轉運站.md "wikilink")：此站位於[中港路上](../Page/中港路.md "wikilink")（台中交流道以西），主要做為[統聯客運國道與市公車的轉運](../Page/統聯客運.md "wikilink")
      - 客運公司：[統聯客運](../Page/統聯客運.md "wikilink")

## 旅遊

  - 百貨及購物中心

<!-- end list -->

  - [中友百貨](../Page/台中中友百貨.md "wikilink")
  - [廣三崇光百貨](../Page/廣三崇光百貨.md "wikilink")
  - [新光三越百貨台中店](../Page/新光三越百貨.md "wikilink")
  - [大遠百台中店](../Page/大遠百.md "wikilink")
  - [勤美誠品綠園道](../Page/勤美誠品綠園道.md "wikilink")
  - [誠品商場台中龍心商場](../Page/誠品商場.md "wikilink")（今龍心商場）
  - Mode Mall新時代購物中心（今[大魯閣新時代購物中心](../Page/大魯閣新時代購物中心.md "wikilink")）
  - [老虎城購物中心](../Page/老虎城購物中心.md "wikilink")
  - [新國自在商場](../Page/新國自在商場.md "wikilink")
  - [日曜天地精品中心](../Page/日曜天地精品中心.md "wikilink")
  - [好市多台中店](../Page/好市多.md "wikilink")

<!-- end list -->

  - 公園及風景區

<!-- end list -->

  - [台中公園](../Page/台中公園.md "wikilink")
  - [中正公園](../Page/中正公園_\(北區\).md "wikilink")
  - [民俗公園](../Page/民俗公園.md "wikilink")
  - [臺中都會公園](../Page/臺中都會公園.md "wikilink")
  - [豐樂雕塑公園](../Page/豐樂雕塑公園.md "wikilink")
  - [經國園道](../Page/經國園道.md "wikilink")
  - [文心森林公園、圓滿戶外劇場](../Page/文心森林公園.md "wikilink")
  - [大坑風景區](../Page/大坑風景區.md "wikilink")（含[大坑登山步道](../Page/大坑登山步道.md "wikilink")、中正露營區）
  - [望高寮夜景公園](../Page/望高寮_\(臺中市\).md "wikilink")
  - [敦化公園](../Page/敦化公園.md "wikilink")

<!-- end list -->

  - 藝術與文物展覽空間

<!-- end list -->

  - [國立自然科學博物館](../Page/國立自然科學博物館.md "wikilink")
  - [國立台灣美術館](../Page/國立台灣美術館.md "wikilink")
  - [國立臺中圖書館](../Page/國立臺中圖書館.md "wikilink")
  - [國立公共資訊圖書館](../Page/國立公共資訊圖書館.md "wikilink")
  - [台中文化創意園區](../Page/台中文化創意園區.md "wikilink")（舊台中酒廠）
  - 台中大都會歌劇院（今[台中國家歌劇院](../Page/台中國家歌劇院.md "wikilink")）
  - [臺中市文化局](../Page/臺中市文化局.md "wikilink")（今[臺中市大墩文化中心址](../Page/臺中市大墩文化中心.md "wikilink")）
      - [文英館](../Page/文英館.md "wikilink")
      - [台中中山堂](../Page/台中中山堂.md "wikilink")
      - [臺中市役所](../Page/臺中市役所.md "wikilink")
  - 臺中市長公館
  - [台中放送局](../Page/台中放送局.md "wikilink")
  - [台中民俗公園](../Page/台中民俗公園.md "wikilink")
  - [西大墩文化館](../Page/西大墩文化館.md "wikilink")[](https://web.archive.org/web/20071214193709/http://www.tccgc.gov.tw/08_country/c02_02.asp?id=3)
  - [萬和宮](../Page/萬和宮_\(臺中市\).md "wikilink")
  - 臺中市[頂橋仔文史館](../Page/頂橋仔文史館.md "wikilink")
  - [台中酒廠酒文物館](../Page/台中酒廠酒文物館.md "wikilink")
  - [興農牛隊史館](../Page/興農牛.md "wikilink")
  - 人類音樂館[](http://www.culture.taichung.gov.tw/FeaturesmuseumContent.aspx?id=546&y1=0&m1=0&d1=0&y2=0&m2=0&d2=0&key1=&forewordTypeID=)
  - [中國醫藥大學立夫中醫藥展示館](../Page/中國醫藥大學.md "wikilink")
  - [台灣香蕉新樂園](../Page/台灣香蕉新樂園.md "wikilink")[1](https://web.archive.org/web/20081221204938/http://www.vernaldew.com.tw/html/main.htm)

<!-- end list -->

  - 國定與市定古蹟

<!-- end list -->

  - [臺中車站](../Page/臺中車站.md "wikilink")
  - [台中州廳](../Page/台中州廳.md "wikilink")
  - [台中公園](../Page/台中公園.md "wikilink")[湖心亭](../Page/湖心亭.md "wikilink")
  - [積善樓](../Page/積善樓.md "wikilink")
  - [臺灣府](../Page/臺灣府.md "wikilink")[儒考棚](../Page/儒考棚.md "wikilink")
  - [萬春宮](../Page/萬春宮.md "wikilink")
  - [萬和宮](../Page/萬和宮_\(臺中市\).md "wikilink")
  - [台中樂成宮](../Page/台中樂成宮.md "wikilink")
  - [台中林氏宗祠](../Page/台中林氏宗祠.md "wikilink")
  - [北屯文昌廟](../Page/北屯文昌廟.md "wikilink")
  - [南屯文昌公廟](../Page/南屯文昌公廟.md "wikilink")
  - [張家祖廟](../Page/張家祖廟.md "wikilink")
  - [清武家廟垂裕堂](../Page/清武家廟垂裕堂.md "wikilink")
  - [張廖家廟](../Page/張廖家廟.md "wikilink")[承祜堂](../Page/承祜堂.md "wikilink")
  - [邱先甲墓園](../Page/邱先甲墓園.md "wikilink")
  - [廖煥文墓](../Page/廖煥文墓.md "wikilink")

<!-- end list -->

  - 商圈

<!-- end list -->

  - [逢甲商圈](../Page/逢甲商圈.md "wikilink")
  - [一中商圈](../Page/一中商圈.md "wikilink")
  - [美術園道](../Page/美術園道.md "wikilink")
  - [繼光街商圈](../Page/繼光街商圈.md "wikilink")
  - [電子街商圈](../Page/電子街商圈.md "wikilink")
  - [精誠商圈](../Page/精誠商圈.md "wikilink")
  - [大隆路商圈](../Page/大隆路商圈.md "wikilink")
  - [天津路商圈](../Page/天津路商圈.md "wikilink")
  - [自由路商圈](../Page/自由路商圈_\(臺中市\).md "wikilink")
  - [大坑商圈](../Page/大坑商圈.md "wikilink")
  - [中科商圈](../Page/中科商圈.md "wikilink")
  - [東海商圈](../Page/東海商圈.md "wikilink")
  - [黎明商圈](../Page/黎明商圈.md "wikilink")
  - [七期商圈](../Page/七期重劃區.md "wikilink")

<!-- end list -->

  -
    <span style="font-size:small;">參見：[臺中市主題商圈](../Page/臺中市主題商圈.md "wikilink")</span>

<File:Taichung> Skyline in the
evening.JPG|臺中市傍晚時分的天際線，圖中為[台中港路周圍的高樓群](../Page/台中港路.md "wikilink")
<File:Sogo> Taichung.JPG|廣三SOGO（於[台中港路](../Page/台中港路.md "wikilink")）
<File:Mayor's> House Taichung.JPG|臺中市長公館 <File:Taichung>
Park.jpg|台中公園湖心亭
[File:Taichung_Chishan_Gate_2.jpg|積善樓](File:Taichung_Chishan_Gate_2.jpg%7C積善樓)
<File:Fengchia> Bento Street.jpg|[逢甲商圈](../Page/逢甲夜市.md "wikilink")
<File:Yizhong> St.Shangquan.jpg|[一中商圈](../Page/一中商圈.md "wikilink")

## 教育

  - 學校

<!-- end list -->

  - [臺中市大專院校列表](../Page/臺中市大專院校列表.md "wikilink")
  - [臺中市高級中等學校列表](../Page/臺中市高級中等學校列表.md "wikilink")
  - [臺中市國中列表](../Page/臺中市國中列表.md "wikilink")
  - [臺中市國小列表](../Page/臺中市國小列表.md "wikilink")

<File:The> Luce Chapel
1.jpg|[東海大學](../Page/東海大學_\(台灣\).md "wikilink")[路思義教堂](../Page/路思義教堂.md "wikilink")
<File:CMU> 20061030.jpg|位於學士路上的[中國醫藥大學](../Page/中國醫藥大學.md "wikilink")
<File:慎思樓.jpg>|[國立臺中第一高級中學](../Page/國立臺中第一高級中學.md "wikilink")
<File:TCGS.jpg>|[國立臺中女子高級中學](../Page/國立臺中女子高級中學.md "wikilink")
<File:新民高中三民路大門.JPG>|[新民高級中學](../Page/臺中市私立新民高級中學.md "wikilink")

  - 體育場地

<!-- end list -->

  - [台中體育場](../Page/台中體育場.md "wikilink")
  - [國立台中棒球場](../Page/台中棒球場.md "wikilink")
  - [台中洲際棒球場](../Page/台中洲際棒球場.md "wikilink")
  - 中興網球場
  - [朝馬足球場](../Page/朝馬足球場.md "wikilink")
  - 萬壽棒球場
  - 櫻花棒球場
  - 中科棒球場
  - 寶山棒球場

## 出身市民

  - 榮譽市民：[王貞治](../Page/王貞治.md "wikilink")、[林之助](../Page/林之助.md "wikilink")、[梅可望](../Page/梅可望.md "wikilink")、[陳憲仁](../Page/陳憲仁.md "wikilink")、[李揚清](../Page/李揚清.md "wikilink")、[賴志勇](../Page/賴志勇.md "wikilink")、[廖瑞民](../Page/廖瑞民.md "wikilink")
  - 出身名人

<!-- end list -->

1.  政治人物：[胡志強](../Page/胡志強.md "wikilink")、[林柏榕](../Page/林柏榕.md "wikilink")、[張溫鷹](../Page/張溫鷹.md "wikilink")、[許世楷](../Page/許世楷.md "wikilink")、[賴幸媛](../Page/賴幸媛.md "wikilink")、[沈智慧](../Page/沈智慧.md "wikilink")
2.  藝術家：[RIVER](../Page/RIVER.md "wikilink")
3.  影視娛樂：[胡因夢](../Page/胡因夢.md "wikilink")、[鄭元暢](../Page/鄭元暢.md "wikilink")、[安心亞](../Page/安心亞.md "wikilink")、[阮經天](../Page/阮經天.md "wikilink")、[徐佳瑩](../Page/徐佳瑩.md "wikilink")、[韋禮安](../Page/韋禮安.md "wikilink")、[吳宇舒](../Page/吳宇舒.md "wikilink")、[簡廷芮](../Page/簡廷芮.md "wikilink")
4.  運動員：[趙士強](../Page/趙士強.md "wikilink")（棒球）、[林威助](../Page/林威助.md "wikilink")（棒球）、[鄧蒔陽](../Page/鄧蒔陽.md "wikilink")（棒球）

## 對外交流

  - 領事機構

臺中市對外國的城市交流始於1960年代，1975年[菲律賓在臺中市設立領事館](../Page/菲律賓.md "wikilink")（代表機構/辦事處），這是目前唯一的一個國家在臺中市建立的交流平臺。另外[美國在台協會亦設有](../Page/美國在台協會.md "wikilink")[臺中線上分處](https://web.archive.org/web/20110512225640/http://taichung.ait.org.tw/)，並與國立臺中圖書館合作設立[台中美國資料中心](https://web.archive.org/web/20100714002308/http://taichung.americancorner.org.tw/)。

  - ／[馬尼拉經濟文化辦事處-台中辦公室](http://www.meco.org.tw/)

<!-- end list -->

  - 科技貿易交流城市

<!-- end list -->

  - [美洲](../Page/美洲.md "wikilink")：／[卑詩省](../Page/卑詩省.md "wikilink")／[本拿比](../Page/本拿比.md "wikilink")

<!-- end list -->

  - 姊妹市

1965年至2010年底，臺中市共締結了18個姊妹市。

**[美洲](../Page/美洲.md "wikilink")**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p>／<a href="../Page/康乃狄克州.md" title="wikilink">康乃狄克州</a>／<a href="../Page/紐哈芬.md" title="wikilink">紐哈芬</a></p></li>
<li><p>／<a href="../Page/亞利桑那州.md" title="wikilink">亞利桑那州</a>／<a href="../Page/土桑市.md" title="wikilink">土桑市</a></p></li>
<li><p>／<a href="../Page/路易斯安那州.md" title="wikilink">路易斯安那州</a>／<a href="../Page/巴頓魯治市.md" title="wikilink">巴頓魯治市</a></p></li>
<li><p>／<a href="../Page/懷俄明州.md" title="wikilink">懷俄明州</a>／<a href="../Page/夏延_(懷俄明州).md" title="wikilink">夏陽</a></p></li>
<li><p>／<a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a>／<a href="../Page/聖地牙哥_(加利福尼亞州).md" title="wikilink">聖地牙哥</a></p></li>
</ul></td>
<td><ul>
<li><p>／<a href="../Page/華盛頓州.md" title="wikilink">華盛頓州</a>／<a href="../Page/塔科馬_(華盛頓州).md" title="wikilink">塔可瑪市</a></p></li>
<li><p>／<a href="../Page/內華達州.md" title="wikilink">內華達州</a>／<a href="../Page/雷諾_(內華達州).md" title="wikilink">雷諾市</a></p></li>
<li><p>／<a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a>／<a href="../Page/奧斯汀.md" title="wikilink">奧斯汀</a></p></li>
<li><p>／<a href="../Page/新罕布夏州.md" title="wikilink">新罕布夏州</a>／<a href="../Page/曼徹斯特_(新罕布希爾州).md" title="wikilink">曼徹斯特</a></p></li>
<li><p>／<a href="../Page/喬治亞州.md" title="wikilink">喬治亞州</a>／<a href="../Page/哥倫布_(喬治亞州).md" title="wikilink">哥倫布</a></p></li>
</ul></td>
<td><ul>
<li><p>／<a href="../Page/緬尼托巴省.md" title="wikilink">緬尼托巴省</a>／<a href="../Page/溫尼伯.md" title="wikilink">溫尼伯</a>（1982）</p></li>
<li><p>／<a href="../Page/汕埠.md" title="wikilink">汕埠</a></p></li>
<li><p>／<a href="../Page/聖克魯斯_(玻利維亞).md" title="wikilink">聖克魯斯</a></p></li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

**[亞洲](../Page/亞洲.md "wikilink")、[非洲與](../Page/非洲.md "wikilink")[大洋洲](../Page/大洋洲.md "wikilink")**

  - ／[忠清北道](../Page/忠清北道.md "wikilink")／[忠州市](../Page/忠州市.md "wikilink")

  - ／[馬尼拉大都會](../Page/馬尼拉大都會.md "wikilink")／[马卡蒂](../Page/马卡蒂.md "wikilink")

  - ／[那他省](../Page/誇祖魯-納塔爾省.md "wikilink")／[彼得馬里茨堡](../Page/彼得馬里茨堡.md "wikilink")

  - ／[北岸市](../Page/北岸市.md "wikilink")

  - ／[瓜佳蓮市](../Page/瓜加林環礁.md "wikilink")

## 統計

| 排名 | 本籍       | 臺中市人口   | 比率     |
| -- | -------- | ------- | ------ |
| 1  | 臺中市      | 295,607 | 38.3%  |
| 2  | 中國大陸各省市籍 | 114,504 | 14.8%  |
| 3  | 彰化縣      | 82,703  | 10.7%  |
| 4  | 臺中縣      | 81,726  | 10.6%  |
| 5  | 南投縣      | 43,918  | 5.7%   |
| 6  | 雲林縣      | 29,511  | 3.8%   |
| 7  | 嘉義縣      | 18,632  | 2.4%   |
| 8  | 臺南縣      | 16,191  | 2.1%   |
| 9  | 苗栗縣      | 15,010  | 1.9%   |
| 10 | 屏東縣      | 10,374  | 1.3%   |
| 11 | 臺北市      | 7,598   | 1.0%   |
| 12 | 臺北縣      | 7,228   | 0.9%   |
| 13 | 高雄縣      | 7,037   | 0.9%   |
| 14 | 桃園縣      | 5,159   | 0.7%   |
| 15 | 高雄市      | 4,845   | 0.6%   |
| 16 | 臺南市      | 4,516   | 0.6%   |
| 17 | 新竹縣      | 4,399   | 0.6%   |
| 18 | 嘉義市      | 4,061   | 0.5%   |
| 19 | 宜蘭縣      | 3,829   | 0.5%   |
| 20 | 臺東縣      | 3,574   | 0.5%   |
| 21 | 花蓮縣      | 3,108   | 0.4%   |
| 22 | 外國籍      | 2,641   | 0.3%   |
| 23 | 澎湖縣      | 2,023   | 0.3%   |
| 24 | 新竹市      | 1,935   | 0.3%   |
| 25 | 基隆市      | 1,743   | 0.2%   |
| 26 | 金門縣      | 835     | 0.1%   |
| 27 | 連江縣      | 121     | 0.0%   |
|    | 總計       | 772,828 | 100.0% |
|    |          |         |        |

本籍別臺中市人口 (1990年)\[5\]

## 參考資料

<references/>

## 外部連結

  - [臺中市政府](http://www.taichung.gov.tw)

[Category:台灣省轄市](../Category/台灣省轄市.md "wikilink")
[臺中市行政區劃](../Category/臺中市行政區劃.md "wikilink")
[臺](../Category/台中市歷史.md "wikilink")
[Category:1947年建立的行政區劃](../Category/1947年建立的行政區劃.md "wikilink")
[Category:2010年廢除的行政區劃](../Category/2010年廢除的行政區劃.md "wikilink")

1.  《遠見雜誌》第240期（2006年6月號）「2008年縣市總體競爭力調查」
2.  [臺中市歷年人口統計表(一) - 臺中市政府民政局](http://demographics.taichung.gov.tw/)
3.  [市府選出20種行道樹替代黑板樹](http://www.tccg.gov.tw/sys/msg_control?mode=viewnews&ts=44c077ee:31a5&theme=&layout=)
    ，臺中市政府
4.  [臺中市政府新聞處，珍惜樹木資源　臺中市樹木銀行即將動工，市政新聞，2009-02-25](http://old.taichung.gov.tw/internet/main/docDetail.aspx?uid=3152&docid=18129)
5.