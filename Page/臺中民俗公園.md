[民俗公園民藝館.jpg](https://zh.wikipedia.org/wiki/File:民俗公園民藝館.jpg "fig:民俗公園民藝館.jpg")
[民俗公園民俗館.jpg](https://zh.wikipedia.org/wiki/File:民俗公園民俗館.jpg "fig:民俗公園民俗館.jpg")
**臺中民俗公園**位於[臺灣](../Page/臺灣.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[北屯區](../Page/北屯區.md "wikilink")，園內充滿各種[閩南傳統式建築以及中國式](../Page/閩南.md "wikilink")[庭園](../Page/庭園.md "wikilink")，包括[四合院](../Page/四合院.md "wikilink")、傳統建築風格宅第與格局等，有別於其他一般公園，是一座具有教育意涵之傳統主題民俗公園。

## 歷史

  - 1981年，時任[台中市市長](../Page/台中市市長.md "wikilink")[林柏榕](../Page/林柏榕.md "wikilink")、地方[仕紳與文化人士推動籌備台灣民俗文化類型的公園](../Page/仕紳.md "wikilink")，獲得[文英基金會董事長](../Page/文英基金會.md "wikilink")[何永捐助經費以及臺中](../Page/何永.md "wikilink")[扶輪社捐獻大量文物](../Page/扶輪社.md "wikilink")。
  - 1984年，選定剛重劃完成的中正、東山重劃區內的1.6[公頃土地興建](../Page/公頃.md "wikilink")，命名為「**民俗公園**」。
  - 1990年，[元宵節正式開館](../Page/台灣元宵節.md "wikilink")。
  - 2003年，[中台科技大學接受](../Page/中台科技大學.md "wikilink")[台中市政府的委託管理](../Page/台中市政府.md "wikilink")。
  - 2008年，改由[亞洲大學接受](../Page/亞洲大學_\(台灣\).md "wikilink")[台中市文化局委託管理公園內的](../Page/台中市文化局.md "wikilink")「台灣民俗文物館」。
  - 2011年，興建地下停車場。
  - 2012年，改由迴廊創新產業育成有限公司接受[台中市文化局委託管理公園內的](../Page/台中市文化局.md "wikilink")「台灣民俗文物館」。
  - 2012年底，民俗公園地下停車場完工，並拆除圍牆改為開放式公園。
  - 2016年2月，[臺中市政府文化局與迴廊創新產業育成有限公司委託營運契約期滿](../Page/臺中市政府文化局.md "wikilink")，由[臺中市政府文化局於](../Page/臺中市政府文化局.md "wikilink")3月接手，續辦理新委外營運案；並於委外營運前辦理民俗文物館整修事宜。
  - 2016年11月，再由中臺科技大學接手經營管理。

\[1\]

<File:Taichung> Folklore Park.JPG|非開放時期的民俗公園入口處 <File:Taichung> Folklore
Museum.JPG|[亞洲大學管理時期的台灣民俗文物館](../Page/亞洲大學_\(台灣\).md "wikilink")

## 空間與建築

  - 兒童藝術館
  - 廣場
  - 中式庭園
  - 地下停車場：汽車273個停車位，45個機車停車位

## 交通

  - [臺中市公車](../Page/臺中市公車.md "wikilink")

<!-- end list -->

  - 民俗公園同熱河公園位在崇德路、熱河路、大連路、旅順路之間

<!-- end list -->

  - ;民俗公園（崇德路）

<!-- end list -->

  - [台中客運](../Page/台中客運.md "wikilink")：[71](../Page/台中市公車71路.md "wikilink")、[72](../Page/台中市公車72路.md "wikilink")、[131](../Page/台中市公車131路.md "wikilink")、[132](../Page/台中市公車132路.md "wikilink")
  - [全航客運](../Page/全航客運.md "wikilink")：[58](../Page/台中市公車58路.md "wikilink")、[65](../Page/台中市公車65路.md "wikilink")
  - [仁友客運](../Page/仁友客運.md "wikilink")：[105](../Page/台中市公車105路.md "wikilink")
  - 聯營：[12](../Page/台中市公車12路.md "wikilink")

<!-- end list -->

  - ;民俗公園（昌平路）

<!-- end list -->

  - [台中客運](../Page/台中客運.md "wikilink")：[14](../Page/台中市公車14路.md "wikilink")
  - [豐榮客運](../Page/豐榮客運.md "wikilink"):
    [127](../Page/台中市公車127路.md "wikilink")
  - [豐原客運](../Page/豐原客運.md "wikilink")：[200](../Page/台中市公車200路.md "wikilink")

<!-- end list -->

  - ;文心熱河路口或文心崇德路口

<!-- end list -->

  - [統聯客運](../Page/統聯客運.md "wikilink")：[53](../Page/台中市公車53路.md "wikilink")、[77](../Page/台中市公車77路.md "wikilink")、[85](../Page/台中市公車85路.md "wikilink")
  - [巨業交通](../Page/巨業交通.md "wikilink")：[68](../Page/台中市公車68路.md "wikilink")

## 參考

## 連結

  - [臺灣民俗文物館](http://taiwanfolkmuseum.blogspot.tw/)

[M](../Category/台灣傳統.md "wikilink")
[M](../Category/台中市博物館.md "wikilink")
[M](../Category/台中市公園.md "wikilink")
[Category:文物博物館](../Category/文物博物館.md "wikilink")
[Category:北屯區](../Category/北屯區.md "wikilink")
[Category:台灣中式園林](../Category/台灣中式園林.md "wikilink")
[Category:1990年完工建築物](../Category/1990年完工建築物.md "wikilink")
[Category:民俗博物馆](../Category/民俗博物馆.md "wikilink")

1.