[Pacific_Sogo_Taipei_Zhongxiao_Store_20091124_night.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Sogo_Taipei_Zhongxiao_Store_20091124_night.jpg "fig:Pacific_Sogo_Taipei_Zhongxiao_Store_20091124_night.jpg")忠孝館\]\]
**臺北東區**是指一個位於[中華民國](../Page/中華民國.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[大安區北側的商圈](../Page/大安區_\(臺北市\).md "wikilink")，泛指在[忠孝東路四段附近一帶以](../Page/忠孝東路.md "wikilink")[遠東SOGO百貨](../Page/遠東SOGO百貨.md "wikilink")、[微風廣場](../Page/微風廣場.md "wikilink")、[頂好名店城](../Page/頂好名店城.md "wikilink")、[明曜百貨等](../Page/明曜百貨.md "wikilink")[商場為中心的](../Page/商場.md "wikilink")[商圈](../Page/商圈.md "wikilink")，又稱**臺北市東區商圈**、**臺北東區商圈**，範圍則在[復興南路以東](../Page/復興南路.md "wikilink")、[光復南路以西](../Page/光復南路.md "wikilink")、[市民大道以南](../Page/市民大道.md "wikilink")、[仁愛路以北之間](../Page/仁愛路_\(臺北市\).md "wikilink")，是[大臺北地區最繁華](../Page/大臺北地區.md "wikilink")，最昂貴的[街區之一](../Page/街區.md "wikilink")，其中最知名的街區為「東區161巷」（敦化南路一段161巷）和「茶街」（忠孝東路四段181巷），是明星藝人開店的一級戰區，也是文創公司的聚集地。

東區商圈由於擁有眾多的[辦公大樓](../Page/辦公大樓.md "wikilink")、[百貨公司](../Page/百貨公司.md "wikilink")、[夜店及眾多](../Page/夜店.md "wikilink")[商家](../Page/商家.md "wikilink")，因此成為了大臺北地區重要的商業與消費中心。然而在政府政策使然，加上東區響亮的名號，使得在[房地產廣告中](../Page/房地產.md "wikilink")，東區商圈的周邊也被冠上「臺北東區」的名號。但此名稱會造成不少外地民眾在區域上的混淆與困擾。

## 東區與西區

[Zhongxiao_East_Road_Sec.4_near_of_Zhongxiao_Dunhua_Station_20090129_night.jpg](https://zh.wikipedia.org/wiki/File:Zhongxiao_East_Road_Sec.4_near_of_Zhongxiao_Dunhua_Station_20090129_night.jpg "fig:Zhongxiao_East_Road_Sec.4_near_of_Zhongxiao_Dunhua_Station_20090129_night.jpg")帶動了臺北東區發展，可說是東區最重要的幹道。\]\]
東區並非一開始就是[臺北市的鬧區](../Page/臺北市.md "wikilink")，在[臺灣清治時期及](../Page/臺灣清治時期.md "wikilink")[臺灣日治時期時](../Page/臺灣日治時期.md "wikilink")，此處都還是一片[稻田](../Page/稻田.md "wikilink")，甚至荒野。但到了1960年代之後至今，隨著[忠孝東路及](../Page/忠孝東路.md "wikilink")[臺北捷運的陸續興闢](../Page/臺北捷運.md "wikilink")，東區才有如麻雀變鳳凰般地，一躍成為[臺北市商業新興重鎮之一](../Page/臺北市.md "wikilink")，與早期的[臺北站前商圈](../Page/臺北站前商圈.md "wikilink")、[西門町等商圈並駕齊驅](../Page/西門町.md "wikilink")。此外，對於喜愛在臺北東區逛街的人，社會還曾給這個消費族群起了個名號「東區新人類」。

相對於臺北東區的商業及貿易機能，臺北西區作為許多中央政府機關的集中地，以政治機能為主，特別是在舊[臺北府城](../Page/臺北府城.md "wikilink")、環繞在[總統府附近一帶的區域](../Page/總統府.md "wikilink")，則統稱為[博愛特區](../Page/博愛特區.md "wikilink")（首都核心區）。而東西區的比較，仍以[市民大道至](../Page/市民大道.md "wikilink")[仁愛路為比較範圍](../Page/仁愛路.md "wikilink")。

## 發展歷程

就臺北的發展史來看，東區逐漸成為[臺北市的商業中心](../Page/臺北市.md "wikilink")，不過是1980年代中葉以後的事情。[臺灣清治時期對臺北的開發](../Page/臺灣清治時期.md "wikilink")，僅侷限於[臺北府城內](../Page/臺北府城.md "wikilink")（即今[中華路](../Page/中華路.md "wikilink")、[中山南路](../Page/中山南路_\(臺北市\).md "wikilink")、愛國西路及[忠孝西路中間的區塊](../Page/忠孝西路.md "wikilink")）而已。日本領臺時，市區的開發雖然向東擴張，但是大致上止於[堀川](../Page/堀川.md "wikilink")（今新生南北路）。而今天東區一帶，在當時不過是一大片的未開發農地。

不過1945年[日本投降之後](../Page/日本投降.md "wikilink")，[中華民國國民政府接收](../Page/中華民國國民政府.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，開始有了轉變。1949年底[第二次國共內戰結束後](../Page/第二次國共內戰.md "wikilink")，大約200餘萬的[中國大陸軍民跟隨](../Page/中國大陸.md "wikilink")[中華民國政府自](../Page/中華民國政府.md "wikilink")[中國大陸遷來](../Page/中國大陸.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，大多數的[外省移民在臺北的中央政府上班](../Page/台灣外省人.md "wikilink")，而[總督府留下的](../Page/總督府.md "wikilink")[宿舍有限](../Page/宿舍.md "wikilink")；此後又由於主要的大專院校皆設於[臺北市或近郊](../Page/臺北市.md "wikilink")，造成眾多的[中臺灣](../Page/中臺灣.md "wikilink")、[南臺灣民眾開始北上求學](../Page/南臺灣.md "wikilink")、工作，使[臺北市的人口快速增加](../Page/臺北市.md "wikilink")，西區因此開始出現飽和或過度擁擠的現象，加上[淡水河阻擋了](../Page/淡水河.md "wikilink")[臺北市向西的發展](../Page/臺北市.md "wikilink")，因此大批的新舊市民開始遷往當時甚少開發的東區。

西元1967年[臺北市人口突破](../Page/臺北市.md "wikilink")100萬人，升格為[直轄市](../Page/直轄市.md "wikilink")（[院轄市](../Page/院轄市.md "wikilink")）。當時為配合市政的大幅建設，[臺北市政府遂開始進行第一期四年公務建設計劃](../Page/臺北市政府.md "wikilink")，開闢通往郊區的基礎交通幹道。[忠孝東路即為當時往東區修築的幹道](../Page/忠孝東路.md "wikilink")，後來成為省道[臺5線北基公路的重要路段](../Page/臺5線.md "wikilink")。一直到1970年代中期以前，[忠孝東路四段沿線還是一片綠油油的稻田景觀](../Page/忠孝東路.md "wikilink")，仍然是[臺北市的郊區](../Page/臺北市.md "wikilink")。在1970年代以後[忠孝東路三](../Page/忠孝東路.md "wikilink")、四段陸續通車後，帶動了整個東區的發展，沿線許多商圈興起，使得東區在稍後初步取代了當時相對擁擠的西區，而成為臺北新興商業中心。後隨著附近[信義計畫區的開發及捷運](../Page/信義計畫區.md "wikilink")[板南線的完工通車](../Page/板南線.md "wikilink")，更加速了東區的蓬勃發展。

## 重要景點

[華新大樓_20160326.jpg](https://zh.wikipedia.org/wiki/File:華新大樓_20160326.jpg "fig:華新大樓_20160326.jpg")
[統領商業大樓A棟_20160326.jpg](https://zh.wikipedia.org/wiki/File:統領商業大樓A棟_20160326.jpg "fig:統領商業大樓A棟_20160326.jpg")
[Bistro_98_20061217_night.jpg](https://zh.wikipedia.org/wiki/File:Bistro_98_20061217_night.jpg "fig:Bistro_98_20061217_night.jpg")

  -
    <small>*備註：下面列出的景點以東區商圈為定義範圍*</small>

<!-- end list -->

  - [遠東SOGO](../Page/遠東SOGO.md "wikilink")

<!-- end list -->

  -
    <small>於東區共有三處據點，分別為忠孝館、敦化館、復興館（2007年[春節前後開幕](../Page/春節.md "wikilink")，與[臺北捷運](../Page/臺北捷運.md "wikilink")[忠孝復興站共構](../Page/忠孝復興站.md "wikilink")）</small>

<!-- end list -->

  - [明曜百貨](../Page/明曜百貨.md "wikilink")
  - [統領廣場](../Page/統領廣場.md "wikilink")
  - [微風廣場](../Page/微風廣場.md "wikilink")

<!-- end list -->

  -
    <small>在東區除了位於[市民大道](../Page/市民大道.md "wikilink")[復興南路口的微風廣場本館](../Page/復興南路.md "wikilink")，原本還有位於[忠孝東路四段上的微風忠孝館](../Page/忠孝東路.md "wikilink")（於2018年6月30日結束營業）</small>

<!-- end list -->

  - [頂好名店城](../Page/頂好名店城.md "wikilink")

<!-- end list -->

  - 東區161巷

<!-- end list -->

  -
    <small>位於敦化南路一段161巷，是知名文創公司，流行服飾品牌，和秘境咖啡店[黑山咖啡的發源地](../Page/黑山咖啡.md "wikilink")</small>

<!-- end list -->

  - 東區茶街

<!-- end list -->

  -
    <small>位於忠孝東路四段181巷7弄，聚集了數家泡沫紅茶店(兼營簡餐)</small>

<!-- end list -->

  - [東區地下街](../Page/東區地下街.md "wikilink")

<!-- end list -->

  - 誠品敦南店

<!-- end list -->

  -
    <small>是[誠品書店的第一家店](../Page/誠品書店.md "wikilink")，也是臺灣第一間[24小時營業的](../Page/24小時營業.md "wikilink")[書店](../Page/書店.md "wikilink")；被許多人認為是臺北的人文地標</small>

<!-- end list -->

  - [Bistro 98](../Page/Bistro_98.md "wikilink")

<!-- end list -->

  -
    <small>為全部為餐廳的大樓</small>

<!-- end list -->

  - 216巷

<!-- end list -->

  -
    <small>全稱為[忠孝東路四段](../Page/忠孝東路.md "wikilink")216巷，和忠孝東路四段223巷直通。[鼎泰豐忠孝店以及紅屋牛排都在此巷內](../Page/鼎泰豐.md "wikilink")。此巷延伸出去的弄，還有許多形形色色的餐廳。</small>

<!-- end list -->

  - [仁愛敦南圓環](../Page/仁愛敦南圓環.md "wikilink")

<!-- end list -->

  -
    <small>位於[仁愛路及](../Page/仁愛路_\(臺北市\).md "wikilink")[敦化南路交叉口](../Page/敦化南路.md "wikilink")；為世界第二大的[圓環](../Page/環形交叉.md "wikilink")，也常是[元宵節時](../Page/臺灣元宵節.md "wikilink")[臺北市街道燈海佈置的中心點](../Page/臺北市.md "wikilink")，附近也有許多的精品店。</small>

<!-- end list -->

  - 阿波羅大廈

<!-- end list -->

  -
    <small>全盛時期有三十二家畫廊，至今仍有十家的規模</small>

<!-- end list -->

  - [松山文創園區](../Page/松山文創園區.md "wikilink")

<!-- end list -->

  -
    <small>本為日治時期的煙草工廠，自1998年關廠後，於2010年轉型成文創園區，園區內經常性地舉辦展演活動，亦有文創商品常態性的販售</small>

此外，主幹道以外巷弄中的住宅大樓，也常常將一樓出租給商家開設店鋪，形成很有特色的住商混合。由於租金較高，一個店面經常分割成更多的小店鋪，形成小商場這種特殊景觀。

## 常見誤解

本條目的**臺北東區**係指一個特定商圈區域之專有名詞，但其他位於臺北市東側的新興商圈或區域，有時會與東區形成混淆。

### 信義計畫區

位於臺北市東側的[信義區](../Page/信義區_\(臺北市\).md "wikilink")，與位於[大安區的](../Page/大安區_\(臺北市\).md "wikilink")**臺北東區**為互不隸屬的兩個區域。

### 東區門戶計畫區

位於臺北市東側的[南港區](../Page/南港區.md "wikilink")，與位於[大安區的](../Page/大安區_\(臺北市\).md "wikilink")**臺北東區**為互不隸屬的兩個區域。

## 影響

2012年，台灣樂團[八三夭第三張專輯](../Page/八三夭.md "wikilink")《[最後的8/31](../Page/最後的8/31.md "wikilink")》中的歌曲〈東區東區〉以東區的歌曲。

[Category:台北市經濟](../Category/台北市經濟.md "wikilink")
[Category:台北市境內地區](../Category/台北市境內地區.md "wikilink")
[Category:台北市旅遊景點](../Category/台北市旅遊景點.md "wikilink")