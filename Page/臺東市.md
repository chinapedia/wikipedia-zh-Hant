**臺東市**（[阿美語](../Page/阿美語.md "wikilink")：Pusung，[卑南語](../Page/卑南語.md "wikilink")：Pusung，[布農語](../Page/布農語.md "wikilink")：Pusung）位於[臺灣東南隅](../Page/臺灣.md "wikilink")，是[臺東縣的縣治](../Page/臺東縣.md "wikilink")，東經121.10度，北緯22.45度，西起[中央山脈山麓等高線與](../Page/中央山脈.md "wikilink")[卑南鄉為鄰](../Page/卑南鄉.md "wikilink")，西南鄰知本溪與[太麻里鄉相望](../Page/太麻里鄉.md "wikilink")，北鄰[海岸山脈以黑髮溪與卑南鄉為界](../Page/海岸山脈.md "wikilink")，東邊為[太平洋](../Page/太平洋.md "wikilink")，是一座背山面海地形狹長的城市。

面積109.7691平方公里，是臺灣面積最大的縣轄市，也是東部人口最多的城市，南北長19.8公里，主要溪流由北到南有－卑南溪、太平溪、利嘉溪、知本溪，依次流入太平洋，而形成沖積扇平原的臺東市地形。全縣近一半的人口設籍於本市。

臺東市區最高點為標高75公尺的[鯉魚山](../Page/鯉魚山_\(臺東縣\).md "wikilink")，可以俯瞰全臺東市景。

## 歷史

[Map_of_Taitō-gai.jpg](https://zh.wikipedia.org/wiki/File:Map_of_Taitō-gai.jpg "fig:Map_of_Taitō-gai.jpg")
[Taito_1_10000_1945.jpg](https://zh.wikipedia.org/wiki/File:Taito_1_10000_1945.jpg "fig:Taito_1_10000_1945.jpg")
[臺東平原早期為原住民](../Page/臺東平原.md "wikilink")[卑南族](../Page/卑南族.md "wikilink")、[阿美族居住地](../Page/阿美族.md "wikilink")，以遊耕方式生活其間。[荷治時期及](../Page/臺灣荷治時期.md "wikilink")[清治初期與](../Page/臺灣清治時期.md "wikilink")[後山其他地方共同被稱為](../Page/後山.md "wikilink")「卑南」，已有[漢人往來進行貿易](../Page/漢人.md "wikilink")。荷蘭人曾經為了採金派兵駐紮此地，並與卑南族[利嘉社](../Page/利嘉社.md "wikilink")、[泰安社發生過戰爭](../Page/泰安社.md "wikilink")，今日臺東市第一公墓萬姓祠內尚存有昔日荷蘭士兵骨骸。

今臺東市街區發源地「寶桑」，原為阿美族之一社名，[清](../Page/清.md "wikilink")[道光年間曾有](../Page/道光.md "wikilink")[平埔族](../Page/平埔族.md "wikilink")[西拉雅族駐留於此](../Page/西拉雅族.md "wikilink")，先後達7年之久，後來受卑南族逼迫而集體北遷。[咸豐初年](../Page/咸豐.md "wikilink")，原居[枋寮之](../Page/枋寮鄉.md "wikilink")[閩南漢人](../Page/閩南.md "wikilink")[鄭尚率先在此建立聚點](../Page/鄭尚.md "wikilink")，與原住民從事交易活動。隨後又有更多漢人移入，並教導卑南族人耕種，逐漸形成村落。同治末年，已有漢人28戶在此定居，稱之為「**寶桑-{庄}-**」。

[光緒年間](../Page/光緒.md "wikilink")，設立[卑南廳](../Page/卑南廳.md "wikilink")，其後臺灣[巡撫](../Page/巡撫.md "wikilink")[劉銘傳推動改制](../Page/劉銘傳.md "wikilink")，名之為[臺東直隸州](../Page/臺東直隸州.md "wikilink")。日本領臺前，改稱「**[南鄉新街](../Page/南鄉_\(堡里\).md "wikilink")**」。日本領臺後，稱為「**卑南街**」（註：卑南街與卑南社不同；前者為漢人市街，後者為原住民部落，即今卑南部落），1919年1月15日改稱「**[臺東街](../Page/臺東街.md "wikilink")**」（府令第5號），屬[臺東廳南鄉](../Page/臺東廳.md "wikilink")。1944年12月1日-{[卑南庄](../Page/卑南庄.md "wikilink")}-廢-{庄}-併入臺東街（府令326號），此為總督府在臺灣最後一個行政區調整。

1945年[中華民國政府接收](../Page/中華民國政府.md "wikilink")[日本](../Page/日本帝國.md "wikilink")[臺灣總督府轄區後](../Page/臺灣總督府.md "wikilink")，更名為「**臺東鎮**」，屬[臺東縣政府管轄](../Page/臺東縣.md "wikilink")，為縣治所在地。原卑南-{庄}-之轄區再度分離，並將臺東街北側利吉、富源、富山等地析出，設置[卑南鄉](../Page/卑南鄉.md "wikilink")。1974年10月10日將卑南鄉富岡、岩灣、南王、新園、建興、南榮、卑南、知本、豐田、建和村併入為里，其名稱不變，以湊合縣轄市10萬人口門檻。1976年元旦升格為「**臺東市**」\[1\]。

**歷年所屬行政區列表**

|            |            |
| ---------- | ---------- |
| 起訖年份       | 行政區        |
| 1915\~1919 | 臺東廳卑南街     |
| 1919\~1920 | 臺東廳臺東街     |
| 1920\~1937 | 臺東廳臺東支廳臺東街 |
| 1937\~1945 | 臺東廳臺東郡臺東街  |
| 1945\~1946 | 臺東縣臺東區臺東鎮  |
| 1946\~1976 | 臺東縣臺東鎮     |
| 1976\~     | 臺東縣臺東市     |

## 地理環境

臺東市位在[臺東平原上](../Page/臺東平原.md "wikilink")，為[卑南溪所沖積而成](../Page/卑南溪.md "wikilink")，屬[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")。

## 行政

### 行政區劃

[Taitung_villages2.svg](https://zh.wikipedia.org/wiki/File:Taitung_villages2.svg "fig:Taitung_villages2.svg")

  - 大同里、文化里、四維里、知本里、南榮里
  - 建興里、富豐里、新興里、豐谷里、鐵花里
  - 中心里、仁愛里、永樂里、卑南里、建和里
  - 馬蘭里、復國里、興國里、[豐原里](../Page/豐原里.md "wikilink")、中山里
  - 民生里、成功里、東海里、建國里、強國里
  - 復興里、豐田里、豐榮里、中正里、民族里
  - 光明里、岩灣里、建農里、康樂里、新生里
  - 豐年里、豐樂里、中華里、民權里、自強里
  - [南王里](../Page/南王部落.md "wikilink")、建業里、富岡里、新園里、豐里里
  - 寶桑里
  - 共46里，1,035鄰。

### 公所組織

置市長一人（民選），下設主任秘書、秘書、專員各一員。公所部門設六課四室及 六個附屬單位，現有正式編制共260人（不含臨時人員）

  - 業務部門

<!-- end list -->

  - 民政課
  - 財政課
  - 經建課
  - 工務課
  - 社會暨兵役課
  - 原住民行政課

<!-- end list -->

  - 行政部門

<!-- end list -->

  - 行政室
  - 人事室
  - 主計室
  - 政風室

<!-- end list -->

  - 附屬單位

<!-- end list -->

  - 市立托兒所
  - 清潔隊
  - 公園路燈管理所
  - 殯葬管理所
  - 公有零售市場管理所
  - 果菜市場有限公司

### 歷任首長

  - 卑南街長（1895年－1945年）
    臺東街役長（1919年－1945年）
    臺東鎮長（1945年－1975年）
    臺東市長（1976年至今）

<!-- end list -->

  - 莊丁波
  - 李輝芳
  - [劉宇](../Page/劉宇.md "wikilink")：1994年因案停職
  - 邱明彥：1994年－1998年（代理）
  - [賴坤成](../Page/賴坤成.md "wikilink")：1998年－2006年
  - 陳建閣：2006年－2014年
  - 張國洲：2014年 - （現任）

## 人口

## 警政治安

  - [臺東縣警局臺東分局](http://www.ttcpb.gov.tw/ttp/index.jsp)-(089)322-034

:\*卑南分駐所(非台東市)

:\*蘭嶼分駐所(非台東市)

:\*綠島分駐所(非台東市)

:\*中興派出所

:\*永樂派出所

:\*知本派出所

:\*馬蘭派出所

:\*南王派出所

:\*富岡派出所

:\*豐里派出所

:\*建蘭派出所(非台東市)

:\*寶桑派出所

:\*初鹿派出所(非台東市)

:\*利嘉派出所(非台東市)

:\*東興派出所

:\*公館派出所(非台東市)

:\*朗島派出所(非台東市)

:\*東清派出所(非台東市)

:\*溫泉派出所

### 監獄

  - [臺東監獄](../Page/法務部矯正署臺東監獄.md "wikilink")

## 產業

本市[中心商業區因](../Page/中心商業區.md "wikilink")[舊站廢站而沒落](../Page/台東舊站.md "wikilink")，[新站附近商業活動仍不興盛](../Page/臺東車站.md "wikilink")。重要農產品有[釋迦](../Page/釋迦.md "wikilink")、[油菜花](../Page/油菜花.md "wikilink")、[梅子](../Page/梅子.md "wikilink")、[柚子等](../Page/柚子.md "wikilink")。

## 教育

  - 大專院校

<!-- end list -->

  - [國立臺東大學](../Page/國立臺東大學.md "wikilink")
  - [國立臺東專科學校](../Page/國立臺東專科學校.md "wikilink")

<!-- end list -->

  - 高級中等學校

<!-- end list -->

  - [國立臺東高級中學](../Page/國立臺東高級中學.md "wikilink")
  - [國立臺東女子高級中學](../Page/國立臺東女子高級中學.md "wikilink")
  - [國立臺東大學附屬體育高級中學](../Page/國立臺東大學附屬體育高級中學.md "wikilink")
  - [國立臺東高級商業職業學校](../Page/國立臺東高級商業職業學校.md "wikilink")
  - [天主教公東高級工業職業學校](../Page/臺東縣私立公東高級工業職業學校.md "wikilink")
  - [臺東縣私立育仁中學](https://web.archive.org/web/20110826021916/http://www.lotus.ttct.edu.tw/news/board.asp)

<!-- end list -->

  - 國民中學

<!-- end list -->

  - [臺東縣立東海國民中學](http://www.thjh.ttct.edu.tw/)
  - [臺東縣立新生國民中學](http://www.hsjh.ttct.edu.tw/)
  - [臺東縣立寶桑國民中學](http://www.tcbjh.ttct.edu.tw/)
  - [臺東縣立卑南國民中學](http://www.pnjh.ttct.edu.tw/)
  - [臺東縣立知本國民中學](http://www.cpjh.ttct.edu.tw/)
  - 臺東縣立豐田國民中學
  - [臺東縣私立育仁中學](https://web.archive.org/web/20110826021916/http://www.lotus.ttct.edu.tw/news/board.asp)國中部
  - [臺東縣私立均一國民中小學](http://je.boe.ttct.edu.tw/front/bin/home.phtml)

<!-- end list -->

  - 國民小學

<!-- end list -->

  - [國立臺東大學附設實驗國民小學](http://www.nttues.nttu.edu.tw/)
  - [臺東縣臺東市仁愛國民小學](http://dns.raps.ttct.edu.tw/school/menu/index.php)
  - [臺東縣臺東市光明國民小學](http://www.gmps.ttct.edu.tw/)
  - [臺東縣臺東市東海國民小學](http://www.dhips.ttct.edu.tw/)
  - [臺東縣臺東市復興國民小學](http://www.fsps.ttct.edu.tw/)
  - [臺東縣臺東市寶桑國民小學](http://www.bsps.ttct.edu.tw/)
  - [臺東縣臺東市新生國民小學](http://www.ssps.ttct.edu.tw/)
  - [臺東縣臺東市豐田國民小學](http://www.ftps.ttct.edu.tw/)
  - [臺東縣臺東市豐里國民小學](http://www.flps.ttct.edu.tw/)
  - [臺東縣臺東市豐年國民小學](http://www.fnps.ttct.edu.tw/)
  - [臺東縣臺東市豐源國民小學](http://www.fayps.ttct.edu.tw/)
  - [臺東縣臺東市豐榮國民小學](http://www.frps.ttct.edu.tw/)
  - [臺東縣臺東市馬蘭國民小學](http://www.mlps.ttct.edu.tw/)
  - [臺東縣臺東市康樂國民小學](http://www.klps.ttct.edu.tw/)
  - [臺東縣臺東市卑南國民小學](http://www.bnps.ttct.edu.tw/)
  - [臺東縣臺東市岩灣國民小學](https://web.archive.org/web/20120212182436/http://www.yups.ttct.edu.tw/front/bin/home.phtml)
  - [臺東縣臺東市南王國民小學](http://210.240.126.195/nwx/)
  - [臺東縣臺東市知本國民小學](http://www.jbps.ttct.edu.tw/)
  - [臺東縣臺東市建和國民小學](http://www.jhps.ttct.edu.tw/)
  - [臺東縣臺東市富岡國民小學](http://www.fgps.ttct.edu.tw/)
  - [臺東縣臺東市新園國民小學](http://www.syps.ttct.edu.tw/)

## 醫療

  - [衛生福利部臺東醫院](../Page/衛生福利部臺東醫院.md "wikilink")
  - [臺北榮民總醫院臺東分院](../Page/臺北榮民總醫院臺東分院.md "wikilink")

:\*勝利院區

  - [臺東基督教醫院](../Page/台東基督教醫院.md "wikilink")
  - [臺東聖母醫院](../Page/台東聖母醫院.md "wikilink")
  - [馬偕紀念醫院臺東分院](../Page/馬偕紀念醫院臺東分院.md "wikilink")

## 交通

[Taiwan_Fugang_Fishery_Harbor.JPG](https://zh.wikipedia.org/wiki/File:Taiwan_Fugang_Fishery_Harbor.JPG "fig:Taiwan_Fugang_Fishery_Harbor.JPG")交界的[富岡漁港](../Page/富岡漁港.md "wikilink")\]\]
[LANDSAT_Taitung_City.jpg](https://zh.wikipedia.org/wiki/File:LANDSAT_Taitung_City.jpg "fig:LANDSAT_Taitung_City.jpg")

### 鐵路

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [臺東線](../Page/臺東線.md "wikilink")
      - [山里隧道](../Page/山里隧道.md "wikilink")
      - [臺東車站](../Page/臺東車站.md "wikilink")
  - [南迴線](../Page/南迴線.md "wikilink")
      - [臺東車站](../Page/臺東車站.md "wikilink")
      - [康樂車站](../Page/康樂車站.md "wikilink")
      - [知本車站](../Page/知本車站.md "wikilink")

### 公車、客運

  - [普悠瑪客運](../Page/普悠瑪客運.md "wikilink")（[台東縣市區公車](../Page/台東縣市區公車.md "wikilink")）
  - [花蓮客運](../Page/花蓮客運.md "wikilink")
  - [鼎東客運](../Page/鼎東客運.md "wikilink")
  - [國光客運](../Page/國光客運.md "wikilink")

### 航運

  - [富岡漁港可搭船至](../Page/富岡漁港.md "wikilink")[蘭嶼鄉](../Page/蘭嶼鄉.md "wikilink")、[綠島鄉](../Page/綠島鄉.md "wikilink")

### 航空

[臺東豐年機場可搭機至](../Page/臺東機場.md "wikilink")[臺北](../Page/臺北.md "wikilink")、[綠島](../Page/綠島.md "wikilink")、[蘭嶼等地](../Page/蘭嶼.md "wikilink")。現有航班包括由[德安航空經營之綠島](../Page/德安航空.md "wikilink")、蘭嶼離島航線，與由[華信航空](../Page/華信航空.md "wikilink")、[立榮航空經營的臺北](../Page/立榮航空.md "wikilink")-臺東定期航班。除國內定期航班之外，臺東機場之航線亦包含由華信航空經營之臺東-[香港季節性包機航線](../Page/香港.md "wikilink")。

### 公路

  - [TW_PHW9.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9.svg "fig:TW_PHW9.svg")[台9線](../Page/台9線.md "wikilink")（[花東公路](../Page/花東公路.md "wikilink")、[南迴公路](../Page/南迴公路.md "wikilink")）
  - [TW_PHW9b.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9b.svg "fig:TW_PHW9b.svg")[台9乙線](../Page/台9線#乙線.md "wikilink")
  - [TW_PHW11.svg](https://zh.wikipedia.org/wiki/File:TW_PHW11.svg "fig:TW_PHW11.svg")[台11線](../Page/台11線.md "wikilink")（花東海岸公路）
  - [TW_PHW11b.svg](https://zh.wikipedia.org/wiki/File:TW_PHW11b.svg "fig:TW_PHW11b.svg")[台11乙線](../Page/台11線#乙線.md "wikilink")
  - [TW_CHW194.svg](https://zh.wikipedia.org/wiki/File:TW_CHW194.svg "fig:TW_CHW194.svg")[縣道194號](../Page/縣道194號.md "wikilink")：[知本](../Page/知本.md "wikilink")－台東市
  - [TW_CHW197.svg](https://zh.wikipedia.org/wiki/File:TW_CHW197.svg "fig:TW_CHW197.svg")[縣道197號](../Page/縣道197號.md "wikilink")：[卑南鄉](../Page/卑南鄉.md "wikilink")－台東市石川

## 商業

  - [大潤發](../Page/大潤發.md "wikilink")
  - [家樂福](../Page/家樂福.md "wikilink")
  - [全聯福利中心](../Page/全聯福利中心.md "wikilink")
  - [寶雅生活館](../Page/寶雅生活館.md "wikilink")
  - [全國電子](../Page/全國電子.md "wikilink")
  - [康是美](../Page/康是美.md "wikilink")
  - [屈臣氏](../Page/屈臣氏.md "wikilink")
  - [秀泰廣場臺東店](../Page/秀泰廣場.md "wikilink")（2013年7月12日開幕），取代[大同戲院](../Page/大同戲院_\(臺東市\).md "wikilink")，結合了美食餐飲、運動服飾品牌，成為台東唯一的購物廣場。

## 大眾媒體

  - [原住民族電視台東部新聞採訪中心](../Page/原住民族電視台.md "wikilink")（位於[國立臺灣史前文化博物館內](../Page/國立臺灣史前文化博物館.md "wikilink")\[2\]）

## 旅遊

[Taitung_City.jpg](https://zh.wikipedia.org/wiki/File:Taitung_City.jpg "fig:Taitung_City.jpg")

  - [國立臺灣史前文化博物館](../Page/國立臺灣史前文化博物館.md "wikilink")
  - [臺東棒球村](../Page/臺東棒球村.md "wikilink")
  - [臺東舊站](../Page/臺東舊站.md "wikilink")
  - [白色陋屋](../Page/白色陋屋.md "wikilink")
  - [南王部落](../Page/南王部落.md "wikilink")
  - [鐵道藝術村](../Page/鐵道藝術村.md "wikilink")（鐵花村）
  - [臺東天后宮](../Page/臺東天后宮.md "wikilink")
  - [海山寺](../Page/海山寺_\(臺東市\).md "wikilink")
  - [小野柳](../Page/小野柳.md "wikilink")
  - [夢幻湖](../Page/知本濕地.md "wikilink")
  - [琵琶湖](../Page/琵琶湖_\(台灣\).md "wikilink")
  - [卑南文化遺址公園](../Page/卑南文化遺址公園.md "wikilink")
  - [台東森林公園](../Page/台東森林公園.md "wikilink")

## 出身人物

  - [劉櫂豪](../Page/劉櫂豪.md "wikilink")，櫂音ㄓㄠˋ，曾任臺東地方法院法官，現任[民主進步黨立法委員](../Page/民主進步黨.md "wikilink")。
  - [谷拉斯·馬亨亨](../Page/谷拉斯·馬亨亨.md "wikilink")：阿美族[馬蘭社重要領袖](../Page/馬蘭社.md "wikilink")，帶領馬蘭社崛起，並擊敗清軍，引領日軍登陸東部，開啟東部日治時期。
  - [陳達達](../Page/陳達達.md "wikilink")：卑南大社聯盟領袖，最後一位[卑南王](../Page/卑南王.md "wikilink")。
  - [楊傳廣](../Page/楊傳廣.md "wikilink")：十項全能史上打破9000分的紀錄保持者（臺東市榮譽市民）
  - [吳明修](../Page/吳明修.md "wikilink")：建築師，臺灣現代建築代表人物。
  - [陳建年](../Page/陳建年_\(政治人物\).md "wikilink")：臺東首位原住民縣長、曾任原住民族委員會主委。
  - [陳建年](../Page/陳建年_\(歌手\).md "wikilink")：[第11屆金曲獎最佳國語男演唱人獎得主](../Page/第11屆金曲獎.md "wikilink")。
  - [紀曉君](../Page/紀曉君.md "wikilink")：[臺灣原住民](../Page/臺灣原住民.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [陽建福](../Page/陽建福.md "wikilink")：[中華職棒](../Page/中華職棒.md "wikilink")[統一獅隊](../Page/統一獅.md "wikilink")[投手](../Page/投手.md "wikilink")。
  - [陽岱鋼](../Page/陽岱鋼.md "wikilink")：[日本職棒](../Page/日本職棒.md "wikilink")[讀賣巨人隊](../Page/讀賣巨人隊.md "wikilink")[外野手](../Page/外野手.md "wikilink")。
  - [陽耀勳](../Page/陽耀勳.md "wikilink")：中華職棒[lamigo桃猿隊外野手](../Page/lamigo桃猿.md "wikilink")。
  - [陽東益](../Page/陽東益.md "wikilink")：中華職棒[統一獅隊](../Page/統一獅.md "wikilink")[內野手](../Page/內野手.md "wikilink")。
  - [陽介仁](../Page/陽介仁.md "wikilink")：前中華職棒[興農牛隊](../Page/興農牛.md "wikilink")[投手](../Page/投手.md "wikilink")，在[中華職棒投出](../Page/中華職棒.md "wikilink")「[無安打比賽](../Page/無安打比賽.md "wikilink")」的第一人。
  - [陳樹菊](../Page/陳樹菊.md "wikilink")：知名慈善家、菜販，2010年[時代百大人物](../Page/時代百大人物.md "wikilink")、2012年[麥格塞塞獎得主](../Page/麥格塞塞獎.md "wikilink")。
  - [沈文程](../Page/沈文程.md "wikilink")：台灣知名男演員、主持人和男歌手

## 政治

  - 2014年臺東市長選舉

|}

## 參考文獻

<references/>

## 外部連結

  - [臺東市公所](http://www.taitungcity.gov.tw/)

[臺東市](../Category/臺東市.md "wikilink")
[Category:臺東縣行政區劃](../Category/臺東縣行政區劃.md "wikilink")
[Category:平地原住民鄉鎮市](../Category/平地原住民鄉鎮市.md "wikilink")
[Category:台灣沿海城市](../Category/台灣沿海城市.md "wikilink")
[Category:台灣行政區劃之最](../Category/台灣行政區劃之最.md "wikilink")
[Category:臺灣縣治](../Category/臺灣縣治.md "wikilink")
[Category:台灣縣轄市](../Category/台灣縣轄市.md "wikilink")

1.  [《發現古地圖：城市的起源》，臺東市公所全球資訊網](http://www.taitungcity.gov.tw/livinginthecity\(0\).htm)

2.  [原民台成立東部新聞中心](http://www.lihpao.com/?action-viewnews-itemid-6096)