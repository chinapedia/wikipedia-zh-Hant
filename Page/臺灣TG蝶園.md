[TG_Butterfly_Garden_on_2006_Taiwan_Pride.JPG](https://zh.wikipedia.org/wiki/File:TG_Butterfly_Garden_on_2006_Taiwan_Pride.JPG "fig:TG_Butterfly_Garden_on_2006_Taiwan_Pride.JPG")
**台灣TG蝶園**，是[台灣第一個](../Page/台灣.md "wikilink")[跨性別民間](../Page/跨性別.md "wikilink")[組織](../Page/組織.md "wikilink")，於2000年在[桃園縣](../Page/桃園市.md "wikilink")[中壢市](../Page/中壢區.md "wikilink")（今桃園市中壢區）成立\[1\]。從一開始的十餘人聚會，已發展成為[通訊簿擁有一百三十餘人的組織](../Page/通訊簿.md "wikilink")，定期[聚會每兩個月一次](../Page/聚會.md "wikilink")。

從批判警方惡意[臨檢](../Page/臨檢.md "wikilink")，到譴責[媒體侵犯](../Page/媒體.md "wikilink")[隱私](../Page/隱私.md "wikilink")，抗議教育部國語辭典使用歧視性語言，維護相關跨性別身分、[兵役](../Page/兵役.md "wikilink")、[醫療](../Page/醫療.md "wikilink")、[工作](../Page/工作.md "wikilink")、[教育等權益](../Page/教育.md "wikilink")，蝶園已逐漸在社運行列中邁出穩健的步伐。

從2004年開始，蝶園固定有隊伍參與[台灣同志大遊行](../Page/台灣同志大遊行.md "wikilink")，提出相關跨性別的各項訴求\[2\]。

從2008年8月開始，開設跨性別[諮詢服務專線](../Page/諮詢.md "wikilink")，主要提供有性別困擾的民眾諮詢。此外，也包含學校老師、醫療人員、家長以及跨性別伴侶的諮詢服務。

2017年蝶園冬眠\[3\]，皓日專線\[4\]跨性別諮詢專線則維持。

## 相關條目

  - [台灣性別不明關懷協會](../Page/台灣性別不明關懷協會.md "wikilink")
  - [跨性別倡議站](../Page/跨性別倡議站.md "wikilink")

## 參考文獻

[Category:台灣跨性別組織](../Category/台灣跨性別組織.md "wikilink")
[Category:2000年建立的組織](../Category/2000年建立的組織.md "wikilink")
[Category:臺灣已解散組織](../Category/臺灣已解散組織.md "wikilink")
[Category:桃園市組織](../Category/桃園市組織.md "wikilink")
[Category:中壢區](../Category/中壢區.md "wikilink")

1.
2.
3.  <http://sex.ncu.edu.tw/jo_article/2017/11/我的跨性別接觸史：兼論台灣tg蝶園/>
4.  <https://www.facebook.com/haori.hotline/>