本條目講述**[臺灣](../Page/臺灣.md "wikilink")[交通](../Page/交通.md "wikilink")**概況及其歷史，包括各種[運輸系統](../Page/運輸.md "wikilink")、[載具](../Page/載具.md "wikilink")、[大眾運輸工具](../Page/公共交通.md "wikilink")、[管道運輸等](../Page/管道運輸.md "wikilink")。目前臺灣交通的最高主管機構是[中華民國交通部](../Page/中華民國交通部.md "wikilink")。

## 歷史

台灣島由於地形關係，東西部交通被[中央山脈所阻擋](../Page/中央山脈.md "wikilink")，南北部交通由[河流所切斷](../Page/臺灣河流列表.md "wikilink")。因此早期臺灣交通極度依賴海運，甚至島嶼南北部連絡常常需要乘船至中國大陸轉乘。「[一府二鹿三艋舺](../Page/一府二鹿三艋舺.md "wikilink")」的稱謂，更是說明[臺南](../Page/臺南市.md "wikilink")、[彰化](../Page/彰化縣.md "wikilink")[鹿港](../Page/鹿港鎮.md "wikilink")、[臺北](../Page/臺北市.md "wikilink")[萬華在當時的](../Page/萬華區.md "wikilink")[航運地位](../Page/航運.md "wikilink")。

[清朝](../Page/清朝.md "wikilink")[治理臺灣的末期時間](../Page/臺灣清治時期.md "wikilink")，才開始大規模整理南北向的陸上交通。至於東西部的連結，[沈葆楨](../Page/沈葆楨.md "wikilink")、[吳沙等人分別開闢](../Page/吳沙.md "wikilink")[淡蘭古道](../Page/淡蘭古道.md "wikilink")、[八通關古道等橫越](../Page/八通關古道.md "wikilink")[雪山山脈](../Page/雪山山脈.md "wikilink")、中央山脈的山間小徑，做為東西部往來的捷徑。時任[巡撫](../Page/巡撫.md "wikilink")[劉銘傳更計畫興建](../Page/劉銘傳.md "wikilink")[臺灣鐵路](../Page/臺灣鐵路_\(清朝\).md "wikilink")，從[基隆至抵臺南](../Page/基隆市.md "wikilink")。但後繼官員為減輕財政負擔，雖然這條鐵路已測量至[大甲溪](../Page/大甲溪.md "wikilink")，僅完成基隆至臺北、臺北至新竹兩條路線，但已領先當時[中國其他地區的鐵路建設](../Page/中國.md "wikilink")。

在[馬關條約簽訂後](../Page/馬關條約.md "wikilink")，[日本](../Page/日本.md "wikilink")[取得台灣主權](../Page/台灣日治時期.md "wikilink")，而今日臺灣交通建設的基礎，也是奠定於此時期。不但[西部鐵路由](../Page/縱貫鐵路.md "wikilink")[新竹延伸至](../Page/新竹市.md "wikilink")[屏東縣](../Page/屏東縣.md "wikilink")[枋寮](../Page/枋寮鄉.md "wikilink")，東部的[花蓮](../Page/花蓮市.md "wikilink")、[臺東之間](../Page/臺東市.md "wikilink")，也建有窄軌鐵路。整個西部平原，[糖業](../Page/糖業鐵路.md "wikilink")、鹽業等專用[輕便鐵路更是密佈](../Page/輕便鐵路.md "wikilink")。至於空中交通，亦是奠定於此時期，但多以軍事機場為優先。[日本殖民時期](../Page/台灣日治時期.md "wikilink")，臺灣的公路及鐵路，遵循日本[靠左行駛的道路規則](../Page/道路通行方向.md "wikilink")；1945年，[中華民國接手統治臺灣之後](../Page/中華民國.md "wikilink")，除了鐵路系統沿用舊制，公路皆改為靠右行駛。

[中華民國政府於](../Page/中華民國政府.md "wikilink")[國共內戰遷臺後](../Page/國共內戰.md "wikilink")，由於面對[兩岸情勢的危機](../Page/海峽兩岸關係.md "wikilink")，因此早期的交通建設以[軍事防衛目的為主](../Page/軍事.md "wikilink")，例如「[糖鐵南北平行線](../Page/南北平行預備線.md "wikilink")」及[西螺大橋的修築](../Page/西螺大橋.md "wikilink")，或[中部橫貫公路的開闢等](../Page/中部橫貫公路.md "wikilink")。近數十年來，[十大建設](../Page/十大建設.md "wikilink")、[鐵路電氣化](../Page/鐵路電氣化.md "wikilink")、環島鐵路計畫、[高速公路路網一一完工](../Page/高速公路.md "wikilink")，加上國內航空路網的建構，以及各大城市[軌道交通系統](../Page/捷運.md "wikilink")、南北[高速鐵路的興築](../Page/高速鐵路.md "wikilink")，今日臺灣交通的面貌逐漸完成。

## 交通系統配置

### 陸上交通

[高鐵448次，高鐵台中站北_(Flickr_id_4628910232).jpg](https://zh.wikipedia.org/wiki/File:高鐵448次，高鐵台中站北_\(Flickr_id_4628910232\).jpg "fig:高鐵448次，高鐵台中站北_(Flickr_id_4628910232).jpg")及[中山高速公路](../Page/中山高速公路.md "wikilink")。\]\]

#### 公路

公路系統遍及台灣島及澎湖群島各鄉鎮，公路系統依公路法規定共分為五級:
**[國道](../Page/中華民國國道.md "wikilink")1,050公里**、**[省道](../Page/台灣省道.md "wikilink")5,262公里**、**[縣道](../Page/台灣縣道.md "wikilink")([市道](../Page/台灣市道.md "wikilink"))3,602公里**、**[鄉道](../Page/台灣鄉道.md "wikilink")([區道](../Page/台灣區道.md "wikilink"))11,383公里**、**[專用公路](../Page/台灣專用公路.md "wikilink")413公里**，總長度43,206-{[公里](../Page/公里.md "wikilink")}-（2017年止）\[1\]。除以上五級公路外，其餘為普通道路，道路又分由水土保持局管理的農路、由林務局管理的林道、位於都市計劃區內的市區道路（含市區快速道路、巷弄）和其它普通產業道路及私人道路。

#### 汽車運輸業

[Motorcycles_in_Taipei.JPG](https://zh.wikipedia.org/wiki/File:Motorcycles_in_Taipei.JPG "fig:Motorcycles_in_Taipei.JPG")街頭的車陣\]\]

汽車運輸業依公路法規定可分為公路汽車客運業、市區汽車客運業、遊覽車客運業、計程車客運業、小客車租賃業、小貨車租賃業，以及汽車貨運業等。

#### 軌道運輸

軌道運輸服務主要分為傳統鐵路（[台鐵](../Page/臺灣鐵路管理局.md "wikilink")）、[高速鐵路](../Page/高速鐵路.md "wikilink")（[台灣高鐵](../Page/台灣高速鐵路.md "wikilink")）、專用鐵路（如[阿里山林鐵](../Page/阿里山森林鐵路.md "wikilink")）以及大眾捷運法所規定的[大眾捷運系統](../Page/捷運.md "wikilink")、（[輕軌](../Page/輕軌.md "wikilink")、[纜車](../Page/纜車.md "wikilink")）等系統。

台灣島在[日治時期](../Page/台灣日治時期.md "wikilink")，主要發展鐵路運輸，各種產業鐵路曾密如蛛網遍佈全島。[第二次世界大戰結束進入](../Page/第二次世界大戰.md "wikilink")[民國時期後](../Page/台灣戰後時期.md "wikilink")，[台灣政府受到](../Page/台灣政府.md "wikilink")[美式公路主義影響](../Page/美國交通.md "wikilink")，改以公路為主要交通方式，例如[臺北市發展](../Page/臺北市.md "wikilink")[城市軌道交通系統時程](../Page/城市軌道交通系統.md "wikilink")，就遲上[亞洲其他新興](../Page/亞洲.md "wikilink")[首都許多](../Page/首都.md "wikilink")；公路建設的興盛，也造成鐵路運量降低，許多鐵路線因而走入歷史。直至1990年代以後，隨著全台快速、高速公路建設逐漸完備，以及都市面臨雍塞、污染問題，政府方又重視軌道運輸，推動如[臺鐵捷運化等政策](../Page/臺鐵捷運化.md "wikilink")。

2006年8月22日，[行政院經濟建設委員會正式宣佈未來以軌道運輸取代公路建設](../Page/中華民國國家發展委員會.md "wikilink")，以解決環境問題。2007年起，由於大量軌道運輸路線的興築，以及公路建設已近完備的原因，軌道與公路的建設預算比例已達到6:4
。2011年，臺鐵先後開闢[沙崙線](../Page/沙崙線.md "wikilink")、[六家線](../Page/六家線.md "wikilink")，為臺鐵19年以來新線。

### 空中交通

[機場多為軍民合用機場](../Page/機場.md "wikilink")，民用部份由[交通部民用航空局管理](../Page/交通部民用航空局.md "wikilink")，軍用則分由[空軍與](../Page/中華民國空軍.md "wikilink")[海軍管理](../Page/中華民國海軍.md "wikilink")。

#### 國際機場

  - 主要有五個國際機場：

<!-- end list -->

  - 位於[台北市](../Page/台北市.md "wikilink")[松山區的](../Page/松山區_\(臺北市\).md "wikilink")[台北松山機場經營東亞四地](../Page/台北松山機場.md "wikilink")（松山與虹橋、羽田、金浦）對飛等國際航線（國際與兩岸定期包機）和國內航線
  - 位於[桃園市](../Page/桃園市.md "wikilink")[大園區的](../Page/大園區.md "wikilink")[台灣桃園國際機場專營國際航線](../Page/台灣桃園國際機場.md "wikilink")
  - 位於[台中市](../Page/台中市.md "wikilink")[大雅區的](../Page/大雅區.md "wikilink")[台中國際機場經營國際和國內航線](../Page/台中國際機場.md "wikilink")（國際與兩岸定期包機）
  - 位於[台南市](../Page/台南市.md "wikilink")[南區與](../Page/南區_\(臺南市\).md "wikilink")[仁德區的](../Page/仁德區.md "wikilink")[臺南國際機場經營國際和國內航線](../Page/臺南國際機場.md "wikilink")（國際與兩岸定期包機）
  - 位於[高雄市](../Page/高雄市.md "wikilink")[小港區的](../Page/小港區.md "wikilink")[高雄國際機場經營國際和國內航線](../Page/高雄國際機場.md "wikilink")

<!-- end list -->

  - 常態性之國際包機起降

<!-- end list -->

  - 位於[花蓮縣](../Page/花蓮縣.md "wikilink")[新城鄉的](../Page/新城鄉_\(台灣\).md "wikilink")[花蓮機場](../Page/花蓮機場.md "wikilink")。
  - 位於[澎湖縣](../Page/澎湖縣.md "wikilink")[湖西鄉的](../Page/湖西鄉.md "wikilink")[馬公機場](../Page/馬公機場.md "wikilink")。

#### 國內機場

  - 位於[嘉義縣](../Page/嘉義縣.md "wikilink")[水上鄉的](../Page/水上鄉.md "wikilink")[嘉義機場](../Page/嘉義機場.md "wikilink")
  - 位於[屏東縣](../Page/屏東縣.md "wikilink")[恆春鎮的](../Page/恆春鎮.md "wikilink")[恆春機場](../Page/恆春機場.md "wikilink")（現今已無任何航班）
  - 位於[澎湖縣](../Page/澎湖縣.md "wikilink")[望安鄉的](../Page/望安鄉.md "wikilink")[望安機場](../Page/望安機場.md "wikilink")
  - 位於[澎湖縣](../Page/澎湖縣.md "wikilink")[七美鄉的](../Page/七美鄉_\(臺灣\).md "wikilink")[七美機場](../Page/七美機場.md "wikilink")
  - 位於[臺東縣](../Page/臺東縣.md "wikilink")[臺東市的](../Page/臺東市.md "wikilink")[臺東機場](../Page/臺東機場.md "wikilink")
  - 位於[臺東縣](../Page/臺東縣.md "wikilink")[綠島鄉的](../Page/綠島鄉.md "wikilink")[綠島機場](../Page/綠島機場.md "wikilink")
  - 位於[臺東縣](../Page/臺東縣.md "wikilink")[蘭嶼鄉的](../Page/蘭嶼鄉.md "wikilink")[蘭嶼機場](../Page/蘭嶼機場.md "wikilink")
  - 位於[金門縣](../Page/金門縣.md "wikilink")[金湖鎮的](../Page/金湖鎮.md "wikilink")[金門機場](../Page/金門機場.md "wikilink")
  - 位於[連江縣](../Page/連江縣_\(中華民國\).md "wikilink")[南竿鄉的](../Page/南竿鄉.md "wikilink")[馬祖南竿機場](../Page/馬祖南竿機場.md "wikilink")
  - 位於[連江縣](../Page/連江縣_\(中華民國\).md "wikilink")[北竿鄉的](../Page/北竿鄉.md "wikilink")[馬祖北竿機場](../Page/馬祖北竿機場.md "wikilink")

#### 直升機航空站

  - 總計：10（2008年）

包括東引、東莒、西莒（連江縣政府）、小琉球（屏東縣政府）、玉里（花蓮縣玉里鎮公所）、嘉義竹崎（中興航空公司）、溪頭米堤、天龍、童綜合醫院及台北榮民總醫院。

#### 熱氣球升空站

主要位於[臺東縣](../Page/臺東縣.md "wikilink")[鹿野鄉鹿野高台](../Page/鹿野鄉.md "wikilink")。

#### 航空公司

[中華航空和](../Page/中華航空.md "wikilink")[長榮航空為台灣最大的兩家營運國際航線的航空公司](../Page/長榮航空.md "wikilink")。而[立榮航空](../Page/立榮航空.md "wikilink")、[華信航空](../Page/華信航空.md "wikilink")、[遠東航空](../Page/遠東航空.md "wikilink")、[德安航空則是以經營國內航線為主的四家公司](../Page/德安航空.md "wikilink")，部分業者也飛行短程國際航線。

### 海上交通

台灣島四面環海，海上交通在國共內戰後期的背景下長期遭受管制，國際物流仰賴政府特許航運公司透過商船運輸，也造就台灣有兩大對外的港口，如：基隆港、高雄港。且由於台灣島四面環海，漁港數量多達231處[漁港](../Page/漁港.md "wikilink")，其中本島有155處（占三分之二），離島76處（占三分之一）。

#### 國際商港

[KeelungHarbor_west_002.JPG](https://zh.wikipedia.org/wiki/File:KeelungHarbor_west_002.JPG "fig:KeelungHarbor_west_002.JPG")
台灣島的國際商港、國內商港的管理業務，由[交通部](../Page/中華民國交通部.md "wikilink")[航港局負責管理商港](../Page/交通部航港局.md "wikilink")（分為國際商港、國內商港）所管轄。

交通部以國營之[臺灣港務公司經營各商港](../Page/臺灣港務公司.md "wikilink")，並劃定各港務分公司轄區；離島方面，交通部委託[金門縣政府](../Page/金門縣政府.md "wikilink")、[連江縣政府成立港務處](../Page/連江縣政府.md "wikilink")，管理縣內國內商港。目前有**4座國際商港**、**4座國際商港輔助港口**、**5座國內商港**。

  - [基隆港](../Page/基隆港.md "wikilink")
  - [臺北港](../Page/臺北港.md "wikilink")
  - [臺中港](../Page/臺中港.md "wikilink")
  - [高雄港](../Page/高雄港.md "wikilink")

#### 工業專用港

中華民國(臺澎金馬)的工業專用港的管理業務，由[經濟部](../Page/中華民國經濟部.md "wikilink")[工業局負責管理](../Page/經濟部工業局.md "wikilink")。工業專用港共有2座營運中、1座興建中。

#### 國內漁港

[Taiwan_2009_SuHua_Highway_SuAo_Port_FRD_6960.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_2009_SuHua_Highway_SuAo_Port_FRD_6960.jpg "fig:Taiwan_2009_SuHua_Highway_SuAo_Port_FRD_6960.jpg")
臺灣港口的管理業務，由[中華民國](../Page/中華民國.md "wikilink")[行政院農業委員會](../Page/行政院農業委員會.md "wikilink")[漁業署負責管理漁港](../Page/漁業署.md "wikilink")。漁港則有**9座第一類漁港**、**216座第二類漁港**\[2\]。

#### 航運業者

  - 最大的三家海運公司[長榮海運](../Page/長榮海運.md "wikilink")（[長榮集團](../Page/長榮集團.md "wikilink")）、[萬海航運](../Page/萬海航運.md "wikilink")、與[陽明海運](../Page/陽明海運.md "wikilink")（原為公營），而離島與本島間的海上客運則由政府與民間業者共同經營。

<!-- end list -->

  - **商船：**

<!-- end list -->

  - 總計：590艘（1,000 GRT 以上）3,417,768 GRT/5,617,318 DWT

<!-- end list -->

  - 类型：散装船36、货船23、化学品运输船2、combination bulk3、集装箱船37、油轮17、refrigerated
    cargo 10、roll on/roll off 2
  - 境外拥有:：[古巴](../Page/古巴.md "wikilink")
    1、[香港](../Page/香港.md "wikilink") 4
  - 在其他国家註冊：457（2004年）

## 現況問題

1.  計程車沒有落實總量管制，造成計程車車輛供過於求。\[3\]

2.  不夠完善的[大眾運輸系統](../Page/大眾運輸系統.md "wikilink")，造成私人交通運具使用率遠於大眾運輸使用率。

3.  汽機車分流之道路規劃未符合實際需求，[機車使用者沒有擁有足夠之路權](../Page/摩托車.md "wikilink")，且直接影響行車安全，使得民怨四起。\[4\]

4.  私有運具持有率極高，造成許多停車問題。例如路邊違規停車、併排停車。造成車道數減少、車速降低、公車無法停靠車站，且違規停車之車輛駕駛人於上下車時也常造成後方車輛之行車安全。

5.
6.  部分駕駛人不遵守交通規則，造成許多交通問題。例如汽車逆向超車、汽機車不禮讓行人、機車在車陣中亂鑽、公車靠站時常發生機車在乘客上下車之處行駛經過而發生危險的現象。

## 參考資料

## 參見

  - [中華民國國道](../Page/中華民國國道.md "wikilink")
  - [臺灣汽車客運](../Page/臺灣汽車客運.md "wikilink")
  - [臺灣鐵路運輸](../Page/臺灣鐵路運輸.md "wikilink")
  - [臺灣公路](../Page/臺灣公路.md "wikilink")
  - [臺灣海運](../Page/臺灣海運.md "wikilink")
  - [臺灣空運](../Page/臺灣空運.md "wikilink")
  - [臺灣公路俗名列表](../Page/臺灣公路俗名列表.md "wikilink")

## 外部連結

  - [交通部全球資訊網](http://www.motc.gov.tw/)
  - [交通部公路總局](http://www.thb.gov.tw/)
  - [交通部民用航空局](http://www.caa.gov.tw/)
  - [交通部高速公路局](https://www.freeway.gov.tw/)

{{-}}

[台灣交通](../Category/台灣交通.md "wikilink")

1.
2.  [漁港類別簡介 -
    漁業署全球資訊網](http://www.fa.gov.tw/pages/detail.aspx?Node=883&Page=9484&Index=9)
3.  [修法提高Uber罰金的荒謬（上）：空車率近40％的牌照濫發才是計程車殺手](https://www.thenewslens.com/article/57647)
4.  [摩托族行不安
    年死1189人-台灣摩托車友協會](http://trocmra.blogspot.tw/2015/02/1189.html)