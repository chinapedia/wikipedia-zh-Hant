[Taiwan2014.svg](https://zh.wikipedia.org/wiki/File:Taiwan2014.svg "fig:Taiwan2014.svg")示意圖\]\]

**臺灣地區**，亦称**臺澎金馬地區**、**臺澎金馬**、**臺閩地區**，《[中華民國憲法增修條文](../Page/中華民國憲法增修條文.md "wikilink")》歷次版本中稱為「**自由地區**」，是指自1955年[大陳島撤退之後迄今](../Page/大陳島撤退.md "wikilink")，[中華民國有效管理的](../Page/中華民國.md "wikilink")[領土](../Page/領土.md "wikilink")\[1\]，與1949年[國共內戰後由](../Page/國共內戰.md "wikilink")[中華人民共和國實際統治的](../Page/中華人民共和國.md "wikilink")「[大陸地區](../Page/中国大陆.md "wikilink")」相對。涵蓋範圍包括[臺灣及附屬島嶼](../Page/臺灣.md "wikilink")、[澎湖群島](../Page/澎湖群島.md "wikilink")、[金門群島](../Page/金門縣.md "wikilink")、[馬祖列島](../Page/馬祖列島.md "wikilink")、[烏坵列嶼](../Page/烏坵鄉.md "wikilink")、[東沙群島](../Page/東沙群島.md "wikilink")、[南沙群島之](../Page/南沙群島.md "wikilink")[太平島與](../Page/太平島.md "wikilink")[中洲礁](../Page/中洲礁.md "wikilink")\[2\]\[3\]，全由[島嶼構成](../Page/中華民國島嶼列表.md "wikilink")。由於臺灣為最主要的組成領土，因此得名。

## 使用

目前[中華民國政府對於表示其有效統治領土的用詞不一](../Page/中華民國政府.md "wikilink")，但以使用「臺灣地區」為主：

  - **臺灣地區** -
    主要使用於《[兩岸人民關係條例](../Page/臺灣地區與大陸地區人民關係條例.md "wikilink")》等[兩岸關係相關法律](../Page/兩岸關係.md "wikilink")。
  - **臺澎金馬（地區）** - 部分政府文件使用。主要用於表示中華民國現有統治範圍的場合。\[4\]
  - **臺閩地區** -
    主要在政府的[統計刊物中使用](../Page/統計.md "wikilink")，使用情形與「[臺澎金馬](../Page/臺澎金馬.md "wikilink")」類似。在統計刊物中，臺灣地區僅指臺灣本島與澎湖群島。\[5\]
  - **自由地區** -
    最早出現於《[動員戡亂時期臨時條款](../Page/動員戡亂時期臨時條款.md "wikilink")》，相對於由中國共產黨所佔領的「淪陷地區」。1991年《[動員戡亂時期臨時條款](../Page/動員戡亂時期臨時條款.md "wikilink")》廢止後所實施的《[中華民國憲法增修條文](../Page/中華民國憲法增修條文.md "wikilink")》\[6\]沿用之，指中華民國政府目前的治權範圍，相對於「大陸地區」以及疆域內中國共產黨統治區域以外非屬於中華民國政府的統治區域。

## 認知差異

「臺灣地區」一詞的意義，依各方立場解釋不同而有差異。

<table>
<thead>
<tr class="header">
<th><p>國家政權或國際組織</p></th>
<th><p>認知立場</p></th>
<th><p>法源依據</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>兩大政治陣營——<a href="../Page/泛藍.md" title="wikilink">泛藍及</a><a href="../Page/泛綠.md" title="wikilink">泛綠各有不同的解讀</a>。惟目前共識是同意在中華民國一中基礎上以「臺灣地區」處理涉及「大陸地區」的事務。<a href="../Page/泛藍.md" title="wikilink">泛藍陣營認為該用法避談國號</a>，在《<a href="../Page/中華民國憲法.md" title="wikilink">中華民國憲法</a>》中，在中華民國固有領土之內不允許有其他國家的出現，而是以「大陸地區」和「自由地區」的地區之間的關係去面對此問題。「自由地區」指的是台澎金馬地區。基於此原則，大陸地區及臺灣地區同屬<a href="../Page/一個中國.md" title="wikilink">一個中國</a>，即中華民國<a href="../Page/一國兩區.md" title="wikilink">一國兩區</a>，依照《<a href="../Page/兩岸人民關係條例.md" title="wikilink">兩岸人民關係條例</a>》處理兩岸關係。</p></td>
<td><p>《<a href="../Page/中華民國憲法.md" title="wikilink">中華民國憲法</a>》<br />
《<a href="../Page/憲法增修條文.md" title="wikilink">憲法增修條文</a>》</p></td>
<td><p><a href="../Page/泛綠.md" title="wikilink">泛綠陣營傾向認為</a>「中華民國（臺灣）」和「中華人民共和國（大陸）」間是<a href="../Page/一邊一國.md" title="wikilink">一邊一國或</a><a href="../Page/特殊兩國論.md" title="wikilink">兩國關係</a>，認為中華民國的領土僅及於臺澎金馬及其附屬島嶼，主張<a href="../Page/中華民國是台灣.md" title="wikilink">中華民國是台灣</a>，且現在<a href="../Page/中華民國總統.md" title="wikilink">總統</a>、<a href="../Page/中華民國副總統.md" title="wikilink">副總統與</a><a href="../Page/立法院.md" title="wikilink">國會均於臺澎金馬選出</a>，再加上「地區」字樣是對<a href="../Page/臺灣.md" title="wikilink">臺灣的不尊重與自我矮化</a>。然而<a href="../Page/民進黨政府.md" title="wikilink">民進黨政府執政時期仍依照</a>《<a href="../Page/兩岸人民關係條例.md" title="wikilink">兩岸人民關係條例</a>》處理兩岸關係。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>基於<a href="../Page/一個中國原則.md" title="wikilink">一個中國原則</a>，不承認中華民國和<a href="../Page/中華民國政府.md" title="wikilink">中華民國政府的存在及合法性</a>；由此衍生出<a href="../Page/臺灣當局.md" title="wikilink">臺灣當局及</a><a href="../Page/臺灣地區領導人.md" title="wikilink">臺灣地區領導人的特定稱謂</a>。</p></td>
<td><p>《<a href="../Page/中華人民共和國憲法.md" title="wikilink">中華人民共和國憲法</a>》<br />
《<a href="../Page/反分裂國家法.md" title="wikilink">反分裂國家法</a>》</p></td>
<td><p><a href="../Page/中華人民共和國政府.md" title="wikilink">中華人民共和國政府以</a><a href="../Page/國務院臺灣事務辦公室.md" title="wikilink">國務院臺灣事務辦公室來處理</a><a href="../Page/海峽兩岸關係.md" title="wikilink">海峽兩岸關係</a>，與此對應的是中華民國<a href="../Page/大陸委員會.md" title="wikilink">大陸委員會</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>承認（recognizes）<a href="../Page/中華人民共和國政府.md" title="wikilink">中華人民共和國政府是中國的唯一合法政府</a>。認知（acknowledges）中國的立場，即只有一個中國。美國不承認台灣為主權國家[7]，目前在<a href="../Page/世界概況.md" title="wikilink">世界概況中將中國大陸地區和台灣地區以相同顏色標註</a>[8]，但美國從未承認中華人民共和國對台灣的主權[9][10][11]，並反對兩岸任何一方改變現狀。</p></td>
<td><p>《<a href="../Page/中美三個聯合公報.md" title="wikilink">中美三個聯合公報</a>》<br />
《<a href="../Page/與臺灣關係法.md" title="wikilink">與臺灣關係法</a>》<br />
《<a href="../Page/臺灣旅行法.md" title="wikilink">臺灣旅行法</a>》</p></td>
<td><p>美國政府以<a href="../Page/美國在臺協會.md" title="wikilink">美國在臺協會來處理</a><a href="../Page/美臺關係.md" title="wikilink">美臺關係事務</a>，協会成员来自<a href="../Page/美國國務院.md" title="wikilink">美國國務院</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/聯合國安理會.md" title="wikilink">聯合國安理會根據</a><a href="../Page/聯合國大會2758號決議.md" title="wikilink">聯合國大會2758號決議承認中華人民共和國政府是中國唯一合法政府</a>。</p></td>
<td><p>《<a href="../Page/聯合國憲章.md" title="wikilink">聯合國憲章</a>》<br />
《<a href="../Page/聯合國大會2758號決議.md" title="wikilink">聯合國大會2758號決議</a>》</p></td>
<td><p><a href="../Page/聯合國.md" title="wikilink">聯合國將臺灣地區定義為</a><a href="../Page/中國臺灣省.md" title="wikilink">中國臺灣省</a>。</p></td>
</tr>
</tbody>
</table>

## 行政區劃

## 註釋

## 参考文献

## 參見

  - [臺灣島](../Page/臺灣.md "wikilink")：[台灣地理](../Page/台灣地理.md "wikilink")、[台灣歷史](../Page/台灣歷史.md "wikilink")
  - [中華民國](../Page/中華民國.md "wikilink")：[中華民國疆域](../Page/中華民國疆域.md "wikilink")、[中華民國行政區劃](../Page/中華民國行政區劃.md "wikilink")
      - [臺灣省](../Page/臺灣省.md "wikilink")、[福建省
        (中華民國)](../Page/福建省_\(中華民國\).md "wikilink")、[中華民國首都](../Page/中華民國首都.md "wikilink")
      - [中華民國島嶼列表](../Page/中華民國島嶼列表.md "wikilink")：[金門](../Page/金門縣.md "wikilink")、[馬祖](../Page/馬祖列島.md "wikilink")、[東沙群島](../Page/東沙群島.md "wikilink")、[南沙](../Page/南沙群島.md "wikilink")[太平島](../Page/太平島.md "wikilink")
  - [台灣地位未定論](../Page/台灣地位未定論.md "wikilink")、[特殊兩國論](../Page/特殊兩國論.md "wikilink")、[一邊一國](../Page/一邊一國.md "wikilink")、[四階段論](../Page/四階段論.md "wikilink")
  - [台灣問題](../Page/台灣問題.md "wikilink")：[台海現狀](../Page/台海現狀.md "wikilink")、[台灣海峽飛彈危機](../Page/台灣海峽飛彈危機.md "wikilink")
  - [臺澎金馬個別關稅領域](../Page/臺澎金馬個別關稅領域.md "wikilink")
  - 其他[華人地區](../Page/華人地區.md "wikilink")：[中國大陸](../Page/中國大陸.md "wikilink")、[港澳地區](../Page/港澳地區.md "wikilink")

{{-}}

[+](../Category/中華民國行政區劃.md "wikilink")
[Category:臺灣海峽兩岸關係術語](../Category/臺灣海峽兩岸關係術語.md "wikilink")
[Category:中华民国政治术语](../Category/中华民国政治术语.md "wikilink")
[Category:中华人民共和国政治术语](../Category/中华人民共和国政治术语.md "wikilink")

1.
2.
3.
4.  [中華民國政府再次重申對臺澎金馬的主權地位](http://www.mofa.gov.tw/News_Content.aspx?n=8742DCE7A2A28761&s=ABCE429A177E6037)
    - [中華民國外交部](../Page/中華民國外交部.md "wikilink")
5.  [臺閩地區各縣市最近五年營利事業家數增長情形統計表](http://www.fia.gov.tw/public/Attachment/47316113571.pdf)
    - [財政部財政資訊中心](../Page/財政部財政資訊中心.md "wikilink")
6.  [中華民國總統府
    中華民國憲法增修條文](http://www.president.gov.tw/Default.aspx?tabid=65)
7.
8.
9.
10.
11.