**臺灣府淡水廳艋舺縣丞**前身為[臺灣府淡水廳新莊縣丞](../Page/臺灣府淡水廳新莊縣丞.md "wikilink")，隸屬於[臺灣道之](../Page/臺灣道.md "wikilink")[臺灣府](../Page/臺灣府.md "wikilink")，為[臺灣清治時期的重要地方官員](../Page/臺灣清治時期.md "wikilink")，是新莊縣丞於嘉慶十四年（1809年）移駐艋舺後更名而來，為此階段大臺北地區的地方父母官，光緒元年（1875年）時裁撤\[1\]。

1853年[械鬥](../Page/械鬥.md "wikilink")[頂下郊拚後](../Page/頂下郊拚.md "wikilink")，艋舺縣丞署燬

## 歷任

以下所列者為歷任的艋舺縣丞：

<table>
<thead>
<tr class="header">
<th><p>名字</p></th>
<th><p>字號</p></th>
<th><p>籍貫</p></th>
<th><p>出身</p></th>
<th><p>任期（中曆）</p></th>
<th><p>任期（西曆）</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/顧晉.md" title="wikilink">顧晉</a></p></td>
<td></td>
<td><p>浙江錢塘</p></td>
<td><p>監生</p></td>
<td><p>嘉慶十四年－？年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龐周.md" title="wikilink">龐周</a></p></td>
<td></td>
<td><p>江蘇江寧</p></td>
<td><p>監生</p></td>
<td><p>嘉慶十五年－？年</p></td>
<td></td>
<td><p>嘉慶十五年署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/弓清瀚.md" title="wikilink">弓清瀚</a></p></td>
<td></td>
<td><p>山西介休</p></td>
<td><p>監生</p></td>
<td><p>嘉慶十五年－？年</p></td>
<td></td>
<td><p>一作簡清瀚，由沙縣縣丞調署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曹汝霖_(艋舺縣丞).md" title="wikilink">曹汝霖 (艋舺縣丞)</a></p></td>
<td></td>
<td><p>直隸宛平（一作大興）</p></td>
<td><p>議敘從九品</p></td>
<td><p>嘉慶十七年－？年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳鴻寶.md" title="wikilink">陳鴻寶</a></p></td>
<td></td>
<td><p>江蘇江寧</p></td>
<td><p>監生</p></td>
<td><p>嘉慶二十年－二十年</p></td>
<td></td>
<td><p>一作陳鴻賓，嘉慶二十年署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曹汝霖_(艋舺縣丞).md" title="wikilink">曹汝霖 (艋舺縣丞)</a></p></td>
<td></td>
<td><p>直隸宛平（一作大興）</p></td>
<td><p>議敘從九品</p></td>
<td><p>嘉慶二十年－？年</p></td>
<td></td>
<td><p>回任</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/徐鍔.md" title="wikilink">徐鍔</a></p></td>
<td></td>
<td><p>直隸大興</p></td>
<td><p>由內閣供事<br />
議敘從九品</p></td>
<td><p>嘉慶二十一年－？年</p></td>
<td></td>
<td><p>以臺灣府經歷署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/俞森.md" title="wikilink">俞森</a></p></td>
<td></td>
<td><p>直隸清苑</p></td>
<td><p>監生</p></td>
<td><p>嘉慶二十二年－？年</p></td>
<td></td>
<td><p>嘉慶二十二年署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王承烈.md" title="wikilink">王承烈</a></p></td>
<td></td>
<td><p>直隸懷柔</p></td>
<td><p>吏員</p></td>
<td><p>嘉慶二十四年－二十四年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/田光珣.md" title="wikilink">田光珣</a></p></td>
<td></td>
<td><p>山東鉅野</p></td>
<td><p>由監生捐未入流</p></td>
<td><p>嘉慶二十四年－？年</p></td>
<td></td>
<td><p>一作田大珣，以羅漢門巡檢署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王承烈.md" title="wikilink">王承烈</a></p></td>
<td></td>
<td><p>直隸懷柔</p></td>
<td><p>吏員</p></td>
<td><p>嘉慶二十五年－？年</p></td>
<td></td>
<td><p>回任</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/詹英.md" title="wikilink">詹英</a></p></td>
<td></td>
<td><p>直隸宛平</p></td>
<td><p>廩貢（一作廩生）</p></td>
<td><p>道光三年－四年九月十三日</p></td>
<td></td>
<td><p>代理，後調任臺灣典史兼羅漢門巡檢</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/趙秉湘.md" title="wikilink">趙秉湘</a></p></td>
<td></td>
<td><p>直隸大興</p></td>
<td><p>廩貢</p></td>
<td><p>道光四年－？年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張元疆.md" title="wikilink">張元疆</a></p></td>
<td></td>
<td><p>安徽桐城</p></td>
<td><p>由監生捐從九品</p></td>
<td><p>道光七年－？年</p></td>
<td></td>
<td><p>由署羅東巡檢兼署頭圍丞調署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/趙秉湘.md" title="wikilink">趙秉湘</a></p></td>
<td></td>
<td><p>直隸大興</p></td>
<td><p>廩貢</p></td>
<td><p>道光八年－？年</p></td>
<td></td>
<td><p>回任</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔣律武.md" title="wikilink">蔣律武</a></p></td>
<td></td>
<td><p>江西鉛山</p></td>
<td><p>由翰林院供事咨送實錄館書成議敘從九品</p></td>
<td><p>道光十六年－？年</p></td>
<td></td>
<td><p>道光十六年署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/易金杓.md" title="wikilink">易金杓</a></p></td>
<td></td>
<td><p>江蘇儀徵</p></td>
<td><p>由監生捐未入流加捐</p></td>
<td><p>道光十七年－？年</p></td>
<td></td>
<td><p>道光十七年署，一說疑由南安縣丞調</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙秉湘.md" title="wikilink">趙秉湘</a></p></td>
<td></td>
<td><p>直隸大興</p></td>
<td><p>廩貢</p></td>
<td><p>道光十八年－？年</p></td>
<td></td>
<td><p>再任</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宓惟慷.md" title="wikilink">宓惟慷</a></p></td>
<td><p>字健生</p></td>
<td><p>浙江海寧州</p></td>
<td><p>供事</p></td>
<td><p>道光二十一年－二十六年</p></td>
<td></td>
<td><p>一作宓惟康，由將樂典史調署，道光二十四年實任，二十六年升任彰化知縣</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冉正品.md" title="wikilink">冉正品</a></p></td>
<td></td>
<td><p>四川廣元</p></td>
<td><p>吏員</p></td>
<td><p>道光二十六年－二十六年</p></td>
<td></td>
<td><p>署（二十六年正月在任）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馮鳴鶴.md" title="wikilink">馮鳴鶴</a></p></td>
<td></td>
<td><p>江蘇吳縣</p></td>
<td><p>優貢</p></td>
<td><p>道光二十六年－？年</p></td>
<td></td>
<td><p>道光二十六年署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬慶釗.md" title="wikilink">馬慶釗</a></p></td>
<td><p>號敦圃</p></td>
<td><p>四川成都</p></td>
<td><p>由監生捐從九品</p></td>
<td><p>道光二十七年－？年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉榛.md" title="wikilink">劉榛</a></p></td>
<td></td>
<td><p>四川桐梁</p></td>
<td><p>監生</p></td>
<td><p>道光二十八年－？年</p></td>
<td></td>
<td><p>道光二十八年署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬慶釗.md" title="wikilink">馬慶釗</a></p></td>
<td><p>號敦圃</p></td>
<td><p>四川成都</p></td>
<td><p>由監生捐從九品</p></td>
<td><p>道光二十九年－？年</p></td>
<td></td>
<td><p>回任</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬克惇.md" title="wikilink">馬克惇</a></p></td>
<td><p>號雲伯</p></td>
<td><p>直隸大興（原籍陝西）</p></td>
<td><p>由優生揀選鴻臚寺，序班官遵例就職縣丞</p></td>
<td><p>？年－？年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳灃.md" title="wikilink">陳灃</a></p></td>
<td></td>
<td><p>浙江山陰</p></td>
<td><p>由監生捐從九品加捐分發</p></td>
<td><p>咸豐八年十一月初四－？年</p></td>
<td></td>
<td><p>一作陳澧，由署下淡水縣丞調任，咸豐十年三月時仍在任</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郭志緯.md" title="wikilink">郭志緯</a></p></td>
<td></td>
<td><p>浙江山陰</p></td>
<td></td>
<td><p>同治元年四月－？年</p></td>
<td></td>
<td><p>同治元年四月署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/樊炳文.md" title="wikilink">樊炳文</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>同治二年－？年</p></td>
<td></td>
<td><p>同治二年署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張國楷.md" title="wikilink">張國楷</a></p></td>
<td></td>
<td><p>江西南昌</p></td>
<td><p>由監生捐從九品</p></td>
<td><p>同治四年－四年十二月前</p></td>
<td></td>
<td><p>同治四年署，該年十二月前調署噶瑪蘭通判</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/葉駿.md" title="wikilink">葉駿</a></p></td>
<td></td>
<td><p>安徽</p></td>
<td></td>
<td><p>同治四年－？年</p></td>
<td></td>
<td><p>代理</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張國楷.md" title="wikilink">張國楷</a></p></td>
<td></td>
<td><p>江西南昌</p></td>
<td><p>由監生捐從九品</p></td>
<td><p>同治五年－五年</p></td>
<td></td>
<td><p>再署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏允達.md" title="wikilink">夏允達</a></p></td>
<td></td>
<td><p>湖南益陽</p></td>
<td><p>從九品</p></td>
<td><p>同治五年－？年</p></td>
<td></td>
<td><p>同治五年署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/章國均.md" title="wikilink">章國均</a></p></td>
<td></td>
<td><p>湖南沅陵</p></td>
<td><p>監生</p></td>
<td><p>同治六年－？年</p></td>
<td></td>
<td><p>同治六年署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李守諧.md" title="wikilink">李守諧</a></p></td>
<td></td>
<td><p>江西宜黃</p></td>
<td><p>監生</p></td>
<td><p>同治七年－？年</p></td>
<td></td>
<td><p>同治七年署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林桂芬.md" title="wikilink">林桂芬</a></p></td>
<td></td>
<td><p>廣東番禺</p></td>
<td><p>由監生投效軍營，剿辦廣西艇匪並克復梧州城，出力保舉以從九品用</p></td>
<td><p>同治八年－八年</p></td>
<td></td>
<td><p>又作林桂芳、李桂芬，同治八年署</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄒祖壽.md" title="wikilink">鄒祖壽</a></p></td>
<td></td>
<td><p>廣東番禺</p></td>
<td><p>監生</p></td>
<td><p>同治八年－？年</p></td>
<td></td>
<td><p>同治八年署</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傅端銓.md" title="wikilink">傅端銓</a></p></td>
<td></td>
<td><p>浙江山陰</p></td>
<td><p>監生</p></td>
<td><p>同治九年十二月廿四－光緒四年秋</p></td>
<td></td>
<td><p>由准補建陽縣丞調任，後升任彰化知縣</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王斌林.md" title="wikilink">王斌林</a></p></td>
<td><p>字雅堂</p></td>
<td><p>安徽</p></td>
<td><p>從九品</p></td>
<td><p>？年－？年</p></td>
<td></td>
<td><p>據稱，曾任「新莊分縣」，疑為「艋舺縣丞」之雅稱，任期不詳，約在同治光緒之間</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考文獻

[分類:艋舺縣丞](../Page/分類:艋舺縣丞.md "wikilink")

[Category:台灣清治時期官職](../Category/台灣清治時期官職.md "wikilink")

1.  劉寧顏編，《重修台灣省通志》，臺北市，台灣省文獻委員會，1994年。