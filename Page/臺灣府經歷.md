**臺灣府經歷**是[臺灣清治時期的地方官員](../Page/臺灣清治時期.md "wikilink")，[經歷](../Page/經歷.md "wikilink")，配置於[臺灣府](../Page/臺灣府.md "wikilink")，品級為正八品，掌管府衙出納、文移與內務，員額一人。

## 歷任

| \#  | 姓名                                      | 任職日期(西曆)                     | 任職日期(中曆)                                  |
| --- | --------------------------------------- | ---------------------------- | ----------------------------------------- |
| 1   | [林起元](../Page/林起元.md "wikilink")        | align ="center"|<small>1684年 | <small>[康熙二十三年](../Page/康熙.md "wikilink") |
| 2   | [方逢月](../Page/方逢月.md "wikilink")        | align ="center"|<small>1687年 | <small>[康熙二十六年](../Page/康熙.md "wikilink") |
| 3   | [王道弘](../Page/王道弘.md "wikilink")        | align ="center"|<small>1689年 | <small>[康熙二十八年](../Page/康熙.md "wikilink") |
| 4   | [尹復](../Page/尹復.md "wikilink")          | align ="center"|<small>1694年 | <small>[康熙三十三年](../Page/康熙.md "wikilink") |
| 5   | [孫琰](../Page/孫琰.md "wikilink")          | align ="center"|<small>1698年 | <small>[康熙三十七年](../Page/康熙.md "wikilink") |
| 6   | [汪元任](../Page/汪元任.md "wikilink")        | align ="center"|<small>1703年 | <small>[康熙四十二年](../Page/康熙.md "wikilink") |
| 7   | [張天銓](../Page/張天銓.md "wikilink")        | align ="center"|<small>1707年 | <small>[康熙四十六年](../Page/康熙.md "wikilink") |
| 8   | [陶宣](../Page/陶宣.md "wikilink")          | align ="center"|<small>1711年 | <small>[康熙五十年](../Page/康熙.md "wikilink")  |
| 9   | [王士勷](../Page/王士勷.md "wikilink")        | align ="center"|<small>1716年 | <small>[康熙五十五年](../Page/康熙.md "wikilink") |
| 10  | [左懋源](../Page/左懋源.md "wikilink")        | align ="center"|<small>1721年 | <small>[康熙六十年](../Page/康熙.md "wikilink")  |
| 11  | [褚天緯](../Page/褚天緯.md "wikilink")        | align ="center"|<small>1729年 | <small>[雍正七年](../Page/雍正.md "wikilink")   |
| 12  | [郭士謙](../Page/郭士謙.md "wikilink")        | align ="center"|<small>1733年 | <small>[雍正十一年](../Page/雍正.md "wikilink")  |
| 13  | [王嗣彥](../Page/王嗣彥.md "wikilink")        | align ="center"|<small>1735年 | <small>[雍正十三年](../Page/雍正.md "wikilink")  |
| 14  | [朱士顯](../Page/朱士顯.md "wikilink")        | align ="center"|<small>1739年 | <small>[乾隆四年](../Page/乾隆.md "wikilink")   |
| 15  | [金文英](../Page/金文英.md "wikilink")        | align ="center"|<small>1742年 | <small>[乾隆七年](../Page/乾隆.md "wikilink")   |
| 16  | [葛舜有](../Page/葛舜有.md "wikilink")        | align ="center"|<small>1746年 | <small>[乾隆十一年](../Page/乾隆.md "wikilink")  |
| 17  | [王如璋](../Page/王如璋.md "wikilink")        | align ="center"|<small>1748年 | <small>[乾隆十三年](../Page/乾隆.md "wikilink")  |
| 18  | [郝敬修](../Page/郝敬修.md "wikilink")        | align ="center"|<small>1751年 | <small>[乾隆十六年](../Page/乾隆.md "wikilink")  |
| 19  | [包瀜](../Page/包瀜.md "wikilink")          | align ="center"|<small>1751年 | <small>[乾隆十六年](../Page/乾隆.md "wikilink")  |
| 20  | [曹煜](../Page/曹煜_\(清朝\).md "wikilink")   | align ="center"|<small>1751年 | <small>[乾隆十六年](../Page/乾隆.md "wikilink")  |
| 21  | [吳克成](../Page/吳克成.md "wikilink")        | align ="center"|<small>1754年 | <small>[乾隆十九年](../Page/乾隆.md "wikilink")  |
| 22  | [沈鈺](../Page/沈鈺.md "wikilink")          | align ="center"|<small>1754年 | <small>[乾隆十九年](../Page/乾隆.md "wikilink")  |
| 23  | [耳孔木](../Page/耳孔木.md "wikilink")        | align ="center"|<small>1755年 | <small>[乾隆二十年](../Page/乾隆.md "wikilink")  |
| 24  | [宓宏謨](../Page/宓宏謨.md "wikilink")        | align ="center"|<small>1755年 | <small>[乾隆二十年](../Page/乾隆.md "wikilink")  |
| 25  | [沈鈺](../Page/沈鈺.md "wikilink")          | align ="center"|<small>1756年 | <small>[乾隆二十一年](../Page/乾隆.md "wikilink") |
| 26  | [吳紘](../Page/吳紘.md "wikilink")          |                              |                                           |
| 27  | [裘建功](../Page/裘建功.md "wikilink")        | align ="center"|<small>1761年 | <small>[乾隆二十六年](../Page/乾隆.md "wikilink") |
| 28  | [孫玉書](../Page/孫玉書.md "wikilink")        | align ="center"|<small>1764年 | <small>[乾隆二十九年](../Page/乾隆.md "wikilink") |
| 29  | [汪朝棟](../Page/汪朝棟.md "wikilink")        | align ="center"|<small>1767年 | <small>[乾隆三十二年](../Page/乾隆.md "wikilink") |
| 30  | [李際時](../Page/李際時.md "wikilink")        |                              |                                           |
| 31  | [王執禮](../Page/王執禮.md "wikilink")        | align ="center"|<small>1769年 | <small>[乾隆三十四年](../Page/乾隆.md "wikilink") |
| 32  | [謝洪光](../Page/謝洪光.md "wikilink")        | align ="center"|<small>1772年 | <small>[乾隆三十七年](../Page/乾隆.md "wikilink") |
| 33  | [李棠](../Page/李棠.md "wikilink")          | align ="center"|<small>1773年 | <small>[乾隆三十八年](../Page/乾隆.md "wikilink") |
| 34  | [葉起蛟](../Page/葉起蛟.md "wikilink")        | align ="center"|<small>1775年 | <small>[乾隆四十年](../Page/乾隆.md "wikilink")  |
| 35  | [張東馨](../Page/張東馨.md "wikilink")        | align ="center"|<small>1778年 | <small>[乾隆四十三年](../Page/乾隆.md "wikilink") |
| 36  | [嚴榕照](../Page/嚴榕照.md "wikilink")        | align ="center"|<small>1782年 | <small>[乾隆四十七年](../Page/乾隆.md "wikilink") |
| 37  | [羅倫](../Page/羅倫.md "wikilink")          | align ="center"|<small>1786年 | <small>[乾隆五十一年](../Page/乾隆.md "wikilink") |
| 38  | [周書鳳](../Page/周書鳳.md "wikilink")        | align ="center"|<small>1788年 | <small>[乾隆五十三年](../Page/乾隆.md "wikilink") |
| 39  | [朱瀾](../Page/朱瀾.md "wikilink")          | align ="center"|<small>1789年 | <small>[乾隆五十四年](../Page/乾隆.md "wikilink") |
| 40  | [周書鳳](../Page/周書鳳.md "wikilink")        | align ="center"|<small>1791年 | <small>[乾隆五十六年](../Page/乾隆.md "wikilink") |
| 41  | [魯岱](../Page/魯岱.md "wikilink")          | align ="center"|<small>1792年 | <small>[乾隆五十七年](../Page/乾隆.md "wikilink") |
| 42  | [李傳樁](../Page/李傳樁.md "wikilink")        | align ="center"|<small>1793年 | <small>[乾隆五十八年](../Page/乾隆.md "wikilink") |
| 43  | [李棟](../Page/李棟.md "wikilink")          |                              |                                           |
| 44  | [曹德裕](../Page/曹德裕.md "wikilink")        | align ="center"|<small>1794年 | <small>[乾隆五十九年](../Page/乾隆.md "wikilink") |
| 45  | [彭思本](../Page/彭思本.md "wikilink")        | align ="center"|<small>1795年 | <small>[乾隆六十年](../Page/乾隆.md "wikilink")  |
| 46  | [陳塤](../Page/陳塤.md "wikilink")          | align ="center"|<small>1796年 | <small>[嘉慶元年](../Page/嘉慶.md "wikilink")   |
| 47  | [張力恕](../Page/張力恕.md "wikilink")        |                              |                                           |
| 48  | [任元壆](../Page/任元壆.md "wikilink")        | align ="center"|<small>1799年 | <small>[嘉慶四年](../Page/嘉慶.md "wikilink")   |
| 49  | [包德墉](../Page/包德墉.md "wikilink")        | align ="center"|<small>1799年 | <small>[嘉慶四年](../Page/嘉慶.md "wikilink")   |
| 50  | [翟灝](../Page/翟灝.md "wikilink")          | align ="center"|<small>1800年 | <small>[嘉慶五年](../Page/嘉慶.md "wikilink")   |
| 51  | [包德墉](../Page/包德墉.md "wikilink")        | align ="center"|<small>1801年 | <small>[嘉慶六年](../Page/嘉慶.md "wikilink")   |
| 52  | [蘇鴻](../Page/蘇鴻.md "wikilink")          |                              |                                           |
| 53  | [李雲龍](../Page/李雲龍_\(臺灣\).md "wikilink") | align ="center"|<small>1803年 | <small>[嘉慶八年](../Page/嘉慶.md "wikilink")   |
| 54  | [呂聖宗](../Page/呂聖宗.md "wikilink")        | align ="center"|<small>1806年 | <small>[嘉慶十一年](../Page/嘉慶.md "wikilink")  |
| 55  | [袁錫山](../Page/袁錫山.md "wikilink")        | align ="center"|<small>1806年 | <small>[嘉慶十一年](../Page/嘉慶.md "wikilink")  |
| 56  | [李雲龍](../Page/李雲龍_\(臺灣\).md "wikilink") | align ="center"|<small>1808年 | <small>[嘉慶十三年](../Page/嘉慶.md "wikilink")  |
| 57  | [徐文尉](../Page/徐文尉.md "wikilink")        | align ="center"|<small>1811年 | <small>[嘉慶十六年](../Page/嘉慶.md "wikilink")  |
| 58  | [李雲龍](../Page/李雲龍_\(臺灣\).md "wikilink") | align ="center"|<small>1811年 | <small>[嘉慶十六年](../Page/嘉慶.md "wikilink")  |
| 59  | [徐延俊](../Page/徐延俊.md "wikilink")        | align ="center"|<small>1812年 | <small>[嘉慶十七年](../Page/嘉慶.md "wikilink")  |
| 60  | [李雲龍](../Page/李雲龍_\(臺灣\).md "wikilink") | align ="center"|<small>1812年 | <small>[嘉慶十七年](../Page/嘉慶.md "wikilink")  |
| 61  | [沈日源](../Page/沈日源.md "wikilink")        | align ="center"|<small>1813年 | <small>[嘉慶十八年](../Page/嘉慶.md "wikilink")  |
| 62  | [宋楘](../Page/宋楘.md "wikilink")          | align ="center"|<small>1814年 | <small>[嘉慶十九年](../Page/嘉慶.md "wikilink")  |
| 63  | [徐延俊](../Page/徐延俊.md "wikilink")        | align ="center"|<small>1814年 | <small>[嘉慶十九年](../Page/嘉慶.md "wikilink")  |
| 64  | [李芸](../Page/李芸.md "wikilink")          | align ="center"|<small>1814年 | <small>[嘉慶十九年](../Page/嘉慶.md "wikilink")  |
| 65  | [徐鍔](../Page/徐鍔.md "wikilink")          | align ="center"|<small>1815年 | <small>[嘉慶二十年](../Page/嘉慶.md "wikilink")  |
| 66  | [葉時秀](../Page/葉時秀.md "wikilink")        |                              |                                           |
| 67  | [徐鍔](../Page/徐鍔.md "wikilink")          | align ="center"|<small>1820年 | <small>[嘉慶二十五年](../Page/嘉慶.md "wikilink") |
| 68  | [劉蔭棠](../Page/劉蔭棠.md "wikilink")        | align ="center"|<small>1821年 | <small>[道光元年](../Page/道光.md "wikilink")   |
| 69  | [王惟馨](../Page/王惟馨.md "wikilink")        | align ="center"|<small>1823年 | <small>[道光三年](../Page/道光.md "wikilink")   |
| 70  | [熊飛](../Page/熊飛.md "wikilink")          | align ="center"|<small>1826年 | <small>[道光六年](../Page/道光.md "wikilink")   |
| 71  | [王惟馨](../Page/王惟馨.md "wikilink")        | align ="center"|<small>1826年 | <small>[道光六年](../Page/道光.md "wikilink")   |
| 72  | [俞國楨](../Page/俞國楨.md "wikilink")        | align ="center"|<small>1826年 | <small>[道光六年](../Page/道光.md "wikilink")   |
| 73  | [沈日源](../Page/沈日源.md "wikilink")        | align ="center"|<small>1826年 | <small>[道光六年](../Page/道光.md "wikilink")   |
| 74  | [俞國楨](../Page/俞國楨.md "wikilink")        | align ="center"|<small>1827年 | <small>[道光七年](../Page/道光.md "wikilink")   |
| 75  | [宗覲庭](../Page/宗覲庭.md "wikilink")        | align ="center"|<small>1828年 | <small>[道光八年](../Page/道光.md "wikilink")   |
| 76  | [陳文起](../Page/陳文起.md "wikilink")        | align ="center"|<small>1828年 | <small>[道光八年](../Page/道光.md "wikilink")   |
| 77  | [張師孚](../Page/張師孚.md "wikilink")        | align ="center"|<small>1830年 | <small>[道光十年](../Page/道光.md "wikilink")   |
| 78  | [魏彥儀](../Page/魏彥儀.md "wikilink")        | align ="center"|<small>1835年 | <small>[道光十五年](../Page/道光.md "wikilink")  |
| 79  | [陳塤](../Page/陳塤.md "wikilink")          | align ="center"|<small>1841年 | <small>[道光二十一年](../Page/道光.md "wikilink") |
| 80  | [屠本](../Page/屠本.md "wikilink")          | align ="center"|<small>1843年 | <small>[道光二十三年](../Page/道光.md "wikilink") |
| 81  | [張德琇](../Page/張德琇.md "wikilink")        | align ="center"|<small>1845年 | <small>[道光二十五年](../Page/道光.md "wikilink") |
| 82  | [費霖](../Page/費霖.md "wikilink")          | align ="center"|<small>1851年 | <small>[咸豐元年](../Page/咸豐.md "wikilink")   |
| 83  | [張德琇](../Page/張德琇.md "wikilink")        | align ="center"|<small>1852年 | <small>[咸豐二年](../Page/咸豐.md "wikilink")   |
| 84  | [徐繼穀](../Page/徐繼穀.md "wikilink")        | align ="center"|<small>1859年 | <small>[咸豐九年](../Page/咸豐.md "wikilink")   |
| 85  | [王沖汶](../Page/王沖汶.md "wikilink")        | align ="center"|<small>1860年 | <small>[咸豐十年](../Page/咸豐.md "wikilink")   |
| 86  | [管綸](../Page/管綸.md "wikilink")          | align ="center"|<small>1862年 | <small>[同治元年](../Page/同治.md "wikilink")   |
| 87  | [周焜](../Page/周焜.md "wikilink")          | align ="center"|<small>1867年 | <small>[同治六年](../Page/同治.md "wikilink")   |
| 88  | [管綸](../Page/管綸.md "wikilink")          | align ="center"|<small>1868年 | <small>[同治七年](../Page/同治.md "wikilink")   |
| 89  | [梁獄鍾](../Page/梁獄鍾.md "wikilink")        | align ="center"|<small>1868年 | <small>[同治七年](../Page/同治.md "wikilink")   |
| 90  | [張儒績](../Page/張儒績.md "wikilink")        | align ="center"|<small>1868年 | <small>[同治七年](../Page/同治.md "wikilink")   |
| 91  | [黃延昭](../Page/黃延昭.md "wikilink")        | align ="center"|<small>1869年 | <small>[同治八年](../Page/同治.md "wikilink")   |
| 92  | [方學李](../Page/方學李.md "wikilink")        | align ="center"|<small>1876年 | <small>[光緒二年](../Page/光緒.md "wikilink")   |
| 93  | [凌汝曾](../Page/凌汝曾.md "wikilink")        | align ="center"|<small>1877年 | <small>[光緒三年](../Page/光緒.md "wikilink")   |
| 94  | [金聯](../Page/金聯.md "wikilink")          | align ="center"|<small>1879年 | <small>[光緒五年](../Page/光緒.md "wikilink")   |
| 95  | [周志侃](../Page/周志侃.md "wikilink")        | align ="center"|<small>1879年 | <small>[光緒五年](../Page/光緒.md "wikilink")   |
| 96  | [平廷熊](../Page/平廷熊.md "wikilink")        | align ="center"|<small>1883年 | <small>[光緒九年](../Page/光緒.md "wikilink")   |
| 97  | [邱瓊章](../Page/邱瓊章.md "wikilink")        | align ="center"|<small>1885年 | <small>[光緒十一年](../Page/光緒.md "wikilink")  |
| 98  | [陳世烈](../Page/陳世烈.md "wikilink")        | align ="center"|<small>1886年 | <small>[光緒十二年](../Page/光緒.md "wikilink")  |
| 99  | [王守誠](../Page/王守誠.md "wikilink")        | align ="center"|<small>1887年 | <small>[光緒十三年](../Page/光緒.md "wikilink")  |
| 100 | [林桂芬](../Page/林桂芬.md "wikilink")        | align ="center"|<small>1888年 | <small>[光緒十四年](../Page/光緒.md "wikilink")  |
| 101 | [陳世烈](../Page/陳世烈.md "wikilink")        | align ="center"|<small>1889年 | <small>[光緒十五年](../Page/光緒.md "wikilink")  |
| 102 | [李烇](../Page/李烇.md "wikilink")          | align ="center"|<small>1889年 | <small>[光緒十五年](../Page/光緒.md "wikilink")  |
| 103 | [林桂芬](../Page/林桂芬.md "wikilink")        | align ="center"|<small>1891年 | <small>[光緒十七年](../Page/光緒.md "wikilink")  |

後[臺灣邁入](../Page/臺灣.md "wikilink")[日治時期](../Page/臺灣日治時期.md "wikilink")，臺灣府經歷一職裁撤。

## 參考文獻

  - 劉寧顏編，《重修臺灣省通志》，臺北市，臺灣省文獻委員會，1994年。

[台灣府經歷](../Category/台灣府經歷.md "wikilink")
[Category:台灣清治時期官職](../Category/台灣清治時期官職.md "wikilink")