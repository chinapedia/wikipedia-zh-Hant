**臺灣省**（），簡稱「**臺**」，是[中華民國已](../Page/中華民國.md "wikilink")[虛級化的](../Page/精省.md "wikilink")[省份](../Page/省_\(中華民國\).md "wikilink")，全省劃分為11[縣](../Page/縣_\(中華民國\).md "wikilink")、3[市](../Page/市_\(中華民國\).md "wikilink")，範圍為[臺灣本島](../Page/臺灣.md "wikilink")[六都](../Page/六都.md "wikilink")以外的地區、[澎湖群島](../Page/澎湖群島.md "wikilink")，以及鄰近臺灣本島之離島。全省面積佔中華民國[實際控制國土面積的](../Page/臺灣地區.md "wikilink")69.38%，而人口則佔總人口的30.68%\[1\]。[彰化縣為該省第一大縣](../Page/彰化縣.md "wikilink")、[新竹市為該省第一大市](../Page/新竹市.md "wikilink")\[2\]。

臺灣之建省，可追溯至[清朝光緒](../Page/清朝.md "wikilink")13年（1887年）成立的[福建臺灣省](../Page/福建臺灣省.md "wikilink")。中華民國臺灣省於1945年（民國34年）設立時，其範圍包含了臺灣全島及離島與[澎湖列島](../Page/澎湖列島.md "wikilink")。因臺灣省與1949年[第二次國共內戰之後中央政府實際控制領土大部份重疊](../Page/第二次國共內戰.md "wikilink")。故在1998年12月20日起實行[臺灣省政府](../Page/臺灣省政府.md "wikilink")[功能業務與組織調整](../Page/臺灣省政府功能業務與組織調整.md "wikilink")，省政府而成為[中央政府的派出機關](../Page/中華民國政府.md "wikilink")，原省政府大部份業務移交中央及[縣](../Page/縣_\(中華民國\).md "wikilink")[市](../Page/市_\(中華民國\).md "wikilink")。2018年6月28日，[行政院決議將臺灣省政府去任務化](../Page/行政院.md "wikilink")\[3\]，自翌年起省級機關預算歸零，所存省級行政組織及業務自同年7月1日完全移交中央政府，[臺灣省政府實質上虛級化](../Page/臺灣省政府.md "wikilink")。

## 行政隸屬

自1945年日本戰敗後，[中華民國政府即開始管轄臺灣](../Page/中華民國政府.md "wikilink")。1949年[國共內戰後](../Page/國共內戰.md "wikilink")，中華民國政府從中國大陸退至[臺灣地區](../Page/臺灣地區.md "wikilink")，[臺灣海峽](../Page/臺灣海峽.md "wikilink")[兩岸進入分立分治狀態](../Page/海峽兩岸.md "wikilink")。中華民國政府在1991年前將中國大陸的「[中華人民共和國政府](../Page/中華人民共和國政府.md "wikilink")」視為叛亂團體；而「中華人民共和國政府」則從1949年成立至今對從未管轄過的臺灣宣稱擁有主權。

[行政院之下設立臺灣省政府](../Page/行政院.md "wikilink")，並指派省政府主席，期間於1994－1998年設一任民選[臺灣省省長](../Page/臺灣省省長.md "wikilink")。現在行政規劃上已不屬臺灣省的[臺北市](../Page/臺北市.md "wikilink")、[新北市](../Page/新北市.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")、[臺中市](../Page/臺中市.md "wikilink")、[臺南市和](../Page/臺南市.md "wikilink")[高雄市則改制為直轄市](../Page/高雄市.md "wikilink")\[4\]\[5\]。

## 歷史

遠古時代，因[地殼運動](../Page/造山運動.md "wikilink")，[歐亞板塊與](../Page/歐亞板塊.md "wikilink")[菲律賓海板塊的擠壓造生](../Page/菲律賓海板塊.md "wikilink")，出現[臺灣島](../Page/臺灣島.md "wikilink")。

[臺灣原住民族是已知最早居住於臺灣的現代人類](../Page/臺灣原住民族.md "wikilink")，其各部族散居臺灣，並未建立統一的治理機構。1971年和1974年，在[臺南縣左鎮鄉](../Page/臺南縣.md "wikilink")(今[臺南市](../Page/臺南市.md "wikilink")[左鎮區](../Page/左鎮區.md "wikilink"))發現臺灣目前最早的人類[化石](../Page/化石.md "wikilink")，命名為「[左鎮人](../Page/左鎮人.md "wikilink")」。還有少部屬於[尼格利陀人種的](../Page/尼格利陀人.md "wikilink")[矮黑人和屬於](../Page/矮黑人.md "wikilink")[琉球人種的](../Page/琉球人.md "wikilink")[琅嶠人](../Page/琅嶠人.md "wikilink")。

有文字記載疑似臺灣的歷史可追溯到東吳[黃龍](../Page/黃龍_\(東吳\).md "wikilink")2年（西元230年），[吳人](../Page/孫吳.md "wikilink")[沈瑩的](../Page/沈瑩.md "wikilink")《[臨海水土志](../Page/臨海水土志.md "wikilink")》留下對臺灣最早的記述。[三國](../Page/三國.md "wikilink")[東吳](../Page/孫吳.md "wikilink")[孫權派兵到](../Page/孫權.md "wikilink")「[夷洲](../Page/夷洲.md "wikilink")」。[隋](../Page/隋朝.md "wikilink")[唐時期](../Page/唐朝.md "wikilink")（西元589年—907年）似稱臺灣為「[流求](../Page/流求.md "wikilink")」。隋朝大業六年（西元610年）[隋煬帝派陳稜](../Page/隋煬帝.md "wikilink")、張鄭州擊流求。[宋](../Page/宋朝.md "wikilink")[元時期](../Page/元朝.md "wikilink")（西元960年—1368年），漢族於澎湖地區已開始有定居人口。西元12世紀中葉，宋朝將澎湖劃歸[福建](../Page/福建.md "wikilink")[泉州](../Page/泉州.md "wikilink")[晉江縣管轄](../Page/晉江縣.md "wikilink")，並派兵戌守\[6\]。元、明兩朝政府在澎湖設[巡檢司](../Page/巡檢司.md "wikilink")。[明朝的](../Page/明朝.md "wikilink")[福建和](../Page/福建.md "wikilink")[鄭芝龍集團曾有組織地移民臺灣](../Page/鄭芝龍.md "wikilink")。[廈門自古有](../Page/廈門.md "wikilink")“扼臺灣之要，為東南門戶”之稱，人民密切關係與往來。臺灣人民70%的祖籍地源於[閩南地區](../Page/閩南.md "wikilink")，因此臺灣人日常生活中時常使用[閩南語](../Page/臺灣閩南語.md "wikilink")，[閩西](../Page/閩西.md "wikilink")、[粵東移民則多用](../Page/粵東.md "wikilink")[客家語](../Page/臺灣客家語.md "wikilink")。16世紀，[西班牙](../Page/西班牙.md "wikilink")、[荷蘭等歐洲列強](../Page/荷蘭.md "wikilink")，開始把觸角伸向亞洲。

### 早期歷史

[臺灣本島出現政權可追溯到](../Page/臺灣.md "wikilink")17世紀。當時，臺灣中部有一超部落的[大肚王國](../Page/大肚王國.md "wikilink")。而荷蘭及西班牙則分別在臺灣西南部及西北部進行統治。1642年荷蘭人派艦[攻佔雞籠](../Page/雞籠之戰.md "wikilink")，將西班牙人趕走，統治臺灣西部的大部分。1661年，南臺灣由[鄭氏王朝统治](../Page/明鄭.md "wikilink")，設置[承天府](../Page/承天府_\(臺灣\).md "wikilink")，管轄天興縣與萬年縣。1662年，[鄭經於明昭宗死後](../Page/鄭經.md "wikilink")，改東都為東寧。1664年至1683年之間，鄭經將原屬承天府轄下的兩縣則升格為天興州與萬年州，並增設澎湖、南路與北路三個安撫司，形成「一府二州三司」六個層級平行的行政單位。

1683年，[鄭克塽歸順清朝](../Page/鄭克塽.md "wikilink")，次年，[康熙帝設立](../Page/康熙帝.md "wikilink")「臺灣府」，隸屬於[福建省](../Page/福建省_\(清\).md "wikilink")。清[光緒年間因英法列強先後覬覦臺灣](../Page/光緒.md "wikilink")，[閩浙總督上書朝廷](../Page/閩浙總督.md "wikilink")，要求福建與臺灣兩地「巡撫分駐」、「建省分治」。於1885年[慈禧太后下旨創建海軍的同時](../Page/慈禧太后.md "wikilink")，下詔閩臺分治；9月5日，宣布臺灣建省，但建省行政工作至1887年才完成，正式名稱則為[福建臺灣省](../Page/福建臺灣省.md "wikilink")\[7\]，首任[臺灣巡撫](../Page/臺灣巡撫.md "wikilink")（西元1887年更名為福建臺灣巡撫）為[劉銘傳](../Page/劉銘傳.md "wikilink")。臺灣省下設三府（[臺北府](../Page/臺北府.md "wikilink")、[臺灣府](../Page/臺灣府.md "wikilink")、[臺南府](../Page/臺南府.md "wikilink")），以及1直隸州（[臺東直隸州](../Page/臺東直隸州.md "wikilink")）。三府下總轄11縣（[宜蘭縣](../Page/宜蘭縣_\(清朝\).md "wikilink")、[淡水縣](../Page/淡水縣.md "wikilink")、[新竹縣](../Page/新竹縣_\(清朝\).md "wikilink")、[苗栗縣](../Page/苗栗縣.md "wikilink")、[臺中縣](../Page/臺灣縣_\(1887年-1895年\).md "wikilink")、[彰化縣](../Page/彰化縣_\(清朝\).md "wikilink")、[雲林縣](../Page/雲林縣_\(清朝\).md "wikilink")、[諸羅縣](../Page/諸羅縣.md "wikilink")、[安平縣](../Page/臺灣縣_\(1684年-1887年\).md "wikilink")、[鳳山縣](../Page/鳳山縣_\(臺灣\).md "wikilink")、[恆春縣](../Page/恆春縣.md "wikilink")），4廳（[基隆廳](../Page/基隆廳.md "wikilink")、[南雅廳](../Page/南雅廳.md "wikilink")、[埔-{里}-社廳](../Page/埔里社廳.md "wikilink")、[澎湖廳](../Page/澎湖廳.md "wikilink")）。

1894年，大清與日本發生甲午戰爭，翌年大清戰敗，於4月17日簽訂《馬關條約》，把臺灣及澎湖割讓給日本，成為日本的「[外地](../Page/外地_\(大日本帝國\).md "wikilink")」。初期，[丘逢甲倡議的](../Page/丘逢甲.md "wikilink")[臺灣民主國曾短暫成立](../Page/臺灣民主國.md "wikilink")，以對抗日本統治，但不久後即宣告滅亡。從此，臺灣成為日本的[殖民地達](../Page/台灣日治時期.md "wikilink")50年之久。日本統治早期承襲清朝區劃
，中期後改劃為二十廳，後整併為十二廳，後期則將臺灣劃分為五州三廳（臺北州、新竹州、臺中州、臺南州、高雄州、花蓮港廳、臺東廳、澎湖廳）。先後採用[特別統治主義](../Page/特別統治主義.md "wikilink")、[內地延長主義以及](../Page/內地延長主義.md "wikilink")[皇民化運動等政策統治臺灣](../Page/皇民化運動.md "wikilink")。

1937年，[七七事變後](../Page/七七事變.md "wikilink")，中國展開[八年抗戰](../Page/八年抗戰.md "wikilink")，期間一些臺灣人加入日軍成為軍中雜役，極少數人則赴中國大陸參加[國民革命軍](../Page/國民革命軍.md "wikilink")。1941年[太平洋戰爭爆發後](../Page/太平洋戰爭.md "wikilink")，12月9日中國政府發出《中國對日宣戰佈告》，明確昭告中外：「所有一切條約、協定、合同，有涉及中日之間關係者，一律廢止」。

### 戰後時期

1945年，[第二次世界大戰結束後](../Page/第二次世界大戰.md "wikilink")，盟軍最高統帥[麥克阿瑟元帥發佈](../Page/麥克阿瑟.md "wikilink")《[一般命令第一號](../Page/一般命令第一號.md "wikilink")》，命令在中国、臺灣、澎湖及越南北部的日軍向[同盟國之代表人](../Page/同盟國_\(第二次世界大戰\).md "wikilink")[蔣中正投降](../Page/蔣中正.md "wikilink")。8月31日，[中華民國國民政府頒布](../Page/中華民國國民政府.md "wikilink")《[臺灣省行政長官公署組織大綱](../Page/臺灣省行政長官公署組織大綱.md "wikilink")》，隔日[臺灣省行政長官公署在](../Page/臺灣省行政長官公署.md "wikilink")[重慶成立臨時辦公處](../Page/重慶.md "wikilink")；9月20日公布《[臺灣省行政長官公署組織條例](../Page/臺灣省行政長官公署組織條例.md "wikilink")》；10月25日臺灣省行政長官公署開始在臺灣運作，
國民政府將臺灣設置為中華民國的一省。臺灣省行政長官公署將臺灣劃分行政區為8[縣](../Page/縣_\(中華民國\).md "wikilink")（由日治時期的五州三廳改制）、9個[省轄市](../Page/市_\(中華民國\).md "wikilink")（由日治時期的[州轄市改制](../Page/臺灣市制.md "wikilink")）。1947年發生[二二八事件](../Page/二二八事件.md "wikilink")，臺灣省行政長官公署改組為[臺灣省政府](../Page/臺灣省政府.md "wikilink")。隨着[第二次國共內戰](../Page/第二次國共內戰.md "wikilink")[中國國民黨的失利](../Page/中國國民黨.md "wikilink")，1949年，[行政院決議將中央政府遷往臺灣本島](../Page/行政院.md "wikilink")，臺灣[實質上成為中華民國的主要領土](../Page/de_facto.md "wikilink")。1950年4月5日，「行政院核准臺灣省試辦地方自治」\[8\]。[臺灣省政府頒布](../Page/臺灣省政府.md "wikilink")[臺灣省各縣市實施地方自治綱要](../Page/臺灣省各縣市實施地方自治綱要.md "wikilink")，以行政命令方式，實施地方自治，調整行政區為5市16縣1管理局。1951年6月28日，本省成立省行政設計委員會。\[9\]1956年，臺灣省政府原位於臺北市，中華民國政府因國共內戰失利，而中央政府於1949年12月遷往臺北後，為防範[中國人民解放軍渡海轟炸臺北而癱瘓行政中樞](../Page/中國人民解放軍.md "wikilink")，臺灣省政府於1956年遷至[南投縣](../Page/南投縣.md "wikilink")[南投市](../Page/南投市.md "wikilink")[中興新村](../Page/中興新村.md "wikilink")。\[10\]

1967年7月1日，[臺北市升格為直轄市](../Page/臺北市.md "wikilink")，轄區減為4[市](../Page/市_\(中華民國\).md "wikilink")16縣1[管理局](../Page/陽明山管理局.md "wikilink")。1968年7月1日，原臺北縣景美鎮、南港鎮、木柵鄉、內湖鄉及[陽明山管理局之士林鎮](../Page/陽明山管理局.md "wikilink")、北投鎮併入[臺北市](../Page/臺北市.md "wikilink")，轄區減為4[市](../Page/市_\(中華民國\).md "wikilink")16縣。1971年5月15日，臺灣省政府析[臺中縣](../Page/臺中縣.md "wikilink")[和平鄉的梨山](../Page/和平區_\(臺中市\).md "wikilink")、平等2村置[梨山管理局](../Page/梨山建設管理局.md "wikilink")（後改稱[梨山建設管理局](../Page/梨山建設管理局.md "wikilink")）。1973年7月1日正式升格，直轄於臺灣省政府。1981年3月1日，降編為梨山風景特定區管理所，改隸於臺灣省交通處之下，原轄區仍改為臺中縣和平鄉。1979年7月1日，[高雄市升格為直轄市](../Page/高雄市_\(1945年-2010年\).md "wikilink")，原高雄縣小港鄉併入高雄市，轄區減為3[市](../Page/市_\(中華民國\).md "wikilink")16縣。1982年7月1日，[新竹市](../Page/新竹市.md "wikilink")、[嘉義市升格為](../Page/嘉義市.md "wikilink")[省轄市](../Page/市_\(中華民國\).md "wikilink")，行政區變更為5[市](../Page/市_\(中華民國\).md "wikilink")16縣。

1994年12月3日，臺灣省舉行民選[省長選舉](../Page/1994年中華民國省市長暨省市議員選舉.md "wikilink")，由[中國國民黨所提名的原省政府主席](../Page/中國國民黨.md "wikilink")[宋楚瑜當選](../Page/宋楚瑜.md "wikilink")，宋楚瑜成為唯一省長。

### 虛級化

1998年12月20日，依照[第四次憲法增修條文第九條第三項規定](../Page/中華民國憲法.md "wikilink")，將臺灣省[虛級化](../Page/臺灣省政府功能業務與組織調整.md "wikilink")，移除省的地方自治法人地位，省政府改為[行政院的官派機關](../Page/行政院.md "wikilink")，但省仍具有公法人資格（同[福建省](../Page/福建省_\(中華民國\).md "wikilink")）。2010年12月25日，原[臺北縣](../Page/新北市.md "wikilink")、[臺中縣](../Page/臺中縣.md "wikilink")[市及](../Page/臺中市_\(省轄市\).md "wikilink")[臺南縣](../Page/臺南縣.md "wikilink")[市改制為](../Page/臺南市_\(省轄市\).md "wikilink")[直轄市](../Page/直轄市_\(中華民國\).md "wikilink")，而原[高雄縣則與現有直轄市](../Page/高雄縣.md "wikilink")[高雄市改制合併為一新直轄市](../Page/高雄市.md "wikilink")，轄區減為3[市](../Page/市_\(中華民國\).md "wikilink")12縣\[11\]。2014年12月25日，原[桃園縣改制為](../Page/桃園市.md "wikilink")[直轄市](../Page/直轄市_\(中華民國\).md "wikilink")，轄區減為3[市](../Page/市_\(中華民國\).md "wikilink")11縣\[12\]。

2018年7月1日，所有員額與預算歸零，相關編制調任中央政府，省政府組織正式完成虛級化。

[ROC_Div_Taiwan.svg](https://zh.wikipedia.org/wiki/File:ROC_Div_Taiwan.svg "fig:ROC_Div_Taiwan.svg")
[1949_Taiwan.svg](https://zh.wikipedia.org/wiki/File:1949_Taiwan.svg "fig:1949_Taiwan.svg")

## 行政區劃

### 省虛級化前

民國34年（1945年）10月，[臺灣省行政長官公署開始在臺灣運作](../Page/臺灣省行政長官公署.md "wikilink")，初分為9市（[基隆市](../Page/基隆市.md "wikilink")、[臺北市](../Page/臺北市_\(省轄市\).md "wikilink")、[新竹市](../Page/新竹市_\(第一次省轄市\).md "wikilink")、[臺中市](../Page/臺中市_\(省轄市\).md "wikilink")、[彰化市](../Page/彰化市_\(省轄市\).md "wikilink")、[嘉義市](../Page/嘉義市_\(第一次省轄市\).md "wikilink")、[臺南市](../Page/臺南市_\(省轄市\).md "wikilink")、[高雄市](../Page/高雄市_\(省轄市\).md "wikilink")、[屏東市](../Page/屏東市_\(省轄市\).md "wikilink")）、8縣（[臺北縣](../Page/新北市.md "wikilink")、[新竹縣](../Page/新竹縣.md "wikilink")、[臺中縣](../Page/臺中縣.md "wikilink")、[臺南縣](../Page/臺南縣.md "wikilink")、[高雄縣](../Page/高雄縣.md "wikilink")、[花蓮縣](../Page/花蓮縣.md "wikilink")、[臺東縣](../Page/臺東縣.md "wikilink")、[澎湖縣](../Page/澎湖縣.md "wikilink")）。民國38年（1949年）8月，析置1管理局（[草山管理局](../Page/陽明山管理局.md "wikilink")）。民國39年（1950年）8月，全臺劃分為5市（[基隆市](../Page/基隆市.md "wikilink")、[臺北市](../Page/臺北市_\(省轄市\).md "wikilink")、[臺中市](../Page/臺中市_\(省轄市\).md "wikilink")、[臺南市](../Page/臺南市_\(省轄市\).md "wikilink")、[高雄市](../Page/高雄市_\(省轄市\).md "wikilink")）、16縣及1管理局。以後臺北、高雄2市改制；新置新竹、嘉義2市及梨山1管理局（1973年升格，1981年降級）。至民國87年（1998年）臺灣省虛級化前，全省轄有5[市](../Page/市_\(中華民國\).md "wikilink")（[基隆市](../Page/基隆市.md "wikilink")、[新竹市](../Page/新竹市.md "wikilink")、[臺中市](../Page/臺中市_\(省轄市\).md "wikilink")、[嘉義市](../Page/嘉義市.md "wikilink")、[臺南市](../Page/臺南市_\(省轄市\).md "wikilink")）、16[縣](../Page/縣_\(中華民國\).md "wikilink")（[臺北縣](../Page/新北市.md "wikilink")、[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[桃園縣](../Page/桃園市.md "wikilink")、[新竹縣](../Page/新竹縣.md "wikilink")、[苗栗縣](../Page/苗栗縣.md "wikilink")、[臺中縣](../Page/臺中縣.md "wikilink")、[彰化縣](../Page/彰化縣.md "wikilink")、[南投縣](../Page/南投縣.md "wikilink")、[雲林縣](../Page/雲林縣.md "wikilink")、[嘉義縣](../Page/嘉義縣.md "wikilink")、[臺南縣](../Page/臺南縣.md "wikilink")、[高雄縣](../Page/高雄縣.md "wikilink")、[屏東縣](../Page/屏東縣.md "wikilink")、[花蓮縣](../Page/花蓮縣.md "wikilink")、[臺東縣](../Page/臺東縣.md "wikilink")、[澎湖縣](../Page/澎湖縣.md "wikilink")）。

### 省虛級化後

臺灣省為[中華民國的](../Page/中華民國.md "wikilink")[一級行政區](../Page/中華民國行政區劃.md "wikilink")，實際管轄範圍包括[臺灣的部分區域](../Page/臺灣.md "wikilink")（不含[臺北](../Page/臺北市.md "wikilink")、[新北](../Page/新北市.md "wikilink")、[桃園](../Page/桃園市.md "wikilink")、[臺中](../Page/臺中市.md "wikilink")、[臺南與](../Page/臺南市.md "wikilink")[高雄六都](../Page/高雄市.md "wikilink")）與其附屬島嶼、以及[澎湖群島](../Page/澎湖群島.md "wikilink")。全省共劃分為11縣、3市（原依據省縣自治法稱「省轄市」，1999年地方制度法通過後，回歸憲法條文規定稱為市）。

<table>
<thead>
<tr class="header">
<th><p>臺灣省行政區劃圖</p></th>
<th><p>No.</p></th>
<th><p>區劃名稱</p></th>
<th><p>轄屬區域</p></th>
<th><p>政府所在</p></th>
<th><p>面積<small>（km²）</small></p></th>
<th><p>人口</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Political_divisions_of_Taiwan_Province.svg" title="fig:Political_divisions_of_Taiwan_Province.svg">Political_divisions_of_Taiwan_Province.svg</a></p></td>
<td><p><strong><a href="../Page/市_(中華民國).md" title="wikilink">市</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><a href="../Page/基隆市.md" title="wikilink">基隆市</a></p></td>
<td><p>7區</p></td>
<td><p><a href="../Page/中正區_(基隆市).md" title="wikilink">中正區</a></p></td>
<td><p>132.7589</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><a href="../Page/新竹市.md" title="wikilink">新竹市</a></p></td>
<td><p>3區</p></td>
<td><p><a href="../Page/北區_(新竹市).md" title="wikilink">北區</a></p></td>
<td><p>104.1526</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="../Page/嘉義市.md" title="wikilink">嘉義市</a></p></td>
<td><p>2區</p></td>
<td><p><a href="../Page/東區_(嘉義市).md" title="wikilink">東區</a></p></td>
<td><p>60.0256</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/縣_(中華民國).md" title="wikilink">縣</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/新竹縣.md" title="wikilink">新竹縣</a></p></td>
<td><p>1市3鎮9鄉</p></td>
<td><p><a href="../Page/竹北市.md" title="wikilink">竹北市</a></p></td>
<td><p>1427.5369</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/苗栗縣.md" title="wikilink">苗栗縣</a></p></td>
<td><p>2市5鎮11鄉</p></td>
<td><p><a href="../Page/苗栗市.md" title="wikilink">苗栗市</a></p></td>
<td><p>1820.3149</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/彰化縣.md" title="wikilink">彰化縣</a></p></td>
<td><p>2市6鎮18鄉</p></td>
<td><p><a href="../Page/彰化市.md" title="wikilink">彰化市</a></p></td>
<td><p>1074.3960</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/南投縣.md" title="wikilink">南投縣</a></p></td>
<td><p>1市4鎮8鄉</p></td>
<td><p><a href="../Page/南投市.md" title="wikilink">南投市</a></p></td>
<td><p>4106.4360</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/雲林縣.md" title="wikilink">雲林縣</a></p></td>
<td><p>1市5鎮14鄉</p></td>
<td><p><a href="../Page/斗六市.md" title="wikilink">斗六市</a></p></td>
<td><p>1290.8326</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/嘉義縣.md" title="wikilink">嘉義縣</a></p></td>
<td><p>2市2鎮14鄉</p></td>
<td><p><a href="../Page/太保市.md" title="wikilink">太保市</a></p></td>
<td><p>1903.6367</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/屏東縣.md" title="wikilink">屏東縣</a></p></td>
<td><p>1市3鎮29鄉</p></td>
<td><p><a href="../Page/屏東市.md" title="wikilink">屏東市</a></p></td>
<td><p>2775.6003</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/宜蘭縣.md" title="wikilink">宜蘭縣</a></p></td>
<td><p>1市3鎮8鄉</p></td>
<td><p><a href="../Page/宜蘭市.md" title="wikilink">宜蘭市</a></p></td>
<td><p>2143.6251</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/花蓮縣.md" title="wikilink">花蓮縣</a></p></td>
<td><p>1市2鎮10鄉</p></td>
<td><p><a href="../Page/花蓮市.md" title="wikilink">花蓮市</a></p></td>
<td><p>4628.5714</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/臺東縣.md" title="wikilink">臺東縣</a></p></td>
<td><p>1市2鎮13鄉</p></td>
<td><p><a href="../Page/臺東市.md" title="wikilink">臺東市</a></p></td>
<td><p>3515.2526</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/澎湖縣.md" title="wikilink">澎湖縣</a></p></td>
<td><p>1市5鄉</p></td>
<td><p><a href="../Page/馬公市.md" title="wikilink">馬公市</a></p></td>
<td><p>126.8641</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small>注：<a href="../Page/基隆市.md" title="wikilink">基隆市與臺灣省中間隔著</a><a href="../Page/直轄市_(中華民國).md" title="wikilink">直轄市</a><a href="../Page/新北市.md" title="wikilink">新北市</a>，不與省相鄰，故為臺灣省的飛地。</small></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>臺灣省</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>代碼</p></td>
</tr>
<tr class="even">
<td><p>10002</p></td>
</tr>
<tr class="odd">
<td><p>10004</p></td>
</tr>
<tr class="even">
<td><p>10005</p></td>
</tr>
<tr class="odd">
<td><p>10007</p></td>
</tr>
<tr class="even">
<td><p>10008</p></td>
</tr>
<tr class="odd">
<td><p>10009</p></td>
</tr>
<tr class="even">
<td><p>10010</p></td>
</tr>
<tr class="odd">
<td><p>10013</p></td>
</tr>
<tr class="even">
<td><p>10014</p></td>
</tr>
<tr class="odd">
<td><p>10015</p></td>
</tr>
<tr class="even">
<td><p>10016</p></td>
</tr>
<tr class="odd">
<td><p>10017</p></td>
</tr>
<tr class="even">
<td><p>10018</p></td>
</tr>
<tr class="odd">
<td><p>10020</p></td>
</tr>
<tr class="even">
<td><p><small>己裁撤、<br />
或改制</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/桃園市.md" title="wikilink">桃園縣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/臺中縣.md" title="wikilink">臺中縣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/臺南縣.md" title="wikilink">臺南縣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高雄縣.md" title="wikilink">高雄縣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/臺中市_(省轄市).md" title="wikilink">臺中市</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彰化市.md" title="wikilink">彰化市</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/臺南市_(省轄市).md" title="wikilink">臺南市</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高雄市.md" title="wikilink">高雄市</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/屏東市.md" title="wikilink">屏東市</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陽明山管理局.md" title="wikilink">陽明山管理局</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梨山建設管理局.md" title="wikilink">梨山建設管理局</a></p></td>
</tr>
</tbody>
</table>

### 行政區劃年表

|                                       |
| ------------------------------------- |
| **臺灣省行政區劃年表**                         |
| 說明：「?」表示不能確定其發生年份或月份，故放於最有可能的一年內，詳見注釋 |
| 西元                                    |
| 1945年                                 |
| 1946年                                 |
| 1947年                                 |
| 1948年                                 |
| 1949年                                 |
| 1950年                                 |
| 1951年                                 |
| 1952年                                 |
| 1953年                                 |
| 1954年                                 |
| 1955年                                 |
| 1956年                                 |
| 1957年                                 |
| 1958年                                 |
| 1959年                                 |
| 1960年                                 |
| 1961年                                 |
| 1962年                                 |
| 1963年                                 |
| 1964年                                 |
| 1965年                                 |
| 1966年                                 |
| 1967年                                 |
| 1968年                                 |
| 1969年                                 |
| 1970年                                 |
| 1971年                                 |
| 1972年                                 |
| 1973年                                 |
| 1974年                                 |
| 1975年                                 |
| 1976年                                 |
| 1977年                                 |
| 1978年                                 |
| 1979年                                 |
| 1980年                                 |
| 1981年                                 |
| 1982年                                 |
| 1983年                                 |
| 1984年                                 |
| 1985年                                 |
| 1986年                                 |
| 1987年                                 |
| 1988年                                 |
| 1989年                                 |
| 1990年                                 |
| 1991年                                 |
| 1992年                                 |
| 1993年                                 |
| 1994年                                 |
| 1995年                                 |
| 1996年                                 |
| 1997年                                 |
| 1998年                                 |
| 1999年                                 |
| 2000年                                 |
| 2001年                                 |
| 2002年                                 |
| 2003年                                 |
| 2004年                                 |
| 2005年                                 |
| 2006年                                 |
| 2007年                                 |
| 2008年                                 |
| 2009年                                 |
| 2010年                                 |
| 2011年                                 |
| 2012年                                 |
| 2013年                                 |
| 2014年                                 |
| 2015年                                 |
| 2016年                                 |
| 2017年                                 |
| 2018年                                 |

## 政府體制

### 省政府與議會

中華民國政府於民國38年（1949年）底遷臺後，中央政府所在地及臺灣省省會都設在[臺北市](../Page/臺北市.md "wikilink")。為防範中共[中國人民解放軍渡海轟炸臺北而直接癱瘓行政中樞](../Page/中國人民解放軍.md "wikilink")，中央政府於民國45年（1956年）著手疏遷計畫，規劃將[臺灣省政府](../Page/臺灣省政府.md "wikilink")、[臺灣省議會與](../Page/臺灣省議會.md "wikilink")[故宮遷移至](../Page/國立故宮博物院.md "wikilink")[臺中縣](../Page/臺中縣.md "wikilink")[霧峰鄉坑口村](../Page/霧峰區.md "wikilink")（今[臺中市霧峰區](../Page/臺中市.md "wikilink")[坑口-{里}-](../Page/坑口里.md "wikilink")），興建辦公及宿舍群，作為戰時指揮中樞，並命名為[光復新村](../Page/光復新村.md "wikilink")。

因土地徵收作業未趕上中央疏遷的期限，政府改在[南投縣](../Page/南投縣.md "wikilink")[南投市](../Page/南投市.md "wikilink")[虎山山麓](../Page/虎山.md "wikilink")-{[營盤口庄](../Page/營盤口庄.md "wikilink")}-建設，民國46年（1957年）建造完成，占地約200[公頃](../Page/公頃.md "wikilink")，命名為[中興新村](../Page/中興新村.md "wikilink")，6月30日，省府正式遷南投縣中興新村；在[臺中市亦有](../Page/臺中市.md "wikilink")[黎明辦公區](../Page/黎明新村.md "wikilink")（[精省後](../Page/臺灣省政府功能業務與組織調整.md "wikilink")，改為[行政院中部聯合服務中心園區](../Page/行政院中部聯合服務中心.md "wikilink")）。

2018年7月1日，[行政院宣佈省及機關](../Page/行政院.md "wikilink")[預算歸零](../Page/預算.md "wikilink")、所有員額與業務由中央相關部會承接，\[13\][臺灣省政府及](../Page/臺灣省政府.md "wikilink")[臺灣省諮議會行政組織開始移交](../Page/臺灣省諮議會.md "wikilink")[國家發展委員會及其他中央政府機關](../Page/中華民國國家發展委員會.md "wikilink")。2018年7月20日，臺灣省政府大樓原址成立「國家發展委員會中興辦公區中興新村活化專案辦公室」。\[14\]

目前臺灣省政府及省諮議會下無任何行政組織或實體辦公場所。

|                                                                                                                                                      |                                                                                                                                                                                                                                                                                                                                                                                     |
| ---------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [臺灣省政府大樓.jpg](https://zh.wikipedia.org/wiki/File:臺灣省政府大樓.jpg "fig:臺灣省政府大樓.jpg")大樓，現為[國家發展委員會中興辦公區中興新村活化專案辦公室](../Page/中華民國國家發展委員會.md "wikilink")\]\] | [Statues_of_Chiang_Kai-shek_in_front_of_Taiwan_Provincial_Council_Building_20101112.jpg](https://zh.wikipedia.org/wiki/File:Statues_of_Chiang_Kai-shek_in_front_of_Taiwan_Provincial_Council_Building_20101112.jpg "fig:Statues_of_Chiang_Kai-shek_in_front_of_Taiwan_Provincial_Council_Building_20101112.jpg")議事堂，現為[臺中市政府管理之直轄市市定古蹟](../Page/臺中市政府.md "wikilink")\]\] |

### 最高首長

1945年10月25日，[在臺日軍向](../Page/第10方面軍.md "wikilink")[同盟國的代表人蔣中正投降](../Page/同盟國_\(第二次世界大戰\).md "wikilink")，臺灣納入中華民國管轄。不同於中華民國其他省份，[國民政府當時以](../Page/國民政府.md "wikilink")「[臺灣省行政長官](../Page/臺灣省行政長官.md "wikilink")」作為臺灣省的最高[行政首長](../Page/行政長官.md "wikilink")，由前福建省主席[陳儀出任](../Page/陳儀.md "wikilink")。由於政府的種種倒行逆施，民不聊生之下\[15\]，於1947年2月底發生[二二八事件](../Page/二二八事件.md "wikilink")，民怨至此總爆發，各地發生軍民衝突，政府增援國軍抵臺鎮壓屠殺與[清鄉](../Page/清鄉.md "wikilink")\[16\]。3月23日，國民黨三中全會通過將陳儀撤職查辦。4月22日，鑒於行政長官公署治臺失敗，行政院會議通過撤銷臺灣省行政長官公署，改組為臺灣省政府。5月16日，[臺灣省政府正式成立](../Page/臺灣省政府.md "wikilink")，[臺灣省政府主席](../Page/臺灣省政府主席.md "wikilink")（簡稱省主席）取代臺灣省行政長官成為臺灣省的最高行政首長，由[魏道明出任首任主席](../Page/魏道明.md "wikilink")\[17\]\[18\]。

臺灣省政府主席在1991年以前一直負責臺灣省內事務，由[中央政府任命省府委員](../Page/中華民國政府.md "wikilink")，並舉派其中一員任職主席。1994年，首次舉行[臺灣省省長直接民選](../Page/臺灣省省長.md "wikilink")，由[宋楚瑜當選](../Page/宋楚瑜.md "wikilink")。1997年[中華民國憲法修正後](../Page/中華民國憲法.md "wikilink")，臺灣省政府進行[功能業務與組織調整](../Page/臺灣省政府功能業務與組織調整.md "wikilink")（通稱「精省」或「凍省」），省長職務再度改為省主席，由[總統指派](../Page/中華民國總統.md "wikilink")。因《[中華民國憲法增修條文](../Page/中華民國憲法增修條文.md "wikilink")》規定，精簡臺灣省的行政組織與功能。

2006年1月24日，因立法院未批-{准}-臺灣省主席、副主席、及省府委員的預算，行政院長[蘇貞昌據此宣佈停止任命臺灣省相關公職](../Page/蘇貞昌.md "wikilink")，持續向精省、政府架構精簡化的方向努力。1月25日由[鄭培富以臺灣省政府秘書長兼任代理臺灣省政府主席](../Page/鄭培富.md "wikilink")。2007年12月7日發布總統令任命行政院[政務委員](../Page/政務委員_\(中華民國\).md "wikilink")[林錫耀兼任臺灣省主席](../Page/林錫耀.md "wikilink")。2008年5月20日發布總統令任命政務委員[蔡勳雄兼任臺灣省主席](../Page/蔡勳雄.md "wikilink")，政務委員兼任省主席漸成慣例，從此維持至今。

### 姐妹州省

臺灣省和以下美國42個州是[姐妹州](../Page/姊妹城市.md "wikilink")\[19\]：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/印地安那州.md" title="wikilink">印地安那州</a> (1979)</p></li>
<li><p><a href="../Page/猶他州.md" title="wikilink">猶他州</a> (1980)</p></li>
<li><p><a href="../Page/奧克拉荷馬州.md" title="wikilink">奧克拉荷馬州</a> (1980)</p></li>
<li><p><a href="../Page/亞利桑那州.md" title="wikilink">亞利桑那州</a> (1980)</p></li>
<li><p><a href="../Page/密蘇里州.md" title="wikilink">密蘇里州</a> (1980)</p></li>
<li><p><a href="../Page/田納西州.md" title="wikilink">田納西州</a> (1980)</p></li>
<li><p><a href="../Page/西維吉尼亞州.md" title="wikilink">西維吉尼亞州</a> (1980)</p></li>
<li><p><a href="../Page/維吉尼亞州.md" title="wikilink">維吉尼亞州</a> (1981)</p></li>
<li><p><a href="../Page/南卡羅萊納州.md" title="wikilink">南卡羅萊納州</a> (1981)</p></li>
<li><p><a href="../Page/肯塔基州.md" title="wikilink">肯塔基州</a> (1982)</p></li>
<li><p><a href="../Page/科羅拉多州.md" title="wikilink">科羅拉多州</a> (1983)</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/密西西比州.md" title="wikilink">密西西比州</a> (1983)</p></li>
<li><p><a href="../Page/內布拉斯加州.md" title="wikilink">內布拉斯加州</a> (1983)</p></li>
<li><p><a href="../Page/阿肯色州.md" title="wikilink">阿肯色州</a> (1983)</p></li>
<li><p><a href="../Page/阿拉巴馬州.md" title="wikilink">阿拉巴馬州</a> (1983)</p></li>
<li><p><a href="../Page/加里福尼亞州.md" title="wikilink">加里福尼亞州</a> (1984)</p></li>
<li><p><a href="../Page/愛達荷州.md" title="wikilink">愛達荷州</a> (1984)</p></li>
<li><p><a href="../Page/明尼蘇達州.md" title="wikilink">明尼蘇達州</a> (1984)</p></li>
<li><p><a href="../Page/喬治亞州.md" title="wikilink">喬治亞州</a> (1984)</p></li>
<li><p><a href="../Page/懷俄明州.md" title="wikilink">懷俄明州</a> (1984)</p></li>
<li><p><a href="../Page/南達科他州.md" title="wikilink">南達科他州</a> (1984)</p></li>
<li><p><a href="../Page/路易西安那州.md" title="wikilink">路易西安那州</a> (1985)</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/俄亥俄州.md" title="wikilink">俄亥俄州</a> (1985)</p></li>
<li><p><a href="../Page/新墨西哥州.md" title="wikilink">新墨西哥州</a> (1985)</p></li>
<li><p><a href="../Page/蒙大拿州.md" title="wikilink">蒙大拿州</a> (1985)</p></li>
<li><p><a href="../Page/內華達州.md" title="wikilink">內華達州</a> (1985)</p></li>
<li><p><a href="../Page/北達科他州.md" title="wikilink">北達科他州</a> (1986)</p></li>
<li><p><a href="../Page/威斯康辛州.md" title="wikilink">威斯康辛州</a> (1986)</p></li>
<li><p><a href="../Page/俄勒岡州.md" title="wikilink">俄勒岡州</a> (1986)</p></li>
<li><p><a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a> (1988)</p></li>
<li><p><a href="../Page/阿拉斯加州.md" title="wikilink">阿拉斯加州</a> (1988)</p></li>
<li><p><a href="../Page/新澤西州.md" title="wikilink">新澤西州</a> (1989)</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/愛荷華州.md" title="wikilink">愛荷華州</a> (1989)</p></li>
<li><p><a href="../Page/堪薩斯州.md" title="wikilink">堪薩斯州</a> (1989)</p></li>
<li><p><a href="../Page/北卡羅萊納州.md" title="wikilink">北卡羅萊納州</a> (1991)</p></li>
<li><p><a href="../Page/麻薩諸塞州.md" title="wikilink">麻薩諸塞州</a> (1992)</p></li>
<li><p><a href="../Page/佛羅里達州.md" title="wikilink">佛羅里達州</a> (1992)</p></li>
<li><p><a href="../Page/伊利諾州.md" title="wikilink">伊利諾州</a> (1992)</p></li>
<li><p><a href="../Page/夏威夷州.md" title="wikilink">夏威夷州</a> (1993)</p></li>
<li><p><a href="../Page/康乃迪克州.md" title="wikilink">康乃迪克州</a> (1999)</p></li>
<li><p><a href="../Page/佛蒙特州.md" title="wikilink">佛蒙特州</a> (1999)</p></li>
<li><p><a href="../Page/德拉瓦州.md" title="wikilink">德拉瓦州</a> (2000)</p></li>
</ul></td>
</tr>
</tbody>
</table>

## 註釋

## 参考文献

## 外部連結

  - [臺灣省政府](https://web.archive.org/web/20190129130923/http://www.tpg.gov.tw/)（停止更新）
  - [臺灣省諮議會](https://web.archive.org/web/20180805143155/https://www.tpa.gov.tw/opencms/)（停止更新）

## 延伸閱讀

  - 國史館地理志編纂委員
    編：《中華民國史·[地理志](../Page/地理志.md "wikilink")》，[中華民國國史館](../Page/中華民國國史館.md "wikilink")。ISBN
    957-9042-02-0

## 参见

  - [臺灣省政府](../Page/臺灣省政府.md "wikilink")
      - [臺灣省政府主席](../Page/臺灣省政府主席.md "wikilink")
  - [臺灣省諮議會](../Page/臺灣省諮議會.md "wikilink")
  - [臺灣省政府功能業務與組織調整](../Page/臺灣省政府功能業務與組織調整.md "wikilink")
  - [臺灣](../Page/臺灣.md "wikilink")
  - [臺灣历史](../Page/臺灣历史.md "wikilink")
  - [臺灣統治者](../Page/臺灣統治者.md "wikilink")
  - [臺灣行政區劃](../Page/臺灣行政區劃.md "wikilink")
  - [中華民國](../Page/中華民國.md "wikilink")
      - [中華民國行政區劃](../Page/中華民國行政區劃.md "wikilink")
  - [臺灣族群](../Page/臺灣族群.md "wikilink")
  - [大清帝國](../Page/大清帝國.md "wikilink")
      - [福建台湾省](../Page/福建台湾省.md "wikilink")

{{-}}

[台](../Category/中華民國省份.md "wikilink")
[臺灣省](../Category/臺灣省.md "wikilink")
[Taiwan](../Category/1945年建立的行政區劃.md "wikilink")
[Category:1945年台灣建立](../Category/1945年台灣建立.md "wikilink")

1.  [中華民國內政部統計月報 2011.4](http://sowf.moi.gov.tw/stat/month/list.htm)
    中華民國內政部統計處

2.  [民國106年7月全國各縣市人口統計](http://sowf.moi.gov.tw/stat/month/list.htm),
    內政部統計處

3.

4.
5.

6.  諸蕃志

7.  [厦门日报，闽臺密不可分:臺湾省由来，2005-10-11](http://www.huaxia.com/la/jwgc/2005/00373930.html)

8.  [張之傑等](../Page/張之傑.md "wikilink")《20世紀臺灣全紀錄》，[台北](../Page/台北.md "wikilink")，[錦繡出版社](../Page/錦繡出版社.md "wikilink")，1991年。

9.
10.

11.
12.
13. [臺灣省政府全球資訊網停止更新公告](http://www.tpg.gov.tw/tpg/info_news2.php?ID=5150)

14. [中興新村活化專案辦公室揭牌](https://www.chinatimes.com/newspapers/20180721000252-260202)

15. [葛超智（George H.
    Kerr）、託管論與二二八事件之關係](http://www.drnh.gov.tw/ImagesPost/e3bc66dd-0cd2-49a1-a1f6-4129f4ccfd3f/893b5d20-4d25-4ed1-94a8-3a6f67a6ff7d_ALLFILES.pdf)，蘇瑤崇，[國史館學術集刊](../Page/國史館.md "wikilink")
    第4期，20040901

16.

17.
18. 責任歸屬研究報告，132-136、161-169、 214-220；二二八事件研究 下卷，595-602

19. [臺灣省政資料館網頁](http://subtpg.tpg.gov.tw/web-life/sister/sister.asp)