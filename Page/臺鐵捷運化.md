[TRA_EMU524_at_Dacing_Station_20041114.jpg](https://zh.wikipedia.org/wiki/File:TRA_EMU524_at_Dacing_Station_20041114.jpg "fig:TRA_EMU524_at_Dacing_Station_20041114.jpg")
[高架鐵路未來生活願景館.jpg](https://zh.wikipedia.org/wiki/File:高架鐵路未來生活願景館.jpg "fig:高架鐵路未來生活願景館.jpg")設於臺中市東區東光路與東成路口的高架鐵路未來生活願景館\]\]
**臺鐵捷運化**，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")（簡稱臺鐵）改善其[西部幹線路段中短途運輸之計畫總稱](../Page/西部幹線.md "wikilink")。目前計有12案施工或規劃中。

## 概要

「[捷運化](../Page/捷運.md "wikilink")」得名之由來，主要是為了使向來以中、長程運輸為主的臺鐵，能仿效[城市軌道運輸系統](../Page/城市軌道交通系統.md "wikilink")（即[捷運](../Page/捷運.md "wikilink")）的3大短程運輸特點：車站密集、班次密集、票種單純。施工除臺鐵自辦外，同屬[中華民國交通部之](../Page/中華民國交通部.md "wikilink")[交通部鐵道局亦執行多項專案](../Page/交通部鐵道局.md "wikilink")。

臺鐵捷運化以沿線人口密度較高的[西部幹線為主要計畫範圍](../Page/西部幹線.md "wikilink")，並將[臺灣西半部劃分為北](../Page/臺灣.md "wikilink")、中、南生活圈作為執行區段。納入[新十大建設後](../Page/新十大建設.md "wikilink")，其下共分為11案。另外加上已經2001年動工之「臺鐵都會區捷運化暨區域鐵路先期建設計畫」，計目前12案進行中。

計畫在硬體方面有幾個目標：

  - 新增車站、路線，約2至3公里有1站。
  - 改建車站、改善路線品質或景觀等。
  - 增購新型電聯車：增加班次，以因應短程輸送密集化的需要。

遠期方面，票價上也計畫仿傚[臺北捷運](../Page/臺北捷運.md "wikilink")，區間列車起跳票價定為基本里程5[公里](../Page/公里.md "wikilink")／[新台幣](../Page/新台幣.md "wikilink")20元，之後每3.6至4公里增加5元，坐滿70公里之票價為110元，如繼續搭乘則依據增加之里程計費。[自強號將調高基本里程長度為](../Page/自強號列車.md "wikilink")20公里，票價為新台幣45元，超過20公里以上之運輸價格不變。

## 歷史

## 計畫

臺鐵捷運化於[新十大建設期間確立](../Page/新十大建設.md "wikilink")，目前計有12案分頭進行中\[1\]。其中前3者屬台鐵主辦，餘為鐵路改建工程局主辦，主管機關則為交通部。另外尚有一些工程不列入臺鐵捷運化範圍，但同樣具有改善路線、改建車站的內容，詳見[\#其他](../Page/#其他.md "wikilink")。

### 進行中列表

註：新=[新十大建設](../Page/新十大建設.md "wikilink")（2003-2008）、愛=[愛台十二建設](../Page/愛台十二建設.md "wikilink")（2009-2016）、前=[前瞻基礎建設計畫](../Page/前瞻基礎建設計畫.md "wikilink")（2017-2024）

<table>
<thead>
<tr class="header">
<th><p>縣市</p></th>
<th><p>路線</p></th>
<th><p>區間</p></th>
<th><p>路<br />
線<br />
長<br />
度<br />
︵<br />
公<br />
里<br />
︶</p></th>
<th><p>*<br />
納<br />
入<br />
建<br />
設<br />
計<br />
畫<br />
名<br />
單</p></th>
<th><p>F/S(<a href="../Page/可行性研究.md" title="wikilink">可行性研究</a>)</p></th>
<th><p>綜合規劃</p></th>
<th><p>都市<br />
計畫<br />
變更</p></th>
<th><p>招標</p></th>
<th><p>動<br />
工</p></th>
<th><p>完<br />
工</p></th>
<th><p>試<br />
營<br />
運</p></th>
<th><p>交<br />
通<br />
部<br />
履<br />
勘</p></th>
<th><p>通車<br />
(預定)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>進<br />
行</p></td>
<td><p>送<br />
審</p></td>
<td><p><a href="../Page/中華民國交通部.md" title="wikilink">交<br />
通<br />
部</a><br />
核<br />
定</p></td>
<td><p><a href="../Page/國家發展委員會.md" title="wikilink">國<br />
發<br />
會</a><br />
核<br />
定</p></td>
<td><p><a href="../Page/行政院.md" title="wikilink">行<br />
政<br />
院</a><br />
核<br />
定</p></td>
<td><p>啟<br />
動</p></td>
<td><p><a href="../Page/环境影响评价.md" title="wikilink">環<br />
境<br />
影<br />
響<br />
評<br />
估</a></p></td>
<td><p><a href="../Page/行政院環境保護署.md" title="wikilink">環<br />
保<br />
署</a><br />
核<br />
定</p></td>
<td><p>交<br />
通<br />
部<br />
核<br />
定</p></td>
<td><p>国<br />
發<br />
會<br />
核<br />
定</p></td>
<td><p>行<br />
政<br />
院<br />
核<br />
定</p></td>
<td><p>送<br />
審</p></td>
<td><p><a href="../Page/中華民國內政部.md" title="wikilink">內<br />
政<br />
部</a><br />
核<br />
定</p></td>
<td><p>公<br />
示<br />
</p></td>
</tr>
<tr class="even">
<td><p>桃園</p></td>
<td><p><a href="../Page/縱貫線_(北段).md" title="wikilink">縱貫線地下化</a></p></td>
<td><p><a href="../Page/鳳鳴車站_(新北市).md" title="wikilink">鳳鳴車站</a> - <a href="../Page/平鎮車站.md" title="wikilink">平鎮車站</a></p></td>
<td><p>15.95</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>●</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>臺中</p></td>
<td><p>臺中線高架化</p></td>
<td><p><a href="../Page/大慶車站.md" title="wikilink">大慶車站</a> - <a href="../Page/烏日車站.md" title="wikilink">烏日車站</a></p></td>
<td><p>3.7</p></td>
<td></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>●</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/成追線.md" title="wikilink">成追線雙軌化</a></p></td>
<td><p><a href="../Page/成功車站.md" title="wikilink">成功車站</a> - <a href="../Page/追分車站.md" title="wikilink">追分車站</a></p></td>
<td><p>2.2</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/海岸線_(臺鐵).md" title="wikilink">海線雙軌高架化</a></p></td>
<td><p>追分車站 - <a href="../Page/大甲車站.md" title="wikilink">大甲車站</a></p></td>
<td><p>23.5</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>●</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/甲后線.md" title="wikilink">甲-{后}-線新建</a></p></td>
<td><p>大甲車站－<a href="../Page/后里車站.md" title="wikilink">后里車站</a></p></td>
<td><p>13.4</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>●</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>彰化</p></td>
<td><p>臺中線&amp;海岸線・<a href="../Page/縱貫線_(南段).md" title="wikilink">縱貫線高架化</a></p></td>
<td><p><a href="../Page/彰化車站.md" title="wikilink">彰化車站周邊</a></p></td>
<td><p>9.35</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>●</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>田中支線新建</p></td>
<td><p><a href="../Page/田中車站.md" title="wikilink">台鐵田中站</a> - <a href="../Page/彰化車站_(高鐵).md" title="wikilink">高鐵彰化站</a></p></td>
<td></td>
<td><p>前</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>雲林</p></td>
<td><p><a href="../Page/縱貫線_(南段).md" title="wikilink">縱貫線高架化</a></p></td>
<td><p><a href="../Page/斗六車站.md" title="wikilink">斗六車站周邊</a></p></td>
<td><p>約5</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>嘉義</p></td>
<td><p>縱貫線高架化</p></td>
<td><p><a href="../Page/嘉北車站.md" title="wikilink">嘉北車站</a> - <a href="../Page/北回歸線車站.md" title="wikilink">北回車站</a></p></td>
<td><p>7.9</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水上鄉.md" title="wikilink">水上鄉</a>、<a href="../Page/民雄鄉.md" title="wikilink">民雄鄉</a>、<a href="../Page/大林鎮_(台灣).md" title="wikilink">大林鎮延伸段</a></p></td>
<td><p>約5</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>臺南</p></td>
<td><p>縱貫線地下化</p></td>
<td><p><a href="../Page/臺南車站.md" title="wikilink">臺南車站</a> - <a href="../Page/南臺南車站.md" title="wikilink">南臺南車站</a></p></td>
<td><p>8.9</p></td>
<td><p>新</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
</tr>
<tr class="odd">
<td><p>縱貫線立體化（地下化）</p></td>
<td><p><a href="../Page/善化車站.md" title="wikilink">善化車站</a> - <a href="../Page/大橋車站.md" title="wikilink">大橋車站</a></p></td>
<td><p>11.05</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>屏東・臺東</p></td>
<td><p>屏東線・<a href="../Page/南迴線.md" title="wikilink">南迴線電氣化</a></p></td>
<td><p><a href="../Page/潮州車站.md" title="wikilink">潮州車站</a> - <a href="../Page/知本車站.md" title="wikilink">知本車站</a></p></td>
<td><p>111.8</p></td>
<td><p>愛</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
</tr>
<tr class="odd">
<td><p>屏東</p></td>
<td><p><a href="../Page/恆春線.md" title="wikilink">恆春線新建</a></p></td>
<td><p><a href="../Page/內獅車站.md" title="wikilink">內獅車站</a> - <a href="../Page/恆春車站.md" title="wikilink">恆春車站</a></p></td>
<td><p>37.72</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>●</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>宜蘭</p></td>
<td><p><a href="../Page/北宜新線.md" title="wikilink">北宜新線新建</a></p></td>
<td><p><a href="../Page/南港車站.md" title="wikilink">南港車站</a> - <a href="../Page/頭城車站.md" title="wikilink">頭城車站</a></p></td>
<td><p>53</p></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>△</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>花蓮・臺東</p></td>
<td><p><a href="../Page/臺東線.md" title="wikilink">臺東線全線雙軌化</a>[2]</p></td>
<td><p><a href="../Page/花蓮車站.md" title="wikilink">花蓮車站</a> - <a href="../Page/臺東車站.md" title="wikilink">臺東車站</a></p></td>
<td></td>
<td><p>前</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>●</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 臺鐵都會區捷運化暨區域鐵路先期建設計畫

計畫期程2001年1月1日至2009年12月31日，分為10大類，總經費計100億元（院臺交字第0920002523號函核定）：

1.  瓶頸路段改善工程：[汐止高架鐵路](../Page/汐止七堵鐵路高架化專案.md "wikilink")
2.  重點車站站場路線旅運設施及轉乘設施改善工程：[山佳](../Page/山佳車站.md "wikilink")、[楊梅](../Page/楊梅車站.md "wikilink")、[湖口](../Page/湖口車站.md "wikilink")、[新豐](../Page/新豐車站.md "wikilink")、[竹南](../Page/竹南車站.md "wikilink")、[沙鹿](../Page/沙鹿車站.md "wikilink")、[斗六](../Page/斗六車站.md "wikilink")、[大林](../Page/大林車站.md "wikilink")、[民雄](../Page/民雄車站.md "wikilink")(場改善或跨站工程)
3.  增設簡易通勤車站：計[三坑](../Page/三坑車站.md "wikilink")、[百福](../Page/百福車站.md "wikilink")、[汐科](../Page/汐科車站.md "wikilink")、[太原](../Page/太原車站.md "wikilink")、[大村](../Page/大村車站.md "wikilink")、[嘉北](../Page/嘉北車站.md "wikilink")、[南科](../Page/南科車站.md "wikilink")、[大橋](../Page/大橋車站.md "wikilink")（2008年增列）
4.  土地徵購
5.  軌道施工機械購置
6.  系統機電設施改善工程：平交道電纜佈放及徑路施設、號誌機電纜移設
7.  增購通勤電車
8.  擴充機檢設施工程
9.  沿線環境景觀改善
10. 高鐵聯外配合工程：增設[北新竹車站](../Page/北新竹車站.md "wikilink")、[沙崙車站](../Page/沙崙車站.md "wikilink")

### 臺鐵都會區捷運化暨區域鐵路後期建設計畫

計畫期程2005年1月1日至2020年12月，總經費計122.2億元（院臺交字第0940002035號函核定）：

1.  新建[浮洲](../Page/浮洲車站.md "wikilink")、[南樹林](../Page/南樹林車站.md "wikilink")、[平鎮](../Page/平鎮車站.md "wikilink")、[三姓橋等車站](../Page/三姓橋車站.md "wikilink")。
2.  改善香山、苗栗等3站。其中苗栗因另有高架案評估中暫緩。
3.  汐止＝南港間擴建為3軌正線。
4.  改善基隆＝苗栗間約140公里鐵路沿線景觀及安全。
5.  增建電聯車維修基地1座，位於[北湖車站](../Page/北湖車站.md "wikilink")（[富岡基地](../Page/富岡基地.md "wikilink")）。
6.  購置176輛通勤電聯車。

### 臺鐵都會區捷運化

#### 高架化

##### 臺中都會區鐵路高架捷運化計畫

  - [豐原車站](../Page/豐原車站.md "wikilink")－[大慶車站間高架化](../Page/大慶車站.md "wikilink")。
  - 新設[栗林](../Page/栗林車站_\(臺灣\).md "wikilink")、[頭家厝](../Page/頭家厝車站.md "wikilink")、[松竹](../Page/松竹車站.md "wikilink")、[精武](../Page/精武車站.md "wikilink")、[五權等車站](../Page/五權車站.md "wikilink")。

工程於2009年9月24日動工，已於2016年10月16日完成高架軌道切換並先行啟用[大慶](../Page/大慶車站.md "wikilink")、[臺中](../Page/臺中車站.md "wikilink")、[太原](../Page/太原車站.md "wikilink")、[潭子及](../Page/潭子車站.md "wikilink")[豐原等五站](../Page/豐原車站.md "wikilink")，新增五個車站於2018年10月28日完工。

##### 彰化市區鐵路高架化計畫

  - 現有[彰化車站改建為](../Page/彰化車站.md "wikilink")[高架車站](../Page/高架車站.md "wikilink")。
  - 新增二座通勤車站：[金馬車站](../Page/金馬車站.md "wikilink")（苦苓腳車站）、[中央車站](../Page/南彰化車站.md "wikilink")（大埔車站）
  - 銜接台中山海線鐵路高架化
  - 消除鐵路沿線4處平交道、2處地下道、4處陸橋和安和人行天橋，改善橫交道路所衍生之交通問題，增進行車安全。

##### 員林市區鐵路高架化計畫

  - 縱貫線員林市段全線高架（3.98公里）
  - 改建[員林站](../Page/員林車站.md "wikilink")

工程已於2006年2月動土，已於2015年8月30日全部完工（舊站拆除、平交道消除、地下道回填、車站美化及公共藝術等）。
高架段已於2014年11月2日提前完工通車，全案於2015年12月正式竣工。

##### 嘉義市區鐵路高架化計畫

  - 縱貫線牛稠溪橋(不含)－[北回車站](../Page/北回歸線車站.md "wikilink")(不含)間全線高架（10.9公里）。
  - 遷移嘉義調車場至北回歸線車站南側，並改名為水上調車場。
  - 改建或新設[嘉北站](../Page/嘉北車站.md "wikilink")、[嘉義站](../Page/嘉義車站.md "wikilink")、[北回站等通勤車站](../Page/北回歸線車站.md "wikilink")。

##### 宜蘭鐵路高架化計畫

  - [頭城](../Page/頭城車站.md "wikilink")－[宜蘭間高架化](../Page/宜蘭車站.md "wikilink")。
  - [二結](../Page/二結車站.md "wikilink")－[冬山河段間高架化](../Page/冬山河.md "wikilink")。

##### 林邊鐵路高架化計畫

  - 由交通部鐵路改建工程局辦理。
  - 施工期間，起始2008年－2012年完工。

##### 高雄－屏東潮州捷運化建設計畫

  - [屏東](../Page/屏東車站.md "wikilink")－[潮州間高架化並電氣化](../Page/潮州車站.md "wikilink")。
  - 改善[屏東](../Page/屏東車站.md "wikilink")、[歸來](../Page/歸來車站.md "wikilink")、[麟洛](../Page/麟洛車站.md "wikilink")、[西勢](../Page/西勢車站.md "wikilink")、[竹田](../Page/竹田車站.md "wikilink")、[潮州等車站](../Page/潮州車站.md "wikilink")。
  - 新建調車場（潮州調車場）。

##### 臺鐵內灣支線改善計畫

  - 新建六家線（[竹中站](../Page/竹中車站.md "wikilink")－[六家站](../Page/六家車站.md "wikilink")）間全線高架雙軌電氣化。
  - [新竹站](../Page/新竹車站.md "wikilink")－[竹中站間高架化並電氣化](../Page/竹中車站.md "wikilink")。
  - 新建包括[千甲](../Page/千甲車站.md "wikilink")、[新莊](../Page/新莊車站_\(新竹市\).md "wikilink")、[竹中](../Page/竹中車站.md "wikilink")、[六家等車站](../Page/六家車站.md "wikilink")。
  - [竹中站](../Page/竹中車站.md "wikilink")－[內灣站間路線改善](../Page/內灣車站.md "wikilink")。

#### 地下化

##### 高雄市區鐵路地下化計畫

計畫期程2009年1月1日至2017年12月31日：

  - [新左營站](../Page/新左營車站.md "wikilink")（不含）－[鳳山站間地下化](../Page/鳳山車站_\(臺鐵\).md "wikilink")。
  - 臺鐵[左營站](../Page/左營車站.md "wikilink")、[高雄站](../Page/高雄車站.md "wikilink")、[鳳山站地下化](../Page/鳳山車站_\(臺鐵\).md "wikilink")。
  - 新設[內惟](../Page/內惟車站.md "wikilink")、[美術館](../Page/美術館車站.md "wikilink")、[鼓山](../Page/鼓山車站.md "wikilink")、[三塊厝](../Page/三塊厝車站.md "wikilink")、[民族](../Page/民族車站.md "wikilink")、[科工館](../Page/科工館車站.md "wikilink")、[正義等車站](../Page/正義/澄清車站.md "wikilink")。
  - 遷移[高雄機廠](../Page/高雄機廠.md "wikilink")、高雄機檢段並設立新左營、潮州基地、潮州機廠。

工程已經於2009年6月26日動土，2018年10月14日完成地下軌道切換通車，同時啟用新增設7座通勤車站，預計2023年完工。

##### 臺南市區鐵路地下化計畫

計畫期程：2017年3月至2024年6月，總經費計293.6億元
計畫範圍：北起臺南市永康站南端之永康橋以南約0.17公里處，南至生產路以南約1.91公里，全長8.23公里

  - 縱貫線臺南市段全線地下化（8.23公里）。
  - 改建[臺南車站](../Page/臺南車站.md "wikilink")、新建[林森車站與](../Page/林森車站.md "wikilink")[南臺南車站](../Page/南臺南車站.md "wikilink")。
  - 消除鐵路沿線9處平交道、4處地下道和3處陸橋，改善橫交道路所衍生之交通問題，增進行車安全。

工程已經於2017年3月15日動土，預計2024年完工。

##### 桃園市區鐵路地下化建設計畫

  - 由[臺灣鐵路管理局工務處臺北工務段辦理](../Page/臺灣鐵路管理局.md "wikilink")，細部設計後交由鐵路改建工程局施工。
  - [鶯歌](../Page/鶯歌車站.md "wikilink")（不含）－[中壢間地下化](../Page/中壢車站.md "wikilink")。
  - 新增[鳳鳴](../Page/鳳鳴車站_\(新北市\).md "wikilink")、[中路](../Page/中路車站.md "wikilink")、[永豐](../Page/永豐路車站.md "wikilink")、[中原](../Page/中原大學車站.md "wikilink")、[平鎮通勤車站](../Page/平鎮車站.md "wikilink")。
  - 鶯歌至[桃園間擴建第三正線](../Page/桃園車站_\(台鐵\).md "wikilink")。

##### 基隆火車站都市更新站區遷移計畫

  - [基隆站](../Page/基隆車站.md "wikilink")－[三坑站間半地下化](../Page/三坑車站.md "wikilink")（1.5公里，含[基隆站以北軍用戰備線](../Page/基隆車站.md "wikilink")）。
  - 改建[基隆站](../Page/基隆車站.md "wikilink")，並利用站區東西側土地的高度差異，將站體半地下化。
  - 配合進行「基隆火車站暨西二西三碼頭都市更新計畫」，將基隆站站體南移、月臺向北延伸，多餘土地則提供[都市更新開發使用](../Page/都市更新.md "wikilink")。

### 屏東南迴線鐵路電氣化計畫

  - [潮州站](../Page/潮州車站.md "wikilink")－[枋寮站間雙軌電氣化](../Page/枋寮車站.md "wikilink")。
  - 改善車站[南州](../Page/南州車站.md "wikilink")、[鎮安](../Page/鎮安車站.md "wikilink")、[崁頂](../Page/崁頂車站.md "wikilink")、[佳冬](../Page/佳冬車站.md "wikilink")、[東海](../Page/東海車站_\(台灣\).md "wikilink")、[枋寮等車站](../Page/枋寮車站.md "wikilink")。
  - [枋寮](../Page/枋寮車站.md "wikilink")－[臺東間電氣化](../Page/臺東車站.md "wikilink")。
  - 除[中央號誌](../Page/中央號誌站.md "wikilink")－[古莊間長](../Page/古莊車站.md "wikilink")16.76公里配置雙軌電化外，其餘路段為單軌電化配置。
  - 改善車站[大武](../Page/大武車站.md "wikilink")、[太麻里](../Page/太麻里車站.md "wikilink")、[知本](../Page/知本車站.md "wikilink")、[康樂等車站](../Page/康樂車站.md "wikilink")。

### 臺鐵臺南沙崙支線計畫

總經費計58.43億元（院臺交字第0930048842號函核定、0980004846號第一次修正），2011年1月2日通車：

  - 新建沙崙線（[中洲站](../Page/中洲車站.md "wikilink")－[沙崙站](../Page/沙崙車站.md "wikilink")）全線高架雙軌電氣化。
  - 改建[中洲](../Page/中洲車站.md "wikilink")、[長榮大學](../Page/長榮大學車站.md "wikilink")、[沙崙等車站](../Page/沙崙車站.md "wikilink")。

### 東部鐵路快捷化計畫

  - 計畫興建[北宜直線鐵路](../Page/北宜直線鐵路.md "wikilink")。
  - 改善[北迴線安全設施](../Page/北迴線.md "wikilink")。
  - 花蓮吉安鐵路高架化。
  - [花東鐵路雙軌電氣化](../Page/花東鐵路電氣化.md "wikilink")。

## 新增車站

後表為以「臺鐵捷運化」為目的新增的通勤車站，故不含[榮華車站](../Page/榮華車站.md "wikilink")（2001年11月24日）及服務[高鐵轉乘的](../Page/台灣高速鐵路.md "wikilink")[新烏日](../Page/新烏日車站.md "wikilink")、[新左營車站](../Page/新左營車站.md "wikilink")（俱為2006年12月1日開通）。除[大慶車站皆在上述](../Page/大慶車站.md "wikilink")12案計畫內。

表中「長期計畫」車站，出現於2003年《臺鐵兼具都會區捷運功能暨增設通勤車站評估規劃定案報告》中，所建議的「長期發展具增站潛力之地點」，因此尚未排入興建時程；興建中及計畫中車站的名稱皆為暫擬，正式啟用前可能再次更改。

|-align=center \!width=25%|站名 \!width=30%|候選站名 \!width=20%|位置
\!width=25%|啟用日期

|-align=center \!colspan=4|[縱貫線（北段）](../Page/縱貫線_\(北段\).md "wikilink")

|-align=center |[三坑](../Page/三坑車站.md "wikilink") |三坑仔\[3\]、龍門社區\[4\]
|[基隆](../Page/基隆車站.md "wikilink")－[八堵間](../Page/八堵車站.md "wikilink")
|2003年5月9日

|-align=center |[百福](../Page/百福車站.md "wikilink") |百福社區
|[七堵](../Page/七堵車站.md "wikilink")－[五堵間](../Page/五堵車站.md "wikilink")
|2007年5月8日

|-align=center |[汐科](../Page/汐科車站.md "wikilink") |南汐止、汐科園區
|[汐止](../Page/汐止車站.md "wikilink")－[南港間](../Page/南港車站.md "wikilink")
|2007年12月30日

|-align=center |[浮洲](../Page/浮洲車站.md "wikilink") |無
|rowspan=2|[板橋](../Page/板橋車站_\(臺灣\).md "wikilink")－[樹林間](../Page/樹林車站.md "wikilink")
|2011年9月2日 |-align=center |[新樹](../Page/新樹車站.md "wikilink") |無
|長期計畫\[5\] |-align=center |[南樹林](../Page/南樹林車站.md "wikilink")
|樹林調車場
|[樹林](../Page/樹林車站.md "wikilink")－[山佳間](../Page/山佳車站.md "wikilink")
|2015年12月23日

|-align=center |[陶瓷老街](../Page/陶瓷老街車站.md "wikilink") |無
|rowspan=2|[鶯歌](../Page/鶯歌車站.md "wikilink")－[桃園間](../Page/桃園車站_\(臺鐵\).md "wikilink")
|長期計畫\[6\]

|-align=center |[鳳鳴](../Page/鳳鳴車站_\(新北市\).md "wikilink") |北桃園 |預定2020年動工

|-align=center |[中路](../Page/中路車站.md "wikilink") |國際路
|rowspan=2|[桃園](../Page/桃園車站_\(臺鐵\).md "wikilink")－[內壢間](../Page/內壢車站.md "wikilink")
|預定2020年動工

|-align=center |[永豐](../Page/永豐車站_\(臺灣\).md "wikilink") |永豐路 |預定2020年動工

|-align=center |[中原](../Page/中原車站_\(桃園市\).md "wikilink") |中原大學
|[內壢](../Page/內壢車站.md "wikilink")－[中壢間](../Page/中壢車站.md "wikilink")
|預定2020年動工

|-align=center |[平鎮](../Page/平鎮車站.md "wikilink") |環南路
|[中壢](../Page/中壢車站.md "wikilink")－[埔心間](../Page/埔心車站.md "wikilink")
|預定2020年動工

|-align=center |[新富](../Page/新富車站.md "wikilink") |無
|rowspan="2"|[富岡](../Page/富岡車站.md "wikilink")－[湖口間](../Page/湖口車站.md "wikilink")
|2017年9月6日

|-align=center |[北湖](../Page/北湖車站.md "wikilink") |北湖口 |2012年9月28日

|-align=center |[北新竹](../Page/北新竹車站.md "wikilink") |無
|[竹北](../Page/竹北車站.md "wikilink")－[新竹間](../Page/新竹車站.md "wikilink")
|2011年11月11日 |-align=center
|[頂埔](../Page/頂埔車站_\(新竹市\).md "wikilink")\[7\] |無
|rowspan=2|[新竹](../Page/新竹車站.md "wikilink")－[香山間](../Page/香山車站_\(臺灣\).md "wikilink")
|長期計畫\[8\] |-align=center |[三姓橋](../Page/三姓橋車站.md "wikilink") |南新竹
|2016年6月29日

|-align=center |[北竹南](../Page/北竹南車站.md "wikilink") |無
|[崎頂](../Page/崎頂車站.md "wikilink")－[竹南間](../Page/竹南車站.md "wikilink")
|長期計畫\[9\] |-align=center
\!colspan=4|[內灣線](../Page/內灣線.md "wikilink")、[六家線](../Page/六家線.md "wikilink")

|-align=center |[千甲](../Page/千甲車站.md "wikilink") |世博
|rowspan=2|[新竹](../Page/新竹車站.md "wikilink")－[竹中間](../Page/竹中車站.md "wikilink")
|2011年11月11日

|-align=center |[新莊](../Page/新莊車站_\(新竹市\).md "wikilink") |關東、竹科
|2011年11月11日

|-align=center |[六家](../Page/六家車站.md "wikilink") |無 |新線 |2011年11月11日

|-align=center \!colspan=4|[臺中線（山線）](../Page/臺中線.md "wikilink")

|-align=center |[栗林](../Page/栗林車站_\(臺灣\).md "wikilink") |豐南
|[豐原](../Page/豐原車站.md "wikilink")－[潭子間](../Page/潭子車站.md "wikilink")
|2018年10月28日

|-align=center |[頭家厝](../Page/頭家厝車站.md "wikilink") |潭南
|rowspan=4|[潭子](../Page/潭子車站.md "wikilink")－[臺中間](../Page/臺中車站.md "wikilink")
|2018年10月28日

|-align=center |[松竹](../Page/松竹車站.md "wikilink") |無 |2018年10月28日

|-align=center |[太原](../Page/太原車站.md "wikilink") |北屯 |2002年11月22日

|-align=center |[精武](../Page/精武車站.md "wikilink") |臺中北站\[10\]
|2018年10月28日 |-align=center |[五權](../Page/五權車站.md "wikilink") |無
|[臺中](../Page/臺中車站.md "wikilink")－[烏日間](../Page/烏日車站.md "wikilink")
|2018年10月28日

|-align=center |[大慶](../Page/大慶車站.md "wikilink") |
|[臺中](../Page/臺中車站.md "wikilink")－[烏日間](../Page/烏日車站.md "wikilink")
|1998年7月7日

|-align=center \!colspan=4|[海岸線（海線）](../Page/海岸線_\(臺鐵\).md "wikilink")

|-align=center |[鐵砧山](../Page/鐵砧山車站.md "wikilink") |無
|[日南](../Page/日南車站.md "wikilink")－[大甲間](../Page/大甲車站.md "wikilink")
|長期計畫\[11\]

|-align=center |[北沙鹿](../Page/北沙鹿車站.md "wikilink") |無
|[清水](../Page/清水車站_\(台灣\).md "wikilink")－[沙鹿間](../Page/沙鹿車站.md "wikilink")
|長期計畫\[12\]

|-align=center |[龍山](../Page/龍山車站.md "wikilink") |無
|[沙鹿](../Page/沙鹿車站.md "wikilink")－[龍井間](../Page/龍井車站.md "wikilink")
|長期計畫\[13\]

|-align=center |[龍安](../Page/隆安車站.md "wikilink") |無
|[龍井](../Page/龍井車站.md "wikilink")－[大肚間](../Page/大肚車站.md "wikilink")
|長期計畫\[14\]

|-align=center \!colspan=4|[縱貫線（南段）](../Page/縱貫線_\(南段\).md "wikilink")

|-align=center |[金馬](../Page/金馬車站.md "wikilink")\[15\] |臺化\[16\]、東彰化
|[追分](../Page/追分車站.md "wikilink") /
[成功](../Page/成功車站.md "wikilink")－[彰化間](../Page/彰化車站.md "wikilink")（彰化市金馬陸橋）
|預定西元2031年 |-align=center |[建國](../Page/建國車站_\(彰化\).md "wikilink") |北彰化
|[追分](../Page/追分車站.md "wikilink") /
[成功](../Page/成功車站.md "wikilink")－[彰化間](../Page/彰化車站.md "wikilink")（彰化市建國陸橋）
|預定西元2031年 |-align=center |[中央](../Page/南彰化車站.md "wikilink")
|南彰化\[17\]、雲長路\[18\]
|[彰化](../Page/彰化車站.md "wikilink")－[花壇間](../Page/花壇車站.md "wikilink")（彰化市中央陸橋）
|預定西元2031年

|-align=center |[大村](../Page/大村車站.md "wikilink") |無
|[花壇](../Page/花壇車站.md "wikilink")－[員林間](../Page/員林車站.md "wikilink")
|2006年4月4日

|-align=center |[南斗六](../Page/南斗六車站.md "wikilink") |無
|[斗六](../Page/斗六車站.md "wikilink")－[斗南間](../Page/斗南車站.md "wikilink")
|長期計畫\[19\]

|-align=center |[南民雄](../Page/南民雄車站.md "wikilink") |無
|rowspan=3|[民雄](../Page/民雄車站.md "wikilink")－[嘉義間](../Page/嘉義車站.md "wikilink")
|長期計畫\[20\] |-align=center |[頭橋工業區](../Page/頭橋工業區車站.md "wikilink")\[21\]
|無 |規劃中

|-align=center |[嘉北](../Page/嘉北車站.md "wikilink") |北嘉義、嘉基 |2005年9月8日

|-align=center |[北回](../Page/北回歸線車站.md "wikilink")\[22\] |無
|[嘉義](../Page/嘉義車站.md "wikilink")－[水上間](../Page/水上車站.md "wikilink")
|計畫中

|-align=center |[南科](../Page/南科車站.md "wikilink") |無
|[善化](../Page/善化車站.md "wikilink")－[新市間](../Page/新市車站.md "wikilink")
|2010年7月14日 |-align=center |[永康工業區](../Page/永康工業區車站.md "wikilink")
|永工、車行
|[新市](../Page/新市車站.md "wikilink")－[永康間](../Page/永康車站.md "wikilink")
|長期計畫\[23\]

|-align=center |[康橋](../Page/康橋車站.md "wikilink") |無
|rowspan=2|[永康](../Page/永康車站.md "wikilink")－[臺南間](../Page/臺南車站.md "wikilink")
|計畫中

|-align=center |[大橋](../Page/大橋車站.md "wikilink") |奇美、六甲頂\[24\]
|2002年10月4日 |-align=center |[林森](../Page/林森車站.md "wikilink") |無
|rowspan=2|[臺南](../Page/臺南車站.md "wikilink")－[保安間](../Page/保安車站.md "wikilink")
|興建中

|-align=center |[南臺南](../Page/南臺南車站.md "wikilink") |文化中心\[25\] |興建中

|-align=center |[仁德](../Page/仁德車站.md "wikilink") |無
|[保安](../Page/保安車站.md "wikilink")－[中洲間](../Page/中洲車站.md "wikilink")
|2014年1月10日

|-align=center |[路科](../Page/路科車站.md "wikilink") |無
|rowspan=2|[路竹](../Page/路竹車站.md "wikilink")－[岡山間](../Page/岡山車站.md "wikilink")
|長期計畫\[26\]

|-align=center |[岡山農工](../Page/岡山農工車站.md "wikilink") |無 |長期計畫\[27\]

|-align=center |[文化中心](../Page/文化中心車站.md "wikilink") |無
|[岡山](../Page/岡山車站.md "wikilink")－[橋頭間](../Page/橋頭車站.md "wikilink")
|長期計畫\[28\]

|-align=center |[內惟](../Page/內惟車站.md "wikilink") |無
|rowspan=4|[左營（舊城）](../Page/左營（舊城）車站.md "wikilink")－[高雄間](../Page/高雄車站.md "wikilink")
|2018年10月14日

|-align=center |[美術館](../Page/美術館車站.md "wikilink") |無 |2018年10月14日

|-align=center |[鼓山](../Page/鼓山車站.md "wikilink") |無 |2018年10月14日

|-align=center |[三塊厝](../Page/三塊厝車站.md "wikilink") |無 |2018年10月14日

|-align=center \!colspan=4|[沙崙線](../Page/沙崙線.md "wikilink")

|-align=center |[長榮大學](../Page/長榮大學車站.md "wikilink") |大潭、武東
|rowspan=2|新線 |2011年1月2日 |-align=center
|[沙崙](../Page/沙崙車站.md "wikilink") |無 |2011年1月2日

|-align=center \!colspan=4|[屏東線](../Page/屏東線.md "wikilink")

|-align=center |[民族](../Page/民族車站.md "wikilink") |無
|rowspan=3|[高雄](../Page/高雄車站.md "wikilink")－[鳳山間](../Page/鳳山車站_\(臺鐵\).md "wikilink")
|2018年10月14日

|-align=center |[科工館](../Page/科工館車站.md "wikilink") |工博館、大順 |2018年10月14日

|-align=center |[正義](../Page/正義車站.md "wikilink") |（澄清路與正義路間）
|2018年10月14日

|-align=center |[高屏溪](../Page/高屏溪車站.md "wikilink") |無
|[九曲堂](../Page/九曲堂車站.md "wikilink")－[六塊厝間](../Page/六塊厝車站.md "wikilink")
|長期計畫\[29\]

|-align=center |[廣東南路](../Page/廣東南路車站.md "wikilink")\[30\] |
|[屏東](../Page/屏東車站.md "wikilink")－[歸來間](../Page/歸來車站.md "wikilink")（廣東南路、屏東工業區附近）
|計畫中

|-align=center \!colspan=4|[宜蘭線](../Page/宜蘭線.md "wikilink")

|-align=center |[縣政中心](../Page/縣政中心車站.md "wikilink") |無
|[宜蘭](../Page/宜蘭車站.md "wikilink") -
[二結間](../Page/二結車站.md "wikilink") |規劃中（隨宜蘭縣內鐵路高架化後興建）

|-align=center \!colspan=4|[臺東線](../Page/臺東線.md "wikilink")
|-align=center |[林榮新光](../Page/林榮新光車站.md "wikilink") |林榮新光
|[豐田](../Page/豐田車站_\(台灣\).md "wikilink")－[南平間](../Page/南平車站_\(臺灣\).md "wikilink")（新光兆豐農場附近）
|2018年7月10日

|-align=center |[長良](../Page/長良車站.md "wikilink") |無
|[玉里](../Page/玉里車站.md "wikilink")－[東里間](../Page/東里車站.md "wikilink")
|計畫中 |}

## 其他

### 日治時期的「捷運化」

[right](../Page/image:臺南東門車站.JPG.md "wikilink")
捷運化的政策在日治末期早有類似者，主要係[鐵道部順應民情增駛氣動車](../Page/台灣總督府交通局鐵道部.md "wikilink")（單節汽油車）、新設乘降場（無人簡易站）。茲列出當時新增車站：

  - 臺北州：[北五堵](../Page/北五堵車站.md "wikilink")、[北臺北](../Page/新生車站.md "wikilink")、新起町、[江子翠](../Page/港嘴車站.md "wikilink")、[浮洲](../Page/浮洲車站.md "wikilink")、[南礁溪](../Page/南礁溪車站.md "wikilink")、[武罕](../Page/武罕車站.md "wikilink")
  - 新竹州：[三湖](../Page/三湖車站.md "wikilink")、[斗崙](../Page/斗崙車站.md "wikilink")、[香山坑](../Page/香山坑車站.md "wikilink")、[內湖](../Page/內湖車站.md "wikilink")、[尖山](../Page/尖山車站.md "wikilink")
  - 臺中州：[老松町](../Page/老松町車站.md "wikilink")、[學田](../Page/學田車站.md "wikilink")、[遊園地前](../Page/遊園地前車站.md "wikilink")、[大村](../Page/大村車站.md "wikilink")、[永靖](../Page/永靖車站.md "wikilink")
  - 臺南州：[東門町](../Page/東門車站_\(台南市\).md "wikilink")、[試驗所前](../Page/南臺南車站.md "wikilink")
  - 高雄州：[山下町](../Page/壽山車站.md "wikilink")

上述車站在日治末期因戰亂缺乏汽油，配合停駛汽油車而休業。戰後有部份車站一度復站，但大部分稍後仍然廢止。一部份站址直到近年臺鐵捷運化政策，終獲再次復站。

### 捷運化與捷運

目前的[臺中都會區鐵路高架捷運化工程完工後](../Page/臺中都會區.md "wikilink")，臺鐵在該都會區內擁有正式名份成為[城市軌道交通系統的一員](../Page/城市軌道交通系統.md "wikilink")，稱為「[臺鐵捷運紅線](../Page/臺鐵捷運紅線.md "wikilink")」。

臺鐵[淡水線於](../Page/臺鐵淡水線.md "wikilink")1988年停駛前，除了預定的改建納入台北捷運系統方案外，也曾有鐵路高架雙軌電氣化方案，但評估效益不如台北捷運方案故放棄。由於預期可達到捷運的效果，此一構想被認為與現今的臺鐵捷運化類似。

[臺北捷運最早引進的BMTC](../Page/臺北捷運.md "wikilink")（英國捷運顧問公司）所提出的大臺北捷運計畫為同台鐵軌距1067mm，也採用相同的架空線系統，但使用直流750V，配合地下及高架等規格施作。所考慮的重點為：技術可及性高、施工難度低、車輛配合隧道的建築界限較小。以當時臺北急迫需要捷運來改善交通，BMTC公司所提雖卻能達到上述的需求，但當時台灣面臨與美國之間的貿易逆差，美方要求開放有關工程利益與市場來平衡彼此的貿易問題。造成原本由BMTC公司顧問案被終結，改由美國捷運顧問公司（ATC）接手，才將有關規格改成歐美捷運較常運用的1435mm軌距，第三軌供電等規範，臺灣的捷運政策與之後臺鐵捷運化的關聯性並不大。\[31\]

### 捷運化與公路客運

配合臺鐵捷運化，地方縣市政府亦同步調整既有、或新闢公車路線。例如[基隆市公車配合](../Page/基隆市公車.md "wikilink")[百福車站增闢](../Page/百福車站.md "wikilink")705（百福車站—百福社區）路線，以及[南部科學工業園區配合](../Page/南部科學工業園區.md "wikilink")[南科車站調整免費巡迴巴士](../Page/南科車站.md "wikilink")\[32\]等。

## 評論

## 相關條目

  - [臺鐵區間車](../Page/臺鐵區間車.md "wikilink")
  - [臺鐵立體化](../Page/臺鐵立體化.md "wikilink")
  - [臺鐵高架化](../Page/臺鐵高架化.md "wikilink")
  - [臺鐵地下化](../Page/臺鐵地下化.md "wikilink")
  - [跨站式站房](../Page/跨站式站房_\(台鐵\).md "wikilink") - 各地捷運化案中大量採用的新式站房建築
  - [區域鐵路](../Page/區域鐵路.md "wikilink")

## 參考資料

## 外部連結

  - [交通部鐵路改建工程局](http://www.rrb.gov.tw/)
  - [交通部臺灣鐵路管理局-臺鐵都會區捷運化桃園段高架化建設計畫](http://www.railway.gov.tw/tw/EAY980521/index.htm)
  - [台鐵都會區捷運化暨區域鐵路先期建設計畫進度報告](http://cmdweb.pcc.gov.tw/pccms/owa/pln90.bro5_2?iboscode=315&iplnno=921518025&iplncode=315180000M)
  - [台鐵都會區捷運化暨區域鐵路後續建設計畫（基隆＝苗栗）進度報告](http://cmdweb.pcc.gov.tw/pccms/owa/pln90.bro5_2?iboscode=315&iplnno=940001690&iplncode=315180000M)
  - [交通部台灣鐵路管理局-臺鐵簡介-鐵路改善工程專案](http://www.railway.gov.tw/intro/introduction-8-2.aspx)
  - [交通部業務概況－鐵路](http://www.motc.gov.tw/hypage.cgi?HYPAGE=about_3_3.asp)

[台鐵捷運化](../Category/台鐵捷運化.md "wikilink")

1.  [97年度新十大建設特別預算經費申請與核列彙總表單位：億元](http://www.cepd.gov.tw/dn.aspx?uid=4385)，行政院經濟建設委員會

2.  [花東鐵路雙軌化可行性研究
    國發會通過](http://news.ltn.com.tw/news/life/paper/1079954),2017年2月21日,[自由時報](../Page/自由時報.md "wikilink")

3.  [張院長參加台鐵基隆三坑仔站動工典禮
    表示增設都會區通勤車站有助台鐵轉型與再生](http://info.gio.gov.tw/ct.asp?xItem=22299&ctNode=3902&mp=1)，行政院新聞局

4.  [台灣地區軌道運輸系統整合規劃](http://www.iot.gov.tw/public/Data/7121810505471.pdf)，行政院新聞局

5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15. [鐵路高架化
    彰化站擬建商城](http://www.libertytimes.com.tw/2012/new/dec/29/today-center10.htm)


16.
17. 八卦山下彰化市月刊

18. 《台鐵兼具都會區捷運功能暨增設通勤車站評估規劃定案報告》，中華顧問工程司，2003

19.
20.
21.
22. 《臺鐵兼具都會區捷運功能暨增設通勤車站評估規劃》原先建議於世賢路三段，增設「南嘉義」車站。唯後續鐵路高架化案中，僅增設北回車站，未規劃南嘉義車站。

23.
24.
25. 《臺鐵兼具都會區捷運功能暨增設通勤車站評估規劃》原先建議於臺南保安間增設文化中心(355.7K \~
    356.8K)、臺南航空站(358.1K \~
    359.2K)兩站。唯後續鐵路地下化案中，將文化中心站南移至南臺南站址、新設林森站以及取消臺南航空站。

26.
27.
28.
29.
30. [屏東鐵路高架
    屏市增通勤車站](http://www.cna.com.tw/news/aloc/201406050112-1.aspx)，2014年6月5日，中央社

31. 《捷運白皮書-4,444億的教訓》，作者：劉寶傑，呂紹煒/合著，時報出版；出版日期：1994年10月21日

32. [如何到台南園區](http://www.stsipa.gov.tw/web/WEB/Jsp/Page/cindex.jsp?frontTarget=DEFAULT&pageID=2102&thisRootID=235)
    ，南部科學工業園區