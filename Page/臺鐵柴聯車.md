**柴聯車**，亦稱**DMU**（**Diesel Multiple
Unit**）是指[臺灣鐵路管理局轄內](../Page/臺灣鐵路管理局.md "wikilink")，以二到三輛編成固定組合，並以[柴油為動力之車組](../Page/柴油.md "wikilink")，目前多用於台鐵[東部幹線及](../Page/台東線.md "wikilink")[南迴線](../Page/南迴線.md "wikilink")、少部份用於[西部幹線的](../Page/縱貫線.md "wikilink")[自強號列車](../Page/自強號列車.md "wikilink")，另2015年10月15日改點後，亦有[屏東線往返](../Page/屏東線.md "wikilink")[潮州](../Page/潮州車站.md "wikilink")－[枋寮間的](../Page/枋寮車站.md "wikilink")[區間車及往返](../Page/臺鐵區間車.md "wikilink")[新左營](../Page/新左營車站.md "wikilink")－[枋寮間的](../Page/枋寮車站.md "wikilink")[區間快班次](../Page/臺鐵區間快車.md "wikilink")。全名為[動力分散式](../Page/動力分散式.md "wikilink")[柴聯車](../Page/柴聯車.md "wikilink")。

[臺灣鐵路管理局的柴聯車皆為日系車輛](../Page/臺灣鐵路管理局.md "wikilink")，車組形式均為「**動力駕駛車**—**電源拖車**—**動力駕駛車**」，電源拖車亦裝有柴油引擎，負責發電供給全組列車的[冷氣及](../Page/冷氣.md "wikilink")[照明設備](../Page/照明.md "wikilink")。本型列車於臺鐵旗下目前共有五種型式，部分型式可互相聯掛後進行總控運轉。

## 車籍資料

### [DR2700型](../Page/台鐵DR2700型柴油車.md "wikilink")

[TRA_DR2700_01.JPG](https://zh.wikipedia.org/wiki/File:TRA_DR2700_01.JPG "fig:TRA_DR2700_01.JPG")

  - 1966年製造
  - 日本[東急車輛製](../Page/東急車輛.md "wikilink")
  - [固敏式NHHRTO](../Page/康明斯.md "wikilink")-6-B1[引擎](../Page/引擎.md "wikilink")（現已改為固敏式NT855-R4引擎）
  - 設計最高時速110km/hr
  - 原25輛，現存15輛。因不鏽鋼車體，外號「白鐵仔」。
  - 2輛被改裝為[台鐵電務段專用工程車](../Page/台鐵電務段.md "wikilink")：DR2712改裝為CMB29，DR2719改裝為CMB30，因端面特殊塗裝又被鐵道迷稱為「皮卡丘」。
  - DR2701、DR2702、DR2704、DR2705、DR2708、DR2709、DR2710、DR2713報廢
  - 除DR2702、DR2704、DR2708、DR2713報廢拆解外，其餘報廢後皆為私人收藏
  - DR2711、DR2720兩輛2014年初已停用
  - 無動力拖車編為DR2750型已全數報廢，保留車為DR2752。
  - 原作為台鐵[光華號對號特快](../Page/光華號.md "wikilink")、柴油對號快車、柴油快車、普快車使用。
  - 曾於2005年8月到2006年1月及2011年2月中行駛林口線免費客運。
  - [西部幹線通勤列車由通勤電聯車接替後](../Page/西部幹線.md "wikilink")，轉移到台鐵[台東線作為普快車營運](../Page/臺東線.md "wikilink")，或作為特殊活動時的專列方式出現於各線。
  - 末期運用車次為4671、4673、4674、4675、4676、4677、4678、4682、4683次（花東鐵路電氣化後改以EMU500行駛）。
  - 已於2014年7月16日退出常規營運，保存車作為特別列車使用。

### [DR2800型](../Page/台鐵DR2800型柴聯車.md "wikilink")

[TRA_DR2800_01.JPG](https://zh.wikipedia.org/wiki/File:TRA_DR2800_01.JPG "fig:TRA_DR2800_01.JPG")

  - 1982年製造
  - [日本](../Page/日本.md "wikilink")[東急車輛製](../Page/東急車輛.md "wikilink")
  - 日本[日立電機系統](../Page/日立.md "wikilink")
  - 原始採用[固敏式NT](../Page/康明斯.md "wikilink")855-R4[引擎](../Page/引擎.md "wikilink")，現已改裝[固敏式NTA](../Page/康明斯.md "wikilink")855-R1引擎
  - 最高時速110KM/Hr
  - 15組（45輛）
  - 單組編組：DR2800-DR2850-DR2800
  - 本組車編號前9組27輛的動力車（DR2801到DR2818）已被打散，部分車輛的方向也有顛倒情況
  - 外觀上車身下方有弧度，類似DR2700型，與DR2700型相比，窗戶加長並加裝空調。
  - 目前全部車組車門已改造成自動門，列車終點指示牌也更新為[LED顯示器](../Page/LED.md "wikilink")。

### [DR2900型](../Page/台鐵DR2900型柴聯車.md "wikilink")

[TRA_DR2900_DR3000_01.JPG](https://zh.wikipedia.org/wiki/File:TRA_DR2900_DR3000_01.JPG "fig:TRA_DR2900_DR3000_01.JPG")

  - 1986年引進
  - [日本](../Page/日本.md "wikilink")[日立製](../Page/日立.md "wikilink")
  - 日本日立電機系統
  - 原採用[固敏式NT](../Page/康明斯.md "wikilink")855-R4[引擎](../Page/引擎.md "wikilink")，現已更新為[固敏式N](../Page/康明斯.md "wikilink")14-R4[引擎](../Page/引擎.md "wikilink")
  - 最高時速110KM/Hr
  - 5組（15輛）
  - 單組編組：DR2900-DR2950-DR2900
  - 與後來引進的台鐵DR3000型為同型車
  - 原本車體側邊中央部分無塗裝[臺灣鐵路管理局的](../Page/臺灣鐵路管理局.md "wikilink")「局徽」，當時為與DR3000型分辨的基本方式，目前已全數加上局徽
  - 目前全部車組車門已改造成自動門，列車終點指示牌也更新為LED顯示器
  - 本型其中一組曾改車體塗裝作為「[嘟嘟列車](../Page/嘟嘟列車.md "wikilink")」運行於[高雄臨港環狀線](../Page/高雄.md "wikilink")，現已經改回一般塗裝並回歸原編組

### [DR3000型](../Page/台鐵DR3000型柴聯車.md "wikilink")

[TRA_DR3000_Tzu-Chiang_Express_at_Songshan_Station.jpg](https://zh.wikipedia.org/wiki/File:TRA_DR3000_Tzu-Chiang_Express_at_Songshan_Station.jpg "fig:TRA_DR3000_Tzu-Chiang_Express_at_Songshan_Station.jpg")

  - 1990年引進，與DR2900為同型車
  - [日本](../Page/日本.md "wikilink")[日立製](../Page/日立.md "wikilink")
  - [日本日立電機系統](../Page/日本.md "wikilink")
  - 原採用[固敏式NT](../Page/康明斯.md "wikilink")855-R4[引擎](../Page/引擎.md "wikilink")，現已更新為[固敏式N](../Page/康明斯.md "wikilink")14-R4[引擎](../Page/引擎.md "wikilink")
  - 最高時速110KM/Hr
  - 27組（81輛）
  - 單組編組：DR3000-DR3070-DR3000
  - DR3017、DR3045、DR3093、DR3051、DR3096、DR3052號因行車事故而報廢，前三輛事故殘餘車重新搭配重組，新編組改為DR3046-DR3079-DR3018，為DR3000編組中之特例，另外DR3048-DR3094-DR3047因2016年6月22日富源出軌事故後整組報廢，
    45DR3051-40DR3096-45DR3052因2013年南迴線土石流事故加上花東鐵路於2014年7月16日電氣化完工，於2015年在花蓮機廠宣告報廢標售
  - 目前全部車組車門已改造成自動門，列車終點指示牌也更新為LED顯示器

### [DR3100型](../Page/台鐵DR3100型柴聯車.md "wikilink")

[TRA_DR3100_01.JPG](https://zh.wikipedia.org/wiki/File:TRA_DR3100_01.JPG "fig:TRA_DR3100_01.JPG")

  - 1998年引進
  - [日本](../Page/日本.md "wikilink")[日本車輛製](../Page/日本車輛.md "wikilink")
  - 日本日立電機系統
  - [固敏式NTA](../Page/康明斯.md "wikilink")855-R1[引擎](../Page/引擎.md "wikilink")
  - 最高時速110KM/Hr
  - 10組（30輛）
  - 單組編組：DR3100-DR3150-DR3100
  - DR3100型的動力駕駛車有兩組車門（DR2800，DR2900，DR3000三型的動力駕駛車皆只有一組車門），方便旅客上下車。
  - DR3100型車的上下車門原始設計即為車長聯控開啟的全自動滑門。其他三種柴聯[自強號列車原先設計為折疊式手動門](../Page/自強號列車.md "wikilink")（目前皆已改為全自動滑門）
  - 柴聯[自強號列車四種車型中](../Page/自強號列車.md "wikilink")，唯一設有無障礙座位及無障礙設施的車型
  - 2008年中旬起，列車終點指示牌陸續改裝為LED顯示器，目前全部車組已改裝完成

## 意外事故

  - 1997年3月27日：由[樹林車站開往](../Page/樹林車站.md "wikilink")[蘇澳車站的](../Page/蘇澳車站.md "wikilink")[自強號列車](../Page/自強號列車.md "wikilink")1083次（DR3045-DR3093-DR3046=DR3039-DR3090-DR3040）於[外澳車站事故中前兩輛](../Page/外澳車站.md "wikilink")（DR3045和DR3093）撞毀報廢。

<!-- end list -->

  - 2002年8月9日：由[彰化車站開往](../Page/彰化車站.md "wikilink")[豐原車站的自強迴送車](../Page/豐原車站.md "wikilink")2051A次（DR3017-DR3079-DR3018=DR3005-DR3073-DR3006=DR3011-DR3076-DR3012，未載客）於[台中車站事故中前四節撞毀](../Page/臺中車站.md "wikilink")，僅DR3017一輛報廢。

<!-- end list -->

  - 2010年4月5日：由[臺東車站開往](../Page/臺東車站.md "wikilink")[豐原車站的自強號](../Page/豐原車站.md "wikilink")2056次於[西勢車站到](../Page/西勢車站.md "wikilink")[麟洛車站間](../Page/麟洛車站.md "wikilink")，其中一節車DR3071發生車體引擎脫落事件。

<!-- end list -->

  - 2012年6月15日：由[新左營車站開往](../Page/新左營車站.md "wikilink")[花蓮車站的自強號](../Page/花蓮車站.md "wikilink")305次於[大武車站到](../Page/大武車站.md "wikilink")[瀧溪車站間因台灣地區多日豪雨導致](../Page/瀧溪車站.md "wikilink")[土石流](../Page/土石流.md "wikilink")，DR3108於此處發生列車出軌。

<!-- end list -->

  - 2013年8月31日：由[臺東車站開往](../Page/臺東車站.md "wikilink")[新左營車站的自強號](../Page/新左營車站.md "wikilink")302次(DR3051-DR3096-DR3052=DR3003-DR3072-DR3004)於[枋野車站到](../Page/枋野車站.md "wikilink")[加祿車站間遭受土石流衝擊](../Page/加祿車站.md "wikilink")。導致列車車廂脫軌分離10公尺。其中受損較嚴重的DR3051-DR3096-DR3052編組，因為整體修復經費龐大且[東部幹線年底即將完成電氣化工程等營運考量](../Page/花東線.md "wikilink")，目前[台鐵暫決定不予修復](../Page/台鐵.md "wikilink")，並作為零件器官車供同型車維修之用。2015年7月6日，台鐵公告45DR3051-40DR3096-45DR3052編組報廢標售。
  - 2015年5月1日:由台中開往台東的自強號371次停在斗六車站,第5節車廂(DR3155)
    車軸卡死起火冒出大量濃煙,幸撲滅火勢,無人員傷亡

<!-- end list -->

  - 2016年6月22日：由[新左營開往](../Page/新左營車站.md "wikilink")[花蓮的自強號](../Page/花蓮車站.md "wikilink")307次，其中的7-9節車廂(DR3048-DR3094-DR3047)於瑞穗-富源間發生出軌意外，車上當時有2位中國的自由行旅客受傷，後續已送往醫院就醫，並加派公路遊覽車輸送旅客。車廂的轉向架則皆有脫落的情形。[臺鐵局先將未出軌的](../Page/臺灣鐵路管理局.md "wikilink")1-6節車廂與7-9車廂編組解聯後由1-6節車廂繼續單獨行駛至終點站[花蓮](../Page/花蓮車站.md "wikilink")。並於隔日10:37分搶通。

## 列車圖片

  - DR2700

<File:Diesel> Railcar in LinKou Line of Taiwan Railway-P1020697.JPG
<File:DR2700> Neishi.JPG <File:DR2700-ordinary-train.jpg> <File:DR2705>
at Taichung.JPG <File:DR2705> in Neiwan Line.jpg

  - DR2800

<File:TRA> DR2800 at Shoufong Station 20060503.jpg <File:TRA> Tokyu
DR2800 DMU Kaohsiung 1997.jpg

  - DR2900與DR3000

<File:TRA> DR2900 DR3000 01.JPG <File:TC> at Jiouzantou.jpg
<File:Tzu-Chiang> Is Passing the Liukuaicuo Railway Station.JPG
<File:Dodo-train.jpg> <File:TC> at Duoliang.jpg
<File:TRA_DR2900_DR3000_03.JPG>

  - DR3100

<File:TRA> DR3100 01.JPG <File:TRA> DR3100 across Fangye No.2 Tunnel
20060202.jpg

## 外部連結

  - [20091031
    DR2700型光華號懷舊之旅通過南港站](http://www.youtube.com/watch?v=zXEaS2CpxsM)（Youtube影片）

[+](../Category/臺灣鐵路管理局車輛.md "wikilink")
[Category:台灣柴聯車](../Category/台灣柴聯車.md "wikilink")