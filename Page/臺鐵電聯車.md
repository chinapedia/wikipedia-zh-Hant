**臺鐵[電聯車](../Page/電聯車.md "wikilink")**是[臺灣鐵路管理局的一種車型](../Page/臺灣鐵路管理局.md "wikilink")，官方歸類於「電車組」\[1\]，主要以三至四輛固定編組組合或八至九輛固定編成之[動力分散式鐵路車輛](../Page/動力分散式.md "wikilink")，從電車線輸入高壓電力，經變壓器傳輸至牽引馬達產生牽引動力之車組，且可多組聯掛成一列車運轉之動力客車。電聯車於台鐵營運上相當廣泛，若以車種分，約略可分[自強號與](../Page/自強號.md "wikilink")[通勤電車兩大類](../Page/台鐵通勤電聯車.md "wikilink")。

## 車型列表

<table>
<thead>
<tr class="header">
<th><p>型號</p></th>
<th><p>製造商</p></th>
<th><p>製造年份</p></th>
<th><p>營業速限</p></th>
<th><p>單編組或固定編成</p></th>
<th><p>現況</p></th>
<th><p>配屬單位</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>自強號</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台鐵EMU100型電聯車.md" title="wikilink">EMU100</a></p></td>
<td><p><a href="../Page/英國.md" title="wikilink">英國</a><a href="../Page/GEC.md" title="wikilink">GEC</a></p></td>
<td><p>1978年</p></td>
<td><p>120km/hr</p></td>
<td><p>EP-EM-ET-ET-ED</p></td>
<td><p>2009-06-15起取消日常排點，目前只有偶爾代替行駛車輛或不定期上線試運轉。</p></td>
<td><p>七堵機務段</p></td>
<td><ul>
<li>出力：1275kw/1710HP</li>
<li>加速度：0.25m/s<sup>2</sup>(0.9km/h/s)</li>
<li>原有13組(65輛)，現有11組(55輛)</li>
<li>EP - 駕駛電源動力車 - 乘員 44 人
<ul>
<li>EP109報廢，EP112改為EPD112(拆除集電弓當ED車用)</li>
</ul></li>
<li>EM - 馬達車 - 乘員 52 人
<ul>
<li>EM103/EM110報廢</li>
</ul></li>
<li>ET - 無動力拖車 - 乘員 52 人
<ul>
<li>ET101/ET113/ET116/ET119報廢</li>
</ul></li>
<li>ED - 駕駛拖車 - 乘員 44 人
<ul>
<li>ED102/ED108/ED110報廢</li>
</ul></li>
</ul>
<ul>
<li>營運編組
<ul>
<li>EP101-EM101-ET123-ET126-ED112</li>
<li>EP106-EM109-ET125-ET102-ED111</li>
<li>EP108-EM112-ET105-ET108-ED105</li>
</ul></li>
<li>停用編組
<ul>
<li>EP102-EM108-ET120-ET117-ED113</li>
<li>EP103-EM104-ET103-ET110-EPD112</li>
<li>EP104-EM111-ET107-ET109-ED101</li>
<li>EP105-EM113-ET114-ET115-ED104</li>
<li>EP107-EM106-ET106-ET111-ED103</li>
<li>EP110-EM102-ET104-ET122-ED107</li>
<li>EP111-EM105-ET124-ET112-ED106</li>
<li>EP113-EM107-ET121-ET118-ED109</li>
</ul></li>
<li>營運編組其中1組為備用車 EP111-EM105-ET124-ET112-ED106因維修考量而停用 目前作器官車維修用</li>
<li>停用編組目前5組停於七堵調車場 3組停於南港車站第三月台</li>
<li>報廢車輛ET116停於樹調北端 車輛編號已去除 目前移至北廠</li>
<li>報廢車輛(北端)EM103、EM110、ET119、ET113(南端)停於竹貨站北 目前待拍賣解體。</li>
<li>車輛報廢事故
<ul>
<li>1981年3月8日<a href="../Page/臺鐵頭前溪橋事故.md" title="wikilink">頭前溪橋事故</a>，1002次，ED110報廢。</li>
<li>1991年11月15日<a href="../Page/臺鐵造橋火車對撞事故.md" title="wikilink">造橋事故</a>，1006次，ED102、ET101報廢。</li>
<li>1994年3月18日大肚北事故，1008次，ED108報廢。</li>
<li>1994年6月22日新竹事故，1009次，EP109報廢。</li>
<li>2009年因車齡已高以及電機組老舊不易維修之故，6月16日起取消日常排點，僅擔任周日加班列車行駛。但在2010年7月4日擔任之1121次加班車故障後完全退出運用，只有偶爾代替行駛車輛或不定期上線試運轉。</li>
</ul></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台鐵EMU200型電聯車.md" title="wikilink">EMU200</a></p></td>
<td><p><a href="../Page/南非.md" title="wikilink">南非</a> <a href="../Page/聯邦鐵路客貨車公司.md" title="wikilink">聯邦鐵路客貨車公司</a>(UCW)</p></td>
<td><p>1986年</p></td>
<td><p>120km/hr</p></td>
<td><p>EMC-EP-EM</p></td>
<td><p>全數<br />
改造為EMU1200型</p></td>
<td><p>-</p></td>
<td><ul>
<li>出力：1000kw/1340HP</li>
<li>加速度：0.4m/s<sup>2</sup>(1.44km/h/s)</li>
<li>原有11組(33輛)，改造時剩10組(30輛)</li>
<li>EM - 駕駛馬達車 - 乘員 48 人
<ul>
<li>EM209、EM211報廢</li>
</ul></li>
<li>EP - 電源動力車 - 乘員 48 人
<ul>
<li>EP211報廢</li>
</ul></li>
<li>EMC - 駕駛馬達車 - 乘員 48 人</li>
<li>剩餘車廂已全數改造為EMU1200型</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台鐵EMU300型電聯車.md" title="wikilink">EMU300</a></p></td>
<td><p><a href="../Page/義大利.md" title="wikilink">義大利</a> <a href="../Page/米蘭工業製造公司.md" title="wikilink">SOCIMI</a></p></td>
<td><p>1989年</p></td>
<td><p>120km/hr[2]</p></td>
<td><p>EM-EP-EMC</p></td>
<td><p>現役</p></td>
<td><p>七堵機務段</p></td>
<td><ul>
<li>出力：928kw/1243HP</li>
<li>加速度：0.6m/s<sup>2</sup>(2.16km/h/s)</li>
<li>現有6組(18輛)</li>
<li>EMU303跟EMU308因車況不佳而停用</li>
<li>EM - 駕駛馬達車 - 乘員 48 人</li>
<li>EP - 電源動力車 - 乘員 52 人</li>
<li>EMC - 駕駛馬達車 - 乘員 48 人</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台鐵EMU1200型電聯車.md" title="wikilink">EMU1200</a></p></td>
<td><p><a href="../Page/臺灣.md" title="wikilink">臺灣</a> <a href="../Page/臺灣車輛.md" title="wikilink">臺灣車輛</a>[3]</p></td>
<td><p>2002年<br />
~2003年</p></td>
<td><p>120km/hr</p></td>
<td><p>EMC-EP(B)-EM-<br />
EM-EP(M)-EM-<br />
EM-EP(B)-EMC</p></td>
<td><p>現役</p></td>
<td><p>高雄機務段</p></td>
<td><ul>
<li>出力：1000kw/1340HP</li>
<li>加速度：0.4m/s<sup>2</sup>(1.44km/h/s)</li>
<li>改造自EMU200型共30輛</li>
<li>現有3組(27輛)和備用編組3輛（備用編組已停用）</li>
<li>EM - 馬達車 - 乘員 48 人</li>
<li>EP - 電源動力車 - 乘員 48 人</li>
<li>EMC - 駕駛馬達車，貫通門拆除 - 乘員 48 人</li>
<li>僅單組9輛運用</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台鐵TEMU1000型電聯車.md" title="wikilink">TEMU1000</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/日立製作所.md" title="wikilink">日立製作所</a></p></td>
<td><p>2006年<br />
~2007年</p></td>
<td><p>130km/hr</p></td>
<td><p>TED-TEM-TEP-TEM-<br />
TEM-TEP-TEM-TED</p></td>
<td><p>現役</p></td>
<td><p>台北機務段</p></td>
<td><ul>
<li><strong><a href="../Page/太魯閣號.md" title="wikilink">太魯閣號</a></strong>專用車型</li>
<li><a href="../Page/傾斜式列車.md" title="wikilink">傾斜式列車</a></li>
<li>出力：3,040kW/4,075HP（兩組編成）</li>
<li>加速度：0.61m/s<sup>2</sup>(2.2km/h/s)</li>
<li>現有16組（64輛）</li>
<li>行駛時為2組聯掛為1列，共可組成8列。</li>
<li>TED - 傾斜式駕駛拖車 - 乘員 38 人</li>
<li>TEM - 傾斜式馬達車 - 乘員 52 人</li>
<li>TEP - 傾斜式電源動力車 - 乘員 48 人</li>
<li>2007年5月8日正式營運。</li>
<li>2012年1月17日，TED1010因<a href="../Page/臺鐵埔心平交道事故.md" title="wikilink">臺鐵埔心平交道事故嚴重損毀</a></li>
<li>2015年2月8日，台鐵宣布TED1010重造車體組裝修復完成，於2015年3月25日重新投入營運</li>
<li>2014年12月，受東部幹線的運量提升以及日圓貶值的影響之下使得購買TEMU2000型的預算有結餘款，因此決定再增購TEMU1000型2編組共16輛，現時TEMU1000型一共有16個編組共64輛。增購的其中一列TEMU1000型於2016年1月12日交車，另一列也已於2016年2月25日交車。</li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台鐵TEMU2000型電聯車.md" title="wikilink">TEMU2000</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/日本車輛製造.md" title="wikilink">日本車輛製造</a></p></td>
<td><p>2012年<br />
~2015年</p></td>
<td><p>140km/hr</p></td>
<td><p>TED-TEMA-TEP-TEMB-<br />
TEMB-TEP-TEMA-TED</p></td>
<td><p>現役</p></td>
<td><p>台北機務段/花蓮機務段</p></td>
<td><p>配屬機務段：</p>
<ul>
<li><ul>
<li>TEMU2001～2006；2009～2016；2019+2020;2035～2038：臺北機務段</li>
<li>TEMU2017~2018;2021~2034：花蓮機務段</li>
</ul></li>
<li>2018年10月21日，TEMU2007+2008因<a href="../Page/2018年宜蘭普悠瑪列車出軌事故.md" title="wikilink">2018年宜蘭普悠瑪列車出軌事故嚴重損毀而整組報廢除籍</a></li>
<li><strong><a href="../Page/普悠瑪號.md" title="wikilink">普悠瑪號</a></strong>專用車型</li>
<li><a href="../Page/傾斜式列車.md" title="wikilink">傾斜式列車</a></li>
<li>出力：3,520kW/4,718HP（兩組編成）</li>
<li>加速度：0.61m/s<sup>2</sup>(2.2km/h/s)</li>
<li>現有36組（144輛）</li>
<li>行駛時為2組聯掛為1列，共可組成19列。</li>
<li>TED - 傾斜式駕駛拖車 - 乘員 36 人</li>
<li>TEMA/B - 傾斜式馬達車 - 乘員 52 人</li>
<li>TEP - 傾斜式電源動力車 - 乘員 48 人</li>
<li>2013年2月6日正式營運。</li>
<li>2014年12月，受東部幹線的運量提升以及日圓貶值的影響之下使得購買TEMU2000型的預算有結餘款，因此決定再增購TEMU2000型2編組共16輛。增購的兩列TEMU2000型已於2015年12月24日交車，現時TEMU2000型總計為19列38組152輛。<a href="../Page/2018年宜蘭普悠瑪列車出軌事故.md" title="wikilink">2018年宜蘭普悠瑪列車出軌事故後</a>，僅剩18列36組144輛仍可正常運轉。</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>形式未定</p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/日立製作所.md" title="wikilink">日立製作所</a></p></td>
<td><p>預計2021年－2025年</p></td>
<td><p>140km/hr</p></td>
<td></td>
<td><p>製造中</p></td>
<td></td>
<td><ul>
<li>首度採用12輛固定編成</li>
<li>預定引進50列(600輛)</li>
</ul></td>
</tr>
<tr class="even">
<td><p>區間車</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台鐵EMU400型電聯車.md" title="wikilink">EMU400</a></p></td>
<td><p><a href="../Page/聯邦鐵路客貨車公司.md" title="wikilink">南非聯合鐵路客貨車公司</a>(UCW)</p></td>
<td><p>1990年</p></td>
<td><p>110km/hr</p></td>
<td><p>EMC-ET-EP-EM</p></td>
<td><p>2015-03-24起取消正班車運用，改為偶爾代替行駛車輛或不定期上線試運轉。</p></td>
<td><p>新竹機務段</p></td>
<td><ul>
<li>出力：1920kw/2574HP</li>
<li>原有12組（48輛），現有2組（8輛）</li>
<li>EM - 駕駛馬達車 - 乘員 60/120 人[4]</li>
<li>EP - 電源動力車 - 乘員 60/120 人</li>
<li>ET - 拖車 - 乘員 60/120 人</li>
<li>EMC - 駕駛馬達車 - 乘員 60/120 人</li>
<li>僅剩EMU405、EMU410計2組1列編組尚能正常營運。</li>
<li>因車齡已高以及電機組老舊不易維修之故，2015年3月24日起取消正班車運用，改為偶爾代替行駛車輛或不定期上線試運轉。</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台鐵EMU500型電聯車.md" title="wikilink">EMU500</a></p></td>
<td><p><a href="../Page/南韓.md" title="wikilink">南韓大宇重工</a></p></td>
<td><p>1995年<br />
~1997年</p></td>
<td><p>110km/hr</p></td>
<td><p>EMC-EP-ET-EM</p></td>
<td><p>現役</p></td>
<td><p>台北／花蓮／新竹/彰化／嘉義機務段</p></td>
<td><ul>
<li>出力：2000kw/2681HP</li>
<li>加速度：0.8m/s<sup>2</sup>(2.88km/h/s)</li>
<li>原有86組（344輛），現有85組（340輛）</li>
<li>EM - 駕駛馬達車 - 乘員 60/120 人</li>
<li>EP - 電源動力車 - 乘員 60/120 人</li>
<li>ET - 拖車 - 乘員 60/120 人</li>
<li>EMC - 駕駛馬達車 - 乘員 60/120 人</li>
<li>配屬：
<ul>
<li>EMU501～507、509～518、578～579：台北機務段</li>
<li>EMU580～586：新竹機務段</li>
<li>EMU519～533：彰化機務段</li>
<li>EMU534、552～577：嘉義機務段</li>
<li>EMU535～551：花蓮機務段</li>
</ul></li>
</ul>
<ul>
<li>車輛報廢事故
<ul>
<li>2007年6月15日<a href="../Page/臺鐵大里車站事故.md" title="wikilink">大里車站事故</a>，EP508、ET508毀損嚴重無法修復，連帶影響EM508與EMC508，等同整組列車(EMU508)報廢。目前EM、EMC508已遷移至台北機廠富岡基地內當做器官車及改造試驗車備用，EP及ET508則以帆布覆蓋並繼續棄置在七堵貨場內至今。</li>
</ul></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台鐵EMU600型電聯車.md" title="wikilink">EMU600</a></p></td>
<td><p>南韓<a href="../Page/現代Rotem.md" title="wikilink">現代Rotem</a>[5]</p></td>
<td><p>2001年<br />
~2002年</p></td>
<td><p>110km/hr</p></td>
<td><p>EMC-EP-ET-EM</p></td>
<td><p>現役</p></td>
<td><p>新竹/嘉義機務段</p></td>
<td><ul>
<li>出力：2000kw/2681HP</li>
<li>加速度：0.8m/s<sup>2</sup>(2.88km/h/s)</li>
<li>現有14組(56輛)</li>
<li>EMC - 駕駛馬達車 - 乘員 62/118 人</li>
<li>EP - 電源動力車 - 乘員 64/116 人</li>
<li>ET - 拖車 - 乘員 52/128 人</li>
<li>EM - 駕駛馬達車 - 乘員 62/118 人</li>
<li>配屬：
<ul>
<li>EMU601~EMU609:新竹機務段</li>
<li>EMU610~EMU614:嘉義機務段</li>
</ul></li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台鐵EMU700型電聯車.md" title="wikilink">EMU700</a></p></td>
<td><p>臺灣<a href="../Page/臺灣車輛.md" title="wikilink">臺灣車輛</a>（日本<a href="../Page/日本車輛.md" title="wikilink">日本車輛示範及監造</a>）</p></td>
<td><p>2007年<br />
~2008年</p></td>
<td><p>110km/hr[6]</p></td>
<td><p>EMC-EP-ET-EM-<br />
EM-ET-EP-EMC</p></td>
<td><p>現役</p></td>
<td><p>新竹機務段</p></td>
<td><ul>
<li>出力：3740kw/4870HP(兩組編成)</li>
<li>加速度：1.0m/s<sup>2</sup>(3.6km/h/s)</li>
<li>現有40組(160輛)</li>
<li>行駛時為2組聯掛為1列。</li>
<li>EMC - 駕駛馬達車 - 乘員 52/112 人</li>
<li>EP - 電源動力車 - 乘員 50/120 人</li>
<li>ET - 拖車 - 乘員 42/132 人</li>
<li>EM - 馬達車 - 乘員 60/122 人</li>
<li>12輛車廂在2007年1月由日本運抵台灣，其餘148輛由<a href="../Page/台灣車輛.md" title="wikilink">台灣車輛組裝</a>。</li>
<li>2007年8月29日先投入一組營運，11月20日再投入兩組營運，目前已全部交車完畢。</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台鐵EMU800型電聯車.md" title="wikilink">EMU800</a></p></td>
<td><p>臺灣<a href="../Page/臺灣車輛.md" title="wikilink">臺灣車輛</a>（日本<a href="../Page/日本車輛.md" title="wikilink">日本車輛示範及監造</a>）</p></td>
<td><p>2012年<br />
~2016年</p></td>
<td><p>110~130km/hr</p></td>
<td><p>ED-EMA-EP-EMB-<br />
EMB-EP-EMA-ED</p></td>
<td><p>現役</p></td>
<td><p>新竹機務段、嘉義機務段、彰化機務段、臺北機務段</p></td>
<td><ul>
<li>配屬機務段:
<ul>
<li>EMU801~EMU806:臺北機務段</li>
<li>EMU807~834;881~892（增備車編組）：新竹機務段</li>
<li>EMU869~874:彰化機務段</li>
<li>EMU835～868：嘉義機務段</li>
</ul></li>
<li>出力：3740kw/4870HP(兩組編成)</li>
<li>加速度：1.0m/s<sup>2</sup>(3.6km/h/s)</li>
<li>現有86組(344輛)</li>
<li>行駛時為2組聯掛為1列。</li>
<li>ED - 駕駛拖車 - 乘員 16/114 人</li>
<li>EMA/B - 馬達車 - 乘員 56/113人</li>
<li>EP - 電源動力車 - 乘員 48/115 人</li>
<li>16輛車廂已在2013年8月由日本運抵台灣，其餘280輛車廂和68輛增購車由<a href="../Page/台灣車輛.md" title="wikilink">台灣車輛組裝</a>。</li>
<li>2014年1月2日兩組原型車開始載客營運，1月10日起量產車陸續開始載客營運，目前已全部交車完畢。</li>
<li>2015年6月9日因為日幣貶值關係，使得當初的採購本型車經費因匯差而產生餘款，台鐵宣布將再增購本型車共計6列12組48輛。增購之後將從原本的37列74組增為43列86組，與EMU500型的數量一致。增購部分於2016年8月起開始分批交車，於同年底全數投入營運。</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台鐵EMU900型電聯車.md" title="wikilink">EMU900</a></p></td>
<td><p>南韓<a href="../Page/現代Rotem.md" title="wikilink">現代Rotem</a></p></td>
<td><p>預計2019年－2024年</p></td>
<td><p>130 km/h</p></td>
<td></td>
<td><p>製造中</p></td>
<td></td>
<td><ul>
<li>首度採用10輛固定編成</li>
<li>預定引進52列(520輛)</li>
</ul></td>
</tr>
</tbody>
</table>

## 列車圖片

<File:TRA> EMU100 at Suao Station.jpg|EMU100 Taiwan Railway
Administration EMU 206 Banqiao 1989.jpg|EMU200 <File:TRA> EMU1200 at
Taichung Station.jpg|EMU1200 <File:TRA> EMU300 01.JPG|EMU300 <File:TRA>
EMC412 in Dingpu Station 20070705.jpg|EMU400 <File:TRA> EMU502 leaving
Dasi Station 20060630.jpg|EMU500型
[File:加裝ITS系統後的EMU600型電聯車.jpg|EMU600型](File:加裝ITS系統後的EMU600型電聯車.jpg%7CEMU600型)
<File:TRA> EMU700 at Songshan Station 20070821.jpg|EMU700 <File:台中車站>
ED861 20160713.jpg|EMU800(原型車) <File:TRA> ED881 at Changhua Station
20161109.jpg|EMU800(增備車) <File:TEMU1000> at Xike Station.jpg|TEMU1000
TRA TEMU2000 TED2007 Xizhi 20130807.jpg|TEMU2000

## 相關條目

  - [自強號](../Page/自強號列車.md "wikilink")
  - [通勤電聯車](../Page/台鐵通勤電聯車.md "wikilink")
  - [太魯閣列車](../Page/太魯閣列車.md "wikilink")
  - [普悠瑪列車](../Page/普悠瑪列車.md "wikilink")

## 備註

[Category:台灣電聯車](../Category/台灣電聯車.md "wikilink")
[+](../Category/臺灣鐵路管理局車輛.md "wikilink")

1.  電車組中還包含了推拉式電車組（自強號）。
2.  EMU300型的建議營業速限為120km/hr，設計速限為130km/hr，雖然在臺鐵的營業速限為130km/hr，但在轉向架更新後已無法達到。
3.  臺灣車輛為臺灣[唐榮鐵工廠](../Page/唐榮鐵工廠.md "wikilink")、[日本](../Page/日本.md "wikilink")[日本車輛](../Page/日本車輛.md "wikilink")、日本[住友商事及臺灣](../Page/住友商事.md "wikilink")[中國鋼鐵公司合資而成](../Page/中國鋼鐵公司.md "wikilink")。
4.  座位 / 立位。
5.  EMU600型招標時，得標公司為1999年由[大宇重工](../Page/大宇重工.md "wikilink")、[現代精工](../Page/現代精工.md "wikilink")、[韓進重工等三家公司的鐵路部門合組成的](../Page/韓進重工.md "wikilink")[KOROS公司](../Page/KOROS.md "wikilink")（Korea
    Rolling
    Stock；[韓國鐵路車輛公司](../Page/韓國鐵路車輛公司.md "wikilink")），當時主事者多為前大宇重工的員工，得標後因股權變動再更名為Rotem公司（現代發動機集團Hyundai
    Motor
    Group成員）。在招標過程中，現代精工也是有單獨投標，甚至作了一組試作車；但直到1999年KOROS成立後，三家母公司依約定不得再參與任何韓國或海外的車輛標案。
6.  EMU700型的設計速限為120km/hr，但台鐵將其營運速限訂為110km/hr，與EMU400-600相同。