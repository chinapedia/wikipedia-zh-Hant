**臼杵市**（）是位于[日本](../Page/日本.md "wikilink")[大分縣東部的一個](../Page/大分縣.md "wikilink")[城市](../Page/城市.md "wikilink")，與大分市位於相同的生活圈。

東側面向[臼杵灣](../Page/臼杵灣.md "wikilink")，主要是區位於[臼杵川河口附近的平原地區](../Page/臼杵川.md "wikilink")；內陸區域的北部為丘陵，南部為約海拔500[公尺的山區](../Page/公尺.md "wikilink")。氣候紹年均溫在15至17度之間，平均降雨量為1,500至1,800[公厘之間](../Page/公厘.md "wikilink")。\[1\]

「臼杵」的名稱被認為源自轄區內[臼冢古墳入口處的石甲](../Page/臼冢古墳.md "wikilink")，由於該石甲外型與「臼」和「杵」相似，因此產生這樣的地名。\[2\]

## 歷史

[戰國時代](../Page/戰國時代_\(日本\).md "wikilink")[大友宗麟在](../Page/大友宗麟.md "wikilink")[丹生島上建築了丹生島城](../Page/丹生島.md "wikilink")（[臼杵城](../Page/臼杵城.md "wikilink")），周邊也開始以[城下町的型態發展](../Page/城下町.md "wikilink")，並成為和[中國](../Page/中國.md "wikilink")、[葡萄牙的商人往來的商業城市](../Page/葡萄牙.md "wikilink")。\[3\]

[大友氏之後](../Page/大友氏.md "wikilink")，臼杵先後成為[福原氏](../Page/福原氏.md "wikilink")、[太田氏的領地](../Page/太田氏.md "wikilink")，直到1600年[稻葉貞通由](../Page/稻葉貞通.md "wikilink")[美濃改封於此](../Page/美濃國.md "wikilink")，直到[明治維新實施](../Page/明治維新.md "wikilink")[廢藩置縣為止的兩百多年間都屬於](../Page/廢藩置縣.md "wikilink")[稻葉氏的領地](../Page/稻葉氏.md "wikilink")。現今的臼杵市區規劃幾乎都是在稻葉氏治理的時期所完成。

1877年[西南戰爭期間](../Page/西南戰爭.md "wikilink")，臼杵一度成為西鄉軍與政府軍的戰場，並受到戰火波及。

### 年表

  - 1871年：[廢藩置縣時](../Page/廢藩置縣.md "wikilink")，曾設置為臼杵縣。
  - 1878年：隸屬[北海部郡](../Page/北海部郡.md "wikilink")，同時在臼杵町設置郡役所。
  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬
      - 北海部郡：**臼杵町**、[市濱村](../Page/市濱村.md "wikilink")、[下南津留村](../Page/下南津留村.md "wikilink")、[上浦村](../Page/上浦村.md "wikilink")、[海邊村](../Page/海邊村.md "wikilink")、[佐志生村](../Page/佐志生村.md "wikilink")、[下之江村](../Page/下之江村.md "wikilink")、[上北津留村](../Page/上北津留村.md "wikilink")、[下北津留村](../Page/下北津留村.md "wikilink")、[南津留村](../Page/南津留村.md "wikilink")
      - [大野郡](../Page/大野郡_\(大分縣\).md "wikilink")：野津市村、[田野村](../Page/田野村.md "wikilink")、[南野津村](../Page/南野津村.md "wikilink")、[川登村](../Page/川登村.md "wikilink")。
  - 1907年7月1日：市濱村、下南津留村、上浦村被[併入臼杵町](../Page/市町村合併.md "wikilink")。
  - 1949年4月1日：野津市村改制為野津市町。
  - 1949年7月1日：野津市町改名為[野津町](../Page/野津町.md "wikilink")。
  - 1950年4月1日：臼杵町和海邊村合併為**臼杵市**。
  - 1951年4月1日：田野村被併入野津町。
  - 1954年3月31日：佐志生村、下之江村、上北津留村、下北津留村、南津留村被併入臼杵市。
  - 1955年3月28日：南野津村、川登村被併入野津町。
  - 2005年1月1日：野津町與臼杵市合併為新設置的**臼杵市**。

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1988年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>北海部郡<br />
臼杵町</p></td>
<td><p>1907年7月1日<br />
臼杵町</p></td>
<td><p>1950年4月1日<br />
臼杵市</p></td>
<td><p>2005年1月1日<br />
合併為臼杵市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>北海部郡<br />
下南津留村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>北海部郡<br />
市濱村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>北海部郡<br />
上浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>北海部郡<br />
海邊村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>北海部郡<br />
佐志生村</p></td>
<td><p>1954年3月31日<br />
併入臼杵市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>北海部郡<br />
下之江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>北海部郡<br />
下北津留村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>北海部郡<br />
上北津留村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>北海部郡<br />
上南津留村</p></td>
<td><p>1907年7月1日<br />
南津留村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>北海部郡<br />
中臼杵村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大野郡<br />
野津市村</p></td>
<td><p>1949年4月1日<br />
野津市町</p></td>
<td><p>1949年7月1日<br />
改名為野津町</p></td>
<td><p>1951年4月1日<br />
野津町</p></td>
<td><p>1955年3月28日<br />
野津町</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>大野郡<br />
田野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大野郡<br />
南野津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>大野郡<br />
川登村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [日豐本線](../Page/日豐本線.md "wikilink")：[佐志生車站](../Page/佐志生車站.md "wikilink")
        - [下之江車站](../Page/下之江車站.md "wikilink") -
        [熊崎車站](../Page/熊崎車站.md "wikilink") -
        [上臼杵車站](../Page/上臼杵車站.md "wikilink") -
        [臼杵車站](../Page/臼杵車站.md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [東九州自動車道](../Page/東九州自動車道.md "wikilink")：[臼杵交流道](../Page/臼杵交流道.md "wikilink")

## 觀光資源

[Usukisekibutsu2.jpg](https://zh.wikipedia.org/wiki/File:Usukisekibutsu2.jpg "fig:Usukisekibutsu2.jpg")
[Panoramic_view_of_Usuki_Stone_Buddhas.jpg](https://zh.wikipedia.org/wiki/File:Panoramic_view_of_Usuki_Stone_Buddhas.jpg "fig:Panoramic_view_of_Usuki_Stone_Buddhas.jpg")[佛像的全景](../Page/佛像.md "wikilink")\]\]

### 景點

  - 臼杵磨崖佛
  - [臼杵城](../Page/臼杵城.md "wikilink")
  - 風連鍾乳洞
  - 明治橋
  - 野上彌生子文學記念館

### 祭典

  - 臼杵櫻祭：4月上旬
  - 臼杵祇園祭：7月中旬
  - 臼杵竹宵：11月上旬

## 教育

### 高等學校

  - [大分縣立臼杵高等學校](../Page/大分縣立臼杵高等學校.md "wikilink")
  - [大分縣立臼杵商業高等學校](../Page/大分縣立臼杵商業高等學校.md "wikilink")
  - [大分縣立海洋科學高等學校](../Page/大分縣立海洋科學高等學校.md "wikilink")
  - [大分縣立野津高等學校](../Page/大分縣立野津高等學校.md "wikilink")

### 特殊學校

  - [大分縣立臼杵養護學校](../Page/大分縣立臼杵養護學校.md "wikilink")

## 姊妹、友好都市

### 海外

  - [康提](../Page/康提.md "wikilink")（[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")）：於1967年2月27日締結姊妹都市。\[4\]

  - [敦煌市](../Page/敦煌市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")）：於1994年9月27日締結友好都市。\[5\]

## 本地出身之名人

  - [吉田敏浩](../Page/吉田敏浩.md "wikilink")：[記者](../Page/記者.md "wikilink")
  - [野上彌生子](../Page/野上彌生子.md "wikilink")：[小説家](../Page/小説家.md "wikilink")
  - [鹽屋俊](../Page/鹽屋俊.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [小室桂子](../Page/小室桂子.md "wikilink")：[歌手](../Page/歌手.md "wikilink")
  - [森直也](../Page/森直也.md "wikilink")（モリナオヤ）：[創作歌手](../Page/創作歌手.md "wikilink")
  - [宗茂](../Page/宗茂.md "wikilink")：前[馬拉松選手](../Page/馬拉松.md "wikilink")、前[旭化成田徑部監督](../Page/旭化成.md "wikilink")、為雙胞胎之哥哥
  - [宗猛](../Page/宗猛.md "wikilink")：前馬拉松選手、旭化成田徑部監督、為雙胞胎之弟弟
  - [吉良俊則](../Page/吉良俊則.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [鳥越裕介](../Page/鳥越裕介.md "wikilink")：前職業棒球選手
  - [廣瀨純](../Page/廣瀨純.md "wikilink")：職業棒球選手
  - [和田博實](../Page/和田博實.md "wikilink")：職業棒球球選手

## 參考資料

## 外部連結

  - [臼杵市觀光情報協會](http://www.usuki-kanko.com/)

  - [臼杵市商工會議所](https://web.archive.org/web/20081227170307/http://www5.ocn.ne.jp/~usukicci/)

<!-- end list -->

1.

2.

3.
4.
5.