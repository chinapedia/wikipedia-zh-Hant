**與野車站**（）位於[日本](../Page/日本.md "wikilink")[埼玉縣](../Page/埼玉縣.md "wikilink")[埼玉市](../Page/埼玉市.md "wikilink")[浦和區](../Page/浦和區.md "wikilink")一丁目，是[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）的[鐵路車站](../Page/鐵路車站.md "wikilink")。

## 停站路線

此站停靠的路線在軌道名稱上有[東北本線與](../Page/東北本線.md "wikilink")[武藏野線](../Page/武藏野線.md "wikilink")（大宮支線）（詳情參見路線條目與），與書類上的分歧站。與旅客列車有關、只有行走東北本線的[電車線的](../Page/電車線與列車線.md "wikilink")[京濱東北線列車停靠](../Page/京濱東北線.md "wikilink")，旅客導覽不顯示「東北（本）線」。

## 車站構造

本站是一地面車站，月台為[島式月台](../Page/島式月台.md "wikilink")1面2線設計，站房則是[跨站式站房設計](../Page/跨站式站房.md "wikilink")。由於站長室位於東口側，故本站於大部分京濱東北線車站不同，是以北行線月台為1號月台。

### 月台配置

| 月台 | 路線                                                                                                                     | 方向                                                                                                                                                                | 目的地                                       |
| -- | ---------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------- |
| 1  | [JR_JK_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JK_line_symbol.svg "fig:JR_JK_line_symbol.svg") 京濱東北線 | 北行                                                                                                                                                                | [大宮方向](../Page/大宮站_\(埼玉縣\).md "wikilink") |
| 2  | 南行                                                                                                                     | [上野](../Page/上野站.md "wikilink")、[東京](../Page/東京站.md "wikilink")、[橫濱](../Page/橫濱站.md "wikilink")、[磯子](../Page/磯子站.md "wikilink")、[大船方向](../Page/大船站.md "wikilink") |                                           |

## 使用情況

2016年度的1日平均上車人次為**26,051人**，是JR東日本範圍全體第158位\[1\]。

近年1日平均上車人次的推移如下表\[2\]。

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>1日平均<br />
上車人次</p></th>
<th><p>順位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2000年（平成12年）</p></td>
<td><p>24,687</p></td>
<td><p>155</p></td>
</tr>
<tr class="even">
<td><p>2001年（平成13年）</p></td>
<td><p>23,881</p></td>
<td><p>162</p></td>
</tr>
<tr class="odd">
<td><p>2002年（平成14年）</p></td>
<td><p>23,625</p></td>
<td><p>162</p></td>
</tr>
<tr class="even">
<td><p>2003年（平成15年）</p></td>
<td><p>24,018</p></td>
<td><p>164</p></td>
</tr>
<tr class="odd">
<td><p>2004年（平成16年）</p></td>
<td><p>24,180</p></td>
<td><p>161</p></td>
</tr>
<tr class="even">
<td><p>2005年（平成17年）</p></td>
<td><p>24,244</p></td>
<td><p>157</p></td>
</tr>
<tr class="odd">
<td><p>2006年（平成18年）</p></td>
<td><p>24,372</p></td>
<td><p>158</p></td>
</tr>
<tr class="even">
<td><p>2007年（平成19年）</p></td>
<td><p>24,915</p></td>
<td><p>158</p></td>
</tr>
<tr class="odd">
<td><p>2008年（平成20年）</p></td>
<td><p>25,112</p></td>
<td><p>157</p></td>
</tr>
<tr class="even">
<td><p>2009年（平成21年）</p></td>
<td><p>24,863</p></td>
<td><p>158</p></td>
</tr>
<tr class="odd">
<td><p>2010年（平成22年）</p></td>
<td><p>24,507</p></td>
<td><p>158</p></td>
</tr>
<tr class="even">
<td><p>2011年（平成23年）</p></td>
<td><p>24,299</p></td>
<td><p>160</p></td>
</tr>
<tr class="odd">
<td><p>2012年（平成24年）</p></td>
<td><p>24,417</p></td>
<td><p>158</p></td>
</tr>
<tr class="even">
<td><p>2013年（平成25年）</p></td>
<td><p>24,856</p></td>
<td><p>157</p></td>
</tr>
<tr class="odd">
<td><p>2014年（平成26年）</p></td>
<td><p>25,551</p></td>
<td><p>156</p></td>
</tr>
<tr class="even">
<td><p>2015年（平成27年）</p></td>
<td><p>26,063</p></td>
<td><p>156</p></td>
</tr>
<tr class="odd">
<td><p>2016年（平成28年）</p></td>
<td><p>26,051</p></td>
<td><p>158</p></td>
</tr>
</tbody>
</table>

## 历史

  - 1912年（[大正元年](../Page/大正.md "wikilink")）11月1日：應當地（原與野町）居民要求，將浦和－大宮段中間的大原號誌站（1906年（[明治](../Page/明治.md "wikilink")39年）4月16日增設）升格為**与野站**。開業時，為方便居民使用，只於面向與野町的西側設有出口（其後才增設東口），是少數沒有面向原主要道路（本站東側為中山道）的出口的車站。
  - 1973年（[昭和](../Page/昭和.md "wikilink")48年）4月1日：武藏野線貨物支線開業。
  - 1987年（昭和62年）4月1日：[國鐵分割民營化](../Page/國鐵分割民營化.md "wikilink")，本站由JR東日本接手經營。
  - 2001年（[平成](../Page/平成.md "wikilink")13年）11月18日：智能卡系統[Suica正式投入服務](../Page/Suica.md "wikilink")。

## 相鄰車站

  - 東日本旅客鐵道
    [JR_JK_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JK_line_symbol.svg "fig:JR_JK_line_symbol.svg")
    京濱東北線
      -

        快速、各站停車

          -
            [北浦和](../Page/北浦和站.md "wikilink")（JK 44）－**與野（JK
            45）**－[埼玉新都心](../Page/埼玉新都心站.md "wikilink")（JK
            46）
    武藏野線貨物支線（大宮支線、東北本線直通旅客列車全部通過）
      -

          -
            [西浦和](../Page/西浦和站.md "wikilink")（JM 27）－（）－**與野（JK 45）**

## 參考資料

## 相關條目

  - [日本鐵路車站列表](../Page/日本鐵路車站列表.md "wikilink")

## 外部連結

  -
[No](../Category/日本鐵路車站_Yo.md "wikilink")
[Category:東北本線車站](../Category/東北本線車站.md "wikilink")
[Category:浦和區鐵路車站](../Category/浦和區鐵路車站.md "wikilink")
[Category:1912年啟用的鐵路車站](../Category/1912年啟用的鐵路車站.md "wikilink")
[Category:大宮支線車站](../Category/大宮支線車站.md "wikilink")

1.  [各駅の乗車人員](http://www.jreast.co.jp/passenger/) 東日本旅客鉄道。

2.