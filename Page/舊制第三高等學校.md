<table>
<thead>
<tr class="header">
<th><p>第三高等學校<br />
（三高）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>創立</p></td>
</tr>
<tr class="even">
<td><p>所在地</p></td>
</tr>
<tr class="odd">
<td><p>初代校長</p></td>
</tr>
<tr class="even">
<td><p>廢校</p></td>
</tr>
<tr class="odd">
<td><p>同學會</p></td>
</tr>
</tbody>
</table>

**舊制第三高等學校**（[日文](../Page/日文.md "wikilink")：*きゅうせいだいさんこうとうがっこう*）為現在[京都大學前身之一的](../Page/京都大學.md "wikilink")[舊制高等學校](../Page/舊制高等學校_\(日本\).md "wikilink")，簡稱為**三高**。

## 沿革

  - 1869年（[明治](../Page/明治.md "wikilink")2年） 大阪設置**舍密局**。
  - 1870年（明治3年） 化學所→理學所→開成所→
  - 1872年（明治5年） 第四大學區第一番中學→
  - 1873年（明治6年） 第三大學區第一番中學→開明學校→
  - 1874年（明治7年） 大阪外國語學校→大阪英語學校。
  - 1879年（明治12年） 大阪専門學校。
  - 1880年（明治13年） 大阪中學校。
  - 1885年（明治18年） 大學分校。
  - 1886年（明治19年） 設立**第三高等中學校**。
  - 1887年（明治20年）
    岡山縣立醫學校改稱為第三高等中學校醫學部（位於[岡山](../Page/岡山市.md "wikilink")[榮町](../Page/榮町.md "wikilink")）。
  - 1889年（明治22年）
    第三高等中學校由[大阪市](../Page/大阪市.md "wikilink")[東區](../Page/東區_\(大阪市\).md "wikilink")（現[中央區](../Page/中央區_\(大阪市\).md "wikilink")）移往[京都市](../Page/京都市.md "wikilink")[上京區](../Page/上京區.md "wikilink")（現[左京區](../Page/左京區.md "wikilink")）。並在已有的醫學部增設法學部。
  - 1893年（明治27年）
    第三高等中學校升格為**第三高等學校**。[預科與中學部廢除](../Page/預科.md "wikilink")。設立工學部。
  - 京都帝國大學設立（1897年）之前，恢復設立大學預科。1901年廢除法學部與工學部。醫學部則遷往[岡山大學](../Page/岡山大學.md "wikilink")。
  - 1949年（昭和24年） [京都帝國大學設立](../Page/京都帝國大學.md "wikilink")，從此改稱分校。
  - 1950年（昭和25年） 第三高等學校最後一批學生畢業，正式廢除。
      - 1954年（昭和29年） 分校改稱**教養部**。
      - 1991年（平成3年） 教養部設立**人類、環境學研究科**。
      - 1992年（平成4年） 教養部改組為**總合人類學部**。教養部廢除。
      - 2003年（平成15年） 總合人類學部與人類、環境學研究科合併。

## 歷代校長

  - 初代 中島永元（1886年4月-1887年4月）
  - 2代 折田彥市（1887年4月-1910年11月）
      - 事實上的初代校長。
  - 3代 酒井佐保（1910年11月-1918年12月）
  - 4代 金子詮太郎（1919年1月-1922年8月）
  - 5代 森外三郎（1922年8月-1931年1月）
  - 6代 溝淵進馬（1931年1月-1935年9月）
  - 7代 森總之助（1935年9月-1941年4月）
  - 8代 前田鼎（1941年4月-1946年）
  - 9代 落合太郎（1946年12月-1949年5月）
  - 10代 島田退藏（1949年6月-1950年3月）

## 出身名人

### 學術

  - [湯川秀樹](../Page/湯川秀樹.md "wikilink")（理論物理學家、[諾貝爾物理學獎得主](../Page/諾貝爾物理學獎.md "wikilink")）
  - [朝永振一郎](../Page/朝永振一郎.md "wikilink")（物理學家、諾貝爾物理學獎得主）
  - [江崎玲於奈](../Page/江崎玲於奈.md "wikilink")（物理學家、諾貝爾物理學獎得主）
  - [高木貞治](../Page/高木貞治.md "wikilink")（[數學家](../Page/數學家.md "wikilink")、[文化勳章](../Page/文化勳章.md "wikilink")）
  - [岡潔](../Page/岡潔.md "wikilink")（數學家、文化勳章）
  - [今西錦司](../Page/今西錦司.md "wikilink")（[人類學家](../Page/人類學家.md "wikilink")、文化勳章）
  - [河盛好藏](../Page/河盛好藏.md "wikilink")（[法國文學專家](../Page/法國文學.md "wikilink")、文化勳章）
  - [濱田耕作](../Page/濱田耕作.md "wikilink")（[考古學家](../Page/考古學家.md "wikilink")、[京都帝國大學校長](../Page/京都帝國大學.md "wikilink")）
  - [羽田亨](../Page/羽田亨.md "wikilink")（[東洋史學家](../Page/東洋史.md "wikilink")、文化勳章）
  - [鳥養利三郎](../Page/鳥養利三郎.md "wikilink")（[電子工程專家](../Page/電子工程.md "wikilink")、京都大學校長）
  - [瀧川幸辰](../Page/瀧川幸辰.md "wikilink")（[刑法學者](../Page/刑法.md "wikilink")、京都大學校長）
  - [末川博](../Page/末川博.md "wikilink")（[民法學者](../Page/民法.md "wikilink")、[立命館大學校長](../Page/立命館大學.md "wikilink")）
  - [大河内一男](../Page/大河内一男.md "wikilink")（勞動經濟學家、[東京大學校長](../Page/東京大學.md "wikilink")）
  - [奥田東](../Page/奥田東.md "wikilink")（土壤肥料學家、京都大學校長）
  - [岡本道雄](../Page/岡本道雄.md "wikilink")（醫學家、京都大學校長）
  - [安藤廣太郎](../Page/安藤廣太郎.md "wikilink")（農學者、文化勳章）
  - [古畑種基](../Page/古畑種基.md "wikilink")（法醫學家、文化勳章）
  - [八木秀次](../Page/八木秀次.md "wikilink")（無線通信學家、文化勳章）
  - [緒方知三郎](../Page/緒方知三郎.md "wikilink")（病理學家、文化勳章）
  - [藪田貞治郎](../Page/藪田貞治郎.md "wikilink")（農化學家、文化勳章）
  - [藤田豐八](../Page/藤田豐八.md "wikilink")（東洋史學家）
  - [喜田貞吉](../Page/喜田貞吉.md "wikilink")（日本史學家）
  - [笹川臨風](../Page/笹川臨風.md "wikilink")（日本史學家）
  - [林鶴一](../Page/林鶴一.md "wikilink")（數學家）
  - [姊崎正治](../Page/姊崎正治.md "wikilink")（宗教學家）
  - [伊波普猷](../Page/伊波普猷.md "wikilink")（民俗學家、沖繩學創始者）
  - [厨川白村](../Page/厨川白村.md "wikilink")（英文學者）
  - [橋本進吉](../Page/橋本進吉.md "wikilink")（國學者）
  - [高木市之助](../Page/高木市之助.md "wikilink")（國學者）
  - [山本一清](../Page/山本一清.md "wikilink")（天文學家）
  - [園正造](../Page/園正造.md "wikilink")（數學家）
  - [脇村義太郎](../Page/脇村義太郎.md "wikilink")（經濟學家、[日本學士院院長](../Page/日本學士院.md "wikilink")）
  - [下村寅太郎](../Page/下村寅太郎.md "wikilink")（哲學家）
  - [服部之總](../Page/服部之總.md "wikilink")（歷史學家）
  - [緒方富雄](../Page/緒方富雄.md "wikilink")（血清學家、[緒方洪庵的曾孫](../Page/緒方洪庵.md "wikilink")）
  - [西堀榮三郎](../Page/西堀榮三郎.md "wikilink")（化學家、登山家、第1次南極越冬隊隊長）
  - [中野好夫](../Page/中野好夫.md "wikilink")（英文學者）
  - [神田喜一郎](../Page/神田喜一郎.md "wikilink")（東洋史學家）
  - [貝塚茂樹](../Page/貝塚茂樹.md "wikilink")（東洋史學家）
  - [赤松俊秀](../Page/赤松俊秀.md "wikilink")（日本史學家）
  - [吉川幸次郎](../Page/吉川幸次郎.md "wikilink")（中國文學家）
  - [小川環樹](../Page/小川環樹.md "wikilink")（中國文學家）
  - [新村猛](../Page/新村猛.md "wikilink")（法國文學專家）
  - [櫻田一郎](../Page/櫻田一郎.md "wikilink")（高分子化學者、文化勳章）
  - [桑原武夫](../Page/桑原武夫.md "wikilink")（法國文學專家、文化勳章）
  - [中村吉治](../Page/中村吉治.md "wikilink")（歷史學家、日本農民史專家）
  - [大塚久雄](../Page/大塚久雄.md "wikilink")（[經濟史學家](../Page/經濟史.md "wikilink")、文化勳章）
  - [西山卯三](../Page/西山卯三.md "wikilink")（[建築學家](../Page/建築學.md "wikilink")）
  - [四手井綱英](../Page/四手井綱英.md "wikilink")（森林生態學家）
  - [辻清明](../Page/辻清明_\(政治學家\).md "wikilink")（政治學專家、行政學專家）
  - [豬木正道](../Page/豬木正道.md "wikilink")（政治學專家）
  - [田畑茂二郎](../Page/田畑茂二郎.md "wikilink")（[國際法專家](../Page/國際法.md "wikilink")）
  - [林屋辰三郎](../Page/林屋辰三郎.md "wikilink")（歷史學家、日本中世文化史）
  - [會田雄次](../Page/會田雄次.md "wikilink")（[西洋史學家](../Page/西洋史.md "wikilink")）
  - [今津晃](../Page/今津晃.md "wikilink")（美國史學家）
  - [島田虔次](../Page/島田虔次.md "wikilink")（東洋史學家）
  - [近藤次郎](../Page/近藤次郎.md "wikilink")（應用數學家、文化勳章）
  - [梅棹忠夫](../Page/梅棹忠夫.md "wikilink")（[文化人類學家](../Page/文化人類學家.md "wikilink")、文化勳章）
  - [川喜田二郎](../Page/川喜田二郎.md "wikilink")（文化人類學家）
  - [尾藤正英](../Page/尾藤正英.md "wikilink")（歷史學家、江戸思想史）
  - [多田道太郎](../Page/多田道太郎.md "wikilink")（法文專家）
  - [杉捷夫](../Page/杉捷夫.md "wikilink")（法文專家）
  - [萩原延壽](../Page/萩原延壽.md "wikilink")（歷史學家）
  - [西田直二郎](../Page/西田直二郎.md "wikilink")（歷史學家、日本史）
  - [宮田光雄](../Page/宮田光雄.md "wikilink")（政治思想史學家）
  - [久木幸男](../Page/久木幸男.md "wikilink")（歷史學家、日本教育史）
  - [中山健男](../Page/中山健男.md "wikilink")（憲法專家）
  - [持田豐](../Page/持田豐.md "wikilink")（工程專家、[日本鐵路建設公團](../Page/日本鐵路建設公團.md "wikilink")[青函隧道建設局長](../Page/青函隧道.md "wikilink")）
  - [沼正作](../Page/沼正作.md "wikilink")（[生化學家](../Page/生化學.md "wikilink")）
  - [石井威望](../Page/石井威望.md "wikilink")（系統工學專家）
  - [藤澤令夫](../Page/藤澤令夫.md "wikilink")（哲學家）
  - [栗津則雄](../Page/栗津則雄.md "wikilink")（法文專家）

### 文化

  - [鈴木三重吉](../Page/鈴木三重吉.md "wikilink")（兒童文學家）
  - [梶井基次郎](../Page/梶井基次郎.md "wikilink")（[小說家](../Page/小說家.md "wikilink")）
  - [織田作之助](../Page/織田作之助.md "wikilink")（小說家）
  - [武田麟太郎](../Page/武田麟太郎.md "wikilink")（小說家）
  - [三好達治](../Page/三好達治.md "wikilink")（[詩人](../Page/詩人.md "wikilink")）
  - [丸山薫](../Page/丸山薫.md "wikilink")（詩人）
  - [北川冬彥](../Page/北川冬彥.md "wikilink")（詩人）
  - [富士正晴](../Page/富士正晴.md "wikilink")（詩人、小說家）
  - [安東次男](../Page/安東次男.md "wikilink")（詩人）
  - [淺野晃](../Page/淺野晃.md "wikilink")（詩人）
  - [野間宏](../Page/野間宏.md "wikilink")（小說家）
  - [田宮虎彥](../Page/田宮虎彥.md "wikilink")（小說家）
  - [外村繁](../Page/外村繁.md "wikilink")（小說家）
  - [隆慶一郎](../Page/隆慶一郎.md "wikilink")（劇本家、小說家）
  - [古山高麗雄](../Page/古山高麗雄.md "wikilink")（小說家）
  - [大宅壯一](../Page/大宅壯一.md "wikilink")（[評論家](../Page/評論家.md "wikilink")）
  - [森毅](../Page/森毅.md "wikilink")（數學家、評論家）
  - [中井正一](../Page/中井正一.md "wikilink")（美學家）
  - [松田道雄](../Page/松田道雄.md "wikilink")（醫師、育兒評論家）
  - [小口太郎](../Page/小口太郎.md "wikilink")（歌人\]）
  - [上田三四二](../Page/上田三四二.md "wikilink")（歌人）
  - [物集高量](../Page/物集高量.md "wikilink")（小說家）
  - [高濱虛子](../Page/高濱虛子.md "wikilink")（俳人）
  - [河東碧梧桐](../Page/河東碧梧桐.md "wikilink")（俳人）
  - [山口誓子](../Page/山口誓子.md "wikilink")（俳人）
  - [日野原重明](../Page/日野原重明.md "wikilink")（[聖路加國際醫院名譽院長](../Page/聖路加國際醫院.md "wikilink")）
  - [小松左京](../Page/小松左京.md "wikilink")（科幻小說作家）
  - [小西得郎](../Page/小西得郎.md "wikilink")（職業棒球教練、棒球球評）
  - [田坂具隆](../Page/田坂具隆.md "wikilink")（電影導演）
  - [松村禎三](../Page/松村禎三.md "wikilink")（作曲家）
  - [須田國太郎](../Page/須田國太郎.md "wikilink")（西洋畫家）

### 政治

  - [濱口雄幸](../Page/濱口雄幸.md "wikilink")（[内閣總理大臣](../Page/内閣總理大臣.md "wikilink")）
  - [幣原喜重郎](../Page/幣原喜重郎.md "wikilink")（[内閣總理大臣](../Page/内閣總理大臣.md "wikilink")）
  - [片山哲](../Page/片山哲.md "wikilink")（[内閣總理大臣](../Page/内閣總理大臣.md "wikilink")）
  - [永田秀次郎](../Page/永田秀次郎.md "wikilink")（拓務大臣、鐵道大臣）
  - [山本宣治](../Page/山本宣治.md "wikilink")（[眾議院議員](../Page/眾議院議員.md "wikilink")）
  - [赤松克麿](../Page/赤松克麿.md "wikilink")（眾議院議員）
  - [麻生久](../Page/麻生久.md "wikilink")（眾議院議員、[社會大眾黨黨魁](../Page/社會大眾黨.md "wikilink")）
  - [藤井真信](../Page/藤井真信.md "wikilink")（[大藏大臣](../Page/大藏大臣.md "wikilink")、大藏次官）
  - [小笠原三九郎](../Page/小笠原三九郎.md "wikilink")（[商工大臣](../Page/商工大臣.md "wikilink")、[農林大臣](../Page/農林大臣.md "wikilink")、[通商產業大臣](../Page/通商產業大臣.md "wikilink")、大藏大臣）
  - [村上義一](../Page/村上義一.md "wikilink")（[運輸大臣](../Page/運輸大臣.md "wikilink")）
  - [水谷長三郎](../Page/水谷長三郎.md "wikilink")（商工大臣）
  - [周東英雄](../Page/周東英雄.md "wikilink")（農林大臣、[經濟安定本部總務長官](../Page/經濟安定本部.md "wikilink")、[自治大臣](../Page/自治大臣.md "wikilink")）
  - [高倉輝](../Page/高倉輝.md "wikilink")（眾議院議員、[參議院議員](../Page/參議院議員.md "wikilink")、小說家）
  - [岸田幸雄](../Page/岸田幸雄.md "wikilink")（參議院議員、首任普選[兵庫縣](../Page/兵庫縣.md "wikilink")[知事](../Page/知事.md "wikilink")）
  - [村山道雄](../Page/村山道雄.md "wikilink")（參議院議員、首任普選[山形縣](../Page/山形縣.md "wikilink")[知事](../Page/知事.md "wikilink")）
  - [吉江勝保](../Page/吉江勝保.md "wikilink")（參議院議員、首任普選[山梨縣](../Page/山梨縣.md "wikilink")[知事](../Page/知事.md "wikilink")）
  - [太田典禮](../Page/太田典禮.md "wikilink")（眾議院議員、晩年推廣[安樂死](../Page/安樂死.md "wikilink")）
  - [山縣勝見](../Page/山縣勝見.md "wikilink")（[國務大臣](../Page/國務大臣.md "wikilink")、[厚生大臣](../Page/厚生大臣.md "wikilink")）
  - [古井喜實](../Page/古井喜實.md "wikilink")（厚生大臣、[法務大臣](../Page/法務大臣.md "wikilink")、内務次官）
  - [早川崇](../Page/早川崇.md "wikilink")（自治大臣、[國家公安委員長](../Page/國家公安委員長.md "wikilink")、[勞動大臣](../Page/勞動大臣.md "wikilink")、厚生大臣）
  - [木村俊夫](../Page/木村俊夫.md "wikilink")（[内閣官房長官](../Page/内閣官房長官.md "wikilink")、外務大臣）
  - [小山長規](../Page/小山長規.md "wikilink")（[環境廳長官](../Page/環境廳長官.md "wikilink")、運輸大臣）
  - [江藤智](../Page/江藤智_\(政治家\).md "wikilink")（運輸大臣、[日本國有鐵道](../Page/日本國有鐵道.md "wikilink")[札幌鐵路管理局長](../Page/札幌.md "wikilink")）
  - [谷垣専一](../Page/谷垣専一.md "wikilink")（[文部大臣](../Page/文部大臣.md "wikilink")、農林官僚）
  - [村田敬次郎](../Page/村田敬次郎.md "wikilink")（通商產業大臣、自治大臣）
  - [上田稔](../Page/上田稔.md "wikilink")（環境廳長官、[建設省河川局長](../Page/建設省.md "wikilink")）
  - [松本十郎](../Page/松本十郎.md "wikilink")（眾議院議員、[大藏省印刷局長](../Page/大藏省.md "wikilink")）
  - [永末英一](../Page/永末英一.md "wikilink")（眾議院議員、[民社黨委員長](../Page/民社黨.md "wikilink")）
  - [中西一郎](../Page/中西一郎.md "wikilink")（[國務大臣](../Page/國務大臣.md "wikilink")、經濟企劃廳國民生活局長）
  - [植木光教](../Page/植木光教.md "wikilink")（[總理府總務長官](../Page/總理府總務長官.md "wikilink")、沖繩開發廳長官）
  - [渡邊朗](../Page/渡邊朗.md "wikilink")（眾議院議員、民社黨國際局長）
  - [八木大介](../Page/八木大介.md "wikilink")（參議院議員、サラリーマン新黨副代表）

### 法律

  - [草野豹一郎](../Page/草野豹一郎.md "wikilink")（[法官](../Page/法官.md "wikilink")、[律師](../Page/律師.md "wikilink")）
  - [村上朝一](../Page/村上朝一.md "wikilink")（最高法院院長）
  - [藤林益三](../Page/藤林益三.md "wikilink")（最高法院院長）
  - [矢口洪一](../Page/矢口洪一.md "wikilink")（最高法院院長）
  - [垂水克己](../Page/垂水克己.md "wikilink")（最高法院法官、東京高等法院院長）
  - [山田作之助](../Page/山田作之助.md "wikilink")（最高法院法官、神戸律師協會會長）
  - [高辻正己](../Page/高辻正己.md "wikilink")（最高法院法官、内閣法制局局長）
  - [大塚喜一郎](../Page/大塚喜一郎.md "wikilink")（最高法院法官、日本律師聯合會事務校長、第一東京律師會會長）
  - [藤井五一郎](../Page/藤井五一郎.md "wikilink")（初代[公安調査廳長官](../Page/公安調査廳.md "wikilink")）
  - [岸本義廣](../Page/岸本義廣.md "wikilink")（[東京高等檢察廳檢察長](../Page/東京高等檢察廳.md "wikilink")）
  - [辻辰三郎](../Page/辻辰三郎.md "wikilink")（[最高法院檢察署檢察總長](../Page/最高法院檢察署檢察總長.md "wikilink")）
  - [安原美穂](../Page/安原美穂.md "wikilink")（[最高法院檢察署檢察總長](../Page/最高法院檢察署檢察總長.md "wikilink")）
  - [大江兵馬](../Page/大江兵馬.md "wikilink")（東京地檢署特搜部部長）

### 行政

  - [岡本英太郎](../Page/岡本英太郎.md "wikilink")（農商務省次官、商工組合中央金庫董事長）
  - [奥田良三](../Page/奥田良三_\(政治家\).md "wikilink")（[奈良縣知事](../Page/奈良縣.md "wikilink")）
  - [栗屋謙](../Page/栗屋謙.md "wikilink")（文部省次官）
  - [河原春作](../Page/河原春作.md "wikilink")（文部省次官）
  - [井出成三](../Page/井出成三.md "wikilink")（文部省次官）
  - [石黑英彥](../Page/石黑英彥.md "wikilink")（文部省次官）
  - [宮田光雄](../Page/宮田光雄_\(内務官僚\).md "wikilink")（警視總監、内閣書記官長）
  - [塚本清治](../Page/塚本清治.md "wikilink")（[内務省次官](../Page/内務省_\(日本\).md "wikilink")、内閣書記官長）
  - [下岡忠治](../Page/下岡忠治.md "wikilink")（[内務省次官](../Page/内務省_\(日本\).md "wikilink")）
  - [篠原英太郎](../Page/篠原英太郎.md "wikilink")（[内務省次官](../Page/内務省_\(日本\).md "wikilink")）
  - [竹内可吉](../Page/竹内可吉.md "wikilink")（商工省次官、企劃院總裁、軍需省次官）
  - [松田太郎](../Page/松田太郎.md "wikilink")（商工省次官）
  - [豐田雅孝](../Page/豐田雅孝.md "wikilink")（商工省次官、商工組合中央金庫董事長）
  - [德永久次](../Page/德永久次.md "wikilink")（[通產省事務次官](../Page/經濟產業省.md "wikilink")）
  - [中野哲夫](../Page/中野哲夫.md "wikilink")（[通產省](../Page/經濟產業省.md "wikilink")2代目企業局長（のちの產政局長））
  - [本田早苗](../Page/本田早苗.md "wikilink")（[通產省企業局長](../Page/經濟產業省.md "wikilink")、丸善石油社長）
  - [岩武輝彥](../Page/岩武輝彥.md "wikilink")（[中小企業廳長官](../Page/中小企業廳.md "wikilink")）
  - [伊原義德](../Page/伊原義德.md "wikilink")（科學技術廳事務次官）
  - [門脇季光](../Page/門脇季光.md "wikilink")（外務省事務次官）
  - [奥村勝藏](../Page/奥村勝藏.md "wikilink")（外務省事務次官）
  - [村田良平](../Page/村田良平.md "wikilink")（外務省事務次官）
  - [原田健](../Page/原田健.md "wikilink")（駐教廷大使(1942-46)、駐義大利大使、宮内廳式部官長）
  - [中江要介](../Page/中江要介.md "wikilink")（駐中國大使(1984-87)）
  - [有光次郎](../Page/有光次郎.md "wikilink")（文部省事務次官、日本藝術院院長）
  - [北川力夫](../Page/北川力夫.md "wikilink")（厚生勞動省事務次官）
  - [渡邊喜久造](../Page/渡邊喜久造.md "wikilink")（國稅廳長官）
  - [吉田太郎一](../Page/吉田太郎一.md "wikilink")（[財務官](../Page/財務官_\(日本\).md "wikilink")）
  - [尾崎英二](../Page/尾崎英二.md "wikilink")（國際通貨基金理事）
  - [海堀洋平](../Page/海堀洋平.md "wikilink")（日銀政策委員、[大藏省主計局次長](../Page/大藏省.md "wikilink")）
  - [江口見登留](../Page/江口見登留.md "wikilink")（初代厚生勞動省事務次官、[警視總監](../Page/警視總監.md "wikilink")、[警察預備隊本部次長](../Page/警察預備隊.md "wikilink")）
  - [鈴木俊一](../Page/鈴木俊一.md "wikilink")（東京都知事）
  - [柴田護](../Page/柴田護.md "wikilink")（總務省事務次官）
  - [大國彰](../Page/大國彰.md "wikilink")（總務省事務次官）
  - [加藤陽三](../Page/加藤陽三.md "wikilink")（防衛省事務次官、眾議院議員）
  - [久保卓也](../Page/久保卓也.md "wikilink")（防衛省事務次官\]\]）
  - [竹岡勝美](../Page/竹岡勝美.md "wikilink")（防衛廳官房長）
  - [小幡久男](../Page/小幡久男.md "wikilink")（防衛施設廳長官）
  - [佐柳藤太](../Page/佐柳藤太.md "wikilink")（[千葉縣知事](../Page/千葉縣.md "wikilink")、内務官僚）
  - [有吉忠一](../Page/有吉忠一.md "wikilink")（朝鮮總督府政務總監、内務官僚）
  - [伊澤多喜男](../Page/伊澤多喜男.md "wikilink")（[警視總監](../Page/警視總監.md "wikilink")、台灣總督、内務官僚）
  - [古海忠之](../Page/古海忠之.md "wikilink")（[滿州國總務廳次長](../Page/滿州國.md "wikilink")、大藏官僚）
  - [島田叡](../Page/島田叡.md "wikilink")（[沖縄縣知事](../Page/沖縄縣.md "wikilink")、内務出身）
  - [岡田宇之助](../Page/岡田宇之助.md "wikilink")（[佐賀縣知事](../Page/佐賀縣.md "wikilink")、[住友財閥理事](../Page/住友財閥.md "wikilink")）
  - [池原鹿之助](../Page/池原鹿之助.md "wikilink")（[大阪市助役](../Page/大阪市.md "wikilink")、内務官僚）
  - [河田貫三](../Page/河田貫三.md "wikilink")（[大藏省丸龜稅務監督局長](../Page/大藏省.md "wikilink")）
  - [黑崎定三](../Page/黑崎定三.md "wikilink")（[法制局長官](../Page/法制局長官.md "wikilink")）
  - [高岡健治](../Page/高岡健治.md "wikilink")（[内閣書記官長](../Page/内閣書記官長.md "wikilink")）
  - [富田健治](../Page/富田健治.md "wikilink")（[内閣書記官長](../Page/内閣書記官長.md "wikilink")）
  - [南波杢三郎](../Page/南波杢三郎.md "wikilink")（[内務省](../Page/内務省_\(日本\).md "wikilink")[警保局保安課長](../Page/警保局.md "wikilink")、檢察官出身）
  - [下山定則](../Page/下山定則.md "wikilink")（[日本國有鐵道首任總裁](../Page/日本國有鐵道.md "wikilink")）
  - [天坊裕彥](../Page/天坊裕彥.md "wikilink")（[國鉄副總裁](../Page/日本國有鉄道.md "wikilink")、參議院議員）
  - [小倉武一](../Page/小倉武一.md "wikilink")（[農林](../Page/農林水產省.md "wikilink")[事務次官](../Page/事務次官.md "wikilink")、政府稅制調査會會長）
  - [中村大造](../Page/中村大造.md "wikilink")（運輸省事務次官、新東京國際空港公團總裁、[全日本空輸送社長](../Page/全日本空輸送.md "wikilink")）
  - [藤井崇治](../Page/藤井崇治.md "wikilink")（電氣廳長官）
  - [岩澤忠恭](../Page/岩澤忠恭.md "wikilink")（建設省事務次官、參議院議員）
  - [稲浦鹿藏](../Page/稲浦鹿藏.md "wikilink")（建設省事務次官、參議院議員）
  - [湯川宏](../Page/湯川宏.md "wikilink")（[大阪府副知事](../Page/大阪府.md "wikilink")、厚生勞動官僚出身。眾議院議員）
  - [竹内直一](../Page/竹内直一.md "wikilink")（日本消費者聯盟代表、農林官僚出身）
  - [當間重剛](../Page/當間重剛.md "wikilink")（琉球政府行政主席）
  - [中西陽一](../Page/中西陽一.md "wikilink")（[石川縣知事](../Page/石川縣.md "wikilink")）
  - [黑田了一](../Page/黑田了一.md "wikilink")（[大阪府知事](../Page/大阪府.md "wikilink")）
  - [林田悠紀夫](../Page/林田悠紀夫.md "wikilink")（[京都府知事](../Page/京都府.md "wikilink")）
  - [立木勝](../Page/立木勝.md "wikilink")（[大分縣知事](../Page/大分縣.md "wikilink")）
  - [大島靖](../Page/大島靖.md "wikilink")（[大阪市市長](../Page/大阪市.md "wikilink")）

### 經濟

  - [矢野恒太](../Page/矢野恒太.md "wikilink")（[第一生命創業者](../Page/第一生命.md "wikilink")
    / 第三高等中學校畢業）
  - [馬場恒吾](../Page/馬場恒吾.md "wikilink")（[讀賣新聞社社長](../Page/讀賣新聞社.md "wikilink")
    / 第三高等中學校畢業）
  - [高橋龍太郎](../Page/高橋龍太郎.md "wikilink")（日本大樓大王）
  - [石川芳次郎](../Page/石川芳次郎.md "wikilink")（京福電氣鉄道首任社長）
  - [原邦造](../Page/原邦造.md "wikilink")（帝都高速度交通營團初代總裁、[日本航空董事長](../Page/日本航空.md "wikilink")）
  - [池田龜三郎](../Page/池田龜三郎.md "wikilink")（[三菱油化董事長](../Page/三菱化學.md "wikilink")）
  - [淺田長平](../Page/淺田長平.md "wikilink")（[神戸製鋼所董事長](../Page/神戸製鋼所.md "wikilink")）
  - [久留島秀三郎](../Page/久留島秀三郎.md "wikilink")（[同和礦業社長](../Page/同和礦業.md "wikilink")、日本[男童軍聯盟理事長](../Page/男童軍.md "wikilink")）
  - [大和田悌二](../Page/大和田悌二.md "wikilink")（[日本曹達社長](../Page/日本曹達.md "wikilink")、[日本電信電話公社經營委員長](../Page/日本電信電話公社.md "wikilink")）
  - [木下又三郎](../Page/木下又三郎.md "wikilink")（[王子製紙董事長](../Page/王子製紙.md "wikilink")）
  - [廣瀬經一](../Page/廣瀬經一.md "wikilink")（[北海道拓殖銀行董事長](../Page/北海道拓殖銀行.md "wikilink")、原大藏官僚）
  - [田路舜哉](../Page/田路舜哉.md "wikilink")（[住友商事初代社長](../Page/住友商事.md "wikilink")）
  - [土井正治](../Page/土井正治.md "wikilink")（[住友化學工業會長](../Page/住友化學工業.md "wikilink")）
  - [鈴木剛](../Page/鈴木剛.md "wikilink")（
    [朝日放送社長](../Page/朝日放送.md "wikilink")）
  - [阿部孝次郎](../Page/阿部孝次郎.md "wikilink")（[東洋紡織董事長](../Page/東洋紡織.md "wikilink")、[關西經濟連合會會長](../Page/關西經濟連合會.md "wikilink")）
  - [鹿島守之助](../Page/鹿島守之助.md "wikilink")（[鹿島建設董事長](../Page/鹿島建設.md "wikilink")、前外交官）
  - [北川一榮](../Page/北川一榮.md "wikilink")（[住友電氣工業董事長](../Page/住友電氣工業.md "wikilink")）
  - [一本松珠璣](../Page/一本松珠璣.md "wikilink")（[日本核能發電社長](../Page/日本核能發電.md "wikilink")）
  - [福田千里](../Page/福田千里.md "wikilink")（[大和證券董事長](../Page/大和證券.md "wikilink")）
  - [廣田壽一](../Page/廣田壽一.md "wikilink")（[住友金屬工業董事長](../Page/住友金屬工業.md "wikilink")）
  - [加藤辨三郎](../Page/加藤辨三郎.md "wikilink")（[協和發酵工業創業者](../Page/協和發酵工業.md "wikilink")）
  - [栗本順三](../Page/栗本順三.md "wikilink")（[栗本鐵工所董事長](../Page/栗本鐵工所.md "wikilink")、[阪神高速道路公團初代理事長](../Page/阪神高速道路公團.md "wikilink")）
  - [井口竹次郎](../Page/井口竹次郎.md "wikilink")（[大阪瓦斯董事長](../Page/大阪瓦斯.md "wikilink")）
  - [谷口豐三郎](../Page/谷口豐三郎.md "wikilink")（東洋紡董事長）
  - [佐伯勇](../Page/佐伯勇.md "wikilink")（[近畿日本鐵道董事長](../Page/近畿日本鐵道.md "wikilink")）
  - [外島健吉](../Page/外島健吉.md "wikilink")（[神戸製鋼所社長](../Page/神戸製鋼所.md "wikilink")）
  - [島本融](../Page/島本融.md "wikilink")（日銀政策委員、[海外派駐財務官](../Page/財務官_\(日本\).md "wikilink")）
  - [寺尾威夫](../Page/寺尾威夫.md "wikilink")（[大和銀行董事長](../Page/大和銀行.md "wikilink")）
  - [工藤信一良](../Page/工藤信一良.md "wikilink")（[太平洋聯盟會長](../Page/太平洋聯盟.md "wikilink")、[毎日新聞社副社長](../Page/毎日新聞社.md "wikilink")）
  - [湯淺佑一](../Page/湯淺佑一.md "wikilink")（[臺灣湯淺電池董事長](../Page/臺灣湯淺電池.md "wikilink")、[關西經濟同友會代表幹事](../Page/關西經濟同友會.md "wikilink")）
  - [森壽五郎](../Page/森壽五郎.md "wikilink")（[丸善石油董事長](../Page/丸善石油.md "wikilink")、[關西電力副社長](../Page/關西電力.md "wikilink")）
  - [田中敦](../Page/田中敦.md "wikilink")（[倉敷紡績社長](../Page/倉敷紡績.md "wikilink")）
  - [四本潔](../Page/四本潔.md "wikilink")（[川崎重工業董事長](../Page/川崎重工業.md "wikilink")）
  - [柴山幸雄](../Page/柴山幸雄.md "wikilink")（住友商事社長）
  - [池田芳藏](../Page/池田芳藏.md "wikilink")（[三井物產董事長](../Page/三井物產.md "wikilink")、[日本放送協會會長](../Page/日本放送協會.md "wikilink")）
  - [乾昇](../Page/乾昇.md "wikilink")（住友金屬工業社長）
  - [菊池稔](../Page/菊池稔.md "wikilink")（[東京海上火災保險社長](../Page/東京海上日動火災保險.md "wikilink")）
  - [岡田貢助](../Page/岡田貢助.md "wikilink")（[川崎汽船社長](../Page/川崎汽船.md "wikilink")）
  - [吉村勘兵衛](../Page/吉村勘兵衛.md "wikilink")（[日本長期信用銀行頭取](../Page/日本長期信用銀行.md "wikilink")）
  - [増田元一](../Page/増田元一.md "wikilink")（[國際電信電話社長](../Page/國際電信電話.md "wikilink")）
  - [田鍋健](../Page/田鍋健.md "wikilink")（[積水房屋社長](../Page/積水房屋.md "wikilink")）
  - [磯田一郎](../Page/磯田一郎.md "wikilink")（[住友銀行董事長](../Page/住友銀行.md "wikilink")）
  - [宇野収](../Page/宇野収.md "wikilink")（東洋紡董事長、關西經濟連合會會長）
  - [高木盛久](../Page/高木盛久.md "wikilink")（[日本電視台社長](../Page/日本電視台.md "wikilink")）
  - [中村金夫](../Page/中村金夫.md "wikilink")（[日本興業銀行董事長](../Page/日本興業銀行.md "wikilink")）
  - [品川正治](../Page/品川正治.md "wikilink")（[日本火災海上保險會長](../Page/日本火災海上保險.md "wikilink")）
  - [轉法輪奏](../Page/轉法輪奏.md "wikilink")（[三井商船董事長](../Page/三井商船.md "wikilink")）
  - [植田新也](../Page/植田新也.md "wikilink")（[產業經濟新聞社社長](../Page/產業經濟新聞社.md "wikilink")）
  - [八城政基](../Page/八城政基.md "wikilink")（[新生銀行會長](../Page/新生銀行.md "wikilink")）

### 台籍人士

  - [彭明敏](../Page/彭明敏.md "wikilink")
  - [林茂生](../Page/林茂生.md "wikilink")

## 相關條目

  - [京都大學](../Page/京都大學.md "wikilink")

## 外部連結

  - [華麗なる旧制高校巡禮 –
    舊制第三高等學校](https://web.archive.org/web/20090319001135/http://www.geocities.jp/qsay55/3rd.html/)

[第三](../Category/日本的舊制高等學校.md "wikilink")
[Category:京都大學](../Category/京都大學.md "wikilink")
[Category:左京區](../Category/左京區.md "wikilink")