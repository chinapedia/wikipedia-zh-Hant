[Peaksummerresidence.jpg](https://zh.wikipedia.org/wiki/File:Peaksummerresidence.jpg "fig:Peaksummerresidence.jpg")
[The_Mountain_Lodge_Guard_House_2016.jpg](https://zh.wikipedia.org/wiki/File:The_Mountain_Lodge_Guard_House_2016.jpg "fig:The_Mountain_Lodge_Guard_House_2016.jpg")
[HK_Peak_Mt_Austin_Road_Governor's_Walk.jpg](https://zh.wikipedia.org/wiki/File:HK_Peak_Mt_Austin_Road_Governor's_Walk.jpg "fig:HK_Peak_Mt_Austin_Road_Governor's_Walk.jpg")
[Gate_Lodge_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Gate_Lodge_\(Hong_Kong\).jpg "fig:Gate_Lodge_(Hong_Kong).jpg")\]\]

**舊總督山頂別墅守衛室** （[英文](../Page/英文.md "wikilink")：**The Mountain Lodge
Guard
House**），是[香港法定古跡之一](../Page/香港法定古蹟列表.md "wikilink")，是**舊總督山頂別墅**（[英文](../Page/英文.md "wikilink")：**The
Mountain
Lodge**）的遺址，與旁邊的[山頂公園園內的石牆地基及](../Page/山頂公園_\(香港\).md "wikilink")[同樂徑旁仍然有的小石碑均刻有](../Page/同樂徑.md "wikilink")[英文港督官邸](../Page/英文.md "wikilink")（Governor's
Residence），見證著[香港20世紀初的歷史變遷](../Page/香港20世紀初歷史.md "wikilink")。

## 位置

舊總督山頂別墅守衛室是在[香港島](../Page/香港島.md "wikilink")[太平山](../Page/太平山_\(香港\).md "wikilink")[扯旗山](../Page/扯旗山.md "wikilink")，[柯士甸山道海拔](../Page/柯士甸山道.md "wikilink")500米之分界路段。

## 建築

於1902年落成的別墅仿照[蘇格蘭式大宅的](../Page/蘇格蘭.md "wikilink")[維多利亞式建築風格興建](../Page/維多利亞式.md "wikilink")，以[花崗石及](../Page/花崗石.md "wikilink")[麻石作為建築材料](../Page/麻石.md "wikilink")。別墅佔地約1,000[平方米](../Page/平方米.md "wikilink")，樓高兩層。而守衛室則屬於[文藝復興式建築設計](../Page/文藝復興.md "wikilink")，佔地約100[平方呎](../Page/平方呎.md "wikilink")，為一座單層建築。

## 歷史

舊總督山頂別墅前身是1862年設立的英軍療養院，於1867年由[香港總督](../Page/香港總督.md "wikilink")[麥當奴向](../Page/麥當奴.md "wikilink")[英國軍隊購買](../Page/英國軍隊.md "wikilink")，興建成為木造的避暑小屋。於1874年，被[颱風吹塌](../Page/甲戌風災.md "wikilink")。雖然當時再度重建，然而於次年被[白蟻侵蝕](../Page/白蟻.md "wikilink")，避暑小屋遂荒廢。

至1892年，香港總督[羅便臣委任](../Page/威廉·羅便臣.md "wikilink")[工務局主管](../Page/工務局.md "wikilink")[谷柏](../Page/谷柏.md "wikilink")（Francis
Cooper）在該址設計新別墅。然而於1894年，[鼠疫爆發](../Page/1894年香港鼠疫爆發.md "wikilink")，使到計劃一度暫停。1900年，香港總督[卜力重新提出計劃](../Page/卜力.md "wikilink")，由於他不喜歡工務局的設計方案，遂委托建築商[公和洋行重新設計新別墅](../Page/公和洋行.md "wikilink")，由[生利建築](../Page/生利建築.md "wikilink")
(Sang Lee Construction)
建造，費用高達97,000港元，於1902年建成。此棟別墅當時被形容為「山頂最宏偉最美觀的建築物」。

雖然其時[舊山頂道及](../Page/舊山頂道.md "wikilink")[山頂纜車已經開通](../Page/山頂纜車.md "wikilink")，但是交通仍然不方便，因此歷任香港總督入住率不高。自1930年[貝璐上任開始](../Page/貝璐.md "wikilink")，再未有香港總督入住。於[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，舊總督山頂別墅飽受破壞。由於別墅修葺費高昂，加上貝璐於[粉嶺落實興建有另外一座港督別墅](../Page/粉嶺.md "wikilink")，山頂別墅於1946年被拆卸，餘下守衛室。別墅原址於1950年代規劃成為[山頂公園](../Page/山頂公園.md "wikilink")，於1970年代加設[涼亭](../Page/涼亭.md "wikilink")。至今，守衛室未有開放；此前，守衛室為山頂公園職員貯物室，鄰近放有農藝小樹苗。

[1977年10月](../Page/1977年10月.md "wikilink")，[古物古蹟辦事處先後在](../Page/古物古蹟辦事處.md "wikilink")[柯士甸山道及](../Page/柯士甸山道.md "wikilink")[夏力道發現刻有Governor](../Page/夏力道.md "wikilink")'s
Residence的方形石頭，為**前港督別墅界石**。1978年6月23日，兩塊界石分別被遷移到山頂公園近[同樂徑的草坪](../Page/同樂徑.md "wikilink")，以及[禮賓府門外的路邊花槽展示](../Page/禮賓府.md "wikilink")。

2007年1月10日，[建築署公佈](../Page/建築署.md "wikilink")[旅遊事務署於](../Page/旅遊事務署.md "wikilink")[2006年12月在山頂公園進行改善工程時](../Page/2006年12月.md "wikilink")，發掘出前總督山頂別墅的部分遺跡，並且交由古物古蹟辦事處[考古發掘](../Page/考古.md "wikilink")。結果顯示，山頂別墅部分地基於1968至1970年興建涼亭時遭受嚴重破壞，但是保存情況基本良好。最重要的發現是門廊上一幅約1米乘3.5米的圖案地磚和大量由minton
Hollins公司生產的地磚。地面勘查時亦發現8塊於1910年豎立的[界石](../Page/界石.md "wikilink")、兩塊軍部的界石、一處曾經用作水務工程平房的頹垣及4個建築構件的棄置點。

經過多年研究，有關部門於2013年落實以[玻璃展覽箱形式展示地基遺迹](../Page/玻璃.md "wikilink")，以木板地台和鋪磚物料重現別墅當年的布局，包括房間和間隔牆，復修工程預計於2014年展開\[1\]。不過到2016年7月，仍未有工程進行。

## 參見

  - [行政長官粉嶺別墅](../Page/行政長官粉嶺別墅.md "wikilink")

## 參考資料

## 外部連結

  - [舊總督山頂別墅守衛室資料](http://www.amo.gov.hk/b5/monuments_52.php)

[Category:太平山 (香港)](../Category/太平山_\(香港\).md "wikilink")
[Category:香港法定古蹟](../Category/香港法定古蹟.md "wikilink")
[Category:香港官邸](../Category/香港官邸.md "wikilink")
[Category:香港西式建築](../Category/香港西式建築.md "wikilink")

1.  [港督山頂別墅「重現」](http://orientaldaily.on.cc/cnt/news/20130220/00176_058.html)
    《東方日報》 2013年2月20日