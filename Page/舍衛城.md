[Mulagandhakuti.jpg](https://zh.wikipedia.org/wiki/File:Mulagandhakuti.jpg "fig:Mulagandhakuti.jpg")
[Preaching_Buddha,_perhaps_the_Great_Miracle_of_Sravasti,_Gandhara,_c._3rd_century_AD,_gray_schist_-_Matsuoka_Museum_of_Art_-_Tokyo,_Japan_-_DSC07120.JPG](https://zh.wikipedia.org/wiki/File:Preaching_Buddha,_perhaps_the_Great_Miracle_of_Sravasti,_Gandhara,_c._3rd_century_AD,_gray_schist_-_Matsuoka_Museum_of_Art_-_Tokyo,_Japan_-_DSC07120.JPG "fig:Preaching_Buddha,_perhaps_the_Great_Miracle_of_Sravasti,_Gandhara,_c._3rd_century_AD,_gray_schist_-_Matsuoka_Museum_of_Art_-_Tokyo,_Japan_-_DSC07120.JPG")

**舍卫城**（[梵語](../Page/梵語.md "wikilink")：श्रावस्ती，Śrāvastī，[巴利語](../Page/巴利語.md "wikilink")：Sāvatthī），古[印度](../Page/印度.md "wikilink")[恆河中游北岸](../Page/恆河.md "wikilink")[拘萨罗国](../Page/拘萨罗.md "wikilink")（Kosala）都城。在今印度[北方邦北部](../Page/北方邦.md "wikilink")，[拉普底河南岸的](../Page/拉普底河.md "wikilink")[謝拉瓦斯蒂縣](../Page/謝拉瓦斯蒂縣.md "wikilink")。相距恆河南岸[摩揭陀國](../Page/摩揭陀.md "wikilink")（Magadha）的[王舍城不太远](../Page/王舍城.md "wikilink")。舍卫城是古印度重要的思想交汇发源地。

## 簡介

在[佛教史上](../Page/佛教.md "wikilink")，因为是著名的[祗园精舍所在地](../Page/祗园精舍.md "wikilink")，[释迦牟尼长年在此居留说法长达二十多年](../Page/释迦牟尼.md "wikilink")\[1\]，因此名闻遐迩，成为佛教[八大聖地之一](../Page/八大聖地.md "wikilink")。7世纪[玄奘法师来此时](../Page/玄奘.md "wikilink")，舍卫城已经荒废。

## 記載

[玄奘](../Page/玄奘.md "wikilink")《[大唐西域記](../Page/大唐西域記.md "wikilink")》：

## 引用

## 外部链接

  - [Sravasti Tourist
    information](https://web.archive.org/web/20060721221948/http://www.tourismofindia.com/hibuddh/buddh_sravasti.htm)
  - [舍卫城遗址照片](http://www.tangmi.com/indiaphoto/shengweiguo.htm)

[Category:佛教史地](../Category/佛教史地.md "wikilink")

1.  《[僧伽羅剎所集經](../Page/僧伽羅剎所集經.md "wikilink")》：「如是世尊於波羅奈國。而轉法輪。初轉此法時。多饒益眾生。即於此夏坐有益於摩竭國王。第二三於靈鷲頂山。第五脾舒離。第六摩拘羅山(白善)為母故。第七於三十三天。第八鬼神界。第九拘苫毘國。第十枝提山中。第十一復鬼神界。第十二摩伽陀閑居處。第十三復還鬼神界。**第十四本佛所遊處。於舍衛祗樹給孤獨園。**第十五迦維羅衛國釋種村中。第十六還迦維羅衛國。第十七羅閱城。第十八復羅閱城。第十九柘梨山中。第二十夏坐在羅閱城。第二十一還柘梨山中。於鬼神界不經歷餘處連四夏坐。**十九年不經歷餘處。於舍衛國夏坐。**如來如是最後夏坐時。於[跋祗境界毘將村中夏坐](../Page/跋祗.md "wikilink")。」