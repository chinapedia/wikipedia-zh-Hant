**舞火龍**，又稱**燒龍**、**燒火龍**、**遊火龍**、**遊草龍**，是[客家及一些](../Page/客家.md "wikilink")[中國少數民族如](../Page/中國少數民族.md "wikilink")[土家族](../Page/土家族.md "wikilink")、[瑤族的傳統民俗之一](../Page/瑤族.md "wikilink")，是[舞龍的一種](../Page/舞龍.md "wikilink")，多在[傳統節日或喜慶場合舉行](../Page/中華傳統節日.md "wikilink")。由於最基本的**火龍**是以[稻草紮成龍的形狀再插上](../Page/稻草.md "wikilink")[香](../Page/香.md "wikilink")，因此又稱**草龍**、**香火龍**，各地的演變出各具特色的舞火龍。湖南汝城香火龙最独具特色。

客家族群的舞火龍見於多個客家人聚居地，在[香港](../Page/香港.md "wikilink")，[中秋節有](../Page/中秋節_\(華人\).md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[大坑的](../Page/大坑_\(香港\).md "wikilink")[大坑舞火龍](../Page/大坑舞火龍.md "wikilink")、[薄扶林村的](../Page/薄扶林村.md "wikilink")[薄扶林村舞火龍](../Page/薄扶林村舞火龍.md "wikilink")。在[中國大陸](../Page/中國大陸.md "wikilink")[廣東](../Page/廣東.md "wikilink")[梅州](../Page/梅州.md "wikilink")[豐順縣](../Page/豐順縣.md "wikilink")[埔寨有](../Page/埔寨.md "wikilink")[元宵節舉行的](../Page/元宵節_\(華人\).md "wikilink")[埔寨火龍](../Page/埔寨火龍.md "wikilink")\[1\]，[四川](../Page/四川.md "wikilink")[成都市](../Page/成都市.md "wikilink")[黃龍溪](../Page/黃龍溪.md "wikilink")[春節期間的的](../Page/春節.md "wikilink")[黃龍溪燒火龍](../Page/黃龍溪燒火龍.md "wikilink")（又稱[火龍燈舞](../Page/火龍燈舞.md "wikilink")）\[2\]\[3\]及[洛帶的](../Page/洛帶.md "wikilink")[劉家龍](../Page/劉家龍.md "wikilink")\[4\]。[江西](../Page/江西.md "wikilink")[婺源有中秋節的](../Page/婺源.md "wikilink")[婺源遊火龍](../Page/婺源遊火龍.md "wikilink")（又稱[婺源舞草龍](../Page/婺源舞草龍.md "wikilink")）\[5\]。[台灣有](../Page/台灣.md "wikilink")[苗栗市在元宵節舉行的](../Page/苗栗市.md "wikilink")[苗栗𪹚龍和](../Page/苗栗𪹚龍.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")[三義鄉的](../Page/三義鄉_\(臺灣\).md "wikilink")[三義雲火龍節](../Page/三義雲火龍節.md "wikilink")\[6\]\[7\]。海外華人地區亦有舞火龍，如[新加坡](../Page/新加坡.md "wikilink")[沙岡萬山福德祠在每年](../Page/沙岡萬山福德祠.md "wikilink")[土地誕有](../Page/土地誕.md "wikilink")[沙岡稻草火龍](../Page/沙岡稻草火龍.md "wikilink")。

中國少數民族的舞火龍則有[贵州省](../Page/贵州省.md "wikilink")[铜仁地区](../Page/铜仁.md "wikilink")[德江县的土家族人於元宵節舉行的](../Page/德江县.md "wikilink")[德江炸龍燈](../Page/德江炸龍燈.md "wikilink")、[廣西省](../Page/廣西省.md "wikilink")[富川瑤族人在元宵節舉行的](../Page/富川.md "wikilink")[富川炸龍等](../Page/富川炸龍.md "wikilink")。

## 製作及舞動

最基本是以稻草紮成龍形，再插上香，例如香港大坑和薄扶林村、江西婺源以及台灣三義的火龍就沿用這種製作方法\[8\]\[9\]，但有些地區的製作材料有變化，例如埔寨火龍、四川客家龍、苗栗𪹚龍是用竹篾做成龙的躯体，再糊上纸而成\[10\]。埔寨火龍又以[煙花](../Page/煙花.md "wikilink")、[火藥代替香枝](../Page/火藥.md "wikilink")\[11\]，這樣火焰會隨著龍舞動而飛出，龍看起來也像火燒一樣。

有些火龍並不是直接把燃燒的物料直接插上龍身，而是由人向龍拋擲燃燒中的物料，造成「火龍」效果，稱為**炸龍**，例如苗栗𪹚龍、四川的火龍、德江炸龍燈及富川炸龍等\[12\]\[13\]。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 參見

  - [舞水龍](../Page/舞水龍.md "wikilink")
  - [舞醉龍](../Page/舞醉龍.md "wikilink")
  - [舞獅](../Page/舞獅.md "wikilink")
  - [舞麒麟](../Page/舞麒麟.md "wikilink")

[Category:客家文化](../Category/客家文化.md "wikilink")
[舞火龍](../Category/舞火龍.md "wikilink")
[Category:土家族文化](../Category/土家族文化.md "wikilink")
[Category:瑤族文化](../Category/瑤族文化.md "wikilink")
[Category:香港非物質文化遺產](../Category/香港非物質文化遺產.md "wikilink")

1.  [试论埔寨火龙的奇特文化](http://www.kejiay.com/archives/fa9ae6daa67546f392929c6d0088c8ab.html)

2.  [黃龍溪火龍節](http://trip.elong.com/huanglongxi/jieqing/)

3.  [黄龙溪火龙灯舞](http://www.cdich.com/news.asp?newsid=164)

4.  [“四川客家龍”和火龍節](http://www.chinareviewnews.com/doc/1004/4/8/0/100448073.html?coluid=55&kindid=1161&docid=100448073&mdate=0912170037)

5.  [婺源舞草龍](http://news.sina.com.tw/article/20130906/10596501.html)

6.  [2011苗栗火旁龍](http://www.mlcg.gov.tw/mlcg_travelnew/album/index-1.php?m2=29&sid=1&page=2)

7.  [三義雲火龍節](http://www.ihakka.net/hakka12/5-2/)

8.
9.
10.
11.
12.
13.