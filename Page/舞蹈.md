**舞蹈**為[八大艺术之一](../Page/八大艺术.md "wikilink")\[1\]，是於三度空間中以身體為語言作「心智交流」現象之人体的运动表达艺术，一般有[音乐伴奏](../Page/音乐.md "wikilink")，以有[节奏的动作为主要表现手段的艺术形式](../Page/节奏.md "wikilink")。它一般借助音乐，也借助其他的道具。舞蹈本身有多元的社會意義及作用，包括[運動](../Page/運動.md "wikilink")、[社交](../Page/社交.md "wikilink")／[求偶](../Page/求偶.md "wikilink")、[祭祀](../Page/祭祀.md "wikilink")、[禮儀等](../Page/禮儀.md "wikilink")。

[Chinese_women_in_pink,_dancing_(2007-07-05).jpg](https://zh.wikipedia.org/wiki/File:Chinese_women_in_pink,_dancing_\(2007-07-05\).jpg "fig:Chinese_women_in_pink,_dancing_(2007-07-05).jpg")

## 歷史

### 西方

在人類文明起源前，舞蹈在儀式、禮儀、慶典和娛樂方面都十分重要。[考古提供舞蹈的痕跡](../Page/考古.md "wikilink")，例如9000年前在[印度的](../Page/印度.md "wikilink")[賓姆貝特卡的岩石壁畫和公元前](../Page/賓姆貝特卡的岩石壁畫.md "wikilink")3300年[埃及墓壁畫描繪的繪畫舞蹈人物](../Page/埃及.md "wikilink")。

### 東方

[中國在五千年以前就已經出現了舞蹈](../Page/中國.md "wikilink")，\[2\]產生於[奴隸社會](../Page/奴隸社會.md "wikilink")\[3\]，發展到[秦](../Page/秦.md "wikilink")[漢之際已形成一定特色](../Page/漢.md "wikilink")：長袖細腰、飄逸輕盈、技藝並重、剛柔相濟。\[4\][魏晉南北朝以來中外樂舞的大交流以及唐代帝王的重視](../Page/魏晉南北朝.md "wikilink")，[楊貴妃等舞蹈家的出現](../Page/楊貴妃.md "wikilink")，使中國表演性舞蹈空前繁榮。\[5\]

[宋](../Page/宋.md "wikilink")[元](../Page/元.md "wikilink")[明](../Page/明.md "wikilink")[清以後](../Page/清.md "wikilink")，中國的表演性舞蹈逐漸融入戲曲，成為塑造角色、表達情感、展示劇情的[戲曲化舞蹈](../Page/戲曲.md "wikilink")。

[中華人民共和國建立後](../Page/中華人民共和國.md "wikilink")，作為獨立的劇場藝術的舞蹈日益完善和成熟起來。\[6\]

## 特色

每一個舞蹈有它的主題，而這主題也要進行發展和變化。這和[音樂的情形是很相似的](../Page/音樂.md "wikilink")。\[7\]

### 拉丁舞

### 中國舞蹈

中國舞蹈有一個明顯的特點，那就是「構圖」的對稱性，左邊的人做了一種姿勢，右邊的人也做一種相應的姿勢。舞蹈的三要素是指表情（不單是臉部的表情，是全身的表情）、節奏和構圖。中
國舞蹈的表情和節奏各有特點，但特徵最顯著的是構圖。這種構圖在舞蹈中叫做「對稱的平衡」。\[8\]

## 分類

根據舞蹈的功能、用途，可分「實用舞蹈」（自娛）與「表演舞蹈」（娛人）兩大類；每大類下，再根據「時間」分為傳統與流行，共計即四類；
然後可再往下細分但無論那一個分類方法，其界線均已模糊，分類表示如下。

### 「實用舞蹈」（自娛）

以健身、怡情、益智、表現為目的，往往參與者即舞者兼觀眾，可不必觀眾。

  - 傳統實用舞蹈
      - [宗教舞蹈](../Page/宗教舞蹈.md "wikilink")
      - [西洋古代宮廷舞](../Page/西洋古代宮廷舞.md "wikilink")
  - 流行實用舞蹈
      - [流行社交舞](../Page/社交舞.md "wikilink")-主分快，慢，3，4步。
      - [土風舞](../Page/土風舞.md "wikilink")
          - [國際民俗舞蹈](../Page/國際民俗舞蹈.md "wikilink")
      - [秧歌舞](../Page/秧歌舞.md "wikilink")
      - [霹靂舞](../Page/霹靂舞.md "wikilink")
      - [迪斯科](../Page/迪斯科.md "wikilink")
      - [爵士舞](../Page/爵士舞.md "wikilink")
      - [街頭爵士舞](../Page/街頭爵士舞.md "wikilink")
      - [踏板舞](../Page/踏板舞.md "wikilink")
      - [健康舞](../Page/有氧健身操.md "wikilink")

[越南民族舞蹈.JPG](https://zh.wikipedia.org/wiki/File:越南民族舞蹈.JPG "fig:越南民族舞蹈.JPG")的民族舞蹈\]\]

### 「表演舞蹈」（娛人）

以表演或欣賞為目的，參與者分為演出者及觀眾。

  - 傳統表演舞蹈
      - [東洋古代古典舞](../Page/東洋古代古典舞.md "wikilink")：含日本[德川](../Page/德川.md "wikilink")、[江戶幕府及](../Page/江戶幕府.md "wikilink")[高麗時期的宮廷舞蹈](../Page/高麗時期.md "wikilink")
      - [中國古代古典舞](../Page/中國古典舞.md "wikilink")：主要是中國宮廷舞蹈，又稱「宮廷舞」選用的創作素材皆為古典題材、典故、古代歷史的傳說等。
          - [彩帶舞](../Page/彩帶舞.md "wikilink")、[蓮香](../Page/蓮香_\(舞蹈\).md "wikilink")、[扇子舞](../Page/扇子舞.md "wikilink")、[劍舞](../Page/劍舞.md "wikilink")、[刀舞](../Page/刀舞.md "wikilink")
      - [芭蕾舞](../Page/芭蕾舞.md "wikilink")/[古典芭蕾舞](../Page/古典芭蕾舞.md "wikilink")
      - [民族舞蹈](../Page/民族舞蹈.md "wikilink")
          - [西班牙](../Page/西班牙.md "wikilink")[弗拉明戈舞](../Page/弗拉明戈舞.md "wikilink")
          - [夏威夷](../Page/夏威夷.md "wikilink")[草裙舞](../Page/草裙舞.md "wikilink")
          - [阿拉伯](../Page/阿拉伯.md "wikilink")[肚皮舞](../Page/肚皮舞.md "wikilink")
          - [中國民間舞蹈與](../Page/中國.md "wikilink")[少數民族舞蹈](../Page/少數民族.md "wikilink")（[蒙古](../Page/蒙古.md "wikilink")、[藏族](../Page/藏族.md "wikilink")、[回族等等](../Page/回族.md "wikilink")）：[套路](../Page/套路.md "wikilink")、[维吾尔族的](../Page/维吾尔族.md "wikilink")[木卡姆](../Page/木卡姆.md "wikilink")，[藏族的](../Page/藏族.md "wikilink")[囊玛](../Page/囊玛.md "wikilink")，[壮族的](../Page/壮族.md "wikilink")[铜鼓舞](../Page/铜鼓舞.md "wikilink")，[傣族的](../Page/傣族.md "wikilink")[孔雀舞](../Page/孔雀舞.md "wikilink")，[彝族的](../Page/彝族.md "wikilink")[跳月](../Page/跳月.md "wikilink")，[苗族的](../Page/苗族.md "wikilink")[芦笙舞](../Page/芦笙舞.md "wikilink")、[朝鲜族](../Page/朝鲜族.md "wikilink")[农乐舞](../Page/农乐舞.md "wikilink")
  - 流行表演舞蹈
      - [現代舞](../Page/現代舞.md "wikilink")
      - [舞踏](../Page/舞踏.md "wikilink")
      - [豔舞](../Page/豔舞.md "wikilink")
      - [現代芭蕾舞](../Page/現代芭蕾舞.md "wikilink")
      - [國際標準交誼舞](../Page/國際標準交誼舞.md "wikilink")（[體育舞蹈](../Page/體育舞蹈.md "wikilink")）
          - [摩登舞](../Page/摩登舞.md "wikilink")（Modern Dance）
              - [華爾茲](../Page/華爾茲.md "wikilink")
              - [維也納華爾茲](../Page/維也納華爾茲.md "wikilink")
              - [探戈](../Page/探戈.md "wikilink")
              - [狐步](../Page/狐步.md "wikilink")
              - [快步舞](../Page/快步舞.md "wikilink")
          - [拉丁舞](../Page/拉丁舞.md "wikilink")
              - [桑巴](../Page/桑巴.md "wikilink")（[森巴](../Page/森巴.md "wikilink")）
              - [倫巴](../Page/倫巴.md "wikilink")
              - [恰恰恰](../Page/恰恰恰.md "wikilink")（Cha Cha）
              - [鬥牛舞](../Page/鬥牛舞.md "wikilink")
              - [牛仔舞](../Page/牛仔舞.md "wikilink")（[捷舞](../Page/捷舞.md "wikilink")）
      - [踢踏舞](../Page/踢踏舞.md "wikilink")
      - [街舞](../Page/街舞.md "wikilink")
      - [嘻哈](../Page/嘻哈.md "wikilink")（Hip hop）
          - [機械舞](../Page/機械舞.md "wikilink")（Popping）

          - [freestyle](../Page/freestyle.md "wikilink")

          - [鎖舞](../Page/鎖舞.md "wikilink")（Locking）

          - [旧学派嘻哈](../Page/旧学派嘻哈.md "wikilink")（Old school hip hop）

          -
          - [LA Style](../Page/LA_Style.md "wikilink")

          - [霹靂舞](../Page/霹靂舞.md "wikilink")（Breaking）

          - [House](../Page/House.md "wikilink")

          -
          - [Vogue](../Page/Vogue.md "wikilink")

          - [wakking](../Page/wakking.md "wikilink")
      - [钢管舞](../Page/钢管舞.md "wikilink")
      - [肚皮舞](../Page/肚皮舞.md "wikilink")

## 参考文献

## 外部链接

## 参见

  - [舞者](../Page/舞者.md "wikilink")
  - [舞曲](../Page/舞曲.md "wikilink")

{{-}}

[舞蹈](../Category/舞蹈.md "wikilink")
[Category:娛樂職業](../Category/娛樂職業.md "wikilink")
[Category:戲劇相關職業](../Category/戲劇相關職業.md "wikilink")

1.  [藝術語彙›八大藝術](http://61.30.235.248/entelechy/pub/LIT_12.asp?ctyp=LITERATURE&catid=3227&pcatid=0)


2.  [中國舞蹈](http://big5.chinaqw.cn:89/node2/node116/node1486/node1495/node1523/index.html)


3.  [中國古典舞](http://www.skhlkmss.edu.hk/chinesedanceclub/chinese_old.htm)

4.
5.
6.
7.  [中國舞蹈的特點](http://big5.china.com.cn/book/txt/2006-10/18/content_7251195.htm)

8.