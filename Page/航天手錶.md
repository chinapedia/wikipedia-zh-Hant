[Aldrin_Apollo_11_original.jpg](https://zh.wikipedia.org/wiki/File:Aldrin_Apollo_11_original.jpg "fig:Aldrin_Apollo_11_original.jpg")是第二名踏上月球的人，他右手上佩戴的是歐米茄「超霸」手錶。\]\]
**航天手錶**是[太空人執行太空任務時所佩戴的手錶](../Page/太空人.md "wikilink")。除了要能承受升空時的強大震動和加速外，更要可抵受外太空的極端環境，包括[真空](../Page/真空.md "wikilink")、無重狀態、巨大溫差和[輻射等](../Page/輻射.md "wikilink")。

因此，航天手錶的規格有以下主要要求：

  - 防震
  - 純氧環境
  - 耐溫
  - 耐壓
  - 耐濕
  - 耐輻射和耐磁場環境
  - 不受聲音影響

此外，某些物料在地球環境是安全，但它們會在太空環境下釋放氣體，加重太空船空氣循環及[空氣淨化系統的負荷](../Page/空氣淨化器.md "wikilink")。故此，在航天手錶物料的選擇上亦有一定的要求\[1\]。

航天手錶的安全性是另一考慮的因素，在真空的環境下，假若錶面被撞碎，一般高級腕錶所採用的人造[藍寶石水晶錶面會化為極小的碎片](../Page/藍寶石.md "wikilink")，在無重狀態下對太空人極為危險。因此，航天手錶的錶面物料會使用[聚甲基丙烯酸甲酯的](../Page/聚甲基丙烯酸甲酯.md "wikilink")[Hesalite水晶](../Page/:en:Hesalite.md "wikilink")\[2\]。

## 生產航天手錶的廠商

[Vintage_Omega_Speedmaster_145.012-67.jpg](https://zh.wikipedia.org/wiki/File:Vintage_Omega_Speedmaster_145.012-67.jpg "fig:Vintage_Omega_Speedmaster_145.012-67.jpg")生產的航天手錶「超霸」\]\]
[Seiko_Spacewalk.JPG](https://zh.wikipedia.org/wiki/File:Seiko_Spacewalk.JPG "fig:Seiko_Spacewalk.JPG")生產的航天手錶Spacewalk\]\]
[Sts088-359-037.jpg](https://zh.wikipedia.org/wiki/File:Sts088-359-037.jpg "fig:Sts088-359-037.jpg")穿梭機任務成員[南希·居里手上佩戴的Timex](../Page/南希·居里.md "wikilink")
Ironman Triathlon Datalink 型號78401\]\]

到目前為止，只有寥寥可數的手錶廠商曾經生產航天手錶，其中包括：

  - [寶傑](../Page/寶傑.md "wikilink")(Poljot)－
    [俄羅斯鐘錶品牌](../Page/俄羅斯.md "wikilink")。是為前[蘇聯太空人所佩戴的航天手錶](../Page/蘇聯.md "wikilink")\[3\]。

<!-- end list -->

  - [豪雅表](../Page/豪雅表.md "wikilink")(TAG Heuer)-
    是首枚進入太空的[瑞士腕錶品牌](../Page/瑞士.md "wikilink")。1962年的[水星-宇宙神6號載人任務](../Page/水星-宇宙神6號.md "wikilink")，太空人[約翰·格倫所佩戴的便是豪雅出品的](../Page/約翰·格倫.md "wikilink")[碼錶](../Page/計時碼錶.md "wikilink")(秒表)，該腕錶的编號為2915A，具有12小時的動力和5分之1秒的精準度\[4\]。

<!-- end list -->

  - [劳力士](../Page/劳力士.md "wikilink")(Rolex) )-
    [瑞士腕錶品牌](../Page/瑞士.md "wikilink")。

<!-- end list -->

  - [Soviet Strella](../Page/Soviet_Strella.md "wikilink") - 前苏联手表品牌。

<!-- end list -->

  - [百年靈](../Page/百年靈.md "wikilink")([Breitling](../Page/:en:Breitling.md "wikilink"))-
    1962年「[水星-宇宙神7號](../Page/水星-宇宙神7號.md "wikilink")」任務的曙光7號太空人[斯科特·卡彭特進行軌道探索時佩戴的是百年靈Navitimer](../Page/斯科特·卡彭特.md "wikilink")
    Cosmonaute型號腕錶。此表的特點是指針在錶盤上運行一圈為24小時，避免太空人混淆上午和下午\[5\]。

<!-- end list -->

  - [歐米茄](../Page/歐米茄.md "wikilink")-
    [瑞士鐘錶品牌](../Page/瑞士.md "wikilink")。型號「超霸」
    ([Speedmaster
    Professional](../Page/:en:Omega_Speedmaster.md "wikilink"))，自從1965年歐米茄超霸計時錶便獲得[美國太空總署認可參與所有載人太空飛行任務](../Page/美國太空總署.md "wikilink")，包括太空船的艙內及[艙外活動](../Page/艙外活動.md "wikilink")\[6\]。其中，1969年7月20日人類首次登陸月球的[太陽神11號任務中](../Page/太陽神11號.md "wikilink")，因太空人所佩戴的是超霸錶，所以當時與及後的超霸型號也被稱為「月球錶」(moonwatch)。此外，在1975年「阿波羅-聯盟測試計劃|太陽神聯盟號測試計劃」（Apollo
    Soyuz Test
    Project）的美國與蘇聯太空會合任務，兩國的太空人均同時佩戴超霸系列腕錶，自此，超霸錶亦成為蘇聯載人太空任務的指定計時錶\[7\]。自2000年，國際太空站上的太空人均是佩戴「超霸專業X-33」腕錶\[8\]。「超霸專業X-33」較「超霸專業」擁有更多功能，外殼以鈦金屬製造，顯示包括指針和液晶，可提供[執行任務時間](../Page/:en:Mission_Elapsed_Time.md "wikilink")、世界時間、倒數計時、國際和美式日期等功能等。值得一提的是其鬧鈴提示功能，除了可發出高達到80[分貝的聲音外](../Page/分貝.md "wikilink")，更同時會以振動提示，因為在[艙外活動時](../Page/艙外活動.md "wikilink")，聲音是無法於真空下傳遞。

<!-- end list -->

  - [Fortis](../Page/:en:Fortis_Uhren_AG.md "wikilink")-
    [瑞士腕錶品牌](../Page/瑞士.md "wikilink")。自1994年成為[俄羅斯指定的航天手錶生產商](../Page/俄羅斯.md "wikilink")\[9\]。

<!-- end list -->

  - [飛亞達](../Page/飛亞達.md "wikilink")－
    [中國腕錶品牌](../Page/中國.md "wikilink")。為中國指定的航天手錶生產商。曾為「[神舟五號](../Page/神舟五號.md "wikilink")」、「[神舟六號](../Page/神舟六號.md "wikilink")」及「[神舟七號](../Page/神舟七號.md "wikilink")」航太員提供計時裝備\[10\]。

<!-- end list -->

  - [精工](../Page/精工.md "wikilink")－
    [日本鐘錶品牌](../Page/日本.md "wikilink")，其Spring Drive
    Spacewalk型號是配合一位自費太空人[理查·蓋瑞特](../Page/理查·蓋瑞特.md "wikilink")(Richard
    Garriott)所設計生產的航天手錶\[11\]。依靠該腕錶的專利設計Spring
    Drive，在他12天的太空旅程中，無須手動上鏈。及後，另外兩只Spacewalk腕錶於2008年12月23日更通過為時5小時38分鐘的「[艙外活動](../Page/艙外活動.md "wikilink")」(太空漫步)測試\[12\]。

<!-- end list -->

  - [天美時](../Page/天美時.md "wikilink")([Timex](../Page/:en:Timex_Group_USA.md "wikilink"))-
    美國腕錶品牌。Timex
    Datalink系列是[美國太空總署認可的航天手錶](../Page/美國太空總署.md "wikilink")。曾經為美國和俄羅斯兩國的太空人所佩戴\[13\]\[14\]。

## 資料來源

## 參考來源

  - [History of the Omega Speedmaster Moon
    Watch](http://www.yorktime.com/articles/200405212862)
  - [新華網：伴楊利偉太空之旅的中國第一隻航太表揭開神秘面紗](http://www.xinhuanet.com/chinanews/2003-10/29/content_1116422.htm)

## 外部連結

  - [超霸系列太空歷史](http://www.omegawatches.com.hk/zh/spirit/pioneering/speedmaster-history)
  - [精工Spring Drive官方網站](http://www.seikospringdrive.com/)
  - [Fortis官方網站](http://www.fortis-watches.com/)
  - [飛亞達官方網站](http://www.fiyta.com.cn/)
  - [航天手表](http://www.jibin123.com/)

[Category:時鐘](../Category/時鐘.md "wikilink")

1.
2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.