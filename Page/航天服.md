[Yang_Liwei_space_suit.JPG](https://zh.wikipedia.org/wiki/File:Yang_Liwei_space_suit.JPG "fig:Yang_Liwei_space_suit.JPG")制造的太空衣，曾為[楊利偉所穿](../Page/楊利偉.md "wikilink")\]\]
[Orlan-MKS-MAKS2013.JPG](https://zh.wikipedia.org/wiki/File:Orlan-MKS-MAKS2013.JPG "fig:Orlan-MKS-MAKS2013.JPG")制造的[Orlan](../Page/w:en:Orlan_space_suit.md "wikilink")-MKS
2 太空衣 (2013年)\]\]

**太空衣**是保护太空人在太空不受低温，[射线等的侵害并提供人类生存所需的](../Page/射线.md "wikilink")[氧气的保护服](../Page/氧气.md "wikilink")。

太空衣的：-{zh-hans:氧气罐;zh-hant:氧氣樽}-為太空人提供氧氣。而排出的[二氧化碳則由](../Page/二氧化碳.md "wikilink")[氫氧化鋰](../Page/氫氧化鋰.md "wikilink")（lithium
hydroxide）所吸收。太空衣的表層有阻隔[輻射的功用](../Page/輻射.md "wikilink")。太空人的體溫則由一套貼身內衣調節，這件內衣佈滿[水管](../Page/水.md "wikilink")，水泵不斷把水循環，把太空人身體所發出的熱帶走，而水則由[升華器](../Page/升華.md "wikilink")（sublimator）所冷卻。太空衣最後一個重要功用，是為太空人提供所需的氣壓（約等於半個標準大氣壓力52kpa）；如果[氣壓過低](../Page/氣壓.md "wikilink")，人體[血液及身體](../Page/血液.md "wikilink")[組織內的](../Page/組織.md "wikilink")[氣體會離開](../Page/氣體.md "wikilink")，令太空人患上類似潛水員常有的[潛水病](../Page/潛水.md "wikilink")（在[真空的情況下](../Page/真空.md "wikilink")，[太空人更會由於血液瞬間](../Page/太空人.md "wikilink")「[沸騰](../Page/沸騰.md "wikilink")」而死亡）。

太空衣可以：

  - 保持太空人體溫;
  - 保持壓力平衡（使太空人承受的壓力與在地球上的相似）;
  - 阻擋強而有害的輻射（如來自太陽的輻射）和隕石;
  - 處理宇航員的排泄物
  - 提供氧及排出二氧化碳

[美国的太空衣价值大约在](../Page/美国.md "wikilink")100万美元。

太空衣一般分两种：舱内宇航服和舱外宇航服。

## 舱内宇航服

舱内宇航服是宇航员在载人航天器座舱内使用的，一般是在发射时和返回地球时穿用，一旦座舱发生气体泄漏和气压突然变低时，舱内宇航服迅速充气，起保护宇航员的作用。

## 舱外宇航服

舱外宇航服是宇航员出航活动，进行太空漫步时使用。舱外宇航服的结构非常复杂，它具有加压、充气、防御宇航射线和微陨星袭击的作用，它里面有有通信系统、还有[生命保障系统](../Page/生命保障系统.md "wikilink")。现在广泛使用的舱外宇航服有美国的[EMU宇航服和俄罗斯的](../Page/舱外活动单元.md "wikilink")[海鹰宇航服两种](../Page/海鹰宇航服.md "wikilink")。

## 趣聞

美國太空總署曾在登月服鞋底印上美國國旗的計畫，但因為國際反對而作罷，因而在太空鞋底上印上國旗是違法的。

[category:载人航天](../Page/category:载人航天.md "wikilink")
[category:工作服](../Page/category:工作服.md "wikilink")