[Aerospaceplane.jpg](https://zh.wikipedia.org/wiki/File:Aerospaceplane.jpg "fig:Aerospaceplane.jpg")的艺术想象图\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:17K-AM.svg "fig:缩略图")

**航空航天飛機**，或**空天飛機**，是一種新型及大部份國家在研究及發展階段的航天运输系统，既能夠[航空亦能夠執行](../Page/航空.md "wikilink")[太空飛行](../Page/航天.md "wikilink")，是集[航空器](../Page/航空器.md "wikilink")、[太空運載工具及](../Page/太空運載工具.md "wikilink")[航天器於一身的](../Page/航天器.md "wikilink")[航空航天](../Page/航空航天.md "wikilink")[飞行器](../Page/飞行器.md "wikilink")，亦可以作為[載人航天器](../Page/載人航天器.md "wikilink")，可以重複使用。

空天飛機上同時有[飛機發動機和](../Page/飛機發動機.md "wikilink")[火箭發動機](../Page/火箭發動機.md "wikilink")，[起飞时也不使用火箭助推器](../Page/起飞.md "wikilink")，可以像[飛行器一樣從](../Page/飛行器.md "wikilink")[飛機場](../Page/飛機場.md "wikilink")[跑道上起飛](../Page/跑道.md "wikilink")，以[高超音速在](../Page/高超音速.md "wikilink")[大氣層飛行](../Page/大氣層.md "wikilink")，直接進入[太空](../Page/太空.md "wikilink")，成為[航天器](../Page/航天器.md "wikilink")，[降落時亦可以像飛機一樣在飛機場跑道上降落](../Page/降落.md "wikilink")。空天飛機將會是21世紀世界各國爭奪[制空權和](../Page/制空權.md "wikilink")[制天權的關鍵](../Page/制天權.md "wikilink")[武器之一](../Page/武器.md "wikilink")。目前[美國](../Page/美國.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")、[中國](../Page/中國.md "wikilink")、[日本及](../Page/日本.md "wikilink")[德国都在研究空天飛機](../Page/德国.md "wikilink")，但沒有获得實質成功，多數方案還是要靠拋棄式運載火箭升空，類似老式[太空梭觀念](../Page/太空梭.md "wikilink")。美國曾在90年代研發[冒险之星飛行器](../Page/冒险之星.md "wikilink")，但最後關鍵技術無法突破而終止。

2018年兩會期間[央視透漏一種中國](../Page/央視.md "wikilink")[空天飛機正在研發](../Page/空天飛機.md "wikilink")，其運載火箭被改成一種巨大的飛機型態載具，能水平起飛直入太空並全部回收降落，而其背負的上半部分是一種小型[太空梭與神龍曝光相片雷同](../Page/太空梭.md "wikilink")。\[1\]

## 参考文献

## 参见

  - [X-37試驗機](../Page/X-37試驗機.md "wikilink")
  - [神龙空天飞机](../Page/神龙空天飞机.md "wikilink")
  - [太空飛機](../Page/太空飛機.md "wikilink")
  - [雲霄塔](../Page/雲霄塔.md "wikilink")

[Category:航天器类型](../Category/航天器类型.md "wikilink")
[Category:航空器类型](../Category/航空器类型.md "wikilink")
[空天飞机](../Category/空天飞机.md "wikilink")
[Category:太空運載工具](../Category/太空運載工具.md "wikilink")
[Category:航空航天](../Category/航空航天.md "wikilink")

1.  [央視官方頻道-空天飛機在研](https://www.youtube.com/watch?v=f8g5-1GwBHk)