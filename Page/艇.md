[lifeboat.17-31.underway.arp.jpg](https://zh.wikipedia.org/wiki/File:lifeboat.17-31.underway.arp.jpg "fig:lifeboat.17-31.underway.arp.jpg")[多塞特郡](../Page/多塞特郡.md "wikilink")[普勒港的](../Page/普勒港.md "wikilink")[塞文类救生艇](../Page/塞文类救生艇.md "wikilink")。这是英国最大型号的救生艇，长达17米。\]\]
[Ambulanza_(new).jpg](https://zh.wikipedia.org/wiki/File:Ambulanza_\(new\).jpg "fig:Ambulanza_(new).jpg")[威尼斯的救护船](../Page/威尼斯.md "wikilink")\]\]
**艇**是一種在水面滑行或飄浮的中小型[船](../Page/船.md "wikilink")，通常排水量低於500噸，在內陸水域（[湖泊或](../Page/湖泊.md "wikilink")[河流](../Page/河流.md "wikilink")）或沿海地區航行，不過也有設計作近海航行的如捕鯨划艇。根據傳統航海[术语定義](../Page/术语.md "wikilink")，艇是「[体积小至可被其他船隻裝載的物體](../Page/体积.md "wikilink")」，不過現今已有不少小船如內河船、湖船、[渡輪的體積已大至不可被其他船隻裝載](../Page/渡輪.md "wikilink")。除了軍事用途外，民用[潛艇多半也算艇的一種](../Page/潛艇.md "wikilink")。

## 种类

[Tug_Boat_NY_1.jpg](https://zh.wikipedia.org/wiki/File:Tug_Boat_NY_1.jpg "fig:Tug_Boat_NY_1.jpg")\]\]

艇可以被分为以下三个种类:

  - 无动力或人力驱动艇：竹筏是无动力艇的典型代表，就意味着不需要额外动力只能向下游航行。而人力驱动艇依靠人力航行，包括独木舟、皮艇等。
  - [帆船](../Page/帆船.md "wikilink") ：以风力驱动船只航行的艇。
  - [马达快艇](../Page/马达快艇.md "wikilink")：在船尾外部装挂马达的一种小型高速快艇。

## 结构

艇主要有船身、[甲板](../Page/甲板.md "wikilink")、机舱和[龙骨的部件组成](../Page/龙骨.md "wikilink")。其中船身需要符合[浮力原理](../Page/浮力.md "wikilink")，保证艇可以浮在水面上。而一般一艘艇只会有一个甲板，机舱用于安置动力驱动装置和燃料，龙骨则用来固定整艘艇的框架。

[Ship_diagram-numbers.svg](https://zh.wikipedia.org/wiki/File:Ship_diagram-numbers.svg "fig:Ship_diagram-numbers.svg")和[舵](../Page/舵.md "wikilink"),
4- [舷](../Page/舷.md "wikilink"), 5- [锚](../Page/锚.md "wikilink"),
6- [球状船首](../Page/球状船首.md "wikilink"), 7- 船头, 8- 甲板, 9-  \]\]\]\]

## 动力

艇的动力来源通常如下:
[Jiajing_Emperor_on_his_state_barge.jpg](https://zh.wikipedia.org/wiki/File:Jiajing_Emperor_on_his_state_barge.jpg "fig:Jiajing_Emperor_on_his_state_barge.jpg")

  - 人力 (无固定支点或有固定支点的手划艇等等)
  - 风力 ([帆船](../Page/帆船.md "wikilink"))
  - 发动机驱动螺旋桨
      - [内置发动机](../Page/内置发动机.md "wikilink")
          - 内燃机 (汽油，柴油)
          - 蒸汽机 (煤, [燃油](../Page/燃油.md "wikilink"))
          - 核动力 (一些海军舰艇和潜艇)
      - [外置发动机](../Page/外置发动机.md "wikilink")
          - 电能
      - [明轮](../Page/明轮.md "wikilink")
      - 喷射机 ([摩托艇](../Page/摩托艇.md "wikilink"),
        [喷射艇](../Page/喷射艇.md "wikilink"))
      - 风机 ([气垫船](../Page/气垫船.md "wikilink"))

### 履带驱动

早期有一种罕见的类似于将坦克或履带车上的履带驱动驱动装置安装到小艇上的动力驱动装置。\[1\]

## 体育赛事

[赛艇是](../Page/赛艇.md "wikilink")[奥林匹克运动会的传统比赛项目之一](../Page/奥林匹克运动会.md "wikilink")，由一名或多名桨手坐在赛艇上,通过桨和桨架之间的简单[杠杆作用进行划水](../Page/杠杆作用.md "wikilink"),使赛艇前进的一项水上运动。

## 图片

Yacht and Sails.JPG <File:BOUALAML.the> boat south
mediterranean-Maghrebis.2.jpg EgyptTombOarboat.jpg Historic Center of
Quito - World Heritage Site by UNESCO - Photo 437.jpg A boat in
India.JPG Canoe-01.jpg| Babur crossing the river Son.jpg| Tug Boat NY
1.jpg DerelictBoatFollyIs.jpg <File:The> boat south
mediterranean-Maghrebis.jpg Boating in fair weather.jpg Jiajing Emperor
on his state barge.jpg|

## 另见

  - [独木舟](../Page/独木舟.md "wikilink")
  - [皮艇](../Page/皮艇.md "wikilink")
  - [筏](../Page/筏.md "wikilink")
  - [游艇](../Page/游艇.md "wikilink")

<!-- end list -->

  - [贡多拉](../Page/贡多拉.md "wikilink")
  - [马达快艇](../Page/马达快艇.md "wikilink")
  - [舢舨](../Page/舢舨.md "wikilink")
  - [渡轮](../Page/渡轮.md "wikilink")
  - [驳船](../Page/驳船.md "wikilink")

## 参考

<references />

## 外部連結

  - [帆船資料庫](http://www.sailingtheweb.net)

[Category:船艦](../Category/船艦.md "wikilink")

1.  *The Caterpillar Is Now Being Applied to Ships*, [Popular
    Science](../Page/Popular_Science.md "wikilink") monthly, December
    1918, page 68, Scanned by Google Books:
    <http://books.google.com/books?id=EikDAAAAMBAJ&pg=PA68>