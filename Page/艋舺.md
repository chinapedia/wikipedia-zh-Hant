**艋舺**（[巴賽語](../Page/巴賽語.md "wikilink")：Bangka；[台羅](../Page/台羅.md "wikilink")：\[1\]），又稱**文甲**，屬[大加蚋堡](../Page/大加蚋堡.md "wikilink")，清末與[臺北城](../Page/臺北城.md "wikilink")、[大稻埕並稱臺北三市街之一](../Page/大稻埕.md "wikilink")，即今[萬華之前身](../Page/萬華區.md "wikilink")。17世紀荷蘭的古地圖中，艋舺位置標示為Handelsplaasts，就是交易場所的意思，足見其商業傳統之古老\[2\]。

艋舺為[台北市發源地](../Page/台北市.md "wikilink")，亦為當時[台湾原住民商聚之地](../Page/台湾原住民.md "wikilink")，其最古老市街在「紗帽廚社」的故址「大溪口」，即今之[貴陽街與](../Page/貴陽街_\(台北市\).md "wikilink")[環河南路口](../Page/環河南路.md "wikilink")。[平埔族驱船与汉人交易](../Page/平埔族.md "wikilink")，因称船为Bangka，而被闽南语转称此地为“艋舺”。[台灣日治時期](../Page/台灣日治時期.md "wikilink")，因[臺灣話發音的](../Page/臺灣話.md "wikilink")「艋舺」，與佛典中[日語发音的](../Page/日語.md "wikilink")「萬華」（*banka*）相似，「艋舺」因此被當時[日本政府易字為](../Page/日本政府.md "wikilink")「萬華」，今日[臺灣閩南語猶稱其](../Page/臺灣.md "wikilink")「艋舺」，但是在地人的定義認為「艋舺」並不等於「萬華區」。

## 沿革

[永興亭.jpg](https://zh.wikipedia.org/wiki/File:永興亭.jpg "fig:永興亭.jpg")[永興亭](../Page/永興亭.md "wikilink")。\]\]
[剝皮寮歷史建築群位於康定路旁的街屋.jpg](https://zh.wikipedia.org/wiki/File:剝皮寮歷史建築群位於康定路旁的街屋.jpg "fig:剝皮寮歷史建築群位於康定路旁的街屋.jpg")位於[康定路旁的街屋](../Page/康定路_\(台北市\).md "wikilink")。\]\]
[西園路旁的萬華林宅.jpg](https://zh.wikipedia.org/wiki/File:西園路旁的萬華林宅.jpg "fig:西園路旁的萬華林宅.jpg")旁的[萬華林宅](../Page/萬華林宅.md "wikilink")。\]\]
[台北市政府舊廈(原建成小學校).jpg](https://zh.wikipedia.org/wiki/File:台北市政府舊廈\(原建成小學校\).jpg "fig:台北市政府舊廈(原建成小學校).jpg")（原[建成小學校](../Page/建成小學校.md "wikilink")），現作為[台北當代藝術館使用](../Page/台北當代藝術館.md "wikilink")。\]\]

### 行政區沿革

[萬華原是](../Page/萬華區.md "wikilink")[凱達格蘭族的](../Page/凱達格蘭族.md "wikilink")[雷里社](../Page/雷里社.md "wikilink")、[沙蔴廚社與](../Page/沙蔴廚社.md "wikilink")[里未社](../Page/里未社.md "wikilink")。原名**艋舺**，為[凱達格蘭語獨木舟的音譯](../Page/凱達格蘭語.md "wikilink")，因萬華西側為[新店溪的](../Page/新店溪.md "wikilink")[港口](../Page/港口.md "wikilink")，先民開發時有很多[獨木舟停靠](../Page/獨木舟.md "wikilink")，因而得名。[大正](../Page/大正.md "wikilink")9年（1920年），取萬年繁華之意，改稱**萬華**\[3\]。萬華於[康熙](../Page/康熙.md "wikilink")23年（1684年）劃屬[諸羅縣](../Page/諸羅縣.md "wikilink")。[雍正元年](../Page/雍正.md "wikilink")（1723年）改隸新成立的[彰化縣](../Page/彰化縣_\(清朝\).md "wikilink")。9年（1731年）改隸新成立的[淡水廳](../Page/淡水廳.md "wikilink")。[光緒元年](../Page/光緒.md "wikilink")（1875年）設立[台北府](../Page/台北府.md "wikilink")，改隸新成立的[淡水縣](../Page/淡水縣.md "wikilink")。11年（1885年）設立[福建臺灣省](../Page/福建臺灣省.md "wikilink")。[明治](../Page/明治.md "wikilink")28年（1895年）台灣進入[日治時期](../Page/台灣日治時期.md "wikilink")，萬華劃屬於[臺北縣](../Page/臺北縣_\(日治時期\).md "wikilink")[台北出張所與](../Page/台北出張所.md "wikilink")[枋橋出張所](../Page/枋橋出張所.md "wikilink")。30年（1897年）劃歸[臺北辨務署與](../Page/臺北辨務署.md "wikilink")[新莊辨務署](../Page/新莊辨務署.md "wikilink")。31年（1898年）新莊辨務署併入[三角湧辨務署](../Page/三角湧辨務署.md "wikilink")。33年（1900年）三角湧辨務署改為[大嵙崁辨務署](../Page/大嵙崁辨務署.md "wikilink")。34年（1901年）廢縣置廳，改隸[臺北廳](../Page/臺北廳.md "wikilink")[大加蚋堡與](../Page/大加蚋堡.md "wikilink")[擺接堡](../Page/擺接堡.md "wikilink")。42年（1909年）改隸[艋舺區與](../Page/艋舺區.md "wikilink")[古亭村區](../Page/古亭村區.md "wikilink")。大正9年（1920年）廢廳置州，改隸[臺北州](../Page/臺北州.md "wikilink")[臺北市](../Page/臺北市_\(州轄市\).md "wikilink")。1945年[台灣光復](../Page/台灣光復.md "wikilink")，臺北市歸類為[省轄市](../Page/省轄市.md "wikilink")，萬華劃屬於臺北市[龍山區與](../Page/龍山區.md "wikilink")[雙園區](../Page/雙園區.md "wikilink")。1967年台北市升格為[直轄市](../Page/直轄市.md "wikilink")。1990年合併龍山、雙園區與部分[古亭區成立萬華區](../Page/古亭區.md "wikilink")。

[清世宗](../Page/清世宗.md "wikilink")[雍正元年](../Page/雍正.md "wikilink")（1723年），[閩南](../Page/閩南.md "wikilink")[泉州三邑之](../Page/泉州三邑.md "wikilink")[泉安](../Page/泉安.md "wikilink")、[南安](../Page/南安.md "wikilink")、[惠安人士渡過](../Page/惠安.md "wikilink")[黑水溝](../Page/黑水溝.md "wikilink")，來到台灣台北，於此地搭建[茅草屋數棟](../Page/茅草.md "wikilink")，販賣[蕃薯為生](../Page/蕃薯.md "wikilink")，而漸成小[村落](../Page/村落.md "wikilink")，人稱“蕃薯市”，當時[平埔族](../Page/平埔族.md "wikilink")[原住民則多以](../Page/台灣原住民.md "wikilink")[獨木舟自](../Page/獨木舟.md "wikilink")[淡水河上游載運農](../Page/淡水河.md "wikilink")、獵產品等物與[漢人交易](../Page/漢人.md "wikilink")；平埔族人稱“獨木舟”為「Vanka
/
Banka」，故附近漢人以[閩南語](../Page/閩南語.md "wikilink")[譯音](../Page/翻譯.md "wikilink")，稱此地為「艋舺」（Bangkah）。

[清代移民於艋舺的有幾大](../Page/台灣清治時期.md "wikilink")[閩南人族群](../Page/閩南人.md "wikilink")，一是[泉州三邑人](../Page/泉州三邑.md "wikilink")，二則[泉州](../Page/泉州.md "wikilink")[安溪移民](../Page/安溪.md "wikilink")，三為泉州[同安移民](../Page/同安.md "wikilink")，這三個群體各自為政，三邑人建立了[艋舺青山宮等廟宇以凝聚團結](../Page/艋舺青山宮.md "wikilink")，但是以祭祀[觀音菩薩的](../Page/觀音菩薩.md "wikilink")[艋舺龍山寺為行政中心](../Page/艋舺龍山寺.md "wikilink")；安溪人則建立主祀[清水祖師的](../Page/清水祖師.md "wikilink")[艋舺清水巖祖師廟為信仰核心](../Page/艋舺清水巖.md "wikilink")，同安移民則在八甲庄\[4\][祭祀民宅中的](../Page/祭祀.md "wikilink")[霞海城隍](../Page/霞海城隍.md "wikilink")[神龕](../Page/神龕.md "wikilink")。

[清文宗](../Page/清文宗.md "wikilink")[咸豐](../Page/咸豐.md "wikilink")3年（1853年），三邑人以艋舺龍山寺為[基地](../Page/基地.md "wikilink")，發動「[頂下郊拚](../Page/頂下郊拚.md "wikilink")」[攻擊](../Page/分類械鬥.md "wikilink")，當時三邑人為了越過沼澤，設局燒毀了安溪人信仰中心艋舺清水巖祖師廟，並利用夜間突襲同安移民。此舉迫使奉祀霞海城隍的同安移民遷居到大稻埕。艋舺因港商之利，曾經盛極一時，於[清代](../Page/台灣清治時期.md "wikilink")，與[台南府城](../Page/台南市.md "wikilink")、[彰化](../Page/彰化縣.md "wikilink")[鹿港並列為三大重要](../Page/鹿港鎮.md "wikilink")[都市](../Page/都市.md "wikilink")，有「[一府二鹿三艋舺](../Page/一府二鹿三艋舺.md "wikilink")」之稱。

1860以後，艋舺港口因河沙淤積，逐漸被同安人群聚的大稻埕取代。

依據[台灣堡圖](../Page/台灣堡圖.md "wikilink")，[地籍意義上的](../Page/地籍.md "wikilink")「艋舺[大字](../Page/大字.md "wikilink")」的範圍北至[忠孝西路](../Page/忠孝西路.md "wikilink")，東至中華路一段（台北城西城牆），西至[淡水河](../Page/淡水河.md "wikilink")，南至三水街。現代一般人將三水街以南至縱貫鐵路（[萬華車站](../Page/萬華車站.md "wikilink")）\[5\]之間也當作艋舺的一部份，但是三水街以南至特三號排水溝（今西藏路）\[6\]屬於「下崁」大字。反而忠孝西路以南至成都路的地段本屬艋舺，現代一般人卻視為「[西門町](../Page/西門町.md "wikilink")」\[7\]。

## 參見

  - [艋舺 (電影)](../Page/艋舺_\(電影\).md "wikilink")
  - [台北歷史](../Page/台北歷史.md "wikilink")

## 註解

## 參考資料

[Category:台北市歷史](../Category/台北市歷史.md "wikilink")
[Category:源自台灣原住民語言的台灣地名](../Category/源自台灣原住民語言的台灣地名.md "wikilink")
[Category:台灣交通史](../Category/台灣交通史.md "wikilink")
[Category:台北市境內地區](../Category/台北市境內地區.md "wikilink")
[Category:大加蚋堡](../Category/大加蚋堡.md "wikilink")
[Category:萬華區](../Category/萬華區.md "wikilink")
[Category:源自閩南語的台灣地名](../Category/源自閩南語的台灣地名.md "wikilink")
[Category:臺北市大字](../Category/臺北市大字.md "wikilink")

1.  臺灣閩南語常用詞辭典
2.  翁佳音、曹銘宗，《大灣大員福爾摩沙：從葡萄牙航海日誌、荷西地圖、清日文獻尋找台灣地名真相》貓頭鷹出版，2016年
3.
4.  今日老松國小附近
5.  「鐵路地下化」後變成[艋舺大道](../Page/艋舺大道.md "wikilink")
6.  加蓋後變成西藏路
7.  這裏在[二次大戰後屬](../Page/二次大戰.md "wikilink")[城中區](../Page/城中區_\(台北市\).md "wikilink")，1990年[公家改劃入萬華區](../Page/公家.md "wikilink")