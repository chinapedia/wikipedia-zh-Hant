**艋舺龍津宮**位於[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[萬華區長沙街](../Page/萬華區.md "wikilink")2段184號，創建於清道光五年（1825年），\[1\]為主祀[福建](../Page/福建.md "wikilink")[泉州](../Page/泉州.md "wikilink")[晉江地區鄉土神](../Page/晉江.md "wikilink")[順正大王之](../Page/順正大王.md "wikilink")[民間信仰廟宇](../Page/民間信仰.md "wikilink")，該廟宇的組織型態為[管理人制](../Page/管理人.md "wikilink")。\[2\]

## 歷史

該廟創建於[清](../Page/台灣清領時期.md "wikilink")[道光五年](../Page/道光.md "wikilink")（1825年），由[晉江縣五店市](../Page/晉江縣.md "wikilink")（今[晉江市](../Page/晉江市.md "wikilink")[青陽街道](../Page/青陽街道.md "wikilink")）經商者自青陽石鼓廟奉請神尊來台，建廟於艋舺帆寮口（今長沙公園）附近，稱為「王公宮」。[日治時期](../Page/台灣日治時期.md "wikilink")[昭和八年](../Page/昭和.md "wikilink")（1933年）因防空理由被拆除。\[3\]

[日本二戰投降](../Page/日本二戰投降.md "wikilink")，[國民政府接管臺灣後](../Page/國民政府接管臺灣.md "wikilink")，民眾集資重建於環河南街147號，然而[民國六十二年](../Page/民國紀年.md "wikilink")（1973年）因[臺北市政府推動](../Page/臺北市政府.md "wikilink")[萬大計畫拓寬道路而遷於第二水門之外](../Page/萬大計畫.md "wikilink")。民國七十六年（1987年）又配合政府築堤政策第三次拆遷，經過委員、信徒討論後，與同受拆遷的聖德宮[福德正神合併](../Page/福德正神.md "wikilink")，同年於現址破土新建，民國八十二年（1993年）落成。\[4\]

## 供奉神祇

一樓正殿：順正府大王公、大太保、二太保、五王大巡（鎮東五位巡爺）、[臨水三奶夫人](../Page/臨水夫人#臨水三夫人.md "wikilink")（順天聖母、順懿夫人、平閩元君）、[太陽星君](../Page/太陽星君.md "wikilink")、[太陰星君](../Page/太陰星君.md "wikilink")、[福德正神](../Page/福德正神.md "wikilink")、[中壇元帥](../Page/中壇元帥.md "wikilink")、[太歲星君](../Page/太歲星君.md "wikilink")、[虎爺](../Page/虎爺.md "wikilink")

二樓前殿：[文昌帝君](../Page/文昌帝君.md "wikilink")、姜府王爺、薛府千歲

二樓後殿（財神殿）：[關聖帝君](../Page/關聖帝君.md "wikilink")、[文財神](../Page/文財神.md "wikilink")、[武財神](../Page/武財神.md "wikilink")、開基順正大王、大太保、二太保

## 祭典

  - 順正王公生辰慶典－[農曆九月初五](../Page/農曆.md "wikilink")，陣頭繞境並邀請信徒徒參與擲[爐主](../Page/爐主.md "wikilink")。
  - 萬華大拜拜（[靈安尊王生辰慶典前夕](../Page/靈安尊王.md "wikilink")）－農曆十月廿二，陣頭繞境。
  - 福德正神生辰慶典－農曆二月初二[頭牙](../Page/頭牙.md "wikilink")，頌經團祈福。\[5\]

## 參見

  - [順正大王](../Page/順正大王.md "wikilink")
  - [鹽埕天后宮](../Page/鹽埕天后宮.md "wikilink")
  - [德心宮](../Page/德心宮.md "wikilink")

## 參考文獻

<references>

\[6\]

\[7\]

\[8\]

</references>

[Category:萬華區廟宇](../Category/萬華區廟宇.md "wikilink")
[Category:1825年台灣建立](../Category/1825年台灣建立.md "wikilink")

1.
2.
3.
4.
5.
6.  臺北市政府民政局編，《臺北市寺廟概覽》，（臺北市：臺北市政府民政局），1994。

7.  〈艋舺龍津宮建宮沿革記略〉，1993年10月19日。

8.