《**艮齋詩集**》（），是[越南](../Page/越南.md "wikilink")[阮朝官員兼學者](../Page/阮朝.md "wikilink")、[華僑後代](../Page/華僑.md "wikilink")[鄭懷德](../Page/鄭懷德.md "wikilink")（，[號](../Page/號.md "wikilink")「艮齋」，這部詩集就是按照他的[號取名](../Page/號.md "wikilink")）的[漢文](../Page/文言文.md "wikilink")[詩集](../Page/詩歌.md "wikilink")，它既是[越南文學作品](../Page/越南文學.md "wikilink")，並具有[中](../Page/中國.md "wikilink")[越關係史之史料價值](../Page/越南.md "wikilink")。

## 成書過程

《艮齋詩集》是[鄭懷德親自編成的](../Page/鄭懷德.md "wikilink")。據《艮齋詩集‧自序》所述，這部詩集主要是他自己所作三部詩集的總集，分別是《觀光集》、《退食追編》及《可以集》。整部《艮齋詩集》的成書過程如下：

[嘉隆](../Page/嘉隆.md "wikilink")[甲子年](../Page/甲子年.md "wikilink")（1804年），他本人出使[中國](../Page/中國.md "wikilink")[清朝完畢及陪同](../Page/清朝.md "wikilink")[阮世祖皇帝回到](../Page/阮福映.md "wikilink")[富春京後](../Page/順化.md "wikilink")，適值有有段工餘時間，便「爰因使行《觀光集》（[鄭懷德本人出使](../Page/鄭懷德.md "wikilink")[中國期間著作的詩集](../Page/中國.md "wikilink")）業編成帙，及蒐索從前詩草，多已散失，止憑所記憶，及得諸朋友傳誦之口，而追編之，終不能得十分之一二，名曰《退食追編》，彙次紀年，以表其時屬正偽、運際窮通，并自註事蹟名號，庶備他時采風之一助可也。」簡言之，《觀光集》及《退食追編》是他出使[中國後不久](../Page/中國.md "wikilink")，乘著空餘整理過往詩作而成的。

其後，[嘉隆](../Page/嘉隆.md "wikilink")[丙子年](../Page/丙子.md "wikilink")（1816年），[鄭懷德在](../Page/鄭懷德.md "wikilink")[嘉定城就任官職時](../Page/胡志明市.md "wikilink")，由於自己所藏的《觀光集》及《退食追編》兩本詩集開始「蟲蠹侵蝕」，便「遽行輯補」，然後又「收拾[甲子年](../Page/甲子年.md "wikilink")（1804年）已後，凡應制、送贈、哀輓諸作，與夫閑時教正子弟門客，永物諸小題埃，次臚列綴於[丙子之下](../Page/丙子.md "wikilink")，名曰《可以集》，而總以本齋，命為《艮齋詩集》。」[鄭懷德把較晚一段時期的詩作](../Page/鄭懷德.md "wikilink")，編成《可以集》，再把過往《觀光集》、《退食追編》兩部詩作合成一部。而《自序》則寫成於[嘉隆十八年](../Page/嘉隆.md "wikilink")（1819年），《艮齋詩集》就大約在這個時候刊行的。\[1\]

## 內容

### 篇幅

  - 《卷首》：為[阮朝官員](../Page/阮朝.md "wikilink")[阮迪吉的](../Page/阮迪吉.md "wikilink")《詩序》、[吳時位](../Page/吳時位.md "wikilink")《詩跋》及[高輝耀](../Page/高輝耀.md "wikilink")《詩跋》。
  - 《退食追編》：蒐集了作者自1782年（[黎](../Page/後黎朝.md "wikilink")[景興四十三年](../Page/景興.md "wikilink")）至1801年（[西山朝](../Page/西山朝.md "wikilink")[景盛九年](../Page/景盛.md "wikilink")）期間的舊詩作，共127首。
  - 《觀光集》：蒐集了作者在1802年至1803年（[嘉隆元年至二年](../Page/嘉隆.md "wikilink")）奉命出使[中國](../Page/中國.md "wikilink")[清朝為](../Page/清朝.md "wikilink")[阮朝求封時](../Page/阮朝.md "wikilink")，往返途中所寫成的詩作，共152首。
  - 《可以集》：蒐集作者在1804年至1818年（[嘉隆三年至十七年](../Page/嘉隆.md "wikilink")）期間應制、贈送、哀輓及雜咏等詩作，凡48首。
  - 《自序》：是作者敍述家世、履歷、出使[中國及有關](../Page/中國.md "wikilink")《艮齋詩集》成書刊行的情況，題為[嘉隆十八年](../Page/嘉隆.md "wikilink")（1819年）[農曆三月二十日寫成](../Page/農曆.md "wikilink")。\[2\]

### 有關[中](../Page/中國.md "wikilink")[越關係史的記述](../Page/越南.md "wikilink")

《艮齋詩集》裡收錄的一些詩句，刻劃了[中國](../Page/中國.md "wikilink")[清王朝作為當時](../Page/清王朝.md "wikilink")[宗主國所展現的泱泱之風](../Page/朝貢體系.md "wikilink")，以及[中](../Page/中國.md "wikilink")[越關係友好的情形](../Page/越南.md "wikilink")。

如《艮齋詩集‧觀光集》裡收錄的《直隸道中書事》一詩，描述[中國首都的富盛景像](../Page/中國.md "wikilink")：

又如《使部出南闗囘國<small>口占</small>》，寫道[中](../Page/中國.md "wikilink")[越邊關的平和友好](../Page/越南.md "wikilink")：

### [華僑方面的史料](../Page/華僑.md "wikilink")

[鄭懷德祖先原籍](../Page/鄭懷德.md "wikilink")[福建](../Page/福建.md "wikilink")[福州](../Page/福州.md "wikilink")[長樂縣](../Page/長樂縣.md "wikilink")[福湖鄉](../Page/福湖鄉.md "wikilink")，他在《艮齋詩集‧自序》裡講述了先人移居[越南的故事](../Page/越南.md "wikilink")，說他的祖先「會[大清初入](../Page/大清.md "wikilink")[中國](../Page/中國.md "wikilink")，不堪變服剃頭之令，留髮南投客於[邊和鎮福隆府平安縣清河社](../Page/同奈省.md "wikilink")，受一廛而為氓。初試[陶朱之技](../Page/范蠡.md "wikilink")，終博[陶朱之名](../Page/范蠡.md "wikilink")，竟成鹿洞（俗名仝狔）巨擘。」\[3\]這都反映了[明](../Page/明.md "wikilink")[清之際](../Page/清.md "wikilink")[華僑在外地的奮鬥經歷](../Page/華僑.md "wikilink")。

## 學術價值

學者[陳荊和認為](../Page/陳荊和.md "wikilink")，《艮齋詩集》不啻是一部文學作品，而且是18、19世紀之交的[中](../Page/中國.md "wikilink")[越交涉史的一部寶貴史料](../Page/越南.md "wikilink")，尤其《自敍》部份及詩中的註文，既闡明了[鄭懷德的生平](../Page/鄭懷德.md "wikilink")，亦提供了[越南](../Page/越南.md "wikilink")[華僑史料](../Page/華僑.md "wikilink")，對於研究[越南歷史及](../Page/越南歷史.md "wikilink")[華僑史有莫大裨益](../Page/華僑.md "wikilink")。\[4\]

## 流傳情況

據[陳荊和所說](../Page/陳荊和.md "wikilink")，《艮齋詩集》保留在[越南](../Page/越南.md "wikilink")[順化](../Page/順化.md "wikilink")、[河內及](../Page/河內.md "wikilink")[西貢的有關學術部門及單位](../Page/胡志明市.md "wikilink")。在[越南國外方面](../Page/越南.md "wikilink")，《艮齋詩集》曾於1960年代的[香港](../Page/香港.md "wikilink")，由[新亞研究所東南亞研究室輯](../Page/新亞研究所東南亞研究室.md "wikilink")，並由[新亞研究所出版](../Page/新亞研究所.md "wikilink")。\[5\]

## 注釋

## 參考文獻

  -

<div class="references-small">

</div>

## 參見

  - [阮朝](../Page/阮朝.md "wikilink")
  - [越南歷史](../Page/越南歷史.md "wikilink")
  - [越南華人](../Page/越南華人.md "wikilink")

[Category:越南漢文典籍](../Category/越南漢文典籍.md "wikilink")
[Category:越南傳統文學](../Category/越南傳統文學.md "wikilink")
[Category:詩集](../Category/詩集.md "wikilink")
[Category:越南古代史書](../Category/越南古代史書.md "wikilink")
[Category:阮朝典籍](../Category/阮朝典籍.md "wikilink")
[Category:清朝典籍](../Category/清朝典籍.md "wikilink")
[Category:越南史書](../Category/越南史書.md "wikilink")
[Category:越南文學](../Category/越南文學.md "wikilink")

1.  鄭懷德《艮齋詩集‧自序》，133-134頁。

2.  陳荊和《艮齋詩集與其他諸作》，附錄於《艮齋詩集》（香港新亞研究所1962年版），19-20頁。

3.  鄭懷德《艮齋詩集‧自序》，126頁。

4.  陳荊和《艮齋詩集與其他諸作》，附錄於《艮齋詩集》（香港新亞研究所1962年版），21頁。

5.