[教宗](../Page/教宗.md "wikilink")**利奧十一世**（，，原名**亚历山德罗·奥塔维亚诺·德·美第奇**（），於1605年4月1日—1605年4月27日岀任教宗）是[天主教歷史上](../Page/天主教歷史.md "wikilink")[在位時間第九短的教宗](../Page/頭十位在位最短的教宗列表.md "wikilink")，在位僅僅二十七日。从母亲一方来看，他与在[佛羅倫斯勢力極大的](../Page/佛羅倫斯.md "wikilink")[美第奇家族有亲属关系](../Page/美第奇家族.md "wikilink")；他的母親，[弗朗切斯卡·萨尔维亚蒂](../Page/弗朗切斯卡·萨尔维亚蒂.md "wikilink")，是[贾科莫·萨尔维亚蒂和美第奇家族的教宗](../Page/贾科莫·萨尔维亚蒂.md "wikilink")[-{zh-cn:利奥十世;
zh-tw:利奧十世;
zh-hk:良十世;}-的姊妹](../Page/利奧十世.md "wikilink")[卢克丽热·德·美第奇的女兒](../Page/卢克丽热·德·美第奇.md "wikilink")；而他的父亲可能也是美第奇家族的一名远亲。他比較遲才成為教士，之前是[義大利中部](../Page/義大利.md "wikilink")[塔斯卡尼的大公](../Page/塔斯卡尼.md "wikilink")。他擔任教宗[庇護五世的大使的職務足足十五年](../Page/庇護五世.md "wikilink")，1573年，教宗[額我略十三世派遣他成為塔斯卡尼主教](../Page/額我略十三世.md "wikilink")，1574年成為佛羅倫斯大主教，1583年晉升為[樞機主教](../Page/樞機主教.md "wikilink")。

[Medici_popes.svg](https://zh.wikipedia.org/wiki/File:Medici_popes.svg "fig:Medici_popes.svg")\]\]

1596年，教皇[克勉八世派遣他為大使前往](../Page/克勉八世.md "wikilink")[法國](../Page/法國.md "wikilink")，那時[瑪麗·德·美第奇正當是法國的王后](../Page/瑪麗·德·美第奇.md "wikilink")。他而且是[菲利浦·內里的朋友和弟子](../Page/菲利浦·內里.md "wikilink")。1605年3月14日，在教宗克勉八世逝世十一日之後，六十二位樞機主教進入了[秘密會議](../Page/秘密會議.md "wikilink")。在眾多著名的候選人之中，最為了不起的是史學家[Baronius和著名的](../Page/Baronius.md "wikilink")[Jesuit
controversialist主教](../Page/Jesuit_controversialist.md "wikilink")[Bellarmine](../Page/Bellarmine.md "wikilink")。不過利奧十一世所領導的義大利籍的主教與法國主教結盟，使他達到當選教宗的票數。教宗利奧十一世;的當選使[西班牙國王](../Page/西班牙.md "wikilink")[腓力三世非常失望](../Page/腓力三世_\(西班牙\).md "wikilink")。法國國王[亨利四世足足花費了](../Page/亨利四世_\(法兰西\).md "wikilink")300,000[法郎來遊說樞機主教團選擇利奧十一世](../Page/法郎.md "wikilink");。

1605年4月1日，他当选羅馬教宗並命名為利奧十一世，然而他已經是七十多歲，垂垂老矣，在他的当选之後就在該月內病逝了。

## 譯名列表

  - \-{良十一世}-：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)、[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作良。
  - \-{利奧十一世}-：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=leo&mode=3)作利奧。
  - \-{利奧十一世}-、-{列奧十一世}-或-{萊奧十一世}-：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作利奧。
  - \-{雷歐十一世}-：[國立編譯舘](http://www.nict.gov.tw/tc/dic/search.php?p=54&ps=10&w1=leo&w2=&w3=&c1=&c2=c2&l=&u=&pr=&r=&o=2&pri=undefined)作雷歐。

[L](../Category/16世纪意大利人.md "wikilink")
[L](../Category/美第奇家族.md "wikilink")
[L](../Category/佛罗伦萨人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")