[Thrace_and_present-day_state_borderlines.png](https://zh.wikipedia.org/wiki/File:Thrace_and_present-day_state_borderlines.png "fig:Thrace_and_present-day_state_borderlines.png")
**色雷斯**（[希腊语](../Page/希腊语.md "wikilink")：Θρκη，[保加利亚语](../Page/保加利亚语.md "wikilink")：Тракия，[土耳其語](../Page/土耳其語.md "wikilink")：Trakya）是[东欧的](../Page/东欧.md "wikilink")[历史学和](../Page/历史学.md "wikilink")[地理学上的概念](../Page/地理学.md "wikilink")。今天的色雷斯包括了[保加利亚南部](../Page/保加利亚.md "wikilink")（北色雷斯）、[希腊北部](../Page/希腊.md "wikilink")（西色雷斯）和[土耳其的欧洲部分](../Page/土耳其.md "wikilink")（东色雷斯）。色雷斯濒临三个海，分别是[黑海](../Page/黑海.md "wikilink")、[爱琴海和](../Page/爱琴海.md "wikilink")[马尔马拉海](../Page/马尔马拉海.md "wikilink")。在土耳其，它也被称为[鲁米利亞](../Page/鲁米利亞.md "wikilink")（Rumeli）。

希腊北部西色雷斯为希腊[东马其顿和色雷斯大区的一部分](../Page/东马其顿-色雷斯.md "wikilink")，称色雷斯地区，辖[埃夫罗斯州](../Page/埃夫罗斯州.md "wikilink")、[克桑西州](../Page/克桑西州.md "wikilink")、[罗多彼州](../Page/罗多彼州.md "wikilink")。

东色雷斯面积24,476km²，西色雷斯面积8,578km²，北色雷斯面积42,155km²。

历史上色雷斯的边界经常发生变化。古色雷斯（即古色雷斯人居住的地域）包括了现在的保加利亚、土耳其的欧洲部分、希腊东北部、[塞尔维亚东北部的一部分以及](../Page/塞尔维亚.md "wikilink")[马其顿共和国的东部](../Page/马其顿共和国.md "wikilink")，北至[多瑙河](../Page/多瑙河.md "wikilink")，南濒[爱琴海](../Page/爱琴海.md "wikilink")，东至[黑海与](../Page/黑海.md "wikilink")[马尔马拉海](../Page/马尔马拉海.md "wikilink")，西达[瓦达河和](../Page/瓦达河.md "wikilink")[大摩拉瓦河](../Page/大摩拉瓦河.md "wikilink")。而作为[罗马行省的色雷斯面积则相对较小](../Page/罗马行省.md "wikilink")，北部边界退至了[巴尔干山脉](../Page/巴尔干山脉.md "wikilink")，西部边界则位于[美斯塔河](../Page/美斯塔河.md "wikilink")。

## 古代历史

[Thraciae-veteris-typvs.jpg](https://zh.wikipedia.org/wiki/File:Thraciae-veteris-typvs.jpg "fig:Thraciae-veteris-typvs.jpg")
[Thracian_Tomb_of_Kazanlak.jpg](https://zh.wikipedia.org/wiki/File:Thracian_Tomb_of_Kazanlak.jpg "fig:Thracian_Tomb_of_Kazanlak.jpg")中的壁画\]\]
色雷斯的原始居民被称为[色雷斯人](../Page/色雷斯人.md "wikilink")，分成许多部落聚族而居。为人所熟知的色雷斯军队曾与[亚历山大大帝一同横跨](../Page/亚历山大大帝.md "wikilink")[赫勒斯滂海峡](../Page/赫勒斯滂海峡.md "wikilink")，进攻[波斯](../Page/波斯.md "wikilink")，并参加了与[波斯人的战役](../Page/波斯人.md "wikilink")。

### 希腊神话中的色雷斯

在[希腊神话中](../Page/希腊神话.md "wikilink")，色雷斯人的祖先是[战神](../Page/战神.md "wikilink")[阿瑞斯的儿子](../Page/阿瑞斯.md "wikilink")[特亚克斯](../Page/特亚克斯.md "wikilink")，据说他最早迁居至色雷斯。在[荷马的](../Page/荷马.md "wikilink")《[伊利亚特](../Page/伊利亚特.md "wikilink")》中，色雷斯则是[特洛伊的盟友](../Page/特洛伊.md "wikilink")，当时的统帅是[阿卡玛斯](../Page/阿卡玛斯.md "wikilink")（Acamas）和[佩罗斯](../Page/佩罗斯.md "wikilink")（Peiros）。而在《伊利亚特》后半部又出现了色雷斯的一位国王[瑞索斯](../Page/瑞索斯.md "wikilink")（Rhesus）。另外，特洛伊[大安提诺的岳父](../Page/大安提诺.md "wikilink")——[希瑟乌斯](../Page/希瑟乌斯.md "wikilink")（Cisseus）也是色雷斯的一位国王。荷马史诗中的色雷斯大概西起于[阿克西奥斯河](../Page/阿克西奥斯河.md "wikilink")（Axios），东至[赫勒斯滂海峡和黑海](../Page/赫勒斯滂海峡.md "wikilink")。《伊利亚特》的船舶目录中提及了三支从特雷斯出发的远征队：从[埃努斯](../Page/埃努斯.md "wikilink")（Aenus）出发，由[阿卡玛斯和](../Page/阿卡玛斯.md "wikilink")[佩罗斯率领的色雷斯人](../Page/佩罗斯.md "wikilink")、从靠近[伊斯马洛斯](../Page/伊斯马洛斯.md "wikilink")（Ismaros）的南色雷斯出发，由[欧菲摩斯](../Page/欧菲摩斯.md "wikilink")（Euphemus）率领的[客科涅斯人](../Page/客科涅斯人.md "wikilink")（Cicones）和从靠近赫勒斯滂海峡的北色雷斯的[西斯塔斯](../Page/西斯塔斯.md "wikilink")（Sestus）出发，由[阿西俄斯](../Page/阿西俄斯.md "wikilink")（Asius）率领的第三只远征队。希腊神话中提及了多位色雷斯的国王，包括[狄俄墨得斯](../Page/狄俄墨得斯.md "wikilink")（Diomedes）、[忒柔斯](../Page/忒柔斯.md "wikilink")（Tereus）、[吕库尔戈斯](../Page/吕库尔戈斯.md "wikilink")（Lycurgus）、[菲纽斯](../Page/菲纽斯.md "wikilink")（Phineus）、[欧摩尔波斯](../Page/欧摩尔波斯.md "wikilink")（Eumolpus）、[波林涅斯托耳](../Page/波林涅斯托耳.md "wikilink")（Polymnestor）、[波尔堤斯](../Page/波尔堤斯.md "wikilink")（Poltys）和[俄阿格罗斯](../Page/俄阿格罗斯.md "wikilink")（Oeagrus，[俄耳甫斯之父](../Page/俄耳甫斯.md "wikilink")）。除了荷马所称的色雷斯人之外，古色雷斯的居民还包括其他众多部落，如[埃多尼亚人](../Page/埃多尼亚人.md "wikilink")（Edones）、[比萨尔提亚人](../Page/比萨尔提亚人.md "wikilink")（Bisaltae）、[客科涅斯人](../Page/客科涅斯人.md "wikilink")（Cicones）和[比斯托尼亚人](../Page/比斯托尼亚人.md "wikilink")（Bistones）。

### 历史学和考古学中的色雷斯

[ThracianCoin.JPG](https://zh.wikipedia.org/wiki/File:ThracianCoin.JPG "fig:ThracianCoin.JPG")
在公元前4世纪奥德里西亚国家建立之前，色雷斯人一直由于分散的部落形式而无法形成持久的政治组织。据古代有限的资料记载，在色雷斯的山区中居住的多是勇猛好战的部族，而平原地区多是与希腊人接触并受之影响的温顺部族。

这些曾被认为是野蛮人和被他们那些文雅和城市化的希腊邻居们统治着的[印欧语系的部族](../Page/印欧语系.md "wikilink")，其实已经发展出了较高等的音乐、诗歌、手工业和艺术形式。尽管他们经常结成小型的王国和部落联盟，但是在希腊古典时期却从未形成任何具有国家性质的长期联盟，或者制定王朝律法。和[高卢人及其他](../Page/高卢人.md "wikilink")[凯尔特部落类似](../Page/凯尔特部落.md "wikilink")，人们认为大部分的色雷斯人都居住在小型的防御性村落中，这些村落很多都位于山顶。尽管真正意义上的城市要到罗马时期才发展起来，但是当时仍然有许多兼具地区集市性质的大型坞堡。总之，尽管拜占庭等地已经开始被希腊殖民地化，色雷斯仍全然一片原野景色。

虽然色雷斯人很早即受到古希腊文化的影响，但他们仍将自己的语言和文化保存了相当长的一段时间。而某些神话甚至表明色雷斯人在很早的一个时期甚至影响过希腊文化，像[奥尔甫斯](../Page/奥尔甫斯.md "wikilink")（Orpheus）等色雷斯人甚至成为了神话中的主角。不过作为非希腊语民族，他们终究还是被希腊人看作是野蛮人。公元前6世纪，希腊人在色雷斯建立了第一个殖民地，并在色雷斯大规模招募骑兵和大规模开采金银矿。

色雷斯位于多瑙河南部的部分（除了培西人的领地）被[大流士时期的](../Page/大流士.md "wikilink")[波斯统治了近半个世纪](../Page/波斯.md "wikilink")，大流士曾在公元前513－512年组织了对该地区的远征。在波斯人撤退之后，马其顿王国扩张之前，色雷斯分裂成了三个部分（东部、中部和西部）。当时东色雷斯著名的统治者是[塞索布勒普提斯](../Page/塞索布勒普提斯.md "wikilink")（Cersobleptes），他曾试图把他的势力扩展到诸多色雷斯部落中，但是最终被马其顿人击败。

[ThracianTribes.jpg](https://zh.wikipedia.org/wiki/File:ThracianTribes.jpg "fig:ThracianTribes.jpg")
该地区于公元前4世纪被马其顿的[菲力二世征服](../Page/菲力二世.md "wikilink")，并被马其顿王国统治了一个半世纪之久。在四次[马其顿战争期间](../Page/马其顿战争.md "wikilink")，罗马人和色雷斯人也发生了冲突。当时马其顿统治阶层的毁灭令他们在色雷斯的统治也随即崩溃，当地的部族势力又活跃起来。公元前168年的[彼得那战役](../Page/彼得那战役.md "wikilink")（Battle
of
Pydna）之后，罗马对马其顿的统治逐渐稳固，色雷斯的治权也随之落入罗马人之手。然而不管是色雷斯人还是马其顿人都不甘心接受罗马的统治，在这个过渡时期发生了多起叛乱。公元前149年爆发的[安德里斯库斯叛乱](../Page/安德里斯库斯叛乱.md "wikilink")（the
revolt of
Andriscus）即得到了色雷斯人的支持。在此期间，也有多个亲罗马的部落，如[培西人等](../Page/培西人.md "wikilink")，曾多次入侵马其顿。

随着第三次马其顿战争的结束，在接下来的一个半世纪里，色雷斯成为了罗马的[附属国](../Page/附属国.md "wikilink")。当时色雷斯仍然由多个部落组成。[撒帕伊亚人](../Page/撒帕伊亚人.md "wikilink")（Sapaei）曾经在[拉斯库波利斯](../Page/拉斯库波利斯.md "wikilink")（Rhascuporis）麾下为罗马战斗，拉斯库波利斯曾是[庞培和](../Page/庞培.md "wikilink")[凯撒的得力助手](../Page/凯撒.md "wikilink")，而在共和国的最后时期他曾支持[共和国军对抗](../Page/共和国军.md "wikilink")[安东尼和](../Page/安东尼.md "wikilink")[屋大维](../Page/屋大维.md "wikilink")。帝国初期，拉斯库波利斯的继任者们都被深深卷入政治丑闻和谋杀之中，一系列的皇室谋杀案在多年里改变了当地的统治秩序。多个派系在罗马皇帝的支持下轮流掌权。

公元前279年，凯尔特高卢人入侵了马其顿、南希腊和色雷斯，不久又被赶出了马其顿和南希腊，但是却在色雷斯停留到本世纪末。从色雷斯出发，三个凯尔特部落分别进入[安纳托里亚](../Page/安纳托里亚.md "wikilink")（Anatolia），并建立了[加拉提亚王国](../Page/加拉提亚王国.md "wikilink")（Galatia）。

[Thracia_SPQR.png](https://zh.wikipedia.org/wiki/File:Thracia_SPQR.png "fig:Thracia_SPQR.png")
当46岁的[撒帕伊亚人的色雷斯国王](../Page/撒帕伊亚人.md "wikilink")[罗伊米塔尔克斯三世](../Page/罗伊米塔尔克斯三世.md "wikilink")（Roimitalkes
III）被其妻谋杀之后，色雷斯正式成为罗马的一个行省，刚开始时受到[资深长官](../Page/资深长官.md "wikilink")（Procurators）的统治，后来则由[禁卫军长官](../Page/禁卫军长官.md "wikilink")（Praetorian
Prefects）统治。罗马在色雷斯的统治中心是[皮林塔斯](../Page/皮林塔斯.md "wikilink")(Perinthus)，但是省内的各个地区往往是由总督手下的军事将领分别统治。尽管城市中心的缺失增加了管理色雷斯的难度，但是最终该行省在罗马的统治下还是繁荣起来了。不过罗马文化对该地区的影响并未取得进展，有人认为这是因为当时色雷斯仍然处于希腊文化的影响之下。

罗马对色雷斯的统治主要依靠驻扎在[默西亚的军团](../Page/默西亚.md "wikilink")。由于远离罗马统治中心，默西亚的驻军往往从当地部族军队补充士兵。在接下来的几个世纪里，色雷斯行省受到了来自[日耳曼部族越来越多的袭击](../Page/日耳曼.md "wikilink")。[查士丁尼一世统治时期该地区就修建了上百个军事堡垒来抵御这类袭击](../Page/查士丁尼一世.md "wikilink")。

## 文化

由于以骁勇善战著称，而在类似色雷斯的多岩石和多山地形中更是勇猛过人，色雷斯人经常被[叙利亚](../Page/叙利亚.md "wikilink")、[帕加马](../Page/帕加马.md "wikilink")（Pergamum）和[比提尼亚](../Page/比提尼亚.md "wikilink")（Bithynia）等地的[希腊化王国招募为](../Page/希腊化王国.md "wikilink")[雇佣兵](../Page/雇佣兵.md "wikilink")。不过他们也被认为过于昂贵且容易叛变。在公元前5－4世纪，色雷斯战士所用的武器主要是[矛和](../Page/矛.md "wikilink")[刀](../Page/刀.md "wikilink")。而在更早之前色雷斯步兵则是以[斧头为武装](../Page/斧头.md "wikilink")，他们的统帅则是乘坐[双轮战车](../Page/双轮战车.md "wikilink")。色雷斯的轻步兵则装备[标枪](../Page/标枪.md "wikilink")、[吊索或](../Page/吊索.md "wikilink")[弓](../Page/弓.md "wikilink")，其中以标枪为主。色雷斯的战士，特别是山地部族的战士，以使用一种特殊的武器而著称，该武器结合了剑、镰刀和长柄武器的特点，被称为[长柄逆刃刀](../Page/长柄逆刃刀.md "wikilink")（Rhomphaia）。在亚历山大大帝及其后的一段时期内，长柄逆刃刀在色雷斯步兵种得到了越来越广泛的使用，最后成为了色雷斯雇佣兵中的[抛矛兵](../Page/抛矛兵.md "wikilink")（peltast）的主要标志。即使是罗马军队也畏惧这种可怕的武器。当时除了[盖塔人](../Page/盖塔人.md "wikilink")（Gatae）外，所有的色雷斯军队都配备了骑兵。盖塔人通常的装备是两把标枪，可近距离戳刺，也可远距离抛掷。他们还携带典型的[双刃曲剑](../Page/双刃曲剑.md "wikilink")。色雷斯人的战斗策略也为人称道，他们善于在快速撤退之后发动突然的进攻，令敌人措手不及。色雷斯军队的支柱是抛矛兵，他们都是精通于近距离搏杀和远距离掷矛杀伤敌人技术的轻装步兵。除了盾牌之外，抛矛兵没有任何装甲防护。富裕的贵族则戴有能容纳其头顶发饰的尖顶头盔。

色雷斯人的历法类似于埃及人的历法。每年12个月，总共360天，在最后一个月之后加上5天；一年分为3个季节。

## 中世纪歷史

公元5世纪中叶，[罗马帝国开始崩溃](../Page/罗马帝国.md "wikilink")，色雷斯人逐渐摆脱了罗马的统治但转而又为[日耳曼人所奴役](../Page/日耳曼人.md "wikilink")。随着罗马的衰亡，在接下来一千年的大部分时间里色雷斯沦为了战场。罗马帝国东部的继承者[拜占庭帝国保持了对色雷斯的统治](../Page/拜占庭帝国.md "wikilink")，直到公元9世纪初时该地区的大部为[保加利亚所吞并](../Page/保加利亚.md "wikilink")。拜占庭在972年重新夺回了色雷斯，然而该地区在公元12世纪末又重为保加利亚占领。整个13世纪和14世纪的上半叶，色雷斯的统治权辗转于保加利亚和拜占庭两者之间。1265年，[金帐汗国蒙古人在](../Page/金帐汗国.md "wikilink")[那海汗](../Page/那海.md "wikilink")（Nogai
Khan）率领下入侵色雷斯。1352年，[鄂圖曼土耳其首次入侵色雷斯](../Page/鄂圖曼土耳其.md "wikilink")，花了20多年时间完全征服了该地区，并统治了长达5世纪之久。

## 现代历史

1878年，北色雷斯被土耳其的半自治行省[东鲁米利亚](../Page/东鲁米利亚.md "wikilink")（Eastern
Rumelia）吞并，东鲁米利亚又于1885年并入保加利亚。经历了[巴尔干战争](../Page/巴尔干战争.md "wikilink")、[第一次世界大战和](../Page/第一次世界大战.md "wikilink")[希腊-土耳其战争后](../Page/希土战争_\(1919年-1922年\).md "wikilink")，色雷斯的其它部分在20世纪初被保加利亚、希腊和土耳其瓜分。

## 参见

  - [色雷斯人](../Page/色雷斯人.md "wikilink")（[色雷斯人](../Page/色雷斯人.md "wikilink")）
  - [色雷斯语](../Page/色雷斯语.md "wikilink")（[色雷斯語](../Page/色雷斯語.md "wikilink")）

{{-}}

[色雷斯](../Category/色雷斯.md "wikilink")
[Category:保加利亚地理](../Category/保加利亚地理.md "wikilink")
[Category:希腊地理](../Category/希腊地理.md "wikilink")
[Category:土耳其地理](../Category/土耳其地理.md "wikilink")
[Category:罗马帝国行省](../Category/罗马帝国行省.md "wikilink")