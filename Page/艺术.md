[Art-portrait-collage_2.jpg](https://zh.wikipedia.org/wiki/File:Art-portrait-collage_2.jpg "fig:Art-portrait-collage_2.jpg")的[自畫像](../Page/自畫像.md "wikilink")，右上是非洲的雕塑，左下是日本冲绳的[風獅爺](../Page/風獅爺.md "wikilink")，右下是[桑德羅·波提切利的](../Page/桑德羅·波提切利.md "wikilink")[維納斯的誕生的一部份](../Page/維納斯的誕生_\(波提切利\).md "wikilink")\]\]
[Portrait_of_Joan_Miro,_Barcelona_1935_June_13.jpg](https://zh.wikipedia.org/wiki/File:Portrait_of_Joan_Miro,_Barcelona_1935_June_13.jpg "fig:Portrait_of_Joan_Miro,_Barcelona_1935_June_13.jpg")

**艺术**（；[法語](../Page/法語.md "wikilink")、；[西班牙語](../Page/西班牙語.md "wikilink")、；）指憑藉技巧、意願、[想象力](../Page/想象力.md "wikilink")、經驗等綜合人為因素的融合與平衡，以創作隱含美學的器物、環境、影像、動作或聲音的表達模式，也指和他人分享美的感覺或有深意的[情感與](../Page/情感.md "wikilink")[意識的人類用以表達既有感知且將個人或群體體驗沉澱與展現的過程](../Page/意識.md "wikilink")\[1\]。

## 範圍和分類

藝術傳統上包括以下種類：[文學藝術](../Page/文學.md "wikilink")（包括[詩歌](../Page/詩歌.md "wikilink")，[戲劇](../Page/戲劇.md "wikilink")，[小說等等](../Page/小說.md "wikilink")）、[視覺藝術](../Page/視覺藝術.md "wikilink")（[繪畫](../Page/繪畫.md "wikilink")，[素描](../Page/素描.md "wikilink")，[雕塑等](../Page/雕塑.md "wikilink")）、圖文設計、[造型藝術](../Page/造型藝術.md "wikilink")（如[雕塑](../Page/雕塑.md "wikilink")，造型），[裝飾藝術](../Page/裝飾藝術.md "wikilink")（如[馬賽克](../Page/馬賽克.md "wikilink")、[室内设计等](../Page/室内设计.md "wikilink")）\[2\]，[表演藝術](../Page/表演藝術.md "wikilink")（[戲劇](../Page/戲劇.md "wikilink")，[舞蹈](../Page/舞蹈.md "wikilink")，[廣告](../Page/廣告.md "wikilink")\[3\]，[音樂等](../Page/音樂.md "wikilink")）\[4\]。

## 藝術與科學

在[歐洲](../Page/歐洲.md "wikilink")17世紀到18世紀，是藝術與[科學一起進入知識累積與相互啓發而一同發展的關鍵時期](../Page/科學.md "wikilink")，這個過程促使藝術與科學的成品成為西方人文思想及文明展現的重要象徵\[5\]。歐洲[啟蒙運動下發展出的](../Page/啟蒙運動.md "wikilink")[百科全書亦是以藝術與科學所引發的人類群體新狀態而合編的以](../Page/百科全書.md "wikilink")[字典為標題的著作](../Page/字典.md "wikilink")\[6\]\[7\]\[8\]\[9\]。

## 語源

法文及英文（藝術）一詞源自於古希臘拉丁语，大義為「技巧」\[10\]，現在雖保有原意，卻也衍生出更廣義的涵義，幾乎包括了所有與人生密切相關的[創造性](../Page/創造.md "wikilink")[學問](../Page/學問.md "wikilink")。

中文的藝術一詞最早出自《[後漢書](../Page/後漢書.md "wikilink")》的文苑列傳上，劉珍篇有提到「校定東觀五經、諸子傳記、百家蓺術」，不過當時的藝術是指[六藝和術數方技等技能的總稱](../Page/六藝.md "wikilink")，和現代藝術的含意不同\[11\]。

以學域的涵容度而言，文学、绘画、音乐、舞蹈、雕塑、建筑、戏剧与电影，这些领域的[知識](../Page/知識.md "wikilink")，都足以單獨成立對應的[學系](../Page/學系.md "wikilink")。此外，[設計](../Page/設計.md "wikilink")、[攝影](../Page/攝影.md "wikilink")、[視頻遊戲](../Page/視頻遊戲.md "wikilink")（[電子遊戲](../Page/電子遊戲.md "wikilink")）以及實驗藝術等一些新興藝術學域，也逐漸被[學術界和大眾接受而被列為單獨的項目](../Page/學術界.md "wikilink")，许多藝術學院已经开設了相關的專業\[12\]。

#### 台湾

在中文的描述中，藝術與[美術常常劃上等號](../Page/美術.md "wikilink")，在台灣教育界也常常出現爭議，時常造成相關藝術類目學生的困惑。一般說來，[藝術是廣義和廣泛的概念](../Page/藝術.md "wikilink")，[美術是](../Page/美術.md "wikilink")[狹義的](../Page/狹義.md "wikilink")，僅是藝術的表達形式之一。“[繪畫](../Page/繪畫.md "wikilink")”、“[雕塑](../Page/雕塑.md "wikilink")”、“[建築](../Page/建築.md "wikilink")”、“[文學](../Page/文學.md "wikilink")”、“[音樂](../Page/音樂.md "wikilink")”、“[戲劇](../Page/戲劇.md "wikilink")”、“[舞蹈](../Page/舞蹈.md "wikilink")”、“[電影](../Page/電影.md "wikilink")”等都為藝術分類範疇。所謂的“美術”一般指的是以“繪畫”、“雕塑”為核心的[視覺藝術](../Page/視覺藝術.md "wikilink")。關於藝術與美術的中英譯名也常常出現爭議，一般來說藝術是指英文的[Art](../Page/Art.md "wikilink")，[純美術則以Fine](../Page/美术.md "wikilink")
Art來代表。

#### 中国大陆

在中国大陆，美术与艺术的词义并没有争议。美术为艺术的一种形式，包括绘画、雕塑、工艺美术、建筑艺术，也包括现代的摄影，和与艺术有关的设计。而艺术则还包括除了美术以外的其他艺术形式，例如音乐、舞蹈、戏剧、文学等。艺术的对应的英文单词为Art，而美术并没有直接对应的单词，常常也翻译为Art，这是由于Art这个英文单词本身的模糊性，既可作为艺术的总称，又可强调美术，造成的。实际上“[视觉艺术](../Page/视觉艺术.md "wikilink")”（）是一个更适合表达美术这个概念的词语，但由于它在英语中只有在讨论艺术分类时才会用到，并不是一个常用的词语，所以一般不用。

## 關於藝術認定爭議

[二十世紀以來至今](../Page/二十世紀.md "wikilink")，結合了[傳統定義藝術領域以外元素的多元藝術形式不斷出現](../Page/傳統.md "wikilink")，如常見的“科技藝術”（Technical
Art），“數位藝術”（Digital Art）、“裝置藝術”（Installation Art）、“觀念藝術”（Conceptual
Art）、“[行為藝術](../Page/行為藝術.md "wikilink")”、“生物藝術”等不勝枚舉。如此多樣的藝術形式隨著時間不斷的在挑戰藝術理論能夠定義的[極限](../Page/極限.md "wikilink")。關於藝術如何認定也已經成為開放式的問題，目前皆以當下的藝術界公認定義為準。

## 藝術的目的

在歷史上藝術有許多不同的功能，因此很難將藝術的目的抽象化成一個單一的概念。這不表示藝術的目的是模糊不清的，只是藝術的創造有許多不同的、唯一的原因。以下會列出一個藝術的目的，會分為無動機（non-motivated）及有動機的（motivated）兩類（Lévi-Strauss）。

### 無動機的目的

無動機的藝術是指那些本來就是人類不可或缺一部份的藝術，這類的藝術超越個人，或是不是為某一特定目的所創作。以此觀點，藝術和創造力一様，是人類依其天性而來的，因此超過實用的層面。

1.  人類對於和諧、平衡及節奏的本能：這個層次的藝術不是某個動作或是物品，而是內在對於平衡及和諧（美）的欣賞，因此是人的基本層面之一，不只是實用的層面而已。
2.  神秘的經驗：藝術提供個人可以體驗個人和宇宙之間的關係。這種體驗常常是無動機的，就像對於藝術、音樂或詩的欣賞一様。
3.  想像的表達：藝術提供表達想像的方式，而且這種方式是沒有文法的，因此不受口說或書面語言的形式影響。文字是循序式的，每個字有既定的定義，而藝術提供許多的形式、符號和概念，而其意義是可以變化的。
4.  儀式和象徵功能：在許多文化中，在儀式、表演及舞蹈中都有藝術，是種裝飾或是標誌。這些常常沒有特定實際的目的，人類學家認為這些是文化的一部份，多半不是由個人提供，而是許多世代變化之後的結果，也和此文化和宇宙的關係有關。

### 有動機的目的

有動機的藝術是指那些因為特定目的產生的藝術。可能是為了政治的變革、對社會的某一議題表示意見、表達特定的感情或是態度無動機的目的、陈述個人心理、描述另一個事物、銷售產品，或是作為一個交流的工具。

1.  交流：藝術是一種溝通交流的工具，許多的交流都是對另一個體有一定的動機，因此這是有動機的目的。
2.  娛樂：藝術一般會產生特定的情緒或態度，例如說使欣賞藝術的人放輕鬆，或是提供娛樂。
3.  [前衛](../Page/前衛.md "wikilink")，政治變革：二十世紀早期藝術的定義之一就是由視覺影像帶來政治變革。達到此目的的藝術運動——像[达达主义](../Page/达达主义.md "wikilink")、[超现实主义](../Page/超现实主义.md "wikilink")、[結構主義及](../Page/結構主義.md "wikilink")[抽象表現主義](../Page/抽象表現主義.md "wikilink")——則稱為[前衛藝術](../Page/前衛.md "wikilink")。
4.  藝術作為一「自由區」：免去社會責難的作用。[當代藝術強調對於文化差異的寬容](../Page/當代藝術.md "wikilink")，也強調其解放的功能（社會調查、行動、顛覆、解構主義），因此在研究和實驗上都更加開放。
5.  針對社會調查、顛覆或（和）[無政府主義的藝術](../Page/無政府主義.md "wikilink")：類似有關政治變革的藝術，顛覆或[解構主義會在沒有特定政治目的的情形下提出特定社會議題的問題](../Page/解構主義.md "wikilink")。此時藝術的功能只是批評社會的某一面向。
6.  針對社會議題的藝術：藝術可以用來喚醒大家對許多社會事件的關注。許多藝術活動的目的就是提高對[自閉症](../Page/自閉症.md "wikilink")\[13\]\[14\]\[15\]、[癌症](../Page/癌症.md "wikilink")\[16\]\[17\]\[18\]、、[人口贩卖](../Page/人口贩卖.md "wikilink")\[19\]\[20\]及許多其他議題的關注。
7.  藝術在心理及治療上的目的：藝術治療師，[心理治療師](../Page/心理治療師.md "wikilink")，臨床心理學家也會用藝術作為工具，稱為藝術療法。
8.  以宣傳或商業化為其目的的藝術：藝術可以是一種宣傳的形式，漸漸的影響大家的觀念或情緒。藝術也可以作為銷售手法中的一部份，也是要影響觀念或是情緒。此時藝術的目的是要影響讀者，對特定事物或概念有特定情緒或心理上的反應\[21\]。
9.  藝術是演化適合度的指標：有人認為人類大腦的能力遠超過在遠古環境下求生所需的能力。有一種[演化心理學的論點認為人類大腦及其相關特性](../Page/演化心理學.md "wikilink")（像藝術及創造力）對人類的作用都類似公[孔雀的尾巴](../Page/孔雀.md "wikilink")。公孔雀華麗的尾巴的作用是在於吸引異性。依此理論，藝術上較好的表現可以吸引異性，在演化上很重要\[22\]。

## 艺术门类

迄今，還没有公認的藝術[分類](../Page/分類.md "wikilink")[標準](../Page/標準.md "wikilink")。各個藝術理論的派別有不同的分類方式，在時間軸上越靠近現代的藝術分類，就越顯得繁複而且具有爭議。

一般來說，根据表现手段和方式的不同，艺术可分为

  - [表演艺术](../Page/表演艺术.md "wikilink")（[音乐](../Page/音乐.md "wikilink")、[舞蹈](../Page/舞蹈.md "wikilink")、[戏剧等](../Page/戏剧.md "wikilink")）
  - [视觉艺术](../Page/视觉艺术.md "wikilink")（[绘画](../Page/绘画.md "wikilink")、[雕塑](../Page/雕塑.md "wikilink")、[建筑等](../Page/建筑.md "wikilink")）
  - 语言艺术（[文学等](../Page/文学.md "wikilink")）
  - 综合艺术（[电影](../Page/电影.md "wikilink")、[电视](../Page/电视.md "wikilink")、[歌剧](../Page/歌剧.md "wikilink")、[音乐剧等](../Page/音乐剧.md "wikilink")）

比較新的分法，則根据[时空性质将艺术分为](../Page/时空.md "wikilink")：

  - 时间艺术
  - [空间艺术](../Page/空间艺术.md "wikilink")
  - 综合艺术

下列是常見的藝術[類別](../Page/類別.md "wikilink")

  - [建築](../Page/建築.md "wikilink")
  - [文學](../Page/文學.md "wikilink")
  - [音樂](../Page/音樂.md "wikilink")
  - [舞蹈](../Page/舞蹈.md "wikilink")
  - [戲劇](../Page/戲劇.md "wikilink")
      - [音樂劇](../Page/音樂劇.md "wikilink")、[戲曲](../Page/戲曲.md "wikilink")、[喜劇](../Page/喜劇.md "wikilink")、[歌劇等](../Page/歌劇.md "wikilink")
  - [杂技](../Page/杂技.md "wikilink")
  - [魔术](../Page/魔术.md "wikilink")
  - [攝影](../Page/攝影.md "wikilink")
  - [電影](../Page/電影.md "wikilink")
      - [電視](../Page/電視.md "wikilink")
      - [廣播](../Page/廣播.md "wikilink")
  - [美術](../Page/美術.md "wikilink")
  - [繪畫](../Page/繪畫.md "wikilink")
      - [油畫](../Page/油畫.md "wikilink")
      - [中國油畫](../Page/中國油畫.md "wikilink")
      - [中國畫](../Page/中國畫.md "wikilink")（水墨）
      - 东亚[書法](../Page/書法.md "wikilink")
          - [篆刻](../Page/篆刻.md "wikilink")
          - [硬笔书法](../Page/硬笔书法.md "wikilink")
      - [粉彩](../Page/粉彩.md "wikilink")
      - [水彩](../Page/水彩.md "wikilink")
      - [素描](../Page/素描.md "wikilink")
      - [版畫](../Page/版畫.md "wikilink")
  - [雕塑](../Page/雕塑.md "wikilink")
      - [木雕](../Page/木雕.md "wikilink")
      - [泥塑](../Page/泥塑.md "wikilink")
      - 石雕
      - 金屬雕塑
      - [冰雕](../Page/冰雕.md "wikilink")
      - [沙雕](../Page/沙雕.md "wikilink")
      - [陶藝](../Page/陶藝.md "wikilink")
      - 紙雕
      - 软雕
  - 多媒体艺术
  - [裝置藝術](../Page/裝置藝術.md "wikilink")
  - [行為藝術](../Page/行為藝術.md "wikilink")
  - [應用藝術](../Page/應用藝術.md "wikilink")
  - 實驗藝術
      - [设计](../Page/设计.md "wikilink")
      - [時裝](../Page/時裝.md "wikilink")

## 藝術表現手法

[Warszawa1zz.jpg](https://zh.wikipedia.org/wiki/File:Warszawa1zz.jpg "fig:Warszawa1zz.jpg"),
华沙, 波兰\]\]

  - 理想化：凸顯人物、自然形態或景象的最佳特色，並除去其瑕疵或改換特色，以符合外界認可的完美標準。\[23\]
  - [引喻](../Page/引喻.md "wikilink")
  - [風格化](../Page/風格化.md "wikilink")
  - [抽象](../Page/抽象.md "wikilink")
  - [象征](../Page/象征.md "wikilink")
  - 意象：使用鮮活且生動的圖像式語詞來描述物件或想法。\[24\]
  - 感官感覺轉移
  - 敘事性：敘說故事的技巧。\[25\]
  - [諧擬](../Page/諧擬.md "wikilink")
  - 修飾
  - [幽默](../Page/幽默.md "wikilink")
  - [諷刺](../Page/諷刺.md "wikilink")
  - [寓言](../Page/寓言.md "wikilink")

## 藝術表達形式

  - [自然主義](../Page/自然主義.md "wikilink")
  - 學院派
  - [古典主義](../Page/古典主義.md "wikilink") /
    [新古典主義](../Page/新古典主義.md "wikilink")
  - [折衷主義](../Page/折衷主義.md "wikilink")
  - [形式主義](../Page/形式主義.md "wikilink")
  - [功能主義](../Page/功能主義.md "wikilink")
  - [歷史相對論](../Page/歷史相對論.md "wikilink")
  - [神祕主義](../Page/神祕主義.md "wikilink")
  - [寫實主義](../Page/寫實主義.md "wikilink")
  - [抽象表现主義](../Page/抽象表现主義.md "wikilink")
  - [地方主義](../Page/地方主義.md "wikilink")
  - [浪漫主義](../Page/浪漫主義.md "wikilink")
  - [同步主義](../Page/同步主義.md "wikilink")
  - [普桑主義](../Page/普桑主義.md "wikilink")
  - [魯本斯風格](../Page/彼得·保羅·魯本斯.md "wikilink")
  - [寫意](../Page/寫意.md "wikilink")

## 參考文獻

<references/>

  - 參考書目

<!-- end list -->

1.  HENDRIK WILLEM VAN LOON。《The Art of Mankind》UK：HARRAP，1953
2.  Hugh Honour。《世界藝術史》A world history of art, 5th
    ed.。吳介禎等譯。台灣：木馬文化出版，2001。ISBN
    9574695352
3.  郭珮君、蔡淳文等。《藝術概論》。台灣：新文京出版，2006 ISBN 9789861509587
4.  曾肅良。《藝術概論》台灣：三藝文化出版，2008 ISBN 9789866788451
5.  林群英。《藝術概論－藝術新理論》台灣：全華科技出版，2008 ISBN 9789572164990
6.  E.H. Gombrich藝術的故事The Story of Art。雨云譯。聯經出版社。1997。ISBN 957081697X

## 参看

  - [藝術史](../Page/藝術史.md "wikilink")

  - [藝術心理學](../Page/藝術心理學.md "wikilink")

  - [藝術形式](../Page/藝術形式.md "wikilink")

  - [藝術類型](../Page/類型_\(藝術\).md "wikilink")

  - [藝術運動](../Page/藝術運動.md "wikilink")

  - [美學](../Page/美學.md "wikilink")

  - [藝術家](../Page/藝術家.md "wikilink")

  -
  - [中国艺术院校列表](../Page/中国艺术院校列表.md "wikilink")

  - [Happening](../Page/Happening.md "wikilink")

## 外部連結

  - [香港比賽網－藝術比賽](http://www.hkcontest.com/hk2/?page_id=41)

[艺术](../Category/艺术.md "wikilink")
[Category:美学](../Category/美学.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")

1.

2.

3.  [Is advertising
    art?](http://www.independent.co.uk/arts-entertainment/is-advertising-art-1593252.html)

4.
5.

6.

7.

8.

9.

10. [五南藝術概論](http://www.wunan.com.tw/www2/download/preview/1Y37.PDF)

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21. Roland Barthes, Mythologies

22. Dutton, Denis. 2003. 'Aesthetics and Evolutionary Psychology' in
    "The Oxford Handbook for Aesthetics". Oxford University Press.

23. [藝術與建築索引典－理想化](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300311112)
    於2011年3月9日查閱

24. [藝術與建築索引典－意象](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300055825)
    於2011年3月9日查閱

25. [藝術與[建築索引典](../Page/建築.md "wikilink")－敘事性](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300055903)
    於2011年3月9日查閱