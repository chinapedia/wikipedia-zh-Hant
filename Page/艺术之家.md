[München_Haus_der_Kunst_2009.jpg](https://zh.wikipedia.org/wiki/File:München_Haus_der_Kunst_2009.jpg "fig:München_Haus_der_Kunst_2009.jpg")
[Bundesarchiv_Bild_146-1990-073-26,_München,_Haus_der_Deutschen_Kunst.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_146-1990-073-26,_München,_Haus_der_Deutschen_Kunst.jpg "fig:Bundesarchiv_Bild_146-1990-073-26,_München,_Haus_der_Deutschen_Kunst.jpg")
**艺术之家**（**Haus der
Kunst**）是德国[慕尼黑的一座美术馆](../Page/慕尼黑.md "wikilink")，位于该市最大的公园[英国公园](../Page/英国公园.md "wikilink")（Englischer
Garten）南部边缘的Prinzregentenstraße 1号。

这座建筑建造于1934年到1937年，是第三帝国的第一座纪念碑式建筑。设计者是建筑师Paul
Troost。该美术馆开放于1937年3月，当时称为德国艺术之家（Haus der
deutschen Kunst）。

[第二次世界大战结束以后](../Page/第二次世界大战.md "wikilink")，这座美术馆一度用作美国占领军军官的

从1946年开始，美术馆被分割成几个较小的空间，开始用于临时展览场地

[Category:慕尼黑文化](../Category/慕尼黑文化.md "wikilink")
[Category:慕尼黑美術館](../Category/慕尼黑美術館.md "wikilink")
[Category:纳粹建筑](../Category/纳粹建筑.md "wikilink")
[Category:1937年德國建立](../Category/1937年德國建立.md "wikilink")