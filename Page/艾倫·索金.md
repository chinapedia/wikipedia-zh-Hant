**艾倫·本杰明·索金**（，），知名美國編劇。著名作品包括《[軍官與魔鬼](../Page/軍官與魔鬼.md "wikilink")》（*A
Few Good Men*）及《[白宮夜未眠](../Page/白宮夜未眠.md "wikilink")》（*American
President*）等；電視片集方面則有《[白宮風雲](../Page/白宮風雲.md "wikilink")》（*The West
Wing*）等。2010年的《[社群網戰](../Page/社群網戰.md "wikilink")》獲得[奧斯卡最佳改編劇本獎](../Page/奧斯卡最佳改編劇本獎.md "wikilink")，2015年的《[史帝夫賈伯斯](../Page/史帝夫賈伯斯_\(電影\).md "wikilink")》获得金球奖最佳剧本奖。

## 生平

索金生于[紐約市](../Page/紐約市.md "wikilink")[曼克頓](../Page/曼克頓.md "wikilink")，[猶太人](../Page/猶太人.md "wikilink")\[1\]\[2\]\[3\]。父親為版權律師，母親是教師\[4\]。索金有一名哥哥及一名姊姊，三人均在[紐約州Scarsdale鎮長大](../Page/紐約州.md "wikilink")\[5\]，而哥哥及姊姊長大後皆成為律師\[6\]。

1983年畢業於[雪城大學](../Page/雪城大學.md "wikilink")，主修音樂舞台劇。

### 事業

2007年聖誕，由他改編的《[韋氏風雲](../Page/韋氏風雲.md "wikilink")》（Charlie Wilson's
War）在美國上映。

2007年11月，二十年以來，他的第二個舞台劇作品Farnsworth Invention將於百老匯公演。

### 私人生活

曾在機場因藏有[大麻及](../Page/大麻.md "wikilink")[可卡因被捕](../Page/可卡因.md "wikilink")。\[7\]\[8\]

## 影視作品

[Aaron_Sorkin_20_August_2008_crop2.jpg](https://zh.wikipedia.org/wiki/File:Aaron_Sorkin_20_August_2008_crop2.jpg "fig:Aaron_Sorkin_20_August_2008_crop2.jpg")

### 電影

  - 《[軍官與魔鬼](../Page/軍官與魔鬼.md "wikilink")》(*A Few Good Men*)
  - 《[體熱邊緣](../Page/體熱邊緣.md "wikilink")》(*Malice*)
  - 《[白宮夜未眠](../Page/白宮夜未眠.md "wikilink")》(*American President*)
  - 《[蓋世奇才](../Page/蓋世奇才.md "wikilink")》（*Charlie Wilson's War*）
  - 《[社群網戰](../Page/社群網戰.md "wikilink")》（*The Social Network*）
  - 《[魔球](../Page/魔球_\(電影\).md "wikilink")》（*Moneyball*）
  - 《[史帝夫賈伯斯](../Page/史帝夫賈伯斯_\(電影\).md "wikilink")》（*Steve Jobs*）
  - 《[決勝女王](../Page/決勝女王.md "wikilink")》（*Molly's Game*，兼導演）

### 電視

  - 《[體育之夜](../Page/體育之夜.md "wikilink")》（*Sports Night*）
  - 《[白宮風雲](../Page/白宮風雲.md "wikilink")》（*The West Wing*）
  - 《[-{zh-hans:日落大道60号演播室; zh-hk:60廠;
    zh-tw:日落大道60號演播室;}-](../Page/日落大道60号演播室.md "wikilink")》（*Studio
    60 on the Sunset Strip*）
  - 《[新聞急先鋒](../Page/新聞急先鋒.md "wikilink")》（*The Newsroom*）

## 參註

## 外部連結

  -
[Category:美國劇作家](../Category/美國劇作家.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:美國電視監製](../Category/美國電視監製.md "wikilink")
[Category:雪城大學校友](../Category/雪城大學校友.md "wikilink")
[Category:奧斯卡最佳改編劇本獲獎者](../Category/奧斯卡最佳改編劇本獲獎者.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:金球獎最佳劇本獲得者](../Category/金球獎最佳劇本獲得者.md "wikilink")
[Category:英国电影学院奖最佳改编剧本获得者](../Category/英国电影学院奖最佳改编剧本获得者.md "wikilink")

1.

2.

3.

4.

5.

6.
7.
8.