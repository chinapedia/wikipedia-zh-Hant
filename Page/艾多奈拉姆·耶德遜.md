**艾多奈拉姆·耶德遜**（**Adoniram
Judson**，）是一位[美國](../Page/美國.md "wikilink")[傳教士](../Page/傳教士.md "wikilink")。也是一位終身奉獻於[緬甸的](../Page/緬甸.md "wikilink")[宣教士](../Page/宣教士.md "wikilink")（1813年）。

## 生平

耶德遜出生於1788年8月9日，在[美國](../Page/美國.md "wikilink")[馬薩諸塞州](../Page/馬薩諸塞州.md "wikilink")的一位牧師家庭中。1807年畢業於普維敦斯學院（Providence
College，現今的[布朗大學](../Page/布朗大學.md "wikilink")），領受學士學位。之後的一段時間，以教書和編寫教科書為主。在1808年進入攻讀神學。\[1\]

## 前往亞洲傳教

。2月19日與結婚。夫婦同行這一段艱辛的路程。

## 進入緬甸傳教

在這遠程航行中，耶德遜在船上從事翻譯[新約聖經](../Page/新約聖經.md "wikilink")，由[希臘文翻譯為](../Page/希臘文.md "wikilink")[英文](../Page/英文.md "wikilink")。由此可以讓他更熟悉[聖經的名辭](../Page/聖經.md "wikilink")，以至於影響之後到[緬甸時翻譯](../Page/緬甸.md "wikilink")[緬甸文](../Page/緬甸語.md "wikilink")[聖經的](../Page/聖經.md "wikilink")[事工](../Page/事工.md "wikilink")。回顧當年耶德遜往[亞洲傳教的行程有多少風險](../Page/亞洲.md "wikilink")，據其他方面的情報，在[緬甸傳教前途甚不樂觀](../Page/緬甸.md "wikilink")。於是認為到[印度比較沒有危險性](../Page/印度.md "wikilink")，但情況並不是像想像中那麼妙，遭遇各種不如意的事情，甚至同伴之妻因患[肺癆去世](../Page/肺癆.md "wikilink")。最後耶德遜已窮途絕境，並無他路可走，只好趁一艘駛往[緬甸的](../Page/緬甸.md "wikilink")[葡萄牙船前往](../Page/葡萄牙.md "wikilink")[仰光](../Page/仰光.md "wikilink")。

1813年6月22日起程，這時候是風季，航行於[印度洋](../Page/印度洋.md "wikilink")[孟加拉灣](../Page/孟加拉灣.md "wikilink")，最為險惡。在行程中，耶夫人產下了一已死胎中的嬰孩，夫人亦患重病。1813年7月13日當天船直入[仰光大江](../Page/仰光大江.md "wikilink")，耶德遜終於到達[緬甸](../Page/緬甸.md "wikilink")－[仰光](../Page/仰光.md "wikilink")，開始了他的福音事工。\[2\]

## 對緬甸的貢獻

耶德遜在緬甸初期的工作，就是學習[緬甸語文](../Page/緬甸語.md "wikilink")。耶德遜後來甚至把[聖經新約](../Page/聖經新約.md "wikilink")，[舊約翻譯成](../Page/舊約.md "wikilink")[緬文](../Page/緬甸語.md "wikilink")[聖經](../Page/聖經.md "wikilink")。1835年，耶德遜的緬文聖經譯本印刷出版。\[3\]

## 緬甸的宗教

緬甸是一個虔誠的佛教國家。如今雖仍然是信奉佛教，但根據耶德遜時代相比，已有不少基督徒。當耶德遜進入緬甸時，沒有一個基督徒。1850年他離世時（62歲）緬甸已有七千多基督徒。耶德遜前在[仰光建立的第一間教會](../Page/仰光.md "wikilink")，就是現在的吳諾（緬甸第一位基督徒）紀念堂。今天的[仰光大學裡能看到一所耶德遜紀念教堂](../Page/仰光大學.md "wikilink")（Judson
Memorial Baptist Church）。

## 参考文献

<div class="references-small">

<references />

</div>

1.  潘恩，《傳教偉人耶德遜》，（香港：基督教輔僑出版社，1963），1-2。
2.  潘恩，《傳教偉人耶德遜》，（香港：基督教輔僑出版社，1963），6-10。
3.  潘恩，《傳教偉人耶德遜》，（香港：基督教輔僑出版社，1963），11-32。