**艾山·买合苏木**（[维吾尔语](../Page/维吾尔语.md "wikilink")：；），[維吾爾族](../Page/維吾爾族.md "wikilink")，原籍[中華人民共和國](../Page/中華人民共和國.md "wikilink")[新疆省](../Page/新疆省.md "wikilink")[喀什地區](../Page/喀什地區.md "wikilink")[疏勒縣](../Page/疏勒縣.md "wikilink")。由於他被指與[基地组织有聯繫](../Page/基地组织.md "wikilink")，被中国及美國官方列為[恐怖分子](../Page/恐怖分子.md "wikilink")\[1\]
。1997年和前辈亲友[阿不都卡德尔·亚甫泉发起](../Page/阿不都卡德尔·亚甫泉.md "wikilink")[东突厥斯坦伊斯兰党](../Page/東突厥斯坦伊斯蘭運動.md "wikilink")。于2003年10月2日在[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[阿富汗边境的一次反恐怖联合行动中被巴基斯坦军队击毙](../Page/阿富汗.md "wikilink")\[2\]。

## 参考文献

{{-}}

[M买](../Category/疏勒县人.md "wikilink")
[M买](../Category/维吾尔族人.md "wikilink")
[Category:东突厥斯坦伊斯兰运动成员](../Category/东突厥斯坦伊斯兰运动成员.md "wikilink")
[M买](../Category/中國－巴基斯坦關係.md "wikilink")
[M买](../Category/中华人民共和国战争身亡者.md "wikilink")
[Category:死在国外的中华人民共和国人](../Category/死在国外的中华人民共和国人.md "wikilink")

1.
2.