[Edo_de_Waart_in_2008.jpg](https://zh.wikipedia.org/wiki/File:Edo_de_Waart_in_2008.jpg "fig:Edo_de_Waart_in_2008.jpg")
**艾度·迪華特**（Edo de
Waart，）是[荷蘭近代最卓越的](../Page/荷蘭.md "wikilink")[指揮家](../Page/指揮家.md "wikilink")。他以指揮[樂團及](../Page/樂團.md "wikilink")[歌劇節目享負盛名](../Page/歌劇.md "wikilink")，並將部分二流樂團轉變為一流的樂團，因而被冠以「樂團建造者」之美譽。

## 在學時期

迪華特曾於[荷蘭](../Page/荷蘭.md "wikilink")[阿姆斯特丹市的史韋琳克音樂學院](../Page/阿姆斯特丹.md "wikilink")(Sweelinck
Conservatory)學習[雙簧管](../Page/雙簧管.md "wikilink")、[鋼琴及](../Page/鋼琴.md "wikilink")[指揮學](../Page/指揮學.md "wikilink")，直至1962年畢業；畢業後之翌年(1963年)，他更被[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")[皇家音樂廳樂團委任為助理首席雙簧管手](../Page/皇家音樂廳樂團.md "wikilink")。23歲時(1964年)，迪華特更於[紐約市的](../Page/紐約市.md "wikilink")[德米特裡·米特羅波洛斯指揮大賽中奪獎](../Page/德米特裡·米特羅波洛斯.md "wikilink")。奪獎後，他正式加盟[紐約愛樂樂團之助理指揮](../Page/紐約愛樂樂團.md "wikilink")，並在一年時間擔當[倫納德·伯恩斯坦之得力助手](../Page/倫納德·伯恩斯坦.md "wikilink")。

## 指揮時期

迪華特於返回荷蘭後，隨即被[伯納德·海廷克委任為](../Page/伯納德·海廷克.md "wikilink")[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")[皇家音樂廳樂團助理指揮](../Page/皇家音樂廳樂團.md "wikilink")。1967年，荷蘭管樂合奏組及[鹿特丹愛樂樂團委任迪華特為其指揮](../Page/鹿特丹愛樂樂團.md "wikilink")，他更於1973至1979年間擔任音樂總監。

迪華特於1975年帶領[三藩市交響樂團作首演](../Page/三藩市交響樂團.md "wikilink")，一年後擔任該樂團的總客席指揮，1977至1985年間更擔任該樂團之音樂總監。到1986至1995年間，他更出任[明尼蘇達樂團之總指揮](../Page/明尼蘇達樂團.md "wikilink")。

迪華特歷經多次巡迴演出後，於1989年返回荷蘭，並被[荷蘭電台愛樂樂團委任為音樂總監](../Page/荷蘭電台愛樂樂團.md "wikilink")，直至1994年辭職為止；其後更出任該樂團之[桂冠指揮](../Page/桂冠指揮.md "wikilink")。

## 全球指揮之路

自1995年開始，迪華特正式擔任[澳洲](../Page/澳洲.md "wikilink")[雪梨交響樂團之總指揮及藝術顧問](../Page/雪梨交響樂團.md "wikilink")。2004年起出任[香港管弦樂團之藝術總監及總指揮至](../Page/香港管弦樂團.md "wikilink")2012年\[1\]。

迪華特曾於世界多個知名樂團擔任客席指揮，如[柏林愛樂樂團](../Page/柏林愛樂樂團.md "wikilink")、[萊比錫布業大廳樂團](../Page/萊比錫布業大廳樂團.md "wikilink")、[愛樂管弦樂團](../Page/愛樂管弦樂團.md "wikilink")、[皇家愛樂樂團](../Page/皇家愛樂樂團.md "wikilink")、[瑞士諾曼第樂團](../Page/瑞士諾曼第樂團.md "wikilink")、[波士頓交響樂團](../Page/波士頓交響樂團.md "wikilink")、[克利夫蘭管弦樂團](../Page/克利夫蘭管弦樂團.md "wikilink")、[洛杉磯愛樂樂團及](../Page/洛杉磯愛樂樂團.md "wikilink")[芝加哥交響樂團](../Page/芝加哥交響樂團.md "wikilink")。迪華特更熱衷於演繹[現代音樂](../Page/現代音樂.md "wikilink")，如擔任[约翰·亚当斯歌劇](../Page/约翰·库利奇·亚当斯.md "wikilink")《[尼克遜在中國](../Page/尼克松在中国_\(歌剧\).md "wikilink")》的首演指揮；此外，他更為[斯蒂夫·莱奇](../Page/斯蒂夫·莱奇.md "wikilink")(Steve
Reich)的《管樂、弦樂及鍵盤之變奏曲(Variations for Winds, Strings and
Keyboards)》及其他作於三藩市的作品作錄音。

作為歌劇節目之經常性指揮，迪華特曾於英、美等地指揮歌劇，如在1971年於[桑特菲歌劇院作首演](../Page/桑特菲歌劇院.md "wikilink")，1975年於[休士頓大歌劇院作指揮](../Page/休士頓大歌劇院.md "wikilink")，1976年於[皇家歌劇院](../Page/皇家歌劇院.md "wikilink")（高文花園）作指揮、及於1976年於[拜羅伊特節日劇院作指揮](../Page/拜羅伊特節日劇院.md "wikilink")。1970年，迪華特更擔任[荷蘭市立歌劇院之經常性指揮](../Page/荷蘭市立歌劇院.md "wikilink")；1980年，他在[三藩市歌劇院指揮](../Page/三藩市歌劇院.md "wikilink")《[尼伯龍根的指環](../Page/尼伯龍根的指環.md "wikilink")》。

近年來，他更於巴黎[巴士底歌劇院指揮](../Page/巴士底歌劇院.md "wikilink")《[玫瑰騎士](../Page/玫瑰騎士.md "wikilink")(Der
Rosenkavalier)》、及在[大都會歌劇院指揮](../Page/大都會歌劇院.md "wikilink")《[魔笛](../Page/魔笛.md "wikilink")》及《[費加羅的婚禮](../Page/費加羅的婚禮.md "wikilink")》。此外，他更在[阿姆斯特丹市指揮](../Page/阿姆斯特丹.md "wikilink")《卡芭諾娃(Katya
Kabanova)》、《[維特](../Page/維特.md "wikilink")》、《[彼得格林](../Page/彼得格林.md "wikilink")》、《[蝴蝶夫人](../Page/蝴蝶夫人.md "wikilink")》及《[特洛伊人](../Page/特洛伊人.md "wikilink")》。其他作品更包括於[日內瓦歌劇院上演](../Page/日內瓦歌劇院.md "wikilink")《[博理斯·古杜諾夫](../Page/博理斯·古杜諾夫.md "wikilink")》及於[薩爾茨堡音樂節上演](../Page/薩爾茨堡音樂節.md "wikilink")《[費加羅的婚禮](../Page/費加羅的婚禮.md "wikilink")》。迪華特亦自1995年起，指揮[華格納作品](../Page/華格納.md "wikilink")，及在2000雪梨奧運藝術節中以演奏《[諸神的黃昏](../Page/諸神的黃昏.md "wikilink")》推向最高潮。2005年，他更領導[香港管弦樂團指揮](../Page/香港管弦樂團.md "wikilink")[理查·史特勞斯名作](../Page/理查·史特勞斯.md "wikilink")《[莎樂美](../Page/莎樂美_\(歌劇\).md "wikilink")》及《[厄勒克特拉](../Page/厄勒克特拉_\(歌剧\).md "wikilink")》，並贏得好評。

2002年3月，迪華特宣告於2004年將辭去自1999年擔任之[荷蘭歌劇院總指揮一職](../Page/荷蘭歌劇院.md "wikilink")。宣告辭官時，迪華特更提及期望抽更多時間照顧兩名子女，然而，他接受荷蘭報章《忠誠報(Trouw)》專訪時，提及他與荷蘭歌劇院導演[皮埃爾·奧迪](../Page/皮埃爾·奧迪.md "wikilink")(Pierre
Audi)排演歌劇《[羅恩格林](../Page/羅恩格林.md "wikilink")》，及與[羅柏特·韋爾松](../Page/羅柏特·韋爾松.md "wikilink")(Robert
Wilson)籌劃《[蝴蝶夫人](../Page/蝴蝶夫人.md "wikilink")》時，曾挑起不少的爭議，並錯過了解人道及方向感。

## 錄音及曾獲之榮譽

迪華特的錄音數量繁多，曾跟不少知名樂團合作灌錄專輯，包括與[荷蘭電台愛樂樂團合作灌錄全套](../Page/荷蘭電台愛樂樂團.md "wikilink")[馬勒交響曲](../Page/古斯塔夫·馬勒.md "wikilink")，及近期與該樂團所灌錄之全套[拉赫曼尼諾夫管弦樂作品及兩張](../Page/拉赫曼尼諾夫.md "wikilink")[華格納管弦樂作品專輯](../Page/華格納.md "wikilink")。

迪華特亦曾獲[荷蘭政府頒發](../Page/荷蘭政府.md "wikilink")[雄獅](../Page/雄獅.md "wikilink")[勳位](../Page/勳位.md "wikilink")，及獲[澳洲政府頒發非軍事最高榮譽](../Page/澳洲.md "wikilink")—澳洲[勳章](../Page/勳章.md "wikilink")，表揚他成功將[雪梨交響樂團提升至世界級地位](../Page/雪梨交響樂團.md "wikilink")，提升澳洲及[雪梨的音樂和藝術生活](../Page/雪梨_\(城市\).md "wikilink")。近期他又獲[香港演藝學院頒發榮譽院士](../Page/香港演藝學院.md "wikilink")，讚揚他對音樂界的貢獻，特別是他對本地下一代[音樂家的培育](../Page/音樂家.md "wikilink")。

## 註解

<references />

[Category:阿姆斯特丹人](../Category/阿姆斯特丹人.md "wikilink")
[Category:荷蘭指揮家](../Category/荷蘭指揮家.md "wikilink")

1.