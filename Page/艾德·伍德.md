**愛德華·大衛·「艾德」·伍德**（，）[美國](../Page/美國.md "wikilink")[好萊塢影片](../Page/好萊塢.md "wikilink")[導演](../Page/導演.md "wikilink")、[製片](../Page/製片.md "wikilink")、[演員](../Page/演員.md "wikilink")、[獨立電影公司經營者](../Page/獨立電影.md "wikilink")。他於1950年代－1960年代，以市場口味大量拍攝低預算的科幻恐怖片，其中結合[飛碟](../Page/飛碟.md "wikilink")、[吸血鬼](../Page/吸血鬼.md "wikilink")、[外星人劇情而成的](../Page/外星人.md "wikilink")1959年自編自導自演低成本作品《[外太空九號計劃](../Page/外太空九號計劃.md "wikilink")》，被多位影評人號稱是影史最爛的[科幻片](../Page/科幻片.md "wikilink")。

晚年因投資失敗與酗酒等原因，晚年生活並不順遂，在他有生之年，他所製造的電影也一直未受到矚目。他于1978年在观看球赛时因突发心脏病而猝死。不過，就因為他對於電影工作的熱情與特殊的[邪典電影風格](../Page/邪典電影.md "wikilink")，就在他过世之後，他的影风和作品反而受到部分後代影評人與小眾影迷的熱愛。這裡面，最著名的為電影導演[提姆·波頓](../Page/提姆·波頓.md "wikilink")；波頓不只在1994年導演一部與艾德同名的另類黑白傳記電影《[艾活傳](../Page/艾德·伍德_\(电影\).md "wikilink")》來紀念自己的崇拜偶像，1996年更使用1億美金的成本，重拍艾德B級科幻片，並將片名稱為《[星戰毀滅者](../Page/星戰毀滅者.md "wikilink")》。

生活中，艾德·伍德有[异装癖的爱好](../Page/异装癖.md "wikilink")，但他是一名[异性恋者](../Page/异性恋.md "wikilink")。

## 參考資料

  - The Haunted World of Edward D. Wood, Jr., dir. Brett Thompson, 1996

## 扩展阅读

  -
## 外部链接

  -
  -
  - [非非共享界影評](https://web.archive.org/web/20070311133425/http://www.fifid.com/subject/1355680/)

  - [好萊塢史上最爛的恐怖片：死靈之舞](https://web.archive.org/web/20090422085227/http://pulolesu.anti-kevin.org/dream/kuso/dance.html)

[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:美国海军陆战队士兵](../Category/美国海军陆战队士兵.md "wikilink")
[Category:美國第二次世界大戰軍事人物](../Category/美國第二次世界大戰軍事人物.md "wikilink")
[Category:美国剪接师](../Category/美国剪接师.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")
[Category:20世紀美國小說家](../Category/20世紀美國小說家.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美国电视剧导演](../Category/美国电视剧导演.md "wikilink")