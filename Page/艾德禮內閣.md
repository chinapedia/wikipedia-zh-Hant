## 註釋

  - [艾德禮在任](../Page/艾德禮.md "wikilink")[英國首相期間曾兩度組閣](../Page/英國首相.md "wikilink")。第一次日期為1945年7月至1950年2月。在[1950年選舉後](../Page/1950年選舉.md "wikilink")，艾德禮連任首相，並再次組閣，日期為1950年2月至1951年10月。

## 艾德禮內閣

### 第一次艾德禮內閣 （1945年7月—1950年2月）

  - 艾德禮：[英國首相及](../Page/英國首相.md "wikilink")[國防部長](../Page/國防大臣.md "wikilink")
  - [喬伊特勳爵](../Page/威廉·喬伊特.md "wikilink")（Lord
    Jowitt）：[大法官](../Page/英國大法官.md "wikilink")
  - [赫伯特·莫里森](../Page/赫伯特·莫里森.md "wikilink")（Herbert
    Morrison）：[樞密院議長及](../Page/樞密院議長.md "wikilink")[下議院領袖](../Page/下議院領袖.md "wikilink")
  - [亞瑟·格林伍德](../Page/亞瑟·格林伍德.md "wikilink")（Arthur
    Greenwood）：[掌璽大臣](../Page/掌璽大臣.md "wikilink")
  - [休·道耳吞](../Page/休·道耳吞.md "wikilink")（Hugh
    Dalton）：[財政大臣](../Page/財政大臣.md "wikilink")
  - [歐內斯特·貝文](../Page/歐內斯特·貝文.md "wikilink")：[外務大臣](../Page/外務及英聯邦事務大臣.md "wikilink")
  - [詹姆士·丘特爾·伊德](../Page/詹姆士·丘特爾·伊德.md "wikilink")（James Chuter
    Ede）：[內務大臣](../Page/內務大臣.md "wikilink")
  - [喬治·亨利·霍爾](../Page/喬治·亨利·霍爾.md "wikilink")（George Henry
    Hall）：[殖民地大臣](../Page/殖民地大臣.md "wikilink")
  - [艾迪生勳爵](../Page/克里斯多夫·艾迪生，第一代艾迪生子爵.md "wikilink")（Lord
    Addison）：[自治領事務大臣](../Page/自治領事務大臣.md "wikilink")
  - [佩西克-勞倫斯勳爵](../Page/弗雷德里克·佩西克-勞倫斯，第一代佩西克-勞倫斯男爵.md "wikilink")（Lord
    Pethick-Lawrence）：[印度及緬甸大臣](../Page/印度大臣.md "wikilink")
  - [A·V·亞歷山大](../Page/A·V·亞歷山大.md "wikilink")（A. V.
    Alexander）：[第一海軍大臣](../Page/第一海軍大臣.md "wikilink")
  - [傑克·勞森](../Page/傑克·勞森.md "wikilink")（Jack
    Lawson）：[陸軍大臣](../Page/陸軍大臣.md "wikilink")
  - [斯坦斯蓋特勳爵](../Page/威廉·威基伍德·貝恩，第一代斯坦斯蓋特子爵.md "wikilink")（Lord
    Stansgate）：[空軍大臣](../Page/空軍大臣.md "wikilink")
  - [埃倫·威爾金森](../Page/埃倫·威爾金森.md "wikilink")（Ellen
    Wilkinson）：[教育部長](../Page/教育及技術大臣.md "wikilink")
  - [若瑟夫·韋斯特伍德](../Page/若瑟夫·韋斯特伍德.md "wikilink")（Joseph
    Westwood）：[蘇格蘭大臣](../Page/蘇格蘭大臣.md "wikilink")
  - [湯姆·威廉士](../Page/湯姆·威廉士_\(政治家\).md "wikilink")（Tom
    Williams）：[農業及漁業部長](../Page/農業及漁業部長.md "wikilink")
  - [喬治·艾薩克](../Page/喬治·艾薩克.md "wikilink")（George
    Isaacs）：[勞工及國民服務部長](../Page/就業大臣.md "wikilink")
  - [安奈林·貝文](../Page/安奈林·貝文.md "wikilink")（Aneurin
    Bevan）：[衛生部長](../Page/衛生大臣.md "wikilink")
  - [斯塔福·克里普斯爵士](../Page/斯塔福·克里普斯.md "wikilink")（Sir Stafford
    Cripps）：[貿易委員會主席](../Page/貿易委員會主席.md "wikilink")
  - [伊曼紐·欣韋爾](../Page/伊曼紐·欣韋爾.md "wikilink")（Emanuel
    Shinwell）：[能源及動力部長](../Page/能源及動力部長.md "wikilink")

#### 變動

  - 1946年7月：亞瑟·格林伍德兼任[財政部主計長](../Page/財政部主計長.md "wikilink")，仍留任掌璽大臣。
  - 1946年10月：陸軍大臣、空軍大臣和第一海軍大臣不再在[內閣供職](../Page/英國內閣.md "wikilink")。但A·V·亞歷山大以[不管部大臣的身份留在內閣](../Page/不管部大臣.md "wikilink")，喬治·亨利·霍爾接替亞歷山大出任第一海軍大臣，但卻因此不在內閣供職。至於霍爾原任的殖民地大臣一職則由[亞瑟·克里奇·瓊斯](../Page/亞瑟·克里奇·瓊斯.md "wikilink")（Arthur
    Creech Jones）接任。
  - 1946年12月：A·V·亞歷山大接替艾德禮為國防部長。
  - 1947年2月：埃倫·威爾金森在教育部長任內去世，其職由[喬治·陶姆林遜](../Page/喬治·陶姆林遜.md "wikilink")（George
    Tomlinson）接替。
  - 1947年3月：亞瑟·格林伍德不再任財政部主計長，但仍留任掌璽大臣，至於繼任主計長的人士不在內閣供職。
  - 1947年7月：亞瑟·格林伍德改任不管部大臣，掌璽大臣由[英曼勳爵](../Page/菲利普·英曼，第一代英曼男爵.md "wikilink")（Lord
    Inman）接任。另外，[利斯托韋爾勳爵](../Page/威廉·黑爾，第五代利斯托韋爾伯爵.md "wikilink")（Lord
    Listowel）取代佩西克-勞倫斯勳爵為印度及緬甸大臣。
  - 1947年7月：[自治領事務辦公室改組為](../Page/自治領事務辦公室.md "wikilink")[英聯邦關係辦公室](../Page/英聯邦關係辦公室.md "wikilink")，並設立[英聯邦關係大臣一職](../Page/英聯邦關係大臣.md "wikilink")，由原任自治領事務大臣的艾迪生勳爵出任。
  - 1947年8月：由於[印度](../Page/印度.md "wikilink")[獨立](../Page/獨立.md "wikilink")，[印度及緬甸辦公室遂改組為](../Page/印度及緬甸辦公室.md "wikilink")[緬甸辦公室](../Page/緬甸辦公室.md "wikilink")。利斯托韋爾勳爵成為[緬甸大臣](../Page/緬甸大臣.md "wikilink")。
  - 1947年9月：斯塔福·克里普斯[爵士改任](../Page/爵士.md "wikilink")[經濟事務部長](../Page/經濟事務大臣.md "wikilink")，[哈羅德·威爾遜則接替他成為貿易委員會主席](../Page/哈羅德·威爾遜.md "wikilink")。亞瑟·格林伍德從內閣退休，但仍任[下議院議員](../Page/英國下議院.md "wikilink")。
  - 1947年10月：艾迪生勳爵取代英曼勳爵為掌璽大臣，[菲利普·諾埃爾-貝克](../Page/菲利普·諾埃爾-貝克.md "wikilink")（Philip
    Noel-Baker）則接替艾迪生勳爵為英聯邦關係大臣。此外，[亞瑟·伍德伯恩](../Page/亞瑟·伍德伯恩.md "wikilink")（Arthur
    Woodburn）取代若瑟夫·韋斯特伍德為蘇格蘭大臣。能源及動力部長伊曼紐·欣韋爾則退出內閣。
  - 1947年11月：斯塔福·克里普斯爵士接替休·道耳吞為財政大臣。
  - 1948年1月：[緬甸](../Page/緬甸.md "wikilink")[獨立](../Page/獨立.md "wikilink")，[緬甸辦公室及](../Page/緬甸辦公室.md "wikilink")[緬甸大臣一併廢置](../Page/緬甸大臣.md "wikilink")。
  - 1948年5月：休·道耳吞以[蘭卡斯特公爵領地總裁的身份重入內閣](../Page/蘭卡斯特公爵領地總裁.md "wikilink")。此外，[派肯漢勳爵](../Page/法蘭·派肯漢，第七代朗福德伯爵.md "wikilink")（Lord
    Pakenham）以[民航部長的身份進入內閣](../Page/民航部長.md "wikilink")。
  - 1948年7月：艾迪生勳爵出掌財政部主計長。
  - 1948年4月：艾迪生勳爵不再任財政部主計長，但仍留任掌璽大臣。繼任主計長的人士不在內閣供職。

### 第二次艾德禮內閣 （1950年2月—1951年10月）

  - 艾德禮：英國首相
  - [喬伊特勳爵](../Page/威廉·喬伊特.md "wikilink")（Lord
    Jowitt）：[大法官](../Page/英國大法官.md "wikilink")
  - [赫伯特·莫里森](../Page/赫伯特·莫里森.md "wikilink")（Herbert
    Morrison）：[樞密院議長及](../Page/樞密院議長.md "wikilink")[下議院領袖](../Page/下議院領袖.md "wikilink")
  - [艾迪生勳爵](../Page/克里斯多夫·艾迪生，第一代艾迪生子爵.md "wikilink")（Lord
    Addison）：[掌璽大臣](../Page/掌璽大臣.md "wikilink")
  - [斯塔福·克里普斯爵士](../Page/斯塔福·克里普斯.md "wikilink")（Sir Stafford
    Cripps）：[財政大臣](../Page/財政大臣.md "wikilink")
  - [歐內斯特·貝文](../Page/歐內斯特·貝文.md "wikilink")：[外務大臣](../Page/外務及英聯邦事務大臣.md "wikilink")
  - [詹姆士·丘特爾·伊德](../Page/詹姆士·丘特爾·伊德.md "wikilink")（James Chuter
    Ede）：[內務大臣](../Page/內務大臣.md "wikilink")
  - [吉姆·格-{里}-菲思](../Page/吉姆·格里菲思.md "wikilink")（Jim
    Griffiths）：[殖民地大臣](../Page/殖民地大臣.md "wikilink")
  - [派翠克·戈登·沃克](../Page/派翠克·戈登·沃克.md "wikilink")（Patrick Gordon
    Walker）：[英聯邦關係大臣](../Page/英聯邦關係大臣.md "wikilink")
  - [哈羅德·威爾遜](../Page/哈羅德·威爾遜.md "wikilink")：[貿易委員會主席](../Page/貿易委員會主席.md "wikilink")
  - [亞歷山大勳爵](../Page/A·V·亞歷山大.md "wikilink")（Lord
    Alexander）：[蘭卡斯特公爵領地總裁](../Page/蘭卡斯特公爵領地總裁.md "wikilink")
  - [喬治·陶姆林遜](../Page/喬治·陶姆林遜.md "wikilink")（George
    Tomlinson）：[教育部長](../Page/教育及技術大臣.md "wikilink")
  - [赫克托·麥克尼爾](../Page/赫克托·麥克尼爾.md "wikilink")（Hector
    McNeil）：[蘇格蘭大臣](../Page/蘇格蘭大臣.md "wikilink")
  - [湯姆·威廉士](../Page/湯姆·威廉士_\(政治家\).md "wikilink")（Tom
    Williams）：[農業及漁業部長](../Page/農業及漁業部長.md "wikilink")
  - [喬治·艾薩克](../Page/喬治·艾薩克.md "wikilink")（George Isaacs）：勞工及國民服務部長
  - [安奈林·貝文](../Page/安奈林·貝文.md "wikilink")（Aneurin
    Bevan）：[衛生部長](../Page/衛生大臣.md "wikilink")
  - [伊曼紐·欣韋爾](../Page/伊曼紐·欣韋爾.md "wikilink")（Emanuel Shinwell）：國防部長
  - [休·道耳吞](../Page/休·道耳吞.md "wikilink")（Hugh Dalton）：都市及郊區發展部長

#### 變動

  - 1950年10月：[曉治·蓋茨克](../Page/曉治·蓋茨克.md "wikilink")（Hugh
    Gaitskell）取代斯塔福·克里普斯爵士為財政大臣。
  - 1951年1月：安奈林·貝文接替喬治·艾薩克為勞工及國民服務大臣。至於接替貝文擔任衛生部長的人士則不在內閣之列。休·道耳吞之職改稱地方政府及規劃部長。
  - 1951年3月：赫伯特·莫里森接替歐內斯特·貝文為外務大臣，艾迪生勳爵則接替莫里森為樞密院議長，而歐內斯特·貝文取代艾迪生勳爵為掌璽大臣。另外，詹姆士·丘特爾·伊德接任莫里森的下議院領袖一職，並同時兼任原本的內務大臣一職。
  - 1951年4月：[理查·斯多克斯](../Page/理查·斯多克斯.md "wikilink")（Richard
    Stokes）接替歐內斯特·貝文為掌璽大臣。[阿爾夫·羅本斯](../Page/阿爾夫·羅本斯.md "wikilink")（Alf
    Robens）則取代辭職的安奈林·貝文，出任勞工及國民服務部長。[哈特利·莎克羅斯爵士](../Page/哈特利·莎克羅斯.md "wikilink")（Sir
    Hartley Shawcross）則接替辭職的哈羅德·威爾遜為貿易委員會主席。

[A](../Category/英國內閣.md "wikilink")