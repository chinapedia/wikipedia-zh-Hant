**艾思奇**（\[1\]），原名**李生萱**，马克思主义哲学家。[云南](../Page/云南省.md "wikilink")[腾冲](../Page/腾冲县.md "wikilink")\[2\]
人，[辛亥革命及](../Page/辛亥革命.md "wikilink")[护国战争元老](../Page/护国战争.md "wikilink")[李曰垓之子](../Page/李曰垓.md "wikilink")，[蒙古族后裔](../Page/蒙古族.md "wikilink")\[3\]。1910年出生于云南腾冲[和顺李家大院](../Page/和順鎮_\(騰衝縣\).md "wikilink")，“艾思奇”的名字是从英文SH
（其英文转写Sheng Hsuen）得到灵感，并成为自己的[笔名](../Page/笔名.md "wikilink")。

## 生平

艾思奇幼年离开腾冲随父母定居[昆明](../Page/昆明.md "wikilink")，早年留学日本，[九一八事变后回国](../Page/九一八事变.md "wikilink")。1935年加入[中国共产党](../Page/中国共产党.md "wikilink")，1937年赴[延安](../Page/延安.md "wikilink")，先后在[抗日军政大学](../Page/抗日军政大学.md "wikilink")、[陕北公学](../Page/陕北公学.md "wikilink")、[延安马列学院等机关单位任教](../Page/延安马列学院.md "wikilink")，1959年历任[中央高级党校副校长](../Page/中央高级党校.md "wikilink")、哲学研究室主任，[中国哲学学会副会长](../Page/中国哲学学会.md "wikilink")，[中国科学院哲学社会科学部委员](../Page/中国科学院.md "wikilink")，长期从事[马列主义哲学的宣传教育工作](../Page/马列主义.md "wikilink")。1966年3月在北京逝世。

## 艾思奇与“毛泽东思想”

艾思奇对[毛泽东自身哲学的形成起到了极为关键的作用](../Page/毛澤東思想.md "wikilink")。海外学者通过文献比对等研究，认为《[矛盾论](../Page/矛盾论.md "wikilink")》、《[实践论](../Page/实践论.md "wikilink")》等著作，和艾思奇的同期著作有“难以计数的相似之处\[4\]。”
比如，《矛盾论》的第四部分题为“主要的矛盾和主要的矛盾方面”，和艾思奇的数篇作品几乎雷同\[5\]。艾被认为是毛泽东的御用哲学家。

## 著作

《[大众哲学](../Page/大众哲学.md "wikilink")》、《[辨证唯物主义纲要](../Page/辨证唯物主义纲要.md "wikilink")》、《[哲学与生活](../Page/哲学与生活.md "wikilink")》等。

## 评价

艾思奇是中国著名[哲学家](../Page/哲学家.md "wikilink")。毛泽东对他所做的贡献给予了极高的评价，称他是学者、战士、真诚的人。

## 家庭

1944年7月与[王丹一结婚](../Page/王丹一.md "wikilink")。艾思奇去世后，王丹一一直帮忙从事“艾思奇思想”的研究，五十年内相继主持出版了《艾思奇文集》两卷本、560万字的《艾思奇全书》八卷本等一系列著作。2016年7月19日王丹一去世，遗体告别仪式给予很高规格：习近平、李克强、张德江、俞正声、刘云山、王岐山、张高丽七常委，刘延东、刘奇葆等中央政治局委员，李鹏、朱镕基、温家宝三任前总理，还有宋平、李岚清、曾庆红、吴官正等退休常委，均赠送了花圈\[6\]。

## 故居

[艾思奇故居位于云南省腾冲县和顺乡](../Page/艾思奇故居.md "wikilink")，为[爱国主义教育基地](../Page/爱国主义教育基地.md "wikilink")。

## 参考资料

[Category:腾冲人](../Category/腾冲人.md "wikilink")
[Category:中国蒙古族人物](../Category/中国蒙古族人物.md "wikilink")
[Category:中華人民共和國哲學家](../Category/中華人民共和國哲學家.md "wikilink")
[Category:第一届全国人大代表](../Category/第一届全国人大代表.md "wikilink")
[Category:第二届全国人大代表](../Category/第二届全国人大代表.md "wikilink")
[Category:第三届全国人大代表](../Category/第三届全国人大代表.md "wikilink")
[Category:抗日军政大学教师](../Category/抗日军政大学教师.md "wikilink")
[Category:葬于保山](../Category/葬于保山.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")

1.  辞海编辑委员会：《[辞海](../Page/辞海.md "wikilink")》1999年版缩印本，上海：上海辞书出版社，2000年，页676。

2.
3.  党史文汇2006年05期《[王丹一忆谈艾思奇](../Page/王丹一.md "wikilink")》，【注：王丹一为艾思奇夫人】

4.  Joshua A. Fogel, "Ai Siqi, Establishment Intellectual by Joshua A.
    Fogel", in Merle Goldman, Timothy Cheek, and Carol Lee Hamrin, eds.,
    China's Intellectuals and the State: In Search of a New Relationship
    (Harvard University Asia Center, 1987), 第29页.

5.  对比毛泽东著，《[毛泽东选集](../Page/毛泽东选集.md "wikilink")》第一卷（人民出版社1962年版），第331-337页和艾思奇著，《研究提纲》，见《艾思奇哲学选集》第502-505页；关于二人的文本分析，见竹内著《Mo
    Takuto》第67页。

6.  [1](http://news.ifeng.com/a/20160802/49706369_0.shtml)，凤凰新闻。