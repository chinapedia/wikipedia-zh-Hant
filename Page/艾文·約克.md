**艾文·科倫·約克**（**Alvin Cullum
York**，），[美國](../Page/美國.md "wikilink")[軍官及戰爭英雄](../Page/軍官.md "wikilink")，官至[美國陸軍](../Page/美國陸軍.md "wikilink")[少校及田納西州](../Page/少校.md "wikilink")[國民警衛隊](../Page/國民警衛隊.md "wikilink")[上校](../Page/上校.md "wikilink")；一戰時任[下士的約克](../Page/下士.md "wikilink")，在所屬排部蒙受過半傷亡後，毅然擔任指揮職，率殘部7人攻下[德軍](../Page/德軍.md "wikilink")[機槍據點](../Page/機槍.md "wikilink")，共摧毀32挺機槍，擊斃敵軍28人，擄獲戰俘132人（包括軍官4人）。獲得獎章無數，升任[中士](../Page/中士.md "wikilink")。

其事蹟於1941年改拍為電影《約克軍曹》並廣為流傳。[二次大戰時](../Page/二次大戰.md "wikilink")，以少校新兵訓練官的身份，為[美軍](../Page/美軍.md "wikilink")[募兵](../Page/募兵.md "wikilink")。

## 個人生平

### 早年生涯

艾文出生於1887年12月13日，出生地點為[美國](../Page/美國.md "wikilink")[田納西州與](../Page/田納西州.md "wikilink")[肯塔基州州界的芬垂絲郡野狼谷帕墨村](../Page/肯塔基州.md "wikilink")（）裡一個貧瘠到不行的農庄；艾文排行老三，下面還有八個弟妹。由於困苦所帶來的限制與苦悶，直到一戰開打以前，艾文只是個把[酒當水喝把鬥毆當飯吃的鄉下小伙子](../Page/酒.md "wikilink")；艾文的父母對於聽聞附近有打架的事件發生，能夠感到意外的只有某一次艾文沒有參加而已。艾文的母親是個[基督教](../Page/基督教.md "wikilink")[和平主義教派](../Page/和平.md "wikilink")（pacifist
Protestant
denomination）虔誠信徒，總是苦口婆心但是徒勞無益地勸阻這個當地打架能手少出門，顯然艾文將母親的勸阻當作開打前的祝福。但是一次艾文最好的朋友兼酒友與助拳者跟在酒吧鬥毆中被對方打到躺在棺材裡；處於震驚難以平復的艾文在好友去世以後在母親引導下成為虔誠的基督徒，打架與酗酒就就此遠離艾文。

1917年6月5日，艾文已經29歲了，這時候才收到徵兵通知登記單（register for the
draft），一直到1919年5月29日艾文從戰場上全身而退為止，田納西州這個[窮鄉僻壤以及全美國並不知道有一位連在戰鬥中都不忘寫日記的英雄即將誕生](../Page/窮鄉僻壤.md "wikilink")。

一直以來有爭議的是到底艾文是不是個所謂「[良心拒服兵役者](../Page/基於道德或宗教信仰原因不肯服兵役者.md "wikilink")」，大部分民眾認定艾文是個和平教派的信徒沒錯，但是有沒有以上述的原因與理由成為正式登記在案的反戰人士則有待了解。因為根據文件指出艾文確實曾經以宗教信仰為由申請免役但是當局於所請不准，是否因為艾文後來撤回申請案還是因為信仰的時間不足（時機點又剛好是[一戰開打的敏感時候](../Page/一戰.md "wikilink")）則有待慢慢釐清。

### 加入軍隊

[Alvin_York_-_Project_Gutenberg_eText_19117.jpg](https://zh.wikipedia.org/wiki/File:Alvin_York_-_Project_Gutenberg_eText_19117.jpg "fig:Alvin_York_-_Project_Gutenberg_eText_19117.jpg")
艾文被[美國陸軍徵招](../Page/美國陸軍.md "wikilink")，並且被派到[喬治亞州葛登營](../Page/喬治亞州.md "wikilink")（）的[第82步兵師](../Page/第82空降師.md "wikilink")，第328步兵團，G連。

艾文在部隊移到喬治亞州奧古斯塔的時候跟著丹佛斯連長（Captain Edward Courtney Bullock
Danforth；1894–1974）一直聊關於《[聖經](../Page/聖經.md "wikilink")》的立場；等部隊到了[羅德島準備要上船的時候又開始跟巴克斯頓營長](../Page/羅德島.md "wikilink")（Major
Gonzalo Edward
Buxton；1880–1949）暢談「天命與上帝」（Providence）；兩位長官很明智地沒有把艾文以「違反宗教理念拒絕服役」的原因丟回軍人監獄處理，反而是說服了這位打架修到博士級地位的二兵，「孩子，戰爭中殺戮難免，但是情有可原；我們所從事的聖戰將要把歐洲的人民從邪惡的德意志帝國中解放出來」。

艾文的名聲是在1918年10月8日靖綏德軍德高維輕鐵路（）任務的時候打響的，他也因此獲頒[榮譽勳章乙座](../Page/榮譽勳章_\(美國\).md "wikilink")。艾文敘述他的戰鬥過程：

『德國佬可逮到咱們了；時機還真準，他們就把咱們堵死在軌道上，他們的機關槍就在高地上頭藏的好好地俯瞰著咱們，咱也說不上來這幾梭子火力是打那兒來的……我跟你說他們打的可準了，咱們的弟兄就跟家裡頭除草機前頭的長草一樣倒了下去。咱的攻勢也沒輒，咱們在那兒只能趴著，離河谷還有一半的路，德國人漫天的槍林彈雨可給咱顏色瞧了。』

德國人的火力的確給328團的傷亡率狠狠地增添了一筆；一共17個弟兄包括艾文在伯納德·厄利[班長](../Page/班長.md "wikilink")（Sergeant
Bernard
Early）的指揮下成功的滲透德軍防線然後不是把這些機槍陣地給拔了就是給端了。這一小股人馬越打越順手還把一個德軍指揮所也順便蹂躪了一番，還把原本預備逆襲的一大群德軍生擒俘擄。

厄利的人馬還在對付這一大票戰俘的同時突然又一陣漫天彈雨像灑胡椒粉似地鋪蓋過來；六個弟兄當場倒下去還有三個重傷，而這一陣彈雨是從山脊上的機槍陣地灑出來的。十七去九剩八，包括厄利也不幸陣亡，艾文就以小小的下士伍長擔起重責大任起來，他叫倖存的弟兄掩護好自己，順便把戰俘也看管好之後，艾文就用自己的方式滅掉了這個陣地。

艾文回憶道：

『這些個機關槍吐出火舌來照著我打，連我身旁的樹叢都被槍子兒像鋸子一樣給修剪的不成形狀；德國佬彼此大呼命令小叫口號，你一輩子都沒聽過人可以喧嘩成那副德性。我也沒時間去找顆樹躲或者是找個樹叢鑽……機關槍愈是提早對我開火，我就開始對他們還擊；我槍還打的挺準的，有三十個以上的德國佬不斷地對我攻擊，我儘快地看見一個就一槍放倒一個；我看他們鬼吼鬼叫我也一直大喊叫他們投降。我還真不得不殺人，但是不是他們死就是我亡，所以我就把我看家的本領一傢伙全使出來。』

艾文一夥人押解的戰俘中，德軍[中尉保羅](../Page/中尉.md "wikilink")·余根·佛爾墨（German First
Lieutenant Paul Jürgen
Vollmer）本來想趁艾文打的你死我活的機會趁機用手槍打死他，結果艾文毫毛未傷，佛爾墨中尉權衡情勢，既然攻擊的機會都沒了，就只好帶著整個單位對艾文投降，讓艾文高興了好一陣子。

艾文之後帶著剩餘的弟兄押著132名德軍戰俘回到美軍的陣地。他英勇的戰鬥除了消滅德軍機槍陣地以外也讓328團重新攻佔德軍輕鐵路。

### 獲頒勳章

原本頒給艾文的[傑出服役十字勳章是要表揚他的英勇事蹟](../Page/傑出服役十字勳章.md "wikilink")，不過高層權衡戰功之後，又提升到頒發[榮譽勳章給艾文](../Page/榮譽勳章_\(美國\).md "wikilink")，並且由總司令[約翰
J.
潘興將軍擔任頒獎人](../Page/潘興.md "wikilink")；[法國頒贈](../Page/法國.md "wikilink")[英勇十字勳章給艾文](../Page/英勇十字勳章.md "wikilink")；[義大利與](../Page/義大利.md "wikilink")[蒙特內哥羅分別頒贈艾文英勇十字勳章與戰爭勳章各乙座](../Page/蒙特內哥羅共和國.md "wikilink")。

### 官階

當艾文將德軍陣地消滅掉的時候還只是下士伍長，任務結束後就地升任中士班長以玆獎勵；至於後來艾文在1919年解釋給他的師長鄧肯將軍關於他英勇交戰的時候，他是這麼說的：

『一股比人還要崇高的力量指引我看顧我並且教我怎麼做。』

後來大家俗稱的約克軍曹也被獲選為無名陣亡將士公墓（）迎靈安葬儀式的護柩人；同樣獲此榮譽的還有第77步兵師308團團長魏托希少校（Maj. ）。

## 榮譽勳章褒揚令內文

  - 官階既所屬單位：美國陸軍第82師328團G連下士。
  - 事蹟發生地點既時間：[法國](../Page/法國.md "wikilink")[阿登省夏帖兒](../Page/阿登省.md "wikilink")-榭艾利（Chatel-Chehery）村；1918年10月8日。
  - 入伍地點：田納西州帕墨村
  - 出生日既出生地：1887年12月13日，田納西州芬垂斯郡。
  - G.O. No.: 59, W.D.（War Department）,
    1919.（美國政府戰爭部第59號令，頒於1919年，華盛頓特區。）

**褒揚令**：

當他（艾文·約克）所屬排蒙受重大傷亡並且有三位士官已列入傷亡之後，約克下士承擔起指揮職務。約克下士無畏地帶領著七名弟兄，他以無比的勇氣衝向一座不斷地對他所屬排吐出死亡火舌的機槍巢。在這英勇的武勳中，敵機槍陣地為其攻克，另有總計四名敵軍官與一百廿八人既敵武器遭擄獲。

## 家庭

1919年6月7日，艾文迎娶了葛蕾希·威廉斯（Gracie
Williams）小姐，婚後生下了七個孩子；七個孩子都被他們的父母以美國歷史人物命名。艾文後來成立了自己的農學院（），因為他覺得自己年輕的歲月都在荒唐中度過，因此利用自己的名氣募款，先募得美金一萬元，之後州議會與地方（包括以典當的方式）又募集到共十萬美元，讓九年級到十二年級的偏遠地區少年能夠受到中級教育（目前該校由於[大蕭條的關係於](../Page/大蕭條.md "wikilink")1937年轉由州政府管理，因此變成公立普通高中）。

艾文另外還成立一間神學院（Bible
School），還有一間開在老家附近的[磨坊也是艾文的](../Page/磨坊.md "wikilink")。

[二戰爆發後艾文又被徵招回国成為現役軍人](../Page/二戰.md "wikilink")，不過艾文已經53歲了，不要說有[廉頗](../Page/廉頗.md "wikilink")[馬援之志](../Page/馬援.md "wikilink")，歲月可不饒人，所以艾文就只好在後方以少校的官階到處徵招役男以及推銷政府公債，同時兼任全國陸軍新兵訓練督導官的職務。

艾文最後因為[腦溢血於](../Page/腦溢血.md "wikilink")1964年9月2日在[納什維爾市榮民醫院病逝](../Page/納什維爾_\(田納西州\).md "wikilink")，享年76歲。艾文身後下葬於老家帕墨村的野狼河墓園。

## 畢生的勳章

艾文的軍旅生涯中一共獲頒下列勳章：

  - [Ribbon-MOH.jpg](https://zh.wikipedia.org/wiki/File:Ribbon-MOH.jpg "fig:Ribbon-MOH.jpg")
    [榮譽勳章](../Page/榮譽勳章.md "wikilink")。
  - [Distinguished_Service_Cross_ribbon.svg](https://zh.wikipedia.org/wiki/File:Distinguished_Service_Cross_ribbon.svg "fig:Distinguished_Service_Cross_ribbon.svg")
    [傑出服役十字勳章](../Page/傑出服役十字勳章.md "wikilink")
  - [World_War_I_Victory_Medal_ribbon.svg](https://zh.wikipedia.org/wiki/File:World_War_I_Victory_Medal_ribbon.svg "fig:World_War_I_Victory_Medal_ribbon.svg")

  - [American_Campaign_Medal_ribbon.svg](https://zh.wikipedia.org/wiki/File:American_Campaign_Medal_ribbon.svg "fig:American_Campaign_Medal_ribbon.svg")

  - [World_War_II_Victory_Medal_ribbon.svg](https://zh.wikipedia.org/wiki/File:World_War_II_Victory_Medal_ribbon.svg "fig:World_War_II_Victory_Medal_ribbon.svg")
    [第二次世界大戰勝利獎章](../Page/第二次世界大戰勝利獎章.md "wikilink")
  - [Legion_Honneur_Chevalier_ribbon.svg](https://zh.wikipedia.org/wiki/File:Legion_Honneur_Chevalier_ribbon.svg "fig:Legion_Honneur_Chevalier_ribbon.svg")
    [法國榮譽軍團勳章](../Page/法國榮譽軍團勳章.md "wikilink")
  - [Ruban_de_la_Croix_de_guerre_1914-1918.png](https://zh.wikipedia.org/wiki/File:Ruban_de_la_Croix_de_guerre_1914-1918.png "fig:Ruban_de_la_Croix_de_guerre_1914-1918.png")
    [法國英勇十字勳章加棕櫚葉](../Page/英勇十字勳章.md "wikilink")。
  - [Croce_di_guerra_al_merito_BAR.svg](https://zh.wikipedia.org/wiki/File:Croce_di_guerra_al_merito_BAR.svg "fig:Croce_di_guerra_al_merito_BAR.svg")
    。
  - [ME_Order_of_Danilo_I_Member_BAR.svg](https://zh.wikipedia.org/wiki/File:ME_Order_of_Danilo_I_Member_BAR.svg "fig:ME_Order_of_Danilo_I_Member_BAR.svg")
    。

## 參考連結

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  - [Sgt. Alvin C. York State Historic Park web
    site](http://tnstateparks.com/parks/about/sgt-alvin-c-york)

[Category:美國陸軍少校](../Category/美國陸軍少校.md "wikilink")
[Category:美國第一次世界大戰人物](../Category/美國第一次世界大戰人物.md "wikilink")
[Category:美國陸軍榮譽勳章獲得者](../Category/美國陸軍榮譽勳章獲得者.md "wikilink")
[Category:法國榮譽軍團勳章持有人](../Category/法國榮譽軍團勳章持有人.md "wikilink")
[Category:軍隊狙擊手‎](../Category/軍隊狙擊手‎.md "wikilink")
[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")
[Category:阿巴拉契亚地区人物](../Category/阿巴拉契亚地区人物.md "wikilink")
[Category:田納西州人](../Category/田納西州人.md "wikilink")
[Category:罹患腦溢血逝世者](../Category/罹患腦溢血逝世者.md "wikilink")