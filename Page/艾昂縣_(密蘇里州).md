**艾昂縣**（**Iron County,
Missouri**）是位於[美國](../Page/美國.md "wikilink")[密蘇里州東南部的一個縣](../Page/密蘇里州.md "wikilink")。面積1,430平方公里，根據[美國2000年人口普查數字](../Page/美國2000年人口普查.md "wikilink")，共有人口10,697。縣治[艾昂頓](../Page/艾昂顿_\(密苏里州\).md "wikilink")
(Ironton)。

成立於1857年2月17日。\[1\]縣名來自當地豐富的[鐵礦](../Page/鐵.md "wikilink")。

## 參考文獻

[I](../Category/密苏里州行政区划.md "wikilink")

1.  <http://www.rootsweb.com/%7Emissour/origins.htm>