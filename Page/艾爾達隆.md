在英國作家[約翰·羅納德·魯埃爾·托爾金](../Page/約翰·羅納德·魯埃爾·托爾金.md "wikilink")（J.R.R.
Tolkien）的奇幻小說世界[中土大陸中](../Page/中土大陸.md "wikilink")，**艾爾達隆**（**Eldalondë**）是位於[努曼諾爾](../Page/努曼諾爾.md "wikilink")（Númenor）西部的城市。

艾爾達隆是努曼諾爾的港口，[伊瑞西亞島](../Page/伊瑞西亞島.md "wikilink")（Tol
Eressëa）的精靈在[努曼諾爾人](../Page/努曼諾爾人.md "wikilink")（Númenóreans）墮落前經此到此拜訪他們。

## 地理

艾爾達隆位於努曼諾爾的西海岸，位於[尼西馬鐸](../Page/尼西馬鐸.md "wikilink")（Nísimaldar）的轄境裡。它座落於[納都爾河](../Page/納都爾河.md "wikilink")（Nunduinë）的河口地區，濱臨[艾爾達那海灣](../Page/艾爾達那.md "wikilink")（Bay
of Eldanna）。

## 名字

**艾爾達隆**（Eldalondë）一名是[昆雅語](../Page/昆雅語.md "wikilink")，意思是「[艾爾達的港口](../Page/艾爾達.md "wikilink")」*Haven
of the Eldar*。\[1\]此地亦被稱為「翠綠的」*the Green*，\[2\]緣於此地的美麗。

[Nisimaldar.jpg](https://zh.wikipedia.org/wiki/File:Nisimaldar.jpg "fig:Nisimaldar.jpg")與艾爾達隆的描繪，由Matěj
Čadil 所畫。\]\]

## 總覽

艾爾達隆是努曼諾爾最美麗的港口，\[3\]背靠的尼西馬鐸地區有許多芳香常綠的樹木生長，\[4\]西海岸還有許多由[艾爾達精靈](../Page/艾爾達.md "wikilink")（Eladr）從西方帶來的植物成長，連艾爾達也稱此地美麗如同伊瑞西亞的精靈港口。\[5\]

在努曼諾爾人墮落前，艾爾達隆是從西邊來的精靈最常去的港口。\[6\]但在登丹人疏遠精靈並反叛維拉後，精靈們來的次數大減，行蹤也變得隱密，艾爾達隆可能因而衰落。緣於此地接近精靈，又是精靈常來的地方，這裡可能有許多[忠實者](../Page/精靈之友.md "wikilink")（the
Faithful）居住。

## 歷史

在努曼諾爾帝國建立的早期，艾爾達精靈經常駕駛白船來到艾爾達隆，\[7\]與努曼諾爾人結交，並送贈禮物給他們。

努曼諾爾人墮落後，精靈們減少前來艾爾達隆。在[亞爾-金密索爾](../Page/亞爾-金密索爾.md "wikilink")（Ar-Gimilzôr）統治時，他將所有查出的忠實者由西海岸強遷到東邊的港口[羅曼納](../Page/羅曼納.md "wikilink")（Rómenna），可能有許多艾爾達隆的居民被逼遷徒。末任皇帝[亞爾-法拉松](../Page/亞爾-法拉松.md "wikilink")（Ar-Pharazôn）統治時，他在努曼諾爾西海岸整軍經武、聚集艦隊，準備攻打維拉，艾爾達隆可能成為了他的基地之一。

亞爾-法拉松向[維拉開戰後](../Page/維拉.md "wikilink")，努曼諾爾在神罰下被大海吞滅，艾爾達隆也隨之沉沒於波浪之下。

## 參考

  - 《[未完成的故事](../Page/未完成的故事.md "wikilink")》

## 資料來源

[E](../Category/中土大陸的地理.md "wikilink")
[E](../Category/中土大陸的城鎮.md "wikilink")

1.  [Thain's Book:
    Eldalondë](http://www.tuckborough.net/towns.html#Eldalonde)

2.  《未完成的故事》1998年 HarperCollins出版 "A Description of Númenor" P.216-217

3.
4.
5.
6.
7.