**艾瑞克·苏曼达**（英文：，），出生於[美國](../Page/美國.md "wikilink")[威斯康辛州的](../Page/威斯康辛州.md "wikilink")[馬夸納戈](../Page/馬夸納戈.md "wikilink")，是一名[電視劇](../Page/電視劇.md "wikilink")[演員](../Page/演員.md "wikilink")。

蘇曼達以出演[CSI犯罪現場中的調查員與實驗室技師格雷格](../Page/CSI犯罪現場.md "wikilink")·桑德斯著稱。也曾在電視劇[網和](../Page/網.md "wikilink")[美國網路中客串出演](../Page/美國網路.md "wikilink")。

艾瑞克還有一個叔叔也是演員，[雷·蘇曼達](../Page/雷·蘇曼達.md "wikilink")，他現在在威斯康辛州地方電視台出演廣告角色，並有一個自己的電台。

## 演出作品

  - *[網](../Page/:en:The_Net_\(TV_series\).md "wikilink")*(1998–1999)–
    雅各·索瑟勒·瑞許
  - *[CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")*(2000-2014)–[葛瑞格·桑德斯](../Page/:en:Greg_Sanders.md "wikilink")
  - *道奇之城*(1999) – 強尼·道奇
  - *[危險景點](../Page/危險景點.md "wikilink")*(2000) –
  - *[愛情磁場](../Page/:en:The_Rules_of_Attraction_\(film\).md "wikilink")*(2000)
    – 紐約大學電影系學生
  - *喔 寶貝*(2000) – 布倫特
  - *[大時代](../Page/:en:Big_Time_\(2000_film\).md "wikilink")*(2000) –
    艾佛瑞
  - *[佐、鄧肯、傑克與珍](../Page/:en:Zoe,_Duncan,_Jack_and_Jane.md "wikilink")*(2000)
    – 麥克斯
  - *[一百個女孩](../Page/:en:100_Girls.md "wikilink")*(2000) – 山姆/電腦天才
  - *[真愛黑膠](../Page/:en:True_Vinyl.md "wikilink")*(2000) – 比利·湯普森
  - *[辣媽連線](../Page/:en:FreakyLinks.md "wikilink")*(2000) – 居住於隧道的流浪漢
  - *[警察部門](../Page/:en:The_Division.md "wikilink")*(2001) – 馬克
  - *[三姊妹](../Page/:en:Three_Sisters.md "wikilink")*(2001) – 戴夫
  - *[雪夜奇蹟](../Page/雪夜奇蹟.md "wikilink")*(2005) – 路克
  - *[小雅典](../Page/:en:Little_Athens.md "wikilink")*(2005) – 德瑞克
  - *[恐怖陰影](../Page/恐怖陰影.md "wikilink")*(2012) – 多米尼克·利瑞

## 外部链接

  - [个人资料](http://www.imdb.com/name/nm844172/)

[S](../Category/1975年出生.md "wikilink")
[S](../Category/美国演员.md "wikilink")
[S](../Category/CSI犯罪現場演員.md "wikilink")