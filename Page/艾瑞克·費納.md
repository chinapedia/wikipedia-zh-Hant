**-{zh-hans:埃里克·费尔纳;zh-hk:艾力·費拿;zh-tw:艾瑞克·費納;}-**，[CBE](../Page/CBE.md "wikilink")（[英语](../Page/英语.md "wikilink")：****，）是一名[英國](../Page/英國.md "wikilink")[電影監製](../Page/電影監製.md "wikilink")。

費納目前與[-{zh-hans:蒂姆·贝万;zh-hk:添·貝雲;zh-tw:提姆·貝文;}-工作於Working](../Page/提姆·貝文.md "wikilink")
Title影業，費納曾製作超過60部電影，包括《[致命檔案](../Page/致命檔案.md "wikilink")》、《[你是我今生的新娘](../Page/你是我今生的新娘.md "wikilink")》、《[越過死亡線](../Page/越過死亡線.md "wikilink")》、《[冰血暴](../Page/冰血暴.md "wikilink")》、《[新娘百分百](../Page/新娘百分百.md "wikilink")》、《[聯航93](../Page/聯航93_\(電影\).md "wikilink")》以及《[BJ單身日記](../Page/BJ單身日記_\(電影\).md "wikilink")》。

[Working Title
Flims影業與](../Page/Working_Title_Flims.md "wikilink")[環球影業在](../Page/環球影業.md "wikilink")1999年簽下6億美元合約，Working
Title可以不需經過環球影業的商議動用3500萬美元的預算。也因此Working
Title成為了英國最大的獨立製片公司，所製作的影片也相當成功。《[你是我今生的新娘](../Page/你是我今生的新娘.md "wikilink")》全球票房2億5千萬美元，《[越過死亡線](../Page/越過死亡線.md "wikilink")》、《[冰血暴](../Page/冰血暴.md "wikilink")》紛紛得到[奧斯卡金像獎榮耀](../Page/奧斯卡金像獎.md "wikilink")。

費納在1972年至1977年間就讀於[英格蘭](../Page/英格蘭.md "wikilink")[薩里郡的克蘭雷學校](../Page/薩里郡.md "wikilink")（Cranleigh
School）。之後就讀倫敦音樂戲劇市政廳學院。

2005年，費納因為對於英國影業的貢獻，獲頒[CBE勳銜](../Page/CBE.md "wikilink")。

## 參與電影

  - 《[致命檔案](../Page/致命檔案.md "wikilink")》（*Hidden Agenda*）
  - 《[你是我今生的新娘](../Page/你是我今生的新娘.md "wikilink")》（*Four Weddings and a
    Funeral*）
  - 《[越過死亡線](../Page/越過死亡線.md "wikilink")》（*Dead Man Walking*）
  - 《[冰血暴](../Page/冰血暴.md "wikilink")》（*Fargo*）
  - 《[新娘百分百](../Page/新娘百分百.md "wikilink")》（*Notting Hill*）
  - 《[聯航93](../Page/聯航93_\(電影\).md "wikilink")》（*United 93*）
  - 《[BJ單身日記](../Page/BJ單身日記_\(電影\).md "wikilink")》（*Bridget Jones's
    Diary*）
  - 《[球戀大滿貫](../Page/球戀大滿貫.md "wikilink")》（*Wimbledon*）
  - 《[音浪青春](../Page/音浪青春.md "wikilink")》（*We Are Your Friends*）
  - 《[丹麦女孩](../Page/丹麦女孩.md "wikilink")》（*The Danish Girl*）
  - 《[凱薩萬歲！](../Page/凱薩萬歲！.md "wikilink")》（*Hail, Caesar\!*）
  - 《[BJ有喜](../Page/BJ有喜.md "wikilink")》（*Bridget Jones's Baby*）
  - 《[玩命再劫](../Page/玩命再劫.md "wikilink")》（*Baby Driver*）

## 外部連結

  - [Working Title影業](http://workingtitlefilms.com/)

  -

[Category:電影監製](../Category/電影監製.md "wikilink")
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")