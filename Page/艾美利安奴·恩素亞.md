**艾美利安奴·恩素亞**（**Emiliano Adrian Insúa
Zapata**，）是一名[阿根廷足球運動員](../Page/阿根廷.md "wikilink")，司職左後衛，為[小保加青訓產品](../Page/小保加體育會.md "wikilink")，現[德甲球會](../Page/德甲.md "wikilink")[斯图加特](../Page/斯图加特体育俱乐部.md "wikilink")。

恩素亞由利物浦在南美洲的球探發掘，[賓尼迪斯決定向](../Page/賓尼迪斯.md "wikilink")[小保加先借](../Page/小保加.md "wikilink")18個月以留意其表現\[1\]，穿上48號球衣。2007年4月28日對[樸茨茅夫的聯賽](../Page/樸茨茅夫.md "wikilink")，由於利物浦正準備歐聯賽事，收起主力球員，恩素亞逐得以正選上陣。

恩素亞一直代表[阿根廷](../Page/阿根廷國家足球隊.md "wikilink")
U20代表隊，他參加了[巴拉圭舉辦的](../Page/巴拉圭.md "wikilink")[2007年南美青年足球錦標賽](../Page/2007年南美青年足球錦標賽.md "wikilink")，可惜不敵巴西。不過，他參加在[加拿大舉辦的](../Page/加拿大.md "wikilink")[世界青年足球錦標賽](../Page/世界青年足球錦標賽.md "wikilink")，則成功在決賽擊敗[捷克奪冠](../Page/捷克國家足球隊.md "wikilink")\[2\]。

2008年5月4日，利物浦主場對[曼城](../Page/曼城.md "wikilink")，恩素亞獲得該季首次上陣的機會，完成整場賽事。2008/09年新賽季，恩素亞獲得22號的新球衣號碼。

2010年夏，土耳其劲旅加拉塔萨雷宣布球队将租借利物浦后卫因苏亚一年，并拥有对阿根廷人的优先买断条款。

2011年8月，利物浦官方宣布因苏亚转会[葡超球队](../Page/葡超.md "wikilink")[体育CP並签下](../Page/葡萄牙体育俱乐部.md "wikilink")5年合约。

## 榮譽

### 球會

  - 利物浦

<!-- end list -->

  - 2008年[英格蘭足球預備組超級聯賽冠軍](../Page/英格蘭足球預備組超級聯賽.md "wikilink")

<!-- end list -->

  - 馬德里競技

<!-- end list -->

  - [西甲](../Page/西甲.md "wikilink"):[2013-14賽季冠軍](../Page/2013–14賽季西班牙足球甲級聯賽.md "wikilink")
  - [西班牙國王盃](../Page/西班牙國王盃.md "wikilink"):[2013年冠軍](../Page/2013年西班牙國王盃決賽.md "wikilink")
  - [西班牙超級盃](../Page/西班牙超級盃.md "wikilink"):
    [2014冠軍](../Page/2014年西班牙超級盃.md "wikilink")
  - [歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink"):
    [2013–14亞軍](../Page/2014年歐洲冠軍聯賽決賽.md "wikilink")

### 國家隊

  - [U20世界盃足球賽](../Page/U20世界盃足球賽.md "wikilink"):[2007年U20世界盃足球賽冠軍](../Page/2007年U20世界盃足球賽.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -
  - [利物浦官方網站
    恩素亞檔案](https://web.archive.org/web/20090317023432/http://www.liverpoolfc.tv/team/squad/insua/)

[Category:阿根廷足球運動員](../Category/阿根廷足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:小保加球員](../Category/小保加球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:加拉塔沙雷球員](../Category/加拉塔沙雷球員.md "wikilink")
[Category:士砵亭球員](../Category/士砵亭球員.md "wikilink")
[Category:馬德里體育會球員](../Category/馬德里體育會球員.md "wikilink")
[Category:巴列卡諾球員](../Category/巴列卡諾球員.md "wikilink")
[Category:史特加球員](../Category/史特加球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:土耳其外籍足球運動員](../Category/土耳其外籍足球運動員.md "wikilink")
[Category:葡萄牙外籍足球運動員](../Category/葡萄牙外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:阿根廷旅外足球運動員](../Category/阿根廷旅外足球運動員.md "wikilink")

1.
2.  [YOUNG RED WINS THE WORLD CUP
    利物浦官方網站](http://www.liverpoolfc.tv/news/archivedirs/news/2007/jul/23/N156454070723-0924.htm)