[Silverplate_B-29_Enola_Gay.jpg](https://zh.wikipedia.org/wiki/File:Silverplate_B-29_Enola_Gay.jpg "fig:Silverplate_B-29_Enola_Gay.jpg")
[B-29_Enola_Gay_w_Crews.jpg](https://zh.wikipedia.org/wiki/File:B-29_Enola_Gay_w_Crews.jpg "fig:B-29_Enola_Gay_w_Crews.jpg")逗留時的合影，圖中叼著煙斗的是機長[保羅·蒂貝茨](../Page/保羅·蒂貝茨.md "wikilink")。\]\]
**艾諾拉·蓋**（）是一架隸屬於[美國陸軍航空軍](../Page/美國陸軍航空軍.md "wikilink")的[B-29超級堡壘轟炸機](../Page/B-29超級堡壘轟炸機.md "wikilink")，「艾諾拉·蓋」這命名源自該機機長[保羅·蒂貝茨](../Page/保羅·蒂貝茨.md "wikilink")（）母親的名字——。此飞机執行了于[日本時間](../Page/日本時間.md "wikilink")1945年8月6日早上8點15分\[1\]，在[廣島上空擲下](../Page/廣島市.md "wikilink")「[小男孩](../Page/小男孩原子彈.md "wikilink")」[原子彈的](../Page/原子弹.md "wikilink")[任务](../Page/廣島市原子彈爆炸.md "wikilink")。\[2\]

## 歷史

艾諾拉·蓋（型號為B-29-45-MO，序號**44-86292**，中隊編號82）是由[格伦·L·马丁公司](../Page/格伦·L·马丁公司.md "wikilink")（今[洛克希德馬丁公司的前身之一](../Page/洛克希德馬丁.md "wikilink")）位於[內布拉斯加州](../Page/內布拉斯加州.md "wikilink")[貝爾維尤](../Page/貝爾維尤.md "wikilink")（Offutt
AFB）內的轟炸機生產線所製造。當時有15架B-29針對原子彈投彈進行改造，稱為「[銀盤計畫](../Page/銀盤計畫.md "wikilink")」，艾諾拉·蓋號就是其中之一。改造內容有：

  - 大幅修改彈艙，包括氣動艙門與英製掛彈/投彈系統；
  - 追加[反向推力能力](../Page/推力反向器.md "wikilink")，與加強降落時的[空氣煞車力量](../Page/空氣煞車_\(航空學\).md "wikilink")；
  - 強化引擎的[燃油噴射與](../Page/燃油噴射.md "wikilink")能力；
  - 移除裝甲與砲塔。

1945年5月9日，艾諾拉·蓋還在生產線時，就已被當時509大隊指揮官蒂貝茨上校給挑走。美國陸軍航空軍在1945年5月18日接收後z轉交給第509大隊393轟炸中隊，於1945年6月14日由羅伯.A.路易斯[上尉率領的團隊](../Page/上尉.md "wikilink")，將它從[內布拉斯加州阿瑪哈市飛往](../Page/內布拉斯加州.md "wikilink")509大隊位於[猶他州溫多弗陸航隊基地](../Page/犹他州.md "wikilink")。

13天後，艾諾拉·蓋號離開溫多弗前往[關島進行彈艙改造](../Page/關島.md "wikilink")，隨後於7月6日飛往[天寧島](../Page/天寧島.md "wikilink")。7月期間，它一共參與了8次訓練與2次任務，包括7月24日與7月26日對[神戶與](../Page/神戶.md "wikilink")[名古屋的工業目標投擲](../Page/名古屋.md "wikilink")[南瓜炸彈](../Page/南瓜炸彈.md "wikilink")。直到7月31日，艾諾拉·蓋號才為了它真正的任務進行演練。起初，該機被授予的中隊編號是12，但基於安全理由需偽裝成第6轟炸大隊旗下的轟炸機，於是在8月1日將機尾標誌塗裝為圓圈中寫著R，並將中隊編號改為82以免與真正的第6轟炸大隊混淆。

## 参与轰炸

除了8月6日的廣島轟炸任務外，艾諾拉·蓋也參與了8月9日的[第二次原子彈攻擊](../Page/長崎市原子彈爆炸.md "wikilink")，在任務中負責對[小倉市的天候狀況偵察工作](../Page/小倉市.md "wikilink")。不過由於攻擊當天原本被設定為首要攻擊目標的小倉氣候狀況不佳，因此實際攜帶「[胖子](../Page/胖子_\(原子彈\).md "wikilink")」原子彈的另一架B-29轟炸機「[博克斯卡號](../Page/博克斯卡號轟炸機.md "wikilink")」（Bockscar）轉而飛至次要目標[長崎市投下炸彈](../Page/長崎市.md "wikilink")。

二戰結束後，艾諾拉·蓋回到美國[新墨西哥州的](../Page/新墨西哥州.md "wikilink")（ Roswell Army Air
Field，今沃克空軍基地）服役，並參與位於[瓜加林環礁的](../Page/瓜加林環礁.md "wikilink")[十字路口行動](../Page/十字路口行動.md "wikilink")，但並沒有參與[比基尼環礁的](../Page/比基尼環礁.md "wikilink")[氫彈試爆](../Page/氫彈.md "wikilink")。1946年8月30日，艾諾拉·蓋號的擁有權由美國空軍轉移到[史密森尼學會手上](../Page/史密森尼學會.md "wikilink")，但因為缺乏適當的存放場地，退役後的飛機在之後的15年中輾轉停放於多處不同的美國空軍設施內。由於露天放置且未能確實看守，期間艾諾拉·蓋號遭到多次闖入，部分的機內零件遭竊。為了避免破壞的程度加遽，1960年8月時史密森尼的工作人員開始將艾諾拉·蓋號解體，將零件運往史密森尼學會位於[馬里蘭州](../Page/馬里蘭州.md "wikilink")（Suitland,
MD）的倉儲設施保存。

[Enola_Gay_on_Display_at_Udvar-Hazy.jpg](https://zh.wikipedia.org/wiki/File:Enola_Gay_on_Display_at_Udvar-Hazy.jpg "fig:Enola_Gay_on_Display_at_Udvar-Hazy.jpg")整機展示的艾諾拉·蓋號。\]\]
到了1980年代，幾名曾在第509飛行大隊服役過的退役軍人們開始進行遊說，希望史密森尼學會能將艾諾拉·蓋號復原並且展出。1995年，艾諾拉·蓋號的機身首次在[華盛頓特區的](../Page/華盛頓特區.md "wikilink")[國立航空太空博物館](../Page/國立航空太空博物館.md "wikilink")（National
Air and Space Museum,
NASM）中展出，在充滿爭議氣氛下紀念人類首次投下原子彈50週年。除了已經公開展示的機身部分外，包括[機翼](../Page/機翼.md "wikilink")、[螺旋槳與](../Page/螺旋槳.md "wikilink")[發動機在內的其他組件也持續地在進行復原工程](../Page/發動機.md "wikilink")，一直到2003年時，所有的組件全被送至即將開幕、位於[華盛頓杜勒斯國際機場的NASM分館](../Page/華盛頓杜勒斯國際機場.md "wikilink")[史蒂文·烏德沃爾哈齊中心](../Page/史蒂文·烏德沃爾哈齊中心.md "wikilink")（Steven
F. Udvar-Hazy
Center）。在那裡艾諾拉·蓋號開始進行整機的組裝工程，並在2003年4月10日完成，至此之後就一直是該中心的重要館藏品之一。

## 參考资料

<references />

[Category:美国轰炸机](../Category/美国轰炸机.md "wikilink")
[Category:曼哈頓計劃](../Category/曼哈頓計劃.md "wikilink")

1.
2.