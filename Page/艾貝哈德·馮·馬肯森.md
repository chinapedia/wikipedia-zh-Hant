**埃貝哈德·馮·馬肯森**（[德文](../Page/德文.md "wikilink")：Eberhard von
Mackensen，）是[第二次世界大戰時](../Page/第二次世界大戰.md "wikilink")[德國的一名一級上將](../Page/德國.md "wikilink")。

## 早年

出生在[德意志帝國](../Page/德意志帝國.md "wikilink")[波森省](../Page/波森.md "wikilink")[比得哥什](../Page/比得哥什.md "wikilink")，他的父親是[德意志帝國陸軍元帥](../Page/德意志帝國.md "wikilink")[奧古斯特·馮·馬肯森](../Page/奧古斯特·馮·馬肯森.md "wikilink")，小馬肯森於1908加入德國陸軍。

[第一次世界大戰時](../Page/第一次世界大戰.md "wikilink")，他是個[中尉](../Page/中尉.md "wikilink")，1915年戰傷轉往參謀官負責謀略，一戰結束後他續留在軍中，1934年升為[上校](../Page/上校.md "wikilink")，1935年被任命[德國第10軍參謀長](../Page/德國第10軍.md "wikilink")，1937年擔任裝甲旅旅長，1938年升為[少將](../Page/少將.md "wikilink")，1939年擔任[威廉·李斯特](../Page/威廉·李斯特.md "wikilink")[上將部隊參謀長](../Page/上將.md "wikilink")。

## 第二次世界大戰

當初擔任[德國第14軍團參謀長](../Page/德國第14軍團.md "wikilink")，於1939年9月謀略進攻[波蘭](../Page/波蘭.md "wikilink")，接著轉任[德國第12軍團參謀長](../Page/德國第12軍團.md "wikilink")，謀略[法國戰役
(二戰)](../Page/法國戰役_\(二戰\).md "wikilink")，1940年1月1日晉升為[中將](../Page/中將.md "wikilink")，8個月後又升官為騎兵上將，1941年1月升任東戰場[德國南方集團軍屬下](../Page/德國南方集團軍.md "wikilink")[德國第3裝甲軍司令開始領帶大軍](../Page/德國第3裝甲軍.md "wikilink")，1942年11月[保羅·路德維希·埃瓦爾德·馮·克莱斯特](../Page/保羅·路德維希·埃瓦爾德·馮·克莱斯特.md "wikilink")[陸軍元帥擔任東戰場](../Page/陸軍元帥.md "wikilink")[德國A集團軍司令時](../Page/德國A集團軍.md "wikilink")，轄管馬肯森改派任[德國第1裝甲軍團司令](../Page/德國第1裝甲軍團.md "wikilink")，並於1943年3月參與[第三次卡爾可夫戰役中大戰](../Page/第三次卡爾可夫戰役.md "wikilink")[蘇聯紅軍](../Page/蘇聯紅軍.md "wikilink")，同年稍後因戰功升[上將](../Page/上將.md "wikilink")，馬肯森被派到[義大利擔任第](../Page/義大利.md "wikilink")14軍團司令，到1944年他奉令退役。

他在戰時榮膺[橡葉騎士鐵十字勳章](../Page/橡葉騎士鐵十字勳章.md "wikilink")。

## 戰後

1945年德國投降並成為戰犯，1947年他被法庭判處長達21年的有期徒刑，但是在1952年就獲釋放，1969年在[Nortorf故逝](../Page/Nortorf.md "wikilink")。

## 注釋

  - Dr. Gerd F. Heuer – *Die Generalobersten des Heeres, Inhaber
    höchster Kommandostellen 1933-45*, ISBN 3-8118-1408-7

[Category:1889年出生](../Category/1889年出生.md "wikilink")
[Category:1969年逝世](../Category/1969年逝世.md "wikilink")
[Category:德國軍事人物](../Category/德國軍事人物.md "wikilink")
[Category:德國大將](../Category/德國大將.md "wikilink")
[Category:第二次世界大戰德國戰犯](../Category/第二次世界大戰德國戰犯.md "wikilink")