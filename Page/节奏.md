[Phenakistoscope_3g07690b.gif](https://zh.wikipedia.org/wiki/File:Phenakistoscope_3g07690b.gif "fig:Phenakistoscope_3g07690b.gif")

**节奏**（）是一种以一定速度的快慢的[节拍](../Page/节拍.md "wikilink")，主要是运用速度上的快慢和音调上的高低把它们组合到一起。例如，2/2拍就是[强](../Page/强.md "wikilink")/[弱](../Page/弱.md "wikilink")，也就是我们常听到的“[嘭恰](../Page/嘭恰.md "wikilink")”，那么3/4拍是[强](../Page/强.md "wikilink")/[弱](../Page/弱.md "wikilink")/[弱](../Page/弱.md "wikilink")，也就是“[嘭恰恰](../Page/嘭恰恰.md "wikilink")”，我们常听到的[圆舞曲大部分就都是](../Page/圆舞曲.md "wikilink")3/4拍的了，4/4拍是[强](../Page/强.md "wikilink")/[弱](../Page/弱.md "wikilink")/[渐强](../Page/渐强.md "wikilink")/[渐弱](../Page/渐弱.md "wikilink")。节奏可以独立被欣赏，例如锣鼓演奏。节奏也可以成为旋律音乐的骨架。

節奏的組成:

<div style="direction: ltr;">

1.模式的規則與不規則 2.聲音的長短 3.聲音的強弱 4.聲音的有無

</div>

  - 1/2拍: ta
  - 1/4拍: ti

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - .

  -
  -
  -
  -
  -
## 延伸閱讀

  -
  - Humble, M. (2002). [The Development of Rhythmic Organization in
    Indian Classical
    Music](https://www.scribd.com/doc/25227226/The-Development-of-Rhythmic-Organization-in-Indian-Classical-Music),
    MA dissertation, School of Oriental and African Studies, University
    of London.

  - Lewis, Andrew (2005). *Rhythm—What it is and How to Improve Your
    Sense of It*. San Francisco:
    [RhythmSource](http://rhythmsource.com/dev/books/) Press. .

  - Williams, C. F. A., *The Aristoxenian Theory of Musical Rhythm*,
    (Cambridge Library Collection—Music), Cambridge University Press;
    first edition, 2009.

## 参看

  - [音乐](../Page/音乐.md "wikilink"): [旋律](../Page/旋律.md "wikilink")

## 外部連結

  - ['Rhythm of Prose', William Morrison Patterson ,Columbia University
    Press 1917](https://archive.org/stream/rhythmofproseexp00pattiala/rhythmofproseexp00pattiala_djvu.txt)
  - [Melodyhound has a "Query by Tapping" search that allows users to
    identify music based on
    rhythm](http://www.melodyhound.com/query_by_tapping.0.html)
  - [Louis Hébert, "A Little Semiotics of Rhythm. Elements of
    Rhythmology", in
    *Signo*](http://www.signosemio.com/semiotics-of-rhythm.asp)

[節奏](../Category/節奏.md "wikilink")
[Category:音乐术语](../Category/音乐术语.md "wikilink")