**节奏布鲁斯**（，簡稱：**R\&B**或**RnB**，中國大陸譯作「-{zh;zh-hant;zh-hans|节奏布鲁斯}-」，香港、臺灣及新馬譯作「-{zh;zh-hant;zh-hans|節奏藍調}-」），是一种[美国](../Page/美国.md "wikilink")[非裔](../Page/非裔.md "wikilink")[艺术家首先采用](../Page/艺术家.md "wikilink")，并融合了[爵士乐](../Page/爵士乐.md "wikilink")、[福音音乐和](../Page/福音音乐.md "wikilink")[蓝调音乐的音乐形式](../Page/蓝调音乐.md "wikilink")。这个音乐术语是由美国[告示牌](../Page/告示牌_\(雜誌\).md "wikilink")（Billboard）于1940年代末所提出。

節奏藍調最初在1947年的美国被告示牌雜誌的Jerry
Wexler创造为一个音乐的市场营销术语\[1\]，取代了[种族音乐](../Page/非裔美國人音樂.md "wikilink")（英語：Race
Music，又譯黑人音樂；最初来源于黑人社群，但是被战后的社会认为是会令人很不愉快的词语）而[告示牌在](../Page/Billboard.md "wikilink")1949年將其分類為[哈林暢銷排行榜](../Page/Harlem.md "wikilink")。这个词最初被用于定义包含了12小節[布鲁斯格式和一种带有节拍背景的](../Page/布鲁斯.md "wikilink")[電藍調融合](../Page/電藍調.md "wikilink")[爵士乐的](../Page/爵士乐.md "wikilink")[摇滚乐](../Page/摇滚乐.md "wikilink")，后来这种音乐转变成了一种[摇滚乐的基本元素](../Page/摇滚乐.md "wikilink")。

在1995年《Rock & Roll: An Unruly History》書中Robert
Palmer把节奏布鲁斯定义为那些約定俗成、用来意指任何一种美国黑人创造的音乐。在他1981年出版的《Deep
Blues》中，Palmer用「R\&B」作为跳跃布鲁斯的缩写。

Lawrence Cohn是《Nothing But the
Blues》的作者，写到节奏布鲁斯是一个宣传人员为了产业便利所发明的，除了[古典音乐和](../Page/古典音乐.md "wikilink")[宗教音乐](../Page/宗教音乐.md "wikilink")，节奏布鲁斯包括了所有的黑人音乐，除了那是一首[福音歌曲並且其銷售足以打入流行榜](../Page/福音音樂.md "wikilink")。

在1960年代，节奏布鲁斯被用于总括[靈魂樂和](../Page/靈魂樂.md "wikilink")[放克音樂的术语](../Page/放克音樂.md "wikilink")。而现在R\&B的缩写差不多一直被用于代替全寫「节奏布鲁斯」，而主流則用它來表示由[迪斯可逐步變得不那麼流行而发展出来的](../Page/迪斯可.md "wikilink")[Contemporary
R\&B](../Page/Contemporary_R&B.md "wikilink")──一種[现代灵魂乐和受](../Page/现代灵魂乐.md "wikilink")[放克音樂影响的](../Page/放克音樂.md "wikilink")[流行音樂](../Page/流行音樂.md "wikilink")。

## 起源

首先由[非洲裔美国人艺术家所演奏](../Page/非洲裔美国人.md "wikilink")，融合了[爵士乐](../Page/爵士乐.md "wikilink")、[福音音乐和](../Page/福音音乐.md "wikilink")[布鲁斯音乐的音乐形式](../Page/布鲁斯音乐.md "wikilink")。

這個音樂術語由是美國告示牌杂志（Billboard）於1940年代末所提出。

## 先驱

20世纪20年代到30年代，非洲裔美国人向芝加哥、底特律、纽约、洛杉矶等工业城市的的迁徙为爵士、蓝调以及其它各类由全职创作音乐的个人或者小团体演奏的音乐开启了一个新的篇章。节奏布鲁斯的先驱们来自创作爵士和蓝调音乐的音乐家。在二十世纪二三十年代的末期，一些音乐家的作品开始出现爵士风格和蓝调风格的融合，如The
Harlem Hamfats乐队1936年的"Oh Red"就是一个很典型的例子。类似的例子还有Lonnie Johnson, Leroy
Carr, Cab Calloway, Count Basie，和T-Bone
Walker他们的作品。除此之外乐器使用上也开始出现一些改变，电吉他开始成为主要的演奏乐器之一。

## 20世纪40年代后期

1948年，唱片发行商RCA Victor在"Blues and Rhythm"分类下发行黑人音乐。在那一年，Louis
Jordan占据了R\&B榜单前五首歌曲中的三首，并且前五首中有两首基于boogie-woogie旋律。boogie-woogie旋律是在20世纪40年代开始流行的。\[2\]
Jordan的乐队Tympany
Five组建于1938年，他在乐队中担任萨克斯手和歌手，同时还有其他音乐家负责小号、高音萨克斯、钢琴、贝斯和鼓。\[3\]Lawrence
Cohn将Jordan的音乐描述为"grittier than his boogie-era jazz-tinged blues"\[4\]
Robert Palmer将其描述为"urbane, rocking, jazz-based music with a heavy,
insistent beat".\[5\] Jordan的音乐，还包括Big Joe Turner、Roy Brown、Billy
Wright和Wynonie Harris的音乐，现在被称为jump blues。Paul Gayten、Roy
Brown和其他一些音乐家当时已经尝试了现在被称为rhythm and
blues的音乐风格。在1948年，Wynonie
Harris重新制作了Brown在1947年发行的专辑"Good Rockin'
Tonight"，并在销售榜上名列第二。乐队领袖Sonny Thompson的"Long Gone"排在第一。\[6\]\[7\]

1949年，"Rhythm and Blues"在Billboard上替代了原先的分类“Harlem Hit
Parade”\[8\]同时在那一年，由乐队领袖兼萨克斯手的Paul Williams录制的"The
Huckle-Buck"成为了排名第一的R\&B音乐，并保持了接近一年。"The Huckle-Buck"由编曲人Andy
Gibson所创作，由于曲调有伤风化且略显下流，它被描述为"dirty boogie"。\[9\] Paul
Williams和他的"The Huckle-Buck"演唱会常常十分骚乱，以至于不止一次得被停止。它的歌词（由Roy
Alfred所写，他同时还写了1955年上榜歌曲Rock and Roll
Waltz的歌词）充满性暗示。一位来自费城的青年说"That
Hucklebuck was a very nasty dance"。\[10\]\[11\]
同时在1949年，一首20年的布鲁斯音乐"[Ain't Nobody's
Business](../Page/Ain't_Nobody's_Business.md "wikilink")"的新版本在榜单上排第四（它由Jimmy
Witherspoon重新创作）。Louis Jordan和他的乐队Tympany Five的作品Saturday Night Fish
Fry也在此名列榜单前五。\[12\]许多这些歌曲都是由当时刚成立的唱片公司发行，例如Savoy（成立于1942年）、King（成立于1943年）、Imperial（成立于1945年）、Specialty（成立于1946年）、Chess（成立于1947年）、Atlantic（成立于1948年）。\[13\]

## 20世纪50年代

1956年，一场“Top Star of 56”蓝调布鲁斯音乐巡演在北美洲展开。许多当红R\&B音乐人参与其中，如Chuck Berry,
Cathy Carr, Shirley & Lee, Della Reese, the
Cleftones等等。这场巡回演唱覆盖面非常广，所到城市中不乏美国的大城市哥伦比亚、、纽约、安纳波利斯、匹兹堡、布法罗，乐队后来又来到了加拿大进行演出，最后跨越了美国的中西部，在德克萨斯结束。这场巡演极大地提升了蓝调布鲁斯音乐的影响力和知名度。

这一时期，走进大众视野的最著名的R\&B音乐人是埃尔维斯·普雷斯利，即我们常说的猫王。1957年，他的两张R\&B专辑取得了销量前5的好成绩，其中"Jailhouse
Rock"/"Treat Me Nice"取得第一名，"All Shook Up"取得第五名。这被视为黑人艺术家创造的前所未有的成就。

另一位爵士钢琴家兼歌手，纳京高（Nat King Cole），同样也在R\&B音乐中取得了不俗的成就。他的作品"Mona Lisa"，"Too
Young"，"Looking Back"/"Do I Like It"也取得了前5的好成绩。

## 现代节奏藍調

**R\&B**一詞在今日被定義為[非裔美式的](../Page/非裔美式.md "wikilink")[流行音樂](../Page/流行音樂.md "wikilink")，源起於[迪斯可音樂後續之](../Page/迪斯可.md "wikilink")1980年代，融合了[靈魂樂](../Page/靈魂樂.md "wikilink")、[黑人靈歌](../Page/黑人聖歌.md "wikilink")、[放克音樂](../Page/放克音樂.md "wikilink")、[流行音樂以及](../Page/流行音樂.md "wikilink")[hip-hop](../Page/嘻哈音樂.md "wikilink")(1986年後)，也就是[當代節奏藍調](../Page/當代節奏藍調.md "wikilink")。本文中僅使用R\&B縮寫一詞表意。現代R\&B的編制特徵包含電音形式[錄音製作的風格](../Page/錄音製作.md "wikilink")、[類比鼓機製式節奏](../Page/類比鼓機.md "wikilink")，以及一種較為柔和、圓渾豐厚的[歌聲](../Page/歌聲.md "wikilink")。雖然其多與[黑人](../Page/黑人.md "wikilink")[靈歌那更富激越而情感高亢的音樂類別有所互異](../Page/靈歌.md "wikilink")，然則兩者實互有不可或缺的重合[元素](../Page/元素.md "wikilink")，尤其在[新傑克搖擺](../Page/新傑克搖擺.md "wikilink")、[嘻哈靈音以及](../Page/嘻哈靈音.md "wikilink")[新靈魂等](../Page/新靈魂.md "wikilink")[嘻哈音樂的類屬中可見一斑](../Page/嘻哈音樂.md "wikilink")。

## 参见

  -
## 參考資料

[\*](../Category/節奏藍調.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.

9.

10.

11.

12.

13.