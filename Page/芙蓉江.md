**芙蓉江**又名**盘古河**，是[乌江的最大支流](../Page/乌江.md "wikilink")，发源于[贵州省](../Page/贵州省.md "wikilink")[绥阳县境内的](../Page/绥阳县.md "wikilink")[大娄山](../Page/大娄山.md "wikilink")，由西南向东北在[重庆市](../Page/重庆市.md "wikilink")[武隆县](../Page/武隆县.md "wikilink")[江口镇汇入](../Page/江口镇_\(武隆\).md "wikilink")[乌江](../Page/乌江.md "wikilink")，干流全长227千米（贵州占87%，重庆占13%），天然落差1075米（贵州占90%，重庆占10%），流域面积7744.5平方千米（贵州占93%，重庆占7%）。\[1\]

芙蓉江流域是典型的[喀斯特地貌](../Page/喀斯特地貌.md "wikilink")，流域内人口密度小，原始生态保存较好。河谷呈是典型的"V"字峡谷，且原始[植被密植两岸](../Page/植被.md "wikilink")。

**芙蓉江风景名胜区**位于武隆县境内，从[浩口乡至江口镇](../Page/浩口苗族仡佬族乡.md "wikilink")，长35公里，面积152.8平方公里，为一原始生态峡谷型景区。2002年5月17日被[国务院列入](../Page/国务院.md "wikilink")[国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")。2007年，[芙蓉洞](../Page/芙蓉洞.md "wikilink")、芙蓉江分别作为“[中国南方喀斯特](../Page/中国南方喀斯特.md "wikilink")”[武隆喀斯特](../Page/武隆喀斯特.md "wikilink")（部分）的核心区和缓冲区列入[世界遗产名录](../Page/世界遗产名录.md "wikilink")。

1991年规划在芙蓉江干流修建10个梯级[水电站](../Page/水电站.md "wikilink")，自上而下分别为[小河口](../Page/小河口水电站.md "wikilink")、[朱老村](../Page/朱老村水电站.md "wikilink")、[牛都](../Page/牛都水电站.md "wikilink")、[田坝](../Page/田坝水电站.md "wikilink")、[良坎](../Page/良坎水电站.md "wikilink")、[沙阡](../Page/沙阡水电站.md "wikilink")、[鱼塘](../Page/鱼塘水电站.md "wikilink")、[石门坎](../Page/石门坎水电站.md "wikilink")、[浩口](../Page/浩口水电站.md "wikilink")、[江口](../Page/江口水库_\(武隆\).md "wikilink")。\[2\]

## 参考文献

[F](../Category/中国国家重点风景名胜区.md "wikilink")
[F](../Category/贵州河流.md "wikilink")
[F](../Category/重庆河流.md "wikilink")
[Category:烏江水系](../Category/烏江水系.md "wikilink")

1.

2.