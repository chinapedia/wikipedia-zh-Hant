**芜湖县**位于[中國](../Page/中國.md "wikilink")[安徽省东南部](../Page/安徽省.md "wikilink")，是[芜湖市下辖的一个县](../Page/芜湖市.md "wikilink")。古称[鸠兹](../Page/鸠兹.md "wikilink")。面积942.2平方千米，人口53.9万人。

[春秋時代因这附近一帶的湖泊草丛中鸠鸟众多](../Page/春秋時代.md "wikilink")，被[吴国命名为](../Page/吴国.md "wikilink")[鸠兹邑](../Page/鸠兹.md "wikilink")，流经此地的[长江一段由此别名](../Page/长江.md "wikilink")[鸠江](../Page/鸠江.md "wikilink")，故址在今芜湖市东南约四十里的[水阳江南岸](../Page/水阳江.md "wikilink")。汉时又因此地湖泊众多，芜草丛生，得名[芜湖](../Page/芜湖.md "wikilink")。

## 行政区划

2004年，根据《关于同意将芜湖县清水镇划入市区的批复》（民地字\[2004\]49号），芜湖县清水镇划入芜湖市鸠江区。
2005年9月13日，国务院批准（国函\[2005\]77号）调整芜湖市部分行政区划，将芜湖县火龙岗镇划归弋江区管辖。2006年2月9日，芜湖市人民政府批准：将芜湖县原荆山镇区域划归镜湖区管辖，将火龙岗镇划归弋江区管辖，将清水镇（不含原荆山镇区域）划归鸠江区管辖。
2011年，方村镇划归镜湖区管辖。 现辖5镇：湾沚镇、六郎镇、陶辛镇、红杨镇、花桥镇。

## 名胜古迹

  - 楚王城遗迹
  - [南唐九十殿](../Page/南唐.md "wikilink")
  - 东门渡官窑
  - [宋代珩琅宝塔](../Page/宋代.md "wikilink")
  - 陶辛水韵：芜湖十景。
  - 和平生态公园

## 交通

  - **高速公路**
    [沪渝高速](../Page/沪渝高速.md "wikilink")、[溧芜高速](../Page/溧芜高速.md "wikilink")
  - **铁路**
    [皖贛铁路](../Page/皖贛铁路.md "wikilink")、[商杭客运专线](../Page/商杭客运专线.md "wikilink")、[皖贛铁路新建双线](../Page/皖贛铁路.md "wikilink")（规划）
  - **航空** [芜湖宣城机场](../Page/芜湖宣城机场.md "wikilink")（规划）
  - **水运**
    境内[水阳江](../Page/水阳江.md "wikilink")、[青弋江下通](../Page/青弋江.md "wikilink")[长江](../Page/长江.md "wikilink")，上达[黄山北麓](../Page/黄山.md "wikilink")[太平湖](../Page/太平湖.md "wikilink")，古为[徽商贩运货物的重要通道](../Page/徽商.md "wikilink")。今有[芜申运河进入](../Page/芜申运河.md "wikilink")[太湖](../Page/太湖.md "wikilink"),通往[宜兴](../Page/宜兴.md "wikilink")、[上海](../Page/上海.md "wikilink"),大大缩短[上海中小船舶进入长江中上游的航程](../Page/上海.md "wikilink")。

## 外部链接

  - [芜湖县政务信息网](http://www.whx.gov.cn/)

[芜湖市](../Page/category:安徽县份.md "wikilink")

[芜湖县](../Category/芜湖县.md "wikilink") [县](../Category/芜湖区县.md "wikilink")