[Park_near_the_bridge,_Wuhu_Yangtze_River_Bridge1.jpg](https://zh.wikipedia.org/wiki/File:Park_near_the_bridge,_Wuhu_Yangtze_River_Bridge1.jpg "fig:Park_near_the_bridge,_Wuhu_Yangtze_River_Bridge1.jpg")
**芜湖长江大桥**位于[长江下游](../Page/长江.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[芜湖市](../Page/芜湖市.md "wikilink")，为一座横跨长江的公、铁两用钢桁梁[斜拉桥](../Page/斜拉桥.md "wikilink")。

芜湖长江大桥1997年3月正式动工，2000年5月16日大桥合龙，9月30日建成开通。大桥由[中华人民共和国铁道部和](../Page/中华人民共和国铁道部.md "wikilink")[安徽省人民政府合资建设](../Page/安徽省人民政府.md "wikilink")，铁道部出资70%，安徽省出资30%。

铁路桥长10624.4米，公路桥长6078.4米，大桥跨江主桥长2193米，主跨312米，在建成时是中国公铁两用桥中跨度最大的桥梁。公路在上层：公路部分为4车道，双人行道，联接[合芜](../Page/合芜高速公路.md "wikilink")、[芜宁](../Page/芜宁高速公路.md "wikilink")、[芜杭等多条高速公路和国道](../Page/芜杭高速公路.md "wikilink")；铁路在下层，铁路部分为[淮南铁路](../Page/淮南铁路.md "wikilink")（国铁Ⅰ级复线电气化铁路，运行限速60KM/H）。

大桥采用低塔，斜拉索加劲的连续钢桁梁新桥型，主塔高84.2米。工程混凝土用量约53万立方米，结构用钢约11万吨。北引桥长6098.82米，南引桥长2228.45米。引桥的上部结构主要为32米预应力简支Ｔ梁（106双线孔）和40米预应力简支箱梁。桥墩除连续梁或刚构和北引桥70孔32mi预应力简支T梁（墩高较低）为板式墩外，其余均为双柱式墩（框架式轻型墩）\[1\]。

## 相关条目

  - [长江桥隧列表](../Page/长江桥隧列表.md "wikilink")

## 参考文献

## 外部鏈接

[Category:芜湖桥梁](../Category/芜湖桥梁.md "wikilink")
[皖](../Category/长江大桥.md "wikilink")
[Category:中国公路铁路两用桥](../Category/中国公路铁路两用桥.md "wikilink")
[Category:中国斜拉桥](../Category/中国斜拉桥.md "wikilink")
[Category:桁架橋](../Category/桁架橋.md "wikilink")
[Category:2000年完工桥梁](../Category/2000年完工桥梁.md "wikilink")

1.