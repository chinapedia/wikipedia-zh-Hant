**芝加哥摇滚之星队**（**Chicago
Rockstars**）是[新的美国篮球协会](../Page/美国篮球协会_\(21世纪\).md "wikilink")2006年新近扩充的一支球队，球队主场设在[芝加哥](../Page/芝加哥.md "wikilink")。球队最初把主体育场设在[芝加哥州立大学内的](../Page/芝加哥州立大学.md "wikilink")[埃米尔和帕特丽西娅·琼斯会议中心但是](../Page/:en:Emil_and_Patricia_Jones_Convocation_Center.md "wikilink")，考虑到可能会和在校学生使用球馆时间有冲突而决定将球队迁到[东南伊利诺伊大学](../Page/东南伊利诺伊大学.md "wikilink")。[1](http://www.oursportscentral.com/services/releases/?id=3385092)

## 外部链接

  - [Team
    Website](https://web.archive.org/web/20070108225009/http://www.rockstarbasketball.com/)

[Category:美国篮球协会(21世纪)球队](../Category/美国篮球协会\(21世纪\)球队.md "wikilink")