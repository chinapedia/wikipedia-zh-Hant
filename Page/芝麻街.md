《**芝麻街**》（）是由[美國](../Page/美國.md "wikilink")制作的一档著名的儿童教育電視節目，自1969年在[美國公共電視台](../Page/美國公共電視台.md "wikilink")（PBS）首播以来，已经陪伴全球140多个国家超过1亿儿童健康快乐成长，荣获150多次[艾美奖](../Page/艾美奖.md "wikilink")，芝麻街也因此被誉为全世界最长的街道。節目最初是由兒童電視工作室（Children's
Television Workshop）製作，後來由2000年分支出來的芝麻街工作室（Sesame
Workshop）繼續製作。芝麻街最為人熟悉的部份，正是節目中採用[布偶](../Page/布偶.md "wikilink")（Puppet）作為大部份角色，布偶是由著名木偶師[吉姆·亨森](../Page/吉姆·亨森.md "wikilink")（Jim
Henson）創造。

## 節目歷史

  - [2016年](../Page/2016年.md "wikilink")[1月16日起](../Page/1月16日.md "wikilink")，由於[HBO啟播](../Page/HBO.md "wikilink")，《芝麻街》在[PBS及](../Page/公共广播电视公司.md "wikilink")[HBO聯播](../Page/HBO.md "wikilink")。

## 芝麻街在中国

  - 1983年，《芝麻街》的“大鸟”首次来到中国，与[中央电视台拍摄](../Page/中央电视台.md "wikilink")《大鸟在中国》特别节目。
  - 1998年，[芝麻街工作室与](../Page/芝麻街工作室.md "wikilink")[上海电视台合作拍摄](../Page/上海电视台.md "wikilink")104集中文版《芝麻街》节目。
  - 2009年，[芝麻街工作室与](../Page/芝麻街工作室.md "wikilink")[上海文广新闻传媒集团](../Page/上海文广新闻传媒集团.md "wikilink")（SMG）合作拍摄52集中文版电视节目《芝麻街之大鸟看世界》。

## 布偶角色

<table>
<thead>
<tr class="header">
<th><p><strong>布偶</strong></p></th>
<th><p><strong>配音</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/艾蒙.md" title="wikilink">艾蒙</a></p></td>
<td><p>最早出現在芝麻街是1980年，<br />
（1985年到2012年）Kevin Clash<br />
Ryan Dillion（2012年至今）</p></td>
</tr>
<tr class="even">
<td><p>Big Bird|大鸟</p></td>
<td><p>Caroll Spinney|卡罗尔斯平尼 （1969年到2018年，宣佈退休）<br />
Matt Vogel（1997年至今）</p></td>
</tr>
<tr class="odd">
<td><p>Abby Cadabby|艾比</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Kermit the Frog|青蛙柯密特</p></td>
<td><p>Jim Henson<br />
Steve Whitmire</p></td>
</tr>
<tr class="odd">
<td><p>Bert|伯特</p></td>
<td><p>Frank Oz|弗兰克 · 奥兹<br />
Eric Jacobson</p></td>
</tr>
<tr class="even">
<td><p>Ernie|恩尼</p></td>
<td><p>Jim Henson<br />
Steve Whitmire</p></td>
</tr>
<tr class="odd">
<td><p>Cookie Monster|饼干怪兽</p></td>
<td><p>Frank Oz<br />
David Rudman</p></td>
</tr>
<tr class="even">
<td><p>Count Von Count|伯爵</p></td>
<td><p>Jerry Nelson|杰里 · 纳尔逊（1971年到2012年）<br />
Matt Vogel（2012年至今）</p></td>
</tr>
<tr class="odd">
<td><p>Grover|格罗弗</p></td>
<td><p>Frank Oz<br />
Eric Jacobson</p></td>
</tr>
<tr class="even">
<td><p>Oscar (Oscar the GROUCH)|奥斯卡</p></td>
<td><p>Caroll Spinney|卡罗尔斯平尼 （1969年到2018年）<br />
Eric Jacobson（2015年至今）</p></td>
</tr>
<tr class="odd">
<td><p>Rosita|罗齐塔</p></td>
<td><p>Carmen Osbahr</p></td>
</tr>
<tr class="even">
<td><p>Telly Monster</p></td>
<td><p>Brian Muehl<br />
Martin P. Robinson</p></td>
</tr>
<tr class="odd">
<td><p>Zoe|佐伊</p></td>
<td><p>Fran Brill|弗兰布瑞尔（1993年到2014年）<br />
Jennifer Barnhart（2015年至今）</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [芝麻街全球官方网站](http://www.sesamestreet.org/home)
  - [芝麻街中国官方网站](http://www.sesamestreetchina.com.cn/)
  - [芝麻街台灣官方網站](http://www.sesamevillage.tw/intro.php)

[Category:美國電視節目](../Category/美國電視節目.md "wikilink")
[Category:兒童電視節目](../Category/兒童電視節目.md "wikilink")
[Category:芝麻街](../Category/芝麻街.md "wikilink")
[Category:皮博迪獎得主](../Category/皮博迪獎得主.md "wikilink")
[Category:無綫電視外購節目](../Category/無綫電視外購節目.md "wikilink")
[Category:英語教學](../Category/英語教學.md "wikilink")