**芦竹碱**（）又名**禾草碱**\[1\]，是一种[吲哚族](../Page/吲哚.md "wikilink")[生物碱](../Page/生物碱.md "wikilink")，有毒。

## 化学性质

  - [IUPAC名称](../Page/IUPAC命名法.md "wikilink")：
      - 中文：吲哚-3-基-*N*,*N*-二甲基甲胺
      - 英文：indolin-3-yl-*N*,*N*-dimethylmethanamine
  - [CAS号](../Page/CAS号.md "wikilink")：87-52-5
  - 分子式：C<sub>11</sub>H<sub>16</sub>N<sub>2</sub>
  - 分子量：176.26
  - 熔点：138\~139℃
  - 溶于[醇](../Page/醇.md "wikilink")、[醚](../Page/醚.md "wikilink")、[氯仿](../Page/氯仿.md "wikilink")，微溶于冷的[丙酮](../Page/丙酮.md "wikilink")，不溶于[水](../Page/水.md "wikilink")、[石油醚](../Page/石油醚.md "wikilink")。

## 来源

  - 可从[大麦中分离获得](../Page/大麦.md "wikilink")。
  - 由[吲哚与](../Page/吲哚.md "wikilink")[甲醛](../Page/甲醛.md "wikilink")、[二甲胺缩合而得](../Page/二甲胺.md "wikilink")。将二甲胺冷至-5℃，加至[冰醋酸中](../Page/冰醋酸.md "wikilink")。在温度不超过5℃的条件下，加入甲醛、吲哚，在30-40℃反应8h，冷至5℃，用40%[氢氧化钠调节pH等于](../Page/氢氧化钠.md "wikilink")1，冷却出芦竹碱结晶。过滤，水洗，抽干后将此粗品用[丙酮加热溶解](../Page/丙酮.md "wikilink")，冷却、析出结晶、过滤、干燥，得成品。

[Category:生物碱](../Category/生物碱.md "wikilink")
[Category:吲哚](../Category/吲哚.md "wikilink")

1.