**芭芭拉·史翠珊**（，），[猶太裔](../Page/猶太人.md "wikilink")[美国](../Page/美国.md "wikilink")[歌手](../Page/歌手.md "wikilink")，[电影](../Page/电影.md "wikilink")[演员](../Page/演员.md "wikilink")，[导演和制片人](../Page/导演.md "wikilink")。

## 早年时期

芭芭拉·史翠珊出生在美國[布鲁克林的](../Page/布鲁克林.md "wikilink")[猶太人家庭](../Page/猶太人.md "wikilink")\[1\]\[2\]。她15個月大時，父親就已經去世\[3\]\[4\]。她的父親Emanuel
Streisand是猶太人，是一位老師\[5\]\[6\]。母親是後裔\[7\]。芭芭拉·史翠珊曾經就讀紐約市布魯克林的猶太學校（Beis
Yakov Jewish School）\[8\]。

1960年开始成为一位[俱乐部歌手](../Page/俱乐部.md "wikilink")。1962年与[哥伦比亚唱片公司签约并发表了她的第一张唱片](../Page/哥伦比亚.md "wikilink")，在1963年贏得两个[葛萊美奖](../Page/葛萊美奖.md "wikilink")，成为最年轻的“年度最佳专辑”大奖得主，随后她也在[百老汇上出演音乐剧](../Page/百老汇.md "wikilink")《妙女郎》，以其改编的影片获得奥斯卡影后桂冠，并在[CBS的电视节目中获得巨大成功](../Page/CBS.md "wikilink")。

## 电影事业

她的第一部电影是1968年百老汇的一个剧目《》，這部電影讓她獲得第41屆[奧斯卡](../Page/奧斯卡.md "wikilink")[最佳女主角獎](../Page/奥斯卡最佳女主角奖.md "wikilink")，特別的是，該屆[奧斯卡最佳女主角獎共有兩位得主](../Page/奥斯卡最佳女主角奖.md "wikilink")，另外一位是[凱瑟琳·赫本](../Page/凯瑟琳·赫本.md "wikilink")（Katharine
Hepburn），這也是[奧斯卡史上第一次也是唯一的一次同時產生兩位影后](../Page/奧斯卡.md "wikilink")。

从影以来，她贏得二座[奥斯卡奖](../Page/奥斯卡奖.md "wikilink")、五座[艾美奖](../Page/艾美奖.md "wikilink")、八座[金球奖和许多的其他奖項](../Page/金球奖.md "wikilink")。她的第二座奥斯卡金像奖是歌曲〈Evergreen〉，来自电影《》（1976年）。在
1995年年她获得了葛莱美终生成就奖。

她还制作了许多她自己的电影，例如《》（1983年），《[潮浪王子](../Page/潮浪王子.md "wikilink")》（1991年）等，并获得了多项奥斯卡提名。

其出演的影片累计全球票房至少超过15亿美元。按2012年美元价值折算，她主演的18部电影（至"Little
Fockers"止）平均北美票房高达1.6亿美元，雄踞女星之首，列电影史第7位。这些影片共获得44项奥斯卡提名，平均提名次数列史上第一，小胜克拉克.盖博。

## 历年电影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>注明</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1968</p></td>
<td><p>《》</p></td>
<td><p><a href="../Page/Fanny_Brice.md" title="wikilink">Fanny Brice</a></p></td>
<td><p><a href="../Page/Academy_Award_for_Best_Actress.md" title="wikilink">Academy Award for Best Actress</a> <small>Tied with <a href="../Page/Katharine_Hepburn.md" title="wikilink">Katharine Hepburn</a> for <em><a href="../Page/The_Lion_in_Winter_(1968_film).md" title="wikilink">The Lion in Winter</a></em></small><br />
<a href="../Page/David_di_Donatello_for_Best_Actress.md" title="wikilink">David di Donatello for Best Foreign Actress</a> <small>Tied with <a href="../Page/Mia_Farrow.md" title="wikilink">Mia Farrow</a> for <em><a href="../Page/Rosemary&#39;s_Baby_(film).md" title="wikilink">Rosemary's Baby</a></em></small><br />
<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Musical_or_Comedy.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Musical or Comedy</a><br />
提名—<a href="../Page/BAFTA_Award_for_Best_Actress_in_a_Leading_Role.md" title="wikilink">BAFTA Award for Best Actress in a Leading Role</a> <small>also for <em><a href="../Page/Hello,_Dolly!_(film).md" title="wikilink">Hello, Dolly!</a></em></small></p></td>
</tr>
<tr class="even">
<td><p>1969</p></td>
<td><p>《》</p></td>
<td><p>Dolly Levi</p></td>
<td><p>提名—<a href="../Page/BAFTA_Award_for_Best_Actress_in_a_Leading_Role.md" title="wikilink">BAFTA Award for Best Actress in a Leading Role</a> <small>also for <em><a href="../Page/Funny_Girl_(film).md" title="wikilink">Funny Girl</a></em></small><br />
提名—<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Musical_or_Comedy.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Musical or Comedy</a></p></td>
</tr>
<tr class="odd">
<td><p>1970</p></td>
<td><p>《》</p></td>
<td><p>Daisy Gamble / Melinda Tentres</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Doris Wilgus/Wadsworth/Wellington/Waverly</p></td>
<td><p>提名—<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Musical_or_Comedy.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Musical or Comedy</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1972</p></td>
<td><p>《<a href="../Page/愛的大追蹤.md" title="wikilink">愛的大追蹤</a>》</p></td>
<td><p>Judy Maxwell</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Margaret Reynolds</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1973</p></td>
<td><p>《<a href="../Page/俏郎君.md" title="wikilink">俏郎君</a>》</p></td>
<td><p>Katie Morosky</p></td>
<td><p><a href="../Page/David_di_Donatello_for_Best_Actress.md" title="wikilink">David di Donatello for Best Foreign Actress</a> <small>Tied with <a href="../Page/Tatum_O&#39;Neal.md" title="wikilink">Tatum O'Neal</a> for <em><a href="../Page/Paper_Moon_(film).md" title="wikilink">Paper Moon</a></em></small><br />
提名—<a href="../Page/Academy_Award_for_Best_Actress.md" title="wikilink">Academy Award for Best Actress</a><br />
提名—<a href="../Page/BAFTA_Award_for_Best_Actress_in_a_Leading_Role.md" title="wikilink">BAFTA Award for Best Actress in a Leading Role</a><br />
提名—<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Drama.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Drama</a></p></td>
</tr>
<tr class="even">
<td><p>1974</p></td>
<td><p>《》</p></td>
<td><p>Henrietta 'Henry' Robbins</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1975</p></td>
<td><p>《》</p></td>
<td><p><a href="../Page/Fanny_Brice.md" title="wikilink">Fanny Brice</a></p></td>
<td><p>提名—<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Musical_or_Comedy.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Musical or Comedy</a></p></td>
</tr>
<tr class="even">
<td><p>1976</p></td>
<td><p>《》</p></td>
<td><p>Esther Hoffman Howard</p></td>
<td><p><a href="../Page/Academy_Award_for_Best_Original_Song.md" title="wikilink">Academy Award for Best Original Song</a> <small>Shared with <a href="../Page/Paul_Williams_(songwriter).md" title="wikilink">Paul Williams</a> (lyrics) for the song "<a href="../Page/Evergreen_(Love_Theme_from_A_Star_Is_Born).md" title="wikilink">Evergreen (Love Theme from A Star Is Born)</a>"</small><br />
<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Musical_or_Comedy.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Musical or Comedy</a><br />
<a href="../Page/Golden_Globe_Award_for_Best_Original_Song.md" title="wikilink">Golden Globe Award for Best Original Song</a> <small>Shared with <a href="../Page/Paul_Williams_(songwriter).md" title="wikilink">Paul Williams</a> (lyrics) for the song "<a href="../Page/Evergreen_(Love_Theme_from_A_Star_Is_Born).md" title="wikilink">Evergreen (Love Theme from A Star Is Born)</a>"</small><br />
提名—<a href="../Page/BAFTA_Award_for_Best_Film_Music.md" title="wikilink">BAFTA Award for Best Film Music</a> <small>Shared with <a href="../Page/Paul_Williams_(songwriter).md" title="wikilink">Paul Williams</a>, <a href="../Page/Kenny_Ascher.md" title="wikilink">Kenny Ascher</a>, <a href="../Page/Rupert_Holmes.md" title="wikilink">Rupert Holmes</a>, <a href="../Page/Leon_Russell.md" title="wikilink">Leon Russell</a>, <a href="../Page/Kenny_Loggins.md" title="wikilink">Kenny Loggins</a>, <a href="../Page/Alan_Bergman.md" title="wikilink">Alan Bergman</a>, <a href="../Page/Marilyn_Bergman.md" title="wikilink">Marilyn Bergman</a>, Donna Weiss</small></p></td>
</tr>
<tr class="odd">
<td><p>1979</p></td>
<td><p>《》</p></td>
<td><p>Hillary Kramer</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1981</p></td>
<td><p>《》</p></td>
<td><p>Cheryl Gibbons</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1983</p></td>
<td><p>《》</p></td>
<td><p>Yentl/Anshel</p></td>
<td><p>(also director and producer)<br />
<a href="../Page/Golden_Globe_Award_for_Best_Director.md" title="wikilink">Golden Globe Award for Best Director</a><br />
<a href="../Page/Nastro_d&#39;Argento_Best_New_Director.md" title="wikilink">Nastro d'Argento for Best New Foreign Director</a><br />
提名—<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Musical_or_Comedy.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Musical or Comedy</a></p></td>
</tr>
<tr class="even">
<td><p>1987</p></td>
<td><p>《<a href="../Page/NUTS.md" title="wikilink">我要求審判</a>》</p></td>
<td><p>Claudia Faith Draper</p></td>
<td><p>提名—<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Drama.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Drama</a></p></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td><p>《<a href="../Page/潮浪王子.md" title="wikilink">潮浪王子</a>》</p></td>
<td><p>Dr. Susan Lowenstein</p></td>
<td><p>(also director and producer)<br />
提名—<a href="../Page/Academy_Award_for_Best_Picture.md" title="wikilink">Academy Award for Best Picture</a> <small>Shared with Andrew S. Karsch</small><br />
提名—<a href="../Page/Directors_Guild_of_America_Award.md" title="wikilink">Directors Guild of America Award</a><br />
提名—<a href="../Page/Golden_Globe_Award_for_Best_Director.md" title="wikilink">Golden Globe Award for Best Director</a></p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p>《》</p></td>
<td><p>Rose Morgan</p></td>
<td><p>(also director and producer)<br />
提名—<a href="../Page/Academy_Award_for_Best_Original_Song.md" title="wikilink">Academy Award for Best Original Song</a> <small>Shared with <a href="../Page/Marvin_Hamlisch.md" title="wikilink">Marvin Hamlisch</a>, <a href="../Page/Robert_John_Lange.md" title="wikilink">Robert John Lange</a> and <a href="../Page/Bryan_Adams.md" title="wikilink">Bryan Adams</a> for the song "<a href="../Page/I_Finally_Found_Someone.md" title="wikilink">I Finally Found Someone</a>"</small><br />
提名—<a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Motion_Picture_Musical_or_Comedy.md" title="wikilink">Golden Globe Award for Best Actress – Motion Picture Musical or Comedy</a><br />
提名—<a href="../Page/Golden_Globe_Award_for_Best_Original_Song.md" title="wikilink">Golden Globe Award for Best Original Song</a> <small>Shared with <a href="../Page/Marvin_Hamlisch.md" title="wikilink">Marvin Hamlisch</a>, <a href="../Page/Robert_John_Lange.md" title="wikilink">Robert John Lange</a> and <a href="../Page/Bryan_Adams.md" title="wikilink">Bryan Adams</a> for the song "<a href="../Page/I_Finally_Found_Someone.md" title="wikilink">I Finally Found Someone</a>"</small></p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p>《<a href="../Page/非常外父生擒霍老爺.md" title="wikilink">非常外父生擒霍老爺</a>》</p></td>
<td><p>Roz Focker</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p>《<a href="../Page/非常外父3.md" title="wikilink">非常外父3</a>》</p></td>
<td><p>Roz Focker</p></td>
<td></td>
</tr>
</tbody>
</table>

## 歌唱事业

她一共出品了61张唱片，许多都是她电影的原声带。其中著名的歌曲有"People","The Way We
Were"，"Evergreen"，"(No More Tears) Enough Is Enough" 和"Woman In
Love"。她是美国唱片销量最高的女歌星，仅次于[猫王和](../Page/猫王.md "wikilink")[披头士](../Page/披头士.md "wikilink")。以下数据来自RIAA官网和芭芭拉·史翠珊官网。

美国唱片工会RIAA金/白金唱片认证总数：

　　51张金唱片（专辑）

　　30张白金唱片（专辑）

　　13张超白金唱片（专辑）

　　8张金唱片（单曲）

　　5张白金唱片（单曲）

　　2张超白金唱片（单曲）

　　5张金唱片（音乐录影带）

　　2张白金唱片（音乐录影带）

　　1张超白金唱片（音乐录影带）

　　音乐纪录：

　　美国唱片工会统计：音乐史上最畅销女艺人之首（全美累积专辑销售达7250万张<依据金唱片数量统计>,实际超过8500万张）

　　美国唱片工会统计：音乐史上拥有最多超白金唱片专辑作品（13张）的女艺人

　　美国唱片工会统计：音乐史上拥有最多白金唱片专辑作品（30张）的女艺人

　　美国唱片工会统计：音乐史上十大最畅销艺人乐团排名第8位,唯一女性（第一位为披头士）

　　美国唱片工会统计：音乐史上拥有最多金唱片、白金唱片、超白金唱片总数的女艺人

　　美国唱片工会统计：音乐史上累积金唱片总数排名第2名（总计51张金唱片），超越披头士合唱团，超越滚石合唱团，仅次与猫王

　　美国唱片工会统计：音乐史上唯一在60、70、80、90、2000、2010年代全部缔创排行榜总冠军专辑的艺人

　　美国唱片工会统计：音乐史上排行 TOP 40 进榜总排名亚军（榜首为猫王）

　　美国唱片工会统计：音乐史上排行 TOP 10 进榜总排名季军（榜首仍为猫王，第二名为披头士）

　　美国唱片工会统计：音乐史上累积冠军专辑总数总排名第三（榜首披头士，亚军Jay-Z，季军猫王，并列）

　　美国唱片工会统计：音乐史上拥有最多进榜单曲（93首）艺人总冠军

　　美国唱片工会统计：音乐史上拥有最多 TOP 40 单曲（58首）艺人总冠军

　　美国唱片工会统计：音乐史上单场演唱会票房收入榜首，创了纪录：一千四百六十九万美元：1999年12月31日米高梅豪华花园剧场（MGM
Grand Garden Arena）“记载世纪”演唱会

　　自无声电影开创以来，第一位身兼主演、导演、编剧、制片的女艺人（作品《杨朵》）

　　有史以来，唯一同时拥有奥斯卡奖、金球奖、艾美奖、百老汇东尼奖奖、葛莱美奖、Cable ACE奖、Peabody奖，数项大奖集一身的艺人.

## 历年唱片

  - 1963年 **
  - 1963年 **
  - 1964年 **
  - 1964年 **
  - 1964年 **
  - 1965年 **
  - 1965年 **
  - 1966年 **
  - 1966年 **
  - 1967年 **
  - 1967年 **
  - 1969年 **
  - 1971年 **
  - 1971年 **
  - 1973年 **
  - 1974年 **
  - 1974年 **
  - 1975年 **
  - 1976年 **
  - 1977年 **
  - 1978年 **
  - 1979年 **
  - 1980年 **
  - 1984年 **
  - 1985年 **
  - 1988年 **
  - 1993年 **
  - 1997年 **
  - 1999年 **
  - 2001年 **
  - 2003年 **
  - 2005年 **
  - 2009年 **
  - 2010年 《The Ultimate Collection》
  - 2011年 *[What Matters
    Most](../Page/What_Matters_Most_\(album\).md "wikilink")*

## 政治立场

坚定的[民主党](../Page/民主党.md "wikilink")[左翼人士](../Page/左翼.md "wikilink")，美国[女权主义运动的积极倡导者](../Page/女权主义运动.md "wikilink")。

## 參看

  - [史翠珊效应](../Page/史翠珊效应.md "wikilink")，以她命名的互聯網現象

## 参考

## 外部链接

  -
  - [BarbraNews.com](http://www.barbranews.com/)

  - [Barbra-Archives.com](http://www.barbra-archives.com/)

  -
  -
  -
  -
  -
[Category:金球獎終身成就獎獲得者](../Category/金球獎終身成就獎獲得者.md "wikilink")
[Category:金球獎最佳導演獲得者](../Category/金球獎最佳導演獲得者.md "wikilink")
[Category:奧斯卡最佳女主角獎獲獎演員](../Category/奧斯卡最佳女主角獎獲獎演員.md "wikilink")
[Category:奥斯卡最佳歌曲奖获奖作曲家](../Category/奥斯卡最佳歌曲奖获奖作曲家.md "wikilink")
[Category:艾美獎獲獎者](../Category/艾美獎獲獎者.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:甘迺迪中心榮譽獎得主](../Category/甘迺迪中心榮譽獎得主.md "wikilink")
[Category:葛萊美終身成就獎獲得者](../Category/葛萊美終身成就獎獲得者.md "wikilink")
[Category:葛萊美獎獲得者](../Category/葛萊美獎獲得者.md "wikilink")
[Category:東尼獎得主](../Category/東尼獎得主.md "wikilink")
[Category:全英音樂獎獲得者](../Category/全英音樂獎獲得者.md "wikilink")
[Category:美國女性電影導演](../Category/美國女性電影導演.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國舞台女演員](../Category/美國舞台女演員.md "wikilink")
[Category:美國女歌手](../Category/美國女歌手.md "wikilink")
[Category:美國爵士歌手](../Category/美國爵士歌手.md "wikilink")
[Category:女性爵士歌手](../Category/女性爵士歌手.md "wikilink")
[Category:美國流行音樂歌手](../Category/美國流行音樂歌手.md "wikilink")
[Category:傳統流行音樂歌手](../Category/傳統流行音樂歌手.md "wikilink")
[Category:法語歌手](../Category/法語歌手.md "wikilink")
[Category:次女高音](../Category/次女高音.md "wikilink")
[Category:美國作曲家](../Category/美國作曲家.md "wikilink")
[Category:美国自由主义](../Category/美国自由主义.md "wikilink")
[Category:美國慈善家](../Category/美國慈善家.md "wikilink")
[Category:美國LGBT權利運動家](../Category/美國LGBT權利運動家.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:纽约布鲁克林弗拉特布什人士](../Category/纽约布鲁克林弗拉特布什人士.md "wikilink")
[Category:伊拉兹马斯霍尔高中校友](../Category/伊拉兹马斯霍尔高中校友.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:猶太歌手](../Category/猶太歌手.md "wikilink")
[Category:哥倫比亞唱片旗下藝人](../Category/哥倫比亞唱片旗下藝人.md "wikilink")
[Category:英語電影導演](../Category/英語電影導演.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.