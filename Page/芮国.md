**芮国**（芮，[汉语拼音ruì](../Page/汉语拼音.md "wikilink")，同瑞），是[西周](../Page/西周.md "wikilink")、[春秋时期的一个诸侯国](../Page/春秋时期.md "wikilink")。

前11世纪，[周武王把卿士](../Page/周武王.md "wikilink")[芮伯良夫封在芮邑](../Page/芮伯良夫.md "wikilink")，位置在[山西省](../Page/山西省.md "wikilink")[芮城县](../Page/芮城.md "wikilink")，另有说法在[陕西省](../Page/陕西省.md "wikilink")[大荔县朝邑镇南方](../Page/大荔县.md "wikilink")。

近些年来，在[陕西省](../Page/陕西省.md "wikilink")[韩城梁带村发现了铸有](../Page/韩城.md "wikilink")“芮”字的铜器。\[1\]

[周成王在位时正式建立芮国](../Page/周成王.md "wikilink")，国君被称为芮伯，曾在[周王室担任司徒的职务](../Page/周朝.md "wikilink")。

[春秋时期](../Page/春秋时期.md "wikilink")，[秦穆公灭亡了芮国](../Page/秦穆公.md "wikilink")。记载秦穆公二十年（即公元前640年）灭芮国。而《[路史](../Page/路史.md "wikilink")》记录了秦穆公二年（即公元前658年）灭芮国的说法。

## 芮國君主列表

  - [芮伯良夫](../Page/芮伯良夫.md "wikilink")（周武王、成王時）
  - [芮伯良夫](../Page/芮伯良夫_\(周厉王\).md "wikilink")（周厉王時）
  - [芮伯多父](../Page/芮伯多父.md "wikilink")（《芮伯多父簋》）
  - [芮伯萬](../Page/芮伯萬.md "wikilink")（見《左傳》桓公三年）

## 参见

  - [芮姓](../Page/芮姓.md "wikilink")

<!-- end list -->

  - [春秋时期](../Page/春秋时期.md "wikilink")

<!-- end list -->

  - [周朝](../Page/周朝.md "wikilink")

<!-- end list -->

  - [春秋战国诸侯国列表](../Page/春秋战国诸侯国列表.md "wikilink")

## 参考

1 杨伯峻《春秋左传注》ISBN 7101002625
2 童书业《春秋左传研究》ISBN 7101051448

[category:山西历史](../Page/category:山西历史.md "wikilink")

[R芮](../Category/春秋时期的国家.md "wikilink")

1.   在19号墓中，出土了4件青铜鬲（古代煮饭用的炊器），鬲沿上铸有“内太子”“内公”等金文习语，古时“内”通“芮”