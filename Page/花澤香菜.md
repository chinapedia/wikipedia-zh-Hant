**花澤香菜**（\[1\]\[2\]\[3\]\[4\]），是一名[日本女性藝人](../Page/日本.md "wikilink")、[聲優](../Page/聲優.md "wikilink")、[演員](../Page/演員.md "wikilink")。[大澤事務所所屬](../Page/大澤事務所.md "wikilink")，前[Smile-Monkey所屬](../Page/Smile-Monkey.md "wikilink")。血型是AB型。身高156.7cm，體重45kg\[5\]。

## 人物

  - 兒童演員出身，於2006年被發掘出來配《ZEGAPAIN》的女主角守凪了子，由於其獨特的棒讀大受好評而為人所認知。
  - 在《ZEGAPAIN》演出當時仍不是聲優，在共演的[川澄綾子建議她成為全職聲優後](../Page/川澄綾子.md "wikilink")，移籍到川澄隸屬的大澤事務所，正式踏上聲優之路。到現在[川澄綾子仍經常給她技術指導](../Page/川澄綾子.md "wikilink")
  - 演員身份方面，曾在東京深夜播放的劇集[怨屋本舖飾演里奈的好友青山美香](../Page/怨屋本舖.md "wikilink")。

<!-- end list -->

  -
    由於是兒童演員出身，所以和一般傳統聲優不同是採用自然聲而非假聲配音，而且能配一些很平實不做作的角色，但亦能配出很動畫化、很誇張的萌音。

<!-- end list -->

  - 以個人風格而非演技多變聞名的聲優，比起動畫化的演出、那種自然的棒讀風反而讓觀眾印象更深刻。
  - 花澤的演出沒有固定類型，由少女向到青年向或兒童向節目都可聽到她的演出。
  - 經常在作品中和比她年長很多的一線聲優大演對手戲，其中女的包括[川澄綾子](../Page/川澄綾子.md "wikilink")、[澤城美雪](../Page/澤城美雪.md "wikilink")、[渡邊明乃](../Page/渡邊明乃.md "wikilink")、[皆川純子](../Page/皆川純子.md "wikilink")、[田村由香里](../Page/田村由香里.md "wikilink")、[堀江由衣](../Page/堀江由衣.md "wikilink")、[水樹奈奈](../Page/水樹奈奈.md "wikilink")、[折笠富美子](../Page/折笠富美子.md "wikilink")、[桑島法子](../Page/桑島法子.md "wikilink")、[中原麻衣](../Page/中原麻衣.md "wikilink")、[能登麻美子](../Page/能登麻美子.md "wikilink")、[生天目仁美](../Page/生天目仁美.md "wikilink")、[齋賀光希](../Page/齋賀光希.md "wikilink")、[茅野爱衣](../Page/茅野爱衣.md "wikilink")、[日笠陽子](../Page/日笠陽子.md "wikilink")、。男的則有[神谷浩史](../Page/神谷浩史.md "wikilink")、[小野大辅](../Page/小野大辅.md "wikilink")、[諏訪部順一](../Page/諏訪部順一.md "wikilink")、[宫野真守](../Page/宫野真守.md "wikilink")、[豐永利行](../Page/豐永利行.md "wikilink")、[木內秀信](../Page/木內秀信.md "wikilink")、[中村悠一](../Page/中村悠一.md "wikilink")、[鳥海浩輔](../Page/鳥海浩輔.md "wikilink")、[石田彰](../Page/石田彰.md "wikilink")、[樱井孝宏](../Page/樱井孝宏.md "wikilink")、[梶裕貴](../Page/梶裕貴.md "wikilink")、[木村良平](../Page/木村良平.md "wikilink")、[冈本信彦等等](../Page/冈本信彦.md "wikilink")。當中合作最多則是[神谷浩史](../Page/神谷浩史.md "wikilink")。
  - 在短短的數年之間已和近十年的第一線聲優都差不多合作過了，而且經常演出需要相當演技的吃重角色。
  - 被訪問時本來想答得婉轉一些但結果卻常出現答非所問的笑話，甚至在電台節目中也常說出牛頭不對馬嘴的怪話。
  - 由於多部作品都演姊妹，所以和[戶松遙感情很好](../Page/戶松遙.md "wikilink")，但有趣是入行时间與實際年紀花澤都比戶松早和大，但動畫中戶松总是演姊姊。另外和[辻步美感情也很好](../Page/辻步美.md "wikilink")。
  - 愛發白日夢，因而被[中世明日香和](../Page/中世明日香.md "wikilink")[牧野由依評為](../Page/牧野由依.md "wikilink")「實寫版梶原空」。
  - 2007年開始是大學生，到2011年為止都是半工半讀。
  - 曾有過偷飲母親的酒而變成吻魔到處吻人的事件，因此現在不喝酒。
  - 曾於C3日本動玩博覽2012 DAY3（2012年3月11日）訪問[香港](../Page/香港.md "wikilink")。
  - 2012年2月宣佈以個人歌手名義出道，唱片公司為[Aniplex](../Page/Aniplex.md "wikilink")，4月25日推出首張单曲《[星空☆終點站](../Page/星空☆終點站.md "wikilink")（）》。
  - 《[Angel
    Beats\!](../Page/Angel_Beats!.md "wikilink")》裡的立华奏一角是[麻枝准个人的强烈要求](../Page/麻枝准.md "wikilink")，他在之后访问中提到：“天使的声音只能是这样，没有别的选择。”
  - 2015年5月3日於武道館舉辦個人演唱會，花澤香菜也是繼聲優歌姬的前輩們第七位登上武道館獻唱的女聲優，登館順序為：[椎名碧流](../Page/椎名碧流.md "wikilink")、[水樹奈奈](../Page/水樹奈奈.md "wikilink")、[田村由香里](../Page/田村由香里.md "wikilink")、[堀江由衣](../Page/堀江由衣.md "wikilink")、[坂本真綾](../Page/坂本真綾.md "wikilink")、[茅原實里等六員女聲優](../Page/茅原實里.md "wikilink")。
  - 近年也開始配些搞笑的角色的聲線，如[廢天使加百列等](../Page/廢天使加百列.md "wikilink")，讓習慣聽花澤較為「正經」聲線的粉絲直呼**「花澤崩壞！」**
  - 2017年2月19日，有相關報導證實與[小野賢章是戀人關係](../Page/小野賢章.md "wikilink")，並同居於東京港區某公寓。本人亦於2017年2月22日在放送中承認交往。
  - 兒童演員時期看過少年漫畫《[鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")》、《[通靈王](../Page/通靈王.md "wikilink")》的改編電視動畫，因此曾經公開自己是這2部作品的粉絲\[6\]。

## 交友關係

與[竹達彩奈自從在](../Page/竹達彩奈.md "wikilink")《[我的妹妹哪有這麼可愛！](../Page/我的妹妹哪有這麼可愛！.md "wikilink")》中合作之後，2人交情突飛猛進。因此，竹達曾在2013年6月23日舉辦的慶生活動「“apple
symphony”the Birthday」中幫花澤慶生，並給她驚喜和發表慶生祝福的話\[7\]。

與[井上麻里奈都是新人時期開始](../Page/井上麻里奈.md "wikilink")，從《[ZEGAPAIN](../Page/ZEGAPAIN.md "wikilink")》、《[月面兔兵器米娜](../Page/月面兔兵器米娜.md "wikilink")》至《[魔法使的條件
夏日的天空](../Page/魔法使的條件.md "wikilink")》等作品經常合作至今，因此2人私底下也有不錯的交情\[8\]。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 2003年

<!-- end list -->

  - [最後流亡](../Page/最後流亡.md "wikilink")（荷莉·麥德賽）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [ZEGAPAIN](../Page/ZEGAPAIN.md "wikilink")（**守凪了子**）
  - [大魔法峠](../Page/大魔法峠.md "wikilink")（田中萤）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [月面兔兵器米娜](../Page/月面兔兵器米娜.md "wikilink")（羽蟬奈琉）
  - [Sketchbook ～full
    color's～](../Page/Sketchbook.md "wikilink")（**梶原空**）
  - [暮蟬悲鳴時解](../Page/暮蟬悲鳴時.md "wikilink")（智美）
  - [-{土豆}-蛋黃醬](../Page/土豆蛋黃醬.md "wikilink")（）（**-{土豆}-蛋黃醬**）
  - [蟲之歌](../Page/蟲之歌.md "wikilink")（**杏本詩歌** / **冬螢** / **飛雪**）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [神槍少女 -IL TEATRINO-](../Page/神槍少女.md "wikilink")（**安潔麗卡**）
  - [神薙](../Page/神薙.md "wikilink")（**懺悔妹妹（）/ 涼城白亞**）
  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（**亂崎優歌/姬宮零子**）
  - [鋼鐵新娘](../Page/鋼鐵新娘.md "wikilink")（リコ·A·アンドロイズ）
  - [Strike Witches](../Page/Strike_Witches.md "wikilink")（諏訪天姬）
  - [出包王女](../Page/出包王女.md "wikilink")（**結城美柑**）
  - [鶺鴒女神](../Page/鶺鴒女神.md "wikilink")（**草野**）
  - [BLASSREITER](../Page/BLASSREITER.md "wikilink")（艾萊亞）
  - [魔法使的條件 夏季的天空](../Page/魔法使的條件.md "wikilink")（**鈴木空**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - （路易絲）

  - [青花](../Page/青花_\(漫畫\).md "wikilink")（王子、孩子們、初等部A）

  - [明日的與一\!](../Page/明日的與一!.md "wikilink")（**斑鳩籬**）

  - [迷宮塔](../Page/迷宮塔.md "wikilink")（**艾納羅**）

  - [浪漫追星社](../Page/浪漫追星社.md "wikilink")（杏幸江）

  - [BASQUASH\!](../Page/BASQUASH!.md "wikilink")（**可可·JD**）

  - [潘朵拉之心](../Page/潘朵拉之心.md "wikilink")（**夏蘿·蘭茲華斯**）

  - [化物語](../Page/化物語.md "wikilink")（**千石撫子**）

  - [奇蹟少女KOBATO.](../Page/奇蹟少女KOBATO..md "wikilink")（**花戶小鳩**）

  - [Darker Than Black
    -流星的雙子-](../Page/DARKER_THAN_BLACK.md "wikilink")（**蘇芳·帕布利切柯**）

  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink")（春上衿衣）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [無頭騎士異聞錄
    DuRaRaRa\!\!](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**園原杏里**）
  - [聖痕煉金士](../Page/聖痕煉金士.md "wikilink")（御手洗史伽）
  - [學生會長是女僕](../Page/學生會長是女僕.md "wikilink")（**花園櫻**、小千）
  - [Angel Beats\!](../Page/Angel_Beats!.md "wikilink")（**天使（立華奏）**）
  - [管家後宮學園](../Page/管家後宮學園.md "wikilink")（艾謝·哈蒂姆）
  - [鶺鴒女神〜Pure Engagement〜](../Page/鶺鴒女神.md "wikilink")（**草野**）
  - [B型H系](../Page/B型H系.md "wikilink")（**宮野真由**）
  - [我的妹妹哪有這麼可愛！](../Page/我的妹妹哪有這麼可愛！.md "wikilink")（**黑貓（五更琉璃）**、五更日向）
  - [世紀末超自然學院](../Page/世紀末超自然學院.md "wikilink")（成瀨梢）
  - [玩伴貓耳娘](../Page/玩伴貓耳娘.md "wikilink")（**雙葉葵**）
  - [海月姫](../Page/海月姫.md "wikilink")（**蒼下月海**）
  - [只有神知道的世界](../Page/只有神知道的世界.md "wikilink")（**汐宮栞**）
  - [半妖少女 石榴](../Page/半妖少女綺麗譚.md "wikilink")（**薄螢**）
  - [更多出包王女](../Page/出包王女.md "wikilink")（**結城美柑**）
  - [聖誕之吻SS](../Page/聖誕之吻.md "wikilink")（圖書管理員）
  - [強襲魔女2](../Page/強襲魔女.md "wikilink")（諏訪天姬）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [零度戰姬](../Page/結界女王.md "wikilink")（**拉娜·林沁**）
  - [IS〈Infinite
    Stratos〉](../Page/IS〈Infinite_Stratos〉.md "wikilink")（**夏綠蒂·迪努亞**）
  - [Fractale](../Page/Fractale.md "wikilink")（**奈莎**）
  - [如果高校棒球女子經理讀了彼得·杜拉克](../Page/如果高校棒球女子經理讀了彼得·杜拉克.md "wikilink")（**宮田夕紀**）
  - [命運石之門](../Page/命運石之門.md "wikilink")（**椎名真由里**）
  - [青之驅魔師](../Page/青之驅魔師.md "wikilink")（**杜山詩惠美**）
  - [死囚樂園](../Page/死囚樂園.md "wikilink")（**小白**）
  - [變研會](../Page/變研會.md "wikilink")（**松隆奈奈子**）
  - [DOG DAYS](../Page/DOG_DAYS.md "wikilink")（諾瓦爾·比諾卡卡歐）
  - [聖痕鍊金士II](../Page/聖痕鍊金士.md "wikilink")（御手洗史伽）
  - [迷茫管家與膽怯的我](../Page/迷茫管家與膽怯的我.md "wikilink")（**坂町紅羽**）
  - [蘿球社！](../Page/蘿球社！.md "wikilink")（**湊智花**）
  - [沉默的森田同學](../Page/沉默的森田同學.md "wikilink")（**森田真由**）
  - [神樣DOLLS](../Page/神樣DOLLS.md "wikilink")（日向真晝）
  - [電波女&青春男](../Page/電波女&青春男.md "wikilink")（花澤）
  - [偶像大師](../Page/偶像大師.md "wikilink")（水谷繪理）
  - [丹特麗安的書架](../Page/丹特麗安的書架.md "wikilink")（修伊（幼年時代））
  - [我的朋友很少](../Page/我的朋友很少.md "wikilink")（**羽瀨川小鳩**）
  - [最後流亡-銀翼的飛夢-](../Page/最後流亡-銀翼的飛夢-.md "wikilink")（艾兒薇絲·漢彌爾敦）
  - [罪惡王冠](../Page/罪惡王冠.md "wikilink")（**篠宮綾瀨**、寒川-{谷}-尋（幼少期））

<!-- end list -->

  - 2012年

<!-- end list -->

  - [猛烈宇宙海賊](../Page/迷你裙宇宙海賊.md "wikilink")（**栗原千秋**）
  - [妖狐×僕SS](../Page/妖狐×僕SS.md "wikilink")（**髏髏宮加留多**）
  - [偽物語](../Page/偽物語.md "wikilink")（千石撫子）
  - [創聖機械天使EVOL](../Page/創聖機械天使EVOL.md "wikilink")（**婕希卡·王**）
  - [機動戰士GUNDAM AGE](../Page/機動戰士GUNDAM_AGE.md "wikilink")（**羅瑪麗·斯通**）
  - [BLACK★ROCK
    SHOOTER](../Page/BLACK★ROCK_SHOOTER.md "wikilink")（**黑衣麻陶**）
  - [咲-Saki-](../Page/咲-Saki-.md "wikilink") 阿知賀編（**松實玄**）
  - [ZETMAN](../Page/ZETMAN.md "wikilink")（**天城小葉**）
  - [紙箱戰機W](../Page/紙箱戰機W.md "wikilink")（**花咲蘭**）
  - [Campione 弒神者！](../Page/Campione_弒神者！.md "wikilink")（**萬里谷祐理**）
  - [SKET DANCE](../Page/SKET_DANCE.md "wikilink")（**安形紗綾**）
  - [白熊咖啡廳](../Page/白熊咖啡廳.md "wikilink")（熊貓妹妹）
  - [戰國Collection](../Page/戰國Collection.md "wikilink")（**“泰平女君”德川家康**）
  - [無賴勇者的鬼畜美學](../Page/無賴勇者的鬼畜美學.md "wikilink")（**桐元葛葉**）
  - [窮神](../Page/窮神_\(漫畫\).md "wikilink")（**櫻市子**）
  - [DOG DAYS'](../Page/DOG_DAYS.md "wikilink")（諾瓦爾·比諾卡卡歐）
  - [鄰座的怪同學](../Page/鄰座的怪同學.md "wikilink")（大島千鶴）
  - [來自新世界](../Page/來自新世界.md "wikilink")（**秋月真理亞**）
  - [PSYCHO-PASS心靈判官](../Page/PSYCHO-PASS.md "wikilink")（**常守朱**）
  - [絕園的暴風雨](../Page/絕園的暴風雨.md "wikilink")（**不破愛花**）
  - [出包王女DARKNESS](../Page/出包王女.md "wikilink")（**結城美柑**）
  - [超譯百人一首戀歌](../Page/超譯百人一首戀歌.md "wikilink")（當子内親王）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（练红玉）
  - [女高网球部](../Page/女高网球部.md "wikilink")（**板东鞠萌**）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [鎖鎖美小姐@不好好努力](../Page/鎖鎖美小姐@不好好努力.md "wikilink")（**邪神鏡**）
  - [我的朋友很少NEXT](../Page/我的朋友很少.md "wikilink")（**羽瀨川小鳩**）
  - [琴浦小姐](../Page/琴浦小姐.md "wikilink")（**御舟百合子**）
  - [我的妹妹哪有這麼可愛。](../Page/我的妹妹哪有這麼可愛！.md "wikilink")（**黑貓（五更琉璃）**、五更日向）
  - [科學超電磁砲S](../Page/科學超電磁砲.md "wikilink")（春上衿衣）
  - [絕對防衛利維坦](../Page/絕對防衛利維坦.md "wikilink")（**賽洛普**）
  - [Little Busters\!](../Page/Little_Busters!.md "wikilink")（杉并睦实）
  - [蘿球社！SS](../Page/蘿球社！.md "wikilink")（**湊智花**）
  - [物語系列 第二季](../Page/物語系列_第二季.md "wikilink")（**千石撫子**）
  - [女高網球部 第二季](../Page/女高網球部.md "wikilink")（**板東鞠萌**）
  - [只有神知道的世界 女神篇](../Page/只有神知道的世界.md "wikilink")（**汐宮栞／彌涅兒娃**）
  - [我不受歡迎，怎麼想都是你們的錯！](../Page/我不受歡迎，怎麼想都是你們的錯！.md "wikilink")（**成瀨優**）
  - [超次元戰記 戰機少女](../Page/超次元戰記_戰機少女.md "wikilink")（**普露露特／愛莉絲之心**）
  - [核爆末世錄](../Page/核爆末世錄.md "wikilink")（**深作葵**）
  - [來自風平浪靜的明天](../Page/來自風平浪靜的明天.md "wikilink")（**向井戶愛花**）
  - [IS〈Infinite
    Stratos〉2](../Page/IS〈Infinite_Stratos〉.md "wikilink")（**夏綠蒂·迪努亞**）
  - [結界女王 震動](../Page/結界女王.md "wikilink")（**拉娜·林沁**）
  - [魔奇少年 The Kingdom of Magic](../Page/魔奇少年.md "wikilink")（練紅玉）
  - [我要成為世界最強偶像](../Page/我要成為世界最強偶像.md "wikilink")（真田朱里）
  - [機巧少女不會受傷](../Page/機巧少女不會受傷.md "wikilink")（火垂）
  - [東京闇鴉](../Page/東京闇鴉.md "wikilink")（**土御門夏目**）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [BUDDY COMPLEX](../Page/BUDDY_COMPLEX.md "wikilink")（**奈須真由佳**）
  - [宇宙浪子](../Page/宇宙浪子.md "wikilink")（雅德莉）
  - [咲-Saki-](../Page/咲-Saki-.md "wikilink") 全國篇（松實玄）
  - [鄰座同學是怪咖](../Page/鄰座同學是怪咖.md "wikilink")（**橫井留美**）
  - [屬性同好會](../Page/屬性同好會.md "wikilink")（**柴崎蘆花**）
  - [農林](../Page/農林.md "wikilink")（**中澤農**）
  - [Wake Up, Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")（安娜）
  - [偽戀](../Page/偽戀.md "wikilink")（**小野寺小咲**）
  - [世界征服 謀略之星](../Page/世界征服_謀略之星.md "wikilink")（**娜塔夏**）
  - [我們大家的河合莊](../Page/我們大家的河合莊.md "wikilink")（**河合律**）
  - [星刻龍騎士](../Page/星刻龍騎士.md "wikilink")（潔西卡·瓦倫泰）
  - [魔法科高中的劣等生](../Page/魔法科高中的劣等生.md "wikilink")（**七草真由美**）
  - [如果折斷她的旗](../Page/如果折斷她的旗.md "wikilink")（**盜賊山惠**）
  - [BREAK BLADE 破刃之劍](../Page/破刃之劍.md "wikilink")（克雷歐·薩布拉夫）
  - [龍孃七七七埋藏的寶藏](../Page/龍孃七七七埋藏的寶藏.md "wikilink")（**星埜達魯克**）
  - [目隱都市的演繹者](../Page/陽炎計劃.md "wikilink")（**小櫻茉莉／Mari**）
  - [東京喰種](../Page/東京喰種.md "wikilink")（**神代利世**）
  - [斬！赤紅之瞳](../Page/斬！赤紅之瞳.md "wikilink")（賽琉）
  - [女神異聞錄4 The Golden](../Page/女神異聞錄4.md "wikilink")（**瑪麗**）
  - [寄生獸 生命的準則](../Page/寄生獸.md "wikilink")（**村野里美**）
  - [境界觸發者](../Page/境界觸發者.md "wikilink")（**木虎藍**）
  - [PSYCHO-PASS心靈判官2](../Page/PSYCHO-PASS.md "wikilink")（**常守朱**）
  - [结城友奈是勇者](../Page/结城友奈是勇者.md "wikilink")（乃木园子）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [東京喰種√A](../Page/東京喰種.md "wikilink")（**神代利世**）
  - [銃皇無盡的法夫納](../Page/銃皇無盡的法夫納.md "wikilink")（**菲莉爾·克雷斯特**）
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2
    承](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**園原杏里**）
  - [DOG DAYS''](../Page/DOG_DAYS.md "wikilink")（諾瓦爾·比諾卡卡歐）
  - [純潔的瑪利亞](../Page/純潔的瑪利亞.md "wikilink")（**以西結**）
  - [電波教師](../Page/電波教師.md "wikilink")（無響零子）
  - [-{zh-tw:庭球社;
    zh-cn:女高网球部;}-（第四季）](../Page/庭球社.md "wikilink")（**板东鞠萌**）
  - [偽戀](../Page/偽戀.md "wikilink")（**小野寺小咲**）
  - [山田君與7人魔女](../Page/山田君與7人魔女.md "wikilink")（飛鳥美琴）
  - [城下町的蒲公英](../Page/城下町的蒲公英.md "wikilink")（**櫻田茜**）
  - [潮與虎](../Page/潮與虎.md "wikilink")（決眉）
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2
    轉](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**園原杏里**）
  - [科學小飛俠 Crowds Insight](../Page/科學小飛俠Crowds.md "wikilink")（**杜蘭莎**）
  - [出包王女DARKNESS 2nd](../Page/出包王女.md "wikilink")（**結城美柑**）
  - [監獄學園](../Page/監獄學園.md "wikilink")（**綠川花**）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [疾走王子Alternative](../Page/疾走王子.md "wikilink")（**櫻井奈奈**）
  - [少女們向荒野進發](../Page/少女們向荒野進發.md "wikilink")（**小早川夕夏**）
  - [大叔與棉花糖](../Page/大叔與棉花糖.md "wikilink")（**MIO5**）
  - [FAIRY TAIL魔導少年 ZERØ](../Page/FAIRY_TAIL.md "wikilink")（**澤菈**）
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2
    結](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**園原杏里**）
  - [她與她的貓 -Everything Flows-](../Page/她與她的貓.md "wikilink")（**她**）
  - [潮與虎](../Page/潮與虎.md "wikilink") 第2期（潔梅）
  - [文豪野犬](../Page/文豪野犬.md "wikilink")（[露西·莫德·蒙哥馬利](../Page/露西·莫德·蒙哥馬利.md "wikilink")）
  - [境界之輪迴](../Page/境界之輪迴.md "wikilink") 第2期（琪比）
  - [Rewrite](../Page/Rewrite.md "wikilink")（**篝**）
  - [orange橘色奇蹟](../Page/orange橘色奇蹟.md "wikilink")（**高宮菜穗**）
  - [聖潔天使](../Page/聖潔天使.md "wikilink")（CODEΩ46塞尼亞）
  - [槍彈辯駁3 －The End of
    希望峰學園－](../Page/槍彈辯駁3_－The_End_of_希望峰學園－.md "wikilink")
    絕望篇（**七海千秋**）
  - 槍彈辯駁3 －The End of 希望峰學園－ 希望篇（**七海千秋**）
  - [終末的伊澤塔](../Page/終末的伊澤塔.md "wikilink")（**愛爾薇拉**）
  - [3月的獅子](../Page/3月的獅子.md "wikilink")（**川本日向**、三色貓）
  - [烏龍麵之國的金色毛毬](../Page/烏龍麵之國的金色毛毬.md "wikilink")（藤山紗枝、 小刺猬2、烏鴉3、旁白）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [超·少年偵探團NEO](../Page/超·少年偵探團NEO.md "wikilink")（謎之女／**貓婦人**）
  - [學園少女突襲者 Animation Channel](../Page/學園少女突襲者.md "wikilink")（**沙島悠水**）
  - [青之驅魔師](../Page/青之驅魔師.md "wikilink") 京都不淨王篇（**杜山詩惠美**）
  - [廢天使加百列](../Page/廢天使加百列.md "wikilink")（**白羽·拉非爾·恩茲沃斯**）
  - Rewrite 2nd season -Moon篇 Terra篇-（**篝**）
  - [重啟咲良田](../Page/重啟咲良田.md "wikilink")（**春埼美空**）
  - [情色漫畫老師](../Page/情色漫畫老師.md "wikilink")（黑貓／五更日向）
  - [戀愛與謊言](../Page/戀愛與謊言.md "wikilink")（**高崎美咲**）
  - [徒然喜歡你](../Page/徒然喜歡你.md "wikilink")（**皆川由紀**）
  - [天使的3P！](../Page/天使的3P！.md "wikilink")（尾城小梅、霧夢〈貴龍〉）
  - [Infini-T Force](../Page/Infini-T_Force.md "wikilink")（貝爾·琳）
  - [食戟之靈](../Page/食戟之靈.md "wikilink") 餐之皿（紀之國寧寧）
  - [少女終末旅行](../Page/少女終末旅行.md "wikilink")（Nuko）
  - 結城友奈是勇者 -鷲尾須美之章-／-勇者之章-（**乃木園子**）
  - [動畫同好會](../Page/動畫同好會.md "wikilink")（楊貝貝）
  - Wake Up, Girls\! 新章（安娜）
  - 3月的獅子 第2期（**川本日向**、三色貓）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [比宇宙還遠的地方](../Page/比宇宙還遠的地方.md "wikilink")（**小淵澤報瀨**）

  - [沒有心跳的少女 BEATLESS](../Page/沒有心跳的少女_BEATLESS.md "wikilink")（堤美佳）

  - [棒球大聯盟2nd](../Page/棒球大聯盟.md "wikilink")（**佐倉睦子**）

  - [重神機潘多拉](../Page/重神機潘多拉.md "wikilink")（**葉坤靈**）

  - [溫泉屋小女將](../Page/溫泉屋小女將.md "wikilink")（**稻田繪里香**）

  - [雷頓神秘偵探社
    ～卡特莉的解謎事件簿～](../Page/雷頓的神秘之旅：卡翠愛兒與大富翁的陰謀.md "wikilink")（**卡翠愛兒·雷頓**）\[9\]

  - [命運石之門0](../Page/命運石之門0.md "wikilink")（**椎名真由里**）

  - [中間管理錄利根川](../Page/中間管理錄利根川.md "wikilink")（聲〈005〉）

  - [工作細胞](../Page/工作細胞.md "wikilink")（**紅血球**／紅血球母細胞）

  - （**白鹿瞳**）

  - [Happy Sugar
    Life](../Page/Happy_Sugar_Life.md "wikilink")（**松坂砂糖**\[10\]）

  - Devidol！（**花**）

  - [東京食屍鬼:re 第2期](../Page/東京喰種.md "wikilink")（**神代利世**）

  - [叛逆性百萬亞瑟王](../Page/擴散性百萬亞瑟王.md "wikilink")（**加拉哈德**）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [不吉波普不笑](../Page/不吉波普系列.md "wikilink")（**水乃星透子**）
  - [五等分的新娘](../Page/五等分的新娘.md "wikilink")（**中野一花**\[11\]）
  - [消滅都市](../Page/消滅都市.md "wikilink")（**小雪**\[12\]）
  - [川柳少女](../Page/川柳少女.md "wikilink")（**雪白七七子**）
  - [MIX](../Page/MIX_\(漫畫\).md "wikilink")（**大山春夏**）
  - [约会大作战](../Page/约会大作战.md "wikilink")（？？？（崇宫澪））
  - [文豪野犬 第三季](../Page/文豪野犬.md "wikilink")(**露西·莫德·蒙哥馬利**)
  - [PSYCHO-PASS心靈判官 3](../Page/PSYCHO-PASS.md "wikilink")（**常守朱**）

### OVA

**2006年**

  - [大魔法峠](../Page/大魔法峠.md "wikilink")（）

**2008年**

  - [世外魔島～黑月之王與雙月公主～](../Page/世外魔島～黑月之王與雙月公主～.md "wikilink")（瑪波·馬修·馬爾薩斯）

**2009年**

  - [出包王女](../Page/出包王女.md "wikilink")（**結城美柑**）
  - [文學少女今日的點心](../Page/文學少女.md "wikilink")「初戀」（**天野遠子**）

**2010年**

  - [BLACK★ROCK
    SHOOTER](../Page/BLACK★ROCK_SHOOTER.md "wikilink")（**黑衣麻陶**）
  - 文學少女回憶篇I 天野遠子篇 「夢想少女的前奏曲」（**天野遠子**）
  - 文學少女回憶篇III 琴吹七瀨篇 「戀愛少女的狂想曲」（**天野遠子**）
  - [變研會](../Page/變研會.md "wikilink")（**松隆奈奈子**）
  - [聖痕鍊金士～女帝的肖像～](../Page/聖痕鍊金士.md "wikilink")（御手洗史伽）
  - [眼鏡女友](../Page/眼鏡女友.md "wikilink")（**倉本千秋**）
  - [.hack//Quantum](../Page/.hack/Quantum.md "wikilink")（**咲夜**）

**2011年**

  - [變研會](../Page/變研會.md "wikilink")（**松隆奈奈子**）
  - [沉默的森田同學](../Page/沉默的森田同學.md "wikilink")（**森田真由**）
  - [Baby Princess 3D 嘉年華
    0](../Page/Baby_Princess_3D_嘉年華_0.md "wikilink")（**觀月**）
  - [IS〈Infinite Stratos〉Encore
    愛情戰火六重奏](../Page/IS〈Infinite_Stratos〉.md "wikilink")（**夏綠蒂·迪諾亞**）
  - [Fate/Prototype](../Page/Fate/Prototype.md "wikilink")（**沙條綾香**）

**2012年**

  - [流星LENS](../Page/流星LENS.md "wikilink")（**花籠凜咲**）

**2013年**

  - [蘿球社！](../Page/蘿球社！.md "wikilink")（**湊智花**）
    ※PSP遊戲『蘿球社！遺落物的祕密』限定版同梱DVD。
  - [机动战士GUNDAM AGE MEMORY OF
    EDEN](../Page/機動戰士鋼彈AGE.md "wikilink")（**罗玛丽·斯通**）
  - [鄰座的怪同學 鄰座的極道同學](../Page/鄰座的怪同學.md "wikilink")（千鶴公主） ※漫畫12卷附贈DVD特裝版

**2014年**

  - [我不受歡迎，怎麼想都是你們的錯！](../Page/我不受歡迎，怎麼想都是你們的錯！.md "wikilink")（**成瀨優**）

**2015年**

  - [無頭騎士異聞錄 DuRaRaRa\!\!×2 承 外傳！？ 第4.5話
    我的心是火鍋的形狀](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**園原杏里**）※劇場先行上映\[13\]

**2016年**

  - [監獄學園](../Page/監獄學園.md "wikilink")（**綠川花**）
  - [少女們向荒野進發](../Page/少女們向荒野進發.md "wikilink")（**小早川夕夏**）
  - [FAIRY TAIL 納茲vs.梅比斯](../Page/FAIRY_TAIL_\(動畫\).md "wikilink")（澤菈）

**2017年**

  - [廢天使加百列](../Page/廢天使加百列.md "wikilink")（**拉非爾**）
  - [食戟之靈](../Page/食戟之靈.md "wikilink")「遠月十傑」（紀之國寧寧） ※漫畫第25卷OAD同捆版

### 動畫電影

**2009年**

  - CENCOROLL（**雪**）

**2010年**

  - 古城荊棘王（）
  - [破刃之劍 劇場版](../Page/破刃之劍.md "wikilink")（**克雷歐·薩布拉夫**）
  - [文學少女 劇場版](../Page/文學少女.md "wikilink")（**天野遠子**）

**2012年**

  - [青之驱魔师 ―劇場版―](../Page/青之驱魔师.md "wikilink")（**杜山詩繪美**）
  - [劇場版閃電十一人GO vs
    紙箱戰機W](../Page/閃電十一人GO_VS_紙箱戰機W.md "wikilink")（**花咲蘭**）
  - [BLOOD-C劇場版](../Page/BLOOD-C.md "wikilink") The Last Dark（月山比呂）
  - [被狙擊的學園](../Page/被狙擊的學園.md "wikilink")（春河佳惠梨）

**2013年**

  - [AURA
    ～魔龍院光牙最後的戰鬥～](../Page/AURA_～魔龍院光牙最後的戰鬥～.md "wikilink")（**佐藤良子**\[14\]）
  - [命運石之門
    負荷領域的既視感](../Page/命運石之門_負荷領域的既視感.md "wikilink")（**椎名真由里**）\[15\]
  - [言葉之庭](../Page/言葉之庭.md "wikilink")（**雪野百香里**）

**2014年**

  - [猛烈宇宙海賊 ABYSS OF HYPERSPACE
    -超空間的深淵-](../Page/迷你裙宇宙海賊.md "wikilink")（**栗原千秋**）
  - [Wake Up, Girls\! 七位偶像](../Page/Wake_Up,_Girls!.md "wikilink")（安娜）

**2015年**

  - [劇場版 PSYCHO-PASS](../Page/心理測量者劇場版.md "wikilink")（**常守朱**）
  - [屍者的帝國](../Page/屍者的帝國.md "wikilink")（**哈達莉·莉莉絲**）
  - [電影 Go！Princess 光之美少女
    Go！Go！！豪華三部曲！！！](../Page/Go！Princess_光之美少女#電影_Go！Princess_光之美少女_Go！Go！！豪華三部曲！！！.md "wikilink")（潘普露露公主）
  - [Wake Up, Girls\! Beyond the
    Bottom](../Page/Wake_Up,_Girls!.md "wikilink")（安娜）

**2016年**

  - [遊戲王：次元的黑暗面](../Page/遊戲王：次元的黑暗面.md "wikilink")（莎拉）
  - [你的名字。](../Page/你的名字。.md "wikilink")（小雪老師）
  - [ZEGAPAIN](../Page/ZEGAPAIN.md "wikilink")（**守凪了子**）

**2017年**

  - [結城友奈是勇者](../Page/結城友奈是勇者.md "wikilink")
    [-鷲尾須美之章-](../Page/鷲尾須美是勇者.md "wikilink")（**乃木園子**\[16\]）
  - [春宵苦短，少女前進吧！](../Page/春宵苦短，少女前進吧！.md "wikilink")（**黑髮少女**\[17\]）
  - [特工次時代](../Page/特工次時代.md "wikilink")（**希波**\[18\]）
  - [劇場版 魔法科高中的劣等生 呼喚繁星的少女](../Page/魔法科高中的劣等生.md "wikilink")（**七草真由美**）
  - [煙花](../Page/煙花_\(劇場版\).md "wikilink")（三浦老師）
  - [GODZILLA -怪獸惑星-](../Page/GODZILLA_-怪獸惑星-.md "wikilink")（**優子·谷**）

**2018年**

  - [續·終物語](../Page/續·終物語.md "wikilink")(**千石撫子**)
  - [GODLIZAR
    -決戰機動增殖都市-](../Page/GODLIZAR_-決戰機動增殖都市-.md "wikilink")(**谷優子**)
  - [GODLIZAR -嗜星者-](../Page/GODLIZAR_-嗜星者-.md "wikilink")(**谷優子**)
  - [阿拉涅的蟲籠](../Page/阿拉涅的蟲籠.md "wikilink")（**凜**）
  - [溫泉小女將](../Page/溫泉小女將.md "wikilink")(**關峰子**)
  - [薄暮](../Page/薄暮.md "wikilink")

**2019年**

  - [PSYCHO-PASS 劇場版
    罪と罰](../Page/PSYCHO-PASS_劇場版_罪と罰.md "wikilink")(**常守朱**)
  - [PSYCHO-PASS 劇場版 First
    Guardian](../Page/PSYCHO-PASS_劇場版_First_Guardian.md "wikilink")(**常守朱**)
  - [約會大作戰](../Page/約會大作戰.md "wikilink")(**？？？**)

### 網路動畫

  - 2008年

<!-- end list -->

  - [亡念之扎姆德](../Page/亡念之扎姆德.md "wikilink")（柊）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [由土里醬](../Page/由土里醬.md "wikilink")（摘込詩織）

<!-- end list -->

  - 2014年

<!-- end list -->

  - （**ゼウシくん**）

  - [精靈寶可夢 終結紅寶石·初始藍寶石](../Page/精靈寶可夢_終結紅寶石·初始藍寶石.md "wikilink")
    （**女主人公**）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [雛蜂](../Page/雛蜂.md "wikilink")（**琉璃**）
  - [弱酸性百萬亞瑟王](../Page/弱酸性百萬亞瑟王.md "wikilink")（**加拉哈德**）

<!-- end list -->

  - 2016年

<!-- end list -->

  - （**ゼウシくん**）

  - [曆物語](../Page/曆物語.md "wikilink")（**千石撫子**）

  - [怪物彈珠 夏季特別篇](../Page/怪物彈珠.md "wikilink")（**奇斯琪爾麗菈**）

### 遊戲

  - [紅線](../Page/紅線.md "wikilink") DS（中川沙良）
  - [ARIA The ORIGINATION
    ～藍色行星的天空～](../Page/水星領航員.md "wikilink")（**阿涅絲·杜馬（安妮）**）
  - [怪物彈珠麗拉](../Page/怪物彈珠.md "wikilink")
  - [小鳩ヶ丘高校女子ぐろ〜部](../Page/小鳩ヶ丘高校女子ぐろ〜部.md "wikilink")（今川ぱぺ子）
  - [D.C.II P.S. 〜ダ・カーポII〜
    プラスシチュエーション](../Page/初音島II.md "wikilink")（**莉乃**）
  - [デモンブライド](../Page/デモンブライド.md "wikilink")（**皇にぃな**）
  - [乃木坂春香的秘密─開始了，角色扮演](../Page/乃木坂春香的秘密#PS2遊戲.md "wikilink")（八咲雪奈）
  - [Project Witch](../Page/Project_Witch.md "wikilink")（リルテ）
  - [偶像大師 深情之星](../Page/偶像大師_深情之星.md "wikilink")（**水谷繪理**）
  - [偶像大師 灰姑娘女孩](../Page/偶像大師_灰姑娘女孩.md "wikilink")（水谷繪理）
  - [心跳回憶4](../Page/心跳回憶#四代.md "wikilink")（**響野里澄**）
  - [Steins;Gate](../Page/Steins;Gate.md "wikilink")（**椎名真由里**）
  - [グラナド・エスパダ](../Page/グラナド・エスパダ.md "wikilink")（**ヴァレリア**）
  - [美德傳奇](../Page/美德傳奇.md "wikilink")（**蘇菲**）
  - [美德傳奇F](../Page/美德傳奇.md "wikilink")（**蘇菲**）
  - [Rewrite](../Page/Rewrite.md "wikilink")（**篝**）
  - [Rewrite Harvest
    festa\!](../Page/Rewrite_Harvest_festa!.md "wikilink")（**篝**）
  - [次の犠牲者をオシラセシマス](../Page/给下一个牺牲者的死亡通告.md "wikilink")（三上霞）
  - [デュラララ\!\! 3way
    standoff](../Page/デュラララ!!_3way_standoff.md "wikilink")（**園原杏里**）
  - [テイルズ オブ ザ ワールド レディアント
    マイソロジー3](../Page/テイルズ_オブ_ザ_ワールド_レディアント_マイソロジー3.md "wikilink")（蘇菲）
  - [我的妹妹哪有這麼可愛
    攜帶版](../Page/我的妹妹哪有這麼可愛.md "wikilink")（**黒猫/五更瑠璃**、五更日向〈黒猫の上の妹〉）
  - [最終幻想 零式](../Page/最終幻想_零式.md "wikilink") PSP（**DEUCE**）
  - [Dissidia Final Fantasy Opera
    Omnia](../Page/Dissidia_Final_Fantasy_Opera_Omnia.md "wikilink")（**DEUCE**）
  - [女神異聞錄4 The GOLDEN](../Page/女神異聞錄4_The_GOLDEN.md "wikilink")（マリー）
  - [靈魂扳機](../Page/靈魂扳機.md "wikilink")（**エマ**）
  - [我的朋友很少 攜帶版](../Page/我的朋友很少.md "wikilink")（**羽瀨川小鳩**）
  - [我的妹妹哪有這麼可愛
    攜帶版哪有可能繼續](../Page/我的妹妹哪有這麼可愛.md "wikilink")（**黒猫/五更瑠璃**、五更日向〈黒猫の上の妹〉）
  - [波色](../Page/波色.md "wikilink")（秋名美月）
  - [時と永遠](../Page/時と永遠.md "wikilink")（**トキ**）
  - [超級槍彈辯駁2 再見絕望校園](../Page/超級槍彈辯駁2_再見絕望校園.md "wikilink")（**七海千秋**）
  - [Phantasy Star Online
    2](../Page/Phantasy_Star_Online_2.md "wikilink")（理紗）
  - [双剣のクロスエイジ](../Page/双剣のクロスエイジ.md "wikilink")（生駒吉乃）
  - [Little
    Busters\!](../Page/Little_Busters!.md "wikilink")（杉並睦實）（Converted
    Edition後版本）
  - [扩散性百万亚瑟](../Page/扩散性百万亚瑟.md "wikilink")（加拉哈德）
  - [SD Gundam GGENERATION OVER
    WORLD](../Page/SD高达G世代.md "wikilink")（露娜·阿尔蒙尼亚、自创人物）
  - [約會大作戰 凜彌烏托邦](../Page/約會大作戰.md "wikilink")（**園神凜彌**）
  - [約會大作戰 Twin Edition 轉世凜緒](../Page/約會大作戰.md "wikilink")（**園神凜禰**）
  - [蘿球社！遺落物的祕密](../Page/蘿球社！.md "wikilink")（**湊智花**）
  - [神次元遊戲 戰機少女V](../Page/神次元遊戲_戰機少女V.md "wikilink")（**普露露特／愛莉絲之心**）
  - [IS〈Infinite Stratos〉2 Ignition
    Hearts](../Page/IS〈Infinite_Stratos〉.md "wikilink")（**夏綠蒂·迪努亞**）
  - [Destiny of Spirits（PS
    Vita亞洲限定版）](../Page/Destiny_of_Spirits（PS_Vita亞洲限定版）.md "wikilink")（**月讀**）
  - [電擊文庫 FIGHTING
    CLIMAX](../Page/電擊文庫_FIGHTING_CLIMAX.md "wikilink")（**湊智花**、黑貓）
  - [granado espada](../Page/granado_espada.md "wikilink")（巴萊黎雅）
  - [ファンタシースターオンライン2
    (PSO2)](../Page/ファンタシースターオンライン2_\(PSO2\).md "wikilink")（莉莎）
  - [白貓Project](../Page/白貓Project.md "wikilink")（ファム）
  - [崩壞學園](../Page/崩壞學園.md "wikilink")（Bronya）
  - [少女們向荒野進發](../Page/少女們向荒野進發.md "wikilink")（小早川夕夏）
  - [Nitro+ Blasterz -Heroines Infinite
    Duel](../Page/Nitroplus_Blasterz_-Heroines_Infinite_Duel-.md "wikilink")-（常守朱）
  - [勇者鬥惡龍 Heroes](../Page/勇者鬥惡龍_Heroes.md "wikilink")（芙蘿拉）
  - [神奇寶貝 終結紅寶石·初始藍寶石](../Page/神奇寶貝_終結紅寶石·初始藍寶石.md "wikilink")（女主人公 小遙）
  - [鋼鐵少女](../Page/钢铁少女.md "wikilink")（土佐）
  - [消滅都市](../Page/消滅都市.md "wikilink")（**小雪**）
  - 屍妹-絕園少女（夏川風斗、沙裡綾子）
  - [Shadowverse](../Page/Shadowverse.md "wikilink")（暗魔女將、機械降神、鮮紅的穿光·瑟塔）
  - [永遠的七日之都](../Page/永遠的七日之都.md "wikilink")（瀨由衣）
  - [為了誰的煉金術師](../Page/為了誰的煉金術師.md "wikilink")（梅林）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [最終幻想 紛爭NT](../Page/最终幻想_纷争_\(2015年游戏\).md "wikilink")（莉諾雅·哈蒂莉）
  - [陰陽師](../Page/陰陽師.md "wikilink") (面靈氣)
  - [蒼之紀元](../Page/蒼之紀元.md "wikilink") (美杜莎)
  - [超異域公主連結 Re:Dive](../Page/超異域公主連結_Re:Dive.md "wikilink") （碧）
  - [電擊文庫 零境交錯](../Page/電擊文庫_零境交錯.md "wikilink") （湊智花、黑貓）
  - [神無月](../Page/神無月.md "wikilink") (妮娜、狄伊莎)
  - [決戰.平安京](../Page/決戰.平安京.md "wikilink") (面靈氣)

### 音樂

  - ちゃんとしてよセイシュン（[出包王女角色歌](../Page/出包王女.md "wikilink")）（結城美柑）

  - （[化物語角色歌](../Page/化物語.md "wikilink")）（千石撫子）

  - （[小鳩角色歌](../Page/小鳩.md "wikilink")）（花戶小鳩）

  - （[玩伴貓耳娘角色歌](../Page/玩伴貓耳娘.md "wikilink")）（雙葉葵）

  - リビング Wars（[更多出包王女角色歌](../Page/出包王女.md "wikilink")）（結城美柑）

  - （[只有神知道的世界角色歌](../Page/只有神知道的世界.md "wikilink")）（汐宮栞）

  - SUPER∞STREAM（[IS片尾曲](../Page/IS〈Infinite_Stratos〉.md "wikilink")）（夏爾／夏綠蒂·迪努亞）

  - （世紀末超自然學院插入曲）（成瀨梢）

  - Masquerade\!（[我的妹妹哪有這麼可愛片尾曲](../Page/我的妹妹哪有這麼可愛.md "wikilink")）（黑貓／五更琉璃）

  - （[我的妹妹哪有這麼可愛片尾曲](../Page/我的妹妹哪有這麼可愛.md "wikilink")）（黑貓／五更琉璃）

  - Platonic
    prison（[我的妹妹哪有這麼可愛黑貓角色歌](../Page/我的妹妹哪有這麼可愛.md "wikilink")）（黑貓／五更琉璃）

  - 贖罪のセレナーデ（[我的妹妹哪有這麼可愛片尾曲](../Page/我的妹妹哪有這麼可愛.md "wikilink")）（黑貓／五更琉璃）

  - SHOOT\!-No.4 MIX- （[蘿球社！角色曲](../Page/蘿球社！.md "wikilink")）（湊智花）

  - My Dear （[蘿球社！角色曲](../Page/蘿球社！.md "wikilink")）（湊智花）

  - Shoooooter\!（[蘿球社！角色曲](../Page/蘿球社！.md "wikilink")）（湊智花）

  - 妖狐×僕SS ED6 sweets parade 髏髏宮加留多

  - （[來自新世界後期ED](../Page/來自新世界.md "wikilink")）（秋月真理亞）

  - モノクロ☆HAPPY
    DAY（[我的妹妹哪有這麼可愛片尾曲](../Page/我的妹妹哪有這麼可愛.md "wikilink")）（黑貓／五更琉璃）

  - 刹那のDestiny（[我的妹妹哪有這麼可愛片尾曲](../Page/我的妹妹哪有這麼可愛.md "wikilink")）（黑貓／五更琉璃）

  - （[囮物語角色歌](../Page/囮物語.md "wikilink")）（千石撫子）

  - "Phantasy Star Online 2" Character CD - Song Festival -（PSO2角色歌）（莉莎）

  - "recover decoration" ([偽戀ed](../Page/偽戀.md "wikilink")2) (小野寺小咲)

### 廣播劇CD

  - [月面兔兵器米娜](../Page/月面兔兵器米娜.md "wikilink")（羽蟬奈琉）
  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（亂崎優歌）
  - [鶺鴒女神](../Page/鶺鴒女神.md "wikilink")（**草野**）
  - [出包王女](../Page/出包王女.md "wikilink")（結城美柑）
  - [Sketchbook ～full
    color's～廣播劇CD](../Page/Sketchbook.md "wikilink")『Sketch
    Book Stories 〜前夜祭〜』（**梶原空**）
  - [ZEGAPAIN廣播劇CD](../Page/ZEGAPAIN.md "wikilink")『audio drama OUR LAST
    DAYS'（守凪了子）
  - [文學少女](../Page/文學少女.md "wikilink")「文學少女」と死にたがりの道化（**天野遠子**）
  - [我的妹妹哪有這麼可愛！](../Page/我的妹妹哪有這麼可愛！.md "wikilink")（**黑猫／五更琉璃**）
  - [我的朋友很少廣播劇CD](../Page/我的朋友很少.md "wikilink") 番外編「禁じられた遊び」（**羽瀨川小鳩**）
  - [日日蝶蝶廣播劇CD](../Page/日日蝶蝶.md "wikilink") 「相遇篇」「文化季篇」（**柴石睡蓮**）
  - [地獄三頭犬的日常](../Page/地獄三頭犬的日常.md "wikilink")（**小茂根陽**）
  - [天使1/2方程式](../Page/天使1/2方程式.md "wikilink")（**高垣由衣子**）
  - [Fate prototype 蒼銀的碎片](../Page/Fate_prototype_蒼銀的碎片.md "wikilink")
    (**沙条綾香**)
  - [我太受歡迎了該怎麼辦?](../Page/我太受歡迎了該怎麼辦?.md "wikilink")(**芹沼花依**)
  - [魔女之旅](../Page/魔女之旅.md "wikilink")(**芙蘭**)

### 電視劇

  - 2001年

<!-- end list -->

  - （牧村佳奈子，[TBS](../Page/TBS.md "wikilink")）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [怨屋本舖](../Page/怨屋本舖.md "wikilink")（青山美香）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [往名古屋的最終列車2018](../Page/往名古屋的最終列車2018.md "wikilink") (不小心弄丟手機的少女)

### 電影

  - 2004年

<!-- end list -->

  - [理由](../Page/理由.md "wikilink")（由香里的朋友）

<!-- end list -->

  - 2010年

<!-- end list -->

  - （友情出演）

<!-- end list -->

  - 2015年

<!-- end list -->

  - （）\[19\]

### 舞台

  - 天使は瞳を閉じて（おしばい軍団もずくぁんず、2009年2月）

### 寫真集

  - Oh\!Baby（1999年）（[辰巳出版](../Page/辰巳出版.md "wikilink")）
  - KANA（2011年）

### 廣告代言

  - [台北地下街](../Page/台北地下街.md "wikilink")12周年形象人物（**莉洋**）（2012年）
  - [最喜歡肉！宙牛君](../Page/最喜歡肉！宙牛君.md "wikilink")（**宙牛君**）

## 唱片

### 单曲

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>發售日</p></th>
<th><p>標題</p></th>
<th><p>規格</p></th>
<th><p>歌曲</p></th>
<th><p>備註</p></th>
<th><p>ORICON最高位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2012年4月25日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-7840～SVWC-7841（初回限定盤）<br />
SVWC-7842（通常盤）</p></td>
<td><ol>
<li></li>
<li>Saturday Night Musical</li>
<li></li>
<li></li>
</ol></td>
<td><p>花澤香菜出道首張单曲</p></td>
<td><p>7位</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2012年7月18日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-7863～SVWC-7864（初回限定盤）<br />
SVWC-7865（通常盤）</p></td>
<td><ol>
<li></li>
</ol>
<p>#{{lang|ja|裸足のvacation</p></td>
<td><p>}}</p>
<ol>
<li></li>
<li></li>
</ol></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2012年10月24日</p></td>
<td><p><strong><a href="../Page/happy_endings.md" title="wikilink">happy endings</a></strong></p></td>
<td><p>SVWC-7899～SVWC-7900（初回限定盤）<br />
SVWC-7901（通常盤）</p></td>
<td><ol>
<li>happy endings</li>
<li>too late for chocolate</li>
<li>trick or treat！</li>
<li>happy endings（Instrumental）</li>
</ol></td>
<td><p>電視動畫『<a href="../Page/絕園的暴風雨.md" title="wikilink">絕園的暴風雨</a>』片尾曲</p></td>
<td><p>7位</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2013年1月16日</p></td>
<td><p><strong><a href="../Page/Silent_Snow.md" title="wikilink">Silent Snow</a></strong></p></td>
<td><p>SVWC-7926～SVWC-7927（初回限定盤）<br />
SVWC-7928（通常盤）</p></td>
<td><ol>
<li>Silent Snow</li>
<li>CALL ME EVERYDAY</li>
<li></li>
<li>Silent Snow（Instrumental）</li>
</ol></td>
<td><p>2012四季企劃最後一張</p></td>
<td><p>8位</p></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>2013年12月25日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-7974～SVWC-7975（初回限定盤）<br />
SVWC-7976（通常盤）</p></td>
<td><ol>
<li></li>
<li></li>
<li>white christmas</li>
<li><p>（Instrumental）</p></li>
</ol></td>
<td></td>
<td><p>11位</p></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p>2014年10月1日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-70021～SVWC-70022（初回限定盤）<br />
SVWC-70023（通常盤）</p></td>
<td><ol>
<li></li>
<li>Timeless</li>
<li>MOMENT</li>
<li><p>(instrumental)</p></li>
</ol></td>
<td></td>
<td><p>12位</p></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p>2014年12月24日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-70041 ～ SVWC-70042（初回限定盤）<br />
SVWC-70043（通常盤）</p></td>
<td><ol>
<li></li>
<li>アブラカタブラ片思い</li>
<li></li>
<li></li>
<li><p>(instrumental)</p></li>
<li><p>(instrumental)</p></li>
</ol></td>
<td></td>
<td><p>13位</p></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p>2015年2月25日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-70053/4（初回限定盤）<br />
SVWC-70055（通常盤）</p></td>
<td><ol>
<li></li>
<li>Blessing Bell</li>
<li></li>
<li></li>
</ol></td>
<td><p>花澤香菜首次主演電影【】之主題曲</p></td>
<td><p>13位</p></td>
</tr>
<tr class="odd">
<td><p>9th</p></td>
<td><p>2016年2月24日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-70138/9（初回限定盤）<br />
SVWC-70140（通常盤）</p></td>
<td><ol>
<li></li>
<li></li>
<li></li>
<li></li>
</ol></td>
<td></td>
<td><p>20位</p></td>
</tr>
<tr class="even">
<td><p>10th</p></td>
<td><p>2016年6月1日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-70170/1（初回限定盤）<br />
SVWC-70172（通常盤）</p></td>
<td><ol>
<li></li>
<li></li>
<li></li>
<li></li>
</ol></td>
<td></td>
<td><p>14位</p></td>
</tr>
<tr class="odd">
<td><p>11th</p></td>
<td><p>2016年11月30日</p></td>
<td><p><strong></strong></p></td>
<td><p>SVWC-70225/6（初回限定盤）<br />
SVWC-70227（通常盤）</p></td>
<td><ol>
<li></li>
<li></li>
<li></li>
<li></li>
</ol></td>
<td></td>
<td><p>20位</p></td>
</tr>
<tr class="even">
<td><p>12th</p></td>
<td><p>2018年2月7日</p></td>
<td><p><strong></strong></p></td>
<td><p>VVCL-1164/5（初回限定盤）<br />
VVCL-1166（通常盤）</p></td>
<td><ol>
<li></li>
<li></li>
<li></li>
<li></li>
</ol></td>
<td></td>
<td><p>15位</p></td>
</tr>
<tr class="odd">
<td><p>13th</p></td>
<td><p>2018年7月25日</p></td>
<td><p><strong></strong></p></td>
<td><p>VVCL-1267/8（初回限定盤）<br />
VVCL-1269（通常盤）<br />
VVCL-1270/1（期間限定盤）</p></td>
<td><ol>
<li></li>
<li></li>
<li></li>
<li></li>
</ol></td>
<td></td>
<td><p>27位</p></td>
</tr>
</tbody>
</table>

### 網路單曲

|     | 發售日         | 標題   | 音質          |
| --- | ----------- | ---- | ----------- |
| 1st | 2015年10月21日 | **** | 96kHz/24bit |
|     |             |      |             |

### 專輯

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>發售日</p></th>
<th><p>標題</p></th>
<th><p>規格</p></th>
<th><p>歌曲</p></th>
<th><p>備註</p></th>
<th><p>最高位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2013年2月20日</p></td>
<td><p><strong><a href="../Page/claire.md" title="wikilink">claire</a></strong></p></td>
<td><p>SVWC-7929</p></td>
<td><ol>
<li></li>
<li>Just The Way You Are</li>
<li></li>
<li></li>
<li></li>
<li></li>
<li>melody</li>
<li>Ring a Bell</li>
<li>Silent Snow</li>
<li></li>
<li></li>
<li></li>
<li></li>
<li>happy endings</li>
</ol></td>
<td><p>花澤香菜首張專輯</p></td>
<td><p>6位</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2014年2月26日</p></td>
<td><p><strong>25</strong></p></td>
<td><p>SVWC-7988/90<br />
SVWC-7991/92</p></td>
<td><p>1.バースデイ ［作詞・作曲・編曲：北川勝利］</p>
<p>2.25 Hours a Day ［作詞：岩里祐穂 / 作曲・編曲：北川勝利］</p>
<p>3.Brand New Days ［作詞・作曲・編曲：矢野博康］</p>
<p>4.恋する惑星 ［作詞：岩里祐穂 / 作曲・編曲：北川勝利 / ストリングス編曲：長谷泰宏］</p>
<p>5.マラソン ［作詞：花澤香菜、岩里祐穂 / 作曲・編曲：北川勝利］</p>
<p>6.YESTERDAY BOYFRIEND ［作詞：西寺郷太 / 作・編曲：奥田健介］</p>
<p>7.無邪気なキミと真夏のメロディ ［作詞・作曲・編曲：中塚武］</p>
<p>8.Make a Difference ［作詞：岩里祐穂 / 作曲・編曲：mito］</p>
<p>9.旅立つ彼女と古い背表紙 ［作詞・作曲・編曲：沖井礼二 / ストリングス編曲：長谷泰宏］</p>
<p>10.Summer Sunset ［作詞：岩里祐穂 / 作・編曲：北川勝利］</p>
<p>11.同心円上のディスタンス ［作詞・作曲・編曲：宮川弾］</p>
<p>12.flattery? ［作詞・作曲・編曲：古川本舗］</p>
<p>13.Waltz for Praha ［作詞・作曲：北川勝利 / 編曲：sugarbeans、北川勝利］</p>
<p>14.片思いが世界を救う ［作詞・作曲・編曲：宮川弾］</p>
<p>15.ダエンケイ ［作詞：岩里祐穂 / 作・編曲：北川勝利］</p>
<p>16.パパ、アイ・ラブ・ユー</p></td>
<td><p>［作詞・作曲・編曲：カジヒデキ］</p>
<p>17.Eeny, meeny, miny, moe ［作詞：浅沼晋太郎 / 作・編曲：白神真志朗］</p>
<p>18.粉雪 ［作詞：花澤香菜 / 作曲：北川勝利 / 編曲：北川勝利、鈴木圭］</p>
<p>19.Young Oh! Oh! ［作詞：花澤香菜、岩里祐穂 / 作曲・編曲：北川勝利］</p>
<p>20.曖昧な世界 ［作詞：岩里祐穂 / 作曲・編曲：北川勝利］</p>
<p>21.真夜中の秘密会議 ［作詞：花澤香菜 / 補作詞：岩里祐穂 / 作曲：北川勝利 / 編曲：桜井康史、北川勝利］</p>
<p>22.Merry Go Round ［作詞：岩里祐穂 / 作曲：北川勝利 / 編曲：宮川弾］</p>
<p>23.last contrast ［作詞・作曲：小出祐介 / 編曲：釣俊輔、小出祐介］</p>
<p>24.花びら ［作詞：岩里祐穂 / 作曲・編曲：北川勝利］</p>
<p>25.Good Conversation ［作詞：岩里祐穂 / 作曲：北川勝利、編曲：IEDA］</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2015年4月22日</p></td>
<td><p><strong>Blue Avenue</strong></p></td>
<td><p>SVWC-70064/5<br />
SVWC-70066</p></td>
<td><p>01.I ♥ New Day ! 作詞：岩里祐穂 作曲・編曲：北川勝利 ホーンアレンジ：村田陽一</p>
<p>02.ほほ笑みモード 作詞：岩里祐穂 作曲・編曲：Studio Apartment</p>
<p>03.Nobody Knows 作詞：岩里祐穂 作曲：北川勝利 編曲：北園みなみ、北川勝利</p>
<p>04.ブルーベリーナイト 作詞・作曲・編曲：宮川弾</p>
<p>05.Trace 作詞：岩里祐穂 作曲・編曲：mito</p>
<p>06.こきゅうとす 作詞・作曲：ティカ・α 編曲：やくしまるえつこ、山口元輝</p>
<p>07.Night And Day 作詞・作曲・編曲：北川勝利 ホーンアレンジ：北園みなみ</p>
<p>08.タップダンスの音が聴こえてきたら 作詞：花澤香菜 作曲・編曲：北川勝利</p>
<p>09.We Are So in Love 作詞・作曲・編曲：矢野博康</p>
<p>10.プール 作詞：花澤香菜 作曲：北川勝利 編曲：zakbee &amp; Hatayoung、北川勝利</p>
<p>11.Dream a Dream 作詞：岩里祐穂 作曲・編曲：後日発表</p>
<p>12.マジカル・ファンタジー・ツアー 作詞・作曲・編曲：中塚武</p>
<p>13.君がいなくちゃだめなんだ 作詞：岩里祐穂 作曲・編曲：北川勝利 ストリングスアレンジ：宮川弾</p>
<p>14.Blue Avenue を探して 作詞：西寺郷太 作曲・編曲：奥田健介</p></td>
<td></td>
<td><p>12位</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2017年2月22日</p></td>
<td><p><strong>Opportunity</strong></p></td>
<td><p>SVWC-70251/2<br />
SVWC-70253</p></td>
<td><p>01.スウィンギング・ガール 作詞：岩里祐穂、作曲・編曲：kz</p>
<p>02.あたらしいうた 作詞：花澤香菜、作曲・編曲：北川勝利</p>
<p>03.FRIENDS FOREVER 作詞：西寺郷太、作曲：Mick Hucknall、編曲：Andy Wright</p>
<p>04.星結ぶとき 作詞：宮川弾、作曲・編曲：Spangle call Lilli line</p>
<p>05.滞空時間 作詞・作曲・編曲：宮川弾</p>
<p>06.カレイドスコープ 作詞・作曲・編曲：沖井礼二</p>
<p>07.透明な女の子 作詞・作曲：山崎ゆかり、編曲・プロデュース：空気公団</p>
<p>08.Marmalade Jam 作詞・作曲・編曲：北川勝利</p>
<p>09.Opportunity 作詞：岩里祐穂、作曲・編曲：北川勝利</p>
<p>10.ざらざら 作詞：花澤香菜、作曲：秦基博、編曲：島田昌典</p>
<p>11.雲に歌えば 作詞：岩里佑穂、作曲・編曲：北川勝利</p>
<p>12.FLOWER MARKET 作詞:岩里祐穂、作曲：片寄明人、編曲：片寄明人, 北川勝利</p>
<p>13.brilliant 作詞：花澤香菜、作曲・編曲：北川勝利</p>
<p>14.Seasons always change 作詞：岩里祐穂、作曲・編曲：矢野博康</p>
<p>15.Blue Water 作詞：岩里祐穂、作曲・編曲：ミト</p></td>
<td></td>
<td><p>15位</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部連結

  - [大澤事務所公開簡歷](http://osawa-inc.co.jp/blocks/index/talent00131.html)

  -
  - －花澤香菜的官方部落格。

  -

  -

  -
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本兒童演員](../Category/日本兒童演員.md "wikilink")
[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本索尼音樂娛樂旗下藝人](../Category/日本索尼音樂娛樂旗下藝人.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:日本女性電視藝人](../Category/日本女性電視藝人.md "wikilink")
[Category:聲優獎助演女優獎得主](../Category/聲優獎助演女優獎得主.md "wikilink")
[Category:大澤事務所](../Category/大澤事務所.md "wikilink")

1.  [花澤香菜｜CDJournal](http://artist.cdjournal.com/a/hanazawa-kana/860263)
2.  [花澤香菜｜TOWER
    RECORDS](http://tower.jp/artist/1028427/%E8%8A%B1%E6%BE%A4%E9%A6%99%E8%8F%9C)
3.  [花澤香菜｜TSUTAYA](http://www.tsutaya.co.jp/artist/00513301.html)
4.
5.  參加2013年十月新番《來自風平浪靜的明天》的声优到场先行上映会時，於声优谈话节目中自己不小心說漏了嘴。
6.  「「。」第2回」、《[月刊Newtype](../Page/Newtype.md "wikilink")》2012年9月號，[角川書店](../Page/角川書店.md "wikilink")，2012年7月10日。
7.  來自「"apple symphony"the Live & the Birthday Blu-ray」Disc02的收錄內容
8.
9.
10.
11.
12.
13. 第4.5話 劇場上映{{\!}}電視動畫「無頭騎士異聞錄
    DuRaRaRa\!\!×2」官方網站|accessdate=2015年7月29日|author=
    |date= |publisher= |language=ja}}
14.
15.
16.
17.
18.
19. \[<http://news.gamme.com.tw/1028349>　聲優花澤香菜首次主演電影：香菜會不會太操勞了！？\]