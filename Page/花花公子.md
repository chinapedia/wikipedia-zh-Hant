《**花花公子**》（）是[美国一份](../Page/美国.md "wikilink")[男性](../Page/男性.md "wikilink")[成人雜誌](../Page/成人雜誌.md "wikilink")，1953年在[美國由](../Page/美國.md "wikilink")[休·赫夫纳所創辦](../Page/休·赫夫纳.md "wikilink")。《花花公子》曾是美國[紐約股票交易所上市的媒體集團企業](../Page/紐約股票交易所.md "wikilink")，其後已除牌，出版多種[刊物](../Page/刊物.md "wikilink")，亦有[電視](../Page/電視.md "wikilink")、[電影等業務](../Page/電影.md "wikilink")。其中《花花公子》除了在美國出版外，更在多個國家出版當地版本。《花花公子》每月出版，內容除了[女性](../Page/女性.md "wikilink")[祼照外](../Page/裸体.md "wikilink")，尚有文章介紹[時裝](../Page/時裝.md "wikilink")、[飲食](../Page/飲食.md "wikilink")、[體育](../Page/體育.md "wikilink")、[消費等](../Page/消費.md "wikilink")；此外亦有[短篇故事](../Page/短篇故事.md "wikilink")、名人專訪以至新聞時事評論。2009年盛傳創辦人[休·赫夫纳以](../Page/休·赫夫纳.md "wikilink")3億美元把花花公子放盤。\[1\]

《花花公子》以言論傾向[自由主義聞名](../Page/自由主義.md "wikilink")。其文章的水平一般甚高。《花花公子》內的裸照以「[品味高尚](../Page/品味.md "wikilink")」、「樂而不淫」作招徠，走的是高格調、[中產](../Page/中產.md "wikilink")[娛樂消費路線](../Page/娛樂.md "wikilink")。

## 歷史

《花花公子》第一期的封面及中間拉頁女郎為[瑪麗蓮·夢露](../Page/瑪麗蓮·夢露.md "wikilink")。本來其裸照是為[月曆拍攝](../Page/月曆.md "wikilink")，[休·赫夫纳購得使用權刊登](../Page/休·赫夫纳.md "wikilink")。第一期在1953年12月出版，售價50美分，總銷售量達53,991本。現仍存的第一期《花花公子》成為收藏的對象。2002年，全新的第一期《花花公子》價值約為五千美元。至於《花花公子》的著名禮服白兔標誌，則是在第二期以後才開始使用。

1972年11月版的《花花公子》創下銷售超過七百萬冊的紀錄。1970年代後期以後，《花花公子》出現甚多競爭對手，如《[閣樓](../Page/閣樓_\(雜誌\).md "wikilink")》等，其銷路逐漸下滑。

《花花公子》的人物專訪以深入見稱，每次專訪通常長達七至十小時。曾經訪問過的名人有[約翰·韋恩](../Page/約翰·韋恩.md "wikilink")、[吉米·卡特](../Page/吉米·卡特.md "wikilink")、[卡斯特羅](../Page/卡斯特羅.md "wikilink")、[羅素](../Page/羅素.md "wikilink")、[马丁·路德·金](../Page/马丁·路德·金.md "wikilink")、[让-保罗·萨特](../Page/让-保罗·萨特.md "wikilink")、[拳王阿里](../Page/拳王阿里.md "wikilink")、[阿拉法](../Page/阿拉法.md "wikilink")、[史提芬·霍金](../Page/史提芬·威廉·霍金.md "wikilink")、[石原慎太郎等等](../Page/石原慎太郎.md "wikilink")。

有种观点认为《花花公子》是一本严肃文学杂志。[休·赫夫纳以高稿酬约请一线名家为他撰稿](../Page/休·赫夫纳.md "wikilink")，如[海明威](../Page/欧内斯特·海明威.md "wikilink")、[列侬](../Page/约翰·列侬.md "wikilink")，[毛姆](../Page/威廉·萨默塞特·毛姆.md "wikilink")，[安迪·沃霍尔](../Page/安迪·沃霍尔.md "wikilink")、[纳博科夫](../Page/弗拉基米尔·弗拉基米罗维奇·纳博科夫.md "wikilink")、[博尔赫斯和](../Page/豪尔赫·路易斯·博尔赫斯.md "wikilink")[约翰·厄普代克](../Page/約翰·厄普代克.md "wikilink")。
ig\[2\]

而在2012年11月1日，美國《花花公子》俱樂部，計劃在12月初在[印度落戶](../Page/印度.md "wikilink")，總投資額約2億9千萬港元\[3\]。

自2016年3月刊起，《花花公子》不再刊登全裸照片\[4\]。CEO斯科特·弗兰德斯（）坦承杂志无法和免费提供的[网络色情和裸体内容竞争](../Page/网络色情.md "wikilink")：“现今你轻轻点击，便能免费看到可以想象的性行为。所以在这个节骨眼上，杂志已经是明日黄花了\[5\]。”赫夫纳同意这一决定\[6\]。重新设计的杂志虽然保留了“[玩伴女郎](../Page/玩伴女郎.md "wikilink")”和女性图像，但僅达到13禁的水平。付费订阅网站PlayboyPlus.com不受影响\[7\]。流行笑话专栏和各式各样的漫画也从杂志中消失\[8\]。

## 紀錄

最暢銷的一期《花花公子》是1972年11月號，銷量達7,161,561本，而該期「每月大相」的美女[Lena
Soderberg全身照片](../Page/莱娜·瑟德贝里.md "wikilink")，其[頭部和](../Page/頭.md "wikilink")[肩膀的位置更成為數字](../Page/肩膀.md "wikilink")[圖像處理的測試圖](../Page/圖像處理.md "wikilink")，該部分被稱為「[蓮娜圖](../Page/蓮娜圖.md "wikilink")。

## 其他語言

[美國以外有](../Page/美國.md "wikilink")30多個[地區曾經出版當地版本的](../Page/地區.md "wikilink")《花花公子》。

首本[中文版是](../Page/中文.md "wikilink")1986年在[香港出版](../Page/香港.md "wikilink")，出版人是[鄭經翰](../Page/鄭經翰.md "wikilink")，背後主要股東為[星島新聞集團的](../Page/星島新聞集團.md "wikilink")[胡仙](../Page/胡仙.md "wikilink")，首期封面女郎為[鄭文雅](../Page/鄭文雅.md "wikilink")。後來香港版在1993年停刊。

國際中文版則在1996年7月創刊，由[協和國際多媒體出版](../Page/協和國際多媒體.md "wikilink")，發行人是[許安進](../Page/許安進.md "wikilink")，創刊號封面及[玩伴女郎為出身](../Page/玩伴女郎.md "wikilink")[荷蘭伊斯布萊登](../Page/荷蘭.md "wikilink")（Ysbrechtum）的安瑪莉·高達（Anna-Marie
Goddard），1996年8月至12月玩伴女郎分別為Stacy Sanches、Holly Witt、Amanda Hope、Donna
D'Errico、Jessica Lee。國際中文版在2003年停刊。

## 国际版本

[Playboy_Club_Bar.jpg](https://zh.wikipedia.org/wiki/File:Playboy_Club_Bar.jpg "fig:Playboy_Club_Bar.jpg")
以下是《花花公子》的不同版本，以創刊時間排列：

  - （1953年）

  - （1972年－2003年）

  - （1972年－）

  - （1973年－）

  - （1975年－2009年1月）

  - （1975年－）

  - （1976年－1998年，2002年－）

  - （1978年－）

  - （1979年－2000年）

  - （1983年－）

  - （1985年－1995年）

  - （1985年－）

  - （1986年－1993年）

  - （1986年－1995年）

  - （1989年－1993年，1999年－）

  - （1990年－2003年）

  - （1991年－）

  - （1992年－）

  - （1993年－1996年）

  - （1995年－）

  - （1997年－2002年）

  - （1997年－）

  - （1998年－1999年）

  - （1998年－1999年）

  - （1999年－）

  - （2001年6月－）

  - （2002年－）

  - （2004年－）

## 參見

  - 《[花花女郎](../Page/花花女郎.md "wikilink")》

## 參考文獻

## 外部連結

  - [花花公子官方網站](http://www.playboy.com)

[Category:美國雜誌](../Category/美國雜誌.md "wikilink")
[Category:成人雜誌](../Category/成人雜誌.md "wikilink")
[Category:1953年成立的公司](../Category/1953年成立的公司.md "wikilink")
[Category:花花公子](../Category/花花公子.md "wikilink")
[Category:芝加哥历史](../Category/芝加哥历史.md "wikilink")
[Category:1953年建立的出版物](../Category/1953年建立的出版物.md "wikilink")
[Category:性革命](../Category/性革命.md "wikilink")

1.  [傳維珍集團3億接手花花公子
    亞洲時報](http://www.atchinese.com/index.php?option=com_content&view=article&id=58674:3&catid=156:2009-01-12-16-15-45&Itemid=174)
2.
3.  [花花公子俱樂部落戶印度](http://the-sun.on.cc/cnt/china_world/20121102/00423_026.html)，太陽報
    2012-11-01
4.
5.
6.
7.
8.