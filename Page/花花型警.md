是2008年[香港動作片電影](../Page/香港.md "wikilink")，由[馬楚成擔任導演](../Page/馬楚成.md "wikilink")。本片主要演員包括[余文樂](../Page/余文樂.md "wikilink")、內地著名演員[陳坤](../Page/陳坤.md "wikilink")、[香港](../Page/香港.md "wikilink")[無綫電視女藝員](../Page/無綫電視.md "wikilink")[鍾嘉欣](../Page/鍾嘉欣.md "wikilink")、[狄龍之子](../Page/狄龍.md "wikilink")[譚俊彥](../Page/譚俊彥.md "wikilink")、[官恩娜等青春偶像領銜主演](../Page/官恩娜.md "wikilink")，演繹各種警匪場面題材，包括對手戲、動作場面等。這影片在2008年2月28日於[香港上映](../Page/香港.md "wikilink")。

根據《電影檢查條例》，本片列為青少年及兒童不宜（IIB）。

## 故事大綱

一宗手法極度殘酷的劫殺案，將一對身處中港兩地，做人處事手法迥異的警探牽引起來。死者親弟林亨，是一名從內地而來的前公安，發誓要揪出殺兄兇手，卻給他碰上了Michael，一個剛被停職的CID，原因是他常不依上級指示，如私自以他的私人直升機作捉賊之用！

本來Michael也沒興趣去理會林亨，但Michael的前女友Lisa因工作關係正被林亨積極追求，Michael無名火起，與林亨約法三章，只要他幫得林亨捉到兇手，林亨便立刻回去，不再纏著Lisa。

Michael作為香港某大富豪之後，查案手法當然異於常人，他有最強的線人網，只要有錢，全港的古惑仔也會出賣線報給他。雖然林亨繼承兄長生意，以及他的富豪俱樂部後錢也不缺，他卻喜歡與人交談，心思細密的他，很容易便能將線索吐露出來。在他們兩種充滿衝突的方法底下，他們查出了一個名字，老鼠！

老鼠聲稱目擊林亨兄長被殺情況，殺他的是個背後紋上蝎子圖案的男人。Michael與林亨從多方面調查，終於查出紋身漢所在，他們本以為案件有端倪，沒料到在他們眼前出現的蝎子紋身漢，竟不過是一個聲東擊西的目標而跟著麻煩接踵而來。

兩人本由敵對態度竟還漸被迫變得患難見真情…終於就在老鼠女友手上得到重要短片而得知林亨哥哥並非被劫殺，而是遭人棄屍，疑兇竟就是一直在俱樂部盡忠職守的經理David！

正好當時是俱樂部每季例行的富豪船河派對，David招呼了眾富豪出海消遣，Michael與林亨以為他們必定能將David捉住，可是船上卻突然來了一眾來歷不明的匪徒，將富豪們綁架，David更當場被就地正法！

更意外的是，Michael的父親也在船上同被綁架！

Michael以為沒大不了，付上贖金便可救回父親。但此刻他才知道，父親已經瀕臨破產邊緣，沒有錢，意味著父親就會被撕票！返回警隊，沒有名貴跑車的他，只能憑他的鬥志與勇氣，一手一腳解決困難，Michael亦因此有所改變。

千頭萬緒不知如何是好之際，竟發現原來David根本未死，先前的不過是個假象，背後竟然是一個難以置信向上流社會富豪的報復計劃。

Michael與林亨決定採取主動，向David來個大反擊，可是此時Lisa卻被David捉去，殘虐致生命危在旦夕！

正當這對的各走極端的拍檔，終於要同心合力，聯手對付眼前的大敵！也正是這對玩世不恭的「型警」成長作為堂堂男子漢的時候。

## 演員

### 麥可民相關

  - [余文樂](../Page/余文樂.md "wikilink") 飾 麥可民／Michael
  - [李修贤](../Page/李修贤.md "wikilink") 飾 麥父
  - [米　雪](../Page/米雪.md "wikilink") 飾 麥后母

### 林亨相關

  - [陳　坤](../Page/陳坤.md "wikilink") 飾 林亨／Lincoin
  - [鍾嘉欣](../Page/鍾嘉欣.md "wikilink") 飾 麗莎／Lisa
  - [連　晉](../Page/連晉.md "wikilink") 飾 林朗
  - [周秀娜](../Page/周秀娜.md "wikilink") 飾 林朗女友

### 黎大衛相關

  - [譚俊彥](../Page/譚俊彥.md "wikilink") 飾 黎大衛／David
  - [陳中泰](../Page/陳中泰.md "wikilink") 飾 黎大衛同黨
  - [譚幹聰](../Page/譚幹聰.md "wikilink") 飾 黎大衛同黨
  - [洛　琦](../Page/洛琦.md "wikilink") 飾 黎大衛同黨
  - [雷小明](../Page/雷小明.md "wikilink") 飾 黎大衛同黨
  - [賴懷德](../Page/賴懷德.md "wikilink") 飾 黎大衛同黨
  - [馮頌晴](../Page/馮頌晴.md "wikilink") 飾 黎大衛女友

### 盧強相關

  - [梁焯滿](../Page/梁焯滿.md "wikilink") 飾 盧強
  - [鄒　娜](../Page/鄒娜.md "wikilink") 飾 盧強女友

### 富豪相關

  - [徐寶三](../Page/徐寶三.md "wikilink") 飾 富豪
  - [傅香山](../Page/傅香山.md "wikilink") 飾 富豪
  - [羅玉驎](../Page/羅玉驎.md "wikilink") 飾 富豪
  - [劉日東](../Page/劉日東.md "wikilink") 飾 富豪
  - [袁寶儀](../Page/袁寶儀.md "wikilink") 飾 富豪情人
  - [袁　丹](../Page/袁丹.md "wikilink") 飾 富豪情人
  - [文曉妮](../Page/文曉妮.md "wikilink") 飾 富豪情人
  - [吳燕安](../Page/吳燕安.md "wikilink") 飾 富豪情人

### 紋身會

  - [熊欣欣](../Page/熊欣欣.md "wikilink") 飾 紋身會會長
  - [李洪基](../Page/李洪基.md "wikilink") 飾 紋身會會員
  - [陳金培](../Page/陳金培.md "wikilink") 飾 紋身會會員
  - [王文成](../Page/王文成.md "wikilink") 飾 紋身會會員
  - [羅偉佳](../Page/羅偉佳.md "wikilink") 飾 紋身會會員
  - [蘇偉南](../Page/蘇偉南.md "wikilink") 飾 紋身會會員
  - [鄭洪發](../Page/鄭洪發.md "wikilink") 飾 紋身會會員
  - [謝榮華](../Page/謝榮華.md "wikilink") 飾 紋身會會員

### 其他演員

  - [黄又南](../Page/黄又南.md "wikilink") 飾 阿龍
  - [谷德昭](../Page/谷德昭.md "wikilink") 飾 王警司
  - [伍允龙](../Page/伍允龙.md "wikilink") 飾 劫匪
  - [官恩娜](../Page/官恩娜.md "wikilink") 飾 女警官
  - [連　晉](../Page/連晉.md "wikilink") 飾 林朗
  - [張國威](../Page/張國威.md "wikilink") 飾 無紋身男人
  - [郭貴華](../Page/郭貴華.md "wikilink") 飾 龍蝦紋身人
  - [蕭展雄](../Page/蕭展雄.md "wikilink") 飾 熊貓紋身人
  - [冼國祥](../Page/冼國祥.md "wikilink") 飾 蝎子紋身伯伯
  - [李尚斌](../Page/李尚斌.md "wikilink") 飾 會所待應
  - [陳詩亮](../Page/陳詩亮.md "wikilink") 飾 會所待應
  - [美　成](../Page/美成.md "wikilink") 飾 保鏢

## 各地電影分級制度

| 國家／地區                    | 級別                       |
| ------------------------ | ------------------------ |
| <span style=>香港</span>   | <span style=>IIB</span>  |
| <span style=>新加坡</span>  | <span style=>PG</span>   |
| <span style=>馬來西亞</span> | <span style=>18PL</span> |
|                          |                          |

## 外部連結

  - [花花型警官方網站](http://playboycops.big.com.hk/)

  -
  -
  -
  -
  -
  -
[Category:香港警匪片](../Category/香港警匪片.md "wikilink")
[Category:美亞電影](../Category/美亞電影.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[8](../Category/2000年代香港電影作品.md "wikilink")