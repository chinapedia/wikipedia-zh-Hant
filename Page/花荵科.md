**花荵科**也叫[翠梅科](../Page/翠梅科.md "wikilink")，包括18-25[属约](../Page/属.md "wikilink")270-400[种](../Page/种.md "wikilink")，分布于北半球和部分[南美洲](../Page/南美洲.md "wikilink")，绝大部分都是[美洲的原生种](../Page/美洲.md "wikilink")，只有一属—[花荵属生长在](../Page/花荵属.md "wikilink")[欧洲](../Page/欧洲.md "wikilink")，两属—[花荵属和](../Page/花荵属.md "wikilink")[天蓝绣球属生长在](../Page/天蓝绣球属.md "wikilink")[亚洲](../Page/亚洲.md "wikilink")。[中国只有](../Page/中国.md "wikilink")6种，包括引种的[电灯花属](../Page/电灯花属.md "wikilink")，主要生长在北方。

本科[植物为一年生或多年生](../Page/植物.md "wikilink")[草本](../Page/草本.md "wikilink")，也有[灌木](../Page/灌木.md "wikilink")，依靠叶卷须攀爬，主要生长在凉爽的[气候条件下](../Page/气候.md "wikilink")，[叶互生或对生](../Page/叶.md "wikilink")，全缘，单叶分裂或羽状复叶，无托叶；[花小](../Page/花.md "wikilink")，两性，两侧对称，[颜色鲜艳](../Page/颜色.md "wikilink")，形成聚伞花序，[花瓣](../Page/花瓣.md "wikilink")5，[花冠合瓣](../Page/花冠.md "wikilink")；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，[种子有棱](../Page/种子.md "wikilink")、有锐角或有翅。

本[科植物包括多种观赏](../Page/科.md "wikilink")[花卉](../Page/花卉.md "wikilink")，其中[黄杨叶坎吐阿木](../Page/黄杨叶坎吐阿木.md "wikilink")（*Cantua
buxifolia*）是[秘鲁和](../Page/秘鲁.md "wikilink")[玻利维亚的国花](../Page/玻利维亚.md "wikilink")。

1981年的[克朗奎斯特分类法将其列入](../Page/克朗奎斯特分类法.md "wikilink")[茄目](../Page/茄目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其列入](../Page/APG_分类法.md "wikilink")[杜鹃花目](../Page/杜鹃花目.md "wikilink")。

## 参考文献

  - [花荵科](http://tolweb.org/Polemoniaceae)
  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[花荵科](https://web.archive.org/web/20061101100909/http://delta-intkey.com/angio/www/polemoni.htm)

[\*](../Category/花荵科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")