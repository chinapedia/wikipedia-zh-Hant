[Nectar.jpg](https://zh.wikipedia.org/wiki/File:Nectar.jpg "fig:Nectar.jpg")的花蜜。\]\]
[Australian_painted_lady_feeding_closeup.jpg](https://zh.wikipedia.org/wiki/File:Australian_painted_lady_feeding_closeup.jpg "fig:Australian_painted_lady_feeding_closeup.jpg")。\]\]

**花蜜**是[開花植物的](../Page/開花植物.md "wikilink")[花內蜜腺等分泌的一種主要成分是小分子醣類](../Page/花.md "wikilink")、比如雙醣的[蔗糖](../Page/蔗糖.md "wikilink")、單醣的[果糖](../Page/果糖.md "wikilink")、[葡萄糖及](../Page/葡萄糖.md "wikilink")[半乳糖](../Page/半乳糖.md "wikilink")、甚或三糖[棉子糖](../Page/棉子糖.md "wikilink")，四糖[水蘇糖等的混和物](../Page/水蘇糖.md "wikilink")，其種類與比例會隨物種甚或品種有相當變化。可吸引[授粉](../Page/授粉.md "wikilink")[昆蟲來採蜜](../Page/昆蟲.md "wikilink")，从而達到幫助授粉的目的。蜜蜂采集花蜜可酿成[蜂蜜](../Page/蜂蜜.md "wikilink")。

花蜜的浓度會隨環境而变化，比如[椴树在空气湿度为](../Page/椴树.md "wikilink")51%时，花蜜含糖量为72%；湿度为100%时，含糖量只有22%。蜜蜂喜欢采食含糖高的花蜜。

然而部分花蜜與花粉對[蜜蜂具有毒性](../Page/蜜蜂.md "wikilink")，比如含有[半乳糖的茶花](../Page/半乳糖.md "wikilink")，以及鉀離子含量偏高的棗花；甚或對人具有毒性，比如[雷公藤](../Page/雷公藤.md "wikilink")（*[Tripterygium
wilfordii](../Page/Tripterygium_wilfordii.md "wikilink")*
<small>Hook.F.</small>）、[博落回](../Page/博落回.md "wikilink") （*[Macleaya
cordata](../Page/Macleaya_cordata.md "wikilink")* <small>(Willd.)
R.Br</small>）、[狼毒](../Page/狼毒.md "wikilink")（*[Stellera
chamaejasme](../Page/Stellera_chamaejasme.md "wikilink")*
L.）。\[1\]\[2\]\[3\]

## 参见

  - [蜜源標記](../Page/蜜源標記.md "wikilink")
  - [蜂蜜](../Page/蜂蜜.md "wikilink")

## 参考资料

## 外部链接

  - [蜜蜂（和花蜜）信息的概述和总结（新闻，经济，贸易，问题等）。](http://en.lamieldeabejas.com/la-miel.html)

  - [蜂鸟植物数据库](http://cubits.org/hummingbirdgardening/db/hummingbirdplants/index.php)

[Category:被子植物](../Category/被子植物.md "wikilink")

1.  [Honey Bee
    Nutrition](http://www.beeccdcap.uga.edu/documents/caparticle10.html)
     College of Agricultural and Environmental Sciences at the
    University of Georgia
2.  [蜜蜂中毒有四种类型：甘露蜜中毒，蜜蜂茶花中毒，蜜蜂枣花中毒，农药中毒](http://www.scnjkj.gov.cn/ZJXT/mifeng/b1002.htm)四川省南江县科学技术局
3.  [蜜蜂采集植物的花蜜、分泌物或蜜露应安全无毒，不得来源于雷公藤（Tripterygium wilfordii
    Hook.F.）、博落回(Macleaya cordata (Willd.) R.Br、狼毒（Stellera
    chamaejasme
    L.）等有毒蜜源植物。](http://www.nhfpc.gov.cn/zwgkzt/psp/201106/51948/files/bf8837ddf29c4dd794d824b26260d206.pdf)食品安全国家标准
    蜂蜜