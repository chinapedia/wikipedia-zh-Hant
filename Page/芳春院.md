**芳春院**（1547年7月25日－1617年8月17日），[前田利家正室](../Page/前田利家.md "wikilink")，本名叫做**松**。生父為[篠原一元](../Page/篠原一元.md "wikilink")（主計），其母竹野氏在丈夫戰死後又改嫁給[高田直吉](../Page/高田直吉.md "wikilink")。因為母親改嫁的關係，她被送到姨母的夫家前田家，由利家的父親[前田利昌養育](../Page/前田利昌.md "wikilink")。她和利家本來是表兄妹的關係，大約在1558年時，與利家結婚，第二年時生下長女[幸](../Page/前田幸.md "wikilink")。

阿松與利家夫妻之間，生育了二男九女，其中長男[前田利長繼承了前田家](../Page/前田利長.md "wikilink")，三女[摩阿做了](../Page/加賀殿.md "wikilink")[豐臣秀吉的側室](../Page/豐臣秀吉.md "wikilink")，四女[豪姬成為秀吉的養女並嫁與五大老之一的](../Page/豐臣豪姬.md "wikilink")[宇喜多秀家](../Page/宇喜多秀家.md "wikilink")，六女[千世則是](../Page/前田千世.md "wikilink")[細川忠隆的妻子](../Page/細川忠隆.md "wikilink")。阿松與利家夫妻在年輕時就與秀吉、[寧寧這對夫妻是好朋友](../Page/北政所.md "wikilink")，他們之間的關係也特別親密，甚至後來阿松也從秀吉那裡得到化粧領地。

1599年利家過世，阿松出家，號芳春院，並且在[京都的大德寺裡建立了芳春院](../Page/京都.md "wikilink")。1600年，[德川家康藉口利家與利長當初有參與暗殺嫌疑而興起征討加賀的想法](../Page/德川家康.md "wikilink")，芳春院為了前田家的前途，自願成為人質前往[江戶](../Page/江戶.md "wikilink")，化解了危機，還讓家康的孫女[珠姬嫁到前田家](../Page/德川珠姬.md "wikilink")。在[關原之戰以後](../Page/關原之戰.md "wikilink")，前田家成為百萬石大名，芳春院功不可沒。1614年，利長過世，芳春院才得以離開居住十五年的江戶，回到[金澤](../Page/金澤.md "wikilink")，並於1617年過世。法名「芳春院殿華嚴宗富大禪定尼」，墓在金澤的[野田山墓地](../Page/野田山墓地.md "wikilink")，而京都的大德寺芳春院也有她的墓。

## 登场作品

  - 小說

<!-- end list -->

  - 前田利家（、著）
  - 前田利家（[講談社](../Page/講談社.md "wikilink")、著）
  - 前田利家（[小学館](../Page/小学館.md "wikilink")、著）
  - 前田利家（[幻冬舍](../Page/幻冬舍.md "wikilink")、著）
  - 前田利家（[学研](../Page/学研.md "wikilink")、著）
  - 前田利家：秀吉最倚賴的男人（[PHP研究所](../Page/PHP研究所.md "wikilink")、著）
  - 前田利家與妻阿松：築起「加賀百萬石」的二人三脚 （PHP研究所、著）
  - 百萬石異聞：前田利家與松 （、野村昭子著）
  - 前田利家物語：加賀百萬石之祖（北國出版社、北村魚泡洞著）

<!-- end list -->

  - 影視劇

<!-- end list -->

  - あばれ大名（1959年、東映、演：吉田江利子）

  - （1970年、KTV、演：）

  - [女太閤記](../Page/女太閤記.md "wikilink")（1981年、[NHK大河劇](../Page/NHK大河劇.md "wikilink")、演：）

  - [關原](../Page/:JA:関ヶ原_\(テレビドラマ\).md "wikilink")（1981年、TBS、演：）

  - [德川家康](../Page/德川家康_\(大河劇\).md "wikilink")（1983年、NHK大河劇、演：）

  - [真田太平記](../Page/:JA:真田太平記_\(テレビドラマ\).md "wikilink")（1985年、NHK、演：）

  - （1988年、NTV、演：[森光子](../Page/森光子.md "wikilink")）

  - [織田信長](../Page/:JA:織田信長_\(1989年のテレビドラマ\).md "wikilink")（1989年、TBS、演：）

  - （1993年、TBS、演：）

  - （1995年、TX、演：）

  - [秀吉](../Page/秀吉_\(大河劇\).md "wikilink")（1996年、NHK大河劇、演：）

  - （1999年、NHK、演：[松坂慶子](../Page/松坂慶子.md "wikilink")）

  - [葵德川三代](../Page/葵德川三代.md "wikilink")（2000年、NHK大河劇、演：）

  - [利家與松](../Page/利家與松.md "wikilink")（2002年、NHK大河劇、演：[松嶋菜菜子](../Page/松嶋菜菜子.md "wikilink")）

  - （2006年、EX、演：）

  - [寧寧：女太閤記](../Page/寧寧：女太閤記.md "wikilink")（2009年、TX、演：）

<!-- end list -->

  - 游戏

<!-- end list -->

  - [戦国BASARA系列](../Page/戦国BASARA.md "wikilink")（配音：[甲斐田裕子](../Page/甲斐田裕子.md "wikilink")）
  - 戦国絵札遊戯 不如帰 -HOTOTOGISU- 乱
  - 戦国大戦

[Category:戰國時代女性 (日本)](../Category/戰國時代女性_\(日本\).md "wikilink")
[Category:1547年出生](../Category/1547年出生.md "wikilink")
[Category:1617年逝世](../Category/1617年逝世.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")
[Category:加賀前田氏](../Category/加賀前田氏.md "wikilink")
[Category:安土桃山時代女性](../Category/安土桃山時代女性.md "wikilink")
[Category:江戶時代女性](../Category/江戶時代女性.md "wikilink")
[Category:加賀藩](../Category/加賀藩.md "wikilink")