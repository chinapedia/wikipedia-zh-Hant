[Ipoh_Chicken.jpg](https://zh.wikipedia.org/wiki/File:Ipoh_Chicken.jpg "fig:Ipoh_Chicken.jpg")
[Bean_sprouts_in_Ipoh.jpg](https://zh.wikipedia.org/wiki/File:Bean_sprouts_in_Ipoh.jpg "fig:Bean_sprouts_in_Ipoh.jpg")
**芽菜雞**是[馬來西亞](../Page/馬來西亞.md "wikilink")[怡保](../Page/怡保.md "wikilink")(Ipoh)舊街場的代表性美食，這道料理的特色是將[雞](../Page/雞.md "wikilink")、[豆芽和](../Page/豆芽.md "wikilink")[沙河粉分別上桌](../Page/沙河粉.md "wikilink")，而並非組合在一個盤子內。

## 雞絲河粉

[Sha_Ho_Fun_in_Ipoh.jpg](https://zh.wikipedia.org/wiki/File:Sha_Ho_Fun_in_Ipoh.jpg "fig:Sha_Ho_Fun_in_Ipoh.jpg")
[怡保為](../Page/怡保.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")[霹靂州的](../Page/霹靂州.md "wikilink")[首府](../Page/首府.md "wikilink")，早期以盛產[錫而盛名](../Page/錫.md "wikilink")，因此又被稱為——“錫都”。在英殖民時期，怡保吸引了大量來自[廣東和](../Page/廣東.md "wikilink")[客家的移民前來開採錫礦](../Page/客家.md "wikilink")，因為思鄉情懷之下，他們開始製作源自廣東[沙河鎮](../Page/沙河鎮.md "wikilink")（今[廣州市](../Page/廣州市.md "wikilink")[天河區](../Page/天河區.md "wikilink")）的[河粉](../Page/河粉.md "wikilink")。這些廣東移民挑起扁擔，在市區內開始賣起用老母雞湯頭熬製，加上雞絲的[雞絲河粉](../Page/雞絲河粉.md "wikilink")。因為怡保的泉水富含礦物質，使得這裡製作的[河粉特別香滑嫩口](../Page/河粉.md "wikilink")，所以造成怡保的[沙河粉名氣大聲](../Page/沙河粉.md "wikilink")。後期隨著[怡保經濟狀況的好轉](../Page/怡保.md "wikilink")，有些商家開始利用附近大河所盛產的大頭蝦，取其蝦殼蝦頭爆香，再加上老母雞雞湯一起熬製，配上鮮蝦幾隻，成為具有怡保特色的“紅油”[雞絲河粉](../Page/雞絲河粉.md "wikilink")。不過自70年代起，開始流行[雞絲河粉搭配](../Page/雞絲河粉.md "wikilink")[芽菜的吃法](../Page/芽菜.md "wikilink")，漸漸使得目前[怡保老街場所販售芽菜雞的店家只會搭配清湯](../Page/怡保.md "wikilink")[沙河粉](../Page/沙河粉.md "wikilink")、[白斬雞和](../Page/白斬雞.md "wikilink")[芽菜](../Page/芽菜.md "wikilink")，不在提供[雞絲河粉了](../Page/雞絲河粉.md "wikilink")。

## 芽菜、雞

[怡保被](../Page/怡保.md "wikilink")[蒂迪旺沙山脈](../Page/蒂迪旺沙山脈.md "wikilink")（Banjaran
Titiwangsa）所環繞，[地下水因](../Page/地下水.md "wikilink")[石灰岩的關係](../Page/石灰岩.md "wikilink")，富含礦物質，使得利用當地山泉水所培育的豆芽菜特別粗大且爽脆可口。這些[芽菜多產至於怡保附近的文冬新村](../Page/芽菜.md "wikilink")，因此當地又被稱為“芽菜的故鄉”，若用自來水來培植，則是無法形成[怡保](../Page/怡保.md "wikilink")[芽菜粗短脆口的特別口感](../Page/芽菜.md "wikilink")。

在50至60年代，當地販售[雞絲河粉的攤販為了增加菜色](../Page/雞絲河粉.md "wikilink")，使用當地生產的[芽菜](../Page/芽菜.md "wikilink")，汆燙後拌上雞油和[醬油](../Page/醬油.md "wikilink")，再撒些[胡椒粉](../Page/胡椒粉.md "wikilink")，作為招攬生意的小菜。除了[芽菜外](../Page/芽菜.md "wikilink")，其他小菜還包括[白斬雞](../Page/白斬雞.md "wikilink")、豬肉丸湯，或者滷雞腳、冬菇等。隨著怡保市民消費能力的增加，大家在點雞絲河粉時，都會搭配其他的小菜，漸漸地這種吃法開始在[怡保流行](../Page/怡保.md "wikilink")。在70、80年代間，[芽菜和](../Page/芽菜.md "wikilink")[白斬雞已成為主角](../Page/白斬雞.md "wikilink")，而[雞絲河粉逐漸演變成沒有雞絲和蝦子的清湯](../Page/雞絲河粉.md "wikilink")[沙河粉](../Page/沙河粉.md "wikilink")，形成了目前怡保芽菜雞獨特的上菜方式。消費者在使用芽菜雞時，也會搭配豬肉丸、[魚丸](../Page/魚丸.md "wikilink")、[水餃等配菜](../Page/水餃.md "wikilink")，河粉也換成是[海南雞飯所使用的油飯](../Page/海南雞飯.md "wikilink")。

## 另見

  - [雞絲河粉](../Page/雞絲河粉.md "wikilink")
  - [白斬雞](../Page/白斬雞.md "wikilink")
  - [海南雞飯](../Page/海南雞飯.md "wikilink")

## 資料來源

  - [尋找芽菜雞的身世](http://news.sinchew.com.my/node/187091)
  - [老黃芽菜雞沙河粉](http://travel.ulifestyle.com.hk/DetailSpot.php?id=ADsRYBEvA3UMLg)
  - [盧覓雪大啖咬怡保名物芽菜雞](http://travel.ulifestyle.com.hk/DetailSpot.php?id=ADsRYBEvA3UMLg)

[Category:馬來西亞食品](../Category/馬來西亞食品.md "wikilink")
[Category:馬來西亞華人美食](../Category/馬來西亞華人美食.md "wikilink")
[Category:麵條食品](../Category/麵條食品.md "wikilink")
[Category:東南亞食品](../Category/東南亞食品.md "wikilink")