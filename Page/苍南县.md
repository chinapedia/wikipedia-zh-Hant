**苍南县**，隶属于[中國](../Page/中國.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")，为[温州市現下辖五](../Page/温州.md "wikilink")[县之一](../Page/县.md "wikilink")。历史上一直属[平阳县辖域](../Page/平阳县.md "wikilink")，于1981年独立设县。

## 地理

苍南县于1981年建县，地处[浙江省最南端](../Page/浙江省.md "wikilink")，素有浙江“南天门”之称，东与东南濒临[东海](../Page/东海.md "wikilink")，西南毗连[福建省](../Page/福建省.md "wikilink")[寧德](../Page/寧德.md "wikilink")[福鼎市](../Page/福鼎市.md "wikilink")，西邻[泰顺县](../Page/泰顺县.md "wikilink")，北与[平阳](../Page/平阳县.md "wikilink")、[文成两县接壤](../Page/文成县.md "wikilink")。陆地界于东经120°07′～121°07′，北纬27°06′～27°36′。领海位于北纬27°00′～27°32′48″，东经121°07′向东至水深200米等深线以内。2006年陆地总面积为1261.08平方公里，海岸线长155公里，沿海滩涂面积97.24平方公里。人口2016年统计为118.4万人。

## 行政

县政府驻地为[灵溪镇](../Page/灵溪镇.md "wikilink")，下辖**17**个[镇](../Page/镇.md "wikilink")、**2**个[民族乡](../Page/民族乡.md "wikilink")。2010年全县总人口118.46万人。

  - [灵溪镇](../Page/灵溪镇.md "wikilink")：“中国塑编之都 中国人参鹿茸集散中心”
  - [龙港镇](../Page/龍港鎮_\(蒼南縣\).md "wikilink")：“中国第一农民城”与中国印刷基地。
  - [金乡镇](../Page/金乡镇.md "wikilink")：“中国[商标第一城](../Page/商标.md "wikilink")”
  - [矾山镇](../Page/矾山镇.md "wikilink")：“世界矾都”
  - [宜山镇](../Page/宜山镇.md "wikilink")、[钱库镇](../Page/钱库镇.md "wikilink")、[马站镇](../Page/马站镇_\(苍南县\).md "wikilink")、[桥墩镇](../Page/桥墩镇.md "wikilink")、[赤溪镇](../Page/赤溪镇_\(苍南县\).md "wikilink")、[藻溪镇](../Page/藻溪镇.md "wikilink")、[大渔镇](../Page/大渔镇.md "wikilink")、[炎亭镇](../Page/炎亭镇.md "wikilink")、[望里镇](../Page/望里镇.md "wikilink")、[莒溪镇](../Page/莒溪镇.md "wikilink")、[南宋镇](../Page/南宋镇.md "wikilink")、[霞关镇](../Page/霞关镇.md "wikilink")、[沿浦镇](../Page/沿浦镇.md "wikilink")。
  - [凤阳畲族乡](../Page/凤阳畲族乡.md "wikilink")、[岱岭畲族乡](../Page/岱岭畲族乡.md "wikilink")。
  - 苍南县有**33**个少数民族，人口**3.1**万，以[畲族和](../Page/畲族.md "wikilink")[回族为主](../Page/回族.md "wikilink")，是浙江省[少数民族人口最多的县](../Page/少数民族.md "wikilink")。

## 语言

  - [闽南语](../Page/闽南语.md "wikilink")[浙南闽语](../Page/浙南閩語.md "wikilink")（福建话）
    ---
    [灵溪镇](../Page/灵溪镇.md "wikilink")、[矾山镇](../Page/矾山镇.md "wikilink")、[马站镇](../Page/马站镇.md "wikilink")、[赤溪镇](../Page/赤溪镇.md "wikilink")、[桥墩镇](../Page/桥墩镇.md "wikilink")、[藻溪镇等乡镇](../Page/藻溪镇.md "wikilink")，以及[龙港镇](../Page/龙港镇_\(苍南县\).md "wikilink")、[金乡镇](../Page/金乡镇.md "wikilink")、[钱库镇](../Page/钱库镇.md "wikilink")、[宜山镇部分村](../Page/宜山镇.md "wikilink")
  - [吳語](../Page/吳語.md "wikilink")[温州话](../Page/温州话.md "wikilink")[瑞平小片](../Page/瑞平小片.md "wikilink")[江南瓯语](../Page/苍南瓯语.md "wikilink")（江南话）---
    [龙港镇](../Page/龙港镇.md "wikilink")、[宜山镇](../Page/宜山镇.md "wikilink")、沪山部分地区、渎浦等
  - [蛮话](../Page/蛮话.md "wikilink") ---
    [钱库镇](../Page/钱库镇.md "wikilink")，[龙港镇部分村](../Page/龙港镇.md "wikilink")，[金乡镇部分村](../Page/金乡镇.md "wikilink")
    ，[宜山镇部分村等](../Page/宜山镇.md "wikilink")
  - [畲客话](../Page/畲客话.md "wikilink") --- 一些乡镇的畲族居住区
  - [金乡话](../Page/金乡话.md "wikilink") ---
    [金乡镇](../Page/金乡镇.md "wikilink")
  - [蒲门话](../Page/蒲门话.md "wikilink") --- 马站镇蒲城办事处

## 交通

### 公路

  - （[甬台温高速公路路段](../Page/甬台温高速公路.md "wikilink")）

  -
  -
### 铁路

  - [15px](../Page/file:China_Railways.svg.md "wikilink")[温福铁路](../Page/温福铁路.md "wikilink")
  - [15px](../Page/file:China_Railways.svg.md "wikilink")[苍南火车站](../Page/苍南火车站.md "wikilink")

### 航空

苍南县内无机场，臨近机场为[温州龙湾国际机场](../Page/温州龙湾国际机场.md "wikilink")，位于温州市龙湾区。

### 水路运输

## 教育

高中：

  - [苍南中学](../Page/苍南中学.md "wikilink")
  - [龍港高级中学](../Page/龍港高级中学.md "wikilink")
  - [灵溪中学](../Page/灵溪中学.md "wikilink")（原灵溪第一高级中学）
  - [灵溪镇第二高级中学](../Page/灵溪镇第二高级中学.md "wikilink")
  - [灵溪镇第三高级中学](../Page/灵溪镇第三高级中学.md "wikilink")（原求知中学）
  - [龙港第二高级中学](../Page/龙港第二高级中学.md "wikilink")
  - [宜山高级中学](../Page/宜山高级中学.md "wikilink")
  - [钱库高级中学](../Page/钱库高级中学.md "wikilink")
  - [金乡高级中学](../Page/金乡高级中学.md "wikilink")
  - [狮山中学](../Page/狮山中学.md "wikilink")
  - [橋墩高級中學](../Page/橋墩高級中學.md "wikilink")
  - [马站镇高级中学](../Page/马站镇高级中学.md "wikilink")
  - [钱库第二高级中学](../Page/钱库第二高级中学.md "wikilink")
  - [赤溪中学](../Page/赤溪中学.md "wikilink")

初中：

  - [灵溪镇第一中学](../Page/灵溪镇第一中学.md "wikilink")
  - [灵溪镇第二中学](../Page/灵溪镇第二中学.md "wikilink")
  - [灵溪镇第三中学](../Page/灵溪镇第三中学.md "wikilink")
  - [宜山一中](../Page/宜山一中.md "wikilink")
  - [嘉乡中学](../Page/嘉乡中学.md "wikilink")
  - [马站镇初级中学](../Page/马站镇初级中学.md "wikilink")

小学：

  - [苍南县实验小学](../Page/苍南县实验小学.md "wikilink")
  - [灵溪镇第一小学](../Page/灵溪镇第一小学.md "wikilink")
  - [灵溪镇第二小学](../Page/灵溪镇第二小学.md "wikilink")
  - [宜山一小](../Page/宜山一小.md "wikilink")
  - [马站镇小学](../Page/马站镇小学.md "wikilink")

注：以上仅为部分主要学校，未完全列出。附苍南县各级各类学校名单链接：http://www.docin.com/p-120816228.html

## 风景及古迹

[Wucheng-cun-P1210812.JPG](https://zh.wikipedia.org/wiki/File:Wucheng-cun-P1210812.JPG "fig:Wucheng-cun-P1210812.JPG")

  - [玉苍山风景区](../Page/玉苍山.md "wikilink")
  - [渔寮沙滩风景区](../Page/渔寮.md "wikilink")
  - [鲸头景区](../Page/鲸头.md "wikilink")
  - 石聚堂景区
  - [文昌阁](../Page/文昌阁.md "wikilink")
  - [法云寺](../Page/法云寺.md "wikilink")
  - [刘基庙](../Page/刘基庙.md "wikilink")
  - [蒲壮所城](../Page/蒲壮所城.md "wikilink")
  - [碗窑古村落](../Page/碗窑古村落.md "wikilink")

## 名人

  - [杨忠道](../Page/杨忠道.md "wikilink") 数学家 原中央研究院院士 曾任美国宾夕法尼亚大学数学教授
  - [姜立夫](../Page/姜立夫.md "wikilink") 数学家 南开大学数学系的创始人 曾任中央研究院数学所所长
  - [姜伯驹](../Page/姜伯驹.md "wikilink") 数学家 中国科学院院士 第三世界科学院院士 北京大学教授
  - 李锐夫 数学教授 数学教育工作者 曾任上海市高等教育局副局长
  - [殷汝驪](../Page/殷汝驪.md "wikilink")
    曾署理[北洋政府財政部次長](../Page/北洋政府.md "wikilink")
    民国初年国会议员 [中国同盟会成员](../Page/中国同盟会.md "wikilink") 曾任江蘇省銀行總經理
  - [殷之浩](../Page/殷之浩.md "wikilink")
    [大陸工程股份有限公司原董事長](../Page/大陸工程股份有限公司.md "wikilink")
    台湾建筑业（营造业）先驱 世界不動產聯合會FIABC總會長 宏基电脑股份有限公司名誉董事长 殷汝驪之子
  - [殷琪](../Page/殷琪.md "wikilink")（籍貫苍南金乡，台湾出生）
    [台湾高速铁路公司原董事长](../Page/台湾高速铁路.md "wikilink")
    [台北之音董事长](../Page/台北之音.md "wikilink") 中華民國總統府原國策顧問 殷之浩之女
  - [殷一璀现任中共上海市委副书记](../Page/殷一璀.md "wikilink")
  - 王均瑶 [均瑶集团创始人及原董事长](../Page/均瑶集团.md "wikilink")，溫州龍灣國際機場原承包人
  - [王均金](../Page/王均金.md "wikilink")
    [均瑶集团现任董事长](../Page/均瑶集团.md "wikilink")
    [吉祥航空公司董事长](../Page/吉祥航空.md "wikilink") 王均瑶之弟
  - [张翎](../Page/张翎.md "wikilink") 作家 获得多项文学奖项
    作品有：《余震》（[冯小刚导演电影](../Page/冯小刚.md "wikilink")《唐山大地震》根据此书改编）、《金山》等

## 参考文献

## 外部链接

  - [苍南县政府网站](http://www.cncn.gov.cn/)

{{-}}

[Category:浙江省县份](../Category/浙江省县份.md "wikilink")
[苍南县](../Category/苍南县.md "wikilink")
[Category:温州县级行政区](../Category/温州县级行政区.md "wikilink")