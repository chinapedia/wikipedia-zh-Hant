[Cangxia_church.JPG](https://zh.wikipedia.org/wiki/File:Cangxia_church.JPG "fig:Cangxia_church.JPG")

**苍霞洲基督堂**是[英国圣公会福建教区的](../Page/英国圣公会.md "wikilink")[主教座堂](../Page/主教座堂.md "wikilink")，位于今[福州市](../Page/福州市.md "wikilink")[台江区](../Page/台江区.md "wikilink")[苍霞洲](../Page/苍霞洲.md "wikilink")[交通巷](../Page/交通巷.md "wikilink")17号。

福建教区是[英国圣公会在中国开创的规模最大的一个](../Page/英国圣公会.md "wikilink")[教区](../Page/教区.md "wikilink")。1850年该会传入福州，初期受到当地士绅强烈反对，1862年，在福州建造的第一座教堂南后街萃贤堂也被烧毁。但传教士们继续不懈的工作，终于使福建成为圣公会在中国最重要的传教区。1906年，[中华圣公会福建教区脱离香港主教](../Page/中华圣公会福建教区.md "wikilink")（港粵教區）的管辖，成为一个独立教区，在[中华圣公会](../Page/中华圣公会.md "wikilink")11个教区中信徒人数遥遥领先。第一任[主教是贝嘉德](../Page/主教.md "wikilink")（1906—1918年）。

该堂创建于1870年，最初在苍霞洲广裕楼租房设立布道所，信徒增加后，1882年，以1800银元买下倚霞桥1号的一个旧茶仓。1924年11月1日[诸圣日](../Page/诸圣日.md "wikilink")，圣公会福建教区主教[恒约翰在此主持奠基仪式](../Page/恒约翰.md "wikilink")，开工建造主教座堂，到1927年11月13日全部建成，举行[座堂落成祝圣典礼](../Page/座堂.md "wikilink")，由[中华圣公会主教院主席](../Page/中华圣公会主教院.md "wikilink")[鄂方智主持并讲道](../Page/鄂方智.md "wikilink")。堂为砖木结构，英国建筑风格，可容纳1000人。中华圣公会重要会议常在此召开。

1950年，中国大陆基督教的[三自爱国运动的签名活动由该堂开始](../Page/三自爱国运动.md "wikilink")。1958年，教堂部分场所被台江制药厂占用。1966年8月17日，[文革开始后](../Page/文革.md "wikilink")，座堂被关闭，全部被药厂占用。

1985年8月11日改名**苍霞堂**重新开放。1994年有信徒1500人，仍是福州市一座重要的基督教堂。

苍霞洲基督堂是福州现存的老教堂之一，被列为台江区文物保护单位\[1\] 。

## 参考文献

<div class="references-small">

<references />

</div>

{{-}}

[Category:福州教堂](../Category/福州教堂.md "wikilink")
[Category:中国前圣公会座堂](../Category/中国前圣公会座堂.md "wikilink")
[Category:福州前圣公会教堂](../Category/福州前圣公会教堂.md "wikilink")

1.  [区级文物保护单位](http://www.fzdqw.com/showtext.asp?ToBook=1110&index=1147&)