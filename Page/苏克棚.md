**苏克棚**是[犹太人在庆祝](../Page/犹太人.md "wikilink")[住棚节期间所住的临时棚屋](../Page/住棚节.md "wikilink")。

## 结构

[Porchsukka.jpg](https://zh.wikipedia.org/wiki/File:Porchsukka.jpg "fig:Porchsukka.jpg")
苏克棚的墙壁可以用从木材到帆布到铝的各种材料建造，而建造屋顶的材料包括松树枝、棕榈叶和竹子。墙壁也可以部分利用房屋或栅栏。有关墙壁结构、高度、间距、材料的详细资料在各种解经书中都可以找到。

## 装饰

[He_wiki_sucot.jpg](https://zh.wikipedia.org/wiki/File:He_wiki_sucot.jpg "fig:He_wiki_sucot.jpg")

许多人在苏克棚的内墙和天花板上悬挂塑料水果、彩带、闪亮的装饰物和图画。全家还会沿着内墙排成一队，回顾以色列人在旷野中由云柱带领的情形。

  - [亚伯拉罕](../Page/亚伯拉罕.md "wikilink")
  - [以撒](../Page/以撒.md "wikilink")
  - [雅各](../Page/雅各.md "wikilink")
  - [约瑟](../Page/約瑟_\(舊約聖經\).md "wikilink")
  - [摩西](../Page/摩西.md "wikilink")
  - [亚伦](../Page/亚伦.md "wikilink")（摩西的哥哥，第一位[大祭司](../Page/大祭司_\(犹太教\).md "wikilink")）
  - [大卫王](../Page/大卫王.md "wikilink")

## 参考文献

## 外部連結

  - [Sukkah
    City](http://www.nytimes.com/slideshow/2010/09/16/arts/design/0917-sukkah-slideshow.html)
    - slideshow by *[The New York
    Times](../Page/The_New_York_Times.md "wikilink")*
  - [What On Earth Is A
    Sukkah?](http://www.npr.org/blogs/pictureshow/2010/09/23/130078125/sukkah)
    - slideshow by *[NPR](../Page/NPR.md "wikilink")*
  - [Sukkahs](http://www.sukkahworld.com/) - 4 Modern Examples
  - [Hilchot
    Succah](https://web.archive.org/web/20130928204242/http://www.sukkahdepot.com/english/Article.aspx?Item=134)
    by Sukkot Nehalim Knai Suf
  - [sukkahsoftheworld.org](http://sukkahsoftheworld.org/) pictures of
    sukkahs from Sharon to Shanghai
  - [Local Sukkah](http://www.localsukkah.org/) worldwide listing of
    sukkahs available for public use
  - [The Laws of the
    Succah](http://www.yeshiva.co/midrash/shiur.asp?cat=74&id=958&q) by
    Rabbi Eliezer Melamed

[Category:犹太教](../Category/犹太教.md "wikilink")
[Category:小屋](../Category/小屋.md "wikilink")