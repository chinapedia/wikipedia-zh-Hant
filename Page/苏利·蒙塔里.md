**蘇利文·阿里·"蘇利"·蒙塔里**（，）是一位[加納](../Page/加納.md "wikilink")[職業職業足球員](../Page/職業.md "wikilink")，曾效力多支[意甲球會包括](../Page/意甲.md "wikilink")[烏甸尼斯](../Page/乌迪内斯足球俱乐部.md "wikilink")、[國際米蘭和](../Page/國際米蘭.md "wikilink")[AC米蘭](../Page/AC米蘭.md "wikilink")。

## 生平

出生於[非洲國家](../Page/非洲.md "wikilink")[加納的蒙達利早在十八歲就已到大利加盟意甲球會](../Page/加納.md "wikilink")[烏甸尼斯](../Page/乌迪内斯足球俱乐部.md "wikilink")。在此之前曾入選國家隊出賽[2001年世青杯](../Page/2001年國際足協世界青年錦標賽.md "wikilink")，贏得一個亞軍。在烏甸尼斯他則為球會贏得聯賽第四名，取得2004年歐聯入場券。

司職中場的蒙達利被[英國電視足球節目](../Page/英國.md "wikilink")*Football
Italia*評為「他擁有[卡卡的觸覺和](../Page/卡卡.md "wikilink")[戴維斯的鬥心](../Page/埃德加·戴维斯.md "wikilink")」。在2005年8月31日，轉會市場關閉的時候，烏甸尼斯宣佈他們拒絕把蒙達利賣到[英超球隊](../Page/英超.md "wikilink")[曼聯](../Page/曼聯.md "wikilink")。諷刺的是，曼聯本來可以在2001年蒙達利還是青年球員的時候把他買入，而不須付出100萬[英鎊的轉會費](../Page/英鎊.md "wikilink")。最終，烏甸尼斯與蒙達利續約，並以頭十名完成[意甲聯賽](../Page/意大利足球甲级联赛.md "wikilink")。

2007年夏季蒙達利以破球會轉會費紀錄的700萬[英鎊加盟](../Page/英鎊.md "wikilink")[英超的](../Page/英超.md "wikilink")[朴茨茅斯](../Page/朴茨茅斯足球俱乐部.md "wikilink")，穿上11號球衣。

2008年夏季蒙達利以1,600萬[歐元的身價從](../Page/歐元.md "wikilink")[樸茨茅夫加盟](../Page/樸茨茅夫足球會.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")\[1\]\[2\]\[3\]，並與球隊簽約5年。蒙達利是一位在[意甲成長起來的球員](../Page/意甲.md "wikilink")，上賽季效力樸茨茅夫征戰英超，表現不俗，樸茨茅夫主帥[哈利·列納也是最終迫不得已才放人](../Page/哈利·列納.md "wikilink")。蒙達利本人則表示選擇國際米蘭有兩個原因，一是國際米蘭是豪門球隊，這是不可多得的機會，二是他非常崇拜[-{zh-hk:摩連奴;zh-cn:穆里尼奥}-](../Page/摩連奴.md "wikilink")。2008年8月28日，蒙達利正式完成轉會手續，並選擇了20號球衣\[4\]。

一度是球隊當然正選的蒙達利於2009/10年球季開始失去正選席位，翌季上陣機會更少，2011年1月16日蒙達利向[國際米蘭提出轉會要求](../Page/國際米蘭.md "wikilink")\[5\]，1月29日以外借身份加盟[英超球會](../Page/英超.md "wikilink")[新特蘭直到球季結束](../Page/桑德兰足球俱乐部.md "wikilink")\[6\]。

2011-12赛季蒙塔里重返国际米兰，然而依然没有获得太多的上场机会，终于在1月31日冬季转会窗结束前，国际米兰俱乐部宣布蒙塔里以租借形式加盟[AC米兰](../Page/AC米兰.md "wikilink")，租期为半年。蒙塔里在参加[2012年非洲國家盃后前往AC米兰](../Page/2012年非洲國家盃.md "wikilink")，2月19日，他首次代表AC米兰出场即取得进球，帮助球队3比1击败[切塞纳](../Page/切塞纳足球俱乐部.md "wikilink")。

## 榮譽

### 國家隊

  - [世青盃亞軍](../Page/世青盃.md "wikilink")：2001年

### 球會

  - 樸茨茅夫

<!-- end list -->

  - [英格蘭足總盃冠軍](../Page/英格蘭足總盃.md "wikilink")：2008

<!-- end list -->

  - 国际米兰

<!-- end list -->

  - [意大利足球甲级联赛冠軍](../Page/意大利足球甲级联赛.md "wikilink")：2009、2010
  - [意大利超级杯冠军](../Page/意大利超级杯.md "wikilink")：2008、2010
  - [意大利杯冠军](../Page/意大利杯.md "wikilink")：2010
  - [欧洲冠军联赛冠军](../Page/欧洲冠军联赛.md "wikilink")：2010
  - [世俱杯冠军](../Page/世俱杯.md "wikilink")：2010

## 參考資料

## 外部連結

  -
[Category:迦納穆斯林](../Category/迦納穆斯林.md "wikilink")
[Category:迦納足球運動員](../Category/迦納足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:自由職業運動員球員](../Category/自由職業運動員球員.md "wikilink")
[Category:烏甸尼斯球員](../Category/烏甸尼斯球員.md "wikilink")
[Category:樸茨茅夫球員](../Category/樸茨茅夫球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:新特蘭球員](../Category/新特蘭球員.md "wikilink")
[Category:伊蒂哈德球員](../Category/伊蒂哈德球員.md "wikilink")
[Category:佩斯卡拉球員](../Category/佩斯卡拉球員.md "wikilink")
[Category:拉科魯尼亞球員](../Category/拉科魯尼亞球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2008年非洲國家盃球員](../Category/2008年非洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2012年非洲國家盃球員](../Category/2012年非洲國家盃球員.md "wikilink")
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")

1.
2.
3.
4.
5.
6.