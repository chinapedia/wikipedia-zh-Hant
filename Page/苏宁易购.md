**苏宁易购集团股份有限公司**（简称**苏宁易购**，），原名**苏宁云商集团股份有限公司**（簡稱**苏宁云商**），是中国的一家连锁型零售和地产开发企业，前身为江苏苏宁交家电有限公司、苏宁电器股份有限公司。2004年7月7日公司成功实现首次公开发行A股，7月21日公司在[深圳证券交易所](../Page/深圳证券交易所.md "wikilink")[上市](../Page/上市.md "wikilink")。

截至2012年，苏宁电器连锁网络覆盖中国大陆30个省，300多个城市、香港和日本地区，拥有1705家连锁店，80多个物流配送中心、3000家售后网点，经营面积500万平米，员工12万多人，年销售规模1240亿元。品牌价值455.38亿元，蝉联中国商业连锁第一品牌。名列中国上规模民企前三，中国企业500强第54位，入选《[福布斯](../Page/福布斯.md "wikilink")》亚洲企业50强、《福布斯》全球2000大企业中国零售企业第一。

2016年12月30日，蘇寧子公司江蘇蘇寧物流以29.75億人民幣現金收購天天快遞70%股份；剩餘的30%的股份作價12.75億元，將在交割完成後12個月內完成。

## 名称

苏宁公司早年位于[江苏省](../Page/江苏省.md "wikilink")[南京市](../Page/南京市.md "wikilink")[江苏路和](../Page/江苏路_\(南京\).md "wikilink")[宁海路之间](../Page/宁海路.md "wikilink")，因而取名为“苏宁”。\[1\]

2013年2月19日，苏宁电器公告称，基于线上线下多渠道融合、全品类经营、开放平台服务的业务形态，苏宁拟将公司名称变更为“苏宁云商集团股份有限公司”，以更好的与企业经营范围和商业模式相适应。此次更名可看做是苏宁电器的科技转型战略迈出的又一大步，也宣告着苏宁“云商”新模式的正式面世。苏宁电器高调宣布更名苏宁云商，更改标识，大肆调整公司架构。所谓云商是指“店商+电商+零售服务商”相结合的新零售业模式。苏宁此次变革，其电商平台苏宁易购地位也得到明显提升和重视，张近东的电商战略已经非常明显。\[2\]

2018年1月14日，苏宁云商发布公告，称公司董事会已全票通过变更公司名称事项，决定将公司名再次变更为“苏宁易购”\[3\]。2018年2月，公司名由“苏宁云商”正式变更为“苏宁易购”\[4\]。

## 概览

  - 1990年，苏宁创立于南京，经过20多年的高速发展，苏宁已经成为中国最大的商业零售企业，位列民营企业前三强，中国企业500强第39位，品牌价值高达956.86亿元。
  - 苏宁1600多家连锁店覆盖中国大陆、香港、日本的700多个城市，旗下电子商务平台苏宁易购位居中国B2C行业前三甲，经营品类涵盖家电、3C、图书、百货、日用品、化妆品及母婴等实体商品，以及内容产品、服务商品等，SKU总数超过300万。2012年苏宁销售规模达2300亿元人民币，员工18万人。

## 历史

[缩略图](https://zh.wikipedia.org/wiki/File:HK_Mongkok_西洋菜南街_Sai_Yeung_Choi_Street_South_蘇寧鐳射_Suning_shop_night_July-2011.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:HK_Tai_Po_Mega_Mall_大埔超級城_Suning_shop_blue_sign_Jan-2013.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:HK_TST_night_金馬倫道_Cameron_Road_shop_signs_Chow_Sang_San_n_Suning_March-2013.JPG "fig:缩略图")

  - 企业初创树立品牌

1990年12月26日，苏宁诞生于中国南京宁海路60号，专营一家200平米的空调店。

1991年，苏宁组建售后服务中心，树立“专业自营”的售后服务品牌。

1993年，苏宁凭借“厂商合作、专业服务、规模经营”三张王牌，在与南京八大商场对垒中胜出，演绎了代表计划经济转型的“苏宁现象”。

  - 空调专营行业第一

1994年，苏宁立足空调专营，自建专业售后服务队伍，树立了“服务为本”的苏宁品牌，销售持续增长，成为中国最大的空调销售企业。

1996年，苏宁走出南京，在扬州开设第一家外埠公司，揭开了连锁发展的序幕；与此同时，率先启动电脑开票系统，实现销售、财务一体化和会计电算化。

  - 壮大规模起步连锁

1997年，苏宁投资3000万元在南京自建第一代物流配送中心和10个售后服务网点，初步形成了“前后台协同发展，后台优先”的经营管理模式。

1998年，苏宁把握行业发展趋势，实施二次创业，向综合电器连锁经营转型。

1999年，苏宁首家自建店——南京新街口旗舰店成功开业，标志着苏宁从空调专营转型到综合电器全国连锁经营。

  - 综合电器探路连锁

2000年，苏宁提出“3年1500店”的目标，全面推进综合电器全国连锁发展。

2001年，苏宁进行内部组织再造，建立了以“专业化分工、标准化作业”为基础的矩阵式管理架构和第一代电器连锁专业ERP信息系统。

2002年，苏宁连锁网络从南京走向浙江、北京、上海、天津、重庆等地，初步建立了全国连锁发展的战略布局。

  - 开发提速全国布局

2003年，苏宁在全国横向扩张、纵向渗透，创立3C旗舰店的经营模式，连锁驶入快车道。

2004年7月21日，苏宁电器（002024）在深交所成功挂牌上市，成为中国家电行业第一品牌。

2005，苏宁商业零售进入武汉，在行业内率先完成全国一级市场网络布局。

  - 连锁升级稳健发展

2006年，苏宁以第五代3C+旗舰店为主导，建立了租、建、购、并四位一体的开发方式，形成了以“内生增长，后台优先”为核心的发展模式，并实施了中国零售业信息化1号工程——上线
SAP/ERP系统，建立了集团化、全球化的经营管理平台。

2007年，苏宁启动“蓝深计划”，苏宁对经营管理平台进一步优化、升级。

  - 三年攻略行业领先

2008年，苏宁实施“三年攻略”，目标到2010年实现行业领先；并推进全国大开发。

2009年，苏宁以1170亿元的销售规模成为中国最大的商业流通企业，提前实现行业领先。成功收购日本LAOX和香港镭射电器公司，迈开了国际化发展步伐。

2010年，苏宁成立20周年，苏宁易购上线运营，连锁深入三、四级县镇市场。

  - 科技转型智慧服务

2011年，苏宁启动以“科技转型，智慧服务”为方向的发展规划，目标到2020年，实体店面将达3500家、销售规模3500亿元，网购销售突破3000亿元，海外销售达100亿美元，并完成300个苏宁生活广场、50家购物中心和100家高星级酒店的建设，多元产业营业收入将达4000亿元，将以万亿级的销售规模跻身跻身世界一流企业行列。

2012年，苏宁沿着智慧化、多元化、国际化的轨道稳健发展，连锁零售持续领先，苏宁连锁与乐购仕连锁双品牌持续推进，超级店、私享家等创新举措不断推出，苏宁易购快速增长，多元产业快速协同发展。新十年、新苏宁战略实施迈出坚实的步伐。

2013年，苏宁正式公布新模式、新组织、新形象，实施线上线下同价战略，升级开放平台，标志着行业革命性的“云商”模式全面落地，开启了跨越式发展的新征程。

美国旧金山时间2013年11月19日，“苏宁美国研发中心暨硅谷研究院”在硅谷隆重揭幕，苏宁董事长张近东正式宣布其全球首家海外研究院开始运行。苏宁于今年明确发布“一体两翼互联网路线图”，就是要以互联网零售为转型总体方向，以O2O和开放平台为两翼，运用互联网技术提升零售业的运营效率与服务体验，苏宁硅谷研究院将为这一愿景提供技术牵引。\[5\]

2014年1月蘇寧雲商日前通過國家郵政局快遞業務經營許可審核，獲准經營國際快遞業務，成為內地首家獲得國際快遞運營牌照的電商。

2015年8月10日，蘇寧雲商旗下PPTV获得2015—2020赛季西甲联赛中国大陆地区的獨家播映權。(其後可延續至2021-22赛季)\[6\]\[7\]\[8\]

2015年8月10日，[阿里巴巴集团宣布將以每股](../Page/阿里巴巴集团.md "wikilink")15.23元人民幣，斥資283億元人民幣战略投资[蘇寧雲商非公開發行Ａ股股票不超過約](../Page/蘇寧雲商.md "wikilink")19.2667億股，佔已擴大股本的19.99％股份成为第二大股东；[蘇寧雲商将以](../Page/蘇寧雲商.md "wikilink")140亿元人民币认购不超过2780万股的阿里新发行股份；双方将打通线上线下全面提升效率，为中国及全球消费者提供更加完善商业服务。\[9\]

2015年苏宁云商向母公司苏宁控股集团出售[PPTV](../Page/PPTV.md "wikilink")。

2016年6月6日，蘇寧雲商母公司苏宁控股集团有限公司收购[江苏舜天足球俱乐部的](../Page/江苏苏宁足球俱乐部.md "wikilink")100%的股份。

2016年6月6日，蘇寧雲商母公司苏宁控股集团将花2.7亿欧元收购意甲球队[国际米兰](../Page/国际米兰.md "wikilink")68.55%的股份，苏宁董事长张近东表示“这是苏宁国际发展进程中的一个重要组成部分。”\[10\]

2016年11月18日, 蘇寧雲商旗下PPTV取得三季 （2019-2022）英格蘭超級聯賽中国大陆地区的獨家播映權\[11\]

2018年4月4日[蘇寧易購公布](../Page/蘇寧易購.md "wikilink")，已就收購[迪亞天天超市中國](../Page/迪亞天天超市.md "wikilink")100%股權事宜，與西班牙連鎖超市迪亞簽訂有約束力的購買與出售協議，惟尚未披露交易財務細節，迪亞在上海擁有約250萬會員，超過300家門店密集布於上海主城區，且基本均處於社區商圈，與蘇寧小店的社區定位一致。

2018年4月4日[蘇寧易購旗下](../Page/蘇寧易購.md "wikilink")[樂購仕將投資](../Page/樂購仕.md "wikilink")12.04億日元收購L
Capital Tokyo 60%的股權L Capital
Tokyo持有日本最大的连锁礼物零售商[Shaddy在日本有](../Page/Shaddy.md "wikilink")3000多家門店，出售各式禮物和伴手禮，如日式點心、生活用品、美妝等，是日本最大的連鎖禮物零售商之一。截止至2017年2月的14個月中，Shaddy
的銷售額達到706.6億日元，經營虧損8.6億日元，淨負債9億日元。

2018年5月30日[蘇寧易購公布](../Page/蘇寧易購.md "wikilink")，出售部分阿里巴巴股份，預計可實現淨利潤約56.01億元人民幣。目前蘇寧易購仍持有0.51%阿里巴巴股份。

2018年7月10日，蘇寧雲商旗下PPTV获得2018-2023赛季德甲联赛中国大陆地区的獨家播映權;\[12\]\[13\]
7月28日再取得法甲2018-2021赛季中国大陆地区的新媒体独家版权。\[14\]

2018年8月3日[蘇寧易購以約](../Page/蘇寧易購.md "wikilink")34億元人民幣認購[華泰證券](../Page/華泰證券.md "wikilink")2.61億股份，交易完成後，[蘇寧易購将持有華泰證券](../Page/蘇寧易購.md "wikilink")3.1574%股份，位列第七大股東

2019年2月12日[蘇寧易購以最高不超過](../Page/蘇寧易購.md "wikilink")89.233億元人民幣向[万达集团購入](../Page/万达集团.md "wikilink")[萬達百貨全數](../Page/萬達百貨.md "wikilink")37間分店

## 苏宁云商子公司

### 苏宁易购

苏宁易购是苏宁云商股份有限公司旗下的一个B2C电子商务网站，销售家电、电脑、母婴等各种日常商品。其主要面对中国大陆的在线消费品零售市场，总部位于江苏省南京市。截止2012年底，苏宁易购是中国大陆按照销售营业额排名第三的B2C零售平台。

### 香港苏宁云商

香港苏宁云商是香港的一间大型电器零售连锁店，现为苏宁云商全资附属公司，前身为旺角相机中心及镭射发展，于1976年成立，2012年销售规模57.73亿人民币，位列香港电器连锁零售商前三甲。

### 红孩子

红孩子是一个母婴用品电子商务网站，成立于2004年3月，在线销售以母婴、化妆、食品、家居、保健、厨电等品类为主的，共5万多种商品。

2012年9月25日，苏宁电器正式宣布以6600万美金收购母婴电商品牌红孩子。在此之前，苏宁已先后进入3C、化妆品、图书、酒类等用户粘性较高的领域，但这一次对垂直电商龙头红孩子的并购才真正是苏宁向“去电器化”迈出实质性的一步。

### 天天快递

“天天快递”品牌创建于1994年。

2017年1月3日，苏宁云商旗下子公司江苏苏宁物流有限公司宣布收购估值42.5亿的天天快递全部股份\[15\]。2017年10月28日，将其总部迁至南京苏宁总部园区\[16\]。由苏宁物流常务副总裁[姚凯担任天天快递总裁](../Page/姚凯.md "wikilink")\[17\]。

### 乐购仕

日本著名家電連鎖零售企業（乐购仕）創立於1930年5月，蘇寧電器在2009年8月注資入股，並在2011年再次注資實現絕對控股。

## 關聯公司

### 苏宁置业

苏宁置业创立于2002年，是苏宁旗下大型房地产开发集团，依托集团在商业零售领域的经营规模和经验，苏宁置业着力于塑造专业地产品牌的同时，彰显建筑城市生活与建筑之关系，打造属于消费者的全方位生活平台。苏宁置业携手全球顶尖建筑设计公司，在全国鼎力开发商业地产，同时涵盖住宅地产、科技地产、旅游地产及物流工业地产的开发。目前全国共90多个项目，开发面积近两千万平米。

苏宁置业商业地产在[北京](../Page/北京市.md "wikilink")、[上海](../Page/上海市.md "wikilink")、[南京](../Page/南京市.md "wikilink")、[天津](../Page/天津市.md "wikilink")、[成都](../Page/成都市.md "wikilink")、[包头等全国众多一](../Page/包头市.md "wikilink")、二线城市，储备开发了数十座苏宁广场、苏宁生活广场等系列产品，业态涵盖大型购物中心、生活广场、五星级酒店、甲级写字楼、酒店式公寓等。在建项目均为各城市核心商圈地标性建筑，400m南京奥体苏宁广场、388m新街口苏宁广场、338m镇江苏宁广场、328m无锡苏宁广场、266m徐州苏宁广场、100m青岛李沧苏宁生活广场等一系列大型商业综合体项目正全面开发建设。

### PPTV

PPLive上海聚力传媒技术有限公司是中国第一家向海外输出中国自主知识产权技术及专利，并被诸如哈佛、麻省理工、微软研究院等国际知名企业和机构广泛引用的互联网视频企业。

2013年10月28日苏宁云商与[联想集团旗下弘毅资本联手](../Page/联想集团.md "wikilink")，斥资4.2亿美元（约33亿港元）入股PPTV，苏宁将成为PPTV单一最大股东。根据交易条款，苏宁云商及弘毅资本将分别出资2.5亿及1.7亿美元，交易完成后，苏宁将持有PPTV约44%股权，成为大股东。

## 社会公益

苏宁公益理念为“扎根社会壮大企业”。\[18\]苏宁在《[财富](../Page/财富.md "wikilink")》中国企业社会责任100强排名中位于前20名。\[19\]

「1+1阳光行」苏宁社工志愿者行动号召苏宁的员工自愿承担社会责任。每年至少用一天的时间进行社会自愿者活动，每年捐出至少1天的工资进行公益捐助。阳光服务（Sunning）是苏宁（Suning）的代名词。\[20\]

除了向慈善机构捐款外，苏宁还发起了为贫困地区修建校舍的“苏宁筑巢行动”\[21\]；帮助偏远地区修桥解决出行难问题的“苏宁溪桥工程”\[22\]；资助乡村教育的“苏宁阳光梦想”。另外苏宁电器在每年还在中国各地区赞助植树绿化活动。\[23\]\[24\]\[25\]\[26\]

2013年12月26日，沿袭一贯“公益庆生”的传统，苏宁开展公益捐赠及阳光行活动，活动内容包括捐赠3695万元、开展“阳光1+1”社工志愿者行动、发出1000万元捐赠的苏宁猜想，以及邀请全国权威公益机构、知名专家举办主题为“网聚仁的力量”的“2014年企业公益论坛”。将互联网转型运用到公益事业创新上，探讨公益创新与互联网的融合之道。\[27\]

## 参考文献

## 外部链接

  - [苏宁云商](http://www.suning.cn/)
  - [苏宁易购](http://www.suning.com/)

[\*](../Category/苏宁云商.md "wikilink")
[Category:中国民营企业](../Category/中国民营企业.md "wikilink")
[Category:南京公司](../Category/南京公司.md "wikilink")
[Category:中国零售商](../Category/中国零售商.md "wikilink")
[Category:電器零售商](../Category/電器零售商.md "wikilink")
[Category:1990年中国建立](../Category/1990年中国建立.md "wikilink")
[Category:1990年成立的公司](../Category/1990年成立的公司.md "wikilink")

1.
2.  [苏宁电器拟更名
    去电器化迈出实质步伐](http://www.nbd.com.cn/articles/2013-02-20/715373.html)
    ，每日经济新闻
3.
4.
5.
6.
7.
8.
9.  [阿里283亿元人民币入股苏宁，成第二大股东：互拉股价的节奏？](http://www.huxiu.com/article/122711/1.html)
10.
11.
12.
    PP体育与德甲达成五年全媒体独家版权合作|accessdate=2018-09-07|date=2018-07-10|work=www.sohu.com}}
13.
14.
15.
16.
17.
18. [苏宁公益理念：扎根社会壮大企业](http://news.xinhuanet.com/tech/2012-12/28/c_124159422.htm)
    ，新华网
19. [2013中国企业社会责任100强排行榜](http://www.fortunechina.com/rankings/c/2013-03/12/content_147601.htm)，财富中文网
20. [1+1阳光行——苏宁社工志愿者行动](http://www.cnsuning.com/snsite/index/gysn/qyshzr/ygx/ygx-1.html)
    ,苏宁电器官方网站
21. [“苏宁·筑巢行动”项目简介](http://www.cnsuning.com/sncms/sn_view.do?method=viewContentDream&contentid=683&siteid=45)
    ,苏宁电器官方网站
22. [苏宁·溪桥工程](http://www.cnsuning.com/sncms/sn_view.do?method=xiBridge)
    ，苏宁电器官方网站
23. [2013绿色植树公益活动
    苏宁电器在行动](http://finance.china.com.cn/roll/20130312/1325089.shtml),中国网
24. [3月11日
    苏宁公益植树邀您来](http://news.ifeng.com/gundong/detail_2012_03/06/13001677_0.shtml)，凤凰网
25. [苏宁易购、苏宁电器联合驴妈妈举办公益植树行动](http://news.xinhuanet.com/tech/2012-12/25/c_124145815.htm)
    ，新华网
26. [本报携手北京苏宁邀三百市民公益植树](http://www.morningpost.com.cn/szb/html/2013-04/08/content_218623.htm),北京晨报
27. [公益先行
    苏宁23周年庆捐3695万并推公益大猜想](http://tech.huanqiu.com/Enterprise/2013-12/4700262.html)
    ，环球网