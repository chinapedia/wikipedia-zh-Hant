**苏州趣普仕足球俱乐部**，是一支[中国的已解散足球俱乐部](../Page/中国.md "wikilink")，球队成立于2004年1月，是[苏州市第一家职业足球俱乐部](../Page/苏州市.md "wikilink")。2009年，苏州趣普仕并入了[宁波华奥](../Page/宁波华奥.md "wikilink")。

前[中国国家足球队队长及](../Page/中国国家足球队.md "wikilink")[亚洲足球先生的](../Page/亚洲足球先生.md "wikilink")[范志毅曾是俱乐部的技术总监以及主教练](../Page/范志毅.md "wikilink")，但由于俱乐部出现劳资纠纷，替球员追讨工资的范志毅与俱乐部管理层产生矛盾，带教练组索要欠薪遭集体解聘\[1\]
。经受劳资纠纷的球队元气大伤，苏州趣普仕在乙级联赛中属于中下游水平。

2008年2月，苏州趣普仕与[镇江中安合併](../Page/镇江中安.md "wikilink")，俱乐部名称为**苏州趣普仕中安足球俱乐部**\[2\]。球队在08年中乙南区仅排第5，未能进入升级淘汰赛阶段。

2009年，苏州趣普仕整队并入了[宁波华奥](../Page/宁波华奥.md "wikilink")。

## 著名球员

  - [华尔康](../Page/华尔康.md "wikilink")
  - [詹可强](../Page/詹可强.md "wikilink")

## 注釋

<div class="references-small">

<references />

</div>

## 外部链接

[苏州趣普仕足球俱乐部官方网站](https://web.archive.org/web/20070325044951/http://www.sztripsfootball.com/)

[Category:中国已解散足球俱乐部](../Category/中国已解散足球俱乐部.md "wikilink")
[Category:江苏足球](../Category/江苏足球.md "wikilink")
[Category:苏州体育](../Category/苏州体育.md "wikilink")
[Category:2004年建立的足球俱樂部](../Category/2004年建立的足球俱樂部.md "wikilink")
[Category:2009年解散的足球俱樂部](../Category/2009年解散的足球俱樂部.md "wikilink")
[Category:2004年中國建立](../Category/2004年中國建立.md "wikilink")
[Category:2009年中國廢除](../Category/2009年中國廢除.md "wikilink")

1.  [东北新闻网：范志毅带教练组索要欠薪被集体解聘](http://news.nen.com.cn/72344609522450432/20070825/2297485.shtml)

2.  [新浪体育：新赛季中乙趣普仕与镇江合并
    主场仍是苏州体育中心](http://sports.sina.com.cn/c/2008-02-20/10163482856.shtml)