**苏庇路里乌玛二世**（活动时期公元前13世纪末）是[赫梯国王](../Page/赫梯.md "wikilink")（大致的统治时间：前1218年～前1200年或前1210年～前1180年；实际时间无法确定）。他是赫梯[新王国的最后一个为人所知的国王](../Page/新王国_\(赫梯\).md "wikilink")。\[1\]

苏庇路里乌玛二世是赫梯国王[图特哈里四世之子](../Page/图特哈里四世.md "wikilink")。有关他的统治情况，保存下来的资料很少：已知在他在位时赫梯发生过一次[饥荒](../Page/饥荒.md "wikilink")，为此国王接受了来自[埃及](../Page/埃及.md "wikilink")[法老](../Page/法老.md "wikilink")[迈伦普塔的谷物援助](../Page/迈伦普塔.md "wikilink")。\[2\]

苏庇路里乌玛二世的对外活动主要是针对[阿拉西亚的](../Page/阿拉西亚.md "wikilink")。阿拉西亚原本臣服于赫梯，但在帝国业已衰落时不再接受控制。苏庇路里乌玛二世可能在另一个[城邦](../Page/城邦.md "wikilink")[乌加列建立了一个](../Page/乌加列.md "wikilink")[港口基地](../Page/港口.md "wikilink")，用于集结进攻阿拉西亚的[舰队](../Page/舰队.md "wikilink")。他与阿拉西亚进行了3次海战，然后又用地面部队攻击它；从现有资料看，他显然是获胜了。为此他立了一块纪念碑。苏庇路里乌玛二世在赫梯帝国风雨飘摇之时保持了对阿拉西亚城邦的宗主权。

苏庇路里乌玛二世是[亚述国王](../Page/亚述.md "wikilink")[图库尔蒂-尼努尔塔一世的同时代人](../Page/图库尔蒂-尼努尔塔一世.md "wikilink")，但没有证据显示他们发生过联系。

## 注释

## 参考

  - Astour, AJA 69 (1965)
  - Güterbock, JNES 26 (1967), 73-81

## 外部链接

  - [Reign of Suppiluliuma
    II](https://web.archive.org/web/20131104112704/http://www.hittites.info/history.aspx?text=history%2FLate+Late+Empire.htm#Suppiluliuma2)

[Category:赫梯君主](../Category/赫梯君主.md "wikilink")

1.  Battle at Sea: 3,000 Years of Naval Warfare. R. G. Grant. 2008.
    Accessed 10 August 2010
2.  trans Astour,
    [AJA](../Page/American_Journal_of_Archaeology.md "wikilink") 69
    (1965), p.256