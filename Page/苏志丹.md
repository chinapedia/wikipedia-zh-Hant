**蘇志丹**（**Danny
Su**，），[中国男](../Page/中国.md "wikilink")[演员](../Page/演员.md "wikilink")。广东戏剧家协会理事，[中国电视家协会会员](../Page/中国电视家协会.md "wikilink")、国家二级演员。

蘇志丹出身於話劇世家，曾經擔任[广东话剧院喜剧团团长](../Page/广东话剧院喜剧团.md "wikilink")，屬於国家二级演员。通常演出忠奸角色，其中《[外来媳妇本地郎](../Page/外来媳妇本地郎.md "wikilink")》飾演康祈光（大哥）便演出深入民心，也就是[丁玲飾演的香蘭的丈夫](../Page/丁玲_\(演員\).md "wikilink")。

配偶是著名中國演員[虎艷芬](../Page/虎艷芬.md "wikilink")（Eva Hu），同樣在《外來媳婦本地郎》中扮演康祁宗的妻子。

## 演出作品

  - 《[外来媳妇本地郎](../Page/外来媳妇本地郎.md "wikilink")》 飾 康祈光
  - 《[我的大嚿父母](../Page/我的大嚿父母.md "wikilink")》 飾 康祈光

## 外部連結

  - [《外来媳妇本地郎》演员逐个数（六）
    “阿光”苏志丹：既老实又精明](https://web.archive.org/web/20060822105614/http://www.gdtv.com.cn/bzbb/show.asp?NewsID=39511)
  - [“大哥”“二嫂”的罗曼史](http://special.dayoo.com/2006/node_3100/node_3108/2006/03/08/1141801024100800.shtml)

[S蘇](../Category/中國電影男演員.md "wikilink")
[S蘇](../Category/中國電視男演員.md "wikilink")
[S蘇](../Category/廣州人.md "wikilink")
[S蘇](../Category/廣東演員.md "wikilink")
[Z志](../Category/蘇姓.md "wikilink")