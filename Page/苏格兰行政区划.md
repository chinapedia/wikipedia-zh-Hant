[英國](../Page/英國.md "wikilink")[蘇格蘭自從](../Page/蘇格蘭.md "wikilink")1996年四月一日起被劃分為32個單一當局管理的「[議會区](../Page/議會.md "wikilink")」，甚至可選擇使用[蓋爾語的名稱](../Page/蓋爾語.md "wikilink")「Comhairle」替代\[1\]。
[Scotland_Administrative_Map_2009.png](https://zh.wikipedia.org/wiki/File:Scotland_Administrative_Map_2009.png "fig:Scotland_Administrative_Map_2009.png")
以下所述的行政区划仍繼續舊本地當局沿有的區界：

## 苏格兰行政区划的變化

今日的蘇格蘭行政區劃，全境含週遭[離島共分為](../Page/離島.md "wikilink")32個[統一管理區](../Page/統一管理區.md "wikilink")（Unitary
Authority
Region），是根據[1994年蘇格蘭地方政府法案](../Page/1994年地方政府法案_\(蘇格蘭\).md "wikilink")，於1996年4月1日起開始使用。但是，有不少蘇格蘭人還是習慣原本分為33個區（Region）的舊分法，蘇格蘭的「區」在性質上因為與[英格蘭及](../Page/英格蘭.md "wikilink")[威爾斯的](../Page/威爾斯.md "wikilink")「[郡](../Page/郡.md "wikilink")」（County）性質接近，因此常常被誤稱為郡，「區」的底下還有第二級的次區劃「[次區](../Page/次區.md "wikilink")」（District），這套分法是自1975年5月16日起啟用。1996年的行政區重劃除了在分區數量上有小小的不同外，最關鍵的一點在於將原本的「區」「次區」兩級制簡化為一級制；緊急服務、選舉及物業評佔事務、衛生事務範圍並不同與行政區劃。

在此之前，蘇格蘭的次區劃的確稱為郡（Administrative
County，行政郡，自1889年時啟用），而在1889年之前，原本都是使用起源於中古時代的[市](../Page/市.md "wikilink")（City），[自治市](../Page/自治市.md "wikilink")（Burgh，相當於英格蘭的Borough），與[教區](../Page/教區.md "wikilink")（Parish）這類舊行政區劃。

傳統上每一個擁用高度自治實體的burgh，由[1707年與英格蘭合併後](../Page/1707年聯合法案.md "wikilink")，可派兩位(共72席)英國國會議席；成為蘇格蘭行政區劃主力。

## 地域统计单位命名法

根据[地域统计单位命名法](../Page/地域统计单位命名法.md "wikilink")，苏格兰分为[东苏格兰](../Page/东苏格兰.md "wikilink")（面积17,969平方公里）、[西南苏格兰](../Page/西南苏格兰.md "wikilink")（面积23,047平方公里）、[东北苏格兰](../Page/东北苏格兰.md "wikilink")（面积6,497平方公里）和[高地和岛屿](../Page/高地和岛屿.md "wikilink")（面积40,333平方公里）。

## 地圖

<table>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td><p><small></p>
<ol>
<li><a href="../Page/印弗克萊德.md" title="wikilink">印弗克萊德</a></li>
<li><a href="../Page/倫弗魯郡.md" title="wikilink">倫弗魯郡</a></li>
<li><a href="../Page/西丹巴頓郡.md" title="wikilink">西丹巴頓郡</a></li>
<li><a href="../Page/東丹巴頓郡.md" title="wikilink">東丹巴頓郡</a></li>
<li><a href="../Page/格拉斯哥市.md" title="wikilink">格拉斯哥市</a></li>
<li><a href="../Page/東倫弗魯郡.md" title="wikilink">東倫弗魯郡</a></li>
<li><a href="../Page/北拉纳克郡.md" title="wikilink">北拉纳克郡</a></li>
<li><a href="../Page/弗爾柯克.md" title="wikilink">弗爾柯克</a></li>
<li><a href="../Page/西洛錫安.md" title="wikilink">西洛錫安</a></li>
<li><a href="../Page/愛丁堡.md" title="wikilink">愛丁堡</a></li>
<li><a href="../Page/中洛錫安.md" title="wikilink">中洛錫安</a></li>
<li><a href="../Page/東洛錫安.md" title="wikilink">東洛錫安</a></li>
<li><a href="../Page/克拉克曼南郡.md" title="wikilink">克拉克曼南郡</a></li>
<li><a href="../Page/法夫.md" title="wikilink">法夫</a></li>
<li><a href="../Page/邓迪.md" title="wikilink">邓迪</a></li>
</ol>
<p></small></p></td>
<td><p><small></p>
<ol start="16">
<li><a href="../Page/安格斯.md" title="wikilink">安格斯</a></li>
<li><a href="../Page/亞伯丁郡.md" title="wikilink">亞伯丁郡</a></li>
<li><a href="../Page/亞伯丁.md" title="wikilink">亞伯丁</a></li>
<li><a href="../Page/莫瑞.md" title="wikilink">莫瑞</a></li>
<li><a href="../Page/高地_(苏格兰行政区).md" title="wikilink">高地</a></li>
<li><a href="../Page/埃利安錫爾.md" title="wikilink">埃利安錫爾</a></li>
<li><a href="../Page/阿蓋爾-比特.md" title="wikilink">阿蓋爾-比特</a></li>
<li><a href="../Page/珀斯-金羅斯.md" title="wikilink">珀斯-金羅斯</a></li>
<li><a href="../Page/史特灵_(苏格兰行政区).md" title="wikilink">史特灵</a></li>
<li><a href="../Page/北艾爾郡.md" title="wikilink">北艾爾郡</a></li>
<li><a href="../Page/東艾爾郡.md" title="wikilink">東艾爾郡</a></li>
<li><a href="../Page/南艾尔郡.md" title="wikilink">南艾尔郡</a></li>
<li><a href="../Page/鄧弗里斯-加洛韋.md" title="wikilink">鄧弗里斯-加洛韋</a></li>
<li><a href="../Page/南拉纳克郡.md" title="wikilink">南拉纳克郡</a></li>
<li><a href="../Page/蘇格蘭邊區.md" title="wikilink">蘇格蘭邊區</a></li>
<li><a href="../Page/奧克尼群島.md" title="wikilink">奧克尼群島</a></li>
<li><a href="../Page/设德兰群岛.md" title="wikilink">设德兰群岛</a></li>
</ol>
<p></small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:ScotlandLabelled.png" title="fig:File:ScotlandLabelled.png"><a href="File:ScotlandLabelled.png">File:ScotlandLabelled.png</a></a></p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 統計

<table>
<thead>
<tr class="header">
<th><p>內陸29區</p></th>
<th><p>面積<br />
(平方英里)</p></th>
<th><p>面積<br />
(平方公里)</p></th>
<th><p>人口<br />
(2001年)</p></th>
<th><p>人口密度<br />
(每平方公里)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿伯丁.md" title="wikilink">阿伯丁市</a></p></td>
<td><p>70</p></td>
<td><p>182</p></td>
<td><p>212,125</p></td>
<td><p>1164</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿伯丁郡.md" title="wikilink">阿伯丁郡</a></p></td>
<td><p>2439</p></td>
<td><p>6317</p></td>
<td><p>226,871</p></td>
<td><p>36</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安格斯.md" title="wikilink">安格斯</a></p></td>
<td><p>843</p></td>
<td><p>2184</p></td>
<td><p>108,400</p></td>
<td><p>50</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿盖尔-比特.md" title="wikilink">阿盖尔-比特</a></p></td>
<td><p>2712</p></td>
<td><p>7023</p></td>
<td><p>91,306</p></td>
<td><p>13</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克拉克曼南郡.md" title="wikilink">克拉克曼南郡</a></p></td>
<td><p>61</p></td>
<td><p>158</p></td>
<td><p>48,077</p></td>
<td><p>304</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丹弗里斯-加洛韦.md" title="wikilink">丹弗里斯-加洛韦</a></p></td>
<td><p>2489</p></td>
<td><p>6446</p></td>
<td><p>147,765</p></td>
<td><p>23</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邓迪.md" title="wikilink">邓迪市</a></p></td>
<td><p>21</p></td>
<td><p>55</p></td>
<td><p>145,663</p></td>
<td><p>2648</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/东艾尔郡.md" title="wikilink">东艾尔郡</a></p></td>
<td><p>492</p></td>
<td><p>1275</p></td>
<td><p>120,235</p></td>
<td><p>94</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/东丹巴顿郡.md" title="wikilink">东丹巴顿郡</a></p></td>
<td><p>68</p></td>
<td><p>176</p></td>
<td><p>108,243</p></td>
<td><p>617</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/东洛锡安.md" title="wikilink">东洛锡安</a></p></td>
<td><p>257</p></td>
<td><p>666</p></td>
<td><p>90,088</p></td>
<td><p>135</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/东伦弗鲁郡.md" title="wikilink">东伦弗鲁郡</a></p></td>
<td><p>65</p></td>
<td><p>168</p></td>
<td><p>89,311</p></td>
<td><p>532</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/爱丁堡.md" title="wikilink">爱丁堡市</a></p></td>
<td><p>100</p></td>
<td><p>260</p></td>
<td><p>448,624</p></td>
<td><p>1725</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/弗尔柯克.md" title="wikilink">弗尔柯克</a></p></td>
<td><p>113</p></td>
<td><p>293</p></td>
<td><p>145,191</p></td>
<td><p>496</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法夫.md" title="wikilink">法夫</a></p></td>
<td><p>517</p></td>
<td><p>1340</p></td>
<td><p>349,429</p></td>
<td><p>261</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格拉斯哥.md" title="wikilink">格拉斯哥市</a></p></td>
<td><p>68</p></td>
<td><p>175</p></td>
<td><p>577,869</p></td>
<td><p>3307</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高地_(苏格兰行政区).md" title="wikilink">高地</a></p></td>
<td><p>10,085</p></td>
<td><p>26,119</p></td>
<td><p>208,914</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/印弗克莱德.md" title="wikilink">印弗克莱德</a></p></td>
<td><p>64</p></td>
<td><p>167</p></td>
<td><p>84,203</p></td>
<td><p>503</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中洛锡安.md" title="wikilink">中洛锡安</a></p></td>
<td><p>135</p></td>
<td><p>350</p></td>
<td><p>80,941</p></td>
<td><p>231</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莫瑞.md" title="wikilink">莫瑞</a></p></td>
<td><p>864</p></td>
<td><p>2237</p></td>
<td><p>86,940</p></td>
<td><p>39</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北艾尔郡.md" title="wikilink">北艾尔郡</a></p></td>
<td><p>343</p></td>
<td><p>888</p></td>
<td><p>135,817</p></td>
<td><p>153</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北拉纳克郡.md" title="wikilink">北拉纳克郡</a></p></td>
<td><p>184</p></td>
<td><p>476</p></td>
<td><p>321,067</p></td>
<td><p>674</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/珀斯-金罗斯.md" title="wikilink">珀斯-金罗斯</a></p></td>
<td><p>2083</p></td>
<td><p>5395</p></td>
<td><p>134,949</p></td>
<td><p>25</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伦弗鲁郡.md" title="wikilink">伦弗鲁郡</a></p></td>
<td><p>102</p></td>
<td><p>263</p></td>
<td><p>172,867</p></td>
<td><p>659</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/苏格兰边区.md" title="wikilink">苏格兰边区</a></p></td>
<td><p>1825</p></td>
<td><p>4727</p></td>
<td><p>106,764</p></td>
<td><p>23</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南艾尔郡.md" title="wikilink">南艾尔郡</a></p></td>
<td><p>475</p></td>
<td><p>1230</p></td>
<td><p>112,097</p></td>
<td><p>93</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南拉纳克郡.md" title="wikilink">南拉纳克郡</a></p></td>
<td><p>686</p></td>
<td><p>1778</p></td>
<td><p>302,216</p></td>
<td><p>170</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史特灵_(苏格兰行政区).md" title="wikilink">史特灵</a></p></td>
<td><p>866</p></td>
<td><p>2243</p></td>
<td><p>86,212</p></td>
<td><p>38</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西丹巴顿郡.md" title="wikilink">西丹巴顿郡</a></p></td>
<td><p>68</p></td>
<td><p>176</p></td>
<td><p>93,378</p></td>
<td><p>531</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西洛锡安.md" title="wikilink">西洛锡安</a></p></td>
<td><p>165</p></td>
<td><p>427</p></td>
<td><p>158,714</p></td>
<td><p>372</p></td>
</tr>
<tr class="even">
<td><p><em>內陸總數</em></p></td>
<td><p><em>28,260</em></p></td>
<td><p><em>73,193</em></p></td>
<td><p><em>4,994,276</em></p></td>
<td><p><em>68</em></p></td>
</tr>
<tr class="odd">
<td><p>外島</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/埃利安锡尔.md" title="wikilink">埃利安锡尔</a></p></td>
<td><p>1185</p></td>
<td><p>3070</p></td>
<td><p>26,502</p></td>
<td><p>9</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥克尼群岛.md" title="wikilink">奥克尼群岛</a></p></td>
<td><p>396</p></td>
<td><p>1025</p></td>
<td><p>19,245</p></td>
<td><p>19</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/设德兰群岛.md" title="wikilink">设德兰群岛</a></p></td>
<td><p>568</p></td>
<td><p>1471</p></td>
<td><p>21,988</p></td>
<td><p>15</p></td>
</tr>
<tr class="odd">
<td><p><em>外島總數</em></p></td>
<td><p><em>2149</em></p></td>
<td><p><em>5566</em></p></td>
<td><p><em>67,735</em></p></td>
<td><p><em>12</em></p></td>
</tr>
<tr class="even">
<td><p>全蘇格蘭</p></td>
<td><p>30,409</p></td>
<td><p>78,759</p></td>
<td><p>5,062,011</p></td>
<td><p>64</p></td>
</tr>
</tbody>
</table>

## 蘇格蘭下屬司法轄區

自1975年元旦起，苏格兰有六大（）：\[2\]

  - [格拉斯哥和斯特拉斯克爾文](../Page/格拉斯哥和斯特拉斯克爾文.md "wikilink")（Glasgow and
    Strathkelvin）
  - [格林皮安、高地及附島](../Page/格林皮安、高地及附島.md "wikilink")（Grampian, Highland
    and Islands）
  - [洛錫安及邊區](../Page/洛錫安及邊區.md "wikilink")（Lothian and Borders）
  - [北史崔克萊](../Page/北史崔克萊.md "wikilink")（North Strathclyde）
  - [南史崔克萊、丹佛里斯及蓋洛威](../Page/南史崔克萊、丹佛里斯及蓋洛威.md "wikilink")（South
    Strathclyde, Dumfries and Galloway）
  - [泰塞德、中央及法夫](../Page/泰塞德、中央及法夫.md "wikilink")（Tayside, Central and
    Fife）

## 民政區

從1930年代起並無行政功能的[民政區](../Page/民政區.md "wikilink")（参见），仍為人口普查的基本單位。

## 社區

社區是蘇格蘭最低級行政區；每1,200百個社區可透過民選產生的為溝通途徑，向上級本地政府反映意見。除了大倫敦受法律上准許外，英格蘭開始盛行民政區的成立。

## 參考文獻

{{-}}

[Scotland](../Category/各国行政区划.md "wikilink")
[苏格兰行政区划](../Category/苏格兰行政区划.md "wikilink")
[Scotland](../Category/英国行政区划.md "wikilink")

1.  [Local Government (Gaelic Names) (Scotland)
    Act 1997](http://www.opsi.gov.uk/acts/acts1997/1997006.htm)
2.  The Sheriffdoms Reorganisation Order 1974 S.I. 1974/2087 (S.191)