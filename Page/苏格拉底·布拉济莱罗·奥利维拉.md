**苏格拉底-{}-·布拉济莱罗·奥利维拉**（**Sócrates Brasileiro Sampaio de Souza Vieira de
Oliveira**，），通常称其为**苏格拉底**（**Sócrates**），是一名[巴西](../Page/巴西.md "wikilink")[足球运动员](../Page/足球.md "wikilink")。\[1\]巴西足球傳奇巨星之一。

苏格拉底是巴西足坛的一个另类，因为他不像[贝利那样年少成名](../Page/贝利.md "wikilink")，更重要的是，尽管因为足球他被世人铭记，但他本人却从不承认自己是一个职业球员。在2004年退役十多年的苏格拉底同意担当[英格兰第九级联赛](../Page/英格兰.md "wikilink")（北部郡东组联赛）球队一个月的球员教练工作。\[2\]\[3\]2011年12月4日，苏格拉底因[食物中毒导致肠道感染辞世](../Page/食物中毒.md "wikilink")，年仅57岁。\[4\]

## 职业联赛生涯

身高达到192厘米（6呎4吋）的苏格拉底，屬智慧型的球員，好菸喜酒，身手敏捷，閱讀球賽的能力廣受稱讚，其無懈可擊的傳球亦無可比擬，後跟傳球更是獨門絕技。

苏格拉底於1974年在其家鄉[圣保罗州的](../Page/圣保罗州.md "wikilink")[里贝朗普雷图](../Page/里贝朗普雷图.md "wikilink")（Riberao
Preto）开始其職業足球員生涯，大部分時間（1978至1984年）是在[哥連泰斯渡過](../Page/哥連泰斯保利斯塔體育會.md "wikilink")。在科林蒂安的6年中，苏格拉底帮助球队3次赢得圣保罗州联赛的冠军，并被认为是俱乐部历史上最受球迷爱戴的球员。在进入职业足坛前22岁的苏格拉底只是圣保罗大学医学系的一名学生，由于他的职业是医生，所以，大家都称他苏格拉底医生或博士，因为医生和博士是同一个词（*Doutor*）。\[5\]在其足球生涯晚期曾加盟[意甲球会](../Page/意大利足球甲级联赛.md "wikilink")[佛罗伦萨](../Page/佛罗伦萨足球俱乐部.md "wikilink")，1986年再次返回巴西国内联赛，效力于[法林明高及](../Page/法林明高競賽會.md "wikilink")[桑托斯](../Page/桑托斯足球俱乐部.md "wikilink")。

作為一名歷史上世界級的中場球員，苏格拉底是[巴西隊於](../Page/巴西國家足球隊.md "wikilink")[1982年](../Page/1982年世界盃足球賽.md "wikilink")（西班牙）及[1986年](../Page/1986年世界盃足球賽.md "wikilink")（墨西哥）[世界盃的隊長](../Page/世界盃.md "wikilink")。[贝利於](../Page/贝利.md "wikilink")2004年3月提名苏格拉底成為[國際足聯成立](../Page/國際足聯.md "wikilink")100週年的[FIFA
100成員之一](../Page/FIFA_100.md "wikilink")。

## 国家队时期

在苏格拉底25岁的时候，他入选了巴西国家队。[1982年世界杯](../Page/1982年世界杯.md "wikilink")，27岁的苏格拉底成为了巴西国家队队长，他与[济科](../Page/济科.md "wikilink")、[-{zh-hans:法尔考;
zh-hk:科高;zh-tw:法爾考}-和](../Page/保罗·罗伯托·法尔考.md "wikilink")[-{zh-hans:塞雷佐;
zh-hk:沙真奴;zh-tw:瑟雷佐}-构筑起了巴西的梦幻中场](../Page/托尼尼奧·塞雷佐.md "wikilink")。在这届世界杯上，巴西踢出了赏心悦目的艺术足球。苏格拉底说：“我一生中看了那么多比赛，但我始终认为1982年的巴西才是最完美的。”

### 入选百大进球

与[苏联国家队的首场小组赛](../Page/俄罗斯国家足球队.md "wikilink")，巴西上半场0-1落后，下半场第30分钟，苏格拉底扳平比分。其时，苏格拉底接队友右路传球，停球晃过苏联队多名后卫后果断起脚射门，皮球应声入网。这个进球也被评为**世界杯百大进球**之一。与[意大利国家队的比赛中](../Page/意大利国家足球队.md "wikilink")，苏格拉底同样打进了一个漂亮的进球，他和濟科用一次簡單明快的小組配合，瞬間甩掉四名防守球員，最後由他殺到門前用腳尖點進這個小角度的射門，这个进球同样被列为世界杯百大进球之一。同一届世界杯上的两个进球全部入选百大进球，由此可见苏格拉底的高超技艺。\[6\]

### 点球饮恨

[1986年世界杯](../Page/1986年世界杯.md "wikilink")，苏格拉底再一次代表巴西出战，他坐镇中场组织全队进攻和防守。可惜的是，在面对[-{zh-hans:米歇尔·普拉蒂尼;
zh-hk:柏天尼;zh-tw:普拉蒂尼}-率领下的](../Page/米歇尔·普拉蒂尼.md "wikilink")[法国国家队的四分之一决赛中](../Page/法国国家足球队.md "wikilink")，双方苦战120分钟打成1-1平，点球决战，首先出场的苏格拉底射出的点球被法国门将-{zh-hans:若尔·巴茨;
zh-hk:碧斯;}-扑出。最终导致巴西被淘汰，苏格拉底的世界杯生涯就此结束。\[7\]

## 技术特点

苏格拉底常用头球攻门或巧妙摆渡，他的脚下动作也非常厉害，运球时从不看球，盘带球的技术可与[-{zh-hans:约翰·克鲁伊夫;
zh-hk:告魯夫;zh-tw:克魯伊夫}-媲美](../Page/约翰·克鲁伊夫.md "wikilink")，而且，他的传球也极为准确，一手脚后跟传球的绝技让人击节赞赏。\[8\]

## 球星逸事

  - 据说，苏格拉底之所以与哲学家[-{zh-hans:苏格拉底;
    zh-hk:蘇格拉底;}-同名](../Page/苏格拉底.md "wikilink")，是因为他出生时其父亲刚好在看经典[政治学名著](../Page/政治学.md "wikilink")《[理想国](../Page/理想国.md "wikilink")》，因此，他的父亲就为儿子取名苏格拉底。
  - 苏格拉底从事过很多职业，包括医生、教练、商人、政治家、音乐家、剧作家、专栏记者等等，但他最喜欢的职业无疑是医生，这也许就是他从来不承认自己是职业球员的原因。退役后，苏格拉底重新回到学校，并顺利拿到了医学博士学位，成为了名副其实的「足球博士」。\[9\]有如此高學歷的足球員是十分罕有的，而巴西另一位足球名將[-{zh-hans:托斯塔奥;
    zh-hk:拖士圖;}-](../Page/托斯塔奥.md "wikilink")（Tostão）也是醫生。
  - 苏格拉底的弟弟[拉易](../Page/拉易·奥利维拉.md "wikilink")，也是前[巴西國家足球隊國腳](../Page/巴西國家足球隊.md "wikilink")，曾經參加過[1994年世界盃](../Page/1994年世界盃足球賽.md "wikilink")。

## 逝世

2011年12月4日，苏格拉底因[食物中毒导致肠道感染而辞世于](../Page/食物中毒.md "wikilink")[圣保罗的阿尔伯特](../Page/圣保罗_\(巴西\).md "wikilink")-爱因斯坦医院，年仅57岁。苏格拉底在去世前曾患有严重的[消化系统疾病](../Page/消化系统.md "wikilink")，2011年8月和11月两度入院就诊，消化系统疾病可能与他在球员时期酷爱饮酒有关。\[10\]

## 参考资料

## 外部链接

  - [Sócrates joins Garforth
    Town](http://news.bbc.co.uk/sport1/hi/funny_old_game/3957519.stm)
  - [No.31
    〖巴西〗苏格拉底·布拉济莱罗·奥利维拉](http://book.sina.com.cn/longbook/1087529517_100footballathlete/33.shtml)

[Category:1986年世界盃足球賽球員](../Category/1986年世界盃足球賽球員.md "wikilink")
[Category:1982年世界盃足球賽球員](../Category/1982年世界盃足球賽球員.md "wikilink")
[Category:巴西足球运动员](../Category/巴西足球运动员.md "wikilink")
[Category:巴西旅外足球運動員](../Category/巴西旅外足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:哥連泰斯球員](../Category/哥連泰斯球員.md "wikilink")
[Category:費倫天拿球員](../Category/費倫天拿球員.md "wikilink")
[Category:法林明高球員](../Category/法林明高球員.md "wikilink")
[Category:山度士球員](../Category/山度士球員.md "wikilink")
[Category:FIFA 100](../Category/FIFA_100.md "wikilink")
[Category:聖保羅大學校友](../Category/聖保羅大學校友.md "wikilink")
[Category:里貝郎普雷圖人](../Category/里貝郎普雷圖人.md "wikilink")
[Category:帕拉州人](../Category/帕拉州人.md "wikilink")

1.  <http://www.sambafoot.com/en/players/373_Socrates.html> -
    /www.sambafoot.com - Sócrates data info

2.   - <http://forum.hkpcf.net>  - 50歲的巴西前球星苏格拉底將短期效力英格蘭一小聯盟球隊

3.  <http://news.bbc.co.uk/sport1/hi/funny_old_game/3957519.stm> -
    [BBC](../Page/BBC.md "wikilink") - Sócrates joins Garforth Town

4.  <http://sports.sina.com.cn/g/2011-12-04/17055855238.shtml> - 新浪体育 -
    苏格拉底因肠道感染去世 无冕之王就此告别人间

5.  <http://www.ofutebol.com/nicknames.shtml> - www.ofutebol.com -
    Players' Nicknames - Sócrates,Doutor

6.  [- www.sohu.com - “我不是职业球员”
    足球博士苏格拉底](http://2006.sohu.com/s2006/7657/s242906409/)
    搜狐体育 世界杯名人堂

7.  《影响世界足球100名人排行榜》 - 海南出版社 - 兆丰著

8.  <http://www.southcn.com/sports/2006wc/celebrity/200604100088.htm> -
    www.southcn.com - 可与克鲁伊夫媲美的盘球大师“思想者”：苏格拉底

9.  <http://book.sina.com.cn/longbook/1087529517_100footballathlete/33.shtml>
    - <http://book.sina.com.cn> - No.31 〖巴西〗苏格拉底·布拉济莱罗·奥利维拉

10.