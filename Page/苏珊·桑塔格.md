**苏珊·桑塔格**（，）生卒于[纽约](../Page/纽约.md "wikilink")，[美国著名的](../Page/美国.md "wikilink")[作家和](../Page/作家.md "wikilink")[评论家](../Page/评论家.md "wikilink")，以及著名的女权主义者，她被认为是近代西方最引人注目；且最有争议性的女作家及评论家。

她的写作领域广泛，以其才华、敏锐的洞察力和广博的知识著称。著作主要有《反对阐释》（*Against
Interpretation*），《激进意志的风格》（*Styles of Radical
Will*），《论摄影》（*On Photography*），《艾滋病及其隐喻》（*AIDS and Its
Metaphors*）和小说《火山情人》（*The Volcano Lover*）。另外其子David
Rieff將其日記與筆記編成一本書[Reborn:Journals and
Notebooks，1947-1963](../Page/Reborn:Journals_and_Notebooks，1947-1963.md "wikilink")

2000年，她的历史小说《[在美国](../Page/在美国.md "wikilink")》获得了[美国國家图书奖](../Page/美国國家图书奖.md "wikilink")（National
Book
Awards）。她还是一位社会评论家和文学评论家，她对时代以及文化的批评包括[摄影](../Page/摄影.md "wikilink")、[艺术](../Page/艺术.md "wikilink")、[文学等](../Page/文学.md "wikilink")，被誉为“美国公众的良心”。此外，她也是一位反战人士，1960年代反对[越南战争](../Page/越南战争.md "wikilink")，也写文章批评过[伊拉克战争](../Page/伊拉克战争.md "wikilink")。

## 生平

[Tombe_de_Susan_Sontag_au_Cimetière_du_Montparnasse.jpg](https://zh.wikipedia.org/wiki/File:Tombe_de_Susan_Sontag_au_Cimetière_du_Montparnasse.jpg "fig:Tombe_de_Susan_Sontag_au_Cimetière_du_Montparnasse.jpg")
蘇珊·桑塔格原名Susan Rosenblatt，是Jack Rosenblatt和其前妻Mildred
Jacobsen的親生女兒。Jack是从事毛皮生意的[猶太商人](../Page/猶太.md "wikilink")，在桑塔格5岁时因[肺结核死於](../Page/肺结核.md "wikilink")[中國](../Page/中國.md "wikilink")。Mildred便改嫁給Nathan
Sontag，於是蘇珊改隨後父姓。桑塔格是第三代的立陶宛裔[美國人](../Page/美國.md "wikilink")。

桑塔格於[亞利桑那州](../Page/亞利桑那州.md "wikilink")[圖森成長](../Page/圖森_\(亞利桑那州\).md "wikilink")，高中在[洛杉磯就讀](../Page/洛杉磯.md "wikilink")。她跳了三級，15歲就高中畢業。[芝加哥大學畢業](../Page/芝加哥大學.md "wikilink")，24歲便在[哈佛大學取得哲學碩士](../Page/哈佛大學.md "wikilink")，之後並前往[牛津大學聖安妮學院研究院與](../Page/牛津大學聖安妮學院.md "wikilink")[巴黎大學](../Page/巴黎大學.md "wikilink")，主要工作在哲學、文學和神學這幾個範疇。

桑塔格因[急性骨髓性白血病于](../Page/急性骨髓性白血病.md "wikilink")2004年12月28日逝世。三十多年來，她與不同形式的[癌症鬥爭](../Page/癌症.md "wikilink")，包括[乳癌和](../Page/乳癌.md "wikilink")[子宮癌](../Page/子宮癌.md "wikilink")。

### 私人生活

17歲時，桑塔格和[菲利普·瑞夫](../Page/菲利普·瑞夫.md "wikilink")（Philip
Rieff）結婚，这段婚姻于8年后结束。他們的兒子[大衛·瑞夫後來成為了作家及其母的編輯](../Page/大衛·瑞夫.md "wikilink")。1989年桑塔格和攝影師[安妮·萊柏維茲展開了新戀情](../Page/安妮·萊柏維茲.md "wikilink")，但這段戀情在桑塔格臨終前一年結束了，但兩人到桑塔格死時仍是朋友。\[1\]

## 部分著作

  - [旁觀他人之痛苦](../Page/旁觀他人之痛苦.md "wikilink")，陳耀成(譯)，麥田出版社出版，2010年 ISBN
    9789861736396 （Regarding the Pain of Others）
  - [重生：桑塔格日記第一部](../Page/重生：桑塔格日記第一部.md "wikilink")，郭寶蓮(譯)，麥田出版社出版，2010年
    ISBN 9789861736419 （Reborn: Journals & Notebooks 1947-1963）
  - [論攝影](../Page/論攝影.md "wikilink")，黃燦然(譯)，麥田出版社出版，2010年 ISBN
    9789861203843 （ON PHOTOGRAPHY）
  - [疾病的隱喻](../Page/疾病的隱喻.md "wikilink")，程巍(譯)，麥田出版社出版，2012年 ISBN
    9789861738086 （Illness as Metaphor and AIDS and Its Metaphors）
  - [土星座下：桑塔格論七位思想藝術大師](../Page/土星座下：桑塔格論七位思想藝術大師.md "wikilink")，廖思逸、姚君偉、陳耀成(譯)，麥田出版社出版，2012年
    ISBN 9789861738338 （Under the Sign of Saturn）
  - [同時：桑塔格隨筆與演說](../Page/同時：桑塔格隨筆與演說.md "wikilink")，黃燦然(譯)，麥田出版社出版，2011年
    ISBN 9789861207797 （At the Same Time）
  - [反對闡釋](../Page/反對闡釋.md "wikilink")，程巍(譯)，上海譯文出版社出版，2011年 ISBN
    9787532753437
    [反詮釋：桑塔格論文集](../Page/反詮釋：桑塔格論文集.md "wikilink")，黃茗芬(譯)，麥田出版社出版，2008年
    ISBN 9789861733883（Against Interpretation and Other Essays）
  - [重點所在](../Page/重點所在.md "wikilink")，陶潔、黃燦然 等(譯)，上海譯文出版社出版，2011年 ISBN
    9787532753444 陳相如(譯)，大田出版社出版，2008年 ISBN 9789861790985 （Where the
    Stress Falls）
  - [火山情人︰一個傳奇](../Page/火山情人︰一個傳奇.md "wikilink")，姚君偉(譯)，上海譯文出版社出版，2012年
    ISBN 9787532756117 （The Volcano Lover︰a legend）
  - [在美國](../Page/在美國.md "wikilink")，何穎怡(譯)，時報出版，2005年 ISBN 957134267X
    廖七一、李小均(譯)，譯林出版社出版，2008年 ISBN 9787544705172 （In America）
  - [死亡匣子](../Page/死亡匣子.md "wikilink")，劉國枝(譯)，上海譯文出版社出版，2009年 ISBN
    9787532748563 （Death Kit）
  - [我，及其他](../Page/我，及其他.md "wikilink")，徐天池、申慧輝(譯)，上海譯文出版社出版，2009年 ISBN
    9787532748426 [我等之辈](../Page/我等之辈.md "wikilink")，王予霞
    (譯)，探索出版社出版，1999年 ISBN 9789576151811（I，etcetera）
  - [恩主](../Page/恩主.md "wikilink")，姚君偉(譯)，上海譯文出版社出版，2007年 ISBN
    9787532743230 （The Benefactor）
  - [床上的愛麗斯](../Page/床上的愛麗斯.md "wikilink")，馮濤(譯)，上海譯文出版社出版，2007年 ISBN
    9787562741632
    [床上的愛麗思](../Page/床上的愛麗思.md "wikilink")，黃翠華(譯)，唐山出版社出版，2001年
    ISBN 9578221665 （Alice in bed : a play in eight scenes）
  - [激進意志的樣式](../Page/激進意志的樣式.md "wikilink")，何寧、周麗華、王磊(譯)，上海譯文出版社出版，
    2007年 ISBN 9787532742820 （Styles of Radical Will）
  - [蘇珊·桑塔格文選](../Page/蘇珊·桑塔格文選.md "wikilink")，黃燦然 等(譯)，麥田出版社出版，2005年
    ISBN 9867413962 （Selected Writings By Susan Sontag）
  - [中國旅行計劃︰蘇珊·桑塔格短篇小說選](../Page/中國旅行計劃︰蘇珊·桑塔格短篇小說選.md "wikilink")，申慧辉(譯)，南海出版公司出版，2005年
    ISBN 9787544227209
  - [沉默的美学：苏珊·桑塔格论文选](../Page/沉默的美学：苏珊·桑塔格论文选.md "wikilink")，黄梅(譯)，南海出版公司出版，2006年
    ISBN 9787544227285
  - [泅泳於死亡之海：母親桑塔格最後的歲月](../Page/泅泳於死亡之海：母親桑塔格最後的歲月.md "wikilink")，David
    Rieff(著)、姚君偉(譯)，麥田出版社出版，2011年 ISBN 9789861737379 （Swimming in a Sea
    of Death: A Son’s Memoir）
  - [在土星的光環下—蘇珊．桑塔格Susan
    Sontag紀念文選](../Page/在土星的光環下—蘇珊．桑塔格Susan_Sontag紀念文選.md "wikilink")，David
    Rieff 等(著)，傾向出版社出版，2007年 ISBN 9789572840863
  - [永遠的蘇珊︰回憶蘇珊.桑塔格](../Page/永遠的蘇珊︰回憶蘇珊.桑塔格.md "wikilink")，Sigrid
    Nunez(著)、阿垚(譯)，上海譯文出版社出版，2012年 ISBN 9787532757503

## 外部链接

  - [苏珊·桑塔格官方网站](http://www.susansontag.com/)
  - [貝嶺：在土星的光環下──紀念蘇珊.桑塔格（上）](http://www.fireofliberty.org/trad/article/3482.asp)
  - [細讀蘇珊．桑塔格](http://blog.roodo.com/fembooks/archives/3719629.html)
  - [坎普札記 (Notes on
    Camp)](http://individual.utoronto.ca/variations/notesoncamp.htm)
  - [苏珊·桑塔格怀念文章（上）](http://www.southcn.com/nfsq/ywhc/ls/200501070595.htm)
  - [蘇珊·桑塔格訪談錄：反對後現代主義及其他](http://www.southcn.com/nfsq/ywhc/ls/200501060743.htm)

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [Susan Sontag](http://www.susansontag.com/), official website

  -
  - ["with Ramona
    Koval"](http://www.abc.net.au/rn/arts/bwriting/stories/s1288925.htm),
    *Books and Writing,* [ABC Radio
    National](../Page/ABC_Radio_National.md "wikilink"), 30/1/2005

  - [Susan
    Sontag](https://web.archive.org/web/20070926234611/http://www.mathieu-bourgois.com/photos-auteur.asp?Clef=55)
    – Photos by Mathieu Bourgois.

  - [The *Friedenspreis* acceptance speech
    (2003-10-12)](http://www.tomdispatch.com/post/1031/)

  - [*Fascinating
    Fascism*](http://www.history.ucsb.edu/faculty/marcuse/classes/33d/33dTexts/SontagFascinFascism75.htm)
    illustrated text of Sontag's seminal 1974 article on Nazi filmmaker
    [Leni Riefenstahl](../Page/Leni_Riefenstahl.md "wikilink")'s
    aesthetics, from *[Under the Sign of
    Saturn](../Page/Under_the_Sign_of_Saturn.md "wikilink")*

  - [Sontag's comments in *The New Yorker*, September 24,
    2001](http://www.newyorker.com/archive/2001/09/24/010924ta_talk_wtc)
    about the September 11th attack on the United States

  - [Terry Castle, *Desperately Seeking Susan*, *London Review of
    Books*, March 2005](http://www.lrb.co.uk/v27/n06/cast01_.html)

  - [Sheelah Kolhatkar, "Notes on camp
    Sontag"](http://www.observer.com/node/50298) *New York Observer*,
    January 8, 2005

  -
  - ['Susan Sontag: The
    Collector'](https://web.archive.org/web/20090411034134/http://www.tnr.com/booksarts/story.html?id=66c66cf7-996f-499d-ba65-67bcdea7b220),
    by Daniel Mendelsohn, *The New Republic*

  - [A review of
    "Reborn"](http://commonsense2.com/2009/04/book-reviews/reborn-journals-notebooks-1947-1963-by-susan-sontag-edited-by-david-rieff/)
    - by James Patrick

  - [*In Depth* interview with Sontag, March 2,
    2003](http://www.c-spanvideo.org/program/172991-1)

[Category:國家圖書獎得主](../Category/國家圖書獎得主.md "wikilink")
[Category:美國女性作家](../Category/美國女性作家.md "wikilink")
[Category:美國LGBT作家](../Category/美國LGBT作家.md "wikilink")
[Category:猶太作家](../Category/猶太作家.md "wikilink")
[Category:女性小說家](../Category/女性小說家.md "wikilink")
[Category:女性主義作家](../Category/女性主義作家.md "wikilink")
[Category:雙性戀作家](../Category/雙性戀作家.md "wikilink")
[Category:LGBT小說家](../Category/LGBT小說家.md "wikilink")
[Category:美國女性主義者](../Category/美國女性主義者.md "wikilink")
[Category:美國社會運動者](../Category/美國社會運動者.md "wikilink")
[Category:美國反越戰人士](../Category/美國反越戰人士.md "wikilink")
[Category:美國持不同政見者](../Category/美國持不同政見者.md "wikilink")
[Category:牛津大學聖安妮學院校友](../Category/牛津大學聖安妮學院校友.md "wikilink")
[Category:巴黎大學校友](../Category/巴黎大學校友.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[Category:美國雙性戀者](../Category/美國雙性戀者.md "wikilink")
[Category:LGBT猶太人](../Category/LGBT猶太人.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:罹患白血病逝世者](../Category/罹患白血病逝世者.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")

1.