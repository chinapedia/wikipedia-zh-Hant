**苔蘚植物**，是[非維管植物中的](../Page/非維管植物.md "wikilink")[有胚植物](../Page/有胚植物.md "wikilink")：它們有組織器官以及封閉的生殖系統，但缺少運輸水分的[維管束](../Page/維管束.md "wikilink")。它們沒有[花朵也不製造](../Page/花朵.md "wikilink")[種子](../Page/種子.md "wikilink")，而是經由[孢子來繁殖](../Page/孢子.md "wikilink")。

## 分類

苔蘚植物並不組成一個[單系群](../Page/單系群.md "wikilink")，而是包含有三個類群：[地錢門](../Page/地錢門.md "wikilink")、[角苔門和](../Page/角苔門.md "wikilink")[苔蘚植物門](../Page/苔蘚植物門.md "wikilink")。

[Bryo_cladogram.jpg](https://zh.wikipedia.org/wiki/File:Bryo_cladogram.jpg "fig:Bryo_cladogram.jpg")

現在對[陸生植物的研究通常會顯示出這兩種圖形的其中一種](../Page/陸生植物.md "wikilink")。在第一種圖形裡，[地錢門是第一個分離的](../Page/地錢門.md "wikilink")，接著是[角苔門](../Page/角苔門.md "wikilink")，而[苔蘚植物門和多孢植物](../Page/苔蘚植物門.md "wikilink")（包含[維管植物](../Page/維管植物.md "wikilink")）最為接近。在第二種圖形裡，[角苔門則是第一個分離出去的](../Page/角苔門.md "wikilink")，第二個是[維管植物](../Page/維管植物.md "wikilink")，而[苔蘚植物門和](../Page/苔蘚植物門.md "wikilink")[地錢門之間的關係最為親近](../Page/地錢門.md "wikilink")。原本，這三個類群是在「苔蘚植物門」裡的三個綱，但因為它們形成了[併系群](../Page/併系群.md "wikilink")，所以現在被放置在三個不同的門裡面。

## 性別模式

此類植物平常為[單倍體的](../Page/單倍體.md "wikilink")[配子體](../Page/配子體.md "wikilink")，只有偶爾才會出現[雙倍體的](../Page/雙倍體.md "wikilink")[孢子體](../Page/孢子體.md "wikilink")。因此，苔蘚植物的性別模式和其他的植物是很不相同的。苔蘚植物的性別模式主要有兩種：

  - 雌雄異株的苔蘚植物只會在單株植物中有[精子器或](../Page/精子器.md "wikilink")[頸卵器的其中一種](../Page/頸卵器.md "wikilink")。
  - 雌雄同株的苔蘚植物在同一株植物上會同時有[精子器和](../Page/精子器.md "wikilink")[頸卵器](../Page/頸卵器.md "wikilink")。

某些苔蘚植物依著環境的不同可以是雌雄同株或是雌雄異株的。其他的物種則只屬於兩種模式的其中一種。

這和[種子植物上所稱的](../Page/種子植物.md "wikilink")「雌雄異株」與「雌雄同株」不同，那是指一個[孢子體植物會帶有一種或兩種的配子體](../Page/孢子體.md "wikilink")。而苔蘚植物上的則是指其為單性或雙性的[配子體植物](../Page/配子體.md "wikilink")。

## 另見

  - [地錢門](../Page/地錢門.md "wikilink")
  - [角苔門](../Page/角苔門.md "wikilink")
  - [苔蘚植物門](../Page/苔蘚植物門.md "wikilink")
  - [有胚植物](../Page/有胚植物.md "wikilink")
  - [植物的性](../Page/植物的性.md "wikilink")

## 參考文獻

  - Chopra, R. N. & Kumra, P. K. (1988). *Biology of Bryophytes*. New
    York: John Wiley & Sons. ISBN 0-470-21359-0.
  - Crum, Howard (2001). *Structural Diversity of Bryophytes*. Ann
    Arbor: University of Michigan Herbarium. ISBN 0-9620733-4-2.
  - Goffinet, Bernard. (2000). Origin and phylogenetic relationships of
    bryophytes. In A. Jonathan Shaw & Bernard Goffinet (Eds.),
    *Bryophyte Biology*, pp. 124-149. Cambridge: [Cambridge University
    Press](../Page/Cambridge_University_Press.md "wikilink"). ISBN
    0-521-66097-1.
  - Oostendorp, Cora (1987). *The Bryophytes of the Palaeozoic and the
    Mesozoic*. Bryophytorum Bibliotheca, Band **34**. Berlin &
    Stuttgart: J. Cramer. ISBN 3-443-62006-X.
  - Prihar, N. S. (1961). *An Introduction to Embryophyta: Volume I,
    Bryophyta* (4th ed.). Allahabad: Central Book Depot.
  - Raven, Peter H., Evert, Ray F., & Eichhorn, Susan E. (2005).
    *Biology of Plants* (7th ed.). New York: W. H. Freeman and Company.
    ISBN 0-7167-1007-2.
  - Schofield, W. B. (1985). *Introduction to Bryology*. New York:
    Macmillan. ISBN 0-02-949660-8.
  - Watson, E. V. (1971). *The Structure and Life of Bryophytes* (3rd
    ed.). London: Hutchinson University Library. ISBN 0-09-109301-5.

## 外部連結

  - Glime, Janice M., 2007. *[Bryophyte
    Ecology](http://www.bryoecol.mtu.edu/)*, Volume 1. *Physiological
    Ecology*. Ebook sponsored by Michigan Technological University and
    the International Association of Bryologists.

[苔蘚植物](../Category/苔蘚植物.md "wikilink")