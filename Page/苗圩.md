**苗圩**\[1\]（），[河北](../Page/河北.md "wikilink")[昌黎人](../Page/昌黎縣.md "wikilink")。1984年9月加入中国共产党，1974年2月参加工作，[合肥工业大学](../Page/合肥工业大学.md "wikilink")[内燃机专业毕业](../Page/内燃机.md "wikilink")，[中共中央党校](../Page/中共中央党校.md "wikilink")[在职研究生学历](../Page/在职研究生.md "wikilink")，高级工程师；是中共第十七届中央候补委員，第十八届、十九届中央委员。曾任[中共湖北省委常委](../Page/中共湖北省委.md "wikilink")、[武汉](../Page/武汉.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")，[东风汽车公司](../Page/东风汽车公司.md "wikilink")[总经理等职](../Page/总经理.md "wikilink")。现任[中华人民共和国工业和信息化部部长](../Page/中华人民共和国工业和信息化部.md "wikilink")、党组书记。

## 简历

  - 1972年到1974年在[合肥市第八中学高中上学](../Page/合肥市第八中学.md "wikilink")
  - 1982年2月，在[合肥工业大学内燃机专业毕业后](../Page/合肥工业大学.md "wikilink")，任职中汽公司销售服务公司
  - 1991年10月，任中汽总公司生产司副司长
  - 1993年11月 任[机械工业部汽车工业司副司长](../Page/机械工业部.md "wikilink")
  - 1996年8月，任[机械工业部副总工程师](../Page/机械工业部.md "wikilink")
  - 1997年9月，[东风汽车公司](../Page/东风汽车公司.md "wikilink")[党委书记](../Page/党委书记.md "wikilink")
  - 1999年2月，任东风汽车公司总经理
  - 2001年7月，任[东风汽车公司总经理](../Page/东风汽车公司.md "wikilink")，党委副书记
  - 2005年5月，任[中共湖北省委常委](../Page/中共湖北省委.md "wikilink")、武汉市委书记\[2\]
  - 2008年3月，任[工业和信息化部副部长](../Page/中华人民共和国工业和信息化部.md "wikilink")
  - 2010年12月，任工业和信息化部党组书记\[3\]
  - 2010年12月，任工业和信息化部部长\[4\]

## 参考文献

{{-}}

[Category:第十届全国人大代表](../Category/第十届全国人大代表.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:中华人民共和国工业和信息化部部长](../Category/中华人民共和国工业和信息化部部长.md "wikilink")
[Category:中国共产党第十七届中央委员会候补委员](../Category/中国共产党第十七届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十八届中央委员会委员](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:中共湖北省委常委](../Category/中共湖北省委常委.md "wikilink")
[Category:中共武漢市委書記](../Category/中共武漢市委書記.md "wikilink")
[Category:合肥工業大學校友](../Category/合肥工業大學校友.md "wikilink")
[Category:昌黎人](../Category/昌黎人.md "wikilink")
[W](../Category/苗姓.md "wikilink")
[Category:东风汽车人物](../Category/东风汽车人物.md "wikilink")
[Category:中国共产党第十九届中央委员会委员](../Category/中国共产党第十九届中央委员会委员.md "wikilink")

1.
2.
3.
4.