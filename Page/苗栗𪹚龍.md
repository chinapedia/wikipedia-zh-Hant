[IMAG0811.jpg](https://zh.wikipedia.org/wiki/File:IMAG0811.jpg "fig:IMAG0811.jpg")
[IMAG0810.jpg](https://zh.wikipedia.org/wiki/File:IMAG0810.jpg "fig:IMAG0810.jpg")
[苗栗縣特殊慶典活動(苗栗炸龍活動大看板)_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:苗栗縣特殊慶典活動\(苗栗炸龍活動大看板\)_-_panoramio.jpg "fig:苗栗縣特殊慶典活動(苗栗炸龍活動大看板)_-_panoramio.jpg")
**苗栗𪹚龍**（
𪹚注音ㄅㄤˋ，拼音：bàng，為客語發音，有「炸」之意思，因部分系統無法顯示𪹚，又常寫作「-{火旁}-」）為臺灣[苗栗縣於](../Page/苗栗縣.md "wikilink")[元宵節的主要核心活動](../Page/台灣元宵節.md "wikilink")，指[臺灣客家人在苗栗發展出來的傳統元宵客家](../Page/臺灣客家人.md "wikilink")[舞火龍活動之一](../Page/舞火龍.md "wikilink")，為**「北天燈、中𪹚龍、南蜂炮、東玄壇、西乞龜」**\[1\]之一，其中「𪹚龍」即苗栗𪹚龍。

## 歷史

臺灣客家人過年，是要從[正月初一一直到](../Page/正月初一.md "wikilink")[正月十五才算過完年](../Page/正月十五.md "wikilink")。而客家人稱元宵節為「正月半」，也稱「燈節」。客家俚諺中有「月半大過年」，說明了客家人過元宵比過新年還熱鬧的景象。這天家家除了吃[湯圓](../Page/湯圓.md "wikilink")（不同於元宵）、吃菜包（又稱「豬籠粄」）、上燈、賞燈、[猜燈謎等過節習俗外](../Page/猜燈謎.md "wikilink")，還有一項最重要的傳統活動——「
𪹚龍」。「𪹚龍」為[客語發音](../Page/客語.md "wikilink")，也就是「炸龍」的意思。𪹚龍活動不僅有迎春納福的意涵，更保有傳統習俗及文化傳承的深遠意義。

苗栗𪹚龍是源於苗栗地區的迎龍慶典而演變而來，是一個相當熱鬧又刺激的元宵迎新年慶典，之後更將舞龍神化為「迎龍」活動，期望藉神龍帶來祥瑞之氣，帶給民眾平安吉祥、五穀豐收。「𪹚龍」就是用[鞭炮炸龍](../Page/鞭炮.md "wikilink")，採用大量鞭炮、[蜂炮去炸舞龍方式得到去邪](../Page/蜂炮.md "wikilink")（去舊）迎新年的作用，每年快接近元宵節時，很多龍隊開始出來練習，到了元宵節那天則進入高潮。臺灣各地皆有舞龍之民俗活動，唯獨苗栗客家發展出「炸龍」的習俗。

## 「 𪹚龍」六部曲

1.  糊龍：由龍主取材製龍，於[正月初九](../Page/正月初九.md "wikilink")（天公生）前完成。
2.  祥龍點睛：祈求天神賜降神靈瑞氣，附於龍身，藉神龍繞境參拜，護祐蒼生。
3.  迎龍：客家人相信「神龍」登門參拜，等於「神明造訪」能為地方消災，給家戶帶來好運。
4.  跈龍：即跟著龍走，帶來平安吉祥。
5.  𪹚龍：龍愈𪹚愈旺，迎龍時燃放鞭炮，有恭迎神龍的靈氣驅邪納吉之意，一方面慶賀助興，一方面增加年節熱鬧氣氛。
6.  化龍返天：過元宵節後，龍隊須返回龍籍點睛的[土地公廟](../Page/土地公.md "wikilink")「謝神化龍」，其意功德圓滿送龍神返天。

## 備註

<references />

## 外部連結

  - [2018苗栗火旁龍](https://www.facebook.com/bombingdragon/)
  - [苗栗火旁龍 facebook官方網站](https://www.facebook.com/bombingdragon/)
  - [苗栗文創網站
    元宵火旁龍添喜氣](http://meuwatch.blogspot.tw/2014/11/meu-watch_18.html)

[Category:台灣元宵節](../Category/台灣元宵節.md "wikilink")
[Category:台灣客家文化](../Category/台灣客家文化.md "wikilink")
[Category:苗栗縣文化](../Category/苗栗縣文化.md "wikilink")
[Category:苗栗縣旅遊景點](../Category/苗栗縣旅遊景點.md "wikilink")
[Category:台灣地方節慶](../Category/台灣地方節慶.md "wikilink")
[Category:舞火龍](../Category/舞火龍.md "wikilink")

1.  也稱「北𪹚龍」，因苗栗在不同情況下可分別歸類於[北臺灣和](../Page/北臺灣.md "wikilink")[中臺灣](../Page/中臺灣.md "wikilink")，所以稱「中𪹚龍」。若加上北部[平溪的天燈](../Page/平溪區.md "wikilink")，則合稱為「北天龍」。