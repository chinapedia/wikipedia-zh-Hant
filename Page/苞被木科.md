**苞被木科**(Trimeniaceae)也叫[腺齿木科或](../Page/腺齿木科.md "wikilink")[早落瓣科](../Page/早落瓣科.md "wikilink")，只包括1-2[属约](../Page/属.md "wikilink")5-9[种](../Page/种.md "wikilink")，只生长在[澳大利亚东部](../Page/澳大利亚.md "wikilink")、[太平洋岛屿和](../Page/太平洋.md "wikilink")[东南亚](../Page/东南亚.md "wikilink")。

本[科植物都是](../Page/科.md "wikilink")[乔木](../Page/乔木.md "wikilink")，小[花两性](../Page/花.md "wikilink")，生长在[热带和](../Page/热带.md "wikilink")[亚热带环境中](../Page/亚热带.md "wikilink")，[植物中可提炼香精](../Page/植物.md "wikilink")。

1981年的[克朗奎斯特分类法将其分在](../Page/克朗奎斯特分类法.md "wikilink")[樟目中](../Page/樟目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为无法将本](../Page/APG_分类法.md "wikilink")[科分入任何一](../Page/科.md "wikilink")[目](../Page/目.md "wikilink")，只得直接放在[被子植物分支之下](../Page/被子植物分支.md "wikilink")，2003年经过修订的[APG
II
分类法将其列在新设立的](../Page/APG_II_分类法.md "wikilink")[木兰藤目中](../Page/木兰藤目.md "wikilink")。

## 参考文献

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[苞被木科](https://web.archive.org/web/20060426025029/http://delta-intkey.com/angio/www/trimenia.htm)
  - [e-flora中的苞被木科](http://www.efloras.org/florataxon.aspx?flora_id=900&taxon_id=20324)
  - [NCBI中的苞被木科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=88918&lvl=3&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/苞被木科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")