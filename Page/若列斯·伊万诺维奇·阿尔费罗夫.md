**若列斯·伊万诺维奇·阿尔费罗夫**（，），[俄罗斯物理学家](../Page/俄罗斯.md "wikilink")，生於[白俄罗斯](../Page/白俄罗斯.md "wikilink")[维捷布斯克](../Page/维捷布斯克.md "wikilink")，2000年获[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。他也是俄罗斯政治家，自1995年以来一直是俄罗斯国家杜马议会下院的成员。\[1\]

## 参考资料

## 外部链接

  - [诺贝尔官方网站若列斯·阿尔费罗夫自传](http://nobelprize.org/nobel_prizes/physics/laureates/2000/alferov-autobio.html)

[A](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:俄羅斯/蘇聯諾貝爾獎獲得者](../Category/俄羅斯/蘇聯諾貝爾獎獲得者.md "wikilink")
[A](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:蘇聯物理學家](../Category/蘇聯物理學家.md "wikilink")
[Category:俄罗斯物理学家](../Category/俄罗斯物理学家.md "wikilink")
[Category:半導體物理學家](../Category/半導體物理學家.md "wikilink")
[Category:俄國猶太人](../Category/俄國猶太人.md "wikilink")
[Category:白俄羅斯猶太人](../Category/白俄羅斯猶太人.md "wikilink")
[Category:中国科学院外籍院士](../Category/中国科学院外籍院士.md "wikilink")
[Category:聖彼得堡國立電子科技大學校友](../Category/聖彼得堡國立電子科技大學校友.md "wikilink")
[Category:京都奖获得者](../Category/京都奖获得者.md "wikilink")
[Category:一级祖国功勋勋章获得者](../Category/一级祖国功勋勋章获得者.md "wikilink")
[Category:二级祖国功勋勋章获得者](../Category/二级祖国功勋勋章获得者.md "wikilink")
[Category:三级祖国功勋勋章获得者](../Category/三级祖国功勋勋章获得者.md "wikilink")
[Category:四级祖国功勋勋章获得者](../Category/四级祖国功勋勋章获得者.md "wikilink")

1.  [Умер Жорес Алферов](https://tass.ru/nauka/6177836)