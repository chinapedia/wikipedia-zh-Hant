**若島津健**，台譯「**袁凱利**」，港譯「**歐陽健威**」、「**若島津健威**」，是著名足球漫畫「[足球小將](../Page/足球小將.md "wikilink")」的人物。曾效力球隊包括明和小學、東邦學園國中、[日本少年隊](../Page/日本.md "wikilink")、東邦學園高中、日本青年軍、日本[橫濱飛翼](../Page/橫濱.md "wikilink")、日本[名古屋八鯨](../Page/名古屋.md "wikilink")。動畫中，若島津的配音員先後有：日本的[飛田展男](../Page/飛田展男.md "wikilink")（第一版）、[中村大樹](../Page/中村大樹.md "wikilink")（Ｊ版少年時代）、[關智一](../Page/關智一.md "wikilink")（Ｊ版青年時代）、[伊藤健太郎](../Page/伊藤健太郎.md "wikilink")（ROAD
TO 2002）以及香港的[黃健強](../Page/黃健強.md "wikilink")
、[蘇強文等](../Page/蘇強文.md "wikilink")。

## 基本資料

  - 綽號：**[空手道](../Page/空手道.md "wikilink")[守門員](../Page/守門員.md "wikilink")**
  - 國籍：[日本](../Page/日本.md "wikilink")
  - 出生日：12月29日
  - 星座：[山羊座](../Page/山羊座.md "wikilink")
  - 身高：187cm
  - 體重：74kg
  - 血型：A型
  - 球衣號碼：**17**（明和小學）、1
  - 擔任位置：[守門員](../Page/守門員.md "wikilink")、[前鋒](../Page/前鋒_\(足球\).md "wikilink")
  - 擅長腳：[右腳](../Page/右腳.md "wikilink")
  - 所屬隊伍：[名古屋八鯨](../Page/名古屋八鯨.md "wikilink")
  - 相關球員：日向小次郎、若林源三、[新田瞬](../Page/新田瞬.md "wikilink")

## 形象設定

在作品中，**若島津健**是一位[守門員](../Page/守門員.md "wikilink")。在[漫畫](../Page/漫畫.md "wikilink")[初期](../Page/初期.md "wikilink")，**若島津**與**日向小次郎**等以強敵及反派的形象與**大空翼**等主角隊伍對敵，後來大家同為[國家](../Page/國家.md "wikilink")[少年軍隊伍出力](../Page/少年軍.md "wikilink")，因此成為了同伴。

**若島津**一直視[日本隊伍中最強的](../Page/日本.md "wikilink")[守門員](../Page/守門員.md "wikilink")**若林源三**為競爭對手，相較之下，**若林**是全能[防守型的](../Page/防守.md "wikilink")[守門員](../Page/守門員.md "wikilink")，而**若島津**則基於強烈的進攻意識，與及優秀的盤球能力、彈跳力及射門能力，因而成為「進攻型」的[門將](../Page/門將.md "wikilink")。另外，**若島津**有著高大的身型，在[日本隊中身高僅次於巨漢](../Page/日本.md "wikilink")**次藤洋**。

**若島津**非常肯定自己的能力，憑藉其[空手道的敏捷身手](../Page/空手道.md "wikilink")，他個人認為自己的實力不下於**若林**。因此他曾經不甘於當**若林**的後備而憤而離開[日本隊](../Page/日本.md "wikilink")。但是身為球員，他還是十分服從**教練**及其[小學至](../Page/小學.md "wikilink")[高中](../Page/高中.md "wikilink")[時期的隊長](../Page/時期.md "wikilink")**日向**。

## 故事發展

若島津出身於[日本的](../Page/日本.md "wikilink")[明和市](../Page/浦和市.md "wikilink")，[家族為](../Page/家族.md "wikilink")[空手道](../Page/空手道.md "wikilink")[世家](../Page/世家.md "wikilink")，稱為「**若堂流[空手道](../Page/空手道.md "wikilink")**」，擁有[家族的](../Page/家族.md "wikilink")[道場](../Page/道場.md "wikilink")。**若島津**本身是一名[空手道高手](../Page/空手道.md "wikilink")，在**「若堂流」**持有**黑帶**段位（[中學](../Page/中學.md "wikilink")[時代是](../Page/時代.md "wikilink")**三段**，到了【GOLDEN23】中升為**五段**），擅長將[空手道招式融入](../Page/空手道.md "wikilink")[足球領域之上](../Page/足球.md "wikilink")，並且創出很多具備個人風格的**必殺技**，因此被稱為「**[空手道](../Page/空手道.md "wikilink")[門將](../Page/門將.md "wikilink")**」。早期曾受到[父親反對其成為](../Page/父親.md "wikilink")[足球員](../Page/足球.md "wikilink")，但若島津的[足球熱誠令](../Page/足球.md "wikilink")[父親亦不得不承認他的決心](../Page/父親.md "wikilink")，並認同**若島津**是一名真正的[足球員](../Page/足球.md "wikilink")（[日本隊](../Page/日本.md "wikilink")[前鋒](../Page/前鋒_\(足球\).md "wikilink")
之一**新田瞬**，後來亦投在若堂流[空手道場中修練](../Page/空手道.md "wikilink")，並視**健威**為武術前輩）。

在[中學全國大賽中](../Page/中學.md "wikilink")，**若島津**作為東邦學園的[門將](../Page/門將.md "wikilink")，曾接下南葛隊**大空翼**的「衝力射球」，印證了他作為[守門員的非凡實力](../Page/守門員.md "wikilink")。到了作為全國代表出戰[國際賽時](../Page/國際.md "wikilink")，**若島津**一直都跟**若林**爭取正選[守門員的資格](../Page/守門員.md "wikilink")。可惜論守衛能力，**若島津**確實不如**若林**且受到手傷影響，因此未能以首席正選[門將的資格為](../Page/門將.md "wikilink")[日本](../Page/日本.md "wikilink")[少年隊出戰與德國的冠軍戰](../Page/少年.md "wikilink")。

到了【世青篇】中，**若島津**加入了[橫濱飛翼](../Page/橫濱飛翼.md "wikilink")，為了要跟**若林**爭一日之長短，他找到了自己的出路，就是要成為「與**若林**走不同路線的[守門員](../Page/守門員.md "wikilink")」。結果這一個決定成功令**若島津**建立了自己的個性，成為隊伍中一名具備獨特風格的「**攻擊型[門將](../Page/門將.md "wikilink")**」。不久，**若林**在[國際賽上受傷](../Page/國際.md "wikilink")，在**日向**游說之下，**若島津**回歸[日本隊成為正選](../Page/日本.md "wikilink")，並幫助隊伍突破[亞洲預備戰](../Page/亞洲.md "wikilink")。**若林**曾對他做出「防守能力與我不相伯仲，然而攻擊力卻猶在我之上」的評價。可是後來在對[烏拉圭的賽事上連番失球](../Page/烏拉圭.md "wikilink")，一度飽受挫折。

目前**若島津**隸屬[日本本土球隊](../Page/日本.md "wikilink")**[名古屋八鯨](../Page/名古屋八鯨.md "wikilink")**。

## Golden 23

在《足球小將Golden
23》中一場[日本對](../Page/日本.md "wikilink")[丹麥的](../Page/丹麥.md "wikilink")[青年交流賽事裡](../Page/青年.md "wikilink")，配合**吉良**領隊以「**潛能**」為大前提的安排之下，**若島津**首次作為前鋒於[足球場上登場](../Page/足球場.md "wikilink")，令全場嘩然。在該場賽事，**若島津**成功轉型為**柱躉式前鋒**，並在隊伍中多次展現其攻擊才能。身型高大並且擁有優秀彈跳力的他，在接應高空球上，竟然連面對來自[北歐](../Page/北歐.md "wikilink")[丹麥隊的巨人時亦能佔優](../Page/丹麥.md "wikilink")，並且在[奈及利亞的友誼賽中以](../Page/奈及利亞.md "wikilink")[倒掛金鉤射入轉任前鋒後的一球](../Page/倒掛金鉤.md "wikilink")。

既具備[守門員的守備能力](../Page/守門員.md "wikilink")，而又有親身擔任[前鋒](../Page/前鋒_\(足球\).md "wikilink")
征戰的戰績，就如他在[足球與](../Page/足球.md "wikilink")[空手道兩方面都有著非凡的發展一樣](../Page/空手道.md "wikilink")，攻守兼備的**若島津**，成為一名**「二刀流」**球員。

## 必殺技

### 守門員立場

  - 三角跳躍防守
  - 牙龍三角飛翔
  - 手刀防守
  - 直拳防守
  - 空手道蹴技

### 前鋒立場

  - 若堂流圓滑水面踢
  - 若堂流上弦跳踢
  - 若堂流背面空中縱回轉踢
  - 若堂流變幻次元踢

## 參考

  - [足球小將](../Page/足球小將.md "wikilink")
  - [足球小將登場人物列表](../Page/足球小將登場人物列表.md "wikilink")
  - [足球小將之球衣人名及號碼](../Page/足球小將之球衣人名及號碼.md "wikilink")

[Category:足球小將角色](../Category/足球小將角色.md "wikilink")
[Category:虛構日本小學生](../Category/虛構日本小學生.md "wikilink")
[Category:虛構日本中學生](../Category/虛構日本中學生.md "wikilink")
[Category:虛構日本高中生](../Category/虛構日本高中生.md "wikilink")