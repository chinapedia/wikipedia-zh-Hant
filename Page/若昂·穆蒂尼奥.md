**若昂·菲利佩·伊里亚·桑托斯·穆蒂尼奥**（，，），[葡萄牙](../Page/葡萄牙.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，司職[中場](../Page/中場.md "wikilink")，现效力于[英超球会](../Page/英超.md "wikilink")[狼隊](../Page/狼隊足球會.md "wikilink")。

## 职业生涯

### 球會

穆蒂尼奥是[里斯本竞技青训体系的优秀代表](../Page/里斯本竞技.md "wikilink")。2004-2005赛季，当时年仅17岁的穆蒂尼奥已经开始代表一线队出战[热身赛](../Page/热身赛.md "wikilink")。在与队友[-{zh-hans:米格尔·维罗索;
zh-hk:維路素;}-及](../Page/米格尔·维罗索.md "wikilink")[-{zh-hans:纳尼;
zh-hk:蘭尼;}-代表球队青年队出战时](../Page/蘭尼.md "wikilink")，又再次为俱乐部赢得国家杯。

2005年1月23日，穆蒂尼奥首次代表一队在[葡萄牙杯出场](../Page/葡萄牙杯.md "wikilink")。经过葡超联赛和欧洲赛事的磨练，穆蒂尼奥逐渐成为球队领袖。2006-2007赛季，穆蒂尼奥以19岁的年纪出任球队副队长。一个赛季后，又升任队长，时年20岁。他也成为俱乐部历史上第二年轻的队长。原先司职中场中路的他，也逐渐成为中场多面手，能够胜任攻击型中场、防守型中场、中前卫、右前卫等多个位置。

2010年7月3日，穆蒂尼奥與[波圖簽下五年合約](../Page/波圖足球會.md "wikilink")，並在當時的教練[保亞斯的領導下](../Page/安德烈·維拉斯-波亞斯.md "wikilink")，協助球隊奪得[葡超聯賽冠軍](../Page/葡萄牙足球超级联赛.md "wikilink")、[葡萄牙盃冠軍及](../Page/葡萄牙盃.md "wikilink")[歐霸盃冠軍](../Page/歐霸盃.md "wikilink")，次年球季成功衛冕聯賽冠軍。

2013年5月24日，穆蒂尼奥與[波尔图隊友](../Page/波尔图足球俱乐部.md "wikilink")[-{zh-cn:罗德里格斯;zh-hk:占士·洛迪古斯;}-一同轉投剛重返](../Page/詹姆斯·罗德里格斯.md "wikilink")[法甲球队](../Page/法甲.md "wikilink")[摩纳哥](../Page/摩纳哥足球俱乐部.md "wikilink")。轉會費合共6,000萬[英鎊](../Page/英鎊.md "wikilink")(約7,000萬[歐元](../Page/歐元.md "wikilink"))，穆蒂尼奥身價為2,150萬[英鎊](../Page/英鎊.md "wikilink")(約2,500萬[歐元](../Page/歐元.md "wikilink"))，合約為期五年至2018年夏季\[1\]。

### 國家隊

穆蒂尼奥于2005年首次入选[葡萄牙国家队](../Page/葡萄牙国家足球队.md "wikilink")，年仅18岁的他于8月17日首次为国出征，对手是[埃及](../Page/埃及国家足球队.md "wikilink")。但他没能入选葡萄牙队[2006年世界杯的大名单](../Page/2006年世界杯.md "wikilink")。[2008年欧洲杯时](../Page/2008年欧洲足球锦标赛.md "wikilink")，穆蒂尼奥终于首次代表葡萄牙队出战重大国际比赛。[2012年歐洲國家盃及](../Page/2012年歐洲國家盃.md "wikilink")[2014年世界盃亦有入選國家隊並擔正](../Page/2014年世界盃.md "wikilink")，其後的[2016年歐洲國家盃](../Page/2016年歐洲國家盃.md "wikilink")，穆蒂尼奥協助國家隊晉身[決賽](../Page/2016年歐洲國家盃決賽.md "wikilink")，並於加時階段助攻予[艾達射入致勝一球](../Page/埃德尔_\(葡萄牙足球运动员\).md "wikilink")，葡萄牙最終首度染指大賽錦標。

## 榮譽

### 士砵亭

  - [葡萄牙盃冠軍](../Page/葡萄牙盃.md "wikilink")﹕2006-07、2007-08
  - [葡萄牙超級盃冠軍](../Page/葡萄牙超級盃.md "wikilink")﹕2007、2008

### 波圖

  - [歐霸盃冠軍](../Page/歐霸盃.md "wikilink")﹕[2010-11](../Page/2010/11賽季歐洲聯賽.md "wikilink")
  - [葡超聯賽冠軍](../Page/葡萄牙足球超级联赛.md "wikilink")﹕2010-11、2011-12、2012-13
  - [葡萄牙盃冠軍](../Page/葡萄牙盃.md "wikilink")﹕2010-11
  - [葡萄牙超級盃冠軍](../Page/葡萄牙超級盃.md "wikilink")﹕2010、2011、2012

### 摩納哥

  - [法甲聯賽](../Page/法國甲組足球联赛.md "wikilink")﹕2016-17
  - [法國聯賽盃亞軍](../Page/法國聯賽盃.md "wikilink")﹕2017

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [Official
    website](https://web.archive.org/web/20110224142337/http://moutinho28.com/)
  - [Stats and profile at
    zerozero.pt](http://www.zerozero.eu/uk/jogador.php?id=7608)
  - [穆蒂尼奥於葡萄牙国家足球队統計](http://www.fpf.pt/nationalteams/nationalteamplayerdetail/tabid/957/jogador/225972/language/en-US/Default.aspx)

[Category:葡萄牙足球运动员](../Category/葡萄牙足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:摩纳哥球員](../Category/摩纳哥球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:波圖球員](../Category/波圖球員.md "wikilink")
[Category:士砵亭球員](../Category/士砵亭球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")
[Category:法國外籍足球運動員](../Category/法國外籍足球運動員.md "wikilink")

1.