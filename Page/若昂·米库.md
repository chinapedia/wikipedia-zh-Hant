**若昂·米库**(**Johan Micoud**，1973年7月24日 -
)乃一名[法國足球員](../Page/法國.md "wikilink")，司職中場，現已退役。

## 生平

麥哥特早年曾效力法國球會[波爾多](../Page/波爾多足球俱樂部.md "wikilink")，主要負責入球工作。他曾為波爾多贏得1998-99年[法甲聯賽冠軍](../Page/法甲.md "wikilink")，同時期者還有[韋托特](../Page/西尔万·维尔托德.md "wikilink")。之後他效力[意甲的](../Page/意甲.md "wikilink")[帕爾馬](../Page/帕爾馬足球俱樂部.md "wikilink")，惟效力兩季僅能取得9球進帳，同時一直未能適應球會環境。於是2002年轉投[德甲的](../Page/德甲.md "wikilink")[雲達不萊梅](../Page/雲達不萊梅.md "wikilink")，並為該球會贏得2004年德甲聯賽和德國杯雙料冠軍。

麥哥特於1999年入選[法國國家隊](../Page/法國國家足球隊.md "wikilink")，並曾出戰[2002年韓日世界杯](../Page/2002年世界盃足球賽.md "wikilink")。由於他是攻擊中場，當時他被法國媒體認為是[施丹的最佳替補](../Page/施丹.md "wikilink")，但在國家隊卻一直表現平平，上陣17場僅得一球進帳。

隨著年事已高，自2004年起麥哥特已經從國家隊退下火線。而在2006-07年賽季，他亦重返法國的[波爾多](../Page/波爾多足球俱樂部.md "wikilink")。

## 榮譽

  - 法甲聯賽冠軍：1998-99
  - 德甲聯賽冠軍：2004

[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:康城球員](../Category/康城球員.md "wikilink")
[Category:波爾多球員](../Category/波爾多球員.md "wikilink")
[Category:帕爾馬球員](../Category/帕爾馬球員.md "wikilink")
[Category:雲達不萊梅球員](../Category/雲達不萊梅球員.md "wikilink")
[Category:1973年出生](../Category/1973年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:濱海阿爾卑斯省人](../Category/濱海阿爾卑斯省人.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:歐洲國家盃冠軍成員](../Category/歐洲國家盃冠軍成員.md "wikilink")