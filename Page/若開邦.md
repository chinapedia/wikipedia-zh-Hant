**若開邦**（）是[緬甸西南的一個邦](../Page/緬甸.md "wikilink")，與[孟加拉接壤](../Page/孟加拉.md "wikilink")。面積36,780平方公里，2014年該邦人口為318.8萬人\[1\]。首府[實兌](../Page/實兌.md "wikilink")，下轄5縣，邦境以南北向的[若開山脈為主](../Page/若開山脈.md "wikilink")，島嶼較多。“若開”一詞來自[巴利語](../Page/巴利語.md "wikilink")，即[食人魔之地](../Page/食人魔.md "wikilink")，可能是指其[矮黑人原住民](../Page/矮黑人.md "wikilink")。

## 歷史

古稱阿拉干王国，自古以[上座佛教立国](../Page/上座佛教.md "wikilink")，在文化上与缅甸更为接近，与西边的孟加拉地区也长期和睦相处。在阿拉干王国的古都妙乌（Mrauk
U），至今仍保留着一座几百年前[回教徒的](../Page/回教徒.md "wikilink")[清真寺](../Page/清真寺.md "wikilink")。不过，那个时候，定居在阿拉干的回教徒人数很少，主要是从事贸易的商人，今日若開邦總人口數約三百五十萬人，該地分為兩大族群：若開族和羅興亞人。

Dhanyawadi、Waithali、Lemro、[謬烏王國](../Page/謬烏王國.md "wikilink")（Mrauk
U）、貢榜王朝佔領的獨立王國（1784年～1826年）、英國統治（1826年～1948年）、緬甸（1948年～）的邦（省）。

## 行政區劃

  - [皎漂縣](../Page/皎漂縣.md "wikilink")（）
  - [孟都縣](../Page/孟都縣.md "wikilink")（）
  - [實兌縣](../Page/實兌縣.md "wikilink")（）
  - [丹兌縣](../Page/丹兌縣.md "wikilink")（）

## 城市

首府[實兌](../Page/實兌.md "wikilink")；重要城市尚有[孟都市](../Page/孟都縣.md "wikilink")、[布迪當市](../Page/布迪當.md "wikilink")、[皎漂市](../Page/皎漂.md "wikilink")（[中緬油氣管道的起点](../Page/中緬油氣管道.md "wikilink")）。

## 宗教衝突

若開邦居民[若開人](../Page/若開人.md "wikilink")（Rakhine）多数為信奉[上座部佛教](../Page/上座部佛教.md "wikilink")（南傳佛教）[佛教徒](../Page/佛教徒.md "wikilink")，少数信奉[伊斯兰教](../Page/伊斯兰教.md "wikilink")；由於謬烏王國国王[明蘇蒙曾经在临近的](../Page/明蘇蒙.md "wikilink")[孟加拉蘇丹國避难](../Page/孟加拉蘇丹國.md "wikilink")，因此有了[穆斯林社區](../Page/穆斯林.md "wikilink")，也成为[緬甸全国](../Page/緬甸.md "wikilink")[穆斯林最多的地区](../Page/穆斯林.md "wikilink")。以[罗兴亚人](../Page/罗兴亚人.md "wikilink")（Rohingya）为主的穆斯林人口，居该邦总人口约四分之一。但由于历史和宗教原因，缅甸官方指為是来自孟加拉国的非法定居者，因此不享有[公民权](../Page/公民权.md "wikilink")。

2012年6月中，若開邦的穆斯林（[罗兴亚族](../Page/罗兴亚族.md "wikilink")）與佛教徒（若開族、[緬族等](../Page/緬族.md "wikilink")）爆發衝突，緬甸總統[登盛於](../Page/登盛.md "wikilink")6月10日晚間宣布若開邦進入緊急狀態。罗兴亚族穆斯林的大量外逃。與2016年11月初，相似情事再次發生，約3萬人逃離家園。\[2\]

## 交通運輸

## 經濟

若開邦是緬甸最貧窮的州之一。69％以上的人口生活貧困。

水稻是該地區的主要作物，佔農業用地總面積的85％左右。椰子和尼帕棕櫚種植園也很重要。捕魚業是一個主要產業，大部分漁獲運到[仰光](../Page/仰光.md "wikilink")，但也有一些出口。從山上提取木材，竹子和木炭等木製品。少量的劣質原油是從原始的，淺的，手挖井中生產出來的，但石油和天然氣生產尚未發現。

旅遊業正在慢慢發展。古老的皇家城鎮[妙烏的廢墟和Ngapali海灘度假勝地是外國遊客的主要景點](../Page/妙烏.md "wikilink")，但設施仍然是原始的，交通基礎設施仍然不成熟。

## 教育

## 參考資料

[若开邦](../Category/若开邦.md "wikilink")
[Category:緬甸的邦](../Category/緬甸的邦.md "wikilink")

1.  [2014 Myanmar
    Censu](https://drive.google.com/file/d/0B067GBtstE5TeUlIVjRjSjVzWlk/view)
2.  [控緬甸種族淨化洛興雅人 亞洲爆示威](http://udn.com/news/story/5/2130027)