**苫小牧市**（）是[北海道西南部的城市](../Page/北海道.md "wikilink")，距離[札幌市僅九十分鐘的車程](../Page/札幌市.md "wikilink")。與[室蘭市都是北海道具代表性的工業都市](../Page/室蘭市.md "wikilink")、[企業城市和港灣都市](../Page/企業城市.md "wikilink")。自[明治時期該市就開始發展造紙業](../Page/明治時期.md "wikilink")，目前每年的[報紙用紙產量約有](../Page/報紙.md "wikilink")100頓，佔全日本的30%，其中[王子製紙苫小牧工場是全世界報紙產量最大的工廠](../Page/王子製紙.md "wikilink")\[1\]，因此目前苫小牧有「紙的城市」之稱，造紙工廠的紅白煙囪更被稱為「王子的煙囪」，成為當地的象徵。

在日本經濟高速成長期時，由於政府在這裡興建苫小牧港、加上有[道央自動車道經過](../Page/道央自動車道.md "wikilink")，並緊臨[新千歲機場](../Page/新千歲機場.md "wikilink")，因此成為綜合工業區，建設了大量的工業基地。

雖然為工業城市，但仍保有豐富的[自然資源](../Page/自然資源.md "wikilink")，東側有被列入[國際重要濕地名錄的宇都內湖](../Page/國際重要濕地名錄.md "wikilink")，同時[北寄貝的漁獲量佔全日本的一成](../Page/北寄貝.md "wikilink")，因此也成為苫小牧市市貝。

市內盛行[競速滑冰和](../Page/競速滑冰.md "wikilink")[冰球](../Page/冰球.md "wikilink")，市內有許多滑冰場和冰球隊。此外，市內的[駒澤大學附屬苫小牧高等學校先後在](../Page/駒澤大學附屬苫小牧高等學校.md "wikilink")2004年、2005年的[夏季甲子園連續取得優勝](../Page/夏季甲子園.md "wikilink")（冠軍），並於2006年獲得準優勝（[亞軍](../Page/亞軍.md "wikilink")），也是目前北海道唯一曾經獲得甲子園優勝的學校\[2\]。

市名源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「to-mak-oma-i」，意思是沼澤內的河。過去的[苫小牧川在阿伊努語中稱為](../Page/苫小牧川.md "wikilink")「mak-oma-i」，意思為進入深山的河川，而苫小牧川在通過沼澤的地方則是加上阿伊努語中有沼澤意義的「to」，因此就成為「to-mak-oma-i」。\[3\]

## 歷史

  - 1799年：設置勇拂會所。\[4\]
  - 1800年：[八王子千人同心從](../Page/八王子千人同心.md "wikilink")[武藏國](../Page/武藏國.md "wikilink")[多摩郡八王子](../Page/多摩郡.md "wikilink")（現在的[東京都](../Page/東京都.md "wikilink")[八王子市](../Page/八王子市.md "wikilink")）派遣約100人北海道[勇拂地區進行防衛工作及開墾](../Page/勇拂郡.md "wikilink")。\[5\]
  - 1870年：[高知藩派遣](../Page/高知藩.md "wikilink")70餘人至勇拂、千歲進行開墾。
  - 1873年9月27日：勇拂郡開拓使出張所改遷移至苫細（現在的苫小牧）。
  - 1874年8月20日：苫細改名為苫小牧。
  - 1880年：設置勇拂外5郡役所（勇拂郡、[白老郡](../Page/白老郡.md "wikilink")、[千歲郡](../Page/千歲郡.md "wikilink")、[沙流郡](../Page/沙流郡.md "wikilink")、[新冠郡](../Page/新冠郡.md "wikilink")、[靜內郡](../Page/靜內郡.md "wikilink")）於苫小牧村。
  - 1889年：在苫小牧村設置苫小牧外15村戶長役場。
  - 1902年4月1日：樽前村、覺生村、錦多峰村、小糸魚村、苫小牧村、勇拂村、植苗村合併為苫小牧村，並成為北海道二級村。
  - 1910年：[王子製紙苫小牧工場開始營運](../Page/王子製紙.md "wikilink")。
  - 1918年1月1日：改制為苫小牧町，並成為北海道二級町。
  - 1919年4月1日：成為北海道一級町。
  - 1948年4月1日：改制為苫小牧市。
  - 1981年：[苫小牧港升格為特定重要港灣](../Page/苫小牧港.md "wikilink")。
  - 1982年10月：第13回國勢調查結果，苫小牧市人口超過[室蘭市](../Page/室蘭市.md "wikilink")，成為膽振支廳內最大城市。
  - 2004年3月：人口超越[帶廣市](../Page/帶廣市.md "wikilink")，成為北海道第五大城，次於[札幌市](../Page/札幌市.md "wikilink")、[旭川市](../Page/旭川市.md "wikilink")、[函館市](../Page/函館市.md "wikilink")、[釧路市](../Page/釧路市.md "wikilink")。

## 產業

苫小牧港的[飼料](../Page/飼料.md "wikilink")、[肥料輸入輸入量佔了全北海道的](../Page/肥料.md "wikilink")39%，紙和紙漿的輸出量則佔了44%。

在此設廠的主要產業包括：

  - 製紙業

<!-- end list -->

  - [王子製紙苫小牧工場](../Page/王子製紙.md "wikilink")
  - [日本製紙勇拂工場](../Page/日本製紙.md "wikilink")
  - 王子Nepia苫小牧工場

<!-- end list -->

  - 汽車零件製造業

<!-- end list -->

  - [五十鈴引擎製造北海道公司](../Page/五十鈴.md "wikilink")
  - [豐田汽車北海道公司](../Page/豐田汽車.md "wikilink")
  - [愛信精機北海道株式會社](../Page/愛信精機.md "wikilink")

<!-- end list -->

  - 鑛工業

<!-- end list -->

  - [石油資源開發](../Page/石油資源開發.md "wikilink")（勇拂天然氣田）
  - [出光興產北海道製油所](../Page/出光興產.md "wikilink")

## 交通

[New_Chitose_Airport.jpg](https://zh.wikipedia.org/wiki/File:New_Chitose_Airport.jpg "fig:New_Chitose_Airport.jpg")
[Tomakomai_Station_South.jpg](https://zh.wikipedia.org/wiki/File:Tomakomai_Station_South.jpg "fig:Tomakomai_Station_South.jpg")
[Tomakomai_Station_North.jpg](https://zh.wikipedia.org/wiki/File:Tomakomai_Station_North.jpg "fig:Tomakomai_Station_North.jpg")

### 機場

  - [新千歲機場](../Page/新千歲機場.md "wikilink")：位於[千歲市和苫小牧市邊界](../Page/千歲市.md "wikilink")，部份區域位於苫小牧市轄區。

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [室蘭本線](../Page/室蘭本線.md "wikilink")：[錦岡車站](../Page/錦岡車站.md "wikilink")
        - [糸井車站](../Page/糸井車站.md "wikilink") -
        [青葉車站](../Page/青葉車站.md "wikilink") -
        [苫小牧車站](../Page/苫小牧車站.md "wikilink") -
        [沼之端車站](../Page/沼之端車站.md "wikilink")
      - [千歲線](../Page/千歲線.md "wikilink")：沼之端車站 -
        [植苗車站](../Page/植苗車站.md "wikilink")
      - [日高本線](../Page/日高本線.md "wikilink")：苫小牧車站 -
        [勇拂車站](../Page/勇拂車站.md "wikilink")
  - 苫小牧港開發鐵道部（1968年～2001年）
      - 臨海鐵路：港南車站 - 石油埠頭車站
  - 王子製紙輕便鐵路（1908年～1951年）
      - 苫小牧-烏柵舞間本線（山線）
      - 苫小牧工場-苫小牧海岸間採砂線（海岸線）
      - 連絡線、坊主山線

### 航運

  - 苫小牧港西港
      - [川崎近海汽船](../Page/川崎近海汽船.md "wikilink")：往[八戶港](../Page/八戶港.md "wikilink")
      - [商船三井渡輪](../Page/商船三井渡輪.md "wikilink")：往[大洗港](../Page/大洗港.md "wikilink")
      - [太平洋渡輪](../Page/太平洋渡輪.md "wikilink")：往[仙台港](../Page/仙台港.md "wikilink")～[名古屋港](../Page/名古屋港.md "wikilink")
  - 苫小牧港東港
      - [新日本海渡輪](../Page/新日本海渡輪.md "wikilink")：往[秋田港](../Page/秋田港.md "wikilink")～[新潟港](../Page/新潟港.md "wikilink")～[敦賀港](../Page/敦賀港.md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [道央自動車道](../Page/道央自動車道.md "wikilink")：[樽前休息區](../Page/樽前休息區.md "wikilink")
    - [苫小牧西交流道](../Page/苫小牧西交流道.md "wikilink") -
    [苫小牧中央交流道](../Page/苫小牧中央交流道.md "wikilink")
    - [苫小牧東交流道](../Page/苫小牧東交流道.md "wikilink")　-
    [美澤休息區](../Page/美澤休息區.md "wikilink")
  - [日高自動車道](../Page/日高自動車道.md "wikilink")：苫小牧東交流道 -
    [沼之端西交流道](../Page/沼之端西交流道.md "wikilink") -
    [沼之端東交流道](../Page/沼之端東交流道.md "wikilink") -
    [苫東中央交流道](../Page/苫東中央交流道.md "wikilink")
      - 苫小牧東交流道
      - 沼之端西交流道
      - 沼之端東交流道
      - 苫東中央交流道

<!-- end list -->

  - [一般國道](../Page/一般國道.md "wikilink")

<!-- end list -->

  - [國道36號](../Page/國道36號.md "wikilink")（室蘭街道）
  - [國道234號](../Page/國道234號.md "wikilink")（早來國道
  - [國道235號](../Page/國道235號.md "wikilink")（浦河國道）
  - [國道276號](../Page/國道276號.md "wikilink")（樽前國道）
  - [國道453號](../Page/國道453號.md "wikilink")

<!-- end list -->

  - [主要地方道](../Page/主要地方道.md "wikilink")

<!-- end list -->

  - 北海道道10號千歲鵡川線
  - 北海道道16號支笏湖公園線
  - 北海道道19號苫小牧停車場線
  - 北海道道91號苫小牧東內部線
  - 北海道道129號靜川美澤線
  - 北海道道130號新千歲空港線
  - 北海道道141號樽前錦岡線

<!-- end list -->

  - [道道](../Page/都道府縣道.md "wikilink")

<!-- end list -->

  - 北海道道259號上厚真苫小牧線
  - 北海道道781號苫小牧環狀線

### 巴士

  - 道南巴士：市內路線、往札幌、新千歲機場空港、登別、平取～浦河方向
  - 北海道中央巴士：高速苫小牧號
  - 厚真巴士：往早來～厚真方向

## 觀光景點

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="觀光">觀光</h3>
<p><a href="https://zh.wikipedia.org/wiki/File:Utonai_Lake_Hokkaido_Japan.jpg" title="fig:Utonai_Lake_Hokkaido_Japan.jpg">Utonai_Lake_Hokkaido_Japan.jpg</a></p>
<ul>
<li>宇都內湖</li>
<li>錦大沼公園</li>
<li>樽前峽谷</li>
<li>丸山遠見</li>
<li>口無沼</li>
<li>苫小牧市博物館</li>
<li>苫小牧市科學中心</li>
<li>白鳥體育場</li>
<li>靜川遺跡</li>
<li>開拓使三角測量勇拂基準點</li>
<li>北海道大學苫小牧地方研究林</li>
<li>勇武津資料館</li>
<li>綠丘公園</li>
<li>支笏洞爺國立公園</li>
<li>佛寶寺</li>
<li>白樺溫泉</li>
<li>樽前溫泉</li>
</ul></td>
<td><h3 id="祭典">祭典</h3>
<ul>
<li>苫小牧滑冰祭（每年2月）</li>
<li>綠丘公園祭（每年5月）</li>
<li>樽前山神社例大祭（每年7月）</li>
<li>苫小牧港祭（每年8月）</li>
<li>紙慶典（每年9月上旬）</li>
<li>山線祭（每年9月中旬）</li>
</ul>
<h3 id="文化遺產">文化遺產</h3>
<ul>
<li>北海道大學苫小牧研究林森林記念館（舊標本貯蔵室）（國指定登陸有形文化財）</li>
<li>靜川遺跡（國指定史跡）</li>
<li>阿伊努丸木舟及推進具（北海道指定有形文化財）：現為苫小牧市博物館藏</li>
<li>開拓使三角測量勇拂基準點（北海道指定史跡）</li>
<li>樽前山熔岩圓頂丘（北海道指定天然記念物）</li>
</ul>
<p><a href="https://zh.wikipedia.org/wiki/File:Tomakomai_Hakucho_Arena.jpg" title="fig:Tomakomai_Hakucho_Arena.jpg">Tomakomai_Hakucho_Arena.jpg</a>]]</p></td>
</tr>
</tbody>
</table>

## 教育

### 大學、短期大學

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/苫小牧駒澤大學.md" title="wikilink">苫小牧駒澤大學</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/北海道大學.md" title="wikilink">北海道大學</a>
<ul>
<li><a href="http://forest.fsc.hokudai.ac.jp/~exfor/Toef/toef.html">苫小牧研究林</a></li>
<li>低溫科學研究所凍上觀測室</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

### 高等專門學校

  - [苫小牧工業高等專門學校](http://www.tomakomai-ct.ac.jp/)

### 高等學校

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>道立
<ul>
<li><a href="http://www.tomahigashi.hokkaido-c.ed.jp/">道立北海道苫小牧東高等學校</a></li>
<li><a href="http://www.tomakomaiminami.ed.jp/">道立北海道苫小牧南高等學校</a></li>
<li><a href="http://www.tomanishi.hokkaido-c.ed.jp/">道立北海道苫小牧西高等學校</a></li>
<li><a href="http://www.tomakomai-th.ed.jp/">道立北海道苫小牧工業高等學校</a></li>
<li><a href="http://www.soukei.hokkaido-c.ed.jp/">道立北海道苫小牧綜合經濟高等學校</a></li>
</ul></li>
</ul></td>
<td><ul>
<li>私立
<ul>
<li><a href="http://www.komazawa-uth.ed.jp/">私立駒澤大学附屬苫小牧高等學校</a></li>
<li><a href="http://www.haragakuen.ac.jp/">私立苫小牧中央高等學校</a></li>
<li><a href="http://www.tomasho.ac.jp/">私立苫小牧高等商業學校</a></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20071113234712/http://www.city.tomakomai.hokkaido.jp/akeno-c/">苫小牧市立明野中學校</a></li>
<li><a href="https://web.archive.org/web/20071114133437/http://www.city.tomakomai.hokkaido.jp/uenae-c/index.htm">苫小牧市立植苗中學校</a></li>
<li><a href="https://web.archive.org/web/20071009220601/http://www.city.tomakomai.hokkaido.jp/kaisei/">苫小牧市立開成中學校</a></li>
<li><a href="https://web.archive.org/web/20071113234726/http://www.city.tomakomai.hokkaido.jp/keihoku/">苫小牧市立啓北中學校</a>
<ul>
<li><a href="https://web.archive.org/web/20071113234758/http://www.city.tomakomai.hokkaido.jp/yamanami/">苫小牧市立啟北中學校山那美分校</a></li>
</ul></li>
<li>苫小牧市立青翔中學校（預定2009年度開始招生）</li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20071113234731/http://www.city.tomakomai.hokkaido.jp/keimei/">苫小牧市立啟明中學校</a></li>
<li><a href="https://web.archive.org/web/20071114133306/http://www.city.tomakomai.hokkaido.jp/kouyou/">苫小牧市立光洋中學校</a></li>
<li><a href="https://web.archive.org/web/20071113234741/http://www.city.tomakomai.hokkaido.jp/numanohata-c/">苫小牧市立沼之端中學校</a></li>
<li><a href="https://web.archive.org/web/20071113234720/http://www.city.tomakomai.hokkaido.jp/higasi-c/">苫小牧市立苫小牧東中學校</a></li>
<li><a href="https://web.archive.org/web/20071113234736/http://www.city.tomakomai.hokkaido.jp/meirin/">苫小牧市立明倫中學校</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20071113234803/http://www.city.tomakomai.hokkaido.jp/yayoi/">苫小牧市立彌生中學校</a></li>
<li><a href="https://web.archive.org/web/20071113234808/http://www.city.tomakomai.hokkaido.jp/yuufutu-c/">苫小牧市立勇拂中學校</a></li>
<li><a href="https://web.archive.org/web/20071113234748/http://www.city.tomakomai.hokkaido.jp/ryoun/">苫小牧市立凌雲中學校</a></li>
<li><a href="https://web.archive.org/web/20071114133326/http://www.city.tomakomai.hokkaido.jp/ryokuryo/index.html">苫小牧市立綠陵中學校</a></li>
<li><a href="https://web.archive.org/web/20071113234753/http://www.city.tomakomai.hokkaido.jp/wako/">小牧市立和光中學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20080207055504/http://www.city.tomakomai.hokkaido.jp/akeno-s/index.htm">苫小牧市立明野小學校</a></li>
<li><a href="https://web.archive.org/web/20071009081829/http://www.city.tomakomai.hokkaido.jp/izumi/">苫小牧市立泉野小學校</a></li>
<li><a href="https://web.archive.org/web/20071114133200/http://www.city.tomakomai.hokkaido.jp/itoi/">苫小牧市立糸井小學校</a></li>
<li><a href="https://web.archive.org/web/20071114133437/http://www.city.tomakomai.hokkaido.jp/uenae-c/index.htm">苫小牧市立植苗小學校</a></li>
<li><a href="https://web.archive.org/web/20071010052855/http://www.city.tomakomai.hokkaido.jp/simizu/">苫小牧市立清水小學校</a></li>
<li><a href="https://web.archive.org/web/20071009081633/http://www.city.tomakomai.hokkaido.jp/sumikawa/">苫小牧市立澄川小學校</a></li>
<li><a href="https://web.archive.org/web/20071009082044/http://www.city.tomakomai.hokkaido.jp/taisei/">苫小牧市立大成小學校</a></li>
<li><a href="https://web.archive.org/web/20071009081628/http://www.city.tomakomai.hokkaido.jp/takuyu/">苫小牧市立拓勇小學校</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20071010053022/http://www.city.tomakomai.hokkaido.jp/tarumae/">苫小牧市立樽前小學校</a></li>
<li><a href="https://web.archive.org/web/20071010052718/http://www.city.tomakomai.hokkaido.jp/toyokawa/">苫小牧市立豐川小學校</a></li>
<li><a href="https://web.archive.org/web/20071203172909/http://www.city.tomakomai.hokkaido.jp/nisi/">苫小牧市立苫小牧西小學校</a></li>
<li><a href="https://web.archive.org/web/20071009220909/http://www.city.tomakomai.hokkaido.jp/nisikioka/">苫小牧市立錦岡小學校</a></li>
<li><a href="https://web.archive.org/web/20071010052833/http://www.city.tomakomai.hokkaido.jp/nissin/">苫小牧市立日新小學校</a></li>
<li><a href="https://web.archive.org/web/20071009220747/http://www.city.tomakomai.hokkaido.jp/numanohata-s/">苫小牧市立沼之端小學校</a></li>
<li><a href="https://web.archive.org/web/20071206121557/http://www.city.tomakomai.hokkaido.jp/higasi-s/">苫小牧市立苫小牧東小學校</a></li>
<li><a href="https://web.archive.org/web/20071010031446/http://www.city.tomakomai.hokkaido.jp/hokusei/">苫小牧市立北星小學校</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20071009081905/http://www.city.tomakomai.hokkaido.jp/hokkou/">苫小牧市立北光小學校</a></li>
<li><a href="https://web.archive.org/web/20071009055310/http://www.city.tomakomai.hokkaido.jp/misono/">苫小牧市立美園小學校</a></li>
<li><a href="https://web.archive.org/web/20071009020157/http://www.city.tomakomai.hokkaido.jp/midori/">苫小牧市立綠小學校</a></li>
<li><a href="https://web.archive.org/web/20071010031728/http://www.city.tomakomai.hokkaido.jp/meitoku/">苫小牧市立明德小學校</a></li>
<li><a href="https://web.archive.org/web/20071009081754/http://www.city.tomakomai.hokkaido.jp/yuufutu-s/">苫小牧市立勇拂小學校</a></li>
<li><a href="https://web.archive.org/web/20080209204458/http://www.city.tomakomai.hokkaido.jp/wakakusa/top-1.html">苫小牧市立若草小學校</a></li>
<li>苫小牧市立宇都內小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [八王子市](../Page/八王子市.md "wikilink")（[東京都](../Page/東京都.md "wikilink")）：於1973年8月10日與[清水市締結姊妹都市](../Page/清水市.md "wikilink")。\[6\]
  - [日光市](../Page/日光市.md "wikilink")（[栃木縣](../Page/栃木縣.md "wikilink")）：於1982年4月16日與[清水市締結姊妹都市](../Page/清水市.md "wikilink")。

### 海外

  - [內皮爾](../Page/內皮爾_\(紐西蘭\).md "wikilink")（[紐西蘭](../Page/紐西蘭.md "wikilink")
    [豪克斯灣](../Page/豪克斯灣.md "wikilink")）：於1980年4月22日締結姊妹都市。

  - [秦皇島市](../Page/秦皇島市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")
    [河北省](../Page/河北省.md "wikilink")）：於1998年9月10日締結友好都市。

## 本地出身的名人

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="政治">政治</h3>
<ul>
<li><a href="../Page/岩倉博文.md" title="wikilink">岩倉博文</a>：<a href="../Page/政治家.md" title="wikilink">政治家</a></li>
<li><a href="../Page/五十嵐紀男.md" title="wikilink">五十嵐紀男</a>：前東京地方檢查署特捜部長</li>
<li><a href="../Page/吉野準.md" title="wikilink">吉野準</a>：前日本<a href="../Page/警視總監.md" title="wikilink">警視總監</a></li>
<li><a href="../Page/木元教子.md" title="wikilink">木元教子</a>：<a href="../Page/評論家.md" title="wikilink">評論家</a></li>
</ul>
<h3 id="文化">文化</h3>
<ul>
<li><a href="../Page/工藤榮一.md" title="wikilink">工藤榮一</a>：<a href="../Page/動畫監督.md" title="wikilink">動畫監督</a></li>
<li><a href="../Page/三上慎也.md" title="wikilink">三上慎也</a>：動畫監督</li>
<li><a href="../Page/矢嶋佑.md" title="wikilink">矢嶋佑</a>：動畫監督</li>
<li><a href="../Page/唐部太郎.md" title="wikilink">唐部太郎</a>：<a href="../Page/作家.md" title="wikilink">作家</a></li>
<li><a href="../Page/成田洋治.md" title="wikilink">成田洋治</a>：<a href="../Page/演出家.md" title="wikilink">演出家</a></li>
<li><a href="../Page/前田弘信.md" title="wikilink">前田弘信</a>：<a href="../Page/劇作家.md" title="wikilink">劇作家</a></li>
<li><a href="../Page/篠有紀子.md" title="wikilink">篠有紀子</a>：<a href="../Page/漫畫家.md" title="wikilink">漫畫家</a></li>
<li><a href="../Page/泉野明.md" title="wikilink">泉野明</a>：<a href="../Page/漫畫主角.md" title="wikilink">漫畫主角</a></li>
</ul></td>
<td><h3 id="運動">運動</h3>
<ul>
<li><a href="../Page/府元晶.md" title="wikilink">府元晶</a>：遊戲作者</li>
<li><a href="../Page/柴田誠也.md" title="wikilink">柴田誠也</a>：<a href="../Page/職業棒球.md" title="wikilink">職業棒球選手</a></li>
<li><a href="../Page/梅村禮.md" title="wikilink">梅村禮</a>：<a href="../Page/卓球.md" title="wikilink">卓球選手</a></li>
<li><a href="../Page/鈴木惠一.md" title="wikilink">鈴木惠一</a>：<a href="../Page/競速滑冰.md" title="wikilink">競速滑冰</a></li>
<li><a href="../Page/岩本裕司.md" title="wikilink">岩本裕司</a>：競速滑冰選手</li>
<li><a href="../Page/櫻井邦彥.md" title="wikilink">櫻井邦彥</a>：競速滑冰選手</li>
<li><a href="../Page/山縣優.md" title="wikilink">山縣優</a>：<a href="../Page/職業摔角.md" title="wikilink">職業摔角</a></li>
<li><a href="../Page/佐野巧真.md" title="wikilink">佐野巧真</a>：<a href="../Page/職業摔角.md" title="wikilink">職業摔角</a></li>
<li><a href="../Page/高西翔太.md" title="wikilink">高西翔太</a>：<a href="../Page/職業摔角.md" title="wikilink">職業摔角</a></li>
<li><a href="../Page/藤澤和雄.md" title="wikilink">藤澤和雄</a>：<a href="../Page/賽馬.md" title="wikilink">賽馬</a><a href="../Page/馴馬師.md" title="wikilink">馴馬師</a></li>
</ul>
<h3 id="藝人">藝人</h3>
<ul>
<li><a href="../Page/伊藤多喜雄.md" title="wikilink">伊藤多喜雄</a>：<a href="../Page/民謠.md" title="wikilink">民謠</a><a href="../Page/歌手.md" title="wikilink">歌手</a></li>
<li><a href="../Page/小笠原千秋.md" title="wikilink">小笠原千秋</a>：<a href="../Page/爵士樂.md" title="wikilink">爵士樂</a><a href="../Page/歌手.md" title="wikilink">歌手</a></li>
<li><a href="../Page/堀江淳.md" title="wikilink">堀江淳</a>：<a href="../Page/歌手.md" title="wikilink">歌手</a></li>
<li><a href="../Page/松村和子.md" title="wikilink">松村和子</a>：<a href="../Page/歌手.md" title="wikilink">歌手</a></li>
<li><a href="../Page/堀內沙織.md" title="wikilink">堀內沙織</a>：<a href="../Page/創作歌手.md" title="wikilink">創作歌手</a></li>
<li><a href="../Page/SHOKICHI.md" title="wikilink">SHOKICHI</a>：<a href="../Page/藝術家.md" title="wikilink">藝術家</a>・<a href="../Page/表演者.md" title="wikilink">表演者</a>、(現<a href="../Page/EXILE.md" title="wikilink">EXILE</a>、前<a href="../Page/J_Soul_Brothers.md" title="wikilink">J Soul Brothers</a>)</li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [苫小牧觀光協會](https://web.archive.org/web/20070705111815/http://www1.tomakomai.or.jp/kanko/)

  - [苫小牧港管理組合](http://www.jptmk.com/)

  - [苫小牧警察署](https://web.archive.org/web/20071103104602/http://www.tomakomai-syo.police.pref.hokkaido.jp/)

  - [苫小牧地方環境監視中心](https://web.archive.org/web/20071027041538/http://www1a.biglobe.ne.jp/tomasen/)

  - [苫小牧市工業技術中心](http://www.tomatech.jp/)

  - [苫小牧工商會議所](https://web.archive.org/web/20071028145708/http://cci.tomakomai.or.jp/)

  - [苫小牧青年會議所](http://toma-jc.jp/)

[Category:日本企業城市](../Category/日本企業城市.md "wikilink")

1.  苫小牧觀光協會 - 苫小牧市的情報
    <http://www1.tomakomai.or.jp/kanko/tomakomaijyouhou.htm>
2.  日本高等學校野球聯盟 - 選手權大會小史 <http://www.jhbf.or.jp/sensyuken/history/#09>
3.  苫小牧市官方網頁 - 苫小牧市的概要
    <http://www.city.tomakomai.hokkaido.jp/tomakomai.htm>
4.  苫小牧市官方網頁 - 苫小牧市的歷史
    <http://www.city.tomakomai.hokkaido.jp/history.htm>
5.  苫小牧市官方網頁 - 八王子千人同心
    <http://www.city.tomakomai.hokkaido.jp/dousin.htm>
6.  苫小牧市官方網頁 - 姊妹都市和友好都市
    <http://www.city.tomakomai.hokkaido.jp/sister.htm>