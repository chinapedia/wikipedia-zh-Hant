**苯乙酮**或稱**乙酰苯**，是分子式為C<sub>6</sub>H<sub>5</sub>COCH<sub>3</sub>的[有機化合物](../Page/有機化合物.md "wikilink")，它可當作製造[藥物](../Page/藥物.md "wikilink")、[樹脂](../Page/樹脂.md "wikilink")、調味劑和[催淚瓦斯的](../Page/催淚瓦斯.md "wikilink")[中間體](../Page/中間體.md "wikilink")，還可以製造[安眠藥](../Page/安眠藥.md "wikilink")。\[1\]
现在苯乙酮大多以[异丙苯氧化制](../Page/异丙苯.md "wikilink")[苯酚和](../Page/苯酚.md "wikilink")[丙酮的副产品获得](../Page/丙酮.md "wikilink")，它还可由[苯用](../Page/苯.md "wikilink")[乙醯氯](../Page/乙醯氯.md "wikilink")[乙酰化制得](../Page/乙酰化.md "wikilink")。

## 存在

苯乙酮存在許多食品中，包括[蘋果](../Page/蘋果.md "wikilink")，[奶酪](../Page/奶酪.md "wikilink")，[杏](../Page/杏.md "wikilink")，[香蕉](../Page/香蕉.md "wikilink")，[牛肉](../Page/牛肉.md "wikilink")，和[椰菜花](../Page/椰菜花.md "wikilink")。

## 另见

  - [苯乙醛](../Page/苯乙醛.md "wikilink")
  - [氯苯乙酮](../Page/氯苯乙酮.md "wikilink")

## 参考文献

[Category:酮类溶剂](../Category/酮类溶剂.md "wikilink")
[Category:芳族酮](../Category/芳族酮.md "wikilink")
[Category:空气污染物](../Category/空气污染物.md "wikilink")
[Category:赋形剂](../Category/赋形剂.md "wikilink")

1.  "乙醯苯"。 大英百科全書。 2008年。 大英線上繁體中文版。