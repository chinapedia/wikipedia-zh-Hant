[曼島](../Page/曼島.md "wikilink"){{·}}
[直佈羅陀](../Page/直佈羅陀.md "wikilink"){{·}}
[馬耳他](../Page/馬耳他.md "wikilink") |congregations = 4,650 \[1\]
|aid = [All We Can](../Page/All_We_Can.md "wikilink") |ministers = 1,760
\[2\] |members = 202,000 \[3\] |publications = Methodist Recorder
|website =  }} **英國循道會**（**Methodist Church of Great
Britain**）是英國的[循道宗教會](../Page/循道宗.md "wikilink")，也是全世界所有循道會的母教\[4\]。1738年，它由[英国人](../Page/英国人.md "wikilink")[约翰·卫斯理](../Page/约翰·卫斯理.md "wikilink")（1703年－1791年）和其弟[查理·卫斯理于](../Page/查理·卫斯理.md "wikilink")[伦敦创立](../Page/伦敦.md "wikilink")，現在信仰者超20万人\[5\]。英國循道會的最高組織是每年由會長舉行的循道會議會（Methodist
Conference）。

## 参考文献

## 外部連接

  -
  -
  - [Methodist Diaconal
    Order](http://www.methodistdiaconalorder.org.uk/)

  - at methodist.org.uk

  - [Anglican-Methodist Covenant](http://www.anglican-methodist.org.uk/)

  - [Methodist Recorder newspaper](http://www.methodistrecorder.co.uk/)

  - [Methodist Evangelicals Together (formerly
    Headway)](https://web.archive.org/web/20080216142307/http://www.met-uk.org/met/index.php)

  - [Dictionary of Methodism in Britain and
    Ireland](http://dmbi.wesleyhistoricalsociety.org.uk/)

  - [BBC](../Page/BBC.md "wikilink") - Religions - [Christianity:
    Methodist
    Church](http://www.bbc.co.uk/religion/religions/christianity/subdivisions/methodist_1.shtml)

[Category:英國循道會](../Category/英國循道會.md "wikilink")
[Category:1932年建立的組織](../Category/1932年建立的組織.md "wikilink")

1.
2.
3.
4.

5.