这是一份英国报刊列表。

英国报章可分为两类，一类是严肃报章：由于版面较大，被称为[大报](../Page/大报.md "wikilink")，比如《[每日电讯报](../Page/每日电讯报.md "wikilink")》，这些报章有時被统称为“优质报章
”；另一类报章严肃性较低，他们更多地报道明星或有趣的信息而较少报道政治及国际新闻：由于版面较小，被称为[小报](../Page/小报.md "wikilink")，这些报章有時被统称为“通俗报章
”。小报又分为更加哗众取宠的“红标题”报纸，如《[太阳报](../Page/太陽報_\(英國\).md "wikilink")》和《[镜报](../Page/每日鏡報.md "wikilink")》，和中间市场报纸，如《[每日快报](../Page/每日快报.md "wikilink")》和《[每日邮报](../Page/每日邮报.md "wikilink")》。

在电视喜剧《[是，首相](../Page/是，首相.md "wikilink")》中，虚构的首相吉姆·哈克如此向内阁文官解释英国主要报章的读者：

  -
    吉姆·哈克首相：不要跟我说教媒体。我清楚地知道报章的读者：《[每日镜报](../Page/每日镜报.md "wikilink")》的读者自认为自己管理国家，《[卫报](../Page/卫报.md "wikilink")》的读者认为该由自己管理国家，《[泰晤士报](../Page/泰晤士报.md "wikilink")》的读者真正管理国家，《[每日邮报](../Page/每日邮报.md "wikilink")》的读者是真正管理国家的人的妻子，《[金融时报](../Page/金融时报.md "wikilink")》的读者拥有这个国家，《[晨星报](../Page/晨星报.md "wikilink")》的读者认为英国应该由其他国家来管理，《[每日电讯报](../Page/每日电讯报.md "wikilink")》的读者认为英国已经由其他国家管理。
    汉弗莱爵士：首相，那么《太阳报》的读者呢？
    伯纳德：《太阳报》的读者不关心谁管理国家，只要她有大咂儿就行。

近年来，《[独立报](../Page/独立报.md "wikilink")》和《[泰晤士报](../Page/泰晤士报.md "wikilink")》更换为濃縮報，極為紧密式的報刊格式，只比小报大一点。《卫报》从2005年9月开始使用“[柏林版式](../Page/柏林版式.md "wikilink")”，稍比紧密式大一点，其周日姊妹刊《观察家》也随之更改。

## 英国近代报纸列表

| 名称                                     | 存在时间         | 性质 |
| -------------------------------------- | ------------ | -- |
| [每周新闻](../Page/每周新闻.md "wikilink")     | 1621年9月\~10月 | 周报 |
| [报纸](../Page/英国近代报纸《报纸》.md "wikilink") | 1622年\~1632年 |    |
| [不列颠信使](../Page/不列颠信使.md "wikilink")   | 1625年\~1627年 |    |
| [政治信使](../Page/政治信使.md "wikilink")     | 1650年\~1660年 | 周报 |
| [公众情报员](../Page/公众情报员.md "wikilink")   | 1655年\~1660年 | 周报 |
| [牛津公报](../Page/牛津公报.md "wikilink")     | 1665年\~      |    |
| [每日郵報](../Page/每日郵報.md "wikilink")     | 1702年\~1735年 | 日报 |
| [评论](../Page/评论.md "wikilink")         | 1704年\~1713年 | 周报 |
| [闲谈者](../Page/闲谈者.md "wikilink")       | 1709年\~1711年 | 周报 |
| [考察家](../Page/考察家.md "wikilink")       | 1710年\~1714年 |    |
| [旁观者](../Page/旁观者.md "wikilink")       | 1711年\~1712年 | 日报 |
| [自由人](../Page/自由人.md "wikilink")       | 1715年\~1716年 |    |
| [自由英国人](../Page/自由英国人.md "wikilink")   | 1729年\~1735年 |    |
| [每日公报](../Page/每日公报.md "wikilink")     | 1735年\~1748年 | 日报 |
| [每日广告报](../Page/每日广告报.md "wikilink")   | 1730年\~1807年 | 日报 |
| [大众广告报](../Page/大众广告报.md "wikilink")   | 1752年\~1798年 | 日报 |
| [早晨纪事报](../Page/早晨纪事报.md "wikilink")   | 1769年\~1862年 | 日报 |
| [晨邮报](../Page/晨邮报.md "wikilink")       | 1772年\~1937年 |    |
| [泰晤士报](../Page/泰晤士报.md "wikilink")     | 1785年\~      | 日报 |
| [每日电讯报](../Page/每日电讯报.md "wikilink")   | 1855年\~      | 日报 |
| [贫民导报](../Page/贫民导报.md "wikilink")     | 1830年\~1835年 |    |
| [北极星报](../Page/北极星报.md "wikilink")     | 1837年\~1852年 |    |
| [蜂房报](../Page/蜂房报.md "wikilink")       | 1861年\~1876年 | 周报 |
| [共和国报](../Page/共和国报.md "wikilink")     | 1865年\~1867年 |    |

## 英国全国性报纸列表

### 关注严肃议题的报纸

#### 大报版式

  - [每日電訊報](../Page/每日電訊報.md "wikilink")
  - [金融時報](../Page/金融時報.md "wikilink")
  - [星期日电讯报](../Page/星期日电讯报.md "wikilink")
  - [星期日泰晤士报](../Page/星期日泰晤士报.md "wikilink")

#### 柏林版式

  - [衞報](../Page/衞報.md "wikilink")（2005\~2018，2018年1月15日改为以小报发行）
  - [观察家报](../Page/观察家报.md "wikilink")

#### 介于大报版式和柏林版式之间的

  - [獨立報](../Page/獨立報.md "wikilink")
  - [泰晤士報](../Page/泰晤士報.md "wikilink")
  - [星期日独立报](../Page/星期日独立报.md "wikilink")
  - **[*i*](../Page/i_\(報章\).md "wikilink")**

### 中间市场报纸

  - [每日快报](../Page/每日快报.md "wikilink")
  - [每日郵報](../Page/每日郵報.md "wikilink")
  - [星期日快报](../Page/星期日快报.md "wikilink")
  - [星期日邮报](../Page/星期日邮报.md "wikilink")

### 通俗小报

  - [每日星报](../Page/每日星报.md "wikilink")
  - [太陽報](../Page/太陽報_\(英國\).md "wikilink")
  - [每日鏡報](../Page/每日鏡報.md "wikilink")
  - [每日体育报](../Page/每日体育报.md "wikilink")
  - [星晨报](../Page/星晨报.md "wikilink")
  - [周日星报](../Page/周日星报.md "wikilink")
  - [人物](../Page/人物.md "wikilink")
  - [周日镜报](../Page/周日镜报.md "wikilink")
  - [周日体育报](../Page/周日体育报.md "wikilink")
  - [世界新闻报](../Page/世界新闻报.md "wikilink")（已停刊）
  - [星期日太阳报](../Page/星期日太阳报.md "wikilink")

[英國報紙](../Category/英國報紙.md "wikilink")
[文](../Category/英國相關列表.md "wikilink")