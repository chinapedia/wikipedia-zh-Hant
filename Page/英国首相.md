**大不列颠及北爱尔兰联合王国首相**（）即英国首相，或简称“联合王国首相”、“英相”、“英揆”（[揆](../Page/总理.md "wikilink")，本意即[宰相](../Page/宰相.md "wikilink")），前稱大不列顛首相，代表[英国王室和](../Page/英国王室.md "wikilink")[英國公民执掌](../Page/英國公民.md "wikilink")[英國](../Page/英國.md "wikilink")[行政权力的最高官员](../Page/行政权力.md "wikilink")、[英国政府](../Page/英国政府.md "wikilink")[首脑](../Page/政府首脑.md "wikilink")。在一般情况下，[国会](../Page/英国国会.md "wikilink")[下议院的多数党党魁或者执政联盟的](../Page/英国下议院.md "wikilink")[领袖自动成为](../Page/领袖.md "wikilink")[首相人選](../Page/首相.md "wikilink")，人选经由[君主确认及任命后](../Page/英國君主.md "wikilink")，才正式成为首相。[英國歷史上](../Page/英國歷史.md "wikilink")，首相一般會兼任[第一财政大臣一職](../Page/第一财政大臣.md "wikilink")，伦敦[唐寧街10號即为第一财政大臣之宅邸](../Page/唐寧街10號.md "wikilink")。根据实际情况會設立[副首相](../Page/英國副首相.md "wikilink")，如[聯合政府時內閣中的少數黨黨魁](../Page/聯合政府.md "wikilink")；而當一個黨取得絕對多數議席時，則可能設[首席大臣作副職](../Page/首席大臣.md "wikilink")。

## 簡介

作為「[女王陛下的政府之首](../Page/英國政府.md "wikilink")」，現今的首相是在英國最高的政治權威，享有很大职权。首相身兼数职，既是议会多数党领袖同时是政府首脑，同时掌握[立法和](../Page/立法.md "wikilink")[行政權力](../Page/行政.md "wikilink")，且可向女王推荐最高法院大法官。在[下議院](../Page/英國下議院.md "wikilink")，首相會參與法律制定的過程，以達到所屬政黨的目標。在行政方面，首相負責統籌所有政府部門和[公務員隊伍](../Page/公務員.md "wikilink")。首相有權向[君主](../Page/英國君主.md "wikilink")（国-{王}-或女王）提名其他[內閣成員和](../Page/內閣.md "wikilink")[國務大臣](../Page/國務大臣.md "wikilink")，亦可提出[解散下议院的请求](../Page/解散国会.md "wikilink")。在首相和內閣的意見下，君主會行使其法定權力及特權，包括任命政府高級官員、司法人員和[英國國教會的相關人員](../Page/英國國教會.md "wikilink")，以及頒授勳銜等。

首相一職從未被「創造」出來，而是在三百年來慢慢演化成的。[光榮革命革命後](../Page/光榮革命.md "wikilink")，革命協定（Revolutionary
Settlement）修改憲法，政治權力由[君主轉移到](../Page/君主.md "wikilink")[議會](../Page/議會.md "wikilink")。1713年，下議院訂立了議事規則第66條（Standing
Order 66），規定「除了君主委託的大臣所作出的動議外，下議院不會對任何有關公帑的議案進行表決」。

1721年前，英國君主依賴替其執行政策。直至1721年，母語是[德文的](../Page/德文.md "wikilink")[乔治一世](../Page/喬治一世_\(大不列顛\).md "wikilink")[國王](../Page/英国国王.md "wikilink")[英文不夠流利](../Page/英文.md "wikilink")，不能如同之前君主一樣直接統治群臣，於是[敕命](../Page/敕.md "wikilink")[辉格党](../Page/英国辉格党.md "wikilink")[領袖](../Page/領袖.md "wikilink")[华波尔爵士為](../Page/罗伯特·沃波尔.md "wikilink")[内阁](../Page/内阁.md "wikilink")[首领](../Page/首领.md "wikilink")，是为「[第一财政大臣](../Page/第一财政大臣.md "wikilink")」，是首相這個職位的概念的前身。

1885年，中記錄了[威廉·格萊斯頓剛籌組完成的內閣](../Page/威廉·格萊斯頓.md "wikilink")，當中首相（Prime
Minister）一字首次正式出现，但只是在官方政府首腦職位──[第一財政大臣之後](../Page/第一財政大臣.md "wikilink")，並在括號裏。

1905年，在英國排名名單上終於有首相一職出現，於非皇室成員中排在[坎特伯雷大主教](../Page/坎特伯雷大主教.md "wikilink")、[约克大主教](../Page/约克大主教.md "wikilink")、以及[大法官之後](../Page/大法官_\(英國\).md "wikilink")。雖然這個職位被各方正式承認，但在[法律中並無首相一職](../Page/法律.md "wikilink")。

20世紀首相已經在君主、議會和內閣中，處於政治權力比較高的地位。在21世紀初，[上議院在法律制定過程中已被邊緣化](../Page/英國上議院.md "wikilink")，這也間接提高了首相的權力，亦令人們的關注該職位越來越像[總統制國家](../Page/總統制.md "wikilink")[總統一職](../Page/總統.md "wikilink")。但是，後來[英國國會制定了一些法案來限制首相的權力](../Page/英國國會.md "wikilink")。

首相年薪可分兩部份，一為[內閣年薪](../Page/內閣.md "wikilink")132,923[鎊](../Page/鎊.md "wikilink")，一為[國會](../Page/國會.md "wikilink")[議員年薪](../Page/議員.md "wikilink")64,766鎊，共計197,689[英鎊](../Page/英鎊.md "wikilink")。首相的官邸為[唐寧街10號](../Page/唐寧街10號.md "wikilink")，也擁有官方鄉間別墅[契克斯](../Page/契克斯.md "wikilink")。卸任後，大多數首相會被授予[勳爵和](../Page/勳爵.md "wikilink")[上議院議員的身份](../Page/英國上議院.md "wikilink")，直至2016年7月為止共有62人曾擔任此職，現任英國首相為[保守黨領袖](../Page/英國保守黨.md "wikilink")[德蕾莎·梅伊](../Page/德蕾莎·梅伊.md "wikilink")，於2016年7月13日上台。2016年7月11日，因唯一對手[利雅華退選](../Page/利雅華.md "wikilink")，自動成為保守黨領袖，加上[大衛·卡麥隆提早卸任](../Page/大衛·卡麥隆.md "wikilink")，令她成為新一任首相。

## 歷任首相列表

## 参考文献

## 外部链接

  - [英國首相府簡介](https://web.archive.org/web/20070703233432/http://www.number-10.gov.uk/output/Page1371.asp)


  -
## 参见

  - [英国君主](../Page/英国君主.md "wikilink")

  - [英国副首相](../Page/英国副首相.md "wikilink")

  - [英国首相列表](../Page/英国首相列表.md "wikilink")

  -
{{-}}

[英国首相](../Category/英国首相.md "wikilink")
[Category:各国政府首脑](../Category/各国政府首脑.md "wikilink")