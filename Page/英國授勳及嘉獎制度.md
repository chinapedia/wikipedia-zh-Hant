**大不列顛及北愛爾蘭聯合王國榮譽制度**是獎勵個體在[聯合王國本土及](../Page/聯合王國.md "wikilink")[海外領土上因其個人英勇行為](../Page/英國海外領土.md "wikilink")、傑出服務或特殊成就的手段。該系統包括榮譽、勳章和獎章三個部分。
**[榮譽](../Page/名譽.md "wikilink")**用於表彰其傑出服務或特殊成就； ****用於表彰其受公認的功績或行為；
**[獎章](../Page/獎章.md "wikilink")**用於表彰其受公認的特定行為或特殊軍事行為，以及長期或有價值的服務，并及良好品行。

發佈這些多種形式的[勳章及獎章的消息一般刊載在](../Page/勳章.md "wikilink")《[倫敦憲報](../Page/倫敦憲報.md "wikilink")》上。

## 制度簡史

眾所周知，雖然在[盎格魯-撒克遜時代的君王會以戒指或其他象征物獎勵他們的忠誠臣民](../Page/七国时代.md "wikilink")，但是直至[諾曼人時代](../Page/諾曼人.md "wikilink")，[騎士制度才成為](../Page/騎士.md "wikilink")[封建政府的一部分](../Page/封建制度_\(歐洲\).md "wikilink")。第一個英國[騎士團](../Page/騎士團.md "wikilink")，即[嘉德騎士團](../Page/嘉德勋章.md "wikilink")，由[愛德華三世在](../Page/爱德华三世_\(英格兰\).md "wikilink")1348年建立。從那時起，英國榮譽制度就在不斷發展以適應時下對表彰受公認服務或行為獎勵的需求。

## 現行榮譽

## 現存勳章

## 閒置勳章

## 與英國君主有關的閒置勳章

### 皇家圭爾夫勳章

<table style="width:145%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 50%" />
<col style="width: 12%" />
<col style="width: 10%" />
<col style="width: 23%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>全稱</p></th>
<th><p>等級（<a href="../Page/勳銜.md" title="wikilink">勳銜</a>）</p></th>
<th><p>綬帶</p></th>
<th><p>頒發時間</p></th>
<th><p>創建者</p></th>
<th><p>格言</p></th>
<th><p>受勳{{\}}受獎資格</p></th>
<th><p>附屬獎章</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/皇家圭爾夫勳章.md" title="wikilink">皇家圭爾夫勳章</a></p></td>
<td><p>大十字騎士（）<br />
司令騎士（）<br />
騎士（）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Royal_Guelphic_Order.png" title="fig:Royal_Guelphic_Order.png">Royal_Guelphic_Order.png</a></p></td>
<td><p>1815年－1841年（英國）<br />
1841年迄今（<a href="../Page/韦尔夫家族.md" title="wikilink">漢諾威王室</a>）</p></td>
<td><p>英國攝政王（<a href="../Page/喬治四世.md" title="wikilink">喬治四世</a>）</p></td>
<td><p>不懼艱險<br />
</p></td>
<td><p>君主意志</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 國家勳章

## 其他榮譽與嘉許

## 英聯邦王國的英國榮譽

## 榮譽獎勵

## 授予儀式

## 拒絕案例

## 吊銷案例

## 佩戴諭令

## 風格樣式

## 相關改革

## 參見

  -
  -
  - 其他榮譽制度

      -
      -
      - [香港榮譽制度](../Page/香港授勳及嘉獎制度.md "wikilink")

      -
  -
  -
  -
## 參考資料

### 腳註

### 文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連接

  - [英國榮譽制度介紹](https://www.gov.uk/honours)在[英國政府網站上](../Page/英國政府.md "wikilink")
  - [《倫敦憲報》官方網站](https://www.thegazette.co.uk/)
  - [埃德和拉芬斯克洛夫：騎士勳章](https://www.edeandravenscroft.com/ceremonial-dress/orders-of-chivalry/)——為各類騎士勳章獲得者訂製服飾的王室御用長袍製造商

[\*](../Category/英國授勳及嘉獎制度.md "wikilink")