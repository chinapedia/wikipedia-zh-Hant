**英國駐華大使**（）是[英國派駐](../Page/英國.md "wikilink")[中國](../Page/中國.md "wikilink")（[大清](../Page/大清.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")、[中華人民共和國](../Page/中華人民共和國.md "wikilink")）最高級別的外交使節，並主管英國設於中國大陸的使領館。目前，這個職位的全稱是**英女皇陛下駐中華人民共和國大使**（**Her
Britannic Majesty's Ambassador to the People's Republic of China**）。

英國駐華使節職稱曾為**英國駐華全權公使**或**英國駐華公使**，是[联合王國](../Page/联合王國.md "wikilink")（1801年-1927年为[大不列颠及爱尔兰联合王国](../Page/大不列颠及爱尔兰联合王国.md "wikilink")，1927年至今为[大不列颠及北爱尔兰联合王国](../Page/大不列颠及北爱尔兰联合王国.md "wikilink")）先后驻[大清国](../Page/大清国.md "wikilink")、[中华民国](../Page/中华民国.md "wikilink")、[中華人民共和國的最高級外交代表](../Page/中華人民共和國.md "wikilink")，主理英國在華事務。一般認為，歷史上第一和第二位英國駐華全權公使是[馬戛爾尼伯爵和](../Page/喬治·馬戛爾尼.md "wikilink")[亞美士德伯爵](../Page/亞美士德.md "wikilink")，兩人分別在1793年和1816年來華，但兩人都並不常駐[中國](../Page/中國.md "wikilink")。第一位常駐中國的駐華全權公使是[砵甸乍](../Page/砵甸乍.md "wikilink")[爵士](../Page/爵士.md "wikilink")，他當時以兼任[香港總督的身份駐守](../Page/香港總督.md "wikilink")[香港](../Page/香港.md "wikilink")，而一直至1857年，港督才不再兼任公使一職。

在1858年《[天津條約](../Page/天津條約.md "wikilink")》簽訂後，英國得以在北京設置[公使馆](../Page/英國駐華大使館.md "wikilink")，1935年升格为大使馆，[二次大戰时随](../Page/第二次中日戰爭.md "wikilink")[国民政府迁往](../Page/国民政府.md "wikilink")[重慶](../Page/重慶.md "wikilink")，1946年又随国民政府迁到[南京](../Page/南京.md "wikilink")。1950年，英國承認中華人民共和國政權，並與中華民國斷交，英方遂於北京設置臨時代辦處，兩國復於1954年正式建立代辦級關係。英國與中華人民共和國兩國後來復於1972年升為大使級外交關係，以迄於今。現今的英國駐華大使館位於北京[建國門外光華路](../Page/建国门_\(北京\).md "wikilink")11號；在[上海](../Page/上海.md "wikilink")、[廣州](../Page/廣州.md "wikilink")、[重慶和](../Page/重慶.md "wikilink")[武汉則置有總領事館](../Page/武汉.md "wikilink")。

## 歷任英國駐華大使

### 英國駐[大清國公使](../Page/大清國.md "wikilink") （1840年-1841年，[第一次鴉片戰爭時期](../Page/第一次鴉片戰爭.md "wikilink")）

<table style="width:169%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 80%" />
<col style="width: 10%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th><p>姓名（中文）</p></th>
<th><p>姓名（英文）</p></th>
<th><p>任命</p></th>
<th><p>到任</p></th>
<th><p>離任</p></th>
<th><p>外交衔级</p></th>
<th><p>外交职务</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>海軍少將<a href="../Page/懿律.md" title="wikilink">懿律</a></p></td>
<td><p>Rear Admiral George Elliot</p></td>
<td></td>
<td><p>1840年2月</p></td>
<td><p>1840年11月</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p><br />
因病去職</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/義律.md" title="wikilink">義律上校</a></p></td>
<td><p>Captain Charles Elliot</p></td>
<td></td>
<td><p>1840年11月</p></td>
<td><p>1841年8月12日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p><br />
拟定《<a href="../Page/穿鼻草约.md" title="wikilink">穿鼻草约</a>》<br />
被免職</p></td>
</tr>
</tbody>
</table>

### 英國駐[大清國公使](../Page/大清國.md "wikilink") （1841年-1857年，[港督兼任時期](../Page/香港總督.md "wikilink")）

<table style="width:169%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 80%" />
<col style="width: 10%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th><p>姓名（中文）</p></th>
<th><p>姓名（英文）</p></th>
<th><p>任命</p></th>
<th><p>到任</p></th>
<th><p>離任</p></th>
<th><p>外交衔级</p></th>
<th><p>外交职务</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/砵甸乍.md" title="wikilink">砵甸乍爵士</a></p></td>
<td><p>Sir Henry Pottinger</p></td>
<td></td>
<td><p>1841年8月12日</p></td>
<td><p>1844年5月8日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p>首位常駐駐華公使<br />
1842年任首位<a href="../Page/香港總督.md" title="wikilink">香港總督</a><br />
中方译为“璞鼎查”</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴維斯_(總督).md" title="wikilink">爹核士爵士</a></p></td>
<td><p>Sir John Francis Davis</p></td>
<td></td>
<td><p>1844年5月8日</p></td>
<td><p>1848年3月18日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p>中方译为“德庇时”</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/文咸.md" title="wikilink">文翰爵士</a></p></td>
<td><p>Sir Samuel George Bonham</p></td>
<td></td>
<td><p>1848年3月18日</p></td>
<td><p>1851年</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/寶靈.md" title="wikilink">-{zh-hans:包令; zh-hant:寶靈}-爵士</a></p></td>
<td><p>Sir John Bowring</p></td>
<td></td>
<td><p>1853年12月20日</p></td>
<td><p>1857年4月17日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p>1854年起兼任港督</p></td>
</tr>
</tbody>
</table>

### 英國駐[大清國公使](../Page/大清國.md "wikilink")（1857年－1911年）

<table style="width:181%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 80%" />
<col style="width: 10%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th><p>姓名（中文）</p></th>
<th><p>姓名（英文）</p></th>
<th><p>任命</p></th>
<th><p>到任</p></th>
<th><p>递交国书</p></th>
<th><p>離任</p></th>
<th><p>外交衔级</p></th>
<th><p>外交职务</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/詹姆斯·布鲁斯，第八代额尔金伯爵.md" title="wikilink">額爾金伯爵</a></p></td>
<td><p>Earl of Elgin</p></td>
<td><p>1857年</p></td>
<td></td>
<td></td>
<td><p>1860年</p></td>
<td></td>
<td></td>
<td><p>大使<br />
<a href="../Page/英法聯軍.md" title="wikilink">英法聯軍總司令</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卜魯斯.md" title="wikilink">卜魯斯爵士</a></p></td>
<td><p>Sir Frederick Bruce</p></td>
<td><p>1858年12月2日[1]</p></td>
<td><p>1860年11月7日</p></td>
<td></td>
<td><p>1864年6月19日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p><a href="../Page/詹姆斯·布鲁斯，第八代额尔金伯爵.md" title="wikilink">額爾金伯爵之弟</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/威妥瑪.md" title="wikilink">威妥瑪爵士</a></p></td>
<td><p>Sir Thomas Francis Wade</p></td>
<td><p>1864年6月19日</p></td>
<td></td>
<td></td>
<td><p>1865年12月7日</p></td>
<td><p><a href="../Page/參贊.md" title="wikilink">參贊</a></p></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿禮國.md" title="wikilink">阿禮國爵士</a></p></td>
<td><p>Sir Rutherford Alcock</p></td>
<td></td>
<td></td>
<td><p>1865年12月7日</p></td>
<td><p>1869年11月1日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p>1869年达成<a href="../Page/阿禮國协定.md" title="wikilink">阿禮國协定</a>，未获英国政府批准</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傅磊斯.md" title="wikilink">傅磊斯</a></p></td>
<td><p>Hugh Fraser</p></td>
<td><p>1869年11月1日</p></td>
<td></td>
<td></td>
<td><p>1869年11月28日</p></td>
<td><p><a href="../Page/副參贊.md" title="wikilink">副參贊</a></p></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/威妥瑪.md" title="wikilink">威妥瑪爵士</a></p></td>
<td><p>Sir Thomas Francis Wade</p></td>
<td><p>1869年11月28日</p></td>
<td></td>
<td></td>
<td><p>1871年8月1日</p></td>
<td><p><a href="../Page/参赞.md" title="wikilink">参赞</a></p></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td><p>升任公使</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/威妥瑪.md" title="wikilink">威妥瑪爵士</a></p></td>
<td><p>Sir Thomas Francis Wade</p></td>
<td></td>
<td></td>
<td><p>1871年8月1日</p></td>
<td><p>1882年8月14日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p>1873年觐见<a href="../Page/同治帝.md" title="wikilink">同治帝</a><br />
1875年至1876年处理<a href="../Page/马嘉理案.md" title="wikilink">马嘉理案</a>，1876年签订《<a href="../Page/烟台条约.md" title="wikilink">烟台条约</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/傅磊斯.md" title="wikilink">傅磊斯</a></p></td>
<td><p>Hugh Fraser</p></td>
<td><p>1876年11月6日</p></td>
<td></td>
<td></td>
<td><p>1879年6月29日</p></td>
<td><p><a href="../Page/參贊.md" title="wikilink">參贊</a></p></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格維納.md" title="wikilink">格維納</a></p></td>
<td><p>Thomas　George Grosvenor</p></td>
<td><p>1882年8月14日</p></td>
<td></td>
<td></td>
<td><p>1883年9月17日</p></td>
<td><p><a href="../Page/參贊.md" title="wikilink">參贊</a></p></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴夏禮.md" title="wikilink">巴夏禮爵士</a></p></td>
<td><p>Sir Harry Smith Parkes</p></td>
<td></td>
<td></td>
<td><p>1883年9月28日</p></td>
<td><p>1885年3月22日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p>任内去世</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歐格訥.md" title="wikilink">歐格訥爵士</a></p></td>
<td><p>Sir Nicholas Roderick O'Conor</p></td>
<td><p>1885年3月22日</p></td>
<td></td>
<td></td>
<td><p>1886年6月15日</p></td>
<td><p><a href="../Page/參贊.md" title="wikilink">參贊</a></p></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/華爾身.md" title="wikilink">華爾身爵士</a></p></td>
<td><p>Sir John Walsham</p></td>
<td></td>
<td></td>
<td><p>1886年6月15日</p></td>
<td><p>1892年9月28日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寶克樂.md" title="wikilink">寶克樂</a></p></td>
<td><p>William Nelthorpe Beauclerk</p></td>
<td><p>1892年9月28日</p></td>
<td></td>
<td></td>
<td><p>1892年11月19日</p></td>
<td></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐格訥.md" title="wikilink">歐格訥爵士</a></p></td>
<td><p>Sir Nicholas Roderick O'Conor</p></td>
<td></td>
<td></td>
<td><p>1892年11月19日</p></td>
<td><p>1895年9月</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寶克樂.md" title="wikilink">寶克樂</a></p></td>
<td><p>William Nelthorpe Beauclerk</p></td>
<td><p>1895年9月</p></td>
<td></td>
<td></td>
<td><p>1896年4月24日</p></td>
<td></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/竇納樂.md" title="wikilink">竇納樂爵士</a></p></td>
<td><p>Sir Claude Maxwell MacDonald</p></td>
<td></td>
<td></td>
<td><p>1896年4月24日</p></td>
<td><p>1900年10月25日</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/薩道義.md" title="wikilink">薩道義爵士</a></p></td>
<td><p>Sir Ernest Mason Satow</p></td>
<td></td>
<td></td>
<td><p>1900年10月25日</p></td>
<td><p>1906年</p></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p>代表英方簽訂<br />
《<a href="../Page/辛丑條約.md" title="wikilink">辛丑條約</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/燾訥里.md" title="wikilink">燾訥里</a></p></td>
<td><p>R. G. Townley</p></td>
<td><p>1902年12月3日</p></td>
<td></td>
<td></td>
<td><p>1903年8月21日</p></td>
<td><p><a href="../Page/參贊.md" title="wikilink">參贊</a></p></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱邇典.md" title="wikilink">朱邇典爵士</a></p></td>
<td><p>Sir John Newell Jordan</p></td>
<td></td>
<td></td>
<td><p>1906年9月19日</p></td>
<td></td>
<td><p><a href="../Page/公使.md" title="wikilink">公使</a></p></td>
<td><p><a href="../Page/特命全权公使.md" title="wikilink">特命全权公使</a></p></td>
<td><p>续任驻中华民国公使</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麻穆勒.md" title="wikilink">麻穆勒</a></p></td>
<td><p>William Grenfell Max-Muller</p></td>
<td><p>1910年3月12日</p></td>
<td></td>
<td></td>
<td><p>1910年11月28日</p></td>
<td><p><a href="../Page/參贊.md" title="wikilink">參贊</a></p></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td></td>
</tr>
</tbody>
</table>

<references group="註" />

### 英國駐[中華民國公使](../Page/中華民國.md "wikilink")（1912年-1935年）

| 姓名（中文）                             | 姓名（英文）                               | 任命        | 到任             | 递交国书                | 離任              | 外交衔级                           | 外交职务                                   | 備註                                                                                                                               |
| ---------------------------------- | ------------------------------------ | --------- | -------------- | ------------------- | --------------- | ------------------------------ | -------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| [朱邇典爵士](../Page/朱邇典.md "wikilink") | Sir John Newell Jordan               |           |                | 1913年12月4日\[2\]     | 1920年3月1日       | [公使](../Page/公使.md "wikilink") | [特命全权公使](../Page/特命全权公使.md "wikilink") | 重新遞交國書                                                                                                                           |
| [艾斯頓爵士](../Page/艾斯頓.md "wikilink") | Sir Beilby Francis Alston            | 1920年3月1日 | 1920年4月15日     | 1920年4月21日\[3\]     | 1922年           | [公使](../Page/公使.md "wikilink") | [特命全权公使](../Page/特命全权公使.md "wikilink") |                                                                                                                                  |
| [克來佛](../Page/克來佛.md "wikilink")   | Robert Henry Clive                   | 1922年     |                |                     | 1922年           |                                | [临时代办](../Page/临时代办.md "wikilink")     |                                                                                                                                  |
| [麻克類爵士](../Page/麻克類.md "wikilink") | Sir James Ronald Macleay             | 1922年     | 1923年2月15日     | 1923年2月21日\[4\]     | 1926年12月20日     | [公使](../Page/公使.md "wikilink") | [特命全权公使](../Page/特命全权公使.md "wikilink") |                                                                                                                                  |
| [藍浦生爵士](../Page/藍浦生.md "wikilink") | Sir Miles Wedderburn Lampson         |           |                | 1926年12月20日         | 1933年12月6日\[5\] | [公使](../Page/公使.md "wikilink") | [特命全权公使](../Page/特命全权公使.md "wikilink") | 1927年[大不列颠及爱尔兰联合王国改为](../Page/大不列颠及爱尔兰联合王国.md "wikilink")[大不列颠及北爱尔兰联合王国](../Page/大不列颠及北爱尔兰联合王国.md "wikilink")，1933年9月3日調任埃及\[6\] |
| [應哥蘭](../Page/應哥蘭.md "wikilink")   | E. M. B. Ingram                      | 1932年5月5日 |                |                     | 1934年3月4日       | [参赞](../Page/参赞.md "wikilink") | [临时代办](../Page/临时代办.md "wikilink")     |                                                                                                                                  |
| [賈德幹爵士](../Page/賈德幹.md "wikilink") | Sir Alexander George Montagu Cadogan | 1933年9月3日 | 1934年3月5日\[7\] | 1934年3月6日\[8\]\[9\] | 1935年5月19日      | [公使](../Page/公使.md "wikilink") | [特命全权公使](../Page/特命全权公使.md "wikilink") | 又譯卡多根                                                                                                                            |

### 英國駐[中華民國大使](../Page/中華民國.md "wikilink")（1935年-1950年）

| 姓名（中文）                                    | 姓名（英文）                                   | 任命                | 到任               | 递交国书             | 離任               | 外交衔级                           | 外交职务                                   | 備註                                    |
| ----------------------------------------- | ---------------------------------------- | ----------------- | ---------------- | ---------------- | ---------------- | ------------------------------ | -------------------------------------- | ------------------------------------- |
| [賈德幹爵士](../Page/賈德幹.md "wikilink")        | Sir Alexander George Montagu Cadogan     |                   |                  | 1935年6月15日\[10\] | 1936年4月5日        | [大使](../Page/大使.md "wikilink") | [特命全权大使](../Page/特命全权大使.md "wikilink") | 首任驻华全權大使                              |
| [賀武](../Page/賀武.md "wikilink")            | Robert George Howe                       | 1936年4月5日         |                  |                  | 1936年9月2日        | [参事](../Page/参事.md "wikilink") | [临时代办](../Page/临时代办.md "wikilink")     |                                       |
| [許閣森爵士](../Page/許閣森.md "wikilink")        | Sir Hughe Montgomery Knatchbull-Hugessen | 1936年2月20日\[11\]  |                  | 1936年9月23日\[12\] | 1937年10月3日\[13\] | [大使](../Page/大使.md "wikilink") | [特命全权大使](../Page/特命全权大使.md "wikilink") | 1937年12月20日免职\[14\]                   |
| [卡爾爵士](../Page/卡爾_\(駐華大使\).md "wikilink") | Sir Archibald Clark Kerr                 | 1937年12月20日\[15\] |                  | 1938年4月12日\[16\] | 1942年2月4日\[17\]  | [大使](../Page/大使.md "wikilink") | [特命全权大使](../Page/特命全权大使.md "wikilink") | 初駐[上海](../Page/上海.md "wikilink")，后迁重庆 |
| [薛穆爵士](../Page/薛穆.md "wikilink")          | Sir Horace James Seymour                 | 1942年1月16日\[18\]  | 1942年2月25日\[19\] | 1942年3月7日\[20\]  | 1946年            | [大使](../Page/大使.md "wikilink") | [特命全权大使](../Page/特命全权大使.md "wikilink") |                                       |
| [施諦文爵士](../Page/施諦文.md "wikilink")        | Sir Ralph Clarmont Skrine Stevenson      | 1946年5月23日\[21\]  | 1946年8月1日\[22\]  | 1946年8月7日\[23\]  | 1950年1月          | [大使](../Page/大使.md "wikilink") | [特命全权大使](../Page/特命全权大使.md "wikilink") |                                       |
| [藍來訥爵士](../Page/藍來訥.md "wikilink")        | Sir Lionel Henry Lamb                    | 1948年             |                  |                  | 1948年            | [公使](../Page/公使.md "wikilink") | [临时代办](../Page/临时代办.md "wikilink")     |                                       |

### 附：英國駐[中華人民共和國代辦](../Page/中華人民共和國.md "wikilink")（1950年－1972年）

1950年1月6日，[英国外交大臣](../Page/英国外交大臣.md "wikilink")[贝文致电](../Page/欧内斯特·贝文.md "wikilink")[中华人民共和国](../Page/中华人民共和国.md "wikilink")[外交部部长](../Page/中央人民政府外交部.md "wikilink")[周恩来](../Page/周恩来.md "wikilink")，承认[中華人民共和國中央人民政府为](../Page/中華人民共和國中央人民政府.md "wikilink")“中国法律上之政府”，愿意同中国建立[外交关系](../Page/外交关系.md "wikilink")，在未任命大使之前，派[胡階森为临时](../Page/胡階森.md "wikilink")[代办](../Page/代办.md "wikilink")。而中華人民共和國政府僅承認胡為“大不列颠及北爱尔兰联合王国政府派来北京就贵我两国建立外交关系问题进行谈判的代表”\[24\]。1954年6月17日，中英双方同时发表公报：“中华人民共和国中央人民政府和联合王国协议，中央人民政府派遣代办驻在伦敦，其地位和任务与英国驻北京代办的地位和任务相同。”\[25\]

<table style="width:181%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 80%" />
<col style="width: 10%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th><p>姓名（中文）</p></th>
<th><p>姓名（英文）</p></th>
<th><p>任命</p></th>
<th><p>到任</p></th>
<th><p>递交委任书</p></th>
<th><p>離任</p></th>
<th><p>外交衔级</p></th>
<th><p>外交职务</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/胡階森.md" title="wikilink">胡階森爵士</a></p></td>
<td><p>Sir John Colville Hutchison</p></td>
<td><p>1950年1月6日[26]</p></td>
<td><p>1950年2月13日[27]</p></td>
<td></td>
<td><p>1951年3月5日[28]</p></td>
<td></td>
<td><p>臨時代辦<br />
（中方稱作<a href="../Page/外交代表.md" title="wikilink">外交代表</a>）</p></td>
<td><p>仅限于谈判建交事宜。详见题下说明。<br />
又译胡阶生</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/藍來訥.md" title="wikilink">藍來訥爵士</a></p></td>
<td><p>Sir Lionel Henry Lamb</p></td>
<td></td>
<td><p>1951年3月6日[29]</p></td>
<td></td>
<td><p>1953年6月19日[30]</p></td>
<td></td>
<td><p><a href="../Page/臨時代辦.md" title="wikilink">臨時代辦</a><br />
（中方稱作<a href="../Page/外交代表.md" title="wikilink">外交代表</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/杜维廉.md" title="wikilink">杜维廉爵士</a></p></td>
<td><p>Sir Humphrey Trevelyan</p></td>
<td></td>
<td><p>1953年8月23日[31]</p></td>
<td></td>
<td><p>1954年6月</p></td>
<td></td>
<td><p>臨時代辦<br />
（中方稱作<a href="../Page/外交代表.md" title="wikilink">外交代表</a>）</p></td>
<td><p>1954年兩國相互建立代辦處后首任駐華臨時代辦<br />
曾參加<a href="../Page/日内瓦会议_(1954年).md" title="wikilink">日内瓦會議</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杜维廉.md" title="wikilink">杜维廉爵士</a></p></td>
<td><p>Sir Humphrey Trevelyan</p></td>
<td></td>
<td><p>1954年6月28日[32]</p></td>
<td><p>1954年7月8日[33]</p></td>
<td><p>1955年5月28日[34]</p></td>
<td></td>
<td><p><a href="../Page/代办.md" title="wikilink">代办</a></p></td>
<td><p>處理<a href="../Page/克什米尔公主号.md" title="wikilink">克什米尔公主号事件</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/欧念儒.md" title="wikilink">欧念儒爵士</a></p></td>
<td><p>Sir Con O'Neill</p></td>
<td></td>
<td><p>1955年7月11日[35]</p></td>
<td><p>1955年7月13日[36]</p></td>
<td><p>1957年6月26日[37]</p></td>
<td></td>
<td><p><a href="../Page/代办.md" title="wikilink">代办</a></p></td>
<td><p>處理<a href="../Page/克什米尔公主号.md" title="wikilink">克什米尔公主号事件</a>[38][39]<br />
處理<a href="../Page/雙十暴動.md" title="wikilink">雙十暴動帶來的外交糾紛</a>[40]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Sir Archibald Duncan Wilson</p></td>
<td></td>
<td><p>1957年9月3日[41]</p></td>
<td><p>1957年9月9日[42]</p></td>
<td><p>1959年7月23日[43]</p></td>
<td></td>
<td><p><a href="../Page/代办.md" title="wikilink">代办</a></p></td>
<td><p>应对<a href="../Page/第二次台海危机.md" title="wikilink">第二次台海危机</a>[44] 文郁生离京后，施棣华来京前由Mr. A. C. Maby署理。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Sir Michael Stewart</p></td>
<td></td>
<td><p>1959年9月14日[45]</p></td>
<td><p>1959年9月16日[46][47]</p></td>
<td><p>1962年4月11日[48]</p></td>
<td></td>
<td><p><a href="../Page/代办.md" title="wikilink">代办</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Sir Terence Garvey</p></td>
<td></td>
<td><p>1962年7月3日[49]</p></td>
<td><p>1962年7月9日[50]</p></td>
<td><p>1965年4月16日[51]</p></td>
<td></td>
<td><p><a href="../Page/代办.md" title="wikilink">代办</a></p></td>
<td><p>处理<a href="../Page/史本基事件.md" title="wikilink">史本基事件</a>[52]<br />
解决<a href="../Page/广东省.md" title="wikilink">广东省向</a><a href="../Page/香港.md" title="wikilink">香港供水问题</a>[53]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Sir Donald Hopson</p></td>
<td></td>
<td><p>1965年5月8日[54]</p></td>
<td><p>1965年5月14日[55]</p></td>
<td><p>1968年8月</p></td>
<td></td>
<td><p><a href="../Page/代办.md" title="wikilink">代办</a></p></td>
<td><p>亲历“<a href="../Page/火烧英国代办处事件.md" title="wikilink">火烧英国代办处事件</a>”</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柯利達.md" title="wikilink">柯利達爵士</a></p></td>
<td><p>Sir Percy Cradock</p></td>
<td><p>1968年8月</p></td>
<td></td>
<td></td>
<td><p>1969年2月[56][57]</p></td>
<td></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td><p>“<a href="../Page/火烧英国代办处事件.md" title="wikilink">火烧英国代办处事件</a>”善后</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>John Denson</p></td>
<td><p>1969年2月</p></td>
<td></td>
<td></td>
<td><p>1971年1月[58]</p></td>
<td></td>
<td><p><a href="../Page/临时代办.md" title="wikilink">临时代办</a></p></td>
<td><p>1970年5月1日在<a href="../Page/天安门.md" title="wikilink">天安门受到</a><a href="../Page/毛泽东.md" title="wikilink">毛泽东接见</a>。[59][60]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>John Denson</p></td>
<td><p>1971年1月[61]</p></td>
<td></td>
<td><p>1971年1月14日[62]</p></td>
<td><p>1971年11月18日[63]</p></td>
<td></td>
<td><p><a href="../Page/代办.md" title="wikilink">代办</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/艾惕思.md" title="wikilink">艾惕思爵士</a></p></td>
<td><p>Sir John Addis</p></td>
<td></td>
<td><p>1972年1月24日[64]</p></td>
<td><p>1972年1月26日[65]</p></td>
<td><p>1972年3月13日[66]</p></td>
<td></td>
<td><p><a href="../Page/代办.md" title="wikilink">代办</a></p></td>
<td><p>升任大使[67]</p></td>
</tr>
</tbody>
</table>

### 英國駐[中華人民共和國大使](../Page/中華人民共和國.md "wikilink")（1972年至今）

<table style="width:181%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 80%" />
<col style="width: 10%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th><p>姓名（中文）</p></th>
<th><p>姓名（英文）</p></th>
<th><p>任命</p></th>
<th><p>到任</p></th>
<th><p>递交国书</p></th>
<th><p>離任</p></th>
<th><p>外交衔级</p></th>
<th><p>外交职务</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/艾惕思.md" title="wikilink">艾惕思爵士</a></p></td>
<td><p>Sir John Addis</p></td>
<td><p>1972年3月13日</p></td>
<td></td>
<td><p>1972年3月29日[68]</p></td>
<td><p>1974年6月17日[69]</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td><p>1972年3月13日两國建立大使级外交关系</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尤德.md" title="wikilink">尤德爵士</a></p></td>
<td><p>Sir Edward Youde</p></td>
<td></td>
<td><p>1974年8月21日[70]</p></td>
<td><p>1974年8月29日[71]</p></td>
<td><p>1978年4月2日[72]</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td><p>1982年-1986年任<a href="../Page/香港總督.md" title="wikilink">香港總督</a><br />
中方初译为“爱德华·游德”</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/柯利達.md" title="wikilink">柯利達爵士</a></p></td>
<td><p>Sir Percy Cradock</p></td>
<td></td>
<td></td>
<td><p>1978年6月15日[73]</p></td>
<td><p>1983年12月[74]</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伊文思_(英國外交官).md" title="wikilink">伊文思爵士</a></p></td>
<td><p>Sir Richard Evans</p></td>
<td></td>
<td><p>1984年1月19日[75]</p></td>
<td><p>1984年1月23日[76]</p></td>
<td><p>1988年3月[77]</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/唐納德_(英國駐華大使).md" title="wikilink">唐納德爵士</a></p></td>
<td><p>Sir Alan Donald</p></td>
<td></td>
<td><p>1988年5月14日[78]</p></td>
<td><p>1988年5月26日[79]</p></td>
<td><p>1991年4月[80]</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥若彬.md" title="wikilink">麥若彬爵士</a></p></td>
<td><p>Sir Robin McLaren</p></td>
<td></td>
<td><p>1991年6月5日[81]</p></td>
<td><p>1991年6月20日[82]</p></td>
<td><p>1994年7月[83]</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/艾博雅.md" title="wikilink">艾博雅爵士</a></p></td>
<td><p>Sir Leonard Appleyard</p></td>
<td></td>
<td></td>
<td><p>1994年9月24日[84]</p></td>
<td><p>1997年</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高德年.md" title="wikilink">高德年爵士</a></p></td>
<td><p>Sir Anthony Galsworthy</p></td>
<td></td>
<td><p>1997年12月16日[85]</p></td>
<td><p>1997年12月29日[86]</p></td>
<td><p>2002年</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/韓魁發.md" title="wikilink">韓魁發爵士</a></p></td>
<td><p>Sir Christopher Hum</p></td>
<td></td>
<td><p>2002年3月19日[87][88]</p></td>
<td><p>2002年4月4日[89]</p></td>
<td><p>2006年</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐威廉.md" title="wikilink">歐威廉爵士</a></p></td>
<td><p>Sir William Ehrman</p></td>
<td></td>
<td></td>
<td><p>2006年3月15日[90]</p></td>
<td><p>2010年1月</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吳思田.md" title="wikilink">吳思田爵士</a></p></td>
<td><p>Sir Sebastian Wood</p></td>
<td></td>
<td></td>
<td><p>2010年3月3日[91]</p></td>
<td><p>2015年1月[92]</p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吴百纳.md" title="wikilink">吴百纳女爵士</a></p></td>
<td><p>Dame Barbara Woodward</p></td>
<td></td>
<td><p>2015年2月20日[93]</p></td>
<td><p>2015年4月15日[94]</p></td>
<td><p><strong>現任</strong></p></td>
<td><p><a href="../Page/大使.md" title="wikilink">大使</a></p></td>
<td><p><a href="../Page/特命全权大使.md" title="wikilink">特命全权大使</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 相關條目

  - [中英關係](../Page/中英關係.md "wikilink")
  - [英国驻华大使馆](../Page/英国驻华大使馆.md "wikilink")
  - [中国驻英国大使列表](../Page/中国驻英国大使列表.md "wikilink")
  - [英国驻香港及澳门总领事](../Page/英国驻香港及澳门总领事.md "wikilink")
  - [英國駐華商務總監](../Page/英國駐華商務總監.md "wikilink")
  - [火燒英國代辦處事件](../Page/火燒英國代辦處事件.md "wikilink")
  - [中華民國與英國關係](../Page/中華民國與英國關係.md "wikilink")
  - [英國在台辦事處代表](../Page/英國在台辦事處.md "wikilink")

<!-- end list -->

  - [撫華道](../Page/撫華道.md "wikilink")
  - [漢務參贊](../Page/漢務參贊.md "wikilink")

## 註釋

## 參考資料

  - [各國駐華使節表（按國別區分）](https://web.archive.org/web/20111115225556/http://archwebs.mh.sinica.edu.tw/digital/data/PDF/8-1-1-7.pdf)，中央研究院近代史研究所，黃文德提供。（PDF格式）

  -
  -
  -
  - [The Diaries of Sir Ernest Satow, British Envoy in Peking
    (1900-06)](http://www.dhs.kyutech.ac.jp/~ruxton/Satow_Peking_Diary_Volume_One_PREVIEW.pdf),
    （PDF格式）

<!-- end list -->

  -
## 外部連結

  - [英國駐華大使館](https://www.gov.uk/government/world/organisations/british-embassy-beijing.zh)

[英國駐華大使](../Category/英國駐華大使.md "wikilink")
[China](../Category/英国驻外大使列表.md "wikilink")
[Y](../Category/驻华大使列表.md "wikilink")

1.  George Clement Boase, "[Bruce, Frederick William
    Adolphus](http://en.wikisource.org/wiki/Bruce,_Frederick_William_Adolphus_%28DNB00%29)",
    Dictionary of National Biography, 1885-1900, Volume 07

2.  [民國重要史事檢索系統第10808條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=10808).中華民國國史館

3.  [民國重要史事檢索系統第18407條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=18407).中華民國國史館

4.  [民國重要史事檢索系統第22069條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=22069).中華民國國史館

5.  [民國重要史事檢索系統第41048條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=41048).中華民國國史館

6.  [民國重要史事檢索系統第40689條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=40689).中華民國國史館

7.  《民國二十四年中國外交年鑑》（上編），世界書局，第204頁。

8.
9.  [民國重要史事檢索系統第41516條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=41516).中華民國國史館

10. [民國重要史事檢索系統第43626條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=43626).中華民國國史館

11. [民國重要史事檢索系統第44974條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=44974).中華民國國史館

12. [民國重要史事檢索系統第45996條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=45996).中華民國國史館

13. 英大使许阁森今午离沪赴菲.《申报 临时夕刊》1937年10月3日第1版

14.
15. [民國重要史事檢索系統第47766條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=47766).中華民國國史館

16. [民國重要史事檢索系統第48130條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=48130).中華民國國史館

17. 英大使寇尔离渝赴苏 馆务由艾伦代.《申报》1942年2月5日第2版

18. [民國重要史事檢索系統第52179條](http://web.drnh.gov.tw/scripts/newsnote/tornado/searcher.exe?s=1&z=1&k=&m=0&p=&v=root&b=52179).中華民國國史館

19. 英驻华新大使由印抵渝.《申报》1942年2月27日第2版

20. 英大使由薛穆呈递国书.《申报》1942年3月8日第2版

21. 史蒂文森继任英驻华大使.《申报》1946年5月25日第1版

22. 英新大使昨抵京.《申报》1946年8月2日第1版

23. 英大使呈递国书.《申报》1946年8月8日第1版

24. 英国及锡兰政府愿与我建外交关系 我分别复电同意进行谈判.《人民日报》1950年1月10日第1版

25. [张素林，新中国的半外交关系，載
    世界知識2005年第1期](http://dag.fmprc.gov.cn/chn/dajmkf/dawz/t308980.htm)


26. 英国及锡兰政府愿与我建外交关系 我分别复电同意进行谈判.《人民日报》1950年21月10日第1版

27. 与我谈判建立外交关系 英印代表昨晨抵京.《人民日报》1950年2月14日第1版

28.
29. 英政府与我谈判代表易人 新派谈判代表蓝来讷抵京.《人民日报》1951年3月7日第4版

30.
31. 英国新派遣谈判代表到北京.《人民日报》1953年8月25日第1版

32. 联合王国政府驻我国代办 杜维廉先生到北京.《人民日报》1954年6月29日第1版

33. 联合王国代办杜维廉 向周外长呈委任书.《人民日报》1954年7月9日第1版

34.
35. 英新任驻华代办欧念儒到京.《人民日报》1955年7月12日第1版

36. 英国驻华代办欧念儒向周外长递交委任书.《人民日报》1955年7月14日第1版

37. 英国驻华代办欧念儒奉调返国.《人民日报》1957年6月27日第6版

38. [外交档案解密鲜为人知历史，載
    [北京日報](../Page/北京日報.md "wikilink")2004年2月5日](http://daj.tyjcp.gov.cn/DetailServlet?siteID=29&infoID=126)

39. [“克什米尔公主号”坠机事件，載
    [新聞午報](../Page/新聞午報.md "wikilink")2006年5月27日](http://web.xwwb.com/wbnews.php?db=11&thisid=52613)

40. [陈新文，50年代两岸在港关系的历史考察，载
    台湾研究集刊1999年第3期](http://www.usc.cuhk.edu.hk/wk_wzdetails.asp?id=2686)

41. 英代办文郁生到京.《人民日报》1957年9月4日第1版

42. 英代办文郁生拜会周外长.《人民日报》1957年9月10日第1版

43. 英驻华代办文郁生返国.《人民日报》1959年7月24日第4版

44. [戴超武，台湾海峡危机、中美关系与亚洲的冷战](http://www.daichaowu.net/ztlw/000015_6.htm)

45. 施棣华抵京.《人民日报》1959年9月15日第5版

46. 施棣华任英国驻华代办.《人民日报》1959年5月30日第5版

47. 英新任驻华代办施棣华向陈毅外长递交委任书.《人民日报》1959年9月17日第5版

48. 英国驻华代办离任回国.《人民日报》1962年4月11日第3版

49. 英国新任驻中国代办贾维到京.《人民日报》1962年7月4日第2版

50. 英国新任驻华代办悌·维·贾维 向陈毅外长递交委任书.《人民日报》1962年7月10日第2版

51. 英国驻华代办离任回国.《人民日报》1965年4月17日第2版

52. [上海外事志·下编·第三篇外国驻沪领事机构和办理侨务人员·第二章各国驻沪领馆和办理侨务人员·第七节　英代办处驻上海办理侨务人员和英国驻上海总领事馆](http://www.shtong.gov.cn/newsite/node2/node2245/node69969/node69985/node70080/node70202/userobject1ai69915.html)

53. [1964年中共宝安地方史大事记](http://www.da.baoan.gov.cn/basz/dsdsj_info.aspx?id=42)


54. 英国新任驻华代办到京.《人民日报》1965年5月9日第4版

55. 英国新任驻华代办 向罗贵波副部长递交介绍书.《人民日报》1965年5月14日第4版

56. [British Diplomatic Oral History
    Programme](http://janus.lib.cam.ac.uk/db/node.xsp?id=EAD%2FGBR%2F0014%2FDOHP%2026)

57. Percy Craddock, *Experiences of China*, Trafalgar Square Publishing,
    1999

58.
59. [中英建交鲜为人知的内幕，新闻午报2006年7月2日，摘自张树德，红墙大事，北京：中央文献出版社，2006年6月](http://web.xwwb.com/wbnews.php?db=12&thisid=57624)


60. [章文晋挨批，改编自吴建民，交流学十四讲，杭州：浙江人民出版社，2004年](http://www.rwabc.com/diqurenwu/diqudanyirenwu.asp?people_id=7930&id=16024&p_name=%E7%AB%A0%E6%96%87%E6%99%8B)

61.
62. 乔冠华副部长会见英国新任驻华代办.《人民日报》1971年1月15日第5版

63. 英国驻华代办谭森离任回国.《人民日报》1971年11月19日第5版

64. 英国新任驻华代办到京.《人民日报》1972年1月25日第6版

65. 英国新任驻华代办向姬鹏飞部长递交委任书.《人民日报》1972年1月27日第5版

66. 中华人民共和国和大不列颠及北爱尔兰联合王国关于互换大使的联合公报.《人民日报》1972年3月14日第1版

67. [中英建交始末：台湾问题纠缠近一年，中评社香港2007年5月16日电](http://gb.chinareviewnews.com/doc/1003/6/8/7/100368744.html?coluid=7&kindid=0&docid=100368744)

68. 英国首任驻华大使向董必武代主席递交国书.《人民日报》1972年3月30日第4版

69. 英国驻华大使艾惕思离任回国.《人民日报》1974年6月18日第4版

70. 姬鹏飞会见英国新任驻华大使爱德华·游德.《人民日报》1974年8月25日第4版

71. 英国新任驻华大使爱德华·游德向朱德委员长递交国书.《人民日报》1974年8月30日第4版

72. 英国驻华大使游德离任回国.《人民日报》1978年4月3日第4版

73. 英国新任驻中国大使珀西·柯利达向乌兰夫副委员长递交国书.《人民日报》1978年6月16日第4版

74. 吴学谦会见将离任的英国、民主也门驻华大使.《人民日报》1983年12月16日第4版

75.
76. 英国新任驻华大使向李先念主席递交国书.《人民日报》1984年1月24日第4版

77. 简讯：“李鹏代总理3月3日晚在钓鱼台国宾馆会见了即将离任的英国驻华大使伊文思爵士。”.《人民日报》1988年3月4日第4版

78.
79. 杨主席接受英国新任驻华大使递交的国书.《人民日报》1988年5月27日第4版

80. 李鹏会见英国冰岛驻华大使.《人民日报》1991年4月17日第1版

81.
82. 英新任大使向杨主席递交国书.《人民日报》1991年6月21日第4版

83. 李鹏会见四国离到任大使.《人民日报》1994年7月21日第4版

84. 江泽民接受5国大使国书.《人民日报》1994年9月25日第4版

85.
86. [江主席接受五国大使国书](http://people.com.cn/item/ldhd/Jiangzm/1998/qita/qt0017.html).《人民日报》1997年12月30日第4版

87. [网站特稿：与新任英国驻华大使韩魁发聊天](http://www.cctv.com/news/world/20020307/16.html)

88.
89. 江泽民接受四国大使递交的国书.《人民日报》2002年4月5日第4版

90. [胡锦涛接受8国新任驻华大使递交的国书](http://politics.people.com.cn/GB/1024/4204483.html).《人民日报》2006年3月16日第4版

91. [胡锦涛接受巴巴多斯等五国新任驻华大使递交国书](http://www.gov.cn/ldhd/2010-03/03/content_1546800.htm)

92. [外交部副部长张明会见英国驻华大使吴思田](https://www.mfa.gov.cn/chn//pds/wjb/zygy/t1222027.htm)

93. [外交部礼宾司司长秦刚接受英国新任驻华大使递交国书副本](https://www.fmprc.gov.cn/web/wjb_673085/zzjg_673183/lbs_674685/xgxw_674687/t1240440.shtml)

94. "[习近平接受九国新任驻华大使递交国书](http://www.xinhuanet.com/politics/2015-04/14/c_1114968963.htm)