## 行政區域

下列所示的5個行政區域，實際上與行政管治沒有很大的相關，而是與規劃有關。

<table>
<thead>
<tr class="header">
<th><p>分區</p></th>
<th><p>主要城鎮</p></th>
<th><p>大小<br />
km<sup>2</sup></p></th>
<th><p>人口<br />
（於2006年估計）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿內加達島.md" title="wikilink">阿內加達島</a></p></td>
<td><p>聚落區</p></td>
<td><p>38.6</p></td>
<td><p>204</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/約斯特·范大克島.md" title="wikilink">約斯特·范大克島</a></p></td>
<td><p>大海港</p></td>
<td><p>8.3</p></td>
<td><p>176</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/托土拉島.md" title="wikilink">托土拉島</a></p></td>
<td><p><a href="../Page/羅德城.md" title="wikilink">羅德城</a></p></td>
<td><p>59.2</p></td>
<td><p>16,630</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/處女戈達島.md" title="wikilink">處女戈達島</a></p></td>
<td><p>西班牙城</p></td>
<td><p>21.2</p></td>
<td><p>3063</p></td>
</tr>
<tr class="odd">
<td><p>其他群島</p></td>
<td><p>彼得島渡假區</p></td>
<td><p>23.7</p></td>
<td><p>181</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英屬處女群島.md" title="wikilink">英屬處女群島</a></p></td>
<td><p><a href="../Page/羅德城.md" title="wikilink">羅德城</a></p></td>
<td><p>151.0</p></td>
<td><p>20,253</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

上面所指的「其他群島」並非指四大島以外餘下之所有島嶼，而是僅指托土拉島以南的島嶼，即受[法蘭西斯·德西克爵士海峽所分隔](../Page/法蘭西斯·德西克爵士海峽.md "wikilink")，又在處女戈達島之西南方島嶼。這包括[諾曼島](../Page/諾曼島.md "wikilink")、[彼得島](../Page/彼得島.md "wikilink")、[庫柏島和](../Page/庫柏島.md "wikilink")[鹽島](../Page/鹽島.md "wikilink")。這數個島嶼一向有習慣被共稱作「[小姊妹島嶼](../Page/小姊妹島嶼.md "wikilink")」（Little
Sisters）。此外，[薑島雖然不在上面範圍](../Page/薑島.md "wikilink")，但也計作「其他群島」之一；至於在這裡沒有交代的剩餘島嶼，則一概已歸併到四大島中計算。

## 戶籍登記區域

英屬處女群島共劃有6個戶籍登記區域：

| 戶籍登記區域 | 地區                                                                    |
| ------ | --------------------------------------------------------------------- |
| 區域 A   | [處女戈達島](../Page/處女戈達島.md "wikilink")                                  |
| 區域 B   | [阿內加達島](../Page/阿內加達島.md "wikilink")                                  |
| 區域 C   | 東堤 （[托土拉島](../Page/托土拉島.md "wikilink")）                               |
| 區域 D   | [羅德城](../Page/羅德城.md "wikilink") （[托土拉島](../Page/托土拉島.md "wikilink")） |
| 區域 E   | 西堤 （[托土拉島](../Page/托土拉島.md "wikilink")）                               |
| 區域 F   | [約斯特·范大克島](../Page/約斯特·范大克島.md "wikilink")                            |
|        |                                                                       |

戶籍登記區域C、D和E皆托土拉島上的分區。

## 選舉區域

  - 根據《1954年憲法及選舉條例》，立法局內議席主要由選舉產生，並因此衍生5大選區。各選區均產生一個議席，惟羅德城選區可產生兩個議席。

<!-- end list -->

  - 到1967年，選區增至7個，每區各產生一個議席。

<!-- end list -->

  - 在1977年，選區再改至9個，各選出一名議員，到有13個議席的立法局。

| 選區   | 地區                                                                        |
| ---- | ------------------------------------------------------------------------- |
| 第一選區 |                                                                           |
| 第二選區 |                                                                           |
| 第三選區 |                                                                           |
| 第四選區 | [羅德城與托土拉島周邊地區](../Page/羅德城.md "wikilink")                                 |
| 第五選區 |                                                                           |
| 第六選區 |                                                                           |
| 第七選區 |                                                                           |
| 第八選區 |                                                                           |
| 第九選區 | [處女戈達島與](../Page/處女戈達島.md "wikilink")[阿內加達島](../Page/阿內加達島.md "wikilink") |
|      |                                                                           |

## 外部連結

  - [World Gazetteer網：
    統計與分區](http://bevoelkerungsstatistik.de/wg.php?x=&men=gadm&lng=de&dat=32&geo=-229&srt=npan&col=aohdq)
  - [Citypopulation網：統計與分區](http://www.citypopulation.de/BritVirgins.html)
  - [有關戶口登記區](http://www.candoo.com/genresources/bvi.htm)
  - [有關選區](https://web.archive.org/web/20060720113600/http://www.bviibc.com/facts.html)
  - [選區歷史](https://web.archive.org/web/20060616083205/http://www.islandsun.com/HISTORY/crono71498.html)

[Category:英屬處女群島](../Category/英屬處女群島.md "wikilink")