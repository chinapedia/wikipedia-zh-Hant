**英敛之**，[满族老姓](../Page/满族.md "wikilink")**[赫舍里氏](../Page/赫舍里氏.md "wikilink")**，名**英华**，字**斂之**，[以字行](../Page/以字行.md "wikilink")，號**安蹇**，又号**万松野人**，又名**玉英华**，教名**文森蒂斯**（Vincentius），[滿洲](../Page/滿洲.md "wikilink")[正紅旗人](../Page/正紅旗.md "wikilink")，[清末民初](../Page/清.md "wikilink")[教育家](../Page/教育家.md "wikilink")、[記者](../Page/記者.md "wikilink")，[保皇黨](../Page/保皇黨.md "wikilink")、[維新派人物](../Page/維新派.md "wikilink")，[中國近代](../Page/中國.md "wikilink")[天主教精神領袖](../Page/天主教.md "wikilink")。[輔仁大學](../Page/輔仁大學.md "wikilink")、《[大公報](../Page/大公報.md "wikilink")》創辦人。其妻**淑仲**是[爱新觉罗氏](../Page/爱新觉罗氏.md "wikilink")，恂勤郡王[允禵的后裔](../Page/允禵.md "wikilink")。

## 生平

[Cliff_inscription_on_Xianlong_Hill_(20170108152422).jpg](https://zh.wikipedia.org/wiki/File:Cliff_inscription_on_Xianlong_Hill_\(20170108152422\).jpg "fig:Cliff_inscription_on_Xianlong_Hill_(20170108152422).jpg")显龙山顶刻有英敛之1913年所书“水流云在”的石壁\]\]

  - 1867年11月23日，英敛之出生于[北京的](../Page/北京.md "wikilink")[旗人家庭](../Page/旗人.md "wikilink")。[汉姓为](../Page/汉姓.md "wikilink")**玉**，又名**玉英华**，幼年家贫，未接受正规[教育](../Page/教育.md "wikilink")，自幼习武从军。
  - 1888年，他成为一名虔诚的天主教徒，并自学[法文](../Page/法文.md "wikilink")。
  - 1898年4月，撰写文章《论兴利必先除弊》，开始评论国事。[戊戌变法失败后](../Page/戊戌变法.md "wikilink")，恐被株连，潜往外地。
  - 1899年8月，在[澳门](../Page/澳门.md "wikilink")《知新报》上发表《党祸余言》，对[变法失败](../Page/变法.md "wikilink")“深感郁结，心不能已”。
  - 1900年3月到[雲南](../Page/雲南.md "wikilink")，担任[法国驻](../Page/法国.md "wikilink")[蒙自领事馆](../Page/蒙自.md "wikilink")[書記](../Page/書記.md "wikilink")。由于当地常发生教案，同年7月随[领事返回](../Page/领事.md "wikilink")[天津](../Page/天津.md "wikilink")。
  - 1901年4月，[天津](../Page/天津.md "wikilink")[紫竹林天主堂总管](../Page/紫竹林.md "wikilink")[柴天宠提议集资开办](../Page/柴天宠.md "wikilink")[报馆](../Page/报馆.md "wikilink")，邀英敛之进行筹备。
  - 1902年7月17日，《[大公报](../Page/大公报.md "wikilink")》在天津发刊，英敛之兼任总理和[編輯工作](../Page/編輯.md "wikilink")，提倡变法维新，抨击时弊，不避权贵，以“开风气，牖民智，挹彼欧西学术，启我同胞聪明”為辦報宗旨，他本人每日为《大公报》写一篇社论。
  - 1906年7月1日，与《[北洋日报](../Page/北洋日报.md "wikilink")》等联名发表《告天津各报大主笔书》，发起组织中国近代第一个新闻团体“天津报馆俱乐部”，作为同业“研究报务、交换知识”的场所。
  - 1912年，他和好友[马相伯一起上书](../Page/马相伯.md "wikilink")[教廷](../Page/教廷.md "wikilink")，请求创办一所[天主教大学](../Page/天主教大学.md "wikilink")。
  - 1916年，英敛之将《大公报》转手售予[王郅隆](../Page/王郅隆.md "wikilink")，迁居[北京](../Page/北京.md "wikilink")[香山](../Page/香山_\(北京\).md "wikilink")[静宜园](../Page/静宜园.md "wikilink")，创办[香山慈幼院和](../Page/香山慈幼院.md "wikilink")[辅仁社](../Page/辅仁社.md "wikilink")（1925年）。
  - 1926年1月10日病逝。去世后的次年，[辅仁大学在北京正式立案](../Page/辅仁大学.md "wikilink")。

## 著作

生前曾将所撰报刊文论汇编为《也是集》、《敝帚千金》等。

## 世系图

<center>

</center>

## 參見

  - [天主教輔仁大學北平時期](../Page/天主教輔仁大學北平時期.md "wikilink")

## 外部链接

  - [关注英氏家族成功奥秘：历代重视中西文化交流](http://news.sina.com.cn/c/sd/2010-01-22/153619525802.shtml)

[Y英](../Category/北京人.md "wikilink")
[Y英](../Category/輔仁大學教授.md "wikilink")
[Y英](../Category/清朝天主教徒.md "wikilink")
[Y英](../Category/中华民国天主教徒.md "wikilink")
[Y英](../Category/中华民国大陆时期记者.md "wikilink")
[Y英](../Category/滿洲正紅旗人.md "wikilink")
[Y英](../Category/赫舍里氏.md "wikilink")
[L](../Category/英姓.md "wikilink")