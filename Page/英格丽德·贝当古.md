[Íngrid_Betancourt_in_Pisa.jpg](https://zh.wikipedia.org/wiki/File:Íngrid_Betancourt_in_Pisa.jpg "fig:Íngrid_Betancourt_in_Pisa.jpg")

**英格丽德·贝当古·普莱西奥**（，\[1\]），[哥伦比亚](../Page/哥伦比亚.md "wikilink")[法國裔的政治家](../Page/法國.md "wikilink")，曾任哥伦比亚参议员及反貪污活躍人士。貝當古於2002年2月23日被反政府的[哥倫比亞革命武裝力量](../Page/哥倫比亞革命武裝力量.md "wikilink")(FARC)綁架，六年半後於2008年7月2日被哥倫比亞軍方的[夏克行動所拯救](../Page/夏克行動.md "wikilink")，連同另外14名人質重獲自由\[2\]\[3\]。在被擄前，她曾是當地總統選舉的[氧氣綠黨代表](../Page/氧氣綠黨.md "wikilink")，在未有理會軍方及警方的勸籲而前往一個被武裝力量控制的地區拉票時被擄。由於她同時擁有哥倫比亞及法國的國籍，事件透過法國傳媒而廣被世人所知。

## 著作

《別為我哭泣》(Until death do us part:my struggle to reclaim Colombia)，英格丽德·贝当古
著，中文版 商周出版社，2008年8月5日，ISBN：9789867747453。

## 參考

## 外部链接

  - [International petition for Betancourt's
    release](http://www.golivewire.com)

  - [Betancourt.info](https://web.archive.org/web/20080303101913/http://www.betancourt.info/indexEng.htm)

  - [Betancourt-France.org](https://web.archive.org/web/20170710194134/http://www.betancourt-france.org/)

  - ["The Kidnapping of Ingrid Betancourt" documentary
    film](https://web.archive.org/web/20080707043343/http://www.wmm.com/filmCatalog/pages/c625.shtml)

  - [Betancourt Interview with
    Salon.com](https://web.archive.org/web/20080614011028/http://dir.salon.com/story/people/conv/2002/01/15/betancourt/)

[Category:哥伦比亚政治人物](../Category/哥伦比亚政治人物.md "wikilink")
[Category:巴黎政治大学校友](../Category/巴黎政治大学校友.md "wikilink")
[Category:人质](../Category/人质.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")

1.
2.
3.