**英格兰PFA足球先生**（**PFA Players' Player of the
Year**，別稱**英格蘭球員先生**）於每年的球季末期由[英格蘭職業足球運動員協會](../Page/英格蘭職業足球運動員協會.md "wikilink")（Professional
Footballers Association，簡稱PFA）的會員互相投票選出前一年的年度最佳球員。首次PFA足球先生在1974年頒發。

自設立這個獎項以來，獲獎者多是英倫三島球員，自[英超成立以來才開始有外籍球員獲獎](../Page/英超.md "wikilink")。首位得獎外籍球員乃[法國人](../Page/法國.md "wikilink")[-{zh-hans:埃里克·坎通纳;
zh-hk:簡東拿;}-](../Page/埃里克·坎通纳.md "wikilink")，於1994年贏得。

1977年的[-{zh-hans:安迪·格雷;
zh-hk:安迪·格雷;}-和](../Page/安迪·格雷.md "wikilink")2007年的[-{zh-hans:克里斯蒂亚诺·罗纳尔多;zh-hk:基斯坦奴·朗拿度;zh-tw:克里斯蒂亞諾·羅納度;}-於同年獲得](../Page/基斯坦奴·朗拿度.md "wikilink")[PFA年度最佳青年球員](../Page/PFA年度最佳青年球員.md "wikilink")。

英格兰足坛年度大奖還有[FWA足球先生](../Page/FWA足球先生.md "wikilink")，由[英格兰足球记者协会](../Page/英格兰足球记者协会.md "wikilink")（[Football
Writers'
Association](../Page/:en:Football_Writers'_Association.md "wikilink")）评选，始于1948年。另一個大獎則為[英格蘭年度球員](../Page/英格蘭年度球員.md "wikilink")，由[英格蘭足總](../Page/英格蘭足總.md "wikilink")（The
Football
Association）頒發，獲選者為[英格蘭足球代表隊的最佳球員](../Page/英格蘭足球代表隊.md "wikilink")，在足總的官方網站theFA.com經球迷選出。

## 歷屆獲獎人

### 1970年代

  - 1974年 -  **[-{zh-hans:诺曼·亨特;
    zh-hk:諾曼·亨特;}-](../Page/诺曼·亨特.md "wikilink")**
    (Norman Hunter)，([-{zh-hans:利兹联;
    zh-hk:列斯聯;}-](../Page/利兹联足球俱乐部.md "wikilink"))
  - 1975年 -  **[-{zh-hans:科林·托德;
    zh-hk:哥連·鐸;}-](../Page/科林·托德.md "wikilink")** (Colin
    Todd)，([-{zh-hans:德比郡;
    zh-hk:打比郡;}-](../Page/德比郡足球俱乐部.md "wikilink"))
  - 1976年 -  **[-{zh-hans:帕特·詹宁斯;
    zh-hk:柏·真寧斯;}-](../Page/帕特·詹宁斯.md "wikilink")**
    (Pat Jennings)，([-{zh-hans:托特纳姆热刺;
    zh-hk:熱刺;}-](../Page/托特纳姆足球俱乐部.md "wikilink"))
  - 1977年 -  **[-{zh-hans:安迪·格雷;
    zh-hk:安迪·格雷;}-](../Page/安迪·格雷.md "wikilink")**
    (Andy Gray)，([阿斯顿维拉](../Page/阿斯顿维拉足球俱乐部.md "wikilink"))
  - 1978年 -  **[-{zh-hans:彼德·希尔顿;
    zh-hk:彼得·施路頓;}-](../Page/彼德·希尔顿.md "wikilink")**
    (Peter Shilton)，([-{zh-hans:诺丁汉森林;
    zh-hk:諾定咸森林;}-](../Page/诺丁汉森林足球俱乐部.md "wikilink"))
  - 1979年 -  **[-{zh-hans:里亚姆·布拉迪;
    zh-hk:林·柏拉迪;}-](../Page/里亚姆·布拉迪.md "wikilink")**
    (Liam Brady)，([-{zh-hans:阿森纳;
    zh-hk:阿仙奴;}-](../Page/阿森纳足球俱乐部.md "wikilink"))

### 1980年代

  - 1980年 -  **[-{zh-hans:泰利·麦克德莫特;
    zh-hk:泰利·麥特莫;}-](../Page/泰利·麦克德莫特.md "wikilink")**
    (Terry McDermott)，([利物浦](../Page/利物浦足球會.md "wikilink"))
  - 1981年 -  **[-{zh-hans:约翰·沃克;
    zh-hk:約翰·獲克;}-](../Page/約翰·獲克.md "wikilink")**
    (John Wark)，([-{zh-hans:伊普斯维奇城;
    zh-hk:葉士域治城;}-](../Page/伊普斯维奇城足球俱乐部.md "wikilink"))
  - 1982年 -  **[-{zh-hans:凯文·基冈;
    zh-hk:奇雲·基謹;}-](../Page/凯文·基冈.md "wikilink")**
    (Kevin Keegan)，([-{zh-hans:南安普顿;
    zh-hk:修咸頓;}-](../Page/南安普顿足球俱乐部.md "wikilink"))
  - 1983年 -  **[-{zh-hans:肯尼·达格利什;
    zh-hk:杜格利殊;}-](../Page/肯尼·达格利什.md "wikilink")**
    (Kenny Dalglish)，([利物浦](../Page/利物浦足球會.md "wikilink"))
  - 1984年 -  **[-{zh-hans:伊恩·拉什;
    zh-hk:魯殊;}-](../Page/伊恩·拉什.md "wikilink")** (Ian
    Rush)，([利物浦](../Page/利物浦足球會.md "wikilink"))
  - 1985年 -  **[-{zh-hans:彼得·里德;
    zh-hk:彼得·列特;}-](../Page/彼得·里德.md "wikilink")**
    (Peter Reid)，([愛華頓](../Page/愛華頓.md "wikilink"))
  - 1986年 -  **[-{zh-hans:加里·莱因克尔;
    zh-hk:連尼加;}-](../Page/加里·莱因克尔.md "wikilink")**
    (Gary Lineker)，([愛華頓](../Page/愛華頓.md "wikilink"))
  - 1987年 -  **[-{zh-hans:克莱夫·阿兰;
    zh-hk:佳夫·阿倫;}-](../Page/克莱夫·阿兰.md "wikilink")**
    (Clive Allen)，([-{zh-hans:托特纳姆热刺;
    zh-hk:熱刺;}-](../Page/托特纳姆热刺足球俱乐部.md "wikilink"))
  - 1988年 -  **[-{zh-hans:约翰·巴恩斯;
    zh-hk:約翰·班尼斯;}-](../Page/约翰·巴恩斯.md "wikilink")**
    (John Barnes)，([利物浦](../Page/利物浦足球會.md "wikilink"))
  - 1989年 -  **[-{zh-hans:马克·休斯;
    zh-hk:馬克·曉士;}-](../Page/马克·休斯.md "wikilink")**
    (Mark Hughes)，([曼聯](../Page/曼聯.md "wikilink"))

### 1990年代

  - 1990年 -  **[-{zh-hans:戴维·普拉特;
    zh-hk:大衛·柏列;}-](../Page/戴维·普拉特.md "wikilink")**
    (David Platt)，([阿斯顿维拉](../Page/阿斯顿维拉足球俱乐部.md "wikilink"))
  - 1991年 -  **[-{zh-hans:马克·休斯;
    zh-hk:馬克·曉士;}-](../Page/马克·休斯.md "wikilink")**
    (Mark Hughes)，([曼聯](../Page/曼聯.md "wikilink"))
  - 1992年 -  **[-{zh-hans:加里·帕莱斯特;
    zh-hk:巴里斯達;}-](../Page/加里·帕莱斯特.md "wikilink")**
    (Gary Pallister)，([曼聯](../Page/曼聯.md "wikilink"))
  - 1993年 -  **[-{zh-hans:保罗·麦格拉斯;
    zh-hk:保羅·麥格夫;}-](../Page/保罗·麦格拉斯.md "wikilink")**
    (Paul McGrath)，([阿士東維拉](../Page/阿士東維拉.md "wikilink"))
  - 1994年 -  **[-{zh-hans:埃里克·坎通纳;
    zh-hk:簡東拿;}-](../Page/埃里克·坎通纳.md "wikilink")**
    (Eric Cantona)，([曼聯](../Page/曼聯.md "wikilink"))
  - 1995年 -
    **[-{zh-hans:阿兰·希勒;zh-hk:舒利亞;zh-tw:阿蘭·希勒;}-](../Page/阿兰·希勒.md "wikilink")**
    (Alan Shearer)，([-{zh-hans:布莱克本流浪者;
    zh-hk:布力般流浪;}-](../Page/布莱克本流浪者足球俱乐部.md "wikilink"))
  - 1996年 -  **[-{zh-hans:莱斯·费迪南德;
    zh-hk:費迪南;}-](../Page/莱斯·费迪南德.md "wikilink")**
    (Les Ferdinand)，([纽卡斯尔](../Page/纽卡斯尔联足球俱乐部.md "wikilink"))
  - 1997年 -
    **[-{zh-hans:阿兰·希勒;zh-hk:舒利亞;zh-tw:阿蘭·希勒;}-](../Page/阿兰·希勒.md "wikilink")**
    (Alan Shearer)，([紐卡素](../Page/紐卡素.md "wikilink"))
  - 1998年 -
    **[-{zh-hans:丹尼斯·博格坎普;zh-hk:柏金;zh-tw:博格坎普;}-](../Page/丹尼斯·博格坎普.md "wikilink")**
    (Dennis Bergkamp)，([-{zh-hans:阿森纳;
    zh-hk:阿仙奴;}-](../Page/阿森纳足球俱乐部.md "wikilink"))
  - 1999年 -  **[-{zh-hans:大卫·吉诺拉;
    zh-hk:真路拿;}-](../Page/大卫·吉诺拉.md "wikilink")**
    (David Ginola)，([-{zh-hans:托特纳姆热刺;
    zh-hk:熱刺;}-](../Page/托特纳姆热刺足球俱乐部.md "wikilink"))

### 2000年代

  - 2000年 -  **[-{zh-hans:罗伊·基恩;
    zh-hk:堅尼;}-](../Page/萊·堅尼.md "wikilink")** (Roy
    Keane)，([曼联](../Page/曼联.md "wikilink"))
  - 2001年 -  **[-{zh-hans:特迪·谢林汉姆;
    zh-hk:舒寧咸;}-](../Page/泰迪·舒寧咸.md "wikilink")**
    (Teddy Sheringham)，([曼联](../Page/曼联.md "wikilink"))
  - 2002年 -  **[-{zh-hans:路德·范尼斯特鲁伊;
    zh-hk:雲尼斯達萊;}-](../Page/路德·范尼斯特鲁伊.md "wikilink")**
    (Ruud van Nistelrooy)，([曼联](../Page/曼联.md "wikilink"))
  - 2003年 -  **[-{zh-hans:蒂埃里·亨利;
    zh-hk:亨利;}-](../Page/蒂埃里·亨利.md "wikilink")** (Thierry
    Henry)，([-{zh-hans:阿森纳;
    zh-hk:阿仙奴;}-](../Page/阿森纳足球俱乐部.md "wikilink"))
  - 2004年 -  **[-{zh-hans:蒂埃里·亨利;
    zh-hk:亨利;}-](../Page/蒂埃里·亨利.md "wikilink")** (Thierry
    Henry)，([-{zh-hans:阿森纳;
    zh-hk:阿仙奴;}-](../Page/阿森纳足球俱乐部.md "wikilink"))
  - 2005年 -
    **[-{zh-hans:约翰·特里;zh-hk:泰利;zh-tw:特里;}-](../Page/約翰·泰利.md "wikilink")**
    (John Terry)，([-{zh-hans:切尔西;
    zh-hk:車路士;}-](../Page/切尔西足球俱乐部.md "wikilink"))
  - 2006年 -  **[-{zh-hans:斯蒂文·杰拉德;
    zh-hk:謝拉特;}-](../Page/史提芬·謝拉特.md "wikilink")**
    (Steven Gerrard)，（[利物浦](../Page/利物浦足球俱乐部.md "wikilink")）
  - 2007年 -
    **[-{zh-hans:克里斯蒂亚诺·罗纳尔多;zh-hk:基斯坦奴·朗拿度;zh-tw:克里斯蒂亞諾·羅納度;}-](../Page/基斯坦奴·朗拿度.md "wikilink")**
    (Cristiano Ronaldo)，([-{zh-hans:曼联;
    zh-hk:曼聯;}-](../Page/曼彻斯特联队.md "wikilink"))<small>\[1\]</small>
  - 2008年 -
    **[-{zh-hans:克里斯蒂亚诺·罗纳尔多;zh-hk:基斯坦奴·朗拿度;zh-tw:克里斯蒂亞諾·羅納度;}-](../Page/基斯坦奴·朗拿度.md "wikilink")**
    (Cristiano Ronaldo)，([-{zh-hans:曼联;
    zh-hk:曼聯;}-](../Page/曼彻斯特联队.md "wikilink"))<small>\[2\]</small>
  - 2009年 -  **[-{zh-hans:瑞恩·吉格斯;
    zh-hk:賴恩·傑斯;}-](../Page/瑞恩·吉格斯.md "wikilink")**
    (Ryan Giggs)，([-{zh-hans:曼联;
    zh-hk:曼聯;}-](../Page/曼彻斯特联队.md "wikilink"))<small>\[3\]</small>

### 2010年代

  - 2010年 -  **[-{zh-hans:韦恩·鲁尼;
    zh-hk:朗尼;}-](../Page/韦恩·鲁尼.md "wikilink")** (Wayne
    Rooney)，([曼聯](../Page/曼徹斯特聯足球俱樂部.md "wikilink"))\[4\]
  - 2011年 -  **[-{zh-hans:格瑞斯·贝尔;
    zh-hk:巴利;}-](../Page/格瑞斯·贝尔.md "wikilink")** (Gareth
    Bale)，([-{zh-hans:托特纳姆热刺;
    zh-hk:熱刺;}-](../Page/托特纳姆热刺足球俱乐部.md "wikilink"))\[5\]
  - 2012年 -  **[-{zh-hans:羅賓·范佩西;
    zh-hk:雲佩斯;}-](../Page/羅賓·范佩西.md "wikilink")**
    (Robin van Persie)，([-{zh-hans:阿森纳;
    zh-hk:阿仙奴;}-](../Page/阿森纳足球俱乐部.md "wikilink"))\[6\]
  - 2013年 -  **[-{zh-hans:格瑞斯·贝尔;
    zh-hk:巴利;}-](../Page/格瑞斯·贝尔.md "wikilink")** (Gareth
    Bale)，([-{zh-hans:托特纳姆热刺;
    zh-hk:熱刺;}-](../Page/托特纳姆热刺足球俱乐部.md "wikilink"))
  - 2014年 -  **[蘇亞雷斯](../Page/路爾斯·艾拔圖·蘇亞雷斯.md "wikilink")** (Luis
    Suárez)，([利物浦](../Page/利物浦.md "wikilink"))
  - 2015年 -  **[-{zh-hans:艾登·阿扎尔;
    zh-hk:伊登·夏薩特;}-](../Page/伊登·夏薩特.md "wikilink")**
    (Eden Hazard)，([-{zh-hans:切尔西;
    zh-hk:車路士;}-](../Page/切尔西足球俱乐部.md "wikilink"))
  - 2016年 -  **[-{zh-hans:马赫雷斯;
    zh-hk:馬列斯;}-](../Page/利亞德·馬列斯.md "wikilink")**
    (Riyad Mahrez)，([-{zh-hans:萊斯特城;
    zh-hk:李斯特城;}-](../Page/李斯特城足球會.md "wikilink"))
  - 2017年 -  **[-{zh-hans:恩戈洛·坎特;
    zh-hk:尼高路·簡迪;}-](../Page/尼高路·簡迪.md "wikilink")**
    (N'Golo Kanté)，([-{zh-hans:切尔西;
    zh-hk:車路士;}-](../Page/切尔西足球俱乐部.md "wikilink"))
  - 2018年 -
    **[-{zh-hans:萨拉赫;zh-hk:沙拿;}-](../Page/穆罕默德·沙拿.md "wikilink")**
    (Mohamed Salah)，([利物浦](../Page/利物浦.md "wikilink"))

## 統計分類

### 獲獎球員的國籍

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>贏得獎項的次數</p></th>
<th><p>贏得獎項的賽季</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/英格蘭.md" title="wikilink">英格蘭</a></p></td>
<td><center>
<p>18</p></td>
<td><p>1973–74賽季，1974–75賽季，1977–78賽季，1979–80賽季，1981–82賽季，1984–85賽季，1985–86賽季，1986–87賽季，1987–88賽季，1989–90賽季，1991–92賽季，1994–95賽季，1995–96賽季，1996–97賽季，2000–01賽季，2004–05賽季，2005–06賽季，2009–10賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/威爾斯.md" title="wikilink">威爾斯</a></p></td>
<td><center>
<p>6</p></td>
<td><p>1983–84賽季，1988–89賽季，1990–91賽季，2008–09賽季，2010–11賽季，2012–13賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/法國.md" title="wikilink">法國</a></p></td>
<td><center>
<p>5</p></td>
<td><p>1993–94賽季，1998–99賽季，2002–03賽季，2003–04賽季，2016–17賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇格蘭.md" title="wikilink">蘇格蘭</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1976–77賽季，1980–81賽季，1982–83賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛爾蘭.md" title="wikilink">愛爾蘭</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1978–79賽季，1992–93賽季，1999–2000賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/荷蘭.md" title="wikilink">荷蘭</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1997–98賽季，2001–02賽季，2011–12賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a></p></td>
<td><center>
<p>2</p></td>
<td><p>2006–07賽季，2007–08賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1975–76賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/烏拉圭.md" title="wikilink">烏拉圭</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2013–14賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/比利時.md" title="wikilink">比利時</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2014–15賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿爾及利亞.md" title="wikilink">阿爾及利亞</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2015–16賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/埃及.md" title="wikilink">埃及</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2017–18賽季</p></td>
</tr>
</tbody>
</table>

### 獲獎球員當時所屬的球會

<table style="width:100%;">
<colgroup>
<col style="width: 32%" />
<col style="width: 34%" />
<col style="width: 34%" />
</colgroup>
<thead>
<tr class="header">
<th><p>球會</p></th>
<th><p>贏得獎項的次數</p></th>
<th><p>贏得獎項的賽季</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/曼徹斯特聯足球俱樂部.md" title="wikilink">曼聯</a></p></td>
<td><center>
<p>11</p></td>
<td><p>1988–89賽季，1990–91賽季，1991–92賽季，1993–94賽季，1999–00賽季，2000–01賽季，2001–02賽季，2006–07賽季，2007–08賽季，2008–09賽季，2009–10賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/利物浦足球俱乐部.md" title="wikilink">利物浦</a></p></td>
<td><center>
<p>7</p></td>
<td><p>1979–80賽季，1982–83賽季，1983–84賽季，1987–88賽季，2005–06賽季，2013-14賽季，2017-18賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/托特纳姆热刺足球俱乐部.md" title="wikilink">-{zh-hans:托特纳姆热刺; zh-hk:托定咸熱刺;zh-tw:托特納姆熱刺;}-</a></p></td>
<td><center>
<p>5</p></td>
<td><p>1975–76賽季，1986–87賽季，1998–99賽季，2010–11賽季，2012–13賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿仙奴足球會.md" title="wikilink">阿仙奴</a></p></td>
<td><center>
<p>5</p></td>
<td><p>1978–79賽季，1997–98賽季，2002–03賽季，2003–04賽季，2011–12賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/切尔西足球俱乐部.md" title="wikilink">-{zh-hans:切尔西;zh-hk:車路士;zh-tw:切爾西}-</a></p></td>
<td><center>
<p>3</p></td>
<td><p>2004–05賽季，2014–15賽季，2016–17賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿斯顿维拉足球俱乐部.md" title="wikilink">-{zh-hans:阿斯顿维拉; zh-hk:阿士東維拉;zh-tw:阿斯頓維拉;}-</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1976–77賽季，1989–90賽季，1992–93賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛華頓足球會.md" title="wikilink">-{zh-hans:埃弗顿;zh-hk:愛華頓;zh-tw:艾佛頓;}-</a></p></td>
<td><center>
<p>2</p></td>
<td><p>1984–85賽季，1985–86賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紐卡素足球會.md" title="wikilink">-{zh-hans:纽卡斯尔联;zh-hk:紐卡素;zh-tw:紐卡索聯;}-</a></p></td>
<td><center>
<p>2</p></td>
<td><p>1995–96賽季，1996–97賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/列斯聯足球會.md" title="wikilink">-{zh-hans:利兹联;zh-hk:列斯聯;zh-tw:里茲聯;}-</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1973–74賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/打吡郡足球會.md" title="wikilink">-{zh-hans:德比郡;zh-hk:打吡郡;zh-tw:德比郡}-</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1974–75賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/諾定咸森林足球會.md" title="wikilink">-{zh-hans:诺丁汉森林;zh-hk:諾定咸森林;zh-tw:諾丁漢森林;}-</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1977–78賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伊普斯维奇城足球俱乐部.md" title="wikilink">-{zh-hans:伊普斯维奇; zh-hk:葉士域治;zh-tw:伊普斯維奇;}-</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1980–81賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南安普顿足球俱乐部.md" title="wikilink">-{zh-hans:南安普顿;zh-hk:修咸頓;zh-tw:南安普頓}-</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1981–82賽季</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布力般流浪足球會.md" title="wikilink">-{zh-hans:布莱克本流浪者;zh-hk:布力般流浪;zh-tw:布萊克本流浪者;}-</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1994–95賽季</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莱斯特城足球俱乐部.md" title="wikilink">-{zh-hans:莱斯特城;zh-hk:李斯特城;zh-tw:萊斯特城}-</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2015–16賽季</p></td>
</tr>
</tbody>
</table>

## 参考資料

## 外部連結

  - [PFA足球先生](https://web.archive.org/web/20060418050425/http://www.givemefootball.com/rolls_of_honour/playersplayerroll.html)

[Category:英格蘭足球](../Category/英格蘭足球.md "wikilink")
[Category:英国足球奖项](../Category/英国足球奖项.md "wikilink")
[Category:1974年建立的獎項](../Category/1974年建立的獎項.md "wikilink")

1.  [-{zh-hans:罗纳尔多;zh-hk:C·朗拿度;zh-tw:羅納度;}-連奪兩獎](http://news.bbc.co.uk/sport2/hi/football/6582201.stm)
2.  [Ronaldo named player of the
    year](http://news.bbc.co.uk/sport2/hi/football/7370319.stm)
3.
4.
5.
6.