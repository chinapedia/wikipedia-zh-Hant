《**英皇制誥**》（；1843年—1997年），通稱「**香港授命狀**」或「**香港憲章**」，是[香港在](../Page/香港.md "wikilink")[英國殖民時期的重要](../Page/香港英治時期.md "wikilink")[憲制性](../Page/憲制.md "wikilink")[法律](../Page/法律.md "wikilink")[文件](../Page/文件.md "wikilink")，為[皇室制誥的一種](../Page/皇室制誥.md "wikilink")。當中的21條條文授予[香港總督權力](../Page/香港總督.md "wikilink")，能夠掌控[行政](../Page/行政.md "wikilink")、[立法及](../Page/立法.md "wikilink")[司法機關](../Page/司法.md "wikilink")，而這些機關並無任何制衡港督的權力，而王室對香港總督和政府有絕對的控制權，確立了香港所有權力集中在總督，使總督能高效率施政的殖民地政體。

## 概況

1843年4月5日，為了給予[英國政府對](../Page/英國政府.md "wikilink")[香港政府的統治提供指引](../Page/香港政府.md "wikilink")，[英國女王](../Page/英國女王.md "wikilink")[維多利亞以皇家特權立法的形式頒發](../Page/維多利亞女王.md "wikilink")《英皇制誥》及《[皇室訓令](../Page/皇室訓令.md "wikilink")》，並於同年6月26日在[香港總督府公佈](../Page/香港禮賓府.md "wikilink")\[1\]，根據前者，香港成為英國皇家殖民地；香港殖民地政府和[定例局則根據後者成立](../Page/定例局.md "wikilink")\[2\]。[英國國王對兩份文件都有絕對權力隨時更改](../Page/英國國王.md "wikilink")，而最後的版本公佈於1996年5月31日，內容主要確立[香港總督的職權](../Page/香港總督.md "wikilink")、授權設立[行政局與](../Page/行政局_\(香港\).md "wikilink")[定例局](../Page/定例局.md "wikilink")（其後改稱[立法局](../Page/立法局.md "wikilink")），以及王室對港督和政府的控制權等等。於[香港主權移交](../Page/香港主權移交.md "wikilink")[中華人民共和國後](../Page/中華人民共和國.md "wikilink")，地位由《[香港特別行政區基本法](../Page/香港特別行政區基本法.md "wikilink")》取代。

英皇制誥賦予香港總督很大權力，對[行政](../Page/行政.md "wikilink")、[立法及](../Page/立法.md "wikilink")[司法機關都有控制權](../Page/司法.md "wikilink")，例如港督兼任行政局和立法局主席，及委任兩局除當然官守議員之外的全部議員，可以違背行政局的決定而行事，亦可以不批准立法局通過的條例草案，並且有法官和官員任命權，可以隨時中止任何法官、官員和議員的職務。因此，立法局被稱為港督的「橡皮圖章」和「舉手機器」。後來制誥的修訂對總督的權力加入一些限制，例如[按察司的罷免必須通過英國樞密院批准](../Page/按察司.md "wikilink")，以維護法官於執行公義的個過程中不會因為行政機關施加壓力而不能夠保持獨立及客觀，以及保持公眾對法院的信心。

英皇制誥亦為立法機關提供了權力來源的確立，授權港督經諮詢立法局後制定香港法律和法例的全權\[3\]。立法局制定的法律不能超越制誥所授予的權限或與制誥有所牴觸，否則一律會被法院宣布無效。

在英國與[中華人民共和國簽訂](../Page/中華人民共和國.md "wikilink")《[中英聯合聲明](../Page/中英聯合聲明.md "wikilink")》，英國確定須交還香港後，立即著手大幅修訂《英皇制誥》及《皇室訓令》多次，從本質上削弱總督權力，例如立法局一直只有官守和委任議員但數次修訂後被民選議員全部取代，總督也不能再擔任立法局主席。（1984年6月30日頒發的《皇室訓令》增加立法局議員數目，計有港督、3名當然官守議員，25名其他官守議員，及32名委任議員；1985年4月2日頒發的《英皇制誥》，卻突然改革立法局，規定為港督，3名當然官守議員，最多7名其他官守議員，最多22名委任議員，及24名民選議員。）《[公民權利和政治權利國際公約](../Page/公民權利和政治權利國際公約.md "wikilink")》也被全盤引入香港，立法局制定法律時須符合公約規定。但是司法機關的權力卻被加強，例如法官的退休年齡推遲，之後仍可以延長任期而無須報請英女王批准，任何人不論資歷和年齡都可以被任命為最高法院按察司，在離主權移交前僅一年一個月時，港督一直以來擁有的中止各法院和審裁處的司法人員職務的權力更被撤消。

## 另見

  - [皇室訓令](../Page/皇室訓令.md "wikilink")
  - [香港基本法](../Page/香港基本法.md "wikilink")

## 參考來源

## 延伸閱讀

  -
  -
[Category:香港法律](../Category/香港法律.md "wikilink")
[Category:香港宪制性文件](../Category/香港宪制性文件.md "wikilink")
[Category:英國王室](../Category/英國王室.md "wikilink")

1.

2.

3.