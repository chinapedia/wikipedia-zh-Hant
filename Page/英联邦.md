\-{zh-cn:;zh-hk:;zh-tw:;zh-sg:}-
**英聯邦**（），是一個由53個[主權國家](../Page/主權國家.md "wikilink")（包括[屬地](../Page/屬地.md "wikilink")）所組成的国际组织，成員大多為前[英國殖民地或者](../Page/英國殖民地.md "wikilink")[保護國](../Page/保護國.md "wikilink")。[英聯邦元首為](../Page/英聯邦元首.md "wikilink")[伊麗莎白二世女王](../Page/伊丽莎白二世.md "wikilink")，同時身兼包括[英國在内的](../Page/英國.md "wikilink")16個[英聯邦王國的](../Page/英聯邦王國.md "wikilink")[國家元首](../Page/國家元首.md "wikilink")，此16國構成了一個現代版的[共主邦聯](../Page/共主邦聯.md "wikilink")。

2018年4月21日，[伊麗莎白二世女王於大英國協高峰會中提到下一任大英國協元首繼任者希望是](../Page/伊丽莎白二世.md "wikilink")[威爾斯親王查爾斯](../Page/威爾斯親王查爾斯.md "wikilink")，在53個會員國領袖討論之下，同意下一任大英國協元首由查爾斯接任。

## 历史

大英帝國的形成是300多年來貿易、移民與武力征服的結果，期間也有和平的商業和外交活動。帝國在全世界範圍内協助傳播了英國的宗教、信仰、法律，制度、人文、科學、技術、商業、語言、管理模式。帝国霸權幫助英国本土實現了驚人的經濟成長，並使其在國際政治中擁有更大的發言權。随着英國本土的民主繼續深化，[海外領土的人民大多已經可以決定其政府的政策與未来](../Page/英國海外領土.md "wikilink")，包括立法機關全面選舉和直選行政首長。當然，直到20世纪初，各殖民地總督仍舊由英國本土委派全權代表英國，直接控制其外交和國防的權力。

### 起源

伊丽莎白女王二世在1959年在[自治领日向加拿大发言时指出](../Page/自治领日.md "wikilink")，加拿大于1867年7月1日組成联邦意味着“大英帝国第一個独立国家”的诞生。她宣称：“这也代表着现在被称为大英国協的独立国家自由联合的开始。”然而，早在1884年，罗斯伯里爵士在访问澳大利亚时曾谈论过改变大英帝国，因为它的一些殖民地变得更加独立，如“大英国協”。英国和殖民地总理的初次会议于1887年召开，而这也导致1911年帝国会议的建立。

英联邦从帝国会议发展而来。[扬·史末资在](../Page/扬·史末资.md "wikilink")1917年提出了一个具体的建议，当时他创造了“英联邦”一词，并设想了1919年巴黎和平会议上的“未来宪政关系和精神调整”。“不列颠联邦”一词在1921年“英爱条约”中首次获得英国于法律上的承认，當時，愛爾蘭自由邦議員在宣誓成為國會議員時，就是用“英联邦”一詞來替代"大英帝國"。

### 自治领

在1926年皇家会议的“巴尔福特宣言”中，英国及其统治地位同意國協成员地位平等。國協成员在国内或外部事务的任何方面，并不属于一个人管理，尽管作为國協国家的成员，需要对官方忠诚。

1931年“[1931年西敏法令](../Page/1931年西敏法令.md "wikilink")”规定了这些关系的这些方面。该法案适用于加拿大而不需要批准，但澳大利亚，新西兰和纽芬兰需要批准该法规生效。1934年2月16日，经议会同意，纽芬兰政府不愿接受条约
，治理又回到了英国的直接控制之下。纽芬兰于1949年加入加拿大为其第十个省。澳大利亚和新西兰分别在1942年和1947年批准了“规约”。

虽然南非联盟不在需要通过“[1931年西敏法令](../Page/1931年西敏法令.md "wikilink")”的国家之列，但是依旧通过了1934年的“联盟地位法”和1934年“皇室执行职能和封锁法”这两项法律。这两项法律的批准确认南非作为一个主权国家的地位。

### 非殖民化

第二次世界大战结束后，大英帝国逐渐瓦解，大部分组成部分已经成为独立国家。截止2017年英国仍有14个海外领土。1949年4月，在“伦敦宣言”之后，“英国人”一词从國協的头衔中删除，以反映其非殖民化的性质。

[CommonwealthPrimeMinisters1944.jpg](https://zh.wikipedia.org/wiki/File:CommonwealthPrimeMinisters1944.jpg "fig:CommonwealthPrimeMinisters1944.jpg")

### 共和国

國協原本不接纳共和国加入，所有國協成员都必须以英王作为国家元首，爱尔兰就是通过修宪成为共和国而脱离國協。后因英国希望印度加入國協而印度不愿效忠英王，所以國協于1949年发表伦敦宣言，允许共和国加入國協。迄今为止國協的52个成员国内大部分为共和国，只有16个成员国以英国女皇作为国家元首，此類国家合稱國協王国成员国。

### G计划与邀请欧洲

2013年3月11日晚，英女王伊丽莎白二世签署了國協首份阐述其核心[价值观的文件](../Page/价值观.md "wikilink")——《[英联邦宪章](../Page/英联邦宪章.md "wikilink")》，这是“國協发展与革新歷程中一个重要里程碑”。《宪章》总结阐释了54个邦联成员在民主、人权、法治、国际和平与安全、可持续发展等16个方面的核心价值观和共同原则，它旨在维护邦联成员间的紧密联系、维持英国在邦联的影响力。2012年12月，该宪章由54个國協国家政府首脑签字通过。\[1\]2016年10月13日，马尔代夫政府决定脱离國協。\[2\]

### 外交代表机构

國協成员国派驻其他國協成员国的最高外交代表机构不称大使馆，而是称为。例如：英国派驻加拿大渥太华的外交代表机构不称英国驻加拿大大使馆，而是稱為。如果一国加入國協，那么其派駐英国以及各國協成员国的大使馆将自动成为高级专员公署，反过来如果一国退出英联邦，那英国和各领联邦成员国驻该国的高级专员公署便自动变成大使馆。高級專員公署的最高使節（）不稱大使，而稱。例如：加拿大驻澳大利亚堪培拉的最高外交使节稱作加拿大驻澳大利亚高级专员，而非加拿大驻澳大利亚大使。英国驻外高级专员公署使用的旗帜与英国驻外使馆使用的旗帜不同，例如：[英国驻華大使馆使用的旗帜为英国皇家徽章位于中央的联合王国国旗](../Page/英国驻華大使馆.md "wikilink")，而所使用的旗帜为普通的英国国旗。在某些情况下國協成员国公民所在地没有其国籍国驻当地使领馆，其可向英国外交代表机构寻求。若需更新护照而护照持有国未在當地設館，英国驻外机构会为其签发一本英国护照，国籍为國協公民。

## 架構

### 元首

歷任元首皆由英國君主兼仼。

### 秘書長

現任秘書長是派翠西亞·蘇格蘭（Patricia Scotland）

### 輪值主席

主席由其中一個成員國的政府首腦兼仼，每兩年更改在仼的成員國。

<table style="width:134%;">
<colgroup>
<col style="width: 20%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 10%" />
<col style="width: 50%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>姓名</p></th>
<th><p>所屬國家</p></th>
<th><p>在所屬國家的職位</p></th>
<th><p>首腦會議年分</p></th>
<th><p>就仼</p></th>
<th><p>離任</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/塔博·姆貝基.md" title="wikilink">塔博·姆貝基</a></p></td>
<td></td>
<td><p><a href="../Page/南非總統.md" title="wikilink">總統</a></p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_1999.md" title="wikilink">1999</a></p></td>
<td><p>1999年11月12日</p></td>
<td><p>2002年3月2日</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/約翰·霍華德.md" title="wikilink">約翰·霍華德</a></p></td>
<td></td>
<td><p><a href="../Page/澳洲總理.md" title="wikilink">總理</a></p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2002.md" title="wikilink">2002</a></p></td>
<td><p>2002年3月2日</p></td>
<td><p>2003年12月5日</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/奧盧塞貢·奧巴桑喬.md" title="wikilink">奧盧塞貢·奧巴桑喬</a></p></td>
<td></td>
<td><p><a href="../Page/尼日利亞總統.md" title="wikilink">總統</a></p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2003.md" title="wikilink">2003</a></p></td>
<td><p>2003年12月5日</p></td>
<td><p>2005年11月25日</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/勞倫斯·岡濟.md" title="wikilink">勞倫斯·岡濟</a></p></td>
<td></td>
<td><p><a href="../Page/馬爾他總理.md" title="wikilink">總理</a></p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2005.md" title="wikilink">2005</a></p></td>
<td><p>2005年11月25日</p></td>
<td><p>2007年11月23日</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/約韋里·穆塞維尼.md" title="wikilink">約韋里·穆塞維尼</a></p></td>
<td></td>
<td><p><a href="../Page/烏干達.md" title="wikilink">總統</a></p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2007.md" title="wikilink">2007</a></p></td>
<td><p>2007年11月23日</p></td>
<td><p>2009年11月27日</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/派屈克·曼寧.md" title="wikilink">派屈克·曼寧</a>[3]</p></td>
<td></td>
<td><p><a href="../Page/千里達和多巴哥總理.md" title="wikilink">總理</a></p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2009.md" title="wikilink">2009</a></p></td>
<td><p>2009年11月27日</p></td>
<td><p>2010年5月25日[4]</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/卡姆拉·珀塞德－比塞薩爾.md" title="wikilink">卡姆拉·珀塞德－比塞薩爾</a>[5]</p></td>
<td><p><em>/</em>[6]</p></td>
<td><p>2010年5月26日[7]</p></td>
<td><p>2011年10月28日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/茱莉婭·吉拉德.md" title="wikilink">茱莉婭·吉拉德</a></p></td>
<td></td>
<td><p>總理</p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2011.md" title="wikilink">2011</a></p></td>
<td><p>2011年10月28日</p></td>
<td><p>2013年6月27日</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/陸克文.md" title="wikilink">陸克文</a></p></td>
<td><p><em>/</em></p></td>
<td><p>2013年6月27日</p></td>
<td><p>2013年9月18日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/托尼·阿博特.md" title="wikilink">托尼·阿博特</a></p></td>
<td><p><em>/</em></p></td>
<td><p>2013年9月18日</p></td>
<td><p>2013年11月15日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/馬欣達·拉賈帕克薩.md" title="wikilink">馬欣達·拉賈帕克薩</a></p></td>
<td></td>
<td><p><a href="../Page/斯里蘭卡總統.md" title="wikilink">總統</a></p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2013.md" title="wikilink">2013</a></p></td>
<td><p>2013年11月15日</p></td>
<td><p>2015年1月8日</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/邁特里帕拉·西里塞納.md" title="wikilink">邁特里帕拉·西里塞納</a></p></td>
<td><p><em>/</em></p></td>
<td><p>2015年1月9日</p></td>
<td><p>2015年11月27日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/約瑟夫·穆斯卡特.md" title="wikilink">約瑟夫·穆斯卡特</a></p></td>
<td></td>
<td><p>總理</p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2015.md" title="wikilink">2015</a></p></td>
<td><p>2015年11月27日</p></td>
<td><p>2018年4月19日</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/文翠珊.md" title="wikilink">文翠珊</a></p></td>
<td></td>
<td><p><a href="../Page/英國首相.md" title="wikilink">首相</a></p></td>
<td><p><a href="../Page/Commonwealth_Heads_of_Government_Meeting_2018.md" title="wikilink">2018</a></p></td>
<td><p>2018年4月19日</p></td>
<td><p><em>在仼</em></p></td>
</tr>
</tbody>
</table>

## 现成員國（依英文字母排列）

[Commonwealth_realms_republics_and_monarchies.svg](https://zh.wikipedia.org/wiki/File:Commonwealth_realms_republics_and_monarchies.svg "fig:Commonwealth_realms_republics_and_monarchies.svg")的英联邦成员国


\]\]

<table>
<thead>
<tr class="header">
<th><p>成員國[8]</p></th>
<th><p>元首</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/安提瓜和巴布達君主.md" title="wikilink">安提瓜和巴布达君主</a>[9]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/澳洲君主.md" title="wikilink">澳洲君主</a>[10]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/巴哈马君主.md" title="wikilink">巴哈马君主</a>[11]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/孟加拉国总统.md" title="wikilink">孟加拉国总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/巴巴多斯君主.md" title="wikilink">巴巴多斯君主</a>[12]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/伯利兹君主.md" title="wikilink">伯利茲君主</a>[13]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/博茨瓦纳总统.md" title="wikilink">博茨瓦纳总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/文莱苏丹列表.md" title="wikilink">文莱苏丹</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/喀麦隆总统.md" title="wikilink">喀麦隆总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/加拿大君主.md" title="wikilink">加拿大君主</a>[14]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/塞浦路斯总统.md" title="wikilink">塞浦路斯总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/多米尼克总统.md" title="wikilink">多米尼克总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/斐济总统.md" title="wikilink">斐济总统</a></p></td>
</tr>
<tr class="even">
<td><p>[15]</p></td>
<td><p><a href="../Page/冈比亚总统.md" title="wikilink">冈比亚总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/加纳元首列表.md" title="wikilink">加纳总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/格林纳达君主.md" title="wikilink">格林纳达君主</a>[16]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/圭亚那总统.md" title="wikilink">圭亚那总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/印度總統.md" title="wikilink">印度总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/牙買加君主.md" title="wikilink">牙买加君主</a>[17]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/肯尼亚总统.md" title="wikilink">肯尼亚总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/基里巴斯總統.md" title="wikilink">基里巴斯总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/賴索托國王.md" title="wikilink">莱索托国王</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/马拉维总统.md" title="wikilink">马拉维总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/马来西亚最高元首.md" title="wikilink">马来西亚最高元首</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/马耳他总统.md" title="wikilink">馬耳他總統</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/毛里裘斯总统.md" title="wikilink">毛里求斯总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/莫桑比克总统.md" title="wikilink">莫桑比克总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/納米比亞總統.md" title="wikilink">纳米比亚总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/諾魯總統.md" title="wikilink">瑙鲁总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/紐西蘭君主.md" title="wikilink">新西兰君主</a>[18]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/奈及利亞總統.md" title="wikilink">尼日利亚总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/巴基斯坦總統.md" title="wikilink">巴基斯坦总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/巴布亞新畿內亞君主.md" title="wikilink">巴布亚新几内亚君主</a>[19]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/卢旺达总统.md" title="wikilink">卢旺达总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/圣基茨和尼维斯君主.md" title="wikilink">圣基茨和尼维斯君主</a>[20]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/圣卢西亚君主.md" title="wikilink">圣卢西亚君主</a>[21]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/聖文森及格林納丁君主.md" title="wikilink">圣文森特和格林纳丁斯君主</a>[22]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/薩摩亞國家元首.md" title="wikilink">萨摩亚国家元首</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/塞舌尔总统.md" title="wikilink">塞舌尔总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/獅子山總統.md" title="wikilink">塞拉利昂总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/新加坡總統.md" title="wikilink">新加坡总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/所羅門群島君主.md" title="wikilink">所罗门群岛君主</a>[23]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/南非总统.md" title="wikilink">南非总统</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/斯里蘭卡總統.md" title="wikilink">斯里兰卡总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/史瓦濟蘭王國國王列表.md" title="wikilink">斯威士兰国王</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/坦桑尼亚总统列表.md" title="wikilink">坦桑尼亚总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/東加國王.md" title="wikilink">汤加国王</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/千里達及托巴哥總統.md" title="wikilink">特立尼达和多巴哥总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/圖瓦盧君主.md" title="wikilink">图瓦卢君主</a>[24]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/烏干達總統.md" title="wikilink">乌干达总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/英国君主.md" title="wikilink">英国君主</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/瓦努阿图总统.md" title="wikilink">瓦努阿图总统</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/赞比亚总统列表.md" title="wikilink">赞比亚总统</a></p></td>
</tr>
</tbody>
</table>

## 特別成員

以下國家（地區）在歷史上和英國均沒有任何關係：

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>原為<a href="../Page/葡萄牙.md" title="wikilink">葡萄牙在</a><a href="../Page/非洲.md" title="wikilink">非洲的</a><a href="../Page/葡屬東非.md" title="wikilink">殖民地</a>，毗鄰<a href="../Page/南非.md" title="wikilink">南非</a>，於1995年11月13日以特殊身份加入。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>原为德国和比利时殖民地，因毗鄰各國皆為該組織成員，於2009年11月28日加入。</p></td>
</tr>
</tbody>
</table>

## 成员国属地

英联邦成员国中的英国、澳大利亚和新西兰分别拥有一些属地或联系国，这些地区也属于英联邦的管辖范围之内。三国在南极洲行驶主权的权利受[南极条约限制](../Page/南极条约.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>主權国</p></th>
<th><p>属地或海外領土</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li><p>（有争议）</p></li>
<li><p>（有爭議，未被普遍承認）</p></li>
<li></li>
<li></li>
<li><p>（有爭議）</p></li>
<li></li>
<li></li>
<li></li>
<li><ul>
<li></li>
<li></li>
<li></li>
</ul></li>
<li></li>
<li></li>
</ul></td>
<td><p><a href="../Page/英国海外领土.md" title="wikilink">英国海外领土</a></p></td>
</tr>
<tr class="even">
<td><ul>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><p><a href="../Page/皇家屬地.md" title="wikilink">皇家屬地</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li><p>（未被普遍承認）</p></li>
</ul></td>
<td><p><a href="../Page/澳大利亚行政区划.md" title="wikilink">海外領土</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li></li>
<li></li>
</ul></td>
<td><p>紐西蘭<a href="../Page/聯繫邦.md" title="wikilink">聯繫邦</a></p></td>
</tr>
<tr class="odd">
<td><ul>
<li></li>
<li><p>（未被普遍承認）</p></li>
</ul></td>
<td><p>新西兰王国自治領土及屬地</p></td>
<td></td>
</tr>
</tbody>
</table>

## 符合條件並已申請加入英联邦的準會員

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>原为英国殖民地，后于1956年独立</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>南也门原为英国殖民地，后于1967年独立，1990年南北也门统一为也门共和国</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>原为苏丹共和国的一部分，于1956年脱离英国统治，2011年脱离苏丹共和国独立</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1799-1816年期间被英国占领，后英国将苏里南归还给荷兰，于1975年独立</p></td>
</tr>
</tbody>
</table>

## 非英联邦成员的前英国属地

以下国家/地区自脱离英国统治/托管后从未申请加入英联邦

<table>
<thead>
<tr class="header">
<th><p>國家/地区</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>原为英国保护国，后于1971年8月15日独立</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>原为英国保护国，后于1922年2月28日独立，最后一批英军部队于1956年<a href="../Page/第二次中东战争.md" title="wikilink">第二次中东战争后撤出</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>原为意大利殖民地，第二次世界大战后联合国委托英国托管至1951年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>原为英国殖民地，后于1997年7月1日<a href="../Page/香港主權移交.md" title="wikilink">移交中华人民共和国</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>原为奥斯曼帝国领土，第一次世界大战后由国际联盟委托英国管理至1932年10月3日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>原为<a href="../Page/奥斯曼帝国.md" title="wikilink">奥斯曼帝国领土</a>，第一次世界大战后由国际联盟委托英国管理至1948年5月14日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>原为奥斯曼帝国领土，第一次世界大战后由国际联盟委托英国管理至1946年5月25日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>原为英国保护国，后于1961年6月19日独立</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>原为奥斯曼和意大利殖民地，第二次世界大战后<a href="../Page/昔兰尼加.md" title="wikilink">昔兰尼加以及</a><a href="../Page/的黎波里塔尼亚.md" title="wikilink">的黎波里塔尼亚地区由英国占领至</a>1951年12月24日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>原为英国殖民地，后于1948年1月4日独立</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>原为英国保护国，后于1972年独立</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>原为英国保护国，后于1971年9月3日独立</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>原为英国保护国，后于1971年12月2日独立</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/北美十三州.md" title="wikilink">北美十三州原为英国殖民地</a>，<a href="../Page/美国独立战争.md" title="wikilink">美国独立战争打响后于</a>1776年宣布脱离大英帝国独立。</p></td>
</tr>
</tbody>
</table>

## 未被联合国承认的原英国属地

原为英国统治，后独立，但未被联合国承认

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/巴勒斯坦國.md" title="wikilink">巴勒斯坦國曾是</a><a href="../Page/巴勒斯坦託管地.md" title="wikilink">英屬巴勒斯坦托管地之一部份</a>。目前為<a href="../Page/以色列.md" title="wikilink">以色列控制下的自治地區</a>，下轄<a href="../Page/約旦河西岸地區.md" title="wikilink">約旦河西岸和</a><a href="../Page/加沙地帶.md" title="wikilink">加沙地帶</a>，同時是聯合國的觀察員。巴勒斯坦國已經表示有興趣在完全獨立之後加入國協。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>現時國際認可為<a href="../Page/索馬里.md" title="wikilink">索馬里的聯邦成員國</a>。該政權宣稱已經獨立，但並未獲得國際普遍承認；佔有<a href="../Page/索馬里.md" title="wikilink">索馬里原有</a>18個州份的其中5個。曾為<a href="../Page/英屬索馬利蘭.md" title="wikilink">英屬索馬里</a>。该国与<a href="../Page/埃塞俄比亚.md" title="wikilink">埃塞俄比亚和</a><a href="../Page/欧盟.md" title="wikilink">欧盟有非正式关系</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>現時國際認可為<a href="../Page/賽普勒斯.md" title="wikilink">塞浦路斯共和国的领土一部分</a>。該政權宣稱已經獨立，但並未獲得國際普遍承認（仅获取<a href="../Page/土耳其.md" title="wikilink">土耳其一国的正式承认</a>）；佔有<a href="../Page/塞浦路斯岛.md" title="wikilink">塞浦路斯岛面积的三分之一</a>。曾為英国殖民地。</p></td>
</tr>
</tbody>
</table>

## 退出英联邦的国家

以下國家原为英联邦成员国，但后来因不同原因退出英联邦

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>原为英联邦创始成员国及大英帝国自治领，1949年爱尔兰共和国法案通过后丧失英联邦成员国资格。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>英联邦在2002年时宣布对<a href="../Page/津巴布韋.md" title="wikilink">津巴布韋停权一年处分</a>，<a href="../Page/津巴布韋.md" title="wikilink">津巴布韋则于不久之后主动退出作为回应</a>。2018年，穆加貝政府倒台後的新政府申請重新加入英聯邦</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>2016年10月13日，马尔代夫政府认为英联邦干涉其内政而宣布退出英联邦。2018年12月6日，马尔代夫议会投票通过重返英联邦的提议。</p></td>
</tr>
</tbody>
</table>

## 曾经退出英联邦的国家

以下国家目前为英联邦成员国，但曾经因不同原因退出过英联邦

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>1961年<a href="../Page/南非联邦.md" title="wikilink">南非联邦修宪成为共和国后退出英联邦</a>，1994年<a href="../Page/曼德拉.md" title="wikilink">曼德拉上台后重新加入英联邦</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>巴基斯坦曾于1972年退出英联邦，后于1989年重回英联邦</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1987年斐济发生军事政变后退出英联邦，后于1997年重新加入</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2013年时任冈比亚总统<a href="../Page/叶海亚·贾梅.md" title="wikilink">叶海亚·贾梅认为英联邦是</a><a href="../Page/新殖民主义.md" title="wikilink">新殖民主义而退出</a>，贾梅下台后冈比亚于2018年重新加入英联邦[25]</p></td>
</tr>
</tbody>
</table>

## 其他國家

由於在1997年後，加入大英國協的標準有所改變，各[成員國認為必須拒絕这些国家的申請](../Page/大英帝國.md "wikilink")。
其中不少國家已表示有興趣加入國協及征求有關資料。儘管這些國家過去沒有被英國統治，不符合現時的入會標準。然而，隨著申請標準正在被重新檢視，他們可能成為大英國協的未來成員。
\[26\]

  -
  - ：已申請加入國協

  - \[27\]

  - \[28\]

  - \[29\]

  - \[30\]

  - ：已申請加入國協

  - \[31\]

## 參考文獻

## 外部链接

  - [國協（組織）官方網站](http://www.thecommonwealth.org)

## 參見

  - [大英帝国](../Page/大英帝国.md "wikilink")
  - [邦联制](../Page/邦联制.md "wikilink")
      - [共主邦聯](../Page/共主邦聯.md "wikilink")
          - [英聯邦王國](../Page/英聯邦王國.md "wikilink")
  - [英聯邦元首](../Page/英聯邦元首.md "wikilink")
  - [英聯邦運動會](../Page/英聯邦運動會.md "wikilink")
  - [英聯邦日](../Page/英聯邦日.md "wikilink")

{{-}}

[Category:邦联](../Category/邦联.md "wikilink")
[Category:政府间国际组织](../Category/政府间国际组织.md "wikilink")
[英聯邦](../Category/英聯邦.md "wikilink")
[Category:英國外交](../Category/英國外交.md "wikilink")

1.

2.

3.

4.
5.

6.
7.
8.

9.  當地元首由英國君主兼任。當地政府通常會委任總督，代表英國君主。

10.
11.
12.
13.
14.
15.

16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.

27.

28.
29.
30.

31.