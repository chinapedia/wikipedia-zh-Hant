為[英語發音的](../Page/英語.md "wikilink") "[SAMPA](../Page/SAMPA.md "wikilink")
簡明版"。

請參見[歐洲語言完整發音](../Page/歐洲語言.md "wikilink")[SAMPA表](../Page/SAMPA表.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>SAMPA: 英語 <a href="../Page/辅音.md" title="wikilink">辅音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>SAMPA</p></td>
</tr>
<tr class="even">
<td><p><code>p</code></p></td>
</tr>
<tr class="odd">
<td><p><code>b</code></p></td>
</tr>
<tr class="even">
<td><p><code>t</code></p></td>
</tr>
<tr class="odd">
<td><p><code>d</code></p></td>
</tr>
<tr class="even">
<td><p><code>tS</code></p></td>
</tr>
<tr class="odd">
<td><p><code>dZ</code></p></td>
</tr>
<tr class="even">
<td><p><code>k</code></p></td>
</tr>
<tr class="odd">
<td><p><code>g</code></p></td>
</tr>
<tr class="even">
<td><p><code>f</code></p></td>
</tr>
<tr class="odd">
<td><p><code>v</code></p></td>
</tr>
<tr class="even">
<td><p><code>T</code></p></td>
</tr>
<tr class="odd">
<td><p><code>D</code></p></td>
</tr>
<tr class="even">
<td><p><code>s</code></p></td>
</tr>
<tr class="odd">
<td><p><code>z</code></p></td>
</tr>
<tr class="even">
<td><p><code>S</code></p></td>
</tr>
<tr class="odd">
<td><p><code>Z</code></p></td>
</tr>
<tr class="even">
<td><p><code>h</code></p></td>
</tr>
<tr class="odd">
<td><p><code>m</code></p></td>
</tr>
<tr class="even">
<td><p><code>n</code></p></td>
</tr>
<tr class="odd">
<td><p><code>N</code></p></td>
</tr>
<tr class="even">
<td><p><code>l</code></p></td>
</tr>
<tr class="odd">
<td><p><code>r</code></p></td>
</tr>
<tr class="even">
<td><p><code>w</code></p></td>
</tr>
<tr class="odd">
<td><p><code>j</code></p></td>
</tr>
<tr class="even">
<td><p><code>W</code></p></td>
</tr>
<tr class="odd">
<td><p><code>x</code></p></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>SAMPA: 英語<a href="../Page/元音.md" title="wikilink">元音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>SAMPA</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公认发音.md" title="wikilink">RP</a></p></td>
</tr>
<tr class="odd">
<td><p><code>A:</code></p></td>
</tr>
<tr class="even">
<td><p><code>i:</code></p></td>
</tr>
<tr class="odd">
<td><p><code>I</code></p></td>
</tr>
<tr class="even">
<td><p><code>E</code></p></td>
</tr>
<tr class="odd">
<td><p><code>3:</code></p></td>
</tr>
<tr class="even">
<td><p><code>{</code></p></td>
</tr>
<tr class="odd">
<td><p><code>A:</code></p></td>
</tr>
<tr class="even">
<td><p><code>V</code></p></td>
</tr>
<tr class="odd">
<td><p><code>Q</code></p></td>
</tr>
<tr class="even">
<td><p><code>O:</code></p></td>
</tr>
<tr class="odd">
<td><p><code>U</code></p></td>
</tr>
<tr class="even">
<td><p><code>u:</code></p></td>
</tr>
<tr class="odd">
<td><p><code>@</code></p></td>
</tr>
<tr class="even">
<td><p><code>@</code></p></td>
</tr>
</tbody>
</table>
<table>
<thead>
<tr class="header">
<th><p>SAMPA: 英語<a href="../Page/雙元音.md" title="wikilink">雙元音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>SAMPA</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公认发音.md" title="wikilink">RP</a></p></td>
</tr>
<tr class="odd">
<td><p><code>eI</code></p></td>
</tr>
<tr class="even">
<td><p><code>aI</code></p></td>
</tr>
<tr class="odd">
<td><p><code>OI</code></p></td>
</tr>
<tr class="even">
<td><p><code>@U</code></p></td>
</tr>
<tr class="odd">
<td><p><code>aU</code></p></td>
</tr>
<tr class="even">
<td><p><code>I@</code></p></td>
</tr>
<tr class="odd">
<td><p><code>E@</code></p></td>
</tr>
<tr class="even">
<td><p><code>U@</code></p></td>
</tr>
<tr class="odd">
<td><p><code>ju:</code></p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

| SAMPA: 其它的符號見于英語發音之副本上 |
| ---------------------- |
| SAMPA                  |
| `"`                    |
| `%`                    |
| `.`                    |
| `=`                    |

1.  更多的[澳洲英語及](../Page/澳洲英語.md "wikilink")[英格蘭英語之](../Page/英格蘭英語.md "wikilink")[母音討論請參見](../Page/母音.md "wikilink")[Bad-lad分裂](../Page/英語短-A語音史#Bad-lad分裂.md "wikilink")。

2.  更多的[北美英語之母音討論請參見](../Page/北美英語.md "wikilink")[低後舌位音素併合](../Page/低後舌位元音語音史#cot-caught整合.md "wikilink")。

## 參見

  - [單元音](../Page/單元音.md "wikilink")（Monophthong）
  - [三元音](../Page/三元音.md "wikilink")（triphthong）
  - [美國英語口音](../Page/美國英語口音.md "wikilink")（GenAm；General American）
  - [澳洲英語音韻學](../Page/澳洲英語音韻學.md "wikilink")（AuE；Australian English
    phonology）
  - [英格蘭英語](../Page/英格蘭英語.md "wikilink")（[English language in
    England](../Page/:en:English_language_in_England.md "wikilink")）
  - [低後舌位音位併合](../Page/英語低後舌位元音語音史#cot-caught整合.md "wikilink")（low back
    merger；cot-caught整合）
  - [英語低後舌位元音語音史](../Page/英語低後舌位元音語音史.md "wikilink")（Phonological
    history of English low back vowels）
  - [Vocaloid](../Page/Vocaloid.md "wikilink")——V2 及 V3
    的编辑器在处理英语音源（如[巡音](../Page/巡音.md "wikilink")）时使用此记法

[Category:音標](../Category/音標.md "wikilink")