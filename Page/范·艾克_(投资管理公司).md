**范·艾克**（）是位于[美国](../Page/美国.md "wikilink")[纽约市的一家资产管理公司](../Page/纽约市.md "wikilink")。该公司主要从事[共同基金的发行和管理工作](../Page/共同基金.md "wikilink")，同时也经营[对冲基金和独立账户](../Page/对冲基金.md "wikilink")（separate
account）。

## 历史

范·艾克成立于1955年，最初主要从事国际市场的股票投资，是美国最早的国际股票基金管理公司之一。后来重心逐渐转向[贵金属](../Page/贵金属.md "wikilink")、[基本金属](../Page/基本金属.md "wikilink")、[石油](../Page/石油.md "wikilink")、[天然气等重要](../Page/天然气.md "wikilink")[商品的投资](../Page/商品.md "wikilink")。此外，该公司还经营[新兴市场基金](../Page/新兴市场.md "wikilink")、[债券基金](../Page/债券.md "wikilink")、[货币市场基金和](../Page/货币市场.md "wikilink")[ETF等多种针对不同投资者的基金](../Page/ETF.md "wikilink")，有着较齐全的基金产品线。

目前在法兰克福、普費菲孔（瑞士施维茨）、马德里、上海和悉尼设有分公司。

范·艾克管理的资产总额，在2016年底为380亿美元。在美国属于中等规模的基金管理公司。

## 外部連結

  - [VanEck 官方网站](http://www.vaneck.com)

[Category:纽约市公司](../Category/纽约市公司.md "wikilink")
[Category:美国投资管理公司](../Category/美国投资管理公司.md "wikilink")
[Category:1955年成立的公司](../Category/1955年成立的公司.md "wikilink")