**范·達因**（**S. S. Van Dine**），原名**威拉得·亨廷頓·萊特**（**Willard Huntington
Wright**，），是[美國身兼](../Page/美國.md "wikilink")[作家與](../Page/作家.md "wikilink")[評論家](../Page/評論家.md "wikilink")。1920年代，他開始動筆創造了紅極一時的推理小說名偵探—[菲洛·凡斯](../Page/菲洛·凡斯.md "wikilink")（Philo
Vance），並將系列作品推進大銀幕與廣播節目。

## 生平

范·達因是個旅館經營者之子，他的雙親是阿奇博爾德·戴文波特·萊特（Archibald Davenport
Wright）與安妮·范·維奇·萊特（Annie Van Vranken
Wright）。他出生於1888年10月15日的[維吉尼亞州的](../Page/維吉尼亞州.md "wikilink")[查倫斯維爾](../Page/查倫斯維爾.md "wikilink")。曾就讀[聖文森大學](../Page/:en:St._Vincent_College.md "wikilink")、[波莫納大學與](../Page/:en:Pomona_College.md "wikilink")[哈佛大學](../Page/哈佛大學.md "wikilink")。為了學習藝術，他也曾在[慕尼黑與](../Page/慕尼黑.md "wikilink")[巴黎見習並為](../Page/巴黎.md "wikilink")[洛杉磯時報撰寫藝文評論](../Page/:en:Los_Angeles_Times.md "wikilink")。

### 早期寫作

范·達因早期（1910-1919）在職業上的寫作內容有兩種。第一種是撰寫[自然論的相關文章](../Page/:en:Naturalism.md "wikilink")，其小說作品《應許之人》（The
Man of Promise）以及其他短篇故事都屬這一種類的作品，而其編輯的雜誌《The Smart
Set》裡也刊登相似類型的小說。另一種是在1917年，他出版的[*《Misinforming
a
Nation》*](http://www.archive.org/details/misinformingnati00vanduoft)，內容尖銳的抨擊《[大英百科全書](../Page/大英百科全書.md "wikilink")》第十一版的內容不精確和其中的[英國偏見](../Page/英國.md "wikilink")。

### 婚姻

他一生結過兩次婚：在1907年，范·達因與凱瑟琳·貝爾·波頓（Katharine Belle
Boynton）在[美國](../Page/美國.md "wikilink")[華盛頓州的](../Page/華盛頓州.md "wikilink")[西雅圖結為連理](../Page/西雅圖.md "wikilink")。1930年，其再婚對象是肖像畫家埃莉諾·羅勒保（Eleanor
Rulapaugh）。

### 第一部推理小說

在1912到1914年間，他編輯紐約的文學雜誌—《智者》（The Smart Set）。在1915年，他出版《尼采教了什麼？》（What
Nietzsche
Taught），在該書裡他詳細描寫了[尼采的資訊](../Page/尼采.md "wikilink")，並對尼采所有的書籍進行評論。范·達因持續撰寫評論與擔任新聞工作者，直到1923年，性格乖張的他范·達因居然動用公款，去贊助與公司對敵的雜誌刊物，最後終於被開除。之後，據稱他生了一場大病；但是，在約翰·朗利（John
Loughery）的傳記—《所謂的范·達因》（Alias S.S. Van
Dine）\[1\]裡則認為范·達因其實是染上藥癮。至少兩年的時間裡，范·達因被他的醫生要求在床上休養（據稱為心臟病，但實際上也可能是因為他染上古柯鹼）。為了消磨挫折感與漫長的時間，他閱讀、收集並研究了超過兩千部的偵探推理小說，並於1926年出版了他的第一部推理小說《[班森謀殺案](../Page/:en:The_Benson_Murder_Case.md "wikilink")》。在范·達因的推理小說著作裡，前六部作品較受歡迎，其中《[主教謀殺案](../Page/:en:The_Bishop_Murder_Case.md "wikilink")》與《[格林家謀殺案](../Page/:en:The_Greene_Murder_Case.md "wikilink")》得到相當高的評價。而後面的六部作品則銷售慘淡。之後，影響美國推理小說的作家則換成[艾勒里·昆恩接手](../Page/艾勒里·昆恩.md "wikilink")。

### 筆名

范·達因的筆名**S. S. Van Dine**裡，縮寫*' S. S.**指的是"steamship"（輪船），而**Van
Dine*'則被范·達因"自稱"是來自一個古老的姓氏，不過約翰·朗利的傳記裡卻找不到這個古老姓氏存在過的證據。范·達因撰寫了超過11部（還有一部於他死後出版）的懸疑故事，前幾本故事描寫的是愛好美術的業餘偵探[菲洛·凡斯](../Page/菲洛·凡斯.md "wikilink")，這些作品大受好評，讓范·達因嚐到富裕的滋味。

### 後期寫作

他後期的作品並不如以往受到支持，因為他的風格已不再是大眾喜愛的模式。於是，范·達因搬進豪華公寓過著他筆下偵探—凡斯的優雅生活。范·達因於1939年4月11日在紐約去世，隔年，出版了一個不流行的實驗性小說《葛蕾西·艾倫謀殺案》。

### 影響與評價

雖然范·達因確實影響了美國的推理文壇，但是傳記作家約翰·朗利認為范·達因的堀起只是被出版社的公關人員偽造出來的假象。他認為范·達因是個混蛋，喜歡伸手借錢卻不還；此外，他的身體不佳，也是因為自己酗酒嗑藥，更認為他之所以會寫推理小說，也是因為莫可奈何的。結果竟然一炮而紅，名利雙收。然而范·達因，卻未曾想起被他遺棄在加州的妻女，這或許就是萊特氏族譜中永遠看不到范達因的緣故。

曾經為了表彰這位成功的推理小說家，范·達因在《世界偉大偵探故事》（The World's Great Detective Stories,
1928）裡被描述成影響推理小說的重要人物。雖然范·達因之後的表現不如預期，但是他對推理小說的影響力仍然非常顯著。

范·達因也在1930到1931年為[華納兄弟寫了一系列的短篇故事](../Page/華納兄弟.md "wikilink")，這些故事一共被拍成20部短片影集，每部影集約20分鐘。其中，《骷髏謀殺案》（The
Skull Murder Mystery,
1931）更展現了范·達因的創作功力。在該作中更加入了華人角色，這是在那個年代裡較少出現的作品實例。而范·達因的銀幕作品並沒有出版成書，現今也沒有留下的手稿。短片在當代紅極一時，就連[好萊塢也曾經製作過](../Page/好萊塢.md "wikilink")。然而，這些影片已經被人們所遺忘，就連影史記錄裡也難以找到。

## [推理小說二十法則](../Page/推理小說二十法則.md "wikilink")

## 著作

### 推理小說

  - 《班森殺人事件》—— The Benson Murder Case（1926）
  - 《金絲雀殺人事件》—— The Canary Murder Case（1927）
  - 《格林家殺人事件》（格林家命案）—— The Greene Murder Case（1928）
  - 《主教殺人事件》（主教謀殺案）—— The Bishop Murder Case（1929）
  - 《聖甲蟲殺人事件》——The Scarab Murder Case（1930）
  - 《狗園殺人事件》—— The Kennel Murder Case（1931）
  - 《龍殺人事件》—— The Dragon Murder Case（1933）
  - 《賭場殺人事件》—— The Casino Murder Case（1934）
  - 《綁架殺人事件》—— The Kidnap Murder Case（1936）
  - 《花園殺人事件》—— The Garden Murder Case（1937）
  - 《葛蕾西·艾倫殺人事件》—— The Gracie Allen Murder Case（1938）
  - 《冬季殺人事件》—— The Winter Murder Case（1939）

原作除了第11部作品外，所有的書名都統一以「The+6個字母的英文單字+Murder Case」命名。而早期的中文譯名稍顯混亂。

## 參考資料

<references/>

## 外部連結

  - [Biography](http://www.classiccrimefiction.com/vandinebiog.htm)
  - Contemporary Biography:
    [Biography](https://web.archive.org/web/20010608235022/http://www.geocities.com/louisebrookssociety/vandine-bio.html)
  - Bio and Work Analysis:
    [Biography](https://web.archive.org/web/19981201071613/http://members.aol.com/MG4273/vandine.htm)
  - Bibliography of UK first Editions:
    [Bibliography](http://www.classiccrimefiction.com/ssvandinebib.htm)
  - [*What Nietzsche Taught* by Willard Huntington
    Wright](http://books.google.com/books?id=A0-XN6_rXV0C&pg=PA9&dq=inauthor:Friedrich+inauthor:Wilhelm+inauthor:Nietzsche&as_brr=1#PPA3,M1)
  - 推理小說二十法則：[博客來網路書店](http://www.books.com.tw/activity/spy/c_dine_1.htm)

[V](../Category/推理小說作家.md "wikilink")
[V](../Category/美国作家.md "wikilink")
[V](../Category/哈佛大學校友.md "wikilink")

1.