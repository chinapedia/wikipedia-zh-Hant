**范振鋒**（****，），[香港](../Page/香港.md "wikilink")[男演員](../Page/男演員.md "wikilink")、[歌手及](../Page/歌手.md "wikilink")[主持](../Page/主持.md "wikilink")，亦是音樂唱作人，曾就讀[嶺南小學](../Page/嶺南小學.md "wikilink")、[堅尼地城官立中學](../Page/堅尼地城官立中學.md "wikilink")、[蘇浙公學](../Page/蘇浙公學.md "wikilink")\[1\]以及[慈幼英文學校](../Page/慈幼英文學校.md "wikilink")（中學部）。他就讀中學時曾入選校足球隊，並獲得學校推薦報名參加暑期青少年足球推廣活動。後來經[香港體育學院受訓後被挑選入香港少年隊作長期訓練](../Page/香港體育學院.md "wikilink")。范振鋒曾代表香港出賽足球賽事。\[2\]現在於[新城知訊台任職](../Page/新城電台.md "wikilink")[唱片騎師及兼任香港](../Page/唱片騎師.md "wikilink")[無綫電視基本藝人合約藝員](../Page/無綫電視.md "wikilink")。

曾於1994年加入[新城電台至](../Page/新城電台.md "wikilink")1998年，及後離開到台灣發展學習幕後工作，亦曾在香港加入飲食界，開設"水世界"街頭小食店。

曾是[香港知名男子音樂組合](../Page/香港.md "wikilink")[Double.R](../Page/Double.R.md "wikilink")（與[梁奕倫](../Page/梁奕倫.md "wikilink")）成員之一（現已解散），而和[梁奕倫一同主持](../Page/梁奕倫.md "wikilink")[香港電台第二台遊戲節目](../Page/香港電台.md "wikilink")《倫住黎試》和《熱血青年》。但後來因范振鋒過檔新城電台，梁奕倫留守港台，Double.R因此拆夥。

剛好[新城娛樂台向他重新招手而於](../Page/新城娛樂台.md "wikilink")2006年正式重返娘家[新城電台](../Page/新城電台.md "wikilink")，旋即加入成為人氣節目《[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")》的主持人。其後與《[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")》的拍擋[杜浚斌](../Page/杜浚斌.md "wikilink")（阿BEN）及[林盛斌](../Page/林盛斌.md "wikilink")（BOB）組成歌手組合BBR，於2008年發表合唱歌曲《[一人少句](../Page/一人少句.md "wikilink")》。

2011年4月於[微博被發現與藝人](../Page/微博.md "wikilink")[李思欣秘密拍拖四個月](../Page/李思欣.md "wikilink")。2011年9月1日宣佈與[李思欣於](../Page/李思欣.md "wikilink")2012年1月15日舉行婚禮。\[3\]

2012年2月，偕太太李思欣一起參加TVB的[情人節](../Page/情人節.md "wikilink")[遊戲節目](../Page/遊戲節目.md "wikilink")。主持更是其同事兼好拍擋[林盛斌](../Page/林盛斌.md "wikilink")。2013年加入英皇電影，成為旗下藝人。

2016年1月8日，[新城知訊台](../Page/新城知訊台.md "wikilink")「大地震」，范振鋒被轉為只在星期六、日出現的兼職主持，主持新節目《開心大派對》及《豐味旅程》，節目改革前一直主持《開心家天下》（1月8日
最後一集）。

## 電台節目

### 現主持節目

  - [新城知訊台](../Page/新城知訊台.md "wikilink")
      - 《[開心大派對](../Page/開心大派對.md "wikilink")》（2016年1月9日起，逢星期六
        14:00-16:00）
      - 《[豐味旅程](../Page/豐味旅程.md "wikilink")》（2016年1月10日起，逢星期日
        11:00-13:00）

### 曾主持節目

  - [香港電台](../Page/香港電台.md "wikilink")
      - 《[倫住黎試](../Page/倫住黎試.md "wikilink")》
      - 《[熱血青年](../Page/熱血青年.md "wikilink")》
  - [新城知訊台](../Page/新城知訊台.md "wikilink")（前稱[新城娛樂台](../Page/新城娛樂台.md "wikilink")）
      - 《[聯 fan BRA Code](../Page/聯_fan_BRA_Code.md "wikilink")》（逢星期一至五
        16:00-17:00）
      - 《[粵港新鮮報](../Page/粵港新鮮報.md "wikilink")》（與[城市之聲聯播節目](../Page/城市之聲.md "wikilink")）（逢星期一至五
        19:00-20:00）
      - 《[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")》（逢星期一至五 23:00-01:00）
      - 《[生活好國度](../Page/生活好國度.md "wikilink")》（逢星期一至五 18:00-19:00）
      - 《[精明開心自由行](../Page/精明開心自由行.md "wikilink")》（逢星期一至五 10:00-11:00）
      - 《[開心家天下](../Page/開心家天下.md "wikilink")》（逢星期一至五 11:00-13:00）
      - 《[SUN車樂地](../Page/SUN車樂地.md "wikilink")》（逢星期日 12:00-13:00）
      - 《[流行直播室](../Page/流行直播室.md "wikilink")》
  - [新城數碼音樂台](../Page/新城數碼音樂台.md "wikilink")
      - 《[80 後星光燦爛](../Page/80_後星光燦爛.md "wikilink")》

### 曾參與廣播劇

  - 在《[倫住黎試](../Page/倫住黎試.md "wikilink")》播放
      - 2002年 《[今天應該更高興](../Page/今天應該更高興.md "wikilink")》
      - 2003年 《[Love...Please
        Encore](../Page/Love...Please_Encore.md "wikilink")》
      - 2004年 《[正義聯盟 Double
        Eleven](../Page/正義聯盟_Double_Eleven.md "wikilink")》
      - 2004年 《[無可奉告](../Page/無可奉告.md "wikilink")》
      - 2004年 《[青蔥歲月](../Page/青蔥歲月.md "wikilink")》

## 音樂

### 單曲

  - 《愛家‧愛衍生》（2011年）

### 監製

  - 《塗鴉》（主唱：[譚晴](../Page/譚晴.md "wikilink")）（2011年）

### 唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/為你憂鬱的眼睛.md" title="wikilink">為你憂鬱的眼睛</a></p></td>
<td style="text-align: left;"><p>1994年6月</p></td>
<td style="text-align: left;"><ol>
<li>為你憂鬱的眼睛</li>
<li>六月份的海</li>
<li>粉筆情緣</li>
<li>一天一天</li>
<li>為你憂鬱的眼睛（Summer Mix)</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/一生情不絕.md" title="wikilink">一生情不絕</a></p></td>
<td style="text-align: left;"><p>1995年1月</p></td>
<td style="text-align: left;"><ol>
<li>一生情不絕</li>
<li>無限次失眠</li>
<li>哭泣影院</li>
<li>有心（<a href="../Page/張玉珊.md" title="wikilink">張玉珊合唱</a>）</li>
<li>愛戀的起點</li>
<li>愛多一世未變</li>
<li>藍色影子</li>
<li>愛念的纏綿/Crash Boom Bang Malaysia</li>
<li>粉筆情緣</li>
<li>為你憂鬱的眼睛</li>
<li>六月份的海</li>
</ol></td>
</tr>
</tbody>
</table>

### 與Double.R

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/好有好友.md" title="wikilink">好有好友</a></p></td>
<td style="text-align: left;"><p>2002年</p></td>
<td style="text-align: left;"><ol>
<li>電腦檔案，不能播放（Computer Data - Not Playable）</li>
<li>好學友（Music Video）</li>
<li>喊濕枕頭（Music Video）</li>
<li>好學友</li>
<li>喊濕枕頭</li>
<li>喊濕枕頭（劇場版）（獨白：容祖兒）</li>
<li>好學友（音樂版）</li>
<li>喊濕枕頭（音樂版）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/Double.R.md" title="wikilink">Double.R</a></p></td>
<td style="text-align: left;"><p>2003年</p></td>
<td style="text-align: left;"><p>Disc 1</p>
<ol>
<li>安哥之歌（Featuring 許志安）</li>
<li>代言人</li>
<li>天不怕地不怕（梁奕倫 獨唱）</li>
<li>今天應該更高興</li>
<li>Magic</li>
<li>胡桃裡的宇宙（范振鋒 獨唱）</li>
<li>大聲講</li>
<li>十分K</li>
</ol>
<p>Disc 2</p>
<ol>
<li>Computer Data - Not Playable</li>
<li>安哥之歌（Featuring 許志安）（MV）</li>
<li>今天應該更高興（MV）</li>
<li>大聲講（MV）</li>
<li>十分K（MV）</li>
<li>安歌之歌（double.r Version）</li>
<li>今天應該更高興（Piano Version）</li>
<li>大聲講（兒歌Version）</li>
<li>今天應該更高興（新春Version）</li>
<li>Magic（香港青年旅社協會會歌填詞比賽冠軍作品）</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3</p></td>
<td style="text-align: left;"><p><a href="../Page/Radio_Days.md" title="wikilink">Radio Days</a></p></td>
<td style="text-align: left;"><p>2005年</p></td>
<td style="text-align: left;"><ol>
<li>戀愛預告</li>
<li>無可奉告（獨白：丘凱敏）</li>
<li>青蔥歲月</li>
<li>孤單的心痛</li>
<li>少一不可</li>
<li>Radio Days</li>
</ol></td>
</tr>
</tbody>
</table>

### 與新香蕉俱樂部

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/The_Less_The_Better.md" title="wikilink">The Less The Better</a></p></td>
<td style="text-align: left;"><p>2008年</p></td>
<td style="text-align: left;"><ol>
<li>一人少句</li>
<li>大男友</li>
<li>扮傻</li>
<li>想太多</li>
<li>撐撐堂</li>
<li>小丈夫</li>
<li>爆米花</li>
<li>BEN, BOB, RICKY獨白</li>
</ol></td>
</tr>
</tbody>
</table>

## 派台歌曲成績

| **派台歌曲上榜最高位置**                                                   |
| ---------------------------------------------------------------- |
| 唱片                                                               |
| **1994年**                                                        |
| [為你憂鬱的眼睛](../Page/為你憂鬱的眼睛.md "wikilink")                         |
| 為你憂鬱的眼睛                                                          |
| **1995年**                                                        |
| 一生情不絕                                                            |
| **2008年**                                                        |
| [The Less The Better](../Page/The_Less_The_Better.md "wikilink") |
| The Less The Better                                              |
| The Less The Better                                              |
| The Less The Better                                              |
| **2010年**                                                        |
| [Guardian Of Love](../Page/Guardian_Of_Love.md "wikilink")       |
| **2011年**                                                        |
|                                                                  |
| **2015年**                                                        |
|                                                                  |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

  - （\*）表示歌曲仍在該榜上
  - (×) 表示沒有派往該台

## 電視節目

### 現主持節目

  - [TVB](../Page/TVB.md "wikilink")
      - 《[夜宵磨](../Page/夜宵磨.md "wikilink")》
  - [J2](../Page/J2.md "wikilink")
      - 《[Cool Guide](../Page/Cool_Guide.md "wikilink")》
      - 《[兄弟幫](../Page/兄弟幫.md "wikilink")》

### 曾主持節目

  - [無綫娛樂新聞台](../Page/無綫娛樂新聞台.md "wikilink")
      - 《[范後感](../Page/范後感.md "wikilink")》
  - [TVB](../Page/TVB.md "wikilink")
      - 長隆旅遊區渡假特約：家添歡樂逍遙遊「與李思欣合演夫妻」
      - [Big Boys蕩漾夏水禮](../Page/兄弟幫.md "wikilink")

### 曾擔任嘉賓節目

  - [TVB](../Page/TVB.md "wikilink")
      - 《[宇宙無敵獎門人](../Page/宇宙無敵獎門人.md "wikilink")》
      - 《[勁歌金曲](../Page/勁歌金曲.md "wikilink")》
      - 《[My Name Is 邦](../Page/My_Name_Is_邦.md "wikilink")》
      - 《[超級遊戲獎門人](../Page/超級遊戲獎門人.md "wikilink")》
      - 《[千奇百趣Summer Fun](../Page/千奇百趣Summer_Fun.md "wikilink")》
      - 《[筋肉擂台](../Page/筋肉擂台.md "wikilink")》
      - 《[今日VIP](../Page/今日VIP.md "wikilink")》
      - 《[登登登對](../Page/登登登對.md "wikilink")》
      - 《[男人食堂](../Page/男人食堂.md "wikilink")》
      - 《[我愛香港](../Page/我愛香港_\(電視節目\).md "wikilink")》
      - 《[築·動·愛](../Page/築·動·愛.md "wikilink")》

## 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                        |                                              |         |         |
| -------------------------------------- | -------------------------------------------- | ------- | ------- |
| **首播**                                 | **劇名**                                       | **角色**  | **性質**  |
| 2005年                                  | 《[奇幻潮之四人再歸西](../Page/奇幻潮.md "wikilink")》     | David   | 單元第二男主角 |
| 2006年                                  | 《[法證先鋒](../Page/法證先鋒.md "wikilink")》         | 允天機     | 單元角色    |
| 2007年                                  | 《[通天幹探](../Page/通天幹探.md "wikilink")》         | Ray     | 單元角色    |
| 2013年                                  | 《[好心作怪](../Page/好心作怪.md "wikilink")》         | 方自力（青年） | 客串      |
| 2014年                                  | 《[廉政行動2014](../Page/廉政行動2014.md "wikilink")》 | 強佬      | 單元角色    |
| 《[女人俱樂部](../Page/女人俱樂部.md "wikilink")》 | 丹男伴                                          | 客串      |         |
| 《[八卦神探](../Page/八卦神探.md "wikilink")》   | 朱潤輝（Simon）                                   | 單元男主角   |         |
| 2015年                                  | 《[倩女喜相逢](../Page/倩女喜相逢.md "wikilink")》       | 都敏俊     | 男配角     |

## 電視劇（[香港電台](../Page/香港電台.md "wikilink")）

  - 2010年：《[火速救兵](../Page/火速救兵.md "wikilink")》之一秒之差 飾
  - 2012年：《[非凡工程梦](../Page/非凡工程梦.md "wikilink")》 飾 Richard

## 電視劇（其他）

  - 《[百法百中](../Page/百法百中.md "wikilink")》短劇
  - 《[K場曱甴](../Page/K場曱甴.md "wikilink")》網劇

## 電影

  - 2001年：《[買凶拍人](../Page/買凶拍人.md "wikilink")》飾 大佬B手下
  - 2001年：《[煞科](../Page/煞科.md "wikilink")》
  - 2001年：《[困獸](../Page/困獸.md "wikilink")》
  - 2002年：《[枕邊凶靈](../Page/枕邊凶靈.md "wikilink")》
  - 2002年：《[風流家族](../Page/風流家族.md "wikilink")》
  - 2002年：《[我愛一碌葛](../Page/我愛一碌葛.md "wikilink")》
  - 2002年：《[黑道風雲](../Page/黑道風雲.md "wikilink")》
  - 2002年：《[狂野臥底](../Page/狂野臥底.md "wikilink")》
  - 2003年：《[魔宅之鐵鎚凶靈](../Page/魔宅之鐵鎚凶靈.md "wikilink")》
  - 2003年：《[百分百感覺2003](../Page/百分百感覺2003.md "wikilink")》
  - 2004年：《[失驚無神](../Page/失驚無神.md "wikilink")》飾 阿卓
  - 2005年：《[美麗酒吧](../Page/美麗酒吧.md "wikilink")》
  - 2008年：《[六樓后座2家屬謝禮](../Page/六樓后座2家屬謝禮.md "wikilink")》飾 導演（Peter）
  - 2009年：《[家有囍事2009](../Page/家有囍事2009.md "wikilink")》飾 工人
  - 2012年：《[起勢搖滾](../Page/起勢搖滾.md "wikilink")》
  - 2016年：《[寒戰II](../Page/寒戰II.md "wikilink")》

## 編劇

  - 2001年：《[困獸](../Page/困獸.md "wikilink")》

## 著作

  - [七日天堂 七日地獄](../Page/七日天堂_七日地獄.md "wikilink")（2008年）
  - [蕉牌旅遊 拍拖行頭](../Page/蕉牌旅遊_拍拖行頭.md "wikilink")（2009年）
  - [蕉牌旅遊 打出身價坡](../Page/蕉牌旅遊_打出身價坡.md "wikilink")（2010年）

## 參與音樂MV

  - [戴夢夢](../Page/戴夢夢.md "wikilink")：當事人（TVB版）
  - [衛詩](../Page/衛詩.md "wikilink")：Funny Jealousy（TVB版）
  - [戴夢夢](../Page/戴夢夢.md "wikilink")、**范振鋒**：愚不可及（合唱版Offical MV）
  - [梁漢文](../Page/梁漢文.md "wikilink")：中鋒（Offical MV）

及其他**范振鋒**有份主唱之歌曲

## 廣告

  - 必利痛傷風感冒丸
  - 碧河花園樓盤廣告
  - Neway卡啦OK
  - 紅十字會捐血廣告

## 獎項

**2010:**

  - 新城勁爆頒獎禮－新城勁爆合唱歌曲獎《愚不可及》

**2015:**

  - 新城勁爆頒獎禮－《膊頭》

## 注釋

## 參考來源

## 外部链接

  -
[chun](../Category/范姓.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:英皇艺人](../Category/英皇艺人.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:嶺南小學 (香港)校友](../Category/嶺南小學_\(香港\)校友.md "wikilink")
[Category:蘇浙公學校友](../Category/蘇浙公學校友.md "wikilink")

1.
2.
3.