[AlessandroValignano.jpg](https://zh.wikipedia.org/wiki/File:AlessandroValignano.jpg "fig:AlessandroValignano.jpg")
[A-Valignano.jpg](https://zh.wikipedia.org/wiki/File:A-Valignano.jpg "fig:A-Valignano.jpg")
**范礼安**（，），[耶稣会意大利籍传教士](../Page/耶稣会.md "wikilink")。其原名[中文直译为](../Page/汉语.md "wikilink")**亞歷山德羅·范禮納諾**，范礼安是他的中文名字。他是继[沙勿略之后](../Page/沙勿略.md "wikilink")，又一位对天主教在[中国传播有重要影响的人物](../Page/中国.md "wikilink")。

## 生平

1539年2月15日出生於[那不勒斯王國](../Page/那不勒斯王國.md "wikilink")（今屬[意大利](../Page/意大利.md "wikilink")）[基耶蒂](../Page/基耶蒂.md "wikilink")（Chieti）。
1566年就讀[帕多瓦大學期間加入](../Page/帕多瓦大學.md "wikilink")[耶稣会](../Page/耶稣会.md "wikilink")。1573年，被任命为耶稣会远东观察员视察[澳门教会](../Page/澳门.md "wikilink")。1578年9月，由[印度](../Page/印度.md "wikilink")[果亞抵達](../Page/果亞.md "wikilink")[澳門](../Page/澳門.md "wikilink")。当时澳门的一些耶稣会传教士要求他们的中国信徒一律要学[葡萄牙语](../Page/葡萄牙语.md "wikilink")，取葡萄牙名字，生活方式也葡萄牙化。
范礼安认为，这种方式不符合传播宗教的原则。他认为应该是传教士中国化，而不是中国人葡萄牙化，才有利于天主教的发展。于是他要求传教士们学习中国语言，采用中国风俗。1579年7月，他从[印度调来的传教士](../Page/印度.md "wikilink")[罗明坚抵達澳門](../Page/罗明坚.md "wikilink")，這時他已離開澳門前往[日本](../Page/日本.md "wikilink")，他留下指示要求罗明坚学习中国语言。1582年3月范禮安神父由日本回到澳門，在澳門成立[耶穌聖名團](../Page/耶穌聖名團.md "wikilink")。1582年8月范禮安神父再從印度調來一批傳教士，这里面就有[巴范济和](../Page/巴范济.md "wikilink")[利玛窦](../Page/利玛窦.md "wikilink")；同月，他離開澳門前往印度果亞。1588年7月，他從果亞回到澳門，同時也帶來了當時歐洲最先進的[古騰堡印刷機](../Page/古騰堡印刷機.md "wikilink")，這是歐洲印刷術首次傳入中國。1594年在澳門建立了中國境內第一所西式大學——[聖保祿學院](../Page/聖保祿學院.md "wikilink")。1606年1月20日在澳門逝世。

## 参考文献

  - Domingos Maurício Gomes dos Santos, S.J.《澳門遠東第一所西方大學》
  - 文德泉(P.e Manuel Teixeira) 《四百週年——澳門印刷業》1988《文化雜誌》中文版第六期
  - 戚印平《遠東耶穌會史研究》，中華書局，2007

## 外部連結

  - [《中国基督教史纲》王治心著](http://www.cclw.net/book/zgjdjhsg/)，[基督徒生活网](http://www.cclw.net)。

## 參見

  - [西學東漸](../Page/西學東漸.md "wikilink")、[明朝天主教](../Page/明朝天主教.md "wikilink")

[F](../Category/意大利传教士.md "wikilink")
[F](../Category/在华天主教传教士.md "wikilink")
[F](../Category/耶稣會會士.md "wikilink")
[F范禮安](../Category/澳門中外交流史.md "wikilink")
[Category:耶穌會會士](../Category/耶穌會會士.md "wikilink")
[Category:江戶時代外國人](../Category/江戶時代外國人.md "wikilink")
[Category:戰國時代人物 (日本)](../Category/戰國時代人物_\(日本\).md "wikilink")
[Category:安土桃山時代人物](../Category/安土桃山時代人物.md "wikilink")
[Category:阿布鲁佐大区人](../Category/阿布鲁佐大区人.md "wikilink")
[Category:帕多瓦大學校友](../Category/帕多瓦大學校友.md "wikilink")
[Category:天主教神父](../Category/天主教神父.md "wikilink")