**范萱蔚**（**Percy
Fan**，），在2004年參與[香港商業電台](../Page/香港商業電台.md "wikilink")「[森美](../Page/森美.md "wikilink")[小儀歌劇團](../Page/小儀.md "wikilink")」的[舞台劇](../Page/舞台劇.md "wikilink")「早安啊！曼克頓」的演出後被現時經理人公司賞識，從而簽約成為[Starj
&
Snazz娛樂旗下藝人](../Page/Starj_&_Snazz娛樂.md "wikilink")，曾於2006年參加[無綫電視](../Page/無綫電視.md "wikilink")（TVB）綜藝娛樂節目《[美女廚房](../Page/美女廚房_\(第一輯\).md "wikilink")》第一輯第十集，並於2007年晉身樂壇。另外，范萱蔚被香港人際口才學會選為「青年大使」，曾與香港人際口才學會合作到各中學進行一個名為「個人表達之青年力量互動講座」，與同學分享與人溝通的技巧。她曾奪得學界[排球比賽銀牌](../Page/排球.md "wikilink")。她的師妹為中日[混血兒](../Page/混血兒.md "wikilink")[裕美](../Page/裕美.md "wikilink")。范萱蔚現已轉行為[化妝師](../Page/化妝師.md "wikilink")。

## 曾參與的電視劇

  - [香港電台電視劇](../Page/香港電台.md "wikilink")「[一家人](../Page/一家人.md "wikilink")——人魚故事」
  - 香港電台電視劇「[海關故事](../Page/海關故事.md "wikilink")——迷迭香行動」 飾 安仔
  - [無綫電視電視劇](../Page/無綫電視.md "wikilink")「[尖子攻略](../Page/尖子攻略.md "wikilink")」
    飾 林亦雙

## 曾參與的電視廣告

  - 實惠旅遊電視廣告
  - 集美郵輪電視廣告
  - 彩福皇宴電視廣告

## 曾參與的舞台劇

  - 2004年[商業電台](../Page/商業電台.md "wikilink")[森美](../Page/森美.md "wikilink")[小儀歌劇團](../Page/小儀.md "wikilink")──早安啊！曼克頓

## 硬照廣告

  - Aso Spa
  - Promise Beauty Car Interior Design
  - 龍皇酒家
  - 集美郵輪
  - 佬沃海景假日酒店及克拉克渡假城平面廣告

## 曾參與的MV

  - 你是我的石頭（Percy）
  - 我買自己（Percy）
  - 邊愛邊罵（Percy與[林泳](../Page/林泳.md "wikilink")）
  - 自卑男（林泳）
  - 京華春夢（[EO2](../Page/EO2.md "wikilink")）
  - 大節日（與EO2、[高皓正](../Page/高皓正.md "wikilink")、[鄧穎芝](../Page/鄧穎芝.md "wikilink")、[裕美](../Page/裕美.md "wikilink")、[唐寧](../Page/唐寧.md "wikilink")、[賈曉晨合唱](../Page/賈曉晨.md "wikilink")）
  - 電光火石（[鄧穎芝](../Page/鄧穎芝.md "wikilink")\&Eddie@EO2）
  - 一起愛（Percy與裕美）
  - 少女大帝（Percy與裕美）
  - 我們就是力量(Percy與EO2、鄧穎芝、裕美、賈曉晨)
  - 一次心動（Percy與裕美）
  - 為寂寞拍拖（Percy與[高皓正](../Page/高皓正.md "wikilink")）
  - 冬甩（Percy）
  - 比武招親（Percy）
  - 我們結婚了（Percy與[葉文輝](../Page/葉文輝.md "wikilink")）
  - 廿二世紀少女（Percy）

## 其他演出

  - 2006 [鳳凰衛視十周年短片](../Page/鳳凰衛視.md "wikilink")“鳳凰戰隊──第十話”
  - 2008 TVB節目《[美女廚房](../Page/美女廚房.md "wikilink")》「美少女廚神」之一
  - 2008 TVB節目《[鐵甲無敵獎門人](../Page/鐵甲無敵獎門人.md "wikilink")》獎門人200集超級派對 嘉賓
  - 2010 TVB節目《[智激校園](../Page/智激校園.md "wikilink")》嘉賓
  - 2013 [情義兩難全之玩爆你個腎「求婚篇」- "爆腎團隊攪氣氛 ·
    夾埋小三玩求婚"](http://www.youtube.com/watch?v=pQeMcslwMFs)

## 歌曲

  - 邊愛邊罵（與[林泳合唱](../Page/林泳.md "wikilink")）
  - 你是我的石頭
  - 大節日（與[EO2](../Page/EO2.md "wikilink")、[高皓正](../Page/高皓正.md "wikilink")、[唐寧](../Page/唐寧.md "wikilink")、[鄧穎芝](../Page/鄧穎芝.md "wikilink")、[賈曉晨](../Page/賈曉晨.md "wikilink")、[裕美合唱](../Page/裕美.md "wikilink")）
  - 我買自己
  - 一起愛（與[裕美合唱](../Page/裕美.md "wikilink")）
  - 少女大帝（與[裕美合唱](../Page/裕美.md "wikilink")）
  - 為寂寞拍拖（與[高皓正合唱](../Page/高皓正.md "wikilink")）
  - 我們就是力量（與[EO2](../Page/EO2.md "wikilink")、[鄧穎芝](../Page/鄧穎芝.md "wikilink")、[賈曉晨](../Page/賈曉晨.md "wikilink")、[裕美合唱](../Page/裕美.md "wikilink")）
  - 一次心動（與[裕美合唱](../Page/裕美.md "wikilink")）
  - 冬甩
  - 義出必行（與[葉文輝](../Page/葉文輝.md "wikilink")、[狄易達](../Page/狄易達.md "wikilink")、[高皓正](../Page/高皓正.md "wikilink")、[裕美](../Page/裕美.md "wikilink")、[劉家麗](../Page/劉家麗.md "wikilink")、[何卓瑩](../Page/何卓瑩.md "wikilink")
    合唱)
  - 你的小天使
  - 全宇宙過聖誕（與[葉文輝](../Page/葉文輝.md "wikilink")、[賈曉晨](../Page/賈曉晨.md "wikilink")、[狄易達](../Page/狄易達.md "wikilink")、[區俊濤](../Page/區俊濤.md "wikilink")、[張苡澂](../Page/張苡澂.md "wikilink")、[Winky](../Page/Winky.md "wikilink")、[Ava](../Page/Ava.md "wikilink")、[Winnie](../Page/Winnie.md "wikilink")、[Yellow合唱](../Page/Yellow.md "wikilink"))
  - 比武招親
  - 我們結婚了（與[葉文輝合唱](../Page/葉文輝.md "wikilink"))
  - 廿二世紀少女

## 唱片

  - 《[一起愛](../Page/一起愛.md "wikilink")》EP（推出日期：2008年1月29日）
  - 《[少女大帝](../Page/少女大帝.md "wikilink")》（與[裕美推出的合輯](../Page/裕美.md "wikilink")）（推出日期：2008年8月25日）

## 獎項

  - PM第五屆樂壇頒獎禮〈PM06讀者喜愛合唱歌曲〉──邊愛邊罵
  - 第五屆《勁歌王》〈最有前途女新人〉
  - 第五屆《勁歌王》〈最受歡迎合唱歌〉──一起愛
  - 新城勁爆兒歌頒獎禮2008 十四首勁爆兒歌之一：你的小天使

## 外部連結

  - [范萱蔚網誌](http://hk.myblog.yahoo.com/perpercy)
  - [范萱蔚訪問聲音及短片](http://pinkmessage.com/shortnews/percy.htm)
  - [范萱蔚相片,短片, 按名字](http://www.pinkmessage.com/talk/viewforum.php?f=23)
  - [范萱蔚的博客-我的藝術家空間](http://www.alivenotdead.com/percyfan)
  - [Percy
    范萱蔚blog](http://www.alivenotdead.com/percyfan)@[Bee.WEBZINE](https://web.archive.org/web/20090426163635/http://bee.newmonday.com.hk/)
  - [裕美開騷晚晚發噩夢](http://orientaldaily.on.cc/cnt/entertainment/20111201/00282_046.html)

[Category:香港电视女演員](../Category/香港电视女演員.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:化妝師](../Category/化妝師.md "wikilink")
[H](../Category/范姓.md "wikilink")