**范質**（），中国[五代](../Page/五代.md "wikilink")[后周和北宋初大臣](../Page/后周.md "wikilink")。字文素。[赵匡胤](../Page/赵匡胤.md "wikilink")“[陈桥兵变](../Page/陈桥兵变.md "wikilink")”后，任[北宋](../Page/北宋.md "wikilink")[宰相](../Page/宰相.md "wikilink")。

## 生平

自幼好學，九歲能文，十三歲誦[五經](../Page/五經.md "wikilink")，博學多聞。[後唐](../Page/後唐.md "wikilink")[長興四年](../Page/长兴_\(李嗣源\).md "wikilink")（933年）進士，官至戶部侍郎。後周太祖[郭威自鄴起兵入京](../Page/郭威.md "wikilink")，范質為避戰禍，藏匿民間，後來被太祖找到，時值嚴冬，郭威脫下外袍給范質披上。封范質為[兵部侍郎](../Page/兵部侍郎.md "wikilink")，[樞密副使](../Page/樞密副使.md "wikilink")。周顯德四年（957年），范質上書朝廷，建議重修法令，編定[后周的](../Page/后周.md "wikilink")《显德刑律统类》。宋代第一部法典《[宋刑統](../Page/宋刑統.md "wikilink")》直接来源于此法典。

顯德六年（959年），周世宗病危，臨終[託孤](../Page/託孤.md "wikilink")，命范質為[顧命大臣](../Page/顧命大臣.md "wikilink")，輔佐七歲的恭帝柴宗訓。封為蕭國公。顯德七年（960年）正月初一，忽傳[北漢](../Page/北漢.md "wikilink")、[契丹聯兵南下](../Page/契丹.md "wikilink")，令趙匡胤統帥[禁軍北上](../Page/禁軍.md "wikilink")。初三趙匡胤陳橋兵變還京，范質率[王溥](../Page/王溥.md "wikilink")、[魏仁浦責問趙匡胤](../Page/魏仁浦.md "wikilink")，帳前[羅彥環拔劍厲聲](../Page/羅彥環.md "wikilink")：“三軍無主，眾將議立檢點為天子，再有異言者斬！”。王溥面如土色，被迫擁立趙為天子。曾舉薦[趙普](../Page/趙普.md "wikilink")、呂慶餘、竇儀等棟樑之臣。宋乾德元年（963年），封范質為魯國公。乾德二年（964年）正月，与王溥、魏仁浦同日罢相。是年（964年）九月，去世，臨終時“戒其后勿请谥立碑，自悔深矣”\[1\]，宋太祖說：「聞范質只有宅第，不置田產，真宰相也。」范質在趙匡胤稱帝後「降階受命」，有負世宗所托，[趙光義說他](../Page/趙光義.md "wikilink")：「循規矩，慎名器，持廉節，無出質右者。但欠世宗一死，為可惜耳。」著有《范魯公集》、《五代通錄》等。

## 家屬

  - [范旻](../Page/范旻.md "wikilink") ：子。
  - [范貽孫](../Page/范貽孫.md "wikilink") ：孫。
  - [范正](../Page/范正.md "wikilink") ：兄弟。
  - [范杲](../Page/范杲.md "wikilink") ：從子。
  - [范晞](../Page/范晞.md "wikilink") ：從子。
  - [范坦](../Page/范坦_\(范質\).md "wikilink") ：從孫。

## 注釋

## 參考書目

  -
  - 《[國老談苑](../Page/國老談苑.md "wikilink")》卷一

[Category:五代十国政治人物](../Category/五代十国政治人物.md "wikilink")
[Category:宋朝政治人物](../Category/宋朝政治人物.md "wikilink")
[Category:宋朝宰相](../Category/宋朝宰相.md "wikilink")
[Category:後周宰相](../Category/後周宰相.md "wikilink")
[Z](../Category/范姓.md "wikilink")
[Category:北宋政治人物](../Category/北宋政治人物.md "wikilink")

1.  《资治通鉴》卷294，显德六年六月癸巳。