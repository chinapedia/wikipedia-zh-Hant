《**茉莉人生**》（，直譯为[波斯波利斯](../Page/波斯波利斯.md "wikilink")）是一部2007年上映的[動畫](../Page/動畫.md "wikilink")[電影](../Page/電影.md "wikilink")，根據[瑪嘉·莎塔碧所創作的](../Page/瑪嘉·莎塔碧.md "wikilink")[同名的漫畫所改編的](../Page/波斯波利斯_\(漫畫\).md "wikilink")。

## 劇情簡介

瑪嘉是一位異想天開的9歲[少女](../Page/少女.md "wikilink")，出身于[德黑兰的一个](../Page/德黑兰.md "wikilink")[左翼知识分子家庭](../Page/左翼.md "wikilink")，外祖父是[卡扎尔王朝的一个王子](../Page/卡扎尔王朝.md "wikilink")，也是一个[共产主义者](../Page/共产主义者.md "wikilink")，最后死在[伊朗沙阿的监狱中](../Page/伊朗君主列表.md "wikilink")，父母都在政治上支持[马克思主义事业并反对](../Page/马克思主义.md "wikilink")[巴列维国王的统治](../Page/穆罕默德-礼萨·巴列维.md "wikilink")，最敬爱的阿鲁什叔叔则是信仰[共产主义的](../Page/共产主义.md "wikilink")[伊朗人民党的成员](../Page/伊朗人民党.md "wikilink")，尝尽[流亡和坐牢的苦头](../Page/流亡.md "wikilink")。她喜愛[西方文化](../Page/西方文化.md "wikilink")，酷愛偶像[李小龍](../Page/李小龍.md "wikilink")。[真主和](../Page/真主.md "wikilink")[马克思是她的密友](../Page/马克思.md "wikilink")，成天愛幻想，夢想是成為全[宇宙最後一位](../Page/宇宙.md "wikilink")[先知](../Page/先知.md "wikilink")。[伊朗伊斯兰革命后](../Page/伊朗伊斯兰革命.md "wikilink")，[伊斯兰原教旨主义者掌握了大权](../Page/伊斯兰原教旨主义.md "wikilink")，在国内实行[政教合一制度](../Page/政教合一.md "wikilink")，推行极其保守的社会政策，严格控制西方文化传播，在学校强制灌输[伊斯兰教义](../Page/伊斯兰教.md "wikilink")，女性被迫戴上头巾，宗教警察在街头监视着人们的一言一行，饮酒及各种为[伊斯兰教法所不容忍的娱乐都被禁止](../Page/伊斯兰教法.md "wikilink")，阿鲁什叔叔等共产主义者被处决，神权当局的蛊惑驱使大量青年在[两伊战争中成为炮灰](../Page/两伊战争.md "wikilink")。这一切让瑪嘉感到极其压抑。她的叛逆和不安分让雙親和外祖母不时感到担忧，週遭的人也認為她是怪孩子，保守的[伊朗婦女認為她是西方腐敗的象徵](../Page/伊朗.md "wikilink")。她喜愛的一切不被保守的祖國所接受，她只能偷偷地去買[黑膠唱片和所有喜愛的東西](../Page/黑膠唱片.md "wikilink")。原本以為日子可以如此幻想地過下去，直到国内的政治局势继续恶化时，雙親决定把她送到[歐洲](../Page/歐洲.md "wikilink")。在成長的歲月中不斷地發生苦難，在被愛情刺得遍體鱗傷後，在西方的颓废事物让她迷茫时，開始懷念家鄉和雙親，還有外祖母和她身上的[茉莉香](../Page/茉莉.md "wikilink")。她選擇回到家鄉，大家一起尋找快樂彌補過去失去的時光，但當自由的真諦出現在眼前時，她又不得不離鄉去追夢......

瑪嘉·莎塔碧说：

## 技术

影片按照原始的[绘本以黑白作为主色调](../Page/绘本.md "wikilink")。影片中除了“现代”的部分有颜色外，历史记叙的部分采用类似[皮影戏的效果](../Page/皮影戏.md "wikilink")。将[漫画版转变为](../Page/漫画.md "wikilink")[动画版的过程](../Page/动画.md "wikilink")，由艺术指导和执行制片人[Marc
Jousset](../Page/Marc_Jousset.md "wikilink") 重新设计。动画版署名是[Perseprod
Studio](../Page/Perseprod_Studio.md "wikilink")，同时还由[Je Suis Bien
Cotent和](../Page/Je_Suis_Bien_Cotent.md "wikilink")[Pumpkin
3D这两个专业工作室负责制作](../Page/Pumpkin_3D.md "wikilink")。

## 評價

《茉莉人生》受到許多影評人與觀眾給予高度的評價，並在[爛番茄與](../Page/爛番茄.md "wikilink")[Metacritic都得到相當高的分數](../Page/Metacritic.md "wikilink")。

## 獎項

  - [第80届奥斯卡金像奖](../Page/第80届奥斯卡金像奖.md "wikilink")
      - [奥斯卡最佳动画片奖提名](../Page/奥斯卡最佳动画片奖.md "wikilink")

<!-- end list -->

  - [第65届金球奖](../Page/第65届金球奖.md "wikilink")
      - [最佳外语片奖提名](../Page/最佳外语片奖.md "wikilink")

<!-- end list -->

  - [第60屆坎城影展](../Page/第60屆坎城影展.md "wikilink")
      - 获得[評審團獎](../Page/評審團獎.md "wikilink")
      - Nominated for the [Palme d'Or](../Page/Palme_d'Or.md "wikilink")

<!-- end list -->

  - 2007 [欧洲电影奖](../Page/欧洲电影奖.md "wikilink")
      - Nominated for Best Picture

<!-- end list -->

  - 2007 [London Film
    Festival](../Page/London_Film_Festival.md "wikilink")
      - Southerland Trophy (Grand prize of the festival)

<!-- end list -->

  - 2007 [Cinemanila International Film
    Festival](../Page/Cinemanila_International_Film_Festival.md "wikilink")
      - Special Jury Prize

<!-- end list -->

  - 2007 [São Paulo International Film
    Festival](../Page/São_Paulo_International_Film_Festival.md "wikilink")
      - **Won** for Best Foreign Language Film

<!-- end list -->

  - 2007 [Vancouver International Film
    Festival](../Page/Vancouver_International_Film_Festival.md "wikilink")
      - **Won** the Rogers People's Choice Award for Most Popular
        International Film

## 外部連結

  -
  -
[Category:2007年劇情片](../Category/2007年劇情片.md "wikilink")
[Category:2007年動畫電影](../Category/2007年動畫電影.md "wikilink")
[Category:法國傳記片](../Category/法國傳記片.md "wikilink")
[Category:法國劇情片](../Category/法國劇情片.md "wikilink")
[Category:法國動畫電影](../Category/法國動畫電影.md "wikilink")
[Category:漫畫改編電影](../Category/漫畫改編電影.md "wikilink")
[Category:漫畫改編動畫](../Category/漫畫改編動畫.md "wikilink")
[Category:女性題材電影](../Category/女性題材電影.md "wikilink")
[Category:黑白電影](../Category/黑白電影.md "wikilink")
[Category:索尼經典電影](../Category/索尼經典電影.md "wikilink")
[Category:伊斯蘭教先知](../Category/伊斯蘭教先知.md "wikilink")
[Category:伊朗背景電影](../Category/伊朗背景電影.md "wikilink")
[Category:1970年代背景電影](../Category/1970年代背景電影.md "wikilink")
[Category:1980年代背景電影](../Category/1980年代背景電影.md "wikilink")
[Category:1990年代背景電影](../Category/1990年代背景電影.md "wikilink")