[thumbnail](../Page/image:CornusMas_Fruits_01.jpg.md "wikilink")
[Cornelian_cherry_1.JPG](https://zh.wikipedia.org/wiki/File:Cornelian_cherry_1.JPG "fig:Cornelian_cherry_1.JPG")
[ぐみ2.jpg](https://zh.wikipedia.org/wiki/File:ぐみ2.jpg "fig:ぐみ2.jpg")
[Gumi1.JPG](https://zh.wikipedia.org/wiki/File:Gumi1.JPG "fig:Gumi1.JPG")
[Cornus_mas_MHNT.BOT.2015.34.42.jpg](https://zh.wikipedia.org/wiki/File:Cornus_mas_MHNT.BOT.2015.34.42.jpg "fig:Cornus_mas_MHNT.BOT.2015.34.42.jpg")

**茱萸**，又名“樧”、“越椒”、“艾子”，是一种常绿带香的[植物](../Page/植物.md "wikilink")，具备杀虫消毒、逐寒祛风的功能。木本茱萸有[吴茱萸](../Page/吴茱萸.md "wikilink")、[山茱萸和](../Page/山茱萸.md "wikilink")[食茱萸之分](../Page/食茱萸.md "wikilink")，都是著名的[中药](../Page/中药.md "wikilink")。按[中国古人的习惯](../Page/中国.md "wikilink")，在九月九日[重阳节时爬山登高](../Page/重阳节.md "wikilink")，臂上佩带插着茱萸的布袋（古时称“茱萸囊”），以示对亲朋好友的怀念。[唐代诗人](../Page/唐.md "wikilink")[王维在](../Page/王维.md "wikilink")“九月九日忆山东兄弟”诗中曾写道：“遥知兄弟登高处，遍插茱萸少一人。”

## 史籍

茱萸在中国史料及[中医著述中](../Page/中医.md "wikilink")，有藙、樧、枣皮、药枣、蜀枣、蜀酸枣、魁实、石枣、鼠矢、鸡足、汤主、山萸肉、萸肉、肉枣等名称。古人已经认识到了茱萸的药用价值，并把它用于祭祀，还把它作为地方向朝廷进贡的[贡品](../Page/贡品.md "wikilink")。

## 分布

茱萸分布较广，山茱萸、吴茱萸多分布在[安徽](../Page/安徽.md "wikilink")、[陕西](../Page/陕西.md "wikilink")[秦岭](../Page/秦岭.md "wikilink")、[四川](../Page/四川.md "wikilink")、[河南](../Page/河南.md "wikilink")、[福建](../Page/福建.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[江苏等地](../Page/江苏.md "wikilink")；[英国](../Page/英国.md "wikilink")、[美国](../Page/美国.md "wikilink")、[朝鲜](../Page/朝鲜.md "wikilink")、[日本等国也有分布](../Page/日本.md "wikilink")。

## 用途

### 祭祀

《[周礼](../Page/周礼.md "wikilink")·内则》中记载：“三牲用樧。”[清代](../Page/清.md "wikilink")[段玉裁解释道](../Page/段玉裁.md "wikilink")：“樧，煎茱萸。”

### 饰物

中国古人在重阳节时，头插茱萸，登高游兴。

### 药用

[西晋冯翊](../Page/西晋.md "wikilink")（今陕西关中西部）太守孙楚《茱萸赋》一文中写到：“有茱萸之嘉木，植茅茨之前庭，历汉女而始育，关百载而长生。森蔓延以盛兴，布绿叶于紫茎。鹑火西阻，白藏授节，零露既凝，鹰隼飘厉。攀紫房于歉枝，缀朱实之酷烈。应神农之本草，疗生民之疹疾。”中国古代许多医学名著，如晋代《[神农本草经](../Page/神农本草经.md "wikilink")》、唐代[孙思邈](../Page/孙思邈.md "wikilink")《[千金翼方](../Page/千金翼方.md "wikilink")》，以及《[吴晋本草](../Page/吴晋本草.md "wikilink")》、《[健康记](../Page/健康记.md "wikilink")》、《[图经本草](../Page/图经本草.md "wikilink")》等，均记载了茱萸的药用价值。山茱萸之果实山萸肉，味酸涩，性微温，有补肝肾、涩精气、固虚脱、健胃壮阳等功能，中医常用以治疗腰膝酸痛、[眩晕](../Page/眩暈_\(醫學\).md "wikilink")、[耳鸣](../Page/耳鸣.md "wikilink")、[遗精](../Page/遗精.md "wikilink")、[尿频](../Page/尿频.md "wikilink")、肝虚寒热、虚汗不止、心摇脉散、[神经衰弱](../Page/神经衰弱.md "wikilink")、[月经不调等症](../Page/月经不调.md "wikilink")。茱萸还是[中成药](../Page/中成药.md "wikilink")[知柏地黄丸](../Page/知柏地黄丸.md "wikilink")、[益明地黄丸](../Page/益明地黄丸.md "wikilink")、[爱味地黄丸](../Page/爱味地黄丸.md "wikilink")、[六味地黄丸的主药](../Page/六味地黄丸.md "wikilink")。山茱萸含有生理活性较强的[山茱萸甙](../Page/山茱萸甙.md "wikilink")、[马草鞭甙](../Page/马草鞭甙.md "wikilink")、[皂甙](../Page/皂甙.md "wikilink")、[鞣甙](../Page/鞣甙.md "wikilink")，以及丰富的[维生素C等营养成分](../Page/维生素C.md "wikilink")。能抑制[痢疾杆菌](../Page/痢疾杆菌.md "wikilink")、[伤寒杆菌](../Page/伤寒杆菌.md "wikilink")、[金黄色葡萄球菌及某些皮肤真菌](../Page/金黄色葡萄球菌.md "wikilink")，有利尿、降压、防癌作用。

### 辟邪

[屈原的](../Page/屈原.md "wikilink")《[离骚](../Page/离骚.md "wikilink")》里提到了茱萸是恶草。[李时珍在](../Page/李时珍.md "wikilink")《[本草纲目](../Page/本草纲目.md "wikilink")》中记载，茱萸的品质“辛辣蜇口惨腹，使人有杀毅党然之状”。古人“悬其子于屋，辟鬼魅”。南朝时就有插茱萸辟邪的记载。晋周处在《风土记》中说：“九月九日折茱萸以插头上，辟除恶气而御初寒。”茱萸雅号“辟邪翁”。

### 酿酒

在古代诗文中有不少咏颂茱萸酒的篇章。现代人生产山茱萸系列果酒、饮料，味道淳美，药效显著。

## 风俗

古人把茱萸作为祭祀、佩饰、药用、避邪之物，形成茱萸风俗。晋代[葛洪](../Page/葛洪.md "wikilink")《[西京雜記](../Page/西京雜記.md "wikilink")》中就记载，汉高祖[刘邦的宠妃戚夫人于每年九月九日](../Page/刘邦.md "wikilink")，头插茱萸，饮菊花酒，食蓬饵，出游欢宴。

重阳节插茱萸的风俗，在[唐代就已经很普遍](../Page/唐代.md "wikilink")。《荊楚歲時記》載楚俗九月九日飲菊花酒。农历九月九日重阳节时，秋高气爽，正是茱萸成熟之时，茱萸被认为能祛病驱邪，所以古人或头插茱萸枝，或臂佩茱萸囊，登高游兴，并把重阳节称为登高节、茱萸节、茱萸会。直至民国时期，一些文人秋季聚会请贴的常用款式为：“×月×日，登高萸觞，候光。”到民国以后，茱萸风俗逐渐衰退。

唐代，[王維十七歲時所做異鄉遊子重九懷鄉思親的抒情](../Page/王維.md "wikilink")[詩](../Page/詩.md "wikilink")：九月九日憶[山東兄弟](../Page/山東.md "wikilink")
(七言絕句) ：獨在異鄉為異客，每逢佳節倍思親。遙知兄弟登高處，遍插茱萸少一人。

茱萸峰位于中国河南焦作的世界地质公园云台山景区，形似茱萸。每逢农历九月季节，秋高气爽，山上野生茱萸茂盛，佩带茱萸，登高以健身、望远、怀念。登上茱萸峰，喝杯怀菊花茶或怀菊花酒，顿觉神清气爽。登高远足、喝菊花茶/酒、插茱萸都具有驱病消灾、健身长寿的意义。

## 相關頁面

  - [重陽節](../Page/重陽節.md "wikilink")
  - [重陽節習俗](../Page/重陽節習俗.md "wikilink")
  - [費長房](../Page/費長房.md "wikilink")

## 参考文献

  - [郭鹏，漫话茱萸
    《文史知识》1995年第5期，页50](https://archive.is/20120922052402/http://dfz.hanzhong.gov.cn/ReadNews.asp?NewsID=516&BigClassID=18&BigClassName=%E5%A7%B9%E5%A4%89%E8%85%91%E9%8D%99%E8%8C%B6%E7%98%BD&SpecialID=0)

[Category:中药](../Category/中药.md "wikilink")
[Category:芸香科](../Category/芸香科.md "wikilink")
[Category:重陽節](../Category/重陽節.md "wikilink")