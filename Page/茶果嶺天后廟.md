**茶果嶺天后廟**是[香港的一座](../Page/香港.md "wikilink")[天后廟](../Page/天后廟.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[觀塘區](../Page/觀塘區.md "wikilink")[茶果嶺](../Page/茶果嶺.md "wikilink")，為香港少數完全使用[麻石興建的](../Page/麻石.md "wikilink")[廟宇](../Page/廟宇.md "wikilink")，現由[華人廟宇委員會負責管理](../Page/華人廟宇委員會.md "wikilink")。

茶果嶺天后廟是[清朝](../Page/清朝.md "wikilink")[道光年間時官府所建](../Page/道光.md "wikilink")，並由當時[觀塘居民合資](../Page/觀塘.md "wikilink")。據廟門石額所記，現存的建築在[光緒辛卯年](../Page/光緒.md "wikilink")（光緒十七年，1891年）重建\[1\]。

廟宇原本位於[觀塘灣灣畔](../Page/觀塘灣.md "wikilink")（今[麗港城一帶](../Page/麗港城.md "wikilink")），直至1941年因該址被選為興建[亞細亞油庫](../Page/亞細亞油庫.md "wikilink")，才搬到茶果嶺現址，並曾於1947年重修。於1999年再重修時，重現原本的石砌建築，呈現古樸的氣質。廟內正中的神壇供奉[天后](../Page/天后.md "wikilink")，以金黃色為主色，而左面的神壇供奉[魯班](../Page/魯班.md "wikilink")，右面的神壇供奉[觀音](../Page/觀音.md "wikilink")。

天后廟面闊三間，深兩進，規模屬中型。而最具特色之處是整座建築都用[花崗岩建成](../Page/花崗岩.md "wikilink")，在香港以至鄰近地區，都是少見的例子\[2\]。村民至今依然虔誠禮奉，每逢[農曆](../Page/農曆.md "wikilink")[三月廿三](../Page/三月廿三.md "wikilink")[天后寶誕](../Page/天后寶誕.md "wikilink")，村民舉辦別具特式的花炮巡遊以及[粵劇賀誕表演慶祝](../Page/粵劇.md "wikilink")。

## 參見

  - [茶果嶺求子石](../Page/茶果嶺求子石.md "wikilink")

## 參考來源

## 外部連結

  - [吃喝玩樂在觀塘：名勝古蹟遊](https://web.archive.org/web/20061206212408/http://www.kwuntong.org.hk/xtc_a.htm)
  - [天后寶誕
    茶果嶺大戲連場](http://paper.wenweipo.com/2007/05/09/HK0705090026.htm)
  - [賀天后誕茶果嶺今辦飄色](http://the-sun.on.cc/channels/news/20070509/20070509022245_0000.html)

[Category:香港天后廟](../Category/香港天后廟.md "wikilink")
[Category:茶果嶺](../Category/茶果嶺.md "wikilink")
[Category:香港非物質文化遺產](../Category/香港非物質文化遺產.md "wikilink")

1.

2.