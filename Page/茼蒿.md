**茼蒿**（[学名](../Page/学名.md "wikilink")：**），別名**春菊**、**打某菜**、**茼蒿菜**、**艾菜、皇帝菜**，是一種[菊屬](../Page/菊屬.md "wikilink")[植物](../Page/植物.md "wikilink")，原產於[地中海南岸](../Page/地中海.md "wikilink")。茼蒿在[歐洲原本是](../Page/歐洲.md "wikilink")[庭園中美麗的觀葉植物](../Page/庭園.md "wikilink")，但在[宋朝引進](../Page/宋朝.md "wikilink")[中國後](../Page/中國.md "wikilink")，卻成為餐桌上美味的佳餚。

## 名稱

### 學名

原產地中海的茼蒿，最早由林奈命名，且根據[命名法規](../Page/國際植物命名法規.md "wikilink")，*Chrysanthemum
coronarium*被定為[保留名](../Page/保留名.md "wikilink")。下列為其[學名](../Page/學名.md "wikilink")（**粗體**）與[異名](../Page/異名.md "wikilink")。

  - ***Chrysanthemum coronarium* L.**, Sp. Pl. 2: 890. 1753, nom. cons.;
    \[1\]
  - *Chrysanthemum roxburghii* Desf.
  - *Glebionis coronaria* (L.) Cassini ex Spach, Hist. Nat. Vég. 10:
    181. 1841.\[2\]
  - *Glebionis roxburghii* (Desf.) Tzvelev; Matricaria coronaria (L.)
    Desrouss.
  - *Pinardia coronaria* (L.) Lessing
  - *Pyrethrum indicum* Sims (1813), not (L.) Cassini (1826)
  - *Xantophtalmum coronarium* (L.) P. D. Sell.

### 俗名

[Tần_ô.jpg](https://zh.wikipedia.org/wiki/File:Tần_ô.jpg "fig:Tần_ô.jpg")市場販售的茼蒿\]\]
**茼蒿**一名的來源，據[明朝](../Page/明朝.md "wikilink")[李時珍](../Page/李時珍.md "wikilink")[本草綱目茼蒿一條下之釋名指其別名為](../Page/本草綱目.md "wikilink")**蓬蒿**，且「形氣同乎蓬蒿，故名。」\[3\]又，「茼」字有些文獻寫作「同」、「蕫」、「董」、「唐」等。\[4\]

它有數個別名：

  - 閩南語俗稱**拍某菜**。由於茼蒿的嫩株富含水份，一經受熱炒煮，體積極度縮小，經常下鍋前看似一、二鍋的量，炒煮後則僅剩一小盤。不明事理的先生認為太太偷吃或偷藏，因而拳腳相向，**拍某菜**之稱由是傳開。\[5\]
  - **菊花菜**、**冬子蒿**。\[6\]
  - **春菊**是日本所使用的羽裂葉茼蒿，但「青味」（草味）較重。\[7\]
  - 英文名稱為 **Garland
    Chrysanthemum**\[8\]，意指其花（頭狀花序）形似代表勝利的花環，亦是[林奈](../Page/林奈.md "wikilink")[命名時](../Page/命名.md "wikilink")[學名之](../Page/學名_\(植物\).md "wikilink")[種小名採用](../Page/種小名.md "wikilink")
    **coronarium** 之意\[9\]。

## 分布

茼蒿原產於[地中海地區](../Page/地中海地區.md "wikilink")，在[中國則廣泛作為](../Page/中國.md "wikilink")[蔬菜栽培](../Page/蔬菜.md "wikilink")：[安徽](../Page/安徽.md "wikilink")、[福建](../Page/福建.md "wikilink")、[廣東](../Page/廣東.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[廣州](../Page/廣州.md "wikilink")、[海南](../Page/海南.md "wikilink")、[河北](../Page/河北.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[吉林](../Page/吉林.md "wikilink")、[山東](../Page/山東.md "wikilink")、[浙江](../Page/浙江.md "wikilink")。\[10\]

## 形態

  - 一年生草本，全株無毛或近無毛。\[11\]
  - 莖直立，高70厘米，分枝少。\[12\]
  - 開花時，基部葉片枯萎。植株中部以下的葉無柄，窄橢圓形或披金形，葉緣開裂1-2回甚至3回，裂瓣為卵形或線形。植株上部的葉子漸小。\[13\]
  - 頭狀花序頂生，單一或多數。花梗15-20厘米。\[14\]
      - 總苞杯形，直徑達3厘米。總苞之苞片（phyllary）四列，線形。
  - 瘦果：舌狀花的瘦果具三片窄翅，管狀花的瘦果一至二稜。\[15\]
  - 花期、果期為九月。\[16\]

染色體數目：2n = 18 。 \[17\]

## 功用

茼蒿在[歐洲原本是庭園中美麗的](../Page/歐洲.md "wikilink")[觀葉植物](../Page/觀葉植物.md "wikilink")，但在[宋朝引進](../Page/宋朝.md "wikilink")[中國後](../Page/中國.md "wikilink")，其幼株作為蔬菜使用。
[Chrysanth_coronarium.JPG](https://zh.wikipedia.org/wiki/File:Chrysanth_coronarium.JPG "fig:Chrysanth_coronarium.JPG")

  - 茼蒿的[葉](../Page/葉.md "wikilink")、[莖含豐富的](../Page/莖.md "wikilink")[維生素](../Page/維生素.md "wikilink")，[鈣](../Page/鈣.md "wikilink")、[鐵的含量也較高](../Page/鐵.md "wikilink")。\[18\]
  - 茼蒿具有[菊科植物特殊的](../Page/菊科.md "wikilink")**蒿氣**\[19\]，具健胃作用，能幫助[消化](../Page/消化.md "wikilink")、舒緩[咳嗽現象](../Page/咳嗽.md "wikilink")。\[20\]
  - 茼蒿富含[葉綠素](../Page/葉綠素.md "wikilink")，能活潑[造血功能具潔血作用](../Page/造血功能.md "wikilink")，預防[成人疾病](../Page/成人疾病.md "wikilink")。\[21\]
  - 茼蒿含豐富[膽鹼](../Page/膽鹼.md "wikilink")，具補[脾健](../Page/脾.md "wikilink")[胃](../Page/胃.md "wikilink")、降壓益[腦的作用](../Page/腦.md "wikilink")。\[22\]

## 栽培

茼蒿适合在温和或略冷的环境中生长，在温暖的夏季很快开花成熟。种子于初春或秋季播撒。

茼蒿屬於含水量高的蔬菜，葉片容易變黃，因此需新鮮使用。\[23\]

在台灣，茼蒿各地均有栽培，但以[台北之](../Page/臺北市.md "wikilink")[社子](../Page/社子_\(臺北市\).md "wikilink")、[士林](../Page/士林區.md "wikilink")，以及[彰化](../Page/彰化縣.md "wikilink")、[雲林最多](../Page/雲林縣.md "wikilink")。\[24\]大約於十月上市，供應至清明節前。\[25\]

## 歷史

原產於[地中海地區](../Page/地中海.md "wikilink")，在[宋朝時引入中國栽培](../Page/宋朝.md "wikilink")。\[26\]然而據[李時珍言](../Page/李時珍.md "wikilink")，則自古即栽培食用，只是到了《[嘉祐本草](../Page/嘉祐本草.md "wikilink")》才收入，並往前推至[唐代](../Page/唐代.md "wikilink")，稱[孫思邈的](../Page/孫思邈.md "wikilink")[千金方已講到茼蒿](../Page/千金方.md "wikilink")\[27\]。

  - 《[嘉祐本草](../Page/嘉祐本草.md "wikilink")》：「同蒿平，主安心氣、養脾胃、消水飲；又動風氣、薰人心，令人氣滿，不可多食。」
  - 《[本草綱目](../Page/本草綱目.md "wikilink")》：「安心氣、養脾胃、消痰飲、利腸胃。」\[28\]

## 料理方式

除了煮湯、清炒、火鍋配料，由於茼蒿本身的季節性，其料理模式亦反應在節令。

  - [冬至食用](../Page/冬至.md "wikilink")[鹹湯圓時](../Page/湯圓.md "wikilink")，湯料多為茼蒿\[29\]。
  - [火鍋配料](../Page/火鍋.md "wikilink")，作為時令之涮煮青菜\[30\]。
  - [蚵仔煎之配菜](../Page/蚵仔煎.md "wikilink")\[31\]。

## 注釋

<references />

## 參考文獻

  - Shi,Z.（石鑄）, Humphries, C. J. & Gilbert, M. G. 2011.
    *[Glebionis](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=250058139)*.
    In: Flora of China Editorial Committee. 2011. Flora of China
    (20-21): 772. Science Press & Missouri Botanical Garden Press,
    Beijing & St. Louis.擷取於2012-07-28。
  - 李時珍，本草綱目。In: 商務印書館/萬有文庫版，1930。本草綱目，卷二十六 菜部。第四冊第78頁。上海：商務印書館。
  - 簡錦玲，2007。臺灣好蔬菜。台北：天下遠見。共279頁。ISBN 978-986-216-017-6。
  - 《台灣蔬果實用百科第一輯》，薛聰賢 著，薛聰賢出版社，2001年，ISDN:957-97452-1-8

## 外部連結

  -
  - [Nutrition Facts for Garland
    chrysanthemum](http://www.healthaliciousness.com/nutritionfacts/nutrition-comparison.php?o=11157&t=11158&h=11698&s=100&e=100&r=100)

[Category:菊属](../Category/菊属.md "wikilink")
[Category:葉菜類](../Category/葉菜類.md "wikilink")

1.  [Fl.
    China](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=250058139)
    在Flora of China中所採用的學名為 Glebionis coronaria (L.) Cassini ex
    Spach，這與[日本維基百科](http://ja.wikipedia.org/wiki/%E3%82%B7%E3%83%A5%E3%83%B3%E3%82%AE%E3%82%AF)所用的學名一樣，意指，在林奈之後，Spach認為林奈的[菊屬植物實可再區分出明顯的](../Page/菊屬.md "wikilink")**茼蒿屬**。但中文維基採其[保留名](../Page/保留名.md "wikilink")。

2.
3.  本草綱目/菜部卷二十六。1930年商務本。

4.  [《臺灣通志/物產/蔬菜類》](http://zh.wikisource.org/wiki/%E8%87%BA%E7%81%A3%E9%80%9A%E5%BF%97/%E7%89%A9%E7%94%A2/%E8%94%AC%E8%8F%9C%E9%A1%9E)：《臺灣通志》（1895年？）列舉了臺灣、鳳山、彰化、《噶瑪蘭》、《淡水志》、《諸羅縣志》、《澎湖廳志》等文獻，其原文：「同蒿，時珍云：『形氣同於蓬蒿，故名。』一名蕫蒿。葉似芥艾，花似小菊，性冷微香，多食令人氣滿。《學團雜蔬》謂之董蒿。」。

5.  簡錦玲，2007。第46-49頁。

6.
7.
8.
9.  [《Dictionary of Botanical
    Epithets》](http://www.winternet.com/~chuckg/dictionary/dictionary.67.html)，[拉丁文字根](../Page/拉丁文.md "wikilink")**coronari**指花環：wreaths,
    garlands, crowns。2012-07-28擷取。

10. [Fl.
    China](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=250058139)：Shi,
    Z., Humphries, C. J. & Gilbert, M. G. 2011. 。

11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.