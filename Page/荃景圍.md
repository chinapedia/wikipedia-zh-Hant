**荃景圍**（）是[香港的一個地方](../Page/香港.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[荃灣區](../Page/荃灣區.md "wikilink")，位置於[荃灣市中心與](../Page/荃灣市中心.md "wikilink")[油柑頭之間](../Page/油柑頭.md "wikilink")，[柴灣角之西北](../Page/柴灣角.md "wikilink")，東近[芙蓉山](../Page/芙蓉山_\(香港\).md "wikilink")，北近[曹公潭](../Page/曹公潭.md "wikilink")，西近[下花山](../Page/下花山.md "wikilink")、[石龍拱](../Page/石龍拱.md "wikilink")，南面以[青山公路－荃灣段](../Page/青山公路－荃灣段.md "wikilink")、[大窩口](../Page/大窩口.md "wikilink")、柴灣角工業區為界。環繞此區的道路亦同樣命名**荃景圍**，像一條環形道路，全長約1.9公里，大致為雙線雙程路。

區內主要為住宅區，另有中學三間，小學兩間。
[Tsuen_King_Circuit_Recreation_Ground_and_Rest_Garden_2018.jpg](https://zh.wikipedia.org/wiki/File:Tsuen_King_Circuit_Recreation_Ground_and_Rest_Garden_2018.jpg "fig:Tsuen_King_Circuit_Recreation_Ground_and_Rest_Garden_2018.jpg")
[Tsuen_King_Circuit_Market_2018.jpg](https://zh.wikipedia.org/wiki/File:Tsuen_King_Circuit_Market_2018.jpg "fig:Tsuen_King_Circuit_Market_2018.jpg")
[HK_Tsuen_Wan_Adventist_Hospital_Tsuen_King_Circuit.JPG](https://zh.wikipedia.org/wiki/File:HK_Tsuen_Wan_Adventist_Hospital_Tsuen_King_Circuit.JPG "fig:HK_Tsuen_Wan_Adventist_Hospital_Tsuen_King_Circuit.JPG")

## 住宅屋苑

  - [愉景新城](../Page/愉景新城.md "wikilink")
  - [荃威花園](../Page/荃威花園.md "wikilink")
  - [荃灣中心](../Page/荃灣中心.md "wikilink")
  - [荃德花園](../Page/荃德花園.md "wikilink")
  - [荃景花園](../Page/荃景花園.md "wikilink")
  - [家興大廈](../Page/家興大廈.md "wikilink")
  - [翠豐臺](../Page/翠豐臺.md "wikilink")
  - [千里臺](../Page/千里臺.md "wikilink")
  - [錦豐園](../Page/錦豐園.md "wikilink")

## 中學

  - [保良局李城璧中學](../Page/保良局李城璧中學.md "wikilink")
  - [仁濟醫院林百欣中學](../Page/仁濟醫院林百欣中學.md "wikilink")
  - [紡織學會美國商會胡漢輝中學](../Page/紡織學會美國商會胡漢輝中學.md "wikilink")

## 小學

  - [柴灣角天主教小學](../Page/柴灣角天主教小學.md "wikilink")
  - [中華基督教會基慧小學](../Page/中華基督教會基慧小學.md "wikilink")

## 幼稚園

  - [荃威幼稚園](http://www.allwaykindergarten.edu.hk)
  - [天主教領報幼稚園](http://www.ack.edu.hk)
  - [心怡天地幼稚園](http://www.joyfulenglish.edu.hk/tc/index.html)

## 醫院

  - [香港港安醫院–荃灣](../Page/香港港安醫院–荃灣.md "wikilink")

## 區議會議席分佈

為方便比較，以下列表以[荃景圍沿線為範圍](../Page/荃景圍.md "wikilink")。

| 年度/範圍                            | 2000-2003                                                                                                            | 2004-2007 | 2008-2011 | 2012-2015 | 2016-2019 | 2020-2023 |
| -------------------------------- | -------------------------------------------------------------------------------------------------------------------- | --------- | --------- | --------- | --------- | --------- |
| [荃景圍](../Page/荃景圍.md "wikilink") | [愉景選區](../Page/愉景.md "wikilink")、[荃灣中心選區及](../Page/荃灣中心_\(選區\).md "wikilink")[荃威選區](../Page/荃威_\(選區\).md "wikilink") |           |           |           |           |           |
|                                  |                                                                                                                      |           |           |           |           |           |

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - 要道

<!-- end list -->

  - [荃景圍](../Page/荃景圍.md "wikilink")
  - [沙咀道天橋](../Page/沙咀道.md "wikilink")
  - [安賢街](../Page/安賢街.md "wikilink")

</div>

</div>

## 外部链接

  - [保良局李城璧中學](http://www.plklsp.edu.hk/)
  - [仁濟醫院林百欣中學](http://www.ychlpyss.edu.hk/)
  - [紡織學會美國商會胡漢輝中學](http://www.tiaccwhf.net/)
  - [柴灣角天主教小學](http://cwp.hkcampus.net/)
  - [中華基督會基慧小學](http://kws.hkcampus.net/)
  - [荃灣港安醫院](http://www.twah.org.hk/)
  - [荃威花園](http://www.allway-gardens.hk/)
  - [荃葵青交通總站巡禮 - 荃景圍 (1)](http://hk.youtube.com/watch?v=Mfiz-yKVj3Q)
  - [荃葵青交通總站巡禮 - 荃景圍 (2)](http://hk.youtube.com/watch?v=pYCxWFN5YRQ)

{{-}}

[Category:柴灣角](../Category/柴灣角.md "wikilink")
[Category:紅色公共小巴禁區](../Category/紅色公共小巴禁區.md "wikilink")