**荃灣官立小學**（英語：**Tsuen Wan Government Primary
School**）是一所位於[香港](../Page/香港.md "wikilink")[荃灣的著名](../Page/荃灣.md "wikilink")[官立小學](../Page/官立小學.md "wikilink")，兼收男女學生，於1961年3月成立\[1\]。前身為[海壩街官立小學](../Page/海壩街官立小學.md "wikilink")\[2\]下午校，該校於1999年9月分拆並遷往[麗城花園的現址](../Page/麗城花園.md "wikilink")\[3\]，同時易名為「荃灣官立小學」\[4\]，是荃灣區內兩間[官立小學之一](../Page/官立小學.md "wikilink")。

## 歷史

成立於1961年的海壩街官立小學\[5\]，是二戰後[新界首間男女文法](../Page/新界.md "wikilink")[小學](../Page/小學.md "wikilink")。此學校的成立是因香港政府發表了《菲沙報告書》，再加上當時來港的難民增加，[荃灣人口增加急速](../Page/荃灣.md "wikilink")，令政府率先於[荃灣區內建立新界首間男女文法](../Page/荃灣.md "wikilink")[小學](../Page/小學.md "wikilink")。

1999年\[6\]，海壩街官立上午小學和海壩街官立下午小學被[教育統籌局](../Page/教育統籌局.md "wikilink")（現稱[教育局](../Page/教育局.md "wikilink")）強行要求轉為[全日制](../Page/全日制.md "wikilink")，並於同年9月正式把上午校及下午校兩所小學分離。當局亦從[新界](../Page/新界.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[青山公路](../Page/青山公路.md "wikilink")600號近[麗城花園的位置修建了一所新校舍](../Page/麗城花園.md "wikilink")\[7\]，讓下午校的學生可以順利過渡新學年。因分離後兩校校名會重疊，因此校長決定向當局申請換名，最後決議海壩街官立下午小學換名為荃灣官立小學\[8\]。

## 資料來源

[T](../Category/香港官立小學.md "wikilink")
[Category:海壩街官立下午小學](../Category/海壩街官立下午小學.md "wikilink")
[Category:荃灣區小學](../Category/荃灣區小學.md "wikilink")
[Category:荃灣](../Category/荃灣.md "wikilink")
[Category:1961年創建的教育機構](../Category/1961年創建的教育機構.md "wikilink")

1.

2.
3.

4.
5.
6.
7.
8.