**荊州**，是中國古代的ㄧ個行政區劃，最早由漢武帝依據九州分區而設置。

## 兩漢時期

[西漢](../Page/西漢.md "wikilink")[武帝](../Page/漢武帝.md "wikilink")[元封五年](../Page/元封_\(汉武帝\).md "wikilink")（公元前106年），全國除中央之外的地域分為13個州[刺史部](../Page/刺史.md "wikilink")，其中荊州的轄域相當廣，大體來說以北為現今[河南省](../Page/河南省.md "wikilink")[南陽市一帶往南延伸至兩湖](../Page/南陽市.md "wikilink")（[湖北](../Page/湖北.md "wikilink")、[湖南](../Page/湖南.md "wikilink")）全境內。但是此时的州刺史部只是監察區，無治所，非真正意義上的行政區。

[東漢](../Page/東漢.md "wikilink")[漢靈帝](../Page/漢靈帝.md "wikilink")[中平五年](../Page/中平.md "wikilink")（188年）后，州成為正式的一級行政區域，依據[後漢書記載](../Page/後漢書.md "wikilink")，荊州轄有[南陽](../Page/南阳郡.md "wikilink")（河南西南）、[南郡](../Page/南郡.md "wikilink")（[湖北西部](../Page/湖北.md "wikilink")）、[江夏](../Page/江夏郡.md "wikilink")（湖北東部）、[長沙](../Page/长沙郡.md "wikilink")（湖南東北）、[桂陽](../Page/桂阳郡.md "wikilink")（[湖南東南](../Page/湖南.md "wikilink")）、[武陵](../Page/武陵郡.md "wikilink")（湖南西北）、[零陵](../Page/零陵郡.md "wikilink")（湖南西南）七郡。州治所一直到[東漢](../Page/東漢.md "wikilink")[漢獻帝時](../Page/漢獻帝.md "wikilink")[劉表領荊州牧以前都在](../Page/劉表.md "wikilink")[漢壽县](../Page/漢壽县.md "wikilink")（[武陵郡轄](../Page/武陵郡.md "wikilink")，今[常德市](../Page/常德市.md "wikilink")[汉寿县](../Page/汉寿县.md "wikilink")），後因荊南地區[黃巾賊餘孽尚未退去才將治所移到當時已經相當繁榮的南郡](../Page/黃巾賊.md "wikilink")[襄陽城](../Page/襄陽.md "wikilink")（今湖北[襄陽市](../Page/襄陽市.md "wikilink")）。

## 三国兩晉時期

208年[赤壁之战后](../Page/赤壁之战.md "wikilink")，原先短暂领有[荆州的](../Page/荆州.md "wikilink")[曹操只保住了荆州北部](../Page/曹操.md "wikilink")[南阳郡](../Page/南阳郡.md "wikilink")、[江夏郡与](../Page/江夏郡.md "wikilink")[南郡的一部分](../Page/南郡.md "wikilink")，而中南部被孙刘联軍佔据。曹操之后从南郡、南阳郡中分出襄阳郡、[南乡郡](../Page/南乡郡.md "wikilink")，荆州成为三分割据的局面：南郡、零陵、武陵归[刘备](../Page/刘备.md "wikilink")，江夏、桂阳、长沙归[孙权](../Page/孙权.md "wikilink")，南阳、襄阳、南乡归[曹操](../Page/曹操.md "wikilink")\[1\]。一般认为这阶段三家各占三郡的局面是“荆襄九郡”一词的来源。

219年，荆州牧刘备的守将[關羽被](../Page/關羽.md "wikilink")[东吴](../Page/孙吴.md "wikilink")、[曹魏联军击败](../Page/曹魏.md "wikilink")，在随后的[夷陵之戰中劉備又敗於孫權勢力後](../Page/夷陵之戰.md "wikilink")，[蜀汉再無力奪回原有荊州轄權](../Page/蜀汉.md "wikilink")，荊州成为由曹魏與[孫吳兩家分領的局面](../Page/孫吳.md "wikilink")。

[三國的曹魏荊州](../Page/三國.md "wikilink")，治所在南阳郡[新野](../Page/新野.md "wikilink")，下轄南陽郡、江夏郡（長江以北）、襄陽郡、[南鄉郡](../Page/南鄉郡.md "wikilink")、[新城郡](../Page/新城郡.md "wikilink")、[上庸郡](../Page/上庸郡.md "wikilink")、[魏興郡等共七郡](../Page/魏興郡.md "wikilink")。三國的孫吳荊州，治所在南郡的[江陵](../Page/江陵.md "wikilink")，下轄南郡、江夏郡（長江以南）、長沙郡、[湘東郡](../Page/湘東郡.md "wikilink")、桂陽郡、[臨賀郡](../Page/臨賀郡.md "wikilink")、零陵郡、[衡陽郡](../Page/衡陽郡.md "wikilink")、武陵郡、[建平郡](../Page/建平郡.md "wikilink")、[宜都郡等共十一郡](../Page/宜都郡.md "wikilink")。

[西晉時期](../Page/西晉.md "wikilink")，荊州治所在江陵，下轄23個郡國，[北魏太和二十一年](../Page/北魏.md "wikilink")（497年）于[穰县](../Page/穰县.md "wikilink")（今河南[邓州市](../Page/邓州市.md "wikilink")）置荆州，辖8郡。

## [南北朝](../Page/南北朝.md "wikilink")

南北朝時期，州的數量增多，地域縮小，[劉宋設有荊州](../Page/劉宋.md "wikilink")，治所在襄陽（今湖北[襄陽](../Page/襄陽.md "wikilink")），[蕭齊改治所為](../Page/蕭齊.md "wikilink")[南郡](../Page/南郡.md "wikilink")，另設[雍州](../Page/雍州.md "wikilink")，治所在[襄陽](../Page/襄陽.md "wikilink")[寧蠻府](../Page/寧蠻府.md "wikilink")（湖北襄陽）。而[北魏設荊州和](../Page/北魏.md "wikilink")[東荊州](../Page/東荊州.md "wikilink")，治所分別在[山北](../Page/山北.md "wikilink")（河南[魯山](../Page/魯山.md "wikilink")）和[泌陽](../Page/泌陽.md "wikilink")[東陵鎮](../Page/東陵鎮.md "wikilink")（河南泌陽）。

## [隋朝](../Page/隋朝.md "wikilink")

到了[隋文帝](../Page/隋文帝.md "wikilink")[開皇三年](../Page/開皇.md "wikilink")（583年），[楊堅廢除郡一級行政區劃](../Page/楊堅.md "wikilink")，改為州縣兩級體制，而[隋煬帝時](../Page/隋煬帝.md "wikilink")，又改州為郡，恢復[秦朝的](../Page/秦朝.md "wikilink")[郡縣兩級體制](../Page/郡县制.md "wikilink")，荊州的名稱只存在地域為現今[荊州市附近](../Page/荊州市.md "wikilink")。一度称为[南郡](../Page/南郡.md "wikilink")、[江陵郡](../Page/江陵郡.md "wikilink")，后来为[江陵府](../Page/江陵府.md "wikilink")、[荆州府](../Page/荆州府.md "wikilink")。

## 参考文献

{{-}}

[荊州](../Category/荊州.md "wikilink")

1.  《[晋书](../Page/晋书.md "wikilink")·地理志下》：“于是南郡、零陵、武陵以西为蜀，江夏、桂阳、长沙三郡为吴，南阳、襄阳、南乡三郡为魏。”