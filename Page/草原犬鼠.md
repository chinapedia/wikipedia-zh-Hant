**草原犬鼠**（**''Cynomys**''）也常稱作**[土撥鼠](../Page/土撥鼠.md "wikilink")**，是一種小型穴棲性[嚙齒目動物](../Page/嚙齒目.md "wikilink")，原產於[北美洲](../Page/北美洲.md "wikilink")[大草原](../Page/大草原.md "wikilink")，當地人稱之為“**草原犬**”（**Prairie
dogs**）。如果算上短尾巴，土撥鼠身長平均约為30至40厘米。土拨鼠棲息在[美國](../Page/美國.md "wikilink")、[加拿大和](../Page/加拿大.md "wikilink")[墨西哥](../Page/墨西哥.md "wikilink")，在美國，土撥鼠主要產於在密西西比河以西，但在東部幾個地點亦有發現。

[Category:草原犬鼠](../Category/草原犬鼠.md "wikilink")