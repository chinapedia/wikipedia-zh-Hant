**草海**又名**松坡湖**、**南海子**、**八仙湖**，位于[中国](../Page/中国.md "wikilink")[贵州省](../Page/贵州省.md "wikilink")[威宁彝族回族苗族自治县县城西南隅](../Page/威宁彝族回族苗族自治县.md "wikilink")，因湖中水草繁茂，故名。草海是中国最大的构造岩溶湖泊，也是贵州省最大的天然湖泊\[1\]。

草海是[中国特有的珍稀](../Page/中国.md "wikilink")[鸟类](../Page/鸟.md "wikilink")[黑颈鹤的越冬栖息地](../Page/黑颈鹤.md "wikilink")，每年有400多只黑颈鹤到此越冬。1992年草海被批准为国家级[自然保护区](../Page/自然保护区.md "wikilink")，全保护区面积为12000公顷，主要保护[动物黑颈鹤](../Page/动物.md "wikilink")，保护[植物为](../Page/植物.md "wikilink")[海菜花](../Page/海菜花.md "wikilink")。

## 参考文献

## 外部链接

  - [草海旅游网](http://gzwngd.com.cn/ly/)

{{-}}

[Category:贵州湖泊](../Category/贵州湖泊.md "wikilink")
[Category:中国国家级自然保护区](../Category/中国国家级自然保护区.md "wikilink")

1.