**草莓牛奶**（，），[日本](../Page/日本.md "wikilink")[北海道出身的](../Page/北海道.md "wikilink")[AV女優](../Page/AV女優.md "wikilink")。[T-POWERS所屬](../Page/T-POWERS.md "wikilink")。

## 經歷

在訪談中表示初次性行為是16歲，對象是長三歲的男友。

2005年7月，草莓牛奶與[日本職棒投手](../Page/日本職棒.md "wikilink")[吉武真太郎發生戀情](../Page/吉武真太郎.md "wikilink")，失戀後服食40粒[安眠藥及](../Page/安眠藥.md "wikilink")[割腕](../Page/割腕.md "wikilink")[自殺未遂](../Page/自殺.md "wikilink")。草莓牛奶曾說：「自從我演出AV後，我和家裡的關係就一直很不好。父親認為我這樣做是丟了全家人的臉。」\[1\]

2012年10月，日本雜誌《[週刊新潮](../Page/週刊新潮.md "wikilink")》指她自殺身亡。在2013年5月23日的有線電視節目《怪談》中，主持曾表示，草莓牛奶已於2012年10月跳樓身亡，年僅31歲。

## 作品

### AV作品

  - 2000年

<!-- end list -->

  - 好きです（2000年12月25日、[KUKI](../Page/九鬼_\(アダルトビデオ\).md "wikilink")）

<!-- end list -->

  - 2001年

<!-- end list -->

  - ストロベリー（2001年1月25日、KUKI）
  - 小さな冒険（2001年2月26日、KUKI）
  - いちごクラブ（2001年3月28日、KUKI）
  - いちごシャーベット（2001年4月25日、KUKI）
  - 濡れイチゴ （2001年5月31日、KUKI）
  - 淫口（2001年6月22日、サマンサ（[マックス・エー](../Page/マックス・エー.md "wikilink")））
  - 夏苺娘（2001年7月20日、KUKI）
  - selfish（2001年8月28日、マックス・エー）
  - ハートブレイク（2001年9月16日、KUKI）
  - スプラッシュ（2001年10月23日、サマンサ）
  - いちご狩り（2001年11月17日、KUKI）
  - 雪苺（2001年12月21日、KUKI）

<!-- end list -->

  - 2002年

<!-- end list -->

  - 苺参上（2002年1月22日、KUKI）
  - ズブ濡れ人生激情（2002年2月20日、KUKI）
  - 南極苺（2002年3月19日、KUKI）
  - グッドバイ（2002年4月21日、KUKI）
  - 苺みるく いちご （2002年5月2日、[TMA](../Page/トータル・メディア・エージェンシー.md "wikilink")）
  - 監禁ボディドール（2002年5月24日、サマンサ）
  - COS-PARA（2002年6月13日、[アトラス21](../Page/アトラスにじゅういち.md "wikilink")）
  - 女尻（2002年7月19日、[アリスJAPAN](../Page/アリスJAPAN.md "wikilink")）
  - 堕天使 X
    （2002年8月11日、[宇宙企画](../Page/宇宙企画.md "wikilink")（[メディアステーション](../Page/メディアステーション.md "wikilink")））
  - 妹の秘密（2002年9月17日、サマンサ）
  - trauma（2002年10月18日、アリスJAPAN）
  - ウライチゴ（2002年11月15日、VIP）
  - Strawberry Over
    Sex（2002年12月1日、[MOODYZ](../Page/MOODYZ.md "wikilink")）

<!-- end list -->

  - 2003年

<!-- end list -->

  - FULL BODY（2003年1月1日、MOODYZ）
  - 祝・解禁 （2003年1月24日、[ケイ・エム・プロデュース](../Page/ケイ・エム・プロデュース.md "wikilink")）
  - 天国と地獄（2003年2月1日、MOODYZ）
  - だめちんずうぉ～か～（2003年3月1日、MOODYZ）
  - EROSTY DOLL（2003年4月1日、MOODYZ）
  - 苺みるくが皆んなのリクエストにお応えします\!（2003年5月1日、MOODYZ）
  - ぶっかけ苺みるく（2003年6月1日、MOODYZ）
  - Angel　（2003年6月8日、[アイデアポケット](../Page/アイデアポケット.md "wikilink")）
  - デジタルモザイクVol.014（2003年7月1日、MOODYZ）
  - 苺嬲り（2003年8月1日、MOODYZ）
  - ぶっかけ中出しFUCK\!（2003年9月1日、MOODYZ）
  - レズ病棟7
    （2003年10月4日、[ディープス](../Page/ディープス.md "wikilink")）共演：[はらだはるな](../Page/はらだはるな.md "wikilink")、[吉沢ミズキ](../Page/吉沢ミズキ.md "wikilink")

<!-- end list -->

  - 2004年

<!-- end list -->

  - DIGITAL CHANNEL （2004年7月8日、アイデアポケット）
  - STRAWBERRY FUCK\!
    （2004年8月19日、[V\&Rプロダクツ](../Page/V&Rプロダクツ.md "wikilink")）
  - 悪女宣言 （2004年9月1日、[ワンズファクトリー](../Page/ワンズファクトリー.md "wikilink")）
  - 苺Milk Forever （2004年10月21日、MAGIC）
  - ミルクドール みるく （2004年11月19日、セブンエイト）

<!-- end list -->

  - 2005年

<!-- end list -->

  - やりまくりんこ 5 （2005年11月8日、デラムプロジェクト）
  - 灼熱デジモレズカップル
    （2005年12月19日、[CROSS](../Page/CROSS_\(アダルトビデオ\).md "wikilink")）共演：[三咲まお](../Page/三咲まお.md "wikilink")、[常夏みかん](../Page/常夏みかん.md "wikilink")、川村みなみ

<!-- end list -->

  - 2006年

<!-- end list -->

  - ワンピース黒タイツ。
    （2006年6月19日、CROSS）…[オムニバス形式作品](../Page/オムニバス.md "wikilink")
    他出演：[麻生岬](../Page/麻生岬.md "wikilink")、石川あいり、華月優、常夏みかん、稲葉ゆうき、[楓（YOKO）](../Page/YOKO_\(AV女優\).md "wikilink")、後藤美咲、長瀬あずさ、奈月やよい、[葉山リカ](../Page/葉山リカ.md "wikilink")、みなとあゆみ
  - 出\!有名女優生本本番\!FILE.1 （2006年9月20日、関西NUG企画）
  - 病院の怪人　（2006年11月7日、[アタッカーズ](../Page/アタッカーズ.md "wikilink")）共演：[葵あげは](../Page/葵あげは.md "wikilink")

<!-- end list -->

  - 2007年

<!-- end list -->

  - 淫魔の生贄 【生中出しの儀】 （2007年2月7日、アタッカーズ）

<!-- end list -->

  - 2008年

<!-- end list -->

  - 復活 苺みるく（2008年12月13日、MOODYZ）

<!-- end list -->

  - 2009年

<!-- end list -->

  - 分娩手術台に括られて… キャリアウーマン蔑みの人体実験 青木玲
    （2009年2月7日、アタッカーズ）共演：[青木玲](../Page/青木玲.md "wikilink")

### 無碼AV作品

  - 2003年

<!-- end list -->

  - ミラクルショット : 苺みるく, 宮下杏奈（2003年12月20日、ツバキハウス）
  - スナップショット・ザ・バージン（2003年12月20日、ツバキハウス）

<!-- end list -->

  - 2004年

<!-- end list -->

  - ストロベリーファック（2004年4月6日、[オリエンタルドリーム](../Page/オリエンタルドリーム.md "wikilink")）
  - 夜桜苺 : 苺みるく（2004年4月7日、ツバキハウス）
  - ティーオフ（2004年4月20日、オリエンタルドリーム）

<!-- end list -->

  - 2005年

<!-- end list -->

  - スクールデイズ（2005年6月17日、[一本道](../Page/一本道.md "wikilink")）
  - アキバ戦隊
    萌えレンジャー！（2005年9月23日、[レッドホットコレクション](../Page/レッドホットコレクション.md "wikilink")）
  - ダブルダブルユー\!\! : 苺みるく, 進藤つみき（2005年10月17日、レッドホットコレクション）
  - スカイエンジェル [Sky Angel](../Page/Sky_Angel.md "wikilink") Vol. 17 :
    苺みるく（2005年10月18日、[スカイハイエンターテインメント](../Page/スカイハイエンターテインメント.md "wikilink")）
  - ごえもん Vol.21 ベリーベスト : 苺みるく（2005年10月26日、五右衛門）
  - ダブルダブルユー\!\! W Double
    You（2005年10月31日、スカイハイエンターテインメント）苺みるく、[進藤つみき](../Page/進藤つみき.md "wikilink")

## 注釋

## 關連項目

  - [AV女優列表](../Page/AV女優列表.md "wikilink")

## 外部連結

  - [Sky High Ent. 女優介紹
    草莓牛奶](https://web.archive.org/web/20101217153414/http://skyhighpremium.com/actress/catid_4-itemid_135.html)
  - [有線電視有關草莓牛奶自殺的節目](http://ent.i-cable.com/program/program_unbelievable/videoPlay.php?video_id=34158)

[Category:1981年出生](../Category/1981年出生.md "wikilink")
[Category:2012年逝世](../Category/2012年逝世.md "wikilink")
[Category:日本AV女優](../Category/日本AV女優.md "wikilink") [Category:Alice
Japan女優](../Category/Alice_Japan女優.md "wikilink")
[Category:MOODYZ女優](../Category/MOODYZ女優.md "wikilink")
[Category:KUKI女優](../Category/KUKI女優.md "wikilink")
[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")
[Category:日本自殺者](../Category/日本自殺者.md "wikilink")

1.  [草莓牛奶因為不倫戀而割腕自殺](http://www.prettyvirgin.com/avreport/in-report13.htm)