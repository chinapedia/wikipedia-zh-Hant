**草莓箭毒蛙**（[学名](../Page/学名.md "wikilink")：**），又名**草莓毒刺蛙**，是在[中美洲的一類](../Page/中美洲.md "wikilink")[箭毒蛙](../Page/箭毒蛙.md "wikilink")，集中在[哥斯達黎加](../Page/哥斯達黎加.md "wikilink")。

草莓箭毒蛙雖是一個[物種](../Page/物種.md "wikilink")，但有很大的變異，其下有超過30個不同的顏色形態。除了顏色上的不同外，不同的形態在棲息地、體型、發聲及親代照顧行為上均有所不同。鈷藍形態是最廣為人知的一類。大部份的形態成體平均長18-20毫米。

草莓箭毒蛙鮮艷的[警戒態表明牠的](../Page/警戒態.md "wikilink")[皮膚上有不同的](../Page/皮膚.md "wikilink")[毒素](../Page/毒素.md "wikilink")。這些毒素令牠們有難聞的氣味，驅趕掠食者。草莓箭毒蛙的毒素不足以嚴重傷害[人類](../Page/人類.md "wikilink")。

草莓箭毒蛙類中包含了[疣背箭毒蛙](../Page/疣背箭毒蛙.md "wikilink")、[桔紅箭毒蛙及](../Page/桔紅箭毒蛙.md "wikilink")[威氏箭毒蛙](../Page/威氏箭毒蛙.md "wikilink")。牠們的近親是[小丑箭毒蛙類](../Page/小丑箭毒蛙.md "wikilink")，當中包含了小丑箭毒蛙自己及*O.
duellmani*。這兩類都會照顧產下的卵及[蝌蚪](../Page/蝌蚪.md "wikilink")。

*Oophaga*是2006年新成立的[屬](../Page/屬.md "wikilink")。\[1\]以往草莓箭毒蛙是分類在[箭毒蛙屬中](../Page/箭毒蛙屬.md "wikilink")，而這個分類仍然廣泛使用。

## 生殖及照顧

在[兩棲類的世界中](../Page/兩棲類.md "wikilink")，草莓箭毒蛙及其相關的[青蛙以高度照顧卵及](../Page/青蛙.md "wikilink")[蝌蚪而聞名](../Page/蝌蚪.md "wikilink")。[交配後](../Page/交配.md "wikilink")，雌性會在樹葉或[鳳梨科葉脈產下](../Page/鳳梨科.md "wikilink")3-5顆卵。雄性會確保卵的濕潤，利用牠的[泄殖腔來輸送](../Page/泄殖腔.md "wikilink")[水份](../Page/水.md "wikilink")。約10日卵孵化後，雌性會揹蝌蚪到有沙的地方。有罕有的情況下，雄性亦會揹送蝌蚪，但究竟是有意或是只是巧合則不得而知。鳳梨科的葉脈是草莓箭毒蛙產卵的勝地，而牠們亦會在其他適合的地方，如樹孔、小土丘或[人類垃圾的](../Page/人類.md "wikilink")[鋁罐中產卵](../Page/鋁.md "wikilink")。

每個位點只會放下一隻蝌蚪，因為牠們是[同類相食的](../Page/同類相食.md "wikilink")。一旦安放好蝌蚪後，雌性每隔幾日就會到蝌蚪的位點產下未受精卵作為食物。有研究曾以其他的食物（如[藻類及其他箭毒蛙的卵](../Page/藻類.md "wikilink")）來餵養蝌蚪，但都很難成功。可見牠們是必須照顧蝌蚪，否則牠們很難生存或成長。

約1個月後，蝌蚪會變態成小蛙。牠們一般會留在原有地方幾日，待尾巴完全消失。

## 飼養

由於草莓箭毒蛙吸引的顏色及獨有的生命週期，故牠是很受歡迎的寵物。於1990年代，牠們已經由不同的地方輸入[美國及](../Page/美國.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")。不過由於已停止運送，草莓箭毒蛙已變得不普遍及很少選擇。在歐洲，有較多不同的草莓箭毒蛙供選擇，很多都是走私入境或是其後代。雖然走私草莓箭毒蛙在其他地方很少見，但牠們仍然殺死大量的動物及破壞棲息地。現時有草莓箭毒蛙從[中美洲出口](../Page/中美洲.md "wikilink")，這樣大大提升了牠們的數量。

## 形態

### Bastimentos種

[Oophaga_pumilio.jpg](https://zh.wikipedia.org/wiki/File:Oophaga_pumilio.jpg "fig:Oophaga_pumilio.jpg")
Bastimentos種一般有三個形態，分別為紅色、黃色或白色，背及腳上有黑點。牠們分佈在[巴拿馬的Bastimentos島上](../Page/巴拿馬.md "wikilink")，相信是某程度上真實遺傳的，而非混種而來。

### 鈷藍種

鈷藍種在[美江相對較稀有](../Page/美江.md "wikilink")。牠們大部份都是於1990年代引入美國，或是其後代。牠們的身體是紅色，而四肢是藍色。牠們在照顧[蝌蚪上一般都很差](../Page/蝌蚪.md "wikilink")，可能是照顧及食性上的問題引致。現時有一種科技可以從較普遍的種，如Man
Creek種或Bastimentos種轉變出鈷藍種的蝌蚪。鈷藍種分佈在[哥斯達黎加](../Page/哥斯達黎加.md "wikilink")，數量非常豐富。

### 奇里基種

奇里基種一般都是綠色的，一些是紅色的，有時有些身體呈黃色而四肢呈藍綠色。奇里基種是在[大奇里基或](../Page/大奇里基.md "wikilink")[奇里基河中捕獲的](../Page/奇里基河.md "wikilink")。由於正確的位點並不清楚，所以很難說是大奇里種或是奇里基河種。

### Man Creek種

Man Creek種非常像鈷藍種，有時甚至會彼此混淆。一些Man
Creek種的四肢是藍色的，但從[眼睛就可以分辨牠們](../Page/眼睛.md "wikilink")。不過，Man
Creek種其實四肢一般都是有灰色的，而前肢沒有灰色的亦很普遍。

## 參考

## 外部連結

  - [Dart Den](http://www.dartden.com)
  - [DendroBoard](http://www.dendroboard.com)
  - [Frognet](http://www.frognet.org)
  - [Dendrobatesworld](http://www.dendrobatesworld.com)

[Category:箭毒蛙屬](../Category/箭毒蛙屬.md "wikilink")

1.