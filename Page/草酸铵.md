**草酸銨**是[草酸的](../Page/草酸.md "wikilink")[铵](../Page/铵.md "wikilink")[盐](../Page/盐.md "wikilink")，分子式為(NH<sub>4</sub>)<sub>2</sub>C<sub>2</sub>O<sub>4</sub>。

## 性質

無色晶體，帶有一分子結晶[水](../Page/水.md "wikilink")，分子式亦可作為(NH<sub>4</sub>)<sub>2</sub>C<sub>2</sub>O<sub>4</sub>．H<sub>2</sub>O。

遇熱會分解成[草酸並放出](../Page/草酸.md "wikilink")[氨氣](../Page/氨氣.md "wikilink")，反應式如下：
(NH<sub>4</sub>)<sub>2</sub>C<sub>2</sub>O<sub>4</sub> + 熱 →
H<sub>2</sub>C<sub>2</sub>O<sub>4</sub> + 2NH<sub>3</sub>↑

## 製备

草酸按可由氨水与草酸的反应制备，反應式為： H<sub>2</sub>C<sub>2</sub>O<sub>4</sub> +
2NH<sub>4</sub>OH →
(NH<sub>4</sub>)<sub>2</sub>C<sub>2</sub>O<sub>4</sub> + 2H<sub>2</sub>O

## 用途

可用於製作安全炸药和供分析的试剂等。

## 參見

  - [草酸铁铵](../Page/草酸铁铵.md "wikilink")
  - [草酸氫銨](../Page/草酸氫銨.md "wikilink")

[Category:有机酸铵盐](../Category/有机酸铵盐.md "wikilink")
[Category:草酸盐](../Category/草酸盐.md "wikilink")