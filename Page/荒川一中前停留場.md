**荒川一中前停留場**（）是一個位於[東京都](../Page/東京都.md "wikilink")[荒川區](../Page/荒川區.md "wikilink")[南千住一丁目](../Page/南千住.md "wikilink")，屬於[都電荒川線（東京櫻電）的](../Page/荒川線.md "wikilink")[停留場](../Page/鐵路車站.md "wikilink")。[車站編號是](../Page/車站編號.md "wikilink")**SA
02**。

## 停留場構造

[對向式月台](../Page/對向式月台.md "wikilink")2面2線的[地面車站](../Page/地面車站.md "wikilink")。

## 可供轉乘的巴士路線

沒有巴士路線行經本電車站。

## 相鄰停留場

  - [PrefSymbol-Tokyo.svg](https://zh.wikipedia.org/wiki/File:PrefSymbol-Tokyo.svg "fig:PrefSymbol-Tokyo.svg")
    東京都交通局
    [Tokyo_Sakura_Tram_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyo_Sakura_Tram_symbol.svg "fig:Tokyo_Sakura_Tram_symbol.svg")
    都電荒川線（東京櫻電）
      -
        [荒川區役所前](../Page/荒川區役所前停留場.md "wikilink")（SA 03）－**荒川一中前（SA
        02）**－[三之輪橋](../Page/三之輪橋停留場.md "wikilink")（SA 01）

## 外部連結

  - [東京都交通局荒川一中前停留場](http://www.kotsu.metro.tokyo.jp/toden/stations/arakawa-itchumae/index.html)

[Rakawaitchuumae](../Category/日本鐵路車站_A.md "wikilink")
[Category:荒川線車站](../Category/荒川線車站.md "wikilink")
[Category:荒川區鐵路車站](../Category/荒川區鐵路車站.md "wikilink")
[Category:2000年啟用的鐵路車站](../Category/2000年啟用的鐵路車站.md "wikilink")
[Category:南千住](../Category/南千住.md "wikilink")
[Category:以學校命名的日本鐵路車站](../Category/以學校命名的日本鐵路車站.md "wikilink")