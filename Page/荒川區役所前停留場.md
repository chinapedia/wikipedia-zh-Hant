**荒川區役所前停留場**（）是一個位於[東京都](../Page/東京都.md "wikilink")[荒川區](../Page/荒川區.md "wikilink")[荒川一丁目](../Page/荒川_\(荒川區\).md "wikilink")，屬於[荒川線（東京櫻電）的](../Page/荒川線.md "wikilink")[停留場](../Page/鐵路車站.md "wikilink")。[車站編號是](../Page/車站編號.md "wikilink")**SA
03**。

## 停留場構造

[對向式月台](../Page/對向式月台.md "wikilink")2面2線的[地面車站](../Page/地面車站.md "wikilink")。

## 可供轉乘的巴士路線

### 「荒川区役所前」停留場

  - [里22系統](../Page/里22系統.md "wikilink")：[龜戶站行](../Page/龜戶站.md "wikilink")、[日暮里站前行](../Page/日暮里站.md "wikilink")。
  - [南千47系統](../Page/南千47系統.md "wikilink")：[南千住站東口行](../Page/南千住站.md "wikilink")、日暮里站前行。
  - [草63系統](../Page/草63系統.md "wikilink")：龍泉經由[淺草雷門行](../Page/淺草雷門.md "wikilink")、[巢鴨站經由](../Page/巢鴨站.md "wikilink")[池袋站東口行](../Page/池袋站.md "wikilink")。
  - [草64系統](../Page/草64系統.md "wikilink")：日本堤經由[淺草雷門行](../Page/淺草雷門.md "wikilink")、[王子站經由池袋站東口行](../Page/王子站.md "wikilink")。

## 相鄰停留場

  - [PrefSymbol-Tokyo.svg](https://zh.wikipedia.org/wiki/File:PrefSymbol-Tokyo.svg "fig:PrefSymbol-Tokyo.svg")
    東京都交通局
    [Tokyo_Sakura_Tram_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyo_Sakura_Tram_symbol.svg "fig:Tokyo_Sakura_Tram_symbol.svg")
    都電荒川線（東京櫻電）
      -
        [荒川二丁目](../Page/荒川二丁目停留場.md "wikilink")（SA 04）－**荒川區役所前（SA
        03）**－[荒川一中前](../Page/荒川一中前停留場.md "wikilink")（SA 02）

## 外部連結

  - [東京都交通局荒川区役所前停留場](http://www.kotsu.metro.tokyo.jp/toden/stations/arakawakuyakushomae/index.html)

[Rakawakuyashomae](../Category/日本鐵路車站_A.md "wikilink")
[Category:荒川線車站](../Category/荒川線車站.md "wikilink")
[Category:荒川區鐵路車站](../Category/荒川區鐵路車站.md "wikilink")
[Category:1913年啟用的鐵路車站](../Category/1913年啟用的鐵路車站.md "wikilink")
[Category:以區命名的鐵路車站](../Category/以區命名的鐵路車站.md "wikilink")
[Category:以建築物命名的鐵路車站](../Category/以建築物命名的鐵路車站.md "wikilink")