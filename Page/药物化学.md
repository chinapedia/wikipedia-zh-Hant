**药物化学（）**，简称**药化**，是建立在[化学和](../Page/化学.md "wikilink")[生物学基础上](../Page/生物学.md "wikilink")，对药物结构和活性进行研究的一门学科。研究内容涉及发现、修饰和优化[先导化合物](../Page/先导化合物.md "wikilink")，从分子水平上揭示药物及具有[生理活性物质的作用机理](../Page/生理活性物质.md "wikilink")，研究[药物及](../Page/药物.md "wikilink")[生理活性物质在体内的代谢过程](../Page/生理活性物质.md "wikilink")。

药物化学的任务包括：研究药物的化学结构和活性间的关系（[构效关系](../Page/构效关系.md "wikilink")）；药物化学结构与[物理化学性质的关系](../Page/物理化学性质.md "wikilink")；阐明药物与[受体的相互作用](../Page/受体.md "wikilink")；鉴定药物在体内[吸收](../Page/吸收.md "wikilink")、[转运](../Page/转运.md "wikilink")、分布的情况及[代谢产物](../Page/代谢产物.md "wikilink")；通过药物[分子设计或对](../Page/分子设计.md "wikilink")[先导化合物的化学修饰获得](../Page/先导化合物.md "wikilink")[新化学实体创制新药](../Page/新化学实体.md "wikilink")。

## 药物化学的历史

19世纪以前的药学作为商品学的一个分支学科，研究的对象是一些天然来源的药物，直到19世纪开始人们从一些天然药物中陆续分离出一些具有生理活性的化学物质(从[鴉片中分离出](../Page/鴉片.md "wikilink")[镇痛药](../Page/镇痛药.md "wikilink")[吗啡](../Page/吗啡.md "wikilink")、从[颠茄中分离出](../Page/颠茄.md "wikilink")[抗胆碱药](../Page/抗胆碱药.md "wikilink")[阿托品](../Page/阿托品.md "wikilink")、从[金鸡纳树皮中分离出](../Page/金鸡纳树.md "wikilink")[抗疟药](../Page/抗疟药.md "wikilink")[奎宁等](../Page/奎宁.md "wikilink"))，以药物分子为主要研究对象的药物化学开始从原来的药物学中独立出来成为一个独立的学科。

随着包括[化学](../Page/化学.md "wikilink")、[生理学](../Page/生理学.md "wikilink")、[生物化学在内的众多相关学科的发展](../Page/生物化学.md "wikilink")，人们对药物分子结构的认知更加清楚，对药物结构和药物活性之间的关系也更加清晰，并产生了解释药物药理作用机理的众多理论，在这些理论的指导下，人们开始对有生理活性的天然来源的药物分子结构进行修饰改造。同时，有机化学尤其是有机合成理论和技术的发展使得人们能够轻松合成较复杂的分子，人们对药物分子改造的设想也得以实现，并使得合成药物取代了天然来源的提取药物成为人们主要应用的药品。

近年来，[分子生物学](../Page/分子生物学.md "wikilink")、[基因组学和](../Page/基因组学.md "wikilink")[信息技术飞速发展](../Page/信息技术.md "wikilink")，人们对一些疾病机理的了解更加清楚，可以对生物大分子和药物小分子的相互作用过程进行计算模拟，定量分析药物的结构和活性的关系，20世纪60年代[Hansch和](../Page/Hansch.md "wikilink")[藤田创立了基于二维定量构效关系的Hansch方法](../Page/藤田.md "wikilink")，标志-{}-着药物化学进入了[合理药物设计](../Page/合理药物设计.md "wikilink")(Rational
Drug Design)的阶段，现在的药物化学家可以借助高性能计算机的帮助，定量地预测某一药物分子结构的生理活性，有目标地设计药物分子。

## 引用

## 参见

  - [药学](../Page/药学.md "wikilink")
  - [药理学](../Page/药理学.md "wikilink")
  - [药剂学](../Page/药剂学.md "wikilink")
  - [调剂学](../Page/调剂学.md "wikilink")
  - [合成药物列表](../Page/合成药物列表.md "wikilink")

{{-}}

[Category:化學分支](../Category/化學分支.md "wikilink")
[藥物化学](../Category/藥物化学.md "wikilink")
[藥學](../Category/藥學.md "wikilink")
[Category:化學信息學](../Category/化學信息學.md "wikilink")