[KLM_hoofdkantoor_Amstelveen.jpg](https://zh.wikipedia.org/wiki/File:KLM_hoofdkantoor_Amstelveen.jpg "fig:KLM_hoofdkantoor_Amstelveen.jpg")
[KLMS.jpg](https://zh.wikipedia.org/wiki/File:KLMS.jpg "fig:KLMS.jpg")的荷航機群\]\]
**荷蘭皇家航空**（，，意譯為「皇家航空公司」，目前的全球形象以為自稱），通常簡稱**荷航**，是一家以[荷蘭](../Page/荷蘭.md "wikilink")[阿姆斯特爾芬為總部的國際航空公司](../Page/阿姆斯特爾芬.md "wikilink")，為全球歷史最悠久的航空公司。目前該航空是[法國航空-荷蘭皇家航空集團](../Page/法国航空-KLM.md "wikilink")（Air
France-KLM Group）的子公司。KLM是荷蘭的[国家航空公司](../Page/国家航空公司.md "wikilink")。

目前Air
France-KLM是法國及荷蘭的主要航空公司，其中KLM主要是經營以阿姆斯特丹[史基浦機場](../Page/史基浦機場.md "wikilink")（Schiphol
Airport）為航空樞紐的部分，並將總部設於[阿姆斯特丹史基浦機場](../Page/阿姆斯特丹史基浦機場.md "wikilink")。

法國航空在2004年5月收購荷航，並因此組成了[法航-荷航集團](../Page/法国航空-KLM.md "wikilink")。法航-荷航集團在法國的法律之下成立。

## 歷史

[Douglas_C-47A_PH-TBP_KLM_ed_Ringway_20.05.47_edited-2.jpg](https://zh.wikipedia.org/wiki/File:Douglas_C-47A_PH-TBP_KLM_ed_Ringway_20.05.47_edited-2.jpg "fig:Douglas_C-47A_PH-TBP_KLM_ed_Ringway_20.05.47_edited-2.jpg")（1947年）\]\]
[Douglas_DC-7CF_PH-DSE_KLM_RWY_06.11.64_edited-3.jpg](https://zh.wikipedia.org/wiki/File:Douglas_DC-7CF_PH-DSE_KLM_RWY_06.11.64_edited-3.jpg "fig:Douglas_DC-7CF_PH-DSE_KLM_RWY_06.11.64_edited-3.jpg")貨機（1964年）\]\]
[KLM_Boeing_747-206_Volpati-1.jpg](https://zh.wikipedia.org/wiki/File:KLM_Boeing_747-206_Volpati-1.jpg "fig:KLM_Boeing_747-206_Volpati-1.jpg")
荷航創立於1919年10月7日，是現在歷史最悠久且仍以原有名稱運作的航空公司。荷航的首個航班是在1920年5月17日由[倫敦飛往阿姆斯特丹](../Page/倫敦.md "wikilink")，載有兩名[英國新聞記者及一些](../Page/英國.md "wikilink")[報紙](../Page/報紙.md "wikilink")。

在成立之初，荷航在歐洲已取得領導者的地位，他們的機隊有國產的福克客機（Fokker），亦有美製的客機。1924年，他們的航線更伸展至荷蘭於[遠東的](../Page/遠東.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")--[印尼的](../Page/印尼.md "wikilink")[巴達維亞](../Page/巴達維亞.md "wikilink")（今天的[雅加達](../Page/雅加達.md "wikilink")），這條航線於1929年提升為定期航班；1934年12月開辦了首條跨[大西洋航線](../Page/大西洋.md "wikilink")--荷蘭至南美的荷屬[庫拉索](../Page/庫拉索.md "wikilink")。此時荷航的航線網絡已遍佈全世界。直至[第二次世界大戰爆發](../Page/第二次世界大戰.md "wikilink")，荷航在荷蘭被[納粹德軍吞併後一度停業](../Page/納粹.md "wikilink")，在印尼的業務亦於[太平洋戰爭爆發](../Page/太平洋戰爭.md "wikilink")，[日本佔領印尼後全面停業](../Page/日本.md "wikilink")。值得一提，在大戰期間，荷航接收了4架[道格拉斯DC-5飛機](../Page/道格拉斯DC-5.md "wikilink")，是世上唯一使用過此型飛機的民用航空公司。戰後荷航復業並火速重新發展，於1946年5月21日更開辦[紐約航線](../Page/紐約.md "wikilink")，是歐洲大陸首間飛行紐約航線的航空公司。

2004年5月，荷航被法國航空收購。荷航本身的獨立地位在收購後5年內維持不變，但最終其運作被認為會與法國航空合併。而荷航與[美國西北航空的長期聯營關係](../Page/美國西北航空.md "wikilink")，暫時不會改變。兩間航空公司在2004年9月同時加入法國航空牽頭的天合聯盟。

## 子公司

[Fokker70-lba.jpg](https://zh.wikipedia.org/wiki/File:Fokker70-lba.jpg "fig:Fokker70-lba.jpg")所屬的一架[福克F70](../Page/福克F70.md "wikilink")，於[英國](../Page/英國.md "wikilink")[利茲布拉德福德國際機場](../Page/利茲布拉德福德國際機場.md "wikilink")。\]\]
荷航的子公司包括荷蘭皇家城市短途航空（KLM Cityhopper）、荷航空運及荷亞航（KLM Asia），KLM
Exel則為荷航與荷蘭當地[包機航空公司Air](../Page/包機航空公司.md "wikilink")
Exel合作時所用的名稱，但該合作案已於2004年12月時結束。荷航擁有[泛航航空](../Page/泛航航空.md "wikilink")（Transavia）的全部股權，[馬丁航空](../Page/馬丁航空.md "wikilink")（Martinair）50%的股權及[肯亞航空](../Page/肯亞航空.md "wikilink")（Kenya
Airways）26%股權。荷航股票在[阿姆斯特丹](../Page/阿姆斯特丹證券交易所.md "wikilink")、[紐約及](../Page/紐約証券交易所.md "wikilink")[巴黎證券交易所上市](../Page/巴黎証券交易所.md "wikilink")。

### 荷蘭亞洲航空

[777-300ER_KLM_ASIA_SBGR_(38413450304).jpg](https://zh.wikipedia.org/wiki/File:777-300ER_KLM_ASIA_SBGR_\(38413450304\).jpg "fig:777-300ER_KLM_ASIA_SBGR_(38413450304).jpg")

[KLM_Asia_Boeing_777-300ER_(PH-BVB)_at_Schiphol.jpeg](https://zh.wikipedia.org/wiki/File:KLM_Asia_Boeing_777-300ER_\(PH-BVB\)_at_Schiphol.jpeg "fig:KLM_Asia_Boeing_777-300ER_(PH-BVB)_at_Schiphol.jpeg")

[PH-BQL_KLM_Royal_Dutch_Airlines_Boeing_777-206(ER),_landing_at_Schiphol_(AMS_-_EHAM),_Netherlands,_pic2.JPG](https://zh.wikipedia.org/wiki/File:PH-BQL_KLM_Royal_Dutch_Airlines_Boeing_777-206\(ER\),_landing_at_Schiphol_\(AMS_-_EHAM\),_Netherlands,_pic2.JPG "fig:PH-BQL_KLM_Royal_Dutch_Airlines_Boeing_777-206(ER),_landing_at_Schiphol_(AMS_-_EHAM),_Netherlands,_pic2.JPG")

[PH-BQM_Boeing_777-206_ER_KLM_Asia_(25656163973).jpg](https://zh.wikipedia.org/wiki/File:PH-BQM_Boeing_777-206_ER_KLM_Asia_\(25656163973\).jpg "fig:PH-BQM_Boeing_777-206_ER_KLM_Asia_(25656163973).jpg")

[KLM_777-200ER_on_short_final_for_runway_27_(35534396320).jpg](https://zh.wikipedia.org/wiki/File:KLM_777-200ER_on_short_final_for_runway_27_\(35534396320\).jpg "fig:KLM_777-200ER_on_short_final_for_runway_27_(35534396320).jpg")

[PH-BFH_KLM_Asia_"Hongkong"_(4371229106).jpg](https://zh.wikipedia.org/wiki/File:PH-BFH_KLM_Asia_"Hongkong"_\(4371229106\).jpg "fig:PH-BFH_KLM_Asia_\"Hongkong\"_(4371229106).jpg")

[EHAM18072010_PH-BFC_KLM_(4806800714).jpg](https://zh.wikipedia.org/wiki/File:EHAM18072010_PH-BFC_KLM_\(4806800714\).jpg "fig:EHAM18072010_PH-BFC_KLM_(4806800714).jpg")

[Boeing_747-406M,_KLM_Asia_AN0136153.jpg](https://zh.wikipedia.org/wiki/File:Boeing_747-406M,_KLM_Asia_AN0136153.jpg "fig:Boeing_747-406M,_KLM_Asia_AN0136153.jpg")

與[英航設立](../Page/英航.md "wikilink")[英亞航](../Page/英亞航空.md "wikilink")、[日航設立](../Page/日本航空.md "wikilink")[日亞航的作法類似](../Page/日本亞細亞航空.md "wikilink")，荷航的子公司**荷蘭亞洲航空**（英語:**KLM
Asia**，簡稱**荷亞航**）是荷蘭皇家航空在台灣註冊的全資子公司，該公司成立於1995年，基於政治考量，為了經營與[台灣之間的航班而特別創立的](../Page/台灣.md "wikilink")，有定期航線往返於[馬尼拉](../Page/馬尼拉.md "wikilink")-[台北](../Page/台北.md "wikilink")-阿姆斯特丹之間。在[土耳其航空於](../Page/土耳其航空.md "wikilink")2015年開航台北-伊斯坦堡航線前，曾長期是唯一一家仍以自有飛機飛航台灣的歐洲航空公司。荷亞航所使用的機種原為6架[波音747-406M廣體客貨機](../Page/波音747.md "wikilink")（Combi），用來飛航經[曼谷的台北航線](../Page/曼谷.md "wikilink")。自荷亞航將[台北航線改為直飛後](../Page/台北.md "wikilink")，使用機種已改為航程較長的7架[波音777-206ER和](../Page/波音777.md "wikilink")2架[波音777-306ER客機](../Page/波音777.md "wikilink")。荷亞航塗裝上大致與一般的荷航機隊相同，但除了航空公司名稱多了「Asia」的字樣外，機尾[垂直尾翼部分上繪上原本荷航企業識別標誌上的皇冠再加上](../Page/垂直尾翼.md "wikilink")「Asia」的字樣取代之，在機身前中段則以繁體中文寫上「荷蘭亞洲航空公司」。荷亞航雖然名義上是專門為了台灣的航線而設，但其機隊有時也會因調度需要飛航歐美之間完全與台灣或亞洲無關的航線甚至是中國，而相反的，飛航台北的班機也不見得全都是荷亞航的機隊，由荷航自有機隊飛航的情況時有所見。

荷蘭亞洲航空也是目前唯一仍使用「亞洲」名義模式飛行台灣的航空公司。

## 航點

以阿姆斯特丹史基普機場為樞紐的荷蘭皇家航空，服務的航點遍佈全球五大洲大部分主要國家的首都。除了航線密度最高的[西歐與](../Page/西歐.md "wikilink")[中歐地區外](../Page/中歐.md "wikilink")，[美國是除了歐洲各國之外荷航航線數量最多的國家](../Page/美國.md "wikilink")，[亞洲的](../Page/亞洲.md "wikilink")[中國則以](../Page/中華人民共和國.md "wikilink")5條航線排名第二。除此之外，由於[荷蘭王國仍然在](../Page/荷蘭王國.md "wikilink")[加勒比海地區擁有數個原本是其海外屬地的自治國](../Page/加勒比海.md "wikilink")，為了維持這些自治國與母國之間的聯繫，荷航在此地區擁有較其他歐洲航空公司更多的航點，

## 現役機隊

截至2018年1月，荷蘭皇家航空機隊平均機齡11.4年，擁有以下飛機\[1\]\[2\]\[3\]：

<center>

| <font style="color:white;">荷蘭皇家航空機隊                   |
| ----------------------------------------------------- |
| <font style="color:white;">機型                         |
| <abbr title="商務艙"><font style="color:white;">C</abbr> |
| [空中巴士A330-203](../Page/空中巴士A330.md "wikilink")        |
| [空中巴士A330-303X](../Page/空中巴士A330.md "wikilink")       |
| [空中巴士A350-900](../Page/空中巴士A350.md "wikilink")        |
| [波音737-7K2](../Page/波音737.md "wikilink")              |
| [波音737-8K2](../Page/波音737.md "wikilink")              |
| [波音737-9K2](../Page/波音737.md "wikilink")              |
| [波音747-406](../Page/波音747.md "wikilink")              |
| [波音747-406M](../Page/波音747.md "wikilink")             |
| [波音777-206ER](../Page/波音777.md "wikilink")            |
| [波音777-306ER](../Page/波音777.md "wikilink")            |
| [波音787-9](../Page/波音787.md "wikilink")                |
| [波音787-10](../Page/波音787.md "wikilink")               |
| <font style="color:white;">貨運機隊                       |
| [波音747-406ERF](../Page/波音747.md "wikilink")           |
| 總數                                                    |

</center>

<File:KLM> Airbus A330-200
Meulemans.jpg|荷蘭皇家航空[空中巴士A330-203](../Page/空中巴士A330.md "wikilink")，攝於[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")[史基普機場](../Page/史基普機場.md "wikilink")。
<File:Klm> Boeing 777-300 skyteam.jpg|荷蘭皇家航空波音777-300ER天合聯盟彩繪。
<File:PH-BXW> 737 KLM
BCN.jpg|荷蘭皇家航空[波音737-800新塗裝](../Page/波音737.md "wikilink")。
<File:Boeing> 737 KLM PH-BXO AMS February
2011.jpg|荷蘭皇家航空[波音737-9K2披上](../Page/波音737.md "wikilink")[天合聯盟塗裝](../Page/天合聯盟.md "wikilink")，攝於阿姆斯特丹史基普機場。
<File:KLM> Boeing 747-400 landing SXM
Coleman.jpg|荷蘭皇家航空[波音747-406](../Page/波音747.md "wikilink")「[瓜亞基爾號](../Page/瓜亞基爾.md "wikilink")」（City
of
Guayaquil）正降落於[聖馬丁島](../Page/聖馬丁島.md "wikilink")[朱麗安娜公主國際機場](../Page/朱麗安娜公主國際機場.md "wikilink")。
<File:PH-BFV> KLM Royal Dutch Airlines Boeing 747-406(M) s-n 28460
"Vancouver - City of Vancouver"
(26393573999).jpg|荷蘭皇家航空[波音747-406M](../Page/波音747.md "wikilink")，攝於[洛杉磯國際機場](../Page/洛杉磯國際機場.md "wikilink")。
<File:KLM> Boeing 777-206ER; PH-BQB@AMS;09.12.2010 590em
(5257388222).jpg|荷蘭皇家航空[波音777-206ER](../Page/波音777.md "wikilink")。
<File:PH-BQL@HKG>
(20181101140317).jpg|荷蘭亞洲航空[波音777-206ER](../Page/波音777.md "wikilink")，攝於[香港國際機場](../Page/香港國際機場.md "wikilink")。
<File:KLM> Boeing 777-300ER PH-BVA SIN
2012-1-2.png|荷蘭皇家航空[波音777-306ER](../Page/波音777.md "wikilink")，攝於[新加坡](../Page/新加坡.md "wikilink")[樟宜國際機場](../Page/樟宜國際機場.md "wikilink")。
<File:KLM> PH-BVA in TPE AUG 2017.jpg|荷蘭皇家航空Orange Pride彩繪機。
<File:PH-BHF@PEK>
(20180207100445).jpg|荷兰皇家航空[波音787-9](../Page/波音787.md "wikilink")，摄于[北京](../Page/北京.md "wikilink")[首都国际机场](../Page/首都国际机场.md "wikilink")
<File:KLM> Cargo Boeing 747-400F
Prasertwit-1.jpg|荷蘭皇家航空[波音747-406ERF](../Page/波音747.md "wikilink")「阿姆斯特丹市徽號」（Wapen
van
Amsterdam），攝於[曼谷](../Page/曼谷.md "wikilink")[蘇凡納布機場](../Page/蘇凡納布機場.md "wikilink")。

### 退役機隊

荷兰皇家航空自创立以来，曾使用过以下机型：\[4\]

<center>

<table>
<tbody>
<tr class="odd">
<td><table>
<caption><strong>荷兰皇家航空退役机队</strong></caption>
<thead>
<tr class="header">
<th><p><span style="color:white;">机型</p></th>
<th><p><span style="color:white;">引进年份</p></th>
<th><p><span style="color:white;">退役年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/洛克希德超级伊莱克特拉.md" title="wikilink">洛克希德超级伊莱克特拉-14</a></p></td>
<td><p>1938</p></td>
<td><p>1948</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道格拉斯DC-3.md" title="wikilink">道格拉斯DC-3</a></p></td>
<td><p>1936</p></td>
<td><p>1964</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福克F.XXXVI.md" title="wikilink">福克F.XXXVI</a></p></td>
<td><p>1935</p></td>
<td><p>1939</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福克F.XXII.md" title="wikilink">福克F.XXII</a></p></td>
<td><p>1935</p></td>
<td><p>1939</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/道格拉斯DC-2.md" title="wikilink">道格拉斯DC-2</a></p></td>
<td><p>1934</p></td>
<td><p>1946</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福克F.XX.md" title="wikilink">福克F.XX</a></p></td>
<td><p>1933</p></td>
<td><p>1936</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福克F.XVIII.md" title="wikilink">福克F.XVIII</a></p></td>
<td><p>1932</p></td>
<td><p>1946</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福克F.XII.md" title="wikilink">福克F.XII</a></p></td>
<td><p>1931</p></td>
<td><p>1936</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福克F.IX.md" title="wikilink">福克F.IX</a></p></td>
<td><p>1930</p></td>
<td><p>1936</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福克F.VIII.md" title="wikilink">福克F.VIII</a></p></td>
<td><p>1927</p></td>
<td><p>1940</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福克F.VII.md" title="wikilink">福克F.VII</a></p></td>
<td><p>1925</p></td>
<td><p>1936</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福克F.III.md" title="wikilink">福克F.III</a></p></td>
<td><p>1921</p></td>
<td><p>1930</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福克F.II.md" title="wikilink">福克F.II</a></p></td>
<td><p>1920</p></td>
<td><p>1924</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈维兰DH.16.md" title="wikilink">哈维兰DH.16</a></p></td>
<td><p>1920</p></td>
<td><p>1924</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/道格拉斯DC-10.md" title="wikilink">麦道DC-10-30</a></p></td>
<td><p>1972</p></td>
<td><p>1995</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-200</a></p></td>
<td><p>1971</p></td>
<td><p>2004(PH-BUF在<a href="../Page/特內里費空難.md" title="wikilink">特內里費空難中被焚化</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/道格拉斯DC-9.md" title="wikilink">道格拉斯DC-9</a></p></td>
<td><p>1966</p></td>
<td><p>1989</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道格拉斯DC-8.md" title="wikilink">道格拉斯DC-8</a></p></td>
<td><p>1960</p></td>
<td><p>1985</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洛克希德L-188.md" title="wikilink">洛克希德L-188</a></p></td>
<td><p>1959</p></td>
<td><p>1969</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/维克斯子爵.md" title="wikilink">维克斯子爵</a></p></td>
<td><p>1957</p></td>
<td><p>1966</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/道格拉斯DC-7.md" title="wikilink">道格拉斯DC-7</a></p></td>
<td><p>1953</p></td>
<td><p>1966</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洛克希德超级星座.md" title="wikilink">洛克希德超级星座L-1049</a></p></td>
<td><p>1953</p></td>
<td><p>1966</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/康维尔340.md" title="wikilink">康维尔340</a></p></td>
<td><p>1953</p></td>
<td><p>1964</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道格拉斯DC-4.md" title="wikilink">道格拉斯DC-4</a></p></td>
<td><p>1946</p></td>
<td><p>1958</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/康维尔240.md" title="wikilink">康维尔240</a></p></td>
<td><p>1948</p></td>
<td><p>1959</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道格拉斯DC-6.md" title="wikilink">道格拉斯DC-6</a></p></td>
<td><p>1948</p></td>
<td><p>1963</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/C-54运输机.md" title="wikilink">道格拉斯C-54空中霸王</a></p></td>
<td><p>1945</p></td>
<td><p>1959</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道格拉斯DC-5.md" title="wikilink">道格拉斯DC-5</a></p></td>
<td><p>1940</p></td>
<td><p>1941</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音767.md" title="wikilink">波音767-300ER</a></p></td>
<td><p>1995</p></td>
<td><p>2007</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥道MD-11.md" title="wikilink">麥道MD-11</a></p></td>
<td><p>1993</p></td>
<td><p>2014</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-400</a></p></td>
<td><p>1989</p></td>
<td><p>2011</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-300</a></p></td>
<td><p>1986</p></td>
<td><p>2011</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A310.md" title="wikilink">空中客车A310-200</a></p></td>
<td><p>1983</p></td>
<td><p>1997</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-300</a></p></td>
<td><p>1983</p></td>
<td><p>2004</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

</center>

## 空難

  - 1977年3月27日，荷蘭皇家航空一架名為『萊茵號』的波音747-200在執行KL4805次航班時，於[特內里費機場滑行起飛時與](../Page/特內里費空難.md "wikilink")[泛美航空的一架波音](../Page/泛美航空.md "wikilink")747-100[相撞](../Page/特內里費空難.md "wikilink")，造成兩架飛機上共583人遇難，其中荷航飛機上234人全部遇難。這次空難為航空史上最嚴重的空難。

## 相關條目

  - [皇家航空](../Page/皇家航空.md "wikilink")：其他也使用「皇家航空」為名的航空公司。

## 參考資料

<references/>

## 外部連結

  - [官方網站](https://www.klm.com/)

[Category:法荷航集团](../Category/法荷航集团.md "wikilink")
[Category:荷蘭航空公司](../Category/荷蘭航空公司.md "wikilink")
[Category:1919年成立的航空公司](../Category/1919年成立的航空公司.md "wikilink")
[Category:总部在荷兰的跨国公司](../Category/总部在荷兰的跨国公司.md "wikilink")

1.
2.
3.  <http://www.airfleets.net/flottecie/KLM.htm>
4.  [KLM fleet](http://www.airfleets.net/flottecie/KLM.htm) Airfleets.
    Retrieved 29 November 2009.