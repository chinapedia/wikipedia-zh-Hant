[cmholland.jpg](https://zh.wikipedia.org/wiki/File:cmholland.jpg "fig:cmholland.jpg")
[Holland_tunnel.jpg](https://zh.wikipedia.org/wiki/File:Holland_tunnel.jpg "fig:Holland_tunnel.jpg")
[New_York_Land_Ventilation_Building_south_side_119149pv.jpg](https://zh.wikipedia.org/wiki/File:New_York_Land_Ventilation_Building_south_side_119149pv.jpg "fig:New_York_Land_Ventilation_Building_south_side_119149pv.jpg")
**荷蘭隧道**（[英語](../Page/英語.md "wikilink")：）是一條位於[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市的](../Page/紐約市.md "wikilink")[隧道](../Page/隧道.md "wikilink")，穿越[哈德遜河下連接紐約市的](../Page/哈德遜河.md "wikilink")[曼哈頓與](../Page/曼哈頓.md "wikilink")[紐澤西州的](../Page/紐澤西州.md "wikilink")[澤西市](../Page/澤西市.md "wikilink")，並載有[78號州際高速公路](../Page/78号州际公路.md "wikilink")。此隧道是紐約市仅有的兩條穿越哈德遜河下的隧道，另一條隧道為[林肯隧道](../Page/林肯隧道.md "wikilink")。荷蘭隧道最初的名稱是**哈德遜河車行隧道**（）或是**運河街隧道**（）。在1920年開工並在1927年完工通車，其名稱是以在完工前已去世的隧道設計者[克里夫·密爾本·荷蘭](../Page/克里夫·密爾本·荷蘭.md "wikilink")（Clifford
Milburn
Holland）命名以茲紀念。荷蘭隧道是最初幾個開始使用[通氣塔設計的隧道](../Page/通氣塔.md "wikilink")，這些通氣塔以直徑的風扇與通風道為隧道內的車輛提供新鮮空氣，這項設計是為了應對大量增長的汽車使用量所導致的大量廢氣，使得隧道內的廢氣得以排出。

荷蘭隧道的設計為一對管狀的隧道組成，兩管各自提供兩線20英尺寬的單向車行道。北管的長度為比南管的稍微長一點。兩管都是沉沒於哈德遜河的河床之上，隧道的最低點大約位於河面之下。在紐澤西邊建有九線道的[收費站](../Page/收費站.md "wikilink")，[電子收費系統為](../Page/電子收費系統.md "wikilink")[E-ZPass](../Page/E-ZPass.md "wikilink")。根據[紐約與新澤西港口事務管理局的統計資料顯示](../Page/紐約與新澤西港口事務管理局.md "wikilink")，2002年總共有15,764,000輛車通過隧道，2004年有33,926,000輛車，2005年有33,964,000輛車通過。在1993年時荷蘭隧道被加入[國家歷史地標登記](../Page/國家史蹟名錄.md "wikilink")（）內（建物編號\#93001619）。

[Category:紐約市隧道](../Category/紐約市隧道.md "wikilink")
[Category:州際公路系統](../Category/州際公路系統.md "wikilink")