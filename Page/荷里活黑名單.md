**-{zh-hk:荷里活;zh-cn:好莱坞;zh-tw:好萊塢;}-黑名單**，更適當的名稱為**“娛樂業黑名單”**，這名單詳述[美國](../Page/美國.md "wikilink")1940和50年代中，因政治信仰而被剝奪工作權的[演員](../Page/演員.md "wikilink")、[導演](../Page/導演.md "wikilink")、[編劇](../Page/編劇.md "wikilink")、[音樂家和其他娛樂業人士](../Page/音樂家.md "wikilink")，其中有真實的，也有被懷疑的。如果艺人被认为是美国共产党党员或对共产主义表示过赞许或同情、参与过认为是有社会主义背景的自由主义或人道主义政治活动、或者拒绝帮助政府调查共产党活动，都有可能被列入黑名单；一些人被列入黑名單僅僅是因為他們的名字出現在錯誤地點和時間。即使在最嚴密的執行期間—40年代後期到50年代後期，黑名單也很少被確定和核實，但它對[美國的娛樂產業發展造成了直接損害](../Page/美國.md "wikilink")，相關人士為了生計經常被迫背叛朋友或原則，整個產業籠罩在意識形態審查的陰影下。

1947年11月25日，十位[作家和導演](../Page/作家.md "wikilink")（史称好莱坞十君子）拒絕提供證詞給[眾議院非美活動調查委員會](../Page/眾議院非美活動調查委員會.md "wikilink")，繼而因[蔑視國會被傳訊](../Page/蔑視國會.md "wikilink")，第二天第一個黑名單便被設立了。一班[電影製作室的经理](../Page/電影製作室.md "wikilink")，在《[美國電影協會](../Page/美國電影協會.md "wikilink")》支持下，发表了大家所認識的《[華德福聲明](../Page/華德福聲明.md "wikilink")》宣布解雇这十位電影從業人員。1950年6月22日，名為《[紅色頻道](../Page/紅色頻道.md "wikilink")》的小冊子出版，它以"紅色法西黨員和他們的支持者"為內容，列出151個娛樂業專家的名字，這也是出版以來最公開詳盡的黑名單;；除了被列名單的那些人之外，許多其它藝人也發現他們同樣處於困難，在許多情況下都不可能在娛樂界獲得工作。1960年，黑名單被當時「-{zh-hk:荷里活;zh-cn:好莱坞;zh-tw:好萊塢;}-十君子」一名頑固成員—[戴爾頓·特蘭波正式打破](../Page/戴爾頓·特蘭波.md "wikilink")，他公開承認作為電影《[萬夫莫敵](../Page/斯巴達克斯_\(电影\).md "wikilink")》和《[出埃及記](../Page/出埃及記_\(美國電影\).md "wikilink")》的編劇；然而有部分被列入黑名單的人士在很多年後，仍被拒於娛樂行業門外。

## 概要

### 黑名單的開始(1947年)

**-{zh-hk:荷里活;zh-cn:好莱坞;zh-tw:好萊塢;}-黑名單**源於30年代和40年代初的事件。在那個時代，之前[蘇聯總理](../Page/蘇聯.md "wikilink")[約瑟夫·維薩里奧諾維奇·史達林的恐怖統治在西方人盡皆知](../Page/史達林.md "wikilink")，[美國共產黨吸引了很多追隨者](../Page/美國共產黨.md "wikilink")，而很多這些追隨者都是在藝術和娛樂領域工作的年輕[理想主義者](../Page/唯心主義.md "wikilink")。[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，當時[美國和蘇聯是](../Page/美國.md "wikilink")[盟國](../Page/盟國.md "wikilink")，美國共產黨的追隨者到達了峰頂50,000人。\[1\]

但這觀念很快在第二次世界大戰末改變了，[共產主義日益增長成為美國恐懼和仇恨的焦點](../Page/共產主義.md "wikilink")。"[紅色恐慌](../Page/紅色恐慌.md "wikilink")"分別被蘇聯鎮壓戰爭後的東部和中歐國家的報告；和[美國共和黨在](../Page/美國共和黨.md "wikilink")1946年次國會競選勝利，保守黨政治權勢在美國的增長而刺激了。另外看得出美國共和黨控制了[1946年美國安置競選和](../Page/1946年美國安置競選.md "wikilink")[1946年美國參議院競選](../Page/1946年美國參議院競選.md "wikilink")。[在1947年](../Page/在1947年.md "wikilink")10月，一些在[荷里活電影工業工作的人員被召喚出現於](../Page/荷里活電影工業.md "wikilink")[眾議院非美活動調查委員會前面](../Page/眾議院非美活動調查委員會.md "wikilink")，宣稱他們的意圖是調查是否如學者[Richard
A.
Schwartz描述](../Page/Richard_A._Schwartz.md "wikilink")「共產黨已成功在荷里活影片裡灌輸共產主義訊息和價值。」\[2\]這批美國電影專家主要是編劇、演員、導演、製作人和其他人著名或聲稱是美國共產黨的成員。有四十三個人被放在證人名單，十九人宣稱他們不會提供證據，實際上只有十一人被召喚到委員會前。十一位"不友善的證人"
，一個逃亡的編劇[貝爾托·布萊希特最後選擇回來回答委員會的問題](../Page/貝爾托·布萊希特.md "wikilink")\[3\]。而其它十位拒絕回答的，則引用[美國憲法第一修正案對言論和集會自由的權力](../Page/美國憲法第一修正案.md "wikilink")，拒絕回答委員會的任何問題。他們拒絕的重要問題是"你現在是或你曾經是美國共產黨的成員?"—這從未不是違法的。(實際上，每個曾一次或多於一次是成員；或他們以前是和只是短暫是，都是共產黨成員。)這十人被正式控告[蔑視國會](../Page/蔑視國會.md "wikilink")，而[美國議院代表開始對他們的訴訟](../Page/美國眾議院.md "wikilink")。

[Huac.jpg](https://zh.wikipedia.org/wiki/File:Huac.jpg "fig:Huac.jpg")

根據"-{zh-hk:荷里活;zh-cn:好莱坞;zh-tw:好萊塢;}-十人"藐視HUAC—除了拒絕作證，許多企圖以聲明誹謗委員會的調查是違反憲法的政治壓力登上電影工業，展示它的"反顛覆"真實。在10月，聽訊仍然舉行中，[美國電影協會主席](../Page/美國電影協會.md "wikilink")[Eric
Johnston](../Page/Eric_Johnston.md "wikilink")，宣稱他從未「雇用任何一個被證明或被公認為共產主義員的人士，因為他們只是分裂性的勢力，並且我不想要他們在周圍。」\[4\]1947年11月17日，[電影演員協會投票去使它的幹事發誓是非共產主義員的承諾](../Page/電影演員協會.md "wikilink")。接下來的星期，1947年11月24日，眾院以346對17的票數批准引用對抗“荷里活十人”為蔑視國會。次日，隨著[紐約](../Page/紐約.md "wikilink")[华道夫阿斯多里亚酒店的](../Page/华道夫阿斯多里亚酒店.md "wikilink")[電影業行政會議](../Page/電影業行政.md "wikilink")，MPAA(美國電影協會)主席Johnston以行政代表發布了新聞發布會，也就是現在指的[華德福聲明](../Page/華德福聲明.md "wikilink")。\[5\]聲明宣稱，那十個被解雇或暫停工作而沒有薪水和未被重新被雇用，直至他們被清除了蔑視控告和發誓他們不是共產主義者。首個荷里活名單就在這時生效。

### 名單的發展(1948年–1950年)

HUAC聆訊不能呈出任何-{zh-hk:荷里活;zh-cn:好莱坞;zh-tw:好萊塢;}-秘密地傳播共產主義宣傳的證據，但娛樂業仍然被改變了。調查餘波是[Floyd
Odlum決定的一個主要因素](../Page/Floyd_Odlum.md "wikilink")，他是[雷電華影片公司的最初擁有人](../Page/雷電華影片公司.md "wikilink")，他離開了他的生意。結果，製作室由[霍華德·休斯接管](../Page/霍華德·休斯.md "wikilink")。在1948年5月接管的幾星期裡，休斯解雇了大多數「雷電華影片公司」的員工，並因他政治上支持其餘的調查，而關閉了製作室近一年半。然後，正當「雷電華影片公司」轉回生產的時候，休斯做出一個決定，去解決一個長久聯盟反壟斷的對策來對抗娛樂業的五大製作室。這是瓦解了支配荷里活，並統治了四分一世紀的很多世界電影的[製作室系統的其中一個關鍵步驟](../Page/製作室系統.md "wikilink")。

1948年的春天,"-{zh-hk:荷里活;zh-cn:好莱坞;zh-tw:好萊塢;}-十人"所有人被判了蔑視罪。在一連串不成功的上訴後,案件到達了在[美國最高法院面前](../Page/美國最高法院.md "wikilink");由204個荷里活專家簡單簽署，在提交仲裁協議書中提出維護十人是顧問。在法院拒絕再審之後，1950年"荷里活十人"開始了1年的監禁判決。在1950年9月，十人當中一個—Edward
Dmytryk導演，公開宣佈他曾是共產主義者和準備提供證據去對抗其他人同樣是黨員的。他提早從監獄釋放了;隨者他1951年出庭HUAC，他描述他在黨裡簡要地位和名字稱呼，於事者他的事業得以恢復。\[6\]而其他大多數保持沈默的在[Adrian
Scott](../Page/Adrian_Scott.md "wikilink")（他出產了Dmytryk的電影，如《[Murder, My
Sweet](../Page/Murder,_My_Sweet.md "wikilink")》、《[Cornered](../Page/Cornered_\(film\).md "wikilink")》、《[So
Well
Remembered](../Page/So_Well_Remembered.md "wikilink")》和《[Crossfire](../Page/Crossfire_\(film\).md "wikilink")》）案件後多年來都無法在美國影片和電視業獲得工作，那些電影名字的當中一個是由他前朋友命名的，他沒有獲得任何電影榮譽，直到1972年，並且他從未生產其他特色電影。一些被列入黑名單的人士繼續暗中為荷里活或廣播業寫作，他們使用假名或用那些實際是作家的朋友的名字(那些允許他們的名字被使用在這種方式的稱為"fronts")。204人簽署了顧問訴書，84人將他們自己列入黑名單。\[7\]

一些非政府組織參加了強制執行和擴展黑名單;特別是，美國軍隊，保守黨的退役軍人小組，有助迫使娛樂業排斥那些政治支持不同的人士。1949年，軍隊的美國分派發布了它自己黑名單—128個列入名冊的人被稱全部都參加"共產主義陰謀"。在軍隊名單的名字之中，有知名編劇[麗蓮·海爾曼](../Page/麗蓮·海爾曼.md "wikilink")。\[8\]在那時候海爾曼寫了或貢獻了約十部電影劇本;直到1966年為止，她一直不被荷里活電影製片廠再次僱用。其它有影響的的團體是1947年建立的"美國商業顧問有限公司"。在週刊《Counterattack的》“反對共產主義實情的時事通訊”的訂閱資訊裡，它宣稱它是由一隊前[FBI人員負責經營的](../Page/FBI.md "wikilink")，和任何一個政府機構都沒有任何聯繫。儘管那麼聲稱，但《Counterattack》的編輯似乎可以直接進出使用到[聯邦調查局和HUAC的檔案](../Page/聯邦調查局.md "wikilink");這樣進出使用檔案的結果變得和1950年6月出版物《紅色頻道》廣泛相似。這個《Counterattack》的附件列出的151個在娛樂和廣播新聞事業方面的人，與他們的介入意指採取共產主義或支持共產主義活動的小冊子的紀錄。\[9\]一些被列入名單的，譬如Hellman，已經否認被僱用於在電影、電視和電台領域。《紅色頻道》出版物更意味著更多人士被放進黑名單。

### 《眾議院非美活動調查委員會》報告(1951年)

1951年，HUAC舉行了荷里活和共產主義的第二次調查。在這以前，那些拒絕的合法策略被作證明改變了;而不是依靠第一修正案，他們行使了[美國憲法的第五修正案的保護來以防自我牽累](../Page/美國憲法的第五修正案.md "wikilink")(實際上，共產黨會員資格並非非法)。這通常准許證人，在不被控告為"蔑視國會"下避免"命名名字"，在HUAC保證一個會被加入娛樂業黑名單之前，採取美國憲法的第五修正案"。學者托Thomas
Doherty 描述HUAC聆訊怎麼清除那些政治上從未特別活躍，和共產主義者的懷疑者的黑名單人士:

> 在1951年3月21日，在HUAC之前，演員[Larry
> Parks在作證時是說出了演員](../Page/Larry_Parks.md "wikilink")[Lionel
> Stander的名字](../Page/Lionel_Stander.md "wikilink")。"您認識Lionel
> Stander嗎?" 委員會法律顧問[Frank S.
> Tavenner質問](../Page/Frank_S._Tavenner.md "wikilink")。Parks回復他認識這男人，但有對於他的政治活動則不清楚。之後再沒有認為關於Stander的事情由Parks或委員會說出—沒有指責，沒有影射。但是Stander的電話停止了響。在Parks作證前，Stander在前100天是從事十個電視節目。但之後,什麼工作也沒有了。\[10\]

### 黑名單的頂峰(1952年–1956年)

1952年，電影作家協會—由"荷里活十人"的三名成員的在30年代成立，批准了電影製作室的"[被電影遺忘的](../Page/電影榮譽.md "wikilink")"任何在會議前不能使自己清白的人士，作家[Dalton
Trumbo](../Page/Dalton_Trumbo.md "wikilink")，"荷里活十人"的其中一人和在黑名單非常重要的人，1950年憑幾年前的寫作獲得了電影榮譽，故事以[哥倫比亞電影](../Page/哥倫比亞電影.md "wikilink")"緊急婚禮"為電影劇本根據。接著直到60年代都沒有其他電影榮譽。[Albert
Maltz](../Page/Albert_Maltz.md "wikilink")，在40年代中期寫了電影\<[長袍](../Page/長袍.md "wikilink")\>的原劇本，1953年電影被發行了，但是無處可看見。\[11\]如威廉奧尼恩斯描述，壓力仍然維持在那上面，儘管他們已表面上"清白"了自己:

> 在1952年12月27日，美國軍隊宣佈不同意一部新影片《[紅磨坊](../Page/紅磨坊_\(1952年電影\).md "wikilink")》，擔任主角的是[José
> Ferrer](../Page/José_Ferrer.md "wikilink")，他並不比其他百多位演員於優秀進步，並已經被HUAC調查過。電影本身是根據[亨利·德·土魯斯-羅特列克的生活](../Page/亨利·德·土魯斯-羅特列克.md "wikilink")，並完全沒有涉及政治的。軍隊的九名成員無論如何把這電影栓住，並提升爭論。在這以前，人們沒有冒險。Ferrer立刻發送電報給軍隊的全國司令員，說他很高興加入退伍軍人的"與共產主義的戰鬥"。\[12\]

在這時期，一些重要的報紙專欄作家報道娛樂業，包括[Walter
Winchell](../Page/Walter_Winchell.md "wikilink")、[Hedda
Hopper](../Page/Hedda_Hopper.md "wikilink")、[Victor
Riesel](../Page/Victor_Riesel.md "wikilink")、[Jack
O'Brian](../Page/Jack_O'Brian.md "wikilink"),和George
Sokolsky、正式提供建議應該加入黑名單的名字。\[13\]

### 黑名單的衰落和減弱(1957年–現在)

荷里活黑名單和[約翰·埃德加·胡佛的FBI扣赤色分子帽子活動長期連在一起](../Page/約翰·埃德加·胡佛.md "wikilink")。HUAC的敵人，如律師Bartley
Crum，1947年他曾在委員會前面保衛"荷里活十人"的部份人士，他們被標示為共產主義支持者或危險份子，並把他們設為調查目標。FBI竊聽了Bartley
Crum的電話，看他的郵件，並把他放在連續監視內。這另Bartley
Crum失去了大多數他的客戶，並因無法承受不間斷騷擾的壓力，於1959年自殺身亡。\[14\]

1960年1月20日，導演[奧托·普雷明格公開地宣佈了](../Page/奧托·普雷明格.md "wikilink")[達爾頓·特朗勃](../Page/達爾頓·特朗勃.md "wikilink")
("好萊塢十人"中最知名的成員)是電影《[出埃及記](../Page/出埃及記\(電影\).md "wikilink")》的編劇。六個半月後，《出埃及記》首映，《紐約時代週刊》宣佈了[環球影片會以他編寫的電影](../Page/環球影片公司.md "wikilink")《[萬夫莫敵](../Page/萬夫莫敵\(電影\).md "wikilink")》，給予特朗勃電影榮譽。做這決定的明星是[寇克·道格拉斯](../Page/寇克·道格拉斯.md "wikilink")，現在被認為是這件事的主要負責人。\[15\]在1950年10月6日，《萬夫莫敵》初次上映—這是特朗勃自1950年憑《緊急婚禮》獲得了電影榮譽後，第一部有他名字的電影。1947年以來，他編寫了大約十七電影，但都沒有獲獎。電影《出埃及記》於同年的12月上映；黑名單明顯將近結束，但它的影響在之後的幾年仍有迴響。

最後被列入[麥卡錫主義黑名單的一個主要人物在是John](../Page/麥卡錫主義.md "wikilink") Henry
Faulk，他是下午喜劇廣播節目的主持人，Faulk是一個活躍在美國電視廣播演員聯盟的左派分子。他被AWARE詳細調查，AWARE是其中一個審查個人去簽署"不忠於"共產主義的私人公司。由於AWARE標記Faulk為不合適，於是Faulk被CBS電台解雇了。他幾乎是眾多黑名單受害人的唯一一個，1957年Faulk決定控告AWARE，最後他於1962年贏取這個案件。\[16\]由於法庭的這個判決，那些私人黑名單者和那些用他們的都被放上通知黑名單是負有法律責任的。這事件促進了出版物，如"Counterattack"的結束。\[17\]

2000年以後，電影編劇協會仍然追求去更正50年代和60年代初期電影的電影榮譽，認為應該正確地反映被列入黑名單的作家，譬如Hugo
Butler和Carl Foreman的工作。\[18\]

## 黑名單

### 荷里活十人和其他1947名被列入黑名單的人士

#### 好萊塢十君子

  - [阿爾瓦·貝西](../Page/阿爾瓦·貝西.md "wikilink")(Alvah Bessie)，編劇
  - [赫伯特·比伯曼](../Page/赫伯特·比伯曼.md "wikilink")(Herbert Biberman)，編劇和導演
  - [萊斯特·科爾](../Page/萊斯特·科爾.md "wikilink")(Lester Cole)，編劇
  - [愛德華·德米特裡克](../Page/愛德華·德米特裡克.md "wikilink")(Edward Dmytryk)，導演
  - [小靈·拉德納](../Page/小靈·拉德納.md "wikilink")(Ring Lardner, Jr.)，編劇
  - [約翰·霍華德·勞森](../Page/約翰·霍華德·勞森.md "wikilink")(John Howard Lawson)，編劇
  - [阿爾伯特·馬爾茲](../Page/阿爾伯特·馬爾茲.md "wikilink")(Albert Maltz)，編劇
  - [沙米爾·奧尼茨](../Page/沙米爾·奧尼茨.md "wikilink")(Samuel Ornitz)，編劇
  - [艾德裡安·斯科特](../Page/艾德裡安·斯科特.md "wikilink")(Adrian Scott)，製作人和編劇
  - [道爾頓·莊柏](../Page/道爾頓·莊柏.md "wikilink")(Dalton Trumbo)，編劇

#### 其他

  - [漢斯·艾斯勒](../Page/漢斯·艾斯勒.md "wikilink")(Hanns
    Eisler)，作曲家(Herman1997:356;Dick1989:7)
  - [伯納德·戈登](../Page/伯納德·戈登.md "wikilink")(Bernard
    Gordon)，編劇(Gordon1999:16)
  - [瓊·斯科特](../Page/瓊·斯科特.md "wikilink")(Joan
    Scott)，編劇(Ceplair和Englund2003:403;Goldstein1999)

### 1948年1月至1950年6月期間首次被列入黑名單的人士

(在詞條後有\*星號的,代表該人也被列入“紅色頻道”)

  - [Ben
    Barzman](../Page/Ben_Barzman.md "wikilink"),編劇(Ceplair和Englund2003:401)
  - [麗蓮·海爾曼](../Page/麗蓮·海爾曼.md "wikilink"),劇作曲家和編劇\*(Newman1989:140)
  - [William
    Sweets](../Page/William_Sweets.md "wikilink"),電台名人\*(Cogley1956:25–28)

### “紅色頻道”黑名單

(see,e.g.,Schrecker\[2002\],p. 244;Barnouw\[1990\],pp. 122–124)

  - [Larry Adler](../Page/Larry_Adler.md "wikilink"),男演員和音樂家
  - [Luther Adler](../Page/Luther_Adler.md "wikilink"),男演員和導演
  - [Stella Adler](../Page/Stella_Adler.md "wikilink"),女演員和教師
  - [Edith Atwater](../Page/Edith_Atwater.md "wikilink"),女演員
  - [Howard Bay](../Page/Howard_Bay.md "wikilink"),舞臺設計師
  - [Ralph Bell](../Page/Ralph_Bell.md "wikilink"),男演員
  - [倫納德·伯恩斯坦](../Page/倫納德·伯恩斯坦.md "wikilink"),作曲家和指揮家
  - [Walter Bernstein](../Page/Walter_Bernstein.md "wikilink"),編劇
  - [Michael Blankfort](../Page/Michael_Blankfort.md "wikilink"),編劇
  - [Marc Blitzstein](../Page/Marc_Blitzstein.md "wikilink"),作曲家
  - [True Boardman](../Page/True_Boardman.md "wikilink"),編劇
  - [Millen Brand](../Page/Millen_Brand.md "wikilink"),作家
  - [Oscar Brand](../Page/Oscar_Brand.md "wikilink"),民歌演唱家
  - [Joseph Edward
    Bromberg](../Page/Joseph_Edward_Bromberg.md "wikilink"),男演員
  - [Himan Brown](../Page/Himan_Brown.md "wikilink"),製作人和導演
  - [John Brown](../Page/John_Brown.md "wikilink"),男演員
  - [Abe Burrows](../Page/Abe_Burrows.md "wikilink"),劇作曲家和作詞家
  - [Morris Carnovsky](../Page/Morris_Carnovsky.md "wikilink"),男演員
  - [Vera Caspary](../Page/Vera_Caspary.md "wikilink"),作家
  - [Edward Chodorov](../Page/Edward_Chodorov.md "wikilink"),編劇和製作人
  - [Jerome Chodorov](../Page/Jerome_Chodorov.md "wikilink"),作家
  - [Mady Christians](../Page/Mady_Christians.md "wikilink"),女演員
  - [LeeJ.Cobb](../Page/LeeJ.Cobb.md "wikilink"),男演員
  - [Marc Connelly](../Page/Marc_Connelly.md "wikilink"),劇作曲家
  - [Aaron Copland](../Page/Aaron_Copland.md "wikilink"),作曲家
  - [Norman Corwin](../Page/Norman_Corwin.md "wikilink"),作家
  - [Howard DaSilva](../Page/Howard_DaSilva.md "wikilink"),男演員
  - [Roger DeKoven](../Page/Roger_DeKoven.md "wikilink"),男演員
  - [Dean Dixon](../Page/Dean_Dixon.md "wikilink"),指揮家
  - [Olin Downes](../Page/Olin_Downes.md "wikilink"),音樂評論家
  - [Alfred Drake](../Page/Alfred_Drake.md "wikilink"),男演員
  - [Paul Draper](../Page/Paul_Draper.md "wikilink"),男演員和舞蹈家
  - [Howard Duff](../Page/Howard_Duff.md "wikilink"),男演員
  - [CliffordJ.Durr](../Page/Clifford_Durr.md "wikilink"),律師
  - [Richard
    Dyer-Bennett](../Page/Richard_Dyer-Bennett.md "wikilink"),民歌演唱家
  - [José Ferrer](../Page/José_Ferrer.md "wikilink"),男演員
  - [Louise
    Fitch(Lewis)](../Page/Louise_Fitch\(Lewis\).md "wikilink"),女演員
  - [Martin Gabel](../Page/Martin_Gabel.md "wikilink"),男演員
  - [Arthur Gaeth](../Page/Arthur_Gaeth.md "wikilink"),電台時事評論者
  - [WilliamS.Gailmor](../Page/WilliamS.Gailmor.md "wikilink"),記者和電台時事評論者
  - [John Garfield](../Page/John_Garfield.md "wikilink"),男演員
  - [Will Geer](../Page/Will_Geer.md "wikilink"),男演員
  - [Jack Gilford](../Page/Jack_Gilford.md "wikilink"),男演員
  - [Tom Glazer](../Page/Tom_Glazer.md "wikilink"),民歌演唱家
  - [Ruth Gordon](../Page/Ruth_Gordon.md "wikilink"),女演員和編劇
  - [Lloyd Gough](../Page/Lloyd_Gough.md "wikilink"),男演員
  - [Morton Gould](../Page/Morton_Gould.md "wikilink"),鋼琴家和作曲家
  - [Shirley Graham](../Page/Shirley_Graham.md "wikilink"),作家
  - [Ben Grauer](../Page/Ben_Grauer.md "wikilink"),電台和電視名人
  - [Mitchell Grayson](../Page/Mitchell_Grayson.md "wikilink"),電台製作人和導演
  - [Horace Grenell](../Page/Horace_Grenell.md "wikilink"),指揮家和音樂製作人

<!-- end list -->

  - [Uta Hagen](../Page/Uta_Hagen.md "wikilink"),女演員和教師
  - [達許·漢密特](../Page/達許·漢密特.md "wikilink"),作家
  - [E.Y."Yip" Harburg](../Page/E.Y."Yip"_Harburg.md "wikilink"),作曲家
  - [RobertP.Heller](../Page/RobertP.Heller.md "wikilink"),電視記者
  - [麗蓮·海爾曼](../Page/麗蓮·海爾曼.md "wikilink"),劇作曲家和編劇
  - [Nat Hiken](../Page/Nat_Hiken.md "wikilink"),作家和製作人
  - [RoseHobart](../Page/RoseHobart.md "wikilink"),女演員
  - [Judy Holliday](../Page/Judy_Holliday.md "wikilink"),女演員
  - [RoderickB.Holmgren](../Page/RoderickB.Holmgren.md "wikilink"),記者
  - [LenaHorne](../Page/LenaHorne.md "wikilink"),歌手和女演員
  - [朗斯頓·休斯](../Page/朗斯頓·休斯.md "wikilink"),作家
  - [Marsha Hunt](../Page/Marsha_Hunt\(美國女演員\).md "wikilink"),女演員
  - [Leo Hurwitz](../Page/Leo_Hurwitz.md "wikilink"),導演
  - [Charles Irving](../Page/Charles_Irving.md "wikilink"),男演員
  - [Burl Ives](../Page/Burl_Ives.md "wikilink"),民歌演唱家和男演員
  - [SamJaffe](../Page/Sam_Jaffe\(男演員\).md "wikilink"),男演員
  - [Leon Janney](../Page/Leon_Janney.md "wikilink"),男演員
  - [Joseph Julian](../Page/Joseph_Julian.md "wikilink"),男演員
  - [Garson Kanin](../Page/Garson_Kanin.md "wikilink"),作家和導演
  - [George Keane](../Page/George_Keane.md "wikilink"),男演員
  - [Donna Keath](../Page/Donna_Keath.md "wikilink")
  - [Pert Kelton](../Page/Pert_Kelton.md "wikilink"),女演員
  - [Alexander Kendrick](../Page/Alexander_Kendrick.md "wikilink")
  - [Adelaide Klein](../Page/Adelaide_Klein.md "wikilink"),女演員
  - [Felix Knight](../Page/Felix_Knight.md "wikilink"),歌手和男演員
  - [Howard Koch編劇](../Page/Howard_Koch\(編劇\).md "wikilink")
  - [Tony Kraber](../Page/Tony_Kraber.md "wikilink"),男演員
  - [Millard Lampell](../Page/Millard_Lampell.md "wikilink"),編劇
  - [John La Touche](../Page/John_La_Touche.md "wikilink"),作詞家
  - [Arthur Laurents](../Page/Arthur_Laurents.md "wikilink")，作家
  - [Gypsy RoseLee](../Page/Gypsy_RoseLee.md "wikilink"),女演員和脫衣舞女
  - [Madeline Lee](../Page/Madeline_Lee.md "wikilink"),女演員
  - [Ray Lev](../Page/Ray_Lev.md "wikilink"),古典鋼琴家
  - [Philip Loeb](../Page/Philip_Loeb.md "wikilink"),男演員
  - [Ella Logan](../Page/Ella_Logan.md "wikilink"),女演員和歌手
  - [Alan Lomax](../Page/Alan_Lomax.md "wikilink"),民俗學研究者和音樂理論家
  - [Avon Long](../Page/Avon_Long.md "wikilink"),男演員和歌手
  - [Joseph Losey](../Page/Joseph_Losey.md "wikilink"),導演
  - [Peter Lyon](../Page/Peter_Lyon.md "wikilink"),電視作家
  - [Aline Mac Mahon](../Page/Aline_Mac_Mahon.md "wikilink"),女演員
  - [Paul Mann](../Page/Paul_Mann.md "wikilink"),導演和教師
  - [Margo](../Page/Margo.md "wikilink"),女演員和舞蹈家
  - [Myron Mc Cormick](../Page/Myron_Mc_Cormick.md "wikilink"),男演員
  - [Paul Mc Grath](../Page/Paul_Mc_Grath.md "wikilink"),電台男演員
  - [Burgess Meredith](../Page/Burgess_Meredith.md "wikilink"),男演員
  - [阿瑟·米勒](../Page/阿瑟·米勒.md "wikilink"),劇作家
  - [Henry
    Morgan](../Page/Henry_Morgan\(comedian\).md "wikilink"),電台和電視喜劇演員
  - [Zero Mostel](../Page/Zero_Mostel.md "wikilink"),男演員
  - [Jean Muir](../Page/Jean_Muir.md "wikilink"),女演員
  - [Meg Mundy](../Page/Meg_Mundy.md "wikilink"),女演員

<!-- end list -->

  - [Lynn Murray](../Page/Lynn_Murray.md "wikilink")
  - [Ben Myers](../Page/Ben_Myers.md "wikilink")
  - [Dorothy Parker](../Page/Dorothy_Parker.md "wikilink"),作家
  - [Arnold Perl](../Page/Arnold_Perl.md "wikilink"),電台作家
  - [Minerva Pious](../Page/Minerva_Pious.md "wikilink"),女演員
  - [Samson Raphaelson](../Page/Samson_Raphaelson.md "wikilink"),編劇和劇作曲家
  - [BernardReis](../Page/BernardReis.md "wikilink")
  - [Anne Revere](../Page/Anne_Revere.md "wikilink"),女演員
  - [Kenneth Roberts](../Page/Kenneth_Roberts.md "wikilink"),作家
  - [Earl Robinson](../Page/Earl_Robinson.md "wikilink"),作曲家和作詞家
  - [EdwardG.Robinson](../Page/EdwardG.Robinson.md "wikilink"),男演員
  - [WilliamN.Robson](../Page/WilliamN.Robson.md "wikilink"),電台和電視作家
  - [Harold Rome](../Page/Harold_Rome.md "wikilink"),作曲家和作詞家
  - [Norman Rosten](../Page/Norman_Rosten.md "wikilink"),作家
  - [Selena Royle](../Page/Selena_Royle.md "wikilink"),女演員
  - [Coby Ruskin](../Page/Coby_Ruskin.md "wikilink"),TV導演
  - [RobertSt.John](../Page/RobertSt.John.md "wikilink"),記者
  - [Hazel Scott](../Page/Hazel_Scott.md "wikilink"),爵士和古典音樂家
  - [Pete Seeger](../Page/Pete_Seeger.md "wikilink"),民歌演唱家
  - [Lisa Sergio](../Page/Lisa_Sergio.md "wikilink"),電台名人
  - [Artie Shaw](../Page/Artie_Shaw.md "wikilink"),爵士音樂家
  - [Irwin Shaw](../Page/Irwin_Shaw.md "wikilink"),作家
  - [Robert Louis
    Shayon](../Page/Robert_Louis_Shayon.md "wikilink"),電台與電視導演協會的前會長
  - [Ann Shepherd](../Page/Ann_Shepherd.md "wikilink"),女演員
  - [WilliamL.Shirer](../Page/WilliamL.Shirer.md "wikilink"),
  - [Allan Sloane](../Page/Allan_Sloane.md "wikilink"),電台和電視作家
  - [HowardK.Smith](../Page/HowardK.Smith.md "wikilink"),記者
  - [Gale Sondergaard](../Page/Gale_Sondergaard.md "wikilink"),女演員
  - [Hester Sondergaard](../Page/Hester_Sondergaard.md "wikilink"),女演員
  - [Lionel Stander](../Page/Lionel_Stander.md "wikilink"),男演員
  - [Johannes Steel](../Page/Johannes_Steel.md "wikilink"),記者
  - [Paul Stewart](../Page/Paul_Stewart\(男演員\).md "wikilink"),男演員
  - [Elliott Sullivan](../Page/Elliott_Sullivan.md "wikilink"),男演員
  - [William Sweets](../Page/William_Sweets.md "wikilink"),電台名人
  - [Helen Tamiris](../Page/Helen_Tamiris.md "wikilink"),編舞者
  - [Betty Todd](../Page/Betty_Todd.md "wikilink"),導演
  - [Louis Untermeyer](../Page/Louis_Untermeyer.md "wikilink"),詩人
  - [Hilda Vaughn](../Page/Hilda_Vaughn.md "wikilink"),女演員
  - [J.Raymond Walsh](../Page/J.Raymond_Walsh.md "wikilink"),電台時事評論者
  - [Sam Wanamaker](../Page/Sam_Wanamaker.md "wikilink"),男演員
  - [Theodore Ward](../Page/Theodore_Ward.md "wikilink"),劇作曲家
  - [Fredi Washington](../Page/Fredi_Washington.md "wikilink"),女演員
  - [Margaret
    Webster](../Page/Margaret_Webster.md "wikilink"),女演員,導演和製作人
  - [奧森·威爾斯](../Page/奧森·威爾斯.md "wikilink"),男演員,作家和導演
  - [Josh White](../Page/Josh_White.md "wikilink"),blues音樂家
  - [Irene Wicker](../Page/Irene_Wicker.md "wikilink"),singer和女演員
  - [Betty Winkler(Keane)](../Page/Betty_Winkler.md "wikilink"),女演員
  - [Martin Wolfson](../Page/Martin_Wolfson.md "wikilink"),男演員
  - [Lesley Woods](../Page/Lesley_Woods.md "wikilink"),女演員
  - [Richard Yaffe](../Page/Richard_Yaffe.md "wikilink"),記者

Madeline Lee—她嫁給男演員Jack Gilford,也被列入“紅色頻道”—經常被人和另一同名女演員[Madaline
Lee混同](../Page/Madaline_Lee.md "wikilink").

### 1950年6月後首次被列入黑名單的人士

  - [Phoebe Brand](../Page/Phoebe_Brand.md "wikilink"),女演員(Schwartz1999)
  - [Charles
    Dagget](../Page/Charles_Dagget.md "wikilink"),動畫繪製者(Cohen2004:178)
  - [Phil
    Eastman](../Page/Phil_Eastman.md "wikilink"),動畫作家(Cohen2004:178)
  - [MichaelGordon](../Page/Michael_Gordon\(film導演\).md "wikilink"),導演(Dick1982:80)
  - [John
    Hubley](../Page/John_Hubley.md "wikilink"),動畫繪製者(Cohen2004:178)
  - [Lester
    Koenig](../Page/Lester_Koenig.md "wikilink"),製作人(Herman1997:356)
  - [Lewis
    Leverett](../Page/Lewis_Leverett.md "wikilink"),男演員(Schwartz1999)
  - [John
    McGrew](../Page/John_McGrew.md "wikilink"),動畫繪製者(Cohen2004:178)
  - [Bill
    Melendez](../Page/Bill_Melendez.md "wikilink"),動畫繪製者(Cohen2004:178)
  - [Paula Miller](../Page/Paula_Miller.md "wikilink"),女演員(Schwartz1999)
  - [Waldo
    Salt](../Page/Waldo_Salt.md "wikilink"),編劇(Buhle和Wagner2003:208)
  - [Bill Scott](../Page/Bill_Scott.md "wikilink"),聲音男演員(Cohen2004:178)
  - [Art Smith](../Page/Art_Smith.md "wikilink"),男演員(Schwartz1999)
  - [Lionel
    Stander](../Page/Lionel_Stander.md "wikilink"),男演員(Doherty2003:31)

### 其他被列入黑名單的娛樂界專家

這是部分被列入黑名單的其他娛樂人士:

  - [Allen Adler](../Page/Allen_Adler.md "wikilink"),編劇
  - [Edgar Barrier](../Page/Edgar_Barrier.md "wikilink"),男演員
  - [Orson Bean](../Page/Orson_Bean.md "wikilink"),男演員
  - [Barbara BelGeddes](../Page/Barbara_BelGeddes.md "wikilink"),女演員
  - [Herschel Bernardi](../Page/Herschel_Bernardi.md "wikilink"),男演員
  - [Joseph Bernard](../Page/Joseph_Bernard.md "wikilink"),男演員
  - [John Berry](../Page/John_Berry.md "wikilink"),男演員,編劇和導演
  - [Allen Boretz](../Page/Allen_Boretz.md "wikilink"),song作家
  - [Lloyd Bridges](../Page/Lloyd_Bridges.md "wikilink"),男演員
  - [Sidney Buchman](../Page/Sidney_Buchman.md "wikilink"),編劇
  - [Hugo Butler](../Page/Hugo_Butler.md "wikilink"),編劇
  - [Charles Chaplin](../Page/Charles_Chaplin.md "wikilink"),男演員,導演和製作人
  - [Jeff Corey](../Page/Jeff_Corey.md "wikilink"),男演員
  - [John Cromwell導演](../Page/John_Cromwell\(導演\).md "wikilink")
  - [Jules Dassin](../Page/Jules_Dassin.md "wikilink"),導演
  - [Cy Endfield](../Page/Cy_Endfield.md "wikilink"),編劇和導演
  - [Brian Eubanks](../Page/Brian_Eubanks.md "wikilink"),律師
  - [John Henry Faulk](../Page/John_Henry_Faulk.md "wikilink"),電台名人
  - [Jerry Fielding](../Page/Jerry_Fielding.md "wikilink"),作曲家
  - [Carl Foreman](../Page/Carl_Foreman.md "wikilink"),製作人和編劇
  - [Betty Garrett](../Page/Betty_Garrett.md "wikilink"),女演員
  - [Lee Grant](../Page/Lee_Grant.md "wikilink"),女演員
  - [Sterling Hayden](../Page/Sterling_Hayden.md "wikilink"),男演員
  - [Sidney Kingsley](../Page/Sidney_Kingsley.md "wikilink"),劇作曲家
  - [Paul Jarrico](../Page/Paul_Jarrico.md "wikilink"),製作人和編劇
  - [Gordon Kahn](../Page/Gordon_Kahn.md "wikilink"),編劇
  - [Victor Kilian](../Page/Victor_Kilian.md "wikilink"),男演員

<!-- end list -->

  - [Alexander Knox](../Page/Alexander_Knox.md "wikilink"),男演員
  - [Marc Lawrence](../Page/Marc_Lawrence.md "wikilink"),男演員
  - [Canada Lee](../Page/Canada_Lee.md "wikilink"),男演員
  - [Robert Lees](../Page/Robert_Lees.md "wikilink"),編劇
  - [LouiseLewis](../Page/LouiseLewis.md "wikilink"),女演員
  - [Philip Loeb](../Page/Philip_Loeb.md "wikilink"),男演員
  - [Arnold Manoff](../Page/Arnold_Manoff.md "wikilink"),編劇
  - [RobertA.McGowan](../Page/RobertA.McGowan.md "wikilink"),編劇和導演
  - [Karen Morley](../Page/Karen_Morley.md "wikilink"),女演員
  - [Clifford Odets](../Page/Clifford_Odets.md "wikilink"),作家
  - [Larry Parks](../Page/Larry_Parks.md "wikilink"),男演員
  - [Leo Penn](../Page/Leo_Penn.md "wikilink"),男演員
  - [Abraham Polonsky](../Page/Abraham_Polonsky.md "wikilink"),編劇和導演
  - [John Randolph](../Page/John_Randolph\(男演員\).md "wikilink"),男演員
  - [Maurice Rapf](../Page/Maurice_Rapf.md "wikilink"),編劇
  - [Rosaura Revueltas](../Page/Rosaura_Revueltas.md "wikilink"),女演員
  - [FredericI.Rinaldo](../Page/FredericI.Rinaldo.md "wikilink"),編劇
  - [Martin Ritt](../Page/Martin_Ritt.md "wikilink"),男演員和導演
  - [Paul Robeson](../Page/Paul_Robeson.md "wikilink"),男演員和歌手
  - [Edwin Rolfe](../Page/Edwin_Rolfe.md "wikilink"),poet
  - [Robert Rossen](../Page/Robert_Rossen.md "wikilink"),編劇
  - [Jean Rouverol](../Page/Jean_Rouverol.md "wikilink"),女演員和作家
  - [Joshua Shelley](../Page/Joshua_Shelley.md "wikilink"),男演員
  - [Frank Tarloff](../Page/Frank_Tarloff.md "wikilink"),編劇
  - [Dorothy Tree](../Page/Dorothy_Tree.md "wikilink"),女演員
  - [Michael Wilson](../Page/Michael_Wilson\(作家\).md "wikilink"),編劇
  - [RichardN.Wright](../Page/Richard_Wright\(author\).md "wikilink"),作家
  - [Nedrick Young](../Page/Nedrick_Young.md "wikilink"),男演員和編劇

## 註釋

## 來源

### 出版

  -
  - Barnouw, Erik (1990 \[1975\]). *Tube of Plenty: The Evolution of
    American Television*. New York and Oxford: Oxford University Press.
    ISBN 978-0-19-506483-4

  - Bosworth, Patricia (1997). *Anything Your Little Heart Desires: An
    American Family Story*. New York: Simon and Schuster. ISBN
    978-0-684-80809-3

  - Buhle, Paul, and David Wagner (2003). *Hide in Plain Sight: The
    Hollywood Blacklistees in Film and Television, 1950-2002*. New York:
    Palgrave Macmillan. ISBN 978-1-4039-6144-0

  - Ceplair, Larry, and Steven Englund (2003). *The Inquisition in
    Hollywood: Politics in the Film Community, 1930-1960*. Urbana and
    Chicago: University of Illinois Press. ISBN 978-0-252-07141-6

  - Cogley, John (1956). "Report on Blacklisting." Collected in
    *Blacklisting: An Original Anthology* (1971), Merle Miller and John
    Cogley. New York: Arno Press/New York Times. ISBN 978-0-405-03579-1

  - Cohen, Karl F. (2004 \[1997\]). *Forbidden Animation: Censored
    Cartoons and Blacklisted Animators in America*. Jefferson, N.C.:
    McFarland.

  - Dick, Bernard F. (1982). *Hellman in Hollywood*. East Brunswick,
    N.J., London, and Toronto: Associated University Presses. ISBN
    978-0-8386-3140-9

  - Dick, Bernard F. (1989). *Radical Innocence: A Critical Study of the
    Hollywood Ten*. Lexington: University Press of Kentucky. ISBN
    978-0-8131-1660-0

  - Doherty, Thomas (2003). *Cold War, Cool Medium: Television,
    McCarthyism, and American Culture*. New York: Columbia University
    Press. ISBN 978-0-231-12952-7

  -
  - Gevinson, Alan (ed.) (1997). *American Film Institute Catalog—Within
    Our Gates: Ethnicity in American Feature Films, 1911-1960*.
    Berkeley, Los Angeles, and London: University of California Press.
    ISBN 978-0-520-20964-0

  - Goldstein, Patrick (1999). "Many Refuse to Clap as Kazan Receives
    Oscar," *Los Angeles Times*, March 22 (available
    [online](http://writing.upenn.edu/~afilreis/50s/kazan-protest.html)).

  - Gordon, Bernard (1999). *Hollywood Exile, Or How I Learned to Love
    the Blacklist*. Austin: University of Texas Press. ISBN
    978-0-292-72827-1

  - Herman, Jan (1997 \[1995\]). *A Talent for Trouble: The Life of
    Hollywood's Most Acclaimed Director, William Wyler*. New York: Da
    Capo. ISBN 030690798X

  - Johnpoll, Bernard K. (1994). *A Documentary History of the Communist
    Party of the United States*, vol. 3. Westport, Conn.: Greenwood
    Press. ISBN 978-0-313-28506-6

  - Newman, Robert P. (1989). *The Cold War Romance of Lillian Hellman
    and John Melby*. Chapel Hill and London: University of North
    Carolina Press. ISBN 978-0-8078-1815-2

  - O'Neill, William L. (1990 \[1982\]). *A Better World: Stalinism and
    the American Intellectuals*. New Brunswick, N.J.: Transaction. ISBN
    978-0-88738-631-2

  - *Red Channels: The Report of Communist Influence in Radio and
    Television* (1950). New York: Counterattack.

  - Ross, Stephen J. (ed.) (2002). *Movies and American Society*.
    Malden, Mass., and Oxford: Blackwell. ISBN 978-0-631-21960-6

  - Schrecker, Ellen (2002). *The Age of McCarthyism: A Brief History
    with Documents*. New York: Palgrave. ISBN 978-0-312-29425-0

  - Schwartz, Jerry (1999). "Some Actors Outraged by Kazan Honor,"
    Associated Press, March 13 (available
    [online](http://writing.upenn.edu/~afilreis/50s/kazan-award-flap.html)).

  - "Seven-Year Justice," *Time*, July 6, 1962 (available
    [online](http://www.time.com/time/magazine/article/0,9171,940029,00.html)).

  - Smith, Jeff (1999). "'A Good Business Proposition': Dalton Trumbo,
    *Spartacus*, and the End of the Blacklist," in *Controlling
    Hollywood: Censorship/Regulation in the Studio Era*, ed. Matthew
    Bernstein. New Brunswick, N.J.: Rutgers University Press. ISBN
    978-0-8135-2707-9

  - Stone, Geoffrey R. (2004). *Perilous Times: Free Speech in Wartime
    from the Sedition Act of 1798 to the War on Terrorism*. New York: W.
    W. Norton. ISBN 978-0-393-05880-2

  - Weinraub, Bernard (2000). "Blacklisted Screenwriters Get Credits,"
    *New York Times*, August 5.

### 線上

  - Schwartz, Richard A. (1999). ["How the Film and Television
    Blacklists Worked"](http://comptalk.fiu.edu/blacklist.htm). Part of
    the Florida International University website.
  - [Bertolt Brecht's Appearance Before the
    HUAC](https://web.archive.org/web/20090719134730/http://www.usc.edu/libraries/archives/arc/libraries/feuchtwanger/exhibits/Brecht/HUAC.html)
    brief essay, including link to contemporary newspaper clipping; part
    of the USC–Feuchtwanger Memorial Library website
  - [Guide to the American Business Consultants, Inc. *Counterattack*:
    Research Files
    1930–1968](https://web.archive.org/web/20060905085310/http://dlib.nyu.edu:8083/tamwagead/servlet/SaxonServlet?source=%2Fcounter.xml&style=%2Fsaxon01t2002.xsl&part=body)
    narrative summary and inventory of document holdings in the Tamiment
    Library/Robert F. Wagner Labor Archives; part of the NYU–Elmer
    Holmes Bobst Library website

## 其他連結

  - ["議會委員與不友善的證人"](http://www.writing.upenn.edu/~afilreis/50s/congcomms.html)
    歷史學家Ellen Schrecker整理的HUAC正式內容的詳細審查
  - ["Seeing
    Red"](http://www.pbs.org/newshour/bb/entertainment/july-dec97/blacklist_10-24.html)
    摘錄於PBS公文"荷里活黑名單的遺產"的副本和*[NewsHour](../Page/NewsHour.md "wikilink")*特派員Elizabeth
    Farnsworth訪問2位被黑名單的藝人，作家以及製作人[Paul
    Jarrico和女演員](../Page/Paul_Jarrico.md "wikilink")[Marsha
    Hunt的內容](../Page/Marsha_Hunt.md "wikilink")。

[Category:好萊塢](../Category/好萊塢.md "wikilink")
[Category:麥卡錫主義受害者](../Category/麥卡錫主義受害者.md "wikilink")
[Category:电影史](../Category/电影史.md "wikilink")
[Category:政治迫害](../Category/政治迫害.md "wikilink")
[H](../Category/好萊塢歷史和文化.md "wikilink")
[好萊塢黑名單](../Category/好萊塢黑名單.md "wikilink")

1.  Johnpoll (1994), p. xv.
2.  Schwartz (1999).
3.  Dick (1989), p. 7; Bertolt Brecht's Appearance.
4.  Dick (1989), p. 7.
5.  至少有兩本以上的重要近代歷史書不正確地將12月3日定為《華德福聲明》的日期:Ross (2002), p. 217; Stone
    (2004)，p.
    365。在許多1947年的資料之中犯了毫無疑問的錯誤，例如《紐約時代週刊》文章"電影驅逐十個被指蔑視國會的人；主要公司也投票去拒絕對共產主義者的工作—'歇斯底里，自由投降'被被告方面的律師指控;在投票去拒絕僱用共產主義者後，電影界將驅逐十個被指蔑視國會的人"出現於11月26日的報紙首頁。
6.  Gevinson (1997), p. 234.
7.  Stone (2004), p. 365.
8.  Newman 1989: 140.
9.  *Red Channels* (1950), pp. 6, 214; Guide to the American Business
    Consultants.
10. Doherty (2003), p.31.
11. Dick (1989)，p. 94
12. O'Neill (1990)， p.239.
13. Cohen (2004), p. 176.
14. Bosworth (1997), passim.
15. Smith (1999), p. 206.
16. Faulk (1963)
17. Albert, (1997) p.197
18. Weinraub(2000).