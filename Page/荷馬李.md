[Homer_Lea_Photo-3-.jpg](https://zh.wikipedia.org/wiki/File:Homer_Lea_Photo-3-.jpg "fig:Homer_Lea_Photo-3-.jpg")
**荷馬李**（，又譯**李何默**\[1\]、**咸马里**，），[美國人](../Page/美國人.md "wikilink")，出生於[美國](../Page/美國.md "wikilink")[科羅拉多州](../Page/科羅拉多州.md "wikilink")[丹佛](../Page/丹佛.md "wikilink")。1912年擔任[中華民國首席軍事顧問](../Page/中華民國.md "wikilink")。[同盟會會員](../Page/同盟會.md "wikilink")。著有[地缘政治学研究等書籍](../Page/地缘政治学.md "wikilink")。

## 經歷

他因患有[脊椎側彎而嚴重駝背](../Page/脊椎側彎.md "wikilink")（另一說是出生時健康，但嬰孩時因掉落後撞到壁爐的石頭而導致駝背），身高只有160公分（約5呎3吋，重約100磅），[視力也不佳](../Page/視力.md "wikilink")。但立志成為[軍事家](../Page/軍事家.md "wikilink")，曾入[西點軍校](../Page/西點軍校.md "wikilink")，不久因健康理由遭到開除，後來進入[斯坦福大學讀書](../Page/斯坦福大學.md "wikilink")。他飽讀世界軍事名著，應[康有為邀請來中國訓練](../Page/康有為.md "wikilink")“保皇軍”，封為“大將軍”。

1908年，他構想出大膽的軍事冒險[紅龍計劃](../Page/中國紅龍計劃.md "wikilink")，準備革命征服中國南部兩廣地區。他與一些美國商人和[容閎密謀](../Page/容閎.md "wikilink")，想要通過容閎爭取中國南方各種派系和秘密會社，形成統一戰線組織一支軍隊，由他指揮革命。成功之後，由容閎領導革命力量的聯合政府，而他和他的共謀者希望在新政府獲得廣泛的經濟利益。紅龍計劃最初想由容閎透過他的學生[唐紹儀爭取](../Page/唐紹儀.md "wikilink")[袁世凱](../Page/袁世凱.md "wikilink")，但是唐紹儀拒絕。1910年他與[孫中山見面後](../Page/孫中山.md "wikilink")，雙方同意合作。\[2\]1912年1月1日，中華民國成立，孫中山成為臨時大總統，荷馬李被任命為首席軍事顧問。荷馬李不久中風，被迫回到美國，在1912年11月1日病逝，下葬時仍身穿中華民國的[將軍服](../Page/將軍.md "wikilink")。

他留下了希望葬在[中國土地上的遺願](../Page/中國.md "wikilink")。1969年4月，其骨灰自美國[洛杉磯遷到](../Page/洛杉磯.md "wikilink")[臺北](../Page/臺北.md "wikilink")[陽明山第一公墓長眠](../Page/陽明山.md "wikilink")、[墓碑書有](../Page/墓碑.md "wikilink")「國父軍事顧問荷馬李將軍夫婦之墓」。\[3\]

## 著作

荷馬李心中所設想的戰略三書（Strategic
Trilogy），其中《[無知之勇](../Page/無知之勇.md "wikilink")》（The
Valor of Ignorance）與《[撤克遜時代](../Page/撤克遜時代.md "wikilink")》（The Day of
the
Saxon）分別於1909年、1912年出版，而撰寫中的第三本：《[斯拉夫的蜂擁](../Page/斯拉夫的蜂擁.md "wikilink")》（The
Swarming of the
Slav）則尚未完成即過世。另著有一本[小說](../Page/小說.md "wikilink")《[紅鉛筆](../Page/紅鉛筆.md "wikilink")》（Vermilion
Pencil），於1908年出版。

他在生前所著《無知之勇》（The Valor of
Ignorance）一書中預測[日本將發動](../Page/日本.md "wikilink")[太平洋戰爭](../Page/太平洋戰爭.md "wikilink")。書中提醒美國須注意國家最大的危險是日本，並判斷日本會從海上攻擊美國。\[4\]

## 註釋

## 外部連結

  - [The Homer Lea Research
    Center](https://web.archive.org/web/20110717014915/http://www.homerlea.org/)
    A research site developed by Dr. Lawrence M. Kaplan, an authority on
    the life of Homer Lea and author of *Homer Lea: American Soldier of
    Fortune.*
  - [Who is Homer Lea?](http://www.homerleasite.com/Site/Welcome.html) A
    website entirely dedicated to our unsung hero, Homer Lea.
  - [Video of Homer Lea's remains arriving at TaipeiSongshan
    Airport.](http://vcenter.iis.sinica.edu.tw/watch.php?val=aWQ9TUV0Z09UTT0=)
  - [The valor of ignorance, Internet
    Archive](http://www.archive.org/details/valorofignorance00leahuoft)

[Category:軍事學家](../Category/軍事學家.md "wikilink")
[Category:在中國的美國人](../Category/在中國的美國人.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:西點軍校校友](../Category/西點軍校校友.md "wikilink")
[Category:科羅拉多州人](../Category/科羅拉多州人.md "wikilink")

1.  《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》p.1644

2.

3.

4.