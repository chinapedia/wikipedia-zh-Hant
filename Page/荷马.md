[William-Adolphe_Bouguereau_(1825-1905)_-_Homer_and_his_Guide_(1874).jpg](https://zh.wikipedia.org/wiki/File:William-Adolphe_Bouguereau_\(1825-1905\)_-_Homer_and_his_Guide_\(1874\).jpg "fig:William-Adolphe_Bouguereau_(1825-1905)_-_Homer_and_his_Guide_(1874).jpg")绘\]\]
**荷马**（<span lang=el>Ὅμηρος</span>，约）\[1\]，相传為[古希腊的](../Page/古希腊.md "wikilink")[遊吟诗人](../Page/遊吟诗人.md "wikilink")，生于[小亚细亚](../Page/小亚细亚.md "wikilink")，[失明](../Page/失明.md "wikilink")，創作了[史诗](../Page/史诗.md "wikilink")《[伊利亚特](../Page/伊利亚特.md "wikilink")》和《[奥德赛](../Page/奥德赛.md "wikilink")》，两者统称《[荷马史诗](../Page/荷马史诗.md "wikilink")》。目前沒有确切证据证明荷马的存在，所以也有人认为他是传说中被构造出来的人物。而关于《荷马史诗》，大多数学者认为是当时经过几个世纪口头流传的诗作的结晶。

## 生平及传说

关于荷马的资料很少，所以对其生平有很多说法，但都无确凿证据。荷马的出生日期與出生地不详，其史詩时间亦不详。关于他的生活年代，[古希腊历史学家](../Page/古希腊.md "wikilink")[希罗多德认为荷马生活在自己的时代之前至多不超过四百年](../Page/希罗多德.md "wikilink")，即大约公元前850年左右。\[2\]除了这个广为接受的说法，也有人（比如[修昔底德](../Page/修昔底德.md "wikilink")）认为荷马的年代离[特洛伊战争的年代不远](../Page/特洛伊战争.md "wikilink")，大约是公元前12世纪早期。\[3\]\[4\]\[5\]荷马年代大约是公元前十世纪。\[6\]

关于荷马的出生地，也有很多说法。曾经就至少有七个地方或城市竞相争夺荷马的“所有权”。\[7\]最为接受的说法是他来自[伊奥尼亚](../Page/伊奥尼亚.md "wikilink")，因为他的诗中有很多伊奥尼亚的方言。\[8\]

## 作品

由这两部史诗组成的荷马史诗，语言简练，情节生动，形象鲜明，结构严谨，是西方第一部重要文學作品，荷马也被稱为欧洲四大史詩诗人之一或之首（另外三人為[維吉爾](../Page/維吉爾.md "wikilink")、[但丁](../Page/但丁.md "wikilink")、[米爾頓](../Page/约翰·弥尔顿.md "wikilink")），[維克多·雨果在](../Page/維克多·雨果.md "wikilink")《莎士比亞》（*William
Shakespeare*）一文中寫道：「世界诞生，荷馬高歌。他是迎來這曙光的鳥。(Le monde naît, Homère chante.
C'est l'oiseau de cette
aurore).」荷马史诗不但文学价值极高，也是古希腊公元前十一世纪到公元前九世纪的唯一文字史料，反映了[迈锡尼文明](../Page/迈锡尼文明.md "wikilink")，所以这一时期也被称为“荷马时代”或“英雄时代”。

被发现於18世纪的荷马史诗《伊利亚特》長15,693行，叙述希腊联军围攻小亚细亚的城市[特洛伊的故事](../Page/特洛伊.md "wikilink")，以希腊联军统帅[阿伽门农和猛将](../Page/阿伽门农.md "wikilink")[阿基里斯的争吵为中心](../Page/阿基里斯.md "wikilink")，集中描写了战争结束前五十天发生的事情。《奥德赛》共12,105行，叙述[伊塔卡王](../Page/綺色佳.md "wikilink")[奥德修斯在攻陷特洛伊后归国途中十年漂泊的故事](../Page/奥德修斯.md "wikilink")，集中描写的是这十年中最后一年零几十天的事情。

## 荷馬問題

古希腊人一直将荷马史诗视作希腊文化的精华将荷马视作民族的骄傲，但丁更称荷马为“诗人之王”。\[9\]但自17世纪末以来，渐渐开始有學者对于荷马是否确有其人，以及他的籍贯、生活年代、史诗是否他一人所作等一系列问题都有不同看法，形成「荷馬問題」。

古代曾留下多篇荷马传记，但内容矛盾，难作参考。荷马的生存年代约为公元前十二世纪到公元前七世纪，关于他的籍贯问题，古代流传下来两行诗，都提到七座城邦，或说他的家乡就在这七座城邦之中。根据史诗的内容和语言特点，荷馬可能是[小亚细亚西部的](../Page/小亚细亚.md "wikilink")[伊奥尼亚人](../Page/伊奥尼亚.md "wikilink")，最后死在[爱琴海的伊奥斯岛](../Page/爱琴海.md "wikilink")。也有古代作家指荷馬從[巴比倫被擄到希臘](../Page/巴比倫.md "wikilink")，「荷馬」一字在古希臘文正作「人質」解。

另外，或說荷馬是個女人，或認為「荷馬」只是某詩人團體的名稱。至於荷馬的失明問題，一者古代的樂師常為瞽者，二者古希臘人相信眼睛失明，會使內心更能看到事物的真相（可比較[提瑞西阿斯的情況](../Page/提瑞西阿斯.md "wikilink")）。

对于史诗本身的争论，自十七世纪以来，就受到人们的广泛关注，人们就史诗的形成过程展开了激烈的争论。真正的争论是出现在十八世纪后期的，1788年发现的《伊利亚特》威尼斯抄本中的一些注释使荷马史诗成为许多人争论的热点，对民间诗歌创作的研究促进了这一争论的深入。

各家的观点基本可分为三类：以德国学者为代表的“短歌说”认为史诗形成与公元前十三世纪至公元前九世纪，各部份由不同的诗人创作，代代口头相传，后来经过加工，整理成文字，基本部份属于荷马\[10\]；以意大利学者为代表的“统一说”认为荷马利用前人的资料创作了统一的诗歌\[11\]；“核心说”实际是对上述两种观点的折衷，认为两部史诗形成之前荷马创作了两部篇幅不同的史诗，后经过他人加工整理，逐渐形成长篇，因此史诗既有统一布局，又包含矛盾。\[12\]\[13\]

美國學者的研究，是「荷馬問題」的研究史上一個轉捩點。實地研究過[南斯拉夫遊吟詩人的傳統後](../Page/南斯拉夫.md "wikilink")，帕里指出荷馬史詩正屬這類遊吟傳統，最大的證明就是荷馬史詩裏充斥的所謂「荷馬式套語」。凡是遊吟詩人，都備有大量那類套語，以便湊合格律，即興演出。荷馬史詩很多看來矛盾的地方，正是出於湊合格律的需要。\[14\]\[15\]\[16\]

## 相關條目

  - [亞該亞人](../Page/亞該亞人.md "wikilink")
  - [阿喀琉斯](../Page/阿喀琉斯.md "wikilink")
  - [埃涅阿斯纪](../Page/埃涅阿斯纪.md "wikilink")
  - [萨莫色雷斯的阿里斯塔库斯](../Page/萨莫色雷斯的阿里斯塔库斯.md "wikilink")
  - [史诗](../Page/史诗.md "wikilink")
  - [希腊神话](../Page/希腊神话.md "wikilink")
  - [赫克特](../Page/赫克特.md "wikilink")
  - [伊薩基島](../Page/伊薩基島.md "wikilink")
  - [奧德修斯](../Page/奧德修斯.md "wikilink")
  - [庇西特拉圖](../Page/庇西特拉圖.md "wikilink")
  - [特洛伊战争](../Page/特洛伊战争.md "wikilink")
  - [特洛伊](../Page/特洛伊.md "wikilink")
  - [澤諾多托斯](../Page/澤諾多托斯.md "wikilink")

</div>

## 参考文献

### 注释

### 参考书籍

  -
  -
  -
  -
## 延伸閱讀

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
## 外部連結

  - [Works by Homer](http://www.gutenberg.org/browse/authors/h#a705) at
    [Project Gutenberg](../Page/Project_Gutenberg.md "wikilink").

  -
  -
  - [The Chicago Homer](http://digital.library.northwestern.edu/homer/)

  -
  -
  -
[H](../Category/古希腊作家.md "wikilink")
[H](../Category/古希腊诗人.md "wikilink")
[H](../Category/人物神.md "wikilink")
[Category:邁錫尼文明](../Category/邁錫尼文明.md "wikilink")

1.

2.  陈中梅（1994），《伊利亚特》，[前言](http://www.tianyabook.com/hemashishi/ylyt/000.html)

3.

4.
5.

6.  《Masterpiece of Eloquence 》第一卷第一章第一页，共二十五卷。出版社“New York P.F.Collier
    \&Son MCMV”1905年。王小松。

7.
8.

9.
10. August Wolf: Prolegomena ad Homerum, 1795

11. Giambattista Vico, 1730

12. [王焕生](../Page/王焕生.md "wikilink") 译（2003），《荷马史诗·奥德赛》，前言

13. 高中甫
    译（2011），《古希腊神话与传说》，[前言](http://www.amazon.cn/%E5%8F%A4%E5%B8%8C%E8%85%8A%E7%A5%9E%E8%AF%9D%E4%B8%8E%E4%BC%A0%E8%AF%B4-%E6%96%BD%E7%93%A6%E5%B8%83/dp/product-description/B005JX2T5W)

14.

15.

16. 程志敏（2007），《荷马史诗导读》，p112-113