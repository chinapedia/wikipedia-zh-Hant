**莊偉倫**（），為一台灣足球員，在[中華台北足球代表隊和](../Page/中華台北足球代表隊.md "wikilink")[大同足球隊分別擔任前鋒和右中場的角色](../Page/大同足球隊.md "wikilink")。

2003年莊偉倫入選[中華台北奧運足球代表隊](../Page/中華台北奧運足球隊.md "wikilink")，並被任命為代表隊隊長。2005年考取[國訓入伍服役](../Page/台灣國訓足球隊.md "wikilink")，同年他被票選為[企業足球聯賽最佳右](../Page/企業足球聯賽.md "wikilink")[中場](../Page/中場.md "wikilink")。

2007年2月9日，繼守門員[呂昆錡之後](../Page/呂昆錡.md "wikilink")，日本[東北社會人聯盟勁旅](../Page/東北社會人聯盟.md "wikilink")[盛岡仙鶴的官方網站宣佈莊偉倫即將加入](../Page/盛岡仙鶴.md "wikilink")\[1\]，球隊並於3月7日[盛岡仙鶴在俱樂部辦公室為莊偉倫與呂昆錡舉辦正式加盟記者會](../Page/盛岡仙鶴.md "wikilink")\[2\]，但在3月13日球隊官方網站卻傳出莊偉倫因家庭因素而未與球隊簽約並返回台灣\[3\]。回到台灣之後，已從國訓退役的莊偉倫回到大同備戰第二循環的企業足球聯賽\[4\]。

## 參考資料

[W](../Category/莊姓.md "wikilink")
[Category:1982年出生](../Category/1982年出生.md "wikilink")
[Category:台灣足球運動員](../Category/台灣足球運動員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:大同球員](../Category/大同球員.md "wikilink")

1.
2.
3.
4.