**莊月明**（），冠夫姓为**李莊月明**，籍貫[廣東](../Page/廣東.md "wikilink")[潮州](../Page/潮州.md "wikilink")[潮安县](../Page/潮安县.md "wikilink")，香港首富[李嘉誠的](../Page/李嘉誠.md "wikilink")[表妹兼](../Page/表妹.md "wikilink")[髮妻](../Page/髮妻.md "wikilink")。其父[莊靜庵乃香港第一代鐘表商](../Page/莊靜庵.md "wikilink")，創辦[中南鐘表有限公司](../Page/中南鐘表有限公司.md "wikilink")。

## 生平

畢業於[英華女學校](../Page/英華女學校.md "wikilink")。1961年，獲[香港大學文學士學位主修英文](../Page/香港大學.md "wikilink")。及後負笈日本[明治大學](../Page/明治大學.md "wikilink")。1963年4月27日\[1\]，與[李嘉誠結婚](../Page/李嘉誠.md "wikilink")。1964年8月1日，誕下長子[李澤鉅](../Page/李澤鉅.md "wikilink")。1966年11月8日，誕下次子[李澤楷](../Page/李澤楷.md "wikilink")。1989年12月31日，與[李嘉誠出席](../Page/李嘉誠.md "wikilink")[君悅酒店的新春晚會](../Page/君悅酒店.md "wikilink")。1990年1月1日下午，她因[心臟病發](../Page/心臟病.md "wikilink")，在家中病逝，終年58歲。1月4日於[香港殯儀館出殯](../Page/香港殯儀館.md "wikilink")\[2\]，遺體下葬[歌連臣角佛教墳場](../Page/歌連臣角佛教墳場.md "wikilink")\[3\]。

## 身後

2006年1月30日，她在[柴灣歌連臣角佛教墳場的墓地被四個匪徒企圖盜取](../Page/柴灣.md "wikilink")[陪葬品](../Page/陪葬品.md "wikilink")，警方隨後就事件展開調查。

## 死因

莊氏於1990年1月1日猝死，享年58歲。

她的死因在坊間出現多個版本，不甚尋常，有報道引述當時[警方電台紀錄](../Page/香港警察.md "wikilink")，指莊月明因服用過量藥物致死，惟此說一直被李家否認，指她乃因[心疾致死](../Page/心臟病.md "wikilink")。

據報，1989年12月31日[除夕](../Page/除夕.md "wikilink")，李氏夫婦聯袂出席[君悅酒店的新年晚會](../Page/君悅酒店.md "wikilink")，不料翌日下午，家人指她[心臟病發身故](../Page/心臟病.md "wikilink")。

報道指出，當時莊月明在[旭龢道寓所服食過量藥物](../Page/旭龢道.md "wikilink")，被人發現報警，其弟將她送到[嘉諾撒醫院](../Page/嘉諾撒醫院.md "wikilink")，搶救後不治。

## 以莊命名的建築物和設施

  - [香港大學](../Page/香港大學.md "wikilink")[莊月明文娛中心](../Page/莊月明文娛中心.md "wikilink")
  - [香港大學](../Page/香港大學.md "wikilink")[莊月明化學樓](../Page/莊月明化學樓.md "wikilink")
  - [香港大學](../Page/香港大學.md "wikilink")[莊月明物理樓](../Page/莊月明物理樓.md "wikilink")
  - [明愛莊月明中學](../Page/明愛莊月明中學.md "wikilink")
  - [聖保羅男女中學李莊月明樓](../Page/聖保羅男女中學.md "wikilink")
  - [香海正覺蓮社主辦佛教李莊月明護養院](../Page/香海正覺蓮社主辦佛教李莊月明護養院.md "wikilink")

## 參見

  - [李嘉誠家族](../Page/李嘉誠家族.md "wikilink")

## 資料來源

[Category:香港人](../Category/香港人.md "wikilink")
[Category:潮安人](../Category/潮安人.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:明治大學校友](../Category/明治大學校友.md "wikilink")
[Category:莊姓](../Category/莊姓.md "wikilink")
[Category:李嘉誠家族](../Category/李嘉誠家族.md "wikilink")
[Category:英華女學校校友](../Category/英華女學校校友.md "wikilink")

1.

2.

3.