**莊碩漢**（**Chuang
Suo-hang**，），[臺灣](../Page/臺灣.md "wikilink")[學者及](../Page/學者.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，出身[國民黨](../Page/國民黨.md "wikilink")、後加入[民主進步黨](../Page/民主進步黨.md "wikilink")，現任[中華民國外貿協會](../Page/中華民國外貿協會.md "wikilink")[副董事長](../Page/副董事長.md "wikilink")。曾任[立法委員](../Page/立法委員.md "wikilink")、[僑務委員會副委員長](../Page/僑務委員會.md "wikilink")、[政治學](../Page/政治學.md "wikilink")[教授等](../Page/教授.md "wikilink")。

## 生平

早年先後在[淡江大學公行系](../Page/淡江大學.md "wikilink")、[世新大學行管系任教](../Page/世新大學.md "wikilink")，曾任[國民黨青工會副主委](../Page/國民黨.md "wikilink")、[銓敘部政務次長](../Page/銓敘部.md "wikilink")，後於民進黨執政後由藍轉綠，陸續擔任[臺灣省副主席](../Page/臺灣省.md "wikilink")、[僑委會副委員長與行政院發言人等職務](../Page/僑委會.md "wikilink")，並於[2004年中華民國立法委員選舉中](../Page/2004年中華民國立法委員選舉.md "wikilink")，在台北縣第一選區以第一高票當選立委；不過，在[2008年中華民國立法委員選舉於台北縣板橋東區尋求連任](../Page/2008年中華民國立法委員選舉.md "wikilink")，不敵[國親共推的](../Page/國親聯盟.md "wikilink")[吳清池](../Page/吳清池.md "wikilink")，未能續留國會。[2012年中華民國立法委員選舉於新北市土城三峽選區捲土重來](../Page/2012年中華民國立法委員選舉.md "wikilink")，最後依然以些微差距不敵尋求連任的國民黨立委[盧嘉辰](../Page/盧嘉辰.md "wikilink")。原有意於[2016年中華民國立法委員選舉重披戰袍而投入](../Page/2016年中華民國立法委員選舉.md "wikilink")[新北市第六選舉區民進黨黨內初選](../Page/新北市第六選舉區.md "wikilink")，最終不敵時任新北市議員、曾任臺北縣板橋市代理市長的[張宏陸而未出線](../Page/張宏陸.md "wikilink")。2017年1月20日，外貿協會董監事會決議由曾任立委並具僑務經驗的莊碩漢接任副董事長。

## 選舉

### 2004年立法委員選舉

| 2004年臺北縣第一選舉區立法委員選舉結果 |
| --------------------- |
| 應選8席                  |
| 號次                    |
| 1                     |
| 2                     |
| 3                     |
| 4                     |
| 5                     |
| 6                     |
| 7                     |
| 8                     |
| 9                     |
| 10                    |
| 11                    |
| 12                    |
| 13                    |
| 14                    |

### 2008年立法委員選舉

| 2008年[臺北縣第七選舉區](../Page/新北市第七選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| 3                                                                                     |
| 4                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

### 2012年立法委員選舉

| 2012年[新北市第十選舉區](../Page/新北市第十選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| 3                                                                                     |
| 4                                                                                     |
| 5                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

## 外部連結

[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:中華民國行政院發言人](../Category/中華民國行政院發言人.md "wikilink")
[Category:臺灣省副主席](../Category/臺灣省副主席.md "wikilink")
[Category:臺灣省政府委員](../Category/臺灣省政府委員.md "wikilink")
[Category:中華民國銓敘部次長](../Category/中華民國銓敘部次長.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:淡江大學教授](../Category/淡江大學教授.md "wikilink")
[Category:世新大學教授](../Category/世新大學教授.md "wikilink")
[Category:南加州大學校友](../Category/南加州大學校友.md "wikilink")
[Category:國立臺灣大學社會科學院校友](../Category/國立臺灣大學社會科學院校友.md "wikilink")
[Category:臺北市立建國高級中學校友](../Category/臺北市立建國高級中學校友.md "wikilink")
[Category:新北市人](../Category/新北市人.md "wikilink")
[Shuo碩](../Category/莊姓.md "wikilink")