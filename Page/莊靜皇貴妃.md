**莊静皇贵妃**（），[他他拉氏](../Page/他他拉氏.md "wikilink")，[滿洲八旗出身](../Page/滿洲八旗.md "wikilink")。主事庆海之女。[咸豐帝寵妃](../Page/咸豐帝.md "wikilink")，[榮安固倫公主生母](../Page/榮安固倫公主.md "wikilink")。

## 早期生平及入宮伊初

[道光十七年二月二十七日出生](../Page/道光.md "wikilink")。「麗」字的滿文意思為「俏麗」或「艷麗」，推測他他拉氏的容貌艶麗。咸丰元年二月，他他拉氏在咸豐朝第一次的外[八旗選秀中](../Page/八旗選秀.md "wikilink")，与[葉赫那拉氏](../Page/慈禧太后.md "wikilink")、[鈕祜祿氏和](../Page/孝貞顯皇后.md "wikilink")[伊爾根覺羅氏同时被选為內廷主位](../Page/玶常在.md "wikilink")。

他他拉氏初居[鐘粹宮](../Page/鐘粹宮.md "wikilink")，《內務府呈稿》曾載：「咸豐二年，[貞貴妃](../Page/慈安太后.md "wikilink")、麗貴人應用什物由鐘粹宮住圓明園挪運。」由此可見，他他拉氏未入宮時，其嫁妝已先被送往擬定的寢宮，即鐘粹宮某配殿。此外，同宮之[貞貴妃](../Page/慈安太后.md "wikilink")[鈕祜祿氏已被擬為](../Page/鈕祜祿.md "wikilink")[皇后](../Page/皇后.md "wikilink")，未知將[鐘粹宮擬定為麗](../Page/鐘粹宮.md "wikilink")[貴人寢宮之緣故](../Page/貴人.md "wikilink")。

咸豐二年五月初九日，他他拉氏和[葉赫那拉氏同时由本家被送入](../Page/慈禧太后.md "wikilink")[圆明园](../Page/圆明园.md "wikilink")，同日被封为**丽贵人**\[1\]，[葉赫那拉氏則被封为兰贵人](../Page/慈禧太后.md "wikilink")。

咸丰三年七月初六日报单内开，传旨著沈振麟画皇上御容和主位喜容稿九张呈览，麗貴人等全體後妃每人一幅畫像。[咸豐四年十二月二十四日](../Page/咸豐.md "wikilink")，敬事房太監張信傳旨：「麗貴人封為麗嬪，蘭貴人封為懿嬪，婉貴人封為婉嬪，玫常在封為玫貴人」，他他拉氏是因懷孕數月而詔晉为**丽嫔**。

## 宮廷生活

[咸豐五年三月二十四日](../Page/咸豐.md "wikilink")，已經懷孕約八個月的麗嬪在寢宮[永和宮聽曲](../Page/永和宮.md "wikilink")，如《新鷓鴣》、《喜春光》等。同年五月初七日，丽嫔生下咸豐帝的皇長女[荣安固伦公主](../Page/荣安固伦公主.md "wikilink")，成为第一个为咸豐帝生儿育女的嫔妃。麗嬪生女兒時，先是[僧格林沁大敗](../Page/僧格林沁.md "wikilink")[太平軍](../Page/太平天國.md "wikilink")，擒[林鳳祥](../Page/林鳳祥.md "wikilink")、[李開芳](../Page/李開芳.md "wikilink")，凱旋班師。四天之後又是[康慈皇貴太妃的壽辰](../Page/孝靜成皇后.md "wikilink")。咸豐帝因欠安而取消了康慈皇貴太妃壽辰的正式行禮和看戲，但在洗三那天，他祭完地壇就陪康慈皇貴太妃去探視麗嬪母女，並晉封麗嬪為**丽妃**，比懿嬪封妃早十个月。注意的是他他拉氏因懷孕而沒有举行嬪位册封礼，直接舉行妃位册封禮。

雖然麗妃未能生下[皇子](../Page/皇子.md "wikilink")，但除了皇后之外，在咸豐帝的[後宮中也只有麗妃比懿嬪得寵](../Page/後宮.md "wikilink")。懿嬪所生的大阿哥載淳與麗妃所生的大公主，所獲的賞賜基本上是平分秋色，足見咸豐帝心目中麗妃的地位之高。

咸丰六年（1856年），咸豐帝在圆明园长春仙馆后面的绿荫轩殿外檐额题上“喜音堂”三字。在咸丰后期图档显示，喜音堂西梢间安设落地罩、西进间后檐添搭响塘炕一铺，並且注有丽字。這表明这曾是咸丰帝丽妃因九洲清晏修缮而临时居住的寝居。同年九月二十日，咸丰帝命如意馆绘制大公主喜容一张，随后又于二十五日命绘制公主生母丽妃娘娘喜容一张。咸丰七年十一月初五日，命交“丽妃、大公主行乐软挂”一件，并且传旨配做一個楠木插盖匣。

宮中有咸豐帝黃箋朱筆寫的“咸豐九年，麗妃移住[咸福宮大吉](../Page/咸福宮.md "wikilink")”的字條。就[御膳房檔案所知](../Page/御茶膳房.md "wikilink")，帝起居之所多在咸福宮後楹同道堂。近水樓台先得月，可想而知咸豐帝當時寵愛麗妃甚於懿嬪。這麼看來，同道堂內簷的“襄贊壼儀”匾額，實際上是咸豐帝寫給麗妃的。麗妃搬到咸豐帝的寢宮，正符合慈禧太后所寫的下聯“端居宵旰早關懷”。

## 後期生平

每逢宮中大宴，東邊頭桌宴是皇后，二桌宴是麗妃和[祺嬪](../Page/端恪皇貴妃.md "wikilink")，而西邊頭桌宴是懿貴妃和[婉嬪](../Page/婉貴妃_\(咸豐帝\).md "wikilink")。麗妃顯然是處處與懿貴妃分庭抗禮的。咸豐十一年除夕，即咸豐帝死後的第一個[除夕](../Page/除夕.md "wikilink")，宮中敬事房日記簿載：“十二月十四日小太監金環具奏，年例乾果盤，隨奉二位[皇太后旨](../Page/皇太后.md "wikilink")，著將麗皇貴妃撤下不給。”一份每年照例的干果盤，竟特傳旨撤除，原因不明。

[同治帝在举行完登极大典的第二天颁发谕旨](../Page/同治帝.md "wikilink")，以丽妃“侍奉皇考有年，诞育大公主”为由，尊丽妃为**皇考丽皇贵妃**。

之後[光绪帝以丽皇貴妃](../Page/光绪帝.md "wikilink")“侍奉文宗显皇帝均称淑慎”为由，尊封为**皇考丽皇贵太妃**，在宮中的地位僅次於[慈安太后與](../Page/慈安太后.md "wikilink")[慈禧太后](../Page/慈禧太后.md "wikilink")。

[光緒十五年正月](../Page/光緒.md "wikilink")，壽西宮麗皇貴太妃下女子二名，撥給[永和宮伺候](../Page/永和宮.md "wikilink")[瑾嬪](../Page/瑾妃.md "wikilink")，位下的另外二名[官女子](../Page/官女子.md "wikilink")，撥給[景仁宮伺候珍嬪](../Page/景仁宮.md "wikilink")。

[光绪十六年十一月十五日](../Page/光绪.md "wikilink")，麗皇貴太妃病逝，终年五十四岁，谥号“莊静皇贵妃”，金棺暂安于田村殡宫。[光绪帝亲自到金棺前奠酒行礼](../Page/光绪帝.md "wikilink")，深表哀悼。光緒十九年四月十八日卯时，葬入定陵妃园寝。莊靜皇貴妃的宝顶居于园寝前排正中，是最尊贵的位置，位居咸豐帝諸妃中首位。

## 參考資料

[莊](../Category/清朝皇贵妃.md "wikilink")
[Zh](../Category/他塔喇氏.md "wikilink")
[Category:谥庄静](../Category/谥庄静.md "wikilink")

1.  中國第一歷史檔案館藏《宮中雜件》：五月初九日，蘭貴人下新進宮女子四名，麗貴人下新進宮女子四名，五月十一日春貴人下新進宮女子四名，婉常在下新進宮女子四名