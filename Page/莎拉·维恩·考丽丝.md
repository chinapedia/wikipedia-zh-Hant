**莎拉·维恩·考丽丝**（，**Sarah Wayne Callies**），原名为**Sarah Anne
Callies**，美国女演员，出生于伊利诺伊州。因其在美国电影连续剧《[越狱](../Page/越狱.md "wikilink")》的第一季、第二季和第四季中扮演监狱医生「莎拉」一角而为人所知。

## 生平经历

### 早年生活

莎拉一岁时，跟随全家一起搬到[夏威夷州首府](../Page/夏威夷州.md "wikilink")[檀香山](../Page/檀香山.md "wikilink")，在整个青少年时期，她就表现出了对-{}-表演的兴趣，并多次参加学校里举办的戏剧演出。

莎拉的父母在成为[夏威夷大学马诺阿分校的教授后搬至马诺阿](../Page/夏威夷大学马诺阿分校.md "wikilink")，但是莎拉没有跟随父母，而是选择了演员作为自己的职业。高中毕业后，她进入位于[新罕布-{}-什尔州汉诺威的](../Page/新罕布什尔州.md "wikilink")[达特茅斯学院](../Page/达特茅斯学院.md "wikilink")，学习的是女权主义，在本色化神学方面获得高级研究基金。毕业后，她继续其在戏剧表演方面的兴趣。\[1\]
她后来在[丹佛国家戏剧学院深造](../Page/丹佛.md "wikilink")，并于2002年获得了[藝術創作碩士学位](../Page/藝術創作碩士.md "wikilink")。\[2\]

### 演员生涯

莎拉第一次出演电视剧是在《Queens Supreme》中扮演Kate
O'Malley。她第四次担当主演是在[华纳兄弟的系列电视短片](../Page/华纳兄弟.md "wikilink")《[泰山](../Page/:en:Tarzan_\(2003_TV_series\).md "wikilink")》中扮演[Detective
Jane Porter](../Page/:en:Jane_Porter_\(Tarzan\).md "wikilink")。

她还为以下电视客串演出过：《[法律与秩序：特殊受害者](../Page/法律与秩序：特殊受害者.md "wikilink")》（Law &
Order: Special Victims
Unit），《[Dragnet](../Page/:en:Dragnet_\(series\).md "wikilink")》and《[數字搜查線](../Page/数字追凶.md "wikilink")》，随后，莎拉在[福克斯公司拍摄的](../Page/福克斯广播公司.md "wikilink")《[越狱](../Page/越狱_\(电视剧\).md "wikilink")》出演了监狱女医生[莎拉](../Page/:en:Sara_Tancredi.md "wikilink")。此外，她还在[环球影业制作的](../Page/环球影业.md "wikilink")《[Whisper](../Page/:en:Whisper_\(film\).md "wikilink")》，独立电影《[The
Celestine
Prophecy](../Page/:en:The_Celestine_Prophecy.md "wikilink")》及《[Bittersweet](http://www.indiepixfilms.com/film/3502)》担任主角。

### 个人生活

2002年6月21日，莎拉与在[达特茅斯学院认识的Josh](../Page/达特茅斯学院.md "wikilink")
Winterhalt结婚。Winterhalt是一位武术老师。2007年1月23日，莎拉的发言人宣布莎拉夫妇正盼望着他们的第一个孩子的到来。2007年7月，莎拉夫妇迎来了他们第一个女孩，取名为Keala
Winterhalt 读音为 Kay-AH-lah\[3\]\[4\]。

## 演出角色

### 电影

| 年份   | 电影                                                                                                   | 角色             |
| ---- | ---------------------------------------------------------------------------------------------------- | -------------- |
| 2006 | *[The Celestine Prophecy](../Page/:en:The_Celestine_Prophecy_\(film\).md "wikilink")*                | Marjorie       |
| 2007 | *[Whisper](../Page/:en:Whisper_\(film\).md "wikilink")*                                              | Roxanne        |
| 2008 | *[Bittersweet](../Page/:en:Bittersweet_\(film\).md "wikilink")*                                      | Robyn          |
| 2010 | Lullaby for Pi                                                                                       | Josephine      |
| 2011 | *[Faces in the Crowd](../Page/:en:Faces_in_the_Crowd_\(film\).md "wikilink")*                        | Francine       |
| 2011 | Four Saints                                                                                          | Mairi Chisholm |
| 2014 | Into the Storm                                                                                       | Allison Stone  |
| 2016 | [The Other Side of the Door](https://en.wikipedia.org/wiki/The_Other_Side_of_the_Door_\(2016_film\)) | Maria          |

### 电视

| 年份                                                                                                 | 电影                                       | 角色                                                       | 其它                          |
| -------------------------------------------------------------------------------------------------- | ---------------------------------------- | -------------------------------------------------------- | --------------------------- |
| 2003                                                                                               | 《Queens Supreme》                         | Kate O'Malley                                            | season 1.ep3,6,7,8,9        |
| 《[Law & Order: Special Victims Unit](../Page/:en:Law_&_Order:_Special_Victims_Unit.md "wikilink")》 | Jenny Rochester                          | season 4.ep17                                            |                             |
| 《[Dragnet](../Page/:en:Dragnet_\(series\).md "wikilink")》                                          | Kathryn Randall                          | season 1.ep6                                             |                             |
| 《[Tarzan](../Page/:en:Tarzan_\(WB_series\).md "wikilink")》                                         | Jane Porter                              |                                                          |                             |
| 2004                                                                                               | 《The Secret Service》                     | Laura Kelly                                              |                             |
| 2005                                                                                               | 《[NUMB3RS](../Page/數字追兇.md "wikilink")》  | Kim Hall                                                 | season 1.ep7                |
| 2005 - 2007                                                                                        | 《[越狱](../Page/越狱_\(电视剧\).md "wikilink")》 | [Sara Tancredi](../Page/:en:Sara_Tancredi.md "wikilink") |                             |
| 2008 -                                                                                             |                                          |                                                          |                             |
| 2010                                                                                               | 《[怪醫豪斯](../Page/怪醫豪斯.md "wikilink")》     | Julia                                                    | season 6.ep19 open and shut |
| 2010                                                                                               | 《[陰屍路](../Page/陰屍路.md "wikilink")》       | [Lori Grimes](../Page/:en:Lori_Grimes.md "wikilink")     |                             |

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Save The Good
    Doctor](http://community.livejournal.com/savethegooddr)

  - [Save-Sara-Tancredi.org](http://www.save-sara-tancredi.org/)

  -
  -
  - [Sarah Wayne Callies'
    biography](http://www.fox.com/prisonbreak/bios/bio_callies.htm) at
    *Prison Break's* official site

[C](../Category/美國電影女演員.md "wikilink")
[Category:達特茅斯學院校友](../Category/達特茅斯學院校友.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")

1.

2.
3.

4.