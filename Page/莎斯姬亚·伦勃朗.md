[Portrait_of_Saskia_van_Uylenburgh_by_Rembrandt.jpg](https://zh.wikipedia.org/wiki/File:Portrait_of_Saskia_van_Uylenburgh_by_Rembrandt.jpg "fig:Portrait_of_Saskia_van_Uylenburgh_by_Rembrandt.jpg")
**莎斯姬亚·凡·优伦堡**（，），是[荷兰画家](../Page/荷兰.md "wikilink")[伦勃朗的妻子](../Page/伦勃朗.md "wikilink")，她曾在一些著名的油画作品中担当[模特](../Page/模特.md "wikilink")。

## 生平

莎斯姬亚出生在[吕伐登](../Page/吕伐登.md "wikilink")，她是八个孩子中最小的一个。他的父亲Rombertus van
Uylenburgh是一名[律师](../Page/律师.md "wikilink")、镇长，同时还是弗拉内克大学的创办人之一。随着父母的先后过世，她在12岁的时候成为了孤儿。

莎斯姬亚通过她的叔叔[汉德瑞克·优伦堡](../Page/汉德瑞克·优伦堡.md "wikilink")（Hendrick
Uylenburgh）邂逅了她的远方表亲，也就是后来成为她丈夫的伦勃朗。汉德瑞克在1625年从[波兰搬家到](../Page/波兰.md "wikilink")[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")，后来成为是阿姆斯特丹颇具影响力的画商。在1631年，汉德瑞克开始为伦勃朗安排他在阿姆斯特丹和[門諾會的客户的订单](../Page/門諾會.md "wikilink")。之后的4年里，伦勃朗招收了一些学徒，其中包括
Govert Flinck, Gerbrandt van den Eeckhout 和 Ferdinand Bol。

1634年，伦勃朗娶了这位贵族表亲莎斯姬亚。由于莎斯姬亚与阿姆斯特丹的上层社会有着密切的联系，这使他从此能出入城里的名门望族之家。1639年伦勃朗与妻子移居到Sint
Anthonisbreestraat（今Jodenbreestraat），也就是现在的伦勃朗故居所在地。这座豪宅总价1.3万[荷兰盾](../Page/荷兰盾.md "wikilink")，并允许[分期付款](../Page/分期付款.md "wikilink")，对一个画家来讲仍是一笔浩大的开支。当时伦勃朗经济上非常富足，仅仅在《[夜巡](../Page/夜巡.md "wikilink")》上露上一脸也要100荷兰盾。莎斯姬亚生下的四個小孩，只有最小的兒子长大成人。这个孩子在1641年9月22日接受洗礼，取名叫提图斯（Titus），源于莎斯姬亚姐姐的名字Tietje。但不幸的是，1642年在提搭斯仅9个月大时，他的母亲莎斯姬亚便由于[肺结核过世了](../Page/肺结核.md "wikilink")，埋葬在[老教堂
(阿姆斯特丹)](../Page/老教堂_\(阿姆斯特丹\).md "wikilink")。

## 参考资料

  - Graaff, A. & M. Roscam Abbing (2006) Rembrandt voor Dummies. Addison
    Wesley.

## 外部链接

  - [The Amsterdam Municipal Archives on Saskia, Geertje, Rembrandt and
    many
    others](http://gemeentearchief.amsterdam.nl/schatkamer/rembrandt_prive/saskia_van_uylenburgh/rembrandt_en_saskia_trouwen/index.en.html)
  - [Look under famous
    names](http://www.gravenopinternet.nl/en/index.html)

[Category:伦勃朗](../Category/伦勃朗.md "wikilink")
[Category:罹患肺结核逝世者](../Category/罹患肺结核逝世者.md "wikilink")
[Category:呂伐登人](../Category/呂伐登人.md "wikilink")
[Category:葬于阿姆斯特丹老教堂](../Category/葬于阿姆斯特丹老教堂.md "wikilink")