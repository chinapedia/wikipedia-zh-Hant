**莎麗·海明斯**（**Sally
Hemings**，）是一位非裔美國[奴隸](../Page/奴隸.md "wikilink")，自從1802年的一篇報導出現以後，關於[托马斯·杰斐逊是否為莎麗](../Page/托马斯·杰斐逊.md "wikilink")·海明斯數名子女的父親之問題一直倍受爭議。

根據傑佛遜的記載，她一共有5名子女：

  - Harriet Hemings (I) (October 5, 1795 - December 7, 1797) \[1\]
  - Beverly Hemings *(possibly born William Beverly Hemings)* (April 1,
    1798 - after 1873) \[2\]
  - Harriet Hemings (II) (May 22, 1801 - after 1863) \[3\]
  - [Madison Hemings](../Page/Madison_Hemings.md "wikilink") *(possibly
    born James Madison Hemings)* (January 19, 1805 - 1877) \[4\]
  - Eston Hemings *(possibly born Thomas Eston Hemings)* (May 21, 1808 -
    1856) \[5\]

## 參考文獻

[Category:非洲裔美国人](../Category/非洲裔美国人.md "wikilink")
[Category:托马斯·杰斐逊](../Category/托马斯·杰斐逊.md "wikilink")
[Category:19世紀美國人](../Category/19世紀美國人.md "wikilink")
[Category:19世紀女性](../Category/19世紀女性.md "wikilink")
[Category:18世纪美国人](../Category/18世纪美国人.md "wikilink")

1.  <http://www.whosyomama.com/gabroaddrick3/26807.htm>
2.  <http://www.whosyomama.com/gabroaddrick3/26808.htm>
3.  <http://www.whosyomama.com/gabroaddrick3/26809.htm>
4.  <http://www.whosyomama.com/gabroaddrick3/26810.htm>
5.  <http://www.whosyomama.com/gabroaddrick3/26811.htm>