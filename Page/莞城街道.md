**莞城街道**通称“**莞城区**”，是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[东莞市下辖的一个](../Page/东莞市.md "wikilink")[街道](../Page/街道.md "wikilink")\[1\]，由原莞城、附城、[步步高等合并而成](../Page/步步高.md "wikilink")，是东莞市旧的政治和文化中心。总面积13.5平方公里，[户籍人口](../Page/户籍人口.md "wikilink")15.2万人，外来人口9.8万人。区内有广东四大名园之一的[东莞可园](../Page/东莞可园.md "wikilink")。

## 行政區劃

莞城街道下辖以下地区：\[2\]

。

## 图集

## 参考资料

  - [莞城概况](https://web.archive.org/web/20070313182957/http://guancheng.dg.gov.cn/desktop/publish/FileView.aspx?CategoryID=3&PageID=1)，东莞市莞城区办事处网站。

## 外部链接

  - [东莞市莞城区办事处网站](https://web.archive.org/web/20061101110055/http://guancheng.dg.gov.cn/)

[Category:东莞行政区划](../Category/东莞行政区划.md "wikilink")

1.
2.