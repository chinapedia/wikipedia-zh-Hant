**莫利塞**（，）是[意大利中部的一个大区](../Page/意大利.md "wikilink")，是意大利第二小的大区。大区的前身是[阿布魯齊莫利塞區](../Page/阿布魯齊莫利塞區.md "wikilink")（直至1963年前与[阿布鲁齐大区合并](../Page/阿布鲁齐.md "wikilink")）的一部分，现时已分成两个个别的大区。莫利塞大区西北临[阿布鲁齐大区](../Page/阿布鲁齐.md "wikilink")，西临[拉齐奥大区](../Page/拉齐奥.md "wikilink")，南界临[坎帕尼亚大区](../Page/坎帕尼亚.md "wikilink")，东南临[普利亚大区及东北界临](../Page/普利亚.md "wikilink")[亚得里亚海](../Page/亚得里亚海.md "wikilink")。

大区面积4,438[平方公里](../Page/平方公里.md "wikilink")，人口321,047人。大区内有两个少数民族:
一个民族是[莫利塞-克罗埃人](../Page/莫利塞-克罗埃人.md "wikilink")（Molisan
Croatians），当中有2,500人说克罗埃-达尔马提亚语中的一种古代方言。另外一个少数民族是莫利塞-阿尔巴尼亚人（Molisan
Albanians），他们则另说阿尔巴尼亚语中的一种古代方言，这种语言跟现时亚得里亚海对岸的民族所说的阿尔巴尼亚语有很大的分别。莫利塞-阿尔巴尼亚人一般相信正教（不是东正教）。

大区内有很多社区于第二次世界大战受到破坏。大批盟军地面部队以坎波巴索为基地，被加拿大士兵称为"枫叶城"。其中一个能在战争中逃过一劫的古老城镇是拿里路。该镇历史悠久的镇中心是南意大利大的镇中心之一。该镇能免受摧毁，某程度上是因为它被山谷包围，位于山谷内一块突出的土地上，所以该镇的建设也严格受到市政府控制。在2002年的数次地震中復原之后，拿里路表现出莫利塞地区于18及19世纪的景象。

莫利塞大区大多数地区都由小城镇组成，分散于山区之间。大区内有其中一间全世界历史最旧的製钟厂，称为[阿格能](../Page/阿格能.md "wikilink")，該厂现时还使用旧式方法制钟。

大区的首府是[坎波巴索](../Page/坎波巴索.md "wikilink")。大区分成两个省分:[坎波巴索省及](../Page/坎波巴索省.md "wikilink")[伊塞尔尼亚省](../Page/伊塞尔尼亚省.md "wikilink")。

其它重要城市包括：[泰爾莫利](../Page/泰爾莫利.md "wikilink")、[韋納夫羅](../Page/韋納夫羅.md "wikilink")、[拉里诺及](../Page/拉里诺.md "wikilink")[博亚诺](../Page/博亚诺.md "wikilink")（古代[萨姆奈特人的首都](../Page/萨姆奈特人.md "wikilink")）。

## 参考文献

## 外部連結

  - [莫利塞大区官方网站](https://web.archive.org/web/20020402070602/http://www.regione.molise.it/)
  - [莫利塞地图](https://web.archive.org/web/20060623032601/http://www.italy-weather-and-maps.com/maps/italy/molise.gif)
  - [Molise Città](http://www.molisecitta.it/comuni.html)
  - [意大利旅游网:莫利塞](https://web.archive.org/web/20060903073415/http://www.italianvisits.com/molise/)

[\*](../Category/莫利塞大区.md "wikilink")