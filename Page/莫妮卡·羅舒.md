**莫妮卡·羅舒**（，）是一位[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")[體操選手](../Page/體操.md "wikilink")，也是奧運金牌得主。羅舒從4歲時開始練習體操，並在2004年[雅典奧運中奪得奧運女子體操跳馬金牌](../Page/2004年夏季奥林匹克运动会.md "wikilink")，於2005年退休。

[Category:羅馬尼亞體操運動員](../Category/羅馬尼亞體操運動員.md "wikilink")
[Category:羅馬尼亞奧林匹克運動會金牌得主](../Category/羅馬尼亞奧林匹克運動會金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:奧林匹克運動會體操獎牌得主](../Category/奧林匹克運動會體操獎牌得主.md "wikilink")
[Category:世界体操锦标赛奖牌得主](../Category/世界体操锦标赛奖牌得主.md "wikilink")