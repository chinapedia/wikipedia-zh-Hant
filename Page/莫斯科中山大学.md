[UTK_studentki_sotrudnicy1926.jpg](https://zh.wikipedia.org/wiki/File:UTK_studentki_sotrudnicy1926.jpg "fig:UTK_studentki_sotrudnicy1926.jpg")
**中國勞動者孫逸仙大學**（），1928年更名为**中國勞動者孫逸仙共產主義大學**，中文通稱**莫斯科中山大學**，是[俄羅斯一所已經廢校的](../Page/俄羅斯.md "wikilink")[大學](../Page/大學.md "wikilink")，於1925年[秋季在](../Page/秋季.md "wikilink")[苏联支持之下成立于](../Page/苏联.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")，因[孙中山的](../Page/孙中山.md "wikilink")[聯俄容共政策而诞生](../Page/聯俄容共.md "wikilink")，故以「中山」為名。1930年夏天宣布解散，前後歷時短短5年。
[UTK-KUTK_students_Moscow.jpg](https://zh.wikipedia.org/wiki/File:UTK-KUTK_students_Moscow.jpg "fig:UTK-KUTK_students_Moscow.jpg")

## 學校性質

莫斯科中山大學所招生的对象并不仅仅是[国民党人](../Page/中國國民黨.md "wikilink")，更多的是[共产党人](../Page/中國共產黨.md "wikilink")，是为当时[国共合作的](../Page/国共合作.md "wikilink")[中国大革命培养政治理论骨干的特殊学校](../Page/第一次国内革命战争.md "wikilink")。莫斯科中山大學採用小班制上課，課程主要有[俄文](../Page/俄文.md "wikilink")、[歷史](../Page/歷史.md "wikilink")（主要是各種革命史）、[哲學](../Page/哲學.md "wikilink")（[唯物論](../Page/唯物論.md "wikilink")、[辯證法](../Page/辯證法.md "wikilink")、政治經濟學）、經濟地理、[列寧主義](../Page/列寧主義.md "wikilink")、軍事科學等。\[1\]学制两年。

1927年7月26日，由於受到蔣介石當局的「清黨」（[四一二事件](../Page/四一二事件.md "wikilink")）的影響，中国国民党中央执行委员会发表声明：“取缔”中山大学，并与之断绝一切关系，同时命令各级组织严禁再向[莫斯科派遣学生](../Page/莫斯科.md "wikilink")。

1928年春，更名为**中国共产主义劳动大学**。\[2\]

莫斯科中山大学于1930年夏天宣布解散，前后历时短短5年。该学校地址为莫斯科16号。

### 「中山大學模式」

[廣州](../Page/中山大學.md "wikilink")、莫斯科、[高雄](../Page/國立中山大學.md "wikilink")3所中山大學皆為列寧式[黨國所創](../Page/黨國.md "wikilink")，基於[社會主義特徵的黨化教育](../Page/社會主義.md "wikilink")（partification
of
education），培育國家革命及建設人才。美國華裔歷史學家[葉文心概括為](../Page/葉文心.md "wikilink")「[中山大學模式](../Page/中山大學模式.md "wikilink")」\[3\]\[4\]。

|                                          | 创建    | 首任校长                                   | 教育背景                                                                                                                                                  |
| ---------------------------------------- | ----- | -------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| [广州中山大学](../Page/广州中山大学.md "wikilink")   | 1924年 | [邹鲁](../Page/邹鲁.md "wikilink")         | 仿[苏联模式创建而成](../Page/苏联.md "wikilink")，培育[国民革命的](../Page/国民革命.md "wikilink")[政工干部与](../Page/政工.md "wikilink")[国家建设人才](../Page/国民政府建国大纲.md "wikilink")。 |
| [莫斯科中山大学](../Page/莫斯科中山大学.md "wikilink") | 1925年 | [卡尔·拉狄克](../Page/卡尔·拉狄克.md "wikilink") | [苏联共产党的党团组织](../Page/苏联共产党.md "wikilink")，培育国共两党、中苏两国的[布尔什维克](../Page/布尔什维克.md "wikilink")。                                                           |
| [高雄中山大学](../Page/高雄中山大学.md "wikilink")   | 1980年 | [李焕](../Page/李焕.md "wikilink")         | 始于[智库学者](../Page/幕僚部门.md "wikilink")，培育[三民主义模范省的国家建设人才](../Page/三民主义模范省.md "wikilink")。                                                               |

## 歷史地位

莫斯科中山大学对[中国现代史产生了深远的影响](../Page/中国现代史.md "wikilink")，他培養了諸如：[国民党方面的内政部长](../Page/国民党.md "wikilink")[谷正纲](../Page/谷正纲.md "wikilink")、[蒋介石之子](../Page/蒋介石.md "wikilink")[蒋经国](../Page/蒋经国.md "wikilink")、[冯玉祥之子](../Page/冯玉祥.md "wikilink")[冯洪国](../Page/冯洪国.md "wikilink")、女兒[冯弗能](../Page/冯弗能.md "wikilink")、[冯弗伐](../Page/冯弗伐.md "wikilink")、[邵力子之子](../Page/邵力子.md "wikilink")[邵志刚](../Page/邵志刚.md "wikilink")、[叶楚伧之子](../Page/叶楚伧.md "wikilink")[叶楠](../Page/叶楠.md "wikilink")、[于右任之女](../Page/于右任.md "wikilink")[-{于}-秀芝](../Page/于秀芝.md "wikilink")，和共产党方面的「[二十八个半布尔什维克](../Page/二十八个半布尔什维克.md "wikilink")」、[邓小平](../Page/邓小平.md "wikilink")、[乌兰夫](../Page/乌兰夫.md "wikilink")、[叶剑英](../Page/叶剑英.md "wikilink")、[董必武](../Page/董必武.md "wikilink")、[林伯渠](../Page/林伯渠.md "wikilink")、[徐特立](../Page/徐特立.md "wikilink")、[何叔衡](../Page/何叔衡.md "wikilink")、[杨之华](../Page/杨之华.md "wikilink")、[杨子烈](../Page/杨子烈.md "wikilink")、[施静宜](../Page/施静宜.md "wikilink")、[趙一曼](../Page/趙一曼.md "wikilink")、[左权等都曾经是该大学的学生](../Page/左权.md "wikilink")。蔣經國說過，[烏蘭夫是跟他當年曾坐於同一椅](../Page/烏蘭夫.md "wikilink")、旁邊的同學。

[台灣共產黨主要成員](../Page/台灣共產黨.md "wikilink")[謝雪紅也曾經在此就讀](../Page/謝雪紅.md "wikilink")，并與蔣經國見過面。

據[劉江南所著的](../Page/劉宜良.md "wikilink")《蔣經國傳》中描述，中山大學實際上只是[蘇聯共產黨的一個黨團組織](../Page/蘇聯共產黨.md "wikilink")，學生每天需上至少4小時的[俄文課](../Page/俄文.md "wikilink")，內容也跟現時人們所知道的[文科天差地遠](../Page/文科.md "wikilink")、甚至幾乎完全無關；學生每天都要撰文「反省」，匯報思想。

不過，[中國籍學生的待遇](../Page/中國人.md "wikilink")，例如食宿，則比[俄籍本地學生的待遇為佳](../Page/俄國人.md "wikilink")；[蔣經國也曾感到](../Page/蔣經國.md "wikilink")「不好意思」，對俄國學生懷有同情。

曾经为中國國、共兩党培养政、軍幹部的兩大学校，就是[黄埔军校和莫斯科中山大学](../Page/黄埔军校.md "wikilink")。

## 历届校长

  - 第一任：[卡尔·拉狄克](../Page/卡尔·拉狄克.md "wikilink") （1925年-1927年）
  - 第二任：[巴威尔·亚历山大洛维奇·米夫](../Page/巴威尔·亚历山大洛维奇·米夫.md "wikilink")（1927年-1929年）
  - 第三任：威格尔 （） (1929-1930)

## 參考資料

<references/>

{{-}}

[Category:中华民国大陆时期历史](../Category/中华民国大陆时期历史.md "wikilink")
[莫斯科中山大学](../Category/莫斯科中山大学.md "wikilink")
[Category:莫斯科的大學](../Category/莫斯科的大學.md "wikilink")
[M](../Category/1925年創建的教育機構.md "wikilink")
[M](../Category/1930年廢除.md "wikilink")
[Category:冠以人名的教育機構](../Category/冠以人名的教育機構.md "wikilink")

1.  李子渝、常壽林編寫，《蔣經國》，[香港](../Page/香港.md "wikilink")，[教育出版社](../Page/教育出版社.md "wikilink")，第11頁，ISBN
    962-12-1573-1
2.  [毛齐华著](../Page/毛齐华.md "wikilink")，《风雨征程七十春》，第55页
3.  余敏玲，〈[國際主義在莫斯科中山大學，1925-1930](http://www.mh.sinica.edu.tw/MHWeb/UserDetail.aspx?userID=59&mid=16&tmid=2)〉，《中央研究院近代史研究所集刊》，第26期，1996年12月。
4.  葉文心，《民國時期大學校園文化（1919——1937）》，第五章。