[Kremlevskaya_Naberezhnaja_Moscow.hires.jpg](https://zh.wikipedia.org/wiki/File:Kremlevskaya_Naberezhnaja_Moscow.hires.jpg "fig:Kremlevskaya_Naberezhnaja_Moscow.hires.jpg")

**莫斯科河**（）是[奧卡河的左](../Page/奧卡河.md "wikilink")[支](../Page/支流.md "wikilink")，流經[莫斯科州和](../Page/莫斯科州.md "wikilink")[斯摩棱斯克州](../Page/斯摩棱斯克州.md "wikilink")，全長503公里，流域面積17,600平方公里。每年十一、二月封凍到第二年的三、四月。

沿河最重要的城市是[俄羅斯首都](../Page/俄羅斯.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")，城名亦由河名而來。有數個說法：可能是[芬蘭-烏戈爾語系中](../Page/芬蘭-烏戈爾語系.md "wikilink")「黑暗、渾濁」的意思，[科米語](../Page/科米語.md "wikilink")「[牛河](../Page/牛.md "wikilink")」或者[莫尔多维亚语支](../Page/莫尔多维亚语支.md "wikilink")（包括[莫克沙语及](../Page/莫克沙语.md "wikilink")[厄尔兹亚语](../Page/厄尔兹亚语.md "wikilink")）「[熊河](../Page/熊.md "wikilink")」的意思。

## 外部連結

  - [](http://www.coolpool.ru/om/rm/index.htm)
  - [](http://www.finmarket.ru/z/nws/news.asp?id=422508&hot=569542)
  - [](http://www.gorod-moscow.ru/photoalbum.php?dir=Rivers/Moscow-river)
  - [](http://mosriver.narod.ru/moskva.htm)
  - [](https://web.archive.org/web/20140817010934/http://rgo.msk.ru/commissions/toponymy/2006_12_12.html)

[М](../Category/伏爾加河.md "wikilink")
[Category:莫斯科州地理](../Category/莫斯科州地理.md "wikilink")
[Category:斯摩棱斯克州地理](../Category/斯摩棱斯克州地理.md "wikilink")