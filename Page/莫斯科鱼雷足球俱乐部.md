**莫斯科鱼雷足球俱乐部**（俄语: Футбольный клуб "Торпедо"
Москва）是一支位于[俄罗斯](../Page/俄罗斯.md "wikilink")[莫斯科的职业足球俱乐部](../Page/莫斯科.md "wikilink")，创办于1924年，曾经使用过这样几个名字：

  - *Proletarskaya kuznitsa*（1924–31）
  - *AMO*（1931–32）
  - *ZIS*（1933–35）
  - *鱼雷（Torpedo）*（1936-1995）
  - *鱼雷-卢日尼基（Torpedo-Luzhniki）*（1996–97）
  - *鱼雷（Torpedo）*（1998起）

莫斯科鱼雷足球俱乐部曾经获得过3次[苏联足球联赛冠军](../Page/苏联足球联赛冠军.md "wikilink") (1960, 1965,
1976秋)， 6次[苏联杯冠军](../Page/苏联杯.md "wikilink")(1949, 1952, 1960, 1968,
1972, 1986)， 1次 [俄罗斯杯冠军](../Page/俄罗斯杯.md "wikilink")
(1993)，以及三次出现在[欧洲冠军杯以及](../Page/欧洲冠军杯.md "wikilink")[欧洲联盟杯的四分之一决赛赛场上](../Page/欧洲联盟杯.md "wikilink")。

## 陣容

*根据
[2007年俄超官方网站阵容](https://web.archive.org/web/20070812133405/http://www.pfl.ru/DESIGN.2001/SOKOLOV/0710_7.HTM).*

## 外部链接

  - [俱乐部官方网站](http://fc-tm.ru/)

[Category:俄罗斯足球俱乐部](../Category/俄罗斯足球俱乐部.md "wikilink")
[Category:莫斯科足球俱乐部](../Category/莫斯科足球俱乐部.md "wikilink")
[Category:1924年建立的足球俱樂部](../Category/1924年建立的足球俱樂部.md "wikilink")