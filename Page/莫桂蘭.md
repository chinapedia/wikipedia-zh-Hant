**莫桂蘭**（）是[嶺南武術宗師](../Page/嶺南.md "wikilink")[黃飛鴻的第四任](../Page/黃飛鴻.md "wikilink")[妻子](../Page/妻子.md "wikilink")，[廣東](../Page/廣東.md "wikilink")[高州人](../Page/高州.md "wikilink")，童年时父母双逝，被广州开武馆的叔父收养和接养，自幼练[南拳](../Page/南拳.md "wikilink")[五大门派之一](../Page/五大门派.md "wikilink")：[莫家拳](../Page/莫家拳.md "wikilink")。

1911年，黃飛鴻在端午節龍舟賽後跟弟子去校場表演梅花樁舞獅。據聞當時19歲的莫桂蘭慕名到場觀摩，卻因為黃飛鴻一時失誤，被他脫出的鞋子擊中。莫桂蘭大怒，躍上擂台展莫家拳狂攻黃飛鴻，一打結緣。最終在莫桂蘭的叔父作主下，莫桂蘭十九歲下嫁花甲黃飛鴻，年齡相差約四十歲。\[1\]\[2\]

由於黃飛鴻當前三任妻子都是早逝，黃飛鴻自認[剋妻](../Page/剋妻.md "wikilink")，且黃曾發誓不再[續弦](../Page/續弦.md "wikilink")，就以莫桂蘭為[側室](../Page/側室.md "wikilink")，然而實際上莫桂蘭是黃飛鴻當時唯一的[配偶](../Page/配偶.md "wikilink")。\[3\]\[4\]

由於莫桂蘭有莫家拳的武術底子，黃飛鴻便將自己的各式洪家拳如虎鶴雙形、鐵綫拳等傳授她。莫桂蘭亦掌握了黃飛鴻的獅藝訣竅，並籌組了一個女子舞獅團，開獅藝先河。\[5\]

1924年，寶芝林醫館在武裝暴亂中被焚毀，之後黃飛鴻過身，莫桂蘭生活不如意，但仍堅持傳授黃飛鴻的武藝。\[6\]莫桂蘭於1936年移居[香港](../Page/香港.md "wikilink")，以行醫和授拳維生，開設[武館授徒](../Page/武館.md "wikilink")。1944年至1969年期間，她於[香港島](../Page/香港島.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[告士打道](../Page/告士打道.md "wikilink")116號開設「黃飛鴻國術社」教授[武術](../Page/武術.md "wikilink")，之後到[筲箕灣](../Page/筲箕灣.md "wikilink")「黃飛鴻健身學院」任教，及後因租金上升和重建等原因，莫桂蘭多次搬遷武館，至1980年代初因習武風潮日漸衰退而結束武館，莫桂蘭於1982年11月3日安詳離世，享年90歲。\[7\]\[8\]

## 相關影視作品

  - [香港](../Page/香港.md "wikilink")[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")：

<!-- end list -->

1.  《[我師傅係黃飛鴻](../Page/我師傅係黃飛鴻.md "wikilink")》：[李彩樺](../Page/李彩樺.md "wikilink")
    飾 莫桂蘭
2.  《[女拳](../Page/女拳.md "wikilink")》：[劉璇](../Page/劉璇_\(體操運動員\).md "wikilink")
    飾 莫桂蘭

## 參考文獻

[Category:高州人](../Category/高州人.md "wikilink")
[Category:香港武術家](../Category/香港武術家.md "wikilink")
[Category:莫姓](../Category/莫姓.md "wikilink")
[Category:中國武術家](../Category/中國武術家.md "wikilink")

1.

2.

3.

4.

5.
6.
7.

8.