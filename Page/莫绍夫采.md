<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><big><strong>莫绍夫采<br />
Mošovce</strong></big></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Mosovce.jpg" title="fig:Mosovce.jpg">Mosovce.jpg</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Map_slovakia_mosovce.png" title="fig:Map_slovakia_mosovce.png">Map_slovakia_mosovce.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>基本资料</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>州</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>面积</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>人口</strong><br />
- 总人口<br />
- <a href="../Page/人口密度.md" title="wikilink">人口密度</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>坐标</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>网站</strong></p></td>
</tr>
</tbody>
</table>

**莫绍夫采 (Mošovce)**是[斯洛伐克境内图列茨地区最大的村落](../Page/斯洛伐克.md "wikilink")。

## 历史

许多历史建筑保存证明这个城市有770多年的历史。在[国王](../Page/国王.md "wikilink")[安德鲁二世的捐赠行为中首次提到](../Page/安德烈二世.md "wikilink").
原本Mošovce是由两个定居点组成:第一,*Machyuch*,
位于今天的*StarýRad*,第二是*TerraMoys*,现在该村仍用此名称，它曾占领地当今Vidrmoch.
即它的名字第二意义*Mojš*的土地。,这使人们相信，全村曾经属于某些*Mojš*先生，他的名字曾被一个缩写的[斯拉夫名字](../Page/斯拉夫人.md "wikilink")*Mojtech*，类似的名称有*Vojtech*或*Mojmír*.
历代名村发生了许多变化，从*Mossovych*, *Mosocz*, *Mossowecz*到凤凰山庄，*Mayos Alio Nomine
Mossovych*村落到*Oppidioum Mayus Sue Mosocz*,发展成*Mosocz Olim
Mayus*的村落现在变成Mošovce.
Mošovce分离的古代部分，前遗址*Chornukov*一直保存在现代*Čerňakov*形式.
Mošovce首先作为皇室部落被自由宣传发展，,从14世纪中叶的城市作为特权城[镇归属于Blatnica皇室](../Page/镇.md "wikilink")[城堡](../Page/城堡.md "wikilink")。它在1527年陷入了Révay家族手中，他们镇压城市特权将近四百年.
过去, Mošovce Turiec是一个重要的工艺中心地区.
工艺经历了惊人传播，约有15个城市设有[公会活动](../Page/公会.md "wikilink");
皮靴商人和毛皮商人[公会存在时间最长](../Page/公会.md "wikilink")。现在Mošovce被改建成一个有许多景点的重要旅游区。

## 景点

其中最引人注目的是[古迹](../Page/不可移动文物.md "wikilink")—[洛可可](../Page/洛可可.md "wikilink")[古典宅邸](../Page/古典.md "wikilink")，18世纪后半期扩建的英格兰式[公园](../Page/公园.md "wikilink").
其他用地城市包括:
JánKollár诞生地，具有宝贵古代前身的古新歌德[天主](../Page/天主教.md "wikilink")[教堂](../Page/教堂.md "wikilink")，建于1784年的路德[教会](../Page/教会.md "wikilink")，陵墓改建的工艺[博物馆](../Page/博物馆.md "wikilink"),从1800年至今的[艺术](../Page/艺术.md "wikilink")[温室](../Page/温室.md "wikilink").

[Dom-Mosovce.jpg](https://zh.wikipedia.org/wiki/File:Dom-Mosovce.jpg "fig:Dom-Mosovce.jpg")

## 大自然

Mošovce周围有很多独特风景. 一个复杂的历史园林和果树园，创造新颖的美学景观，这些风景林地扩大到Veľká
Fatra[山区](../Page/山脉.md "wikilink").
这个山脉是[斯洛伐克最吸引人的山脉](../Page/斯洛伐克.md "wikilink").
美国海军和[石灰质的奇异造型](../Page/石灰岩.md "wikilink")，以及在Blatnická附近的美丽大自然和Gaderská村落，吸引着来自世界各地的人.

## 文化传统

Mošovce产生了许多重要人物. 最伟大的是Frico
Kafenda(1883年至1963年),[作曲家](../Page/作曲家.md "wikilink")。
Lacková安娜-Zora(1899年至1988年)、[作家](../Page/作家.md "wikilink") 。Štefan
Krčméry(1892年至1955年)、[文学评论家](../Page/文学评论家.md "wikilink")、[历史学家](../Page/历史学家.md "wikilink")、[诗人](../Page/诗人.md "wikilink")。
Júr Tesák
Mošovský、[巴洛克](../Page/巴洛克艺术.md "wikilink")[剧作家](../Page/剧作家.md "wikilink")。
Miloslav Schmidt,创办了业余消防队。
但是，也许最重要的人是出生在Mošovce的斯拉夫[诗人](../Page/诗人.md "wikilink")、[哲学](../Page/哲学.md "wikilink")[家路德Preacher](../Page/信義宗.md "wikilink"),
Ján Kollár(1793年至1852年),他的[文学影响广泛](../Page/文学.md "wikilink")，他的诗剧*Slávy
Dcera*至少深刻地影响两个国家。他的作品充满了当代爱国主义与[民族活动家的热情](../Page/民族.md "wikilink").
已被译成各种[斯拉夫以及非斯拉夫语言](../Page/斯拉夫语族.md "wikilink")。

## 对外联系

  - [国际互联网 书籍 - Mošovce](http://www.mosovce.sk)
  - [观光客
    假订本](https://web.archive.org/web/20070928160138/http://www.mosovce.sk/doc/tur_spr/Brozura-anj.doc)
  - [资讯 - Mošovce](http://www.tourist-channel.sk/mosovce/indexen.html)
  - [Drienok](https://web.archive.org/web/20081028143321/http://www.bb.telecom.sk/drienok/english/)

## 画廊

[File:Kastielmosovce.jpg|洛可可-古典宅邸在Mošovce](File:Kastielmosovce.jpg%7C洛可可-古典宅邸在Mošovce)
[File:Kostolmosovce1.jpg|路德教会Mošovce](File:Kostolmosovce1.jpg%7C路德教会Mošovce)
[File:Kostolmosovce2.JPG|天主教Mošovce](File:Kostolmosovce2.JPG%7C天主教Mošovce)
[File:Kaplnka-Mosovce.jpg|neogothic教堂、陵墓后的博物馆Mošovce](File:Kaplnka-Mosovce.jpg%7Cneogothic教堂、陵墓后的博物馆Mošovce)

[File:Vlajka-Mosovce.jpg|区旗Mošovce](File:Vlajka-Mosovce.jpg%7C区旗Mošovce)
[File:Mapa-Mosovce.png|Mošovce](File:Mapa-Mosovce.png%7CMošovce)

[Category:斯洛伐克城市](../Category/斯洛伐克城市.md "wikilink")