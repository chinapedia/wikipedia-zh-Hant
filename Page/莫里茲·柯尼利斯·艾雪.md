[Gevel_Escher_in_Het_Paleis_300_dpi.jpg](https://zh.wikipedia.org/wiki/File:Gevel_Escher_in_Het_Paleis_300_dpi.jpg "fig:Gevel_Escher_in_Het_Paleis_300_dpi.jpg")艾雪博物館,牆上作品為《白天與黑夜》\]\]
**莫里茲·柯尼利斯·艾雪**（或譯為**埃舍爾**、**艾薛爾**、**葉夏**）（Maurits Cornelis
Escher）IPA發音:\[ˈmʌurɪts kɔrˈneːlɪs
ˈɛsxər\]（），為知名荷蘭[版畫藝術家](../Page/版畫.md "wikilink")，其活躍於[義大利](../Page/義大利.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[比利時與](../Page/比利時.md "wikilink")[荷蘭地區](../Page/荷蘭.md "wikilink")60年間。知名於其[錯視藝術作品](../Page/錯視.md "wikilink")，於平面視覺藝術有極大成就。

## 生平

[Escher_Circle_Limit_IV.jpg](https://zh.wikipedia.org/wiki/File:Escher_Circle_Limit_IV.jpg "fig:Escher_Circle_Limit_IV.jpg")》,
*Circle Limit IV*, 1960\]\]
[Escher_Circle_Limit_III.jpg](https://zh.wikipedia.org/wiki/File:Escher_Circle_Limit_III.jpg "fig:Escher_Circle_Limit_III.jpg")》,
*Circle Limit III*, 1959\]\]
莫里茲·柯尼利斯·艾雪於1898年6月17日生於[弗里斯蘭省首府](../Page/弗里斯蘭省.md "wikilink")[吕伐登](../Page/吕伐登.md "wikilink")，他是他父親喬治·阿諾德·艾雪（一位土木工程師）和第二個妻子薩拉·格里奇曼所生的小兒子。1903年莫里茲·柯尼利斯·艾雪一家遷到[阿納姆](../Page/阿納姆.md "wikilink")，在那裏他度過了小學和中學時期。幼年的艾雪體質羸弱，曾在特殊學校就讀，1912至18年間，他就讀[中學](../Page/中學.md "wikilink")。他的繪畫天分很高，可惜其學業成績頗差，二年級時甚至要重讀。十三歲前，他學習了[木工和](../Page/木工.md "wikilink")[鋼琴](../Page/鋼琴.md "wikilink")。1919年他進入[哈勒姆](../Page/哈勒姆.md "wikilink")（Haarlem）建築及裝飾藝術學校學習建築，但是在數門科目[掛科後轉學裝飾藝術](../Page/掛科.md "wikilink")。在該校擔任美術教授的版畫家薩繆爾·吉西農·德·馬斯奎塔（
Samuel Jesserun de
Mesquita）發現了莫里茲·柯奈利斯·艾雪的才能，教授他製作版畫的技術。兩人後來成為了多年的朋友。1922年他離開該校。在這一時期，莫里茲·柯奈利斯·艾雪創作的作品[八張臉](../Page/八張臉.md "wikilink")（Eight
Heads）、[聖巴佛大教堂，哈勒姆](../Page/聖巴佛大教堂，哈勒姆.md "wikilink") （St.Bavo's,
Haarlem）已經顯露出他的獨特視角。

畢業之後莫里茲·柯尼利斯·艾雪去往[義大利旅行](../Page/義大利.md "wikilink")。是年6月，他第一次來到在西班牙格拉納達的[阿爾罕布拉宮](../Page/阿爾罕布拉宮.md "wikilink")。這座14世紀的穆斯林建築運用在墻壁和地板上的平面[鑲嵌圖案讓他對](../Page/鑲嵌.md "wikilink")[密舖平面產生了興趣](../Page/密舖平面.md "wikilink")，並且嘗試應用在作品中。開始的嘗試並不成功，他放棄了這個主題並回到了義大利。1923年莫里茲·柯奈利斯·艾雪在阿瑪爾菲海岸旅行，遇上瑞典姑娘耶塔·烏米克（Jetta
Umiker），兩人後來於1924年結婚。他們定居於[羅馬](../Page/羅馬.md "wikilink")，1935年，因[墨索里尼統治下的政治氣氛而遷往](../Page/墨索里尼.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")。他們在當地住了兩年。

他十分喜愛意大利的風景，因此他在瑞士不太快樂。1937年他們搬家去比利時的小鎮[于克勒](../Page/于克勒.md "wikilink")（Ukkel）。[二戰爆發后](../Page/二戰.md "wikilink")，1941年1月，他們又一次搬家，搬到荷蘭[巴伦](../Page/巴伦_\(荷兰\).md "wikilink")（Baarn）。他們在這裏住上了30年。

他大部分名作都是在這段時期繪畫。荷蘭多雲、冷、潮濕的天氣逼使他專注工作，除了1962年的他進行外科手術後的一段時間。

1970年，他搬到[拉伦](../Page/拉伦.md "wikilink")（Laren），那裏是他的退休居所，他有自己的工作室。他在兩年後逝世，享年73歲。

## 藝術風格

### 錯視藝術

艾雪數學成績並不好，但他的版畫作品常使用到[幾何的概念](../Page/幾何.md "wikilink")，對於「[錯視](../Page/錯視.md "wikilink")」藝術極為拿手。艾雪作品反應出幽微的內心世界，也是他對於當時歷史的反思，也反映出他探討宇宙秩序奧密的渴望。艾雪的版畫技術卓越，足以充分善用不同技巧的優點。他以明顯的黑、白色對比與精準的線條刻劃，樹立了與眾不同的版畫風格；他也創造演繹出複雜奇幻的變形結構，顛覆了自然的規律。

他擅長以重複排列的圖形填充畫面空間，甚至將平面的畫面轉變為凹或凸的球面，讓圖形排列的透視效果更顯著。

[MC-Escher-Drawing-Hands-1948-245x212.jpg](https://zh.wikipedia.org/wiki/File:MC-Escher-Drawing-Hands-1948-245x212.jpg "fig:MC-Escher-Drawing-Hands-1948-245x212.jpg")
他不斷挑戰平面與立體的界線，甚至再加上循環畫面的技術，使觀眾無法確認畫面的開始與結束，例如：《畫手》(Drawing
hands,1948)中即利用了以上手法。他另一個創作重要手法為「[反射](../Page/反射.md "wikilink")」，於1930年開始繪製、研究靜物，經常使用[球體或水面反射來刻劃](../Page/球體.md "wikilink")。

### 人物

對於人物形象描繪艾雪也有著墨，早期作品中常描繪人面孔清晰甚至與他親近的人物。然而，經歷了[二戰](../Page/二戰.md "wikilink")，艾雪對於社會的看法趨於黑暗，改描繪不明人物或者長相怪異缺乏臉部特徵。於人物描繪作品中具代表性的作品有艾雪與妻子的《天長地久不分離》。

### 風景

## 设计作品

Image:Waterfall.svg|Waterfall, 1961
不可能的瀑布 Image:Onm kubus2.png|內克方塊,艾雪版畫作品中常用的素材 Image:C. Conde de
Romanones 14 (Madrid) 03.jpg| Image:MCEscherParvusz.jpg|《天長地久不分離》中艾雪自畫像
Image:Universiteit Twente Mesa Plus Escher Object.jpg|小行星
Image:Metamorphose Escher (cropped).jpg|《變形2》局部

## 外部連結

  - [官方網站](http://www.mcescher.com/)
  - 使用[樂高來模仿埃舍尔的作品](../Page/樂高.md "wikilink")*[Relativity](http://multigraphic.dk/lounge/weblog/weblog.php?id=P352)*、*[Ascending
    and
    Descending](http://news.scotsman.com/topics.cfm?tid=632&id=1278982003)*
  - [數學畫　石厚高](http://w3.math.sinica.edu.tw/math_media/d222/22207.pdf)（《[數學傳播](../Page/數學傳播.md "wikilink")》）

[Category:荷兰画家](../Category/荷兰画家.md "wikilink")
[Category:呂伐登人](../Category/呂伐登人.md "wikilink")