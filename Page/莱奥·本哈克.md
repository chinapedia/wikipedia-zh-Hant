**莱奥·本哈克**（**Leo
Beenhakker**，）生於[荷蘭的](../Page/荷蘭.md "wikilink")[鹿特丹](../Page/鹿特丹.md "wikilink")，是國際著名的足球教練。任教的著名球隊包括[阿積士](../Page/阿積士.md "wikilink")、[飛燕諾](../Page/飛燕諾.md "wikilink")、[皇家馬德里及](../Page/皇家马德里.md "wikilink")[薩拉戈薩等](../Page/皇家薩拉戈薩.md "wikilink")，亦曾出任[沙烏地阿拉伯和](../Page/沙烏地阿拉伯國家足球隊.md "wikilink")[荷蘭](../Page/荷蘭國家足球隊.md "wikilink")（兩度）的國家隊主教練，在執教[千里達时](../Page/千里達及托巴哥國家足球隊.md "wikilink")，成功帶領球隊首次晉級[2006年世界盃決賽周](../Page/2006年世界盃.md "wikilink")，其後成為[波兰首位外籍領隊及帶隊首次晉級](../Page/波兰国家足球队.md "wikilink")[2008年歐國盃決賽周](../Page/2008年欧洲足球锦标赛.md "wikilink")。比赫加與西班牙足球結下不解之緣而被戲稱為“唐·李奧”（*Don
Leo*）。

比赫加精通[荷蘭語](../Page/荷蘭語.md "wikilink")、[英語及](../Page/英語.md "wikilink")[西班牙語](../Page/西班牙語.md "wikilink")，在任教[波蘭時](../Page/波蘭國家足球隊.md "wikilink")，亦學習[波蘭語](../Page/波蘭語.md "wikilink")。比赫加嗜好吸食[雪茄](../Page/雪茄.md "wikilink")，其名句：‘我已逐漸厭倦它……’（*I'm
getting so tired of this...*）廣為人知。

## 領隊

比赫加在很年青時已出任教練，經驗豐富，喜歡四處漂泊，執教球隊遍及全球。2000至2003年期間出任阿積士的技術總監時<small>\[1\]</small>，因球隊戰績下滑而辭退[阿德里安塞](../Page/阿德里安塞.md "wikilink")（Co
Adriaanse），禮聘[罗纳德·科曼](../Page/罗纳德·科曼.md "wikilink")（Ronald Koeman）接任。

比赫加曾帶領[荷蘭](../Page/荷蘭國家足球隊.md "wikilink")、[千里達及](../Page/千里達及托巴哥國家足球隊.md "wikilink")[波蘭進軍大賽決賽周](../Page/波蘭國家足球隊.md "wikilink")，但未曾取得一場勝仗，包括荷蘭在[1990年世界盃](../Page/1990年世界盃足球賽.md "wikilink")3場分組賽全部賽和，16強淘汰賽1-2負於[德國](../Page/德國國家足球隊.md "wikilink")；千里達在[2006年世界盃](../Page/2006年世界盃足球賽.md "wikilink")3場分組賽1和2負而被德國淘汰出局；波蘭在[2008年歐國盃](../Page/2008年欧洲足球锦标赛.md "wikilink")3場分組賽同樣錄得1和2負提早出局。

2009年9月10日於[波蘭以](../Page/波蘭國家足球隊.md "wikilink")0-3負於[斯洛文尼亞後](../Page/斯洛文尼亞國家足球隊.md "wikilink")，出線[2010年世界盃決賽週的形勢危殆](../Page/2010年世界盃足球賽.md "wikilink")，在餘下兩場比賽必須全勝兼同組賽果有利才有機會，比赫加被辭退<small>\[2\]</small>。

## 參考資料

[Category:荷蘭足球主教練](../Category/荷蘭足球主教練.md "wikilink")
[Category:飛燕諾主教練](../Category/飛燕諾主教練.md "wikilink")
[Category:阿積士主教練](../Category/阿積士主教練.md "wikilink")
[Category:薩拉戈薩主教練](../Category/薩拉戈薩主教練.md "wikilink")
[Category:皇家馬德里主教練](../Category/皇家馬德里主教練.md "wikilink")
[Category:荷蘭國家足球隊主教練](../Category/荷蘭國家足球隊主教練.md "wikilink")
[Category:千里達及托巴哥國家足球隊主教練](../Category/千里達及托巴哥國家足球隊主教練.md "wikilink")
[Category:波蘭國家足球隊主教練](../Category/波蘭國家足球隊主教練.md "wikilink")
[Category:禾寧丹主教練](../Category/禾寧丹主教練.md "wikilink")
[Category:蘇黎世草蜢主教練](../Category/蘇黎世草蜢主教練.md "wikilink")
[Category:亞美利加主教練](../Category/亞美利加主教練.md "wikilink")
[Category:瓜達拉哈拉主教練](../Category/瓜達拉哈拉主教練.md "wikilink")
[Category:維迪斯主教練](../Category/維迪斯主教練.md "wikilink")
[Category:迪加史卓普主教練](../Category/迪加史卓普主教練.md "wikilink")
[Category:1990年世界盃足球賽主教練](../Category/1990年世界盃足球賽主教練.md "wikilink")
[Category:2006年世界盃足球賽主教練](../Category/2006年世界盃足球賽主教練.md "wikilink")
[Category:2008年歐洲國家盃主教練](../Category/2008年歐洲國家盃主教練.md "wikilink")
[Category:西甲主教練](../Category/西甲主教練.md "wikilink")
[Category:荷甲主教練](../Category/荷甲主教練.md "wikilink")

1.
2.