[Leopold_Auer_Project_Gutenberg.jpg](https://zh.wikipedia.org/wiki/File:Leopold_Auer_Project_Gutenberg.jpg "fig:Leopold_Auer_Project_Gutenberg.jpg")
**莱奥波德·奥尔**（，[匈牙利語原名](../Page/匈牙利語.md "wikilink")：，），[匈牙利](../Page/匈牙利.md "wikilink")[小提琴演奏家](../Page/小提琴.md "wikilink")、作曲家、音乐教师。

## 生平

奥尔诞生在[匈牙利的](../Page/匈牙利.md "wikilink")[维斯普雷姆](../Page/维斯普雷姆.md "wikilink")，在[布达佩斯以及](../Page/布达佩斯.md "wikilink")[维也纳的音乐学院学习小提琴演奏](../Page/维也纳.md "wikilink")，后在[德国](../Page/德国.md "wikilink")[汉诺威师从小提琴演奏家](../Page/汉诺威.md "wikilink")[约瑟夫·约阿西姆](../Page/约瑟夫·约阿西姆.md "wikilink")（Joseph
Joachim）。1868年到1917年间，奥尔在[俄国](../Page/俄国.md "wikilink")[圣彼得堡音乐学院执教](../Page/圣彼得堡.md "wikilink")。1918年俄国[十月革命之后](../Page/十月革命.md "wikilink")，他移居[美国](../Page/美国.md "wikilink")，在[费城](../Page/费城.md "wikilink")[科蒂斯音乐学院执教](../Page/科蒂斯音乐学院.md "wikilink")。

奥尔被人们称为全球最著名的小提琴演奏家之一。他培养了数位后来名声大噪的音乐家，如[埃弗伦·津巴利斯特](../Page/埃弗伦·津巴利斯特.md "wikilink")（Efrem
Zimbalist），[米夏·艾尔曼](../Page/米夏·艾尔曼.md "wikilink")（Mischa
Elman），[雅沙·海飞兹](../Page/雅沙·海飞兹.md "wikilink")，[内森·米尔斯坦等人](../Page/内森·米尔斯坦.md "wikilink")。奥尔在收到[柴科夫斯基所作的協奏曲後](../Page/柴科夫斯基.md "wikilink")，认为這協奏曲是無法被演奏。許多的作曲家为他写作了许多小提琴的曲目。數年過後，奥尔改寫了柴可夫斯基協奏曲，將樂譜修飾成為他認為可以被演奏的方式後，才將這柴可夫斯基的作品列入他的擅長曲目中。奧尔所擁有的史特拉底瓦里琴，今日是由[約翰–傑克·康投侯福Jean](../Page/約翰–傑克·康投侯福.md "wikilink")-Jacques
Kantorow使用。

此外，奥尔也写作了一部分小提琴的曲谱，包括第二号匈牙利[狂想曲](../Page/狂想曲.md "wikilink")（Rhapsodie
honroise）。他也为[贝多芬和](../Page/贝多芬.md "wikilink")[勃拉姆斯的小提琴协奏曲写作了一些](../Page/勃拉姆斯.md "wikilink")[装饰乐段](../Page/装饰乐段.md "wikilink")。另外，他写作了三部关于小提琴演奏的著作，分别是《我的小提琴演奏教学法》（Violin
Playing as I Teach it，1920年出版）、《我漫长的音乐生涯》（My Long Life in
Music，1923年出版）)《小提琴经典作品的演奏解释》。

奥尔在[德国](../Page/德国.md "wikilink")[德累斯顿的郊区洛什维兹](../Page/德累斯顿.md "wikilink")（Loschwitz）逝世，葬于美国[纽约哈茨戴尔镇](../Page/纽约.md "wikilink")（Hartsdale）[芬克里夫墓園](../Page/芬克里夫墓園.md "wikilink")（Ferncliff
Cemetery）。

[爵士乐的](../Page/爵士乐.md "wikilink")[颤音演奏家](../Page/顫音_\(樂器技巧\).md "wikilink")[维拉·奥尔](../Page/维拉·奥尔.md "wikilink")，是他的侄女。演员[奥尔·米夏是他的孙子](../Page/奥尔·米夏.md "wikilink")。

## 著作

  - 《我的小提琴演奏教学法》（匈）奥厄 著，[司徒华诚](../Page/司徒华诚.md "wikilink")
    译，[人民音乐出版社](../Page/人民音乐出版社.md "wikilink")，1980
  - Violin Playing as I Teach it, Duckworth & Co London 1922
  - My Long Life in Music
  - 《小提琴经典作品的演奏解释》

## 参考资料

[Category:匈牙利小提琴家](../Category/匈牙利小提琴家.md "wikilink")
[Category:匈牙利音樂家](../Category/匈牙利音樂家.md "wikilink")
[Category:葬于芬克里夫墓园](../Category/葬于芬克里夫墓园.md "wikilink")