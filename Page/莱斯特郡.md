**萊斯特郡**（，英文簡稱：Leics，[讀音](../Page/國際音標.md "wikilink")：），[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[東米德蘭茲的](../Page/東米德蘭茲.md "wikilink")[郡](../Page/英格蘭的郡.md "wikilink")。2002年，保護植物慈善机构[植物生活](../Page/植物生活.md "wikilink")（Plantlife）选了[毛地黃](../Page/毛地黃.md "wikilink")（Foxglove）作為萊斯特郡郡花。[格倫菲爾德是行政總部](../Page/格倫菲爾德_\(萊斯特郡\).md "wikilink")。以人口計算，[萊斯特是第](../Page/萊斯特.md "wikilink")1大（亦是唯一一個）[城市](../Page/英國的城市地位.md "wikilink")、第1大[自治市鎮](../Page/英國的自治市鎮.md "wikilink")（Borough）；[查恩伍德是第](../Page/查恩伍德區.md "wikilink")2大自治市鎮；[拉夫伯勒是第](../Page/拉夫伯勒.md "wikilink")1大鎮（Town），[欣克利是第](../Page/欣克利.md "wikilink")2大鎮。

萊斯特郡是34個[非都市郡之一](../Page/非都市郡.md "wikilink")，實際管轄7個[非都市區](../Page/非都市區.md "wikilink")，[佔地](../Page/面積.md "wikilink")2,083[平方公里](../Page/平方公里.md "wikilink")（[第26](../Page/英格蘭的非都市郡列表_\(以面積排列\).md "wikilink")），有635,000[人口](../Page/人口.md "wikilink")（[第17](../Page/英格蘭的非都市郡列表_\(以人口排列\).md "wikilink")）；如看待成48個[名譽郡之一](../Page/名譽郡.md "wikilink")，它名義上包含多1個[單一管理區](../Page/單一管理區.md "wikilink")─[萊斯特](../Page/萊斯特.md "wikilink")，[佔地增至](../Page/面積.md "wikilink")2,156平方公里（[第28](../Page/英格蘭的名譽郡列表_\(以面積排列\).md "wikilink")），人口增至924,700（[第21](../Page/英格蘭的名譽郡列表_\(以人口排列\).md "wikilink")）。

## 行政區劃

[ 1. [查恩伍德](../Page/查恩伍德區.md "wikilink")
2\. [梅爾頓](../Page/梅爾頓.md "wikilink")
3\. [哈伯勒](../Page/哈伯勒.md "wikilink")
4\. [奧德比-威格斯頓](../Page/奧德比-威格斯頓.md "wikilink")
5\. [布萊比](../Page/布萊比區.md "wikilink")
6\. [欣克利-博斯沃思](../Page/欣克利-博斯沃思.md "wikilink")
7\. [萊斯特郡西北](../Page/萊斯特郡西北.md "wikilink")
8\.
[萊斯特](../Page/萊斯特.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
](https://zh.wikipedia.org/wiki/File:Leicestershire_Ceremonial_Numbered.png "fig: 1. 查恩伍德 2. 梅爾頓 3. 哈伯勒 4. 奧德比-威格斯頓 5. 布萊比 6. 欣克利-博斯沃思 7. 萊斯特郡西北 8. 萊斯特（單一管理區） ")
萊斯特郡是[非都市郡](../Page/非都市郡.md "wikilink")，實際管轄7個[非都市區](../Page/非都市區.md "wikilink")：[查恩伍德](../Page/查恩伍德區.md "wikilink")（Charnwood）、[梅爾頓](../Page/梅爾頓.md "wikilink")（Melton）、[哈伯勒](../Page/哈伯勒.md "wikilink")（Harborough）、[布萊比](../Page/布萊比區.md "wikilink")（Blaby）、[欣克利-博斯沃思](../Page/欣克利-博斯沃思.md "wikilink")（Hinckley
and Bosworth）、[萊斯特郡西北](../Page/萊斯特郡西北.md "wikilink")（North West
Leicestershire）、[奧德比-威格斯頓](../Page/奧德比-威格斯頓.md "wikilink")（Oadby and
Wigston）；如看待成[名譽郡](../Page/名譽郡.md "wikilink")，它名義上包含多1個[單一管理區](../Page/單一管理區.md "wikilink")：[萊斯特](../Page/萊斯特.md "wikilink")。

## 地理

下圖顯示英格蘭48個名譽郡的分佈情況。萊斯特郡東北與[林肯郡相鄰](../Page/林肯郡.md "wikilink")，東與[拉特蘭相鄰](../Page/拉特蘭.md "wikilink")，東南與[北安普敦郡相鄰](../Page/北安普敦郡.md "wikilink")，西南與[沃里克郡相鄰](../Page/沃里克郡.md "wikilink")，西與[斯塔福德郡相鄰](../Page/斯塔福德郡.md "wikilink")，西北與[打比郡相鄰](../Page/打比郡.md "wikilink")，北與[諾丁漢郡相鄰](../Page/諾丁漢郡.md "wikilink")。

## 教育

### 大学

  - [拉夫堡大学](../Page/拉夫堡大学.md "wikilink")
  - [莱斯特大学](../Page/莱斯特大学.md "wikilink")
  - [德蒙福特大学](../Page/德蒙福特大学.md "wikilink")

## 交通

  - [东密德兰机场](../Page/东密德兰机场.md "wikilink")

在萊斯特郡與隔鄰的[諾丁漢郡之間存在一條](../Page/諾丁漢郡.md "wikilink")[史蹟鐵路](../Page/史蹟鐵路.md "wikilink")（Heritage
railway）──[大中央鐵路](../Page/大中央鐵路.md "wikilink")（Great Central
Railway，GCR）──今日的GCR仍然使用老舊的[蒸汽机关车載運遊客](../Page/蒸汽机关车.md "wikilink")，是英國境內唯一一條擁有雙線配置的[標準軌史蹟鐵路](../Page/標準軌.md "wikilink")，也是郡內的旅遊名勝之一。

## 註釋

[Category:英格蘭的郡](../Category/英格蘭的郡.md "wikilink")
[Category:非都市郡](../Category/非都市郡.md "wikilink")