[Leon_Battista_Alberti.jpg](https://zh.wikipedia.org/wiki/File:Leon_Battista_Alberti.jpg "fig:Leon_Battista_Alberti.jpg")[烏菲茲美術館的莱昂](../Page/烏菲茲美術館.md "wikilink")·巴蒂斯塔·阿尔伯蒂像。\]\]
**莱昂·巴蒂斯塔·阿尔伯蒂**（，）是[文艺复兴时期在](../Page/文艺复兴时期.md "wikilink")[意大利的](../Page/意大利.md "wikilink")[建筑师](../Page/建筑师.md "wikilink")、[建筑理论家](../Page/建筑理论家.md "wikilink")、[作家](../Page/作家.md "wikilink")、[詩人](../Page/詩人.md "wikilink")、[哲學家](../Page/哲學家.md "wikilink")、[密碼學家](../Page/密碼學.md "wikilink")，是當時的一位[通才](../Page/通才.md "wikilink")。他被譽為是真正作爲復興時期的代表建築師，將文藝復興建築的營造提高到理論高度。他著有《[论建筑](../Page/论建筑.md "wikilink")》，於1485年出版，是當時第一部完整的建筑理论著作，並因應[谷騰堡的印刷術的幫助下](../Page/谷騰堡.md "wikilink")，推动了文艺复兴运动的发展。阿尔伯蒂仿古建筑设计手法严谨纯正，他在《論建築》這本書裡面體現了從文藝復興人文主義者地角度討論了建築的可能性，並提出應該根據[歐幾里得的數學原理](../Page/歐幾里得幾何.md "wikilink")，在圓形、方形等基本集合體制上進行合乎比例的重新組合，以找到建築中「[美的黃金分割](../Page/黃金分割.md "wikilink")」。他在建設[魯切拉宮的過程中使用了各種柱式](../Page/魯切拉宮.md "wikilink")，而這些柱子都擁有經過精心推敲的比例。而宮殿的頂部也是獨創的深出簷，甚至遮住了屋頂，使得建築的外觀保持完整的方形。

## 生平

### 童年與在學時期

萊昂本身是[佛羅倫斯一個富裕的商人Lorenzo](../Page/佛羅倫斯.md "wikilink")
Alberti與Bologna的一位寡婦在[熱那亞所生的兩位](../Page/熱那亞.md "wikilink")[私生子之一](../Page/私生子.md "wikilink")。由於他的私生子身份，他的一家被統治佛羅倫斯的阿爾伯蒂家族禁止在佛羅倫斯居住，而另一方面，他的母親亦於瘟疫中死去，所以萊昂在早年與父親只好投靠在[威尼斯辦銀行的叔叔](../Page/威尼斯.md "wikilink")。他的父親在他4歲時的1408年再婚，而在1428年時，家族的禁令撤銷，使他們一家可以回到佛羅倫斯定居。

## 外部連結

  - Online resources for Alberti's buildings
      - [S. Andrea, Mantua,
        Italy](http://www.greatbuildings.com/buildings/S._Andrea.html)
      - [Sta. Maria Novella, Florence,
        Italy](http://www.greatbuildings.com/buildings/S._Andrea.html)

[Category:意大利建築師](../Category/意大利建築師.md "wikilink")
[Category:意大利語言學家](../Category/意大利語言學家.md "wikilink")
[Category:意大利音樂家](../Category/意大利音樂家.md "wikilink")
[Category:意大利畫家](../Category/意大利畫家.md "wikilink")
[Category:意大利哲學家](../Category/意大利哲學家.md "wikilink")
[Category:意大利詩人](../Category/意大利詩人.md "wikilink")
[Category:意大利文藝復興作家](../Category/意大利文藝復興作家.md "wikilink")
[Category:意大利雕塑家](../Category/意大利雕塑家.md "wikilink")
[Category:熱那亞人](../Category/熱那亞人.md "wikilink")
[Category:15世紀哲學家](../Category/15世紀哲學家.md "wikilink")
[Category:博洛尼亞大學校友](../Category/博洛尼亞大學校友.md "wikilink")