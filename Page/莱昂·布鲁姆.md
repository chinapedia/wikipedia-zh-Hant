**安德烈·莱昂·布鲁姆**（，，），為法國政壇溫和左派的代表人物和三任法國總理。布魯姆是法國社會黨領袖[尚·饒勒斯的徒弟並在](../Page/尚·饒勒斯.md "wikilink")1914年繼承他的志業。他在1936～1937年當上人民陣線聯合政府的領袖，成為法國第一位[社會黨籍](../Page/社會黨_\(法國\).md "wikilink")（也是第一位猶太人）總理。由於堅定反對親德的維琪政權而遭到逮捕，被監禁到1945年才獲釋。戰後成為法國主要的元老政治家之一。

## 早年

布魯姆在1872年出生於巴黎一個富裕的同化[猶太人家庭](../Page/猶太人.md "wikilink")，他的父親亞伯拉罕是位來自亞爾薩斯的商人。布魯姆就讀[巴黎高等師範學校和](../Page/巴黎高等師範學校.md "wikilink")[巴黎大學](../Page/巴黎大學.md "wikilink")，畢業後同時成為了律師和文學評論家。

## 進入政壇

原本布魯姆對政治毫無興趣，直到1894年[德雷福斯事件對其造成極大的影響](../Page/德雷福斯事件.md "wikilink")，如同其他法國猶太人一樣受到社會的敵意和攻擊。作為德雷福斯派的成員使布魯姆接觸到當時社會黨的領袖饒勒斯並對其產生欽佩感，促使他加入了社會黨(當時稱為[工人國際法國支部](../Page/工人國際法國支部.md "wikilink"))和黨報[人道報](../Page/人道報.md "wikilink")，之後他成為黨內的主要理論家。

1914年[第一次世界大戰爆發時饒勒斯遭到激進民族主義者刺殺](../Page/第一次世界大戰.md "wikilink")，布魯姆因此開始在社會黨領導階層中嶄露頭角，1919年當選巴黎地區的代表進入[國民議會](../Page/國民議會_\(法國\).md "wikilink")。他始終相信一個「好的專制統治」根本不存在，所以布魯姆反對社會黨加入[共產國際](../Page/共產國際.md "wikilink")，1920年[俄國革命時他曾嘗試阻止黨內的分裂](../Page/俄國革命.md "wikilink")，但最終激進派還是帶著人道報等組織分裂出去另組[法國共產黨](../Page/法國共產黨.md "wikilink")。

布魯姆繼續領導社會黨度過20和30年代，同時也是黨報人民報的總編輯。
[Léon_Blum_reading.jpg](https://zh.wikipedia.org/wiki/File:Léon_Blum_reading.jpg "fig:Léon_Blum_reading.jpg")

[Category:法國總理](../Category/法國總理.md "wikilink")
[Category:法國社會黨黨員](../Category/法國社會黨黨員.md "wikilink")
[Category:法国作家](../Category/法国作家.md "wikilink")
[Category:法国犹太人](../Category/法国犹太人.md "wikilink")
[Category:巴黎大學校友](../Category/巴黎大學校友.md "wikilink")
[Category:巴黎高等師範學院校友](../Category/巴黎高等師範學院校友.md "wikilink")
[Category:亨利四世中学校友](../Category/亨利四世中学校友.md "wikilink")