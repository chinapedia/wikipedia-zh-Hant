**菊地直哉**（），[日本運動足球員](../Page/日本.md "wikilink")，曾効力日本J1級聯賽球會[新潟天鵝及](../Page/新潟天鵝.md "wikilink")[磐田喜悦](../Page/磐田喜悦.md "wikilink")，目前效力於[鳥栖砂岩](../Page/鳥栖砂岩.md "wikilink")。

## 來歷

在小學一年級的時候已經學習踢球。在高校畢業校曾直接到尋找日本國外球隊加入，不過最終被磐田喜悦簽下合約，最初數年表現活躍，曾代表日本參加2004年雅典奧運。在2005年由於他出場機會減少，他被借用到新潟天鵝一年，其出色的表現，終於在2006年再次成為磐田喜悦隊主力。

## 買春事件

事件起因於2007年5月29日，菊地直哉與[濱松市某高校一名](../Page/濱松市.md "wikilink")15歲的女學生於車上發生了不合法性關係，事後欲以10000日圓作為補償，女方不允下最後帶同菊地直哉的錢包到警署報案，菊地直哉亦因此而被逮捕。

[Category:磐田喜悦球员](../Category/磐田喜悦球员.md "wikilink")
[Category:日本足球運動員](../Category/日本足球運動員.md "wikilink")
[Category:日本旅外足球運動員](../Category/日本旅外足球運動員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會足球運動員](../Category/2004年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:日本奧運足球運動員](../Category/日本奧運足球運動員.md "wikilink")
[Category:新潟天鵝球員](../Category/新潟天鵝球員.md "wikilink")
[Category:卡爾斯耶拿球員](../Category/卡爾斯耶拿球員.md "wikilink")
[Category:大分三神球員](../Category/大分三神球員.md "wikilink")
[Category:鸟栖砂岩球员](../Category/鸟栖砂岩球员.md "wikilink")
[Category:北海道札幌岡薩多球員](../Category/北海道札幌岡薩多球員.md "wikilink")
[Category:靜岡市出身人物](../Category/靜岡市出身人物.md "wikilink")