《**菊次郎之夏**》（[日語](../Page/日語.md "wikilink")：<span lang=ja>**菊次郎の夏**、きくじろうのなつ</span lang=ja>），[日本](../Page/日本.md "wikilink")[电影](../Page/电影.md "wikilink")，于1999年6月5日公映，由[北野武自編自導自演](../Page/北野武.md "wikilink")，[久石讓編曲](../Page/久石讓.md "wikilink")，第52届[戛纳电影节竞赛作品](../Page/戛纳电影节.md "wikilink")。

該劇外景取自[滨松](../Page/滨松.md "wikilink")、[名古屋等城市](../Page/名古屋.md "wikilink")；「菊次郎」是北野武已去世之父亲名字。

## 情节

故事講述小學三年級學生的正男（關口雄介飾）尋覓其離異的母親的故事，在與鄰居菊次郎（北野武飾）一同尋覓的途中，兩人產生微妙的關係，而正男亦因此得到「天使之鈴」而走出陰霾；本片有別於北野武的其他作品，清新、搞笑而不落俗套，帶著淡淡的情感。

## 角色

  - 菊次郎：[北野武](../Page/北野武.md "wikilink")
  - 菊次郎的老婆：[岸本加世子](../Page/岸本加世子.md "wikilink")
  - 正男：[關口雄介](../Page/關口雄介.md "wikilink")
  - 正男的母親：[大家由佑子](../Page/大家由佑子.md "wikilink")
  - 正男的奶奶：[吉行和子](../Page/吉行和子.md "wikilink")
  - 車之女：[細川典江](../Page/細川典江.md "wikilink")
  - 變態老頭（落武者）：[磨赤兒](../Page/磨赤兒.md "wikilink")
  - 騎摩托車的男人（胖叔叔）：[谷雷特義太夫](../Page/谷雷特義太夫.md "wikilink")
  - 騎摩托車的男人（光頭叔叔）：[井手辣韭](../Page/井手辣韭.md "wikilink")
  - 小哥（溫柔叔叔）：[今村鼠](../Page/今村鼠.md "wikilink")
  - 黑道的幹部：[關根大學](../Page/關根大學.md "wikilink")
  - 黑道：[田中要次](../Page/田中要次.md "wikilink")、[蹈宮誠](../Page/蹈宮誠.md "wikilink")、[村澤壽彥](../Page/村澤壽彥.md "wikilink")
  - 公車站的男人：[彼得清](../Page/彼得清.md "wikilink")
  - 的屋：[諏訪太郎](../Page/諏訪太郎.md "wikilink")、[江端英久](../Page/江端英久.md "wikilink")
  - 老闆娘：[小島可奈子](../Page/小島可奈子.md "wikilink")、[永田杏子](../Page/永田杏子.md "wikilink")、[小林惠美](../Page/小林惠美.md "wikilink")、[大葉冬](../Page/大葉冬.md "wikilink")、[塚本友希](../Page/塚本友希.md "wikilink")、[安井祐子](../Page/安井祐子.md "wikilink")

## 工作人員

  - 製作：森昌行、吉田多喜男
  - 導演：[北野武](../Page/北野武.md "wikilink")
  - 編劇：[北野武](../Page/北野武.md "wikilink")
  - 音樂：[久石讓](../Page/久石讓.md "wikilink")
  - 攝影：柳島克己
  - 剪接：[北野武](../Page/北野武.md "wikilink")

## 奖项

  - [第23回日本電影學院獎最佳作曲](../Page/第23回日本電影學院獎.md "wikilink")（久石讓）、最佳女配角(岸本加世子)\[1\]

## 其他

根據[北野武本人的隨筆式回憶錄](../Page/北野武.md "wikilink")«[菊次郎與佐紀](../Page/菊次郎與佐紀.md "wikilink")»(中譯本ISBN:9789869577571)談及其父北野菊次郎：戰後，美軍進駐日本，日本人對碧眼金髮者既不熟悉又害怕，菊次郎也是同感。後來曾有美軍送給菊次郎巧克力，讓菊次郎對美國人大為改觀並頗有好感。此為北野武對父親與小時候的回憶。

## 外部連結

  - [官方網頁](https://web.archive.org/web/20060116084123/http://www.office-kitano.co.jp/contents/kikujiro/)

[Category:日本電影作品](../Category/日本電影作品.md "wikilink")
[Category:1999年电影](../Category/1999年电影.md "wikilink")
[Category:北野武電影](../Category/北野武電影.md "wikilink")
[Category:濱松市背景電影](../Category/濱松市背景電影.md "wikilink")
[Category:久石让配乐电影](../Category/久石让配乐电影.md "wikilink")
[Category:日語電影](../Category/日語電影.md "wikilink")
[Category:日本電影學院獎最佳女配角獲獎電影](../Category/日本電影學院獎最佳女配角獲獎電影.md "wikilink")

1.   第23回日本電影學院獎