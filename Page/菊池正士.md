**菊池正士**（），[日本核物理学家](../Page/日本.md "wikilink")，他最早发现了[电子显微学中出现的](../Page/电子显微学.md "wikilink")，并给出了正确的理论解释\[1\]。

## 简历

1902年生于[东京](../Page/东京.md "wikilink")。1926年毕业于[东京帝国大学理学部](../Page/东京帝国大学.md "wikilink")[物理学专业](../Page/物理学.md "wikilink")。1928年进入[理化学研究所开始](../Page/理化学研究所.md "wikilink")[电子束](../Page/电子.md "wikilink")[衍射方面的研究](../Page/衍射.md "wikilink")，并在世界上首次观察到[云母薄膜的完整电子衍射花样](../Page/云母.md "wikilink")，首次提出并解释了**菊池线**现象。1929年赴[德国留学](../Page/德国.md "wikilink")。1934年起任[大阪帝国大学教授](../Page/大阪帝国大学.md "wikilink")，主持建造了日本第二个考克饒夫－瓦耳頓型（）直流高压加速器（次於[臺北帝國大學物理學講座的](../Page/臺北帝國大學.md "wikilink")[荒勝文策教授](../Page/荒勝文策.md "wikilink")）\[2\]\[3\]。[二战结束后](../Page/二战.md "wikilink")，于1955年起任[东京大学原子核研究所首任所长](../Page/东京大学.md "wikilink")，并成功主持完成了可变能量[回旋加速器的建造](../Page/回旋加速器.md "wikilink")。1959年～1964年任[日本原子力研究所理事长](../Page/日本原子力研究所.md "wikilink")。1974年逝世\[4\]。

## 参考资料

<references/>

[Category:1902年出生](../Category/1902年出生.md "wikilink")
[Category:1974年逝世](../Category/1974年逝世.md "wikilink")
[Category:日本物理学家](../Category/日本物理学家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")
[Category:文化勳章獲得者](../Category/文化勳章獲得者.md "wikilink")

1.

2.

3.

4.