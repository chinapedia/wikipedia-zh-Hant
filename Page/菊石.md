**菊石**是一群已經[滅絕的](../Page/滅絕.md "wikilink")[海洋生物總稱](../Page/海洋.md "wikilink")，隸屬於**菊石亞綱**（[學名](../Page/學名.md "wikilink")：）。牠們約在[志留紀晚期至](../Page/志留紀.md "wikilink")[泥盆紀初期第一次出現在地球上](../Page/泥盆紀.md "wikilink")，最後與[恐龍](../Page/恐龍.md "wikilink")、[箭石等生物一起於](../Page/箭石.md "wikilink")[白堊紀晚期滅絕](../Page/白堊紀.md "wikilink")。由於菊石的演化速度很快，分布也很廣，非常適合作為[標準化石](../Page/標準化石.md "wikilink")，地質學家可以使用牠們來確定含有菊石[化石的地層的年代](../Page/化石.md "wikilink")。\[1\]菊石亞綱與現存的[頭足綱關係最接近的可能是](../Page/頭足綱.md "wikilink")[蛸亞綱](../Page/蛸亞綱.md "wikilink")，如[章魚](../Page/章魚.md "wikilink")、[烏賊和](../Page/烏賊.md "wikilink")[魷魚等](../Page/魷魚.md "wikilink")，而不是與其外觀相似的[鸚鵡螺亞綱](../Page/鸚鵡螺亞綱.md "wikilink")[鸚鵡螺目](../Page/鸚鵡螺目.md "wikilink")。

## 分類

生物學家根據化石的殼室的結構與外殼的花紋，目前將菊石亞綱分成3個[目](../Page/目_\(生物\).md "wikilink")：[菊石目](../Page/菊石目.md "wikilink")、[棱菊石目和](../Page/棱菊石目.md "wikilink")[齒菊石目](../Page/齒菊石目.md "wikilink")。其中菊石目分成四個已知的[亞目](../Page/亞目.md "wikilink")，分別為[菊石亞目](../Page/菊石亞目.md "wikilink")（下[侏羅紀至](../Page/侏羅紀.md "wikilink")[上白堊紀](../Page/上白堊紀.md "wikilink")）、[勾菊石亞目](../Page/勾菊石亞目.md "wikilink")（上侏羅紀至上白堊紀）、[葉菊石亞目](../Page/葉菊石亞目.md "wikilink")（下[三疊紀至上白堊紀](../Page/三疊紀.md "wikilink")）及[弛菊石亞目](../Page/弛菊石亞目.md "wikilink")（下侏羅紀至上白堊紀）。

在某些分類法中，菊石亞綱被分為8個目：

  - [菊石目](../Page/菊石目.md "wikilink") Ammonitida
  - [棱菊石目](../Page/棱菊石目.md "wikilink") Goniatitida
  - [齒菊石目](../Page/齒菊石目.md "wikilink") Ceratitida
  - [葉菊石目](../Page/葉菊石目.md "wikilink") Phylloceratida
  - [弛菊石目](../Page/弛菊石目.md "wikilink") Lytoceratida
  - [前碟菊石目](../Page/前碟菊石目.md "wikilink") Prolecanitida
  - [無角菊石目](../Page/無角菊石目.md "wikilink") Agoniatitida
    ，即[似古菊石目](../Page/似古菊石目.md "wikilink") Anarcestida
  - [海神石目](../Page/海神石目.md "wikilink") Clymeniida

## 外觀

大多數菊石的壳沿平面卷曲，呈盘状，两面对称，壳表面光滑或具细的生长线纹，有些具特殊的纹饰，如纵棱、横肋、瘤和刺等。牠們的體管（連接殼內各腔室的管）位於殼腹的外面。但也有些菊石的外殼擁有特殊的形狀，如殼形筆直的[桿菊石](../Page/桿菊石.md "wikilink")、塔狀螺旋的[塔菊石](../Page/塔菊石.md "wikilink")、不規則扭曲狀的[日本菊石等](../Page/日本菊石.md "wikilink")。[古生代菊石的骨縫是單一的](../Page/古生代.md "wikilink")，如[海神石等](../Page/海神石.md "wikilink")；[中生代](../Page/中生代.md "wikilink")[菊石目菊石的骨縫則是複合的](../Page/菊石目.md "wikilink")，如[弛菊石等](../Page/弛菊石.md "wikilink")。\[2\]由於牠們已經滅絕，因此我們對菊石的軟體和生活習性知道的並不多，估計牠們有很多觸手來捕捉獵物\[3\]，且可能會噴墨汁來逃避掠食，因為一些化石標本保留了這些墨汁\[4\]，但在目前保存的化石標本中很少發現有咬顎、[齒舌或](../Page/齒舌.md "wikilink")[墨囊等構造](../Page/墨囊.md "wikilink")。\[5\]

## 生態

菊石一般漂浮在海水上层，下面经常是极其缺氧区域，没有生物。菊石死后，沉到海底，逐渐埋没。细菌分解遗体时候，把附近水中性质变化，降低[矿物质](../Page/矿物质.md "wikilink")[溶解度](../Page/溶解度.md "wikilink")，尤其是[磷酸鹽和](../Page/磷酸鹽.md "wikilink")[碳酸盐](../Page/碳酸盐.md "wikilink")。

## 化石

菊石化石上面有一圈一圈的[矿物质](../Page/矿物质.md "wikilink")，因此保存很多高质标本。在[歐洲的](../Page/歐洲.md "wikilink")[中世紀時期](../Page/中世紀.md "wikilink")，某些菊石的外殼化石被人們視為盤曲、石化的無頭[蛇](../Page/蛇.md "wikilink")，稱為**蛇石**，例如[指菊石等](../Page/指菊石.md "wikilink")，商人們會在菊石的開口刻上蛇的[頭後賣給旅者](../Page/頭.md "wikilink")。\[6\]

## 參考文獻

## 外部連結

  - [菊石化石](https://web.archive.org/web/20170306235239/http://y2u.co.uk/%26002_Images/Lyme%20Regis%20Fossils%20%2001.htm)
  - [Cephalopod Fossil Articles from
    TONMO.com](http://www.tonmo.com/science/fossils/fossilsjump.php)
  - [Photos of Ammonites at Lyme Regis,
    UK](https://web.archive.org/web/20170306235239/http://y2u.co.uk/%26002_Images/Lyme%20Regis%20Fossils%20%2001.htm)
  - [Paleozoic.org: Gallery of ammonite
    photographs](https://web.archive.org/web/20060404024923/http://paleozoic.org/gallery.htm)
  - [Descriptions and pictures of ammonite
    fossils](http://www.fossilmuseum.net/Fossil_Galleries/Ammonites.htm)

[菊石亞綱](../Page/分類:菊石亞綱.md "wikilink")
[分類:古生代動物](../Page/分類:古生代動物.md "wikilink")
[分類:中生代生物](../Page/分類:中生代生物.md "wikilink")

[cs:Amonit](../Page/cs:Amonit.md "wikilink")

1.

2.
3.

4.

5.
6.