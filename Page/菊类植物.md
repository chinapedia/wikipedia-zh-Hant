**菊类植物**（[学名](../Page/学名.md "wikilink")：）是在[APG
分类法](../Page/APG_分类法.md "wikilink")、[APG
II分类法](../Page/APG_II分类法.md "wikilink")、[APG
III分类法及](../Page/APG_III分类法.md "wikilink")[APG
IV分类法所确立的](../Page/APG_IV分类法.md "wikilink")[被子植物分支之一](../Page/被子植物.md "wikilink")，是具有亲缘关系的一个[植物类群](../Page/植物.md "wikilink")，和[蔷薇分支一起组成](../Page/蔷薇分支.md "wikilink")[真双子叶植物分支中的主要部分](../Page/真双子叶植物分支.md "wikilink")。

菊分支中的植物[种类主要包括](../Page/种.md "wikilink")1981年[克朗奎斯特分类法中分到](../Page/克朗奎斯特分类法.md "wikilink")[菊亚纲的种类以及一些合瓣](../Page/菊亚纲.md "wikilink")[花类植物](../Page/花.md "wikilink")。由于这种分类法更能正确地表示植物种类之间的亲缘关系，因而被多数的植物学家所采用。

菊分支包括：

  - '''菊分支 Asterids
      -
        [山茱萸目](../Page/山茱萸目.md "wikilink") Cornales
        [杜鹃花目](../Page/杜鹃花目.md "wikilink") Ericales
        '''[唇形分支](../Page/唇形分支.md "wikilink")
        Lamiids（[I类真菊分支](../Page/I类真菊分支.md "wikilink")
        Euasterids I）
          -
            [茶茱萸目](../Page/茶茱萸目.md "wikilink") Icacinales
            [水螅花目](../Page/水螅花目.md "wikilink") Metteniusales
            [丝缨花目](../Page/丝缨花目.md "wikilink") Garryales
            [龙胆目](../Page/龙胆目.md "wikilink") Gentianales
            [茄目](../Page/茄目.md "wikilink") Solanales
            [紫草目](../Page/紫草目.md "wikilink") Boraginales
            [黄漆姑目](../Page/黄漆姑目.md "wikilink") Vahliales
            [唇形目](../Page/唇形目.md "wikilink") Lamiales
        '''[桔梗分支](../Page/桔梗分支.md "wikilink")
        Campanulids（[II类真菊分支](../Page/II类真菊分支.md "wikilink")
        Euasterids II）
          -
            [冬青目](../Page/冬青目.md "wikilink") Aquifoliales
            [南鼠刺目](../Page/南鼠刺目.md "wikilink") Escalloniales
            [菊目](../Page/菊目.md "wikilink") Asterales
            [绒球花目](../Page/绒球花目.md "wikilink") Bruniales
            [伞形目](../Page/伞形目.md "wikilink") Apiales
            [盔被花目](../Page/盔被花目.md "wikilink") Paracryphiales
            [川续断目](../Page/川续断目.md "wikilink") Dipsacales

|                                                                                                      |        |                                                                                          |        |        |                    |                                             |                                             |                                                |                                             |                                             |                                             |          |                                         |                                           |                                                                              |                                             |                                           |                  |                                      |                                      |                                        |    |                                        |                                          |
| ---------------------------------------------------------------------------------------------------- | ------ | ---------------------------------------------------------------------------------------- | ------ | ------ | ------------------ | ------------------------------------------- | ------------------------------------------- | ---------------------------------------------- | ------------------------------------------- | ------------------------------------------- | ------------------------------------------- | -------- | --------------------------------------- | ----------------------------------------- | ---------------------------------------------------------------------------- | ------------------------------------------- | ----------------------------------------- | ---------------- | ------------------------------------ | ------------------------------------ | -------------------------------------- | -- | -------------------------------------- | ---------------------------------------- |
| {{分支|style=font-size:80%; line-height:90%|标1=[核心真双子叶植物](../Page/核心真双子叶植物.md "wikilink")              | 1={{分支 | 1={{分支|标1=[超菊类植物](../Page/超菊类植物.md "wikilink")|标2=[超蔷薇类植物](../Page/超蔷薇类植物.md "wikilink") | 1={{分支 | 1={{分支 | 1={{分支|标1=**菊类植物** | 1={{分支|2=[山茱萸目](../Page/山茱萸目.md "wikilink") | 1={{分支|2=[杜鹃花目](../Page/杜鹃花目.md "wikilink") | 1={{分支|标2=[唇形类植物](../Page/唇形类植物.md "wikilink") | 2={{分支|2=[茶茱萸目](../Page/茶茱萸目.md "wikilink") | 1={{分支|2=[水螅花目](../Page/水螅花目.md "wikilink") | 1={{分支|2=[丝缨花目](../Page/丝缨花目.md "wikilink") | 1=}}}}}} | 标1=[桔梗类植物](../Page/桔梗类植物.md "wikilink") | 1={{分支|2=[冬青目](../Page/冬青目.md "wikilink") | 1={{分支|3=[南鼠刺目](../Page/南鼠刺目.md "wikilink")|2=[菊目](../Page/菊目.md "wikilink") | 1={{分支|2=[绒球花目](../Page/绒球花目.md "wikilink") | 1={{分支|2=[伞形目](../Page/伞形目.md "wikilink") | 1=}}}}}}}}}}}}}} | 2=[石竹目](../Page/石竹目.md "wikilink")}} | 2=[檀香目](../Page/檀香目.md "wikilink")}} | 2=[红珊藤目](../Page/红珊藤目.md "wikilink")}} | 2= | 3=[五桠果目](../Page/五桠果目.md "wikilink")}} | 2=[大叶草目](../Page/大叶草目.md "wikilink")}}}} |
| 菊类植物与其它[核心真双子叶植物的亲缘关系](../Page/核心真双子叶植物.md "wikilink")（[APG IV分类法](../Page/APG_IV分类法.md "wikilink")） |        |                                                                                          |        |        |                    |                                             |                                             |                                                |                                             |                                             |                                             |          |                                         |                                           |                                                                              |                                             |                                           |                  |                                      |                                      |                                        |    |                                        |                                          |

## 参考文献

## 外部链接

  - [真双子叶植物分支](http://www.mobot.org/MOBOT/Research/APweb/orders/ranunculalesweb.htm#Eudicots)
  - [APG网站](http://www.mobot.org/MOBOT/research/APweb)

[category:植物學](../Page/category:植物學.md "wikilink")

[Category:生物分類學](../Category/生物分類學.md "wikilink")
[\*](../Category/菊分支.md "wikilink")