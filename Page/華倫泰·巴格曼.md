**華倫泰·巴格曼**（，），出生於[德國](../Page/德國.md "wikilink")[柏林的](../Page/柏林.md "wikilink")[物理學家與](../Page/物理學家.md "wikilink")[數學家](../Page/數學家.md "wikilink")。

## 生平

從1925到1933年，巴格曼就讀於[柏林大學](../Page/柏林大學.md "wikilink")。[希特勒奪得德國政權後](../Page/希特勒.md "wikilink")，巴格曼轉到[瑞士的](../Page/瑞士.md "wikilink")[蘇黎世大學求學](../Page/蘇黎世大學.md "wikilink")。在恩師[文策](../Page/文策.md "wikilink")（Gregor
Wentzel）地悉心教導下，他得到博士學位。

畢業以後，由於整個歐洲瀰漫著戰爭的氣息，他決定申請移民美國。很幸運地，在他的護照失效前兩天，終於得到[美國駐德大使館的](../Page/美國駐德國大使館.md "wikilink")[簽證](../Page/美國簽證.md "wikilink")。他立刻啟程前往美國。1937
年，到了美國後，很快地，他進入[普林斯頓高等研究院繼續研究工作](../Page/普林斯頓高等研究院.md "wikilink")。後來，又被聘請為[阿爾伯特·愛因斯坦的助理](../Page/阿爾伯特·愛因斯坦.md "wikilink")。因為巴格曼嚴謹的治學精神和解答問題的能力，他得到了一個「要求巴格曼」的綽號。休閒時候，巴格曼是一位很有天份的鋼琴演奏者，常常與愛因斯坦及同事們合奏[莫扎特和](../Page/莫扎特.md "wikilink")[巴赫的](../Page/巴赫.md "wikilink")[室內音樂](../Page/室內音樂.md "wikilink")。1941
年，他開始在[普林斯頓大學教書](../Page/普林斯頓大學.md "wikilink")，一直教到1976 年退休。因心臟病，1989
年逝世於普林斯頓。

巴格曼發現了SL<sub>2</sub>(R)的不可約表示（）與[勞侖茲群的不可約表示](../Page/勞侖茲群.md "wikilink")（）。

1978
年，巴格曼得到[維格納獎](../Page/維格納獎.md "wikilink")（），獎勵他在群論與基礎物理上優異的研究成果。與他一同得獎的是[尤金·維格納本人](../Page/尤金·維格納.md "wikilink")。這也是開始頒發[維格納獎的第一年](../Page/維格納獎.md "wikilink")。1988
年，[德國物理學會因為巴格曼在理論物理學的傑出貢獻](../Page/德國物理學會.md "wikilink")，授予他最高榮譽，[馬克斯·普朗克獎章](../Page/馬克斯·普朗克獎章.md "wikilink")。

## 參閱

[拉普拉斯-龍格-冷次向量](../Page/拉普拉斯-龍格-冷次向量.md "wikilink")

## 參考文獻

  - V. Bargmann
    [*勞侖茲群的不可約么正表現*](http://links.jstor.org/sici?sici=0003-486X%28194707%292%3A48%3A3%3C568%3AIUROTL%3E2.0.CO%3B2-Z)
    The Annals of Mathematics 2nd Ser., Vol. 48, No. 3 (Jul., 1947), pp.
    568-640
  - [美國國家科學院傳記回憶錄](http://www.nap.edu/readingroom/books/biomems/vbargmann.html)

[Category:20世紀數學家](../Category/20世紀數學家.md "wikilink")
[Category:馬克斯·普朗克獎章獲得者](../Category/馬克斯·普朗克獎章獲得者.md "wikilink")
[Category:德國數學家](../Category/德國數學家.md "wikilink")
[Category:美國數學家](../Category/美國數學家.md "wikilink")
[Category:德國物理學家](../Category/德國物理學家.md "wikilink")
[Category:美國物理學家](../Category/美國物理學家.md "wikilink")
[Category:普林斯頓大學教師](../Category/普林斯頓大學教師.md "wikilink")
[Category:苏黎世大学校友](../Category/苏黎世大学校友.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:柏林人](../Category/柏林人.md "wikilink")