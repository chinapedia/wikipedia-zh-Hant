[HK_SW_上環_Sheung_Wan_皇后大道_Queen's_Road_華僑永亨銀行_OCBC_Wing_Hang_Bank_Building_shop_Feb_2017_IX1_02.jpg](https://zh.wikipedia.org/wiki/File:HK_SW_上環_Sheung_Wan_皇后大道_Queen's_Road_華僑永亨銀行_OCBC_Wing_Hang_Bank_Building_shop_Feb_2017_IX1_02.jpg "fig:HK_SW_上環_Sheung_Wan_皇后大道_Queen's_Road_華僑永亨銀行_OCBC_Wing_Hang_Bank_Building_shop_Feb_2017_IX1_02.jpg")總行\]\]
[HK_SW_上環_Sheung_Wan_皇后大道_Queen's_Road_Central_Oct_2018_SSG_02_華僑永亨銀行_OCBC_Wing_Hang_Bank_Building_shop.jpg](https://zh.wikipedia.org/wiki/File:HK_SW_上環_Sheung_Wan_皇后大道_Queen's_Road_Central_Oct_2018_SSG_02_華僑永亨銀行_OCBC_Wing_Hang_Bank_Building_shop.jpg "fig:HK_SW_上環_Sheung_Wan_皇后大道_Queen's_Road_Central_Oct_2018_SSG_02_華僑永亨銀行_OCBC_Wing_Hang_Bank_Building_shop.jpg")總行內貌\]\]
[WingHangBank_logo.svg](https://zh.wikipedia.org/wiki/File:WingHangBank_logo.svg "fig:WingHangBank_logo.svg")
[HK_Wing_Lung_Bank_Building_Queen_s_Road_Central_161.JPG](https://zh.wikipedia.org/wiki/File:HK_Wing_Lung_Bank_Building_Queen_s_Road_Central_161.JPG "fig:HK_Wing_Lung_Bank_Building_Queen_s_Road_Central_161.JPG")[皇后大道](../Page/皇后大道.md "wikilink")161號，鄰近[中遠大廈及](../Page/中遠大廈.md "wikilink")[中環中心](../Page/中環中心.md "wikilink")。\]\]
**華僑永亨銀行有限公司**（港交所除牌時0302.HK），前稱**永亨銀行有限公司**，當時於[香港交易所上市的金融公司](../Page/香港交易所.md "wikilink")。主要業務是經營銀行及有關之財務服務。公司在[香港註冊](../Page/香港.md "wikilink")，主席為[馮鈺斌](../Page/馮鈺斌.md "wikilink")，唯一股東是新加坡[華僑銀行](../Page/華僑銀行.md "wikilink")（原先是[馮堯敬家族](../Page/馮堯敬.md "wikilink")21.58%及[紐約梅隆銀行](../Page/紐約梅隆銀行.md "wikilink")20.28%）。华侨永亨在香港有约45间分行，并在中国大陆设有5间分行和3间异地支行。

## 歷史

[SZ_Tour_Street_90412_永亨銀行_Wing_Hang_Bank.JPG](https://zh.wikipedia.org/wiki/File:SZ_Tour_Street_90412_永亨銀行_Wing_Hang_Bank.JPG "fig:SZ_Tour_Street_90412_永亨銀行_Wing_Hang_Bank.JPG")
永亨銀行原名**永亨銀號**，由原董事長馮堯敬先生於1937年在[廣州市](../Page/廣州市.md "wikilink")[西關](../Page/西關.md "wikilink")[十三行創立](../Page/十三行.md "wikilink")，最初是經營金銀找換業務。二次大戰後，永亨銀號在香港重整業務，1945年在香港重新建立，初期有員工19人。經過一段有系統發展之後，永亨銀行於1960年獲香港政府發給銀行牌照，並註冊成立為永亨銀行。。1979年，將總行拆卸重建為樓高廿層之總行大廈，作為擴展之用。

1973年1月，美國紐約歐文信託公司購入永亨銀行超過半數權益。1988年，歐文信託公司與美國紐約銀行集團（現紐約銀行梅隆集團）合併，成為全美第十大之銀行。

1991年，永亨再度进入大陆市场，在深圳成立代表处。1993年，深圳代表处升格为深圳分行。

1993年7月，永亨銀行在[香港聯交所上市](../Page/香港聯交所.md "wikilink")。

1997年，广州代表处成立。2000年，上海代表处成立，并於2005年经[中国人民银行批准升格为上海分行](../Page/中国人民银行.md "wikilink")，是[CEPA之後首批获准在上海开设分行的港资银行之一](../Page/CEPA.md "wikilink")。

2004年8月，永亨銀行正式完成併購原由孔氏家族及[日本](../Page/日本.md "wikilink")[第一勸業銀行](../Page/第一勸業銀行.md "wikilink")（即現在的[瑞穗銀行](../Page/瑞穗銀行.md "wikilink")）持有的[浙江第一銀行](../Page/浙江第一銀行.md "wikilink")。

2006年，廣州代表處升格為廣州分行，其宣傳口號就是「我們回來了」。總經理馮建明說：「無論廣州還是永亨均已今非昔比，我們將把在香港、澳門的經驗帶回來，重建廣州永亨」。

2007年1月，從美国银行及英之杰收購一家合營的分期付款購買及租賃融資企業——英利信用財務有限公司。

2007年6月1日，在深圳成立法人银行[永亨银行（中国）有限公司](../Page/永亨银行（中国）有限公司.md "wikilink")，是永亨银行有限公司全资拥有的附属银行，办公地点位于[罗湖区](../Page/罗湖.md "wikilink")[地王大厦](../Page/地王大厦.md "wikilink")。同年在廣州開設分行，

2012年12月，永亨銀行出售[華一銀行](../Page/華一銀行.md "wikilink")3.89%權益。

2014年4月1日新加坡[華僑銀行同意以每股](../Page/華僑銀行.md "wikilink")125港元，交易價相等於市賬率2.02倍，斥資最多387.12億港元全購永亨銀行。

2014年10月1日，永亨銀行更名為華僑永亨銀行。

2014年10月16日，華僑永亨銀行已向[港交所撤銷上市](../Page/港交所.md "wikilink")。

2016年5月，华侨银行（中国）有限公司吸收合并永亨银行（中国）有限公司，并更名为华侨永亨银行（中国）有限公司。\[1\].

2017年3月20日，[香港人壽原本股東](../Page/香港人壽.md "wikilink")[亞洲保險有限公司](../Page/亞洲保險有限公司.md "wikilink")、[創興銀行有限公司](../Page/創興銀行有限公司.md "wikilink")、華僑永亨銀行有限公司、[上海商業銀行有限公司和](../Page/上海商業銀行有限公司.md "wikilink")[永隆銀行有限公司股東以](../Page/永隆銀行有限公司.md "wikilink")71億港元，出售予神祕買家首元國際有限公司（中國先鋒集團持有），其主席[陳智文](../Page/陳智文.md "wikilink")（[陳有慶兒子](../Page/陳有慶.md "wikilink")，也是[陳智思親屬](../Page/陳智思.md "wikilink")）及總經理張立輝表示，出售後維持正常運作。而[高盛（亞洲）有限責任公司和](../Page/高盛（亞洲）有限責任公司.md "wikilink")[野村國際（香港）有限公司作為上述](../Page/野村國際（香港）有限公司.md "wikilink")5名股東聯席財務顧問\[2\]\[3\]\[4\]\[5\]\[6\]。

## 參考

## 參見

  - [澳門華僑永亨銀行](../Page/澳門華僑永亨銀行.md "wikilink")

## 外部連結

  - [ocbcwhhk.com](http://www.ocbcwhhk.com)
  - [華僑永亨銀行（中国）有限公司](http://www.whbcn.com)

[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:香港銀行](../Category/香港銀行.md "wikilink")
[Category:華資銀行](../Category/華資銀行.md "wikilink")
[Category:广州历史](../Category/广州历史.md "wikilink")
[Category:1937年成立的公司](../Category/1937年成立的公司.md "wikilink")

1.  [中国银监会关于华侨银行（中国）有限公司吸收合并永亨银行（中国）有限公司的批复
    中国银监会 2016-05-20](http://www.cbrc.gov.cn/chinese/home/docView/3F717E904EC44C3BBFF79C1B824AA1EE.html)
2.  [亞洲保險有限公司、創興銀行有限公司、華僑永亨銀行有限公司、上海商業銀行有限公司和永隆銀行有限公司聯合宣佈就出售香港人壽保險有限公司並建立新的銀保合作關係達成協議](http://www.hklife.com.hk/hklifeweb/web/about/about_script_detail.jsp?lang=chi&id=166)2017年3月20日，香港人壽保險有限公司
3.  [亞洲金融：料出售香港人壽最快明年完成](http://hk.apple.nextmedia.com/realtime/finance/20170323/56469764)2017年3月23日，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")
4.  [香港人壽買家為神秘富豪](http://hk.apple.nextmedia.com/financeestate/art/20170322/19966185)2017年3月22日，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")
5.  [香港人壽賣盤予首元國際
    作價達71億元](http://hk.apple.nextmedia.com/realtime/finance/20170320/56456504)2017年3月20日，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")
6.  [亞洲金融指出售香港人壽股權仍待保監會審批](http://www.metroradio.com.hk/news/default.aspx?NewsID=20170323180710)[新城廣播有限公司](../Page/新城廣播有限公司.md "wikilink")，2017年3月23日