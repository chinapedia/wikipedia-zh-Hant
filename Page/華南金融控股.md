**華南金融控股股份有限公司**（英語：Hua Nan Financial Holdings Co.,
Ltd.，簡稱：**華南金控**），是[臺灣於](../Page/臺灣.md "wikilink")《金融控股公司法》[三讀通過後第一家成立之](../Page/三讀通過.md "wikilink")[金融控股公司](../Page/金融控股公司.md "wikilink")。

## 歷史

1999年11月14日，[華南銀行](../Page/華南銀行.md "wikilink")（華銀）與[永昌綜合證券](../Page/永昌綜合證券.md "wikilink")（永昌證）雙方召開股東臨時會，通過共同以已發行股份全數轉換方式轉型為華南金控，是[三商銀中首家](../Page/三商銀.md "wikilink")[股票上市的金融控股公司](../Page/股票上市.md "wikilink")，並推選原華銀董事長[林明成為華南金控董事長](../Page/林明成.md "wikilink")，以同年11月19日為華南金控開業日暨股票掛牌上市轉換基準日，[資本額定為](../Page/資本額.md "wikilink")[新台幣一千億元](../Page/新台幣.md "wikilink")。日後將陸續增設結合華銀現有的180多個分行及永昌證的40個分點成立證券與銀行理財服務的據點。
[Hua_Nan_Bank_headquarters_20100912.jpg](https://zh.wikipedia.org/wiki/File:Hua_Nan_Bank_headquarters_20100912.jpg "fig:Hua_Nan_Bank_headquarters_20100912.jpg")

2001年12月26日，[中華民國財政部發出許可執照](../Page/中華民國財政部.md "wikilink")，華南金控正式成立。

2014年12月27日，華南金控總部與華南銀行總行由臺北市[中正區](../Page/中正區_\(臺北市\).md "wikilink")[重慶南路一段](../Page/重慶南路_\(臺北市\).md "wikilink")38號遷往[華銀總行大樓](../Page/華銀總行大樓.md "wikilink")。

2016年9月12日，前財政部次長[吳當傑接任華南金控董事長](../Page/吳當傑.md "wikilink")。

## 旗下子公司

  - [華南銀行](../Page/華南銀行.md "wikilink")
  - [華南永昌綜合證券](../Page/華南永昌綜合證券.md "wikilink")
  - [華南金資產管理](../Page/華南金資產管理.md "wikilink")
  - [華南金創業投資](../Page/華南金創業投資.md "wikilink")
  - [華南產物保險](../Page/華南產物保險.md "wikilink")
  - [華南永昌投信](../Page/華南永昌投信.md "wikilink")

## 参考文献

## 外部連結

  - [華南金控](http://www.hnfhc.com.tw/index.shtml)
  - [華南銀行](http://www.hncb.com.tw/)
  - [華南永昌綜合證券](https://web.archive.org/web/20070608210221/http://www.entrust.com.tw/index1024.asp)
  - [華南永昌投信](http://www.hnitc.com.tw/)
  - [華南產物保險](http://www.south-china.com.tw/)
  - [華南票券](https://web.archive.org/web/20070609214923/http://www.huananbills.com.tw/)
  - [華南金資產管理](http://www.hnamc.com.tw/)

[ja:華南銀行](../Page/ja:華南銀行.md "wikilink")

[H華](../Category/臺灣的金融控股公司.md "wikilink")
[2](../Category/臺灣證券交易所上市公司.md "wikilink")
[H華](../Category/總部位於臺北市信義區的工商業機構.md "wikilink")
[H華](../Category/2001年成立的公司.md "wikilink")
[Category:中華民國公營事業](../Category/中華民國公營事業.md "wikilink")