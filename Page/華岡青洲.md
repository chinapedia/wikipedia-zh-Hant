**華岡青洲**（），名**震**，字**伯行**，又名**雲平**，號**青洲**、**隨賢**。日本[江戶時代末期著名漢醫學者](../Page/江戶時代.md "wikilink")、[外科醫生](../Page/外科醫生.md "wikilink")，出生於[幕府末期](../Page/幕府.md "wikilink")[紀州](../Page/紀州.md "wikilink")（[和歌山縣](../Page/和歌山縣.md "wikilink")）的傑出人物。是全世界首位使用[全身麻醉](../Page/全身麻醉.md "wikilink")，成功完成[乳癌手術的外科醫師](../Page/乳癌.md "wikilink")。

## 生平

華岡青洲在1782年（天明二年）來到[京都](../Page/京都.md "wikilink")，跟隨[吉益南涯醫師學習古代的醫術](../Page/吉益南涯.md "wikilink")。另外又跟隨[大和見立研習](../Page/大和見立.md "wikilink")[蘭醫](../Page/蘭醫.md "wikilink")（荷蘭醫學）的外科醫術。1785年回鄉後，開始開業行醫。他的信條是「內外合一，活物窮理」。華岡見到[歐傷牛草有麻藥作用](../Page/歐傷牛草.md "wikilink")，並據[華陀](../Page/華陀.md "wikilink")[麻沸散的故典](../Page/麻沸散.md "wikilink")，便潛心研究，終於製造出叫做「[通仙散](../Page/通仙散.md "wikilink")」的麻醉劑，並且以其母及妻子作[人體試驗之後](../Page/人體試驗.md "wikilink")，才大膽地用在各種外科手術上，讓患者服用麻醉劑進行無痛手術，在當時的日本還是史無前例的。

尤其是1805年他成功地完成了乳癌手術，更是震驚世人。華岡青洲的麻醉劑比[摩頓以](../Page/摩頓.md "wikilink")[乙醚當麻醉劑早了四十年](../Page/乙醚.md "wikilink")。在乳癌手術之後，他也將麻醉劑運用在治療[惡瘡](../Page/惡瘡.md "wikilink")、[惡性腫瘤](../Page/惡性腫瘤.md "wikilink")、[痔瘡](../Page/痔瘡.md "wikilink")、[脫肛](../Page/脫肛.md "wikilink")、[下疳](../Page/下疳.md "wikilink")、[乳腺炎等各種外科手術上](../Page/乳腺炎.md "wikilink")。

全國各地湧來求醫的病患自四面八方聚集到他的診所來，加上要向他學習麻醉術的人竟多達1300人。他除了為人治病外，並培育了很多門生來擴大自己的外科學說。其中[本間玄調](../Page/本間玄調.md "wikilink")、[嫌田玄台](../Page/嫌田玄台.md "wikilink")、[熱田玄淹](../Page/熱田玄淹.md "wikilink")、[館玄淹](../Page/館玄淹.md "wikilink")、[難波立愿](../Page/難波立愿.md "wikilink")、[三村玄澄等都是當時十分傑出的外科醫師](../Page/三村玄澄.md "wikilink")。

他的診療特色是信守古代的醫術所主張「實物親試」（即重視實驗，並親眼確定之）的精神。

他所用的歐傷牛草是屬於[茄科的植物](../Page/茄科.md "wikilink")，即[曼陀羅花](../Page/曼陀羅花.md "wikilink")，又名[朝鮮牽牛花](../Page/朝鮮牽牛花.md "wikilink")，原產於印度，於[十七世紀時由](../Page/十七世紀.md "wikilink")[國外傳到日本](../Page/國外.md "wikilink")，原本一般都當作[止痛劑或](../Page/止痛劑.md "wikilink")[安眠藥使用](../Page/安眠藥.md "wikilink")，後來才改良為麻醉藥。華岡青洲以自己的母親及妻子作人體實驗。在重複不斷實驗下，據說他的母親因而[死亡](../Page/死亡.md "wikilink")，其妻也為此雙目[失明](../Page/失明.md "wikilink")，下場十分悲慘。

## 年譜

  - 1782年，來到京都，學習古醫學及西洋的外科醫術。
  - 1785年，回到紀州，開始從事醫療工作。製造出配合了歐傷牛草（藥名）的內服全身麻醉劑（通仙散），並以其母親及妻子作人體實驗來做臨床藥理學的檢討。
  - 1805年，第一次利用通仙散，成功地完成乳癌手術。

[Category:日本发明家](../Category/日本发明家.md "wikilink")
[Category:外科醫生](../Category/外科醫生.md "wikilink")
[Category:日本漢醫學家](../Category/日本漢醫學家.md "wikilink")
[Category:江戶時代醫師](../Category/江戶時代醫師.md "wikilink")
[Category:江戶時代醫學家](../Category/江戶時代醫學家.md "wikilink")
[Category:紀州藩](../Category/紀州藩.md "wikilink")
[Category:紀伊國出身人物](../Category/紀伊國出身人物.md "wikilink")