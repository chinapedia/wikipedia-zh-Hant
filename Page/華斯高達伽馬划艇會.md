**華斯高達伽馬划艇會**（），是一間位於[巴西](../Page/巴西.md "wikilink")[里約熱內盧的體育會](../Page/里約熱內盧.md "wikilink")，在1898年8月21日成立，而足球部則於1915年11月5日成立。球會以著名[葡萄牙](../Page/葡萄牙.md "wikilink")[航海家](../Page/航海家.md "wikilink")[瓦斯科·达·伽马命名](../Page/瓦斯科·达·伽马.md "wikilink")，所以現在很多里約熱內盧的葡萄牙移民都很支持華斯高。

華斯高的主場位於[聖贊拿利奧球場](../Page/聖贊拿利奧球場.md "wikilink")，能容納31,000人，但有些比賽則在[馬拉卡拿進行](../Page/馬拉卡拿.md "wikilink")，那裡能容納103,000人。他們的球衣主要由白色再加上一條黑色對角線組成，還有白褲和白襪。

華斯高其中一個特殊角色是消除種族歧視，這個問題困擾了巴西足球一段長時間，所以球迷方面經常會引以自豪。

## 榮譽

  - ***國際***
      - [南美自由盃冠軍](../Page/南美自由盃.md "wikilink")(1): 1998
  - ***國家***
      - [巴西足球甲級聯賽冠軍](../Page/巴西足球甲級聯賽.md "wikilink")(4): 1974, 1989,
        1997, 2000
  - ***州際***
      - [州聯賽冠軍](../Page/州聯賽冠軍.md "wikilink")(22): 1923, 1924, 1929,
        1934, 1936, 1945, 1947, 1949, 1950, 1952, 1956, 1958, 1970,
        1977, 1982, 1987, 1988, 1992, 1993, 1994, 1998, 2003
  - ***其他***
      - [南美洲俱樂部錦標賽](../Page/1948年南美洲俱樂部錦標賽.md "wikilink")：1948年

## 聯賽紀錄

| 年度       | 排名      | 年度       | 排名      | 年度       | 排名      | 年度       | 排名   | 年度       | 排名  |
| -------- | ------- | -------- | ------- | -------- | ------- | -------- | ---- | -------- | --- |
| **1971** | 第12名    | **1981** | 第5名     | **1991** | 第11名    | **2001** | 第11名 | **2011** | 第2名 |
| **1972** | 第7名     | **1982** | 第10名    | **1992** | 第3名     | **2002** | 第15名 | **2012** |     |
| **1973** | 第14名    | **1983** | 第6名     | **1993** | 第20名    | **2003** | 第17名 | **2013** |     |
| **1974** | **第1名** | **1984** | 第2名     | **1994** | 第13名    | **2004** | 第16名 | **2014** |     |
| **1975** | 第20名    | **1985** | 第11名    | **1995** | 第20名    | **2005** | 第12名 | **2015** |     |
| **1976** | 第12名    | **1986** | 第13名    | **1996** | 第18名    | **2006** | 第6名  | **2016** |     |
| **1977** | 第12名    | **1987** | 第10名    | **1997** | **第1名** | **2007** | 第10名 | **2017** |     |
| **1978** | 第4名     | **1988** | 第5名     | **1998** | 第10名    | **2008** | 第18名 | **2018** |     |
| **1979** | 第2名     | **1989** | **第1名** | **1999** | 第6名     | **2009** | 巴乙   | **2019** |     |
| **1980** | 第8名     | **1990** | 第12名    | **2000** | **第1名** | **2010** | 第11名 | **2020** |     |

## 神射手

1.  [甸拿馬迪](../Page/甸拿馬迪.md "wikilink") - 在1110場賽事中射入698球
2.  [艾迪米](../Page/艾迪米.md "wikilink") - 在429場賽事中射入301球
3.  [羅馬里奧](../Page/羅馬里奧.md "wikilink") - 在376場賽事中射入290球
4.  [賓加](../Page/賓加.md "wikilink") - 在466場賽事中射入250球
5.  [伊波祖根](../Page/伊波祖根.md "wikilink") - 在413場賽事中射入225球

## 神射手（單季）

1.  [羅馬里奧](../Page/羅馬里奧.md "wikilink") - 在2000年球季射入65球
2.  [甸拿馬迪](../Page/甸拿馬迪.md "wikilink") - 在1981年球季射入61球

## 著名球員

  - [艾迪米](../Page/艾迪米.md "wikilink")
  - [戴亞士](../Page/戴亞士.md "wikilink")
  - [岩馬路](../Page/岩馬路.md "wikilink")
  - [桑尼安達臣](../Page/桑尼安達臣.md "wikilink")
  - [巴保沙](../Page/巴保沙.md "wikilink")
  - [貝比圖](../Page/貝比圖.md "wikilink")
  - [比連尼](../Page/比連尼.md "wikilink")
  - [畢圖](../Page/畢圖.md "wikilink")
  - [祖文奴](../Page/祖文奴.md "wikilink")
  - [芝高](../Page/芝高.md "wikilink")
  - [高加達](../Page/高加達.md "wikilink")
  - [戴素](../Page/戴素.md "wikilink")
  - [埃德蒙多·艾維斯·迪·蘇沙](../Page/埃德蒙多·艾維斯·迪·蘇沙.md "wikilink")
  - [法斯圖](../Page/法斯圖.md "wikilink")
  - [菲利比](../Page/菲利比.md "wikilink")
  - [基拔圖](../Page/基拔圖.md "wikilink")
  - [伊波祖根](../Page/伊波祖根.md "wikilink")
  - [波利斯達祖連奴](../Page/波利斯達祖連奴.md "wikilink")
  - [P祖連奴](../Page/P祖連奴.md "wikilink")
  - [馬高安東尼奧](../Page/馬高安東尼奧.md "wikilink")
  - [加維奧](../Page/加維奧.md "wikilink")
  - [馬仙奴](../Page/馬仙奴.md "wikilink")
  - [比根拿](../Page/比根拿.md "wikilink")
  - [奧斯卡連奴](../Page/奧斯卡連奴.md "wikilink")
  - [柏斯高](../Page/柏斯高.md "wikilink")
  - [柏甸奴](../Page/柏甸奴.md "wikilink")
  - [甸拿馬迪](../Page/甸拿馬迪.md "wikilink")
  - [羅馬里奧](../Page/羅馬里奧.md "wikilink")
  - [沙巴拿](../Page/沙巴拿.md "wikilink")
  - [華拿](../Page/華拿.md "wikilink")
  - [瓦瓦](../Page/瓦瓦_\(足球运动员\).md "wikilink")
  - [薛馬利亞](../Page/薛馬利亞.md "wikilink")

## 著名教練

  - [Abel Braga](../Page/Abel_Braga.md "wikilink")
  - [Antônio Lopes](../Page/Antônio_Lopes.md "wikilink")
  - [Gentil Cardoso](../Page/Gentil_Cardoso.md "wikilink")
  - [Otto Glória](../Page/Otto_Glória.md "wikilink")
  - [Joel Santana](../Page/Joel_Santana.md "wikilink")
  - [Sebastião Lazaroni](../Page/Sebastião_Lazaroni.md "wikilink")
  - [Zagallo](../Page/Mário_Zagallo.md "wikilink")
  - [Zezé Moreira](../Page/Zezé_Moreira.md "wikilink")

## 球場

華斯高的主場位於[聖贊拿利奧球場](../Page/聖贊拿利奧球場.md "wikilink")，在1927年落成，能容納31,000人。

## 趣聞

  - 華斯高的同市宿敵有:[富明尼斯](../Page/富明尼斯.md "wikilink")、[保地花高和](../Page/保地花高.md "wikilink")[法林明高](../Page/法林明高.md "wikilink")，當中後者是最大的對手。
  - 因為華斯高的傳統，有很多來自世界各地的體育會都叫「華斯高」。

## 會歌

華斯高的官方會歌在1918年由Joaquim Barros Ferreira da Silva創作。另一首官方會歌為*Meu
Pavilhão*(意思為*我的休息處*)，在三十年代由João de Freitas創作，Hernani
Correia作曲。後來這首歌取代了舊會歌。

不過，會最著名的會歌並非來自官方，而是由Lamartine Babo在1942年創作的會歌。

## 球迷組織

  - [Torcida Força Jovem Vasco](http://www.grtofjv.com.br/)
  - [Torcida Mancha Negra Vasco](http://www.manchanegravasco.cjb.net/)
  - Torcida Organizada do Vasco
  - Kamikazes Vascaínos
  - Pequenos Vascaínos
  - Renovascão Vasco Campeão
  - ResenVasco
  - VasBoaVista

## 外部連結

  - [官方網站](http://www.crvascodagama.com/)
  - [Torcida Força Jovem Vasco](http://www.grtofjv.com.br/)
  - [Torcida Mancha Negra Vasco](http://www.manchanegravasco.cjb.net/)
  - [非官方網站](http://www.netvasco.com.br//)
  - [非官方網站](http://www.supervasco.com.br//)

[Category:巴西足球俱樂部](../Category/巴西足球俱樂部.md "wikilink")
[Category:1898年建立的足球俱樂部](../Category/1898年建立的足球俱樂部.md "wikilink")