**華沙猶太人起義**（[波蘭語](../Page/波蘭語.md "wikilink"): **Powstanie w getcie
warszawskim** [英文](../Page/英文.md "wikilink")：**Warsaw Ghetto
Uprising**）。[希特勒為了要滅絕所有](../Page/希特勒.md "wikilink")[猶太人](../Page/猶太人.md "wikilink")，在[華沙地區也建立了猶太人居住區](../Page/華沙.md "wikilink")。1943年4月19日，猶太人群起反抗，結果是換來更快更狠的殺戮。短短3個星期之內殺了超過13,000人；另外，300,000人送到死亡營。

## 參考

[W](../Category/波兰德占时期.md "wikilink")
[\*](../Category/波蘭猶太人.md "wikilink")
[W](../Category/华沙历史.md "wikilink")
[W](../Category/第二次世界大戰歐洲戰場戰役.md "wikilink")
[Category:1943年波兰](../Category/1943年波兰.md "wikilink")
[Category:1943年德国](../Category/1943年德国.md "wikilink")
[Category:猶太歷史](../Category/猶太歷史.md "wikilink")
[Category:猶太人大屠殺](../Category/猶太人大屠殺.md "wikilink")