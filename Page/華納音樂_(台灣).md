**華納國際音樂股份有限公司**（），其前身為[飛碟唱片](../Page/飛碟唱片.md "wikilink")，是[華納音樂集團](../Page/華納音樂集團.md "wikilink")（全球三大唱片公司之一）的[台灣](../Page/台灣.md "wikilink")[子公司](../Page/子公司.md "wikilink")，在台灣也是主要[唱片公司之一](../Page/唱片公司.md "wikilink")。

## 沿革

台灣華納唱片的前身為[飛碟唱片](../Page/飛碟唱片.md "wikilink")（成立於1982年12月1日）。1991年[華納音樂集團來台合資](../Page/華納音樂集團.md "wikilink")，[收購飛碟唱片的部分](../Page/收購.md "wikilink")[股權](../Page/股權.md "wikilink")，並於1996年1月1日完全收購飛碟唱片，將飛碟唱片的品牌改為「**華納飛碟有限公司**」，簡稱「**華納飛碟**」（），並更改其商標為華納音樂與飛碟唱片並列。由於原經營團隊於1995年因經營理念歧見紛紛離職、另行成立[豐華唱片](../Page/豐華唱片.md "wikilink")，華納飛碟在人事變動的混亂情況下，經營業績及市場佔有率在1997年中期一度陷入低迷不振，所屬歌手約滿後紛紛跳槽或不再續約，旗下一度呈現僅有[鄭秀文及](../Page/鄭秀文.md "wikilink")[郭富城兩位指標性知名歌手的窘境](../Page/郭富城.md "wikilink")。

1998年1月1日，前[科藝年代總經理](../Page/千禧年代_\(唱片公司\).md "wikilink")[周建輝就職華納飛碟總經理](../Page/周建輝.md "wikilink")，並開始進行華納飛碟的企業整頓。1999年1月1日，華納飛碟改名為「**華納國際音樂股份有限公司**」（簡稱「**華納唱片**」；），並更改其企業標誌為[華納音樂集團企業標誌](../Page/華納音樂集團.md "wikilink")，原飛碟企業標誌正式走入歷史。

2000年，華納推出傾全力打造的新人[孫燕姿](../Page/孫燕姿.md "wikilink")，首張同名專輯市場銷售反應熱烈，隨後更陸續網羅[那英](../Page/那英.md "wikilink")、[張惠妹](../Page/張惠妹.md "wikilink")、[蔡健雅等知名歌手](../Page/蔡健雅.md "wikilink")，華納唱片的業績及聲勢在唱片市場一片衰退不景氣的情況下逆勢止跌回升，重新成為台灣唱片市場的指標公司。

2005至2008年，為[喬傑立娛樂旗下組合](../Page/喬傑立娛樂.md "wikilink")[5566](../Page/5566.md "wikilink")、[七朵花](../Page/七朵花.md "wikilink")、[183club發行唱片](../Page/183club.md "wikilink")。[新亞洲娛樂集團及](../Page/新亞洲娛樂集團.md "wikilink")[大國大熊星娛樂旗下歌手唱片由華納音樂代理發行](../Page/大國大熊星娛樂.md "wikilink")。

2006年，[劉德華在台灣以其創立之](../Page/劉德華.md "wikilink")[映藝娛樂名義發行國語唱片](../Page/映藝娛樂.md "wikilink")。

2010年，香港[東亞唱片及](../Page/東亞唱片_\(集團\).md "wikilink")[A
Music旗下歌手在台灣](../Page/東亞唱片_\(製作\).md "wikilink")、星馬地區的代理發行已轉由華納音樂代理。

2011年，[樂華娛樂旗下藝人在台灣由華納音樂代理發行](../Page/樂華娛樂.md "wikilink")。

2014年6月中旬，[金牌大風被華納唱片收購](../Page/金牌大風.md "wikilink")，旗下歌手唱片發行合約以及外語唱片代理版權，包括曾被[科藝百代收購的](../Page/科藝百代.md "wikilink")[點將唱片發行的專輯版權](../Page/點將唱片.md "wikilink")，皆轉入台灣華納唱片。原金牌大風的[YouTube官方頻道被更名為](../Page/YouTube.md "wikilink")“藍色點唱機”，並陸續發佈科藝百代、點將唱片時期的[音樂影片](../Page/音樂影片.md "wikilink")。

## 旗下藝人

<table>
<thead>
<tr class="header">
<th><p>男歌手</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/蕭敬騰.md" title="wikilink">蕭敬騰</a><br />
（<a href="../Page/喜鵲娛樂.md" title="wikilink">喜鵲娛樂</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊培安.md" title="wikilink">楊培安</a><br />
（星光娛樂）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳勢安.md" title="wikilink">陳勢安</a><br />
（星光娛樂）</p></td>
</tr>
<tr class="even">
<td><p>黃靖恆<br />
（<a href="../Page/喜鵲娛樂.md" title="wikilink">喜鵲娛樂</a>）</p></td>
</tr>
<tr class="odd">
<td><p>女歌手</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李佳薇_(歌手).md" title="wikilink">李佳薇</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃小琥.md" title="wikilink">黃小琥</a><br />
（星光娛樂）</p></td>
</tr>
<tr class="even">
<td><p>組合</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/獅子合唱團.md" title="wikilink">獅子合唱團</a><br />
（<a href="../Page/喜鵲娛樂.md" title="wikilink">喜鵲娛樂</a>）</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

### 合作及代理藝人（非合約）

#### 其他合作公司／代理藝人

<table>
<thead>
<tr class="header">
<th><p>男歌手</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/魏晨.md" title="wikilink">魏晨</a><br />
（威斯貝斯娛樂）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙傳.md" title="wikilink">趙傳</a><br />
（旋風音樂）</p></td>
</tr>
<tr class="odd">
<td><p>女歌手</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Vava.md" title="wikilink">Vava</a><br />
（混血兒娛樂）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/于文文.md" title="wikilink">-{于}-文文</a><br />
（-{于}-文文工作室）</p></td>
</tr>
</tbody>
</table>

#### 日本 (包含代理發行)

#### 韓國 (包含代理發行)

## 已約滿／解約歌手

### 飛碟唱片／華納唱片 (台灣／大陸)

<div class="NavFrame" style="text-align: left; width:100%">

  - [王杰](../Page/王杰_\(歌手\).md "wikilink")（1987－1995）　1995年轉投[波丽佳音](../Page/波丽佳音.md "wikilink")
  - [呂方](../Page/呂方.md "wikilink")（1990－1998）　1998年轉投[華研國際音樂](../Page/華研國際音樂.md "wikilink")
  - [關德輝](../Page/關德輝.md "wikilink")（1995－1998）　1999年自設德輝工作室，轉投乾坤唱片
  - [增山裕紀](../Page/增山裕紀.md "wikilink")（2001－2003）　2004年被華納解約
  - [郭富城](../Page/郭富城.md "wikilink")（1990－2006）　2006年轉投[大國文化集團](../Page/大國文化集團.md "wikilink")
  - [周杰倫](../Page/周杰倫.md "wikilink")（2004－2005）
    代理發行，2005年轉回[新力博得曼音樂](../Page/台灣索尼音樂娛樂.md "wikilink")（原本唱片公司是[新力音樂](../Page/台灣索尼音樂娛樂.md "wikilink")，2004年[阿爾發音樂跟](../Page/阿爾發音樂.md "wikilink")[新力音樂解約](../Page/台灣索尼音樂娛樂.md "wikilink")，改與[華納音樂合作](../Page/華納唱片_\(台灣\).md "wikilink")，但是到了2005年，合約又回到[新力博得曼音樂](../Page/台灣索尼音樂娛樂.md "wikilink")）
  - [孫楠](../Page/孫楠.md "wikilink")（2003－2007）　2007年合約期滿
  - [張菲](../Page/張菲.md "wikilink")（2004－2005）　2005年合約期滿
  - [郭品超](../Page/郭品超.md "wikilink")（2004－2009）　2010年轉投[華研國際音樂](../Page/華研國際音樂.md "wikilink")
  - [費玉清](../Page/費玉清.md "wikilink")（1992－2009）　2010年轉投[台灣索尼音樂娛樂](../Page/台灣索尼音樂娛樂.md "wikilink")
  - [李玖哲](../Page/李玖哲.md "wikilink")（2003－2010）　2010年轉投[亞神音樂](../Page/亞神音樂.md "wikilink")　
  - [周立銘](../Page/周立銘.md "wikilink")（2003－2010）　2010年轉投[亞神音樂](../Page/亞神音樂.md "wikilink")，2015年轉投[華研國際音樂](../Page/華研國際音樂.md "wikilink")　
  - [方大同](../Page/方大同.md "wikilink")（2005－2013）　2013年轉投[金牌大風](../Page/金牌大風.md "wikilink")
  - [黃立行](../Page/黃立行.md "wikilink")（2008－2010）　原簽三張唱片合約，僅發行一張便提前解約
  - [何維健](../Page/何維健.md "wikilink")（2008－2010）　2010年轉投[金牌大風](../Page/金牌大風.md "wikilink")
  - [周湯豪](../Page/周湯豪.md "wikilink")（2008－2010）　2010年轉投[金牌大風](../Page/金牌大風.md "wikilink")
  - [黃靖倫](../Page/黃靖倫.md "wikilink")（2008－2010）　2010年轉投[金牌大風](../Page/金牌大風.md "wikilink")
  - [張棟樑](../Page/張棟樑.md "wikilink")（2009－2010）　2011年轉投[種子音樂](../Page/種子音樂.md "wikilink")，2015年轉投[環球音樂](../Page/環球唱片_\(台灣\).md "wikilink")
  - [劉家昌](../Page/劉家昌.md "wikilink")（2009－2010）　2011年轉投延伸娛樂
  - [劉子千](../Page/劉子千.md "wikilink")（2009－2010）　2011年轉投[延伸娛樂](../Page/延伸娛樂.md "wikilink")
  - [歐得洋](../Page/歐得洋.md "wikilink")（2005－2011）　2011年轉投[擎天娛樂](../Page/擎天娛樂.md "wikilink")
  - [張信哲](../Page/張信哲.md "wikilink")（2006－2011）　2012年轉投[金牌大風](../Page/金牌大風.md "wikilink")（唱片發行合約——[潮水音樂](../Page/潮水音樂.md "wikilink")）
  - [陳偉聯](../Page/陳偉聯.md "wikilink")（2006－2011）　2012年轉投[擎天娛樂](../Page/擎天娛樂.md "wikilink")
  - [胡彥斌](../Page/胡彥斌.md "wikilink") (2011－2012)
    　2013年成立太歌文化工作室，唱片發行合約轉投[海蝶音樂](../Page/海蝶音樂.md "wikilink")，台灣唱片約則由[金牌大風發行](../Page/金牌大風_\(台灣\).md "wikilink")
  - [王識賢](../Page/王識賢.md "wikilink") (2012－2013) 2013年合約期滿
  - [蕭閎仁](../Page/蕭閎仁.md "wikilink")（2013－2014）　2014年返回[台灣索尼音樂娛樂](../Page/台灣索尼音樂娛樂.md "wikilink")，經紀約仍是[無限延伸](../Page/無限延伸.md "wikilink")
  - [黃明志](../Page/黃明志.md "wikilink")（2013－2014）
    代理发行，2015年轉投[量能文創](../Page/量能文創.md "wikilink")，由[爱貝克思發行](../Page/愛貝克思_\(台灣\).md "wikilink")
  - [鶴天賜](../Page/鶴天賜.md "wikilink")（2014）　代理發行
  - [拉卡．飛琅](../Page/拉卡·巫茂.md "wikilink") (2013－2015) 2015年合約期滿
  - [王尚恩](../Page/王尚恩.md "wikilink")（2015）　代理發行
  - [喬任梁](../Page/喬任梁.md "wikilink")（2012－2016） 2016年因憂鬱症自殺
  - [吳克群](../Page/吳克群.md "wikilink") (2014－2017)
    和群娛樂製作，2017年合約期滿，2018年重返[種子音樂](../Page/種子音樂.md "wikilink")
  - [蕭煌奇](../Page/蕭煌奇.md "wikilink") (2011－2018)
    2018年轉投[環球音樂](../Page/環球唱片_\(台灣\).md "wikilink")

</div>

<div class="NavFrame" style="text-align: left; width:100%">

  - [孫翠鳳](../Page/孫翠鳳.md "wikilink") 只簽一張合約
  - [苏芮](../Page/苏芮.md "wikilink")（1983－1989,1993－1995）　1989年轉投[福茂唱片](../Page/福茂唱片.md "wikilink")，1995年轉投[豐華唱片](../Page/豐華唱片.md "wikilink")
  - [曾慶瑜](../Page/曾慶瑜.md "wikilink")（1996－1999）　1999年合約期滿，引退
  - [郭舒賢](../Page/郭舒賢.md "wikilink")（1997－2000）　2000年合約期滿，回歸電視圈
  - [那英](../Page/那英.md "wikilink")（2001－2004）　2004年合約期滿，2011年轉投[亞神音樂](../Page/亞神音樂.md "wikilink")
  - [周迅](../Page/周迅.md "wikilink")（2004－2006）　2006年合約期滿，同年轉投[華誼兄弟](../Page/華誼兄弟.md "wikilink")（[華誼音樂](../Page/華誼兄弟.md "wikilink")）
  - [張惠妹](../Page/張惠妹.md "wikilink")（2001－2006）　2007年自設聲動娛樂並轉投[金牌大風](../Page/金牌大風.md "wikilink")，2014年轉投[百代唱片](../Page/環球唱片_\(台灣\).md "wikilink")
  - 張鳳鳳（2003－2005）　大紅音樂製作，2007年轉投[海蝶音樂](../Page/海蝶音樂.md "wikilink")
  - [陳嘉唯](../Page/陳嘉唯.md "wikilink")（2003－2006）　2008年轉投[台灣索尼音樂娛樂](../Page/台灣索尼音樂娛樂.md "wikilink")
  - [蔡健雅](../Page/蔡健雅.md "wikilink")（2003－2006）　2007年自設天涯音樂工作室並轉投[亞神音樂](../Page/亞神音樂.md "wikilink")
  - [孫燕姿](../Page/孫燕姿.md "wikilink")（2000－2006）　2007年轉投[Capitol唱片](../Page/金牌大風.md "wikilink")，2011年轉投[美妙音樂](../Page/美妙音樂.md "wikilink")，2013年轉投[環球音樂](../Page/環球唱片_\(台灣\).md "wikilink")
  - [蕭亞軒](../Page/蕭亞軒.md "wikilink")（2005－2007）　2008年轉投[金牌大風](../Page/金牌大風.md "wikilink")，2013年轉投[台灣索尼音樂娛樂](../Page/台灣索尼音樂娛樂.md "wikilink")
  - [辛曉琪](../Page/辛曉琪.md "wikilink")（2006－2008）　2008年合約期滿，2011年轉投[美夢成真音樂](../Page/美夢成真音樂.md "wikilink")
  - 楊韻禾（2007－2009）　2010年轉投錞藝音樂
  - [何耀珊](../Page/何耀珊.md "wikilink")（2006－2009）　2009年合約期滿
  - [蔡淳佳](../Page/蔡淳佳.md "wikilink")（2004－2010）　2010年合約期滿，自己成立Joi Music
  - [徐若瑄](../Page/徐若瑄.md "wikilink")（2007－2010）　2010年與華納解約，2011年轉投[環球音樂](../Page/環球唱片_\(台灣\).md "wikilink")，2014年轉投[美妙音樂](../Page/美妙音樂.md "wikilink")
  - [郭美美](../Page/郭美美.md "wikilink")（2005－2011）　2011年合約期滿，2012年轉投[擎天娛樂](../Page/擎天娛樂.md "wikilink")，2016年轉投[海蝶音樂](../Page/海蝶音樂.md "wikilink")
  - [黃鶯鶯](../Page/黃鶯鶯.md "wikilink")（2012）　代理發行
  - [梁詠琪](../Page/梁詠琪.md "wikilink")（2012）　代理發行
  - [舞思愛](../Page/舞思愛.md "wikilink")（2011－2013） 2013年合約期滿
  - [陳思函](../Page/陳思函.md "wikilink")（2008－2013） 2013年合約期滿
  - [李愛綺](../Page/李愛綺.md "wikilink")（2010－2015） 2015年合約期滿
  - [王詩安](../Page/王詩安.md "wikilink")（2013－2016） 2016年合約期滿，2016年轉投闊思音樂
  - [郭采潔](../Page/郭采潔.md "wikilink")（2007－2016） 2016年合約期滿
  - [吳映潔](../Page/吳映潔.md "wikilink")（2016）　代理發行
  - [利菁](../Page/利菁.md "wikilink")（2015－2017）
    魔耳國際娛樂製作，2018年轉投[台灣索尼音樂娛樂](../Page/台灣索尼音樂娛樂.md "wikilink")
  - [蔡依林](../Page/蔡依林.md "wikilink")（2008－2018）凌時差音樂製作，2018年轉投[台灣索尼音樂娛樂](../Page/台灣索尼音樂娛樂.md "wikilink")

</div>

<div class="NavFrame" style="text-align: left; width:100%">

  - [Choc7](../Page/Choc7.md "wikilink")（2008－2009）　2009年轉投[金牌大風
    (台灣)](../Page/金牌大風_\(台灣\).md "wikilink")
  - [183club](../Page/183club.md "wikilink")（2005－2009）　2009年解散
  - [5566](../Page/5566.md "wikilink")（2005－2009）　2009年合約期滿
  - [七朵花](../Page/七朵花.md "wikilink")（2005－2009）　2009年解散
  - [黃立成](../Page/黃立成.md "wikilink") &
    [麻吉](../Page/麻吉.md "wikilink")（2003－2005）　2005年解約
  - [黑Girl](../Page/黑Girl.md "wikilink")（2008－2009）　2009年合約期滿，2011年轉投群石國際
  - [中國娃娃](../Page/中國娃娃_\(組合\).md "wikilink")（2008－2011）2011年合約期滿
  - [東城衛](../Page/東城衛.md "wikilink")(2010－2013) 2013年合約期滿
  - [COLOR BAND](../Page/COLOR_BAND.md "wikilink")(2011－2013) 2013年合約期滿
  - [F.I.R.](../Page/F.I.R..md "wikilink")（2004－2015）
    2015年合約期滿，同年轉投[華研音樂](../Page/華研國際音樂.md "wikilink")
  - [SpeXial](../Page/SpeXial.md "wikilink")（2012－2016）
    2016年合約期滿，2017年轉投[堅果音樂](../Page/堅果音樂.md "wikilink")（眾水之音文化為台灣代理）

</div>

### 金牌大風 （台灣／大陸）

## 參見

  - [飛碟唱片](../Page/飛碟唱片.md "wikilink")
  - [金牌大風](../Page/金牌大風_\(台灣\).md "wikilink")
  - [華納音樂集團](../Page/華納音樂集團.md "wikilink")
  - [華納兄弟唱片公司](../Page/華納兄弟唱片公司.md "wikilink")
  - [相信音樂](../Page/相信音樂.md "wikilink")

## 外部連結

  - [華納線上音樂雜誌](http://www.warnermusic.com.tw)（華納音樂官方網站）

  -
  -
  -
  -
  -
  -
  -
  - [飛碟唱片出版目錄：1983～1991](http://roxytom.bluecircus.net/archives/008849.html)

## 參考文獻

<div class="references-small">

<references />

</div>

[分類:華納音樂集團](../Page/分類:華納音樂集團.md "wikilink")

[Category:台灣唱片公司](../Category/台灣唱片公司.md "wikilink")