[缩略图](https://zh.wikipedia.org/wiki/File:Huaxi_Night_Market.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Huaxi_Street_night_market_0250_-_Summer_2007.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Huaxi_Street_night_market_0266_-_Summer_2007.jpg "fig:缩略图")
**華西街觀光夜市**位於[臺灣](../Page/臺灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[萬華區華西街香火鼎盛的](../Page/萬華區.md "wikilink")[艋舺龍山寺附近](../Page/艋舺龍山寺.md "wikilink")，被桂林路分隔成兩段，是台灣專門規劃的第一座[觀光夜市](../Page/夜市.md "wikilink")，以販賣各式山產、海鮮及野味小吃為大宗，其中以蛇店最為引人注目，是很多國內外觀光客最鍾愛的景點之一。

## 概要

夜市入口處為傳統牌樓建築，兩側掛滿紅色宮燈，極具特色，兩旁店家皆為老字號，連高級餐廳也有據點，像是廣受日本觀光客青睞的台南[擔仔麵一店](../Page/擔仔麵.md "wikilink")，就是在這裡發蹟，其他小吃類，例如大鼎[肉羹或是兩喜號](../Page/肉羹.md "wikilink")[魷魚](../Page/魷魚.md "wikilink")[羹等](../Page/羹.md "wikilink")，而這裡的海鮮店也是這個夜市足堪引人入勝之處，其中以[燒酒](../Page/燒酒.md "wikilink")[蝦獨具一格](../Page/蝦.md "wikilink")。

這幾年並有[腳底按摩等休閒業在此開設分店以服務觀光客](../Page/腳底按摩.md "wikilink")。此外又因靠近早期尋芳客密集地[寶斗里](../Page/寶斗里.md "wikilink")，因此夜市出現了許多以去毒[壯陽為號召的](../Page/壯陽.md "wikilink")[蛇店及](../Page/蛇.md "wikilink")[鱉店](../Page/鱉.md "wikilink")，形成當地小吃的特色，其中烤蛇肉串、蛇肉湯、炒蛇肝等往往成為這邊店內有名的招牌菜，此外還有蛇鞭、蛇酒等更是當地一絕。除了蛇肉店之外，沿街還有本土味十足的[草藥店及賣](../Page/草藥.md "wikilink")[靈芝茶的小攤](../Page/靈芝.md "wikilink")，一樣也成為這個夜市最具特色的賣點。

## 週邊

鄰近[龍山寺](../Page/龍山寺.md "wikilink")，附近有萬華區公所、[青草巷](../Page/青草巷.md "wikilink")、[萬華夜市](../Page/萬華夜市.md "wikilink")、[捷運龍山寺站及](../Page/捷運龍山寺站.md "wikilink")[萬華車站等](../Page/萬華車站.md "wikilink")。

## 公共自行車

[台北市公共自行車租賃系統華西公園站](../Page/台北市公共自行車租賃系統.md "wikilink")

## 特色小吃

  - [擔仔麵](../Page/擔仔麵.md "wikilink")
  - [燒酒蝦](../Page/燒酒蝦.md "wikilink")
  - [蛇肉製品](../Page/蛇.md "wikilink")、蛇鞭等
  - [青草茶](../Page/青草茶.md "wikilink")
  - 大鼎[肉羹](../Page/肉羹.md "wikilink")
  - [炒米粉](../Page/炒米粉.md "wikilink")
  - [碗粿](../Page/碗粿.md "wikilink")
  - 阿猜嬤甜湯、北港甜湯

## 電影取景

  - [成龍](../Page/成龍.md "wikilink")1993年的電影《[重案組](../Page/重案組_\(電影\).md "wikilink")》中，[霹靂小組攻堅的橋段即在此取景](../Page/霹靂小組.md "wikilink")。
  - [鈕承澤電影作品](../Page/鈕承澤.md "wikilink")《[艋舺](../Page/艋舺_\(電影\).md "wikilink")》。
  - 2018年的韩国電影《[北風](../Page/北風_\(2018年電影\).md "wikilink")》中，将台北市的华西街夜市打造成王府井大街。

## 參見

  - [台灣夜市列表](../Page/台灣夜市列表.md "wikilink")
  - [台灣夜市](../Page/台灣夜市.md "wikilink")
  - [台灣小吃](../Page/台灣小吃.md "wikilink")
  - [夜市](../Page/夜市.md "wikilink")
  - [艋舺夜市](../Page/艋舺夜市.md "wikilink")

## 外部連結

  - [華西街夜市線上逛街Q版夜市地圖](http://www.walkgoler.cc/qmap/96)
  - [住台辦華西街交通實景圖](http://www.like2twn.com/newwin_favorite.jsp?pgname=trafic_tpe_lst.jsp&password=123)
  - [華西街觀光夜市
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/1535)

[category:台北市夜市](../Page/category:台北市夜市.md "wikilink")

[Category:萬華區](../Category/萬華區.md "wikilink")