**華語文學傳媒大獎**由《[南方都市報](../Page/南方都市報.md "wikilink")》在2003年斥資設立，每年評選去年度的優秀作家，為中國首個由[大眾傳媒設立](../Page/大眾傳媒.md "wikilink")、及有公正人員參與評選全過程的年度文學獎項，设立时是大陸獎金最高的雅文學大獎。

## 年度傑出作家獎

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>得獎人/作品</p></th>
<th><p>其它提名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2002年（第一屆）</p></td>
<td><p><a href="../Page/史鐵生.md" title="wikilink">史鐵生</a> 《病隙碎筆》</p></td>
<td><ul>
<li><a href="../Page/张洁.md" title="wikilink">张洁</a> 《無字》</li>
<li><a href="../Page/北島_(詩人).md" title="wikilink">北島</a> 《北岛诗歌集》</li>
</ul>
<p>|- </p></td>
</tr>
<tr class="even">
<td><p>2004年（第三屆）</p></td>
<td><p><a href="../Page/格非.md" title="wikilink">格非</a> 《人面桃花》</p></td>
<td><ul>
<li><a href="../Page/琦君.md" title="wikilink">琦君</a> 《素心笺》</li>
<li><a href="../Page/于坚.md" title="wikilink">于坚</a> 《于坚集》</li>
</ul>
<p>|- </p></td>
</tr>
<tr class="odd">
<td><p>2006年（第五屆）</p></td>
<td><p><a href="../Page/韓少功.md" title="wikilink">韓少功</a> 《山南水北》</p></td>
<td><ul>
<li><a href="../Page/铁凝.md" title="wikilink">铁凝</a> 《笨花》</li>
<li><a href="../Page/苏童.md" title="wikilink">苏童</a> 《碧奴》</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2007年（第六屆）</p></td>
<td><p><a href="../Page/王安忆.md" title="wikilink">王安忆</a> 《启蒙时代》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年（第七屆）</p></td>
<td><p><a href="../Page/阿來.md" title="wikilink">阿來</a> 《空山》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年（第八屆）</p></td>
<td><p><a href="../Page/苏童.md" title="wikilink">苏童</a>（《河岸》）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年（第九屆）</p></td>
<td><p><a href="../Page/张炜.md" title="wikilink">张炜</a>（《你在高原》）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年（第十屆）</p></td>
<td><p><a href="../Page/方方.md" title="wikilink">方方</a>（《武昌城》）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年（第十一屆）</p></td>
<td><p><a href="../Page/翟永明.md" title="wikilink">翟永明</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年（第十二屆）</p></td>
<td><p><a href="../Page/余華.md" title="wikilink">余華</a>《第七天》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年（第十三屆）</p></td>
<td><p><a href="../Page/賈平凹.md" title="wikilink">賈平凹</a>《老生》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年（第十四屆）</p></td>
<td><p><a href="../Page/歐陽江河.md" title="wikilink">歐陽江河</a>《大是大非》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年（第十五屆）</p></td>
<td><p><a href="../Page/于堅.md" title="wikilink">于堅</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年（第十六屆）</p></td>
<td><p><a href="../Page/葉兆言.md" title="wikilink">葉兆言</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 年度小說家獎

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>得獎人/作品</p></th>
<th><p>其它提名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2002年（第一屆）</p></td>
<td><p><a href="../Page/韓少功.md" title="wikilink">韓少功</a> 《暗示》</p></td>
<td><p>内容 |- </p></td>
</tr>
<tr class="even">
<td><p>2004年（第三屆）</p></td>
<td><p><a href="../Page/林白_(作家).md" title="wikilink">林白</a> 《婦女閒聊錄》</p></td>
<td><ul>
<li><a href="../Page/王刚.md" title="wikilink">王刚</a> 《英格力士》</li>
<li><a href="../Page/北村.md" title="wikilink">北村</a> 《愤怒》</li>
<li><a href="../Page/李洱.md" title="wikilink">李洱</a> 《石榴树上结樱桃》</li>
<li><a href="../Page/陈希我.md" title="wikilink">陈希我</a> 《抓痒》</li>
</ul>
<p>|- </p></td>
</tr>
<tr class="odd">
<td><p>2006年（第五屆）</p></td>
<td><p><a href="../Page/北村.md" title="wikilink">北村</a> 《我和上帝有个约》</p></td>
<td><ul>
<li><a href="../Page/严歌苓.md" title="wikilink">严歌苓</a> 《第九个寡妇》</li>
<li><a href="../Page/都梁.md" title="wikilink">都梁</a> 《狼烟北平》</li>
<li><a href="../Page/李銳_(小說家).md" title="wikilink">李銳</a> 《太平风物》</li>
<li><a href="../Page/范稳.md" title="wikilink">范稳</a> 《悲悯大地》</li>
</ul>
<p>|- </p></td>
</tr>
<tr class="even">
<td><p>2008年（第七屆）</p></td>
<td><p><a href="../Page/畢飛宇.md" title="wikilink">畢飛宇</a> 《推拿》（拒絕領獎） |- </p></td>
<td><p>2009年（第八屆）</p></td>
</tr>
<tr class="odd">
<td><p>2010年（第九屆）</p></td>
<td><p><a href="../Page/魏微.md" title="wikilink">魏微</a> |- </p></td>
<td><p>2011年（第十屆）</p></td>
</tr>
<tr class="even">
<td><p>2012年（第十一屆）</p></td>
<td><p><a href="../Page/金宇澄.md" title="wikilink">金宇澄</a> |- </p></td>
<td><p>2013年（第十二屆）</p></td>
</tr>
<tr class="odd">
<td><p>2014年（第十三屆）</p></td>
<td><p><a href="../Page/徐則臣.md" title="wikilink">徐則臣</a>《耶路撒冷》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年（第十四屆）</p></td>
<td><p><a href="../Page/路內.md" title="wikilink">路內</a>《慈悲》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年（第十五屆）</p></td>
<td><p><a href="../Page/張悅然.md" title="wikilink">張悅然</a>《繭》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年（第十六屆）</p></td>
<td><p><a href="../Page/嚴歌苓.md" title="wikilink">嚴歌苓</a>《芳華》</p></td>
<td></td>
</tr>
</tbody>
</table>

## 年度散文家獎

  - 第一屆 (2002) - [李國文](../Page/李國文.md "wikilink") 《中国文人的非正常死亡》
  - 第二屆 (2003) - [余光中](../Page/余光中.md "wikilink") 《左手的掌纹》
  - 第三屆 (2004) - [南帆](../Page/南帆.md "wikilink") 《关于我父母的一切》
  - 第四屆 (2005) - [徐曉](../Page/徐曉.md "wikilink") 《半生為人》
  - 第五屆 (2006) - [李辉](../Page/李辉_\(作家\).md "wikilink")
    《〈收获〉杂志2006年第1期至6期的“封面中国”系列散文》
  - 第六屆 (2007) - [舒婷](../Page/舒婷.md "wikilink") 《真水无香》
  - 第七屆 (2008) - [李西闽](../Page/李西闽.md "wikilink")（《幸存者》）
  - 第八屆 (2009) - [張承志](../Page/張承志.md "wikilink")
  - 第九屆 (2010) - [齊邦媛](../Page/齊邦媛.md "wikilink")
  - 第十屆 (2011) - [赵越胜](../Page/赵越胜.md "wikilink")（《燃灯者：忆周辅成》）
  - 第十一屆 (2012) - [梁鸿](../Page/梁鸿.md "wikilink")
  - 第十二屆 (2013) - [李辉](../Page/李辉_\(作家\).md "wikilink")《绝响·八十年代亲历记》
  - 第十三屆 (2014) - [毛尖](../Page/毛尖.md "wikilink")《有一只老虎在浴室》
  - 第十四屆 (2015) - [趙柏田](../Page/趙柏田.md "wikilink")《南華錄》
  - 第十五屆 (2016) - [李敬澤](../Page/李敬澤.md "wikilink")《會飲記》
  - 第十六屆 (2017) - [周晓枫](../Page/周晓枫.md "wikilink")《有如候鸟》

## 年度詩人獎

  - 第一屆 (2002) - [于堅](../Page/于堅.md "wikilink")
  - 第二屆 (2003) - [王小妮](../Page/王小妮.md "wikilink") 《十枝水蓮》
  - 第三屆 (2004) - [多多](../Page/多多.md "wikilink")
  - 第四屆 (2005) - [李亚伟](../Page/李亚伟.md "wikilink")《豪猪的诗篇》
  - 第五屆 (2006) - [雷平阳](../Page/雷平阳.md "wikilink")《雷平阳诗选》
  - 第六屆 (2007) - [杨键](../Page/杨键.md "wikilink")《古桥头》
  - 第七屆 (2008) - [臧棣](../Page/臧棣.md "wikilink")
  - 第八屆 (2009) - [朵渔](../Page/朵渔.md "wikilink")
  - 第九屆 (2010) - [歐陽江河](../Page/歐陽江河.md "wikilink")
  - 第十屆 (2011) - [黄灿然](../Page/黄灿然.md "wikilink")（《我的灵魂》）
  - 第十一屆 (2012) - [沈浩波](../Page/沈浩波.md "wikilink")
  - 第十二屆 (2013) - [张执浩](../Page/张执浩.md "wikilink")《宽阔》
  - 第十三屆 (2014) - [沈苇](../Page/沈苇.md "wikilink")《沈苇诗选》
  - 第十四屆 (2015) - [宇向](../Page/宇向.md "wikilink")《向他們湧來》
  - 第十五屆 (2016) - [陈先发](../Page/陈先发.md "wikilink")《裂隙与巨眼》
  - 第十六屆 (2017) - [蓝蓝](../Page/蓝蓝.md "wikilink")《说吧，悲伤》

## 年度文學評論家獎

  - 第一屆 (2002) - [陳曉明](../Page/陳曉明.md "wikilink") 《表意的焦虑————历史祛魅与当代文学变革》
  - 第二屆 (2003) - [王尧](../Page/王尧.md "wikilink")
  - 第三屆 (2004) - [李敬澤](../Page/李敬澤.md "wikilink") 《见证一千零一夜》、《文学：行动与联想》
  - 第四屆 (2005) - [张新颖](../Page/张新颖.md "wikilink") 《双重见证》《沈从文精读》
  - 第五屆 (2006) - [王德威](../Page/王德威.md "wikilink") 《当代小说二十家》
  - 第六屆 (2007) - [陈超](../Page/陈超.md "wikilink") 《中国先锋诗歌论》
  - 第七屆 (2008) - [耿占春](../Page/耿占春.md "wikilink")
  - 第八屆 (2009) - [郜元宝](../Page/郜元宝.md "wikilink")
  - 第九屆 (2010) - [張清華](../Page/張清華.md "wikilink")
  - 第十屆 (2011) - [李静](../Page/李静.md "wikilink")（《捕风记：九评中国作家》）
  - 第十一屆 (2012) - [孟繁华](../Page/孟繁华.md "wikilink")
  - 第十二屆 (2013) - [孫郁](../Page/孫郁.md "wikilink")
  - 第十三屆 (2014) - [李洁非](../Page/李洁非.md "wikilink")《文学史微观察》
  - 第十四屆 (2015) - [唐曉渡](../Page/唐曉渡.md "wikilink")《先行到失敗中去》
  - 第十五屆 (2016) - [江弱水](../Page/江弱水.md "wikilink")
  - 第十六屆 (2017) - [敬文东](../Page/敬文东.md "wikilink")《感叹诗学》

## 年度最具潛力新人獎

  - 第一屆 (2002) - [盛可以](../Page/盛可以.md "wikilink") 《水乳》
  - 第二屆 (2003) - [須一瓜](../Page/須一瓜.md "wikilink") 《淡绿色的月亮》、《蛇宫》等
  - 第三屆 (2004) - [張悅然](../Page/張悅然.md "wikilink") 《十愛》
  - 第四屆 (2005) - [李師江](../Page/李師江.md "wikilink") 《逍遙遊》
  - 第五屆 (2006) - [乔叶](../Page/乔叶.md "wikilink") 《打火机》、《锈锄头》等
  - 第六屆 (2007) - [徐则臣](../Page/徐则臣.md "wikilink") 《工地上的女人》、《艳阳》等
  - 第七屆 (2008) - [塞壬](../Page/塞壬_\(作家\).md "wikilink")
  - 第八屆 (2009) - [笛安](../Page/笛安.md "wikilink")（《西决》）
  - 第九屆 (2010) - [七堇年](../Page/七堇年.md "wikilink")
  - 第十屆 (2011) - [阿乙](../Page/阿乙_\(作家\).md "wikilink")（《寡人》））
  - 第十一屆 (2012) - [颜歌](../Page/颜歌.md "wikilink")
  - 第十二屆 (2013) - [赵志明](../Page/赵志明.md "wikilink")《我亲爱的精神病患者》
  - 第十三屆 (2014) - [文珍](../Page/文珍.md "wikilink")《我们夜里在美术馆谈恋爱》
  - 第十四屆 (2015) - [蔡東](../Page/蔡東.md "wikilink")《我想要的一天》
  - 第十五屆 (2016) - [双雪涛](../Page/双雪涛.md "wikilink")
  - 第十六屆 (2017) - [郝景芳](../Page/郝景芳.md "wikilink")《人之彼岸》

[Category:中華人民共和國文學獎](../Category/中華人民共和國文學獎.md "wikilink")
[Category:2003年建立的奖项](../Category/2003年建立的奖项.md "wikilink")
[Category:华语文学传媒大奖](../Category/华语文学传媒大奖.md "wikilink")