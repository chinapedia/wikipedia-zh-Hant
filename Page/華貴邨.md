[Noble_Square.jpg](https://zh.wikipedia.org/wiki/File:Noble_Square.jpg "fig:Noble_Square.jpg")
[Wah_Kwai_Estate_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Wah_Kwai_Estate_Basketball_Court.jpg "fig:Wah_Kwai_Estate_Basketball_Court.jpg")
[Wah_Kwai_Estate_Football_Field.jpg](https://zh.wikipedia.org/wiki/File:Wah_Kwai_Estate_Football_Field.jpg "fig:Wah_Kwai_Estate_Football_Field.jpg")
[Wah_Kwai_Estate_Gym_Zone_(4)_and_Pavilion.jpg](https://zh.wikipedia.org/wiki/File:Wah_Kwai_Estate_Gym_Zone_\(4\)_and_Pavilion.jpg "fig:Wah_Kwai_Estate_Gym_Zone_(4)_and_Pavilion.jpg")
[Wah_Kwai_Estate_Playground_(4),_Pebble_Walking_Trail_and_Gym_Zone.jpg](https://zh.wikipedia.org/wiki/File:Wah_Kwai_Estate_Playground_\(4\),_Pebble_Walking_Trail_and_Gym_Zone.jpg "fig:Wah_Kwai_Estate_Playground_(4),_Pebble_Walking_Trail_and_Gym_Zone.jpg")
[Wah_Kwai_Estate_Chess_Zone.jpg](https://zh.wikipedia.org/wiki/File:Wah_Kwai_Estate_Chess_Zone.jpg "fig:Wah_Kwai_Estate_Chess_Zone.jpg")
[Wah_Kwai_Estate_Covered_Walkway.jpg](https://zh.wikipedia.org/wiki/File:Wah_Kwai_Estate_Covered_Walkway.jpg "fig:Wah_Kwai_Estate_Covered_Walkway.jpg")
[Wah_Kwai_Estate_Car_Park.jpg](https://zh.wikipedia.org/wiki/File:Wah_Kwai_Estate_Car_Park.jpg "fig:Wah_Kwai_Estate_Car_Park.jpg")
**華貴邨**（）是一條[香港公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區_\(香港\).md "wikilink")[雞籠灣旁](../Page/雞籠灣.md "wikilink")，興建目的主要是接收[田灣邨及](../Page/田灣邨.md "wikilink")[石排灣邨部分的重建戶](../Page/石排灣邨.md "wikilink")，Y型大廈及商場由房屋署總建築師設計，外型呈W字型的華愛樓則由[巴馬丹拿建築及工程師有限公司設計](../Page/巴馬丹拿.md "wikilink")，為港島區首幢小型單位大廈，並由[香港房屋委員會負責樓宇管理](../Page/香港房屋委員會.md "wikilink")，由港島西區租約事務管理處負責屋邨租約事務，停車場由[領展負責管理](../Page/領展.md "wikilink")，現時已經成立有業主立案法團，華孝樓、華廉樓、華禮樓、華善樓和華賢樓由新昌物業管理有限公司管理，華愛樓和公共用地則由嘉怡物業管理有限公司管理。

## 屋邨歷史

華貴邨的原址屬於[雞籠灣（奇力灣）的一部份](../Page/雞籠灣.md "wikilink")，1985年，[香港政府決定遷拆雞籠環村及後在並在該處](../Page/香港殖民地時期#香港政府.md "wikilink")[填海](../Page/填海.md "wikilink")，同時解除地段的「[薄扶林延期履行權](../Page/薄扶林延期履行權.md "wikilink")」規劃限制，交由當時[香港房屋委員會建立華貴邨](../Page/香港房屋委員會.md "wikilink")。屋邨於1990年落成入伙，主要收容舊[田灣邨居民](../Page/田灣邨.md "wikilink")。1998年，華貴邨成為當時唯一在南區成立的[租者置其屋計劃的屋邨](../Page/租者置其屋計劃.md "wikilink")，首個在[港島推行租者置其屋計劃的屋邨](../Page/港島.md "wikilink")。

## 地理位置

華貴邨位於薄扶林西南部；[華富邨東南部](../Page/華富邨.md "wikilink")，[嘉隆苑以北](../Page/嘉隆苑.md "wikilink")，面向[火藥洲及](../Page/火藥洲.md "wikilink")[鴨脷洲](../Page/鴨脷洲.md "wikilink")，遠眺[東博寮海峽及](../Page/東博寮海峽.md "wikilink")[南丫島](../Page/南丫島.md "wikilink")。

## 屋邨建築設計

華貴邨5座Y4型樓宇按山勢呈半月形排列，前方則以T型排列的居屋嘉隆苑，務求製造更多的海景及山景單位，減低單位間的互望。而華貴邨及嘉隆苑的外牆選色以地盤的自然地貌為主，以海的藍色及山的綠色為主色，以灰白色襯托，再以附近山勢形態作外牆圖案。華貴邨近海邊設有一個圓形水飾廣場更是一個遊樂場與嬉水池雙結合的嶄新概念設計，可惜除更衣室及洗手間之外，其餘設施因屋邨管理問題而停用至今。
華貴邨也是繼華富邨後，另一個以[市鎮概念設計的屋邨](../Page/市鎮.md "wikilink")，屋邨規劃以華貴商場為中心，商場更滙聚飲食、超級市場、銀行等不同行業設施，街市則已結業，雖然規模比華富邨小，但也同樣一應俱全。而在華貴商場旁，設有華貴社區中心及升降機塔連行人天橋連接山上的華富邨。

## 樓宇與設施

### 樓宇

| 樓宇名稱 | 樓宇類型                             | 落成年份 | 備註           |
| ---- | -------------------------------- | ---- | ------------ |
| 華孝樓  | [Y4型](../Page/Y4型.md "wikilink") | 1990 |              |
| 華廉樓  |                                  |      |              |
| 華禮樓  |                                  |      |              |
| 華善樓  |                                  |      |              |
| 華賢樓  |                                  |      |              |
| 華愛樓  | 小型單位大廈                           | 1997 | 非「租者置其屋計劃」大廈 |

### 設施

邨內設有社區中心、民政事務處、公眾室內停車場、商場、巴士總站及小巴總站。

Wah Kwai Estate Playground and Gym Zone (2).jpg|兒童遊樂場及健體區（2） Wah Kwai
Estate Playground.jpg|兒童遊樂場（3） Wah Kwai Estate Playground
(3).jpg|傳統[鞦韆遊樂場](../Page/鞦韆.md "wikilink") Wah Kwai Estate
Gym Zone (3).jpg|健體區（3）

#### 商場

商場名為華貴商場，原為[香港房屋委員會持有](../Page/香港房屋委員會.md "wikilink")，2005年售予[領展房地產信託基金](../Page/領展房地產信託基金.md "wikilink")。商場曾設有酒樓、街市（華貴街市）、診所、停車場等設施。
商場於2014年被[領展以](../Page/領展.md "wikilink")5.18億港元出售後，商場改名為「華貴坊」。新業主原本聲稱翻新街市，但2015年6月突然在沒有通知下將街市結業，20多個商戶頓失生計，[音樂噴泉](../Page/音樂噴泉.md "wikilink")，街市盛傳會改建成大型超市或特賣場。華貴邨業主立案法團指有近2萬居民受影響，長者和輪椅住戶現只能光顧超市「捱貴菜」，促盡快重開街市，盼政府介入協助。\[1\]。2016年，新業主更將整個商場關閉圍封，以將商場翻新。

Noble Square (2).jpg|華貴坊（2） Wah Kwai Shopping Centre.JPG|翻新前的華貴商場 Wah
Kwai Market.JPG|結業前的華貴街市

#### 停車場

2014年，林子峰以5.18億港元購入華貴商場和停車場後。其後業主將停車場改名「華貴坊」。2016年，華貴坊近300個車位被拆售，業主共套現約2.9億港元。其中一個車位以108萬港元成交，刷新全港最貴公屋車位紀錄。華貴坊餘下約60個車位將用作長線收租之用。\[2\]

## 教育及福利設施

### 幼稚園

  - [聖文嘉中英文幼稚園（華貴邨）](http://www.stmonicawk.edu.hk)（1991年創辦）（位於華賢樓地下）
  - [保良局莊啟程夫人（華貴）幼稚園](http://kgn.poleungkuk.org.hk/tc/935/page.html)（1992年創辦）（位於華貴社區中心4樓）
  - [道爾頓幼稚園](http://www.littledalton.com/chinese/)（2012年創辦）（位於華善樓地下KG01室）

<!-- end list -->

  - *已結束*

<!-- end list -->

  - 華貴村中英文幼稚園（1991年創辦）（位於華善樓地下）

### 綜合青少年服務中心

  - [香港小童群益會賽馬會南區青少年綜合服務中心](https://southern.bgca.org.hk)（位於華貴社區中心1及3樓）

### 長者鄰舍中心

  - [香港仔坊會方王換娣長者鄰舍中心](http://www.aka.org.hk/home.php?p=service_fwwt)（位於華貴社區中心地下）

### 長者日間護理中心

  - [香港仔坊會華貴長者日間護理中心](http://www.aka.org.hk/home.php?p=service_wkdcc)（位於華愛樓地下）

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [華貴邨巴士總站](../Page/華貴邨巴士總站.md "wikilink")

<!-- end list -->

  - [華富道](../Page/華富道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - [華富邨至](../Page/華富邨.md "wikilink")[黃竹坑線](../Page/黃竹坑.md "wikilink")\[3\]
  - [香港仔至](../Page/香港仔.md "wikilink")[中環線](../Page/中環.md "wikilink")\[4\]
  - 香港仔至[西環線](../Page/西環.md "wikilink")\[5\]
  - 香港仔至[旺角線](../Page/旺角.md "wikilink") (24小時服務)\[6\]
  - 香港仔至[觀塘線](../Page/觀塘.md "wikilink") (平日下午時段服務)\[7\]

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  - 香港至[汕頭線](../Page/汕頭.md "wikilink")\[8\]

</div>

</div>

## 資料來源

## 外部連結

  - [房委會華貴邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2753)

[en:Public housing estates in Pok Fu Lam, Aberdeen and Ap Lei Chau\#Wah
Kwai
Estate](../Page/en:Public_housing_estates_in_Pok_Fu_Lam,_Aberdeen_and_Ap_Lei_Chau#Wah_Kwai_Estate.md "wikilink")

[Category:雞籠灣](../Category/雞籠灣.md "wikilink")
[Category:紅色公共小巴禁區](../Category/紅色公共小巴禁區.md "wikilink")
[Category:租者置其屋計劃屋邨](../Category/租者置其屋計劃屋邨.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.
2.
3.  [華富邨－黃竹坑站](http://www.16seats.net/chi/rmb/r_h71.html)
4.  [香港仔東勝道　—　中環交易廣場](http://www.16seats.net/chi/rmb/r_h71.html)
5.  [香港仔漁暉道　—　石塘咀創業商場](http://www.16seats.net/chi/rmb/r_h43.html)
6.  [香港仔東勝道　—　旺角金雞廣場](http://www.16seats.net/chi/rmb/r_kh70.html)
7.  [香港仔湖北街　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
8.  [香港 \<\>
    汕頭(澄海花園酒店)](http://www.i-busnet.com/china/busroute/guangdong/hk-shatou.php)