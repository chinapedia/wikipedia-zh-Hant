<table>
<tbody>
<tr class="odd">
<td><table>
<caption><font size="+1"><strong>菲尔·戈夫</strong></font></caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Labour_Leader_Phil_Goff_in_Hamilton.JPG" title="fig:Labour_Leader_Phil_Goff_in_Hamilton.JPG">Labour_Leader_Phil_Goff_in_Hamilton.JPG</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>职位</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>任职时段</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>前任</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>出生日期</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>出生地点</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>配偶</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong><a href="../Page/政党.md" title="wikilink">政党</a></strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

**菲尔·戈夫**（,
），出生于[纽西兰](../Page/纽西兰.md "wikilink")[奥克兰市](../Page/奥克兰市.md "wikilink")。后就读于[奥克兰大学政治学院](../Page/奥克兰大学.md "wikilink")，后获得文学硕士学位。

1968年至1974年期间，曾于一件冷藏工厂当冷藏工人。

1975年至1979年期间，于母校[奥克兰大学担任讲师](../Page/奥克兰大学.md "wikilink")。

1991年至1993年期间，在[奥克兰理工学院担任高级讲师](../Page/奥克兰理工学院.md "wikilink")。

## 工党时期

1969年加入[纽西兰工党](../Page/纽西兰工党.md "wikilink")，并先后在多个党内部门从事行政工作。

1975年至1977年，菲尔·戈夫任职于纽西兰工党青年咨询委员会并担任该委员会主席职务。

1977年至1978年，为工党纽西兰委员会代表。

1980年至1981年作为组织者参与纽西兰保险业公会。

1981年，作为纽西兰工党Mt. Roskill国会议员候选人并成功当选国会议员。

1984年至1990年，菲尔·戈夫作为当时的工党政府成员，相继擔任住房部长、环境部长、就业部长、旅游部长、青年事务部长、教育部长等职。

1990年，工党国会选举失利，退出政府。

1993年，与新一届国内大选中再次当选Mt. Roskill选區国会议员。

1996年，当选New
Lyn选区国会议员，同时担任[纽西兰国家党政府时期的](../Page/纽西兰国家党.md "wikilink")[反对党](../Page/反对党.md "wikilink")[纽西兰工党在](../Page/纽西兰工党.md "wikilink")[国会的司法](../Page/国会.md "wikilink")、法庭和矫正事务发言人。

1999年至2002年，两次连续当选Mt. Roskill选区国会议员，任外交贸易部长和司法部长。

2005年9月17日，在當年的纽西兰国会大选中，同样在Mt.
Roskill选区，菲尔·戈夫获得了总共32931张选票当中的19,476，以领先第二名[纽西兰国家党候选人Jackie](../Page/纽西兰国家党.md "wikilink")
Blue9095张选票和[行动党](../Page/行动党.md "wikilink")[华裔候选人的](../Page/华裔.md "wikilink")[王小选](../Page/王小选.md "wikilink")1882张选票，再次轻松当选国会议员，并出任[纽西兰工党政府的内阁国防部长职务至](../Page/纽西兰工党.md "wikilink")2008年11月。

菲尔·戈夫目前为救助残障儿童社团、纽西兰青年信托和地方体育俱乐部的赞助人。

菲尔·戈夫已婚，有三个子女。

## 參考

[紐西蘭2005年國會選舉：Mt
Roskill选取投票結果](https://web.archive.org/web/20070731071551/http://2005.electionresults.govt.nz/electorate-28.html)

## 外部連結

  - [本届纽西兰工党政府内的关于菲尔·戈夫的网页](https://web.archive.org/web/20061206100514/http://www.beehive.govt.nz/Minister.aspx?MinisterID=38)

[G](../Category/紐西蘭政治人物.md "wikilink")
[G](../Category/奧克蘭大學校友.md "wikilink")
[Category:奥克兰大学教师](../Category/奥克兰大学教师.md "wikilink")