《**菲比尋常Live\!**》（***Faye Wong
Live\!***）是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王菲的第三張](../Page/王菲.md "wikilink")[演唱會專輯](../Page/演唱會.md "wikilink")，於2004年11月12日在[香港发行](../Page/香港.md "wikilink")，2CD裝。CD1是17首歌曲，CD2為16首歌曲。

## 曲目

## 幕後人員

出自專輯內頁

  - 導演: Ida Wong
  - 音樂總監: 張亞東

<!-- end list -->

  - Produced By: Jim Lee
  - Mastering Engineer: Jim Lee
  - Mixed By:Jim Lee
  - 後期影像製作導演: 鄺盛

## 獎項

IFPI香港2005年10大暢銷囯語專輯。\[1\]

## 唱片版本

  - [台灣版](../Page/台灣.md "wikilink")：2CD，紅色大圓形貼標，附送海報一張。
  - [香港版](../Page/香港.md "wikilink")：2CD，進口台灣版，有王菲頭像側標。
  - [大陸版](../Page/大陸.md "wikilink")：分拆成CD1、CD2兩張發行。

其他版本：

  - [SACD](../Page/SACD.md "wikilink")：2004年11月。
  - [DVD](../Page/DVD.md "wikilink")：2004年12月。
  - [VCD](../Page/VCD.md "wikilink")：2004年12月。

## 注釋

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[Category:王菲音樂專輯](../Category/王菲音樂專輯.md "wikilink")
[Category:2004年音樂專輯](../Category/2004年音樂專輯.md "wikilink")

1.  <http://www.ifpihk.org/hong-kong-top-sales-music-award-presented-01-11/hong-kong-top-sales-music-award-presented/2005>
    IFPI Hong Kong