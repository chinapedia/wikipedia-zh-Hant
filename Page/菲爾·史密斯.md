**菲利普·史密斯**（，），出生於[倫敦](../Page/倫敦.md "wikilink")[哈羅](../Page/哈羅.md "wikilink")（Harrow），[英格蘭](../Page/英格蘭.md "wikilink")[職業](../Page/職業.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，司職[門將](../Page/門將.md "wikilink")，現時效力[英乙球會](../Page/英乙.md "wikilink")[艾迪索特](../Page/艾迪索特镇足球俱乐部.md "wikilink")。

## 生平

菲爾·史密斯加入[-{zh-hans:米尔沃尔;
zh-hant:米禾爾;}-的青年隊開展其職業生涯](../Page/米爾沃爾足球俱乐部.md "wikilink")，後來因不獲續約而轉投非職業球隊[福克斯通恩維達](../Page/福克斯通恩維達足球俱樂部.md "wikilink")（Folkestone
Invicta），又輾轉加入[多佛爾競技](../Page/多佛爾競技俱樂部.md "wikilink")（Dover
Athletic）及[馬蓋特](../Page/馬蓋特足球俱樂部.md "wikilink")（Margate）。他終在[克拉雷成為正選球員](../Page/克劳利镇足球俱乐部.md "wikilink")。

克拉雷在進行內部調整期間將所有球員掛牌出售，菲爾·史密斯是其中一位有參與季前集訓的五位克拉雷球員之一。2006年夏季，菲爾·史密斯經試腳後加盟「知更鳥」。菲爾·史密斯來到史雲頓後便須與同隊的門將[彼得·比列蘇雲](../Page/彼得·比列蘇雲.md "wikilink")（Peter
Brezovan）爭奪正選上陣的機會。2006年10月14日，比列蘇雲在主場迎戰[甘士比的比賽裡手臂受傷](../Page/格林斯比镇足球俱乐部.md "wikilink")，菲爾·史密斯遂得以上陣，其他的比賽都由菲爾·史密斯及比列蘇雲輪流上陣。

自此，菲爾·史密斯的守門能力甚為穩固。在2007年1月27日主場對陣[馬爾斯菲之前](../Page/馬科斯菲爾德足球俱乐部.md "wikilink")，菲爾·史密斯獲選為12月英格蘭職業足球員聯會球迷最喜愛的球員，他亦獲續約兩年至2009年，確保了他在球會內的地位。他在2006年至2007年賽季期間頑強的穩健表現並沒有被球迷忽視，他在2007年5月獲選為年度最佳球員。

球會的教練團隊對他的表現很滿意，2007年夏季，他獲得1號球衣號碼。在新球季的第一場聯賽，菲爾·史密斯的表現良好，協助球隊作客以1-1賽和[諾咸頓](../Page/北安普頓足球俱乐部.md "wikilink")。菲爾·史密斯持續的出色表現使他成為正選門將。10月6日，菲爾·史密斯並以清白之身的狀態協助球隊在主場以5-0大勝[基寧咸](../Page/吉林漢姆足球俱乐部.md "wikilink")。10月10日，菲爾·史密斯因傷需休養一個月，無法在客場對[維爾港的賽事上陣](../Page/韋爾港足球俱乐部.md "wikilink")，也無法在接下來的兩場主場聯賽賽事上陣。

2009年6月16日菲爾·史密斯獲[史雲頓提供一年新約](../Page/斯温登足球俱乐部.md "wikilink")，是17名約滿球員中獲留用的4名球員之一<small>\[1\]</small>。

## 參考資料

## 外部連結

  -
[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:米禾爾球員](../Category/米禾爾球員.md "wikilink")
[Category:多佛爾競技球員](../Category/多佛爾競技球員.md "wikilink")
[Category:馬蓋特球員](../Category/馬蓋特球員.md "wikilink")
[Category:卡維尼球員](../Category/卡維尼球員.md "wikilink")
[Category:史雲頓球員](../Category/史雲頓球員.md "wikilink")
[Category:樸茨茅夫球員](../Category/樸茨茅夫球員.md "wikilink")
[Category:艾迪索特球員](../Category/艾迪索特球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:英格蘭足球全國聯賽球員](../Category/英格蘭足球全國聯賽球員.md "wikilink")

1.