**萊恩·羅尼·雷諾斯**（，）\[1\]是一名[加拿大男演員和製片人](../Page/加拿大.md "wikilink")。

雷諾斯較著名的是在[ABC的](../Page/美國廣播公司.md "wikilink")[情境喜劇](../Page/情境喜劇.md "wikilink")《》（1998年至2001年）與[YTV的加拿大青少年電視劇](../Page/YTV.md "wikilink")《》（1991年至1993年）的演出。於[漫威漫畫改編電影](../Page/漫威漫畫.md "wikilink")《[刀鋒戰士3](../Page/刀鋒戰士3.md "wikilink")》（2004年）中飾演漢尼拔·金以及《[X戰警：金鋼狼](../Page/X戰警：金鋼狼.md "wikilink")》中的[韋德·威爾遜／死侍](../Page/死侍.md "wikilink")（2009年）。他還曾於[DC漫畫改編電影](../Page/DC漫畫.md "wikilink")《[綠光戰警](../Page/綠光戰警_\(電影\).md "wikilink")》（2011年）飾演[哈爾·喬丹／綠光戰警](../Page/哈爾·喬丹.md "wikilink")。2013年，雷諾斯於動畫電影《[渦輪方程式](../Page/渦輪方程式.md "wikilink")》中擔任了主角蝸寶的配音。

2016年，他於《[惡棍英雄：死侍](../Page/惡棍英雄：死侍.md "wikilink")》中不僅再度飾演死侍一角，也是再度擔任超級英雄電影主角。該片贏得了高評與相當成功的票房，並使雷諾斯獲得[金球獎提名](../Page/金球獎.md "wikilink")。

## 早期生活

萊恩出生於[不列顛哥倫比亞的](../Page/不列顛哥倫比亞.md "wikilink")[溫哥華](../Page/溫哥華.md "wikilink")，母親譚米（Tammy）是一名售貨員和在職學生，父親吉姆·雷諾斯是溫哥華當地的食品經銷商，同時也曾是一名半職業的[拳擊手](../Page/拳擊.md "wikilink")\[2\]。他在家中的四名兄弟中是年紀最小的；他有一個兄長目前是[加拿大皇家騎警的警官](../Page/加拿大皇家騎警.md "wikilink")，他的父親也曾短暫擔任過警察。萊恩於1994年畢業於。他也曾就讀列治文的昆特蘭大學（Kwantlen
Polytechnic University）一段時間。

## 職業生涯

雷諾斯的演藝生涯始於1990年，他在加拿大的青少年電視劇《》中飾演比利。1996年，他與[梅莉莎·喬·杭特共同主演電視電影](../Page/梅莉莎·喬·杭特.md "wikilink")《》。成年後，他主演了電影《》和在美國電視劇《》中飾演麥克·「伯格」·卑爾根。在1993年至1994年間，他還曾在電視劇《》演出過。他與[麥克·道格拉斯](../Page/麥克·道格拉斯.md "wikilink")、[艾伯特·布魯克斯一起在](../Page/艾伯特·布魯克斯.md "wikilink")《》（2003年）中擔任主演，以及主演加拿大電影《》。雷諾斯在2004年的喜劇電影《》中以[客串方式演出](../Page/客串.md "wikilink")。

2005年，他在浪漫喜劇《》中飾演了一個名為蒙迪的服務員，並在與[安娜·法瑞絲](../Page/安娜·法瑞絲.md "wikilink")、[艾咪·史瑪特主演的](../Page/艾咪·史瑪特.md "wikilink")《》中飾演克里斯一角。雷諾斯在2008年的電影《[愛情三選一](../Page/愛情三選一.md "wikilink")》中擔任主角。此外他還在電視劇《[實習醫生成長記](../Page/實習醫生成長記.md "wikilink")》的第二季大結局中短暫演出。2007年，他在TBS電視台的電視劇《[My
Boys](../Page/My_Boys.md "wikilink")》的「Douchebag in the
City」一集中出演。2009年，雷諾斯與[珊卓·布拉克一同主演了電影](../Page/珊卓·布拉克.md "wikilink")《[愛情限時簽](../Page/愛情限時簽.md "wikilink")》，並獲得了還不錯的評價，在此之前他也曾在《[菜鳥新人王](../Page/菜鳥新人王.md "wikilink")》中飾演麥克·康奈爾。

雖然雷諾斯早期出演的作品多以喜劇為主，但他積極地鍛練過自己的體格，以詮釋體能要求較高的角色。2004年的《[刀鋒戰士3](../Page/刀鋒戰士3.md "wikilink")》中的角色。他在1979年恐怖片的2005年重拍版電影《》中飾演一角。此外，他還和[雷·利奥塔一起在](../Page/雷·利奥塔.md "wikilink")2006年的犯罪動作片《[五路追殺令](../Page/五路追殺令.md "wikilink")》中飾演FBI探員。雷諾斯後來還飾演過動漫超級英雄人物，即2009年《[X戰警：金鋼狼](../Page/X戰警：金鋼狼.md "wikilink")》與2016年《[惡棍英雄：死侍](../Page/惡棍英雄：死侍.md "wikilink")》的死侍，以及2011年在[華納兄弟的DC漫畫英雄電影](../Page/華納兄弟.md "wikilink")《[綠光戰警](../Page/綠光戰警_\(電影\).md "wikilink")》中詮釋的[哈爾·喬丹／綠光戰警](../Page/哈爾·喬丹.md "wikilink")，這使他成為同時飾演過漫威漫畫和DC漫畫人物的演員之一\[3\]。

雷諾斯也有在其他類型的電影演出與配音取得成績。2010年，他出演了西班牙和美國合拍的驚悚片《》，並也在[日舞影展上亮相](../Page/日舞影展.md "wikilink")\[4\]。2010年6月，雷諾斯受邀加入成為[美國電影藝術與科學學會的一員](../Page/美國電影藝術與科學學會.md "wikilink")\[5\]。2012年，他與[丹佐·華盛頓一同主演電影](../Page/丹佐·華盛頓.md "wikilink")《[狡兔計畫](../Page/狡兔計畫.md "wikilink")》。而他在2013年的動畫電影《[古魯家族](../Page/古魯家族.md "wikilink")》與《[渦輪方程式](../Page/渦輪方程式.md "wikilink")》中分別擔任阿蓋與渦寶的配音，兩部電影皆有很好的評價和票房。雷諾斯在[環球影業改編自](../Page/環球影業.md "wikilink")[黑馬漫畫的電影](../Page/黑馬漫畫.md "wikilink")《[降魔戰警](../Page/降魔戰警.md "wikilink")》（2013年）中飾演尼克·華克\[6\]。他曾在2014年電影《[百萬種硬的方式](../Page/百萬種硬的方式.md "wikilink")》中客串登場過一情節。2015年，他與[海倫·米蘭合作演出的藝術電影](../Page/海倫·米蘭.md "wikilink")《[名畫的控訴](../Page/名畫的控訴.md "wikilink")》獲得了不錯的票房。2016年，除了《惡棍英雄：死侍》外，他還在驚悚犯罪片《[換腦行動](../Page/換腦行動.md "wikilink")》中飾演一名配角。

2016年2月16日，宣布雷諾斯將與[蕾貝卡·弗格森](../Page/蕾貝卡·弗格森_\(演員\).md "wikilink")、[傑克·葛倫霍一起共演科幻驚悚片](../Page/傑克·葛倫霍.md "wikilink")《[異星智慧](../Page/異星智慧.md "wikilink")》，導演為[丹尼爾·伊斯皮諾薩](../Page/丹尼爾·伊斯皮諾薩.md "wikilink")\[7\]。電影於2017年3月在北美上映。雷諾斯與[山繆·傑克森共同主演的動作喜劇片](../Page/山繆·傑克森.md "wikilink")《[殺手保鑣](../Page/殺手保鑣.md "wikilink")》則於2017年8月上映，由[派屈克·休斯執導](../Page/派屈克·休斯.md "wikilink")。2018年5月22日，宣布雷諾斯將出演[麥可·貝執導的](../Page/麥可·貝.md "wikilink")[Netflix動作片](../Page/Netflix.md "wikilink")《[飛天遁地](../Page/飛天遁地.md "wikilink")》\[8\]。

### 扮演超級英雄電影人物

於2005年3月接受採訪時，雷諾斯表示很感興趣參與由[大衛·S·高耶編劇的](../Page/大衛·S·高耶.md "wikilink")[死侍獨立電影](../Page/死侍.md "wikilink")\[9\]，並可能在未來的電影項目中飾演[DC漫畫角色閃電人](../Page/DC漫畫.md "wikilink")[華利·威斯特](../Page/華利·威斯特.md "wikilink")，但後來換成[綠光戰警](../Page/綠光戰警.md "wikilink")。

雷諾斯先在《[X戰警：金鋼狼](../Page/X戰警：金鋼狼.md "wikilink")》（2009年）中飾演[韋德·威爾遜／死侍](../Page/死侍.md "wikilink")，並後來官方重新宣布將塑造他的死侍分拆系列\[10\]。這部《死侍》由於《[X戰警：未來昔日](../Page/X戰警：未來昔日.md "wikilink")》發生的事件使故事架構重啟，過去的電影情節成為平行時空，得以忽略掉在《X戰警：金鋼狼》中不忠實於原著且反應不佳的情節，為角色打造一個全新的故事\[11\]。2014年，電影《死侍》的測試影片（電腦動畫）被洩露在網路上，片中的死侍也是由雷諾斯配音\[12\]，並於2015年3月中開始拍攝\[13\]\[14\]\[15\]\[16\]，5月29日[殺青](../Page/殺青.md "wikilink")\[17\]。由於《惡棍英雄：死侍》成功的口碑，使得在2016年4月，證實雷諾斯確定回歸主演續集\[18\]。在[第74屆金球獎上](../Page/第74屆金球獎.md "wikilink")，雷諾斯憑在《惡棍英雄：死侍》中的演出而入圍了[金球獎最佳音樂及喜劇類電影男主角](../Page/金球獎最佳音樂及喜劇類電影男主角.md "wikilink")。續集《[死侍2](../Page/死侍2.md "wikilink")》於2018年上映，雷諾斯再次回歸飾演死侍。後續確認有《X特遣隊》、《死侍3》。而[華特迪士尼公司在](../Page/華特迪士尼公司.md "wikilink")2019年3月20日收購福斯後，確認雷諾斯將繼續在[漫威電影宇宙飾演死侍](../Page/漫威電影宇宙.md "wikilink")，《X特遣隊》、《死侍3》照常製作不重啟。

## 個人生活

2002年至2007年間，雷諾斯和女歌手[艾拉妮絲·莫莉塞特傳出戀情](../Page/艾拉妮絲·莫莉塞特.md "wikilink")，並於2004年訂婚\[19\]。2006年7月，傳出兩人已經分手的消息，但並未獲得任何一方的證實。當月稍晚，有消息指出兩人的確正在交往，而
Contact Music
網站報導兩人分手的消息只是謠言。莫莉塞特和雷諾斯被拍到兩人在洛杉磯牽手的照片，謠言不攻自破。然而，2007年2月，兩人決定取消訂婚。2008年，他與女星[史嘉蕾·喬韓森結婚](../Page/史嘉蕾·喬韓森.md "wikilink")，但这段婚姻于2010年结束\[20\]。2012年9月9日，雷诺斯和[布蕾克·萊芙莉结婚](../Page/布蕾克·萊芙莉.md "wikilink")。女兒詹姆絲（James）出生於2014年12月。2016年雷諾斯和萊芙莉迎来兩人的第二個小孩，該女兒名為伊妮絲（Ines）。\[21\]

此外，雷諾斯是美式足球隊伍[綠灣包裝工隊的球迷](../Page/綠灣包裝工.md "wikilink")。

## 作品列表

[Ryan_Reynolds_Cannes_2014.jpg](https://zh.wikipedia.org/wiki/File:Ryan_Reynolds_Cannes_2014.jpg "fig:Ryan_Reynolds_Cannes_2014.jpg")上\]\]

### 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>標題</p></th>
<th><p>角色</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1993</p></td>
<td><p>《》</p></td>
<td><p>Ganesh／Jeffrey<br />
加涅許／傑佛瑞</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p>《My Name Is Kate》</p></td>
<td><p>Kevin Bannister<br />
凱文·班尼斯特</p></td>
<td><p><a href="../Page/電視電影.md" title="wikilink">電視電影</a></p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p>《》</p></td>
<td><p>Andy<br />
安迪</p></td>
<td><p>電視電影</p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p>《》</p></td>
<td><p>Ben Colson<br />
班·科爾森</p></td>
<td><p>電視電影</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>Seth<br />
賽斯</p></td>
<td><p>電視電影</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Bobby Rupp<br />
伯比·魯普</p></td>
<td><p>電視電影</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p>《》</p></td>
<td><p>Howard Ancona<br />
霍華·安科納</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p>《Tourist Trap》</p></td>
<td><p>Wade Early<br />
韋德</p></td>
<td><p>電視電影</p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p>《》</p></td>
<td><p>Henry Lipschitz<br />
亨利·利普希茲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Chip<br />
薛皮</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p>《Big Monster on Campus》</p></td>
<td><p>Karl O'Reilly<br />
卡爾·歐賴利</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Red Shoes<br />
瑞德·秀斯</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p>《》</p></td>
<td><p>Quigley<br />
奎格利</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p>《》</p></td>
<td><p>Van Wilder<br />
范·懷爾德</p></td>
<td><p>提名-<a href="../Page/MTV電影大獎.md" title="wikilink">MTV電影大獎突破性男演員</a></p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>Mike Hanson<br />
麥克·漢森</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p>《》</p></td>
<td><p>Mark Tobias<br />
馬克·托拜厄斯</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>Kevin<br />
凱文</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>《》</p></td>
<td><p>男護士</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/刀鋒戰士3.md" title="wikilink">刀鋒戰士3</a>》</p></td>
<td><p>Hannibal King<br />
</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p>《》</p></td>
<td><p>Michael "Mr. D" D'Angelo<br />
麥可·「D先生」·安吉洛</p></td>
<td><p>電視電影</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>George Lutz<br />
</p></td>
<td><p><a href="../Page/青少年票選獎.md" title="wikilink">青少年票選獎必選電影可怕場景</a>[22]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Monty<br />
蒙迪</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>Chris Brander<br />
克里斯·布蘭德</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p>《<a href="../Page/五路追殺令.md" title="wikilink">五路追殺令</a>》</p></td>
<td><p>Richard Messner<br />
李察·梅斯納爾</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p>《》</p></td>
<td><p>Gary／Gavin／Gabriel<br />
蓋瑞／蓋文／蓋布瑞</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>《》</p></td>
<td><p>Frank Allen<br />
法蘭克·艾倫</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/愛情三選一.md" title="wikilink">愛情三選一</a>》</p></td>
<td><p>Will Hayes<br />
威爾·海斯</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Michael Taylor<br />
麥可·泰勒</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>《<a href="../Page/菜鳥新人王.md" title="wikilink">菜鳥新人王</a>》</p></td>
<td><p>Mike Connell<br />
麥克·康奈爾</p></td>
<td><p>提名-<a href="../Page/哥譚獨立電影獎.md" title="wikilink">哥譚獨立電影獎最佳全體演員</a></p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/X戰警：金鋼狼.md" title="wikilink">X戰警：金鋼狼</a>》</p></td>
<td><p>Wade Wilson／Deadpool<br />
<a href="../Page/死侍.md" title="wikilink">韋德·威爾森／死侍</a></p></td>
<td><p>與<a href="../Page/史考特·艾金斯.md" title="wikilink">史考特·艾金斯共演此角</a><br />
提名-<a href="../Page/全美民選獎.md" title="wikilink">全美民選獎最喜愛的螢幕團隊</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/愛情限時簽.md" title="wikilink">愛情限時簽</a>》</p></td>
<td><p>Andrew Paxton<br />
安德魯·帕克斯頓</p></td>
<td><p>提名-<a href="../Page/MTV電影大獎.md" title="wikilink">MTV電影大獎最佳喜劇表演</a><br />
提名-<a href="../Page/全美民選獎.md" title="wikilink">全美民選獎最喜愛的螢幕團隊</a><br />
提名-<a href="../Page/青少年票選獎.md" title="wikilink">必選暑期電影明星</a><br />
青少年票選獎必選暑期電影演員-浪漫喜劇類<br />
提名-青少年票選獎必選電影-來電螢幕情侶<br />
提名-青少年票選獎必選電影-接吻戲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Captain Excellent<br />
超讚隊長</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p>《秘密起源：英雄漫畫的故事》</p></td>
<td><p>旁白</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Paul Steven Conroy<br />
保羅·史蒂芬·康羅伊</p></td>
<td><p>提名-<a href="../Page/哥雅獎.md" title="wikilink">哥雅獎最佳男演員</a><br />
提名-<a href="../Page/MTV電影大獎.md" title="wikilink">MTV電影大獎最佳嚇破膽演出</a><br />
提名-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p>《<a href="../Page/綠光戰警_(電影).md" title="wikilink">綠光戰警</a>》</p></td>
<td><p>Hal Jordan／Green Lantern<br />
<a href="../Page/哈爾·喬丹.md" title="wikilink">哈爾·喬丹／綠光戰警</a></p></td>
<td><p>提名-<a href="../Page/青少年票選獎.md" title="wikilink">青少年票選獎必選電影演員-科幻／奇幻類</a></p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Mitch Planko<br />
米契</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《The Whale》[23]</p></td>
<td><p>旁白</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p>《<a href="../Page/狡兔計畫.md" title="wikilink">狡兔計畫</a>》</p></td>
<td><p>Matt Weston<br />
麥特·威斯頓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/熊麻吉.md" title="wikilink">熊麻吉</a>》</p></td>
<td><p>Jared<br />
傑瑞德</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p>《<a href="../Page/古魯家族.md" title="wikilink">古魯家族</a>》</p></td>
<td><p>Guy<br />
阿蓋</p></td>
<td><p>配音</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/渦輪方程式.md" title="wikilink">渦輪方程式</a>》</p></td>
<td><p>Turbo<br />
渦寶</p></td>
<td><p>配音</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/降魔戰警.md" title="wikilink">降魔戰警</a>》</p></td>
<td><p>Nick Walker<br />
尼克·華克</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p>《<a href="../Page/百萬種硬的方式.md" title="wikilink">百萬種硬的方式</a>》</p></td>
<td><p>在酒吧的男子</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/血色孤語.md" title="wikilink">血色孤語</a>》</p></td>
<td><p>Jerry Hickfang<br />
傑瑞·希克范</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>Matthew Piper<br />
馬修·派伯</p></td>
<td><p>提名-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p>《》</p></td>
<td><p>Curtis<br />
柯蒂斯</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/名畫的控訴.md" title="wikilink">名畫的控訴</a>》</p></td>
<td><p>Randol Schoenberg<br />
</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/換命法則.md" title="wikilink">換命法則</a>》</p></td>
<td><p>Edward "Mark" Hale<br />
愛德華·「馬克」·海爾</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p>《<a href="../Page/惡棍英雄：死侍.md" title="wikilink">惡棍英雄：死侍</a>》</p></td>
<td><p>Wade Wilson／Deadpool<br />
韋德·威爾遜／死侍</p></td>
<td><p>兼監製<br />
<a href="../Page/土星獎最佳電影男主角.md" title="wikilink">土星獎最佳電影男主角</a><br />
<a href="../Page/MTV電影大獎.md" title="wikilink">MTV電影大獎最佳喜劇演出</a><br />
<a href="../Page/MTV電影大獎.md" title="wikilink">MTV電影大獎最佳打鬥</a><small>（與<a href="../Page/艾德·斯克林.md" title="wikilink">艾德·斯克林共享</a>）</small><br />
提名-<a href="../Page/MTV電影獎.md" title="wikilink">MTV電影獎最佳男演員</a><br />
提名-<a href="../Page/MTV電影獎.md" title="wikilink">MTV電影獎最佳接吻</a><br />
<a href="../Page/青少年票選獎.md" title="wikilink">青少年票選獎電影選擇獎：怒火戲</a><br />
提名-<a href="../Page/青少年票選獎.md" title="wikilink">青少年票選獎電影選擇獎：動作片男演員</a><br />
<br />
第22屆<a href="../Page/評論家選擇獎.md" title="wikilink">評論家選擇獎</a>：最佳喜劇類電影男主角<br />
提名-<a href="../Page/第74屆金球獎.md" title="wikilink">第74屆金球獎</a><a href="../Page/金球獎最佳音樂及喜劇類電影男主角.md" title="wikilink">音樂及喜劇類最佳男主角</a></p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/換腦行動.md" title="wikilink">換腦行動</a>》</p></td>
<td><p>Bill Pope<br />
比爾·波普</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p>《<a href="../Page/死侍：好心沒好報.md" title="wikilink">死侍：好心沒好報</a>》</p></td>
<td><p>Wade Wilson／Deadpool<br />
韋德·威爾遜／死侍</p></td>
<td><p>網路短片，《<a href="../Page/羅根_(電影).md" title="wikilink">羅根</a>》放映前的短片的加長版本</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/異星智慧.md" title="wikilink">異星智慧</a>》</p></td>
<td><p>Roy Adams<br />
羅伊·亞當斯</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/殺手保鑣.md" title="wikilink">殺手保鑣</a>》</p></td>
<td><p>Michael Bryce<br />
麥可·布萊斯</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p>《<a href="../Page/死侍2.md" title="wikilink">死侍2</a>》</p></td>
<td><p>Wade Wilson／Deadpool<br />
韋德·威爾遜／死侍<br />
Cain Marko／Juggernaut<br />
<a href="../Page/紅坦克_(漫威漫畫).md" title="wikilink">凱因·馬可／紅坦克</a></p></td>
<td><p>兼監製與動作捕捉和配音（紅坦克）</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/死侍2#發行.md" title="wikilink">從前有個死侍</a>》</p></td>
<td><p>《死侍2》的PG-13重映版</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019</p></td>
<td><p>《<a href="../Page/POKÉMON_名偵探皮卡丘.md" title="wikilink">POKÉMON 名偵探皮卡丘</a>》</p></td>
<td><p>Detective Pikachu<br />
<a href="../Page/名偵探皮卡丘.md" title="wikilink">名偵探皮卡丘</a></p></td>
<td><p>動作捕捉和配音</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/飛天遁地.md" title="wikilink">飛天遁地</a>》</p></td>
<td><p>One</p></td>
<td><p>兼監製</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>標題</p></th>
<th><p>角色</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1991–1993</p></td>
<td><p>《》</p></td>
<td><p>Billy Simpson<br />
比利·辛普森</p></td>
<td><p>又稱《Hillside》，65集<br />
提名-<a href="../Page/青年藝術家獎.md" title="wikilink">青年藝術家獎在一電視連續劇系列最佳年輕演員</a></p></td>
</tr>
<tr class="even">
<td><p>1993–1994</p></td>
<td><p>《》</p></td>
<td><p>Macro<br />
馬克羅</p></td>
<td><p>14集</p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p>《》</p></td>
<td><p>Derek Tillman<br />
德里克·蒂爾曼</p></td>
<td><p>單集：「」</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>Rick<br />
瑞克</p></td>
<td><p>單集：「The Heartbreak Kid」</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>Griffin<br />
格里芬</p></td>
<td><p>單集：「Redemption」</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p>《<a href="../Page/X檔案.md" title="wikilink">X檔案</a>》</p></td>
<td><p>Jay "Boom" DeBoom<br />
傑·迪布姆</p></td>
<td><p>單集：「」</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>Tony Hemingway<br />
東尼·海明威</p></td>
<td><p>單集：「Napping to Success」</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997–1998</p></td>
<td><p>《》</p></td>
<td><p>Paul Nodel<br />
保羅·諾德爾</p></td>
<td><p>2集</p></td>
</tr>
<tr class="odd">
<td><p>1998–2001</p></td>
<td><p>《》</p></td>
<td><p>Michael "Berg" Bergen<br />
</p></td>
<td><p>81集</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p>《<a href="../Page/實習醫生成長記.md" title="wikilink">實習醫生成長記</a>》</p></td>
<td><p>Spence<br />
史朋斯</p></td>
<td><p>單集：「My Dream Job」</p></td>
</tr>
<tr class="odd">
<td><p>2004–2005</p></td>
<td><p>《》</p></td>
<td><p>Ty Cheese<br />
起司</p></td>
<td><p>配音，14集</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p>《<a href="../Page/My_Boys.md" title="wikilink">My Boys</a>》</p></td>
<td><p>Hams<br />
漢斯</p></td>
<td><p>單集：「Douchebag in the City」</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>《<a href="../Page/週六夜現場.md" title="wikilink">週六夜現場</a>》</p></td>
<td><p>主持人</p></td>
<td><p>第三十五季（第660集）</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p>《<a href="../Page/居家男人.md" title="wikilink">居家男人</a>》</p></td>
<td><p>他自己</p></td>
<td><p>配音，單集：「Stewie Goes for a Drive」</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p>《<a href="../Page/頂級跑車秀.md" title="wikilink">頂級跑車秀</a>》</p></td>
<td><p>他自己</p></td>
<td><p>第十八季，單集：「Star in a Reasonably-Priced Car」</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/居家男人.md" title="wikilink">居家男人</a>》</p></td>
<td><p>Overweight Guy<br />
重量級蓋伊</p></td>
<td><p>配音，單集：「」</p></td>
<td></td>
</tr>
</tbody>
</table>

### MV

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>標題</p></th>
<th><p>歌手</p></th>
<th><p>角色</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2009</p></td>
<td><p>""</p></td>
<td></td>
<td><p>他自己</p></td>
<td><p>《<a href="../Page/週六夜現場.md" title="wikilink">週六夜現場</a>》主持人</p></td>
</tr>
</tbody>
</table>

## 參考文獻

## 外部連結

  -
  - [萊恩·雷諾斯在 HuffPost
    撰寫的文章](http://www.huffingtonpost.com/ryan-reynolds)

  - [萊恩的刀鋒戰士健身計畫](http://www.sixpacknow.com/ryan_reynolds_workout.html)

[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:加拿大電視演員](../Category/加拿大電視演員.md "wikilink")
[Category:加拿大電影演員](../Category/加拿大電影演員.md "wikilink")
[Category:加拿大男配音演員](../Category/加拿大男配音演員.md "wikilink")
[Category:電影監製](../Category/電影監製.md "wikilink")
[Category:溫哥華人](../Category/溫哥華人.md "wikilink")
[Category:20世紀加拿大男演員](../Category/20世紀加拿大男演員.md "wikilink")
[Category:昆特崙理工大學校友](../Category/昆特崙理工大學校友.md "wikilink")

1.
2.  <http://www.filmreference.com/film/48/Ryan-Reynolds.html>
3.
4.
5.  [Academy invites 135 new
    members](http://insidemovies.ew.com/2010/06/25/academy-new-members-bono-gabourey-sidibe)
6.
7.
8.
9.
10.
11.
12. <http://www.dailymotion.com/video/x22bfmv_deadpool-movie-test-footage-san-diego-comic-con-2014-vo-hq_shortfilms>
13. [1](https://twitter.com/VancityReynolds/status/540594148097916928)
14. [2](https://twitter.com/VancityReynolds/status/540594962463997952)
15.
16.
17.
18.
19. <http://www.people.com/people/article/0,,653651,00.html>
20. <http://www.findarticles.com/p/articles/mi_m1608/is_10_21/ai_n15955414>
21. . *Celebritybabies.people.com*. April 14, 2016.
22.
23.