**莱格尼察**（波兰语：；[{{IPA](../Page/Media:Pl-Legnica.ogg.md "wikilink")，原称*Lignica*；德语：**Liegnitz**，**里格尼茨**）是[波兰西南部城市](../Page/波兰.md "wikilink")，在[卡查瓦河](../Page/卡查瓦河.md "wikilink")（Kaczawa）沿岸，2005年时有106,122名居民。该市原为[莱格尼察省](../Page/莱格尼察省.md "wikilink")（1975-98年）首府，自从1999年以来改屬[下西里西亚省](../Page/下西里西亚省.md "wikilink")。在7世紀時就有人定居在這裡。城市在1004年首次被提及。是重要的工業都市。在二战期间，古城仅有少量建筑被破坏，可是1965年之后，大多数保存完好的精美建筑却被拆除，历史上的风格被摒弃，代之以现代风格。在冷戰期間，有大量蘇聯軍隊駐紮在這裡。礦業是萊格尼察的主要產業。

公元1241年蒙古軍於萊格尼察擊敗[波蘭軍隊及](../Page/波蘭王國_\(1025年–1385年\).md "wikilink")[條顿騎士團軍隊](../Page/條顿騎士團.md "wikilink")，為[列格尼卡戰役](../Page/列格尼卡戰役.md "wikilink")，此地亦為[蒙古帝國最西界](../Page/蒙古帝國.md "wikilink")。1760年，[七年战争中](../Page/七年战争.md "wikilink")，此地发生[李格尼茨战役](../Page/李格尼茨战役_\(1760年\).md "wikilink")。

## 外部链接

  - [莱格尼察网页](http://www.legnica.net.pl)
  - [The Academic Grammar School in Legnica](http://www.alo.legnica.pl)

[L](../Category/西里西亞.md "wikilink") [L](../Category/波兰城市.md "wikilink")