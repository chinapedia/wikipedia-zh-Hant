**萨博集团**（）是位于[瑞典的一家](../Page/瑞典.md "wikilink")[航空及](../Page/航空.md "wikilink")[武器制造商](../Page/武器.md "wikilink")，成立于1937年的Trollhättan。它的名字來自於瑞典飞机有限公司（）的缩写。在1939年，SAAB同总部位于[林雪平的飞机制造商ASJA公司合并](../Page/林雪平.md "wikilink")，并将总部搬迁到林雪平，在上世纪90年代，经过公司所有权的更变，名称变为SAAB
AB。

从上世纪40年代末起，SAAB为了在商业上更加多样化，产品从原有的飞机扩展至汽车，汽车分公司Saab
Automobile的总部位于Trollhättan。1969年萨博公司兼并了卡车制造商[斯堪尼亚公司](../Page/斯堪尼亚汽车.md "wikilink")，在1969至1995期间，这个公司被称为萨博－斯堪尼亚公司（Saab-Scania
AB）。

## 歷史

創立於1937年的SAAB，原名為"Svenska Aeroplan AB"公司，原為瑞典政府為維護二次大戰該國制空權，而成立的飛機製造厂。

起先SAAB只依特許合約承製[飛機](../Page/飛機.md "wikilink")，但該公司成立3年後，於1940年5月，製造了第一架屬於自己設計的飛機
- 即單引擎的「[SAAB
B17](../Page/SAAB_B17.md "wikilink")」。該型飛機根據所安裝引擎之不同，可被用為[戰鬥機](../Page/戰鬥機.md "wikilink")、[轟炸機](../Page/轟炸機.md "wikilink")、或[偵察機](../Page/偵察機.md "wikilink")。1942年誕生的SAAB
18，是當時全球速度最快的單引擎轟炸機，以SAAB當時的規模和成立年份看，[SAAB
18的誕生稱得上是重大成就](../Page/SAAB_18.md "wikilink")。1950年代初期，SAAB又推出一架噴射戰鬥機
- [SAAB
29](../Page/SAAB_29.md "wikilink")。該型飛機在韓戰中需求量非常大，因此SAAB再度獲得榮耀的機會。1955年SAAB再度大出風頭
- SAAB 29噴射戰鬥機以1,000公里的時速打破世界最快的紀錄。

从上世纪40年代末起，SAAB为了在商业上更加多样化，产品从原有的飞机扩展至汽车，汽车分公司Saab
Automobile的总部位于Trollhättan，第一辆车是1947年6月10日推出的[Saab
92001](../Page/Saab_92001.md "wikilink")。随着不斷的改良與精進，公司很快发展了在安全性和可靠性上的声誉。

在1950年代後期，SAAB公司的分公司DataSAAB冒险进入了电子计算机市场。

1960年代末期，由於越戰的爆發，世界政局再度牽動了SAAB的發展。當年儘管瑞典並未參戰，但瑞典國防部仍訂購了175架[SAAB
Vigge戰鬥機](../Page/SAAB_Vigge.md "wikilink")。Viggen曾是[SAAB
9-3高性能車型的名稱](../Page/SAAB_9-3.md "wikilink")。雖然目前SAAB飛機與SAAB汽車是由兩家公司製造，但SAAB汽車仍然反應出曾為飛機製造廠的獨特傳統與工程技術。

1969年，SAAB與1890年成立的[Scania-Vabis車廠合併成SAAB](../Page/Scania-Vabis.md "wikilink")-Scania集團，是世界上同時製造[飛機](../Page/飛機.md "wikilink")、[汽車](../Page/汽車.md "wikilink")、[卡車](../Page/卡車.md "wikilink")、[戰車](../Page/戰車.md "wikilink")、[火箭](../Page/火箭.md "wikilink")、[衛星](../Page/衛星.md "wikilink")、[電腦與](../Page/電腦.md "wikilink")[通訊等高科技產品的工業集團](../Page/通訊.md "wikilink")。
SAAB生產的維京式多功能超音速戰鬥機[J-37更曾名噪一時](../Page/J-37.md "wikilink")。

1989年，SAAB汽車部門從Saab-Scania分出，重組成一家獨立的公司：薩博汽車公司Saab Automobile AB

预计2017年撤销品牌。

## 產品

### 飞机

<File:SAAB> B 18B, 01.jpg|萨博18B (B 18B) <File:Lansen> 2.JPG|萨博32 Lansen
(J 32B) <File:Saab> 91C 01.jpg|萨博91C (Sk 50C) <File:J> 29F.jpg|萨博29
Tunnan (J 29F) [File:Viggen.JPG|萨博37](File:Viggen.JPG%7C萨博37) Viggen (SF
37) <File:JAS> Gripen.jpg|萨博Gripen (JAS 39) <File:FAA>
Saab340.jpg|萨博340B
[File:Saab340AEW\&C.jpg|萨博340](File:Saab340AEW&C.jpg%7C萨博340)
<File:Golden> Air SE-LTX
20071020.JPG|[萨博2000](../Page/萨博2000.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 参见

  - [萨博汽车](../Page/萨博汽车.md "wikilink")

[Category:瑞典軍工廠](../Category/瑞典軍工廠.md "wikilink")
[Category:瑞典飛機公司](../Category/瑞典飛機公司.md "wikilink")
[\*](../Category/萨博.md "wikilink")