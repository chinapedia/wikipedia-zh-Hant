**萨嘎县**（）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[西藏自治区](../Page/西藏自治区.md "wikilink")[日喀则市下属的一个](../Page/日喀则市.md "wikilink")[县](../Page/县级行政区.md "wikilink")\[1\]；[藏语中意思是](../Page/藏语.md "wikilink")“可爱的地方”。

## 歷史

公元1354年[帕木竹巴王朝建立了](../Page/帕木竹巴.md "wikilink")“萨嘎敦巴宗”，宗政府在萨嘎。1974年10月14日县政府正式迁至加加鎮。\[2\]

## 行政区划

下辖：\[3\] 。

## 参考资料

[萨嘎县](../Category/萨嘎县.md "wikilink")
[Category:日喀则地区县份](../Category/日喀则地区县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.
2.  [日喀则萨嘎县概况](http://www.xizang.gov.cn/xzly/zrdl/200611/t20061117_3542.html).
    西藏自治区人民政府
3.