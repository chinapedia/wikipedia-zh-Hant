**森姆·萊特**（，），[美國](../Page/美國.md "wikilink")[智力遊戲設計師](../Page/智力遊戲.md "wikilink")、[趣味數學家](../Page/趣味數學.md "wikilink")。

萊特發明了[移動15](../Page/移動15.md "wikilink")，並設計了不少[國際象棋棋題](../Page/國際象棋.md "wikilink")。其中最著名的棋題之一（見圖），白要於五步內將死黑\[1\]。這道被萊特稱為“Excelsior”的期題靈感來自[亨利·沃茲沃恩·朗費羅的](../Page/亨利·沃茲沃恩·朗費羅.md "wikilink")[同名詩](https://web.archive.org/web/20080430090202/http://rpo.library.utoronto.ca/poem/1325.html)。

萊特還是[七巧板的愛好者](../Page/七巧板.md "wikilink")，曾出版一本含七百個七巧板構圖的書。

## 書籍

  - *Sam Loyd's Book of Tangram Puzzles* (ISBN 0486220117)：森姆·萊特著
  - *Mathematical Puzzles of Sam Loyd* (ISBN 0486204987):
    [馬丁·葛登能編選](../Page/馬丁·葛登能.md "wikilink")
  - *More Mathematical Puzzles of Sam Loyd* (ISBN 0486207099)：馬丁·葛登能編選
  - *The Puzzle King: Sam Loyd's Chess Problems and Selected
    Mathematical Puzzles* (ISBN 1886846057): Sid Pickard編

## 參見

  - [亨利·杜德耐](../Page/亨利·杜德耐.md "wikilink")

## 外部連結

  - [The Cyclopedia of Puzzles](http://www.mathpuzzle.com/loyd/)：整本書逐頁印出
  - [Cyclopedia of Puzzles by Sam
    Loyd](http://www.cyclopediaofpuzzles.com/)：包括了書中的趣題
  - [Sam Loyd's Office - includes biography and his
    puzzles](http://www.samloyd.com/)

## 注释

<references/>

[Loyd](../Category/智力遊戲設計師.md "wikilink")

1.  1\. b4 Rc5+ 2. bxc5 a2 3. c6 Bc7 4. cxb7 any 5. bxa8=Q\#（或bxa8=B\#）。