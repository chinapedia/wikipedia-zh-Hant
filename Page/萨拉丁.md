**萨拉丁·优素福·伊本·阿尤布**（ / ：Ṣalāḥ ad-Dīn Yūsuf ibn Ayyūb； / ALC-LC：Selahedînê
Eyûbî；），通称**萨拉丁**
(Saladin，），[埃及](../Page/埃及.md "wikilink")[阿尤布王朝的第一位](../Page/阿尤布王朝.md "wikilink")[苏丹及](../Page/苏丹_\(称谓\).md "wikilink")[敘利亞的第一位苏丹](../Page/沙姆地区.md "wikilink")，1174年－1193年在位。[阿尤布王朝是得名自他父親Najm](../Page/阿尤布王朝.md "wikilink")
ad-Din Ayyub。

萨拉丁是[埃及歷史的](../Page/埃及歷史.md "wikilink")[民族英雄](../Page/民族英雄.md "wikilink")，因為他在[阿拉伯人對抗](../Page/阿拉伯人.md "wikilink")[十字軍東征的過程中](../Page/十字軍東征.md "wikilink")，表现出卓越的领袖作為、骑士风度、军事才能，闻名於[基督徒和](../Page/基督徒.md "wikilink")[穆斯林世界](../Page/穆斯林.md "wikilink")。他为人慷慨，从不吝惜钱财，死后的财产只有几个[第纳尔](../Page/第纳尔.md "wikilink")，還不够支付葬礼费用。

他是[庫爾德族的](../Page/庫爾德族.md "wikilink")[穆斯林](../Page/穆斯林.md "wikilink")\[1\]\[2\]\[3\]，也是首位成為[埃及及](../Page/埃及.md "wikilink")[敘利亞](../Page/敘利亞.md "wikilink")[蘇丹的庫爾德族人](../Page/蘇丹.md "wikilink")，[阿尤布王朝的建立者](../Page/阿尤布王朝.md "wikilink")，在中，領導穆斯林及阿拉伯人對抗來自[法國與](../Page/法國.md "wikilink")[歐洲的十字軍](../Page/歐洲.md "wikilink")。在權力最鼎盛時期，其蘇丹國包括了[埃及](../Page/埃及.md "wikilink")、[敘利亞](../Page/敘利亞.md "wikilink")、[美索不达米亚](../Page/美索不达米亚.md "wikilink")、[库尔德斯坦](../Page/库尔德斯坦.md "wikilink")、[希賈茲及](../Page/希賈茲.md "wikilink")[也門](../Page/也門.md "wikilink")。

在[哈丁戰役中](../Page/哈丁戰役.md "wikilink")，薩拉丁所領導的軍隊收复了自[法蒂瑪王朝期间被十字軍佔領長達八十八年的](../Page/法蒂瑪王朝.md "wikilink")[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")。雖然[耶路撒冷的十字軍王國仍持續了一段時期](../Page/耶路撒冷.md "wikilink")，但他們在[哈丁之役中的戰敗](../Page/哈丁戰役.md "wikilink")，成了這場[穆斯林與十字軍戰爭的轉捩點](../Page/穆斯林.md "wikilink")。也因此薩拉丁在[庫爾德族](../Page/庫爾德族.md "wikilink")、[阿拉伯及穆斯林文化中均享有盛名](../Page/阿拉伯人.md "wikilink")。薩拉丁是虔誠的[遜尼派穆斯林](../Page/遜尼派.md "wikilink")，但其義行及高貴的行為在基督教經典中也可發見，尤其是在[卡拉克攻防戰中的著墨甚多](../Page/卡拉克攻防戰.md "wikilink")；雖說是十字軍的敵人，也贏得了不少對方的敬重，例如[獅心王理查](../Page/理查一世.md "wikilink")。薩拉丁雖是歐洲人最憎惡的人物，但也是體現古老騎士精神的最好範例。

## 資料來源

現代與近代均可找到許多作品描述薩拉丁的生平，偏向正面評價的史學家有Al-Qadi
al-Fadil的作品[Ascalon](../Page/Ascalon.md "wikilink")，和[Imad al-Din
al-Isfahani與](../Page/Imad_al-Din_al-Isfahani.md "wikilink")，但[美索不達米雅的法學家](../Page/美索不達米亞.md "wikilink")[伊本·艾西尔](../Page/伊本·艾西尔.md "wikilink")（Ibn
al-Athir）作品裡的薩拉丁則呈現比較兇殘的樣貌。

## 统一阿拉伯

1138年，萨拉丁出生於[库尔德人的家庭](../Page/库尔德人.md "wikilink")，地點在[底格里斯河边的](../Page/底格里斯河.md "wikilink")[提克里特](../Page/提克里特.md "wikilink")（今[伊拉克](../Page/伊拉克.md "wikilink")[萨拉赫丁省](../Page/萨拉赫丁省.md "wikilink")[省会](../Page/省会.md "wikilink")）。他前往[大马士革完成学业](../Page/大马士革.md "wikilink")，並在国王的宮廷服务十年，以其对[逊尼派经文的兴趣而著名](../Page/逊尼派.md "wikilink")。

1160年，努尔丁派遣萨拉丁的叔父[谢尔库赫去埃及作战](../Page/谢尔库赫.md "wikilink")，他随行学习军事。1169年，他取代[法蒂玛王朝的](../Page/法蒂玛王朝.md "wikilink")[哈里发和他叔父成为埃及总督](../Page/哈里发.md "wikilink")，那時[耶路撒冷的拉丁人国王](../Page/耶路撒冷.md "wikilink")[阿马尔里克正要入侵埃及](../Page/阿马尔里克_\(耶路撒冷国王\).md "wikilink")。

起初，萨拉丁的政权很不穩固，没人认为他能长久待在埃及。因为，在之前多次的权力更迭裡，宰相们勾心斗角扶植許多哈里发繼承人。其次，萨拉丁领导的是外国军队，無力控制埃及的[什叶派军队](../Page/什叶派.md "wikilink")，后者仍效忠失势的法蒂玛王朝哈里发。再者，萨拉丁雖掌控統治權，但名义上附属于[巴格达](../Page/巴格达.md "wikilink")[阿拔斯王朝的哈里发](../Page/阿拔斯王朝.md "wikilink")，以及[赞吉王朝苏丹努尔丁](../Page/赞吉王朝.md "wikilink")。1171年9月，努尔丁去世，萨拉丁受[近卫军支持发动](../Page/近卫军.md "wikilink")[政变](../Page/政变.md "wikilink")。1174年成功奪權，穆斯林教长们宣布他繼位为苏丹。[阿尤布王朝從此建立](../Page/阿尤布王朝.md "wikilink")。

其后，萨拉丁收复[叙利亚和](../Page/叙利亚.md "wikilink")[两河流域的大部分](../Page/两河流域.md "wikilink")，发动[圣战抵抗](../Page/宗教战争.md "wikilink")[十字军](../Page/十字军.md "wikilink")。1187年，他俘虏[耶路撒冷国王](../Page/耶路撒冷.md "wikilink")[居伊和](../Page/吕西尼昂的居伊.md "wikilink")[圣殿骑士团团长](../Page/圣殿骑士团.md "wikilink")，重新收复圣城耶路撒冷。欧洲震動，導致其[第三次十字军东征](../Page/第三次十字军东征.md "wikilink")。1192年9月，雙方签定三年三个月的和约，[狮心王理查带著](../Page/狮心王理查.md "wikilink")[真十字架撤退](../Page/真十字架.md "wikilink")，約定三年后回来一决雌雄。但两人皆未實现诺言。1193年3月4日，萨拉丁病逝於[大马士革](../Page/大马士革.md "wikilink")。1199年4月6日，[狮心王理查戰場中箭後死于伤口感染](../Page/狮心王理查.md "wikilink")。

## 對西方世界的影響

在十九世紀，萨拉丁因為其對抗十字軍以及其慷慨，有著[騎士精神的形像](../Page/騎士精神.md "wikilink")。萨拉丁在[中古時期並不出名](../Page/中古時期.md "wikilink")，但在[戈特霍尔德·埃夫莱姆·莱辛的劇本](../Page/戈特霍尔德·埃夫莱姆·莱辛.md "wikilink")《[智者納坦](../Page/智者納坦.md "wikilink")》(1779)及[沃尔特·司各特的小說](../Page/沃尔特·司各特.md "wikilink")《》(1825)有比較正面的形象。當代對萨拉丁是以這些文學作品為準。依照的說法，司各特對萨拉丁的描述是：「十九世紀自由派的歐洲人，但中古時期的西方人會對其印象不佳。」\[4\]。十字军在1099年攻克耶律撒冷時曾进行屠杀，但萨拉丁大赦天主教徒，也讓一般的天主教徒自由通行，甚至是戰敗的基督教軍隊，只要可以付出事先提出的贖金即可（[正教會因為反對十字軍](../Page/正教會.md "wikilink")，萨拉丁對正教會的待遇更好。）的小說《The
Book of Saladin》提出了有關萨拉丁及當時世界的有趣觀點\[5\]。

儘管萨拉丁和基督教的君主的信仰不同，但萨拉丁獲得了基督教君主的尊敬，特別是[獅心王理查](../Page/獅心王理查.md "wikilink")。理查曾稱讚萨拉丁是位偉大的王子，是伊斯蘭世界最偉大也最有權力的國王\[6\]。萨拉丁也說沒有任何一個基督教的國王比獅心王理查偉大。在二人簽訂條約後，雙方互贈許多禮物以表尊敬，但兩人再也沒有再見面。在1191年4月，一位法蘭克婦人的三個月嬰兒從帳篷中被偷走，被賣到市場中。獅心王理查要她去找萨拉丁表達其委屈。根據Bahā'
al-Dīn的敘述，萨拉丁自己出錢將嬰兒贖了回來。

## 對伊斯兰教世界的影響

[Coat_of_arms_of_Egypt_(Official).svg](https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Egypt_\(Official\).svg "fig:Coat_of_arms_of_Egypt_(Official).svg")上的[薩拉丁之鹰](../Page/薩拉丁之鹰.md "wikilink")\]\]
[Coat_of_Arms_of_Kurdistan.svg](https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Kurdistan.svg "fig:Coat_of_Arms_of_Kurdistan.svg")

1898年[德意志皇帝](../Page/德意志皇帝.md "wikilink")[威廉二世造訪萨拉丁的墓](../Page/威廉二世_\(德国\).md "wikilink")，表示其敬意\[7\]。此次的造訪再加上反帝國主義的情緒，讓阿拉伯的民族主義者將萨拉丁視為對抗西方的英雄。他們認知的萨拉丁形象是由[沃尔特·司各特及當時的歐洲人創造的浪漫角色](../Page/沃尔特·司各特.md "wikilink")，忽略了萨拉丁本身是[库尔德人的事實](../Page/库尔德人.md "wikilink")
\[8\]

現代的阿拉拍國家用各種方式紀念萨拉丁，多半也是以十九世紀西方所描繪出來的形象為基礎\[9\]。像[伊拉克行政區劃中包括](../Page/伊拉克行政區劃.md "wikilink")[薩邁拉及](../Page/薩邁拉.md "wikilink")[提克里特的](../Page/提克里特.md "wikilink")[萨拉赫丁省都是因他而得名](../Page/萨拉赫丁省.md "wikilink")，而[伊拉克库尔德斯坦最大城市](../Page/伊拉克库尔德斯坦.md "wikilink")[艾比爾的](../Page/艾比爾.md "wikilink")也是紀念萨拉丁。

有些和薩拉丁有關的建築物仍存留在現在的城市中，薩拉丁最早強化的[開羅城堡](../Page/開羅大城堡區.md "wikilink")（1175–1183）有圓頂，景觀相當優美。在[敘利亞即使最小的城市也有一個可以抵抗入侵的](../Page/敘利亞.md "wikilink")[城塞](../Page/城塞.md "wikilink")，薩拉丁將這個設施引入埃及。

雖然[阿尤布王朝在萨拉丁死後只多存留了](../Page/阿尤布王朝.md "wikilink")57年，但有關萨拉丁的事蹟仍在[阿拉伯世界中流傳](../Page/阿拉伯世界.md "wikilink")，一直到今天。隨著20世紀[阿拉伯民族主义的興起](../Page/阿拉伯民族主义.md "wikilink")，部份原因也和[阿以冲突有關](../Page/阿以冲突.md "wikilink")，萨拉丁的英雄事蹟成為新的記號。萨拉丁從[十字軍手中奪回](../Page/十字軍東征.md "wikilink")[巴勒斯坦的事蹟](../Page/巴勒斯坦.md "wikilink")，用來鼓舞今日的阿拉伯人，對抗讓猶太人在巴勒斯坦重建[以色列的](../Page/以色列.md "wikilink")[錫安主義](../Page/錫安主義.md "wikilink")。而且薩拉丁時期阿拉伯的統一及榮耀也成為阿拉伯民族主义（如[贾迈勒·阿卜杜-纳赛尔](../Page/贾迈勒·阿卜杜-纳赛尔.md "wikilink")）一個完美的符號。像[埃及](../Page/埃及.md "wikilink")1984年後的[國徽上就有代表薩拉丁的](../Page/埃及國徽.md "wikilink")[薩拉丁之鹰](../Page/薩拉丁之鹰.md "wikilink")、而[阿聯](../Page/阿拉伯聯合酋長國國徽.md "wikilink")、[伊拉克](../Page/伊拉克國徽.md "wikilink")
[巴勒斯坦民族權力機構及](../Page/巴勒斯坦民族權力機構徽章.md "wikilink")[葉門的國徽都有類似的薩拉丁之鹰](../Page/葉門國徽.md "wikilink")。

## 相關電影作品

  - 《》（*Al Nasser Salah
    Ad-Din*），1963年上映，講述薩拉丁與十字軍爭奪耶路撒冷的戰爭故事，由埃及導演[尤賽夫·夏因拍攝](../Page/尤賽夫·夏因.md "wikilink")，由主演。
  - 《[-{zh-hans:天国王朝;zh-hk:天國驕雄;zh-tw:王者天下;}-](../Page/天国王朝.md "wikilink")》（*Kingdom
    of Heaven*），2005年上映，講述耶路撒冷保衛戰的故事，薩拉丁由Ghassan Massoud扮演。

## 軟體遊戲中的薩拉丁

[微軟旗下知名的](../Page/微軟.md "wikilink")《[世紀帝國II：帝王世紀](../Page/世紀帝國II：帝王世紀.md "wikilink")》中的劇情，以一名被俘虜的[十字軍成員的口中](../Page/十字軍.md "wikilink")，傳述薩拉丁跟他麾下的[伊斯蘭戰士](../Page/伊斯蘭.md "wikilink")，比[歐洲的任何貴族更注重](../Page/歐洲.md "wikilink")[騎士的精神](../Page/騎士.md "wikilink")，用來諷刺[十字軍東征的歐洲人因宗教狂熱而引起的嗜血](../Page/十字軍東征.md "wikilink")。在《[世紀帝國2帝王世紀](../Page/世紀帝國2.md "wikilink")》中，有關十字軍東征的全線劇情，除了薩拉丁之外，另外一個是巴巴羅薩（「[紅鬍子](../Page/腓特烈一世_\(神聖羅馬帝國\).md "wikilink")」[腓特烈一世](../Page/腓特烈一世_\(神圣罗马帝国\).md "wikilink")），在｢巴巴羅薩｣最後的主線劇情中，將國王的遺體放在醋桶中，運到圓頂清真寺，算是達成國王到達耶路撒冷的夢想。

## 相关作品

  - [薩拉丁之鹰](../Page/薩拉丁之鹰.md "wikilink")
      - [阿拉伯国家国徽列表](../Page/阿拉伯国家国徽列表.md "wikilink")
  - 《[天国王朝](../Page/天国王朝.md "wikilink")》
  - 《》
  - 《》

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
## 参见

  -
  -
  -
## 外部連結

  - [Stanley Lane-Poole, "The Life of Saladin and the Fall of the
    Kingdom of Jerusalem", in "btm"
    format](https://web.archive.org/web/20090304191952/http://www.third-millennium-library.com/readinghall/GalleryofHistory/Saladin/LIFE-DOOR.html)
  - [Rosebault Ch.J. Saladin. Prince of
    Chivalri](http://www.medievalist.globalfolio.net/eng/r/rosebault_saladin/_content.php)
  - [De expugnatione terrae sanctae per
    Saladinum](http://www.fh-augsburg.de/~harsch/Chronologia/Lspost12/Libellus/lib_expu.html#1)
    A European account of Saladin's conquests of the Crusader states.
  - [Saladin: The Sultan and His
    Times, 1138–1193](http://www.press.jhu.edu/books/title_pages/9465.html)
  - [Richard and Saladin: Warriors of the Third
    Crusade](http://www.shadowedrealm.com/articles/exclusive/richard_saladin_warriors_third_crusade)

{{-}}

[Category:敘利亞歷史](../Category/敘利亞歷史.md "wikilink")
[Category:埃及君主](../Category/埃及君主.md "wikilink")
[Category:1193年逝世](../Category/1193年逝世.md "wikilink")
[Category:伊拉克库尔德人](../Category/伊拉克库尔德人.md "wikilink") [Category:苏丹
(称谓)](../Category/苏丹_\(称谓\).md "wikilink")

1.  A number of contemporary sources make note of this. The biographer
    Ibn Khallikan writes, "Historians agree in stating that
    \[Saladin's\] father and family belonged to Duwin \[Dvin\]. ... They
    were Kurds and belonged to the Rawādiya (sic), which is a branch of
    the great tribe al-Hadāniya": Minorsky (1953), p. 124. The medieval
    historian Ibn Athir, who is a Kurd and therefore his credibility is
    questionable, relates a passage from another commander: "... both
    you and Saladin are Kurds and you will not let power pass into the
    hands of the Turks": Minorsky (1953), p. 138.
2.
3.
4.  Riley Smith, Jonathan, "The Crusades, Christianity and Islam",
    (Columbia 2008), p. 67
5.  (London: Verso, 1998)
6.
7.  The Kaiser laid a wreath on the tomb baring the inscription, "A
    Knight without fear or blame who often had to teach his opponents
    the right way to practice chivalry." Grousset (1970).
8.  Riley Smith, Jonathan, "The Crusades, Christianity and Islam",
    (Columbia 2008), p. 63-66
9.  Madden, Thomas F.: *The Concise History of the Crusades*; 3rd
    edition, Rowman & Littlefield, 2013. pp. 201-204.