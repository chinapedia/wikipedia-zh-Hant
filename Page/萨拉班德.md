**萨拉班德**（，又稱**薩拉邦德**)是一种庄严的[西班牙舞曲](../Page/西班牙.md "wikilink")，是缓慢的三[拍子](../Page/拍子.md "wikilink")，其中第二拍为强拍。

萨拉班德最早是被[巴拿马](../Page/巴拿马.md "wikilink")[诗人麦克西亚于](../Page/诗人.md "wikilink")1539年在其[诗歌中提到的一种](../Page/诗歌.md "wikilink")[舞蹈](../Page/舞蹈.md "wikilink")\[1\]，这种舞蹈在西班牙的[南美洲殖民地中风行](../Page/南美洲.md "wikilink")，后来传播回西班牙，但在1583年因为认为其猥亵而被禁止，在当时的[文学作品中常被提起](../Page/文学.md "wikilink")，例如在[塞万提斯和](../Page/塞万提斯.md "wikilink")[维加的作品中](../Page/维加.md "wikilink")。

在[巴洛克时期](../Page/巴洛克音樂.md "wikilink")，萨拉班德常被用作组曲中，一般都放在第三乐章，例如[巴赫的](../Page/巴赫.md "wikilink")[无伴奏大提琴组曲](../Page/无伴奏大提琴组曲.md "wikilink")，这时的萨拉班德比在西班牙流行时期更缓慢，适合[欧洲宫廷的舞蹈](../Page/欧洲.md "wikilink")。在20世纪时，萨拉班德重新风行，在[克劳德·德彪西和](../Page/克劳德·德彪西.md "wikilink")[埃里克·薩蒂的作品中都有应用](../Page/埃里克·薩蒂.md "wikilink")。

## 参考文献

<div class="references-small">

  -

</div>

## 外部链接

  - [萨拉班德舞曲欣赏](http://qk.cn010w.com/quku_player.asp?pf_id=9008&lb_id=14)

[Category:乐曲形式](../Category/乐曲形式.md "wikilink")
[Category:巴洛克舞蹈](../Category/巴洛克舞蹈.md "wikilink")

1.