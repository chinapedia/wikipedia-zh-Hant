**[萨摩亚国旗](../Page/萨摩亚.md "wikilink")**于1949年2月24日作为[联合国托管西萨摩亚的区旗而制定](../Page/萨摩亚#历史.md "wikilink")，1962年1月1日萨摩亚独立时被采用为国旗。旗地为红色，左上方的蓝色长方形内绘有[南十字座](../Page/南十字座.md "wikilink")。1948年设计当初，[南十字座和](../Page/南十字座.md "wikilink")[新西兰的国旗一样仅有四颗星](../Page/新西兰国旗.md "wikilink")，1949年追加了第五个星。红色象征忠诚、白色象征纯洁、蓝色表示爱国心。

萨摩亚国旗与[青天白日滿地紅旗](../Page/中華民國國旗.md "wikilink")（中华民国国旗\[1\]）相似。

## 歷代國旗

### 歷代正式國旗

### 曾作為或拟议的國旗

## 相似旗帜

## 外部链接

  -
  - [World Statesmen - Samoa](http://www.worldstatesmen.org/Samoa.html)

[Category:国旗](../Category/国旗.md "wikilink")
[Category:萨摩亚](../Category/萨摩亚.md "wikilink")
[Category:1949年面世的旗幟](../Category/1949年面世的旗幟.md "wikilink")

1.  [青天白日滿地紅旗](../Page/中華民國國旗#國旗使用歷程.md "wikilink")，最初自1912年（中華民國元年）時即為官定中華民國海軍旗。1921年孫中山曾提出主張以此為中華民國國旗。1928年中國[國民革命軍北伐結束後](../Page/國民革命軍北伐.md "wikilink")，此旗幟始在中國大陸廣泛使用。青天白日滿地紅旗現今仍為[臺灣、澎湖、金門、馬祖等地的官方正式旗幟](../Page/臺灣地區.md "wikilink")。