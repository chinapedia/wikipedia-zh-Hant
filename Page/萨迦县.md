**萨迦县**（）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[西藏自治区](../Page/西藏自治区.md "wikilink")[日喀则市下属的一个](../Page/日喀则市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。[藏语中意思是](../Page/藏语.md "wikilink")“灰白土”。

## 行政区划

下辖：\[1\] 。

## 参考资料

[萨迦县](../Category/萨迦县.md "wikilink")
[Category:日喀则地区县份](../Category/日喀则地区县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.