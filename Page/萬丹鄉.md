**萬丹鄉**（下淡水、Tampsuy）位於[臺灣](../Page/臺灣.md "wikilink")[屏東縣西南方](../Page/屏東縣.md "wikilink")，西臨[高屏溪](../Page/高屏溪.md "wikilink")（下淡水溪）與[高雄市為界](../Page/高雄市.md "wikilink")，北鄰[屏東市](../Page/屏東市.md "wikilink")，東以[隘寮溪和](../Page/隘寮溪.md "wikilink")[竹田鄉分界](../Page/竹田鄉.md "wikilink")，南接[新園鄉和](../Page/新園鄉_\(台灣\).md "wikilink")[崁頂鄉](../Page/崁頂鄉.md "wikilink")，東北方接[麟洛鄉](../Page/麟洛鄉.md "wikilink")，地處[屏東平原](../Page/屏東平原.md "wikilink")，全鄉地勢平坦。

清朝時期的幾大渡船口皆在萬丹轄區內，進出的人口眾多因而逐漸繁榮，即有一說法萬丹地名來自於「萬舟雲集」，成為屏東平原上第一個「街市」，故有句臺語古諺「屏東古早係阿猴，萬丹係街仔頭」說明了萬丹的起源特色。

萬丹鄉地理位置優越，鄰近屏東市、潮州、東港及高雄市，亦是屏東縣的交通重鎮之一。

居民以[閩南人為大多數](../Page/閩南裔台灣人.md "wikilink")，[客家人約佔兩成左右](../Page/客家裔台灣人.md "wikilink")\[1\]，散居鄉內。

本鄉為全縣人口第四多之鄉鎮，約五萬人，僅次於[縣治屏東市](../Page/縣治.md "wikilink")、[內埔鄉及](../Page/內埔鄉.md "wikilink")[潮州鎮](../Page/潮州鎮.md "wikilink")，人口則主要分布於市中心及西側的社皮、崙頂、及後-{庄}-等地。

## 歷史沿革

萬丹鄉在[臺灣清治時期](../Page/臺灣清治時期.md "wikilink")，屬於[鳳山縣所轄](../Page/鳳山縣_\(臺灣\).md "wikilink")。[臺灣日治時期初為](../Page/臺灣日治時期.md "wikilink")[阿猴廳港西下里](../Page/阿猴廳.md "wikilink")，[大正九年](../Page/大正.md "wikilink")（1920年）改屬[高雄州](../Page/高雄州.md "wikilink")[東港郡](../Page/東港郡.md "wikilink")-{[萬丹庄](../Page/萬丹庄.md "wikilink")}-。戰後屬[高雄縣](../Page/高雄縣.md "wikilink")，1946年改隸[屏東市管轄](../Page/屏東市_\(省轄市\).md "wikilink")，設萬丹區，1950年10月，改為屏東縣萬丹鄉迄今。廣闊肥沃的平原，翠綠的農作物覆蓋著美麗的大地，藍天綠野構成萬丹鄉優雅純樸的景緻。

## 歷屆鄉長

<table>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>任別</p></th>
<th><p>姓名</p></th>
<th><p>黨籍</p></th>
<th><p>到任日期</p></th>
<th><p>卸任日期</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>1</p></td>
<td><p><a href="../Page/林得.md" title="wikilink">林得</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2</p></td>
<td><p><a href="../Page/陳天豹.md" title="wikilink">陳天豹</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>3</p></td>
<td><p><a href="../Page/李子毓.md" title="wikilink">李子毓</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>4</p></td>
<td><p><a href="../Page/黃昌臨.md" title="wikilink">黃昌臨</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>5</p></td>
<td><p><a href="../Page/伍金井.md" title="wikilink">伍金井</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>6</p></td>
<td><p><a href="../Page/陳財.md" title="wikilink">陳財</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>7</p></td>
<td><p>|<a href="../Page/林枝讚.md" title="wikilink">林枝讚</a></p></td>
<td></td>
<td></td>
<td><p>1999年8月9日</p></td>
<td><p>鄉長林枝讚因賄選案被判處有期徒刑六個月，得易科罰金，並褫奪公權三年，依法解職</p></td>
</tr>
<tr class="even">
<td><p>代理</p></td>
<td><p><a href="../Page/沈商嶽.md" title="wikilink">沈商嶽</a></p></td>
<td></td>
<td><p>1999年8月10日</p></td>
<td></td>
<td><p>屏東縣政府指派代理鄉長</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p><a href="../Page/林正峰.md" title="wikilink">林正峰</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>8</p></td>
<td><p><a href="../Page/林正峰.md" title="wikilink">林正峰</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>9</p></td>
<td><p><a href="../Page/郭寶聯.md" title="wikilink">郭寶聯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>10</p></td>
<td><p><a href="../Page/劉昭相.md" title="wikilink">劉昭相</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政區域

[Wandan_villages2.svg](https://zh.wikipedia.org/wiki/File:Wandan_villages2.svg "fig:Wandan_villages2.svg")

  - 上村村、四維村、社中村、廈北村、萬後村、磚寮村
  - 水仙村、加興村、社皮村、廈南村、萬惠村、興安村
  - 水泉村、竹林村、香社村、萬生村、新庄村、興全村
  - 田厝村、社上村、後村村、萬安村、新鐘村、寶厝村
  - 甘棠村、社口村、崙頂村、萬全村、廣安村、灣內村

## 人口

## 警政治安

  - [屏東縣警察局屏東分局](http://www.ptpolice.gov.tw/_Branches/pingdung/Default.aspx)
      - 萬丹分駐所：屏東縣萬丹鄉萬安村萬丹路136號。Tel:(08)7772004
      - 社皮派出所：屏東縣萬丹鄉社口村社皮路二段399號。Tel:(08)7073044
      - 新鐘派出所：屏東縣萬丹鄉新鐘村新鐘路64號。Tel:(08)7772254

## 教育

### 國民中學

  - [屏東縣立萬丹國民中學](http://www.wtjh.ptc.edu.tw/)
  - [屏東縣立萬新國民中學](../Page/屏東縣立萬新國民中學.md "wikilink")
    [校網](http://www.wsjh.ptc.edu.tw/)

### 國民小學

  - [屏東縣萬丹鄉萬丹國民小學](https://web.archive.org/web/20061116210321/http://www.wtps.ptc.edu.tw/home.htm)
  - 屏東縣萬丹鄉萬丹國民小學竹林分校
  - [屏東縣萬丹鄉新庄國民小學](http://www.sjps.ptc.edu.tw/)
  - [屏東縣萬丹鄉廣安國民小學](http://www.kaps.ptc.edu.tw/)
  - [屏東縣萬丹鄉興華國民小學](http://www.shps.ptc.edu.tw/)
  - [屏東縣萬丹鄉四維國民小學](http://www.swps.ptc.edu.tw/)
  - [屏東縣萬丹鄉社皮國民小學](http://www.spps.ptc.edu.tw/)
  - [屏東縣萬丹鄉新興國民小學](http://www.sses.ptc.edu.tw/)
  - [屏東縣萬丹鄉興化國民小學](http://www.shes.ptc.edu.tw/)

## 交通

### 公路

  - 省道

<!-- end list -->

  - ：[屏東市](../Page/屏東市.md "wikilink") - 萬丹鄉 -
    [新園鄉](../Page/新園鄉_\(台灣\).md "wikilink")

  - ：[大寮區](../Page/大寮區.md "wikilink") - 萬丹鄉 -
    [竹田鄉](../Page/竹田鄉.md "wikilink")

      - [萬大大橋](../Page/萬大大橋.md "wikilink")
      - [萬丹交流道](../Page/萬丹交流道.md "wikilink")（15k，接）

<!-- end list -->

  - 縣道

<!-- end list -->

  - （原。2014年1月10日併編）

  - （原。2014年1月10日併編）

  - ：[大寮區](../Page/大寮區.md "wikilink") - 萬丹鄉 -
    [竹田鄉](../Page/竹田鄉.md "wikilink")

      - [萬大大橋](../Page/萬大大橋.md "wikilink")

  - ：[屏東市](../Page/屏東市.md "wikilink") - 萬丹鄉 -
    [潮州鎮](../Page/潮州鎮.md "wikilink")

      - ：[屏東市](../Page/屏東市.md "wikilink") - 萬丹鄉（原【加工區聯外道路】。2014年5月1日編入）

<!-- end list -->

  - 鄉道

<!-- end list -->

  -   -
  -
  -
  -
  -
  -   -
  -
  -
  -
  -   -
  -
  -
## 旅遊

  - 鯉魚山泥火山：鯉魚山位於萬丹鄉與新園鄉交界，灣仔內-{庄}-外的小山丘，因貌似鯉魚所以稱為鯉魚山。
  - 萬丹-威聖會
  - 萬丹教會
  - 石翠蘭花園
  - 萬丹百年老街
  - 赤山巖
  - 社皮夜市(週三開放)
  - 萬丹公園
  - 阿國臭豆腐
  - 趙記肉粽
  - 街頭牛肉爐
  - 王品羊肉
  - 萬丹紅豆餅店
  - 新興宮
  - 嘉新宮
  - 新庄夜市(每週五營業)
  - 萬丹肉粽珍
  - 崙頂河堤公園
  - 萬丹商展（夜市）(二、四、日開放)
  - 萬丹街頭角保全宮，主神奉祀保生大帝
  - 萬丹萬福宮陳府城隍爺廟
  - [萬泉寺歷史悠久的建築](../Page/萬丹萬泉寺.md "wikilink")，奉祀玄天上帝
  - 萬丹街後角大憲宮：主神奉祀：大使爺，二使爺
  - 萬丹竹巷口竹蕙宮：主神奉祀：伍府千歲
  - 萬丹市仔口眾心堂：主神奉祀：廣澤尊王
  - 萬丹頭前厝農惠宮：主神奉祀：神農大帝
  - 萬安古地：主神奉祀：福德正神，文昌帝君
  - 萬丹街頭角福德祠：主神奉祀：福德正神
  - 萬丹寶長厝保榮宮：主神奉祀：五府千歲
  - 萬丹澎湖厝救世堂：主神奉祀：池府三千歲
  - 犁頭碑（萬丹碑）
  - [萬惠宮](../Page/萬惠宮.md "wikilink")：肇建於清[乾隆](../Page/乾隆.md "wikilink")21年，1921年（大正十年）因地震震毀重修時，聘請[台灣的黃龜理與](../Page/台灣.md "wikilink")[廈門的楊秀興兩派匠師](../Page/廈門.md "wikilink")，各憑技藝進行[對場作](../Page/對場作.md "wikilink")，萬惠宮媽祖廟是萬丹人的信仰中心，也是屏東縣最有文化價值的廟宇之一。

## 特產

[萬丹鄉四維村的紅豆節與紅豆像.JPG](https://zh.wikipedia.org/wiki/File:萬丹鄉四維村的紅豆節與紅豆像.JPG "fig:萬丹鄉四維村的紅豆節與紅豆像.JPG")

  - [紅豆](../Page/紅豆.md "wikilink")
  - [苦瓜](../Page/苦瓜.md "wikilink")
  - [絲瓜](../Page/絲瓜.md "wikilink")
  - [牛奶](../Page/牛奶.md "wikilink")
  - [毛豆](../Page/毛豆.md "wikilink")
  - [小麥](../Page/小麥.md "wikilink")

## 名人

  - [張山鐘](../Page/張山鐘.md "wikilink")：首任民選[屏東縣縣長](../Page/屏東縣縣長.md "wikilink")。
  - [張豐緒](../Page/張豐緒.md "wikilink")：前[屏東縣縣長](../Page/屏東縣縣長.md "wikilink")、[臺北市長](../Page/臺北市長.md "wikilink")、[內政部長](../Page/內政部長.md "wikilink")，張山鐘之子。
  - [陳命珠](../Page/陳命珠.md "wikilink")：[中華民國第一位作錢幣主角的平民](../Page/中華民國.md "wikilink")。
  - [伍澤元](../Page/伍澤元.md "wikilink")、[伍錦霖兄弟](../Page/伍錦霖.md "wikilink")：前[屏東縣縣長](../Page/屏東縣縣長.md "wikilink")、[考試院長](../Page/考試院長.md "wikilink")。
  - [王柏融](../Page/王柏融.md "wikilink")：[Lamigo桃猿職棒選手](../Page/Lamigo桃猿.md "wikilink")。
  - [李金木](../Page/李金木.md "wikilink")：[Lamigo桃猿職棒選手](../Page/Lamigo桃猿.md "wikilink")。
  - [郭建宏](../Page/郭建宏.md "wikilink")：[La
    New熊隊](../Page/La_New熊隊.md "wikilink")、[兄弟象隊](../Page/兄弟象隊.md "wikilink")、[統一7-ELEVEn獅隊職棒選手](../Page/統一7-ELEVEn獅隊.md "wikilink")。

## 參考資料

## 外部連結

  - [萬丹鄉公所](http://www.pthg.gov.tw/TownWto/)
  - [萬新國中](http://www.wsjh.ptc.edu.tw/)
  - [萬丹國中](http://www.wtjh.ptc.edu.tw/)
  - [萬丹鄉音](http://bantan.pt.pct.org.tw/)

[Category:屏東縣行政區劃](../Category/屏東縣行政區劃.md "wikilink")
[萬丹鄉](../Category/萬丹鄉.md "wikilink")

1.