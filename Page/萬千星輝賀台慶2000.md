**[萬千星輝賀台慶](../Page/萬千星輝賀台慶.md "wikilink")**2000，於2000年11月19日清水灣電視城一廠直播。該年除節目表演及抽獎遊戲外，於節目期間內舉行電視頒奬禮，頒發共5個獎項。由[沈殿霞](../Page/沈殿霞.md "wikilink")、[汪明荃](../Page/汪明荃.md "wikilink")、[曾志偉](../Page/曾志偉.md "wikilink")、[陳百祥](../Page/陳百祥.md "wikilink")、[顧紀筠](../Page/顧紀筠.md "wikilink")、[郭羨妮](../Page/郭羨妮.md "wikilink")、[鄧梓峰](../Page/鄧梓峰.md "wikilink")、[谷德昭主持](../Page/谷德昭.md "wikilink")。\[1\]。

## 提名及獲獎名單

### 本年度我最喜愛的男主角

|    |                                                      |                                          |         |
| -- | ---------------------------------------------------- | ---------------------------------------- | ------- |
| 編號 | 劇集                                                   | 演員                                       | 角色      |
| 01 | [茶是故鄉濃](../Page/茶是故鄉濃.md "wikilink")                 | [林家棟](../Page/林家棟.md "wikilink")         | 方有為     |
| 02 | [洗冤錄](../Page/洗冤錄.md "wikilink")                     | [歐陽震華](../Page/歐陽震華.md "wikilink")       | 宋　慈     |
| 03 | [洗冤錄](../Page/洗冤錄.md "wikilink")                     | [林文龍](../Page/林文龍.md "wikilink")         | 宋　翊/方　俊 |
| 04 | [無業樓民](../Page/無業樓民.md "wikilink")                   | [劉松仁](../Page/劉松仁.md "wikilink")         | 洪大龍     |
| 05 | [無業樓民](../Page/無業樓民.md "wikilink")                   | [江　華](../Page/江華_\(香港演員\).md "wikilink") | 凌希斯     |
| 06 | [撻出愛火花](../Page/撻出愛火花.md "wikilink")                 | [謝霆鋒](../Page/謝霆鋒.md "wikilink")         | 趙家俊     |
| 07 | [緣份無邊界](../Page/緣份無邊界.md "wikilink")                 | [林保怡](../Page/林保怡.md "wikilink")         | 周國正     |
| 08 | [男親女愛](../Page/男親女愛.md "wikilink")                   | [黃子華](../Page/黃子華.md "wikilink")         | 余樂天     |
| 09 | [Loving You我愛你](../Page/Loving_You我愛你.md "wikilink") | [吳啟華](../Page/吳啟華.md "wikilink")         | 高俊邦     |
| 10 | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")         | [羅嘉良](../Page/羅嘉良.md "wikilink")         | 葉榮添     |
| 11 | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")         | [郭晉安](../Page/郭晉安.md "wikilink")         | 馬志強     |
| 12 | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")         | [古天樂](../Page/古天樂.md "wikilink")         | 張自力     |
| 13 | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")         | [秦　沛](../Page/秦沛.md "wikilink")          | 葉孝禮     |
| 14 | [生命有 Take 2](../Page/生命有TAKE2.md "wikilink")         | [黃日華](../Page/黃日華.md "wikilink")         | 周家全     |
| 15 | [陀槍師姐II](../Page/陀槍師姐II.md "wikilink")               | [歐陽震華](../Page/歐陽震華.md "wikilink")       | 陳小生     |
| 16 | [陀槍師姐II](../Page/陀槍師姐II.md "wikilink")               | [魏駿傑](../Page/魏駿傑.md "wikilink")         | 程　峰     |
| 17 | [十月初五的月光](../Page/十月初五的月光.md "wikilink")             | [張智霖](../Page/張智霖.md "wikilink")         | 文　初     |
| 18 | [十月初五的月光](../Page/十月初五的月光.md "wikilink")             | [馬浚偉](../Page/馬浚偉.md "wikilink")         | 司徒禮信    |
| 19 | [金裝四大才子](../Page/金裝四大才子.md "wikilink")               | [林家棟](../Page/林家棟.md "wikilink")         | 文徵明     |
| 20 | [金裝四大才子](../Page/金裝四大才子.md "wikilink")               | [歐陽震華](../Page/歐陽震華.md "wikilink")       | 祝枝山     |
| 21 | [金裝四大才子](../Page/金裝四大才子.md "wikilink")               | [張家輝](../Page/張家輝.md "wikilink")         | 唐伯虎     |
| 22 | [金裝四大才子](../Page/金裝四大才子.md "wikilink")               | [魏駿傑](../Page/魏駿傑.md "wikilink")         | 周文賓     |
| 23 | [廟街·媽·兄弟](../Page/廟街·媽·兄弟.md "wikilink")             | [李克勤](../Page/李克勤.md "wikilink")         | 楊帶寶     |
| 24 | [廟街·媽·兄弟](../Page/廟街·媽·兄弟.md "wikilink")             | [梁漢文](../Page/梁漢文.md "wikilink")         | 王文迪     |
| 25 | [雷霆第一關](../Page/雷霆第一關.md "wikilink")                 | [王　喜](../Page/王喜.md "wikilink")          | 林志剛     |
| 26 | [雷霆第一關](../Page/雷霆第一關.md "wikilink")                 | [李修賢](../Page/李修賢.md "wikilink")         | 塗令山     |

  - 得獎資料

<table>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>劇集</p></th>
<th><p>最後五強</p></th>
<th><p>頒獎嘉賓</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p>《<a href="../Page/洗冤錄.md" title="wikilink">洗冤錄</a>》</p></td>
<td><p><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a>（宋慈 － 洗冤錄）<br />
<a href="../Page/黃子華.md" title="wikilink">黃子華</a>（余樂天 － 男親女愛）<br />
<a href="../Page/吳啟華.md" title="wikilink">吳啟華</a>（歐陽明 － Loving You我愛你）<br />
<a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a>（葉榮添 － 創世紀II天地有情）<br />
<a href="../Page/張智霖.md" title="wikilink">張智霖</a>（文初 － 十月初五的月光）</p></td>
<td><p><a href="../Page/羅蘭.md" title="wikilink">羅蘭</a></p></td>
</tr>
</tbody>
</table>

### 本年度我最喜愛的女主角

|    |                                                      |                                  |         |
| -- | ---------------------------------------------------- | -------------------------------- | ------- |
| 編號 | 劇集                                                   | 演員                               | 角色      |
| 01 | [茶是故鄉濃](../Page/茶是故鄉濃.md "wikilink")                 | [張可頤](../Page/張可頤.md "wikilink") | 盤妹雅     |
| 02 | [洗冤錄](../Page/洗冤錄.md "wikilink")                     | [陳妙瑛](../Page/陳妙瑛.md "wikilink") | 聶　楓     |
| 03 | [洗冤錄](../Page/洗冤錄.md "wikilink")                     | [宣　萱](../Page/宣萱.md "wikilink")  | 唐　思     |
| 04 | [無業樓民](../Page/無業樓民.md "wikilink")                   | [伍詠薇](../Page/伍詠薇.md "wikilink") | 羅繡蘭     |
| 05 | [無業樓民](../Page/無業樓民.md "wikilink")                   | [張燊悅](../Page/張燊悅.md "wikilink") | 孫碧鈴     |
| 06 | [楊貴妃](../Page/楊貴妃_\(2000年電視劇\).md "wikilink")        | [向海嵐](../Page/向海嵐.md "wikilink") | 楊玉嬛     |
| 07 | [楊貴妃](../Page/楊貴妃_\(2000年電視劇\).md "wikilink")        | [吳美珩](../Page/吳美珩.md "wikilink") | 梅　妃     |
| 08 | [楊貴妃](../Page/楊貴妃_\(2000年電視劇\).md "wikilink")        | [郭少芸](../Page/郭少芸.md "wikilink") | 武惠妃     |
| 09 | [男親女愛](../Page/男親女愛.md "wikilink")                   | [鄭裕玲](../Page/鄭裕玲.md "wikilink") | 毛小慧     |
| 10 | [Loving You我愛你](../Page/Loving_You我愛你.md "wikilink") | [鄧萃雯](../Page/鄧萃雯.md "wikilink") | 程家思     |
| 11 | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")         | [郭可盈](../Page/郭可盈.md "wikilink") | 岑穎欣、高美娜 |
| 12 | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")         | [蔡少芬](../Page/蔡少芬.md "wikilink") | 田　寧     |
| 13 | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")         | [汪明荃](../Page/汪明荃.md "wikilink") | 方健平     |
| 14 | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")         | [陳慧珊](../Page/陳慧珊.md "wikilink") | 霍希賢     |
| 15 | [陀槍師姐II](../Page/陀槍師姐II.md "wikilink")               | [關詠荷](../Page/關詠荷.md "wikilink") | 朱素娥     |
| 16 | [陀槍師姐II](../Page/陀槍師姐II.md "wikilink")               | [滕麗名](../Page/滕麗名.md "wikilink") | 陳三元     |
| 17 | [十月初五的月光](../Page/十月初五的月光.md "wikilink")             | [佘詩曼](../Page/佘詩曼.md "wikilink") | 祝君好     |
| 18 | [十月初五的月光](../Page/十月初五的月光.md "wikilink")             | [薛家燕](../Page/薛家燕.md "wikilink") | 朱莎嬌     |
| 19 | [金裝四大才子](../Page/金裝四大才子.md "wikilink")               | [陳松伶](../Page/陳松伶.md "wikilink") | 朱娉婷     |
| 20 | [金裝四大才子](../Page/金裝四大才子.md "wikilink")               | [翁　虹](../Page/翁虹.md "wikilink")  | 石　榴     |
| 21 | [金裝四大才子](../Page/金裝四大才子.md "wikilink")               | [文頌嫻](../Page/文頌嫻.md "wikilink") | 祝曉蓮     |
| 22 | [金裝四大才子](../Page/金裝四大才子.md "wikilink")               | [關詠荷](../Page/關詠荷.md "wikilink") | 秋　香     |
| 23 | [廟街·媽·兄弟](../Page/廟街·媽·兄弟.md "wikilink")             | [葉　璇](../Page/葉璇.md "wikilink")  | 何　喜     |
| 24 | [廟街·媽·兄弟](../Page/廟街·媽·兄弟.md "wikilink")             | [黃淑儀](../Page/黃淑儀.md "wikilink") | 陳鳳英     |
| 25 | [雷霆第一關](../Page/雷霆第一關.md "wikilink")                 | [汪明荃](../Page/汪明荃.md "wikilink") | 呂馥明     |
| 26 | [雷霆第一關](../Page/雷霆第一關.md "wikilink")                 | [宣　萱](../Page/宣萱.md "wikilink")  | 馮滿芬     |

  - 得獎資料

<table>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>劇集</p></th>
<th><p>最後五強</p></th>
<th><p>頒獎嘉賓</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a></strong></p></td>
<td><p>《<a href="../Page/男親女愛.md" title="wikilink">男親女愛</a>》</p></td>
<td><p><a href="../Page/張可頤.md" title="wikilink">張可頤</a>（盤妹雅 － 茶是故鄉濃）<br />
<a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>（毛小慧 － 男親女愛）<br />
<a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a>（祝君好 － 十月初五的月光）<br />
<a href="../Page/薛家燕.md" title="wikilink">薛家燕</a>（朱莎嬌 － 十月初五的月光）<br />
<a href="../Page/宣萱.md" title="wikilink">宣　萱</a>（馮滿芬 － 雷霆第一關）</p></td>
<td><p><a href="../Page/黃子華.md" title="wikilink">黃子華</a><br />
（由於鄭裕玲和黃子華去演出《<a href="../Page/男親女愛舞台劇.md" title="wikilink">男親女愛舞台劇</a>》，所以頒獎嘉賓是其拍檔黃子華）<br />
外景主持：<a href="../Page/梁思浩.md" title="wikilink">梁思浩</a></p></td>
</tr>
</tbody>
</table>

### 本年度我最喜愛的拍檔（戲劇組）

|    |                                                                                                                                       |                                               |
| -- | ------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------- |
| 編號 | 演員                                                                                                                                    | 劇集                                            |
| 01 | [張可頤](../Page/張可頤.md "wikilink")、[林家棟](../Page/林家棟.md "wikilink")                                                                     | [茶是故鄉濃](../Page/茶是故鄉濃.md "wikilink")          |
| 02 | [陳妙瑛](../Page/陳妙瑛.md "wikilink")、[歐陽震華](../Page/歐陽震華.md "wikilink")                                                                   | [洗冤錄](../Page/洗冤錄.md "wikilink")              |
| 03 | [劉松仁](../Page/劉松仁.md "wikilink")、[江　華](../Page/江華_\(演員\).md "wikilink")                                                               | [無業樓民](../Page/無業樓民.md "wikilink")            |
| 04 | [張燊悅](../Page/張燊悅.md "wikilink")、[謝霆鋒](../Page/謝霆鋒.md "wikilink")                                                                     | [撻出愛火花](../Page/撻出愛火花.md "wikilink")          |
| 05 | [向海嵐](../Page/向海嵐.md "wikilink")、[江　華](../Page/江華_\(演員\).md "wikilink")                                                               | [楊貴妃](../Page/楊貴妃_\(2000年電視劇\).md "wikilink") |
| 06 | [鄭裕玲](../Page/鄭裕玲.md "wikilink")、[黃子華](../Page/黃子華.md "wikilink")                                                                     | [男親女愛](../Page/男親女愛.md "wikilink")            |
| 07 | [苑瓊丹](../Page/苑瓊丹.md "wikilink")、[蕭　亮](../Page/蕭亮.md "wikilink")                                                                      | [男親女愛](../Page/男親女愛.md "wikilink")            |
| 08 | [汪明荃](../Page/汪明荃.md "wikilink")、[秦　沛](../Page/秦沛.md "wikilink")                                                                      | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")  |
| 09 | [郭可盈](../Page/郭可盈.md "wikilink")、[羅嘉良](../Page/羅嘉良.md "wikilink")                                                                     | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")  |
| 10 | [蔡少芬](../Page/蔡少芬.md "wikilink")、[古天樂](../Page/古天樂.md "wikilink")                                                                     | [創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")  |
| 11 | [關詠荷](../Page/關詠荷.md "wikilink")、[歐陽震華](../Page/歐陽震華.md "wikilink")                                                                   | [陀槍師姐II](../Page/陀槍師姐II.md "wikilink")        |
| 12 | [滕麗名](../Page/滕麗名.md "wikilink")、[魏駿傑](../Page/魏駿傑.md "wikilink")                                                                     | [陀槍師姐II](../Page/陀槍師姐II.md "wikilink")        |
| 13 | [佘詩曼](../Page/佘詩曼.md "wikilink")、[張智霖](../Page/張智霖.md "wikilink")                                                                     | [十月初五的月光](../Page/十月初五的月光.md "wikilink")      |
| 14 | [張家輝](../Page/張家輝.md "wikilink")、[歐陽震華](../Page/歐陽震華.md "wikilink")、[林家棟](../Page/林家棟.md "wikilink")、[魏駿傑](../Page/魏駿傑.md "wikilink") | [金裝四大才子](../Page/金裝四大才子.md "wikilink")        |
| 15 | [葉　璇](../Page/葉璇.md "wikilink")、[李克勤](../Page/李克勤.md "wikilink")、[梁漢文](../Page/梁漢文.md "wikilink")                                     | [廟街·媽·兄弟](../Page/廟街·媽·兄弟.md "wikilink")      |
| 16 | [汪明荃](../Page/汪明荃.md "wikilink")、[李修賢](../Page/李修賢.md "wikilink")                                                                     | [雷霆第一關](../Page/雷霆第一關.md "wikilink")          |
| 17 | [宣　萱](../Page/宣萱.md "wikilink")、[王　喜](../Page/王喜.md "wikilink")                                                                       | [雷霆第一關](../Page/雷霆第一關.md "wikilink")          |

  - 得獎資料

<table>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>最後五强</p></th>
<th><p>頒獎嘉賓</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a></strong>、<strong><a href="../Page/張智霖.md" title="wikilink">張智霖</a></strong><br />
《<a href="../Page/十月初五的月光.md" title="wikilink">十月初五的月光</a>》</p></td>
<td><p><a href="../Page/張可頤.md" title="wikilink">張可頤</a>、<a href="../Page/林家棟.md" title="wikilink">林家棟</a>（茶是故鄉濃）<br />
<a href="../Page/向海嵐.md" title="wikilink">向海嵐</a>、<a href="../Page/江華.md" title="wikilink">江華</a>（楊貴妃）<br />
<a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>、<a href="../Page/黃子華.md" title="wikilink">黃子華</a>（男親女愛）<br />
<a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a>、<a href="../Page/張智霖.md" title="wikilink">張智霖</a>（十月初五的月光）<br />
<a href="../Page/張家輝.md" title="wikilink">張家輝</a>、<a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a>、<a href="../Page/林家棟.md" title="wikilink">林家棟</a>、<a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a>（金裝四大才子）</p></td>
<td><p><a href="../Page/薛家燕.md" title="wikilink">薛家燕</a></p></td>
</tr>
</tbody>
</table>

### 本年度我最喜愛的拍檔（非戲劇組）

|    |                                                                                                    |                                                |
| -- | -------------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| 編號 | 主持                                                                                                 | 節目名稱                                           |
| 01 | [王賢誌](../Page/王賢誌.md "wikilink")、[歐子欣](../Page/歐子欣.md "wikilink")、[張熙傑](../Page/張熙傑.md "wikilink") | [K-100](../Page/K-100.md "wikilink")           |
| 02 | [余文詩](../Page/余文詩.md "wikilink")、[鄺佐輝](../Page/鄺佐輝.md "wikilink")                                  | [都市閒情](../Page/都市閒情.md "wikilink")             |
| 03 | [呂　方](../Page/呂方.md "wikilink")、[李克勤](../Page/李克勤.md "wikilink")                                   | [勁歌金曲](../Page/勁歌金曲.md "wikilink")             |
| 04 | [張燊悅](../Page/張燊悅.md "wikilink")、[林曉峰](../Page/林曉峰.md "wikilink")                                  | [勁歌金曲](../Page/勁歌金曲.md "wikilink")             |
| 05 | [阮兆祥](../Page/阮兆祥.md "wikilink")、[鄭中基](../Page/鄭中基.md "wikilink")、[蔡一傑](../Page/蔡一傑.md "wikilink") | [爆笑急轉彎](../Page/爆笑急轉彎.md "wikilink")           |
| 06 | [向海嵐](../Page/向海嵐.md "wikilink")、[陳霽平](../Page/陳霽平.md "wikilink")、[陳穎妍](../Page/陳穎妍.md "wikilink") | [娛樂串燒三姊妹](../Page/娛樂串燒三姊妹.md "wikilink")       |
| 07 | [胡　楓](../Page/胡楓.md "wikilink")、[祝文君](../Page/祝文君.md "wikilink")、[康子妮](../Page/康子妮.md "wikilink")  | [互動娛樂圈](../Page/互動娛樂圈.md "wikilink")           |
| 08 | [鄭裕玲](../Page/鄭裕玲.md "wikilink")、[謝天華](../Page/謝天華.md "wikilink")                                  | [智尊打0比賽](../Page/智尊打0比賽.md "wikilink")         |
| 09 | [沈殿霞](../Page/沈殿霞.md "wikilink")、[洪天明](../Page/洪天明.md "wikilink")、[吳家樂](../Page/吳家樂.md "wikilink") | [公益開心百萬Show](../Page/公益開心百萬Show.md "wikilink") |
| 10 | [顧紀筠](../Page/顧紀筠.md "wikilink")、[阮兆祥](../Page/阮兆祥.md "wikilink")                                  | [奧運大玩特玩](../Page/奧運大玩特玩.md "wikilink")         |
| 11 | [鄧浩光](../Page/鄧浩光.md "wikilink")、[陳詠嫻](../Page/陳詠嫻.md "wikilink")                                  | [深海大揭秘](../Page/深海大揭秘.md "wikilink")           |
| 12 | [陳百祥](../Page/陳百祥.md "wikilink")、[顧紀筠](../Page/顧紀筠.md "wikilink")、[谷德昭](../Page/谷德昭.md "wikilink") | [奧運第一現場](../Page/奧運第一現場.md "wikilink")         |
| 13 | [劉天賜](../Page/劉天賜.md "wikilink")、[陶　傑](../Page/陶傑.md "wikilink")、[劉綽琪](../Page/劉綽琪.md "wikilink")  | [犀牛俱樂部](../Page/犀牛俱樂部.md "wikilink")           |
| 14 | [沈殿霞](../Page/沈殿霞.md "wikilink")、[洪天明](../Page/洪天明.md "wikilink")、[任港秀](../Page/任港秀.md "wikilink") | [為食大贏家](../Page/為食大贏家.md "wikilink")           |

  - 得獎資料

<table>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>最後五强</p></th>
<th><p>頒獎嘉賓</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/沈殿霞.md" title="wikilink">沈殿霞</a></strong>、<strong><a href="../Page/洪天明.md" title="wikilink">洪天明</a></strong>、<strong><a href="../Page/吳家樂.md" title="wikilink">吳家樂</a></strong><br />
《<a href="../Page/公益開心百萬Show.md" title="wikilink">公益開心百萬Show</a>》</p></td>
<td><p><a href="../Page/王賢誌.md" title="wikilink">王賢誌</a>、<a href="../Page/歐子欣.md" title="wikilink">歐子欣</a>、<a href="../Page/張熙傑.md" title="wikilink">張熙傑</a><br />
（K-100）<br />
<a href="../Page/張燊悅.md" title="wikilink">張燊悅</a>、<a href="../Page/林曉峰.md" title="wikilink">林曉峰</a><br />
（勁歌金曲）<br />
<a href="../Page/沈殿霞.md" title="wikilink">沈殿霞</a>、<a href="../Page/洪天明.md" title="wikilink">洪天明</a>、<a href="../Page/吳家樂.md" title="wikilink">吳家樂</a><br />
（公益開心百萬Show）<br />
<a href="../Page/陳百祥.md" title="wikilink">陳百祥</a>、<a href="../Page/顧紀筠.md" title="wikilink">顧紀筠</a>、<a href="../Page/谷德超.md" title="wikilink">谷德超</a><br />
（奥運第一現場）<br />
<a href="../Page/劉天賜.md" title="wikilink">劉天賜</a>、<a href="../Page/陶傑.md" title="wikilink">陶傑</a>、<a href="../Page/劉綽琪.md" title="wikilink">劉綽琪</a><br />
（犀牛俱樂部）</p></td>
<td><p><a href="../Page/曾志偉.md" title="wikilink">曾志偉</a>、<a href="../Page/林曉峰.md" title="wikilink">林曉峰</a></p></td>
</tr>
</tbody>
</table>

### 本年度我最喜愛的電視角色

|    |                                                |                                    |              |
| -- | ---------------------------------------------- | ---------------------------------- | ------------ |
| 編號 | 劇集                                             | 演員                                 | 角色           |
| 01 | 《[十月初五的月光](../Page/十月初五的月光.md "wikilink")》     | [張智霖](../Page/張智霖.md "wikilink")   | 文初           |
| 02 | 《[創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")》 | [古天樂](../Page/古天樂.md "wikilink")   | 張自力（亞力／朱古力）  |
| 03 | 《[男親女愛](../Page/男親女愛.md "wikilink")》           | [黃子華](../Page/黃子華.md "wikilink")   | 余樂天（亞Lok）    |
| 04 | 《[創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")》 | [羅嘉良](../Page/羅嘉良.md "wikilink")   | 葉榮添（亞添）      |
| 05 | 《[男親女愛](../Page/男親女愛.md "wikilink")》           | [鄭裕玲](../Page/鄭裕玲.md "wikilink")   | 毛小慧（Miss Mo） |
| 06 | 《[金裝四大才子](../Page/金裝四大才子.md "wikilink")》       | [張家輝](../Page/張家輝.md "wikilink")   | 唐伯虎（伯虎）      |
| 07 | 《[茶是故鄉濃](../Page/茶是故鄉濃.md "wikilink")》         | [林家棟](../Page/林家棟.md "wikilink")   | 方有為（鵪鶉仔）     |
| 08 | 《[創世紀II天地有情](../Page/創世紀II天地有情.md "wikilink")》 | [汪明荃](../Page/汪明荃.md "wikilink")   | 方健平（Liza）    |
| 09 | 《[雷霆第一關](../Page/雷霆第一關.md "wikilink")》         | [宣萱](../Page/宣萱.md "wikilink")     | 馮滿芬（芬女）      |
| 10 | 《[十月初五的月光](../Page/十月初五的月光.md "wikilink")》     | [佘詩曼](../Page/佘詩曼.md "wikilink")   | 祝君好          |
| 11 | 《[十月初五的月光](../Page/十月初五的月光.md "wikilink")》     | [薛家燕](../Page/薛家燕.md "wikilink")   | 朱莎嬌          |
| 12 | 《[洗冤錄](../Page/洗冤錄.md "wikilink")》             | [歐陽震華](../Page/歐陽震華.md "wikilink") | 宋慈           |

頒獎嘉賓：[利孝和夫人](../Page/利孝和夫人.md "wikilink")

## 參考資料

[Category:香港電視頒獎典禮](../Category/香港電視頒獎典禮.md "wikilink")
[Category:萬千星輝頒獎典禮](../Category/萬千星輝頒獎典禮.md "wikilink")
[Category:2000年电视奖项](../Category/2000年电视奖项.md "wikilink")
[Category:2000年無綫電視節目](../Category/2000年無綫電視節目.md "wikilink")

1.  [2000年TVB的视帝视后：欧阳振华](http://blog.sina.com.cn/s/blog_62aea7df0100fmps.html)