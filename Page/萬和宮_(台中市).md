**萬和宮**（亦稱**犁頭店聖母廟**），1684年初建，為[臺中市歷史最悠久的廟宇](../Page/臺中市.md "wikilink")。係因當地昔日打製犁頭等農具店舖聚集的「犁頭店街」（今[南屯區內](../Page/南屯區.md "wikilink")）而得名，奉祀[湄洲](../Page/湄洲.md "wikilink")[天上聖母](../Page/天上聖母.md "wikilink")（俗稱[媽祖](../Page/媽祖.md "wikilink")、老大媽），為[臺中市的四大媽祖廟之一](../Page/臺中市.md "wikilink")。

## 歷史

  - 1684年（[康熙二十三年](../Page/康熙.md "wikilink")），相傳萬和宮聖母神像由[張國](../Page/張國.md "wikilink")（後任[台灣北路營](../Page/台灣北路營.md "wikilink")[參將](../Page/參將.md "wikilink")）自福建湄洲恭請護船來台，神像安置犁頭店，初建小祠祭祀。
  - 1726年（[雍正四年](../Page/雍正.md "wikilink")）9月20日，由時居犁頭店地區的[張](../Page/張姓.md "wikilink")、[廖](../Page/廖姓.md "wikilink")、[簡](../Page/簡姓.md "wikilink")、[江](../Page/江姓.md "wikilink")、[劉](../Page/劉姓.md "wikilink")、[黃](../Page/黃姓.md "wikilink")、[何](../Page/何姓.md "wikilink")、[賴](../Page/賴姓.md "wikilink")、[楊](../Page/楊姓.md "wikilink")、[戴](../Page/戴姓.md "wikilink")、[陳](../Page/陳姓.md "wikilink")、[林等十二大姓氏集資擴建](../Page/林姓.md "wikilink")，大廟建竣，定名為**萬和宮**。
  - 1960年代，時任[南屯區區長林劉連炳與](../Page/南屯區.md "wikilink")[台中市議員林錫三](../Page/台中市議員.md "wikilink")、地方[士紳廖朝樹及南屯里里長陳舜仁等人籌組](../Page/士紳.md "wikilink")，成立「[財團法人臺中市萬和宮](../Page/財團法人.md "wikilink")」。
  - 1985年11月27日，[內政部依](../Page/中華民國內政部.md "wikilink")《[文化資產保存法](../Page/文化資產保存法.md "wikilink")》將萬和宮登錄為[國家第三級古蹟](../Page/:分類:臺灣三級古蹟.md "wikilink")。
  - 1991年2月5日萬和宮文化大樓動工，1993年6月落成啟用。
  - 1997年8月16日，成立「財團法人萬和文教基金會」。
  - 1999年，[臺中市政府斥資四千餘萬元進行古蹟修復工程](../Page/臺中市政府.md "wikilink")，2001年底修復完成。
  - 2002年10月29日，舉行「三級古蹟工程竣工啟用及天上聖母等眾神尊入火安座大典」。
  - 2004年10月24日，於萬和宮文化大樓五樓設立「麻芛文化館」，為台中市第一座民間地方文化館。
  - 2008年8月12日，[台中市文化局依](../Page/台中市文化局.md "wikilink")《文化資產保存法》將萬和宮傳統慶典中的「[犁頭店穿木屐躦鯪鯉](../Page/犁頭店穿木屐躦鯪鯉.md "wikilink")」登錄為民俗類（風俗）的[無形文化資產](../Page/無形文化資產.md "wikilink")。
  - 2012年6月19日，臺中市文化資產管理中心（今[臺中市文化資產處](../Page/臺中市文化資產處.md "wikilink")）依《文化資產保存法》將萬和宮傳統慶典中的「[萬和宮字姓戲](../Page/萬和宮字姓戲.md "wikilink")」與「[老二媽西屯省親遶境](../Page/老二媽西屯省親遶境.md "wikilink")」登錄為民俗類（信仰）的無形文化資產。

## 祭祀

[萬和宮.jpg](https://zh.wikipedia.org/wiki/File:萬和宮.jpg "fig:萬和宮.jpg")
[萬和三川.JPG](https://zh.wikipedia.org/wiki/File:萬和三川.JPG "fig:萬和三川.JPG")

  - 正殿

<!-- end list -->

  - 主神：天上聖母
      - 老大媽（鎮殿媽祖）
      - 老二媽（1803年雕塑，相傳為廖姓 ）
      - 聖二媽（1886年雕塑）
      - 聖三媽（1886年雕塑）
      - 聖老二媽（1976年雕塑）
  - 配祀：
      - [千里眼](../Page/千里眼.md "wikilink")
      - [順風耳](../Page/順風耳.md "wikilink")

<!-- end list -->

  - 後殿

<!-- end list -->

  - 主神：[觀音菩薩](../Page/觀音菩薩.md "wikilink")
  - 配祀：
      - 善才、玉女
      - [十八羅漢](../Page/十八羅漢.md "wikilink")
      - [註生娘娘](../Page/註生娘娘.md "wikilink")
      - [城隍爺](../Page/城隍爺.md "wikilink")
      - [福德正神](../Page/福德正神.md "wikilink")
      - 廟基主：[張國](../Page/張國.md "wikilink")、[賴清標祿位](../Page/賴清標.md "wikilink")

<!-- end list -->

  - 後殿東廂

<!-- end list -->

  - 主神：[關聖帝君](../Page/關聖帝君.md "wikilink")
  - 配祀：[關平](../Page/關平.md "wikilink")、[周倉](../Page/周倉.md "wikilink")

<!-- end list -->

  - 後殿西廂

<!-- end list -->

  - 主神：[神農大帝](../Page/神農大帝.md "wikilink")、[西秦王爺](../Page/西秦王爺.md "wikilink")

<!-- end list -->

  - 列位尊神

<!-- end list -->

  - 正殿：太子爺（大太子、二太子、[三太子](../Page/三太子.md "wikilink")）
  - 內殿：[地藏王菩薩](../Page/地藏王菩薩.md "wikilink")、[斗姥星君](../Page/斗姥.md "wikilink")、[玉皇上帝](../Page/玉皇上帝.md "wikilink")、[三官大帝](../Page/三官大帝.md "wikilink")、[地母娘娘](../Page/地母娘娘.md "wikilink")、[玄天上帝](../Page/玄天上帝.md "wikilink")、[定光古佛](../Page/定光古佛.md "wikilink")、[開漳聖王](../Page/開漳聖王.md "wikilink")。

## 建築

  - 廟宇

萬和宮為[福建](../Page/福建.md "wikilink")[漳](../Page/漳州府.md "wikilink")[泉地區傳統建築風格](../Page/泉州府.md "wikilink")，為早期漳泉[汀](../Page/汀州府.md "wikilink")[嘉](../Page/嘉應府.md "wikilink")[潮](../Page/潮州府.md "wikilink")[惠六府人士開發台中盆地](../Page/惠州府.md "wikilink")，做了最好見證。

  - 文化大樓

地下室為餐廳、一樓為辦公室、二樓為會議廳、三樓為圖書館、四樓為文物館、五樓為麻芛文化館。頂樓作為「眺望樓」，西望[大肚台地](../Page/大肚台地.md "wikilink")，東眺台中市區哦。

  - 麻芛文化館

麻芛文化館是財團法人萬和文教基金會接受文建會指導，定位為地方歷史暨常民文化館。

  - 展示內容：分為南屯的歷史軌跡、[黃麻輝煌史](../Page/黃麻.md "wikilink")、珍品典藏、麻芛文化在南屯、創意藝廊
  - 開放時間：每逢[農曆初一及十五](../Page/農曆.md "wikilink")，以及周休假日開放參觀，由導覽[志工隊派員解說服務](../Page/志工.md "wikilink")。

## 特色

### 老二媽西屯省親遶境

[台中西屯南屯1944空照圖.jpg](https://zh.wikipedia.org/wiki/File:台中西屯南屯1944空照圖.jpg "fig:台中西屯南屯1944空照圖.jpg")
1803年（[嘉慶](../Page/嘉慶.md "wikilink")8年）11月，特別增塑「老二媽」神像一尊。相傳[妝佛完畢後](../Page/妝佛.md "wikilink")，舉行開光點眼儀式，正好有[西屯大魚池一位少女廖品娘突然去世](../Page/西屯區.md "wikilink")，其[魂魄飛往萬和宮](../Page/魂魄.md "wikilink")，卻在廟中與賣針線化妝品的[女紅商人相遇](../Page/女紅.md "wikilink")，商人不知道其已經逝世，回程時轉告其父母。廖母趕至萬和宮探尋，見「老二媽」神像眼中墜下一顆淚珠，遺留於臉頰，知道她愛女已羽化，並成為媽祖的[分靈](../Page/分靈.md "wikilink")\[1\]。

此傳聞被列為犁頭店媽祖神蹟之一，也與西屯大魚池結下不解之緣。自嘉慶年間後，[西屯廖氏稱老二媽為](../Page/張廖家族.md "wikilink")「老姑婆」（廖姓媽祖\[2\]）。老二媽西屯省親遶境：三年一次恭迎「老二媽」[回娘家](../Page/回娘家.md "wikilink")[大魚池烈美堂敬拜](../Page/大魚池烈美堂.md "wikilink")，成為當地習俗之一。

### 字姓戲

相傳1824年（[道光](../Page/道光.md "wikilink")4年），「老二媽」於[旱溪媽祖遊庄至南屯例行接駕並相隨遶境南屯地區](../Page/旱溪媽祖遶境十八庄.md "wikilink")。「老二媽」神轎遶境回返時，突然重如萬鈞而無法抬入廟内，經[擲筶得示以演](../Page/擲筊.md "wikilink")「[字姓戲](../Page/字姓戲.md "wikilink")」娛神代替遶境，每年自[農曆](../Page/農曆.md "wikilink")3月21日起，由各字姓舉行[三獻禮以及演](../Page/三獻禮.md "wikilink")[梨園戲請媽祖觀賞](../Page/梨園戲.md "wikilink")，獲首肯，神轎才順利入廟。

自此從1825年（[道光](../Page/道光.md "wikilink")5年）[農曆](../Page/農曆.md "wikilink")3月21日起開演字姓戲。21日為漳洲戲、22日[廣東戲](../Page/廣東.md "wikilink")（即潮州、嘉應州、惠州等）、24日泉洲戲、25日汀洲戲，接著是各[姓氏的字姓戲](../Page/姓氏.md "wikilink")、謝神戲與兵仔戲，持續一、兩個月，迄今歷代不斷，已成為地方重要傳統民俗活動。至此信眾陸續增加，早期的十二字姓，已增至廿八字姓。

### 犁頭店穿木屐躦鯪鯉

犁頭店穿木屐躦鯪鯉（踩[木屐震醒](../Page/木屐.md "wikilink")[鯪鯉](../Page/穿山甲.md "wikilink")），是[台灣南屯地區](../Page/台灣.md "wikilink")[端午節的特有活動](../Page/端午節.md "wikilink")。相傳犁頭店在[風水上是](../Page/風水.md "wikilink")「鯪鯉穴」，此區也是重要稻作地區；而[穿山甲在土中活動可翻鬆](../Page/穿山甲.md "wikilink")[土壤以利農耕](../Page/土壤.md "wikilink")，但因其具[冬眠習性](../Page/冬眠.md "wikilink")。

為使其翻動使耕作順利，進而帶動地區一年的發展，於是當地發展出穿著木屐來回重踏地上，發出巨大的劈啪聲響，彷彿真能震醒穿山甲一般的習俗。隨後發展成每年[端午節不同於其他地區](../Page/端午節.md "wikilink")-{[划龍舟](../Page/划龍舟.md "wikilink")}-的穿著長木屐競賽活動，顯示台中市南屯地區[農業社會的特殊風俗](../Page/農業.md "wikilink")。

## 相關條目

  - [湄洲媽祖祖廟](../Page/湄洲媽祖祖廟.md "wikilink")
  - [台灣媽祖信仰](../Page/台灣媽祖信仰.md "wikilink")
  - [臺中市文化資產列表](../Page/臺中市文化資產列表.md "wikilink")

## 參考資料

<references/>

## 外部連結

  - [萬和宮媽祖廟](http://www.wanhegong.org.tw/)
  - [萬和宮—台中市文化資產處](http://www.tchac.taichung.gov.tw/building?uid=33&pid=3)
  - [萬和宮老二媽西屯省親遶境—台中市文化資產處](http://www.tchac.taichung.gov.tw/historybuilding?uid=39&pid=147)
  - [萬和宮老二媽西屯省親遶境—文化部文化資產局](https://nchdb.boch.gov.tw/assets/advanceSearch/folklore/20120615000001)

[W](../Category/台中市古蹟.md "wikilink")
[Category:台中市四大媽祖廟](../Category/台中市四大媽祖廟.md "wikilink")
[W](../Category/南屯區.md "wikilink")

1.
2.  [一個台中的動人故事～『老二媽』傳說](http://forpeople.pixnet.net/blog/post/797983-%E4%B8%80%E5%80%8B%E5%8F%B0%E4%B8%AD%E7%9A%84%E5%8B%95%E4%BA%BA%E6%95%85%E4%BA%8B%EF%BD%9E%E3%80%8E%E8%80%81%E4%BA%8C%E5%AA%BD%E3%80%8F%E5%82%B3%E8%AA%AA)
    ，政治可以不一樣