**萬華媒體集團有限公司**（One Media Group
Limited）是一家以[香港為基地的](../Page/香港.md "wikilink")[中文](../Page/中文.md "wikilink")[媒體集團](../Page/媒體.md "wikilink")，前身為[明報企業集團旗下的雜誌部門](../Page/明報企業.md "wikilink")，公司於[開曼群島註冊](../Page/開曼群島.md "wikilink")，集團總部設於[香港](../Page/香港.md "wikilink")[柴灣嘉業街](../Page/柴灣.md "wikilink")18號明報工業中心A座15樓。

萬華媒體是在[大中華地區從事印刷](../Page/大中華地區.md "wikilink")、數碼及戶外媒體業務具有實力的媒體集團之一，2005年10月於[香港交易所上市](../Page/香港交易所.md "wikilink")（），行業分類為服務業-多媒體傳媒。出版業務方面，主要是推廣及發行中文消閒生活[雜誌](../Page/雜誌.md "wikilink")，並銷售該等雜誌的廣告版面。

集團現時於[香港及](../Page/香港.md "wikilink")[中國大陸共出版](../Page/中國大陸.md "wikilink")7本[中文](../Page/中文.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")，並投資在幾個數碼媒體平台,包括流動閱讀平台ByRead百閱,
網上娛樂平台hi好酷及[社交網絡](../Page/社交網絡.md "wikilink")[應用程式Partyline](../Page/手機應用程式.md "wikilink")。於2012年11月，集團更開始拓展高速客運廣告業務。

萬華媒體集團有限公司是於香港聯合交易所及[馬來西亞證券交易所兩地雙邊上市的](../Page/馬來西亞證券交易所.md "wikilink")[世界華文媒體有限公司](../Page/世界華文媒體.md "wikilink")（香港股份代號：0685；馬來西亞股份代號：5090）旗下的大中華旗艦媒體集團。

## 出版刊物

  - 香港

<!-- end list -->

  - 《[明報周刊](../Page/明報周刊.md "wikilink")》
  - 《[TopGear 極速誌](../Page/TopGear_極速誌.md "wikilink")》
  - 《[明錶](../Page/明錶.md "wikilink")》
  - 《[100毛](../Page/100毛.md "wikilink")》
  - 《[黑紙](../Page/黑紙.md "wikilink")》
  - 《[優遊香港](../Page/優遊香港.md "wikilink")》
  - 《[港澳台自由行專輯](../Page/港澳台自由行專輯.md "wikilink")》

<!-- end list -->

  - 中國大陸

<!-- end list -->

  - 《[TOPGEAR汽車測試報告](../Page/TOPGEAR汽車測試報告.md "wikilink")》
  - 《[科技新時代](../Page/科技新時代.md "wikilink")》

## 數碼媒體

  - 香港

<!-- end list -->

  - 《[Partyline](../Page/Partyline.md "wikilink")》
  - 《[評台](../Page/評台.md "wikilink")》

<!-- end list -->

  - 中國大陸

<!-- end list -->

  - 《[hi好酷](../Page/hi好酷.md "wikilink")》
  - 《[ByRead百閱](../Page/ByRead百閱.md "wikilink")》

## 明報集團重組後情況

2007年1月，同樣由[丹斯里拿督張曉卿擁有的香港](../Page/丹斯里.md "wikilink")《[明報](../Page/明報.md "wikilink")》、馬來西亞《[南洋商報](../Page/南洋商報.md "wikilink")》以及馬來西亞《[星洲日報](../Page/星洲日報.md "wikilink")》重組，全部併入[明報企業](../Page/明報企業.md "wikilink")(現稱[世界華文媒體](../Page/世界華文媒體.md "wikilink"))名下，並且於香港及[馬來西亞兩個地方同時上市](../Page/馬來西亞.md "wikilink")。萬華媒體的獨立上市地位，並未受到是次明報企業集團重組的影響，而明報企業集團依然為萬華媒體的大股東。

## 外部連結

  - [萬華媒體網站](http://www.omghk.com)

[Category:香港媒體](../Category/香港媒體.md "wikilink")
[Category:馬來西亞中文媒體](../Category/馬來西亞中文媒體.md "wikilink")
[Category:2005年建立](../Category/2005年建立.md "wikilink")