《**落窪物語**》，全文共四卷，為日本的[物語文學之一](../Page/物語.md "wikilink")，出現在約[十世紀的日本](../Page/十世紀.md "wikilink")[平安時代初期](../Page/平安時代.md "wikilink")，作者亡佚。故事內容近似於[格林童話中的](../Page/格林童話.md "wikilink")《[灰姑娘](../Page/灰姑娘.md "wikilink")》，和[中國公元九世紀成書的](../Page/中國.md "wikilink")《[酉陽雜俎](../Page/酉陽雜俎.md "wikilink")·[叶限姑娘](../Page/叶限.md "wikilink")》。

## 故事內容

[平安時代某](../Page/平安時代.md "wikilink")[天皇朝](../Page/天皇.md "wikilink")，有一名中納言[源忠賴](../Page/源忠賴.md "wikilink")。源忠賴和[北之方](../Page/北之方.md "wikilink")\[1\]間共有三男四女，而和另一位[皇女某](../Page/皇女.md "wikilink")，亦有一花容月貌的女兒。後來皇女某過世，中納言忠賴便將該女接回家中撫養。

接回家中後，由於此女出身高貴（[皇女所出](../Page/皇女.md "wikilink")），又美若天仙，因此引起北之方的忌妒，之後北之方強迫她住在破舊低窪的[房間](../Page/房間.md "wikilink")，並要眾人稱此女為「落窪姬」，且由於北之方的諂媚，中納言源忠賴並沒有對落窪姬伸出援手。

北之方夫人並將落窪姬當作[丫鬟](../Page/丫鬟.md "wikilink")，強迫其打雜。而因落窪姬擅長[女紅等針線活](../Page/女紅.md "wikilink")，北之方夫人便將原本由他人所做的針線活，全數交由落窪姬負責，之後又因落窪姬擅彈[琴](../Page/琴.md "wikilink")[箏](../Page/箏.md "wikilink")，北之方夫人便命落窪姬教導自己所生的三公子彈[琴](../Page/琴.md "wikilink")，而這三公子也是家中唯一同情落窪姬遭遇的人。

落窪姬身邊原有一個[女房](../Page/女房.md "wikilink")，名為[阿漕](../Page/阿漕.md "wikilink")，也隨著落窪姬進入[中納言家中](../Page/中納言.md "wikilink")，但北之方夫人在將落窪姬當作一般侍女後，便命阿漕離開落窪姬，前往服侍新婚的三之君\[2\]。三之君的丈夫[藏人少將身邊有一隨從名為](../Page/藏人少將.md "wikilink")[帶刀](../Page/帶刀.md "wikilink")，阿漕和帶刀[戀愛](../Page/戀愛.md "wikilink")，而帶刀的[母親是右近少將](../Page/母親.md "wikilink")[道賴的](../Page/道賴.md "wikilink")[乳母](../Page/乳母.md "wikilink")，也因為這層關係，使得道賴得知落窪姬的遭遇。

道賴少將在聽聞落窪姬的遭遇和身世後，起了愛慕之心，便送[和歌給落窪姬](../Page/和歌.md "wikilink")，落窪姬起初不加以會，最後是道賴少將強同落窪姬結為[夫妻](../Page/夫妻.md "wikilink")，並許諾將落窪姬帶離中納言家。

這件事情被北之方知曉後，北之方大為反對，因為她原本打算，將落窪姬強嫁與自己的親戚某兵部少輔，落窪姬經過反抗後，被北之方幽禁起來，之後是道賴少將將落窪姬救出，並報復了中納言一家，最後道賴少將將落窪姬帶離，兩人結為連理。

## 注釋

<div class="references-small">

<references />

</div>

## 另見

  - [灰姑娘](../Page/灰姑娘.md "wikilink")

  - [葉限](../Page/葉限.md "wikilink")

  -
  -
[Category:日本歷史小說](../Category/日本歷史小說.md "wikilink")
[Category:平安時代背景作品](../Category/平安時代背景作品.md "wikilink")
[Category:灰姑娘型故事](../Category/灰姑娘型故事.md "wikilink")

1.  「之方」（）是日本古代已婚貴族女性的稱呼之一
2.  北之方夫人的三女兒