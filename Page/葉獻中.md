**葉獻中**（），為[台灣](../Page/台灣.md "wikilink")[足球及](../Page/足球.md "wikilink")[室內足球運動員](../Page/室內足球.md "wikilink")，職司守門員，小名「紅中」。[臺中市立惠文高級中學足球隊教練](../Page/臺中市立惠文高級中學.md "wikilink")。

葉獻中為[中華台北足球代表隊現役成員](../Page/中華台北足球代表隊.md "wikilink")，曾參加於[2004年夏季奧林匹克運動會足球比賽會外賽及](../Page/2004年夏季奧林匹克運動會足球比賽.md "wikilink")[亞洲室內足球錦標賽](../Page/亞洲室內足球錦標賽.md "wikilink")。

葉獻中也是[大同足球隊的選手](../Page/大同足球隊.md "wikilink")，曾參加[2006年亞洲足協會長盃](../Page/2006年亞洲足協會長盃.md "wikilink")。\[1\]

## 參考資料

[Category:台灣足球運動員](../Category/台灣足球運動員.md "wikilink")
[Category:葉姓](../Category/葉姓.md "wikilink")
[Category:臺北市立體育學院校友](../Category/臺北市立體育學院校友.md "wikilink")
[Category:台灣足球主教練](../Category/台灣足球主教練.md "wikilink")

1.