**葉芳雄**，[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）[政治人物](../Page/政治人物.md "wikilink")，曾代表[中國國民黨任職](../Page/中國國民黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。

## 經歷

  - 第六屆[立法委員](../Page/立法委員.md "wikilink")
  - [竹北市第四](../Page/竹北市.md "wikilink")、五屆市長
  - [竹北市後備軍人輔導中心主任](../Page/竹北市.md "wikilink")
  - 救國團[竹北市團委會會長](../Page/竹北市.md "wikilink")
  - [竹北市民眾服務分社理事長](../Page/竹北市.md "wikilink")
  - [新竹縣退伍軍人協會理事長](../Page/新竹縣.md "wikilink")
  - [竹北市體育會理事長](../Page/竹北市.md "wikilink")

## 選舉

### 1998年竹北市長選舉

| 1998年[竹北市市長選舉結果](../Page/竹北市.md "wikilink") |
| ------------------------------------------- |
| 號次                                          |
| 1                                           |
| 2                                           |

### 2002年竹北市長選舉

| 2002年[竹北市市長選舉結果](../Page/竹北市.md "wikilink") |
| ------------------------------------------- |
| 號次                                          |
| 1                                           |
| 2                                           |
| **選舉人數**                                    |
| **投票數**                                     |
| **有效票**                                     |
| **無效票**                                     |
| **投票率**                                     |

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00173&stage=6)

[category:第6屆中華民國立法委員](../Page/category:第6屆中華民國立法委員.md "wikilink")

[Category:竹北市市長](../Category/竹北市市長.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Fang芳](../Category/葉姓.md "wikilink")