**葉詠捷**（）
，為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾效力於[中華職棒](../Page/中華職棒.md "wikilink")[兄弟象隊](../Page/兄弟象.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")。

## 經歷

  - [臺北縣](../Page/新北市.md "wikilink")[光華國小](../Page/光華國小.md "wikilink")[少棒隊](../Page/少棒.md "wikilink")
  - [臺北縣二重國中青少棒隊](../Page/新北市.md "wikilink")
  - [臺北縣](../Page/新北市.md "wikilink")[穀保家商青棒隊](../Page/穀保家商.md "wikilink")
  - [文化大學棒球隊](../Page/文化大學.md "wikilink")（美孚巨人）
  - [合作金庫棒球隊](../Page/合作金庫.md "wikilink")
  - [La New熊隊練習生](../Page/La_New熊.md "wikilink")
  - [兄弟象隊代訓球員](../Page/兄弟象.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[兄弟象隊球員](../Page/兄弟象.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[兄弟象](../Page/兄弟象.md "wikilink")[投手教練](../Page/投手教練.md "wikilink")
  - [中信兄弟助理投手教練](../Page/中信兄弟.md "wikilink")
  - [穀保家商棒球隊投手教練](../Page/穀保家商.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                               | 出賽  | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死  | 三振  | 責失  | 投球局數  | 防禦率   |
| ----- | -------------------------------- | --- | -- | -- | -- | -- | -- | -- | --- | --- | --- | ----- | ----- |
| 2006年 | [兄弟象](../Page/兄弟象.md "wikilink") | 24  | 4  | 8  | 1  | 0  | 0  | 0  | 40  | 36  | 51  | 69.2  | 6.59  |
| 2007年 | [兄弟象](../Page/兄弟象.md "wikilink") | 29  | 5  | 7  | 0  | 0  | 0  | 0  | 51  | 73  | 45  | 79.0  | 5.12  |
| 2008年 | [兄弟象](../Page/兄弟象.md "wikilink") | 47  | 8  | 8  | 10 | 1  | 0  | 0  | 25  | 52  | 24  | 63.2  | 3.39  |
| 2009年 | [兄弟象](../Page/兄弟象.md "wikilink") | 34  | 4  | 0  | 2  | 0  | 0  | 0  | 29  | 33  | 31  | 46.1  | 6.021 |
| 2010年 | [兄弟象](../Page/兄弟象.md "wikilink") | 34  | 3  | 5  | 10 | 0  | 0  | 0  | 19  | 24  | 24  | 49.1  | 4.378 |
| 2011年 | [兄弟象](../Page/兄弟象.md "wikilink") | 24  | 1  | 2  | 0  | 0  | 0  | 0  | 34  | 29  | 30  | 53.1  | 5.062 |
| 2012年 | [兄弟象](../Page/兄弟象.md "wikilink") | 6   | 0  | 1  | 0  | 0  | 0  | 0  | 9   | 6   | 8   | 8.2   | 8.307 |
| 合計    | 7年                               | 198 | 25 | 31 | 23 | 1  | 0  | 0  | 207 | 253 | 213 | 371.0 | 5.167 |

## 特殊事蹟

  - 2013年4月1日，球團董事長[洪瑞河提議他轉任球隊投手教練](../Page/洪瑞河.md "wikilink")，接替原先兼任球員的[湯瑪仕工作](../Page/湯瑪仕B.T.md "wikilink")。

## 外部連結

[Y](../Category/葉姓.md "wikilink") [Y](../Category/在世人物.md "wikilink")
[Y](../Category/1985年出生.md "wikilink")
[Y](../Category/兄弟象隊球員.md "wikilink")
[Y](../Category/台灣棒球選手.md "wikilink")
[Y](../Category/中國文化大學校友.md "wikilink")