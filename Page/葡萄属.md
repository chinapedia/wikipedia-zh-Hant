**葡萄屬**（[学名](../Page/学名.md "wikilink")：）是[葡萄目](../Page/葡萄目.md "wikilink")[葡萄科的一群](../Page/葡萄科.md "wikilink")[落叶](../Page/落叶植物.md "wikilink")[藤本植物](../Page/藤本植物.md "wikilink")，大約包含60種植物，主要分佈於[北半球](../Page/北半球.md "wikilink")。葡萄屬植物可制成[葡萄汁](../Page/葡萄汁.md "wikilink")、[葡萄干和](../Page/葡萄干.md "wikilink")[葡萄酒](../Page/葡萄酒.md "wikilink")。一系列研究、培育葡萄屬植物的文化被稱為[葡萄文化](../Page/葡萄文化.md "wikilink")。

## 分类

  - *Vitis acerifolia*
  - *Vitis adenoclada*
  - *Vitis aestivalis*
  - [山葡萄](../Page/山葡萄.md "wikilink") *Vitis amurensis*
  - [亞利桑那葡萄](../Page/亞利桑那葡萄.md "wikilink") *Vitis arizonica*
  - [小果葡萄](../Page/小果葡萄.md "wikilink") *Vitis balansana*
  - *Vitis barbata*
  - [麦黄葡萄](../Page/麦黄葡萄.md "wikilink") *Vitis bashanica*
  - [美丽葡萄](../Page/美丽葡萄.md "wikilink") *Vitis bellula*
  - *Vitis berlandieri*
  - [樺葉葡萄](../Page/樺葉葡萄.md "wikilink") *Vitis betulifolia*
  - *Vitis biformis*
  - *Vitis blancoi*
  - *Vitis bloodworthiana*
  - *Vitis bourgaeana*
  - [蘡薁](../Page/蘡薁.md "wikilink") *Vitis bryoniifolia*
  - [美洲葡萄](../Page/美洲葡萄.md "wikilink") *Vitis californica*
  - [東南葡萄](../Page/東南葡萄.md "wikilink") *Vitis chunganensis*
  - [闽赣葡萄](../Page/闽赣葡萄.md "wikilink") *Vitis chungii*
  - *Vitis cinerea*
  - [紫葛](../Page/紫葛.md "wikilink") *Vitis coignetiae*
  - *Vitis davidii*
  - [红叶葡萄](../Page/红叶葡萄.md "wikilink") *Vitis erythrophylla*
  - [鳳慶葡萄](../Page/鳳慶葡萄.md "wikilink") *Vitis fengqinensis*
  - [桑葉葡萄](../Page/桑葉葡萄.md "wikilink") *Vitis ficifolia*
  - [葛藟葡萄](../Page/葛藟葡萄.md "wikilink") *Vitis flexuosa*
  - *Vitis girdiana*
  - [菱叶葡萄](../Page/菱叶葡萄.md "wikilink") *Vitis hancockii*
  - *Vitis heyneana*
  - [庐山葡萄](../Page/庐山葡萄.md "wikilink") *Vitis hui*
  - *Vitis jacquemontii*
  - *Vitis jaegeriana*
  - [井冈葡萄](../Page/井冈葡萄.md "wikilink") *Vitis jinggangensis*
  - [基隆葡萄](../Page/基隆葡萄.md "wikilink") *Vitis kelungensis*
  - [巨峰葡萄](../Page/巨峰葡萄.md "wikilink") *Vitis giant peak*
  - [雞足葡萄](../Page/雞足葡萄.md "wikilink") *Vitis lanceolatifoliosa*
  - *Vitis lincecumii*
  - [龍泉葡萄](../Page/龍泉葡萄.md "wikilink") *Vitis longquanensis*
  - [羅城葡萄](../Page/羅城葡萄.md "wikilink") *Vitis luochengensis*
  - [勐海葡萄](../Page/勐海葡萄.md "wikilink") *Vitis menghaiensis*
  - [蒙自葡萄](../Page/蒙自葡萄.md "wikilink") *Vitis mengziensis*
  - *Vitis monticola*
  - *Vitis mustangensis*
  - *Vitis nesbittiana*
  - *Vitis palmata*
  - *Vitis peninsularis*
  - [變葉葡萄](../Page/變葉葡萄.md "wikilink") *Vitis piasezkii*
  - *Vitis pilosonerva*
  - *Vitis popenoei*
  - [華東葡萄](../Page/華東葡萄.md "wikilink") *Vitis pseudoreticulata*
  - [綿毛葡萄](../Page/綿毛葡萄.md "wikilink") *Vitis retordii*
  - [河岸葡萄](../Page/河岸葡萄.md "wikilink") *Vitis riparia*
  - *Vitis romanetii*
  - [麝香葡萄](../Page/麝香葡萄.md "wikilink") *Vitis rotundifolia*
  - *Vitis rupestris*
  - [乳源葡萄](../Page/乳源葡萄.md "wikilink") *Vitis ruyuanensis*
  - [陕西葡萄](../Page/陕西葡萄.md "wikilink") *Vitis shenxiensis*
  - *Vitis shuttleworthii*
  - [湖北葡萄](../Page/湖北葡萄.md "wikilink") *Vitis silvestrii*
  - [小葉葡萄](../Page/小葉葡萄.md "wikilink") *Vitis sinocinerea*
  - *Vitis tiliifolia*
  - *Vitis treleasei*
  - [狹葉葡萄](../Page/狹葉葡萄.md "wikilink") *Vitis tsoii*
  - [釀酒葡萄](../Page/釀酒葡萄.md "wikilink") *Vitis vinifera*
  - *Vitis vulpina*
  - [温州葡萄](../Page/温州葡萄.md "wikilink") *Vitis wenchouensis*
  - [网脉葡萄](../Page/网脉葡萄.md "wikilink") *Vitis wilsonae*
  - [武汉葡萄](../Page/武汉葡萄.md "wikilink") *Vitis wuhanensis*
  - [米葡萄](../Page/米葡萄.md "wikilink") *Vitis xunyangensis*
  - [燕山葡萄](../Page/燕山葡萄.md "wikilink")*Vitis yeshanensis*
  - [雲南葡萄](../Page/雲南葡萄.md "wikilink") *Vitis yunnanensis*
  - [浙江蘡薁](../Page/浙江蘡薁.md "wikilink") *Vitis zhejiang-adstricta*

### 杂交种

  - *Vitis* × *andersonii*
  - *Vitis* × *bourquina*
  - *Vitis* × *champinii*
  - *Vitis* × *doaniana*
  - *Vitis* × *novae-angliae*
  - *Vitis* × *slavinii*

## 参考资料

[Category:水果](../Category/水果.md "wikilink")
[Category:葡萄](../Category/葡萄.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")