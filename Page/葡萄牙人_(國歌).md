《**葡萄牙人**》（），是[葡萄牙共和國的](../Page/葡萄牙共和國.md "wikilink")[国歌](../Page/国歌.md "wikilink")。歌詞譜自亨利克·罗佩斯·德·孟东萨（），阿尔弗雷多·凯尔（）作曲。由於[英國在](../Page/英國.md "wikilink")19世紀末段下最後通牒挑釁葡萄牙，葡萄牙軍隊退出在[安哥拉和](../Page/安哥拉.md "wikilink")[莫桑比克之間的疆土](../Page/莫桑比克.md "wikilink")，激起[民族主義熱潮](../Page/民族主義.md "wikilink")，故誕生這首富有戰鬥力的歌曲。1910年[葡萄牙共和國成立](../Page/葡萄牙共和國.md "wikilink")，此曲成为國歌。

## 历史

19世紀末，正值葡萄牙共和運動興起，葡萄牙布拉干薩皇朝提出了[粉紅地圖計劃](../Page/粉紅地圖計劃.md "wikilink")，希望佔領葡屬[安哥拉和葡屬](../Page/安哥拉.md "wikilink")[莫桑比克之間的英國殖民地](../Page/莫桑比克.md "wikilink")（即今日的[贊比亞全境和今日](../Page/贊比亞.md "wikilink")[津巴布韋部份領土](../Page/津巴布韋.md "wikilink")），並向本國百姓加重徵稅，作為實行計劃的資金來源。

1890年1月11日，[英國发出最后通牒](../Page/英國.md "wikilink")，要求葡萄牙全面放弃其粉紅地圖計劃。其後英方更派遺軍艦遠赴首都里斯本港口，威脅葡方若不下令撤走非洲莫桑比克及安哥拉之間的葡萄牙軍隊,就會對里斯本開火。朝廷在英國的威脅下，唯有應允英方條件。

朝廷被迫接受英国的条件一事傳出，國內民眾嘩然、並發生骚乱；原本因徵稅而一窮二白的葡萄牙民眾，對国王不滿的程度火上加油；令他們對君主-{制}-生疑，希望推翻王室，建立共和。

共和黨人作家孟东萨有感於此作了一首诗，敦促他的同胞为祖国战斗（），並透過描寫海军以称颂国家，尤其是15世紀及16世紀。为此作曲，这首歌曲马上深入民心，表達了葡萄牙人对葡萄牙当局向英國人俯首称臣的不滿。

1891年1月31日，共和主义者在葡萄牙北部城市[波尔图发动革命](../Page/波尔图.md "wikilink")，高唱此曲。后來革命被镇压，歌曲亦被取缔。1910年10月5日，革命成功推翻布拉干薩王朝，廢除君主制，建立葡萄牙共和国。1911年，此曲正式立法成为葡萄牙的国歌。

1999年12月20日，《葡萄牙人》在澳門的國歌地位被《[義勇軍進行曲](../Page/義勇軍進行曲.md "wikilink")》取代。

## 歌詞

本曲的歌詞只包含孟东萨原詩中的第一段及重句，昔原詩的第二及第三段均沒有納入歌詞內；而歌詞重句中的第五句是，意為「冒著炮火前進，前進！」；但原詩卻是，意為「冒著英寇前進，前進！」。

<table>
<thead>
<tr class="header">
<th><p>葡萄牙國歌 </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>葡語原文</p></td>
</tr>
<tr class="even">
<td><p>第一段<br />
</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>重句</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>第二段</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>重句</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>第三段</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>重句</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [葡萄牙國歌--葡萄牙人](http://chinesenationsymbols.tripod.com/portuguese_macau_anthem.htm)
  - [A
    Portuguesa](http://www.governo.gov.pt/Portal/PT/Portugal/Simbolos_Nacionais/HinoNacional.htm)
    - 葡萄牙政府網站
  - [Hino
    Nacional](https://web.archive.org/web/20140923154005/http://5outubro.centenariorepublica.pt/index.php?option=com_content&view=article&id=101&Itemid=96)
    - 慶祝葡萄牙共和國成立一百週年網站

{{-}}

[P](../Category/国歌.md "wikilink")
[Category:葡萄牙国家象征](../Category/葡萄牙国家象征.md "wikilink")
[Category:葡萄牙歌曲](../Category/葡萄牙歌曲.md "wikilink")