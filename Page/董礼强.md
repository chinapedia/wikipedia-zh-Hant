**董礼强**，（），[中国](../Page/中国.md "wikilink")[辽宁人](../Page/辽宁.md "wikilink")，著名足球运动员，现已退役。

董礼强在踢球生涯主要效力于[辽宁队](../Page/辽宁足球俱乐部.md "wikilink")，多次入选[中国国家足球队](../Page/中国国家足球队.md "wikilink")。在场上司职中后卫，身体强壮，拼抢凶狠。

1986年进入辽宁足球队，获得8次全国冠军。

在[1990年世界杯足球赛亚洲区预选赛对](../Page/1990年世界杯足球赛.md "wikilink")[阿联酋的比赛中](../Page/阿联酋国家足球队.md "wikilink")，董礼强一次解围踢漏和一次带球失误导致中国队在3分钟内被连失两球，最终输掉了比赛并未能出线。
这一事件后来被称为“黑色三分钟”。<sup>[1](http://top.ce.cn/home/tyzh/200704/19/t20070419_11091037_1.shtml)</sup>

1997年由[辽宁队转会至](../Page/辽宁足球俱乐部.md "wikilink")[深圳平安](../Page/深圳平安.md "wikilink")，1999年底退役。

退役后董礼强先后在[沈阳海狮](../Page/沈阳海狮.md "wikilink")、[南京有有](../Page/南京有有.md "wikilink")、[深圳科健](../Page/深圳科健足球俱乐部.md "wikilink")、[辽宁队](../Page/辽宁足球俱乐部.md "wikilink")、[河南建业担任助理教练](../Page/河南建业.md "wikilink")。

[Category:遼寧宏運球員](../Category/遼寧宏運球員.md "wikilink")
[Category:中国国家足球队运动员](../Category/中国国家足球队运动员.md "wikilink")
[Category:深圳足球俱乐部球员](../Category/深圳足球俱乐部球员.md "wikilink")
[Category:中国足球运动员](../Category/中国足球运动员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:董姓](../Category/董姓.md "wikilink")
[Category:中國足球主教練](../Category/中國足球主教練.md "wikilink")