**葫芦岛市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[辽宁省下辖的](../Page/辽宁省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，原名**锦西**，位于辽宁省西南部。市境东北接[锦州市](../Page/锦州市.md "wikilink")，北临[朝阳市](../Page/朝阳市.md "wikilink")，西界[河北省](../Page/河北省.md "wikilink")[秦皇岛市](../Page/秦皇岛市.md "wikilink")，南滨[辽东湾](../Page/辽东湾.md "wikilink")。地处辽冀两省交界，辽西山地丘陵区与沿海平原交接地带，濒临[渤海湾](../Page/渤海湾.md "wikilink")，南接[山海关](../Page/山海关.md "wikilink")。地势西北高，东南低，西部为[燕山山脉](../Page/燕山山脉.md "wikilink")，北部为[松岭山脉](../Page/松岭山脉.md "wikilink")。主要河流有[女儿河](../Page/女儿河.md "wikilink")、[六股河](../Page/六股河.md "wikilink")、[兴城河等](../Page/兴城河.md "wikilink")。全市总面积1.04万平方公里，人口280万。葫芦岛市是[京沈线上重要的工业](../Page/京沈铁路.md "wikilink")、旅游、军事城市，有多处旅游景点，被誉为“关外第一市”、“北京后花园”。

葫芦岛市矿产资源颇丰富，拥有[铅](../Page/铅.md "wikilink")、[锌](../Page/锌.md "wikilink")、[铜](../Page/铜.md "wikilink")、[钼等矿产](../Page/钼.md "wikilink")，亦有葫芦岛化工（集团）有限责任公司、葫芦岛化工机械（集团）有限责任公司、中国石油锦西炼油化工总厂、锦西天然气有限责任公司、锌厂和渤海造船厂等工业企业。[渤海造船厂生产了中国的第一艘](../Page/渤海造船厂.md "wikilink")[核潜艇](../Page/核潜艇.md "wikilink")，是中国重要的常规潜艇基地。

## 历史

葫芦岛，最初为海岛名称，始见于《[全辽志](../Page/全辽志.md "wikilink")》。[明](../Page/明朝.md "wikilink")[嘉靖四十五年](../Page/嘉靖.md "wikilink")（1566年）刊行的《全辽志》卷一山川，[宁远卫条目下](../Page/宁远卫.md "wikilink")，列有葫芦岛、[觉华岛](../Page/觉华岛.md "wikilink")、[桃花岛](../Page/桃花岛.md "wikilink")。对葫芦岛则注明：“在海岸四十里，半山入海”两行小字。其后，[天启三年](../Page/天启.md "wikilink")（1623年）中极殿[大学士](../Page/大学士.md "wikilink")、[兵部](../Page/兵部.md "wikilink")[尚书](../Page/尚书.md "wikilink")[孙承宗](../Page/孙承宗.md "wikilink")《奏报关东情形疏》和国父[孙中山](../Page/孙中山.md "wikilink")《建国大纲》以及其他许多文献，都提到葫芦岛的名字，有的称“葫芦套”或称“断冈”、“折冈”。[清朝末叶](../Page/清朝.md "wikilink")，经过[英国工程师](../Page/英国.md "wikilink")[秀思勘测](../Page/秀思.md "wikilink")，并由清廷、[中华民国](../Page/中华民国.md "wikilink")、[满洲国和](../Page/满洲国.md "wikilink")[中华人民共和国](../Page/中华人民共和国.md "wikilink")，分阶段几次建筑海港，以及其后海港在军事、交通、贸易诸方面发挥出的显著作用，葫芦岛已蜚声海内外。

据考古发掘证实，今葫芦岛地区早在新石器时代已开始有人类繁衍生息。[商周以后](../Page/商周.md "wikilink")，在不同历史时期主要有[山戒](../Page/山戒.md "wikilink")、[东胡](../Page/东胡.md "wikilink")、[乌桓](../Page/乌桓.md "wikilink")、[鲜卑](../Page/鲜卑.md "wikilink")、[契丹](../Page/契丹.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[女真等少数民族人同](../Page/女真.md "wikilink")[汉族在这里生活](../Page/汉族.md "wikilink")。[燕](../Page/燕.md "wikilink")、[秦开征战东北以后](../Page/秦.md "wikilink")，汉族明显增多。秦统一全国后，今境属[辽西郡](../Page/辽西郡.md "wikilink")、[辽东郡](../Page/辽东郡.md "wikilink")、[右北平郡](../Page/右北平郡.md "wikilink")。[西汉初属燕国的领地](../Page/西汉.md "wikilink")。[汉武帝](../Page/汉武帝.md "wikilink")[元朔元年](../Page/元朔.md "wikilink")（前128年）废燕王改置辽西、辽东二郡。今葫芦岛境内的有辽西郡徒河、文成二县；徒河县治所在今连山区台集屯，辖境西至今[兴城东部](../Page/兴城.md "wikilink")，东于[锦州一带](../Page/锦州.md "wikilink")，南至海滨地区，文成县治所在今[绥中县北部地区](../Page/绥中县.md "wikilink")。位于附近并辖有本境一部分地区的有三县：海阳县，治所在今山海关西海阳镇，本境兴城西部及绥中南部为其辖区；柳城（今朝阳）、狐苏二县，辖苏二县，辖有今兴城、连山、南票的北部地区。除辽西郡外，右北平郡的石城、广城、白狼三县辖有今建昌地区。广场县治所在今建昌县东北大凌河（当地称木伦河）右岸土城子。石城、白狼二县治所在今喀左境内（一说白狼县在今建昌宫山咀乡章京营子）。

[东汉时](../Page/东汉.md "wikilink")，为安抚和客理叛附不定的[乌桓族](../Page/乌桓.md "wikilink")，[汉安帝](../Page/汉安帝.md "wikilink")[永初元年](../Page/永初.md "wikilink")（107年），在昌黎（即交黎，亦称昌辽，今[义昌](../Page/义昌.md "wikilink")）设属国都尉。从辽西郡划出昌黎、宾徒（今锦州北营城子）、徒河三县，从辽东郡划出无虑（今[北镇](../Page/北镇.md "wikilink")）、房县（[盘山](../Page/盘山.md "wikilink")）、险渎（[台安](../Page/台安.md "wikilink")）三县，置辽东属国。原临渝县由今[义县九道岭一带](../Page/义县.md "wikilink")，西迁至今山海关镇一带。当时，本境[建昌一带被乌桓占据](../Page/建昌.md "wikilink")，余者分属于辽东属国的昌辽县和徒河县。东汉后期，本境均为乌桓占据。[汉灵帝](../Page/汉灵帝.md "wikilink")[中平六年](../Page/中平.md "wikilink")（189年），权臣[董卓任命](../Page/董卓.md "wikilink")[公孙度为辽东太守](../Page/公孙度.md "wikilink")。《辽东志》称：“辽远也，以其在九州之东，故名辽东。又兼辽西也。”[公孙度上任的第二年](../Page/公孙度.md "wikilink")，自立为辽东侯，称平州牧，州设在襄平。从此，辽东地区成为公孙氏割据政权。

[建安十二年](../Page/建安.md "wikilink")（207年），[曹操北伐](../Page/曹操.md "wikilink")[乌桓](../Page/乌桓.md "wikilink")。五月出师，经卢龙，过平冈，向乌桓王驻地柳城进发。当曹军进至[建昌一带](../Page/建昌.md "wikilink")，乌桓才发觉，便急忙调兵迎战。八月，两军在白狼山（即今[喀左](../Page/喀左.md "wikilink")[太阳山](../Page/太阳山.md "wikilink")，一说建昌[大黑山](../Page/大黑山.md "wikilink")）相遇，曹操以[张辽为先锋](../Page/张辽.md "wikilink")，纵军出击，大败乌桓军，斩乌桓王[蹋顿](../Page/蹋顿.md "wikilink")，乌桓及归附的汉人投降者20余万。曹操攻占柳城，尽占辽水以西之地。公孙氏对曹操政权时附时叛，景初二年（238年），魏明帝派司马懿为将，向公孙氏发动总攻，破襄平，斩杀公孙渊父子，统一辽东地区。

[魏晋时期](../Page/魏晋.md "wikilink")，曹魏撤销了公孙氏设置的平州将[辽西](../Page/辽西.md "wikilink")、[昌黎](../Page/昌黎.md "wikilink")、[玄菟](../Page/玄菟.md "wikilink")、[辽东四郡又划归](../Page/辽东.md "wikilink")[幽州](../Page/幽州.md "wikilink")。昌黎郡原是东汉辽东属国，魏时改为郡。晋武帝泰始十年（274年），又置平州，割州的昌黎辽东、玄菟、乐浪、带方（经公孙氏设置，在朝鲜北部）五郡属平州，州治在辽东郡的襄平。辽西郡仍属幽州郡治在阳乐（原在朝阳松树咀子，西晋时已西迁）。本境，魏时属幽州昌黎郡，西晋属平州昌黎郡，由昌黎、宾徒、集宁、徒河等县统辖；东晋时，本境基本上为鲜卑族分割，其统治中心在龙城（今[朝阳](../Page/朝阳.md "wikilink")），先后由[前燕](../Page/前燕.md "wikilink")、[前秦](../Page/前秦.md "wikilink")、[后燕](../Page/后燕.md "wikilink")、[北燕所统辖](../Page/北燕.md "wikilink")。北燕时，在辽西地区分置平州昌黎郡（建昌境内）及青州营丘郡。本境分属于昌黎建德、石城三郡。

[南北朝时期](../Page/南北朝.md "wikilink")，北魏尽占燕地，在龙城置营州，辖昌黎、[冀阳](../Page/冀阳.md "wikilink")、[建德](../Page/建德.md "wikilink")、[营丘](../Page/营丘.md "wikilink")、[辽东](../Page/辽东.md "wikilink")、[乐浪六郡](../Page/乐浪.md "wikilink")。其中，在今辽西地区有昌黎、建德、营丘（锦县一带）三郡。昌黎郡，与营州同治龙城，领龙城、广兴、定荒三县。本境除建昌以外均属广兴县。建德郡消费品市场所在白狼城，领石城、广都、阳城（一说阳武）三县，今建昌地区主要在石城县境内。

[隋朝](../Page/隋朝.md "wikilink")，文帝开皇三年占有营州后，只留建德一郡和龙城一县，其余并废。建德郡仍治白狼城，龙城县治今朝阳市。开皇十八年（598年），废郡，改龙城县为柳城。隋炀帝大业八年（612年），置柳城郡与辽西郡。柳城郡治柳城县（今朝阳），本境为柳城县地。

[唐代设](../Page/唐代.md "wikilink")[安东都护府](../Page/安东都护府.md "wikilink")（初设在平壤，后移治辽东城，即今辽阳市）和[营州总管府](../Page/营州总管府.md "wikilink")（地址在今朝阳市）。改为营州都督府，隶属河北道，督领[柳城县和](../Page/柳城县.md "wikilink")[营州](../Page/营州.md "wikilink")、[辽州等七州](../Page/辽州.md "wikilink")，地域范围相当于今辽西地区。设在今本市境内的有威州（即辽州）后改为瑞州，治来远县，地址在今绥中前卫。唐灭东突厥后，将突厥乌突罕部安置在威州及今建昌一带。据《辽宁地方史》记载：在今连山区西区北孤竹营子设有带州孤竹县，查锦西旧志及新编《锦西市志》对此均无记载。按地方志考评，原锦西县当时属营州泸河镇（今义县东南）；[兴城为柳城](../Page/兴城.md "wikilink")、来远二县地；绥中属来远县（后改为州）；建昌为柳城县地，后为契丹割据。唐朝地方建置，除设州县外，为加强控制周边各民族还设有军镇和节度使。[开元五年](../Page/开元.md "wikilink")（717年），唐玄宗在营州置平卢军使，第三年升格为平卢节度使。平卢节度使例兼营州都督和安东都护府都护。[营州](../Page/营州.md "wikilink")，实为唐朝在东北地区的军政及经济文化中心。太宗[李世民征](../Page/李世民.md "wikilink")[高丽](../Page/高丽.md "wikilink")，曾在此亲自祭奠阵亡将士。

[辽代中京道辖境大部在今朝阳](../Page/辽代.md "wikilink")、[锦州和葫芦岛地区](../Page/锦州.md "wikilink")。原锦西一带为中京定府锦州所辖安昌县地，西北一部地区初为榆州永和县，后并入安昌县，县址为今暖池塘安昌岘，还有直属中京大定府的[神水县](../Page/神水县.md "wikilink")。《[辽史](../Page/辽史.md "wikilink")·圣宗本纪》，[开泰二年](../Page/开泰.md "wikilink")“二月丙子诏以女河咱为神水县”，《辽史地理志》载“神水县本汉徒河县地”，其遗址在今连山区台集屯。锦州还辖有设在今兴城市西南曹庄四城子的严州，附郭县为兴城县（一说兴城县址在海中菊花岛）。辽圣宗太平元年（1021年），在今绥中县前卫镇置来州，治来宾县。来州另辖隰州治海滨县，地址在今兴城市东辛庄镇东关站，辖境包括今兴城西部及建昌南部地区。今建昌大部为中京道谭州龙山县地，州县同治，地址在今喀左白塔子。

[金代设在今本市境内外内及附近的](../Page/金代.md "wikilink")（府）县有：锦州、辖永乐（附郭）、安昌、神水三县。兴中府（今朝阳市）辖瑞安（附郭，今前卫镇）、海阳（今山海关西海阳镇）、海滨三县。利州（今喀左大城子），辖阜裕（附郭）、龙山二县。今建昌北部为龙山县地，南部属瑞州海滨县。

[元朝改金代的北京路为](../Page/元朝.md "wikilink")[大宁路](../Page/大宁路.md "wikilink")，今本市全境均为大宁路所辖，并废永乐、安昌、神水、兴城诸县入锦州；废海滨县，瑞安县入[瑞州](../Page/瑞州.md "wikilink")；今建昌地区为[大宁路](../Page/大宁路.md "wikilink")[利州龙山县地](../Page/利州.md "wikilink")。

[明](../Page/明朝.md "wikilink")[洪武八年](../Page/洪武.md "wikilink")（1375年）在辽阳设辽东都指挥使司（简称辽东都司），领二十五卫和二州。[正统元年](../Page/正统.md "wikilink")（1436年）后，直接由辽东巡抚统辖，设在本市境内及附近的卫所有：广宁中屯卫（今锦州）、广宁前屯卫（即今绥中前卫）。宣德三年（1428年），在前屯卫城东五十里杏林铺（今绥中县城）置中后千户所，在城西五十里急水河置中前千户所。同年，为加强辽西防务，分广宁前屯、中屯二卫地于曹庄、汤池之北置宁远卫、总兵巫凯、都御史包怀德督建宁远城（今兴城）。从此，宁远成为辽西军事重镇。宁远城经几次维修成为全国现存最完好的一座古城。宣德五年（1430年），在宁远城东五十里塔山置中左千户所，在城西四十里小沙河置中右千户所。另外，在今朝阳置营州卫，建昌县为营州中屯卫（卫所在今白塔子）地，后为[兀良哈蒙古牧地](../Page/兀良哈.md "wikilink")。[崇祯八年](../Page/崇祯.md "wikilink")（1635年），归附后金的蒙古族喀喇沁左旗，旗王府设在官大海，后迁至南宫营子，辖境包括今建昌地区。

[清](../Page/清朝.md "wikilink")[康熙三年](../Page/康熙.md "wikilink")（1644年）置[广宁府](../Page/广宁府.md "wikilink")，第二年改置[锦州府](../Page/锦州府.md "wikilink")，府治今锦州，属奉天府统辖。锦州府，初领一州、二县，后增至二州、二厅、三县。其中，锦县，与府同治，辖境包括今连山大部地区。本今本市境内有一州、一厅、一县。康熙三年，并宁远卫、广宁前屯卫为宁远州（治今兴城镇）。[光绪二十八年](../Page/光绪.md "wikilink")，划宁远州西境六股河以西置绥中县（今县址）。光绪三十二年，划锦县西境地置江家屯厅，后更名为锦西厅（厅址，今[连山区](../Page/连山区.md "wikilink")[钢屯镇](../Page/钢屯镇.md "wikilink")）。今建昌一带，当时属于[喀喇沁部塔希卓氏王氏王公](../Page/喀喇沁.md "wikilink")（汉姓乌氏）的喀喇沁左翼旗。旗署专管旗民事务。对汉民管理，也先后设厅县。[乾隆三年](../Page/乾隆.md "wikilink")（1738年），设塔子沟厅，乾隆四十三年，改为建昌县，隶属直录省承德府。清朝末期，徐世昌任东三省总督后，推行新政，废除旗民二重制。1913年改锦西县。

[抗日战争结束后](../Page/抗日战争.md "wikilink")，东北约百万日本侨民在此地被遣返回国。见[葫芦岛日侨大遣返](../Page/葫芦岛日侨大遣返.md "wikilink")。[第二次国共内战时](../Page/第二次国共内战.md "wikilink")，[辽沈战役中的](../Page/辽沈战役.md "wikilink")[塔山阻击战于此地展开](../Page/塔山阻击战.md "wikilink")。市下所辖[绥中县是中国第一位](../Page/绥中县.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")[杨利伟的出生地](../Page/杨利伟.md "wikilink")。

## 地理

[Huludao_New_District.jpg](https://zh.wikipedia.org/wiki/File:Huludao_New_District.jpg "fig:Huludao_New_District.jpg")
葫芦岛市中心地理方位为东经120°38\`，北纬
40°56\`，地处[辽宁省西南部](../Page/辽宁省.md "wikilink")，东邻[锦州](../Page/锦州.md "wikilink")，西接[山海关](../Page/山海关.md "wikilink")，南临渤海辽东湾，与辽宁省的[大连](../Page/大连.md "wikilink")、[营口](../Page/营口.md "wikilink")，[河北省的](../Page/河北省.md "wikilink")[秦皇岛](../Page/秦皇岛.md "wikilink")、[唐山](../Page/唐山.md "wikilink")，[天津市](../Page/天津市.md "wikilink")，[山东省的](../Page/山东省.md "wikilink")[烟台](../Page/烟台.md "wikilink")、[青岛等市构成](../Page/青岛.md "wikilink")[环渤海经济圈](../Page/环渤海经济圈.md "wikilink")，扼关内外之咽喉，是[中国东北地区的西大门](../Page/中国东北.md "wikilink")，为[华北前往](../Page/华北.md "wikilink")[山海关外的第一座城市](../Page/山海关.md "wikilink")。

## 自然资源

### 土地资源

葫芦岛市土地面积104.14万公顷。其中农用地面积726005.84公顷，占全市土地总面积的69.8%，耕地226003.17公顷，占农用地面积的31.1%；建设用地83083.36公顷，占全市土地总面积的7.9%；未利用地231414.61公顷，占全市土地总面积的22.2%。

### 矿产资源

葫芦岛市是矿业开发时间早，矿山企业较多的地区。发现和探明的矿产资源有51种，已开发利用的矿种，主要矿种有[煤](../Page/煤.md "wikilink")、[钼](../Page/钼.md "wikilink")、[铅](../Page/铅.md "wikilink")、[锌](../Page/锌.md "wikilink")、[硫](../Page/硫.md "wikilink")、[锰](../Page/锰.md "wikilink")、[金](../Page/金.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[耐活土及各种建筑用石料和砖瓦粘土等](../Page/耐活土.md "wikilink")。煤矿是第一矿种，累计探明储量近4亿吨，目前保有地质量约1.8亿吨。主要分布在[南票](../Page/南票.md "wikilink")、[建昌冰沟和](../Page/建昌.md "wikilink")[连山虹螺岘地区](../Page/连山.md "wikilink")，占全市矿业总产值的二分之一以上。钼是优势矿种，累计探明矿石储量3.8亿吨位，金属量40多万吨，保有储量约1.5亿吨，金属量约15万吨。主要分布在连山区钢屯、[杨家杖子地区](../Page/杨家杖子.md "wikilink")，是全国第二大钼精矿产地。铅锌、银矿以共生和伴生赋存为主，累计探明矿石储量1400万吨，金属量60万吨位，保有矿石量500万吨。硫铁已探明储量近2000万吨，年生产能力40万吨，主要分布在八家子地区。锰矿已探明储量近10吨，经过开采目前已濒临枯竭。除上述矿种外，本市的[大理石](../Page/大理石.md "wikilink")、[花岗石](../Page/花岗石.md "wikilink")、[耐火粘土](../Page/耐火粘土.md "wikilink")、[硅石](../Page/硅石.md "wikilink")、[沸石](../Page/沸石.md "wikilink")、[地热等储量也很大](../Page/地热.md "wikilink")，质量好。

### 森林资源

葫芦岛市自然概貌为七山一水二分田，林业用地527269.8公顷，占土地总面积的50.6%，森林面积365398公顷，森林覆盖率为35.4%。

在植物区系上，葫芦岛市处于华北植物区系内，有长白植物区系和内蒙古植物区系的侵入。由于气候、地势及地理位置等因素的影响，本市的植物种类复杂、资源比较丰富。全市共有野生植物113科507属1097种。

### 野生动物资源

葫芦岛市按动物地理区划，以华北为主，是华北、蒙新、东北三个动物地理区相互交错种过渡地带，处于[候鸟陆路和水路两条重要的迁徙线上](../Page/候鸟.md "wikilink")，是大批候鸟春秋季迁徙的“中转站”，地理位置十分重要。全市生境复杂，地貌特殊，具有野动物物种多样性、遗传多样性和生态多样性的特点。境内陆生野生脊椎动物共有477种，其中属[国家重点保护动物](../Page/国家重点保护动物.md "wikilink")101种，属省重点保护动物100种，必国家有益，有重要经济科研价值的野生动物276种。其中：[哺乳动物](../Page/哺乳动物.md "wikilink")7目16科58种，[爬行动物](../Page/爬行动物.md "wikilink")23种，[两栖动物](../Page/两栖动物.md "wikilink")9种，[鸟类动物](../Page/鸟类.md "wikilink")387种。

森林[昆虫](../Page/昆虫.md "wikilink")：森林昆虫在中国虽然不属于野生动物的范畴，但是昆虫在野生植物和野生动物的生态环境中起着重要的作用。全市共有森林昆虫9目65科226种。

## 政治

### 现任领导

<table>
<caption>葫芦岛市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党葫芦岛市委员会.md" title="wikilink">中国共产党<br />
葫芦岛市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/葫芦岛市人民代表大会.md" title="wikilink">葫芦岛市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/葫芦岛市人民政府.md" title="wikilink">葫芦岛市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议葫芦岛市委员会.md" title="wikilink">中国人民政治协商会议<br />
葫芦岛市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/孙轶.md" title="wikilink">孙轶</a>[1][2]</p></td>
<td><p><a href="../Page/王力威.md" title="wikilink">王力威</a>[3]</p></td>
<td><p><a href="../Page/石文光.md" title="wikilink">石文光</a>[4]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/辽宁省.md" title="wikilink">辽宁省</a><a href="../Page/凌源市.md" title="wikilink">凌源市</a></p></td>
<td><p>辽宁省<a href="../Page/丹东市.md" title="wikilink">丹东市</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年3月</p></td>
<td><p>2018年1月</p></td>
<td><p>2016年10月</p></td>
<td><p>2018年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

[Huludao_city_center_train_station.jpg](https://zh.wikipedia.org/wiki/File:Huludao_city_center_train_station.jpg "fig:Huludao_city_center_train_station.jpg")
现辖3个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[连山区](../Page/连山区.md "wikilink")、[龙港区](../Page/龙港区.md "wikilink")、[南票区](../Page/南票区.md "wikilink")
  - 县级市：[兴城市](../Page/兴城市.md "wikilink")
  - 县：[绥中县](../Page/绥中县.md "wikilink")、[建昌县](../Page/建昌县.md "wikilink")

此外，葫芦岛市还设立以下经济管理区：[葫芦岛经济开发区](../Page/葫芦岛经济开发区.md "wikilink")、[杨家杖子经济技术开发区](../Page/杨家杖子经济技术开发区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>葫芦岛市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>211400</p></td>
</tr>
<tr class="odd">
<td><p>211402</p></td>
</tr>
<tr class="even">
<td><p>211403</p></td>
</tr>
<tr class="odd">
<td><p>211404</p></td>
</tr>
<tr class="even">
<td><p>211421</p></td>
</tr>
<tr class="odd">
<td><p>211422</p></td>
</tr>
<tr class="even">
<td><p>211481</p></td>
</tr>
<tr class="odd">
<td><p>注：连山区数字包含杨家杖子经济技术开发区所辖2街道。</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>葫芦岛市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="odd">
<td><p>葫芦岛市</p></td>
<td><p>2623541</p></td>
<td><p>100</p></td>
</tr>
<tr class="even">
<td><p>连山区</p></td>
<td><p>605186</p></td>
<td><p>23.07</p></td>
</tr>
<tr class="odd">
<td><p>龙港区</p></td>
<td><p>244053</p></td>
<td><p>9.30</p></td>
</tr>
<tr class="even">
<td><p>南票区</p></td>
<td><p>120969</p></td>
<td><p>4.61</p></td>
</tr>
<tr class="odd">
<td><p>绥中县</p></td>
<td><p>586620</p></td>
<td><p>22.36</p></td>
</tr>
<tr class="even">
<td><p>建昌县</p></td>
<td><p>520537</p></td>
<td><p>19.84</p></td>
</tr>
<tr class="odd">
<td><p>兴城市</p></td>
<td><p>546176</p></td>
<td><p>20.82</p></td>
</tr>
<tr class="even">
<td><p>注：连山区的常住人口数据中包含杨家杖子经济技术开发区31677人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")2623541人\[8\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加42856人，增长1.66%。年平均增长率为0.16%。其中，男性人口为1334538人，占50.87%；女性人口为1289003人，占49.13%。总人口性别比（以女性为100）为103.53。0－14岁人口为391503人，占14.92%；15－59岁人口1837323人，占70.03%；60岁及以上人口为394715人，占15.05%，其中65岁及以上人口为265487人，占10.12%。居住在城镇的人口为1089843人，占41.54%；居住在乡村的人口为1533698人，占58.46%。

{{-}}

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [满族](../Page/满族.md "wikilink") | [蒙古族](../Page/蒙古族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [朝鲜族](../Page/朝鲜族.md "wikilink") | [锡伯族](../Page/锡伯族.md "wikilink") | [傈僳族](../Page/傈僳族.md "wikilink") | [苗族](../Page/苗族.md "wikilink") | [黎族](../Page/黎族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | ------------------------------ | -------------------------------- | ------------------------------ | -------------------------------- | -------------------------------- | -------------------------------- | ------------------------------ | ------------------------------ | -------------------------------- | ---- |
| 人口数          | 1903409                        | 669915                         | 36914                            | 10449                          | 1016                             | 526                              | 241                              | 203                            | 144                            | 140                              | 584  |
| 占总人口比例（%）    | 72.55                          | 25.53                          | 1.41                             | 0.40                           | 0.04                             | 0.02                             | 0.01                             | 0.01                           | 0.01                           | 0.01                             | 0.02 |
| 占少数民族人口比例（%） | \---                           | 93.03                          | 5.13                             | 1.45                           | 0.14                             | 0.07                             | 0.03                             | 0.03                           | 0.02                           | 0.02                             | 0.08 |

**葫芦岛市民族构成（2010年11月）**\[9\]

## 风景旅游

历史的变迁和社会的发展，给葫芦岛市留下了大量的文物古迹和风景名胜，如九门口长城、觉华岛、兴城的明代古城等。

## 学校

  - [辽宁工程技术大学葫芦岛校区](../Page/辽宁工程技术大学.md "wikilink")
  - [辽宁财贸学院](../Page/辽宁财贸学院.md "wikilink")
  - [葫芦岛市第一高级中学](../Page/葫芦岛市第一高级中学.md "wikilink")
  - 葫芦岛市第二高级中学
  - 葫芦岛市实验高中
  - 葫芦岛市龙港区风华中学
  - [葫芦岛市实验中学](../Page/葫芦岛市实验中学.md "wikilink")
  - 葫芦岛市第六初级中学
  - 葫芦岛市第七中学
  - [葫芦岛市实验小学](../Page/葫芦岛市实验小学.md "wikilink")
  - [葫芦岛市第二实验小学](../Page/葫芦岛市第二实验小学.md "wikilink")

## 姐妹、友好城市

  - [美國](../Page/美國.md "wikilink")[內華達州](../Page/內華達州.md "wikilink")[拉斯维加斯](../Page/拉斯维加斯.md "wikilink")\[10\]

<!-- end list -->

  - [日本国](../Page/日本国.md "wikilink")[宫崎县](../Page/宫崎县.md "wikilink")[宫崎市](../Page/宫崎市.md "wikilink")\[11\]

<!-- end list -->

  - [日本国](../Page/日本国.md "wikilink")[长野县](../Page/长野县.md "wikilink")[松本市](../Page/松本市.md "wikilink")\[12\]
  - [德国](../Page/德国.md "wikilink")[黑森州](../Page/黑森州.md "wikilink")[奥博乌尔泽尔市](../Page/上乌瑟尔.md "wikilink")\[13\]
  - [泰国](../Page/泰国.md "wikilink")[碧武里府](../Page/碧武里府.md "wikilink")[碧武里市](../Page/碧武里市.md "wikilink")\[14\]
  - [拉脱维亚](../Page/拉脱维亚.md "wikilink")[叶尔加瓦市](../Page/叶尔加瓦市.md "wikilink")\[15\]
  - [韩国](../Page/韩国.md "wikilink")[仁川广域市](../Page/仁川广域市.md "wikilink")[富平区](../Page/富平区.md "wikilink")\[16\]
  - [台湾](../Page/台湾.md "wikilink")[台东市](../Page/台东市.md "wikilink")\[17\]
  - [河北省](../Page/河北省.md "wikilink")[秦皇岛市](../Page/秦皇岛市.md "wikilink")\[18\]

## 参考文献

## 外部链接

  - [明珠海岸](https://web.archive.org/web/20070224221936/http://www.hld163.com/)
  - [葫芦岛网](http://www.huludao.net/)
  - [海风旅行网](http://www.ehaifeng.com/)

{{-}}

[sr:Хулудао](../Page/sr:Хулудао.md "wikilink")

[辽](../Category/中国大城市.md "wikilink")
[Category:辽宁地级市](../Category/辽宁地级市.md "wikilink")
[葫芦岛](../Category/葫芦岛.md "wikilink")
[Category:1994年建立的行政區劃](../Category/1994年建立的行政區劃.md "wikilink")
[Category:渤海沿海城市](../Category/渤海沿海城市.md "wikilink")
[H](../Category/辽宁沿海经济带.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10. <http://www.hld.gov.cn/hldgk/djs/200704/t20070416_218022.html>
11. <http://news.cnr.cn/native/city/20160923/t20160923_523157128.shtml>
12. <http://news.cnr.cn/native/city/20160923/t20160923_523157128.shtml>
13. <http://www.hld.gov.cn/xwdt/zwyw/201208/t20120816_176478.html>
14. <http://www.hld.gov.cn/xwdt/zwyw/201503/t20150313_200813.html>
15. <http://www.hld.gov.cn/xwdt_5274/zwyw/201010/t20101021_177784.html>
16. <http://www.hld.gov.cn/xwdt/mrtt/201109/t20110921_144840.html>
17. <http://www.hld.gov.cn/xwdt/mrtt/201410/t20141017_198440.html>
18. <http://www.hld.gov.cn/xwdt/zwyw/201611/t20161124_415350.html>