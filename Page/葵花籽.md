[Sunflower_Seeds_Kaldari.jpg](https://zh.wikipedia.org/wiki/File:Sunflower_Seeds_Kaldari.jpg "fig:Sunflower_Seeds_Kaldari.jpg")[Sonnenblumenkerne_sunflower_seeds.jpg](https://zh.wikipedia.org/wiki/File:Sonnenblumenkerne_sunflower_seeds.jpg "fig:Sonnenblumenkerne_sunflower_seeds.jpg")
[Sunflowers_seeds_food.jpg](https://zh.wikipedia.org/wiki/File:Sunflowers_seeds_food.jpg "fig:Sunflowers_seeds_food.jpg")
**葵花籽**，是指[向日葵的](../Page/向日葵.md "wikilink")[果实](../Page/果实.md "wikilink")
（連殼）或種子（去殼後）。顏色有[黑色](../Page/黑色.md "wikilink")、[白色和](../Page/白色.md "wikilink")[褐色](../Page/褐色.md "wikilink")，大部份种子都是多色於一体。

可以做為[零食](../Page/零食.md "wikilink")，也可以榨油，[葵花籽油可以用來煮菜](../Page/#壓榨油.md "wikilink")，而且含有[不飽和脂肪酸約](../Page/不飽和脂肪酸.md "wikilink")90%，也是種很多人都認為健康的[食用油](../Page/食用油.md "wikilink")。

## 食用

### 成份

以100公克中，泰半是脂肪，主要是[單元不飽和脂肪和](../Page/單元不飽和脂肪.md "wikilink")[多元不飽和脂肪](../Page/多元不飽和脂肪.md "wikilink")，其中[亞油酸](../Page/亞油酸.md "wikilink")（亞麻油酸）所佔比例為高。此外，種子含有可能有助於降低[血脂](../Page/血脂.md "wikilink")（血液膽固醇\[1\]）水平的[植物固醇](../Page/植物固醇.md "wikilink")。

### 壓榨油

  - 主條目：**、

原生向日葵油（亞油酸向日葵油）包含高不飽和脂肪酸（約68％的[亞油酸](../Page/亞油酸.md "wikilink")）和低飽和[脂肪酸](../Page/脂肪酸.md "wikilink")（如[棕櫚酸和](../Page/棕櫚酸.md "wikilink")[硬脂酸](../Page/硬脂酸.md "wikilink")）。然而因種種目的，開發各[混種](../Page/雜交種.md "wikilink")，以改變作物的脂肪酸譜。\[2\]

#### 與其他植物油的比較

## 参见

  -
<File:Sunflower_seed444.jpg> <File:Sunflower_seeds.jpg>
<File:Sunflower_Seeds_Kaldari.jpg> <File:SunflowerSeed2009.JPG>

## 參考來源

  - 引用

<!-- end list -->

  - 書目

<!-- end list -->

  - 歐陽英：《生機食療實務大全》，躍昇文化出版社，初版16刷2002年12月。
  - 時報國際◎企劃製作：《堅果萬歲》，時報國際廣告股份有限公司，初版2006年2月。

[Category:可食用坚果与种子](../Category/可食用坚果与种子.md "wikilink")
[Category:零食](../Category/零食.md "wikilink")
[Category:向日葵属](../Category/向日葵属.md "wikilink")
[Category:中國賀年食品](../Category/中國賀年食品.md "wikilink")

1.
2.