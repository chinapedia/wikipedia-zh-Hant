**蒙巴頓-溫莎**（），是當今英女王[伊丽莎白二世的父系後代所冠的姓氏](../Page/伊丽莎白二世.md "wikilink")。這個名稱是源於女王在1960年發布的一道樞密院御令，御令當中宣佈其後裔的姓氏將為**蒙巴頓-溫莎**，王朝名號保持**[溫莎](../Page/溫莎王朝.md "wikilink")**。

## 背景

\[\[Image:A Good Riddance - George V of the United Kingdom cartoon in
Punch, 1917.png|thumb|**A Good riddance**
圖：英王[喬治五世於](../Page/乔治五世_\(英国\).md "wikilink")[第一次世界大戰時](../Page/第一次世界大戰.md "wikilink")，與德國斷交並開戰；為畫清界線、安撫民心並鞏固國內的民族主義，喬治五世毅然改朝換姓，改易英國王族所有有德國淵源的姓氏。

-----

Cartoon from *Punch* magazine Vol. 152, June 27, 1917, noting the UK
Royal Family's change of name to Windsor\]\]

**[蒙巴頓](../Page/蒙巴頓.md "wikilink")**一詞最早出現在1914年。那時[第一次世界大戰爆發](../Page/第一次世界大戰.md "wikilink")，[英](../Page/英國.md "wikilink")、[德交戰](../Page/德國.md "wikilink")。身居英国的[路易斯·巴滕贝格亲王在表親英王](../Page/路易斯·亞歷山大·蒙巴頓，第一代米爾福德黑文侯爵.md "wikilink")[喬治五世的建議下](../Page/乔治五世_\(英国\).md "wikilink")，將家族德文姓氏從**巴滕贝格**（）意译为英文的**蒙巴頓**，以示與德國畫清界線。

1947年12月20日，[伊丽莎白二世与其三表哥](../Page/伊丽莎白二世.md "wikilink")[希腊王子](../Page/希腊.md "wikilink")[菲利普结婚](../Page/菲利普亲王_\(爱丁堡公爵\).md "wikilink")。而菲利普亲王在结婚同年的3月18日，宣誓放弃希腊王位的继承权。希腊王室的家族姓名是[石勒蘇益格-荷爾斯泰因-索恩德堡-格呂克斯堡](../Page/石勒蘇益格-荷爾斯泰因-索恩德堡-格呂克斯堡.md "wikilink")。为了方便称呼，菲利普亲王在加入英国国籍时，将姓氏改为母姓**蒙巴頓**。

婚礼后[蒙巴顿勋爵提出英国王朝將來改名為蒙巴顿王朝的要求](../Page/路易斯·蒙巴頓，第一代緬甸的蒙巴頓伯爵.md "wikilink")。這個要求被伊丽莎白的祖母，[瑪麗太王太后否決](../Page/瑪麗王后_\(英國\).md "wikilink")。她更要求當時的[英國首相](../Page/英國首相.md "wikilink")[邱吉爾爵士將英女王伊丽莎白二世的後裔姓氏和王朝名號將維持為](../Page/温斯顿·丘吉尔.md "wikilink")**溫莎**的議案提交國會，國会其後于1952年4月通过决议。

礙於瑪麗太王太后的反對，女王一直未對子女姓氏做出決定。直至1960年瑪麗太王太后去世7年后，女王才發布一道[樞密院御令](../Page/樞密院.md "wikilink")，宣佈其後裔的姓氏將會是她丈夫和她自己姓氏的合體，即是**蒙巴頓-溫莎**，王朝名號保持溫莎。此舉被視為一個成功的折衷方案。

## 參看

  - [溫莎王朝](../Page/溫莎王朝.md "wikilink")
  - [韦廷王朝](../Page/韦廷王朝.md "wikilink")

## 資料來源

本文的[英文原本在編寫時曾徵詢過英國](../Page/w:en:Mountbatten-Windsor.md "wikilink")[白金漢宮和](../Page/白金漢宮.md "wikilink")[聖詹姆斯宮的職員](../Page/聖詹姆斯宮.md "wikilink")。

[Category:英語姓氏](../Category/英語姓氏.md "wikilink")
[Category:溫莎王朝](../Category/溫莎王朝.md "wikilink")
[Category:格呂克斯堡王朝](../Category/格呂克斯堡王朝.md "wikilink")