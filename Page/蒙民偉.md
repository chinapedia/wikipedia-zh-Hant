**蒙民偉**（**Dr. William Mong Man
Wai**，），生於[香港](../Page/香港.md "wikilink")，祖籍[廣東](../Page/廣東.md "wikilink")[番禺](../Page/番禺.md "wikilink")<small>\[1\]</small>，祖居[廣州](../Page/廣州.md "wikilink")，是[香港](../Page/香港.md "wikilink")[信興集團創辦人及董事長](../Page/信興集團.md "wikilink")，[代理](../Page/代理.md "wikilink")[樂聲牌家庭電器及影音產品](../Page/樂聲牌.md "wikilink")，人稱「蒙叔」，有「電器大王」之稱，同時兼任[東亞銀行非執行董事](../Page/東亞銀行.md "wikilink")<small>\[2\]</small>。他亦熱心於公益事務，尤其於教育方面，一些大學（[清华大学](../Page/清华大学.md "wikilink")、[北京大学](../Page/北京大学.md "wikilink")、[南京大学](../Page/南京大学.md "wikilink")、[浙江大学](../Page/浙江大学.md "wikilink")、[國立清華大學](../Page/國立清華大學.md "wikilink")、[嶺南大學](../Page/嶺南大學.md "wikilink")、[香港大學](../Page/香港大學.md "wikilink")、[香港中文大學](../Page/香港中文大學.md "wikilink")、[香港科技大學](../Page/香港科技大學.md "wikilink")、[香港理工大學](../Page/香港理工大學.md "wikilink")、[香港城市大學](../Page/香港城市大學.md "wikilink")、[香港浸會大學](../Page/香港浸會大學.md "wikilink")、[香港公開大學](../Page/香港公開大學.md "wikilink")、[香港教育大學](../Page/香港教育大學.md "wikilink")）及中學（[中華基督教會蒙民偉書院](../Page/中華基督教會蒙民偉書院.md "wikilink")）的設施都以其命名。2010年7月21日在香港[養和醫院病逝](../Page/養和醫院.md "wikilink")，終年82歲\[3\]\[4\]。

## 生平

### 背景

蒙民偉的雙親均為在[日本出生的第二代](../Page/日本.md "wikilink")[華僑](../Page/華僑.md "wikilink")，其[祖父及](../Page/祖父.md "wikilink")[外祖父在](../Page/外祖父.md "wikilink")[明治期間移居](../Page/明治.md "wikilink")[九州](../Page/九州.md "wikilink")[長崎](../Page/長崎.md "wikilink")，父母在[第一次世界大戰剛結束後成婚](../Page/第一次世界大戰.md "wikilink")，按傳統回鄉祭祖，隨後在[香港定居](../Page/香港.md "wikilink")。父親蒙國平在[三菱香港分公司任職](../Page/三菱.md "wikilink")，期間蒙民偉與大姊瑤英（Stella）、二兄民傑（Jeffrey）、四弟民德（Martin）及兩名妹妹瑤琴（Lillian）和瑤珍（Esther）先後出生。其後蒙國平辭去三菱的職務，與兩名弟弟合資經營裕信祥（Yue
Shun
Cheung），最初販賣電器用品，日後逐漸向多元化發展，包括建材、食品等出入口業務，是[旭味](../Page/旭味.md "wikilink")（Asahi
Aji）調味粉和[花王石鹼](../Page/花王株式會社.md "wikilink")（Kao Sekken）的香港代理。

在[第二次世界大戰爆發前蒙民偉在](../Page/第二次世界大戰.md "wikilink")[喇沙書院就讀](../Page/喇沙書院.md "wikilink")，[香港日治時期他到](../Page/香港日治時期.md "wikilink")[廣州繼續了一年學業](../Page/廣州.md "wikilink")。1945年舉家遷返香港，蒙民偉於1946年重返喇沙完成最後一年高中課程，並參加[清華大學入學試](../Page/清華大學.md "wikilink")，獲取錄到[北平選修航空工程](../Page/北平.md "wikilink")，惟[国共内战被迫中斷學業](../Page/国共内战.md "wikilink")，並返港加入叔父的公司工作。1949年下半年蒙民偉被父親送往[日本留學](../Page/日本.md "wikilink")，雖然父母均能操流利[日語](../Page/日語.md "wikilink")，但由於日本侵華，中國人痛恨日本人，為怕被人誤會，兩人出外絕不會說日語，在家中大部分時間亦說[粵語](../Page/粵語.md "wikilink")，而孩子們則全部使用粵語，故此蒙民偉剛到日本時連一個日本字母亦不會，寄居於父親三菱舊同事原清（Hara
Kiyoshi）在東[銀座的家中](../Page/銀座.md "wikilink")，年已22歲的他獲安排入讀千代田小學，被校內的小朋友稱為「大阿哥」（Oniichaman），僅用了1個月便唸完一年級，花了6個月便唸完小學課程，練得一口流利日語<small>\[5\]</small>。

### 創業

蒙民偉在日本學習近兩年後回港，於1952年開設信和公司，從日本進口膠花、陶瓷等雜貨，在[先施及](../Page/先施.md "wikilink")[永安等百貨公司出售](../Page/永安百貨.md "wikilink")。其父親一向與[松下電器有業務往來](../Page/松下電器.md "wikilink")，通過父親的推介，蒙民偉於1953年8月15日以3萬港元資本成立信興行（Shun
Hing
Hong），取意「有信則興」，開始與松下發展業務，公司連同蒙本人合共只有三人，當時信興行仍以進口雜貨為主，商品包括仿製真珠手袋、雨傘、運動鞋、塑膠電線等，以至78轉[黑膠唱片](../Page/黑膠唱片.md "wikilink")。

信興行最初進口的「National」[品牌產品是PL](../Page/品牌.md "wikilink")-420型[真空管手提](../Page/真空管.md "wikilink")[收音機](../Page/收音機.md "wikilink")，以[电池發動](../Page/电池.md "wikilink")，於1954年向松下訂購30部，對雙方而言均為一張大訂單，蒙民偉並為品牌取名「樂聲牌」。期間蒙民偉與松下的子公司松下電器貿易進行總代理權的商討，惟一直未能落實，最終蒙民偉與松下電器貿易行政總裁陣內恆雄（Jinnai
Tsuneo）一同到位於[大阪的松下總部會見](../Page/大阪.md "wikilink")[松下幸之助](../Page/松下幸之助.md "wikilink")，經蒙民偉游說後，兩人握手作實，松下將香港與[澳門的總代理權交予信興行](../Page/澳門.md "wikilink")，此後雙方從沒有就總代理權簽署文件，是全世界松下代理中唯一沒有書面契約的代理。

取得代理權後，蒙民偉更加落力，更向未婚妻楊雪姬許諾要賣出1萬部收音機後才結婚。1957年松下向競爭對手[索尼購買](../Page/索尼.md "wikilink")[半导体器件再裝配成自家品牌的](../Page/半导体器件.md "wikilink")[電晶體收音機](../Page/晶体管.md "wikilink")，對沒有電力供應的地區及水上居民而言，用電池發動的電晶體收音機非常方便，香港雖然是一個小市場，但蒙民偉透過華商網絡，利用香港作為自由港的優勢，將松下的產品轉口到[東南亞地區](../Page/東南亞.md "wikilink")，利用大數量爭取更優惠價格，賺取差價，其再出口產品的價格甚至比當地松下代理店還低，如[新加坡代理](../Page/新加坡.md "wikilink")「Hagenmeyer」便曾向松下總公司投訴蒙民偉的[水貨擾亂市場價格](../Page/平行進口貨品.md "wikilink")。蒙民偉僅花了一年，便於1958年達成銷售1萬部收音機的目標，並正式迎娶楊雪姬。

### 電飯煲

[HK_National_RiceCooker.JPG](https://zh.wikipedia.org/wiki/File:HK_National_RiceCooker.JPG "fig:HK_National_RiceCooker.JPG")
1955年[東芝推出名為](../Page/東芝.md "wikilink")「電飯煲」（Denkigama）的[電飯煲](../Page/電飯煲.md "wikilink")，在盛米鍋與發熱器之間注水加熱煮沸，再藉沸水間接將米煮成飯。松下其後成立電飯煲部門，其部份負責人坂本達之亮開發一款名「炊飯器」的電飯煲，直接將把鍋內的米煮成飯，免卻每次煮飯均需添水的麻煩。坂本帶了幾個新產品造訪在[東京營商的蒙國平](../Page/東京.md "wikilink")，並展示及示範其用途，蒙國平意識到新產品的商機，隨即通知在香港的兒子，當時香港人是燒[火水爐](../Page/煤油爐.md "wikilink")、炭爐或柴灶，用瓦鍋煲飯煮粥，蒙民偉亦意會到電飯煲將會帶來一場廚房現代化革命，而當時市面居領導地位的歐、美電器製造商沒有同類產品，沒有競爭對手，可以傾全力開發市場。

蒙民偉向松下訂購100個電飯煲運到香港，當時戰後不久，香港人仍有反日情緒，更認為日本貨品質差劣，戲稱為「日半貨」，意謂產品用不上一天半便會壞掉。加上電飯煲為新產品，單看其白色外表，沒法知道其用途，更有人誤會其為新式痰盂。蒙民偉採用親身示範作為推銷方式，與兩名下屬帶著電飯煲、[糯米和](../Page/糯米.md "wikilink")[臘腸挨家挨戶向主婦示範無火煮飯](../Page/臘腸.md "wikilink")，主要為[北角一帶的中產家庭](../Page/北角.md "wikilink")，贏得顧客口碑。其後蒙民偉開始在[銅鑼灣的](../Page/銅鑼灣.md "wikilink")[大丸百貨內及附近的](../Page/大丸.md "wikilink")[戲院前](../Page/戲院.md "wikilink")、[新界的](../Page/新界.md "wikilink")[餐廳和日本人俱樂部推銷電飯煲](../Page/餐廳.md "wikilink")。經過連串的努力，成功贏得「電飯煲即是樂聲牌」的印象，銷售量由最初的100個激增至1965年的每年88,000個，佔松下電飯煲出口量的一半。

電飯煲的成功，除了歸功於推銷手法得宜外，更與蒙民偉細心的觀察力分不開。他注意到香港人在煮飯時候，喜歡順便將[鹹蛋](../Page/鹹蛋.md "wikilink")、臘腸或用架盛碟上的餸菜在飯面蒸熟，但蒸的時間很重要，於是便經常打開鍋蓋檢視，但一打開蓋看，飯就容易夾生，蒙民偉因此便向松下要求在鍋蓋上嵌一個玻璃窗，以便觀察煮飯的生熟程度。松下的技師佐野啟明在鍋蓋上加上一面向內凹陷的玻璃窗，解決水蒸氣遇上玻璃凝成水份倒流的問題，而坂本再為這香港版電飯煲加上一個黑色半月形手柄，一直沿用數十年，直到電腦型電飯煲興起才止。50多年來經蒙民偉手賣出的電飯煲約有1,000萬個，一個連一個排列起來，其長度達到3,000[公里](../Page/公里.md "wikilink")，可以比擬[萬里長城](../Page/萬里長城.md "wikilink")。

### 向索尼說不

1963年蒙民偉隨同松下參加在[芝加哥舉行的](../Page/芝加哥.md "wikilink")「消費者電器展覽會」（Consumer
Electronics
Show），其時正銳意開拓[美國市場的索尼亦有參展](../Page/美國.md "wikilink")，蒙民偉在展館內碰見當時擔任副社長的[盛田昭夫](../Page/盛田昭夫.md "wikilink")，盛田與他閒聊一會後，邀請蒙民偉下次到[日本時再會面](../Page/日本.md "wikilink")。

蒙民偉在參觀完展覽會後再往[歐洲轉了一圈](../Page/歐洲.md "wikilink")，才飛赴[東京](../Page/東京.md "wikilink")，他致電盛田後兩人相約翌天吃飯。盛田在[柳橋的高級茶屋宴請蒙民偉](../Page/柳橋.md "wikilink")，席上盛田詢問他可否替索尼辦事，其時一間公司掛兩個招牌分別代理敵對公司的產品十分普遍，蒙民偉以跟[松下幸之助有約在先而予婉拒](../Page/松下幸之助.md "wikilink")。

二十多年後，盛田昭夫訪問[香港](../Page/香港.md "wikilink")，在[富麗華酒店向財經界人士發表索尼發展](../Page/富麗華酒店.md "wikilink")「[Walkman](../Page/Walkman.md "wikilink")」的演講，蒙民偉亦有列席，在聚餐及交談時間向盛田打招呼，盛田一把拉著鄰座的太太介紹說：『這一位是蒙先生。回絕索尼代理權的，世上只有他一個。』

### 三種神器

1960年代在[日本是](../Page/日本.md "wikilink")「三種神器」——黑白[電視機](../Page/電視機.md "wikilink")、[雪櫃及](../Page/冰箱.md "wikilink")[洗衣機最暢銷的年代](../Page/洗衣機.md "wikilink")。「信興行」在1960年更名為「信興電器貿易」，更獲得松下有股份的[星牌](../Page/JVC.md "wikilink")（JVC）影音產品總代理權，信興於1962年在[尖沙咀](../Page/尖沙咀.md "wikilink")[重慶大廈地下開設首家樂聲牌陳列室](../Page/重慶大廈.md "wikilink")。

蒙民偉亦開始引入松下的[白色家電](../Page/白色家電.md "wikilink")，作為頭砲的[雪櫃卻因松下沒有商業性輸出經驗而接連受挫](../Page/冰箱.md "wikilink")。首張訂單30個雪櫃包裝在密封木箱內於船運途中全數損壞；第二次訂購80部，松下汲取經驗改用框架，但卻因用錯長釘將雪櫃壁上開了一個一個小釘洞，蒙民偉一氣之下將有洞的雪櫃運回[九龍塘家中的花園](../Page/九龍塘.md "wikilink")，排成一行，晚上睡在上面。蒙民偉為此事在1966年乘以父母之名設宴招待松下高層，來賓包括松下幸之助夫婦及[松下正治夫婦](../Page/松下正治.md "wikilink")，蒙民偉致辭：『松下實在了不起。在雪櫃也開了洞，還免費附送鐵釘。我想，松下首創這種把空氣往外送的雪櫃。』把松下幸之助氣過半死。但從此松下在物流上加倍小心，更邀請蒙國平擔任顧問，輸出的[家電因改良包裝更有長足的發展](../Page/家用电器.md "wikilink")。

隨著1970年代香港新興中產階層的出現，住屋環境的改善，[無綫電視的啟播](../Page/電視廣播有限公司.md "wikilink")，香港對「三種神器」的需求日增，蒙民偉重視商機，深明以客為先的道理，更懂得產品的宣傳，早於1962年已在[彌敦道上豎立當時全港最大的霓虹光管招牌](../Page/彌敦道.md "wikilink")，並舉辦大型產品展覽會，廣設產品陳列室，配合周全的售後服務，使樂聲牌贏得消費者的口碑，迄立數十年不倒。

## 家庭

蒙民偉一直在位於[九龍塘的大宅居住](../Page/九龍塘.md "wikilink")，是其父親於1943年以22,000[日本軍用手票購入](../Page/日本軍用手票.md "wikilink")。蒙民偉的父親蒙國平於1981年去世，而母親黃花沃於8年後的1989年相繼離世。

1945年蒙民偉經父執朋友介紹，認識僑居[神戶並在](../Page/神戶.md "wikilink")[關西學院大學就讀的](../Page/關西學院大學.md "wikilink")[楊雪姬](../Page/楊雪姬.md "wikilink")（Serena
Yang
Hsueh-chi），當時蒙剛開始與松下合作，在他赴日談生意時，兩人才有機會見面。當楊雪姬畢業後，獲頒文學士學位，經蒙及其父介紹進入松下工作。1958年兩人成婚，楊雪姬為蒙家誕下三子三女，長女倩兒（Cynthia）在婚後翌年的1959年出生，1962年誕下長子德揚（David）、1964年次女蕙兒（Viola）出生，但次子德聰（Duncan）僅1歲多便因感染斑疹傷寒而早夭，楊雪姬自此放棄工作，專心留在家照顧孩子，幼子幼女德勳（Stephen）及藻兒（Josephine）分別於1970年及1971年出生。

蒙民偉經常往內地公幹，在[上海投資時](../Page/上海.md "wikilink")，認識了比他年輕40多年在日本餐廳工作的女侍應王蓓芬，其後更協助王蓓芬取得單程證來港定居，兩人育有幼女珮兒（Perlie），而蒙民偉與楊雪姬維持了44年的婚姻最終步向破裂，於2001年宣佈[分居](../Page/分居.md "wikilink")，一年後正式[離婚](../Page/離婚.md "wikilink")，高院法官[夏正民](../Page/夏正民.md "wikilink")（Justice
Michael
Hartmann）基於蒙、楊年紀大，而雙方子女已成年，運用酌情權，將42日冷靜期濃縮為一日，於2002年1月22日雙方回復自由身，涉及46億港元巨額家產，兩人最終庭外和解，傳聞楊獲得10億港元[贍養費](../Page/贍養費.md "wikilink")。蒙民偉離婚後順理成章與王蓓芬註冊結婚。

楊雪姬於丈夫與王蓓芬之女兒（珮兒）出生後，與蒙民偉的關係日漸疏離，毅然決定於1994年在62歲之齡以訪問學生身份進入[香港大學修習心理學](../Page/香港大學.md "wikilink")，更成為心理學系名譽高級附屬研究員，其後於2010年獲香港大學頒授名譽社會科學博士學位。

長子蒙德揚在[洛杉磯加利福尼亞大學](../Page/洛杉磯加利福尼亞大學.md "wikilink")（UCLA）取得電子工程學位，於1985年回港渡寒假時被蒙民偉安排到松下實習，一住四年，再赴[聖塔克萊拉大學](../Page/聖塔克萊拉大學.md "wikilink")（Santa
Clara
University）修讀[工商管理碩士](../Page/工商管理碩士.md "wikilink")，期間更負責信興在[旧金山的分公司營運工作](../Page/旧金山.md "wikilink")。於1991年學成後回港正式加入信興集團，逐漸接棒成為第二代領導人。蒙德揚的妻子為居港日僑園田惠美（Sonoda
Emi），外父是[味之素的總裁之一](../Page/味之素.md "wikilink")。蒙民偉長女蒙倩兒在信興投資部門擔任兼職，而其餘三名兒女則長居[美國](../Page/美國.md "wikilink")。蒙民偉疼愛幼女珮兒，她自4歲開始習琴，考獲鋼琴演奏級，曾在[劉詩昆中心舉行的青少年鋼琴比賽中奪冠](../Page/劉詩昆.md "wikilink")，蒙民偉更安排她在信興集團創業55周年晚宴上獻技表演，並在「郎朗樂聲獻香江」演奏會中與[郎朗表演四手聯彈](../Page/郎朗.md "wikilink")。

蒙民偉工餘時喜歡[釣魚](../Page/釣魚.md "wikilink")、[游泳及打](../Page/游泳.md "wikilink")[高爾夫球](../Page/高爾夫球.md "wikilink")，能操流利[粵語](../Page/粵語.md "wikilink")、[普通話](../Page/普通話.md "wikilink")、[滬語](../Page/滬語.md "wikilink")、[英語及](../Page/英語.md "wikilink")[日語](../Page/日語.md "wikilink")。他經常對員工說「要積穀防飢、絕不投機」。

蒙民偉於1995年證實患上[前列腺癌](../Page/前列腺癌.md "wikilink")，於2010年年初病情持續惡化，入住[養和醫院](../Page/養和醫院.md "wikilink")，更缺席信興每年舉辦的[傳媒](../Page/傳媒.md "wikilink")[春茗](../Page/春茗.md "wikilink")，其後出現[肺炎及血科病](../Page/肺炎.md "wikilink")，特首[曾蔭權和商界朋友都曾往探望](../Page/曾蔭權.md "wikilink")，留醫多月後，於7月21日傍晚病逝，家人子女陪伴在側<small>\[6\]</small>。7月29日在[香港殯儀館一樓大堂設靈](../Page/香港殯儀館.md "wikilink")，曾蔭權伉儷、中聯辦副主任[黎桂康](../Page/黎桂康.md "wikilink")、香港大學校長[徐立之](../Page/徐立之.md "wikilink")、中文大學校長[沈祖堯和](../Page/沈祖堯.md "wikilink")[新世界集團主席](../Page/新世界集團.md "wikilink")[鄭裕彤等到場致祭](../Page/鄭裕彤.md "wikilink")<small>\[7\]</small>，翌日早上8時30分開始安所儀式，徐立之和蒙民偉兒子蒙德勳先後致辭，9時30分舉殯，葬禮以[天主教儀式舉行](../Page/天主教.md "wikilink")，擔任扶靈嘉賓有[曾俊華](../Page/曾俊華.md "wikilink")、阪本俊弘、徐立之、松永大介、[周亦卿](../Page/周亦卿.md "wikilink")、[顧秉林](../Page/顧秉林.md "wikilink")、李岩松、潘永華、陳瑞顯和[梁智仁](../Page/梁智仁.md "wikilink")，靈柩送往歌連臣角火葬場火化<small>\[8\]</small>。

### 爭產風波

2010年10月20日在蒙民偉身故後不足百日，蒙的前妻楊雪姬與所生的五子女入稟[香港高等法院](../Page/香港高等法院.md "wikilink")，被告的兩間公司是信託基金「The
Huge Surplus
Trust」的受託人，以信託身份持有市值50億港元「信興集團」一半股份的資產，原訴人要求法庭聲明他們在信託所佔的實益擁有權，下令兩名受託人交代賬目及交出資產，如有需要會要求頒佈禁制令或賠償損失\[9\]。

## 慈善活動

[HK_HKCU_MongManWaiBuilding.JPG](https://zh.wikipedia.org/wiki/File:HK_HKCU_MongManWaiBuilding.JPG "fig:HK_HKCU_MongManWaiBuilding.JPG")
[HK_HKPU_MongManWaiBuilding.JPG](https://zh.wikipedia.org/wiki/File:HK_HKPU_MongManWaiBuilding.JPG "fig:HK_HKPU_MongManWaiBuilding.JPG")
[HK_HKBU_WilliamMWMongCourtyard.JPG](https://zh.wikipedia.org/wiki/File:HK_HKBU_WilliamMWMongCourtyard.JPG "fig:HK_HKBU_WilliamMWMongCourtyard.JPG")
[HK_HKIE_MongManWaiLibrary.JPG](https://zh.wikipedia.org/wiki/File:HK_HKIE_MongManWaiLibrary.JPG "fig:HK_HKIE_MongManWaiLibrary.JPG")
蒙民偉於1984年設立「信興教育及慈善基金」以回饋社會，並將信興集團的百分之十的利潤撥入基金。自以成立來，本著「取諸社會，用諸社會」的宗旨，支持各類公益慈善活動，截至2009年捐贈已逾5億港元。

蒙民偉熱心資助教育文化事業，惟捐資一定是要幫助理工科，在[香港各所知名大學內也可以看到以蒙民偉命名的樓宇](../Page/香港.md "wikilink")，包括[香港中文大學科研大樓](../Page/香港中文大學.md "wikilink")、[香港理工大學教學大樓](../Page/香港理工大學.md "wikilink")、[香港大學的醫學大樓](../Page/香港大學.md "wikilink")、[香港城市大學行政及辦公室大樓](../Page/香港城市大學.md "wikilink")、[香港嶺南大學學生宿舍大樓等](../Page/香港嶺南大學.md "wikilink")。在[香港浸會大學還有蒙民偉廣場](../Page/香港浸會大學.md "wikilink")，[香港公開大學有蒙民偉電腦實驗室](../Page/香港公開大學.md "wikilink")，[香港教育大學有蒙民偉圖書館](../Page/香港教育大學.md "wikilink")，[香港科技大學則成立了香港首間](../Page/香港科技大學.md "wikilink")「蒙民偉半導體實驗室」，促進[納米科技研究](../Page/納米.md "wikilink")。蒙民偉亦有資助[中國內地及海外的教育事業](../Page/中國.md "wikilink")，在[清華大學有學生文化活動中心](../Page/清華大學.md "wikilink")、蒙民偉理學館、醫學與系統生物學研究所<small>\[10\]</small>、清華大學—香港中文大學蒙民偉眼科中心等，同時捐助[南京大學科學技術館](../Page/南京大學.md "wikilink")<small>\[11\]</small>、[暨南大學理工樓](../Page/暨南大學.md "wikilink")、[天津](../Page/天津.md "wikilink")[南開大學教學實驗大樓](../Page/南開大學.md "wikilink")<small>\[12\]</small>、[台灣](../Page/台灣.md "wikilink")[國立清華大學學生文化活動中心等](../Page/國立清華大學.md "wikilink")。1996年蒙民偉捐贈150萬[英鎊予](../Page/英鎊.md "wikilink")[英國](../Page/英國.md "wikilink")[剑桥大学悉尼·苏塞克斯学院](../Page/剑桥大学悉尼·苏塞克斯学院.md "wikilink")，興建一幢多用途講學及會議廳大樓，獲命名為「蒙民偉樓」，是劍橋大學首幢以[亞洲人命名的建築物](../Page/亞洲人.md "wikilink")。

2007年11月在蒙民偉慶祝八十大壽時，基金斥資1,000萬港元成立「蒙民偉國內香港大學交換生獎學金」，分別資助6所內地大學及6所本地大學的理工及工程科的學生進行交流<small>\[13\]</small>。

由「信興教育及慈善基金」[贊助或興建以蒙民偉命名的教育大樓包括](../Page/贊助.md "wikilink")：

1.  英國劍橋大學「蒙民偉樓」；
2.  清華大學「清華大學·香港中文大學·蒙民偉眼科中心」；
3.  清華大學「蒙民偉理科館」；
4.  清華大學「蒙民偉醫學系統生物學研究所」；
5.  清華大學「蒙民偉樓── 學生文化活動中心」；
6.  國立清華大學「蒙民偉樓── 學生文化活動中心」；
7.  南京大學「蒙民偉樓」；
8.  南開大學「蒙民偉樓」；
9.  浙江大學「蒙民偉樓」；
10. 暨南大學「蒙民偉理工樓」；
11. 香港大學「蒙民偉樓」；
12. 香港中文大學「蒙民偉樓」；
13. 香港中文大學「蒙民偉工程學大樓」；
14. 香港科技大學「蒙民偉納米科技研究所」；
15. 香港城市大學「蒙民偉樓」；
16. 香港理工大學「蒙民偉樓」；
17. 香港浸會大學「偉衡體育中心」（與[何善衡聯合命名](../Page/何善衡.md "wikilink")）；
18. 香港浸會大學「蒙民偉廣場」<small>\[14\]</small>；
19. 香港公開大學「蒙民偉電腦實驗室」；
20. 香港教育大學「蒙民偉圖書館」；
21. 嶺南大學「蒙民偉學生宿舍」。

以蒙民偉或家人命名的學校：

  - [中華基督教會蒙民偉書院](../Page/中華基督教會蒙民偉書院.md "wikilink")；
  - 中華基督教會蒙黃花沃紀念小學，以蒙民偉先母黃花沃命名，位於[屯門](../Page/屯門.md "wikilink")[田景邨](../Page/田景邨.md "wikilink")，是香港第二所採用標準中學校舍的小學；
  - 已停辦的香港學位教師會蒙楊雪姬幼稚園，以蒙民偉前妻蒙楊雪姬命名，位於[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")；

## 公職

  - 曾任一國兩制研究中心顧問委員會委員；
  - 曾獲委任為基本法諮詢委員會、四川省政協委員會及深圳市政協委員會委員；
  - 香港電器製造業協會名譽主席；
  - [南京大學董事會名譽董事長](../Page/南京大學.md "wikilink")；
  - [清華大學教育基金會顧問及清華大學高等研究中心基金會董事](../Page/清華大學.md "wikilink")；
  - [北京大學名譽校董及教育基金會名譽理事](../Page/北京大學.md "wikilink")；
  - [上海交通大學董事會名譽董事](../Page/上海交通大學.md "wikilink")；
  - [廣東暨南大學董事會董事](../Page/暨南大学.md "wikilink")；
  - [香港科技大學顧問委員會榮譽委員](../Page/香港科技大學.md "wikilink")；
  - [香港浸會大學諮議會榮譽委員](../Page/香港浸會大學.md "wikilink")；
  - 喇沙基金會會員。

## 榮譽

  - 1988年獲[日本天皇裕仁授予勳四等](../Page/昭和天皇.md "wikilink")[瑞寶章](../Page/瑞寶章.md "wikilink")；
  - 1994年獲法蘭西委員會（Comite de France）國際傑出人物金盃獎；
  - [香港理工大學](../Page/香港理工大學.md "wikilink")、[香港大學](../Page/香港大學.md "wikilink")、[香港城市大學](../Page/香港城市大學.md "wikilink")、[香港公開大學](../Page/香港公開大學.md "wikilink")、[香港中文大學](../Page/香港中文大學.md "wikilink")、[谢菲尔德大学及](../Page/谢菲尔德大学.md "wikilink")[香港科技大學頒授榮譽博士學位](../Page/香港科技大學.md "wikilink")；
  - [清華大學頒發顧問教授頭銜及名譽博士學位](../Page/清華大學.md "wikilink")；
  - [北京大學及](../Page/北京大學.md "wikilink")[上海交通大學頒發顧問教授頭銜](../Page/上海交通大學.md "wikilink")；
  - [浙江大學頒授名譽教授頭銜](../Page/浙江大學.md "wikilink")；
  - [劍橋大學授予榮譽院士榮銜](../Page/劍橋大學.md "wikilink")；
  - [紫金山天文台於](../Page/紫金山天文台.md "wikilink")1996年將編號3678之[小行星命名為](../Page/小行星.md "wikilink")「蒙民偉星」；
  - [南京市政府更頒贈榮譽市民證書](../Page/南京市.md "wikilink")；
  - 2006年獲[香港特別行政區政府頒授](../Page/香港特別行政區.md "wikilink")[金紫荊星章](../Page/金紫荊星章.md "wikilink")。

## 參考資料

  - 書本

<!-- end list -->

  - 《由樂聲牌電飯煲而起》，利琦珍、中野嘉子、王向華 合著，香港大學出版社，ISBN 962-209-682-4

<!-- end list -->

  - 註腳

## 外部連結

  - [信興集團](http://www.shunhinggroup.com/site/chinese/index.aspx)
  - [東周刊：電器大王蒙民偉病重入院](http://www.eastweek.com.hk/index.php?link=content_detail&article_id=5990)
  - [作風穩健無懼世紀衝擊—五十五周年前夕訪信興集團主席蒙民偉](https://web.archive.org/web/20100310121907/http://www.shunhinggroup.com/site/chinese/news_120.aspx)
  - [蒙民偉肩負推廣文化重任](http://www.yzzk.com/cfm/Content_Archive.cfm?Channel=ed&Path=2390910282/24eda.cfm)
  - [GG細語：拜別蒙民偉](http://hk.apple.nextmedia.com/template/apple_sub/art_main.php?iss_id=20100728&sec_id=12187389&art_id=14283949)
  - [終身學習心理學 楊雪姬](http://paper.wenweipo.com/2010/03/11/FC1003110003.htm)

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港慈善家](../Category/香港慈善家.md "wikilink")
[Category:東亞銀行](../Category/東亞銀行.md "wikilink")
[M](../Category/喇沙書院校友.md "wikilink")
[Category:广州人](../Category/广州人.md "wikilink")
[Min民偉](../Category/蒙姓.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:香港公開大學榮譽博士](../Category/香港公開大學榮譽博士.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:四川省政協委員](../Category/四川省政協委員.md "wikilink")
[Category:深圳市政協委員](../Category/深圳市政協委員.md "wikilink")
[Category:香港城市大學榮譽博士](../Category/香港城市大學榮譽博士.md "wikilink")
[Category:香港科技大學榮譽博士](../Category/香港科技大學榮譽博士.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:香港理工大學榮譽博士](../Category/香港理工大學榮譽博士.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.