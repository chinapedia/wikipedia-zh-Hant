**蒙特苏馬二世**（；
；），一譯**蒙特祖馬二世**、**蒙特蘇马·索科約特辛**（“年轻的蒙特蘇马”），古代[墨西哥](../Page/墨西哥.md "wikilink")[阿茲特克帝國的](../Page/阿茲特克帝國.md "wikilink")[特諾奇蒂特兰君主](../Page/特諾奇蒂特兰.md "wikilink")。他曾一度稱霸[中美洲](../Page/中美洲.md "wikilink")，最後卻被[西班牙征服者](../Page/西班牙.md "wikilink")[埃尔南·科爾特斯所征服](../Page/埃尔南·科爾特斯.md "wikilink")，導致[阿茲特克帝國灭亡](../Page/阿茲特克帝國.md "wikilink")。

## 有關蒙特蘇馬二世的歷史資料

### 《[征服新西班牙信史](../Page/征服新西班牙信史.md "wikilink")》

於十六世紀初入主中美洲的[西班牙人](../Page/西班牙.md "wikilink")，曾經對蒙特蘇馬二世作了不少記述。較為重要的一部，是士兵[貝爾納爾·迪亞斯·德爾·卡斯蒂略的](../Page/貝爾納爾·迪亞斯·德爾·卡斯蒂略.md "wikilink")《[征服新西班牙信史](../Page/征服新西班牙信史.md "wikilink")》。該書提到[埃尔南·科爾特斯對](../Page/埃尔南·科爾特斯.md "wikilink")[阿茲特克地區的征戰](../Page/阿茲特克.md "wikilink")，以及蒙特蘇馬二世與[中美洲諸部落間縱橫捭闔的關係](../Page/中美洲.md "wikilink")。作者亦親身參與征戰活動，還曾晉見過蒙特蘇馬二世，故該書史料價值極高。

### 《[西印度毀滅述略](../Page/西印度毀滅述略.md "wikilink")》

當代的一位神父[巴托洛梅·德拉斯·卡薩斯所撰寫的](../Page/巴托洛梅·德拉斯·卡薩斯.md "wikilink")《[西印度毀滅述略](../Page/西印度毀滅述略.md "wikilink")》中的篇章《新西班牙的毁滅》，亦簡明扼要地談到蒙特蘇馬二世的敗亡過程\[1\]。

## 蒙特蘇馬二世治下的[墨西哥城](../Page/墨西哥城.md "wikilink")

[Houghton_Typ_625.99.800_Istoria_della_conquista_del_Messico_-_Motezuma.jpg](https://zh.wikipedia.org/wiki/File:Houghton_Typ_625.99.800_Istoria_della_conquista_del_Messico_-_Motezuma.jpg "fig:Houghton_Typ_625.99.800_Istoria_della_conquista_del_Messico_-_Motezuma.jpg")

### 城守

據十六世紀[西班牙征服者留下的資料說](../Page/西班牙.md "wikilink")，該城「築於湖上，只有幾條堤道可以通往城內，每條堤道有幾個口子，上面架有木橋，進出城都要從這些橋上通過，一旦把橋吊起，就無法從橋的這頭走到另一頭，也就無法進入城內；城裡大部份房屋建於湖上，如無吊橋或小船，房屋之間便不能相通，每幢房子的屋頂上都有平台，平台上修有壁障，可供作戰用。」\[2\]
將建築物築於水上的設計，並在屋頂修整戰鬥設施，對於守城起到了相當大的幫助。據[卡斯蒂略所說](../Page/貝爾納爾·迪亞斯·德爾·卡斯蒂略.md "wikilink")，西班牙人在1521年攻打[墨西哥城新君主](../Page/墨西哥城.md "wikilink")[夸烏特莫克時](../Page/夸烏特莫克.md "wikilink")，每攻到一個建築物時，隨即被前面的水道攔阻，難以迅速行軍，他又提到當時西班牙軍的狼狽：「我們便想方設法把我們攻佔的水道堵死，把房屋或街區拆毁、夷平，因放火燒須焚燒很久，又因每幢房屋都建在水上，不過橋或坐船，便無法從一處到另一處去，所以火勢也無法從一幢房屋傳到另一座房屋去。我們若從水上泅渡，他們便從屋頂給我們以重創。」\[3\]

### 市集

蒙特蘇馬二世時，[墨西哥城的商業既繁榮又具規模](../Page/墨西哥城.md "wikilink")，是[中美洲的商業中心](../Page/中美洲.md "wikilink")，「整個[新西班牙出售的商品](../Page/新西班牙.md "wikilink")，這裡樣樣皆有」；「平民和商人熙來攘往，一切井然有序」；「各種商品分開出售，絕不相混，而且都有固定的攤住」。而貨品亦繁多，有金銀、寶石、羽毛、布匹、刺綉品、奴隸、家禽、水果、熟食、木材、煙草等等。\[4\]

### 神廟

蒙特蘇馬二世時代，[墨西哥城內有眾多廟宇](../Page/墨西哥城.md "wikilink")，而且都是富麗堂皇，「每四五個街區就有一座神廟及其偶像。」而[特諾奇蒂特蘭人所信奉的神祇](../Page/特諾奇蒂特蘭.md "wikilink")，則有戰神[維齊洛波奇特利神](../Page/維齊洛波奇特利.md "wikilink")、大人物、男子及女子的婚姻守護神等。此外，墨西哥城有大量祭司，負責向神祇獻祭。
當中較為重要的，是戰神的大神廟。這座神廟範圍廣闊，蒙特蘇馬二世本人亦會親自到該廟致祭。廟內的祭司，會用人的心臟作祭品。\[5\]

## 與[阿茲特克各部落的關係](../Page/阿茲特克.md "wikilink")

[Motzume.jpg](https://zh.wikipedia.org/wiki/File:Motzume.jpg "fig:Motzume.jpg")，1584年\]\]

### 貢稅冊

蒙特蘇馬二世對於他所統屬的部落，每當要徵收貢品時，都會依「貢稅冊」來收取。貢稅冊記載了納貢的[黃金等物從何處運來](../Page/金.md "wikilink")，何處有金礦、[可可及棉布製品](../Page/可可.md "wikilink")；從貢稅冊內可以看到這些地區向[墨西哥城納貢的數字](../Page/墨西哥城.md "wikilink")。\[6\]

## [墨西哥城的動亂與身亡](../Page/墨西哥城.md "wikilink")

### [埃尔南·科爾特斯部將](../Page/埃尔南·科爾特斯.md "wikilink")[阿爾瓦拉多在](../Page/阿爾瓦拉多.md "wikilink")[墨西哥城被圍](../Page/墨西哥城.md "wikilink")

當[埃尔南·科爾特斯在](../Page/埃尔南·科爾特斯.md "wikilink")[墨西哥城駐紥了一段日子後](../Page/墨西哥城.md "wikilink")，他的政治對手[古巴總督](../Page/古巴.md "wikilink")[迭戈·貝拉斯克斯](../Page/迭戈·貝拉斯克斯.md "wikilink")（Diego
Velázquez de Cuéllar）派出[納瓦埃斯](../Page/納瓦埃斯.md "wikilink")（Pánfilo de
Narváez），務求追捕[科爾特斯](../Page/埃尔南·科爾特斯.md "wikilink")。[科爾特斯親自率軍迎戰](../Page/埃尔南·科爾特斯.md "wikilink")，期間只留下[阿爾瓦拉多](../Page/阿爾瓦拉多.md "wikilink")（Pedro
de
Alvarado）駐紥[墨西哥城](../Page/墨西哥城.md "wikilink")。[科爾特斯擊敗](../Page/埃尔南·科爾特斯.md "wikilink")[納瓦埃斯後](../Page/納瓦埃斯.md "wikilink")，回[墨西哥城時郤得悉](../Page/墨西哥城.md "wikilink")，在他離開期間，[阿爾瓦拉多在一次](../Page/阿爾瓦拉多.md "wikilink")[阿茲特克人的祭神活動中](../Page/阿茲特克.md "wikilink")，竟出兵襲擊，多名[阿茲特克人被殺傷](../Page/阿茲特克.md "wikilink")。（《征服新西班牙信史》載，[阿爾瓦拉多向](../Page/阿爾瓦拉多.md "wikilink")[埃尔南·科爾特斯解釋是由於](../Page/埃尔南·科爾特斯.md "wikilink")[阿茲特克要殺他](../Page/阿茲特克.md "wikilink")，他才先下手為強。而蒙特蘇馬二世則「不願意看到這樣的事發生」，他「倒是安撫墨西哥人，勸他們停止進攻」。）事件令阿茲特克人大為憤恨，這亦是[墨西哥城動亂的導火線](../Page/墨西哥城.md "wikilink")。於是便攻擊[阿爾瓦拉多的駐軍](../Page/阿爾瓦拉多.md "wikilink")。一直到[埃尔南·科爾特斯返回](../Page/埃尔南·科爾特斯.md "wikilink")[墨西哥城時](../Page/墨西哥城.md "wikilink")，便遭受更大規模的襲擊。

### 遇害身亡

[埃尔南·科爾特斯回到](../Page/埃尔南·科爾特斯.md "wikilink")[墨西哥城時](../Page/墨西哥城.md "wikilink")，第一時間便是去找蒙特蘇馬二世，但卻惹來更多[阿茲特克人來襲](../Page/阿茲特克.md "wikilink")。[埃尔南·科爾特斯利用蒙特蘇馬二世與起事者談判](../Page/埃尔南·科爾特斯.md "wikilink")，但[阿茲特克人已另立](../Page/阿茲特克.md "wikilink")[奎特拉瓦克](../Page/奎特拉瓦克.md "wikilink")（Cuitláuac）為新王，談判註定失敗。

據[貝爾納爾·迪亞斯·德爾·卡斯蒂略所說](../Page/貝爾納爾·迪亞斯·德爾·卡斯蒂略.md "wikilink")，蒙特蘇馬二世是在[西班牙士兵的看守下](../Page/西班牙.md "wikilink")，到住所的屋頂與[阿茲特克人談判](../Page/阿茲特克.md "wikilink")。突然[阿茲特克人向蒙特蘇馬二世投擲無數石塊和投槍](../Page/阿茲特克.md "wikilink")，三塊石頭擊中他，一塊擊中頭部，一塊擊中手臂，一塊擊中腿。在旁的西班牙人要求蒙特蘇馬二世接受治療，但他不接受，並在不久後死去。時為1520年6、7月間。\[7\]

### 蒙特蘇馬二世死後的亂局

蒙特蘇馬二世死後，[埃尔南·科爾特斯決定率領](../Page/埃尔南·科爾特斯.md "wikilink")[西班牙士兵及同行的其他](../Page/西班牙.md "wikilink")[印第安人](../Page/印第安人.md "wikilink")，撤離墨西哥城。在撤走時，[阿茲特克對之窮追猛打](../Page/阿茲特克.md "wikilink")。雖然[埃尔南·科爾特斯最終成功撤離](../Page/埃尔南·科爾特斯.md "wikilink")，但據估計，約有860[西班牙人及](../Page/西班牙.md "wikilink")1200[特拉斯卡拉人](../Page/特拉斯卡拉.md "wikilink")（與[埃尔南·科爾特斯同盟的](../Page/埃尔南·科爾特斯.md "wikilink")[印第安人部族](../Page/印第安人.md "wikilink")）被殺。這次事件發生在1520年6、7月間的一個晚上，歷史上稱之為「[悲痛之夜](../Page/悲痛之夜.md "wikilink")」（La
Noche Triste）。

[西班牙人遭此慘敗](../Page/西班牙.md "wikilink")，決定要反攻[墨西哥城](../Page/墨西哥城.md "wikilink")。1521年年中，[埃尔南·科爾特斯率軍圍攻](../Page/埃尔南·科爾特斯.md "wikilink")[墨西哥城](../Page/墨西哥城.md "wikilink")，經過93天日以繼夜的激戰後，[阿茲特克的君王](../Page/阿茲特克.md "wikilink")[夸烏特莫克終於被擒](../Page/夸烏特莫克.md "wikilink")，[阿茲特克至此敗亡](../Page/阿茲特克.md "wikilink")。

## 注釋

<div class="貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第七十一章》（上冊），北京商務印書館1991年版，第161頁。">

<div class="貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第一百三十二章》（下冊），北京商務印書館1991年版，第86頁。">

<div class="貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第八十五章》（上冊），北京商務印書館1991年版，第210-211頁">

<div class="貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第八十五章》（上冊），北京商務印書館1991年版，第212-219頁。">

<div class="貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第一百三十七章》（下冊），北京商務印書館1991年版，第123頁。">

<div class="貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第一百十五章》（下冊），北京商務印書館1991年版，第10-11頁。">

<references />

</div>

## 参考文献

  - 《[征服新西班牙信史](../Page/征服新西班牙信史.md "wikilink")》，[貝爾納爾·迪亞斯·德爾·卡斯蒂略著](../Page/貝爾納爾·迪亞斯·德爾·卡斯蒂略.md "wikilink")，江禾、林光譯，[北京](../Page/北京.md "wikilink")[商務印書館](../Page/商務印書館.md "wikilink")1991年版。

## 参见

  - [埃尔南·科爾蒂斯](../Page/埃尔南·科爾蒂斯.md "wikilink")
  - [阿茲特克](../Page/阿茲特克.md "wikilink")
  - [墨西哥城](../Page/墨西哥城.md "wikilink")

[Category:墨西哥人](../Category/墨西哥人.md "wikilink")
[Category:特拉托阿尼](../Category/特拉托阿尼.md "wikilink")
[Category:墨西哥歷史](../Category/墨西哥歷史.md "wikilink")

1.
2.  貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第七十一章》（上冊），北京商務印書館1991年版，第161頁。
3.  貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第一百三十二章》（下冊），北京商務印書館1991年版，第86頁。
4.  貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第八十五章》（上冊），北京商務印書館1991年版，第210-211頁。
5.  貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第八十五章》（上冊），北京商務印書館1991年版，第212-219頁。
6.  貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第一百三十七章》（下冊），北京商務印書館1991年版，第123頁。
7.  貝爾納爾·迪亞斯·德爾·卡斯蒂略《征服新西班牙信史‧第一百十五章》（下冊），北京商務印書館1991年版，第10-11頁。