**蒙骜**（）
，《[戰國策](../Page/戰國策.md "wikilink")》中作**蒙傲**\[1\]，[中国](../Page/中国.md "wikilink")[战国时](../Page/战国.md "wikilink")[秦国名将](../Page/秦国.md "wikilink")。官位和[爵位达到](../Page/爵位.md "wikilink")[上卿](../Page/上卿.md "wikilink")。子[蒙武](../Page/蒙武.md "wikilink")，孙[蒙恬](../Page/蒙恬.md "wikilink")、[蒙毅也都是秦国和](../Page/蒙毅.md "wikilink")[秦朝的名将](../Page/秦朝.md "wikilink")。

## 早年经历

蒙骜的出生年月不详，早年的经历史书上记载也很简单，只知道他原是[齐国人](../Page/齐国.md "wikilink")，在[秦昭襄王时投奔秦国](../Page/秦昭襄王.md "wikilink")，他在秦昭襄王时期和[秦孝文王时期的经历也没有具体的记载](../Page/秦孝文王.md "wikilink")。

## 在秦莊襄王時期的為將經歷

公元前249年[秦莊襄王繼位](../Page/秦莊襄王.md "wikilink")，蒙骜從此出现在歷史記载中。

同年，蒙骜奉秦庄襄王命进攻[韩国](../Page/韩国_\(战国\).md "wikilink")，攻占[成皋和](../Page/成皋.md "wikilink")[荥阳](../Page/荥阳.md "wikilink")。秦国在此建立了[三川郡](../Page/三川郡.md "wikilink")。另据《[史记](../Page/史记.md "wikilink")·秦本纪》记载此战之后，韩国割让成皋和[巩给秦国](../Page/巩.md "wikilink")，秦国设立三川郡，秦国的疆界到达了[魏国的都城](../Page/魏国_\(战国\).md "wikilink")[大梁](../Page/大梁.md "wikilink")。

据《[史记](../Page/史记.md "wikilink")·秦本纪》记载公元前248年率军进攻[赵国](../Page/赵国.md "wikilink")，平定[太原](../Page/太原.md "wikilink")。

据《史记·秦本纪》记载公元前248年带兵攻打魏国，攻克了[高都和](../Page/高都.md "wikilink")[汲](../Page/汲.md "wikilink")。

公元前248年率军进攻[赵国](../Page/赵国.md "wikilink")，攻占[榆次](../Page/榆次.md "wikilink")、[新城](../Page/朔城區.md "wikilink")、[狼孟等三十七城](../Page/狼孟.md "wikilink")。而根据《史记·秦本纪》记载此役发生在公元前247年。

公元前247年在[河外和](../Page/河外.md "wikilink")[信陵君率领的](../Page/信陵君.md "wikilink")[燕](../Page/燕国.md "wikilink")、赵、韩、[楚](../Page/楚国.md "wikilink")、魏五国联军作战失利，被迫退回秦国。据记载，当时秦国不断的进攻魏国，魏国赦免并召回因[窃符救赵事件而流亡在赵国的信陵君](../Page/信陵君#窃符救赵.md "wikilink")，信陵君因此组织了五国联军对抗秦军。显然在此之前蒙骜应该在率领军队进攻魏国。

## 秦王政时期

公元前246年秦王政继位。在[秦王政继位的初年](../Page/秦王政.md "wikilink")，蒙骜与[王龁](../Page/王龁.md "wikilink")、[麃公三人成为了当时秦国最重要的将领](../Page/麃公.md "wikilink")。

据《史记·秦本纪》记载，同年[晋阳反叛](../Page/晋阳.md "wikilink")，被蒙骜平定。而据《史记·六国年表》记载是秦军攻占了赵国的晋阳。可能是被秦国占领的原属赵国的晋阳又叛归赵国，但又被秦军攻占。

公元前244年率军进攻韩，攻克十三城。

据《史记·秦本纪》记载同年进攻魏国的[畼和](../Page/畼.md "wikilink")[有诡](../Page/有诡.md "wikilink")，到第二年（公元前243年）攻占了这两城。

公元前242年攻魏国，攻占[酸枣](../Page/酸枣.md "wikilink")、[燕](../Page/燕_\(古城\).md "wikilink")、[虚](../Page/虚.md "wikilink")、[长平](../Page/长平.md "wikilink")、[雍丘](../Page/雍丘.md "wikilink")、[山阳等二十城](../Page/山阳.md "wikilink")。并建立了[东郡](../Page/东郡.md "wikilink")。

## 死亡

公元前240年，秦军分两路：一路由名将蒙骜统帅，北出太行，攻打赵国的龙（今河北行唐）、孤（今河北行唐北）、庆都（今河北行唐附近），试图切断邯郸周围地区与北方代、雁门的联系，防止李牧南下救援邯郸。另一路则由[秦始皇的弟弟长安君](../Page/秦始皇.md "wikilink")[成蛟率领](../Page/成蛟.md "wikilink")，预期从上党的屯留（今山西屯留南）东出太行，直逼赵国都城邯郸。[庞煖派兵埋伏于太行山密林深处](../Page/庞煖.md "wikilink")，乱箭射杀蒙骜，秦军大败。\[2\]

## 家族

蒙骜是秦国蒙氏家族开创者，为蒙氏家族在秦国的立足开创了非常好的基础。其子蒙武、孙蒙恬、蒙毅等都相继为将，显然都是与蒙骜的战功分不开的。蒙骜仅在其被史书记载的九年里几乎每年的作为主将带兵出征，总共攻克七十余城，在秦国历史上也不多见。

## 参见

  - [秦朝](../Page/秦朝.md "wikilink")
  - [秦国](../Page/秦国.md "wikilink")
  - [赵国](../Page/赵国.md "wikilink")
  - [魏国](../Page/魏国_\(战国\).md "wikilink")
  - [韩国](../Page/韩国_\(战国\).md "wikilink")
  - [秦昭襄王](../Page/秦昭襄王.md "wikilink")
  - [秦庄襄王](../Page/秦庄襄王.md "wikilink")
  - [信陵君](../Page/信陵君.md "wikilink")
  - [秦始皇](../Page/秦始皇.md "wikilink")
  - [赵高](../Page/赵高.md "wikilink")
  - [蒙武](../Page/蒙武.md "wikilink")
  - [蒙恬](../Page/蒙恬.md "wikilink")
  - [蒙毅](../Page/蒙毅.md "wikilink")

## 参考资料

  - [司马迁](../Page/司马迁.md "wikilink")[史记](../Page/史记.md "wikilink")：秦本纪
    秦始皇本纪 六国年表 赵世家 魏世家 韩世家 魏公子列传 蒙恬列传

## 注釋

<div class="references-small">

<references>

\[3\]

</references>

</small>

[M蒙](../Category/中國軍事人物.md "wikilink")
[M蒙](../Category/秦朝人.md "wikilink")
[M蒙](../Category/春秋战国人物.md "wikilink")
[A](../Category/蒙姓.md "wikilink")

1.
2.  七年，彗星先出东方，见北方，五月见西方。将军骜死。以攻龙、孤、庆都，还兵攻汲。彗星复见西方十六日。<史记秦始皇本纪第六>

3.  [《鬼影侠踪．第五回》](http://vip.book.sina.com.cn/book/chapter_42159_27548.html)