[Garlic_Press_and_Garlic.jpg](https://zh.wikipedia.org/wiki/File:Garlic_Press_and_Garlic.jpg "fig:Garlic_Press_and_Garlic.jpg")

**蒜茸鉗**是一種[烹調工具](../Page/烹調工具.md "wikilink")，設計目的為快速壓碎[大蒜](../Page/大蒜.md "wikilink")，製作[蒜茸](../Page/蒜茸.md "wikilink")。蒜茸鉗藉由一個[活塞將大蒜壓入一系列的小洞](../Page/活塞.md "wikilink")，當大蒜通過小洞後就變成泥狀，可以作為調味料或用以製作醬汁。大多數的蒜茸鉗還附有一系列的棒狀物，可以簡單的將洞中清理乾淨。

蒜茸鉗還有一些用途，例如壓碎其他的小東西(包括[橄欖](../Page/橄欖.md "wikilink")、[剌山柑](../Page/剌山柑.md "wikilink")、[魚肉及](../Page/魚肉.md "wikilink")[辣椒等](../Page/辣椒.md "wikilink"))，或是用來榨出少量的汁液(例如：[洋蔥](../Page/洋蔥.md "wikilink"))用以調味。

## 評語

一般認為，以蒜茸鉗壓碎的蒜茸與以其他方式壓碎的蒜茸有著不同的風味。這是由於在使用蒜茸鉗壓碎大蒜時，有較多的[細胞壁破裂](../Page/細胞壁.md "wikilink")，釋放出較多造成大蒜強烈風味的化合物。有一些主廚偏好使用蒜茸鉗，它們認為以蒜茸鉗壓碎的蒜茸風味較佳。[天然食物主廚Renée](../Page/天然食物.md "wikilink")
Underkoffler說\[1\] ：

另一方面，一些主廚認為，使用蒜茸鉗壓碎的蒜茸比起以其他方式壓碎的蒜茸（例如以菜刀壓碎）來說，風味較差。例如，知名主廚[安東尼·波登說蒜茸鉗](../Page/安東尼·波登.md "wikilink")「令人討厭」（abominations），並且建議大眾\[2\]：

[英國的](../Page/英國.md "wikilink")[食譜作家](../Page/食譜.md "wikilink")寫過一篇文章，標題是「蒜茸鉗毫無用處」（Garlic
Presses are Utterly Useless）\[3\]

## 註腳

## 参见

[Category:烹饪工具](../Category/烹饪工具.md "wikilink")

1.   p. 179.
2.   p. 81.
3.   p. 51.