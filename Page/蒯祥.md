**蒯祥**（），[字](../Page/表字.md "wikilink")**廷瑞**，[號](../Page/號.md "wikilink")**香山**，[明代吴县香山](../Page/明代.md "wikilink")（今[江苏](../Page/江苏.md "wikilink")[苏州](../Page/苏州.md "wikilink")[胥口](../Page/胥口镇_\(苏州市\).md "wikilink")）人，知名建筑[工匠](../Page/工匠.md "wikilink")，[香山帮匠人的鼻祖](../Page/香山帮匠人.md "wikilink")。

他的祖父[蒯思明](../Page/蒯思明.md "wikilink")、父亲[蒯福都是技艺精湛](../Page/蒯福.md "wikilink")、名闻遐迩的木匠。其父蒯福曾于明初主持过[金陵皇宫的木作工程](../Page/金陵.md "wikilink")，在建筑界颇有声望。在祖父和父亲的熏陶下，蒯祥也是造诣很深，年轻时就有“巧木匠”之称。

[明成祖迁都北京](../Page/明成祖迁都北京.md "wikilink")，调集能工巧匠建造[皇宫](../Page/皇宫.md "wikilink")。蒯祥随其父应征，随后升任“营缮所丞”，设计并直接指挥明宫城的营建。

據記載，蒯詳曾参与和主持了众多的皇室工程。如北京宮殿和[長陵](../Page/長陵.md "wikilink")、[獻陵](../Page/獻陵.md "wikilink")、[景陵](../Page/景陵.md "wikilink")、[裕陵四座皇陵](../Page/裕陵.md "wikilink")，还有皇宫前三殿（[奉天殿](../Page/奉天殿.md "wikilink")、[华盖殿](../Page/华盖殿.md "wikilink")、[谨身殿](../Page/谨身殿.md "wikilink")）二宫（[乾清宫](../Page/乾清宫.md "wikilink")、[坤宁宫](../Page/坤宁宫.md "wikilink")）的重建、北京衙署、北京[隆福寺](../Page/隆福寺_\(北京\).md "wikilink")、[南內](../Page/南內.md "wikilink")，以及北京[西苑](../Page/西苑.md "wikilink")（今[北海](../Page/北海公园.md "wikilink")、[中海](../Page/中南海.md "wikilink")、[南海](../Page/中南海.md "wikilink")）殿宇的建造，包括设计建造[承天门](../Page/承天门.md "wikilink")（今[天安门](../Page/天安门.md "wikilink")）。

《[吴县志](../Page/吴县志.md "wikilink")》中有“略用尺（淮下加十）度……造成以置原所，不差毫厘”的记载。康熙时《[苏州府志](../Page/苏州府志.md "wikilink")》也有“永乐间，召建大内，凡殿阁楼榭，以至回廊曲宇，祥随手图之，无不称上意”的佳话，遂被世人称为“蒯鲁班”。

蒯祥83岁去世，最后归葬故里[香山](../Page/香山街道_\(蘇州市\).md "wikilink")。

蒯祥官至[工部左](../Page/工部.md "wikilink")[侍郎](../Page/侍郎.md "wikilink")。[北京过去有一条](../Page/北京.md "wikilink")[蒯侍郎胡同](../Page/蒯侍郎胡同.md "wikilink")，据说就是蒯祥当年住过的地方。

## 参考文献

  - 《[中国大百科全书](../Page/中国大百科全书.md "wikilink")》<蒯祥>条目（张勖采）

## 外部链接

  - [蒯祥与明景、裕二陵](https://web.archive.org/web/20050422011614/http://www.bjcpdag.gov.cn/xiandu/zhanggu/20.htm)（北京市[昌平区档案局](../Page/昌平.md "wikilink")《昌平掌故》）
  - [蒯祥庙](https://web.archive.org/web/20051028190227/http://www.xukou.gov.cn/yuyou/kuaixiang.htm)

[K蒯](../Category/明朝畫家.md "wikilink")
[K蒯](../Category/中國建築師.md "wikilink")
[K蒯](../Category/苏州人.md "wikilink")
[Category:蒯姓](../Category/蒯姓.md "wikilink")
[Category:明朝工部左侍郎](../Category/明朝工部左侍郎.md "wikilink")