[Potoiislands.png](https://zh.wikipedia.org/wiki/File:Potoiislands.png "fig:Potoiislands.png")
[Po_Toi_Islands.jpg](https://zh.wikipedia.org/wiki/File:Po_Toi_Islands.jpg "fig:Po_Toi_Islands.jpg")
[Tsing_Yi_Island_in_Yuet_Tai_Kei.png](https://zh.wikipedia.org/wiki/File:Tsing_Yi_Island_in_Yuet_Tai_Kei.png "fig:Tsing_Yi_Island_in_Yuet_Tai_Kei.png")

**蒲台群島**（），又稱**蒲苔群島**或**蒲笞群島**，是位於[香港東南海域的一組](../Page/香港.md "wikilink")[群島](../Page/群島.md "wikilink")，[地區行政上屬於](../Page/香港行政區劃.md "wikilink")[離島區](../Page/離島區.md "wikilink")。群島位於[香港島](../Page/香港島.md "wikilink")[石澳之東南](../Page/石澳.md "wikilink")，是香港境內最南的區域，主要包括[蒲台島](../Page/蒲台島.md "wikilink")、[螺洲](../Page/螺洲島.md "wikilink")（包括[螺洲白排](../Page/螺洲白排.md "wikilink")）、[宋崗](../Page/宋崗.md "wikilink")、[橫瀾島](../Page/橫瀾島.md "wikilink")、[墨洲](../Page/墨洲.md "wikilink")（連同[墨洲排](../Page/墨洲排.md "wikilink")）這五大島及[雙四門](../Page/雙四門.md "wikilink")。由於地處偏遠，交通不便，除蒲台島有少量居民外，其他島嶼均是無人島。亦基於同樣原因，群島上動植物物種均很豐富，所以被列为重要陆上自然保育区的同时，也被认为是具发展潜力的郊野公园所在地。據統計，現時群島有植物93科、203屬、245種，還有珍稀動物[盧文氏樹蛙](../Page/盧文氏樹蛙.md "wikilink")。

然而，群島現時亦面對不少發展上的壓力：有財團多次企圖在島上開發墓葬群\[1\]；而過往政府亦曾有計劃把整個蒲台群島填海，以作發展\[2\]\[3\]。

## 參考文獻

## 外部連結

{{-}}

{{-}}

[蒲台群島](../Category/蒲台群島.md "wikilink")

1.
2.  <http://www.pland.gov.hk/pland_en/p_study/comp_s/swnt/final-report/figures/fig4-1.gif>
3.  <http://www.pland.gov.hk/pland_en/p_study/comp_s/swnt/final-report/4rds.htm>