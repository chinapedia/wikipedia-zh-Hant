**蒲式耳**（）是[英制的](../Page/英制.md "wikilink")[容量及](../Page/容量.md "wikilink")[重量](../Page/重量.md "wikilink")[單位](../Page/單位.md "wikilink")，於[英國及](../Page/英國.md "wikilink")[美國通用](../Page/美國.md "wikilink")，主要用於量度[乾貨](../Page/乾貨.md "wikilink")，尤其是[農產品的](../Page/農產品.md "wikilink")[重量](../Page/重量.md "wikilink")。通常1蒲式耳等於8[加侖](../Page/加侖.md "wikilink")（約36.37[公升](../Page/公升.md "wikilink")），但不同的農產品對蒲式耳的定義各有不同。在農產品的[美國](../Page/美國.md "wikilink")[期貨市場上](../Page/期貨.md "wikilink")，會使用「[美分](../Page/美分.md "wikilink")／**蒲式耳**」作為價格單位。

以下是不同的農產品對蒲式耳的定義：

  - 1蒲式耳[燕麥](../Page/燕麥.md "wikilink")＝32磅（約14.52[公斤](../Page/公斤.md "wikilink")）
  - 1蒲式耳[麥芽](../Page/麥芽.md "wikilink")＝34磅（約15.42公斤）
  - 1蒲式耳[大麥](../Page/大麥.md "wikilink")＝48磅（約21.77公斤）
  - 1蒲式耳[玉米](../Page/玉米.md "wikilink")＝56磅（約25.40公斤）
  - 1蒲式耳[小麥或](../Page/小麥.md "wikilink")[大豆](../Page/大豆.md "wikilink")＝60磅（約27.22公斤）

{{-}}

[Category:英制](../Category/英制.md "wikilink")
[Category:重量單位](../Category/重量單位.md "wikilink")