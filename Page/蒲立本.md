**蒲立本**（，），[加拿大汉学家](../Page/加拿大.md "wikilink")。[艾伯塔省](../Page/艾伯塔省.md "wikilink")[卡尔加里人](../Page/卡尔加里.md "wikilink")。

## 生平

蒲立本1942年毕业于[艾伯塔大学](../Page/艾伯塔大学.md "wikilink")，主修希腊文和拉丁文经典。参与太平洋战争期间情报工作时接触[日语](../Page/日语.md "wikilink")，私下也在[渥太华的](../Page/渥太华.md "wikilink")[卡爾頓大學学习](../Page/卡爾頓大學.md "wikilink")[汉语](../Page/汉语.md "wikilink")。1946年到[伦敦大学攻读中国语言和历史](../Page/伦敦大学.md "wikilink")。1951年获哲学博士学位，论文《安禄山的家世和早年生活》，然后到[日本](../Page/日本.md "wikilink")[东京和](../Page/东京.md "wikilink")[京都的图书馆进修至](../Page/京都.md "wikilink")1952年。1953年评上[剑桥大学](../Page/剑桥大学.md "wikilink")[汉语教授职位](../Page/汉语教授.md "wikilink")。1955年出版著作《安禄山叛乱的背景》。在《[亚非学院院刊](../Page/亚非学院院刊.md "wikilink")》、《[通报](../Page/通报.md "wikilink")》和《[大亚细亚](../Page/大亚细亚.md "wikilink")》发表唐代历史论文，兼及中亚历史。

1959年因“受早年对印欧比较语言学兴趣的启发”和“对更好的古典汉语教材的需要”，开始研究汉语的历史语言学。他从语法入手，1960年在《大亚细亚》发表《早期汉语语法研究（一）》。随后转到了[中古汉语和](../Page/中古汉语.md "wikilink")[上古汉语的构拟](../Page/上古汉语.md "wikilink")，发表《上古汉语的辅音系统》（1962年）、《上古汉语的辅音系统（二）》（1962年）、《上古汉语和缅文元音系统的诠释》（1963年）。1966年到[不列颠哥伦比亚大学出任教授](../Page/不列颠哥伦比亚大学.md "wikilink")，1968年出任[亚洲研究系系主任](../Page/亚洲研究系.md "wikilink")。在《大亚细亚》发表《晚期中古汉语》（1970年）、《晚期中古汉语（二）》（1971年）。1973年在《[中国语言学报](../Page/中国语言学报.md "wikilink")》创刊号发表《关于汉语词族的一些新假设》、在《亚非学院院刊》发表《关于上古汉语-s的更多证据及其消失的时间》。1974年卸下系主任职位。在《[华裔学志](../Page/华裔学志.md "wikilink")》发表《上古汉语的韵尾辅音》（1977年—1978年）。1987年成为荣誉退休教授。

1980年后出版的专书有《中古汉语：历史语音学的研究》（1984年）、《早期中古汉语、晚期中古汉语、早期官话构拟发音的词汇》（1991年）、《一份以中亚婆罗米文书写的汉语文献：晚期中古汉语和于阗语发音的新证据》（1993年合著）、《古典汉语语法纲要》（1995年）。历年出任的其他职位包括：1980年当选[加拿大](../Page/加拿大.md "wikilink")[皇家学会会员](../Page/皇家学会.md "wikilink")、1993年成为[意大利](../Page/意大利.md "wikilink")[中东和远东研究所通讯会员](../Page/中东和远东研究所.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")[亚洲研究学会会长](../Page/亚洲研究学会.md "wikilink")（1971年—1974年)、[美国](../Page/美国.md "wikilink")[东方学会会长](../Page/东方学会.md "wikilink")（1990年—1991年)、[美国东方学会西部支部会长](../Page/美国.md "wikilink")（1992年—1993年）、[国际汉语语言学学会主席](../Page/国际汉语语言学学会.md "wikilink")（1995年—1996年）。

## 外部連結

  - [Asian Studies E.G.
    Pulleyblank](https://web.archive.org/web/20100516071004/http://www.asia.ubc.ca/people/faculty/emeritus/eg-pulleyblank.html)

[Category:加拿大皇家學會院士](../Category/加拿大皇家學會院士.md "wikilink")
[Category:加拿大语言学家](../Category/加拿大语言学家.md "wikilink")
[Category:加拿大漢學家](../Category/加拿大漢學家.md "wikilink")
[Category:不列顛哥倫比亞大學教師](../Category/不列顛哥倫比亞大學教師.md "wikilink")
[Category:倫敦大學校友](../Category/倫敦大學校友.md "wikilink")
[Category:卡爾頓大學校友](../Category/卡爾頓大學校友.md "wikilink")
[Category:阿爾伯塔大學校友](../Category/阿爾伯塔大學校友.md "wikilink")
[Category:卡加利人](../Category/卡加利人.md "wikilink")