**蒸發散**，又称**蒸散**、**蒸散量**等，包括了地表水分[蒸发与](../Page/蒸发.md "wikilink")[植物体内水分的](../Page/植物.md "wikilink")[蒸腾](../Page/蒸腾.md "wikilink")。它是维持陆面水分平衡的一个重要组成部分，也是维持地表能量平衡的主要部分\[1\]。

计算蒸散的模型有：

  - [鲍恩比能量平衡法](../Page/鲍恩比能量平衡法.md "wikilink")（BREB法）
  - [空气动力学方法](../Page/空气动力学.md "wikilink")
  - Penman Monteith公式
  - [涡旋相关法](../Page/涡旋.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Z](../Category/水文学.md "wikilink") [Z](../Category/气候学.md "wikilink")
[Z](../Category/农学.md "wikilink") [Z](../Category/生态学.md "wikilink")
[Category:土壤學](../Category/土壤學.md "wikilink")
[Category:水和环境](../Category/水和环境.md "wikilink")

1.