\[1\]
[Simple_distillation_apparatus.svg](https://zh.wikipedia.org/wiki/File:Simple_distillation_apparatus.svg "fig:Simple_distillation_apparatus.svg")
**蒸馏**（英語：Distillation、Distilled）是一种[热力学的分离工艺](../Page/热力学.md "wikilink")，它利用混合[液体或液](../Page/液体.md "wikilink")-固体系中各组分[沸点不同](../Page/沸点.md "wikilink")，使低沸点组分[蒸发](../Page/蒸发.md "wikilink")，再[冷凝以分离整个组分的单元操作过程](../Page/冷凝.md "wikilink")，是蒸发和冷凝两种单元操作的联合。与其它的分离手段，如[萃取](../Page/萃取.md "wikilink")、[吸附等相比](../Page/吸附.md "wikilink")，它的优点在于不需使用系统组分以外的其它[溶剂](../Page/溶剂.md "wikilink")，从而保证不会引入新的杂质。

## 原理

不同组分液体具有不同的沸点，是蒸馏最基本的理论依据。在相同温度下，液体上方各个组分蒸气的[分压也有所不同](../Page/分压.md "wikilink")。

参考[二元单相液体的](../Page/二元.md "wikilink")[相图](../Page/相图.md "wikilink")，当由液体A和B按一定比例配成的混合液（设A的浓度为c1）经过加热后到达[沸点线](../Page/沸点线.md "wikilink")，然后通过无差异的[气液平衡](../Page/气液平衡.md "wikilink")（从1到2），在生成的气相中得到A的含量为c2的混合气；该气体经过降温冷凝后收集便得到具有新组成（A浓度为c2）的液体。按图例来讲，就是左侧的A组分被提纯了（因为C2大于c1）。

蒸馏有两种形式：

  - **简单蒸馏**，如制造蒸馏水以去除其中溶解的固体杂质；制造[蒸馏酒以浓缩](../Page/蒸馏酒.md "wikilink")[酒精](../Page/酒精.md "wikilink")，去除部分水分；
  - **精馏**，也叫**[分馏](../Page/分馏.md "wikilink")**。

## 参见

## 外部链接

  - [蒸馏与分馏.PDF](http://202.194.137.16/jpkc/chem/ziyuan/dianzijiaoan/24.pdf)

[蒸馏](../Category/蒸馏.md "wikilink")
[Category:单元操作](../Category/单元操作.md "wikilink")
[Category:炼金术过程](../Category/炼金术过程.md "wikilink")
[Category:分离过程](../Category/分离过程.md "wikilink")
[Category:實驗室技術](../Category/實驗室技術.md "wikilink")
[Category:相变](../Category/相变.md "wikilink")

1.