[17-05-06-Miniaturen_RR79033.jpg](https://zh.wikipedia.org/wiki/File:17-05-06-Miniaturen_RR79033.jpg "fig:17-05-06-Miniaturen_RR79033.jpg")
**蒸馏酒**（），是利用穀物、水果或蔬菜在經過[酒精發酵後的產物為原料](../Page/酒精發酵.md "wikilink")，以[蒸餾方式所制造出的酒](../Page/蒸餾.md "wikilink")。蒸餾的過程會淨化液體並除去稀釋酒的成分（例如[水](../Page/水.md "wikilink")），目的是增加其酒精濃度（通常以[ABV表示濃度](../Page/酒精濃度.md "wikilink")）。\[1\]由于製酒[发酵过程中产生過浓的](../Page/发酵.md "wikilink")[乙醇溶液可以将](../Page/乙醇.md "wikilink")[酵母杀死](../Page/酵母.md "wikilink")，无法继续[发酵](../Page/发酵.md "wikilink")，所以经发酵[酿造的酒类含乙醇浓度最高只能达](../Page/酿造.md "wikilink")10%—20%。但[酒精的](../Page/酒精.md "wikilink")[沸点是](../Page/沸点.md "wikilink")78.3℃，经加热使温度超过酒精沸点而不到水的沸点，酒精[蒸汽逸出](../Page/蒸汽.md "wikilink")，再经[冷凝可得到](../Page/冷凝.md "wikilink")80%—90%以上浓度的[乙醇](../Page/乙醇.md "wikilink")[溶液](../Page/溶液.md "wikilink")，经勾兑可制造高浓度的烈酒（）。由於要獲得更高濃度的酒往往經過蒸餾過程，故一般來說，烈酒是蒸餾酒的同義詞。在[北美](../Page/北美.md "wikilink")，-{「}-烈酒-{」}-一詞已用於區分蒸餾酒和未蒸餾酒。

蒸馏酒最早出現西元1世紀的[亞歷山卓](../Page/亞歷山卓.md "wikilink")\[2\] 。

中国的蒸馏酒是在南宋至元朝之間出现的\[3\]，其技術可能來自阿拉伯人\[4\]\[5\]\[6\]。[中国的](../Page/中国.md "wikilink")[白酒中由于](../Page/白酒_\(蒸餾酒\).md "wikilink")[蒸馏过程中提取的](../Page/蒸馏.md "wikilink")[馏分不同](../Page/馏分.md "wikilink")，有时分为“[头曲](../Page/头曲.md "wikilink")”“[二曲](../Page/二曲.md "wikilink")”或“[二锅头](../Page/二锅头.md "wikilink")”等。

世界著名的蒸馏酒有[英國以](../Page/英國.md "wikilink")[大麥蒸馏的](../Page/大麥.md "wikilink")[威士忌](../Page/威士忌.md "wikilink")、[法国以](../Page/法国.md "wikilink")[葡萄蒸馏的](../Page/葡萄.md "wikilink")[白蘭地](../Page/白蘭地.md "wikilink")、[俄羅斯及](../Page/俄羅斯.md "wikilink")[东欧以](../Page/东欧.md "wikilink")[麵包蒸馏的](../Page/麵包.md "wikilink")[伏特加](../Page/伏特加.md "wikilink")、[加勒比地区以](../Page/加勒比.md "wikilink")[蔗糖](../Page/蔗糖.md "wikilink")[糖蜜蒸馏的](../Page/糖蜜.md "wikilink")[朗姆酒](../Page/朗姆酒.md "wikilink")、[荷兰以](../Page/荷兰.md "wikilink")[杜松子調味的](../Page/杜松子.md "wikilink")[琴酒和墨西哥以](../Page/琴酒.md "wikilink")[龍舌蘭](../Page/龍舌蘭.md "wikilink")[糖漿蒸馏的](../Page/糖漿.md "wikilink")[龍舌蘭酒](../Page/龍舌蘭酒.md "wikilink")，并称世界六大[鸡尾酒基酒](../Page/鸡尾酒.md "wikilink")。

## 註釋

[Category:蒸餾酒](../Category/蒸餾酒.md "wikilink")

1.
2.
3.  [中国酒大观目录·蒸馏酒的起源](http://www.sytu.edu.cn/zhgjiu/j1-2.htm)
4.  [叶子奇](../Page/葉子奇_\(明朝\).md "wikilink")《[草木子](../Page/草木子.md "wikilink")》：“法酒，用器烧酒之精液取之，名曰哈喇基。酒极醲烈，其清如水，盖酒露也。……此皆元朝之法酒，古无有也。”
5.  [李时珍](../Page/李时珍.md "wikilink")《[本草纲目](../Page/本草纲目.md "wikilink")》：“烧酒非古法也，自元时始创其法。”
6.  明[方以智](../Page/方以智.md "wikilink")《[物理小识](../Page/物理小识.md "wikilink")》：“烧酒元时始创其法，名阿剌吉。”