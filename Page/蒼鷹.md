**蒼鷹**（[學名](../Page/學名.md "wikilink")：**），別名**牙鷹**，是鷹科鷹屬中一種中到大型的[猛禽](../Page/猛禽.md "wikilink")。與其他[鵟屬及](../Page/鵟屬.md "wikilink")[鷹等一樣都在日間活动](../Page/鷹.md "wikilink")。

苍鹰廣泛分布在[北半球的溫帶地區](../Page/北半球.md "wikilink")。在[北美地区被稱为](../Page/北美.md "wikilink")*Northern
Goshawk*，是當地的[留鳥](../Page/留鳥.md "wikilink")。但在較冷的地區，如[亞洲北部及](../Page/亞洲.md "wikilink")[加拿大等地的蒼鷹則會迁移到南方過冬](../Page/加拿大.md "wikilink")。

蒼鷹的图案也出现在[葡萄牙領土](../Page/葡萄牙.md "wikilink")[亞速爾群島的旗幟上](../Page/亞速爾群島.md "wikilink")。因为最初來到這個群島的葡萄牙[探險者踏上這片土地時正好看到一只猛禽在上空盤旋](../Page/探險者.md "wikilink")，他们認為那是一只蒼鷹。事實上亞速爾群島上從来沒有蒼鷹出沒。經过後來的學者研究，那应该是[普通鵟的一个亞種](../Page/普通鵟.md "wikilink")。

## 外觀描述

[AccipterGentilisJuvenileFlight1.jpg](https://zh.wikipedia.org/wiki/File:AccipterGentilisJuvenileFlight1.jpg "fig:AccipterGentilisJuvenileFlight1.jpg")
蒼鷹是鷹屬鳥類中體型最大的一員\[1\]，首先於1758年由[卡爾·林奈在其](../Page/卡爾·林奈.md "wikilink")〈[自然系統](../Page/自然系統.md "wikilink")〉（**）一書中介紹及命名\[2\]。其翅膀短而寬闊，並有長長的尾羽，這是林棲鳥種適應高密度樹林生活的重要特征。苍鹰有明顯的[兩性異形現象](../Page/兩性異形.md "wikilink")，雄鳥上半身灰藍，下半身則為有條紋的灰，體長49至57厘米、翼展為93至105厘米。雌鳥比雄鳥體型更大，可長達58至64厘米，翼展達108至127厘米，上半身暗藍灰、下半身則為純灰。最小的雄鳥體重只有630[克](../Page/克.md "wikilink")，但最大的雌鳥則可重達2.4[公斤](../Page/公斤.md "wikilink")。亞成鳥上半身是有條紋的灰色，下半身則為純灰色。牠們常展現出獨有的飛行特徵──五次緩慢的拍翼後滑翔而去。

在[歐亞大陸上](../Page/歐亞大陸.md "wikilink")，雄鳥常與雌性的[北雀鷹](../Page/北雀鷹.md "wikilink")（[Sparrowhawk](../Page/:en:Sparrowhawk.md "wikilink")）相混淆，但一般它們都有肥胖的體型和相對较長的翅膀。

在[北美洲](../Page/北美洲.md "wikilink")，苍鹰幼鳥容易與小型的[條紋鷹及](../Page/條紋鷹.md "wikilink")[雞鷹相混淆](../Page/雞鷹.md "wikilink")，但人们还是能够通过比较體型将它們区分开来。

## 捕獵及食物

蒼鷹主要捕捉其他細小的鳥類及[哺乳動物為食](../Page/哺乳動物.md "wikilink")，常常集体進行掠地飛行，越过茂密的森林，並对獵物突然發起袭击。與大部分猛禽一樣，蒼鷹也是“機會主義者”，只要看到合適的獵物就會出擊。但牠們還是對鳥類情有獨鍾，特別是[披肩雞](../Page/披肩雞.md "wikilink")（[ruffed
grouse](../Page/:en:ruffed_grouse.md "wikilink")、[鴒形目的鳥種及](../Page/鴒形目.md "wikilink")[雀形目中的椋鳥等](../Page/雀形目.md "wikilink")。水鳥當中，體型小於[綠頭鴨的鳥類也是牠們的目標](../Page/綠頭鴨.md "wikilink")。此外，蒼鷹也偶尔捕捉一些體型較大的動物，如[雪兔及](../Page/雪兔.md "wikilink")[長耳大野兔等](../Page/長耳大野兔.md "wikilink")。

## 習性

[GOSHAWK.JPG](https://zh.wikipedia.org/wiki/File:GOSHAWK.JPG "fig:GOSHAWK.JPG")
进入[春天的繁殖季節](../Page/春天.md "wikilink")，雄鳥就會開始牠們如[過山車般的飛行表演](../Page/過山車.md "wikilink")，而這時也是觀察這種神秘鳥类的最佳時機。成鳥會在三至四月回到牠們築巢的地方，並在四至五月期間下蛋。巢內一般有1到5顆鳥蛋在內，大小為59
x
45mm，約重60克。孵卵期在28到38日內。幼鳥則在35日起開始離巢，10日後便會尝试飛行。即使在一年後，幼鳥仍會在其父母身边活動，這與成鳥會保護整個區域有關。牠們對任何入侵者均沒有好感，即使是路過的人類也會遭到牠們激烈的驅趕。因此，即使是它们的鳥蛋或幼鳥也极少被捕掠。蒼鷹偶爾也會招到如[雕鴞和](../Page/雕鴞.md "wikilink")[鵟屬猛禽的攻擊](../Page/鵟屬.md "wikilink")。

## 狀況

19世紀时，蒼鷹因为受到收藏家的青睞及被當地的[獵場看守人猎杀而在](../Page/獵場看守人.md "wikilink")[英國灭绝了](../Page/英國.md "wikilink")，但近年有不少來自歐洲大陆的蒼鷹重新移居英国。目前在英國[諾森伯蘭的](../Page/諾森伯蘭.md "wikilink")[開爾德森林內已發現了牠們的蹤影](../Page/開爾德森林.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [Northern Goshawk Species
    Account](http://www.birds.cornell.edu/AllAboutBirds/BirdGuide/Northern_Goshawk.html)
    - Cornell Lab of Ornithology
  - [Northern
    Goshawk](http://www.mbr-pwrc.usgs.gov/id/framlst/i3340id.html) -
    USGS Patuxent Bird Identification InfoCenter
  - [Northern Goshawk
    Information](http://sdakotabirds.com/species/northern_goshawk_info.htm)
    - South Dakota Birds and Birding
  - [Environment Canada Goshawk page, including sound clip of Goshawk
    Call](https://web.archive.org/web/20090515024043/http://www.on.ec.gc.ca/wildlife/wildspace/life.cfm?ID=NOGO&Page=Call&Lang=e)
  - [Read Congressional Research Service (CRS) Reports regarding
    Northern
    Goshawks](https://web.archive.org/web/20071205022230/http://digital.library.unt.edu/govdocs/crs/search.tkl?q=goshawk&search_crit=fulltext&search=Search&date1=Anytime&date2=Anytime&type=form)
  - [Goshawk
    videos](http://ibc.hbw.com/ibc/phtml/especie.phtml?idEspecie=710)
    Internet Bird Collection
  - [Picture of Northern & European Goshawk chicks and other
    Accipiters](http://www.birdwatching-bliss.com/accipiters.html)
  - [Nature writer recounts goshawk pair's fierce defense of their
    nesting
    territory](https://web.archive.org/web/20080509054018/http://pages.cthome.net/rwinkler/goshawk2.htm)
  - [Ageing and sexing (PDF) by Javier
    Blasco-Zumeta](http://www.ibercajalav.net/img/117_GoshawkAgentilis.pdf)

[Category:鷹屬](../Category/鷹屬.md "wikilink")
[Category:中國國家二級保護動物](../Category/中國國家二級保護動物.md "wikilink")
[Category:廣東動物](../Category/廣東動物.md "wikilink")

1.
2.