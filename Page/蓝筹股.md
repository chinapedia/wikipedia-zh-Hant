**藍籌股**又稱**績優股**、**權值股**，指在某一行業中處於重要支配地位、業績優良、交易活躍、公司知名度高、市值大、公司經營者可信任、營收獲利穩定、每年固定分配[股利](../Page/股利.md "wikilink")、紅利優厚、市場認同度高的大公司的[股票](../Page/股票.md "wikilink")。藍籌股是一家公司的股票，在質量，可靠性以及在經濟好時壞的情況下有利可圖的運營能力方面享譽全國。

買入[股價穩定度高](../Page/股價.md "wikilink")、高[殖利率](../Page/殖利率.md "wikilink")（每年配發高[現金股利](../Page/現金.md "wikilink")）的績優股，長期持有不賣出，每年都能領取現金股利，如同將資金轉入「[銀行](../Page/銀行.md "wikilink")[定期存款](../Page/定期存款.md "wikilink")」一般，則可稱為**定存股**。

## 釋名

「藍籌」的名稱據說來自[賭博桌上最高額的](../Page/賭博.md "wikilink")[籌碼是](../Page/籌碼.md "wikilink")[藍色的](../Page/藍色.md "wikilink")\[1\]，引申為最大規模或最大市值的[上市公司](../Page/上市公司.md "wikilink")。一般人將藍籌股等同股票[成份股](../Page/成份股.md "wikilink")，但事實上成份股不一定是最大或最佳的上市公司。\[2\]

## 例子

  - 美國：[蘋果公司](../Page/蘋果公司.md "wikilink")、[麥當勞](../Page/麥當勞.md "wikilink")、[波音](../Page/波音.md "wikilink")、[寶潔](../Page/寶潔.md "wikilink")、[強生等](../Page/強生.md "wikilink")。如**[道瓊斯工業指數](../Page/道瓊斯工業指數.md "wikilink")**
  - 臺灣：[台積電](../Page/台積電.md "wikilink")（代號：2330）、[鴻海](../Page/鴻海.md "wikilink")（代號：2317）、[中鋼](../Page/中鋼.md "wikilink")（代號：2002）、[中華電](../Page/中華電信.md "wikilink")（代號：2412）等**[臺灣50指數成分股票](../Page/台灣50.md "wikilink")**。
  - 香港：即**[恒生指數成分股](../Page/恒生指數#恒生指數成份股.md "wikilink")**，共有50檔藍籌股，例如[匯豐](../Page/匯豐.md "wikilink")、[友邦保險](../Page/友邦保險.md "wikilink")、[電能實業等](../Page/電能實業.md "wikilink")。
  - 中國：[萬科](../Page/萬科.md "wikilink")、[中國平安](../Page/中國平安.md "wikilink")、[建設銀行及](../Page/建設銀行.md "wikilink")**[上證50指數成分股](../Page/上證50指數.md "wikilink")**等。

## 参考文献

## 参见

  - [紅籌股](../Page/紅籌股.md "wikilink")
  - [紫籌股](../Page/紫籌股.md "wikilink")
  - [每股盈餘](../Page/每股盈餘.md "wikilink") (EPS)

[Category:股市](../Category/股市.md "wikilink")

1.
2.  March 12th, 2008, Dow Jones internal news item "Ever Wonder How
    ‘Blue Chip’ Stocks Started?"