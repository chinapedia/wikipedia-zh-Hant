**蓟县构造期**，简称**蓟县期**，是[元古宙](../Page/元古宙.md "wikilink")[蓟县纪](../Page/蓟县纪.md "wikilink")(14-10億年前)期间的[构造期](../Page/构造期.md "wikilink")，在此期间，在今[中国及周边地区发生了](../Page/中国.md "wikilink")**蓟县运动**或称**蓟县事件**。

## 名称由来和别名

蓟县期是以蓟县纪命名的。蓟县纪是专门用于中国的[地质年代](../Page/地质年代.md "wikilink")，以[天津](../Page/天津.md "wikilink")[蓟县命名](../Page/蓟县.md "wikilink")，相当于[国际地质科学联合会](../Page/国际地质科学联合会.md "wikilink")(2004)确定的[中元古代](../Page/中元古代.md "wikilink")[延展纪](../Page/延展纪.md "wikilink")(14-12億年前)和[狭带纪](../Page/狭带纪.md "wikilink")(12-10億年前)的全部。在中国南方，习惯上把蓟县运动称为**四堡运动**，把蓟县期（常常再包括之前的[长城期](../Page/长城构造期.md "wikilink")）称为**四堡期**，这是用[福建](../Page/福建.md "wikilink")[连城县](../Page/连城县.md "wikilink")[四堡镇命名的](../Page/四堡镇.md "wikilink")。蓟县运动（四堡运动）的别名还有**武陵运动**（[湖南](../Page/湖南.md "wikilink")）、**梵净山运动**（[贵州](../Page/贵州.md "wikilink")）、**黑龙江运动**（[黑龙江](../Page/黑龙江省.md "wikilink")）、**皖南运动**
([安徽](../Page/安徽.md "wikilink")）等。\[1\]

## 构造活动

蓟县期的年代久远，目前只能对这期间的构造运动做粗略的描述。在蓟县期，构成后世中国大陆的[地块处于离散状态](../Page/地块.md "wikilink")，所属[构造域与长城期相同](../Page/构造域.md "wikilink")。在此期间，由原始[中朝板块分裂而成的古](../Page/中朝板块.md "wikilink")[塔里木板块](../Page/塔里木板块.md "wikilink")、古[柴达木地块和古中朝板块继续分裂](../Page/柴达木地块.md "wikilink")；[秦别-大别地块](../Page/秦别-大别地块.md "wikilink")（此时可能已经分裂成几个小地块）则开始出现碰撞、挤压现象。蓟县期[构造运动最强烈的地方属于](../Page/构造运动.md "wikilink")[亲扬子构造域](../Page/亲扬子构造域.md "wikilink")，在此期间，北[扬子板块和南扬子板块](../Page/扬子板块.md "wikilink")（即[湘桂地块](../Page/湘桂地块.md "wikilink")）发生碰撞，形成[江南碰撞带](../Page/江南碰撞带.md "wikilink")，从而拼合成统一的[扬子板块](../Page/扬子板块.md "wikilink")。

其他地块在蓟县期或者比较稳定，或者资料不足难于判断其活动性。

## 注释

## 参考文献

  -
[J](../Page/category:构造期.md "wikilink")

1.  [中国可持续发展信息网 - 地质构造](http://www.sdinfo.net.cn/geomap/dizhigz.htm)
    于2008-03-31访问