[Pone-Kingpetch.jpg](https://zh.wikipedia.org/wiki/File:Pone-Kingpetch.jpg "fig:Pone-Kingpetch.jpg")
**蓬金鐵**（泰語 : **โผน กิ่งเพชร**，英文翻譯 :**Pone
Kingpetch**，原名：**馬拉·西多拔**，**มานะ
สีดอกบวบ**，）是[泰王國歷史上的第一個](../Page/泰王國.md "wikilink")[拳擊世界冠軍](../Page/拳擊.md "wikilink")，1935年2月12日他生于[班武里府](../Page/班武里府.md "wikilink")[華欣](../Page/華欣.md "wikilink")。少年時，習武於[曼谷](../Page/曼谷.md "wikilink")[隆賓尼大拳場](../Page/隆賓尼大拳場.md "wikilink")，得名師指點，進步神速，十九歲即取得泰國青年冠軍，1960年他挑戰並擊敗世界次輕量級冠軍[阿根廷選手](../Page/阿根廷.md "wikilink")[巴斯奎爾·佩雷斯](../Page/巴斯奎爾·佩雷斯.md "wikilink")（Pascual
Pérez）第一次獲得世界輕量級拳王稱號，後來他三度迎擊日本[原田政彥的挑戰](../Page/原田政彥.md "wikilink")，在初戰失利的情況下遇挫不餒，再戰得勝，一時傳為佳話。

後來他1965年挂上拳套，告別職業拳手生涯，設館授徒，培育了一批泰國拳擊好手。1982年蓬金鐵逝世于曼谷，享年四十七歲。今天在他的家鄉華欣市中心可以看到「**蓬金鐵公園**」，裡面最顯眼的是他得勝后振臂歡呼的青銅像，不斷激勵后人為國爭光。

## 外部連結

  - [世界拳手冠軍榜](http://www.boxrec.com/boxer_display.php?boxer_id=11494)
  - [拳戰片段（一）](http://video.mthai.com/player.php?id=8M1175178807M200)
  - [拳戰片段（二）](http://video.mthai.com/player.php?id=8M1175075984M161)
  - [拳手參考資料](https://web.archive.org/web/20021124041535/http://www.geocities.com/samgler8/champ1.html)


[Category:泰国拳击运动员](../Category/泰国拳击运动员.md "wikilink")
[Category:蝇量级拳击运动员](../Category/蝇量级拳击运动员.md "wikilink")
[Category:世界蝇量级拳王](../Category/世界蝇量级拳王.md "wikilink")
[Category:世界拳击协会拳王](../Category/世界拳击协会拳王.md "wikilink")
[Category:世界拳击理事会拳王](../Category/世界拳击理事会拳王.md "wikilink")