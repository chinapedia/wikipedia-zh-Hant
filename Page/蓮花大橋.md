**蓮花大橋**（），為第一條連接[澳門和](../Page/澳門.md "wikilink")[珠海](../Page/珠海.md "wikilink")[橫琴的跨境大橋](../Page/橫琴.md "wikilink")。澳門橋口位於[路氹連貫公路旁](../Page/路氹連貫公路.md "wikilink")、由填海而成的[路氹城](../Page/路氹城.md "wikilink")，珠海橋口為橫琴。蓮花大橋全長1756米，橋寬30米，雙向六條行車道，不准行人行走。

## 沿革

[Taipa,_Coloane,_Lotus_Bridge_from_Macau_Tower.jpg](https://zh.wikipedia.org/wiki/File:Taipa,_Coloane,_Lotus_Bridge_from_Macau_Tower.jpg "fig:Taipa,_Coloane,_Lotus_Bridge_from_Macau_Tower.jpg")
蓮花大橋由[中國鐵建](../Page/中國鐵建.md "wikilink")[中鐵第四勘察設計院與](../Page/中鐵第四勘察設計院.md "wikilink")[澳葡政府共同設計](../Page/澳葡政府.md "wikilink")，珠海與澳門共同投資約兩億元[人民幣興建](../Page/人民幣.md "wikilink")。1998年5月動工修建，直至1999年[澳門回歸前的](../Page/澳門回歸.md "wikilink")12月10日，由[澳门总督](../Page/澳门总督.md "wikilink")[韋奇立](../Page/韋奇立.md "wikilink")、候任[澳門特別行政區行政長官](../Page/澳門特別行政區行政長官.md "wikilink")[何厚鏵](../Page/何厚鏵.md "wikilink")、[新華社澳門分社社長](../Page/新華社澳門分社.md "wikilink")[王啟人等剪彩揭幕](../Page/王啟人.md "wikilink")。

2000年3月啟用，成為第二個連接澳門和[珠海市的陸上交通邊境](../Page/珠海市.md "wikilink")。此大橋的落成，分流了[關閘邊檢大樓中澳兩地客](../Page/關閘邊檢大樓.md "wikilink")、貨物運送之壓力。

自2005年9月17日起，由於中方邊檢大樓結構出現問題，關口封閉，及至2007年5月1日重開\[1\]。

## 相關條目

  - [路氹邊檢大樓](../Page/路氹邊檢大樓.md "wikilink")
  - [蓮花口岸專線巴士](../Page/蓮花口岸專線巴士.md "wikilink")

## 外部連結

  - [Google
    Maps上的蓮花大橋](http://maps.google.com/maps?f=q&hl=en&q=macau&ie=UTF8&z=16&ll=22.141461,113.551512&spn=0.012839,0.020449&t=k&om=1)

## 參考資料

[Category:澳门跨海大桥](../Category/澳门跨海大桥.md "wikilink")
[Category:中国公路桥](../Category/中国公路桥.md "wikilink")
[Category:梁桥](../Category/梁桥.md "wikilink")
[Category:2000年完工桥梁](../Category/2000年完工桥梁.md "wikilink")
[Category:澳門出入境口岸](../Category/澳門出入境口岸.md "wikilink")
[Category:珠海桥梁](../Category/珠海桥梁.md "wikilink")

1.  [珠海横琴口岸“五一”恢复通关](http://www.gd.xinhuanet.com/newscenter/2007-04/30/content_9935012.htm)