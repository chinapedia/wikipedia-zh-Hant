[莲花山-人工丹霞_-_panoramio_-_蒋鹏飞.jpg](https://zh.wikipedia.org/wiki/File:莲花山-人工丹霞_-_panoramio_-_蒋鹏飞.jpg "fig:莲花山-人工丹霞_-_panoramio_-_蒋鹏飞.jpg")

**蓮花山**位於[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[广州市](../Page/广州市.md "wikilink")[番禺區石樓鎮](../Page/番禺區.md "wikilink")，在番禺区的東邊，面瀶[珠江的獅子洋水道](../Page/珠江.md "wikilink")，海拔约108米(2010年数据)。與[廣州港的新沙港码头隔江相望](../Page/廣州港.md "wikilink")，距市桥城區約17公里。以往，得名于屹立於珠江旁約百多米高的**蓮花山**（因山處於獅子洋畔，故古名**石獅頭**）因全山皆為堅硬的[麻石](../Page/麻石.md "wikilink")，所以成為古代的採礦場，雖然古採礦場已荒廢了幾百年，但是也造就了該鎮的漁農業和旅遊發展。

2002年，莲花山风景旅游区被入选成为“新世纪[羊城八景](../Page/羊城八景.md "wikilink")”之一的“莲峰观海”。

## 历史

莲花山的历史可以追溯到[西汉时期](../Page/西汉.md "wikilink")，这里曾经是[采石场](../Page/采石场.md "wikilink")，一直延续至[清朝](../Page/清朝.md "wikilink")[道光年间](../Page/道光.md "wikilink")。[西汉南越王墓的石料就是采自莲花山](../Page/西汉南越王墓.md "wikilink")。

1957年建村，因地处莲花山旁得名。1958年成立莲花山公社，1980年改区，1986年建莲花山镇。2002年3月莲花山镇并入[石楼镇至今](../Page/石楼镇_\(广州市\).md "wikilink")，现已成为**莲花山风景旅游区**。

## 交通

因為蓮花山面临[珠江](../Page/珠江.md "wikilink")，所以其水路交通極為發達，在南沙客運碼頭建成之前，是番禺地區對外水路交通主要樞紐。**蓮花山碼頭**距離[香港和](../Page/香港.md "wikilink")[澳門均約](../Page/澳門.md "wikilink")60海里，每天有多班客輪往返香港。[佛莞城际轨道规划将在其北侧之茭塘村附近设地面](../Page/佛莞城际轨道.md "wikilink")[莲花站](../Page/莲花站（广州）.md "wikilink")。地铁方面，在[官桥站尚未开通之情况下](../Page/官桥站.md "wikilink")，较多游客选择从[广州地铁4号线](../Page/广州地铁4号线.md "wikilink")[石碁站乘坐市交委公交或政府宣布](../Page/石碁站.md "wikilink")“非法”的“摩托搭客仔”前往莲花山。

## 參見

  - [蓮花塔](../Page/蓮花塔.md "wikilink")

## 外部链接

  - [番禺莲花山旅游区](https://web.archive.org/web/20151006020231/http://www.pylianhuashan.com/)

[L](../Category/广州山峰.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")