[CDC_cranberry2.jpg](https://zh.wikipedia.org/wiki/File:CDC_cranberry2.jpg "fig:CDC_cranberry2.jpg")\]\]
**蔓越莓汁**是由[蔓越莓果肉榨出的](../Page/蔓越莓.md "wikilink")[果汁](../Page/果汁.md "wikilink")。商家在销售时很少会销售纯果汁，因为纯果汁并不美味，其味道感覺先酸後澀，而且也有成本考量。一般来说，蔓越莓汁会以“蔓越莓鸡尾”（cranberry
juice
cocktail）销售，这种饮料加入了[蔗糖或是](../Page/蔗糖.md "wikilink")[甜味剂](../Page/甜味剂.md "wikilink")（如[阿斯巴甜或](../Page/阿斯巴甜.md "wikilink")[三氯蔗糖](../Page/三氯蔗糖.md "wikilink")），或者是与[蘋果汁及](../Page/蘋果汁.md "wikilink")[葡萄汁混合](../Page/葡萄汁.md "wikilink")，有时也加入其他果汁和调味剂。

## 益处

在[醫學上](../Page/醫學.md "wikilink")，蔓越莓汁已被證實可有效降低[心血管疾病](../Page/心血管疾病.md "wikilink")\[1\]、[牙周病](../Page/牙周病.md "wikilink")、[胃潰瘍與](../Page/胃潰瘍.md "wikilink")[癌症](../Page/癌症.md "wikilink")\[2\]等疾病的罹患風險\[3\]；其富含[草酸盐](../Page/草酸盐.md "wikilink")，以前人们认为其能增加肾结石的患病率\[4\]，但现在研究表明蔓越莓汁可以降低[肾结石的患病风险](../Page/肾结石.md "wikilink")\[5\]。時常飲用蔓越莓汁，能使存在[尿液及](../Page/尿液.md "wikilink")[泌尿道中的](../Page/泌尿道.md "wikilink")[大肠杆菌鞭毛无法发挥作用](../Page/大肠杆菌.md "wikilink")，使其不易附著在[尿道管壁上](../Page/尿道.md "wikilink")、使[尿道環境較不利於](../Page/尿道.md "wikilink")[細菌生長](../Page/細菌.md "wikilink")，進而預防[泌尿道感染疾病](../Page/泌尿道感染.md "wikilink")\[6\]，不过蔓越莓汁对无[鞭毛的](../Page/鞭毛.md "wikilink")[大腸桿菌是没有作用的](../Page/大腸桿菌.md "wikilink")。蔓越莓汁也是天然的[利尿剂](../Page/利尿剂.md "wikilink")，可用来缓解[月经肿痛](../Page/月经.md "wikilink")。

## 健康問題

虽然蔓越莓汁能抑制细菌繁殖\[7\]，但其酸性很大，[pH达到](../Page/pH.md "wikilink")2.3－2.52，酸性超过大部分[軟性飲料](../Page/軟性飲料.md "wikilink")，会使[牙釉质很快溶解](../Page/牙釉质.md "wikilink")。

## 参考资料

[Category:果汁](../Category/果汁.md "wikilink")

1.

2.
3.  [SuperFruit: A Rundown on Cranberry
    Juice](http://cranberryjuice.com/benefits.html), CranberryJuice.com

4.

5.

6.  [How Cranberry Juice Can Prevent Urinary Tract
    Infections](http://www.sciencedaily.com/releases/2008/07/080721152005.htm),
    ScienceDaily

7.