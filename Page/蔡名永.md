**蔡名永**[湖北省](../Page/湖北省_\(中華民國\).md "wikilink")[雲夢縣大山鄉人](../Page/雲夢縣.md "wikilink")，[中華民國空軍](../Page/中華民國空軍.md "wikilink")[中將](../Page/中將.md "wikilink")。

## 曾獲勳章

  - [青天白日勳章](../Page/青天白日勳章.md "wikilink")
  - [寶鼎勳章](../Page/寶鼎勳章.md "wikilink")
  - [雲麾勳章](../Page/雲麾勳章.md "wikilink")
  - [洛書勳章](../Page/洛書勳章.md "wikilink")
  - [乾元勳章](../Page/乾元勳章.md "wikilink")
  - [勝利勳章](../Page/抗战胜利勋章.md "wikilink")
  - [復興勳章](../Page/復興勳章.md "wikilink")
  - [忠勤勳章](../Page/忠勤勳章.md "wikilink")
  - [宣威獎章](../Page/宣威獎章.md "wikilink")
  - 一星序獎章
  - 陸海空軍獎章
  - 忠貞獎章
  - 雄鷲獎章
  - 彤弓獎章
  - 光華獎章
  - 干城獎章
  - 懋績獎章
  - 楷模獎章
  - [美國飛鷹勳章](../Page/美國.md "wikilink")
  - [美國](../Page/美國.md "wikilink")[紫心勳章](../Page/紫心勳章.md "wikilink")

## 重要經歷

  - [兩廣事變](../Page/兩廣事變.md "wikilink")[贛州送款](../Page/贛州.md "wikilink")
  - [西安事變](../Page/西安事變.md "wikilink")
  - [抗日](../Page/抗日.md "wikilink")[蚌埠](../Page/蚌埠.md "wikilink")[會戰](../Page/會戰.md "wikilink")
  - 抗日[曹娥江會戰](../Page/曹娥江.md "wikilink")
  - 抗日京滬之戰
  - 抗日[西北防空](../Page/西北.md "wikilink")
  - 抗日[湘桂空防](../Page/湘桂.md "wikilink")
  - 抗日[重慶空防](../Page/重慶.md "wikilink")
  - 抗日川、甘、空防
  - 抗日[中原第三次會戰](../Page/中原.md "wikilink")
  - [國共內戰](../Page/國共內戰.md "wikilink")[魯南綏北](../Page/山東省.md "wikilink")[东北會戰](../Page/中国东北地区.md "wikilink")
  - 國共內戰[海南](../Page/海南.md "wikilink")、[舟山](../Page/舟山.md "wikilink")、撤退[臺灣之役](../Page/臺灣.md "wikilink")
  - [臺海空防](../Page/臺海.md "wikilink")
  - 臺海之戰[金門砲戰](../Page/金門砲戰.md "wikilink")
  - [中美聯合作戰](../Page/中美.md "wikilink")

[Category:青天白日勳章獲得者](../Category/青天白日勳章獲得者.md "wikilink")
[Category:中華民國空軍中將](../Category/中華民國空軍中將.md "wikilink")
[Category:國共戰爭人物](../Category/國共戰爭人物.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:中華民國空軍軍官學校校友](../Category/中華民國空軍軍官學校校友.md "wikilink")
[Category:湖北裔台灣人](../Category/湖北裔台灣人.md "wikilink")
[Category:雲夢人](../Category/雲夢人.md "wikilink") [Ming
Yong](../Category/蔡姓.md "wikilink")