**蔡啟芳**（）台湾政治人物，曾因[一清專案而入獄](../Page/一清專案.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")[布袋鎮新塭人](../Page/布袋鎮.md "wikilink")，[民主進步黨籍](../Page/民主進步黨.md "wikilink")。

## 生平

蔡啟芳出身政治世家，其祖父[蔡仁](../Page/蔡仁.md "wikilink")、父親[蔡長銘都擔任過嘉義縣議員](../Page/蔡長銘.md "wikilink")，父親蔡長銘為國民黨嘉義黃派要角，曾擔任嘉義縣議會第七、八屆副議長以及第九屆議長
\[1\]，其子[蔡易餘為民進黨籍現任立法委員](../Page/蔡易餘.md "wikilink")。

1984年因為[一清專案而入獄](../Page/一清專案.md "wikilink")。出獄後曾於1989年參選嘉義縣的增額立法委員選舉落選，但是在1990年當選嘉義縣[布袋鎮長](../Page/布袋鎮.md "wikilink")；1991年的第二屆[國大代表選舉中](../Page/國民大會.md "wikilink")，於嘉義市當選。1996年，蔡啟芳當選不分區國大代表並曾擔任國大黨團召集人，1997年，有意參選嘉義市長，但在黨內初選期間便被民進黨拒絕。蔡啟芳也曾擔任民進黨嘉義縣黨部主委以及嘉義市黨部主委，並且曾經加入過民進黨內次團[正義連線](../Page/正義連線.md "wikilink")。

  - 2004年，蔡啟芳連任第六屆立法委員，而且還擔任[台灣日光燈股份有限公司董事長](../Page/台灣日光燈股份有限公司.md "wikilink")。

<!-- end list -->

  - 2005年12月15日，宣布參選民主進步黨主席，但最終並未參加選舉。

<!-- end list -->

  - 2006年，[民進黨立委](../Page/民進黨.md "wikilink")[蕭美琴有意草擬及遊說](../Page/蕭美琴.md "wikilink")《同性婚姻法》的法律制定案，遭蔡等23名立委提出反對連署，阻擋《同性婚姻法》一讀。

<!-- end list -->

  - 2016年5月9日，蔡啟芳在[嘉義市政府後方開設蔡啟芳做嘜固](../Page/嘉義市政府.md "wikilink")[小吃店](../Page/小吃店.md "wikilink")，並擔任老闆\[2\]。但約在2017年即歇業。

<!-- end list -->

  - 2017年5月25日，其子[蔡易餘於政論節目](../Page/蔡易餘.md "wikilink")[新聞挖挖哇披露](../Page/新聞挖挖哇.md "wikilink")，在得知其很疼愛的一個外甥實為同性戀者後，轉而支持同性婚姻。\[3\]

## 爭議

### 黑道背景

蔡啟芳雖然出身政治世家，但初中時便在道上背著「菜市仔」的名號\[4\]，「走闖」[妓院](../Page/妓院.md "wikilink")、[酒家](../Page/酒家.md "wikilink")、[賭場](../Page/賭場.md "wikilink")，更曾因率眾砍人\[5\]，而與[竹聯幫老大](../Page/竹聯幫.md "wikilink")[陳啟禮](../Page/陳啟禮.md "wikilink")、[四海幫老大](../Page/四海幫.md "wikilink")[蔡冠倫等大哥級人物](../Page/蔡冠倫.md "wikilink")，一起移送[綠島管訓](../Page/綠島.md "wikilink")。

  - 1997年4月4日，蔡啟芳召開記者會，對於民進黨中執會，因為他具有黑道背景，而遲遲不提名他代表民進黨參選[嘉義市長表示不滿](../Page/嘉義市長.md "wikilink")\[6\]\[7\]。

<!-- end list -->

  - 2013年5月1日，[民進黨中常委](../Page/民進黨.md "wikilink")[李清福在中常會提及](../Page/李清福.md "wikilink")，「前立委蔡啟芳有槍砲彈藥前科、立委陳明文也有前科，是不是黑道？排黑要不要溯及既往？」\[8\]。[蔡易餘指出](../Page/蔡易餘.md "wikilink")，蔡啟芳以前曾贏得嘉義市長的黨內初選，當時就是因為黨內的「排黑」，無法獲得提名，此為確實案例。

### 立院三寶

  - [民進黨立委](../Page/民進黨.md "wikilink")[林重謨](../Page/林重謨.md "wikilink")、[侯水盛的](../Page/侯水盛.md "wikilink")[問政風格口無遮攔](../Page/問政.md "wikilink")，大小爭議不斷，甚至出口成髒，被外界戲稱為民進黨三寶，但3人不以為意，而且還組成三寶連線聯合競選，口無遮攔的三寶，3個都淪為票房毒藥\[9\]\[10\]。

### 相關人士遭檢方以賄選偵辦未起訴

2007年7月11日，[民進黨嘉義縣黨部](../Page/民進黨.md "wikilink")[水上鄉席開](../Page/水上鄉.md "wikilink")20桌，宴請水上鄉地區的樁腳與選民共一百八十人，與會者包括[陳明文](../Page/陳明文.md "wikilink")、蔡啟芳、嘉義縣議員[吳李綠](../Page/吳李綠.md "wikilink")、[劉敬祥](../Page/劉敬祥.md "wikilink")、水上鄉農會總幹事[林滄賢](../Page/林滄賢.md "wikilink")、[林崑山等地方重要樁腳](../Page/林崑山.md "wikilink")，檢調表示，陳明文、蔡啟芳在餐會中，除公開要求賓客投票支持蔡啟芳，並要求與會者提早動員拉票。嘉義地檢署約談民進黨嘉義縣黨部執行長[吳溪瀨](../Page/吳溪瀨.md "wikilink")、水上鄉前鄉長[林崑山](../Page/林崑山.md "wikilink")、立委蔡啟芳助理[謝坤林等三人到案](../Page/謝坤林.md "wikilink")，檢方並以涉嫌替蔡啟芳期約賄選，分別各以十五、二十、十萬元交保\[11\]。2008年2月25日，檢察官[王振名偵結](../Page/王振名.md "wikilink")，民進黨嘉義縣黨部主委[蔡長雄等](../Page/蔡長雄.md "wikilink")20人全部獲得不起訴處分。蔡長雄表示，希望檢方往後辦案更慎重\[12\]。

### 侮辱女性

  - 2003年4月2日，蔡啟芳在議場上，一面做出女性胸部的動作，一面向同僚說：「[陳文茜委員曾說到乳房是女人社交的工具](../Page/陳文茜.md "wikilink")，我也想跟她交際一下」\[13\]\[14\]。

### 侮辱老師

  - 2003年11月，蔡在立院大罵：「多數老師都是一些王八蛋」；泛藍立委立即予以強烈譴責；全國教師會也強烈要求蔡道歉。原本還不願承認失言的蔡啟芳，最後仍在民進黨團的要求下，下午主動舉行記者會，承認自己措詞不當，向全國的老師道歉\[15\]\[16\]。

### 介入媒體

  - 2005年，蔡曾宣稱如TVBS不關門，就改姓「婊」，但隨後[陳水扁總統出面表示](../Page/陳水扁.md "wikilink")，在他任內不會關掉任何媒體\[17\]\[18\]，蔡啟芳後向TVBS道歉，並且在自己的立委辦公室名牌貼上「裱」字，強調一切就事論事，以後大家叫他「裱」哥也沒關係\[19\]。

### 嗆兒不推赦扁就斷關係

  - 2017年5月22日，蔡在[臉書上向](../Page/臉書.md "wikilink")[立委兒子](../Page/立委.md "wikilink")[蔡易餘](../Page/蔡易餘.md "wikilink")「喊話」表示，「立院本會期蔡易餘是司委會召委，如不把《赦免法》之修正案排入議程，那父子關係就結束」\[20\]\[21\]。蔡易餘表示，「父親很明顯的，他應該是在[講幹話](../Page/講幹話.md "wikilink")。大家都知道，我爸爸是講幹話之王。」\[22\]\[23\]\[24\]。

### 爆料民進黨派系花錢養黨員

  - 2017年5月24日，蔡啟芳在臉書上爆料，民進黨長期被[新潮流系與](../Page/新潮流系.md "wikilink")[美麗島系把持](../Page/美麗島系.md "wikilink")，要當黨主席的人必須看這兩個派系的臉色，因為這兩個派系在全國各地皆有經營黨員、代繳黨費等，所以各地方黨部主任委員或黨代表不是新潮流系、就是美麗島系，「而這兩個派系又皆是[親中的派系](../Page/親中.md "wikilink")，金錢方面當不虞匱乏」。蔡啟芳甚至宣稱，近二十年來，他每年花費新臺幣數千多萬元，在民進黨嘉義縣黨部及嘉義市黨部養了四千多名[人頭黨員](../Page/人頭黨員.md "wikilink")。同月25日，[民進黨秘書長](../Page/民進黨秘書長.md "wikilink")[洪耀福回應](../Page/洪耀福.md "wikilink")，民進黨過去因中下階層黨員較多而有些公職人員幫忙代繳黨費，但是繳黨費制度已經改了很多年，現在一年黨費新臺幣300元多是黨員親自去繳，「蔡啟芳活在二十年前」\[25\]。

## 第七屆立委選舉

  - 2008年，參選連任第七屆立法委員失利。

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><strong>蔡啟芳</strong></p></td>
<td></td>
<td><p>55860</p></td>
<td><p>42.53%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/翁重鈞.md" title="wikilink">翁重鈞</a></p></td>
<td></td>
<td><p>75489</p></td>
<td><p>57.47%</p></td>
<td><div align="center">
<p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
</tbody>
</table>

## 參考文獻

<div class="references-small">

<references/>

</div>

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00194&stage=6)

[Category:第2屆中華民國國民大會代表](../Category/第2屆中華民國國民大會代表.md "wikilink")
[Category:第3屆中華民國國民大會代表](../Category/第3屆中華民國國民大會代表.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:布袋鎮鎮長](../Category/布袋鎮鎮長.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:國立空中大學校友](../Category/國立空中大學校友.md "wikilink")
[Category:布袋人](../Category/布袋人.md "wikilink")
[Q啟](../Category/蔡姓.md "wikilink")
[Category:台灣罪犯](../Category/台灣罪犯.md "wikilink")
[Category:台灣黑社會成員](../Category/台灣黑社會成員.md "wikilink")
[Category:政治家族](../Category/政治家族.md "wikilink")
[Category:反同性婚姻人士](../Category/反同性婚姻人士.md "wikilink")

1.  [嘉義縣議會會史館歷屆正副議長](http://www.cyscc.gov.tw/oldsit/main_02.htm)
2.
3.
4.  [蔡啟芳
    鳥笑話唬人](http://www.appledaily.com.tw/appledaily/article/supplement/20030612/103495/%E8%94%A1%E5%95%9F%E8%8A%B3%E9%B3%A5%E7%AC%91%E8%A9%B1%E5%94%AC%E4%BA%BA)
5.
6.  [蔡啟芳不滿民進黨提名制度](http://news.cts.com.tw/cts/general/199704/199704040002317.html)
7.  [「黑道」從政展實力？掉漆的也不少](http://news.tvbs.com.tw/ttalk/blog_author_detail/2682)
8.  [蔡啟芳、陳明文被抹黑　蔡易餘擬嗆聲為嘉義幫討公道](http://www.nownews.com/n/2013/05/07/278958)
9.  [動口又動手\!
    新三寶恐變選舉毒藥](http://news.cts.com.tw/cts/politics/201305/201305041237146.html#.WSRDP2iGM2w)
10. [民進黨“立院三寶”全部落馬](http://news.sina.com.cn/c/2008-01-12/203214728449.shtml)
11. [蔡啟芳赴百人宴
    捲入賄選](http://www.appledaily.com.tw/appledaily/article/headline/20070711/3633602/)
12.
13. [台灣“立委”蔡啟芳“乳房交際說”攪起波瀾](http://news.southcn.com/hktwma/hot/200304031275.htm)
14. [乳房交際說 國親批蔡啟芳](http://old.ltn.com.tw/2003/new/apr/3/today-p3.htm)
15. [髒話立委蔡啟芳罵老師王八蛋](http://www.appledaily.com.tw/appledaily/article/headline/20031118/505358/)
16. [蔡啟芳罵老師王八蛋　國親圍剿](http://news.tvbs.com.tw/other/393822)
17. [新聞花絮
    蔡啟芳改姓「裱」假道歉](http://www.appledaily.com.tw/appledaily/article/headline/20051102/2173176/)
18. [立法院全球資訊網－立法委員－蔡啟芳最新消息](http://www.ly.gov.tw/03_leg/0301_main/leg_news/newsView.action?id=3601&lgno=00194&stage=6&atcid=3601)
19. [阿扁最大 蔡啟芳大轉彎
    「叫我婊哥」](http://www.idn.com.tw/news/news_content.php?catid=1&catsid=2&catdid=0&artid=20051102andy005)
20. [為了特赦阿扁 前立委蔡啟芳向立委兒子下通牒](https://udn.com/news/story/6656/2478515)
21. [蔡啟芳喊話特赦阿扁
    不惜與蔡易餘「斷絕關係」](http://news.ltn.com.tw/news/politics/breakingnews/2075670)
22. [蔡啟芳要斷父子關係？蔡易餘：爸爸是講幹話之王](https://udn.com/news/story/6656/2479435)
23. [蔡啟芳嗆兒斷絕父子關係
    蔡易餘：爸是幹話之王](http://www.chinatimes.com/realtimenews/20170523002497-260407)
24. [蔡啟芳為赦扁嗆休兒　酸小英「民進黨馬英九」](http://www.appledaily.com.tw/realtimenews/article/new/20170523/1124391/)
25.