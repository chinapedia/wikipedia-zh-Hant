**蔡士勤**（），為[台灣的](../Page/台灣.md "wikilink")[棒球選手之一](../Page/棒球.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[統一獅隊](../Page/統一獅.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")。2010年底，因戰力外遭球隊釋出。

## 經歷

  - [台南市公園國小少棒隊](../Page/台南市.md "wikilink")（府城）
  - [屏東縣美和中學青少棒隊](../Page/屏東縣.md "wikilink")
  - [屏東縣美和中學青棒隊](../Page/屏東縣.md "wikilink")（義美）
  - [台南市南英商工青棒隊](../Page/台南市.md "wikilink")
  - [文化大學棒球隊](../Page/文化大學.md "wikilink")（美孚巨人）
  - 聲寶巨人棒球隊
  - [台灣大聯盟](../Page/台灣大聯盟.md "wikilink")[嘉南勇士隊](../Page/嘉南勇士.md "wikilink")（2000年－2002年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰太陽隊](../Page/誠泰太陽.md "wikilink")（2003年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰COBRAS隊](../Page/誠泰COBRAS.md "wikilink")（2004年－2007年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[統一獅隊](../Page/統一獅.md "wikilink")（2007年－2010年）

## 職棒生涯成績

  - **粗體字**為全聯盟最佳或最高成績

| 年度                               | 球隊                                               | 出賽  | 勝投 | 敗投     | 中繼 | 救援 | 完投 | 完封 | 四死  | 三振  | 責失   | 投球局數  | 防禦率  |
| -------------------------------- | ------------------------------------------------ | --- | -- | ------ | -- | -- | -- | -- | --- | --- | ---- | ----- | ---- |
| 2003年                            | [誠泰太陽](../Page/誠泰太陽.md "wikilink")               | 36  | 5  | **13** | 0  | 0  | 1  | 0  | 51  | 71  | 62   | 129.0 | 4.33 |
| 2004年                            | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink")       | 31  | 2  | 7      | 0  | 0  | 1  | 0  | 29  | 54  | 31   | 84.0  | 3.32 |
| 2005年                            | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink")       | 18  | 1  | 1      | 0  | 0  | 0  | 0  | 19  | 15  | 25   | 36.1  | 6.19 |
| 2006年                            | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink")       | 13  | 2  | 1      | 0  | 0  | 0  | 0  | 7   | 15  | 15   | 29.0  | 4.66 |
| 2007年                            | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink")       | 11  | 0  | 1      | 0  | 0  | 1  | 0  | 6   | 10  | 13   | 14.2  | 7.98 |
| [統一獅](../Page/統一獅.md "wikilink") | 13                                               | 0   | 1  | 0      | 2  | 0  | 0  | 7  | 14  | 7   | 20.0 | 3.15  |      |
| 2008年                            | [統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink") | 15  | 1  | 2      | 3  | 0  | 0  | 0  | 5   | 9   | 8    | 14.1  | 5.02 |
| 2009年                            | [統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink") | 10  | 0  | 0      | 2  | 0  | 0  | 0  | 12  | 11  | 2    | 18.0  | 1.00 |
| 合計                               | 7年                                               | 147 | 11 | 25     | 5  | 2  | 2  | 0  | 136 | 199 | 163  | 345.1 | 4.25 |

## 特殊事蹟

## 外部連結

[S](../Category/蔡姓.md "wikilink") [T](../Category/在世人物.md "wikilink")
[T](../Category/1976年出生.md "wikilink")
[T](../Category/統一獅隊球員.md "wikilink")
[T](../Category/台灣棒球選手.md "wikilink")
[T](../Category/嘉南勇士隊球員.md "wikilink")
[T](../Category/美和中學青少棒隊.md "wikilink")
[T](../Category/中國文化大學校友.md "wikilink")
[T](../Category/誠泰COBRAS隊球員.md "wikilink")