**蔡明**，[女](../Page/女.md "wikilink")，[北京人](../Page/北京.md "wikilink")，[回族](../Page/回族.md "wikilink")。[中国民主促进会会员](../Page/中国民主促进会.md "wikilink")\[1\]。[中国著名](../Page/中国.md "wikilink")[演员](../Page/演员.md "wikilink")。\[2\]\[3\]\[4\]小学时扮演电影《[海霞](../Page/海霞_\(电影\).md "wikilink")》中的小海霞而成名，之后蔡明多次在[春节联欢晚会等节目中亮相](../Page/春节联欢晚会.md "wikilink")，成为了中国大陆家喻户晓的影视、小品演员。\[5\]

## 关于

蔡明在小学时担任了电影《海霞》中的主人公小海霞而一炮成名，另外她还主演过《[离婚大战](../Page/离婚大战.md "wikilink")》、《[捣蛋部队](../Page/捣蛋部队.md "wikilink")》、《[临时家庭](../Page/临时家庭.md "wikilink")》、《[心理诊所](../Page/心理诊所.md "wikilink")》以及喜剧《[闲人马大姐](../Page/闲人马大姐.md "wikilink")》等[连续剧](../Page/连续剧.md "wikilink")、[喜剧](../Page/喜剧.md "wikilink")。另外，蔡明还出了一系列歌曲翻唱的唱片，被人们称为“百变蔡明”。

## 作品

### 电影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>中文名</p></th>
<th><p>英文名</p></th>
<th><p>角色</p></th>
<th><p>合作演员</p></th>
<th><p>导演</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1973</p></td>
<td><p><a href="../Page/海霞.md" title="wikilink">海霞</a></p></td>
<td></td>
<td><p>海霞</p></td>
<td><p><a href="../Page/吴海燕.md" title="wikilink">吴海燕</a>、<a href="../Page/陈强.md" title="wikilink">陈强</a>、<a href="../Page/田冲.md" title="wikilink">田冲</a></p></td>
<td><p><a href="../Page/谢铁骊.md" title="wikilink">谢铁骊</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1980</p></td>
<td><p><a href="../Page/戴手铐的旅客.md" title="wikilink">戴手铐的旅客</a></p></td>
<td></td>
<td><p>魏小明</p></td>
<td><p><a href="../Page/于洋.md" title="wikilink">于洋</a></p></td>
<td><p>于洋</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1984</p></td>
<td><p><a href="../Page/生财有道.md" title="wikilink">生财有道</a></p></td>
<td></td>
<td><p>清秀</p></td>
<td><p>陈强</p></td>
<td><p><a href="../Page/谢添.md" title="wikilink">谢添</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1985</p></td>
<td><p><a href="../Page/泪洒姑苏.md" title="wikilink">泪洒姑苏</a></p></td>
<td></td>
<td><p>小玲</p></td>
<td><p><a href="../Page/邬倩倩.md" title="wikilink">邬倩倩</a></p></td>
<td><p>陈方千</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/倒蛋部队.md" title="wikilink">倒蛋部队</a></p></td>
<td></td>
<td><p>农村女子</p></td>
<td><p><a href="../Page/章金莱.md" title="wikilink">章金莱</a>、郭达、<a href="../Page/傅艺伟.md" title="wikilink">傅艺伟</a></p></td>
<td><p>王凤奎</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td><p><a href="../Page/离婚大战.md" title="wikilink">离婚大战</a></p></td>
<td></td>
<td><p>女青年</p></td>
<td><p>英子</p></td>
<td><p><a href="../Page/葛优.md" title="wikilink">葛优</a>、<a href="../Page/侯耀华.md" title="wikilink">侯耀华</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/生死劫.md" title="wikilink">生死劫</a></p></td>
<td></td>
<td><p>姨妈</p></td>
<td><p><a href="../Page/周迅.md" title="wikilink">周迅</a>、吴军、苏小明</p></td>
<td><p>李少红</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/亲家过年.md" title="wikilink">亲家过年</a></p></td>
<td></td>
<td><p>蔡云</p></td>
<td><p><a href="../Page/文章_(演员).md" title="wikilink">文章</a>、<a href="../Page/张丰毅.md" title="wikilink">张丰毅</a>、<a href="../Page/丛珊.md" title="wikilink">丛珊</a></p></td>
<td><p>叶伟民</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p><a href="../Page/天气预爆.md" title="wikilink">天气预爆</a></p></td>
<td></td>
<td><p>王母娘娘</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 电视剧

| 年份                                       | 中文名                                            | 角色          | 备注 |
| ---------------------------------------- | ---------------------------------------------- | ----------- | -- |
| 1993                                     | [女人心](../Page/女人心_\(电视剧\).md "wikilink")       | 叮咚          |    |
| 1994                                     | [临时家庭](../Page/临时家庭.md "wikilink")             | 周星星         |    |
| [我爱我家](../Page/我爱我家.md "wikilink")       | 燕红                                             |             |    |
| 1999                                     | [曹操](../Page/曹操_\(电视剧\).md "wikilink")         | 于禁          |    |
| [爱我好不好](../Page/爱我好不好.md "wikilink")     | 孟小蔷                                            |             |    |
| 2000                                     | [酒结良缘](../Page/酒结良缘.md "wikilink")             | 小婉妈妈        |    |
| [闲人马大姐](../Page/闲人马大姐.md "wikilink")     | 马大姐                                            |             |    |
| [马大姐和邻居们](../Page/马大姐和邻居们.md "wikilink") | 马大姐                                            |             |    |
| 2001                                     | [东北一家人](../Page/东北一家人.md "wikilink")           | 高娃          |    |
| 2002                                     | [党员马大姐](../Page/党员马大姐.md "wikilink")           | 马大姐         |    |
| 2003                                     | [带着孩子结婚](../Page/带着孩子结婚.md "wikilink")         | 刘小红         |    |
| 2004                                     | [家和万事兴之双喜临门](../Page/家和万事兴之双喜临门.md "wikilink") | 陶兰          |    |
| 2006                                     | [十全十美之有梦年代](../Page/十全十美之有梦年代.md "wikilink")   | 朱丽云         |    |
| [绝代妖后](../Page/绝代妖后.md "wikilink")       | 福晋                                             |             |    |
| 2007                                     | [马大姐新传](../Page/马大姐新传.md "wikilink")           | 马大姐         |    |
| 2008                                     | [清明上河](../Page/清明上河.md "wikilink")             | 灾民女         |    |
| [业余侦探](../Page/业余侦探.md "wikilink")       | 杨霞                                             |             |    |
| [好孕来临](../Page/好孕来临.md "wikilink")       | 丹丹妈妈                                           |             |    |
| 2009                                     | [超人马大姐](../Page/超人马大姐.md "wikilink")           | 马大姐         |    |
| [眼花缭乱](../Page/眼花缭乱.md "wikilink")       | 吴莎莎                                            |             |    |
| 2010                                     | [鸡毛蒜皮没小事](../Page/鸡毛蒜皮没小事.md "wikilink")       | 张大妈         |    |
| [荣河镇的男人们](../Page/荣河镇的男人们.md "wikilink") | 何三姑                                            |             |    |
| 2012                                     | [奇异家庭](../Page/奇异家庭.md "wikilink")             | 芥兰          |    |
| 2014                                     | [老妈的三国时代](../Page/老妈的三国时代.md "wikilink")       | 曹梅花         |    |
| 2015                                     | [嫁个老公过日子](../Page/嫁个老公过日子.md "wikilink")       | 劉慧榮         |    |
| [涨停板跌停板](../Page/涨停板跌停板.md "wikilink")   |                                                | 网剧, 2010年拍攝 |    |
|                                          |                                                |             |    |

### 春晚小品

<table>
<thead>
<tr class="header">
<th><p>年</p></th>
<th><p>英文名</p></th>
<th><p>中文名</p></th>
<th><p>合作者</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1991</p></td>
<td></td>
<td><p>《陌生人》</p></td>
<td><p><a href="../Page/巩汉林.md" title="wikilink">巩汉林</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td></td>
<td><p>《黄土坡》</p></td>
<td><p><a href="../Page/郭达.md" title="wikilink">郭达</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td></td>
<td><p>《越洋电话》</p></td>
<td><p>郭达</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
<td></td>
<td><p>《父亲》</p></td>
<td><p>郭达、<a href="../Page/赵宝乐.md" title="wikilink">赵宝乐</a>、<a href="../Page/于海伦.md" title="wikilink">于海伦</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
<td></td>
<td><p>《<a href="../Page/机器人趣话.md" title="wikilink">机器人趣话</a>》</p></td>
<td><p>郭达</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td></td>
<td><p>《过年》</p></td>
<td><p>郭达、<a href="../Page/郭冬临.md" title="wikilink">郭冬临</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td></td>
<td><p>《球迷》</p></td>
<td><p>郭达、郭冬临</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td></td>
<td><p>《爱笑的女孩》</p></td>
<td><p><a href="../Page/文兴宇.md" title="wikilink">文兴宇</a>、<a href="../Page/句号_(演员).md" title="wikilink">句号</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td></td>
<td><p>《红娘》</p></td>
<td><p>郭达</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td></td>
<td><p>《邻里之间》</p></td>
<td><p>郭达、<a href="../Page/牛群.md" title="wikilink">牛群</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td></td>
<td><p>《都是亲人》</p></td>
<td><p>郭达、<a href="../Page/李文启.md" title="wikilink">李文启</a>、刘晓梅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td></td>
<td><p>《婚礼》</p></td>
<td><p>英壮、<a href="../Page/李咏.md" title="wikilink">李咏</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td></td>
<td><p>《浪漫的事》</p></td>
<td><p>郭达、韩英</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td></td>
<td><p>《<a href="../Page/马大姐外传.md" title="wikilink">马大姐外传</a>》</p></td>
<td><p>郭达</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td></td>
<td><p>《送礼》</p></td>
<td><p>郭达、句号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td></td>
<td><p>《<a href="../Page/梦幻家园.md" title="wikilink">梦幻家园</a>》</p></td>
<td><p>郭达、<a href="../Page/王平_(相声演员).md" title="wikilink">王平</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td></td>
<td><p>《北京欢迎你》</p></td>
<td><p>郭达</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td></td>
<td><p>《家有毕业生》</p></td>
<td><p><a href="../Page/郭达.md" title="wikilink">郭达</a>、<a href="../Page/黄杨.md" title="wikilink">黄杨</a>、<a href="../Page/郭笑.md" title="wikilink">郭笑</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td></td>
<td><p>《想跳就跳》</p></td>
<td><p><a href="../Page/潘长江.md" title="wikilink">潘长江</a>、<a href="../Page/穆雪峰.md" title="wikilink">穆雪峰</a>、<a href="../Page/高粼粼.md" title="wikilink">高粼粼</a>、<a href="../Page/刘畅_(男演员).md" title="wikilink">刘畅</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td></td>
<td><p>《扰民》</p></td>
<td><p><a href="../Page/华少.md" title="wikilink">华少</a>、<a href="../Page/岳云鹏.md" title="wikilink">岳云鹏</a>、<a href="../Page/大鹏.md" title="wikilink">大鹏</a></p></td>
<td><p>[6]</p></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td></td>
<td><p>《车站奇遇》</p></td>
<td><p><a href="../Page/潘长江.md" title="wikilink">潘长江</a>、<a href="../Page/穆雪峰.md" title="wikilink">穆雪峰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td></td>
<td><p>《网购奇遇》</p></td>
<td><p><a href="../Page/潘长江.md" title="wikilink">潘长江</a>、<a href="../Page/高粼粼.md" title="wikilink">高粼粼</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td></td>
<td><p>《老伴》</p></td>
<td><p><a href="../Page/潘長江.md" title="wikilink">潘長江</a>、<a href="../Page/潘斌龍.md" title="wikilink">潘斌龍</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td></td>
<td><p>《学车》</p></td>
<td><p><a href="../Page/潘长江.md" title="wikilink">潘长江</a>、<a href="../Page/贾冰.md" title="wikilink">贾冰</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2019</p></td>
<td></td>
<td><p>《<a href="../Page/“儿子”来了.md" title="wikilink">“儿子”来了</a>》</p></td>
<td><p><a href="../Page/葛优.md" title="wikilink">葛优</a>、<a href="../Page/乔杉.md" title="wikilink">乔杉</a>、<a href="../Page/翟天临.md" title="wikilink">翟天临</a>、<a href="../Page/潘长江.md" title="wikilink">潘长江</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 综艺

  - 2016-17年 [王牌对王牌](../Page/王牌对王牌_\(真人秀\).md "wikilink")

### 音乐专辑

  - 《跨越》，包括歌曲59首，2004年1月1日发行。

### 有声读物

  - 《开心奶奶讲故事》\[7\]\[8\]\[9\]\[10\]

## 奖项

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>作品</p></th>
<th><p>奖项</p></th>
<th><p>是否得奖</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2000</p></td>
<td></td>
<td><p>中国大众电视老百姓最喜爱影视明星</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td></td>
<td><p>第一届华鼎奖公众形象最佳曲艺演员</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td></td>
<td><p>第二届华鼎奖中国年度影视最佳表现女演员</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td></td>
<td><p>第十届中国国际儿童电影节一路成长的童星</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td></td>
<td><p>第四届华鼎奖老百姓最喜爱影视明星</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 相关条目

  - [郭达](../Page/郭达.md "wikilink")
  - [小品](../Page/小品.md "wikilink")
  - [喜剧](../Page/喜剧.md "wikilink")
  - [闲人马大姐](../Page/闲人马大姐.md "wikilink")

## 参考文献

## 外部链接

  -
  -
[C蔡](../Category/中国小品演员.md "wikilink")
[C蔡](../Category/中国喜剧演员.md "wikilink")
[C蔡](../Category/北京人.md "wikilink")
[C蔡](../Category/回族演員.md "wikilink")
[C蔡](../Category/中国民主促进会会员.md "wikilink")
[M明](../Category/蔡姓.md "wikilink")

1.  [相声演员于谦已低调加入中国国民党革命委员会](http://ent.163.com/16/0509/11/BMKBOTT800031H2L.html#p=9HDPTG6O00AJ0003)
2.
3.
4.
5.
6.  [蔡明央视马年春晚再坐轮椅
    网友调侃：真瘸了？](http://news.cnwest.com/content/2014-01/30/content_10685666.htm).
    北方网. 2014年1月30日.
7.
8.
9.
10.