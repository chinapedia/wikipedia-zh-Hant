**蔡素玉**（，），前[香港立法會議員](../Page/香港立法會.md "wikilink")，現任香港[東區區議會錦屏選區民選區議員](../Page/東區區議會.md "wikilink")，[港區全國人大代表](../Page/港區全國人大代表.md "wikilink")。而她因兼任人大被評為「港島民選人大」。

## 生平

蔡素玉父親為[菲律賓華僑](../Page/菲律賓.md "wikilink")、[中國國民黨員](../Page/中國國民黨.md "wikilink")，[中國共產黨取得](../Page/中國共產黨.md "wikilink")[中國大陸的](../Page/中國大陸.md "wikilink")[政權後](../Page/政權.md "wikilink")，蔡素玉父親把蔡素玉由[福建](../Page/福建.md "wikilink")[晉江申請移居到香港](../Page/晉江.md "wikilink")，為[北角的居民以及區議員](../Page/北角.md "wikilink")，現居於[炮台山站一帶](../Page/炮台山站.md "wikilink")，曾就讀[福建小學](../Page/福建小學.md "wikilink")、[英華女學校](../Page/英華女學校.md "wikilink")。[香港大學](../Page/香港大學.md "wikilink")[哲學碩士](../Page/哲學碩士.md "wikilink")、[理學士](../Page/理學士.md "wikilink")，未婚。

蔡素玉本來是[港進聯的成員](../Page/港進聯.md "wikilink")，1995年首度參選[立法局直選](../Page/香港立法會.md "wikilink")，挑戰當時港島東的民主黨主席[李柱銘](../Page/李柱銘.md "wikilink")，最終落敗。

1996年，蔡素玉加入[臨時立法會](../Page/臨時立法會.md "wikilink")。

在[2000年香港立法會選舉](../Page/2000年香港立法會選舉.md "wikilink")，蔡素玉為了幫助[民建聯取得香港島第二個議席而參加民建聯](../Page/民建聯.md "wikilink")，夥同[程介南參選](../Page/程介南.md "wikilink")，並成功當選，其後程介南因以權謀私辭職，她維持民建聯的一席。

2005年獲政府委任為[太平紳士](../Page/太平紳士.md "wikilink")。2008年1月，當選為第十一屆[港區全國人大代表](../Page/港區全國人大代表.md "wikilink")。

[2008年立法會選舉中](../Page/2008年香港立法會選舉.md "wikilink")，蔡素玉落選立法會議席，其與民建聯前主席[曾鈺成合組之名單](../Page/曾鈺成.md "wikilink")，最後曾鈺成連任成功，只取得60,417票，無足夠票讓名單排第二的蔡素玉入選。而同屬建制派的參選人，還有前保安局局長[葉劉淑儀及](../Page/葉劉淑儀.md "wikilink")[史泰祖之名單](../Page/史泰祖.md "wikilink")，葉劉淑儀當選議員。

她現正專注港島[東區區議會工作](../Page/東區區議會.md "wikilink")，繼續為港島區市民服務，而她亦於2011年及2015年自動當選錦屏選區區議員。

## 爭議

### 2004年香港立法會選舉

[2004年香港立法會選舉](../Page/2004年香港立法會選舉.md "wikilink")，[民建聯雖然為街坊設海鮮餐](../Page/民建聯.md "wikilink")，惹起賄選之嫌，但最終證據不足；最終她以些微票數壓倒[何秀蘭奪得港島區最後一個議席](../Page/何秀蘭.md "wikilink")。

2003年11月12日，[涂謹申提出](../Page/涂謹申.md "wikilink")07/08普選的議案，民建聯投下反對票。不過，於選舉期間，蔡出席港台選舉論壇時，卻指自己支持07/08普選，更指「民建聯是最有誠信的，我們是支持07/08的，要在政綱上」，引起市民強烈批評。\[1\]

## 參考資料

<references />

[Category:第十一屆港區全國人大代表](../Category/第十一屆港區全國人大代表.md "wikilink")
[Category:第十二屆港區全國人大代表](../Category/第十二屆港區全國人大代表.md "wikilink")
[Category:女性全國人大代表](../Category/女性全國人大代表.md "wikilink")
[Category:福建省政協委員](../Category/福建省政協委員.md "wikilink")
[Category:東區區議員](../Category/東區區議員.md "wikilink")
[Category:民主建港協進聯盟成員](../Category/民主建港協進聯盟成員.md "wikilink")
[Category:前香港協進聯盟成員](../Category/前香港協進聯盟成員.md "wikilink")
[Category:香港女性政治人物](../Category/香港女性政治人物.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:英華女學校校友](../Category/英華女學校校友.md "wikilink")
[Category:香港福建人](../Category/香港福建人.md "wikilink")
[Category:晋江人](../Category/晋江人.md "wikilink")
[S](../Category/蔡姓.md "wikilink")
[Category:前香港立法會議員](../Category/前香港立法會議員.md "wikilink")
[Category:第十三屆港區全國人大代表](../Category/第十三屆港區全國人大代表.md "wikilink")

1.  [蔡素玉出席選舉論壇電視畫面](http://evchk.wikia.com/wiki/Image:Dab_honesty.jpg)