**蔡適應**（），[中華民國政治人物](../Page/中華民國.md "wikilink")，[國軍](../Page/國軍.md "wikilink")[陸軍](../Page/陸軍.md "wikilink")[志願役](../Page/志願役.md "wikilink")[砲兵科](../Page/砲兵.md "wikilink")[上尉出身](../Page/上尉.md "wikilink")\[1\]，[民主進步黨籍](../Page/民主進步黨.md "wikilink")，曾任[基隆市議員](../Page/基隆市.md "wikilink")、民進黨基隆市黨部主任委員、[經國管理暨健康學院](../Page/經國管理暨健康學院.md "wikilink")[講師](../Page/講師.md "wikilink")、基隆市觀光協會理事長、台灣四季文教協會秘書長。[2016年](../Page/2016年.md "wikilink")[1月16日當選第九屆](../Page/1月16日.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")（基隆市）。於立法院第一、二、三、四、五會期連續獲[公督盟評鑑為優秀立委](../Page/公督盟.md "wikilink")。
立法院第九屆第四會期、第七會期外交與國防委員會召集委員、民進黨立院黨團副幹事長、副書記長，以及民進黨中央黨部發言人。

## 經歷

蔡適應乃是台灣民主進步黨新世代青年高階幹部代表人物之一，台灣一般稱為民進黨幕僚世代。所謂幕僚世代，是指民進黨內出生於60年代末70年代初的年輕世代，他們多是出於對政治的興趣，在大學畢業後進入立法院或其他民進黨公職人物辦公室，從助理幕僚工作做起，逐漸在政壇嶄露頭角。

蔡適應出身於中產階級，陸軍[志願役](../Page/志願役.md "wikilink")[軍官退伍後](../Page/軍官.md "wikilink")\[2\]，進入立委[王拓服務處擔任助理工作](../Page/王拓.md "wikilink")，歷任民進黨基隆市黨部評議委員、執行委員、黨政小組召集人、2004年[陳水扁總統基隆市競選總部副總幹事](../Page/陳水扁.md "wikilink")，2005年台灣縣市議員暨縣市長選舉，在王拓支持下，代表民進黨出馬，競選基隆市[安樂區市議員](../Page/安樂區.md "wikilink")，並以議會最年經之姿當選。後接任基隆市議會建設小組副召集人、基隆市議會民進黨團總召。

蔡適應於就讀大學階段，正是台灣變遷的重要時刻，蔡適應亦開始參與，諸如[工運](../Page/工運.md "wikilink")[秋鬥](../Page/秋鬥.md "wikilink")、[春鬥](../Page/春鬥.md "wikilink")、[廢除刑法一百條](../Page/中華民國刑法第一百條.md "wikilink")，廢國大、[地下電台合法化遊行等](../Page/地下電台.md "wikilink")。過程中，增加其對社會的關懷與投入。此外在校園內，亦主張學生權的擴大與法制化，例如參與學生會，擔任學生議會議員等，以及參與要求修訂[大學法等](../Page/大學法.md "wikilink")。

2009年，台灣縣市議員暨縣市長選舉在安樂區獲最高票連任。

2014年5月起，擔任基隆市黨部主任委員，負責年底九合一選舉，輔選市長議員候選人順利當選。

2014年11月29日，九合一選舉在安樂區獲第一高票連任。

2016年1月16日，當選為基隆改為單一選區後，首任民進黨籍的立法委員。

## 參選

### 2016年立法委員選舉

## 學歷

  - [東海大學政治學系國際關係組學士](../Page/東海大學_\(台灣\).md "wikilink")
  - [國立臺灣師範大學國家發展碩士學分班結業](../Page/國立臺灣師範大學.md "wikilink")
  - [淡江大學國際事務與戰略研究所碩士](../Page/淡江大學.md "wikilink")
  - [比利時](../Page/比利時.md "wikilink")[魯汶大學](../Page/魯汶大學.md "wikilink")(KU
    LEUVEN),[European Spatial Development
    Planning](http://en.wikipedia.org/wiki/European_Spatial_Development_Planning)(ESDP)課程研究
  - [國立臺北大學都市計畫研究所博士候選人](../Page/國立臺北大學.md "wikilink")\[3\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [蔡適應個人部落格](https://web.archive.org/web/20160322094730/http://www.xn--ulur00ftxh.tw/)
  - [蔡適應個人網站](http://www.fourseason.dreamhosters.com/)
  - [蔡適應臉書粉絲專頁](https://www.facebook.com/%E5%9F%BA%E9%9A%86%E8%94%A1%E9%81%A9%E6%87%89-114129008623973/)
  - [基隆市議會
    蔡適應議員簡介](http://www.kmc.gov.tw/kmcweb/councilorArea2.asp?Area=安樂區&Inum=10)
  - [蔡適應部落格](http://blog.roodo.com/coopertsai)(舊網)
  - [台灣社區通
    台灣四季文教協會](http://sixstar.cca.gov.tw/community/newpage/aboutus.php?CommID=4915)
  - [東海大學校友總會
    蔡適應](https://web.archive.org/web/20060628103530/http://thu.maxfortune.com.tw/max/front/bin/ptdetail.phtml?Part=805940&Category=101423)
  - [中選會資料庫網站](http://210.69.23.140/cec/vote32.asp?pass1=G2005A62170504317050000)
  - [我的E政府
    台灣年鑑](http://www7.www.gov.tw/todaytw/2006/TWtaiwan/ch03/2-3-3-1.html)
  - [淡江大學國際事務與戰略研究所 歷屆傑出校友](http://www2.tku.edu.tw/~titx/web/07.htm)
  - [東海大學政治系
    歷屆傑出校友](http://www2.thu.edu.tw/~politic/alumni/alumni01.htm)
  - [中央社
    陸客來台專題](https://web.archive.org/web/20160304104537/http://www.cna.com.tw/CNA/specnews/default.aspx?E_specno=0497)
  - [台北大學
    博士班錄取公告](http://www.ntpu.edu.tw/admin/a7/org/a7-3/files/2008061691330.pdf)

[分類:基隆市選出的立法委員](../Page/分類:基隆市選出的立法委員.md "wikilink")

[Category:第17屆基隆市議員](../Category/第17屆基隆市議員.md "wikilink")
[Category:第16屆基隆市議員](../Category/第16屆基隆市議員.md "wikilink")
[Category:中華民國陸軍軍官](../Category/中華民國陸軍軍官.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:東海大學校友](../Category/東海大學校友.md "wikilink")
[Category:國立臺灣師範大學校友](../Category/國立臺灣師範大學校友.md "wikilink")
[Category:淡江大學校友](../Category/淡江大學校友.md "wikilink")
[Category:國立臺北大學校友](../Category/國立臺北大學校友.md "wikilink")
[Category:臺灣省立基隆高級中學校友](../Category/臺灣省立基隆高級中學校友.md "wikilink")
[Category:基隆市人](../Category/基隆市人.md "wikilink")
[Shi適](../Category/蔡姓.md "wikilink")

1.  [1](http://m.ltn.com.tw/news/politics/paper/1091450)

2.
3.  [國立台北大學博士班錄取公告](http://www.ntpu.edu.tw/admin/a7/org/a7-3/files/2008061691330.pdf)