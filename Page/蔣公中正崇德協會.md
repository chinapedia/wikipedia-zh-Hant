[Exhibition_by_Generalissimo_Chiang_Kai_Shek_Association_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Exhibition_by_Generalissimo_Chiang_Kai_Shek_Association_Hong_Kong.jpg "fig:Exhibition_by_Generalissimo_Chiang_Kai_Shek_Association_Hong_Kong.jpg")

**蔣公中正崇德協會，**簡稱**蔣公協會**，前稱**蔣公中正香港協會**，是[香港的民間](../Page/香港.md "wikilink")[親台團體](../Page/香港親台團體.md "wikilink")，於2006年成立，現任[會長為](../Page/會長.md "wikilink")[劉伯權](../Page/劉伯權.md "wikilink")，副會長為蘇建名\[1\]。協會以「尊崇[中華民國已故](../Page/中華民國.md "wikilink")[總統](../Page/中華民國總統.md "wikilink")[蔣中正](../Page/蔣中正.md "wikilink")、傳續[中華文化傳統](../Page/中華文化.md "wikilink")、推動香港當地[自由](../Page/自由.md "wikilink")[民主發展](../Page/民主.md "wikilink")」等作為其活動宗旨。

## 參與及主辦活動

蔣公中正崇德協會曾經參與[革命先烈紀念日](../Page/革命先烈紀念日.md "wikilink")／[三二九青年節](../Page/三二九青年節.md "wikilink")\[2\]、[蔣公逝世紀念日](../Page/蔣公逝世紀念日.md "wikilink")（4月5日）\[3\]\[4\]、由[香港市民支援愛國民主運動聯合會](../Page/香港市民支援愛國民主運動聯合會.md "wikilink")（支聯會）主辦的民主大遊行與[六四燭光晚會](../Page/六四.md "wikilink")\[5\]、[七一遊行](../Page/七一遊行.md "wikilink")\[6\]、[七七事變紀念日](../Page/七七事變.md "wikilink")\[7\]\[8\]\[9\]、[八一五光復紀念日](../Page/香港重光.md "wikilink")\[10\]\[11\]\[12\]、[九三炮戰紀念日](../Page/九三炮戰.md "wikilink")\[13\]\[14\]\[15\]、九九受降日（日本在中國投降之紀念日）\[16\]、[九一八事變紀念日](../Page/九一八事變.md "wikilink")（[瀋陽事變](../Page/瀋陽事變.md "wikilink")）\[17\]\[18\]\[19\]\[20\]、[中華民國國慶](../Page/中華民國國慶.md "wikilink")（雙十節）\[21\]\[22\]\[23\]、[蔣公誕辰紀念日](../Page/蔣公誕辰紀念日.md "wikilink")（10月31日）\[24\]\[25\]、[中國國民黨建黨紀念日](../Page/中國國民黨.md "wikilink")（11月24日，[興中會成立日](../Page/興中會.md "wikilink")）\[26\]\[27\]、[南京大屠殺紀念日](../Page/南京大屠殺.md "wikilink")（12月13日）\[28\]\[29\]\[30\]\[31\]及[香港淪陷紀念日](../Page/香港淪陷.md "wikilink")（12月25日，黑色聖誕節）\[32\]\[33\]等之紀念活動。每逢4月5日蔣介石逝世紀念日和10月31日蔣介石壽辰，團體都會在[旺角行人專用區舉辦集會和展覽](../Page/旺角.md "wikilink")，向警方申請[不反對通知書都會表示沒有青天白日旗](../Page/不反對通知書.md "wikilink")，到舉行活動時才展示，警方一般不理會。\[34\]

此外，蔣公中正崇德協會曾主辦各類[中華民國節慶活動](../Page/中華民國節日與歲時列表.md "wikilink")、悼念在[六七暴動中遭](../Page/六七暴動.md "wikilink")[親中共](../Page/親共.md "wikilink")[左派伏擊並燒死的](../Page/左派.md "wikilink")[林彬](../Page/林彬.md "wikilink")\[35\]、拜祭[孫中山母親墓](../Page/百花林#孫中山母親墓.md "wikilink")\[36\]、紀念蔣中正的活動及圖片展覽等活動。協會亦曾多次號召會員、民眾，並結合其他香港親臺灣團體至[日本駐港領事館抗議](../Page/日本駐港領事館.md "wikilink")[日本皇軍於](../Page/日本皇軍.md "wikilink")[八年抗戰期間在](../Page/八年抗戰.md "wikilink")[中國的暴行](../Page/中國.md "wikilink")，參與[保衛釣魚台之活動](../Page/保衛釣魚台.md "wikilink")\[37\]\[38\]\[39\]\[40\]\[41\]，以及舉辦聲援[中華民國的示威遊行](../Page/中華民國.md "wikilink")，同時針對[中國共產黨及](../Page/中國共產黨.md "wikilink")[中華人民共和國政府對](../Page/中華人民共和國政府.md "wikilink")[中華民國政府的壓制](../Page/中華民國政府.md "wikilink")，例如《[反分裂國家法](../Page/反分裂國家法.md "wikilink")》的通過、阻攔中華民國參加[世界衛生組織](../Page/世界衛生組織.md "wikilink")、反對中華民國重返[聯合國等動作表示強烈譴責](../Page/聯合國.md "wikilink")。

## 相關條目

  - [香港親台團體](../Page/香港親台團體.md "wikilink")
  - [日本](../Page/日本.md "wikilink")[蔣公遺德顯彰會](../Page/蔣公遺德顯彰會.md "wikilink")

## 參見資料

## 外部連結

  - [蔣公中正崇德協會活動](http://www.youtube.com/user/jimmyso2001)，[Youtube](../Page/Youtube.md "wikilink")

[分類:紀念蔣中正組織](../Page/分類:紀念蔣中正組織.md "wikilink")

[Category:香港親台團體](../Category/香港親台團體.md "wikilink")
[Category:蒋中正](../Category/蒋中正.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.

9.

10.
11.

12.

13.
14.

15.

16.

17.
18.

19.

20.

21.
22.

23.

24.

25.
26.

27.

28.
29.

30.

31.

32.

33.

34.
35.

36.

37.
38.

39.

40.

41.