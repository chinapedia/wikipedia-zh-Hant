**蔣碩傑**（），[湖北](../Page/湖北.md "wikilink")[應城人](../Page/應城.md "wikilink")，生于[上海](../Page/上海.md "wikilink")，傑出[經濟學](../Page/經濟學.md "wikilink")[專家](../Page/專家.md "wikilink")，[中央研究院院士](../Page/中央研究院院士.md "wikilink")。[蔣作賓之四子](../Page/蔣作賓.md "wikilink")。

將[新古典經濟學派與](../Page/新古典經濟學.md "wikilink")[重貨幣學派的思潮帶進台灣經濟學界](../Page/重貨幣學派.md "wikilink")，曾與[王作榮辯論經濟政策](../Page/王作榮.md "wikilink")，成為著名的[蔣王論戰](../Page/蔣王論戰.md "wikilink")。

## 生平

父[蔣作賓曾任](../Page/蔣作賓.md "wikilink")[國民政府陸軍部次長](../Page/國民政府.md "wikilink")（1913年）、國民政府的第一任駐[德國](../Page/德國.md "wikilink")[公使兼駐](../Page/公使.md "wikilink")[奧地利全權公使](../Page/奧地利.md "wikilink")、駐日公使和首任駐日大使、[內政部部長](../Page/內政部.md "wikilink")(1935年12月12日-1937年11月20日)及安徽省政府主席。

長兄[蔣碩民曾留學](../Page/蔣碩民.md "wikilink")[德國](../Page/德國.md "wikilink")（1927年）取得[哥廷根大學](../Page/哥廷根大學.md "wikilink")[數學博士](../Page/數學.md "wikilink")，回國任[南開大學教授](../Page/南開大學.md "wikilink")。

蒋硕杰1921年開始與兄、姊共同受教於家庭教師[朱子秋](../Page/朱子秋.md "wikilink")（后留日）。1926年他插班進入姨母[張默君創辦的](../Page/張默君.md "wikilink")[神州女學附屬小學四年級](../Page/神州女學.md "wikilink")。1928年轉學南洋中學附屬高小二年級，並於翌年進入[南洋中學](../Page/南洋中學.md "wikilink")。16歲到[日本讀過](../Page/日本.md "wikilink")[慶應大學预科](../Page/慶應大學.md "wikilink")。

21歲到28歲這八年，蒋硕杰先後就讀於英國[倫敦政治經濟學院和](../Page/倫敦政治經濟學院.md "wikilink")[劍橋大學](../Page/劍橋大學.md "wikilink")，1941年获得[伦敦政经学院](../Page/伦敦政经学院.md "wikilink")[经济学](../Page/经济学.md "wikilink")[学士](../Page/学士.md "wikilink")。1943年由经济学大师[哈耶克推薦得到奖学金重回伦敦政经学院的研究所](../Page/哈耶克.md "wikilink")。当年11月份在[经济学刊撰文批驳](../Page/经济学刊.md "wikilink")[凯因斯人口成长与就业关系的文章](../Page/凯因斯.md "wikilink")。1944年又于经济学刊發表批判[尼古拉斯·卡爾多股票投机学说的文章](../Page/尼古拉斯·卡爾多.md "wikilink")。之後撰文批评[剑桥大学资深教授](../Page/剑桥大学.md "wikilink")[庇古所著](../Page/庇古.md "wikilink")《就业与均衡》部份內容之錯誤，庇古认错并修改其书中二章內容。1945年以流动分析（flow
approach）理论探討經濟发展，获得[經濟學](../Page/經濟學.md "wikilink")[博士](../Page/博士.md "wikilink")。\[1\]

在伦敦政经学院就学期间，蒋硕杰曾发表数篇论文刊登在该校主办的学术期刊《[经济学刊](../Page/经济学刊.md "wikilink")》上，更获得象征最佳论文的“赫其森銀牌奖”（Hutchinson
Silver Medal）。

其後，蔣陸續任职于[国立北京大学](../Page/国立北京大学.md "wikilink")、[国际货币基金组织](../Page/国际货币基金组织.md "wikilink")、美国[罗彻斯特大学和](../Page/罗彻斯特大学.md "wikilink")[康奈尔大学](../Page/康奈尔大学.md "wikilink")，回國後出任[台灣經濟研究院首任院長](../Page/台灣經濟研究院.md "wikilink")，之後又擔任首任[中華經濟研究院](../Page/中華經濟研究院.md "wikilink")[院長](../Page/院長.md "wikilink")。

1981年，主張穩定物價優先，與王作榮進行蔣王論戰，是台灣首次在報紙上討論國家經濟方針。

1982年，蔣碩傑曾獲[諾貝爾經濟學獎提名](../Page/諾貝爾經濟學獎.md "wikilink")，是首位獲提名的[华人](../Page/华人.md "wikilink")[經濟學家](../Page/經濟學家.md "wikilink")，但他並未得獎。\[2\]

## 家庭

其女蔣人瑞，芝加哥大學教授，其女婿[拉尔斯·彼得·汉森為](../Page/拉尔斯·彼得·汉森.md "wikilink")2013年[諾貝爾經濟學獎得主](../Page/諾貝爾經濟學獎.md "wikilink")。

## 相關條目

  - [蔣王論戰](../Page/蔣王論戰.md "wikilink")

## 论文著作

<span style="font-size:small">

  - Tsiang, S.C. "The 1951 improvement in the Danish balance of
    payments." IMF Staff Papers, vol. 3, 1953, pp. 155-170
  - Tsiang, S.C. "Liquidity preference and loanable funds theories,
    multiplier and velocity anlysises: a synthesis," American Economic
    Review, vol. 46, 1956, pp.539-64.
  - Tsiang, S.C. "The Theory of Forward Exchange and Effects of
    Government Intervention on the Forward Exchange Market", 1959, IMF
    Staff Papers.
  - Tsiang, S.C. "The precautionary demand for money: an inventory
    theoretical approach", Journal of Political Economy, vol. 76, 1968.
  - Tsiang, S.C. "Taiwan's economic miracle: lessons in economic
    development," in A.C. Harberger, editor, "World Economic Growth;
    Case Studies of Developed and Developing Countries," Institute for
    Contemporary Studies, San Francisco, CA, 1984.</span>

## 参考文献

<div class="references-small">

<references />

</div>

[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")
[Category:中華民國經濟學家](../Category/中華民國經濟學家.md "wikilink")
[Category:康乃爾大學教師](../Category/康乃爾大學教師.md "wikilink")
[Category:倫敦政治經濟學院校友](../Category/倫敦政治經濟學院校友.md "wikilink")
[Category:慶應義塾大學校友](../Category/慶應義塾大學校友.md "wikilink")
[Category:在美國的中華民國人](../Category/在美國的中華民國人.md "wikilink")
[Category:台灣戰後湖北移民](../Category/台灣戰後湖北移民.md "wikilink")
[Category:台灣戰後上海移民](../Category/台灣戰後上海移民.md "wikilink")
[Category:应城人](../Category/应城人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[S碩](../Category/蔣姓.md "wikilink")
[Category:台灣經濟學家](../Category/台灣經濟學家.md "wikilink")

1.
2.