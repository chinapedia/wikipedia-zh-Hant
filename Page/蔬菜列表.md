[Kleinmarkthalle_Frankfurt_Gemüsestand.jpg](https://zh.wikipedia.org/wiki/File:Kleinmarkthalle_Frankfurt_Gemüsestand.jpg "fig:Kleinmarkthalle_Frankfurt_Gemüsestand.jpg")
**蔬菜列表**，列舉所有可以做菜、[烹饪成为](../Page/烹饪.md "wikilink")[食品的](../Page/食品.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")。

## [葉菜類](../Page/葉菜類.md "wikilink")

  - [菠菜](../Page/菠菜.md "wikilink")
  - [香菜](../Page/香菜.md "wikilink")
  - [芹菜](../Page/芹菜.md "wikilink")
  - [蕹菜](../Page/蕹菜.md "wikilink")（空心菜）
  - [茼蒿](../Page/茼蒿.md "wikilink")
  - [茴香](../Page/茴香.md "wikilink")
  - [苋菜](../Page/苋菜.md "wikilink")
  - [莴苣](../Page/莴苣.md "wikilink")（生菜）
      - [油麦菜](../Page/油麦菜.md "wikilink")

### [十字花科类](../Page/十字花科.md "wikilink")

  - [白菜类](../Page/白菜.md "wikilink")
      - [小白菜](../Page/小白菜.md "wikilink")
      - [塌棵菜](../Page/塌棵菜.md "wikilink")
      - [菜心](../Page/菜心.md "wikilink")（菜薹）
      - [紫菜薹](../Page/紫菜薹.md "wikilink")
      - [油菜薹](../Page/油菜薹.md "wikilink")
      - [白菜薹](../Page/白菜薹.md "wikilink")
      - [薹菜](../Page/薹菜.md "wikilink")
      - [大白菜](../Page/大白菜.md "wikilink")
      - [油菜](../Page/油菜.md "wikilink")（南方[油白菜](../Page/油白菜.md "wikilink")、北方[小油菜](../Page/小油菜.md "wikilink")、[上海白菜](../Page/上海白菜.md "wikilink")、[青江菜](../Page/青江菜.md "wikilink")、湯匙菜）
      - [薺菜](../Page/薺菜.md "wikilink")

<!-- end list -->

  - [芥菜类](../Page/芥菜.md "wikilink")
      - [子芥菜](../Page/子芥菜.md "wikilink")
      - [芜菁](../Page/芜菁.md "wikilink")（根用芥菜）
      - [油菜](../Page/油菜.md "wikilink")（芥菜型）

<!-- end list -->

  - [甘蓝类](../Page/甘蓝.md "wikilink")
      - [苤藍](../Page/苤藍.md "wikilink")
      - [西蘭花](../Page/西蘭花.md "wikilink")（綠花椰菜）
      - [花椰菜](../Page/花椰菜.md "wikilink")
      - [卷心菜](../Page/卷心菜.md "wikilink")（甘藍菜）
      - [抱子甘蓝](../Page/抱子甘蓝.md "wikilink")
      - [紫甘蓝](../Page/紫甘蓝.md "wikilink")
      - [羽衣甘蓝](../Page/羽衣甘蓝.md "wikilink")
      - [木立花椰菜](../Page/木立花椰菜.md "wikilink")
      - [白花芥蓝](../Page/白花芥蓝.md "wikilink")
      - [芥蓝](../Page/芥蓝.md "wikilink")

<!-- end list -->

  - [薯芋類](../Page/薯芋.md "wikilink")
      - [番薯](../Page/番薯.md "wikilink")
      - [馬鈴薯](../Page/馬鈴薯.md "wikilink")
      - [山藥](../Page/山藥.md "wikilink")
      - [芋頭](../Page/芋頭.md "wikilink")

<!-- end list -->

  - [水生类](../Page/水生.md "wikilink")
      - [西洋菜](../Page/西洋菜.md "wikilink")
      - [水芹](../Page/水芹.md "wikilink")

### [葱蒜类](../Page/葱蒜类.md "wikilink")

  - [葱](../Page/葱.md "wikilink")
  - [青蔥](../Page/青蔥.md "wikilink")
  - [香葱](../Page/香葱.md "wikilink")
  - [分葱](../Page/分葱.md "wikilink")
  - [胡葱](../Page/胡葱.md "wikilink")
  - [楼子葱](../Page/楼子葱.md "wikilink")
  - [蒜頭](../Page/蒜頭.md "wikilink")
  - [洋葱头](../Page/洋蔥.md "wikilink")
  - [韭菜](../Page/韭菜.md "wikilink")
  - [韭葱](../Page/韭葱.md "wikilink")
  - [薤](../Page/薤.md "wikilink")
  - [大头蒜](../Page/大头蒜.md "wikilink")

## [豆荚类](../Page/豆類.md "wikilink")

  - [莢果](../Page/莢果.md "wikilink")
      - [紅豆](../Page/紅豆.md "wikilink")
      - [豌豆](../Page/豌豆.md "wikilink")
      - [菜豆](../Page/菜豆.md "wikilink")
      - [刀豆](../Page/刀豆.md "wikilink")
      - [蚕豆](../Page/蚕豆.md "wikilink")
      - [四季豆](../Page/四季豆.md "wikilink")
  - [大豆](../Page/大豆.md "wikilink")
  - [豆角](../Page/豆角.md "wikilink")
  - [豇豆](../Page/豇豆.md "wikilink")
  - [扁豆](../Page/扁豆.md "wikilink")
  - [毛豆](../Page/毛豆.md "wikilink")

[:分类:豆科](../Page/:分类:豆科.md "wikilink")

## [水生菜类](../Page/水生菜类.md "wikilink")

  - [莲藕](../Page/莲藕.md "wikilink")
  - [茭白](../Page/茭白.md "wikilink")
  - [荸荠](../Page/荸荠.md "wikilink")
  - [菱角](../Page/菱角.md "wikilink")
  - [莼菜](../Page/莼菜.md "wikilink")
  - [慈菇](../Page/慈菇.md "wikilink")
  - [水芹](../Page/水芹.md "wikilink")
  - [芡实](../Page/芡实.md "wikilink")
  - [蒲菜](../Page/蒲菜.md "wikilink")

## 参见

  - [园艺](../Page/园艺.md "wikilink")
  - [水果列表](../Page/水果列表.md "wikilink")

[蔬菜](../Category/蔬菜.md "wikilink")
[Category:植物列表](../Category/植物列表.md "wikilink")