**蕅益智旭**（），俗姓鍾，字**智旭**，又字**素华**，號**蕅益**\[1\]，又號**西有**，別號**八不道人**、**藕益老人**\[2\]。[南直隸](../Page/南直隸.md "wikilink")[蘇州府](../Page/蘇州府.md "wikilink")[吳縣](../Page/吳縣.md "wikilink")[瀆川](../Page/木瀆.md "wikilink")（[江蘇](../Page/江蘇.md "wikilink")[蘇州](../Page/蘇州.md "wikilink")[木瀆](../Page/木瀆.md "wikilink")）人，[蓮柏椒蕅四師之一](../Page/明末四大高僧.md "wikilink")，被尊為[淨土宗九祖](../Page/淨土宗.md "wikilink")。晚年住於[浙江](../Page/浙江.md "wikilink")[杭州的靈峯](../Page/杭州.md "wikilink")（今安吉境內北天目靈峰寺），因此又被尊稱為**靈峯蕅益**大師。


## 生平

[明天啟二年](../Page/明.md "wikilink")（1622年），因為仰慕[憨山大師](../Page/憨山德清.md "wikilink")，從其弟子雪嶺出家，時二十四歲。他私淑[天台](../Page/天台.md "wikilink")，被認為是[天台宗一位大成就者](../Page/天台宗.md "wikilink")，兼通[禪宗](../Page/禪宗.md "wikilink")、[華嚴宗](../Page/華嚴宗.md "wikilink")、[法相宗](../Page/法相宗.md "wikilink")，晚年行持是[淨宗彌陀信仰](../Page/淨宗.md "wikilink")。

## 思想特色

## 著作

《阿彌陀經要解》《梵網經玄義》《菩薩戒本箋要》《法華經玄義節要》《法華經會義》《楞嚴經玄義》《楞嚴經文句》《[閱藏知津](../Page/閱藏知津.md "wikilink")》《法海觀瀾》《周易禪解》《教觀綱宗》等數十種。今存有21冊《澫益大師全集》。

[File:OuyiZhixu4.jpg|thumb|藕益大师画像](File:OuyiZhixu4.jpg%7Cthumb%7C藕益大师画像)
[File:OuyiZhixu3.jpg|thumb|藕益大师画像](File:OuyiZhixu3.jpg%7Cthumb%7C藕益大师画像)

## 注释

## 外部連結

  - [澫益大師全集](https://web.archive.org/web/20130723070604/http://www.ouyi.mymailer.com.tw/)
  - [聖嚴法師：〈澫益大師的淨土思想〉](http://ccbs.ntu.edu.tw/FULLTEXT/JR-NX012/nx01304.htm)
  - 黃俊傑：〈[如何導引「儒門道脈同歸佛海」？──蕅益智旭對《論語》的解釋](http://huang.cc.ntu.edu.tw/pdf/CCA3810.pdf)〉。
  - [靈峰蕅益大師選定《淨土十要》](http://www.pureland-buddhism.org/%E6%B7%A8%E5%9C%9F%E7%B6%93%E8%AB%96/%E6%B7%A8%E5%9C%9F%E5%8D%81%E8%A6%81/D%E6%B7%A8%E5%9C%9F%E5%8D%81%E8%A6%81%E5%BA%8F.htm)
  - [陈士强：《阅藏知津》要解-佛教导航](http://www.fjdh.cn/wumin/2013/11/163554305325.html)
  - [閱藏知津-線上俄文版(譯者:)](http://www.litres.ru/vasiliy-vasilev/buddizm-ego-dogmaty-istoriya-i-literatura/)

[O澫](../Category/1599年出生.md "wikilink")
[O澫](../Category/1655年逝世.md "wikilink")
[O澫](../Category/明朝僧人.md "wikilink")
[O澫](../Category/淨土宗人物.md "wikilink")
[Category:鍾姓](../Category/鍾姓.md "wikilink")

1.  「蕅」即「藕」之異體
2.  《八不道人傳》：「八不道人，震旦之逸民也。古者有儒、有禪、有律、有教，道人既蹴然不敢；今亦有儒、有禪、有律、有教，道人又艴然不屑；故名八不也。」