[Amanita_muscaria_2.jpg](https://zh.wikipedia.org/wiki/File:Amanita_muscaria_2.jpg "fig:Amanita_muscaria_2.jpg")（學名：*Amanita
muscaria*）可分離出[蕈毒鹼](../Page/蕈毒鹼.md "wikilink")。\]\]
**蕈毒鹼型乙醯膽鹼受器**是一大類固定在[細胞膜上的](../Page/細胞膜.md "wikilink")[乙醯膽鹼受器](../Page/乙醯膽鹼受器.md "wikilink")，其對[蕈毒鹼比對](../Page/蕈毒鹼.md "wikilink")[菸鹼](../Page/菸鹼.md "wikilink")（尼古丁）更為敏感。反過來對菸鹼比較敏感的則稱為[菸鹼型乙醯膽鹼受器](../Page/菸鹼型乙醯膽鹼受器.md "wikilink")。蕈毒鹼與菸鹼兩者皆為[生物鹼](../Page/生物鹼.md "wikilink")(alkaloid)。許多藥品與其他類物質(例如與[東莨菪鹼](../Page/東莨菪鹼.md "wikilink"))常以[促進劑或](../Page/促進劑.md "wikilink")[拮抗劑的角色發生作用](../Page/拮抗劑.md "wikilink")，並且其作用是僅單一針對蕈毒鹼受器或單一針對菸鹼受器，使得這樣的受器分類顯得有用。

## 背景

[乙醯膽鹼受器中](../Page/乙醯膽鹼.md "wikilink")，*蕈毒鹼受器*是一種次分類，廣泛分布於[中樞神經系統與](../Page/中樞神經系統.md "wikilink")[自主神經系統](../Page/自主神經系統.md "wikilink")。自主神經系統在解剖上與[體神經系統相異](../Page/體神經系統.md "wikilink")。

自主神經系統可分成兩個部分：[交感神經系統與](../Page/交感神經系統.md "wikilink")[副交感神經系統](../Page/副交感神經系統.md "wikilink")。

## 生理學

乙醯膽鹼(Acetylcholine,
**ACh**)是一種[神經傳導物質](../Page/神經傳導物質.md "wikilink")，在[腦部與自主神經系統中廣泛存在](../Page/腦.md "wikilink")。

## 蕈毒鹼型受器的型態

## 受器形式的多樣性

### 受器亞型m2

### 受器亞型m3

### 受器亞型m4

### 受器亞型m5

## 相關條目

[蕈毒鹼受器促進劑](../Page/蕈毒鹼受器促進劑.md "wikilink")

## 文獻

1.   [Fulltext (PDF, subscription
    required)](http://openurl.proquest.com/in?service=pq&issn=0036-8075&volume=252&issue=5007&spage=802&pid=ipauto)

2.   [Free Fulltext
    (PDF)](http://www.jbc.org/cgi/pmidlookup?view=long&pmid=8449930)

3.  Messer, Jr, William S.
    [*Acetylcholine*](https://web.archive.org/web/20071014020222/http://www.neurosci.pharm.utoledo.edu/MBC3320/acetylcholine.htm).
    20 Jan 2000. University of Toledo. Accessed 11 September 2005.

4.  Johnson, Gordon E. *PDQ Pharmacology*. 2<sup>nd</sup> edition. BC
    Decker. 2002. ISBN 1550091093

5.  Richelson, Elliot. *Cholinergic Transduction*. *The Fourth
    Generation of Progress*. *The American College of
    Neuropsychopharmacology*. 2000.

6.  [*see* Johnson, 2002.](../Page/#endnote_PDQ.md "wikilink")

7.  [*see* Richelson, 2000.](../Page/#endnote_Richelson.md "wikilink")

8.  *G protein diversity and complexity in G-protein Signaling*. RA
    Fisher. University of Iowa, Lecture Notes, 2004.

9.  [*see* Richelson, 2000.](../Page/#endnote_Richelson.md "wikilink")

10.  [Fulltext (subscription
    required)](http://www.biochemj.org/bj/315/0883/bj3150883.htm)

11. *University of Sydney lecture notes on the M2 receptors*, 2005.

12. *University of Sydney lecture notes on the M3 receptors*, 2005.

13.
[Category:G蛋白偶联受体](../Category/G蛋白偶联受体.md "wikilink")
[Category:神经化学](../Category/神经化学.md "wikilink")
[Category:神經生理學](../Category/神經生理學.md "wikilink")