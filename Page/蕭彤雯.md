**蕭彤雯**（），[台灣知名](../Page/台灣.md "wikilink")[新聞主播](../Page/新聞主播.md "wikilink")。畢業於[國立政治大學](../Page/國立政治大學.md "wikilink")[新聞學系](../Page/新聞學.md "wikilink")。曾經擔任[華視](../Page/華視.md "wikilink")[記者](../Page/記者.md "wikilink")、[主播和](../Page/主播.md "wikilink")[主持人](../Page/主持人.md "wikilink")。2004年跳槽至[三立新聞台擔任專任](../Page/三立新聞台.md "wikilink")[主播](../Page/主播.md "wikilink")。2010年轉至[壹電視擔任專職](../Page/壹電視.md "wikilink")[主播](../Page/主播.md "wikilink")。與前夫林谷苑育有一女。蕭父台大外文系畢業，曾任基隆中學英語老師，蕭母為家管。現任丈夫周治轅，兩人為差六歲的姊弟戀。

## 學歷

  - [臺北市立第一女子高級中學](../Page/臺北市立第一女子高級中學.md "wikilink")
  - [國立政治大學新聞系](../Page/國立政治大學.md "wikilink")

## 經歷

  - [飛碟電台](../Page/飛碟電台.md "wikilink")[記者](../Page/記者.md "wikilink")（1996年～1999年）
  - [TVBS記者](../Page/TVBS.md "wikilink")（1999年）
  - [華視新聞記者](../Page/華視新聞.md "wikilink")（醫藥、交通）、[氣象](../Page/氣象.md "wikilink")[主播](../Page/主播.md "wikilink")、[新聞主播](../Page/新聞主播.md "wikilink")、主持人（1999年8月～2004年12月）
  - [三立新聞台](../Page/三立新聞台.md "wikilink")[主播](../Page/主播.md "wikilink")、主持人、製作人（2004年12月～2009年12月）
  - [壹電視新聞台](../Page/壹電視新聞台.md "wikilink")[主播](../Page/主播.md "wikilink")（2010年1月1日～2016年4月20日）
  - [中天新聞台](../Page/中天新聞台.md "wikilink")《[新聞大審判](../Page/新聞大審判.md "wikilink")》主持人（2016年5月8日至2016年10月16日）
  - [安麗網路電視](../Page/安麗.md "wikilink")《Be a better
    you》主持人（2016年7月16日至2017年12月）
  - [Ettoday](../Page/Ettoday.md "wikilink")《史瑪特過生活》節目主持人（2017年9月至今）
  - [飛碟電台](../Page/飛碟電台.md "wikilink")《生活同樂會》節目主持人（2019年1月7日至今～）

## 獲獎

  - 2003年：第二屆卓越新聞獎：電視類即時新聞採訪獎

## 曾主播過或主持過的節目

|                                                   |                                                 |                                          |        |
| ------------------------------------------------- | ----------------------------------------------- | ---------------------------------------- | ------ |
| **時間**                                            | **頻道**                                          | **節目**                                   | **備註** |
| |rowspan=6|[華視](../Page/中華電視台.md "wikilink")      | 《華視晨間新聞》                                        |                                          |        |
| |《華視夜間新聞》                                         |                                                 |                                          |        |
| |《華視晚間新聞》                                         | 氣象主播                                            |                                          |        |
| |《台灣NO.1》                                         |                                                 |                                          |        |
| | 《莒光園地》                                          |                                                 |                                          |        |
| 2003年5月至2003年7月\[1\]                              | 《魔法梅蒂亞》（[媒體識讀教育節目](../Page/媒體識讀.md "wikilink")） |                                          |        |
| |[三立新聞台](../Page/三立新聞台.md "wikilink")             | 《1000、1100 三立整點新聞》（又名《三立新聞網》）                   |                                          |        |
| |rowspan=3|[壹電視新聞台](../Page/壹電視新聞台.md "wikilink") | 《午間新聞》                                          |                                          |        |
| |《晚間新聞》                                           |                                                 |                                          |        |
| 2014年6月2日14:00                                    | 《2014台北市長在野整合政見發表會》                             |                                          |        |
| 2016年5月8日至2016年10月16日                             | [中天新聞台](../Page/中天新聞台.md "wikilink")            | 《[新聞大審判](../Page/新聞大審判.md "wikilink")》   | 主持人    |
| 2017年9月至今                                         | [Ettoday](../Page/Ettoday.md "wikilink")        | 《[史瑪特過生活](../Page/史瑪特過生活.md "wikilink")》 | 主持人    |
| 2019年1月7日至今                                       | [飛碟電台](../Page/飛碟電台.md "wikilink")              | 《生活同樂會》                                  | 主持人    |

## 注釋

<div style="font-size:small">

<references />

</div>

[T彤](../Category/蕭姓.md "wikilink")
[H蕭](../Category/臺北市立第一女子高級中學校友.md "wikilink")
[H蕭](../Category/國立政治大學校友.md "wikilink")
[H蕭](../Category/台灣記者.md "wikilink")
[H蕭](../Category/台灣電視主播.md "wikilink")
[H蕭](../Category/台灣新聞節目主持人.md "wikilink")
[H蕭](../Category/華視主播.md "wikilink")
[H蕭](../Category/三立主播.md "wikilink")
[H蕭](../Category/壹電視主播.md "wikilink")

1.  [中華民國電視學會無線電視年鑑編纂委員](../Page/中華民國電視學會.md "wikilink")
    編纂，《[中華民國無線電視年鑑第十三輯](../Page/中華民國無線電視年鑑.md "wikilink")：民國九十一年至九十二年》，中華民國電視學會2004年7月30日-{出}-版，第50頁。