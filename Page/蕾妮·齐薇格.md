**蕾妮·凱絲琳·齐薇格**（，），[美国](../Page/美国.md "wikilink")[女演员](../Page/女演员.md "wikilink")，生于[得克萨斯州](../Page/得克萨斯州.md "wikilink")[休斯顿郊区的卡地镇](../Page/休斯顿.md "wikilink")。她獲得多個讚賞及獎項，包括[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")、[英國電影學院獎](../Page/英國電影學院獎.md "wikilink")、三個[金球獎和三個](../Page/金球獎.md "wikilink")[美國演員工會獎](../Page/美國演員工會獎.md "wikilink")，其中更獲得[第76届奥斯卡金像奖](../Page/第76届奥斯卡金像奖.md "wikilink")[最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")。至2007年起，她是其中一名最高身價的[荷里活女演員](../Page/荷里活.md "wikilink")。\[1\]

## 简介

蕾妮的父母都是[移民](../Page/移民.md "wikilink")，父亲是[瑞士人](../Page/瑞士.md "wikilink")，母亲是[挪威人](../Page/挪威.md "wikilink")。她1987年毕业于Katy高中，曾当过[啦啦队队员](../Page/啦啦队.md "wikilink")，[体操运动员](../Page/体操.md "wikilink")，也加入过[戏剧社團](../Page/戏剧.md "wikilink")。她進入[德州大學奥斯汀分校英語系学习](../Page/德州大學奥斯汀分校.md "wikilink")，选修了一门戏剧课，但唯一原因是为了完成[美术学分](../Page/美术.md "wikilink")。她平常靠在酒吧、饭店中打零工维持生计。

1991年從[德州大學奧斯汀分校毕业后](../Page/德州大學奧斯汀分校.md "wikilink")，她走上了演艺道路，积极为[德州商业广告和小电影试镜](../Page/德州.md "wikilink")。

1994年她凭借在《》（*Texas Chainsaw Massacre: The Next
Generation*）中的表演一举成名，其后又主演了《》（*Love and a
.45*）。自此之后她的影视事业一帆风顺，参与演出了三部《[BJ单身日记](../Page/BJ单身日记.md "wikilink")》、《[芝加哥](../Page/芝加哥_\(電影\).md "wikilink")》与《[冷山](../Page/冷山.md "wikilink")》等一系列广受好评的电影。

## 影视作品

### 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>譯名</p></th>
<th><p>原名</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1993</p></td>
<td><p>《》</p></td>
<td><p><em>My Boyfriend's Back</em></p></td>
<td></td>
<td><p>未掛名</p></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><em></em></p></td>
<td><p>Girl in blue pickup truck</p></td>
<td><p>未掛名</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p>《<a href="../Page/四個畢業生.md" title="wikilink">四個畢業生</a>》</p></td>
<td><p><em>Reality Bites</em></p></td>
<td><p>Tami</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>8 Seconds</em></p></td>
<td><p>Prescott Buckle Bunny</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Love and a .45</em></p></td>
<td><p>史塔琳<br />
Starlene Cheatham</p></td>
<td><p>提名 — <a href="../Page/獨立精神獎.md" title="wikilink">獨立精神獎最佳首次表演</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Texas Chainsaw Massacre: The Next Generation</em></p></td>
<td><p>珍妮<br />
Jenny</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p>《》</p></td>
<td><p><em>Empire Records</em></p></td>
<td><p>吉娜<br />
Gina</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><em></em></p></td>
<td><p>詩人poet</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
<td><p>《》</p></td>
<td><p><em>The Whole Wide World</em></p></td>
<td><p><br />
Novalyne Price</p></td>
<td><p>提名 — <a href="../Page/獨立精神獎.md" title="wikilink">獨立精神獎最佳女主角</a></p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/征服情海.md" title="wikilink">征服情海</a>》</p></td>
<td><p><em>Jerry Maguire</em></p></td>
<td><p>桃樂絲·鮑伊德<br />
Dorothy Boyd</p></td>
<td><p><a href="../Page/廣播影評人協會.md" title="wikilink">廣播影評人協會突破演員獎</a><br />
<a href="../Page/國家評論協會.md" title="wikilink">國家評論協會突破表演獎</a><br />
提名 — <a href="../Page/:en:Satellite_Award_for_Best_Supporting_Actress_-_Motion_Picture.md" title="wikilink">衛星獎最佳電影女配角</a><br />
提名 - <a href="../Page/影視演員協會獎.md" title="wikilink">影視演員協會最佳電影女配角獎</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p>《》</p></td>
<td><p><em>Deceiver</em></p></td>
<td><p>伊麗莎白<br />
Elizabeth</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p>《》</p></td>
<td><p><em>A Price Above Rubies</em></p></td>
<td><p>Sonia Horowitz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>One True Thing</em></p></td>
<td><p>愛倫·古爾登<br />
Ellen Gulden</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p>《》</p></td>
<td><p><em>The Bachelor</em></p></td>
<td><p>安妮·亞登<br />
Anne Arden</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p>《》</p></td>
<td><p><em>Nurse Betty</em></p></td>
<td><p>貝蒂·賽茲摩爾<br />
Betty Sizemore</p></td>
<td><p><a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球獎最佳女主角：音樂及喜劇電影</a><br />
<a href="../Page/:en:Satellite_Award_for_Best_Actress_-_Motion_Picture_Musical_or_Comedy.md" title="wikilink">衛星獎最佳音樂或喜劇電影女主角</a><br />
提名 — <a href="../Page/:en:American_Comedy_Award.md" title="wikilink">美國喜劇獎最惹笑電影女主角</a><br />
提名 — <a href="../Page/:en:London_Film_Critics_Circle_Award_for_Best_Actress.md" title="wikilink">倫敦影評人圈最佳女主角獎</a></p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Me, Myself &amp; Irene</em></p></td>
<td><p>艾琳·P·華特斯<br />
Irene P. Waters</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p>《<a href="../Page/BJ單身日記_(電影).md" title="wikilink">BJ單身日記</a>》</p></td>
<td><p><em>Bridget Jones's Diary</em></p></td>
<td><p><br />
Bridget Jones</p></td>
<td><p>提名 - <a href="../Page/奧斯卡最佳女主角獎.md" title="wikilink">奧斯卡最佳女主角獎</a><br />
提名 - <a href="../Page/英國電影學院獎最佳女主角.md" title="wikilink">英國電影學院獎最佳女主角</a><br />
提名 — <a href="../Page/廣播影評人協會.md" title="wikilink">廣播影評人協會獎最佳女主角</a><br />
提名 — 帝國電影大獎最佳女主角<br />
提名 - <a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球獎最佳女主角：音樂及喜劇電影</a><br />
提名 — <a href="../Page/:en:Satellite_Award_for_Best_Actress_-_Motion_Picture_Musical_or_Comedy.md" title="wikilink">衛星獎最佳音樂或喜劇電影女主角</a><br />
提名- <a href="../Page/美國演員工會獎最佳女主角.md" title="wikilink">美國演員工會獎最佳女主角</a></p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p>《》</p></td>
<td><p><em>White Oleander</em></p></td>
<td><p>克萊兒·理查茲<br />
Claire Richards</p></td>
<td><p>提名 — <a href="../Page/:en:Satellite_Award_for_Best_Supporting_Actress_-_Motion_Picture.md" title="wikilink">衛星獎最佳電影女配角</a></p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/芝加哥_(電影).md" title="wikilink">芝加哥</a>》</p></td>
<td><p><em>Chicago</em></p></td>
<td><p><a href="../Page/蘿西·哈特.md" title="wikilink">蘿西·哈特</a><br />
Roxie Hart</p></td>
<td><p><a href="../Page/廣播影評人協會.md" title="wikilink">廣播影評人協會獎最佳集體演出</a><br />
<a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球獎最佳女主角：音樂及喜劇電影</a><br />
<a href="../Page/美國演員工會獎最佳女主角.md" title="wikilink">美國演員工會獎最佳女主角</a><br />
<a href="../Page/美國演員工會獎.md" title="wikilink">美國演員工會獎最佳集體演出</a><br />
提名 - <a href="../Page/奧斯卡最佳女主角.md" title="wikilink">奧斯卡最佳女主角</a><br />
提名- <a href="../Page/英國電影學院獎最佳女主角.md" title="wikilink">英國電影學院獎最佳女主角</a><br />
提名 — <a href="../Page/芝加哥影評人協會.md" title="wikilink">芝加哥影評人協會獎最佳女主角</a><br />
提名 — <a href="../Page/鳳凰城影評人協會.md" title="wikilink">鳳凰城影評人協會獎最佳女主角</a><br />
提名 — <a href="../Page/鳳凰城影評人協會.md" title="wikilink">鳳凰城影評人協會獎最佳集體演出</a><br />
提名 — <a href="../Page/:en:Satellite_Award_for_Best_Actress_-_Motion_Picture_Musical_or_Comedy.md" title="wikilink">衛星獎最佳音樂或喜劇電影女主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p>《》</p></td>
<td><p><em>Down with Love</em></p></td>
<td><p>芭芭拉·諾華<br />
Barbara Novak</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/冷山.md" title="wikilink">冷山</a>》</p></td>
<td><p><em>Cold Mountain</em></p></td>
<td><p>露比·薩威斯<br />
Ruby Thewes</p></td>
<td><p><a href="../Page/奧斯卡最佳女配角獎.md" title="wikilink">奧斯卡最佳女配角獎</a><br />
<a href="../Page/英國電影學院獎最佳女配角.md" title="wikilink">英國電影學院獎最佳女配角</a><br />
<a href="../Page/廣播影評人協會.md" title="wikilink">廣播影評人協會獎最佳女配角</a><br />
<a href="../Page/芝加哥影評人協會獎最佳女配角.md" title="wikilink">芝加哥影評人協會獎最佳女配角</a><br />
<a href="../Page/達拉斯—沃斯堡影評人協會.md" title="wikilink">達拉斯—沃斯堡影評人協會獎最佳女配角</a><br />
<a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球獎最佳電影女配角</a><br />
<a href="../Page/聖地牙哥影評人協會.md" title="wikilink">聖地牙哥影評人協會獎最佳女配角</a><br />
<a href="../Page/美國演員工會獎最佳女配角.md" title="wikilink">美國演員工會獎最佳女配角</a><br />
<a href="../Page/東南影評人協會.md" title="wikilink">東南影評人協會獎最佳女配角</a><br />
提名 — Online影評人協會獎最佳女配角獎<br />
提名 — <a href="../Page/鳳凰城影評人協會.md" title="wikilink">鳳凰城影評人協會獎最佳女配角</a><br />
提名 — <a href="../Page/華盛頓特區影評人協會.md" title="wikilink">華盛頓特區影評人協會獎最佳女配角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p>《<a href="../Page/鯊魚黑幫.md" title="wikilink">鯊魚黑幫</a>》</p></td>
<td><p><em>Shark Tale</em></p></td>
<td><p>Angie</p></td>
<td><p>配音</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/BJ單身日記：男人禍水_(電影).md" title="wikilink">BJ單身日記：男人禍水</a>》</p></td>
<td><p><em>Bridget Jones: The Edge of Reason</em></p></td>
<td><p>布麗琪·瓊斯</p></td>
<td><p>提名 - <a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球獎最佳女主角：音樂及喜劇電影</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/擊動深情.md" title="wikilink">擊動深情</a>》</p></td>
<td><p><em>Cinderella Man</em></p></td>
<td><p>Mae Braddock</p></td>
<td><p>提名 — Empire Award for Best Actress</p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>《》</p></td>
<td><p><em>Miss Potter</em></p></td>
<td><p><a href="../Page/碧雅翠絲·波特.md" title="wikilink">碧雅翠絲·波特</a></p></td>
<td><p>提名 - <a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球獎最佳女主角</a>：音樂及喜劇電影<br />
提名 — <a href="../Page/:en:Saturn_Award_for_Best_Actress.md" title="wikilink">土星獎最佳女主角</a></p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>《<a href="../Page/蜜蜂電影.md" title="wikilink">蜜蜂電影</a>》</p></td>
<td><p><em>Bee Movie</em></p></td>
<td><p>凡妮莎·布魯姆<br />
Vanessa Bloom</p></td>
<td><p>配音</p></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p>《<a href="../Page/愛情決勝分.md" title="wikilink">愛情決勝分</a>》</p></td>
<td><p><em>Leatherheads</em></p></td>
<td><p>Lexi Littleton</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><em></em></p></td>
<td><p>Allie French</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>《》</p></td>
<td><p><em>New in Town</em></p></td>
<td><p>露西·希爾<br />
Lucy Hill</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/怪獸大戰外星人.md" title="wikilink">怪獸大戰外星人</a>》</p></td>
<td><p><em>Monsters vs. Aliens</em></p></td>
<td><p>凱蒂<br />
Katie</p></td>
<td><p>客串配音</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>My One and Only</em></p></td>
<td><p>安妮·戴維羅<br />
Anne Deveraux</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Case 39</em></p></td>
<td><p>愛蜜莉·詹金斯<br />
Emily Jenkins</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p>《》</p></td>
<td><p><em>My Own Love Song</em></p></td>
<td><p>Jane</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p>《》</p></td>
<td><p><em>The Whole Truth</em></p></td>
<td><p>Loretta</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/BJ有喜.md" title="wikilink">BJ有喜</a>》</p></td>
<td><p><em>Bridget Jones's Baby</em></p></td>
<td><p>Bridget Jones</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p>《》</p></td>
<td><p><em>Same Kind of Different as Me</em></p></td>
<td><p>Deborah Hall</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>譯名</p></th>
<th><p>原名</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1992</p></td>
<td><p>《》</p></td>
<td><p><em>A Taste for Killing</em></p></td>
<td><p>Mary Lou</p></td>
<td><p><a href="../Page/電視電影.md" title="wikilink">電視電影</a></p></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td><p>Barbara Von Busch</p></td>
<td><p>迷你劇<br />
未掛名</p></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p>《》</p></td>
<td><p><em>Shake, Rattle and Rock!</em></p></td>
<td><p>蘇珊·道爾<br />
Susan Doyle</p></td>
<td><p>電視電影</p></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p>《》</p></td>
<td><p><em>King of the Hill</em></p></td>
<td><p>Tammy Duvall（配音）</p></td>
<td><p>Episode: "Ho, Yeah!"</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p>《》</p></td>
<td><p><em>Living Proof</em></p></td>
<td></td>
<td><p>執行製作人</p></td>
</tr>
</tbody>
</table>

## 獲獎紀錄

<table>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>影片</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球奖喜剧/音乐剧电影最佳女主角</a></p></td>
<td><p>《<a href="../Page/护士贝蒂.md" title="wikilink">护士贝蒂</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奧斯卡最佳女主角獎.md" title="wikilink">奧斯卡最佳女主角獎</a></p></td>
<td><p>《<a href="../Page/BJ單身日記_(電影).md" title="wikilink">BJ单身日记</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球奖喜剧/音乐剧电影最佳女主角獎</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英国电影电视艺术学院.md" title="wikilink">英国电影电视艺术学院奖</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球奖喜剧/音乐剧电影最佳女主角獎</a></p></td>
<td><p>《<a href="../Page/芝加哥_(电影).md" title="wikilink">芝加哥</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奧斯卡最佳女主角獎.md" title="wikilink">奧斯卡最佳女主角獎</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英国电影电视艺术学院.md" title="wikilink">英国电影电视艺术学院奖</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奧斯卡最佳女配角獎.md" title="wikilink">奧斯卡最佳女配角獎</a></p></td>
<td><p>《<a href="../Page/冷山.md" title="wikilink">冷山</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/影視演員協會奖.md" title="wikilink">影視演員協會獎最佳女配角獎</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球奖剧情类电影最佳女配角獎</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考文獻

## 外部連結

  -
[Category:奧斯卡最佳女配角獎獲獎演員](../Category/奧斯卡最佳女配角獎獲獎演員.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:德州大學奧斯汀分校校友](../Category/德州大學奧斯汀分校校友.md "wikilink")
[Category:挪威裔美國人](../Category/挪威裔美國人.md "wikilink")
[Category:瑞士裔美國人](../Category/瑞士裔美國人.md "wikilink")
[Category:德克薩斯州人](../Category/德克薩斯州人.md "wikilink")
[Category:美国演员工会奖最佳电影群体演出奖得主](../Category/美国演员工会奖最佳电影群体演出奖得主.md "wikilink")
[Category:瑞典裔美國人](../Category/瑞典裔美國人.md "wikilink")
[Category:金球獎最佳音樂及喜劇類電影女主角獲獎演員](../Category/金球獎最佳音樂及喜劇類電影女主角獲獎演員.md "wikilink")
[Category:英國電影學院獎最佳女配角獲得者](../Category/英國電影學院獎最佳女配角獲得者.md "wikilink")
[Category:金球獎最佳電影女配角獲得者](../Category/金球獎最佳電影女配角獲得者.md "wikilink")
[Category:美國女配音演員](../Category/美國女配音演員.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")

1.