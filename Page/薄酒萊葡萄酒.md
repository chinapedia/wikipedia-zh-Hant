[Louis_Jadot_Cru_Beaujolais_in_glass.jpg](https://zh.wikipedia.org/wiki/File:Louis_Jadot_Cru_Beaujolais_in_glass.jpg "fig:Louis_Jadot_Cru_Beaujolais_in_glass.jpg")
[Cote_de_Brouilly_bottle_of_Beaujolais_wine.jpg](https://zh.wikipedia.org/wiki/File:Cote_de_Brouilly_bottle_of_Beaujolais_wine.jpg "fig:Cote_de_Brouilly_bottle_of_Beaujolais_wine.jpg")
**薄酒萊葡萄酒**（[法文](../Page/法文.md "wikilink")：，或常簡稱），是生產於[法國](../Page/法國.md "wikilink")[勃艮第南部](../Page/勃艮第.md "wikilink")[博若莱](../Page/博若莱.md "wikilink")（Beaujolais）地區之[葡萄酒](../Page/葡萄酒.md "wikilink")。在多種薄酒萊葡萄酒之中，一種被稱為**薄酒萊新酒**（，但在法國以外地區較常使用Beaujolais
Nouveau的名稱）的酒種，擁有壓倒性的產量與銷售量及強勢的行銷，因此常常被誤認為是唯一一種薄酒萊葡萄酒。

## 历史

博若莱葡萄酒是世界上最广泛饮用的红葡萄酒之一。该酒用[嘉美葡萄制成](../Page/嘉美葡萄.md "wikilink")，色泽中红，醇度较低，有清凉果味。该地区南部所产葡萄酒简单地称为博若莱葡萄酒，北部某些地方所产葡萄酒称作博若莱村庄葡萄酒；通常色泽较深，醇度较高，令人们认为这种酒质量较高。北部有10个村庄生产最佳的博若莱葡萄酒，其中大名鼎鼎的有：“慕玲纳凡”和“弗勒利”。在20世紀后半叶博若莱葡萄酒的声誉迅速提高。在葡萄收获后不久饮用博若莱葡萄酒成为一种时尚，这种新酿造出来的酒叫新博若莱葡萄酒。到1990年代初期，所产的博若莱葡萄酒有一半以上是作为新酒而饮用的。博若莱葡萄酒，特别是新博若莱葡萄酒，常常在冷却后供饮用。

## 特色

薄酒萊葡萄酒特色在於它是法國境內唯一一個使用[嘉美葡萄](../Page/嘉美葡萄.md "wikilink")（，全名應稱為，白汁黑嘉美葡萄）為原料製造葡萄酒的產區，使用特殊的[二氧化碳浸泡法](../Page/二氧化碳浸泡法.md "wikilink")（，部分酒區使用改良的半二氧化碳浸泡）發酵，並且不經[橡木桶陳年或只短暫陳年之後就裝瓶發售](../Page/橡木桶.md "wikilink")。基本上，使用這種作法的葡萄酒[單寧含量少](../Page/單寧.md "wikilink")，口味上較為清新、果香重，常常被形容帶有[梨子糖](../Page/梨子糖.md "wikilink")（Pear
Drop）的味道，但缺點是對於習慣正統陳年葡萄酒口味的人來說，太過年輕的薄酒萊葡萄酒欠缺人們對於葡萄酒所期待的標準口感。

## 產區

在薄酒萊地區生產的葡萄酒，分別屬於十二個[法定產區](../Page/法定產區.md "wikilink")（），其中薄酒萊區、薄酒萊新酒區（）與薄酒萊村莊區（）有不小面積的地區是互相重疊的，但薄酒萊地區真正最受推崇的葡萄酒，卻是產自由其他十個社區性產區共同合稱的薄酒萊酒莊區（）。

  - 地區性（）法定產區
      - 薄酒萊區（）
      - 薄酒萊新酒區（，或又稱為）
      - 薄酒萊村莊區（）
  - 社區性（）法定產區
      - 布胡伊區（）
      - 謝納區（）
      - 希胡布勒區（）
      - 布胡伊邊緣區（）
      - 富勒希區（）
      - 朱麗耶那區（）
      - 摩恭區（）
      - 風車區（）
      - 黑尼耶區（）
      - 聖塔姆赫區（）

## 薄酒萊新酒

雖然只是[勃艮第境南一個產區](../Page/勃艮第.md "wikilink")，薄酒萊葡萄酒每年產量，足足有勃艮第其餘地區年產量兩倍半。除此之外，因大部分薄酒萊葡萄酒都是屬於未經過橡木桶封陳、單寧度低的所謂「新酒」產品，製造出來後不能久放，因此非常強調「當年產酒，當年飲用」。配合這樣特性，從1970年代起，逐漸出現一種薄酒萊葡萄酒慶典──每年11月第三個星期四，將當年9月11日之後下桶開始釀造、並在10月初製作完成的葡萄酒桶打開，開始暢飲。近年薄酒萊新酒透過行銷推廣到全世界，但仍然大致遵循此傳統，規定已經裝瓶運抵銷售地的產品，禁止在這日期之前提早上市，而變成一種話題。

一般來說薄酒萊新酒的保存期限很短，依照保存環境的差異，頂多只能存放到隔年初就該喝光。薄酒萊村莊區產出的新酒因為製造時發酵時間稍長，因此可以多存放更久一些，至於薄酒萊酒莊區的產品則依照不同的製作方式，有部份的酒款可以像一般葡萄酒一般進行長時間陳年存放。

## 參考文獻

  - Tom Stevenson, The New Sotheby's Wine Encyclopedia (3rd Edition,
    2001), Dorking Kindersley Ltd., London, UK, ISBN 0-7894-8039-5

## 参考

## 外部連結

  - [法國薄酒萊地區官方網站（法文，英文，德文版）](http://www.beaujolais.com/)
  - [法国葡萄酒年份表](http://www.classic-wine.co.uk/pages/vinatge_france.htm)-包括博若莱
  - [博若莱](http://www.intowine.com/beaujolais.html)-介绍
  - [博若莱美食和葡萄酒配对](http://www.foodandwine.com/slideshows/beaujolais-pairings)
  - [葡萄酒地区地图](http://www.wineweb.com/map_beaujolais.html)

[B](../Category/法國葡萄酒.md "wikilink")