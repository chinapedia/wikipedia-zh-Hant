**薇拉·凯瑟**（，\[1\]），[美國作家](../Page/美國.md "wikilink")，以"One of
Ours"一書，於1923年得到[普立茲獎](../Page/普立茲獎.md "wikilink")，作品以擅長描寫女性及美國早期移民的拓荒開墾生活而聞名（著作如《哦·拓荒者！》及《我的安東妮亞》)，為美國重要的鄉土作者之一。她在[内布拉斯加州长大](../Page/内布拉斯加州.md "wikilink")，毕业于内布拉斯加大学。在[匹兹堡生活和工作了十年](../Page/匹兹堡.md "wikilink")，然后在她33岁时搬到了[纽约](../Page/纽约.md "wikilink")，在那里度过了余生。

## 生平

[Willa_Cather_house_from_NE_3.JPG](https://zh.wikipedia.org/wiki/File:Willa_Cather_house_from_NE_3.JPG "fig:Willa_Cather_house_from_NE_3.JPG")
薇拉·凯瑟生於[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[溫徹斯特](../Page/溫徹斯特_\(維吉尼亞州\).md "wikilink")，9岁时隨家人移居[内布拉斯加州](../Page/内布拉斯加州.md "wikilink")，薇拉有六个弟妹：Roscoe，Douglass，Jessica，James，John和Elsie。\[2\]根据薇拉的传记作者Hermione
Lee描述，薇拉与弟弟关系很好，与妹妹关系不十分亲密。\[3\]在女性長輩的財務支持下，她毕业于内布拉斯加大学，成為當時少數擁有大學學歷的女性之一，她熱愛寫作，學生時期常在Nebraska
State Journal發表文章\[4\]，大學畢業後當過高中教師及 McClure's 雜誌社總編輯。

她於1906年由内布拉斯加遷至紐約，任職於 McClure's
雜誌社，1908年升為總編輯，於業餘寫作，並擁有多位文壇上的好友。她的第一本著作為
"Alexander's Bridge"
，文風深受[亨利·詹姆斯影響](../Page/亨利·詹姆斯.md "wikilink")，後認識作家Sarah
Orne Jewett，建議她改變寫作方向，多描述自己的故鄉内布拉斯加，她因而回到故鄉尋找寫作靈感，而那些著作(My Antonia 及 O\!
Pioneers\!)使她深受文藝評論及讀者歡迎。

## 个人生活

19世纪90年代初薇拉·凯瑟作为一名学生在内布拉斯加大学时，有着男性化的绰号“威廉”，喜欢穿着男性化的服装。\[5\]内布拉斯加大学的档案照片中显示凯瑟穿得像一个年轻的男子。\[6\]

凯瑟終生未婚，因與數位女性過從甚密\[7\]，且在紐約工作時與女性同事共居一屋\[8\]，故曾被質疑為[女同性戀者](../Page/女同性戀.md "wikilink")，但她否認。由於平日來往的書信已在死前被她焚毀，雖有少數保存在美國大學圖書館的書信留傳下來，但她在遺書中言明不許對外公開生平書信內容，因而學界至今遲遲還未能證明她的真實性向。

1947年4月24日凯瑟在纽约由于脑出血而死亡，被埋葬在[新罕布什尔州Jaffrey的一处墓地内](../Page/新罕布什尔州.md "wikilink")。\[9\]\[10\]

## 书籍

### 非小说类

  - Willa Cather 和 Georgine Milmine, *The Life of Mary Baker G. Eddy and
    the History of Christian Science* (1909, 1993)
  - *Not Under Forty* (1936 )
  - *On Writing* (1949, 1988, ISBN 978-0-8032-6332-1 )

### 小说

  - *[Alexander's Bridge](../Page/Alexander's_Bridge.md "wikilink")*
    (1912)
  - *[O Pioneers\!](../Page/O_Pioneers!.md "wikilink")* (1913)
  - *[The Song of the Lark](../Page/The_Song_of_the_Lark.md "wikilink")*
    (1915)
  - *[My Ántonia](../Page/My_Ántonia.md "wikilink")* (1918)
  - *[One of Ours](../Page/One_of_Ours.md "wikilink")* (1922)
  - *[A Lost Lady](../Page/A_Lost_Lady.md "wikilink")* (1923)
  - *[The Professor's
    House](../Page/The_Professor's_House.md "wikilink")* (1925)
  - *[My Mortal Enemy](../Page/My_Mortal_Enemy.md "wikilink")* (1926)
  - *[Death Comes for the
    Archbishop](../Page/Death_Comes_for_the_Archbishop.md "wikilink")*
    (1927)
  - *[Shadows on the Rock](../Page/Shadows_on_the_Rock.md "wikilink")*
    (1931)
  - *[Lucy Gayheart](../Page/Lucy_Gayheart.md "wikilink")* (1935)
  - *[Sapphira and the Slave
    Girl](../Page/Sapphira_and_the_Slave_Girl.md "wikilink")* (1940)

## 参考资料

## 外部链接

  - [薇拉·凯瑟](http://cather.unl.edu)在内布拉斯加 - 林肯大学

  - [薇拉·凯瑟作品](http://www.archive.org/search.php?query=mediatype%3A\(texts\)%20-contributor%3Agutenberg%20AND%20\(subject%3A%22Cather%2C%20Willa%2C%201873-1947%22%20OR%20creator%3A%22Cather%2C%20Willa%2C%201873-1947%22%20OR%20creator%3A%22Willa%20Cather%22%20OR%20title%3A%22Willa%20Cather%22%20OR%20description%3A%22Willa%20Cather%22\))在[互联网档案馆](../Page/互联网档案馆.md "wikilink")
    （扫描的书籍原版彩色图版）

  - （纯文本，HTML和音频）

  -
<!-- end list -->

  - [薇拉·凯瑟论文](http://www.nebraskahistory.org/lib-arch/research/manuscripts/family/cather.htm)在内布拉斯加州历史学会

  - [薇拉·凯瑟](http://theotherpages.org/poems/poem-cd.html#cather)在[诗人角](../Page/诗人角.md "wikilink")

  - [薇拉·凯瑟](http://www.americanwriters.org/writers/cather.asp)在[C-SPAN的](../Page/C-SPAN.md "wikilink")*[American
    Writers: A Journey Through
    History](../Page/American_Writers:_A_Journey_Through_History.md "wikilink")*

  -
  -
[Category:普立茲小說獎得主](../Category/普立茲小說獎得主.md "wikilink")
[Category:美國女性作家](../Category/美國女性作家.md "wikilink")
[Category:美國詩人](../Category/美國詩人.md "wikilink")
[Category:美國小說家](../Category/美國小說家.md "wikilink")
[Category:女性小說家](../Category/女性小說家.md "wikilink")
[Category:女性詩人](../Category/女性詩人.md "wikilink")
[Category:女同性戀作家](../Category/女同性戀作家.md "wikilink")
[Category:美國LGBT人物](../Category/美國LGBT人物.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:維吉尼亞州人](../Category/維吉尼亞州人.md "wikilink")
[Category:曼哈顿人](../Category/曼哈顿人.md "wikilink")

1.  Woodress, James Leslie. *Willa Cather: A Literary Life*, Omaha, NE:
    University of Nebraska Press, 1987, p. 516. Cather's birth date is
    confirmed by a birth certificate and a January 22, 1874 letter of
    her father's referring to her. While working at *[McClure's
    Magazine](../Page/McClure's.md "wikilink")*, Cather claimed to be
    born in 1875. After 1920, she claimed 1876 as her birth year. That
    is the date carved into her gravestone at [Jaffrey, New
    Hampshire](../Page/Jaffrey,_New_Hampshire.md "wikilink").
2.  Lewis, Edith. *Willa Cather Living: A Personal Record*, New York:
    Alfred Knopf, 1953, pp. 5–7.
3.  Lee, Hermione. *Willa Cather: Double Lives*. New York: Pantheon,
    1989, p. 36.
4.  Woodress, James, pp. 72–3.
5.  O'Brien, Sharon. *Willa Cather: The Emerging Voice*. New York:
    Oxford, 1987. pp. 96–113.
6.  Lewis, Edith. *Willa Cather Living: A Personal Record*, p. 38. New
    York: Alfred Knopf, 1953
7.  Rolfe, Lionel. (2004). *The Uncommon Friendship of Yaltah Menuhin &
    Willa Cather*. American Legends/California Classics Books, 168 pp.
    ISBN 1-879395-46-0.
8.  ["Cather's Life: Chronology"](http://cather.unl.edu), The Willa
    Cather Archive, University of Nebraska, accessed March 21, 2007
9.
10.