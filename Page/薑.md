**薑**（[學名](../Page/學名.md "wikilink")：），原產於[東南亞熱帶地區](../Page/東南亞.md "wikilink")[植物](../Page/植物.md "wikilink")，開有黃綠色[花並有刺激性香味的](../Page/花.md "wikilink")[根莖](../Page/根莖.md "wikilink")。根莖鮮品或乾品可以作為調味品。薑經過泡製作為[中藥](../Page/中藥.md "wikilink")[藥材之一](../Page/藥材.md "wikilink")，也可以沖泡為[草本茶](../Page/草本茶.md "wikilink")。薑汁亦可用來製成[甜食](../Page/甜食.md "wikilink")，如[薑糖](../Page/薑糖.md "wikilink")、[薑汁撞奶](../Page/薑汁撞奶.md "wikilink")、[薑母茶等](../Page/薑母茶.md "wikilink")。薑耕種起源於[亞洲並擁有悠久](../Page/亞洲.md "wikilink")[歷史](../Page/歷史.md "wikilink")，目前在[印度](../Page/印度.md "wikilink")、[東南亞](../Page/東南亞.md "wikilink")、[西非和](../Page/西非.md "wikilink")[加勒比生產也見增長](../Page/加勒比.md "wikilink")。

## [園藝](../Page/園藝.md "wikilink")

薑由[白色和桃紅色的花芽開出](../Page/白色.md "wikilink")[黃色的](../Page/黃色.md "wikilink")[花](../Page/花.md "wikilink")，由於它的美麗和對溫暖氣候的適應性，薑在[亞熱帶常用於環境美化](../Page/亞熱帶.md "wikilink")，是[多年生植物](../Page/多年生植物.md "wikilink")，三到四[英呎高](../Page/英呎.md "wikilink")。

## 生產趨向

[印度目前佔全球薑的生產量約](../Page/印度.md "wikilink")30%，取代[中國成為最大生產國](../Page/中國.md "wikilink")，中國是第二大生產國(\~20.5%)，跟隨的是[印度尼西亞](../Page/印度尼西亞.md "wikilink")(\~12.7%)，[尼泊爾](../Page/尼泊爾.md "wikilink")(\~11.5%)和[尼日利亞](../Page/尼日利亞.md "wikilink")(\~10%)。

薑屬於會嚴重消耗土壤養分的作物，農業界甚至有土地種植一年薑後需休耕七年來恢復地力的說法；由於印尼，尼日利亞乃至中國、台灣的薑農常採用破壞森林的掠奪式農業來開闢薑田，薑的種植在主要生產國家都產生了生態問題。

## 中药

中醫認為，薑性溫。乾薑性斂，對止腹瀉療效顯著。生薑則散、降。《[本草綱目](../Page/本草綱目.md "wikilink")》記載，薑黃可治療：

1.  心痛難忍
2.  胎寒腹痛（嬰兒啼哭吐乳，大便色青，狀如驚風，出冷汗）
3.  產後血痛（腹內有血塊）
4.  瘡癬初發

《本草綱目》：「元素曰：[乾薑](../Page/乾薑.md "wikilink")...其用有四：通心陽，一也；去臟腑沉寒痼冷，二也；發諸經之寒氣，三也；治感寒腹痛，四也。」乾薑是薑母（母薑）乾燥所製，為能守能走、溫中散寒之至藥。乾薑炒至表黑內棕黃成為炮薑，亦為守而不走、溫經止血的中藥材。

營養學方面
可對抗發炎、清腸、減輕痙攣和抽筋，及刺激血液循環。是一種強的抗氧化劑，對於疼痛和傷口是一種有效的殺菌劑，可保護肝臟和胃，對治療腸道疾病、血液循環問題、關節炎、發燒、頭痛、熱潮紅、消化不良、孕婦晨吐、動暈症、肌肉疼痛、噁心和嘔吐很有幫助。

## 安全性

食用薑的量如果過大會有[副作用](../Page/副作用.md "wikilink")\[1\]。雖然薑獲美國
[FDA列為](../Page/Food_and_Drug_Administration.md "wikilink")「一般是安全」，目前已知它可能影響抗凝血劑[Warfarin](../Page/Warfarin.md "wikilink")，另外有膽結石的人應該避免。\[2\]

食用薑如果發生過敏反應，一般都是以[紅疹表現](../Page/紅疹.md "wikilink")。雖然一般都是安全，但薑仍可導致[胸口灼熱感](../Page/胸口灼熱感.md "wikilink")、脹氣或噁心，尤其是以粉狀服用。吞食新鮮的嫩薑可能導致腸道阻塞，有消化道潰瘍、發炎性消化道疾病或已有腸阻塞病史者，可在服用多量嫩薑時可能發生嚴重反映。\[3\]
因薑促進膽汁分泌，有膽結石的病人可能產生不良反應。\[4\] 另外也有研究顯示薑可能影響血壓、凝血功能與導致心律不整。\[5\]

2011年6月，台灣衛生單位扣押從中國禾博天然產物有限公司進口，以已汙染[DIBP的薑製成的](../Page/DIBP.md "wikilink")8萬顆膠囊。\[6\]

<table>
<thead>
<tr class="header">
<th><p>十大最大薑生產國— 2008年6月11日</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>國家</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><strong></strong></p></td>
</tr>
<tr class="odd">
<td><p>沒有標誌=官方數據、P =官方數據， F =糧食與農業組織估計， * =非官方或半官方或者不反映數據， C =計算數據，A =集體數據(可以包括官方，半官方或估計);<br />
來源: <a href="https://web.archive.org/web/20150906230329/http://faostat.fao.org/site/567/DesktopDefault.aspx?PageID=567#ancor">Food And Agricultural Organization of United Nations: Economic And Social Department: The Statistical Devision</a></p></td>
</tr>
</tbody>
</table>

{{-}}

## 圖片集

<File:Zingiber> officinale.JPG|植株 <File:Zingiber> officinale young
leaf.jpg|葉 [File:ഇഞ്ചിപ്പൂവ്.JPG|花苞](File:ഇഞ്ചിപ്പൂവ്.JPG%7C花苞)
<File:Ginger> rhizome.jpg|根莖 <File:Ginger> cross section.jpg|根莖剖面
<File:Ginger> tea-01.jpg|薑茶 <File:Ginger> ale.jpg|薑汁啤酒 <File:Handmade>
ginger fudge.jpg|薑糖 <File:Berner> Haselnusslebkuchen von Beck Glatz
sx.jpg|薑餅

## 參見

  - [蔥](../Page/蔥.md "wikilink")
  - [生抽](../Page/生抽.md "wikilink")、[醬油](../Page/醬油.md "wikilink")
  - [熟油](../Page/熟油.md "wikilink")
  - [蒸魚](../Page/蒸魚.md "wikilink")
  - [調味料](../Page/調味料.md "wikilink")
  - [姜罚](../Page/姜罚.md "wikilink")

## 参考來源

<div class="references-small">

  - 引用

<!-- end list -->

  - 書目

<!-- end list -->

  -
  -
  -
<!-- end list -->

  - 資料庫

<!-- end list -->

  -
  - [薑,
    Jiang](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&record=1&channelid=1288&searchword=%28Zingiber+officinale%29&extension=all)
    藥用植物圖像數據庫 ([香港浸會大學中醫藥學院](../Page/香港浸會大學.md "wikilink"))

</div>

## 外部連結

  - [李時珍《本草綱目》：薑性溫，味辛](https://web.archive.org/web/20090202095019/http://chiusang.com.hk/vegetable/vegetable_73.htm)
  - [薑
    Jiang](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00510)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [生薑
    Shengjiang](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00012)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [乾薑
    Ganjiang](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00013)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [乾薑 Gan
    Jiang](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00659)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [優┷噸酮
    Euxanthone](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00613)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[塊](../Category/藥用植物.md "wikilink")
[Category:調味料](../Category/調味料.md "wikilink")
[officinale](../Category/薑屬.md "wikilink")
[Category:食療](../Category/食療.md "wikilink")

1.

2.

3.

4.
5.
6.