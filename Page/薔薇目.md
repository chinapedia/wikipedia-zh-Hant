**薔薇目**（[学名](../Page/学名.md "wikilink")：Rosales）是[開花植物中的一](../Page/開花植物.md "wikilink")[目](../Page/目.md "wikilink")，擁有9個[科](../Page/科.md "wikilink")，约261[属](../Page/属.md "wikilink")，7725[种](../Page/种.md "wikilink")\[1\]
。

## 分类

薔薇目分为9科：

  - [鈎毛樹科](../Page/鈎毛樹科.md "wikilink") Barbeyaceae
  - [大麻科](../Page/大麻科.md "wikilink") Cannabaceae
  - [八瓣果科](../Page/八瓣果科.md "wikilink") Dirachmaceae
  - [胡穨子科](../Page/胡穨子科.md "wikilink") Elaeagnaceae
  - [桑科](../Page/桑科.md "wikilink") Moraceae
  - [薔薇科](../Page/薔薇科.md "wikilink") Rosaceae
  - [鼠李科](../Page/鼠李科.md "wikilink") Rhamnaceae
  - [榆科](../Page/榆科.md "wikilink") Ulmaceae
  - [蕁麻科](../Page/蕁麻科.md "wikilink") Urticaceae

根据[分子生物学及](../Page/分子生物学.md "wikilink")[种系发生学](../Page/种系发生学.md "wikilink")，薔薇科是薔薇目最基底的成员，然后分成两支：其中一支以榆科为基底成员，然后大麻科与荨麻科和桑科为[姐妹群](../Page/姐妹群.md "wikilink")；另一支则以鼠李科为最先分出的分支，胡颓子科与钩毛树和八瓣果科为姐妹群\[2\]。如下：

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[\*](../Category/蔷薇目.md "wikilink")
[Category:双子叶植物纲](../Category/双子叶植物纲.md "wikilink")

1.
2.