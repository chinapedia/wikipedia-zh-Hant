**薛仁果**，或作**仁杲**（，[注音](../Page/注音.md "wikilink")：ㄍㄠˇ，）\[1\]，[河東汾陰](../Page/河東.md "wikilink")（今[山西省](../Page/山西省.md "wikilink")[運城市](../Page/運城市.md "wikilink")[萬榮縣](../Page/萬榮縣.md "wikilink")）人，[中国](../Page/中国.md "wikilink")[唐朝初隴西割据者](../Page/唐朝.md "wikilink")“秦帝”[薛舉長子](../Page/薛舉.md "wikilink")，后被唐朝攻灭。

## 生平

生年没有记载。[隋炀帝](../Page/隋炀帝.md "wikilink")[大业十三年](../Page/大业_\(年号\).md "wikilink")（617年）薛仁果跟隨父親[薛舉起兵](../Page/薛舉.md "wikilink")，[薛舉本](../Page/薛舉.md "wikilink")[金城](../Page/金城.md "wikilink")[校尉](../Page/校尉.md "wikilink")，後自称「秦王」、「西秦霸王」，後稱秦帝，[年号](../Page/年号.md "wikilink")[秦兴](../Page/秦兴.md "wikilink")。据[陇西之地](../Page/陇西.md "wikilink")，扼[丝路交通](../Page/丝绸之路.md "wikilink")。

[唐高祖](../Page/唐高祖.md "wikilink")[武德元年](../Page/武德.md "wikilink")（618年）五月，[李淵](../Page/李淵.md "wikilink")[稱帝](../Page/稱帝.md "wikilink")，建立唐朝。同年六月，薛舉入涇州涇川（今[甘肅](../Page/甘肅.md "wikilink")[涇川縣北五里](../Page/涇川縣.md "wikilink")），縱兵虜掠，直至豳州（今[陝西](../Page/陝西.md "wikilink")[彬縣](../Page/彬縣.md "wikilink")）、岐州（今陝西[鳳翔](../Page/鳳翔.md "wikilink")）一帶。唐朝秦王[李世民以西討元帥的名義](../Page/李世民.md "wikilink")，和[劉文靜](../Page/劉文靜.md "wikilink")、[殷開山等](../Page/殷開山.md "wikilink")「八總管」領兵前往抗擊，時世民患[瘧疾](../Page/瘧疾.md "wikilink")，殷開山等人輕敵出戰，唐軍八總管皆敗，士卒死者十之五六，李安遠、[劉弘基](../Page/劉弘基.md "wikilink")、慕容羅睺等人被俘，但八月時擄獲唐將的薛举亦勞累而病卒，仁果继位。十一月，李世民率軍坚守高墌（今陝西[長武縣北](../Page/長武縣.md "wikilink")），相持六十餘日，薛仁果軍糧盡，其將梁胡郎降唐，諸將各懷異心，世民乘機攻擊，大敗薛仁果於淺水原（今陝西長武東北），擒仁果，被斬於市，史稱[淺水原之戰](../Page/淺水原之戰.md "wikilink")。

## 家庭

### 父亲

西秦霸王[薛举](../Page/薛举.md "wikilink")

### 兄弟

晉王薛仁越等

## 参考文献

<div class="references-small">

<references />

</div>

[X](../Category/隋末民变政权皇帝.md "wikilink")
[X](../Category/唐朝被處決者.md "wikilink")
[R](../Category/河东薛氏.md "wikilink")
[R](../Category/薛姓.md "wikilink")

1.  《[舊唐書](../Page/舊唐書.md "wikilink")》和《[新唐書](../Page/新唐書.md "wikilink")》皆稱“薛仁杲”。[吴兢](../Page/吴兢.md "wikilink")《太宗勋史》、焦璐《唐朝年代记》、[陈嶽](../Page/陈嶽.md "wikilink")《唐统纪》皆作“薛仁果”。《[資治通鑑](../Page/資治通鑑.md "wikilink")》引[昭陵六骏的铭文確認為薛仁果](../Page/昭陵六骏.md "wikilink")。