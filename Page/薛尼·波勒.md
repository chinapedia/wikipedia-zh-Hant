**薛尼·波勒**（，），美國電影[導演](../Page/導演.md "wikilink")，憑影片《[非洲之旅](../Page/非洲之旅.md "wikilink")》（1985年）奪得[第58屆](../Page/第58屆奧斯卡金像獎.md "wikilink")[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")[最佳電影獎](../Page/奥斯卡最佳影片奖.md "wikilink")。

1934年生於[美國](../Page/美國.md "wikilink")[印第安納州](../Page/印第安納州.md "wikilink")[拉斐特](../Page/拉斐特_\(印第安纳州\).md "wikilink")，2008年因胃癌死于洛杉矶，享壽73歲。

## 作品

### 導演

  - 《[攞命舞](../Page/攞命舞.md "wikilink")》（*They Shoot Horses, Don't They?*）
  - 《[俏郎君](../Page/俏郎君.md "wikilink")》（*The Way We Were*）
  - 1982年 《[杜絲先生](../Page/杜絲先生.md "wikilink")》（）
  - 1985年 《[遠離非洲](../Page/遠離非洲.md "wikilink")》 Out of Africa
  - 1993年 《[黑色豪門企業](../Page/黑色豪門企業.md "wikilink")》 The Firm
  - 1995年 《[新龍鳳配](../Page/新龍鳳配.md "wikilink")》　Sabrina - Remake
  - 1999年 《[疑雲密佈](../Page/疑雲密佈.md "wikilink")》　Random HEARTS
  - 2005年 《[雙面翻譯](../Page/雙面翻譯.md "wikilink")》　The Interpreter

### 製片

  - 2008年 《[為愛朗讀](../Page/為愛朗讀.md "wikilink")》The Reader
  - 2008年 《[速寫建築大師](../Page/弗兰克·盖里#.E9.9B.BB.E5.BD.B1.md "wikilink")》The
    Sketches of Frank Gehry
  - 2007年 《[全面反擊](../Page/全面反擊.md "wikilink")》（）
  - 2005年 《雙面翻譯》　The Interpreter
  - 2003年 《[冷山](../Page/冷山.md "wikilink")》　Cold Mountain
  - 2001年 《[驚婚計](../Page/驚婚計.md "wikilink")》　Birthday Girl
  - 1999年 《疑雲密佈》　Random HEARTS
  - 1995年 《[理性與感性](../Page/理性與感性_\(電影\).md "wikilink")》 Sense and
    Sensibility （註：[李安導演](../Page/李安.md "wikilink")）

### 演員

  - 2007年 《全面反擊》　Michael Clayton （客串）
  - 2002年 《[命運交錯](../Page/命運交錯.md "wikilink")》　Changing Lanes
  - 1999年 《[大開眼戒](../Page/大開眼戒.md "wikilink")》　Eyes Wide Shut
  - 1999年 《疑雲密佈》　Random HEARTS
  - 1982年 《窈窕淑男》 Tootsie

## 参考资料

## 外部參考

  - [影音台 -
    薛尼波拉克](https://web.archive.org/web/20070121045445/http://movie.kingnet.com.tw/search/index.html?act=actor&r=1127386236)
  - [美聯社 - 薛尼·波勒 Sydney
    Pollack，享壽73歲。](http://www.msnbc.msn.com/id/24832016/)

[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:奥斯卡最佳导演奖获奖者](../Category/奥斯卡最佳导演奖获奖者.md "wikilink")
[Category:犹太裔美国作家](../Category/犹太裔美国作家.md "wikilink")
[Category:印第安纳州男演员](../Category/印第安纳州男演员.md "wikilink")
[Category:罹患胃癌逝世者](../Category/罹患胃癌逝世者.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美國飛行員](../Category/美國飛行員.md "wikilink")
[Category:黃金時段艾美獎獲獎者](../Category/黃金時段艾美獎獲獎者.md "wikilink")
[Category:英語電影導演](../Category/英語電影導演.md "wikilink")
[Category:加利福尼亞州癌症逝世者](../Category/加利福尼亞州癌症逝世者.md "wikilink")
[Category:奧斯卡最佳導演獎獲獎者](../Category/奧斯卡最佳導演獎獲獎者.md "wikilink")
[Category:美国电视剧导演](../Category/美国电视剧导演.md "wikilink")