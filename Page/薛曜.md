**薛曜**，[蒲州汾陰](../Page/蒲州.md "wikilink")（今[山西](../Page/山西.md "wikilink")[萬榮](../Page/萬榮.md "wikilink")）人，[唐代畫家](../Page/唐代.md "wikilink")。

## 生平

薛曜出身[河东薛氏西祖](../Page/河东薛氏.md "wikilink")，是[宰相](../Page/宰相.md "wikilink")[薛元超之子](../Page/薛元超.md "wikilink")\[1\]，是詩人[薛道衡曾孫](../Page/薛道衡.md "wikilink")，[魏徵外甥](../Page/魏徵.md "wikilink")。與[薛稷同一師承](../Page/薛稷.md "wikilink")，書學[褚遂良](../Page/褚遂良.md "wikilink")，瘦硬有神，但薛曜的字體更纖細，是[宋徽宗](../Page/宋徽宗.md "wikilink")「[瘦金體](../Page/瘦金體.md "wikilink")」的前源。\[2\][吳湖帆的字畫也是從他學習得來的](../Page/吳湖帆.md "wikilink")。\[3\]。《夏日游石淙詩并序》是薛曜的代表作，為“[石淙河摩崖題記](../Page/石淙河摩崖題記.md "wikilink")”之一。

## 作品

  - 《子夜冬歌》
  - 《奉和聖製夏日遊石淙山》
  - 《邙山古意》
  - 《送道士入天台》

## 参考文献

<div class="references-small">

<references />

</div>

## 參見

  - [薛瓘](../Page/薛瓘.md "wikilink")

[X薛](../Category/唐朝畫家.md "wikilink")
[X薛](../Category/万荣县人.md "wikilink")
[Y曜](../Category/河东薛氏.md "wikilink")

1.  许多史书（包含《[资治通鉴](../Page/资治通鉴.md "wikilink")》在内）都将[薛瓘与薛曜视为同一人](../Page/薛瓘.md "wikilink")，《通鉴》卷第202曰：“及吐蕃求和亲，请尚[太平公主](../Page/太平公主.md "wikilink")，上乃为之立太平观，以公主为观主以拒之。至是，始选光禄卿汾阴薛曜之子[绍尚焉](../Page/薛绍.md "wikilink")。绍母，太宗女[城阳公主也](../Page/城阳公主.md "wikilink")。”这是错误的。据西安碑林指出，薛瓘是[薛怀昱之后](../Page/薛怀昱.md "wikilink")，而薛曜是宰相薛元超之子。
2.
3.