**薛舉**（），[河东郡](../Page/河東郡_\(中國\).md "wikilink")[汾阴县](../Page/汾阴县.md "wikilink")（今[山西省](../Page/山西省.md "wikilink")[运城市](../Page/运城市.md "wikilink")[万荣县](../Page/万荣县.md "wikilink")）人，其父薛汪时徙居[兰州](../Page/兰州.md "wikilink")[金城郡](../Page/金城郡.md "wikilink")（今[甘肃省](../Page/甘肃省.md "wikilink")[兰州市](../Page/兰州市.md "wikilink")），[中國](../Page/中國.md "wikilink")[隋代末年群雄之一](../Page/隋代.md "wikilink")。大业十三年（617年）四月，薛举起兵反隋，自称西秦霸王，年号秦兴。大业十三年（617年）七月，薛举称帝，迁都秦州。武德元年（618年），薛举与唐军交战，在浅水原大败秦王李世民，正欲乘胜直取长安，却突然病逝。

## 生平

父親[薛汪時移居到](../Page/薛汪.md "wikilink")[金城](../Page/金城郡.md "wikilink")，薛舉武勇豪邁，家中巨富，在地方上結識了許多豪傑人士。後擔任金城的[校尉](../Page/校尉.md "wikilink")。[大業](../Page/大业_\(年号\).md "wikilink")13年（617年），隋朝衰落，各地大亂，金城一帶飢民眾多，盜賊蜂起，金城令[郝瑗募兵鎮壓](../Page/郝瑗.md "wikilink")。薛舉在出征前的宴會中劫持[郝瑗](../Page/郝瑗.md "wikilink")，以他的名義發兵逮捕地方官，開倉救齊災民。

他號稱西秦霸王，年號[秦興](../Page/秦興.md "wikilink")，擊敗隋的地方軍隊，佔領隴西地區，隨即稱帝於蘭州。其後欲進攻關中，為[李世民所敗](../Page/李世民.md "wikilink")，薛舉因此有降唐之心。但[郝瑗反對](../Page/郝瑗.md "wikilink")，於是欲聯合[突厥](../Page/突厥.md "wikilink")[莫賀咄設合攻長安](../Page/莫賀咄設.md "wikilink")，但因莫賀咄設被李世民所懷柔未發兵而失敗。

618年再度與唐朝軍隊作戰，主帥李世民以及唐朝將領[劉文靜](../Page/劉文靜.md "wikilink")、[殷開山等因輕敵而被薛舉擊敗](../Page/殷開山.md "wikilink")，欲順勢進攻長安時病死，由他的兒子[薛仁果繼任](../Page/薛仁杲.md "wikilink")，給他[諡號](../Page/諡號.md "wikilink")**武帝**。史書上記載薛舉性格較為殘忍，對戰俘加以殺害，甚至使用割舌鼻等酷刑，因此無法獲得人民廣泛的支持。

## 家庭

### 妻子

  - [鞠皇后](../Page/鞠皇后.md "wikilink")

### 子女

  - 秦帝[薛仁果](../Page/薛仁杲.md "wikilink")
  - 晉王[薛仁越](../Page/薛仁越.md "wikilink")
  - 其余诸子，失名

## 部將

  - [宗羅睺](../Page/宗羅睺.md "wikilink")：興王
  - [鍾利俗](../Page/鍾利俗.md "wikilink")：羌族部落酋長
  - [常仲興](../Page/常仲興.md "wikilink")
  - [郝瑗](../Page/郝瑗.md "wikilink")
  - [褚亮](../Page/褚亮.md "wikilink")
  - [仵士政](../Page/仵士政.md "wikilink")：詐降擒住[常達](../Page/常達.md "wikilink")
  - [張貴](../Page/張貴.md "wikilink")

## 脚注

## 参考资料

  - 《[旧唐书](../Page/旧唐书.md "wikilink")》薛舉传
  - 《[资治通鉴](../Page/资治通鉴.md "wikilink")》

[Category:隋末民变政权皇帝](../Category/隋末民变政权皇帝.md "wikilink")
[Category:隋朝地方官员](../Category/隋朝地方官员.md "wikilink")
[Category:万荣人](../Category/万荣人.md "wikilink")
[J](../Category/河东薛氏.md "wikilink")
[J](../Category/薛姓.md "wikilink")
[Category:諡武](../Category/諡武.md "wikilink")