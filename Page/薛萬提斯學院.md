[Banco_Español_del_Río_de_la_Plata_(Madrid)_05.jpg](https://zh.wikipedia.org/wiki/File:Banco_Español_del_Río_de_la_Plata_\(Madrid\)_05.jpg "fig:Banco_Español_del_Río_de_la_Plata_(Madrid)_05.jpg")的塞萬提斯學院總部\]\]
**塞万提斯学院**（）由[西班牙政府於](../Page/西班牙.md "wikilink")1991年創辦，是世界性的[非營利組織](../Page/非營利組織.md "wikilink")，以推廣[西班牙語](../Page/西班牙語.md "wikilink")，以至西班牙及美洲西班牙語地區文化為目的。其總部設於[馬德里](../Page/馬德里.md "wikilink")[阿尔拉卡街](../Page/阿尔卡拉街.md "wikilink")49号，以紀念西班牙文學名著[堂吉诃德的作者](../Page/堂吉诃德.md "wikilink")[塞万提斯](../Page/塞万提斯.md "wikilink")。

2006年7月14日，北京塞万提斯学院（Instituto Cervantes de
Pekin）揭幕，位于北京市[朝阳区工体南门甲](../Page/朝阳区.md "wikilink")1号，占地3000多平方米，是塞万提斯学院的第58所分院。[1](https://web.archive.org/web/20070328003422/http://news.xinhuanet.com/newscenter/2006-07/14/content_4835001.htm)

## 參看

  - [法國文化協會](../Page/法國文化協會.md "wikilink")
  - [歌德学院](../Page/歌德学院.md "wikilink")
  - [英國文化協會](../Page/英國文化協會.md "wikilink")
  - [孔子学院](../Page/孔子学院.md "wikilink")
  - [卡蒙斯学院](../Page/卡蒙斯学院.md "wikilink")

## 外部連結

  - [塞万提斯学院网站](http://www.cervantes.es/) （西班牙文）
  - [塞万提斯学院网站](http://pekin.cervantes.es/) （中文）
  - [Portal del hispanismo](http://www.hispanismo.es/)
  - [塞万提斯学院马来西亚网站](https://helpcat.edu.my/partners/aula-cervantes/) （英文）

[category:西班牙文化](../Page/category:西班牙文化.md "wikilink")

[Category:西班牙教育](../Category/西班牙教育.md "wikilink")
[Category:語言教育](../Category/語言教育.md "wikilink")
[Category:西班牙語教學](../Category/西班牙語教學.md "wikilink")
[Category:1991年西班牙建立](../Category/1991年西班牙建立.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")
[Category:文化推广组织](../Category/文化推广组织.md "wikilink")