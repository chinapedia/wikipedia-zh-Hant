**薩利克法**（[英語](../Page/英語.md "wikilink")：Salic law;
[拉丁语](../Page/拉丁语.md "wikilink")：lex
Salica），是[中世紀以來](../Page/中世紀.md "wikilink")[西欧通行的法典](../Page/西欧.md "wikilink")。此法限制女性繼承。至今，欧洲一些国家仍存在女性无权继承[王位](../Page/王位.md "wikilink")、[世袭爵位的政治传统](../Page/爵位.md "wikilink")。在家族男性后裔绝嗣的情况下，容许女性继承，是为半萨利克法。

## 起源

萨利克法发源于[法兰克人萨利克部族中通行的各种习惯法](../Page/法兰克人.md "wikilink")，并因此而得名。在公元[6世纪初期](../Page/6世纪.md "wikilink")，这些习惯法被[法兰克帝国国王](../Page/法兰克帝国.md "wikilink")[克洛维一世汇编为法律](../Page/克洛维一世.md "wikilink")。萨利克法是[查理曼帝国法律的基础](../Page/查理曼帝国.md "wikilink")。

萨利克法主要是一部刑法典和程序法典，极其详细规定了列举了各种违法犯罪应科处的赔偿金，其中对于人身[伤害](../Page/伤害.md "wikilink")、[财产损害](../Page/财产.md "wikilink")、[偷盗和](../Page/偷盗.md "wikilink")[侮辱的赔偿规定尤为详细](../Page/侮辱.md "wikilink")，并规定受害者所得到的赔偿金的三分之一应交给[王室](../Page/王室.md "wikilink")。萨利克法也包括一些民法的法令，其中包括女性后裔不得继承土地的条款。但是6世纪下半叶，[法兰克国王](../Page/法兰克.md "wikilink")[希爾佩里克一世曾经颁布过一道修改萨利克法的](../Page/希爾佩里克一世.md "wikilink")[敕令](../Page/敕令.md "wikilink")，规定死者如无子嗣，土地可改由其[女儿继承](../Page/女儿.md "wikilink")，而不再交还公社。

## 历史影响

萨利克法中有关女性继承权的规定随着法兰克帝国的分裂和联姻扩散到大多数[欧洲的](../Page/欧洲.md "wikilink")[天主教国家中](../Page/天主教.md "wikilink")，女性无权继承土地的规定逐渐演变为对女性继承权的剥夺，并对[中世纪和近代欧洲历史产生了很大的影响](../Page/中世纪.md "wikilink")。[西班牙的历次](../Page/西班牙.md "wikilink")[卡洛斯战争](../Page/卡洛斯战争.md "wikilink")，其起源都是旁系男性继承人对直系女性继承人权利的争议。

[英法百年战争诱因之一](../Page/英法百年战争.md "wikilink")，就是[法国](../Page/法国.md "wikilink")[卡佩王朝](../Page/卡佩王朝.md "wikilink")[查理四世死后没有男性继承人](../Page/查理四世_\(法兰西\).md "wikilink")，[英格兰国王](../Page/英格兰.md "wikilink")[爱德华三世因是](../Page/爱德华三世.md "wikilink")[查理四世妹妹](../Page/查理四世_\(法兰西\).md "wikilink")[伊莎贝拉的儿子要求得到](../Page/伊莎贝拉_\(法兰西\).md "wikilink")[法國國王](../Page/法國國王.md "wikilink")[寶座](../Page/寶座.md "wikilink")，法国方面则认定萨利克法不支持女性系后裔的继承权，[查理四世的堂兄](../Page/查理四世_\(法兰西\).md "wikilink")[腓力六世随之](../Page/腓力六世_\(法兰西\).md "wikilink")[加冕](../Page/加冕.md "wikilink")，开创法国的[瓦卢瓦王朝](../Page/瓦卢瓦王朝.md "wikilink")，爱德华三世虽然妥协，但冲突的火种已然埋下。

[英国允许女性继承](../Page/英国.md "wikilink")，但[汉诺威实行萨利克法](../Page/汉诺威.md "wikilink")，因此汉诺威王朝的[维多利亚女王继為](../Page/维多利亚女王.md "wikilink")[英国君主时](../Page/英国君主.md "wikilink")，不得不將汉诺威王位轉讓给其叔父[恩斯特親王](../Page/恩斯特·奥古斯特一世_\(汉诺威\).md "wikilink")。此外，在唯一仍由英国统治的原[诺曼第公国领土](../Page/诺曼第.md "wikilink")——[海峡群岛上](../Page/海峡群岛.md "wikilink")，英国女王[伊丽莎白二世的头衔是](../Page/伊丽莎白二世.md "wikilink")[诺曼第公爵](../Page/诺曼第公爵.md "wikilink")（Duke
of Normandy），而非“女公爵”（Duchess）。

为了回避萨利克法的不利影响，[波兰女王](../Page/波兰.md "wikilink")[雅德维加在](../Page/雅德维加.md "wikilink")1384年继承波兰王位时，宣布自己为国王（Hedvig
Rex Poloniæa），而非女王（Hedvig Regina Poloniæa）。

在被[废黜的王室中](../Page/废黜.md "wikilink")，萨利克法仍有影响。俄罗斯[皇位觊觎者](../Page/皇位觊觎者.md "wikilink")[弗拉基米尔·基里洛维奇仅有一女](../Page/弗拉基米尔·基里洛维奇.md "wikilink")[玛利亚·弗拉基米洛夫娜](../Page/玛利亚·弗拉基米洛夫娜.md "wikilink")。他逝世后，因对女性继承权的分歧，俄罗斯皇位觊觎者分为两人，一即玛利亚·弗拉基米洛夫娜，另为[季米特里·羅曼諾維奇](../Page/季米特里·羅曼諾維奇.md "wikilink")。[羅馬尼亞王國末代国王](../Page/羅馬尼亞王國.md "wikilink")[米哈伊一世在](../Page/米哈伊一世.md "wikilink")1947年退位，因为没有儿子，长女[瑪格麗塔在](../Page/瑪格麗塔公主_\(羅馬尼亞\).md "wikilink")1997年成为[王储](../Page/王储.md "wikilink")，但这并不符合罗马尼亚王国法律。

## 备注

## 參考資料

  - 《薩利克法典神話與十六七世紀法國排斥女性的政治文化傳統》.湯曉燕

[Category:欧洲法律史](../Category/欧洲法律史.md "wikilink")
[Category:繼承法](../Category/繼承法.md "wikilink")