[缩略图](https://zh.wikipedia.org/wiki/File:Satsumanagano_Station.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Sendaigawa_No2-2860-r1.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Central_Satsuma_from_tabaru2.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Satsuma_Police_Station_Yachi_Police_Box.JPG "fig:缩略图")屋地[派出所](../Page/派出所.md "wikilink")\]\]
**薩摩町**（）是位於[日本](../Page/日本.md "wikilink")[鹿兒島縣北部內陸地區的一](../Page/鹿兒島縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[薩摩郡](../Page/薩摩郡.md "wikilink")，成立于2005年3月22日，由舊[宮之城町](../Page/宮之城町.md "wikilink")、[鶴田町](../Page/鶴田町_\(鹿兒島縣\).md "wikilink")、[薩摩町合併而成](../Page/薩摩町_\(2005年以前\).md "wikilink")。新設置的薩摩町和過去的薩摩町的中文名稱雖然同為「薩摩」，但[日文原名是不同的](../Page/日文.md "wikilink")，新的薩摩町日文原名是使用「薩摩」的[平假名](../Page/平假名.md "wikilink")「」。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬[南伊佐郡鶴田村](../Page/南伊佐郡.md "wikilink")、山崎村、[佐志村](../Page/佐志村.md "wikilink")、宮之城村、[永野村](../Page/永野村.md "wikilink")、大村。
  - 1896年3月29日：南伊佐郡被併入[薩摩郡](../Page/薩摩郡.md "wikilink")。
  - 1919年7月1日：宮之城村改制為[宮之城町](../Page/宮之城町.md "wikilink")。
  - 1922年4月1日：[求名村從宮之城町分村](../Page/求名村.md "wikilink")。
  - 1949年2月1日：[中津川村從大村](../Page/中津川村.md "wikilink")（大村後已合併為[薩摩川內市](../Page/薩摩川內市.md "wikilink")）分村。
  - 1953年11月3日：山崎村改制為[山崎町](../Page/山崎町.md "wikilink")。
  - 1954年10月15日：宮之城町和佐志村[合併為新設置的宮之城町](../Page/市町村合併.md "wikilink")。
  - 1954年12月1日：永野村、求名村、中津川村合併為[薩摩町](../Page/薩摩町_\(2005年以前\).md "wikilink")。
  - 1955年4月1日：宮之城町和山崎町合併為新設置的宮之城町。
  - 1963年4月1日：鶴田村改制為[鶴田町](../Page/鶴田町_\(鹿兒島縣\).md "wikilink")。
  - 2005年3月22日：宮之城町、鶴田町和薩摩町合併為新設置的**薩摩町**<small>（日文原名：）</small>。\[1\]

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1910年</p></th>
<th><p>1910年-1930年</p></th>
<th><p>1930年-1950年</p></th>
<th><p>1950年-1970年</p></th>
<th><p>1970年-1990年</p></th>
<th><p>1990年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>樋脇村</p></td>
<td><p>1940年11月10日<br />
改制為樋脇町</p></td>
<td><p>2004年10月12日<br />
合併為薩摩川內市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>入來村</p></td>
<td><p>1948年10月1日<br />
改制為入來町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上東鄉村</p></td>
<td><p>1952年12月1日<br />
改制並改名為東鄉町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下東鄉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>川內市</p></td>
<td><p>川內市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>永利村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>隈之城村</p></td>
<td><p>1929年5月20日<br />
川內町</p></td>
<td><p>1940年2月11日<br />
改制為川內市</p></td>
<td><p>1951年4月1日<br />
川內市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>平佐村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高城郡<br />
東水引村</p></td>
<td><p>1896年3月29日<br />
薩摩郡東水引村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高城郡<br />
西水引村</p></td>
<td><p>1896年3月29日<br />
薩摩郡西水引村</p></td>
<td><p>1933年7月1日<br />
改名為水引村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高城郡<br />
高城村</p></td>
<td><p>1896年3月29日<br />
薩摩郡高城村</p></td>
<td><p>1960年1月1日<br />
改制為高城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>甑島郡<br />
上甑村</p></td>
<td><p>1896年3月29日<br />
薩摩郡上甑村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>甑島郡<br />
里村</p></td>
<td><p>1896年3月29日<br />
薩摩郡里村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>甑島郡<br />
下甑村</p></td>
<td><p>1896年3月29日<br />
薩摩郡下甑村</p></td>
<td><p>下甑村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1949年4月1日<br />
鹿島村分村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南伊佐郡<br />
藺牟田村</p></td>
<td><p>1896年3月29日<br />
薩摩郡藺牟田村</p></td>
<td><p>1955年4月1日<br />
合併為祁答院町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南伊佐郡<br />
黑木村</p></td>
<td><p>1896年3月29日<br />
薩摩郡黑木村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南伊佐郡<br />
大村</p></td>
<td><p>1896年3月29日<br />
薩摩郡大村</p></td>
<td><p>大村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1949年2月1日<br />
中津川村分村</p></td>
<td><p>1954年12月1日<br />
合併為薩摩町</p></td>
<td><p>2005年3月22日<br />
合併為薩摩町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南伊佐郡<br />
永野村</p></td>
<td><p>1896年3月29日<br />
薩摩郡永野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南伊佐郡<br />
宮之城村</p></td>
<td><p>1896年3月29日<br />
薩摩郡宮之城村</p></td>
<td><p>1922年4月1日<br />
求名村分村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1919年7月1日<br />
改制為宮之城町</p></td>
<td><p>1954年10月15日<br />
合併為宮之城町</p></td>
<td><p>1955年4月1日<br />
合併為宮之城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南伊佐郡<br />
佐志村</p></td>
<td><p>1896年3月29日<br />
薩摩郡佐志村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南伊佐郡<br />
山崎村</p></td>
<td><p>1896年3月29日<br />
薩摩郡山崎村</p></td>
<td><p>1953年11月3日<br />
改制為山崎町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南伊佐郡<br />
西鶴田村</p></td>
<td><p>1896年3月29日<br />
薩摩郡鶴田村</p></td>
<td><p>1963年4月1日<br />
改制為鶴田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

目前轄區內無鐵路經過，在1987年以前曾有[國鐵](../Page/國鐵.md "wikilink")[宮之城線通過](../Page/宮之城線.md "wikilink")。

  - [國鐵](../Page/國鐵.md "wikilink")
      - [宮之城線](../Page/宮之城線.md "wikilink")：[薩摩山崎車站](../Page/薩摩山崎車站.md "wikilink")
        - [船木車站](../Page/船木車站.md "wikilink") -
        [宮之城車站](../Page/宮之城車站.md "wikilink") -
        [佐志車站](../Page/佐志車站.md "wikilink") -
        [薩摩湯田車站](../Page/薩摩湯田車站.md "wikilink") -
        [薩摩鶴田車站](../Page/薩摩鶴田車站.md "wikilink") -
        [薩摩求名車站](../Page/薩摩求名車站.md "wikilink") -
        [廣橋車站](../Page/廣橋車站.md "wikilink") -
        [薩摩永野車站](../Page/薩摩永野車站.md "wikilink")

## 觀光資源

  - [宮之城鐵道記念館](../Page/宮之城鐵道記念館.md "wikilink")：舊宮之城車站
  - [鶴田鐵道記念館](../Page/鶴田鐵道記念館.md "wikilink")：舊薩摩鶴田車站
  - [永野鐵道記念館](../Page/永野鐵道記念館.md "wikilink")：舊薩摩永野車債
  - [興詮寺](../Page/興詮寺.md "wikilink")
  - [宮之城溫泉](../Page/宮之城溫泉.md "wikilink")

## 教育

### 高等學校

  - [鹿兒島縣立宮之城高等學校](../Page/鹿兒島縣立宮之城高等學校.md "wikilink")
  - [鹿兒島縣立宮之城農業高等學校](../Page/鹿兒島縣立宮之城農業高等學校.md "wikilink")
  - [鹿兒島縣立薩摩中央高等學校](../Page/鹿兒島縣立薩摩中央高等學校.md "wikilink")

## 參考資料

## 外部連結

1.