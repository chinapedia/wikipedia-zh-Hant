**薩摩郡**（）是[日本](../Page/日本.md "wikilink")[鹿兒島縣轄下的一個](../Page/鹿兒島縣.md "wikilink")[郡](../Page/郡.md "wikilink")。

現僅轄有以下1町：

  - [薩摩町](../Page/薩摩町.md "wikilink")

過去的轄區還曾包含現在的[薩摩川內市](../Page/薩摩川內市.md "wikilink")。

## 沿革

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，薩摩郡下設置[隈之城村](../Page/隈之城村.md "wikilink")、[高江村](../Page/高江村.md "wikilink")、[永利村](../Page/永利村.md "wikilink")、[平佐村](../Page/平佐村.md "wikilink")、上東鄉村、[下東鄉村](../Page/下東鄉村.md "wikilink")、樋脇村、入來村。（8村）
  - 1896年3月29日 -
    [高城郡](../Page/高城郡_\(鹿兒島縣\).md "wikilink")（高城村、[東水引村](../Page/東水引村.md "wikilink")、西水引村）、[南伊佐郡](../Page/南伊佐郡.md "wikilink")（鶴田村、山崎村、[佐志村](../Page/佐志村.md "wikilink")、宮之城村、[永野村](../Page/永野村.md "wikilink")、[大村](../Page/大村_\(鹿兒島縣\).md "wikilink")、[黑木村](../Page/黑木村.md "wikilink")、[藺牟田村](../Page/藺牟田村.md "wikilink")）和[甑島郡](../Page/甑島郡.md "wikilink")（[上甑村](../Page/上甑村.md "wikilink")、[里村](../Page/里村.md "wikilink")、[下甑村](../Page/下甑村.md "wikilink")）併入薩摩郡。（22村）
  - 1919年7月1日：宮之城村改制為[宮之城町](../Page/宮之城町.md "wikilink")。（1町21村）
  - 1922年4月1日：[求名村從宮之城町分村](../Page/求名村.md "wikilink")。（1町22村）
  - 1929年5月20日：隈之城村、平佐村、東水引村[合併為川內町](../Page/市町村合併.md "wikilink")。（2町19村）
  - 1933年7月1日：西水引村改名為[水引村](../Page/水引村.md "wikilink")。
  - 1940年2月11日：川內町改制為[川內市](../Page/川內市.md "wikilink")，並脫離薩摩郡。（1町19村）
  - 1940年11月10日：樋脇村改制為[樋脇町](../Page/樋脇町.md "wikilink")。（2町18村）
  - 1948年10月1日：入來村改制為[入來町](../Page/入來町.md "wikilink")。（3町17村）
  - 1949年2月1日：[中津川村從大村分村](../Page/中津川村.md "wikilink")。（3町18村）
  - 1949年4月1日：[鹿島村從](../Page/鹿島村.md "wikilink")[下甑村分村](../Page/下甑村.md "wikilink")。（3町19村）
  - 1951年4月1日：水引村被併入川內市。（3町18村）
  - 1952年12月1日：上東鄉村改制並改名為[東鄉町](../Page/東鄉町_\(鹿兒島縣\).md "wikilink")。（4町17村）
  - 1953年11月3日：山崎村改制為[山崎町](../Page/山崎町.md "wikilink")。（5町16村）
  - 1954年10月15日：宮之城町和佐志村合併為新設置的宮之城町。（5町15村）
  - 1954年12月1日：永野村、求名村和中津川村合併為[薩摩町](../Page/薩摩町_\(2005年以前\).md "wikilink")。（6町12村）
  - 1955年4月1日：（6町9村）
      - 宮之城町和山崎町合併為新設置的宮之城町。
      - 大村、藺牟田村、黑木村合併為[祁答院町](../Page/祁答院町.md "wikilink")。
  - 1956年9月30日：永利村和高江村被併入川內市。（6町7村）
  - 1957年4月1日：下東鄉村被廢除，轄區分別併入川內市、高城村和東鄉町。（6町6村）
  - 1960年1月1日：高城村改制為[高城町](../Page/高城町.md "wikilink")。（7町5村）
  - 1963年4月1日：鶴田村改制為[鶴田町](../Page/鶴田町_\(鹿兒島縣\).md "wikilink")。（8町4村）
  - 1965年4月15日：高城町被併入川內市，並脫離薩摩郡。（7町4村）
  - 2004年10月12日：入來町、祁答院町、東鄉町、樋脇町、鹿島村、上甑村、里村、下甑村和川內市合併為[薩摩川內市](../Page/薩摩川內市.md "wikilink")，並脫離薩摩郡。\[1\]（3町）
  - 2005年3月22日：薩摩町、鶴田町、宮之城町合併為[薩摩町](../Page/薩摩町.md "wikilink")<small>（日文原名：）</small>。\[2\]（1町）

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1910年</p></th>
<th><p>1910年-1930年</p></th>
<th><p>1930年-1950年</p></th>
<th><p>1950年-1970年</p></th>
<th><p>1970年-1990年</p></th>
<th><p>1990年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>樋脇村</p></td>
<td><p>1940年11月10日<br />
改制為樋脇町</p></td>
<td><p>2004年10月12日<br />
合併為薩摩川內市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>入來村</p></td>
<td><p>1948年10月1日<br />
改制為入來町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上東鄉村</p></td>
<td><p>1952年12月1日<br />
改制並改名為東鄉町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下東鄉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>川內市</p></td>
<td><p>川內市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>永利村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>隈之城村</p></td>
<td><p>1929年5月20日<br />
川內町</p></td>
<td><p>1940年2月11日<br />
改制為川內市</p></td>
<td><p>1951年4月1日<br />
川內市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>平佐村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高城郡<br />
東水引村</p></td>
<td><p>1896年3月29日<br />
薩摩郡東水引村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高城郡<br />
西水引村</p></td>
<td><p>1896年3月29日<br />
薩摩郡西水引村</p></td>
<td><p>1933年7月1日<br />
改名為水引村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高城郡<br />
高城村</p></td>
<td><p>1896年3月29日<br />
薩摩郡高城村</p></td>
<td><p>1960年1月1日<br />
改制為高城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>甑島郡<br />
上甑村</p></td>
<td><p>1896年3月29日<br />
薩摩郡上甑村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>甑島郡<br />
里村</p></td>
<td><p>1896年3月29日<br />
薩摩郡里村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>甑島郡<br />
下甑村</p></td>
<td><p>1896年3月29日<br />
薩摩郡下甑村</p></td>
<td><p>下甑村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1949年4月1日<br />
鹿島村分村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南伊佐郡<br />
藺牟田村</p></td>
<td><p>1896年3月29日<br />
薩摩郡藺牟田村</p></td>
<td><p>1955年4月1日<br />
合併為祁答院町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南伊佐郡<br />
黑木村</p></td>
<td><p>1896年3月29日<br />
薩摩郡黑木村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南伊佐郡<br />
大村</p></td>
<td><p>1896年3月29日<br />
薩摩郡大村</p></td>
<td><p>大村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1949年2月1日<br />
中津川村分村</p></td>
<td><p>1954年12月1日<br />
合併為薩摩町</p></td>
<td><p>2005年3月22日<br />
合併為薩摩町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南伊佐郡<br />
永野村</p></td>
<td><p>1896年3月29日<br />
薩摩郡永野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南伊佐郡<br />
宮之城村</p></td>
<td><p>1896年3月29日<br />
薩摩郡宮之城村</p></td>
<td><p>1922年4月1日<br />
求名村分村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1919年7月1日<br />
改制為宮之城町</p></td>
<td><p>1954年10月15日<br />
合併為宮之城町</p></td>
<td><p>1955年4月1日<br />
合併為宮之城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南伊佐郡<br />
佐志村</p></td>
<td><p>1896年3月29日<br />
薩摩郡佐志村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南伊佐郡<br />
山崎村</p></td>
<td><p>1896年3月29日<br />
薩摩郡山崎村</p></td>
<td><p>1953年11月3日<br />
改制為山崎町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南伊佐郡<br />
西鶴田村</p></td>
<td><p>1896年3月29日<br />
薩摩郡鶴田村</p></td>
<td><p>1963年4月1日<br />
改制為鶴田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

[Category:薩摩國](../Category/薩摩國.md "wikilink")

1.
2.