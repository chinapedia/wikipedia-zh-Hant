**薩爾蘭**（），原名**薩爾**（），是[德国西南部的一个](../Page/德国.md "wikilink")[联邦州](../Page/联邦州_\(德国\).md "wikilink")，首府[萨尔布吕肯](../Page/萨尔布吕肯.md "wikilink")，与德國[莱茵兰-普法尔茨邦](../Page/莱茵兰-普法尔茨.md "wikilink")、[法国](../Page/法国.md "wikilink")[洛林區](../Page/洛林.md "wikilink")，以及[卢森堡接壤](../Page/卢森堡.md "wikilink")。萨尔兰州與上述各地區以及[比利时的](../Page/比利时.md "wikilink")[瓦隆地區等五處](../Page/瓦隆.md "wikilink")，共同构成，以彰顯德法雙語文化交流之在地特色。萨尔兰州的面积只有约2600平方公里，是德国除[不来梅邦](../Page/不来梅邦.md "wikilink")、[汉堡和](../Page/汉堡.md "wikilink")[柏林外最小的一个邦](../Page/柏林.md "wikilink")；人口约100万。1999年[德国基督教民主联盟在该州执政至今](../Page/德国基督教民主联盟.md "wikilink")。

## 历史

历史上萨尔兰曾经数易其主，仅最近200年就有8次之多，[一战后](../Page/一战.md "wikilink")，根据《[凡尔赛和约](../Page/凡尔赛和约.md "wikilink")》「[薩爾盆地地區](../Page/薩爾盆地地區.md "wikilink")」从德国划分出来归[国际联盟管辖](../Page/国际联盟.md "wikilink")，实际則由法国控制，萨尔矿区由法国独占15年，以作为法国在战争期间被毁坏矿井的补偿。条约还提出，在15年期满之后，由公民投票决定萨尔的未来。1935年，超過90%的當地居民選擇回归德国。[二战中](../Page/二战.md "wikilink")，薩爾遭受了沉重的轰炸，戰後薩爾地區再度被[盟軍交由法國託管](../Page/盟軍.md "wikilink")，成为[薩爾保護領而未加入](../Page/薩爾保護領.md "wikilink")[西德](../Page/西德.md "wikilink")。1946年，法国在该地区推行自治，並从1940年代末到1950年代初，法国一直试图让萨尔成为一个独立的国家，但終究未能成功。1955年10月23日经薩爾地區公民投票，多數居民希望该地区早日回归德国。1956年[西德與法國在](../Page/西德.md "wikilink")[盧森堡簽訂](../Page/盧森堡.md "wikilink")[薩爾條約](../Page/薩爾條約.md "wikilink")（因此又稱[盧森堡條約](../Page/盧森堡條約.md "wikilink")），約定原薩爾地區於1957年1月1日起回归德国版图，隨後並成立「薩爾蘭邦」。\[1\]

## 教育

  - （Hochschule für Technik und Wirtschaft des Saarlandes）

簡稱HTW Saar，成立於1946年，幾經發展於1991年改為今名。「德意志-法蘭西高等學院」計畫（Deutsch-Französisches
Hochschulinstitut）始於1999年，以双硕士（德国、法国）和三硕士（德国、法国、卢森堡）为特色，课程在德国大学、法国大学和其他国家进行。它的前身是1978年德法两国签署协议的「德意志-法蘭西高等科技學院」計畫（德语：Deutsch-Französische
Hochschulinstitut für Technik und Wirtschaft，缩写：DFHI；法语：L'institut
Supérieur Franco-Allemand de Techniques, d'Économie et de
Sciences，缩写：ISFATES），院址选在德国的萨尔布吕肯和法国的[梅斯](../Page/梅斯.md "wikilink")，学生可以选择这两个城市和卢森堡完成3年或5年的大学学业。

  - （Hochschule für Musik Saar）

成立於1947年，為一培育高等音樂人才之公立高等院校。

  - [薩爾蘭大学](../Page/薩爾蘭大学.md "wikilink")（Universität des Saarlandes）

為邦內一所綜合型公立大學，係由法國政府透過法國[南錫大學的協助於](../Page/南錫.md "wikilink")1948年创立，位於[萨尔布吕肯主校区的](../Page/萨尔布吕肯.md "wikilink")[法学和](../Page/法学.md "wikilink")[電腦科學等专业在全德具有良好的声誉](../Page/计算机科学.md "wikilink")，享譽國際的[馬克斯·普朗克學會即選址於薩爾蘭大學成立](../Page/馬克斯·普朗克學會.md "wikilink")[馬克斯·普朗克電腦科學研究所](../Page/馬克斯·普朗克電腦科學研究所.md "wikilink")。而Homburg校区的[医学專業研究水平則在德国领先](../Page/医学.md "wikilink")，萨尔兰州的肿瘤研究数据库中心則位于萨尔布吕肯主校區。薩爾蘭大學擁有8個學院。

## 经济

[VH_pano5_1.jpg](https://zh.wikipedia.org/wiki/File:VH_pano5_1.jpg "fig:VH_pano5_1.jpg")\]\]
薩爾蘭的最重要经济支柱是[汽车制造业和汽车配件供应业](../Page/汽车.md "wikilink")，包括[萨尔路易的](../Page/萨尔路易.md "wikilink")[福特汽车](../Page/福特汽车.md "wikilink")，Homburg的[博世公司](../Page/博世公司.md "wikilink")、[舍弗乐集团和](../Page/舍弗乐集团.md "wikilink")[米其林轮胎](../Page/米其林.md "wikilink")，以及[萨尔布吕肯的](../Page/萨尔布吕肯.md "wikilink")[采埃孚等工厂](../Page/采埃孚.md "wikilink")，提供了大部分的工业就业岗位。

薩爾蘭在历史上深受[煤炭和](../Page/煤炭.md "wikilink")[钢铁的影响](../Page/钢铁.md "wikilink")，经营超过一百年的[弗尔克林根钢铁厂因为代表了钢铁工业的黄金时期](../Page/弗尔克林根钢铁厂.md "wikilink")，在1994年入选[联合国教科文组织的](../Page/联合国教科文组织.md "wikilink")[世界文化遗产名录](../Page/世界文化遗产.md "wikilink")，如今位于[弗尔克林根的](../Page/弗尔克林根.md "wikilink")[萨尔钢铁股份公司](../Page/萨尔钢铁股份公司.md "wikilink")（Saarstahl
AG）仍然扮演着重要的角色。[陶瓷制造商](../Page/陶瓷.md "wikilink")[威勒罗尔&宝赫公司](../Page/威勒罗尔&宝赫公司.md "wikilink")（Villeroy\&Boch）的知名度也远远超越了薩爾蘭的边界。此外[信息技术和](../Page/信息技术.md "wikilink")[生物产业也正在迅速崛起](../Page/生物产业.md "wikilink")，如薩爾布魯根的[IDS
Scheer和Ingbert的](../Page/IDS_Scheer.md "wikilink")[SAP](../Page/SAP公司.md "wikilink")。

2005年薩爾蘭[经济增长率达到历史最高点](../Page/经济增长率.md "wikilink")，2005年上半年比前一年同期上涨2.8%，而当时全德国的平均增长为0.6%。

## 政治

位于德国西南部的[薩爾蘭州是除了](../Page/薩爾蘭州.md "wikilink")[柏林](../Page/柏林.md "wikilink")、[汉堡和](../Page/汉堡.md "wikilink")[不莱梅之外德国面积最小的邦](../Page/不莱梅.md "wikilink")。保守派的[基督教民主联盟](../Page/基督教民主联盟.md "wikilink")（基民盟）在该邦仅获得34.5％的选票，远低于2004年邦议会选举的47.5％；[社会民主党](../Page/社会民主党.md "wikilink")（[社民党](../Page/社民党.md "wikilink")）也只获得24.5％的选票，低于2004年邦议会选举的30.8％。两党均创下了在薩爾蘭最差的邦议会选举纪录。[左翼党](../Page/左翼党.md "wikilink")－[民社党](../Page/民社党.md "wikilink")（左翼党）则在薩爾蘭的选举中取得巨大胜利，获得了21.3％的选民支持，而该党2004年仅获得了2.3％的选票。此外，联盟90／[绿党](../Page/绿党.md "wikilink")（绿党）和自由民主党（自民党）分别获得了5.9％和9.2％的选票。

## 参考资料

## 外部链接

  - [萨尔兰州政府网站](http://www.saarland.de/)

[\*](../Category/萨尔兰州.md "wikilink")
[S](../Category/德国联邦州.md "wikilink")
[Saarland](../Category/1957年建立的行政區劃.md "wikilink")

1.