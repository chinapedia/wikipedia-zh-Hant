**薩馬島**
（****）位於[菲律賓中部](../Page/菲律賓.md "wikilink")，是[米沙鄢群島最東的島嶼](../Page/米沙鄢群島.md "wikilink")。下分三省：[北薩馬省](../Page/北薩馬省.md "wikilink")、[東薩馬省和](../Page/東薩馬省.md "wikilink")[薩馬省](../Page/薩馬省.md "wikilink")。这三个省是[東米沙鄢](../Page/東米沙鄢.md "wikilink")[政區的一部分](../Page/菲律賓政區.md "wikilink")，[莱特岛和](../Page/莱特岛.md "wikilink")[比利兰岛是附近的岛屿省份](../Page/比利兰省.md "wikilink")。薩馬島面積13,080平方公里，是菲律宾的第四大岛，仅次于[吕宋岛](../Page/吕宋岛.md "wikilink")、[棉兰老岛和](../Page/棉兰老岛.md "wikilink")[内格罗斯岛](../Page/内格罗斯岛.md "wikilink")。該島在2013年11月被[颱風海燕重創](../Page/颱風海燕_\(2013年\).md "wikilink")。

## 地理與氣候

[Panapukan_beach.jpg](https://zh.wikipedia.org/wiki/File:Panapukan_beach.jpg "fig:Panapukan_beach.jpg")的[海灘](../Page/海灘.md "wikilink")\]\]

薩馬島是在[米沙鄢群岛最东端的岛屿](../Page/米沙鄢群岛.md "wikilink")。[雷伊泰島和薩馬島是由](../Page/雷伊泰島.md "wikilink")[聖胡安尼克海峽分开](../Page/聖胡安尼克海峽.md "wikilink")，在其最窄处只有大约两公里宽。有[聖胡安尼克桥跨越海峡](../Page/聖胡安尼克桥.md "wikilink")。薩馬島和菲律賓最大的岛屿[吕宋岛](../Page/吕宋岛.md "wikilink")[比科尔半岛东南部由](../Page/比科尔半岛.md "wikilink")[圣贝纳迪诺海峡分开](../Page/圣贝纳迪诺海峡.md "wikilink")。薩馬島東部、北部面臨[菲律宾海和](../Page/菲律宾海.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")。薩馬島西部面臨[薩馬海](../Page/薩馬海.md "wikilink")。萨马岛南部的雷伊泰湾東毗菲律宾海和太平洋。

薩馬島上大部分是山區和叢林，[热带雨林气候](../Page/热带雨林气候.md "wikilink")，有大量的热带动植物
\[1\]。受從11月到4月和從8月至10月的[季風影響](../Page/季風.md "wikilink")，8月至4月是雨季。從5月至7月是比較乾燥的[夏季](../Page/夏季.md "wikilink")。此外，夏季由於發生在[太平洋西向的](../Page/太平洋.md "wikilink")[颱風](../Page/颱風.md "wikilink")，很容易在山區有非常強大的風雨。

## 城市

[卡尔贝扬](../Page/卡尔贝扬.md "wikilink")（Calbayog）是薩馬島以及薩馬省最大的城市\[2\]。[卡塔曼](../Page/卡塔曼.md "wikilink")（Catarman）是北薩馬省的首府。[菲律賓東部大學位於此](../Page/菲律賓東部大學.md "wikilink")\[3\]。[東薩馬省首府為](../Page/東薩馬省.md "wikilink")[博龍岸](../Page/博龍岸.md "wikilink")（Borongan）。

## 語言與產業

主要語言是[瓦瑞瓦瑞語](../Page/瓦瑞瓦瑞語.md "wikilink")，[宿务语](../Page/宿务语.md "wikilink")、[菲律賓語](../Page/菲律賓語.md "wikilink")、[英語也通](../Page/英語.md "wikilink")。[農業](../Page/農業.md "wikilink")，[林業](../Page/林業.md "wikilink")，[漁業是薩馬島的主要產業](../Page/漁業.md "wikilink")。農業有[水稻](../Page/水稻.md "wikilink")、[木薯](../Page/木薯.md "wikilink")、[番薯](../Page/番薯.md "wikilink")、[椰子等農產品](../Page/椰子.md "wikilink")。肯定的是，有很多[颱風帶來的損壞](../Page/颱風.md "wikilink")，其次是20世紀80年代[新人民軍反政府的](../Page/新人民軍.md "wikilink")[游擊戰帶來的破壞](../Page/游擊戰.md "wikilink")，[工業基礎在](../Page/工業.md "wikilink")[菲律賓相對的比較脆弱](../Page/菲律賓.md "wikilink")，人民比較貧窮。

## 歷史

雖然東南亞和中國的貿易蓬勃發展，米沙鄢群島不是一個統一的國家，薩馬的位置也稍有偏離主要海上貿易路線。

1521年[斐迪南·麥哲倫率领](../Page/斐迪南·麥哲倫.md "wikilink")[西班牙船队首次环航地球](../Page/西班牙.md "wikilink")。3月17日麦哲伦船队来到萨马岛附近一个无人居住的小岛上，以便在那里补充一些淡水，并让海员们休整一下。邻近小岛上的居民前来观看西班牙人，用[椰子](../Page/椰子.md "wikilink")、棕榈酒等换取西班牙人的红帽子和一些小玩物。

然後，在1543年西班牙人路易·洛佩茲通過[呂宋南部的部分達到薩馬島](../Page/呂宋.md "wikilink")。將這些島嶼命名為（[西班牙王子](../Page/西班牙.md "wikilink")）菲利普二世（Feripinasu）群島，這是[菲律賓當前名稱的由來](../Page/菲律賓.md "wikilink")。

1900年[美菲战争](../Page/美菲战争.md "wikilink")，菲律賓游击战包围薩馬島北薩馬Catubig。1901年9月28日菲律賓游擊隊攻擊薩馬島東薩馬，48名[美軍第](../Page/美軍.md "wikilink")9步兵團的成員因此遇難。美軍雅各·H·史密斯將軍下令報復，殺害當地的10歲以上的每個菲律賓人。

在[第二次世界大战中最具有决定性的海战之一](../Page/第二次世界大战.md "wikilink")－[雷伊泰灣海戰在萨马岛南部的雷伊泰湾发生](../Page/雷伊泰灣海戰.md "wikilink")。

## 參考資料

### 外部連結

  - [薩馬省簡介](http://samar.lgu-ph.com/)

[S](../Category/菲律賓島嶼.md "wikilink")
[S](../Category/太平洋島嶼.md "wikilink")

1.

2.
3.