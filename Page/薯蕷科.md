**薯蕷科**（[学名](../Page/学名.md "wikilink")：）为[单子叶植物](../Page/单子叶植物.md "wikilink")[薯蓣目的一科](../Page/薯蓣目.md "wikilink")，包括8-9[属约](../Page/属.md "wikilink")750[种](../Page/种.md "wikilink")。

## 形态

薯蕷科植物是多年生草质缠绕[藤本植物](../Page/藤本植物.md "wikilink")，有块状或根状的地下[茎](../Page/茎.md "wikilink")；[茎平滑或有刺](../Page/茎.md "wikilink")；[叶互生或在上部对生](../Page/叶.md "wikilink")，为单叶或掌状复叶，中脉和侧脉由叶基发出，有掌状脉或网脉，这点同其他单子叶植物有别；[花单性](../Page/花.md "wikilink")，雌雄异株，雌花也带有几枚退化的雄蕊，花被裂片6枚，2列，雄蕊6或3枚，子房下位，3室，每室有2个胚珠；有翅蒴果或浆果，有3个翅状的棱，开裂为3个果瓣，种子一般都有翅。

## 分布

薯蕷科植物广泛分布在全世界的[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，[中国只有](../Page/中国.md "wikilink")2属约80[种](../Page/种.md "wikilink")，分布在[长江以南各地](../Page/长江.md "wikilink")。

## 分类

[克朗奎斯特分类法将本科列入](../Page/克朗奎斯特分类法.md "wikilink")[百合目](../Page/百合目.md "wikilink")，1998年的[APG
分类法根据](../Page/APG_分类法.md "wikilink")[基因分析将其单独划分为一个](../Page/基因.md "wikilink")[目](../Page/目.md "wikilink")，2003年经过改进的[APG
II
分类法将](../Page/APG_II_分类法.md "wikilink")[蒟蒻薯科和毛柄花科合并到本科中](../Page/蒟蒻薯科.md "wikilink")。

### 内部分类

  - 1998年的薯蕷科 Dioscoreaceae

<!-- end list -->

  - [比利牛斯薯蓣属](../Page/比利牛斯薯蓣属.md "wikilink") *Borderea*
  - [薯蓣属](../Page/薯蓣属.md "wikilink")
    *Dioscorea*（包括[山藥](../Page/山藥.md "wikilink")、[香芋等等](../Page/香芋.md "wikilink")，部份品種俗稱Yam）
  - *[Epipetrum](../Page/Epipetrum.md "wikilink")*
  - *[Rajania](../Page/Rajania.md "wikilink")*
  - *[Stenomeris](../Page/Stenomeris.md "wikilink")*
  - [浆果薯蓣属](../Page/浆果薯蓣属.md "wikilink") *Tamus* （通常列入薯蓣属中）

<!-- end list -->

  - ( 蒟蒻薯科 Taccaceae)

<!-- end list -->

  - [蒟蒻薯属](../Page/蒟蒻薯属.md "wikilink") *Tacca*

<!-- end list -->

  - ( 毛柄花科 Trichopodaceae)

<!-- end list -->

  - *[Avetra](../Page/Avetra.md "wikilink")*
  - [毛柄花属](../Page/毛柄花属.md "wikilink") *Trichopus*

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[薯蕷科](http://delta-intkey.com/angio/www/dioscore.htm)，[蒟蒻薯科](http://delta-intkey.com/angio/www/taccacea.htm)
  - [(USDA)单子叶植物](http://www.efloras.org/florataxon.aspx?flora_id=1200&taxon_id=10280)
  - [北美植物中的薯蕷科](http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=10280)
  - [NCBI分类法](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=4671&lvl=3&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL分类中的薯蕷科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Dioscoreaceae)，[蒟蒻薯科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Taccaceae)和[毛柄花科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Trichopodaceae)

[\*](../Category/薯蕷科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")
[Category:雌雄異株植物](../Category/雌雄異株植物.md "wikilink")