**薰衣草屬**（[学名](../Page/学名.md "wikilink")：**）是唇形科的一個屬，約有25-30種。[多年生](../Page/多年生植物.md "wikilink")[草本](../Page/草本.md "wikilink")、亞[灌木或小灌木](../Page/灌木.md "wikilink")\[1\]。

## 分布

原產地由[地中海地區](../Page/地中海.md "wikilink")（如[西班牙](../Page/西班牙.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")）、南至熱帶[非洲](../Page/非洲.md "wikilink")、東至[印度](../Page/印度.md "wikilink")，包括[加那利群島](../Page/加那利群島.md "wikilink")、非洲北部和東部、[歐洲南部](../Page/歐洲.md "wikilink")、[地中海地區](../Page/地中海.md "wikilink")、[阿拉伯半島和](../Page/阿拉伯半島.md "wikilink")[印度等地區](../Page/印度.md "wikilink")。

因为可栽培的品种被广泛栽培在世界各地的栽培园里，人们偶尔也能在许多非原产地发现从园里飘逸出来的种子形成的野生狀態的薰衣草族群。

在中國大陸，主要的種植地是[新疆](../Page/新疆.md "wikilink")[伊犁](../Page/伊犁.md "wikilink")（伊寧）地區和[甘肅清水地區](../Page/甘肅.md "wikilink")。

## 世界四大產地

  - [法國](../Page/法國.md "wikilink")[普羅旺斯](../Page/普羅旺斯.md "wikilink")
  - [日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")
  - [俄羅斯](../Page/俄羅斯.md "wikilink")[高加索山](../Page/高加索山.md "wikilink")
  - 中國伊犁河谷

## 栽培與利用

[Lavender_in_Singapore.jpg](https://zh.wikipedia.org/wiki/File:Lavender_in_Singapore.jpg "fig:Lavender_in_Singapore.jpg")
狹葉薰衣草（又叫真薰衣草或英國薰衣草）原産於地中海的西部，它的出油率較高，出油品質最高。提鍊後得到的精油可以直接在皮膚上使用。主要種植地是歐洲的英國，保加利亞的高山地區，大洋洲的澳大利亞和中國大陸的甘肅清水地區少量種植。

寬葉薰衣草： 葉子較狹葉薰衣草寬，出油率和質量都一般。一般園藝種植的較多。

雜交薰衣草：
狹葉薰衣草和寬葉薰衣草兩者雜交而成。它的出油率最高，出油品質較高，因此被廣泛用於香水，香皂以及日化產品中，也是世界上種植面積最廣的薰衣草品種。主要種植地是法國普羅旺斯地區和中國大陸的新疆伊犁地區。

另外目前還培育出了很多不同類型的品種。其他常種植來觀賞的種類有西班牙薰衣草、齒葉薰衣草和蕨葉薰衣草。

薰衣草的花穗可以做乾燥花和飾品。淡紫色、有香味的[花和](../Page/花.md "wikilink")[花蕾可以做香罐和香包](../Page/花蕾.md "wikilink")，把乾燥的[花密封在袋子內便可做成香包](../Page/花.md "wikilink")，將香包放在衣櫃內，可以使衣服帶有清香，並且可以防止蟲蛀。薰衣草的商業栽培，主要是要取得薰衣草的花來提取薰衣草精油，薰衣草[精油可以做為](../Page/精油.md "wikilink")[殺菌劑和](../Page/殺菌劑.md "wikilink")[芳香療法時使用的香精油](../Page/芳香療法.md "wikilink")\[2\]\[3\]。

薰衣草的花會分泌大量的[花蜜](../Page/花蜜.md "wikilink")，這些花蜜可以生產高品質[蜂蜜](../Page/蜂蜜.md "wikilink")，地中海周邊的國家是薰衣草蜂蜜的主要產地\[4\]。薰衣草的花可以做薰衣草[果醬](../Page/果醬.md "wikilink")，可以做為西點蛋糕的材料及裝飾物。薰衣草可以在烹調食物時，做為辛香料使用，可以單獨使用薰衣草，或是和其他香草混合做成[普羅旺斯香料使用](../Page/普羅旺斯香料.md "wikilink")。

目前台灣以甜薰衣草以及羽葉薰衣草最常見，其中又以甜薰衣草最易栽種。甜薰衣草是由狹葉薰衣草與齒葉薰衣草交配而成，可泡茶，茶味清香帶甜。而羽葉薰衣草，則是薰衣草品種中花期最長的，羽葉薰衣草含有多量樟腦酮，不建議食用，但可泡澡。
使用 薰衣草是花及葉均能使用的香草植物。甜薰衣草的花及葉可用來泡茶或沐浴，而羽葉薰衣草的花、葉都只適合沐浴。孕婦不適合長期使用。

## 用途

減輕壓力和沮喪，有益皮膚。對於燒燙傷、頭痛、牛皮癬和其他皮膚疾病有幫助。 鎮定、止痛、放鬆的效果

## 歷史

在[古希臘時代](../Page/古希臘.md "wikilink")，薰衣草被稱為**納德斯**（Nardus），有時也簡稱為**納德**（Nard），這個名稱來自於[敘利亞一個叫做](../Page/敘利亞.md "wikilink")[納達](../Page/納達.md "wikilink")（Naarda）的城市。

在[古羅馬時代](../Page/古羅馬.md "wikilink")，薰衣草的花一磅可以賣到一百[迪納里](../Page/迪納里.md "wikilink")（denarii），這個價錢約等於當時一個農場工人的一個月的工資，或是理髮師幫五十個人理髮所得的報酬。羅馬人會將薰衣草和各種香草一起放到洗澡水內，他們將這種沐浴的方法引進到[不列顛](../Page/不列顛.md "wikilink")。

在[黑死病肆虐的時代](../Page/黑死病.md "wikilink")，[法國](../Page/法國.md "wikilink")[格拉斯製作手套的工人](../Page/格拉斯.md "wikilink")，因為常以薰衣草油浸泡皮革，許多工人因此逃過[鼠疫的侵襲](../Page/鼠疫.md "wikilink")。這個故事也許有些真實性，因為鼠疫病菌是經由[跳蚤傳播的](../Page/跳蚤.md "wikilink")，而薰衣草可以驅除跳蚤。

## 草本療法

薰衣草已經被廣泛的使用於[藥草學上](../Page/中藥學.md "wikilink")，據說薰衣草油可以減輕和治療[昆蟲的咬傷](../Page/昆蟲.md "wikilink")，而薰衣草的花束可以驅除昆蟲。薰衣草油據說可以減輕[頭痛的症狀](../Page/頭痛.md "wikilink")。薰衣草常用來輔助入眠：將薰衣草的[種子和](../Page/種子.md "wikilink")[花加到](../Page/花.md "wikilink")[枕頭內](../Page/枕頭.md "wikilink")，可以幫助入眠；或是在睡前喝一杯薰衣草[茶](../Page/茶.md "wikilink")，在一杯熱水中加入一點薰衣草精油，有鎮靜安神、放鬆心情的效果。同時對於肌肉放鬆也有明顯的效果。薰衣草油（或是萃取的精油）以一比十的比例，薰衣草油一，水及[玫瑰水](../Page/玫瑰.md "wikilink")（或[金縷梅水](../Page/北美金縷梅.md "wikilink")）十，混合的稀釋液，據說可以治療[粉刺](../Page/粉刺.md "wikilink")。薰衣草油也可以用來治療[皮膚燒燙傷或發炎](../Page/皮膚.md "wikilink")（[伊朗的一種傳統療法](../Page/伊朗.md "wikilink")），有科學上的證據支持這種治療方式，因為它有消炎的功效，不過必須小心謹慎的使用，因為薰衣草油同時也是一種強力的[過敏源](../Page/過敏.md "wikilink")。也是一種有效的殺菌劑，能改善免疫系統功能，中和平衡身體狀況，對抗細菌和真菌感染，抒解憂鬱，減輕發炎。對粉刺、燙傷、溼疹、皮膚復原、睡眠問題和生活壓力有幫助！

## 種類

[Lavandula_angustifolia_-_Köhler–s_Medizinal-Pflanzen-087.jpg](https://zh.wikipedia.org/wiki/File:Lavandula_angustifolia_-_Köhler–s_Medizinal-Pflanzen-087.jpg "fig:Lavandula_angustifolia_-_Köhler–s_Medizinal-Pflanzen-087.jpg")
[Lavandula_latifolia_DehesaBoyalPuertollano.jpg](https://zh.wikipedia.org/wiki/File:Lavandula_latifolia_DehesaBoyalPuertollano.jpg "fig:Lavandula_latifolia_DehesaBoyalPuertollano.jpg")
[Lavandula_multifida.jpg](https://zh.wikipedia.org/wiki/File:Lavandula_multifida.jpg "fig:Lavandula_multifida.jpg")
[Topped_lavendar_flowerhead.jpg](https://zh.wikipedia.org/wiki/File:Topped_lavendar_flowerhead.jpg "fig:Topped_lavendar_flowerhead.jpg")

  - [狹葉薰衣草](../Page/狹葉薰衣草.md "wikilink")（*Lavandula angustifolia*）
  - *[Lavandula antineae](../Page/Lavandula_antineae.md "wikilink")*
  - *[Lavandula
    aristibracteata](../Page/Lavandula_aristibracteata.md "wikilink")*
  - *[Lavandula
    atriplicifolia](../Page/Lavandula_atriplicifolia.md "wikilink")*
  - *[Lavandula bipinnata](../Page/Lavandula_bipinnata.md "wikilink")*
  - *[Lavandula bramwellii](../Page/Lavandula_bramwellii.md "wikilink")*
  - *[Lavandula buchii](../Page/Lavandula_buchii.md "wikilink")*
  - [加那利薰衣草](../Page/加那利薰衣草.md "wikilink")（*Lavandula canariensis*）
  - *[Lavandula citriodora](../Page/Lavandula_citriodora.md "wikilink")*
  - *[Lavandula
    coronopifolia](../Page/Lavandula_coronopifolia.md "wikilink")*
  - [齒葉薰衣草](../Page/齒葉薰衣草.md "wikilink")（*Lavandula dentata*）
  - *[Lavandula
    dhofarensis](../Page/Lavandula_dhofarensis.md "wikilink")*
  - *[Lavandula erythraeae](../Page/Lavandula_erythraeae.md "wikilink")*
  - *[Lavandula
    galgalloensis](../Page/Lavandula_galgalloensis.md "wikilink")*
  - *[Lavandula gibsonii](../Page/Lavandula_gibsonii.md "wikilink")*
  - *[Lavandula hasikensis](../Page/Lavandula_hasikensis.md "wikilink")*
  - [綿毛薰衣草](../Page/綿毛薰衣草.md "wikilink")（*Lavandula lanata*）
  - [寬葉薰衣草](../Page/寬葉薰衣草.md "wikilink")（*Lavandula latifolia*）
  - *[Lavandula macra](../Page/Lavandula_macra.md "wikilink")*
  - *[Lavandula mairei](../Page/Lavandula_mairei.md "wikilink")*
  - *[Lavandula maroccana](../Page/Lavandula_maroccana.md "wikilink")*
  - *[Lavandula minutolii](../Page/Lavandula_minutolii.md "wikilink")*
  - [蕨葉薰衣草](../Page/蕨葉薰衣草.md "wikilink")（*Lavandula multifida*）
  - *[Lavandula nimmoi](../Page/Lavandula_nimmoi.md "wikilink")*
  - *[Lavandula
    pedunculata](../Page/Lavandula_pedunculata.md "wikilink")*
  - [羽葉薰衣草](../Page/羽葉薰衣草.md "wikilink")（*Lavandula pinnata*）
  - *[Lavandula pubescens](../Page/Lavandula_pubescens.md "wikilink")*
  - *[Lavandula qishnensis](../Page/Lavandula_qishnensis.md "wikilink")*
  - *[Lavandula rejdalii](../Page/Lavandula_rejdalii.md "wikilink")*
  - *[Lavandula
    rotundifolia](../Page/Lavandula_rotundifolia.md "wikilink")*
  - *[Lavandula saharica](../Page/Lavandula_saharica.md "wikilink")*
  - *[Lavandula
    samhanensis](../Page/Lavandula_samhanensis.md "wikilink")*
  - *[Lavandula setifera](../Page/Lavandula_setifera.md "wikilink")*
  - *[Lavandula
    somaliensis](../Page/Lavandula_somaliensis.md "wikilink")*
  - *[Lavandula
    sublepidota](../Page/Lavandula_sublepidota.md "wikilink")*
  - *[Lavandula subnuda](../Page/Lavandula_subnuda.md "wikilink")*
  - [西班牙薰衣草](../Page/西班牙薰衣草.md "wikilink")（*Lavandula stoechas*）
  - *[Lavandula tenuisecta](../Page/Lavandula_tenuisecta.md "wikilink")*
  - [綠薰衣草](../Page/綠薰衣草.md "wikilink")（*Lavandula viridis*）

此外还有以下几种主要的杂交品种：

  - *[Lavandula x allardii](../Page/Lavandula_x_allardii.md "wikilink")*
  - *[Lavandula x
    chaytorae](../Page/Lavandula_x_chaytorae.md "wikilink")*
  - *[Lavandula x
    christiana](../Page/Lavandula_x_christiana.md "wikilink")*
  - [綿毛大薰衣草](../Page/綿毛大薰衣草.md "wikilink") (*[Lavandula x
    ginginsii](../Page/Lavandula_x_ginginsii.md "wikilink")*)
  - *[Lavandula x
    heterophylla](../Page/Lavandula_x_heterophylla.md "wikilink")*
  - *[Lavandula x
    intermedia](../Page/Lavandula_x_intermedia.md "wikilink")*
  - [錦毛薰衣草](../Page/錦毛薰衣草.md "wikilink")（*Lavandula x Sawyer's Hybrid*）

## 參見

  - [薰衣草色](../Page/薰衣草色.md "wikilink")，根據薰衣草而命名的一種顏色。

## 參考文獻

## 伸延閱讀

  - 《台灣蔬果實用百科第二輯》，薛聰賢 著，薛聰賢出版社，2001年
  - 《蘋果日報》走進香草的世界，2004年11月27日

## 外部链接

  -
<!-- end list -->

  - [薰衣草
    Xunyicao](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00298)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[分類:草本花卉](../Page/分類:草本花卉.md "wikilink")

[灌](../Category/藥用植物.md "wikilink")
[Category:薰衣草属](../Category/薰衣草属.md "wikilink")
[Category:香草](../Category/香草.md "wikilink")
[Category:唇形科](../Category/唇形科.md "wikilink")

1.
2.
3.
4.