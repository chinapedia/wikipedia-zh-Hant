**藍天**（**Lam
Tin**，），原名**藍繼澤**，籍貫[廣東](../Page/廣東.md "wikilink")[潮州縣](../Page/潮州.md "wikilink")，已故[香港](../Page/香港.md "wikilink")[演員](../Page/演員.md "wikilink")，他是1970年代[無綫電視著名甘草](../Page/無綫電視.md "wikilink")[演員之一](../Page/演員.md "wikilink")。

## 簡介

在1961年與[方心](../Page/方心.md "wikilink")、[賀蘭等加入](../Page/賀蘭.md "wikilink")「桃源電影公司」當基本演員，1965年合約屆滿離開\[1\]。1967年[無線電視開臺](../Page/電視廣播有限公司.md "wikilink")，他於該年加入無線電視，專門參與[配音工作](../Page/配音.md "wikilink")。隨後藍天轉任[演員](../Page/演員.md "wikilink")，參與過多部[無綫電視劇集演出](../Page/無綫電視.md "wikilink")，主要是配角。他比較突出的乃於1980年《[親情](../Page/親情_\(電視劇\).md "wikilink")》一劇裡面裡飾演[周潤發的](../Page/周潤發.md "wikilink")[父親](../Page/父親.md "wikilink")。

1987年4月14日拍攝[無綫電視重頭劇](../Page/無綫電視.md "wikilink")《[大運河](../Page/大運河_\(電視劇\).md "wikilink")》期間，藍天在開車外出時，在[獅子山隧道撞車](../Page/獅子山隧道.md "wikilink")[身亡](../Page/身亡.md "wikilink")，終年49歲。

## 演出電影

  - 藍天的銀幕處男作《貧賤夫妻百事哀》(1952年，飾演念慈)；
  - 《花開燕子歸》》(1952年，飾演孟小全)；
  - 息影近10年才恢復演出[粵語](../Page/粵語.md "wikilink")[電影](../Page/電影.md "wikilink")《新姊妹花》(1962年，飾演馬嘉祥祥)；
  - 藍天的[電影遺作](../Page/電影.md "wikilink")《越南仔》(1982年)；
  - 藍天一生參演約30多齣[粵語](../Page/粵語.md "wikilink")[電影](../Page/電影.md "wikilink")。

### 演出電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

  - [家變](../Page/家變_\(1977年電視劇\).md "wikilink")(1977) 飾 呂剛 (金滿樓大廚)
  - [抉擇](../Page/抉擇_\(電視劇\).md "wikilink")(1979) 飾 韓麒
  - [親情](../Page/親情_\(電視劇\).md "wikilink")(1980) 飾 石啟泰
  - [上海灘續集](../Page/上海灘續集.md "wikilink")(1980) 跑龍套
  - [烽火飛花](../Page/烽火飛花.md "wikilink")(1981) 飾 犬養光雄
  - [火鳳凰](../Page/火鳳凰_\(電視劇\).md "wikilink")(1981) 飾 湯鶴年
  - [情謎](../Page/情謎.md "wikilink")（1981年）
  - [楊門女將](../Page/楊門女將_\(1981年劇集\).md "wikilink")(1981) 飾
    [王欽若](../Page/王欽若.md "wikilink")
  - [老虎甩鬚](../Page/老虎甩鬚.md "wikilink")(1981)
  - [萬水千山總是情](../Page/萬水千山總是情.md "wikilink")（1982）飾 超叔
  - [播音人](../Page/香港無線電視.md "wikilink")(1983) 飾 區長發
  - [警花出更](../Page/警花出更.md "wikilink")(1983) 飾 謝劍堂
  - [五虎將](../Page/五虎將_\(電視劇\).md "wikilink")(1984) 飾 鄧亨
  - [鹿鼎記](../Page/鹿鼎記.md "wikilink")(1984) 飾
    [康親王](../Page/康親王.md "wikilink")
  - [新紮師兄](../Page/新紮師兄.md "wikilink")(1984) 飾 曾敬思
  - [碧血劍](../Page/碧血劍.md "wikilink")(1985) 飾 木桑道人
  - [雪山飛狐](../Page/雪山飛狐_\(1985年電視劇\).md "wikilink")(1985) 飾
    [馬行空](../Page/馬行空.md "wikilink")
  - [新紮師兄續集](../Page/新紮師兄續集.md "wikilink")(1985) 飾 曾敬思
  - [六指琴魔](../Page/六指琴魔_\(1985年电视剧\).md "wikilink")(1985)
  - [流氓大亨](../Page/流氓大亨.md "wikilink")(1986) 飾 達 張先生（電視台高層）
  - [倚天屠龍記](../Page/倚天屠龍記_\(1986年電視劇\).md "wikilink")(1986) 飾
    [殷天正](../Page/殷天正.md "wikilink")
  - [大運河](../Page/大運河_\(電視劇\).md "wikilink")(1987) 飾
    [宇文述](../Page/宇文述.md "wikilink")
  - [鄭成功](../Page/鄭成功.md "wikilink")(1987) 飾 達固倫親王
  - [少林與詠春](../Page/少林與詠春.md "wikilink")(1987) 飾 大老爺

## 配音作品

### 動畫

| 首播年份     | 作品名稱                                | 配演角色          | 備註 |
| -------- | ----------------------------------- | ------------- | -- |
| **1980** | [太空戰士](../Page/太空戰士.md "wikilink")  | **湯金（德金．薩比）** |    |
| **1986** | [天眼急先鋒](../Page/銀河戰士.md "wikilink") | 魯窩根           |    |
|          |                                     |               |    |

### 特攝片

| 播映年份     | 作品名稱                                 | 配演角色    | 角色演員 | 備註 |
| -------- | ------------------------------------ | ------- | ---- | -- |
| **1979** | [伏魔三劍俠](../Page/伏魔三劍俠.md "wikilink") | **加貝拉** |      |    |
|          |                                      |         |      |    |

### 日劇

| 首播年份     | 作品名稱                                               | 配演角色   | 角色演員                               | 備註 |
| -------- | -------------------------------------------------- | ------ | ---------------------------------- | -- |
| **1970** | [大內煞星](../Page/:jp:大江戸捜査網.md "wikilink")           | 大內密探頭領 | 有待確認                               |    |
| **1972** | [柔道龍虎榜](../Page/:jp:姿三四郎_\(テレビドラマ\).md "wikilink") | 檀義磨    | [松枝錦治](../Page/松枝錦治.md "wikilink") |    |
|          |                                                    |        |                                    |    |

### 台劇

| 首播年份     | 作品名稱                                        | 配演角色    | 角色演員                             | 備註 |
| -------- | ------------------------------------------- | ------- | -------------------------------- | -- |
| **1975** | [保鑣](../Page/保鑣_\(1974年電視劇\).md "wikilink") | 賈胡桃／萬見愁 | [張允文](../Page/張允文.md "wikilink") |    |
|          |                                             |         |                                  |    |

### 英／美劇

| 首播年份                                                   | 作品名稱                                                                  | 配演角色                                                           | 角色演員                                                           | 備註 |
| ------------------------------------------------------ | --------------------------------------------------------------------- | -------------------------------------------------------------- | -------------------------------------------------------------- | -- |
| **1970年代**                                             | [神偷諜影](../Page/:en:It_Takes_a_Thief_\(1968_TV_series\).md "wikilink") | **拜恩**                                                         | [Malachi Throne](../Page/:en:Malachi_Throne.md "wikilink")     |    |
| [玄機妙算](../Page/:en:Search_\(TV_series\).md "wikilink") | **高華**                                                                | [Doug McClure](../Page/:en:Doug_McClure.md "wikilink")         |                                                                |    |
| [無敵女金剛](../Page/:en:The_Bionic_Woman.md "wikilink")    | **高西加**                                                               | [Richard Anderson](../Page/:en:Richard_Anderson.md "wikilink") |                                                                |    |
| **1976**                                               | [神探俏嬌娃](../Page/霹靂嬌娃.md "wikilink")                                   | **包士萊**                                                        | [David Doyle](../Page/:en:David_Doyle_\(actor\).md "wikilink") | 前期 |
| **1978**                                               | [神勇法醫官](../Page/:en:Quincy,_M.E..md "wikilink")                       | **昆西**                                                         | [傑克·克盧格曼](../Page/傑克·克盧格曼.md "wikilink")                       |    |
|                                                        |                                                                       |                                                                |                                                                |    |

### 電影

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>角色演員</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1980</strong></p></td>
<td><p><a href="../Page/教父_(電影).md" title="wikilink">教父</a></p></td>
<td><p>胡德志</p></td>
<td></td>
<td><p>[2]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [香港電影資料館：藍天參演電影的記錄](http://ipac.hkfa.lcsd.gov.hk/ipac20/ipac.jsp?session=L254V262094G7.1483&profile=hkfa&uri=link=3100036@!325@!3100024@!3100036&menu=search&submenu=basic_search&source=192.168.110.61@!horizon)

  -
## 參考

<div class="references-small">

<references />

</div>

[Category:香港车祸身亡者](../Category/香港车祸身亡者.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港配音員](../Category/香港配音員.md "wikilink")
[藍](../Category/前無綫電視配音員.md "wikilink")
[Category:香港粵語片演員](../Category/香港粵語片演員.md "wikilink")
[tin](../Category/藍姓.md "wikilink")

1.  《[香港影星](../Page/香港.md "wikilink")[年鑑](../Page/年鑑.md "wikilink")
    1967年》，[香港](../Page/香港.md "wikilink")《伴侶》[雜誌出版](../Page/雜誌.md "wikilink")，第88頁。
2.  [〈教父粵語配音，無綫鄭重其事〉](https://upload.cc/i/HQZIWc.jpg)，〖配音逸聞〗，《華僑日報》，1980年05月09日，第八張第二頁。