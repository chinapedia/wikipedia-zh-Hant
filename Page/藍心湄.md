**藍心湄**（英文名: Pauline
Lan，），生于[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")，俗稱“藍教主”，是台灣著名女藝人和商人。自1984年出道，橫跨[歌手](../Page/歌手.md "wikilink")、[主持人](../Page/主持人.md "wikilink")、[演員](../Page/演員.md "wikilink")，影、視、歌「三棲」紅星，有「百變女王」、「臺灣[瑪丹娜](../Page/瑪丹娜.md "wikilink")」之稱，並偶有詞曲創作，藍心湄的經典名曲包括濃妝搖滾、一見鍾情、愛我到今生等，稱霸[華語](../Page/華語.md "wikilink")[樂壇](../Page/樂壇.md "wikilink")、歷久不衰。現為[TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink")《[女人我最大](../Page/女人我最大.md "wikilink")》主持人，帶領時下[女性流行風潮](../Page/女性.md "wikilink")，受封為「時尚教主」。副業經營投資豐碩有成，坐擁九家餐廳和運動品牌代理商，身價超過15億，年收入逼近2億。

2012年11月10日首度登上[台北小巨蛋舉行出道](../Page/台北小巨蛋.md "wikilink")28年以來最大型售票演唱會[LanDIVA](../Page/LanDIVA.md "wikilink")\[1\]，售票率達9成，藍心湄甚至於演唱會上挑戰全裸換裝。

## 個人生活

  - 台北出生，[雲林縣出身](../Page/雲林縣.md "wikilink")，其父母育有三名子女，她为老二，上有一哥下有一妹（相差12歲）。

<!-- end list -->

  - [蘭雅國中](../Page/蘭雅國中.md "wikilink")（與前[立法委員](../Page/立法委員.md "wikilink")[姚文智同屆](../Page/姚文智.md "wikilink")\[2\]）、[聖心女中畢業](../Page/新北市私立聖心女子高級中學.md "wikilink")。

<!-- end list -->

  - 藍心湄的叔叔是「台灣三大鼓王」之一的[藍宜慧](../Page/藍宜慧.md "wikilink")（Roger
    Lan），曾任[台視大樂團](../Page/台視大樂團.md "wikilink")[爵士鼓手](../Page/爵士鼓.md "wikilink")、[高凌風專屬](../Page/高凌風.md "wikilink")[樂團總監](../Page/樂團.md "wikilink")，[孔鏘等藝人都是他的弟子](../Page/孔鏘.md "wikilink")。

<!-- end list -->

  - 幼年时曾经父亲的[橡皮筋生意相当成功](../Page/橡皮筋.md "wikilink")，当时家境不错；但至十六岁左右，先是哥哥意外英年早逝，接着雪上加霜父亲的生意破产负债达3000万之巨。

<!-- end list -->

  - 藍心湄非常喜歡[狗與](../Page/狗.md "wikilink")[河馬](../Page/河馬.md "wikilink")，家中有一隻領養的[流浪狗](../Page/流浪狗.md "wikilink")「LADY」，也因為喜歡河馬，歌迷特地為她成立[歌友會](../Page/歌友會.md "wikilink")「河馬家族」。

<!-- end list -->

  - 藍心湄在演藝圈有多位[義子女](../Page/結義.md "wikilink")，如[黃小柔](../Page/黃小柔.md "wikilink")、[汪東城](../Page/汪東城.md "wikilink")、[唐禹哲](../Page/唐禹哲.md "wikilink")、[謝和弦](../Page/謝和弦.md "wikilink")、[阿Ben](../Page/阿Ben.md "wikilink")、[張本渝](../Page/張本渝.md "wikilink")、[郭彥均](../Page/郭彥均.md "wikilink")、[郭彥甫](../Page/郭彥甫.md "wikilink")、[Gigi](../Page/林如琦.md "wikilink")。

<!-- end list -->

  - 1991年，藍心湄邀[舒淇](../Page/舒淇.md "wikilink")、[陶晶瑩](../Page/陶晶瑩.md "wikilink")、Roger與[黃乙玲等人一同投資成都川菜料理KiKi餐廳](../Page/黃乙玲.md "wikilink")，年營業額估約1億元台幣。

<!-- end list -->

  - 2008年6月，藍心湄赴[日本進行異源](../Page/日本.md "wikilink")[人工授精並成功](../Page/人工授精.md "wikilink")[懷孕](../Page/懷孕.md "wikilink")，不料2個月後仍[流產](../Page/流產.md "wikilink")。

## 音樂作品

### 專輯

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>專輯</p></th>
<th style="text-align: center;"><p>專輯</p></th>
<th style="text-align: center;"><p>發行日期</p></th>
<th style="text-align: center;"><p>唱片公司</p></th>
<th style="text-align: center;"><p>曲目</p></th>
<th style="text-align: center;"><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>1</strong></p></td>
<td style="text-align: center;"><p><strong>濃妝搖滾</strong></p></td>
<td style="text-align: center;"><p>1984年8月</p></td>
<td style="text-align: center;"><p><a href="../Page/飛羚唱片.md" title="wikilink">飛羚唱片</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>藍心湄化名「泥鰍」為「濃妝搖滾」、「週末夜天使」作詞。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>2</strong></p></td>
<td style="text-align: center;"><p><strong>勁舞的女孩</strong></p></td>
<td style="text-align: center;"><p>1985年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「迎接孤獨」為藍心湄親自作詞。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>3</strong></p></td>
<td style="text-align: center;"><p><strong>上緊發條</strong></p></td>
<td style="text-align: center;"><p>1985年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「夏夜燃燒的夢」為藍心湄親自作詞。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>4</strong></p></td>
<td style="text-align: center;"><p><strong>我與傑西的夢</strong></p></td>
<td style="text-align: center;"><p>1986年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「夜天使」、「我與傑西的夢」是藍心湄作詞。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>5</strong></p></td>
<td style="text-align: center;"><p><strong>鼓舞</strong></p></td>
<td style="text-align: center;"><p>1987年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「飛身舞勁」、「搖滾選手」為藍心湄親自作詞。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>6</strong></p></td>
<td style="text-align: center;"><p><strong>紅蜻蜓的夢</strong></p></td>
<td style="text-align: center;"><p>1988年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「小木偶的話」作詞者為藍心湄。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>7</strong></p></td>
<td style="text-align: center;"><p><strong>無色彩</strong></p></td>
<td style="text-align: center;"><p>1989年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「我的心想要飛出去」為藍心湄親自作詞。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>8</strong></p></td>
<td style="text-align: center;"><p><strong>要不要</strong></p></td>
<td style="text-align: center;"><p>1989年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>9</strong></p></td>
<td style="text-align: center;"><p><strong>我要你變心</strong></p></td>
<td style="text-align: center;"><p>1990年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>10</strong></p></td>
<td style="text-align: center;"><p><strong>不偽裝的溫柔</strong></p></td>
<td style="text-align: center;"><p>1990年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>11</strong></p></td>
<td style="text-align: center;"><p><strong>Something in My Eyes</strong></p></td>
<td style="text-align: center;"><p>1991年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>12</strong></p></td>
<td style="text-align: center;"><p><strong>我的溫柔只有你看得見</strong></p></td>
<td style="text-align: center;"><p>1992年8月</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「喜歡躲在你搖曳的髮」為藍心湄作詞，「跟著夢想飛行」為藍心湄作曲。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>13</strong></p></td>
<td style="text-align: center;"><p><strong>一見鍾情</strong></p></td>
<td style="text-align: center;"><p>1993年7月</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主打歌曲“一見鍾情”改編自<a href="../Page/黎瑞恩.md" title="wikilink">黎瑞恩的</a>“一人有一個夢想”</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>14</strong></p></td>
<td style="text-align: center;"><p><strong>愛我到今生</strong></p></td>
<td style="text-align: center;"><p>1994年9月</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「心慌意亂」、「隨心所欲」、「忘記」為藍心湄作詞。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>15</strong></p></td>
<td style="text-align: center;"><p><strong>夜長夢多</strong></p></td>
<td style="text-align: center;"><p>1995年4月</p></td>
<td style="text-align: center;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「愛你愛我愛誰都沒錯」為藍心湄作詞。<br />
「三心二意」獲channel V 華語榜中榜年度二十歌曲。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>16</strong></p></td>
<td style="text-align: center;"><p><strong>愛走了</strong></p></td>
<td style="text-align: center;"><p>1996年9月16日</p></td>
<td style="text-align: center;"><p><a href="../Page/台灣索尼音樂娛樂.md" title="wikilink">新力唱片</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>藍心湄擔任「S.P.P」單曲製作人。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>17</strong></p></td>
<td style="text-align: center;"><p><strong>糖果</strong></p></td>
<td style="text-align: center;"><p>1997年6月9日</p></td>
<td style="text-align: center;"><p>新力唱片</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>18</strong></p></td>
<td style="text-align: center;"><p><strong>肉餅飯糰</strong></p></td>
<td style="text-align: center;"><p>1998年5月12日</p></td>
<td style="text-align: center;"><p>新力唱片</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>入圍<a href="../Page/金曲獎.md" title="wikilink">金曲獎最佳專輯製作人</a>(<a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a>)。同名單曲由周軒作詞，黃韻玲、藍心湄共同譜曲。<br />
獲<a href="../Page/中國時報.md" title="wikilink">中國時報年度十大專輯票選</a>、<a href="../Page/飛碟電台.md" title="wikilink">飛碟電台勁碟民選大賞五月之星金獎</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>19</strong></p></td>
<td style="text-align: center;"><p><strong>心湄看新湄(Brand New Pauline From Pauline Eyes)</strong></p></td>
<td style="text-align: center;"><p>1999年4月8日</p></td>
<td style="text-align: center;"><p>新力唱片</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「亂糟糟」為藍心湄詞曲創作；「新鮮」為藍心湄作詞。<br />
「你的電話」獲<a href="../Page/Hit_FM.md" title="wikilink">Hit FM音樂大獎DJ最愛歌曲</a>、飛碟電台勁碟民選大賞合唱歌曲金獎<br />
獲飛碟電台勁碟民選大賞四月之星銀獎</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>20</strong></p></td>
<td style="text-align: center;"><p><strong>你開心了湄？</strong></p></td>
<td style="text-align: center;"><p>2000年2月18日</p></td>
<td style="text-align: center;"><p>新力唱片</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>「春天的貓」作詞者為藍心湄。<br />
「狂奔」入圍<a href="../Page/MTV.md" title="wikilink">MTV音樂大獎最佳中文音樂錄影帶獎</a>。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>21</strong></p></td>
<td style="text-align: center;"><p><strong>LanDIVA</strong></p></td>
<td style="text-align: center;"><p>2012年8月</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>數位單曲</p></td>
</tr>
</tbody>
</table>

### 精選輯

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>專輯</p></th>
<th style="text-align: center;"><p>專輯</p></th>
<th style="text-align: center;"><p>發行日期</p></th>
<th style="text-align: center;"><p>唱片公司</p></th>
<th style="text-align: center;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>1</strong></p></td>
<td style="text-align: center;"><p><strong>精心集</strong></p></td>
<td style="text-align: center;"><p>1987年</p></td>
<td style="text-align: center;"><p><a href="../Page/飛羚唱片.md" title="wikilink">飛羚</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>2</strong></p></td>
<td style="text-align: center;"><p><strong>精彩重播</strong></p></td>
<td style="text-align: center;"><p>1990年</p></td>
<td style="text-align: center;"><p><a href="../Page/飛羚唱片.md" title="wikilink">飛羚</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>3</strong></p></td>
<td style="text-align: center;"><p><strong>我愛心湄十年為證</strong></p></td>
<td style="text-align: center;"><p>1994年12月</p></td>
<td style="text-align: center;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>4</strong></p></td>
<td style="text-align: center;"><p><strong>湄目傳情</strong></p></td>
<td style="text-align: center;"><p>1996年9月12日</p></td>
<td style="text-align: center;"><p><a href="../Page/寶麗金.md" title="wikilink">寶麗金</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>5</strong></p></td>
<td style="text-align: center;"><p><strong>超愛藍心湄super hits 1984－1995千禧超級精選</strong></p></td>
<td style="text-align: center;"><p>2000年3月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/環球唱片_(台灣).md" title="wikilink">環球</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>6</strong></p></td>
<td style="text-align: center;"><p><strong>絕對收藏藍心湄</strong></p></td>
<td style="text-align: center;"><p>2009年3月17日</p></td>
<td style="text-align: center;"><p><a href="../Page/台灣索尼音樂娛樂.md" title="wikilink">索尼</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>7</strong></p></td>
<td style="text-align: center;"><p><strong>百變女王-影音典藏精選</strong></p></td>
<td style="text-align: center;"><p>2012年11月19日</p></td>
<td style="text-align: center;"><p><a href="../Page/台灣索尼音樂娛樂.md" title="wikilink">索尼</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 影音作品

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>專輯</p></th>
<th style="text-align: center;"><p>專輯資料</p></th>
<th style="text-align: center;"><p>發行日期</p></th>
<th style="text-align: center;"><p>唱片公司</p></th>
<th style="text-align: center;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>1</strong></p></td>
<td style="text-align: center;"><p><strong>LanDIVA藍心湄演唱會LIVE DVD+新歌</strong></p></td>
<td style="text-align: center;"><p>2013年10月19日</p></td>
<td style="text-align: center;"><p><a href="../Page/亞神音樂.md" title="wikilink">亞神音樂</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### [卡通主題歌](../Page/卡通.md "wikilink")

  - 中華電視公司《[剛彈勇士](../Page/剛彈勇士.md "wikilink")》

## 主持

### 電視

| 年份                                       | 節目                                                 | 頻道                                                                       | 附註                                                                              |
| ---------------------------------------- | -------------------------------------------------- | ------------------------------------------------------------------------ | ------------------------------------------------------------------------------- |
| 1990年                                    | 周日發燒友                                              | [台視](../Page/台視.md "wikilink")                                           | 藍心湄和[曹啟泰共同主持](../Page/曹啟泰.md "wikilink")，已停播                                    |
| 1991年                                    | [雞蛋碰石頭](../Page/雞蛋碰石頭.md "wikilink")               | [中視](../Page/中視.md "wikilink")                                           | 藍心湄和[巴戈共同主持](../Page/巴戈.md "wikilink")，已停播                                      |
| [今天真好](../Page/今天真好.md "wikilink")       | [中視](../Page/中視.md "wikilink")                     | 已停播                                                                      |                                                                                 |
| 1994年                                    | [金曲龍虎榜](../Page/金曲龍虎榜.md "wikilink")               | [華視](../Page/華視.md "wikilink")                                           | 代班主持，已停播                                                                        |
| 1995年                                    | [黃金綜藝通](../Page/黃金綜藝通.md "wikilink")               | [華視](../Page/華視.md "wikilink")                                           | 與[謝祖武](../Page/謝祖武.md "wikilink")、[王中平共同主持](../Page/王中平.md "wikilink")，已停播      |
| [綜藝大聯盟](../Page/綜藝大聯盟.md "wikilink")     | [華視](../Page/華視.md "wikilink")                     | 與[胡瓜](../Page/胡瓜.md "wikilink")、[方芳芳共同主持](../Page/方芳芳.md "wikilink")，已停播 |                                                                                 |
| 1996年                                    | [冠軍家庭TV秀](../Page/冠軍家庭TV秀.md "wikilink")           | [中視](../Page/中視.md "wikilink")                                           | 已停播                                                                             |
| [聰明孩子王](../Page/聰明孩子王.md "wikilink")     | [台視](../Page/台視.md "wikilink")                     | 已停播。此節目獲得[第32屆金鐘獎電視金鐘獎最佳兒童節目主持人](../Page/第32屆金鐘獎.md "wikilink")          |                                                                                 |
| [新雞蛋碰石頭](../Page/新雞蛋碰石頭.md "wikilink")   | [衛視中文台](../Page/衛視中文台.md "wikilink")               | 與[巴戈共同主持](../Page/巴戈.md "wikilink")，已停播                                  |                                                                                 |
| 1997年                                    | [藍心湄Show Time](../Page/藍心湄Show_Time.md "wikilink") | [中視二台](../Page/中視二台.md "wikilink")                                       | 已停播                                                                             |
| [明星擺龍門](../Page/明星擺龍門.md "wikilink")     | [中視二台](../Page/中視二台.md "wikilink")                 | 已停播                                                                      |                                                                                 |
| 1998年                                    | [超級東西軍](../Page/超級東西軍.md "wikilink")               | [中視](../Page/中視.md "wikilink")                                           | 前期與[董至成主持](../Page/董至成.md "wikilink")，後期與[吳宗憲主持](../Page/吳宗憲.md "wikilink")，已停播 |
| [冠軍家庭TV秀](../Page/冠軍家庭TV秀.md "wikilink") | [中視](../Page/中視.md "wikilink")                     | [黃子佼加入主持](../Page/黃子佼.md "wikilink")，已停播                                 |                                                                                 |
| 2000年                                    | [超級大富翁](../Page/超級大富翁.md "wikilink")               | [台視](../Page/台視.md "wikilink")                                           | 與[黃子佼共同主持](../Page/黃子佼.md "wikilink")，已停播                                       |
| [冠軍媽媽樂](../Page/冠軍媽媽樂.md "wikilink")     | [中視](../Page/中視.md "wikilink")                     | 已停播                                                                      |                                                                                 |
| 2001年                                    | [台灣大冒險](../Page/台灣大冒險.md "wikilink")               | [台視](../Page/台視.md "wikilink")                                           | 與[黃子佼共同主持](../Page/黃子佼.md "wikilink")，已停播                                       |
| [港都金曲](../Page/港都金曲.md "wikilink")       | [ET Jacky](../Page/ET_Jacky.md "wikilink")         | 已停播                                                                      |                                                                                 |
| [娛樂心新聞](../Page/娛樂心新聞.md "wikilink")     | [ET Jacky](../Page/ET_Jacky.md "wikilink")         | 已停播                                                                      |                                                                                 |
| 2002年                                    | [天天大精彩](../Page/天天大精彩.md "wikilink")               | [緯來綜合台](../Page/緯來綜合台.md "wikilink")                                     | 與[庹宗康共同主持](../Page/庹宗康.md "wikilink")，已停播                                       |
| [台灣風雲榜](../Page/台灣風雲榜.md "wikilink")     | [台視](../Page/台視.md "wikilink")                     | 與[黃嘉千共同主持](../Page/黃嘉千.md "wikilink")，已停播                                |                                                                                 |
| [星星大閱兵](../Page/星星大閱兵.md "wikilink")     | [緯來綜合台](../Page/緯來綜合台.md "wikilink")               | 與[庹宗康共同主持](../Page/庹宗康.md "wikilink")，已停播                                |                                                                                 |
| [寶島模仿王](../Page/寶島模仿王.md "wikilink")     | [緯來綜合台](../Page/緯來綜合台.md "wikilink")               | 擔任評審，已停播                                                                 |                                                                                 |
| 2003年至今                                  | [女人我最大](../Page/女人我最大.md "wikilink")               | [TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink")                                 | 前期與[九孔搭檔主持](../Page/九孔.md "wikilink")，後期改為獨自一人主持。                               |
| 2007年                                    | [恐怖名人榜](../Page/恐怖名人榜.md "wikilink")               | [八大綜合台](../Page/八大綜合台.md "wikilink")                                     | 播至2008年3月30日                                                                    |
| 2008年                                    | [娛樂大發現](../Page/娛樂大發現.md "wikilink")               | [民視](../Page/民視.md "wikilink")                                           | 與[2moro共同主持](../Page/2moro.md "wikilink")，播至2009年1月31日                          |
| [超級大滿貫](../Page/超級大滿貫.md "wikilink")     | [東森綜合台](../Page/東森綜合台.md "wikilink")               | 與[小鍾共同主持](../Page/小鍾.md "wikilink")，播至12月27日                             |                                                                                 |
| [冠軍家庭美食秀](../Page/冠軍家庭美食秀.md "wikilink") | [東森綜合台](../Page/東森綜合台.md "wikilink")               | 與[小鍾共同主持](../Page/小鍾.md "wikilink")，播至10月23日                             |                                                                                 |
| 2009年                                    | [星空不夜城](../Page/星空不夜城.md "wikilink")               | [衛視中文台](../Page/衛視中文台.md "wikilink")                                     | 7月15日至9月17日                                                                     |
| 2010年                                    | [金曲超級星](../Page/金曲超級星.md "wikilink")               | [中視](../Page/中視.md "wikilink")                                           | 1月3日至8月1日                                                                       |
|                                          |                                                    |                                                                          |                                                                                 |

### 廣播

| 年份 | 頻道                                 | 節目      | 附註 |
| -- | ---------------------------------- | ------- | -- |
|    | [台北之音](../Page/台北之音.md "wikilink") | 《台北新樂園》 |    |
|    | [飛碟電台](../Page/飛碟電台.md "wikilink") | 《冠軍笑花》  |    |
|    | 《週末一夜情》                            |         |    |
|    |                                    |         |    |

### 頒獎典禮主持

  - 金曲獎

| 年份    | 節目                                         | 頻道                             | 備註                               |
| ----- | ------------------------------------------ | ------------------------------ | -------------------------------- |
| 1994年 | [第6屆金曲獎頒獎典禮](../Page/第6屆金曲獎.md "wikilink") | [中視](../Page/中視.md "wikilink") | 搭檔[巴戈](../Page/巴戈.md "wikilink") |
|       |                                            |                                |                                  |

  - 金馬獎

| 年份    | 節目                                           | 頻道                             | 備註                                     |
| ----- | -------------------------------------------- | ------------------------------ | -------------------------------------- |
| 2007年 | [第44屆金馬獎頒獎典禮](../Page/第44屆金馬獎.md "wikilink") | [衛視](../Page/衛視.md "wikilink") | 搭檔[陳建州](../Page/陳建州.md "wikilink")、周瑛琦 |
|       |                                              |                                |                                        |

## 影視作品

### 電視劇

|      |                                                                                                              |                                                        |        |       |
| ---- | ------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------ | ------ | ----- |
| 年代   | 頻道                                                                                                           | 劇名                                                     | 飾演     | 性質    |
| 1988 | [華視](../Page/華視.md "wikilink")                                                                               | 《[追妻三人行](../Page/追妻三人行.md "wikilink")》                 | 林美好    |       |
| 1990 | 中視                                                                                                           | 《[親愛的，我把英雄變美人啦！](../Page/親愛的，我把英雄變美人啦！.md "wikilink")》 |        |       |
| 1990 | [華視](../Page/華視.md "wikilink")                                                                               | 《[追妻三人行大運](../Page/追妻三人行大運.md "wikilink")》             | 林美好    |       |
| 1993 |                                                                                                              | 《[英雄少年](../Page/英雄少年.md "wikilink")》                   | 葉玲     |       |
| 1993 |                                                                                                              | 《[天才保母](../Page/天才保母.md "wikilink")》                   |        |       |
| 1997 |                                                                                                              | 《[台獨份子](../Page/台獨份子.md "wikilink")》                   | 劉素蘭    | 女主角   |
| 2001 |                                                                                                              | 《[追夫三人行](../Page/追夫三人行.md "wikilink")》                 |        | 客串    |
| 2003 | [華視](../Page/華視.md "wikilink")                                                                               | 《[安室愛美惠](../Page/安室愛美惠.md "wikilink")》                 | 美惠好友   | 客串    |
| 2006 | [東森綜合台](../Page/東森綜合台.md "wikilink")                                                                         | 《[麻辣一家親](../Page/麻辣一家親.md "wikilink")》                 | 江曾滿足   |       |
| 2007 | [八大綜合台](../Page/八大綜合台.md "wikilink")                                                                         | 《[終極一家](../Page/終極一家.md "wikilink")》                   | 夏蘭荇德．雄 | 配角    |
| 2009 | [民視](../Page/民視.md "wikilink")                                                                               | 《[夜市人生](../Page/夜市人生_\(2009年電視劇\).md "wikilink")》      | 田咪咪    | 配角    |
| 2014 | [八大綜合台](../Page/八大綜合台.md "wikilink")                                                                         | 《[終極X宿舍](../Page/終極X宿舍.md "wikilink")》                 | 夏蘭荇德．雄 | 第二女主角 |
| 2016 | [中視](../Page/中視.md "wikilink")                                                                               | 《[原來1家人](../Page/原來1家人.md "wikilink")》                 | Jolin  | 主演    |
| 2017 | [中視](../Page/中視.md "wikilink")                                                                               | 《[稍息立正我愛你](../Page/稍息立正我愛你.md "wikilink")》             | 呂美花    | 客串    |
| 2018 | [TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink")                                                                     | 《[女兵日記](../Page/女兵日記.md "wikilink")》                   | 藍月湄    | 客串    |
| 2019 | [台視](../Page/台視.md "wikilink")、[新傳媒8頻道](../Page/新傳媒8頻道.md "wikilink")、[新傳媒U頻道](../Page/新傳媒U頻道.md "wikilink") | 《[你那邊怎樣·我這邊OK](../Page/你那邊怎樣·我這邊OK.md "wikilink")》     | 甄蒂     | 配角    |

### 電影

|       |                                            |     |                                                                                                                                            |
| ----- | ------------------------------------------ | --- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| 年份    | 片名                                         | 飾演  | 合作演員                                                                                                                                       |
| 1985年 | 《[台北神話](../Page/台北神話.md "wikilink")》       |     |                                                                                                                                            |
| 1986年 | 《[頑皮家族](../Page/頑皮家族.md "wikilink")》       |     |                                                                                                                                            |
| 1986年 | 《[歡樂龍虎榜](../Page/歡樂龍虎榜.md "wikilink")》     |     |                                                                                                                                            |
| 1987年 | 《[孫小毛魔界歷險](../Page/孫小毛魔界歷險.md "wikilink")》 |     | 與[曹啟泰](../Page/曹啟泰.md "wikilink")、[倪敏然](../Page/倪敏然.md "wikilink")、夏玲玲、[馬邵君](../Page/馬邵君.md "wikilink")、[紅唇族主演](../Page/紅唇族.md "wikilink") |
| 2014年 | 《[等一個人咖啡](../Page/等一個人咖啡.md "wikilink")》   | 金刀嫂 | 與[李㼈](../Page/李㼈.md "wikilink")、[宋芸樺](../Page/宋芸樺.md "wikilink")、[布魯斯主演](../Page/布魯斯_\(藝人\).md "wikilink")                                 |
| 2016年 | 《[時間差](../Page/時間差.md "wikilink")》         |     | 與[郭采潔](../Page/郭采潔.md "wikilink")、[李東學](../Page/李東學.md "wikilink")、[鄭愷主演](../Page/鄭愷.md "wikilink")                                        |
|       |                                            |     |                                                                                                                                            |

### MV

|         |                                        |     |
| ------- | -------------------------------------- | --- |
| 年代      | 演唱者《歌曲》                                | 角色  |
| 2015年6月 | [彭佳慧](../Page/彭佳慧.md "wikilink")《大齡女子》 | 女主角 |

## 廣告作品

  - [溫娣漢堡](../Page/溫娣漢堡.md "wikilink")
  - [華元食品](../Page/華元食品.md "wikilink")「華元鹹蔬餅」
  - [台灣比雅久](../Page/台灣比雅久.md "wikilink")「金祥瑞50」機車
  - [金車公司](../Page/金車公司.md "wikilink")「金車紐約咖啡」
  - [台三工業](../Page/台三工業.md "wikilink")「三和牌衣櫥」
  - [耐斯企業](../Page/耐斯企業.md "wikilink")「白鴿冷洗精」
  - [最佳女主角國際美容機構](../Page/最佳女主角國際美容機構.md "wikilink")
  - [統一百事](../Page/統一百事.md "wikilink")（今[台灣百事食品](../Page/台灣百事食品.md "wikilink")）「話匣子」餅乾
  - [美之本國際](../Page/美之本國際.md "wikilink")「京城之霜」系列保養品-2017
  - [台灣大哥大](../Page/台灣大哥大.md "wikilink")（與[蕭敬騰合作](../Page/蕭敬騰.md "wikilink")）-2018
  - TVBS「享食尚滴雞精」-2018
  - [白蘭氏](../Page/白蘭氏.md "wikilink")「保捷膠原錠」-2018

## 獎項

### 電視金鐘獎

|- | 1997 | 《[聰明孩子王](../Page/聰明孩子王.md "wikilink")》 |
[第32屆金鐘獎](../Page/第32屆金鐘獎.md "wikilink")
[兒童節目主持人獎](../Page/金鐘獎.md "wikilink") |

### 星和無綫電視大獎

|- | 2003 | 《女人我最大》 | [星和無綫電視大獎](../Page/星和無綫電視大獎.md "wikilink")
我最愛TVB綜藝節目主持人 |

## 参考資料

## 外部連結

  - [KiKi餐廳](http://www.kiki1991.com/main/tw/)

  -
  -
  -
  -
  -
  -
  -
[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Category:台灣電影女演員](../Category/台灣電影女演員.md "wikilink")
[Category:台灣綜藝節目主持人](../Category/台灣綜藝節目主持人.md "wikilink")
[Category:金鐘獎兒童少年電視節目主持人得主](../Category/金鐘獎兒童少年電視節目主持人得主.md "wikilink")
[Category:聖心女子高級中學校友](../Category/聖心女子高級中學校友.md "wikilink")
[Category:臺北市立蘭雅國民中學校友](../Category/臺北市立蘭雅國民中學校友.md "wikilink")
[Category:西螺人](../Category/西螺人.md "wikilink")
[X心](../Category/藍姓.md "wikilink")

1.  [LanDIVA 2012
    藍心湄演唱會](http://www.arena.taipei/ct.asp?xItem=47286333&ctNode=62914&mp=12203a)
2.  葉文正.[心湄怨姚文智
    沒放馬來追](http://ent.ltn.com.tw/news/paper/53918).自由時報.2006-01-19