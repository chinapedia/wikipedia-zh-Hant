**藍正龍**（****，），[台灣](../Page/台灣.md "wikilink")[男藝人](../Page/男藝人.md "wikilink")，出生於[臺灣省](../Page/臺灣省.md "wikilink")[宜蘭縣](../Page/宜蘭縣.md "wikilink")，永平國小、漳和國中\[1\]\[2\]、[華夏工專](../Page/華夏工專.md "wikilink")[機械科畢業](../Page/機械工程.md "wikilink")。因演出[可口可樂廣告而受矚目](../Page/可口可樂.md "wikilink")。2000年參演[王小棣導演的](../Page/王小棣.md "wikilink")《大醫院小醫師》成名，之後成為影視一線男演員。2008年與[林佑威參演](../Page/林佑威.md "wikilink")[王小棣導演的](../Page/王小棣.md "wikilink")《[波麗士大人](../Page/波麗士大人.md "wikilink")》精采演出，卻未獲評審青睞，雙男主角均未獲提名入圍，[王小棣導演為藍正龍與](../Page/王小棣.md "wikilink")[林佑威轟新聞局](../Page/林佑威.md "wikilink")\[3\]。2015年憑藉《[妹妹](../Page/妹妹_\(電視劇\).md "wikilink")》戴耀起一角，成功奪下[第50屆金鐘獎](../Page/第50屆金鐘獎.md "wikilink")[戲劇節目男主角獎](../Page/金鐘獎戲劇節目男主角獎得獎列表.md "wikilink")\[4\]。

## 個人生活

  - 因[扁平足僅當](../Page/扁平足.md "wikilink")45天國民兵。
  - 2014年5月17日與女演員[周幼婷在永和戶政事務所辦理登記結婚](../Page/周幼婷.md "wikilink")。\[5\]
  - 2015年5月16日長女小籃球誕生。
  - 2016年10月5日長子藍小弟誕生。

## 演出作品

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
<td><p>劇名</p></td>
<td><p>角色</p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p><a href="../Page/大醫院小醫師.md" title="wikilink">大醫院小醫師</a></p></td>
<td><p>楊格</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/麻辣鲜師.md" title="wikilink">麻辣鲜師</a></p></td>
<td><p>藍星龍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/多桑與红玫瑰.md" title="wikilink">多桑與红玫瑰</a></p></td>
<td><p>程秉華</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/活得像個人樣.md" title="wikilink">活得像個人樣</a></p></td>
<td><p>未知</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/流星花園_(台灣電視劇).md" title="wikilink">流星花園</a></p></td>
<td><p>亞門</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/追夫三人行.md" title="wikilink">追夫三人行</a></p></td>
<td><p>林天星</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛情大魔咒_(電視劇).md" title="wikilink">愛情大魔咒</a></p></td>
<td><p>黎濤</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/赴宴.md" title="wikilink">赴宴</a></p></td>
<td><p>郭民峰</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鬥魚_(2004年電視劇).md" title="wikilink">鬥魚</a></p></td>
<td><p>單立傑</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/求婚事務所.md" title="wikilink">求婚事務所</a>《情書》</p></td>
<td><p>謝以樹</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/家有菲菲.md" title="wikilink">家有菲菲</a></p></td>
<td><p>徐少緯</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/火線任務.md" title="wikilink">火線任務</a></p></td>
<td><p>唐漢生</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冷鋒過境.md" title="wikilink">冷鋒過境</a></p></td>
<td><p>小龍（司機）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中華英雄.md" title="wikilink">中華英雄</a></p></td>
<td><p>慕西</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/45度C天空下.md" title="wikilink">45度C天空下</a></p></td>
<td><p>林宇誠</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金色年華.md" title="wikilink">金色年華</a></p></td>
<td><p>成子華</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/永遠的銘記.md" title="wikilink">永遠的銘記</a></p></td>
<td><p>金南植</p></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/危險心靈_(電視劇).md" title="wikilink">危險心靈</a></p></td>
<td><p>勞倫斯</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/北極光_(電視劇).md" title="wikilink">北極光</a></p></td>
<td><p>宋懷恩</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/波麗士大人.md" title="wikilink">波麗士大人</a></p></td>
<td><p>劉國昌（少年）<br />
劉漢強</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/幸福的抉擇.md" title="wikilink">幸福的抉擇</a></p></td>
<td><p>李浩哲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/福氣又安康.md" title="wikilink">福氣又安康</a></p></td>
<td><p>嚴旺財（青年）<br />
嚴大風</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日落之前愛上你.md" title="wikilink">日落之前愛上你</a></p></td>
<td><p>張世令</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/絲絲心動.md" title="wikilink">絲絲心動</a></p></td>
<td><p>歐陽晨</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/偷心大聖PS男.md" title="wikilink">偷心大聖PS男</a></p></td>
<td><p>夏和-{杰}-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/女王不下班.md" title="wikilink">女王不下班</a></p></td>
<td><p>吳天良</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/粉愛粉愛你.md" title="wikilink">粉愛粉愛你</a></p></td>
<td><p>常宇傑</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/幸福三顆星.md" title="wikilink">幸福三顆星</a></p></td>
<td><p>安少成</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/流氓蛋糕店_(台灣電視劇).md" title="wikilink">流氓蛋糕店</a></p></td>
<td><p>秦是吾</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/妹妹_(電視劇).md" title="wikilink">妹妹</a></p></td>
<td><p>戴耀起</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/植劇場.md" title="wikilink">植劇場</a>《姜老師，妳談過戀愛嗎》</p></td>
<td><p>陳威霖</p></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p><a href="../Page/麻醉風暴2.md" title="wikilink">麻醉風暴2</a></p></td>
<td><p>外科部主任 謝騰豐</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逃婚一百次.md" title="wikilink">逃婚一百次</a></p></td>
<td><p>酒保</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p><a href="../Page/前男友不是人.md" title="wikilink">前男友不是人</a></p></td>
<td><p>戴海安</p></td>
</tr>
<tr class="even">
<td><p>2019</p></td>
<td><p><a href="../Page/你那邊怎樣·我這邊OK.md" title="wikilink">你那邊怎樣·我這邊OK</a></p></td>
<td><p>郭豪森</p></td>
</tr>
</tbody>
</table>

### 電影

|                                                  |                                                |                                        |                                                                     |
| ------------------------------------------------ | ---------------------------------------------- | -------------------------------------- | ------------------------------------------------------------------- |
| 上映年份                                             | 電影名稱                                           | 飾演                                     | 導演                                                                  |
| 1999年                                            | [新賭國仇城](../Page/新賭國仇城.md "wikilink")           | 小朱                                     | [葉鴻偉](../Page/葉鴻偉.md "wikilink")                                    |
| 2004年                                            | [戀人](../Page/戀人_\(電影\).md "wikilink")          | 阿倫                                     | [王明台](../Page/王明台.md "wikilink")                                    |
| 2006年                                            | [十日天堂](../Page/十日天堂.md "wikilink")             | 王聰                                     |                                                                     |
| 2007年                                            | [神選者](../Page/神選者.md "wikilink")               | 大學助教                                   |                                                                     |
| 2008年                                            | [愛到底](../Page/愛到底.md "wikilink")-華山24          | Vincent                                | [方文山](../Page/方文山.md "wikilink")                                    |
| 2010年                                            | [酷馬](../Page/酷馬.md "wikilink")                 | 劉教練                                    | [王小棣](../Page/王小棣.md "wikilink")                                    |
| 2011年                                            | [雞排英雄](../Page/雞排英雄.md "wikilink")             | 陳一華(阿華)                                | [葉天倫](../Page/葉天倫.md "wikilink")                                    |
| [巨额交易](../Page/巨额交易.md "wikilink")               | 張澤                                             | 馬儷文                                    |                                                                     |
| [電哪吒](../Page/電哪吒.md "wikilink")                 | 阿豪                                             | [李運傑](../Page/李運傑.md "wikilink")(兼任監製) |                                                                     |
| [飲食男女2：好遠又好近](../Page/飲食男女2：好遠又好近.md "wikilink") | 張全                                             | [曹瑞原](../Page/曹瑞原.md "wikilink")       |                                                                     |
| 2013年                                            | [阿嬤的夢中情人](../Page/阿嬤的夢中情人.md "wikilink")       | 劉奇生                                    | [北村豐晴](../Page/北村豐晴.md "wikilink")、[蕭力修](../Page/蕭力修.md "wikilink") |
| [再見北陸](../Page/再見北陸.md "wikilink")               | Blue                                           |                                        |                                                                     |
| 2015年                                            | [大囍臨門](../Page/大囍臨門.md "wikilink")             | 機場人員                                   | [黃朝亮](../Page/黃朝亮.md "wikilink")                                    |
| 2017年                                            | [大釣哥](../Page/大釣哥.md "wikilink")               | 藍小龍                                    | [黃朝亮](../Page/黃朝亮.md "wikilink")                                    |
| [神秘家族](../Page/神秘家族.md "wikilink")               | 黑衣人                                            | [朴裕焕](../Page/朴裕焕.md "wikilink")       |                                                                     |
| 2018年                                            | [殺無赦](../Page/殺無赦\(網絡電影\).md "wikilink")(網絡電影) |                                        |                                                                     |
| 2018年                                            | [鬥魚](../Page/鬥魚_\(電影\).md "wikilink")          | 成年的單立傑                                 | 柯翰辰、胡寧遠                                                             |
|                                                  |                                                |                                        |                                                                     |

### 音樂錄影帶

  - 1999年 [劉虹嬅](../Page/劉虹嬅.md "wikilink")《清晨五點\[6\]》
  - 1999年 [丁小芹](../Page/丁小芹.md "wikilink")《說謊\[7\]》
  - 2001年 [林凡](../Page/林凡.md "wikilink")《都是他\[8\]》
  - 2003年 [鄭秀文](../Page/鄭秀文.md "wikilink")《美麗的誤會\[9\]》
  - 2004年 [張宇](../Page/張宇.md "wikilink")《毀類\[10\]》
  - 2008年 [丁噹](../Page/丁噹.md "wikilink")《我愛上的\[11\]》　
  - 2013年 [路嘉欣](../Page/路嘉欣.md "wikilink") featuring 藍正龍《幸福 前進吧！\[12\]》
  - 2015年 [王心凌](../Page/王心凌.md "wikilink")《遠在眼前的你\[13\]》

## 音樂作品

### 專輯

  - 2001年《軟心腸》大醫院小醫師CD有聲書

### 單曲

  - 2009年《愛情黑洞》《[波麗士大人](../Page/波麗士大人.md "wikilink")》電視原聲帶
  - 2013年 路嘉欣 featuring 藍正龍《幸福 前進吧！》(日本觀光廳旅遊微電影主題曲)

## 獎項紀錄

<table style="width:146%;">
<colgroup>
<col style="width: 72%" />
<col style="width: 16%" />
<col style="width: 17%" />
<col style="width: 19%" />
<col style="width: 11%" />
<col style="width: 9%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>影片</p></th>
<th><p>角色</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/第39屆金鐘獎.md" title="wikilink">第39屆金鐘獎</a></p></td>
<td><p><a href="../Page/金鐘獎戲劇節目男主角獎得獎列表.md" title="wikilink">戲劇節目男主角獎</a></p></td>
<td><p>《<a href="../Page/冷鋒過境.md" title="wikilink">冷鋒過境</a>》</p></td>
<td><p>小龍</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/第49屆金鐘獎.md" title="wikilink">第49屆金鐘獎</a></p></td>
<td><p>《<a href="../Page/流氓蛋糕店_(台灣電視劇).md" title="wikilink">流氓蛋糕店</a>》</p></td>
<td><p>秦是吾</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/第50屆金鐘獎.md" title="wikilink">第50屆金鐘獎</a></p></td>
<td><p>《<a href="../Page/妹妹_(電視劇).md" title="wikilink">妹妹</a>》</p></td>
<td><p>戴耀起</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/第52屆金鐘獎.md" title="wikilink">第52屆金鐘獎</a></p></td>
<td><p><a href="../Page/植劇場.md" title="wikilink">植劇場</a>－《姜老師，你談過戀愛嗎》</p></td>
<td><p>陳威霖</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考來源

## 外部連結

  -
  -
  -
  - [B L U E ‧ L A N](http://bluelan31.pixnet.net/blog/)

  -
  -
  -
  -
[category:台灣男性模特兒](../Page/category:台灣男性模特兒.md "wikilink")

[Category:臺灣電影男演員](../Category/臺灣電影男演員.md "wikilink")
[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:華夏技術學院校友](../Category/華夏技術學院校友.md "wikilink")
[Category:礁溪人](../Category/礁溪人.md "wikilink")
[Z](../Category/藍姓.md "wikilink")
[Category:金鐘獎戲劇節目男主角得主](../Category/金鐘獎戲劇節目男主角得主.md "wikilink")

1.  [《明星導遊 故鄉尋星蹤》誤把財神當月老
    張天霖初吻跑去烘爐地](http://blog.sina.com.cn/s/blog_554285420100036q.html)
2.  [《藍正龍發情小公牛
    漳和國中孵初戀》蘋果日報](http://www.appledaily.com.tw/appledaily/article/entertainment/20121007/34557889/)
3.  [王小棣愛將藍正龍金鐘摃龜
    轟新聞局要交代](http://tw.nextmedia.com/applenews/article/art_id/31935206/IssueID/20090912)
4.  [藍正龍金鐘獎稱帝
    感性謝周幼婷](http://www.cna.com.tw/news/firstnews/201509265027-1.aspx)中央社
5.  [藍正龍周幼婷結婚
    戀3年成正果](http://www.cna.com.tw/news/firstnews/201405170149-1.aspx)
    中央社，2014年5月17日
6.  [清晨五點](https://www.youtube.com/watch?v=I-sGnEGTA88)
7.  [說謊](http://www.youtube.com/watch?v=MsOq1K6OouE)
8.  [都是他](https://www.youtube.com/watch?v=agXoUnSmNds)
9.  [美麗的誤會](https://www.youtube.com/watch?v=DGfufyi_8uU)
10. [毀類](http://www.youtube.com/watch?v=njnCazRHzZk)
11. [我愛上的](https://www.youtube.com/watch?v=minihAYyl7Y)
12. [幸福 前進吧！](https://www.youtube.com/watch?v=-epCZF7UIj4)
13. [遠在眼前的你](https://www.youtube.com/watch?v=pTQO9AktIV8)