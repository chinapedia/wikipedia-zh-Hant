**藍莓**，狭义是指一群[越橘屬](../Page/越橘屬.md "wikilink")[越橘亚属](../Page/越橘亚属.md "wikilink")**青液果组**（[学名](../Page/学名.md "wikilink")：**）的開花植物，广义上可以包括[越橘屬中长有蓝色浆果的所有物种](../Page/越橘屬.md "wikilink")。這些種的植物原生於[北美洲與](../Page/北美洲.md "wikilink")[東亞](../Page/東亞.md "wikilink")，為[灌木](../Page/灌木.md "wikilink")，高度可從10[公分到](../Page/公分.md "wikilink")4[公尺](../Page/公尺.md "wikilink")；矮小種稱為「矮叢藍莓」（lowbush
blueberries），而高大種稱為「高叢藍莓」（highbush
blueberries）。[葉可為](../Page/葉.md "wikilink")[落葉性或](../Page/落葉性.md "wikilink")[長青性](../Page/長青性.md "wikilink")，葉形[卵圓形到](../Page/卵圓.md "wikilink")，長1到8公分，寬0.5到3.5公分。[花朵為](../Page/花.md "wikilink")[鐘形](../Page/鐘.md "wikilink")，顏色從[白色](../Page/白色.md "wikilink")、[桃色到](../Page/桃色.md "wikilink")[紅色都有](../Page/紅色.md "wikilink")，有時帶有淡淡的綠色調。[果實在](../Page/果實.md "wikilink")[植物學上是](../Page/植物學.md "wikilink")[假果](../Page/假果.md "wikilink")，直徑5到16[公厘](../Page/公厘.md "wikilink")，帶有喇叭形的冠在末端；一開始呈[淺綠色](../Page/淺綠色.md "wikilink")，然後轉為[紅紫色](../Page/紅紫色.md "wikilink")，最後成為[藍色或](../Page/藍色.md "wikilink")[深紫色](../Page/深紫色.md "wikilink")，此時[成熟可以採收](../Page/成熟.md "wikilink")。成熟時帶有甜甜的風味，而[酸度各異](../Page/酸度.md "wikilink")。藍莓的產季從每年[五月到](../Page/五月.md "wikilink")[十月](../Page/十月.md "wikilink")，在[七月達最高峰](../Page/七月.md "wikilink")。

[黑果越橘也叫欧洲蓝莓](../Page/黑果越橘.md "wikilink")，[笃斯越橘也常叫中国野生蓝莓](../Page/笃斯越橘.md "wikilink")，但它们不是狭义上的蓝莓。

## 成分

藍莓不是特別營養密集的（[超級食物的特徵](../Page/超級食物.md "wikilink")\[1\]）它們只含有三種必需營養素：維生素C，維生素K和錳。\[2\]

## 用途

藍莓用在[果凍](../Page/果凍.md "wikilink")、[果醬](../Page/果醬.md "wikilink")、[冰淇淋與](../Page/冰淇淋.md "wikilink")[派等甜點上](../Page/派.md "wikilink")，也會加入[瑪芬中](../Page/瑪芬.md "wikilink")[烘培](../Page/烘培.md "wikilink")。它是許多[點心與佳餚的成分之一](../Page/點心.md "wikilink")。許多烘[培製品會加入人造藍莓調味](../Page/培製品.md "wikilink")，事實上是不含真正藍莓成分。**藍莓果醬**是由藍莓、[糖](../Page/糖.md "wikilink")、[水與](../Page/水.md "wikilink")[果膠所組成的](../Page/果膠.md "wikilink")[果醬](../Page/果醬.md "wikilink")。商品包裝的[果醬通常含有](../Page/果醬.md "wikilink")[化學防腐劑](../Page/化學防腐劑.md "wikilink")，如[檸檬酸](../Page/檸檬酸.md "wikilink")。頂級的藍莓果醬在[加拿大與](../Page/加拿大.md "wikilink")[美國出產](../Page/美國.md "wikilink")，採用野生藍莓；其體型較小，也不易收成，但口感較栽培藍莓更為強烈。主要產地在[美國](../Page/美國.md "wikilink")[緬因州](../Page/緬因州.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")[安大略省西北以及](../Page/安大略省.md "wikilink")[魁北克的](../Page/魁北克.md "wikilink")[圣约翰湖区](../Page/圣约翰湖区.md "wikilink")（Saguenay-Lac-Saint-Jean）。藍莓，尤其是野生種，含有[抗氧化劑](../Page/抗氧化劑.md "wikilink")，可以減低[癌症發生的機會](../Page/癌症.md "wikilink")。藍莓等漿果類植物屬於強力抗氧化水果，能夠幫忙減緩老化、活化腦力、增強記憶力，國外研究發現吃藍莓或草莓不但可以延緩老化，且每周只要吃1/2杯藍莓或是1杯草莓的份量，就可以改善記憶力。在2004年國際長壽會議（International
Conference on
Longevity），一些研究者釋放出一項研究的細節，顯示藍莓中一些特定[化合物](../Page/化合物.md "wikilink")（以及一些相似植物，例如[小紅莓](../Page/小紅莓.md "wikilink")）對於腦功能退化的抑制有強烈影響，相關疾病包括了[阿茲海默症](../Page/阿茲海默症.md "wikilink")。\[3\]\[4\][美國](../Page/美國.md "wikilink")[羅格斯大學的研究者](../Page/羅格斯大學.md "wikilink")\[5\]也展示了藍莓可能可以防止[泌尿道感染](../Page/泌尿道感染.md "wikilink")。40[克新鮮藍莓包含了](../Page/克.md "wikilink")3克的[膳食纖維與](../Page/膳食纖維.md "wikilink")14毫克[維他命C](../Page/維他命C.md "wikilink")。\[6\]由于蓝莓富含花青素[花青素](../Page/花青素.md "wikilink")，具有活化视网膜功效，可以强化视力，防止眼球疲劳而备受注目。也是世界粮农组织推荐的五大健康水果之一。并且据美国、日本、欧洲科学家研究，经常食用蓝莓制品，还可明显地增强视力，消除眼睛疲劳。医学临床报告也显示，蓝莓中的花青素[花青素可以促进视网膜细胞中的视紫质再生](../Page/花青素.md "wikilink")，预防近视，增进视力。以及丰富的[维生素A](../Page/维生素A.md "wikilink")、[维生素E](../Page/维生素E.md "wikilink")、[類胡蘿蔔素](../Page/類胡蘿蔔素.md "wikilink")、[钾和](../Page/钾.md "wikilink")[锌](../Page/锌.md "wikilink")。

## 栽植

### 北美洲

藍莓可分為栽培種與野生種。在[北美洲最常栽培的品種是](../Page/北美洲.md "wikilink")[北方高叢藍莓](../Page/北方高叢藍莓.md "wikilink")（*V.
corymbosum*）。與[越橘屬的其他品種](../Page/越橘屬.md "wikilink")[雜交產生的種類則可以適應美國南部的](../Page/雜交種.md "wikilink")[氣候](../Page/氣候.md "wikilink")，這些種常合稱作**南方高叢藍莓**。

野生藍莓通常較小，價值卻高於栽培藍莓，因其有更強烈的味道與更鮮明的顏色。[矮叢藍莓](../Page/矮叢藍莓.md "wikilink")（*V.
angustifolium*）出現在[紐芬蘭](../Page/紐芬蘭.md "wikilink")，向西、向南直到[密西根州與](../Page/密西根州.md "wikilink")[西維吉尼亞州](../Page/西維吉尼亞州.md "wikilink")。在某些地區有時候會出現一大片天然的藍莓植被，這些天然的藍莓園裡面通常只會生長單一種類的藍莓。在加拿大安大略省已經有數個[第一原住民社區在採收野生的藍莓](../Page/第一民族.md "wikilink")。矮叢藍莓的種類可以忍受野火的侵襲，在森林火災發生過後，藍莓果實的產量常會增加，這是因為藍莓的再生力迅速，而且大火也同時燒掉了一些與藍莓競爭的植物。

在美國，[緬因州是矮叢藍莓最大宗產地](../Page/緬因州.md "wikilink")。緬因州生產的藍莓大約需要五萬箱蜂箱的[蜜蜂](../Page/蜜蜂.md "wikilink")，才能完成藍莓授粉的工作，而其中大部份的蜜蜂蜂箱，都是由別的州用卡車運進來的。

[密西根州](../Page/密西根州.md "wikilink")、[紐約州](../Page/紐約州.md "wikilink")、[紐澤西州與](../Page/紐澤西州.md "wikilink")[北卡羅來納州則為高叢藍莓的大宗產地](../Page/北卡羅來納州.md "wikilink")。

[奧勒岡州](../Page/奧勒岡州.md "wikilink")、[華盛頓州與加拿大](../Page/華盛頓州.md "wikilink")[英屬哥倫比亞目前正發展成藍莓大宗產地](../Page/英屬哥倫比亞.md "wikilink")；而[加州則正快速栽植中](../Page/加州.md "wikilink")。

[兔眼藍莓](../Page/兔眼藍莓.md "wikilink")（*V.
virgatum*）則為一種南方品種的藍莓，產地遍及南、北卡羅萊納州與[墨西哥灣沿岸數州](../Page/墨西哥灣.md "wikilink")。

在北美洲也很重要的其他藍莓種類，包括有原產於美國東部的旱地藍莓，本種在[阿帕拉契和](../Page/阿帕拉契.md "wikilink")[皮埃蒙特非常普遍](../Page/皮埃蒙特.md "wikilink")。原產於美國東南部的[臼莓](../Page/臼莓.md "wikilink")（*V.
arboreum*），在沙質土壤的地區是一種常見的野生藍莓，它的果實可以做為[野生動物的食物](../Page/野生動物.md "wikilink")，花對蜂農而言則是重要的蜜源植物。

### 南美洲

目前[智利是世界第二大蓝莓出口国](../Page/智利.md "wikilink")，仅次於美国。\[7\]

### 亚洲

  - 台灣

[臺灣原先引進藍莓時](../Page/臺灣.md "wikilink")，以[清境農場等高冷地帶為主要種植區域](../Page/清境農場.md "wikilink")，產量不大。而目前在[臺北建國假日花市亦有花販出售藍莓](../Page/台北建國假日花市.md "wikilink")，標示上卻是分為「原生種」與「暖地種」，推斷為北方藍莓種系與南方藍莓種系的別稱。　

  - 中國大陸

[青岛市](../Page/青岛市.md "wikilink")[黄岛区于](../Page/黄岛区.md "wikilink")2001年试种蓝莓成功，现已发展成为全中国大陸种植面积最大、产业化水平最高的蓝莓种苗中心和生产基地，
并于2013年举办了首届青岛国际蓝莓节暨中国国际蓝莓大会。

在[黑龙江北部天然生長着篤斯越橘](../Page/黑龙江.md "wikilink")（但並不屬於青液果组），[大兴安岭地區行政公署所在地](../Page/大兴安岭.md "wikilink")[加格达奇在](../Page/加格达奇.md "wikilink")2009年舉辦了首届國際藍莓山貨節。當地也栽培有歐美品種的藍莓。湖北省襄阳市保康县于2008年试种蓝莓成功，现以发展成为华中地区种植面积最大，蓝莓种苗培育和生产基地。

## 物種

  - [狭叶越橘](../Page/狭叶越橘.md "wikilink") *Vaccinium angustifolium*
    （[矮叢藍莓](../Page/矮叢藍莓.md "wikilink")，Lowbush Blueberry）
  - *Vaccinium boreale*
  - *Vaccinium caesariense*
  - *Vaccinium corymbosum*
    （[北方高叢藍莓](../Page/北方高叢藍莓.md "wikilink")，Northern
    Highbush Blueberry）
  - *Vaccinium darrowii*
  - *Vaccinium elliottii*
  - *Vaccinium formosum*
  - *Vaccinium fuscatum* （[黑果木](../Page/黑果木.md "wikilink")，Black
    highbush blueberry；异名*V. atrococcum* ）
  - *Vaccinium hirsutum*
    （[毛果藍莓](../Page/毛果藍莓.md "wikilink")，Hairy-fruited
    Blueberry）
  - *Vaccinium koreanum*
  - *Vaccinium myrsinites* （[長青藍莓](../Page/長青藍莓.md "wikilink")，Evergreen
    Blueberry）
  - *Vaccinium myrtilloides*
    （[加拿大藍莓](../Page/加拿大藍莓.md "wikilink")，Canadian
    Blueberry）
  - *Vaccinium pallidum* （[旱地藍莓](../Page/旱地藍莓.md "wikilink")，Dryland
    Blueberry）
  - *Vaccinium simulatum*
  - *Vaccinium tenellum* （[南方藍莓](../Page/南方藍莓.md "wikilink")，Southern
    Blueberry）
  - *Vaccinium virgatum* （[兔眼藍莓](../Page/兔眼藍莓.md "wikilink")，Rabbiteye
    Blueberry；异名*V. ashei*）

## 图集

[File:Vaccinium_corymbosum(01).jpg|高叢蓝莓](File:Vaccinium_corymbosum\(01\).jpg%7C高叢蓝莓)
*Vaccinium corymbosum*
[File:Vaccinium_corymbosum_Blauwe_bes_bloemen.jpg|高叢蓝莓](File:Vaccinium_corymbosum_Blauwe_bes_bloemen.jpg%7C高叢蓝莓)

<File:PattsBlueberries.jpg>
[File:Maturing_blueberry.jpg|成熟的北极星蓝莓](File:Maturing_blueberry.jpg%7C成熟的北极星蓝莓)（*Vaccinium*
'Polaris'） <File:Vaccinium_corymbosum0.jpg>
<File:Vaccinium_corymbosum_Strauch.jpg> <File:Blauwe_bes_struik.jpg>

## 参考文献

### 引用

### 来源

  - Joseph, J.A., Shukitt-Hale B., Denisova, N.A. Bielinksi D., Martin,
    A., McEwen, J.J., & Bickford, P.C., 1999. Reversals of age-related
    declines in neuronal signal transduction, cognitive, and motor
    behavioral deficits with blueberry, spinach, or strawberry dietary
    supplementation. *Journal of Neuroscience* 19 (18): 8114–8121.

## 外部連結

  - [美國藍莓協會](https://web.archive.org/web/20140502032759/http://www.blueberry.org/china/)
    （中文）
  - [Complete nutritional
    info.](http://www.nutritiondata.com/foods-blueberr009000000000000000000.html)
  - [The World's Healthiest
    Foods.](http://www.whfoods.com/genpage.php?tname=foodspice&dbid=8)

[fa:قره قات](../Page/fa:قره_قات.md "wikilink")

[Category:越橘屬](../Category/越橘屬.md "wikilink")
[Category:水果](../Category/水果.md "wikilink")
[Category:健康食品](../Category/健康食品.md "wikilink")

1.  <https://www.eufic.org/en/healthy-living/article/the-science-behind-superfoods-are-they-really-super>
2.  <https://nutritiondata.self.com/facts/fruits-and-fruit-juices/1851/2>
3.  [Alzheimer's Disease Education & Referral
    Center](http://www.alzheimers.org/nianews/nianews23.html)
4.  [Blueberry
    brainfood](http://www.abc.net.au/rn/talks/natint/stories/s1059896.htm)

5.  [Blueberry and Cranberry
    Research](http://aesop.rutgers.edu/~bluecran/medicinalgeneralinfopage.htm),
    Rutgers - New Jersey Agricultural Experiment Station
6.  [Blueberry Health](http://www.suffolkblues.co.uk/health.html) ,
    Suffork Blues
7.