《**藍蘭島漂流記**》（）是[藤代健的一部](../Page/藤代健.md "wikilink")[愛情](../Page/愛情.md "wikilink")[喜劇](../Page/喜劇.md "wikilink")[漫畫作品](../Page/漫畫.md "wikilink")，通常簡稱《**藍蘭島**》。臺灣及香港中文版由[東立出版社代理](../Page/東立出版社.md "wikilink")。

## 概要

2002年1月首次以《》為名，在[Square
Enix的雜誌](../Page/Square_Enix.md "wikilink")《[月刊少年GANGAN](../Page/月刊少年GANGAN.md "wikilink")》上刊載；同年6月，開始連載。同時，在作為該雜誌增刊號的《Gangan
Power》（）季刊上也從2002年秋季號開始連載。

每一話的副標題（除TV外），全部都是以「」結束（TV中，則是以「」作結束）。

在作品初期並沒有詳細交代[時間的流逝](../Page/時間.md "wikilink")（曾於原作漫畫第9話中，交代了行人已漂流至島上10天、離家出走20天）。不過在連載開始約4年後的第50話中，交代了行人漂流藍蘭島已有經過三個月（另外，之前亦有交代迷路的小忍用了「三個月」時間才回到家中）。而行人的妹妹美咲的[生日是於](../Page/生日.md "wikilink")5月（為原作[漫畫第](../Page/漫畫.md "wikilink")10話當天），保守估計，行人在漂流至藍蘭島的時間為5月中旬（15日\~20日左右）。同時，亦不清楚島上是正在使用[舊曆或是](../Page/舊曆.md "wikilink")[新曆](../Page/新曆.md "wikilink")。而村中的長老阿婆（阿琴）是出生於[安政元年](../Page/安政.md "wikilink")（[西曆](../Page/西曆.md "wikilink")1854年），現在為148歲，因此可推算出作品中的「現在」為[西曆](../Page/西曆.md "wikilink")2002年。實際上2002年的「葉之月」（8月）的滿月是於23日，因此可推斷原作[漫畫中第](../Page/漫畫.md "wikilink")54\~56話為當天。而第40\~42話（紅夜叉騷動）中，當天亦同樣是滿月，因而估計當天為7月24日或是6月25日（恐怕是前者）。另外，作品中已交代了除美咲以外的女主角們的[生日](../Page/生日.md "wikilink")，但到現時為止並沒有描寫他們為[生日慶祝](../Page/生日.md "wikilink")（截至11冊現在）。

[廣播劇CD和小說版等](../Page/廣播劇CD.md "wikilink")[跨媒體製作也隨著漫畫的熱賣而展開](../Page/跨媒體製作.md "wikilink")。其後作品亦動畫化，並於2007年4月至9月播放全26話。

## 故事簡介

與父親吵架憤而離家出走的14歲男主角**東方院行人**，旅途中從所乘的船上跌落海中，遇到百年一次的超級暴風雨，經過了數日的漂流後，行人漂流到一個名為**藍蘭島**的小島上。由於島的四周佈滿危險的漩渦，無法離開的行人只好暫時住在島上。然而，除了行人外，這座島上只有女性……

與個性豐富的女生相處，行人害羞卻又有趣的新生活，就這樣開始了。

## 登場人物

## 行人所帶的行李(第一卷)

  - [Game Boy
    Advance](../Page/Game_Boy_Advance.md "wikilink")（電視版為[NDS](../Page/NDS.md "wikilink")）
    不過已沒有[電池](../Page/電池.md "wikilink")，無法使用。
  - [腳踏車](../Page/腳踏車.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")
    因浸在[海水而變得皺皺了](../Page/海水.md "wikilink")。
  - [漫畫](../Page/漫畫.md "wikilink")
    似乎是《[鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")》？不過也變得皺皺的。暫時確認的是第6話第1卷及第10話第2卷。
  - [行動電話](../Page/行動電話.md "wikilink")
    在島上收不到[訊號](../Page/訊號.md "wikilink")，無法使用。
  - [數學課本](../Page/數學.md "wikilink")
    是行人急於出門而放在背包內的。目前確認是中學二年級的數學教科書。
  - 練習服
    一直放在行人的背包內，直至與小忍決鬥才拿出來使用。
  - 美咲的[照片](../Page/照片.md "wikilink")
    夾在數學課本裡，是美咲入學典禮時拍的。
  - 背包
    在行人漂流的期間，一直在行人左右。由於行人能有效運用其背包，因此內裡亦有不少東西。到達藍蘭島後，內裡行李並沒有濕透得太嚴重，可見防水能力十分強。

## 千影的藏書

  - **角色扮演名鑑**
  - **年輕小太太大集合（第6期）**
  - **角色扮演美少女**
  - **水手服美少女大集合**
  - **色情的書**
  - **年輕小太太大集合（第9期）**
  - **魔導之書**
  - **封印之書**
  - **迷宮館殺人事件**
  - **月見館殺人事件**
  - **愛的三角關係**
  - **愛的暴風雨**
  - **陷入泥沼的情人物語**
  - **宮本武藏**

## 背景介紹

### 藍蘭島

  - 在[江戶時代以前](../Page/江戶時代.md "wikilink")，是個[無人島](../Page/無人島.md "wikilink")。
  - 130年前，要回航[歐洲的客船](../Page/歐洲.md "wikilink")「Island號」在[上海與](../Page/上海.md "wikilink")[長崎間遭遇](../Page/長崎.md "wikilink")[暴風雨而沈沒時](../Page/暴風雨.md "wikilink")，生還的船員與乘客漂流到島上。由船名將該島取名為「藍蘭島（）」，開始在島上生活。
  - 而後，藍蘭島[文明便維持在](../Page/文明.md "wikilink")[明治初期庶民生活的等級](../Page/明治.md "wikilink")，沒有現代的[電力](../Page/電力.md "wikilink")、[煤氣](../Page/煤氣.md "wikilink")、[自來水等等基本建設](../Page/自來水.md "wikilink")，也沒有[貨幣](../Page/貨幣.md "wikilink")。島上過著自給自足的生活。
  - 12年前，藍蘭島舉行「只有男子漢的大船釣魚大會」時受暴風雨襲擊之故，男性島民所乘坐的船全都被漂流到外面去了。在東方院行人漂流上岸之前，藍蘭島島民只有女性。
  - 在島上的動物基本上都長得非常可愛，與外界動物的長相都有明顯的差異，另外島上動物的歲數都跟人類是一樣的成長的。
  - 另外在藍蘭島上，食動物肉是屬禁忌的（因為動物長得太可愛，讓人們不敢下手去吃牠們），因此都是捕食魚肉或農作物來食用，一開始來到島上的人們因魚長得非常奇怪而並不習慣吃魚。

## 書籍資訊

### 單行本

<table>
<thead>
<tr class="header">
<th><p>rowspan ="2"|冊數</p></th>
<th><p>colspan ="2"| <a href="../Page/Square_Enix.md" title="wikilink">Square Enix</a></p></th>
<th><p>colspan ="2"|  <a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>日文版（初回限定特裝版*）</p></td>
<td><p>初版發售、初版發行</p></td>
<td><p>中文版</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>ISBN 978-4-7575-0856-9</p></td>
<td><p>2003年1月22日、2003年2月22日</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>ISBN 978-4-7575-1120-0 *</p></td>
<td><p>2004年2月21日、2004年3月22日</p></td>
</tr>
<tr class="even">
<td><p>ISBN 978-4-7575-1138-5 |- </p></td>
<td><p>4</p></td>
<td><p>ISBN 978-4-7575-1232-0</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>ISBN 978-4-7575-1316-7 *</p></td>
<td><p>2005年1月22日、2005年2月22日</p></td>
</tr>
<tr class="even">
<td><p>ISBN 978-4-7575-1354-9 |- </p></td>
<td><p>6</p></td>
<td><p>ISBN 978-4-7575-1473-7</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>ISBN 978-4-7575-1607-6</p></td>
<td><p>2006年1月21日、2006年2月22日</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>ISBN 978-4-7575-1797-4</p></td>
<td><p>2006年10月21日、2006年11月22日</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>ISBN 978-4-7575-2077-6</p></td>
<td><p>2007年8月22日、2007年9月22日</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>ISBN 978-4-7575-2322-7</p></td>
<td><p>2008年7月22日</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>ISBN 978-4-7575-2490-3 *</p></td>
<td><p>2009年5月22日、2009年6月23日</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>ISBN 978-4-7575-2931-1</p></td>
<td><p>2010年7月22日</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>ISBN 978-4-7575-3366-0</p></td>
<td><p>2011年9月22日</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>ISBN 978-4-7575-3778-1</p></td>
<td><p>2012年11月22日</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>ISBN 978-4-7575-4200-6</p></td>
<td><p>2014年1月22日</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p>ISBN 978-4-7575-4582-3</p></td>
<td><p>2015年3月20日</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>ISBN 978-4-7575-4974-6</p></td>
<td><p>2016年5月21日</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p>ISBN 978-4-7575-5182-4</p></td>
<td><p>2016年12月22日</p></td>
</tr>
<tr class="even">
<td><p>集數</p></td>
<td><p>ISBN 日版初回限定ISBN號碼 *</p></td>
<td><p><a href="../Page/年.md" title="wikilink">年</a><a href="../Page/月日.md" title="wikilink">月日</a>(日本初版發售)、<a href="../Page/年.md" title="wikilink">年</a><a href="../Page/月日.md" title="wikilink">月日</a>(日本初版發行)</p></td>
</tr>
<tr class="odd">
<td><p>ISBN 日版通常版ISBN號碼</p>
<p><code>    單數本有初回限定格式 --&gt;</code></p>
<p>&lt;!-- 雙數本有初回限定格式 |- </p></td>
<td><p>集數</p></td>
<td><p>ISBN 日版初回限定ISBN號碼 *</p></td>
</tr>
</tbody>
</table>

### 小說

小說藍蘭島漂流記《仙女的神諭》（）

  - 日文版：ISBN 978-4-7575-1331-0 2004年11月30日初版發售・2004年12月21日初版發行
  - 中文版：ISBN 978-986-10-1390-9 出版日期 2008年2月13日

小說藍蘭島漂流記2《倍感思念》（）

  - 日文版：ISBN 978-4-7575-1988-6 2007年3月22日初版發售・2007年4月12日初版發行
  - 中文版：ISBN 978-986-10-1391-6 出版日期 2008年4月3日

小說藍蘭島漂流記3《穿越時空》（）

  - 日文版：ISBN 978-4-7575-2256-5 2008年4月30日初版發售・2008年5月21日初版發行
  - 中文版：ISBN 978-986-10-6980-7 出版日期 2010年1月27日

### 其他

藍蘭島漂流記完全導讀手冊（）

  - 日文版：ISBN 978-4-7575-1359-4 2005年3月31日初版發售・2005年4月21日初版發行
  - 中文版：ISBN 978-986-11-7651-2 出版日期 2006年8月31日

藤代健插畫集 藍蘭島繪卷（）

  - 日文版：ISBN 978-4-7575-2098-1 2007年8月22日初版發售・2007年9月12日初版發行
  - 中文版：ISBN 978-986-10-1395-4 出版日期 2008年2月14日

<!-- end list -->

  - 日文版：ISBN 978-4-7575-0993-1 2003年7月22日初版發售・2003年8月12日初版發行

<!-- end list -->

  - 日文版：ISBN 978-4-7575-1985-5 2007年4月27日初版發售・2007年5月18日初版發行

## 電視動畫

2007年4月4日電視動畫開始播映。小鈴以外的主要角色的聲優皆與廣播劇版不同。

### 工作人員

  - 企劃：田口浩司、[大月俊倫](../Page/大月俊倫.md "wikilink")
  - 原作：藤代健
  - 刊載：月刊少年GANGAN
  - 製作人：森山敦、柏田圭一、、池田慎一
  - 企劃協力：渡邊純也、松崎武吏、倉重宣之、木村康貴
  - 系列構成：池田真美子
  - 角色設定：細田直人
  - 美術監督：渡邊三千恵
  - 色彩設計：岩井田洋
  - 攝影監督：近藤靖尚
  - 音響監督：
  - 編輯：田熊純
  - 音樂：水谷廣實
  - 企劃協力・製作：GANSIS
  - 動畫製作：[feel.](../Page/feel..md "wikilink")
  - 監督：[岡本英樹](../Page/岡本英樹.md "wikilink")
  - 製作：

### 各話標題

TV全26話已播放完畢。

<table>
<thead>
<tr class="header">
<th><p>電視台播放順序</p></th>
<th><p>標題（日文原文）</p></th>
<th><p>標題（中文翻譯）</p></th>
<th><p>播放日期（<a href="../Page/東京電視台.md" title="wikilink">東京電視台</a>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1話</p></td>
<td></td>
<td><p>落難漂流…卻到了天堂?</p></td>
<td><p>2007年4月4日 |- </p></td>
</tr>
<tr class="even">
<td><p>第3話</p></td>
<td></td>
<td><p>有點用吧…寄人籬下的!</p></td>
<td><p>2007年4月18日 |- </p></td>
</tr>
<tr class="odd">
<td><p>第5話</p></td>
<td></td>
<td><p>去找出熊熊吧</p></td>
<td><p>2007年5月2日 |- </p></td>
</tr>
<tr class="even">
<td><p>第7話</p></td>
<td><p>A：</p></td>
<td><p>您說啥喵,師父大人？</p></td>
<td><p>2007年5月16日</p></td>
</tr>
<tr class="odd">
<td><p>B：</p></td>
<td><p>想要去賞櫻 |- </p></td>
<td><p>第8話</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第9話</p></td>
<td></td>
<td><p>想要誘惑住 繼承人</p></td>
<td><p>2007年5月30日 |- </p></td>
</tr>
<tr class="odd">
<td><p>第11話</p></td>
<td></td>
<td><p>興奮高興的取冰去</p></td>
<td><p>2007年6月13日 |- </p></td>
</tr>
<tr class="even">
<td><p>第13話</p></td>
<td></td>
<td><p>想去見 行人</p></td>
<td><p>2007年6月27日 |- </p></td>
</tr>
<tr class="odd">
<td><p>第15話</p></td>
<td></td>
<td><p>想要治好 梅梅</p></td>
<td><p>2007年7月11日 |- </p></td>
</tr>
<tr class="even">
<td><p>第17話</p></td>
<td></td>
<td><p>變換吧 魔法</p></td>
<td><p>2007年7月25日 |- </p></td>
</tr>
<tr class="odd">
<td><p>第19話</p></td>
<td></td>
<td><p>滿是謎團的 偵探 前篇</p></td>
<td><p>2007年8月8日 |- </p></td>
</tr>
<tr class="even">
<td><p>第21話</p></td>
<td></td>
<td><p>被化身,被耍的 貍貓</p></td>
<td><p>2007年8月22日 |- </p></td>
</tr>
<tr class="odd">
<td><p>第23話</p></td>
<td></td>
<td><p>帶我去吧 寺子屋私塾</p></td>
<td><p>2007年9月5日 |- </p></td>
</tr>
<tr class="even">
<td><p>第25話</p></td>
<td></td>
<td><p>鍛鍊吧遜腳</p></td>
<td><p>2007年9月19日 |- </p></td>
</tr>
</tbody>
</table>

### 播放電視台

| 播映地域                                   | 電視台                                      | 播映期間                   | 播映時間              | 所屬聯播網                                |
| -------------------------------------- | ---------------------------------------- | ---------------------- | ----------------- | ------------------------------------ |
| [關東地區](../Page/關東地區.md "wikilink")     | [東京電視台](../Page/東京電視台.md "wikilink")     | 2007年4月4日 -2007年9月26日  | 水曜25時20分 - 25時50分 | [東京電視網](../Page/東京電視網.md "wikilink") |
| [大阪府](../Page/大阪府.md "wikilink")       | [大阪電視台](../Page/大阪電視台.md "wikilink")     | 2007年4月4日 -2007年9月26日  | 水曜25時20分 - 25時50分 | [東京電視網](../Page/東京電視網.md "wikilink") |
| [愛知縣](../Page/愛知縣.md "wikilink")       | [愛知電視台](../Page/愛知電視台.md "wikilink")     | 2007年4月4日 -2007年9月26日  | 水曜25時28分 - 25時58分 | [東京電視網](../Page/東京電視網.md "wikilink") |
| [福岡縣](../Page/福岡縣.md "wikilink")       | [TVQ九州放送](../Page/TVQ九州放送.md "wikilink") | 2007年4月8日 -2007年9月30日  | 日曜26時45分 - 27時15分 | [東京電視網](../Page/東京電視網.md "wikilink") |
| [北海道](../Page/北海道.md "wikilink")       | [TV北海道](../Page/TV北海道.md "wikilink")     | 2007年4月9日 -2007年10月1日  | 月曜25時30分 - 26時00分 | [東京電視網](../Page/東京電視網.md "wikilink") |
| [岡山縣](../Page/岡山縣.md "wikilink")       | [瀨戶內電視台](../Page/瀨戶內電視台.md "wikilink")   | 2007年4月11日 -2007年10月3日 | 水曜25時48分 - 26時18分 | [東京電視網](../Page/東京電視網.md "wikilink") |
| [全国放送](../Page/:ja:全国放送.md "wikilink") | [AT-X](../Page/AT-X.md "wikilink")       | 2007年4月9日 -2007年10月1日  | 月曜09時30分 - 10時00分 | [CS放送](../Page/衛星电视.md "wikilink")   |

-----

### 主題曲

  - 片頭曲、最終話片尾曲

<!-- end list -->

  - 「Days」

<!-- end list -->

  -
    作詞：田代智一、，作曲：田代智一，編曲：安藤高弘，歌：[堀江由衣](../Page/堀江由衣.md "wikilink")（小鈴）

  - 片尾曲

<!-- end list -->

  - 「Say cheese\!」（第1話～第12話）

<!-- end list -->

  -
    作詞：田代智一、，作曲：田代智一，編曲：近藤昭雄，歌：[堀江由衣](../Page/堀江由衣.md "wikilink")（小鈴）

<!-- end list -->

  - 「冷奴」（凉豆腐）（第13話）

<!-- end list -->

  -
    作曲：[松浦有希](../Page/松浦有希.md "wikilink")，編曲：松浦有希，歌手：[渡邊明乃](../Page/渡邊明乃.md "wikilink")（豬排）

<!-- end list -->

  - 「」（戀愛的天氣圖）（第14話～第25話）

<!-- end list -->

  -
    作詞：只野菜摘，作曲：古池孝浩，編曲：古池孝浩，歌：[堀江由衣](../Page/堀江由衣.md "wikilink")（小鈴）

  - 插入曲

<!-- end list -->

  - 「雨降りお月さん」（第10話）

<!-- end list -->

  -
    作詞：野口雨情，作曲：中山晋平，編曲：大川茂伸，歌：[堀江由衣](../Page/堀江由衣.md "wikilink")（小鈴）

<!-- end list -->

  - 「愛のために」（第26話）

<!-- end list -->

  -
    作詞：松浦有希，作曲：大川茂伸，編曲：大川茂伸，歌：[榊原由依](../Page/榊原由依.md "wikilink")

## 藍蘭島專輯

  - 「」

<!-- end list -->

  -
    商品號碼:KICA-840
    媒體:CD
    發售日:2007年3月21日

<!-- end list -->

  - 收錄曲

<!-- end list -->

  - 01\. Welcome\!（opening）
  - 02\. Lovi'n you　
      -
        歌：[堀江由衣](../Page/堀江由衣.md "wikilink")（小鈴）、作詞：松浦有希、作曲：春行、編曲：大坪直樹
  - 03\. Thrill（interlude）
  - 04\. Fancy cat's eye　
      -
        歌：[高橋美佳子](../Page/高橋美佳子.md "wikilink")（麻知）、作詞：松浦有希、作曲：大川茂伸、編曲：大坪直樹
  - 05\. Tricky（interlude）
  - 06\. Hatsukoi　
      -
        歌：[千葉紗子](../Page/千葉紗子.md "wikilink")（綾音）、作詞：松浦有希、作曲：洞澤徹、編曲：大坪直樹
  - 07\. （interlude）
  - 08\. 　
      -
        歌：[白石涼子](../Page/白石涼子.md "wikilink")（玲玲）、作詞：松浦有希、作曲：春行、編曲：大坪直樹
  - 09\. Starry-eyed girl（interlude）
  - 10\. 　
      -
        歌：[伊藤静](../Page/伊藤静.md "wikilink")（千影）、作詞：松浦有希、作曲：大久保薫、編曲：大坪直樹
  - 11\. Bambina（interlude）
  - 12\. 　
      -
        歌：[長谷川静香](../Page/長谷川静香.md "wikilink")（雪乃）、作詞：松浦有希、作曲：、編曲：大坪直樹
  - 13\. Cruel（interlude）
  - 14\. Unite　
      -
        歌：[高橋美佳子](../Page/高橋美佳子.md "wikilink")、[千葉紗子](../Page/千葉紗子.md "wikilink")（麻知、綾音）、作詞：松浦有希、作曲：、編曲：大坪直樹
  - 15\. （interlude）
  - 16\. Siesta　
      -
        歌：[堀江由衣](../Page/堀江由衣.md "wikilink")、[下野紘](../Page/下野紘.md "wikilink")（小鈴、東方院行人）、作詞：松浦有希、作曲：河又圭一、編曲：大坪直樹
  - 17\. Snatch\!\!（ending）
  - 18\. 寶物
      -
        歌：[堀江由衣](../Page/堀江由衣.md "wikilink")、[高橋美佳子](../Page/高橋美佳子.md "wikilink")、[千葉紗子](../Page/千葉紗子.md "wikilink")、[白石涼子](../Page/白石涼子.md "wikilink")、[伊藤静](../Page/伊藤静.md "wikilink")、[長谷川静香](../Page/長谷川静香.md "wikilink")、[下野紘](../Page/下野紘.md "wikilink")（小鈴、麻知、綾音、玲玲、千影、雪乃、東方院行人）、作詞：飯田建彦、、作曲：飯田建彦、編曲：大坪直樹

<!-- end list -->

  - 「」

<!-- end list -->

  -
    商品號碼:KICA-847
    媒體:CD
    發售日:2007年6月27日

<!-- end list -->

  - 收錄曲

<!-- end list -->

  - 01\.
      -
        歌：[堀江由衣](../Page/堀江由衣.md "wikilink")（小鈴）、作詞：松浦有希、作曲：河合英嗣、編曲：河合英嗣
  - 02\.
      -
        歌：[白石涼子](../Page/白石涼子.md "wikilink")（玲玲）、作詞：松浦有希、作曲：大久保美幸、岡本友里子、編曲：PON
  - 03\.
      -
        歌：[伊藤静](../Page/伊藤静.md "wikilink")（千影）、作詞：松浦有希、作曲：永井真一、編曲：大坪直樹
  - 04\.
      -
        歌：[長谷川静香](../Page/長谷川静香.md "wikilink")（雪乃）、作詞：松浦有希、作曲：小池修也、編曲：岩瀨聰志
  - 05\.
      -
        歌：[高橋美佳子](../Page/高橋美佳子.md "wikilink")（麻知）、作詞：松浦有希、作曲：春行、編曲：
  - 06\.
      -
        歌：[千葉紗子](../Page/千葉紗子.md "wikilink")（綾音）、作詞：松浦有希、作曲：菊谷知樹、編曲：菊谷知樹
  - 07\.
      -
        歌：[生天目仁美](../Page/生天目仁美.md "wikilink")（梅梅）、作詞：松浦有希、作曲：、編曲：
  - 08.冷奴
      -
        歌：[渡邊明乃](../Page/渡邊明乃.md "wikilink")（豬排）、作曲：松浦有希、編曲：松浦有希
  - 09\. one-way love
      -
        歌：[白石涼子](../Page/白石涼子.md "wikilink")、[渡辺明乃](../Page/渡辺明乃.md "wikilink")（玲玲、小尊）、作詞：松浦有希、作曲：大久保美幸、岡本友里子、編曲：POM
  - 10\.
      -
        歌：[下野紘](../Page/下野紘.md "wikilink")（東方院行人）、作詞：松浦有希、作曲：飯田建彦、編曲：岩瀨聰志
  - 11.寶物～Island Breeze～
      -
        歌：[堀江由衣](../Page/堀江由衣.md "wikilink")、[高橋美佳子](../Page/高橋美佳子.md "wikilink")、[千葉紗子](../Page/千葉紗子.md "wikilink")、[白石涼子](../Page/白石涼子.md "wikilink")、[伊藤静](../Page/伊藤静.md "wikilink")、[長谷川静香](../Page/長谷川静香.md "wikilink")、[下野紘及其它](../Page/下野紘.md "wikilink")（小鈴、麻知、綾音、玲玲、千影、雪乃、東方院行人及其它）、作詞：松浦有希、作曲：飯田建彦、編曲：松浦有希

## 外部連結

  - [少年Gangan月刊網站](http://gangan.square-enix.co.jp/index.html)
  - [藍蘭島漂流記動畫版公式網頁](http://www.starchild.co.jp/special/airantou/index.html)
  - [東京電視台藍蘭島網站](http://www.tv-tokyo.co.jp/anime/airantou/)

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:月刊少年GANGAN連載作品](../Category/月刊少年GANGAN連載作品.md "wikilink")
[Category:後宮型作品](../Category/後宮型作品.md "wikilink")
[Category:日本輕小說](../Category/日本輕小說.md "wikilink")
[Category:2007年東京電視網動畫](../Category/2007年東京電視網動畫.md "wikilink")
[Category:東京電視台深夜動畫](../Category/東京電視台深夜動畫.md "wikilink")
[Category:GANGAN POWERED](../Category/GANGAN_POWERED.md "wikilink")
[Category:虛構島嶼背景動畫](../Category/虛構島嶼背景動畫.md "wikilink")
[Category:Feel.](../Category/Feel..md "wikilink")