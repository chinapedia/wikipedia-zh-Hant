**藍貝**（**Blue
mussel**，[学名](../Page/学名.md "wikilink")**)，俗稱「藍青口」、「食用殼菜蛤」，，[馬祖出產的又稱](../Page/馬祖.md "wikilink")**馬祖淡菜**\[1\]，是[贻贝目](../Page/贻贝目.md "wikilink")[壳菜蛤科](../Page/壳菜蛤科.md "wikilink")[壳菜蛤属的一种](../Page/壳菜蛤属.md "wikilink")。\[2\]\[3\]\[4\]\[5\]

## 分布與產地

主要分布于[中国大陸沿海](../Page/中国大陸.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")[北海](../Page/北海_\(大西洋\).md "wikilink")、[美國北](../Page/美國.md "wikilink")[大西洋](../Page/大西洋.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、馬祖、新北市貢寮鄉蚊子坑，花蓮海岸等海域，常栖息在低潮区至浅海水域\[6\]。

## 食用

藍貝是一種可供人類食用的[雙殼綱](../Page/雙殼綱.md "wikilink")[軟體動物](../Page/軟體動物.md "wikilink")。

## 参考文献

[Category:可食用軟體動物](../Category/可食用軟體動物.md "wikilink")
[Category:殼菜蛤屬](../Category/殼菜蛤屬.md "wikilink")

1.  [海中威而鋼「馬祖淡菜」
    海大證實是真的](https://udn.com/news/story/7266/3463426)，聯合報，2018年11月6日

2.

3.
4.
5.
6.