**藜木科**又名[肉穗果科或](../Page/肉穗果科.md "wikilink")[腌藜草科](../Page/腌藜草科.md "wikilink")，只有1[属](../Page/属.md "wikilink")2[种](../Page/种.md "wikilink")，分布在[热带](../Page/热带.md "wikilink")[美洲沿海](../Page/美洲.md "wikilink")，[澳大利亚北部沿海和](../Page/澳大利亚.md "wikilink")[新几内亚以及一些](../Page/新几内亚.md "wikilink")[太平洋岛屿](../Page/太平洋.md "wikilink")。

本科[植物为盐生](../Page/植物.md "wikilink")[亚灌木](../Page/亚灌木.md "wikilink")；单[叶小](../Page/叶.md "wikilink")，肉质，对生，有小托叶；[花单性](../Page/花.md "wikilink")，雌雄同株，[花瓣](../Page/花瓣.md "wikilink")4，雌花无花瓣；[果实为](../Page/果实.md "wikilink")[核果](../Page/核果.md "wikilink")。

1981年的[克朗奎斯特分类法单独列出一个](../Page/克朗奎斯特分类法.md "wikilink")[藜木目](../Page/藜木目.md "wikilink")，属于[五桠果亚纲](../Page/五桠果亚纲.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其合并到](../Page/APG_分类法.md "wikilink")[十字花目中](../Page/十字花目.md "wikilink")，2003年经过修订的[APG
II 分类法维持原分类](../Page/APG_II_分类法.md "wikilink")。

## 外部链接

  - 在 [L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)
    中的 [藜木科](http://delta-intkey.com/angio/www/batidace.htm)
  - [APG网站中的藜木科](http://www.mobot.org/MOBOT/Research/APWeb/orders/brassicalesweb.htm#Bataceae)
  - [NCBI中的藜木科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=4434)

[\*](../Category/藜木科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")