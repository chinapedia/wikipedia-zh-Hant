**藤子·F·不二雄製作公司**，日文全名「****」（台灣[青文出版社譯為](../Page/青文出版社.md "wikilink")**藤子·F·不二雄創作公司**），是[藤子·F·不二雄所創立的](../Page/藤子·F·不二雄.md "wikilink")[漫畫製作公司](../Page/漫畫.md "wikilink")，通稱「****」（藤子製作）。前代表[非執行董事會長是藤本正子](../Page/非執行董事.md "wikilink")，代表[執行董事社長是伊藤善章](../Page/執行董事.md "wikilink")（前[小學館製作董事](../Page/小學館製作.md "wikilink")）。

## 概要

  - 前身是[藤子·F·不二雄](../Page/藤子·F·不二雄.md "wikilink")（藤本弘）和[藤子不二雄A在](../Page/藤子不二雄A.md "wikilink")「[藤子不二雄](../Page/藤子不二雄.md "wikilink")」組合時代所成立的「[有限会社](../Page/有限会社.md "wikilink")」（）。1987年藤子不二雄解散之後，藤子工作室繼承了藤子不二雄A的作品[版權管理](../Page/版權.md "wikilink")，藤子·F·不二雄的作品版權則由新設立的「有限会社藤子·F·不二雄製作」（）管理；當時的藤子·F·不二雄製作會長就是藤子·F·不二雄（藤本弘）本人。藤本弘逝世後，藤子·F·不二雄製作的公司形態轉為[株式會社](../Page/株式會社.md "wikilink")，由夫人藤本正子接任會長；2006年藤本正子退休，社長則由伊藤善章接任。
  - 現在公司位於[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[新宿區](../Page/新宿區.md "wikilink")[西新宿](../Page/西新宿.md "wikilink")6丁目。
  - 現在主要負責[哆啦A夢等漫畫角色的製作管理](../Page/哆啦A夢_\(漫畫角色\).md "wikilink")。而從2004年開始，停止製作漫畫的業務，並讓公司下的[漫畫家全部獨立](../Page/漫畫家.md "wikilink")。
  - 在2011年成立[藤子·F·不二雄博物館](../Page/藤子·F·不二雄博物館.md "wikilink")，是以展示藤子·F·不二雄創作資料為主的紀念館。

## 與藤子工作室的關係

  - 目前與藤子不二雄A的「株式會社藤子工作室」並沒有資本或業務上的相關聯繫，是兩間分開營運的不同公司，分別負責管理「[藤子·F·不二雄](../Page/藤子·F·不二雄.md "wikilink")」與「[藤子不二雄A](../Page/藤子不二雄A.md "wikilink")」兩位作家的著作權。若是與藤子不二雄名義相關的著作權，則會同時寫出兩家公司的名字。
  - 有兩間公司因為《[小鬼Q太郎](../Page/Q太郎.md "wikilink")》的再版發行而發生不愉快，但詳細的情形不明（關於《小鬼Q太郎》的再版問題，請參閱該條目）。

## 評價

## 相關條目

  - [藤子不二雄](../Page/藤子不二雄.md "wikilink")

  - [藤子·F·不二雄](../Page/藤子·F·不二雄.md "wikilink")

  - [藤子不二雄A](../Page/藤子不二雄A.md "wikilink")

  -
  - [岡田康則](../Page/岡田康則.md "wikilink")

  - [朝日電視台](../Page/朝日電視台.md "wikilink")

  - [小學館](../Page/小學館.md "wikilink")

  - [麥原伸太郎](../Page/麥原伸太郎.md "wikilink")

  - [哆啦A夢](../Page/哆啦A夢.md "wikilink")

## 外部連結

  - [藤子·F·不二雄製作公司](https://web.archive.org/web/20061130031240/http://dora-world.com/index.html)

[Category:1990年代開業公司](../Category/1990年代開業公司.md "wikilink")
[Category:日本電影公司](../Category/日本電影公司.md "wikilink")
[Category:日本動畫工作室](../Category/日本動畫工作室.md "wikilink")
[Category:藤子·F·不二雄](../Category/藤子·F·不二雄.md "wikilink")
[Category:新宿區公司](../Category/新宿區公司.md "wikilink")