**蘇來強**（**So Loi
Keung**，），生於[香港](../Page/香港.md "wikilink")[香港仔](../Page/香港仔.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")，司職[中場](../Page/中場.md "wikilink")，現時效力[香港甲組聯賽球會](../Page/香港甲組聯賽.md "wikilink")[中西區及擔任](../Page/中西區足球隊.md "wikilink")[自由人足球會教練](../Page/自由人足球會.md "wikilink")。

## 球會生涯

蘇來強生於[香港](../Page/香港.md "wikilink")[香港仔](../Page/香港仔.md "wikilink")，自小在街場出身未受過正統足球訓練直至17歲時透過[香港仔明愛社工穿針引線下加盟](../Page/香港明愛.md "wikilink")[二合參加當時球隊的青年軍計劃](../Page/二合足球隊.md "wikilink")，期間適逢[比利時球會](../Page/比利時.md "wikilink")[真特的教練來港目標是在](../Page/皇家真特體育會.md "wikilink")[愉園的青年軍揀蟀惟](../Page/愉園體育會.md "wikilink")[愉園球員外出備戰](../Page/愉園體育會.md "wikilink")[亞協盃](../Page/亞協盃.md "wikilink")，於是[真特教練便選擇到](../Page/皇家真特體育會.md "wikilink")[跑馬地觀看](../Page/跑馬地.md "wikilink")[二合青年軍訓練更看中時任](../Page/二合足球隊.md "wikilink")[二合球員蘇來強並邀請加盟但惟雙方在溝通上有問題後來便再沒有下文在約滿](../Page/二合足球隊.md "wikilink")[二合後他曾經一度沒再踢足球期間做過跟車和裝飾材料員](../Page/二合足球隊.md "wikilink")，後來獲[乙組球會](../Page/香港乙組足球聯賽.md "wikilink")[福建邀請加盟同時為生活兼職任救生員並協助球隊首度升班挑戰](../Page/福建體育會.md "wikilink")[甲組聯賽作賽](../Page/香港甲組足球聯賽.md "wikilink")，結果他更在2014/15球季成為球隊隊長惟該季[福建不幸宣告降班使他轉會](../Page/福建體育會.md "wikilink")[流浪及後因病離隊](../Page/香港流浪足球會.md "wikilink")，輾轉在相識5年的[甲組球員](../Page/香港甲組足球聯賽.md "wikilink")[趙俊傑介紹下復出加盟](../Page/趙俊傑.md "wikilink")[新界地區球會](../Page/新界.md "wikilink")[和富大埔迎來職業生涯的高峰](../Page/大埔足球會.md "wikilink")，他在其生涯第二場對[愉園的](../Page/愉園體育會.md "wikilink")[甲組賽事中以一記中柱彈入的遠射取得職業生涯首個入球](../Page/香港甲組足球聯賽.md "wikilink")，而[和富大埔首個](../Page/大埔足球會.md "wikilink")[甲組入球亦是由蘇來強於以](../Page/香港甲組足球聯賽.md "wikilink")1–6負[傑志的賽事中取得](../Page/傑志體育會.md "wikilink")，其後他在2009年6月6日協助[大埔於](../Page/大埔足球會.md "wikilink")[足總盃決賽中以](../Page/2008–09年香港足總盃.md "wikilink")4–2擊敗[天水圍飛馬獲得升班三年後首個錦標](../Page/天水圍飛馬.md "wikilink")，\[1\]

而幾年間蘇來強為[大埔贏得](../Page/大埔足球會.md "wikilink")[高級組銀牌和](../Page/香港高級組銀牌.md "wikilink")[足總盃冠軍更奪得](../Page/2008–09年香港足總盃.md "wikilink")[亞協盃分組賽資格到](../Page/2010年亞洲足協盃.md "wikilink")2010年他以隊長身份出戰[亞協盃分組賽](../Page/2010年亞洲足協盃.md "wikilink")。而當年[大埔僅得](../Page/大埔足球會.md "wikilink")600萬班費全隊只得兩名外援但分心[亞協盃卻未有降班只是蘇來強在狀態最高峰時卻無緣入選當年只得大球會成員組成的](../Page/2010年亞洲足協盃.md "wikilink")[香港代表隊](../Page/香港足球代表隊.md "wikilink")，到2010年暑假適逢當時兩支勁旅[飛馬及](../Page/香港飛馬足球會.md "wikilink")[傑志對他作為斟介雖然](../Page/傑志體育會.md "wikilink")[飛馬開出的薪酬較高但他最後因](../Page/香港飛馬足球會.md "wikilink")[傑志當時訓練的地方較使他能多陪伴懷孕的妻子](../Page/傑志體育會.md "wikilink")，所以他於2010年5月與[傑志宣佈簽訂合約最終協助](../Page/傑志體育會.md "wikilink")[傑志以](../Page/傑志體育會.md "wikilink")18戰44分以1分壓過[南華奪得](../Page/南華足球隊.md "wikilink")[甲組聯賽冠軍而他則取得](../Page/2011–12年香港甲組足球聯賽.md "wikilink")3個入球同年更首度入選[香港隊](../Page/香港足球代表隊.md "wikilink")，至2011年6月離開[傑志轉投來季同樣得到](../Page/傑志體育會.md "wikilink")[亞協盃參賽資格的](../Page/2012年亞洲足協盃.md "wikilink")[公民](../Page/公民足球隊.md "wikilink")，到2014年6月由於[公民季尾決定不會參加來季的](../Page/公民足球隊.md "wikilink")[港超聯使他轉投](../Page/港超聯.md "wikilink")[港超聯球會](../Page/港超聯.md "wikilink")[天行元朗到季尾不獲留用](../Page/元朗足球會.md "wikilink")，先後隨[標準流浪和](../Page/香港流浪足球會.md "wikilink")[黃大仙試腳但最終都未能就條件與球隊達成協議經與家人及多名球圈中好友傾談後決定與其要做兼職幫補收入倒不如宣告退役專注教練工作的發展](../Page/黃大仙區康樂體育會.md "wikilink")，決定告別職業足球後來受到[甲組聯賽球會](../Page/香港甲組聯賽.md "wikilink")[晨曦邀請在業餘賽事繼續球員生涯](../Page/晨曦體育會.md "wikilink")。\[2\]

## 國際賽生涯

蘇來強於2010年1月首度入選[香港代表隊並於](../Page/香港足球代表隊.md "wikilink")[亞洲盃外圍賽作客](../Page/2011年亞洲盃外圍賽.md "wikilink")[巴林國家隊的賽事中正選登場](../Page/巴林國家足球隊.md "wikilink")，到2011年9月30日蘇來強在[龍騰盃對](../Page/2011年龍騰盃國際足球邀請賽.md "wikilink")[菲律賓國家隊的比賽中被趕出場而最終](../Page/菲律賓國家足球隊.md "wikilink")[香港亦成功衛冕](../Page/香港足球代表隊.md "wikilink")[龍騰盃冠軍](../Page/龍騰盃國際足球邀請賽.md "wikilink")，其後蘇來強獲徵召入選[香港五人足球代表隊期間協助](../Page/香港五人制足球代表隊.md "wikilink")[香港擊敗](../Page/香港五人制足球代表隊.md "wikilink")[中國與](../Page/中國.md "wikilink")[韓國以及](../Page/韓國.md "wikilink")[科威特等勁旅](../Page/科威特.md "wikilink")。

## 個人生活

蘇來強和女朋友劉妙賢在2007年9月15日結束5年愛情長跑而一對新人於當日[旺角大球場對](../Page/旺角大球場.md "wikilink")[愉園的賽事進行期間到場為隊友打氣](../Page/愉園體育會.md "wikilink")，到半場休息時更到場中拍照留念成為首位[香港足球運動員在運動場拍攝結婚相片](../Page/香港.md "wikilink")，其後更與前隊友[溫遠雄合作創辦](../Page/溫遠雄.md "wikilink")[自由人足球會希望可培育更多](../Page/自由人足球會.md "wikilink")[香港足球未來接班人](../Page/香港.md "wikilink")。\[3\]

## 參考資料

## 外部連結

  - [香港足球總會網頁球員註冊資料](http://www.hkfa.com/zh-hk/player_view.php?player_id=55)

[Category:蘇姓](../Category/蘇姓.md "wikilink")
[Category:港超球員](../Category/港超球員.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:福建球員](../Category/福建球員.md "wikilink")
[Category:流浪球員](../Category/流浪球員.md "wikilink")
[Category:大埔球員](../Category/大埔球員.md "wikilink")
[Category:傑志球員](../Category/傑志球員.md "wikilink")
[Category:公民球員](../Category/公民球員.md "wikilink")
[Category:元朗球員](../Category/元朗球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")
[Category:香港培英中學校友](../Category/香港培英中學校友.md "wikilink")

1.  [【生活與生存】從兼職救生員到足球教練——專訪蘇來強](http://www.inmediahk.net/node/1038535)
    香港獨立媒體 2015-10-29
2.  [港超聯　諗清諗楚！蘇來強掛靴](http://hk.on.cc/hk/bkn/cnt/sport/20150829/bkn-20150829160343334-0829_00882_001.html)
    on.cc東網 2015年08月29日
3.  [【港足二十年．蘇來強】見盡球圈不合理　望年輕球員自由高飛](https://www.hk01.com/%E9%AB%94%E8%82%B2/116345/-%E6%B8%AF%E8%B6%B3%E4%BA%8C%E5%8D%81%E5%B9%B4-%E8%98%87%E4%BE%86%E5%BC%B7-%E8%A6%8B%E7%9B%A1%E7%90%83%E5%9C%88%E4%B8%8D%E5%90%88%E7%90%86-%E6%9C%9B%E5%B9%B4%E8%BC%95%E7%90%83%E5%93%A1%E8%87%AA%E7%94%B1%E9%AB%98%E9%A3%9B)
    [香港01](../Page/香港01.md "wikilink") 2017-09-04