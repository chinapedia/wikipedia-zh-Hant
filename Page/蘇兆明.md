**蘇兆明**（**Nicholas Robert
Sallnow-Smith**，），[英國人](../Page/英國.md "wikilink")，前[置地集團行政總裁](../Page/置地集團.md "wikilink")，現擔任[領展房地產信託基金董事會主席及](../Page/領展房地產信託基金.md "wikilink")[渣打東北亞地區行政總裁](../Page/渣打.md "wikilink")。

蘇兆明於[英國](../Page/英國.md "wikilink")[劍橋大學岡維爾與凱爾斯學院及](../Page/劍橋大學.md "wikilink")[萊斯特大學畢業](../Page/萊斯特大學.md "wikilink")，1998年起於香港置地任職財務董事，2000年獲升為[行政總裁](../Page/行政總裁.md "wikilink")。蘇兆明作風進取，在他任內，置地重新進軍[香港](../Page/香港.md "wikilink")[豪宅物業](../Page/豪宅.md "wikilink")，及重建[中環多幢商業物業](../Page/中環.md "wikilink")，包括[遮打大廈](../Page/遮打大廈.md "wikilink")，亦進軍[北京](../Page/北京.md "wikilink")、[重慶及](../Page/重慶.md "wikilink")[澳門等地](../Page/澳門.md "wikilink")[地產市場](../Page/地產.md "wikilink")；其後專責於[商場業務](../Page/商場.md "wikilink")。

2006年10月3日香港置地宣布，蘇兆明於2007年3月31日離任行政總裁。2007年4月1日起，蘇兆明將接替[鄭明訓出任](../Page/鄭明訓.md "wikilink")[領展房地產信託基金](../Page/領展房地產信託基金.md "wikilink")[董事會主席一職](../Page/董事會主席.md "wikilink")。\[1\]由於[領展房地產信託基金管理人合規手冊所載企業管治政策之規定服務年期不可超過九年](../Page/領展房地產信託基金.md "wikilink")。因此，蘇兆明已在服務領展九年後於2016年3月31日退任。\[2\]

蘇兆明於2016年4月起擔任[獅子山學會主席](../Page/獅子山學會.md "wikilink")。\[3\]

## 公職

  - [亞洲青年管弦樂團董事會主席](../Page/亞洲青年管弦樂團.md "wikilink")
  - [青年藝術協會有限公司行政委員會成員](../Page/青年藝術協會有限公司.md "wikilink")
  - 2022 Foundation Ltd董事
  - [香港總商會人力委員會成員](../Page/香港總商會.md "wikilink")
  - [英基學校協會](../Page/英基學校協會.md "wikilink")(香港總商會代表)成員
  - 財資市場公會議會成員

## 資料來源

  - [蘇兆明作風進取置地積極擴張](http://news.sina.com.hk/cgi-bin/news/show_news.cgi?ct=headlines&type=headlines&date=2007-01-30&id=2321928)
    明報，2007年1月30日
  - [蘇兆明與TCI同聲同氣](http://www.worldjournal.com/wj-hk-news.php?nt_seq_id=1480842)
    世界日報
  - [領匯易帥商戶恐大加租](https://web.archive.org/web/20070311194001/http://www.takungpao.com/news/07/01/30/GW-685733.htm)
    大公報，2007年1月30日

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:英國商人](../Category/英國商人.md "wikilink")
[Category:英國人](../Category/英國人.md "wikilink")
[Category:怡和](../Category/怡和.md "wikilink")
[Category:劍橋大學岡維爾與凱斯學院校友](../Category/劍橋大學岡維爾與凱斯學院校友.md "wikilink")
[Category:萊斯特大學校友](../Category/萊斯特大學校友.md "wikilink")

1.  [領展房地產信託基金公告](http://www.hkexnews.hk/listedco/listconews/SEHK/2016/0107/LTN201601071336_C.pdf)
2.  [聶雅倫接任領展主席](http://m.mingpao.com/php/mpfnewscontent.php?group=FinanceAndEconomics&file=20160108/news/ww_ww1.htm)
3.