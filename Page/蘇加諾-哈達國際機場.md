[Sukarno_hatta_airport_-_Terminal_-_Jakarta_-_Indonesia.jpg](https://zh.wikipedia.org/wiki/File:Sukarno_hatta_airport_-_Terminal_-_Jakarta_-_Indonesia.jpg "fig:Sukarno_hatta_airport_-_Terminal_-_Jakarta_-_Indonesia.jpg")
[Jakarta_Airport_200507-2.jpg](https://zh.wikipedia.org/wiki/File:Jakarta_Airport_200507-2.jpg "fig:Jakarta_Airport_200507-2.jpg")
[Jakarta_Airport_200507-1.jpg](https://zh.wikipedia.org/wiki/File:Jakarta_Airport_200507-1.jpg "fig:Jakarta_Airport_200507-1.jpg")
[Jakarta_Airport_Dept_area.jpg](https://zh.wikipedia.org/wiki/File:Jakarta_Airport_Dept_area.jpg "fig:Jakarta_Airport_Dept_area.jpg")
[Check_in_hall.JPG](https://zh.wikipedia.org/wiki/File:Check_in_hall.JPG "fig:Check_in_hall.JPG")
[Terminal2cgk2.jpg](https://zh.wikipedia.org/wiki/File:Terminal2cgk2.jpg "fig:Terminal2cgk2.jpg")
[CGK_T2_check_in.jpg](https://zh.wikipedia.org/wiki/File:CGK_T2_check_in.jpg "fig:CGK_T2_check_in.jpg")
[Sukarno_hatta_airport_-_Entrance_-_Jakarta_-_Indonesia.jpg](https://zh.wikipedia.org/wiki/File:Sukarno_hatta_airport_-_Entrance_-_Jakarta_-_Indonesia.jpg "fig:Sukarno_hatta_airport_-_Entrance_-_Jakarta_-_Indonesia.jpg")

**蘇加諾-哈達國際機場**（，）位於[印尼首都](../Page/印尼.md "wikilink")[雅加達以西](../Page/雅加達.md "wikilink")20公里，於1985年4月開始營運，名字來源於印尼[開國總統](../Page/印尼總統.md "wikilink")[蘇加諾和副總統](../Page/蘇加諾.md "wikilink")[穆罕默德·哈达](../Page/穆罕默德·哈达.md "wikilink")。機場建築以印尼王宮為範本。[印尼航空公司以該機場作為營運樞紐](../Page/印尼航空公司.md "wikilink")，該機場有許多國際航空在此設站，也成為亞洲轉運中心之一。2008年被列為全世界第8個最有特色的機場，2013年被列為全世界第10個繁忙機場。

## 航空公司與目的地

### 客运

<table>
<thead>
<tr class="header">
<th><p>航空公司</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/印尼鹰航.md" title="wikilink">印尼鹰航</a></p></td>
<td><p><a href="../Page/巴蒂穆拉国际机场.md" title="wikilink">安汶</a>、<a href="../Page/苏丹阿吉·穆罕默德·苏莱曼国际机场.md" title="wikilink">巴厘巴板</a>、<a href="../Page/苏丹伊斯坎达·穆达国际机场.md" title="wikilink">班达亚齐</a>、<a href="../Page/雷丁·印甸二世国际机场.md" title="wikilink">班达楠榜</a>、、<a href="../Page/韩那丁国际机场.md" title="wikilink">巴淡</a>、<a href="../Page/法玛瓦蒂·苏加诺机场.md" title="wikilink">明古鲁</a>、<a href="../Page/弗兰斯·凯西波国际机场.md" title="wikilink">比亚克</a>、<a href="../Page/伍拉·赖国际机场.md" title="wikilink">登巴萨</a>、<a href="../Page/苏丹塔哈机场.md" title="wikilink">占碑</a>、<a href="../Page/仙谷国际机场.md" title="wikilink">查亚普拉</a>、<a href="../Page/苏丹哈桑丁国际机场.md" title="wikilink">望加锡</a>、<a href="../Page/萨姆·拉图兰吉国际机场.md" title="wikilink">万鸦老</a>、<a href="../Page/阿卜杜勒·拉赫曼·萨利赫机场.md" title="wikilink">玛琅</a>、<a href="../Page/龙目国际机场.md" title="wikilink">马塔兰</a>、<a href="../Page/瓜拉纳穆国际机场.md" title="wikilink">棉兰</a>、<a href="../Page/莫帕国际机场.md" title="wikilink">马老奇</a>、<a href="../Page/米南加保国际机场.md" title="wikilink">巴东</a>、<a href="../Page/吉利里乌机场.md" title="wikilink">帕朗卡拉亚</a>、<a href="../Page/苏丹马赫穆德·巴达鲁丁二世国际机场.md" title="wikilink">巨港</a>、<a href="../Page/穆提亚拉西斯朱弗里机场.md" title="wikilink">帕卢</a>、<a href="../Page/德帕蒂·阿米尔机场.md" title="wikilink">邦加槟港</a>、<a href="../Page/苏丹谢里夫·卡西姆二世国际机场.md" title="wikilink">北干巴鲁</a>、<a href="../Page/苏巴迪奥国际机场.md" title="wikilink">坤甸</a>、、<a href="../Page/阿迪·苏马尔莫国际机场.md" title="wikilink">梭罗</a>、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/拉贾·哈吉·斐萨比里拉国际机场.md" title="wikilink">丹戎槟榔</a>、<a href="../Page/苏丹巴布拉机场.md" title="wikilink">特尔纳特</a>、<a href="../Page/莫泽什吉朗因国际机场.md" title="wikilink">蒂米卡</a>、<a href="../Page/阿迪苏吉普托国际机场.md" title="wikilink">日惹</a>、<a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a>、<a href="../Page/素万那普机场.md" title="wikilink">曼谷-素万那普</a>、<a href="../Page/北京首都国际机场.md" title="wikilink">北京-首都</a>、<a href="../Page/上海浦东国际机场.md" title="wikilink">上海-浦东</a>、<a href="../Page/广州白云国际机场.md" title="wikilink">广州</a>、<a href="../Page/香港国际机场.md" title="wikilink">香港</a>、<a href="../Page/仁川国际机场.md" title="wikilink">首尔-仁川</a>、<a href="../Page/东京国际机场.md" title="wikilink">东京-羽田</a>、<a href="../Page/关西国际机场.md" title="wikilink">大阪-关西</a>、<a href="../Page/中部國際機場.md" title="wikilink">名古屋-中部</a>、<a href="../Page/贾特拉帕蒂·希瓦吉国际机场.md" title="wikilink">孟买</a>、<a href="../Page/阿卜杜勒-阿齐兹国王国际机场.md" title="wikilink">吉达</a>、<a href="../Page/穆罕默德·本·阿卜杜勒-阿齐兹亲王国际机场.md" title="wikilink">麦地那</a>、<a href="../Page/阿姆斯特丹斯希普霍尔机场.md" title="wikilink">阿姆斯特丹</a>、<a href="../Page/伦敦希思罗机场.md" title="wikilink">伦敦-希思罗</a>、<a href="../Page/悉尼机场.md" title="wikilink">悉尼</a>、<a href="../Page/墨尔本机场.md" title="wikilink">墨尔本</a>、<a href="../Page/珀斯机场.md" title="wikilink">珀斯</a><br />
<strong>包机</strong>：<a href="../Page/桂林两江国际机场.md" title="wikilink">桂林</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印尼鹰航.md" title="wikilink">印尼鹰航</a><br />
由探索喷射机航空运营</p></td>
<td><p><a href="../Page/布林宾萨里机场.md" title="wikilink">外南梦</a>、<a href="../Page/埃尔·塔里国际机场.md" title="wikilink">古邦</a>、<a href="../Page/科莫多机场.md" title="wikilink">拉布汉巴焦</a>、、、<a href="../Page/H.A.S.哈南柔丁国际机场.md" title="wikilink">丹戎潘丹</a>、<a href="../Page/朱瓦塔国际机场.md" title="wikilink">打拉根</a><br />
<strong>包机</strong>：<a href="../Page/圣诞岛机场.md" title="wikilink">圣诞岛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴泽航空.md" title="wikilink">巴泽航空</a></p></td>
<td><p><a href="../Page/巴蒂穆拉国际机场.md" title="wikilink">安汶</a>、<a href="../Page/苏丹阿吉·穆罕默德·苏莱曼国际机场.md" title="wikilink">巴厘巴板</a>、<a href="../Page/苏丹伊斯坎达·穆达国际机场.md" title="wikilink">班达亚齐</a>、<a href="../Page/雷丁·印甸二世国际机场.md" title="wikilink">班达楠榜</a>、、<a href="../Page/韩那丁国际机场.md" title="wikilink">巴淡</a>、<a href="../Page/伍拉·赖国际机场.md" title="wikilink">登巴萨</a>、<a href="../Page/加拉鲁丁机场.md" title="wikilink">哥伦打洛</a>、<a href="../Page/苏丹塔哈机场.md" title="wikilink">占碑</a>、<a href="../Page/仙谷国际机场.md" title="wikilink">查亚普拉</a>、<a href="../Page/哈罗柳机场.md" title="wikilink">肯达里</a>、<a href="../Page/埃尔·塔里国际机场.md" title="wikilink">古邦</a>、<a href="../Page/科莫多机场.md" title="wikilink">拉布汉巴焦</a>、<a href="../Page/希兰帕里机场.md" title="wikilink">卢布林高</a>、<a href="../Page/苏丹哈桑丁国际机场.md" title="wikilink">望加锡</a>、<a href="../Page/萨姆·拉图兰吉国际机场.md" title="wikilink">万鸦老</a>、<a href="../Page/任代尼机场.md" title="wikilink">马诺夸里</a>、<a href="../Page/龙目国际机场.md" title="wikilink">马塔兰</a>、<a href="../Page/瓜拉纳穆国际机场.md" title="wikilink">棉兰</a>、<a href="../Page/苏丹马赫穆德·巴达鲁丁二世国际机场.md" title="wikilink">巨港</a>、<a href="../Page/穆提亚拉西斯朱弗里机场.md" title="wikilink">帕卢</a>、<a href="../Page/苏丹谢里夫·卡西姆二世国际机场.md" title="wikilink">北干巴鲁</a>、<a href="../Page/苏巴迪奥国际机场.md" title="wikilink">坤甸</a>、、、<a href="../Page/多米尼克爱德华·奥索机场.md" title="wikilink">索龙</a>、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/朱瓦塔国际机场.md" title="wikilink">打拉根</a>、<a href="../Page/苏丹巴布拉机场.md" title="wikilink">特尔纳特</a>、<a href="../Page/阿迪苏吉普托国际机场.md" title="wikilink">日惹</a>、<a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a>、<a href="../Page/亚庇国际机场.md" title="wikilink">亚庇</a><br />
<strong>包机：</strong><a href="../Page/桂林两江国际机场.md" title="wikilink">桂林</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/连城航空.md" title="wikilink">连城航空</a></p></td>
<td><p><a href="../Page/苏丹阿吉·穆罕默德·苏莱曼国际机场.md" title="wikilink">巴厘巴板</a>、、<a href="../Page/韩那丁国际机场.md" title="wikilink">巴淡</a>、<a href="../Page/法玛瓦蒂·苏加诺机场.md" title="wikilink">明古鲁</a>、<a href="../Page/伍拉·赖国际机场.md" title="wikilink">登巴萨</a>、<a href="../Page/苏丹塔哈机场.md" title="wikilink">占碑</a>、<a href="../Page/苏丹哈桑丁国际机场.md" title="wikilink">望加锡</a>、<a href="../Page/阿卜杜勒·拉赫曼·萨利赫机场.md" title="wikilink">玛琅</a>、<a href="../Page/萨姆·拉图兰吉国际机场.md" title="wikilink">万鸦老</a>、<a href="../Page/瓜拉纳穆国际机场.md" title="wikilink">棉兰</a>、<a href="../Page/米南加保国际机场.md" title="wikilink">巴东</a>、<a href="../Page/苏丹马赫穆德·巴达鲁丁二世国际机场.md" title="wikilink">巨港</a>、<a href="../Page/德帕蒂·阿米尔机场.md" title="wikilink">邦加槟港</a>、<a href="../Page/苏丹谢里夫·卡西姆二世国际机场.md" title="wikilink">北干巴鲁</a>、<a href="../Page/苏巴迪奥国际机场.md" title="wikilink">坤甸</a>、、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/H.A.S.哈南柔丁国际机场.md" title="wikilink">丹戎潘丹</a>、<a href="../Page/阿迪苏吉普托国际机场.md" title="wikilink">日惹</a>、<a href="../Page/尼古劳·洛巴托总统国际机场.md" title="wikilink">帝力</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/印尼亚洲航空.md" title="wikilink">印尼亚洲航空</a></p></td>
<td><p><a href="../Page/伍拉·赖国际机场.md" title="wikilink">登巴萨</a>、<a href="../Page/瓜拉纳穆国际机场.md" title="wikilink">棉兰</a>、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/阿迪苏吉普托国际机场.md" title="wikilink">日惹</a>、<a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a>、<a href="../Page/槟城国际机场.md" title="wikilink">槟城</a>、<a href="../Page/廊曼国际机场.md" title="wikilink">曼谷-廊曼</a>、<a href="../Page/澳门国际机场.md" title="wikilink">澳门</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印尼全亞洲航空.md" title="wikilink">印尼亚洲航空长途公司</a></p></td>
<td><p><a href="../Page/伍拉·赖国际机场.md" title="wikilink">登巴萨</a>、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a>、<a href="../Page/槟城国际机场.md" title="wikilink">槟城</a>、<a href="../Page/廊曼国际机场.md" title="wikilink">曼谷-廊曼</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/印尼狮航.md" title="wikilink">印尼狮航</a></p></td>
<td><p><a href="../Page/巴蒂穆拉国际机场.md" title="wikilink">安汶</a>、<a href="../Page/苏丹阿吉·穆罕默德·苏莱曼国际机场.md" title="wikilink">巴厘巴板</a>、<a href="../Page/雷丁·印甸二世国际机场.md" title="wikilink">班达楠榜</a>、、<a href="../Page/韩那丁国际机场.md" title="wikilink">巴淡</a>、<a href="../Page/法玛瓦蒂·苏加诺机场.md" title="wikilink">明古鲁</a>、<a href="../Page/伍拉·赖国际机场.md" title="wikilink">登巴萨</a>、<a href="../Page/苏丹塔哈机场.md" title="wikilink">占碑</a>、<a href="../Page/仙谷国际机场.md" title="wikilink">查亚普拉</a>、<a href="../Page/哈罗柳机场.md" title="wikilink">肯达里</a>、<a href="../Page/埃尔·塔里国际机场.md" title="wikilink">古邦</a>、<a href="../Page/苏丹哈桑丁国际机场.md" title="wikilink">望加锡</a>、<a href="../Page/阿卜杜勒·拉赫曼·萨利赫机场.md" title="wikilink">玛琅</a>、<a href="../Page/萨姆·拉图兰吉国际机场.md" title="wikilink">万鸦老</a>、<a href="../Page/龙目国际机场.md" title="wikilink">马塔兰</a>、<a href="../Page/瓜拉纳穆国际机场.md" title="wikilink">棉兰</a>、<a href="../Page/米南加保国际机场.md" title="wikilink">巴东</a>、<a href="../Page/吉利里乌机场.md" title="wikilink">帕朗卡拉亚</a>、<a href="../Page/苏丹马赫穆德·巴达鲁丁二世国际机场.md" title="wikilink">巨港</a>、<a href="../Page/德帕蒂·阿米尔机场.md" title="wikilink">邦加槟港</a>、<a href="../Page/穆提亚拉西斯朱弗里机场.md" title="wikilink">帕卢</a>、<a href="../Page/苏丹谢里夫·卡西姆二世国际机场.md" title="wikilink">北干巴鲁</a>、<a href="../Page/苏巴迪奥国际机场.md" title="wikilink">坤甸</a>、、<a href="../Page/阿迪·苏马尔莫国际机场.md" title="wikilink">梭罗</a>、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/H.A.S.哈南柔丁国际机场.md" title="wikilink">丹戎潘丹</a>、<a href="../Page/拉贾·哈吉·斐萨比里拉国际机场.md" title="wikilink">丹戎槟榔</a>、<a href="../Page/朱瓦塔国际机场.md" title="wikilink">打拉根</a>、<a href="../Page/苏丹巴布拉机场.md" title="wikilink">特尔纳特</a>、<a href="../Page/阿迪苏吉普托国际机场.md" title="wikilink">日惹</a>、<a href="../Page/阿卜杜勒-阿齐兹国王国际机场.md" title="wikilink">吉达</a><a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a>、<a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a><br />
<strong>包机</strong>：<a href="../Page/海口美兰国际机场.md" title="wikilink">海口</a>、<a href="../Page/三亚凤凰国际机场.md" title="wikilink">三亚</a>[1]</p></td>
</tr>
<tr class="even">
<td><p></p></td>
<td><p><a href="../Page/布林宾萨里机场.md" title="wikilink">外南梦</a>、<a href="../Page/法玛瓦蒂·苏加诺机场.md" title="wikilink">明古鲁</a>、<a href="../Page/希兰帕里机场.md" title="wikilink">卢布林高</a>、<a href="../Page/苏丹马赫穆德·巴达鲁丁二世国际机场.md" title="wikilink">巨港</a>、<a href="../Page/德帕蒂·阿米尔机场.md" title="wikilink">邦加槟港</a>、<a href="../Page/伊斯坎达尔机场.md" title="wikilink">庞卡兰布翁</a>、<a href="../Page/苏巴迪奥国际机场.md" title="wikilink">坤甸</a>、、、<a href="../Page/阿迪·苏马尔莫国际机场.md" title="wikilink">梭罗</a>、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/多米尼克爱德华·奥索机场.md" title="wikilink">索龙</a>、<a href="../Page/H.A.S.哈南柔丁国际机场.md" title="wikilink">丹戎潘丹</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三佛齐航空.md" title="wikilink">三佛齐航空</a></p></td>
<td><p><a href="../Page/苏丹阿吉·穆罕默德·苏莱曼国际机场.md" title="wikilink">巴厘巴板</a>、<a href="../Page/雷丁·印甸二世国际机场.md" title="wikilink">班达楠榜</a>、<a href="../Page/韩那丁国际机场.md" title="wikilink">巴淡</a>、<a href="../Page/伍拉·赖国际机场.md" title="wikilink">登巴萨</a>、<a href="../Page/仙谷国际机场.md" title="wikilink">查亚普拉</a>、<a href="../Page/苏丹塔哈机场.md" title="wikilink">占碑</a>、<a href="../Page/苏丹哈桑丁国际机场.md" title="wikilink">望加锡</a>、<a href="../Page/阿卜杜勒·拉赫曼·萨利赫机场.md" title="wikilink">玛琅</a>、<a href="../Page/瓜拉纳穆国际机场.md" title="wikilink">棉兰</a>、<a href="../Page/米南加保国际机场.md" title="wikilink">巴东</a>、<a href="../Page/苏丹马赫穆德·巴达鲁丁二世国际机场.md" title="wikilink">巨港</a>、<a href="../Page/德帕蒂·阿米尔机场.md" title="wikilink">邦加槟港</a>、<a href="../Page/苏巴迪奥国际机场.md" title="wikilink">坤甸</a>、、、<a href="../Page/阿迪·苏马尔莫国际机场.md" title="wikilink">梭罗</a>、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/H.A.S.哈南柔丁国际机场.md" title="wikilink">丹戎潘丹</a>、<a href="../Page/拉贾·哈吉·斐萨比里拉国际机场.md" title="wikilink">丹戎槟榔</a>、<a href="../Page/苏丹巴布拉机场.md" title="wikilink">特尔纳特</a>、<a href="../Page/阿迪苏吉普托国际机场.md" title="wikilink">日惹</a><br />
<strong>包机</strong>：<a href="../Page/张家界荷花国际机场.md" title="wikilink">张家界</a>、<a href="../Page/梅县长岗岌机场.md" title="wikilink">梅州</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/航星航空.md" title="wikilink">航星航空</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/印尼快捷航空.md" title="wikilink">印尼快捷航空</a></p></td>
<td><p><a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/特里嘎纳航空.md" title="wikilink">特里嘎纳航空</a></p></td>
<td><p><a href="../Page/伊斯坎达尔机场.md" title="wikilink">庞卡兰布翁</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/快速航空.md" title="wikilink">快速航空</a></p></td>
<td><p><a href="../Page/多米尼克爱德华·奥索机场.md" title="wikilink">索龙</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/捷星亚洲航空.md" title="wikilink">捷星亚洲航空</a></p></td>
<td><p><a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/酷航.md" title="wikilink">酷航</a></p></td>
<td><p><a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马来西亚航空.md" title="wikilink">马来西亚航空</a></p></td>
<td><p><a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马印航空.md" title="wikilink">马印航空</a></p></td>
<td><p><a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亚洲航空.md" title="wikilink">亚洲航空</a></p></td>
<td><p><a href="../Page/士乃国际机场.md" title="wikilink">新山</a>、<a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/泰国国际航空.md" title="wikilink">泰国国际航空</a></p></td>
<td><p><a href="../Page/素万那普机场.md" title="wikilink">曼谷-素万那普</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/捷特亚洲航空.md" title="wikilink">捷特亚洲航空</a></p></td>
<td><p><a href="../Page/素万那普机场.md" title="wikilink">曼谷-素万那普</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/泰国狮子航空.md" title="wikilink">泰国狮子航空</a></p></td>
<td><p><a href="../Page/廊曼国际机场.md" title="wikilink">曼谷-廊曼</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/越南航空.md" title="wikilink">越南航空</a></p></td>
<td><p><a href="../Page/新山一国际机场.md" title="wikilink">胡志明市</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/越捷航空.md" title="wikilink">越捷航空</a></p></td>
<td><p><a href="../Page/新山一国际机场.md" title="wikilink">胡志明市</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/文莱皇家航空.md" title="wikilink">文莱皇家航空</a></p></td>
<td><p><a href="../Page/文莱国际机场.md" title="wikilink">斯里巴加湾市</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/菲律宾航空.md" title="wikilink">菲律宾航空</a></p></td>
<td><p><a href="../Page/尼诺伊·阿基诺国际机场.md" title="wikilink">马尼拉</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/菲律宾亚洲航空.md" title="wikilink">菲律宾亚洲航空</a></p></td>
<td><p><a href="../Page/尼诺伊·阿基诺国际机场.md" title="wikilink">马尼拉</a>[2]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宿务太平洋航空.md" title="wikilink">宿务太平洋航空</a></p></td>
<td><p><a href="../Page/尼诺伊·阿基诺国际机场.md" title="wikilink">马尼拉</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中华航空.md" title="wikilink">中华航空</a></p></td>
<td><p><a href="../Page/香港国际机场.md" title="wikilink">香港</a>、<a href="../Page/台湾桃园国际机场.md" title="wikilink">台北-桃园</a>、<a href="../Page/小港国际机场.md" title="wikilink">高雄</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/长荣航空.md" title="wikilink">长荣航空</a></p></td>
<td><p><a href="../Page/台湾桃园国际机场.md" title="wikilink">台北-桃园</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中国国际航空.md" title="wikilink">中国国际航空</a></p></td>
<td><p><a href="../Page/北京首都国际机场.md" title="wikilink">北京-首都</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/首都航空.md" title="wikilink">首都航空</a></p></td>
<td><p><a href="../Page/海口美兰国际机场.md" title="wikilink">海口</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/深圳航空.md" title="wikilink">深圳航空</a></p></td>
<td><p><a href="../Page/深圳寶安国际机场.md" title="wikilink">深圳</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中国东方航空.md" title="wikilink">中国东方航空</a></p></td>
<td><p><a href="../Page/上海浦东国际机场.md" title="wikilink">上海-浦东</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中国南方航空.md" title="wikilink">中国南方航空</a></p></td>
<td><p><a href="../Page/广州白云国际机场.md" title="wikilink">广州</a>、<a href="../Page/深圳宝安国际机场.md" title="wikilink">深圳</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/祥鹏航空.md" title="wikilink">祥鹏航空</a></p></td>
<td><p><a href="../Page/昆明长水国际机场.md" title="wikilink">昆明</a>、<a href="../Page/南宁吴圩国际机场.md" title="wikilink">南宁</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/昆明航空.md" title="wikilink">昆明航空</a></p></td>
<td><p><a href="../Page/武汉天河国际机场.md" title="wikilink">武汉</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山东航空.md" title="wikilink">山东航空</a></p></td>
<td><p><a href="../Page/杭州萧山国际机场.md" title="wikilink">杭州</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西藏航空.md" title="wikilink">西藏航空</a></p></td>
<td><p><a href="../Page/西安咸阳国际机场.md" title="wikilink">西安</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/厦门航空.md" title="wikilink">厦门航空</a></p></td>
<td><p><a href="../Page/福州长乐国际机场.md" title="wikilink">福州</a>、<a href="../Page/厦门高崎国际机场.md" title="wikilink">厦门</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/国泰航空.md" title="wikilink">国泰航空</a></p></td>
<td><p><a href="../Page/香港国际机场.md" title="wikilink">香港</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大韩航空.md" title="wikilink">大韩航空</a></p></td>
<td><p><a href="../Page/仁川国际机场.md" title="wikilink">首尔-仁川</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/韩亚航空.md" title="wikilink">韩亚航空</a></p></td>
<td><p><a href="../Page/仁川国际机场.md" title="wikilink">首尔-仁川</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日本航空.md" title="wikilink">日本航空</a></p></td>
<td><p><a href="../Page/成田国际机场.md" title="wikilink">东京-成田</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/全日本空輸.md" title="wikilink">全日本空輸</a></p></td>
<td><p><a href="../Page/东京国际机场.md" title="wikilink">东京-羽田</a>、<a href="../Page/成田国际机场.md" title="wikilink">东京-成田</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/澳洲航空.md" title="wikilink">澳洲航空</a></p></td>
<td><p><a href="../Page/悉尼机场.md" title="wikilink">悉尼</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯里兰卡航空.md" title="wikilink">斯里兰卡航空</a></p></td>
<td><p><a href="../Page/班达拉奈克国际机场.md" title="wikilink">科伦坡</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿联酋航空.md" title="wikilink">阿联酋航空</a></p></td>
<td><p><a href="../Page/迪拜国际机场.md" title="wikilink">迪拜-国际</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿提哈德航空.md" title="wikilink">阿提哈德航空</a></p></td>
<td><p><a href="../Page/阿布扎比国际机场.md" title="wikilink">阿布扎比</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沙特阿拉伯航空.md" title="wikilink">沙特阿拉伯航空</a></p></td>
<td><p><a href="../Page/阿卜杜勒-阿齐兹国王国际机场.md" title="wikilink">吉达</a>、<a href="../Page/穆罕默德·本·阿卜杜勒-阿齐兹亲王国际机场.md" title="wikilink">麦地那</a>、<a href="../Page/哈立德国王国际机场.md" title="wikilink">利雅得</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/纳斯航空.md" title="wikilink">纳斯航空</a></p></td>
<td><p><strong>包机</strong>：<a href="../Page/阿卜杜勒-阿齐兹国王国际机场.md" title="wikilink">吉达</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡塔尔航空.md" title="wikilink">卡塔尔航空</a></p></td>
<td><p><a href="../Page/哈马德国际机场.md" title="wikilink">多哈</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿曼航空.md" title="wikilink">阿曼航空</a></p></td>
<td><p><a href="../Page/马斯喀特国际机场.md" title="wikilink">马斯喀特</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/土耳其航空.md" title="wikilink">土耳其航空</a></p></td>
<td><p><a href="../Page/伊斯坦布尔阿塔图尔克机场.md" title="wikilink">伊斯坦布尔-阿塔图尔克</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/荷兰皇家航空.md" title="wikilink">荷兰皇家航空</a></p></td>
<td><p><a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a>、<a href="../Page/阿姆斯特丹斯希普霍尔机场.md" title="wikilink">阿姆斯特丹</a></p></td>
</tr>
</tbody>
</table>

### 货运

<table>
<thead>
<tr class="header">
<th><p>航空公司</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/印尼鹰航.md" title="wikilink">印尼鹰航货运</a></p></td>
<td><p><a href="../Page/韩那丁国际机场.md" title="wikilink">巴淡</a>、<a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/关西国际机场.md" title="wikilink">大阪-关西</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/苏丹阿吉·穆罕默德·苏莱曼国际机场.md" title="wikilink">巴厘巴板</a>、<a href="../Page/米南加保国际机场.md" title="wikilink">巴东</a>、<a href="../Page/苏丹谢里夫·卡西姆二世国际机场.md" title="wikilink">北干巴鲁</a>、<a href="../Page/瓜拉纳穆国际机场.md" title="wikilink">棉兰</a>、<a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/素万那普机场.md" title="wikilink">曼谷-素万那普</a>、<a href="../Page/内排国际机场.md" title="wikilink">河内</a>、<a href="../Page/仁川国际机场.md" title="wikilink">首尔-仁川</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/苏丹阿吉·穆罕默德·苏莱曼国际机场.md" title="wikilink">巴厘巴板</a>、<a href="../Page/苏丹哈桑丁国际机场.md" title="wikilink">望加锡</a>、<a href="../Page/朱安达国际机场.md" title="wikilink">泗水</a>、<a href="../Page/阿迪·苏马尔莫国际机场.md" title="wikilink">梭罗</a>、<a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/韩那丁国际机场.md" title="wikilink">巴淡</a>、<a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a></p></td>
</tr>
<tr class="odd">
<td><p>亚洲连接货运航空<br />
Asialink Cargo Airlines</p></td>
<td><p><a href="../Page/德帕蒂·阿米尔机场.md" title="wikilink">邦加槟港</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新加坡航空货运.md" title="wikilink">新加坡航空货运</a></p></td>
<td><p><a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马来西亚航空货运.md" title="wikilink">马来西亚航空货运</a></p></td>
<td><p><a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/拉亚航空.md" title="wikilink">拉亚航空</a></p></td>
<td><p><a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡-国际</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/素万那普机场.md" title="wikilink">曼谷-素万那普</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中华航空#货运服务.md" title="wikilink">中华航空货运</a></p></td>
<td><p><a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/台湾桃园国际机场.md" title="wikilink">台北-桃园</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/国泰货运.md" title="wikilink">国泰货运</a></p></td>
<td><p><a href="../Page/内排国际机场.md" title="wikilink">河内</a>、<a href="../Page/香港国际机场.md" title="wikilink">香港</a>、<a href="../Page/槟城国际机场.md" title="wikilink">槟城</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/全日空航空.md" title="wikilink">全日空货运</a></p></td>
<td><p><a href="../Page/成田国际机场.md" title="wikilink">东京-成田</a>[3]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大韩航空.md" title="wikilink">大韩航空货运</a></p></td>
<td><p><a href="../Page/槟城国际机场.md" title="wikilink">槟城</a>、<a href="../Page/新山一国际机场.md" title="wikilink">胡志明市</a>、<a href="../Page/仁川国际机场.md" title="wikilink">首尔-仁川</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/汉莎货运航空.md" title="wikilink">汉莎货运航空</a></p></td>
<td><p><a href="../Page/法兰克福机场.md" title="wikilink">法兰克福</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亚特兰大冰岛航空.md" title="wikilink">亚特兰大冰岛航空</a></p></td>
<td><p><a href="../Page/凯夫拉维克国际机场.md" title="wikilink">雷克雅未克-凯夫拉维克</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空桥货运航空.md" title="wikilink">空桥货运航空</a></p></td>
<td><p><a href="../Page/谢列梅捷沃国际机场.md" title="wikilink">莫斯科-谢列梅捷沃</a></p></td>
</tr>
<tr class="even">
<td><p>空环货运航空<br />
Aerotrans Cargo</p></td>
<td><p><a href="../Page/香港国际机场.md" title="wikilink">香港</a>、<a href="../Page/特拉布宗机场.md" title="wikilink">特拉布宗</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/联邦快递.md" title="wikilink">联邦快递</a></p></td>
<td><p><a href="../Page/新加坡樟宜机场.md" title="wikilink">新加坡</a>、<a href="../Page/新山一国际机场.md" title="wikilink">胡志明市</a>、<a href="../Page/广州白云国际机场.md" title="wikilink">广州</a></p></td>
</tr>
<tr class="even">
<td><p><br />
由<a href="../Page/亚特拉斯航空.md" title="wikilink">亚特拉斯航空运营</a></p></td>
<td><p><a href="../Page/悉尼机场.md" title="wikilink">悉尼</a></p></td>
</tr>
</tbody>
</table>

## 资料来源

## 外部連結

  - [蘇卡諾-哈達國際機場](http://soekarnohatta-airport.co.id/en/home)

  -
[Category:印度尼西亞機場](../Category/印度尼西亞機場.md "wikilink")
[Category:1985年启用的机场](../Category/1985年启用的机场.md "wikilink")

1.
2.
3.