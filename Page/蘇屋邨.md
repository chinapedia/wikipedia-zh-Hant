[So_Uk_Estate_Phase_1_(2016).jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Estate_Phase_1_\(2016\).jpg "fig:So_Uk_Estate_Phase_1_(2016).jpg")
[Orchid_House_So_Uk_Estate.jpg](https://zh.wikipedia.org/wiki/File:Orchid_House_So_Uk_Estate.jpg "fig:Orchid_House_So_Uk_Estate.jpg")
[So_Uk_Estate_Basketball_Court_(sunlight).jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Estate_Basketball_Court_\(sunlight\).jpg "fig:So_Uk_Estate_Basketball_Court_(sunlight).jpg")
[So_Uk_Estate_Badminton_Court_(sunlight).jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Estate_Badminton_Court_\(sunlight\).jpg "fig:So_Uk_Estate_Badminton_Court_(sunlight).jpg")
[So_Uk_Estate_Table_Tennis_play_area.jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Estate_Table_Tennis_play_area.jpg "fig:So_Uk_Estate_Table_Tennis_play_area.jpg")
[So_Uk_Estate_Community_Play_Area_1.jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Estate_Community_Play_Area_1.jpg "fig:So_Uk_Estate_Community_Play_Area_1.jpg")
[So_Uk_Estate_Community_Play_Area_5.jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Estate_Community_Play_Area_5.jpg "fig:So_Uk_Estate_Community_Play_Area_5.jpg")
[So_Uk_Estate_Pebble_Walking_Trail.jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Estate_Pebble_Walking_Trail.jpg "fig:So_Uk_Estate_Pebble_Walking_Trail.jpg")
[So_Uk_Estate_sitting_area.jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Estate_sitting_area.jpg "fig:So_Uk_Estate_sitting_area.jpg")
[SoUkPostOffice2018.jpg](https://zh.wikipedia.org/wiki/File:SoUkPostOffice2018.jpg "fig:SoUkPostOffice2018.jpg")
[So_Uk_Bus_Terminus_(clear_view_and_blue_sky).jpg](https://zh.wikipedia.org/wiki/File:So_Uk_Bus_Terminus_\(clear_view_and_blue_sky\).jpg "fig:So_Uk_Bus_Terminus_(clear_view_and_blue_sky).jpg")\]\]
**蘇屋邨**（）是[香港的公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[長沙灣](../Page/長沙灣.md "wikilink")[尖山南面](../Page/尖山_\(香港\).md "wikilink")，是[香港房屋委員會歷史最悠久的公共屋邨之一](../Page/香港房屋委員會.md "wikilink")。蘇屋邨是房委會前身[香港屋宇建設委員會繼](../Page/香港屋宇建設委員會.md "wikilink")[北角邨及](../Page/北角邨.md "wikilink")[西環邨後策劃建設的第三個屋邨](../Page/西環邨.md "wikilink")，當時獲譽為[遠東規模最大的地區性住宅計劃](../Page/遠東.md "wikilink")\[1\]。

蘇屋邨於2006年開始計劃分兩期重建（項目編號：SP01），於2009年10月22日封閉第一期，2011年完成清拆，2016
年9月第1期首6幢樓宇重建完成入伙；至2012年年中全邨遷出，於同年11月26日起封閉待清拆，全邨正式進行重建。重建計劃共分2期進行，共有14幢住宅大廈，項目是由房屋署總建築師（1）進行總體設計，細部設計則以外判服務合約方式判給了周林建築師有限公司作詳細設計，全邨均由[瑞安建業有限公司承建](../Page/瑞安建業.md "wikilink")，而大廈以非標準型設計，結構外型分別為6幢長型、4幢L型、3幢T型及1幢相連I型大廈；住宅單位則採用構件式單位設計（Modular
Flat Design），主要提供1/2人，2/3人，1睡房及2睡房出租公屋單位。單位室內面積由14.1-38.0平方米不等。

## 歷史

深水埗區議會出版的《從深水步到深水埗》 認為蘇屋邨的前身蘇屋村大概在清朝時候建村。

### 興建

蘇屋邨的地盤平整工程於1957年初展開，並於1958年4月完成。當中由[徙置事務處先遷拆](../Page/徙置事務處.md "wikilink")19間[木屋及安置共](../Page/寮屋.md "wikilink")222名[居民](../Page/居民.md "wikilink")，並於稍後時間清拆另外60間木屋及安置共800名居民。

舊蘇屋邨的興建工程分五期落成，全部建築於1960年至1963年間建成，共有16幢[樓宇](../Page/樓宇.md "wikilink")。在地盤平整之前，這裏曾是一大片耕作用的[農地及](../Page/農地.md "wikilink")[寮屋區](../Page/寮屋區.md "wikilink")，佔地約7.8公頃，是當時遠東最大型的[住宅發展計劃](../Page/住宅.md "wikilink")。

### 重建

2005年9月，房屋署展開全面結構勘察計劃，目的在於了解樓齡較高屋邨的樓宇狀況，並確定所需的修葺及鞏固工程，使樓宇可繼續使用一段合理的時間（15年）。但由於維修費用過於高昂，因此房屋委員會決定重建蘇屋邨，並分兩期進行。

房屋委員會已經安排提早搬遷舊蘇屋邨的租戶，把部分家庭調遷到當時新落成的[富昌邨以及](../Page/富昌邨.md "wikilink")[海麗邨](../Page/海麗邨.md "wikilink")，空置單位亦不再出租。房委會亦安排[元州邨第](../Page/元州邨.md "wikilink")2期、第4期、[長沙灣工廠大廈重建項目](../Page/長沙灣工廠大廈.md "wikilink")（元州邨第5期）、[石硤尾邨第](../Page/石硤尾邨.md "wikilink")2期、第5期及佐敦谷[彩盈邨定為接收屋邨](../Page/彩盈邨.md "wikilink")，以接收舊蘇屋邨重建而要搬遷的居民。

2006年3月30日決定重建第一期（建於高平台的興建第3期、第4期及第5期共10座：牡丹樓、蘭花樓、壽菊樓、石竹樓、劍蘭樓、楓林樓、丁香樓、金松樓、綠柳樓及櫻桃樓），該範圍的租戶於2009年8月前遷出，2010年3月清拆；通往山上的通道亦在2009年10月22日封閉。2008年2月20日決定於2011年尾封閉第二期（百合樓、彩雀樓、荷花樓、杜鵑樓、海棠樓及茶花樓），但由於新的接收邨（元州邨第五期及石硤尾邨第二及第五期）於2012年4月底才開始落成，該範圍的租戶於2012年6月前搬遷，所以它們於2012年11月26日起清拆，即是比原定清拆計劃延後了一年。

2010年1月29日，[紅磡](../Page/紅磡.md "wikilink")[馬頭圍道](../Page/馬頭圍道.md "wikilink")45J號樓高五層的唐樓[突然倒塌](../Page/馬頭圍道唐樓倒塌事故.md "wikilink")，連同鄰近四幢唐樓（45E和45H座）也一同出現有倒塌危機，當局其後下令撤走該四幢大廈的住客、商戶和倒塌大廈的45J住戶到紅磡[聖匠堂和](../Page/聖匠堂.md "wikilink")[油麻地](../Page/油麻地.md "wikilink")[梁顯利社區中心暫住](../Page/梁顯利社區中心.md "wikilink")，並對合乎資格入住公屋的災民在2至3個月內分配入住公屋，而未合資格申請公屋的災民，原定計劃在舊蘇屋邨暫居，但舊蘇屋邨早已計劃在2009年第三季開始進行第一期重建工程，水電煤設備早於2009年8月23日停用和拆去，並在2010年3月拆卸，故此紅磡馬頭圍道45號的災民改至較遠的[石籬邨之兩座舊式徙置大廈](../Page/石籬邨.md "wikilink")（10座和11座，現作[中轉房屋之用](../Page/中轉房屋.md "wikilink")）居住。

2016年9月蘇屋邨重建第1期第一階段的6幢住宅樓宇及蘇屋社區綜合服務大樓落成入伙。至於第二期，則預計在2018年落成，並早已展開預派單位程序；但由於接受是次預派的新租戶不足，其餘單位將撥為[白田邨](../Page/白田邨.md "wikilink")9-11及13座重建受影響居民的主要安置屋邨\[2\]。

## 整體規劃

### 興建

蘇屋邨建於1960至1963年，共有16幢相連長型或Y型大廈，樓高8至18層，單位約有5,300個。

整個蘇屋邨由設計北角邨的香港已故著名[建築師](../Page/建築師.md "wikilink")[甘洺](../Page/甘洺.md "wikilink")（Eric
Cumine，1905-2002）負責整體規劃，並將項目劃分為五個小區，推薦給不同建築師共同合作，甘洺居中協調，致力制定配合多種租金選擇的間隔，及另外四家私人執業建築師樓負責不同期數的設計，所以每期的設計風格都截然不同，並順著山勢由南至北而建，使不同期數的樓宇錯落有而序散落在不同高度平台，此設計令大部分座數的單位都可向南及看到海景，並為住宅單位提供充足的[光線及](../Page/採光.md "wikilink")[空氣](../Page/通風.md "wikilink")，屋邨還提供了大量室外及有蓋保憩空間，以及在P、Q及R座地下及一樓提供兩所設有24個課室的小學，而兩所小學的業權則由政府向屋建會購買，以紓緩屋建會的投資負擔。而屋建會對蘇屋邨的總投資額為5,000萬港元。

司徒惠建築師樓負責蘇屋邨的[海水沖廁系統設計](../Page/香港供水#沖廁水供應.md "wikilink")，海水泵房位於長沙灣海濱，而海水供水管則沿興華街抽上海拔280米高的尖山水務署海水配水庫，再分配給蘇屋邨的用戶。

#### 大廈設計

蘇屋邨共有16座住宅大樓，分5期興建，而各期分別聘請不同私人建築師樓負責設計，分別為：

  - **第1期 － 杜鵑樓（S）、海棠樓（T）及茶花樓（U）**

<!-- end list -->

  -
    由利安建築師樓（Messrs. Leigh &
    Orange）設計，於1959年6月接受申請，並於1960年11月全部落成，提供了1768個4至11人房的不同大小類型單位。

<!-- end list -->

  - **第2期 － 百合樓（P）、彩雀樓（Q）及荷花樓（R）**

<!-- end list -->

  -
    由[陸謙受建築師樓](../Page/陸謙受.md "wikilink")（Mr. H. S.
    Luke）設計，於1959年8月接受申請，並於1961年1月全部落成，提供了729個6至9人房（主力為8人房）的較大型的單位。

<!-- end list -->

  - **第3期 － 楓林樓（E）、丁香樓（F）、金松樓（G）、綠柳樓（H）及櫻桃樓（I）**

<!-- end list -->

  -
    由[司徒惠建築師樓](../Page/司徒惠.md "wikilink")（Mr. W.
    Szeto）設計，於1959年5月接受申請，並於1961年4月全部落成，提供了1030個5及7人房的中小型單位。這五座大廈更是香港首批「Y」型設計的公屋大廈。

<!-- end list -->

  - **第4期 － 劍蘭樓（M）**

<!-- end list -->

  -
    周李氏建築師樓（Messrs. Chau &
    Lee）設計，於1960年9月接受申請，並於1962年4月全部落成，提供了174個6及8人房（主力為8人房）的較大型的單位。

<!-- end list -->

  - **第5期 － 牡丹樓（A）、蘭花樓（B）、壽菊樓（C）及石竹樓（D）**

<!-- end list -->

  -
    由周李氏建築師樓（Messrs. Chau &
    Lee）設計，於1961年3月接受申請，並於1963年5月全部落成，提供了1610個主力為6人房的中型單位。

最初，建築師們希望把單位分2大類，其中一類（佔全邨25%）是跟北角邨差不多有梗房（固定間隔房間）設計的，其餘的75%就是水凖較低的單位。後來因要進一步減低成本，令大部份單位也取消了梗房設計，只有杜鵑樓的最大型單位（即13、14、31、32、43、44、61、62、73及74室）才有1個大梗房及2個[露台的設計](../Page/露台.md "wikilink")。

經過建築師們就有關住宅單位類型，大小及相關的標凖的商議，達成共識。基本上蘇屋邨的目標，是要達至提供相當闊的租金等級，可行的話，面積相同的單位按不同的舒適程度及位置來訂定不同級別的租金（並非最大面積為最貴），位於較上部份的單位租金會較貴。

原蘇屋邨共提供5,311個住宅單位，可容納約31,600人。

### 重建

蘇屋邨已經有近50年歷史，是老化的屋邨，昔日的精英多已遷往新建的私人樓宇，剩下多是老弱居民。屋邨設施亦日漸老舊，有些單位天花剝落漏水，有些外牆斜裂。房屋署在2005年的全面結構勘察計劃，表示本邨樓宇結構仍然安全，但若延長蘇屋邨壽命，須進行的大型工程，包括：

1.  其中兩座大廈（櫻桃樓及綠柳樓）需要大規模加設鋼製[外柱及](../Page/外柱.md "wikilink")[橫樑](../Page/橫樑.md "wikilink")，以及鞏固[懸臂式走廊的結構](../Page/懸臂式走廊.md "wikilink")；另外六座（金松樓、彩雀樓、丁香樓、百合樓、荷花樓、楓林樓）需要把混凝土護欄更換為鋼欄，以減輕荷載，並以鋼架局部鞏固[樓板](../Page/樓板.md "wikilink")；
2.  全部十座設有懸臂式走廊的大廈（杜鵑樓、金松樓、櫻桃樓、劍蘭樓、彩雀樓、丁香樓、百合樓、荷花樓、楓林樓、綠柳樓）需要重鋪[砂漿](../Page/砂漿.md "wikilink")[地台和更換銹蝕](../Page/地台.md "wikilink")[鋼筋](../Page/鋼筋.md "wikilink")；
3.  重澆廁所及垃圾房樓板；
4.  更換天台[水缸](../Page/水缸.md "wikilink")、露台護欄橫樑及混凝土通花磚；
5.  [結構牆及飯廳](../Page/結構牆.md "wikilink")[樑板需要進行結構改善工程](../Page/樑板.md "wikilink")

房屋委員會考慮過樓宇的整體狀況（即使維修最多僅可維持十多年）、所需改善工程規模、費用（將蘇屋邨壽命增加15年的費用高達2.5億港元），以及工程可能對住戶造成的滋擾程度後，決定清拆蘇屋邨。當中會保留蘇屋邨入口[牌匾](../Page/牌匾.md "wikilink")、昔日用作售賣[火水的白色三角屋及記載整個前蘇屋邨面貌的一幅大型天花](../Page/火水.md "wikilink")[壁畫](../Page/壁畫.md "wikilink")\[3\]。

本來，房署早前宣布重建蘇屋邨後，一直有意見認為應將該地皮改建私人樓宇，以地盡其用（因為蘇屋邨的估值超過300億元，同時亦是目前市面上唯一一處大型的住宅用地）。不過，隨著金融海嘯衝擊，將公屋地皮轉為私樓的壓力降低，因應公屋需求未來將會增加，房署決定把蘇屋邨地皮重建公屋。首期[地基平整工程於](../Page/地基.md "wikilink")2010年開始，原定於2015年完工。不過早前因施工期間被驗出焊料含鉛，需要全面更換喉管，加上天雨頻繁，延誤至2016年9月入伙\[4\]，第二期地基工程則會於2014年開工，約2018年完工，另設有約10,000平方呎商場。

完成重建後的蘇屋邨提供合共6,985個住宅單位，可容納約19,000人。

## 屋邨資料

### 重建後樓宇

蘇屋邨第一期第一階段其中6座住宅大廈共2917個單位已經於2016年9月重建完成入伙，而第一期第二階段的1座住宅大廈共374個單位亦已於2018年8月重建完成入伙，另外還有第二期共有7座住宅大廈正在重建中，樓高16-41層，並會繼續沿用其中舊邨的14座以花卉命名的中英文樓宇名稱，棄用「楓林樓」及「丁香樓」名稱。其中前楓林樓將預留地下一層作展館，佈置成為一個模擬單位，放置上世紀的舊式生活用品。

| 樓宇名稱（座號）                                  | 樓宇類型                                              | 樓層數目 | 期數              | 入伙年份            |
| ----------------------------------------- | ------------------------------------------------- | ---- | --------------- | --------------- |
| 蘭花樓 Orchid House（第1座）                     | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink") （L型）     | 23   | 1               | 2016年9月 \[5\]   |
| 壽菊樓 Marigold House（第2座）                   | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink") （Z型）     |      |                 |                 |
| 牡丹樓 Peony House（第3座）                      | 22                                                |      |                 |                 |
| 金松樓 Cedar House（第4座）                      | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink") （L型）     | 29   |                 |                 |
| 綠柳樓 Willow House（第5座）                     |                                                   |      |                 |                 |
| 櫻桃樓 Cherry House（第6座）                     | 25                                                |      |                 |                 |
| 荷花樓 Lotus House（第7座）                      | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink") （T型）     | 39   | 2               | 2019年2月21日\[6\] |
| 百合樓 Lily House（第8座）                       | 40                                                |      |                 |                 |
| 彩雀樓 Larkspur House （第9座）                  | 27                                                |      |                 |                 |
| 劍蘭樓 Gladiolus House（第11A座）                | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink") （短T型雙座式） | 22   |                 |                 |
| 石竹樓 Carnation House（第11B座）                |                                                   |      |                 |                 |
| 杜鵑樓 Azalea House（第12座）                    | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink") （長型）     |      |                 |                 |
| 海棠樓 Begonia House（第13座）                   |                                                   |      |                 |                 |
| 茶花樓A座及B座 Block A\&B, Camellia House（第14座） | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink") （短I型雙座式） | 21   | 2018年8月10日\[7\] |                 |
|                                           |                                                   |      |                 |                 |

（\*註：第一期第10座為蘇屋社區綜合服務大樓、天橋及蘇屋邨辦事處大樓）

## 屋邨設施

包括有特殊幼兒中心暨早期教育及訓練中心、兒童之家、嚴重殘疾人士護理院、綜合職業康復服務中心、中度及嚴重弱智人士宿舍、展能中心、兒童遊樂場、籃球場、羽毛球場和停車場。另外，前屋邨辦事處活化後以象徵式租金租予郵政局，取代未獲續約的[李鄭屋商場郵局](../Page/李鄭屋商場.md "wikilink")。

屋苑設有商場，名為[蘇屋商場](../Page/蘇屋商場.md "wikilink")，位於保安道／長發街交界，將設有便利店、餐廳、超級市場、髮型屋等設施。

## 歷代樓宇

### 重建前樓宇

舊邨重建前16座樓宇全部皆以花卉的中英文名稱命名：

| （座別）樓宇名稱                | 樓宇類型   | 入伙年份 | 拆卸年份 |
| ----------------------- | ------ | ---- | ---- |
| （S座）杜鵑樓 Azalea House    | 舊長型    | 1960 | 2013 |
| （T座）海棠樓 Begonia House   |        |      |      |
| （U座）茶花樓 Camelia House   |        |      |      |
| （P座）百合樓 Lily House      | T型     | 1961 |      |
| （Q座）彩雀樓 Larkspur House  |        |      |      |
| （R座）荷花樓 Lotus House     |        |      |      |
| （E座）楓林樓 Maple House     | Y型（舊式） | 2010 |      |
| （F座）丁香樓 Lilac House     |        |      |      |
| （G座）金松樓 Cedar House     |        |      |      |
| （H座）綠柳樓 Willow House    |        |      |      |
| （I座）櫻桃樓 Cherry House    |        |      |      |
| （M座）劍蘭樓 Gladiolus House | 相連舊長型  | 1962 |      |
| （D座）石竹樓 Carnation House | 1963   |      |      |
| （C座）壽菊樓 Marigold House  |        |      |      |
| （B座）蘭花樓 Orchid House    |        |      |      |
| （A座）牡丹樓 Peony House     |        |      |      |
|                         |        |      |      |

## 事蹟

  - 蘇屋邨是[香港房屋委員會前身](../Page/香港房屋委員會.md "wikilink")[屋宇建設委員會繼](../Page/屋宇建設委員會.md "wikilink")[北角邨及](../Page/北角邨.md "wikilink")[西環邨之後策劃建設的第三個屋邨](../Page/西環邨.md "wikilink")，當時獲譽為[遠東規模最大的地區性住宅計劃](../Page/遠東.md "wikilink")。
  - 楓林樓、丁香樓、金松樓、綠柳樓及櫻桃樓是香港首批[Y型設計的公屋大廈](../Page/Y型大廈.md "wikilink")。
  - 於1961年11月8日訪問本邨的[雅麗珊郡主](../Page/雅麗珊郡主.md "wikilink")（H.R.H. Princess
    Alexandra），栽種了一棵[櫻桃樹亦稱公主樹](../Page/櫻桃樹.md "wikilink")，位於前蘇屋邨房屋署辦事處旁邊，在重建計劃中獲得保留。\[8\]
  - 第一代運用海水沖廁的屋邨，由司徒惠建築師樓負責蘇屋邨的海水沖廁系統設計，海水泵房位於長沙灣海濱，而海水供水管則沿[興華街抽上海拔](../Page/興華街.md "wikilink")280米高，[大埔道對上的水務署海水配水庫](../Page/大埔道.md "wikilink")，再分配給蘇屋邨的用戶。
  - 蘇屋邨背山望海，[交通便捷](../Page/交通.md "wikilink")，環境清幽，已有50年歷史。早年的蘇屋邨曾是公屋的皇牌，入住者大都具有一定的[經濟能力](../Page/經濟.md "wikilink")，可算是香港[中產階級的搖籃](../Page/中產階級.md "wikilink")，不少邨民的後裔甚至到了[西歐](../Page/西歐.md "wikilink")、[北美等海外地區開枝散葉](../Page/北美.md "wikilink")。
  - 「蘇屋邨三寶」等前蘇屋邨舊建築物在重建計劃中保留和活化，以傳承屋邨的歷史。燕子亭復修和原址保留；小白屋復修和活化後作商業用途；前屋邨入囗門廊將在邨內重置。其他舊建築物如前屋邨辦事處，重建後用途再作安排；前楓林樓的部分地下大堂結構已保留和復修；部分舊邨具歷史價值的物品，將視乎情況在前楓林樓內展示；雅麗珊郡主栽種的樹都獲保留，重拾並延續集體回憶。\[9\]
  - 2006年底，時任[全國人大常委會委員長](../Page/全國人民代表大會常務委員會委員長.md "wikilink")[吳邦國曾造訪蘇屋邨](../Page/吳邦國.md "wikilink")。
  - 擁有多年歷史的蘇屋邨，曾為不少名人的住所，包括「歌神」[許冠傑與其兄長](../Page/許冠傑.md "wikilink")[許冠文和](../Page/許冠文.md "wikilink")[許冠英等](../Page/許冠英.md "wikilink")，少年時曾於蘇屋邨居住。除此之外，殿堂級樂隊[Beyond的成員](../Page/Beyond.md "wikilink")[黃家強及其已故兄長](../Page/黃家強.md "wikilink")[黃家駒](../Page/黃家駒.md "wikilink")，亦曾是蘇屋邨居民。其他曾住蘇屋邨的藝人包括[蔡楓華](../Page/蔡楓華.md "wikilink")、[伍衛國](../Page/伍衛國.md "wikilink")、粵語片甘草演員[周吉的兒子](../Page/周吉.md "wikilink")[周啟生及已移民](../Page/周啟生.md "wikilink")[加拿大但近年回流香港拍劇的](../Page/加拿大.md "wikilink")[林嘉華等](../Page/林嘉華.md "wikilink")；除藝人外，現時負責主理蘇屋邨重建計劃的房屋署總建築師（1）陸光偉先生以前也曾經居住過蘇屋邨。

## 圖集

### 蘇屋邨往時面貌

HK_SoUkEst_CameliaHouse.JPG|重建前蘇屋邨茶花樓
HK_SoUkEst_AzaleaHouse.JPG|重建前蘇屋邨杜鵑樓 So Uk Estate Penoy
House.jpg|重建前蘇屋邨牡丹樓 So Uk Estate Cedar House & Maple
House.jpg|重建前蘇屋邨金松樓及楓林樓 So Uk Estate Overview
200902.jpg|重建前蘇屋邨保安道入口 So Uk Estate Abanded Gas Station.jpg|重建前蘇屋邨油站 So
Uk Estate Access.jpg|重建前蘇屋邨通道 So Uk Estate Open Space 1.jpg|重建前蘇屋邨休憩空間
So Uk Estate Mosaic Map.jpg|重建前蘇屋邨馬賽克地圖 So Uk Estate VTC Youth
College.jpg|重建前蘇屋邨職業訓練局青年學院

### 蘇屋邨重建時面貌

So Uk Estate 2012 part1.JPG|已拆卸的蘇屋邨的杜鵑樓，為第二期重建項目（2012年） So Uk Estate
2012 part2.JPG|蘇屋邨牌匾將會被保留（2012年） So Uk Estate 2012
part3.JPG|已拆卸的蘇屋邨的茶花樓，為第二期重建項目（2012年） So Uk
Estate 2012 part4.JPG|正在平整地基的蘇屋邨第一期（2012年） So Uk Estate 2012
part5.JPG|蘇屋邨拆卸時期的荷花樓與彩雀樓之間休憩廣場（2012年） So Uk Estate 2012
part6.JPG|重建中的蘇屋邨第一期（2012年） So Uk Estate 2012
part7.JPG|蘇屋邨彩雀街部分範圍已被封閉（2012年） So Uk Estate
2012 part8.JPG|蘇屋邨所有商店已遷出（2012年） So Uk Estate 2013
part1.JPG|搭棚中的蘇屋邨（2013年） So Uk Estate under
reconstruction in July 2014 (revised).jpg|重建中的蘇屋邨一期東南面（2014年7月） So Uk
Estate under rebuild in December 2014.JPG|重建中的蘇屋邨一期西南面（2014年12月） So Uk
Estate 2015 05
part3.JPG|從[李鄭屋邨遠望重建中的蘇屋邨一期東南面](../Page/李鄭屋邨.md "wikilink")（2015年5月）
So Uk Estate under reconstruction in June 2015.jpg|重建中的蘇屋邨一期西南面（2015年6月）
Northeast of So Uk Estate Phase 1 under rebuild in June
2015.jpg|重建中的蘇屋邨一期東北面（2015年6月） So Uk
Estate.jpg|重建中的蘇屋邨二期（2015年10月） So Uk Estate Phase 2
under rebuild in November 2016.jpg|重建中的蘇屋邨二期（2016年11月） So Uk Estate
Phase 2 under rebuild in June 2017.jpg|重建中的蘇屋邨二期（2017年6月） So Uk Estate
in May 2018.jpg|重建中的蘇屋邨二期（2018年5月）

### 蘇屋邨一期重建後設施

So Uk Estate Community Play Area 2.jpg|二號社區遊樂場 So Uk Estate Community
Play Area 4.jpg|四號社區遊樂場[搖搖板](../Page/搖搖板.md "wikilink") So Uk Estate
Community Play Area 4 (2).jpg|四號社區遊樂場（2） So Uk Estate Community Play
Area 4 (3).jpg|四號社區遊樂場（3） So Uk Estate Parallel Bars.jpg|雙桿 So Uk Estate
Leg Stretch.jpg|壓腿架 So Uk Estate Stump Walk.jpg|梅花椿 So Uk Estate Amenity
And Community Building.jpg|社區綜合服務大樓 So Uk Estate Phase 1 decoration
contractor office.jpg|一期裝修承辦商（入伙初期運作）

### 蘇屋邨二期重建後面貌

So Uk Estate 2018 06 part5.jpg|蘇屋邨二期重建後外貌，後方最高樓宇為百合樓 So Uk Estate 2018
06 part3.jpg|蘇屋邨茶花樓 So Uk Estate 2018 06 part4.jpg|蘇屋邨杜鵑樓及海棠樓 So Uk
Estate 2018 06 part2.jpg|蘇屋邨石竹樓，後方為劍蘭樓

## 重建前蘇屋邨全景

## 負面事件

1975年4月28日發生蘇屋邨山坡姦殺案，休班女[輔警劉靈仙](../Page/輔警.md "wikilink")（警號A7733）與男友徐振偉在蘇屋邨石竹樓（男方住所）後面的山坡散步談情，遇上一持刀幪面的賊人，交出財物後賊人嫌少，賊人喝令徐振偉伏在地上然後將他用布帶綁住，後把劉靈仙帶走，數小時後徐振偉掙脫綑綁後跑下山坡到蘇屋巴士站，剛好遇上巡警，然後一同上山兜截，但找不到匪徒或劉靈仙。三天後，龍翔道建築天橋的工人發現劉靈仙屍體，驗屍後證實死前曾遭強姦，本案至今仍未破案，相信疑凶仍然逍遙法外。\[10\]

1979年1月24日發生蘇屋邨醫生綁架撕票案，西醫黎鴻荃離開他位於蘇屋邨的住所到旺角診所上班，其後被綁架，醫生家人曾收勒索電話要求五萬元贖金，但到交易地點時對方沒有露面，22天後，醫生的腐屍在龍蝦灣一個懸崖邊被發現，本案至今仍未破案。\[11\]

1990年6月27日發生蘇屋邨彩雀樓劫殺案，四名匪徒在彩雀樓爆竊被發現，殺死單位一名女子。

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[長沙灣站](../Page/長沙灣站.md "wikilink")（步行約15分鐘）

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [蘇屋巴士總站](../Page/蘇屋巴士總站.md "wikilink")/[廣利道](../Page/廣利道.md "wikilink")

<!-- end list -->

  - [保安道](../Page/保安道.md "wikilink")

<!-- end list -->

  - [青山道](../Page/青山道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - [青山道至](../Page/青山道.md "wikilink")[油塘線](../Page/油塘.md "wikilink")\[12\]
  - 青山道至[土瓜灣](../Page/土瓜灣.md "wikilink")/[紅磡線](../Page/紅磡.md "wikilink")
    (24小時服務)\[13\]
  - 青山道至[黃大仙](../Page/黃大仙.md "wikilink")/[九龍城線](../Page/九龍城.md "wikilink")\[14\]
  - 青山道至[觀塘線](../Page/觀塘.md "wikilink")\[15\]
  - 青山道至[觀塘](../Page/觀塘.md "wikilink")/[黃大仙線](../Page/黃大仙.md "wikilink")
    (通宵線)\[16\]
  - 美孚至[佐敦道線](../Page/佐敦道.md "wikilink")<ref>[美孚　—　旺角及佐敦道](http://www.16seats.net/chi/rmb/r_k63.html)

</ref>

  - [大埔道](../Page/大埔道.md "wikilink")

</div>

</div>

## 參考文獻

  - Far East Builder,1957

## 外部連結

  - [房委會蘇屋邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=15280)
  - [再別蘇屋](http://www.housingauthority.gov.hk/tc/common/video/about-us/photos-and-videos/videos/historic-estates-and-buildings/souk.asx)
  - [重建後蘇屋邨模型](http://www.housingauthority.gov.hk/hdw/content/images/b5/about_us/resources/publications/housing_dimensions/large_okl272.jpg)

[蘇屋邨 第一期 平面圖](http://cyclub.happyhongkong.com/viewthread.php?tid=93904)

[Category:長沙灣](../Category/長沙灣.md "wikilink")
[Category:蘇屋](../Category/蘇屋.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  《建聞築蹟》， 吳啟聰、朱卓雄 著，經濟日報出版社，ISBN 978-962-678-453-2

2.  [香港公營房屋討論區-好消息！白田居民可以參觀蘇屋單位了](http://cyclub.happyhongkong.com/viewthread.php?tid=219827&page=1)

3.

4.
5.

6.  [蘇屋邨完成重建最後三座入伙（附圖）](https://www.info.gov.hk/gia/general/201902/21/P2019022000269.htm)

7.  [蘇屋邨茶花樓今起入伙（附圖）](http://www.info.gov.hk/gia/general/201808/10/P2018081000490.htm)

8.
9.

10. [女輔警遭姦殺案](http://blog.yahoo.com/_RUEYXOZZC3GX6AYIOUWS4QPKQQ/articles/671590)

11. [完美謀殺](http://blog.yahoo.com/_RUEYXOZZC3GX6AYIOUWS4QPKQQ/articles/1226413/category/%E6%83%A1%E9%AD%94%E5%9C%A8%E6%BC%AB%E6%AD%A5)

12. [青山道香港紗廠　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_k38.html)

13. [土瓜灣及紅磡　—　青山道](http://www.16seats.net/chi/rmb/r_k61.html)

14. [黃大仙及九龍城　—　青山道](http://www.16seats.net/chi/rmb/r_k62.html)

15. [觀塘協和街　—　青山道香港紗廠](http://www.16seats.net/chi/rmb/r_k02.html)

16. [觀塘及黃大仙　—　青山道](http://www.16seats.net/chi/rmb/r_k22.html)