**蘇有朋**（**英文名**：，），著名[男歌手](../Page/男歌手.md "wikilink")、[男演員](../Page/男演員.md "wikilink")，生於[台灣](../Page/台灣.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")。15歲時考入[臺北市立建国中學并被](../Page/臺北市立建国中學.md "wikilink")[張小燕相中加入](../Page/張小燕.md "wikilink")[小虎隊](../Page/小虎隊_\(台灣\).md "wikilink")，成為風靡亞洲的偶像團體成員。1991年以優異成绩考入[國立臺灣大學](../Page/國立臺灣大學.md "wikilink")。小虎队解散後單飛，與[吴奇隆](../Page/吴奇隆.md "wikilink")、[林志颖](../Page/林志颖.md "wikilink")、[金城武並稱為四小天王](../Page/金城武.md "wikilink")。之後轉入戲劇界，1997年，接拍[瓊瑤的](../Page/瓊瑤.md "wikilink")《[還珠格格](../Page/還珠格格.md "wikilink")》飾演五阿哥一角，該劇以百分之65的收視率創造了[中國大陸的收視奇蹟](../Page/中國大陸.md "wikilink")，也使蘇有朋再次红遍亞洲。之後主演《[倚天屠龍记](../Page/倚天屠龍記_\(2003年電視劇\).md "wikilink")》、《情定愛琴海》、《[楊門虎將](../Page/楊門虎將.md "wikilink")》、《[刁蠻公主](../Page/刁蠻公主.md "wikilink")》等八點檔電視劇，均坐收收視率冠軍寶座。其中《情定爱琴海》和《刁蠻公主》在亞洲各国掀起收视热潮。2002到2005年，連續4年獲得新加坡聯合早報評選的亞洲人氣偶像前三甲。2006年開轉型，出演舞台劇《菊花香》，主演《将计就计》，《[熱愛](../Page/热爱_\(電視劇\).md "wikilink")》等熱播劇。2008年下半年起，以電影演出為主，並於2010年10月，以電影《[風聲](../Page/風聲_\(电影\).md "wikilink")》（2009年10月上映）中的演出，得到中國大陸第30屆[大眾電影百花獎最佳男配角獎](../Page/大眾電影百花獎.md "wikilink")，成功转型为实力派演员。12月，以電影《[康定情歌](../Page/康定情歌_\(电影\).md "wikilink")》，得到第2屆[澳門國際電影節最佳男主角獎](../Page/澳門國際電影節.md "wikilink")。2011年8月，以近年來在電影中的傑出表現，獲得中國電影表演藝術學會第13屆金鳳凰獎評委會特別獎。2012年制作并主演电视剧《[非缘勿扰](../Page/非缘勿扰.md "wikilink")》，该剧打破央视收视纪录。目前為享誉海内外的電視、電影、歌唱的全方面藝人。

## 生平

### 出生

蘇有朋1973年9月11日出生於[台北市](../Page/台北市.md "wikilink")。蘇有朋與他母親的農曆生日同為[中秋節](../Page/中秋節.md "wikilink")，因此取名「有朋」－家中有兩個月亮的意思。蘇有朋學業成績優異，高中畢業於第一志願[台北市立建國中學](../Page/台北市立建國中學.md "wikilink")，並考取[國立台灣大學機械工程系](../Page/國立台灣大學.md "wikilink")（当年第5志愿），後來因為轉系失敗休學，因此沒有取得學士學位。

### 經歷

  - [小虎隊時期](../Page/小虎隊_\(台灣\).md "wikilink")

1988年，與[吳奇隆和](../Page/吳奇隆.md "wikilink")[陳志朋組成](../Page/陳志朋.md "wikilink")[男子團體](../Page/男子團體.md "wikilink")[小虎隊](../Page/小虎隊_\(台灣\).md "wikilink")，並加入[開麗創意組合經紀公司](../Page/開麗娛樂經紀.md "wikilink")。當時蘇有朋只有十五歲，因當時就讀明星高中[建國中學之故](../Page/建國中學.md "wikilink")，經紀公司為他取了「乖乖虎」的外號。小虎隊本是為了當時中華電視公司（華視）所播出的《[青春大對抗](../Page/青春大對抗.md "wikilink")》節目所甄選的男性節目助理。工作內容是在節目的錄製現場幫忙帶動氣氛和協助安置節目道具。同時，經紀公司並訓練小虎隊三人做一些簡單的歌舞表演，以便節目串場之用。但無心插柳柳成蔭，三位少年在節目中曝光之後，因為外貌清秀、形象健康，受到當時台灣少男少女的廣泛喜愛，不少少女更常在開麗辦公室及華視大樓外，等候小虎隊。經紀公司看到小虎隊引發如此風潮，便安排小虎隊在同一經紀公司的師姐──[女子團體](../Page/女子團體.md "wikilink")[憂歡派對的專輯中合唱](../Page/憂歡派對_\(台灣\).md "wikilink")，試試市場反應。在這張「新年快樂」合輯中，小虎隊演唱了「新年快樂」（與憂歡派對合唱）、「青蘋果樂園」、「彩色青春彩色夢」三首歌曲，專輯推出後，受到市場的熱烈支持，小虎隊成為當時台灣最受歡迎的偶像團體之一。

由於小虎隊會玩會唸書的形象相當良好，歌曲曲風健康清新，不但受到當時台灣青少年歡迎，也改變了社會上對於演藝人員較為負面的印象，和歌迷間亦有「重榮譽、守秩序」的口號及規範。有些父母還陪著小孩一起追小虎隊。然而，即使經紀公司為了讓小虎隊成員都能夠兼顧學業，把出唱片及宣傳、排練舞蹈、錄製節目、拍電影等工作都儘可能安排在寒暑假期間，但仍難免會影響到三位少年的學業，尤其是背負考大學壓力的蘇有朋。由於小虎隊的演藝工作繁忙，蘇有朋在建中一、二年級的課業成績並不理想，在蘇有朋家人和經紀公司的幾番討論之下，決定讓蘇有朋在升上高三之後，暫停小虎隊的所有工作，專心唸書。蘇有朋在閉關苦讀一年，窮盡心力追回先前落後的課業之後，於1991年7月，考上了[台灣大學的](../Page/台灣大學.md "wikilink")[機械工程學系](../Page/機械工程.md "wikilink")（就當年的成績排序，為理工組的第五志願）。
之後蘇重新歸隊小虎隊，推出了新專輯《愛》。專輯再度把小虎隊推向一波高峰。同年，小虎隊亦因受到中國大陸青少年喜愛，進而獲邀到大陸進行賑災義演，並舉辦多場演唱會，成為第一組至大陸演出的臺灣藝人。

在赴大陸巡演回來之後，聲勢如日中天的小虎隊，卻因為成員小帥虎陳志朋接獲召集令，必須入伍服役，在1991年12月，小虎隊推出第六張專輯《再見》之後，小虎隊便暫別舞台。在陳志朋服役期間，[吳奇隆及](../Page/吳奇隆.md "wikilink")[蘇有朋推出個人專輯](../Page/蘇有朋.md "wikilink")，在市場上也獲得不錯的成績，並和當時同樣受到青少年喜愛的偶像──[金城武和](../Page/金城武.md "wikilink")[林志穎](../Page/林志穎.md "wikilink")，併稱為台灣演藝圈的四小天王。1993年，陳志朋退伍歸隊，三人再度合作，推出小虎隊專輯《星光依舊燦爛》。此時的小虎隊因為年齡漸長，開始面臨轉型問題。加上過去兩年吳奇隆和蘇有朋的個人特色和市場上年輕的偶像眾多，小虎隊逐漸以團體及單飛並行出唱片。

重組後的小虎隊陸續推出《星光依舊燦爛》、《快樂的感覺永遠一樣》、《庸人自擾》三張專輯，只有《星光依舊燦爛》較佳，其他兩張專輯在推出之後，因為演藝圈的偶像藝人輩出，年輕歌迷擁有多種選擇，原有的小虎迷分散成三人的個別歌迷等原因，銷量不如重組前的小虎隊。1995年，[吳奇隆在小虎隊舉辦了三場爆滿的](../Page/吳奇隆.md "wikilink")「龍騰虎嘯演唱會」之後入伍服役。小虎隊雖然沒有言明解散，但自此之後，就沒有再推出專輯。之後，三人的演藝工作，也都從唱片圈轉向戲劇。

  - 與課業的抉擇

於1994年10月17日唸大三的蘇有朋做出了震驚台灣演藝圈的決定─向[台灣大學辦理休學手續](../Page/台灣大學.md "wikilink")。消息一出，媒體大幅報導，貶抑的聲浪多過理解，社會上謾罵的語句多過於諒解，因為蘇有朋優異的學業成績（建中及台大）幾乎成為了社會及家長們心中好孩子及優等生的代名詞，而歌迷們也形成支持及反對兩派。整個社會及輿論壓力，讓蘇有朋面臨了人生及事業的巨大低潮。對蘇有朋個人來說，才剛考上[建中就加入小虎隊的他](../Page/建中.md "wikilink")，幾乎是頂著整個台灣社會對於優等生形象及要求在過日子，唸書及考試的壓力比同齡的學生大，在他所著的「我在建中的日子」一書（2003年再版時，更名為「青春的場所」），以及部份媒體訪談中，蘇有朋不諱言自己在高三閉關的那一年，一直被同一種惡夢糾結著，「我怕要是我沒有考好，在路上遇到一些帶著小孩的媽媽們，一定都會指著我，然後跟他們的小孩說，『你看，那就是只會玩不會唸書的乖乖虎』」，而有人更是以看好戲的心態，等著看他究竟能考上什麼學校。在這樣的壓力下，17歲的蘇有朋考上了[台大](../Page/台大.md "wikilink")[機械工程系](../Page/機械工程.md "wikilink")，更加坐實了「乖乖虎」的優等生形象。

但進了大學之後的蘇有朋才發現，自己對於[機械工程一點興趣都沒有](../Page/機械工程.md "wikilink")，當初是為了向社會大眾及歌迷證明自己及小虎隊是「會玩會唸書」的，所以在填志願時都按照分數排行榜的學校及學系來填，完全沒有顧慮到自己的喜好及未來。因此進了台大的蘇有朋一邊從事忙碌演藝工作，一邊唸書，也和幾個同樣覺得選錯系的同學一起準備轉系考試，並選修想轉入系別的相關課程（[企管](../Page/企管.md "wikilink")），但大二及大三兩次的轉系考試中，蘇有朋都沒能轉系成功。尤其是第二次轉系考試，經紀公司的同仁不慎走漏風聲，媒體報導了蘇有朋想轉系的新聞，新聞一出，在台大引起不小的風波。甚至，有企管系的老師在班上詢問同學（投票決定），覺得該不該讓蘇有朋轉到我們企管系（當年為[工管系企管組](../Page/工管系企管.md "wikilink")）來。

至此，蘇有朋轉系的成功率幾近於零，但若留在[機械工程系上再繼續唸](../Page/機械工程.md "wikilink")，他得補不少當初因為選修企管類學分，而放棄的機械工程本科課程，得多唸兩年才能拿到大學文憑。他考慮並和家人和經紀公司討論過後，大三的蘇有朋決定休學，他說，「就算我把機械工程的課程唸完，拿到文憑，我也不可能去當機械工程師。如果要繼續做演藝工作，那取得機械工程的文憑又有什麼意義。」

但是這個決定，讓他「乖乖虎」的形象受損，也使他的演藝工作受到了衝擊。在1995年小虎隊「龍騰虎嘯演唱會」結束之後，蘇有朋到[英國遊學](../Page/英國.md "wikilink")，遠離台灣社會關注的目光和壓力，遊學數月。

  - 參與主持節目

<!-- end list -->

  - 回國之後，除了出唱片之外，也開始接觸主持及戲劇。
  - 1996年，蘇有朋與[曹蘭](../Page/曹蘭.md "wikilink")、[董至成共同主持](../Page/董至成.md "wikilink")[華視益智節目](../Page/中華電視公司.md "wikilink")《大家開獎》。

<!-- end list -->

  - 戲劇生涯

1997年，接拍了[瓊瑤的](../Page/瓊瑤.md "wikilink")《[還珠格格](../Page/還珠格格.md "wikilink")》1998年《[還珠格格](../Page/還珠格格.md "wikilink")》在台灣上映，受到了觀眾的喜愛，創下極高的收視率。接著，《還珠格格》在中國大陸、香港、東南亞、韓國等地陸續上映，在大陸以百分之65的收視率創造了大陸收視率的奇迹。之後主演《情深深雨蒙蒙》《倚天屠龙记》《情定爱琴海》《楊門虎將》《魔術奇缘》《刁蛮公主》《將計就計》《熱愛》等八點連續劇，均为收視率冠军，其中《情定爱琴海》《刁蛮公主》在韓國東南亞掀起收視熱潮。使他成为韓國最受歡迎外國男演員，《将计就计》《热爱》则为他的转型奠定了基础。是名副其实的“电视剧收视王”甚至出现他的7部电视剧同时在各大电视台暑期档反复播出的奇观。2008年起转战影坛，凭借《风声》夺得第30届大众电影百花奖最佳男配角奖，因《康定情歌》夺得第二届澳门国际电影节最佳男主角奖，斩获影帝。2011年，荣获第十三届中国电影表演艺术学会金凤凰奖评委会特别奖。2012年首度制作并主演电视剧《[非缘勿扰](../Page/非缘勿扰.md "wikilink")》，被央视以史上时装剧最高价格收购，央视新闻多次播出其预告片，且打破央视收视记录。2013年荣获亚洲偶像盛典“年度最具实力制作人"及"年度杰出艺人"奖。同年11月，该片荣获第九届中美电影节优秀中国电视剧“金天使奖”。

## 個人生活

1999年2月14日（[情人節](../Page/情人節.md "wikilink")）時，被媒體拍到與香港富豪女兒卓文初在餐廳吃飯。被眾人懷疑兩人正在談戀愛。兩天後，蘇有朋回應“感情順利”證明了與白富美卓文初的戀情。2000年8月12日，蘇有朋與大他5歲的女友結婚。2001年3月23日，蘇有朋宣布當爸。長女蘇晨恩順利生產。2010年3月7日，蘇有朋被拍到與新加坡上市公司總裁的女兒溫熙宜在新加坡植物園遊玩，粉絲認為蘇有朋只不過是在陪太太客戶閒逛。同年6月18日，蘇有朋被拍到與溫熙宜從百貨公司走進車裡，到了女方在台灣的住宅。蘇有朋4個小時後從女方家出來。兩天後，蘇有朋做出回應，說與對方只是要好的朋友。同年7月1日，蘇有朋被狗仔隊拍到與溫熙宜上酒店。蘇有朋摟著女方的腰部。同年8月，蘇有朋承認與小14歲白富美出軌。2011年1月21日，蘇有朋宣布離婚。同年5月17日，蘇有朋宣布要與交往1年的女友結婚。同年8月21日，蘇有朋與溫熙宜在新加坡香格里拉酒店舉行第一場婚禮。1個月後在女方父母家倫敦舉行第二場婚禮。兩個月後在男方父母家溫哥華舉行第三場婚禮。兩個月後在女方讀書的地方在華盛頓舉行遊輪婚禮。兩人有兩個小孩。一男一女。

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>唱片公司</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p>《<a href="../Page/我只要你愛我.md" title="wikilink">我只要你愛我</a>》</p></td>
<td style="text-align: left;"><p>1992年12月10日</p></td>
<td style="text-align: left;"><p><a href="../Page/飛碟唱片.md" title="wikilink">飛碟唱片</a></p></td>
<td style="text-align: left;"><p><a href="../Page/中華民國國語.md" title="wikilink">國語</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p>《<a href="../Page/等到那一天.md" title="wikilink">等到那一天</a>》</p></td>
<td style="text-align: left;"><p>1993年7月10日</p></td>
<td style="text-align: left;"><p><a href="../Page/飛碟唱片.md" title="wikilink">飛碟唱片</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p>《<a href="../Page/珍惜的背包.md" title="wikilink">珍惜的背包</a>》</p></td>
<td style="text-align: left;"><p>1994年3月10日</p></td>
<td style="text-align: left;"><p><a href="../Page/飛碟唱片.md" title="wikilink">飛碟唱片</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p>《<a href="../Page/傷口_(蘇有朋專輯).md" title="wikilink">傷口</a>》</p></td>
<td style="text-align: left;"><p>1994年11月10日</p></td>
<td style="text-align: left;"><p><a href="../Page/飛碟唱片.md" title="wikilink">飛碟唱片</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>5th</p></td>
<td style="text-align: left;"><p>《<a href="../Page/愛上你的一切事情.md" title="wikilink">愛上你的一切事情</a>》</p></td>
<td style="text-align: left;"><p>1995年2月27日</p></td>
<td style="text-align: left;"><p><a href="../Page/華納唱片_(香港).md" title="wikilink">華納唱片 (香港)</a></p></td>
<td style="text-align: left;"><p>粵語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>6th</p></td>
<td style="text-align: left;"><p>《<a href="../Page/走_(蘇有朋專輯).md" title="wikilink">走</a>》</p></td>
<td style="text-align: left;"><p>1995年9月15日</p></td>
<td style="text-align: left;"><p><a href="../Page/飛碟唱片.md" title="wikilink">飛碟唱片</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>7th</p></td>
<td style="text-align: left;"><p>《<a href="../Page/風聲雨聲聽蘇聲.md" title="wikilink">風聲雨聲聽蘇聲</a>》</p></td>
<td style="text-align: left;"><p>1995年12月10日</p></td>
<td style="text-align: left;"><p><a href="../Page/飛碟唱片.md" title="wikilink">飛碟唱片</a></p></td>
<td style="text-align: left;"><p>國語/粵語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>8th</p></td>
<td style="text-align: left;"><p>《<a href="../Page/你快不快樂.md" title="wikilink">你快不快樂</a>》</p></td>
<td style="text-align: left;"><p>2000年8月10日</p></td>
<td style="text-align: left;"><p><a href="../Page/擎天娛樂.md" title="wikilink">擎天娛樂</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>9th</p></td>
<td style="text-align: left;"><p>《<a href="../Page/不只深情.md" title="wikilink">不只深情</a>》</p></td>
<td style="text-align: left;"><p>2001年7月13日</p></td>
<td style="text-align: left;"><p><a href="../Page/天中文化.md" title="wikilink">天中文化</a></p></td>
<td style="text-align: left;"><p>國語/粵語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>10th</p></td>
<td style="text-align: left;"><p>《<a href="../Page/玩真的.md" title="wikilink">玩真的</a>》</p></td>
<td style="text-align: left;"><p>2002年6月14日</p></td>
<td style="text-align: left;"><p><a href="../Page/天中文化.md" title="wikilink">天中文化</a><br />
<a href="../Page/擎天娛樂.md" title="wikilink">擎天娛樂</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>11st</p></td>
<td style="text-align: left;"><p>《<a href="../Page/以前以後.md" title="wikilink">以前以後</a>》</p></td>
<td style="text-align: left;"><p>2004年6月2日</p></td>
<td style="text-align: left;"><p><a href="../Page/天中文化.md" title="wikilink">天中文化</a><br />
<a href="../Page/擎天娛樂.md" title="wikilink">擎天娛樂</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 精選輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>唱片公司</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p>《<a href="../Page/擦肩而過.md" title="wikilink">擦肩而過</a>》</p></td>
<td style="text-align: left;"><p>1994年1月10日</p></td>
<td style="text-align: left;"><p><a href="../Page/華納唱片_(香港).md" title="wikilink">華納唱片 (香港)</a></p></td>
<td style="text-align: left;"><p><a href="../Page/中華民國國語.md" title="wikilink">國語</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p>《<a href="../Page/這般發生.md" title="wikilink">這般發生</a>》</p></td>
<td style="text-align: left;"><p>1994年7月15日</p></td>
<td style="text-align: left;"><p><a href="../Page/華納唱片_(香港).md" title="wikilink">華納唱片 (香港)</a></p></td>
<td style="text-align: left;"><p>國語/粵語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p>《<a href="../Page/瞭解.md" title="wikilink">瞭解</a>》</p></td>
<td style="text-align: left;"><p>2000年9月18日</p></td>
<td style="text-align: left;"><p><a href="../Page/華納唱片_(台灣).md" title="wikilink">華納音樂</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p>《<a href="../Page/最愛_(蘇有朋專輯).md" title="wikilink">最愛</a>》</p></td>
<td style="text-align: left;"><p>2002年12月27日</p></td>
<td style="text-align: left;"><p><a href="../Page/天中文化.md" title="wikilink">天中文化</a><br />
<a href="../Page/擎天娛樂.md" title="wikilink">擎天娛樂</a></p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 演唱会

| 日期            | 名称                    |
| :------------ | :-------------------- |
| 1989.5-1989.6 | 《逍遥 货柜 小虎队》巡回演唱会（小虎队） |
| 1991.9        | 小虎队大陆巡回演唱会（小虎队）       |
| 1993.12.19    | 星光依旧灿烂演唱会（小虎队）        |
| 1995.04.05    | 虎啸龙腾狂飚95演唱会（小虎队）      |
| 2002.03.22    | 苏有朋上海个人演唱会            |
| 2002.08.31    | 长城.居庸关(苏有朋.绿茶)演唱会     |

## 作品集

### 電視劇

| 首播／拍攝                                                | 劇名                                                  | 角色                                                                                                                                                                                                                                       | 合作演員                                                                                                                                                                                                           |
| ---------------------------------------------------- | --------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1992年                                                | 《[母雞带小鴨](../Page/母雞带小鴨.md "wikilink")》              | 梁志朋                                                                                                                                                                                                                                      | [文英](../Page/文英.md "wikilink")、[許傑輝](../Page/許傑輝.md "wikilink")、[蔡燦得](../Page/蔡燦得.md "wikilink")、陸元琪、[施易男](../Page/施易男.md "wikilink")、韓志傑、范惠君、范溢榮、葉景睿                                                          |
| 1996年                                                | 《[偶像一级棒](../Page/偶像一级棒.md "wikilink")》              | 便利商店店長                                                                                                                                                                                                                                   |                                                                                                                                                                                                                |
| 1998年                                                | 《[還珠格格](../Page/還珠格格.md "wikilink")》                | 五阿哥-[愛新覺羅永琪](../Page/愛新覺羅永琪.md "wikilink")                                                                                                                                                                                               | [趙薇](../Page/趙薇.md "wikilink")、[林心如](../Page/林心如.md "wikilink")、[周杰](../Page/周杰.md "wikilink")                                                                                                                 |
| 《[布袋和尚之仙女奇缘](../Page/布袋和尚.md "wikilink")》            | 程榮                                                  | [蕭薔](../Page/蕭薔.md "wikilink")、[王明台](../Page/王明台.md "wikilink")、[六月](../Page/蔡君茹.md "wikilink")、[康丁](../Page/康丁_\(演員\).md "wikilink")、[司馬玉嬌](../Page/司馬玉嬌.md "wikilink")、[劉福助](../Page/劉福助.md "wikilink")、[趙舜](../Page/趙舜.md "wikilink") |                                                                                                                                                                                                                |
| 《一品夫人芝麻官》\\《[台灣第一巡撫](../Page/台灣第一巡撫.md "wikilink")》  | 阿勝師傅（客串）                                            |                                                                                                                                                                                                                                          |                                                                                                                                                                                                                |
| 1999年                                                | 《[老房有喜](../Page/老房有喜.md "wikilink")》                | 蘇小鵬                                                                                                                                                                                                                                      | [趙薇](../Page/趙薇.md "wikilink")、[季芹](../Page/季芹.md "wikilink")、[邰智源](../Page/邰智源.md "wikilink")                                                                                                                 |
| 《[還珠格格](../Page/還珠格格.md "wikilink")2》                | 五阿哥-[愛新覺羅永琪](../Page/愛新覺羅永琪.md "wikilink")          | [趙薇](../Page/趙薇.md "wikilink")、[林心如](../Page/林心如.md "wikilink")、[周杰](../Page/周杰.md "wikilink")                                                                                                                                           |                                                                                                                                                                                                                |
| 2000年                                                | 《[絕代雙驕](../Page/絕代雙驕.md "wikilink")》                | 花無缺                                                                                                                                                                                                                                      | [林志颖](../Page/林志颖.md "wikilink")、[陈德容](../Page/陈德容.md "wikilink")、[李绮红](../Page/李绮红.md "wikilink")                                                                                                             |
| 2001年                                                | 《[情深深雨濛濛](../Page/情深深雨濛濛.md "wikilink")》            | 杜飛                                                                                                                                                                                                                                       | [趙薇](../Page/趙薇.md "wikilink")、[林心如](../Page/林心如.md "wikilink")、[古巨基](../Page/古巨基.md "wikilink")、[高鑫](../Page/高鑫.md "wikilink")                                                                                |
| 《[絕世雙驕](../Page/絕世雙驕.md "wikilink")》                 | 風無缺（客串）                                             |                                                                                                                                                                                                                                          |                                                                                                                                                                                                                |
| 2002年                                                | 《[少年張三豐](../Page/少年張三豐.md "wikilink")》              | 易天行                                                                                                                                                                                                                                      | [張衛健](../Page/張衛健.md "wikilink")、[李冰冰](../Page/李冰冰.md "wikilink")、嚴屹寬、[胡靜](../Page/胡靜.md "wikilink")、牛青峰、朱晏、[李小璐](../Page/李小璐.md "wikilink")、[林心如](../Page/林心如.md "wikilink")、[張鐵林](../Page/張鐵林.md "wikilink") |
| 《[相約青春](../Page/相約青春.md "wikilink")》                 | 徐敬濤                                                 |                                                                                                                                                                                                                                          |                                                                                                                                                                                                                |
| 《[拍案驚奇](../Page/拍案驚奇.md "wikilink")》                 | [杭鐵生](../Page/杭鐵生.md "wikilink")                    | [王艳](../Page/王艳.md "wikilink")、[宋丹丹](../Page/宋丹丹.md "wikilink")                                                                                                                                                                          |                                                                                                                                                                                                                |
| 《[麻辣高校生](../Page/麻辣高校生.md "wikilink")》-老師我愛你         | 趙友朋（客串）                                             |                                                                                                                                                                                                                                          |                                                                                                                                                                                                                |
| 2003年                                                | 《[倚天屠龍記](../Page/倚天屠龍記_\(2003年電視劇\).md "wikilink")》 | [張無忌](../Page/張無忌.md "wikilink")、[張翠山](../Page/張翠山.md "wikilink")                                                                                                                                                                        | [贾静雯](../Page/贾静雯.md "wikilink")、[高圆圆](../Page/高圆圆.md "wikilink")、[陈秀丽](../Page/陈秀丽.md "wikilink")                                                                                                             |
| 《[心動列車之南向北向的列車](../Page/心動列車之南向北向的列車.md "wikilink")》 | 阿晃                                                  |                                                                                                                                                                                                                                          |                                                                                                                                                                                                                |
| 2004年                                                | 《[楊門虎將](../Page/楊門虎將.md "wikilink")》                | 杨延朗                                                                                                                                                                                                                                      | [蔡琳](../Page/蔡琳.md "wikilink")、[狄龍](../Page/狄龍.md "wikilink")、[趙雅芝](../Page/趙雅芝.md "wikilink")                                                                                                                 |
| 《[情定愛琴海](../Page/情定愛琴海.md "wikilink")》               | 陸恩祈                                                 | [蔡琳](../Page/蔡琳.md "wikilink")、[何润东](../Page/何润东.md "wikilink")                                                                                                                                                                          |                                                                                                                                                                                                                |
| 2005年                                                | 《[魔術奇緣](../Page/魔術奇緣.md "wikilink")》                | 吳俊安                                                                                                                                                                                                                                      |                                                                                                                                                                                                                |
| 2006年                                                | 《[刁蠻公主](../Page/刁蠻公主.md "wikilink")》                | 朱允                                                                                                                                                                                                                                       |                                                                                                                                                                                                                |
| 2007年                                                | 《[將計就計](../Page/將計就計.md "wikilink")》                | 莊若龍                                                                                                                                                                                                                                      |                                                                                                                                                                                                                |
| 2009年                                                | 《[熱愛](../Page/熱愛.md "wikilink")》                    | 蘇明濤                                                                                                                                                                                                                                      | [韓雪](../Page/韓雪.md "wikilink")、[陳龍](../Page/陳龍.md "wikilink")、[薛佳凝](../Page/薛佳凝.md "wikilink")                                                                                                                 |
| 2013年                                                | 《[非緣勿擾](../Page/非緣勿擾.md "wikilink")》                | 陸西諾                                                                                                                                                                                                                                      | [秦嵐](../Page/秦嵐.md "wikilink")、[熊乃瑾](../Page/熊乃瑾.md "wikilink")、李明珠、[譚俊彥](../Page/譚俊彥.md "wikilink")、肖光奕                                                                                                       |
| 《[天龍八部](../Page/天龍八部_\(2013年電視劇\).md "wikilink")》    | [無崖子](../Page/無崖子.md "wikilink")（客串）                |                                                                                                                                                                                                                                          |                                                                                                                                                                                                                |
|                                                      |                                                     |                                                                                                                                                                                                                                          |                                                                                                                                                                                                                |

### 電視節目

| 年份                                       | 節目名稱                                       | 角色    | 備註                  |
| ---------------------------------------- | ------------------------------------------ | ----- | ------------------- |
| 1992年                                    | 《[乞丐王子](../Page/乞丐王子.md "wikilink")》       | 乞丐    | 連環泡短劇               |
| 1993年                                    | 《[黄飛鸿笑傳](../Page/黄飛鸿笑傳.md "wikilink")》     | 牙擦蘇   | 連環泡短劇               |
| 《[唐伯虎點秋香](../Page/唐伯虎點秋香.md "wikilink")》 | 唐伯虎                                        | 連環泡短劇 |                     |
| 1996年                                    | 《大家開獎》                                     |       | 與曹蘭、董至成共同主持華視益智節目   |
| 2013年                                    | 《[中国達人秀](../Page/中国達人秀.md "wikilink")》     |       | 與趙薇、劉燁、王偉忠共同擔任第五季評審 |
| 2017年                                    | 《[明星大偵探](../Page/明星大偵探.md "wikilink")》     | 苏前台   | 参与第二季第九期            |
| 2018年                                    | 《[中餐厅第二季](../Page/中餐厅.md "wikilink")》      |       |                     |
| 2019年                                    | 《[創造營2019](../Page/創造營2019.md "wikilink")》 |       |                     |

### 電影

#### 演員作品

| 首播／拍攝                                      | 片名                                                                                   | 角色       |
| ------------------------------------------ | ------------------------------------------------------------------------------------ | -------- |
| 1989年                                      | 《[遊俠兒](../Page/遊俠兒.md "wikilink")》                                                   |          |
| 1990年                                      | 《[烏龍小子流浪記](../Page/烏龍小子流浪記.md "wikilink")》                                           |          |
| 1996年                                      | 《[號角響起](../Page/號角響起.md "wikilink")》（港譯《[四個不平凡的少年](../Page/四個不平凡的少年.md "wikilink")》） | 羅志堅      |
| 1996年                                      | 《[泡妞專家](../Page/泡妞專家.md "wikilink")》                                                 | 處男輝      |
| 1996年                                      | 《[情色](../Page/情色\(電影\).md "wikilink")》                                               | 蔡悟       |
| 1998年                                      | 《[红娘](../Page/红娘.md "wikilink")》                                                     | 張生       |
| 1999年                                      | 《[大赢家](../Page/大赢家.md "wikilink")》                                                   | 施笙紫      |
| 《[白棉花](../Page/白棉花.md "wikilink")》         | 馬成功                                                                                  |          |
| 2001年                                      | 《[初戀的故事](../Page/初戀的故事.md "wikilink")》                                               |          |
| 2002年                                      | 《[手足情深](../Page/手足情深_\(電影\).md "wikilink")》                                          | 張家聰      |
| 2003年                                      | 《爺爺的家》（《[爱你在心口难开](../Page/爱你在心口难开.md "wikilink")》）                                   | 志嘉（客串）   |
| 2005年                                      | 《[塔克拉瑪干](../Page/塔克拉瑪干.md "wikilink")》                                               | 成成       |
| 2008年                                      | 《[愛情呼叫轉移2：愛情左右](../Page/愛情呼叫轉移2：愛情左右.md "wikilink")》                                 | 選秀狂人——郭影 |
| 《[愛到底](../Page/愛到底.md "wikilink")》         | 開場白口白                                                                                |          |
| 《[四個丘比特](../Page/四個丘比特.md "wikilink")》     | 齊泊霖                                                                                  |          |
| 2009年                                      | 《[風聲](../Page/風聲_\(電影\).md "wikilink")》                                              | 白小年      |
| 《[尋找劉三姐](../Page/尋找劉三姐.md "wikilink")》     | 韋文德                                                                                  |          |
| 《[少年星海](../Page/少年星海.md "wikilink")》       | 蕭友梅                                                                                  |          |
| 《[孤島秘密戰](../Page/孤島秘密戰.md "wikilink")》     | 客串                                                                                   |          |
| 2010年                                      | 《[康定情歌](../Page/康定情歌.md "wikilink")》                                                 | 李蘇杰      |
| 《[密室之不可告人](../Page/密室之不可告人.md "wikilink")》 | 柳飛雲                                                                                  |          |
| 2011年                                      | 《[一九四二](../Page/一九四二.md "wikilink")》                                                 | 宋子文      |
| 《[密室之不可靠岸](../Page/密室之不可靠岸.md "wikilink")》 | 柳飛雲                                                                                  |          |
| 《[少年鄧恩銘](../Page/少年鄧恩銘.md "wikilink")》     | 一水族大哥                                                                                |          |
| 2012年                                      | 《[銅雀台](../Page/銅雀台_\(電影\).md "wikilink")》                                            | 漢獻帝劉協    |
| 《[殺生](../Page/殺生_\(電影\).md "wikilink")》    | 牛醫生                                                                                  |          |
| 2014年                                      | 《[最佳嫌疑人](../Page/最佳嫌疑人.md "wikilink")》                                               | 林以泰      |
| 《[甜蜜殺機](../Page/甜蜜殺機.md "wikilink")》       | 王志毅                                                                                  |          |
| 《[四大名捕大结局](../Page/四大名捕大结局.md "wikilink")》 | 宋徽宗                                                                                  |          |
| 2016年                                      | 《[誰的青春不迷茫](../Page/誰的青春不迷茫.md "wikilink")》                                           | 錢老師(客串)  |
|                                            |                                                                                      |          |

#### 導演作品

| 播映    | 片名                                                          |
| ----- | ----------------------------------------------------------- |
| 2015年 | 《[左耳](../Page/左耳_\(電影\).md "wikilink")》                     |
| 2017年 | 《[嫌疑人X的獻身](../Page/嫌疑人X的献身_\(2017年电影\).md "wikilink")》\[1\] |
|       |                                                             |

### 電影配音

|           |                                     |        |
| --------- | ----------------------------------- | ------ |
| **首播／拍攝** | **片名**                              | **角色** |
| 1996年     | 《[小倩](../Page/小倩.md "wikilink")》中文版 | 十方大師   |
|           |                                     |        |

### 書籍

| 出版日期    | 書名                                            | 出版社   | ISBN               |
| ------- | --------------------------------------------- | ----- | ------------------ |
| 1991年   | 《[永遠的小虎隊](../Page/永遠的小虎隊.md "wikilink")》（小虎隊） | 晨星出版社 | ISBN 9789575831936 |
| 1993年   | 《[最真的朋友](../Page/最真的朋友.md "wikilink")》寫真集     |       |                    |
| 1995年8月 | 《[我在建中的日子](../Page/我在建中的日子.md "wikilink")》    | 聯合文學  | ISBN 9789575224172 |
| 2003年   | 《[青春的場所](../Page/青春的場所.md "wikilink")》        | 聯合文學  | ISBN 9789575224172 |
| 2004年   | 《[男人與地中海](../Page/男人與地中海.md "wikilink")》寫真集   |       |                    |
| 2006年   | 《DREAM--寫在話劇菊花香之前》                            |       |                    |

### 廣告

  - 1993年：環華百科全書
  - 2000年：康師傅綠茶

## 獎項

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>頒獎典禮</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p><strong>作品</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/華鼎獎.md" title="wikilink">華鼎獎</a></p></td>
<td><p>公眾形象港台最佳影視男演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/華鼎獎.md" title="wikilink">華鼎獎</a></p></td>
<td><p>公眾最滿意演藝明星團體獎</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/華鼎獎.md" title="wikilink">華鼎獎</a></p></td>
<td><p>年度最佳歌影迷團體：朋迷（蘇有朋的粉絲團）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>第30屆<a href="../Page/大眾電影百花獎.md" title="wikilink">大眾電影百花獎</a></p></td>
<td><p>最佳男配角</p></td>
<td><p>《風聲》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p>第2屆<a href="../Page/澳門國際電影節.md" title="wikilink">澳門國際電影節金蓮花獎</a></p></td>
<td><p>最佳男主角</p></td>
<td><p>《康定情歌》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/第52屆金馬獎.md" title="wikilink">第52屆金馬獎</a></p></td>
<td><p>最佳新導演</p></td>
<td><p>《<a href="../Page/左耳_(電影).md" title="wikilink">左耳</a>》</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  -
  -
  -
  - (官方)

  -
  -
  -
  -
[Category:台灣男歌手](../Category/台灣男歌手.md "wikilink")
[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:臺灣電影男演員](../Category/臺灣電影男演員.md "wikilink")
[Category:華誼兄弟藝人](../Category/華誼兄弟藝人.md "wikilink")
[Category:電影演員兼導演](../Category/電影演員兼導演.md "wikilink")
[S蘇](../Category/臺北市立建國高級中學校友.md "wikilink")
[Category:台灣佛教徒](../Category/台灣佛教徒.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[S](../Category/苏姓.md "wikilink")

1.