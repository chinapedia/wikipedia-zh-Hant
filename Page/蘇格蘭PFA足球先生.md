**蘇格蘭PFA足球先生**（）於每年[蘇格蘭球季末頒發](../Page/蘇格蘭.md "wikilink")，由球員工會[PFA蘇格蘭](../Page/蘇格蘭職業足球員協會.md "wikilink")（PFA
Scotland）的會員投票選出上年度的最佳球員。提名名單於每年4月份公佈，而在稍後於[格拉斯哥舉行的頒獎典禮中揭曉獲獎者](../Page/格拉斯哥.md "wikilink")。

## 歷屆獲獎人

於1978年頒發首次獎項，以下為全部得獎者名單：

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>國籍</p></th>
<th><p>球員名稱</p></th>
<th><p>效力球會</p></th>
<th><p>同年獲獎</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1977–78_in_Scottish_football.md" title="wikilink">1977–78年</a></p></td>
<td></td>
<td><p><a href="../Page/Derek_Johnstone.md" title="wikilink">德雷克·约翰斯通</a>（Derek Johnstone）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td><p>[1]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978–79_in_Scottish_football.md" title="wikilink">1978–79年</a></p></td>
<td></td>
<td><p><a href="../Page/Paul_Hegarty.md" title="wikilink">保罗·赫加蒂</a>（Paul Hegarty）</p></td>
<td><p><a href="../Page/邓迪联足球俱乐部.md" title="wikilink">登地聯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1979–80_in_Scottish_football.md" title="wikilink">1979–80年</a></p></td>
<td></td>
<td><p><a href="../Page/David_Provan_(footballer_born_1956).md" title="wikilink">戴维·普罗万</a>（Davie Provan）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1980–81_in_Scottish_football.md" title="wikilink">1980–81年</a></p></td>
<td></td>
<td><p><a href="../Page/Mark_McGhee.md" title="wikilink">马克·麦克格赫</a>（Mark McGhee）</p></td>
<td><p><a href="../Page/阿伯丁足球俱乐部.md" title="wikilink">鴨巴甸</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1981–82_in_Scottish_football.md" title="wikilink">1981–82年</a></p></td>
<td></td>
<td><p><a href="../Page/Sandy_Clark.md" title="wikilink">桑迪·克拉克</a>（Sandy Clark）</p></td>
<td><p><a href="../Page/艾迪爾人足球會.md" title="wikilink">艾迪爾人</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1982–83_in_Scottish_football.md" title="wikilink">1982–83年</a></p></td>
<td></td>
<td><p><a href="../Page/Charlie_Nicholas.md" title="wikilink">查利·尼绍拉斯</a>（Charlie Nicholas）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1983–84_in_Scottish_football.md" title="wikilink">1983–84年</a></p></td>
<td></td>
<td><p><a href="../Page/Willie_Miller.md" title="wikilink">威利·米勒</a>（Willie Miller）</p></td>
<td><p><a href="../Page/阿伯丁足球俱乐部.md" title="wikilink">鴨巴甸</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1984–85_in_Scottish_football.md" title="wikilink">1984–85年</a></p></td>
<td></td>
<td><p><a href="../Page/Jim_Duffy_(football).md" title="wikilink">吉米·达菲</a>（Jim Duffy）</p></td>
<td><p><a href="../Page/格里诺克慕顿足球俱乐部.md" title="wikilink">-{zh-hans:慕顿; zh-hk:摩頓;}-</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1985–86_in_Scottish_football.md" title="wikilink">1985–86年</a></p></td>
<td></td>
<td><p><a href="../Page/Richard_Gough_(footballer).md" title="wikilink">理查德·高夫</a>（Richard Gough）</p></td>
<td><p><a href="../Page/邓迪联足球俱乐部.md" title="wikilink">登地聯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986–87_in_Scottish_football.md" title="wikilink">1986–87年</a></p></td>
<td></td>
<td><p><a href="../Page/布莱恩·麦克莱尔.md" title="wikilink">-{zh-hans:布莱恩·麦克莱尔; zh-hant:麥佳亞;}-</a>（Brian McClair）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1987–88_in_Scottish_football.md" title="wikilink">1987–88年</a></p></td>
<td></td>
<td><p><a href="../Page/Paul_McStay.md" title="wikilink">保罗·麦克斯泰</a>（Paul McStay）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1988–89_in_Scottish_football.md" title="wikilink">1988–89年</a></p></td>
<td></td>
<td><p><a href="../Page/Theo_Snelders.md" title="wikilink">热奥·斯内尔德斯</a>（Theo Snelders）</p></td>
<td><p><a href="../Page/阿伯丁足球俱乐部.md" title="wikilink">鴨巴甸</a></p></td>
<td></td>
<td><p>[2]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1989–90_in_Scottish_football.md" title="wikilink">1989–90年</a></p></td>
<td></td>
<td><p><a href="../Page/Jim_Bett.md" title="wikilink">吉姆·贝特</a>（Jim Bett）</p></td>
<td><p><a href="../Page/阿伯丁足球俱乐部.md" title="wikilink">鴨巴甸</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1990–91_in_Scottish_football.md" title="wikilink">1990–91年</a></p></td>
<td></td>
<td><p><a href="../Page/Paul_Elliott_(footballer).md" title="wikilink">保罗·埃里奥特</a>（Paul Elliot）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1991–92_in_Scottish_football.md" title="wikilink">1991–92年</a></p></td>
<td></td>
<td><p><a href="../Page/埃利·麥考伊斯特.md" title="wikilink">阿利·麦科伊斯特</a>（Ally McCoist）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992–93_in_Scottish_football.md" title="wikilink">1992–93年</a></p></td>
<td></td>
<td><p><a href="../Page/Andy_Goram.md" title="wikilink">安迪·戈拉姆</a>（Andy Goram）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1993–94_in_Scottish_football.md" title="wikilink">1993–94年</a></p></td>
<td></td>
<td><p><a href="../Page/马克·哈德利.md" title="wikilink">-{zh-hans:马克·哈德利; zh-hant:馬克·希利;}-</a>（Mark Hateley）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994–95_in_Scottish_football.md" title="wikilink">1994–95年</a></p></td>
<td></td>
<td><p><a href="../Page/布莱恩·劳德鲁普.md" title="wikilink">-{zh-hans:布莱恩·劳德鲁普; zh-hant:白賴仁·勞特立;}-</a>（Brian Laudrup）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1995–96_in_Scottish_football.md" title="wikilink">1995–96年</a></p></td>
<td></td>
<td><p><a href="../Page/保罗·加斯科因.md" title="wikilink">-{zh-hans:保罗·加斯科因; zh-hant:加斯居尼;}-</a>（Paul Gascoigne）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1996–97_in_Scottish_football.md" title="wikilink">1996–97年</a></p></td>
<td></td>
<td><p><a href="../Page/保罗·迪卡尼奥.md" title="wikilink">-{zh-hans:保罗·迪卡尼奥; zh-hant:迪肯尼奧;}-</a>（Paolo Di Canio）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1997–98_in_Scottish_football.md" title="wikilink">1997–98年</a></p></td>
<td></td>
<td><p><a href="../Page/杰基·麦克纳马拉.md" title="wikilink">-{zh-hans:杰基·麦克纳马拉; zh-hant:麥拿馬拉;}-</a>（Jackie McNamara）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998–99_in_Scottish_football.md" title="wikilink">1998–99年</a></p></td>
<td></td>
<td><p><a href="../Page/亨里克·拉尔森.md" title="wikilink">-{zh-hans:亨里克·拉尔森; zh-hant:軒歷·拿臣;}-</a>（Henrik Larsson）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1999–2000_in_Scottish_football.md" title="wikilink">1999–00年</a></p></td>
<td></td>
<td><p><a href="../Page/马克·维杜卡.md" title="wikilink">-{zh-hans:马克·维杜卡; zh-hant:維杜卡;}-</a>（Mark Viduka）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td></td>
<td><p>[3]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000–01_in_Scottish_football.md" title="wikilink">2000–01年</a></p></td>
<td></td>
<td><p><a href="../Page/亨里克·拉尔森.md" title="wikilink">-{zh-hans:亨里克·拉尔森; zh-hant:軒歷·拿臣;}-</a>（Henrik Larsson）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td><p>[4]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2001–02_in_Scottish_football.md" title="wikilink">2001–02年</a></p></td>
<td></td>
<td><p><a href="../Page/洛伦佐·阿莫鲁索.md" title="wikilink">-{zh-hans:洛伦佐·阿莫鲁索; zh-hant:亞武羅素;}-</a>（Lorenzo Amoruso）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002–03_in_Scottish_football.md" title="wikilink">2002–03年</a></p></td>
<td></td>
<td><p><a href="../Page/巴里·弗格森.md" title="wikilink">-{zh-hans:巴里·弗格森; zh-hant:巴利·費格遜;}-</a>（Barry Ferguson）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2003–04_in_Scottish_football.md" title="wikilink">2003–04年</a></p></td>
<td></td>
<td><p><a href="../Page/克里斯·萨顿.md" title="wikilink">-{zh-hans:克里斯·萨顿; zh-hant:瑟頓;}-</a>（Chris Sutton）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2004–05_in_Scottish_football.md" title="wikilink">2004–05年</a></p></td>
<td></td>
<td><p><a href="../Page/费尔南多·里克森.md" title="wikilink">-{zh-hans:费尔南多·里克森; zh-hant:歷格臣;}-</a>（Fernando Ricksen）〈併列獲選〉</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/约翰·哈特森.md" title="wikilink">-{zh-hans:约翰·哈特森; zh-hant:赫臣;}-</a>（John Hartson）〈併列獲選〉</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2005–06_in_Scottish_football.md" title="wikilink">2005–06年</a></p></td>
<td></td>
<td><p><a href="../Page/肖恩·马洛尼.md" title="wikilink">-{zh-hans:肖恩·马洛尼; zh-hant:馬隆尼;}-</a>（Shaun Maloney）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭PFA年度最佳青年球員.md" title="wikilink">SYPY</a></p></td>
<td><p>[5]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006–07_in_Scottish_football.md" title="wikilink">2006–07年</a></p></td>
<td></td>
<td><p><a href="../Page/中村俊輔.md" title="wikilink">中村俊輔</a>（Shunsuke Nakamura）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007–08_in_Scottish_football.md" title="wikilink">2007–08年</a></p></td>
<td></td>
<td><p><a href="../Page/埃登·麦克格迪.md" title="wikilink">-{zh-hans:埃登·麦克格迪; zh-hk:艾登·麥基迪;}-</a>（Aiden McGeady）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭PFA年度最佳青年球員.md" title="wikilink">SYPY</a></p></td>
<td><p>[6]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008–09_in_Scottish_football.md" title="wikilink">2008–09年</a></p></td>
<td></td>
<td><p><a href="../Page/斯科特·布朗_(足球运动员).md" title="wikilink">-{zh-hans:斯科特·布朗; zh-hk:史葛·布朗;}-</a>（Scott Brown）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td></td>
<td><p>[7]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009–10_in_Scottish_football.md" title="wikilink">2009–10年</a></p></td>
<td></td>
<td><p><a href="../Page/斯蒂芬·戴维斯.md" title="wikilink">-{zh-hans:斯蒂芬·戴维斯; zh-hant:史堤芬·戴維斯;}-</a>（Steven Davis）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td></td>
<td><p>[8]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010–11_in_Scottish_football.md" title="wikilink">2010–11年</a></p></td>
<td></td>
<td><p><a href="../Page/埃米利奥·伊萨奎尔.md" title="wikilink">-{zh-hans:埃米利奥·伊萨奎尔; zh-hk:伊薩古利;}-</a>（Emilio Izaguirre）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">些路迪</a></p></td>
<td><p><a href="../Page/蘇格蘭FWA足球先生.md" title="wikilink">SFWA</a></p></td>
<td><p>[9]</p></td>
</tr>
</tbody>
</table>

## 参考資料及註腳

## 參見

  - [蘇格蘭PFA年度最佳青年球員](../Page/蘇格蘭PFA年度最佳青年球員.md "wikilink")
  - [蘇格蘭FWA足球先生](../Page/蘇格蘭FWA足球先生.md "wikilink")

[Category:蘇格蘭足球](../Category/蘇格蘭足球.md "wikilink")
[Category:英国足球奖项](../Category/英国足球奖项.md "wikilink")
[Category:1978年建立的獎項](../Category/1978年建立的獎項.md "wikilink")

1.  首名同年獲得蘇格蘭PFA足球先生及[蘇格蘭FWA足球先生的球員](../Page/蘇格蘭FWA足球先生.md "wikilink")。
2.  首名獲選的非[英國球員](../Page/英國.md "wikilink")。
3.  首名獲選的非[歐洲球員](../Page/歐洲.md "wikilink")。
4.  首名兩屆得主。
5.  首名同年獲得蘇格蘭PFA足球先生及[蘇格蘭PFA年度最佳青年球員的球員](../Page/蘇格蘭PFA年度最佳青年球員.md "wikilink")。
6.
7.
8.
9.