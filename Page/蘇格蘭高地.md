[Scottish_Highlands_and_Lowlands.png](https://zh.wikipedia.org/wiki/File:Scottish_Highlands_and_Lowlands.png "fig:Scottish_Highlands_and_Lowlands.png")
[Highlands_sign.jpg](https://zh.wikipedia.org/wiki/File:Highlands_sign.jpg "fig:Highlands_sign.jpg")[行政区边界地区的](../Page/苏格兰行政区划.md "wikilink")[高地行政区标识](../Page/高地行政区.md "wikilink")，表示这也是苏格兰高地的边界\]\]
[Inverness_Ness\&Castle_15751.JPG](https://zh.wikipedia.org/wiki/File:Inverness_Ness&Castle_15751.JPG "fig:Inverness_Ness&Castle_15751.JPG")，高地的首府\]\]

**蘇格蘭高地**（；；）是[苏格兰](../Page/苏格兰.md "wikilink")[高地边界断层以西和以北的](../Page/高地边界断层.md "wikilink")[山地的称呼](../Page/山地.md "wikilink")。许多人将蘇格蘭高地称为是欧洲风景最优美的地区。與之相對的是蘇格蘭低地。

蘇格蘭高地人烟稀少，有多座[山脉](../Page/山脉.md "wikilink")，包括英国境内最高峰[本内维斯山](../Page/本内维斯山.md "wikilink")。虽然它位于人口密集的[大不列颠岛上](../Page/大不列颠岛.md "wikilink")，但这里的人口密度甚至于少于相近纬度的[瑞典和](../Page/瑞典.md "wikilink")[挪威等地](../Page/挪威.md "wikilink")。

## 文化

在文化上苏格兰高地与[低地的差别非常大](../Page/苏格兰低地.md "wikilink")。[苏格兰盖尔语主要集中在高地](../Page/苏格兰盖尔语.md "wikilink")，在这里[高地英语也非常普及](../Page/高地英语.md "wikilink")。宗教上高地受罗马天主教的影响非常深，而且天主教始终未能被杜绝，相对于低地而言至今仍有势力。

## 历史地理

传统上蘇格蘭高地指的是[敦巴顿](../Page/敦巴顿.md "wikilink")-[斯通黑文一线西北的地区](../Page/斯通黑文.md "wikilink")，包括[赫布里底群岛](../Page/赫布里底群岛.md "wikilink")、[佩思郡的部分和](../Page/佩思郡.md "wikilink")[比特郡](../Page/比特郡.md "wikilink")，但是不包括[奧克尼群島](../Page/奧克尼群島.md "wikilink")、[昔德蘭群島](../Page/设得兰群岛.md "wikilink")、[奈恩郡](../Page/奈恩郡.md "wikilink")、马里郡、[班夫郡的海岸低地部分以及东](../Page/班夫郡.md "wikilink")[阿伯丁郡的大部分地区](../Page/阿伯丁郡.md "wikilink")。高地地区与低地地区在语言和传统上分歧很大，这里在英国化后[盖尔语和传统习俗依然被保存](../Page/盖尔语.md "wikilink")。不过一直到14世纪末这个分歧才被注意到。印威內斯一般被看作是高地的首府。不过高地与低地的分界线不是很明确。

## 行政

高地地区约40%的面积由[高地议会区管理](../Page/高地_\(苏格兰行政区\).md "wikilink")。剩下的地域分属[阿伯丁郡](../Page/阿伯丁郡.md "wikilink")、[安格斯](../Page/安格斯.md "wikilink")、[阿盖尔-比特](../Page/阿盖尔-比特.md "wikilink")、[莫瑞](../Page/莫瑞.md "wikilink")、[珀斯-金罗斯和](../Page/珀斯-金罗斯.md "wikilink")[斯特灵等六个行政区](../Page/史特灵\(苏格兰行政区\).md "wikilink")。[阿伦岛虽然属于](../Page/阿伦岛.md "wikilink")[北艾尔郡管理](../Page/北艾尔郡.md "wikilink")，但其北部被看作是蘇格蘭高地的一部分。

整个高地地区的行政中心在[印威内斯市](../Page/印威内斯.md "wikilink")，故而它也常被称为“高地之都”。

### 高地和群岛

苏格兰的高地和几大群岛由于人口普遍稀少，在行政上往往被划分在一起，如在[苏格兰议会选举中就作为同一个选区](../Page/苏格兰议会.md "wikilink")。但是这一名称在不同场合，也涵盖不同范围的地域。

## 地质

蘇格蘭高地由古老的、分裂的[高原组成](../Page/高原.md "wikilink")。古老的岩石被水流和冰川分割成峡谷和湖泊。留下来的是一个非常不规则的山区。几乎所有的山顶的高度差不多一样高。

## 图片库

<center>

Image:Loch Gairloch.jpg|[盖尔湖](../Page/盖尔湖.md "wikilink") Image:Kyle of
Durness.jpg|The Kyle of Durness Image:Loch
Long.jpg|[长湖](../Page/长湖.md "wikilink") Image:Scotland single
track road.jpg|Aultivullin附近的一条单线公路 Image:Blaven geograph.jpg|Blaven
Image:Inverness Ness Footbridge
15760.JPG|[因弗内斯](../Page/因弗内斯.md "wikilink") Image:Loch
Maree.jpg|[马里湖上的岛屿](../Page/马里湖.md "wikilink") Image:Smoo Cave-Second
Chamber.jpg|[斯穆洞穴的内部](../Page/斯穆洞穴.md "wikilink")，萨瑟兰 Image:Cape Wrath
lighthouse.jpg|高地极西北端的[Wrath角灯塔](../Page/Wrath角.md "wikilink")
Image:N2_glenfinnan_viaduct.jpg|仰视[格兰芬兰高架桥](../Page/格兰芬兰高架桥.md "wikilink")
Image:Saddle and sgurr na sgine 06-07 086.jpg|[The
Saddle](../Page/The_Saddle.md "wikilink")
Image:Loch_Scavaig,_Skye.jpg|[斯凯岛上的Scavaig湖](../Page/斯凯岛.md "wikilink")

</center>

## 参见

  - [高地行政区](../Page/高地行政区.md "wikilink")

[Category:蘇格蘭地理](../Category/蘇格蘭地理.md "wikilink")