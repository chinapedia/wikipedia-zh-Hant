**蘇照彬**，[台灣](../Page/台灣.md "wikilink")[電影](../Page/電影.md "wikilink")[編劇和](../Page/編劇.md "wikilink")[導演](../Page/導演.md "wikilink")，[國立交通大學研究所畢業](../Page/國立交通大學.md "wikilink")。其編劇作品曾獲得入圍[金馬獎](../Page/金馬獎.md "wikilink")、[香港電影金像獎等多項影展肯定](../Page/香港電影金像獎.md "wikilink")。而後嘗試執導電影，第二部執導的劇情長片《[詭絲](../Page/詭絲.md "wikilink")》不但在金馬獎入圍五項獎項，台灣票房表現亦為當年台產電影之冠。\[1\]

## 經歷

1994年從交通大學傳播科技研究所（現更名為傳播研究所）畢業，並於經濟部工業局擔任HDTV高畫質電視計畫人機互動小組工程師、
還擔任過工研院電腦與通訊研究所研究員及程式設計師，也曾任MTV台特約創意人員。 \[2\]

## 電影作品

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>電影</p></th>
<th><p>職務</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/導演.md" title="wikilink">導演</a></p></td>
<td><p><a href="../Page/編劇.md" title="wikilink">編劇</a></p></td>
<td><p><a href="../Page/監製.md" title="wikilink">監製</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p><a href="../Page/想死趁現在.md" title="wikilink">想死趁現在</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/運轉手之戀.md" title="wikilink">運轉手之戀</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p><a href="../Page/愛情靈藥_(電影).md" title="wikilink">愛情靈藥</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p>台北晚九朝五</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/三更_(電影).md" title="wikilink">三更之回家</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/雙瞳_(電影).md" title="wikilink">雙瞳</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/三更_(電影).md" title="wikilink">三更 (電影)</a> 之〈回家〉</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/詭絲.md" title="wikilink">詭絲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/遺落的玻璃珠.md" title="wikilink">遺落的玻璃珠</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/劍雨.md" title="wikilink">劍雨</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/四分人.md" title="wikilink">四分人</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/愛情無全順.md" title="wikilink">愛情無全順</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014、2015</p></td>
<td><p><a href="../Page/太平輪_(電影).md" title="wikilink">太平輪 (電影)</a> 上、下</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p><a href="../Page/泡沫之夏.md" title="wikilink">泡沫之夏</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p><a href="../Page/夏天19歲的肖像.md" title="wikilink">夏天19歲的肖像</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 劇本作品

  - 「穿隧」獲得[中華民國行政院新聞局九十二年度優良電影劇本](../Page/中華民國.md "wikilink")。此劇本現暫未改編為影視作品\[3\]
  - 「月球暗面」獲得[中華民國九十四年度國產電影長片輔導金新臺幣一千五百萬元](../Page/中華民國.md "wikilink")。有傳出多次的拍攝開機新聞，不過至今仍未有上映消息。

\[4\]

## 廣告

  - 京都念慈庵川貝枇杷膏潤喉糖

## 註釋

<small>a.
成英姝認為該部電影劇本被刪改甚多，與其創作的初稿無關，且對新的劇本不甚滿意。故在其部落格上公開否認《台北晚九朝五》為她的作品，並提出「居中協調的人S」才是真正編劇的說法。\[5\]</small>
<small>b. 蘇照彬在電影「台北晚九朝五」的職務是參考該電影的片尾</small>

## 参考文献

## 外部連結

  - [臺灣電影網：蘇照彬](http://www.taiwancinema.com/ct.asp?xItem=12538&ctNode=39)

  - [台灣電影筆記：蘇照彬](https://web.archive.org/web/20120204055011/http://movie.cca.gov.tw/files/16-1000-1632.php)

  - [放映週報：不是鬼片，是《詭絲》─蘇照彬、陳柏霖、張鈞甯、林嘉欣專訪](https://web.archive.org/web/20080408104744/http://www.funscreen.com.tw/head.asp?period=76)

  - [【專訪：蘇照彬】只有自畫像的導演](http://www.wretch.cc/blog/yijia&article_id=4910934)

  - [不重要事件選集(蘇照彬的部落格)](http://blog.xuite.net/unit9.picture/blog)

  - [他是最被低估的华语编剧，30岁就写出《双瞳》！](http://www.sohu.com/a/232396816_257537)

  -
  -
  -
  -
  -
[S](../Category/臺灣劇情片導演.md "wikilink")
[S](../Category/台湾编剧.md "wikilink")
[Category:國立交通大學校友](../Category/國立交通大學校友.md "wikilink")
[Z](../Category/蘇姓.md "wikilink")
[Category:香港電影評論學會大獎最佳導演得主](../Category/香港電影評論學會大獎最佳導演得主.md "wikilink")
[Category:金馬電影學院教授](../Category/金馬電影學院教授.md "wikilink")

1.  [台灣電影網：人物特寫─蘇照彬](http://movie.cca.gov.tw/People/Content.asp?ID=171)
2.  [編劇頑童，玩起大電影](https://www.cheers.com.tw/article/article.action?id=5023319&eturec=1)
3.  [第92年度優良電影劇本獎得獎名單](http://www.movieseeds.com.tw/screenplay_prizer/index.php?cid=92)

4.  [行政院新聞局公布94年度國產電影長片輔導金第3梯次獲選企畫案名單](http://www.taiwancinema.com/InfoNew/InfoNewContent/?ContentUrl=52495)
5.  [《台北晚九朝五》不是我寫的--小說家看編劇這回事](http://www.mjkc.tw/2007/07/blog-post_17.html)