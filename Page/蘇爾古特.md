**蘇爾古特**
（）是[俄羅斯](../Page/俄羅斯.md "wikilink")[漢特-曼西自治區最大的城市](../Page/漢特-曼西自治區.md "wikilink")、[秋明州第二大城市](../Page/秋明州.md "wikilink")，位於[鄂畢河畔](../Page/鄂畢河.md "wikilink")。2010年人口為306,703
人。

始建於1594年，1965年6月12日建市。

## 历史

苏尔古特于1594年在[沙皇](../Page/沙皇.md "wikilink")[费奥多尔一世在位期间建立](../Page/费奥多尔·伊万诺维奇.md "wikilink")，是西伯利亚最古老的城市之一，苏尔古特一词来源于[汉特语单词](../Page/汉特语.md "wikilink")
"sur" （鱼） "gut"
（洞）的组合。20世纪60年代，该市成为[秋明州石油天然气工业中心](../Page/秋明州.md "wikilink")，1965年6月25日建市，2000年通车的[苏尔古特大桥是世界上最长的独塔斜拉桥](../Page/苏尔古特大桥.md "wikilink")。

## 气候

苏尔古特属于亚寒带气候（[柯本气候分类法属于](../Page/柯本气候分类法.md "wikilink")*Dfc*型），冬季苦寒，夏季稍事温暖，降水量中等。

## 经济

石油资源和天然气加工是苏尔古特的支柱产业，该市享有“俄罗斯石油之都”的美誉，该市最知名的企业是Surgutneftegaz和Surgutgazprom（[俄罗斯天然气工业股份公司的子公司](../Page/俄罗斯天然气工业股份公司.md "wikilink")）。苏尔古特电厂GRES-1和GRES-2产生超过7200
MW的廉价电力来供应该地区的生产生活需要。

## 交通运输

[苏尔古特国际机场距离苏尔古特市区](../Page/苏尔古特国际机场.md "wikilink")10公里，有通往[莫斯科](../Page/莫斯科.md "wikilink")、[圣彼得堡](../Page/圣彼得堡.md "wikilink")、[迪拜](../Page/迪拜.md "wikilink")、[伊尔库茨克等地的定期航班](../Page/伊尔库茨克.md "wikilink")。

秋明-新乌连戈伊铁路传过该市，通往[秋明方向](../Page/秋明.md "wikilink")，苏尔古特的下一站是撒雷母（Salym），[荷兰皇家壳牌石油公司在该地拥有油田设施](../Page/荷兰皇家壳牌石油公司.md "wikilink")。

P-404公路可以从苏尔古特通往秋明。

该市有[鄂毕河港口](../Page/鄂毕河.md "wikilink")。

<div style="width:70%;">

</div>

## 姐妹城市

  - [朝陽市](../Page/朝陽市.md "wikilink")

  - [佐洛埃格塞格](../Page/佐洛埃格塞格.md "wikilink")

  - [南達科他州](../Page/南達科他州.md "wikilink")[蘇瀑](../Page/蘇瀑.md "wikilink")

## 参考资料

## 外部連結

  - [Карты Сургута](http://surgut-gps.ucoz.ru/)
  - [Веб-сервер администрации города Сургут](http://www.admsurgut.ru)
  - [Сургут в энциклопедии «Мой
    город»](http://www.mojgorod.ru/hmao/surgut/index.html)

[С](../Category/漢特-曼西自治區城市.md "wikilink")