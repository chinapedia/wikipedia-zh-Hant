**蘇牟龍科**（學名：Shuvosauridae）是[鑲嵌踝類主龍](../Page/鑲嵌踝類主龍.md "wikilink")[波波龍超科的一個科](../Page/波波龍超科.md "wikilink")，由[薩克·查特吉](../Page/薩克·查特吉.md "wikilink")（Sankar
Chatterjee）在1993年根據[蘇牟龍所建立](../Page/蘇牟龍.md "wikilink")\[1\]。化石發現於[美國](../Page/美國.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")，地質年代相當於[三疊紀晚期的](../Page/三疊紀.md "wikilink")[卡尼階到](../Page/卡尼階.md "wikilink")[雷蒂亞階](../Page/雷蒂亞階.md "wikilink")。

在2007年，S.
Nesbitt發現波波龍科的[靈鱷非常類似](../Page/靈鱷.md "wikilink")[蘇牟龍](../Page/蘇牟龍.md "wikilink")，並且是[鑲嵌踝類主龍的一個物種](../Page/鑲嵌踝類主龍.md "wikilink")；鑲嵌踝類主龍是一個朝者現代[鱷魚演化的演化支](../Page/鱷魚.md "wikilink")。同時，S.
Nesbitt證實蘇牟龍與[查特吉鱷是同一種動物](../Page/查特吉鱷.md "wikilink")，並與靈鱷、[南美洲的](../Page/南美洲.md "wikilink")[岩鱷構成一個](../Page/岩鱷.md "wikilink")[演化支](../Page/演化支.md "wikilink")，而該演化支屬於波波龍超科。儘管查特吉鱷不再是個有效的屬名，但R.A.
Long與P.A.
Murray在1995年根據[查特吉鱷命名了](../Page/查特吉鱷.md "wikilink")[查特吉鱷科](../Page/查特吉鱷科.md "wikilink")（Chatterjeeidae）\[2\]。蘇牟龍科與查特吉鱷亞科的範圍相同。根據[國際動物命名委員會的優先權命名規則](../Page/國際動物命名委員會.md "wikilink")，蘇牟龍亞科較早被命名，因此具有優先權\[3\]。在2011年，S.
Nesbitt提出蘇牟龍科的定義：包含[蘇牟龍](../Page/蘇牟龍.md "wikilink")、[岩鱷在內的最小](../Page/岩鱷.md "wikilink")[演化支](../Page/演化支.md "wikilink")\[4\]。

## 參考資料

[\*](../Category/勞氏鱷目.md "wikilink")

1.
2.
3.
4.