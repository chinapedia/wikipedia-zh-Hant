**蘇珊娜·塔瑪洛**（），1957年12月12日出生於[的里雅斯特](../Page/的里雅斯特.md "wikilink")()，[義大利女作家](../Page/義大利.md "wikilink")。與義大利作家[斯韋沃](../Page/斯韋沃.md "wikilink")()有姻親關係（斯韋沃為塔瑪洛母親的姑丈）。

## 生平

父母離異後由外祖母扶養長大。塔瑪洛從師範專科畢業後，1976年至[羅馬電影實驗學校學習電影導演](../Page/羅馬電影實驗學校.md "wikilink")，曾經為電視拍攝過幾部紀錄片。第一部文學作品《飛過星空的聲音》(,
原文意為漂浮在雲端的幻想)於1989年出版，榮獲文學獎。1991年出版第二部作品《唯有聲音》()，此本短篇作品集獲得許多重要評論家的讚譽，使塔瑪洛更確定文學寫作的志向。1994年,《[依隨你心](../Page/依隨你心.md "wikilink")》()一書出版，成為文學界的重大盛事（兩年內銷售量即達兩百五十萬本）。此書採[書信體形式寫作](../Page/書信體.md "wikilink")，女導演康門契尼()並於1995年將此故事改編為電影。1997年出版的《[親愛的瑪蒂達](../Page/親愛的瑪蒂達.md "wikilink")》()則是將原本在《基督教家庭》週刊發表的專欄集結成書。

2000年10月塔瑪洛在蘇黎世創立了慈善基金會。2003年塔瑪洛開始編寫並執導電影
。2006年塔瑪洛與創作歌手以及女作家，合作編寫一部戲劇作品。

## 作品

  - [穿越星空的聲音](../Page/穿越星空的聲音.md "wikilink")（La testa tra le
    nuvole,1989）
  - 唯有聲音（Per voce sola,1991）
  - 肥肉之心（Cuore di ciccia,1992）
  - 魔術圈（Il cerchio magico,1994）
  - [依隨你心](../Page/依隨你心.md "wikilink")（Va' dove ti porta il cuore, 1994）
  - 精神世界（Anima Mundi, 1997）
  - [親愛的瑪蒂達](../Page/親愛的瑪蒂達.md "wikilink")（Cara Mathilda. Lettere a
    un'amica,1997）
  - 托比亞與天使（Tobia e l'angelo,1998）
  - 回家（Verso casa, 1999）
  - 紙草恐懼症（Papirofobia, 2000）
  - 回答我（Rispondimi, 2001）
  - 風火同生（Più fuoco più vento, 2002）
  - 外（Fuori, 2003）
  - 每個字都是種子（Ogni parola e\` un seme, 2005）
  - (Luisito. Una storia d'amore, 2008\]\])

## 外部連結

<http://www.susannatamaro.it/> 義大利文及英文官方網站

[T](../Category/意大利小说家.md "wikilink")
[T](../Category/1957年出生.md "wikilink")