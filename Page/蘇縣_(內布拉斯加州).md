**蘇縣**（）是[美國](../Page/美國.md "wikilink")[內布拉斯加州最西北部的一個縣](../Page/內布拉斯加州.md "wikilink")，北鄰[南達科他州](../Page/南達科他州.md "wikilink")，西鄰[懷俄明州](../Page/懷俄明州.md "wikilink")，面積5,354平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口1,311。\[1\]本縣縣治為[哈里森](../Page/哈里森_\(內布拉斯加州\).md "wikilink")（Harrison）。

## 歷史

[Sioux_County,_Nebraska_courthouse_from_E.JPG](https://zh.wikipedia.org/wiki/File:Sioux_County,_Nebraska_courthouse_from_E.JPG "fig:Sioux_County,_Nebraska_courthouse_from_E.JPG")
成立於1886年9月20日\[2\]，其縣名紀念[印第安人的一支](../Page/印第安人.md "wikilink")，[蘇族](../Page/蘇族.md "wikilink")。

在成立當初，本縣的面積比現代要大得多。本縣當時的領土東鄰[霍爾特縣](../Page/霍爾特縣_\(內布拉斯加州\).md "wikilink")，南達北緯42度線。可是，[羅克縣](../Page/羅克縣_\(內布拉斯加州\).md "wikilink")、[布朗縣](../Page/布朗縣_\(內布拉斯加州\).md "wikilink")、[基亞帕哈縣和](../Page/基亞帕哈縣_\(內布拉斯加州\).md "wikilink")[切里縣於](../Page/切里縣.md "wikilink")1883年自本縣成立，致使本縣的面積減少了不少。於1885年，[道斯縣](../Page/道斯縣_\(內布拉斯加州\).md "wikilink")、[巴克斯岡縣和](../Page/巴克斯岡縣_\(內布拉斯加州\).md "wikilink")[謝里敦縣自本縣成立](../Page/謝里敦縣_\(內布拉斯加州\).md "wikilink")，令本縣的面積再度減少。自此，本縣的邊界再沒有更動。\[3\]

## 地理

[Agathe_National_Monument10.jpg](https://zh.wikipedia.org/wiki/File:Agathe_National_Monument10.jpg "fig:Agathe_National_Monument10.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，蘇縣的總面積為，其中有，即99.96%為陸地；，即0.04%為水域。\[4\]

### 毗鄰縣

  - [內布拉斯加州](../Page/內布拉斯加州.md "wikilink")
    [巴克斯岡縣](../Page/巴克斯岡縣_\(內布拉斯加州\).md "wikilink")：東方
  - 內布拉斯加州 [道斯縣](../Page/道斯縣_\(內布拉斯加州\).md "wikilink")：東方
  - 內布拉斯加州 [斯科茨布拉夫縣](../Page/斯科茨布拉夫縣.md "wikilink")：南方
  - [懷俄明州](../Page/懷俄明州.md "wikilink")
    [戈申縣](../Page/戈申縣_\(懷俄明州\).md "wikilink")：西南方
  - 懷俄明州 [奈厄布拉勒縣](../Page/奈厄布拉勒縣_\(懷俄明州\).md "wikilink")：西北方
  - [南達科他州](../Page/南達科他州.md "wikilink")
    [福爾里弗縣](../Page/福爾里弗縣_\(南達科他州\).md "wikilink")：北方

### 國家保護區

  - [瑪瑙化石床國家紀念區](../Page/瑪瑙化石床國家紀念區.md "wikilink")
  - [內布拉斯加州國家森林公園](../Page/內布拉斯加州國家森林公園.md "wikilink")（部分）
  - [奧格拉拉國家草原](../Page/奧格拉拉國家草原.md "wikilink")（部分）

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，蘇縣擁有1,475居民、605住戶和444家庭。\[5\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")1居民（每平方公里0.24居民）。\[6\]本縣擁有780間房屋单位，其密度為每平方英里0.38間（每平方公里0.15間）。\[7\]而人口是由97.63%[白人](../Page/歐裔美國人.md "wikilink")、0.14%[土著](../Page/美國土著.md "wikilink")、0.2%[亞洲人](../Page/亞裔美國人.md "wikilink")、1.15%其他[種族和](../Page/種族.md "wikilink")0.88%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")2.31%。在97.63%白人中，有36.9%是[德國人](../Page/德國人.md "wikilink")、11.3%是[愛爾蘭人](../Page/愛爾蘭人.md "wikilink")、以及10.6%是[英國人](../Page/英國人.md "wikilink")。\[8\]

在605住户中，有28.1%擁有一個或以上的兒童（18歲以下）、65.3%為夫妻、5.1%為單親家庭、26.6%為非家庭、23.6%為獨居、9.4%住戶有同居長者。平均每戶有2.44人，而平均每個家庭則有2.86人。在1,475居民中，有24.3%為18歲以下、7.2%為18至24歲、24.7%為25至44歲、27.5%為45至64歲以及16.2%為65歲以上。人口的年齡中位數為42歲，女子對男子的性別比為100：111。成年人的性別比則為100：102.2。\[9\]

本縣的住戶收入中位數為$29,851，而家庭收入中位數則為$31,406。男性的收入中位數為$23,409，而女性的收入中位數則為$21,490，[人均收入為](../Page/人均收入.md "wikilink")$15,999。約11.1%家庭和15.4%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括24.4%兒童（18歲以下）及7.5%長者（65歲以上）。\[10\]

## 參考文獻

[S](../Category/內布拉斯加州行政區劃.md "wikilink")

1.  [2010 census data](http://2010.census.gov/2010census/data/)

2.  [Nebraska...Our Towns - Harrison -- Sioux
    County](http://casde.unl.edu/history/counties/sioux/harrison/index.php)

3.  [EARLY HISTORY OF SIOUX
    COUNTY](http://www.usgennet.org/usa/ne/county/sioux/history2.html)

4.

5.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

6.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

7.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

8.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

9.  [American FactFinder](http://factfinder.census.gov/)

10.