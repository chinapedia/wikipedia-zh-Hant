《**蘇菲的世界**》（，）是一本[喬斯坦·賈德著作的](../Page/喬斯坦·賈德.md "wikilink")[小說](../Page/小說.md "wikilink")，首次於1991年以[挪威語出版](../Page/挪威語.md "wikilink")。本書多以蘇菲和一位名叫艾伯特（Alberto
Knox）的神秘人之對話錄，是以由浅入深的方式簡介[西方哲學的小說](../Page/西方哲學.md "wikilink")。

## 简介

1990年，蘇菲（Sophie
Amudsen）是一位14歲的[挪威女孩](../Page/挪威.md "wikilink")，她與她的媽媽和一群寵物一起居住。而她的父親是一位[油輪的](../Page/油輪.md "wikilink")[船長](../Page/船長.md "wikilink")，經常一離開家就是好幾年，这一角色从未出現在情节中。

故事開始時蘇菲過著平凡的生活，直至有天她在郵箱中收到兩封來歷不明的信寫著：「你是誰？世界從何來？」與此同時，她收到一封古怪的[明信片](../Page/明信片.md "wikilink")，上面的收件人是請蘇菲轉交給席妲（Hilde），[邮戳来自](../Page/邮戳.md "wikilink")[黎巴嫩](../Page/黎巴嫩.md "wikilink")。蘇菲不久後收到一封匿名信件，信件中记载着一部分与哲学和[哲学史相关的内容](../Page/哲学史.md "wikilink")。她开始学习哲学史，由浅入深的课程也定期寄到家中的信箱。

像这样莫名其妙的函授课程持续进行着，联络方式也一变再变；痛苦憤怒的亞伯特（Alberto
Knox）终于出现在她的面前，并开始教她哲學史。亞伯特用淺顯易懂的筆觸帶著蘇菲回顧從[蘇格拉底哲學以前的希臘直到](../Page/前蘇格拉底哲學.md "wikilink")[尚-保羅·沙特時代的哲學發展](../Page/尚-保羅·沙特.md "wikilink")。同时，“席妲”的名字不断在她身边以不可思议的方式频繁出现。而后，席妲出现了。原来蘇菲這個人物，只是在黎巴嫩[聯合國部隊工作的挪威](../Page/聯合國維安部隊.md "wikilink")[少校亞伯特](../Page/少校.md "wikilink")（Albert
Knag）為慶祝女兒席妲生日而虛構出來的人物，把她的故事編寫成一個富有哲學趣味的故事當作女兒的生日禮物。

不可思议的是，在亞伯特的提示下，蘇菲渐渐意识到自己只是书中的一个角色，是亞伯特少校意识的一部分，在忠实地完成自己主角的任务之余，他们开始了向[上帝](../Page/上帝.md "wikilink")（亞伯特少校）的抗争。在他们的努力下，书中世界渐渐扭曲了，变得古怪而难以驾驭。最终，这本书以一个开放式的结局告终，亞伯特和蘇菲莫名地逃出了书中世界、来到了席妲所在的世界。

這部哲學書創造性且簡易地呈現，蘇菲在與亞伯特的對談中學習到[中世紀的哲學](../Page/中世紀.md "wikilink")。打扮得像位[僧侶](../Page/僧侶.md "wikilink")，在古代的教堂中，而她也在[法國的](../Page/法國.md "wikilink")[咖啡店裡學到了尚](../Page/咖啡店.md "wikilink")-保羅·沙特與[西蒙·波娃](../Page/西蒙·波娃.md "wikilink")。不同的哲學問題及推論方法出現，蘇菲試圖靠著自己解決。

作者藉著描寫故事中的故事，嘗試解釋真實世界背後的世界、人的自由意志、本和體的道理等等……故事中，亞伯特向蘇菲解釋從古希臘哲學發展到現今哲學的歷程和轉變，讓席德（換言之，讓讀者）了解[歐洲哲學史](../Page/歐洲.md "wikilink")。

## 中文版删节

《苏菲的世界》的[作家出版社中文版](../Page/作家出版社.md "wikilink")（[萧宝森译](../Page/萧宝森.md "wikilink")）中，部分内容被[中华人民共和国文化部要求删除](../Page/中华人民共和国文化部.md "wikilink")，如[馬克思部分结尾处的](../Page/馬克思.md "wikilink")32个段落。\[1\]

## 参考资料

[Category:挪威小說](../Category/挪威小說.md "wikilink")
[Category:哲学书籍](../Category/哲学书籍.md "wikilink")

1.