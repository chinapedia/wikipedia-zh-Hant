[Territories_and_Voyages_of_the_Vikings_blank.png](https://zh.wikipedia.org/wiki/File:Territories_and_Voyages_of_the_Vikings_blank.png "fig:Territories_and_Voyages_of_the_Vikings_blank.png")

**蘭塞奧茲牧草地**（，源自法文，即「水母灣」）是處於[加拿大](../Page/加拿大.md "wikilink")[紐芬蘭與拉布拉多省](../Page/紐芬蘭與拉布拉多.md "wikilink")[紐芬蘭島最北端](../Page/紐芬蘭島.md "wikilink")，於1960年由[挪威](../Page/挪威.md "wikilink")[探險家](../Page/探險家.md "wikilink")[海爾格·英斯塔](../Page/海爾格·英斯塔.md "wikilink")（Helge
Ingstad）和他的[考古學家妻子](../Page/考古學家.md "wikilink")[安妮·斯泰恩·英斯塔](../Page/安妮·斯泰恩·英斯塔.md "wikilink")（Anne
Stine
Ingstad）所發現的[維京人村落遺跡](../Page/維京人.md "wikilink")。[聯合國教科文組織在](../Page/聯合國教科文組織.md "wikilink")1978年把這地方列為[世界文化遺產](../Page/世界文化遺產.md "wikilink")。（這遺跡的名稱曾被紐芬蘭人唸成"lancey
meadows"。）

## 聚居地遺跡

[Carlb-ansemeadows-vinland-01.jpg](https://zh.wikipedia.org/wiki/File:Carlb-ansemeadows-vinland-01.jpg "fig:Carlb-ansemeadows-vinland-01.jpg")
作為[格陵蘭以外](../Page/格陵蘭.md "wikilink")[北美唯一被證實的](../Page/北美.md "wikilink")[維京聚居地](../Page/維京.md "wikilink")，蘭塞奧茲牧草地的遺跡是經過多年的發掘才得以窺探全貌。在這個遺跡裡找到的住所、工具和器具證實了它的年份。這個殖民地遺留的建築物是全北美最古老的歐式建築，比[哥倫布的時代還要早五個世紀](../Page/哥倫布.md "wikilink")。\[1\]蘭塞奧茲牧草地現在被普遍認為是[萊弗·艾瑞克森於](../Page/萊弗·艾瑞克森.md "wikilink")1000年左右開拓，在[北歐史詩中所記載的](../Page/薩迦_\(文學\).md "wikilink")「[文蘭](../Page/文蘭.md "wikilink")」。

蘭塞奧茲牧草地遺跡出土的有最少八座建築物，包括一個[鑄造場](../Page/鑄造場.md "wikilink")、冶煉廠，還有提供木材給[船廠的](../Page/船廠.md "wikilink")[伐木場](../Page/伐木場.md "wikilink")。這些建築物當中最大的佔地28.8
× 15.6米，內裡劃還分數間房。\[2\]遺跡內出土的紡織工具表示殖民地當時有女性居住。

## 殖民地興衰

那時候紐芬蘭的氣候比起現在和暖得多。根據多-{卷}-[薩迦記載](../Page/薩迦_\(文學\).md "wikilink")，[萊弗·艾瑞克森由](../Page/萊弗·艾瑞克森.md "wikilink")[格陵蘭出海去尋找](../Page/格陵蘭.md "wikilink")[比雅尼·何爾約夫森](../Page/比雅尼·何爾約夫森.md "wikilink")（Bjarni
Herjólfsson）所描述的新陸地。他找到一個充滿[葡萄和](../Page/葡萄.md "wikilink")[鮭魚的和暖地方過冬](../Page/鮭魚.md "wikilink")，然後砍伐木材帶回缺乏木材的格陵蘭。由此推斷蘭塞奧茲牧草地的身份有幾個可能性：一、第一個營地；二、逃離「野蠻人」（Skræling）攻擊後所建立的營地；或三、一個沒有被史詩記載的營地。

[Vinland-travel.jpg](https://zh.wikipedia.org/wiki/File:Vinland-travel.jpg "fig:Vinland-travel.jpg")的可能路線。\]\]
史詩又描述另一次殖民的經過。[托爾芬·克爾塞夫尼](../Page/托爾芬·克爾塞夫尼.md "wikilink")（Thorfinn
Karlsefni）率領為數達135男15女的殖民者，航行到艾瑞克森的舊營地（有可能是蘭塞奧茲牧草地），然後以此為基地。這些人的當中又包括艾瑞克森同父異母的妹妹[菲迪絲·艾瑞克絲多蒂爾](../Page/菲迪絲·艾瑞克絲多蒂爾.md "wikilink")（Freydís
Eiríksdóttir）。雖然蘭塞奧茲牧草地是否史詩所述的文蘭已經無從稽考，但可以確定的是這裡於1000年左右曾有一班維京殖民者在這裡居住。\[3\]

蘭塞奧茲牧草地可能是來往[格陵蘭和在](../Page/格陵蘭.md "wikilink")[聖勞倫斯灣南部地域的另一個殖民地的中轉站](../Page/聖勞倫斯灣.md "wikilink")，或者是提供由格陵蘭出發的維京探險家[過冬的場所](../Page/過冬.md "wikilink")。\[4\]然而，這個地點只曾使用兩三年。根據文學與考古証據推測，與原住民的惡劣關係大概就是維京殖民者放棄這地方的主因。由女人而起的內部鬥爭和突如其來的天氣轉變也被認為是具可能性的原因之一。

史詩對此也有個有趣的記述：維京人為求與原住民建立起良好的關係，便設宴款待了些原住民酋長，而在筵席當中曾供應過[奶](../Page/奶.md "wikilink")。可能這些原住民身患[乳糖不耐症而不自知](../Page/乳糖不耐症.md "wikilink")，在喝了這些奶後生病，反而因此懷疑維京人下毒，故此籠絡原住民的計畫便以失敗告終。

另外，蘭塞奧茲牧草地或許跟[阿崗昆族傳說中的](../Page/阿崗昆族.md "wikilink")「[薩格奈王國](../Page/薩格奈王國.md "wikilink")」有關連。傳說中提到「薩格奈王國」的國民都是白皮膚金髮碧眼，而且還擁有豐富的毛皮和金屬—這些可能跟蘭塞奧茲牧草地有關，但這仍只是推測。

## 參閱

  - [文蘭地圖](../Page/文蘭地圖.md "wikilink")

## 參考資料

## 外部連結

  - [加拿大國家公園局](https://web.archive.org/web/20081216063635/http://www.pc.gc.ca/lhn-nhs/nl/meadows/index_E.asp)

  - [Severed Ways, The Norse Discovery of America, a drama set
    in 1000A.D. and filmed in L'Anse aux
    Meadows](http://www.heathenfilms.com/severedways.html)
  - [Medievalists.net](https://web.archive.org/web/20070604234228/http://www.medievalists.net/travel/lanseauxmeadows.htm)
    內含關於維京殖民地的文章與資料

## 圖片

<File:Carlb-ansemeadows-vinland-03.jpg>|[紐芬蘭蘭塞奧茲牧草地](../Page/紐芬蘭.md "wikilink")
<File:Viking> landing.jpg|2000年重演於蘭塞奧茲牧草地登陸
[File:VikingBarracklookingoutside.JPG|由維京人屋內向外望的風景，2006年攝](File:VikingBarracklookingoutside.JPG%7C由維京人屋內向外望的風景，2006年攝)

[category:加拿大历史博物馆](../Page/category:加拿大历史博物馆.md "wikilink")

[Category:加拿大世界遺產](../Category/加拿大世界遺產.md "wikilink")
[Category:加拿大考古遺址](../Category/加拿大考古遺址.md "wikilink")
[Category:加拿大國家歷史遺址](../Category/加拿大國家歷史遺址.md "wikilink")
[Category:紐芬蘭與拉布拉多省歷史](../Category/紐芬蘭與拉布拉多省歷史.md "wikilink")
[Category:加拿大博物館](../Category/加拿大博物館.md "wikilink")
[Category:維京人](../Category/維京人.md "wikilink")

1.

2.  [自Canadian Encyclopedia的L'Anse aux
    Meadows條目](http://www.thecanadianencyclopedia.com/index.cfm?PgNm=TCE&Params=J1ARTJ0004527)

3.
4.