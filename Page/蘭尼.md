**路易斯·卡洛斯·阿尔梅达·達库纳**（，，），一般通稱**蘭尼**（），是一名[葡萄牙](../Page/葡萄牙.md "wikilink")[足球員](../Page/足球員.md "wikilink")，司職[翼鋒](../Page/中場.md "wikilink")，出身於葡萄牙[士砵亭](../Page/葡萄牙士砵亭俱樂部.md "wikilink")，現時效力[美職聯球隊](../Page/美職聯.md "wikilink")[奧蘭多城](../Page/奧蘭多城足球俱樂部.md "wikilink")。

## 球員生涯

### 球會

#### 士砵亭

蘭尼出道於葡萄牙勁旅[士砵亭](../Page/士砵亭.md "wikilink")，2006/07年球季成為了球隊的正選攻擊中場，其出色的表現為球隊帶來了一項[葡萄牙盃冠軍](../Page/葡萄牙盃.md "wikilink")。由於他在[C朗拿度轉投曼聯後開始於士砵亭冒起](../Page/C朗拿度.md "wikilink")，加上踢法與其甚為相似，又同是葡萄牙國腳，所以傳媒稱他為「新C朗拿度」。蘭尼入球後的後空翻慶祝動作亦成為了他的標記。司職翼鋒的蘭尼速度快、盤带功夫出色，20歲的他已是葡萄牙最矚目新星，獲盛讚可媲美同樣出身於士砵亭、該季在[曼聯光芒四射的國家隊師兄C朗拿度](../Page/曼聯.md "wikilink")，前途無限的他勢成「老紅魔」[傑斯的接班人](../Page/傑斯.md "wikilink")。

#### 曼聯

2007年5月，英超勁旅曼聯宣佈以1400萬鎊的轉會費把他帶到[英國](../Page/英國.md "wikilink")，並與[巴西新晉國腳](../Page/巴西.md "wikilink")[安達臣一同加盟](../Page/安德森·路易斯·迪·阿布努·奧利維拉.md "wikilink")。

蘭尼於2007年夏天的曼聯季前遠東之旅，於[澳門對深圳上清飲的友賽](../Page/澳門.md "wikilink")，蘭尼首次披上曼聯17號球衣正選上陣並攻入一球，協助球隊大勝6:0。

2007年8月26日，蘭尼在对[托特纳姆热刺的英超联赛中远射破网](../Page/托特纳姆热刺.md "wikilink")，攻入正式比赛第一球，帮助球队1-0取胜。此后，蘭尼更是一直以不断的助攻证明自己的价值，他所主罚的角球极具威胁，经常能直接助攻队友进球或在对方禁区中造成混乱。

纳尼在07-08赛季的出色表现引起人们极大地关注，但在08-09赛季中，纳尼在英超联赛中表现不佳，在主力位置竞争中输给了勤奋的韩国人[朴智星](../Page/朴智星.md "wikilink")，在仅有的几次联赛首发场次中，多次被中途换下，两个赛季的对比反差让曼联球迷感到失望，一度有传言纳尼在09年夏天会被曼联扫地出门，但是纳尼本人否认了这个说法。

2010年1月，曼聯於[足總盃以](../Page/英格蘭足總盃.md "wikilink")0-1不敵[列斯聯後](../Page/列斯聯.md "wikilink")，最近有報導說領隊[費格遜準備在季後實行大換血](../Page/費格遜.md "wikilink")，把部分表現不理想的球員一起賣掉，表現不理想的球員除了他外，還有隊友[安達臣](../Page/安達臣.md "wikilink")、[貝碧托夫](../Page/貝碧托夫.md "wikilink")、[蘇蘭托錫等](../Page/蘇蘭托錫.md "wikilink")，然而賽季開始後，蘭尼以不斷的表現，終結了可能會轉隊的傳言。

其後在2010/11球季是蘭尼加盟以來表現最好的一季，其中主場對[曼城的](../Page/曼城.md "wikilink")[英超聯賽先把握單刀機會射破](../Page/2010年至2011年英格蘭超級聯賽.md "wikilink")[祖·赫特的十指關](../Page/祖·赫特.md "wikilink")，後右路妙傳[朗尼造就後者射入一記精彩的倒掛擊敗同城對手](../Page/韋恩·朗尼.md "wikilink")，蘭尼該季總計有9球進帳和18次助攻，亦令他入選該年英超最佳陣容。

2011/12球季在主場對[阿仙奴的](../Page/阿仙奴.md "wikilink")[聯賽中射入一球](../Page/2011年至2012年英格蘭超級聯賽.md "wikilink")，協助球隊大勝8:2，對[車路士則射入世界波](../Page/車路士足球會.md "wikilink")，表現一直穩定。

但其後的2012/13球季他被主帥[費格遜棄用](../Page/亞歷斯·費格遜.md "wikilink")，令他在2013/14球季前的轉會傳聞甚囂塵上，當中以[祖雲達斯對他最為積極](../Page/祖雲達斯.md "wikilink")。但最終於2013年9月5日他與曼聯簽署一份新的五年合約留隊至2018年6月\[1\]。

#### 士砵亭

2014年8月20日，由於阿根廷國腳[馬高斯·盧祖以](../Page/馬高斯·盧祖.md "wikilink")2千萬歐元，由葡超士砵亭轉投英超曼聯，24歲盧祖是[阿根廷在](../Page/阿根廷.md "wikilink")[巴西世界盃的主力](../Page/2014年世界盃.md "wikilink")，司職[左閘或](../Page/左閘.md "wikilink")[中堅](../Page/中堅.md "wikilink")。作為交易條件之一，效力曼聯7年的蘭尼，則以租借形式重返母會士砵亭，披上77號球衣。27歲的蘭尼為紅魔上陣超過220場，射入40球，其間球隊贏過4次英超和1次歐聯錦標。

重回母會後，蘭尼開局不順，對戰[阿洛卡的](../Page/阿洛卡足球會.md "wikilink")[葡超聯賽中先射失](../Page/2014/15球季葡萄牙超級足球聯賽.md "wikilink")[十二碼](../Page/十二碼.md "wikilink")，後領黃牌，賽至第77分鐘被調換離場。他的首個進球在歐冠盃對陣[斯洛文尼亞球會](../Page/斯洛文尼亞.md "wikilink")[馬里博爾的分組賽事中取得](../Page/馬里博爾足球俱樂部.md "wikilink")，雙方最後賽和1-1，他本人獲選為全場最佳。首個聯賽進球則在[基維辛迪身上取得](../Page/吉爾維森特足球俱樂部.md "wikilink")，助球隊大勝4-0。而[葡萄牙盃賽事方面](../Page/葡萄牙盃.md "wikilink")，蘭尼在第三圈與[波圖的強強對話中建功](../Page/波圖足球會.md "wikilink")，作客以3-1淘汰火龍隊，及至決賽互射十二碼階段操刀射入其中一球，助士砵亭擊敗[布拉加](../Page/布拉加足球會.md "wikilink")，贏得隊史第16座葡萄牙盃冠軍，並打破球會將近七年的錦標荒，士砵亭該季以第三名完成聯賽。自己全季共有12個進球，打破個人進球記錄。

#### 費倫巴治

2015年7月5日，蘭尼正式加盟費倫巴治。

#### 瓦倫西亞

### 國家隊

蘭尼第一次為葡萄牙國家隊上陣為2006年9月1日，賽事為國際友誼賽作客對[丹麥國家足球隊](../Page/丹麥國家足球隊.md "wikilink")，他第一場上陣的賽事便射入一球為[葡萄牙追平](../Page/葡萄牙國家足球隊.md "wikilink")2:2，但最終負2:4，當時他只有19歲。他現時於國家隊經常穿上17號球衣。2006年夏天，蘭尼亦有份代表葡萄牙參與21歲以下歐洲錦標賽，並射入一球。
可惜葡萄牙於該項賽事中表現差強人意，於分組賽便被淘汰出局。

## 榮譽

  - 士砵亭

<!-- end list -->

  - [葡萄牙盃冠軍](../Page/葡萄牙盃.md "wikilink")：2006/07年、2014/15年；

<!-- end list -->

  - 曼聯

<!-- end list -->

  - [英格蘭超級聯賽冠軍](../Page/英格蘭超級聯賽.md "wikilink")：2007/08年、2008/09年、2010/11年、2012/13年；
  - [英格蘭聯賽盃](../Page/英格蘭聯賽盃.md "wikilink")：2008/09年、2009/10年；
  - [英格蘭社區盾](../Page/英格蘭社區盾.md "wikilink")：2007年、2008年、2010年、2011年；
  - [歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")：2007/08年；
  - [國際足協世界冠軍球會盃](../Page/國際足協世界冠軍球會盃.md "wikilink")：[2008年](../Page/2008年世界冠軍球會盃.md "wikilink")；

<!-- end list -->

  - 國家隊

<!-- end list -->

  - [歐洲國家盃冠軍](../Page/歐洲國家盃.md "wikilink") (
    [2016年](../Page/2016年歐洲國家盃.md "wikilink") )

## 参考来源

## 外部連結

  -
  - [Profile](https://web.archive.org/web/20170305002356/http://www.manutd.com/default.sps?pagegid=%7BFE60904B-C2A8-4E60-9B05-700DBBC29BBC%7D&section=playerProfile&teamid=458&bioid=92680)
    at ManUtd.com

  - [Nani PortuGOAL
    profile](http://portugoal.net/index.php/player-profiles/123-players-m-o/89-player-profile-nani)

  - [UEFA.com
    profile](http://www.uefa.com/competitions/ucl/players/player=101336/index.html)

[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:葡萄牙國家足球隊球員](../Category/葡萄牙國家足球隊球員.md "wikilink")
[Category:葡萄牙足球运动员](../Category/葡萄牙足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:士砵亭球員](../Category/士砵亭球員.md "wikilink")
[Category:曼聯球員](../Category/曼聯球員.md "wikilink")
[Category:費倫巴治球員](../Category/費倫巴治球員.md "wikilink")
[Category:華倫西亞球員](../Category/華倫西亞球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:奧蘭多城球員](../Category/奧蘭多城球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:美職球員](../Category/美職球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:土耳其外籍足球運動員](../Category/土耳其外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:美國外籍足球運動員](../Category/美國外籍足球運動員.md "wikilink")
[Category:葡萄牙旅外足球運動員](../Category/葡萄牙旅外足球運動員.md "wikilink")
[Category:在英國的葡萄牙人](../Category/在英國的葡萄牙人.md "wikilink")
[Category:在土耳其的葡萄牙人](../Category/在土耳其的葡萄牙人.md "wikilink")
[Category:維德角裔葡萄牙人](../Category/維德角裔葡萄牙人.md "wikilink")
[Category:非洲裔葡萄牙人](../Category/非洲裔葡萄牙人.md "wikilink")
[Category:維德角人](../Category/維德角人.md "wikilink")
[Category:里斯本人](../Category/里斯本人.md "wikilink")
[Category:2016年歐洲國家盃球員](../Category/2016年歐洲國家盃球員.md "wikilink")

1.