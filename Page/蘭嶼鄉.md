**蘭嶼鄉**（[達悟語](../Page/達悟語.md "wikilink")：**Ponso no
Tao**），屬[台灣](../Page/臺灣.md "wikilink")[臺東縣轄下的行政區](../Page/臺東縣.md "wikilink")，涵蓋[蘭嶼](../Page/蘭嶼.md "wikilink")、[小蘭嶼及其他零碎小島](../Page/小蘭嶼.md "wikilink")（礁）。位於[台灣本島](../Page/臺灣.md "wikilink")（[達悟族人稱](../Page/達悟族.md "wikilink")：**Ilaod**）東南外海、[太平洋](../Page/太平洋.md "wikilink")（[菲律賓海](../Page/菲律賓海.md "wikilink")）中。

達悟族語的名稱意為「**人之島**」，漢人以[閩南語音譯](../Page/閩南語.md "wikilink")**紅頭嶼**或紅豆嶼，西方國家早年稱蘭嶼為「Botel
Tobago」（譯為-{煙草島}-）。1947年因島上盛產[蝴蝶蘭而改名](../Page/蝴蝶蘭屬.md "wikilink")。原住民與菲律賓[巴丹島原住民語言相通](../Page/巴丹島.md "wikilink")。

## 歷史

[Ponso_no_Tao.jpg](https://zh.wikipedia.org/wiki/File:Ponso_no_Tao.jpg "fig:Ponso_no_Tao.jpg")）的拼板舟；取自1931年[總督府文書](../Page/臺灣總督府.md "wikilink")\]\]
1644年，[荷蘭人派兵登陸蘭嶼探勘](../Page/荷蘭人.md "wikilink")。

1645年，荷蘭人二度派兵登陸蘭嶼探勘。

1877年（清[光緒](../Page/光緒.md "wikilink")3年），恆春知縣[周有基將紅頭嶼](../Page/周有基.md "wikilink")（蘭嶼）併入清帝國版圖，隸屬[恒春縣](../Page/恒春縣.md "wikilink")\[1\]。

1895年，[馬關條約簽訂後](../Page/馬關條約.md "wikilink")，[日本政府禁止一般人進入蘭嶼內開發](../Page/日本.md "wikilink")，並設立研究區。

1897年，日本人[鳥居龍藏第一次入內探勘](../Page/鳥居龍藏.md "wikilink")，稱居民「**yami**」（雅美），這是達悟族以前被稱為雅美族的由來。

1946年，蘭嶼鄉雖為離島，但在[日本時代](../Page/日本時代.md "wikilink")（1895－1945年）被日本殖民者劃入[台東廳](../Page/臺東廳.md "wikilink")[台東郡的](../Page/臺東郡.md "wikilink")[蕃地之一部份](../Page/蕃地_\(台東郡\).md "wikilink")，所以[二戰後](../Page/臺灣戰後時期.md "wikilink")，在行政制度上，全臺灣的[蕃地都被改編成立各地新的](../Page/蕃地.md "wikilink")[山地鄉](../Page/山地鄉.md "wikilink")（[山地原住民鄉](../Page/山地鄉.md "wikilink")），所以本鄉也被劃為[山地鄉的類別](../Page/山地鄉.md "wikilink")，成立當時，本鄉初名為「**紅頭嶼鄉**公所」。

1947年，藉盛產[蝴蝶蘭的名義而更名為](../Page/蝴蝶蘭.md "wikilink")「**蘭嶼鄉**」。而本鄉也是目前[台東縣行政編制上的](../Page/臺東縣.md "wikilink")5個[山地原住民鄉之一](../Page/山地鄉.md "wikilink")。

1982年，[行政院原子能委員會在蘭嶼建造低放射](../Page/行政院原子能委員會.md "wikilink")[核廢料儲存場](../Page/放射性废料.md "wikilink")—[蘭嶼貯存場](../Page/蘭嶼貯存場.md "wikilink")，於1990年七月[台灣電力公司依據行政院頒布之](../Page/台灣電力公司.md "wikilink")「放射性廢棄物管理方針」接管該場之經營，原能會則負責安全監督工作。直到現在在鄉內仍然有不少反對的聲音。\[2\]\[3\]

1996年，達悟族人因懷疑台電運料船夾帶高放射核廢料而發動激烈抗爭，停運任何核廢料至今，目前[台灣核電廠產生的低階核廢料](../Page/臺灣發電廠列表.md "wikilink")，都暫時貯存在各電廠中。\[4\]

2002年1月17日[立法院通過](../Page/立法院.md "wikilink")《[離島建設條例](../Page/離島建設條例.md "wikilink")》第十四條修正案（2月6日總統公布），規定：「蘭嶼地區住民自用住宅之用電費用應予免收」，其立法理由是：「增訂但書蘭嶼地區用電費用免收的規定，係基於達悟族人特別犧牲而給予的合理補（賠）償。」\[5\]

2012年8月，[天秤颱風有二度經過台灣東南海域](../Page/颱風天秤_\(2012年\).md "wikilink")，對綠島、蘭嶼造成嚴重災情。以蘭嶼災情而言，由於水電中斷，加上島上唯一超市、加油站全被掃平而消失，出現無法供油、缺糧、通信中斷等災情，並還有消防隊因為被土石擋道，無法出動救援車輛前往救災，信用部與雜貨店也都有毀損。在蘭嶼的海陸交通方面：[開元港積滿殘骸](../Page/開元港.md "wikilink")，港內漁船毀損或被漂流到港外海域，甚至於有漁船被海浪給沖上岸；環島公路全佈滿巨岩礁石擋道，還有路基掏空、坍方等路阻狀況；[蘭嶼航空站的跑道全被漂流木](../Page/蘭嶼機場.md "wikilink")、礁石擋道，還有部分路基有被掏空的現象，使得航空站得關閉進行清理與搶修，造成二天無法正常營運。

## 地理

[Lanyu_Ivalino_Ia-gin_village_hill_view.jpg](https://zh.wikipedia.org/wiki/File:Lanyu_Ivalino_Ia-gin_village_hill_view.jpg "fig:Lanyu_Ivalino_Ia-gin_village_hill_view.jpg")
蘭嶼位於[西太平洋位置](../Page/太平洋.md "wikilink")，在臺灣的東南方，在[綠島的南方](../Page/綠島.md "wikilink")，南臨[-{巴士}-海峽與](../Page/巴士海峽.md "wikilink")[菲律賓之](../Page/菲律賓.md "wikilink")[巴丹群島遙遙相望](../Page/巴丹群島.md "wikilink")。巴丹群島最北端的[雅米島](../Page/雅米島.md "wikilink")，距離小蘭嶼99公里。即東經121度5分，北緯22度間。面積有48.3892平方公里。氣候為[熱帶雨林氣候](../Page/熱帶雨林氣候.md "wikilink")，年雨量常在3,000公釐以上，年降雨日數達224天。

蘭嶼為[火山島地形](../Page/火山岛.md "wikilink")，最高點[紅頭山海拔](../Page/紅頭山.md "wikilink")552[公尺](../Page/公尺.md "wikilink")。大部分為[山地](../Page/山地.md "wikilink")，島上[丘陵綿亙](../Page/丘陵.md "wikilink")，溪流分歧，只有沿海部分為平地、海岸線曲折，熱帶林木遍佈。蘭嶼的地質以[角閃石的](../Page/角闪石.md "wikilink")[安山岩質熔岩及](../Page/安山岩.md "wikilink")[玄武岩質的集塊岩為主](../Page/玄武岩.md "wikilink")。無人島小蘭嶼則以黑雲母角閃石為主。

## 行政

### 部落（村）

蘭嶼過去有曾有七個部落，目前劃分為四村，分別為[椰油村](../Page/椰油村.md "wikilink")（椰油、<s>伊法達斯</s>）、[紅頭村](../Page/紅頭村.md "wikilink")（紅頭、漁人）、[東清村](../Page/東清村.md "wikilink")（東清、野銀）、[朗島村](../Page/朗島村.md "wikilink")（朗島）。

| 部落名             | 漢名   | 註解                |
| --------------- | ---- | ----------------- |
| Jiayo           | 椰油   | 行政區劃為椰油村          |
| Jiraralay       | 朗島   | 行政區劃為朗島村          |
| Jiranmilek      | 東清   | 行政區劃為東清村          |
| Jivalino        | 野銀   | 隸屬東清村             |
| Jimowrod/Imorod | 紅頭   | 行政區劃為紅頭村          |
| Jiratay         | 漁人   | 1946年隸屬紅頭村        |
| Jivatas         | 伊法達斯 | 1940年劃入椰油村，現已無人居住 |

<table>
<tbody>
<tr class="odd">
<td><p><font size="-1"><strong>蘭嶼鄉行政區劃</strong></font></p></td>
</tr>
<tr class="even">
<td><div style="position: relative;font-size:100%">
<p><a href="https://zh.wikipedia.org/wiki/File:Lanyu_villages.svg" title="fig:Lanyu_villages.svg">Lanyu_villages.svg</a>    </p>
</div></td>
</tr>
</tbody>
</table>

### 歷任鄉長

| 任次  | 姓名                                             | 任期         |
| --- | ---------------------------------------------- | ---------- |
| 第一任 | [周雅雯](../Page/周雅雯.md "wikilink")               | 1985\~1993 |
| 第二任 | [廖班佳](../Page/廖班佳.md "wikilink")               | 1993\~2001 |
| 第三任 | [周貴光](../Page/周貴光.md "wikilink")               | 2001\~2009 |
| 第四任 | [江多利](../Page/江多利.md "wikilink")               | 2009\~2014 |
| 第五任 | [夏曼·迦拉牧](../Page/夏曼·迦拉牧.md "wikilink")（漢名：張慶來） | 2014\~     |

### 政治參與

蘭嶼鄉民的投票率，於[2008年中華民國立法委員選舉中為臺東縣倒數的](../Page/2008年中華民國立法委員選舉.md "wikilink")。根據其政黨票開票結果，蘭嶼人雖偏向[中國國民黨](../Page/中國國民黨.md "wikilink")（70.4%）或[民主進步黨](../Page/民主進步黨.md "wikilink")（9.6%），但在12個參與的政黨當中，有10個政黨得到超過1%，在所有鄉鎮中算是較特別的。[新黨](../Page/新黨.md "wikilink")、[台灣團結聯盟皆超過](../Page/台灣團結聯盟.md "wikilink")4%。也許是因為抗核廢料的緣故，[綠黨得票數在蘭嶼的佔有率是全國第一](../Page/綠黨.md "wikilink")。

[2012年立委選舉](../Page/2012年中華民國立法委員選舉.md "wikilink")，[綠黨派出蘭嶼達悟族單親媽媽](../Page/綠黨_\(臺灣\).md "wikilink")[希婻．瑪飛洑參選不分區第一名](../Page/希婻．瑪飛洑.md "wikilink")。結果綠黨在蘭嶼得票35.76%（對比全國得票率1.74%），是全國得票率最高的地方。

## 人口

蘭嶼鄉的主要民族為[達悟族](../Page/達悟族.md "wikilink")，通行語言為[達悟語](../Page/達悟語.md "wikilink")\[6\]。蘭嶼鄉內90%的居民為本地的達悟族人，其他為居民多自台灣本島移入。蘭嶼的祖先來自菲律賓[巴丹島原住民](../Page/巴丹島.md "wikilink")。達悟族人大約於800年前，自現今[菲律賓](../Page/菲律賓.md "wikilink")[巴丹群島遷入](../Page/巴丹群島.md "wikilink")，因此達悟族人跟巴丹人的語言及文化有許多共通的地方。達悟族人製作[拼板舟](../Page/拼板舟.md "wikilink")，並在春夏季節出海捕撈[飛魚](../Page/飛魚.md "wikilink")，亦稱為「飛魚季」，因此蘭嶼有「飛魚的故鄉」之稱。拼版舟是由許多不一樣品種的樹木，製成木板再拼裝而成的，並非一體成型的[獨木舟](../Page/獨木舟.md "wikilink")。

## 交通

### 對外航線

海運由新發輪船運公司負責航運，每週定期行駛兩次，從[開元漁港出發](../Page/開元漁港.md "wikilink")，分別往來[墾丁](../Page/墾丁.md "wikilink")、台東[富岡漁港](../Page/富岡漁港.md "wikilink")、[綠島](../Page/綠島.md "wikilink")，多為旅行團包船，也開放餘票供一般民眾搭乘。\[7\]

空運方面，[蘭嶼機場與](../Page/蘭嶼機場.md "wikilink")[臺東機場間設有每日](../Page/臺東機場.md "wikilink")6班往返定期航線，由[德安航空營運](../Page/德安航空.md "wikilink")，使用19人座小飛機。\[8\]

### 環島公路

[蘭嶼鄉公所經營之大型客車](../Page/蘭嶼鄉公所.md "wikilink")，每日行駛島內兩班次。環島一週約37公里，若騎乘[摩托車悠閒行進](../Page/摩托車.md "wikilink")，約90分鐘可環繞全島。\[9\]蘭嶼鄉全鄉公路皆沒有[紅綠燈](../Page/紅綠燈.md "wikilink")。

## 教育

  - 高級中等學校

<!-- end list -->

  - [臺東縣立蘭嶼高級中學](../Page/臺東縣立蘭嶼高級中學.md "wikilink")

<!-- end list -->

  - 國民小學

<!-- end list -->

  - [臺東縣蘭嶼鄉蘭嶼國民小學](http://www.laps.ttct.edu.tw/front/bin/home.phtml)
  - [臺東縣蘭嶼鄉東清國民小學](http://www.donps.ttct.edu.tw/front/bin/home.phtml)
  - [臺東縣蘭嶼鄉朗島國民小學](http://210.240.145.196/school/web/index.php)
  - [臺東縣蘭嶼鄉椰油國民小學](http://www.yyps.ttct.edu.tw/front/bin/rcglist.phtml?Rcg=1)
  - [臺東縣蘭嶼鄉紅頭國民小學](https://web.archive.org/web/20130412070149/http://220.134.186.181/)

## 旅遊

[Langyu_seaside.jpg](https://zh.wikipedia.org/wiki/File:Langyu_seaside.jpg "fig:Langyu_seaside.jpg")
[Langyu_smallskypool.jpg](https://zh.wikipedia.org/wiki/File:Langyu_smallskypool.jpg "fig:Langyu_smallskypool.jpg")

  - Jimagaod（[小蘭嶼](../Page/小蘭嶼.md "wikilink")）：位於蘭嶼東南方的無人島，是一座[火山島](../Page/火山岛.md "wikilink")，距離蘭嶼本島大約三[浬左右航程](../Page/浬.md "wikilink")，當地人稱為神秘島。小蘭嶼面積為1.75平方公里，環島全長約4.3公里，海岸多為懸崖分佈，國軍將其列為[飛彈的射擊](../Page/飛彈.md "wikilink")[靶場](../Page/靶場.md "wikilink")。
  - [大天池](../Page/大天池_\(蘭嶼\).md "wikilink")
  - [小天池](../Page/小天池_\(蘭嶼\).md "wikilink")
  - [野銀舊部落](../Page/東清村.md "wikilink")
  - [朗島舊部落](../Page/朗島村.md "wikilink")
  - [八代灣](../Page/八代灣.md "wikilink")
  - [東清灣](../Page/東清灣.md "wikilink")
  - [紅頭岩](../Page/紅頭岩.md "wikilink")
  - Jiahaod（[軍艦岩](../Page/軍艦岩_\(蘭嶼\).md "wikilink")）
  - Jikazahem（[五孔洞](../Page/五孔洞.md "wikilink")）
  - Jimacingeh（[鱷魚岩](../Page/鱷魚岩.md "wikilink")）
  - Jimalacip（[象鼻岩](../Page/象鼻岩.md "wikilink")）
  - Jimaricing（[龍頭岩](../Page/龍頭岩.md "wikilink")）
  - Jimavonot（[火把岩](../Page/火把岩.md "wikilink")）
  - Jimazamay（位於現在核廢料[蘭嶼存放場](../Page/蘭嶼貯存場.md "wikilink")）
  - Jipanatosan（[雙獅岩](../Page/雙獅岩.md "wikilink")）
  - Jipaneytayan（[坦克岩](../Page/坦克岩.md "wikilink")）
  - Jirakarang（[虎頭坡](../Page/虎頭坡.md "wikilink")）
  - Pinadon no vahao（[鋼盔岩](../Page/鋼盔岩.md "wikilink")）
  - [巴丹島](../Page/巴丹島.md "wikilink")（隸屬菲律賓[巴丹群島省](../Page/巴丹群島省.md "wikilink")）

## 參考資料

## 外部連結

  - [蘭嶼鄉公所](http://www.lanyu.gov.tw)

[Category:蘭嶼鄉](../Category/蘭嶼鄉.md "wikilink")
[Category:臺東縣行政區劃](../Category/臺東縣行政區劃.md "wikilink")
[Category:台東縣旅遊景點](../Category/台東縣旅遊景點.md "wikilink")
[Category:山地鄉](../Category/山地鄉.md "wikilink")
[Category:台灣地標](../Category/台灣地標.md "wikilink")

1.  《蘭嶼入我版圖之沿革》，林熊祥，1958年，臺灣省文獻委員會印行
2.  [反核廢料 蘭嶼鄉民明天抗爭
    不滿政府及台電未兌現年底遷離貯存場承諾](http://www.abohome.org.tw/index.php?option=com_content&view=article&id=1219:records-record15-1219&catid=46:record14&Itemid=245)
3.  [蘭嶼貯存場安全管制說明](http://www.aec.gov.tw/%E6%A0%B8%E7%89%A9%E6%96%99%E7%AE%A1%E5%88%B6/%E7%AE%A1%E5%88%B6%E8%83%8C%E6%99%AF%E8%AA%AA%E6%98%8E/%E8%98%AD%E5%B6%BC%E8%B2%AF%E5%AD%98%E5%A0%B4%E5%AE%89%E5%85%A8%E7%AE%A1%E5%88%B6%E8%AA%AA%E6%98%8E--6_47_164.html)
4.  [《台灣核廢料何去何從》蘭嶼
    綠色壕溝藏「惡靈」](http://www.libertytimes.com.tw/2002/new/jul/7/today-i1.htm)

5.  [立法院法律系統《離島建設條例》民國91年（2002年）條文立法理由](http://lis.ly.gov.tw/lghtml/lawstat/reason2/0123591011700.htm)
6.  [55公所
    原民「地方通行語」公告了](http://news.ltn.com.tw/news/politics/breakingnews/2220153)
7.  船期及訂票詳見[船遊網](http://www.ezboat.com.tw/)。
8.  航期及訂票詳見[德安航空](http://www.dailyair.com.tw/)。
9.