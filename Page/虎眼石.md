[Tigers_eye_01937.jpg](https://zh.wikipedia.org/wiki/File:Tigers_eye_01937.jpg "fig:Tigers_eye_01937.jpg")
[Polished_Tiger's_eye_Macro.JPG](https://zh.wikipedia.org/wiki/File:Polished_Tiger's_eye_Macro.JPG "fig:Polished_Tiger's_eye_Macro.JPG")

**虎眼石**（*Tigerite*或*Tiger's
eye*），或稱**虎睛石**，是一種具有[貓眼效果的](../Page/貓眼效果.md "wikilink")[寶石](../Page/寶石.md "wikilink")，多呈[黃](../Page/黃色.md "wikilink")[棕色](../Page/棕色.md "wikilink")，寶石內帶有仿[絲質的光紋](../Page/絲.md "wikilink")。虎眼石是[石英的其中一個品種](../Page/石英.md "wikilink")，這種寶石可以利用青石棉纖維[硅製作進行假晶替代](../Page/二氧化硅.md "wikilink")。硅化的未完成過程所產生的[藍色變異種](../Page/藍色.md "wikilink")，則稱為「**鷹眼石**」。

## 特徵

虎眼石經常以凸圓形寶石切割法處理，這種處理方法最能突出虎眼石本身的貓眼光。虎眼石經過微熱加工可以變成紅色。漂亮的[蜜糖色虎眼石有機會可以冒充另一種價值更高的貓眼石](../Page/蜜糖.md "wikilink")「[金綠寶石](../Page/金綠寶石.md "wikilink")（*chrysoberyl*）」，不過效果通常不能使人滿意。以人造[纖維](../Page/光導纖維.md "wikilink")[玻璃所製作的寶石是虎眼石常見的替代石](../Page/玻璃.md "wikilink")，而且這種纖維玻璃還能製作出很多種顏色的寶石。

[tiger_iron.jpg](https://zh.wikipedia.org/wiki/File:tiger_iron.jpg "fig:tiger_iron.jpg")
另外有一種與虎眼石極有關連的岩石，名叫**鐵虎眼**。鐵虎眼是一種滲合天然虎眼石、[紅碧玉及](../Page/賈斯珀.md "wikilink")[黑色](../Page/黑色.md "wikilink")[赤鐵礦而成的](../Page/赤鐵礦.md "wikilink")[礦石](../Page/礦石.md "wikilink")。它的成色、光紋對比都非常華麗，經常被製作為[珠寶](../Page/珠寶.md "wikilink")、高價裝飾品、[念珠以及鑲嵌在刀柄上的寶石](../Page/念珠.md "wikilink")。鐵虎眼與虎眼石大多採自[南非及](../Page/南非.md "wikilink")[西澳大利亞州](../Page/西澳大利亞州.md "wikilink")，主要成份是[二氧化硅](../Page/二氧化硅.md "wikilink")，[密度約為](../Page/密度.md "wikilink")2.64至2.71\[1\]，由變種青石棉及酸化鐵構成石英質的顏色。虎眼石的著名產源主要來自[美國](../Page/美國.md "wikilink")、[南非](../Page/南非.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[中國](../Page/中國.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[納米比亞](../Page/納米比亞.md "wikilink")、[印度及](../Page/印度.md "wikilink")[緬甸](../Page/緬甸.md "wikilink")。

## 備注

<References />

  - [Allenpress：虎眼石與鷹眼石的形成](https://web.archive.org/web/20070927214939/http://lists.allenpress.com/GSA/i0091-7613-31-6-e44.pdf)

[Category:寶石](../Category/寶石.md "wikilink")
[Category:珠寶](../Category/珠寶.md "wikilink")
[Category:礦物小作品](../Category/礦物小作品.md "wikilink")
[Category:矽酸鹽礦物](../Category/矽酸鹽礦物.md "wikilink")

1.  [柏克萊加州大學網站](http://ist-socrates.berkeley.edu/~eps2/wisc/sg.html)