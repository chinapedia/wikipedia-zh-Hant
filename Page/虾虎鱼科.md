**虾虎鱼**即指鱼类分类学[鰕虎目中的](../Page/鰕虎目.md "wikilink")**虾-{虎}-鱼科**（），又作**鰕-{虎}-科**。它是[鱼类中最大的](../Page/鱼.md "wikilink")[科之一](../Page/科.md "wikilink")，已知[品種超过](../Page/种（生物）.md "wikilink")2000种。绝大多数体型細小，一般短于10厘米。世界上最短小的[脊椎動物要數](../Page/脊椎動物.md "wikilink")[微蝦虎魚](../Page/微蝦虎魚.md "wikilink")（*Trimmatom
nanus*）和[矮蝦虎魚](../Page/矮蝦虎魚.md "wikilink")（*Pandaka
pygmaea*）也屬虾虎鱼科，它们成大后也短于1厘米。体型较大的虾虎魚有[拟虾虎鱼](../Page/拟虾虎鱼.md "wikilink")（*Gobioides*）种类和[斑駁尖塘鱧](../Page/斑駁尖塘鱧.md "wikilink")，体长可達30厘米，但這是少有的例外。尽管虾虎鱼本身不是人们的重要食物来源，但它们却是许多经济鱼类（如[鳕鱼](../Page/鳕鱼.md "wikilink")、[黑线鳕](../Page/黑线鳕.md "wikilink")、[黑鲈和](../Page/黑鲈.md "wikilink")[比目鱼等](../Page/比目鱼.md "wikilink")）的重要食糧。有些虾虎鱼还是很受水族馆欢迎的种类，如[短虾虎鱼屬](../Page/短虾虎鱼屬.md "wikilink")（*Brachygobius*）的种类等。

虾虎鱼类最突出的形态特征就是其腹鳍愈合成一吸盘状。该吸盘的功能与[鮣的背鳍吸盘和](../Page/鮣.md "wikilink")[圆鳍鱼科的腹鳍吸盘类似](../Page/圆鳍鱼.md "wikilink")，但在解剖上是十分不同的结构，因此只是[趋同进化的结果](../Page/趋同进化.md "wikilink")。经常可以看到野生的虾虎鱼以吸盘吸附在岩石或[珊瑚上](../Page/珊瑚.md "wikilink")，在水族箱中它们也很乐意吸在鱼缸的玻璃上。

虾虎鱼基本上生存于浅海环境，包括[潮间带水坑](../Page/潮间带.md "wikilink")、[珊瑚礁和](../Page/珊瑚礁.md "wikilink")[海草牧场](../Page/海草牧场.md "wikilink")，也大量存在于海水和河口栖息地，包括[河流下游](../Page/河流下游.md "wikilink")、[红树林湿地和](../Page/红树林湿地.md "wikilink")[盐沼地](../Page/盐沼地.md "wikilink")。只有少数种类可完全适应于淡水环境，當中包括[亚洲河流中的](../Page/亚洲.md "wikilink")[吻虾虎鱼屬](../Page/吻虾虎鱼屬.md "wikilink")（*Rhinogobius*）、[澳大利亚](../Page/澳大利亚.md "wikilink")[沙漠的](../Page/沙漠.md "wikilink")[雷虾虎鱼](../Page/雷虾虎鱼.md "wikilink")（*Redigobius*）及[欧洲的](../Page/欧洲.md "wikilink")[淡水虾虎鱼](../Page/淡水虾虎鱼.md "wikilink")（*Padagobius
martensii*）。

## 分类

虾虎鱼科的分类经历了重大修正，[虾虎鱼亚目下原有几个独立的科](../Page/虾虎鱼亚目.md "wikilink")：[蚓鰕虎科](../Page/蚓鰕虎科.md "wikilink")（Microdesmidae）、[沙鰕虎科](../Page/沙鰕虎科.md "wikilink")（Kraemeriidae）及[辛氏鱼科](../Page/辛氏鱼科.md "wikilink")（Schindleriidae）目前都被并入本科。

相反的，本科除了原有的[虾虎鱼亚科](../Page/虾虎鱼亚科.md "wikilink")（Gobiinae）保持不变外，以下四个过去曾归类于本科的亚科均已被独立出来成为另外一科，即[背眼虾虎鱼科](../Page/背眼虾虎鱼科.md "wikilink")（Oxudercidae）：

  - [近盲虾虎鱼亚科](../Page/近盲虾虎鱼亚科.md "wikilink")（Amblyopinae）：如[鳗虾虎鱼类](../Page/鳗虾虎鱼类.md "wikilink")（*Taenioides*）、[狼牙虾虎鱼类](../Page/狼牙虾虎鱼类.md "wikilink")（*Odontamblyopus*）。俗称「奶鱼」。
  - [拟虾虎鱼亚科](../Page/拟虾虎鱼亚科.md "wikilink")（Gobionellinae）
  - [背眼虾虎鱼亚科](../Page/背眼虾虎鱼亚科.md "wikilink")（Oxudercinae）：如[弹涂鱼类](../Page/弹涂鱼.md "wikilink")。中国过去称此亚科為「弹涂鱼科（Periophthalmidae）」，与虾虎鱼科并列归入虾虎鱼亚目当中。现已并入虾虎鱼科作为一亚科。
    胸鳍基底发达成臂状，在陆上充当行动器官。眼背位，靠近，突出，下眼睑发达。下颌牙仅一排。
  - [双盘虾虎鱼亚科](../Page/双盘虾虎鱼亚科.md "wikilink")（Sicydiinae）

虾虎鱼类由于非主流经济鱼类，因此受人们关注不及其它的大型鱼类。也由于其多样性极为丰富、某些种屬间形态相近，鉴别难度大，因此对于其分类，尤其是屬内及屬间分类尚存在一些争议。本科鱼类估计尚有不少未被描述和定名的物种有待人们发现。（详见[虾虎鱼科分类表](../Page/虾虎鱼科分类表.md "wikilink")）

## 共生现象

[Goby_fish_with_shrimp.jpg](https://zh.wikipedia.org/wiki/File:Goby_fish_with_shrimp.jpg "fig:Goby_fish_with_shrimp.jpg")
一些虾虎鱼种类与掘穴的[虾类](../Page/虾.md "wikilink")[共生](../Page/互利共生.md "wikilink")。虾负责打理两者共同居住的洞穴。小虾的视力不及虾虎鱼，但如果它看见或感觉到虾虎突然游回洞穴，它便能跟着缩回。虾虎鱼和小虾总是保持着联系,
虾通过其触角触碰虾虎, 当有危险时，虾虎轻拍尾鳍以示警告。这类虾虎因此有时又叫'看门虾虎'。代表屬有丝虾虎鱼屬 *Cryptocentrus*,
*Amblyeleostris*等。

另一种共生现象可以在鮈虾虎魚（*Gobiosoma*
spp.）看到。它们都扮演了清洁工的角色，为各种大鱼清除皮肤、鳍、口和鳃中的寄生虫。这种共生关系最令人惊讶的就是许多来到"清洁厂"的鱼类在其它情况下会将这样的小鱼当作美味。（如
groupers 和 snappers）。

## 塘鳢

**塘鳢**隶屬于[塘鳢科](../Page/塘鳢科.md "wikilink")（Eleotridae），与虾虎鱼科 Gobiidae
十分接近。和“真”虾虎鱼相似，它们通常是体型小的海产鱼类，生活于底层水域。经常藏身植物、洞穴或岩石和[珊瑚的缝隙中](../Page/珊瑚.md "wikilink")。尽管它们与虾虎鱼在许多方面类似，但塘鳢没有腹鳍吸盘，加上其它形态学差异，就可以区分这两个科。多数人相信
Gobiidae 和 Eleotridae来自共同的祖先, 因此它们和其它一些类似虾虎的鱼类被同置于虾虎鱼亚目Gobioidei当中。

*Dormitator* 和 *Eleotris* 是两个分布最广泛的屬, 包含了各种生活于海洋、河口和淡水的种。例如，*Dormitator
maculatus*可长至（30 cm）长，广泛分布于美国东南部和墨西哥的浅海区。还有一些肉食性的塘鳢可以生长至更大,
例如东南亚淡水的*Oxyeleotris marmorata*, 可长达（60 cm）。然而，多数种都较小,
例如澳大利亚的淡水和咸水种类*Hypseleotris* spp., 当地称为 "gudgeons"
（不要与欧亚淡水的[鲤科鱼类](../Page/鲤科.md "wikilink")
*Gobio gobio* 混淆）。

## 参考文献

## 外部链接

  - [FishBase
    虾虎鱼科条目](http://www.fishbase.org/Summary/FamilySummary.cfm?ID=405)
  - [Gobioid Research Institute](http://www.gobiidae.com)
  - [Richard Mleczko's Mudskipper & Goby
    Page](http://www.ozemail.com.au/~thebobo/goby.htm)
  - [Article on cleaner gobies in
    aquaria](http://www.wetwebmedia.com/clnrfaqs.htm)
  - [Brackish Water Aquarium FAQ entry on
    gobies](https://web.archive.org/web/20041106235638/http://homepage.mac.com/nmonks/aquaria/brackfaqpart6.html#gobiidae)
  - [虾虎鱼科亚科检索](http://gobiidae.com/FAO_gobiid_IP.htm)

[Category:共生](../Category/共生.md "wikilink")
[\*](../Category/虾虎鱼科.md "wikilink")