**蛇肉**，是主要流行于[中国](../Page/中国.md "wikilink")[华南](../Page/华南.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[美国](../Page/美国.md "wikilink")、[琉球及](../Page/琉球.md "wikilink")[東南亞等地的一种肉类](../Page/東南亞.md "wikilink")。中国是世界上最大的蛇肉消费国，每年消费的蛇肉超过一万吨\[1\]，因蛇常为野生捕获，为此许多珍稀蛇类濒临灭绝。这种食用习惯常受到动物保护组织的谴责。

## 華人地區

广东人自古就吃蛇肉和使用蛇皮，並相信蛇肉有食療功效\[2\]\[3\]。蛇肉经常被做成[蛇羹](../Page/蛇羹.md "wikilink")、或煮、烤后食用。設計出各種蛇宴，有[五蛇龍鳳會](../Page/五蛇龍鳳會.md "wikilink")、[龍王夜宴](../Page/龍王夜宴.md "wikilink")、[龍吟虎嘯](../Page/龍吟虎嘯.md "wikilink")、[龍鳳始祥等](../Page/龍鳳始祥.md "wikilink")。

[香港仍有不少市民](../Page/香港.md "wikilink")，在秋冬時節到蛇肉食肆進食[蛇羹或飲用蛇湯進補的習慣](../Page/蛇羹.md "wikilink")\[4\]。

蛇是中国少数民族[瑶族的守护神灵](../Page/瑶族.md "wikilink")，所以瑶族禁止吃蛇肉。

[台灣也有人吃蛇肉但不普遍](../Page/台灣.md "wikilink")，[台北市的](../Page/台北市.md "wikilink")[華西街夜市有現場宰蛇的店](../Page/華西街夜市.md "wikilink")。

## 琉球

蛇肉是[琉球料理常用食材之一](../Page/琉球料理.md "wikilink")，[琉球人喜歡吃](../Page/琉球人.md "wikilink")[半环扁尾海蛇](../Page/半环扁尾海蛇.md "wikilink")（，），會用來做菜和煮[永良部湯](../Page/永良部湯.md "wikilink")（）。亦有食用另一種叫「[本波布](../Page/本波布.md "wikilink")」（[琉球語](../Page/琉球語.md "wikilink")：，學名：*Protobothrops
flavoviridis*，簡稱「波布」（）的[毒蛇](../Page/毒蛇.md "wikilink")。

## 相关条目

  - [龙虎凤](../Page/龙虎凤.md "wikilink")
  - [太史五蛇羹](../Page/太史五蛇羹.md "wikilink")

## 參考

<references />

## 外部链接

  - [BBC中文网:蛇肉畅销,中国蛇濒临灭绝](http://news.bbc.co.uk/chinese/simp/hi/newsid_1690000/newsid_1694800/1694833.stm)
  - [中国农民称吃蛇使谷物歉收](http://news.bbc.co.uk/chinese/simp/hi/newsid_1140000/newsid_1141500/1141511.stm)
  - [每天万斤死蛇流入市场
    蛇肉发臭食之无益](http://news.163.com/41119/1/15I3RB9H0001122B.html)

[Category:蛇肉](../Category/蛇肉.md "wikilink")

1.
2.  [东汉](../Page/东汉.md "wikilink")[杨孚](../Page/杨孚.md "wikilink")《[异物志](../Page/异物志.md "wikilink")》：髯惟大蛇，既洪且长；采色驳荦，其文锦章。食豕吞鹿，腴成养创。宾享嘉宴，是豆是觞。
3.  宋[周去飞](../Page/周去飞.md "wikilink")《[岭外代答](../Page/岭外代答.md "wikilink")》：蚺蛇，能食獐鹿，人見獐鹿驚逸，必知其為蛇，相與赴之，環而謳歌，呼之曰徙架反，謂姊也。蛇聞歌卽俯首，人競採野花置蛇首，蛇愈伏，乃投以木株，蛇就枕焉。人掘坎枕側，蛇不顧也。坎成，以利刃一揮，墮首於坎，急壓以土，人乃四散。食頃，蛇身騰擲，一方草木為摧。旣死，則剥其皮以鞔鼓，取其膽以和藥，飽其肉而棄其膏。蓋膏能痿人陽道也
4.  [蛇羹：香港人的暖冬佳品](http://www.nanzao.com/tc/14c3151671171c9/she-geng-xiang-gang-ren-di-nuan-dong-jia-pin)，[南華早報](../Page/南華早報.md "wikilink")，2013年11月21日