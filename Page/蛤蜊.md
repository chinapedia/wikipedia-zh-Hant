**蛤蜊**（），常被誤寫為**蛤蠣**，也稱為**蛤**、**文蛤**、**西施舌**、**蚌**、****、**花蛤**（部分方言误作花甲），是[雙殼綱](../Page/雙殼綱.md "wikilink")[軟體動物](../Page/軟體動物.md "wikilink")[簾蛤目多個物種的統稱](../Page/簾蛤目.md "wikilink")，不一定只限於[蛤蜊總科的物種](../Page/蛤蜊總科.md "wikilink")，但一般皆指其中某些種類可以食用的物種。

## 概述

蛤蜊是一種對於可食用[雙殼綱貝類的泛稱](../Page/雙殼綱.md "wikilink")。

在中國古代，蛤、或蚌泛稱具兩片相等的殼的[軟體動物](../Page/軟體動物.md "wikilink")，有時特指[文蛤](../Page/文蛤.md "wikilink")\[1\]。蛤蜊則是指生長在東南沿海中的[軟體動物](../Page/軟體動物.md "wikilink")\[2\]。

在福建地區，蛤蜊通常指[泥蚶](../Page/泥蚶.md "wikilink")。

在台灣，可食用的雙瞉貝類都泛稱為蛤蜊，如[文蛤](../Page/文蛤.md "wikilink")、[花蛤](../Page/花蛤.md "wikilink")、粉蛤、[竹蛤](../Page/竹蛤.md "wikilink")，相當於古代的蚶、車螯和花蛤。居住在海水中的，[臺灣閩南語稱蚶仔](../Page/臺灣閩南語.md "wikilink")（ham-á），主要是指[文蛤](../Page/文蛤.md "wikilink")。居住在淡水中的，[臺灣閩南語稱蜊仔](../Page/臺灣閩南語.md "wikilink")（lâ-á），或[蜆](../Page/蜆.md "wikilink")，通常是指[河蜆](../Page/河蜆.md "wikilink")。

雙殼類通常棲於浅海、淡水或河海交界的砂質或泥質的水底。有较高的食用价值。

## 形态

有一束[閉殼肌連於兩殼之間](../Page/閉殼肌.md "wikilink")，用以閉殼。有強大、肌肉質的足。多數蛤類棲於淺水水域，埋於水底泥沙中免受波浪之擾。蛤將水從進水管吸進，又從出水管排出，從而進行呼吸和攝食。體型大小差異極大，從0.1公釐到1.2公尺都有。

許多蛤類可食，包括[文蛤](../Page/文蛤.md "wikilink")、[花蛤](../Page/花蛤.md "wikilink")、[斧蛤](../Page/斧蛤.md "wikilink")、[圓蛤](../Page/圓蛤.md "wikilink")、[严蛤](../Page/严蛤.md "wikilink")、[女神蛤和](../Page/女神蛤.md "wikilink")[軟殼蛤](../Page/軟殼蛤.md "wikilink")。

## 參考來源

  - 書目

<!-- end list -->

  -
<!-- end list -->

  - 引用

## 相關條目

  - [-{zh-hans:蛤肉汤; zh-hant:周打蜆湯;
    zh-tw:蛤蜊巧達湯;}-](../Page/蛤蜊巧達湯.md "wikilink")

[蛤蜊](../Category/蛤蜊.md "wikilink")
[Category:食用貝類](../Category/食用貝類.md "wikilink")

1.  《康熙字典》：「【玉篇】蚌蛤也。【禮·月令】雀入大水爲蛤。【國語註】小曰蛤，大曰蜃。【前漢·地理志】果蓏蠃蛤，食物常足。【註】似蚌而圓。【大戴禮】蚌蛤龜珠，與月盈虧。　又魁蛤。【韻會】一名復累，老服翼所化。　又文蛤。【夢溪筆談】文蛤卽吳人所食花蛤也。　又靈蛤。【酉陽雜俎】仙藥有白水靈蛤。　又萬年蛤。【飛燕外傳】[眞臘夷獻萬年蛤](../Page/眞臘.md "wikilink")。　又山蛤。【本草】在山石中藏蟄，似蝦蟇而大，黃色，能吞氣飮風露。」
2.  《康熙字典》：「【類篇】蛤蜊，蟲名。海蚌也。【本草】生東南海中，白殻紫脣，大二三寸者，閩、浙以其肉充海錯。【南史·王融傳】不知許事，且食蛤蜊。」