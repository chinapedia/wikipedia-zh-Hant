**蛯原友里**（，，是出身於[日本](../Page/日本.md "wikilink")[宮崎縣](../Page/宮崎縣.md "wikilink")[宮崎市的](../Page/宮崎市.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")、[女演員](../Page/演員.md "wikilink")，以擔任[小學館出版的女性雜誌](../Page/小學館.md "wikilink")《》之專屬模特兒而知名。

## 簡介

蛯原友里在家中是孿生姊妹中的家姐，高中時代是籃球部隊長，在[九州產業大學藝術學部設計學系畢業](../Page/九州產業大學.md "wikilink")。她和[山田優](../Page/山田優.md "wikilink")、[押切萌一同被合稱是小學館出版的另一本時裝雜誌](../Page/押切萌.md "wikilink")《[CanCam](../Page/CanCam.md "wikilink")》的三大名模。

## 人物其它

  - 友里的特長是游泳，由於從小接受父親的嚴格指導，小學時代已經擁有了在地區大會的頂點立足的實力。幼年時期，通過父親在腹肌，背肌，手臂，下蹲和跑步等訓練課程的培養，其實友里與大家想象中的那弱不禁風的樣子大相徑庭，她緊繃的腹肌明顯地一塊塊分開，而且擁有運動型的倒三角完美體型。
  - 友里曾經是高中籃球隊的隊長，而且當時和英式橄欖球隊的前輩戀愛交往了一段時間，後由於對方畢業後雙方見面少而關係疏遠，導致最後分手。
  - 友里的暱稱是（Ebichan）。此外也常被朋友和歌迷叫作「」（Yurippe）、「」（Yurichan）、「」（Ebiyuri），押切萌和山田優叫她「」（Ebi）。
  - 友里的家庭成員有父母，妹妹和弟弟。其中妹妹和她是雙胞胎，而且已經結婚了，職業是護士。
  - 友里奮斗的目標是達到模特[SHIHO的知名度](../Page/矢野志保.md "wikilink")。
  - 友里的大拇指能比普通人向外彎曲，利用這個，她能夠用手指做出很漂亮的心型。
  - 友里美中不足的是稍微有些羅圈腿。
  - 友里大學時代的專業是空間設計，畢業論文是和同學共同發表老人院設施的設計。
  - 友里在大學時代，通過在福岡的一家百貨店打工（做的是發放打折優惠傳單的模特）為契機，立志做一名真正的職業模特，隨後參加了選拔賽，2002年4月去了東京。
  - 2009年5月，友里與[嘻哈團體](../Page/嘻哈.md "wikilink")[屎爛幫成員](../Page/屎爛幫.md "wikilink")[ILMARI的交往正式曝光](../Page/ILMARI.md "wikilink")；同年12月23日，正式向戶政機關登記[結婚](../Page/結婚.md "wikilink")，並預定於2010年6月中旬在[巴黎近郊的教堂舉行婚禮](../Page/巴黎.md "wikilink")。\[1\]

## 演出作品

### 電視劇

  - [變身特派員](../Page/特命系长只野仁.md "wikilink")（2003年、2005年、2007年、2009年) -
    山吹一惠
  - [上班族金太郎4](../Page/上班族金太郎4.md "wikilink")（2004年）- 相川雅美
  - [愛情慢舞](../Page/愛情慢舞.md "wikilink")（2005年）- 園田雪絵
  - [愛上恐龍妹](../Page/愛上恐龍妹.md "wikilink")（2006年）- 蛯原友美
  - [鬼嫁日記2](../Page/鬼嫁日記2.md "wikilink")（2007年）- 秋山ユリ

### 电影

  - [變身特派員](../Page/特命系长只野仁.md "wikilink") 最后的剧场版 (2008年12月公映
    [松竹配给](../Page/松竹.md "wikilink")) - 山吹一惠

### 广告

  - [资生堂](../Page/资生堂.md "wikilink") ANESSA、MAQUillAGE、Benefique系列
  - [麦当劳](../Page/麦当劳.md "wikilink")
  - [明治巧克力](../Page/明治製菓.md "wikilink") 果音篇
  - [富士相機](../Page/富士相機.md "wikilink") Finepx系列
  - [世嘉公司](../Page/世嘉公司.md "wikilink")
    [魔法氣泡](../Page/魔法氣泡.md "wikilink")
  - 福助 f\*ing BIZWALK
  - 三井製糖

### 雜誌

  - [CanCam](../Page/CanCam.md "wikilink")
  - [AneCan](../Page/AneCan.md "wikilink")
  - [Domani](../Page/Domani.md "wikilink")

## 注釋

## 參考文獻

## 外部連結

  - [EBIHARA YURI Official
    Website](https://web.archive.org/web/20120922020210/http://pearl-tokyo.jp/models/ebihara.html)
  - [AneCan TV](http://anecan.tv/index.html)
  - [Shiseido
    MAQillAGE](https://web.archive.org/web/20090804023204/http://www.shiseido.co.jp/mq/index.htm)

[Category:日本女性模特兒](../Category/日本女性模特兒.md "wikilink")
[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本女性電視藝人](../Category/日本女性電視藝人.md "wikilink")
[Category:宮崎縣出身人物](../Category/宮崎縣出身人物.md "wikilink")
[Category:CanCam模特兒](../Category/CanCam模特兒.md "wikilink")
[Category:九州產業大學校友](../Category/九州產業大學校友.md "wikilink")
[Category:雙胞胎人物](../Category/雙胞胎人物.md "wikilink")

1.  [](http://www.sponichi.co.jp/entertainment/news/2010/05/10/01.html)