**蛺蝶科**（學名：Nymphalidae）是蝴蝶鳳蝶總科中的一個科。全世界有約6000個已被描述的物種。此分類包含了許多著名的物種，例如[帝王斑蝶](../Page/帝王斑蝶.md "wikilink")、[小紅蛺蝶](../Page/小紅蛺蝶.md "wikilink")、[北美眼蛺蝶](../Page/眼蛺蝶屬.md "wikilink")、[銀紋紅袖蝶](../Page/銀紋紅袖蝶.md "wikilink")、[艾地堇蛺蝶](../Page/堇蛺蝶屬.md "wikilink")、[大藍閃蝶](../Page/大藍閃蝶.md "wikilink")\[1\]。

## 語源

蛺蝶科的學名是科級分類後綴「-idae」配上源自[新拉丁語的](../Page/新拉丁語.md "wikilink")「nymphalis」，源自[拉丁文](../Page/拉丁文.md "wikilink")「nympha」，源自[古希臘語](../Page/古希臘語.md "wikilink")「νύμφη」，解作新娘。

## 分佈

物種遍佈世界各地，各類陸上環境都有種類適合棲息。[熱帶地區的](../Page/熱帶.md "wikilink")[物種多樣性最高](../Page/生物多樣性.md "wikilink")，然而[溫帶和](../Page/溫帶.md "wikilink")[寒帶地區也有許多獨特的類群棲息](../Page/寒帶.md "wikilink")。

在[台灣記錄了約](../Page/臺灣蝴蝶列表.md "wikilink")130種；[香港](../Page/香港蝴蝶列表#蛺蝶科_Nymphalidae.md "wikilink")88種。

## 形態

### 成蟲

蛺蝶科物種體型差異極大，小者體型有如[灰蝶](../Page/灰蝶科.md "wikilink")，大者甚至可及小鳥。型態亦極為多，主要特徵為觸角腹面有3條縱稜（carinae），因此形成一對凹陷。此外大多數種類前足末端特化無爪，收縮在胸前不用於步行，僅以中及後足站立，只有喙蝶亞科的雌蝶前足還能用於行走。

翅膀翅室開放或封閉。前翅R脈五分支，有一條臀脈；後翅則有兩條臀脈。有些種類的雄蝶具有性標。成蝶的[兩性異型見於部分種類](../Page/兩性異型.md "wikilink")。

### 幼期

蛺蝶科物種的[卵](../Page/卵.md "wikilink")、[幼蟲至](../Page/幼蟲.md "wikilink")[蛹的形態多變](../Page/蛹.md "wikilink")，各亞科有自己獨特的形態或特徵。

卵形態變化多端，在蝶類中最爲多樣化，表面常有呈現幾何圖案的刻紋及種種突起。

幼蟲軀體通常呈圓筒形，頭頂往往長有突起，有些種類在體表長有棘刺，另外有不少類群在尾端具有一對突起。蛺蝶幼蟲主要以寄主植物之營養器官爲食，而且不乏專食老葉的種類。

蛹缺乏圍繞胸部的絲線，只在尾部以垂懸器附著絲墊，呈倒吊的姿態，即垂蛹或吊蛹。造型多變，許多種類生有變化多端的突起並搭配各種體色。常有很好的隱蔽、僞裝及警戒效果。

Stripedtiger egg sec.jpg|[虎斑蝶的卵](../Page/虎斑蝶.md "wikilink")
呈稍長的炮彈狀 Melanargia galathea
egg.jpg|[加勒白眼蝶的卵](../Page/加勒白眼蝶.md "wikilink")
較圓和光滑 Ariadnemerione egg sec.jpg|細紋波蛺蝶的卵
長有細長的棘刺 Caterpilar head (Bicyclus
anynana).jpg|[偏瞳蔽眼蝶的幼蟲](../Page/偏瞳蔽眼蝶.md "wikilink")
頭部有兩處凸起 Moduza procris - Commander caterpillar 01.jpg|穆蛺蝶的幼蟲
表面長有棘刺 Melanitis leda – Common Evening Brown pupa
04.jpg|[暮眼蝶的吊蛹](../Page/暮眼蝶.md "wikilink") Morpho peleides
qtl2.jpg|[黑框藍閃蝶的前足收起](../Page/黑框藍閃蝶.md "wikilink")
只用兩對腳站立 Androconia Bicyclus anynana San-Martin.jpg|雄性偏瞳蔽眼蝶
後翅上的性標

## 習性

### 成蟲

成蝶除訪花外，亦吸食腐果、死屍和排遣等。部分亞科的雄蝶有很明顯的領域行爲。雌蝶通常把卵產在寄主植物體上，但也有不少種類產在寄主植物附近的樹皮、落葉、石塊等雜物上，甚至有直接將卵粒抛落的種類。卵粒單產及成卵塊產下的種類均存在，而且不乏一次產卵數百粒成一群的情形。

Red Admiral (Vanessa Atalanta) (8094944856).jpg|在訪花的優紅蛺蝶
*Vanessa atalanta* Gaudy Baron Butterfly perching on a
fruit.jpg|在吸食果汁的紅斑翠蛺蝶
*Euthalia lubentina* A butterfly feeding on the tears of a turtle in
Ecuador (cropped to
butterfly).jpg|在吸[側頸龜眼淚的](../Page/亞馬遜側頸龜屬.md "wikilink")[珠袖蝶](../Page/珠袖蝶.md "wikilink")
*Dryas iulia* Grand mars
changeant.jpg||在吸食糞便汁液的[柳紫閃蛺蝶](../Page/柳紫閃蛺蝶.md "wikilink")
*Apatura ilia*

### 幼蟲

幼蟲食性非常多樣化，[單](../Page/單子葉植物.md "wikilink")、[雙子葉植物](../Page/雙子葉植物.md "wikilink")，還有[蕨類或](../Page/蕨.md "wikilink")[裸子植物的種類都是不同蛺蝶物種的](../Page/裸子植物.md "wikilink")[寄主](../Page/寄主.md "wikilink")。另外，有些類群以高毒性的植物，如[夾竹桃科](../Page/夾竹桃科.md "wikilink")、[茄科和](../Page/茄科.md "wikilink")[西番蓮科有毒植物也是一些蛺蝶科物種幼蟲寄主](../Page/西番蓮科.md "wikilink")。亦部分種類爲廣食性，可取食許多不同科的植物。幼蟲終齡的齡期也不限定是五齡。

## 種系發生

由於物種形態歧異度很高，蛺蝶科過去被分為許多個科，包括狹義的蛺蝶科、斑蝶科、綃蝶科（蜓斑蝶）、眼蝶科（蛇目蝶）、環蝶科（環紋蝶）、大翅環蝶科（梟蝶、貓頭鷹蝶）、閃蝶科（摩爾浮蝶）、袖蝶科（毒蝶）、喙蝶科（天狗蝶）和珍蝶科，甚至有學者成立蛺蝶總科以包括以上各科。然而，基於形態學和分子系統發生學的研究數據，以前狹義的蛺蝶科與其他科別全部為一個[單系群](../Page/單系群.md "wikilink")。即使中華地區的學者都沒有把這些蝶種的名稱後綴都改為-蛺蝶，現今的種系發生學研究都認為把以上蝶類合併成現今更廣義的蛺蝶科\[2\]\[3\]。其蛺蝶科有最接近的親緣關係是「灰蝶科+蜆蝶科」的[演化支](../Page/演化支.md "wikilink")\[4\]\[5\]。

**註：**
<small>**i**：斑蝶亞科包括了以往綃蝶的分類。
**ii**：釉蛺蝶亞科包括了以往袖蝶、珍蝶的分類。
**iii**：眼蝶亞科包括了以往閃蝶、環蝶和貓頭鷹蝶的分類。</small>

## 圖集

Libythea lepita open.JPG|[喙蝶亞科的麗喙蝶](../Page/喙蝶亞科.md "wikilink")
*Libythea lepita* Tree Nymph Butterfly - Idea leuconoe - 大胡麻斑(オオゴマダラ)
(8908739794).jpg|[斑蝶亞科的](../Page/斑蝶亞科.md "wikilink")[大帛斑蝶](../Page/大帛斑蝶.md "wikilink")
*Idea leoconoe* Greta Oto (Glasswing) Butterfly
(6917391571).jpg|斑蝶亞科的寬紋黑脈綃蝶
*Greta morgane oto* Calinaga buddha formosana
20130316.jpg|[絹蛺蝶亞科的](../Page/絹蛺蝶亞科.md "wikilink")[絹蛺蝶](../Page/絹蛺蝶.md "wikilink")
*Calinaga buddha* Charaxes solon at
MNP.jpg|[螯蛺蝶亞科的鎖龍螯蛺蝶](../Page/螯蛺蝶亞科.md "wikilink")
*Charaxes solon* Blushing Phantom (Cithaerias pireta pireta) - Colón
Island, Bocas del Toro, Panama - July
2012.jpg|[眼蝶亞科的紅暈綃眼蝶](../Page/眼蝶亞科.md "wikilink")
*Cithaerias pireta* Blue Morpho
(11786190916).jpg|眼蝶亞科的[黑框藍閃蝶](../Page/黑框藍閃蝶.md "wikilink")
*Morpho peleides* Papilio 002.JPG|眼蝶亞科的黃裳貓頭鷹環蝶
*Caligo memnon* Tanaecia pelea 051223
kopg.jpg|[線蛺蝶亞科的](../Page/線蛺蝶亞科.md "wikilink")[箭紋玳蛺蝶](../Page/箭紋玳蛺蝶.md "wikilink")
*Tanaecia pelea* Heliconius
sara.jpg|[釉蛺蝶亞科的拴袖蝶](../Page/釉蛺蝶亞科.md "wikilink")
*Heliconius sara* Dichorragia nesimachus formosanus back
20140621.jpg|[秀蛺蝶亞科的](../Page/秀蛺蝶亞科.md "wikilink")[電蛺蝶](../Page/電蛺蝶.md "wikilink")
*Dichorragia nesimachus* Doxocopa agathina
sup.jpg|[閃蛺蝶亞科的榮蛺蝶](../Page/閃蛺蝶亞科.md "wikilink")
*Doxocopa agathina* Eighty-eight Butterfly (Diaethria
anna).JPG||[苾蛺蝶亞科的安娜渦蛺蝶](../Page/苾蛺蝶亞科.md "wikilink")
（88蝶）*Diaethria anna* Marpesia zerynthia edit by Telemaque My
Son.jpg|[絲蛺蝶亞科的召龍鳳蛺蝶](../Page/絲蛺蝶亞科.md "wikilink")
*Marpesia zerynthia* Nymphalis
californica.jpg|[蛺蝶亞科的凱麗蛺蝶](../Page/蛺蝶亞科.md "wikilink")
*Nymphalis californica*

## 腳註

## 參考文獻

  - Heikkilä, M., Kaila, L., Mutanen, M., Peña, C., & Wahlberg, N.
    (2012, March). Cretaceous origin and repeated tertiary
    diversification of the redefined butterflies. In Proc. R. Soc. B
    (Vol. 279, No. 1731, pp. 1093–1099). The Royal Society.

  - Ackery, P. R., R. de Jong, and R. I. Vane-Wright. 1999. The
    butterflies: Hedyloidea, Hesperioidea, and Papilionoidea. Pages
    264-300 in: Lepidoptera: Moths and Butterflies. 1. Evolution,
    Systematics, and Biogeography. Handbook of Zoology Vol. IV, Part 35.
    N. P. Kristensen, ed. De Gruyter, Berlin and New York.

  - Brower, A. V. Z. 2000. Phylogenetic relationships among the
    Nymphalidae (Lepidoptera), inferred from partial sequences of the
    wingless gene. Proceedings of the Royal Society of London Series B
    Biological Sciences 267:1201-1211.

  - Freitas, A. V. L. and K. S. Brown. 2004. Phylogeny of the
    Nymphalidae (Lepidoptera). Systematic Biology 53 (3):363-383.

  - ; ;  2003: Towards a better understanding of the higher systematics
    of Nymphalidae (Lepidoptera: Papilionoidea). Molecular phylogenetics
    and evolution, **28**(3): 473-484.  [Full article
    (PDF)](https://web.archive.org/web/20071025220449/http://nymphalidae.utu.fi/Wahlbergetal2003b.pdf)

  - Wahlberg, N. and Wheat, C. W. 2008 Genomic outposts serve the
    phylogenomic pioneers: designing novel nuclear markers for genomic
    DNA extractions of Lepidoptera. Systematic Biology 57: 231-242.
    (doi: 10.1080/10635150802033006)

  -
  -
  -
## 外部連結

  - Wahlberg, Niklas and Andrew V. Z. Brower. 2009. [Nymphalidae
    Rafinesque 1815. Brush-footed
    Butterflies.](http://tolweb.org/Nymphalidae/12172/2009.09.15)
    Version 15 September 2009 (under construction). in [The Tree of Life
    Web Project](http://tolweb.org/)
  - [The Nymphalidae Systematics
    Group](http://www.nymphalidae.net/index.htm)
  - [Nymphalidae
    phylogeny](https://web.archive.org/web/20160518193732/http://www.nymphalidae.net/old_site/Phylogeny/Phylogeny.htm)
    - Nymphalidae.net
  - [*Nymphalidae*](http://ftp.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/papilionoidea/nymphalidae/)
    - ftp.funet.fi

{{-}}

[Category:鳳蝶總科](../Category/鳳蝶總科.md "wikilink")
[\*](../Category/蛺蝶科.md "wikilink")

1.  <http://www.nymphalidae.net/old_site/home.htm>
2.  张敏, 曹天文, 金科, 任竹梅, 郭亚平, 施婧, ... & 马恩波. (2008). 蛱蝶科亚科间的分歧时间估计. 科学通报,
    53(15), 1809-1814.
3.  Wahlberg, N., M. F. Braby, A. V. Z. Brower, R. de Jong, M.-M. Lee,
    S. Nylin, N. Pierce, F. A. Sperling, R. Vila, A. D. Warren, & E.
    Zakharov. 2005: Synergistic effects of combining morphological and
    molecular data in resolving the phylogeny of butterflies and
    skippers. Proceedings of the Royal Society of London Series B
    Biological Sciences 272:1577-1586.
    [<doi:10.1098/rspb.2005.3124>](http://dx.doi.org/10.1098/rspb.2005.3124)
4.  Heikkilä, M., Mutanen, M., Wahlberg, N., Sihvonen, P., & Kaila, L.
    (2015). Elusive ditrysian phylogeny: an account of combining
    systematized morphology with molecular data (Lepidoptera). BMC
    evolutionary biology, 15(1), 260.
5.  Brower, Andrew V. Z. 2008. Papilionoidea Latreille 1802. True
    Butterflies. Version 07 October 2008 (under construction).
    <http://tolweb.org/Papilionoidea/12027/2008.10.07> in The Tree of
    Life Web Project, <http://tolweb.org/>