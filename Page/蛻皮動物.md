**蛻皮動物總門**（）是一大類[原口動物](../Page/原口動物.md "wikilink")，包括[節肢動物門](../Page/節肢動物門.md "wikilink")、[線蟲動物門和幾個小](../Page/線蟲動物門.md "wikilink")[門](../Page/門_\(生物\).md "wikilink")。最初由Auinaldo等人於1997年定義，主要根據是18S
[核糖體RNA樹](../Page/核糖體RNA.md "wikilink")。而這個分類同時也被一系列[形態學證據所支持](../Page/形態學.md "wikilink")，因爲這個分類包括所有蛻掉[外骨骼的動物](../Page/外骨骼.md "wikilink")（見）。與蛻皮動物接近的定義也曾經被Perrier在1897年和Seurat在1920年僅依據形態作出。

## 形态特征

蛻皮動物最顯著的共同特徵是一個三層的[表皮](../Page/表皮.md "wikilink")（英文cuticle），由有機物組成，能夠隨著動物生長週期性蛻掉。蛻皮動物由此得名。蛻皮動物缺乏運動[纖毛](../Page/纖毛.md "wikilink")，產生變形蟲樣[精子](../Page/精子.md "wikilink")，其胚胎不像其他[原口動物一樣](../Page/原口動物.md "wikilink")[螺旋卵裂](../Page/螺旋卵裂.md "wikilink")。蛻皮動物的一些分支還有其它一些共同特徵，比如緩步動物和線蟲動物都有一個[三輻射對稱的](../Page/三輻射對稱.md "wikilink")[咽](../Page/咽.md "wikilink")(pharynx)。

## 内部分类

蛻皮動物包括如下[門](../Page/門_\(生物\).md "wikilink")：[節肢動物門](../Page/節肢動物門.md "wikilink")、[有爪動物門](../Page/有爪動物門.md "wikilink")、[緩步動物門](../Page/緩步動物門.md "wikilink")、[動吻動物門](../Page/動吻動物門.md "wikilink")、[鰓曳動物門](../Page/鰓曳動物門.md "wikilink")、[鎧甲動物門](../Page/鎧甲動物門.md "wikilink")、[線蟲動物門和](../Page/線蟲動物門.md "wikilink")[線形動物門](../Page/線形動物門.md "wikilink")。其它一些門類，比如[腹毛動物門](../Page/腹毛動物門.md "wikilink")，曾被認爲可能是其中的一員，但因其缺乏蛻皮動物的主要特徵，現在通常被劃出。[泛節肢動物的共同特徵是具有分節的身體](../Page/泛節肢動物.md "wikilink")，原來以爲由[環節動物演變而來](../Page/環節動物.md "wikilink")，而與其一起組成[分節動物](../Page/分節動物.md "wikilink")(Articulata)。然而它們並不具有太多的共同特徵，現在看來他們分別演化出了分節的特點。此外，從18S
核糖體RNA樹來看，後口動物中的[毛顎動物門可能與其餘後口動物不同而屬於蛻皮動物](../Page/毛顎動物門.md "wikilink")。蛻皮動物中不屬泛節肢動物的成員曾經被組成[環神經動物](../Page/環神經動物.md "wikilink")（Cycloneuralia），但通常被認爲是[並系群](../Page/並系群.md "wikilink")。

## 參見

  - [後口動物](../Page/後口動物.md "wikilink")
  - [冠輪動物](../Page/冠輪動物.md "wikilink")

## 參考資料

  - Aguinaldo, A. M. A., J. M. Turbeville, L. S. Linford, M. C. Rivera,
    J. R. Garey, R. A. Raff, & J. A. Lake, 1997. *Evidence for a clade
    of nematodes, arthropods and other moulting animals*. Nature 387:
    489-493.
  - Wagele, J. W., T. Erikson, P. Lockhart, & B. Misof, 1999. *The
    Ecdysozoa: Artifact or monophylum?* J. Zoo. Syst. Evol. Research 37:
    211-223.

## 外部連結

英文：

  - [UCMP-Ecdysozoa
    introduction](http://www.ucmp.berkeley.edu/phyla/ecdysozoa.html)
  - <https://web.archive.org/web/20030313115747/http://www.palaeos.com/Kingdoms/Animalia/Ecdysozoa.html>
  - <https://web.archive.org/web/20051226094057/http://nema.cap.ed.ac.uk/tardigrades/Tardigrades_and_Ecdysozoa.html>
  - <https://web.archive.org/web/20100510081324/http://chuma.cas.usf.edu/~garey/articulata.html>
  - <https://web.archive.org/web/20030313065540/http://chuma.cas.usf.edu/~garey/essential.html>
  - [Tree of Life web project -
    Bilateria](http://tolweb.org/tree?group=Bilateria&contgroup=Animals)

[Category:动物](../Category/动物.md "wikilink")
[Category:原口动物](../Category/原口动物.md "wikilink")
[\*](../Category/蛻皮動物.md "wikilink")