《**蜜蜂電影**》（****）是一部2007年的[美國](../Page/美國.md "wikilink")[動畫](../Page/動畫.md "wikilink")[喜劇](../Page/喜劇.md "wikilink")[電影](../Page/電影.md "wikilink")，由[夢工廠製片](../Page/夢工廠.md "wikilink")，[史提夫·希拿及](../Page/史提夫·希拿.md "wikilink")[西蒙·史密夫執導](../Page/西蒙·史密夫.md "wikilink")，三位主角則是由[謝利·宋飛](../Page/謝利·宋飛.md "wikilink")、[蕾妮·齐薇格和](../Page/蕾妮·齐薇格.md "wikilink")[馬修·波特歷配音](../Page/馬修·波特歷.md "wikilink")。

## 劇情

貝瑞是一隻住在[紐約市的蜜蜂](../Page/紐約市.md "wikilink")，剛剛從蜜蜂大學畢業，不慎墮入人間，幸好得一人類女子凡妮莎相救。二人頓成好友，可是蜜蜂世界的「蜂管法」條例列明蜜蜂不能跟人類說話，但貝瑞卻違法了。他在人類[超級市場發現了一大堆他們辛苦採摘的蜂蜜](../Page/超級市場.md "wikilink")，於是向蜜蜂世界通風報信，弄致蜜蜂和人類開庭。後來蜜蜂勝訴，人類不能再取用蜜蜂採集的蜂蜜，而蜜蜂開始懶惰，並不再勤勞的採蜜。由於沒有蜜蜂來授粉，全世界的花漸漸枯萎，最後貝瑞想起花車上僅存的花朵，並採集花粉，之後，各地的花朵才漸漸恢復了生機。

## 配音

| 角色                              | 配音                                                   |
| ------------------------------- | ---------------------------------------------------- |
| 英版                              | 台版                                                   |
| 貝瑞·B·班森（Barry B. Benson）        | [傑里·賽恩菲爾德](../Page/傑里·賽恩菲爾德.md "wikilink")           |
| 凡妮莎·布羅（Vanessa Bloome）          | [蕾妮·齐薇格](../Page/蕾妮·齐薇格.md "wikilink")               |
| 亞當·費里曼（Adam Flayman）            | [馬修·波特歷](../Page/馬修·波特歷.md "wikilink")               |
| 阿肯（Ken）                         | [派屈克·華博頓](../Page/派屈克·華博頓.md "wikilink")             |
| 萊頓·T·蒙哥馬利（Layton T. Montgomery） | [約翰·古德曼](../Page/約翰·古德曼.md "wikilink")               |
| 蚊哥（Mooseblood the Mosquito）     | [基斯·洛克](../Page/基斯·洛克.md "wikilink")                 |
| 珍奈特·B·班森（Janet B. Benson），貝瑞的母親 | [凱西·貝茲](../Page/凱西·貝茲.md "wikilink")                 |
| 馬丁·B·班森（Martin B. Benson），貝瑞的父親 | [巴瑞·李文森](../Page/巴瑞·李文森.md "wikilink")               |
| （Trudy）                         | [梅根·姆萊利](../Page/:en:Megan_Mullally.md "wikilink")   |
| 蜂蜜工廠導遊（Honex Tour Guide）        | [梅根·姆萊利](../Page/:en:Megan_Mullally.md "wikilink")   |
| （Lou Lo Duca）                   | [雷普·汤恩](../Page/雷普·汤恩.md "wikilink")                 |
| （Pollen Jocks General）          | [雷普·汤恩](../Page/雷普·汤恩.md "wikilink")                 |
| 波布頓法官（Judge Bumbleton）          | [奧花·雲費](../Page/奧花·雲費.md "wikilink")                 |
| （Bud Ditchwater）                | [邁克·里察斯](../Page/邁克·里察斯.md "wikilink")               |
| 賴瑞金蜜蜂（Bee Larry King）           | [拉里·金](../Page/拉里·金.md "wikilink")                   |
| 嗡嗡先生（Dean Buzzwell）             | [拉里·米勒](../Page/拉里·米勒.md "wikilink")                 |
| 旁白（Title Narrator）              | [吉姆·康明斯](../Page/:en:Jim_Cummings.md "wikilink")     |
| 畢業典禮廣播員（Graduation Announcer）   | [吉姆·康明斯](../Page/:en:Jim_Cummings.md "wikilink")     |
| 海克特（Hector）                     | 大衛·莫斯·皮們泰爾 海克特                                       |
| 安迪（Andy）                        | 查克·馬丁                                                |
| 珊蒂（Sandy Shrimpkin）             | 布萊恩·哈普金斯                                             |
| TSA代理人（TSA Agent）               | 布萊恩·哈普金斯                                             |
| （Bailiff）                       | [約翰·迪·馬吉歐](../Page/約翰·迪·馬吉歐.md "wikilink")           |
| （Janitor）                       | [約翰·迪·馬吉歐](../Page/約翰·迪·馬吉歐.md "wikilink")           |
| 鐘珍娜（Jeanette Chung）             | [翠絲·馬克尼爾](../Page/:en:Tress_MacNeille.md "wikilink") |
| 母親（Mother）                      | [翠絲·馬克尼爾](../Page/:en:Tress_MacNeille.md "wikilink") |
| 牛（Cow）                          | [翠絲·馬克尼爾](../Page/:en:Tress_MacNeille.md "wikilink") |
| 貨車司機（Truck Driver）              | [西蒙·J·史密斯](../Page/西蒙·J·史密斯.md "wikilink")           |
| 切特（Chet）                        | [西蒙·J·史密斯](../Page/西蒙·J·史密斯.md "wikilink")           |
| 雷·利奥塔（Ray Liotta）               | [雷·利奥塔](../Page/雷·利奥塔.md "wikilink")                 |
| 史汀（Sting）                       | [史汀](../Page/史汀.md "wikilink")                       |
| 蜜蜂（Bee）                         | [羅伯特·簡恩](../Page/羅伯特·簡恩.md "wikilink")               |
| 卡爾·卡斯（Carl Kasell）              | [卡爾·卡斯](../Page/:en:Carl_Kasell.md "wikilink")       |

其他配音員：

  - 台版：[林谷珍](../Page/林谷珍.md "wikilink")、[夏治世](../Page/夏治世.md "wikilink")、[佟紹宗](../Page/佟紹宗.md "wikilink")、[康殿宏](../Page/康殿宏.md "wikilink")、[孫誠](../Page/孫誠.md "wikilink")、[陳進益](../Page/陳進益.md "wikilink")、[王華怡](../Page/王華怡.md "wikilink")

香港版配音：
[林海峰](../Page/林海峰.md "wikilink")、[林曉峰](../Page/林曉峰.md "wikilink")、[蘇民峰](../Page/蘇民峰.md "wikilink")、[鄧梓峰](../Page/鄧梓峰.md "wikilink")、[周國豐](../Page/周國豐.md "wikilink")、[范振鋒](../Page/范振鋒.md "wikilink")，[胡楓](../Page/胡楓.md "wikilink")，[鄭融](../Page/鄭融.md "wikilink")
（大部分配音員名字都有「蜂」之同音字，為一大賣點）

## 反響

### 專業評價

[爛番茄新鮮度](../Page/爛番茄.md "wikilink")51%，基於169條評論，平均分為5.7/10，而在[Metacritic上得到](../Page/Metacritic.md "wikilink")54分，[IMDB上得](../Page/IMDB.md "wikilink")6.2分，屬於好壞參半的評價。

## 外部連結

  -
  -
  -
  -
  -
[Category:昆蟲電影](../Category/昆蟲電影.md "wikilink")
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:2000年代喜劇片](../Category/2000年代喜劇片.md "wikilink")
[Category:2000年代冒險片](../Category/2000年代冒險片.md "wikilink")
[Category:美國喜劇片](../Category/美國喜劇片.md "wikilink")
[Category:美國冒險片](../Category/美國冒險片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國家庭片](../Category/美國家庭片.md "wikilink")
[Category:動畫喜劇片](../Category/動畫喜劇片.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:美國動畫電影](../Category/美國動畫電影.md "wikilink")
[Category:昆虫主角故事](../Category/昆虫主角故事.md "wikilink")
[Category:紐約市背景電影](../Category/紐約市背景電影.md "wikilink")
[Category:夢工廠動畫公司動畫電影](../Category/夢工廠動畫公司動畫電影.md "wikilink")
[Category:夢工廠電影](../Category/夢工廠電影.md "wikilink")
[Category:電腦動畫電影](../Category/電腦動畫電影.md "wikilink")