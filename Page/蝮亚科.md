**蝮亚科**（学名*Crotalinae*）是[蝰蛇科的一个亚科](../Page/蝰蛇科.md "wikilink")，统称**蝮蛇**，其主要特征是在眼与鼻孔之间具颊窝。除少數為[卵生外](../Page/卵生.md "wikilink")，絕大部分品種爲[卵胎生](../Page/卵胎生.md "wikilink")。

## 其代表蛇岛蝮简介

详见：[蛇島蝮](../Page/蛇島蝮.md "wikilink")
其代表为蛇岛蝮，灰褐色，是唯一一种不处于濒危状态的毒蛇。分布于[蛇島](../Page/蛇島.md "wikilink")。

## 分佈

中低海拔樹林樹腐葉裡及近水邊潮溼地，常於樹根部及石塊邊盤成一團，人往往因休息坐於樹石被咬；在台灣稱為[百步蛇](../Page/百步蛇.md "wikilink")，中國稱為[七步蛇](../Page/七步蛇.md "wikilink")、[五步蛇](../Page/五步蛇.md "wikilink")。

## 食性

食量大，食性廣，以鳥為主食，兼食蛙、鼠、蜥蜴、和小型无毒蛇，如[游蛇科的成員和](../Page/游蛇科.md "wikilink")[裸鱗蛇](../Page/裸鱗蛇.md "wikilink")、[針尾蛇和](../Page/針尾蛇.md "wikilink")[盲蛇等](../Page/盲蛇.md "wikilink")。

## 性格

性格较温顺，攻击性比[眼镜蛇低](../Page/眼镜蛇.md "wikilink")，但比蝰亚科的成员攻击性高。毒牙较短，属于原始和进步之间的蝰蛇科成员。

## 分類

  - 屬：[蝮蛇屬](../Page/蝮蛇屬.md "wikilink")（Palisot de Beauvois，1799）
  - 屬：[跳蝮屬](../Page/跳蝮屬.md "wikilink")（Werman，1992）
  - 屬：[棕櫚蝮屬](../Page/棕櫚蝮屬.md "wikilink")（Peters，1859）
  - 屬：[森蝮屬](../Page/森蝮屬.md "wikilink")（Peters，1861）
  - 屬：[矛頭蝮屬](../Page/矛頭蝮屬.md "wikilink")（Wagler，1824）
  - 屬：[紅口蝮屬](../Page/紅口蝮屬.md "wikilink")（Cope，1860）
  - 屬：[山蝮屬](../Page/山蝮屬.md "wikilink")（Campbell & Lamar，1992）
  - 屬：[響尾蛇屬](../Page/響尾蛇屬.md "wikilink")（Linnaeus，1758）
  - 屬：[尖吻蝮屬](../Page/尖吻蝮屬.md "wikilink")（Gloyd，1979）
  - 屬：[亞洲蝮屬](../Page/亞洲蝮屬.md "wikilink")（Hoge & Romano-Hoge，1981）
  - 屬：[瘤鼻蝮屬](../Page/瘤鼻蝮屬.md "wikilink")（Fitzinger，1843）
  - 屬：[巨蝮屬](../Page/巨蝮屬.md "wikilink")（Daudin，1803）
  - 屬：[墨西哥角蝮屬](../Page/墨西哥角蝮屬.md "wikilink")（Cope，1887）
  - 屬：[烙鐵頭屬](../Page/烙鐵頭屬.md "wikilink")（Burger，1981）
  - 屬：[豬鼻蝮屬](../Page/豬鼻蝮屬.md "wikilink")（Cope，1871）
  - 屬：[侏儒響尾蛇屬](../Page/侏儒響尾蛇屬.md "wikilink")（Garman，1883）
  - 屬：[竹葉青屬](../Page/竹葉青屬.md "wikilink")（Lacépède，1804）
  - 屬：[黑綠烙鐵頭屬](../Page/黑綠烙鐵頭屬.md "wikilink")（ Wagler，1830 ）
  - 屬：[莽山烙鐵頭屬](../Page/莽山烙鐵頭屬.md "wikilink")（Zhaoermi，1990 ）

[Category:蝰蛇科](../Category/蝰蛇科.md "wikilink")
[\*](../Category/蝮亚科.md "wikilink")