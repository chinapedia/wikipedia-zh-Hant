**螢橋車站**是一曾存在於[台灣](../Page/台灣.md "wikilink")[台北市](../Page/臺北市.md "wikilink")[中正區的已廢](../Page/中正區_\(臺北市\).md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")，在廢站前曾為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[新店線](../Page/新店線_\(台鐵\).md "wikilink")（也已廢線）的沿線車站之一。
早期（[日治時代至](../Page/台灣日治時期.md "wikilink")[戰後初期](../Page/台灣戰後時期.md "wikilink")），螢橋火車站周邊人煙稀少。夏夜適合賞[螢火蟲](../Page/螢火蟲.md "wikilink")。但隨著都市開發，螢火蟲亦已消逝。

車站位置大約介於詔安街口至[螢橋國小前之間的](../Page/螢橋國小.md "wikilink")[汀州路上](../Page/汀州路_\(台北市\).md "wikilink")\[1\]。也正因為當初設校時汀州路還是一條鐵路這歷史因素，今日螢橋國小的正門是位於較為狹小的詔安街側上，而不是面對較為寬闊的汀州路。

## 歷史

  - 1921年（大正10年）1月22日設「螢橋驛」。
  - 戰後新店線由台鐵接手經營。
  - 1965年（民國54年）3月24日：隨著新店線停駛而廢止。

### 車站周邊

  - 川端橋（今[中正橋](../Page/中正橋.md "wikilink")）
  - [螢橋](../Page/螢橋.md "wikilink")
  - [川端渡船頭](../Page/川端渡船頭.md "wikilink")
  - [川端公園](../Page/川端公園.md "wikilink")（包含[川端競馬場](../Page/川端競馬場.md "wikilink")）
  - [螢橋國校](../Page/螢橋國校.md "wikilink")（今[螢橋國小](../Page/螢橋國小.md "wikilink")）

## 鄰近車站

## 参考文献

<div class="references-small">

<references />

</div>

[新](../Category/台灣鐵路廢站.md "wikilink")
[廢](../Category/中正區鐵路車站_\(臺北市\).md "wikilink")
[Category:1965年關閉的鐵路車站](../Category/1965年關閉的鐵路車站.md "wikilink")
[Category:1921年启用的铁路车站](../Category/1921年启用的铁路车站.md "wikilink")

1.  [消失的螢橋 | 三腳貓學狗叫](http://blog.hi3b.com/archives/1030)