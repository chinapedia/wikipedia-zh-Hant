**螺絲山公園**（，或稱**馬交石公園**），是位於[澳門](../Page/澳門.md "wikilink")[花地瑪堂區的](../Page/花地瑪堂區.md "wikilink")[公園](../Page/公園.md "wikilink")，公園的歷史價值已為澳門政府所評定。

螺絲山公園佔地約9500平方公尺，公園有螺旋形小徑引導遊人迴旋到達上方的人工眺望台。由於整座公園形似一顆巨型螺絲，人工眺望台同樣是螺絲形，所以稱為螺絲山公園。

## 地理位置

螺絲山公園（或稱**馬交石公園**），是位於[澳門](../Page/澳門.md "wikilink")[望廈山與](../Page/望廈山.md "wikilink")[馬交石山之間的一個小山崗上](../Page/馬交石山.md "wikilink")，山崗高約30公尺。公園有兩個入口，分別位於新雅馬路和亞馬喇馬路。

## 設施

  - 眺望台
  - [聖若望鮑思高塑像](../Page/聖若望鮑思高.md "wikilink")
  - 兒童電動[高卡車活動場](../Page/高卡車.md "wikilink")
  - 兒童遊樂場
  - 休憩區
  - 餐廳

<center>

{{ gallery | width = 230 | <File:San> Giovanni Bosco Statue, Garden of
Montanha Russa 20171008.jpg|聖若望鮑思高塑像 | <File:Karting> in Russa Mountain
Park.jpg|兒童高卡車場 | <File:Montanha> Russa.JPG|公園眺望台 | <File:Russa>
Mountain Park 02.jpg|兒童遊樂場 }}

</center>

## 參考資料

[Category:澳門公園](../Category/澳門公園.md "wikilink")
[Category:澳門康樂設施](../Category/澳門康樂設施.md "wikilink")