[HK_EmpireCourt.JPG](https://zh.wikipedia.org/wiki/File:HK_EmpireCourt.JPG "fig:HK_EmpireCourt.JPG")

**蟾宮大廈**（），是[香港一座單幢式](../Page/香港.md "wikilink")[商](../Page/商業.md "wikilink")[住](../Page/住宅.md "wikilink")[大廈](../Page/大廈.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[利園山](../Page/利園山.md "wikilink")[希慎道](../Page/希慎道.md "wikilink")2至4號。大廈於1958年落成，樓高17層，取代當時[中環的](../Page/中環.md "wikilink")[中國銀行大廈成為](../Page/中國銀行大廈.md "wikilink")[香港最高的](../Page/香港.md "wikilink")[建築物](../Page/建築物.md "wikilink")，直至1962年[中環的](../Page/中環.md "wikilink")[恆生大廈落成](../Page/恆生大廈.md "wikilink")。

## 歷史

蟾宮大廈由香港著名[商人](../Page/商人.md "wikilink")[霍英東興建](../Page/霍英東.md "wikilink")，以130萬港元從[利希慎家族購入](../Page/利希慎家族.md "wikilink")[地皮](../Page/地皮.md "wikilink")，並首創賣[樓花及分層分](../Page/樓花.md "wikilink")[單位發售的銷售手法](../Page/單位.md "wikilink")\[1\]，奠定[香港](../Page/香港.md "wikilink")[地產](../Page/地產.md "wikilink")[發展商其後賣樓花的基礎](../Page/發展商.md "wikilink")。

當樓花開售時，地下是舖位，二樓當寫字樓，其它各層是住宅，但坊間盛傳霍英東因從事走私生意，即將被香港政府遞解出境，令已買樓花的業主一度恐慌，霍英東的立信置業不得不在報紙上刊登「重要啟事」，指「近有無恥之徒散播讕言」。

據霍英東說：「蟾宮大廈，包括建築費在內，總投資約200萬港元，但其實我們只動用地價一成的資金10.3萬港元，其餘的費用全靠賣樓花，用買家的錢來交地價、蓋樓。樓價好像是每英呎80港元，我們賺了100多萬港元。」在短短一年多時間裡，霍英東就賺到1000萬港元以上。其中他動用的本錢不足100萬港元。

## 圖片

<File:HK> Causeway Bay Hysan Avenue 2 Empire Court.JPG|希慎道2號蟾宮大廈
<File:HK> Causeway Bay Ying Kee Tea
House.JPG|位於地鋪的[英記茶莊](../Page/英記茶莊.md "wikilink")

## 參考來源

<references />

## 參見

  - [香港摩天大樓](../Page/香港摩天大樓.md "wikilink")

{{-}}

[Category:灣仔區單幢式住宅](../Category/灣仔區單幢式住宅.md "wikilink")
[Category:綜合用途建築物](../Category/綜合用途建築物.md "wikilink")
[Category:香港摩天大樓](../Category/香港摩天大樓.md "wikilink")
[Category:銅鑼灣](../Category/銅鑼灣.md "wikilink")

1.