**蠅虎科**或**跳蛛科**（學名：），又名“**虩**”（讀音同“隙”），是[蜘蛛目的一科](../Page/蜘蛛目.md "wikilink")，也是**蝇虎总科**的唯一科。

## 分類

根據[ITIS引用的](../Page/ITIS.md "wikilink")2011年數據，本科之下有570個[屬](../Page/屬.md "wikilink")。部分動物學家嘗試將這些屬分類，但結果絕大多數分類仍被歸入[跳蛛亞科](../Page/跳蛛亞科.md "wikilink")。

下列部分本科的屬：

  - *[Carrhotus](../Page/Carrhotus.md "wikilink")*
  - *[Clynotis](../Page/Clynotis.md "wikilink")*
  - *[Diplocanthopoda](../Page/Diplocanthopoda.md "wikilink")*
  - [華蛛屬](../Page/華蛛屬.md "wikilink")
    *[Chinattus](../Page/Chinattus.md "wikilink")* 1 種
  - [胞蛛屬](../Page/胞蛛屬.md "wikilink")
    *[Cytaea](../Page/Cytaea.md "wikilink")* 1 種
  - [右蛛屬](../Page/右蛛屬.md "wikilink")
    *[Dexippus](../Page/Dexippus.md "wikilink")* 1 種
  - [侏斑蛛屬](../Page/侏斑蛛屬.md "wikilink")
    *[Euophrys](../Page/Euophrys.md "wikilink")*
  - [蟆蛛屬](../Page/蟆蛛屬.md "wikilink")
    *[Harmochirus](../Page/Harmochirus.md "wikilink")* 1 種
  - [遜蠅虎屬](../Page/遜蠅虎屬.md "wikilink")
    *[Hasarius](../Page/Hasarius.md "wikilink")* 1 種
  - [闪蛛属](../Page/闪蛛属.md "wikilink")
    *[Heliophanus](../Page/Heliophanus.md "wikilink")*
  - [蠅獅屬](../Page/蠅獅屬.md "wikilink")
    *[Marpissa](../Page/Marpissa.md "wikilink")* \[1\]
  - *[Mendoza](../Page/Mendoza.md "wikilink")* 1 種
  - *[Menemerus](../Page/Menemerus.md "wikilink")* 1 種
  - [蟻蛛屬](../Page/蟻蛛屬.md "wikilink")
    *[Myrmarachne](../Page/Myrmarachne.md "wikilink")* 7 種
  - [新跳蛛屬](../Page/新跳蛛屬.md "wikilink") *Neon* 2 種
  - [盤蛛屬](../Page/盤蛛屬.md "wikilink")
    *[Pancorius](../Page/Pancorius.md "wikilink")* 3 種
  - *[Pellenes](../Page/Pellenes.md "wikilink")*
  - [黑條蠅虎屬](../Page/黑條蠅虎屬.md "wikilink")
    *[Phintella](../Page/Phintella.md "wikilink")*：又名[金蟬蛛屬](../Page/金蟬蛛屬.md "wikilink")。
  - *[Phlegra](../Page/Phlegra.md "wikilink")*
  - *[Pseudeuophrys](../Page/Pseudeuophrys.md "wikilink")*
  - [斑蠅虎屬](../Page/斑蠅虎屬.md "wikilink")
    *[Plexippus](../Page/Plexippus.md "wikilink")* 2 種
  - [孔蛛屬](../Page/孔蛛屬.md "wikilink")
    *[Portia](../Page/Portia.md "wikilink")*
  - [兜蠅虎屬](../Page/兜蠅虎屬.md "wikilink")
    *[Ptocasius](../Page/Ptocasius.md "wikilink")* 1 種
  - *[Rhene](../Page/Rhene.md "wikilink")* 1 種
  - *[Satticus](../Page/Satticus.md "wikilink")*
  - *[Siler](../Page/Siler.md "wikilink")* 1 種
  - [褐蠅虎屬](../Page/褐蠅虎屬.md "wikilink")
    *[Sitticus](../Page/Sitticus.md "wikilink")*
  - [雀躍蛛屬](../Page/雀躍蛛屬.md "wikilink")
    *[Spartaeus](../Page/Spartaeus.md "wikilink")*
    ：又名[散蛛屬](../Page/散蛛屬.md "wikilink")
  - *[Synageles](../Page/Synageles.md "wikilink")*
  - [合蠅虎屬](../Page/合蠅虎屬.md "wikilink")
    *[Synagelides](../Page/Synagelides.md "wikilink")* 2 種
  - *[Tauala](../Page/Tauala.md "wikilink")* 1 種
  - [紐蛛屬](../Page/紐蛛屬.md "wikilink")
    *[Telamonia](../Page/Telamonia.md "wikilink")* 1 種
  - [方胸蛛屬](../Page/方胸蛛屬.md "wikilink")
    *[Thiania](../Page/Thiania.md "wikilink")* 1 種
  - *[Wanlessia](../Page/Wanlessia.md "wikilink")* 1 種
  - *[Yaginumaella](../Page/Yaginumaella.md "wikilink")* 1 種
  - *[Zebraplatys](../Page/Zebraplatys.md "wikilink")* 1 種

## 型態描述

中國廣東粵東地區常見為灰色，背面有黑色斑紋，全身絨毛。

跳蛛有8只单眼，体型较小，最小的只有約一毫米。因其捕獵物的方式，及主要以蒼蠅為食，俗稱**[蒼蠅老虎](../Page/蒼蠅.md "wikilink")**；又因視力发达，善於跳躍，亦稱**跳蛛**。潛近獵物時，先逐步靠近，再跳到獵物身上。

跳蛛一般不織網，但能吐絲，因從樹上等高處摔下危險，預備跳時先帖上一根絲，就像爬山者用岩釘及繩子防止墜落。

蠅虎蜘蛛頭胸部比起其他科是方形的。（見圖一。）頭胸部上的八個眼睛，其中兩個平常比別的都大。（見圖二。）

-----

<File:Salticidae> cephalothorax di.svg|圖一 <File:Salticidae> eyes
diag.svg|圖二 <File:Phidippus> species.jpg|Phidippus whitmani
<File:Salticus> scenicus with a fly I.jpg|*Salticus scenicus* 吃蒼蠅
<File:Phidippus_audax3.JPG>|*Phidippus audax*
<File:Phidippus_workmani_dorsal.jpg>|*Phidippus workmani*
<File:JumpingSpider.jpg>|*Paraphidippus aurantius* <File:Ant> Mimic
Spider.jpg|ant mimic spider

## 參考文獻

## 外部連結

  -
[Category:蜘蛛目](../Category/蜘蛛目.md "wikilink")
[跳蛛科](../Category/跳蛛科.md "wikilink")

1.