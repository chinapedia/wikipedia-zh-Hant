**血管紧张肽I转化酶抑制剂**（，简称为**ACEI**）是一类抗[高血压药](../Page/高血压.md "wikilink")。

## 作用机理

[血管紧张素转化酶](../Page/血管紧张素转化酶.md "wikilink")（ACE）是[肾素](../Page/肾素.md "wikilink")-[血管紧张素](../Page/血管紧张素.md "wikilink")-[醛固酮](../Page/醛固酮.md "wikilink")（RAA）系统中的一个重要环节，该系统对血压的调节有着及其重要的意义。ACE的主要作用是将血管紧张素I转化为具有强烈缩血管作用的[血管紧张素II](../Page/血管紧张素#血管紧张素II.md "wikilink")；此外ACE还能够催化有促血管舒张作用的[缓激肽水解](../Page/缓激肽.md "wikilink")。

而ACEI主要的药理作用是抑制ACE活性，减少血管紧张素II的生成，减少缓激肽的水解，导致血管舒张，血容量减少血压下降。此类药的作用与血浆肾素水平有密切关系，对血浆肾素活性高者效果更好。

## 不良反应

此类药物可能有[咳嗽](../Page/咳嗽.md "wikilink")、[味觉异常](../Page/味觉异常.md "wikilink")、[粒细胞减少](../Page/粒细胞.md "wikilink")、[皮疹](../Page/皮疹.md "wikilink")、[发热](../Page/发热.md "wikilink")、等[不良反应](../Page/不良反应.md "wikilink")。在临床应用过程中，常有患者抱怨莫名其妙的严重咳嗽和味觉消失，这主要是由于上述两种不良反应体感明显，且主观上给患者带来更大的不适感。

## 发展历史

ACEI是一类发展迅速的抗高血压药物，此类药物最早应用于临床的是从蛇毒中提取的九肽[替普罗肽](../Page/替普罗肽.md "wikilink")，但由于肽类化合物的性质，此类药物口服无效，通过对替普罗肽的[构效关系研究](../Page/构效关系.md "wikilink"),人们研制出了第一个ACEI类非肽药物[卡托普利](../Page/卡托普利.md "wikilink")，为了减少卡托普利产生的味觉消失等给患者造成较严重不适感的不良反应，人们研制出了无[巯基的ACEI类药物](../Page/巯基.md "wikilink")[依那普利](../Page/依那普利.md "wikilink")。近年来人们研制出了20余种ACEI类药物，目前临床应用较多的有[雷米普利等](../Page/雷米普利.md "wikilink")。

## 临床应用

主要用于高血压，近年也用于I型糖尿病肾病以及心衰竭的治疗。

## 常见的ACEI

  - 含巯基(-SH)或硫基(-SR)类
      - [卡托普利](../Page/卡托普利.md "wikilink")(Captopril)
      - [阿拉普利](../Page/阿拉普利.md "wikilink")(Alacepril)
  - 含羧基(-COOH)类
      - [依那普利](../Page/依那普利.md "wikilink")()
      - [赖诺普利](../Page/赖诺普利.md "wikilink")()
      - [培哚普利](../Page/培哚普利.md "wikilink")(Perindopril)
      - [雷米普利](../Page/雷米普利.md "wikilink")()
      - [喹那普利](../Page/喹那普利.md "wikilink")(Quinapril)
      - [地拉普利](../Page/地拉普利.md "wikilink")(Delapril)
      - [西拉普利](../Page/西拉普利.md "wikilink")(Cilazapril)
      - [贝那普利](../Page/贝那普利.md "wikilink")()
      - [螺普利](../Page/螺普利.md "wikilink")(Spirapril)
      - [群多普利](../Page/群多普利.md "wikilink")(Trandolapril)
      - [莫昔普利](../Page/莫昔普利.md "wikilink")(Moexipril)
      - [咪达普利](../Page/咪达普利.md "wikilink")(Imidapril)
  - 含次膦酸基(-POO-)类
      - [福辛普利](../Page/福辛普利.md "wikilink")()

## 相关条目

  - [血管紧张素](../Page/血管紧张素.md "wikilink")
  - [肾素-血管紧张素系统](../Page/肾素-血管紧张素系统.md "wikilink")
  - [血管紧张素II受体拮抗剂](../Page/血管紧张素II受体拮抗剂.md "wikilink")

## 外部連結

  - [ACE Inhibitors: Summary of Recommendations - Consumer Reports Best
    Buy Drugs - free public education
    project](http://www.consumerreports.org/health/best-buy-drugs/ace-inhibitors.htm)

[血管紧张素转化酶抑制剂](../Category/血管紧张素转化酶抑制剂.md "wikilink")