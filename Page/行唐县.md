**行唐县**是[中国](../Page/中国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[石家庄市所辖的一个](../Page/石家庄市.md "wikilink")[县](../Page/县.md "wikilink")。

## 交通

  - [行唐站](../Page/行唐站.md "wikilink")（[朔黄线](../Page/朔黄线.md "wikilink")）

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、11个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 外部链接

  - [行唐人民政府](http://www.xingtang.gov.cn/)
  - [行唐县简介](https://web.archive.org/web/20060110104506/http://www.hebiic.gov.cn/173county/sjz/sjzxtx/index.asp)

[行唐县](../Category/行唐县.md "wikilink")
[县](../Category/石家庄县级行政区.md "wikilink")
[石家庄](../Category/河北省县份.md "wikilink")