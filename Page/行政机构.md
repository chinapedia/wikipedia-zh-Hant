**行政机构**也称**执法机构**、**行政機關**
，其工作是对组织進行日常的管理并施行法律政策等相关活动，是[三權分立中的其中一部份](../Page/三權分立.md "wikilink")。广义上，**行政**被定义为负责国家政策的执行的政府机构。狭义上，则专指[内阁等行政决策机构](../Page/内阁.md "wikilink")，而只从事事务性执行工作的人员和机构则归为官僚机构\[1\]。但是这种区分也存在争议，例如[法国大革命前以及欧洲的许多](../Page/法国大革命.md "wikilink")[君主立宪政体](../Page/君主立宪政体.md "wikilink")，占据行政机构顶端的部长职位的也是行政管理者\[2\]。

## 不同制度下的行政机构

在[原始社會時期](../Page/原始社會.md "wikilink")，行政權通常掌握在部落[酋長手裡](../Page/酋長.md "wikilink")。

从有[国家出现以后](../Page/国家.md "wikilink")，行政权都是掌握在[政府手里](../Page/政府.md "wikilink")，政府是国家机器中最早出现的机构。

在[中世纪](../Page/中世纪.md "wikilink")，政府的首長一般都是國教的權威。[基督教和](../Page/基督教.md "wikilink")[伊斯蘭教國家政府的首長都是](../Page/伊斯蘭教.md "wikilink")[大主教](../Page/大主教.md "wikilink")；[佛教和](../Page/佛教.md "wikilink")[印度教國家宗教領袖通常只控制](../Page/印度教.md "wikilink")[立法權](../Page/立法.md "wikilink")，政府首長由[軍隊領袖擔任](../Page/軍隊.md "wikilink")；[儒家文化區域的](../Page/儒家.md "wikilink")[國家](../Page/國家.md "wikilink")，[君主掌握](../Page/君主.md "wikilink")[立法權](../Page/立法.md "wikilink")，政府首長由大儒擔任。

現代國家體制不同，政府的體制也不同，但職責基本相同。在[三權分立的國家](../Page/三權分立.md "wikilink")，[行政權和](../Page/行政.md "wikilink")[立法權](../Page/立法.md "wikilink")、[司法權各自獨立行使](../Page/司法.md "wikilink")，互相制衡。

在[總統制的國家中](../Page/總統制.md "wikilink")，如：[美國](../Page/美國.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[韓國](../Page/韓國.md "wikilink")，政府首長就是[國家元首](../Page/國家元首.md "wikilink")（[總統](../Page/總統.md "wikilink")），單獨選舉。其他政府成員由[總統任命](../Page/總統.md "wikilink")。[總統是唯一對國家所有行政事務負責的行政官員](../Page/總統.md "wikilink")，各部的[部長負責各自的部門](../Page/部長.md "wikilink")，并對總統負責。

在[半總統制下](../Page/半總統制.md "wikilink")，總統領導的政府向[議會負責](../Page/議會.md "wikilink")，總統任命政府總理，其他政府成員由總理提名，並由總統任命。例如：[法國](../Page/法國.md "wikilink")、[俄羅斯和](../Page/俄羅斯.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")。

在[議會制及部分](../Page/議會制.md "wikilink")[君主立憲制的國家中](../Page/君主立憲制.md "wikilink")，國家元首是禮儀性的（[虚位元首](../Page/虚位元首.md "wikilink")），如：[德國](../Page/德國.md "wikilink")、[意大利的總統](../Page/意大利.md "wikilink")，[加拿大和](../Page/加拿大.md "wikilink")[澳大利亞的](../Page/澳大利亞.md "wikilink")[總督](../Page/總督.md "wikilink")，[日本的](../Page/日本.md "wikilink")[天皇](../Page/日本天皇.md "wikilink")，[英國的](../Page/英國.md "wikilink")[女王](../Page/英國君主.md "wikilink")，[马来西亚的](../Page/马来西亚.md "wikilink")[最高元首](../Page/最高元首.md "wikilink")。政府首長是[總理或稱](../Page/總理.md "wikilink")[首相](../Page/首相.md "wikilink")，一般由[議會選舉中獲勝的最大黨黨魁擔任](../Page/議會.md "wikilink")，或经由[議员選舉產生](../Page/議员.md "wikilink")，首相不一定是最大黨黨魁，也有由國家元首直接任命。首長領導[內閣決定並執行國家政策](../Page/內閣.md "wikilink")，集體對[議會負責](../Page/議會.md "wikilink")。

在[社會主義國家中](../Page/社會主義國家.md "wikilink")，行政機構為國家權力機構的執行單位，不過實際操作中執行的是國內唯一[執政黨的決策](../Page/執政黨.md "wikilink")，例如：[中華人民共和國](../Page/中華人民共和國.md "wikilink")、[朝鮮民主主義人民共和國](../Page/朝鮮民主主義人民共和國.md "wikilink")、[越南等](../Page/越南.md "wikilink")。

在[委員會制國家中](../Page/委員會制.md "wikilink")，最高行政機構的成員由[議會選舉產生](../Page/議會.md "wikilink")。大部分的重要決策由委員們共同作出，政府首長只不過是一個行動委員會的主席，例如：[瑞士聯邦委員會](../Page/瑞士聯邦委員會.md "wikilink")。

## 结构

任何国家的行政结构都呈金字塔型，层级关系严格，上级领导下级，下级必须严格执行上级的命令。这种结构有效率，理性，但是也会因为机构庞杂而造成机械僵化的问题\[3\]。

一个国家的行政机构大体上可以分为\[4\]：

1.  领导机构：即各行政部门的首脑和核心，主要职责是对辖下的重大行政问题进行决策，并指挥与监督决策的实施，也对辖下的工作进行协调。
2.  办公机构：领导机构的辅助办事机构，主要职责包括日常办公室事务，协调工作，辅助决策以及领导机构临时交办的工作或特别事务，例如各种[办公室](../Page/办公室.md "wikilink")
3.  咨询机构：也就是[智囊团或参谋机构](../Page/智囊团.md "wikilink")，通过调查研究、政策协调等方式，对领导机构的决策进行辅助。
4.  信息机构：负责信息情报的收集整理，为领导机构提供信息服务，常见的部门有统计局、情报部门、档案室、资料室等
5.  职能机构：在领导机构领导下，负责组织一个或多个专门的行政事务，例如一个[部下的各个](../Page/部.md "wikilink")[局](../Page/局.md "wikilink")。
6.  派出机构：是某一级政府根据辖区授权委托，代表政府的分支机构。例如中国的[行政公署](../Page/行政公署.md "wikilink")、[区公所](../Page/区公所.md "wikilink")、[街道办事处](../Page/街道办事处.md "wikilink")。主要职责包括监督检查下级行政机构执行上级行政机构的命令、决策，并向委派机构报告辖区内的情况等。
7.  监督机构：各种行政监察机构，审计机构，负责对各行政机构及其人员进行执法性监督和检查。

## 职能

行政机构的功能就是执行国家意志。包括宪法和法律的执行，并参与国家立法；对国家内外政策进行决策和实施，任免政府官员；管理国家公共事务；向立法机构提出预算，干预和调整社会经济\[5\]。

一般而言，政府行政部門和普通公民的接觸和掌握的資源都最多、對公民的生活影響也最大，因此[憲政和分權的主要目的就是要限制和制衡行政部門的權力](../Page/憲政.md "wikilink")。但是在实际层面上，行政机构往往具有一定的自由裁量权，而不仅仅是执行。取决于政治制度的不同，有些国家的行政机构拥有难以监控的强大权力，甚至在一些国家，行政机构几乎就是国家的决策中心。在不少国家，行政机构已经成为了事实上的主要立法者。许多议案都是由行政机构起草，然后交由议会通过。在经济社会领域，行政机构可以单独发布和正式立法相同作用的各种规则，以影响公民的社会生活\[6\]。

虽然行政机构的权力在不断扩大，但是对其制约的机制也在不断完善。立法机构通过专业化和委员会方式，司法机构通过监督方式都在规制行政机构的权力\[7\]。技术的进步，互联网的发展在使公民能够更多的影响社会的同时，也在制约着行政力量\[8\]。

## 参考文献

## 參見

  - [三權分立](../Page/三權分立.md "wikilink")
      - [行政](../Page/行政.md "wikilink")：[行政機構](../Page/行政機構.md "wikilink")
      - [立法](../Page/立法.md "wikilink")：[立法機構](../Page/立法機構.md "wikilink")
      - [司法](../Page/司法.md "wikilink")：[司法機構](../Page/司法機構.md "wikilink")
  - [五權分立](../Page/五權分立.md "wikilink")
      - [行政](../Page/行政.md "wikilink")：[行政機構](../Page/行政機構.md "wikilink")
      - [立法](../Page/立法.md "wikilink")：[立法機構](../Page/立法機構.md "wikilink")
      - [司法](../Page/司法.md "wikilink")：[司法機構](../Page/司法機構.md "wikilink")
      - [考試](../Page/考試.md "wikilink")：[考試機構](../Page/考試院.md "wikilink")
      - [監察](../Page/監察.md "wikilink")：[監察機構](../Page/監察院.md "wikilink")
  - 与行政现象相关的学问包含[行政学](../Page/行政学.md "wikilink")、[行政法学](../Page/行政法学.md "wikilink")、[政治学](../Page/政治学.md "wikilink")、[经营学等](../Page/经营学.md "wikilink")。

{{-}}

[Category:政府机构类型](../Category/政府机构类型.md "wikilink")
[行政机构](../Category/行政机构.md "wikilink")

1.

2.

3.
4.
5.
6.
7.
8.