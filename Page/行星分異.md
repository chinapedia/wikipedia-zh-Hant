**行星分異**是[行星科學中](../Page/行星科學.md "wikilink")，行星[密度較高的成分向中心下沉](../Page/密度.md "wikilink")，較輕的物質上升至表面，使中心密度愈行增高的過程。這樣的過程傾向於創造[核心](../Page/行星核心.md "wikilink")、[地殼和](../Page/地殼.md "wikilink")[地函](../Page/地函.md "wikilink")。

## 熱化

當[太陽在](../Page/太陽.md "wikilink")[太陽星雲被點燃時](../Page/太陽星雲.md "wikilink")，臨近太陽附近的[氫](../Page/氫.md "wikilink")、[氦和其他易揮發物質都成為蒸汽](../Page/氦.md "wikilink")。[太陽風和](../Page/太陽風.md "wikilink")[光壓將這些密度較低的物质推離太陽](../Page/輻射壓.md "wikilink")，岩石和沉陷其中的物质推積成為[微行星](../Page/微行星.md "wikilink")。

早期的微行星有較多的[放射性元素](../Page/放射性.md "wikilink")，由於[輻射衰減使得含量逐漸減少](../Page/放射性.md "wikilink")。由輻射、撞擊和重力壓產生的熱迫使微行星的部分熔化並且逐漸成長成為[行星](../Page/行星.md "wikilink")。在熔化的區域中，較重的元素向中心下沉，較輕的元素則升至表面。有些[隕石的構造顯示在一些小行星也發生了分異](../Page/隕石.md "wikilink")。

當微行星[增加更多的物質](../Page/吸積.md "wikilink")，撞擊的能量會造成局部的加熱。除了這種臨時的熱化之外，當質量足夠大時，重力在新形成的團塊內所產生的溫度和壓力將足以熔化某些物質，也允許某些[化學反應和不同](../Page/化學反應.md "wikilink")[密度物質的分離或混合](../Page/密度.md "wikilink")，以及軟性物質在表面的伸展。

在[地球](../Page/地球.md "wikilink")，大片熔化的[鐵顯然會比](../Page/鐵.md "wikilink")[大陸地殼物質的密度更高](../Page/大陸地殼.md "wikilink")，因此會迫使鐵沉降至[地函](../Page/地函.md "wikilink")。在外太陽系，相似的效應也會發生，但那兒的材料只有[碳氫化合物](../Page/碳氫化合物.md "wikilink")，例如[甲烷](../Page/甲烷.md "wikilink")、[水冰或凍結的](../Page/水.md "wikilink")[二氧化碳](../Page/二氧化碳.md "wikilink")。

## 化學分異

值得注意的是有些物質的分異經由化學關係的效果遠高於密度，與其它的物質結合在一起進行分異的也有，在地殼中的[鈾就是個例子](../Page/鈾.md "wikilink")。

## 物理分異

### 重力分離

高密度的物質有通過輕的物質下沉的傾向。這種傾向受到物質結構相對強度的影響，在塑膠或溶解的材料中，會隨溫度的上昇而減少。鐵特別傾向於聚集在星球的中心，伴隨著下沉的還有許多其他（即能與鐵熔合形成[合金的元素](../Page/合金.md "wikilink")）。然而，不是所有的重元素都會這樣轉換，一些重元素會和密度較低的元素結合，這樣形成的材料會變得較輕而能避免實質上的分異。

較輕的物質則會設法通過高密度的物質上浮。在地球，[鹽穹是穿越周遭的岩石上升的鹽聚集在地殼外形成的](../Page/鹽穹.md "wikilink")。也存在著其他物質形成的[刺穿褶皺](../Page/刺穿褶皺.md "wikilink")，有時也會在表面形成[泥火山](../Page/泥火山.md "wikilink")。

### 月球的鉀稀土元素鏻物質

在月球，一種形成在表面的物質被認為是不相容的物質被熔解後，在冷卻的過程中形成的。這種物質含有高量的[鉀](../Page/鉀.md "wikilink")、[稀土元素和](../Page/稀土金屬.md "wikilink")[磷](../Page/磷.md "wikilink")，被提到時經常以縮寫[KREEP稱之](../Page/KREEP.md "wikilink")；它也有高量的[鈾和](../Page/鈾.md "wikilink")[釷](../Page/釷.md "wikilink")。

### 分餾的結晶

在地球，當[岩漿上升到某些高度時](../Page/岩漿.md "wikilink")，被熔化在其中的材料也許會因為某些溫度和壓力而形成結晶。重新固化的物質會在熔岩中除去了某幾種的元素，當熔岩重回到[地函後](../Page/地函.md "wikilink")，便會欠缺這幾種元素。

### 熱擴散

當物質的加熱不均勻時，就會顯現[索瑞特效應](../Page/熱泳.md "wikilink")。越輕的材料存在於溫度越高的區域，而越重的材料處在溫度越低的區域。

### 由碰撞造成的差別

[地球的](../Page/地球.md "wikilink")[衛星](../Page/衛星.md "wikilink") －
[月球](../Page/月球.md "wikilink")，似乎是由一個巨大的天體在早期撞擊地球後飛濺至軌道上的物質形成的。地球的分異很可能已經將較輕的物質分離至表面，所以從行星上飛濺出的都是較輕的物質。月球的密度比地球小了許多，地球的[地殼可能比他過去擁有的更薄](../Page/地殼.md "wikilink")，同時[板塊構造造成的作用也不同於同是行星的](../Page/板塊構造.md "wikilink")[金星和](../Page/金星.md "wikilink")[火星](../Page/火星.md "wikilink")。

## 地球上的密度差異

在[地球](../Page/地球.md "wikilink")，這樣的過程創造的表面密度是3,000公斤/米<sup>3</sup>，而整體的平均密度是5,515公斤/米<sup>3</sup>。

[Category:行星科學](../Category/行星科學.md "wikilink")