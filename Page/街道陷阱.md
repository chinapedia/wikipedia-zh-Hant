**街道陷阱**就是在[地图上的一条虚构](../Page/地图.md "wikilink")[街道](../Page/街道.md "wikilink")，通常位于地图覆盖面以外[区域](../Page/区域.md "wikilink")。以「诱捕」潜在的地图盗版者为目的，这样就不能证明他们地图上含有的地图陷阱是正当的了。\[1\]

这种街道陷阱可以很容易识别[侵权者而不大会妨碍](../Page/侵权.md "wikilink")[导航器](../Page/导航器.md "wikilink")。例如，一张地图也许增加了不存在的街道[转弯](../Page/转弯.md "wikilink")，或者不改变[地点或和其他街道的连接而把主要的街道描绘成窄巷](../Page/地点.md "wikilink")。

[出版社对于街道陷阱](../Page/出版社.md "wikilink")，多半是[例行公事地](../Page/例行公事.md "wikilink")[否认](../Page/否认.md "wikilink")。承認街道陷阱的[情況並不多](../Page/情況.md "wikilink")，例如一个知名的雅典驾驶员地图册便以此作为警告，希望潜在的盗版者注意图册中存在此类街道陷阱。\[2\]

## 参见

  - [水印街](../Page/水印街.md "wikilink")：一部涉及“鬼街”（即本文所述“街道陷阱”）的电影。\[3\]

## 参考文献

## 外部链接

  - [Copyright Easter
    Eggs](../Page/openstreetmap:Copyright_Easter_Eggs.md "wikilink") -
    [OpenStreetMap](../Page/OpenStreetMap.md "wikilink")

[T](../Category/骗局.md "wikilink") [T](../Category/地图学.md "wikilink")
[T](../Category/街道.md "wikilink") [T](../Category/知识产权.md "wikilink")
[T](../Category/虛構地方.md "wikilink")

1.  Cecil Adams. [*Do maps have 'copyright traps' to permit detection of
    unauthorized
    copies?*](http://www.straightdope.com/columns/read/1058/do-maps-have-copyright-traps-to-permit-detection-of-unauthorized-copies)
    *The Straight Dope*, August 16, 1991.
2.  "" Greek-language map book published by Nik. & Ioan Fotis O.E.
    (Νικ. & Ιωάν. Φωτής Ο.Ε. , <http://www.fotismaps.gr> ),
    Greek-language warning inside front cover
3.