**衛氏肺吸蟲**（[学名](../Page/学名.md "wikilink")：，又譯**卫氏并殖吸虫**）为[斜睪目](../Page/斜睪目.md "wikilink")[住胞科](../Page/住胞科.md "wikilink")[並殖屬的动物](../Page/並殖屬.md "wikilink")。是一種扁平似似[咖啡豆的](../Page/咖啡豆.md "wikilink")[寄生蟲](../Page/寄生蟲.md "wikilink")，是肺吸蟲症[致病原中最重要者](../Page/致病原.md "wikilink")，必須透過數種中間[宿主才能感染人類或其他物種](../Page/宿主.md "wikilink")，並完成[生活史](../Page/生活史.md "wikilink")，呈世界性分布，但[東亞](../Page/東亞.md "wikilink")、[東南亞為主要感染盛行區域](../Page/東南亞.md "wikilink")。

## 形態

[Paragonimus_westermanii.jpg](https://zh.wikipedia.org/wiki/File:Paragonimus_westermanii.jpg "fig:Paragonimus_westermanii.jpg")
[Paragonimus_westermani_01.jpg](https://zh.wikipedia.org/wiki/File:Paragonimus_westermani_01.jpg "fig:Paragonimus_westermani_01.jpg")
成蟲雌雄同體，大小約1[公分左右](../Page/公分.md "wikilink")，體型卵圓、色澤棕紅，與咖啡豆極為相似，兩個狀似[銀杏葉的](../Page/銀杏.md "wikilink")[睪丸並列於蟲體後三分之一處](../Page/睪丸.md "wikilink")，因此分類於並殖科，口吸盤與腹吸盤約略等大，分別位於蟲體前端和腹部中央。肺吸蟲的[卵呈黃褐色](../Page/蟲卵.md "wikilink")，長度介於68至118[微米](../Page/微米.md "wikilink")、寬約39至67微米之間，具有厚壁，且因一端較平使得外觀不對稱，較大的一端則有清楚可見的卵蓋。

## 生活史

當肺吸蟲的卵碰觸乾淨的水源，[纖毛幼蟲](../Page/纖毛幼蟲.md "wikilink")（miracidium）便可進入作為第一中間宿主的[川蜷](../Page/川蜷.md "wikilink")、[瘤蜷體內](../Page/瘤蜷.md "wikilink")，並先後發育成[胞狀幼蟲](../Page/胞狀幼蟲.md "wikilink")（sporocyst）、[雷氏幼蟲](../Page/雷氏幼蟲.md "wikilink")（redia），待發育成[尾動幼蟲](../Page/尾動幼蟲.md "wikilink")（cercaria）後便可以感染[淡水](../Page/淡水.md "wikilink")[甲殼綱動物等第二中間宿主](../Page/甲殼綱.md "wikilink")，包括淡水[蟹](../Page/蟹.md "wikilink")（如[澤蟹](../Page/澤蟹.md "wikilink")、[屎蟹](../Page/屎蟹.md "wikilink")、[宮崎蟹](../Page/宮崎蟹.md "wikilink")、[毛蟹等](../Page/毛蟹.md "wikilink")）和淡水[蝦](../Page/蝦.md "wikilink")（如[螯蝦](../Page/螯蝦.md "wikilink")、[淡水長臂大蝦](../Page/淡水長臂大蝦.md "wikilink")），並於這類宿主的[鰓或](../Page/鰓.md "wikilink")[肌肉組織形成](../Page/肌肉.md "wikilink")[囊腫](../Page/囊腫.md "wikilink")（cyst），再發育成[囊狀幼蟲](../Page/囊狀幼蟲.md "wikilink")（metacercaria）。生食這些受感染的第二中間宿主，可讓這些幼蟲有機會侵犯[腸胃道](../Page/腸胃道.md "wikilink")，並循[腹腔穿過](../Page/腹腔.md "wikilink")[橫膈並發育成蟲](../Page/橫膈.md "wikilink")，可在人或其他動物體內造成[肺部](../Page/肺.md "wikilink")、腹腔、甚至可因蟲體移行至各處，造成其他[器官的損害](../Page/器官.md "wikilink")。
[衛氏肺吸蟲生活史.gif](https://zh.wikipedia.org/wiki/File:衛氏肺吸蟲生活史.gif "fig:衛氏肺吸蟲生活史.gif")

## 盛行區

衛氏肺吸蟲分布于[日本](../Page/日本.md "wikilink")、[朝鮮](../Page/朝鮮.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[印度](../Page/印度.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、[印度尼西亞](../Page/印度尼西亞.md "wikilink")、[中華民國以及](../Page/中華民國.md "wikilink")[中華人民共和國的](../Page/中華人民共和國.md "wikilink")[遼寧](../Page/遼寧.md "wikilink")、[吉林](../Page/吉林.md "wikilink")、[黑龙江](../Page/黑龙江.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[江西](../Page/江西.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[山東](../Page/山東.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[河南等地](../Page/河南.md "wikilink")，營[寄生生活](../Page/寄生.md "wikilink")，终末宿主[虎](../Page/虎.md "wikilink")、[豹](../Page/豹.md "wikilink")、[狼](../Page/狼.md "wikilink")、[貂](../Page/貂.md "wikilink")、[猴](../Page/猴.md "wikilink")、[野猫](../Page/野猫.md "wikilink")、[伶鼬](../Page/伶鼬.md "wikilink")、[猞猁](../Page/猞猁.md "wikilink")、[家猫](../Page/家猫.md "wikilink")、[麝猫](../Page/麝猫.md "wikilink")、[犬](../Page/犬.md "wikilink")、[猪](../Page/猪.md "wikilink")、[牛及](../Page/牛.md "wikilink")[人等](../Page/人.md "wikilink")。\[1\]

衛氏肺吸蟲為世界性分布，是可寄生人體的各種肺吸蟲中，研究最多的一種，其中又以日本、韓國、中華人民共和國、東南亞及[新幾內亞為主要盛行區](../Page/新幾內亞.md "wikilink")，[印度](../Page/印度.md "wikilink")、[非洲則有零星案例發生](../Page/非洲.md "wikilink")，這可能與東亞喜食未完全熟食淡水域水產的飲食文化習慣有關，例如[中菜的](../Page/中菜.md "wikilink")(淡水)[醉蝦](../Page/醉蝦.md "wikilink")、(淡水)[醉蟹](../Page/醉蟹.md "wikilink")、[日本菜生食的](../Page/日本菜.md "wikilink")(淡水)[刺身及](../Page/刺身.md "wikilink")[韓國菜的](../Page/韓國菜.md "wikilink")[醬油蟹](../Page/醬油蟹.md "wikilink")(淡水)，其製作過程未能完全殺死寄生蟲所致。此外，若其他動物曾不慎食入這些受感染物種，同樣也可能被感染，若人類或貓、狗、豬吃下牠們，在處理不完全的情況下，也會有染病的風險，但目前尚未有人與人直接傳染的案例。中華民國則因部分[河川污染嚴重](../Page/河川.md "wikilink")，居民少用淡水蝦蟹，儘管有衛氏肺吸蟲分布，但少有此類病例。

## 診斷與治療

患者可能出現[咳嗽](../Page/咳嗽.md "wikilink")、呼吸困難、血痰、[胸痛](../Page/胸痛.md "wikilink")、[腹痛](../Page/腹痛.md "wikilink")、[腹瀉等典型症狀](../Page/腹瀉.md "wikilink")，乍看與[結核病的極為相似](../Page/結核病.md "wikilink")，因此必須藉由檢驗[痰液](../Page/痰液.md "wikilink")、[糞便](../Page/糞便.md "wikilink")、[胃液中的蟲卵作為](../Page/胃液.md "wikilink")[鑑別診斷](../Page/鑑別診斷.md "wikilink")（differential
diagnosis）的依據，此外，[血液中的](../Page/血液.md "wikilink")[嗜酸性白血球](../Page/嗜酸性白血球.md "wikilink")（eosinophil）數量也會增高，[淋巴球也會製造出相應的](../Page/淋巴球.md "wikilink")[抗體](../Page/抗體.md "wikilink")，因此也是診斷的輔助項目，目前以[藥物](../Page/藥物.md "wikilink")[praziquantel或](../Page/praziquantel.md "wikilink")[Bithionol作為主要的治療](../Page/Bithionol.md "wikilink")。

## 參考文獻

<div class="references-small">

1.  M. Yokogawa, Paragonimus and paragonimiasis, *Adv Parasitol.*, 1969,
    Vol 7, p.375\~387.

2.

</div>

## 外部連結

  - [寄生蟲學-Nematoda(線蟲)-Paragonimus
    westermani(衛氏肺吸蟲)](http://smallcollation.blogspot.com/2013/02/nematoda-paragonimus-westermani.html)

  - [人類肺吸蟲之衛氏肺吸蟲](https://web.archive.org/web/20060110140516/http://www.path.cam.ac.uk/~schisto/OtherFlukes/Paragonimus.html)
    英國劍橋吸蟲研究小組

  -
[Category:寄生蟲](../Category/寄生蟲.md "wikilink")
[westermani](../Category/並殖屬.md "wikilink")

1.