**衛詩雅**（曾經使用藝名**詩雅**，，），[香港女](../Page/香港.md "wikilink")[藝人](../Page/藝人.md "wikilink")，在入行前，為兼職[模特兒及職業](../Page/模特兒.md "wikilink")[鑽石銷售員](../Page/鑽石.md "wikilink")。

## 名字

衛詩雅是她的真實姓名，入行不久經紀人公司因應[演藝圈已有](../Page/演藝圈.md "wikilink")「[衛詩](../Page/衛詩.md "wikilink")」，故建議她省略「衛」字變成「詩雅」。及後衛詩淡出，而[連詩雅則入行](../Page/連詩雅.md "wikilink")，衛詩雅覺得這樣避來避去不是辦法，遂主動向經紀人公司建議用回原名「衛詩雅」。\[1\]

## 背景

[诗雅.jpg](https://zh.wikipedia.org/wiki/File:诗雅.jpg "fig:诗雅.jpg")
衛詩雅於香港上水[太平邨長大](../Page/太平邨.md "wikilink")，家有一姊三弟。\[2\]而父親為一名地盤工人。她在2003年畢業於[上水官立中學](../Page/上水官立中學.md "wikilink")。中五畢業後與友人到[六福珠寶任職](../Page/六福珠寶.md "wikilink")[鑽石銷售員](../Page/鑽石.md "wikilink")。任職期間考取專業鑽石鑑定證書。衛詩雅早於16歲時擔任兼職模特兒，後於2008年獲當時所屬的模特兒公司
Jamcast
介紹去為電視劇《[四葉草之盛裝舞步愛作戰](../Page/盛裝舞步愛作戰.md "wikilink")》作角色試鏡。結果，她成功入圍，並對演戲產生興趣。之後便簽約成為[英皇娛樂集團旗下女藝人](../Page/英皇娛樂集團.md "wikilink")，由[霍汶希擔任其經理人](../Page/霍汶希.md "wikilink")。衛詩雅現時仍活躍於娛樂圈，並攻讀寵物美容及傳心術課程以為開寵物美容店做準備。

## 演出

### 電視

  - 2008年：[無綫電視](../Page/無綫電視.md "wikilink")
    《[四葉草之盛裝舞步愛作戰](../Page/盛裝舞步愛作戰.md "wikilink")》
    飾 Michelle
  - 2008年：[無綫電視](../Page/無綫電視.md "wikilink")《藍兒本色》
  - 2009年：[香港電台](../Page/香港電台.md "wikilink")《現代婚姻物語 – 後25的少女心事》
  - 2010年：[香港電台](../Page/香港電台.md "wikilink")《愛回家》
  - 2010年：《[青春旋律](../Page/青春旋律.md "wikilink")》飾 田惠
  - 2012年：[香港電台](../Page/香港電台.md "wikilink")《[火速救兵II](../Page/火速救兵II.md "wikilink")》飾
    Apple
  - 2012年：《[歡樂元帥](../Page/歡樂元帥.md "wikilink")》飾 南海二公主
  - 2013年：[香港電台](../Page/香港電台.md "wikilink")《非常平等任務 - 少數》飾 唐詠思
  - 2015年：[香港電視網絡](../Page/香港電視網絡.md "wikilink")《[還來得及再愛你](../Page/還來得及再愛你.md "wikilink")》飾
    樂晞嵐／小雪
  - 2016年：[香港電台](../Page/香港電台.md "wikilink")《[獅子山下2016](../Page/獅子山下.md "wikilink")》
    飾 Jill
  - 待上映：[ViuTV](../Page/ViuTV.md "wikilink")《歎息橋》 飾 何樂兒 Joyce

### 電影

  - 2007年：《12 Faces of Woman Short Film》（Director: Sammi Cheng）
  - 2008年：《[很想和你在一起](../Page/很想和你在一起.md "wikilink")》飾 歐陽冠楠
  - 2009年：《[愛出貓](../Page/愛出貓.md "wikilink")》飾 Michelle
  - 2010年：《[全城熱戀熱辣辣](../Page/全城熱戀熱辣辣.md "wikilink")》飾 周沅沅
  - 2010年：《[前度](../Page/前度.md "wikilink")》飾 戴佩詩
  - 2010年：《[囡囡](../Page/囡囡_\(電影\).md "wikilink")》飾 Icy
  - 2010年：《[白蛇傳說](../Page/白蛇傳說.md "wikilink")》飾 蝙蝠妖（客串）
  - 2012年：《[起勢摇滚](../Page/起勢摇滚.md "wikilink")》飾 Michelle
  - 2012年：《[紮職](../Page/紮職.md "wikilink")》飾 Michelle
  - 2013年：《[哭喪女](../Page/哭喪女.md "wikilink")》飾 綿綿
  - 2013年：《兜售回憶》（微電影）飾 初二
  - 2013年：《[重口味](../Page/重口味_\(電影\).md "wikilink")》飾 林寶兒
  - 2013年：《[救火英雄](../Page/救火英雄.md "wikilink")》飾 發電廠職員
  - 2014年：《[金雞SSS](../Page/金雞SSS.md "wikilink")》飾 阿花
  - 2015年：《[12金鴨](../Page/12金鴨.md "wikilink")》飾 甜品學生
  - 2015年：《[暴疯语](../Page/暴疯语.md "wikilink")》飾 Mona
  - 2015年：《[巴黎假期](../Page/巴黎假期.md "wikilink")》飾 地產經紀
  - 2016年：《[暗色天堂](../Page/暗色天堂.md "wikilink")》飾 阿怡
  - 2016年：《大手牽小手》飾 謝有愛(年輕版)
  - 2016年：《沖天火》飾 宗天保妻
  - 2017年：《女人永遠是對的》（又名：聖女奇緣）飾 若妍
  - 2017年：《[決戰食神](../Page/決戰食神.md "wikilink")》飾 美食主持人
  - 2017年：《[失眠](../Page/失眠_\(電影\).md "wikilink")》飾 侯文媛/侯文禎
  - 2017年：《[原諒他77次](../Page/原諒他77次.md "wikilink")》飾 Mandy
  - 2017年：《[喵星人](../Page/喵星人_\(電影\).md "wikilink")》飾 Miss Li
  - 2018年：《[我的情敵女婿](../Page/我的情敵女婿.md "wikilink")》飾 嘉欣
  - 2018年：《[洩密者](../Page/洩密者.md "wikilink")》飾 JOJO
  - 2018年：《告別之前》飾 張譚子君
  - 待上映：《獅子山上》
  - 待上映：《熟女愛漫游》飾 陶莎莎
  - 待上映：《[風林火山](../Page/風林火山_\(2019年電影\).md "wikilink")》
  - 待上映：《西謊遊記之唐僧踩錯界》
  - 待上映：《雙魂》飾 馬宥心
  - 待上映：《[再見UFO](../Page/再見UFO.md "wikilink")》
  - 待上映：《笑林寺》
  - 待上映：《[掃毒2之天地對決](../Page/掃毒2之天地對決.md "wikilink")》
  - 待上映：《限期破案》
  - 待上映：《感動她77次》飾 Mandy

### 配音

  - 2018：《[尋找小腳八](../Page/尋找小腳八.md "wikilink")》聲演：美紫

### 電台節目

  - 2009年：OM Live 《廿一世紀少年》

### 參與音樂MV

  - [張敬軒](../Page/張敬軒.md "wikilink")《餘震》、《老了十歲》、《青春常駐》
  - [方力申](../Page/方力申.md "wikilink")《認命》
  - [陳柏宇](../Page/陳柏宇.md "wikilink")《你來自那顆星》
  - [洪卓立](../Page/洪卓立.md "wikilink")《男孩看見野玫瑰》（TVB版本）、《很想和你在一起》
  - [陳偉霆](../Page/陳偉霆.md "wikilink")《今天終於知道錯》、《狐狸小姐》
  - [容祖兒](../Page/容祖兒.md "wikilink")《這就是愛嗎？》 、 《很忙》、 《戀人未滿》
  - [泳兒](../Page/泳兒.md "wikilink")《Forever Friends》
  - [鄭嘉穎](../Page/鄭嘉穎.md "wikilink")《幸福的遺憾》
  - The Bright Lights《原來這就是愛情》
  - [許靖韻](../Page/許靖韻.md "wikilink")《事與願違》(MV微電影 飾 Michelle)

### 曾拍攝廣告

  - [中信嘉華銀行](../Page/中信嘉華銀行.md "wikilink")（[中信銀行國際前身](../Page/中信銀行國際.md "wikilink")）（電視廣告）
  - mangocity.com LTD（電視廣告）
  - Canon（平面廣告）
  - [One2Free](../Page/One2Free.md "wikilink")（平面廣告）
  - 2006年：BGM時裝（平面廣告）
  - 2006年：[富邦銀行](../Page/富邦銀行.md "wikilink")（平面廣告）
  - 2008年：[Reebok China](../Page/Reebok.md "wikilink")
  - 2008年：[KFC](../Page/KFC.md "wikilink")「鬼馬芝士波」（電視廣告）
  - 2008年：[Canon](../Page/Canon.md "wikilink") Printer "Better to print
    yourself"（電視廣告）
  - 2009年 [元綠壽司](../Page/元綠壽司.md "wikilink")（平面廣告）
  - 2010年：解渴誌飲品（平面廣告）
  - 2010年：[小林製藥安美露止痛劑](../Page/小林製藥.md "wikilink")
  - 2016-2018年：Eurobeauté（平面廣告）
  - 2017-2018年：Lumière（平面廣告）
  - 2018年：MANE N TAIL美國箭牌洗髮系列

### 曾拍攝之品牌宣傳照

  - 2008年：Hush Puppies
  - 2008年：Bauhaus
  - 2008年：Disney Land
  - 2009年：St.lves（Skincare）
  - 2009年：Pappagallo（Gelato）
  - 2009年：Olay（Skincare）
  - 2010年：[西九龍中心](../Page/西九龍中心.md "wikilink")
  - 2010年：[Pizza Hut](../Page/Pizza_Hut.md "wikilink")
  - 2010年：[Lenovo](../Page/Lenovo.md "wikilink") Notebook
  - 2010年：Columbia
  - 2011年：[Reebok](../Page/Reebok.md "wikilink")

### 曾獲得獎項

  - 2010年：The Accolade Competition, California: Award of
    Excellence（香港電台《愛回家》）

### 曾擔任之大使

  - 2008年：愛貓大使
  - 2008年：PCM香港I.T.至尊大獎至專大使
  - 2009年：第13屆美國冒險樂園好學生大使
  - 2010年：香港電台太陽計劃太陽少女

## 音樂作品

  - 2010年：[很想和你在一起](../Page/很想和你在一起.md "wikilink")（合唱版；與[洪卓立合唱](../Page/洪卓立.md "wikilink")，收錄於《[Taste
    of Love](../Page/Taste_of_Love.md "wikilink")》）

## 注釋

## 參考文獻

## 外部連結

  -
  -
  -
  - [衛詩雅](https://www.youtube.com/channel/UCqG6xsWE9zqs4lVmgC5HQLg)YouTube
    Channel

  - [英皇娛樂官方網站](https://eeg.zone/#/artist/293)

[Category:前香港電視網絡藝員](../Category/前香港電視網絡藝員.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[shi](../Category/衛姓.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Wai衛](../Category/香港女歌手.md "wikilink")
[Category:上水官立中學校友](../Category/上水官立中學校友.md "wikilink")

1.
2.  [「牌王」衛詩雅走過娛樂圈低谷　為生計積極「考牌」](https://topick.hket.com/article/1994030/%E3%80%8C%E7%89%8C%E7%8E%8B%E3%80%8D%E8%A1%9B%E8%A9%A9%E9%9B%85%E8%B5%B0%E9%81%8E%E5%A8%9B%E6%A8%82%E5%9C%88%E4%BD%8E%E8%B0%B7%E3%80%80%E7%82%BA%E7%94%9F%E8%A8%88%E7%A9%8D%E6%A5%B5%E3%80%8C%E8%80%83%E7%89%8C%E3%80%8D)