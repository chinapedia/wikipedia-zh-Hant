**衡阳东站**，曾称**新衡阳站**，位于[中国](../Page/中国.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[衡阳市](../Page/衡阳市.md "wikilink")[珠晖区](../Page/珠晖区.md "wikilink")，茅坪村、王江村附近，是[京广高速铁路的一座车站](../Page/京广高速铁路.md "wikilink")，为[武广客运专线工程湖南段的客运站](../Page/武广客运专线.md "wikilink")、最大的非省会城市客运站，同时也是[衡柳铁路及](../Page/衡柳铁路.md "wikilink")[怀衡铁路的起点站](../Page/怀衡铁路.md "wikilink")，于2009年建成启用。

## 車站設施

衡阳东站设有站台5座，共有12条股道。站房建筑面积1.6万[平方米](../Page/平方米.md "wikilink")。

## 請參閱

  - [衡阳站](../Page/衡阳站.md "wikilink")

## 参考資料

  - [湖南城乡规划信息港（衡阳站）](https://web.archive.org/web/20070929202803/http://hyghw.gov.cn/article/article1.asp?id=14411&classid=33)
  - [衡阳市政府门户网站](http://www.hengyang.gov.cn/hengyang/pubtemplet/iframebody.asp?infoid=13918&style=%7B538116AD-AF2A-4978-B786-5A2A1621E91C%7D&secstyle=%7B82DA77AA-623C-4B59-B93B-D34D6BF09B70%7D)

|-    |-    |-    |-

[Category:衡阳市铁路车站](../Category/衡阳市铁路车站.md "wikilink")
[Category:珠晖区](../Category/珠晖区.md "wikilink")
[Category:中国铁路广州局集团车站](../Category/中国铁路广州局集团车站.md "wikilink")
[Category:2009年启用的铁路车站](../Category/2009年启用的铁路车站.md "wikilink")