[Hengyang_Station.jpg](https://zh.wikipedia.org/wiki/File:Hengyang_Station.jpg "fig:Hengyang_Station.jpg")

**衡阳站**位于[中国](../Page/中国.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[衡阳市](../Page/衡阳市.md "wikilink")[珠晖区](../Page/珠晖区.md "wikilink")，于1935年10月10日启用，为[广州铁路集团](../Page/广州铁路集团.md "wikilink")[衡阳车务段管辖的客运](../Page/衡阳车务段.md "wikilink")[特等站](../Page/特等站_\(中鐵\).md "wikilink")。经过铁路有[京广铁路](../Page/京广铁路.md "wikilink")、[湘桂铁路](../Page/湘桂铁路.md "wikilink")、[衡柳铁路](../Page/衡柳铁路.md "wikilink")、[吉衡铁路和](../Page/吉衡铁路.md "wikilink")[怀衡铁路](../Page/怀衡铁路.md "wikilink")。衡阳站的早期建筑属于[衡阳市文物保护单位](../Page/衡阳市文物保护单位.md "wikilink")。

## 車站設施

衡阳站建有候车站台4座、地道4座。南北两场共有到发线49股道，编组线22股道。另有零担货场、整车货场及车廠集装箱货场各1个。

## 使用情况

衡阳站日均办理旅客列车101列、交通车10列、货物列车184列。主要为[广州站到发](../Page/广州站.md "wikilink")、[衡柳铁路和](../Page/衡柳铁路.md "wikilink")[吉衡铁路的各类旅客列车](../Page/吉衡铁路.md "wikilink")。每年旅客到发达370万人次，每年货物发送量73万吨。

### 旅客列车目的地

<table>
<thead>
<tr class="header">
<th><p>担当路局</p></th>
<th><p>车次</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center></td>
<td><p>K760</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>K9093</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K9121</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 历史

  - 1935年10月10日：衡阳站建成启用。
  - 1950年：衡阳站进行扩建。
  - 1964年：衡阳站於修建武衡双线期間再次扩建。
  - 1990年至1995年：衡阳站全面进行技术改造，新建候车大楼，站坪亦向南扩宽。
  - 1994年4月15日：衡阳站首开衡阳←→[广州的](../Page/广州站.md "wikilink")**83/84**、**85/86**次特快车次（現已停運），全程521公里，运行时间8小时24分，结束无特快列车始发的历史。
  - 1995年5月30日：衡阳站由[一等站升格为](../Page/一等站_\(中鐵\).md "wikilink")[特等站](../Page/特等站_\(中鐵\).md "wikilink")。
  - 2005年5月19日：[衡阳北站](../Page/衡阳北站.md "wikilink")、[衡阳西站统一由衡阳站管理](../Page/衡阳西站.md "wikilink")。
  - 2010年10月21日：原衡阳直属站、原永州车务段、原郴州车务段合并为衡阳车务段\[1\]。

## 邻近车站

|-   |-  |-    |-

## 参考文献

  - [中国衡阳新闻网：衡阳火车站历史回顾](https://web.archive.org/web/20150717131644/http://www.e0734.com/2007/0426/40724.html)
  - [鼎言商旅网：衡阳站介绍](http://hunan.d0086.com/SKM/sljt/xxlcz.htm)

## 外部链接

## 参见

  - [衡阳东站](../Page/衡阳东站.md "wikilink")

{{-}}

[Category:衡阳市铁路车站](../Category/衡阳市铁路车站.md "wikilink")
[Category:京广铁路车站](../Category/京广铁路车站.md "wikilink")
[Category:湘桂铁路车站](../Category/湘桂铁路车站.md "wikilink")
[Category:珠晖区](../Category/珠晖区.md "wikilink")
[Category:1935年启用的铁路车站](../Category/1935年启用的铁路车站.md "wikilink")

1.