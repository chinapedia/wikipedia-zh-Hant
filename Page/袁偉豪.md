**袁偉豪**（，），出身於[無綫電視](../Page/無綫電視.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")，現為[無綫電視經理人合約男藝員及力捧小生之一](../Page/無綫電視.md "wikilink")。

## 簡歷

### 早年生活

袁偉豪中學時期就讀[新法書院](../Page/新法書院.md "wikilink")（太子分校），在當選1997年「Yes\!校花校草選舉」校草後，他仍有繼續學業，同時參與模特兒和電影的拍攝工作。\[1\]他曾任[人體](../Page/人體.md "wikilink")[素描](../Page/素描.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")，早於參選2007年度[香港先生前已有不少幕前演出](../Page/香港先生選舉.md "wikilink")，由於演出機會漸少，所以亦曾當上送貨員。2007年參選香港先生前任職高級私人教練\[2\]\[3\]。

### 參選勝出

2007年8月11日，袁偉豪在[香港先生選舉中勝出](../Page/香港先生選舉.md "wikilink")，成為冠軍。由於他於1998年曾演出[維他奶廣告與簽約](../Page/維他奶.md "wikilink")[力圖唱片](../Page/力圖唱片.md "wikilink")，故他於參選[香港先生時](../Page/香港先生.md "wikilink")，一直被報刊傳媒稱呼為「維他奶仔」。

### 演藝經歷

他當選後一直以拍攝電視劇為主，但多以閒角或配角身份出場，至近年角色及戲份開始吃香及上位，更在2013年於[TVB
马来西亚星光荟萃颁奖典礼2013奪得](../Page/TVB_马来西亚星光荟萃颁奖典礼2013.md "wikilink")“TVB最具潛質男藝員”及入圍“最喜愛TVB男配角”最後五強，並首度於[萬千星輝頒獎典禮2013提名](../Page/萬千星輝頒獎典禮2013.md "wikilink")“飛躍進步男藝員”。

2015年再度於[萬千星輝頒獎典禮2015獲提名](../Page/萬千星輝頒獎典禮2015.md "wikilink")「飛躍進步男藝員」和憑《[拆局專家](../Page/拆局專家.md "wikilink")》獲提名「最受歡迎電視男角色」。

2016年主演無綫電視劇集《[城寨英雄](../Page/城寨英雄.md "wikilink")》，飾演主角段迎風（牙佬），牙佬一角讓袁偉豪大獲好評，人氣急升，逐漸為人所知，廣受觀眾認同及支持，亦因此劇令廣大觀眾認識他而走紅，其後袁偉豪於[萬千星輝頒獎典禮2016獲提名](../Page/萬千星輝頒獎典禮2016.md "wikilink")「最佳男主角」，並奪得「最受歡迎電視男角色」，另外亦在[星和無綫電視大獎2016奪得](../Page/星和無綫電視大獎2016.md "wikilink")「我最愛TVB男角色」、「我最愛TVB熒幕情侶」及在[TVB
馬來西亞星光薈萃頒獎典禮2016奪得](../Page/TVB_馬來西亞星光薈萃頒獎典禮2016.md "wikilink")「最喜愛TVB電視角色」。

近年為無綫電視監製[林志華及](../Page/林志華.md "wikilink")[潘嘉德的常用演員](../Page/潘嘉德.md "wikilink")。

2018年，於[香港書展自資推出個人影集](../Page/香港書展.md "wikilink")《Benontheroad》\[4\]。在同年11月19日「萬千星輝賀台慶」中憑在劇集《[棟仁的時光](../Page/棟仁的時光.md "wikilink")》的演出，獲頒發「馬來西亞最喜愛TVB男主角」和「新加坡最喜愛TVB男主角」兩個獎項，在同一台慶節目上[黃智雯亦為奪得兩地](../Page/黃智雯.md "wikilink")「雙料」名銜的演員\[5\]。另外亦在劇集《[三個女人一個「因」](../Page/三個女人一個「因」.md "wikilink")》的出色演出入圉[觀眾在民間電視大奬](../Page/觀眾在民間電視大奬.md "wikilink")2018「民選最佳男主角」，最終得票第三名。

## 感情生活

袁偉豪2010年與馬沛詩合作舞台劇《我不要被你記住》後相戀，拍拖年半後分手，之後2013年一度復合，袁偉豪更曾公開表示要儲錢結婚，可惜最後仍分手收場。\[6\]

袁偉豪前女友為[2012年度香港小姐競選最上鏡小姐](../Page/2012年度香港小姐競選.md "wikilink")[岑杏賢](../Page/岑杏賢.md "wikilink")，他曾揚言視[岑杏賢為結婚對象](../Page/岑杏賢.md "wikilink")\[7\]\[8\]\[9\]。於2016年11月20日，袁偉豪於微博宣布分手\[10\]。

2017年8月，他承認[2016年度香港小姐競選友誼小姐](../Page/2016年度香港小姐競選.md "wikilink")[張寶兒為其現任女友](../Page/張寶兒.md "wikilink")\[11\]\[12\]。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                                        |                                                    |               |           |
| ------------------------------------------------------ | -------------------------------------------------- | ------------- | --------- |
| 首播                                                     | 劇名                                                 | 角色            | 性質        |
| 2008年                                                  | [法證先鋒II](../Page/法證先鋒II.md "wikilink")             | 吉家浩（Eric）     | 客串        |
| [尖子攻略](../Page/尖子攻略.md "wikilink")                     | 連長勝                                                | 男配角           |           |
| 2009年                                                  | [桌球天王](../Page/桌球天王.md "wikilink")                 | Jimmy         | 客串        |
| [古靈精探B](../Page/古靈精探B.md "wikilink")                   | 唐啟發                                                | 客串            |           |
| 2010年                                                  | [五味人生](../Page/五味人生.md "wikilink")                 | 阿伯拉罕（Abraham） | 客串        |
| [戀愛星求人](../Page/戀愛星求人.md "wikilink")                   | Kelvin                                             | 客串            |           |
| [情人眼裏高一D](../Page/情人眼裏高一D.md "wikilink")               | 參賽者                                                | 客串            |           |
| [老公萬歲](../Page/老公萬歲.md "wikilink")                     | 模特兒                                                | 客串            |           |
| [搜下留情](../Page/搜下留情.md "wikilink")                     | 周偉棠                                                | 客串            |           |
| [掌上明珠](../Page/掌上明珠.md "wikilink")                     | 彭駿才                                                | 客串            |           |
| [讀心神探](../Page/讀心神探.md "wikilink")                     | 盧文健                                                | 客串            |           |
| 2011年                                                  | [魚躍在花見](../Page/魚躍在花見.md "wikilink")               | 年紀細           | 客串        |
| [洪武三十二](../Page/洪武三十二.md "wikilink")                   | 李景隆                                                | 男配角           |           |
| [點解阿Sir係阿Sir](../Page/點解阿Sir係阿Sir.md "wikilink")       | 關Sir                                               | 客串            |           |
| [花花世界花家姐](../Page/花花世界花家姐.md "wikilink")               | 陳步禮                                                | 客串            |           |
| [團圓](../Page/團圓_\(香港電視劇\).md "wikilink")               | Vincent                                            | 客串            |           |
| [真相](../Page/真相_\(無綫電視劇\).md "wikilink")               | David                                              | 客串            |           |
| [紫禁驚雷](../Page/紫禁驚雷.md "wikilink")                     | 穆峰                                                 | 男配角           |           |
| [九江十二坊](../Page/九江十二坊.md "wikilink")                   | 徐老闆                                                | 客串            |           |
| [結·分@謊情式](../Page/結·分@謊情式.md "wikilink")               | 高青雲（Alvin）                                         | 第三男主角         |           |
| [荃加福祿壽探案](../Page/荃加福祿壽探案.md "wikilink")               | 唐十三                                                | 客串            |           |
| 2012年                                                  | [On Call 36小時](../Page/On_Call_36小時.md "wikilink") | 劉炳燦（Benjamin） | 第三男主角     |
| [拳王](../Page/拳王_\(電視劇\).md "wikilink")                 | 黎英                                                 | 男配角           |           |
| [飛虎](../Page/飛虎_\(電視劇\).md "wikilink")                 | 謝家星                                                | 男配角           |           |
| [天梯](../Page/天梯_\(無綫電視劇\).md "wikilink")               | 賀世亮                                                | 客串            |           |
| [雷霆掃毒](../Page/雷霆掃毒.md "wikilink")                     | 謝家星                                                | 客串            |           |
| 2013年                                                  | [好心作怪](../Page/好心作怪.md "wikilink")                 | 丁俊平（Martin）   | 第四男主角     |
| [師父·明白了](../Page/師父·明白了.md "wikilink")                 | 年青樂無涯                                              | 客串            |           |
| [情逆三世緣](../Page/情逆三世緣.md "wikilink")                   | [展昭](../Page/展昭.md "wikilink") / 展超仁               | 男配角           |           |
| [On Call 36小時II](../Page/On_Call_36小時II.md "wikilink") | 劉炳燦（Benjamin）                                      | 第四男主角         |           |
| [My盛Lady](../Page/My盛Lady.md "wikilink")               | 青年才俊                                               | 客串            |           |
| 2014年                                                  | [載得有情人](../Page/載得有情人.md "wikilink")               | 饒毅忠（Jason）    | 男配角       |
| [飛虎II](../Page/飛虎II.md "wikilink")                     | 謝家星                                                | 第四男主角         |           |
| [宦海奇官](../Page/宦海奇官.md "wikilink")                     | 方貴祥                                                | 第三男主角         |           |
| 2015年                                                  | **[拆局專家](../Page/拆局專家.md "wikilink")**             | **麥行直**       | **第二男主角** |
| 2016年                                                  | **[鐵馬戰車](../Page/鐵馬戰車.md "wikilink")**             | **施　馬**       | **第二男主角** |
| [EU超時任務](../Page/EU超時任務.md "wikilink")                 | 郭尚正                                                | 第二男主角         |           |
| **[城寨英雄](../Page/城寨英雄.md "wikilink")**                 | **段迎風（牙佬）／段通天**                                    | **第二男主角**     |           |
| [流氓皇帝](../Page/流氓皇帝_\(2016年電視劇\).md "wikilink")        | 朱祥勳                                                | 第二男主角         |           |
| 2017年                                                  | **[使徒行者2](../Page/使徒行者2.md "wikilink")**           | **徐天堂（天堂哥）**  | **第三男主角** |
| 2018年                                                  | **[三個女人一個「因」](../Page/三個女人一個「因」.md "wikilink")**   | **利東佳（Kai）**  | **第一男主角** |
| **[棟仁的時光](../Page/棟仁的時光.md "wikilink")**               | **曾棟仁（棟仁）**                                        | **第一男主角**     |           |
| [再創世紀](../Page/再創世紀.md "wikilink")                     | 方澤雨（Walter）                                        | 第三男主角         |           |
| 2019年                                                  | **[鐵探](../Page/鐵探.md "wikilink")**                 | **尚　垶**       | **第一男主角** |
|                                                        |                                                    |               |           |

### 電視劇（[香港電台](../Page/香港電台.md "wikilink")）

|        |                                        |        |
| ------ | -------------------------------------- | ------ |
| **首播** | **劇名**                                 | **角色** |
| 1999年  | 完全学生手册：死心塌地                            | 阿Sam   |
| 2001年  | [Y2K+01](../Page/Y2K+01.md "wikilink") | 老野     |
| 2003年  | 執法群英II：重案行動(上)鎗戰 (下)折箭                 | 警員     |

### 綜藝節目（[有線電視](../Page/有線電視.md "wikilink")）

|        |                |          |
| ------ | -------------- | -------- |
| **首播** | **節目名**        | **備註**   |
| 2001年  | YMC人We Wet Web | 主持（YMC台） |
|        |                |          |

### 綜藝節目（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>節目名</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>2007年度<a href="../Page/香港先生選舉.md" title="wikilink">香港先生選舉</a></p></td>
<td><p>參賽者（冠軍）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/舞動奇跡.md" title="wikilink">舞動奇跡</a></p></td>
<td><p>參賽者</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/智激校園.md" title="wikilink">智激校園</a></p></td>
<td><p>參賽者</p></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/千奇百趣Summer_Fun.md" title="wikilink">千奇百趣Summer Fun</a></p></td>
<td><p>嘉賓（第5、6集）</p></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/千奇百趣省港澳.md" title="wikilink">千奇百趣省港澳</a></p></td>
<td><p>嘉賓（第26集）</p></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/千奇百趣東南亞.md" title="wikilink">千奇百趣東南亞</a></p></td>
<td><p>嘉賓（第2集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小島怡情_(節目).md" title="wikilink">小島怡情</a></p></td>
<td><p>與<a href="../Page/許亦妮.md" title="wikilink">許亦妮主持</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/千奇百趣高B班.md" title="wikilink">千奇百趣高B班</a></p></td>
<td><p>嘉賓（第10集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/千奇百趣玩FREE_D.md" title="wikilink">千奇百趣玩FREE D</a></p></td>
<td><p>嘉賓（第23集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/玩嘢王.md" title="wikilink">玩嘢王</a></p></td>
<td><p>嘉賓（第7集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星夢傳奇.md" title="wikilink">星夢傳奇</a></p></td>
<td><p>參賽者</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/超級無敵獎門人_終極篇.md" title="wikilink">超級無敵獎門人 終極篇</a></p></td>
<td><p>嘉賓（第22集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/萬千星輝頒獎典禮2013.md" title="wikilink">萬千星輝頒獎典禮2013</a></p></td>
<td><p>「飛躍進步男藝員」候選人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/今日VIP.md" title="wikilink">今日VIP</a></p></td>
<td><p>嘉賓（4月1日－與《<a href="../Page/宦海奇官.md" title="wikilink">宦海奇官</a>》演員）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星級健康.md" title="wikilink">星級健康</a>（星級健康2）</p></td>
<td><p>主持（第6集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/今日VIP.md" title="wikilink">今日VIP</a></p></td>
<td><p>嘉賓（4月7日）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/超強選擇1分鐘.md" title="wikilink">超強選擇1分鐘</a></p></td>
<td><p>嘉賓（第11集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萬千星輝賀台慶.md" title="wikilink">萬千星輝賀台慶</a>2015</p></td>
<td><p>嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/萬千星輝頒獎典禮2015.md" title="wikilink">萬千星輝頒獎典禮2015</a></p></td>
<td><p>「最受歡迎電視男角色」及「飛躍進步男藝員」<br />
候選人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p><a href="../Page/星級健康.md" title="wikilink">星級打工時代</a>（星級健康3之星夢成真）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/今日VIP.md" title="wikilink">今日VIP</a></p></td>
<td><p>嘉賓（1月18日－與<a href="../Page/唐詩詠.md" title="wikilink">唐詩詠</a>、<a href="../Page/蔡思貝.md" title="wikilink">蔡思貝</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>一綫娛樂</p></td>
<td><p>嘉賓<br />
（1月18、25日－與《<a href="../Page/鐵馬戰車.md" title="wikilink">鐵馬戰車</a>》演員）<br />
（5月16日－與<a href="../Page/許家傑.md" title="wikilink">許家傑</a>、<a href="../Page/翟威廉.md" title="wikilink">翟威廉及</a><a href="../Page/李晉強.md" title="wikilink">李晉強</a>）[13]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Sunday好戲王.md" title="wikilink">Sunday好戲王</a></p></td>
<td><p>嘉賓（第1集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/湊仔攻略.md" title="wikilink">湊仔攻略</a></p></td>
<td><p>嘉賓（第5集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/今日VIP.md" title="wikilink">今日VIP</a></p></td>
<td><p>嘉賓（3月21日）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Do姐有問題.md" title="wikilink">Do姐有問題</a></p></td>
<td><p>嘉賓（第22集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>萬千星輝睇多D</p></td>
<td><p>表演嘉賓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夜宵磨.md" title="wikilink">夜宵磨</a>（第三輯）</p></td>
<td><p>嘉賓（第22集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/男人食堂.md" title="wikilink">男人食堂</a></p></td>
<td><p>嘉賓（第8集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/打工捱世界.md" title="wikilink">反斗紅星冇暑假－打工捱世界</a></p></td>
<td><p>與<a href="../Page/楊明_(香港).md" title="wikilink">楊明主持</a><br />
与<a href="../Page/楊明.md" title="wikilink">楊明荣获</a>《<a href="../Page/TVB_馬來西亞星光薈萃頒獎典禮2016.md" title="wikilink">TVB 馬來西亞星光薈萃頒獎典禮2016我最愛TVB綜藝節目主持人</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>萬千星輝放暑假</p></td>
<td><p>表演嘉賓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我愛香港_(電視節目).md" title="wikilink">我愛香港</a></p></td>
<td><p>嘉賓（第3集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/你健康嗎？.md" title="wikilink">你健康嗎？（第二輯）</a></p></td>
<td><p>嘉賓（第5集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p><a href="../Page/星级健康.md" title="wikilink">星級超常任務</a>（星級健康4之Go Green碳世界）</p></td>
<td><p>主持（第1、4、7集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Ben_Sir研究院.md" title="wikilink">Ben Sir研究院</a></p></td>
<td><p>嘉賓（第 集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/打工捱世界II.md" title="wikilink">打工捱世界II</a></p></td>
<td><p>與<a href="../Page/楊明_(香港).md" title="wikilink">楊明主持</a><br />
与<a href="../Page/楊明.md" title="wikilink">楊明入围</a>《<a href="../Page/星和無綫電視大獎2017.md" title="wikilink">星和無綫電視大獎2017我最愛TVB綜藝節目主持人</a>》<br />
与<a href="../Page/楊明.md" title="wikilink">楊明入围</a>《<a href="../Page/TVB_馬來西亞星光薈萃頒獎典禮2017.md" title="wikilink">TVB 馬來西亞星光薈萃頒獎典禮2017最喜愛TVB節目主持</a>》<br />
与<a href="../Page/楊明.md" title="wikilink">楊明入围</a>《<a href="../Page/萬千星輝頒獎典禮2017.md" title="wikilink">萬千星輝頒獎典禮2017最受歡迎電視拍檔</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td><p><a href="../Page/TVB_50+1再出發節目巡禮2019.md" title="wikilink">TVB 50+1再出發節目巡禮2019</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歡樂滿東華.md" title="wikilink">歡樂滿東華</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td><p><a href="../Page/新春開運王.md" title="wikilink">新春開運王第四輯</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/打工捱世界III.md" title="wikilink">打工捱世界III</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

|                                            |                                                          |         |
| ------------------------------------------ | -------------------------------------------------------- | ------- |
| **首映**                                     | **電影名**                                                  | **角色**  |
| 1998年                                      | [新古惑仔之少年激鬥篇](../Page/新古惑仔之少年激鬥篇.md "wikilink")           | 包達明（巢皮） |
| 1999年                                      | [烈火戰車2極速傳說](../Page/烈火戰車2極速傳說.md "wikilink")             | 大頭文     |
| 薄餅速遞                                       |                                                          |         |
| [賭俠1999](../Page/賭俠1999.md "wikilink")     | Jack                                                     |         |
| 鬼咁過癮                                       |                                                          |         |
| 還我情心                                       |                                                          |         |
| [中華英雄](../Page/中華英雄.md "wikilink")         | 水賀                                                       |         |
| 死蠢                                         |                                                          |         |
| 新鬼片王之再現凶榜                                  | Nokia                                                    |         |
| [串燒三兄弟](../Page/串燒三兄弟.md "wikilink")       |                                                          |         |
| 2000年                                      | [好兄吾兄](../Page/好兄吾兄.md "wikilink")                       |         |
| [猛鬼情迷](../Page/猛鬼情迷.md "wikilink")         |                                                          |         |
| [偷吻](../Page/偷吻_\(2000年电影\).md "wikilink") |                                                          |         |
| [香港處男](../Page/香港處男.md "wikilink")         | 阿澤                                                       |         |
| 2001年                                      | [拳神](../Page/拳神.md "wikilink")                           |         |
| [不死情謎](../Page/不死情謎.md "wikilink")         |                                                          |         |
| 2002年                                      | [一蚊雞保鑣](../Page/一蚊雞保鑣.md "wikilink")                     | 收數佬     |
| [無間道](../Page/無間道.md "wikilink")           | 重案組探員                                                    |         |
| 2003年                                      | [無間道III終極無間](../Page/無間道III終極無間.md "wikilink")           | 情報科探員   |
| 2004年                                      | [爆裂都市](../Page/爆裂都市.md "wikilink")                       | Glen手下  |
| [重案黐孖Gun](../Page/重案黐孖Gun.md "wikilink")   | 信仔                                                       |         |
| [後備甜心](../Page/後備甜心.md "wikilink")         | 生日會朋友                                                    |         |
| 2007年                                      | [校墓處](../Page/校墓處.md "wikilink")                         | 阿文      |
| 2009年                                      | [Laughing Gor之變節](../Page/Laughing_Gor之變節.md "wikilink") | 探員      |
| 2010年                                      | [72家租客](../Page/72家租客.md "wikilink")                     | 石堅店售貨員  |
| [翡翠明珠](../Page/翡翠明珠.md "wikilink")         | 何公子                                                      |         |
| 拍摄中                                        | [使徒行者：諜影行動](../Page/使徒行者：諜影行動.md "wikilink")             |         |
|                                            |                                                          |         |

### 歌曲

  - 2016年：[Amazing Summer](../Page/Amazing_Summer.md "wikilink") 主題曲
    《乘風》 [1](https://www.youtube.com/watch?v=d6lvYBpPkFE)

### 舞台劇

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>場地</strong></p></td>
<td><p><strong>合作</strong></p></td>
</tr>
<tr class="even">
<td><p>2010年3月<br />
2010年9月</p></td>
<td><p><a href="http://hk.apple.nextmedia.com/entertainment/art/20100326/13862267">我不要被你記住</a><br />
<a href="http://timable.com/event/890">我不要被你記住</a>（重演）</p></td>
<td><p><a href="../Page/上環文娛中心.md" title="wikilink">上環文娛中心</a><br />
<a href="../Page/葵青劇院.md" title="wikilink">葵青劇院</a></p></td>
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪</a>、<a href="../Page/賴芸芬.md" title="wikilink">賴芸芬</a>、<a href="../Page/馬沛詩.md" title="wikilink">馬沛詩</a><br />
<a href="../Page/周柏豪.md" title="wikilink">周柏豪</a>、<a href="../Page/楊淇.md" title="wikilink">楊淇</a></p></td>
</tr>
<tr class="odd">
<td><p>2012年8月</p></td>
<td><p><a href="http://timable.com/event.php?eid=43227">失身1234 My First Time</a></p></td>
<td><p><a href="../Page/香港文化中心.md" title="wikilink">香港文化中心</a></p></td>
<td><p><a href="../Page/陳慧心.md" title="wikilink">陳慧心</a>、<a href="../Page/茜利妹.md" title="wikilink">茜利妹</a>、<a href="../Page/白只.md" title="wikilink">白只</a></p></td>
</tr>
</tbody>
</table>

## 廣告 / 代言

|        |                                                                      |
| ------ | -------------------------------------------------------------------- |
| **年份** | **產品**                                                               |
| 1998年  | [維他奶新生代](../Page/維他奶.md "wikilink")                                  |
| 2007年  | MediBeauty for Man                                                   |
| 2013年  | 韓國《Mask House》Celebrity精華 代言人                                        |
| 2015年  | [20 LASER男士激光脫毛中心](http://20laser.com.hk)                            |
| 2016年  | [L'Oréal Paris 酵素精華肌底液](https://www.youtube.com/watch?v=OnT8_lNRXEk) |
| 2017年  | [渣打 MARVEL ATM Card](https://www.youtube.com/watch?v=4VkgNItSFvg)    |

## 獎項

|                                                                    |                                                                    |                       |
| ------------------------------------------------------------------ | ------------------------------------------------------------------ | --------------------- |
| **年份**                                                             | **頒獎典禮**                                                           | **獎項**                |
| 1997年                                                              | [Yes\!校花校草選舉](../Page/Yes!.md "wikilink")                          | 校草冠軍                  |
| 2007年                                                              | 2007年度[香港先生選舉](../Page/香港先生選舉.md "wikilink")                       | 冠軍                    |
| 2013年                                                              | [TVB 馬來西亞星光薈萃頒獎典禮2013](../Page/TVB_馬來西亞星光薈萃頒獎典禮2013.md "wikilink") | TVB最具潛質男藝員            |
| 2015年                                                              | [TVB 馬來西亞星光薈萃頒獎典禮2015](../Page/TVB_馬來西亞星光薈萃頒獎典禮2015.md "wikilink") | 最喜愛TVB電視角色 《拆局專家》     |
| 最喜愛TVB男配角 《拆局專家》                                                   |                                                                    |                       |
| 2016年                                                              | [星和無綫電視大獎2016](../Page/星和無綫電視大獎2016.md "wikilink")                 | 我最愛TVB男角色 《城寨英雄》      |
| 我最愛TVB熒幕情侶 《城寨英雄》                                                  |                                                                    |                       |
| [TVB 馬來西亞星光薈萃頒獎典禮2016](../Page/TVB_馬來西亞星光薈萃頒獎典禮2016.md "wikilink") | 最喜愛TVB電視角色 《城寨英雄》                                                  |                       |
| 最喜愛TVB節目主持 《反斗紅星冇暑假 打工捱世界》                                         |                                                                    |                       |
| [萬千星輝頒獎典禮2016](../Page/萬千星輝頒獎典禮2016.md "wikilink")                 | 最受歡迎電視男角色 《城寨英雄》                                                   |                       |
| 2017年                                                              | [星和無綫電視大獎2017](../Page/星和無綫電視大獎2017.md "wikilink")                 | 我最愛TVB男角色 《使徒行者2》     |
| [TVB 馬來西亞星光薈萃頒獎典禮2017](../Page/TVB_馬來西亞星光薈萃頒獎典禮2017.md "wikilink") | 最喜愛TVB電視角色 《使徒行者2》                                                 |                       |
| 2018年                                                              | [萬千星輝頒獎典禮2018](../Page/萬千星輝頒獎典禮2018.md "wikilink")                 | 馬來西亞最喜愛TVB男主角 《棟仁的時光》 |
| 新加坡最喜愛TVB男主角 《棟仁的時光》                                               |                                                                    |                       |
| [觀眾在民間電視大奬](../Page/觀眾在民間電視大奬.md "wikilink")2018                   | 民選最佳男主角 《三個女人一個「因」》（第三名）                                           |                       |

## 參考資料

## 注釋

## 外部連結

  -
  - [香港先生競選的個人介紹網頁](https://web.archive.org/web/20071020082628/http://tvcity.tvb.com/special/mrhk2007/profile/07.html)

[袁](../Category/香港新教徒.md "wikilink")
[Category:香港先生](../Category/香港先生.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:前有線電視藝員](../Category/前有線電視藝員.md "wikilink")
[Category:新法書院校友](../Category/新法書院校友.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:星夢一幫](../Category/星夢一幫.md "wikilink")
[Category:香港東莞人](../Category/香港東莞人.md "wikilink")
[WAI](../Category/袁姓.md "wikilink")

1.
2.
3.
4.  [【書展】袁偉豪出書硬撼性感寫真](https://bka.mpweekly.com/focus/20180718-123361)
5.
6.
7.  [娶硬岑杏賢　袁偉豪：唔想浪費女友時間](http://www.orientalsunday.hk/%E8%97%9D%E4%BA%BA%E5%B0%88%E8%A8%AA/%E5%A8%B6%E7%A1%AC%E5%B2%91%E6%9D%8F%E8%B3%A2-%E8%A2%81%E5%81%89%E8%B1%AA%EF%BC%9A%E5%94%94%E6%83%B3%E6%B5%AA%E8%B2%BB%E5%A5%B3%E5%8F%8B%E6%99%82%E9%96%93/)
8.  [爆肌袁偉豪唔敢拖手　一眼關七](http://orientaldaily.on.cc/cnt/entertainment/20160509/00282_028.html)
9.  [袁偉豪派定心丸：我屬於岑杏賢](http://orientaldaily.on.cc/cnt/entertainment/20160512/00282_031.html)
10.
11.
12. [【獨家】又係嗒港姐！袁偉豪秘撻張寶兒](http://hk.on.cc/hk/bkn/cnt/entertainment/20170816/bkn-20170816000008946-0816_00862_001.html)，東網，2017年8月16日
13. [梁麗翹奉旨非禮半裸袁偉豪加成班港男](http://hk.on.cc/hk/bkn/cnt/entertainment/20160516/bkn-20160516060003687-0516_00862_001.html)