[Refusing_the_Seat_-_Anonymous_painter_during_the_Song_dynasty.jpg](https://zh.wikipedia.org/wiki/File:Refusing_the_Seat_-_Anonymous_painter_during_the_Song_dynasty.jpg "fig:Refusing_the_Seat_-_Anonymous_painter_during_the_Song_dynasty.jpg")
**袁盎**，[字](../Page/表字.md "wikilink")**丝**，[西汉](../Page/西汉.md "wikilink")[楚国人](../Page/楚国.md "wikilink")。[汉文帝](../Page/汉文帝.md "wikilink")、[漢景帝時大臣](../Page/漢景帝.md "wikilink")。

## 生平

袁盎的父親是盜賊，[遷徙到了](../Page/遷徙.md "wikilink")[安陵](../Page/安陵.md "wikilink")。袁盎的兄長[袁噲擔任官員](../Page/袁噲.md "wikilink")。袁盎起初為[呂祿的](../Page/呂祿.md "wikilink")[舍人](../Page/舍人.md "wikilink")。

[漢文帝](../Page/漢文帝.md "wikilink")[即位後](../Page/即位.md "wikilink")，哥哥[袁噲舉薦袁盎進入](../Page/袁噲.md "wikilink")[朝廷](../Page/朝廷.md "wikilink")，為[郎中](../Page/郎中_\(官职\).md "wikilink")。绛侯[周勃](../Page/周勃.md "wikilink")[拜相](../Page/拜相.md "wikilink")，因此甚为得意，對皇帝也常常擺臉色。文帝因周勃[誅殺諸呂立下大功](../Page/誅殺諸呂.md "wikilink")，認為周勃是守護社稷之臣，對周勃非常恭敬。袁盎进谏说，社稷之臣应该是与與君王共存共亡才對，[吕太后封诸吕為](../Page/吕太后.md "wikilink")[諸侯王](../Page/諸侯王.md "wikilink")，周勃身为[太尉却不能撥亂反正](../Page/太尉.md "wikilink")。吕太后駕崩，大臣結盟[誅殺諸呂时](../Page/誅殺諸呂.md "wikilink")，周勃掌握兵权，剛好順應時勢成功平亂而已，只能称作功臣，不能称作「社稷之臣」。每每周勃有對君主驕傲之臉色，而陛下却谦退，這樣毫无君臣之礼。文帝接受了袁盎的建议，上朝时逐渐嚴肅了起来，周勃因此敬畏皇帝，也怨恨袁盎。

後來周勃遇到了袁盎，就罵他说：“我跟你哥哥袁哙很要好，今天你小子竟然在朝廷毁谤我！”袁盎也不願意道歉。后来周勃被[罢免](../Page/罢免.md "wikilink")，回到封地，害怕被殺害，經常披掛[鎧甲](../Page/鎧甲.md "wikilink")，命令家人手持武器會見[郡守和](../Page/郡守.md "wikilink")[郡尉](../Page/郡尉.md "wikilink")，被指控[谋反](../Page/谋反.md "wikilink")，被关押在[京城狱中](../Page/京城.md "wikilink")。朝中的[王公](../Page/王公.md "wikilink")[大臣無人替他说情](../Page/大臣.md "wikilink")，只有袁盎一直出面澄清周勃不可能造反。周勃出狱后，感念袁盎出力颇多，乃与他结为挚友。

淮南王[劉長為母報仇](../Page/劉長.md "wikilink")，殺了辟阳侯[审食其](../Page/审食其.md "wikilink")，又為人驕橫，袁盎就暗示皇帝稍加譴責，文帝不理會，直到劉長涉及謀反，丞相[張蒼領群臣要求文帝處死劉長](../Page/張蒼.md "wikilink")。文帝只廢其[王爵](../Page/王爵.md "wikilink")，發配[蜀郡](../Page/蜀郡.md "wikilink")[嚴縣的邛萊山](../Page/嚴縣.md "wikilink")，並且送給劉長十個美女，每天五斤肉、兩斗酒。时任[中郎将的袁盎反對](../Page/中郎将.md "wikilink")，認為劉長必定會在途中死去，漢文帝不相信。後來，劉長因為不堪屈辱，[自殺而亡](../Page/自殺.md "wikilink")。文帝非常難過，向袁盎表示自己當初不該不聽勸告，背负杀弟之名。袁盎暗示皇帝獎賞淮南王的子嗣，文帝便将淮南王的三个儿子都封为王，於是袁盎在朝廷中名声大振。

漢文帝的愛妾[慎夫人很受宠](../Page/慎夫人.md "wikilink")，常和文帝、[窦皇后同席而坐](../Page/窦猗.md "wikilink")。一次，文帝、窦皇后、慎夫人在[上林苑游玩](../Page/上林苑.md "wikilink")，袁盎把慎夫人的坐席往后拉，不讓他們一起坐，慎夫人於是不肯就坐，文帝也很生气，就起身回宫了。袁盎後來劝文帝：皇后是主，慎夫人只不过是个妾，妾怎么能與主同席呢！難道皇上沒聽過[戚夫人被呂太后虐殺的事麼](../Page/戚夫人.md "wikilink")？慎夫人得知後非常高興，赐给袁盎金五十斤。

文帝從[霸陵上山](../Page/霸陵.md "wikilink")，打算乘馬車從西邊的陡坡而下。袁盎騎著馬，緊靠著皇帝的車子，還拉著馬車韁繩。皇上說：「將軍害怕出事嗎？」袁盎說：「臣聽說家有千金的人不靠近屋簷坐臥，家有百金的人不倚靠樓臺的欄杆，英明的君主不應冒險而心存僥倖。現在陛下放縱駕車的六匹馬下陡坡，假如馬匹受驚、車輛毀壞，陛下縱然看輕自己，又怎麼對得起高祖和太后呢？」文帝於是中止。

袁盎因為人直言，不見容於朝廷，終於被外派為陇西都尉。在軍中，袁盎对士兵们爱护有加，士兵们都争着为他效命。不久，调任为齐国[国相](../Page/国相.md "wikilink")，不久，又调到吴国为[国相](../Page/国相.md "wikilink")。袁盎的侄子[袁种对他说](../Page/袁种.md "wikilink")：「吴王[刘濞骄横已久](../Page/刘濞.md "wikilink")，常有造反之心。你如果想要弹劾他，他就先上书弹劾你，如果沒[彈劾](../Page/彈劾.md "wikilink")，就会直接謀殺你了。南方地勢低又潮湿，你每天只管饮酒度日，不管公事，常劝吴王不要谋反就行了，这样才能不被杀害。」袁盎聽信了這段話，果然平安度過任期。

袁盎曾路遇丞相[申屠嘉](../Page/申屠嘉.md "wikilink")，申屠嘉對袁盎不禮貌，袁盎於是去拜見了申屠嘉，並對他說教了一番，申屠嘉心悅誠服，奉袁盎為上賓。

袁盎和[晁错不和](../Page/晁错.md "wikilink")，[漢景帝](../Page/漢景帝.md "wikilink")[七国之乱时](../Page/七国之乱.md "wikilink")，晁错[彈劾袁盎曾經收受吴王](../Page/彈劾.md "wikilink")[刘濞的金錢財物](../Page/刘濞.md "wikilink")，景帝赦免袁盎為庶人，後來晁错又想繼續告發袁盎，袁盎心急如焚，於是透過[窦婴](../Page/窦婴.md "wikilink")，終於面見皇帝，袁盎力劝[汉景帝杀晁错](../Page/汉景帝.md "wikilink")，安撫諸侯，景帝於是突襲式[腰斬了晁错](../Page/腰斬.md "wikilink")。

晁错被[處決之后](../Page/處決.md "wikilink")，袁盎出使至吴国，吴王刘濞想要留用袁盎，袁盎不想歸順叛軍，於是逃亡，刘濞派了五百人圍困袁盎，袁盎當年在吳國，曾經赦免了一個[私通其婢女的軍官](../Page/私通.md "wikilink")，甚至把該婢女也賞賜給他。該軍官於是幫了袁盎逃出吳國。

[七國之亂平定后](../Page/七國之亂.md "wikilink")，袁盎被任为楚[國相](../Page/國相.md "wikilink")，不久就稱病退休了，但景帝仍常派人向他詢問國家大事。

后来因反对立梁王[刘武为储君](../Page/刘武.md "wikilink")，遭到梁王忌恨，梁王派[刺客欲杀袁盎](../Page/刺客.md "wikilink")。刺客見到袁盎說：「我接受了梁王的金錢來刺殺你，您行事厚道，我不忍心刺殺您。」並警告袁盎應該小心，還有十幾個刺客會來襲，袁盎於是跑出去[算命](../Page/算命.md "wikilink")，回家時，另一批梁國刺客還是在安陵外城門外面刺殺了袁盎。

## 軼事

袁盎為人慷慨激昂，很講道義，與他結交的很多都是當代有名的[游俠](../Page/游俠.md "wikilink")，如[劇孟](../Page/劇孟.md "wikilink")、[季心等](../Page/季心.md "wikilink")。

据《[太平广记](../Page/太平广记.md "wikilink")》，袁盎的墓被[广川王](../Page/广川王.md "wikilink")[刘去](../Page/刘去.md "wikilink")[盜墓](../Page/盜墓.md "wikilink")，其中棺椁皆为陶瓦材质，陪葬品只有一面铜镜。

## 参考文献

  -
[Category:西汉郎中](../Category/西汉郎中.md "wikilink")
[Category:西汉中郎将](../Category/西汉中郎将.md "wikilink")
[Category:西汉都尉](../Category/西汉都尉.md "wikilink")
[Category:西漢國相](../Category/西漢國相.md "wikilink")
[Category:西汉太常](../Category/西汉太常.md "wikilink")
[Y袁](../Category/西漢遇刺身亡者.md "wikilink")
[A盎](../Category/袁姓.md "wikilink")