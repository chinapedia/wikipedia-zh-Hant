**被提**（英語：Rapture），是[基督教末世论中的一種概念](../Page/基督教末世论.md "wikilink")，認為當[耶穌再臨之前](../Page/耶穌再臨.md "wikilink")（或同時），已死的信徒將會被復活高升，活著的信徒也将會一起被送到天上与[基督相會](../Page/基督.md "wikilink")，并且身體将升华为不朽的身体。\[1\]\[2\]雖然幾乎所有基督徒团体都相信自己得到救赎和永远的生命，「被提」这个术语特别用于这个特别事件。这个概念在1830年代，以及较近的1970年代，由[千禧年前论者](../Page/千禧年前论.md "wikilink")，特别是[时代论者对圣经的解释而开始普及](../Page/时代论.md "wikilink")。根据这些理论，世界上的事件表明，末日预言即将應驗，被提可能在任何时刻发生。許多较保守的[新教](../Page/新教.md "wikilink")[基督徒](../Page/基督徒.md "wikilink")（包括[基要派](../Page/基要派.md "wikilink")、[福音派](../Page/福音派.md "wikilink")、[五旬节派](../Page/五旬节派.md "wikilink")、[浸信会和许多独立团体](../Page/浸信会.md "wikilink")）支持此解釋。

「被提」的確切意義、時間以及影響，在基督徒社群中仍然有爭議\[3\]，這個術語至少會被以兩種意義使用。[災前被提論的觀點](../Page/大災難.md "wikilink")，一群人將會像字面上一樣離開“與主在空中相會”，另一群人則會被留在地上。這是現在最常用的意義，特別在美國基要派之中。\[4\]過去傳統的觀點，則只是將「被提」當作「最後復活」的同義詞，不相信有一群人會另外被從地上帶走。\[5\]\[6\]這個區別很重要，因為某些類型的基督教從來沒有在宗教教育中提及“the
Rapture”，但在描述最後的復活發生時，可能會使用較古老但更一般性的詞彙“rapture”。\[7\]

關於耶穌再臨的時間有很多種說法（包括究竟會是一次事件或兩次），被提的時間也有兩種解釋，一種認為被提將发生在[大灾难](../Page/大灾难.md "wikilink")（[耶稣再临之前为期](../Page/耶稣再临.md "wikilink")7年的时期）前夕，另一种解释认为被提将会发生在大灾难中途，此后基督到地上建立他的王国，统治世界1000年（见[千禧年](../Page/千禧年.md "wikilink")）。對於[帖撒羅尼迦前書](../Page/帖撒羅尼迦前書.md "wikilink")4章所描寫「在空中相遇」的目的為何，也有不同的觀點。許多教派如[羅馬天主教](../Page/羅馬天主教.md "wikilink")\[8\]
，[東正教](../Page/東正教.md "wikilink")\[9\]，[路德宗和](../Page/路德宗.md "wikilink")[改革宗](../Page/改革宗.md "wikilink")，\[10\]相信被提的意義只是表示當耶穌再臨時，一般性的最後復活。他們不相信在[帖撒羅尼迦前書](../Page/帖撒羅尼迦前書.md "wikilink")4章17節的事件發生後，還會那個[大災難的時期](../Page/大災難.md "wikilink")，讓一群被留在地上的人經歷。\[11\]

學者普遍認為，[災前被提論起源於十八世紀的](../Page/災前被提論.md "wikilink")[清教徒傳道](../Page/清教徒.md "wikilink")[英克里斯·馬瑟和](../Page/英克里斯·馬瑟.md "wikilink")[科頓·馬瑟](../Page/科頓·馬瑟.md "wikilink")，\[12\]\[13\]
隨後由[約翰·納爾遜·達秘在](../Page/約翰·納爾遜·達秘.md "wikilink")1830年代普及，\[14\]
再透過[司可福串注聖經的廣泛流傳普及於](../Page/司可福串注聖經.md "wikilink")20世紀初[美國](../Page/美國.md "wikilink")。\[15\]。其他人包括[格蘭特·傑佛瑞則認為](../Page/格蘭特·傑佛瑞.md "wikilink")，七世紀敘利亞教會文獻《[托名的以法蓮的啟示錄](../Page/托名的以法蓮的啟示錄.md "wikilink")》支持災前被提。\[16\]

## 语源

「被提」一詞源自[拉丁語](../Page/拉丁語.md "wikilink")（raptus
為「已經被掌握的」之意），而非圣经所使用的的语言希伯来语和[希腊语](../Page/希腊语.md "wikilink")。希臘語的「harpazo」在希臘語的帖撒羅尼迦前書4:17出現過，其意為「使之從地面升起」、「為自己而拿」，或簡單地說，「選擇」（haireomai）。這些意思都和「airo」一詞很像，其意為「舉起」。

## 历史

对于被提教义的起源有激烈的争议。[东正教](../Page/东正教.md "wikilink")、主流新教和[罗马天主教这些传统教派不教导](../Page/罗马天主教.md "wikilink")，甚或抵制这一教义，一方面是因为他们未发现早期教父提到这一教义。\[17\]另一方面还因为他们不同意相信被提者对这些经文的解释。

至少有一本18世纪和2本19世纪的书说到灾前被提，一本出版于1788年，一本是1812年天主教神父Emmanuel Lacunza所写的著作
\[18\]，还有一本是1827年[达秘的著作](../Page/达秘.md "wikilink")\[19\]。不过，1788年出版的著作和Lacunza的著作持相反观点

灾前被提信仰的兴起有时被归因于一位15岁的苏格兰-爱尔兰女孩Margaret Macdonald (Prophecy)（Edward
Irving的追随者)，她在1830年看见异象，1861年出版成书\[20\]。

被提这个名词的普及与[普利茅斯弟兄会领袖](../Page/普利茅斯弟兄会.md "wikilink")[达秘的教导](../Page/达秘.md "wikilink")，以及19世纪末英语教会中[千禧年前论](../Page/千禧年前论.md "wikilink")（premillennialism）和[时代论](../Page/时代论.md "wikilink")（dispensationalism）的兴起有很大的关系。1908年，被提的教义由于福音传道者William
Eugene Blackstone的著作Jesus Is
Coming（销售量达100多万册）而进一步普及\[21\]。“被提”一词首先出现在出版物中是1909年的Scofield
Reference Bible\[22\]。

1957年，[达拉斯神学院的神学家](../Page/达拉斯神学院.md "wikilink")博士写了一本书《被提问题》（The
Rapture Question），支持灾前被提理论，这本书的销售量达到65,000册。1958年，也写了一本书《圣经末世学研究》（Things
to Come: A Study in Biblical Eschatology）支持灾前被提理论，这本书的销售量达到215,000册。

1970年代，被提成为一个流行的话题，这部分是由于的书籍，包括《过去的伟大地球》（The Late Great Planet
Earth）一书，该书销售了1500万-3500万册
\[23\]。何凌西声称被提即将来临，这个思想受到当时世界情形的影响。[冷战和欧洲经济共同体显然是](../Page/冷战.md "wikilink")[哈米吉多顿迫近的预报](../Page/哈米吉多顿.md "wikilink")。1970年代其他全球政治问题也已经在[圣经中被预言](../Page/圣经.md "wikilink")。Lindsey
暗示启示录中提到的七头十角的兽就是欧洲经济共同体（[欧盟的前身](../Page/欧盟.md "wikilink")），当时有10个成员国，现在已有27个成员国。

1995年，灾前被提的教义由于[黎曦庭的丛书](../Page/黎曦庭.md "wikilink")《[末日迷踪](../Page/末日迷踪.md "wikilink")》而进一步普及，这本书销售了上千万册，并被拍摄成几部影片。

今天，被提的教义仍然是[基督新教基要派末世论的重要组成部分](../Page/基督新教基要派.md "wikilink")。许多基督徒仍然感觉世界局势正趋向于被提、大灾难，基督很快就要回来。

## 圣经依据

被提教义的支持者普遍引用以下[新约经文作为依据](../Page/新约.md "wikilink")\[24\]：

  - “看哪，我把一个奥秘告诉你们：我们不是都要睡觉，乃是都要改变，就是在一刹那，眨眼之间，末次号筒的时候；因号筒要响，死人要复活，成为不朽坏的，我们也要改变。
    ”\[25\]
  - “我们现在凭着主的话，告诉你们这件事，就是我们这些活着还存留到主来临的人，绝不能在那已经睡了的人之先；因为主必亲自从天降临，有1发令的呼叫，有天使长的声音，又有神的号声，那在基督里死了的人必先复活，然后我们这些活着还存留的人，必同时与他们一起被提到云里，在空中与主相会；这样，我们就要和主常常同在。”\[26\]
  - “祂要按着祂那甚至能叫万有归服自己的动力，将我们这卑贱的身体改变形状，使之同形于祂荣耀的身体。”\[27\]
  - “人子显现的日子，也要这样。当那日，人在房顶上，器具在屋子里，不要下来拿；在田地里的，也照样不要回去。你们要回想罗得的妻子。凡想要保全魂生命的，必丧失魂生命；凡丧失魂生命的，必使魂生命得以存活。我告诉你们，当那一夜，两个人在一个床上，要取去一个，撇下一个。两个女人在一处推磨，要取去一个，撇下一个。两个人在田里，要取去一个，撇下一个。
    ”\[28\]

## 被提时间的主要观点

  - 災前被提论
  - 灾中被提论
  - 怒前被提
  - 部分被提
  - 灾后被提

## 参考文献

## 外部链接

  -
  - 關於[被提](https://www.figprayer.com/category/be-raptured)

  - [被提](https://www.expecthim.com/be-true.html)的真意

## 参见

  - [启示录](../Page/启示录.md "wikilink")、[圣经预言](../Page/圣经预言.md "wikilink")
  - [基督教末世論](../Page/基督教末世論.md "wikilink")、[时代论](../Page/时代论.md "wikilink")
  - [敌基督](../Page/敌基督.md "wikilink")、[兽名数目](../Page/兽名数目.md "wikilink")
  - [大灾难](../Page/大灾难.md "wikilink")、[哈米吉多顿](../Page/哈米吉多顿.md "wikilink")
  - [耶稣再临](../Page/耶稣再临.md "wikilink")、[末日审判](../Page/末日审判.md "wikilink")
  - [得救](../Page/得救.md "wikilink")、[得胜](../Page/得胜.md "wikilink")、[重生](../Page/重生_\(基督教\).md "wikilink")、[永生](../Page/永生.md "wikilink")
  - [天国](../Page/天国.md "wikilink")、[新天新地](../Page/新天新地.md "wikilink")、[新耶路撒冷](../Page/新耶路撒冷.md "wikilink")

{{-}}

[Category:啟示錄中的事件](../Category/啟示錄中的事件.md "wikilink")
[Category:基督教术语](../Category/基督教术语.md "wikilink")
[Category:基督教新教相关争议](../Category/基督教新教相关争议.md "wikilink")

1.

2.

3.

4.  Cf. Michael D. Guinan, "Raptured or Not? A Catholic Understanding",
    *Catholic Update*, October 2005,
    <https://web.archive.org/web/20140404105238/http://www.americancatholic.org/Newsletters/CU/ac1005.asp>
    ("For many *American* fundamentalist Christians, the Rapture forms
    part of the scenario of events that will happen at the end of the
    world....\[T\]he more common view is \[the pre-tribulation view\].")
    (emphasis added); (*American* Anglican commentary), Comment of Jon
    Edwards ("\[T\]he word 'rapture' can be found before 1830. But
    before 1830 it always referred to a POST-TRIB rapture....").

5.  Michael D. Guinan, "Raptured or Not? A Catholic Understanding",
    *Catholic Update*, October 2005,
    <https://web.archive.org/web/20140404105238/http://www.americancatholic.org/Newsletters/CU/ac1005.asp>
    ("But what do we mean by 'the Rapture'? The word can be used in
    different ways. Spiritual writers have used it for mystical union
    with God, or our final sharing in God’s heavenly life. This is not
    the sense we are using it in here; we are using it in a much more
    specific way. For many American fundamentalist Christians, the
    Rapture forms part of the scenario of events that will happen at the
    end of the world....\[T\]he more common view is \[the
    pre-tribulation view\].") (Roman Catholic commentary).

6.
7.  See, for example, Michael D. Guinan, "Raptured or Not? A Catholic
    Understanding", *Catholic Update*, October 2005,
    <https://web.archive.org/web/20140404105238/http://www.americancatholic.org/Newsletters/CU/ac1005.asp>
    ("The word can be used in different ways. Spiritual writers have
    used it for mystical union with God, or our final sharing in God’s
    heavenly life. This is not the sense we are using it in here. . . .
    'What is the Catholic teaching on the Rapture?' It was over 30 years
    ago that a student in my Scripture class asked me that question.
    Drawing on all my years of Catholic education (kindergarten through
    the seminary and doctoral studies), I replied, 'The what?' I had
    never heard of it.").

8.  Michael D. Guinan, "Raptured or Not? A Catholic Understanding",
    *Catholic Update*, October 2005,
    <https://web.archive.org/web/20140404105238/http://www.americancatholic.org/Newsletters/CU/ac1005.asp>
    . Cf. ["Catechism of the Catholic Church - The Profession of
    Faith"](http://www.vatican.va/archive/ccc_css/archive/catechism/p1s2c2a7.htm).
    [Vatican](../Page/Holy_See.md "wikilink").va. Retrieved 2011-10-21.

9.  Anthony M. Coniaris, "The Rapture: Why the Orthodox don't preach
    it," Light & Life Publishing, Life Line, September 12, 2005, Volume
    2, Issue 3, available at
    <https://web.archive.org/web/20121109035607/http://www.light-n-life.com/newsletters/09-12-2005.htm>
    ("As already stated, most Christians, Orthodox, Roman Catholics and
    Protestants do not believe in the Rapture.") (Orthodox commentary),
    last accessed January 27, 2012.

10. Brian M. Schwertley, "Is the Pretribulation Rapture Biblical?",
    Reformed Online,
    <https://web.archive.org/web/20130311041013/http://reformedonline.com/view/reformedonline/rapture.htm>,
    last accessed January 27, 2012.

11. See notes above for specific denominations (Catechism - Catholic,
    Light & Life Newsletter - Orthodox, Lutheran Witness - Lutheran,
    Reformed Online - Reformed).

12. Cf. Ian S. Markham, "John Darby", The Student's Companion to the
    Theologians, p.263-64 (Wiley-Blackwell, 2013) ("\[Darby\]
    simultaneously created a theology that holds the popular imagination
    and was popularized very effectively in the margins of the Schofield
    Bible."), <http://books.google.com/books?id=h6SHSAjeCrYC> .

13. Carl E. Olson, "Five Myths About the Rapture," Crisis p. 28-33
    (Morley Publishing Group, 2003) ("LaHaye declares, in Rapture Under
    Attack, that “virtually all Christians who take the Bible literally
    expect to be raptured before the Lord comes in power to this earth.”
    This would have been news to Christians — both Catholic and
    Protestant — living prior to the 18th century, since the concept of
    a pretribulation Rapture was unheard of prior to that time. Vague
    notions had been considered by the Puritan preachers Increase
    (1639-1723) and Cotton Mather (1663-1728), and the late 18th-century
    Baptist minister Morgan Edwards, but it was John Nelson Darby who
    solidified the belief in the 1830s and placed it into a larger
    theological framework."). Reprinted at
    <http://www.catholicculture.org/culture/library/view.cfm?recnum=5788>
    .

14.

15. *The Scofield Bible: Its History and Impact on the Evangelical
    Church*, Magnum & Sweetnam. Pages 188-195, 218.

16. Ephraem the Syrian, JoshuaNet, 27 Jul. 2010.
    <http://joshuanet.org/articles/ephraem1.htm> & © 1995 Grant R.
    Jeffrey, Final Warning, published by Frontier Research Publications,
    Inc., Box 120, Station "U", Toronto, Ontario M8Z 5M4

17. <http://www.aroundomaha.com/sschool/rapture.html>

18.

19.

20.

21. <http://www.jesus-is-savior.com/Books,%20Tracts%20&%20Preaching/Printed%20Books/JIC/jic-intro.htm>

22. <http://www.bibleprophesy.org/clement.htm>

23.

24.

25. [哥林多前书](../Page/哥林多前书.md "wikilink") 15章51–52节

26. [帖撒罗尼迦前书](../Page/帖撒罗尼迦前书.md "wikilink")4章15–17节

27. [腓立比书](../Page/腓立比书.md "wikilink") 3章21节

28. 路加福音17章30-35节