[Polkadotskirt.jpg](https://zh.wikipedia.org/wiki/File:Polkadotskirt.jpg "fig:Polkadotskirt.jpg")
[The_Evolution_of_the_Skirt_(1916).webm](https://zh.wikipedia.org/wiki/File:The_Evolution_of_the_Skirt_\(1916\).webm "fig:The_Evolution_of_the_Skirt_(1916).webm")
**裙**，俗稱**裙子**，為覆蓋腰身以下，筒形或圓錐形的[衣服](../Page/衣服.md "wikilink")。與[褲不同](../Page/褲.md "wikilink")，襯裙並不開叉，雙腿不用分別穿套（然而，有人把在胯下分開的馬褲套進裙子）。在許多文化中，基於線條搭配審美觀念，以至後來普遍[男性認為穿裙對于行動和工作不甚方便](../Page/男性.md "wikilink")，裙因此常被視為[女性的衣著](../Page/女性.md "wikilink")，但也有例外。例如[蘇格蘭裙](../Page/蘇格蘭裙.md "wikilink")（*kilt*）被視為蘇格蘭的傳統[男性衣著](../Page/男性.md "wikilink")，並逐漸成為世界各地的時尚。
[Minirock_(Lack)_Photo_Model_2.jpg](https://zh.wikipedia.org/wiki/File:Minirock_\(Lack\)_Photo_Model_2.jpg "fig:Minirock_(Lack)_Photo_Model_2.jpg")
[孔府藏妝花織金藍緞裙.jpg](https://zh.wikipedia.org/wiki/File:孔府藏妝花織金藍緞裙.jpg "fig:孔府藏妝花織金藍緞裙.jpg")

## 裙子的历史

[裙子的历史在](../Page/裙子的历史.md "wikilink")[女性的](../Page/女性.md "wikilink")[衣服中是最古老的](../Page/衣服.md "wikilink")。从古埃及时就有，开始是把布缠绕在身上或缝成单纯的筒形，到了中世纪，又设计出了应用省缝和喇叭形的裁剪法。由此可见，裙子的制作技术有了显著的进步，并且为以后的发展奠定了牢固的基础。

随着服装整体的装饰化，到了16世纪中期，又有一部份人使用了[裙环](../Page/裙环.md "wikilink")（hooP，为裙子造型用的一种衬裙），把它放入裙子的里面，使裙子的造型有了膨胀感，在裙子的历史中，被认为最具有豪华与装饰性的是18世纪的[洛可可时代](../Page/洛可可.md "wikilink")。从那时以后，由于[法国大革命的爆发](../Page/法国大革命.md "wikilink")，被加以夸张的裙子也一时消失，再次又流行了把硬衬布衬裙（crinoIine，用马尾和麻的混纺布制作的[衬裙](../Page/衬裙.md "wikilink")）放入裙内的[裙子](../Page/克里諾林裙襯.md "wikilink")，19世纪末期，又出现了在臀部放入后腰垫（bustle）的[裙子](../Page/巴斯爾裙襯.md "wikilink")。

20世纪后，由于[第一次世界大战的发生](../Page/第一次世界大战.md "wikilink")，伴随女性加入[社会生活的同时](../Page/社会生活.md "wikilink")，裙子也变为易于活动的短裙形。第二次世界大战后，像[长裙](../Page/长裙.md "wikilink")、[超短裙等](../Page/超短裙.md "wikilink")，根据流行出现了各式裙形，直至如今。

## 裙子的分類

[Wongwt_官也街_(17284915092).jpg](https://zh.wikipedia.org/wiki/File:Wongwt_官也街_\(17284915092\).jpg "fig:Wongwt_官也街_(17284915092).jpg")\]\]
按裙腰在腰节线的位置区分，有中腰裙、低腰裙、高腰裙；按裙长区分，有长裙(裙摆至胫中以下)、中裙（裙摆至膝以下、胫中以上)、短裙（裙摆至膝以上）和超短裙（裙摆仅及大腿中部）；按裙体外形轮廓区分，可分为**统裙、斜裙、缠绕裙**三大类。

广义的裙子还包括[衬裙](../Page/衬裙.md "wikilink")，[連身裙](../Page/連身裙.md "wikilink")（連衣裙）、[褲裙](../Page/褲裙.md "wikilink")。

### 统裙

[Plaid_Kilt_Nontraditional.jpg](https://zh.wikipedia.org/wiki/File:Plaid_Kilt_Nontraditional.jpg "fig:Plaid_Kilt_Nontraditional.jpg")
从裙腰开始自然垂落的筒状或管状裙。又称筒裙、直裙、直统裙。常见的有[旗袍裙](../Page/旗袍裙.md "wikilink")、[西装裙](../Page/西装裙.md "wikilink")、[夹克裙](../Page/夹克裙.md "wikilink")、[围裹裙](../Page/围裹裙.md "wikilink")、[鉛筆裙等](../Page/鉛筆裙.md "wikilink")。

  - 旗袍裙。左右侧缝开衩。因造型与[旗袍中腰以下部分相同而得名](../Page/旗袍.md "wikilink")。多选用[丝绸](../Page/丝绸.md "wikilink")、丝绒、锦缎、羊毛绒等面料裁制。
  - 西装裙。又稱窄裙、一步裙，通常采用收颡、打褶等方法使裙体合身。因与[西装上衣配套穿着而得名](../Page/西装.md "wikilink")。多选用呢、绒、化纤混纺织物和针织面料裁制。
  - 夹克裙注重拼缝装饰，在缝合处缉明线，有横插袋或明贴袋，后裙摆开衩或前中缝开门，也可采用暗褶。因与夹克衫的装饰特点相近而得名。多以坚固呢、小帆布等比较厚实的面料裁制。
  - 围裹裙。从裙腰至摆开口的裙片 ，通常在前身交叠，以纽带系合。因围裹式穿着而得名。面料不限。也可不用纽带，围裹下体后将余幅塞入裙腰。

### 斜裙

由腰部至下摆斜向展开略呈A字形的裙。多用[棉布](../Page/棉布.md "wikilink")、[丝绸](../Page/丝绸.md "wikilink")、薄呢料和[化纤织物等裁制](../Page/化纤.md "wikilink")。按裙型构成可分为单片斜裙和多片斜裙。单片斜裙又称圆台裙。是将一块幅宽与长度等同的面料，在中央挖剪出腰围洞的裙，宜选用软薄面料裁制。多片斜裙由两片以上的扇形面料纵向拼接构成。通常以片数命名，有两片斜裙、4片斜裙、16片斜裙等。

  - 钟形裙。外形似钟的裙。腰部常以褶饰使裙体蓬起，内加衬里或亚麻布质的衬裙。
  - 喇叭裙。裙体上部与腰臀紧密贴附，由臀线斜向下展开，形似喇叭状。
  - 超短裙。又稱[迷你裙](../Page/迷你裙.md "wikilink")，指長度較短不過膝，僅能遮蓋住一部份大腿甚至僅遮蓋臀部的裙子。
  - 褶裙。有定型褶的裙。通常采用可塑性高的面料，加热压出褶形。有[百褶裙](../Page/百褶裙.md "wikilink")、褶裥裙等。百褶裙的裙体为等宽一边倒的明褶和暗褶。褶裥裙通常在臀围以上部位为收拢缉缝的裥，臀围线以下为烫出的活褶。褶裥裙的褶裥一般比百褶裙宽，并富于变化。
  - 节裙。又称塔裙、蛋糕裙。裙体以多层次的横向多片剪接，外形如同塔状。通常为曳地长裙，每节裙片抽碎褶，产生波浪效果。19世纪初盛行于欧洲皇室，多用于隆重的社交场合。现已将节裙改短，便于日常穿着。

### 缠绕裙

用布料缠绕躯干和腿部，用立体裁剪法裁制的裙。因缠绕方法不一，裙式也多种多样。缠绕裙常作为晚礼服，当人体动作时，裙体绉褶的光影效果给人以韵律美感。

## 男裙

在[西方文明里](../Page/西方世界.md "wikilink")，裙子，连衣裙与其他类似的服装通常被被认为是女装，虽然历史上并不是这样。虽然，一些设计师如[讓-保羅·高緹耶设计了](../Page/讓-保羅·高緹耶.md "wikilink")[男裙](../Page/男裙.md "wikilink")，[苏格兰裙在一些场合下被广泛接受](../Page/苏格兰裙.md "wikilink")，男士着裙的现象一般在[变装的时候才能看到](../Page/变装.md "wikilink")。如今，男性穿裙的風氣也逐漸流行。

## 相關條目

  - [裙襬指數](../Page/裙襬指數.md "wikilink")
  - [安全裤](../Page/安全裤.md "wikilink")

## 参考资料

  - 《中国大百科全书》
  - [Batik Kaftan](http://www.kaftansale.com/)

[Category:裙子](../Category/裙子.md "wikilink")