**裴勇浚**\[1\]\[2\]（，），[韓國](../Page/韓國.md "wikilink")[男演員和](../Page/男演員.md "wikilink")[Keyeast經紀公司創辦人](../Page/Keyeast.md "wikilink")，名字常被誤寫為**裴勇俊**。1994年11月1日，參加[KBS](../Page/KBS.md "wikilink")
Audition而出道。2015年5月14日，根據韓國經紀公司KEYEAST宣布，演員兼社長的裴勇俊和旗下女藝人[朴秀真從](../Page/朴秀真.md "wikilink")2015年2月開始互有好感，逐漸發展成戀人，並暫定於2015年秋季舉行婚禮。\[3\]其後更於2015年7月27日(一)在韓國首爾華克山莊舉行非公開婚禮，並否認是奉子成婚。\[4\]2016年4月29日，二人透過經紀公司公開朴秀珍懷孕的消息。\[5\]

## 演出作品

### 電視劇

  - 1994年：[KBS](../Page/韓國放送公社.md "wikilink")《[愛的問候](../Page/愛的問候.md "wikilink")》飾演
    金勇民
  - 1995年：KBS《[年輕人的陽地](../Page/年輕人的陽地.md "wikilink")》
  - 1995年：KBS《[离别的六阶段](../Page/离别的六阶段.md "wikilink")》
  - 1995年：PSB《[海風](../Page/海風.md "wikilink")》
  - 1996年：KBS《[爸爸](../Page/爸爸_\(電視劇\).md "wikilink")》
  - 1996年：KBS《[初戀](../Page/初戀_\(1996年電視劇\).md "wikilink")》飾演 成燦宇
  - 1998年：KBS《[赤足青春](../Page/赤足青春.md "wikilink")》
  - 1999年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[我們真的愛過嗎](../Page/我們真的愛過嗎.md "wikilink")》
  - 2001年：MBC《[情定大飯店](../Page/情定大飯店.md "wikilink")》飾演 申東賢
  - 2002年：KBS《[冬季戀歌](../Page/冬季戀歌.md "wikilink")》飾演 李民亨
  - 2007年：MBC《[太王四神記](../Page/太王四神記.md "wikilink")》飾演 桓雄、谈德
  - 2007年：[EX](../Page/朝日電視台.md "wikilink")《[情定大飯店](../Page/情定大飯店_\(日本電視劇\).md "wikilink")》（客串）
  - 2011年：KBS《[Dream High](../Page/Dream_High.md "wikilink")》飾演 鄭夏明（客串）

### 電影

  - 1995年：《[畢九](../Page/畢九.md "wikilink")》
  - 2003年：《[醜聞-朝鮮男女相悅之事](../Page/醜聞-朝鮮男女相悅之事.md "wikilink")》
  - 2005年：《[外出](../Page/四月的雪.md "wikilink")》

## 書籍

  - 2010年《-{zh-hant:探尋韓國之美的旅程;zh-hans:寻找韩国之美的旅行}-》，譯者：葛增娜，出版社：布克文化，ISBN
    9789866278099。

## 評價

  - 《[冬季戀歌](../Page/冬季戀歌.md "wikilink")》在[台灣播出後](../Page/台灣.md "wikilink")，裴勇浚在台灣被稱為“師奶殺手”。也有某部分的人暱稱其為“勇樣”（源於日文漢字）

## 公益宣傳

  - 2009年12月將為聯合國氣候條約「Seal the
    Deal」公益宣傳視頻做配音，向世人宣揚氣候變化對地球影響的嚴重性，並呼籲人類應拯救地球的未來。

## 爭議事件

於2010年播出的日本動畫《[學園默示錄](../Page/學園默示錄.md "wikilink")》（HIGHSCHOOL OF THE
DEAD）第五集，有一隻殭屍長得像裴勇浚，就算變成殭屍仍然保持著眼鏡和笑容，被南里香譏諷為「一副討厭的[偽娘相](../Page/偽娘.md "wikilink")」，並且成為第一位被南里香狙殺的殭屍。播出後引來很大的爭議，甚至有韓國粉絲要求電視台停播。\[6\]\[7\]\[8\]

## 注釋

## 外部链接

  -
  - [裴勇浚官方網頁](http://www.byj.co.kr)

  - [裴勇浚 Profile & Photo
    Gallery](https://web.archive.org/web/20070115040026/http://gbreak.com/index/Bae_Yong_Jun)

  -
[B](../Category/韩国電視演员.md "wikilink")
[B](../Category/韓國電影演員.md "wikilink")
[B](../Category/韩国男性模特儿.md "wikilink")
[Category:韓國天主教徒](../Category/韓國天主教徒.md "wikilink")
[B](../Category/成均館大學校友.md "wikilink")
[B](../Category/首爾特別市出身人物.md "wikilink")
[B](../Category/裴姓.md "wikilink") [B](../Category/韓國男演員.md "wikilink")
[B](../Category/1972年出生.md "wikilink")

1.  [裴勇浚簽名「裴勇浚」答謝
    書寫工整盡顯誠意](http://ent.sina.com.cn/s/j/2004-03-09/1331325910.html)
2.  「裴」字在[韓語漢字應作](../Page/韓語漢字.md "wikilink")「-{裵}-」，但在中文及[日語中均被視為](../Page/日語.md "wikilink")[異體字](../Page/異體字.md "wikilink")。
3.  [師奶心碎
    裴勇俊朴秀真要結婚了](http://www.cna.com.tw/news/firstnews/201505145011-1.aspx)，中央社，2015年5月14日
4.  [裴勇俊公開與朴秀珍婚紗照
    難掩喜悅](http://www.cna.com.tw/news/firstnews/201507270199-1.aspx)
5.  [裴勇俊、朴秀珍結婚9個就出現「第三者」？！](http://www.vlovekpop.com/20160502-bae-park/)
    vlovekpop
6.  [Yahoo新聞—裵勇浚無啦啦變日本動畫喪屍](http://hk.news.yahoo.com/article/100803/18/jhvi.html)
7.  [自由時報—日動畫獵殺裵勇浚 挨轟 學園默示錄惡搞
    韓流天王慘變微笑殭屍](http://www.libertytimes.com.tw/2010/new/aug/4/today-show3.htm)

8.  [日動畫醜化裵帥
    粉絲串連抵制](http://www.worldjournal.com/view/full_entertainment/8994136/article-%E6%97%A5%E5%8B%95%E7%95%AB%E9%86%9C%E5%8C%96%E8%A3%B4%E5%B8%A5-%E7%B2%89%E7%B5%B2%E4%B8%B2%E9%80%A3%E6%8A%B5%E5%88%B6?instance=en_bull_jk)