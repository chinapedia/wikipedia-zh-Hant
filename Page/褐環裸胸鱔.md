**褐環裸胸鱔**，又名**黑環裸胸鯙**，俗名薯鰻、錢鰻，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鰻鱺目](../Page/鰻鱺目.md "wikilink")[鯙亞目](../Page/鯙亞目.md "wikilink")[鯙科的其中一個](../Page/鯙科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚僅分布於西[太平洋海域](../Page/太平洋.md "wikilink")，包括[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[琉球群島](../Page/琉球群島.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律賓等](../Page/菲律賓.md "wikilink")。

## 深度

水深5至50公尺。

## 特徵

本魚體側橫帶成明顯之暗褐色並具白邊，各橫帶之背腹相連形成完整之環狀，其共有13條橫帶，環狀橫帶之間及頭部具有黑點，與[網紋裸胸鱔相似](../Page/網紋裸胸鱔.md "wikilink")，但本種體高較低，頭部也較短。具有銳利的牙齒，需小心咬傷，體長可達60公分以上。

## 生態

主要棲息在珊瑚礁區間之洞穴或礁石與沙地所形成之縫隙中，以[魚類](../Page/魚類.md "wikilink")、[頭足類為食](../Page/頭足類.md "wikilink")。

## 經濟利用

[食用魚](../Page/食用魚.md "wikilink")，味道鮮美，也可作為[觀賞魚](../Page/觀賞魚.md "wikilink")。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

  -
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[chlamydatus](../Category/裸胸鱔屬.md "wikilink")