**褚亮**（），[字希明](../Page/表字.md "wikilink")，[杭州](../Page/杭州_\(古代\).md "wikilink")[錢塘人](../Page/钱塘县.md "wikilink")，祖籍[河南](../Page/河南郡.md "wikilink")[阳翟](../Page/阳翟县.md "wikilink")，唐初十八学士之一。

少年博覽群書，能文，喜交遊名士。[南朝陈時](../Page/南朝陈.md "wikilink")，任[尚書](../Page/尚書.md "wikilink")[殿中侍郎](../Page/殿中侍郎.md "wikilink")。入[隋為東宮學士](../Page/隋朝.md "wikilink")、[太常博士](../Page/太常.md "wikilink")。[隋煬帝嫉其才](../Page/隋煬帝.md "wikilink")，誣賴他與[楊玄感有牽連](../Page/楊玄感.md "wikilink")，貶為西海郡[司戶](../Page/司戶.md "wikilink")。[大業十三年](../Page/大业_\(年号\).md "wikilink")（617年），金城校尉[薛舉稱王](../Page/薛舉.md "wikilink")，任褚亮為[黃門侍郎](../Page/黃門侍郎.md "wikilink")。大業十四年，秦王[李世民滅薛舉之子](../Page/李世民.md "wikilink")[薛仁杲](../Page/薛仁杲.md "wikilink")，以褚亮為鎧曹參軍。後入秦王府文學館，任文學館學士，史载“凡分三番逆宿于阁下，悉给珍膳。每暇日，访以政事，讨论故籍，榷略前载，无常礼之间”，“太宗每有征伐亮常侍从，军中宴筵，必预欢赏，从容讽议，多所俾益”\[1\]。

[貞觀元年](../Page/貞觀_\(唐朝\).md "wikilink")（627年），與[杜如晦等十八人授](../Page/杜如晦.md "wikilink")[弘文館學士](../Page/弘文館.md "wikilink")，被封为封陽翟[縣男](../Page/縣男.md "wikilink")，褚亮支持唐太宗拓疆政策，命子[褚遂良從軍](../Page/褚遂良.md "wikilink")，出兵[突厥](../Page/突厥.md "wikilink")。累遷至通直[散騎常侍](../Page/散騎常侍.md "wikilink")，十六年，进爵为陽翟縣侯。后[致仕归家](../Page/致仕.md "wikilink")。貞觀二十一年（647年）卒，年八十八。[谥号曰康](../Page/谥号.md "wikilink")，赠太常卿，陪葬昭陵。

## 家世

  - 高祖：[褚炫](../Page/褚炫.md "wikilink")，宋[升明初与](../Page/升明.md "wikilink")[谢朏](../Page/谢朏.md "wikilink")、[江敩](../Page/江敩.md "wikilink")、[刘俣](../Page/刘俣.md "wikilink")，人称“四友”。官至散骑常侍、侍中，齐东阳太守，复为侍中、吏部尚书，赠太常卿，谥贞子。
  - 曾祖：[褚湮](../Page/褚湮.md "wikilink")，梁御史中丞。
  - 祖父：[褚蒙](../Page/褚蒙.md "wikilink")，梁仪同、庐陵王东-{閣}-祭酒，太子中舍人。
  - 父亲：[褚玠](../Page/褚玠.md "wikilink")，陈秘书监，谥号恭公。
  - 夫人：[柳氏](../Page/柳氏.md "wikilink")
      - 长子：[褚遂贤](../Page/褚遂贤.md "wikilink")，普州长史、袭封阳翟侯。
          - 孙：[褚兼善](../Page/褚兼善.md "wikilink")，
          - 孙：[褚兼爱](../Page/褚兼爱.md "wikilink")，
          - 孙：[褚兼艺](../Page/褚兼艺.md "wikilink")，永州司功。
      - 次子：[褚遂良](../Page/褚遂良.md "wikilink")，高宗时宰相。
      - 女：褚氏，嫁王義童。\[2\]

## 褚亮碑

唐太宗[昭陵陪葬碑铭之一](../Page/唐昭陵.md "wikilink")，原存于陕西[醴泉县烟霞乡上严峪村东南约](../Page/醴泉县.md "wikilink")200米处褚亮墓前，1975年迁移入昭陵博物馆。碑身高2.98米，下宽1.10米，厚0.39米。碑额题《大唐褚卿之碑》六字。[篆书兼隶](../Page/篆书.md "wikilink")，传为[唐太宗所书](../Page/唐太宗.md "wikilink")。碑文为[殷仲容书书](../Page/殷仲容.md "wikilink")，隶书36行，满行65字，碑上截部分字迹尚存，余均磨灭不见。此碑书法精工，金石家誉为“唐石最嘉者”（《庚子销夏录》）。《[全唐文](../Page/全唐文.md "wikilink")》有著录。

## 注釋

[Category:南陈侍郎](../Category/南陈侍郎.md "wikilink")
[Category:南陈太子洗马](../Category/南陈太子洗马.md "wikilink")
[Category:隋朝学士](../Category/隋朝学士.md "wikilink")
[Category:隋朝太常博士](../Category/隋朝太常博士.md "wikilink")
[Category:唐朝学士](../Category/唐朝学士.md "wikilink")
[Category:唐朝散骑常侍](../Category/唐朝散骑常侍.md "wikilink")
[Category:唐朝县侯](../Category/唐朝县侯.md "wikilink")
[Category:钱塘人](../Category/钱塘人.md "wikilink")
[L](../Category/河南褚氏.md "wikilink")

1.  《[新唐書](../Page/新唐書.md "wikilink")·褚亮傳》
2.  唐恆州刺史建昌公王公神𨕥碑：「夫人陽翟縣君河南褚氏，即太常卿陽翟康矦亮之女，中書令河南郡公遂良之妹也。」