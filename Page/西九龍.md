[West_kowloon.PNG](https://zh.wikipedia.org/wiki/File:West_kowloon.PNG "fig:West_kowloon.PNG")
[West_Kowloon.jpg](https://zh.wikipedia.org/wiki/File:West_Kowloon.jpg "fig:West_Kowloon.jpg")遠眺西九龍填海區
(2004年)\]\]
[West_Kowloon_Aerial_view_201811.jpg](https://zh.wikipedia.org/wiki/File:West_Kowloon_Aerial_view_201811.jpg "fig:West_Kowloon_Aerial_view_201811.jpg")遠眺西九龍填海區
(2018年)\]\]
[Langham_place_hotel_birdview.jpg](https://zh.wikipedia.org/wiki/File:Langham_place_hotel_birdview.jpg "fig:Langham_place_hotel_birdview.jpg")、[葵青貨櫃碼頭和](../Page/葵青貨櫃碼頭.md "wikilink")[荃灣新市鎮一帶景致](../Page/荃灣新市鎮.md "wikilink")(2004年)\]\]
[West_Kowloon_Cultural_District_201810.jpg](https://zh.wikipedia.org/wiki/File:West_Kowloon_Cultural_District_201810.jpg "fig:West_Kowloon_Cultural_District_201810.jpg")望向西九龍\]\]
[HK_1985.jpg](https://zh.wikipedia.org/wiki/File:HK_1985.jpg "fig:HK_1985.jpg")
[West_Kowloon_near_Cheung_Sha_Wan_2018.jpg](https://zh.wikipedia.org/wiki/File:West_Kowloon_near_Cheung_Sha_Wan_2018.jpg "fig:West_Kowloon_near_Cheung_Sha_Wan_2018.jpg")（左）和[富昌邨](../Page/富昌邨.md "wikilink")（右）\]\]
[Union_Square_Overview_201008.jpg](https://zh.wikipedia.org/wiki/File:Union_Square_Overview_201008.jpg "fig:Union_Square_Overview_201008.jpg")上蓋物業「[Union
Square](../Page/Union_Square.md "wikilink")」；圖左起為[擎天半島](../Page/擎天半島.md "wikilink")、[天璽](../Page/天璽_\(香港\).md "wikilink")、[環球貿易廣場](../Page/環球貿易廣場.md "wikilink")、[君臨天下](../Page/君臨天下.md "wikilink")、[凱旋門](../Page/凱旋門_\(香港\).md "wikilink")\]\]
[The_Long_Beach_GF_Open_Space_2012.jpg](https://zh.wikipedia.org/wiki/File:The_Long_Beach_GF_Open_Space_2012.jpg "fig:The_Long_Beach_GF_Open_Space_2012.jpg")與[浪澄灣住宅之間設公共休憩空間](../Page/浪澄灣.md "wikilink")\]\]

**西九龍**（，亦作），顧名思義就是指[香港](../Page/香港.md "wikilink")[九龍半島的西面](../Page/九龍半島.md "wikilink")，地域從北至南由[昂船洲伸延至](../Page/昂船洲.md "wikilink")[渡船角附近](../Page/渡船角.md "wikilink")，除了昂船洲外，其餘土地均是透過[填海得來](../Page/香港填海造地.md "wikilink")，總面積達334[公頃](../Page/公頃.md "wikilink")，因此西九龍很多時候又被稱為**西九龍填海區**，也常被簡稱**西九**；在[十八區地區行政上橫跨](../Page/香港十八區.md "wikilink")[油尖旺區及](../Page/油尖旺區.md "wikilink")[深水埗區](../Page/深水埗區.md "wikilink")；填海計劃名為《**西九龍填海計劃**》，是《[香港機場核心計劃](../Page/香港機場核心計劃.md "wikilink")》中的十項工程之一。

## 地區

西九龍大致可分為[昂船洲](../Page/昂船洲.md "wikilink")、[長沙灣西及](../Page/長沙灣.md "wikilink")[深水埗西](../Page/深水埗.md "wikilink")（南昌站地區）、[大角咀西及](../Page/大角咀.md "wikilink")[旺角西](../Page/旺角.md "wikilink")（奧運站地區）及[油麻地西](../Page/油麻地.md "wikilink")（九龍站地區）。貫穿西九龍的主要道路包括[西九龍公路](../Page/西九龍公路.md "wikilink")、[深旺道](../Page/深旺道.md "wikilink")、[連翔道](../Page/連翔道.md "wikilink")，[西區海底隧道九龍出口位於西九龍盡頭](../Page/西區海底隧道.md "wikilink")；鐵路則有[東涌綫](../Page/東涌綫.md "wikilink")、[機場快綫](../Page/機場快綫.md "wikilink")、[西鐵綫及](../Page/西鐵綫.md "wikilink")[廣深港高速鐵路香港段](../Page/廣深港高速鐵路香港段.md "wikilink")。

### 昂船洲

[昂船洲未與](../Page/昂船洲.md "wikilink")[九龍半島併合前](../Page/九龍半島.md "wikilink")，是[駐港英軍的](../Page/駐港英軍.md "wikilink")[海軍總部](../Page/海軍.md "wikilink")。併合後，原海軍總部的南面成為[駐港解放軍的海軍基地](../Page/駐港解放軍.md "wikilink")，北面則成為8號[貨櫃碼頭](../Page/葵涌貨櫃碼頭.md "wikilink")。另外已通車的[八號幹線亦包括昂船洲段](../Page/八號幹線.md "wikilink")，其中[昂船洲大橋連接着](../Page/昂船洲大橋.md "wikilink")[青衣和西九龍](../Page/青衣.md "wikilink")，方便來往[香港國際機場及](../Page/香港國際機場.md "wikilink")[新界西北](../Page/新界.md "wikilink")。

### 深水埗西

[深水埗西](../Page/深水埗.md "wikilink")（）泛指[深盛路至](../Page/深盛路.md "wikilink")[聚魚道](../Page/聚魚道.md "wikilink")、[深旺道](../Page/深旺道.md "wikilink")、[海輝道的地區](../Page/海輝道.md "wikilink")，北面[深盛路](../Page/深盛路.md "wikilink")、[興華街西一帶在未填海時是幾間小船廠的所在地](../Page/興華街.md "wikilink")，在填海後建成了很多住宅，有[昇悅居](../Page/昇悅居.md "wikilink")、[泓景臺](../Page/泓景臺.md "wikilink")、[宇晴軒](../Page/宇晴軒.md "wikilink")、[碧海藍天](../Page/碧海藍天.md "wikilink")（即[地產代理所稱的](../Page/地產代理.md "wikilink")「西九四小龍」）和[海麗邨](../Page/海麗邨.md "wikilink")，被視為[長沙灣西](../Page/長沙灣.md "wikilink")；而小船廠及[九巴荔枝角巴士廠遷到](../Page/九巴.md "wikilink")[西九龍公路以西的](../Page/西九龍公路.md "wikilink")[興華街西沿線](../Page/興華街.md "wikilink")。南面[欽州街附近則有幾個](../Page/欽州街.md "wikilink")[公共屋邨](../Page/香港公共屋邨.md "wikilink")，例如[富昌邨和](../Page/富昌邨.md "wikilink")[南昌邨](../Page/南昌邨.md "wikilink")（當中南昌邨位於1977年深水埗第一期填海用地，而富昌邨則位於第二代[深水埗碼頭巴士總站部分](../Page/深水埗碼頭.md "wikilink")）。另外[南昌站一帶的土地多幅用地過往劃為](../Page/南昌站_\(香港\).md "wikilink")[長沙灣副食品市場預留用地](../Page/長沙灣副食品市場.md "wikilink")，近年香港政府陸續將用地改作公私營住宅發展，連同鄰近的西鐵南昌站[匯璽](../Page/匯璽.md "wikilink")，合共提供近1.27萬伙，可望在2019年以後陸續落成。

區內5個公私營住宅項目將動工發展，當中臨海的一幅私人住宅用地，可建樓面約98.8萬平方呎，估計足以興建近2,000個海景住宅，帶來大量高收入住戶。此外，毗鄰長沙灣副食品批發市場的一幅5.3萬平方呎用地，現時屬「綜合發展區」用途，將會作私人住宅和酒店發展。住宅地在2017年11月16日由信置牽頭的中港財團以172.88億元奪得，成為香港賣地史上最貴住宅地價紀錄。將擬建1,400伙單位和公眾能享用的海濱長廊。酒店地由新地以50.6億港元奪得，呎價高達1.35萬元，擬建975個酒店房。

至於連翔道3號、5號及6號地盤的兩個公屋項目[海盈邨及](../Page/海盈邨.md "wikilink")[海達邨](../Page/海達邨.md "wikilink")、居屋項目[凱樂苑](../Page/凱樂苑.md "wikilink")，以及位於海達邨對面的單幢居屋[凱德苑](../Page/凱德苑.md "wikilink")，將會帶來大量新增人口。\[1\]\[2\]

### 大角咀西及旺角西

[奧運站一帶填海區土地包括大角咀西及旺角西](../Page/奧運站.md "wikilink")，奧運站位於大角咀西的土地上，原址是前[大角咀碼頭及其附近海面](../Page/大角咀碼頭.md "wikilink")，而旺角西的原址為[油麻地避風塘一部分](../Page/油麻地避風塘.md "wikilink")。填海後，新油麻地避風塘向西移到新填海地區的海傍。該等土地興建了多個大型屋苑，包括大角咀西的[維港灣](../Page/維港灣.md "wikilink")、[凱帆軒](../Page/凱帆軒.md "wikilink")、[浪澄灣](../Page/浪澄灣.md "wikilink")、[瓏璽](../Page/瓏璽.md "wikilink")、[一號銀海和](../Page/一號銀海.md "wikilink")[君匯港](../Page/君匯港.md "wikilink")，以及旺角西的[富榮花園](../Page/富榮花園.md "wikilink")、[海富苑](../Page/海富苑.md "wikilink")、[柏景灣和](../Page/柏景灣.md "wikilink")[帝柏海灣等](../Page/帝柏海灣.md "wikilink")。由於奧運站鄰近[旺角中心地區](../Page/旺角.md "wikilink")，又因為[東涌綫的交通便利](../Page/東涌綫.md "wikilink")，[匯豐中心](../Page/匯豐中心.md "wikilink")、[中銀中心及](../Page/中銀中心.md "wikilink")[奧海城商場相繼落成](../Page/奧海城.md "wikilink")，使奧運站地區成為西九龍的主要商住地區。

### 油麻地西及渡船角周邊

發展商將[九龍站的發展計劃命名為](../Page/九龍站.md "wikilink")[Union
Square](../Page/Union_Square.md "wikilink")，分為7期發展。第1至4期是住宅項目：[漾日居](../Page/漾日居.md "wikilink")、[擎天半島](../Page/擎天半島.md "wikilink")、[凱旋門和](../Page/凱旋門_\(香港\).md "wikilink")[君臨天下](../Page/君臨天下.md "wikilink")。第5期至第7期是[商業大廈](../Page/商業大廈.md "wikilink")、[酒店](../Page/酒店.md "wikilink")、[服務式住宅及](../Page/服務式住宅.md "wikilink")[豪宅混合發展項目](../Page/豪宅.md "wikilink")。第5期為[圓方商場](../Page/圓方.md "wikilink")，第6期為[天璽及](../Page/天璽_\(香港\).md "wikilink")[W
Hotel](../Page/W_Hotel.md "wikilink")。而第7期[環球貿易廣場](../Page/環球貿易廣場.md "wikilink")，於2011年建成後成為香港最高的大廈。九龍站南面的新填海地區，政府發展成為[西九文化區](../Page/西九文化區.md "wikilink")，其中一部分土地被發展成臨時的[西九龍海濱長廊](../Page/西九龍海濱長廊.md "wikilink")。

此外[港鐵](../Page/港鐵.md "wikilink")[西鐵綫在九龍站東面](../Page/西鐵綫.md "wikilink")[渡船角近](../Page/渡船角.md "wikilink")[廣東道設立](../Page/廣東道.md "wikilink")[柯士甸站](../Page/柯士甸站.md "wikilink")，並發展上蓋物業[The
Austin及](../Page/The_Austin.md "wikilink")[Grand
Austin](../Page/Grand_Austin.md "wikilink")。2018年9月建成的[廣深港高速鐵路香港終點站](../Page/廣深港高速鐵路.md "wikilink")[香港西九龍站則在](../Page/香港西九龍站.md "wikilink")[柯士甸站與九龍站之間的土地設立](../Page/柯士甸站.md "wikilink")。

而渡船角北邊的[油麻地交匯處則正在興建](../Page/油麻地交匯處.md "wikilink")[中九龍幹線](../Page/中九龍幹線.md "wikilink")，並且開始研究加設上蓋起樓。\[3\]

<File:West> Kowloon Waterfront Promenade
200710-1.jpg|[西九龍海濱長廊](../Page/西九龍海濱長廊.md "wikilink")
<File:West> Kowloon Olympic Station Buildings
20110510.jpg|[奧運站一帶屋苑](../Page/奧運站.md "wikilink")
<File:Hoi> Fai Road Promenade night view
201510.jpg|[海輝道海濱花園](../Page/海輝道海濱花園.md "wikilink")
<File:Kowloon> Waterfront, Hong Kong, 2013-08-09, DD 03.jpg|位於[Union
Square的](../Page/Union_Square.md "wikilink")[九龍站建築群](../Page/九龍站.md "wikilink")
<File:Hong> Kong West Kowloon Station view
201810.jpg|[香港西九龍站](../Page/香港西九龍站.md "wikilink")

## 未來發展

預計西九龍發展區道路網絡於2031年不足應付交通需求，多個燈號控制交界處將會達至容車量極限，甚至超出交通負荷，以至出現車龍。2014年，[運輸及房屋局擬向立法會申請約](../Page/運輸及房屋局.md "wikilink")8億1千萬港元撥款以進行西九龍填海區的第一期道路改善工程，若果項目獲得[立法會交通事務委員會支持](../Page/立法會交通事務委員會.md "wikilink")，上述部門將於5月向[立法會工務小組委員會提交建議](../Page/立法會工務小組委員會.md "wikilink")，及於6月向[立法會財務委員會申請撥款](../Page/立法會財務委員會.md "wikilink")，預計於7月展開建造工程。項目包括興建多條高架單線行車道連接西九龍公路及附近路段，以應付西九龍填海區區區內各項發展項目，包括[西九文化區](../Page/西九文化區.md "wikilink")、[廣深港高速鐵路香港段](../Page/廣深港高速鐵路香港段.md "wikilink")[西九龍總站及](../Page/西九龍總站.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[柯士甸站上蓋物業等等所帶來的交通需求](../Page/柯士甸站.md "wikilink")，有關工程於2018年9月底竣工。\[4\]。

## 途經交通服務

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{東涌綫色彩}}">█</font>[東涌綫](../Page/東涌綫.md "wikilink")、<font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[南昌站](../Page/南昌站_\(香港\).md "wikilink")
  - <font color="{{東涌綫色彩}}">█</font>東涌綫：[奧運站](../Page/奧運站.md "wikilink")
  - <font color="{{東涌綫色彩}}">█</font>東涌綫、<font color={{機場快綫色彩}}>█</font>[機場快綫](../Page/機場快綫.md "wikilink")：[九龍站](../Page/九龍站_\(香港\).md "wikilink")
  - <font color="{{高速鐵路色彩}}">█</font>[廣深港高速鐵路香港段](../Page/廣深港高速鐵路香港段.md "wikilink")：[香港西九龍站](../Page/香港西九龍站.md "wikilink")
  - <font color="{{西鐵綫色彩}}">█</font>西鐵綫：[柯士甸站](../Page/柯士甸站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 參見

  - [九龍角填海計劃](../Page/九龍角.md "wikilink")：現已擱置
  - [西九文化區](../Page/西九文化區.md "wikilink")
  - [香港機場核心計劃](../Page/香港機場核心計劃.md "wikilink")
  - [昂船洲](../Page/昂船洲.md "wikilink")
  - [屏風樓](../Page/屏風樓.md "wikilink")
  - [九龍西選區](../Page/九龍西選區.md "wikilink")

## 鐵路車站

  - [南昌站](../Page/南昌站_\(香港\).md "wikilink")
  - [奧運站](../Page/奧運站.md "wikilink")
  - [九龍站](../Page/九龍站_\(港鐵\).md "wikilink")
  - [柯士甸站](../Page/柯士甸站.md "wikilink")
  - [香港西九龍站](../Page/香港西九龍站.md "wikilink")

## 參考

<references/>

## 外部連結

  - [香港地方介紹](http://www.hk-place.com/view.php?id=104)
  - [香港機場核心計劃網頁](https://web.archive.org/web/20051104005952/http://www.info.gov.hk/napco/)

{{-}}  {{-}}

[西九龍](../Category/西九龍.md "wikilink")
[Category:油尖旺區](../Category/油尖旺區.md "wikilink")
[Category:深水埗區](../Category/深水埗區.md "wikilink")
[Category:紅色公共小巴禁區](../Category/紅色公共小巴禁區.md "wikilink")

1.  長沙灣填海地 商業樓面75萬呎 余敏欽 《香港經濟日報》 2015-04-15
2.  [長沙灣地王出爐
    業主反價點算?](https://www.edigest.hk/property_market/%e9%95%b7%e6%b2%99%e7%81%a3%e5%9c%b0%e7%8e%8b%e5%87%ba%e7%88%90-%e6%a5%ad%e4%b8%bb%e5%8f%8d%e5%83%b9%e9%bb%9e%e7%ae%97/)
    《經濟一週》 2017年11月20日
3.  [交通交匯處上蓋起樓？覓地小組倡用空置校舍增土地供應](https://hk.news.appledaily.com/local/realtime/article/20180213/57828299/)
    《蘋果動新聞》 2018年2月13日
4.  [申8.1億改善西九龍道路網](http://orientaldaily.on.cc/cnt/news/20140408/00176_018.html)
    《東方日報》 2014年4月8日