**西原四丁目停留場**（）是一個位於[東京都](../Page/東京都.md "wikilink")[北區](../Page/北區_\(東京都\).md "wikilink")四丁目，屬於[荒川線的](../Page/荒川線.md "wikilink")[停留場](../Page/鐵路車站.md "wikilink")。

## 停留場構造

[對向式月台](../Page/對向式月台.md "wikilink")2面2線的[地面車站](../Page/地面車站.md "wikilink")。

## 相鄰停留場

  - [PrefSymbol-Tokyo.svg](https://zh.wikipedia.org/wiki/File:PrefSymbol-Tokyo.svg "fig:PrefSymbol-Tokyo.svg")
    東京都交通局
    [Tokyo_Sakura_Tram_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyo_Sakura_Tram_symbol.svg "fig:Tokyo_Sakura_Tram_symbol.svg")
    都電荒川線（東京櫻電）
      -
        [新庚申塚](../Page/新庚申塚停留場.md "wikilink")（SA 20）－**西原四丁目（SA
        19）**－[瀧野川一丁目](../Page/瀧野川一丁目停留場.md "wikilink")（SA 18）

## 外部連結

  - [東京都交通局
    西ヶ原四丁目停留場](http://www.kotsu.metro.tokyo.jp/toden/stations/nishigahara4-chome/index.html)

[ShigaharaYonchoume](../Category/日本鐵路車站_Ni.md "wikilink")
[Category:荒川線車站](../Category/荒川線車站.md "wikilink")
[Category:北區鐵路車站 (東京都)](../Category/北區鐵路車站_\(東京都\).md "wikilink")
[Category:1911年啟用的鐵路車站](../Category/1911年啟用的鐵路車站.md "wikilink")