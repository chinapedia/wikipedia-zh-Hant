**西原村**（）是位于[日本](../Page/日本.md "wikilink")[熊本縣中部內陸地區屬](../Page/熊本縣.md "wikilink")[阿蘇郡的一個](../Page/阿蘇郡.md "wikilink")[村](../Page/村.md "wikilink")，位於[阿蘇外輪山西側](../Page/阿蘇外輪山.md "wikilink")。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬[阿蘇郡山西村和](../Page/阿蘇郡.md "wikilink")[上益城郡河原村](../Page/上益城郡.md "wikilink")。
  - 1956年8月1日：阿蘇郡錦野村被廢除，部分地區被併入山西村，其餘地區被合併為[大津町](../Page/大津町.md "wikilink")。
  - 1960年9月1日：山西村和河原村[合併為](../Page/市町村合併.md "wikilink")**西原村**。\[1\]

### 演變表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 -1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1999年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>錦野村</p></td>
<td><p>1956年8月1日<br />
併入菊池郡大津町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1956年8月1日<br />
併入山西村</p></td>
<td><p>西原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>山西村</p></td>
<td><p>1960年9月1日<br />
西原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>上益城郡<br />
河原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

1.