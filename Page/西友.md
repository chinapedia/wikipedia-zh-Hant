**西友**（，Seiyu,
GK.）是[日本一家經營](../Page/日本.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[超級市場及](../Page/超級市場.md "wikilink")[量販店的零售企業](../Page/量販店.md "wikilink")，1963年創立，原為[西武百貨旗下子公司](../Page/西武百貨.md "wikilink")，現屬跨國零售企業[沃尔玛旗下](../Page/沃尔玛.md "wikilink")。

西友原在[香港](../Page/香港.md "wikilink")[沙田經營](../Page/沙田.md "wikilink")[西田百貨](../Page/西田百貨.md "wikilink")，2005年把全數股權出售予[新鴻基地產](../Page/新鴻基地產.md "wikilink")，而[西田百貨亦已於](../Page/西田百貨.md "wikilink")2008年4月27日易名為[一田百貨繼續經營](../Page/一田百貨.md "wikilink")。

## 歷史

  - 1956年 - 日本[西武百貨成立](../Page/西武百貨.md "wikilink")「西武商店」（）。
  - 1963年 - 改名為「西友商店」（）。
  - 1980年 - 開發原創品牌「[無印良品](../Page/無印良品.md "wikilink")」。
  - 1983年 -
    更改公司名稱為「西友」。無印良品在[東京](../Page/東京.md "wikilink")[青山開設店面](../Page/青山_\(東京\).md "wikilink")。
  - 1990年 - 將無印良品的經營權轉讓給「良品計畫」公司。
  - 2000年 - 以西武百貨作為交換條件，日本[住友商事成為最大股東](../Page/住友商事.md "wikilink")。
  - 2002年 -
    與[美國的](../Page/美國.md "wikilink")[沃爾瑪集團合作](../Page/沃爾瑪.md "wikilink")，沃爾瑪成為最大股東。
  - 2004年 - 1,600名員工提出辭職。
  - 2005年10月7日 - 推出「西友Shopping Card」。
  - 2005年 - 成為[沃爾瑪集團旗下的子公司](../Page/沃爾瑪.md "wikilink")。

## 集團公司

  - 連鎖[超級市場事業](../Page/超級市場.md "wikilink")
      - 西友

[Seiyu_store_akabane.JPG](https://zh.wikipedia.org/wiki/File:Seiyu_store_akabane.JPG "fig:Seiyu_store_akabane.JPG")
[SUNNY_Maebaru.jpg](https://zh.wikipedia.org/wiki/File:SUNNY_Maebaru.jpg "fig:SUNNY_Maebaru.jpg")

  -   - 北海道西友
      - 九州西友
          - 上面三家公司使用相同商標
      - 東北西友
      - S.S.V -
        [日本](../Page/日本.md "wikilink")[長野地區的業務推廣](../Page/長野.md "wikilink")
      - Sunny - 日本[福岡地區的業務推廣](../Page/福岡.md "wikilink")
          - Sunny使用自有的商標

  - 其他

      - 二光 - 經營[電視購物事業](../Page/電視購物.md "wikilink")，同時也是西友公司的人才派遣公司。
      - 若菜 -
        負責西友商店內的熟食販售、賣場營運。以及獨自的[便當和](../Page/便當.md "wikilink")[熟食店面](../Page/熟食.md "wikilink")。
      - Nicoh - 西友集團店面內[行動電話的銷售](../Page/行動電話.md "wikilink")。

## 外部連結

  - [西友｜SEIYU](https://www.seiyu.co.jp)

[Category:日本零售商](../Category/日本零售商.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:香港超級市場](../Category/香港超級市場.md "wikilink")
[Category:1956年成立的公司](../Category/1956年成立的公司.md "wikilink")
[Category:日本超級市場](../Category/日本超級市場.md "wikilink")
[Category:沃尔玛](../Category/沃尔玛.md "wikilink")