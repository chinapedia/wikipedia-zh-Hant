**西尼德·瑪麗·伯納黛特·奥康娜**
（，），是著名的[愛爾蘭流行歌手和歌曲作者](../Page/愛爾蘭.md "wikilink")。除了音樂以外，她還以反傳統的行為和有爭議性的觀點而聞名。代表作為翻唱[王子的](../Page/王子_\(音樂家\).md "wikilink")[Nothing
Compares 2 U](../Page/Nothing_Compares_2_U.md "wikilink")。

## 早年

奥康纳生在[都柏林](../Page/都柏林.md "wikilink")，五兄弟姐妹中排行第三，其他孩子分別名為Joseph, Eimear,
John, and Eoin。 她原本的名字是由愛爾蘭總統[Eamon de
Valera的妻子](../Page/Eamon_de_Valera.md "wikilink")[Sinéad de
Valera改的](../Page/Sinéad_de_Valera.md "wikilink")。

她的父親是John
O'Connor，原任職結構工程師，後來轉職為[律師](../Page/律師.md "wikilink")；而母親是Marie
O'Connor。她的父母很早已結婚，但當奧康娜8歲時便已離婚，5兄弟姊妹中，最年長的3位（包括奧康娜）與母親同居。

## 周末夜現場爭議

1992年10月3日，奥康娜出現於美國電視節目*[Saturday Night
Live](../Page/Saturday_Night_Live.md "wikilink")*。在場獻唱了[鲍勃·马利的歌曲](../Page/鲍勃·马利.md "wikilink")「戰爭」，抗議[羅馬天主教的性虐待醜聞](../Page/罗马天主教.md "wikilink")。然後拿出[教宗](../Page/教宗.md "wikilink")[若望·保禄二世的照片](../Page/若望·保禄二世.md "wikilink")，大喊
（打倒真正的敵人），並當著觀眾面前撕毁照片，另外因這個節目是現場直播，也是在數百萬的電視觀眾面前做的。\[1\]

不令人意外的，在媒體的推波助瀾之下，奥康娜接著便不斷地被噓下演出舞台並受到觀眾的言語攻擊。例如此事件兩週後，在麥迪遜花園舉辦的[鲍勃·迪伦](../Page/鲍勃·迪伦.md "wikilink")30週年紀念演唱會，當奥康娜要演唱"I
Believe In
You"時，噓聲（也有鼓勵聲）太大以致於她無法開唱，於是她便大聲喊出了"War"的歌詞（《war》-[鲍勃·马利的作品](../Page/鲍勃·马利.md "wikilink")）。主持人[Kris
Kristofferson並安慰她](../Page/Kris_Kristofferson.md "wikilink")「不要讓那些渾球影響你的心情」。

[Saturday Night Live](../Page/Saturday_Night_Live.md "wikilink")
在事前並不知道奥康娜會這麼做，並且拒絕了重播的要求（不過這在節目推出的DVD特集*Saturday Night
Live - 25 Years of Music*第四集中可以看到）。Comedy
Central重播這段時，則用奥康娜拿著一張微笑黑人兒童的照片的畫面取代原本畫面（應該是彩排時的影像）。SNL為了向觀眾表示歉意，其中一個動作是讓主持人[Joe
Pesci在隔週的演出時拿著被貼回原狀的教宗照片](../Page/Joe_Pesci.md "wikilink")。

這不是奥康娜第一次跟*Saturday Night Live*不合。此前她拒絕在[Andrew Dice
Clay主持的秀中出現](../Page/Andrew_Dice_Clay.md "wikilink")（因為他「排斥女性」）。但她同意上另一集由[Kyle
MacLachlan主持的節目](../Page/Kyle_MacLachlan.md "wikilink")。

1997年9月22日，意大利周报*Vita*采访奥康娜。奥康娜请教宗宽恕她。她说撕毁照片是一件可笑的做法。她还说明她并非叛离天主教，还是信教的，並引用
[聖奧古斯丁所說的](../Page/聖奧古斯丁.md "wikilink")：「憤怒是前往勇氣的第一步」。但是在2002年，她說对当年撕毁照片這事不会改變主意，同時亦表示已離開梵蒂岡的天主教，加入愛爾蘭的天主教和東正教使徒教會
(Irish Orthodox Catholic and Apostolic Church)。

2018年10月19日，奥康娜在 Twitter 表示自己已改信伊斯蘭教，並改名為**舒哈達·戴維特** ()。

## 作品列表

### 專輯

  - [The Lion and the
    Cobra](../Page/The_Lion_and_the_Cobra.md "wikilink") (1987年)
  - [I Do Not Want What I Haven't
    Got](../Page/I_Do_Not_Want_What_I_Haven't_Got.md "wikilink") (1990年)
  - Am I Not Your Girl? (1992年)
  - Universal Mother (1994年)
  - Gospel Oak EP (1997年)
  - So Far...The Best Of Sinéad O'Connor (1997年)
  - Faith And Courage (2000年)
  - Sean-Nós Nua (2002年)
  - [She Who Dwells in the Secret Place of the Most High Shall Abide
    Under the Shadow of the
    Almighty](../Page/She_Who_Dwells_in_the_Secret_Place_of_the_Most_High_Shall_Abide_Under_the_Shadow_of_the_Almighty.md "wikilink")
    (2003年)
  - Collaborations (2005年)
  - Throw Down Your Arms (2005年) ''(A collection of cover versions of
    Reggae hits)
  - Theology (2007年)
  - How About I Be Me (And You Be You?)
    ([2012](../Page/2012.md "wikilink"))''

## 單曲

  - "Mandinka" (1987年)
  - "I Want Your (Hands on Me)" {1988年)
  - "Troy" (1988年)
  - "Three Babies" (1990年)
  - "[Nothing Compares 2 U](../Page/Nothing_Compares_2_U.md "wikilink")"
    (1990年)
  - "My Special Child" (1991年)
  - "Success Has Made a Failure of our Home" (1992年)
  - "Don't Cry For Me, Argentina" (1992年)
  - "Fire on Babylon" (1994年)
  - "You Made Me the Thief of Your Heart" (1994年)
  - "Famine"/"All Apologies" (1995年)
  - "Thank You For Hearing Me" (1996年)
  - "This is a Rebel Song" (1997年)
  - "This is to Mother You" (1997年)
  - "No Man's Woman" (2000年)
  - "Jealous" (2000年)
  - "Troy (remix)" (2002年)
  - "Only You" (2009年)

## 延伸閱讀

  - Guterman, Jimmy. *Sinéad : Her Life and Music*. Warner Books, 1991.
    ISBN 0-446-39254-5.
  - Hayes, Dermott. *Sinéad O'Connor: So Different*. Omnibus, 1991. ISBN
    0-7119-2482-1.

## 外部連結

  - <http://www.sineadoconnor.com/(官网)>
  - <http://www.sinead-oconnor.com/>

## 注释

<references />

[Category:葛萊美獎獲得者](../Category/葛萊美獎獲得者.md "wikilink")
[Category:愛爾蘭女歌手](../Category/愛爾蘭女歌手.md "wikilink")
[Category:愛爾蘭流行音樂歌手](../Category/愛爾蘭流行音樂歌手.md "wikilink")
[Category:愛爾蘭創作歌手](../Category/愛爾蘭創作歌手.md "wikilink")
[Category:愛爾蘭語歌手](../Category/愛爾蘭語歌手.md "wikilink")
[Category:英語歌手](../Category/英語歌手.md "wikilink")
[Category:女性吉他手](../Category/女性吉他手.md "wikilink")
[Category:女性音樂家](../Category/女性音樂家.md "wikilink")
[Category:愛爾蘭女性主義者](../Category/愛爾蘭女性主義者.md "wikilink")
[Category:另类摇滚吉他手](../Category/另类摇滚吉他手.md "wikilink")
[Category:雙性戀音樂家](../Category/雙性戀音樂家.md "wikilink")
[Category:雙性戀歌手](../Category/雙性戀歌手.md "wikilink")
[Category:愛爾蘭LGBT人物](../Category/愛爾蘭LGBT人物.md "wikilink")
[Category:都柏林人](../Category/都柏林人.md "wikilink")

1.  [奥康娜撕教宗照片](http://www.notbored.org/sinead.html)