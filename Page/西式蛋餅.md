**歐姆蛋**（）是煎熟的[純蛋黃](../Page/純蛋黃.md "wikilink")，中間或可放些餡料捲着。其发源地为法国。做法是先在[平底锅内加進蛋汁煎到凝固](../Page/平底锅.md "wikilink")，再將煎好的圓形蛋餅對折成半圓形即可。\[1\]

[香港式](../Page/香港.md "wikilink")[茶餐廳會放](../Page/茶餐廳.md "wikilink")[火腿](../Page/火腿.md "wikilink")、[粗鹽醃牛肉](../Page/粗鹽醃牛肉.md "wikilink")、鮮牛肉、[午餐肉](../Page/午餐肉.md "wikilink")、[豌豆作為西式蛋餅的餡料](../Page/豌豆.md "wikilink")；[台灣則流行用](../Page/台灣.md "wikilink")[培根作餡料](../Page/培根.md "wikilink")，稱為「培根蛋餅」。

在[日本](../Page/日本.md "wikilink")，則會在內放入蕃茄醬炒飯改造成[蛋包飯](../Page/蛋包飯.md "wikilink")，成為日本國內無人不曉的一到日本洋食（之所以稱為Omuraisu，是因为**Ome**let+
**rice**）。

## 历史

歐姆蛋的英文「Omelette」是一個非常老的英文單字。 根據艾倫·戴維森的說法,\[2\] 英文的
*omelette*來源於法國，是一個在16世紀中期開始被發明、隨後被宮廷內廣泛使用的詞彙。
另外，還有諸如 *alumelle*和 *alumete* 的這些拼法，收錄于[Ménagier de
Paris](../Page/Ménagier_de_Paris.md "wikilink")（II, 5）當中。\[3\]

## 相關條目

  - [義大利煎蛋](../Page/義大利煎蛋.md "wikilink")

  - [中式蛋餅](../Page/中式蛋餅.md "wikilink")

  -
  - （日式煎蛋捲）

## 参考资料

[Category:蛋料理](../Category/蛋料理.md "wikilink")

1.  [Julia Child The French Chef- The Omlette
    Show](https://www.youtube.com/watch?v=hWi3NwDrQok)
2.  Alan Davidson, *Oxford Companion to Food* (Oxford University Press)
    1999 (pp. 550, 553)
3.  ["Omelette"](http://www.mediadico.com/dictionnaire/definition/omelette/1)