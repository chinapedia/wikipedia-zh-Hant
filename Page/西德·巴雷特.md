**罗杰·基思·“席德”·巴雷特**（****，1946年1月6日 -
2006年7月7日），是一位[英国音乐人](../Page/英国.md "wikilink")、词曲作者、歌手、画家，以[平克·弗洛伊德的创始成员身份最为知名](../Page/平克·弗洛伊德.md "wikilink")。在乐队的[迷幻时期](../Page/迷幻搖滾.md "wikilink")，他作为主唱、吉他手和主创作者，掌控了乐队早年创作在音乐和风格上的方向。他也是乐队的命名者。巴雷特于1968年4月离开了乐队，并短暂住院，被推测患有由服用毒品而恶化的精神疾病\[1\]。

巴雷特的音乐生涯持续了不到10年，他作为平克·弗洛伊德的一员录制了4首单曲，处女专辑《》（也参与了第二张专辑），和一些未发行的歌曲。1969年，巴雷特发行了单曲《》从而开始了单飞生涯，该曲出自他的第一张单人专辑《》（1970）。该专辑的录制用了一年时间（1968–1969），共用了五位不同的制作人（[Peter
Jenner](../Page/Peter_Jenner.md "wikilink")、Malcolm
Jones、[大衛·吉爾摩](../Page/大衛·吉爾摩.md "wikilink")、[罗杰·沃特斯和巴雷特自己](../Page/罗杰·沃特斯.md "wikilink")）。在《》发行的近两个月后，巴雷特开始制作他第二张也是最后一张专辑《》（由吉爾摩担任制作人，[理查德·赖特也有所贡献](../Page/理查德·赖特_\(音乐家\).md "wikilink")），该专辑于1970年末发行。随后，他自己选择隐退，直到2006年去世。1988年，一张包含了未发行的曲目及废弃段落的专辑《》在巴雷特的准许下由[百代唱片](../Page/百代唱片.md "wikilink")（EMI）发行。

巴雷特创新的吉他作品及对实验性技术的探索，例如运用不和谐音、失真、回授，对许多音乐人有巨大影响，如[大卫·鲍伊](../Page/大卫·鲍伊.md "wikilink")、[布莱恩·伊诺和](../Page/布莱恩·伊诺.md "wikilink")[吉米·佩奇](../Page/吉米·佩奇.md "wikilink")。在结束音乐生涯后，巴雷特继续绘画，把时间花在从事园艺上。八十年代，他的传记开始出现。[平克·佛洛伊德创作了几首向他致敬的作品](../Page/平克·佛洛伊德.md "wikilink")，最著名的是1975年的专辑《》，其中包含了纪念巴雷特的歌曲《》。

## 脚注

## 外部链接

  - [Malcolm Jones, The Making of the Madcap
    Laughs](http://www.pink-floyd.org/artint/64.htm)
  - \[<http://allmusic.com/cg/amg.dll?p=amg&sql=61>::67NP The Madcap
    Gets the Last Laugh\]
  - [Pink Floyd Fans Discuss The News Of The Death Of Syd
    Barrett](http://www.cbs4denver.com/video/?id=19073@kcnc.dayport.com)
  - [The Syd Barrett
    Archives](https://web.archive.org/web/20080704105838/http://www.sydbarrett.net/welcome.htm)
  - ['The Thing About
    Syd'](http://www.bbc.co.uk/radio/aod/mainframe.shtml?http://www.bbc.co.uk/radio/aod/radio2_aod.shtml?radio2/r2_sydbarrett)
  - [Profile of Syd
    Barrett](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=14893566)
  - [The New Syd Barrett Appreciation
    Society](http://www.sydbarrett.org/)
  - [Dolly Rocker](http://www.pink-floyd.org/barrett/)
  - [1500 Photos of Syd
    Barrett](http://www.neptunepinkfloyd.co.uk/gallery/v/SydBarrettPhotos/)

[Category:1946年出生](../Category/1946年出生.md "wikilink")
[Category:2006年逝世](../Category/2006年逝世.md "wikilink")
[Category:20世紀歌手](../Category/20世紀歌手.md "wikilink")
[Category:平克·佛洛伊德](../Category/平克·佛洛伊德.md "wikilink")
[Category:英格蘭男歌手](../Category/英格蘭男歌手.md "wikilink")
[Category:英國搖滾歌手](../Category/英國搖滾歌手.md "wikilink")
[Category:英格蘭吉他手](../Category/英格蘭吉他手.md "wikilink")
[Category:英格蘭創作歌手](../Category/英格蘭創作歌手.md "wikilink")
[Category:英格蘭畫家](../Category/英格蘭畫家.md "wikilink")
[Category:主音吉他手](../Category/主音吉他手.md "wikilink")
[Category:節奏吉他手](../Category/節奏吉他手.md "wikilink")
[Category:迷幻搖滾音樂家](../Category/迷幻搖滾音樂家.md "wikilink")
[Category:精神分裂症患者](../Category/精神分裂症患者.md "wikilink")
[Category:死于糖尿病的人](../Category/死于糖尿病的人.md "wikilink")
[Category:罹患胰腺癌逝世者](../Category/罹患胰腺癌逝世者.md "wikilink")
[B](../Category/摇滚名人堂入选者.md "wikilink")
[Category:劍橋人](../Category/劍橋人.md "wikilink")

1.