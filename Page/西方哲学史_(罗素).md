《**西方哲学史**》（）是一部从[前苏格拉底时期到](../Page/前蘇格拉底哲學.md "wikilink")[二十世纪早期的](../Page/二十世紀哲學.md "wikilink")[西方哲学的指南](../Page/西方哲学.md "wikilink")，作者为[英国哲学家](../Page/英国.md "wikilink")[伯特兰·罗素](../Page/伯特兰·罗素.md "wikilink")。

## 关于本书

该书写于[二战期间](../Page/二战.md "wikilink")，源于罗素1941年－1942年在[费城Barnes](../Page/费城.md "wikilink")
Foundation的一系列哲学史的讲座。很多关于历史方面的研究是由罗素的第三任妻子完成的。1943年，罗素收到出版商Simon and
Schuster预付的3000美元，尔后1943年－1944年间在他在Bryn Mawr
College写成该书。该书1945年在美国出版，一年后在英国出版。尽管学术界哲学家对该书多有批评，该书当时即在商业上大获成功，到1947年止即已重印几次，直到今天仍在重印。该书的成功带给罗素生命的最后二十五年稳定的财源。

该书幽默易懂，也体现了二十世纪最伟大哲学家之一——作者自身的哲学。当罗素1950年被授予[诺贝尔奖时](../Page/诺贝尔奖.md "wikilink")，《西方哲学史》被列举为获奖因素之一，并在获奖演说中两次提及该书。

罗素不仅讨论该书中主要人物的生活、历史背景、社会环境、和他们的哲学系统，他随后还饶有兴致的解释他们错在哪里以及为何出错。因此《西方哲学史》经常被看成既是关于书中人物哲学也是关于罗素自己哲学的一部著作。这一观点，同中國的“[郭象注庄子](../Page/莊子注_\(郭象\).md "wikilink")”颇有异曲同工之意。

## 基督教末世論與馬克思主義

本書將[馬克思主義與](../Page/馬克思主義.md "wikilink")[聖奧古斯丁的](../Page/聖奧古斯丁.md "wikilink")[末世論相比](../Page/末世論.md "wikilink")，認為兩者在心理與感情上相似。\[1\]

| 末世論                                        | 馬克思主義                                  |
| ------------------------------------------ | -------------------------------------- |
| [耶和華](../Page/耶和華.md "wikilink")           | [辯證唯物主義](../Page/辯證唯物主義.md "wikilink") |
| [彌賽亞](../Page/彌賽亞.md "wikilink")           | [馬克思](../Page/馬克思.md "wikilink")       |
| [選民 (宗教)](../Page/選民_\(宗教\).md "wikilink") | [無產階級](../Page/無產階級.md "wikilink")     |
| [教會](../Page/教會.md "wikilink")             | [共產黨](../Page/共產黨.md "wikilink")       |
| [耶穌再臨](../Page/耶穌再臨.md "wikilink")         | （無產階級）[革命](../Page/革命.md "wikilink")   |
| [地獄](../Page/地獄.md "wikilink")             | 打倒[資產階級](../Page/資產階級.md "wikilink")   |
| [天國](../Page/神的國.md "wikilink")            | 共產主義天堂                                 |

末世論與馬克思主義的對照

## 罗素本人的评论

## 中譯本

  -
  -
## 參考書目

## 外部链接

  - [西方哲学史（中译本）](http://www.tianyabook.com/zhexue/xifang/index.html)

[Category:哲学史](../Category/哲学史.md "wikilink")
[Category:哲学书籍](../Category/哲学书籍.md "wikilink")
[Category:欧洲历史著作](../Category/欧洲历史著作.md "wikilink")
[Category:伯特兰·罗素](../Category/伯特兰·罗素.md "wikilink")

1.