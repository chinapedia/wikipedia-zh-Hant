**西日耳曼語支**是[日耳曼語族中最大的一支](../Page/日耳曼語族.md "wikilink")，包括[德語](../Page/德語.md "wikilink")、[英語和](../Page/英語.md "wikilink")[弗里西语](../Page/弗里西语.md "wikilink")，還包括[荷語及](../Page/荷蘭語.md "wikilink")[南非語](../Page/南非語.md "wikilink")。日耳曼語族其他的分支尚有[北日耳曼語支和](../Page/北日耳曼語支.md "wikilink")[東日耳曼語支](../Page/東日耳曼語支.md "wikilink")。

## 歷史

西日耳曼語支下面有各種[分支語言](../Page/分支語言.md "wikilink")，但這些並不是從某個單一的“[原始西日耳曼語](../Page/原始西日耳曼語.md "wikilink")”分化演变下來。這樣分類主要是為了將[地理上相近](../Page/地理.md "wikilink")、相互之間有很多共同點的[語言進行概括](../Page/語言.md "wikilink")；此支下某些語言在某些方面也與[北日耳曼語或](../Page/北日耳曼語支.md "wikilink")[東日耳曼語相近](../Page/東日耳曼語.md "wikilink")。

## 发展

請-{注}-意，日耳曼語族下的分類很難準確定義；大多數都擁有[方言連續性](../Page/方言連續性.md "wikilink")，鄰近的方言能夠互相理解，距離較遠的則不能。

**西日耳曼語支**

  - [高地德語](../Page/高地德語.md "wikilink")
      - [古高地德語](../Page/古高地德語.md "wikilink")（Old High German）()
          - [古法蘭克語](../Page/古法蘭克語.md "wikilink")（Old Frankish）()
          - [中部德語](../Page/中部德語.md "wikilink")
              - [東中部德語](../Page/東中部德語.md "wikilink")
                  - [標準德語](../Page/標準德語.md "wikilink") ()
                  - [图林根语](../Page/图林根语.md "wikilink")（，）
                  - [西里西亞語](../Page/西里西亚语_\(德语方言\).md "wikilink")()
                  - [上撒克遜語](../Page/上撒克遜語.md "wikilink")()
                  - [劳西茨-诺伊马克方言](../Page/劳西茨-诺伊马克方言.md "wikilink")（）
                  - [上普鲁士语](../Page/上普鲁士语.md "wikilink")（，，接近消亡）
              - [西中部德語](../Page/西中部德語.md "wikilink")
                  - [中部法兰克语](../Page/中部法兰克语.md "wikilink")
                      - [利普里安语](../Page/利普里安语.md "wikilink")
                          - [科隆语](../Page/科隆语.md "wikilink")()
                      - [摩泽尔法兰克语](../Page/摩泽尔法兰克语.md "wikilink")（，），包括[盧森堡語](../Page/盧森堡語.md "wikilink")
                        ()
                  - [莱茵法兰克语](../Page/莱茵法兰克语.md "wikilink")
                      - [普法尔茨德語](../Page/普法尔茨德語.md "wikilink")
                        （，）()，包括[洛林法兰克语](../Page/洛林法兰克语.md "wikilink")（，）
                          - [賓夕法尼亞德語](../Page/賓夕法尼亞德語.md "wikilink")
                            ()（美國[賓夕法尼亞州東南部居住的](../Page/賓夕法尼亞州.md "wikilink")[阿們宗派與其他群體使用](../Page/阿米希人.md "wikilink")）
                      - [黑森语](../Page/黑森语.md "wikilink")（，）
          - [高地法兰克语](../Page/高地法兰克语.md "wikilink")
              - [东法兰克语](../Page/东法兰克语.md "wikilink")
                  - [美因法蘭克語](../Page/美因法蘭克語.md "wikilink")（）()
              - [南法兰克语](../Page/南法兰克语.md "wikilink")
          - [上德語](../Page/上部德语.md "wikilink")
              - [阿勒曼尼語](../Page/阿勒曼尼語.md "wikilink")，包括[瑞士德語](../Page/瑞士德語.md "wikilink")（SIL|gsw}}）
                  - [施瓦本语](../Page/施瓦本语.md "wikilink")（，）()
                  - [低地阿勒曼尼語](../Page/低地阿勒曼尼語.md "wikilink")
                      - [阿尔萨斯语](../Page/阿尔萨斯语.md "wikilink")（，）
                      - [巴塞爾德語](../Page/巴塞爾德語.md "wikilink") （，）
                      - [阿勒曼尼殖民語](../Page/阿勒曼尼殖民語.md "wikilink")（Colonia
                        Tovar dialect，委內瑞拉德國殖民者後裔使用）()
                  - [高地阿勒曼尼語](../Page/高地阿勒曼尼語.md "wikilink")（High
                    Alemannic German）
                      - [伯爾尼德語](../Page/伯爾尼德語.md "wikilink")（，）
                      - [蘇黎世德語](../Page/蘇黎世德語.md "wikilink")（，）
                  - [最高地阿勒曼尼語](../Page/最高地阿勒曼尼語.md "wikilink")（Highest
                    Alemannic German）
                      - [瓦爾瑟德語](../Page/瓦爾瑟德語.md "wikilink")（Walser
                        German）()
                  - [巴伐利亞語](../Page/巴伐利亞語.md "wikilink")()
                      - [北巴伐利亞語](../Page/北巴伐利亞語.md "wikilink")，包括[纽伦堡](../Page/纽伦堡.md "wikilink")
                      - [中巴伐利亞語](../Page/中巴伐利亞語.md "wikilink")，包括[慕尼黑](../Page/慕尼黑.md "wikilink")、[維也納](../Page/維也納.md "wikilink")
                      - [南巴伐利亞語](../Page/南巴伐利亞語.md "wikilink")，包括[因斯布鲁克](../Page/因斯布鲁克.md "wikilink"),
                        [克拉根福](../Page/克拉根福.md "wikilink").
                        意大利[博尔扎诺自治省](../Page/博尔扎诺自治省.md "wikilink")
                      - [辛布里語](../Page/辛布里語.md "wikilink")（，）()，意大利东北部
                      - [默切諾語](../Page/默切諾語.md "wikilink")（Mòcheno）()，意大利东北部
                      - [哈特德語](../Page/哈特德語.md "wikilink")（，，美国、加拿大[哈特派信徒使用](../Page/胡特尔派.md "wikilink")）()
          - [意第緒語](../Page/意第緒語.md "wikilink")（從[希伯來語和其他語言輸入很多字彙](../Page/希伯來語.md "wikilink")，傳統上用[希伯來字母書寫](../Page/希伯來字母.md "wikilink")）()
              - 東意第緒語 ()
              - 西意第緒語 ()
          - [倫巴底語](../Page/倫巴底語.md "wikilink")（已消亡）()
      - [维拉莫维安语](../Page/维拉莫维安语.md "wikilink")（從[低地撒克遜語](../Page/低地撒克遜語.md "wikilink")、[荷蘭語](../Page/荷蘭語.md "wikilink")、[波蘭語和](../Page/波蘭語.md "wikilink")[蘇格蘭語輸入很多字彙](../Page/蘇格蘭語.md "wikilink")，幾近滅亡）()
  - [低地法蘭克語](../Page/低地法蘭克語.md "wikilink")
      - [古低地法蘭克語](../Page/古低地法蘭克語.md "wikilink")（Old Low Franconian）()
          - [中古荷蘭語](../Page/中古荷蘭語.md "wikilink")（Middle Dutch）()
              - [荷蘭語](../Page/荷蘭語.md "wikilink") ()
                  - [佛兰德语](../Page/佛兰芒语.md "wikilink")（荷兰语：Vlaams）()
                      - [西佛兰德语](../Page/西佛兰德语.md "wikilink")
                      - [东佛兰德语](../Page/东佛兰德语.md "wikilink")
                  - [西蘭語](../Page/西蘭語.md "wikilink")（荷兰语：Zeeuws）()
                  - [荷兰方言](../Page/荷兰方言.md "wikilink")
                    （荷兰语：Hollands，英语：Hollandic）
                  - [布拉班特方言](../Page/布拉班特方言.md "wikilink")（荷兰语：Brabants，英语：Brabantian）
                  - [林堡語](../Page/林堡語.md "wikilink") ()
              - [南非荷蘭語](../Page/南非荷蘭語.md "wikilink")（從其他語言輸入很多字彙）()

<!-- end list -->

  - [低地德語](../Page/低地德語.md "wikilink")（從古撒克逊語／古低地法蘭克語變化下來）
      - [古撒克遜語](../Page/古撒克遜語.md "wikilink") ()
          - [中古低地德語](../Page/中古低地德語.md "wikilink") ()
              - [西低地德语](../Page/西低地德语.md "wikilink")
                  - [威斯特法伦语](../Page/威斯特法伦语.md "wikilink")()
                  - [奥斯特法伦语](../Page/奥斯特法伦语.md "wikilink")
                  - [北下萨克森语](../Page/北下萨克森语.md "wikilink")()
                      - [石勒苏益格语](../Page/石勒苏益格语.md "wikilink")
                      - [荷尔施泰因语](../Page/荷尔施泰因语.md "wikilink")
                          - [汉堡方言](../Page/汉堡方言.md "wikilink")
                      - [奥尔登堡方言](../Page/奥尔登堡方言.md "wikilink")
                      - [东弗里西亚下萨克森语](../Page/东弗里西亚下萨克森语.md "wikilink")
                        ()
              - [东低地德语](../Page/东低地德语.md "wikilink")
                  - [勃兰登堡语](../Page/勃兰登堡语.md "wikilink")
                  - [梅克伦堡-前波美拉尼亚语](../Page/梅克伦堡-前波美拉尼亚语.md "wikilink")
                  - [下普鲁士语](../Page/下普鲁士语.md "wikilink")
                  - [东波美拉尼亚语](../Page/东波美拉尼亚语.md "wikilink")
                  - [西波美拉尼亚语](../Page/西波美拉尼亚语.md "wikilink")
                  - [門諾低地德語](../Page/門諾低地德語.md "wikilink")（门诺低地德语：Plautdietsch，英语：Mennonite
                    Low
                    German，美國[賓夕法尼亞州東南部居住的](../Page/賓夕法尼亞州.md "wikilink")[門諾會信徒與其他群體使用](../Page/門諾會.md "wikilink")）()
              - [格罗宁根方言](../Page/格罗宁根方言.md "wikilink") ()
                  - [海尔德兰方言](../Page/海尔德兰方言.md "wikilink")
                      - Achterhoeks ()
                  - [德伦特方言](../Page/德伦特方言.md "wikilink") ()
                  - [费吕沃方言](../Page/费吕沃方言.md "wikilink") ()
                  - Stellingwerfs ()
                  - Sallands ()
                  - Twents ()
  - 盎格魯-弗里西語
      - [古弗里西語](../Page/古弗里西語.md "wikilink") ()
      - [弗里西語](../Page/弗里西語.md "wikilink")、[西弗里西語](../Page/西弗里西語.md "wikilink")
        ()
      - [北弗里西語](../Page/北弗里西語.md "wikilink") ()
      - [沙特弗里西語](../Page/沙特弗里西語.md "wikilink")（Saterfriesisch）()
      - [古英语](../Page/古英语.md "wikilink")、[盎格鲁-撒克遜语](../Page/盎格鲁-撒克遜语.md "wikilink")（由古代[盎格魯人](../Page/盎格魯人.md "wikilink")、[撒克遜人和](../Page/撒克遜人.md "wikilink")[朱特人等西日耳曼諸部族的方言互相影響而成](../Page/朱特人.md "wikilink")）()
          - [中古英語](../Page/中古英語.md "wikilink") ()
              - [英语](../Page/英语.md "wikilink")
                ()（輸入大量[拉丁語字彙和語法](../Page/拉丁語.md "wikilink")，大部分通過[諾曼法語而來](../Page/諾曼法語.md "wikilink")，很多方言。參看[英語種類列表](../Page/英語種類列表.md "wikilink")）
              - [低地蘇格蘭語](../Page/低地蘇格蘭語.md "wikilink") ()
                  - 海島方言
                  - 北部方言（包括 Doric 方言）
                  - 中部方言
                  - 南部方言
                  - 烏爾斯特語 （Ulster Scots）
                  - 都市方言
          - [约拉语](../Page/约拉语.md "wikilink")（Yola，已绝迹）

[西日耳曼语支](../Category/西日耳曼语支.md "wikilink")
[Category:日耳曼語族](../Category/日耳曼語族.md "wikilink")