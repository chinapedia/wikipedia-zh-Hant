<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>Provinz Westpreußen</strong><br />
<strong>西普鲁士省</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Flagge_Preußen_-_Provinz_Westpreußen.svg" title="fig:Flagge_Preußen_-_Provinz_Westpreußen.svg">Flagge_Preußen_-_Provinz_Westpreußen.svg</a></p></td>
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Wappen_Preußische_Provinzen_-_Westpreußen.png" title="fig:Wappen_Preußische_Provinzen_-_Westpreußen.png">Wappen_Preußische_Provinzen_-_Westpreußen.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>[ <a href="../Page/旗帜.md" title="wikilink">旗帜</a> ]</small></p></td>
<td style="text-align: center;"><p><small>[ <a href="../Page/徽章.md" title="wikilink">徽章</a> ]</small></p></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>存在时期</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/首府.md" title="wikilink">首府</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong><a href="../Page/人口.md" title="wikilink">人口</a></strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/人口密度.md" title="wikilink">人口密度</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>版图</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:German_Empire_-_Prussia_-_West_Prussia_(1878).svg" title="fig:German_Empire_-_Prussia_-_West_Prussia_(1878).svg">German_Empire_-_Prussia_-_West_Prussia_(1878).svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1871年德意志帝国时期，普鲁士王国内的西普鲁士省</p></td>
</tr>
</tbody>
</table>

**西普鲁士省**（;
）是1773－1824年及1878－1918年间[普鲁士的一个省分](../Page/普鲁士.md "wikilink")。1918年后，该省的中部成为了[波兰走廊和](../Page/波兰走廊.md "wikilink")[但泽自由市](../Page/但泽自由市.md "wikilink")，而其余仍是[魏玛共和国的领土则成为了](../Page/魏玛共和国.md "wikilink")[波森-西普鲁士和位于](../Page/波森-西普鲁士.md "wikilink")[东普鲁士的](../Page/东普鲁士.md "wikilink")[西普鲁士地区](../Page/西普鲁士地区.md "wikilink")。

除了这两段时间西普鲁士作为一个省分存在之外，「西普鲁士」一词在13世纪开始，至1945年亦是中欧[历史地区的名称](../Page/历史地区.md "wikilink")，历史上这地区是普鲁士人居住的地方，之后的多个世纪[德国人](../Page/德国人.md "wikilink")、[斯洛温人](../Page/斯洛温人.md "wikilink")、[卡舒比人](../Page/卡舒比人.md "wikilink")、[雨格诺派教徒](../Page/雨格诺派.md "wikilink")、[波兰人](../Page/波兰人.md "wikilink")、[门诺派教徒](../Page/门诺派.md "wikilink")、[苏格兰人等相继迁入](../Page/苏格兰人.md "wikilink")。

## 历史

在[十三年战争](../Page/十三年战争.md "wikilink")（1454-1466年）之中，[波美拉尼亚和普鲁士地区的城镇反抗](../Page/波美拉尼亚.md "wikilink")[条顿骑士团國](../Page/条顿骑士团國.md "wikilink")，并寻求波兰国王[卡齐米日四世的协助](../Page/卡齐米日四世.md "wikilink")。在1466年签订的[托伦条约之中](../Page/托伦条约_\(1466年\).md "wikilink")，波美拉尼亚和西普鲁士地区成为了波兰的省分[皇家普鲁士](../Page/皇家普鲁士.md "wikilink")，获得了数个特权，特别是在[但泽市](../Page/但泽.md "wikilink")。1569年皇家普鲁士成为了[波兰立陶宛联邦的一部分](../Page/波兰立陶宛联邦.md "wikilink")，保留了普鲁士人的自治政府。另一方面，在[托伦条约中降为波兰附庸的](../Page/托伦条约_\(1466年\).md "wikilink")[条顿骑士团仍然拥有东普鲁士](../Page/条顿骑士团.md "wikilink")。条顿骑士团國1525年升格为[普鲁士公国](../Page/普鲁士公国.md "wikilink")，1660年波兰丧失了对此地的宗主权。

1772年大部分的王室普鲁士地区在[第一次瓜分波兰之中合并到](../Page/瓜分波兰.md "wikilink")[普鲁士之内](../Page/普鲁士.md "wikilink")，次年成为了普鲁士的西普鲁士省，例外的是并入[东普鲁士省的](../Page/东普鲁士省_\(普鲁士\).md "wikilink")[威米亚地区](../Page/威米亚.md "wikilink")。1793年[第二次瓜分波兰之中](../Page/瓜分波兰.md "wikilink")，但泽这个[汉萨城市已经不能靠本身的财富而存活](../Page/汉萨同盟.md "wikilink")，所以但泽与另一个汉萨城市托伦一同加入普鲁士的西普鲁士。[大波兰的一些地区](../Page/大波兰.md "wikilink")1772年合并到普鲁士，[力斯地区](../Page/力斯地区.md "wikilink")1793年亦合并到西普鲁士。

1806年[拿破仑战争时期](../Page/拿破仑战争.md "wikilink")，西普鲁士的南部地区被划分到[华沙公国之内](../Page/华沙公国.md "wikilink")。1824年至1878年间西普鲁士和东普鲁士合并到[普鲁士省](../Page/普鲁士省.md "wikilink")，之后重新分开。西普鲁士1871年成为[德意志帝国的一部分](../Page/德意志帝国.md "wikilink")。

1919年[凡尔赛条约签订后](../Page/凡尔赛条约.md "wikilink")，大部分的西普鲁士地区被割让予[波兰第二共和国](../Page/波兰第二共和国.md "wikilink")，该省西部和东部小量地区仍属[魏玛共和国](../Page/魏玛共和国.md "wikilink")。剩余的西部地区1922年组成了[波森-西普鲁士](../Page/波森-西普鲁士.md "wikilink")，东部地区则成为[东普鲁士的](../Page/东普鲁士.md "wikilink")[西普鲁士地区](../Page/西普鲁士地区.md "wikilink")。

1945年的[第二次世界大战的](../Page/第二次世界大战.md "wikilink")[波茨坦会议中](../Page/波茨坦会议.md "wikilink")，决定将所有前西普鲁士的领土都划分波兰管治。这些地区的德国人口都被驱逐到西面，然后让波兰人迁入。1949年，这些难民建立了非牟利的[西普鲁士领土协会](../Page/:en:Territorial_Association_of_West_Prussia.md "wikilink")。

## 历史人口

[Westpreußen_und_DanzigerBucht.png](https://zh.wikipedia.org/wiki/File:Westpreußen_und_DanzigerBucht.png "fig:Westpreußen_und_DanzigerBucht.png")

|      | 居民         | 非德裔市民  |
| ---- | ---------- | ------ |
| 西普鲁士 | 1,433,681人 | 1,976人 |

1890年普鲁士及其省分的人口

由1885年至1890年，西普鲁士的人口上升了1%。

  - 1875年 - 1,343,057人
  - 1880年 - 1,405,898人
  - 1890年 - 1,433,681人（717,532名天主教徒、681,195名新教徒、21,750名犹太人或其它教徒）
  - 1900年 - 1,563,658人（800,395名天主教徒、730,685名新教徒、18,226名犹太人或其它教徒）

## 另见

  - [东普鲁士](../Page/东普鲁士.md "wikilink")
  - [东普鲁士省](../Page/东普鲁士省_\(普鲁士\).md "wikilink")
  - [波美拉尼亚](../Page/波美拉尼亚.md "wikilink")
  - [普鲁士王室](../Page/普鲁士王室.md "wikilink")

## 外部链接

  - [www.westpreussen-online.de](http://www.westpreussen-online.de/)
  - [Administrative subdivision of the province
    in 1910](http://www.gemeindeverzeichnis.de/gem1900//gem1900.htm?westpreussen/westpreussen1900.htm)

  - [Das Westpreußenlied (Real
    Audio)](http://www.rosenberg-wpr.de/westpreussen.rm)
  - [West Prussia
    FAQ](https://web.archive.org/web/20070104213903/http://users.foxvalley.net/~goertz/faqwpr.html)
  - [East and West Prussia
    Gazetteer](https://web.archive.org/web/20070105163554/http://www.progenealogists.com/germany/prussia/index.html)

[Category:普鲁士历史](../Category/普鲁士历史.md "wikilink")