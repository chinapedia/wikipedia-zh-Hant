[Sai_Sha_Road_at_night.JPG](https://zh.wikipedia.org/wiki/File:Sai_Sha_Road_at_night.JPG "fig:Sai_Sha_Road_at_night.JPG")
**西沙路**（英文：**Sai Sha
Road**）是一條來往[香港](../Page/香港.md "wikilink")[新界東](../Page/新界東.md "wikilink")[西貢區](../Page/西貢區.md "wikilink")[西貢及](../Page/西貢.md "wikilink")[沙田區](../Page/沙田區.md "wikilink")[馬鞍山的主要道路](../Page/馬鞍山_\(香港市鎮\).md "wikilink")，東起西貢[大網仔路](../Page/大網仔路.md "wikilink")[麥邊迴旋處](../Page/麥邊.md "wikilink")，北行途經[企嶺下](../Page/企嶺下海.md "wikilink")、[十四鄉](../Page/十四鄉.md "wikilink")、[烏溪沙](../Page/烏溪沙.md "wikilink")，轉西南經[馬鞍山市中心](../Page/馬鞍山市中心.md "wikilink")，早期到[頌安邨連接](../Page/頌安邨.md "wikilink")[恆康街止](../Page/恆康街.md "wikilink")，其後伸延到[錦泰苑連接](../Page/錦泰苑.md "wikilink")[恒德街終止](../Page/恒德街.md "wikilink")。由[烏溪沙站到](../Page/烏溪沙站.md "wikilink")[恆安站一段](../Page/恆安站.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[馬鞍山綫路軌架空在西沙路中央](../Page/馬鞍山綫.md "wikilink")。雖然此路並非斜路，但有頗多彎位。

## 歷史

西沙路在1970年代末期興建，早期只由[大網仔路伸展到](../Page/大網仔.md "wikilink")[泥涌](../Page/泥涌.md "wikilink")，稱為**泥涌路**（Nai
Chung Access
Road），於1986年改稱**西沙公路**。隨著[沙田新市鎮向東擴展至](../Page/沙田新市鎮.md "wikilink")[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")，直達[烏溪沙](../Page/烏溪沙.md "wikilink")，[香港政府將](../Page/香港政府.md "wikilink")[馬鞍山路由](../Page/馬鞍山路.md "wikilink")[恒安邨及](../Page/恒安邨.md "wikilink")[耀安邨向東伸展](../Page/耀安邨.md "wikilink")，直達泥涌與西沙公路連接，成為西沙公路的延長部份，並於1988年通車\[1\]。[馬鞍山市中心一段已預留中央空間於](../Page/馬鞍山市中心.md "wikilink")2001年10月興建馬鐵（今[港鐵](../Page/港鐵.md "wikilink")[馬鞍山綫](../Page/馬鞍山綫.md "wikilink")），而西沙公路更進一步連接[馬鞍山繞道直通各行車隧道](../Page/馬鞍山繞道.md "wikilink")。

西沙路[馬鞍山市中心的一段](../Page/馬鞍山市中心.md "wikilink")（恆康街至恆安站）時速限制為70[公里](../Page/公里.md "wikilink")，[西貢大網仔路交匯處起至](../Page/西貢.md "wikilink")[泥涌](../Page/泥涌.md "wikilink")[帝琴灣迴旋處止的一段則為](../Page/帝琴灣.md "wikilink")50[公里](../Page/公里.md "wikilink")，其後帝琴灣外一段放寬至80公里\[2\]
。[九巴](../Page/九巴.md "wikilink")[99線及](../Page/九龍巴士99線.md "wikilink")[299X線均行經此路](../Page/九龍巴士299X線.md "wikilink")，分別以馬鞍山恆安邨及[沙田市中心為總站前往西貢](../Page/沙田市中心.md "wikilink")。

另外，由麥邊迴旋處起點至泥涌停車場的路段，逢星期日或公眾假期，禁止騎單車通行。

[File:HK_SaiShaRoad_MakPin.JPG|西沙路起點麥邊迴旋處](File:HK_SaiShaRoad_MakPin.JPG%7C西沙路起點麥邊迴旋處)
[File:HK_SaiShaRoad_OTau.JPG|西沙路近](File:HK_SaiShaRoad_OTau.JPG%7C西沙路近)[澳頭](../Page/澳頭.md "wikilink")，為雙向單線無分隔路段
<File:SaiShaRoad> NaiChung.jpg|西沙路近[泥涌一段](../Page/泥涌.md "wikilink")
<File:Ma> On Shan Station Sai Sha
Road.jpg|西沙路近[馬鞍山站](../Page/馬鞍山站_\(香港\).md "wikilink")
[File:HK_SaiShaRoad_ChungOnEstate.JPG|西沙路近](File:HK_SaiShaRoad_ChungOnEstate.JPG%7C西沙路近)[馬鞍山運動場及](../Page/馬鞍山運動場.md "wikilink")[頌安邨一段](../Page/頌安邨.md "wikilink")

## 參見

  - [西貢區](../Page/西貢區.md "wikilink")
  - [西貢](../Page/西貢.md "wikilink")
  - [沙田區](../Page/沙田區.md "wikilink")
  - [馬鞍山](../Page/馬鞍山.md "wikilink")
  - [西沙茶座](../Page/西沙茶座.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [立法會財務委員會：介乎錦英路與日後的T7號主幹路交界處的西沙路擴闊工程](http://www.legco.gov.hk/yr01-02/chinese/fc/pwsc/papers/p01-85c.pdf)
  - [路政署將擴闊西沙路路段](http://www.info.gov.hk/gia/general/200208/12/0812167.htm)

[Category:西貢區街道](../Category/西貢區街道.md "wikilink")
[Category:大埔區街道](../Category/大埔區街道.md "wikilink")
[Category:沙田區街道](../Category/沙田區街道.md "wikilink")

1.  [沙田西貢一線牽](http://how.hk/york/tkosk4.html) ，「參見將軍」，於2008年12月19日查閱
2.  [香港附屬法例：更改車速限制公告](http://www.hklii.org/hk/legis/ch/reg/374M/s3.html)，於2008年12月19日查閱