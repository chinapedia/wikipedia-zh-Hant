**西泽·奥古斯塔斯·罗德尼**（**Caesar Augustus
Rodney**，，[美国政治家](../Page/美国.md "wikilink")，[民主共和党成员](../Page/民主共和党.md "wikilink")，曾任[美国众议院议员](../Page/美国众议院.md "wikilink")（1803年-1805年、1821年-1822年）、[美国司法部长](../Page/美国司法部长.md "wikilink")（1807年-1811年）、[美国参议院议员](../Page/美国参议院.md "wikilink")（1822年-1823年）。

[R](../Category/美国民主共和党联邦众议员.md "wikilink")
[R](../Category/美國外交官.md "wikilink")
[R](../Category/賓夕法尼亞大學校友.md "wikilink")
[Category:美國民主共和黨聯邦參議員](../Category/美國民主共和黨聯邦參議員.md "wikilink")
[Category:德拉瓦州聯邦參議員](../Category/德拉瓦州聯邦參議員.md "wikilink")
[Category:德拉瓦州聯邦眾議員](../Category/德拉瓦州聯邦眾議員.md "wikilink")
[Category:德拉瓦州眾議員](../Category/德拉瓦州眾議員.md "wikilink")