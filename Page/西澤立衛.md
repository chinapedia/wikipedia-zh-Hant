**西澤立衛**（），日本[建築師](../Page/建築師.md "wikilink")，[橫濱國立大學](../Page/橫濱國立大學.md "wikilink")[準教授](../Page/準教授.md "wikilink")。建築師[西澤大良是他親哥哥](../Page/西澤大良.md "wikilink")。2010年，西澤立衛及[妹島和世獲得建築界最高榮譽](../Page/妹島和世.md "wikilink")[普利兹克奖](../Page/普利兹克奖.md "wikilink")。

## 略歷

  - 1966年 出生於[神奈川縣](../Page/神奈川縣.md "wikilink")。
  - 1988年 畢業於橫濱國立大學工學部建築學科。
  - 1990年 橫濱國立大學[研究所工學研究科計劃建設學專業碩士課程修畢](../Page/研究所.md "wikilink")。
  - 1995年
    與[妹島和世共同成立](../Page/妹島和世.md "wikilink")[SANAA](../Page/SANAA.md "wikilink")。
  - 1997年 成立西澤立衛建築設計事務所。
  - 2005年
    與妹島和世一同獲得[肖克獎](../Page/肖克獎.md "wikilink")（[視覺藝術部門](../Page/視覺藝術.md "wikilink")）。
  - 2010年 與妹島和世一同獲得普立茲克建築師獎

## 主要作品

※此處僅限於以西澤立衛建築設計事務所為名義發表的作品。與SANAA及妹島和世建築事務所合作的項目記載於[SANAA](../Page/SANAA.md "wikilink")。

  - 週末住宅　<small>（[群馬縣](../Page/群馬縣.md "wikilink")[碓冰郡](../Page/碓冰郡.md "wikilink")／專用住宅／1997-1998）</small>
  - Paper
    Show　<small>（[東京都](../Page/東京都.md "wikilink")[港区Spiral](../Page/港区_\(東京都\).md "wikilink")
    Garden／展覽會會場佈置／1999／會期結束）</small>
  - 鎌倉住宅　<small>（[神奈川縣](../Page/神奈川縣.md "wikilink")[鎌倉市](../Page/鎌倉市.md "wikilink")／專用住宅／1999-2001）</small>
  - Love
    Planet展會場佈置　<small>（[岡山縣](../Page/岡山縣.md "wikilink")[岡山市](../Page/岡山市.md "wikilink")／展覽會會場佈置／2003／會期結束）</small>
  - [直島地中美術館辦公室](../Page/直島地中美術館.md "wikilink")　<small>（[香川縣](../Page/香川縣.md "wikilink")[香川郡](../Page/香川郡.md "wikilink")[直島](../Page/直島.md "wikilink")／辦公室，畫廊／2004）</small>
  - 船橋公寓　<small>（[千葉縣](../Page/千葉縣.md "wikilink")[船橋市](../Page/船橋市.md "wikilink")／[集合住宅](../Page/集合住宅.md "wikilink")／2002-2004)</small>
  - 森山邸　<small>（[東京都](../Page/東京都.md "wikilink")／專用住宅+集合住宅／2002-2005）</small>
  - House A　<small>（[大阪府](../Page/大阪府.md "wikilink")／專用住宅／2007）</small>
  - 十和田市現代美術館　<small>（[青森縣](../Page/青森縣.md "wikilink")[十和田市](../Page/十和田市.md "wikilink")／美術館／2008）</small>
  - 豊島美術館　<small>（[香川縣](../Page/香川縣.md "wikilink")[小豆郡](../Page/小豆郡.md "wikilink")[豐島](../Page/豐島.md "wikilink")／美術館／2010）</small>
  - 輕井澤千住博美術館　<small>（[長野縣](../Page/長野縣.md "wikilink")[北佐久郡](../Page/北佐久郡.md "wikilink")[輕井澤](../Page/輕井澤.md "wikilink")／美術館／2011）</small>
  - 葺田Pavilion　<small>（[香川縣](../Page/香川縣.md "wikilink")[小豆郡](../Page/小豆郡.md "wikilink")[小豆島](../Page/小豆島.md "wikilink")／涼亭／2013）</small>
  - 寺崎邸　<small>（[神奈川縣](../Page/神奈川縣.md "wikilink")／專用住宅／2014）</small>

## 相關連結

  - [十和田市現代美術館](http://www.artstowada.com/)
  - [輕井澤千住博美術館](http://www.senju-museum.jp/)

[Category:日本建筑师](../Category/日本建筑师.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")
[Category:橫濱國立大學校友](../Category/橫濱國立大學校友.md "wikilink")
[Category:罗尔夫·朔克奖获得者](../Category/罗尔夫·朔克奖获得者.md "wikilink")