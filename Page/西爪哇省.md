| image_flag = {{\#property:p41}} | image_alt = | image_caption = |
motto =
Gemah Ripah Repeh Rapih
([巽他語](../Page/巽他語.md "wikilink"))
(meaning: Serene, Prosperous, Peaceful, United)\[1\] | image_map = West
Java in Indonesia.svg | map_alt = | map_caption = | coordinates =  |
coor_pinpoint = | coordinates_footnotes = | subdivision_type = 国家 |
subdivision_name =  | established_title = 建立 | established_date =
1945年8月19日 | established_title1 = 重建 | established_date1 = 1950年7月14日
| seat_type = [首府](../Page/印度尼西亚省份列表.md "wikilink") | seat =
[萬隆](../Page/萬隆.md "wikilink") | governing_body = West Java
Regional Government | leader_title = 首长 | leader_name = [Ridwan
Kamil](../Page/Ridwan_Kamil.md "wikilink") | leader_title1 = 副首长 |
leader_name1 = [Uu Ruzhanul
Ulum](../Page/Uu_Ruzhanul_Ulum.md "wikilink") | leader_title2 = 立法机构 |
leader_name2 = [West Java Provincial
Council](../Page/West_Java_Provincial_Council.md "wikilink") |
total_type = | unit_pref = Metric | area_total_km2 = 35377.76\[2\] |
area_total_sq_mi = | area_rank = 21st | elevation_min_m = 0 |
elevation_point = | elevation_max_m = 3078 | elevation_max_ft = |
elevation_max_point = [澤勒邁火山](../Page/澤勒邁火山.md "wikilink") |
population_total = 48683861\[3\] | population_as_of = 2018 |
population_footnotes = | population_density_km2 = auto |
population_density_sq_mi = | population_est = 49316712\[4\] |
pop_est_as_of = 2019 | population_rank = 1st |
population_density_rank = 2nd | population_demonym = West Javan
Warga Jawa Barat <small>([id](../Page/印尼语.md "wikilink"))</small>
<small>([巽他語](../Page/巽他語.md "wikilink"))</small> | demographics_type1
= 人口 | demographics1_footnotes = | demographics1_title1 = 族群 |
demographics1_info1 = [巽他族](../Page/巽他族.md "wikilink") (79%),
[爪哇族](../Page/爪哇族.md "wikilink") (10%),
[Cirebonese](../Page/Cirebonese.md "wikilink") (7%),
[Betawi](../Page/Betawi_people.md "wikilink") (4%) |
demographics1_title2 = 宗教 | demographics1_info2 =
[伊斯兰教](../Page/伊斯兰教.md "wikilink") (97%),
[基督教](../Page/基督教.md "wikilink") (1.81%),
[佛教](../Page/佛教.md "wikilink") (0.58%),
[儒家](../Page/儒家.md "wikilink") (0.22%),
[印度教](../Page/印度教.md "wikilink") (0.05%), [Sunda
Wiwitan](../Page/Sunda_Wiwitan.md "wikilink") | demographics1_title3 =
语言 | demographics1_info3 = [印尼语](../Page/印尼语.md "wikilink")
[巽他語](../Page/巽他語.md "wikilink")
[Cirebonese](../Page/Cirebonese_language.md "wikilink")
[Betawi](../Page/Betawi_language.md "wikilink")  | timezone1 =
[印度尼西亚标准时间](../Page/印度尼西亚标准时间.md "wikilink") |
utc_offset1 = +7 | postal_code_type =
[Postcodes](../Page/Postal_codes_in_Indonesia.md "wikilink") |
postal_code = 1xxxx, 4xxxx | area_code = (62)2x, (62)2xx |
area_code_type = [Area
codes](../Page/Telephone_numbers_in_Indonesia.md "wikilink") | geocode =
| iso_code = ID-JB | registration_plate_type = [Vehicle
sign](../Page/Vehicle_registration_plates_of_Indonesia.md "wikilink") |
registration_plate = B, D, E, F, T, Z | website =  }}

**西爪哇** ()
是[印度尼西亚](../Page/印度尼西亚.md "wikilink")[爪哇岛上的一个](../Page/爪哇岛.md "wikilink")[省](../Page/省.md "wikilink")，首府為[万隆](../Page/万隆.md "wikilink")。

## 历史

西爪哇是印尼最老的一个省，1950年它正式成为印尼的省，2000年10月17日[萬丹被从西爪哇分离出去形成了一个新的省](../Page/万丹省.md "wikilink")。

## 地理和人口

[缩略图](https://zh.wikipedia.org/wiki/File:Deep_fog_in_Ciater,_Indonesia.jpg "fig:缩略图")
2000年班顿分离后西爪哇的人口为35,500,611。

向西西爪哇临印尼首都[雅加达和班顿省](../Page/雅加达.md "wikilink")，向东临[中爪哇省](../Page/中爪哇.md "wikilink")，向北滨[爪哇海](../Page/爪哇海.md "wikilink")，向南滨[印度洋](../Page/印度洋.md "wikilink")。与印尼其它省份不同的是西爪哇的省府万隆不在海边，而是一个内陆高山城市。[西大魯河是西爪哇省最長與最大的河流](../Page/西大魯河.md "wikilink")，在當地人民的生活上佔有重要的角色。

除印尼的官方语言[印尼语外](../Page/印尼语.md "wikilink")，另一个通用的语言是[巽他语](../Page/巽他语.md "wikilink")，在南部临近中爪哇的地区[爪哇语也比较常用](../Page/爪哇语.md "wikilink")。在[井里汶和附近地区](../Page/井里汶.md "wikilink")[井里汶语比较常用](../Page/井里汶语.md "wikilink")，在一些偏远地区印尼语很少有人懂。

## 自然资源

根据印尼国家秘书处的数据，2006年西爪哇省稻田的总面积为94,886,23平方千米，生产了941萬吨稻米,由910萬吨水田稻米和31萬吨农场稻米组成。Palawija的产量，达到了200萬吨，每公顷的产量是178.28公担。不过，种植最广泛的当属商品玉米，种植面积达到了14萬8千公顷。西爪哇也生产园艺农产品，包括293萬吨蔬菜、319萬吨水果以及15萬9千吨药用植物。

西爪哇的森林有764,387.59公顷，占全省面积的20.62%。由362,980.40公顷（9.79%）产业林地、22,8727.11公顷（6.17%）受保护林地和172,680公顷（4.63%）保养林地组成。红树林面积达到40,129.89公顷，范围延伸到10个有海岸的摄政统治区。此外，在西爪哇和万丹还有另一块面积为32,313.59的受保护林地由Perum
Perhutani 三世所管理。

2006年西爪哇从经济林中收获了将近200,675立方米木材，尽管该省每年的木材需求量约为4百万立方米。直到2006年，居民种植的森林有214,892公顷，木材产量约为893,851.75立方米。西爪哇同时也生产非林业产品，这在发展前景上与林业产品一样具有潜力，例如Sutera
alat 蘑菇、松木、gerah dammar、maleleuca、藤条、竹子和燕窝。

[\*](../Category/西爪哇省.md "wikilink")
[Category:爪哇島](../Category/爪哇島.md "wikilink")

1.  Sigar, Edi. *Buku Pintar Indonesia*. Jakarta: Pustaka Delaprasta,
    1996
2.
3.
4.