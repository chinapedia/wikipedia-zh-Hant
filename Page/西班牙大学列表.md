以下是[西班牙各大学的列表](../Page/西班牙.md "wikilink")：

## [安达卢西亚自治区](../Page/安达卢西亚自治区.md "wikilink")（Andalucía）

  - [阿尔梅里亚大学](../Page/阿尔梅里亚大学.md "wikilink")，（西班牙语：Universidad de
    Almería）位于[阿尔梅里亚省](../Page/阿尔梅里亚.md "wikilink")（[安达卢西亚自治区](../Page/安达卢西亚.md "wikilink")），[公立大学](../Page/公立大学.md "wikilink")。
  - [加的斯大学](../Page/加的斯大学.md "wikilink")，（西语：Universidad de
    Cádiz）位于[加的斯省](../Page/加的斯.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。
  - [格拉纳达大学](../Page/格拉纳达大学.md "wikilink")，（Universidad de Granada
    ）位于[格拉纳达省](../Page/格拉纳达.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。
  - [韦尔瓦大学](../Page/韦尔瓦大学.md "wikilink")，（Universidad de
    Huelva）位于[韦尔瓦省](../Page/韦尔瓦.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。
  - [哈恩大学](../Page/哈恩大学.md "wikilink")，（Universidad de
    Jaén）位于[哈恩省](../Page/哈恩省.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。
  - [马拉加大学](../Page/马拉加大学.md "wikilink")，（Universidad de
    Málaga）位于[马拉加省](../Page/马拉加.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。
  - [塞维利亚大学](../Page/塞维利亚大学.md "wikilink")，（Universidad de
    Sevilla）位于[塞维利亚省](../Page/塞维利亚.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。
  - [安达卢西亚洛约拉大学](../Page/安达卢西亚洛约拉大学.md "wikilink")，（Universidad Loyola
    Andalucía）位于[塞维利亚省](../Page/塞维利亚.md "wikilink")，[私立大学](../Page/私立大学.md "wikilink")。
  - [巴布罗·德·奥拉维戴大学](../Page/巴布罗·德·奥拉维戴大学.md "wikilink")，（Universidad
    Pablo de
    Olavide）位于[塞维利亚省](../Page/塞维利亚.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。
  - [安达卢西亚国际大学](../Page/安达卢西亚国际大学.md "wikilink")，（Universidad
    Internacional de
    Andalucía）位于[塞维利亚省](../Page/塞维利亚.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。
  - [马尔贝拉大学](../Page/马尔贝拉大学.md "wikilink")，（Universidad de
    Marbella）位于[马尔贝拉](../Page/马尔贝拉.md "wikilink")（[马拉加省](../Page/马拉加省.md "wikilink")），[私立大学](../Page/私立大学.md "wikilink")。

## [阿拉贡自治区](../Page/阿拉贡自治区.md "wikilink")（Aragón）

  - [沙拉哥薩大學](../Page/沙拉哥薩大學.md "wikilink")，（Universidad de
    Zaragoza）位于[萨拉戈萨省](../Page/萨拉戈萨.md "wikilink")（[阿拉贡自治区](../Page/阿拉贡.md "wikilink")），[公立大学](../Page/公立大学.md "wikilink")。
  - [圣·乔治大学](../Page/圣·乔治大学.md "wikilink")，（Universidad San
    Jorge）位于Villanueva de
    Gállego（[萨拉戈萨省](../Page/萨拉戈萨省.md "wikilink")），[私立大学](../Page/私立大学.md "wikilink")。

## [阿斯图里亚斯自治区](../Page/阿斯图里亚斯自治区.md "wikilink")（Asturias）

  - [奥维耶多大学](../Page/奥维耶多大学.md "wikilink")，（Universidad de
    Oviedo）位于[奥维耶多省](../Page/奥维耶多.md "wikilink")（[阿斯图里亚斯自治区](../Page/阿斯图里亚斯.md "wikilink")），[公立大学](../Page/公立大学.md "wikilink")。

## [卡斯提亞-雷昂自治區](../Page/卡斯提亞-雷昂.md "wikilink")（Castilla y León）

  - [薩拉曼卡大學](../Page/薩拉曼卡大學.md "wikilink")（University of Salamanca）

## [加泰隆尼亞自治區](../Page/加泰隆尼亞自治區.md "wikilink")（Cataluña）

  - [巴塞罗那自治大学](../Page/巴塞罗那自治大学.md "wikilink")，（Universitat Autònoma de
    Barcelona，Barcelona）
  - [巴塞罗那大学](../Page/巴塞罗那大学.md "wikilink")，（Universitat de
    Barcelona，Barcelona）
  - [庞培法布拉大学](../Page/庞培法布拉大学.md "wikilink")，（[Universitat Pompeu
    Fabra](http://en.wikipedia.org/wiki/Pompeu_Fabra_University)，Barcelona）
  - [洛维拉维吉利大学](../Page/洛维拉维吉利大学.md "wikilink")，（Universitat Rovira i
    Virgili，Tarragona）
  - [加泰罗尼亚理工大学](../Page/加泰罗尼亚理工大学.md "wikilink")，（西班牙语：Universidad
    Politécnica de Cataluña， 加泰罗尼亚语：Universitat Politècnica de
    Catalunya）
  - [加泰羅尼亞開放大學](../Page/加泰羅尼亞開放大學.md "wikilink") (Open University of
    Catalonia)

## [馬德里自治區](../Page/馬德里自治區.md "wikilink")（Comunidad de Madrid）

  - [阿爾卡拉大學](../Page/阿爾卡拉大學.md "wikilink") (University of Alcalá)
  - [圣路易斯大学](../Page/圣路易斯大学.md "wikilink")，（Saint Louis University）
  - [马德里欧洲大学](../Page/马德里欧洲大学.md "wikilink")，（Universidad Europea de
    Madrid）
  - [马德里康普顿斯大学](../Page/马德里康普顿斯大学.md "wikilink")，（Universidad
    Complutense de Madrid）
  - [马德里理工大学](../Page/马德里理工大学.md "wikilink"), (Universidad Politécnica
    de Madrid).
  - [马德里卡洛斯三世大学](../Page/马德里卡洛斯三世大学.md "wikilink"), (Universidad Carlos
    III de Madrid)
  - [馬德里自治大學](../Page/马德里自治大学.md "wikilink") (Autonomous University of
    Madrid)

## [巴利阿里群岛](../Page/巴利阿里群岛.md "wikilink")（Islas Baleares）

  - [巴利阿里群岛大学](../Page/巴利阿里群岛大学.md "wikilink")，（Universidad de las Islas
    Baleares）位于[帕尔马](../Page/帕尔马.md "wikilink")，[公立大学](../Page/公立大学.md "wikilink")。

## [納瓦拉自治區](../Page/納瓦拉自治區.md "wikilink")（Navarra）

  - [納瓦拉大學](../Page/納瓦拉大學.md "wikilink") (University of Navarra)

## [穆尔西亚自治區](../Page/穆尔西亚自治区.md "wikilink")（Murcia）

  - [卡塔赫纳理工大学](../Page/卡塔赫纳理工大学.md "wikilink")（Universidad politécnica
    de Cartagena）
  - [莫夕亞大學](../Page/莫夕亞大學.md "wikilink") (University of Murcia)

## [瓦倫西亞自治區](../Page/瓦倫西亞自治區.md "wikilink")（Valenciana）

  - [瓦倫西亞大學](../Page/華倫西亞大學.md "wikilink") (University of Valencia)
  - [瓦倫西亞理工大學](../Page/瓦伦西亚理工大学.md "wikilink") (Polytechnic University
    of Valencia)

## 参看

  - [西班牙教育](../Page/西班牙教育.md "wikilink")
  - [世界各国大学列表](../Page/世界各国大学列表.md "wikilink")
  - [世界大学列表](../Page/世界大学列表.md "wikilink")

[Category:歐洲大學列表](../Category/歐洲大學列表.md "wikilink")
[Category:西班牙文化列表](../Category/西班牙文化列表.md "wikilink")
[西班牙大學](../Category/西班牙大學.md "wikilink")
[Category:各国高校列表](../Category/各国高校列表.md "wikilink")