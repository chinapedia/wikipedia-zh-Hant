[spanish.steps.arp.jpg](https://zh.wikipedia.org/wiki/File:spanish.steps.arp.jpg "fig:spanish.steps.arp.jpg")的住所\]\]

**西班牙廣場**（）是位于[義大利](../Page/義大利.md "wikilink")[羅馬的廣場](../Page/羅馬.md "wikilink")，旁有[羅馬地鐵的](../Page/羅馬地鐵.md "wikilink")[同名車站](../Page/西班牙廣場站.md "wikilink")。[山上天主圣三教堂](../Page/山上天主圣三教堂.md "wikilink")（Trinità
dei
Monti，由[法國](../Page/法國.md "wikilink")[波旁王朝的國王所資助建造](../Page/波旁王朝.md "wikilink")）就位在與西班牙廣場相接的[西班牙階梯](../Page/西班牙階梯.md "wikilink")（）頂端。西班牙廣場也曾經出現在電影《[羅馬假期](../Page/羅馬假期.md "wikilink")》的場景中。

## 設計

西班牙廣場上有一個[巴洛克藝術的噴泉](../Page/巴洛克藝術.md "wikilink")，被稱為[破船噴泉](../Page/破船噴泉.md "wikilink")（La
Fontana della
Barcaccia）。是由[義大利雕塑家](../Page/義大利.md "wikilink")[濟安·貝尼尼](../Page/濟安·貝尼尼.md "wikilink")（Gian
Lorenzo Bernini）的父親[彼得·貝尼尼](../Page/彼得·貝尼尼.md "wikilink")（Pietro
Bernini）所設計，於1627年-1629年間建造完成。彼得·貝尼尼於1623年起成為[特萊維噴泉](../Page/特萊維噴泉.md "wikilink")（Acqua
Vergine）的主建築師。根據一個不太可信的傳聞，教皇[伍朋八世因為在](../Page/伍朋八世.md "wikilink")[臺伯河水災中對一艘被沖到此地的船有深刻的印象](../Page/臺伯河.md "wikilink")，所以在此建造了破船噴泉。
[fontana.della.barcaccia.arp.jpg](https://zh.wikipedia.org/wiki/File:fontana.della.barcaccia.arp.jpg "fig:fontana.della.barcaccia.arp.jpg")
在西班牙廣場與西班牙階梯相接的右側，有一棟黃白色的建築曾經是詩人[約翰·濟慈的住所](../Page/濟慈.md "wikilink")，1821年濟慈也在這裡去世。而現在這棟黃白色濟慈生前的住所變成博物館，來紀念濟慈。同樣在西班牙階梯的右側，15世紀的[樞機主教Lorenzo](../Page/樞機主教.md "wikilink")
Cybo de Mari的宅邸也座落在此。
[Fontana_della_Barcaccia_2.jpg](https://zh.wikipedia.org/wiki/File:Fontana_della_Barcaccia_2.jpg "fig:Fontana_della_Barcaccia_2.jpg")

## 參考資料

  -
  -
  -
  -
## 外部連結

  - [Piazza di Spagna website](http://www.piazzadispagna.it/)
  - [Spanish Steps
    Photos](http://www.romestate.it/fotobook_event.php?id=17&lang=eng)
  - [Spanish Steps](http://www.italyguides.it/us/roma/spanish_steps.htm)
    Virtual reality movie and picture gallery
  - [Detailed information](http://www.romeartlover.it/Vasi40.html) with
    photos and 18th-century engravings by Giuseppe Vasi
  - [Image and Link
    Collection](http://travel.duchs.com/Italy/Rome/See/Spanish_Steps/)
  - [The Spanish
    Steps](http://www.panoramas.dk/fullscreen5/f3_rome.html) 360 degree
    panorama - QuickTime VR.

[Category:罗马广场](../Category/罗马广场.md "wikilink")