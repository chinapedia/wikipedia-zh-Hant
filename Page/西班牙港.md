**西班牙港**（**Port of
Spain**）是[特立尼達和多巴哥的首都](../Page/千里達及托巴哥.md "wikilink")，位于[千里達岛西部](../Page/千里達岛.md "wikilink")[帕里亚湾畔](../Page/帕里亚湾.md "wikilink")。西班牙港人口约350,000，是该国仅次于[查瓜纳斯和](../Page/查瓜纳斯.md "wikilink")[圣费尔南多的人口第三多城市](../Page/圣费尔南多.md "wikilink")。

## 姊妹市

  - [亞特蘭大](../Page/亞特蘭大.md "wikilink")

  - [瓜德羅普](../Page/瓜德羅普.md "wikilink")[Morne-à-l'Eau](../Page/Morne-à-l'Eau.md "wikilink")（Morne-a-Lou）

  - [喬治敦](../Page/喬治敦_\(蓋亞那\).md "wikilink")

  - [列治文](../Page/列治文_\(加利福尼亞州\).md "wikilink")

  - [圣凯瑟琳斯](../Page/圣凯瑟琳斯.md "wikilink")

  - [拉哥斯](../Page/拉哥斯.md "wikilink")

  - [麗水市](../Page/麗水市_\(韓國\).md "wikilink")

[\*](../Category/西班牙港.md "wikilink")
[Category:特立尼达和多巴哥城市](../Category/特立尼达和多巴哥城市.md "wikilink")
[Category:北美洲首都](../Category/北美洲首都.md "wikilink")