**西班牙语**（），也称**卡斯提亞語**（），简称**西语**，是起源於[西班牙](../Page/西班牙.md "wikilink")[卡斯提亞地方的](../Page/卡斯提亞.md "wikilink")[語言](../Page/語言.md "wikilink")，為[罗曼语族的分支](../Page/罗曼语族.md "wikilink")。除了發源地西班牙之外，使用者主要集中在[拉丁美洲国家](../Page/拉丁美洲国家.md "wikilink")，约有五億人使用。按照[第一语言使用者数量排名](../Page/第一语言.md "wikilink")，[西班牙语为全世界第二位](../Page/西班牙语.md "wikilink")，僅次於[華語](../Page/華語.md "wikilink")。加上第二語言人口，總使用人数排名則為世界第三，僅次於[華語及](../Page/華語.md "wikilink")[英語](../Page/英語.md "wikilink")。

西班牙语是除[英語](../Page/英語.md "wikilink")、[法語和](../Page/法語.md "wikilink")[阿拉伯語之外最多國家的](../Page/阿拉伯語.md "wikilink")[官方語言](../Page/官方語言.md "wikilink")，同時為[聯合國官方語言之一](../Page/聯合國官方語言.md "wikilink")\[1\]。全世界有超過五億人的母語或第二語言是西班牙語，另外有二千萬的學生將西班牙語當作外語學習\[2\]\[3\]。

## 历史

[Castellano-Español.png](https://zh.wikipedia.org/wiki/File:Castellano-Español.png "fig:Castellano-Español.png")
[Linguistic_map_Southwestern_Europe-en.gif](https://zh.wikipedia.org/wiki/File:Linguistic_map_Southwestern_Europe-en.gif "fig:Linguistic_map_Southwestern_Europe-en.gif")各語在[伊比利半島的發展](../Page/伊比利半島.md "wikilink")\]\]
西班牙语是从[通俗拉丁语中发展而来的](../Page/通俗拉丁语.md "wikilink")，同时受到[巴斯克语和](../Page/巴斯克语.md "wikilink")[阿拉伯语的影响](../Page/阿拉伯语.md "wikilink")。西班牙语音的典型特征是[辅音弱化](../Page/辅音弱化.md "wikilink")、[腭音化](../Page/腭音化.md "wikilink")。相同的现象也可以在大部分的罗曼语系的语言中发现。

[中世纪的辅音系统在](../Page/中世纪.md "wikilink")「犹太－西班牙语」中得到更好的保留。这种语言使用于在15世纪被驱逐出西班牙的[犹太人后裔中](../Page/犹太人.md "wikilink")。

从十六世纪起，西班牙的美洲殖民运动将西班牙语带到了[美洲和](../Page/美洲.md "wikilink")[西属东印度群岛](../Page/西属东印度群岛.md "wikilink")。二十世纪，西班牙语传播到了[赤道几内亚和](../Page/赤道几内亚.md "wikilink")[西撒哈拉](../Page/西撒哈拉.md "wikilink")。与此同时，由于来自拉美移民的影响，西班牙语也开始在不属于昔日[西班牙帝国的地区传播开来](../Page/西班牙帝国.md "wikilink")，比较典型的例子包括[美国](../Page/美国.md "wikilink")[纽约市的](../Page/纽约市.md "wikilink")[西班牙哈莱姆地区](../Page/东哈莱姆.md "wikilink")。

在西班牙语的发展历程中，作家[塞万提斯占有极其重要的地位](../Page/塞万提斯.md "wikilink")，以至西班牙语经常被人们稱为“塞万提斯的语言”。

## 分布

西班牙语是[联合国的六大官方语言之一](../Page/联合国.md "wikilink")\[4\]，同时也是[非洲联盟](../Page/非洲联盟.md "wikilink")，[欧盟和](../Page/欧盟.md "wikilink")[南方共同市场的官方语言](../Page/南方共同市场.md "wikilink")。
在21世纪使用西班牙语作为官方语言的国家和地区有：[阿根廷](../Page/阿根廷.md "wikilink")、[玻利维亚](../Page/玻利维亚.md "wikilink")、[智利](../Page/智利.md "wikilink")、[哥伦比亚](../Page/哥伦比亚.md "wikilink")、[哥斯达黎加](../Page/哥斯达黎加.md "wikilink")、[古巴](../Page/古巴.md "wikilink")、[多米尼加](../Page/多米尼加.md "wikilink")、[厄瓜多尔](../Page/厄瓜多尔.md "wikilink")、[萨尔瓦多](../Page/萨尔瓦多.md "wikilink")、[赤道几内亚](../Page/赤道几内亚.md "wikilink")、[危地马拉](../Page/危地马拉.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[巴拿马](../Page/巴拿马.md "wikilink")、[巴拉圭](../Page/巴拉圭.md "wikilink")、[秘鲁](../Page/秘鲁.md "wikilink")、[波多黎各](../Page/波多黎各.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[乌拉圭和](../Page/乌拉圭.md "wikilink")[委内瑞拉](../Page/委内瑞拉.md "wikilink")。在[美国雖僅有](../Page/美国.md "wikilink")[新墨西哥州列為該州官方語言](../Page/新墨西哥州.md "wikilink")，但美國聯邦政府、部份州政府及地方政府的網站、文件、商店、产品说明及公共场所等大多也會提供西班牙文。

除此之外，西班牙语也在[安道尔](../Page/安道尔.md "wikilink")、[伯利兹](../Page/伯利兹.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[直布罗陀](../Page/直布罗陀.md "wikilink")、[以色列](../Page/以色列.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[特立尼达和多巴哥以及](../Page/特立尼达和多巴哥.md "wikilink")[西撒哈拉被使用](../Page/西撒哈拉.md "wikilink")。

在西班牙和拉丁美洲拥有很多不同的西班牙语方言，西班牙北部的[卡斯蒂利亚方言发音通常被认为是西班牙语的标准发音](../Page/卡斯蒂利亚.md "wikilink")。在[美洲](../Page/美洲.md "wikilink")，第一批西班牙人带来他们的地方口音，今天仍然可以发现在美洲的不同地方存在着明显不同的方言口音。

## 字母表

此外，还有重音标志的字母*á*、*é*、*í*、*ó*、*ú*。字母*ü*用于字母组合*güe*和*güi*来表示*u*是发音的，因为其他的组合*gue*、*gui*中，*u*是不发音的。

根據1994年西班牙皇家學院的決定，「CH」跟「LL」不再作為單獨的字母出現在字母表中。但是不受該院管轄的美洲西班牙語則依然將這兩個字母列在字母表內。

注意：字母 "W" 和 "K" 平常时一般不被使用。它们只出现于外来词汇，比如 "kilogramo"（公斤）或
"wáter"（厕所,也可寫做"váter"）。

## 语法

相对而言，西班牙语是一种[屈折语](../Page/屈折语.md "wikilink")，依靠词形变化表明语素的联系，名词分为阴性和阳性，每个动词有约五十种变位形式，有人称和单复数的区别，并依[时态](../Page/时态.md "wikilink")、[语气](../Page/语气.md "wikilink")、[体和](../Page/体_\(语法\).md "wikilink")[语态而变](../Page/语态.md "wikilink")。且形容词和限定词随名词的[性](../Page/性_\(语法\).md "wikilink")、[数变化而变](../Page/數_\(語法\).md "wikilink")。但与[古典拉丁语和](../Page/古典拉丁语.md "wikilink")[俄语这样的强屈折语相比](../Page/俄语.md "wikilink")，西班牙语没有名词变格，且更多地借助[前置词来建立受词与句子中其他部分的联系](../Page/前置词.md "wikilink")。西班牙语倾向于将修饰语置于中心语之后，但如需表达特定语义，形容词也会位于其所修饰的名词之前。如同其它罗曼语，基本语序为[主谓宾结构](../Page/主谓宾结构.md "wikilink")，但常常不限于此。西班牙语的主语可以省略，因为人称可以由动词变位体现，只要不引起歧义且不强调主语，表明主语就是没有必要的。西班牙语还是一种动词框架语言，以动词来表明路径，而不是像日耳曼语言那样用小品词或词缀。例如西班牙语的“subir
corriendo”（向上跑）和“salir volando”（飞出），译为英语就是“to run up”和“to fly out”。

西班牙语的一般疑问句可不将主语和动词倒装，更不靠语气词，分辨陈述句和一般疑问句有时全靠语调。

### [性](../Page/性_\(语法\).md "wikilink")

西班牙语的名词和形容词分为阳性和阴性。通常以-o结尾的名词或形容词为阳性，以-a结尾的名词或形容词为阴性。（例如: pelo-頭髮\[阳性\],
chica-女孩\[阴性\]，和葡萄牙語相同），-dad,-tad,-ción,-sión,tión结尾的名词多数也是阴性。（例如：ciudad-城市、universidad-大学、dificultad-困难
situacion-局势,television-电视，gestion-办事处）

### [数](../Page/數_\(語法\).md "wikilink")

名词和形容词都有单复数两种形式。通常为在词根后面加-s（结尾是元音字母的单词）或-es（结尾是辅音字母的单词）。句子中的名词和形容词的性、数要一致。

### [动词](../Page/动词.md "wikilink")

西班牙语的动词体系复杂而规律，有很多不同的时态。在不同的时态中根据不同的动词和人称有不同的变位方式。通常西班牙语的动词分为三类：以-ar结尾的动词、以-er结尾的动词和以-ir结尾的动词。

以现在时为例：

| 动词原形                              | hablar（说） | comer（吃） | vivir（住） |
| --------------------------------- | --------- | -------- | -------- |
| 第一人称单数（yo我）                       | hablo     | como     | vivo     |
| 第一人称复数（nosotros我們）                | hablamos  | comemos  | vivimos  |
| 第二人称单数（tú你）                       | hablas    | comes    | vives    |
| 第二人称复数（vosotros你們）                | habláis   | coméis   | vivís    |
| 第三人称单数（él他/ella她/usted您）          | habla     | come     | vive     |
| 第三人称复数（ellos他們/ellas她們/ustedes您們） | hablan    | comen    | viven    |

西班牙语的陈述语序通常是[主谓宾结构](../Page/主谓宾结构.md "wikilink")。西班牙语的感叹句、疑问句分别要在前加上[倒感叹号及倒问号](../Page/倒感叹号及倒问号.md "wikilink")，后面加上感叹号、问号。例如：（这是什么？），（那不是真的！）

有些動詞並不規則，例如estar（狀態～，～在～）和ser（是～）：

| 动词原形                              | estar   | ser   |
| --------------------------------- | ------- | ----- |
| 第一人称单数（yo我）                       | estoy   | soy   |
| 第一人称复数（nosotros我們）                | estamos | somos |
| 第二人称单数（tú你）                       | estás   | eres  |
| 第二人称复数（vosotros你們）                | estáis  | sois  |
| 第三人称单数（él他/ella她/usted您）          | está    | es    |
| 第三人称复数（ellos他們/ellas她們/ustedes您們） | están   | son   |

### [副詞](../Page/副詞.md "wikilink")

在西班牙语的形容詞後加上*mente*會變成副詞，o結尾的形容詞則要換成a，例如：

| 形容詞       | 中文解釋 | 副詞                 | 中文解釋 |
| --------- | ---- | ------------------ | ---- |
| frecuente | 頻繁的  | frecuente**mente** | 頻繁地  |
| económico | 經濟的  | económica**mente** | 經濟地  |
| lento     | 慢的   | lenta**mente**     | 慢慢地  |
| rápido    | 快的   | rápida**mente**    | 快快地  |

如果兩個副詞連續，mente只會在後面的副詞用一次。如果前面本身的形容詞為o結尾，也要把o改為a。

| 形容詞                  | 中文解釋    | 副詞                            | 中文解釋    |
| -------------------- | ------- | ----------------------------- | ------- |
| ágil y exacto        | 快而準的    | ágil y exacta**mente**        | 快而準地    |
| político y económica | 政治和經濟性的 | política y económica**mente** | 政治和經濟性地 |

## 书写系统

## 参见

  - 語言：
      - [西班牙語動詞](../Page/西班牙語動詞.md "wikilink")
      - [西班牙語言](../Page/西班牙語言.md "wikilink")
  - 方言：
      - [美国的西班牙语](../Page/美国的西班牙语.md "wikilink")
      - [Yeísmo](../Page/Yeísmo.md "wikilink")
      - [Seseo](../Page/Seseo.md "wikilink")
      - [Voseo](../Page/Voseo.md "wikilink")
  - 文學
      - [西班牙文学](../Page/西班牙文学.md "wikilink")

（和葡萄牙語一部分一樣）

  - 西班牙語國家及族群
      - [西班牙語國家和地區列表](../Page/西班牙語國家和地區列表.md "wikilink")
      - [西班牙裔](../Page/西班牙裔.md "wikilink")
  - 受西班牙語影響的語言及方言：
      - [伊比利吉普賽語](../Page/伊比利吉普賽語.md "wikilink")
      - [查瓦卡諾語](../Page/查瓦卡諾語.md "wikilink")
      - [拉迪諾語](../Page/拉迪諾語.md "wikilink")
      - [帕皮阿门托语](../Page/帕皮阿门托语.md "wikilink")
      - [查莫罗语](../Page/查莫罗语.md "wikilink")

## 参考资料

## 外部連結

  - [Chinese Talking
    Dictionary](http://www.dictionarist.com/)<small>（[www.dictionarist.com](http://www.dictionarist.com)）</small>
  - [線上法語、義大利語、西班牙語字典](http://www.wordreference.com/)<small>（[WordReference.com](http://www.wordreference.com)）</small>

[Category:西班牙語言](../Category/西班牙語言.md "wikilink")
[Category:西班牙语](../Category/西班牙语.md "wikilink")
[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")
[Category:屈折語](../Category/屈折語.md "wikilink")
[Category:委內瑞拉語言](../Category/委內瑞拉語言.md "wikilink")

1.
2.
3.
4.