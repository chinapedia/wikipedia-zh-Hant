**西班牙蒼蠅**（；\[1\]）是一種的[鞘翅目甲殼昆蟲](../Page/鞘翅目.md "wikilink")，屬[芫菁科](../Page/芫菁科.md "wikilink")，體形常隨著不同種類而大小有所改變，常居於[忍冬科和](../Page/忍冬科.md "wikilink")[木犀科的植物之上](../Page/木犀科.md "wikilink")。能分泌一種氣味辛辣的黃色液體，而芫菁體內含有最多5%的芫菁素，能刺激動物的細胞組織。而利用它烘焙壓成的粉末，粉色閃亮，並且呈淺黃褐至褐[橄欖色](../Page/橄欖.md "wikilink")，食味苦澀，氣味難聞。幼蟲吃[地棲蜂所釀的蜜](../Page/地棲蜂.md "wikilink")。

有文獻顯示，芫菁會不時刺激[農場的動物](../Page/農場.md "wikilink")[交配](../Page/交配.md "wikilink")。\[2\]芫菁在尿液中的[分泌物會刺激](../Page/分泌物.md "wikilink")[尿道](../Page/尿道.md "wikilink")，生殖器會產生[炎症反應](../Page/炎症.md "wikilink")，導致持繼[勃起](../Page/勃起.md "wikilink")。因此，早有人類使用芫菁作[春藥用途](../Page/春藥.md "wikilink")。不過由於需要的份量很少，而且易於超出[有效劑量](../Page/有效劑量.md "wikilink")，使用時有一定的危險。服用芫菁後，會出發熱、排尿疼痛，甚至血尿等徵狀，而且可能對[腎臟和](../Page/腎.md "wikilink")[生殖器造成永久損害](../Page/生殖器.md "wikilink")。該成份毒性極大，曾引起許多中毒死亡的案例。

## 歷史

[Collecting_cantharides.jpg](https://zh.wikipedia.org/wiki/File:Collecting_cantharides.jpg "fig:Collecting_cantharides.jpg")

古[希臘時期](../Page/希臘.md "wikilink")，醫學家[希波克拉底早在記載西班牙苍蝇的藥效](../Page/希波克拉底.md "wikilink")，而芫菁的翅亦會用作研製發起[水泡的膏布](../Page/水泡.md "wikilink")，並用作抗刺激藥。在古代[中國](../Page/中國.md "wikilink")，曾出現世上首個有記錄的[臭彈](../Page/臭彈.md "wikilink")（stink
bomb），就是以這些芫菁昆蟲類，混合[砒霜](../Page/砒霜.md "wikilink")、[附子和人類的](../Page/附子.md "wikilink")[糞便而成](../Page/糞便.md "wikilink")。\[3\]在[桑特利亞](../Page/桑特利亞.md "wikilink")（Santeria），這些芫菁則會用作制[香](../Page/香.md "wikilink")。\[4\]

而芫菁更早已是世界知名的[春药](../Page/春药.md "wikilink")，[羅馬帝國開國君主](../Page/羅馬帝國.md "wikilink")[屋大维的妻子](../Page/屋大维.md "wikilink")[莉薇婭](../Page/莉薇婭.md "wikilink")，會將斑蟊混入賓客的食物當中，引誘賓客輕薄自己，其後再以此勒索。\[5\]據記載，神聖羅馬皇帝[亨利四世](../Page/亨利四世_\(神圣罗马帝国\).md "wikilink")（1050-1106）亦因曾服用斑蟊而有損健康。1572年，法國[外科學醫生巴雷](../Page/外科學.md "wikilink")（Ambroise
Paré）曾記錄了一位男子服用一定份量的[蕁麻](../Page/蕁麻.md "wikilink")（nettle）和斑蟊後，出現一種「最可怕的淫亂症」。\[6\]1670年代，法國[黑魔法師](../Page/黑魔法.md "wikilink")
La Voisin
會混和烘乾的[鼴鼠](../Page/鼴鼠.md "wikilink")（moles）和[蝙蝠的](../Page/蝙蝠.md "wikilink")[血液製成](../Page/血液.md "wikilink")\[7\]，滲入[路易十四的食物](../Page/路易十四.md "wikilink")，以維持路易十四對他的情婦[蒙提斯斑夫人](../Page/蒙提斯斑夫人.md "wikilink")（Françoise-Athénaïs,marquis
de Montespan）的需索。18世紀斑蟊昆蟲在歐洲流行，在法國稱之「pastilles
Richelieu」，[薩德侯爵更被指在](../Page/薩德侯爵.md "wikilink")1772年，給妓女服用[大茴香](../Page/大茴香.md "wikilink")（anise）味的香碇和西班牙苍蝇，強行與妓女[群交及](../Page/群交.md "wikilink")[雞姦她們](../Page/雞姦.md "wikilink")，薩德侯爵因此被判處死，但及後上訴得到緩刑。

同時，西班牙苍蝇亦會用作為[墮胎藥](../Page/墮胎.md "wikilink")、興奮劑（因為用後會導致[失眠和神經緊張](../Page/失眠.md "wikilink")），甚至毒藥，若以粉狀滲進食物則不易察覺。[意大利](../Page/意大利.md "wikilink")[美第奇家族的一位女伯爵](../Page/美第奇家族.md "wikilink")
Toffana 研製的毒藥「Aqua toffana （aquetta di
Napoli）」，就是以砒霜和西班牙蒼蠅混合而成。據說，在[水和](../Page/水.md "wikilink")[酒中加入](../Page/酒.md "wikilink")4至6滴，服後數小時內便能無痛死亡。\[8\]為了證明西班牙蒼蠅令人致死的藥效，他們會進行測試，其中一個方式是將中毒死者的內臟分解於[油中](../Page/油.md "wikilink")，塗在[兔的臉上](../Page/兔.md "wikilink")，以觀察兔子有否出現水泡。

[<File:Lytta_vesicatoria_Spanische_Fliege_Brehms.png>](https://zh.wikipedia.org/wiki/File:Lytta_vesicatoria_Spanische_Fliege_Brehms.png "fig:File:Lytta_vesicatoria_Spanische_Fliege_Brehms.png")

## 毒性

芫菁有很强的肾毒性，属剧毒物品。 实验显示：
小鼠注射芫菁素7.5～10毫克，连用10天，可致心肌纤维、肝细胞和肾小管上皮细胞混浊肿胀，肺脾瘀血或小灶性出血。其对皮肤粘膜及胃肠道均有较强的刺激作用，吸收后由肾脏排泄，可刺激尿道，出现肾炎及膀胱炎症状，甚至导致急性肾功能衰竭。而成人口服0.6克能導致中毒，致命劑量為1.5克\[9\]。

## 商業和烹調用途

除了用於[畜牧业和供註冊醫護人員治療](../Page/畜牧业.md "wikilink")[疣外](../Page/疣.md "wikilink")，美國法例禁止使用斑蟊。一些以郵購或網絡售賣催情藥，以「西班牙蒼蠅」的名稱作招徠，但不少都是由[卡宴辣椒粉](../Page/卡宴辣椒粉.md "wikilink")（cayenne
pepper）製成的膠囊，有時則混以[人參](../Page/人參.md "wikilink")、[巨藻](../Page/巨藻.md "wikilink")（kelp）、[薑](../Page/薑.md "wikilink")、[崩大碗](../Page/崩大碗.md "wikilink")。\[10\]在德國有一種產品名為「Spanische
Fliege （西班牙蒼蠅）」，只是[顺势疗法的藥物](../Page/顺势疗法.md "wikilink")，有效成份極低，一般服用風險不大。

而一種產自[北非的果醬](../Page/北非.md "wikilink")「」，除[大麻製劑](../Page/大麻.md "wikilink")（Hashish）、[杏仁漿](../Page/杏仁.md "wikilink")、[开心果](../Page/开心果.md "wikilink")、[砂糖](../Page/砂糖.md "wikilink")、[橙或](../Page/橙.md "wikilink")[羅望子皮](../Page/羅望子.md "wikilink")、[丁香外](../Page/丁香.md "wikilink")，有時亦會加入西班牙蒼蠅。在[摩洛哥以及其他北非地方](../Page/摩洛哥.md "wikilink")，其中一種名為「Ras
el
hanout」（[北非综合香料](../Page/北非综合香料.md "wikilink")）的香料，成份亦曾包括斑蟊，不過摩洛哥已在1990年代禁售。

在香港，斑蝥與[水銀](../Page/汞.md "wikilink")、[砒霜等同屬烈性或毒性中藥](../Page/三氧化二砷.md "wikilink")，受《[中醫藥條例](../Page/中醫藥條例.md "wikilink")》嚴格管制，只有註冊中醫才可處方，銷售及配發紀錄均需保留。據《[中華人民共和國藥典](../Page/中華人民共和國藥典.md "wikilink")》記載，斑蝥性味辛熱，本身有劇毒，能破血逐瘀，散結消癥\[11\]。

## 注釋

## 參考資料

  - .

  - .

  - .

  - .

  - .

  - .

  - .

  - .

  - .

## 外部連結

  - [斑蝥
    Banmao](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00367)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [斑蝥 Ban
    Mao](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00212)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [科學松鼠會 »
    惡毒的愛：一響貪歡後，有人減重10%，有人腦袋被掏空](http://songshuhui.net/archives/98024)

[Category:鞘翅目](../Category/鞘翅目.md "wikilink")
[Category:有毒昆蟲](../Category/有毒昆蟲.md "wikilink")

1.  有些藥劑師會同樣稱其他[鞘翅目](../Page/鞘翅目.md "wikilink")[地膽科昆蟲為西班牙蒼蠅](../Page/地膽科.md "wikilink")。另外**西班牙蒼蠅**的學名有時也會稱作「*Cantharis
    vesicatoria*」，但它是[芫青屬](../Page/芫青屬.md "wikilink")（*Lytta*），並非[菊虎科的屬](../Page/菊虎科.md "wikilink")「*Cantharis*」。
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.