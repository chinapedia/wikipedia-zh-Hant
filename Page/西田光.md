**西田光**（にしだ
ひかる、）是[日本一名](../Page/日本.md "wikilink")[女優](../Page/女優.md "wikilink")、[歌手](../Page/歌手.md "wikilink")（婚後改稱衣斐光）。

## 簡介

  - 出生後10個月，因父親被派駐[美國而舉家](../Page/美國.md "wikilink")[移民到](../Page/移民.md "wikilink")[加州](../Page/加州.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")。
  - 1985年回國，1994年畢業於[上智大學](../Page/上智大學.md "wikilink")[比較文化學系](../Page/比較文化學.md "wikilink")。
  - 1998年擔任[長野](../Page/長野.md "wikilink")[奧運會大使及](../Page/奧運會.md "wikilink")[火炬手](../Page/火炬手.md "wikilink")。
  - 2002年5月，與[電氣機械會社勤務](../Page/電氣機械會社.md "wikilink")[衣斐茂樹結婚](../Page/衣斐茂樹.md "wikilink")。
  - 近年，獲[日本癌症基金會邀請擔任](../Page/日本癌症基金會.md "wikilink")『[粉紅絲帶](../Page/粉紅絲帶.md "wikilink")』宣傳大使之一，呼籲定期檢查[乳癌](../Page/乳癌.md "wikilink")。
  - 2006年8月18日晚上8時（美國[西岸時間](../Page/美國時區.md "wikilink")），在美國加州產下3885克重的男嬰（長男）。
  - 2009年11月30日下午2時37分（美國西岸時間），在美國加州產下4235克重的男嬰（次男）。

## 作品

### 單曲

  - 我們的仙迪（[小公子西迪主題曲](../Page/小公子西迪.md "wikilink")，實際上的出道作）
  - Fifteen（1988年4月6日，ハウス食品「フルーチェ」廣告歌曲，官方公佈的出道作）
  - Nice-catch\!（1988年7月21日）
  - Little Chance（1988年10月21日，花王潔面乳廣告歌曲）
  - 戀愛白T-shirt（1989年3月15日，ハウス食品「フルーチェ」廣告主題曲）
  - Natural Summer days（1989年7月5日，花王潔面乳廣告主題曲）
  - 淚之 PEARL MOON（1989年10月21日）
  - Pun Pun Pun（1990年4月4日）
  - 本領一般的我（1990年8月1日）
  - Oh my god\!\! 弊傢伙（1990年11月7日，ノーシンホワイト廣告主題曲）
  - 只想靠在心傍（1991年3月21日）
  - 心跳（1991年8月7日，TBS劇集《デパート！夏物語》主題曲）
  - 邂逅（1992年2月5日，東京電視台劇集《パパと呼ばせて\!》主題曲）
  - 活著是一件美妙的事情（1992年7月24日，NHK《西田ひかるの痛快人間伝 -Dashing life story-》節目主題曲）
  - 渴望變得光輝（1992年11月6日，TBS劇集《デパート！秋物語》插曲）
  - 淚 不停地流（1993年5月12日，三菱電器廣告主題曲）
  - 其中一定有愛（1994年5月20日，三菱電器廣告主題曲）
  - 人生變化或許在夏天（1995年3月17日，[朝日啤酒Asahi](../Page/朝日.md "wikilink") Breweries
    Z廣告主題曲）　
  - C'mon3\!（1995年8月2日，TBS劇集《夏！デパート物語》主題曲）
  - Go\!
    Paradise（1995年8月19日，[西日本旅客鐵道廣告主題曲](../Page/西日本旅客鐵道.md "wikilink")）
  - 我的NO.1～You're the only one～（1996年5月2日，三菱電器廣告主題曲）
  - Melody（1996年10月1日）
  - 啊！真的做到（1997年8月20日，ハウス食品「ピュア・イン」廣告主題曲）
      - （A面）Love is Changing（動畫[烈火之焰片尾曲](../Page/烈火之焰.md "wikilink")）
  - 漸漸變成姑娘（1997年10月17日，三菱電器廣告主題曲）
  - pure（1998年4月29日，ハウス食品「ピュア・イン」廣告主題曲）
      - （A面）我是我（NHK、BS 劇集《明天也是好天氣》主題曲
  - AS PURE AS…（1999年4月21日）
  - 空色（2000年11月1日）
  - 請給我翅膀（2002年5月12日，日韓合辦世界盃記念）

### 專輯

  - CLEAR（1988年12月7日）
  - 人影（1989年12月21日）
  - 心跳的次序（1990年12月5日）
  - Esprit（1991年10月2日）
  - 19 Dreams（1992年8月5日）
  - Sun dance（1993年7月7日）
  - Love Always（1994年8月3日）
  - VERY BEST OF HIKARU（1994年12月7日）
  - A FILE of LIFE（1995年9月6日）
  - Sophisticated 淘氣 Lady（1995年11月17日）
  - 24 two-four（1996年7月3日）
  - LIVE two-four（1997年1月18日）
  - HIKARU NISHIDA LIVE'97 Hippie Happy Groove\!\!（1998年3月4日）
  - 幸福的形態（1998年9月2日）
  - 西田光·BEST（2002年3月20日）
  - Love For All Seasons（2002年5月22日）

### Video & LD

  - P-CAN ISLAND（Video，1998年10月5日）
  - 1st Concert「真夏之SURPRISE」（Video：1990年10月21日；LD：年11月21日）
  - HIKARU NISHIDA 1991 CONCERT「真夏上手」（Video：1991年2月14日；LD：同年10月21日）
  - 「相遇在天使間 - HIKARU NISHIDA IN LOS
    ANGELES-」（LD：1991年2月14日；Video：同年10月21日）
  - NIKARU NISHIDA 1992 CONCERT「真夏上手 Vol.2
    LADY」（Video：1992年10月21日；LD：同年11月6日）
  - HIKARU NISHIDA 1994 CONCERT「Love
    Always」（Video：1994年12月16日；LD：同年11月16日）
  - HIKARU NISHIDA 1995 CONCERT「Sophisticated 淘氣
    Lady」（Video：1995年11月17日；LD：同年11月17日）
  - NISHIDA 1（Video，1996年9月20日）
  - HIKARU 2（Video，1996年9月20日）
  - HIKARU NISHIDA FALL CONCERT 1996「LIVE
    two-four」（Video：1997年1月18日；LD：同年）
  - HIKARU NISHIDA Live'97「Hippie Happy Groove\!\!」（LD，1998年3月4日）

### 演出

#### 劇集

  - NHK朝の連続電視台小説|連続電視台小説《青春家族》（1989年4-9月，[日本放送協會](../Page/日本放送協會.md "wikilink")）
  - 《いつも誰かに恋してるッ》（1990年1-3月，[富士電視台](../Page/富士電視台.md "wikilink")）
  - 《パパ\!かっこつかないぜ》（1990年7-9月，富士電視台）
  - 《いつか誰かと朝帰りッ》（1990年10-12月，富士電視台）
  - 《ピットに賭ける恋》（1991年，富士電視台）
  - 《デパート\!夏物語》（1991年7-9月，[大映電視台](../Page/大映電視台.md "wikilink")、[東京放送](../Page/東京放送.md "wikilink")）
  - 《パパと呼ばせて\!》（1992年1-3月，[東京電視台](../Page/東京電視台.md "wikilink")）
  - 《世にも奇妙な物語》《真夜中のディスクジョッキー》（1991年，富士電視台）
  - 《デパート\!秋物語》（1992年10-12月，大映電視台、東京放送）
  - 《チャンス\!》（1993年4-6月，富士電視台）
  - 《世にも奇妙な物語》《ふたり》（1994年，富士電視台）
  - 《上を向いて歩こう\!》（1994年4-6月，富士電視台）
  - 《夏\!デパート物語》（1995年7-9月，大映電視台、東京放送）
  - 朝日電視台星期四劇集《外科医
    柊又三郎2》（1996年10-12月，[朝日電視台](../Page/朝日電視台.md "wikilink")）
  - 山口放送45周年記念劇集《卒業写真》（1997年8月，[日本電視台](../Page/日本電視台.md "wikilink")，**志村由香里**）
  - 《あした天気に》（1998年9月，NHK，**市丸ちづる**）
  - 《愛，ときどき嘘》（1998年4-6月，日本電視台）
  - 《デジドラ・ワンシーン》（1998年7月，東京電視台）
  - 《もう呼ぶな海\!》（1999年2月，日本電視台）
  - 《恋愛結婚の法則》（1999年7-9月，富士電視台）
  - 《そして，友だち》（2000年1-3月，朝日電視台）
  - 火曜サスペンス劇場《鎖の微笑》（2000年3月，日本電視台，小暮奈津子、小暮有美子）
  - 《小学校教師 沢木千太郎事件ノート》（2000年7月，TBS）
  - 金曜エンタテイメント《ガーデンデザイナー春川さくら 月下美人の殺意》（2000年12月，富士電視台）
  - 大河劇《北條時宗》（2001年，NHK，**北条祝子（時宗の妻）**）
  - 《歓迎\!ダンジキ御一行様》（2001年10月，日本電視台）
  - 火曜サスペンス劇場《六月の花嫁 偽りのドレス》（2004年6月，日本電視台，新庄久美子）
  - 《お江戸吉原事件帖》(2007年10月，東京電視台，遊女、夕霧）
  - 《水戸黄門 第40部・第5話「美人剣士の婿選び・山形」》（2009年8月24日、TBS，**時枝美雪**）
  - 《生まれる。》 第5話「ママの所に来てよかった」（2011年5月20日，TBS， 西嶋沙紀）

#### 綜藝節目

  - 「鶴ちゃんのプッツン5」（1988年5月 - 1992年3月、NTV）
  - 「どーもデス\!」（1989年10月 - 1990年5月、ANB）
  - 「テレビ探偵団」（1989年10月 - 1992年3月、TBS）
  - FNS番組対抗\!なるほど\!ザ・春秋の祭典スペシャル（富士電視台）
      - いつか誰かと朝帰りッチームとして出場（1990年）、チャンス\!チームとして出場（1993年）、上を向いて歩こうチームとして出場（1994年）

#### 其他電視節目

  - 「西田ひかるの痛快人間伝 -Dashing life story-」（1991年10月 - 1993年、NHK）メインパーソナリティ
  - 「青春のポップス」（2000年3月、NHK）司儀

### 電影

  - 「[山田ババアに花束を](../Page/山田ババアに花束を.md "wikilink")」（1990年12月、[東宝](../Page/東宝.md "wikilink")）
      -
        監督：大井利夫
        共演：[山田邦子](../Page/山田邦子.md "wikilink")、ほか
        原作：花井愛子
  - 「[伊能 忠敬
    ～子午線の夢～](../Page/伊能_忠敬_～子午線の夢～.md "wikilink")」（2001年10月、[東映](../Page/東映.md "wikilink")）
      -
        監督：
        共演：
        原作：

### 配音工作

  - 電影《[X檔案](../Page/X檔案.md "wikilink")》1999年7月CS放映版（DANA SCULLY）

### 舞台劇

  - 「[小公子セディ](../Page/小公子セディ.md "wikilink")」（1988年3 - 4月、全国公演）
      -
        セディ役・主演
        演出：
        共演：[瑳川哲郎](../Page/瑳川哲郎.md "wikilink")、[安奈淳ほか](../Page/安奈淳.md "wikilink")
        脚本：
  - 「[楽園伝説](../Page/楽園伝説.md "wikilink")」（1995年7 - 8月、）
      -
        主演
        演出：
        共演：[上条恒彦](../Page/上条恒彦.md "wikilink")、[日向薫ほか](../Page/日向薫.md "wikilink")
        脚本：
  - 「[夏の庭](../Page/夏の庭.md "wikilink")」（1997年8月、）
      -
        共演：[奥田瑛二](../Page/奥田瑛二.md "wikilink")、[藤村志保ほか](../Page/藤村志保.md "wikilink")
  - 「[バルセロナ物語](../Page/バルセロナ物語.md "wikilink")」（1998年12月、2000年8 -
    9月、全国公演）
      -
        共演：[木の実ナナ](../Page/木の実ナナ.md "wikilink")、上条恒彦ほか
  - 「[シラノ・ザ・ミュージカル](../Page/シラノ・ザ・ミュージカル.md "wikilink")」（2000年12月 -
    2001年1月、）
      -
        共演：[市村正親](../Page/市村正親.md "wikilink")、[山本耕史ほか](../Page/山本耕史.md "wikilink")
  - 「[モーツァルト\!](../Page/モーツァルト!.md "wikilink")」（2002年12月）
      -
        共演：[井上芳雄](../Page/井上芳雄.md "wikilink")、市村正親、[高橋由美子ほか](../Page/高橋由美子.md "wikilink")

### 紅白歌唱比賽

  - 「ときめいて」（1991）
  - 「生きているって素晴らしい」（1992）
  - 「涙止まらない」（1993）
  - 「ザッツ・ディズニー・ファンタジー」（1998）

### 廣播劇

  - 「真里子とひかるのチャレンジ学園・夢計画」（1988年、[文化放送](../Page/文化放送.md "wikilink")）
  - 「真里子とひかるのチャレンジ学園・まるかるクラブ」（1989年、文化放送）
  - 「西田ひかるのピーカンサンルーム」（1990年4 - 9月、文化放送）
  - 「西田ひかるのJUST ONE MORE KISS」（1990年10月 - 1991年3月、文化放送）
  - 「西田ひかるのマグナムSOUND BUREEZE」（1995年4月、[TBS](../Page/TBS.md "wikilink")）

### 廣告（部分）

  - ハウス食品 「フルーチェ」ほか（1988 - 1997年）
  - JAS 「'88コーラルアイランドキャンペーン」（1988年）
  - アラクス 「ノーシンホワイト」（1990 - 1992年）なおりたガール
  - JR西日本（1990 - 1997年）
  - 花王 「ビオレ洗顔フォーム」
  - 三菱電機 各種暖房機器、電話機等（1992 - 1998年）
  - CASIO 時計
  - ブリヂストン 「ドーナツ（タイヤ）」
  - アサヒビール 生ビール「Z」（1995年）
  - ECCジュニア（1996年 - 2000年）
  - マイクロソフト オフィス97（1997年）
  - コニカ コニカカラー各種イメージキャラクター
  - ライオン 洗濯洗剤「トップ」（2004年）
  - 阪急電鉄・HANA PLUSカード（2006年）

## 書籍

### 寫真集

  - 「P-CAN ISLAND」（1988年9月、[扶桑社](../Page/扶桑社.md "wikilink")）
  - 「Hi-tide」（1990年2月、[ワニブックス](../Page/ワニブックス.md "wikilink")）
  - 「In your dreams」（1991年4月、ワニブックス）
  - 「19 Dreams」（1992年10月、[講談社](../Page/講談社.md "wikilink")）
  - 「Making of N.H」（1994年8月、ワニブックス）ポストカード及びビデオ付き
  - 「[月刊
    西田ひかる](../Page/「月刊」シリーズ.md "wikilink")」（2000年6月、[新潮社](../Page/新潮社.md "wikilink")）（写真
    [藤代冥砂](../Page/藤代冥砂.md "wikilink")）

### 翻譯書籍

  - 「HUG」（1992年12月5日、扶桑社）初訳本
  - 「HUG II」（1993年4月20日、扶桑社）初訳本
  - 「イヌの飼いかた」（1997年6月27日、講談社）
  - 「ネコの飼いかた」（1997年6月27日、講談社）
  - 「ケイト・グリーナウェイのマザーグース」（2002年4月9日、[飛鳥新社](../Page/飛鳥新社.md "wikilink")）
  - 「おさぼりメードの不思議な世界」（2002年4月、[中経出版](../Page/中経出版.md "wikilink")）

### 其他書籍

  - 「ハートのレッスン」（1996年12月18日、ワニブックス）
  - 「サンタのびっくり絵本」（1999年11月4日、[小学館](../Page/小学館.md "wikilink")）
  - 「おいしく食べたい 朝も夜も」（2002年4月5日、講談社）
  - 「カリフォルニアの空から」（2006年10月30日、[産経新聞社](../Page/産経新聞社.md "wikilink")）

## 獎項

  - 第28回[ゴールデン・アロー賞グラフ賞](../Page/ゴールデン・アロー賞.md "wikilink")
    （1991年2月、日本雑誌協会）
  - 第14回[日本電影金像獎新人俳優賞](../Page/日本電影金像獎.md "wikilink")（1991年3月、日本電影金像獎協会）
  - 第8回[ベストジーニスト](../Page/ベストジーニスト.md "wikilink")'91（1991年5月、日本ジーンズメーカー協議会）
  - 「チューリップの似合う人」第一位（1994年3月、オランダ国際花き球根協会）
  - 第24回[ベストドレッサー賞受賞](../Page/ベストドレッサー賞.md "wikilink")（1995年12月、日本メンズファッション協会）
  - 第33回ゴールデン・アロー賞演劇新人賞 及び最優秀新人賞（1996年2月、日本雑誌協会）

## 相關連結

  - [公式網頁 Site「Welcome to Hikaru Nishida Home
    Page」](http://www.nishida-hikaru.com/)
  - [公式BLOG「In the Sun」](http://yaplog.jp/hik816/)

[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")
[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:上智大學校友](../Category/上智大學校友.md "wikilink")
[Category:日本電影學院獎新人獎得主](../Category/日本電影學院獎新人獎得主.md "wikilink")