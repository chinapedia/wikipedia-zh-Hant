**[西科斯基](../Page/西科斯基飛行器公司.md "wikilink")
S-42**是第一種真正能夠越洋飛行的[水上飛機](../Page/水上飛機.md "wikilink")。只為[泛美航空制造過](../Page/泛美航空.md "wikilink")10架，於1934年3月30日首飛。S-42亦被稱為**飛行飛剪船**（**Flying
Clipper**）和**泛美飛剪船**（**Pan Am Clipper**）。

## 規格 (S-42A)

[Sikorsky_S42_(crop).jpg](https://zh.wikipedia.org/wiki/File:Sikorsky_S42_\(crop\).jpg "fig:Sikorsky_S42_(crop).jpg")

## 參考資料

  - [Fagan, Dave. 'Hamble' *Aviation in Hampshire UK 1900 to
    2000*](https://archive.is/20120524155014/daveg4otu.tripod.com/airfields/ham.html)

## 外部連結

  - [Sikorsky S-42''](http://www.flyingclippers.com/S42.html)

[Category:賽考斯基飛行器](../Category/賽考斯基飛行器.md "wikilink")
[Category:民航飛機](../Category/民航飛機.md "wikilink")
[Category:美國航空器](../Category/美國航空器.md "wikilink")