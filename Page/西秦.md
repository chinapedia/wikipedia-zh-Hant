**西秦**（385年-400年，409年-431年）是中国历史上[十六国时期](../Page/十六国.md "wikilink")[鲜卑人](../Page/鲜卑人.md "wikilink")[乞伏國仁建立的政权](../Page/乞伏國仁.md "wikilink")。其国号“秦”以地处战国时[秦国故地为名](../Page/秦国.md "wikilink")。《[十六国春秋](../Page/十六国春秋.md "wikilink")》始用西秦之称，以别于[前秦和](../Page/前秦.md "wikilink")[后秦](../Page/后秦.md "wikilink")，后世袭用之\[1\]。

## 历史

公元385年，鲜卑酋长[乞伏国仁在陇西称大单于](../Page/乞伏国仁.md "wikilink")，又被[前秦封为苑川王](../Page/前秦.md "wikilink")，都勇士川（今[甘肃](../Page/甘肃.md "wikilink")[榆中](../Page/榆中.md "wikilink")）。388年，其弟[乞伏-{乾}-歸立](../Page/乞伏乾歸.md "wikilink")，称大单于，河南王，迁都金城（今甘肃[兰州西](../Page/兰州.md "wikilink")）。400年為[後秦所滅](../Page/後秦.md "wikilink")。409年，二月，乞伏乾归自后秦返回苑川。七月，西秦复国，复都苑川。412年，[乞伏熾磐又迁都枹罕](../Page/乞伏熾磐.md "wikilink")（今甘肃[临夏市东北](../Page/临夏市.md "wikilink")）。

最盛时期，其统治范围包括甘肃西南部，青海部分地区。

431年被[夏國所灭](../Page/夏國.md "wikilink")。

## 君主列表

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p><a href="../Page/廟號.md" title="wikilink">廟號</a></p></th>
<th><p><a href="../Page/諡號.md" title="wikilink">諡號</a></p></th>
<th><p>統治時間</p></th>
<th><p><a href="../Page/年號.md" title="wikilink">年號</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/乞伏國仁.md" title="wikilink">乞伏國仁</a></p></td>
<td><p>烈祖</p></td>
<td><p>宣烈王</p></td>
<td><p>385年-388年</p></td>
<td><p><a href="../Page/建義_(西秦).md" title="wikilink">建義</a> 385年-388年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乞伏乾歸.md" title="wikilink">乞伏-{乾}-歸</a></p></td>
<td><p>高祖</p></td>
<td><p>武元王</p></td>
<td><p>388年-400年<br />
409年-412年</p></td>
<td><p><a href="../Page/太初_(西秦).md" title="wikilink">太初</a> 388年-400年<br />
<a href="../Page/更始_(西秦).md" title="wikilink">更始</a> 409年-412年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/乞伏熾磐.md" title="wikilink">乞伏熾磐</a></p></td>
<td><p>太祖</p></td>
<td><p>文昭王</p></td>
<td><p>412年-428年</p></td>
<td><p><a href="../Page/永康_(乞伏熾磐).md" title="wikilink">永康</a> 412年-419年<br />
<a href="../Page/建弘.md" title="wikilink">建弘</a> 420年-428年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乞伏暮末.md" title="wikilink">乞伏暮末</a></p></td>
<td><p>無</p></td>
<td><p>無</p></td>
<td><p>428年-431年</p></td>
<td><p><a href="../Page/永弘.md" title="wikilink">永弘</a> 428年-431年</p></td>
</tr>
</tbody>
</table>

### 头衔

  - 单于385年9月－387年3月
  - 苑川王387年3月－388年6月
  - 河南王388年6月－389年1月
  - 金城王389年1月－394年1月
  - 河南王394年1月－394年6月
  - 梁王394年6月－394年12月
  - 秦王394年12月－400年7月
  - 秦王409年7月－411年1月
  - 河南王411年1月－414年10月
  - 秦王414年10月－431年1月

## 世系图

<center>

</center>

## 西秦都城

公元385年，[乞伏国仁建立政权时](../Page/乞伏国仁.md "wikilink")，筑[勇士城](../Page/勇士城.md "wikilink")，为西秦第一个都城。

公元388年，[乞伏乾归继位后](../Page/乞伏乾归.md "wikilink")，将都城迁到[金城](../Page/金城郡.md "wikilink")(今[甘肃](../Page/甘肃.md "wikilink")[兰州西固城西](../Page/兰州.md "wikilink"))。

公元395年，都城南景门倒塌。出于忌讳，乞伏乾归又将西秦都城迁至西城(今[甘肃](../Page/甘肃.md "wikilink")[白银市境内](../Page/白银市.md "wikilink"))。

公元400年，乞伏乾归降于[后秦](../Page/后秦.md "wikilink")[姚兴](../Page/姚兴.md "wikilink")，并又将都城迁回[苑川](../Page/苑川.md "wikilink")(今[甘肃](../Page/甘肃.md "wikilink")[榆中县北](../Page/榆中县.md "wikilink"))。

公元409年，二月，乞伏乾归自后秦返回[苑川](../Page/苑川.md "wikilink")。七月，西秦复国，临时都城为[度堅城](../Page/度堅城.md "wikilink")(今[甘肃](../Page/甘肃.md "wikilink")[靖远县](../Page/靖远县.md "wikilink"))。

公元410年，复都苑川。

公元412年，[乞伏炽磐继位后](../Page/乞伏炽磐.md "wikilink")，迁都[枹罕](../Page/枹罕.md "wikilink")(今临夏)。直至西秦灭。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[Category:中國古代民族與國家](../Category/中國古代民族與國家.md "wikilink")
[Category:西秦](../Category/西秦.md "wikilink")
[Category:五胡十六国](../Category/五胡十六国.md "wikilink")
[Category:385年建立的國家或政權](../Category/385年建立的國家或政權.md "wikilink")
[Category:409年建立的國家或政權](../Category/409年建立的國家或政權.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")

1.