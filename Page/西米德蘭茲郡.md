**西密德蘭郡**（），[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[西密德蘭區域的](../Page/西密德蘭.md "wikilink")[郡](../Page/英格蘭的郡.md "wikilink")，範圍像舉起[拇指的拳頭向右臥](../Page/拇指.md "wikilink")。1974年前原屬[沃里克郡](../Page/沃里克郡.md "wikilink")、[伍斯特郡](../Page/伍斯特郡.md "wikilink")、[斯塔福德郡範圍](../Page/斯塔福德郡.md "wikilink")。以人口計算，[伯明翰是第](../Page/伯明翰.md "wikilink")1大[城市](../Page/英國的城市地位.md "wikilink")、第1大[都市自治市](../Page/都市自治市.md "wikilink")，[考文垂是第](../Page/考文垂.md "wikilink")2大城市、第2大都市自治市，[伍爾弗漢普頓是第](../Page/伍爾弗漢普頓.md "wikilink")3大城市、第3大都市自治市；[達德利是第](../Page/達德利.md "wikilink")1大鎮，[沃爾索爾是第](../Page/沃爾索爾.md "wikilink")2大鎮，[西布罗姆维奇是第](../Page/西布罗姆维奇.md "wikilink")3大鎮（詳見[地方人口段落](../Page/西密德蘭郡#地方.md "wikilink")）。

## 地方沿革

### 1966年

1966年，西密德蘭地區的[郡自治市鎮](../Page/郡自治市鎮.md "wikilink")（County
borough）兼併鄰近的[市自治市鎮](../Page/市自治市鎮.md "wikilink")（Municipal
borough）、鎮、村：

  - [伯明翰](../Page/伯明翰.md "wikilink")：沒有大變動
  - [索利哈爾](../Page/索利哈爾.md "wikilink")：沒有大變動
  - [達德利](../Page/達德利.md "wikilink")：兼併[布賴爾希爾](../Page/布賴爾希爾.md "wikilink")
    ，大部分Coseley、[塞奇利](../Page/塞奇利.md "wikilink")
    ，小部分[安布爾科特](../Page/安布爾科特.md "wikilink")
    、[蒂普頓](../Page/蒂普頓.md "wikilink")
    、[羅利里吉斯](../Page/羅利里吉斯.md "wikilink") 。
  - [沃爾索爾](../Page/沃爾索爾.md "wikilink")：兼併[達拉頓](../Page/達拉頓.md "wikilink")
    ，大部分[威倫霍爾](../Page/威倫霍爾.md "wikilink")
    ，小部分[温斯伯利](../Page/温斯伯利.md "wikilink")
    、Coseley、[温斯菲爾德](../Page/温斯菲爾德.md "wikilink")
    、[比爾斯頓](../Page/比爾斯頓.md "wikilink") 。
  - [沃利](../Page/沃利.md "wikilink")：新設，由[斯梅斯威克](../Page/斯梅斯威克.md "wikilink")
    、[奧爾德伯里](../Page/奧爾德伯里.md "wikilink")
    、大部分[羅利里吉斯](../Page/羅利里吉斯.md "wikilink")、[達德利](../Page/達德利.md "wikilink")、[蒂普頓](../Page/蒂普頓.md "wikilink")、[西布羅姆維奇](../Page/西布羅姆維奇.md "wikilink")、[黑爾斯歐文](../Page/黑爾斯歐文.md "wikilink")
    合併而成。
  - [西布羅姆維奇](../Page/西布羅姆維奇.md "wikilink")：兼併大部分[温斯伯利](../Page/温斯伯利.md "wikilink")、[蒂普頓](../Page/蒂普頓.md "wikilink")，小部分[比爾斯頓](../Page/比爾斯頓.md "wikilink")、[奧爾德伯里](../Page/奧爾德伯里.md "wikilink")、[斯梅斯威克](../Page/斯梅斯威克.md "wikilink")、[沃爾索爾](../Page/沃爾索爾.md "wikilink")
  - [伍爾弗漢普頓](../Page/伍爾弗漢普頓.md "wikilink")：兼併大部分[比爾斯頓](../Page/比爾斯頓.md "wikilink")、[温斯菲爾德](../Page/温斯菲爾德.md "wikilink")、[蒂頓霍爾](../Page/蒂頓霍爾.md "wikilink")
    ，小部分[塞奇利](../Page/塞奇利.md "wikilink")、Coseley、[威倫霍爾](../Page/威倫霍爾.md "wikilink")。

### 1974年

1974年3月31日或以前，西密德蘭地方原屬[沃里克郡的西部範圍](../Page/沃里克郡.md "wikilink")。《[1972年地方政府法案](../Page/1972年地方政府法案.md "wikilink")》在1974年4月1日生效後，它脫離沃里克郡，升格為新設的[都市郡](../Page/都市郡.md "wikilink")，取名為「西密德蘭」，與所屬的[英格蘭區域同名](../Page/英格蘭的區域.md "wikilink")。西密德蘭郡的設立，使沃里克郡的範圍呈畸形。西密德蘭下轄的5個次級行政區都是[都市自治市](../Page/都市自治市.md "wikilink")（也就是[單一管理區](../Page/單一管理區.md "wikilink")），有獨立的都市自治市議會，實際不受西密德蘭的管轄。

每一個[名譽郡都有一個代表](../Page/名譽郡.md "wikilink")[英國皇室但沒有實權的](../Page/英國皇室.md "wikilink")[郡尉](../Page/郡尉_\(英國\).md "wikilink")（Lord
Lieutenant）常駐，西密德蘭是其一。從地方政府或郡尉的角度看，西密德蘭都只是「有郡界的地理範圍」而已。

### 1980年代－

  - 西密德蘭議會在1986年被廢除。不過，都市自治市仍以「西密德蘭」名義（少部份部門直接以都市自治市的名義處理郡級事務）組成聯合部門（Joint-boards）統籌、協調牽涉多個都市自治市的民政事務，負責警務、消防、交通等。
  - 1994年4月1日，原屬西密德蘭[沃利索爾都市自治市的](../Page/沃利索爾都市自治市.md "wikilink")[蔡斯沃特](../Page/蔡斯沃特.md "wikilink")（Chasewater）[水塘改歸](../Page/水塘.md "wikilink")[斯塔福德郡](../Page/斯塔福德郡.md "wikilink")[利奇菲爾德區管轄](../Page/利奇菲爾德區.md "wikilink")。西密德蘭的水面[面積少了](../Page/面積.md "wikilink")3[平方公里](../Page/平方公里.md "wikilink")。
  - 1995年，原屬[伍斯特郡](../Page/伍斯特郡.md "wikilink")[雅芳河畔斯特拉特福區的](../Page/雅芳河畔斯特拉特福區.md "wikilink")[弗蘭克利](../Page/弗蘭克利.md "wikilink")（Frankley）和[巴特利水塘](../Page/巴特利水塘.md "wikilink")（Bartley
    Reservoir）改歸西密德蘭[伯明翰管轄](../Page/伯明翰.md "wikilink")。

## 地方政府

雖然西密德蘭議會被廢除，但都市自治市仍以「西密德蘭」名義組成聯合部門（Joint-boards）統籌、協調牽涉多個都市自治市的民政事務：

  - [西密德蘭警察局](../Page/西密德蘭警察局.md "wikilink")
  - [西密德蘭消防局](../Page/西密德蘭消防局.md "wikilink")
  - [西密德蘭旅客運輸執行委員會](../Page/西密德蘭旅客運輸執行委員會.md "wikilink")

## 行政區劃

[ 1. [伍爾弗漢普頓](../Page/伍爾弗漢普頓.md "wikilink")
2\. [達德利](../Page/達德利都市自治市.md "wikilink")
3\. [沃爾索爾](../Page/沃爾索爾都市自治市.md "wikilink")
4\. [桑德韋爾](../Page/桑德韋爾都市自治市.md "wikilink")
5\. [伯明翰](../Page/伯明翰.md "wikilink")
6\. [索利哈爾](../Page/索利哈爾都市自治市.md "wikilink")
7\. [考文垂](../Page/考文垂.md "wikilink")
](https://zh.wikipedia.org/wiki/File:West_Midlands_numbered_districts.svg "fig: 1. 伍爾弗漢普頓 2. 達德利 3. 沃爾索爾 4. 桑德韋爾 5. 伯明翰 6. 索利哈爾 7. 考文垂 ")
西密德蘭包含7個[都市自治市](../Page/都市自治市.md "wikilink")（也就同時是單一管理區）：[伍爾弗漢普頓](../Page/伍爾弗漢普頓.md "wikilink")、[達德利](../Page/達德利都市自治市.md "wikilink")（Dudley）、[沃爾索爾](../Page/沃爾索爾都市自治市.md "wikilink")（Walsall）、[桑德韋爾](../Page/桑德韋爾都市自治市.md "wikilink")（Sandwell）、[伯明翰](../Page/伯明翰.md "wikilink")、[索利哈爾](../Page/索利哈爾都市自治市.md "wikilink")（Solihull）、[考文垂](../Page/考文垂.md "wikilink")。

### 地方

西密德蘭是[都市郡](../Page/都市郡.md "wikilink")，人口超過40000的地方較[非都市郡多數倍](../Page/非都市郡.md "wikilink")：

<table>
<thead>
<tr class="header">
<th><p>地名</p></th>
<th><p>地位</p></th>
<th><p>人口</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/伯明翰.md" title="wikilink">伯明翰</a></p></td>
<td><p><a href="../Page/英國的城市地位.md" title="wikilink">城市</a><br />
<a href="../Page/都市自治市.md" title="wikilink">都市自治市</a></p></td>
<td><p>1,006,500</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/考文垂.md" title="wikilink">考文垂</a></p></td>
<td><p>306,600</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伍爾弗漢普頓.md" title="wikilink">伍爾弗漢普頓</a></p></td>
<td><p>236,600</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/達德利.md" title="wikilink">達德利</a></p></td>
<td><p>鎮</p></td>
<td><p>194919</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沃爾索爾.md" title="wikilink">沃爾索爾</a></p></td>
<td><p>174994</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西布罗姆维奇.md" title="wikilink">西布罗姆维奇</a></p></td>
<td><p>136940</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/索利哈爾.md" title="wikilink">索利哈爾</a></p></td>
<td><p>94753</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑爾斯歐文.md" title="wikilink">黑爾斯歐文</a></p></td>
<td><p>57918</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯陶爾布里奇.md" title="wikilink">斯陶爾布里奇</a></p></td>
<td><p>54661</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布洛克斯維奇.md" title="wikilink">布洛克斯維奇</a></p></td>
<td><p>40000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/威倫霍爾.md" title="wikilink">威倫霍爾</a></p></td>
<td><p>40000</p></td>
<td></td>
</tr>
</tbody>
</table>

## 地理

下圖顯示英格蘭48個名譽郡的分佈情況。西密德蘭東北、東、東南與[沃里克郡相鄰](../Page/沃里克郡.md "wikilink")，西南與[伍斯特郡相鄰](../Page/伍斯特郡.md "wikilink")。

## 註釋

## 外部連結

  - [西密德蘭郡聯合委員會](https://web.archive.org/web/20060626195956/http://westmidlandsltp.gov.uk/default.php?id=295)

[西米德蘭茲郡](../Category/西米德蘭茲郡.md "wikilink")
[Category:英格蘭的郡](../Category/英格蘭的郡.md "wikilink")
[Category:都市郡](../Category/都市郡.md "wikilink")