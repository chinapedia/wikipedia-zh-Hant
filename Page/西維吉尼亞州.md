**西維吉尼亞州**（），又译为**-{zh-hans:西维吉尼亚州;zh-hk:西維吉尼亞州;zh-tw:西維吉尼亞州;}-**，簡稱**西維州**，是[美國東部的一個州](../Page/美國.md "wikilink")，有著名的[阿帕拉契山脈](../Page/阿帕契山脉.md "wikilink")，景觀優美，別稱為「山脈之州」。

現仍有相當多人認為西維吉尼亞州屬於[美國南方](../Page/美國南部.md "wikilink")，在西維吉尼亞州北邊長臂處卻已經充滿了美國北方色彩，與此州南邊的文化大為不同。西維吉尼亞州是在美國南北戰爭時期由當時的維吉尼亞州所分裂出來的。今日的西維吉尼亞州以[煤礦廢墟聞名](../Page/煤.md "wikilink")，也在美國[工會運動史上占有重要地位](../Page/工會.md "wikilink")。

西維吉尼亞州的天然景觀由於地勢起伏較大的關係，相當豐富，旅遊勝地包括有[新河峽大橋](../Page/新河峽大橋.md "wikilink")（New
River Gorge Bridge）長3030呎，高876呎，全美第二高，在聯邦規定的築橋紀念日（Bridge
day）時，准許跳傘跟高空彈跳。此外也有許多國家公園和34座州立公園。

西弗吉尼亚州一共有55個[縣](../Page/縣.md "wikilink")：[西弗吉尼亚州行政区划](../Page/西弗吉尼亚州行政区划.md "wikilink")。

西維吉尼亞州同時也是[美国国家射电天文台](../Page/美国国家射电天文台.md "wikilink")（National Radio
Astronomy Observatory）放置綠堤天文望遠鏡（Green Bank Telescope）之處。

## 歷史

在17世纪70年代河狸战争期间，[易洛魁人从该地区驱赶出其他美洲印第安部落](../Page/易洛魁联盟.md "wikilink")，以保留上俄亥俄谷作为狩猎场。
诸如Moneton的苏族语言部落也被记录曾在该地区活动。

西維吉尼亞的第一個有記載的[殖民者是](../Page/殖民.md "wikilink")1726年的。

西弗吉尼亚原屬[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")，1861年4月，[美国内战爆发](../Page/美国内战.md "wikilink")，弗吉尼亚州退出[美利堅合眾國](../Page/美利堅合眾國.md "wikilink")，改加入[美利堅聯盟國](../Page/美利堅聯盟國.md "wikilink")。弗吉尼亚州西北部四十八[郡](../Page/郡.md "wikilink")，由于经济结构和东部不同，[奴隶制并不普及](../Page/奴隶制.md "wikilink")，所以并不支持从[联邦分离](../Page/联邦.md "wikilink")，時稱「西弗吉尼亚」。[合眾國的](../Page/合眾國.md "wikilink")[北軍在](../Page/北軍.md "wikilink")[麦克莱伦的领导下](../Page/麦克莱伦.md "wikilink")，于该年夏天控制了现在的西弗吉尼亚。1863年《惠靈公約》簽訂，西弗吉尼亚被[美國国会纳为美国的第](../Page/美國国会.md "wikilink")35个州。

## 法律與政府

西維吉尼亞州的立法採[兩院制](../Page/兩院制.md "wikilink")，有[眾議院和](../Page/眾議院.md "wikilink")[參議院](../Page/參議院.md "wikilink")。立法者是兼任制，而非[全職制](../Page/全職.md "wikilink")，簡要的說，就是立法者自己本身在他們所屬的[社區擁有全職的工作](../Page/社區.md "wikilink")。此點讓西維吉尼亞州與鄰州如賓州、[馬里蘭州](../Page/馬里蘭州.md "wikilink")、[俄亥俄州相當不同](../Page/俄亥俄州.md "wikilink")。

## 地理

西維吉尼亞州的[地理基本上分為三塊](../Page/地理.md "wikilink")，分別是：[阿帕拉契高原](../Page/阿帕拉契高原.md "wikilink")、[阿帕拉契山脊與](../Page/阿帕拉契山.md "wikilink")[山谷區](../Page/山谷.md "wikilink")（或大阿帕拉契山谷）、以及[東部狹長區](../Page/東.md "wikilink")。其中阿帕拉契高原占全洲80%的[面積](../Page/面積.md "wikilink")。西維吉尼亞州的主要[河流則包括有](../Page/河.md "wikilink")[俄亥俄河](../Page/俄亥俄河.md "wikilink")、[大珊蒂河](../Page/大珊蒂河.md "wikilink")、[普塔麥克河](../Page/普塔麥克河.md "wikilink")、以及[雪蘭多河](../Page/謝南多亞河.md "wikilink")，本州大部分的自然資源均可在此區域發現。介於阿帕拉契高原與阿帕拉契山脊與山谷區，為一道名為阿拉根尼‧富壤特（Allegheny
Front）的險峻山牆。東部重重森林山脊區，被狹隘的山谷區分為阿帕拉契山脊與山谷區。蘋果與桃子果樹盛產於東部狹長地帶的豐饒沃土區：藍嶺山區（Blue
Ridge Mountains）\[1\]。

## 旅遊與觀光資訊

[缩略图](https://zh.wikipedia.org/wiki/File:UH-60_Black_Hawk_006_\(9311999721\).jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Ginos-Huntington-WV.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Coal_cars_near_the_company_store._Gilliam_Coal_and_Coke_Co,_Gilliam_Mine,_Gilliam,_McDowell_County,_West_Virginia._-_NARA_-_540828.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Summersvilledamspillway.jpg "fig:缩略图")

  - 新河峽國家公園（New River Gorge National Park）
  - 查爾斯城（Charles Town）
  - 卡佛爾德大橋（Covered Bridge）
  - 格來福溪丘（Grave Creek Mound）
  - 貝克利煤礦展示場（Beckley Exhibition Coal Mine）
  - 凱斯·西尼克鐵路（Cass Scenic Rail Road）
  - 哈普斯渡船口海岬（The Point of Harpers Ferry）
  - 馬里蘭高地小徑（Maryland Heights Trail）
  - 藍嵴山脈（Blue Ridge Mountains）
  - 柏克萊泉（Berkeley Springs）：美國第一個洗三溫暖的地區，美國總統喬治·華盛頓曾常來拜訪。
  - 南北戰爭的古戰場（Antietam）

## 經濟

根据2009年[世界银行的预测](../Page/世界银行.md "wikilink")，西弗吉尼亚州的经济名义上将是全球第62大经济体，落后于[伊拉克](../Page/伊拉克.md "wikilink")，超过克罗地亚。\[2\]根据2010年11月经济分析局的报告，该州2009年的预计名义GSP为633.4亿美元，实际GSP为550.4亿美元。

西維吉尼亞州盛產[煤礦](../Page/煤.md "wikilink")，採礦歷史悠久，並曾發生過多起[工會運動事件](../Page/工會.md "wikilink")，是美國煤礦史的重要地點。
主要矿产为烟煤、天然气、盐等。主要产业为能源和制造业。该州天然气丰富，年产煤1.5亿吨、天然气52亿立方米、原油190万桶，且是全美硬木主要供应来源。制造业主要以化学、金属及玻璃为主，新兴产业则有[汽车零件](../Page/汽车零件.md "wikilink")、[資訊科技](../Page/信息技术.md "wikilink")、[木工業](../Page/木工.md "wikilink")，[塑胶及](../Page/塑料.md "wikilink")[合成纤维最著名](../Page/合成纖維.md "wikilink")\[3\]。

## 人口

依據美國2003年的人口普查，西維吉尼亞州大概是美國各州中移民第二少的州。
此州只有1.1%的居民是外國出生的。此外，西維吉尼亞州是全美「英語非母語」人數最少的州，根據資料，本州英語非母語的人口僅有2.7%。根据2014年的預測，西弗吉尼亚州总人口为185萬0326人，在50州内排名38位，相对于2010年普查结果下降0.14%\[4\]。

<table>
<caption><strong>西維吉尼亞州種組成</strong></caption>
<thead>
<tr class="header">
<th><p>种族组成</p></th>
<th><p>1990[5]</p></th>
<th><p>2000[6]</p></th>
<th><p>2010[7]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/白種人.md" title="wikilink">白種人</a></p></td>
<td><p>96.2%</p></td>
<td><p>95.0%</p></td>
<td><p>93.9%</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑人.md" title="wikilink">黑人</a></p></td>
<td><p>3.1%</p></td>
<td><p>3.2%</p></td>
<td><p>3.4%</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亞裔.md" title="wikilink">亞裔</a></p></td>
<td><p>0.4%</p></td>
<td><p>0.5%</p></td>
<td><p>0.7%</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美國原住民.md" title="wikilink">美國原住民</a></p></td>
<td><p>0.1%</p></td>
<td><p>0.2%</p></td>
<td><p>0.2%</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏威夷原住民.md" title="wikilink">夏威夷原住民</a> 和<br />
<a href="../Page/太平洋岛民.md" title="wikilink">其他太平洋岛民</a></p></td>
<td><p>–</p></td>
<td><p>–</p></td>
<td><p>–</p></td>
</tr>
<tr class="even">
<td><p>其他</p></td>
<td><p>0.1%</p></td>
<td><p>0.2%</p></td>
<td><p>0.3%</p></td>
</tr>
<tr class="odd">
<td><p>混血</p></td>
<td><p>–</p></td>
<td><p>0.9%</p></td>
<td><p>1.5%</p></td>
</tr>
</tbody>
</table>

## 重要城鎮

西維吉尼亞州的重要城鎮有本州的首府[查理斯敦](../Page/查尔斯顿_\(西弗吉尼亚州\).md "wikilink")（Charleston）、交通大城[亨廷顿](../Page/亨廷顿_\(西弗吉尼亚州\).md "wikilink")（Huntington）、[惠靈](../Page/惠灵_\(西弗吉尼亚州\).md "wikilink")（Wheeling）、以及[帕克斯堡](../Page/帕克斯堡_\(西維吉尼亞州\).md "wikilink")（Parkersburg）。

[NewRiverGorgeBridgeWV.jpg](https://zh.wikipedia.org/wiki/File:NewRiverGorgeBridgeWV.jpg "fig:NewRiverGorgeBridgeWV.jpg")
县 | city_1 = 查尔斯顿 (西弗吉尼亚州){{\!}}查尔斯顿 | div_1 = 卡诺瓦县{{\!}}卡诺瓦 | pop_1
= 50,404 | img_1 = Charleston WV skyline.jpg | city_2 = Huntington,
West Virginia{{\!}}Huntington | div_2 = Cabell County, West
Virginia{{\!}}Cabell | pop_2 = 48,807 | img_2 = Marshall University
Old Main Building.jpg | city_3 = Morgantown, West
Virginia{{\!}}Morgantown | div_3 = Monongalia County, West
Virginia{{\!}}Monongalia | pop_3 = 31,073 | img_3 = MotownDOWNTOWN.PNG
| city_4 = Parkersburg, West Virginia{{\!}}Parkersburg | div_4 = Wood
County, West Virginia{{\!}}Wood | pop_4 = 30,981 | img_4 = Boreman
view.jpg | city_5 = Wheeling, West Virginia{{\!}}Wheeling | div_5 =
Ohio County, West Virginia{{\!}}Ohio | pop_5 = 27,790 | img_5 = |
city_6 = Weirton, West Virginia{{\!}}Weirton | div_6 = Hancock County,
West Virginia{{\!}}Hancock | pop_6 = 19,362 | img_6 = | city_7 =
Fairmont, West Virginia{{\!}}Fairmont | div_7 = Marion County, West
Virginia{{\!}}Marion | pop_7 = 18,740 | img_7 = | city_8 =
Martinsburg, West Virginia{{\!}}Martinsburg | div_8 = Berkeley County,
West Virginia{{\!}}Berkeley | pop_8 = 17,743 | img_8 = | city_9 =
Beckley, West Virginia{{\!}}Beckley | div_9 = Raleigh County, West
Virginia{{\!}}Raleigh | pop_9 = 17,238 | img_9 = | city_10 =
Clarksburg, West Virginia{{\!}}Clarksburg | div_10 = Harrison County,
West Virginia{{\!}}Harrison | pop_10 = 16,242 | img_10 = }}

## 交通

### 重要高速公路

  - [64號州際公路](../Page/64號州際公路.md "wikilink")
  - [68號州際公路](../Page/68號州際公路.md "wikilink")
  - [70號州際公路](../Page/70號州際公路.md "wikilink")
  - [77號州際公路](../Page/77號州際公路.md "wikilink")
  - [79號州際公路](../Page/79號州際公路.md "wikilink")
  - [81號州際公路](../Page/81號州際公路.md "wikilink")

## 教育

### 大學

<table>
<tbody>
<tr class="odd">
<td><h4 id="州立大學">州立大學</h4>
<ul>
<li><a href="../Page/西維吉尼亞大學.md" title="wikilink">西維吉尼亞大學</a></li>
<li><a href="../Page/馬歇爾大學.md" title="wikilink">馬歇爾大學</a></li>
<li></li>
<li><a href="../Page/康科德.md" title="wikilink">肯寇爾德學院</a></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li><a href="../Page/西弗吉尼亞大學理工學院.md" title="wikilink">西弗吉尼亞大學理工學院</a></li>
<li></li>
<li></li>
</ul></td>
<td><h4 id="私立大學">私立大學</h4>
<ul>
<li></li>
<li></li>
<li><a href="../Page/戴维斯暨埃尔金斯学院.md" title="wikilink">戴维斯暨埃尔金斯学院</a></li>
<li><a href="../Page/查爾斯頓大學.md" title="wikilink">查爾斯頓大學</a></li>
<li>賽倫大學</li>
<li></li>
<li></li>
<li><a href="../Page/惠靈耶穌會大學.md" title="wikilink">惠靈耶穌會大學</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 重要體育團體

### 美式足球

  - [NCAA](../Page/NCAA.md "wikilink")
      - 西维吉尼亚大学（West Virginia University）（Mountaineers）
          - 馬歇爾大學（Marshall University ) (HERDS)

### 篮球

  - NCAA
      - 西维吉尼亚大学（West Virginia University）（Mountaineers）

### 棒球

西維吉尼亞州的[小聯盟棒球隊代表是](../Page/小聯盟.md "wikilink")：

  - 藍田金鶯（Bluefield
    Orioles，新人級阿帕拉契聯盟，母隊：[巴爾的摩金鶯](../Page/巴爾的摩金鶯.md "wikilink")）─是西維吉尼亞州和[維吉尼亞州兩個同名的藍田市市隊代表](../Page/維吉尼亞州.md "wikilink")，主場比賽在[維吉尼亞州的藍田市](../Page/維吉尼亞州.md "wikilink")）
  - 普林斯頓蝠魟（Princeton Devil
    Rays，新人級阿帕拉契聯盟，母隊：[坦帕灣蝠魟](../Page/坦帕灣蝠魟.md "wikilink")）
  - 西維吉尼亞力量（West Virginia
    Power，1A級南大西洋聯盟，母隊：[密爾瓦基釀酒人](../Page/密爾瓦基釀酒人.md "wikilink")─主場地在[查爾斯頓](../Page/查尔斯顿_\(西弗吉尼亚州\).md "wikilink")）

### 曲棍球

小聯盟曲棍球隊是：

  - 威靈釘工隊（Wheeling Nailers）

## 其他資訊

西維吉尼亞州的[威爾頓市](../Page/威爾頓市.md "wikilink")（Weirton），為美國唯一橫跨兩州州界的城鎮。

## 参考文献

## 外部連結

  - [The Charleston Gazette](http://www.wvgazette.com) 日報
  - [西维吉尼亚大学 West Virginia University](http://www.wvu.edu/)
  - [Search for Public Schools](http://www.searchforpublicschools.com/)

## 参见

  - [弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")

{{-}}

[Category:美国州份](../Category/美国州份.md "wikilink")
[西弗吉尼亚州](../Category/西弗吉尼亚州.md "wikilink")
[Category:美国南部](../Category/美国南部.md "wikilink")
[Category:阿巴拉契亚地理](../Category/阿巴拉契亚地理.md "wikilink")
[Category:1863年美国建立](../Category/1863年美国建立.md "wikilink")

1.

2.  ["The World Bank: World Development Indicators
    database"](http://siteresources.worldbank.org/DATASTATISTICS/Resources/GDP.pdf),
    World Bank. September 27, 2010. Accessed December 11, 2010.

3.
4.

5.  [Historical Census Statistics on Population Totals By Race, 1790
    to 1990, and By Hispanic Origin, 1970 to 1990, For The United
    States, Regions, Divisions, and
    States](http://www.census.gov/population/www/documentation/twps0056/twps0056.html)

6.  [Population of West Virginia: Census 2010 and 2000 Interactive Map,
    Demographics, Statistics, Quick
    Facts](http://censusviewer.com/city/WV)

7.