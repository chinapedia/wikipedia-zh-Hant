|        |
| ------ |
| **西門** |
| 性别:    |
| 起源:    |
| 含义:    |
|        |

**西蒙**（[英语](../Page/英语.md "wikilink")：****）是一个西方名字，源于[西缅](../Page/西缅.md "wikilink")，可以指：

## 基督教人物

  - [彼得 (使徒)](../Page/彼得_\(使徒\).md "wikilink")，耶穌十二使徒之一，本名西門
  - [奮銳黨的西門](../Page/奮銳黨的西門.md "wikilink")，耶穌十二使徒之一
  - [西門
    (耶穌的兄弟)](../Page/西門_\(耶穌的兄弟\).md "wikilink")，《馬可福音》與《馬太福音》中，耶穌的兄弟

## 美国人名

  - [保罗·西蒙](../Page/保罗·西蒙.md "wikilink")（Paul
    Simon）[美国流行音乐歌手](../Page/美国.md "wikilink")
  - [妮娜·西蒙](../Page/妮娜·西蒙.md "wikilink")（Nina Simone）美国歌手、钢琴表演家
  - [大衛·西蒙](../Page/大衛·西蒙.md "wikilink")（David Seymour）美国摄影师
  - [西蒙·史密斯·库兹涅茨](../Page/西蒙·史密斯·库兹涅茨.md "wikilink")（Simon Smith
    Kuznets）[俄裔美国经济学家](../Page/俄罗斯.md "wikilink")，1971年[诺贝尔经济学奖获得者](../Page/诺贝尔经济学奖.md "wikilink")

## 法国人名

  - [皮埃尔-西蒙·拉普拉斯](../Page/皮埃尔-西蒙·拉普拉斯.md "wikilink")（Pierre-Simon
    Laplace）[法国天文学家](../Page/法国.md "wikilink")、数学家
  - [西蒙·波娃](../Page/西蒙·波娃.md "wikilink")（Simone de
    Beauvoir）[女权运动创始人之一](../Page/女权运动.md "wikilink")
  - [朱尔·西蒙](../Page/朱尔·西蒙.md "wikilink")（Jules François
    Simon）法国哲学家，1876-1877年[法国总理](../Page/法国总理.md "wikilink")
  - [克洛德·西蒙](../Page/克洛德·西蒙.md "wikilink")（Claude
    Simon）法国小说家，1985年[诺贝尔文学奖获得者](../Page/诺贝尔文学奖.md "wikilink")
  - [吉爾·西蒙](../Page/吉爾·西蒙.md "wikilink")(Gilles Simon)法国男子网球运动员

## 荷兰人名

  - [西蒙·斯特芬](../Page/西蒙·斯特芬.md "wikilink")（Simon
    Stevin）[荷兰数学家](../Page/荷兰.md "wikilink")、工程师
  - [西蒙·范德梅爾](../Page/西蒙·范德梅爾.md "wikilink")（Simon van der
    Meer）荷兰物理学家，1984年获[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")

## 奥地利人名

  - [西蒙·维森塔尔](../Page/西蒙·维森塔尔.md "wikilink")（Simon
    Wiesenthal）[犹太裔](../Page/犹太.md "wikilink")[奥地利籍建筑工程师](../Page/奥地利.md "wikilink")、犹太人大屠杀的幸存者，[纳粹猎人](../Page/纳粹.md "wikilink")
  - [西蒙威森索中心](../Page/西蒙威森索中心.md "wikilink")（The Simon Wiesenthal
    Center）国际犹太人权组织

## 英国人名

  - [西蒙·拉特爵士](../Page/西蒙·拉特.md "wikilink")（Sir Simon
    Rattle）[英国](../Page/英国.md "wikilink")[指挥家](../Page/指挥家.md "wikilink")

## 加拿大人名

  - [西蒙·普勞夫](../Page/西蒙·普勞夫.md "wikilink")（Simon
    Plouffe）[加拿大数学家](../Page/加拿大.md "wikilink")

## 其他

  - Simon亦为西班牙著名电气制造商。
  - Simon亦為中國複姓[-{西門}-的英文譯法](../Page/西門姓.md "wikilink")。
  - [IBM Simon](../Page/IBM_Simon.md "wikilink")，智能手机。
  - [關西蒙](../Page/關西蒙.md "wikilink")（Simon
    Kwan）前[香港](../Page/香港.md "wikilink")[商业电台唱片骑师](../Page/商业电台.md "wikilink")。
  - Simon在台灣式英文裡，除了指人名外，亦可作為及物動詞，意指＂將下巴靠在別人肩膀上＂之行為。　用法例句：＂When James is
    sad, Simon always simon him to make him happy."
  - 日本卡通片[SIMOUN也常被非官方地音譯作](../Page/SIMOUN.md "wikilink")「西蒙」。
  - [西門·馬吉斯](../Page/西門·馬吉斯.md "wikilink")，公元1世紀持[諾斯底主義的](../Page/諾斯底主義.md "wikilink")[撒馬利亞人](../Page/撒馬利亞.md "wikilink")。
  - [SIMON破障槍榴彈](../Page/SIMON破障槍榴彈.md "wikilink")，一種由拉斐爾先進防禦系統研製及生產的城市作戰破門用途槍榴彈。

## 参看

  - [西蒙尼](../Page/西蒙尼.md "wikilink")（Simone）
  - [圣西蒙](../Page/圣西蒙.md "wikilink")（saint-simon）
  - [吉麦罗](../Page/吉麦罗.md "wikilink")（Jimeno）