**西蒙·維森塔爾**（，）是[猶太裔](../Page/猶太.md "wikilink")[奧地利籍](../Page/奧地利.md "wikilink")[建築工程師](../Page/建築工程.md "wikilink")、[猶太人大屠殺的倖存者](../Page/猶太人大屠殺.md "wikilink")，是著名的。他一生致力追查[納粹黨人和蒐證](../Page/納粹.md "wikilink")，把他們送上法庭，要他們為[戰爭罪行和](../Page/戰爭罪行.md "wikilink")[非人道罪行負責](../Page/非人道罪行.md "wikilink")。[西蒙·維森塔爾中心為紀念他而設立](../Page/西蒙·維森塔爾中心.md "wikilink")。

## 早年生活和二戰

維森塔爾出生於[奧匈帝國的](../Page/奧匈帝國.md "wikilink")[布恰奇](../Page/布恰奇.md "wikilink")（Buczacz，現屬[烏克蘭的](../Page/烏克蘭.md "wikilink")[利維夫州](../Page/利維夫州.md "wikilink")）一個猶太商人家庭。由於對猶太學生人數設限，他被拒入讀勒沃夫理工大學（現利維夫理工學院），最終1932年他畢業於[布拉格工業大學](../Page/布拉格捷克理工大學.md "wikilink")。1936年他與齊拉·繆勒
(Cyla Müller)結婚。

[二戰初期](../Page/二戰.md "wikilink")，維森塔爾居於[波蘭的勒沃夫](../Page/波蘭.md "wikilink")。基於[苏德互不侵犯条约](../Page/苏德互不侵犯条约.md "wikilink")，該地成為[蘇聯的](../Page/蘇聯.md "wikilink")[軍事佔領區](../Page/軍事佔領.md "wikilink")。他的繼父和弟弟遭蘇聯的[秘密警察](../Page/秘密警察.md "wikilink")[NKVD探員殺害](../Page/NKVD.md "wikilink")。維森塔爾被迫結束公司，改於工廠工作。當[納粹德國於](../Page/納粹德國.md "wikilink")1941年入侵蘇聯時，他與家人被拘捕。

他與妻子首先被囚禁於雅諾夫斯基
(Janowski)集中營，被迫修建[鐵路](../Page/鐵路.md "wikilink")。波蘭的地下抗德組織營救了他的妻子，以換取他所繪製的鐵路交點分布圖。她的金色頭髮讓她得以隱藏猶太人的身份；雖然被迫在[萊茵河地工作](../Page/萊茵河地.md "wikilink")，但逃過屠殺厄運。

1943年，納粹開始屠殺營內猶太人，維森塔爾逃離集中營，但於當年六月被捉回。經過兩次[自殺不遂](../Page/自殺.md "wikilink")，他與營中僅餘的34名囚犯被押解至波蘭和德國不同的集中營，最後抵達奧地利的毛陶森
(Mauthausen)集中營。他被釋放時，已前後被囚禁於12個集中營，其中5個是進行屠殺的集中營，多次差點被殺害。在大屠殺中，他與妻子共失去了89名親人。

## 納粹獵人

1945年5月5日，維森塔爾被[美軍救出](../Page/美軍.md "wikilink")。美軍發現他時，身高6英尺，卻只有45公斤重。當他回復健康，他隨即為[紐倫堡審訊蒐證](../Page/紐倫堡審訊.md "wikilink")。1947年，他與其餘30個志願者於奧地利[林茨成立](../Page/林茨.md "wikilink")[猶太人檔案中心](../Page/猶太人檔案中心.md "wikilink")，為日後的審訊蒐集資料。可惜，[美國和蘇聯對進一步審判戰爭罪行失去興趣](../Page/美國.md "wikilink")，志願者紛紛離去。維森塔爾全職為受二戰影響的人提供協助，同時繼續蒐證工作。

1962年，[阿道夫·艾希曼被處死後](../Page/阿道夫·艾希曼.md "wikilink")，維森塔爾重開猶太人檔案中心，繼續蒐證，把納粹戰犯送上法庭。其中最為人熟知的是協助成功拘捕卡爾·修伯爾鮑爾（Karl
Silberbauer）。他是當時拘捕[安妮·法蘭克的](../Page/安妮·法蘭克.md "wikilink")[盖世太保軍官](../Page/盖世太保.md "wikilink")。根據修伯爾鮑爾的口供，推翻了《安妮的日記》是子虛烏有的指責。他成功找出另外9名在逃納粹戰犯，並以謀殺猶太人的罪名送往[西德的法庭受審](../Page/西德.md "wikilink")。他協助拘捕[弗蘭茨·施坦格爾](../Page/弗蘭茨·施坦格爾.md "wikilink")（Franz
Stangl，[特雷布林卡滅絕營和](../Page/特雷布林卡滅絕營.md "wikilink")[索比布爾集中營的指揮官](../Page/索比布爾集中營.md "wikilink")）和[赫爾明娜·布勞恩施泰因納爾](../Page/赫爾明娜·布勞恩施泰因納爾.md "wikilink")（Hermine
Braunsteiner，曾任納粹集中營的女性軍官，戰後居於[長島](../Page/長島.md "wikilink")。她曾下令虐待和殺害大量在[邁丹尼克集中營的小孩](../Page/邁丹尼克集中營.md "wikilink")。）

## 西蒙·維森塔爾中心

1977年,
一個猶太人大屠殺紀念組織命名為[西蒙·維森塔爾中心](../Page/西蒙·維森塔爾中心.md "wikilink")。中心提倡人們關注[反猶太主義](../Page/反猶太主義.md "wikilink")、監察[新納粹團體](../Page/新納粹.md "wikilink")、於[洛杉磯和](../Page/洛杉磯.md "wikilink")[耶路撒冷舉辦展覽會](../Page/耶路撒冷.md "wikilink")、以及把納粹戰犯送上法庭受審。

## 奧國政治和晚年

1970年代，維森塔爾涉足奧地利政壇，始於他指控由[社會黨首相](../Page/SPÖ.md "wikilink")[布鲁诺·克赖斯基新組成的內閣中有成員曾經是納粹黨人](../Page/布鲁诺·克赖斯基.md "wikilink")。本身是猶太人的[克赖斯基攻擊維森塔爾是](../Page/克赖斯基.md "wikilink")「用石頭砸自己腳的人」（
*Nestbeschmutzer*）。其實，奧地利要數十年時間才能接受自己在納粹罪行中扮演的角色，期間，維森塔爾常被忽略，甚至被揶揄。

多年來，維森塔爾接過多次死亡恐嚇。1982年，德國和奧地利的新納粹份子在他[維也納的家外引爆炸彈](../Page/維也納.md "wikilink")。

維森塔爾90歲後仍常留在位於維也納的猶太人檔案中心的細小辦公室中工作。

[2003年4月](../Page/2003年4月.md "wikilink")，維森塔爾宣布退休，聲稱他已找到他要找的屠殺者，又說：「我比他們活得久。如果仍有漏網之魚，他們已衰老得不能接受審判。我的工作已經完成。」根據維森塔爾，最後一個仍在生的奧地利主要戰犯是阿羅伊斯·布魯納爾（Alois
Brunner），他是阿道夫·艾希曼的心腹，相信他藏身於[敘利亞](../Page/敘利亞.md "wikilink")，受到[巴沙尔·阿萨德政權的保護](../Page/巴沙尔·阿萨德.md "wikilink")。

維森塔爾的晚年在維也納渡過。陪伴他的妻子於2003年11月10日因病逝世，享年95歲。維森塔爾於2005年9月20日在睡夢中離世。

## 榮譽

  - 名譽[大英帝國爵級司令勫章](../Page/大英帝國勳章.md "wikilink")（2004年2月19日）：表揚他「終生奉獻於彰顯人道」。
  - 解放奧地利勳章（1979）
  - 意大利共和國三等（爵級）勳章（1979）
  - 美國[總統自由勳章](../Page/總統自由勳章.md "wikilink")（2000）
  - 美國[國會金質獎章](../Page/國會金質獎章.md "wikilink")（1980）
  - 法國[榮譽軍團六等（騎士級）勳章](../Page/法國榮譽軍團勳章.md "wikilink")
  - 尼德蘭奧倫葉－拿騷爵級司令勳章
  - 盧森堡大公國勳章（1981）
  - 德意志聯邦共和國[大十字勳章](../Page/聯邦十字勳章.md "wikilink")（1985）
  - [奧地利科學與藝術十字勳章](../Page/奧地利科學與藝術十字勳章.md "wikilink")（1993）
  - [波蘭再生勳章](../Page/波蘭再生勳章.md "wikilink")（1994）
  - 捷克共和國[白獅子勳章](../Page/白獅子勳章.md "wikilink")（1999）
  - 效忠奧地利共和國六等榮譽勳章（2005）

## 批評

維森塔爾指非猶太人在大屠殺中的死亡人數達500萬，新納粹分子Peter Novick和Yehuda Bauer批評數字是維森塔爾捏造。

另一名納粹獵人Tuviah
Friedman批評維森塔爾，指他在利用艾希曼的事件中吹噓自己的能力和說謊，並在事件中獲得厚利。[1](https://web.archive.org/web/20051226133738/http://motlc.specialcol.wiesenthal.com/instdoc/d09c07/iss21z3.html),[2](https://web.archive.org/web/20051203223445/http://motlc.specialcol.wiesenthal.com/instdoc/d09c07/iss12z3.html),[3](https://web.archive.org/web/20051203223641/http://motlc.specialcol.wiesenthal.com/instdoc/d09c07/iss13z3.html),[4](https://web.archive.org/web/20051203223531/http://motlc.specialcol.wiesenthal.com/instdoc/d09c07/iss15z3.html),[5](https://web.archive.org/web/20051203223349/http://motlc.specialcol.wiesenthal.com/instdoc/d09c07/iss17z3.html)

[美國特別調查處處長](../Page/美國特別調查處.md "wikilink") Eli Rosenbaum 在他關於 Kurt
Waldheim 事件的書 *Betrayal: The Untold Story of the Kurt Waldheim
Investigation and Coverup*中寫道：

  -
    「總括來說，維森塔爾在重大的納粹戰犯案時，如 Mengele、Martin Bormann 和艾希曼案，表現出愚拙、誇大和自誇。」

他向維森塔爾的自傳作家形容維森塔爾為「天生的說謊者」。[6](http://hgs.oxfordjournals.org/cgi/reprint/11/2/256?maxtoshow=&HITS=10&hits=10&RESULTFORMAT=&fulltext=wiesenthal&andorexactfulltext=and&searchid=1120901548741_96&stored_search=&FIRSTINDEX=0&sortspec=relevance&resourcetype=1&journalcode=holgen)

## 媒體形象

作家[艾拉·萊文以維森塔爾作為小說](../Page/艾拉·萊文.md "wikilink")《巴西來的男孩們》角色以斯拉·利柏文的藍本。在弗雷德里克·福赛斯的小說《敖德萨档案》中，維森塔爾曾出現，向一名德國記者提供尋找納粹戰犯的線索。電影方面，[班·金斯利在](../Page/班·金斯利.md "wikilink")《戰犯就在你身邊：西蒙·維森塔爾傳》一片中飾演主角西蒙·維森塔爾。

## 參閱

  - [納粹獵人列表](../Page/納粹獵人列表.md "wikilink")

## 參考

1.  [7](http://h-net.msu.edu/cgi-bin/logbrowse.pl?trx=vx&list=h-holocaust&month=0005&week=c&msg=%2b6jdxOXCMf4wOdVQyiqttA&user=&pw=),
    [8](http://lipstadt.blogspot.com/2005/02/transcript-of-wash-post-online.html),[9](http://www.berkeleyinternet.com/holocaust/)

2.  Schachter, Jonathan "Isser Harel Takes On Nazi-Hunter. Wiesenthal
    'Had No Role' In Eichmann Kidnapping." *The Jerusalem Post* 7 May
    1991

3.  Mass, Haim, "Wiesenthal: Redressing the Balance" *The Jerusalem
    Post* 10 May 1992

## 外部連結

  - [傳記（西蒙·維森塔爾中心）](http://www.wiesenthal.com/about/wiesenthal_bio.cfm)

  -
  -
  - [有關西蒙·維森塔爾的紀錄片](http://riverlightspictures.com/taor/welcome.html)

  - [BBC關於西蒙·維森塔爾逝世的報道（05年9月20日）](http://news.bbc.co.uk/2/hi/europe/4262892.stm)

  - [奧地利大屠殺紀念](http://www.gedenkdienst.at/)

  - [西蒙·維森塔爾訃聞](http://www.wiesenthal.com/site/apps/nl/content2.asp?c=fwLYKnN8LzH&b=245494&ct=1436285)

  - [西蒙·維森塔爾 1908-2005](http://www.isracast.com/transcripts/200905a_trans.htm)

  - ["die jüdische"](http://www.juedische.at)

[Category:纳粹猎人](../Category/纳粹猎人.md "wikilink")
[Category:奧地利猶太人](../Category/奧地利猶太人.md "wikilink")
[Category:犹太作家](../Category/犹太作家.md "wikilink")
[Category:納粹集中營倖存者](../Category/納粹集中營倖存者.md "wikilink")
[Category:烏克蘭猶太人](../Category/烏克蘭猶太人.md "wikilink")
[Category:伊拉斯谟奖得主](../Category/伊拉斯谟奖得主.md "wikilink")
[Category:总统自由勋章获得者](../Category/总统自由勋章获得者.md "wikilink")
[Category:KBE勳銜](../Category/KBE勳銜.md "wikilink")
[Category:布拉格捷克理工大學校友](../Category/布拉格捷克理工大學校友.md "wikilink")
[Category:第二次世界大战平民战俘](../Category/第二次世界大战平民战俘.md "wikilink")