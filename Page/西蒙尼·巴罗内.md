**西蒙尼·巴罗内**（**Simone
Barone**，）是一名[意大利現役足球運動員](../Page/意大利.md "wikilink")，現時為意大利甲組球會[拖連奴及](../Page/拖連奴.md "wikilink")[意大利國家隊成員](../Page/意大利國家足球隊.md "wikilink")。2006年於德國協助意大利第四度世界盃冠軍。

巴朗尼於[帕爾馬開始展開職業球員生涯](../Page/帕爾馬足球會.md "wikilink")，1997年5月4日首度為球隊上陣迎戰[阿特蘭大](../Page/阿特蘭大足球會.md "wikilink")，翌年被球會外借至丙組一級球會[帕多瓦](../Page/帕多瓦足球會.md "wikilink")，1999年再一次外借往乙組球會Alzano
Virescit，2000年以共同擁有的身份效力基爾禾兩年，及後返回帕爾馬。此時巴朗尼己成為帕爾馬當中較為獨當一面的一人，2004年以五百萬英鎊轉會至巴勒莫[1](http://www.uefa.com/footballeurope/news/Kind=2/newsId=208287.html)，同年並首次入選意大利國家隊，於2月18日迎戰捷克首次上陣，該場球賽以2比2打成平手。2006年獲國家隊主教練[納比徵召參加](../Page/納比.md "wikilink")[2006年德國世界盃](../Page/2006年世界盃足球賽.md "wikilink")，賽事期間他先後在對捷克及烏克蘭兩度後備出場，最終協助意大利奪得世界盃冠軍。

2006年8月5日，巴朗尼正式以270萬英鎊加盟[拖連奴](../Page/拖連奴.md "wikilink")\[1\]

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [個人檔案
    (出自巴勒莫官方網站)](http://www.ilpalermocalcio.it/it/2006/scheda.jsp?id=1639)

[Category:意大利足球运动员](../Category/意大利足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:帕爾馬球員](../Category/帕爾馬球員.md "wikilink")
[Category:基爾禾球員](../Category/基爾禾球員.md "wikilink")
[Category:巴勒莫球員](../Category/巴勒莫球員.md "wikilink")
[Category:拖連奴球員](../Category/拖連奴球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")

1.  [2](http://www.channel4.com/sport/football_italia/aug5k.html)