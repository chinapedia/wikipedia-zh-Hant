**西藏人民起义日**或稱**西藏抗暴紀念日**，**西藏抗暴日**或稱**西藏起義日**（[英文](../Page/英文.md "wikilink")：**Tibetan
Uprising
Day**）定於**3月10日**，是流亡藏人用來紀念[1959年西藏抗暴運動的纪念日](../Page/1959年西藏抗暴運動.md "wikilink")。

每年的3月10日，[流亡藏人在世界各地纪念](../Page/流亡藏人.md "wikilink")[1959年在](../Page/1959年.md "wikilink")[拉萨与](../Page/拉萨.md "wikilink")[解放军的武装冲突](../Page/解放军.md "wikilink")（[中华人民共和国政府称之为](../Page/中华人民共和国政府.md "wikilink")“[武装叛乱](../Page/武装叛乱.md "wikilink")”\[1\]，[西藏流亡政府称之为](../Page/西藏流亡政府.md "wikilink")“抗暴起义”\[2\]）。在[解放军炮击藏族反抗武装占据的](../Page/解放军.md "wikilink")[羅布林卡后](../Page/羅布林卡.md "wikilink")，[达赖喇嘛和](../Page/第十四世达赖喇嘛.md "wikilink")8万西藏人流亡[印度](../Page/印度.md "wikilink")。

中国政府称，事件的源头与权利分配和经济利益有很大关系，与中国要在[西藏推行土地改革有关](../Page/西藏土地改革運動.md "wikilink")，因此冲突中对方并未得西藏下层人士支持\[3\]；[西藏流亡政府一方则不同意这种说法](../Page/西藏流亡政府.md "wikilink")，认为起义是由于中国政府撕毁“[十七条协议](../Page/十七条协议.md "wikilink")”、压迫西藏人所造成的，起义得到西藏僧俗各界的支持。著名[西藏歷史學家](../Page/西藏歷史.md "wikilink")[茨仁夏加的著作指出](../Page/茨仁夏加.md "wikilink")，西藏貴族、喇嘛們3月10日當天都在解放軍總部接受解放軍款待，不可能策動起义。\[4\]\[5\]

西藏流亡政府宣称，[十世班禅在](../Page/十世班禅.md "wikilink")1987年3月28日的[人大小组会上发言时谈到](../Page/全國人民代表大會.md "wikilink")：“1958年我在[青海听到党内文件上说](../Page/青海.md "wikilink")‘要挑起叛乱、压出叛乱，然后在平叛过程中，彻底解决宗教和民族问题’”，他们认为整个事件是中共以预谋的策略推动的，目的是借机彻底改变西藏社会\[6\]。

## 由來

1959年3月，[中国人民解放军西藏军区第一政委](../Page/中国人民解放军.md "wikilink")[张经武邀請](../Page/张经武.md "wikilink")[達賴喇嘛到](../Page/丹增嘉措.md "wikilink")[西藏軍區看戲](../Page/西藏軍區.md "wikilink")，藏族人有人相信“中共要毒殺達賴喇嘛”，遂於3月10日包圍達賴喇嘛的夏宮[羅布林卡](../Page/羅布林卡.md "wikilink")，勸阻達賴喇嘛赴約，並在大街上張貼海報、呼口號，要求所有的漢族人離開西藏。3月17日[中国人民解放军炮击藏族反抗武装領有的羅布林卡](../Page/中国人民解放军.md "wikilink")，當天深夜達賴喇嘛離開拉薩，流亡印度。後來流亡藏族人將3月10日視為正式起義、反抗中共的日子，每年3月10日就舉辦活動紀念\[7\]\[8\]\[9\]。

## 影响

一些国家或地区的地方政府和议会，曾签署法令或通过法案、声明，宣布定3月10日为“西藏日”（“圖博日”），对流亡西藏人给予帮助\[10\]\[11\]\[12\]。以下是一张采取上述行动的政府的列表：

  - 美国[威斯康辛州政府及议会](../Page/威斯康辛州.md "wikilink")
  - 美国[印第安纳州](../Page/印第安纳州.md "wikilink")[印第安纳波利斯市](../Page/印第安纳波利斯.md "wikilink")
  - [中華民國](../Page/中華民國.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")\[13\]

## 参考文献

## 外部链接

  - [March10.org](http://www.march10.org/)
    2007年1月30日，由[自由西藏学生运动所架設的網站](../Page/自由西藏学生运动.md "wikilink")。

  - [西藏流亡政府官方網站](http://www.tibet.net/)

  - [西藏之頁（繁體）](http://www.xizang-zhiye.org/)

  - [西藏流亡政府倫敦辦事處的官方網站](http://www.tibet.com/)

  - [2008年11月西藏方面向北京提出的「全體藏人真正自治備忘錄」全文](http://www.freetibet.org/about/memorandum-genuine-autonomy-tibetan-people)

  - [*Faith in Exile* Guerrilla
    新聞網的一段視頻](http://www.archive.org/download/tibet_gnn/tibet_bb.mov)

  - [西藏學社 Tibet Society](http://www.tibetsociety.com/)

## 参见

  - [西藏历史](../Page/西藏历史.md "wikilink")
  - [西藏问题](../Page/西藏问题.md "wikilink")
  - [西藏獨立運動](../Page/西藏獨立運動.md "wikilink")
  - [西藏流亡政府](../Page/西藏流亡政府.md "wikilink")
  - [1959年藏區騷亂](../Page/1959年藏區騷亂.md "wikilink")
  - [西藏民主改革](../Page/西藏民主改革.md "wikilink")
  - [2008年西藏骚乱](../Page/2008年西藏骚乱.md "wikilink")
  - [藏區連環自焚事件](../Page/藏區連環自焚事件.md "wikilink")
  - [西藏百万农奴解放纪念日](../Page/西藏百万农奴解放纪念日.md "wikilink")

[Category:3月節日](../Category/3月節日.md "wikilink")
[Category:西藏歷史](../Category/西藏歷史.md "wikilink")
[Category:西藏獨立運動](../Category/西藏獨立運動.md "wikilink")

1.
2.
3.
4.
5.
6.  [班禅喇嘛就 "平叛" 等问题在
    "人代＂讨论会上的发言](http://www.xizang-zhiye.org/gb/arch/writings/banchan/banchan1.html)

7.  [流亡政府概況](http://www.tibet.org.tw/tibet_gov.php)
    [達賴喇嘛西藏宗教基金會官方網站](../Page/達賴喇嘛西藏宗教基金會.md "wikilink")
8.  [紀念西藏抗暴48週年
    籲請國際正視中國暴行](http://www.rti.org.tw/big5/recommend/media/content2.aspx?id=48)
     2007年03月12日 [中央廣播電台](../Page/中央廣播電台.md "wikilink")
    網站一篇訪問「達賴喇嘛西藏宗教基金會」秘書長「索朗多杰」的訪問紀錄。
9.   由[自由西藏学生运动所架設的網站](../Page/自由西藏学生运动.md "wikilink")。
10.
11.
12. [Marchers commemorate end of journey and March 10 at Indianapolis'
    Monument Circle](http://www.rangzen.org/walk2003/walk2003_3_10.htm)
13.