**西藏南路**位於[上海市](../Page/上海市.md "wikilink")，是一條南北向城市次干道，北起[延安东路接](../Page/延安东路_\(上海\).md "wikilink")[西藏中路](../Page/西藏中路.md "wikilink")，南到[黄浦江](../Page/黄浦江.md "wikilink")，地底設有[轨道交通八号线车站](../Page/上海轨道交通八号线.md "wikilink")，全长3594米。\[1\]该路通过其南端的[西藏南路过江隧道通往浦东](../Page/西藏南路隧道.md "wikilink")，[2010年上海世博会大门亦设于该路南端](../Page/2010年上海世博会.md "wikilink")。

## 历史

西藏南路的北段原为[上海县城西侧的一条小河](../Page/上海县城.md "wikilink")，名为周泾，南面到[老西门附近与东西向的](../Page/老西门.md "wikilink")[肇嘉浜](../Page/肇嘉浜.md "wikilink")（今[复兴东路](../Page/复兴东路_\(上海\).md "wikilink")）相通，北面在今[大世界处与东西向的](../Page/大世界.md "wikilink")[洋泾浜](../Page/洋泾浜.md "wikilink")（今[延安东路](../Page/延安东路_\(上海\).md "wikilink")）相通。19世纪后期，为[上海法租界的西部界限](../Page/上海法租界.md "wikilink")。1900年法租界越过周泾向西扩展。20世纪初，上海法租界公董局填埋周泾，修筑成宽阔的马路，以法国首任驻沪领事[敏体尼命名为](../Page/敏体尼.md "wikilink")**敏体尼荫路**。

## 交会道路（北向南）

  - [延安东路](../Page/延安东路.md "wikilink")
  - [宁海东路](../Page/宁海东路.md "wikilink")
  - [金陵中路](../Page/金陵中路.md "wikilink")、[金陵东路](../Page/金陵东路.md "wikilink")
  - [淮海中路](../Page/淮海中路.md "wikilink")、[淮海东路](../Page/淮海东路_\(上海\).md "wikilink")
  - [桃源路](../Page/桃源路.md "wikilink")
  - [寿宁路](../Page/寿宁路.md "wikilink")
  - [会稽路](../Page/会稽路.md "wikilink")
  - [浏河口路](../Page/浏河口路.md "wikilink")
  - [方浜西路](../Page/方浜西路.md "wikilink")
  - [自忠路](../Page/自忠路.md "wikilink")
  - [复兴中路](../Page/复兴中路.md "wikilink")、[复兴东路](../Page/复兴东路_\(上海\).md "wikilink")
  - [肇周路](../Page/肇周路.md "wikilink")
  - [盐城路](../Page/盐城路.md "wikilink")
  - [方斜路](../Page/方斜路.md "wikilink")、[方斜支路](../Page/方斜支路.md "wikilink")
  - [建国新路](../Page/建国新路.md "wikilink")、[安澜路](../Page/安澜路.md "wikilink")
  - [大吉路](../Page/大吉路.md "wikilink")
  - [大林路](../Page/大林路.md "wikilink")
  - [陆家浜路](../Page/陆家浜路.md "wikilink")
  - [丽园路](../Page/丽园路.md "wikilink")
  - [徽宁路](../Page/徽宁路.md "wikilink")
  - [斜土东路](../Page/斜土东路.md "wikilink")
  - [西凌家宅路](../Page/西凌家宅路.md "wikilink")
  - [瞿溪路](../Page/瞿溪路.md "wikilink")
  - [中山南一路](../Page/中山南一路.md "wikilink")、[中山南路](../Page/中山南路_\(上海\).md "wikilink")
  - [高雄路](../Page/高雄路.md "wikilink")、[清流北街](../Page/清流北街.md "wikilink")

## 參見

  - [西藏中路](../Page/西藏中路.md "wikilink")

## 参考资料

<references/>

[Category:黄浦区](../Category/黄浦区.md "wikilink")
[X](../Category/上海道路.md "wikilink")

1.  [专业志 \>\> 上海市政工程志 \>\> 第一篇市区道路 \>\> 第二章主要干道 \>\>
    第二节　南北向主要干道](http://shtong.gov.cn/newsite/node2/node2245/node68289/node68294/node68312/node68325/userobject1ai65726.html)上海地方志办公室