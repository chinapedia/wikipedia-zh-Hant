是一齣[香港](../Page/香港.md "wikilink")[動作片](../Page/動作片.md "wikilink")，由其中三位[七小福](../Page/七小福_\(演員\).md "wikilink")：[元彪](../Page/元彪.md "wikilink")、[元華](../Page/元華.md "wikilink")、[成龍](../Page/成龍.md "wikilink")（客串演出）主演。

## 演員陣容

|                                                      |                                                           |
| ---------------------------------------------------- | --------------------------------------------------------- |
| **演　員**                                              | **角　色**                                                   |
| [元彪](../Page/元彪.md "wikilink")                       | 罗布拉西（Wong La）                                             |
| [李嘉欣](../Page/李嘉欣.md "wikilink")                     | 邱胜男（Chiu Seng-Neng）                                       |
| [元華](../Page/元華.md "wikilink")                       | [黑教教主](../Page/黑教.md "wikilink") （Black Section Sorcerer） |
| [利智](../Page/利智.md "wikilink")                       | 黑教教主的妹妹（Sorcerer's Sister）                                |
| [喬宏](../Page/喬宏.md "wikilink")                       | 罗便臣（Robinson）                                             |
| [Michael Dingo](../Page/Michael_Dingo.md "wikilink") | Michael                                                   |
| [午馬](../Page/午馬.md "wikilink")                       | 拉西的师傅，[黄教教主](../Page/黄教.md "wikilink")（Wong Master）       |
| [樓南光](../Page/樓南光.md "wikilink")                     | 機場保安護衛員                                                   |
| [成龍](../Page/成龍.md "wikilink")                       | 路人                                                        |

## 幕后

该片为[元彪出演](../Page/元彪.md "wikilink")《[孔雀王子](../Page/孔雀王子.md "wikilink")》和《[阿修罗](../Page/阿修罗.md "wikilink")》之后再次出演关于[藏传佛教的电影](../Page/藏传佛教.md "wikilink")。该片在[西藏](../Page/西藏.md "wikilink")[布达拉宫实地取景](../Page/布达拉宫.md "wikilink")。

## 外部链接

  - {{@movies|fkhk40103294}}

  -
  -
  -
  -
  -
  -
[2](../Category/1990年代香港電影作品.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:功夫片](../Category/功夫片.md "wikilink")
[X西](../Category/西藏背景電影.md "wikilink")