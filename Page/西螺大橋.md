**西螺大橋**，[台灣公路橋樑](../Page/台灣.md "wikilink")，在[日據時稱](../Page/臺灣日據時期.md "wikilink")**濁水溪大橋**\[1\]，位於[雲林縣與](../Page/雲林縣.md "wikilink")[彰化縣之間](../Page/彰化縣.md "wikilink")，橫跨[濁水溪下游](../Page/濁水溪.md "wikilink")，以[華倫式桁架橋設計](../Page/華倫式桁架橋.md "wikilink")，連接起南端之[西螺鎮](../Page/西螺鎮.md "wikilink")、北端之[溪州鄉二地的交通](../Page/溪州鄉.md "wikilink")。1952年完工時，西螺大橋是僅次於[美國](../Page/美國.md "wikilink")[舊金山](../Page/舊金山.md "wikilink")[金門大橋的世界第二大橋](../Page/金門大橋.md "wikilink")，也是當時全台灣最長的橋樑，被譽為「遠東第一大橋」。

## 諸元

  - 橋身全長：1939.03[公尺](../Page/公尺.md "wikilink")。
  - 橋面寬：7.32公尺。
  - 橋墩：32座。
  - 橋孔：31個。
  - 橋身架構：華倫式設計，以鋼鐵桁架做為梁，橋墩主要構成為[水泥](../Page/水泥.md "wikilink")。

## 歷史

### 興建背景

濁水溪為[台灣中部重要分界河](../Page/中台灣.md "wikilink")。由於河面廣闊，16世紀歐洲人所繪之台灣地圖，甚至誤將台灣分為北、南兩島。長期以來，兩岸人民往來時必須長期倚賴竹筏等交通工具，若遇上溪水暴漲，則嚴重影響兩地交通，因此一直有興建交通橋樑之提議。

在日治時期，跨越濁水溪下游的橋梁僅有[縱貫鐵路之](../Page/縱貫線_\(鐵路\).md "wikilink")[濁水溪橋](../Page/台鐵濁水溪橋.md "wikilink")，未有公路陸橋跨過。旅客渡濁水溪則須轉乘鐵路或竹筏。

### 興建歷程

1937年（昭和12年），發包興建濁水溪大橋\[2\]，主要的建設部分為橋墩，方法是先以鐵條綁出橢圓形空心板模，接著灌漿抽沙來加重重量使板模下沉，如此反覆作業，使每座橋墩的基樁高度約二層樓高，32座橋墩於1941年完成，之後因戰爭鋼材挪作他用而中止建設，成為未成道路。[戰後](../Page/臺灣戰後時期.md "wikilink")[國民政府遷台](../Page/國民政府.md "wikilink")，在[美援的資助下於](../Page/美援.md "wikilink")1952年5月29日再度開工。

### 竣工通車

西螺大橋於1952年12月25日完工，1953年1月28日正式通車，雲林、彰化兩縣居民為橋名引起爭執，後來是沿用美國總統[杜魯門在國會使用英譯名](../Page/杜魯門.md "wikilink")，才稱為西螺大橋\[3\]。

完工當時是僅次於[美國](../Page/美國.md "wikilink")[舊金山](../Page/舊金山.md "wikilink")[金門大橋的世界第二大橋](../Page/金門大橋.md "wikilink")，也是當時遠東第一大橋\[4\]，亦使日治時期即定線的縱貫道路全線通車。另外，橋上亦併設[糖業鐵路](../Page/糖業鐵路.md "wikilink")，形成鐵公路同行現象，為[南北平行預備線最晚完工的一段](../Page/南北平行預備線.md "wikilink")，唯1979年以妨礙交通為由拆除\[5\]。

### 近況

1994年，[中沙大橋與西螺大橋間的](../Page/中沙大橋.md "wikilink")[溪州大橋完工後](../Page/溪州大橋_\(濁水溪\).md "wikilink")，西螺大橋轉為供小型車、機車、自行車通行的便橋，2000年曾被提議因此橋老舊應予拆除。在雲林縣與彰化縣政府的努力下，使西螺大橋轉型為觀光大橋，2004年11月19日，彰化縣及雲林縣政府均將西螺大橋列入該縣的歷史建築。
大甲媽每年農曆三月前往新港遶境進出雲林必走西螺大橋，是少數特例開放大型車走西螺大橋的特例。[2013雲林農業博覽會期間獲選為百大亮點](../Page/2013雲林農業博覽會.md "wikilink")。\[6\]

## 圖片

<File:西螺大橋> 雲林縣 歷史建築橋樑 Venation 3.jpg|西螺大橋
[File:西螺大橋1.JPG|西螺大橋](File:西螺大橋1.JPG%7C西螺大橋)
[File:Hsilobridge.jpg|西螺大橋雲林端，攝於2005年](File:Hsilobridge.jpg%7C西螺大橋雲林端，攝於2005年)
[File:西螺大橋底部.JPG|西螺大橋底部因應糖鐵經過所做的補強結構](File:西螺大橋底部.JPG%7C西螺大橋底部因應糖鐵經過所做的補強結構)

## 其它紀事

  - 在1963年和1968年發行的[第一套橫式新臺幣](../Page/第一套橫式新臺幣.md "wikilink")10元鈔票正面皆印有西螺大橋圖像。
  - 在2012年公路邦網站舉辦的網路票選中以1,057票獲選為臺灣公路八景之一\[7\]。
  - 在西螺端的橋頭，插有日本、中華民國及[美國國旗](../Page/美國國旗.md "wikilink")，象徵該橋歷經了日治和戰後兩個時期、分別由這三國完成。
  - 在西螺大橋完成前，跨越濁水溪下游的陸橋，僅有臺鐵縱貫線鐵路濁水溪橋1座。而現在（2017年）則已有9座：國道3號、臺鐵縱貫線、縣道141（[彰雲大橋](../Page/彰雲大橋.md "wikilink")）、國道1號（[中沙大橋](../Page/中沙大橋.md "wikilink")）、省道臺1線（[溪州大橋](../Page/溪州大橋_\(濁水溪\).md "wikilink")）、縣道145（西螺大橋）、臺灣高速鐵路、省道臺19線（自強大橋）、省道臺61/17線(西濱大橋)。

## 相鄰道路

<table>
<thead>
<tr class="header">
<th><p><strong><a href="../Page/濁水溪.md" title="wikilink">濁水溪主要橋樑</a></strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>北行道路</strong></p></td>
</tr>
<tr class="even">
<td><p>中山路<br />
彰水路<br />
<small><a href="../Page/溪州鄉.md" title="wikilink">溪州鄉</a></small></p></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部連結

  - [西螺大橋的故事](http://digiarch.sinica.edu.tw/content.jsp?option_id=2441&index_info_id=4764)
  - [西螺大橋](https://web.archive.org/web/20160304210814/http://www.boch.gov.tw/boch/frontsite/cultureassets/caseBasicInfoAction.do?method=doViewCaseBasicInfo&caseId=PA09602000665&assetsClassifyId=1.2&version=1)，文化部文化資產局
  - [西螺大橋（北段）](https://web.archive.org/web/20160304212604/http://www.boch.gov.tw/boch/frontsite/cultureassets/caseBasicInfoAction.do?method=doViewCaseBasicInfo&caseId=NA09602000901&version=1&assetsClassifyId=1.2)，文化部文化資產局
  - [【台灣，你好！】沙發環島空拍鷹眼系列 - 2015/7/26 雲林西螺大橋
    卷一](https://www.youtube.com/watch?v=x3LVhRUKvlI&list=PLOEs_R7Am6v_OgFukirZQUDsf2mpKuF3K&index=22/)

[Category:文建會歷史建築百景](../Category/文建會歷史建築百景.md "wikilink")
[Category:公路鐵路兩用橋](../Category/公路鐵路兩用橋.md "wikilink")
[Category:1952年完工橋梁](../Category/1952年完工橋梁.md "wikilink")
[Category:雲林縣橋梁](../Category/雲林縣橋梁.md "wikilink")
[Category:彰化縣橋梁](../Category/彰化縣橋梁.md "wikilink")
[Category:桁架橋](../Category/桁架橋.md "wikilink")
[Category:濁水溪橋樑](../Category/濁水溪橋樑.md "wikilink")
[Category:西螺鎮](../Category/西螺鎮.md "wikilink")
[Category:溪州鄉](../Category/溪州鄉.md "wikilink")
[Category:台灣地標](../Category/台灣地標.md "wikilink")
[Category:臺灣鐵路橋](../Category/臺灣鐵路橋.md "wikilink")
[Category:梁桥](../Category/梁桥.md "wikilink")
[彰](../Category/臺灣鐵路文化資產.md "wikilink")

1.

2.
3.

4.

5.  [走過台灣一甲子 1950～1959](http://www7.www.gov.tw/twsix/tlMain_age.php?years=2&start=18)（黑白照片）

6.  [百大亮點 - 2013雲林農業博覽會](http://expo.yunlin.gov.tw/?page_id=2248)

7.