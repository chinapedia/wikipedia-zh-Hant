**西表島**（，）是[日本](../Page/日本.md "wikilink")[琉球列島](../Page/琉球列島.md "wikilink")[八重山群島中面積最大的島嶼](../Page/八重山群島.md "wikilink")，也是日本[沖繩縣內僅次於](../Page/沖繩縣.md "wikilink")[沖繩島的第二大島](../Page/沖繩島.md "wikilink")。西表島的面積有289平方公里，島上人口約有2300人。行政區劃屬於日本沖繩縣[八重山郡](../Page/八重山郡.md "wikilink")[竹富町](../Page/竹富町.md "wikilink")。西表島位於[台灣東北部外海](../Page/台灣.md "wikilink")，距離台灣[宜蘭](../Page/宜蘭縣.md "wikilink")[蘇澳港僅約](../Page/蘇澳港.md "wikilink")200公里。

## 地理環境

[Iriomote.jpg](https://zh.wikipedia.org/wiki/File:Iriomote.jpg "fig:Iriomote.jpg")
西表島被稱為東洋的亞馬遜，處於[亞熱帶環境](../Page/亞熱帶.md "wikilink")，年均溫為[攝氏](../Page/攝氏.md "wikilink")23.4度，月均溫最高為七月的攝氏28.4度，最低為一月的攝氏17.6度。每年六月到九月常遭受[颱風的侵襲](../Page/颱風.md "wikilink")。

島上90%土地為[亞熱帶原生](../Page/亞熱帶.md "wikilink")[森林及](../Page/森林.md "wikilink")[紅樹林](../Page/紅樹林.md "wikilink")[沼澤所覆蓋](../Page/沼澤.md "wikilink")，其中80%是受到保護的國有土地，[西表石垣國立公園佔全島](../Page/西表石垣國立公園.md "wikilink")34.3%。西表島平地較少，古見岳（，Komidake）高470[公尺為島上最高的山](../Page/公尺.md "wikilink")。

[西表山貓為島上的特有動物](../Page/西表山貓.md "wikilink")，為[夜行性山貓](../Page/夜行性.md "wikilink")，已瀕臨絕種，目前估計僅存約一百隻。

## 人文

島上部分居民使用[八重山方言](../Page/八重山方言.md "wikilink")。

東經123度45分6.789秒的子午線正好通過西表島西部，島上特地在該位置建立「竹富町子午線接觸館」，作為觀光景點。

## 歷史

於二次戰前曾經爆發[瘧疾流行](../Page/瘧疾.md "wikilink")，因此造成島上開發較晚，直到[美國治理琉球時期消滅了瘧疾](../Page/美國治理琉球時期.md "wikilink")，島上才開始有較顯著的開發。

西表島上曾有煤礦開採。

## 外部連結

  - [竹富町公所網頁的西表島介紹](https://web.archive.org/web/20061006133909/http://www.town.taketomi.okinawa.jp/island/iriomote2.htm)

  - [Yahoo\!地圖情報：西表島](http://map.yahoo.co.jp/pl?nl=24.20.46.335&el=123.49.8.953&la=0&sc=6&CE.x=230&CE.y=250)

  - [西表島交通株式會社網站](http://www.iriomote.com/)

  - [西表島渡假地開發終止訴訟・由原告團設置](http://www.geocities.co.jp/NatureLand/2032/)

  - [@Yaima【西表島觀光情報】](https://web.archive.org/web/20060821153759/http://yaima.cool.ne.jp/iriomoteinfo.html)

  - [竹富町子午線接觸館](https://web.archive.org/web/20061006140726/http://www.town.taketomi.okinawa.jp/island/fureaikan/index.htm)

  - [西表野生動物保護中心](https://web.archive.org/web/20100704190656/http://www.town.taketomi.okinawa.jp/island/wildlife/wildlife.htm)

[X西](../Category/八重山群島.md "wikilink")
[Category:竹富町](../Category/竹富町.md "wikilink")