**西貢鄧肇堅運動場**（），是[香港一個](../Page/香港.md "wikilink")[田徑](../Page/田徑.md "wikilink")[運動場](../Page/運動場.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[西貢市](../Page/西貢市.md "wikilink")[福民路](../Page/福民路.md "wikilink")，為[西貢區內大型運動場](../Page/西貢區.md "wikilink")，佔地24,000平方米，由[康樂及文化事務署管理](../Page/康樂及文化事務署.md "wikilink")。

該運動場曾是[將軍澳](../Page/將軍澳新市鎮.md "wikilink")（同屬西貢區）學校舉辦學校陸運會的主要場地，不過於2009年12月之後，當新建的[將軍澳運動場完成](../Page/將軍澳運動場.md "wikilink")[東亞運動會比賽後](../Page/2009年東亞運動會.md "wikilink")，大部分[將軍澳區學校會改為在將軍澳運動場舉辦陸運會](../Page/將軍澳新市鎮.md "wikilink")。

## 歷史

西貢鄧肇堅運動場由[西貢區鄉事委員長及](../Page/西貢區鄉事委員長.md "wikilink")[西貢區體育會發起籌建](../Page/西貢區體育會.md "wikilink")，獲得時任[新界政務司的](../Page/新界政務司.md "wikilink")[鍾逸傑協助](../Page/鍾逸傑.md "wikilink")，邀請香港慈善家[鄧肇堅捐資](../Page/鄧肇堅.md "wikilink")300萬港元，合共600萬元興建，位處一幅[填海土地](../Page/填海.md "wikilink")，於1983年3月落成啟用，運動場遂以鄧肇堅命名，於5月27日由[港督](../Page/香港總督.md "wikilink")[尤德主持正式揭幕](../Page/尤德.md "wikilink")<small>\[1\]</small>。

2001年，西貢鄧肇堅運動場曾進行過大規模重修。

## 結構

運動場的看台上蓋呈波浪形，最多可容納約3,000名觀眾，設有11人真草足球場。運動場鄰近西貢巴士總站。另外，該運動場曾經是[西貢區足球會的主場球場](../Page/西貢區足球會.md "wikilink")，惟由2010年開始，已改為以新建的[將軍澳運動場作主場](../Page/將軍澳運動場.md "wikilink")。

  - 設施

<!-- end list -->

  - 有蓋看台
  - 1條標準8線田徑跑道（400米）；
  - 1個天然草地球場（設有泛光燈）

[File:HK_SaiKungTangShiuKinSportsGround_Overview.JPG|鳥瞰西貢鄧肇堅運動場](File:HK_SaiKungTangShiuKinSportsGround_Overview.JPG%7C鳥瞰西貢鄧肇堅運動場)
<File:Sai> Kung Tang Shiu Kin Sports Ground.jpg|西貢鄧肇堅運動場有蓋看台

## 開放時間

  - 緩步跑時間：06:30-22:30
  - 每星期保養日：逢星期三

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參考文獻

  - 《西貢歷史與風物》，馬木池、張兆和、黃永豪、廖迪生、劉義章、蔡志祥
    著，[西貢區議會](../Page/西貢區議會.md "wikilink")
    出版，ISBN 988-97421-1-X

## 參考資料

## 外部連結

  - [康文署：西貢鄧肇堅運動場](https://www.lcsd.gov.hk/tc/facilities/facilitieslist/facilities.php?fid=375)
  - [影視處：西貢鄧肇堅運動場](http://www.fso-createhk.gov.hk/chi/lib/locations_details.cfm?Photo_Num=00592)

[Category:西貢市](../Category/西貢市.md "wikilink")
[Category:香港運動場](../Category/香港運動場.md "wikilink")
[Category:香港足球場](../Category/香港足球場.md "wikilink")
[Category:香港乙、丙組足球會主場](../Category/香港乙、丙組足球會主場.md "wikilink")

1.  「鄧肇堅運動場 港督主持揭幕」，《大公報》，1983年5月23日，第2張第8版