**西里尔维文**（，[拉丁维文](../Page/拉丁维文.md "wikilink")：（缩写为：UKY），[老维文](../Page/老维文.md "wikilink")：）亦稱斯拉夫維文，是以[西里尔字母为基础的](../Page/西里尔字母.md "wikilink")[维吾尔文](../Page/维吾尔文.md "wikilink")。主要被前[苏联国家中的](../Page/苏联.md "wikilink")[维吾尔人使用](../Page/维吾尔.md "wikilink")。\[1\]

除了[维语的](../Page/维语.md "wikilink")32个字母，还另有几个字母用于[俄语外来词](../Page/俄语.md "wikilink")，字母有[印刷体和](../Page/印刷体.md "wikilink")[手写体的区别](../Page/手写体.md "wikilink")。

## 字母表

<table>
<thead>
<tr class="header">
<th><p>大写字母</p></th>
<th><p>А</p></th>
<th><p>Б</p></th>
<th><p>В</p></th>
<th><p>Г</p></th>
<th><p>Ғ</p></th>
<th><p>Д</p></th>
<th><p>Е</p></th>
<th><p>Ә</p></th>
<th><p>Ж</p></th>
<th><p>Җ</p></th>
<th><p>З</p></th>
<th><p>И</p></th>
<th><p>Й</p></th>
<th><p>К</p></th>
<th><p>Қ</p></th>
<th><p>Л</p></th>
<th><p>М</p></th>
<th><p>Н</p></th>
<th><p>Ң</p></th>
<th><p>О</p></th>
<th><p>Ө</p></th>
<th><p>П</p></th>
<th><p>Р</p></th>
<th><p>С</p></th>
<th><p>Т</p></th>
<th><p>У</p></th>
<th><p>Ү</p></th>
<th><p>Ф</p></th>
<th><p>Х</p></th>
<th><p>Һ</p></th>
<th><p>Ч</p></th>
<th><p>Ш</p></th>
<th><p>Ю</p></th>
<th><p>Я</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>小写字母</strong></p></td>
<td><p>a</p></td>
<td><p>б</p></td>
<td><p>в</p></td>
<td><p>г</p></td>
<td><p>ғ</p></td>
<td><p>д</p></td>
<td><p>е</p></td>
<td><p>ə</p></td>
<td><p>ж</p></td>
<td><p>җ</p></td>
<td><p>з</p></td>
<td><p>и</p></td>
<td><p>й</p></td>
<td><p>к</p></td>
<td><p>қ</p></td>
<td><p>л</p></td>
<td><p>м</p></td>
<td><p>н</p></td>
<td><p>ң</p></td>
<td><p>о</p></td>
<td><p>ө</p></td>
<td><p>п</p></td>
<td><p>р</p></td>
<td><p>с</p></td>
<td><p>т</p></td>
<td><p>у</p></td>
<td><p>ү</p></td>
<td><p>ф</p></td>
<td><p>х</p></td>
<td><p>Һ</p></td>
<td><p>ч</p></td>
<td><p>ш</p></td>
<td><p>ю</p></td>
<td><p>я</p></td>
</tr>
<tr class="even">
<td><p><strong>国际音标</strong></p></td>
<td><p>ɑ,a</p></td>
<td><p>b</p></td>
<td><p>w,v</p></td>
<td><p>g</p></td>
<td><p>ʁ,ɣ</p></td>
<td><p>d</p></td>
<td><p>e</p></td>
<td><p>ɛ,æ</p></td>
<td><p>ʒ</p></td>
<td><p>dʒ</p></td>
<td><p>z</p></td>
<td><p>i,ɨ</p></td>
<td><p>j</p></td>
<td><p>k</p></td>
<td><p>q</p></td>
<td><p>l</p></td>
<td><p>m</p></td>
<td><p>n</p></td>
<td><p>ŋ</p></td>
<td><p>o,ɔ</p></td>
<td><p>ø</p></td>
<td><p>p</p></td>
<td><p>r,ɾ</p></td>
<td><p>s</p></td>
<td><p>t</p></td>
<td><p>u,ʊ</p></td>
<td><p>y,ʏ</p></td>
<td><p>f,ɸ</p></td>
<td><p>χ,x</p></td>
<td><p>h,ɦ</p></td>
<td><p>tʃ</p></td>
<td><p>ʃ</p></td>
<td><p>ju</p></td>
<td><p>ja<br />
</p></td>
</tr>
</tbody>
</table>

</center>

## 参看

  - [维吾尔语字母](../Page/维吾尔语字母.md "wikilink")
  - [突厥语族](../Page/突厥语族.md "wikilink")

## 註釋

## 外部連結

  -

  -

[Category:维吾尔语](../Category/维吾尔语.md "wikilink")
[Category:西里尔字母系统](../Category/西里尔字母系统.md "wikilink")
[Category:突厥語族使用的字母系統](../Category/突厥語族使用的字母系統.md "wikilink")

1.