**西麗克·史密莎**曾是南印度色情電影界的王后級人物。

## 傳記

出生于[安得拉邦的Eluru貧窮人家](../Page/安得拉邦.md "wikilink")，原名為：維嘉雅蘭斯密（Vijayalaxmi），小學四年級因家貧輟學從影。
她跟從姨娘來到當時南印度電影中心城市[馬德拉斯](../Page/馬德拉斯.md "wikilink")\[1\]，更名為**史密莎**（Smitha）。\[2\]她的第一部扮演主要角色的泰米爾語電影為：*[Vandi
Chakkaram](../Page/Vandi_Chakkaram.md "wikilink")* (The
Wheel)，1979年她又在自己的名字前加上**西麗克**（Silk——是她在一部電影中的角色名）。\[3\]

她在此後的十多年中一直很成功，但是步入九十年代事業開始走下坡。她嘗試製作電影但是損失了大量金錢，以致身心交瘁，終於在1996年以自殺結束了自己的生命。

## 電影事業

西麗克·史密莎在從影二十多年中演過不下200部[泰米爾語](../Page/泰米爾語.md "wikilink")、[泰盧固語](../Page/泰盧固語.md "wikilink")、[马拉雅拉姆语](../Page/马拉雅拉姆语.md "wikilink")、[卡納達克語的電影](../Page/卡納達克語.md "wikilink")，她在影片中大膽的舞蹈表演使得她在南印度擁有眾多的觀眾，
\[4\]她的電影代表作有：

  - *[Layanam](../Page/Layanam.md "wikilink")* 、\[5\]
  - **[新月牙兒](../Page/新月牙兒.md "wikilink")** <big>/</big> *[Moondram
    Pirai](../Page/Moondram_Pirai.md "wikilink")*
    （印地語版本標題為：*[Sadma](../Page/Sadma.md "wikilink")*）\[6\]

## 電影作品

  - [Moondram Pirai](../Page/Moondram_Pirai.md "wikilink")
  - [Miss Pamela](../Page/Miss_Pamela.md "wikilink")
  - [Andru peitha Mazhayil](../Page/Andru_peitha_Mazhayil.md "wikilink")
  - [Sphadikam](../Page/Sphadikam.md "wikilink")
  - [Jeeva](../Page/Jeeva.md "wikilink")
  - [Play Girls](../Page/Play_Girls.md "wikilink")
  - [Soorakkottai
    Singakutti](../Page/Soorakkottai_Singakutti.md "wikilink")
  - [Challenge](../Page/Challenge_\(Telugu_film\).md "wikilink")
  - [Moondru Mugam](../Page/Moondru_Mugam.md "wikilink")
  - [Layanam](../Page/Layanam.md "wikilink")
  - [Kozhi Kuvuthu](../Page/Kozhi_Kuvuthu.md "wikilink")
  - [Khaidi](../Page/Khaidi.md "wikilink")
  - [Amaran](../Page/Amaran.md "wikilink")
  - [Niraparathi](../Page/Niraparathi.md "wikilink")
  - [Yamakinkarudu](../Page/Yamakinkarudu.md "wikilink")
  - [Rustum](../Page/Rustum.md "wikilink")

## 外部連結

  -
  - [她的玉照](http://www.yellowpagesindia.info/forum/forum_posts.asp?TID=342&PID=709)

## 參考文

  - Ashish Rajadhyaksha, Encyclopedia of Indian Cinema, Oxford
    University Press, 1994 (ISBN 0-85170-669-X)

  - Roopa Swaminathan, Star Dust: Vignettes from the Fringes of the Film
    Industry, Penguin, 2004 (ISBN 0-14-303243-7)

  - Suparna Bhaskaran, Made in India: Decolonizations, Queer
    Sexualities, Trans/National Projects, Palgrave Macmillan, 2004 (ISBN
    1-4039-6726-1)

  -

    <div class="references-small">

<references/>

</div>

[Category:1960年出生](../Category/1960年出生.md "wikilink")
[Category:1996年逝世](../Category/1996年逝世.md "wikilink")
[Category:泰盧固人](../Category/泰盧固人.md "wikilink")
[Category:印度自殺者](../Category/印度自殺者.md "wikilink")
[Category:印度電影女演員](../Category/印度電影女演員.md "wikilink")
[Category:印度兒童演員](../Category/印度兒童演員.md "wikilink")

1.
2.
3.
4.
5.
6.