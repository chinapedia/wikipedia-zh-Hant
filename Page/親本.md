**親本**，指的是[生成新](../Page/生成.md "wikilink")[個體的](../Page/個體.md "wikilink")[生物](../Page/生物.md "wikilink")。而**父母**、**家長**、**雙親**、**子女**的[定義則較廣泛](../Page/定義.md "wikilink")，只要是[養育](../Page/養育.md "wikilink")[孩子](../Page/孩子.md "wikilink")[成長的](../Page/發育.md "wikilink")[人](../Page/人.md "wikilink")，都可以用此[稱呼](../Page/稱呼.md "wikilink")。

語出《[禮記](../Page/禮記.md "wikilink")·曲禮下》：「生曰父，曰母，曰妻；死曰考，曰妣，曰嬪。」。「考」字（名詞）原指[父親](../Page/父親.md "wikilink")，不論是健在的已故的\[1\]\[2\]，當形容詞，指「高壽、長壽」\[3\]\[4\]。「妣」字原指[母親](../Page/母親.md "wikilink")，不論是健在的已故的\[5\]。

## 母親

**母親**指的可以是生物上生育或社會角色上養育[孩子的女性](../Page/孩子.md "wikilink")，相對於母親一詞便是[父親](../Page/父親.md "wikilink")。[母性則指的是一個母親對他的或別人小孩的感覺](../Page/母性.md "wikilink")，或稱[母愛](../Page/母愛.md "wikilink")。

諸如[人類的](../Page/人類.md "wikilink")[哺乳類](../Page/哺乳類.md "wikilink")，母親扮演[懷胎的角色](../Page/懷胎.md "wikilink")，[胎兒在](../Page/胎兒.md "wikilink")[子宮中成長直到出生](../Page/子宮.md "wikilink")。小孩一-{出}-生，母親就會製造[乳汁孕育小孩](../Page/乳汁.md "wikilink")。

## 父親

**父親**在傳統角色上是孩子的男性雙親。就如同母親，父親與孩子也可被歸類為生物上、社會角色上或是法律上的[人際關係](../Page/人際關係.md "wikilink")。傳統上，生物關係上的[父權就已被歸類在父親](../Page/父權.md "wikilink")。然而，法律上的父權實質上問題和社會角色上時常把母親的丈夫視為孩子的父親。

## 子女

子女依据[血亲关系](../Page/血亲.md "wikilink")，可分为亲生子女，非亲生子女（[养子女](../Page/养子女.md "wikilink")、[继子女](../Page/继子女.md "wikilink")）。亲生子女中，依父母婚姻状态，又可分为[婚生子女](../Page/婚生子女.md "wikilink")，[非婚生子女（即私生子）](../Page/非婚生子女.md "wikilink")。

## 參考來源

  - 文獻

<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  - 引用

## 外部連結

  - [國家教育網路公司（NEN）](http://www.nenionline.org) - 免費的父母教育課程資源

## 參見

  - [家主](../Page/家主.md "wikilink")
  - [父權](../Page/父權.md "wikilink")
  - [家長作風](../Page/家長作風.md "wikilink")
  - [監護人](../Page/監護人.md "wikilink")

[Category:親屬關係](../Category/親屬關係.md "wikilink")
[Category:親屬稱謂](../Category/親屬稱謂.md "wikilink")
[Category:母亲身份](../Category/母亲身份.md "wikilink")
[Category:父親](../Category/父親.md "wikilink")
[Category:家庭結構](../Category/家庭結構.md "wikilink")

1.  《[楚辭](../Page/楚辭.md "wikilink")·屈原·離騷》：「帝高陽之苗裔兮，朕皇考曰伯庸。」
2.  《[新唐書](../Page/新唐書.md "wikilink")·卷一三·禮樂志三》：「或兄弟分官，則各祭考妣於正寢。」
3.  《[說文解字](../Page/說文解字.md "wikilink")·老部》：「考，老也。」
4.  《[詩經](../Page/詩經.md "wikilink")·大雅·棫樸》：「周王壽考，遐不作人。」
5.  《說文解字·女部》：「妣，歿母也。」