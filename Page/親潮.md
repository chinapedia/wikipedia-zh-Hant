[Spring_Bloom_Colors_the_Pacific_Near_Hokkaido.jpg](https://zh.wikipedia.org/wiki/File:Spring_Bloom_Colors_the_Pacific_Near_Hokkaido.jpg "fig:Spring_Bloom_Colors_the_Pacific_Near_Hokkaido.jpg")在[北海道附近碰撞](../Page/北海道.md "wikilink")。當兩個洋流發生碰撞，它們造成[漩渦](../Page/漩渦.md "wikilink")。在表層水生長的[浮游植物沿著這些漩渦的邊界變得密集](../Page/浮游植物.md "wikilink")，追查出來的水的運動踪跡。\]\]
[Japan's_ocean_currents.PNG](https://zh.wikipedia.org/wiki/File:Japan's_ocean_currents.PNG "fig:Japan's_ocean_currents.PNG")|2.[北太平洋洋流](../Page/北太平洋洋流.md "wikilink")（黑潮續流）|3.黑潮反流|4.|5.|6.|7.親潮（[千島群島洋流](../Page/千島群島.md "wikilink")）|8.（寒流）}}\]\]

**親潮**，或又稱為**千島群島洋流**，是一股北太平洋的循環[洋流](../Page/洋流.md "wikilink")（寒流）\[1\]，自[北極海逆時鐘方向向南經由](../Page/北極海.md "wikilink")[白令海流往西北](../Page/白令海.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")，在[日本東部海域與](../Page/日本.md "wikilink")[黑潮會合形成](../Page/黑潮.md "wikilink")[北太平洋洋流](../Page/北太平洋洋流.md "wikilink")。親潮對於[俄羅斯遠東地區的氣候產生極大的影響](../Page/俄羅斯遠東地區.md "wikilink")，這點在[俄國沿太平洋地區的](../Page/俄國.md "wikilink")[堪察加半島與](../Page/堪察加半島.md "wikilink")[楚科奇半島的](../Page/楚科奇半島.md "wikilink")[林木線可以觀察到](../Page/林木線.md "wikilink")，這兩個地區的林線比內陸的[西伯利亞地區還要低緯度十度左右](../Page/西伯利亞.md "wikilink")，這是因為寒冷的洋流將其經過地區的氣溫降低到[樹木無法生長的程度](../Page/樹木.md "wikilink")。

親潮豐富的養分使得其經過的水域成為富裕的[漁場](../Page/漁場.md "wikilink")。親潮的經過也使得[海參崴成為最靠近赤道但在](../Page/海參崴.md "wikilink")[冬季時仍需要](../Page/冬季.md "wikilink")[破冰船的](../Page/破冰船.md "wikilink")[港口](../Page/港口.md "wikilink")。另外在[冰河時期時](../Page/冰河時期.md "wikilink")，降低的[海平面造成了](../Page/海平面.md "wikilink")[白令海峽陸橋](../Page/白令陸橋.md "wikilink")，這使得冰河時期的親潮無法到達它今日所能到達的緯度，日本的[東北地方與](../Page/東北地方.md "wikilink")[北海道雖然處在](../Page/北海道.md "wikilink")[東亞地區唯一有足夠降雪量以產生冰河](../Page/東亞.md "wikilink")，但是並未像[歐洲與](../Page/歐洲.md "wikilink")[北美洲在冰河時期時被冰河大面積的覆蓋](../Page/北美洲.md "wikilink")，反而只有在高海拔地區才有些許的冰。而這點則解釋了為什麼東亞地區保留了96%[上新世的植物](../Page/上新世.md "wikilink")，而歐洲卻只有27%。

## 參見

  - [物理海洋学](../Page/物理海洋学.md "wikilink")

## 參考資料

  - Reddy, MPM (2001): *Descriptive physical oceanography* Taylor and
    Francis, ISBN 90-5410-706-5, pp 367–368

## 外部連結

  - [UNEP Global International Waters Assessment (GIWA) Regional
    Definition page for the Oyashio Current, accessed 2
    July 2010](http://www.unep.org/dewa/giwa/areas/reports/r31/regional_definition_giwa_r31.pdf)

[Category:洋流](../Category/洋流.md "wikilink")

1.  參見[環流](../Page/環流.md "wikilink")