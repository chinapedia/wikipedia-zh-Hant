**觀護盃國際籃球邀請賽**（**Hualien Probation
Baketball**，"HPB"），是[台灣](../Page/台灣.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")[觀護志工協進會所舉辦](../Page/觀護志工協進會.md "wikilink")，長期與[ESPN合作轉播的國際邀請賽事](../Page/ESPN.md "wikilink")。

## 成立與沿革

觀護盃國際籃球邀請賽前身是「觀護盃全國甲一級四強菁英挑戰賽」，首屆在2003年舉辦，當屆有四隊參賽，2004年第二屆時有六隊參賽。2005年開始邀請國外球隊參賽，改名「觀護盃國際籃球邀請賽」，本屆有八隊參賽，2006年之後則擴充到十隊參賽。創辦理事長林有志(花蓮縣林姓宗親會理事長、行政院東部聯合服務中心副執行長）

## 2018年觀護盃國際籃球邀請賽

此屆觀護盃同時也是第十六季[超級籃球聯賽季前熱身賽](../Page/超級籃球聯賽.md "wikilink")。

冠軍是[寶島夢想家](../Page/寶島夢想家.md "wikilink")，亞軍是[皓宇國際籃球隊](../Page/皓宇國際籃球隊.md "wikilink")，季軍是[台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")，殿軍是[南華籃球隊](../Page/南華籃球隊.md "wikilink")，第五名是[台灣銀行籃球隊](../Page/台灣銀行籃球隊.md "wikilink")，第六名是[印尼隊](../Page/印尼隊.md "wikilink")，第七名是[金門酒廠籃球隊](../Page/金門酒廠籃球隊.md "wikilink")，第八名是[美國隊](../Page/美國隊.md "wikilink")。

### 參賽隊伍

A組

  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")(台灣[超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink"))
  - [泰國隊](../Page/泰國隊.md "wikilink")
  - [寶島夢想家](../Page/寶島夢想家.md "wikilink")([東南亞職業籃球聯賽](../Page/東南亞職業籃球聯賽.md "wikilink"))
  - [美國隊](../Page/美國隊.md "wikilink")
  - [台灣銀行籃球隊](../Page/台灣銀行籃球隊.md "wikilink")(台灣[超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink"))

B組

  - [金門酒廠籃球隊](../Page/金門酒廠籃球隊.md "wikilink")(台灣[超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink"))
  - [皓宇國際籃球隊](../Page/皓宇國際籃球隊.md "wikilink")
  - [南華籃球隊](../Page/南華籃球隊.md "wikilink")([香港籃球聯賽](../Page/香港籃球聯賽.md "wikilink"))
  - [印尼隊](../Page/印尼隊.md "wikilink")

## 2017年觀護盃國際籃球邀請賽

此屆觀護盃同時也是第十五季[超級籃球聯賽季前熱身賽](../Page/超級籃球聯賽.md "wikilink")，[SBL](../Page/SBL.md "wikilink")4支隊伍都有參賽。

冠軍是[桃園璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")，亞軍是[台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")，季軍是[金門酒廠籃球隊](../Page/金門酒廠籃球隊.md "wikilink")，殿軍是[臺灣銀行籃球隊](../Page/臺灣銀行籃球隊.md "wikilink")，第五名是[洛杉磯海灣](../Page/洛杉磯海灣.md "wikilink")，第六名是[鈦鼎科技籃球隊](../Page/鈦鼎科技籃球隊.md "wikilink")，第七名是[印尼隊](../Page/印尼隊.md "wikilink")，第八名是[美國隊](../Page/美國隊.md "wikilink")。

### 參賽隊伍

A組

  - [璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")(台灣[超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink"))
  - [金門酒廠籃球隊](../Page/金門酒廠籃球隊.md "wikilink")(台灣[超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink"))
  - [印尼籃球隊](../Page/印尼籃球隊.md "wikilink")
  - [美國隊](../Page/美國隊.md "wikilink")

B組

  - [鈦鼎科技籃球隊](../Page/鈦鼎科技籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")(台灣[超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink"))
  - [洛杉磯海灣籃球隊](../Page/洛杉磯海灣籃球隊.md "wikilink")
  - [台灣銀行籃球隊](../Page/台灣銀行籃球隊.md "wikilink")(台灣[超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink"))

## 2016年觀護盃國際籃球邀請賽

此屆觀護盃同時也是第十四季[超級籃球聯賽季前熱身賽](../Page/超級籃球聯賽.md "wikilink")，[SBL](../Page/SBL.md "wikilink")4支隊伍都有參賽。

冠軍是[達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")，亞軍是[台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")，季軍是[台灣銀行籃球隊](../Page/台灣銀行籃球隊.md "wikilink")，殿軍是[璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")。

### 參賽隊伍

  - [達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")
  - [璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [台灣銀行籃球隊](../Page/台灣銀行籃球隊.md "wikilink")
  - [美國菁英隊](../Page/美國菁英隊.md "wikilink")

## 2015年觀護盃國際籃球邀請賽

此屆觀護盃同時也是第十三季[超級籃球聯賽季前熱身賽](../Page/超級籃球聯賽.md "wikilink")，[SBL](../Page/SBL.md "wikilink")6支隊伍都有參賽。

冠軍是[璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")，亞軍是[裕隆納智捷籃球隊](../Page/裕隆納智捷籃球隊.md "wikilink")，季軍是[達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")，第四名是[臺灣銀行籃球隊](../Page/臺灣銀行籃球隊.md "wikilink")，第五名是[日本](../Page/日本.md "wikilink")[富山雷鳥](../Page/富山雷鳥.md "wikilink")（[富山サンダーバード](../Page/:ja:富山サンダーバード.md "wikilink")），第六名是[美國菁英隊](../Page/美國菁英隊.md "wikilink")，第七名是[台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")，第八名是[蒙古籃球隊](../Page/蒙古籃球隊.md "wikilink")。

### 參賽隊伍

A組

  - [富邦勇士籃球隊](../Page/富邦勇士籃球隊.md "wikilink")
  - [蒙古籃球隊](../Page/蒙古籃球隊.md "wikilink")
  - [達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")
  - [璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")
  - [美國菁英隊](../Page/美國菁英隊.md "wikilink")

B組

  - [裕隆納智捷籃球隊](../Page/裕隆納智捷籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [日本雷鳥籃球隊](../Page/日本雷鳥籃球隊.md "wikilink")
  - [台灣銀行籃球隊](../Page/台灣銀行籃球隊.md "wikilink")

## 2014年觀護盃國際籃球邀請賽

此屆觀護盃同時也是第十二季[超級籃球聯賽季前熱身賽](../Page/超級籃球聯賽.md "wikilink")，[SBL](../Page/SBL.md "wikilink")5支隊伍都有參賽。

冠軍是\*[日本link tochigi
brex](../Page/日本link_tochigi_brex.md "wikilink")，亞軍是\*[日本Chiba
Jets](../Page/日本Chiba_Jets.md "wikilink")，季軍戰因場地濕滑，[達欣工程籃球隊和](../Page/達欣工程籃球隊.md "wikilink")[台灣銀行籃球隊並列季軍](../Page/台灣銀行籃球隊.md "wikilink")。

### 參賽隊伍

  - [裕隆納智捷籃球隊](../Page/裕隆納智捷籃球隊.md "wikilink")
  - [達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [日本link tochigi brex](../Page/日本link_tochigi_brex.md "wikilink")
  - [台灣銀行籃球隊](../Page/台灣銀行籃球隊.md "wikilink")
  - [日本Chiba Jets](../Page/日本Chiba_Jets.md "wikilink")
  - [璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")
  - [美國菁英隊](../Page/美國菁英隊.md "wikilink")

## 2013年觀護盃國際籃球邀請賽

## 2012年觀護盃國際籃球邀請賽

2012年10月31日至11月4日於[花蓮縣立體育館](../Page/花蓮縣立體育館.md "wikilink")（德興小巨蛋）舉行。

此屆觀護盃同時也是第十季[超級籃球聯賽季前熱身賽](../Page/超級籃球聯賽.md "wikilink")，[SBL](../Page/SBL.md "wikilink")7支隊伍都有參賽。

冠軍是[美國菁英隊](../Page/美國菁英隊.md "wikilink")，亞軍是[璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")，季軍是[台灣大籃球隊](../Page/台灣大籃球隊.md "wikilink")，第四名是[達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")。

### 參賽隊伍

  - [裕隆恐龍籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")
  - [達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [金門酒廠籃球隊](../Page/金門酒廠籃球隊.md "wikilink")
  - [台灣銀行籃球隊](../Page/台灣銀行籃球隊.md "wikilink")
  - [台灣大籃球隊](../Page/台灣大籃球隊.md "wikilink")
  - [璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")
  - [美國菁英隊](../Page/美國菁英隊.md "wikilink")

## 2011年觀護盃國際籃球邀請賽

## 2010年觀護盃國際籃球邀請賽

## 2009年觀護盃國際籃球邀請賽

2009年10月14日至10月18日於[花蓮縣立體育館](../Page/花蓮縣立體育館.md "wikilink")（德興小巨蛋）舉行。

冠軍是[中華人民共和國](../Page/中華人民共和國.md "wikilink")[山西猛龍籃球俱樂部](../Page/山西猛龍籃球俱樂部.md "wikilink")，亞軍是[台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")，季軍是[裕隆恐龍籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")，第四名是[韓國](../Page/韓國.md "wikilink")[慶熙大學籃球隊](../Page/慶熙大學籃球隊.md "wikilink")（Spring
Cooking Oil）。

### 參賽隊伍

  - [裕隆恐龍籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")
  - [達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [金門酒廠籃球隊](../Page/金門酒廠籃球隊.md "wikilink")
  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")[中國男子籃球職業聯賽](../Page/中國男子籃球職業聯賽.md "wikilink")[山西猛龍籃球俱樂部](../Page/山西猛龍籃球俱樂部.md "wikilink")
  - [馬來西亞](../Page/馬來西亞.md "wikilink")[國家代表隊](../Page/馬來西亞國家籃球代表隊.md "wikilink")
  - [韓國](../Page/韓國.md "wikilink")[慶熙大學籃球隊](../Page/慶熙大學籃球隊.md "wikilink")（韓國大學冠軍隊）
  - [阿富汗](../Page/阿富汗.md "wikilink")[留美學生聯隊](../Page/阿富汗留美學生聯隊.md "wikilink")（NCAA美國大學籃球聯盟代表隊）

## 2008年觀護盃國際籃球邀請賽

## 2007年觀護盃國際籃球邀請賽

2007年10月3日至10月7日於[花蓮縣立體育館](../Page/花蓮縣立體育館.md "wikilink")（德興小巨蛋）舉行。

冠軍是[達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")，亞軍是[韓國](../Page/韓國.md "wikilink")[慶熙大學籃球隊](../Page/慶熙大學籃球隊.md "wikilink")，季軍是[裕隆恐龍籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")，第四名是[米迪亞精靈籃球隊](../Page/米迪亞精靈籃球隊.md "wikilink")，第五名是[日本](../Page/日本.md "wikilink")[沖繩國王隊](../Page/沖繩國王隊.md "wikilink")（[琉球ゴールデンキングス](../Page/:ja:琉球ゴールデンキングス.md "wikilink")），第六名是[台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")，第七名是[阿富汗](../Page/阿富汗.md "wikilink")[留美學生聯隊](../Page/阿富汗留美學生聯隊.md "wikilink")，第八名是[璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")，第九名是[馬來西亞](../Page/馬來西亞.md "wikilink")[國家代表隊](../Page/馬來西亞國家籃球代表隊.md "wikilink")，第十名是[菲律賓](../Page/菲律賓.md "wikilink")[春天食用油籃球隊](../Page/春天食用油籃球隊.md "wikilink")（Spring
Cooking Oil）。

### 參賽隊伍

  - [裕隆恐龍籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")
  - [達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [璞園建築籃球隊](../Page/璞園建築籃球隊.md "wikilink")（原[東風老鷹籃球隊](../Page/東風老鷹籃球隊.md "wikilink")）
  - [米迪亞精靈籃球隊](../Page/米迪亞精靈籃球隊.md "wikilink")（原[東森羚羊籃球隊](../Page/東森羚羊籃球隊.md "wikilink")）
  - [日本](../Page/日本.md "wikilink")[BJ聯盟](../Page/BJ聯盟.md "wikilink")（[Bj
    league](../Page/:en:Bj_league.md "wikilink")）[沖繩國王隊](../Page/沖繩國王隊.md "wikilink")（[琉球ゴールデンキングス](../Page/:ja:琉球ゴールデンキングス.md "wikilink")）
  - [菲律賓](../Page/菲律賓.md "wikilink")[NBC聯盟](../Page/NBC聯盟.md "wikilink")（[National
    Basketball
    Conference](../Page/:en:National_Basketball_Conference.md "wikilink")）[春天食用油隊](../Page/春天食用油隊.md "wikilink")（Spring
    Cooking Oil）
  - [馬來西亞](../Page/馬來西亞.md "wikilink")[國家代表隊](../Page/馬來西亞國家籃球代表隊.md "wikilink")
  - [韓國](../Page/韓國.md "wikilink")[慶熙大學籃球隊](../Page/慶熙大學籃球隊.md "wikilink")（韓國大學冠軍隊）
  - [阿富汗](../Page/阿富汗.md "wikilink")[留美學生聯隊](../Page/阿富汗留美學生聯隊.md "wikilink")（NCAA美國大學籃球聯盟代表隊）

## 2006年觀護盃國際籃球邀請賽

2006年5月24日至5月28日於[花蓮縣立體育館](../Page/花蓮縣立體育館.md "wikilink")（德興小巨蛋）舉行。

冠軍是[裕隆恐龍籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")，亞軍是[韓國](../Page/韓國.md "wikilink")[慶熙大學籃球隊](../Page/慶熙大學籃球隊.md "wikilink")，季軍是[日本](../Page/日本.md "wikilink")[大阪EVESSA籃球隊](../Page/大阪EVESSA籃球隊.md "wikilink")（[大阪エヴェッサ](../Page/:ja:大阪エヴェッサ.md "wikilink")），第四名是[東森羚羊籃球隊](../Page/東森羚羊籃球隊.md "wikilink")，第五名是[達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")，第六名是[台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")，第七名是[幼敏電銷籃球隊](../Page/幼敏電銷籃球隊.md "wikilink")，第八名是[緯來獵人籃球隊](../Page/緯來獵人籃球隊.md "wikilink")。

### 參賽隊伍

  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [東森羚羊籃球隊](../Page/東森羚羊籃球隊.md "wikilink")
  - [幼敏電銷籃球隊](../Page/幼敏電銷籃球隊.md "wikilink")
  - [菲律賓](../Page/菲律賓.md "wikilink")[NBC聯盟](../Page/NBC聯盟.md "wikilink")（[National
    Basketball
    Conference](../Page/:en:National_Basketball_Conference.md "wikilink")）[春天食用油籃球隊](../Page/春天食用油籃球隊.md "wikilink")（Spring
    Cooking Oil）
  - [韓國](../Page/韓國.md "wikilink")[慶熙大學籃球隊](../Page/慶熙大學籃球隊.md "wikilink")
  - [裕隆恐龍籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")
  - [達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")
  - [緯來獵人籃球隊](../Page/緯來獵人籃球隊.md "wikilink")
  - [臺灣銀行籃球隊](../Page/臺灣銀行籃球隊.md "wikilink")
  - [日本](../Page/日本.md "wikilink")[BJ聯盟](../Page/BJ聯盟.md "wikilink")（[Bj
    league](../Page/:en:Bj_league.md "wikilink")）[大阪EVESSA籃球隊](../Page/大阪EVESSA籃球隊.md "wikilink")（[大阪エヴェッサ](../Page/:ja:大阪エヴェッサ.md "wikilink")）

## 2005年觀護盃國際籃球邀請賽

2005年5月26日至5月29日於[花蓮縣立體育館](../Page/花蓮縣立體育館.md "wikilink")（德興小巨蛋）舉行。

冠軍是[日本](../Page/日本.md "wikilink")[愛信海馬籃球隊](../Page/愛信海馬籃球隊.md "wikilink")（[アイシンシーホース](../Page/:ja:アイシンシーホース.md "wikilink")），亞軍是[台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")，季軍是[緯來電視籃球隊](../Page/緯來獵人籃球隊.md "wikilink")，第四名是[九太科技籃球隊](../Page/九太科技籃球隊.md "wikilink")，第五名是[裕隆籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")，第六名是[東森電視籃球隊](../Page/東森羚羊籃球隊.md "wikilink")，第七名是[新浪獅籃球隊](../Page/新浪獅籃球隊.md "wikilink")，第八名是[菲律賓](../Page/菲律賓.md "wikilink")[春天食用油籃球隊](../Page/春天食用油籃球隊.md "wikilink")（Spring
Cooking Oil）。

### 參賽隊伍

  - [九太科技籃球隊](../Page/九太科技籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [裕隆籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")
  - [緯來電視籃球隊](../Page/緯來獵人籃球隊.md "wikilink")
  - [新浪獅籃球隊](../Page/新浪獅籃球隊.md "wikilink")
  - [日本](../Page/日本.md "wikilink")[JBL聯盟](../Page/JBL聯盟.md "wikilink")（[日本バスケットボールリーグ](../Page/:ja:日本バスケットボールリーグ.md "wikilink")）[愛信海馬籃球隊](../Page/愛信海馬籃球隊.md "wikilink")（[アイシンシーホース](../Page/:ja:アイシンシーホース.md "wikilink")）
  - [菲律賓](../Page/菲律賓.md "wikilink")[NBC聯盟](../Page/NBC聯盟.md "wikilink")（[National
    Basketball
    Conference](../Page/:en:National_Basketball_Conference.md "wikilink")）[春天食用油籃球隊](../Page/春天食用油籃球隊.md "wikilink")（Spring
    Cooking Oil）
  - [東森電視籃球隊](../Page/東森羚羊籃球隊.md "wikilink")

## 2004年觀護盃菁英挑戰賽

2004年6月4日至6月6日於[花蓮縣立體育館](../Page/花蓮縣立體育館.md "wikilink")（德興小巨蛋）舉行。

### 參賽隊伍

  - [新浪獅籃球隊](../Page/新浪獅籃球隊.md "wikilink")
  - [一統徵信籃球隊](../Page/一統徵信籃球隊.md "wikilink")
  - [達欣工程籃球隊](../Page/達欣工程籃球隊.md "wikilink")
  - [台灣啤酒籃球隊](../Page/台灣啤酒籃球隊.md "wikilink")
  - [九太科技籃球隊](../Page/九太科技籃球隊.md "wikilink")
  - [裕隆汽車籃球隊](../Page/裕隆恐龍籃球隊.md "wikilink")

## 2003年觀護盃全國甲一級四強菁英挑戰賽

## 參照

  - [超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink")

## 外部連結

  - [花蓮縣籃球協會](http://www.hlba.tw/)

  - [Taiwan
    Hoops](http://twhoops.blogspot.com/)有關[觀護盃](http://twhoops.blogspot.com/search/label/guanhu%20cup)的資料，裡面有各場比賽分數結果

  - [Asia-Basket](http://www.asia-basket.com/)，裡面可以查到參賽各國隊伍的球員名單與資料

[Category:台灣籃球競賽](../Category/台灣籃球競賽.md "wikilink")
[Category:花蓮縣體育](../Category/花蓮縣體育.md "wikilink")