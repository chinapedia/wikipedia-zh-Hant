**视觉皮层**（[英文](../Page/英文.md "wikilink")：**Visual
cortex**）是指[大脑皮层中主要负责处理](../Page/大脑皮层.md "wikilink")[视觉信息的部分](../Page/视觉.md "wikilink")，位于大脑后部的[枕叶](../Page/枕叶.md "wikilink")。[人类的视觉皮层包括初级视皮层](../Page/人类.md "wikilink")（V1，亦称纹状皮层（Striate
cortex））以及纹外皮层（Extrastriate
cortex，例如V2，V3，V4，V5等）。初级视皮层位于[Brodmann](../Page/Brodmann分区系统.md "wikilink")17区。纹外皮层包括[Brodmann](../Page/Brodmann分区系统.md "wikilink")18区和[Brodmann](../Page/Brodmann分区系统.md "wikilink")19区。

[大脑的两个半球各有一部分视觉皮层](../Page/大脑.md "wikilink")。左半球的视觉皮层从右视野接收信息，而右半球的视觉皮层从左视野接收信息。

## 概况

视觉皮层坐落于[枕叶的](../Page/枕叶.md "wikilink")[距状裂周围](../Page/距状裂.md "wikilink")，是一种典型的感觉型粒状皮层（Koniocortex
cortex）。它的输入主要来自于[丘脑的](../Page/丘脑.md "wikilink")[外侧膝状体](../Page/外侧膝状体.md "wikilink")。初级视皮层（V1）的输出信息出送到两个渠道，分别成为**背侧流**（Dorsal
stream）和**腹侧流**（Ventral stream）。

  - **背侧流**起始于V1，通过V2，进入背内侧区和[中颞区](../Page/中颞区.md "wikilink")（MT，亦称V5），然后抵达[顶下小叶](../Page/顶下小叶.md "wikilink")。背侧流常被称为“**空间通路**”（**Where/How
    pathway**），参与处理物体的空间位置信息以及相关的运动控制，例如眼跳（saccade）和伸取（Reaching）。

<!-- end list -->

  - **腹侧流**起始于V1，依次通过V2，V4，进入[下颞叶](../Page/下颞叶.md "wikilink")（Inferior
    temporal lobe）。该通路常被称为“内容通路”（**Who/What
    pathway**），参与物体识别，例如面孔识别。该通路也于[长期记忆有关](../Page/长期记忆.md "wikilink")。

[category:大脑](../Page/category:大脑.md "wikilink")
[category:视觉系统](../Page/category:视觉系统.md "wikilink")