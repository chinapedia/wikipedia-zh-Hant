**-{zh-cn:角;zh-hk:角;zh-tw:}-一**（**α Vir** / **室女座α**
/[英语](../Page/英语.md "wikilink")：****）位於[室女座](../Page/室女座.md "wikilink")，是全天空[第十五亮的](../Page/恆星亮度列表.md "wikilink")[恆星](../Page/恆星.md "wikilink")，也是室女座最明亮的恆星。[北半球的觀測者在](../Page/北半球.md "wikilink")[春季夜晚](../Page/春季.md "wikilink")，可以在東南方向的天空看到这颗明亮的1等星。想要找到角宿一，觀測者只需要沿着位于[大熊座的](../Page/大熊座.md "wikilink")[北斗七星的斗柄和](../Page/北斗七星.md "wikilink")[牧夫座的](../Page/牧夫座.md "wikilink")[大角连成的曲线方向往下就可以看到它](../Page/大角.md "wikilink")。角宿一是一顆[藍巨星](../Page/藍巨星.md "wikilink")，屬於[仙王座β型變星](../Page/仙王座β型變星.md "wikilink")。角宿一距離[地球有](../Page/地球.md "wikilink")260[光年之遥](../Page/光年.md "wikilink")\[1\]\[2\]。

## 觀測史

[古希臘](../Page/古希臘.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")[喜帕恰斯根據角宿一的移動發現](../Page/喜帕恰斯.md "wikilink")[歲差運動](../Page/歲差_\(天文\).md "wikilink")\[3\]。[埃及](../Page/埃及.md "wikilink")[底比斯一座獻給](../Page/底比斯_\(埃及\).md "wikilink")[哈索爾](../Page/哈索爾.md "wikilink")[女神的](../Page/女神.md "wikilink")[神廟被認為是當時的人們根據角宿一的方位在西元前](../Page/神廟.md "wikilink")3200年所建造的，隨著時間流逝，角宿一與神廟的相對位置出現緩慢但明顯的變化\[4\]。[波蘭天文學家](../Page/波蘭.md "wikilink")[尼古拉·哥白尼使用自己製造的工具對角宿一進行大量觀測](../Page/尼古拉·哥白尼.md "wikilink")，主要是為了研究歲差運動\[5\]\[6\]。因為角宿一是距離[太陽最近的大質量](../Page/太陽.md "wikilink")[雙星系統之一](../Page/雙星.md "wikilink")，所以天文學家經常對它進行大量研究\[7\]。

## 物理性質

角宿一實際上是一對距離很近，互相環繞公轉的[雙星系统](../Page/雙星.md "wikilink")，系统内兩顆恒星彼此距離只有0.12[天文单位](../Page/天文单位.md "wikilink")，[公轉軌道周期只有](../Page/公轉.md "wikilink")4.0145天。因為這兩顆恆星距離非常接近，所以肉眼無法分辨出來。這對双星的軌道運動變化導致單一恆星的[光學頻譜](../Page/光學頻譜.md "wikilink")[譜線產生](../Page/譜線.md "wikilink")[都卜勒效應](../Page/都卜勒效應.md "wikilink")，所以角宿一也是[光譜聯星](../Page/聯星#光譜聯星.md "wikilink")
\[8\]。1966年至1970年之間，天文學家使用[干涉儀觀測角宿一](../Page/干涉儀.md "wikilink")，直接計算它的軌道數據及角距離。天文學家後來認為角距離為,而軌道[半長軸略大於](../Page/半長軸.md "wikilink")\[9\]。

角宿一A的[光谱属温度很高的B](../Page/光谱.md "wikilink")1
III-IV型，[恆星光譜位於](../Page/恆星光譜.md "wikilink")[巨星及](../Page/巨星.md "wikilink")[次巨星之間](../Page/次巨星.md "wikilink")，不再屬於[B型主序星](../Page/B型主序星.md "wikilink")
\[10\]。它的質量超過10倍[太陽質量](../Page/太陽質量.md "wikilink")，半徑為[太陽的](../Page/太陽半徑.md "wikilink")7倍。角宿一A的实际亮度为太阳的12,100倍，也是角宿一B的8倍。角宿一A是將會成為距離太陽最近的[II型超新星之一](../Page/II型超新星.md "wikilink")\[11\]。角宿一A自轉速度很快，[赤道自轉速度為每秒](../Page/赤道.md "wikilink")199[公里](../Page/公里.md "wikilink")
\[12\]。

角宿一B的[光谱属是B](../Page/光谱.md "wikilink")2
V型，為一顆[主序星](../Page/主序星.md "wikilink")
\[13\]。角宿一B也是[仙王座β型变星](../Page/仙王座β型变星.md "wikilink")，亮度變化週期大於0.1738天，與[徑向速度變化週期一致](../Page/徑向速度.md "wikilink")，顯示恆星表面正在規律的膨脹與縮小。

天文學家觀測到角宿一B出現[S-S效應](../Page/S-S效應.md "wikilink")，這是一種光學頻譜譜線強度在公轉時產生不規則變化的效應，當恆星遠離觀測者時，譜線強度將會減弱\[14\]。這種現象可能是因為角宿一A散發出強烈的[恆星風導致遠離遠離觀測者的角宿一B亮度降低](../Page/恆星風.md "wikilink")\[15\]。角宿一B比角宿一A還小，質量約為7倍[太陽質量](../Page/太陽質量.md "wikilink")，半徑為[太陽的](../Page/太陽半徑.md "wikilink")3.6倍\[16\]。

角宿一也是[旋轉橢球變星](../Page/旋轉橢球變星.md "wikilink")，所以[視星等會有](../Page/視星等.md "wikilink")0.03等的變化，幾乎難以察覺\[17\]。角宿一雙星系统的两颗恒星自轉速度比公轉速度還快。角宿一是温度最高的一等星之一，高温使其同时辐射强度相当高的[紫外线](../Page/紫外线.md "wikilink")，这使得角宿一实际比其外观上光度更高。

## 觀測

[Finding_spica.png](https://zh.wikipedia.org/wiki/File:Finding_spica.png "fig:Finding_spica.png")

角宿一位于[天球南緯](../Page/天球.md "wikilink")11度附近，同时也坐落在接近[黄道的位置上](../Page/黄道.md "wikilink")，因此有可能被[月球和其他](../Page/月球.md "wikilink")[行星遮掩](../Page/行星.md "wikilink")，发生行星[掩星现象](../Page/掩星.md "wikilink")。角宿一最近一次行星掩星現象發生在1783年11月10日，當時[金星正好通過角宿一前方](../Page/金星.md "wikilink")\[18\]。下一次行星掩星現象將發生在2197年9月2日，屆時金星將再度通過角宿一前方。太陽在每年10月16日都會通過角宿一北方2°左右的地區，它的[偕日升發生在兩個星期之後](../Page/偕日升.md "wikilink")，金星會在角宿一偕日升時通過它的前方。金星在2009年11月3日通過角宿一北方3.5°左右的地區\[19\]。

## 文化意義

[秋季時](../Page/秋季.md "wikilink")[太阳会经过室女座](../Page/太阳.md "wikilink")，这使得角宿一成为秋季作物收获的象征星，英文名稱来自于[拉丁语中](../Page/拉丁语.md "wikilink")，意为“室女的[麥穗](../Page/麥.md "wikilink")”。角宿一在中國天文學中位於[角宿](../Page/角宿.md "wikilink")。角宿一在印度天文學中，對應於[二十七宿中的Chitra](../Page/二十七宿.md "wikilink")。在中世紀天文學中，角宿一被分類為[吉普賽固定之星](../Page/吉普賽固定之星.md "wikilink")，象徵[祖母綠及](../Page/祖母綠.md "wikilink")[鼠尾草](../Page/藥用鼠尾草.md "wikilink")。

## 參考文獻

## 外部連結

  - [Spica](http://stars.astro.illinois.edu/sow/spica.html)

[室女座α](../Category/拜耳天體.md "wikilink") [J](../Category/双星.md "wikilink")
[Category:巨星](../Category/巨星.md "wikilink")
[Category:次巨星](../Category/次巨星.md "wikilink")
[Category:室女座](../Category/室女座.md "wikilink") [Category:角
(星官)](../Category/角_\(星官\).md "wikilink")
[Category:星座最亮星](../Category/星座最亮星.md "wikilink")
[Category:仙王座β變星](../Category/仙王座β變星.md "wikilink")

1.  <http://www.sci-museum.kita.osaka.jp/~kato/8.mus_pla/brights.html>

2.

3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18. [Earth-Sky Tonight,
    March 26, 2010](http://www.berthoudrecorder.com/2010/03/25/earthsky-tonight-%E2%80%94-march-26-2010-moon-swings-close-to-regulus/)


19.