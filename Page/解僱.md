[1909_Tyee_-_Faculty_Foot.jpg](https://zh.wikipedia.org/wiki/File:1909_Tyee_-_Faculty_Foot.jpg "fig:1909_Tyee_-_Faculty_Foot.jpg")表示)的插畫。\]\]
**解僱**，俗稱**炒魷魚**、**開除**。解僱是「永久終止[僱傭關係](../Page/僱傭.md "wikilink")」或「終止[勞務](../Page/勞務.md "wikilink")[合約關係](../Page/合約.md "wikilink")」的一種，關鍵是由[資方或用人單位方面](../Page/僱主.md "wikilink")，主動提出。相反，若由勞方[僱員主動提出的](../Page/僱員.md "wikilink")，稱為「請辭」，或者「辭職」。

有些地方法律規定不可以[無理解僱](../Page/無理解僱.md "wikilink")，至於解僱的理由可以包括:

  - 工作失誤，令公司有損失。
  - 經常遲到早退。\[1\]
  - 工作能力未能勝任。
  - 公司改組，內部另外未有其他適合的空缺。
  - 欠缺誠信
  - [冗員](../Page/冗員.md "wikilink")\[2\]\[3\]

## 「解僱」與「辭職」的比較

按勞務合約條款，或者當地《[勞動法](../Page/勞動法.md "wikilink")》:

  - 「解僱」:
    由僱主按比例，支付[代通知金予僱員](../Page/代通知金.md "wikilink")，以補償僱員失去此[工作](../Page/工作.md "wikilink")[收入的損失](../Page/收入.md "wikilink")，又稱為「資遣」。
  - 單合約僱員因為未能完成指定的完整合約期，所以無權追討約滿[花紅](../Page/花紅_\(酬金\).md "wikilink")。
  - 部分公司在想要裁員時，會以各種手段強迫員工「自願辭職」，以規避資遣費。

## 注释

## 参见

  - [裁員](../Page/裁員.md "wikilink")：即是大量解僱職員，當公司[結業](../Page/清盤.md "wikilink")、[企業改組之時常有](../Page/企業改組.md "wikilink")。
  - [提早退休計劃](../Page/提早退休計劃.md "wikilink")
  - [解僱部門](../Page/解僱部門.md "wikilink")

## 外部鏈結

  - [中華民國勞動部- 雇主可以資遣（解僱）我嗎？](https://www.mol.gov.tw/23323/23324/23326/)

## 參考資料

[解雇](../Category/解雇.md "wikilink")
[Category:僱傭](../Category/僱傭.md "wikilink")

1.
2.
3.