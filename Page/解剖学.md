**解剖学**（英語：**Anatomy**）是涉及生命体的结构和组织的[生物学分支学科](../Page/生物学.md "wikilink")\[1\]。解剖学和[胚胎學](../Page/胚胎學.md "wikilink")、[比較解剖學](../Page/比較解剖學.md "wikilink")、[進化生物學和](../Page/進化生物學.md "wikilink")[系統發育有密切關係](../Page/系統發育.md "wikilink")\[2\]，而這些也可以看出解剖結構在即時（胚胎學）和長期（演化）時間尺度下的變化。[人体解剖学是](../Page/人体解剖学.md "wikilink")[醫學的基礎學科之一](../Page/醫學.md "wikilink")\[3\]。

解剖学也可以分為微觀尺度及巨觀尺度。巨觀尺度的解剖学為，是用[肉眼來觀察動物的身體及器官](../Page/肉眼.md "wikilink")。大體解剖學也包括，而其他的部位常利用[剖割的方法來進行研究](../Page/解剖.md "wikilink")。顯微鏡解剖学是用光學儀器（如[顯微鏡](../Page/顯微鏡.md "wikilink")）來研究[組織](../Page/組織_\(生物\).md "wikilink")（[組織學](../Page/組織學.md "wikilink")）、[細胞及胞器](../Page/細胞.md "wikilink")。

[解剖学史的特點是對](../Page/解剖学史.md "wikilink")[人體結構及](../Page/人體.md "wikilink")[器官功能的漸進式了解](../Page/器官.md "wikilink")。其方法也有很大的進展，從一早期檢驗動物及人的屍體，到二十世紀的[醫學成像技術](../Page/醫學成像.md "wikilink")，包括，[超音波和](../Page/医学超声检查.md "wikilink")[核磁共振成像技術](../Page/核磁共振成像.md "wikilink")。

解剖学和[生理学都是研究器官以及各部份的結構及](../Page/生理学.md "wikilink")，因此很自然的會用進行研究。

如果解剖學單指[人體解剖學](../Page/人體解剖學.md "wikilink")，這時候解剖學會依照各器官系統性地分類，而不是依部位來陳述。每篇解剖學的文章首先包括一个[器官或](../Page/器官.md "wikilink")[系统](../Page/系统.md "wikilink")。例如：[神经](../Page/神经.md "wikilink")、[动脉](../Page/动脉.md "wikilink")、[心臟等的结构描述](../Page/心臟.md "wikilink")，根據在人體找到甚麼而定。就此而論，解剖學文章有双重目的；首先，提供關于結構的足夠資料，令文章在生理学、外科、內科和病理学方面均有可謮性；第二，给非专家的查詢者或在某門科学分支上工作的人提供建立解剖學的現代科學基礎的主要理论。

## 動物組織

[Anima_cell_notext.svg](https://zh.wikipedia.org/wiki/File:Anima_cell_notext.svg "fig:Anima_cell_notext.svg")
[後生動物包括了](../Page/後生動物.md "wikilink")[異養和](../Page/異營生物.md "wikilink")[能動的](../Page/能動性.md "wikilink")[多細胞生物](../Page/多細胞生物.md "wikilink")
（不過有些的生活形態是生活）。大部份動物的身體可以分為不同的[组织](../Page/组织_\(生物学\).md "wikilink")，這些動物稱為[真後生動物](../Page/真後生動物.md "wikilink")。這些動物的體內有消化腔、有一個或二個開口，其[配子是由多細胞組成的性器官產生](../Page/配子.md "wikilink")，而[受精卵在胚胎發育期間有](../Page/受精卵.md "wikilink")[囊胚階段](../Page/囊胚.md "wikilink")。[多孔动物门的細胞沒有依其功用不同而分化](../Page/多孔动物门.md "wikilink")，因此不算是後生動物\[4\]。

[動物細胞和](../Page/動物細胞.md "wikilink")[植物細胞不同](../Page/植物細胞.md "wikilink")，動物細胞不會有[細胞壁及](../Page/細胞壁.md "wikilink")[葉綠體](../Page/葉綠體.md "wikilink")。動物細胞內若有空泡，其數量會較在植物細胞中要多，大小也會比較小。身體組織由各種的細胞組成，包括[肌肉](../Page/肌肉.md "wikilink")、[神經及](../Page/神經.md "wikilink")[皮膚中的細胞](../Page/皮膚.md "wikilink")。細胞一般都會有由[磷脂組成的](../Page/磷脂.md "wikilink")[細胞膜](../Page/細胞膜.md "wikilink")、以及[細胞質和](../Page/細胞質.md "wikilink")[细胞核](../Page/细胞核.md "wikilink")。動物的細胞都是由胚胎的[胚层所分化而成](../Page/胚层.md "wikilink")。構造較簡單的无脊椎动物是由外胚层及内胚层二層的胚层分化而成，這種動物稱為，其他較複雜的动物有三層胚层（[外胚层](../Page/外胚层.md "wikilink")、[中胚层及](../Page/中胚层.md "wikilink")[内胚层](../Page/内胚层.md "wikilink")），稱為\[5\]。

動物組織一般可以分為[结缔组织](../Page/结缔组织.md "wikilink")、[上皮組織](../Page/上皮組織.md "wikilink")、[肌肉及](../Page/肌肉.md "wikilink")[神經組織](../Page/神經組織.md "wikilink")。

## 动物解剖学

动物解剖学当中的[比较解剖学](../Page/比较解剖学.md "wikilink")（即动物形态学）指不同[动物種類間的结构的研究](../Page/动物.md "wikilink")，專論解剖學則局限于个別一種动物的结构的研究。

### 脊椎动物解剖学

[VolRenderShearWarp.gif](https://zh.wikipedia.org/wiki/File:VolRenderShearWarp.gif "fig:VolRenderShearWarp.gif")頭骨\]\]
[脊椎动物在其](../Page/脊椎动物.md "wikilink")[胚胎發育期的外形都相當類似](../Page/胚胎發育.md "wikilink")，有共同的[脊索动物特徵](../Page/脊索动物.md "wikilink")：
[脊索](../Page/脊索.md "wikilink")、中空的、以及肛門後的尾巴。[脊髓受](../Page/脊髓.md "wikilink")[脊柱保護](../Page/脊柱.md "wikilink")，在[脊索及](../Page/脊索.md "wikilink")[消化道的上方](../Page/消化道.md "wikilink")\[6\]。神經組織是由[外胚層衍生](../Page/外胚層.md "wikilink")，[結締組織是由](../Page/結締組織.md "wikilink")[中胚層衍生](../Page/中胚層.md "wikilink")，而腸是由[內胚層衍生](../Page/內胚層.md "wikilink")。最後是尾巴，是脊髓和椎骨的延伸。嘴巴在動物的最前方，而[肛門是尾巴的下方](../Page/肛門.md "wikilink")\[7\]。[脊柱是用來定義脊椎動物的特徵](../Page/脊柱.md "wikilink")，是由分段的[椎骨形成](../Page/椎骨切迹.md "wikilink")。大多數的脊椎動物，脊索成為[椎間盤的](../Page/椎間盤.md "wikilink")[髓核](../Page/髓核.md "wikilink")，不過像[鱘魚和](../Page/鱘魚.md "wikilink")[腔棘魚之類的脊椎動物](../Page/腔棘魚.md "wikilink")，脊索仍會維持到成年\[8\]。[顎脊椎動物的特徵是成對的附肢](../Page/顎脊椎動物.md "wikilink")、鰭或腿部，這些部份視為是[同源器官](../Page/同源器官.md "wikilink")\[9\]。

### 无脊椎动物解剖学

[Chirocephalus_diaphanus_male_head.png](https://zh.wikipedia.org/wiki/File:Chirocephalus_diaphanus_male_head.png "fig:Chirocephalus_diaphanus_male_head.png")的頭部\]\]
[无脊椎动物分布的範圍很廣](../Page/无脊椎动物.md "wikilink")，從[草履虫之類的單細胞](../Page/草履虫.md "wikilink")[真核生物到像](../Page/真核生物.md "wikilink")[章魚](../Page/章魚.md "wikilink")、[龍蝦及](../Page/龍蝦.md "wikilink")[蜻蜓等複雜的多細胞动物](../Page/蜻蜓.md "wikilink")。動物的[物種中有](../Page/物種.md "wikilink")95%是无脊椎动物。依其定義來看，无脊椎动物沒有像脊椎之類的結構。單細胞[原生動物的細胞其基本結構和多細胞生物的細胞相近](../Page/原生動物.md "wikilink")，但其單細胞動物的細胞中有部份特化為類似生物體組織或是器官的功能，像運動可能會透過[纖毛](../Page/纖毛.md "wikilink")、[鞭毛或是](../Page/鞭毛.md "wikilink")[偽足進行](../Page/偽足.md "wikilink")，會用[吞噬作用來攝取食物](../Page/吞噬作用.md "wikilink")，可能透過[光合作用取得能量](../Page/光合作用.md "wikilink")，其細胞可能可以以[內骨骼或](../Page/內骨骼.md "wikilink")[內骨骼為其骨架](../Page/內骨骼.md "wikilink")。有些原生動物會形成多细胞的集落\[10\]。

[後生動物是多細胞生物](../Page/後生動物.md "wikilink")，不同種類的細胞會依其功能形成群體。後生動物組織中最基本的是上皮和結締組織，幾乎所有的无脊椎动物都有這些組織。上皮的外層一般是由上皮細胞組成，會分泌[细胞外基质來支持生物體](../Page/细胞外基质.md "wikilink")。[內骨骼是](../Page/內骨骼.md "wikilink")[中胚層衍生而來](../Page/中胚層.md "wikilink")，於[棘皮動物](../Page/棘皮動物.md "wikilink")，[海綿和一些](../Page/海綿.md "wikilink")[頭足動物都有這類組織](../Page/頭足動物.md "wikilink")。[外骨骼是由表皮衍生](../Page/外骨骼.md "wikilink")，[節肢動物](../Page/節肢動物.md "wikilink")（[昆蟲](../Page/昆蟲.md "wikilink")、[蜘蛛](../Page/蜘蛛.md "wikilink")、[蜱](../Page/蜱.md "wikilink")、[蝦](../Page/蝦.md "wikilink")、[蟹](../Page/蟹.md "wikilink")、[龍蝦](../Page/龍蝦.md "wikilink")）的外骨骼是由[幾丁質組成](../Page/幾丁質.md "wikilink")。而[軟體動物及腕足類及一些](../Page/軟體動物.md "wikilink")[多毛纲的殼是由](../Page/多毛纲.md "wikilink")[碳酸鈣組成](../Page/碳酸鈣.md "wikilink")，[矽藻及](../Page/矽藻.md "wikilink")[放射蟲的外殼是由](../Page/放射蟲.md "wikilink")[二氧化矽構成](../Page/二氧化矽.md "wikilink")\[11\]。其他的无脊椎动物可能沒有堅固的外殼，但其表皮會分泌許多不同的成份，像海綿表面的pinacoderm、腔腸動物（[水螅](../Page/水螅.md "wikilink")、[海葵](../Page/海葵.md "wikilink")，[水母](../Page/水母.md "wikilink")）表皮膠質中的刺胞，以及[环节动物门表面的](../Page/环节动物门.md "wikilink")[膠原蛋白](../Page/膠原蛋白.md "wikilink")。表皮層中也可能包括像感覺細胞、腺細胞和刺細胞的細胞。其表皮也可能有凸起，例如[微絨毛](../Page/微絨毛.md "wikilink")，纖毛，刷毛，和等\[12\]。

[马尔切洛·马尔皮吉是顯微解剖学之父](../Page/马尔切洛·马尔皮吉.md "wikilink")，他發現植物體內有小管，和他在蠶身上看到的類似。他也觀查到若從樹幹除去一圈的樹皮，在去除部份的上方會有組織腫脹
\[13\]。

## 人類解剖学

人体解剖学（[英语](../Page/英语.md "wikilink")：或）是研究正常人体形态结构的科学。广义的解剖学包括（gross
anatomy，以肉眼觀察的解剖學）、[组织学](../Page/组织学.md "wikilink")（微觀的以[顯微鏡觀察的解剖學](../Page/顯微鏡.md "wikilink")）、[细胞学和](../Page/细胞学.md "wikilink")[胚胎学](../Page/胚胎学.md "wikilink")（加上時間軸的解剖學）。解剖学又可分为[系统解剖学和](../Page/系统解剖学.md "wikilink")[局部解剖学](../Page/局部解剖学.md "wikilink")。系统解剖学着重在人体构成的各系统分析，而局部解剖学注重在于人体部分区域的分析，因而与外科学联系紧密

從實用性看，对于人类的研究是專論解剖學當中最重要的分組，故此人类解剖学可从不同的观点处理。醫學當中的人体解剖学包括健康人体的各样结构的确切形態、位置、大小和各結構間的关系的知識，上述學問被稱為描述性或形態性人類解剖學，又稱為人體解剖學。

人體極其错综复杂，所以只有少數的专业人體解剖学家，經過多年观察病人后，能掌握它的全部细节。大多数專家專攻人體某些部分，例如[大脑或](../Page/大脑.md "wikilink")[内脏](../Page/内脏.md "wikilink")，對于人體其餘部位，亦掌握能應付工作需要的知识。解剖學的知識必须从重复的解剖和觀察人類[屍體當中得到](../Page/屍體.md "wikilink")。這種知識如同領航者的知識一般，必須精確，而且在緊要關頭能夠發揮。從[形態學上的觀點](../Page/形態學.md "wikilink")，人體解剖學是一項科學性和令人著迷的學問，其目標是探索人體現有的結構從何而來，需要[胚胎學或](../Page/胚胎學.md "wikilink")[發育生物學和](../Page/發育生物學.md "wikilink")[組織學這些相近學科的知識](../Page/組織學.md "wikilink")。另外，如果涉及病理方面，該解剖学亦可通稱[病理解剖学](../Page/病理解剖学.md "wikilink")。這時候，解剖就需要研究生病器官。

正常解剖学的分科，根据研究方法和叙述方式的不同，解剖学可分为以下学科：系统解剖学（systenmatic
anatomy）是将人体器官划分为若干功能系统来进行描述和研究的学科；局部解剖学（regional
anatomy）是在系统解剖学的基础上按局部（头、颈、胸、腹、盆、会阴、上肢、下肢等）来研究人体各部分的结构形态和相互关系的学科；为适应[X射线计算机断层成像](../Page/X射线计算机断层成像.md "wikilink")，[超声和](../Page/超声.md "wikilink")[磁共振成像等应用](../Page/磁共振成像.md "wikilink")，研究人体在不同层面上各器官形态结构、毗邻关系的学科，称断层解剖学（sectional
anatomy）；结合临床需要，以临床各科应用为目的的而进行人体解剖学研究的学科，称外科解剖学（surgical
anatomy）；应用[X射线研究人体形态结构的则称X射线解剖学](../Page/X射线.md "wikilink")（X-ray
anatomy）；研究人体在生活过程中，各器官形态结构的变化规律，或在特定条件下，观察外因对人体器官形态结构变化影响的解剖学，称为机能解剖学（functional
anatomy）；以研究体育运动或提高体育运动效果为目的的解剖学，称运动解剖学（locomotive
anatomy）；而比較不同種族的人類之間的差異的解剖學則稱為人體人类学或人类学解剖学。并且随着医学与生物学的迅猛发展，形态学的研究已进入分子生物学水平，对人体的研究会更深入，将会有一些新的学科不断从解剖学中分化出去，但广义上仍属于解剖学范畴。

## 植物解剖学

植物解剖學主要是研究植物的內部構造，通常是現生植物，從而可以瞭解植物體各部之功能\[14\]。

植物是高度[演化](../Page/演化.md "wikilink")，其結構上及功能上的特化，於植株外部反應了其身體的[分化](../Page/分化.md "wikilink")，在內部則反應於不同類別的[細胞](../Page/細胞.md "wikilink")、[組織](../Page/组织_\(生物学\).md "wikilink")、組織系統、[器官等](../Page/器官.md "wikilink")\[15\]。

早期的植物解剖學被涵納在描述植物的外形及外部構造的[植物形態學之中](../Page/植物形態學.md "wikilink")，大約在20世紀中期方被考慮為一獨立的學門，具有專門的研究領域，並被認為是專門研究植物內部構造的科學\[16\]。現代的植物解剖學經常是[細胞層級的](../Page/細胞.md "wikilink")[顯微構造](../Page/顯微鏡學.md "wikilink")，即[組織切片](../Page/组织_\(生物学\).md "wikilink")、[顯微鏡學相關議題等](../Page/顯微鏡學.md "wikilink")\[17\]。

## 解剖学的其他分支

  - 是研究出現在生物外部輪廓，可輕易看到的解剖標誌\[18\]。醫生或是獸醫可以依此看出一些深層結構的位置及情形。此處的「表面」是指這些部位較接近生物體的表面\[19\]。

  - 美學解剖学是和美學有關的解剖学研究。

## 参考文献

## 參見

  - [解剖学基础模型本体](../Page/解剖学基础模型.md "wikilink")

  - [植物解剖學](../Page/植物解剖學.md "wikilink")

  - [艾氏人体解剖学](../Page/艾氏人体解剖学.md "wikilink")

  -
  -
  - [人体解剖学方位](../Page/人体解剖学方位.md "wikilink")

  -
  - [人體](../Page/人體.md "wikilink")

  -
## 外部連結

  -
  - [Anatomy](http://www.bbc.co.uk/podcasts/series/iots/all#playepisode115),
    *In Our Time*. BBC Radio 4. Melvyn Bragg with guests Ruth
    Richardson, Andrew Cunningham and Harold Ellis.

{{-}}

[解剖学](../Category/解剖学.md "wikilink")
[Category:医学院所教科目](../Category/医学院所教科目.md "wikilink")
[Category:希腊语外来词](../Category/希腊语外来词.md "wikilink")

1.  Merriam Webster Dictionary

2.

3.  Arráez-Aybar et al. (2010). Relevance of human anatomy in daily
    clinical practice. Annals of Anatomy-Anatomischer Anzeiger, 192(6),
    341–348.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14. \[Esau, K. 1977.\]，Esau開宗明義定義本書的內容，即植物解剖學的內容及範疇。

15. \[Esau, K. 1977.\]，Introduction，第1-6頁。

16. Raven, P. H.; Evert, R. F. and Eichhorn, S. E. (2005) *Biology of
    Plants* (7th edition) W. H. Freeman, New York, page 9, ISBN
    0-7167-1007-2

17. Evert, Ray Franklin and Esau, Katherine (2006) *Esau's Plant
    anatomy: meristems, cells, and tissues of the plant body - their
    structure, function and development* Wiley, Hoboken, New Jersey,
    [page *xv*](http://books.google.com/books?id=iM-BHf-yWVAC&pg=PR15),
    ISBN 0-471-73843-3

18.
19.