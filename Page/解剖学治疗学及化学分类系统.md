**解剖学治疗学及化学分类系统**（），是[世界卫生组织对](../Page/世界卫生组织.md "wikilink")[药品的官方分类系统](../Page/药品.md "wikilink")。

ATC系统由世界卫生组织药物统计方法整合中心（）所制定，第一版在1976年发布。1996年，ATC系统成为[国际标准](../Page/国际标准.md "wikilink")。现在ATC系统已经发布2006版。

## 代码

ATC代码共有7位，其中第1、4、5位为字母，第2、3、6、7位为数字。 ATC系统将药物分为5个级别

### 第一级

ATC代码第一级为一位字母，表示解剖学上的分类，共有14个组别。

| 编码                                    | 内容                                                                      |
| ------------------------------------- | ----------------------------------------------------------------------- |
| [**A**](../Page/ATC代码A.md "wikilink") | [消化系统和](../Page/消化系统.md "wikilink")[代谢系统](../Page/代谢系统.md "wikilink")   |
| [**B**](../Page/ATC代码B.md "wikilink") | [血液及](../Page/血液.md "wikilink")[造血器官](../Page/造血.md "wikilink")         |
| [**C**](../Page/ATC代码C.md "wikilink") | [心血管系统](../Page/心血管系统.md "wikilink")                                    |
| [**D**](../Page/ATC代码D.md "wikilink") | [皮肤病](../Page/皮肤病.md "wikilink")                                        |
| [**G**](../Page/ATC代码G.md "wikilink") | [泌尿生殖系统及](../Page/泌尿生殖系统.md "wikilink")[性激素](../Page/性激素.md "wikilink") |
| [**H**](../Page/ATC代码H.md "wikilink") | 体激素                                                                     |
| [**J**](../Page/ATC代码J.md "wikilink") | 抗感染药                                                                    |
| [**L**](../Page/ATC代码L.md "wikilink") | [抗肿瘤药及](../Page/抗肿瘤药.md "wikilink")[免疫用药](../Page/免疫.md "wikilink")     |
| [**M**](../Page/ATC代码M.md "wikilink") | [肌骨骼系统](../Page/肌骨骼系统.md "wikilink")                                    |
| [**N**](../Page/ATC代码N.md "wikilink") | [神经系统](../Page/神经系统.md "wikilink")                                      |
| [**P**](../Page/ATC代码P.md "wikilink") | 抗寄生虫药                                                                   |
| [**R**](../Page/ATC代码R.md "wikilink") | [呼吸系统](../Page/呼吸系统.md "wikilink")                                      |
| [**S**](../Page/ATC代码S.md "wikilink") | [感觉器](../Page/感觉器.md "wikilink")                                        |
| [**V**](../Page/ATC代码V.md "wikilink") | 其它                                                                      |

ATC代码第一级不使用 ，，，，，，，，，， 和  字母。

### 第二级

  - ATC代码第二级为两位数字，表示治疗学上的分类。

### 第三级

  - ATC代码第三级为一位字母，表示药理学上的分类。

### 第四级

  - ATC代码第四级为一位字母，表示化学上的分类。

### 第五级

  - ATC代码第五级为两位数字，表示化合物上的分类。

### ATCvet

*ATCvet*（Anatomical Therapeutic Chemical Classification System for
veterinary medicinal
products）是用于[兽药分类的一个体系](../Page/兽药.md "wikilink")。ATCvet代码在多数人用药物的ATC代码前加上Q。

而另一些代码则为兽药专用，如*[QI](../Page/ATCvet_code_QI.md "wikilink") 免疫学*,
*[QJ51](../Page/ATCvet_code_QJ51.md "wikilink") 乳房用抗菌药*或*QN05AX90
[安哌齐特](../Page/安哌齐特.md "wikilink")*\[1\]。

## 举例

[对乙酰氨基酚](../Page/对乙酰氨基酚.md "wikilink")（扑热息痛）的代码是N02BE01

  - 第1位N，表示神经系统(Nervous System)
  - 第2、3位02，表示止痛药(Analgesics)
  - 第4位B，表示其他止痛退药及退热药(Other Analgesics and Antipyretics)
  - 第5位E，表示苯胺类(Anilides)
  - 第6、7位01，表示乙酰氨基酚(Paracetamol)

## 参见

  - [国际疾病与相关健康问题统计分类](../Page/国际疾病与相关健康问题统计分类.md "wikilink")（[ICD-10](../Page/国际疾病与相关健康问题统计分类第十版.md "wikilink")）

## 參考文獻

## 外部链接

  -
  -
{{-}}

[ATC代码](../Category/ATC代码.md "wikilink")
[Category:藥物列表](../Category/藥物列表.md "wikilink")

1.