《**解體新書**》出版於1774年（[安永](../Page/安永.md "wikilink")3年），由[杉田玄白譯自德国醫學家J](../Page/杉田玄白.md "wikilink").
Kulmus所著*Anatomische
Tabellen*的荷兰语本，是[日本第一部譯自外文的](../Page/日本.md "wikilink")[人體解剖學書籍](../Page/人體解剖學.md "wikilink")。

## 歷史

日本首部人體解剖學書籍為1754年出版，由[山脇東洋所著的](../Page/山脇東洋.md "wikilink")《[藏志](../Page/藏志.md "wikilink")》。

1771年（[明和](../Page/明和_\(年號\).md "wikilink")8年）3月4日，杉田玄白、[前野良澤](../Page/前野良澤.md "wikilink")（）與[中川淳庵等蘭方醫](../Page/中川淳庵.md "wikilink")（荷蘭醫學的醫師），在[江戶](../Page/江戶.md "wikilink")[小塚原](../Page/小塚原.md "wikilink")（今[東京都](../Page/東京都.md "wikilink")[荒川區](../Page/荒川區.md "wikilink")[南千住](../Page/南千住.md "wikilink")2丁目）的[刑場見習死刑犯的遺體](../Page/刑場.md "wikilink")[解剖](../Page/解剖.md "wikilink")，對於人體構造與*Anatomische
Tabellen*書中所繪的絲毫未差而頗為震撼，因此開始著手翻譯此書。

1773年（安永2年），為求外界的反應，杉田玄白先發行了《[解體約圖](../Page/解體約圖.md "wikilink")》（）。隔年正式出版《解體新書》。

1826年（[文政](../Page/文政.md "wikilink")9年），[大槻玄澤](../Page/大槻玄澤.md "wikilink")（）修訂了原書中的錯誤及不足之處後，出版了《重訂解體新書》。

## 內容

此書有本文四卷與附圖一卷，以[漢文寫成](../Page/漢字.md "wikilink")。

  - 卷一：總論、形態、名称、要素、骨骼與關節總論、骨骼與關節個論
  - 卷二：[頭](../Page/頭.md "wikilink")、[口](../Page/口.md "wikilink")、[腦與](../Page/腦.md "wikilink")[神經](../Page/神經.md "wikilink")、[眼](../Page/眼.md "wikilink")、[耳](../Page/耳.md "wikilink")、[鼻](../Page/鼻.md "wikilink")、[舌](../Page/舌.md "wikilink")
  - 卷三：[胸](../Page/胸.md "wikilink")・[膈膜](../Page/膈膜.md "wikilink")、[肺](../Page/肺.md "wikilink")、[心臟](../Page/心臟.md "wikilink")、[動脈](../Page/動脈.md "wikilink")、[靜脈](../Page/靜脈.md "wikilink")、[門脈](../Page/門脈.md "wikilink")、[腹](../Page/腹.md "wikilink")、[腸](../Page/腸臟.md "wikilink")・[胃](../Page/胃.md "wikilink")、[腸間膜](../Page/腸間膜.md "wikilink")・[乳糜管](../Page/乳糜管.md "wikilink")、[胰臟](../Page/胰臟.md "wikilink")
  - 卷四：[脾臟](../Page/脾臟.md "wikilink")、[肝臟](../Page/肝臟.md "wikilink")・[膽囊](../Page/膽囊.md "wikilink")、[腎臟](../Page/腎臟.md "wikilink")・[膀胱](../Page/膀胱.md "wikilink")、[生殖器](../Page/生殖器.md "wikilink")、[妊娠](../Page/妊娠.md "wikilink")、[筋肉](../Page/筋肉.md "wikilink")

## 對後世的影響

《解體新書》的翻譯發行，讓以往只熟稔[漢醫的日本人認識到西洋的醫學](../Page/漢醫.md "wikilink")。而有些書中首次翻譯的人體器官的名稱，如「神經」、「軟骨」、「動脈」等，在[漢字圈內廣為使用至今](../Page/漢字圈.md "wikilink")。

而現今有些講解某事物構造及原理的書，也借用了解體新書之名。

## 外部連結既參考資料

  - [酒井靜：《解體新書的翻譯與出版》](http://www.ihp.sinica.edu.tw/~medicine/discuss/essay/sakai1.PDF)
    中央研究院歷史語言研究所

  - [解体新書](https://web.archive.org/web/20071117085931/http://www.lib.nakamura-u.ac.jp/yogaku/kaitai/head.htm)
    中村學園大學

[category:解剖學書籍](../Page/category:解剖學書籍.md "wikilink")

[Category:江戶時代典籍](../Category/江戶時代典籍.md "wikilink")
[Category:解剖學史](../Category/解剖學史.md "wikilink")
[Category:1774年書籍](../Category/1774年書籍.md "wikilink")