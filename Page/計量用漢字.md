[磅秤.JPG](https://zh.wikipedia.org/wiki/File:磅秤.JPG "fig:磅秤.JPG")\]\]
**計量用漢字**，又稱為**度量衡略字**或**縮寫**，是十九世紀初期[中國翻譯外來](../Page/中國.md "wikilink")[度量衡](../Page/度量衡.md "wikilink")[單位而創造的字](../Page/單位.md "wikilink")，通常由兩字合成，例如「兛」代表「[千克](../Page/千克.md "wikilink")」、「糎」起初代表「[厘米](../Page/厘米.md "wikilink")」，後來也有用來代表「[毫米](../Page/毫米.md "wikilink")」。初時採用了不少日本所造的漢字，但是日文用「瓦」字組成的漢字，中文改用「克」字組成。\[1\]計量用漢字特別之處是模仿[日文漢字](../Page/日文漢字.md "wikilink")，單一個字配上兩個[音節](../Page/音節.md "wikilink")，打破了中文[漢字一字一音的慣例](../Page/漢字.md "wikilink")，故又名**雙音節漢字**；现在这些旧单位用字已棄置不用。

1977年7月20日，中国[文字改革委员会和](../Page/國家語言文字工作委員會.md "wikilink")[国家计量总局聯合颁布](../Page/国家质量监督检验检疫总局#国家计量总局.md "wikilink")《[关于部份计量单位名称统一用字的通知](../Page/关于部份计量单位名称统一用字的通知.md "wikilink")》，並附上了《部分计量单位名称用字统一用字表》\[2\]。此命令规定採用一字一音一概念，並統一了計量单位名稱，並要求所有出版物均應採用。其中，淘汰的单位用字主要有：“[浬](../Page/海里.md "wikilink")”、“[哩](../Page/英里.md "wikilink")”、“[㖊、浔](../Page/英寻.md "wikilink")”、“[呎](../Page/英尺.md "wikilink")”、“[吋](../Page/英寸.md "wikilink")”、“[𠺖](../Page/英亩.md "wikilink")”、“[竔](../Page/公升.md "wikilink")”、“[嘝](../Page/蒲式耳.md "wikilink")”、“[呏、嗧](../Page/加仑.md "wikilink")”、“[𠵆](../Page/英担.md "wikilink")”、“[𠰴](../Page/英石.md "wikilink")”、“[唡](../Page/盎司.md "wikilink")”、“[喱](../Page/格令.md "wikilink")”、“[瓩](../Page/千瓦.md "wikilink")”、“[乇](../Page/托.md "wikilink")”、“[㕫](../Page/方.md "wikilink")”、“[𠳼](../Page/响度单位.md "wikilink")”、“[𠸍](../Page/梅尔刻度.md "wikilink")”、“[𥿝](../Page/紡織材料測量單位#丹尼尔.md "wikilink")”、“[糹太](../Page/紡織材料測量單位#特克斯.md "wikilink")”。

[大五碼及](../Page/大五碼.md "wikilink")[CNS
11643第一字面的](../Page/CNS_11643.md "wikilink")[符號區有九個計量用字](../Page/符號.md "wikilink")：兙兛兞兝兡兣嗧瓩糎\[3\]；[統一碼視之為普通](../Page/統一碼.md "wikilink")[漢字](../Page/漢字.md "wikilink")，收入[中日韓越統一表意文字的正字區](../Page/中日韓越統一表意文字.md "wikilink")。

## 計量用漢字

### 公制

公制有兩套單位縮寫漢字，起初主要是沿襲日本；及至1929年，中華民國政府公佈《度量衡法》，該法律沒制定過單位縮寫，但還是由此產生了新的單位縮寫，也改變了部份縮寫的用法。不過這套新的單位縮寫不如前一套易用和流行。\[4\]

| 早期用字 | 後期用字                                | 單位                                          |
| ---- | ----------------------------------- | ------------------------------------------- |
| 長度   |                                     |                                             |
| 粁    | 粴                                   | [千米](../Page/千米.md "wikilink")（公里）          |
| 粨    | 粌                                   | [百米](../Page/百米.md "wikilink")（公引）          |
| 籵    | 粀                                   | [十米](../Page/十米.md "wikilink")（公丈）          |
| 粎    | 粎                                   | [米](../Page/米.md "wikilink")（公-{}-尺）        |
| 粉    | 籿                                   | [分米](../Page/分米.md "wikilink")（公-{}-寸）      |
| 糎    | 粉                                   | [釐米](../Page/釐米.md "wikilink")（公-{}-分）      |
| 粍    | 糎                                   | [毫米](../Page/毫米.md "wikilink")（公-{}-釐）      |
| 地積   |                                     |                                             |
| 𡩛    | 𩓩                                   | [公頃](../Page/公頃.md "wikilink")              |
| 安    | 𤲾                                   | [公畝](../Page/公畝.md "wikilink")              |
| 𡪸    | 𬪿\[5\]                              | [平方米](../Page/平方米.md "wikilink")（公-{}-釐）    |
| 容積   |                                     |                                             |
| 竏    | 𥪕                                   | 千公升（公秉）                                     |
| 竡    | 䇉                                   | 百公升（公[石](../Page/石_\(容量單位\).md "wikilink")） |
| 竍    | 䇆                                   | 十公升（公斗）                                     |
| 竔    | 竔                                   | [公升](../Page/公升.md "wikilink")              |
| 竕    | 𥩻                                   | [分升](../Page/分升.md "wikilink")（公合）          |
| 竰    | 𥩘                                   | [釐升](../Page/厘升.md "wikilink")（公勺）          |
| 竓    | 𥪳                                   | [毫升](../Page/毫升.md "wikilink")（公撮）          |
| 重量   |                                     |                                             |
| |𠓎   | 千公斤（[公鐓](../Page/公噸.md "wikilink")） |                                             |
| |𠓏   | 百公斤（公[擔](../Page/擔.md "wikilink")）  |                                             |
| 𠒐    | 𠓈                                   | 十公斤（公衡）                                     |
| 兛    | 𠒙                                   | \-{[千克](../Page/千克.md "wikilink")}-（公斤）     |
| 兡    | 𠒘                                   | 百克（公[兩](../Page/兩.md "wikilink")）           |
| 兙    | 𠒲                                   | 十克（公[錢](../Page/錢_\(單位\).md "wikilink")）    |
| 克    | 兝                                   | [克](../Page/克.md "wikilink")（公-{}-分）        |
| 兝    | 兣                                   | [分克](../Page/分克.md "wikilink")（公-{}-釐）      |
| 兣    | 兞                                   | [釐克](../Page/釐克.md "wikilink")（公-{}-毫）      |
| 兞    | 𠒭                                   | [毫克](../Page/毫克.md "wikilink")（公絲）          |

## 另見

  - [合字](../Page/合字.md "wikilink")
  - [合文](../Page/合文.md "wikilink")

## 注釋

<references />

## 外部連結

  - [教育部異體字字典單位詞參考表](https://web.archive.org/web/20170116162414/http://dict2.variants.moe.edu.tw/variants/rbt/unit_term_tiles.rbt?pageId=2982189)
  - [关于部分计量单位名称统一用字的通知](https://web.archive.org/web/20160728025421/http://www.china-language.gov.cn/wenziguifan/shanghi/008.htm)

## 參考

  -
  -
  -
  -
  -
[Category:汉字](../Category/汉字.md "wikilink")
[Category:度量衡](../Category/度量衡.md "wikilink")

1.
2.
3.  <https://www.cns11643.gov.tw/AIDB/query_symbol_results.do?org.apache.catalina.filters.CSRF_NONCE=3CB04548B5D9A4AFC96FE789DC77D169>
4.  例如多年來再版多次的《國音常用字彙》附錄〈度量衡之略字〉中，一直只收錄前一套縮寫。而[科學名詞審查會在](../Page/科學名詞審查會.md "wikilink")1940年出版《理化名詞彙編》，在〈物理學名詞凡例〉中，批評當時的法定單位名稱笨拙混亂（如「公分」同時表釐米和克），且每十進就用新名稱，缺乏系統，難於記憶，因此主張採用前一套縮寫，讀法也不用法定單位而用舊有音譯（如「糎」讀作「釐米」而非「公分」）。
5.  𬪿字的字形為「亞厘」。