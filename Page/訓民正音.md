《**訓民正音**》（）是[朝鲜王朝第四代國王](../Page/朝鲜王朝.md "wikilink")[世宗大王李祹与其子第五代國王](../Page/朝鲜世宗.md "wikilink")[文宗大王李珦主导創制的](../Page/朝鲜文宗.md "wikilink")[朝鲜语文字](../Page/朝鲜语.md "wikilink")，又稱[諺文](../Page/諺文.md "wikilink")（），今多稱**[韓文](../Page/韓文.md "wikilink")**
(한문) 、**[韓字](../Page/韓字.md "wikilink")** (한글)
、**[朝鮮文](../Page/朝鮮文.md "wikilink")** (조선문)
、**[朝鮮字](../Page/朝鮮字.md "wikilink")** (조선글)
。原有28字，其中母音字母11個，子音字母17個。该书完成于朝鲜世宗25年（1443年）末或朝鲜世宗26年（1444年）初，于朝鲜世宗28年（1446年）正式出版。

朝鲜半岛古代使用[漢字表記](../Page/漢字.md "wikilink")，文言分離。只有[士大夫學習](../Page/士大夫.md "wikilink")[漢文](../Page/漢文.md "wikilink")，因此平民大多是[文盲](../Page/文盲.md "wikilink")。相傳[三国末年](../Page/三国_\(朝鲜\).md "wikilink")[薛聰藉漢字音義標註漢字](../Page/薛聰_\(新羅\).md "wikilink")，創[吏讀文字](../Page/吏讀.md "wikilink")，此外尚有[鄉札](../Page/鄉札.md "wikilink")、[口訣等表記方法](../Page/口訣_\(朝鮮語\).md "wikilink")，但這些由於使用的不便等原因未能取代漢文。[朝鮮王朝第四代國王](../Page/朝鮮王朝.md "wikilink")[世宗深感漢字對於](../Page/朝鮮世宗.md "wikilink")[文化傳播的不便](../Page/文化.md "wikilink")，迫切需要一可完整表達本國語音之文字，故特設諺文局，召[鄭麟趾](../Page/鄭麟趾.md "wikilink")、[成三問](../Page/成三問.md "wikilink")、[申叔舟等學者](../Page/申叔舟.md "wikilink")，在研究[朝鮮語音和](../Page/朝鮮語音系.md "wikilink")[漢語音韻的基礎上](../Page/漢語音韻學.md "wikilink")，以方塊字組合，一音節佔一字，創立表音文字。

## 历史

[Hunminjeongum.jpg](https://zh.wikipedia.org/wiki/File:Hunminjeongum.jpg "fig:Hunminjeongum.jpg")
西元15世纪以前，韩语只有语言没有文字，以[汉字为书写工具](../Page/汉字.md "wikilink")\[1\]。由于[韩语与](../Page/韩语.md "wikilink")[汉语是完全不同的语系](../Page/汉语.md "wikilink")，使用汉字记录韩语是一件很不容易的事，加之一般百姓不懂得[汉文](../Page/汉语.md "wikilink")，非常不利于文化的交流与发展\[2\]。为了解决韩民族书写文字的问题，1443年[朝鲜王朝](../Page/朝鲜王朝.md "wikilink")[世宗大王组织一批学者创造了适合标记韩语语音的文字体系](../Page/世宗大王.md "wikilink")——韩字。这些文字当时被称作“[训民正音](../Page/训民正音.md "wikilink")”，意为“教老百姓以正确的字音”\[3\]。1940年發現的《訓民正音解例本製字解》（發行于朝鮮世宗28年，1446年）宣稱韓字的子音與母音是根據人的口腔構造、中國古來的天地人思想以及[陰陽學說而創制出来的](../Page/陰陽學說.md "wikilink")。

书中記載如下：

不过1443年朝鲜世宗命令使用諺文的時候，受到眾多使用汉字的朝鮮文人和[两班贵族的批评](../Page/两班.md "wikilink")，因为他们認爲抛棄[漢字就等于失去](../Page/漢字.md "wikilink")[中國文明](../Page/中國.md "wikilink")，而成為[夷狄之邦](../Page/蠻夷.md "wikilink")。[崔万理曾說](../Page/崔万理.md "wikilink")：“自古[九州之内](../Page/九州_\(中国\).md "wikilink")，風土雖異，未有因方言而别爲文字者。唯[蒙古](../Page/蒙古.md "wikilink")、[西夏](../Page/西夏.md "wikilink")、[女真](../Page/女真.md "wikilink")、[日本](../Page/日本.md "wikilink")、[西蕃之類](../Page/吐蕃.md "wikilink")，各有其字，是皆夷狄事耳，无足道者。《傳》曰：『用夏變夷,
未聞變於夷者也。』[歷代中国皆以我國有](../Page/中國歷史.md "wikilink")[箕子遺風](../Page/箕子.md "wikilink")，文物禮樂，比拟中華。今别作諺文，舍中國而自同于[夷狄](../Page/蠻夷.md "wikilink")，是所謂棄蘇合之香，而取螗螂之丸也；豈非文明之大累哉？”。

由于朝鮮文人和贵族的抵制，無法成為官場上普遍使用的文字，因此政府決定也就與漢字並用，其通用範圍局限在兩班之間的私函、地方官衙或婦女。諺文直到20世纪才被广泛使用。\[4\]韩文的发明推动了韩国政治、经济、文化的发展，世宗大王也得到了后世的爱戴\[5\]。世宗大王所创造的韩字，被认为是世界上最实用卻不普遍的“注音文”之一，其科学性得到了文字研究专家的高度评价\[6\]。

## 世宗原詔

<table>
<thead>
<tr class="header">
<th><p>汉文</p></th>
<th><p>谚汉并书</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt></dt>
<dd>國之語音
</dd>
<dd>異乎中國
</dd>
<dd>與文字不相流通
</dd>
<dd>故愚民 有所欲言
</dd>
<dd>而終不得伸其情者多矣
</dd>
<dd>予爲此憫然
</dd>
<dd>新制二十八字
</dd>
<dd>欲使人人易習便於日用'耳'
</dd>
</dl></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Hunmin_Jeongeum_mixed.svg" title="fig:Hunmin_Jeongeum_mixed.svg">Hunmin_Jeongeum_mixed.svg</a></p></td>
</tr>
<tr class="even">
<td><p>谚文</p></td>
<td><p>现代韩文</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Hunmin_Jeongeum.svg" title="fig:Hunmin_Jeongeum.svg">Hunmin_Jeongeum.svg</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 註釋

## 参考文献

### 引用

### 书籍

  - [《龙飞御天歌》](../Page/龙飞御天歌.md "wikilink")（），第一首用諺文寫作的[詩歌](../Page/詩歌.md "wikilink")
  - [《月印千江之曲》](../Page/月印千江之曲.md "wikilink")（），[朝鲜世宗和其子](../Page/朝鲜世宗.md "wikilink")[朝鲜世祖](../Page/朝鲜世祖.md "wikilink")。
  - [韓國國立國語院編](../Page/韓國國立國語院.md "wikilink")；韓梅
    譯，《訓民正音》，世界圖書北京公司，2008年10月。ISBN
    978-7-5062-9537-6

{{-}}

## 外部链接

  - [新华网：朝鲜文字560年](https://web.archive.org/web/20060425091725/http://news.xinhuanet.com/banyt/2005-02/03/content_2542317.htm)
  - [韩国为什么“恢复”汉字](http://tech.icxo.com/htmlnews/2005/12/20/741579_0.htm)
  - [訓民正音諺解全文](http://project.ktug.or.kr/dvipdfmx/sample/cjk-latex/hunmin.pdf)

## 参见

  - [朝鮮語](../Page/朝鮮語.md "wikilink")、[朝鲜汉字](../Page/朝鲜汉字.md "wikilink")
  - [諺文](../Page/諺文.md "wikilink")、[古諺文](../Page/古諺文.md "wikilink")、[吏读](../Page/吏读.md "wikilink")
  - [大韩民国国宝](../Page/大韩民国国宝.md "wikilink")

[\*](../Category/训民正音.md "wikilink")
[Category:韓語教學](../Category/韓語教學.md "wikilink")
[Category:朝鲜王朝官修典籍](../Category/朝鲜王朝官修典籍.md "wikilink")
[Category:世界记忆名录](../Category/世界记忆名录.md "wikilink")
[Category:識字教學](../Category/識字教學.md "wikilink")

1.

2.

3.
4.  [韩语的语系和历史以及在世界的地位](http://www.mykrkr.com/news/view.asp?id=3613)，
    韩语韩国

5.
6.