[Compactflash.jpg](https://zh.wikipedia.org/wiki/File:Compactflash.jpg "fig:Compactflash.jpg")
CompactFlash I型卡\]\]

**存储卡**（memory card），或称**快闪存储卡**（flash memory
card）、或稱閃卡，是一种固态电子[快闪存储器](../Page/快闪存储器.md "wikilink")[数据存储设备](../Page/数据存储设备.md "wikilink")，多為卡片或者方块状。它一般是使用Flash（[快闪存储器](../Page/快闪存储器.md "wikilink")）[芯片作为储存介质](../Page/芯片.md "wikilink")。主要用于[数码相机](../Page/数码相机.md "wikilink")、[PDA和](../Page/PDA.md "wikilink")[笔记本电脑](../Page/笔记本电脑.md "wikilink")、[音乐播放器](../Page/音乐.md "wikilink")、[掌上游戏机和其他](../Page/掌上游戏机.md "wikilink")[电子设备](../Page/电子设备.md "wikilink")。它能提供可重复读写，无需外部电源的存储形式。也有非固态的存储卡。

快闪存储卡曾被视为[软盘的替代品](../Page/软盘.md "wikilink")，但是这一角色被[闪存盘所取代](../Page/闪存盘.md "wikilink")。闪存盘可以通过[USB口与任一电脑相连](../Page/USB.md "wikilink")。

存储卡的格式有很多种，通常在数码相机、掌上游戏机、[移动电话](../Page/移动电话.md "wikilink")，和其他设备上都有。[PC卡](../Page/PC卡.md "wikilink")（PCMCIA）在1990年代就成为了首批投入商业生产的存储卡格式，但是目前它主要用于工业设备或承担输入输出功能，作为设备的连接标准。同样在1990年代，其它的一些较小型的卡格式出现了，包括CF卡、[SM卡和Mini卡](../Page/SmartMedia.md "wikilink")。在移动电话、掌上游戏机等设备开始使用一些独有的[微型内嵌存储卡](../Page/微型内嵌存储卡.md "wikilink")（SID），而[PDA](../Page/PDA.md "wikilink")、数字音乐播放器等倾向于使用移动存储卡。

从1990年代后期到21世纪初期，一些新的格式出现了，例如SD/MMC、记忆棒、[xD圖像卡和其他一些衍生格式](../Page/xD圖像卡.md "wikilink")，由于移动电话、PDA、数码相机等产品的尺寸不断缩小，它们对于更小的存储卡的需求变得更强烈，这也使得原先的“小型”卡显得很大。数码相机上SM卡和CF卡曾经非常成功。2001年，单SM卡就占有50%的[市场份额](../Page/市场份额.md "wikilink")，CF在专业数码相机领域也所向披靡。但是到了2005年，SD/MMC卡已经取代了SM卡的位置，它的对手也已经不是CD卡，而是XD卡、记忆棒等格式。在工业领域，前任的王者PC卡还能维持生计；在移动电话和PDA方面，记忆卡市场已经是群雄纷争了。

目前，不少新的[个人电脑都已经内置了多功能的](../Page/个人电脑.md "wikilink")[读卡器](../Page/读卡器.md "wikilink")。

## 记忆卡格式资讯表

| 名称                                                                                   |  首字母缩写   | 外形尺寸                 |       [数字版权管理](../Page/Digital_rights_management.md "wikilink")       |
| ------------------------------------------------------------------------------------ | :------: | -------------------- | :-------------------------------------------------------------------: |
| [CompactFlash I](../Page/CompactFlash.md "wikilink")                                 |   CF-I   | 43 × 36 × 3.3 mm     |                                 不支持|                                  |
| [CompactFlash II](../Page/CompactFlash.md "wikilink")                                |  CF-II   | 43 × 36 × 5 mm       |                                 不支持|                                  |
| [SmartMedia Card](../Page/SmartMedia.md "wikilink")                                  |   SMC    | 45 × 37 × 0.76 mm    |                                 不支持|                                  |
| [Memory Stick](../Page/Memory_Stick.md "wikilink")                                   |    MS    | 50.0 × 21.5 × 2.8 mm |             [MagicGate](../Page/MagicGate.md "wikilink")              |
| [Memory Stick Duo](../Page/Memory_Stick.md "wikilink")                               |  MS Duo  | 31.0 × 20.0 × 1.6 mm |             [MagicGate](../Page/MagicGate.md "wikilink")              |
| [Memory Stick Micro M2](../Page/Memory_Stick.md "wikilink")                          |          | 15.0 × 12.5 × 1.2 mm |             [MagicGate](../Page/MagicGate.md "wikilink")              |
| [Multi Media Card](../Page/MultiMediaCard.md "wikilink")                             |   MMC    | 32 × 24 × 1.5 mm     |                                 不支持|                                  |
| [Reduced Size Multi Media Card](../Page/Reduced_Size_Multi_Media_Card.md "wikilink") |  RS-MMC  | 16 × 24 × 1.5 mm     |                                 不支持|                                  |
| [MMCmicro Card](../Page/Multi_Media_Card.md "wikilink")                              | MMCmicro | 12 × 14 × 1.1 mm     |                                 不支持|                                  |
| [Secure Digital Card](../Page/Secure_Digital_card.md "wikilink")                     |    SD    | 32 × 24 × 2.1 mm     | [CPRM](../Page/Content_Protection_for_Recordable_Media.md "wikilink") |
| [miniSD Card](../Page/MiniSD.md "wikilink")                                          |  miniSD  | 21.5 × 20 × 1.4 mm   | [CPRM](../Page/Content_Protection_for_Recordable_Media.md "wikilink") |
| [microSD Card](../Page/microSD.md "wikilink")                                        | microSD  | 11 × 15 × 1 mm       | [CPRM](../Page/Content_Protection_for_Recordable_Media.md "wikilink") |
| [xD-Picture Card](../Page/XD圖像卡.md "wikilink")                                       |    xD    | 20 × 25 × 1.7 mm     |                                 不支持|                                  |

大多数[EEPROM设备都有一定的写次数限制](../Page/EEPROM.md "wikilink")，某些卡已经采用了[耗損平衡](../Page/耗損平衡.md "wikilink")（wear
leveling）演算法，以减少经常写的位置可能出现的损耗。在記憶卡市場，售價越來越便宜，相反不同[品牌的容量則競相增加](../Page/品牌.md "wikilink")。在2016年，32GB是市面常見的容量，256GB的記憶卡已經出現。

## 記憶卡類型概覽

[Flash_memory_cards_size.jpg](https://zh.wikipedia.org/wiki/File:Flash_memory_cards_size.jpg "fig:Flash_memory_cards_size.jpg")
[PSandPS2_MemoryCard.jpg](https://zh.wikipedia.org/wiki/File:PSandPS2_MemoryCard.jpg "fig:PSandPS2_MemoryCard.jpg")
[PC-Card_Adapter26in1.jpg](https://zh.wikipedia.org/wiki/File:PC-Card_Adapter26in1.jpg "fig:PC-Card_Adapter26in1.jpg")\]\]

  - PCMCIA ATA Type I Flash Memory Card（PC Card ATA Type I）(max 8 GB (8
    [GiB](../Page/gibibyte.md "wikilink")) flash as of 2005）
      - PCMCIA [Linear Flash](../Page/Linear_Flash.md "wikilink") Cards,
        SRAM cards等。
      - PCMCIA Type II, Type III cards
  - CompactFlash Card (Type I), CompactFlash High-Speed（max 32 GB as of
    2008）
  - CompactFlash Type II, CF+(CF2.0), CF3.0
      - Microdrive（max 6 GB as of 2005）
  - MiniCard（[Miniature Card](../Page/Miniature_Card.md "wikilink")）（max
    64 MB (64 [MiB](../Page/mebibyte.md "wikilink"))）
  - SmartMedia Card (SSFDC)（max 128 MB，3.3 V,5 V）
  - xD-Picture Card, xD-Picture Card型M
  - Memory Stick, MagicGate Memory Stick (max 128 MB); Memory Stick
    Select, MagicGate Memory Stick Select（"Select" means: 2x128 MB with
    A/B switch）
  - SecureMMC
  - Secure Digital (SD Card), Secure Digital High-Speed, Secure Digital
    Plus/Xtra/etc（SD with USB connector）
      - miniSD Card
      - microSD Card（aka Transflash, T-Flash）
      - SDHC
  - MU-Flash（Mu-Card，Mu-Card Alliance of OMIA）
  - C-Flash
  - [SIM卡](../Page/SIM.md "wikilink")（Subscriber Identity Module）
  - [Smart card](../Page/Smart_card.md "wikilink")（ISO 7810 Card
    Standard , ISO 7816 Card Standard等.）
  - UFC ([USB FlashCard](../Page/USB_FlashCard.md "wikilink"))
    [1](https://web.archive.org/web/20060224062747/http://www.lexar.com/ufc/)（使用[USB](../Page/USB.md "wikilink")）
  - FISH Universal Transportable Memory Card Standard（使用USB）
  - Disk memory cards:
      - [Clik\!](../Page/Clik!.md "wikilink")（PocketZip），（40 MB
        PocketZip）
      - [Floppy
        disk](../Page/Floppy_disk.md "wikilink")（[LS120](../Page/LS120.md "wikilink"),
        2-inch, 3.5-inch,等.）
  - Intelligent Stick（iStick, a USB-based flash memory card with MMS）
  - [MicroSD與SD記憶卡.JPG](https://zh.wikipedia.org/wiki/File:MicroSD與SD記憶卡.JPG "fig:MicroSD與SD記憶卡.JPG")SxS
    (S-by-S) memory card, a new memory card specification developed by
    [Sandisk](../Page/Sandisk.md "wikilink") and
    [Sony](../Page/Sony.md "wikilink"). SxS complies to the
    [ExpressCard](../Page/ExpressCard.md "wikilink") industry standard.
    [2](http://www.dpreview.com/news/0704/07041601sxsmemorycardformat.asp)
  - Nexflash
    [Winbond](https://web.archive.org/web/20071013124716/http://winbond-usa.com/en/component/option,com_staticxt/Itemid,553/xt_item,1/staticfile,Nexflash~press_011000.htm)
    Serial Flash Moduel (SFM) cards, size range 1
    [mb](../Page/millibit?.md "wikilink"), 2 mb and 4 mb.

## 参见

  - [存储卡比较](../Page/存储卡比较.md "wikilink")
  - [閃存盤](../Page/閃存盤.md "wikilink")
  - [数码相机品牌列表](../Page/数码相机品牌列表.md "wikilink")

## 外部链接

[Category:固態電腦儲存媒體](../Category/固態電腦儲存媒體.md "wikilink")
[Category:摄影](../Category/摄影.md "wikilink")
[記憶卡](../Category/記憶卡.md "wikilink")