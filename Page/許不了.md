**許不了**（；），[本名](../Page/本名.md "wikilink")**葉鐵雄**，[台灣](../Page/台灣.md "wikilink")[新竹市人](../Page/新竹市.md "wikilink")（[出生於](../Page/出生.md "wikilink")[基隆市](../Page/基隆市.md "wikilink")[暖暖區](../Page/暖暖區.md "wikilink")），台灣知名的[喜劇演員](../Page/喜劇演員.md "wikilink")，有「台灣[卓別林](../Page/卓別林.md "wikilink")」的美譽。

## 生平

  - 1951年2月20日，許不了出生於[基隆市](../Page/基隆市.md "wikilink")[暖暖區的一個](../Page/暖暖區.md "wikilink")[貧窮](../Page/貧窮.md "wikilink")[家庭](../Page/家庭.md "wikilink")，這個家庭貧窮到經常只能以[開水代替](../Page/開水.md "wikilink")[牛奶來餵養](../Page/牛奶.md "wikilink")[嬰兒時期的許不了](../Page/嬰兒.md "wikilink")。由於無力撫養許不了，許不了的親生父母把許不了送給新竹「愛樂夢[酒家](../Page/酒家.md "wikilink")」（今[新竹牧場樓上](../Page/新竹牧場.md "wikilink")）領班兼[魔術師](../Page/魔術師.md "wikilink")[葉清江](../Page/葉清江.md "wikilink")。許不了的養母[徐明珠](../Page/徐明珠.md "wikilink")，本為愛樂夢酒家的[歌女](../Page/歌女.md "wikilink")，婚後成為葉清江的魔術助理。許不了的本名「葉鐵雄」就是葉清江取的。

<!-- end list -->

  - 葉清江創辦了[特技團](../Page/特技.md "wikilink")「華僑歌舞魔術特技團」，團員30人，以[滑稽](../Page/滑稽.md "wikilink")[魔術絕活賣藝](../Page/魔術.md "wikilink")。該團曾經代表台灣到[日本參加](../Page/日本.md "wikilink")[國際魔術大會](../Page/國際魔術大會.md "wikilink")。在葉清江的教導下，[童年時期的許不了學會](../Page/童年.md "wikilink")[吞火](../Page/吞火.md "wikilink")、[雜耍](../Page/雜耍.md "wikilink")、[口技](../Page/口技.md "wikilink")、[魔術](../Page/魔術.md "wikilink")、[吹奏樂器](../Page/吹奏樂器.md "wikilink")、[爵士鼓等本事](../Page/爵士鼓.md "wikilink")。許不了6歲時，已經是葉清江的助手，頗受葉清江表演風格的影響。

<!-- end list -->

  - 許不了沒有什麼童年時光，只在[新竹市東區東門國民小學唸了一](../Page/新竹市東區東門國民小學.md "wikilink")[學期](../Page/學期.md "wikilink")，就開始[逃家](../Page/逃家.md "wikilink")、[逃學](../Page/逃學.md "wikilink")，後來在「王中強特技團」學習魔術、雜耍、口技等各項絕活。許不了12歲時，開始學習[舞台上的表演功夫](../Page/舞台.md "wikilink")，起初是練習[撲克牌魔術](../Page/撲克牌.md "wikilink")，後來是練習吞[乒乓球](../Page/乒乓球.md "wikilink")（把乒乓球塞到[喉嚨裡](../Page/喉嚨.md "wikilink")，再把乒乓球吐出來）；其功力之高，即使他喉嚨裡塞著乒乓球時，仍然可以喝[水](../Page/水.md "wikilink")。其後，許不了與父親搭檔，巡迴台灣各地表演。由於許不了的師父的[藝名為](../Page/藝名.md "wikilink")「大[伯樂](../Page/伯樂.md "wikilink")」，所以當時許不了的藝名也順理成章叫做「小伯樂」。許不了滿20歲前，徐明珠在-{[埔里](../Page/埔里鎮.md "wikilink")}-[自殺身亡](../Page/自殺.md "wikilink")，葉清江當時已[再婚](../Page/再婚.md "wikilink")。

<!-- end list -->

  - 許不了20歲時，在[桃園縣一家](../Page/桃園市.md "wikilink")[歌廳表演](../Page/歌廳.md "wikilink")，遇到他的滑稽魔術的愛好者[蘇美燕](../Page/蘇美燕.md "wikilink")，兩人進而[結婚](../Page/結婚.md "wikilink")，蘇美燕成為許不了的魔術助手。夫妻兩人隨著葉清江的特技團，遠赴日本、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞等地表演](../Page/馬來西亞.md "wikilink")。1973年，許不了服[兵役](../Page/兵役.md "wikilink")，被分發到[衛武營的](../Page/衛武營.md "wikilink")[陸軍八軍團](../Page/陸軍八軍團.md "wikilink")-{干城}-[藝工隊](../Page/藝工隊.md "wikilink")，蘇美燕被聘為-{干城}-藝工隊的僱員。1975年，許不了退伍，夫妻兩人繼續到處賣藝。

<!-- end list -->

  - 1977年初，從日本表演回來的許不了到[南沙群島](../Page/南沙群島.md "wikilink")[勞軍](../Page/勞軍.md "wikilink")，忍不住在他搭乘的[中華民國海軍](../Page/中華民國海軍.md "wikilink")[軍艦上展現他逗人發笑的才藝](../Page/軍艦.md "wikilink")，受到同艦的[中國電視公司](../Page/中國電視公司.md "wikilink")[新聞部經理](../Page/中視新聞.md "wikilink")[王曉祥的賞識](../Page/王曉祥.md "wikilink")。王曉祥寫了一封[推薦信](../Page/推薦信.md "wikilink")，告訴許不了，可以拿著它，去找中視節目部副理[王世綱安排演出機會](../Page/王世綱.md "wikilink")；但許不了沒有主動去找王世綱。同年，台灣知名歌星[鳳飛飛在](../Page/鳳飛飛.md "wikilink")[藍寶石大歌廳](../Page/藍寶石大歌廳.md "wikilink")[作秀](../Page/作秀.md "wikilink")，當時鳳飛飛的專屬[舞群](../Page/舞群.md "wikilink")「四騎士」一位成員因故不能上場，改由許不了上場。前去藍寶石大歌廳給鳳飛飛探班（當時鳳飛飛是中視基本[演藝人員](../Page/演藝人員.md "wikilink")）的王世綱，在觀眾席上看到了許不了、[方正與鳳飛飛的表演](../Page/方正_\(演員\).md "wikilink")；加上同年許不了在「[中華民國各界](../Page/中華民國.md "wikilink")[金門](../Page/金門縣.md "wikilink")[前線勞軍團](../Page/前線.md "wikilink")」勞軍表演時表現突出，王世綱遂主動邀請許不了與方正進入中視。許不了與方正相繼與中視簽訂基本演藝人員[合約](../Page/合約.md "wikilink")，成為中視基本演藝人員；兩人進入中視的日期相距三個月。

<!-- end list -->

  - 與中視簽約後的許不了，最初接連在中視幾部[電視劇中](../Page/電視劇.md "wikilink")[跑龍套](../Page/跑龍套.md "wikilink")。之後，許不了、方正、[徐風](../Page/徐風.md "wikilink")、[倪敏然與](../Page/倪敏然.md "wikilink")[凌峰並為鳳飛飛主持的中視](../Page/凌峰.md "wikilink")[綜藝節目](../Page/綜藝節目.md "wikilink")《[你愛周末](../Page/你愛周末.md "wikilink")》的固定班底，許不了與鳳飛飛對演的魔術單元〈笑說魔術〉是該節目的一大特色。在1977年中視午間[閩南語連續劇](../Page/閩南語.md "wikilink")《[雷峰塔](../Page/雷峰塔.md "wikilink")》（1977年4月18日至1977年5月16日播映，男主角[許仙由中視演員](../Page/許仙.md "wikilink")[李滔飾演](../Page/李滔.md "wikilink")，女主角[白素貞由](../Page/白素貞.md "wikilink")[陳佩伶飾演](../Page/陳佩伶.md "wikilink")）裏，許不了扮演許仙的[書僮](../Page/書僮.md "wikilink")「許不了」（[台語諧音](../Page/台語.md "wikilink")「苦不完」）而成名，故改藝名為「許不了」。1978年7月9日，鳳飛飛主持的中視綜藝節目《[一道彩虹](../Page/一道彩虹.md "wikilink")》開播，許不了與方正擔任該節目固定班底兼助理主持人，三人經常在該節目中聯手搞笑。閒暇之餘，許不了會抽時間去拜訪[育幼院](../Page/育幼院.md "wikilink")，與院童一起玩，變魔術給院童看，並捐款給院方。

<!-- end list -->

  - 1979年，許不了自組勞軍團赴金門前線勞軍，自費新台幣70多萬元打點團內其他藝人的[機票費](../Page/機票.md "wikilink")、通告費等；同年，許不了因參演[中國電影製片廠](../Page/中國電影製片廠.md "wikilink")[軍教片](../Page/軍教片.md "wikilink")《成功嶺上》而無法上《一道彩虹》的[通告](../Page/通告.md "wikilink")，被中視認定[違約而遭罰](../Page/違約.md "wikilink")「停演[電視一年](../Page/電視.md "wikilink")」。許不了在《成功嶺上》中的優異表現，讓他一炮而紅；從此，許不了便以主角身份參演電影，電影邀約總是接連不斷。1980年，許不了參演[朱延平首次執導的電影](../Page/朱延平.md "wikilink")《[小丑](../Page/小丑_\(台灣電影\).md "wikilink")》轟動大街小巷，造成一股「許不了旋風」，片酬飆漲至新台幣兩百六十萬元；同年，許不了與方正合演電影《小魚吃大魚》，兩人成為好友。

<!-- end list -->

  - 1982年[第19屆金馬獎](../Page/第19屆金馬獎.md "wikilink")，許不了與[泰迪羅賓一同頒獎](../Page/泰迪羅賓.md "wikilink")。

<!-- end list -->

  - 由於許不了本身所俱有的[草根性及早年累積的](../Page/草根.md "wikilink")[喜劇表演基礎](../Page/喜劇.md "wikilink")，搭配所營造的[悲喜劇的表演風格](../Page/悲喜劇.md "wikilink")，讓他在演藝工作獲得極大的成就。後來，許不了與朱延平合作的一系列電影，更得到觀眾們的認同，甚至連許不了掛名客串演出的影片都很賣座，如同成為「[票房保證](../Page/票房.md "wikilink")」一般。

<!-- end list -->

  - 但是「許不了旋風」成形後，許不了成了[黑道眼中的](../Page/黑道.md "wikilink")[搖錢樹](../Page/搖錢樹.md "wikilink")（在[秀場上](../Page/秀場.md "wikilink")，許不了只要單獨雜耍與口技就可以支撐票房，不像鳳飛飛還要搭配舞群與[樂隊](../Page/樂隊.md "wikilink")），成了他晚年悲慘生活的根源。在早期台灣演藝圈與黑道的糾結不清下，再加上許不了成名後累積的[人情壓力](../Page/人情.md "wikilink")，許不了被[槍械與人情債押著](../Page/槍械.md "wikilink")，不斷四處奔波、軋戲，日夜顛倒，[免疫力下降](../Page/免疫力.md "wikilink")，許多病痛隨之而來。為了不斷趕通告，許不了飲[酒](../Page/酒.md "wikilink")、注射[止痛藥來迅速提振精神與紓解病痛](../Page/止痛藥.md "wikilink")，因前者而罹患[酒精性肝炎](../Page/酒精性肝炎.md "wikilink")，因後者而染上[B型肝炎與](../Page/B型肝炎.md "wikilink")[毒癮](../Page/毒癮.md "wikilink")。另外，許不了欠下大額[賭債](../Page/賭債.md "wikilink")。蘇美燕不堪這種生活，而與許不了[離婚](../Page/離婚.md "wikilink")。1984年1月，許不了參演中視連續劇《[降龍羅漢](../Page/降龍羅漢_\(1984年電視劇\).md "wikilink")》時，王世綱帶許不了去[醫院作](../Page/醫院.md "wikilink")[健康檢查](../Page/健康檢查.md "wikilink")，證實許不了已經因飲酒過量而罹患[肝硬化](../Page/肝硬化.md "wikilink")。之後，許不了陸續併發[十二指腸潰瘍](../Page/十二指腸潰瘍.md "wikilink")、[胰臟炎](../Page/胰臟炎.md "wikilink")、[黃疸](../Page/黃疸.md "wikilink")、[紅血球過多等疾病](../Page/紅血球.md "wikilink")。

<!-- end list -->

  - 依據朱延平的描述，許不了的晚年生活是這樣的：許不了在[電影製片廠時](../Page/電影製片廠.md "wikilink")，經常會逗大家開心，是個很快樂的工作夥伴；但是他返家以後，經常獨自喝酒解悶直到天亮，喝醉了就放聲大哭。他的朋友們勸他[住院接受完整治療](../Page/住院.md "wikilink")，他卻經常在住院期間趁機逃出醫院，因為他不喜歡住院，他認為住院的生活就像[坐牢](../Page/坐牢.md "wikilink")。他不信任[西醫](../Page/西醫.md "wikilink")，只相信他成名前結交的朋友們給他服用的[中醫藥方](../Page/中醫.md "wikilink")；他服用那些藥方之後，健康狀況有些好轉，但是他又開始飲酒。這些情況反覆下來，導致他的病情惡化。

<!-- end list -->

  - 許不了參演他與朱延平合作的最後一部電影《小-{丑}-與天鵝》時，在正式開拍一場在[路邊攤喝酒的戲之前](../Page/路邊攤.md "wikilink")，他喝醉，抱著朱延平大哭，說自己活不久了，懇求朱延平代為照顧他的兩歲大的小女兒。1985年7月2日深夜，許不了[嘔吐不已](../Page/嘔吐.md "wikilink")，被送進[馬偕紀念醫院台北院區](../Page/馬偕紀念醫院.md "wikilink")。最後，1985年7月3日，《小丑與天鵝》上映前三天，許不了因酒精性肝炎導致[心臟衰竭而病逝](../Page/心臟衰竭.md "wikilink")，僅得年34歲。許不了的命運，正如台語諧音「苦不完」的藝名一樣；朱延平形容，他的一生是一場喜劇、也是一場悲劇、更是一場鬧劇，他就是現實生活中天生的小丑，把歡樂帶給別人，把眼淚流給自己。

<!-- end list -->

  - 1979年至1985年間，許不了共參演64部電影，部份[台灣電影史](../Page/台灣電影史.md "wikilink")[學者稱此六年之間為](../Page/學者.md "wikilink")「許不了時代」。許不了參演的最後一部電影是[群龍電影於](../Page/群龍電影.md "wikilink")1985年6月在台北市[中正國宅開拍的](../Page/中正國宅.md "wikilink")《寶貝家庭》。許不了病逝之後，群龍電影找[替身演員接替他的遺缺](../Page/替身演員.md "wikilink")，拍完《寶貝家庭》。1985年7月27日，《寶貝家庭》上映日，群龍電影舉辦許不了[公祭](../Page/公祭.md "wikilink")。公祭結束後，許不了被安葬在[臺北縣](../Page/新北市.md "wikilink")[三芝鄉](../Page/三芝區.md "wikilink")。

<!-- end list -->

  - 許不了病逝之後，導演[吳念真沉痛地出面呼籲](../Page/吳念真.md "wikilink")，別再剝削藝人了。

<!-- end list -->

  - 導演[吳宇森曾在受訪時讚揚許不了是](../Page/吳宇森.md "wikilink")「台灣[卓別林](../Page/卓別林.md "wikilink")」。

## 家庭

許不了曾與三位女性有婚約，育有兩男兩女。

  - 周桂英：初戀女友

:\*
[周明增](../Page/周明增.md "wikilink")：往演藝圈發展。神似許不了的臉龐與諧趣風格出道，演出不少知名喜劇。但是曾因酒後駕車及持有毒品而風波不斷。

  - [蘇美燕](../Page/蘇美燕.md "wikilink")：第一任妻子

:\* [葉明樂](../Page/葉明樂.md "wikilink")：往傳播業發展。

:\* [葉嘉凌](../Page/葉嘉凌.md "wikilink")

  - [陳秀霞](../Page/陳秀霞.md "wikilink")：第二任妻子

:\* [葉蓉庭](../Page/葉蓉庭.md "wikilink")：往演藝圈發展。

## 演藝作品

### 電視

#### [綜藝節目](../Page/綜藝節目.md "wikilink")

| **年份**                | **頻道**                                 | **節目名**                                          |
| --------------------- | -------------------------------------- | ------------------------------------------------ |
| 1977年                 | [中國電視公司](../Page/中國電視公司.md "wikilink") | 《你愛周末》魔術單元〈笑說魔術〉                                 |
| 1977年                 | 中國電視公司                                 | 《一道彩虹》固定班底兼助理主持人                                 |
| 1983年2月12日21:30～00:00 | 中國電視公司                                 | 《傻丁傻福》（許不了、方正[電視專輯](../Page/電視專輯.md "wikilink")） |
| 1985年                 | 中國電視公司                                 | 《一個小丑的故事：許不了回顧專輯》（梁昆傑製作，方笛旁白）                    |
|                       |                                        |                                                  |

#### [連續劇](../Page/連續劇.md "wikilink")

| **年份** | **頻道** | **劇名** | '''飾演 |
| ------ | ------ | ------ | ----- |
| 1977年  | 中國電視公司 | 《雷峰塔》  | 許不了   |
| 1977年  | 中國電視公司 | 《岳父大人》 | 甘樂    |
| 1984年  | 中國電視公司 | 《降龍羅漢》 | 濟公    |
|        |        |        |       |

### 電影

#### 導演

| **年份** | **片名**  |
| ------ | ------- |
| 1981年  | 《雞蛋石頭碰》 |
|        |         |

#### 演員

| **年份**                                         | **片名**                                 | **飾演** |
| ---------------------------------------------- | -------------------------------------- | ------ |
| 1979年                                          | 《[樓上樓下](../Page/樓上樓下.md "wikilink")》   |        |
| 《[功夫大拍賣](../Page/功夫大拍賣.md "wikilink")》         |                                        |        |
| 《[傳奇人物廖添丁](../Page/傳奇人物廖添丁.md "wikilink")》     |                                        |        |
| 《[成功嶺上](../Page/成功嶺上.md "wikilink")》           |                                        |        |
| 《[三十七計](../Page/三十七計.md "wikilink")》           |                                        |        |
| 《[祝老三笑譚](../Page/祝老三笑譚.md "wikilink")》         |                                        |        |
| 《[丐幫傳奇](../Page/丐幫傳奇.md "wikilink")》           |                                        |        |
| 《[醉魚 醉蝦 醉螃蟹](../Page/醉魚_醉蝦_醉螃蟹.md "wikilink")》 |                                        |        |
| 《[要命的小方](../Page/要命的小方.md "wikilink")》         |                                        |        |
| 《[怪拳 怪招 怪師傅](../Page/怪拳_怪招_怪師傅.md "wikilink")》 |                                        |        |
| 1980年                                          | 《[天才蠢材](../Page/天才蠢材.md "wikilink")》   |        |
| 《[真假大亨](../Page/真假大亨.md "wikilink")》           |                                        |        |
| 《[小丑對紅唇](../Page/小丑對紅唇.md "wikilink")》         |                                        |        |
| 《[古寧頭大戰](../Page/古寧頭大戰.md "wikilink")》         |                                        |        |
| 《[小魚吃大魚](../Page/小魚吃大魚.md "wikilink")》         |                                        |        |
| 《[舞拳](../Page/舞拳.md "wikilink")》               |                                        |        |
| 《[大人物](../Page/大人物.md "wikilink")》             |                                        |        |
| 《[套房出租](../Page/套房出租.md "wikilink")》           |                                        |        |
| 《[小丑](../Page/小丑_\(電影\).md "wikilink")》        |                                        |        |
| 1981年                                          | 《[瘋狂大發財](../Page/瘋狂大發財.md "wikilink")》 |        |
| 《[雞蛋石頭碰](../Page/雞蛋石頭碰.md "wikilink")》         |                                        |        |
| 《[大小姐與流浪漢](../Page/大小姐與流浪漢.md "wikilink")》     |                                        |        |
| 《[掃蕩大賭場](../Page/掃蕩大賭場.md "wikilink")》         |                                        |        |
| 《[東追西趕跑跳碰](../Page/東追西趕跑跳碰.md "wikilink")》     |                                        |        |
| 《[瘋狂世界](../Page/瘋狂世界.md "wikilink")》           |                                        |        |
| 《[傻兵立大功](../Page/傻兵立大功.md "wikilink")》         |                                        |        |
| 《[美人國](../Page/美人國.md "wikilink")》             |                                        |        |
| 《[四個臭皮匠](../Page/四個臭皮匠.md "wikilink")》         |                                        |        |
| 1982年                                          | 《[新濟公活佛](../Page/新濟公活佛.md "wikilink")》 |        |
| 《[新西遊記](../Page/新西遊記.md "wikilink")》           |                                        |        |
| 《[黑獄大逃亡](../Page/黑獄大逃亡.md "wikilink")》         |                                        |        |
| 《[神劍動山河](../Page/神劍動山河.md "wikilink")》         |                                        |        |
| 《[糊塗妙賊立大功](../Page/糊塗妙賊立大功.md "wikilink")》     |                                        |        |
| 《[浪子 名花 金花黨](../Page/浪子_名花_金花黨.md "wikilink")》 |                                        |        |
| 《[傻搭檔](../Page/傻搭檔.md "wikilink")》             |                                        |        |
| 《[行船人的愛](../Page/行船人的愛.md "wikilink")》         |                                        |        |
| 《[紅粉遊俠](../Page/紅粉遊俠.md "wikilink")》           |                                        |        |
| 《[紅粉兵團](../Page/紅粉兵團.md "wikilink")》           |                                        |        |
| 《[酒色財氣](../Page/酒色財氣.md "wikilink")》           |                                        |        |
| 《[傻丁有傻福](../Page/傻丁有傻福.md "wikilink")》         |                                        |        |
| 《[開心一級棒](../Page/開心一級棒.md "wikilink")》         |                                        |        |
| 1983年                                          | 《[四傻害羞](../Page/四傻害羞.md "wikilink")》   |        |
| 《[情報販子](../Page/情報販子.md "wikilink")》           |                                        |        |
| 《[進攻要塞地](../Page/進攻要塞地.md "wikilink")》         |                                        |        |
| 《[迷你特攻隊](../Page/迷你特攻隊.md "wikilink")》         |                                        |        |
| 《[寶貝尋寶記](../Page/寶貝尋寶記.md "wikilink")》         |                                        |        |
| 《[火龍任務A計劃](../Page/火龍任務A計劃.md "wikilink")》     |                                        |        |
| 《[聰明學生笨老師](../Page/聰明學生笨老師.md "wikilink")》     |                                        |        |
| 《[女學生與機關槍](../Page/女學生與機關槍.md "wikilink")》     |                                        |        |
| 《[大集合](../Page/大集合.md "wikilink")》             |                                        |        |
| 《[一九八三大驚奇](../Page/一九八三大驚奇.md "wikilink")》     |                                        |        |
| 《[王哥柳哥](../Page/王哥柳哥.md "wikilink")》           |                                        |        |
| 1984年                                          | 《[七隻狐狸](../Page/七隻狐狸.md "wikilink")》   |        |
| 《[男人真命苦](../Page/男人真命苦.md "wikilink")》         |                                        |        |
| 《[天生一對](../Page/天生一對_\(台灣電影\).md "wikilink")》  |                                        |        |
| 《[獵艷高手](../Page/獵艷高手.md "wikilink")》           |                                        |        |
| 1985年                                          | 《[打鬼救夫](../Page/打鬼救夫.md "wikilink")》   |        |
| 《[快樂寶貝](../Page/快樂寶貝.md "wikilink")》           |                                        |        |
| 《[少爺兵](../Page/少爺兵.md "wikilink")》             |                                        |        |
| 《[八番坑口的新娘](../Page/八番坑口的新娘.md "wikilink")》     |                                        |        |
| 《[醜小鴨](../Page/醜小鴨.md "wikilink")》             |                                        |        |
| 《[小丑與天鵝](../Page/小丑與天鵝.md "wikilink")》         |                                        |        |
| 《[寶貝家庭](../Page/寶貝家庭.md "wikilink")》           |                                        |        |
|                                                |                                        |        |

### 唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>日期</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>出版公司</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1981年</p></td>
<td style="text-align: left;"><p>滾石歡樂英雄系列第二輯《方正、許不了：歡樂大兵專輯》</p></td>
<td style="text-align: left;"><p>台灣：<a href="../Page/滾石有聲出版社.md" title="wikilink">滾石有聲出版社</a></p></td>
<td style="text-align: left;"><ol>
<li>大兵歌</li>
<li>向前衝</li>
<li>上班去</li>
<li>姑娘一朵花</li>
<li>向前衝演奏曲</li>
<li>打靶練槍法</li>
<li>癡癡的等</li>
<li>飛呀！飛呀！</li>
<li>白羊黑羊過小橋</li>
<li>大兵歌演奏曲</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

## 資料來源

  - \-{梁昆}-傑製作，[方笛](../Page/方笛.md "wikilink")[旁白](../Page/旁白.md "wikilink")，中國電視公司[特別節目](../Page/特別節目.md "wikilink")《一個小-{丑}-的故事：許不了-{回}-顧專輯》，[中視無線台](../Page/中視無線台.md "wikilink")1985年首播（日期不詳），[中視綜藝台](../Page/中視綜藝台.md "wikilink")2007年12月27日重播。
  - 葉龍彥，[〈許不了小傳（1951－1985）〉](https://web.archive.org/web/20080418063504/http://media.hcccb.gov.tw/manazine/2002-04-23/magazine1-15.htm)，《竹塹文獻雜誌》，2002年4月23日。
  - [〈台灣的卓別林—【許不了】〉](https://web.archive.org/web/20070927030308/http://media.hcccb.gov.tw/otherweb/9405shi/index.htm)，[新竹市政府文化局](../Page/新竹市政府文化局.md "wikilink")。
  - [黑道餵毒控制\!
    許不了的悲歌](http://news.cts.com.tw/cts/society/201109/201109160824758.html)

## 外部連結

  -
  -
[X許](../Category/台灣喜劇演員.md "wikilink")
[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:臺灣電影男演員](../Category/臺灣電影男演員.md "wikilink")
[X許](../Category/台灣綜藝界人物.md "wikilink")
[X許](../Category/基隆市人.md "wikilink")
[Category:葉姓](../Category/葉姓.md "wikilink")
[Category:台灣魔術師](../Category/台灣魔術師.md "wikilink")