**陳子鈞**，本姓**許**，原名**許仁-{杰}-**，目前以原本名**許仁杰**作為藝名在演藝圈發展，是一位出身於臺灣[高雄的](../Page/高雄.md "wikilink")[歌手與](../Page/歌手.md "wikilink")[演員](../Page/演員.md "wikilink")。

## 家族

陳子鈞的母親為[美濃客家人](../Page/美濃.md "wikilink")。
許仁杰是其原本名後因故改名為現名陳子鈞

## 職業生涯

2007年，陳子鈞以原本名許仁-{杰}-參加[中國電視公司歌唱比賽節目](../Page/中國電視公司.md "wikilink")《[超級星光大道](../Page/超級星光大道.md "wikilink")》，在「老歌指定賽」中以一曲〈天上人間〉受到矚目，比賽中演唱之〈我還能愛誰〉、〈少年〉、〈聽你聽我〉、〈Forever
Love〉、〈專屬天使〉、〈天使忌妒的生活〉皆獲好評，最後獲得比賽第七名的佳績。

2007年7月，簽入[華研國際音樂旗下](../Page/華研國際音樂.md "wikilink")，同年10月於第二張合輯[《愛星光精選‧昨天今天明天》中](../Page/愛星光精選-昨天今天明天.md "wikilink")，推出個人首支單曲《不安靜的夜》。媒體稱他和同樣簽入華研的[林宥嘉](../Page/林宥嘉.md "wikilink")、[周定緯](../Page/周定緯.md "wikilink")、[潘裕文為](../Page/潘裕文.md "wikilink")「星光四少」或「華研四公子」。

自比賽結束後，許仁-{杰}-除了發行音樂作品，在各類型的音樂場合演出之外，還因優質形象收主持節目《[客家安可](../Page/客家安可.md "wikilink")》，陸續接下許多優質戲劇《[花樹下的約定](../Page/花樹下的約定.md "wikilink")》、《[情義月光](../Page/情義月光.md "wikilink")》、《[谷風少年](../Page/谷風少年.md "wikilink")》和《[歸·娘家](../Page/歸·娘家.md "wikilink")》等演出工作。

## 《超級星光大道》比賽曲目

### 第一屆《超級星光大道》比賽成績

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>首播日期</p></th>
<th><p>主題</p></th>
<th><p>演唱歌曲</p></th>
<th><p>原唱者</p></th>
<th><p>分數</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>02</p></td>
<td><p>2007/01/19</p></td>
<td><p>百萬巨星殊死戰(下)</p></td>
<td><p>愛到底</p></td>
<td><p><a href="../Page/庾澄慶.md" title="wikilink">庾澄慶</a></p></td>
<td><p>15分</p></td>
<td><p>過關</p></td>
</tr>
<tr class="even">
<td><p>04</p></td>
<td><p>2007/02/02</p></td>
<td><p>入選資格賽 50人取20(下)<br />
（片段剪接，未完整播出）</p></td>
<td><p>Kiss Goodbye</p></td>
<td><p><a href="../Page/王力宏.md" title="wikilink">王力宏</a></p></td>
<td><p>未滿16分</p></td>
<td><p>失敗，<strong>但評審保障名額被救回</strong>。</p></td>
</tr>
<tr class="odd">
<td><p>08</p></td>
<td><p>2007/03/02</p></td>
<td><p>適者生存 一對一PK賽(上)</p></td>
<td><p>男人的錯</p></td>
<td><p><a href="../Page/陳奕迅.md" title="wikilink">陳奕迅</a></p></td>
<td><p>18分</p></td>
<td><p>勝<a href="../Page/鄭宇辰.md" title="wikilink">鄭宇辰</a>（10分），過關</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2007/03/16</p></td>
<td><p>再見風華 精選老歌指定賽</p></td>
<td><p>天上人間</p></td>
<td><p><a href="../Page/李麗華.md" title="wikilink">李麗華</a></p></td>
<td><p>20分</p></td>
<td><p>過關</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>2007/03/30</p></td>
<td><p>同舟共濟 16強合唱指定賽</p></td>
<td><p>明明很愛你</p></td>
<td><p><a href="../Page/品冠.md" title="wikilink">品冠</a>&amp;<a href="../Page/梁靜茹.md" title="wikilink">梁靜茹</a></p></td>
<td><p>14分</p></td>
<td><p>與<a href="../Page/陳亭慧.md" title="wikilink">陳亭慧合唱</a>，失敗</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>2007/04/06</p></td>
<td><p>突破重圍 15強淘汰賽</p></td>
<td><p>Forever Love</p></td>
<td><p><a href="../Page/王力宏.md" title="wikilink">王力宏</a></p></td>
<td><p>12分</p></td>
<td><p>過關</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>2007/04/13</p></td>
<td><p>向偶像致敬 大震撼！臨場狀況考驗淘汰賽</p></td>
<td><p>凌晨三點鐘</p></td>
<td><p><a href="../Page/張智成.md" title="wikilink">張智成</a></p></td>
<td><p>16分</p></td>
<td><p>過關</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>2007/04/20</p></td>
<td><p>優勝劣敗 1對1PK淘汰賽</p></td>
<td><p>不如這樣</p></td>
<td><p><a href="../Page/陳奕迅.md" title="wikilink">陳奕迅</a></p></td>
<td><p>17分</p></td>
<td><p>勝<a href="../Page/安伯政.md" title="wikilink">安伯政</a>（14分），過關</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>2007/04/27</p></td>
<td><p>挑戰·試煉 藝人合唱指定賽</p></td>
<td><p>對的人</p></td>
<td><p><a href="../Page/戴愛玲.md" title="wikilink">戴愛玲</a></p></td>
<td><p>15分</p></td>
<td><p>與<a href="../Page/戴愛玲.md" title="wikilink">戴愛玲合唱</a>，失敗</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>2007/05/04</p></td>
<td><p>機會·命運 踢館挑戰賽Part Ⅰ</p></td>
<td><p>我還能愛誰</p></td>
<td><p><a href="../Page/許志安.md" title="wikilink">許志安</a></p></td>
<td><p>20分</p></td>
<td><p>勝<a href="../Page/張珮瑩.md" title="wikilink">張珮瑩</a>（12分），過關</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p>2007/05/11</p></td>
<td><p>隱藏的危機 踢館挑戰賽Part Ⅱ</p></td>
<td><p>聽你聽我</p></td>
<td><p><a href="../Page/張惠妹.md" title="wikilink">張惠妹</a></p></td>
<td><p>16分</p></td>
<td><p>敗<a href="../Page/蕭敬騰.md" title="wikilink">蕭敬騰</a>（19分），失敗</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p>2007/05/18</p></td>
<td><p>心魔 踢館挑戰賽Part Ⅲ</p></td>
<td><p>專屬天使</p></td>
<td><p><a href="../Page/TANK.md" title="wikilink">TANK</a></p></td>
<td><p>19分</p></td>
<td><p>勝<a href="../Page/吳孟桓.md" title="wikilink">吳孟桓</a>（14分），過關</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>2007/05/25</p></td>
<td><p>他山之石可以攻錯 終極版指名踢館賽</p></td>
<td><p>天使忌妒的生活</p></td>
<td><p><a href="../Page/曹格.md" title="wikilink">曹格</a></p></td>
<td><p>17分</p></td>
<td><p>勝<a href="../Page/候筱威.md" title="wikilink">候筱威</a>（13分），過關</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>2007/06/01</p></td>
<td><p>險峰 不插電演唱淘汰賽</p></td>
<td><p>安靜</p></td>
<td><p><a href="../Page/周杰倫.md" title="wikilink">周杰倫</a></p></td>
<td><p>14分</p></td>
<td><p>失敗，淘汰，<strong>確定為第七名</strong>。</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>2007/06/15</p></td>
<td><p>老戰友 默契合唱考驗賽</p></td>
<td><p>少年</p></td>
<td><p><a href="../Page/曹格.md" title="wikilink">曹格</a>&amp;<a href="../Page/光良.md" title="wikilink">光良</a></p></td>
<td><p>21分</p></td>
<td><p>與<a href="../Page/盧學叡.md" title="wikilink">盧學叡合唱</a>，<strong>而許仁-{杰}-獲選為下一集與藝人合唱機會</strong>。</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>2007/06/22</p></td>
<td><p>星光依舊燦爛 偶像對唱抗壓戰</p></td>
<td><p>重返寂寞</p></td>
<td><p><a href="../Page/張智成.md" title="wikilink">張智成</a></p></td>
<td><p>18分</p></td>
<td><p>與<a href="../Page/張智成.md" title="wikilink">張智成合唱</a></p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>2007/07/13</p></td>
<td><p>今夜星光多美好 星光頒獎典禮</p></td>
<td><p>名字</p></td>
<td><p><a href="../Page/張智成.md" title="wikilink">張智成</a></p></td>
<td><p>表演</p></td>
<td><p>獲得最佳美聲獎</p></td>
</tr>
<tr class="even">
<td><p>因為我相信</p></td>
<td><p><a href="../Page/星光幫.md" title="wikilink">星光幫</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 星光學長姐合唱成績

| 屆數  | 首播日期       | 主題             | 演唱歌曲 | 原唱者                                                               | 分數     | 備註                                  |
| --- | ---------- | -------------- | ---- | ----------------------------------------------------------------- | ------ | ----------------------------------- |
| 星光二 | 2007/11/16 | 星光同學會 星光合唱決定賽  | 說走就走 | [陶喆](../Page/陶喆.md "wikilink")                                    | 17分    | 與[吳忠明合唱](../Page/吳忠明.md "wikilink") |
| 星光四 | 2008/11/07 | 饗宴續篇 學長姐合唱賽（下） | 掌心   | [無印良品](../Page/無印良品.md "wikilink")                                | 20分    | 與[何書宇合唱](../Page/何書宇.md "wikilink") |
| 星光五 | 2009/07/24 | 發光 學長姐合唱賽      | 復刻回憶 | [方大同](../Page/方大同.md "wikilink")&[薛凱琪](../Page/薛凱琪.md "wikilink") | 17382票 | 與[劉明湘合唱](../Page/劉明湘.md "wikilink") |
|     |            |                |      |                                                                   |        |                                     |

### 《超級星光大道》特別節目比賽成績

<table>
<thead>
<tr class="header">
<th><p>節目</p></th>
<th><p>首播日期</p></th>
<th><p>主題</p></th>
<th><p>演唱歌曲</p></th>
<th><p>原唱者</p></th>
<th><p>分數</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>特別節目</p></td>
<td><p>2008/02/06</p></td>
<td><p>星光閃耀賀新春 除夕特別節目</p></td>
<td><p>超能力魔術秀+魔術急智歌<br />
(曲為蝸牛與黃鸝鳥歌詞改編)</p></td>
<td><p><a href="../Page/銀霞.md" title="wikilink">銀霞</a></p></td>
<td><p>18分</p></td>
<td><p>與<a href="../Page/潘裕文.md" title="wikilink">潘裕文合作</a>，勝<a href="../Page/曾沛慈.md" title="wikilink">曾沛慈</a>+<a href="../Page/方志友.md" title="wikilink">方-{志}-友魔術表演</a>（16分）<br />
抽抽樂抽到台幣500元。</p></td>
</tr>
<tr class="even">
<td><p>城裡的月光</p></td>
<td><p><a href="../Page/TANK.md" title="wikilink">TANK</a></p></td>
<td><p>21分</p></td>
<td><p>勝<a href="../Page/紀文蕙.md" title="wikilink">紀文蕙</a>（17分）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>特別節目</p></td>
<td><p>2009/10/23</p></td>
<td><p>超級星光大道 唱旺星光總動員<br />
中視開台40週年特別節目</p></td>
<td><p>感恩的心</p></td>
<td><p><a href="../Page/歐陽菲菲.md" title="wikilink">歐陽菲菲</a></p></td>
<td><p>20分</p></td>
<td><p>勝<a href="../Page/方宥心.md" title="wikilink">方宥心</a>（18分）</p></td>
</tr>
<tr class="even">
<td><p>相思比夢長</p></td>
<td><p><a href="../Page/費玉清.md" title="wikilink">費玉清</a></p></td>
<td><p>20分</p></td>
<td><p>與<a href="../Page/徐詠琳.md" title="wikilink">徐詠琳</a>﹑<a href="../Page/茵茵.md" title="wikilink">茵茵代表白隊合作</a>，最後與紅隊同樣得到20分。<br />
【歌中劇單元】一代皇后大玉兒之清宮稗史</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 《超級星光大道 星光傳奇賽》比賽成績

| 集數                                | 首播日期       | 主題                               | 演唱歌曲    | 原唱者                                | 分數  | 備註                                                                                                            |
| --------------------------------- | ---------- | -------------------------------- | ------- | ---------------------------------- | --- | ------------------------------------------------------------------------------------------------------------- |
| 01                                | 2010/05/21 | 重返榮耀 24強晉級資格賽                    | 安靜      | [周杰倫](../Page/周杰倫.md "wikilink")   | 21分 | 晉級                                                                                                            |
| 03                                | 2010/06/04 | 史無前例 跨屆一對一PK                     | 閃閃惹人愛   | [蕭亞軒](../Page/蕭亞軒.md "wikilink")   | 17分 | 敗[葉瑋庭](../Page/葉瑋庭.md "wikilink")（24分），失敗                                                                     |
| 04                                | 2010/06/11 | 夢幻對決 終極分組對抗賽【默契合唱】               | 檸檬草的味道  | [蔡依林](../Page/蔡依林.md "wikilink")   | 4票  | 與[康禎庭合唱](../Page/康禎庭.md "wikilink")，勝[吳忠明](../Page/吳忠明.md "wikilink")+[徐詠琳](../Page/徐詠琳.md "wikilink")（1票），晉級 |
| 05                                | 2010/06/18 | 撼動人心 地獄悲歌挑戰（上）【天堂High歌】          | 愛一天多一天  | [陳曉東](../Page/陳曉東.md "wikilink")   | 20分 | 晉級                                                                                                            |
| 撼動人心 地獄悲歌挑戰（上）【地獄悲歌】              | 一次幸福的機會    | [劉若英](../Page/劉若英.md "wikilink") |         |                                    |     |                                                                                                               |
| 07                                | 2010/07/02 | 黃金陣容 星光音樂饗宴                      | 忠孝東路走九遍 | [動力火車](../Page/動力火車.md "wikilink") | 不評分 | 與[尤秋興合唱](../Page/尤秋興.md "wikilink")，晉級                                                                        |
| 08                                | 2010/07/09 | 唱出百分百自己 殘酷考驗 老歌新曲殊死戰【第一輪】        | 我的眼睛在下雨 | [李茂山](../Page/李茂山.md "wikilink")   | 22分 | 晉級【第二輪】                                                                                                       |
| 唱出百分百自己 殘酷考驗 老歌新曲殊死戰【第二輪】         | 背對背擁抱      | [林俊傑](../Page/林俊傑.md "wikilink") | 23分     | 晉級                                 |     |                                                                                                               |
| **因無法兼顧比賽與主持工作，故作出退賽決定，名次為前11強。** |            |                                  |         |                                    |     |                                                                                                               |
|                                   |            |                                  |         |                                    |     |                                                                                                               |

## 音樂作品

### 合輯

  - [華研唱片](../Page/華研國際音樂股份有限公司.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>發行日期</p></th>
<th><p>合輯名稱</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2007-06-29</p></td>
<td><p><a href="../Page/星光同學會-超級星光大道10強紀念合輯.md" title="wikilink">星光同學會</a><br />
超級星光大道10強紀念合輯</p></td>
<td><p>《Forever Love》(原唱:<a href="../Page/王力宏.md" title="wikilink">王力宏</a>)</p></td>
</tr>
<tr class="even">
<td><p>2007-10-05</p></td>
<td><p><a href="../Page/愛星光精選-昨天今天明天.md" title="wikilink">愛星光精選</a><br />
昨天今天明天</p></td>
<td><p>《不安靜的夜》<br />
《天使忌妒的生活》(原唱:<a href="../Page/曹格.md" title="wikilink">曹格</a>)</p></td>
</tr>
</tbody>
</table>

  - [喜瑪拉雅](../Page/喜瑪拉雅音樂股份有限公司.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>發行日期</p></th>
<th><p>合輯名稱</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2011-12-22</p></td>
<td><p><a href="../Page/大愛人間-心光燦爛.md" title="wikilink">大愛人間-心光燦爛</a><br />
大愛人間-心光燦爛</p></td>
<td><p>《愛你，像你疼我》(大愛劇場--<a href="../Page/陪你看天星.md" title="wikilink">陪你看天星片尾曲</a>)</p></td>
</tr>
</tbody>
</table>

### EP

<table>
<thead>
<tr class="header">
<th><p>發行日期</p></th>
<th><p>單曲名稱</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2008-11-07</p></td>
<td><p>夢見<br />
-{許仁杰}-首張個人EP</p></td>
<td><p>《夢見》 《花樹下的約定》</p>
<p>《忘了說愛你》</p>
<p>《有甚麼》</p></td>
</tr>
</tbody>
</table>

### 線上數位發行

<table>
<thead>
<tr class="header">
<th><p>發行日期</p></th>
<th><p>單曲名稱</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2010-11-04</p></td>
<td><p>整晚的音樂會精華live數位專輯</p></td>
<td><p>《人在雨中》</p>
<p>《等我回來》</p>
<p>《還是會寂寞》</p>
<p>《秘密》</p>
<p>《夢見》</p>
<p>《沒有空》</p>
<p>《引擎》</p>
<p>《一次幸福的機會》</p>
<p>《Stand By Me》</p>
<p>《花樹下的約定》</p></td>
</tr>
</tbody>
</table>

### 單曲

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>語言</p></th>
<th><p>歌名</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>英語</p></td>
<td><p>《The biggest little thing》</p></td>
<td><ul>
<li></li>
</ul></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>國語</p></td>
<td><p>《給你們》</p></td>
<td><ul>
<li></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>國語</p></td>
<td><p>《如果這裡忽然下雪》</p></td>
<td><ul>
<li></li>
</ul></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p>國語</p></td>
<td><p>《殘夢》</p></td>
<td><ul>
<li></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p>國語</p></td>
<td><p>《學會幸福》</p></td>
<td><ul>
<li>大愛電視劇《<a href="../Page/月娘_(2015年大愛電視劇).md" title="wikilink">月娘</a>》片尾曲</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>國語</p></td>
<td><p>《不要叫醒我》</p></td>
<td><ul>
<li>與<a href="../Page/陳嘉唯.md" title="wikilink">陳嘉唯合唱</a>，收錄於陳嘉唯《Bad Bad Girl》EP中</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p>客語</p></td>
<td><p>《這是我說的》</p></td>
<td><ul>
<li>客家電視偶像劇《<a href="../Page/谷風少年.md" title="wikilink">谷風少年</a>》片尾曲</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p>國語</p></td>
<td><p>《單身剛好》</p></td>
<td><ul>
<li></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p>國語</p></td>
<td><p>《家的味道》</p></td>
<td><ul>
<li>大愛電視劇《<a href="../Page/稻香家味.md" title="wikilink">稻香家味</a>》片尾曲</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p>國語</p></td>
<td><p>《別讓愛情沈睡了》</p></td>
<td><ul>
<li>優酷網路劇《<a href="../Page/鮮肉老師.md" title="wikilink">鮮肉老師</a>》插曲</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p>國語</p></td>
<td><p>《You Should Try》</p></td>
<td><ul>
<li>與<a href="../Page/周定緯.md" title="wikilink">周定緯合唱</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p>國語</p></td>
<td><p>《入戲太深》</p></td>
<td><ul>
<li>KKTV網路劇《<a href="../Page/瑪麗蘇遇上傑克蘇.md" title="wikilink">瑪麗蘇遇上傑克蘇</a>》插曲</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p>國語</p></td>
<td><p>《我們都輸了》</p></td>
<td><ul>
<li>KKTV網路劇《<a href="../Page/第一次.md" title="wikilink">第一次</a>》插曲，與<a href="../Page/陳嘉唯.md" title="wikilink">陳嘉唯合唱</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td><p>國語</p></td>
<td><p>《只想傻給你》</p></td>
<td><ul>
<li><a href="../Page/愛奇藝台灣站.md" title="wikilink">愛奇藝台灣站網路劇</a>《<a href="../Page/如果愛，重來.md" title="wikilink">如果愛，重來</a>》插曲，與<a href="../Page/黃柏文.md" title="wikilink">黃柏文合唱</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 個人創作

  - 填詞及作曲

<!-- end list -->

  - 《花樹下的約定》
    ([客家話版及](../Page/客家話.md "wikilink")[國語版](../Page/國語.md "wikilink"))-[客家電視台](../Page/客家電視台.md "wikilink")《花樹下的約定》主題曲
  - 《The Biggest Little Thing》，於2011年「Wanna
    Be展翅飛翔生日音樂會」發表。但早在2010年行腳節目《客家安可》第22集中就已曝光。
  - 《給你們》
  - 《殘夢》
  - 《這是我說的》-[客家電視台](../Page/客家電視台.md "wikilink")《谷風少年》片尾曲

<!-- end list -->

  - 作曲

<!-- end list -->

  - 《沒有空》，陳信延作詞。發表於於2010年「許仁杰整晚的音樂會」。
  - 《引擎》，陳信延作詞。發表於2010年「許仁杰整晚的音樂會」。
  - 《阿嬤的香味》（[台語歌](../Page/台語歌.md "wikilink")），武雄作詞，2011年擔任「陳小霞Composer演唱會II」嘉賓時發表。
  - 《如果這裡忽然下雪》，陳信延作詞。於2011年「Wanna Be展翅飛翔生日音樂會」發表。
  - 《了斷》，楊培安、陳天佑作詞。發表於於2016年
  - 《單身剛好》，張簡君偉作詞。發表於於2016年
  - 《錯到底》，李焯雄作詞。發表於於2017年

### MV

  - 星光同學會‧超級星光大道10強紀念合輯：《因為我相信》
  - 愛星光精選‧昨天今天明天：《總在我身旁》、《不安靜的夜》
  - 首張個人單曲‧夢見：《夢見》、《花樹下的約定》
  - 《單身剛好》
  - 《微甜的回憶》

## 廣告、活動代言

  - 2007年 《7-11》迪士尼星光大道（&星光幫成員）
  - 2007年 警政署運動會 活動代言
  - 2007年 遠雄建設馬賽克 公益大使
  - 2008年 NIKE831路跑 活動代言
  - 2008年 兒童福利聯盟 泰迪熊義賣 活動代言
  - 2008年 客家電視台MV創作大賽 活動代言
  - 2009年 《麥當勞》咖哩香雞翅（&潘裕文、周定緯）
  - 2010年 第三屆東陽盃 我最愛現 交通大使
  - 2011年 全民減碳綠生活 活動代言
  - 2012年 童心蓮心關懷弱勢族群慈善園遊會慈善天使代言人
  - 2013年 童心蓮心關懷弱勢族群慈善演唱會代言人
  - 2016年 樂活熊城市嘉年華 親善大使

## 主持作品

  - [客家電視台](../Page/客家電視台.md "wikilink")《客家安可》（播映期間：2010年4月12日-10月4日，每週一晚上21:00\~22:00）：外景行腳節目，尋訪台灣各地的客家庄探索客家文化及美食，是許仁杰第一次擔任主持的節目。
  - [客家電視台](../Page/客家電視台.md "wikilink")《果子頭家找A咖》

## 劇場作品

### 舞台劇

<table>
<thead>
<tr class="header">
<th><p>首演</p></th>
<th><p>劇團</p></th>
<th><p>劇名</p></th>
<th><p>飾演</p></th>
<th><p>性質</p></th>
<th><p>合作演員</p></th>
<th><p>地點</p></th>
<th><p>場次日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2012年7月20日</p></td>
<td><p>臺灣戲劇表演家劇團</p></td>
<td><p>時代舞台劇<br />
《<a href="../Page/天若光.md" title="wikilink">天若光</a>》</p></td>
<td><p>老鼠</p></td>
<td><p>男主角</p></td>
<td><p><a href="../Page/盧學叡.md" title="wikilink">盧學叡</a>、<a href="../Page/安程希.md" title="wikilink">安程希</a>、<a href="../Page/周詠軒.md" title="wikilink">周詠軒</a>、<a href="../Page/楊可凡.md" title="wikilink">楊可凡</a>、<a href="../Page/梁家銘.md" title="wikilink">梁家銘</a>、<a href="../Page/邵翔.md" title="wikilink">邵翔</a></p></td>
<td><p><a href="../Page/高雄市文化中心.md" title="wikilink">高雄文化中心至德堂</a></p></td>
<td><p>2012年7月20日~7月22日</p></td>
</tr>
<tr class="even">
<td><p>2018年4月21日</p></td>
<td></td>
<td><p>《<a href="../Page/台北男女.md" title="wikilink">台北男女</a>》</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/樊光耀.md" title="wikilink">樊光耀</a>、<a href="../Page/呂曼茵.md" title="wikilink">呂曼茵</a>、<a href="../Page/朱德剛.md" title="wikilink">朱德剛</a>、<a href="../Page/楊琪.md" title="wikilink">楊琪</a>、<a href="../Page/邱逸峰.md" title="wikilink">邱逸峰</a>、<a href="../Page/葉育德.md" title="wikilink">葉育德</a>、<a href="../Page/李芙蓉.md" title="wikilink">李芙蓉</a></p></td>
<td><p><a href="../Page/臺中市中山堂.md" title="wikilink">臺中市中山堂</a><br />
<a href="../Page/新竹市文化局演藝廳音樂廳.md" title="wikilink">新竹市文化局演藝廳音樂廳</a><br />
<a href="../Page/中壢藝術館音樂廳.md" title="wikilink">中壢藝術館音樂廳</a><br />
<a href="../Page/臺南文化中心演藝廳.md" title="wikilink">臺南文化中心演藝廳</a></p></td>
<td><p>2018年4月21日、4月28日<br />
2018年5月12日、5月19日</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 音樂劇

<table>
<thead>
<tr class="header">
<th><p>首演</p></th>
<th><p>劇團</p></th>
<th><p>劇名</p></th>
<th><p>飾演</p></th>
<th><p>性質</p></th>
<th><p>合作演員</p></th>
<th><p>地點</p></th>
<th><p>場次日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2017年12月22日</p></td>
<td><p>Be劇團</p></td>
<td><p>房間小戲9<br />
《<a href="../Page/婚姻倒頭栽.md" title="wikilink">婚姻倒頭栽</a>》</p></td>
<td><p>艾才</p></td>
<td><p>男主角</p></td>
<td><p><a href="../Page/鮑奕安.md" title="wikilink">鮑奕安</a>、<a href="../Page/鄭慕岑.md" title="wikilink">鄭慕岑</a>、<a href="../Page/顏辰歡.md" title="wikilink">顏辰歡</a></p></td>
<td><p><a href="../Page/臺北.md" title="wikilink">臺北</a><a href="../Page/長安122青年旅館.md" title="wikilink">長安122青年旅館</a></p></td>
<td><p>2017年12月22日~12月24日</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 戲劇作品

|             |                                      |                                                    |     |      |                                                                                                                                                                                                            |    |
| ----------- | ------------------------------------ | -------------------------------------------------- | --- | ---- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -- |
| 首播          | 播出頻道                                 | 劇名                                                 | 角色  | 性質   | 合作演員                                                                                                                                                                                                       | 備註 |
| 2008年7月21日  | [客家電視台](../Page/客家電視台.md "wikilink") | 《[花樹下的約定](../Page/花樹下的約定.md "wikilink")》           | 沈大智 | 男主角  | [黃玉榮](../Page/黃玉榮.md "wikilink")、[陳靖婕](../Page/陳靖婕.md "wikilink")、[亞里](../Page/亞里.md "wikilink")、[謝瓊煖](../Page/謝瓊煖.md "wikilink")                                                                          |    |
| 2010年5月23日  | [大愛電視台](../Page/大愛電視台.md "wikilink") | 《[情義月光](../Page/情義月光.md "wikilink")》               | 沈順從 | 男主角  | [簡沛恩](../Page/簡沛恩.md "wikilink")、[林鴻翔](../Page/林鴻翔.md "wikilink")、[黃婕菲](../Page/黃婕菲.md "wikilink")、[趙美齡](../Page/趙美齡.md "wikilink")、[林鴻翔](../Page/林鴻翔.md "wikilink")                                       |    |
| 2011年9月17日  | [客家電視台](../Page/客家電視台.md "wikilink") | 《[阿戇妹](../Page/阿戇妹.md "wikilink")》                 | 阿昌  | 男主角  | [黃采儀](../Page/黃采儀.md "wikilink")、[王琄](../Page/王琄.md "wikilink")、[廖苡喬](../Page/廖苡喬.md "wikilink")、[黃健瑋](../Page/黃健瑋.md "wikilink")                                                                          |    |
| 2012年3月3日   | [客家電視台](../Page/客家電視台.md "wikilink") | 《[神仙谷事件](../Page/神仙谷事件.md "wikilink")》             | 許誌剛 | 男主角  | [張書偉](../Page/張書偉.md "wikilink")、[高靖榕](../Page/高靖榕.md "wikilink")、[鍾瑤](../Page/鍾瑤.md "wikilink")、[朱陸豪](../Page/朱陸豪.md "wikilink")、[陸明君](../Page/陸明君.md "wikilink")                                         |    |
| 2013年5月11日  | [客家電視台](../Page/客家電視台.md "wikilink") | 南風。六堆-單元劇《[檳榔蔭下](../Page/檳榔蔭下.md "wikilink")》      | 溫立超 | 男主角  | [徐麗雯](../Page/徐麗雯.md "wikilink")、[張敏瑛](../Page/張敏瑛.md "wikilink")、[黃梅惠](../Page/黃梅惠.md "wikilink")、[涂舒涵](../Page/涂舒涵.md "wikilink")                                                                        |    |
| 2013年5月25日  | [客家電視台](../Page/客家電視台.md "wikilink") | 南風。六堆-單元劇《[竹田車站](../Page/竹田車站.md "wikilink")》      | 李哲楷 | 男主角  | [王文澐](../Page/王文澐.md "wikilink")、[張世](../Page/張世.md "wikilink")、[張詩盈](../Page/張詩盈.md "wikilink")、[蔡頤榛](../Page/蔡頤榛.md "wikilink")                                                                          |    |
| 2015年6月21日  | [大愛電視台](../Page/大愛電視台.md "wikilink") | 《[月娘](../Page/月娘_\(2015年大愛電視劇\).md "wikilink")》    | 鄭添吉 | 男主角  | [戴君竹](../Page/戴君竹.md "wikilink")、[許家榮](../Page/許家榮.md "wikilink")、[梅嫦芬](../Page/梅嫦芬.md "wikilink")、[陳德烈](../Page/陳德烈.md "wikilink")、[張維錫](../Page/張維錫.md "wikilink")                                       |    |
| 2016年3月4日   | [三立台灣台](../Page/三立台灣台.md "wikilink") | 《[紫色大稻埕](../Page/紫色大稻埕.md "wikilink")》             | 顏水龍 | 客串   | [施易男](../Page/施易男.md "wikilink")、[江國賓](../Page/江國賓.md "wikilink")、[曾子益](../Page/曾子益.md "wikilink")、[吳鈴山](../Page/吳鈴山.md "wikilink")、[張世賢](../Page/張世賢.md "wikilink")                                       |    |
| 2016年3月28日  | [客家電視台](../Page/客家電視台.md "wikilink") | 《[谷風少年](../Page/谷風少年.md "wikilink")》               | 方原  | 男配角  | [林雨宣](../Page/林雨宣.md "wikilink")、[吳政迪](../Page/吳政迪.md "wikilink")、[游君萍](../Page/游君萍.md "wikilink")、[徐麗雯](../Page/徐麗雯.md "wikilink")、[劉爾金](../Page/劉爾金.md "wikilink")                                       |    |
| 2016年3月29日  | [大愛電視台](../Page/大愛電視台.md "wikilink") | 《[歸·娘家](../Page/歸·娘家.md "wikilink")》               | 鍾明雄 | 男主角  | [楊小黎](../Page/楊小黎.md "wikilink")、[陳瑋薇](../Page/陳瑋薇.md "wikilink")、[歐陽倫](../Page/歐陽倫.md "wikilink")、[尹昭德](../Page/尹昭德.md "wikilink")、[耿慧珠](../Page/耿慧珠.md "wikilink")                                       |    |
| 2016年11月23日 | [台視](../Page/台視.md "wikilink")       | 《[700歲旅程](../Page/700歲旅程.md "wikilink")》           | 陳必先 | 客串   | [楊小黎](../Page/楊小黎.md "wikilink")、[張書偉](../Page/張書偉.md "wikilink")                                                                                                                                          |    |
| 2017年1月4日   | [大愛電視台](../Page/大愛電視台.md "wikilink") | 《[稻香家味](../Page/稻香家味.md "wikilink")》               | 林德燿 | 男配角  | [唐治平](../Page/唐治平.md "wikilink")、[藍葦華](../Page/藍葦華.md "wikilink")、[何依霈](../Page/何依霈.md "wikilink")、[張詩盈](../Page/張詩盈.md "wikilink")、[汪建民](../Page/汪建民.md "wikilink")、[阿嬌](../Page/阿嬌.md "wikilink")        |    |
| 2017年10月15日 | [公視](../Page/公視.md "wikilink")       | 《-{[乒乓](../Page/乒乓_\(電視電影\).md "wikilink")}-》      | 毒男  | 男配角  | [吳慷仁](../Page/吳慷仁.md "wikilink")、[李依瑾](../Page/李依瑾.md "wikilink")、[藍葦華](../Page/藍葦華.md "wikilink")、[周宜霈](../Page/周宜霈.md "wikilink")                                                                        |    |
| 2019年3月21日  | [大愛電視台](../Page/大愛電視台.md "wikilink") | 《[最美的相遇](../Page/最美的相遇.md "wikilink")》             | 彭瑞松 | |男配角 | [楊鎮](../Page/楊鎮_\(藝人\).md "wikilink")、[李佳豫](../Page/李佳豫.md "wikilink")、[謝其文](../Page/謝其文.md "wikilink")、[蘇意菁](../Page/蘇意菁.md "wikilink")、[薛提縈](../Page/薛提縈.md "wikilink")、[林筳諭](../Page/林筳諭.md "wikilink") |    |
| 2019年3月27日  | [中視](../Page/中視.md "wikilink")       | 《[鑑識英雄II 正義之戰](../Page/鑑識英雄II_正義之戰.md "wikilink")》 | 吳東霖 | 男配角  | [蔡淑臻](../Page/蔡淑臻.md "wikilink")、[王識賢](../Page/王識賢.md "wikilink")                                                                                                                                          |    |
| (待播出)       | [大愛電視台](../Page/大愛電視台.md "wikilink") | 《[愛的路上我和你](../Page/愛的路上我和你.md "wikilink")》         |     |      | [王中平](../Page/王中平.md "wikilink")、[蔡函岑](../Page/蔡函岑.md "wikilink")                                                                                                                                          |    |
|             |                                      |                                                    |     |      |                                                                                                                                                                                                            |    |

## 電影作品

### 電影

|            |                                                |     |      |                                  |                                                                                                                                                                                                                                                                          |                                                 |
| ---------- | ---------------------------------------------- | --- | ---- | -------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------- |
| 首映         | 片名                                             | 角色  | 性質   | 導演                               | 合作演員                                                                                                                                                                                                                                                                     | 重要記事                                            |
| 2011年9月2日  | 《[美好冤家](../Page/美好冤家.md "wikilink")》           | 陳小馬 | 聯合主演 | [韓耀光](../Page/韓耀光.md "wikilink") | [夏雨](../Page/夏雨_\(香港\).md "wikilink")、[洪愛玲](../Page/洪愛玲.md "wikilink")、[王欣](../Page/王欣.md "wikilink")、[齊騛](../Page/齊騛.md "wikilink")、[賴力豪](../Page/賴力豪.md "wikilink")、[陳建彬](../Page/陳建彬.md "wikilink")、[鄭良雄](../Page/鄭良雄.md "wikilink")、[王惠珊](../Page/王惠珊.md "wikilink") | 本片為[許仁-{杰}-首部正式參演電影](../Page/許仁杰.md "wikilink") |
| 2016年1月8日  | 《[他媽²的藏寶圖](../Page/他媽²的藏寶圖.md "wikilink")》     | 阿偉  | 聯合主演 | [黃竑寯](../Page/黃竑寯.md "wikilink") | [任容萱](../Page/任容萱.md "wikilink")、[阿喜](../Page/林育品.md "wikilink")、[郭葦昀](../Page/郭葦昀.md "wikilink")、[王宇婕](../Page/王宇婕.md "wikilink")、[唐振剛](../Page/唐振剛.md "wikilink")、[郭耀仁](../Page/郭耀仁.md "wikilink")、[韓琳](../Page/韓琳.md "wikilink")、[茵芙](../Page/茵芙.md "wikilink")       |                                                 |
| 2017年6月10日 | 《[青春熱之惡魔軟妹幫](../Page/青春熱之惡魔軟妹幫.md "wikilink")》 |     | 男配角  | [彭宇](../Page/彭宇.md "wikilink")   |                                                                                                                                                                                                                                                                          |                                                 |
| 2018年7月29日 | 《[我的刁蠻女明星](../Page/我的刁蠻女明星.md "wikilink")》     |     | 男配角  | [簡學彬](../Page/簡學彬.md "wikilink") | [王樂妍](../Page/王樂妍.md "wikilink")、[潘柏希](../Page/潘柏希.md "wikilink")、[陳幼芳](../Page/陳幼芳.md "wikilink")、[陳文山](../Page/陳文山.md "wikilink")、[陳彥壯](../Page/陳彥壯.md "wikilink")、[方語昕](../Page/方語昕.md "wikilink")、[范少勳](../Page/范少勳.md "wikilink")                                   |                                                 |
|            |                                                |     |      |                                  |                                                                                                                                                                                                                                                                          |                                                 |

### 微電影

|            |                                          |    |     |                                          |                                                                   |                           |
| ---------- | ---------------------------------------- | -- | --- | ---------------------------------------- | ----------------------------------------------------------------- | ------------------------- |
| 首映         | 片名                                       | 角色 | 性質  | 導演                                       | 合作演員                                                              | 重要記事                      |
| 2017年4月14日 | 《[我們都有秘密](../Page/我們都有秘密.md "wikilink")》 | 志榮 | 男主角 | [Vivi Su](../Page/Vivi_Su.md "wikilink") | [陳淑芳](../Page/陳淑芳.md "wikilink")、[周嘉麗](../Page/周嘉麗.md "wikilink") | 出品：大寶科技股份有限公司、監製：永吉影業有限公司 |
|            |                                          |    |     |                                          |                                                                   |                           |

## 配音作品

  - 2011年4月8日--動畫電影《[里約大冒險](../Page/里約大冒險.md "wikilink")》，為主角雄的藍金剛鸚鵡「阿藍」配音。
  - 2014年4月3日--動畫電影《[里約大冒險2](../Page/里約大冒險2.md "wikilink")》，為主角雄的藍金剛鸚鵡「阿藍」配音。

## 演唱會

### 售票演唱會

  - 2010年8月27日：在公館The
    wall舉行首場個人售票音樂會，演唱二十五首歌曲，大秀才藝和語言天分，演唱客語歌"花樹下的約定"、粵語歌"歲月如歌"、台語歌"落雨聲"，還自己彈電子琴和電吉他演唱"閃閃惹人愛"和"戀愛ing",並發表二首由許仁杰作曲、陳信延寫詞的全新創作歌曲，嘻哈R\&B風格的"沒有空"，和中快板的"引擎"。

<!-- end list -->

  - 2011年1月26日：在公館河岸留言舉辦「維多利亞的秘密」不插電小型音樂會。演唱十二首歌，包括<薇多莉亞的秘密><惡作劇><任性><紅豆><都是夜歸人><心動>等經典女歌手作品。不但全新編曲，更請來新生代知名吉他手Jerry
    C、鍵盤手蕭恆嘉和B-Boxer Tony擔任伴奏。

<!-- end list -->

  - 2011年3月5日：與潘裕文在華山Legacy共同舉辦「P.S.音樂會」。個人獨唱12首歌（包括<Closer><Just The Way You Are><我們都寂寞><三人遊><代表作>等），兩人合唱5首。

<!-- end list -->

  - 2011年8月28日：在公館The Wall舉辦「Wanna
    Be展翅飛翔音樂會」，是與華研解約後第一場個人音樂會。演唱18首歌，包括自創曲<The Biggest Little Thing><如果這裡忽然下雪>和<阿嬤的香味>。

<!-- end list -->

  - 2015年9月12日：在河岸留言西門店舉辦「許仁杰【樂囚】音樂會」。

<!-- end list -->

  - 2016年7月30日：與林宥嘉、周定緯、潘裕文、閻奕格和倪安東在馬來西亞雲頂雲星劇場共同舉辦「星光傳奇演唱會」。

<!-- end list -->

  - 2017年8月18日：與周定緯在台北三創生活園區5樓-Clapper Studio舉辦「許仁杰 x 周定緯 You Should Try
    It 音樂會」。

<!-- end list -->

  - 2017年11月18日：在台南Hummingbird Nest 蜂鳥巢「舉辦【許仁杰 - 取暖o輕巡迴】小型演唱會」。

<!-- end list -->

  - 2017年11月25日：在台中Forro Cafe'「舉辦【許仁杰 - 取暖o輕巡迴】小型演唱會」。

<!-- end list -->

  - 2017年12月2日：在桃園ThERE Cafe'「舉辦【許仁杰 - 取暖o輕巡迴】小型演唱會」。

<!-- end list -->

  - 2017年12月9日：在高雄In Our Time「舉辦【許仁杰 - 取暖o輕巡迴】小型演唱會」。

<!-- end list -->

  - 2018年1月7日：在宜蘭賣捌所「舉辦【許仁杰 - 取暖o輕巡迴】小型演唱會」。

<!-- end list -->

  - 2018年1月13日：在台北後台BackStage Cafe'「舉辦【許仁杰 - 取暖o輕巡迴】小型演唱會」。

### 非售票演唱會與其它活動

  - 2010年12月18日：台茂開幕Super Star Live Band（vs.潘裕文）。
  - 2011年12月31日：2012桃園縣中壢市超級壢High跨年晚會。
  - 2012年8月18日：台茂百貨七夕情歌特別夜。
  - 2012年9月2日：在公館河岸留言舉辦「有仁陪你過杰」生日Party。
  - 2014年3月22日：高雄市福誠高中「扶輪有愛、啟動你和我: 希望 公益演唱會」。
  - 2014年3月29日：金門縣國立金門大學「金馬到來－好韻旺來」歌唱決賽藝人兼評審。
  - 2014年10月19日：台中TIGER CITY購物中心戶外廣場「2014潮客樂」演唱會。
  - 2016年12月17日：台北信義誠品一樓戶外廣場 「2016聖誕音樂會」

## 獎項紀錄

  - 2007年：[第一屆《超級星光大道》](../Page/超級星光大道_\(第一屆\).md "wikilink")：第七名
  - 2008年：2008年[HITO流行音樂獎頒獎典禮](../Page/HITO流行音樂獎頒獎典禮.md "wikilink")-Hito
    聲猛新人獎(星光幫)
  - 2008年：2008年[HITO流行音樂獎頒獎典禮](../Page/HITO流行音樂獎頒獎典禮.md "wikilink")-最受歡迎新人獎(星光幫)
  - 2008年：2008年度[新城國語力頒獎禮](../Page/新城國語力頒獎禮.md "wikilink")—新城國語新勢力組合(星光幫)
  - 2008年：2008年度[新城國語力頒獎禮](../Page/新城國語力頒獎禮.md "wikilink")—新城國語力人氣偶像大獎(星光幫)
  - 2010年：《[超級星光大道 星光傳奇賽](../Page/超級星光大道：星光傳奇賽.md "wikilink")》：前11強

## 外部連結

  -
[H許](../Category/客家裔臺灣人.md "wikilink")
[H許](../Category/星光幫.md "wikilink")
[X](../Category/陳姓.md "wikilink")
[Category:高雄市人](../Category/高雄市人.md "wikilink")