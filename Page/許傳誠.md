**許傳誠**（），原名**許傳啟**，[台灣](../Page/台灣.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")，右投右打，守備位置是[捕手](../Page/捕手.md "wikilink")。他於2004年12月22日在[中華職棒新人選秀會中被](../Page/中華職棒.md "wikilink")[La
New熊隊以第一順位第三指名選入](../Page/La_New熊.md "wikilink")\[1\]。
2007年10月15日因為在非戰力名單中，遭[La
New熊隊釋出](../Page/La_New熊.md "wikilink")\[2\]\[3\]。

## 棒球球隊經歷

  - [臺北縣光華國小少棒隊](../Page/新北市.md "wikilink")
  - [臺北縣丹鳳國中青少棒隊](../Page/新北市.md "wikilink")
  - [臺北縣](../Page/新北市.md "wikilink")[中華中學青棒隊](../Page/中華中學.md "wikilink")（榮工）
  - 那魯灣榮工棒球隊
  - 國立體院棒球隊（新寶）
  - [中華職棒](../Page/中華職棒.md "wikilink")[La
    New熊隊](../Page/La_New熊.md "wikilink")（2004年～2007年）

## 參考文獻

## 外部連結

  -
  - [許傳誠](https://web.archive.org/web/20071110055107/http://twbaseball.info/player/?func=FindPlayer&PlayerID=1233)於[台灣棒球資訊網](http://twbaseball.info/)的個人檔案

[H](../Category/台灣棒球選手.md "wikilink")
[H](../Category/Lamigo桃猿隊球員.md "wikilink")
[H](../Category/國立體育大學校友.md "wikilink")
[H](../Category/新北市人.md "wikilink")
[H](../Category/許姓.md "wikilink")

1.
2.
3.