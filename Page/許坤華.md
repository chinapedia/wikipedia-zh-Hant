**許坤華**，[香港企业家](../Page/香港.md "wikilink")。也是[十友控股](../Page/十友控股.md "wikilink")(0033.HK)創辦人和前董事。

## 生平

許坤華在創辦[十友控股之前](../Page/十友控股.md "wikilink")，在1980年畢業於[拔萃男書院後](../Page/拔萃男書院.md "wikilink")，讀上[香港理工大學](../Page/香港理工大學.md "wikilink")，直至1984年畢業，畢業後，他加入[安達信](../Page/安達信.md "wikilink")（受美國[安然事件已倒閉](../Page/安然.md "wikilink")）已達至5年。1987年起為[英國特許公認會計師公會](../Page/英國特許公認會計師.md "wikilink")、[香港會計師公會會員](../Page/香港會計師公會.md "wikilink")、執業會計師兼[英格蘭及威爾斯特許會計師公會會員](../Page/英格蘭及威爾斯特許會計師公會.md "wikilink")。1995年獲[香港大學頒授工商管理碩士學位](../Page/香港大學.md "wikilink")。

## 創業轉捩點

1996年，他創立[十友控股](../Page/十友控股.md "wikilink")，主要向美國一元店提供設計商品，因此銷售強勁。2000年，他成功邀請[吳志民先生加入董事局](../Page/吳志民.md "wikilink")；2001年，因為要面對911事件問題，在[萬聖節時日](../Page/萬聖節.md "wikilink")，於是他們隨時應變，快速將[美國國旗運去](../Page/美國.md "wikilink")[美國](../Page/美國.md "wikilink")，迎合市場需求，故此在[美國銷售強勁](../Page/美國.md "wikilink")；2005年，他再向[黃國定先生加入董事局](../Page/黃國定.md "wikilink")。

2007年1月，他成功帶領把[十友控股於](../Page/十友控股.md "wikilink")[香港交易所主板上市](../Page/香港交易所.md "wikilink")。

## 馬主生涯

許坤華和妻子葉思貝、好友凌志昌、劉學倫是[香港賽馬會的會員](../Page/香港賽馬會.md "wikilink")，名下馬匹（包括故園情團體、AA84團體、暗飛聲團體、劍重逢團體、劍追風團體、拔萃78之友團體、拔萃舊生團體、香港愛馬女士協會團體、入得廚房團體）包括凌雲木、荊山玉、夜光杯、故園情、劍追風（SILLY
BOY）、碧紗籠、華劍寶、劍飛聲、劍十三、暗飛聲、劍如虹、劍無情、劍凌雲、劍斷浪、劍無名、劍驚雲、劍聶風、劍二十、愛馬劍、劍追風（MY
ALLY）、邊個邊個、劍追聲、任我行、嘜誰嘜誰等等。

## 學歷

  - [香港理工大學](../Page/香港理工大學.md "wikilink")
  - [香港大學工商管理碩士學位](../Page/香港大學.md "wikilink")

## 相關連結

  - [「 一 元 店 」 十 友 下 月 掛
    牌](http://www.singtao.com/index_archive.asp?d_str=20071029&htmlpage=main&news=1029do02.html)
  - [十友上市推介搞鬼](http://the-sun.on.cc/channels/fina/20071101/20071101010303_0000.html)

[H](../Category/香港商人.md "wikilink")
[Category:香港理工大學校友](../Category/香港理工大學校友.md "wikilink")
[H](../Category/拔萃男書院校友.md "wikilink")
[Category:香港會計師](../Category/香港會計師.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:許姓](../Category/許姓.md "wikilink")
[Category:會計師出身的商人](../Category/會計師出身的商人.md "wikilink")