[HKU_Campus_Hui_Oi_Chow_Science_Building_RainyD.JPG](https://zh.wikipedia.org/wiki/File:HKU_Campus_Hui_Oi_Chow_Science_Building_RainyD.JPG "fig:HKU_Campus_Hui_Oi_Chow_Science_Building_RainyD.JPG")以其為名的許愛周科學館\]\]
**許愛周** （Hui Oi
Chow，），字**國仁**，是已故[船王之一](../Page/船王.md "wikilink")，在[廣東省吳川縣坡頭](../Page/廣東省.md "wikilink")（今湛江市坡头区坡头镇）博立村出生，於法屬殖民地[廣州灣](../Page/廣州灣.md "wikilink")（今[湛江](../Page/湛江.md "wikilink")）長大。[小學畢業後](../Page/小學.md "wikilink")，隨父經商，創立「福泰號」，專營[花生油等糧油雜貨](../Page/花生油.md "wikilink")。
在[吳川](../Page/吳川.md "wikilink")、[赤坎](../Page/赤坎.md "wikilink")、[霞山](../Page/霞山.md "wikilink")、[硇洲等地再開設](../Page/硇洲.md "wikilink")「仁和號」、「廣宏泰」、「天元號」等店號。在[香港開設有](../Page/香港.md "wikilink")「廣宏泰」，進口外國貨，貨品包括花生油、[煤油](../Page/煤油.md "wikilink")、[水產等](../Page/水產.md "wikilink")，成為當年的富商。

在1920年代，許愛周開始發展地產，在赤坎[填海獲得土地](../Page/填海.md "wikilink")。1930年代，發展[酒店業及工商](../Page/酒店.md "wikilink")[住宅物業](../Page/住宅物業.md "wikilink")，又獲利。之後，更成立「順昌航業公司」，發展[中國](../Page/中國.md "wikilink")[沿海及](../Page/沿海.md "wikilink")[內河運輸](../Page/內河運輸.md "wikilink")，與當時的外商比高低。在[中日戰爭時期](../Page/中日戰爭.md "wikilink")，由於[法國](../Page/法國.md "wikilink")、[日本的](../Page/日本.md "wikilink")[外交關係](../Page/外交.md "wikilink")，令許愛周以[廣州灣為基地的](../Page/廣州灣.md "wikilink")[航運業務更盛](../Page/航運.md "wikilink")。及後他合夥經營的大安航業公司、太平航業公司、泰豐航業公司及廣利航業公司等，大舉購置[輪船](../Page/輪船.md "wikilink")，商船遠至[東南亞等地](../Page/東南亞.md "wikilink")，名世人稱為「航運界巨子」。1949年，在香港成立「仁興礦務公司」。1952年，在香港註冊有「順昌航業有限公司」。1957年興友人合資創立[中建企業有限公司](../Page/中建企業有限公司.md "wikilink")，購入位於[皇后大道中的](../Page/皇后大道.md "wikilink")[香港大酒店原址](../Page/香港大酒店.md "wikilink")，建成[中建大廈](../Page/中建大廈.md "wikilink")。及後又建有中環附近的[亞細亞行](../Page/香港中匯大廈.md "wikilink")，成為香港著名地產發展商之一。

1966年許愛周逝世，安葬於[上水](../Page/上水.md "wikilink")[松柏塱的](../Page/松柏塱.md "wikilink")[愛園別墅](../Page/愛園別墅.md "wikilink")。\[1\]次子[許士芬捐資](../Page/許士芬.md "wikilink")[香港大學](../Page/香港大學.md "wikilink")，立[許愛周科學館紀念其父許愛周](../Page/許愛周科學館.md "wikilink")。

## 參考文獻

## 相關

  - [許愛周家族](../Page/許愛周家族.md "wikilink")

## 外部連線

  - [中國僑界:《許愛周家族的湧泉之報》](http://www.chinaqw.com.cn/node2/node116/node1165/node1177/node1836/userobject6ai129573.html)
  - [愛國愛鄉的許愛周家族
    作者/秋夜之聲](http://www.diarybooks.com/article/2006/8/21/1324372.html)
  - [中華許氏族譜 -
    一代船王許愛周](https://web.archive.org/web/20070930120759/http://www.chinaxu.net/show.asp?cat_id=6&id=361)

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:湛江人](../Category/湛江人.md "wikilink")
[Category:船王](../Category/船王.md "wikilink")
[O](../Category/許姓.md "wikilink")
[Category:許愛周家族](../Category/許愛周家族.md "wikilink")

1.