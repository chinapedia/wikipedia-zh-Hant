**許榮淑**，[台灣](../Page/台灣.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，是[民主進步黨及](../Page/民主進步黨.md "wikilink")[人民最大黨創黨黨員](../Page/人民最大黨.md "wikilink")，曾任[中華民國](../Page/中華民國.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，現任人民最大黨主席。許榮淑的丈夫為民主進步黨創黨黨員[張俊宏](../Page/張俊宏.md "wikilink")，兩人於1967年[結婚](../Page/結婚.md "wikilink")，分居超過20年，從未簽[離婚協議書](../Page/離婚.md "wikilink")。
1981年，許榮淑代替被判[叛亂罪的張俊宏參選](../Page/叛亂罪.md "wikilink")[中彰投增額立法委員](../Page/中彰投.md "wikilink")，當選。
1998年，許榮淑結合民進黨剩餘[美麗島系山頭成立](../Page/美麗島系.md "wikilink")[新動力國會辦公室](../Page/新動力國會辦公室.md "wikilink")\[1\]。

2005年1月，張俊宏與其女友林慧珍爆發糾紛，許榮淑說，她認為現在張俊宏遭到[天譴](../Page/天譴.md "wikilink")，而她也很不諒解為何張俊宏把林慧珍帶回家：「你（張俊宏）去坐牢很辛苦嘛，出來想要玩一玩，我都可以！但是不要把牛都牽到家裡嘛，這樣不好嘛，這樣對我不敬嘛！（林慧珍）一天到晚都寫黑函，說我討[契兄](../Page/契兄.md "wikilink")，寫了許多亂七八糟，好像你交的朋友都變成契兄。10年前我就知道他（張俊宏）會有天譴。」\[2\]

2006年4月，許榮淑涉嫌協助[陳育珅](../Page/陳育珅.md "wikilink")[詐騙集團](../Page/詐騙集團.md "wikilink")[詐欺與](../Page/詐欺.md "wikilink")[掏空](../Page/掏空.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")[清三電子](../Page/清三電子.md "wikilink")，遭[邱毅告發](../Page/邱毅.md "wikilink")[詐欺罪](../Page/詐欺罪.md "wikilink")；許榮淑叱之無稽，反控邱毅涉嫌[誹謗罪](../Page/誹謗罪.md "wikilink")。後許榮淑以證據不足未遭起訴，邱毅毀謗案後以[和解收場](../Page/和解.md "wikilink")。

2009年7月23日，民進黨中評會以許榮淑違紀赴[中華人民共和國](../Page/中華人民共和國.md "wikilink")[長沙市出席第五屆](../Page/長沙市.md "wikilink")[兩岸經貿文化論壇為由](../Page/兩岸經貿文化論壇.md "wikilink")，將許榮淑停止黨權三年。7月24日，許榮淑說，身為大黨的民進黨還是要面對中國問題，她今天的行為就是要讓民進黨面對中國問題，「（[民進黨主席](../Page/民進黨主席.md "wikilink")）[蔡英文](../Page/蔡英文.md "wikilink")，妳的中國政策是什麼？告訴我這個老阿嬤」；她同時諷刺，在民主、自由、人權及法治都落實的時候，民進黨中央現在又要用言論箝制她與同樣出席第五屆兩岸經貿文化論壇的[范振宗](../Page/范振宗.md "wikilink")\[3\]。7月27日，民進黨召開臨時中評會，開除許黨籍\[4\]。曾在許榮淑立委任內擔任其助理的民進黨[臺北縣](../Page/臺北縣.md "wikilink")[三重市黨部主任委員](../Page/三重市.md "wikilink")[鍾鎮宇在自己主持的](../Page/鍾鎮宇.md "wikilink")[廣播電台節目上呼籲](../Page/廣播電台.md "wikilink")[泛綠群眾冷靜看待許榮淑出席兩岸經貿文化論壇一事](../Page/泛綠.md "wikilink")，引發部分深綠支持者不滿，他的節目被該電台停播，甚至有人打算罷免他\[5\]。

2009年9月26日，許榮淑創立人民最大黨。

2010年4月29日，許榮淑、美麗島系大老[許信良](../Page/許信良.md "wikilink")、[中國國民黨榮譽主席](../Page/中國國民黨榮譽主席.md "wikilink")[連戰](../Page/連戰.md "wikilink")、中國國民黨榮譽主席[吳伯雄](../Page/吳伯雄.md "wikilink")、[親民黨主席](../Page/親民黨.md "wikilink")[宋楚瑜參觀](../Page/宋楚瑜.md "wikilink")[中國2010年上海世界博覽會](../Page/中國2010年上海世界博覽會.md "wikilink")，在[上海西郊賓館會見](../Page/上海西郊賓館.md "wikilink")[中國共產黨中央委員會總書記](../Page/中國共產黨中央委員會總書記.md "wikilink")[胡錦濤](../Page/胡錦濤.md "wikilink")\[6\]。

2011年9月19日，許榮淑宣布參選[2012年中華民國總統選舉](../Page/2012年中華民國總統選舉.md "wikilink")，副手由[地球公義連線總召集人](../Page/地球公義連線.md "wikilink")[吳嘉琍擔任](../Page/吳嘉琍.md "wikilink")，提出「人人當[王雪紅](../Page/王雪紅.md "wikilink")」、「[健保免費](../Page/全民健康保險.md "wikilink")」等政見，並發起[新台幣和](../Page/新台幣.md "wikilink")[人民幣](../Page/人民幣.md "wikilink")[匯率一比一的](../Page/匯率.md "wikilink")[公民投票](../Page/公民投票.md "wikilink")，及表達聲援前總統[陳水扁的立場](../Page/陳水扁.md "wikilink")\[7\]\[8\]\[9\]。然而，由於許榮淑及吳嘉琍僅取得不到2萬5800份連署書，且並未將連署書送交[中央選舉委員會審查](../Page/中華民國中央選舉委員會.md "wikilink")，因此失去參選資格。

2011年9月28日，許榮淑與人民最大黨副秘書長[莊嚴挑著一邊裝](../Page/莊嚴_\(政治人物\).md "wikilink")[山蘇](../Page/山蘇.md "wikilink")、一邊裝[空心菜的](../Page/空心菜.md "wikilink")[扁擔](../Page/扁擔.md "wikilink")，前往民進黨中央黨部「祝賀」民進黨25週年黨慶，嘲諷蔡英文「乞丐趕廟公」開除許榮淑黨籍，也嘲諷民進黨既聽不見人民的聲音、也遠離許榮淑25年前奮鬥的目標\[10\]。

2015年5月2日，许荣淑與人民最大黨副主席[杨荣光到](../Page/杨荣光.md "wikilink")[上海市出席第十届两岸经贸文化论坛](../Page/上海市.md "wikilink")，许荣淑行前在[桃园机场搭机时接受](../Page/桃园机场.md "wikilink")[中评社采访](../Page/中评社.md "wikilink")，希望已於2014年回任民进党主席的蔡英文勇于突破、[大胆西进](../Page/大胆西进.md "wikilink")。\[11\]

2015年7月7日，許榮淑宣布參選[2016年中華民國總統選舉](../Page/2016年中華民國總統選舉.md "wikilink")，提出「制定新憲法、解散舊政黨、改造新國會、新地方自治、新城鄉平衡、新吏治風氣、新公民正義」七大訴求。許榮淑說，她一生追求民主、自由、人權，也因此成為民進黨創黨黨員，推動過[中華民國國軍老兵返鄉探親等人權活動](../Page/中華民國國軍.md "wikilink")，民進黨卻因為她赴中國大陸訪問就開除她的黨籍，她很不滿民進黨不尊重人民的聲音，所以她創立人民最大黨；她主張兩岸關係正常化、和平發展；不和中國大陸交流，就不會知道中國大陸多進步，也會沒有發言權。\[12\]期間許榮淑未將連署書送交中央選舉委員會審查，因此失去參選資格。

[2016年中華民國立法委員選舉](../Page/2016年中華民國立法委員選舉.md "wikilink")，[健保免費連線提名許榮淑等三人參選不分區立委](../Page/健保免費連線.md "wikilink")，但三人都落選。

2016年9月28日是民進黨30週年黨慶，許榮淑發布新聞稿，期許二度執政的民進黨明定兩岸政策，「儘量避免淪為見招拆招的被動式因應」\[13\]。

許榮淑親妹妹[許恆慈是前](../Page/許恆慈.md "wikilink")[雲林縣縣長](../Page/雲林縣縣長.md "wikilink")[廖泉裕之妻](../Page/廖泉裕.md "wikilink")，他們的女兒[廖紫岑曾任](../Page/廖紫岑.md "wikilink")[台灣數位光訊科技](../Page/台灣數位光訊科技.md "wikilink")[執行長](../Page/執行長.md "wikilink")\[14\]。

## 参考文献

|- |colspan="3"
style="text-align:center;"|**[人民最大黨](../Page/人民最大黨.md "wikilink")**
|-

[Category:第1屆國民大會增額代表](../Category/第1屆國民大會增額代表.md "wikilink")
[Category:被開除民主進步黨黨籍者](../Category/被開除民主進步黨黨籍者.md "wikilink")
[Category:人民最大黨黨員](../Category/人民最大黨黨員.md "wikilink")
[Category:臺灣女性政治人物](../Category/臺灣女性政治人物.md "wikilink")
[Category:國立臺灣師範大學校友](../Category/國立臺灣師範大學校友.md "wikilink")
[Category:國立中山大學校友](../Category/國立中山大學校友.md "wikilink")
[Category:南投人](../Category/南投人.md "wikilink")
[R榮](../Category/許姓.md "wikilink")
[Category:臺灣政黨領袖](../Category/臺灣政黨領袖.md "wikilink")

1.
2.
3.
4.  [民進黨開除許榮淑-{范}-振宗](http://www.libertytimes.com.tw/2009/new/jul/28/today-fo2.htm)

5.
6.
7.  [人人當王雪紅！　人民最大黨政見另類](http://www.tvbs.com.tw/news/news_list.asp?no=vestashi20110811124938)，TVBS新聞，2011年8月11日。
8.  [許榮淑發起公投兩岸貨幣一比一](http://www.chinareviewnews.com/doc/1018/6/8/6/101868671.html?coluid=7&kindid=0&docid=101868671)，中國評論新聞，2011年8月11日。
9.  [許榮淑高院聲援陳水扁](http://wenews.nownews.com/news/41/news_41427.htm)
    ，今日新聞網，2011年10月17日。
10. [民進黨今天25歲生日
    許榮淑送山蘇、空心菜祝壽](http://www.nownews.com/n/2011/09/28/479295)
11. [启程赴上海
    许荣淑：蔡英文应大胆西进](http://www.crntt.com/doc/1037/3/5/2/103735203.html?coluid=46&kindid=0&docid=103735203&mdate=0502094324)，中评社，2015-05-02
12. [台灣阿嬤許榮淑參選總統 盼結束民主內戰](http://newtalk.tw/news/view/2015-07-07/61985)
13.
14.