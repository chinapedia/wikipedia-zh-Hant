**許榮茂**（）是[香港](../Page/香港.md "wikilink")、[澳州](../Page/澳州.md "wikilink")、[中國大陸以及](../Page/中國大陸.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")[企業家以及](../Page/企業家.md "wikilink")[富豪](../Page/富豪.md "wikilink")。他的籍貫是[福建](../Page/福建.md "wikilink")[石獅](../Page/石狮市.md "wikilink")，後來遷居[香港](../Page/香港.md "wikilink")，80年代再以港商身份回中國大陸以及澳州發展，為[香港世茂集團創辦人及董事長](../Page/香港世茂集團.md "wikilink")，[世茂股份有限公司](../Page/世茂股份有限公司.md "wikilink")、[世茂房地產](../Page/世茂房地產.md "wikilink")（）及[世茂國際主席](../Page/世茂國際.md "wikilink")，公司的總部在香港以及上海。許榮茂至今仍是香港的永久居民，而且獲得[金紫荊星章以及香港](../Page/金紫荊星章.md "wikilink")「傑出貢獻人物」的獎項，而且擔任港區全國政協委員。\[1\]此外，許榮茂还拥有[美籍華人以及](../Page/美籍華人.md "wikilink")[澳洲永久居民的身份](../Page/澳洲永久居民.md "wikilink")。2013年福布斯華人富豪榜五十大當中有十名香港人上榜，而許榮茂在香港地區排第9位，全世界華人(其餘富豪大多數在東南亞、台灣)排第32位，旗下資產遍佈[香港](../Page/香港.md "wikilink")、[澳州](../Page/澳州.md "wikilink")、[中國大陸以及](../Page/中國大陸.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")，但最多都在上海，他亦是上海首富，中國房地產首富，許榮茂同時入選澳州富豪榜第七位。許榮茂的世茂集團15年來累計捐款超7億元。\[2\]

## 簡介

許榮茂原為中醫，1979年，許榮茂來到香港。無錢無背景，他什麽行業都做，只要不違法的錢就賺。先涉足香港證券市場起家為股票經紀，常年累月地勤奮工作使他小有積蓄，而敏銳的判斷能力和過人的投資天分幫助其在香港資本市場大展拳腳并在[香港開設金融公司](../Page/香港.md "wikilink")。從此以後，在"買入賣出"的幾年間，他獲利豐厚。有人推算，許榮茂炒股所賺的錢至少在5億元左右。股市裏的錢來得容易，去得快。恰逢1987年香港股災，看著周圍股友家破人亡，體會到股市炎涼，許榮茂決定把錢"固化"下來。他回到內地，先在中國內地深圳和蘭州做紡織和成衣，后又從事[紡織品出口貿易](../Page/紡織品.md "wikilink")，主要出口到[美國](../Page/美國.md "wikilink")\[3\]。其後改行進軍房地產業於[北京](../Page/北京.md "wikilink")、[蘭州進行發展物業賺得缽滿盆滿且異常低調](../Page/蘭州.md "wikilink")。2004年，許榮茂以低價購入[上海](../Page/上海.md "wikilink")[浦東](../Page/浦東.md "wikilink")[黃浦江邊](../Page/黃浦江.md "wikilink")，接近100萬平方米的土地，興建[世茂濱江花園](../Page/世茂濱江花園.md "wikilink")，落成後成為上海著名豪宅，並為世茂房地產帶來利潤。現時其房地產項目分別於[福建](../Page/福建.md "wikilink")、[北京](../Page/北京.md "wikilink")、[上海及香港等地](../Page/上海.md "wikilink")。近年亦拓展[長江三角洲二線城市](../Page/長江三角洲.md "wikilink")，如[昆山](../Page/昆山.md "wikilink")、[常熟](../Page/常熟.md "wikilink")、[南京](../Page/南京.md "wikilink")、[蕪湖及](../Page/蕪湖.md "wikilink")[紹興等地](../Page/紹興.md "wikilink")，土地儲備近1500萬平方米。而許榮茂旗下的世茂國際，則投資於[黑龍江省](../Page/黑龍江省.md "wikilink")[綏芬河市與](../Page/綏芬河市.md "wikilink")[俄羅斯](../Page/俄羅斯.md "wikilink")[波格拉尼奇的中俄邊境貿易綜合區](../Page/波格拉尼奇.md "wikilink")，總投資達100億元[人民幣](../Page/人民幣.md "wikilink")。許榮茂不願入選富豪榜
吃飯以粥和青菜為主

2017年9月30日，許榮茂透過旗下兩間全資附屬公司「至選」及「雅選」，總共斥資21.72億元認購[中信銀行（國際）](../Page/中信銀行（國際）.md "wikilink")7.27億股份，佔擴大後已發行股本6%。

## 逸闻

許榮茂在業界那是出了名的低調，平時生活要求很低，吃飯一般是粥和青菜為主。許榮茂有三大愛好：看報、喝茶、收藏名家字畫。\[4\]許榮茂不願入選富豪榜，曾經曝光因在2007年[福布斯中國富豪榜把他列入榜單中排行第](../Page/福布斯中國富豪榜.md "wikilink")2位後，而弄得許榮茂大發雷霆。所以，至今沒有人了解許榮茂在香港股市上到底賺了多少錢，當然更不會人知道他炒過哪些股票。但是，十幾年前，許榮茂炒別人公司的股票;十幾年後，世茂系股票風聲水起，購買者趨之若鶯。

## 家庭

許榮茂已婚，其子[許世壇為](../Page/許世壇.md "wikilink")[世茂房地產執行董事](../Page/世茂房地產.md "wikilink")，女兒[許薇薇亦協助](../Page/許薇薇.md "wikilink")[世茂國際的業務](../Page/世茂國際.md "wikilink")。

## 參見

  - [世茂房地產](../Page/世茂房地產.md "wikilink")
  - [世茂國際](../Page/世茂國際.md "wikilink")

## 参考资料

## 資料來源

  - [背景資料：世茂集團董事長許榮茂](http://finance.sina.com.cn/o/20020302/176236.html)
  - [世茂無意中俄邊境搞賭場](http://chinabiz.mpfinance.com/cfm/content2.cfm?PublishDate=20060816&TopicID=hp05&Filename=ebd1.txt)
    明報
  - [中俄邊境打造北方深圳](http://chinabiz.mpfinance.com/cfm/content2.cfm?PublishDate=20060817&TopicID=hp05&Filename=eab1.txt)
    明報

[Category:中国房地产商](../Category/中国房地产商.md "wikilink")
[Category:石獅人](../Category/石獅人.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:獲頒授香港金紫荊星章者](../Category/獲頒授香港金紫荊星章者.md "wikilink")
[Category:中国企业家](../Category/中国企业家.md "wikilink")
[Category:中華人民共和國億萬富豪](../Category/中華人民共和國億萬富豪.md "wikilink")
[Category:香港億萬富豪](../Category/香港億萬富豪.md "wikilink")
[Category:全国政协香港委员](../Category/全国政协香港委员.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:第十二届全国政协委员](../Category/第十二届全国政协委员.md "wikilink")
[R](../Category/許姓.md "wikilink")
[Category:福建人](../Category/福建人.md "wikilink")
[Category:福建企业家](../Category/福建企业家.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")

1.  <http://paper.wenweipo.com/2012/08/02/HK1208020012.htm>
2.  2012年12月20日10:38 來源：公益時報
3.  <http://www.northnews.cn/2012/0917/921291.shtml>
4.  許榮茂不願入選富豪榜 吃飯以粥和青菜為主