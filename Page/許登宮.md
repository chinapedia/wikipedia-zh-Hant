**許登宮**（），生於台灣嘉義縣民雄鄉東湖村10鄰東勢湖，[政治人物](../Page/政治人物.md "wikilink")，曾經代表[中國國民黨及](../Page/中國國民黨.md "wikilink")[台灣團結聯盟任職](../Page/台灣團結聯盟.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。

## 生平

國小畢業後就至台北市工作，第一份工作是在三重市名人[蔡詩祥開設的永祥油漆行](../Page/蔡詩祥.md "wikilink")。

以從事油漆業而致富。在致富後，於林口地區購置許多土地。1990年間，成為股市主力之一，被稱為「林口許」。

2018年3月16日辭世，享壽80歲。\[1\]

## 注釋

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00112&stage=5)

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:第10屆臺灣省議員](../Category/第10屆臺灣省議員.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:台灣團結聯盟黨員](../Category/台灣團結聯盟黨員.md "wikilink")
[Category:民雄人](../Category/民雄人.md "wikilink")
[Deng登](../Category/許姓.md "wikilink")

1.  [台聯前立委許登宮辭世　享壽80歲](https://tw.news.appledaily.com/politics/realtime/20180321/1318939/)