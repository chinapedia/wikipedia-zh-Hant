**許秋怡**（****，），[香港](../Page/香港.md "wikilink")[女歌手](../Page/女歌手.md "wikilink")，最著名歌曲是與[張智霖合唱](../Page/張智霖.md "wikilink")《現代愛情故事》。

## 簡介

許秋怡出生在一個[粵劇世家](../Page/粵劇.md "wikilink")，其父是香港粵劇名伶[許堅信](../Page/許堅信.md "wikilink")，她六歲起學習粵劇唱功與功架。\[1\]小學於[元朗](../Page/元朗.md "wikilink")[鐘聲學校畢業](../Page/鐘聲學校.md "wikilink")，中學於[新界鄉議局元朗區中學畢業](../Page/新界鄉議局元朗區中學.md "wikilink")，後來加入[永高創意成為歌手](../Page/永高創意.md "wikilink")。

1991年，她被安排與[張智霖合唱](../Page/張智霖.md "wikilink")《現代愛情故事》而一曲成名，11月二人推出一張全合唱同名專輯，達雙白金銷量。碟內其他歌曲《片片楓葉情》等也成為熱門合唱K歌。同年公司易名[創意家族](../Page/創意家族.md "wikilink")。1992年1月，與張智霖榮獲[商業電台](../Page/香港商業電台.md "wikilink")[叱咤樂壇流行榜生力軍組合金獎](../Page/叱咤樂壇流行榜.md "wikilink")。

1992年初，與張智霖一同簽約[無線電視](../Page/無線電視.md "wikilink")，再次亮相各大電視節目宣傳《現代愛情故事》及《片片楓葉情》，並以《現代愛情故事》奪得[勁歌金曲第一季季選歌曲獎](../Page/勁歌金曲.md "wikilink")。同年中，又參與和[蔣志光](../Page/蔣志光.md "wikilink")、[韋綺姍](../Page/韋綺姍.md "wikilink")、[張智霖](../Page/張智霖.md "wikilink")、[黃寶欣](../Page/黃寶欣.md "wikilink")、[甘子暉合唱的](../Page/甘子暉.md "wikilink")《舊朋友》，此歌奪得[勁歌金曲第三季季選歌曲獎](../Page/勁歌金曲.md "wikilink")。由於是年唱片公司部署先為[張智霖推出個人專輯](../Page/張智霖.md "wikilink")，許秋怡則在1993年才正式作個人發展。

1993年，創意家族正式給她獨立出唱片——《秋怡》，以「得意少女」作為宣傳號召，專輯得到[金唱片的銷量](../Page/金唱片.md "wikilink")，同年創意家族改為製作公司，由[麗音唱片發行](../Page/麗音唱片.md "wikilink")，首年獨立發展為許秋怡奪得[商業電台](../Page/香港商業電台.md "wikilink")[叱咤樂壇流行榜生力軍女歌手銅獎](../Page/叱咤樂壇流行榜.md "wikilink")。

1994年-1995年，許秋怡分別推出《The Colour of Love》及《電影少女》兩張粵語專輯後，改為發展台灣市場。

1996年，許秋怡出版首張[國語唱片](../Page/國語.md "wikilink")《[傻的可以](../Page/傻的可以_\(許秋怡專輯\).md "wikilink")》，由[現代派唱片發行](../Page/現代派唱片.md "wikilink")。

1999年，許秋怡離開現代派唱片後，許秋怡仍然時不時在[無綫電視的綜藝節目出現做嘉賓或出演劇集](../Page/香港無綫電視.md "wikilink")。

許秋怡經常北上[中國大陸演唱](../Page/中國大陸.md "wikilink")，偶爾海外登台，出騷頻密，努力賺錢，2010年被雜誌報導為隱形富婆之一。\[2\]

2011年[張智霖首次在紅館舉行兩場](../Page/張智霖.md "wikilink")《我是外星人》演唱會，邀請舊拍檔許秋怡做其中一場的嘉賓，雖然只合唱一首歌（《現代愛情故事》），但由於觀眾已多年沒有機會見原唱者同台演唱此曲，所以全場觀眾情緒高漲，引起哄動。\[3\]

## 感情生活

她初出道時與拍檔[張智霖被稱為](../Page/張智霖.md "wikilink")「金童玉女」，二人曾有過一段戀情，後分手。2003年與[王書麒組織家庭](../Page/王書麒.md "wikilink")，並誕下一子王綽枬，兒子就讀[拔萃男書院附屬小學](../Page/拔萃男書院附屬小學.md "wikilink")。\[4\]

2017年9月 許秋怡於海俊傑婚禮上自爆與王書麒至今仍未註冊結婚。

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/秋怡.md" title="wikilink">秋怡</a></p></td>
<td style="text-align: left;"><p>粵語大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/創意家族.md" title="wikilink">創意家族</a></p></td>
<td style="text-align: left;"><p>1993年6月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/The_Colour_of_Love.md" title="wikilink">The Colour of Love</a></p></td>
<td style="text-align: left;"><p>粵語大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/創意家族.md" title="wikilink">創意家族</a></p></td>
<td style="text-align: left;"><p>1994年12月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3</p></td>
<td style="text-align: left;"><p><a href="../Page/電影少女_(許秋怡專輯).md" title="wikilink">電影少女</a></p></td>
<td style="text-align: left;"><p>粵語新曲+精選</p></td>
<td style="text-align: left;"><p><a href="../Page/創意家族.md" title="wikilink">創意家族</a></p></td>
<td style="text-align: left;"><p>1995年8月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4</p></td>
<td style="text-align: left;"><p><a href="../Page/傻的可以_(許秋怡專輯).md" title="wikilink">傻的可以</a></p></td>
<td style="text-align: left;"><p>國語大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/現代派唱片.md" title="wikilink">現代派唱片</a></p></td>
<td style="text-align: left;"><p>1996年6月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>5</p></td>
<td style="text-align: left;"><p><a href="../Page/懶得再戀愛.md" title="wikilink">懶得再戀愛</a></p></td>
<td style="text-align: left;"><p>國語大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/現代派唱片.md" title="wikilink">現代派唱片</a></p></td>
<td style="text-align: left;"><p>1997年1月</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 合輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/現代愛情故事.md" title="wikilink">現代愛情故事</a></p></td>
<td style="text-align: left;"><p>粵語大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/創意家族.md" title="wikilink">創意家族</a></p></td>
<td style="text-align: left;"><p>1991年11月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/蔣志光與他的朋友.md" title="wikilink">蔣志光與他的朋友</a></p></td>
<td style="text-align: left;"><p>新曲＋精選</p></td>
<td style="text-align: left;"><p><a href="../Page/創意家族.md" title="wikilink">創意家族</a></p></td>
<td style="text-align: left;"><p>1992年5月</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 單曲

1992年：《舊朋友》（[蔣志光](../Page/蔣志光.md "wikilink")、[韋綺姍](../Page/韋綺姍.md "wikilink")、[黃寶欣](../Page/黃寶欣.md "wikilink")、[張智霖](../Page/張智霖.md "wikilink")、[甘子暉合唱](../Page/甘子暉.md "wikilink")）（收錄於《[蔣志光與他的朋友](../Page/蔣志光與他的朋友.md "wikilink")》專輯》）
1992年：《為分開慶賀》（[張智霖合唱](../Page/張智霖.md "wikilink")）（收錄於張智霖《[逗我開心吧](../Page/逗我開心吧.md "wikilink")》專輯）

## 派台歌曲成績

| **派台歌曲成績**                                                     |
| -------------------------------------------------------------- |
| 唱片                                                             |
| **1991年**                                                      |
| [現代愛情故事](../Page/現代愛情故事.md "wikilink")                         |
| 現代愛情故事                                                         |
| **1992年**                                                      |
| [蔣志光與他的朋友](../Page/蔣志光與他的朋友.md "wikilink")                     |
| **1993年**                                                      |
| [秋怡](../Page/秋怡.md "wikilink")                                 |
| 秋怡                                                             |
| 秋怡                                                             |
| 秋怡                                                             |
| [The Colour of Love](../Page/The_Colour_of_Love.md "wikilink") |
| **1994年**                                                      |
| The Colour of Love                                             |
| The Colour of Love                                             |
| **1995年**                                                      |
| [電影少女](../Page/電影少女_\(許秋怡專輯\).md "wikilink")                   |
| 電影少女                                                           |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 戲劇演出

  - 1993年：[無綫電視](../Page/無綫電視.md "wikilink")《[射雕英雄傳之九陰真經](../Page/射雕英雄傳之九陰真經.md "wikilink")》飾
    裘彩蘭（裘千仞之女）（與[張智霖合作](../Page/張智霖.md "wikilink")）
  - 1994年：[香港電台](../Page/香港電台.md "wikilink")《[獅子山下](../Page/獅子山下.md "wikilink")
    之 [再進沈園](../Page/再進沈園.md "wikilink")》 飾 蔣婷
  - 1995年：[無綫電視](../Page/無綫電視.md "wikilink")《[刀馬旦](../Page/刀馬旦_\(無綫電視劇\).md "wikilink")》飾
    張菊秋（周慧敏之師妹）
  - 1996年：[無綫電視](../Page/無綫電視.md "wikilink")《[西遊記](../Page/西遊記_\(1996年電視劇\).md "wikilink")》飾
    [紅孩兒](../Page/紅孩兒.md "wikilink")
  - 2012年：[無綫電視](../Page/無綫電視.md "wikilink")《[肥婆奶奶扭計媳](../Page/肥婆奶奶扭計媳.md "wikilink")》飾
    何晶晶

## 獎項

  - 1991年度商台[叱咤樂壇流行榜生力軍組合金獎](../Page/1991年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")－
    [張智霖](../Page/張智霖.md "wikilink")、許秋怡
  - 1992年[勁歌金曲第一季季選得獎歌曲](../Page/勁歌金曲優秀選.md "wikilink")－《現代愛情故事》（與[張智霖合唱](../Page/張智霖.md "wikilink")）
  - 1992年[勁歌金曲第二季季選得獎歌曲](../Page/勁歌金曲優秀選.md "wikilink")－《舊朋友》（與[蔣志光](../Page/蔣志光.md "wikilink")、[韋綺姍](../Page/韋綺姍.md "wikilink")、[張智霖](../Page/張智霖.md "wikilink")、[黃寶欣](../Page/黃寶欣.md "wikilink")、[甘子暉合唱](../Page/甘子暉.md "wikilink")）
  - 1992年[新城電台](../Page/新城電台.md "wikilink")“勁爆合唱歌曲大獎”
    得獎歌曲－《現代愛情故事》（與[張智霖合唱](../Page/張智霖.md "wikilink")）
  - 1993年度商台[叱咤樂壇流行榜生力軍女歌手銅獎](../Page/1993年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  -
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[chau](../Category/許姓.md "wikilink")
[Category:新界鄉議局元朗區中學校友](../Category/新界鄉議局元朗區中學校友.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")

1.  [1](http://ent.i-cable.com/cen/news_video/index.php?video_id=32152)
    2013年1月24日有線寬頻-許秋怡的粵劇童年
2.  [2](http://eastweek.my-magazine.me/index.php?aid=9566)
    2010年11月10日《東週刊》 378期 Book B 【娛樂頭條】 ：5大隱形富婆 親授發達秘笈
3.  [3](http://www.youtube.com/watch?v=ba9yLuR8i54) YouTube.
    2011年3月29日蘋果動新聞
4.  \[[http://ent.i-cable.com/cen/news_video/index.php?video_id=32422\]王書麒憶與許秋怡修喜帖情意](http://ent.i-cable.com/cen/news_video/index.php?video_id=32422%5D王書麒憶與許秋怡修喜帖情意)