**許雲雲**為[台灣女性](../Page/台灣.md "wikilink")[配音員](../Page/配音員.md "wikilink")。

## 配音作品

  - 主要角色名稱以**粗體**顯示。
  - 以下皆為國語配音。

### 台灣動畫電影

| 播映年份 | 作品名稱                                     | 配演角色 | 備註 |
| ---- | ---------------------------------------- | ---- | -- |
| 2007 | [海之傳說－媽祖](../Page/海之傳說－媽祖.md "wikilink") | 五姐   |    |
|      |                                          |      |    |

### 台灣遊戲

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>遊戲名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/大富翁8.md" title="wikilink">大富翁8</a></p></td>
<td><p>星期三<br />
莎菈公主</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/火鳳三國Online.md" title="wikilink">火鳳三國Online</a></p></td>
<td><p><strong><a href="../Page/馬岱.md" title="wikilink">馬岱</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/星界神話.md" title="wikilink">星界神話</a></p></td>
<td><p>男 6<small>（禮貌男聲）</small><br />
凱西<br />
桑提勒雅</p></td>
<td><p>[1]</p></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/天使帝國系列.md" title="wikilink">天使帝國4</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 中國動畫

| 播映年份 | 作品名稱                               | 配演角色   | 首播平台 | 備註 |
| ---- | ---------------------------------- | ------ | ---- | -- |
|      | [電擊小子](../Page/電擊小子.md "wikilink") | **小光** |      |    |
|      |                                    |        |      |    |

### 日本動畫

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>首播平台</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/天才小釣手.md" title="wikilink">天才小釣手</a></p></td>
<td><p><strong>三平三平</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/機械女神.md" title="wikilink">機械女神</a></p></td>
<td><p>美麗王二世</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/凱蒂貓的奇妙冒險.md" title="wikilink">凱蒂貓的奇妙冒險</a></p></td>
<td><p>KIKI</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/小公主優希.md" title="wikilink">小公主優希</a></p></td>
<td><p><strong>歌可露</strong></p></td>
<td><p><a href="../Page/卡通頻道.md" title="wikilink">卡通頻道</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大耳鼠.md" title="wikilink">大耳鼠</a></p></td>
<td><p>春日百合<br />
內木翔</p></td>
<td><p><a href="../Page/東森幼幼台.md" title="wikilink">東森幼幼台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/未來少年柯南.md" title="wikilink">未來少年柯南</a></p></td>
<td><p><strong>柯南</strong></p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/妮嘉尋親記.md" title="wikilink">明日之星娜佳</a></p></td>
<td><p>旁白<br />
老婆婆</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/東洋魔女勝利女排.md" title="wikilink">東洋魔女勝利女排</a></p></td>
<td><p>葉月優</p></td>
<td><p><a href="../Page/台視主頻.md" title="wikilink">台視</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/甜蜜戰士.md" title="wikilink">甜蜜戰士</a></p></td>
<td><p>夏子</p></td>
<td><p><a href="../Page/三立都會台.md" title="wikilink">三立都會台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/爆熱時空.md" title="wikilink">爆熱時空</a></p></td>
<td><p>史蘇利<br />
莎雷碧<br />
蜜樹</p></td>
<td><p><a href="../Page/八大綜合台.md" title="wikilink">八大綜合台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/麻辣教師GTO.md" title="wikilink">麻辣教師GTO</a></p></td>
<td><p><strong>冬月梓</strong><br />
相澤雅</p></td>
<td><p><a href="../Page/Channel_V.md" title="wikilink">Channel [V</a>]<br />
<a href="../Page/衛視電影台.md" title="wikilink">衛視電影台</a></p></td>
<td><p>[2]</p></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p><a href="../Page/新世紀福音戰士.md" title="wikilink">新世紀福音戰士</a></p></td>
<td><p><strong><a href="../Page/惣流·明日香·蘭格雷.md" title="wikilink">惣流·明日香·蘭格雷</a></strong><br />
伊吹摩耶</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p><a href="../Page/櫻桃小丸子.md" title="wikilink">櫻桃小丸子</a></p></td>
<td><p>櫻小竹<br />
野口笑子<br />
丸尾末男<br />
富田太郎<br />
山根強<br />
土橋年子<br />
關口慎志<br />
冬田美鈴<br />
小杉太<br />
西村高志</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td><p>[3]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/流星花園.md" title="wikilink">流星花園</a></p></td>
<td><p><strong>牧野杉菜</strong></p></td>
<td><p><a href="../Page/台視主頻.md" title="wikilink">台視</a></p></td>
<td><p>[4]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p><a href="../Page/小孩子的遊戲.md" title="wikilink">玩偶遊戲</a></p></td>
<td><p><strong>羽山秋人</strong><br />
<strong>巴比特</strong><br />
<strong>加村直澄</strong><br />
志村千代<br />
羽山小春<br />
熊谷久惠<br />
來海麻子<br />
安藤老師<br />
飛田真由<br />
西露·漢彌頓<br />
志津</p></td>
<td><p><a href="../Page/卡通頻道.md" title="wikilink">卡通頻道</a></p></td>
<td><p>[5]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中華一番.md" title="wikilink">中華一番</a></p></td>
<td><p><strong>劉昴星<small>（小當家）</small></strong></p></td>
<td><p><a href="../Page/台視主頻.md" title="wikilink">台視</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/湯姆歷險記_(動畫).md" title="wikilink">湯姆歷險記</a></p></td>
<td><p><strong>佩琪‧柴契爾</strong><br />
席德‧索耶<br />
美雅麗<br />
艾美‧羅倫斯<small>（前期）</small><br />
娜塔莉‧罗斯</p></td>
<td><p><a href="../Page/中都卡通台.md" title="wikilink">中都卡通台</a></p></td>
<td><p>[6]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/男女蹺蹺板.md" title="wikilink">男女蹺蹺板</a></p></td>
<td><p>宮澤花野</p></td>
<td><p><a href="../Page/中視數位台.md" title="wikilink">中視</a></p></td>
<td><p>[7]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/遊戲王_(動畫).md" title="wikilink">遊戲王</a></p></td>
<td><p><strong>寶兒<small>（真崎杏子）</small></strong></p></td>
<td><p><a href="../Page/三立都會台.md" title="wikilink">三立都會台</a></p></td>
<td><p>[8]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/反斗小王子.md" title="wikilink">丸少爺</a></p></td>
<td><p>小愛<br />
金仔<br />
黃臉<br />
傳話精靈</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/爆球連發！！超級彈珠人.md" title="wikilink">爆球連發–彈珠超人</a></p></td>
<td><p><strong>戶阪玉悟</strong><br />
中尊寺美樹</p></td>
<td><p><a href="../Page/台視主頻.md" title="wikilink">台視</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麗佳公主.md" title="wikilink">麗佳公主</a></p></td>
<td><p>篠原瑾<br />
八重<br />
千明修女<br />
普莉</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td><p>[9]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/棋靈王.md" title="wikilink">棋靈王</a></p></td>
<td><p><strong>進藤光</strong><br />
金子正子<br />
小池仁志</p></td>
<td><p><a href="../Page/華視主頻.md" title="wikilink">華視</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/笑園漫畫大王.md" title="wikilink">笑園漫畫大王</a></p></td>
<td><p>春日步<br />
神同學</p></td>
<td><p><a href="../Page/中視數位台.md" title="wikilink">中視</a></p></td>
<td><p>[10]</p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/元氣五胞胎.md" title="wikilink">元氣五胞胎</a></p></td>
<td><p><strong>森野強強</strong><br />
小玲<br />
小橘之母</p></td>
<td><p><a href="../Page/迪士尼頻道.md" title="wikilink">迪士尼頻道</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/原子小金剛.md" title="wikilink">原子小金剛</a></p></td>
<td><p>健一<br />
達夫</p></td>
<td><p><a href="../Page/卡通頻道.md" title="wikilink">卡通頻道</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/爆走兄弟.md" title="wikilink">爆走兄弟MAX</a></p></td>
<td><p><strong>一文字烈矢</strong><br />
大善一馬<br />
服部龍平<br />
正雄<br />
鷹羽二郎丸</p></td>
<td><p><a href="../Page/東森電影台.md" title="wikilink">東森電影台</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔法少年賈修.md" title="wikilink">魔法少年賈修</a></p></td>
<td><p><strong>蒂歐</strong>[11]<br />
水野鈴芽[12]<br />
可可[13]<br />
可魯魯<small>（重譯版）</small><br />
小蓮<br />
迪度</p></td>
<td><p><a href="../Page/台視主頻.md" title="wikilink">台視</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小公主莎拉.md" title="wikilink">小公主莎拉</a></p></td>
<td><p>維妮<small>（拉維妮亞‧賀伯特）</small><br />
亞美<small>（愛蜜莉亞‧明晴）</small><br />
彼得<br />
愛麗莎‧聖約翰<br />
瑪莎<br />
賀伯特夫人</p></td>
<td><p><a href="../Page/AXN.md" title="wikilink">AXN</a></p></td>
<td><p>[14]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/花田少年史.md" title="wikilink">花田少年史</a></p></td>
<td><p><strong>花田一路</strong><br />
春彥的母親</p></td>
<td><p><a href="../Page/八大綜合台.md" title="wikilink">八大綜合台</a></p></td>
<td><p>國語版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/彩夢芭蕾.md" title="wikilink">彩夢芭蕾</a></p></td>
<td><p>小比<br />
伊蝶爾<br />
旁白</p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/灼眼的夏娜.md" title="wikilink">灼眼的夏娜</a></p></td>
<td><p>阪井千草<small>（第二季）</small><br />
黑卡蒂<small>（第二季）</small><br />
平井緣<br />
緒方真竹<br />
中村公子<br />
瑪蒂達·桑特美爾<br />
瑪瓊林·朵<br />
蒂雅瑪特<br />
卡姆辛·奈夫哈維<br />
蒂麗亞<small>（愛染他）</small><br />
貝露佩歐露<br />
多米諾<br />
梅亞<small>（戲睡鄉）</small><br />
蘇菲·薩法利修</p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天使心_(漫畫).md" title="wikilink">天使心</a></p></td>
<td><p>槙村香<br />
香瑩</p></td>
<td><p><a href="../Page/中視數位台.md" title="wikilink">中視</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/嬉皮笑園.md" title="wikilink">嬉皮笑園</a></p></td>
<td><p><strong>橘玲</strong><br />
<strong>上原都</strong><br />
來-{栖}-柚子<br />
宮田晶<small>（前四集）</small><br />
柏木優麻<br />
綿貫響</p></td>
<td><p><a href="../Page/卡通頻道.md" title="wikilink">卡通頻道</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/偶像宣言.md" title="wikilink">花漾明星KIRARIN</a></p></td>
<td><p><strong>雲井佳澄</strong><br />
奶奶<br />
藤堂吹雪<br />
宙人的媽媽<br />
風真南雲<br />
霧澤葵<br />
南茜<small>（ep.39）</small><br />
美空晴子<br />
田中英子<br />
雨宮嵐<small>（幼年）</small></p></td>
<td><p><a href="../Page/MOMO親子台.md" title="wikilink">MOMO親子台</a></p></td>
<td><p>ep.1～52</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/爆丸.md" title="wikilink">爆丸</a></p></td>
<td><p><strong>空操彈馬</strong><br />
<strong>愛麗絲</strong><br />
神威鳳凰<br />
宋黎恩</p></td>
<td><p><a href="../Page/MOMO親子台.md" title="wikilink">MOMO親子台</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/涼風.md" title="wikilink">涼風</a></p></td>
<td><p><strong>朝比奈涼風</strong><br />
松本惠美<small>（ep.1）</small></p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/鬼太郎.md" title="wikilink">鬼太郎4</a></p></td>
<td><p><strong>撒沙婆婆</strong></p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
<td><p>[15]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魔法水果籃.md" title="wikilink">魔法水果籃</a></p></td>
<td><p><strong>草摩由希</strong><br />
草摩燈路<br />
老闆娘</p></td>
<td><p><a href="../Page/緯來綜合台.md" title="wikilink">緯來綜合台</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/鬼太郎.md" title="wikilink">鬼太郎5</a></p></td>
<td><p><strong>撒沙婆婆</strong><br />
女水泥牆<br />
河獺<br />
骨女</p></td>
<td><p><a href="../Page/台視主頻.md" title="wikilink">台視</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狼與辛香料.md" title="wikilink">狼與辛香料</a></p></td>
<td><p><strong>伊弗·波倫</strong></p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/星光少女_極光之夢.md" title="wikilink">星光少女 極光之夢</a></p></td>
<td><p>渡<br />
貝貝<br />
阿世知今日子<br />
城之內莎莉娜<br />
慧</p></td>
<td><p><a href="../Page/東森幼幼台.md" title="wikilink">東森幼幼台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/星光少女_美夢成真.md" title="wikilink">星光少女 美夢成真</a></p></td>
<td><p>時倫<br />
阿世知今日子<br />
城之內莎莉娜</p></td>
<td><p><a href="../Page/東森幼幼台.md" title="wikilink">東森幼幼台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/星光少女_彩虹舞台.md" title="wikilink">星光少女 彩虹舞台</a></p></td>
<td><p><strong>涼野糸</strong><br />
荊千里<small>（摩摩）</small></p></td>
<td><p><a href="../Page/東森幼幼台.md" title="wikilink">東森幼幼台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/網球王子_(動畫).md" title="wikilink">新網球王子OVA vs Genius10</a></p></td>
<td><p><strong>越前龍馬</strong><br />
丸井聞太<br />
遠野篤京</p></td>
<td><p><a href="../Page/東森電影台.md" title="wikilink">東森電影台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星光樂園.md" title="wikilink">星光樂園</a></p></td>
<td><p><strong>東堂詩音</strong><br />
大神田葛蘿麗亞<br />
櫪乙女愛</p></td>
<td><p><a href="../Page/東森幼幼台.md" title="wikilink">東森幼幼台</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 歐美動畫

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>首播平台</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/愛酷一族.md" title="wikilink">愛酷一族</a></p></td>
<td><p><strong>曼蒂</strong></p></td>
<td><p><a href="../Page/卡通頻道.md" title="wikilink">卡通頻道</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/:en:Chalkzone.md" title="wikilink">粉筆世界</a></p></td>
<td><p><strong>魯弟</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/少年超人隊.md" title="wikilink">少年超人隊</a></p></td>
<td><p><strong>土星女</strong><br />
分身女孩<br />
阿寶</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p><a href="../Page/藍精靈.md" title="wikilink">藍精靈</a></p></td>
<td><p>小迷人精<br />
小詩人</p></td>
<td><p><a href="../Page/卡通頻道.md" title="wikilink">卡通頻道</a></p></td>
<td><p>[16]</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/阿奇的異想世界.md" title="wikilink">阿奇的異想世界</a></p></td>
<td><p>貝蒂</p></td>
<td><p><a href="../Page/迪士尼頻道.md" title="wikilink">迪士尼頻道</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/魔法俏佳人.md" title="wikilink">魔法俏佳人</a></p></td>
<td><p><strong>鐵蘭</strong></p></td>
<td><p><a href="../Page/卡通頻道.md" title="wikilink">卡通頻道</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 韓國動畫

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>首播平台</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/魔法小迷狐.md" title="wikilink">魔法小迷狐</a></p></td>
<td><p><strong>小徹</strong><br />
小靜</p></td>
<td><p><a href="../Page/東森幼幼台.md" title="wikilink">東森幼幼台</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 日本動畫電影

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份不明</p></td>
<td><p><a href="../Page/劇場版美少女戰士R.md" title="wikilink">劇場版美少女戰士R</a></p></td>
<td><p><strong><a href="../Page/愛野美奈子.md" title="wikilink">愛野美奈子</a></strong><br />
<strong><a href="../Page/豆釘兔.md" title="wikilink">小小兔</a></strong></p></td>
<td><p>[17]</p></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p><a href="../Page/名偵探柯南_往天國的倒數計時.md" title="wikilink">名偵探柯南 往天國的倒數計時</a></p></td>
<td><p><strong><a href="../Page/灰原哀.md" title="wikilink">灰原哀</a></strong><br />
常磐美緒<br />
澤口知奈美多<br />
母亲<br />
纳圈售货员</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/名偵探柯南_貝克街的亡靈.md" title="wikilink">名偵探柯南 貝克街的亡靈</a></p></td>
<td><p><strong>灰原哀</strong><br />
諸星秀树<br />
<a href="../Page/艾琳·艾德勒.md" title="wikilink">艾琳·安多拉</a><br />
哈德森夫人<br />
美国电视新闻播报员</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>[[劇場版_鬼太郎_日本爆裂</p></td>
<td><p>|劇場版 鬼太郎 日本爆裂</p></td>
<td><p>]]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 歐美動畫電影

| 播映年份 | 作品名稱                                   | 配演角色   | 備註 |
| ---- | -------------------------------------- | ------ | -- |
|      | [天才小子吉米](../Page/天才小子吉米.md "wikilink") | **吉米** |    |
|      | [里約大冒險](../Page/里約大冒險.md "wikilink")   | 琳達     |    |
|      |                                        |        |    |

### 海外遊戲

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>遊戲名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/劍靈.md" title="wikilink">劍靈</a>（Blade &amp; Soul）</p></td>
<td><p><strong>秦瑟兒</strong><br />
劉家安<br />
銳思浪<br />
殷妝兒<br />
嚴柔<br />
羅絲綢<br />
伊美<br />
楚兒<br />
徐娉<br />
蓁美味<br />
張志華<br />
泰胡蘭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/爐石戰記：魔獸英雄傳.md" title="wikilink">-{zh-cn:炉石传说：魔兽英雄传;zh-tw:爐石戰記：魔獸英雄傳;}-</a></p></td>
<td><p><strong>-{zh-cn:瓦莉拉·萨古纳尔;zh-tw:瓦麗拉·桑古納爾;}-</strong>[18]<br />
納迦海巫</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

[X](../Category/台灣女性配音員.md "wikilink") [Y](../Category/許姓.md "wikilink")

1.  [《星界神話》封測於 6 月初正式展開 公布中文聲優陣容與 CB
    調整、開放內容](http://gnn.gamer.com.tw/7/115307.html)
2.  與[超視版本不同](../Page/超級電視台.md "wikilink")
3.  第二期前52集
4.  與[大地頻道版本不同](../Page/大地頻道.md "wikilink")
5.  與[MOMO親子台版本不同](../Page/MOMO親子台.md "wikilink")
6.  與[中視版本不同](../Page/中視數位台.md "wikilink")
7.  與[大地频道版本不同](../Page/大地频道.md "wikilink")
8.  與[華視版本不同](../Page/華視主頻.md "wikilink")
9.  與[華視版本不同](../Page/華視主頻.md "wikilink")
10. 與[卡通頻道版本不同](../Page/卡通頻道.md "wikilink")
11. 第53集起，包含重譯1-52集
12. 第53集起，包含重譯1-52集
13. 第53集起，包含重譯1-52集
14. 與[中視版本不同](../Page/中視數位台.md "wikilink")
15. 與[超視版本不同](../Page/超級電視台.md "wikilink")
16. 與[東森幼幼台版本不同](../Page/東森幼幼台.md "wikilink")
17. 早期版本，與[MOMO親子台版本不同](../Page/MOMO親子台.md "wikilink")
18. [《探險者協會》幕後配音特輯-許雲雲](https://www.youtube.com/watch?v=vZj37nND1Fw)