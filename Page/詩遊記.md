《**詩遊記**》（**Poetic
Journey**）是香港[亞洲電視一個](../Page/亞洲電視.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")，由2006年9月25日至12月1日星期一至星期五晚上09:00至09:30在[本港台播出](../Page/本港台.md "wikilink")。由[倪震主持](../Page/倪震_\(香港\).md "wikilink")。

節目介紹[中國古代](../Page/中國.md "wikilink")[詩人和近代](../Page/詩人.md "wikilink")[中國文學](../Page/中國文學.md "wikilink")[作家](../Page/作家.md "wikilink")、其生平經歷、寫作背景、創作時候的心景、其名作等。並由兩位[香港](../Page/香港.md "wikilink")[大專畢業生重遊古人曾經到過的名山勝地](../Page/大學.md "wikilink")，感受文人當年的創作靈感。

在2008年5月，逢星期六早上10:30（後改為早上9:30），在亞視本港台重播。

## 詩人

《詩遊記》電視節目，大約每兩晚介紹一位[中國古代及近代](../Page/中國.md "wikilink")[文學家](../Page/文學家.md "wikilink")，包括：

### 古代詩人

1.  大文豪 [蘇軾](../Page/蘇軾.md "wikilink")
2.  詩仙 [李白](../Page/李白.md "wikilink")
3.  詩聖 [杜甫](../Page/杜甫.md "wikilink")
4.  大文學家 [柳宗元](../Page/柳宗元.md "wikilink")
5.  田園詩人 [陶淵明](../Page/陶淵明.md "wikilink")
6.  香山居士 [白居易](../Page/白居易.md "wikilink")
7.  [唐宋八大家之一的](../Page/唐宋八大家.md "wikilink")
    [歐陽修](../Page/歐陽修.md "wikilink")
8.  [北宋](../Page/北宋.md "wikilink")[變法的](../Page/王安石變法.md "wikilink")
    [王安石](../Page/王安石.md "wikilink")
9.  詩佛 [王維](../Page/王維.md "wikilink")
10. [宋代女](../Page/宋代.md "wikilink")[詞人](../Page/詞人.md "wikilink")
    [李清照](../Page/李清照.md "wikilink")
11. [晚唐](../Page/晚唐.md "wikilink")[詩人](../Page/詩.md "wikilink")
    [李商隱](../Page/李商隱.md "wikilink")
12. [南宋詞人](../Page/南宋.md "wikilink") [辛棄疾](../Page/辛棄疾.md "wikilink")
13. 主張「文以載道」的 [韓愈](../Page/韓愈.md "wikilink")

### 近代詩人

1.  [徐志摩](../Page/徐志摩.md "wikilink")
2.  [魯迅](../Page/魯迅.md "wikilink")
3.  [聞一多](../Page/聞一多.md "wikilink")
4.  [朱自清](../Page/朱自清.md "wikilink")

## 每週遊記

  - 第1集至第5集：[蘇軾](../Page/蘇軾.md "wikilink")（[林偉豪](../Page/林偉豪.md "wikilink")（Calvin）
    和[何傲兒](../Page/何傲兒.md "wikilink")）
  - 第6集至第10集：[李白](../Page/李白.md "wikilink")（陳嘉勵（Carol）和[姜紫藍](../Page/姜紫藍.md "wikilink")（Color））
  - 第11集至第15集：[白居易](../Page/白居易.md "wikilink")（陳靈晟（Roy）和梁嘉麗（嘉麗））
  - 第16集至第18集：[杜甫](../Page/杜甫.md "wikilink")（吳啟揚（Louis）和蕭麗芳）
  - 第19集至第20集：[柳宗元](../Page/柳宗元.md "wikilink")（Louis和蕭麗芳）
  - 第21集至第22集：[歐陽修](../Page/歐陽修.md "wikilink")（Calvin和Color）
  - 第23集至第24集：[王安石](../Page/王安石.md "wikilink")（Calvin和Color）
  - 第25集至第26集：[陶淵明](../Page/陶淵明.md "wikilink")（Calvin和Color）
  - 第27集至第29集：[王維](../Page/王維.md "wikilink")（Carol和黃鈺華（華女））
  - 第30集至第32集：[李清照](../Page/李清照.md "wikilink")（Carol和華女）
  - 第33集至第36集：[李商隱](../Page/李商隱.md "wikilink")（陳靈晟（Roy）和蕭麗芳）
  - 第37集至第38集：[辛棄疾](../Page/辛棄疾.md "wikilink")（Roy和蕭麗芳）
  - 第39集至第40集：[韓愈](../Page/韓愈.md "wikilink")（嘉麗和Louis）
  - 第41集至第42集：[徐志摩](../Page/徐志摩.md "wikilink")（嘉麗和何傲兒）
  - 第43集至第45集；[魯迅](../Page/魯迅.md "wikilink")（嘉麗和何傲兒）
  - 第46集至第48集：[聞一多](../Page/聞一多.md "wikilink")（Carol和董玥）
  - 第49集至第50集：[朱自清](../Page/朱自清.md "wikilink")（Carol和靜慧博）

## 相關

  - [亞洲電視節目列表](../Page/亞洲電視節目列表.md "wikilink")
  - [何傲兒](../Page/何傲兒.md "wikilink")（現為[無綫電視藝員](../Page/電視廣播有限公司.md "wikilink")）

## 外部連線

  - 《詩遊記》[亞洲電視台官方網址](https://web.archive.org/web/20120207144354/http://www.hkatv.com/infoprogram/06/poetry/)

[Category:亞洲電視節目](../Category/亞洲電視節目.md "wikilink")