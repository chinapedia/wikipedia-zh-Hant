**該猶**，3世纪初教会作家，罗马教会的[司铎](../Page/司铎.md "wikilink")，[該撒利亞的優西比烏认为他活躍於](../Page/該撒利亞的優西比烏.md "wikilink")201年至219年年间，约[教宗](../Page/教宗.md "wikilink")[聖宰斐琳在位期间](../Page/聖宰斐琳.md "wikilink")（199年－217年）。

優西比烏在其《[教会史](../Page/教会史.md "wikilink")》中曾四次引证（II. 25, III. 28, 31, VI.
20）該猶的一篇對話錄，記載的是[聖宰斐琳任](../Page/聖宰斐琳.md "wikilink")[羅馬主教期间](../Page/羅馬主教.md "wikilink")，該猶與[孟他奴主義者](../Page/孟他奴主義.md "wikilink")[普罗克洛斯](../Page/普罗克洛斯.md "wikilink")（Proclus）之间的辩论。[居比路的狄奥多莱](../Page/居比路的狄奥多莱.md "wikilink")（*Hæreticarum
Fabularum Compendium*, II, 3, III,
2）、[耶柔米](../Page/耶柔米.md "wikilink")（*De viris
illustribus*, 59）和[尼賽福祿](../Page/尼賽福祿.md "wikilink")（*Historia
Ecclesia*, IV, 12,
20）也都援引優西比烏对該猶的记载。[阜丢斯](../Page/阜丢斯.md "wikilink")（*Bibliotheca*,
codex 48）除了對話錄以外，還提到該猶的另三部作品．

在優西比烏所保存的殘片中，該猶向論敵指出（II,
25）[梵蒂岡和](../Page/梵蒂岡.md "wikilink")[俄斯替亚道有](../Page/俄斯替亚道.md "wikilink")“建立[教會根基之](../Page/教會.md "wikilink")[使徒們](../Page/使徒.md "wikilink")”（即使徒[保罗与](../Page/保罗.md "wikilink")[彼得](../Page/彼得.md "wikilink")）之墓，而普罗克洛斯和該猶咸認為（III.
xxxi）[腓力與他的四個女兒葬在](../Page/腓力.md "wikilink")[希拉波立](../Page/希拉波立.md "wikilink")。該猶（VI,
20）“只提到保羅的十三封書信”，而未將《[希伯來書](../Page/希伯來書.md "wikilink")》包括其中。穆拉多利認為同樣未包括《[希伯來書](../Page/希伯來書.md "wikilink")》的[穆拉多利斷片的作者是該猶](../Page/穆拉多利斷片.md "wikilink")，因為其結尾拒絕了孟他奴派的作品。當優西比烏論及[異端](../Page/異端.md "wikilink")[克林妥](../Page/克林妥.md "wikilink")（III.
xxviii）時，節錄該猶的著作說“克林妥謊稱自己擁有一位偉大使徒所寫的啟示，並擁有[天使向他展示的奇物](../Page/天使.md "wikilink")。克林妥還宣稱在復活後，地上將有一個[基督的國](../Page/基督.md "wikilink")，那時住在[耶路撒冷的人會落入慾望和享樂當中](../Page/耶路撒冷.md "wikilink")。此人敵對《[聖經](../Page/聖經.md "wikilink")》，矇騙世人，還說會有為時一千年的婚筵。”\[1\]有觀點認為此處指的是列入[正典的](../Page/正典.md "wikilink")[啟示錄](../Page/啟示錄.md "wikilink")，[亞歷山太的狄尼修](../Page/亞歷山太的狄尼修.md "wikilink")\[2\]引证了与該猶类似的观点，并指明是[約翰的啟示錄](../Page/約翰.md "wikilink")。

Gwynn 发表了[希坡律陀的](../Page/希坡律陀.md "wikilink") *Capita contra Caium*
残片\[3\]，见于 Cod. Mus. Brit. Orient. 560，表明該猶認為約翰的啟示錄與《聖經》的並不一致。

## 参考文献

## 外部链接

  - [優西比烏《教會史》](http://ekklesiahistory.fttt.org.tw/big5/index.html)

  - [Gaius of Rome, a complicated
    case.](http://www.textexcavation.com/gaiusrome.html)

[Category:神學家](../Category/神學家.md "wikilink")

1.  Eusebius, *H. E.*, III, 28.
    [1](http://ekklesiahistory.fttt.org.tw/big5/book03/chapter28.htm)
2.  Eusebius, *H. E.*, VII, 25.
    [2](http://ekklesiahistory.fttt.org.tw/big5/book07/chapter25.htm)
3.  *Hermathena*, VI, p. 397
    [3](http://www.tertullian.org/fathers/dionysius_syrus_revelation_01.htm)