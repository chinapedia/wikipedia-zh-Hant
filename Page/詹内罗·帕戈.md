**詹内罗·帕戈**（，），生于[伊利诺伊州](../Page/伊利诺伊州.md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")，美国职业篮球运动员，司职[控球后卫](../Page/控球后卫.md "wikilink")。\[1\]帕戈在加盟黄蜂队之前先后效力过[洛杉矶湖人](../Page/洛杉矶湖人.md "wikilink")，[多伦多猛龙](../Page/多伦多猛龙.md "wikilink")，和[芝加哥公牛](../Page/芝加哥公牛.md "wikilink")。
在大学期间他就读于[阿肯色大学](../Page/阿肯色大学.md "wikilink")。

帕戈以及时的三分射手而著称，在公牛队时在球队担任着重要的角色，在2005年NBA季候赛首轮面对[华盛顿奇才队时](../Page/华盛顿奇才队.md "wikilink")，他在球队打平后5秒内出手3分命中，在一分钟的时间内为公牛队夺得10分，3分球3投3中，只是球队最后2分惜败在[吉尔伯特·阿里纳斯的压哨跳头上](../Page/吉尔伯特·阿里纳斯.md "wikilink")。

2006赛季帕戈转会到新奥尔良黄蜂，在黄蜂队帕戈也屡次有单场10多分甚至20多分的上佳表现。

他的弟弟杰瑞米·帕戈（Jeremy Pargo）是[冈再伽大学篮球队的后卫](../Page/冈再伽大学.md "wikilink")。

## 参考资料

## 外部链接

  - [NBA.com Profile - Jannero
    Pargo](http://www.nba.com/playerfile/jannero_pargo/)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:洛杉磯湖人隊球員](../Category/洛杉磯湖人隊球員.md "wikilink")
[Category:多伦多猛龙队球员](../Category/多伦多猛龙队球员.md "wikilink")
[Category:芝加哥公牛隊球員](../Category/芝加哥公牛隊球員.md "wikilink")
[Category:新奥尔良黄蜂队球员](../Category/新奥尔良黄蜂队球员.md "wikilink")

1.  <http://sports.espn.go.com/nba/news/story?id=2538962>