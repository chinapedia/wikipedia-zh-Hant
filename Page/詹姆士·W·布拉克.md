**詹姆士·懷特·布拉克**爵士，[OM](../Page/功績勳章_\(英國\).md "wikilink")，[FRS](../Page/英國皇家學會.md "wikilink")，[FRSE](../Page/愛丁堡皇家學會.md "wikilink")，[FRCP](../Page/英國倫敦皇家內科醫學院.md "wikilink")（，），[蘇格蘭](../Page/蘇格蘭.md "wikilink")[藥理學家](../Page/藥理學.md "wikilink")，發明藥物[Propranolol和合成出](../Page/普萘洛尔.md "wikilink")[Cimetidine](../Page/甲氰咪胍.md "wikilink")。他因這些成就而在1988年獲頒贈[諾貝爾醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")\[1\]。

## 生平

布拉克早年就讀於[蘇格蘭](../Page/蘇格蘭.md "wikilink")[法夫](../Page/法夫.md "wikilink")（Fife）[考登比思](../Page/考登比思.md "wikilink")（Cowdenbeath）的[比思](../Page/比思.md "wikilink")[高中](../Page/高中.md "wikilink")，其後在法夫的[聖安德魯斯大學修讀](../Page/聖安德魯斯大學.md "wikilink")[藥理學](../Page/藥理學.md "wikilink")，並曾經在[丹地](../Page/丹地.md "wikilink")（Dundee）作實習。他在[馬來亞大學取得教席前](../Page/馬來亞大學.md "wikilink")，曾加入聖安德魯斯大學的生理學系。布拉克在1950年回到蘇格蘭，加入[格拉斯哥大學的獸醫學院](../Page/格拉斯哥大學.md "wikilink")，並為該校成立生理學院。除了活躍於學術界外，布拉克曾先後獲[帝國化學工業](../Page/帝國化學工業.md "wikilink")（1958年至1964年）、[葛蘭素史克](../Page/葛蘭素史克.md "wikilink")（1964年至1973年）和[惠康基金會](../Page/惠康基金會.md "wikilink")（1978年至1984年）聘用。另外，他又獲[倫敦大學學院](../Page/倫敦大學學院.md "wikilink")（1973年至1978年）和[倫敦國王學院](../Page/倫敦國王學院.md "wikilink")（1984年至1992年）聘用為藥理學教授。在1976年，他當選為[皇家學會會員](../Page/英國皇家學會.md "wikilink")，同年獲頒[拉斯克獎](../Page/拉斯克獎.md "wikilink")\[2\]。

布拉克在1981年獲冊立為[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")。在2000年，布拉克爵士獲[英女皇](../Page/英女皇.md "wikilink")[伊莉莎伯二世頒贈](../Page/伊莉莎伯二世.md "wikilink")[功績勳章](../Page/功績勳章_\(英國\).md "wikilink")，該勳章只有24個名額，是英國君主所能授予的最高級榮譽。

布拉克爵士在科學界甚有貢獻，當中尤以[心臟病學為最](../Page/心臟病學.md "wikilink")。他發明的[β受體阻斷藥](../Page/Β受体阻断药.md "wikilink")[Propranolol](../Page/普萘洛尔.md "wikilink")，對治療[心絞痛起革命性的影響](../Page/心絞痛.md "wikilink")，並被認為是20世紀對[藥理學起最大貢獻的藥物之一](../Page/藥理學.md "wikilink")，他憑這些貢獻在1988年獲頒贈[諾貝爾獎](../Page/諾貝爾獎.md "wikilink")\[3\]\[4\]。他上述的的研究完全是憑他一人之功勞\[5\]。

自1992年至2006年5月，布拉克爵士出任[丹地大學的](../Page/丹地大學.md "wikilink")[校監](../Page/大學校監.md "wikilink")。在2006年8月，屬於該校生命科學學院的詹姆士·布拉克爵士中心落成啟用。

## 請參見

  - [格特魯德·B·埃利恩](../Page/格特魯德·B·埃利恩.md "wikilink")（1988年[諾貝爾醫學獎得獎者](../Page/諾貝爾生理學或醫學獎.md "wikilink")）
  - [喬治·H·希欽斯](../Page/喬治·H·希欽斯.md "wikilink")（1988年[諾貝爾醫學獎得獎者](../Page/諾貝爾生理學或醫學獎.md "wikilink")）

## 參考資料

<div class=
"references-small">

<references/>

</div>

## 連結

[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:沃爾夫醫學獎得主](../Category/沃爾夫醫學獎得主.md "wikilink")
[Category:聖安德魯斯大學校友](../Category/聖安德魯斯大學校友.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")
[Category:拉斯克临床医学研究奖获得者](../Category/拉斯克临床医学研究奖获得者.md "wikilink")

1.
2.
3.
4.
5.