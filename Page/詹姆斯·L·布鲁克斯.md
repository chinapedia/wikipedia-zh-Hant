**詹姆斯**·**L**·**布鲁克斯**（James L.
Brooks，）是一位[美国](../Page/美国.md "wikilink")[制片人](../Page/执行制片人.md "wikilink")、[剧作家与](../Page/剧作家.md "wikilink")[电影导演](../Page/电影导演.md "wikilink")。他曾获得过3次[奥斯卡奖与](../Page/奥斯卡奖.md "wikilink")20次[艾美奖](../Page/艾美奖.md "wikilink")；同时，他还是[金球奖的持有者](../Page/金球奖.md "wikilink")。

詹姆斯的作品有许多，其中最著名的美国[电视节目有](../Page/电视节目.md "wikilink")《[玛丽·泰勒·穆尔秀](../Page/玛丽·泰勒·穆尔秀.md "wikilink")
》《[辛普森一家](../Page/辛普森一家.md "wikilink")》（他在其中创造了各种各样的角色，其中包括整个[布维尔家族](../Page/布维尔家族.md "wikilink")）《[罗达](../Page/罗达.md "wikilink")
》与《[出租车](../Page/出租车_\(电视剧\).md "wikilink")
》等。他最著名的电影是1983年的《[母女情深](../Page/母女情深_\(1983年電影\).md "wikilink")》，这部电影让他在1984年获得了三项[奥斯卡奖](../Page/奥斯卡奖.md "wikilink")。

## 外部链接

  -
  - [Gracie Films Website](http://www.graciefilms.com/)

[Category:奧斯卡最佳導演獎獲獎者](../Category/奧斯卡最佳導演獎獲獎者.md "wikilink")
[B](../Category/黃金時段艾美獎獲獎者.md "wikilink")
[Category:美国电影导演](../Category/美国电影导演.md "wikilink")
[Category:美国电视剧导演](../Category/美国电视剧导演.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:猶太作家](../Category/猶太作家.md "wikilink")
[Category:猶太導演](../Category/猶太導演.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")