**詹姆斯·斯库克拉夫特·舍曼**（**James Schoolcraft
Sherman**，），[美国政治家](../Page/美国.md "wikilink")，[美国共和党成员](../Page/美国共和党.md "wikilink")，曾任第27任[美国副总统](../Page/美国副总统.md "wikilink")。

[S](../Category/1855年出生.md "wikilink")
[S](../Category/1912年逝世.md "wikilink")
[S](../Category/美国副总统.md "wikilink")
[Category:1912年美国副总统候选人](../Category/1912年美国副总统候选人.md "wikilink")
[Category:共和党美国副总统](../Category/共和党美国副总统.md "wikilink")
[Category:纽约州律师](../Category/纽约州律师.md "wikilink")