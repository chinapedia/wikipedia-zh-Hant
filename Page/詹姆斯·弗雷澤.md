[JamesGeorgeFrazer.jpg](https://zh.wikipedia.org/wiki/File:JamesGeorgeFrazer.jpg "fig:JamesGeorgeFrazer.jpg")
**詹姆斯·乔治·弗雷泽**爵士，[OM](../Page/功績勳章_\(英國\).md "wikilink")（，），出生于[苏格兰](../Page/苏格兰.md "wikilink")[格拉斯哥](../Page/格拉斯哥.md "wikilink")，社会[人类学家](../Page/人类学家.md "wikilink")、[神话学和](../Page/神话学.md "wikilink")[比较宗教学的先驱](../Page/比较宗教学.md "wikilink")。曾在[格拉斯哥大学和](../Page/格拉斯哥大学.md "wikilink")[剑桥大学三一学院学习](../Page/剑桥大学三一学院.md "wikilink")。弗雷泽在1914年获封[爵士](../Page/爵士.md "wikilink")。由于长期劳作，1930年之后，他的[视力大为下降](../Page/视力.md "wikilink")。

弗雷泽的主要研究领域包括[神话和](../Page/神话.md "wikilink")[宗教](../Page/宗教.md "wikilink")。除了[意大利和](../Page/意大利.md "wikilink")[希腊](../Page/希腊.md "wikilink")，他游历不广。他进行研究工作的主要来源是浩如烟海的史料文献，以及来自世界各地的调查表。弗雷泽在人类学上的启蒙者是人类学开创者[爱德华·伯内特·泰勒](../Page/爱德华·伯内特·泰勒.md "wikilink")，以及泰勒的名著《[原始文化](../Page/原始文化_\(书\).md "wikilink")》（1871）。弗雷泽一生的研究尽在《[金枝](../Page/金枝:_巫术与宗教之研究.md "wikilink")》（*The
Golden Bough*）一书，第一版出版于1890年，两卷。1915年第三版出版的时候，已经扩充到十二卷。

## 基本著作年表

  - 《[图腾信仰](../Page/图腾信仰.md "wikilink")》（Totemism，1887）
  - 《[金枝: 巫术与宗教之研究](../Page/金枝:_巫术与宗教之研究.md "wikilink")》（The Golden
    Bough: a Study in Magic and Religion）第一版（1890）
  - 《希腊述描》，[保薩尼亞斯原著](../Page/保薩尼亞斯_\(地理學家\).md "wikilink")，弗雷泽翻译与注释（1897）
  - 《金枝》，第二版（1900）
  - 《灵魂之工作》（Psyche's Task，1909）
  - 《图腾信仰与异族通婚》（Totemism and Exogamy，1910）
  - 《金枝》，第三版（1906-15; 1936）
  - 《信仰不朽与死亡崇拜》（The Belief in Immortality and the Worship of the
    Dead），3卷（1913-24）
  - 《古遗嘱中的民间传说》（Folk-lore in the Old Testament，1918）
  - 《阿波罗多罗斯：书库》（Apollodorus: the Library，1921）
  - 《自然崇拜》（The Worship of Nature，1926）
  - 《蛇怪之首与若干文学片断》（The Gorgon's Head and other Literary Pieces，1927）
  - 《人、神、不朽》（Man, God, and Immortality，1927）
  - 《[纪年表](../Page/纪年表.md "wikilink")》（Fasti），[奥维德原著](../Page/奥维德.md "wikilink")，弗雷泽翻译（1929）
  - 《火源神话》（Myths of the Origin of Fire，1930）
  - 《柏拉图理念论的发展》（The Growth of Plato's Ideal Theory，1930）
  - 《如烟一束》（Garnered Sheaves，1931）
  - 《人类思想进步中的孔多赛》（Condorcet on the Progress of the Human Mind，1933）
  - 《原始宗教中的死亡恐惧》（The Fear of the Dead in Primitive Religion，1933-36）
  - 《原始宇宙进化论的创造与发展及其他》（Creation and Evolution in Primitive Cosmogenies,
    and Other Pieces，1935）

[F](../Category/英国人类学家.md "wikilink")
[F](../Category/1854年出生.md "wikilink")
[F](../Category/1941年逝世.md "wikilink")
[F](../Category/功績勳章成員.md "wikilink")
[F](../Category/下級勳位爵士.md "wikilink")
[Category:英國國家學術院院士](../Category/英國國家學術院院士.md "wikilink")
[Category:格拉斯哥大學校友](../Category/格拉斯哥大學校友.md "wikilink")
[Category:劍橋大學三一學院校友](../Category/劍橋大學三一學院校友.md "wikilink")