**詹姆斯·威廉·怀特四世**（，），出生于[华盛顿特区](../Page/华盛顿特区.md "wikilink")，[美国职业](../Page/美国.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，司职[得分後衛](../Page/得分後衛.md "wikilink")，亦能頂上[小前鋒的位置](../Page/小前鋒.md "wikilink")。在[2006年NBA选秀中于第二轮第](../Page/2006年NBA选秀.md "wikilink")31顺位被[波特兰开拓者队选中](../Page/波特兰开拓者队.md "wikilink")，随即被交换去[印第安纳步行者队](../Page/印第安纳步行者队.md "wikilink")，但未能得到合同。后怀特转而效力于[NBA](../Page/NBA.md "wikilink")[圣安东尼奥马刺队](../Page/圣安东尼奥马刺队.md "wikilink")，但未能获得多少表现机会，于2007年夏季被马刺解雇。2009年[休斯顿火箭队与其签订了](../Page/休斯顿火箭队.md "wikilink")10天的短合同，大學時代以[灌籃聞名](../Page/灌籃.md "wikilink")，有「飛行機器」之稱，目前是唯二能够罚球线起跳胯下换手扣篮，和罚球线外一步起跳扣篮的选手。
[youtube網站有很多關於他灌籃的影片](../Page/youtube.md "wikilink")。 另一位是查克·拉文

## 参考资料

## 外部链接

  - [NBA.com
    profile](http://www.nba.com/historical/playerfile/index.html?player=james_white)

  - [Player profile on
    euroleague.net](http://www.euroleague.net/competition/players/showplayer?clubcode=ulk&pcode=THC)

  -
[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:圣安东尼奥马刺队球员](../Category/圣安东尼奥马刺队球员.md "wikilink")
[Category:休斯頓火箭隊球員](../Category/休斯頓火箭隊球員.md "wikilink")