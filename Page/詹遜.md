**詹遜**爵士，[KCMG](../Page/KCMG.md "wikilink")，[KStJ](../Page/聖約翰美德勳章.md "wikilink")（Sir
**Franklin Charles
Gimson**，）一譯**金遜**爵士，[英國殖民地官員](../Page/英國殖民地.md "wikilink")，曾先後擔任[香港輔政司和](../Page/香港輔政司.md "wikilink")[星加坡總督等職](../Page/星加坡總督.md "wikilink")。

## 生平

### 早年生涯

詹遜1890年9月10日出生於[英格蘭](../Page/英格蘭.md "wikilink")[列斯特郡](../Page/列斯特郡.md "wikilink")，父親名叫C·K·詹遜（C.
K.
Gimson），是一位[牧師](../Page/牧師.md "wikilink")。詹遜早年入讀[牛津大學](../Page/牛津大學.md "wikilink")[貝利奧爾學院](../Page/牛津大學貝利奧爾學院.md "wikilink")，並取得到[文學士學位](../Page/文學士.md "wikilink")。

### 殖民地生涯

詹遜在1914年前往英屬[錫蘭](../Page/錫蘭.md "wikilink")，加入當地的殖民地政府，任職[見習官](../Page/見習官.md "wikilink")（Cadet），一直至1937年累遷為錫蘭的勞工署署長（Controller
of
Labour）。後來，詹遜被轉派到[香港政府工作](../Page/港英政府.md "wikilink")，並在1941年12月7日抵港履任[輔政司一職](../Page/輔政司.md "wikilink")，不料到翌日12月8日，[日軍從](../Page/日軍.md "wikilink")[深圳進攻香港](../Page/深圳.md "wikilink")，[香港保衛戰一觸即發](../Page/香港保衛戰.md "wikilink")，期間駐港英軍節節敗退。到了12月25日[聖誕節](../Page/聖誕節.md "wikilink")，[總督](../Page/香港總督.md "wikilink")[楊慕琦](../Page/楊慕琦.md "wikilink")[爵士在](../Page/爵士.md "wikilink")[半島酒店正式投降](../Page/半島酒店.md "wikilink")，楊慕琦與其他英方軍政要員遂一同淪為[戰俘](../Page/戰俘.md "wikilink")，並被關押在今日的[赤柱監獄](../Page/赤柱監獄.md "wikilink")。

香港淪陷與楊慕琦投降以後，詹遜成為了前政府的代表，並一度在[太子行設置辦公室](../Page/太子大廈.md "wikilink")，主理英方戰敗以後的善後事宜，所以詹遜要到1942年3月才正式被關押往赤柱監獄。在囚禁赤柱期間，英籍戰俘成立了一個「英國共同委員會」（British
Communal
Council），有限度地對戰俘營的運作和秩序作有限度的管治。由於當時楊慕琦已被轉介到[瀋陽的戰俘營](../Page/瀋陽.md "wikilink")，所以詹遜成為了戰俘營內最高級別的前政府官員，並獲委任為委員會的主席，負責與日方進行交涉事宜。

[Gimson_and_Harcourt.jpg](https://zh.wikipedia.org/wiki/File:Gimson_and_Harcourt.jpg "fig:Gimson_and_Harcourt.jpg")的海軍少將[夏慤](../Page/夏慤.md "wikilink")（左）會面。\]\]
經歷過三年零八個月的淪陷歲月後，日本終於在1945年8月15日宣佈[無條件投降](../Page/無條件投降.md "wikilink")。日本投降以後的一段短時間，由於無人接管香港，所以日本政府仍作出有限度的管治，但以往日治時期的種種限制禁令即一一取消。在日本準備投降的前夕，身在[重慶的](../Page/重慶.md "wikilink")[英國駐華大使](../Page/英國駐華大使.md "wikilink")爵士早已設法與詹遜聯絡，著他儘早設立臨時政府，代理英國行使主權。薛穆的指示要到8月23日方才傳到詹遜處。但詹遜早於8月16日得悉日本投降後，便宣稱自己為「署理總督」，並著手與日方交涉成立「臨時政府」。詹遜後來於23日收到薛穆的指示後，遂與一眾英國戰俘離開[戰俘營](../Page/集中營.md "wikilink")，並從日治政府接過管治權，在[法國外方傳道會大樓正式設立](../Page/法國外方傳道會大樓.md "wikilink")「臨時政府」總部，不過由於當時詹遜並沒有掌握任何實權，所以即使「臨時政府」已經成立，香港仍十分依賴日軍維持秩序。到8月27日，詹遜又透過[電台通知香港市民](../Page/電台.md "wikilink")，宣告「臨時政府」已經成立。

「臨時政府」成立以後不久，[海軍少將](../Page/英國海軍.md "wikilink")[夏慤在](../Page/夏慤.md "wikilink")1945年8月30日率領等艦抵達香港的[維多利亞港](../Page/維多利亞港.md "wikilink")，詹遜曾向夏慤要求讓「臨時政府」繼續運作，但由於夏慤乃奉命來港設立臨時軍政府，所以軍政府仍舊於9月1日宣告成立，並由夏慤本人出任軍政府首長。可是，當時軍政府的主要人員尚未抵港，所以詹遜仍然繼續作出管治。但有鑑於詹遜的權力基本上是來自1917年的《[英皇制誥](../Page/英皇制誥.md "wikilink")》，並不符合軍政府的情況，所以夏慤在軍政府成立的同一日發表了一項宣告，委任詹遜為[副總督](../Page/香港副總督.md "wikilink")（Lieutenant-Governor），可以代理行使以往總督的權力，不過卻要聽命於夏慤。然而，對委任詹遜為副總督一事上不表贊同，故此當軍政府的首席民政事務官（Chief
Civil Affairs
Officer）[麥道高與他的九名隨員在](../Page/麥道高.md "wikilink")9月7日正式抵港運作後，詹遜隨即卸任副總督。

與其他曾被囚禁的戰俘一樣，詹遜在戰後也出現了營養不良的毛病，因此他在卸任後於9月16日返回[英國休養](../Page/英國.md "wikilink")。在返回英國前一日，即9月15日，詹遜曾在電台發表講話，表示香港在未來應就政治制度進行[改革](../Page/改革.md "wikilink")。

### 星加坡總督

1946年，詹遜獲委任為[星加坡總督兼三軍總司令](../Page/星加坡總督.md "wikilink")，並在同年4月1日履新，取消[蒙巴頓勳爵設置的臨時軍政府](../Page/路易斯·蒙巴頓.md "wikilink")，復設文官政府。他上任總督初期，由於經濟在戰後遲未恢復，以致1947年的時候曾發生過大規模的騷亂。在1947年7月，星加坡脫離[海峽殖民地](../Page/海峽殖民地.md "wikilink")，成為獨立的皇家[殖民地](../Page/殖民地.md "wikilink")。詹遜於是成立了[立法局和](../Page/新加坡立法局.md "wikilink")[行政局](../Page/新加坡行政局.md "wikilink")，至1948年3月，他又容許立法局內25個議席中的6個由[選舉產生](../Page/選舉.md "wikilink")。

到1948年6月，[馬來亞共產黨發動](../Page/馬來亞共產黨.md "wikilink")[游擊戰](../Page/游擊戰.md "wikilink")，馬來亞殖民地政府隨即宣佈進入[緊急狀態](../Page/馬來亞緊急狀態.md "wikilink")。詹遜立即對星加坡的[左翼團體加以打壓](../Page/左翼.md "wikilink")，並通過極具爭議性的《內部安全法案》，容許未經審訊，就可以無限期拘留「對安全構成威脅」的人士。此外，詹遜任內又極力限制[星加坡華人接觸](../Page/新加坡華人.md "wikilink")「[新中國](../Page/新中國.md "wikilink")」。當中，星加坡華商[陳嘉庚在](../Page/陳嘉庚.md "wikilink")1950年企圖從中國大陸返回星加坡的時候，就被拒絕入境。

詹遜於1951年舉行了第2次立法局選舉，今次經選舉產生的議席由6席增至9席。但仍為不少人批評不夠開放。其實，星加坡市民在[第二次世界大戰以後](../Page/第二次世界大戰.md "wikilink")，對英國政權曾為日本打敗而大大失去了以往的信心，所以在詹遜任內，反殖、[自治以至](../Page/自治.md "wikilink")[獨立的呼聲已經不斷醞釀](../Page/獨立.md "wikilink")。詹遜在1952年11月15日卸任星加坡總督。

### 晚年生涯

詹遜卸任星加坡總督後退出殖民地服務，返回英國開展退休生活，1975年2月13日卒於[北約克郡](../Page/北約克郡.md "wikilink")[皮卡令](../Page/皮卡令_\(北約克郡\).md "wikilink")（Pickering）寓所內，終年84歲。[1](http://www.london-gazette.co.uk/issues/46517/pages/3604)

## 家庭

詹遜在1922年與瑪格麗特·桃樂絲·華德，[MBE](../Page/MBE.md "wikilink")（Margaret Dorothy
Ward）結婚，瑪格麗特的父親名叫卡農·華德（Canon Ward）。而詹遜兩夫婦共育有兩名女兒。

## 榮譽

  - [C.M.G.](../Page/CMG.md "wikilink") （1945年）
  - [K.C.M.G.](../Page/KCMG.md "wikilink") （1946年）
  - [K.St.J.](../Page/聖約翰美德勳章.md "wikilink")

### 其他

  - [馬來亞大學榮譽法律博士學位](../Page/馬來亞大學.md "wikilink") （1952年）
  - [皇家聯邦會會員](../Page/皇家聯邦會.md "wikilink")
  - 星加坡城[榮譽市民](../Page/榮譽市民.md "wikilink")

## 參見

  - [香港保衛戰](../Page/香港保衛戰.md "wikilink")
  - [香港重光](../Page/香港重光.md "wikilink")
  - [新加坡歷史](../Page/新加坡歷史.md "wikilink")

## 參考資料

<div class="references-small">

  - *Who's Who*，A & C Black，1969年。
  - *Fragrant harbour : a short history of Hong Kong*，G. B. Endacott and
    A. Hinton，[香港](../Page/香港.md "wikilink")：牛津大學出版社，1962年。
  - *Hong Kong Eclipse*，G. B.
    Endacott，[香港](../Page/香港.md "wikilink")：牛津大學出版社，1978年。
  - <http://www.worldstatesmen.org/Singapore.html>，*Singapore*，World
    Statemen。
  - <http://countrystudies.us/singapore/9.htm>，*Singapore - Aftermath of
    War*，[美國國會圖書館](../Page/美國國會圖書館.md "wikilink")，2006年6月18日。
  - <https://web.archive.org/web/20070929194719/http://59.42.251.241:9010/kt0445019/article/1178/632922913497968750.aspx>，*抗战胜利后中英香港受降权之争*，李铜玉，2006年8月27日。

</div>

[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:英國政治人物](../Category/英國政治人物.md "wikilink")
[Category:牛津大学贝利奥尔学院校友](../Category/牛津大学贝利奥尔学院校友.md "wikilink")
[Category:第二次世界大战日本拘禁的平民战俘](../Category/第二次世界大战日本拘禁的平民战俘.md "wikilink")
[Category:香港第二次世界大战人物](../Category/香港第二次世界大战人物.md "wikilink")