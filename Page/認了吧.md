《**認了吧**》為[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[陳奕迅的](../Page/陳奕迅.md "wikilink")[國語](../Page/國語.md "wikilink")[音樂專輯](../Page/音樂.md "wikilink")，於2007年4月24日正式發行。

陳奕迅憑《**認了吧**》專輯，成功第4度入圍台灣[第19屆金曲獎](../Page/第19屆金曲獎.md "wikilink")[最佳國語男歌手](../Page/最佳國語男歌手獎_\(金曲獎\).md "wikilink")。更進入到決選最後一輪，最後以2票之差敗給[曹格](../Page/曹格.md "wikilink")《Super
Sunshine》，有評審在討論中指出陳奕迅在專輯中唱法較先前作品保守穩當。

## 專輯簡介

本專輯收錄專輯內共有10首歌曲，包括兩首全新國語歌：《淘汰》和《快樂男生》。其餘8首皆是改編自2006年的[粵語專輯](../Page/粵語.md "wikilink")《[What's
Going
On...?](../Page/What's_Going_On...?.md "wikilink")》，分別如下：《煙味》改編自《裙下之臣》、《紅玫瑰》改編自《白玫瑰》、《月黑風高》改編自《黑擇明》、《愛情轉移》改編自《富士山下》、《好久不見》改編自《不如不見》、《白色球鞋》改編自《粵語殘片》，另外兩首重新編曲，使得改編自《心深傷透》的《愛是一本書》成為R\&B的曲風；改編自《最後的嬉皮士》的《第一個雅皮士》成為一首Big
Band Song。

香港版專輯還收錄了一首[叱咤903長篇](../Page/叱咤903.md "wikilink")[廣播劇](../Page/廣播劇.md "wikilink")「[黃金少年](../Page/黃金少年.md "wikilink")
Season III」的粵語插曲《月球上的人》。

## 曲目

| 次序   | 歌名     | 作曲                                                         | 填詞                               | 編曲                                                | 監製                                                                                                              | 粵語版本   |
| ---- | ------ | ---------------------------------------------------------- | -------------------------------- | ------------------------------------------------- | --------------------------------------------------------------------------------------------------------------- | ------ |
| 1\.  | 煙味     | [Alex San](../Page/Alex_San.md "wikilink")                 | [方文山](../Page/方文山.md "wikilink") | Alex San                                          | [Davy Chan](../Page/Davy_Chan.md "wikilink") / [C.Y. Kong](../Page/C.Y._Kong.md "wikilink") / 陳奕迅               | 裙下之臣   |
| 2\.  | 淘汰     | [周杰倫](../Page/周杰倫.md "wikilink")                           | 周杰倫                              | C.Y. Kong                                         | Davy Chan / C.Y. Kong / 陳奕迅                                                                                     |        |
| 3\.  | 快樂男生   | C.Y. Kong                                                  | [易家揚](../Page/易家揚.md "wikilink") | C.Y. Kong                                         | [梁榮駿](../Page/梁榮駿.md "wikilink") / C.Y. Kong / Davy Chan / [Stanley Leung](../Page/Stanley_Leung.md "wikilink") |        |
| 4\.  | 紅玫瑰    | 梁翹柏                                                        | [李焯雄](../Page/李焯雄.md "wikilink") | 梁翹柏                                               | 梁翹柏                                                                                                             | 白玫瑰    |
| 5\.  | 月黑風高   | C.Y. Kong                                                  | [林夕](../Page/林夕.md "wikilink")   | C.Y. Kong                                         | 梁榮駿 / C.Y. Kong                                                                                                 | 黑擇明    |
| 6\.  | 愛情轉移   | [Christopher Chak](../Page/Christopher_Chak.md "wikilink") | 林夕                               | [陳珀](../Page/陳珀.md "wikilink") / C.Y. Kong        | 梁榮駿                                                                                                             | 富士山下   |
| 7\.  | 好久不見   | [陳小霞](../Page/陳小霞.md "wikilink")                           | [施立](../Page/施立.md "wikilink")   | [孫偉明](../Page/孫偉明.md "wikilink") / C.Y. Kong / 陳珀 | Davy Chan / C.Y. Kong / 陳奕迅                                                                                     | 不如不見   |
| 8\.  | 愛是一本書  | Davy Chan                                                  | 易家揚                              | Davy Chan                                         | Davy Chan / C.Y. Kong / 陳奕迅                                                                                     | 心深傷透   |
| 9\.  | 第一個雅皮士 | [梁翹柏](../Page/梁翹柏.md "wikilink")                           | [周耀輝](../Page/周耀輝.md "wikilink") | [Ted Lo](../Page/Ted_Lo.md "wikilink")            | 梁翹柏                                                                                                             | 最後的嬉皮士 |
| 10\. | 白色球鞋   | C.Y. Kong / 陳奕迅                                            | [姚謙](../Page/姚謙.md "wikilink")   | C.Y. Kong                                         | Davy Chan / C.Y. Kong / 陳奕迅                                                                                     | 粵語殘片   |
| 11\. | 月球上的人  | [蘇耀宗](../Page/蘇耀宗.md "wikilink")                           | [林若寧](../Page/林若寧.md "wikilink") | [王菀之](../Page/王菀之.md "wikilink")                  | 蘇耀宗                                                                                                             |        |
|      |        |                                                            |                                  |                                                   |                                                                                                                 |        |

### 音樂錄像

| 曲目次序 | 歌名    |
| ---- | ----- |
| 02   | 淘汰    |
| 03   | 快樂男生  |
| 06   | 愛情轉移  |
| 07   | 好久不見  |
| 08   | 愛是一本書 |
|      |       |

## 大眾迴響及銷售情況

在[台灣](../Page/台灣.md "wikilink")，本專輯曾登上[G-Music 風雲榜
(華語榜)共十三週](../Page/玫瑰大眾唱片.md "wikilink")，最高排行第3位。

## 相關獎項或提名

### 專輯《認了吧》

  - 香港：2007年度[新城國語力頒獎禮](../Page/新城國語力頒獎禮.md "wikilink")－新城國語力優秀專輯
  - 香港：[IFPI香港唱片銷量大獎](../Page/IFPI.md "wikilink")2007－十大銷量國語唱片
  - 台灣：第十九屆金曲獎－[最佳國語男歌手獎](../Page/最佳國語男歌手獎_\(金曲獎\).md "wikilink")（提名）
  - 中國：[北京](../Page/北京.md "wikilink")[2008年度音樂風雲榜頒獎盛典](../Page/2008年度音樂風雲榜頒獎盛典得獎名單.md "wikilink")－（港台）最佳專輯

### 歌曲《淘汰》

  - 香港：[2007年度十大勁歌金曲頒獎典禮](../Page/2007年度十大勁歌金曲得獎名單.md "wikilink")－最受歡迎華語歌曲獎銀獎
  - 香港：2007年度[TVB8金曲頒獎典禮](../Page/TVB8金曲頒獎典禮.md "wikilink")－十大金曲獎
  - 香港：2007年度TVB8金曲頒獎典禮－最佳監製

### 歌曲《愛情轉移》

  - 台灣：[2007中華音樂人交流協會](../Page/中華音樂人交流協會.md "wikilink")－年度十大單曲
  - 中國：2007年度中國移動無線音樂排行榜－最暢銷影視金曲獎 (下載7217538次)
  - 中國：[第八屆華語音樂傳媒大獎](../Page/華語音樂傳媒大獎.md "wikilink")－年度國語歌曲
  - 中國：2008年度音樂風雲榜頒獎盛典－（港台）年度最佳歌曲
  - 中國：2008年度音樂風雲榜頒獎盛典－（港台）年度最佳作詞
  - 中國：2008年度音樂風雲榜頒獎盛典－（港台）年度最佳作曲

### 歌曲《好久不見》

  - 日本：[2008年MTV日本音樂錄影帶大獎](../Page/2008年MTV日本音樂錄影帶大獎.md "wikilink")－最佳BuzzAsia獎（提名）

## 外部連結

  - [認了吧 - 環球唱片專頁](http://www.umg.com.hk/minisite/eason_admit_it_v2/)
  - [G-Music 風雲榜 (華語榜)](http://www.g-music.com.tw/GMusicBillboard1.aspx)

[Category:陳奕迅音樂專輯](../Category/陳奕迅音樂專輯.md "wikilink")
[Category:陳奕迅國語專輯](../Category/陳奕迅國語專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")