**認領**係指生父承認[非婚生子女為自己親生](../Page/非婚生子女.md "wikilink")[子女者](../Page/子女.md "wikilink")，被認領之非婚生子女即因認領取得其生父之[婚生子女之法律地位](../Page/婚生子女.md "wikilink")。由於生母與子女之間可藉[分娩之事實確定真實血緣聯絡](../Page/分娩.md "wikilink")，故子女毋須經生母認領即發生法律上母子關係，而生父與非婚生子女間則無法藉由分娩之事實確定真實血緣聯絡，故法律上特設認領制度使非婚生子女得藉此取得生父之婚生子女之法律地位。又認領依是否得依生父之意思為之可分為**任意認領**及**強制認領**二者，如未特別言之，認領一詞一般均指前者而言。

## 認領之法律性質

關於認領之法律性質，素有爭議，採取理論之不同亦將影響無真實血緣聯絡時之認領效力問題：

  - 意思表示說

此說係[台灣學界通說所採](../Page/台灣.md "wikilink")，認為認領行為係屬[法律行為之一種](../Page/法律行為.md "wikilink")，且為[單獨行為](../Page/單獨行為.md "wikilink")，毋須被認領人或生母之同意，乃生父承認該非婚生子女為自己子女之[意思表示](../Page/意思表示.md "wikilink")，一經生父為之即生該非婚生子女取得婚生子女地位之法律上效力。惟發生此項效力仍以認領人與被認領人間有真實血緣聯絡為必要，如無則無從發生認領之效力。

  - 觀念通知說

此說為當代先進國家主流立法例所採，認為認領僅係生父向非婚生子女表示其已認識到自己與該非婚生子女間有血緣聯絡之事實而已。至於認領之效力則係因法律規定而發生，並非基於生父之意思。此說亦以兩者間具真實血緣聯絡為必要。

  - 折衷說

此說為台灣學者[戴東雄所提出](../Page/戴東雄.md "wikilink")，係以子女最佳利益原則出發，認為認領效力之發生不以真實血緣聯絡為必要，僅認領人為認領之表示（其認為此表示係意思表示之特別型態），法律上即先[推定發生認領之效力](../Page/推定.md "wikilink")，使該非婚生子女可先得認領人之保護教養。而如認領人與被認領人間實無真實血緣聯絡，可依否認認領（[民法
(中華民國)第](../Page/台灣民法.md "wikilink")1066條）或撤銷認領（同法第1070）之方式救濟。

## 認領之要件

一般認為認領須符合下列要件方為合法：

  - 須生父本人為之，此係因[身分行為不許](../Page/身分行為.md "wikilink")[代理之故](../Page/代理.md "wikilink")，且生父須具備意思能力。\[1\]故未成年人不須徥法定代理人之同意，而若受[監護宣告但已回復正常狀態](../Page/監護宣告.md "wikilink")，兩者皆具有意思能力，亦可單獨為認領。\[2\]
  - 被認領者須為非[婚生子女](../Page/婚生子女.md "wikilink")，若為他人之受[婚生推定之子女](../Page/婚生推定.md "wikilink")，於遭提起否認之訴確定前，仍具有法律上之親子關係，從而不許另有他人再對之為認領。
  - 胎兒是否為認領的對象，中華民國民法中未有規定，有學者認為[胎兒亦得為認領之對象](../Page/胎兒.md "wikilink")）。\[3\]

日本民法783條第1項，以及民法1594條第4項則為保護胎兒之利益而肯定。而若認領的對象已死亡，是否有認領之效果，中華民國民法也未有規定，日本民法第783條第2項則規定，若已死亡之子女有直系血親卑親屬，則可對之認領。但若直系血親卑親屬已成年，須得到其同意\[4\]。

  - 須具備真實血緣連繫，否則可能被起訴確認，而將致不實的親子關係遭到推翻而自始無效。

但不少國外的立法例，則尚要求認領必須為要式行為方具備效力，如法國民法、瑞士民法、日本民法，及韓國民法\[5\]。

## 擬制認領

依台灣民法第1065條第1項後段之規定，非婚生子女經生父撫育者視為認領，此即為**擬制認領**。此之撫育係指負擔生活費用而言，且台灣實務見解認為以預付方式亦得為之。

## 強制認領

若生父本人不為認領，則子女或其生母或其[法定代理人可以向法院起訴](../Page/法定代理人.md "wikilink")，請求強制認領，亦有簡稱之為**請求認領**，\[6\]。若生父已死亡，前述有權提起之人，得向生父之[繼承人起訴請求](../Page/繼承人.md "wikilink")，如果生父無[繼承人者](../Page/繼承人.md "wikilink")，也可以向社會福利主管機關為之。\[7\]

## 認領之法律效果

1.  經生父認領之子女，視為其[婚生子女](../Page/婚生子女.md "wikilink")，取得準[婚生子女之身分](../Page/婚生子女.md "wikilink")，其權利與義務，均與[婚生子女相同](../Page/婚生子女.md "wikilink")。
2.  認領之效力，溯及於出生時即發生，但法律往往為保護第三人之既得權，故明定，第三人已取得之權利，不因此而受影響。

## 參考文獻

[Category:親子法](../Category/親子法.md "wikilink")
[Category:法律術語](../Category/法律術語.md "wikilink")
[Category:家事法](../Category/家事法.md "wikilink")
[Category:親屬關係](../Category/親屬關係.md "wikilink")

1.  林秀雄，《親屬法講義》，頁239\~241，2011年7月，ISBN 978-957-41-8156-8

2.  戴炎輝、戴東雄、戴瑀如合著，《親屬法》，頁347，20141年8月

3.
4.
5.

6.  民法第1067條第一項，有事實足認其為非婚生子女之生父者，非婚生子女或其生母或其他法定代理人，得向生父提起認領之訴。

7.  民法第1067條第二項