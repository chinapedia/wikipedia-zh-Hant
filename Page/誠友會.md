四代目**誠友會**、位於[北海道](../Page/北海道.md "wikilink")
[札幌市](../Page/札幌市.md "wikilink")[中央區南](../Page/中央區_\(札幌市\).md "wikilink")7条西6-2-1本部的設置，[日本的](../Page/日本.md "wikilink")[指定暴力團之一](../Page/指定暴力團.md "wikilink")・六代目[山口組的次團體](../Page/山口組.md "wikilink")。正式與非正式成員約500人。

誠友會前身為[柳川組北海道支部](../Page/柳川組.md "wikilink")。

## 历史

1962年12月
[柳川組組長](../Page/柳川組.md "wikilink")・[柳川次郎](../Page/柳川次郎.md "wikilink")（三代目[山口組若眾](../Page/山口組.md "wikilink")）與長岡、石間2人交盃（兄弟盃），長岡就任[柳川組北海道支部初代支部長](../Page/柳川組.md "wikilink")。

1965年11月
長岡引退，[石間春夫就任柳川組北海道支部第](../Page/石間春夫.md "wikilink")2代支部長。在石間服役期間[田村武志就任柳川組北海道支部第](../Page/田村武志.md "wikilink")3代支部長。

1969年4月9日
柳川組解散，原[柳川組北海道支部結成](../Page/柳川組.md "wikilink")「北海道誠友會」、[田村武志任會長](../Page/田村武志.md "wikilink")，石間就任相談役；後來石間退出「北海道誠友會」另創[北誠會](../Page/北誠會.md "wikilink")。

1977年
北誠會與北海道誠友會合併為[誠友會](../Page/誠友會.md "wikilink")、[石間春夫就任總長](../Page/石間春夫.md "wikilink")。
1985年4月 石間加入四代目[山口組任舍弟](../Page/山口組.md "wikilink")。

1990年1月4日
[石間春夫](../Page/石間春夫.md "wikilink")（59歳）在札幌被三代目[共政會](../Page/共政會.md "wikilink")
島上組
稲田組系的右翼團體「維新天誅會」成員射殺身亡。此後代理總長・[田村武志](../Page/田村武志.md "wikilink")2代目繼承，並昇格為五代目[山口組直参](../Page/山口組.md "wikilink")。

2001年10月18日
[田村武志肝病去世](../Page/田村武志.md "wikilink")。同年，若頭・[船木一治](../Page/船木一治.md "wikilink")3代目継承，在12月昇格為五代目[山口組直参](../Page/山口組.md "wikilink")。

2005年8月27日
舎弟頭・[茶谷政雄](../Page/茶谷政雄.md "wikilink")（[茶谷政一家總長](../Page/茶谷政一家.md "wikilink")）被升格為六代目[山口組直参](../Page/山口組.md "wikilink")，脫離誠友會。

2013年3月30日，山口組「幹部」船木一治（三代目誠友會會長）於[札幌市的醫院病逝](../Page/札幌市.md "wikilink")\[1\]。四代目組長繼承人不明；組織暫時納入山口組其他2次團體傘下。

2013年5月，四代目組長繼承人・渡部 隆升格為六代目山口組直系組長\[2\]。

## 歴代組長

  - 初代（1977年～1990年）：[石間春夫](../Page/石間春夫.md "wikilink")（五代目山口組舍弟）
  - 二代目（～2001年10月18日）：[田村武志](../Page/田村武志.md "wikilink")（五代目山口組若中）
  - 三代目（2001年～2013年）：[船木一治](../Page/船木一治.md "wikilink")（六代目山口組若中）
  - 四代目（2013年～）：渡部 隆\[3\]

## 最高幹部

  - 會長・渡部 隆
  - 若頭・關谷弘志（山勝會會長）
  - 最高顧問・田村一夫（二代目田村組組長）
  - 顧問・高橋哲雄（山闘連合總長）
  - 舍弟頭・高橋清一（二代目西村會會長）
  - 本部長・兼松義幸（二代目渡部會會長）

## 旗下團體（山口組的4次團體）

  - 高山組 [中谷組](../Page/中谷組_\(高山組\).md "wikilink")
  - 高山組 [濱野組](../Page/濱野組.md "wikilink")
  - 高山組 [前田組](../Page/前田組.md "wikilink")
  - 山元組 [田中組](../Page/田中組_\(山元組\).md "wikilink")
  - 山元組 [須見健組](../Page/須見健組.md "wikilink")
  - 小仲組 [作田組](../Page/作田組.md "wikilink")
  - 小仲組 [菊地組](../Page/菊地組.md "wikilink")
  - 高橋組 [室井組](../Page/室井組.md "wikilink")
  - 高橋組 [中山組](../Page/中山組_\(高橋組\).md "wikilink")
  - 菊心會 太田組
  - 照井組 [北照會](../Page/北照會.md "wikilink")

## 資料來源

[Category:誠友會](../Category/誠友會.md "wikilink") [Category:中央區
(札幌市)](../Category/中央區_\(札幌市\).md "wikilink")

1.
2.
3.