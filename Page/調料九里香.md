**調料九里香**（[学名](../Page/学名.md "wikilink")：**）又名**麻绞叶**、**咖哩葉**、**咖哩樹**、**可因氏月橘**\[1\]，是[芸香科](../Page/芸香科.md "wikilink")[月橘屬的一種](../Page/月橘屬.md "wikilink")[植物](../Page/植物.md "wikilink")。原產於[印度](../Page/印度.md "wikilink")，分佈於[熱帶與](../Page/熱帶.md "wikilink")[亞熱帶地區](../Page/亞熱帶.md "wikilink")。以其葉子有[咖哩的味道可做](../Page/咖哩.md "wikilink")[香料使用而得名](../Page/香料.md "wikilink")。

植株為小[喬木](../Page/喬木.md "wikilink")，可以生長至四至六[公尺高](../Page/公尺.md "wikilink")，樹幹直徑可達四十[公分粗](../Page/公分.md "wikilink")。[葉為羽狀複葉](../Page/葉.md "wikilink")，有小葉11-21片，小葉長2-4公分，寬1-2公分。[花小](../Page/花.md "wikilink")，白色，有香味。[果實為小](../Page/果實.md "wikilink")[漿果](../Page/漿果.md "wikilink")，黑色。果實可以食用，不過裡面的[種子具有毒性](../Page/種子.md "wikilink")。

## 用途

本种葉子具有強烈的香味，可做香辛料使用，在[印度及](../Page/印度.md "wikilink")[斯里蘭卡的料理裡](../Page/斯里蘭卡.md "wikilink")，特別是在做咖哩料理時，常會加入這種葉子來當做[調味料使用](../Page/調味料.md "wikilink")。新鮮的葉子保存期限很短，若是存放在[冰箱內](../Page/冰箱.md "wikilink")，則可以保存一個星期左右。市面上也可以買到乾燥的葉子，不過乾品的香味比較不香。

## 参考文献

[灌](../Category/藥用植物.md "wikilink")
[koenigii](../Category/九里香属.md "wikilink")
[Category:調味料](../Category/調味料.md "wikilink")

1.