**諫早市**（）是位於[日本](../Page/日本.md "wikilink")[長崎縣中央的](../Page/長崎縣.md "wikilink")[城市](../Page/城市.md "wikilink")，人口為長崎縣內人口第三多的。近年來該市面臨市街地空洞化的危機。

轄區內正在進行的為日本正在進行的大型[围垦工程之一](../Page/围垦.md "wikilink")。

## 地理

轄區北部為[多良山系的山區](../Page/多良山系.md "wikilink")，西側為[長崎半島](../Page/長崎半島.md "wikilink")，南側為[島原半島](../Page/島原半島.md "wikilink")，西北部臨[大村灣](../Page/大村灣.md "wikilink")，東側臨[諫早灣](../Page/諫早灣.md "wikilink")，南臨[橘灣](../Page/橘灣.md "wikilink")，共有三面為海。

  - 河川 - [本明川](../Page/本明川.md "wikilink")、長田川、半造川、東大川
  - 山 - 五家原岳
  - 湾 - [諫早灣](../Page/諫早灣.md "wikilink")
  - 平野 - 諫早平野

### 相鄰的自治体

  - 長崎縣
      - [長崎市](../Page/長崎市.md "wikilink")
      - [大村市](../Page/大村市.md "wikilink")
      - [雲仙市](../Page/雲仙市.md "wikilink")
      - [西彼杵郡](../Page/西彼杵郡.md "wikilink")：[長与町](../Page/長与町.md "wikilink")
  - [佐賀縣](../Page/佐賀縣.md "wikilink")
      - [藤津郡](../Page/藤津郡.md "wikilink")：[太良町](../Page/太良町.md "wikilink")

## 歷史

[鎌倉時代被稱為](../Page/鎌倉時代.md "wikilink")「伊佐早」，16世紀[龍造寺氏將此地更名為](../Page/龍造寺氏.md "wikilink")「諫早」，並設為「[佐賀藩諫早領](../Page/佐賀藩.md "wikilink")」，諫早領的範圍包括現在的諫早市、長崎市和佐賀縣的部分地區。\[1\]

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [北高來郡](../Page/北高來郡.md "wikilink")：**諫早町**、北諫早村、諫早村、小栗村、小野村、有喜村、真津山村、本野村、長田村、森山村、江之浦村、田結村、湯江村、小江村、深海村、小長井村。
      - [西彼杵郡](../Page/西彼杵郡.md "wikilink")：伊木力村、大草村、喜喜津村。
  - 1923年4月1日：諫早町、北諫早村、諫早村[合併為新設置的](../Page/市町村合併.md "wikilink")**諫早町**。
  - 1940年9月1日：諫早町、小栗村、小野村、有喜村、真津山村、本野村、長田村合併為**諫早市**。
  - 1940年11月3日：湯江村改制為[湯江町](../Page/湯江町.md "wikilink")。
  - 1955年2月11日：
      - 江之浦村和田結村合併為飯盛村。
      - 伊木力村、大草村、喜喜津村合併為多良見村。
  - 1956年9月20日：湯江町、小江村、深海村合併為[高來町](../Page/高來町.md "wikilink")。
  - 1965年4月1日：飯盛村改制為[飯盛町](../Page/飯盛町.md "wikilink")。
  - 1965年11月23日：多良見村改制為[多良見町](../Page/多良見町.md "wikilink")。
  - 1966年11月1日：小長井村改制為[小長井町](../Page/小長井町.md "wikilink")。
  - 1969年4月1日：森山村改制為[森山町](../Page/森山町.md "wikilink")。
  - 2005年3月1日：諫早市、飯盛町、森山町、高來町、小長井町、多良見町合併為新設置的**諫早市**。\[2\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1959年</p></th>
<th><p>1960年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>諫早町</p></td>
<td><p>1923年4月1日<br />
諫早町</p></td>
<td><p>1940年9月1日<br />
諫早市</p></td>
<td><p>2005年3月1日<br />
諫早市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>諫早村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>北諫早村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>小栗村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>本野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>真津山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>有喜村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>小野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>長田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>田結村</p></td>
<td><p>1955年2月11日<br />
飯盛村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>江之浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>森山村</p></td>
<td><p>1969年4月1日<br />
森山町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>湯江村</p></td>
<td><p>1940年11月3日<br />
湯江町</p></td>
<td><p>1956年9月20日<br />
高來町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>小江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>深海村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>小長井村</p></td>
<td><p>1966年11月1日<br />
小長井町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>喜喜津村</p></td>
<td><p>1955年2月11日<br />
多良見村</p></td>
<td><p>1965年11月23日<br />
多良見町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大草村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>伊木力村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [長崎本線](../Page/長崎本線.md "wikilink")：[小長井車站](../Page/小長井車站.md "wikilink")
        - [長里車站](../Page/長里車站.md "wikilink") -
        [湯江車站](../Page/湯江車站.md "wikilink") -
        [小江車站](../Page/小江車站.md "wikilink") -
        [肥前長田車站](../Page/肥前長田車站.md "wikilink") -
        [東諫早車站](../Page/東諫早車站.md "wikilink") -
        [諫早車站](../Page/諫早車站.md "wikilink") -
        [西諫早車站](../Page/西諫早車站.md "wikilink") -
        [喜喜津車站](../Page/喜喜津車站.md "wikilink") -
        [市布車站](../Page/市布車站.md "wikilink")
      - 長崎本線（舊線）：喜喜津車站 - [東園車站](../Page/東園車站_\(日本\).md "wikilink") -
        [大草車站](../Page/大草車站.md "wikilink")
      - [大村線](../Page/大村線.md "wikilink")：諫早車站
  - [島原鐵道](../Page/島原鐵道.md "wikilink")
      - [島原鐵道線](../Page/島原鐵道線.md "wikilink")：諫早車站 -
        [本諫早車站](../Page/本諫早車站.md "wikilink") -
        [幸車站](../Page/幸車站.md "wikilink") -
        [小野本町車站](../Page/小野本町車站.md "wikilink") -
        [干拓之里車站](../Page/干拓之里車站.md "wikilink") -
        [森山車站](../Page/森山車站.md "wikilink") -
        [釜之鼻車站](../Page/釜之鼻車站.md "wikilink") -
        [諫早東高校前車站](../Page/諫早東高校前車站.md "wikilink")

|                                                                                                                                                         |                                                                                                                                                         |                                                                                                                                                |
| ------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| [IsahayaStation.jpg](https://zh.wikipedia.org/wiki/File:IsahayaStation.jpg "fig:IsahayaStation.jpg")                                                    | [Yue_Station_20080402.jpg](https://zh.wikipedia.org/wiki/File:Yue_Station_20080402.jpg "fig:Yue_Station_20080402.jpg")                                | [JRKyushu_Oe_Station_1.jpg](https://zh.wikipedia.org/wiki/File:JRKyushu_Oe_Station_1.jpg "fig:JRKyushu_Oe_Station_1.jpg")                   |
| [JRKyushu_HizenNagata_Station_1.jpg](https://zh.wikipedia.org/wiki/File:JRKyushu_HizenNagata_Station_1.jpg "fig:JRKyushu_HizenNagata_Station_1.jpg") | [JRKyushu_Konagai_Station_1.jpg](https://zh.wikipedia.org/wiki/File:JRKyushu_Konagai_Station_1.jpg "fig:JRKyushu_Konagai_Station_1.jpg")             | [JRKyushu_Ichinuno_Station_1.jpg](https://zh.wikipedia.org/wiki/File:JRKyushu_Ichinuno_Station_1.jpg "fig:JRKyushu_Ichinuno_Station_1.jpg") |
| [JRKyushu_Kikitsu_Station.jpg](https://zh.wikipedia.org/wiki/File:JRKyushu_Kikitsu_Station.jpg "fig:JRKyushu_Kikitsu_Station.jpg")                    | [JRKyushu_Higashisono_Station_1.jpg](https://zh.wikipedia.org/wiki/File:JRKyushu_Higashisono_Station_1.jpg "fig:JRKyushu_Higashisono_Station_1.jpg") | [JRKyushu_Okusa_Station_1.jpg](https://zh.wikipedia.org/wiki/File:JRKyushu_Okusa_Station_1.jpg "fig:JRKyushu_Okusa_Station_1.jpg")          |

### 道路

  - 高速道路

<!-- end list -->

  - [長崎自動車道](../Page/長崎自動車道.md "wikilink")：[諫早交流道](../Page/諫早交流道.md "wikilink")

## 觀光資源

### 景點

  - [諫早陣屋](../Page/諫早陣屋.md "wikilink")
  - [眼鏡橋](../Page/眼鏡橋_\(諫早市\).md "wikilink")
  - 轟溪谷：[名水百選之一](../Page/名水百選.md "wikilink")
  - 富川溪谷
  - 白木峰高原

### 祭典、活動

  - 諫早杜鵑花祭：4月
  - 諫早、川祭：7月25日，為過去諫早發生重大水災之日期
  - のんのこ諫早祭：9月

## 相關文化作品

### 小說

  - 蝶々さん（作者：[市川森一](../Page/市川森一.md "wikilink")）
  - 諫早菖蒲日記　草のつるぎ　など（作者：[野呂邦暢](../Page/野呂邦暢.md "wikilink")）

### 電視劇

  - [親戚たち](../Page/親戚たち.md "wikilink")（編劇：[市川森一](../Page/市川森一.md "wikilink")、主演：[役所廣司](../Page/役所廣司.md "wikilink")）

### 舞台劇

  - 泥人魚：（\[劇本：\[唐十郎\]\]）

## 教育

### 大學

  - [長崎衛斯理大學](../Page/長崎衛斯理大學.md "wikilink")

### 高等學校

<table width=100%>

<tr valign=top>

<td width=50%>

  - 公立

<!-- end list -->

  - 長崎縣立諫早高等學校
      - 高來分校
  - 長崎縣立西陵高等學校
  - 長崎縣立諫早東高等學校
  - 長崎縣立諫早農業高等學校
  - 長崎縣立諫早商業高等學校

</td>

<td>

  - 私立

<!-- end list -->

  - 長崎日本大學中學校、高等學校
  - 創成館高等學校
  - 鎮西學院高等學校

</td>

</tr>

</table>

### 特殊學校

  - 長崎縣立諫早養護學校
  - 長崎縣立諫早東養護學校
  - 長崎縣立希望丘高等養護學校
  - 長崎縣立虹之原養護みさかえ分校

## 姊妹、友好都市

### 日本

  - [出雲市](../Page/出雲市.md "wikilink")
    （[島根縣](../Page/島根縣.md "wikilink")）：於1981年7月28日共同簽訂友好交流都市協定。
  - [津山市](../Page/津山市.md "wikilink")
    （[岡山縣](../Page/岡山縣.md "wikilink")）：於1981年7月28日共同簽訂友好交流都市協定。

### 海外

  - [雅典](../Page/雅典_\(田納西州\).md "wikilink")（[美國](../Page/美國.md "wikilink")[田納西州](../Page/田納西州.md "wikilink")）

  - [漳州市](../Page/漳州市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")[福建省](../Page/福建省.md "wikilink")）

## 本地出身之名人

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/蒼井のぞみ.md" title="wikilink">蒼井のぞみ</a>：<a href="../Page/俳優.md" title="wikilink">俳優</a></li>
<li><a href="../Page/伊東靜雄.md" title="wikilink">伊東靜雄</a>：<a href="../Page/詩人.md" title="wikilink">詩人</a></li>
<li><a href="../Page/市川森一.md" title="wikilink">市川森一</a>：<a href="../Page/編劇.md" title="wikilink">編劇</a></li>
<li><a href="../Page/內村航平.md" title="wikilink">內村航平</a>：<a href="../Page/體操.md" title="wikilink">體操選手</a></li>
<li><a href="../Page/梅崎司.md" title="wikilink">梅崎司</a>：<a href="../Page/職業足球.md" title="wikilink">職業足球選手</a></li>
<li><a href="../Page/大城英司.md" title="wikilink">大城英司</a>：俳優</li>
<li><a href="../Page/貝塚政秀.md" title="wikilink">貝塚政秀</a>：<a href="../Page/職業棒球.md" title="wikilink">職業棒球選手</a></li>
<li><a href="../Page/垣根涼介.md" title="wikilink">垣根涼介</a>：<a href="../Page/小説家.md" title="wikilink">小説家</a></li>
<li><a href="../Page/草場道輝.md" title="wikilink">草場道輝</a>：<a href="../Page/漫畫家.md" title="wikilink">漫畫家</a></li>
<li><a href="../Page/坪田真理子.md" title="wikilink">坪田真理子</a>：前<a href="../Page/歌手.md" title="wikilink">歌手</a></li>
<li><a href="../Page/嗣子鵬慶昌.md" title="wikilink">嗣子鵬慶昌</a>：前<a href="../Page/相撲力士.md" title="wikilink">相撲力士</a></li>
<li><a href="../Page/津山ちなみ.md" title="wikilink">津山ちなみ</a>：漫畫家</li>
<li><a href="../Page/鳥羽結子.md" title="wikilink">鳥羽結子</a>：<a href="../Page/演歌.md" title="wikilink">演歌歌手</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/中村隼人.md" title="wikilink">中村隼人</a>：前職業棒球選手</li>
<li><a href="../Page/中村北斗.md" title="wikilink">中村北斗</a>：職業足球選手</li>
<li><a href="../Page/野呂邦暢.md" title="wikilink">野呂邦暢</a>：小説家</li>
<li><a href="../Page/野口彌太郎.md" title="wikilink">野口彌太郎</a>：<a href="../Page/畫家.md" title="wikilink">畫家</a></li>
<li><a href="../Page/初村瀧一郎.md" title="wikilink">初村瀧一郎</a>：<a href="../Page/政治家.md" title="wikilink">政治家</a></li>
<li><a href="../Page/東良信.md" title="wikilink">東良信</a>：內閣府審議官</li>
<li><a href="../Page/藤原新.md" title="wikilink">藤原新</a>：<a href="../Page/田徑.md" title="wikilink">田徑選手</a></li>
<li><a href="../Page/松橋章太.md" title="wikilink">松橋章太</a>：職業足球選手</li>
<li><a href="../Page/松橋優.md" title="wikilink">松橋優</a>：職業足球選手</li>
<li><a href="../Page/御厨哲美.md" title="wikilink">御厨哲美</a>：漫畫家</li>
<li><a href="../Page/役所廣司.md" title="wikilink">役所廣司</a>：俳優</li>
<li><a href="../Page/吉崎觀音.md" title="wikilink">吉崎觀音</a>：漫畫家</li>
<li><a href="../Page/宮田由佳里.md" title="wikilink">宮田由佳里</a>：排球選手</li>
<li><a href="../Page/濱口華菜里.md" title="wikilink">濱口華菜里</a>：排球選手</li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [諫早觀光物產聯合協會](http://www.isahaya-kankou.com/)

  - [諫早商工會](http://www.isahayacci.com/)

<!-- end list -->

1.

2.