**諸大綬**（），[字](../Page/表字.md "wikilink")**端甫**，[號](../Page/號.md "wikilink")**南明**，[浙江](../Page/浙江.md "wikilink")[山陰](../Page/山陰.md "wikilink")（今[紹興](../Page/紹興.md "wikilink")）漓渚人。[明朝大臣](../Page/明朝.md "wikilink")。

## 生平

諸大綬為[嘉靖三十五年](../Page/嘉靖.md "wikilink")（1556年）丙辰科一甲一名[進士](../Page/進士.md "wikilink")（[狀元](../Page/狀元.md "wikilink")），授[翰林院](../Page/翰林院.md "wikilink")[修撰](../Page/修撰.md "wikilink")，曾侍[穆宗曰講](../Page/明穆宗.md "wikilink")，史載大绶“立朝不激不随，有公辅之望”。與蕭勉、[陳鶴](../Page/陳鶴.md "wikilink")、楊珂、朱公節、沈練、錢鞭、[柳林](../Page/柳林.md "wikilink")、[徐渭](../Page/徐渭.md "wikilink")、呂光升等並號“越中十子”。\[1\][隆庆元年](../Page/隆庆.md "wikilink")（1567年）升[侍讲学士掌院事](../Page/侍讲学士.md "wikilink")，[隆庆四年](../Page/隆庆.md "wikilink")（1570年）又升[礼部左侍郎](../Page/礼部左侍郎.md "wikilink")。[萬曆元年](../Page/萬曆.md "wikilink")（1573年）營救徐渭出獄。同年卒，赠[礼部尚书](../Page/礼部尚书.md "wikilink")，[谥](../Page/谥.md "wikilink")**文懿**。

## 墓葬

[嘉慶](../Page/嘉慶.md "wikilink")《山陰縣誌》載，其墓在城西南三十里漓渚宝寿山九板桥。\[2\]。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [诸大绶](http://fuzhuang.bjdclib.com/subdb/exam/examperson/200908/t20090818_21954.html)
    北京市東城區圖書館

{{-}}

[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:明朝翰林](../Category/明朝翰林.md "wikilink")
[Category:明朝禮部侍郎](../Category/明朝禮部侍郎.md "wikilink")
[Category:紹興人](../Category/紹興人.md "wikilink")
[D](../Category/諸姓.md "wikilink")
[Category:諡文懿](../Category/諡文懿.md "wikilink")

1.  《紹興府志·卷六九·徐渭傳》：“渭與蕭柱山勉，陳海樵鶴，楊秘圖珂，朱東武公節，沈青霞錬，錢八山楩，柳少明文，及諸龍泉、呂對明稱越中十子。”
2.  《嘉庆山阴县志》