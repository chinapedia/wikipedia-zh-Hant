[Norbert_wiener.jpg](https://zh.wikipedia.org/wiki/File:Norbert_wiener.jpg "fig:Norbert_wiener.jpg")
**諾伯特·維納**（，），生於[美國](../Page/美國.md "wikilink")[密蘇里州哥倫比亞](../Page/密蘇里州.md "wikilink")，美国[應用數學家](../Page/應用數學家.md "wikilink")，在[電子工程方面貢獻良多](../Page/電子工程.md "wikilink")。他是[隨機過程和](../Page/隨機過程.md "wikilink")[噪声信号处理的先驅](../Page/噪声信号处理.md "wikilink")，又提出「[控制論](../Page/控制論.md "wikilink")」一詞。

## 生平

他的父親里奧·維納是[波蘭籍](../Page/波蘭.md "wikilink")[猶太移民](../Page/猶太.md "wikilink")，母親是[德國籍](../Page/德國.md "wikilink")[猶太人](../Page/猶太.md "wikilink")。里奧是[哈佛大學](../Page/哈佛大學.md "wikilink")[斯拉夫語族的講師](../Page/斯拉夫語族.md "wikilink")，他用他自創的高壓方法培育諾伯特。憑他的天份再加上父親的培育，他成為[神童](../Page/神童.md "wikilink")。1903年，他開始上學；1906年（12歲），高中畢業，同年9月入讀[塔夫斯學院修讀數學](../Page/塔夫斯大學.md "wikilink")；1909年（15歲）時他已取得學士學位，入讀[哈佛大學研究](../Page/哈佛大學.md "wikilink")[動物學](../Page/動物學.md "wikilink")。一年後他往[康乃爾大學轉讀](../Page/康乃爾大學.md "wikilink")[哲學](../Page/哲學.md "wikilink")。1912年，18歲的他取得[數理邏輯的博士學位](../Page/數理邏輯.md "wikilink")。

他到[英國](../Page/英國.md "wikilink")[劍橋隨](../Page/劍橋.md "wikilink")[伯特兰·罗素](../Page/伯特兰·罗素.md "wikilink")、[戈弗雷·哈代學習](../Page/戈弗雷·哈罗德·哈代.md "wikilink")，1914年又到[德國](../Page/德國.md "wikilink")[哥廷根跟](../Page/哥廷根.md "wikilink")[大衛·希爾伯特和](../Page/大衛·希爾伯特.md "wikilink")[爱德蒙·兰道](../Page/爱德蒙·兰道.md "wikilink")。之後他回到劍橋，再回到美國。1915至16年，他在哈佛教授哲學，其後為[通用電氣和](../Page/通用電氣.md "wikilink")[大美百科全書工作](../Page/大美百科全書.md "wikilink")。戰爭期間在[馬里蘭州](../Page/馬里蘭州.md "wikilink")[阿伯丁試驗場鑽研](../Page/阿伯丁試驗場.md "wikilink")[彈道學](../Page/彈道學.md "wikilink")。戰後他在[麻省理工学院教授數學](../Page/麻省理工学院.md "wikilink")。他講課技巧惡劣，常常鬧笑話或心不在焉。

在[麻省理工学院任教時](../Page/麻省理工学院.md "wikilink")，他不時到[歐洲](../Page/歐洲.md "wikilink")。1926年和一名德國移民結婚。他們有兩個女兒。他主要在哥廷根和劍橋，研究[布朗運動](../Page/布朗運動.md "wikilink")、[傅立葉變換](../Page/傅立葉變換.md "wikilink")、[調和分析](../Page/調和分析.md "wikilink")、[狄利克雷問題和](../Page/狄利克雷問題.md "wikilink")等。1929年维纳指导当时在贝尔电话公司学习的博士生[李郁荣研制了](../Page/李郁荣.md "wikilink")[李-维纳网络](../Page/李-维纳网络.md "wikilink")，获得美国专利。1935年维纳应[国立清华大学校长](../Page/国立清华大学.md "wikilink")[梅贻琦和数学系主任](../Page/梅贻琦.md "wikilink")[熊庆来之聘](../Page/熊庆来.md "wikilink")，到清华大学讲学，主讲[傅立叶变换](../Page/傅立叶变换.md "wikilink")，听讲者包括[华罗庚](../Page/华罗庚.md "wikilink")、[段学复等](../Page/段学复.md "wikilink")。维纳曾推荐[华罗庚和](../Page/华罗庚.md "wikilink")[徐贤修合写的论文发表在麻省理工学院的](../Page/徐贤修.md "wikilink")《[数学物理学报](../Page/数学物理学报.md "wikilink")》（上）
\[1\]。（同时期者尚有法国数学家[雅克·阿达马](../Page/雅克·阿达马.md "wikilink")）。

[二戰時](../Page/二戰.md "wikilink")，他在槍炮控制方面工作，引發了他對[通訊理論和](../Page/通訊理論.md "wikilink")[反馈的興趣](../Page/反馈.md "wikilink")，著《[控制论：或关于在动物和机器中控制和通信的科学](../Page/控制论：或关于在动物和机器中控制和通信的科学.md "wikilink")》一书，促成了控制論的誕生。戰後發生了一件怪事，他邀請了[人工智能](../Page/人工智能.md "wikilink")、[電腦科學和](../Page/電腦科學.md "wikilink")[神經心理學的年輕學者到麻省理工](../Page/神經心理學.md "wikilink")。當這批學者來到時，他卻突然斷絕所有來往。這可能由他的神經質性格引起。他的家族中包括弟弟等，有嚴重的[精神分裂症病史](../Page/精神分裂症.md "wikilink")，而維納本身則有重度的近視（在麻省理工任教時，幾乎得用手摸著牆壁走路）與嚴重的[躁鬱症](../Page/躁鬱症.md "wikilink")。躁症發作時，他瘋狂似的跑遍校園，向別人宣傳他的發現。而躁鬱症症狀產生時，他則多次向麻省的同事列文森訴說自己的自殺念頭。

二戰後，他對科學研究的政治影響和科學的軍事用途更為關注。他拒絕接受政府資助或參與軍事計劃。

1964年他在[瑞典](../Page/瑞典.md "wikilink")[斯德哥爾摩逝世](../Page/斯德哥爾摩.md "wikilink")。

## 著作

部分:

  - 《行為、目的和目的論》
  - 《
    [控制论：或关于在动物和机器中控制和通信的科学](../Page/控制论：或关于在动物和机器中控制和通信的科学.md "wikilink")》

自傳：

  - 1953年*Ex-Prodigy: My Childhood and Youth*（舊神童：我的童年與青年時代）
  - 1956年*I am a Mathematician*（我是數學家）

## 參見

  - [維納方程](../Page/維納方程.md "wikilink")
  - [維納過濾](../Page/維納過濾.md "wikilink")
  - [維納過程](../Page/維納過程.md "wikilink")

## 参考文献

## 外部链接

  -
  -
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:波蘭裔美國人](../Category/波蘭裔美國人.md "wikilink")
[Category:美国数学家](../Category/美国数学家.md "wikilink")
[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:猶太科學家](../Category/猶太科學家.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:劍橋大學校友](../Category/劍橋大學校友.md "wikilink")
[Category:康乃爾大學校友](../Category/康乃爾大學校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:和平主義者](../Category/和平主義者.md "wikilink")
[Category:密蘇里州人](../Category/密蘇里州人.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:博谢纪念奖获得者](../Category/博谢纪念奖获得者.md "wikilink")

1.  [张奠宙著](../Page/张奠宙.md "wikilink")《中国近代数学的发展》127-128页