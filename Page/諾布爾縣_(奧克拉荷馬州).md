**諾布爾县**（）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州中北部的一個縣](../Page/奧克拉荷馬州.md "wikilink")，面積1,923平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口11,561人。\[1\]本縣縣治為（）。

## 歷史

[Cherokee_Outlet_1885.jpg](https://zh.wikipedia.org/wiki/File:Cherokee_Outlet_1885.jpg "fig:Cherokee_Outlet_1885.jpg")
諾布爾县一帶一直都是[印第安人](../Page/印第安人.md "wikilink")[切諾基領地的一部份](../Page/切諾基人.md "wikilink")，直至[俄克拉何馬領地於](../Page/俄克拉何馬領地.md "wikilink")1890年成立為止。於1983年，聯邦政府開放了這一帶予非印第安人\]居住。\[2\]

本縣於1893年9月16日成立，原名**P縣**。於1894年11月6日，改為現名，以紀念[本傑明·哈里森內閣中任](../Page/本傑明·哈里森.md "wikilink")[內政部長的](../Page/美國內政部長.md "wikilink")[約翰·W·諾布爾](../Page/約翰·W·諾布爾.md "wikilink")。\[3\]

### 著名事件

[McVeigh_mugshot.jpg](https://zh.wikipedia.org/wiki/File:McVeigh_mugshot.jpg "fig:McVeigh_mugshot.jpg")
策劃1995年4月19日[奧克拉荷馬市爆炸案的美國公民](../Page/奧克拉荷馬市爆炸案.md "wikilink")[提摩太·占士·麥克維是在本縣內被捕](../Page/提摩太·占士·麥克維.md "wikilink")。當時，他正駕著車沿著[35號州際公路逃到北方](../Page/35號州際公路.md "wikilink")。期間，麥克維因汽車沒有車牌而被巡警警官查爾斯·漢格（）截查，並因無證駕駛和違法持有武器而被捕。奧克拉荷馬市爆炸案共造成168人死亡\[4\]，超過680人受傷\[5\]，是[九一一襲擊事件發生之前在美國本土造成死亡人數最多的恐怖襲擊事件](../Page/九一一襲擊事件.md "wikilink")。\[6\]

逮捕麥克維的警官漢格於2004年被選為諾布爾縣警長。\[7\]\[8\]

## 地理

[Sunrise_in_Perry_Oklahoma.jpg](https://zh.wikipedia.org/wiki/File:Sunrise_in_Perry_Oklahoma.jpg "fig:Sunrise_in_Perry_Oklahoma.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，諾布爾縣的總面積為，其中有，即98.58為陸地；，即1.42%為水域。\[9\]

### 主要公路

  - [I-35.svg](https://zh.wikipedia.org/wiki/File:I-35.svg "fig:I-35.svg")
    [35號州際公路](../Page/35號州際公路.md "wikilink")
  - [US_64.svg](https://zh.wikipedia.org/wiki/File:US_64.svg "fig:US_64.svg")
    [美国国道64](../Page/美国国道64.md "wikilink")
  - [US_77.svg](https://zh.wikipedia.org/wiki/File:US_77.svg "fig:US_77.svg")
    [美国国道77](../Page/美国国道77.md "wikilink")
  - [US_177.svg](https://zh.wikipedia.org/wiki/File:US_177.svg "fig:US_177.svg")
    [美国国道177](../Page/美国国道177.md "wikilink")
  - [US_412.svg](https://zh.wikipedia.org/wiki/File:US_412.svg "fig:US_412.svg")
    [美国国道412](../Page/美国国道412.md "wikilink")
  - [Oklahoma_State_Highway_15.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_15.svg "fig:Oklahoma_State_Highway_15.svg")
    [俄克拉何马州州道15](../Page/俄克拉何马州州道15.md "wikilink")
  - [Oklahoma_State_Highway_86.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_86.svg "fig:Oklahoma_State_Highway_86.svg")
    [俄克拉何马州州道86](../Page/俄克拉何马州州道86.md "wikilink")
  - [Oklahoma_State_Highway_108.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_108.svg "fig:Oklahoma_State_Highway_108.svg")
    [俄克拉何马州州道108](../Page/俄克拉何马州州道108.md "wikilink")
  - [Oklahoma_State_Highway_156.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_156.svg "fig:Oklahoma_State_Highway_156.svg")
    [俄克拉何马州州道156](../Page/俄克拉何马州州道156.md "wikilink")
  - [Oklahoma_State_Highway_164.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_164.svg "fig:Oklahoma_State_Highway_164.svg")
    [俄克拉何马州州道164](../Page/俄克拉何马州州道164.md "wikilink")

### 毗鄰縣

所有諾布爾縣的毗鄰縣皆為奧克拉荷馬州的縣份

  - [凱縣](../Page/凱縣_\(奧克拉荷馬州\).md "wikilink")：北方
  - [歐塞奇縣](../Page/歐塞奇縣_\(奧克拉荷馬州\).md "wikilink")：東北方
  - [波尼縣](../Page/波尼縣_\(奧克拉荷馬州\).md "wikilink")：東方
  - [佩恩縣](../Page/佩恩縣_\(奧克拉荷馬州\).md "wikilink")：南方
  - [洛根縣](../Page/洛根縣_\(奧克拉荷馬州\).md "wikilink")：西南方
  - [加菲爾德縣](../Page/加菲爾德縣_\(奧克拉荷馬州\).md "wikilink")：西方

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，諾布爾縣擁有11,411居民、4,504住戶和3,211家庭。\[10\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")16居民（每平方公里6居民）。\[11\]本縣擁有5,082間房屋单位，其密度為每平方英里7間（每平方公里3間）。\[12\]而人口是由86.44%[白人](../Page/歐裔美國人.md "wikilink")、1.58%[黑人](../Page/非裔美國人.md "wikilink")、7.53%[土著](../Page/美國土著.md "wikilink")、0.33%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.03%[太平洋白民](../Page/太平洋白民.md "wikilink")、0.65%其他[種族和](../Page/種族.md "wikilink")3.4%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")1.8%。\[13\]

在4,504住户中，有32%擁有一個或以上的兒童（18歲以下）、59%為夫妻、8.4%為單親家庭、28.7%為非家庭、25.5%為獨居、11.2%住戶有同居長者。平均每戶有2.47人，而平均每個家庭則有2.97人。在11,411居民中，有25.5%為18歲以下、7.9%為18至24歲、27.5%為25至44歲、23.9%為45至65歲以及15.2%為65歲以上。人口的年齡中位數為38歲，女子對男子的性別比為100：97.4。成年人的性別比則為100：96。\[14\]

本縣的住戶收入中位數為$33,968，而家庭收入中位數則為$40,180。男性的收入中位數為$16,643，而女性的收入中位數則為$21,235，[人均收入為](../Page/人均收入.md "wikilink")$17,022。約9.6%家庭和12.8%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括16.4%兒童（18歲以下）及11%長者（65歲以上）。\[15\]

## 參考文獻

[N](../Category/俄克拉何马州行政区划.md "wikilink")

1.  [2010 census data](http://2010.census.gov/2010census/data/)

2.

3.  [Everett, Dianna. *Encyclopedia of Oklahoma History and Culture*.
    "Noble
    County."](http://digital.library.okstate.edu/encyclopedia/entries/N/NO003.html)
     Retrieved October 3, 2013.

4.

5.

6.

7.  [Oklahoma Trooper Reflects on McVeigh Arrest -
    officer.com](http://www.officer.com/web/online/Top-News-Stories/Oklahoma-Trooper-Reflects-on-McVeigh-Arrest/1$51844)

8.  [Sheriffs profiles Hanger - oklahoma
    sheriffs](http://www.oklahomasheriffs.com/Sheriffs%20Photos%20&%20Profiles/Hanger.htm)


9.

10. [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

11. [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

12. [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

13. [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

14. [American FactFinder](http://factfinder.census.gov/)

15.