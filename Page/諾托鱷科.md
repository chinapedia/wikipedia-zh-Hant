**諾托鱷科**（Notosuchidae）是[中真鱷類的一科](../Page/中真鱷類.md "wikilink")，是群小型、陸生動物，生存於[白堊紀晚期的南方各大陸](../Page/白堊紀.md "wikilink")。

## 屬

  - [諾托鱷](../Page/諾托鱷.md "wikilink")（*Notosuchus*）：生存於[康尼亞克階到](../Page/康尼亞克階.md "wikilink")[桑托階的](../Page/桑托階.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")。\[1\]
  - [馬里利亞鱷](../Page/馬里利亞鱷.md "wikilink")（*Mariliasuchus*）：生存於[土侖階到桑托階的](../Page/土侖階.md "wikilink")[巴西](../Page/巴西.md "wikilink")。\[2\]

## 參考資料

[Category:鱷形超目](../Category/鱷形超目.md "wikilink")

1.  Hugo CA, Leanza HA. 2001. Hoja Geológica 3969-IV, General Roca.
    Provincias de Río Negro y Neuquén. *Boletín Servicio Geológico
    Minero Argentino, Instituto de Geología y Recursos Minerales*
    **308**: 1-65.
2.  Candeiro CRA, Martinelli AG, Avilla LS, Rich TH. 2006. Tetrapods
    from the Upper Cretaceous (Turonian-Maastrichtian) Bauru Group of
    Brazil: a reappraisal. *Cretaceous Research* **27** (6): 923-946.