**諾斯底主義**（Gnosticism
或稱**靈知派**和**靈智派**）的“諾斯底”一词在[希臘語中意为](../Page/希腊语.md "wikilink")“[知識](../Page/知識.md "wikilink")”，諾斯底是指在不同[宗教運動及團體中的同一信念](../Page/宗教運動.md "wikilink")，這信念可能源自於[史前時代](../Page/史前時代.md "wikilink")，但卻於公元的首數個[世紀活躍於](../Page/世紀.md "wikilink")[地中海週圍與伸延至](../Page/地中海.md "wikilink")[中亞地區](../Page/中亞.md "wikilink")。了解這個核心信念的鑰匙就是要透過擁有「」（Gnosis，或譯「真知」），“靈知”在[希腊语原文是指透過個人經驗所獲得的一種](../Page/希腊语.md "wikilink")[知識或](../Page/知識.md "wikilink")[意識](../Page/意識.md "wikilink")。諾斯底主義者相信透過這種超凡的經驗，可使他們脫離無知及現世。

## 歷史

歷史上不同的諾斯底主義教派（尤其[基督教的](../Page/基督教.md "wikilink")[諾斯底教派](../Page/諾斯替教.md "wikilink")）都是被持有對抗信念的教派所逼害。缺乏證明早期諾斯底主義者自稱為「[諾斯底](../Page/諾斯底.md "wikilink")」，這個稱呼在現今學者中經常用來表示那些以個人智慧來獲得拯救的人。雖然很多諾斯底主義者都遵循[耶穌基督的教導](../Page/耶穌基督.md "wikilink")，甚至自稱為[基督徒](../Page/基督徒.md "wikilink")，但當中亦有很多不同宗教定位的諾斯底主義者，尤其是遠久的[波斯](../Page/波斯.md "wikilink")[先知](../Page/先知.md "wikilink")[摩尼的跟隨者](../Page/摩尼.md "wikilink")（[摩尼教](../Page/摩尼教.md "wikilink")）及在[伊拉克及](../Page/伊拉克.md "wikilink")[伊朗早於基督教的](../Page/伊朗.md "wikilink")[曼達安教](../Page/曼達安教.md "wikilink")。

[歐洲諾斯底主義嚴重地受到光與暗的鬥爭的](../Page/歐洲.md "wikilink")[神話觀念所影響](../Page/神話.md "wikilink")。這影響導致強烈的[二元論發展](../Page/二元論.md "wikilink")：在天國的國土與物質的世界有著明顯的分隔，這個物質世界是由創造它的無知的神所支配。受到基督教廣泛的傳播所影響，德謬哥被認為與[撒但](../Page/撒但.md "wikilink")十分相似。其中一個二元論的來源可能是直接由其他近東諾斯底主義引入，如[鮑格米勒教派](../Page/鮑格米勒教派.md "wikilink")。

在二十世紀發現的[死海古卷可找到](../Page/死海古卷.md "wikilink")[諾斯底教痕跡](../Page/諾斯替教.md "wikilink")，但在[十九世紀前的知識完全來自](../Page/十九世紀.md "wikilink")[愛任紐](../Page/愛任紐.md "wikilink")、[希波律陀](../Page/希波律陀.md "wikilink")、[俄利根](../Page/俄利根.md "wikilink")、[特土良](../Page/特土良.md "wikilink")、[伊皮法紐等基督徒著作](../Page/伊皮法紐.md "wikilink")。

## 性質及構成

### 典型與特徵

儘管很難為諾斯底主義落一個定義，但那些稱為諾斯底主義的古代[哲學能給予一個典型的模樣](../Page/哲學.md "wikilink")。諾斯底主義通常都具備以下標記：

  - 一個遙遠、至高的及不為人知的[獨一個體神格觀念](../Page/獨一個體.md "wikilink")，祂有著不同的名字，包括**[普累若麻](../Page/普累若麻.md "wikilink")**（Pleroma，[希臘文原文即](../Page/希臘文.md "wikilink")「豐盛」的意思）及**[拜多斯](../Page/拜多斯.md "wikilink")**（Bythos，希臘文原文即「深」的意思）；
  - 更多的神祇從獨一個體「流出」。流出的神祇會漸次的遠離獨一個體，帶有不穩定的神祇性質；
  - [人類的墮落是在獨一個體內發生的事件](../Page/人類.md "wikilink")，而非因人類的行為。人類的墮落是因蘇菲亞（Sophia，希臘文原文即「智慧」的意思）從人類體內流出所造成的；
  - 另一個不同的造物主，以[柏拉圖主義的傳統命名為](../Page/柏拉圖主義.md "wikilink")[德謬哥](../Page/德謬哥.md "wikilink")；
    <small>證據顯示德謬哥的觀念是由[柏拉圖的](../Page/柏拉圖.md "wikilink")《[蒂邁歐篇](../Page/蒂邁歐篇.md "wikilink")》及《[理想國](../Page/理想國.md "wikilink")》而來。在《蒂邁歐篇》中，德謬哥是一位仁慈的創造者，從先在的物質創造了宇宙，卻在創造時受了迷惑。在《理想國》中，在形容[蘇格拉底](../Page/蘇格拉底.md "wikilink")[靈魂中的慾望就有像有著](../Page/靈魂.md "wikilink")[獅子形象的德謬哥](../Page/獅子.md "wikilink")。在其他地方，德謬哥亦被稱作「[伊達波思](../Page/德謬哥.md "wikilink")」（Ialdabaoth）、「薩麥爾」（Samael，[亞蘭文即盲目的神的意思](../Page/亞蘭文.md "wikilink")）或「薩迦拉」（Saklas，[古敘利亞文即愚蠢的意思](../Page/古敘利亞文.md "wikilink")），祂有時會忽視獨一個體及甚至會與之反抗，因此祂會顯得帶有惡意。德謬哥創造了一批「[執政者](../Page/執政者.md "wikilink")」統管整個物質世界，並且阻礙靈魂的攀升。</small>
  - 世界因而是帶有缺陷或者是因錯誤而產生，但最少與構成它的物質一樣的善良。這個世界是較高層次的存在或意識的幻影，它的下等可以與[油畫](../Page/油畫.md "wikilink")、[雕刻或其他](../Page/雕刻.md "wikilink")[手藝相比](../Page/手藝.md "wikilink")，是真實的模仿。在某些情況下，它是邪惡的及緊縮的，是一個為當中的居民所設計的監獄；
  - 以複雜的[神話及](../Page/神話.md "wikilink")[宇宙論的戲劇形式表達以上的狀況](../Page/宇宙論.md "wikilink")：神祇因墮落至物質世界而寄在某些人類之內，祂們可以透過覺醒而回到靈界。對於某人的救贖就等同於一個神祇的復原，並不只是一個人被救贖這麼簡單，卻是提升至是一件宇宙內重大的事情；
  - 某一種的知識是這個復原過程的重要因素，並且透過救贖者的幫助（如[基督](../Page/基督.md "wikilink")、[塞特或蘇菲亞](../Page/塞特.md "wikilink")）。

以上諾斯底主義的特徵只是[敘利亞](../Page/敘利亞.md "wikilink")／[埃及教派的特徵](../Page/埃及.md "wikilink")，而最大的[波斯教派則以](../Page/波斯.md "wikilink")[摩尼教及](../Page/摩尼教.md "wikilink")[曼達安教自稱](../Page/曼達安教.md "wikilink")。事實上，諾斯底主義只是指敘利亞／埃及教派，而摩尼教則是指波斯教派。

### 二元論及一元論

諾斯底主義只是普遍地為[二元論](../Page/二元論.md "wikilink")，他們可以由徹底的二元論（即[摩尼教](../Page/摩尼教.md "wikilink")）到傳統較輕的緩和二元論。[華倫提努另外發展了一套](../Page/華倫提努.md "wikilink")[一元論](../Page/一元論.md "wikilink")，是以過往為二元論的用字來表達的。

  - **徹底二元論**：或稱為「絕對二元論」，當中有兩個同等的神祇，摩尼教指祂們分別是光明與黑暗，因黑暗混亂的行動而使得捲入紛爭之中，及後，部份光明的元素被囚在黑暗之中。創造物質的目的就是要設定緩慢的步驟，去將這些光明的元素從黑暗中抽出，使得光明的國度在最後能壓過黑暗。摩尼教的二元神話相信是從[瑣羅亞斯德教繼承得來](../Page/瑣羅亞斯德教.md "wikilink")，永恆的神[阿胡拉·瑪茲達受到祂的對頭](../Page/阿胡拉·瑪茲達.md "wikilink")[安格拉·曼紐的對抗](../Page/安格拉·曼紐.md "wikilink")。兩者牽涉在大鬥爭之中，而最終阿胡拉·瑪茲達會得到勝利。摩尼教創造的神話見證了光明逐漸的流出，每一次流出都逐漸地墮落而使得[卜塔的出現](../Page/卜塔.md "wikilink")。黑暗的神創造及統治物質世界。再者祂沉醉在其惡意中。一般諾斯底主義相信物質世界與黑暗所帶來對惡意的沉醉是要將光明的元素囚禁在物質世界中，或囚禁在黑暗中或因醉酒分心而變得無知。

<!-- end list -->

  - **緩和二元論**：是指兩個神祇中的一個較另一個低級。這個古典的諾斯底主義就是塞特派，他們認為物質世界是由一位較比他們信仰的真神低位的神祇所創造；而靈界則是徹底的與物質世界不同，與真神一同存在，亦是那些醒悟的[人類真正的家園](../Page/人類.md "wikilink")。因此，他們會表現出與[世界激烈的分割](../Page/世界.md "wikilink")，終極目標是讓[靈魂脫離物質世界的阻礙](../Page/靈魂.md "wikilink")。

<!-- end list -->

  - **一元論**：是指爭議較低位神祇的神性或半神性。華倫提努所提倡的諾斯底[神話是指他們對](../Page/神話.md "wikilink")[宇宙的可能是一元的](../Page/宇宙.md "wikilink")，而非二元的。在神話中[德謬哥的惡念被減輕](../Page/德謬哥.md "wikilink")，祂所創造有缺憾的世界並非因任何道德的缺點，而是祂真實地不認識高位的靈界。所以華倫提努相比塞特派對物質世界帶有較輕的輕視，認為實體是「認知的錯誤」，而非甚麼神聖的實質。華倫提努的世界觀是一元的，所有的事物都是神聖方面的，而人類一般的視野因「認知的錯誤」受到物質世界的限制。

### 道德及禮儀

諾斯底主義的道德品行一般都是[禁慾主義的](../Page/禁慾主義.md "wikilink")，尤以性及飲食方面最為要緊。在其他地方上，諾斯底主義者則會採取較溫和的[禁慾](../Page/禁慾.md "wikilink")，以改正其行為。根據[托勒密的](../Page/托勒密.md "wikilink")《Epistle
to Flora》就定下了禁慾主義是個人的道德取向，以使其靈魂能從[洛格斯中獲得好處](../Page/洛格斯.md "wikilink")。

另一方面，根據一些[基督教](../Page/基督教.md "wikilink")[教父的記述](../Page/教父.md "wikilink")，一部份的諾斯底主義者不是奉行禁慾主義，相反卻是奉行[放蕩主義](../Page/放蕩主義.md "wikilink")，或是假裝禁慾。[伊皮法紐就曾指](../Page/伊皮法紐.md "wikilink")[執政教派中有些人是克制自己的慾念](../Page/執政教派.md "wikilink")，但有些卻是假裝禁食以此欺騙他人。[愛任紐在其](../Page/愛任紐.md "wikilink")《[反異端](../Page/反異端.md "wikilink")》中指術士[賽門·馬革斯建立了道德自由的學派](../Page/賽門·馬革斯.md "wikilink")（即[道德無涉主義](../Page/道德無涉主義.md "wikilink")），說只要相信他及他的妓女同伴[特洛依的海倫](../Page/特洛依.md "wikilink")（Helen
of
Troy），就不需理會任何《[聖經](../Page/聖經.md "wikilink")》中的先知和他們有關道德的教訓，他們因賽門的恩典而得救，而非因自己的德行，所以可以做他們想做的事。雖然對賽門與放蕩主義的關係所知甚少，但的確有他的跟隨者結婚及生下子女，由此可見他們亦非完全的禁慾。

愛任紐亦指賽門的門生華倫提努吃異教祭物、[性濫交及娶了收養的姊妹為妻子](../Page/性濫交.md "wikilink")。華倫提努更以性事作為[聖禮的儀式](../Page/聖禮.md "wikilink")，意思是以此來模仿一對構成[普累若麻的](../Page/普累若麻.md "wikilink")[陰陽基質](../Page/陰陽基質.md "wikilink")（syzygy）。諾斯底主義的[迦坡加德派則聲稱他們有著力量去做任何的事](../Page/迦坡加德派.md "wikilink")，包括邪惡的或是無信仰的，而道德只是人類眼中的善良或邪惡。迦坡加德的兒子[伊皮法尼斯更教導說雜婚是神的律法](../Page/伊皮法尼斯.md "wikilink")。

### 占星術

[Universum.jpg](https://zh.wikipedia.org/wiki/File:Universum.jpg "fig:Universum.jpg")展示了諾斯底主義者對靈界的探知。\]\]
諾斯底也特指現在所谓[占星術](../Page/占星術.md "wikilink")，其理論是認為每一個星球都有一個屬靈的統治者，在不同的星球有不同的影響力，並將和地球會分開。

## 影響

諾斯底對人們實際的生活有兩方面的影響：

  - 一、身體是超道德的，所以放縱情慾是沒有關係的。
  - 二、物質的身體是沒有真實價值的，所以人可以自己盡情的放縱也可以克制己身，而諾斯底信徒都比較於偏向於克制己身。\[1\]

## 主要學派及著作

從以上可見，諾斯底主義的學派可大致上分為[波斯學派及](../Page/波斯.md "wikilink")[敘利亞](../Page/敘利亞.md "wikilink")／[埃及學派](../Page/埃及.md "wikilink")。前者有著較重的[二元論趨向](../Page/二元論.md "wikilink")，反映受著波斯[瑣羅亞斯德教的強烈影響](../Page/瑣羅亞斯德教.md "wikilink")；而敘利亞／埃及學派就是傾向[一元論](../Page/一元論.md "wikilink")。後期的諾斯底主義，如[清潔派](../Page/清潔派.md "wikilink")、[鮑格米勒派及](../Page/鮑格米勒派.md "wikilink")[迦坡加德派卻有著包含以上兩種元素](../Page/迦坡加德派.md "wikilink")。

[里基諾斯書是](../Page/里基諾斯書.md "wikilink")[范倫提納所寫](../Page/范倫提納.md "wikilink")，主張[人復活不是](../Page/人復活.md "wikilink")[實在形體](../Page/實在形體.md "wikilink")。《[多馬福音](../Page/多馬福音.md "wikilink")》收集了114段耶穌說的話。[《腓力福音》討論聖禮](../Page/《腓力福音》.md "wikilink")，包括施洗、婚事等。《[約翰旁經](../Page/約翰旁經.md "wikilink")》詳細敘述[宇宙起源](../Page/宇宙起源.md "wikilink")。\[2\]

### 波斯諾斯底主義

波斯學派是諾斯底主義的古老形式，他們是個別的宗教，而非[基督教或](../Page/基督教.md "wikilink")[猶太教的分支](../Page/猶太教.md "wikilink")。

  - [曼達安教只剩少數仍然存在於](../Page/曼達安教.md "wikilink")[伊拉克及](../Page/伊拉克.md "wikilink")[伊朗的](../Page/伊朗.md "wikilink")[胡齊斯坦省](../Page/胡齊斯坦省.md "wikilink")，「曼底安」（Mandā
    d-Heyyi）大約解釋為「生命的知識」。曼底安教的來源已經無從稽考，它是以[施洗約翰為信仰的主要對象](../Page/施洗約翰.md "wikilink")，亦著重[浸禮](../Page/浸禮.md "wikilink")。除了部份與[基督教吻合外](../Page/基督教.md "wikilink")，他們不相信[摩西](../Page/摩西.md "wikilink")、[耶穌或](../Page/耶穌.md "wikilink")[穆罕默德](../Page/穆罕默德.md "wikilink")。他們的信仰及修行與那些信仰對象所提倡的並不相同，因而必須分清。大量曼底安經典的原稿依然存在，就如《Genzā
    Rabbā》相信是早於2世紀的手抄本；另外亦有《Qolastā》（即祈禱聖典）及《施洗約翰記》。

<!-- end list -->

  - [摩尼教是一個完全獨立的信仰遺產](../Page/摩尼教.md "wikilink")，由先知[摩尼所創立而卻接近消滅](../Page/摩尼.md "wikilink")。大部份有關於摩尼教的著作均已失去，只要是發現一本原稿著作亦可幫助發掘更多資料，現時放置在[德國](../Page/德國.md "wikilink")[科隆的](../Page/科隆.md "wikilink")《Codex
    Manichaicus
    Coloniensis》主要包含摩尼教先知的生平資料及他的教訓與主張。除了與基督教的聯繫外，找不到摩尼教與任何基督教宗派有任何的關聯。

### 敘利亞／埃及諾斯底主義

[敘利亞](../Page/敘利亞.md "wikilink")／[埃及學派是從](../Page/埃及.md "wikilink")[柏拉圖主義引申發展出來](../Page/柏拉圖主義.md "wikilink")，描述創造是一連串由獨一個體的流出過程，最終形成物質世界。他們傾向認為物質是邪惡的，而非另一股等同的力量。事實上，邪惡與善良在這學派是一相對的觀念：前者是與獨一個體分離的極端。大部份此學派的著作都是與[基督教有關的](../Page/基督教.md "wikilink")，都是在[拿戈瑪第經集中發現的](../Page/拿戈瑪第經集.md "wikilink")：

  - **賽特著作**，以[亞當及](../Page/亞當.md "wikilink")[夏娃的第三子命名](../Page/夏娃.md "wikilink")，傳說是靈知的擁有者及傳播者。著作包括有：
      - 《[約翰密傳](../Page/約翰密傳.md "wikilink")》
      - 《亞當啟示錄》
      - 《掌權者的本質》
      - 《雷：完美的思想》
      - 《三形的普洛特諾尼亞》
      - 《科普特語埃及人福音|埃及人福音》
      - 《唆斯特利阿努》
      - 《阿羅基耐》
      - 《塞特三柱》

<!-- end list -->

  - **多馬著作**是按[聖多馬的學派所命名](../Page/聖多馬.md "wikilink")。就此學派的著作包括：
      - 《珍珠之歌》
          -
            《珍珠之歌》（Hymn of the
            Pearl）發現於《多馬行傳》之中，「珍珠之歌」是現代譯者所取的名字，它原來的名字是「使徒猶大·多馬在印度國土之歌。」文中以蛇象徵這個世界的統治者或惡的原則；以海象徵代表物質世界或黑暗世界，是神明沈入其中的所在；而埃及則被象徵為物質的世界，諾斯底主義把埃及當作「這個世界」，也就是物質世界、無知識界、邪惡宗教的象徵。「珍珠」在諾斯底主義的象徵中，是代表著有超自然意義上的「靈魂」的永恆的隱喻。這裡的珍珠，它的本質上是「遺失了」的珍珠，它被包圍在野獸的殼裡或隱藏在深處。當靈魂被稱為珍珠時，表示表示它的來源及它是很珍貴的，但是也是與它現在的無價值來作對比，以它的閃亮與它所陷入的黑暗來對照。若珍珠所代表的就是靈魂，珍珠是「遺失的」，那麼找回珍珠就是天上所關注的，取回珍珠也就成了聖子的使命。在王子出現前，它墮落到黑暗的能量之中，而王子為了它就承擔下降與流放的命運。\[3\]
      - 《[多馬福音](../Page/多馬福音.md "wikilink")》
      - 《爭戰者多馬書》

<!-- end list -->

  - **華倫提努著作**是主教及諾斯底師傅[華倫提努](../Page/華倫提努.md "wikilink") (Valentinus)
    的著作，他在賽特傳統外發展了一部複雜的宇宙觀。他的著作包括（有\*者為殘篇）：
      - 《The Divine Word Present in the Infant》(Fragment A) \*
      - 《On the Three Natures》(Fragment B) \*
      - 《Adam's Faculty of Speech》(Fragment C) \*
      - 《To Agathopous: Jesus' Digestive System》(Fragment D) \*
      - 《Annihilation of the Realm of Death》(Fragment F) \*
      - 《On Friends: The Source of Common Wisdom》(Fragment G) \*
      - 《Epistle on Attachments》(Fragment H) \*
      - 《Summer Harvest》\*
      - 《[真理的福音](../Page/真理的福音.md "wikilink")》\*
      - 《托密勒的諾斯底神話》
      - 《使徒保羅禱文》
      - 《Ptolemy's Epistle to Flora》
      - 《論復活》
      - 《腓力福音》

<!-- end list -->

  - **巴西利得著作**是以[巴西利得來命名](../Page/巴西利得.md "wikilink")，只有從反對者的聲音中，如[愛任紐的](../Page/愛任紐.md "wikilink")《[反異端](../Page/反異端.md "wikilink")》及[亞歷山大的革力免的著作得知其內容](../Page/亞歷山大的革力免.md "wikilink")：
      - 《The Octet of Subsistent Entities》(Fragment A)
      - 《The Uniqueness of the World》(Fragment B)
      - 《Election Naturally Entails Faith and Virtue》(Fragment C)
      - 《The State of Virtue》(Fragment D)
      - 《The Elect Transcend the World》(Fragment E)
      - 《轉世》(Fragment F)
      - 《Human Suffering and the Goodness of Providence》(Fragment G)
      - 《Forgivable Sins》(Fragment H)

### 後期的諾斯底主義

除了以上的諾斯底主義學派外，亦有不同的教派出現，按時序表列如下：

  - **[賽門·馬革斯](../Page/賽門·馬革斯.md "wikilink")**及**[馬吉安](../Page/馬吉安主義.md "wikilink")**均有著諾斯底主義的傾向，但他們為人熟悉的思想卻不是諾斯底主義的，他們都分別建立了一個龐大的教派。有人因而稱呼他們為「偽諾斯底主義者」或「原始諾斯底主義者」，其中賽門的學生[米南德就屬於這一類](../Page/米南德.md "wikilink")。
  - **[塞林則](../Page/塞林則.md "wikilink")**是一個1世紀末至2世紀教派的領袖。他相信這個世界是由一個較低級的創造者所掌管，並認為會有一個慶典、犧牲和宰殺祭物等方式所組成基督的王國。
  - **[俄斐特派](../Page/俄斐特派.md "wikilink")**是一群崇拜[創世記中記載的銅蛇的教派](../Page/創世記.md "wikilink")，奉它為授與知識的象徵。
  - **[該隱派](../Page/該隱派.md "wikilink")**是崇拜[該隱](../Page/該隱.md "wikilink")、[以掃](../Page/以掃.md "wikilink")、[可拉以及](../Page/可拉.md "wikilink")[所多瑪而得名的教派](../Page/所多瑪.md "wikilink")。對於此教派只有少許資料，但可以推測得他們是以放縱情慾及不道德的行為作為救贖的方式。
  - **[迦坡加德派](../Page/迦坡加德派.md "wikilink")**
  - **[Borborites](../Page/Borborites.md "wikilink")**
  - **[鮑格米勒教派](../Page/鮑格米勒教派.md "wikilink")**

[Simple_crossed_circle.svg](https://zh.wikipedia.org/wiki/File:Simple_crossed_circle.svg "fig:Simple_crossed_circle.svg")所使用的徽章\]\]

  - **[純潔派](../Page/純潔派.md "wikilink")**（或譯**[迦他利派](../Page/迦他利派.md "wikilink")**，另稱為**[阿爾比根派](../Page/阿爾比根派.md "wikilink")**或**[阿爾比派](../Page/阿爾比派.md "wikilink")**）是模仿諾斯底主義，於12世紀盛行西歐的教派。

## 和佛教關係的研究

在其著作《[諾斯底福音](../Page/諾斯底福音.md "wikilink")》（1979年）以及《》（2003年）指出[多馬福音及](../Page/多馬福音.md "wikilink")[拿戈瑪第經集的經文和](../Page/拿戈瑪第經集.md "wikilink")[佛經極相似](../Page/佛經.md "wikilink")。柏高絲認為若把[多馬福音裏的耶穌的名字換成](../Page/多馬福音.md "wikilink")[佛陀](../Page/佛陀.md "wikilink")，經文的許多教誨都和佛經所說的一樣\[4\]。學者\[5\]及[愛德華·公茲](../Page/愛德華·公茲.md "wikilink")\[6\]亦持相似意見，認為印度人和諾斯底教派有接觸，並指公元三世紀亞述的諾斯底教派神學家巴·達伊臣亦曾和印度交換使節。

受古印度[怛特羅密教](../Page/怛特羅密教.md "wikilink")（[性力派](../Page/性力派.md "wikilink")）傳統的影響，部份佛教中晚期宗派追求極樂悟道，特別是[密宗](../Page/密宗.md "wikilink")（[怛特羅](../Page/怛特羅.md "wikilink")，[金剛乘](../Page/金剛乘.md "wikilink")）以慾望作為道德驅動力的觀念上，在[唐朝和](../Page/唐朝.md "wikilink")[宋朝時傳入中國](../Page/宋朝.md "wikilink")，並對[元朝產生很大的政治影響](../Page/元朝.md "wikilink")；由於秘傳故，被世人誤解為[邪教歪道](../Page/邪教.md "wikilink")。以致成為宋朝後激發[理學的勃興](../Page/理學.md "wikilink")、恢復儒學及文人的政治地位的導因之一。

## 参见

  - [諾斯底主義歷史](../Page/諾斯底主義歷史.md "wikilink")
  - [諾斯底教](../Page/諾斯底教.md "wikilink")
  - [西比拉神谕](../Page/西比拉神谕.md "wikilink")
  - [多馬福音](../Page/多馬福音.md "wikilink")
  - [拿戈瑪第經集](../Page/拿戈瑪第經集.md "wikilink")
  - [佛教與基督教](../Page/佛教與基督教.md "wikilink")
  - [摩尼教](../Page/摩尼教.md "wikilink")

## 注释

## 参考文献

{{-}}

[諾斯底主義](../Category/諾斯底主義.md "wikilink")
[Category:基督教運動及主義](../Category/基督教運動及主義.md "wikilink")
[Category:各種宗教宗派及運動](../Category/各種宗教宗派及運動.md "wikilink")

1.  約翰．甘乃迪，《見証的火炬－兩千年教會的屬靈歷史》，劉志雄譯（台北：提比哩亞出版社，1997），74。
2.  陶理，《基督教兩千年史》（香港：海天書樓，2001）96。
3.  漢斯·約納斯著，張新樟譯，《諾斯替宗教》（上海：上海三聯書店，2006，105-120）
4.
5.
6.