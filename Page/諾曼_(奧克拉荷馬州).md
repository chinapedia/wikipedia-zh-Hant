**諾曼**（Norman,
Oklahoma）是[美國](../Page/美國.md "wikilink")[俄克拉何馬州](../Page/俄克拉何馬州.md "wikilink")[克里夫蘭縣的縣治](../Page/克里夫蘭縣_\(奧克拉荷馬州\).md "wikilink")。面積490.8平方公里，2006年人口102,827人，是該州第三大城市。\[1\]

[奧克拉荷馬大學位於本市](../Page/奧克拉荷馬大學.md "wikilink")。

## 姐妹城市

  - [京都府](../Page/京都府.md "wikilink")[精華町](../Page/精華町.md "wikilink")

  - [克萊蒙費朗](../Page/克萊蒙費朗.md "wikilink")

  - [科利馬](../Page/科利馬.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[N](../Category/俄克拉何馬州城市.md "wikilink")
[Category:俄克拉何马州克里夫兰县城市](../Category/俄克拉何马州克里夫兰县城市.md "wikilink")

1.  [Norman city, Oklahoma- Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=&geo_id=16000US4052500&_geoContext=01000US%7C04000US40%7C16000US4052500&_street=&_county=Norman+city&_cityTown=Norman+city&_state=04000US40&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=160&_submenuId=population_0&ds_name=DEC_2000_SAFF&_ci_nbr=null&qr_name=&reg=%3Anull&_keyword=&_industry=)