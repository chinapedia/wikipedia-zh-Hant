[Nordkapp_map.png](https://zh.wikipedia.org/wiki/File:Nordkapp_map.png "fig:Nordkapp_map.png")
[Kinnarodden1000.jpg](https://zh.wikipedia.org/wiki/File:Kinnarodden1000.jpg "fig:Kinnarodden1000.jpg")

**諾爾辰角**（Cape
Nordkinn或Kinnarodden）在是[挪威大陸的最北點](../Page/挪威.md "wikilink")，所以同時亦是[歐洲大陸的最北點](../Page/歐洲.md "wikilink")。諾爾辰角位處諾爾辰半島（Nordkyn
peninsula），距離[芬馬克郡](../Page/芬馬克郡.md "wikilink")（Finnmark）的[加姆維克](../Page/加姆維克.md "wikilink")（Gamvik）約為20公里。

## 兩個最北點

諾爾辰角在北緯71度8分2秒，與挪威的[北角](../Page/北角_\(挪威\).md "wikilink")（Nordkapp）或是（Knivskjellodden，歐洲官方的最北點）的南面，相距兩分。但因為北角在[馬格爾島](../Page/馬格爾島.md "wikilink")（Magerøya）上，北角不能算是“歐陸”的最北點。諾爾辰角距[北極](../Page/北極.md "wikilink")2106.6公里，比較北角更南6公里。

## 旅遊

與擁有廣泛旅遊基本建設及大量訪客的北角相比，諾爾辰角則是一個較為寂靜的地方。若要參觀諾爾辰角就只能由加姆維克（單程距離為廿三公里）徒步旅行一整天到達，而回程亦要花整整一天。諾爾辰角亦可以由船到達。芬馬克郡的旅客事務處能夠提供船隻旅遊的相關資料。諾爾辰角亦可以由[挪威沿岸遊船](../Page/挪威沿岸遊船.md "wikilink")（Hurtigruten）看到。

## 參見

  - [歐洲地理](../Page/歐洲地理.md "wikilink")

## 外部連結

  - [Tourist info from Gamvik municipality about
    hiking](https://web.archive.org/web/20060614141501/http://www.gamvik.kommune.no/top-of-europe-kinnarodden.256644-17949.html)
  - [Map over hiking
    trail](https://web.archive.org/web/20060528074952/http://www.gamvik.kommune.no/getfile.php/149783.646/Tursti.htm)
  - [Boat tours to the Cape
    Nordkinn](https://web.archive.org/web/20071008141422/http://www.nordicsafari.no/en.php/summer_adventures/tours_to_cape_nordkyn/)
  - [Nordic Safari AS, Mehamn](http://www.nordicsafari.no/)

[N](../Category/挪威地理.md "wikilink") [N](../Category/海岬.md "wikilink")