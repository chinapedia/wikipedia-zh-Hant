**諾瓦塔縣**（）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州東北部的一個縣](../Page/奧克拉荷馬州.md "wikilink")，北鄰[堪薩斯州](../Page/堪薩斯州.md "wikilink")，面積1,504平方公里。根據[2010年的人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口10,536人\[1\]。本縣縣治為[諾瓦塔](../Page/諾瓦塔_\(奧克拉荷馬州\).md "wikilink")（）。

## 歷史

[Oklahoma1885.jpg](https://zh.wikipedia.org/wiki/File:Oklahoma1885.jpg "fig:Oklahoma1885.jpg")
幾千年來，諾瓦塔縣一帶都是印第安人的家園。《俄克拉何馬州歷史和文化百科全書》（）指出考古證據表明，人類在這一帶的首個定居點為[弗迪格里斯河山谷](../Page/弗迪格里斯河.md "wikilink")，而這些人類是在6000年前遷入的。\[2\]在17世紀，白人首次到達這一帶時發現這裡已被歐塞奇族和誇保族佔據。於1803年，[路易斯安那購地案後](../Page/路易斯安那購地.md "wikilink")，諾瓦塔縣一帶正式被納入歐塞奇族的領地中。於1819年，[阿肯色領地正式組織完成](../Page/阿肯色領地.md "wikilink")。於1828年，美國政府與[切諾基國的協議令到諾瓦塔縣一帶被納入切諾基國](../Page/切諾基國.md "wikilink")。於1867年，切諾基國與特拉華人達成協議，讓特拉華人能夠遷居本縣一帶。\[3\]

本縣於1890年成立，縣名來自縣治名。而縣治名在[特拉華語有](../Page/特拉華人.md "wikilink")「歡迎」的意思。\[4\]\[5\]\[6\]

## 地理

[USACE_Newt_Graham_Lock_and_Dam.jpg](https://zh.wikipedia.org/wiki/File:USACE_Newt_Graham_Lock_and_Dam.jpg "fig:USACE_Newt_Graham_Lock_and_Dam.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，克雷格縣的總面積為，其中有，即97.26%為陸地；，即2.74%為水域。\[7\][弗迪格里斯河將本縣分成東西兩部份](../Page/弗迪格里斯河.md "wikilink")。\[8\]

### 主要公路

  - [US_60.svg](https://zh.wikipedia.org/wiki/File:US_60.svg "fig:US_60.svg")
    [美國60號公路](../Page/美國60號公路.md "wikilink")
  - [US_169.svg](https://zh.wikipedia.org/wiki/File:US_169.svg "fig:US_169.svg")
    [美國169號公路](../Page/美國169號公路.md "wikilink")
  - [Oklahoma_State_Highway_10.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_10.svg "fig:Oklahoma_State_Highway_10.svg")
    [10號高速公路](../Page/10號高速公路_\(奧克拉荷馬州\).md "wikilink")
  - [Oklahoma_State_Highway_28.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_28.svg "fig:Oklahoma_State_Highway_28.svg")
    [28號高速公路](../Page/28號高速公路_\(奧克拉荷馬州\).md "wikilink")

### 毗鄰縣

  - [奧克拉荷馬州](../Page/奧克拉荷馬州.md "wikilink")
    [克雷格縣](../Page/克雷格縣_\(奧克拉荷馬州\).md "wikilink")：東方
  - 奧克拉荷馬州 [羅傑斯縣](../Page/羅傑斯縣_\(奧克拉荷馬州\).md "wikilink")：南方
  - 奧克拉荷馬州 [華盛頓縣](../Page/華盛頓縣_\(奧克拉荷馬州\).md "wikilink")：西方
  - [堪薩斯州](../Page/堪薩斯州.md "wikilink")
    [蒙哥馬利縣](../Page/蒙哥馬利縣_\(堪薩斯州\).md "wikilink")：北方
  - 堪薩斯州 [拉貝特縣](../Page/拉貝特縣_\(堪薩斯州\).md "wikilink")：東北方

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，諾瓦塔縣擁有10,569居民、4,147住戶和2,989家庭。\[9\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")19居民（每平方公里7居民）。\[10\]本縣擁有6,459間房屋单位，其密度為每平方英里8間（每平方公里3間）。\[11\]而人口是由72.43%[白人](../Page/歐裔美國人.md "wikilink")、2.46%[黑人](../Page/非裔美國人.md "wikilink")、16.56%[土著](../Page/美國土著.md "wikilink")、0.12%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.26%其他[種族和](../Page/種族.md "wikilink")8.17%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")1.23%。\[12\]

在4,147住户中，有31.8%擁有一個或以上的兒童（18歲以下）、58.8%為夫妻、9.8%為單親家庭、27.9%為非家庭、25.5%為獨居、13.3%住戶有同居長者。平均每戶有2.5人，而平均每個家庭則有2.97人。在10,569居民中，有26.1%為18歲以下、7.6%為18至24歲、25.3%為25至44歲、23.7%為45至64歲以及17.3%為65歲以上。人口的年齡中位數為39歲，女子對男子的性別比為100：96.7。成年人的性別比則為100：93.2。\[13\]

本縣的住戶收入中位數為$29,470，而家庭收入中位數則為$36,354。男性的收入中位數為$27,047，而女性的收入中位數則為$19,371，[人均收入為](../Page/人均收入.md "wikilink")$14,244。約9%家庭和14.1%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括18%兒童（18歲以下）及11.3%長者（65歲以上）。\[14\]

## 参考文献

[N](../Category/俄克拉何马州行政区划.md "wikilink")

1.  [2010 census data](http://2010.census.gov/2010census/data/)

2.
3.
4.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.

5.  [Cheatham, Gary L. *Encyclopedia of Oklahoma History and Culture*:
    "Nowata County." Retrieved October 1,
    2011.](http://digital.library.okstate.edu/encyclopedia/entries/N/NO018.html)


6.  ["Origin of County Names in Oklahoma." In:*Chronicles of Oklahoma*.
    Volume 2, Number 1. March
    1924.](http://digital.library.okstate.edu/chronicles/v002/v002p075.html)
    Retrieved October 3, 2013.

7.

8.
9.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

10. [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

11. [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

12. [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

13. [American FactFinder](http://factfinder.census.gov/)

14.