**諾福克**（**Norfolk**  ；
）是[美國](../Page/美國.md "wikilink")[維吉尼亞州東南部](../Page/維吉尼亞州.md "wikilink")[漢普頓錨地的一個](../Page/漢普頓錨地.md "wikilink")[獨立市](../Page/獨立市_\(美国\).md "wikilink")，是該州第二大城市。

諾福克成立於1945年2月11日。城名來自[英國的](../Page/英國.md "wikilink")[諾福克郡](../Page/諾福克郡.md "wikilink")。\[1\][諾福克海军基地是美國最大的](../Page/諾福克海军基地.md "wikilink")[海军基地](../Page/海军基地.md "wikilink")，美國海軍絕大部分艦艇都在這裡生産。

## 地理与气候

诺福克属[副热带湿润气候](../Page/副热带湿润气候.md "wikilink")，四季分明，气温变化相对和缓，全年降水分配均匀。冬季温和，微潮，时而偏向温暖，日最高气温低于的平均日数为3.3天，日最低气温低于的平均日数为46天，低于的平均日数为8.6天；夏季相对炎热潮湿，日最高气温超过的日数年均有64天，超过的有9.1天。\[2\]最冷月（1月）均温，极端最低气温（1985年1月21日）。\[3\]最热月（7月）均温，极端最高气温（上次出现于2010年7月24日与7月25日）。\[4\]无霜期平均为250天（3月18日至11月22日）。\[5\]年均降水量约，年极端最少降水量为（1986年），最多为（1889年）。\[6\]年均降雪量为，\[7\]但降雪年际变化较大；1998–99年的降雪量最少，积累降雪量微少以致难以测量，1979–80年的降雪量最多，积累降雪量为。\[8\]

[Norfolk,_VA.jpg](https://zh.wikipedia.org/wiki/File:Norfolk,_VA.jpg "fig:Norfolk,_VA.jpg")
[Norfolk_Scope.jpg](https://zh.wikipedia.org/wiki/File:Norfolk_Scope.jpg "fig:Norfolk_Scope.jpg")

## 姐妹城市

  - [北九州市](../Page/北九州市.md "wikilink")（1963年）

  - [威廉港](../Page/威廉港.md "wikilink")（1976年）

  - [諾福克郡](../Page/諾福克郡.md "wikilink")（1986年）

  - [土倫](../Page/土倫.md "wikilink")（1989年）

  - [加里寧格勒](../Page/加里寧格勒.md "wikilink")（1992年）

  - [哈利法克斯](../Page/哈利法克斯.md "wikilink")（2006年）\[9\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:弗吉尼亚州城市](../Category/弗吉尼亚州城市.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.

2.
3.
4.
5.
6.
7.
8.
9.  Sister Cities designated by [Sister Cities International,
    Inc.（SCI）](http://www.sister-cities.org/icrc/directory/usa/VA)
    ，擷取於2006年8月18日。