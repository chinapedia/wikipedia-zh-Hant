**諾芭晚·樂吉佤淦**（[泰語](../Page/泰語.md "wikilink")：，）是[泰王國的](../Page/泰王國.md "wikilink")[網球](../Page/網球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，當今世界女子（少年）排名第二。

諾芭晚1991年11月18日生於[曼谷](../Page/曼谷.md "wikilink")。2005年年僅15歲的諾芭晚即代表泰國參加[聯合會盃比賽](../Page/聯合會盃.md "wikilink")，2006年參加[多哈亞運會](../Page/多哈亞運會.md "wikilink")，後來更參加多次國際網球聯合會的亞洲區域比賽。並奪得五項比賽冠軍。她的教練是Marek
Malaszszak。

2008年6月，她參加[溫布爾登網球公開賽女子單打少女組的比賽](../Page/溫布爾登網球公開賽.md "wikilink")，一路過關斬將進入決賽，雖然在決賽里不敵佔盡天時、地利、人和的東道主運動員[勞拉·羅布森](../Page/勞拉·羅布森.md "wikilink")（比分：1-2），卻創造了[亞洲少年女子選手在該項賽事中的最佳記錄](../Page/亞洲.md "wikilink")。獲得獎金12,220英鎊。

## 獲得比賽冠軍

  - Asian Closed Junior Tennis Championships （2008年）
  - 19th Mitsubishi-Lancer International Juniors Championships （2008年）
  - 11th Sarawak Chief Minister's Cup ITF Junior Tennis Championships
    （2008年）
  - Asia/Oceania Closed Championships （2007年）
  - TrueVisions Thailand Open （2007年）

## 獲得比賽亞軍

  - 溫布爾登網球公開賽女子少年組 （2008年）

## 外部連結

  -
  -
  - [Fed Cup - Player profile - Noppawan LERTCHEEWAKARN
    (THA)](http://www.fedcup.com/en/players/player/profile.aspx?playerid=100078927)

[分类:泰国网球运动员](../Page/分类:泰国网球运动员.md "wikilink")