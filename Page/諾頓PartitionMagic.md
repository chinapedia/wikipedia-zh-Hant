**Norton
PartitionMagic**是最初由[PowerQuest公司所開發的](../Page/PowerQuest.md "wikilink")[個人電腦](../Page/個人電腦.md "wikilink")[硬碟分割](../Page/硬碟分割.md "wikilink")[軟件](../Page/軟件.md "wikilink")，而該公司今為[賽門鐵克公司收購](../Page/賽門鐵克.md "wikilink")。其適用之環境為[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")
[作業系統或由引導](../Page/作業系統.md "wikilink")[光盤而適用於裝有任何作業系統和沒有作業系統的個人電腦](../Page/光盤.md "wikilink")。現有分區可以在不受數據損失的情況下調整大小。

## 特點

Norton Partition
Magic可在無損數據的情況下對NTFS或FAT16/32的磁盤分區進行複製、移動和調整大小，可以實現FAT16，FAT32和NTFS分區之間的格式轉換，可以修改FAT16/32和NTFS文件系統的文件簇的大小，可以合併相鄰的FAT或NTFS文件系統，還有限地支持EXT2和EXT3文件系統。

## 詬病

PartitionMagic在PowerQuest時代保持定期的版本更新，持續添加有用的新特性。但自PowerQuest被Symantec收購後，PartitionMagic便再無新版本問世，Symantec聲明沒有發佈PartitionMagic新版本的計劃。\[1\]

## 參考資料

<references/>

## 參見

  - [硬碟分割](../Page/硬碟分割.md "wikilink")
  - [FIPS](../Page/FIPS.md "wikilink")
  - [GNU Parted](../Page/GNU_Parted.md "wikilink")
  - [QtParted](../Page/QtParted.md "wikilink")
  - [Paragon Partition
    Manager](../Page/Paragon_Partition_Manager.md "wikilink")
  - [Acronis Disk Director
    Suite](../Page/Acronis_Disk_Director_Suite.md "wikilink")

## 外部連結

  - [Partition Magic home
    page](http://www.symantec.com/home_homeoffice/products/overview.jsp?pcid=sp&pvid=pm80)

[Category:Partitions
managers](../Category/Partitions_managers.md "wikilink")
[Category:賽門鐵克軟件](../Category/賽門鐵克軟件.md "wikilink")
[Category:Windows軟體](../Category/Windows軟體.md "wikilink")
[Category:付費軟體](../Category/付費軟體.md "wikilink")

1.