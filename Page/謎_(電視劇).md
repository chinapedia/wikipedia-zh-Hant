《**謎**》（*puzzle*）是由[石原里美主演的電視劇](../Page/石原里美.md "wikilink")，[朝日放送](../Page/朝日放送.md "wikilink")（ABC）跟[朝日電視台所共同制作](../Page/朝日電視台.md "wikilink")，從2008年4月18日起，每週五21:00～21:54（日本時間），在朝日電視台系列播放。

## 故事簡介

## 登場人物

### 近松大學附屬辯秀高等學校

#### 教師職員

  -
    **鮎川
    美沙子**（[石原里美飾](../Page/石原里美.md "wikilink")）（香港配音：[朱妙蘭](../Page/朱妙蘭.md "wikilink")）
    **大道 吾郎**（[金児憲史飾](../Page/金児憲史.md "wikilink")）

#### 學生

  -
    **今村 真一**（[山本裕典飾](../Page/山本裕典.md "wikilink")）

<!-- end list -->

  -
    **神崎 明**（[木村了飾](../Page/木村了.md "wikilink")）

<!-- end list -->

  -
    **塚本 善雄**（[永山絢斗飾](../Page/永山絢斗.md "wikilink")）

### 櫻葉女學院

  -
    **松尾 裕子(松尾 ゆうこ)**([岩田小百合飾](../Page/岩田小百合.md "wikilink"))

<!-- end list -->

  -
    **高村 谷子(高村 みちる)**([佐藤千亜妃飾](../Page/佐藤千亜妃.md "wikilink"))

<!-- end list -->

  -
    **和田 瞳子(和田 ひとみ)**([朝倉亞紀飾](../Page/朝倉亞紀.md "wikilink"))

### 警察

  -
    **鎌田 虎彦**([塩見三省飾](../Page/塩見三省.md "wikilink"))

<!-- end list -->

  -
    **長塚 健一**([辻谷嘉真飾](../Page/辻谷嘉真.md "wikilink"))

### 其他人物

## 主題曲

  - 「[Moon Crying](../Page/MOON_\(倖田來未單曲\).md "wikilink")」 -
    [倖田來未](../Page/倖田來未.md "wikilink")

## 播放日期、副題、收視率

<table>
<thead>
<tr class="header">
<th><p>各話</p></th>
<th><p>播放日</p></th>
<th><p>原文副標</p></th>
<th><p>中文副標</p></th>
<th><p>劇本</p></th>
<th><p>導演</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Piece1</p></td>
<td><p>2008年4月18日</p></td>
<td><p>{{lang|ja|強欲教師と旧家の暗号! 死を呼ぶ遺言</p></td>
<td><p>}}</p></td>
<td></td>
<td><p>蒔田光治</p></td>
<td><p>片山修</p></td>
</tr>
<tr class="even">
<td><p>Piece2</p></td>
<td><p>2008年4月25日</p></td>
<td></td>
<td></td>
<td><p>宮下健作</p></td>
<td><p>10.9%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Piece3</p></td>
<td><p>2008年5月2日</p></td>
<td></td>
<td></td>
<td><p>片山修</p></td>
<td><p>11.0%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Piece4</p></td>
<td><p>2008年5月9日</p></td>
<td></td>
<td></td>
<td><p>宮下健作</p></td>
<td><p>9.2%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Piece5</p></td>
<td><p>2008年5月16日</p></td>
<td></td>
<td></td>
<td><p>遠藤彩見</p></td>
<td><p>片山修</p></td>
<td><p>10.2%</p></td>
</tr>
<tr class="even">
<td><p>Piece6</p></td>
<td><p>2008年5月23日</p></td>
<td></td>
<td></td>
<td><p>蒔田光治</p></td>
<td><p>8.9%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Piece7</p></td>
<td><p>2008年5月30日</p></td>
<td></td>
<td></td>
<td><p>高橋伸之</p></td>
<td><p>9.5%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Piece8</p></td>
<td><p>2008年6月6日</p></td>
<td></td>
<td></td>
<td><p>林壮太郎</p></td>
<td><p>宮下健作</p></td>
<td><p><font color=blue>8.0%</p></td>
</tr>
<tr class="odd">
<td><p>Piece9</p></td>
<td><p>2008年6月13日</p></td>
<td></td>
<td></td>
<td><p>蒔田光治</p></td>
<td><p>木村政和</p></td>
<td><p>8.4%</p></td>
</tr>
<tr class="even">
<td><p>Final Piece</p></td>
<td><p>2008年6月20日</p></td>
<td></td>
<td></td>
<td><p>大野敏哉</p></td>
<td><p>片山修</p></td>
<td><p>10.0%</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [官方網站](http://puzzle.asahi.co.jp/)

## 作品的變遷

[Category:2008年日本電視劇集](../Category/2008年日本電視劇集.md "wikilink")
[Category:朝日電視台週五晚間九點連續劇](../Category/朝日電視台週五晚間九點連續劇.md "wikilink")
[Category:教師主角電視劇](../Category/教師主角電視劇.md "wikilink")
[Category:日本推理劇](../Category/日本推理劇.md "wikilink")
[Category:日本校園劇](../Category/日本校園劇.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")