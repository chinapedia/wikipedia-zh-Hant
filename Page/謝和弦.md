**謝和弦**（），華語流行音樂、台語流行音樂\[1\]的創作歌手，臺灣南投縣埔里人，2014年1月8日加盟華納唱片。2016年5月20日，与女友戴怡軒(Keanna)结婚。

## 音樂作品

### 錄音室專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯</p></th>
<th style="text-align: left;"><p>名稱</p></th>
<th style="text-align: left;"><p>發行時間</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>1st</strong></p></td>
<td style="text-align: left;"><p>《<a href="../Page/雖然很芭樂.md" title="wikilink">雖然很芭樂</a>》</p></td>
<td style="text-align: left;"><p>2009年4月15日</p></td>
<td style="text-align: left;"><p><a href="../Page/亞神音樂.md" title="wikilink">亞神音樂</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2nd</strong></p></td>
<td style="text-align: left;"><p>《<a href="../Page/於是長大了以後.md" title="wikilink">於是長大了以後</a>》</p></td>
<td style="text-align: left;"><p>2011年5月27日</p></td>
<td style="text-align: left;"><p><a href="../Page/亞神音樂.md" title="wikilink">亞神音樂</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>3rd</strong></p></td>
<td style="text-align: left;"><p>《不需要裝乖》</p></td>
<td style="text-align: left;"><p>2015年6月5日</p></td>
<td style="text-align: left;"><p><a href="../Page/華納音樂_(台灣).md" title="wikilink">華納音樂</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>4th</strong></p></td>
<td style="text-align: left;"><p>《<a href="../Page/要你知道.md" title="wikilink">要你知道</a>》</p></td>
<td style="text-align: left;"><p>2016年8月26日</p></td>
<td style="text-align: left;"><p><a href="../Page/華納音樂_(台灣).md" title="wikilink">華納音樂</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>5th</strong></p></td>
<td style="text-align: left;"><p>《<a href="../Page/像水一樣.md" title="wikilink">像水一樣</a>》</p></td>
<td style="text-align: left;"><p>2018年12月14日</p></td>
<td style="text-align: left;"><p><a href="../Page/華納音樂_(台灣).md" title="wikilink">華納音樂</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 已發表的創作

| 發行日期     | 專輯收錄                                                                   | 歌曲                                   | 主唱                                                                                                                                          | 作詞                                                                  | 作曲                                                            | 備註                                  |
| -------- | ---------------------------------------------------------------------- | ------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------- | ------------------------------------------------------------- | ----------------------------------- |
| 2005年12月 | 《[終極一班電視原聲帶](../Page/終極一班.md "wikilink")》                              | 終極一班                                 | [Tank](../Page/Tank.md "wikilink")                                                                                                          | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [Tank](../Page/Tank.md "wikilink")                            | 和唱：[謝和弦](../Page/謝和弦.md "wikilink") |
| 2005年12月 | 《[終極一班電視原聲帶](../Page/終極一班.md "wikilink")》                              | 從今以後                                 | [Tank](../Page/Tank.md "wikilink")                                                                                                          | [謝和弦](../Page/謝和弦.md "wikilink")/[Tank](../Page/Tank.md "wikilink") | [Tank](../Page/Tank.md "wikilink")                            |                                     |
| 2007年8月  | [唐禹哲](../Page/唐禹哲.md "wikilink")《愛我》                                   | 只欠一句 我愛你                             | [唐禹哲](../Page/唐禹哲.md "wikilink")                                                                                                            | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [謝和弦](../Page/謝和弦.md "wikilink")                              |                                     |
| 2007年8月  | 《[終極一家電視原聲帶](../Page/終極一家.md "wikilink")》                              | 嗚啦巴哈                                 | [a Chord](../Page/a_Chord.md "wikilink")、[汪東城](../Page/汪東城.md "wikilink")、[藍心湄](../Page/藍心湄.md "wikilink")、[黃小柔](../Page/黃小柔.md "wikilink") | [謝和弦](../Page/謝和弦.md "wikilink")、顏璽軒                                |                                                               | 印尼歌曲                                |
| 2007年8月  | 《[東城衛 & R.Chord-合作創作單曲](../Page/東城衛.md "wikilink")》                    | 夠愛                                   | [謝和弦](../Page/謝和弦.md "wikilink")                                                                                                            | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [陈德脩](../Page/陈德脩.md "wikilink")                              |                                     |
| 2007年12月 | 《[惡作劇2吻電視原聲帶](../Page/惡作劇2吻.md "wikilink")》                            | 你曾經讓我心動                              | [謝和弦](../Page/謝和弦.md "wikilink")                                                                                                            | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [謝和弦](../Page/謝和弦.md "wikilink")                              |                                     |
| 2009年    | 《[強辯之【終極三國】三團鼎立SUPER大鬥陣](../Page/強辯之【終極三國】三團鼎立SUPER大鬥陣.md "wikilink")》 | 夠愛                                   | [曾沛慈](../Page/曾沛慈.md "wikilink")                                                                                                            | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [東城衛](../Page/東城衛.md "wikilink")-[脩](../Page/脩.md "wikilink") | 原唱：[謝和弦](../Page/謝和弦.md "wikilink") |
| 2011年    | 《[月球漫步](../Page/JPM.md "wikilink")》                                    | 那不是雪中紅                               | [JPM](../Page/JPM.md "wikilink")                                                                                                            | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [謝和弦](../Page/謝和弦.md "wikilink")                              |                                     |
| 2012年8月  | 《LanDIVA》演唱會專輯                                                         | Say Yeah                             | [藍心湄](../Page/藍心湄.md "wikilink")                                                                                                            | R-Chord、克麗絲叮（Christine Welch）                                       | [謝和弦](../Page/謝和弦.md "wikilink")、Skot Suyama（陶山）              |                                     |
| 2012年8月  | 《LanDIVA》演唱會專輯                                                         | 到底是哪個混蛋敢欺負我的朋友                       | [藍心湄](../Page/藍心湄.md "wikilink")                                                                                                            | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [謝和弦](../Page/謝和弦.md "wikilink")                              |                                     |
| 2014年4月  | 《擁抱失敗》數位單曲                                                             | 擁抱失敗                                 | [謝和弦](../Page/謝和弦.md "wikilink")                                                                                                            | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [謝和弦](../Page/謝和弦.md "wikilink")                              |                                     |
| 2018年8月  |                                                                        | [台灣的未來](../Page/台灣的未來.md "wikilink") | [謝和弦](../Page/謝和弦.md "wikilink")                                                                                                            | [謝和弦](../Page/謝和弦.md "wikilink")                                    | [謝和弦](../Page/謝和弦.md "wikilink")                              |                                     |
|          |                                                                        |                                      |                                                                                                                                             |                                                                     |                                                               |                                     |

## 全創作Demo清單

<div class="NavFrame collapsed" style="margin:0.5em auto;">

<div class="NavHead" style="text-align: center; padding-left:1em;">

謝和弦創作Demo列表，請按顯示展開

</div>

<div class="NavContent">

從開始創作，至今145首（按筆劃排序）

  - 18禁
  - 2007向前走（謝和弦）
  - 2008痴人唱夢
  - 7年6班社會學
  - Crazy Love
  - Fuck禁掰掰
  - God Damn
  - Happy Man
  - I Don't Know Why
  - I'm Not Bad Boy
  - It's My Way
  - Oh My Girl
  - 一直走下去
  - 一個人的台北車站
  - 一笑帶過
  - 一笑帶過2008
  - 十字架上的耶穌
  - 大什麼大 人什麼人
  - 大拇指小指頭
  - 大時代的偶像劇
  - 大時代的悲劇
  - 不會
  - 不過是想
  - 太多
  - 心碎社會
  - 心裡的話
  - 世界最遙遠的距離
  - 主耶穌在你心中
  - 他們都唱張學友
  - 功夫
  - 北平烤鴨
  - 只欠一句我愛你
  - 只欠一句對哩欸愛
  - 只肯讓你懂
  - 台灣學生必聽的歌
  - 正妹老師
  - 甘願
  - 白色巨塔
  - 白痴兄弟
  - 光害
  - 同病相憐
  - 吃飽寶 睡飽寶
  - 在沒有你以後
  - 在異鄉的夜
  - 多想告訴你
  - 好笑了
  - 好聚怎麼好散
  - 安安
  - 安蒐蕊
  - 有你作伴
  - 有沒有那麼誇張
  - 你是真的離開
  - 你曾經讓我心動
  - 忍住不掉的淚
  - 我只有大睡 我沒有呼呼
  - 我來不及
  - 我的天使是你
  - 我的英文很弱
  - 我的第一次
  - 我是真的愛上了你
  - 我們之間
  - 我們要學會珍惜
  - 我們都不是誰的全部
  - 我們都成了大人
  - 我們說好的
  - 我真的了
  - 我真的都明白
  - 我給的溫柔
  - 我腦海中的希雷夢
  - 找尋
  - 每一個玩音樂的人都是希望
  - 芒果亂報之老師有講嘛 不聽 不聽 不聽
  - 乖乖認輸
  - 依賴
  - 到底是哪個混蛋敢欺負我的朋友
  - 孤單聖誕節
  - 幸福計畫
  - 於是長大了以後
  - 爸爸 辛苦了
  - 牧羊人
  - 牧羊人2008
  - 玩命關頭
  - 芭樂大總會
  - 阿不是要長大
  - 阿嬤的溫柔
  - 叛變
  - 流星雨
  - 活在當下 不是褲襠下
  - 為了你存在
  - 相思病
  - 借過
  - 海平面
  - 真理
  - 神經病
  - 秘密禮物
  - 草莓族
  - 起飛
  - 偷偷心碎
  - 夠愛
  - 情人都去死吧
  - 情和義　值千金
  - 教師節快樂
  - 晚安夜之美
  - 這是最後一次
  - 這是最後一次依賴
  - 都是有故事的人
  - 都是為了你
  - 最佳選擇
  - 插撥
  - 無法定義
  - 等我回來
  - 陽明山的聖誕節
  - 愛你
  - 愛吧
  - 愛的鼓勵預備來 小明之歌
  - 愛情大傻瓜
  - 愛情墳場
  - 新我
  - 會不會
  - 歲月如梭 還有你陪我
  - 過來人
  - 夢幻女郎
  - 夢是半徑
  - 夢真的實現
  - 對你的愛
  - 對你愛不完
  - 摸著良心問自己
  - 睡美人
  - 嘿 我親愛的女孩
  - 請你不通嫌棄我說台語
  - 請原諒我的真
  - 誰可以明白
  - 醉後的答案
  - 曖昧
  - 謊言
  - 還有雨陪我
  - 還有雨陪我2004
  - 還是一樣挺你
  - 雖然很芭樂
  - 雜草精神
  - 羅馬不是一天造成的
  - 關於
  - 體會
  - 不要臉
  - 擁抱失敗

### 合作歌曲Demo清單

  - 卡美卡美卡
  - 別在耍我
  - 牧羊人2008
  - 披著羊皮的狼
  - 門
  - 時間.現實VS鋼琴手.意志
  - 啊不是要去海邊
  - 牽心萬苦
  - 對你說
  - 蘋果

### 其它歌曲Demo清單

  - 東區現實麵
  - 阿梅我愛你
  - 猴子之歌
  - 舊情也綿綿
  - 你怎麼捨得我難過
  - 玉米慄
  - 希雷夢一號
  - 謝和弦
  - 登登登登
  - 概
  - 概2
  - 希雷夢二號
  - 後青春是什麼輓歌
  - 那不是雪中紅

### 2012 DEMO

  - 我每天早上都被自己給帥醒
  - 你再犯規我就要把你吃掉了喔喔
  - 把不起妹妹 把不起妹妹就巴頭
  - 打不死的小強
  - 你是真的離開我（完整版）
  - 我不是白馬王子 但妳是我的公主
  - 光是看到你就勃起
  - 被逼死的那五人

|  |
|  |
|  |

</div>

</div>

## MV

  - [張智成](../Page/張智成.md "wikilink")-CAP GIRL
  - [S.H.E](../Page/S.H.E.md "wikilink")-安全感
  - [唐禹哲](../Page/唐禹哲.md "wikilink")-愛我
  - [蔡健雅](../Page/蔡健雅.md "wikilink")-晨間新聞
  - **謝和弦**-雖然很芭樂
  - **謝和弦**-對你愛不完
  - **謝和弦**-過來人
  - **謝和弦**-請你不通嫌棄我
  - **謝和弦**-關於
  - **謝和弦**&海平面樂團-地球其實沒有那麼危險
  - **謝和弦**-於是長大了以後
  - **謝和弦**Feat.[徐佳瑩](../Page/徐佳瑩.md "wikilink")-柳樹下
  - **謝和弦**-寂寞瘋了
  - **謝和弦**-牽心萬苦
  - **謝和弦**-後青春是什麼輓歌
  - **謝和弦**&海平面樂團-台北台北
  - **謝和弦**-我不會讓你失望
  - **謝和弦**-擁抱失敗
  - **謝和弦**Feat.[王詩安](../Page/王詩安.md "wikilink")-愛不需要裝乖
  - **謝和弦**-這是最後一次
  - **謝和弦**-本事
  - **謝和弦**-每一個玩音樂的人
  - **謝和弦**-你甘攏袂不甘
  - **謝和弦**-謝謝你愛我
  - **謝和弦**-蘋果
  - **謝和弦**-在沒有你以後
  - **謝和弦**Feat.[陳零九](../Page/陳零九.md "wikilink")、[玖壹壹](../Page/玖壹壹.md "wikilink")-每一個玩音樂的人
  - **謝和弦**Feat.[李佳薇](../Page/李佳薇.md "wikilink")-不愛，也是愛我
  - **謝和弦**-光害
  - **謝和弦**-就是在講你喔
  - **謝和弦**-[台灣的未來](../Page/台灣的未來.md "wikilink")
  - **謝和弦**-像水一樣
  - **謝和弦**-備胎 (feat. Eetu Kalavainen)
  - **謝和弦**-It's You (feat. [王艷薇](../Page/王艷薇.md "wikilink"))
  - **謝和弦**-安怎講(feat. [阿夜](../Page/林志融.md "wikilink"))
  - **謝和弦**-我不是白馬王子 (feat. 高爾宣、李杰明)
  - **謝和弦**-就是在講你喔
  - **謝和弦**-幸福不上鎖 (feat. 林京燁)
  - **謝和弦**-那不是雪中紅
  - **謝和弦**-酷
  - **謝和弦**-依賴
  - **謝和弦**-你是真的離開我

## 電視劇

| 年度       | 播出頻道                                                                | 劇名                                                    | 飾演      |
| -------- | ------------------------------------------------------------------- | ----------------------------------------------------- | ------- |
| 2004年8月  | [TVBS-G](../Page/TVBS-G.md "wikilink")                              | 《香草戀人館》                                               | 客串演出    |
| 2005年11月 | [八大電視台](../Page/八大電視台.md "wikilink")                                | 《[終極一班](../Page/終極一班.md "wikilink")》                  | 鯊魚      |
| 2006年    | [公共電視台](../Page/公共電視台.md "wikilink")                                | 《畢業生》－黑色暑假                                            | 阿國      |
| 2006年11月 | [華視](../Page/華視.md "wikilink")、[八大電視台](../Page/八大電視台.md "wikilink") | 《[花樣少年少女](../Page/花樣少年少女_\(2006年電視劇\).md "wikilink")》 | 司馬樹     |
| 2007年8月  | [八大電視台](../Page/八大電視台.md "wikilink")                                | 《[終極一家](../Page/終極一家.md "wikilink")》                  | a Chord |
| 2014年    | [八大電視台](../Page/八大電視台.md "wikilink")                                | 《[終極X宿舍](../Page/終極X宿舍.md "wikilink")》                | a Chord |
|          |                                                                     |                                                       |         |

## 金曲獎

<table style="width:255%;">
<colgroup>
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 15%" />
<col style="width: 12%" />
<col style="width: 18%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>屆次</p></th>
<th><p>專輯</p></th>
<th><p>獎項</p></th>
<th><p>入圍</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/第21屆金曲獎.md" title="wikilink">第21屆金曲獎</a></p></td>
<td><p>《雖然很芭樂》</p></td>
<td><p>最佳新人獎</p></td>
<td><p>《雖然很芭樂》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/第23屆金曲獎.md" title="wikilink">第23屆金曲獎</a></p></td>
<td><p>《於是長大了以後》</p></td>
<td><p>最佳年度歌曲獎</p></td>
<td><p>《於是長大了以後》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳作曲人獎</p></td>
<td><p>謝和弦《於是長大了以後》</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳單曲製作人獎</p></td>
<td><p><a href="../Page/何官錠.md" title="wikilink">何官錠</a>、謝和弦《於是長大了以後》</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳音樂錄影帶獎</p></td>
<td><p><a href="../Page/比爾賈.md" title="wikilink">比爾賈</a>《寂寞瘋了》</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 爭議事件

  - 2012年8月3日，謝和弦在個人Facebook上表示自己吸食大麻\[2\]\[3\]，隨後到案亦自認吸毒，藥檢結果亦呈陽性反應，地檢署傳喚其出庭應訊。所屬的[亞神音樂已表示待官司告一段落後會與其解約](../Page/亞神音樂.md "wikilink")。期間，謝和弦父親謝志孟在專訪時表示，曾將被診斷出有躁鬱症的謝和弦送至南投草屯公立療養院接受治療\[4\]。

<!-- end list -->

  - 2012年9月28日，謝和弦在個人Facebook貼出公開道歉文，表示已得到應得的警惕和教訓，並為此不良示範致歉，此外，並未向其他被罵的藝人道歉\[5\]。

<!-- end list -->

  - 2012年10月6日，因謝和弦在暴走事件後仍不斷在臉書上發表不適當言論，其唱片公司[亞神音樂要求解約](../Page/亞神音樂.md "wikilink")，謝和弦便在臉書上發表「我會死給你們看」等言論\[6\]。

<!-- end list -->

  - 2012年10月8日，謝和弦在臉書暗指演藝圈大哥以「出外景」為由拐騙女孩，以「胡姓大哥」與「吳姓大哥」讓外界自己去聯想。對此，[吳宗憲在](../Page/吳宗憲.md "wikilink")[華視](../Page/華視.md "wikilink")「[POWER星期天](../Page/POWER星期天.md "wikilink")」回嗆：「要有Guts（膽量），請拿出證據、指名道姓，別藉網路標新立異、搏名氣，很不實在」\[7\]。

<!-- end list -->

  - 2012年11月4日，謝和弦在臉書上放話，說演藝圈的胡姓跟吳姓大哥，用權威拐騙那些有夢想的女孩，之後甚至點名吳姓大哥就是吳宗憲，讓吳宗憲氣的說已經在蒐集資料，考慮對謝和弦提告，但在今年的某場商演中，親自跟吳宗憲道歉，吳宗憲也歡迎謝和弦上綜藝大熱門\[8\]。

<!-- end list -->

  - 2017年5月25日，謝和弦在[新浪微博上表示自己并不在意](../Page/新浪微博.md "wikilink")[中国大陸市场](../Page/中国大陸.md "wikilink")，在台湾的工作收入已经很知足了，并不打算去中国大陸賺钱，有[台灣媒體說此舉導致中国大陸的部分网友](../Page/台灣媒體.md "wikilink")「玻璃心全碎」\[9\]。对此也有部分中国大陸网友表示謝和弦在中国大陸并不为人所熟知，微博的粉丝数量更是只有7万人\[10\]。

<!-- end list -->

  - 2017年7月3日，謝和弦在新浪微博平台釋出一段影片，其頭戴墨鏡，為了反斥“台灣人窮”的言論，便隨手剪爛身邊的服飾和衣帽，並在片尾解釋說該舉動是要證明用錢買不到“骨氣”。該影片引來了眾多的争议并使謝和弦一度登上了微博的熱門搜索。\[11\]

<!-- end list -->

  - 2018年5月31日，他於臉書上發文，透露自己不只是想要做埔里鎮長，目標是要當上「南投縣長」，誓言要讓南投縣脫貧。並對勸退他的人嗆聲：「謙你貓的卑，請我爸勸退也沒用！」直批台灣政府太廢，「年輕人已厭惡黨派鬥爭，老百姓也賺不到錢養生」，他認為選舉是唯一可以合法打政府臉的管道，呼籲大家「一起善用它吧！」\[12\]

## 参考來源

## 外部連結

  - [謝和弦 R-chord Facebook粉絲團](https://www.facebook.com/chord0415)
  - [謝和弦新浪微博](http://weibo.com/chord415/)

[Category:台灣男歌手](../Category/台灣男歌手.md "wikilink")
[Category:台灣創作歌手](../Category/台灣創作歌手.md "wikilink")
[Category:台灣華語流行音樂歌手](../Category/台灣華語流行音樂歌手.md "wikilink")
[Category:台灣音樂製作人](../Category/台灣音樂製作人.md "wikilink")
[Category:埔里人](../Category/埔里人.md "wikilink")
[He和](../Category/謝姓.md "wikilink")
[Category:臺灣新教徒](../Category/臺灣新教徒.md "wikilink")
[\*](../Category/謝和弦.md "wikilink")

1.
2.  [謝和弦自爆吸食奶頭
    台北市刑警大隊介入調查](http://www.nownews.com/2012/08/05/138-2841773.htm)
    .Nownews今日新聞，2012-08-05
3.  [謝和弦又失控罵劉家昌過時
    窩埔里老家嗆等警察抓](http://www.appledaily.com.tw/appledaily/article/entertainment/20120806/34418977)
    .蘋果日報，2012-08-06
4.  [謝和弦躁鬱症自殘
    父再送療養院](http://tw.news.yahoo.com/%E8%AC%9D%E5%92%8C%E5%BC%A6%E8%BA%81%E9%AC%B1%E7%97%87%E8%87%AA%E6%AE%98-%E7%88%B6%E5%86%8D%E9%80%81%E7%99%82%E9%A4%8A%E9%99%A2-213000801.html)
    .Yahoo奇摩新聞，取自中國時報，2012-08-31
5.  [謝和弦臉書公開道歉
    網友反應兩極](http://iservice.libertytimes.com.tw/liveNews/news.php?no=701504&type=%E5%BD%B1%E5%8A%87)
    .自由時報，2012-09-28
6.  [謝和弦爆輕生之意
    「我會死給你們看」要公司別逼他](http://tw.omg.yahoo.com/news/%E8%AC%9D%E5%92%8C%E5%BC%A6%E7%88%86%E8%BC%95%E7%94%9F%E4%B9%8B%E6%84%8F-%E6%88%91%E6%9C%83%E6%AD%BB%E7%B5%A6%E4%BD%A0%E5%80%91%E7%9C%8B-%E8%A6%81%E5%85%AC%E5%8F%B8%E5%88%A5%E9%80%BC%E4%BB%96-030738309.html)
    .Yahoo奇摩娛樂新聞，2012-10-10
7.  [謝和弦再開砲　爆演藝圈大哥揪女星上床](http://tw.omg.yahoo.com/news/%E8%AC%9D%E5%92%8C%E5%BC%A6%E5%86%8D%E9%96%8B%E7%A0%B2-%E7%88%86%E6%BC%94%E8%97%9D%E5%9C%88%E5%A4%A7%E5%93%A5%E6%8F%AA%E5%A5%B3%E6%98%9F%E4%B8%8A%E5%BA%8A-025048376.html)
    .Yahoo奇摩娛樂新聞，2012-10-09
8.  [怒謝和弦亂爆料
    吳宗憲首次告藝人](http://tw.news.yahoo.com/%E6%80%92%E8%AC%9D%E5%92%8C%E5%BC%A6%E4%BA%82%E7%88%86%E6%96%99-%E5%90%B3%E5%AE%97%E6%86%B2%E9%A6%96%E6%AC%A1%E5%91%8A%E8%97%9D%E4%BA%BA-062118277.html)
    .Yahoo奇摩新聞，2012-11-04
9.  [【吸滿仇恨值】謝和弦引戰中國網友遭圍剿　一句話讓玻璃心碎滿地](http://www.appledaily.com.tw/realtimenews/article/new/20170526/1126597/)
10. [谢和弦的新浪微博](http://www.weibo.com/chord415?is_hot=1)
11. [自由時報：台灣人被中國酸很窮？
    謝和弦嗆這個用錢買不到](http://ent.ltn.com.tw/news/breakingnews/2120903)
12.