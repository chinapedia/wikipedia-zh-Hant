**謝旻樹**（，），是一位[香港游泳運動員](../Page/香港.md "wikilink")，擅長於背泳項目。曾於[西島中學就讀](../Page/西島中學.md "wikilink")，[史丹福大學理學士畢業](../Page/史丹福大學.md "wikilink")。

## 簡歷

謝旻樹是[台](../Page/台灣.md "wikilink")[馬](../Page/馬來西亞.md "wikilink")[混血兒](../Page/混血兒.md "wikilink")，在香港出生及長大，10歲起加入香港青年軍，曾就讀於[西島中學](../Page/西島中學.md "wikilink")，後負芨美國[史丹福大學](../Page/史丹福大學.md "wikilink")，2013年7月修畢理學士學位，現為全職游泳運動員。\[1\]

### 出戰里約奧運會

謝旻樹在奧運達標期曾在男子50米自由泳游出22秒39，距離奧運入圍標準（A標）22秒27僅差0.12秒，但由於香港沒有男泳手達A標，故可有一名參加過[2015年世界游泳錦標賽的男泳手獲](../Page/2015年世界游泳錦標賽.md "wikilink")「[外卡](../Page/外卡.md "wikilink")」資格，他因此能夠代表香港出戰[里約奧運](../Page/里約奧運.md "wikilink")\[2\]。

## 參賽經驗

  - 2007年[亞洲室內運動會](../Page/亞洲室內運動會.md "wikilink")
  - 2007年世界游泳錦標賽
  - 2006年[多哈亞運](../Page/多哈亞運.md "wikilink")
  - 2005年亞太分齡錦標游泳賽

## 游泳紀錄

謝旻樹現時保持著11項香港的游泳紀錄；另保持3項青年組別紀錄\[3\]\[4\]：

<table>
<thead>
<tr class="header">
<th></th>
<th></th>
<th><p>項目</p></th>
<th><p>時間</p></th>
<th><p>比賽日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>短池</p></td>
<td><p>50米自由泳</p></td>
<td><p>21.86</p></td>
<td><p>2014年9月30日</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>短池</p></td>
<td><p>50米背泳</p></td>
<td><p>24.18</p></td>
<td><p>2014年9月30日</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>短池</p></td>
<td><p>100米背泳</p></td>
<td><p>52.08</p></td>
<td><p>2014年9月29日</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>短池</p></td>
<td><p>50米蝶泳</p></td>
<td><p>23.35</p></td>
<td><p>2014年9月29日</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>短池</p></td>
<td><p>100米個人混合泳</p></td>
<td><p>54.70</p></td>
<td><p>2013年10月17日</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>長池</p></td>
<td><p>50米自由泳</p></td>
<td><p>22.39</p></td>
<td><p>2015年12月3-5日</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>長池</p></td>
<td><p>100米自由泳</p></td>
<td><p>49.69</p></td>
<td><p>2013年7月6-17日</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>長池</p></td>
<td><p>100米背泳</p></td>
<td><p>56.63</p></td>
<td><p>2014年6月19-22日</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>長池</p></td>
<td><p>50米蝶泳</p></td>
<td><p>24.12</p></td>
<td><p>2015年8月2日</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>長池</p></td>
<td><p>100米蝶泳</p></td>
<td><p>53.70</p></td>
<td><p>2013年7月6-17日</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>長池</p></td>
<td><p>4×100米混合泳接力<br />
（香港隊－<a href="../Page/黃鍇威.md" title="wikilink">黃鍇威</a>、<a href="../Page/王俊仁_(游泳運動員).md" title="wikilink">王俊仁</a>、謝旻樹、<a href="../Page/吳鎮男.md" title="wikilink">吳鎮男</a>）</p></td>
<td><p>3:44.67</p></td>
<td><p>2013年10月10-15日</p></td>
</tr>
<tr class="even">
<td><p>青年組別紀錄（16歲以下）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>短池</p></td>
<td><p>200米背泳</p></td>
<td><p>2:04.15</p></td>
<td><p>2006年3月18日</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>長池</p></td>
<td><p>100米背泳</p></td>
<td><p>58.76</p></td>
<td><p>2006年8月1日</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>長池</p></td>
<td><p>200米背泳</p></td>
<td><p>2:08.71</p></td>
<td><p>2006年8月5日</p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [謝旻樹](http://hkswim.com/swimmer/cheahgeoffrey.html)

[Category:香港游泳運動員](../Category/香港游泳運動員.md "wikilink")
[Category:马来族混血儿](../Category/马来族混血儿.md "wikilink")
[Category:台灣裔香港人](../Category/台灣裔香港人.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Category:台裔混血兒](../Category/台裔混血兒.md "wikilink")
[Category:2006年亞洲運動會游泳運動員](../Category/2006年亞洲運動會游泳運動員.md "wikilink")
[Category:2014年亞洲運動會游泳運動員](../Category/2014年亞洲運動會游泳運動員.md "wikilink")
[Category:男子自由式游泳運動員](../Category/男子自由式游泳運動員.md "wikilink")
[Category:仰泳运动员](../Category/仰泳运动员.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:香港奧林匹克運動會游泳選手](../Category/香港奧林匹克運動會游泳選手.md "wikilink")
[Category:2016年夏季奧林匹克運動會游泳運動員](../Category/2016年夏季奧林匹克運動會游泳運動員.md "wikilink")

1.
2.
3.
4.