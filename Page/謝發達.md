**謝發達**（），[中華民國駐新加坡代表](../Page/中華民國駐新加坡代表.md "wikilink")，曾任[行政院經濟建設委員會副主任委員](../Page/行政院經濟建設委員會.md "wikilink")、[中華民國經濟部常務次長](../Page/中華民國經濟部.md "wikilink")，提出推展金磚四國的對外貿易計畫。

## 專訪文章

  -
  -
[S](../Category/國立政治大學校友.md "wikilink")
[Category:谢姓](../Category/谢姓.md "wikilink")
[Category:中華民國駐新加坡代表](../Category/中華民國駐新加坡代表.md "wikilink")
[Category:中華民國行政院經濟建設委員會副主任委員](../Category/中華民國行政院經濟建設委員會副主任委員.md "wikilink")
[Category:台中市人](../Category/台中市人.md "wikilink")
[Category:中華民國經濟部次長](../Category/中華民國經濟部次長.md "wikilink")
[Category:中華民國駐新加坡大使](../Category/中華民國駐新加坡大使.md "wikilink")