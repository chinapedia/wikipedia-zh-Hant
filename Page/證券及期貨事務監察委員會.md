[Overlook_Cheung_Kong_Center.JPG](https://zh.wikipedia.org/wiki/File:Overlook_Cheung_Kong_Center.JPG "fig:Overlook_Cheung_Kong_Center.JPG")
[Chater_House_2008.jpg](https://zh.wikipedia.org/wiki/File:Chater_House_2008.jpg "fig:Chater_House_2008.jpg")
**證券及期貨事務監察委員會**（簡稱**證監會**，[英文](../Page/英文.md "wikilink"):
）是[香港獨立於](../Page/香港.md "wikilink")[政府公務員架構外的](../Page/香港公務員.md "wikilink")[法定組織](../Page/法定組織.md "wikilink")，負責監管香港的[證券及](../Page/證券.md "wikilink")[期貨市場運作](../Page/期貨.md "wikilink")。

證監會於1989年5月根據《證券及期貨事務監察委員會條例》（《證監會條例》）（[香港法例第](../Page/香港法例.md "wikilink")24章）而成立，而成立原因是用來糾正[1987年下半年股災做成之危機](../Page/1987年香港股災.md "wikilink")。2003年4月1日，《證監會條例》與9條涉及證券期貨的條例（包括《證券條例》、《槓桿外匯條例》、《保障投資者條例》、《證券（披露權益）條例》、《商品交易條例》、《證券交易所合併條例》、《證券及期貨（結算所）條例》等）\[1\]合併，成為《證券及期貨條例》（香港法例第571章）。

證監會的工作，包括監管證券、期貨及槓桿式[外匯交易的參與者](../Page/外匯.md "wikilink")，其他證券及期貨中介人，[香港交易所及所有香港上市公司](../Page/香港交易所.md "wikilink")，並對違規者執行紀律處分。

## 主席\[2\]

  - [區偉賢](../Page/區偉賢.md "wikilink")（Robert Owen，1989－1992）

  - [羅德滔](../Page/羅德滔.md "wikilink")（Robert Nottle，1992－1994）

  - [梁定邦](../Page/梁定邦_\(大律師\).md "wikilink")（Anthony Neoh，1995－1998）

  - [沈聯濤](../Page/沈聯濤.md "wikilink")（Andrew Sheng
    Len-tao，1998年－2005年10月1日）

  - （Martin Wheatley，2005年10月1日－2006年10月20日，改任行政總裁）

  - [方正](../Page/方正_\(香港\).md "wikilink")（Eddy Fong
    Ching，2006年10月20日－2012年10月19日）

  - [唐家成](../Page/唐家成.md "wikilink")\[3\]（Carlson
    Tong，2012年10月20日－2018年10月19日）

  - [雷添良](../Page/雷添良.md "wikilink")\[4\]\[5\]（2018年10月20日－2021年10月19日）

## 行政總裁

  - （2006－2011）

  - [歐達禮](../Page/歐達禮.md "wikilink")（2011－）

## 架構

  - 證監會董事局
      - 稽核委員會
      - 薪酬委員會
      - 財政預算委員會
      - 諮詢委員會
      - 各監管事務委員會
      - 執行委員會
          - 企業融資部
          - 法規執行部
          - 政策、中國事務及投資產品部
          - 市場監察部（包括研究科）
          - 法律服務部
          - 發牌科
          - 中介團體監察科
          - 機構事務部
              - 行政總裁辦公室及證監會秘書處
              - 財務及行政科
              - 人力資源/培訓及發展科
              - 資訊科技科
              - 對外事務科

## 總辦事處變遷

  - [中環](../Page/中環.md "wikilink")[遮打大廈](../Page/遮打大廈.md "wikilink")5樓505-510室及6-9樓（2003年7月－2013年2月1日）
  - [中環皇后大道中](../Page/中環.md "wikilink")2號[長江集團中心](../Page/長江集團中心.md "wikilink")30-33樓及35樓全層（2013年2月4日－）

## 證券及期貨從業員資格考試

[證券及期貨從業員資格考試](../Page/證券及期貨從業員資格考試.md "wikilink")（LE），是一個實用及以市場為本的考試，這個考試的設計牽涉到監管機構，學術界以及業界的精英。這個考試由[香港證券及投資學會舉辦](../Page/香港證券及投資學會.md "wikilink")。

### 爭議

[證券及期貨從業員資格考試在舉辦這個考試中面對很多爭議](../Page/證券及期貨從業員資格考試.md "wikilink")，主要因為這些考試並沒有由第三方獨立審核過考試題目。除此以外，這個考試並沒有官方的上訴機制以及任何制衡機制，導致這個考試的合理性成疑。這個考試的不同語言版本也出現嚴重差異，因此飽受批評。

## 参考文献

## 外部連結

  - [證券及期貨事務監察委員會](http://www.sfc.hk/web/TC/index.html)

{{-}}

[Category:香港股市](../Category/香港股市.md "wikilink")
[Category:香港法定機構](../Category/香港法定機構.md "wikilink")
[Category:香港證監會](../Category/香港證監會.md "wikilink")
[Category:1989年建立](../Category/1989年建立.md "wikilink")

1.  [證券及期貨條例諮詢文件](http://www.sfc.hk/sfc/doc/TC/legislation/securities/others/condoc-c.pdf)
2.  [歷經轉變二十載 -
    歷任主席](http://www.sfc.hk/web/doc/TC/speeches/public/annual/rep08-09/full.pdf)
    證監會2008-09年報第56-59頁
3.
4.
5.