**譚玉齡**（），[滿族](../Page/滿族.md "wikilink")，[他他拉氏](../Page/他他拉氏.md "wikilink")。清朝末代皇帝[溥仪妾室](../Page/溥仪.md "wikilink")。

## 生平

出生在一個[北京的](../Page/北京.md "wikilink")[旗人家庭](../Page/旗人.md "wikilink")，与其他滿人家庭类似，在民国后，由於身份會招來麻煩，原[满族姓氏按音轉關係改成](../Page/滿族姓氏.md "wikilink")[漢姓](../Page/漢姓.md "wikilink")[譚](../Page/譚姓.md "wikilink")。

1937年初正在[北京讀中學堂的譚玉齡被满洲国皇帝](../Page/北京.md "wikilink")[溥仪選中](../Page/溥仪.md "wikilink")，從北京前赴[新京](../Page/新京.md "wikilink")（今[長春](../Page/长春市.md "wikilink")）的，成为他的第三位妻子（妾室）。当年4月6日（农历二月二十五日\[1\]），谭玉龄在[新京](../Page/新京.md "wikilink")（今[長春](../Page/长春市.md "wikilink")）的[滿洲國皇宮被正式冊封為](../Page/伪满洲国皇宫旧址.md "wikilink")**祥贵人**。《[爱新觉罗宗谱](../Page/爱新觉罗宗谱.md "wikilink")·[星源集庆](../Page/星源集庆.md "wikilink")》所记，谭玉龄封号为庆贵人\[2\]。[秦翰才称宫中呼她为](../Page/秦翰才.md "wikilink")“董贵人”\[3\]。

婚後，譚玉齡與溥儀感情甚篤。緝熙樓為她在[滿洲國皇宮居住的客廳](../Page/滿洲國.md "wikilink")。五年後，譚玉齡因[傷寒去世](../Page/傷寒.md "wikilink")，得年22歲，被追谥為**明賢貴妃**\[4\]。按[清朝](../Page/清朝.md "wikilink")[貴妃例治喪](../Page/貴妃.md "wikilink")，殯於[長春般若寺](../Page/般若寺_\(长春市\).md "wikilink")，有禁卫看守。[滿洲國垮台後](../Page/滿洲國.md "wikilink")，至[长春战役时尚未安葬](../Page/1946年长春进攻战役.md "wikilink")\[5\]。后，溥儀囑族人將其棺柩火化，[骨灰轉存於北京親屬處](../Page/骨灰.md "wikilink")，溥儀獲釋後，曾一度接至自己家中，後由侄兒[毓喦代為安葬](../Page/毓喦.md "wikilink")。玉齡的玉照，溥儀一直帶在身邊，直到1967年逝世，在照片背後有著“我的最親愛的玉齡”字樣。

## 死因之謎

關於她的死因至今仍是一個謎。在[婉容精神失常以後](../Page/婉容.md "wikilink")，「御用挂」[吉岡安直](../Page/吉岡安直.md "wikilink")[中將就向](../Page/中將.md "wikilink")[溥儀提議選一個日本女人入宮](../Page/溥儀.md "wikilink")。但溥儀卻因已在北京選好譚玉齡而作罷。吉岡因此而感到不滿。因為譚玉齡患有[膀胱炎](../Page/膀胱炎.md "wikilink")，在吉岡的一次診治後，卻在經注射後不到天明即死去。但譚玉齡是否為吉岡所害，卻因沒有更多證據，而眾說紛紜。

## 参考资料

[T](../Category/滿洲國妃嬪.md "wikilink")
[Y玉龄](../Category/他塔喇氏.md "wikilink")
[T](../Category/滿洲國滿族人.md "wikilink")
[Y玉龄](../Category/谭姓.md "wikilink")
[Category:諡明賢](../Category/諡明賢.md "wikilink")

1.
2.  《[爱新觉罗宗谱](../Page/爱新觉罗宗谱.md "wikilink")·[星源集庆](../Page/星源集庆.md "wikilink")》（页九九）今上皇帝溥仪......皇后郭博罗氏......庆贵人他他拉氏.康德四年二月.敕封.庆贵人……

3.

4.

5.