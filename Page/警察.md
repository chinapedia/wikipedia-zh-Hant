[Chicago_police_officer_on_segway.jpg](https://zh.wikipedia.org/wiki/File:Chicago_police_officer_on_segway.jpg "fig:Chicago_police_officer_on_segway.jpg")[芝加哥警察局人員駕駛](../Page/芝加哥警察局.md "wikilink")[賽格威](../Page/賽格威.md "wikilink")[巡邏](../Page/巡邏.md "wikilink")。\]\]
[Israel_police_officers.jpg](https://zh.wikipedia.org/wiki/File:Israel_police_officers.jpg "fig:Israel_police_officers.jpg")警員。\]\]
[2007TourDeTaiwan7thStage-37.jpg](https://zh.wikipedia.org/wiki/File:2007TourDeTaiwan7thStage-37.jpg "fig:2007TourDeTaiwan7thStage-37.jpg")[臺北市政府警察局交通警察大隊機車巡邏](../Page/臺北市政府警察局.md "wikilink")。\]\]
[Kravallpolis.jpg](https://zh.wikipedia.org/wiki/File:Kravallpolis.jpg "fig:Kravallpolis.jpg")的[瑞典](../Page/瑞典.md "wikilink")[防暴警察人員](../Page/防暴警察.md "wikilink")。\]\]
**警察**（[英文](../Page/英文.md "wikilink")：**Police**，或稱**Cop**），為一種[職業](../Page/職業.md "wikilink")，主要職責為[執法](../Page/執法.md "wikilink")、維持[公共安全](../Page/公共安全.md "wikilink")、保護[性命及保障](../Page/性命.md "wikilink")[財產](../Page/財產.md "wikilink")。各國警察體系一般都分為[巡邏警察和](../Page/巡邏警察.md "wikilink")[刑事警察兩大系統](../Page/刑事警察.md "wikilink")，然後再細分為多種崗位及工作類別。[英國](../Page/英國.md "wikilink")[倫敦警察廳是](../Page/倫敦警察廳.md "wikilink")[世界首個以現代警察制度以維持](../Page/世界.md "wikilink")[治安的](../Page/治安.md "wikilink")[城市警察部門](../Page/城市.md "wikilink")，[香港警務處為全](../Page/香港警務處.md "wikilink")[亞洲首個以現代警察制度以維持治安的警察部門](../Page/亞洲.md "wikilink")。

絕大部分国家和地区的警察與[軍隊分立](../Page/軍隊.md "wikilink")，互不隸屬，各有其專業性。在[法國和](../Page/法國.md "wikilink")[中華人民共和國](../Page/中華人民共和國.md "wikilink")，除了警察外，隸屬軍隊或单独的[國家憲兵也會負責處理或协理治安事務](../Page/國家憲兵.md "wikilink")。在少數地區，警察機關成為[軍隊的輔助](../Page/軍隊.md "wikilink")[武裝部隊](../Page/武裝部隊.md "wikilink")。在[無軍隊或无軍隊指揮權的国家中](../Page/没有军队的国家列表.md "wikilink")，警察成為當地[政府或者](../Page/政府.md "wikilink")[政權唯一可以直接調度的武裝部隊](../Page/政權.md "wikilink")，例如[冰岛和](../Page/冰岛.md "wikilink")[巴拿马](../Page/巴拿马.md "wikilink")。而在[獨裁政體及](../Page/獨裁政體.md "wikilink")[極權國家中](../Page/極權國家.md "wikilink")，亦可能存在[秘密警察及](../Page/秘密警察.md "wikilink")[宗教警察等警察系統](../Page/宗教警察.md "wikilink")。

至於[憲兵](../Page/憲兵.md "wikilink")，亦即[軍事警察](../Page/軍事警察.md "wikilink")，為在[軍營和](../Page/軍營.md "wikilink")[軍區中負責維持](../Page/軍區.md "wikilink")[軍隊秩序的警察](../Page/軍隊.md "wikilink")，在設有憲兵的國家及地區中，憲兵可以依據軍法維護軍隊秩序，糾舉軍人，以及協助軍事檢察官辦理軍法案件。部份國家及地區另外設立有[國家憲兵](../Page/國家憲兵.md "wikilink")，駐守於要塞、邊界、重要機關，持有步兵等級的重型武裝，對民眾亦具備執法權力

## 名稱起源

### 中文

[中文警察一詞出現於](../Page/中文.md "wikilink")1900年前，在[清](../Page/清.md "wikilink")[光緒二十年](../Page/光緒二十年.md "wikilink")（1894年）十月後。有可能是當朝重臣[劉坤一和](../Page/劉坤一.md "wikilink")[張之洞借用](../Page/張之洞.md "wikilink")[日本](../Page/日本.md "wikilink")[明治維新時採用的](../Page/明治維新.md "wikilink")[漢字警察一詞](../Page/漢字.md "wikilink")；然而警察一詞，最早出於[唐代](../Page/唐代.md "wikilink")[玄奘](../Page/玄奘.md "wikilink")[法師](../Page/法師.md "wikilink")《[大唐西域記](../Page/大唐西域記.md "wikilink")·[藍摩國](../Page/藍摩國.md "wikilink")》：「野象群行，採花以散，冥力警察，初無間替。」、《[新五代史](../Page/新五代史.md "wikilink")·[史弘肇傳](../Page/史弘肇.md "wikilink")》：「弘肇出兵警察，務行殺戮，罪無大無小皆死。」及[宋代](../Page/宋代.md "wikilink")[陸游](../Page/陸游.md "wikilink")
《[南唐書](../Page/南唐書.md "wikilink")·[盧郢傳](../Page/盧郢.md "wikilink")》：「後主命[韓德霸為都城烽火使](../Page/韓德霸.md "wikilink")，警察非常。」當時警察並非[名詞](../Page/名詞.md "wikilink")，實乃**警戒監察**之意。

[義大利當代](../Page/義大利.md "wikilink")[漢學家馬西尼](../Page/漢學家.md "wikilink")（Federico
Masini）在《[現代漢語語彙的形成](../Page/現代漢語語彙的形成.md "wikilink")》一書裡，做過詳細的考辨。馬西尼指出，1884年（光緒10年），清廷的[總理衙門指示](../Page/總理衙門.md "wikilink")[翰林院及](../Page/翰林院.md "wikilink")[六部](../Page/六部.md "wikilink")，擬定了一份派遣高級官員前往[外國訪問考察的名單](../Page/外國.md "wikilink")，要求官員撰寫考察報告，俾作爲改革的基礎。這一波大臣出洋考察以[傅雲龍爲始](../Page/傅雲龍.md "wikilink")，去了日本、[美國](../Page/美國.md "wikilink")、[秘魯和](../Page/秘魯.md "wikilink")[巴西等四國](../Page/巴西.md "wikilink")，計爲時兩年。傅雲龍歸來後寫了許多考察記，在日本部分即有《[遊歷日本圖經](../Page/遊歷日本圖經.md "wikilink")》和《[遊歷日本圖經餘記](../Page/遊歷日本圖經餘記.md "wikilink")》兩冊，書中將日本以[漢字書寫的警察帶回中國](../Page/漢字.md "wikilink")。接著，中國第一代日本專家[黃遵憲在所著的](../Page/黃遵憲.md "wikilink")《[日本國志](../Page/日本國志.md "wikilink")》裏簡介了日本的警察制度。此時，警察這個現代名詞開始出現。

### 英文

警察英文「Police」一詞根本源於[古希臘](../Page/古希臘.md "wikilink")，表示[秩序和社會和平的意思](../Page/秩序.md "wikilink")\[1\]。
另一英文詞語「Constabulary」亦可代表警察部隊，源於十四世紀[拉丁文表示社區內執法隊伍的意思](../Page/拉丁文.md "wikilink")\[2\]。

## 近代警察制度的建立

[HH_Polizeihauptmeister_MZ.jpg](https://zh.wikipedia.org/wiki/File:HH_Polizeihauptmeister_MZ.jpg "fig:HH_Polizeihauptmeister_MZ.jpg")
追溯現代警察的創設，法國於1667年[路易十四時代就建立警察制度](../Page/路易十四時代.md "wikilink")；而英國則緣於1829年，由當時的內政部長[罗伯特·皮尔所提出的警察法案](../Page/罗伯特·皮尔.md "wikilink")（Police
Bill），經由[英國議會通過](../Page/英國議會.md "wikilink")，於同年6月26日獲得[英皇](../Page/英皇.md "wikilink")[喬治四世批准後](../Page/喬治四世.md "wikilink")，經過數月的籌備，一群戴皮帽、穿藏青色[燕尾服上衣及褲子](../Page/燕尾服.md "wikilink")，穿[皮鞋及打皮綁腿的](../Page/皮鞋.md "wikilink")[軍事化人員](../Page/軍事化.md "wikilink")，終於於同年9月29日出現在倫敦街道上；此為[倫敦警察廳](../Page/倫敦警察廳.md "wikilink")[現代化的第一次出現](../Page/現代化.md "wikilink")。

## 種類

  - [巡邏警察](../Page/巡邏警察.md "wikilink")
  - [保安警察](../Page/保安警察.md "wikilink")
  - [刑事警察](../Page/刑事警察.md "wikilink")
  - [輔助警察](../Page/輔助警察.md "wikilink")
  - [防暴警察](../Page/防暴警察.md "wikilink")
  - [特種警察](../Page/特種警察.md "wikilink")
  - [軍事警察](../Page/軍事警察.md "wikilink")
  - [網路警察](../Page/網路警察.md "wikilink")
  - [秘密警察](../Page/秘密警察.md "wikilink")
  - [宗教警察](../Page/宗教警察.md "wikilink")

## 各國家及地區警察管理機構

###

[China_patrol_car.jpg](https://zh.wikipedia.org/wiki/File:China_patrol_car.jpg "fig:China_patrol_car.jpg")

中華人民共和國人民警察分为公安部门管理的公安机关人民警察（即狭义“公安”，包括治安、户籍、刑侦、交通等）、国家安全部门管理的国家安全机关人民警察、司法行政机关人民警察（即监狱警察）以及人民法院、人民检察院司法警察四大类。人民警察是国家公务员，实行警监、警督、警司、警员的警衔制度，服装以藏黑为主色调。

###

[HK_Police_HQs_logo.jpg](https://zh.wikipedia.org/wiki/File:HK_Police_HQs_logo.jpg "fig:HK_Police_HQs_logo.jpg")門前的[香港警察徽章](../Page/香港警察徽章.md "wikilink")。\]\]

[香港警務處於](../Page/香港警務處.md "wikilink")1844年5月1日成立，現隸屬於[香港特別行政區政府](../Page/香港特別行政區政府.md "wikilink")[保安局](../Page/保安局.md "wikilink")，為編制最龐大的[香港政府部門及](../Page/香港政府部門.md "wikilink")[紀律部隊](../Page/香港紀律部隊.md "wikilink")，為[世界之次及](../Page/世界.md "wikilink")[亞洲首個以](../Page/亞洲.md "wikilink")[現代警察](../Page/現代.md "wikilink")[制度運作的](../Page/制度.md "wikilink")[警察機關](../Page/警察機關.md "wikilink")。香港警務處是[香港政府的](../Page/香港政府.md "wikilink")「最後可依賴力量」（Agency
of Last
Resort），於香港境內外全天候為[香港社會服務](../Page/香港社會.md "wikilink")\[3\]，為[香港治安及社會穩定奠定基礎](../Page/香港治安.md "wikilink")\[4\]\[5\]\[6\]\[7\]\[8\]\[9\]。

現任[警務處處長為](../Page/警務處處長.md "wikilink")[卢伟聪](../Page/卢伟聪.md "wikilink")，領導正規（至2014年8月31日：28,656名）、[輔助警察](../Page/香港輔助警察隊.md "wikilink")（4,500名）和文職（包括[交通督導員在內](../Page/交通督導員.md "wikilink")4,593名）合共近38,000名人員\[10\]，編制及形成之[警民比例均為世界上規模最龐大及首幾位](../Page/警民比例.md "wikilink")。憑藉維持[香港為世界上其中一座罪案率為最低度以及高度的](../Page/香港.md "wikilink")[破案率的城市](../Page/破案率.md "wikilink")，香港警務處於1960年代起被[國際刑警組織及](../Page/國際刑警組織.md "wikilink")[國際評定及評價為](../Page/國際.md "wikilink")「亞洲最佳」（Asia's
Finest）\[11\]\[12\]\[13\]\[14\]\[15\]\[16\]，世界上最專業及優秀的警察機構之一\[17\]\[18\]\[19\]\[20\]\[21\]\[22\]\[23\]。

###

[澳門特別行政區有兩大警察體系](../Page/澳門特別行政區.md "wikilink")，分別為[司法警察局和](../Page/司法警察局.md "wikilink")[治安警察局](../Page/治安警察局.md "wikilink")，互不隸屬，於[澳門回歸後](../Page/澳門回歸.md "wikilink")，由[警察總局統一協調及指揮](../Page/警察總局.md "wikilink")。司法警察源自[葡萄牙司法警察體系](../Page/葡萄牙.md "wikilink")，實為[華人社會通稱的](../Page/華人社會.md "wikilink")[刑事警察](../Page/刑事警察.md "wikilink")。反[洗錢](../Page/洗錢.md "wikilink")、打擊資訊犯罪及[反恐等皆屬司法警察之責任](../Page/反恐.md "wikilink")，該局與[國際刑警有緊密聯繫](../Page/國際刑警.md "wikilink")。治安警察的主要負責為維持社會治安、指揮交通、出入境管理及保護要人等等。

###

中華民國的警察主管機關是[內政部警政署](../Page/內政部警政署.md "wikilink")。各縣市設有警察局，人口較多的鄉鎮市區設有警察分局，分局之下又設有派出所、分駐所。依警察勤務條例第三條規定，警察勤務之實施，應晝夜執行，普及轄區，並以行政警察為中心，其他各種警察配合之。另外設有各類專業警察（刑事警察、保安警察、交通警察、鐵路警察、航空警察、港務警察、外事警察等），就現行制度而言除專業警察外，其餘皆為行政警察。另外[海洋委員會海巡署艦隊分署](../Page/海洋委員會海巡署艦隊分署.md "wikilink")（原保安警察第七總隊所改制）亦是屬於警政機構，負責海洋巡防，目前該署約有警察7萬名。警察機關除了一般公部門有的[政風單位外](../Page/政風.md "wikilink")，另有[督察單位負責督導](../Page/督察.md "wikilink")。

另外，[司法院及](../Page/司法院.md "wikilink")[法務部所屬機關亦有隸屬之](../Page/法務部.md "wikilink")[法警](../Page/法警.md "wikilink")，工作性質上分，院方負責輔助法官審判之司法警察事務，檢方負責輔助檢察官犯罪偵查之司法警察事務，大約負責細項如下：（1）法院法警：辦理值庭、候審戒護、具保責付、協助民事[強制執行](../Page/強制執行.md "wikilink")、送達、拘提、同行、搜索、扣押、解送人犯、警衛、夜間值班及有關[司法警察事務及其他長官交辦之事項](../Page/司法警察.md "wikilink")。（2）[檢察署法警](../Page/檢察署.md "wikilink")：關於訴訟文書之送達、人犯之押提及具保、責付手續之辦理；值庭、拘提、搜索、扣押、調查及通緝犯之查緝；關於值勤（須輪值夜間勤務）、警衛及安全之防護及其他有關法警事務或長官交辦事項等。

###

[TOYOTA_170_system_Crown_police_car.jpg](https://zh.wikipedia.org/wiki/File:TOYOTA_170_system_Crown_police_car.jpg "fig:TOYOTA_170_system_Crown_police_car.jpg")\]\]

[日本警察的主管機關為](../Page/日本警察.md "wikilink")[警察廳](../Page/警察廳.md "wikilink")，由[內閣機關之一的](../Page/日本內閣.md "wikilink")[國家公安委員會管轄](../Page/國家公安委員會.md "wikilink")；在[關東](../Page/關東地方.md "wikilink")、[中部](../Page/中部地方.md "wikilink")、[近畿](../Page/近畿地方.md "wikilink")、[九州及](../Page/九州.md "wikilink")[四國設有管區警察局](../Page/四國.md "wikilink")。[東京都設](../Page/東京都.md "wikilink")[警視廳](../Page/警視廳.md "wikilink")，各地方政府（道、府、縣）則設置警察本部。警視廳或警察本部下設有警署，警署之下設有[交番](../Page/派出所.md "wikilink")。日本警察的主要勤務有刑事、交通、生活安全及警備4種。

###

的主管机构为[俄罗斯联邦内务部](../Page/俄罗斯联邦内务部.md "wikilink")。设部长1名，第一副部长1名，副部长3名（分别主管刑事警察部门、治安警察部门、北高加索地区反恐行动指挥部），下设15个司：行政司、国家保护财产司、干部司、交通管理司、非开放地区和重要设施治安管理司、交通治安司、治安司、打击有组织犯罪和反恐怖司、内部安全司、后勤司、刑事侦查司、经济安全司、组织监察司、法制司、财务司。

内务部还设有预审委员会、内卫部队总司令部等相对独立的机构，以及总信息分析中心、行动侦察局、特种技术措施局、打击高科技领域犯罪局（K局）、国际刑警组织俄罗斯国家中心局、刑事鉴定中心等直属机构。

此外，俄罗斯联邦移民局名义上归内务部领导，实际上是独立的联邦机构，其机关设局长1名（由总统根据总理的提名任免，向内务部长负责）、第一副局长1名、副局长5名，下设13个局：移民监察局、法律保障和国际合作局、总务局、动员工作局、国籍工作局、签证和登记工作局、护照工作和居民登记局、涉外劳务工作局、财务局、后勤和被迫移民工作局、信息资源局、组织分析局、危机处置局。

###

[美国警察主要分為联邦](../Page/美国警察.md "wikilink")、州和市县三级，联邦和各州的警察分别行使联邦和州所赋予的[警察权力](../Page/警察权力.md "wikilink")。州以下的警察的权力則由各州自行决定，除了联邦警察外，州警察、城市警察和县警察及私人保安与联邦政府没有任何的從屬上下关系，乃是直接由地方政府所领导。

美國共有18,000多个警察机构，3,088个县警察部门，13,578个市警察机构，49个州警察机构（[夏威夷外](../Page/夏威夷.md "wikilink")），50个联邦执法机构，1,626个特殊警察部门（管辖权力限制於如[公园](../Page/公园.md "wikilink")、[交通](../Page/交通.md "wikilink")、[机场及](../Page/机场.md "wikilink")[學校等](../Page/學校.md "wikilink")，當中包括[美國公園警察及](../Page/美國公園警察.md "wikilink")[美国国会警察等](../Page/美国国会警察.md "wikilink")）。美国共有920,000名人員，[警民比例为](../Page/警民比例.md "wikilink")3.25‰，与总面积比例为10名警察/100平方公里。其中，660,000名正规警察，260,000名文职人员，从警察局的人数和规模来看，大多数警察部门平均不逾100人，有800多个警察部门仅有1至2名正规警察，只有38个警察部门人数逾1,000人。

###

[马来西亚皇家警察](../Page/马来西亚皇家警察.md "wikilink")（；）是[马来西亚的执法及治安单位](../Page/马来西亚.md "wikilink")，工作覆盖范围包括交通管制、犯罪调查以及镇暴等，一般的调查工作由警长指挥。皇家警察总部位于[吉隆坡](../Page/吉隆坡.md "wikilink")[武吉阿曼警察总部](../Page/武吉阿曼.md "wikilink")，全国警察总长为[丹斯里卡立](../Page/丹斯里卡立.md "wikilink")。

皇家警察是由[馬來西亞内政部直接管轄](../Page/馬來西亞内政部.md "wikilink")。而在實際運作方面，皇家警察除了擁有本身的普通行動部隊執行常規巡邏任務外，也訓練志願警衛團、機場輔助警察、學生警察和警察之友做出社區治安的配合。而在國際運作上，皇家警察與[國際刑警組織長期進行合作及情報交流](../Page/國際刑警組織.md "wikilink")，特别是馬來西亞四個鄰國的警衛部門：[印尼國家警察](../Page/印尼國家警察.md "wikilink")、[汶萊皇家警察](../Page/汶萊皇家警察.md "wikilink")、[泰國皇家警察和](../Page/泰國皇家警察.md "wikilink")[新加坡警察](../Page/新加坡警察.md "wikilink")。

皇家警察的最高领导者为全国警察总长，其次为副全国警察总长，传统上这两个职位者都拥有丹斯里头衔。目前皇家警察共分8个部门，分别为刑事调查部、毒品罪案调查部、国家内部安全与公共秩序部、商业罪案调查部，政治部、反恐特别行动部、行政部和后勤部，所有部门都是由警阶为警察总监的8名高级警官领导，俗称警队八大天王，一般上这8名高级警官都拥有拿督斯里或拿督的头衔。

  - 行政部（行政處、福利組、訓練組、研究與發展）、援助組、禮議組、指揮營組、大馬皇家警察體育協會）
  - 後勤部（鑑證／管理組、通訊組、資訊工藝組、運輸組、財政組、技術所得組、軍火組、普通所得組、建築物組、銷毀／儲存／鑑定／報銷組）
  - 刑事調查部（行政處、刑事登記中心、監督調查報告組、秘密調查組、主控／法律組、技術援助組、取締非法組織／肅賭與反風化組、研究／策劃組、特别調查组（重案组）、科學鑑證組、婦女與兒童事務組、國際刑警）
  - 毒品罪案调查部（特别调查组、协调／国际关系组、行政处、扣留组、充公财产组、审问组、专家／技术援助组、监督组、登记处、后勤组、海关组）
  - 国家内部安全与公共秩序部（秘书处、行政处、发展／供给组、行动组、无线电巡逻队、[联邦后备部队](../Page/联邦后备部队.md "wikilink")、骑警组、普通行动部队、特别行动部队、交通警察组、水警行动部队、飞行组、大马控制中心）
  - 商业罪案调查部（洗黑钱调查组、会计剖析调查组、金融调查组、企业调查组、其他伪造调查组、电子与多媒体调查组、行动与技术援助组、行政与国际协调组、研究与秘密调查组、监督调查报告组、法律与诉讼组、商业刑事调查组）
  - 政治部*或稱特别事務部*（分析組／秘書處、技術情報、社會情報、外交情報、政治情報、經濟情報、安全情報、行政處）
  - 反恐特别行动部队（反恐特别部队）

## 參考文獻

## 外部链接

  -
## 参见

  - 國際概況

<!-- end list -->

  - [各國警察數量列表](../Page/各國警察數量列表.md "wikilink")
  - [國際刑警](../Page/國際刑警.md "wikilink")

<!-- end list -->

  - 辦公場所

<!-- end list -->

  - [警察局](../Page/警察局.md "wikilink")
  - [警署](../Page/警署.md "wikilink")
  - [派出所](../Page/派出所.md "wikilink")

<!-- end list -->

  - 制度與硬件

<!-- end list -->

  - [警察誓詞](../Page/警察誓詞.md "wikilink")
  - [警銜](../Page/警銜.md "wikilink")
  - [警車](../Page/警車.md "wikilink")

<!-- end list -->

  - 概念

<!-- end list -->

  - [警力](../Page/警力.md "wikilink")
  - [破案率](../Page/破案率.md "wikilink")

<!-- end list -->

  - 歷史

<!-- end list -->

  - [差人](../Page/差人.md "wikilink")
  - [差役](../Page/差役.md "wikilink")
  - [捕快](../Page/捕快.md "wikilink")
  - [趙秉鈞](../Page/趙秉鈞.md "wikilink")（中國現代警察制度創始人）

<!-- end list -->

  - 相關職業

<!-- end list -->

  - [臥底](../Page/臥底.md "wikilink")
  - [保安](../Page/保安.md "wikilink")
  - [刑警](../Page/刑警.md "wikilink")
  - [偵探](../Page/偵探.md "wikilink")
  - [保鑣](../Page/保鑣.md "wikilink")
  - [国家宪兵](../Page/国家宪兵.md "wikilink")

<!-- end list -->

  - 其他

<!-- end list -->

  -
  -
{{-}}

[警察](../Category/警察.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")
[Category:法国发明](../Category/法国发明.md "wikilink")

1.  <http://www.etymonline.com/index.php?allowed_in_frame=0&search=police&searchmode=none>
    Online Etymology Dictionary
2.  <http://dictionary.reference.com/browse/constabulary?s=t>
    Dictionary.com
3.  [處長李明逵談警隊工作](http://www.police.gov.hk/offbeat/793/chi/n01.htm)
    《警聲》第793期
4.  [香港警隊紀律嚴明、表現優秀](http://www.info.gov.hk/gia/general/199811/07/1107094.htm)
    《新聞公報》 1998年11月7日
5.  [大班文摘
    珍惜得來不易的和諧警民關係](http://www.albertcheng.hk/tc/main_article.asp?id=640)
    2010年2月17日
6.  [曾德成讚賞警隊是優秀隊伍](http://www.hkheadline.com/instantnews/news_content/200901/10/20090110a124115.html?cat=a)
    《頭條日報》 2009年1月10日
7.  [勞工及福利局局長在香港警察學院結業會操致辭全文](http://www.lwb.gov.hk/chi/speech/18102008.htm)
    新聞公報及刊物 2008年10月18日
8.  [香港繁榮穩定有賴優秀警隊](http://www.info.gov.hk/gia/general/200901/10/P200901100132.htm)《新聞公報》
    2009年1月10日
9.  [曾德成讚賞警隊是優秀隊伍](http://www.singtao.com/breakingnews/20090110a124115.asp)
    《星島日報》2009年1月10日
10. [香港警務處員額分布表](http://www.police.gov.hk/info/review/2010/tc/pdf/TC_E_annexes.pdf)
11. \[Kevin Sinclair & Nelson Ng: Asia's Finest Marches On ISBN
    9628513028\]
12. [香港警隊－如何在執法中運用資訊科技](http://www.icac.org.hk/newsl/issue5big5/pe.html)
    廉政公署
13. [警隊改革有助香港繁榮安定](http://www.info.gov.hk/gia/general/199812/12/1212138.htm)
    《新聞公報》 1998年12月12日
14. [中華人民共和國香港特別行政區成立五周年大事紀要](http://www.info.gov.hk/info/sar5/claw_2.htm)
    2002年7月
15. [政務司司長警務處顧客服務改善推展計劃啟用典禮演辭](http://www.info.gov.hk/gia/general/199906/03/0603079.htm)
    《新聞公報》 1999年6月3日
16. [行政長官讚揚警隊表現專業](http://www.police.gov.hk/offbeat/972/chi/index.htm)
    《警聲》第972期
17. [20th INTERPOL Asian Regional
    Conference](http://www.interpol.int/public/ICPO/speeches/2008/20thARCnoble20080305.asp)
    國際刑警組織
18. [衝出亞洲 走向世界](http://www.csb.gov.hk/hkgcsb/csn/53chi/3-7.pdf) 公務員事務局
19. [警察機動部隊是保護市民安全的英雄](http://www.hkcatch.com/zh_TW/news/people-living/421-ptu-the-protector-of-the-public)
     捕捉香港 2008年11月22日
20. [立法會保安事務委員會會議紀要](http://www.legco.gov.hk/yr98-99/chinese/panels/se/minutes/se051198.htm)
    1998年11月5日
21. [市建局主席勉勵結業學員　執法處事　剛柔並濟](http://www.police.gov.hk/offbeat/957/chi/index.htm)
    《警聲》第957期
22. [律政司司長檢閱警察學院結業會操致辭全文](http://www.info.gov.hk/gia/general/201210/27/P201210270335.htm)
    《新聞公報》 2012年10月27日
23. [知識管理成就卓越
    香港警隊蜚聲國際](http://www.csb.gov.hk/hkgcsb/csn/csn89/89c/close_up_1.html)
    公務員通訊 2014年2月