[Mikiki_Mall_Main_Enterance_201205.jpg](https://zh.wikipedia.org/wiki/File:Mikiki_Mall_Main_Enterance_201205.jpg "fig:Mikiki_Mall_Main_Enterance_201205.jpg")
[HK_新蒲崗_SPK_San_Po_Kong_景福街_King_Fuk_Street_譽港灣_Latitude_Mikiki_mall_night_turnaround_Mar-2014.JPG](https://zh.wikipedia.org/wiki/File:HK_新蒲崗_SPK_San_Po_Kong_景福街_King_Fuk_Street_譽港灣_Latitude_Mikiki_mall_night_turnaround_Mar-2014.JPG "fig:HK_新蒲崗_SPK_San_Po_Kong_景福街_King_Fuk_Street_譽港灣_Latitude_Mikiki_mall_night_turnaround_Mar-2014.JPG")\]\]

**譽·港灣**（**The
Latitude**），位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[新蒲崗](../Page/新蒲崗.md "wikilink")[太子道東](../Page/太子道東.md "wikilink")638號，該地皮原為[新蒲崗裁判法院和](../Page/新蒲崗裁判法院.md "wikilink")[新蒲崗政府合署的所在地](../Page/新蒲崗政府合署.md "wikilink")。2004年10月由[新鴻基地產購入該地皮作重建發展](../Page/新鴻基地產.md "wikilink")，項目提供1,159伙住宅單位，基座設有20萬[平方呎商場](../Page/平方呎.md "wikilink")[Mikiki](../Page/Mikiki.md "wikilink")，項目於2011年3月入-{伙}-。由新鴻基地產旗下的[康業服務負責管理](../Page/康業服務.md "wikilink")。

## 簡介

譽·港灣地盤[面積為](../Page/面積.md "wikilink")137,000[平方呎](../Page/平方呎.md "wikilink")，住宅樓面[面積為](../Page/面積.md "wikilink")1,025,000平方呎，合共5座，提供1,159伙單位，標準單位提供1至4房間隔，面積約571至2,032方呎。物業基座為佔地約21萬方呎商場Mikiki，於2011年9月開幕。項目於2009年4月中發售，整個項目用料成本約值10億元。唯商場裝修只佔1.6億元。

當中1A、2A座提供1,700至1,900方呎4房單位以及2,031及2,032方呎4房特大戶。交樓標準較其餘座數豪華，配備德國名牌Gaggenau爐具，浴室交樓標準包括Kohler浴缸、Hansgrohe水龍頭等。飯廳及睡房採用的玻璃就與同系的天璽同級，為雙層玻璃中空設計。

2012年5月1A座45樓A室複式戶，面積3217方呎連2個車位，於本月中以9007.6萬元售出，呎價2.8萬元。買家為嚴志明，以[漢語拼音Yan](../Page/漢語拼音.md "wikilink")
Zhiming登記，估計為一名來自[中國大陸的買家](../Page/中國大陸.md "wikilink")。

## 住客會所

譽‧港灣的住客會所命名為「Club
Latitude」，佔地約10萬平方呎，發展商聲稱耗資4億元建造，主要設施包括35米室外度假式泳池、20米室內泳池、多用途宴會廳、餐廳、保齡球場、舞蹈室、桌球室、兒童遊樂天地、迷你影院、多功能宴會廳、音樂創作室。

## 歷史

[新鴻基地產於](../Page/新鴻基地產.md "wikilink")2004年10月以47億元投得，每方呎樓面地價3,820元購入該幅土地。項目於2005年前命名為稱**萊茵港**（**Rhine
Harbour**）。模擬圖於2005年在[新鴻基地產的年報中首次曝光](../Page/新鴻基地產.md "wikilink")。2008年7月14日，地盤正式更名為「譽·港灣」，模擬圖中的設計也有輕微轉變，2009年11月1日，譽·港灣平頂，於2011年3月落成入伙。

## 商場

Mikiki位於譽·港灣樓下，商場樓高3層，總面積逾20萬平方呎，設約100間商舖，目標客群將以年輕消費群為主。於2011年8月開幕。

## 興建圖片

<File:HK> The Latitude.jpg|「譽·港灣」建築地盤入口（未掛有顯示物業名稱的橫幅）
<File:Thelatitude.jpg>|「譽·港灣」建築地盤入口 <File:HK> The Latitude
200906.jpg||「譽·港灣」建築地盤（2009年7月） <File:HK> Rhine Harbour
200806.jpg|「譽·港灣」建築地盤（2008年6月） <File:The> Latitude Tower 1A&2A
201010.jpg|譽·港灣則面（2010年10月） <File:The> Latitude
201104.jpg|thumb|250px|譽·港灣（2011年4月）

## 交通

譽·港灣對出的[太子道東及](../Page/太子道東.md "wikilink")[彩虹道有多線巴士途經](../Page/彩虹道.md "wikilink")，2019年會有行人天橋連接[港鐵](../Page/港鐵.md "wikilink")[啟德站](../Page/啟德站.md "wikilink")。而位於地庫的專線小巴總站亦於2011年8月起啟用，其中專線小巴[84由於客量不足於](../Page/九龍區專線小巴84線.md "wikilink")2012年12月1日停止服務，而專線小巴[85因長期虧損亦於](../Page/九龍區專線小巴85線.md "wikilink")2018年3月1日停止服務，目前只有[25B使用地庫的專線小巴總站](../Page/九龍區專線小巴25B線.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><strong><a href="../Page/港鐵.md" title="wikilink">港鐵</a></strong></dt>

</dl>
<ul>
<li><font color="{{屯馬綫色彩}}">█</font><a href="../Page/屯馬綫.md" title="wikilink">屯馬綫</a>：<a href="../Page/啟德站.md" title="wikilink">啟德站</a>(興建中)</li>
</ul>
<dl>
<dt><a href="../Page/爵祿街.md" title="wikilink">爵祿街</a></dt>

</dl>
<dl>
<dt><a href="../Page/康強街.md" title="wikilink">康強街</a></dt>

</dl>
<dl>
<dt>譽．港灣公共運輸交匯處</dt>

</dl>
<dl>
<dt><a href="../Page/協調道.md" title="wikilink">協調道</a></dt>

</dl>
<dl>
<dt><a href="../Page/彩虹道.md" title="wikilink">彩虹道</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>觀塘至青山道線 (通宵服務)[1]</li>
<li>荃灣至九龍城/黃大仙線[2]</li>
<li>黃大仙至青山道線[3]</li>
<li>黃大仙至旺角線 (24小時服務)[4]</li>
<li>慈雲山至旺角線 (24小時服務)[5]</li>
<li>慈雲山至佐敦道線[6]</li>
<li>慈雲山至銅鑼灣線 (下午至通宵服務)[7]</li>
</ul>
<dl>
<dt><a href="../Page/景福街.md" title="wikilink">景福街</a><br />
<a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>荃灣至坪石線[8]</li>
</ul>
<dl>
<dt><a href="../Page/太子道東.md" title="wikilink">太子道東</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">紅色小巴</a> (只在往觀塘方向停站)</dt>

</dl>
<ul>
<li>觀塘至<a href="../Page/青山道.md" title="wikilink">青山道線</a>[9]</li>
<li>觀塘至青山道線[10] (通宵線)</li>
<li>觀塘至<a href="../Page/美孚.md" title="wikilink">美孚線</a>[11]</li>
<li>觀塘至<a href="../Page/佐敦道.md" title="wikilink">佐敦道</a>/<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣線</a>[12] (24小時線)</li>
<li>牛頭角至旺角線[13]</li>
<li>牛頭角至佐敦道線[14] (上午線)</li>
<li>九龍灣至旺角線[15]</li>
<li>西貢至旺角線[16]</li>
<li>香港仔至觀塘線[17] (黃昏線)</li>
<li>觀塘至西環線[18] (上午線)</li>
<li>觀塘至銅鑼灣線[19](黃昏至通宵線)</li>
<li>中環至觀塘線[20] (晚上至通宵線)</li>
<li><a href="../Page/香港仔.md" title="wikilink">香港仔至觀塘線</a>[21] (黃昏線)</li>
<li><a href="../Page/紅磡.md" title="wikilink">紅磡至觀塘線</a> (24小時服務)[22]</li>
</ul></td>
</tr>
</tbody>
</table>

## 參見

  - [啟德發展計劃](../Page/啟德發展計劃.md "wikilink")

## 參考文獻

  - [Yahoo\!介紹：萊茵港基座「墊高」](http://hk.realestate.yahoo.com/061020/299/1v3vu.html)
  - [WHOMS.NET新聞：新地料未來樓市以現樓為主](http://www.whoms.net/servlet/template?series=24&article=6167)
  - [WHOMS.NET新聞：萊茵港料年內推售](http://www.whoms.net/servlet/print.jsp?series=24&article=10936)

## 外部連結

  - [「譽·港灣」官網](https://web.archive.org/web/20080721075642/http://www.the-latitude.com.hk/)
  - [譽‧港灣 The Latitude
    業主討論區](https://web.archive.org/web/20120619205649/http://www.thelatitudehk.com/)
  - [新鴻基地產官方介紹](http://www.shkp.com.hk/zh-hk/scripts/about/about_upcoming36.php)
  - [「譽·港灣」的外貌模擬圖](http://www.shkp.com.hk/zh-hk/images/about/upcoming/2005_photo_Kln6308.jpg)

[Category:新蒲崗](../Category/新蒲崗.md "wikilink")
[Category:黃大仙區私人屋苑](../Category/黃大仙區私人屋苑.md "wikilink")
[Category:新鴻基地產物業](../Category/新鴻基地產物業.md "wikilink")

1.  [觀塘及黃大仙—青山道](http://www.16seats.net/chi/rmb/r_k22.html)
2.  [荃灣荃灣街市街—黃大仙及九龍城](http://www.16seats.net/chi/rmb/r_kn45.html)
3.  [黃大仙及九龍城—青山道](http://www.16seats.net/chi/rmb/r_k62.html)
4.  [黃大仙及九龍城—旺角先達廣場](http://www.16seats.net/chi/rmb/r_k09.html)
5.  [慈雲山—旺角先達廣場](http://www.16seats.net/chi/rmb/r_k33.html)
6.  [慈雲山—佐敦道南京街](http://www.16seats.net/chi/rmb/r_k36.html)
7.  [慈雲山—銅鑼灣鵝頸橋](http://www.16seats.net/chi/rmb/r_kh16.html)
8.  [荃灣荃灣街市街　—　新蒲崗及坪石](http://www.16seats.net/chi/rmb/r_kn42.html)
9.  [觀塘協和街—青山道香港紗廠](http://www.16seats.net/chi/rmb/r_k02.html)
10. [觀塘及黃大仙—青山道](http://www.16seats.net/chi/rmb/r_k22.html)
11. [觀塘協和街—美孚](http://www.16seats.net/chi/rmb/r_k64.html)
12. [觀塘同仁街—佐敦道上海街](http://www.16seats.net/chi/rmb/r_k12.html)
13. [牛頭角站—旺角登打士街](http://www.16seats.net/chi/rmb/r_k24.html)
14. [牛頭角站—旺角登打士街](http://www.16seats.net/chi/rmb/r_k24.html)
15. [旺角\>九龍灣(Megabox)](http://www.i-busnet.com/minibus/minibus_red/mok_megabox.php)
16. [西貢市中心—旺角登打士街](http://www.16seats.net/chi/rmb/r_kn96.html)
17. [香港仔湖北街—觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
18. [觀塘宜安街＞西環卑路乍街,
    西環修打蘭街＞觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh18.html)
19. [銅鑼灣鵝頸橋—牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh01.html)
20. [中環／灣仔＞牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh41.html)
21. [香港仔湖北街—觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
22. [紅磡差館里＞牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_k15.html)