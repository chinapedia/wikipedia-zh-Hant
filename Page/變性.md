[TransgenreatParis2005.JPG](https://zh.wikipedia.org/wiki/File:TransgenreatParis2005.JPG "fig:TransgenreatParis2005.JPG")手掌上寫著一個字母為“XY”。\]\]

**變性者**（）經歷[性別認同與其出生時的](../Page/性別認同.md "wikilink")[性別指定不一致或性別表達與其出生時的](../Page/性別指定.md "wikilink")[性別指定沒有文化的相關性](../Page/性別指定.md "wikilink")，並希望身體永久轉變為符合他們的[性別認同](../Page/性別認同.md "wikilink")，通常尋求醫療援助（包括[激素替代療法和其他](../Page/激素替代療法.md "wikilink")[性別轉換療法](../Page/性別轉換療法.md "wikilink")），以幫助他們將自己的[生物性別與其](../Page/生物性別.md "wikilink")[性別認同對齊](../Page/性別認同.md "wikilink")。

**變性者**是[跨性別的子分類](../Page/跨性別.md "wikilink")，但是一些變性者拒絕[跨性別的標籤](../Page/跨性別.md "wikilink")\[1\]\[2\]\[3\]\[4\]\[5\]\[6\]\[7\]。如果一個人表達了願望認爲要成爲一種[生物性別以符合](../Page/生物性別.md "wikilink")[性別認同](../Page/性別認同.md "wikilink")，並且因其[性別認同而困擾](../Page/性別認同.md "wikilink")，他們就可以進行[性別不安的醫學診斷](../Page/性別不安.md "wikilink")。\[8\]
\[9\]

## 變性／性別重置過程

變性／性別重置的過程，通常包括了實際生活體驗、[荷爾蒙更換治療](../Page/荷爾蒙.md "wikilink")，以及旨在改造身體第一性徵的性別重置手術。在制度嚴謹的國家和地區，欲進行荷爾蒙或手術治療的變性者往往需要持久並反覆地向醫師表達強烈的治療慾望，他（她）們才會獲得合適的診斷。為了使身體與心理認同性別保持一致，很多變性女亦會選擇進行除毛、[隆胸和磨平](../Page/隆胸.md "wikilink")[喉結等整容手術](../Page/喉結.md "wikilink")，而很多變性男亦會選擇儘早進行[縮胸手術](../Page/縮胸.md "wikilink")。

在[英語中有時會用transsexual](../Page/英語.md "wikilink")，以用來減少它和精神病以及醫學上的關聯性。

## 歧視

變性人常被歧視，[人妖是中文中常用的一种](../Page/人妖.md "wikilink")[蔑称](../Page/蔑称.md "wikilink")。

人妖作为一種[蔑稱](../Page/蔑稱.md "wikilink")，原意指男扮女、女扮男、行為怪異、不守常法的人。《[南史](../Page/南史.md "wikilink")·卷四十五·崔慧景傳》：「東陽女子偵闛變服詐為丈夫，粗知圍棋，解文義，遍游公卿，仕至揚州議曹從事。……此人妖也。」

## 各地

### 泰國

[Kathoy1649.jpg](https://zh.wikipedia.org/wiki/File:Kathoy1649.jpg "fig:Kathoy1649.jpg")作歌舞秀的泰國變性女性\]\]
[泰國變性女性](../Page/泰國變性女性.md "wikilink")（泰文：กะเทย，羅馬拼音：Kathoey或Katoey，[IPA](../Page/國際音標.md "wikilink")：\[kaʔtʰɤːj\]）是[泰國男性變女性](../Page/泰國.md "wikilink")**變性**人，又名**ladyboy**。許多華人常將其稱之為「**泰國人妖**」。

在古代，กะเทย是指[雌雄同體](../Page/雌雄同體.md "wikilink")，與現代所指的[異裝者](../Page/異裝者.md "wikilink")、[變性者或](../Page/變性者.md "wikilink")[男同性戀者不同](../Page/男同性戀者.md "wikilink")。現代幾乎所有的กะเทย都有接荷爾蒙補充療法，大部分有作過[隆胸手術](../Page/隆胸.md "wikilink")，有些已接受[變性手術以及其他](../Page/變性手術.md "wikilink")[整形手術](../Page/整形.md "wikilink")。通常กะเทย很年輕就開始[異裝](../Page/異裝.md "wikilink")、注射[荷爾蒙](../Page/荷爾蒙.md "wikilink")、整形等等。กะเทย的異裝形象與身材通常非常女性化。กะเทย通常在娛樂業與旅遊業作舞者，歌舞秀，以及[性工作](../Page/性工作.md "wikilink")。也常有聽說kathoey在觀光區行竊。從事娛樂業與旅遊業之外的並不多，有些則是知名[美容](../Page/美容.md "wikilink")[理髮師](../Page/理髮.md "wikilink")。在泰國，多數家庭如果有個成為กะเทย的兒子會很失望。法律則完全不承認變性女人。即使在變性手術之後，變性女人也不能變更其法定性別。กะเทย通常來自社會低層，很大一部分幼年时期因生活所迫被贩卖后被改造成กะเทย，自殺率顯著高出一般人，平均寿命一般不超过五十岁。

變性者研究員、活動家普蘭普里達·巴莫·娜·阿育塔雅（Prempreeda Pramoj Na
Ayutthaya）指出，泰國存在對變性人教育與就業機會方面的性別歧視。\[10\]。2013年的多篇報導也指出，泰國尚未發現變性人從事高層官員、醫生、律師、科學家以及公辦學校教師等職位，在商業機構也不存在任職管理人員的變性人。簡言之，對變性女性來說，政府機構和大型企業的大門依舊緊閉。而這也是他們成為自僱人士或自由職業者的原因\[11\]。无论是否为政府職員，泰国法律都不给予进行手術後的男性變女性變性人在工作场合中穿着女性制服的权利\[12\]。

1996年，一個大半由[男同性戀與กะเทย組成的排球隊](../Page/男同性戀.md "wikilink")（外號「鐵娘子」）贏得全國[排球大賽冠軍](../Page/排球.md "wikilink")。但是泰國政府以國家形象為由禁止其中兩個kathoey參加國手隊作國際比賽。กะเทย排球隊的事件在2000年改編成賣座電影《[人妖打排球](../Page/人妖打排球.md "wikilink")》，並於2003年推出續集。

根據[東森新聞](../Page/東森新聞.md "wikilink")2004年6月與7月的報導，已常常見到娛樂業與旅遊業之外的變性女人，一所技術學校也設立了變性女人專用廁所，同志和變性女人並且於此時成立「泰國同性戀政治群」，目的是向政府在同志權利議題施壓。[帕莉亞是一位參加](../Page/帕莉亞.md "wikilink")[泰拳比賽的知名變性女性](../Page/泰拳.md "wikilink")。

### 伊朗

[伊朗是唯一認可變性權利的](../Page/伊朗.md "wikilink")[伊斯蘭國家](../Page/伊斯蘭.md "wikilink")。在1979年[伊朗革命前](../Page/伊朗革命.md "wikilink")，政府從未正式關注變性議題。從1980年代中期開始，伊朗伊斯兰共和国政府正式認可跨性別人士並且准許進行變性手術，惟與同性戀者一樣，跨性別人士受到極大的歧視。

## 相關電影

  - [玫瑰少年](../Page/玫瑰少年.md "wikilink")（比利時1997）
  - [-{zh-tw:男孩別哭; zh-hk:沒哭聲的抉擇;
    zh-cn:男孩别哭;}-](../Page/男孩別哭.md "wikilink")（美國1999）美國Teena
    Brandon的真實故事改編。
  - [人妖打排球](../Page/人妖打排球.md "wikilink")（泰國2000）1996年一個大半由[男同性戀與變性人組成的排球隊](../Page/男同性戀.md "wikilink")（外號「鐵娘子」）贏得全國[排球大賽冠軍的故事](../Page/排球.md "wikilink")。
  - [人妖打排球續集](../Page/人妖打排球續集.md "wikilink")（泰國2003）
  - [美麗拳王](../Page/美麗拳王.md "wikilink")（泰國2003）泰國有名的變性女人泰拳冠軍[帕莉亞的真實故事改編](../Page/帕莉亞.md "wikilink")。
  - [-{zh-hans:穿越美国;zh-hk:尋找他媽...的故事;zh-tw:窈窕老爸;}-](../Page/窈窕老爸.md "wikilink")（美國2005）美國一位還未動手術的MtF變性女人與兒子相處的故事。
  - [肥皂](../Page/肥皂_\(電影\).md "wikilink")（丹麦／瑞典2006）
  - [为己而生](../Page/为己而生.md "wikilink")（日本2006）
  - [穿高跟鞋的男生](../Page/穿高跟鞋的男生.md "wikilink") The Boy Who Wears High
    Heels （香港2013）
  - [丹麥女孩](../Page/丹麥女孩.md "wikilink")（美國2015）美國劇情傳記電影，講述第一名變性人莉莉·埃尔伯的故事
  - [當他們認真編織時](../Page/當他們認真編織時.md "wikilink")( 日本2017 )

## 參見

  - [跨性別](../Page/跨性別.md "wikilink")
  - [性別認同障礙](../Page/性別認同障礙.md "wikilink")
  - [異裝](../Page/異裝.md "wikilink")
  - [偽娘](../Page/偽娘.md "wikilink")
  - [藥娘](../Page/藥娘.md "wikilink")

## 注釋

## 外部連結

  - Peter Jackson, [Performative Genders, Perverse Desires: A
    Bio-History of Thailand's Same-Sex and Transgender
    Cultures](http://wwwsshe.murdoch.edu.au/intersections/issue9/jackson.html).（"The
    Homosexualisation of Cross-Dressing"那段）

  - Andrew Matzner: [In Legal Limbo: Thailand, Transgendered Men, and
    the
    Law](https://web.archive.org/web/20041120094937/http://home.att.net/~leela2/inlegallimbo.htm),
    1999.描述1999年時kathoey的一般社會與法律狀況。

  -
[跨性別](../Category/LGBT.md "wikilink")
[跨性別](../Category/跨性別.md "wikilink")
[Category:性學](../Category/性學.md "wikilink")
[Category:性別](../Category/性別.md "wikilink")
[變性人](../Category/變性人.md "wikilink")

1.  *Transgender Rights* (2006, ), edited by Paisley Currah, Richard M.
    Juang, Shannon Minter
2.  Thomas E. Bevan, *The Psychobiology of Transsexualism and
    Transgenderism* (2014, ), page 42: "The term transsexual was
    introduced by Cauldwell (1949) and popularized by Harry Benjamin
    (1966) \[...\]. The term transgender was coined by John Oliven
    (1965) and popularized by various transgender people who pioneered
    the concept and practice of transgenderism. It is sometimes said
    that Virginia Prince (1976) popularized the term, but history shows
    that many transgender people advocated the use of this term much
    more than Prince. The adjective *transgendered* should not be used
    \[...\]. Transsexuals constitute a subset of transgender people."
3.  A. C. Alegria, *Transgender identity and health care: Implications
    for psychosocial and physical evaluation*, in the *Journal of the
    American Academy of Nurse Practitioners*, volume 23, issue 4 (2011),
    pages 175–182: "Transgender, Umbrella term for persons who do not
    conform to gender norms in their identity and/or behavior
    (Meyerowitz, 2002). Transsexual, Subset of transgenderism; persons
    who feel discordance between natal sex and identity (Meyerowitz,
    2002)."
4.  Valentine, David. *Imagining Transgender: An Ethnography of a
    Category*, Duke University, 2007
5.  [Stryker, Susan](../Page/Susan_Stryker.md "wikilink"). Introduction.
    In Stryker and S. Whittle (Eds.), *The Transgender Studies Reader,*
    New York: Routledge, 2006. 1–17
6.  Kelley Winters, "Gender Madness in American Psychiatry, essays from
    the struggle for dignity, 2008, p. 198. "Some Transsexual
    individuals also identify with the broader transgender community;
    others do not."
7.
8.
9.
10. <http://www.bangkokpost.com/news/local/303850/sex-drugs-stigma-put-thai-transsexuals-at-hiv-risk>
11. [Katoey face closed
    doors](http://www.bangkokpost.com/opinion/opinion/355011/katoey-face-closed-doors)
12. <http://www.bangkokpost.com/news/local/301113/transgender-official-right-to-wear-dress>