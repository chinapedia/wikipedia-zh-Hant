[Allium_fistulosum_bulbifera0.jpg](https://zh.wikipedia.org/wiki/File:Allium_fistulosum_bulbifera0.jpg "fig:Allium_fistulosum_bulbifera0.jpg")
**變態莖**（）基本是指[植物中](../Page/植物.md "wikilink")[莖的一種分類](../Page/莖.md "wikilink")，因為生長環境而演變出有具有莖的基本功能，但是其生長型態有異於一般的莖就稱為變態莖，屬於植物的[營養器官的一種](../Page/營養.md "wikilink")。通常莖具備有支撐以及輸送或儲藏養分的功能，部份也有能行[光合作用的莖](../Page/光合作用.md "wikilink")，呈直立式支撐著整株植物，大多數都是植物的主幹。但是有些型態不同的，例如常見的[馬鈴薯就屬於](../Page/馬鈴薯.md "wikilink")**塊莖**，[大蒜](../Page/大蒜.md "wikilink")、[蔥是屬於](../Page/蔥.md "wikilink")**[鱗莖](../Page/鱗莖.md "wikilink")**…等；這些都是屬於「變態莖」。換句話說，植物的莖經過演化，改變了一般正常莖的型態，這些都算是**變態莖**。\[1\]

## 概要

[Potatoes.jpg](https://zh.wikipedia.org/wiki/File:Potatoes.jpg "fig:Potatoes.jpg")
[Colocasia_esculenta_P1190432.jpg](https://zh.wikipedia.org/wiki/File:Colocasia_esculenta_P1190432.jpg "fig:Colocasia_esculenta_P1190432.jpg")
一般比較為人所知的多細胞陸地植物是屬於[有胚植物](../Page/有胚植物.md "wikilink")，其中包括有[根](../Page/根.md "wikilink")、[莖](../Page/莖.md "wikilink")、[葉等](../Page/葉.md "wikilink")[器官的植物](../Page/器官.md "wikilink")，屬於[維管植物](../Page/維管植物.md "wikilink")。由於長久以來[環境上的影響](../Page/環境.md "wikilink")，有些部份會產生[演化](../Page/演化.md "wikilink")。植物的某些[器官因為對環境的](../Page/器官.md "wikilink")[適應而擁有的變化](../Page/適應.md "wikilink")，就稱為[變態](../Page/變態.md "wikilink")。而變態莖就是指植物的[莖產生了變態](../Page/莖.md "wikilink")，大致上可分為「發達」以及「退化」兩大類。不過無論是如何的改變了狀態，都還是能保有原本[莖的特徵](../Page/莖.md "wikilink")。\[2\]

依據莖的演化狀況的不同，變態莖可以分為地面下與地面上等兩個類別。地面下的莖統稱為地下莖，像是塊莖、球莖、鱗莖與根狀莖等都是屬於此類。地面上的莖統稱為地上莖，例如莖刺、葉狀莖、肉質莖、攀緣莖等。\[3\]

### 分辨

這類已經產生了演化過程的莖有時候看起來不像莖，尤其是地下莖的部份，常常都會被誤認為是[根](../Page/根.md "wikilink")。其實如上述一樣，莖即使變成看起來不像莖，但是還是有其屬於莖的形態與功能。舉例來說，我們一般常見的[胡蘿蔔就不屬於莖](../Page/胡蘿蔔.md "wikilink")，而是屬於「塊根」。[馬鈴薯它屬於塊莖](../Page/馬鈴薯.md "wikilink")，並不是根。

在一般正常的情況之下，莖的特性在於莖上方固定的地方會長出葉子、葉腋會長出芽、葉與芽之間的地方稱為節、節與節之間的部分稱為節間、有氣孔或是皮孔等。這些是根所沒有的型態，所以蕃薯上長有根毛，不會直接長出葉子或開花，是屬於根的特性。而馬鈴薯會長芽和鱗葉，這都是屬於莖的特質，所以它是屬於變態莖中的塊莖。\[4\]

## 種類

[Pumpkin_with_stalk.jpg](https://zh.wikipedia.org/wiki/File:Pumpkin_with_stalk.jpg "fig:Pumpkin_with_stalk.jpg")
[SADEHodHasharon.jpg](https://zh.wikipedia.org/wiki/File:SADEHodHasharon.jpg "fig:SADEHodHasharon.jpg")
**變態莖**的常見的種類大致可以分為以下數種，由於並沒有統一的特殊名稱，所以多以其型態的狀況來述說種類。\[5\]\[6\]

  - **[塊莖](../Page/塊莖.md "wikilink")**
    ()，短而肥大且具有薄壁組織的莖，可以儲存豐富的養分，有些還含有對人體有害的毒素，例如：馬鈴薯等。
  - **[球莖](../Page/球莖.md "wikilink")**
    ()，部份會膨脹成圓球型或橢圓形，節與節之間比較明顯，例如：[芋](../Page/芋.md "wikilink")、[慈菇與](../Page/慈菇.md "wikilink")[荸薺等](../Page/荸薺.md "wikilink")。
  - **[鱗莖](../Page/鱗莖.md "wikilink")**
    ()，盤狀型而且較短，其上方生出肉厚有如鱗片般的鱗葉，例如：[水仙](../Page/水仙.md "wikilink")、[洋蔥](../Page/洋蔥.md "wikilink")、[大蒜](../Page/大蒜.md "wikilink")、韮蔥、青蔥等。
  - **[根莖](../Page/根莖.md "wikilink")**
    ()，顧名思義是型態像一般的根，節與節之間也非常明顯。[竹就有這種莖](../Page/竹.md "wikilink")，稱為竹鞭；[蓮的根莖就是蓮藕](../Page/蓮.md "wikilink")，以及[生薑也是屬於此類的變態莖](../Page/生薑.md "wikilink")。
  - **莖刺**，又稱針莖，主要變態的地方是芽，會成針刺狀，維管組織相連。例如[玫瑰](../Page/玫瑰.md "wikilink")、[柚子皂等](../Page/柚子皂.md "wikilink")。
  - **葉狀莖**，有明顯的節以及節間，扁扁的狀態酷似葉片，而原本的葉已退化。例如[竹節蓼](../Page/竹節蓼.md "wikilink")、[天門冬等](../Page/天門冬.md "wikilink")。
  - **肉質莖**，綠色，能行[光合作用](../Page/光合作用.md "wikilink")，薄壁較一般發達，有貯藏水分的功能。其直接長出的葉片已退化或是成針葉狀，適合生長於乾旱地帶，例如[仙人掌](../Page/仙人掌.md "wikilink")、[火龍果等](../Page/火龍果.md "wikilink")。
  - **攀緣莖**，又稱鬚莖，屬於[攀緣植物一類](../Page/攀緣植物.md "wikilink")，主要是由枝所演化的變態莖。這類莖必須攀附在其他的物體上纏繞生長，例如[葡萄](../Page/葡萄.md "wikilink")、[黃瓜](../Page/黃瓜.md "wikilink")、[南瓜與](../Page/南瓜.md "wikilink")[長春藤等](../Page/長春藤.md "wikilink")。
  - **匍匐莖**，柔軟而無法直立，利用攀爬生長於地上的莖，一般常見的植物有[甘藷](../Page/甘藷.md "wikilink")、[草莓](../Page/草莓.md "wikilink")、[西瓜等](../Page/西瓜.md "wikilink")。

## 参考來源與註解

<div class="references-small">

<references />

</div>

## 參見

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/植物.md" title="wikilink">植物</a></li>
<li><a href="../Page/莖.md" title="wikilink">莖</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/馬鈴薯.md" title="wikilink">馬鈴薯</a></li>
<li><a href="../Page/大蒜.md" title="wikilink">大蒜</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [植物的莖](https://web.archive.org/web/20071008171642/http://content1.edu.tw/publish/dreambluesky/material/219912/Dolphin/page-2-b.htm)

  - [植物的基本構造](http://www.biol.tsukuba.ac.jp/~algae/BotanyWEB/plan.html)

  - [有關植物構造的詳細說明](http://www.geocities.jp/a124351514/Know/Plants/shosai.html)

  - [變態莖的介紹](http://www.backyardnature.net/stemtype.htm)

[en:Plant_stem\#Specialized_terms_for_stems](../Page/en:Plant_stem#Specialized_terms_for_stems.md "wikilink")

[B](../Category/植物解剖學.md "wikilink")

1.  參考：[台灣海洋生態資訊學習網](http://study.nmmba.gov.tw/01_room/encyclopedia.aspx?enc_rid=294&res_objno=ossary0295)中的解說。
2.  參考：[中國大百科智慧藏](http://210.240.193.70/xency/Content.asp?ID=19511)
3.  參考：[中國大百科智慧藏](http://210.240.193.70/xency/Content.asp?ID=19511)
4.  參考：[午夜飛行的魔幻王國─為什麼馬鈴薯是莖，蘿蔔是根？](http://www.wretch.cc/blog/MidnightFly&article_id=1891893)作者：快樂雪兒，引用自[工研院的說明資料](../Page/工研院.md "wikilink")
5.  參考：[午夜飛行的魔幻王國─為什麼馬鈴薯是莖，蘿蔔是根？](http://www.wretch.cc/blog/MidnightFly&article_id=1891893)作者：快樂雪兒，引用自[工研院的說明資料](../Page/工研院.md "wikilink")
6.  參考：[中國大百科智慧藏](http://210.240.193.70/xency/Content.asp?ID=19511)