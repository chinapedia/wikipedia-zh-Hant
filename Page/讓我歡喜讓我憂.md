**讓我歡喜讓我憂**是[香港歌手](../Page/香港.md "wikilink")[周華健的一張](../Page/周華健.md "wikilink")[國語專輯](../Page/國語.md "wikilink")，發行於1991年11月28日，由[滾石唱片公司發行](../Page/滾石唱片.md "wikilink")，唱片編號：RD1151。

## 簡介

據統計，這張專輯是周華健銷量最高的專輯之一，当年正版銷量逾200萬張。專輯中包括《讓我歡喜讓我憂》、《怕黑》、《你現在還好嗎》在內的歌曲，都是廣為人知的經典。其中《讓我歡喜讓我憂》是翻唱自[日本知名團體](../Page/日本.md "wikilink")[恰克與飛鳥於](../Page/恰克與飛鳥.md "wikilink")1981年發行之單曲「」([男與女](../Page/男與女_\(恰克與飛鳥單曲\).md "wikilink"))\[1\]。

## 曲目

<table>
<tbody>
<tr class="odd">
<td><p><font color=darkgreen><strong>曲序</strong></font></p></td>
<td><p><font color=darkgreen><strong>曲名</strong></font></p></td>
<td><p><font color=darkgreen><strong>作曲</strong></font></p></td>
<td><p><font color=green><strong>填詞</strong></font></p></td>
<td><p><font color=darkgreen><strong>編曲</strong></font></p></td>
<td><p><font color=darkgreen><strong>附註</strong></font></p></td>
</tr>
<tr class="even">
<td><p>01</p></td>
<td><p>你現在還好嗎</p></td>
<td><p><a href="../Page/劉志宏.md" title="wikilink">劉志宏</a></p></td>
<td><p><a href="../Page/娃娃.md" title="wikilink">娃娃</a></p></td>
<td><p><a href="../Page/Wayne_Miller.md" title="wikilink">Wayne Miller</a></p></td>
<td><p>粵語原曲為<a href="../Page/曾航生.md" title="wikilink">曾航生</a>《你現在還好嗎》</p></td>
</tr>
<tr class="odd">
<td><p>02</p></td>
<td><p>讓我歡喜讓我憂</p></td>
<td><p><a href="../Page/Ryo_Aska.md" title="wikilink">Ryo Aska</a></p></td>
<td><p><a href="../Page/李宗盛.md" title="wikilink">李宗盛</a></p></td>
<td><p><a href="../Page/鮑比達.md" title="wikilink">鮑比達</a></p></td>
<td><p>日語原曲<a href="../Page/CHAGE&amp;ASKA.md" title="wikilink">CHAGE&amp;ASKA</a>《男と女》<br />
粵語原曲為<a href="../Page/葉蒨文.md" title="wikilink">葉蒨文</a>《情人知己》<br />
台語原曲為<a href="../Page/林慧萍.md" title="wikilink">林慧萍</a>《傷心PARTY》</p></td>
</tr>
<tr class="even">
<td><p>03</p></td>
<td><p>怕黑</p></td>
<td><p><a href="../Page/周華健.md" title="wikilink">周華健</a></p></td>
<td><p>周華健</p></td>
<td><p>Wayne Miller</p></td>
<td><p>粵語原曲為《怕黑》</p></td>
</tr>
<tr class="odd">
<td><p>04</p></td>
<td><p>兩種</p></td>
<td><p>周華健</p></td>
<td><p><a href="../Page/武雄.md" title="wikilink">武雄</a></p></td>
<td><p><a href="../Page/江建民_(音乐).md" title="wikilink">江建民</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>05</p></td>
<td><p>我站在全世界的屋頂</p></td>
<td><p><a href="../Page/陳揚.md" title="wikilink">陳揚</a></p></td>
<td><p><a href="../Page/陳可華.md" title="wikilink">陳可華</a></p></td>
<td><p>Wayne Miller</p></td>
<td><p>原唱<a href="../Page/张艾嘉.md" title="wikilink">张艾嘉</a></p></td>
</tr>
<tr class="odd">
<td><p>06</p></td>
<td><p>傷心的歌</p></td>
<td><p><a href="../Page/Rod_Troot.md" title="wikilink">Rod Troot</a><br />
<a href="../Page/Jon_Sweet.md" title="wikilink">Jon Sweet</a></p></td>
<td><p>周華健</p></td>
<td><p>鮑比達</p></td>
<td><p>英語原曲為<a href="../Page/奇里夫·李察.md" title="wikilink">奇里夫·李察</a>《Ocean Deep》</p></td>
</tr>
<tr class="even">
<td><p>07</p></td>
<td><p>若不能擁有你</p></td>
<td><p>劉志宏</p></td>
<td><p>李宗盛</p></td>
<td><p>Wayne Miller</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>08</p></td>
<td><p>讓世界叫響這個名字</p></td>
<td><p><a href="../Page/張弘毅.md" title="wikilink">張弘毅</a></p></td>
<td><p>-{<a href="../Page/范可欽.md" title="wikilink">范可欽</a>}-</p></td>
<td><p><a href="../Page/張乃仁.md" title="wikilink">張乃仁</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>09</p></td>
<td><p>我不要知道我的未來</p></td>
<td><p>周華健</p></td>
<td><p>武雄<br />
周華健</p></td>
<td><p>Wayne Miller</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>璀璨</p></td>
<td><p>周華健</p></td>
<td><p>周華健</p></td>
<td><p>周華健</p></td>
<td></td>
</tr>
</tbody>
</table>

## 唱片版本

  - 錄音帶版
  - CD版

## 參考資料

[Category:周華健音樂專輯](../Category/周華健音樂專輯.md "wikilink")
[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:1991年音樂專輯](../Category/1991年音樂專輯.md "wikilink")
[Category:滚石唱片音乐专辑](../Category/滚石唱片音乐专辑.md "wikilink")
[Category:台灣百佳音樂專輯](../Category/台灣百佳音樂專輯.md "wikilink")

1.  \[[http://ja.wikipedia.org/wiki/%E5%91%A8%E8%8F%AF%E5%81%A5\]日本維基，2010年6月13日](http://ja.wikipedia.org/wiki/%E5%91%A8%E8%8F%AF%E5%81%A5%5D日本維基，2010年6月13日)