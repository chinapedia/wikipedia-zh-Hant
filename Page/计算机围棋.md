**计算机[围棋](../Page/围棋.md "wikilink")**是[人工智能](../Page/人工智能.md "wikilink")（AI）的一个领域，该领域致力于开发出可以下围棋的[电脑程式](../Page/电脑程式.md "wikilink")。围棋是[棋盘游戏的一种](../Page/棋盘游戏.md "wikilink")，有很古老的历史。

## 歷史

### alpha-beta 剪枝法

最先電腦圍棋也試圖用類似處理西洋棋的演算法——[alpha-beta
剪枝法](../Page/Alpha-beta剪枝.md "wikilink")，即一般認為的暴力搜尋法，但成長非常慢。[1986年](../Page/1986年.md "wikilink")，[應昌期懸賞](../Page/應昌期.md "wikilink")100萬美金，徵求可以打敗人類的圍棋軟體，並以15年為期限，但沒有任何人拿走獎金。到了20世紀末，這類程式表現最好的是[陳志行製作的](../Page/陈志行.md "wikilink")[手談](../Page/手談.md "wikilink")，其宣稱可以接近業餘初段，至少與低段職業差距9子以上，其他如[GNU
Go更是只有業餘](../Page/GNU_Go.md "wikilink")5\~10級左右。

代表：

  - [手談 (圍棋軟體)](../Page/手談_\(圍棋軟體\).md "wikilink")
  - [GNU Go](../Page/GNU_Go.md "wikilink")

### 蒙地卡羅搜尋樹

[Crazy
Stone首次引進了](../Page/Crazy_Stone.md "wikilink")[蒙地卡羅搜尋樹](../Page/蒙地卡羅搜尋樹.md "wikilink")，其原理是用[蒙地卡羅法快速的把棋局下至終局](../Page/蒙地卡羅方法.md "wikilink")，然後藉此判斷局勢，用這個方法，電腦圍棋得到飛快性的成長，並突破了業餘初段的壁障。這時代表現最好的是[Zen](../Page/Zen.md "wikilink")，在AlphaGo出現的前一年，Zen的平行運算版本可以達到與職業棋士差距3\~4子的水平。

這時期開始，開始出現了[UEC杯等電腦圍棋比賽](../Page/UEC杯世界電腦圍棋大會.md "wikilink")。在其中發生一個插曲，2010年時，[黃士傑的](../Page/黃士傑.md "wikilink")[Erica在](../Page/Erica.md "wikilink")2010電腦奧林匹亞獲得19路圍棋的冠軍，隔年又在UEC盃拿下亞軍，這在當時引起許多注目，因為Erica是單機程式，而其對手都是使用大型電腦，這也使得他獲得[DeepMind公司的邀請](../Page/DeepMind.md "wikilink")。

代表程式：

  - [Zen](../Page/Zen.md "wikilink")
  - [Crazy Stone](../Page/Crazy_Stone.md "wikilink")
  - [石子旋風](../Page/石子旋風.md "wikilink")
  - [Fuego](../Page/Fuego.md "wikilink")

### 深度學習

[深度學習原本主要應用是圖像分析](../Page/深度学习.md "wikilink")，利用電腦[模擬神經元](../Page/人工神经网络.md "wikilink")，可以訓練電腦有類似人類「直覺」的反應，2014年左右，[Google
DeepMind和](../Page/Google_DeepMind.md "wikilink")[facebook等公司意識這可能可以用在處理電腦圍棋](../Page/facebook.md "wikilink")。最直接的想法是輸入人類的圍棋棋譜，並在程式中設定圍棋規則，以及各棋譜的最後勝負，利用[監督學習讓電腦得到](../Page/監督式學習.md "wikilink")「棋感」，電腦因而可以給出特定局面下有哪些可能的行棋方法，後來這個方法在[AlphaGo的論文中被稱為](../Page/AlphaGo.md "wikilink")「走子網路」。2015年左右，[DeepMind的David](../Page/DeepMind.md "wikilink")
Silver意識到，其實圍棋的形勢判斷也可以交由神經網路決定，「價值網路」因此誕生。接著[DeepMind團隊再使用](../Page/DeepMind.md "wikilink")[強化學習](../Page/强化学习.md "wikilink")——大眾媒體稱之為左右互搏——增強兩種神經網路，在大約三千萬盤的左右互搏後，超越了職業選手水平，這使得[DeepMind最終贏得這項與facebook的競賽](../Page/DeepMind.md "wikilink")。

2016年1月27日，《[自然](../Page/自然_\(期刊\).md "wikilink")》發表了[Google
DeepMind开发](../Page/Google_DeepMind.md "wikilink")[AlphaGo的論文](../Page/AlphaGo.md "wikilink")，于2015年10月，在未讓子的挑戰中，以5:0戰績，擊敗歐洲圍棋冠軍——職業圍棋二段[樊麾](../Page/樊麾.md "wikilink")。這是電腦程式首次在公平比賽中擊敗職業棋手。2016年3月，AlphaGo在韓國首爾以4:1擊敗棋士[李世乭](../Page/李世乭.md "wikilink")。\[1\]\[2\]
2017年5月,
AlphaGo在[中国乌镇围棋峰会的三局比赛中击败](../Page/中国乌镇围棋峰会.md "wikilink")\[3\]当时世界排名第一\[4\]\[5\]的中国棋手[柯洁](../Page/柯洁.md "wikilink")。

代表程式：

  - [AlphaGo](../Page/AlphaGo.md "wikilink")
  - [AlphaGo Zero](../Page/AlphaGo_Zero.md "wikilink")
  - [CGI](../Page/CGI_\(圍棋軟體\).md "wikilink")
  - [Darkforest](../Page/Darkforest.md "wikilink")（facebook最終失敗的計畫）
  - [DeepZenGo](../Page/DeepZenGo.md "wikilink")
  - [ELF OpenGo](../Page/ELF_OpenGo.md "wikilink")
  - [Leela](../Page/Leela.md "wikilink")
  - [Leela Zero](../Page/Leela_Zero.md "wikilink")
  - [PhoenixGo](../Page/PhoenixGo.md "wikilink")
  - [絕藝](../Page/絕藝.md "wikilink")

## 难点

圍棋給程式設計師們帶來了許多[人工智能領域裡的挑戰](../Page/人工智能.md "wikilink")。當如[IBM深藍那樣的超級電腦](../Page/深藍_\(電腦\).md "wikilink")，已經能夠擊敗世界上最好的西洋棋棋手的同時；卻有不少人能擊敗圍棋軟體。可見，要編寫出超越初級水平的電腦圍棋程式，是極其困難的一回事。

### 棋盘太大

围棋的棋盘很大（19×19），因此通常被认为是难以编写围棋程序的一个重要原因。然而，像[Amazon这样的游戏](../Page/亞馬遜棋.md "wikilink")，，却较易编写出超越初级水平的电脑程序。

### 可行的着法太多

与其它棋盘游戏相比，围棋的着法几乎不受规则限制。[中国象棋第一步有](../Page/中国象棋.md "wikilink")42种选择，[国际象棋有](../Page/国际象棋.md "wikilink")20种选择，但围棋有361种选择。有些着法较常见，有些几乎从未走过（例如第一步下在边线上），但所有着法都有可能。

象棋（以及大部分棋盘游戏如[西洋跳棋和](../Page/西洋跳棋.md "wikilink")[双陆棋](../Page/双陆棋.md "wikilink")）棋局过程中，棋子数逐渐减少，使游戏简化。但是，围棋中每下一子，都会使局势变得更复杂。

### 估值函数

### 组合问题

## 策略搜索

## 状态表示

## 系统设计

### 处理问题的新方法

### 编程语言选择

### 设计哲学

#### Minimax 树搜索

#### [蒙特卡罗方法](../Page/蒙特卡罗方法.md "wikilink")

#### Knowledge-based 系统

#### 机器学习

## 电脑围棋程序的竞赛

### 历史

第一個電腦圍棋競賽是由[USENIX贊助](../Page/USENIX.md "wikilink")，在1984年到1988年間舉行。

### 电脑对电脑程序中的问题

## 注释和参考

<references/>

### 参考文献

1.  [AI-oriented survey of
    Go](http://citeseer.ist.psu.edu/bouzy01computer.html)
2.  [Monte-Carlo
    Go](https://web.archive.org/web/20040919093249/http://www.cs.ualberta.ca/~emarkus/monte-carlo/monte-carlo.pdf),
    presented by Markus Enzenberger, Computer Go Seminar, University of
    Alberta, April 2004
3.  [Monte-Carlo
    Go](http://www.math-info.univ-paris5.fr/~bouzy/publications/bouzy-helmstetter.pdf),
    written by B. Bouzy and B. Helmstetter from Scientific Literature
    Digital Library
4.  [Static analysis of life and death in the game of
    Go](http://www.cs.ualberta.ca/~games/go/seminar/2002/020703/ld.pdf),
    written by Ken Chen & Zhixing Chen, 20 February 1999
5.  [Co-Evolving a Go-Playing Neural
    Network](http://nn.cs.utexas.edu/downloads/papers/lubberts.coevolution-gecco01.pdf),
    written by Alex Lubberts & Risto Miikkulainen, 2001

## 参见

  - [圍棋\#围棋软件与人工智慧](../Page/圍棋#围棋软件与人工智慧.md "wikilink")
  - [围棋程序列表](../Page/围棋程序列表.md "wikilink")
  - [Go Text Protocol](../Page/Go_Text_Protocol.md "wikilink")

## 外部链接

### General info

  - [Online Computer Go
    bibliography](https://web.archive.org/web/20070714221230/http://www.cs.ualberta.ca/~emarkus/compgo_biblio/).
  - [Computer Go](http://senseis.xmp.net/?ComputerGo) and [Computer Go
    Programming](http://senseis.xmp.net/?ComputerGoProgramming) pages at
    [Sensei's Library](http://senseis.xmp.net)
  - [computer-go mailing
    list](http://www.computer-go.org/mailman/listinfo/computer-go/)
  - The Computer Go Room on the [Kiseido Go
    Server](http://kgs.kiseido.com) (KGS) for online discussion and
    running "bots"
  - [Playing with
    Shannon](https://web.archive.org/web/20070519083703/http://mizarchessengine.agff.net/):
    a forum about computer go programming

### Specific info

  - [Information on the Go Text
    Protocol](http://www.lysator.liu.se/~gunnar/gtp/) commonly used for
    interfacing Go playing engines with graphical clients and internet
    servers
  - Kinger, Tim and Mechner, David. *[An Architecture for Computer
    Go](https://web.archive.org/web/20061029224917/http://www.cns.nyu.edu/~mechner/compgo/acg/)*
    (1996年)

<!-- end list -->

  - Published articles about computer go on
    [Ideosphere](http://www.ideosphere.com/fx-bin/Claim?claim=GoCh)
    gives current estimate of whether a Go program will be best player
    in the world
  - [XS4All Internet
    B.V.](http://www.xs4all.nl/~janrem/Artikelen/Artikelen.html)
  - [Minimalism in Ubiquitous Interface
    Design](http://affect.media.mit.edu/pdfs/04.wren-reynolds.pdf) by
    Wren and Reynolds describes a simple computer vision system for
    playing Go

### 计算机程序

  - [Go++](http://www.goplusplus.com) by Michael Reiss
  - [Handtalk](http://www.airgo.com.tw/progdetail_online.asp?id=G029&type=G),
    developed in China by Zhixing Chen
  - [The Many Faces of Go](http://www.smart-games.com/manyfaces.html) by
    David Fotland
  - [KCC](http://www.silverstar.co.jp) Igo, from Korea (sold as Silver
    Star in Japan)
  - [Go Intellect](http://www.yutopian.com/go/soft/EAC19.html) by Ken
    Chen
  - [GNU Go](http://www.gnu.org/software/gnugo), the strongest [open
    source](../Page/open_source.md "wikilink") Go program
  - [Smart Go](http://www.smartgo.com/) by Anders Kierulf, inventor of
    the [Smart Game Format](../Page/Smart_Game_Format.md "wikilink")
  - [Free Go
    Software](http://www.gnu.org/software/gnugo/free_go_software.html)
  - [GoKnot, a Windows solution open for
    developing](https://web.archive.org/web/20100127065951/http://www.goknot.eu.com/)
  - [MIni GO Solver](../Page/MIGOS.md "wikilink")
  - [AYA](https://web.archive.org/web/20061211061341/http://www15.ocn.ne.jp/~yss/)
    by Hiroshi Yamashita
  - [CrazyStone](http://remi.coulom.free.fr/CrazyStone/) by Rémi Coulom
  - [GNU Go](../Page/GNU_Go.md "wikilink"), the strongest [open
    source](../Page/open_source.md "wikilink") Go program
  - [Go++](http://www.goplusplus.com) by Michael Reiss (sold as
    *Strongest Go* or Tuyoi Igo in Japan)
  - Go Intellect by Ken Chen
  - Handtalk/Goemate, developed in China by Zhixing Chen (sold as Shudan
    Taikyoku in Japan)
  - Haruka by Ryuichi Kawa (sold as Saikouhou in Japan)
  - Indigo by Bruno Bouzy
  - Katsunari by Shin-ichi Sei
  - KCC Igo, from North Korea (sold as Silver Star or Ginsei Igo in
    Japan)
  - [The Many Faces of Go](http://www.smart-games.com/manyfaces.html) by
    David Fotland (sold as AI Igo in Japan)
  - [MoGo](https://web.archive.org/web/20061128074317/http://www.lri.fr/~gelly/MoGo.htm)
    by Sylvain Gelly
  - [Smart Go](http://www.smartgo.com/) by Anders Kierulf, inventor of
    the [Smart Game Format](../Page/Smart_Game_Format.md "wikilink")
  - [Leela及](../Page/Leela.md "wikilink")[Leela
    Zero](../Page/Leela_Zero.md "wikilink")

### Computer Go vs human/computer & tournament

  - [Comprehensive list of past computer go
    events](http://www.computer-go.info/events/index.html)
  - [All systems
    Go](https://web.archive.org/web/20061029225411/http://www.cns.nyu.edu/~mechner/compgo/sciences/)
    by David A. Mechner, discusses the game where professional go player
    [Janice Kim](../Page/Janice_Kim.md "wikilink") won a game against
    program [Handtalk](../Page/Handtalk.md "wikilink") after giving a
    25-stone handicap.
  - [Two Representative Computer Go
    Games](http://www.cs.ualberta.ca/~mmueller/cgo/survey/twogames.html),
    an article about two computer go games, the one with two computers
    players, and the other, a 29-stone handicap human-computer game

[category:人工智能](../Page/category:人工智能.md "wikilink")

[Category:围棋](../Category/围棋.md "wikilink")
[Category:电脑游戏](../Category/电脑游戏.md "wikilink")
[Category:电子棋類游戏](../Category/电子棋類游戏.md "wikilink")
[Category:游戏人工智能](../Category/游戏人工智能.md "wikilink") [Go
programming](../Category/Computer_and_video_board_games.md "wikilink")
[Category:Game artificial
intelligence](../Category/Game_artificial_intelligence.md "wikilink")

1.  {{ cite web |
    url=<http://www.nature.com/nature/journal/v529/n7587/full/nature16961.html>
    | title=深度神经网络加树形检索可以下围棋了 | author=David Silver, Aja Huang, Chris J.
    Maddison, Arthur Guez, Laurent Sifre, George van den Driessche,
    Julian Schrittwieser, Ioannis Antonoglou, Veda Panneershelvam, Marc
    Lanctot, Sander Dieleman, Dominik Grewe, John Nham, Nal
    Kalchbrenner, Ilya Sutskever, Timothy Lillicrap, Madeleine Leach,
    Koray Kavukcuoglu, Thore Graepel & Demis Hassabis | date=2016-01-27
    | accessdate=2016-01-29 | language=en }}
2.  {{ cite web | url=<http://www.guokr.com/article/441144/> |
    title=面对谷歌围棋AI，人类最后的智力骄傲即将崩塌 | author=开明 | date=2016-01-28 |
    accessdate=2016-01-29 }}
3.
4.
5.