<div class="thumb tright">

<div class="thumbinner" style="width: 200px;">

|                                                                                                                           |                                                                                                                                                   |
| ------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Utah_teapot_simple_2.png](https://zh.wikipedia.org/wiki/File:Utah_teapot_simple_2.png "fig:Utah_teapot_simple_2.png") | [Sorting_quicksort_anim_frame.png](https://zh.wikipedia.org/wiki/File:Sorting_quicksort_anim_frame.png "fig:Sorting_quicksort_anim_frame.png") |
| [Lambda_lc.svg](https://zh.wikipedia.org/wiki/File:Lambda_lc.svg "fig:Lambda_lc.svg")                                    | [3-Tasten-Maus_Microsoft.jpg](https://zh.wikipedia.org/wiki/File:3-Tasten-Maus_Microsoft.jpg "fig:3-Tasten-Maus_Microsoft.jpg")                  |

<div class="thumbcaption">

计算机科学用于解决信息与计算的理论基础，以及实现和应用它们的实用技术。

</div>

</div>

</div>

**计算机科学**（，有时[缩写为](../Page/缩写.md "wikilink")）是系统性研究[信息与](../Page/信息.md "wikilink")[计算的理论基础以及它们在](../Page/计算.md "wikilink")[计算机系统中如何](../Page/计算机.md "wikilink")与应用的实用技术的学科。\[1\]
\[2\]它通常被形容为对那些创造、描述以及转换信息的[算法处理的系统研究](../Page/算法.md "wikilink")。计算机科学包含很多[分支领域](../Page/分支领域.md "wikilink")；有些强调特定结果的计算，比如[计算机图形学](../Page/计算机图形学.md "wikilink")；而有些是探討[计算问题的性质](../Page/计算问题.md "wikilink")，比如[计算复杂性理论](../Page/计算复杂性理论.md "wikilink")；还有一些领域專注于怎样实现计算，比如[程式語言理論是研究描述计算的方法](../Page/程式語言理論.md "wikilink")，而[程式设计是应用特定的](../Page/程式设计.md "wikilink")[程式語言解决特定的计算问题](../Page/程式語言.md "wikilink")，[人机交互则是專注于怎样使计算机和计算变得有用](../Page/人机交互.md "wikilink")、好用，以及随时随地为[人所用](../Page/人.md "wikilink")。

有时公众会误以为计算机科学就是解决计算机问题的事业（比如[信息技术](../Page/信息技术.md "wikilink")），或者只是与使用计算机的经验有关，如玩游戏、上网或者文字处理。其实计算机科学所关注的，不仅仅是去理解实现类似游戏、浏览器这些软件的程序的性质，更要通过现有的知识创造新的程序或者改进已有的程序。\[3\]

尽管计算机科学（computer
science）的名字里包含计算机这几个字，但实际上计算机科学相当数量的领域都不涉及计算机本身的研究。因此，一些新的名字被提议出来。某些重点大学的院系倾向于术语*计算科学*（computing
science），以精确强调两者之间的不同。丹麦科学家[Peter
Naur建议使用术语](../Page/Peter_Naur.md "wikilink")"datalogy"，以反映这一事实，即科学学科是围绕着数据和数据处理，而不一定要涉及计算机。第一个使用这个术语的科学机构是哥本哈根大学Datalogy学院，该学院成立于1969年，Peter
Naur便是第一任教授。这个术语主要被用于北欧国家。同时，在计算技术发展初期，《ACM通讯》建议了一些针对计算领域从业人员的术语：turingineer，turologist，flow-charts-man，applied
meta-mathematician及applied epistemologist。\[4\]
三个月后在同样的期刊上，*comptologist*被提出，第二年又变成了*hypologist*。\[5\]
术语*computics*也曾经被提议过。在欧洲大陆，起源于信息（information）和数学或者自动（automatic）的名字比起源于计算机或者计算（computation）更常见，如informatique（法语），Informatik（德语），informatika（[斯拉夫语族](../Page/斯拉夫语族.md "wikilink")）。

著名计算机科学家[Edsger
Dijkstra曾经指出](../Page/艾兹赫尔·戴克斯特拉.md "wikilink")：“计算机科学并不只是关于计算机，就像天文学并不只是关于望远镜一样。”（"Computer
science is no more about computers than astronomy is about
telescopes."）设计、部署计算机和计算机系统通常被认为是非计算机科学学科的领域。例如，研究[计算机硬件被看作是](../Page/个人电脑硬件.md "wikilink")[计算机工程的一部分](../Page/计算机工程.md "wikilink")，而对于商业[计算机系统的研究和部署被称为](../Page/电子计算机.md "wikilink")[信息技术或者](../Page/信息技术.md "wikilink")[信息系统](../Page/信息系统.md "wikilink")。然而，现如今也越来越多地融合了各类计算机相关学科的思想。计算机科学研究也经常与其它学科交叉，比如[心理学](../Page/心理学.md "wikilink")，[认知科学](../Page/认知科学.md "wikilink")，[语言学](../Page/语言学.md "wikilink")，[数学](../Page/数学.md "wikilink")，[物理学](../Page/物理学.md "wikilink")，[统计学和](../Page/计算统计学.md "wikilink")[经济学](../Page/计算经济学.md "wikilink")。

计算机科学被认为比其它科学学科与数学的联系更加密切，一些观察者说计算就是一门数学科学。\[6\]
早期计算机科学受数学研究成果的影响很大，如[Kurt
Gödel和](../Page/库尔特·哥德尔.md "wikilink")[Alan
Turing](../Page/艾伦·图灵.md "wikilink")，这两个领域在某些学科，例如[数理逻辑](../Page/数理逻辑.md "wikilink")、[范畴论](../Page/范畴论.md "wikilink")、[域理论和](../Page/域理论.md "wikilink")[代数](../Page/代数.md "wikilink")，也不断有有益的思想交流。

## 历史

早期计算机科学建立的基础得追溯到最近[电子计算机的发明](../Page/电子计算机.md "wikilink")。那些计算固定数值任务的机器，比如[算盘](../Page/算盘.md "wikilink")，自古希腊时期即已存在。[Wilhelm
Schickard在](../Page/Wilhelm_Schickard.md "wikilink")1623年设计了世界上第一台机械计算器，但没有完成它的建造。\[7\][布莱兹·帕斯卡在](../Page/布莱兹·帕斯卡.md "wikilink")1642年设计并且建造了世界上第一台可以工作的机械计算器[Pascaline](../Page/Pascal's_calculator.md "wikilink")。[埃达·洛夫莱斯协助](../Page/埃达·洛夫莱斯.md "wikilink")\[8\][查尔斯·巴贝奇在](../Page/查尔斯·巴贝奇.md "wikilink")[维多利亚时代设计了](../Page/维多利亚时代.md "wikilink")[差分机](../Page/差分机.md "wikilink")。\[9\]1900年左右，[打孔机](../Page/打孔机.md "wikilink")\[10\]问世。然而以上这些机器都局限在只能完成单个任务，或者充其量是所有可能任务的子集。

到了20世纪40年代，随着更新更强大的计算机器被发明，术语“计算机”开始用于指代那些机器而不是它们的祖先。\[11\]计算机的概念变得更加清晰，它不仅仅用于数学运算，总的来说计算机科学的领域也扩展到了对于[计算的研究](../Page/计算.md "wikilink")。20世纪50年代至20世纪60年代早期，计算机科学开始被确立为不同种类的学术学科。\[12\]
世界上第一个计算机科学学位点由[普渡大学在](../Page/普渡大学.md "wikilink")1962年设立。\[13\]随着实用计算机的出现，很多计算的应用都以它们自己的方式逐渐转变成了研究的不同领域。

虽然最初很多人并不相信计算机可能成为科学研究的领域，但是随后的50年里也逐渐被学术界认可。\[14\][IBM公司是那段时期计算机科学革命的参与者之一](../Page/IBM.md "wikilink")。在那段探索时期，IBM（International
Business Machines的缩写）发布的IBM 704以及之后的IBM
709计算机被广泛使用。“不过，使用IBM电脑工作仍然是一件很沮丧的事情。如果你弄错了一条指令中的一个字母，程序将会崩溃，而你也得从头再来。”\[15\]20世纪50年代后期，计算机科学学科還在发展階段，这種問題在当时是一件很常見的事情。

随着时间的推移，计算机科学技术在可用性和有效性上都有显著提升。现代社会见证了计算机从仅仅由专业人士使用到被广大用户接受的重大转变。最初，计算机非常昂贵，要有效利用它们，某种程度上必须得由专业的计算机操作员来完成。然而，随着计算机变得普及和低廉，已经几乎不需要专人的协助，虽然某些时候援助依旧存在。

### 主要成就

[Enigma.jpg](https://zh.wikipedia.org/wiki/File:Enigma.jpg "fig:Enigma.jpg")军在[二战时用于加密通信的](../Page/第二次世界大战.md "wikilink")[恩尼格玛密码机](../Page/恩尼格玛密码机.md "wikilink")。恩尼格玛加密信息在[布莱切利园被大量破译被认为是帮助盟军在二战中获胜的重要因素](../Page/布莱切利园.md "wikilink")。\[16\]\]\]

虽然计算机科学被认定为正式学术学科的历史很短暂，但仍对[科学和](../Page/科学.md "wikilink")[社会作出了很多基础贡献](../Page/社会.md "wikilink")。包括：

  - “数位革命”的开端：[信息时代与](../Page/信息时代.md "wikilink")[互联网](../Page/互联网.md "wikilink")。\[17\]
  - 对于[计算和](../Page/计算.md "wikilink")[可计算理论的正式定义](../Page/可计算理论.md "wikilink")，证明了存在计算上及[难解型问题](../Page/难解型问题.md "wikilink")。\[18\]
  - 提出[程式语言的概念](../Page/程式语言.md "wikilink")，作為一种使用不同的抽象層次來精确表达處理程序的工具。\[19\]
  - 在[密码学领域](../Page/密码学.md "wikilink")，[恩尼格玛密码机的破译被视为盟军在二战取得胜利的重要因素](../Page/恩尼格玛密码机.md "wikilink")。\[20\]
  - [科学计算实现了高复杂度处理的实用价值](../Page/计算科学.md "wikilink")，以及完全使用软件进行实验。同时也实现了对人类思想的深入研究，使得[人类基因组计划绘制人类基因成为可能](../Page/人类基因组计划.md "wikilink")。\[21\]还有探索[蛋白质折叠的](../Page/蛋白质折叠.md "wikilink")[分布式计算项目](../Page/分布式计算.md "wikilink")[Folding@home](../Page/Folding@home.md "wikilink")。
  - [算法交易增长了金融市场的](../Page/算法交易.md "wikilink")[经济效益与](../Page/经济效益.md "wikilink")[市场流通性](../Page/市场流通性.md "wikilink")，通过[人工智能](../Page/人工智能.md "wikilink")，[机器学习及大规模的](../Page/机器学习.md "wikilink")[统计和](../Page/统计学.md "wikilink")[数值技术](../Page/数值分析.md "wikilink")。\[22\]
  - [電腦成像廣泛用於](../Page/電腦成像.md "wikilink")[娛樂](../Page/娛樂.md "wikilink")，包括電視、電影、廣告、[動畫](../Page/動畫.md "wikilink")、[電子遊戲等](../Page/電子遊戲.md "wikilink")。
  - [自然语言处理](../Page/自然语言处理.md "wikilink")，包括语音到文字（speech-to-text）转换、语言间的自动翻译
  - 对各种过程的[模拟](../Page/模拟.md "wikilink")，包括[计算流体力学](../Page/计算流体力学.md "wikilink")、物理、电气、电子系统和电路，以及同人类居住地联系在一起的社会和社会形态（尤其是战争游戏，war
    games）。现代计算机能够对这些设计进行优化，如飞机设计。尤其在电气与电子电路设计中，[SPICE软件对新的物理实现](../Page/SPICE.md "wikilink")（或修改）设计具有很大帮助。它包含了针对集成电路的基本设计软件。

## 哲学

提出计算机科学可以分成三个领域：数学、工程学、科學。Amnon H.
Eden提议了三种[范式应用于计算机科学的各个领域](../Page/范式.md "wikilink")：\[23\]

  - “理性主义范式”，将计算机科学看作是数学的分支，在理论计算机科学中很流行，主要利用[演绎推理](../Page/演绎推理.md "wikilink")。
  - “技术专家范式”，这类范式有着很明显的[工程学倾向](../Page/工程学.md "wikilink")，尤其是在软件工程领域。
  - “科学范式”，[人工智能的某些分支可以作为这类范式的代表](../Page/人工智能.md "wikilink")（比如说对于[人工生命的研究](../Page/人工生命.md "wikilink")）。

## 计算机科学的领域

作为一个学科，计算机科学涵盖了从算法的理论研究和计算的极限，到如何通过硬件和软件实现计算系统。\[24\]\[25\]
[CSAB](../Page/CSAB_\(专业机构\).md "wikilink")（以前被叫做*Computing Sciences
Accreditation Board*），由[Association for Computing
Machinery](../Page/计算机协会.md "wikilink")（ACM）和（IEEE-CS）的代表组成\[26\]，确立了计算机科学学科的4个主要领域：*计算理论*，*算法与数据结构*，*编程方法与编程语言*，以及*计算机组成与架构*。CSAB还确立了其它一些重要领域，如软件工程，人工智能，计算机网络与通信，数据库系统，并行计算，分布式计算，人机交互，计算机图形学，操作系统，以及数值和符号计算。

### 理論電腦科学

广义的[理論電腦科學包括经典的计算理论和其它專注于更抽象](../Page/理論電腦科學.md "wikilink")、逻辑与数学方面的计算。

#### 数据结构和算法

|                                    |                                                                                                                                |                                                                                                                    |                                                                                                                           |                                                                                 |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------- |
| \(O(n^2)\)                         | [Sorting_quicksort_anim.gif](https://zh.wikipedia.org/wiki/File:Sorting_quicksort_anim.gif "fig:Sorting_quicksort_anim.gif") | [Singly_linked_list.png](https://zh.wikipedia.org/wiki/File:Singly_linked_list.png "fig:Singly_linked_list.png") | [SimplexRangeSearching.png](https://zh.wikipedia.org/wiki/File:SimplexRangeSearching.png "fig:SimplexRangeSearching.png") | [6n-graf.svg](https://zh.wikipedia.org/wiki/File:6n-graf.svg "fig:6n-graf.svg") |
| [算法分析](../Page/算法分析.md "wikilink") | [算法](../Page/算法.md "wikilink")                                                                                                 | [数据结构](../Page/数据结构.md "wikilink")                                                                                 | [计算几何](../Page/计算几何.md "wikilink")                                                                                        | [图论](../Page/图论.md "wikilink")                                                  |

算法指定义良好的计算过程，它取一个或一组值作为输入，经过一系列定义好的计算过程，得到一个或一组输出。\[27\]算法是计算机科学研究的一个重要领域，也是许多其他计算机科学技术的基础。算法主要包括[数据结构](../Page/数据结构.md "wikilink")、[计算几何](../Page/计算几何.md "wikilink")、[图论等](../Page/图论.md "wikilink")。除此之外，算法还包括许多杂项，如[模式匹配](../Page/模式匹配.md "wikilink")、部分[数论等](../Page/数论.md "wikilink")。

#### 计算理论

按照[Peter J.
Denning的说法](../Page/Peter_J._Denning.md "wikilink")，计算机科学的最根本问题是“什么能够被有效地自动化？”\[28\][计算理论的研究就是專注于回答这个根本问题](../Page/计算理论.md "wikilink")，关于什么能够被计算，去实施这些计算又需要用到多少资源。为了试图回答第一个问题，[递归论检验在多种理论](../Page/递归论.md "wikilink")[计算模型中哪个计算问题是可解的](../Page/计算模型.md "wikilink")。而[计算复杂性理论则被用于回答第二个问题](../Page/计算复杂性理论.md "wikilink")，研究解决一个不同目的的计算问题的时间与空间消耗。

著名的“[P=NP?](../Page/P/NP问题.md "wikilink")”问题，[千禧年大奖难题之一](../Page/千禧年大奖难题.md "wikilink")，\[29\]是计算理论的一个。

|                                                                                          |                                                                                           |                                          |                                  |                                                                                             |
| ---------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | ---------------------------------------- | -------------------------------- | ------------------------------------------------------------------------------------------- |
| [DFAexample.svg](https://zh.wikipedia.org/wiki/File:DFAexample.svg "fig:DFAexample.svg") | [Wang_tiles.png](https://zh.wikipedia.org/wiki/File:Wang_tiles.png "fig:Wang_tiles.png") | **P = NP** ?                             | **GNITIRW-TERCES**               | [Blochsphere.svg](https://zh.wikipedia.org/wiki/File:Blochsphere.svg "fig:Blochsphere.svg") |
| [自动机理论](../Page/自动机理论.md "wikilink")                                                     | [递归论](../Page/递归论.md "wikilink")                                                          | [计算复杂性理论](../Page/计算复杂性理论.md "wikilink") | [密码学](../Page/密码学.md "wikilink") | [量子计算论](../Page/量子计算机.md "wikilink")                                                        |

#### 信息论与编码理论

信息论与信息量化相关，由[克劳德·香农创建](../Page/克劳德·香农.md "wikilink")，用于寻找信号处理操作的根本极限，比如压缩数据和可靠的数据存储与通讯。编码理论是对编码以及它们适用的特定应用性质的研究。编码（code）被用于数据压缩，密码学，前向纠错，近期也被用于网络编码。研究编码的目的在于设计更高效、可靠的数据传输方法。

#### 编程语言和编译器

编程语言理论是计算机科学的一个分支，主要处理[编程语言的设计](../Page/编程语言.md "wikilink")、实现、分析、描述和分类，以及它们的个体特性。它属于计算机科学学科，既受影响于也影响着[数学](../Page/数学.md "wikilink")、[软件工程和](../Page/软件工程.md "wikilink")[语言学](../Page/语言学.md "wikilink")。它是公认的计算机科学分支，同时也是活跃的研究领域，研究成果被发表在众多[学术期刊](../Page/学术期刊.md "wikilink")，计算机科学以及工程出版物。

|                                  |                                                                                                       |                                                                                                                    |
| -------------------------------- | ----------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------ |
| \(\Gamma\vdash x: \text{Int}\)   | [Ideal_compiler.png](https://zh.wikipedia.org/wiki/File:Ideal_compiler.png "fig:Ideal_compiler.png") | [Python_add5_syntax.svg](https://zh.wikipedia.org/wiki/File:Python_add5_syntax.svg "fig:Python_add5_syntax.svg") |
| [类型论](../Page/类型论.md "wikilink") | [编译器设计](../Page/编译器设计.md "wikilink")                                                                  | [程序设计语言](../Page/程序设计语言.md "wikilink")                                                                             |

#### 形式化方法

形式化方法是一种特别的基于[数学的技术](../Page/数学.md "wikilink")，用于[软件和](../Page/软件.md "wikilink")[硬件系统的](../Page/硬件.md "wikilink")[形式规范](../Page/规范.md "wikilink")、开发以及[驗證](../Page/形式驗證.md "wikilink")。在软件和硬件设计方面，形式化方法的使用动机，如同其它工程学科，是通过适当的数学分析便有助于设计的可靠性和健壮性的期望。但是，使用形式化方法会带来很高的成本，意味着它们通常只用于高可靠性系统，这种系统中[安全或](../Page/安全.md "wikilink")[保安](../Page/保安.md "wikilink")（security）是最重要的。对于形式化方法的最佳形容是各种[理论计算机科学基础种类的应用](../Page/理论计算机科学.md "wikilink")，特别是[计算机逻辑演算](../Page/逻辑.md "wikilink")，[形式语言](../Page/形式语言.md "wikilink")，[自动机理论和](../Page/自动机.md "wikilink")[形式语义学](../Page/形式语义学.md "wikilink")，此外还有[类型系统](../Page/类型系统.md "wikilink")、[代数数据类型](../Page/代数数据类型.md "wikilink")，以及软件和硬件规范和验证中的一些问题。

### 计算机系统

#### 计算机体系结构与计算机工程

计算机系统结构，或者数字计算机组织，是一个计算机系统的概念设计和根本运作结构。它主要侧重于CPU的内部执行和内存访问地址。这个领域经常涉及计算机工程和电子工程学科，选择和互连硬件组件以创造满足功能、性能和成本目标的计算机。

|                                                                                     |                                                                                                                  |                                                                        |
| ----------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| [NOR_ANSI.svg](https://zh.wikipedia.org/wiki/File:NOR_ANSI.svg "fig:NOR_ANSI.svg") | [Fivestagespipeline.png](https://zh.wikipedia.org/wiki/File:Fivestagespipeline.png "fig:Fivestagespipeline.png") | [SIMD.svg](https://zh.wikipedia.org/wiki/File:SIMD.svg "fig:SIMD.svg") |
| [数字电路](../Page/数字电路.md "wikilink")                                                  | [微架构](../Page/微架构.md "wikilink")                                                                                 | [多处理机](../Page/多处理机.md "wikilink")                                     |

#### 操作系统

操作系统是管理电脑硬体与软体资源的电脑程式，同时也是电脑系统的核心与基石。作业系统需要处理如管理与配置记忆体、决定系统资源供需的优先次序、控制输入与输出装置、操作网路与管理档案系统等基本事务。作业系统也提供一个让使用者与各电脑设备互动的操作介面。

#### 并发、并行与分布式系统

并发性（concurrency）是系统的一种性质，这类系统可以同时执行多个可能互相交互的计算。一些数学模型，如[Petri网](../Page/Petri网.md "wikilink")、[进程演算和](../Page/进程演算.md "wikilink")[PRAM模型](../Page/PRAM模型.md "wikilink")，被建立以用于通用并发计算。分布式系统将并发性的思想扩展到了多台由网络连接的计算机。同一分布式系统中的计算机拥有自己的私有内存，它们之间经常交换信息以达到一个共同的目的。

#### 计算机网络

计算机网络是管理遍及全球的计算机连接成的网络的计算机科学分支。

#### 计算机安全和密码学

计算机安全是计算机技术的一个分支，其目标包括保护信息免受未经授权的访问、中断和修改，同时为系统的预期用户保持系统的可访问性和可用性。密码学是对于隐藏（加密）和破译（解密）信息的实践与研究。现代密码学主要跟计算机科学相关，很多加密和解密算法都是基于它们的计算复杂性。

#### 数据库

数据库是为了更容易地组织、存储和检索大量数据。数据库由数据库管理系统管理，通过[数据模型和](../Page/数据模型.md "wikilink")[查询语言来存储](../Page/查询语言.md "wikilink")、创建、维护和搜索数据。

### 计算机应用技术

#### 计算机图形学

计算机图形学是对于数字视觉内容的研究，涉及图像数据的合成和操作。它跟计算机科学的许多其它领域密切相关，包括[计算机视觉](../Page/计算机视觉.md "wikilink")、[图像处理](../Page/图像处理.md "wikilink")、[计算几何与](../Page/计算几何.md "wikilink")[可视化](../Page/可视化.md "wikilink")，同时也被大量运用在[特效和](../Page/特效.md "wikilink")[电子游戏](../Page/电子游戏.md "wikilink")。

#### 科学计算

[科学计算](../Page/科学计算.md "wikilink")（或者[计算科学](../Page/计算科学.md "wikilink")）是关注构建[数学模型和](../Page/科学建模.md "wikilink")[量化分析技术的研究领域](../Page/数值分析.md "wikilink")，同时通过计算机分析和解决[科学问题](../Page/科学.md "wikilink")。在实际使用中，它通常是[计算机模拟和](../Page/计算机模拟.md "wikilink")[计算等形式在各个科学学科问题中的应用](../Page/计算.md "wikilink")。

|                                                                                                                       |                                                                                           |                                                                                                                        |                                                                                                      |
| --------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- |
| [Lorenz_attractor_yb.svg](https://zh.wikipedia.org/wiki/File:Lorenz_attractor_yb.svg "fig:Lorenz_attractor_yb.svg") | [Quark_wiki.jpg](https://zh.wikipedia.org/wiki/File:Quark_wiki.jpg "fig:Quark_wiki.jpg") | [Naphthalene-3D-balls.png](https://zh.wikipedia.org/wiki/File:Naphthalene-3D-balls.png "fig:Naphthalene-3D-balls.png") | [1u04-argonaute.png](https://zh.wikipedia.org/wiki/File:1u04-argonaute.png "fig:1u04-argonaute.png") |
| [数值分析](../Page/数值分析.md "wikilink")                                                                                    | [计算物理学](../Page/计算物理学.md "wikilink")                                                      | [计算化学](../Page/计算化学.md "wikilink")                                                                                     | [生物信息学](../Page/生物信息学.md "wikilink")                                                                 |

#### 多媒体技术

#### 数据挖掘

[資料探勘也就是將人類在過去的歷史當中所收集的資料](../Page/資料探勘.md "wikilink")，加以匯集起來，再將這些資料結合機器學習，來達到分析、管理、前瞻等能力。

#### 人工智能

这个计算机科学分支旨在创造可以解决计算问题，以及像动物和人类一样思考与交流的人造系统。无论是在理论还是应用上，都要求研究者在多个学科领域具备细致的、综合的专长，比如[应用数学](../Page/应用数学.md "wikilink")，[逻辑](../Page/逻辑.md "wikilink")，[符号学](../Page/符号学.md "wikilink")，[电机工程学](../Page/电机工程学.md "wikilink")，[精神哲学](../Page/精神哲学.md "wikilink")，[神经生理学和](../Page/神经生理学.md "wikilink")[社会智力](../Page/社会智力.md "wikilink")，用于推动智能研究领域，或者被应用到其它需要计算理解与建模的学科领域，如[金融或是](../Page/金融.md "wikilink")[物理科学](../Page/物理科学.md "wikilink")。人工智能领域开始变得正式源于[Alan
Turing这位](../Page/艾伦·图灵.md "wikilink")[人工智能先驱提出了](../Page/人工智能.md "wikilink")[图灵试验](../Page/图灵试验.md "wikilink")，以回答这样一个终极问题：“计算机能够思考吗？”

##### 自动推理

##### 机器学习

[机器学习是人工智慧的其中一个分支](../Page/机器学习.md "wikilink")，让机器可以自动学习、从巨量资料中找到规则，进而有能力做出预测。人工智慧让过去只能透过人类或动物智慧解决的问题也能透过电脑系统迎刃而解；机器人是自动执行工作的机器装置，而人工智慧则可以让机器人快速、精准处理大量资料。简单来说，机器人像是人的「身躯」，人工智慧则是人的「脑」。\[30\]

##### 计算机视觉

##### 自然语言处理

### 软件工程

软件工程是对于设计、实现和修改软件的研究，以确保软件的高质量、适中的价格、可维护性，以及能够快速构建。它是一个系统的软件设计方法，涉及工程实践到软件的应用。

计算机科学和[软件工程的关系是一个有争议的话题](../Page/软件工程.md "wikilink")，随后关于什么是“软件工程”，计算机科学又该如何定义的争论使得情况更加混乱。[David
Parnas从其它工程和科学学科之间的关系得到启示](../Page/David_Parnas.md "wikilink")，宣称计算机科学的主要重点总的来说是研究计算的性质，而软件工程的主要重点是具体的计算设计，以达到实用的目的，这样便构成了两个独立但又互补的学科。\[31\]

## 学术界

### 会议

### 期刊

## 作为高等教育的二级专业

[缩略图](https://zh.wikipedia.org/wiki/File:Bangalore_India_Tech_books_for_sale_\(kid_got_in_the_shot\)_IMG_5255.jpg "fig:缩略图")
主要进行计算和算法推理的研究。其中包括[计算理论](../Page/计算理论.md "wikilink")、[算法分析](../Page/算法分析.md "wikilink")、[形式化方法](../Page/形式化方法.md "wikilink")、[并行理论](../Page/并行性.md "wikilink")、[数据库](../Page/数据库.md "wikilink")、[计算机图形学以及](../Page/计算机图形学.md "wikilink")[系统分析等](../Page/系统分析.md "wikilink")。通常也教授[程序设计](../Page/程序设计.md "wikilink")，但仅仅将它看作是支持计算机科学其它领域的媒介，而不是高级研究的重心。

的计算机科学课程则主要侧重于训练高级编程，而不是算法和计算理论。这些课程着重教授那些对于软件工业很重要的技能。像这样的计算机编程过程通常被叫做[软件工程](../Page/软件工程.md "wikilink")。

然而，尽管计算机科学专业日益推动着美国经济，但是计算机科学教育依然不存在大多数美国K-12课程中。2010年10月由[ACM](http://www.acm.org/)和[计算机科学教师协会（CSTA）](http://csta.acm.org/)共同发表了一篇名为[“Running
on Empty: The Failure to Teach K-12 Computer Science in the Digital
Age”](https://web.archive.org/web/20110702060623/http://www.acm.org/Runningonempty/)的报告，文中揭示了仅有14个州通过了有意义的高中计算机科学教育标准。同时，仅有9个州将高中计算机科学课程算作毕业要求的核心学科。配合“Running
on Empty”这篇文章，一个新的无党派宣传联盟：[Computing in the
Core（CinC）](http://www.computinginthecore.org/)被建立，以影响联邦和政府政策，比如[Computer
Science Education
Act](http://www.govtrack.us/congress/bill.xpd?bill=h111-5929&tab=summary)要求政府拨款以制定计划完善计算机科学教育及支持计算机科学教师。

在中国，“计算机科学”或“计算机科学与技术”是工科（一级门类）下的二级专业。一般可细分为三级专业：

  - [计算机软件](../Page/计算机软件.md "wikilink")：围绕着软件开发、[软件工程](../Page/软件工程.md "wikilink")、[编程语言等](../Page/编程语言.md "wikilink")
  - [计算机应用](../Page/计算机应用.md "wikilink")：计算机的各种应用[算法](../Page/算法.md "wikilink")、技术。如：[模式识别](../Page/模式识别.md "wikilink")、[计算视觉](../Page/计算机视觉.md "wikilink")、[计算机图形学等等](../Page/计算机图形学.md "wikilink")。
  - [计算机体系结构](../Page/计算机体系结构.md "wikilink")：关于计算机或计算机信息系统的“硬件”。

## 工业界

## 参考文献

## 延伸阅读

  - 概述

<!-- end list -->

  -   - "Within more than 70 chapters, every one new or significantly
        revised, one can find any kind of information and references
        about computer science one can imagine. \[...\] all in all,
        there is absolute nothing about Computer Science that can not be
        found in the 2.5 kilogram-encyclopaedia with its 110 survey
        articles \[...\]." (Christoph Meinel, *[Zentralblatt
        MATH](../Page/Zentralblatt_MATH.md "wikilink")*)

  -   - "\[...\] this set is the most unique and possibly the most
        useful to the \[theoretical computer science\] community, in
        support both of teaching and research \[...\]. The books can be
        used by anyone wanting simply to gain an understanding of one of
        these areas, or by someone desiring to be in research in a
        topic, or by instructors wishing to find timely information on a
        subject they are teaching outside their major areas of
        expertise." (Rocky Ross, *[SIGACT
        News](../Page/SIGACT_News.md "wikilink")*)

  -   - "Since 1976, this has been the definitive reference work on
        computer, computing, and computer science. \[...\]
        Alphabetically arranged and classified into broad subject areas,
        the entries cover hardware, computer systems, information and
        data, software, the mathematics of computing, theory of
        computation, methodologies, applications, and computing milieu.
        The editors have done a commendable job of blending historical
        perspective and practical reference information. The
        encyclopedia remains essential for most public and academic
        library reference collections." (Joe Accardin, Northeastern
        Illinois Univ., Chicago)

<!-- end list -->

  - 论文选集

<!-- end list -->

  -   - "Covering a period from 1966 to 1993, its interest lies not only
        in the content of each of these papers — still timely today —
        but also in their being put together so that ideas expressed at
        different times complement each other nicely." (N. Bernard,
        *Zentralblatt MATH*)

<!-- end list -->

  - 文章

<!-- end list -->

  - Peter J. Denning. *[Is computer science
    science?](http://portal.acm.org/citation.cfm?id=1053309&coll=&dl=ACM&CFID=15151515&CFTOKEN=6184618)*,
    Communications of the ACM, April 2005.
  - Peter J. Denning, *[Great principles in computing
    curricula](http://portal.acm.org/citation.cfm?id=971303&dl=ACM&coll=&CFID=15151515&CFTOKEN=6184618)*,
    Technical Symposium on Computer Science Education, 2004.
  - Research evaluation for computer science, Informatics Europe
    [report](https://web.archive.org/web/20110720212845/http://www.informatics-europe.org/docs/research_evaluation.pdf).
    Shorter journal version: Bertrand Meyer, Christine Choppy, Jan van
    Leeuwen and Jorgen Staunstrup, *Research evaluation for computer
    science*, in [Communications of the
    ACM](../Page/Communications_of_the_ACM.md "wikilink"), vol. 52, no.
    4, pp. 31-34, April 2009.

<!-- end list -->

  - 课程与分类

<!-- end list -->

  - [Association for Computing
    Machinery](../Page/Association_for_Computing_Machinery.md "wikilink").
    [1998 ACM Computing Classification
    System](https://web.archive.org/web/20080828002940/http://www.acm.org/class/1998/overview.html).
    1998.
  - Joint Task Force of Association for Computing Machinery (ACM),
    [Association for Information
    Systems](../Page/Association_for_Information_Systems.md "wikilink")
    (AIS) and [IEEE Computer
    Society](../Page/IEEE_Computer_Society.md "wikilink") (IEEE-CS).
    [Computing Curricula 2005: The Overview
    Report](https://web.archive.org/web/20141021153204/http://www.acm.org/education/curric_vols/CC2005-March06Final.pdf).
    September 30, 2005.
  - [Norman Gibbs](../Page/Norman_Gibbs.md "wikilink"), [Allen
    Tucker](../Page/Allen_Tucker.md "wikilink"). "A model curriculum for
    a liberal arts degree in computer science". *Communications of the
    ACM*, Volume 29 Issue 3, March 1986.

## 外部链接

  -
  - [Scholarly Societies in Computer
    Science](http://www.lib.uwaterloo.ca/society/compsci_soc.html)

  - [Best Papers Awards in Computer Science
    since 1996](http://jeffhuang.com/best_paper_awards.html)

  - [Photographs of computer
    scientists](http://se.ethz.ch/~meyer/gallery/) by [Bertrand
    Meyer](../Page/Bertrand_Meyer.md "wikilink")

<!-- end list -->

  - 参考文献和学术搜索引擎

<!-- end list -->

  - [CiteSeer<sup>*x*</sup>](http://citeseerx.ist.psu.edu/): search
    engine, digital library and repository for scientific and academic
    papers with a focus on computer and information science.
  - [DBLP Computer Science Bibliography](http://dblp.uni-trier.de/):
    computer science bibliography website hosted at Universität Trier,
    in Germany.
  - [The Collection of Computer Science
    Bibliographies](http://liinwww.ira.uka.de/bibliography/)

<!-- end list -->

  - Webcast

<!-- end list -->

  - [Directory of free university lectures in Computer
    Science](https://web.archive.org/web/20080512152252/http://lecturefox.com/computerscience/)
  - [Collection of computer science
    lectures](http://videolectures.net/Top/Computer_Science/)
  - [UCLA Computer Science 1 Freshman Computer Science Seminar
    Section 1](https://web.archive.org/web/20110317053007/http://www.oid.ucla.edu/webcasts/courses/2006-2007/2006fall/cs1)
  - [Berkeley Introduction to
    Computers](http://webcast.berkeley.edu/course_details.php?seriesid=1906978395)

## 参见

  - [未解決的計算機科學問題](../Page/未解決的計算機科學問題.md "wikilink")
  - [计算机科学家的学术家谱](../Page/计算机科学家的学术家谱.md "wikilink")
  - [计算机科学家](../Page/计算机科学家.md "wikilink")
  - [计算](../Page/计算.md "wikilink")
  - [计算机科学的历史](../Page/计算机科学的历史.md "wikilink")
  - [信息学](../Page/信息学_\(学术领域\).md "wikilink")
  - [学术计算机科学系列表](../Page/学术计算机科学系列表.md "wikilink")
  - [计算机科学会议列表](../Page/计算机科学会议列表.md "wikilink")
  - [计算机科学家列表](../Page/计算机科学家列表.md "wikilink")
  - [计算机科学开放问题列表](../Page/计算机科学中尚未解决的问题.md "wikilink")
  - [计算机科学出版物列表](../Page/计算机科学重要出版物列表.md "wikilink")
  - [计算机科学先驱列表](../Page/计算机科学先驱列表.md "wikilink")
  - [软件工程主题列表](../Page/软件工程主题列表.md "wikilink")
  - [计算机科学的哲学](../Page/计算机科学的哲学.md "wikilink")
  - [计算机领域中的女性](../Page/计算机领域中的女性.md "wikilink")
  - [计算机](../Page/计算机.md "wikilink")
  - [计算尺](../Page/计算尺.md "wikilink")
  - [算盤](../Page/算盤.md "wikilink")
  - [語言](../Page/語言.md "wikilink")
  - [科学](../Page/科学.md "wikilink")
  - [科学家](../Page/科学家.md "wikilink")
  - [算籌](../Page/算籌.md "wikilink")
  - [科學會議](../Page/科學會議.md "wikilink")
  - [計算機遊戲](../Page/計算機遊戲.md "wikilink")
  - [按揭計算機](../Page/按揭計算機.md "wikilink")
  - [計算機程式](../Page/計算機程式.md "wikilink")
  - [CASIO 計算機](../Page/CASIO_計算機.md "wikilink")
  - [計算機工程](../Page/計算機工程.md "wikilink")
  - [桌上型計算機](../Page/桌上型計算機.md "wikilink")

{{-}}

[计算机科学](../Category/计算机科学.md "wikilink")

1.

2.

3.  "*Common myths and preconceptions about Cambridge Computer Science*"
    [Computer Science
    Department](http://www.cl.cam.ac.uk/admissions/undergraduate/myths/)
    , [University of
    Cambridge](../Page/University_of_Cambridge.md "wikilink")

4.  Communications of the ACM 1(4):p.6

5.  Communications of the ACM 2(1):p.4

6.
7.

8.

9.

10.

11. The [Association for Computing
    Machinery](../Page/Association_for_Computing_Machinery.md "wikilink")
    (ACM) was founded in 1947.

12. [CAM.ac.uk](http://www.cl.cam.ac.uk/conference/EDSAC99/statistics.html)

13. [Computer science pioneer Samuel D. Conte dies
    at 85](http://www.cs.purdue.edu/feature/conte.html)  July 1, 2002

14.

15.
16.
17. [Cornell.edu](http://www.cis.cornell.edu/Dean/Presentations/Slides/bgu.pdf)


18.

19.

20. [David Kahn](../Page/David_Kahn_\(writer\).md "wikilink"), [The
    Codebreakers](../Page/The_Codebreakers.md "wikilink"), 1967, ISBN
    978-0-684-83130-5.

21.
22. [Black box traders are on the
    march](http://www.telegraph.co.uk/money/main.jhtml?xml=/money/2006/08/27/ccsoft27.xml)
    The Telegraph, August 26, 2006

23.

24.

25.

26. [CSAB, Inc.](http://www.csab.org/)

27. 《算法导论》1.1［美］Thomas H. Cormen, Charles E. Leiserson, Ronald L.
    Rivest, Clifford Stein著, 潘金贵、顾铁成、李成法、叶懋译，机械工业出版社 2011年7月第1版

28.

29. [Clay Mathematics
    Institute](http://www.claymath.org/millennium/P_vs_NP/)  P=NP

30.

31.