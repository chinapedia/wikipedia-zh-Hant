**《认定》**（）是[澳大利亚音乐组合](../Page/澳大利亚.md "wikilink")[野人花园的第二张专辑](../Page/野人花园.md "wikilink")，于1999年11月9日发行。专辑中所有的歌曲都是由[戴伦·海斯和](../Page/戴伦·海斯.md "wikilink")[丹尼尔·琼斯创作的](../Page/丹尼尔·琼斯.md "wikilink")。

## 曲目

1.  "Affirmation"
2.  "Hold Me"
3.  "I Knew I Loved You"
4.  "Best Thing"
5.  "Crash and Burn"
6.  "Chained to You"
7.  "Animal Song"
8.  "Lover After Me"
9.  "Two Beds and a Coffee Machine"
10. "You Can Still Be Free"
11. "Gunning Down Romance"
12. "I Don't Know You Anymore"

[A](../Page/category:野人花园音乐专辑.md "wikilink")
[A](../Page/category:1999年音樂專輯.md "wikilink")

[A](../Category/澳洲音樂專輯.md "wikilink")