**让-保罗·贝尔蒙多**（，），出生於[法國](../Page/法國.md "wikilink")[塞纳河畔纳伊](../Page/塞纳河畔纳伊.md "wikilink")，法國[電影演員](../Page/電影演員.md "wikilink")，[動作片演員](../Page/動作片演員.md "wikilink")；父親是法國著名雕塑家[保羅·貝蒙](../Page/保羅·貝蒙.md "wikilink")，早年多拍攝[文藝片](../Page/文藝片.md "wikilink")，於1960年擔綱演出[法國新浪潮名導](../Page/法國新浪潮.md "wikilink")[尚盧·高達名作](../Page/尚盧·高達.md "wikilink")[斷了氣聲名大噪](../Page/斷了氣.md "wikilink")。

## 出演的著名影视剧

  - 1960年 斷了氣 (法语：À bout de souffle)
  - 1962年 一只猴子在冬季（法语：Un singe en hiver）
  - 1962年 Cartouche
  - 1965年 《[狂人皮埃罗](../Page/狂人皮埃罗.md "wikilink")》
  - 1968年 Ho \!
  - 1969年 大脑（法语：Le Cerveau）
  - 1970年 Borsalino
  - 1972年 医生 "Popaul"（法语：Docteur Popaul）
  - 1973年 继承人（法语：L'Héritier）
  - 1974年 Stavisky
  - 1975年 恐惧于城市（法语：Peur sur la ville）
  - 1981年 专业（法语：Le Professionnel）
  - 1983年 边际（法语：Le Marginal）
  - 1988年 行程一个被惯坏的孩子（法语：Itinéraire d'un enfant gâté）
  - 1995年 可怜人（法语：Les Misérables）
  - 2001年 Ferchaux 长子（法语：L'Aîné des Ferchaux）
  - 2009年 老男人和狗（法语：Un homme et son chien）

## 獎項

  - 兩度入圍[英國電影學院獎最佳外籍演員](../Page/英國電影學院獎.md "wikilink")\[1\]
  - 1989年榮膺法國[凱撒獎最佳男主角](../Page/凱撒獎.md "wikilink")\[2\]
  - 2016年获颁荣誉[金狮奖](../Page/金狮奖.md "wikilink")

## 腳註

[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:法国电影演员](../Category/法国电影演员.md "wikilink")
[Category:電影監製](../Category/電影監製.md "wikilink")
[Category:凱撒電影獎獲得者](../Category/凱撒電影獎獲得者.md "wikilink")

1.

2.