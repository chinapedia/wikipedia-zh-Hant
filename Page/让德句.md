**让德句**（，）是从1940年到1945年间代表[维希法国政府的](../Page/维希法国.md "wikilink")[法属印度支那總督](../Page/法属印度支那.md "wikilink")。

1884年，让德句出生在法国港口城市[波尔多](../Page/波尔多.md "wikilink")。

## 經歷

1940年6月，法国本土被德国占领。[维希政府派遣让德句担任法属印度支那總督](../Page/维希.md "wikilink")。让德句在印度支那的任务是
推翻前任[Georges
Catroux将军](../Page/乔治斯·卡特鲁.md "wikilink")（1877-1969）的亲[日政策](../Page/日本.md "wikilink")，但是政治现实很快就迫使他也走上同一条道路。战后让德句遭到逮捕和审讯，但宣告无罪。1950年著有《》一书。1963年10月21日，让德句在[巴黎去世](../Page/巴黎.md "wikilink")，享年79岁。

## 參見

  -
  - [法屬中南半島總督列表](../Page/法屬中南半島總督列表.md "wikilink")

  - [阿尔贝特·萨罗](../Page/阿尔贝特·萨罗.md "wikilink")

## 参考书籍

  - de Folin, Jacques, *Indochine 1940-1955: La fin d'un rêve*,
    Librairie Académique Perrin, Paris, 1993.
  - Duroselle, Jean-Baptiste, *L'Abîme*, Imprimerie Nationale, Paris,
    1981.
  - Grandjean, Philippe, "L'amiral Decoux à la barre", in *La Nouvelle
    Revue d'Histoire*, No 12, mai-juin 2005, pp. 35-38.
  - Le Bourgeois, Jean, *Saïgon sans la France*, Plon, Paris, 1949.
  - Ministère des Affaires Étrangères: Archives de la France d'outre-mer
    (FOM).
  - Ministère des Affaires Étrangères: Fonds Decoux.
  - Romé, Amiral Paul (FNEO 1939-1945), *Les oubliés du bout du monde:
    Journal d'un marin d'Indochine de 1939 à 1946*, Éditions Danclau,
    Dinard, 1998.

[J](../Category/法國軍事人物.md "wikilink")
[Category:法屬印度支那總督](../Category/法屬印度支那總督.md "wikilink")
[Category:与大日本帝国合作的法国人](../Category/与大日本帝国合作的法国人.md "wikilink")