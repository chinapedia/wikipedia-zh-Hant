**讷伊桥站**（）是[巴黎地铁1号线的一个车站](../Page/巴黎地铁1号线.md "wikilink")，位于[塞纳河畔讷伊](../Page/塞纳河畔讷伊.md "wikilink")，于1937年启用。

车站的名称来自邻近的[讷伊桥](../Page/讷伊桥.md "wikilink")。

## 車站結構

<table>
<tbody>
<tr class="odd">
<td><p><strong>街道</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>B1</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>B2<br />
月台</strong></p></td>
</tr>
<tr class="even">
<td><p>西行</p></td>
</tr>
<tr class="odd">
<td><p>東行</p></td>
</tr>
<tr class="even">
<td><center>
<p><small>設有<a href="../Page/月台幕門.md" title="wikilink">月台幕門的</a><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門</small></p>
</center></td>
</tr>
</tbody>
</table>

[Pont de Neuilly](../Category/巴黎地鐵1號線車站.md "wikilink") [Pont de
Neuilly](../Category/上塞納省鐵路車站.md "wikilink")
[Category:1937年啟用的鐵路車站](../Category/1937年啟用的鐵路車站.md "wikilink")
[Category:1937年法國建立](../Category/1937年法國建立.md "wikilink")