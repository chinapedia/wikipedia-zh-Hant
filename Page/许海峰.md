**许海峰**（），[籍贯](../Page/籍贯.md "wikilink")[安徽](../Page/安徽.md "wikilink")[和县](../Page/和县.md "wikilink")，出生於[福建](../Page/福建.md "wikilink")[漳州](../Page/漳州.md "wikilink")。[中國退役](../Page/中華人民共和國.md "wikilink")[射擊運動員](../Page/射擊.md "wikilink")，任國家射擊隊總教練和[國家體育總局射擊中心副主任](../Page/國家體育總局.md "wikilink")、首位奪得奧運金牌的中國人。

## 生涯

許海峰於1982年加入[安徽省射擊隊](../Page/安徽省.md "wikilink")，開始了他的射擊生涯。

1983年，許海峰奪得人生中首面重要賽事中的獎牌：在[五運會中奪得兩面](../Page/五運會.md "wikilink")[銀牌](../Page/銀牌.md "wikilink")。一年後，即1984年，許海峰於[奧運的男子自選手槍項目](../Page/1984年夏季奧林匹克運動會.md "wikilink")，以566環的成績奪得该届奥运会首枚金牌——50米手枪慢射[金牌](../Page/金牌.md "wikilink")，是中國奥运会金牌「零的突破」。20年後，當[記者訪問他成為首位奪得金牌的運動員有何感覺](../Page/記者.md "wikilink")，許海峰-{回應}-認為自己是首位得到[奧運金牌的中國運動員只是由於射擊項目較早舉行他才能有此美譽](../Page/奧運.md "wikilink")。現時該面無價的金牌陳列在[中國革命博物館](../Page/中國革命博物館.md "wikilink")。

1985年4月26日\[1\]，加入中国共产党。許海峰在[1986年亚运会中](../Page/1986年亚运会.md "wikilink")，奪得三個冠軍，而其中自選[手槍項目中更破了](../Page/手槍.md "wikilink")[世界紀錄](../Page/世界紀錄.md "wikilink")。及後的全國射擊中再一次打破世界紀錄。到了1990年在[北京亞運會中摘下](../Page/1990年亞洲運動會.md "wikilink")4面[金牌](../Page/金牌.md "wikilink")，1年後世界氣槍錦標賽冠軍以及亞洲錦標賽的5面金牌為許海峰的射擊生涯加添更多色彩。

1994年底，許海峰退役後擔任國家女子手槍主教練，後來成為國家射擊隊副總教練，到了2001年理任國家射擊隊總教練，其後的6月出任[國家體育總局射擊中心副主任](../Page/國家體育總局.md "wikilink")。

許海峰旗下的愛徒包括了[杜麗](../Page/杜麗.md "wikilink")、[陶璐娜](../Page/陶璐娜.md "wikilink")、[王成意等有名的射擊運動員](../Page/王成意.md "wikilink")，大都是[奧運的獎牌得主](../Page/奥林匹克运动会.md "wikilink")。許海峰曾於2003年獲得由[中國奧委會](../Page/中國奧委會.md "wikilink")、[中華全國體育總會與](../Page/中華全國體育總會.md "wikilink")[中央電視台聯合頒布的](../Page/中央電視台.md "wikilink")[中國電視體育獎中的最佳教練員獎](../Page/中國電視體育獎.md "wikilink")。

2005年，许海峰改任国家体育总局自行车和击剑运动管理中心的副主任。

## 注释

[Category:中国射击运动员](../Category/中国射击运动员.md "wikilink")
[Category:中国奥运射击运动员](../Category/中国奥运射击运动员.md "wikilink")
[Category:中国国家队总教练](../Category/中国国家队总教练.md "wikilink")
[Category:中国国家队主教练](../Category/中国国家队主教练.md "wikilink")
[Category:国家体育总局官员](../Category/国家体育总局官员.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:中国奥林匹克运动会铜牌得主](../Category/中国奥林匹克运动会铜牌得主.md "wikilink")
[Category:1984年夏季奧林匹克運動會獎牌得主](../Category/1984年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1988年夏季奧林匹克運動會獎牌得主](../Category/1988年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奧林匹克運動會射擊獎牌得主](../Category/奧林匹克運動會射擊獎牌得主.md "wikilink")
[Category:中国共产党党员
(1985年入党)](../Category/中国共产党党员_\(1985年入党\).md "wikilink")
[Category:漳州籍运动员](../Category/漳州籍运动员.md "wikilink")
[Category:马鞍山籍运动员](../Category/马鞍山籍运动员.md "wikilink")
[Category:和县人](../Category/和县人.md "wikilink")
[Category:中国体育之最](../Category/中国体育之最.md "wikilink")
[Haifung](../Category/許姓.md "wikilink")
[Category:安徽籍运动员](../Category/安徽籍运动员.md "wikilink")
[Category:改革先锋称号获得者](../Category/改革先锋称号获得者.md "wikilink")

1.