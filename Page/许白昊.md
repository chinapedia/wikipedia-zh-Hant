**许白昊**（），[湖北省](../Page/湖北省.md "wikilink")[应城县人](../Page/应城县.md "wikilink")。[中国共产党早期领导人](../Page/中国共产党.md "wikilink")，工人运动活动家。

## 生平

许白昊幼年父母双亡，以半工半读毕业于[武昌甲种工业学校机械科](../Page/武昌甲种工业学校.md "wikilink")。后到[上海](../Page/上海.md "wikilink")、[杭州工厂任修机匠](../Page/杭州.md "wikilink")，后又考入[浙江省立工业学校学习](../Page/浙江省立工业学校.md "wikilink")。1919年弃学去上海从事劳工运动。

1921年8月加入[中国劳动组合书记部](../Page/中国劳动组合书记部.md "wikilink")，同年底赴[苏联](../Page/苏联.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")，出席次年1月召开的[远东各国共产党及民族革命团体第一次代表大会](../Page/远东各国共产党及民族革命团体第一次代表大会.md "wikilink")。1922年春回国加入中国共产党，到武汉负责中国劳动组合书记部长江分部领导工作。7月16日至23日，作为湖北代表出席在上海召开的[中国共产党第二次全国代表大会](../Page/中国共产党第二次全国代表大会.md "wikilink")\[1\]。

10月被选为[湖北全省工团联合会](../Page/湖北全省工团联合会.md "wikilink")（原[武汉工团联合会](../Page/武汉工团联合会.md "wikilink")）秘书兼组织部副主任。1923年2月初赴郑州参加[京汉铁路总工会成立盛典](../Page/京汉铁路总工会.md "wikilink")，积极发动湖北各工团声援[二七大罢工](../Page/二七大罢工.md "wikilink")。不久被推选为[湖北全省工团联合会执行委员](../Page/湖北全省工团联合会.md "wikilink")，并任[中共武汉区执行委员会委员](../Page/中共武汉区执行委员会.md "wikilink")。1927年4至5月出席在[武昌召开的](../Page/武昌.md "wikilink")[中国共产党第五次全国代表大会](../Page/中国共产党第五次全国代表大会.md "wikilink")，被选为中共中央监察委员和中央工人运动委员会委员。

[七一五事变后他逃亡于](../Page/宁汉合流.md "wikilink")[上海公共租界](../Page/上海公共租界.md "wikilink")，担任中共上海总工会党团书记兼总工会组织部部长。1928年2月17日因叛徒[唐瑞林告密](../Page/唐瑞林.md "wikilink")，与[陈乔年](../Page/陈乔年.md "wikilink")、[郑复他等](../Page/郑复他.md "wikilink")11人同时被捕。

1928年6月6日，他在上海与陈乔年、郑复他同时被杀害\[2\]。

妻子[秦怡君](../Page/秦怡君.md "wikilink")，许遇害后，秦改嫁[李求实](../Page/李求实.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:中国工人运动领袖](../Category/中国工人运动领袖.md "wikilink")
[Category:中国共产党第五届中央监察委员会委员](../Category/中国共产党第五届中央监察委员会委员.md "wikilink")
[Category:中华民国大陆时期中共烈士](../Category/中华民国大陆时期中共烈士.md "wikilink")
[X](../Category/中国共产党党员_\(1922年入党\).md "wikilink")
[X](../Category/应城人.md "wikilink")
[B](../Category/许姓.md "wikilink")
[Category:中共二大代表](../Category/中共二大代表.md "wikilink")

1.  [中国共产党第二次全国代表大会](http://www.gov.cn/test/2007-08/28/content_729220.htm)
2.  [許白昊歷史](http://www.yingchengnet.com/2007-05/26/cms358200article.shtml)