**评书**，又稱**說書**，[湖北](../Page/湖北.md "wikilink")、[广东](../Page/广东.md "wikilink")[粤语地区及](../Page/粤语.md "wikilink")[闽南语地区称](../Page/闽南语.md "wikilink")**讲古**\[1\]\[2\]，在[四川称为](../Page/四川.md "wikilink")**讲书**，古称**說話**，是[中國](../Page/中國.md "wikilink")[东北](../Page/东北.md "wikilink")、[华北](../Page/华北.md "wikilink")、
[两广](../Page/两广.md "wikilink")、[湖广](../Page/湖广.md "wikilink")、[四川一带一种口头讲说的表演形式](../Page/四川.md "wikilink")，在[宋代開始流行](../Page/宋代.md "wikilink")。各地的**說書人**以自己的[母語對人說著不同的](../Page/母語.md "wikilink")[故事](../Page/故事.md "wikilink")，因此也是[方言](../Page/方言.md "wikilink")[文化的一部份](../Page/文化.md "wikilink")。[清末](../Page/清末.md "wikilink")[民初時](../Page/民初.md "wikilink")，評書的表演為一人坐於桌後表演，道具有[折扇和](../Page/折扇.md "wikilink")[醒木](../Page/醒木.md "wikilink")，服裝為[長衫](../Page/長衫.md "wikilink")；至20世纪中叶，多不再用桌椅及折扇、醒木等道具，而以站立說演，服裝也較不固定。

## 历史

以说书者的说法：评书起源于[东周时期](../Page/东周.md "wikilink")，[周庄王是评书的](../Page/周庄王.md "wikilink")[祖师爷](../Page/祖师爷.md "wikilink")。但这只是一个[传说](../Page/传说.md "wikilink")。实际评书的创始人为[明末](../Page/明末.md "wikilink")[清初的](../Page/清初.md "wikilink")[柳敬亭](../Page/柳敬亭.md "wikilink")，最初只是[说唱艺术的一部分](../Page/说唱艺术.md "wikilink")，称为“[弦子书](../Page/弦子书.md "wikilink")”，他的老師[莫後光提到說話理論是](../Page/莫後光.md "wikilink")：「夫演義雖小技，其以辨性情、考方俗、形容萬類，不與[儒者異道](../Page/儒者.md "wikilink")。故取之欲其肆，中之欲其微，促而赴之欲其迅，舒而繹之欲其安，進而止之欲其留，整而歸之欲其潔。非天下之精者，其孰與於斯矣！」\[3\]。

[北宋](../Page/北宋.md "wikilink")[汴京人](../Page/汴京.md "wikilink")[霍四究以](../Page/霍四究.md "wikilink")“說三分”著名，“不以風雨寒暑，諸棚看人，日日如是”，“說三分”即講[三國故事](../Page/三國.md "wikilink")\[4\]。晚清[光绪年间](../Page/光绪.md "wikilink")，评书传入[皇宫中](../Page/皇宫.md "wikilink")；因皇宫唱歌多有不便，于是改说唱为“评说”，于是评书的艺术形式便固定下来。

[民国是评书中兴的时期](../Page/民国.md "wikilink")，另外，评书与[相声也有很大的渊源](../Page/相声.md "wikilink")。尤其是单口相声和评书的“片子活”技巧几乎相同。有些相声的“段子”也来源于评书章节。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，评书也得到了发展。尤其是[广播和](../Page/广播.md "wikilink")[收音机的普及](../Page/收音机.md "wikilink")，给评书极大的生存空间。评书艺人又创作了许多新的段子，比如《[林海雪原](../Page/林海雪原.md "wikilink")》、《平原枪声》，[文革结束](../Page/文革.md "wikilink")，评书艺人又创作了不少段子，甚至也说几段外国书，比如《[珍珠港](../Page/珍珠港.md "wikilink")》（书名《太平洋大海战》），还制作了[Flash评书](../Page/Flash.md "wikilink")《[白眉大侠](../Page/白眉大侠.md "wikilink")》，但并不特别受书迷欢迎。

现在，几乎每个广播电台都有评书专栏，部分电台更有专门的评书或故事频率。主要听众是老年人或[出租车司机](../Page/出租车.md "wikilink")，评书仍很受听众的欢迎，其中原因除了本身评书喜闻乐见，还由于广播的局限。但一些评书演员担心评书会渐渐的消失，实际上，现在很难再看到年青的说书人了。

### 说话与评书

在[唐代](../Page/唐代.md "wikilink")，[佛教融入了通俗文學](../Page/佛教.md "wikilink")，[僧人為了吸引](../Page/僧人.md "wikilink")[檀越](../Page/檀越.md "wikilink")，發明了一種說演[佛經故事的藝術](../Page/佛經.md "wikilink")，稱之[變文](../Page/變文.md "wikilink")。不久，又出現了一種[曲艺艺术](../Page/曲艺.md "wikilink")，和评书的表演方式相似，这种曲艺形式称为“说话”。到宋代中兴时期\[5\]，在中国古典文学认为，“说话”这种表演的形式对明清[章回小说的影响是非常大的](../Page/章回小说.md "wikilink")\[6\]。“说话”发展到俗说后表演方式与“评书”是非常类似的。比如评书中的开场诗，说话称为“[押座文](../Page/押座文.md "wikilink")”。也有“且听下回分解”一类用句。当然，这些都影响了明清的小说。同时，说话文学也在日本有所發展，成爲一种。

事实上如《三国演义》、《[水浒传](../Page/水浒传.md "wikilink")》最初都是说话的[话本](../Page/话本.md "wikilink")。《三国演义》话本为《[全相平话三国志](../Page/三国志平话.md "wikilink")》；《水浒传》则为《[醉翁谈录](../Page/醉翁谈录.md "wikilink")》。[晚唐诗人](../Page/晚唐.md "wikilink")[李商隐有](../Page/李商隐.md "wikilink")《驕兒》一诗写到：“或谑[张飞](../Page/张飞.md "wikilink")[鬍](../Page/鬍.md "wikilink")，或笑[邓艾](../Page/邓艾.md "wikilink")[吃](../Page/口吃.md "wikilink")。”说明当时喜欢说话这门曲艺的百姓是非常多的。

研究“说话”，多数是从话本对明清的小说的影响加以研究。也有一些曲艺艺人写过专门的资料。很多证据表明“说话”类似于“评书”。但两者似乎没有任何确实的传承关系。

## 題材

说书虽然以讲史为主，但多数为引人入胜，并没有尊重历史的原貌。比如：[商朝有](../Page/商朝.md "wikilink")[大炮](../Page/大炮.md "wikilink")；[汉代有](../Page/汉代.md "wikilink")[银票](../Page/银票.md "wikilink")；[三国有](../Page/三国.md "wikilink")[武狀元](../Page/武狀元.md "wikilink")。但由于说书面向的是[百姓](../Page/百姓.md "wikilink")，客观上起到普及文化和历史的作用。在古代，许多不认识字的人都能吟两句[打油诗](../Page/打油诗.md "wikilink")、了解历史，唯一的途径就是评书和[戏曲的传播](../Page/戏曲.md "wikilink")。

说书的段子来源分四类：史书（《[三国演义](../Page/三国演义.md "wikilink")》、《[隋唐演义](../Page/隋唐演义.md "wikilink")》）、神怪书（《[封神演义](../Page/封神演义.md "wikilink")》、《[济公传](../Page/济公传.md "wikilink")》）公案书（[施公案](../Page/施公案.md "wikilink")、[包公案](../Page/包公.md "wikilink")）、[武侠书](../Page/武侠.md "wikilink")（《[三侠五义](../Page/三侠五义.md "wikilink")》、《[童林传](../Page/童林传.md "wikilink")》）。但几乎只要是[小说](../Page/小说.md "wikilink")，经过略微加工，就可以成书评说。

## 形式

每一段的评书称“回”，小于100回的书称小段。如果只是一回或几回称“片子活”。如果大于100回那么称为，“长篇活”。

说一部开篇先赋一首诗，称为“开场诗”，常说一首诗或小令诗。比较常见的是《[西江月](../Page/西江月.md "wikilink")》或《[临江仙](../Page/临江仙.md "wikilink")》。回与回之间一般以“且听下回分解”和“上回书，我们说到......”连接。这和[章回小说是一样的](../Page/章回小说.md "wikilink")。

评书以语言除了“开场诗”外，还有多种技巧：

  - 开脸儿：介绍新人物的即将人物的来历、身份、相貌、性格等等特征
  - 摆砌末：讲述故事的场景
  - [赋赞](../Page/赋赞.md "wikilink")：如果赞美故事中人物的品德、相貌或风景名胜，念诵大段落[对偶句式韵文](../Page/对偶.md "wikilink")，称作“赋赞”，富有语言的美感；
  - [垛句](../Page/垛句.md "wikilink")：说演到紧要处或精彩处，使用[排比重叠的句式以强化说演效果](../Page/排比.md "wikilink")。

## 传承

评书从[柳敬亭](../Page/柳敬亭.md "wikilink")、[王鸿兴开始](../Page/王鸿兴.md "wikilink")，经十代传承之今。以前三代谱系：

  - 第一代：王鸿兴
  - 第二代：[安良臣](../Page/安良臣.md "wikilink")、[邓光臣](../Page/邓光臣.md "wikilink")、[何良臣](../Page/何良臣.md "wikilink")
  - 第三代：[张海凤](../Page/张海凤.md "wikilink")、[张沛然](../Page/张沛然.md "wikilink")、[柯光玉](../Page/柯光玉.md "wikilink")、[安太和](../Page/安太和.md "wikilink")

著名的说书艺人：

  - 老一辈的有：[张悦楷](../Page/张悦楷.md "wikilink")、[林兆明](../Page/林兆明.md "wikilink")、[连阔如](../Page/连阔如.md "wikilink")、[袁阔成](../Page/袁阔成.md "wikilink")、[李鑫荃](../Page/李鑫荃.md "wikilink")、[田连元](../Page/田连元.md "wikilink")、[连丽如](../Page/连丽如.md "wikilink")、[单田芳](../Page/单田芳.md "wikilink")、[刘兰芳](../Page/刘兰芳.md "wikilink")、[刘延广](../Page/刘延广.md "wikilink")、[金文声](../Page/金文声.md "wikilink")、[刘立福](../Page/刘立福.md "wikilink")
  - 新一辈的有：[孙一](../Page/孙一.md "wikilink")、[张少佐](../Page/张少佐.md "wikilink")、[王玥波](../Page/王玥波.md "wikilink")、[李伯清](../Page/李伯清.md "wikilink")

## 注释

<div class="references-small">

<references />

</div>

## 相關條目

  - [有聲書](../Page/有聲書.md "wikilink")
  - [廣播劇](../Page/廣播劇.md "wikilink")
  - [相声](../Page/相声.md "wikilink")

## 參考

  - Vibeke Bordahl著，米鋒等譯：《揚州評話探討》（北京：人民文學出版社，2007）。
  - B. Riftin（李福清）著，尹錫康等譯：《三國演義與民間文學傳統》（上海：上海古籍出版社，1997）。

[评书](../Page/category:评书.md "wikilink")
[category:曲藝](../Page/category:曲藝.md "wikilink")

1.  [湖北大公网 \> 民风民俗 \> 讲古](http://www.hbdgw.com/content.asp?id=26394)
2.  [中華民國教育部臺灣閩南語常用詞辭典](http://twblg.dict.edu.tw/holodict_new/result_detail.jsp?n_no=12410&curpage=1&sample=%E8%AC%9B%E5%8F%A4&radiobutton=1&querytarget=1&limit=20&pagenum=1&rowcount=2)
3.  [李延是](../Page/李延是.md "wikilink")：《南吳舊話錄》卷二十一
4.  [孟元老](../Page/孟元老.md "wikilink")，《[东京梦华录笺注](../Page/东京梦华录.md "wikilink")》卷五《京瓦伎艺》，[中华书局](../Page/中华书局.md "wikilink")2006年版，第462页
5.  [翟颧](../Page/翟颧.md "wikilink")《通俗编》卷三十一《徘优》条引耐得翁《古杭梦游录》:“说话有四家:一者小说，谓之银字儿，如烟粉、灵怪、传奇、说公案，皆是传(搏)拳提刀赶棒及发迹变态之事;说铁骑儿，谓士马金鼓之事;说经，谓演说佛书，说参谓参禅;讲史书，谓说前代兴废争战之事。”
6.  见[鲁迅](../Page/鲁迅.md "wikilink")《宋人之“说话”及其影响》；[相恺](../Page/相恺.md "wikilink")《宋元小说史》。