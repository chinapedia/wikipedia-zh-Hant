**玛哈·扎克里·诗琳通公主殿下**（，），[泰国](../Page/泰国.md "wikilink")[扎克里王朝公主](../Page/扎克里王朝.md "wikilink")\[1\]。她是[普密蓬国王的次女](../Page/普密蓬·阿杜德.md "wikilink")。她的称呼为“[公主](../Page/公主.md "wikilink")[殿下](../Page/殿下.md "wikilink")”。1977年12月5日，封号變更為“玛哈·扎克里公主”，又被称为**帕贴**（，，意为“天佛”、“天使公主”）\[2\]。

在家中排名第三，僅次於姐姐[烏汶叻公主和哥哥](../Page/烏汶叻公主.md "wikilink")[瑪哈·瓦集拉隆功](../Page/瑪哈·瓦集拉隆功.md "wikilink")（現任泰王[拉瑪十世](../Page/拉瑪十世.md "wikilink")）。另有一妹[朱拉蓬公主殿下](../Page/朱拉蓬公主.md "wikilink")。

## 生平

诗琳通公主在1955年4月2日生於泰國曼谷的泰國皇宮中，1978年獲[朱拉隆功大学](../Page/朱拉隆功大学.md "wikilink")[梵語和](../Page/梵語.md "wikilink")[巴利語碩士](../Page/巴利語.md "wikilink")，1980年獲[金石學](../Page/金石學.md "wikilink")（[梵文和](../Page/梵文.md "wikilink")[柬埔寨文](../Page/柬埔寨文.md "wikilink")）碩士，1987年[詩納卡寧威洛大學發展教育學](../Page/詩納卡寧威洛大學.md "wikilink")[哲學博士](../Page/哲學博士.md "wikilink")\[3\]。现任副会长、[帕尊宗浩陆军军官学校历史专业教授](../Page/帕尊宗浩陆军军官学校.md "wikilink")。

诗琳通公主聪颖好学，年幼时便在国王及王后指导下开始学习[中国历史和](../Page/中国历史.md "wikilink")[中国文学](../Page/中国文学.md "wikilink")，能说一口流利的[汉语](../Page/汉语.md "wikilink")，鉴赏[漢詩](../Page/漢詩.md "wikilink")、[詞](../Page/宋词.md "wikilink")，擅长中国[书画](../Page/书画.md "wikilink")，能用[二胡等](../Page/二胡.md "wikilink")[中国民族乐器演奏](../Page/中国民族乐器.md "wikilink")。

詩琳通公主深受泰國人愛戴，在國內除其已故父親，前任國王[普密蓬·阿杜德](../Page/普密蓬·阿杜德.md "wikilink")（拉瑪九世）及詩麗吉王太后外無出其右。和父王一樣，詩琳通公主非常關心貧苦大眾，經常探訪貧民，如[2004年印度洋大地震中](../Page/2004年印度洋大地震.md "wikilink")，探望受灾災民。泰国人民对她的爱戴，似乎遠超其兄长，現任國王[摩訶·瓦集拉隆功](../Page/摩訶·瓦集拉隆功.md "wikilink")。

詩琳通公主曾就王位繼承權表示無意和兄長[摩訶·瓦集拉隆功爭位](../Page/摩訶·瓦集拉隆功.md "wikilink")。

詩琳通公主至今仍然未婚。

## 作品

著有《[顽皮透顶的盖珥](../Page/顽皮透顶的盖珥.md "wikilink")》、《[踏访龙的国土](../Page/踏访龙的国土.md "wikilink")》、《[平沙万里行](../Page/平沙万里行.md "wikilink")》、《[採桃花](../Page/採桃花.md "wikilink")》、《[長城遊](../Page/長城遊.md "wikilink")》、《[青木瓜沙拉之歌](../Page/青木瓜沙拉之歌.md "wikilink")》等小说、散文和诗歌作品。1994年1月，中国的[生活·读书·新知三联书店出版了](../Page/生活·读书·新知三联书店.md "wikilink")《[诗琳通公主诗文画集](../Page/诗琳通公主诗文画集.md "wikilink")》。

### 关于笔名

名字“诗琳通”源于[巴利语](../Page/巴利语.md "wikilink")，意思是“光荣的”\[4\]。除“诗琳通”（）外，她还曾使用过4个笔名：

1.  功欣功古（）：原意为[鹅卵石](../Page/鹅卵石.md "wikilink")，诗琳通公主的化名之一。
2.  菀盖珥（）\[5\]：诗琳通公主为自己定下的笔名，原意为玻璃镜（在泰语里也是的俗名）。
3.  小宝贝（）：公主的别名之一。
4.  班丹（）：原意为“创造”。\[6\]

## 图集

<File:Royal> Monogram of Princess Maha Chakri Sirindhorn.svg|皇家标志
<File:Royal> Flag of Princess Maha Chakri
Sirindhorn.svg|[皇家旗帜](../Page/泰国王室旗帜.md "wikilink")
<File:Princess> sirinhorn 01.jpg|工作中的公主，摄于1992年 <File:สมเด็จพระเทพฯ>
เสด็จฯ รร.สวนกุหลาบวิทยาลัย นนทบุรี พ.ศ.2535
(2).jpg|1992年于[放生](../Page/放生.md "wikilink") Her Royal Highness
Princess Maha Chakri Sirindhorn (4103332252).jpg|一位画家正在描绘诗琳通公主的御容，2004年。
<File:Princess> Maha Chakri Sirindhorn.jpg|2006年 <File:Princess> Maha
Chakri Sirindhorn 2010-12-7.jpg|2010年 <File:นายกรัฐมนตรี>
กราบบังคลทูลรายงาน สมเด็จพระเทพรัตนราชสุด
- Flickr - Abhisit Vejjajiva.jpg|2011年 <File:Thai> Princess
(34036080496)
(cropped).jpg|2015年4月19日参访[冲绳科学技术大学院大学](../Page/冲绳科学技术大学院大学.md "wikilink")，手机壳上图案即是公主的卡通形象\[7\]。
The President, Shri Pranab Mukherjee presenting the Padma Bhushan Award
to H.R.H. Princess Maha Chakri Siridhorn , at a Civil Investiture
Ceremony, at Rashtrapati Bhavan, in New Delhi on March 30,
2017.jpg|2017年3月30日，印度总统[普拉纳布·慕克吉向诗琳通公主颁发](../Page/普拉纳布·慕克吉.md "wikilink")[莲花装勋章](../Page/莲花装勋章.md "wikilink")\[8\]。
<File:Princess> Sirindhorn, February 3, 2018 (cropped).jpg|2018年2月3日
<File:2016> Bangkok, Dystrykt Samphanthawong, Brama Chinatown (04)
(cropped).jpg|曼谷唐人街[崇圣牌楼的](../Page/崇圣牌楼.md "wikilink")“圣寿无疆”题字
<File:DSC06465> Art from Royal Crematorium king Rama 9 Photographed by
Trisorn Triboon.jpg|九世王火葬亭分馆的壁画，诗琳通公主常伴随在父王的身后。

## 参见

  - [中国人民的老朋友](../Page/中国人民的老朋友.md "wikilink")
  - [中泰关系](../Page/中泰关系.md "wikilink")
  - [2015年诗琳通公主60岁寿辰](../Page/2015年诗琳通公主60岁寿辰.md "wikilink")
  - [诗琳通公主访华列表](../Page/诗琳通公主访华列表.md "wikilink")

## 参考文献

## 外部链接

  - [Her Royal Highness Princess Maha Chakri Sirindhorn’s Personal
    Affairs
    Division](https://web.archive.org/web/20090503035152/http://www.sirindhorn.net/index.en.html)
  - "[Biography of Her Royal Highness Princess Maha Chakri
    Sirindhorn](http://kanchanapisek.or.th/biography/sirindhorn/index.en.html)",
    at Kanchanapisek Network
  - [Maha Chakri Sirindhorn and Thai Heritage
    Conservation](http://www.cs.ait.ac.th/wutt/pthep/pthep.html)
  - [The Forest
    Herbarium](https://web.archive.org/web/20060818213440/http://www.dnp.go.th/Botany/Web_Plant/plantdetail.aspx?monthno=200109&smonthname=September)
  - [Scholarship Website
    Information](http://www.liv.ac.uk/international/money-and-scholarships/princess-sirindhorn.htm=September)

{{-}}

[Category:泰国公主](../Category/泰国公主.md "wikilink")
[诗琳通公主](../Category/诗琳通公主.md "wikilink")
[Category:泰国作家](../Category/泰国作家.md "wikilink")
[Category:旅游作家](../Category/旅游作家.md "wikilink")
[Category:女性翻譯家](../Category/女性翻譯家.md "wikilink")
[Category:泰国翻译家](../Category/泰国翻译家.md "wikilink")
[Category:泰国汉学家](../Category/泰国汉学家.md "wikilink")
[泰](../Category/中国人民的老朋友.md "wikilink")
[Category:联合国教科文组织亲善大使](../Category/联合国教科文组织亲善大使.md "wikilink")
[Category:詩納卡寧威洛大學校友](../Category/詩納卡寧威洛大學校友.md "wikilink")
[Category:朱拉隆功大学校友](../Category/朱拉隆功大学校友.md "wikilink")
[Category:华东师范大学名誉教授](../Category/华东师范大学名誉教授.md "wikilink")
[Category:北京大学名誉博士](../Category/北京大学名誉博士.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:麥格塞塞獎獲得者](../Category/麥格塞塞獎獲得者.md "wikilink")
[Category:女性詩人](../Category/女性詩人.md "wikilink")

1.
2.
3.  Pattayamail, [more education
    information](http://www.pattayamail.com/localnews/a-most-happy-birthday-to-her-royal-highness-princess-maha-chakri-sirindhorn-36213)
4.
5.
6.
7.
8.  [诗琳通公主获印度“莲花装勋章”](https://wemedia.ifeng.com/11716972/wemedia.shtml)