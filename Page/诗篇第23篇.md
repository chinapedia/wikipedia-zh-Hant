[Eastman_Johnson_-_The_Lord_is_my_Shepard_-_ebj_-_fig_76_pg141.jpg](https://zh.wikipedia.org/wiki/File:Eastman_Johnson_-_The_Lord_is_my_Shepard_-_ebj_-_fig_76_pg141.jpg "fig:Eastman_Johnson_-_The_Lord_is_my_Shepard_-_ebj_-_fig_76_pg141.jpg")

**《诗篇》第23篇**（），因其首句“是我的牧者”而有时被称为****（“牧者诗篇”），是《[诗篇](../Page/诗篇.md "wikilink")》中较著名的一首。

这首诗歌的主题是说到[上帝扮演保护者和供应者的角色](../Page/上帝.md "wikilink")，因而为[犹太人和](../Page/犹太人.md "wikilink")[基督徒所喜爱](../Page/基督徒.md "wikilink")。对于基督徒，“[主](../Page/耶稣是主.md "wikilink")”指[耶稣](../Page/耶稣.md "wikilink")；因为《[约翰福音](../Page/约翰福音.md "wikilink")》记载，耶稣自称“[好牧人](../Page/好牧人.md "wikilink")”。

《诗篇》第23篇已经多次被[圣诗作者谱上乐曲](../Page/圣诗.md "wikilink")。

## 寫作背景

正如詩篇23篇所言，這篇的作者是[大衛](../Page/大卫王.md "wikilink")。由於大衛原是牧人，這篇亦以牧人和羊作比喻的詩篇對大衛的身世尤其貼切。除了以「牧者」、「青草」、「安歇的水邊」、「你的杖，你的竿」等充滿牧人特色的詞句外，這篇詩篇亦對應了大衛的人生經歷。

大衛一生都在逃避敵人殺害的日子中渡過。除了早年被掃羅追殺外，晚年更經歷兒子押沙龍的叛變，但大衛往往能逃命。因此，詩篇中有提到「我雖然行過死蔭的幽谷，也不怕遭害，因為你與我同在」，以及「在我敵人面前，你為我摆設筵席」。

## 译文

### 汉语

<u>大衛</u>的诗。
[耶和华是我的牧者](../Page/耶和华.md "wikilink")；我必不至缺乏。
祂使我躺卧在青草地上，
领我在可安歇的水边。
祂使我的灵魂甦醒，
为自己的名引导我走义路。
我虽然行过死荫的幽谷，
也不怕遭害，因为你与我同在；你的杖，你的竿，都安慰我。
在我敌人面前，你为我摆设筵席；
你用油膏了我的头，使我的福杯满溢。
我一生一世必有恩惠慈爱随着我，
我且要住在耶和华的殿中，直到永远。

## 使用

### 犹太教

在周六下午，[犹太人进食](../Page/犹太人.md "wikilink")[安息日第三餐时](../Page/安息日.md "wikilink")，经常用古代[希伯来语吟唱这首诗歌](../Page/希伯来语.md "wikilink")。[瑟法底犹太人和部分Hassidic犹太人在周五下午仪式上](../Page/瑟法底犹太人.md "wikilink")，以及安息日晚餐和日餐上都唱这首诗。犹太教葬礼（Yizkor）中也会唱这首诗歌。在犹太假日期间，墓地葬礼上用诵读这首诗来代替传统的祷告。

### 基督教

[东正教徒在领受](../Page/东正教.md "wikilink")[圣餐前的祷告中通常都包括这首诗歌](../Page/圣餐.md "wikilink")。

由于诗中提到安慰与保护，以及曲调为人们所熟知，诗篇23篇成为葬礼仪式中的重要部分。

## 在通俗文化中

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Tehillim - Psalm 23 (Judaica
    Press)](http://www.chabad.org/article.asp?aid=16244) translation
    with [Rashi](../Page/Rashi.md "wikilink")'s commentary at
    [Chabad.org](../Page/Chabad.org.md "wikilink")
  - [Text of Psalm 23 in
    Hebrew](http://www.mechon-mamre.org/p/pt/pt2623.htm)
  - [Psalm 23 recited in
    Hebrew](http://media.snunit.k12.il/kodeshm/mp3/t2623.mp3)
  - [Text of Psalm 23 in English (King James
    Version)](http://www.christnotes.org/bible.php?q=psalm+23&ver=kjv)
  - [text of Psalm 22 in the Latin
    Vulgate](http://www.drbo.org/lvb/chapter/21022.htm)
  - [Psalm 23 as Manifestation
    Prayer](http://www.themiraclemerchant.com/manifestation-prayer-a-3.html)

{{-}}

[\#23](../Category/诗篇.md "wikilink")
[Category:希伯来圣经章节](../Category/希伯来圣经章节.md "wikilink")