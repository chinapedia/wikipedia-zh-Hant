**诺曼·厄尔·萨加德**（，），前[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")、[美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")[上尉](../Page/上尉.md "wikilink")，执行过行[STS-7](../Page/STS-7.md "wikilink")、[STS-51-B](../Page/STS-51-B.md "wikilink")、[STS-30](../Page/STS-30.md "wikilink")、[STS-42](../Page/STS-42.md "wikilink")、以及[STS-71任务](../Page/STS-71.md "wikilink")。

## 外部链接

  - [美国国家航空航天局网站的萨加德介绍](https://web.archive.org/web/20110829052911/http://www.codeshop.info/)

  - [萨加德个人网站](https://web.archive.org/web/20110829052911/http://www.codeshop.info/)

[T](../Category/美國海軍陸戰隊軍官.md "wikilink")
[T](../Category/第八组宇航员.md "wikilink")
[T](../Category/佛罗里达州立大学校友.md "wikilink")