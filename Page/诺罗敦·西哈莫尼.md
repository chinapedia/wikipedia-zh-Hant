**诺罗敦·西哈莫尼**（；），[中文简称](../Page/中文.md "wikilink")“**西哈莫尼**”，[柬埔寨](../Page/柬埔寨.md "wikilink")[国王](../Page/柬埔寨国王.md "wikilink")（2004年10月14日）。\[1\]西哈莫尼为前[国王](../Page/国王.md "wikilink")[西哈努克与第六位妻子](../Page/诺罗敦·西哈努克.md "wikilink")[莫尼列王后](../Page/诺罗敦·莫尼列·西哈努克.md "wikilink")（即莫尼克王后，擁有[法](../Page/法國.md "wikilink")、[中](../Page/中国.md "wikilink")、[意](../Page/意大利.md "wikilink")、柬混血血統）长子，有华人血统，诺罗敦·西哈莫尼1994年被[封为](../Page/封号.md "wikilink")[坤](../Page/坤.md "wikilink")[亲王](../Page/亲王.md "wikilink")，继任国王之前任柬埔寨驻[联合国教科文组织代表](../Page/联合国教科文组织.md "wikilink")（大使）。2004年10月6日，寓居[北京的柬埔寨国王西哈努克发表告全国书](../Page/北京.md "wikilink")，宣布因健康原因退出王位，之后[谢辛临时受命为](../Page/谢辛.md "wikilink")[代理](../Page/代理_\(政治\).md "wikilink")[国家元首](../Page/国家元首.md "wikilink")；10月14日，[柬埔寨王位委员会成员](../Page/柬埔寨王位委员会.md "wikilink")[首相](../Page/柬埔寨首相.md "wikilink")[洪森](../Page/洪森.md "wikilink")、[国民议会](../Page/柬埔寨国民议会.md "wikilink")[议长](../Page/议长.md "wikilink")[拉那烈亲王等](../Page/诺罗敦·拉那烈.md "wikilink")9人于[金边王宫举行会议](../Page/金边王宫.md "wikilink")，推选西哈莫尼继任柬埔寨新国王。

西哈努克与莫尼列王后1952年4月结婚后，次年在柬埔寨首都金边生下西哈莫尼。

西哈莫尼从小对艺术有着强烈兴趣，曾是一名[芭蕾舞](../Page/芭蕾舞.md "wikilink")[演员](../Page/演员.md "wikilink")，以前从未介入过政治，因此也没有得罪过任何政治人物，被看作是柬各政治派别都可以接受的王位继承者，也是西哈努克和莫尼列王后覺得合適的继承人。

1993年8月30日，西哈莫尼被任命为柬埔寨驻[联合国教科文组织的代表](../Page/联合国教科文组织.md "wikilink")。西哈努克宣布退位前不久，他已经辞去这一职务从[法国](../Page/法国.md "wikilink")[巴黎返回金边](../Page/巴黎.md "wikilink")。舆论认为，西哈莫尼这一举动暗示他很可能继承柬埔寨王位。

西哈努克退位前公开表态，支持西哈莫尼继任王位。他在致全国公开信中说，西哈莫尼是“一位正直的爱国者，不论在任何环境都会与国家、人民同甘共苦，对抗侵略者”。他还表示，西哈莫尼「立场中立，不介入政治，不属于任何党派，这适合柬埔寨现状」。

柬埔寨王位委员会2004年10月14日在金边发表公报宣布，首相[洪森和国民议会议长](../Page/洪森.md "wikilink")[诺罗敦·拉那烈亲王等](../Page/诺罗敦·拉那烈.md "wikilink")9名王位委员会成员当天在金边王宫举行会议，以无记名投票方式一致推选51岁的诺罗敦·西哈莫尼为柬埔寨王国新国王。

## 參考資料

{{-}}

[Category:柬埔寨君主](../Category/柬埔寨君主.md "wikilink")
[Category:高棉裔混血兒](../Category/高棉裔混血兒.md "wikilink")
[Category:现任君主](../Category/现任君主.md "wikilink")
[Category:法國榮譽軍團大軍官勳位](../Category/法國榮譽軍團大軍官勳位.md "wikilink")
[Category:法蘭西裔混血兒](../Category/法蘭西裔混血兒.md "wikilink")
[Category:義大利裔混血兒](../Category/義大利裔混血兒.md "wikilink")
[Category:亚洲纸币上的人物](../Category/亚洲纸币上的人物.md "wikilink")

1.  [柬埔寨国王西哈莫尼和太后莫尼列来华](http://www.apdnews.com/news/69955.html)
    ，亚太日报，2014年2月20日