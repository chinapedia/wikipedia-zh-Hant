[Chinese_condiments_at_the_restaurant_by_bhollar_in_Nerima,_Tokyo.jpg](https://zh.wikipedia.org/wiki/File:Chinese_condiments_at_the_restaurant_by_bhollar_in_Nerima,_Tokyo.jpg "fig:Chinese_condiments_at_the_restaurant_by_bhollar_in_Nerima,_Tokyo.jpg")、[麻油](../Page/麻油.md "wikilink")、[港式辣油](../Page/港式辣油.md "wikilink")、[鹽](../Page/鹽.md "wikilink")）\]\]
[Kruiden_in_Egyptische_bazaar_Eminönü_Istanbul.JPG](https://zh.wikipedia.org/wiki/File:Kruiden_in_Egyptische_bazaar_Eminönü_Istanbul.JPG "fig:Kruiden_in_Egyptische_bazaar_Eminönü_Istanbul.JPG")的香料市场\]\]

**調味料**或**调味品**是指加入其他食物中，用来[改善味道的](../Page/調味.md "wikilink")[食品成分](../Page/食品.md "wikilink")。如果细分，调味料可以分为**作料**和**佐料**。作料指的是在[烹调食物之前和过程中加入的调味料](../Page/烹调.md "wikilink")，比如腌制食物的[料酒](../Page/料酒.md "wikilink")、炒菜时洒下的[盐](../Page/盐.md "wikilink")；佐料则是在烹调后调味，即在食用过程中，供以添、蘸、抹、撒等，比如吃[面条时添入的](../Page/面条.md "wikilink")[辣椒油](../Page/辣椒油.md "wikilink")、吃[白斩鸡时的蘸料](../Page/白斩鸡.md "wikilink")[蒜蓉](../Page/蒜蓉.md "wikilink")。从来源上，调味料多数直接或间接来自植物，少数为动物成分（例如[日本料理中](../Page/日本料理.md "wikilink")[味噌汤所用的](../Page/味噌汤.md "wikilink")[柴魚片](../Page/柴魚片.md "wikilink")）或者合成成分（例如[味精](../Page/味精.md "wikilink")）。

一些调味料在特定情况下用来作主食或主要成分来食用。例如洋葱也可以为法国洋葱汤等的主要蔬菜成分。

## 在各地区的变迁

不同国家和同一国内不同地区的烹饪流派，一般都有自己的特色调味料为标志。例如[興渠仅在印度部分地区使用](../Page/興渠.md "wikilink")。在全世界大部分地区和文化中最常见的调味料是[食盐](../Page/食盐.md "wikilink")。

各个地区可以用不同的调味料达到异曲同工的结果，例如东亚的[葱和欧洲的](../Page/葱.md "wikilink")[洋葱](../Page/洋葱.md "wikilink")、中国古代的[醋和西方古代的](../Page/醋.md "wikilink")[酸葡萄汁](../Page/酸葡萄汁.md "wikilink")。

同一种调味料在不同地区的用途可以截然不同。[肉桂类香料在东南亚](../Page/肉桂.md "wikilink")（以及[意大利某些菜肴中](../Page/意大利.md "wikilink")）用来给肉类调味，在欧美则是加入[甜品中](../Page/甜品.md "wikilink")。

在历史上，各地区之间的物产和文化交流也会改变上述习俗。在15世纪之前，[中国菜调味的辣味主要靠](../Page/中国菜.md "wikilink")[花椒](../Page/花椒.md "wikilink")，欧洲烹饪主要靠[胡椒](../Page/胡椒.md "wikilink")。[地理大发现将原产](../Page/地理大发现.md "wikilink")[美洲的](../Page/美洲.md "wikilink")[辣椒传播到其他地方](../Page/辣椒.md "wikilink")，成为主要的辣味调味料。由此在[清代中期](../Page/清代.md "wikilink")，中国确立了主要调味料——辣椒和[蔗糖的运用](../Page/蔗糖.md "wikilink")\[1\]。

## 调味料列表

  - 有效成分为简单化学品的：[食盐](../Page/食盐.md "wikilink")、[白糖](../Page/白糖.md "wikilink")、[味精](../Page/味精.md "wikilink")、[白醋](../Page/白醋.md "wikilink")
  - 发酵的调味料：
      - 酱类：[醬油](../Page/醬油.md "wikilink")、[酱](../Page/酱.md "wikilink")、[蝦油](../Page/蝦油.md "wikilink")、[魚露](../Page/魚露.md "wikilink")、[蝦醬](../Page/蝦醬.md "wikilink")、[豆豉](../Page/豆豉.md "wikilink")、[麵豉](../Page/麵豉.md "wikilink")、[甜面酱](../Page/甜面酱.md "wikilink")、[南乳](../Page/南乳.md "wikilink")、[腐乳](../Page/腐乳.md "wikilink")、[豆瓣醬](../Page/豆瓣醬.md "wikilink")、[味噌](../Page/味噌.md "wikilink")、[辣蠶豆瓣醬](../Page/辣蠶豆瓣醬.md "wikilink")
      - 酒类：[料酒](../Page/料酒.md "wikilink")、[酒釀](../Page/酒釀.md "wikilink")、[紅糟](../Page/紅糟.md "wikilink")、[味醂](../Page/味醂.md "wikilink")、[酿造醋](../Page/酿造醋.md "wikilink")
      - 油类：[麻油](../Page/麻油.md "wikilink")、[辣椒油](../Page/辣椒油.md "wikilink")、[花椒油](../Page/花椒油.md "wikilink")
  - 单一植物成分，鲜用：[葱](../Page/葱.md "wikilink")、[生薑](../Page/薑.md "wikilink")、[大蒜](../Page/大蒜.md "wikilink")、[洋葱](../Page/洋葱.md "wikilink")、[辣椒](../Page/辣椒.md "wikilink")、[蝦夷蔥](../Page/蝦夷蔥.md "wikilink")、[韭菜](../Page/韭菜.md "wikilink")、[香菜](../Page/香菜.md "wikilink")、[香芹](../Page/香芹.md "wikilink")、[辣根](../Page/辣根.md "wikilink")、[山葵](../Page/山葵.md "wikilink")、[白松露菌](../Page/白松露菌.md "wikilink")、[九層塔](../Page/九層塔.md "wikilink")、[破布子](../Page/破布子.md "wikilink")、[干葱](../Page/干葱.md "wikilink")、[韭黄](../Page/韭黄.md "wikilink")、[蒜苗](../Page/蒜苗.md "wikilink")
  - 单一植物成份，乾用（[香料](../Page/香料.md "wikilink")）：
      - 種子:
        [胡椒](../Page/胡椒.md "wikilink")、[八角](../Page/八角.md "wikilink")、[小茴香](../Page/小茴香.md "wikilink")、[大茴香](../Page/大茴香.md "wikilink")、[豆蔻](../Page/豆蔻.md "wikilink")、[芝麻](../Page/芝麻.md "wikilink")、[罂粟籽](../Page/罌粟.md "wikilink")、[芥末](../Page/芥末.md "wikilink")、[花生](../Page/花生.md "wikilink")、[芫荽籽](../Page/芫荽籽.md "wikilink")、[孜然](../Page/孜然.md "wikilink")
      - 果實:
        [花椒](../Page/花椒.md "wikilink")、[辣椒](../Page/辣椒.md "wikilink")、[胡椒](../Page/胡椒.md "wikilink")、[肉桂](../Page/肉桂.md "wikilink")、[陳皮](../Page/陳皮.md "wikilink")、[食茱萸](../Page/食茱萸.md "wikilink")、[罗望子](../Page/罗望子.md "wikilink")(Tamarind)、[草果](../Page/草果.md "wikilink")
      - 花:
        [丁香](../Page/丁香.md "wikilink")、[番红花](../Page/番红花.md "wikilink")、[姜花](../Page/姜花.md "wikilink")
      - 草:
        [蒔蘿](../Page/蒔蘿.md "wikilink")、[七葉蘭](../Page/七葉蘭.md "wikilink")
      - 葉:
        [月桂葉](../Page/月桂葉.md "wikilink")、[檸檬葉](../Page/檸檬.md "wikilink")、[薄荷](../Page/薄荷.md "wikilink")、[香草](../Page/香草.md "wikilink")、[百里香](../Page/百里香.md "wikilink")、[茶叶](../Page/茶叶.md "wikilink")、[迷迭香](../Page/迷迭香.md "wikilink")、[薰衣草](../Page/薰衣草.md "wikilink")、[藥用鼠尾草](../Page/藥用鼠尾草.md "wikilink")、[紫苏](../Page/紫苏.md "wikilink")、[香茅](../Page/香茅.md "wikilink")、[香椿](../Page/香椿.md "wikilink")、[海苔](../Page/海苔.md "wikilink")、[叻沙葉](../Page/叻沙葉.md "wikilink")、[香叶](../Page/香叶.md "wikilink")、[咖哩葉](../Page/咖哩葉.md "wikilink")、[班兰叶](../Page/班兰叶.md "wikilink")、[棕榈叶](../Page/棕榈叶.md "wikilink")、[香蕉叶](../Page/香蕉叶.md "wikilink")、[乌芭叶](../Page/乌芭叶.md "wikilink")、
      - 其他:
        [干姜](../Page/姜.md "wikilink")、[桂皮](../Page/桂皮.md "wikilink")、[甘草](../Page/甘草.md "wikilink")、[興渠](../Page/興渠.md "wikilink")、[南姜](../Page/南姜.md "wikilink")、[桂枝](../Page/桂枝.md "wikilink")、[沙姜](../Page/沙姜.md "wikilink")、[柴桂](../Page/柴桂.md "wikilink")、[黄姜](../Page/黄姜.md "wikilink")、[亚参皮](../Page/亚参皮.md "wikilink")、[辣椒干](../Page/辣椒干.md "wikilink")
  - 单一植物成分，液態：[玫瑰香水](../Page/玫瑰香水.md "wikilink")、[果汁](../Page/果汁.md "wikilink")、[花生油](../Page/花生油.md "wikilink")、[麻油](../Page/麻油.md "wikilink")、[椰浆](../Page/椰浆.md "wikilink")
  - 多种成分混合的：
      - 固体：[五香粉](../Page/五香粉.md "wikilink")、[十三香](../Page/十三香.md "wikilink")、[孜然粉](../Page/孜然.md "wikilink")、[黑胡椒粉](../Page/黑胡椒.md "wikilink")、[葛拉姆馬薩拉](../Page/葛拉姆馬薩拉.md "wikilink")（印度[咖哩粉](../Page/咖哩.md "wikilink")）、[七味粉](../Page/七味粉.md "wikilink")、[七色粉](../Page/七色粉.md "wikilink")、[梅子粉](../Page/梅子粉.md "wikilink")、[薑黃粉](../Page/薑黃.md "wikilink")、[辣椒粉](../Page/辣椒粉.md "wikilink")、[小茴香粉](../Page/小茴香粉.md "wikilink")
      - 流质：[番茄醬](../Page/番茄醬.md "wikilink")、[喼汁](../Page/喼汁.md "wikilink")、[滷水](../Page/滷水.md "wikilink")、[蠔油](../Page/蠔油.md "wikilink")、[海鮮醬](../Page/海鮮醬.md "wikilink")、[柱侯醬](../Page/柱侯醬.md "wikilink")、[XO醬](../Page/XO醬.md "wikilink")、[HP醬](../Page/HP醬.md "wikilink")、[沙茶醬](../Page/沙茶醬.md "wikilink")、[芝麻酱](../Page/芝麻酱.md "wikilink")、[辣椒酱](../Page/辣椒酱.md "wikilink")

## 注释

## 参考文献

  - 引用

## 外部链接

  -
## 参见

  -
  - [味觉](../Page/味觉.md "wikilink")

{{-}}

[調味料](../Category/調味料.md "wikilink")

1.