**谢和赓**（），[笔名张诚](../Page/笔名.md "wikilink")、张春熙等，[中国](../Page/中国.md "wikilink")[广西](../Page/广西.md "wikilink")[桂林人](../Page/桂林.md "wikilink")。中国作家、情报人员。

## 生平

谢毕业于[桂林中学](../Page/桂林中学.md "wikilink")，1933年在[北平中国大学求学时经](../Page/北平中国大学.md "wikilink")[宣侠父介绍秘密加入](../Page/宣侠父.md "wikilink")[中国共产党](../Page/中国共产党.md "wikilink")，旋奉命打入[察哈尔民众抗日同盟军](../Page/察哈尔民众抗日同盟军.md "wikilink")，先后任[冯玉祥和](../Page/冯玉祥.md "wikilink")[吉鸿昌的](../Page/吉鸿昌.md "wikilink")[秘书](../Page/秘书.md "wikilink")。察哈尔抗日同盟军失败后，谢持冯玉祥和[李济深的推荐信投奔](../Page/李济深.md "wikilink")[白崇禧](../Page/白崇禧.md "wikilink")，又由于谢父谢顺慈与白崇禧的岳父马健卿同是清末秀才，为世交，谢很快得到白崇禧的信任，成为其[机要秘书](../Page/机要秘书.md "wikilink")。

1942年，被[国民政府派往](../Page/国民政府.md "wikilink")[美国留学](../Page/美国.md "wikilink")，地下替中共从事情报和[统一战线工作](../Page/统一战线.md "wikilink")，并先后就读于[美国国际事务研究所和](../Page/美国国际事务研究所.md "wikilink")[西北大学](../Page/西北大学_\(美国伊利诺州\).md "wikilink")。妻子则留在国内，情人[王莹随同前往](../Page/王莹_\(电影演员\).md "wikilink")。1950年，结束与妻子的婚姻关系，后与王莹在美国结婚。1954年底，在[麦卡锡主义风潮中被美国移民局逮捕](../Page/麦卡锡主义.md "wikilink")，后被驱逐出境回国。任《世界知识》高级编辑兼欧美组组长等职。

1957年，谢在[反右运动中因提了](../Page/反右运动.md "wikilink")“[中南海应向老百姓开放](../Page/中南海.md "wikilink")”的意见被打为[右派](../Page/右派.md "wikilink")，流放[黑龙江省](../Page/黑龙江省.md "wikilink")，次年在[周恩来等人的营救下回到北京](../Page/周恩来.md "wikilink")。1967年，谢在[文化大革命中再次被逮捕](../Page/文化大革命.md "wikilink")，1974年获知妻子被迫害致死消息而[精神失常](../Page/精神失常.md "wikilink")，1975年在周恩来的营救下出狱。后在[外交部工作](../Page/中华人民共和国外交部.md "wikilink")。

## 家庭

谢和赓第二位妻子是演员、作家[王莹](../Page/王莹_\(电影演员\).md "wikilink")。两人于1930年代相识，1942年一同赴美。1951年2月22日，在美国纽约市政府登记结婚。

## 參見

[潛伏於中華民國國軍中的中共間諜列表](../Page/潛伏於中華民國國軍中的中共間諜列表.md "wikilink")

## 外部链接

  - \[<http://cpc.people.com.cn/GB/68742/70423/70424/6183932.html>　“少年党员”王莹的传奇人生\]

[Category:潛伏於中華民國國軍中的中共間諜](../Category/潛伏於中華民國國軍中的中共間諜.md "wikilink")
[Category:中华人民共和国间谍](../Category/中华人民共和国间谍.md "wikilink")
[Category:中华人民共和国外交部官员](../Category/中华人民共和国外交部官员.md "wikilink")
[Category:中華人民共和國編輯](../Category/中華人民共和國編輯.md "wikilink")
[Category:恢复中国共产党党籍者](../Category/恢复中国共产党党籍者.md "wikilink")
[Category:美國西北大學校友](../Category/美國西北大學校友.md "wikilink")
[Category:右派分子](../Category/右派分子.md "wikilink")
[Category:桂林人](../Category/桂林人.md "wikilink")
[H和赓](../Category/谢姓.md "wikilink")