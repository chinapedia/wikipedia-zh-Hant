**谢尔盖·米罗诺维奇·基洛夫**，（；）原名**谢尔盖·米洛诺维奇·柯斯特里科夫**，[苏联](../Page/苏联.md "wikilink")[布尔什维克革命者和早期重要领导人](../Page/布尔什维克.md "wikilink")。他曾在1926年至1934年担任[列宁格勒州委书记的职务](../Page/列宁格勒.md "wikilink")，任内在其办公室内被[列昂尼德·尼古拉耶夫开枪打死](../Page/列昂尼德·尼古拉耶夫.md "wikilink")。此遇刺事件直接触发了被称为[大清洗的恐怖镇压](../Page/大清洗.md "wikilink")，但真相至今仍扑朔迷离。

## 早年革命

基洛夫出生于[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")[烏爾茹姆](../Page/烏爾茹姆.md "wikilink")（位于今天[俄罗斯联邦](../Page/俄罗斯联邦.md "wikilink")[基洛夫州](../Page/基洛夫州.md "wikilink")）的一个穷人家中，原名「谢尔盖·米洛诺维奇·柯斯特里科夫」。他自小父母双亡，祖母把他养到7岁，然后把他送到了一个孤儿院。1904年，18岁的他信仰[马克思主义](../Page/马克思主义.md "wikilink")，加入了[俄国社会民主工党中的](../Page/俄国社会民主工党.md "wikilink")[布尔什维克派](../Page/布尔什维克.md "wikilink")。

1905年他参加了[俄国1905年革命被捕入狱](../Page/俄国1905年革命.md "wikilink")。出狱后于1906年和1909年三度因印刷非法刊物入狱。之后他搬到[高加索地区](../Page/高加索.md "wikilink")，以自由派报纸《捷列克报》撰稿人的身份进行工作，在北高加索领导布尔什维克党的政治工作，直到1917年[二月革命后](../Page/二月革命.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")[尼古拉二世逊位](../Page/尼古拉二世.md "wikilink")。在此期间他取了[笔名](../Page/笔名.md "wikilink")“基洛夫”。

## 地方领导

1917年[十月革命后](../Page/十月革命.md "wikilink")，他建立了[北高加索](../Page/北高加索.md "wikilink")[苏维埃政权](../Page/苏维埃.md "wikilink")，成了[阿斯特拉罕布尔什维克的军事长官](../Page/阿斯特拉罕.md "wikilink")，任阿斯特拉罕边疆区临时革命军事委员会主席和南方集团军革命军事委员会委员。之后的三年中他参加了[苏联国内战争](../Page/苏联国内战争.md "wikilink")，打退了[邓尼金的](../Page/邓尼金.md "wikilink")[哥萨克](../Page/哥萨克.md "wikilink")[白军的进攻](../Page/白军.md "wikilink")。1921年成为[阿塞拜疆共产党中央书记](../Page/阿塞拜疆共产党_\(1920年\).md "wikilink")，领导的[巴库油田的恢复建设](../Page/巴库.md "wikilink")。

在1920年代初，在苏共全党的大辩论中，他坚决支持[斯大林](../Page/斯大林.md "wikilink")，反对[托洛茨基和](../Page/托洛茨基.md "wikilink")[季诺维也夫](../Page/季诺维也夫.md "wikilink")。1926年到[列宁格勒接替季诺维也夫任州委书记](../Page/列宁格勒.md "wikilink")。在列宁格勒，他领导了当地的工业建设，强硬推行[农业集体化](../Page/农业集体化.md "wikilink")，拆除了几十座教堂，逮捕被认为是对抗[社会主义建设的](../Page/社会主义.md "wikilink")[知识分子](../Page/知识分子.md "wikilink")。同时，他口才出众，当时政治局委员中，敢于到工厂去直接面向工人讲演的，唯有他一个\[1\]。1930年，担任列宁格勒州委第一书记和[联共](../Page/联共.md "wikilink")（布）中央西北局书记。

## 政治新星

1930年代，基洛夫在苏共党内风头日盛。1930年7月13日，基洛夫在苏共中央全会上被选为政治局委员和中央委员会书记，进入苏联最高领导层。

在[柳京集团反对斯大林的处理会议中](../Page/柳京集团.md "wikilink")，基洛夫挺身而出，反对斯大林对他们处以极刑的决定。

1934年的中央委员会选举中，基洛夫得到最少的反对票3票，同时斯大林得到最多的292张反对票。政治局委员[卡冈诺维奇下令将选票销毁](../Page/卡冈诺维奇.md "wikilink")，宣布斯大林和基洛夫并列得到3张反对票。斯大林得以继任总书记，而基洛夫任政治局委员、党中央书记、中央组织局成员。斯大林曾建议基洛夫到[莫斯科工作](../Page/莫斯科.md "wikilink")，基洛夫没有同意。

回到列宁格勒之后，基洛夫还作主把一部分军队的库存粮食拿出来分给饥馑的工人。

[Sergei_Kirov_and_Joseph_Stalin,_1934.jpg](https://zh.wikipedia.org/wiki/File:Sergei_Kirov_and_Joseph_Stalin,_1934.jpg "fig:Sergei_Kirov_and_Joseph_Stalin,_1934.jpg")

## 遇刺和影响

1934年12月1日，一个因为工作安排不得志而多次上访的人，[列昂尼德·瓦西里耶维奇·尼古拉耶夫](../Page/列昂尼德·瓦西里耶维奇·尼古拉耶夫.md "wikilink")，畅通无阻地进入了平日戒备森严的列宁格勒市委所在地[斯莫尔尼宫](../Page/斯莫尔尼宫.md "wikilink")，用手枪将基洛夫打死。

这次刺杀的动机有争风吃醋（基洛夫很讨女人欢心，与多人有染）、[德国人主使](../Page/德国.md "wikilink")（尼古拉耶夫事前数次去德国[领事馆](../Page/领事馆.md "wikilink")，事发后次日德国领事回国再未赴苏联）、斯大林清除威胁等多种说法。

斯大林从一开始就显示出对此案的高度重视，他亲自前往列宁格勒参与调查和审讯，并且宣称此次暗杀是由托洛茨基及其反对派所策划，继而以此案为契机，开始了针对[布哈林](../Page/布哈林.md "wikilink")、[季诺维也夫和](../Page/季诺维也夫.md "wikilink")[加米涅夫等老布尔什维克的清洗](../Page/加米涅夫.md "wikilink")，史称“[大清洗](../Page/大清洗.md "wikilink")”。1936年8月，莫斯科审判了所谓“托洛斯基—季诺维也夫[恐怖中心](../Page/恐怖活动.md "wikilink")”，季诺维也夫、加米涅夫等人被判处死刑并立即执行。整个[大清洗中](../Page/大清洗.md "wikilink")，近百万人被公开或秘密处决，更多的人被送进[古拉格](../Page/古拉格.md "wikilink")，全苏联两百多万党员中近半数被开除出党。苏联红军肃反前的5名元帅，有3人被处决，只剩下[伏罗希洛夫和](../Page/伏罗希洛夫.md "wikilink")[布琼尼两人](../Page/布琼尼.md "wikilink")。各军区、海军、空军的司令员、集团军的军长等高级指挥官大部分被杀，海军舰队司令员只剩下1名。在415名师、旅长中，有296人被处决\[2\]。这为德国在[第二次世界大战中进攻苏联的](../Page/第二次世界大战.md "wikilink")[巴巴羅薩作戰创造了便利](../Page/巴巴羅薩作戰.md "wikilink")。

根据逃亡美国的苏联格别乌将军[亚历山大·奥尔洛夫回忆](../Page/亞歷山大·米哈伊洛維奇·奧爾洛夫.md "wikilink")，对基洛夫的暗杀从始至终是斯大林的特务头子[雅戈达所一手策划](../Page/雅戈达.md "wikilink")。[赫鲁晓夫上台后](../Page/赫鲁晓夫.md "wikilink")，在苏共二十大上，公开暗示是斯大林策划了这起暗杀事件。[戈尔巴乔夫执政期间曾组织人调查](../Page/戈尔巴乔夫.md "wikilink")，但时日已久，没有结论。

基洛夫死后苏联有多处地点以他命名，如列宁格勒芭蕾舞基洛夫剧院（现[马林斯基剧院](../Page/马林斯基剧院.md "wikilink")）、基洛夫州及其首府[基洛夫市](../Page/基洛夫_\(基洛夫州\).md "wikilink")、[卡卢加州的](../Page/卡卢加州.md "wikilink")[基洛夫市](../Page/基洛夫_\(卡卢加州\).md "wikilink")、[莫斯科地铁的](../Page/莫斯科地铁.md "wikilink")[基洛夫站](../Page/清塘站_\(莫斯科\).md "wikilink")（1935年至1990年）、[伏尔加格勒的基洛夫区](../Page/伏尔加格勒.md "wikilink")、列宁格勒的基洛夫工厂、基洛夫剧院（1935年至1992年，现[马林斯基剧院](../Page/马林斯基剧院.md "wikilink")），等等。另外，还有[基洛夫级核动力导弹巡洋舰](../Page/基洛夫级巡洋舰.md "wikilink")（[基洛夫级巡洋舰
(1936年)](../Page/基洛夫级巡洋舰_\(1936年\).md "wikilink")）、电子游戏[《红色警报2》中的](../Page/红色警报.md "wikilink")[基洛夫空艇](../Page/基洛夫空艇.md "wikilink")，等等。1939年在[巴库建造了一座巨大的基洛夫塑像](../Page/巴库.md "wikilink")，1991年阿塞拜疆独立后塑像被拆除。

## 著作

著作有《基洛夫言论集》，并参加了《苏联国内战争史》一书的编辑工作，同斯大林、[日丹诺夫一起审阅过](../Page/日丹诺夫.md "wikilink")《苏联近代史教程》、《苏联通史教程》，还发表过《关于苏联历史教科书提纲的意见》和《关于近代史教科书提纲的意见》等\[3\]。

## 参考文献

## 外部連結

  - [Киров Сергей
    Миронович](https://web.archive.org/web/20021002213950/http://www.hronos.km.ru/biograf/kirov.html)
    на сайте Хроноса
  - [Сергей
    Миронович](http://www.peoples.ru/state/statesmen/sergey_kirov/)
    на сайте peoples.ru

[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:紅旗勳章獲得者](../Category/紅旗勳章獲得者.md "wikilink")
[Category:阿塞拜疆蘇維埃社會主義共和國政治人物](../Category/阿塞拜疆蘇維埃社會主義共和國政治人物.md "wikilink")
[Category:蘇聯政治人物](../Category/蘇聯政治人物.md "wikilink")
[Category:俄國內戰人物](../Category/俄國內戰人物.md "wikilink")
[Category:蘇聯軍事人物](../Category/蘇聯軍事人物.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:俄国革命家](../Category/俄国革命家.md "wikilink")
[Category:俄羅斯共產主義者](../Category/俄羅斯共產主義者.md "wikilink")
[Category:遇刺身亡的政治人物](../Category/遇刺身亡的政治人物.md "wikilink")
[Category:俄羅斯遇刺身亡者](../Category/俄羅斯遇刺身亡者.md "wikilink")
[Category:蘇聯遇刺身亡者](../Category/蘇聯遇刺身亡者.md "wikilink")
[Category:在蘇聯被謀殺身亡者](../Category/在蘇聯被謀殺身亡者.md "wikilink")
[Category:基洛夫州人](../Category/基洛夫州人.md "wikilink")
[Category:安葬於克里姆林宮紅場墓園者](../Category/安葬於克里姆林宮紅場墓園者.md "wikilink")
[Category:老布尔什维克](../Category/老布尔什维克.md "wikilink")

1.  *<http://novel.jschina.com.cn/hanyuwenxue/zhentwx/zhentanwenxue64.htm>
     基洛夫被害*

2.
3.  *[为中华之崛起--纪念中国共产党成立80周年](http://www.cass.net.cn/zhuanti/y_party/yc/yc_c/yc_c_012.htm)
     基洛夫(1886--1934)*