**谢苗·康斯坦丁诺维奇·铁木辛哥**（[俄语](../Page/俄语.md "wikilink")：，[乌克兰语](../Page/乌克兰语.md "wikilink")：****，），[苏联军事家](../Page/苏联.md "wikilink")、苏联元帅，是1941年[苏德战争爆发时](../Page/苏德战争.md "wikilink")[苏联红军的高级指挥官](../Page/苏联红军.md "wikilink")，曾两次获得[苏联英雄荣誉称号](../Page/苏联英雄.md "wikilink")。

## 生平

1895年2月18日（[俄历](../Page/俄历.md "wikilink")2月6日），铁木辛哥生于[俄罗斯帝國](../Page/俄罗斯帝國.md "wikilink")[南比萨拉比亚](../Page/南比萨拉比亚.md "wikilink")（今属[乌克兰](../Page/乌克兰.md "wikilink")[敖德萨州](../Page/敖德萨州.md "wikilink")）富尔曼卡（Furmanca）镇的一个[农民家庭](../Page/农民.md "wikilink")，1915年被征入[沙俄军队参加](../Page/沙俄.md "wikilink")[第一次世界大战](../Page/第一次世界大战.md "wikilink")，是西方面军中的一名[骑兵](../Page/骑兵.md "wikilink")。1917年[二月革命爆发后](../Page/二月革命.md "wikilink")，他加入了革命派，后于1918年加入[红军](../Page/苏联红军.md "wikilink")，1919年加入[布爾什維克党](../Page/布爾什維克.md "wikilink")。

在[苏联国内战争期间](../Page/苏联国内战争.md "wikilink")，铁木辛哥参加了多条战线的作战，其中在[察里津](../Page/察里津.md "wikilink")（后改名[斯大林格勒](../Page/斯大林格勒.md "wikilink")）的作战使他认识[斯大林](../Page/斯大林.md "wikilink")，两人自此结为朋友，这为他在1920年代斯大林主政时期的迅速受到提拔奠定了基础。1920——1921年间，铁木辛哥在[谢苗·布琼尼的](../Page/谢苗·布琼尼.md "wikilink")[第一骑兵队中服役](../Page/第一骑兵队.md "wikilink")，他与布琼尼之后成为苏共内部受到斯大林青睐的所谓“骑兵派”的主要成员，掌握着红军的主要指挥权。

在国内战争和[波苏战争战争的末期](../Page/波苏战争.md "wikilink")，铁木辛哥已成为红军骑兵的总指挥。1930年代，他先后担任驻[白俄罗斯](../Page/白俄罗斯.md "wikilink")（1933年）、[基辅](../Page/基辅.md "wikilink")（1935年、1938年）、[北高加索](../Page/北高加索.md "wikilink")（1937年）、[哈尔科夫](../Page/哈尔科夫.md "wikilink")（1937年）等地红军的总司令。到1939年，其辖制范围已包括苏联的整个西部边境地区。在1939年与[德国联合](../Page/德国.md "wikilink")[武力瓜分波兰的战斗中](../Page/波兰战役.md "wikilink")，铁木辛哥担任了[乌克兰方面军总司令](../Page/乌克兰方面军.md "wikilink")。同年，他当选为[苏共中央委员会委员](../Page/苏共中央委员会.md "wikilink")。由于与斯大林的密交，铁木辛哥在[大清洗中未受到任何牵连](../Page/大清洗.md "wikilink")。

1940年1月，铁木辛哥接手[苏芬战争](../Page/苏芬战争.md "wikilink")，接替此前因指挥不力导致惨败的[克里门特·伏罗希洛夫任苏军总司令](../Page/克里门特·伏罗希洛夫.md "wikilink")。在其指挥下，苏军在付出惨重伤亡后成功突破了芬军位于[卡累利阿地峡的](../Page/卡累利阿地峡.md "wikilink")[曼纳海姆防线](../Page/曼纳海姆防线.md "wikilink")，迫使芬兰于3月向苏联求和。此役令铁木辛哥的声望上升，他于5月被任命为[苏联国防人民委员](../Page/苏联国防人民委员.md "wikilink")，并被授予[苏联元帅军衔](../Page/苏联元帅.md "wikilink")。

铁木辛哥属于传统型将领，在其任职期间，他在红军中恢复了沙俄时期军队的严酷纪律。但与当时蘇军中其他一些保守派将领相比，他具有较强的战略眼光和指挥能力。在他的力主之下，蘇军加快了军队机械化步伐，增置了大批[坦克](../Page/坦克.md "wikilink")，以应付预料逼近的[苏德战争](../Page/苏德战争.md "wikilink")。后来的战事证明，这些措施是非常及时的。

1941年6月22日，德军入侵苏联。同年7月，最高统帅部改组，斯大林接替了苏联国防人民委员一职。由于西方面军在巴甫洛夫大将指挥下惨败，形成巨大的战役突破口。铁木辛哥于1941年7月危急局势下接手[西方面军任司令员](../Page/西方面军.md "wikilink")，指挥在[斯摩棱斯克的撤退作战](../Page/斯摩棱斯克.md "wikilink")。尽管付出了巨大的代价，但铁木辛哥成功稳住了西方面军的局势，使得战线稳定到1941年9月底德军发动[莫斯科会战](../Page/莫斯科会战.md "wikilink")。1941年9月铁木辛哥调任[乌克兰前线](../Page/乌克兰.md "wikilink")，指挥刚刚在[基辅和](../Page/基辅.md "wikilink")[乌曼被歼灭](../Page/乌曼\(乌克兰\).md "wikilink")150万人的蘇军成功将战线稳定了下来。1941年12月，铁木辛克成功指挥了[罗斯托夫反击战](../Page/罗斯托夫战役_\(1941年\).md "wikilink")。

1942年5月铁木辛哥率领640,000名蘇军在[哈尔科夫发动反攻](../Page/哈尔科夫.md "wikilink")，这是苏联在参战后的首次主动进攻。虽然反攻首战告捷，但随后德军猛攻了蘇军突出的左翼，迅速瓦解了铁木辛哥的攻势。此役虽暂时减缓了德军向[斯大林格勒方向的推进速度](../Page/斯大林格勒.md "wikilink")，但铁木辛哥还是被迫承担了未能阻挡德军进攻的责任。

1941年12月[格奥尔基·朱可夫将军率领苏军取得莫斯科保卫战的胜利后](../Page/格奥尔基·朱可夫.md "wikilink")，斯大林认为[朱可夫能力在铁木辛哥之上](../Page/朱可夫.md "wikilink")，遂令朱可夫接替了铁木辛哥的前线指挥权。此后一直到[第二次世界大战结束](../Page/第二次世界大战.md "wikilink")，铁木辛哥先后担任斯大林格勒（1942年6月）、西北（1942年10月）、[列宁格勒](../Page/列宁格勒.md "wikilink")（1943年6月）、[高加索](../Page/高加索.md "wikilink")（1944年6月）和[波罗的海](../Page/波罗的海.md "wikilink")（1944年8月）方面军总指挥。作为大本营代表，1944年在罗马尼亚、1945年在匈牙利协调乌克兰第2、第3方面军作战。

二战结束后，铁木辛哥被重新任命为驻[白俄罗斯军区司令员](../Page/白俄罗斯军区.md "wikilink")（1946年3月）、[南乌拉尔军区司令员](../Page/南乌拉尔军区.md "wikilink")（1946年6月）、白俄罗斯军区司令员（1949年3月）。1960年4月起任[国防部总监察组总监荣誉职务](../Page/国防部总监察组.md "wikilink")，1961年兼任[战争退伍军人国家委员会主席](../Page/战争退伍军人国家委员会.md "wikilink")，1970年在莫斯科逝世。

## 荣誉

铁木辛哥曾于1940年和1965年两度被授予[苏联英雄荣誉](../Page/苏联英雄.md "wikilink")，他获得过的其他荣誉包括1945年获得的[胜利勋章](../Page/胜利勋章.md "wikilink")、5次[列宁勋章](../Page/列宁勋章.md "wikilink")、[十月革命勋章](../Page/十月革命勋章.md "wikilink")、5次[红旗勋章和](../Page/红旗勋章.md "wikilink")3次[苏沃洛夫勋章](../Page/苏沃洛夫勋章.md "wikilink")。以下為其所獲得榮譽一覽：

### 俄羅斯帝國

|                                                                                                                                                       |                   |
| ----------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
| [RUS_Georgievsky_Krest_2st_BAR.svg](https://zh.wikipedia.org/wiki/File:RUS_Georgievsky_Krest_2st_BAR.svg "fig:RUS_Georgievsky_Krest_2st_BAR.svg") | 聖喬治十字勳章（二級、三級及四級） |
|                                                                                                                                                       |                   |

### 蘇聯

|                                                                                                                                                                                                                                                                                                                    |                                                                                           |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------- |
| [Hero_of_the_Soviet_Union_medal.png](https://zh.wikipedia.org/wiki/File:Hero_of_the_Soviet_Union_medal.png "fig:Hero_of_the_Soviet_Union_medal.png")[Hero_of_the_Soviet_Union_medal.png](https://zh.wikipedia.org/wiki/File:Hero_of_the_Soviet_Union_medal.png "fig:Hero_of_the_Soviet_Union_medal.png") | [蘇聯英雄金星獎章](../Page/蘇聯英雄.md "wikilink")：1940年3月21日、1965年2月18日                              |
| [Ordervictory_rib.png](https://zh.wikipedia.org/wiki/File:Ordervictory_rib.png "fig:Ordervictory_rib.png")                                                                                                                                                                                                        | [勝利勳章](../Page/勝利勳章.md "wikilink")：1945年4月6日（第十一號）                                        |
| [Order_of_Lenin_ribbon_bar.png](https://zh.wikipedia.org/wiki/File:Order_of_Lenin_ribbon_bar.png "fig:Order_of_Lenin_ribbon_bar.png")                                                                                                                                                                          | [列寧勳章](../Page/列寧勳章.md "wikilink")：1938年2月22日、1940年3月21日、1945年2月21日、1965年2月18日、1970年2月18日 |
| [Order_october_revolution_rib.png](https://zh.wikipedia.org/wiki/File:Order_october_revolution_rib.png "fig:Order_october_revolution_rib.png")                                                                                                                                                                  | [十月革命勳章](../Page/十月革命勳章.md "wikilink")：1968年2月22日                                         |
| [Order_of_Red_Banner_ribbon_bar.png](https://zh.wikipedia.org/wiki/File:Order_of_Red_Banner_ribbon_bar.png "fig:Order_of_Red_Banner_ribbon_bar.png")                                                                                                                                                          | [紅旗勳章](../Page/紅旗勳章.md "wikilink")：1920年7月25日、1921年5月11日、1930年2月22日、1944年3月11日、1947年6月11日 |
| [Order_suvorov1_rib.png](https://zh.wikipedia.org/wiki/File:Order_suvorov1_rib.png "fig:Order_suvorov1_rib.png")                                                                                                                                                                                                 | [一級蘇沃洛夫勳章](../Page/蘇沃洛夫勳章.md "wikilink")：1943年9月10日、1944年9月12日、1945年4月27日                 |
|                                                                                                                                                                                                                                                                                                                    | 革命榮譽武器：1920年11月28日（紅旗軍刀）                                                                  |
| [Именная_шашка.png](https://zh.wikipedia.org/wiki/File:Именная_шашка.png "fig:Именная_шашка.png")                                                                                                                                                                                                                 | 榮譽武器 – 鑲有金色蘇聯國徽的配劍：1968年2月22日                                                             |
| [Defstalingrad.png](https://zh.wikipedia.org/wiki/File:Defstalingrad.png "fig:Defstalingrad.png")                                                                                                                                                                                                                  | [保衛史達林格勒獎章](../Page/保衛史達林格勒獎章.md "wikilink")                                              |
| [Defleningrad.png](https://zh.wikipedia.org/wiki/File:Defleningrad.png "fig:Defleningrad.png")                                                                                                                                                                                                                     | [保衛列寧格勒獎章](../Page/保衛列寧格勒獎章.md "wikilink")                                                |
| [Defkiev_rib.png](https://zh.wikipedia.org/wiki/File:Defkiev_rib.png "fig:Defkiev_rib.png")                                                                                                                                                                                                                       | 保衛基輔獎章                                                                                    |
| [Defcaucasus_rib.png](https://zh.wikipedia.org/wiki/File:Defcaucasus_rib.png "fig:Defcaucasus_rib.png")                                                                                                                                                                                                           | [保衛高加索獎章](../Page/保衛高加索獎章.md "wikilink")                                                  |
| [Capturebudapest_rib.png](https://zh.wikipedia.org/wiki/File:Capturebudapest_rib.png "fig:Capturebudapest_rib.png")                                                                                                                                                                                               | [攻克布達佩斯獎章](../Page/攻克布達佩斯獎章.md "wikilink")                                                |
| [CaptureOfViennaRibbon.png](https://zh.wikipedia.org/wiki/File:CaptureOfViennaRibbon.png "fig:CaptureOfViennaRibbon.png")                                                                                                                                                                                          | 攻克維也納獎章                                                                                   |
| [Liberationbelgrade_rib.png](https://zh.wikipedia.org/wiki/File:Liberationbelgrade_rib.png "fig:Liberationbelgrade_rib.png")                                                                                                                                                                                      | 解放布拉格獎章                                                                                   |
| [Victoryjapan_rib.png](https://zh.wikipedia.org/wiki/File:Victoryjapan_rib.png "fig:Victoryjapan_rib.png")                                                                                                                                                                                                        | [戰勝日本獎章](../Page/戰勝日本獎章.md "wikilink")                                                    |
| [OrderStGeorge4cl_rib.png](https://zh.wikipedia.org/wiki/File:OrderStGeorge4cl_rib.png "fig:OrderStGeorge4cl_rib.png")                                                                                                                                                                                            | [1941-1945年大衛國戰爭戰勝德國獎章](../Page/1941-1945年大衛國戰爭戰勝德國獎章.md "wikilink")                      |
| [20_years_of_victory_rib.png](https://zh.wikipedia.org/wiki/File:20_years_of_victory_rib.png "fig:20_years_of_victory_rib.png")                                                                                                                                                                                | [1941-1945年大衛國戰爭二十週年紀念獎章](../Page/1941-1945年大衛國戰爭二十週年紀念獎章.md "wikilink")                  |
| [20_years_saf_rib.png](https://zh.wikipedia.org/wiki/File:20_years_saf_rib.png "fig:20_years_saf_rib.png")                                                                                                                                                                                                      | [工農紅軍二十週年紀念獎章](../Page/工農紅軍二十週年紀念獎章.md "wikilink")                                        |
| [30_years_saf_rib.png](https://zh.wikipedia.org/wiki/File:30_years_saf_rib.png "fig:30_years_saf_rib.png")                                                                                                                                                                                                      | [蘇聯陸海軍三十週年紀念獎章](../Page/蘇聯陸海軍三十週年紀念獎章.md "wikilink")                                      |
| [40_years_saf_rib.png](https://zh.wikipedia.org/wiki/File:40_years_saf_rib.png "fig:40_years_saf_rib.png")                                                                                                                                                                                                      | [蘇聯武裝力量四十週年紀念獎章](../Page/蘇聯武裝力量四十週年紀念獎章.md "wikilink")                                    |
| [50_years_saf_rib.png](https://zh.wikipedia.org/wiki/File:50_years_saf_rib.png "fig:50_years_saf_rib.png")                                                                                                                                                                                                      | [蘇聯武裝力量五十週年紀念獎章](../Page/蘇聯武裝力量五十週年紀念獎章.md "wikilink")                                    |
| [Soviet_250th_Anniversary_Of_Leningrad_Ribbon.jpg](https://zh.wikipedia.org/wiki/File:Soviet_250th_Anniversary_Of_Leningrad_Ribbon.jpg "fig:Soviet_250th_Anniversary_Of_Leningrad_Ribbon.jpg")                                                                                                                | [列寧格勒建城兩百五十週年紀念獎章](../Page/列寧格勒建城兩百五十週年紀念獎章.md "wikilink")                                |
| [800thMoscowRibbon.png](https://zh.wikipedia.org/wiki/File:800thMoscowRibbon.png "fig:800thMoscowRibbon.png")                                                                                                                                                                                                      | [莫斯科建城八百週年紀念獎章](../Page/莫斯科建城八百週年紀念獎章.md "wikilink")                                      |
|                                                                                                                                                                                                                                                                                                                    |                                                                                           |

### 外國榮譽

|                                                                                                                                                                                                                   |                        |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- |
| [TCH_CS_Vojensky_Rad_Bileho_Lva_1st_(1945)_BAR.svg](https://zh.wikipedia.org/wiki/File:TCH_CS_Vojensky_Rad_Bileho_Lva_1st_\(1945\)_BAR.svg "fig:TCH_CS_Vojensky_Rad_Bileho_Lva_1st_(1945)_BAR.svg")       | 一級白獅軍事勳章（捷克斯洛伐克）       |
| [Med_XXXth_anniversary_of_chalkin_gol_victory_rib.PNG](https://zh.wikipedia.org/wiki/File:Med_XXXth_anniversary_of_chalkin_gol_victory_rib.PNG "fig:Med_XXXth_anniversary_of_chalkin_gol_victory_rib.PNG") | 諾門罕戰役三十週年獎章" （蒙古人民共和國） |
|                                                                                                                                                                                                                   |                        |

## 参考资料

  - [谢苗·铁木辛哥传记](http://www.warheroes.ru/hero/hero.asp?Hero_id=896)
  - [1940年苏芬战争中的铁木辛哥](http://armor.kiev.ua/army/hist/akt-vor-tim.shtml)

[Category:蘇聯英雄](../Category/蘇聯英雄.md "wikilink")
[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:紅旗勳章獲得者](../Category/紅旗勳章獲得者.md "wikilink")
[Category:十月革命勳章獲得者](../Category/十月革命勳章獲得者.md "wikilink")
[Category:蘇沃洛夫勳章獲得者](../Category/蘇沃洛夫勳章獲得者.md "wikilink")
[Category:勝利勳章獲得者](../Category/勝利勳章獲得者.md "wikilink")
[Category:苏联元帅](../Category/苏联元帅.md "wikilink")
[Category:蘇聯國防部長](../Category/蘇聯國防部長.md "wikilink")
[Category:苏联政治人物](../Category/苏联政治人物.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:蘇聯第二次世界大戰人物](../Category/蘇聯第二次世界大戰人物.md "wikilink")
[Category:波蘇戰爭人物](../Category/波蘇戰爭人物.md "wikilink")
[Category:俄國內戰人物](../Category/俄國內戰人物.md "wikilink")
[Category:烏克蘭裔蘇聯人](../Category/烏克蘭裔蘇聯人.md "wikilink")
[Category:烏克蘭裔俄羅斯人](../Category/烏克蘭裔俄羅斯人.md "wikilink")
[Category:敖德薩州人](../Category/敖德薩州人.md "wikilink")
[Category:安葬於克里姆林宮紅場墓園者](../Category/安葬於克里姆林宮紅場墓園者.md "wikilink")