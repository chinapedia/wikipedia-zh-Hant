**谭钟麟**（），[字文卿](../Page/表字.md "wikilink")，原名谭二监，[中国](../Page/中国.md "wikilink")[湖南](../Page/湖南.md "wikilink")[茶陵人](../Page/茶陵.md "wikilink")，[清朝末年政治人物](../Page/清朝.md "wikilink")，[中华民国政治家](../Page/中华民国.md "wikilink")[谭延闿之父](../Page/谭延闿.md "wikilink")。曾任两广总督，任內鎮壓了[杨衢云](../Page/杨衢云.md "wikilink")、[孫中山領導的](../Page/孫中山.md "wikilink")[第一次广州起义](../Page/第一次广州起义.md "wikilink")，下令當時的南海縣知縣審理[陸皓東等革命黨人的犯罪事實](../Page/陸皓東.md "wikilink")。

## 生平

谭钟麟于[咸丰六年](../Page/咸丰.md "wikilink")（1856年）中[进士](../Page/进士.md "wikilink")，入[翰林院任编修](../Page/翰林院.md "wikilink")。[同治年间外放](../Page/同治.md "wikilink")，历任多职。

同治十年(1871年)，因[左宗棠疏荐](../Page/左宗棠.md "wikilink")，出任[陕西](../Page/陕西.md "wikilink")[布政使](../Page/布政使.md "wikilink")，不久代理陕西[巡抚](../Page/巡抚.md "wikilink")。由于他保证了左宗棠进军[新疆期间的粮草供应](../Page/新疆.md "wikilink")，为平定[阿古柏之乱做出了贡献](../Page/阿古柏之乱.md "wikilink")，[光绪元年](../Page/光绪.md "wikilink")（1875年），实授陕西巡抚一职。

光绪五年（1879年），谭钟麟调任[浙江巡抚](../Page/浙江.md "wikilink")，在任期间整修了[文澜阁](../Page/文澜阁.md "wikilink")，有政绩。光绪七年（1881年），升任[陕甘总督](../Page/陕甘总督.md "wikilink")，在任内大力发展农业，使得当地经济有了长足发展。

光绪十四年（1888年），谭钟麟告病辞职，但三年以后即被再次征召入[京](../Page/北京.md "wikilink")，以[尚书衔任](../Page/尚书.md "wikilink")[吏部左](../Page/吏部.md "wikilink")[侍郎兼署](../Page/侍郎.md "wikilink")[户部左侍郎](../Page/户部.md "wikilink")，管理[国库](../Page/国库.md "wikilink")。

光绪十八年（1892年）升任[工部尚书](../Page/工部.md "wikilink")，不久调任[闽浙总督](../Page/闽浙总督.md "wikilink")。

光绪二十一年（1895年），调任[两廣總督](../Page/两廣總督.md "wikilink")。任內禁止了當時在廣州流行的一種[闈姓賭局](../Page/闈姓.md "wikilink")；也鎮壓了[孫中山領導的](../Page/孫中山.md "wikilink")[第一次广州起义](../Page/第一次广州起义.md "wikilink")，下令當時的南海縣知縣審理[陸皓東等革命黨人的犯罪事實](../Page/陸皓東.md "wikilink")。其间并多次上疏反对[戊戌变法](../Page/戊戌变法.md "wikilink")，受到[慈禧太后的赏识](../Page/慈禧太后.md "wikilink")。

光绪二十五年（1899年），谭钟麟因不满租借[新界](../Page/新界.md "wikilink")，以病告归。

光绪31年（1905年）病逝于[长沙](../Page/长沙.md "wikilink")，[谥文勤](../Page/谥.md "wikilink")。

## 參考文獻

  - [清史稿](../Page/清史稿.md "wikilink")

## 外部連結

  - [譚鍾麟](http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%C3%D3%C1%E9%C5%EF)
    中研院史語所

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝陝西布政使](../Category/清朝陝西布政使.md "wikilink")
[Category:清朝陝西巡撫](../Category/清朝陝西巡撫.md "wikilink")
[Category:清朝浙江巡撫](../Category/清朝浙江巡撫.md "wikilink")
[Category:清朝陝甘總督](../Category/清朝陝甘總督.md "wikilink")
[Category:清朝閩浙總督](../Category/清朝閩浙總督.md "wikilink")
[Category:清朝兩廣總督](../Category/清朝兩廣總督.md "wikilink")
[Category:茶陵人](../Category/茶陵人.md "wikilink")
[Z鍾](../Category/譚姓.md "wikilink")
[Category:諡文勤](../Category/諡文勤.md "wikilink")