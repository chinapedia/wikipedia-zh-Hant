**谯周**（），字**允南**，[巴蜀](../Page/巴蜀.md "wikilink")[巴西](../Page/巴西郡.md "wikilink")[西充](../Page/西充.md "wikilink")（今[四川](../Page/四川.md "wikilink")[阆中](../Page/阆中.md "wikilink")）人，[蜀汉学者](../Page/蜀汉.md "wikilink")。

## 生平

譙周幼贫喪父，与母、兄同居。少读典籍，精研六经，颇晓天文，為蜀地大儒之一，門下有[陳壽](../Page/陳壽.md "wikilink")、[羅憲](../Page/羅憲.md "wikilink")、[文立](../Page/文立.md "wikilink")、[李密](../Page/李密_\(西晉\).md "wikilink")、[杜軫等學生](../Page/杜軫.md "wikilink")。[诸葛亮做](../Page/诸葛亮.md "wikilink")[益州牧时](../Page/益州牧.md "wikilink")，任命他做劝学从事。诸葛亮死后，谯周前往奔丧，虽然朝廷随后下诏禁止奔丧，但谯周仍因行动迅速而得以到达。劉禪立太子[劉璿時任命他做太子僕](../Page/劉璿.md "wikilink")，調令家令，之後升任[中散大夫](../Page/中散大夫.md "wikilink")，[光禄大夫](../Page/光禄大夫.md "wikilink")。炎兴元年（263年）魏入侵蜀漢，劝[后主](../Page/后主.md "wikilink")[刘禅降](../Page/刘禅.md "wikilink")，遂被封为[阳城亭侯](../Page/阳城亭侯.md "wikilink")，迁[骑都尉](../Page/骑都尉.md "wikilink")，[散骑常侍](../Page/散骑常侍.md "wikilink")。

在蜀漢任官時期。見[姜維](../Page/姜維.md "wikilink")[多次北伐而虛耗蜀漢國力](../Page/姜維北伐.md "wikilink")，因而不滿，著《[仇國論](../Page/仇國論.md "wikilink")》力陳姜維北伐師旅數出之失弊。

西元265年，文立拜訪譙周。譙周因病說話不清楚，於是寫上：「典午忽兮，月酉沒兮。」典午指的是司馬，月酉代表八月的時候，結果[司馬昭果真在八月時病逝](../Page/司馬昭.md "wikilink")。

## 人物形象

在《[三國演義](../Page/三國演義.md "wikilink")》中，以譙周多以星相家和反戰者的角色出現。

  - 第65回隨[劉璋投降](../Page/劉璋.md "wikilink")[劉備](../Page/劉備.md "wikilink")。
  - 第80回[諸葛亮和譙周商議立劉備為帝](../Page/諸葛亮.md "wikilink")。
  - 第91回諸葛亮準備北伐，譙周以天文勸阻諸葛亮不被接納。
  - 第102回再勸阻諸葛亮停止北伐，仍不被接納。
  - 第105回譙周觀星看出諸葛亮逝世，報告[劉禪](../Page/劉禪.md "wikilink")。
  - 第112回[姜維準備北伐](../Page/姜維.md "wikilink")，譙周作《仇國論》勸阻不被接納。
  - 第115回再次勸阻姜維仍不被接納。
  - 第118回魏軍迫近，勸劉禪投降魏國。
  - 第119回隨劉禪降魏。

## 外表性格

身高八尺，相貌純扑，性格坦率，沒有辯才，但很有學識。

## 著作

《[仇國論](../Page/仇國論.md "wikilink")》：用因餘和肇建兩國的紛爭，由古喻今，提及[周文王](../Page/周文王.md "wikilink")，[勾踐以穩定而以小勝強](../Page/勾踐.md "wikilink")，[漢太祖處亂世而](../Page/漢太祖.md "wikilink")[張良以速戰戰勝](../Page/張良.md "wikilink")[項羽](../Page/項羽.md "wikilink")。認為蜀漢當時應與民休息，減少用兵。

撰《古史考》、《蜀本纪》、《论语注》、《五经然否论》等，均佚。

## 家庭

### 父

  - [譙𡸫](../Page/譙𡸫.md "wikilink")，字榮始，尚書學者，精通河圖緯書，[益州州府來召請他](../Page/益州.md "wikilink")，都沒有答應，州府於是任命他為師友從事。

### 子

  - [譙熙](../Page/譙熙.md "wikilink")
  - [譙賢](../Page/譙賢.md "wikilink")
  - [譙同](../Page/譙同.md "wikilink")：喜愛譙周的學習，被舉為孝廉，任錫縣縣令、東宮洗馬，被朝延徵召但沒有就任。

### 孫

  - [譙秀](../Page/譙秀.md "wikilink")，譙熙子，晉朝隱士。
  - [譙登](../Page/譙登.md "wikilink")，譙賢子，晉揚烈將軍、梓潼太守。

## 參考資料

  - 《三國志·蜀書·譙周傳》
  - [易中天『品三國』第42集談譙周誤國](https://www.youtube.com/watch?v=XFLU9Jit8y4)

[category:经学家](../Page/category:经学家.md "wikilink")
[category:三國歷史學家](../Page/category:三國歷史學家.md "wikilink")
[category:蜀漢政治人物](../Page/category:蜀漢政治人物.md "wikilink")
[category:閬中人](../Page/category:閬中人.md "wikilink")
[category:晉朝散騎常侍](../Page/category:晉朝散騎常侍.md "wikilink")
[Zhou周](../Page/category:譙姓.md "wikilink")

[Category:晉朝歷史學家](../Category/晉朝歷史學家.md "wikilink")