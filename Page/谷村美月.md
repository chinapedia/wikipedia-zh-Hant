**谷村
美月**，[日本女](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。[大阪府出身](../Page/大阪府.md "wikilink")，隸屬[HORI
AGENCY事務所](../Page/HORI_AGENCY.md "wikilink")。[身高](../Page/身高.md "wikilink")159[公分](../Page/公分.md "wikilink")，[血型O型](../Page/血型.md "wikilink")。興趣是[電影鑑賞](../Page/電影.md "wikilink")，特長是[舞蹈](../Page/舞蹈.md "wikilink")。\[1\]

## 人物簡介

  - 2002年，在[晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")『[滿天](../Page/滿天.md "wikilink")（）』作為女演員出道。
  - 2005年，初出演及初主演的電影『[Canaria](../Page/Canaria.md "wikilink")（）』獲第79回[電影旬報日本十佳電影第](../Page/電影旬報.md "wikilink")7位、第13屆[Raindance
    Film
    Festival](../Page/Raindance_Film_Festival.md "wikilink")（[倫敦](../Page/倫敦.md "wikilink")）Grand
    Prix獎，谷村自己也在第20回[高崎電影節中獲得最優秀新人獎](../Page/高崎電影節.md "wikilink")。
  - 2007年，憑電影『』在第2回[大阪Cinema
    Festival中獲得新人獎](../Page/大阪Cinema_Festival.md "wikilink")。
  - 2008年，憑電影『[檸檬時代](../Page/檸檬時代.md "wikilink")』、『[茶茶
    天涯的貴妃](../Page/茶茶_天涯的貴妃.md "wikilink")』及『[魍魎之匣](../Page/魍魎之匣.md "wikilink")』在第3回大阪Cinema
    Festival中獲得最佳女配角獎。

## 演出作品

### 電影

#### 真人演出

  - [Canaria](../Page/Canaria.md "wikilink")
    （2005年3月12日公映、[cinequanon](../Page/cinequanon.md "wikilink")）飾演新名由希（主演）

  - [東京喪波](../Page/東京喪波.md "wikilink")（2005年12月10日公映、[東芝Entertainment](../Page/東芝Entertainment.md "wikilink")）飾演

  - [歡笑大天使](../Page/歡笑大天使.md "wikilink")（或譯[冷笑大天使](../Page/冷笑大天使.md "wikilink")）（2006年7月15日公映、[Albatros
    Film](../Page/Albatros_Film.md "wikilink")）飾演沈丁花娘

  - [海與夕陽與她的眼淚 STRAWBERRY
    FIELDS](../Page/海與夕陽與她的眼淚_STRAWBERRY_FIELDS.md "wikilink")（2006年9月16日公映、[Video
    Planning](../Page/Video_Planning.md "wikilink")）飾演

  - （2006年7月25日公映）飾演（主演）

  - [red
    letters](../Page/red_letters.md "wikilink")（2006年9月23日公映、[frontmen
    entertainment](../Page/frontmen_entertainment.md "wikilink")）飾演掛橋的女兒

  - [銀河鐵道之夜 I carry a ticket of
    eternity](../Page/銀河鐵道之夜_I_carry_a_ticket_of_eternity.md "wikilink")（2006年11月4日公映、[KAERUCAFE](../Page/KAERUCAFE.md "wikilink")）飾演（主演）

  - [家庭的秘密](../Page/家庭的秘密.md "wikilink")（2006年12月2日公映、[SHIMA
    FILMS](../Page/SHIMA_FILMS.md "wikilink")）飾演櫻井典子

  - [酒井家的幸福](../Page/酒井家的幸福.md "wikilink")（2006年12月23日公映、[Bitters
    End](../Page/Bitters_End.md "wikilink")）飾演筒井秋

  - [檸檬時代](../Page/檸檬時代.md "wikilink")（）（2007年3月31日公映、[There's
    Enterprise](../Page/There's_Enterprise.md "wikilink")）飾演白田惠（主演）

  - [戀路物語 -each little
    thing-](../Page/戀路物語_-each_little_thing-.md "wikilink")（2007年5月12日首映）飾演（澤崎MIKI；主演）

  - [魍魎之匣](../Page/魍魎之匣.md "wikilink")（2007年12月22日公映、[SHOWGATE](../Page/SHOWGATE.md "wikilink")）飾演楠本賴子

  - [茶茶
    天涯的貴妃](../Page/茶茶_天涯的貴妃.md "wikilink")（2007年12月22日公映、[東映](../Page/東映.md "wikilink")）飾演[千姬](../Page/千姬.md "wikilink")

  - [奪命捉迷藏](../Page/奪命捉迷藏.md "wikilink")（2008年2月2日公映、[PHANTOM
    FILM](../Page/PHANTOM_FILM.md "wikilink")）飾演佐藤愛（主演）

  - [神之謎](../Page/神之謎.md "wikilink")（2008年6月7日公映、東映）飾演穗瑞沙羅華（主演）

  - （2008年8月30日公映）飾演（主演）

  - （2008年9月20日公映）飾演（主演）

  - [孩子的孩子](../Page/孩子的孩子.md "wikilink")（）（2008年9月27日公映、Bitters
    End）飾演持田秋美

  - （2009年5月9日公映）飾演成瀨汀（主演）

  - （2009年5月16日公映）飾演上田茜

  - [蟹工船](../Page/蟹工船.md "wikilink")（2009年7月4日公映）飾演

  - [BOX！](../Page/BOX！.md "wikilink")（2010年5月22日公映）飾演丸野智子

  - （2010年8月1日公映）飾演櫻美散（主演）

  - [十三刺客](../Page/十三刺客_\(2010年電影\).md "wikilink")（2010年9月25日公映）飾演牧野千世

  - （2010年9月25日公映）飾演須藤華

  - [偶然路過的街](../Page/偶然路過的街.md "wikilink")（2010年11月20日公映）飾演藤本江理

  - [海炭市敘景](../Page/海炭市敘景.md "wikilink") （2010年12月18日公映）飾演井川帆波

  - [阪急電車
    片道15分之奇蹟](../Page/阪急電車_\(小說\).md "wikilink")（2011年4月29日公映）飾演權田原美帆

  - [HESOMORI](../Page/HESOMORI.md "wikilink")（2011年9月3日公映）飾演

  - [逆轉裁判](../Page/逆轉裁判.md "wikilink")（2012年2月11日公映、[東寶](../Page/東寶株式會社.md "wikilink")）飾演大澤木夏美

  - [Salvage
    Mice](../Page/Salvage_Mice.md "wikilink")（2012年3月24日公映、[T-JOY](../Page/T-JOY.md "wikilink")）飾演有棲川真唯（Salvage
    Mice）（主演）

  - [東京無印女子物語](../Page/東京無印女子物語.md "wikilink")（2012年6月16日公映、[Best
    Brain](../Page/Best_Brain.md "wikilink")）飾演（主演）

  - （2012年7月7日公映、[東京劇場](../Page/東京劇場.md "wikilink")）飾演女義工

  - （2012年9月13日公映、[KlockWorx](../Page/KlockWorx.md "wikilink")）飾演高林光（主演）

  - 「人妻」（2012年9月29日公映、[角川映畫](../Page/角川映畫.md "wikilink")）飾演淺野年子（主演）

  - （2012年11月17日公映、PHANTOM FILM）飾演關由美子

  - [半徑3公里的世界](../Page/半徑3公里的世界.md "wikilink")（2013年3月2日公映、[映像產業振興機構](../Page/映像產業振興機構.md "wikilink")）飾演優香

  - [向陽處的她](../Page/向陽處的她.md "wikilink")（2013年10月12日公映、東寶、[Asmik
    Ace](../Page/Asmik_Ace.md "wikilink")）飾演峯岸百合

  - [ARCANA](../Page/ARCANA.md "wikilink")（2013年10月19日公映、[日活](../Page/日活.md "wikilink")）飾演友近三枝子

  - （2013年11月9日公映、[SEDIC
    International](../Page/SEDIC_International.md "wikilink")、[電通](../Page/電通.md "wikilink")）

  - （2013年12月14日公映、東寶）飾演長濱美和子

  - （2014年3月29日公映、松竹） 飾演

  - [FLARE](../Page/FLARE.md "wikilink")（2014年4月26日公映、[EYE-MOTION](../Page/EYE-MOTION.md "wikilink")）

  - [Sweet
    Poolside](../Page/Sweet_Poolside.md "wikilink")（2014年6月14日公映、松竹MEDIA事業部）
    飾演北友里子

  - （2014年6月21日公映、東寶映像事業部）飾演竹田富美枝

  - （2014年7月12日公映、[樂映舍](../Page/樂映舍.md "wikilink")）飾演桃黑瞳

  - [幕末高校生](../Page/幕末高校生.md "wikilink")（2014年7月26日公映、東映）飾演千代

  - （2014年8月2日公映、[Ippo](../Page/Ippo.md "wikilink")）飾演（主演）

  - [幻肢](../Page/幻肢_\(小說\).md "wikilink")（2014年9月27日公映、[D-rights](../Page/D-rights.md "wikilink")）飾演糸永遙

  - [深夜食堂](../Page/深夜食堂.md "wikilink")（2015年1月31日公映、東映）飾演

  - [白河夜船](../Page/白河夜船_\(小說\)#電影.md "wikilink")（2015年4月25日公映、[COPIAPOA
    FILM](../Page/COPIAPOA_FILM.md "wikilink")）飾演

  - （2015年5月30日公映、松竹）飾演櫻井麻衣

  - （2015年10月3日公映、PHANTOM FILM）飾演小澤早苗

  - [火星異種](../Page/火星異種#電影.md "wikilink")（2016年4月29日公映、[華納兄弟電影](../Page/華納兄弟電影.md "wikilink")）

  - （2016年6月4日公映、東映）飾演辰見洋子

  - （2016年8月27日公映、[Toki
    Entertainment](../Page/Toki_Entertainment.md "wikilink")）飾演山咲佳奈

  - （2018年3月3日公映、[Aniplex](../Page/Aniplex.md "wikilink")）飾演館林弓

  - [飛上天空的輪胎](../Page/飛上天空的輪胎.md "wikilink")（2018年6月15日公映、松竹） 飾演柚木妙子

#### 聲音演出

  - [劇場版動畫
    穿越時空的少女](../Page/穿越時空的少女.md "wikilink")（2006年7月15日公映、[角川先驅映畫](../Page/角川先驅映畫.md "wikilink")）聲演藤谷果穗

  - [夏日大作戰](../Page/夏日大作戰.md "wikilink")（2009年8月1日公映）聲演池澤佳主馬

  - （2009年8月22日公映）聲演美穗

  - [狼的孩子雨和雪](../Page/狼的孩子雨和雪.md "wikilink")（2012年7月21日公映、[東寶](../Page/東寶株式會社.md "wikilink")）聲演土肥太太

  - [怪物的孩子](../Page/怪物的孩子.md "wikilink")（2015年7月11日公映、東寶）

### 電視劇

  - [晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")
    [滿天](../Page/滿天.md "wikilink")（）（[日本放送協會](../Page/日本放送協會.md "wikilink")、2002年
    - 2003年）飾演穗積未海（首演作品）

  - [真實的恐怖故事](../Page/真實的恐怖故事.md "wikilink") 夏之特別篇2004
    「孤單的少女」（[富士電視台](../Page/富士電視台.md "wikilink")、2004年）飾演三田村美雪

  - 真實的恐怖故事（第2輯） 「走到窗戶那兒的少女」（富士電視台、2004年11月22日）飾演（主演）

  - [11通之…未能拿出來的情信](../Page/11通之…未能拿出來的情信.md "wikilink")
    第10通「」（[朝日電視台](../Page/朝日電視台.md "wikilink")、2005年）飾演飯田早紀

  - 戰後60年特別企劃電視劇
    [祖國](../Page/祖國_\(電視劇\).md "wikilink")（[WOWOW](../Page/WOWOW.md "wikilink")、2005年）飾演小野寺江梨

  - [生物彗星WoO](../Page/生物彗星WoO.md "wikilink")（日本放送協會、2006年）飾演（主演）

  - [街頭律師](../Page/街頭律師.md "wikilink")（或譯[平民律師](../Page/平民律師.md "wikilink")）（第5集、第6集（大結局））（[NHK綜合頻道](../Page/NHK綜合頻道.md "wikilink")、2006年）飾演深川友香（客串演出）

  - [14歲的母親](../Page/14歲的母親.md "wikilink")（[日本電視台](../Page/日本電視台.md "wikilink")、2006年）飾演柳澤真由那

  - （[每日放送](../Page/每日放送.md "wikilink")、2007年）飾演（主演）

  - （富士電視台、2007年）（主演）

  - [與她遊玩的正確方法](../Page/與她遊玩的正確方法.md "wikilink")（）（朝日電視台、2007年）飾演西園寺靜香

  - [我們的教科書](../Page/我們的教科書.md "wikilink")（富士電視台、2007年）飾演仁科朋美

  - （第2、3集）（NHK綜合頻道、2007年）飾演杉原友美惠（客串演出）

  - （[關西電視台](../Page/關西電視台.md "wikilink")、2007年10月15日）飾演谷村美月（主演）

  - [無止境的殺人](../Page/無止境的殺人.md "wikilink")（）（WOWOW、2007年）飾演三室直美

  - [初戀net.com〜不能被遺忘的戀愛之歌〜](../Page/初戀net.com〜不能被遺忘的戀愛之歌〜.md "wikilink")（）（第3集）（朝日電視台、2007年）飾演

  - [轉瞬為風](../Page/轉瞬為風.md "wikilink")（富士電視台、2008年）飾演鳥澤圭子

  - （富士電視台、2008年）飾演春奈（主演）

  - [潘朵拉](../Page/潘朵拉_\(電視劇\).md "wikilink")（WOWOW、2008年）飾演水野愛美
    [相關資訊](https://web.archive.org/web/20080403063925/http://www.wowow.co.jp/dramaw/pandora/top.html)

  - [貓街](../Page/貓街.md "wikilink")（日本放送協會、2008年）飾演青山惠都（主演）

  - [太陽與海的教室](../Page/太陽與海的教室.md "wikilink")（富士電視台、2008年）飾演澤水羽菜

  - [藤子·F·不二雄的Parallel
    Space](../Page/藤子·F·不二雄的Parallel_Space.md "wikilink")
    征地球論（WOWOW、2008年）飾演鈴木美咲（主演）

  - （富士電視台、2008年）（主演）

  - [必殺仕事人2009](../Page/必殺仕事人2009.md "wikilink")（朝日電視台、2009年）飾演如月

  - [我的帥管家](../Page/我的帥管家.md "wikilink")（富士電視台、2009年）飾演山田多美

  - [手機搜查官7](../Page/手機搜查官7.md "wikilink")（大結局）（[東京電視台](../Page/東京電視台.md "wikilink")、2009年）

  - [面探型男](../Page/面探型男.md "wikilink")（Episode 7）（日本電視台、2009年）飾演

  - （富士電視台、2009年）飾演崎山里香（主演）

  - [無家可歸的中學生2](../Page/無家可歸的中學生.md "wikilink")（富士電視台、2009年4月12日）飾演奧寺奈央

  - [育子互動](../Page/育子互動.md "wikilink")（第12集）（每日放送、2009年）飾演山口結子

  - [MW 第0章
    〜惡魔的遊戲〜](../Page/MW_\(漫畫\).md "wikilink")（日本電視台、2009年6月30日）飾演（主演）

  - （富士電視台、2009年）飾演佐藤由香

  - （朝日電視台、2009年）飾演園田奈央（主演）

  - 〜LOVE STORIES VI〜「」（日本電視台、2009年9月25日）飾演（主演）

  - [戀歌Drama Special](../Page/戀歌Drama_Special.md "wikilink")
    三日月（第二夜）（[東京廣播公司](../Page/東京廣播公司.md "wikilink")、2009年9月30日）飾演島田遙夏（主演）

  - （富士電視台、2009年）飾演（主演）

  - [武士高校](../Page/武士高校.md "wikilink")（第3集）（日本電視台、2009年）飾演戰國時代的兒媳（客串演出）

  - [Real
    Clothes](../Page/Real_Clothes.md "wikilink")（大結局）（[關西電視台](../Page/關西電視台.md "wikilink")、2009年）飾演

  - （日本電視台、2009年）

  - [W之悲劇](../Page/W之悲劇.md "wikilink")（東京廣播公司、2010年）飾演和辻摩子

  - （[BS富士](../Page/BS富士.md "wikilink")、2010年）飾演寺野繪子

  - [青山1seg開發](../Page/青山1seg開發.md "wikilink") （第3集「」）（[NHK
    1seg2](../Page/1seg2.md "wikilink")、2010年）（主演）

  - 特備劇 [必殺仕事人2010](../Page/必殺仕事人2010.md "wikilink")（朝日電視台、2010年）飾演如月

  - [Strawberry
    Night](../Page/草莓之夜_\(電視劇\).md "wikilink")（試播版）（富士電視台、2010年11月13日）飾演深澤由香里

  - （東京電視台、2010年）飾演吉岡小春

  - [醫龍-Team Medical
    Dragon-](../Page/醫龍-Team_Medical_Dragon-.md "wikilink")3（富士電視台、2010年）飾演真柄冬實

  - （日本放送協會、2010年）飾演

  - （第1集）（NHK綜合頻道、2010年）

  - （第1集）（[讀賣電視台](../Page/讀賣電視台.md "wikilink")、2011年）飾演三崎優子

  - [世界奇妙物語](../Page/世界奇妙物語.md "wikilink")21世紀第21年特別篇「PETS」（富士電視台、2011年5月14日）飾演櫻（主演）

  - [堺市廣報節目](../Page/堺市.md "wikilink")
    （[大阪電視台](../Page/大阪電視台.md "wikilink")、2011年9月4日
    - 25日）飾演花子（主演）

  - [宮部美幸Special](../Page/宮部美幸.md "wikilink") （富士電視台、2011年9月9日）飾演高木和子

  - （[NHK大阪放送局](../Page/NHK大阪放送局.md "wikilink")、2011年9月16日）飾演小林優花

  - （第3集）（NHK綜合頻道、2011年9月27日）飾演

  - 關西特集劇 （NHK大阪放送局、2011年10月10日）飾演里美／

  - [造花之蜜](../Page/造花之蜜.md "wikilink")（WOWOW、2011年11月27日）飾演小杉康美

  - 堺市廣報節目 （大阪電視台、2011年11月27日 - 12月18日）飾演花子（主演）

  - [湊佳苗Mystery](../Page/湊佳苗.md "wikilink")
    [境遇](../Page/境遇_\(湊佳苗\).md "wikilink")（[朝日放送](../Page/朝日放送.md "wikilink")、2011年12月3日）飾演高倉陽子（17年前）

  - （日本放送協會、2012年）飾演加賀山妃美佳、加賀山鞠香（兩個角色）

  - [Hungry！](../Page/Hungry！.md "wikilink")（第4集）（關西電視台、2012年）飾演美咲

  - [科調所的女法研](../Page/科調所的女法研.md "wikilink")（第11系列第15集、大結局）（朝日電視台、2012年）飾演崎本菜津美

  - （日本放送協會、2012年3月26日）飾演磯崎-{叶}-江（主演）

  - 特備劇 （[BS-TBS](../Page/BS-TBS.md "wikilink")、2012年3月28日）飾演安達加奈

  - [SPEC〜翔〜](../Page/SPEC〜警視廳公安部公安第五課_未詳事件特別對策係事件簿〜.md "wikilink")（東京廣播公司、2012年4月1日）飾演久遠望

  - （讀賣電視台、2012年）飾演（主演）

  - [Platina
    Town](../Page/Platina_Town.md "wikilink")（WOWOW、2012年）飾演山田春奈

  - （關西電視台、2012年11月24日）飾演星野千秋（主演）

  - （日本電視台、2012年12月17日 - 19日）飾演

  - [八重之櫻](../Page/八重之櫻.md "wikilink")（日本放送協會、2013年）飾演[小田時榮](../Page/小田時榮.md "wikilink")

  - [LAST HOPE](../Page/LAST_HOPE.md "wikilink")（第7集）（富士電視台、2013）飾演西村杏子

  - （大結局）（TBS電視台、2013年）飾演田端翔子

  - [Woman](../Page/Woman_\(電視劇\).md "wikilink")（日本電視台、2013年）飾演砂川藍子

  - （朝日電視台、2013年7月27日）飾演

  - [律政狂人2](../Page/Legal_high#第二季.md "wikilink")（第2集）（富士電視台、2013年）飾演玉川玉

  - （富士電視台、2013年10月25日）飾演高城累

  - （富士電視台、2013年12月9日 - 20日）飾演長濱美和子

  - [新春WIDE時代劇](../Page/新春WIDE時代劇.md "wikilink")
    [影舞者德川家康](../Page/影舞者德川家康.md "wikilink")（東京電視台、2014年1月2日）飾演

  - [黑暗中的獵人](../Page/黑暗中的獵人.md "wikilink")（；2014年版）（[時代劇専門頻道](../Page/時代劇専門頻道.md "wikilink")、2014年10月4日
    - 5日）飾演

  - [告別自己](../Page/告別自己.md "wikilink")（日本放送協會、2014年）

  - （第3集）（讀賣電視台、2014年）飾演野村未步

  - （第1、16集）（[LaLa
    TV](../Page/LaLa_TV.md "wikilink")、2014年11月4日、2015年2月24日）飾演

  - [深夜食堂](../Page/深夜食堂.md "wikilink")（第24、30集）（[MBS電視台](../Page/MBS電視台.md "wikilink")、2014年）飾演

  - [松本清張〜霧之旗](../Page/霧之旗.md "wikilink")（朝日電視台、2014年12月7日）飾演工藤信子

  - （東京電視台、2014年12月30日）飾演阿部友子

  - （[NHK BS
    Premium](../Page/NHK_BS_Premium.md "wikilink")、2015年1月3日）飾演柏木文香

  - [電視未來遺產](../Page/電視未來遺產.md "wikilink") （TBS電視台、2015年1月19日）飾演木村優子

  - [獻給阿爾吉儂的花束](../Page/獻給阿爾吉儂的花束.md "wikilink")（2015年版）（TBS電視台、2015年）飾演河口梨央

  - 真實的恐怖故事「」（2015年8月29日）飾演早瀨知美

  - （朝日電視台、2015年）飾演武市富子

  - [地域發電視劇](../Page/地域發電視劇.md "wikilink") （NHK BS
    Premium、2015年10月28日）飾演伊藤加奈（主演）

  - [產科醫鴻鳥](../Page/產科醫鴻鳥#電視劇.md "wikilink")（第8集）（TBS電視台、2015年）飾演土屋真紀

  - （第1集）（關西電視台、2016年）飾演

  - 晨間小說連續劇 [別嬪小姐](../Page/別嬪小姐.md "wikilink")（日本放送協會、2016年 -
    2017年）飾演小野明美

  - 大阪環狀線每個站的戀愛故事Part 2（第9集）（關西電視台、2017年）飾演

  - [孤獨的美食家Season
    6](../Page/孤獨的美食家#電視劇.md "wikilink")（第3集）（東京電視台、2017年）飾演女店員

  - （[名古屋電視台](../Page/名古屋電視台.md "wikilink")／[神奈川電視台](../Page/神奈川電視台.md "wikilink")／[千葉電視台](../Page/千葉電視台.md "wikilink")／[KBS京都](../Page/KBS京都.md "wikilink")／[SUN電視台](../Page/SUN電視台.md "wikilink")／[長野放送](../Page/長野放送.md "wikilink")／、2017年）飾演臼井幸（主演）

  - （東京電視台、2017年）飾演內海優

  - [99.9
    -刑事專門律師-](../Page/99.9_-刑事專門律師-.md "wikilink")（第2季，第1集）（TBS電視台、2018年）飾演鈴木加代

  - （）（富士電視台、2018年）飾演河村禮菜

  - （）（第7集 - 大結局）（朝日電視台，2018年）飾演百百瀨佐智

  - [遺留搜查](../Page/遺留搜查.md "wikilink")（第5季，第2集）（朝日電視台、2018年）飾演若名友子

  - [相棒](../Page/相棒.md "wikilink")（第17季，第1 - 2集）（朝日電視台、2018年）飾演鬼束祥

  - 大阪環狀線每個站的戀愛故事Part 4（第3集）（關西電視台、2018年10月27日）飾演渚

  - （第5集「天城峠殺人事件」）（TBS電視台、2019年1月7日）飾演渚小林朝美（女主角）

<!-- end list -->

  - （第1集）（朝日電視台、2019年）飾演野宮涼子

### 其他電視節目

  - [CINEMA通信](../Page/CINEMA通信.md "wikilink")「亞細亞美女特集」（[東京電視台](../Page/東京電視台.md "wikilink")、2006年10月5日）

  - [夏歌2007](../Page/夏歌.md "wikilink")（[NHK綜合頻道](../Page/NHK綜合頻道.md "wikilink")、2007年8月14日）（於節目中的短劇中演出）飾演

  - （[富士電視台](../Page/富士電視台.md "wikilink")、2008年5月20日）

<!-- end list -->

  -
    在節目的其中一個環節「[Telephone
    Shock](../Page/Telephone_Shock.md "wikilink")」中作嘉賓演出。

<!-- end list -->

  - 夏歌2008（NHK綜合頻道、2008年7月26日）

  - （富士電視台、2008年9月21日）

<!-- end list -->

  -
    與同樣在電影《》演出的[中越典子一同擔任節目的嘉賓](../Page/中越典子.md "wikilink")。

<!-- end list -->

  - [新堂本兄弟](../Page/新堂本兄弟.md "wikilink")（富士電視台、2008年9月21日）

<!-- end list -->

  -
    在節目中作首次公開演唱。

<!-- end list -->

  - [來自偵探X的挑戰書！](../Page/來自偵探X的挑戰書！.md "wikilink")、來自偵探X的挑戰書！2（[日本放送協會](../Page/日本放送協會.md "wikilink")、2009年）

  - Dear Woman
    電影中的她與朋友們（[日本電影專門頻道](../Page/日本電影專門頻道.md "wikilink")、2009年）

  - （日本放送協會、2009年8月）

  - （2009年9月14日）

  - [NONFICTION W](../Page/NONFICTION_W.md "wikilink")
    （[WOWOW](../Page/WOWOW.md "wikilink")、2010年）

  - （[NHK教育頻道](../Page/NHK教育頻道.md "wikilink")、2010年）

  - （[關西電視台](../Page/關西電視台.md "wikilink")、2010年）

  - （NHK綜合頻道、2011年）旁述演出

  - [1seg Lunchbox](../Page/1seg_Lunchbox.md "wikilink")（[NHK
    1seg2](../Page/NHK_1seg2.md "wikilink")、2011年）

  - （NHK 1seg2、NHK教育頻道、2011年）

  - [another
    sky](../Page/another_sky.md "wikilink")（[日本電視台](../Page/日本電視台.md "wikilink")、2011年）

  - （season6）（NHK綜合頻道、2010年5月 - 9月）

  - [RICOH](../Page/理光.md "wikilink") presents
    （[BS朝日](../Page/BS朝日.md "wikilink")、2011年9月24日、10月1日）

  - （關西電視台、2011年10月7日 - 21日）飾演旅客

  - 「」（日本放送協會、2012年2月13日）說故事演出

  - （再現劇：川崎篇）（[NHK BS
    Premium](../Page/NHK_BS_Premium.md "wikilink")、2012年8月11日）飾演川崎節子

  - （日本放送協會、2012年10月9日）飾演嚮導

  - （日本放送協會、2013年6月14日）旁述演出

### 錄影帶首映作品

  - [pieces of love](../Page/pieces_of_love.md "wikilink")
    「」（2008年）飾演主角（主演）

### 舞台劇

  - （2009年）飾演早峰（主演）

  - [NYLON 100℃](../Page/NYLON_100℃.md "wikilink") 35th SESSION
    「」（2010年）

  - （2011年）飾演民谷蓮

  - 騒音歌舞伎-Rock Musical-
    「[我的四谷怪談](../Page/我的四谷怪談.md "wikilink")」（2012年）飾演

  - （[新國立劇場](../Page/新國立劇場.md "wikilink")、2013年6月4 - 23日）

  - [歌姬](../Page/歌姬_\(舞台劇\).md "wikilink")（2014年再演版）（[赤坂ACT
    Theater](../Page/赤坂ACT_Theater.md "wikilink")、2014年3月21日 - 30日）飾演岸田鈴

### 電子遊戲

  - （2009年）聲演

### 廣告

#### 電視廣告及宣傳片

  - [德島製粉](../Page/德島製粉.md "wikilink")「」（2001年）
  - [小僧壽司本部](../Page/小僧壽司本部.md "wikilink")「2001年夏之CAMPAIGN」（2001年）
  - [Unilever Japan](../Page/Unilever_Japan.md "wikilink")（2002年）
  - [NEC](../Page/NEC.md "wikilink") Refresh
    PC「IT、de、ECHO」「返回的父女」篇（2004年 - 2005年）
  - [MPA](../Page/日本國際電影著作權協會.md "wikilink")「盜版撲滅運動」電影（2004年 - ）
  - [倍樂生](../Page/倍樂生.md "wikilink")「進研Seminar中學生講座 測驗之前一刻」篇（2006年）
  - [TOMBOW鉛筆](../Page/TOMBOW鉛筆.md "wikilink")（2006年 - 2007年）
      - 「與文具同在的人」篇
      - 「橡皮擦」篇
  - [佳能](../Page/佳能.md "wikilink")「Face Catch Technology」製品技術廣告（2007年 -
    2008年）
  - [NEXON Japan](../Page/Nexon.md "wikilink") [-{zh:楓之谷; zh-hans:冒险岛;
    zh-hant:楓之谷; zh-sg:MapleStory;}-](../Page/楓之谷.md "wikilink")（2007年 -
    2008年）
      - 「轉校」篇 [相關片段](http://www.youtube.com/watch?v=kYkpwxrwy78)
      - 「畢業」篇 [相關片段](http://www.youtube.com/watch?v=lygwsnJqPaA)
      - 「三人面談」篇 [相關片段](http://www.youtube.com/watch?v=G6E1IW-USEk)
      - 「同級生」篇 [相關片段](http://www.youtube.com/watch?v=OEimr8VaTk0)
      - TV CM Campaign 2008
        [相關片段](http://www.youtube.com/watch?v=0y2IelXJ20E)
  - [avex
    entertainment](../Page/avex_entertainment_\(藝能事務所\).md "wikilink")（2008年）
      - 「H2O」
      - [奥村初音](../Page/奥村初音.md "wikilink")「謝謝」（「戀、花火」音樂錄影帶中的片段）
        [相關片段](http://hk.video.yahoo.com/video/video.html?id=606110)
  - [SOFTBANK MOBILE](../Page/SOFTBANK_MOBILE.md "wikilink")（2008年）
      - 「白戶家 3年犬組」篇
      - 「SOFTBANK」篇
  - [JP日本郵便](../Page/郵便事業.md "wikilink")「賀年卡活動」（2008年）
      - 「數數張數（購買）」篇 [相關片段](http://www.youtube.com/watch?v=O7pNt1qkDUo)
      - 「請告訴我地址（製作）」篇 [相關片段](http://www.youtube.com/watch?v=fqF7SjrohCY)
      - 「早出門」篇
  - [北海道電力](../Page/北海道電力.md "wikilink")「」（2010年）
      - 「」篇
      - 「」篇
      - 「」篇
  - [第一三共Healthcare](../Page/第一三共Healthcare.md "wikilink") CAKONAL2
    V顆粒（2010年）
  - [久光製藥](../Page/久光製藥.md "wikilink")  「實感」篇（2013年1月 - ）
  - [NatureLab](../Page/NatureLab.md "wikilink") LITS Series（2013年4月 - ）

#### 電台廣告

  - 日本[麥當勞](../Page/麥當勞.md "wikilink")「」（2010年）

#### 硬照廣告

  - [全國防犯協會聯合會](../Page/全國防犯協會聯合會.md "wikilink")／[社會安全研究財團等等](../Page/社會安全研究財團.md "wikilink")「防止濫用藥物」海報、單張（2006年）
  - [Tower Records](../Page/Tower_Records.md "wikilink")「No life, No
    music.」海報（2007年）
  - [Mutow](../Page/Mutow.md "wikilink")
  - [HIRAKI](../Page/HIRAKI.md "wikilink")
  - [京阪百貨店](../Page/京阪百貨店.md "wikilink")
  - [近鐵百貨店](../Page/近鐵百貨店.md "wikilink")
  - [JP日本郵便](../Page/郵便事業.md "wikilink")「賀年卡活動」（2008年）
  - [警察廳](../Page/警察廳.md "wikilink")「」參加者招募用海報（2010年）
  - 第17回[統一地方選舉](../Page/統一地方選舉.md "wikilink") 神奈川縣投票参加啟發海報（2011年）
  - [堺市選舉管理委員會](../Page/堺市.md "wikilink")「堺市長選舉投票啟發海報」（2013年）

### 互聯網劇集

  - 澀谷怪談 的都市傳說（第8、14傳說）（[Geneon
    Entertainment](../Page/Geneon_Entertainment.md "wikilink")／[Ampersand
    Broadband](../Page/Ampersand_Broadband.md "wikilink")、2004年公映）飾演真帆
  - HAVE YOUR MEASURE THE SHORT
    MOVIE（[富士電視台傳訊](../Page/富士電視台.md "wikilink")、2007年）飾演辻里香（主演）
      - 第1集 identity（2007年6月8日公映）
      - 第2集 metamorphosis（2007年6月22日公映）
  - [人生補時](../Page/人生補時.md "wikilink") 貓篇（富士電視台傳訊、2009年）飾演（主演）

<!-- end list -->

  - [I am GHOST](../Page/I_am_GHOST.md "wikilink")（[NTT
    DoCoMo](../Page/NTT_DoCoMo.md "wikilink")、[Bee
    TV](../Page/Bee_TV.md "wikilink")、2009年10月20日 - ）飾演佐佐木愛（主演）

  - （2009年）飾演主角（主演）

  - （NTT DoCoMo動畫、2011年）飾演（主演）

  - （[FRESHNESS BURGER](../Page/FRESHNESS_BURGER.md "wikilink")
    WEB、2012年11月10日 - ）（主演）

  - [Love Concierge](../Page/Love_Concierge.md "wikilink")（）
    第1週「」（[NOTTV](../Page/NOTTV.md "wikilink")、2012年11月19日 -
    23日）飾演（主演）

  - （[Hulu](../Page/Hulu.md "wikilink")、2015年11月13日 - ）飾演高峰美智子

### 其他互聯網節目

  - OCN免費動畫節目 Talking Japan Vol.086（2008年5月 - ）
  - [motteco書店](../Page/motteco書店.md "wikilink") 訪問（2008年11月25日）

### 音樂錄影帶

  - [森大輔](../Page/森大輔_\(唱作歌手\).md "wikilink")「moonlight」（[WARNER MUSIC
    JAPAN](../Page/WARNER_MUSIC_JAPAN.md "wikilink")、2005年8月24日發布）

  - [UNDER GRAPH](../Page/UNDER_GRAPH.md "wikilink")「」（2006年）

  - [林直次郎](../Page/平川地一丁目.md "wikilink")「」（電影『』中的片段）（2007年）

  - [奧村初音](../Page/奧村初音.md "wikilink")「[戀、花火](../Page/戀、花火.md "wikilink")」（2007年）

  - [Dreams Come True](../Page/Dreams_Come_True.md "wikilink")「」（2007年）

  - [KOTOKO](../Page/KOTOKO.md "wikilink")「[奪命捉迷藏](../Page/奪命捉迷藏_\(KOTOKO\).md "wikilink")」（電影『[奪命捉迷藏](../Page/奪命捉迷藏.md "wikilink")』中的片段）（2007年）

  - 「Cassini」（2008年）

  - [清水翔太](../Page/清水翔太.md "wikilink")「」（2009年）

  - [岡平健治](../Page/岡平健治.md "wikilink")「」（2011年）

  - [GUMMY](../Page/GUMMY.md "wikilink")「」（2013年）

### DVD

  - Making Special（2009年）擔任旁白

<!-- end list -->

  -

    的DVD映像特典。

## 出版作品

### 寫真集

  - 谷村美月寫真集《花美月》（[關惠美](../Page/關惠美.md "wikilink")、[集英社](../Page/集英社.md "wikilink")、2008年1月25日發售、ISBN
    978-4-0878-0483-6）
  - 谷村美月寫真集《LANKA》（BOMB特別編集）（[長野博文](../Page/長野博文.md "wikilink")、[學習研究社](../Page/學習研究社.md "wikilink")、2010年9月22日發售、ISBN
    978-4-0540-4646-7）
  - 谷村美月寫真集《FAKE》（[熊谷貫](../Page/熊谷貫.md "wikilink")、[WANI
    BOOKS](../Page/WANI_BOOKS.md "wikilink")、2012年6月18日發售、ISBN
    978-4-8470-4465-6）

### 連載作品

  - 1分鐘的紀實文學！主演谷村美月（[共同通信社](../Page/共同通信社.md "wikilink")、2009年1月 - 12月）

### 其他

  - 谷村美月 2012年月曆（[TRY-X](../Page/TRY-X.md "wikilink")、2011年10月22日發售、ISBN
    978-4-7774-8119-4）

## 外部連結

  - [所屬事務所網站中的官方個人資料](http://www.horiagency.co.jp/talent/tanimura/index.html)
  - [官方網誌](http://www.47news.jp/mitsuki)

## 參考資料

<div class="references-small">

<references />

</div>

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")

1.  [Yahoo日本-谷村美月資料](https://archive.is/20120715075557/talent.yahoo.co.jp/talent/16/w05-0527.html)