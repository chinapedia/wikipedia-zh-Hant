**豐原交流道**為台灣[國道一號](../Page/國道一號.md "wikilink")[中山高速公路的交流道](../Page/中山高速公路.md "wikilink")，雖名豐原，但實際地理位置為[台中市](../Page/台中市.md "wikilink")[神岡區](../Page/神岡區.md "wikilink")，里程為168k。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW1.svg" title="fig:TWHW1.svg">TWHW1.svg</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_PHW10.svg" title="fig:TW_PHW10.svg">TW_PHW10.svg</a></p></td>
<td><p><strong>神岡</strong> <strong>豐原</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 鄰近交流道

[Category:台中市交流道](../Category/台中市交流道.md "wikilink")
[Category:神岡區](../Category/神岡區.md "wikilink")