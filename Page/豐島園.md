[Toshimaen_entrance.jpg](https://zh.wikipedia.org/wiki/File:Toshimaen_entrance.jpg "fig:Toshimaen_entrance.jpg")

**豐島園**（），是一家位於[日本](../Page/日本.md "wikilink")[東京的](../Page/東京.md "wikilink")[主題樂園](../Page/主題樂園.md "wikilink")，座落於[東京市](../Page/東京市.md "wikilink")[練馬區](../Page/練馬區.md "wikilink")[向山](../Page/向山.md "wikilink")，於1926年開業，在[第二次世界大戰期間停業](../Page/第二次世界大戰.md "wikilink")，於1946年恢復營業，目前由[西武集團的豐島園公司營運](../Page/西武集團.md "wikilink")。

## 歷史

### 前史

[室町時代](../Page/室町時代.md "wikilink")，豊島氏在這裡築城，是為練馬城。與太田氏交戰後，練馬城被廢。之後，為板橋的「なべや」所有，再之後被時為[上練馬村長的増田所入手](../Page/上練馬村.md "wikilink")。

[明治時代後期](../Page/明治.md "wikilink")，練馬城址被改為豊島公園。

[1916年](../Page/1916年.md "wikilink")（[大正](../Page/大正.md "wikilink")6年），[樺太工業](../Page/樺太工業.md "wikilink")（後來的[王子製紙](../Page/王子製紙_\(初代\).md "wikilink")）專務[藤田好三郎由於自身静養的需要](../Page/藤田好三郎.md "wikilink")，買入[石神井川南側的](../Page/石神井川.md "wikilink")12,000坪土地持有\[1\]。這段時間在這裡建造家宅，休息日則成為其家族到訪的地點\[2\]。

[1922年](../Page/1922年.md "wikilink")（大正12年），藤田在建造住居的同時，又再追加入手了六千坪土地\[3\]。

[1925年](../Page/1925年.md "wikilink")（大正14年）秋
，藤田好三郎買入石神井川北側的18,000坪土地\[4\]。

### 開園後

  - [1926年](../Page/1926年.md "wikilink")（[大正](../Page/大正.md "wikilink")15年）[9月15日](../Page/9月15日.md "wikilink")
    - 部分開園。
  - [1927年](../Page/1927年.md "wikilink")（[昭和](../Page/昭和.md "wikilink")2年）[4月29日](../Page/4月29日.md "wikilink")
    - 舉行日本庭園的開園式。
  - [1944年](../Page/1944年.md "wikilink")（昭和19年）[4月](../Page/4月.md "wikilink")
    -
    由於[太平洋戰爭戰況惡化](../Page/太平洋戰爭.md "wikilink")，一時閉園。此時園内的練馬城舊跡，被設置了一個軍事設施（練馬監視哨）。
  - [1946年](../Page/1946年.md "wikilink")（昭和21年）[3月](../Page/3月.md "wikilink")
    - 再度開始營業。
  - [1951年](../Page/1951年.md "wikilink")（昭和26年）[1月31日](../Page/1月31日.md "wikilink")
    -
    株式會社豊島園臨時股東總會宣佈公司解散し，其事業決議由[西武鉄道株式会社繼承](../Page/西武鉄道株式会社.md "wikilink")\[5\]。
  - [1952年](../Page/1952年.md "wikilink")（昭和27年）[9月](../Page/9月.md "wikilink")
    - 豊島園旅館開業。
  - [1963年](../Page/1963年.md "wikilink")（昭和38年）[8月23日](../Page/8月23日.md "wikilink")
    - 豊島園股份有限公司解散，其事業由西武鉄道株式会社正式繼承。

}}</ref>。

## 外部連結

  - [豐島園網站](http://www.toshimaen.co.jp/index.html)

[Category:東京都觀光地](../Category/東京都觀光地.md "wikilink")
[Category:日本主題公園](../Category/日本主題公園.md "wikilink")
[Category:練馬區](../Category/練馬區.md "wikilink")

1.  内山正雄、蓑茂寿太郎『東京の遊園地』[財団法人東京都公園協会発行](../Page/東京都公園協会.md "wikilink")、1981年

2.
3.
4.
5.  同年[2月7日](../Page/2月7日.md "wikilink")、官報第7221号にて公告