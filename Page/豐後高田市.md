**豐後高田市**（）是位于[日本](../Page/日本.md "wikilink")[大分縣北部](../Page/大分縣.md "wikilink")[國東半島上的一個](../Page/國東半島.md "wikilink")[城市](../Page/城市.md "wikilink")，接鄰[周防灘](../Page/周防灘.md "wikilink")，生活圈屬於[中津都市圈](../Page/中津都市圈.md "wikilink")。

由於國東半島的地形是以中央的[兩子山為中心](../Page/兩子山.md "wikilink")，向外以放射狀延伸出[山谷與](../Page/山谷.md "wikilink")[山脈](../Page/山脈.md "wikilink")，因此轄區內主要的市區都位於各山谷間河川及出海口附近。\[1\]

## 歷史

1196年[高田重定在此建設了](../Page/高田重定.md "wikilink")[高田城](../Page/高田城_\(豐後國\).md "wikilink")，
1632年成為[松平重直的](../Page/松平重直.md "wikilink")[龍王藩的一部分](../Page/龍王藩.md "wikilink")，並在居城遷移至高田時改名為[高田藩](../Page/高田藩_\(豐後國\).md "wikilink")。

1669年則改成為[島原藩的](../Page/島原藩.md "wikilink")[飛地領地](../Page/飛地.md "wikilink")，並在此設有[島原領豐州陣屋](../Page/島原領豐州陣屋.md "wikilink")。

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬**高田町**、玉津村、來繩村、美和村、河內村、東都甲村、西都甲村、草地村、吳崎村、田染村、西真玉村、中真玉村、上真玉村、臼野村、岬村、三浦村、三重村。
  - 1895年7月12日：玉津村改制為玉津町。
  - 1907年4月1日：高田町、玉津町、來繩村、美和村合併為新設置的**高田町**。
  - 1919年1月1日：岬村改制並改名為[香香地町](../Page/香香地町.md "wikilink")。
  - 1941年4月1日：西真玉村和中真玉村合併為真玉村。
  - 1951年4月1日：高田町、河內村、東都甲村、西都甲村、草地村合併為新設置的**高田町**。
  - 1954年3月31日：
      - 吳崎村被併入高田町。
      - 真玉村、上真玉村、臼野村合併為真玉村。
  - 1954年5月10日：高田町改名為豐後高田町。
  - 1954年5月31日：田染村被併入豐後高田町，同時豐後高田町改制為**豐後高田市**。
  - 1954年8月31日：香香地町、三浦村、三重村合併為新設置的香香地町。
  - 1955年1月1日：真玉村改制為[真玉町](../Page/真玉町.md "wikilink")。
  - 1955年3月31日：封戶村的水崎地區被併入豐後高田市（封戶村的其他地區與宇佐町、北馬城村合併為新設置的[宇佐町](../Page/宇佐町.md "wikilink")）。
  - 2005年3月31日：豐后高田市與[西國東郡](../Page/西國東郡.md "wikilink")[真玉町](../Page/真玉町.md "wikilink")、[香香地町](../Page/香香地町.md "wikilink")[合併為新設置的新豐後高田市](../Page/市町村合併.md "wikilink")。\[2\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1988年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>高田町</p></td>
<td><p>1907年4月1日<br />
高田町</p></td>
<td><p>1951年4月1日<br />
高田町</p></td>
<td><p>1954年5月10日<br />
改名為豐後高田町</p></td>
<td><p>1954年5月31日<br />
豐後高田市</p></td>
<td><p>2005年3月31日<br />
豐後高田市</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>玉津村</p></td>
<td><p>1895年7月12日<br />
玉津町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>來繩村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>美和村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>河內村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>東都甲村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>西都甲村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>草地村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>吳崎村</p></td>
<td><p>1954年3月31日<br />
併入高田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>田染村</p></td>
<td><p>1954年5月31日<br />
併入豐後高田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>西真玉村</p></td>
<td><p>1941年4月1日<br />
真玉村</p></td>
<td><p>1954年3月31日<br />
真玉村</p></td>
<td><p>1955年1月1日<br />
真玉町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>中真玉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>上真玉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>臼野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>岬村</p></td>
<td><p>1919年1月1日<br />
改制並改名為香香地町</p></td>
<td><p>1954年8月31日<br />
香香地町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>三浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>三重村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 觀光資源

[Showa_no_Machi_09.jpg](https://zh.wikipedia.org/wiki/File:Showa_no_Machi_09.jpg "fig:Showa_no_Machi_09.jpg")之町的新町街[商業街](../Page/商業街.md "wikilink")\]\]

  - [富貴寺](../Page/富貴寺.md "wikilink")
  - [熊野磨崖佛](../Page/熊野磨崖佛.md "wikilink")
  - [真木大堂](../Page/真木大堂.md "wikilink")
  - [昭和之町](../Page/昭和之町.md "wikilink")

### 祭典、活動

  - [修正鬼會](../Page/修正鬼會.md "wikilink")
  - [ホーランエンヤ](../Page/ホーランエンヤ.md "wikilink")
  - [若宮八幡秋季大祭 裸祭](../Page/若宮八幡秋季大祭_裸祭.md "wikilink")

## 教育

### 高等學校

  - [大分縣立高田高等學校](../Page/大分縣立高田高等學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [島原市](../Page/島原市.md "wikilink")（[長崎縣](../Page/長崎縣.md "wikilink")）：於1969年4月25日締結為兄弟都市。

## 本地出身之名人

  - [一松定吉](../Page/一松定吉.md "wikilink")：[政治家](../Page/政治家.md "wikilink")
  - [金田政彦](../Page/金田政彦.md "wikilink")：前[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [石井均](../Page/石井均.md "wikilink")：[藝人](../Page/藝人.md "wikilink")
  - [片多徳郎](../Page/片多徳郎.md "wikilink")：[西洋畫家](../Page/西洋畫家.md "wikilink")
  - [方倉陽二](../Page/方倉陽二.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
  - [南次郎](../Page/南次郎.md "wikilink")：[大日本帝國陸軍](../Page/大日本帝國陸軍.md "wikilink")[軍人](../Page/軍人.md "wikilink")、[二次大戰](../Page/二次大戰.md "wikilink")[甲級戰犯](../Page/甲級戰犯.md "wikilink")
  - [江口章子](../Page/江口章子.md "wikilink")：詩人

## 參考資料

## 外部連結

  - [豐後高田市觀光協會](http://www2.city.bungotakada.oita.jp/kankou/)

<!-- end list -->

1.

2.