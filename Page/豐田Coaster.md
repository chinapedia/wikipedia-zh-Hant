**豐田Coaster**（****）是一款由[豐田汽車公司所出產的](../Page/豐田汽車公司.md "wikilink")[小型巴士](../Page/小型巴士.md "wikilink")。Coaster設有多個版本，有短軸版（16座位，6.2米長）、長軸版（20-24座位，6.9米長）以及超長軸版（23-28座位，7.7米長）。

因為豐田Coaster的可靠性及成本效益皆高，豐田Coaster在多個國家或地區，尤其是[亞洲和](../Page/亞洲.md "wikilink")[中東地區十分常見](../Page/中東地區.md "wikilink")，如在香港幾乎所有小巴營運商都使用豐田Coaster。

## 名稱差異

豐田Coaster的名稱會因各地的稱呼而異。在[港澳台地區](../Page/港澳台.md "wikilink")，一般會直接名為「豐田Coaster」；在[中國內地](../Page/中國內地.md "wikilink")，豐田Coaster則會被直接翻譯為「豐田柯斯達」或「豐田-{考斯特}-」；在[歐洲地區](../Page/歐洲.md "wikilink")，豐田Coaster因採用[葡萄牙Salvador](../Page/葡萄牙.md "wikilink")
Caetano設計的車身，名為「Salvador Caetano Optimo」。

## 歷史

### 第一代－U10／B10系列（1969年－1982年）

[Mullaways_Medical_CannaBus.JPG](https://zh.wikipedia.org/wiki/File:Mullaways_Medical_CannaBus.JPG "fig:Mullaways_Medical_CannaBus.JPG")
首代豐田Coaster於1969年推出，車身採用四盞圓形車頭燈的設計，配上左右向上翹起的防撞欄柵，並附設一對霧燈。

### 第二代－B20／B30系列（1982年－1992年）

[Toyota_Coaster_003.JPG](https://zh.wikipedia.org/wiki/File:Toyota_Coaster_003.JPG "fig:Toyota_Coaster_003.JPG")
豐田於1982年對Coaster作出大幅度改良，最明顯之處為新車加設空氣調節系統，冷氣從藏於車底的空調系統送出，再經由隱藏管道將冷氣送到車頂兩側的冷氣槽，每列座位之上設有獨立送風口，乘客可自由調校風向及風量，大大提升乘坐舒適度；車身亦經過重新設計，其中車頭燈分別有四盞圓形或方形的設計；車門更可選擇手動或自動驅動模式；此版本亦成為[港澳台地區予以細分的](../Page/港澳台.md "wikilink")「第一型」（圓形頭燈）以及「第二型」（方形頭燈）Coaster。

1985年，豐田Coaster加上[全自動變速系統以及長](../Page/自動變速器.md "wikilink")[軸距版本供用家選擇](../Page/軸距.md "wikilink")。

### 第三代－B40／B50系列（1992年－2017年）

[New_Territories_Minibus_Route_91.JPG](https://zh.wikipedia.org/wiki/File:New_Territories_Minibus_Route_91.JPG "fig:New_Territories_Minibus_Route_91.JPG")
[NT_MINIBUS_96B.JPG](https://zh.wikipedia.org/wiki/File:NT_MINIBUS_96B.JPG "fig:NT_MINIBUS_96B.JPG")
[2001-2007_Toyota_Coaster_bus_01.jpg](https://zh.wikipedia.org/wiki/File:2001-2007_Toyota_Coaster_bus_01.jpg "fig:2001-2007_Toyota_Coaster_bus_01.jpg")
[Toyota_Coaster_of_China_TV_389-WB_20150926.jpg](https://zh.wikipedia.org/wiki/File:Toyota_Coaster_of_China_TV_389-WB_20150926.jpg "fig:Toyota_Coaster_of_China_TV_389-WB_20150926.jpg")

  - 1992年12月，豐田再推出全新設計的Coaster。新系列分別有標準軸距的3200亳米以及長3935亳米兩種，並有16座位、20座位、26座位以及29座位等多種配搭可供選擇；另外車身亦捨棄以往方正的外觀，改用較圓渾的設計，並附設四盞圓形車頭燈；1997年出廠的16座位Coaster的開門桿設在變速桿左邊，原意是增加司機的活動空間，不過這設計劣評如潮，自此往後的16座位Coaster的開門桿設在變速桿右邊；此版本是為[港澳台地區稱呼的](../Page/港澳台.md "wikilink")「第三型」Coaster。
  - 1998年，豐田對Coaster作出改良，引擎因應平成10年氣體排放法規，更換成更環保的型號，車頭燈亦一併改為兩盞呈梯形的石英頭燈；是為[港澳台地區稱呼的](../Page/港澳台.md "wikilink")「第四型」Coaster。
  - 2002年，為響應環保，豐田推出以[液化石油氣作為燃料的Coaster](../Page/液化石油氣.md "wikilink")，並只提供石油氣版的標準軸距Coaster局限用家選擇，長軸距版本則仍然提供柴油或石油氣版本；此版本是為[港澳台地區稱呼的](../Page/港澳台.md "wikilink")「第五型」Coaster。於同年推出28座版本。
  - 2007年，豐田再對Coaster作出多項改良，其中引擎改配符合最新的歐盟四型標準引擎，同時將六前速手動變速更改為五前速；擋風玻璃以下部分的設計經過改良，改用更光亮的鑽石頭燈，透氣口的面積亦被加大，防撞欄柵上附設兩盞圓形霧燈，車側更加設指揮燈；車廂方面，錶板面積被加大，錶板上的按鈕編排變得更為整齊，車門亦加上蜂鳴器，當車門未完全關上會發出聲響提醒司機；此版本是為[港澳台地區稱呼的](../Page/港澳台.md "wikilink")「第六型」Coaster。由此代起配備符合歐盟四型或五型標準引擎的Coaster在行駛一段距離後便會亮起指示燈，提示化合物儲存器將近儲滿，須停車空轉，將未完全燃燒的化合物燃燒，俗稱「燒碳」，每次大約須時15至20分鐘，期間不能行駛及須保持引擎空轉，完成後指示燈便會關掉。
  - 2008年，豐田Coaster重新提供配上柴油引擎的標準軸距版本供用家選擇。
  - 2013年開始，Coaster在左、右兩邊車身近裙腳的位置各加裝了三粒長方形的紅色反光板，車廂內下車門的裙腳由銀色鐵片改為黑色的塑膠片。
  - 2015年開始，Coaster的座位有所更改，並且由兩點式安全帶改為三點式安全帶。
  - 2016年日本豐田車廠宣佈2017年1月全面停產第3代的Coaster（出口版本另行日期），在2017年1月23日於日本推出第4代後繼產品，而現行的28座版本經已全面停產。

### 第四代－B60／B70系列（2017年－）

[ToyotaCoaster2017.jpg](https://zh.wikipedia.org/wiki/File:ToyotaCoaster2017.jpg "fig:ToyotaCoaster2017.jpg")
第四代豐田Coaster於2016年12月首度亮相，於2017年1月23日由日本首次正式發售。此版本是為[港澳台地區稱呼的](../Page/港澳台.md "wikilink")「第七型」Coaster。

## 銷售情況

### 香港

**（請注意此章節中所謂一代至七代並非香港公共小型巴士行內專用術語，而有人方便管理而另起術語取代之。雖然所謂該等術語於香港交通發燒友界普及化，但當對行內人士談及時或許毫無頭緒。此分類方法雖可套用到香港所有款式的豐田Coaster，但一般只於談論公共小巴或專線小巴時使用，用作居民巴士或[客車的Coaster不使用此分類方法](../Page/客車.md "wikilink")。）**

豐田Coaster在香港隨處可見，甚至形成「一廠獨大」的局面，此現象有數個成因：

#### 公共小巴

[公共小巴是香港引入小型巴士數量最多的群體](../Page/香港小巴.md "wikilink")，現時小巴營辦商絕大部分傾向引入豐田Coaster。

自[三菱率先於](../Page/三菱FUSO.md "wikilink")1983年推出空調Rosa小巴後，豐田亦於1984年推出空調Coaster小巴，為求於香港小巴這個大市場中爭一席位。空調小巴甫推出，便極受小巴營辦商歡迎，紛紛棄用當年大行其道但不設空調的日產Echo小巴並轉投空調小巴。經過多年汰弱留強，豐田Coaster憑其高可靠性以及維修經濟性等優點成功落地生根，成為大贏家。

針對香港日益嚴重的空氣污染問題，[環保署與](../Page/環保署.md "wikilink")[運輸署合作於](../Page/運輸署.md "wikilink")2000年間推行「另類燃料小巴測試計劃」，旨在研究另類燃料小巴的可靠程度，從而逐步推行小巴另類燃料化，減低整體碳排放。參與是次計劃的小巴生產商共有四間，共派出15輛另類燃料小巴作為測試，其中包括8輛石油氣版本豐田Coaster。經過一連串測試，最終只有豐田Coaster成功通過所有測試，使其往後的「一哥」地位更為鞏固。

[File:ToyotaCoaster_80s_SmallDestinationDisplay.jpg|1980年代生產的第一代Coaster小巴（BB23](File:ToyotaCoaster_80s_SmallDestinationDisplay.jpg%7C1980年代生產的第一代Coaster小巴（BB23)），車頭採用較細小的路線牌箱，故俗稱「細牌箱」。最後一輛同款小巴EC7798已於2010年2月退役。
<File:HK> Toyota 90s
MiniBus.jpg|1990年至1992年生產的第二代Coaster小巴，改用較為易見的大型路線牌箱，故俗稱「大牌箱」。此款小巴的數目一直減少（2005年起更大量淘汰），最後一輛同款小巴車牌EK9319（於1990年登記）已於2011年11月正式退役。
[File:ToyotaCoaster_3rd.jpg|thumb|豐田第三代Coaster小巴（BB42](File:ToyotaCoaster_3rd.jpg%7Cthumb%7C豐田第三代Coaster小巴（BB42)），1993年至1998年生產（在1995年至1998年推出的版本，改配歐盟一型引擎），車頭採用圓形頭燈設計，故俗稱「圓燈子彈頭」，在1993年至1994年推出的同款小巴數目已全數退役（2006年起更大量淘汰），而在1995年至1998年推出的版本的數目一直減少（2014年起更大量淘汰），最後一輛同款小巴車牌JG3977已於2018年6月除牌
<File:ToyotaCoaster> 4th
turbo.jpg|豐田第四代Coaster小巴（BB43），1998年至2002年生產，車頭採用方形頭燈設計，故俗稱「方燈子彈頭」。此款小巴的數目一直減少（2016年起更大量淘汰）。
<File:NTMinibus40>
LS9868.jpg|豐田第五代Coaster小巴，以石油氣作燃料，2002年至2007年生產，又稱「石仔」或「氣艇」。
[File:NWMinibus040.jpg|長陣版本石油氣Coaster小巴，2003年至2005年及2007年期間生產（在2007年推出的版本，車頭設計經過改良](File:NWMinibus040.jpg%7C長陣版本石油氣Coaster小巴，2003年至2005年及2007年期間生產（在2007年推出的版本，車頭設計經過改良)）。
[File:NWMinibus88D.jpg|長陣版本柴油Coaster小巴，2003年至2007年期間生產（在2007年推出的版本，車頭設計經過改良，改配歐盟四型引擎](File:NWMinibus88D.jpg%7C長陣版本柴油Coaster小巴，2003年至2007年期間生產（在2007年推出的版本，車頭設計經過改良，改配歐盟四型引擎)）
[File:NWMinibus88G.jpg|豐田第六代的石油氣Coaster小巴，頭幅設計經過改良，2007年起生產。此車車頭防撞杆為後期換上](File:NWMinibus88G.jpg%7C豐田第六代的石油氣Coaster小巴，頭幅設計經過改良，2007年起生產。此車車頭防撞杆為後期換上)。
<File:Minibus>
481A.JPG|豐田5EL豪華Coaster小巴，車主為[進智公交](../Page/進智公交.md "wikilink")，在2002年期間生產。（此款車型於2018起逐步除牌退役）
[file:NTMinibus44A_UG0706.jpg|2015年起投入服務的豐田第6代柴油小巴，採用較豪華的客車規格，車門是採用電動外趟門而非手動摺門，車窗方面採用黑色窗框，座椅亦較豪華，亦採用自動波箱](file:NTMinibus44A_UG0706.jpg%7C2015年起投入服務的豐田第6代柴油小巴，採用較豪華的客車規格，車門是採用電動外趟門而非手動摺門，車窗方面採用黑色窗框，座椅亦較豪華，亦採用自動波箱)。
[File:HKIMinibus55_UD1876.jpg|2016年起投入服務的豐田第6代石油氣小巴，採用較豪華的座椅](File:HKIMinibus55_UD1876.jpg%7C2016年起投入服務的豐田第6代石油氣小巴，採用較豪華的座椅)。
<File:NTMinibus43> VR5710.jpg|2017年起投入服務的豐田第7代石油氣小巴，設有19個座位及使用電子路線顯示器。

#### 巴士公司

香港亦有巴士公司購入多輛豐田Coaster。

早於1987年，[九巴旗下的兩條路線](../Page/九龍巴士.md "wikilink")，[90號線及](../Page/九龍巴士90線.md "wikilink")[290號線來往](../Page/九龍巴士290線_\(第一代\).md "wikilink")[彩虹邨與](../Page/彩虹邨.md "wikilink")[調景嶺](../Page/調景嶺.md "wikilink")，因行經路面極惡劣之路段，一直使用短小的英國亞比安單層巴士行駛。因應當時此車年事已高，須物色新車取代，在豐田的推介下，九巴於該年購入首批共9輛豐田Coaster，為24座位長陣版本。同時巴士車頂加上了一塊鋁質擋板阻隔陽光熱力，並採用茶色玻璃窗。及後為發展空調巴士服務，九巴再向豐田購入Coaster，總數達91輛。由於車款與小巴極為相近，座位數量又較小巴為多，長期引起小巴營辦商不滿，加上載客量比普通單層大巴還要低，1990年代，已有大量豐田Coaster退下火線，只保留少數服務90號線，直至1996年因應調景嶺村清拆後90號線取消為止。最後全數豐田Coaster於1997年退役。

而另外，[九廣鐵路於](../Page/九廣鐵路.md "wikilink")1988年因應[輕鐵通車](../Page/輕鐵.md "wikilink")，開辦一部分接駁巴士線。但有部分地區因為路面惡劣，難以採用較大型巴士服務。為此九廣鐵路於1988年，引入12輛豐田Coaster，與九巴的同為長陣版本。此批豐田Coaster主要行駛來往[屯門碼頭以及](../Page/屯門碼頭.md "wikilink")[龍鼓灘的](../Page/龍鼓灘.md "wikilink")[520接駁巴士線](../Page/港鐵巴士K52綫.md "wikilink")。由於載客量始終較低，當及後多款普通單層巴士被引進後，其中6輛豐田Coaster隨即出售予[雅高巴士](../Page/雅高巴士.md "wikilink")，餘者則被編為後備車輛，間中行駛來往[大埔墟火車站與](../Page/大埔墟火車站.md "wikilink")[大埔滘九廣鐵路職員宿舍的](../Page/大埔滘.md "wikilink")50R線，直至1997年全數退役。

香港大部份旅遊巴士公司亦有採用豐田Coaster(多為內崁式門)，而且款式和配搭比小巴更多元化。

#### 警察機動部隊

[Police_Midibus_AM6752.JPG](https://zh.wikipedia.org/wiki/File:Police_Midibus_AM6752.JPG "fig:Police_Midibus_AM6752.JPG")專用的新款警察運兵車，俗稱**子彈頭**，於2008年投入服務。\]\]

於2008年年中，[香港警務處率先引入](../Page/香港警務處.md "wikilink")32輛豐田Coaster予[警察機動部隊作新運兵車](../Page/警察機動部隊.md "wikilink")，一輛新運兵車連同改裝費用，約40萬港元，期後將來陸續引入更多。新警車乃經過豐田Coaster超長陣標準29座改裝成為18座，車尾後座位置轉成裝備存放間。新警車採用更先進、省油以及環保的歐盟四型廢氣排放標準的引擎。新車內籠較為寬闊，車窗面積同為更大，能夠充份擴闊視野；與此同時，為求達至[保安關係](../Page/保安.md "wikilink")，所有車窗皆使用了防爆玻璃及裝設了防暴網。此外，新車運行時寧靜，坐位亦設計寬敞\[1\]。

### 澳門

[新福利](../Page/澳門新福利公共汽車有限公司.md "wikilink")、[澳巴也曾引進豐田Coaster](../Page/澳門公共汽車有限公司.md "wikilink")，但情況則不如香港般理想：

[澳門新福利於](../Page/澳門新福利公共汽車有限公司.md "wikilink")1994年率先引進6輛豐田Coaster作測試，但效果欠佳，其中一輛更在行駛半年後被退回豐田車廠。其後新福利再訂購新小巴時，訂單最終落入其對手[三菱Rosa中](../Page/新福利ROSA小巴.md "wikilink")。自此沒有再增購。而餘下的豐田Coaster已於2005至2006年間全數售予集團屬下的AA旅遊作觀光用途，最後於2010年退役。

[澳巴則在新福利引入後的不久](../Page/澳門公共汽車有限公司.md "wikilink")，引進首批豐田Coaster達20輛。但澳巴沒有如新福利般棄用，在1997年繼續增購，最終購入達50輛。澳巴的豐田Coaster與日產Civilian小巴一直是車隊中的主力車款，在2006年因日產Civilian接連發生車禍而被政府勒令停駛，豐田Coaster的地位更顯重要。全數豐田Coaster已於2011年8月1日「新巴士營運模式」實行前退役，澳巴卻因大勢所趨，再沒有選擇購入新的豐田Coaster接替舊車，而轉投中國製的五洲龍尿素SCR小巴。豐田Coaster在澳門的專利巴士歷史就此結束。

澳門亦有私人機構和政府部門選擇引入豐田Coaster，但數量及規模均不多，亦要和[三菱Rosa](../Page/三菱Rosa.md "wikilink")、日產Civilian（已停產）、日野Liésse
II（豐田Coaster之OEM車型）等品牌競爭，故其普及程度遠不及香港。目前澳門大多巴士都是國產之[金龍汽車](../Page/金龍汽車.md "wikilink")、[宇通客車](../Page/宇通客車.md "wikilink")、[五洲龍汽車為主](../Page/五洲龙汽车.md "wikilink")。

### 台灣

[TaipeiBus_275sub_2U132_Front.jpg](https://zh.wikipedia.org/wiki/File:TaipeiBus_275sub_2U132_Front.jpg "fig:TaipeiBus_275sub_2U132_Front.jpg")275路副線使用的豐田Coaster\]\]
[基隆客運](../Page/基隆客運.md "wikilink")、[基隆市公車](../Page/基隆市公車.md "wikilink")、[台北客運](../Page/台北客運.md "wikilink")、[新北客運](../Page/新北客運.md "wikilink")、[指南客運](../Page/指南客運.md "wikilink")、[中興巴士](../Page/中興巴士.md "wikilink")、[光華巴士](../Page/光華巴士.md "wikilink")、[淡水客運](../Page/淡水客運.md "wikilink")、[欣欣客運](../Page/欣欣客運.md "wikilink")、[大南汽車](../Page/大南汽車.md "wikilink")、[東南客運](../Page/東南客運.md "wikilink")、[首都客運](../Page/首都客運.md "wikilink")、[大都會客運](../Page/大都會客運.md "wikilink")、[三重客運](../Page/三重客運.md "wikilink")、[桃園客運](../Page/桃園客運.md "wikilink")、[中壢客運](../Page/中壢客運.md "wikilink")、[統聯客運](../Page/統聯客運.md "wikilink")、[中台灣客運](../Page/中台灣客運.md "wikilink")、[台中客運](../Page/台中客運.md "wikilink")、[豐榮客運](../Page/豐榮客運.md "wikilink")、[豐原客運](../Page/豐原客運.md "wikilink")、[南投客運](../Page/南投客運.md "wikilink")、[員林客運](../Page/員林客運.md "wikilink")、[彰化客運](../Page/彰化客運.md "wikilink")、[嘉義縣公車](../Page/嘉義縣公車.md "wikilink")、[新營客運](../Page/新營客運.md "wikilink")、[興南客運](../Page/興南客運.md "wikilink")、[府城客運](../Page/府城客運.md "wikilink")、[港都客運](../Page/港都客運.md "wikilink")、[高雄客運](../Page/高雄客運.md "wikilink")、[南台灣客運](../Page/南台灣客運.md "wikilink")、[義大客運](../Page/義大客運.md "wikilink")、[屏東客運](../Page/屏東客運.md "wikilink")、[鼎東客運](../Page/鼎東客運.md "wikilink")、金門縣車船管理處等客運業者，皆有使用[豐田Coaster營運](../Page/豐田Coaster.md "wikilink")。且是乙類大客車中最常用的一款。

<File:KaohsiungCityBus> 36 015XH
Front.jpg|[高雄市公車](../Page/高雄市公車.md "wikilink")36路使用豐田Coaster，為第四代的24座位長陣版本。但這輛Coaster側面的指揮燈，比香港的Coaster更早出現。
<File:2008Computex> Inter-Hall Shuttle
716FL.jpg|[台北](../Page/台北.md "wikilink")[大南汽車的Coaster市民小巴](../Page/大南汽車.md "wikilink")，參與2008年[台北國際電腦展覽會的展館接駁車營運](../Page/台北國際電腦展覽會.md "wikilink")。

### 中國大陆

[Wen_Jiabao_09.JPG](https://zh.wikipedia.org/wiki/File:Wen_Jiabao_09.JPG "fig:Wen_Jiabao_09.JPG")在[清华大学视察时在考斯特内向民众挥手](../Page/清华大学.md "wikilink")\]\]
中國早於1980年代已有豐田Coaster之引進，主要是高級應用，例如作酒店接送、机场要客接驳等。该车因外观低调、承载人数适合团队出行等特点也被作为中国大陆[党和国家领导人落区视察时的专车使用](../Page/党和国家领导人.md "wikilink")\[2\]。現時於中國大陆銷售的豐田Coaster由[一汽豐田汽車製造](../Page/一汽豐田.md "wikilink")，内饰经过改造\[3\]。

<File:Coaster> on Putuo
Island.jpg|在[中國大陸的豐田Coaster一般為](../Page/中國大陸.md "wikilink")[四川一汽豐田製造](../Page/一汽豐田.md "wikilink")，圖為[普陀山的Coaster](../Page/普陀山.md "wikilink")。
<File:Small> coach
(2904469572).jpg|北京五洲[皇冠假日酒店使用的考斯特](../Page/皇冠假日酒店.md "wikilink")
<File:AirChinabus1.jpg>|[中国国际航空使用的考斯特](../Page/中国国际航空.md "wikilink")
<File:51891> at Tianzhu
(20180228175817).jpg|[海南航空使用的考斯特](../Page/海南航空.md "wikilink")

### 歐洲

豐田Coaster在歐洲的銷售情況一直十分平穩，箇中原因相信是其歐洲製車身，較符合當地人的要求。車身供應商方面亦會定時更改車身設計吸引買家。

## 圖片

<File:Toyota> Coaster in the Aeropuerto Internacional del Caribe
Santiago
Mariño.jpg|在[委內瑞拉](../Page/委內瑞拉.md "wikilink")[瑪格麗塔島的豐田Coaster](../Page/瑪格麗塔島.md "wikilink")，車尾不設太平門（台灣稱安全門）。

## 生產工廠

  - [岐阜車體工業](../Page/岐阜車體工業.md "wikilink")
  - [四川一汽丰田汽车](../Page/四川一汽丰田汽车.md "wikilink")
  - [國瑞汽車觀音工廠](../Page/國瑞汽車.md "wikilink")

## 競爭對手

  - [三菱扶桑Rosa](../Page/三菱Rosa.md "wikilink")
  - [日產Civilian](../Page/日產Civilian.md "wikilink")

## 相關條目

  - [香港小巴](../Page/香港小巴.md "wikilink")
  - [台北客運](../Page/台北客運.md "wikilink")
  - [大南汽車](../Page/大南汽車.md "wikilink")

## 参考资料

## 外部連結

  - [豐田Coaster](http://toyota.jp/coaster/index.html)
  - [豐田澳洲：Coaster](http://coaster.toyota.com.au/toyota/vehicle/HomePage/0,4666,2321_822,00.html)
  - [Coaster 在
    香港豐田網站](https://web.archive.org/web/20090228202105/http://www.toyota.com.hk/cars/new_cars/coaster/index.asp)
  - [石油氣Coaster
    在香港豐田網站](https://web.archive.org/web/20071016045118/http://www.toyota.com.hk/showroom/lpg_coaster/index.html)
  - [歐洲版Coaster官方網站](http://empresas.gruposalvadorcaetano.pt/fogeca/MainFogeca?cmd=getcompanies&idbusarea=13&l=cn)

[丰田考斯特改装
丰田考斯特改装方案](http://www.carxoo.com/depreciate/saleinfo-352742.html)

[C](../Category/豐田車輛.md "wikilink")
[Category:香港小巴](../Category/香港小巴.md "wikilink")
[Category:巴士型號](../Category/巴士型號.md "wikilink")

1.  [警察機動部隊新型運員車
    有助提升行動效率](http://www.police.gov.hk/offbeat/897/chi/n06.htm)
    《警聲》 第897期
2.
3.