是一個位於[日本](../Page/日本.md "wikilink")[愛知縣](../Page/愛知縣.md "wikilink")[豐田市的](../Page/豐田市.md "wikilink")[足球場](../Page/足球場.md "wikilink")。球場於2001年建成並可以容納45,000人。豐田球場是[日本職業足球聯賽球隊](../Page/日本職業足球聯賽.md "wikilink")[名古屋鯨魚的主場](../Page/名古屋鯨魚.md "wikilink")，其贊助商[豐田汽車亦擁有球場的冠名權](../Page/豐田汽車.md "wikilink")。

由2005年至2008年，豐田球場成為[世界冠軍球會盃的其中一個比賽場地](../Page/世界冠軍球會盃.md "wikilink")。

## 图片

<File:Toyota> Stadium, Mori-cho Toyota 2012.JPG|2012年8月撮影 <File:Toyota>
sta 0313 1.JPG|2010年3月撮影

## 外部連結

  - [世界球場](https://web.archive.org/web/20080621113854/http://www.worldstadiums.com/stadium_pictures/asia/japan/chubu/toyota_stadium.shtml)

  - [官方網頁](http://www.toyota-stadium.co.jp/)

[Category:豐田市](../Category/豐田市.md "wikilink")
[Category:日本足球場](../Category/日本足球場.md "wikilink")
[Category:橄欖球場](../Category/橄欖球場.md "wikilink")
[Category:2001年完工體育場館](../Category/2001年完工體育場館.md "wikilink")