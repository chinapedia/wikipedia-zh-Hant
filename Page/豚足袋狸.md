**豚足袋狸**（*Chaeropus
ecaudatus*）是一種細小及[草食性的](../Page/草食性.md "wikilink")[袋狸](../Page/袋狸.md "wikilink")，分佈在[澳洲內陸乾旱及半乾旱的平原](../Page/澳洲.md "wikilink")。

## 特徵

豚足袋狸約有小[貓的大小](../Page/貓.md "wikilink")，外觀像[兔耳袋狸](../Page/兔耳袋狸.md "wikilink")，有幼長的四肢，[耳朵大而且尖](../Page/耳朵.md "wikilink")，尾巴很長。豚足袋狸在近看時則很奇特，前肢有兩指，指甲像蹄，有點像[豬或](../Page/豬.md "wikilink")[鹿](../Page/鹿.md "wikilink")。後腳的四趾較大，趾上的爪像小馬的蹄，其他的趾都已退化。

## 棲息地

豚足袋狸棲息在多種的環境，包括林地及草原等。

## 行為

很少人可以見到活的豚足袋狸。唯一的紀錄指牠們移動得很緩慢，似是拖著後肢來走路。但[原住民則指牠們日間在巢中睡覺](../Page/原住民.md "wikilink")，被騷擾時則走得很快。有指牠們是棲息在樹穴中，或會挖掘短的隧道來築巢。

## 標本

豚足袋狸的第一個標本是於1836年在[維多利亞州北部近](../Page/維多利亞州.md "wikilink")[穆理河及](../Page/穆理河.md "wikilink")[馬蘭比吉河交界處採集的](../Page/馬蘭比吉河.md "wikilink")。其餘的標本是在[南澳州](../Page/南澳州.md "wikilink")、[西澳州及](../Page/西澳州.md "wikilink")[北領地的乾旱郊區採集的](../Page/北領地.md "wikilink")。最後確實的標本是於1901年採集的。有指牠們在南澳州生存多了20年以上，而在西澳州則生存至1950年代。

[Pig-footed_Bandicoot_Pengo.jpg](https://zh.wikipedia.org/wiki/File:Pig-footed_Bandicoot_Pengo.jpg "fig:Pig-footed_Bandicoot_Pengo.jpg")

## 保育狀況

豚足袋狸在[歐洲殖民前已經不怎麼普遍](../Page/歐洲.md "wikilink")，但於19世紀有科學紀錄時就已經大幅減少。到了20世紀初時，牠們就已經在[西澳州](../Page/西澳州.md "wikilink")[滅絕了](../Page/滅絕.md "wikilink")。牠們滅絕的原因不明。消失當時[狐狸及](../Page/狐狸.md "wikilink")[野兔並未到達西澳州](../Page/野兔.md "wikilink")，而[野貓則有可能掠食牠們](../Page/野貓.md "wikilink")。牠們最有可能的是失去棲息地而滅亡。

根據其[食道及](../Page/食道.md "wikilink")[齒列結構](../Page/齒列.md "wikilink")，並標本內的食物，豚足袋狸似乎主要是[草食性的](../Page/草食性.md "wikilink")。另外牠們亦喜歡吃肉，包括[螞蟻及](../Page/螞蟻.md "wikilink")[白蟻](../Page/白蟻.md "wikilink")。

## 分類

豚足袋狸以往是與[兔耳袋狸分類在](../Page/兔耳袋狸.md "wikilink")[袋狸科](../Page/袋狸科.md "wikilink")。但是由於牠們的外觀與普通的[袋狸及兔耳袋狸相距甚遠](../Page/袋狸.md "wikilink")，加上[分子證據顯示牠們的不同](../Page/分子.md "wikilink")，牠們現正分類在自己的[豚足袋狸科中](../Page/豚足袋狸科.md "wikilink")。\[1\]

## 參考

[CE](../Category/IUCN絕滅物種.md "wikilink")
[E](../Category/袋狸目.md "wikilink")

1.