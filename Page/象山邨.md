[Cheung_Shan_Estate_(full_view).JPG](https://zh.wikipedia.org/wiki/File:Cheung_Shan_Estate_\(full_view\).JPG "fig:Cheung_Shan_Estate_(full_view).JPG")
[Podium_of_Cheung_Shan_Estate.jpg](https://zh.wikipedia.org/wiki/File:Podium_of_Cheung_Shan_Estate.jpg "fig:Podium_of_Cheung_Shan_Estate.jpg")
[Cheung_Shan_Shopping_Centre_Floor_2.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Shan_Shopping_Centre_Floor_2.jpg "fig:Cheung_Shan_Shopping_Centre_Floor_2.jpg")
[Cheung_Shan_Market.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Shan_Market.jpg "fig:Cheung_Shan_Market.jpg")
[Cheung_Shan_Estate_Cooked_Food_Stalls.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Shan_Estate_Cooked_Food_Stalls.jpg "fig:Cheung_Shan_Estate_Cooked_Food_Stalls.jpg")
[Playground_in_Cheung_Shan_Estate.jpg](https://zh.wikipedia.org/wiki/File:Playground_in_Cheung_Shan_Estate.jpg "fig:Playground_in_Cheung_Shan_Estate.jpg")
**象山邨**（）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[荃灣區](../Page/荃灣區.md "wikilink")[象鼻山高鐵隧道上方](../Page/象鼻山_\(香港\).md "wikilink")，鄰近[城門水塘](../Page/城門水塘.md "wikilink")，遠離[荃灣市中心及](../Page/荃灣市中心.md "wikilink")[大窩口](../Page/大窩口.md "wikilink")。屋邨共有三座樓宇，現時由高耀物業管理有限公司負責屋邨管理。

## 歷史及簡介

象山邨前身是[城門道與](../Page/城門道.md "wikilink")[老圍村之間的一片農地和象山](../Page/老圍村.md "wikilink")[寮屋區](../Page/寮屋區.md "wikilink")。象山[寮屋區因被政府徵用發展](../Page/寮屋區.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，在1975至1976年期間清拆，按前香港屋宇建設委員會標準設計，建成後的屋邨只有三座樓宇，並命名為象山邨，並於1978至1979年間入伙，交由[香港房屋委員會管理](../Page/香港房屋委員會.md "wikilink")。

由於象山邨的位置與[圓玄學院](../Page/圓玄學院.md "wikilink")、西方寺非常接近，所以象山邨商場的漢城酒家曾經吸引許多於圓玄學院及西方寺拍攝電視劇的明星光顧。不過，因象山邨位置極為偏遠，而居民亦較喜歡於早上前往[荃灣市中心喝茶](../Page/荃灣市中心.md "wikilink")，然後買菜回家做飯，令光顧象山邨酒家的客人愈來愈少，後來酒家結業，該址改為[老人院](../Page/老人院.md "wikilink")。

## 屋邨資料

| 樓宇名稱 | 樓宇類型                               | 落成年份 |
| ---- | ---------------------------------- | ---- |
| 樂山樓  | [雙塔式](../Page/雙塔式大廈.md "wikilink") | 1978 |
| 秀山樓  | 1979                               |      |
| 翠山樓  | [舊長型](../Page/舊長型大廈.md "wikilink") |      |

## 區議員

[區議會選區屬](../Page/區議會.md "wikilink")[荃灣K](../Page/荃灣.md "wikilink")17象石選區，其範圍包括象山邨三座樓宇、[石圍角邨的石翠樓與石菊樓](../Page/石圍角邨.md "wikilink")，以及象鼻山路一帶村屋，現任[區議員是](../Page/區議員.md "wikilink")[民建聯的陳振中](../Page/民建聯.md "wikilink")。

## 設施

### 公共設施

  - 象山邨街市
  - 象山商場
  - [城門谷游泳池](../Page/城門谷游泳池.md "wikilink")（附設室內泳池）
  - [城門谷運動場](../Page/城門谷運動場.md "wikilink")
  - [城門谷公園](../Page/城門谷公園.md "wikilink")

### 教育設施

  - [路德會呂明才中學](../Page/路德會呂明才中學.md "wikilink")

<!-- end list -->

  - [荃灣信義學校](../Page/荃灣信義學校.md "wikilink")（已結束）

### 居民組織

  - 秀山樓互委會，親社民連/親[中華民國](../Page/中華民國.md "wikilink")
  - 樂山樓互委會，親[民建聯](../Page/民建聯.md "wikilink")。
  - 翠山樓互委會，中立。
  - 象山邨居民聯會
  - 象山邨關注組

## 著名居民

  - [譚凱邦](../Page/譚凱邦.md "wikilink")：荃灣區議員，[環保觸覺創辦人](../Page/環保觸覺.md "wikilink")
  - [龍緯汶](../Page/龍緯汶.md "wikilink")：[南方民主同盟主席](../Page/南方民主同盟.md "wikilink")
  - [黃智賢](../Page/黃智賢_\(香港\).md "wikilink")：電視演員

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [象山邨西路](../Page/象山邨西路.md "wikilink")「象山邨秀山樓」巴士站

<!-- end list -->

  - 32線來回程繞經並停靠「象山邨秀山樓」巴士站。
  - 32M線以「象山邨秀山樓」巴士站作為「象山」循環點折返[葵芳站](../Page/葵芳站公共運輸交匯處.md "wikilink")。
  - 36線只限往[梨木樹方向才繞經並停靠](../Page/梨木樹公共運輸交匯處.md "wikilink")「象山邨秀山樓」巴士站。
  - 48X線只限逢星期六、日及公眾假期07:30及08:30由[灣景花園開往](../Page/灣景花園巴士總站.md "wikilink")[禾輋之班次繞經並停靠](../Page/禾輋巴士總站.md "wikilink")「象山邨秀山樓」巴士站。

<!-- end list -->

  - 象山商場及大排檔外小巴總站

<!-- end list -->

  - [三棟屋路](../Page/三棟屋路.md "wikilink")

<!-- end list -->

  - 象山邨西路及[象山邨東路交界](../Page/象山邨東路.md "wikilink")
    紅色公共小巴

<!-- end list -->

  - 象山/[石圍角至](../Page/石圍角邨.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")\[1\]
  - 旺角至象山/石圍角 (下午至晚上服務)\[2\]

</div>

</div>

## 圖像

Cheung Shan Shopping Centre.jpg|象山商場 Atrium in Cheung Shan Shopping
Centre.jpg|象山商場中庭 Cheung Shan Shopping Centre Floor 2 (2).jpg|2樓商店
Cheung Shan Estate Table Tennis zone.jpg|乒乓球區 Cheung Shan Estate Pebble
Walking Trail.jpg|卵石路步行徑 Cheung Shan Estate Car Park.jpg|停車場

## 資料來源

## 外部連結

  - [房屋署象山邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2734)
  - [荃葵青交通總站巡禮 - 象山](http://hk.youtube.com/watch?v=SU92i4ofQFg)
  - [象山總站](http://hkbus.wikia.com/wiki/%E8%B1%A1%E5%B1%B1%E7%B8%BD%E7%AB%99)
  - [象山邨秀山樓](http://hkbus.wikia.com/wiki/%E8%B1%A1%E5%B1%B1%E9%82%A8%E7%A7%80%E5%B1%B1%E6%A8%93)

[en:Public housing estates in Tsuen Wan\#Cheung Shan
Estate](../Page/en:Public_housing_estates_in_Tsuen_Wan#Cheung_Shan_Estate.md "wikilink")

[Category:荃灣](../Category/荃灣.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  [荃灣海壩街　—　石圍角及象山](http://www.16seats.net/chi/rmb/r_n32.html)
2.  [旺角砵蘭街　—　石圍角及象山](http://www.16seats.net/chi/rmb/r_kn32.html)