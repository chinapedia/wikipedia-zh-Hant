**豪豬**，又稱**箭豬**，是一類披有尖刺的[齧齒目](../Page/齧齒目.md "wikilink")，可以用來防禦[掠食者](../Page/掠食者.md "wikilink")。豪豬是齧齒目中體型第三大的，僅次於[水豚及](../Page/水豚.md "wikilink")[河狸](../Page/河狸.md "wikilink")，但與[刺蝟不同](../Page/刺蝟.md "wikilink")。大部份豪豬約長60—90厘米，尾巴長20—25厘米，重5—16公斤。牠們圓潤及行動緩慢。豪豬有褐色、灰色及白色。

不同豪豬物種的刺有不同的形狀，不過所有都是改變了的毛髮，表面上有一層[角質素](../Page/角質素.md "wikilink")，嵌入在[皮膚的](../Page/皮膚.md "wikilink")[肌肉組織](../Page/肌肉.md "wikilink")。[舊大陸豪豬](../Page/舊大陸豪豬.md "wikilink")（[豪豬科](../Page/豪豬科.md "wikilink")）的刺是一束束的，而[新大陸豪豬](../Page/新大陸豪豬.md "wikilink")（[美洲豪豬科](../Page/美洲豪豬科.md "wikilink")）的刺則是與毛髮夾雜在一起。

豪豬的刺銳利，很易脫落，會刺入攻擊者中。牠們的刺有倒鈎，可以掛在皮膚上，很難除去。牠們的刺長75毫米及闊2毫米。若刺鈎在攻擊者的[組織上](../Page/组织_\(生物学\).md "wikilink")，在正常的肌肉運動下，倒鈎會令刺插得更深，每日可以深入約幾毫米。曾有掠食者因被刺刺中及感染而死去，在死去後刺仍會繼續嵌入體內。\[1\]古代相信豪豬可以擲出牠們刺來攻擊敵人，但其實是錯誤的。\[2\]\[3\]

在[非洲及](../Page/非洲.md "wikilink")[阿拉伯的一些地區](../Page/阿拉伯.md "wikilink")，豪豬可以作為[叢林肉食用](../Page/叢林肉.md "wikilink")。牠的肉在[義大利及](../Page/義大利.md "wikilink")[越南的一些地區很受歡迎](../Page/越南.md "wikilink")。

豪豬分佈在[亞洲](../Page/亞洲.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[北美洲及](../Page/北美洲.md "wikilink")[南美洲的](../Page/南美洲.md "wikilink")[熱帶及](../Page/熱帶.md "wikilink")[溫帶](../Page/溫帶.md "wikilink")。豪豬生活在[森林](../Page/森林.md "wikilink")、[沙漠及](../Page/沙漠.md "wikilink")[草原](../Page/草原.md "wikilink")。有一些住在樹上，其他的則留在地上。

豪豬會在[人類棲息地尋找](../Page/人類.md "wikilink")[鹽](../Page/鹽.md "wikilink")，會吃以[硝酸鈉保存的夾板](../Page/硝酸鈉.md "wikilink")\[4\]、某些[油漆](../Page/油漆.md "wikilink")、手工具、鞋、衣服及其他會有[汗液的物件](../Page/汗液.md "wikilink")。豪豬會被用在溶雪的岩鹽吸引到路上，且會咬輪胎。若有[鹽漬地可以阻礙豪豬的破壞](../Page/鹽漬地.md "wikilink")。

豪豬可以從有豐富鹽份的[植物](../Page/植物.md "wikilink")、活[動物的骨頭](../Page/動物.md "wikilink")、樹皮、[土壤及有](../Page/土壤.md "wikilink")[尿液的物件獲得天然的鹽](../Page/尿液.md "wikilink")。\[5\]}}

## 物種

豪豬是[齧齒目下的](../Page/齧齒目.md "wikilink")[美洲豪豬科或](../Page/美洲豪豬科.md "wikilink")[豪豬科](../Page/豪豬科.md "wikilink")，共有27個[物種](../Page/物種.md "wikilink")。這兩科有一些分別，且不是近親。牠們與[刺蝟不同](../Page/刺蝟.md "wikilink")，且與[針鼴科相差甚遠](../Page/針鼴科.md "wikilink")。豪豬的大小差別很大，[南美洲的](../Page/南美洲.md "wikilink")*Coendou
rothschildi*的體重小於1公斤，但[非洲冕豪豬則重超過](../Page/非洲冕豪豬.md "wikilink")20公斤。

### 舊大陸豪豬

[Porcupine_Berlin_Zoo.jpg](https://zh.wikipedia.org/wiki/File:Porcupine_Berlin_Zoo.jpg "fig:Porcupine_Berlin_Zoo.jpg")
[舊大陸豪豬的](../Page/舊大陸豪豬.md "wikilink")11個物種差不多都是陸上生活的及較大體型。牠們與[豪豬下目在](../Page/豪豬下目.md "wikilink")3000萬年前分隔。以下是有關的物種：

  - [豪豬科](../Page/豪豬科.md "wikilink")
  - [Atherurus](../Page/Atherurus.md "wikilink")
      - [叢尾豪豬](../Page/叢尾豪豬.md "wikilink")（*Atherurus africanus*）
      - （*Atherurus karnuliensis*）
      - [掃尾豪豬](../Page/掃尾豪豬.md "wikilink")（*Atherurus macrourus*）

亞種

  -   - （*Atherurus africanus africanus (Gray, 1842)*）
      - （*Atherurus africanus centralis (Thomas, 1895)*）
      - （*Atherurus africanus turneri (St. Leger, 1932)*）

  - [Hystrix](../Page/Hystrix.md "wikilink")

      - [南非豪豬](../Page/南非豪豬.md "wikilink")（*Hystrix africaeaustralis*）
      - [馬來豪豬](../Page/馬來豪豬.md "wikilink")（*Hystrix brachyura*）
      - [粗棘印尼豪豬](../Page/粗棘印尼豪豬.md "wikilink")（*Hystrix crassispinis*）
      - [非洲冕豪豬](../Page/非洲冕豪豬.md "wikilink")（*Hystrix cristata*）
      - [印度豪豬](../Page/印度豪豬.md "wikilink")（*Hystrix indicus*）
      - [爪哇豪豬](../Page/爪哇豪豬.md "wikilink")（*Hystrix javanica*）
      - [巴拉望豪猪](../Page/巴拉望豪猪.md "wikilink")（*Hystrix pumila*）
      - [蘇門豪豬](../Page/蘇門豪豬.md "wikilink")（*Hystrix sumatrae*）

亞種

  -   - （*Hystrix africaeaustralis africaeaustralis Peter, 1852*）
      - （*Hystrix africaeaustralis zuluensis Roberts, 1936*）
      - （*Hystrix brachyura brachyura (Linnaeus, 1758)*）
      - [中國豪豬](../Page/中國豪豬.md "wikilink")（*Hystrix brachyura hodgsoni
        (Gray, 1847)*）
      - （*Hystrix brachyura klossi (Thomas, 1916)*）
      - （*Hystrix brachyura papae (Allen, 1927)*）
      - （*Hystrix brachyura subcristata (Swinhoe, 1870)*）

  - [Thecurus](../Page/Thecurus.md "wikilink")

      - [長尾豪豬](../Page/長尾豪豬.md "wikilink")（*Trichys fasciculata*）

### 新大陸豪豬

[新大陸豪豬的](../Page/新大陸豪豬.md "wikilink")12個物種較細小，且是攀樹的能手。牠們自行的[演化](../Page/演化.md "wikilink")，與齧齒目的其他科更為接近。以下是有關的物種：

  - [美洲豪豬科](../Page/美洲豪豬科.md "wikilink")
  - [Chaetomys](../Page/Chaetomys.md "wikilink")
      - [細針豪豬](../Page/細針豪豬.md "wikilink")（*Chaetomys
        subspinosus*）：有時被認為是屬於[棘鼠科](../Page/棘鼠科.md "wikilink")。
  - [Coendou](../Page/Coendou.md "wikilink")
      - *Coendou baturitensis*
      - [雙色卷尾豪豬](../Page/雙色卷尾豪豬.md "wikilink")（*Coendou bicolor*）
      - *Coendou nycthemera*
      - *Coendou prehensilis*
      - *Coendou quichua*
      - *Coendou rothschildi*
      - *Coendou sanctamartae*
      - *Coendou speratus*

亞種

  -   - （*Coendou bicolor bicolor Tschudi, 1844*）
      - （*Coendou bicolor richardsoni Allen, 1913*）
      - （*Coendou bicolor simonsi Thomas, 1902*）

  - [Sphiggurus](../Page/Sphiggurus.md "wikilink")

      - *Sphiggurus ichillus*
      - *Sphiggurus insidiosus*
      - *Sphiggurus melanurus*
      - [墨西哥樹豪豬](../Page/墨西哥樹豪豬.md "wikilink")（*Sphiggurus mexicanus*）
      - *Sphiggurus pruinosus*
      - *Sphiggurus roosmalenorum*
      - [多刺卷尾豪豬](../Page/多刺卷尾豪豬.md "wikilink")（*Sphiggurus spinosus*）
      - [衣豪豬](../Page/衣豪豬.md "wikilink")（*Sphiggurus vestitus*）
      - *Sphiggurus villosus*
      - (( *Sphiggurus pallidus*：已[滅絕](../Page/滅絕.md "wikilink")。))

亞種

  -   - （*Sphiggurus mexicanus laenatus Thomas, 1903*）
      - （*Sphiggurus mexicanus mexicanus Kerr, 1792*）
      - （*Sphiggurus mexicanus yucataniae Thomas, 1902*）

  - [Erethizon](../Page/Erethizon.md "wikilink")

      - [北美豪豬](../Page/北美豪豬.md "wikilink")（*Erethizon dorsatum*）

亞種

  -   - （*Erethizon dorsatum bruneri Swenk, 1916*）
      - （*Erethizon dorsatum couesi Mearns, 1897*）
      - （*Erethizon dorsatum dorsatum Linnaeus, 1758*）
      - （*Erethizon dorsatum epixanthus Brandt, 1835*）
      - （*Erethizon dorsatum myops Merriam, 1900*）
      - （*Erethizon dorsatum nigrescens Allen, 1903*）
      - （*Erethizon dorsatum picinum Bangs, 1900*）

  - [Echinoprocta](../Page/Echinoprocta.md "wikilink")

      - [亞瑪遜林豪豬](../Page/亞瑪遜林豪豬.md "wikilink")（*Echinoprocta rufescens*）

## 參考

## 外部連結

  - [Porcupine Facts and
    Photos](https://web.archive.org/web/20080130115258/http://depts.washington.edu/natmap/facts/porcupine_712.html)
    - NatureMapping Program
  - [Porcupines: Wildlife summary from the African Wildlife
    Foundation](http://www.awf.org/wildlives/179)
  - ["Resource Cards: What About
    Porcupines?"](http://science-ed.pnl.gov/pals/resource/cards/porcupines.stm)
    Pacific Northwest National Laboratory
  - [*Porcupine control in the western
    states*](http://digital.library.unt.edu/permalink/meta-dc-1544:1)
    hosted by the [UNT Government Documents
    Department](http://digital.library.unt.edu/browse/department/govdocs/)
  - [Why the porcupine is the logo of the Free State
    Project](http://www.freestateproject.org/about/faq.php#why)

[Category:豪豬](../Category/豪豬.md "wikilink")

1.
2.
3.
4.
5.