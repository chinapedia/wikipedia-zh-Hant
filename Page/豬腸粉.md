[Chucheungfun.jpg](https://zh.wikipedia.org/wiki/File:Chucheungfun.jpg "fig:Chucheungfun.jpg")
[thumb](../Page/file:JueCheungFan_with_sauce.jpg.md "wikilink")、[廣東甜醬油](../Page/醬油.md "wikilink")、花生醬、[芝麻醬](../Page/芝麻醬.md "wikilink")、[甜酱和](../Page/甜面酱.md "wikilink")[辣醬](../Page/辣醬.md "wikilink")\]\]
**豬腸粉**又稱**齋腸**,是一種使用[米漿製作的](../Page/米漿.md "wikilink")[廣東食品](../Page/廣東.md "wikilink")，在[香港及](../Page/香港.md "wikilink")[馬來西亞是常見的街頭小吃](../Page/馬來西亞.md "wikilink")\[1\]。香港的豬腸粉除了使用醬油調味外，還可按喜好加入[甜面酱](../Page/甜面酱.md "wikilink")、花生醬、[辣椒醬或](../Page/辣椒醬.md "wikilink")[芝麻醬而且當地的酒樓常以](../Page/芝麻醬.md "wikilink")[XO醬與豬腸粉同炒](../Page/XO醬.md "wikilink")，製作XO醬炒豬腸粉\[2\]。

## 製作

[Chee_cheong_fun_in_KL.jpg](https://zh.wikipedia.org/wiki/File:Chee_cheong_fun_in_KL.jpg "fig:Chee_cheong_fun_in_KL.jpg")
豬腸粉初期形狀大小與豬大腸相近，故名為「豬腸粉」，是由沙河粉改良而成，在米漿中加入適量的薯粉、玉米粉、馬蹄粉或澄粉，同時在整鍋上塗上一層油並隔著一塊布蒸熟，使得口感比沙河粉更嫩滑細緻。蒸熟後捲起，切成手指般大小而成，並配以香油、甜醬、辣椒醬、芝麻、生抽、咖哩醬等食用\[3\]。

## 在馬來西亞的發展

馬來西亞的豬腸粉是隨著早期中國南來的移民而帶來，當地的吃法除了在豬腸粉上淋上各種醬料後直接使用，還習慣搭配魚丸、豬肉丸、腐竹、豬皮或各種釀豆腐的材料來使用，醬料多使用辣椒醬、甜醬、蝦膏或咖哩醬，多在早餐或下午茶時間享用\[4\]。隨著豬腸粉已落地超過百年，目前已經在地化，在當地除了捲成豬腸裝外，也有成扁平狀。在馬來西亞霹靂州安順（Teluk
Intan）的豬腸粉特色則以加入炒香調味的沙葛、蝦米及油蔥酥，使用時搭配獨特醃辣椒\[5\]

## 參見

  - [腸粉](../Page/腸粉.md "wikilink")
  - [炸兩](../Page/炸兩.md "wikilink")
  - [餅卷](../Page/餅卷.md "wikilink")
  - [捲仔粿](../Page/捲仔粿.md "wikilink")

## 參考資料

<references />

[zh-yue:豬腸粉](../Page/zh-yue:豬腸粉.md "wikilink")

[Category:稻米食品](../Category/稻米食品.md "wikilink")
[Category:廣東小吃](../Category/廣東小吃.md "wikilink")
[Category:香港小吃](../Category/香港小吃.md "wikilink")
[Category:中国食品](../Category/中国食品.md "wikilink")
[Category:馬來西亞食品](../Category/馬來西亞食品.md "wikilink")
[Category:馬來西亞華人美食](../Category/馬來西亞華人美食.md "wikilink")
[Category:馬來西亞麵條食品](../Category/馬來西亞麵條食品.md "wikilink")
[Category:東南亞食品](../Category/東南亞食品.md "wikilink")

1.  [透薄香軟熱腸粉](http://orientaldaily.on.cc/cnt/lifestyle/20120330/00296_004.html)，[東方日報](../Page/東方日報.md "wikilink")，2012-3-30
2.  [豬腸粉不復舊時味](http://www.goldenage.hk/b5/ga/ga_article.php?article_id=750)
    ，長訊月刊，
3.  [的確涼布
    拉出完美腸粉](http://hk.apple.nextmedia.com/supplement/food/art/20140817/18832121)，[蘋果日報](../Page/蘋果日報.md "wikilink")，2014-8-17
4.  [蝦膏豬腸粉](http://news.sinchew.com.my/node/267224?tid=46)
5.  [安順豬腸粉溫熱滑口](http://life.sinchew.com.my/node/12339)