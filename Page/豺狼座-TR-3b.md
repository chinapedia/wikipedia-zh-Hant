**豺狼座-TR-3b**是一個環繞著[豺狼座-TR-3的](../Page/豺狼座-TR-3.md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")，距離地球8950[光年之遠](../Page/光年.md "wikilink")，它是一顆的典型的[熱木星](../Page/熱木星.md "wikilink")\[1\]。

## 參見

  - [豺狼座-TR-3](../Page/豺狼座-TR-3.md "wikilink")

## 参考资料

## 外部链接

[Category:2007年發現的系外行星](../Category/2007年發現的系外行星.md "wikilink")
[Category:豺狼座](../Category/豺狼座.md "wikilink")
[Category:熱木星](../Category/熱木星.md "wikilink")
[Category:凌星現象](../Category/凌星現象.md "wikilink")

1.  {{ cite journal | title=THE LUPUS TRANSIT SURVEY FOR HOT JUPITERS:
    RESULTS AND LESSONS |
    url=<http://www.iop.org/EJ/abstract/1538-3881/137/5/4368> |
    author=Bayliss *et al.* | journal= [The Astronomical
    Journal](../Page/The_Astronomical_Journal.md "wikilink") | year=2009
    | volume=137 | issue=5 | pages=4368–4376 |
    doi=10.1088/0004-6256/137/5/4368 | last2=Weldrake | first2=David T.
    F. | last3=Sackett | first3=Penny D. | last4=Tingley |
    first4=Brandon W. | last5=Lewis | first5=Karen M. |
    bibcode=2009AJ....137.4368B}}