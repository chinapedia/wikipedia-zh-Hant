**豺狼座**，是现代88[星座之一](../Page/星座.md "wikilink")，也是[托勒密的](../Page/托勒密.md "wikilink")48星座之一。此星座是南天星座之一，位於[天秤座正南](../Page/天秤座.md "wikilink")，[天蠍座西南](../Page/天蠍座.md "wikilink")，也就是在南天亮星[心宿二和](../Page/心宿二.md "wikilink")[南門雙星之間](../Page/南門雙星.md "wikilink")，可惜，它在[赤緯](../Page/赤緯.md "wikilink")－40°附近，北半球不大容易看到。它包含中国古代星座：顿顽，从官，骑官，车骑，积卒，柱，骑阵将军。

基本上本星座没有特别亮的星，但[视星等在](../Page/视星等.md "wikilink")2-3等的星有30颗左右。亮度在第6等的星共70颗，其中包含几个[双星和多星系统](../Page/双星系统.md "wikilink")。最亮的星豺狼座α是[蓝巨星](../Page/蓝巨星.md "wikilink")；豺狼座β中國星名為騎官十。

## 历史和神话

没有明确的神话对应，有时候说它代表希腊神话中的国王Lycaon。托勒密将代表物设定成狼。希腊天文学家[喜帕恰斯在前](../Page/喜帕恰斯.md "wikilink")200年左右把它从半人马座中分出来，称为“兽”。另外还被和附近的[天箭座](../Page/天箭座.md "wikilink")、[半人马座一起被认为是被神话英雄](../Page/半人马座.md "wikilink")[海格力斯杀死的](../Page/海格力斯.md "wikilink")“厄律曼托斯山上的野猪”。

## [深空天体](../Page/深空天体.md "wikilink")

北部有[NGC 5824](../Page/NGC_5824.md "wikilink")、[NGC
5986](../Page/NGC_5986.md "wikilink")[球状星团](../Page/球状星团.md "wikilink")，另外附近有[暗星云](../Page/暗星云.md "wikilink")[B
228](../Page/B_228.md "wikilink")。南部有两个[疏散星团](../Page/疏散星团.md "wikilink")[NGC
5822](../Page/NGC_5822.md "wikilink")、[NGC
5749](../Page/NGC_5749.md "wikilink")。西部边上有两个[漩涡星系](../Page/漩涡星系.md "wikilink")，和包含已知最高温恒星的[行星星系](../Page/行星星系.md "wikilink")[IC
4406](../Page/IC_4406.md "wikilink")。星座中心也有一个行星狀星系[NGC
5882](../Page/NGC_5882.md "wikilink")。著名的[超新星遗迹](../Page/超新星遗迹.md "wikilink")[SN
1006也位于豺狼座](../Page/SN_1006.md "wikilink")。

## 恒星

  -
    有独立名字的恆星（數字為其光度）：
      - （[豺狼座α](../Page/豺狼座α.md "wikilink")）2.30 ***Men***或***Kakkab***
      - （[豺狼座β](../Page/豺狼座β.md "wikilink")）2.68 ***騎官四***
    [拜耳命名法中的恆星](../Page/拜耳命名法.md "wikilink")（數字為其光度）：
      -
        [豺狼座γ](../Page/豺狼座γ.md "wikilink")
        2.80；[豺狼座δ](../Page/豺狼座δ.md "wikilink")
        3.22；[豺狼座ε](../Page/豺狼座ε.md "wikilink")
        3.37；[豺狼座ζ](../Page/豺狼座ζ.md "wikilink")
        3.41；[豺狼座η](../Page/豺狼座η.md "wikilink")
        3.42；[豺狼座θ](../Page/豺狼座θ.md "wikilink")
        4.22；[豺狼座ι](../Page/豺狼座ι.md "wikilink")
        3.55；[豺狼座κ1](../Page/豺狼座κ1.md "wikilink")
        3.88；[豺狼座κ2](../Page/豺狼座κ2.md "wikilink")
        5.70；[豺狼座λ](../Page/豺狼座λ.md "wikilink")
        4.07；[豺狼座μ](../Page/豺狼座μ.md "wikilink")
        4.27；[豺狼座ν1](../Page/豺狼座ν1.md "wikilink")
        4.99；[豺狼座ν2](../Page/豺狼座ν2.md "wikilink")
        5.65；[豺狼座ξ1](../Page/豺狼座ξ1.md "wikilink")
        5.14；[豺狼座ξ2](../Page/豺狼座ξ2.md "wikilink")
        5.59；[豺狼座ο](../Page/豺狼座ο.md "wikilink")
        4.32；[豺狼座π](../Page/豺狼座π.md "wikilink")
        –；[双星系统](../Page/双星系统.md "wikilink")3.91,
        4.82；[ρ](../Page/Rho_Lupi.md "wikilink")
        4.05；[豺狼座σ](../Page/豺狼座σ.md "wikilink")
        4.44；[豺狼座τ1](../Page/豺狼座τ1.md "wikilink")
        4.56；[豺狼座τ2](../Page/豺狼座τ2.md "wikilink")
        4.33；[豺狼座υ](../Page/豺狼座υ.md "wikilink")
        5.36；[豺狼座χ](../Page/豺狼座χ.md "wikilink")/5
        3.97；[豺狼座φ1](../Page/豺狼座φ1.md "wikilink")
        3.57；[豺狼座φ2](../Page/豺狼座φ2.md "wikilink")
        4.54；[豺狼座ψ1](../Page/豺狼座ψ1.md "wikilink")/3
        4.66；[豺狼座ψ2](../Page/豺狼座ψ2.md "wikilink")/4
        4.75；[豺狼座ω](../Page/豺狼座ω.md "wikilink")
        4.34；[豺狼座a](../Page/豺狼座a.md "wikilink")
        5.39；[豺狼座b](../Page/豺狼座b.md "wikilink")
        5.22；[豺狼座c](../Page/豺狼座c.md "wikilink")
        5.38；[豺狼座d](../Page/豺狼座d.md "wikilink")
        4.55；[豺狼座e](../Page/豺狼座e.md "wikilink")
        4.83；[豺狼座f](../Page/豺狼座f.md "wikilink")/2
        4.35；[豺狼座g](../Page/豺狼座g.md "wikilink")
        4.64；[豺狼座h](../Page/豺狼座h.md "wikilink")
        5.23；[豺狼座i](../Page/豺狼座i.md "wikilink")/1
        4.91；[豺狼座k](../Page/豺狼座k.md "wikilink") 4.60

[Chai2](../Category/星座.md "wikilink")
[豺狼座](../Category/豺狼座.md "wikilink")