[貓空壺穴.JPG](https://zh.wikipedia.org/wiki/File:貓空壺穴.JPG "fig:貓空壺穴.JPG")
**貓空**（臺語：猫空，Bâ-khang）位於[臺灣](../Page/臺灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[文山區](../Page/文山區.md "wikilink")，屬於[二格山系](../Page/二格山.md "wikilink")，昔日為台北市最大產茶區之一，茶區上古道縱橫，乃為運送茶葉所建，目前為茶藝及夜景勝地。

## 名稱

「貓空」這個名稱的由來，最常見的說法是來自當地河流沖刷岩礁所形成的「[壺穴](../Page/壺穴.md "wikilink")」地形，[台語稱凹凸不平的臉孔為](../Page/台語.md "wikilink")「貓面」（[台羅](../Page/台羅.md "wikilink")、[白話字](../Page/白話字.md "wikilink")，下同：niau-bīn、bâ-bīn），「貓」在閩南語中有時唸「bâ」有時唸「niau」\[1\]\[2\]。在[日治時期的台灣堡圖上記載當地為](../Page/台灣日治時期.md "wikilink")「-{猫}-空」及發音為「」（Bâ-khang）。
[Taipei_Zoo_(24301625776).jpg](https://zh.wikipedia.org/wiki/File:Taipei_Zoo_\(24301625776\).jpg "fig:Taipei_Zoo_(24301625776).jpg")
翁佳音和曹銘宗認為，貓空和壺穴關連的說法有點勉強，照台灣堡圖記載的發音、台語的地名用詞和此地環境，推論貓空是指[果子狸](../Page/果子狸.md "wikilink")（bâ）出沒的山谷（khang）\[3\]。

## 特色

[Night_view_from_MaoKong.jpg](https://zh.wikipedia.org/wiki/File:Night_view_from_MaoKong.jpg "fig:Night_view_from_MaoKong.jpg")
貓空鄰近台北郊區，處於[台北盆地邊緣](../Page/台北盆地.md "wikilink")。所以天氣晴朗時，從山上便可瞭望整個台北，景色一覽無遺。加上夜晚的台北燈火爛漫，所以貓空成為賞夜景的勝地。貓空也有多條的登山步道，可以從山腳下的[國立政治大學一直爬到山頂](../Page/國立政治大學.md "wikilink")，週末假日便有許多人到此健行。

貓空早年產茶，[鐵觀音為此處特色茶種](../Page/鐵觀音.md "wikilink")。此區開設了許多茶莊，以觀光休閒為導向，將傳統茶藝結合餐飲，不但可以喝到好茶也能吃到好菜，加上可以瞭望整個台北的風景，為一受歡迎的休閒去處。

貓空附近的[指南宮](../Page/指南宮.md "wikilink")，為台北著名[道教廟宇](../Page/道教.md "wikilink")，每逢節慶便湧入大量人潮，祈求平安順利。

## 交通

  - 由於貓空出入道路狹窄，因此市政府在此興建[貓空纜車](../Page/貓空纜車.md "wikilink")。可於[動物園站轉乘](../Page/動物園站_\(臺北市\).md "wikilink")[台北捷運文山線](../Page/台北捷運文山線.md "wikilink")，全程票價50元。已經於2007年7月4日正式通車，曾因[薔蜜颱風造成第](../Page/超強颱風薔薇_\(2008年\).md "wikilink")16號塔柱地基嚴重流失，自2008年10月1日停駛，2010年3月26日恢復營運。

## 參考資料

## 相關條目

  - [貓空纜車](../Page/貓空纜車.md "wikilink")
  - [指南宮](../Page/指南宮.md "wikilink")
  - [台灣茶文化](../Page/台灣茶文化.md "wikilink")
  - [醉夢溪](../Page/醉夢溪.md "wikilink")

[Category:台北市境內地區](../Category/台北市境內地區.md "wikilink")
[Category:台灣茶史](../Category/台灣茶史.md "wikilink")
[Category:台北市旅遊景點](../Category/台北市旅遊景點.md "wikilink")
[Category:文山區](../Category/文山區.md "wikilink")

1.  《[廣韻](../Page/廣韻.md "wikilink")》“貓”有兩讀：一讀武瀌切（音苗），一讀莫交切（音毛）。在台語中，前者演變成[文讀](../Page/文讀.md "wikilink")“niau”，用豸旁的“-{貓}-”記錄；後者演變成[白讀](../Page/白讀.md "wikilink")“bâ”，用犬旁的“-{猫}-”記錄。
2.  [台語辭典（台日大辭典台語譯本）](http://taigi.fhl.net/dict/index.html)
3.