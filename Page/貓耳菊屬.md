**貓耳菊屬**又可稱為[猫儿菊属](../Page/猫儿菊属.md "wikilink")（）是[菊科的一種植物](../Page/菊科.md "wikilink")。\[1\]

## 品種

  - *[Hypochaeris acaulis](../Page/Hypochaeris_acaulis.md "wikilink")*
  - *[Hypochaeris
    achyrophorus](../Page/Hypochaeris_achyrophorus.md "wikilink")*
  - *[Hypochaeris alba](../Page/Hypochaeris_alba.md "wikilink")*
  - *[Hypochaeris
    albiflora](../Page/Hypochaeris_albiflora.md "wikilink")*
  - *[Hypochaeris
    angustifolia](../Page/Hypochaeris_angustifolia.md "wikilink")*
  - *[Hypochaeris
    apargioides](../Page/Hypochaeris_apargioides.md "wikilink")*
  - *[Hypochaeris
    arachnoides](../Page/Hypochaeris_arachnoides.md "wikilink")*
  - *[Hypochaeris arenaria](../Page/Hypochaeris_arenaria.md "wikilink")*
  - *[Hypochaeris
    atlantica](../Page/Hypochaeris_atlantica.md "wikilink")*
  - *[Hypochaeris balbisii](../Page/Hypochaeris_balbisii.md "wikilink")*
  - *[Hypochaeris
    brasiliensis](../Page/Hypochaeris_brasiliensis.md "wikilink")*
  - *[Hypochaeris
    caespitosa](../Page/Hypochaeris_caespitosa.md "wikilink")*
  - *[Hypochaeris
    catharinensis](../Page/Hypochaeris_catharinensis.md "wikilink")*
  - *[Hypochaeris
    chillensis](../Page/Hypochaeris_chillensis.md "wikilink")*
  - *[Hypochaeris
    chondrilloides](../Page/Hypochaeris_chondrilloides.md "wikilink")*
  - *[Hypochaeris
    chubutensis](../Page/Hypochaeris_chubutensis.md "wikilink")*
  - *[Hypochaeris ciliata](../Page/Hypochaeris_ciliata.md "wikilink")*
  - *[Hypochaeris
    clarionoides](../Page/Hypochaeris_clarionoides.md "wikilink")*
  - *[Hypochaeris claryi](../Page/Hypochaeris_claryi.md "wikilink")*
  - *[Hypochaeris confusa](../Page/Hypochaeris_confusa.md "wikilink")*
  - *[Hypochaeris
    crepidioides](../Page/Hypochaeris_crepidioides.md "wikilink")*
  - *[Hypochaeris
    cretensis](../Page/Hypochaeris_cretensis.md "wikilink")*
  - *[Hypochaeris
    cupressorum](../Page/Hypochaeris_cupressorum.md "wikilink")*
  - *[Hypochaeris
    deserticola](../Page/Hypochaeris_deserticola.md "wikilink")*
  - *[Hypochaeris dolosa](../Page/Hypochaeris_dolosa.md "wikilink")*
  - *[Hypochaeris dubia](../Page/Hypochaeris_dubia.md "wikilink")*
  - *[Hypochaeris
    echegarayi](../Page/Hypochaeris_echegarayi.md "wikilink")*
  - *[Hypochaeris elata](../Page/Hypochaeris_elata.md "wikilink")*
  - *[Hypochaeris erecta](../Page/Hypochaeris_erecta.md "wikilink")*
  - *[Hypochaeris
    eremophila](../Page/Hypochaeris_eremophila.md "wikilink")*
  - *[Hypochaeris
    eriolaena](../Page/Hypochaeris_eriolaena.md "wikilink")*
  - *[Hypochaeris foliosa](../Page/Hypochaeris_foliosa.md "wikilink")*
  - *[Hypochaeris gayana](../Page/Hypochaeris_gayana.md "wikilink")*
  - [光貓耳菊](../Page/光貓耳菊.md "wikilink")*（*Hypochaeris glabra''）
  - *[Hypochaeris glabrata](../Page/Hypochaeris_glabrata.md "wikilink")*
  - *[Hypochaeris graminea](../Page/Hypochaeris_graminea.md "wikilink")*
  - *[Hypochaeris
    grandidentata](../Page/Hypochaeris_grandidentata.md "wikilink")*
  - *[Hypochaeris
    grandiflorus](../Page/Hypochaeris_grandiflorus.md "wikilink")*
  - *[Hypochaeris
    grisebachii](../Page/Hypochaeris_grisebachii.md "wikilink")*
  - *[Hypochaeris
    helvetica](../Page/Hypochaeris_helvetica.md "wikilink")*
  - *[Hypochaeris hirta](../Page/Hypochaeris_hirta.md "wikilink")*
  - *[Hypochaeris
    hispidula](../Page/Hypochaeris_hispidula.md "wikilink")*
  - *[Hypochaeris
    hohenackeri](../Page/Hypochaeris_hohenackeri.md "wikilink")*
  - *[Hypochaeris incana](../Page/Hypochaeris_incana.md "wikilink")*
  - *[Hypochaeris jussieui](../Page/Hypochaeris_jussieui.md "wikilink")*
  - *[Hypochaeris
    laciniosa](../Page/Hypochaeris_laciniosa.md "wikilink")*
  - *[Hypochaeris
    lessingii](../Page/Hypochaeris_lessingii.md "wikilink")*
  - *[Hypochaeris
    linearifolia](../Page/Hypochaeris_linearifolia.md "wikilink")*
  - *[Hypochaeris lutea](../Page/Hypochaeris_lutea.md "wikilink")*
  - [斑點貓耳菊](../Page/斑點貓耳菊.md "wikilink")（*Hypochaeris maculata*）
  - *[Hypochaeris
    magellanica](../Page/Hypochaeris_magellanica.md "wikilink")*
  - *[Hypochaeris
    megapotamica](../Page/Hypochaeris_megapotamica.md "wikilink")*
  - *[Hypochaeris
    melanolepis](../Page/Hypochaeris_melanolepis.md "wikilink")*
  - *[Hypochaeris
    meyeniana](../Page/Hypochaeris_meyeniana.md "wikilink")*
  - *[Hypochaeris
    microcephala](../Page/Hypochaeris_microcephala.md "wikilink")*
  - *[Hypochaeris montana](../Page/Hypochaeris_montana.md "wikilink")*
  - *[Hypochaeris mucida](../Page/Hypochaeris_mucida.md "wikilink")*
  - *[Hypochaeris
    nahuelvutae](../Page/Hypochaeris_nahuelvutae.md "wikilink")*
  - *[Hypochaeris
    neopinnatifida](../Page/Hypochaeris_neopinnatifida.md "wikilink")*
  - *[Hypochaeris
    oligocephala](../Page/Hypochaeris_oligocephala.md "wikilink")*
  - *[Hypochaeris
    palustris](../Page/Hypochaeris_palustris.md "wikilink")*
  - *[Hypochaeris
    pampasica](../Page/Hypochaeris_pampasica.md "wikilink")*
  - *[Hypochaeris
    parvifolia](../Page/Hypochaeris_parvifolia.md "wikilink")*
  - *[Hypochaeris
    patagonica](../Page/Hypochaeris_patagonica.md "wikilink")*
  - *[Hypochaeris
    petiolaris](../Page/Hypochaeris_petiolaris.md "wikilink")*
  - *[Hypochaeris pilosa](../Page/Hypochaeris_pilosa.md "wikilink")*
  - *[Hypochaeris
    procumbens](../Page/Hypochaeris_procumbens.md "wikilink")*
  - *[Hypochaeris radiata](../Page/Hypochaeris_radiata.md "wikilink")*
  - [貓耳菊](../Page/貓耳菊.md "wikilink")（*Hypochaeris radicata*）：本屬最普遍的品種。
  - *[Hypochaeris robertia](../Page/Hypochaeris_robertia.md "wikilink")*
  - *[Hypochaeris rutea](../Page/Hypochaeris_rutea.md "wikilink")*
  - *[Hypochaeris
    saldensis](../Page/Hypochaeris_saldensis.md "wikilink")*
  - *[Hypochaeris
    salzmanniana](../Page/Hypochaeris_salzmanniana.md "wikilink")*
  - *[Hypochaeris
    schizoglossa](../Page/Hypochaeris_schizoglossa.md "wikilink")*
  - *[Hypochaeris
    scorzonerae](../Page/Hypochaeris_scorzonerae.md "wikilink")*
  - *[Hypochaeris
    serioloides](../Page/Hypochaeris_serioloides.md "wikilink")*
  - *[Hypochaeris
    sessiliflora](../Page/Hypochaeris_sessiliflora.md "wikilink")*
  - *[Hypochaeris setosa](../Page/Hypochaeris_setosa.md "wikilink")*
  - *[Hypochaeris
    shermanum](../Page/Hypochaeris_shermanum.md "wikilink")*
  - *[Hypochaeris
    sichorioides](../Page/Hypochaeris_sichorioides.md "wikilink")*
  - *[Hypochaeris
    soratensis](../Page/Hypochaeris_soratensis.md "wikilink")*
  - *[Hypochaeris
    spathulata](../Page/Hypochaeris_spathulata.md "wikilink")*
  - *[Hypochaeris
    taraxacoides](../Page/Hypochaeris_taraxacoides.md "wikilink")*
  - *[Hypochaeris
    tenerifolia](../Page/Hypochaeris_tenerifolia.md "wikilink")*
  - *[Hypochaeris
    tenuiflora](../Page/Hypochaeris_tenuiflora.md "wikilink")*
  - *[Hypochaeris
    tenuifolia](../Page/Hypochaeris_tenuifolia.md "wikilink")*
  - *[Hypochaeris
    thermarum](../Page/Hypochaeris_thermarum.md "wikilink")*
  - *[Hypochaeris
    thrincioides](../Page/Hypochaeris_thrincioides.md "wikilink")*
  - *[Hypochaeris
    toltensis](../Page/Hypochaeris_toltensis.md "wikilink")*
  - *[Hypochaeris
    tropicalis](../Page/Hypochaeris_tropicalis.md "wikilink")*
  - *[Hypochaeris uniflora](../Page/Hypochaeris_uniflora.md "wikilink")*
  - *[Hypochaeris
    variegata](../Page/Hypochaeris_variegata.md "wikilink")*
  - *[Hypochaeris webbii](../Page/Hypochaeris_webbii.md "wikilink")*

## 参考文献

## 外部链接

  -
[Category:菊科](../Category/菊科.md "wikilink")
[Category:貓耳菊屬](../Category/貓耳菊屬.md "wikilink")

1.