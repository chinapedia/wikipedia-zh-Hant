**貓肝吸蟲**（學名：**）是一種[吸蟲綱](../Page/吸蟲綱.md "wikilink")[寄生蟲](../Page/寄生.md "wikilink")，可寄生在[哺乳類動物的](../Page/哺乳類動物.md "wikilink")[膽管和](../Page/膽管.md "wikilink")[膽囊](../Page/膽囊.md "wikilink")，以[膽汁為食](../Page/膽汁.md "wikilink")，可造成[肝](../Page/肝.md "wikilink")、[膽病變](../Page/膽.md "wikilink")。

本物種於1884年首次由[Sebastiano
Rivolta在](../Page/Sebastiano_Rivolta.md "wikilink")[北義大利的](../Page/義大利.md "wikilink")[貓](../Page/貓.md "wikilink")[肝發現](../Page/肝.md "wikilink")。1891年，[俄羅斯](../Page/俄羅斯帝國.md "wikilink")[Konstantin
Nikolaevich
Vinogradov](../Page/Konstantin_Nikolaevich_Vinogradov.md "wikilink")
(1847–1906)\[1\]首度於人體發現，但當時未知乃同一物種，所以被命名為「西伯利亞肝吸蟲」。
1931年，[德國](../Page/德國.md "wikilink")[汉堡](../Page/汉堡.md "wikilink")
（Hans Vogel）在期刊發表表物種的[完整生活史](../Page/生物生命週期.md "wikilink")\[2\]。

## 分佈

全球約有1700萬人到感染，主要分布在環[地中海地區的](../Page/地中海.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")、[法國](../Page/法國.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[北馬其頓](../Page/北馬其頓.md "wikilink")、[希臘和](../Page/希臘.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")，[中歐的](../Page/中歐.md "wikilink")[德國](../Page/德國.md "wikilink")、[瑞士和](../Page/瑞士.md "wikilink")[波蘭](../Page/波蘭.md "wikilink")，以及[俄羅斯的](../Page/俄羅斯.md "wikilink")[高加索地區和](../Page/高加索.md "wikilink")[西伯利亞地區](../Page/西伯利亞.md "wikilink")。

## 形態

成蟲尺寸介於長7至12毫米、寬2至3毫米的範圍，體型狹長而扁平，蟲體一端具有口吸盤，其腸管自此延蟲體兩側分叉延伸至尾端，在前端約五分之一界線處，另有一個與口吸盤約略等大的腹吸盤。此外泰國肝吸蟲雌雄同體，兩顆[睪丸呈現深裂的分葉狀](../Page/睪丸.md "wikilink")，分布在口吸盤的另一端，約是蟲體後端五分之一的區域，[卵巢則位於中後段三分之一線上](../Page/卵巢.md "wikilink")，[子宮則在中段盤繞](../Page/子宮.md "wikilink")，分布在全長第一至第三個五等分線之間，兩旁可見網點狀的卵黃腺。

## 生活史

[Opisthorchis_LifeCycle.gif](https://zh.wikipedia.org/wiki/File:Opisthorchis_LifeCycle.gif "fig:Opisthorchis_LifeCycle.gif")。\]\]

貓肝吸蟲的[生活史與](../Page/生活史.md "wikilink")[中華肝吸蟲和](../Page/中華肝吸蟲.md "wikilink")[泰國肝吸蟲極為類似](../Page/泰國肝吸蟲.md "wikilink")，以[淡水螺為其](../Page/淡水螺.md "wikilink")[第一中間宿主](../Page/中間宿主.md "wikilink")：蟲卵必須在其中孵化。然後經過四個發育階段後離開螺體，並尋找[鯉科的](../Page/鯉科.md "wikilink")[淡水魚作為第二中間宿主](../Page/淡水魚.md "wikilink")，在魚鱗、皮膚、肌肉組織發育後，透過其他動物生吃、未完全熟食這些魚，幼蟲可在食用者體內脫去外囊，經過四週發育成蟲，可在宿主體內活上40年之久，每日約產下1000至2500顆卵，可隨[膽汁一同進入](../Page/膽汁.md "wikilink")[消化道](../Page/消化道.md "wikilink")，最後隨[糞便排出](../Page/糞便.md "wikilink")，是診斷的重要依據。

以下為常見的第一中間宿主：

  - *[Bithynia
    inflata](../Page/Bithynia_inflata.md "wikilink")*（異名：*[Codiella
    inflata](../Page/Codiella_inflata.md "wikilink")*）
  - **
  - **
  - **

常見的第二宿主魚種計有：

  - [高体雅罗鱼](../Page/高体雅罗鱼.md "wikilink") *[Leuciscus
    idus](../Page/Leuciscus_idus.md "wikilink")*

  - [丁鱥](../Page/丁鱥.md "wikilink") *[Tinca
    tinca](../Page/Tinca_tinca.md "wikilink")*

  - [欧鳊](../Page/欧鳊.md "wikilink") *[Abramis
    brama](../Page/Abramis_brama.md "wikilink")*

  - *Ballerus sapa*

  - **

  - [鯉](../Page/鯉.md "wikilink") *[Cyprinus
    carpio](../Page/Cyprinus_carpio.md "wikilink")*

  - **

  - [高体雅罗鱼](../Page/高体雅罗鱼.md "wikilink") *[Leuciscus
    idus](../Page/Leuciscus_idus.md "wikilink")*

  - [歐白魚](../Page/歐白魚.md "wikilink") *[Alburnus
    alburnus](../Page/Alburnus_alburnus.md "wikilink")*

  - **，以及

  - [紅眼魚](../Page/紅眼魚.md "wikilink") *[Scardinius
    erythrophthalmus](../Page/Scardinius_erythrophthalmus.md "wikilink")*

最終宿主包括所有會吃魚的哺乳類動物，例如：狗、狐、貓、大鼠、豬、兔、海豹、獅子、狼獾、貂、臭鼬和人類。

## 對人類的影響

估算在俄羅斯約有150萬人被貓肝吸蟲[寄生](../Page/寄生.md "wikilink")[感染](../Page/感染.md "wikilink")。在[西伯利亚](../Page/西伯利亚.md "wikilink")，當地居民習慣食用只是用鹽稍微淹製過、未經煮熟的冰凍浸漬魚。這很可能令他們受寄生在淡水魚中的貓肝吸蟲感染。

專指由本物種引起的疾病，其嚴重程度從沒有明顯病徵到嚴重疾病，視乎能否及早檢測和接受治療。在人類，本物種感染的結果會影響肝臟、[胰脏及膽囊](../Page/胰脏.md "wikilink")。如感染者未能及早就醫，病情可能會惡化至[肝硬化](../Page/肝硬化.md "wikilink")，更會增加患上[肝細胞癌的風險](../Page/肝細胞癌.md "wikilink")。不過，兒童受感染後可能沒有明顯病徵。

當貓肝吸蟲進入體內兩週後，會開始感染。這時的病徵包括[发热](../Page/发热.md "wikilink")、容易感覺疲倦、出現[皮疹和胃腸道紊亂](../Page/疹.md "wikilink")。嚴重時的病徵會有[贫血和肝臟受損](../Page/贫血.md "wikilink")，令感染者在1-2個月內失去能力。治療通常會使用單次劑量的[吡喹酮](../Page/吡喹酮.md "wikilink")。

## 參考文獻

1.  Orit Yossepowitch et al., [Opisthorchiasis from Imported Raw
    Fish](https://web.archive.org/web/20070927200922/http://0-www.cdc.gov.mill1.sjlibrary.org/ncidod/EID/vol10no12/04-0410.htm),
    *Emerg Infect Dis.* 2004 Dec;10(12):2122-6. PMID 15663848

2.  A.M. Bronstein , V.D.Zavoikin and O.P.Zelya, [Brief update on
    Opisthorchis felineus infection in
    Russia](https://web.archive.org/web/20070203023748/http://www.cdfound.to.it/html/bronste.htm).

## 參看

  -
  - [中華肝吸蟲](../Page/中華肝吸蟲.md "wikilink")

  - [泰國肝吸蟲](../Page/泰國肝吸蟲.md "wikilink")

  - [牛羊肝吸蟲](../Page/牛羊肝吸蟲.md "wikilink")

  - [布氏薑片蟲](../Page/布氏薑片蟲.md "wikilink")

## 外部連結

  -
[Category:貓寄生蟲](../Category/貓寄生蟲.md "wikilink")
[Category:扁形动物门](../Category/扁形动物门.md "wikilink")
[felineus](../Category/後睪屬.md "wikilink") [Category:Animals described in
1895](../Category/Animals_described_in_1895.md "wikilink")
[Category:人畜共患病](../Category/人畜共患病.md "wikilink")
[Category:Plagiorchiida](../Category/Plagiorchiida.md "wikilink")

1.
2.  [1](https://web.archive.org/web/20030923204125/http://www15.bni-hamburg.de/bni/bni2/neu2/getfile.acgi?area_engl=history&pid=132)
    Bernhard Nocht Institute for Tropical Medicine