**貝克縣**（**Becker County,
Minnesota**）是[美國](../Page/美國.md "wikilink")[明尼蘇達州西北部的一個縣](../Page/明尼蘇達州.md "wikilink")。面積3,743平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口30,000人。縣治[底特律湖](../Page/底特律湖.md "wikilink")（Detroit
Lakes）。

成立於1858年3月18日。縣名紀念當選[眾議員喬治](../Page/美國眾議院.md "wikilink")·盧米斯·貝克（George
Loomis Becker，但因議席分配數目不足經抽籤而落選）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[B](../Category/明尼苏达州行政区划.md "wikilink")

1.  <http://www.house.leg.state.mn.us/hinfo/govseries/No3.htm>