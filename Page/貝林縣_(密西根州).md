**貝林县**（**Berrien County,
Michigan**）是[美國](../Page/美國.md "wikilink")[密西根州](../Page/密西根州.md "wikilink")[下半島西南角的一個縣](../Page/下半島.md "wikilink")，西臨[密西根湖](../Page/密西根湖.md "wikilink")，南鄰[印地安納州](../Page/印地安納州.md "wikilink")。面積4,096平方公里
（其中2,617平方公里為水面面積）。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口162,453人。縣治[聖約瑟](../Page/聖約瑟_\(密西根州\).md "wikilink")
（St. Joseph）。

成立於1856年2月25日。縣名紀念[參議員](../Page/美國參議院.md "wikilink")、[美國司法部長](../Page/美國司法部長.md "wikilink")[約翰·M·貝林](../Page/約翰·M·貝林.md "wikilink")
（John Macpherson Berrien）\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[B](../Category/密歇根州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.