在[托爾金](../Page/托爾金.md "wikilink")（J. R. R.
Tolkien）的奇幻小說，**貝烈蓋爾海**（Belegaer）或**隔離之海**（Sundering
Seas）是[阿爾達](../Page/阿爾達.md "wikilink")（Arda）的海域，位於[中土大陸以西](../Page/中土大陸.md "wikilink")。

[第二紀元前](../Page/第二紀元.md "wikilink")，貝烈蓋爾海起自極北的伊爾門（Ilmen），那裡的[西爾卡瑞西海峽](../Page/西爾卡瑞西海峽.md "wikilink")（Helcaraxë）連接[阿門洲](../Page/阿門洲.md "wikilink")（Aman）及中土大陸，延伸至極南的寒冷地區。貝烈蓋爾海的北部比南部狹窄，最寬闊的位於阿爾達的赤道。

貝烈蓋爾海的真實範圍不明，直達極北及極南的冰封之地。貝烈蓋爾海與[大西洋](../Page/大西洋.md "wikilink")（Atlantic
Ocean）相似，其寛度可能大致一樣。

貝烈蓋爾海是[辛達林語](../Page/辛達林語.md "wikilink")，字節有*Beleg*或*Might*及*Aer*或*Eär*，有「海」的意思，這亦可見於航海家[埃蘭迪爾](../Page/埃蘭迪爾.md "wikilink")（Eärendil）。昆雅語則說成*Alatairë*。

貝爾蘭在[第一紀元末沉沒之前](../Page/第一紀元.md "wikilink")，貝烈蓋爾海北部狹窄且冰封，形成西爾卡瑞西海峽，可能可以由阿門洲步行前往中土大陸，雖然有難度，但[芬國昐](../Page/芬國昐.md "wikilink")（Fingolfin）及[諾多精靈](../Page/諾多精靈.md "wikilink")（Noldor）的確由此逃往中土大陸。

[憤怒之戰](../Page/憤怒之戰.md "wikilink")（War of
Wrath）結束後，貝烈蓋爾海因部分中土大陸沉沒而擴闊，北部的封海峽消失，使中土大陸及阿門洲分離。第二紀元，世界變成球體，貝烈蓋爾海被扭曲。阿門洲消失，在另一次元存在，只有被選擇的人才可經[筆直航道到達](../Page/筆直航道.md "wikilink")[維林諾](../Page/維林諾.md "wikilink")（Valinor）。在故事內並沒有交代貝烈蓋爾海的西端是甚麼，但有迹象顯示努曼諾爾人到貝烈蓋爾海西端尋找維林諾。貝烈蓋爾海西端也有可能是阿爾達的一部分，貝烈蓋爾海以西可能就是中土大陸東部。

## 週遭地區

貝烈蓋爾海有多個島嶼及群島：

  - [伊瑞西亞島](../Page/伊瑞西亞島.md "wikilink")（Tol Eressëa），孤獨島，靠近阿爾達海岸
  - [魔法島嶼](../Page/魔法島嶼.md "wikilink")（Enchanted Isles），於伊瑞西亞島對面
  - [努曼諾爾](../Page/努曼諾爾.md "wikilink")（只存在於第二紀元）
  - 西方島嶼（第一紀元後）
      - [莫玟島](../Page/莫玟島.md "wikilink")（Tol Morwen）
      - [多索尼安](../Page/多索尼安.md "wikilink")（Dorthonion）
      - [海姆瑞恩](../Page/海姆瑞恩.md "wikilink")（Himling）
  - [巴拉爾島](../Page/巴拉爾島.md "wikilink")（Balar，只存在於第一紀元）
  - [托爾法拉斯](../Page/托爾法拉斯.md "wikilink")（Tolfalas），位於貝爾法拉斯灣

其他貝烈蓋爾海的地貌：

  - [艾爾達瑪灣](../Page/艾爾達瑪灣.md "wikilink")（Eldamar）
  - [巴拉爾灣](../Page/巴拉爾灣.md "wikilink")（Bay of Balar，只存在於第一紀元）
  - [大海灣](../Page/大海灣.md "wikilink")（Great Gulf，只存在於第一紀元）
  - [專吉斯特峽灣](../Page/專吉斯特峽灣.md "wikilink")（Drengist，只存在於第一紀元）
  - [福羅契爾冰封灣](../Page/福羅契爾冰封灣.md "wikilink")（Ice-bay of Forochel，第一紀元後）
  - [隆恩灣](../Page/隆恩灣.md "wikilink")（Gulf of Lune，第一紀元後）
  - [貝爾法拉斯灣](../Page/貝爾法拉斯灣.md "wikilink")（Bay of Belfalas，第一紀元後）

[B](../Category/中土大陸的地理.md "wikilink")