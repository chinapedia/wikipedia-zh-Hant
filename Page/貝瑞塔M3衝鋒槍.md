**貝瑞塔M3**（Beretta Model
3）是[義大利](../Page/義大利.md "wikilink")[貝瑞塔在](../Page/貝瑞塔.md "wikilink")1956年制造的[9×19毫米口徑](../Page/9×19mm鲁格弹.md "wikilink")[衝鋒槍](../Page/衝鋒槍.md "wikilink")。

## 歷史及設計

貝瑞塔M3是[二戰時期](../Page/二戰.md "wikilink")[貝瑞塔M1938的重新設計改進版本](../Page/貝瑞塔M1938.md "wikilink")，貝瑞塔M1938原本的木制[槍托改為鋼制摺疊槍托](../Page/槍托.md "wikilink")，加入手槍型握把及延長彈匣接口，並對應20或40發彈匣。貝瑞塔M3裝有兩個[扳機](../Page/扳機.md "wikilink")，一個為全自動，另一個為單發。貝瑞塔M3採用[開放式槍機設計](../Page/開放式槍機.md "wikilink")，射速只有每分鐘550發，低射速令其在全自動及單發時凖確度較高。

義大利軍隊的貝瑞塔M3在1959年被[貝瑞塔M12取代](../Page/貝瑞塔M12衝鋒槍.md "wikilink")。

## 參考

  - [貝瑞塔M1938](../Page/貝瑞塔M1938.md "wikilink")
  - [貝瑞塔M12](../Page/貝瑞塔M12衝鋒槍.md "wikilink")
  - [貝瑞塔Mx4 Storm](../Page/貝瑞塔Mx4_Storm衝鋒槍.md "wikilink")

## 外部連結

  - —[貝瑞塔M3介紹](http://www.nasog.net/datasheets/firearms/smg/Beretta_Model_3.htm)

[Category:衝鋒槍](../Category/衝鋒槍.md "wikilink")
[Category:貝瑞塔](../Category/貝瑞塔.md "wikilink")
[Category:義大利槍械](../Category/義大利槍械.md "wikilink")
[Category:9毫米魯格彈槍械](../Category/9毫米魯格彈槍械.md "wikilink")