**貝璐**爵士<span style="font-size:smaller;">，[KCMG](../Page/聖米迦勒及聖喬治爵級司令勳章.md "wikilink")，[KBE](../Page/英帝國爵級司令勳章.md "wikilink")</span>（Sir
**William
Peel**，），[英國](../Page/英國.md "wikilink")[殖民地官員](../Page/殖民地.md "wikilink")，曾長年在[馬來亞任公職](../Page/馬來亞.md "wikilink")，後自1930年至1935年出任[香港總督一職](../Page/香港總督.md "wikilink")。

## 生平

### 早年生涯

貝璐在1875年2月27日生於[英格蘭北部](../Page/英格蘭.md "wikilink")[諾森伯蘭郡的](../Page/諾森伯蘭郡.md "wikilink")[赫克瑟姆](../Page/赫克瑟姆.md "wikilink")（Hexham），父親名叫W·E·貝璐，曾在[約克郡](../Page/約克郡.md "wikilink")[波士頓溫泉](../Page/波士頓溫泉.md "wikilink")（Boston
Spa）任職[牧師](../Page/牧師.md "wikilink")。貝璐早年入讀位於[韋克菲爾德](../Page/韋克菲爾德.md "wikilink")（Wakefield）的[錫爾科茨公學](../Page/錫爾科茨公學.md "wikilink")（Silcoates
School），後來於1893年升讀[劍橋大學](../Page/劍橋大學.md "wikilink")[皇后學院](../Page/劍橋大學皇后學院.md "wikilink")，其中在1896年的數學優等考試中名列第11名（11th
Wrangler），同年亦以[文學士資格畢業](../Page/文學士.md "wikilink")。在1931年，貝璐再獲皇后學院頒授[文學碩士學位](../Page/文學碩士.md "wikilink")。

### 殖民地生涯

從劍橋大學畢業後，貝璐在1897年前往[馬來亞](../Page/馬來亞.md "wikilink")，在當地的殖民地政府任職[官學生](../Page/官學生.md "wikilink")，未幾先後在1898年和1899年於[高淵和](../Page/高淵.md "wikilink")[大山腳擔任署理地方](../Page/大山腳.md "wikilink")[民政事務專員](../Page/民政事務專員.md "wikilink")。另在1901年以前，他亦一直在[威省內任同職](../Page/威省.md "wikilink")。

在1902年，貝璐被改調到[星加坡出任署理助理輔政司](../Page/星加坡.md "wikilink")。但不久以後，他在1905年派回[檳城改任署理助理裁判司兼法醫官](../Page/檳城.md "wikilink")。至1908年，貝璐改任審計官，到1909年轉到[馬來聯邦管轄下的](../Page/馬來聯邦.md "wikilink")[雪蘭莪出任署理參政司秘書](../Page/雪蘭莪.md "wikilink")，1910年改於[下霹靂州任署理地方](../Page/霹靂州.md "wikilink")[民政事務專員](../Page/民政事務專員.md "wikilink")。在1911年，貝璐調回檳城，擢升為檳城市議會專員署主席，期間曾在1917年2月至10月署任檳城[參政司](../Page/參政司.md "wikilink")（Resident
Councillor）。

在1918年，貝璐累遷為星加坡市議會專員署主席，到1919年轉任馬來聯邦食物管制官（Food
Controller），至1920年擢升到[海峽殖民地](../Page/海峽殖民地.md "wikilink")（三洲府）任勞工署署長（Controller
of
Labour），其中又曾於1921年在當地政府兼任歐籍人士失業委員會主席。在1922年，貝璐出任英國駐[吉打地方政府顧問](../Page/吉打.md "wikilink")，期間曾在1925年5月至7月同時署任檳城參政司之職。未幾在1926年，貝璐終獲擢升為馬來聯邦[布政司](../Page/布政司.md "wikilink")，當中在1927年5月至6月，他更署任了馬來聯邦[護督兼高級專員之職](../Page/護督.md "wikilink")。

貝璐在1930年卸任馬來聯邦布政司，被改派到[香港接替](../Page/香港.md "wikilink")[金文泰爵士](../Page/金文泰.md "wikilink")，升任第18任[總督](../Page/香港總督.md "wikilink")。貝璐隨後在1930年5月9日抵達香港的[皇后碼頭](../Page/皇后碼頭.md "wikilink")，正式履新。其實，他早在1927年任馬來亞護督的時候，英政府就曾詢問貝璐，問他是否願意擔任一個重要性較低的總督級職位；當時貝璐表示同意，結果就在1930年，他就獲委任當港督。

相較於前任港督金文泰爵士，貝璐為人較為隨和，作風沉穩，對[英政府持合作的態度](../Page/英國.md "wikilink")，並不會像金文泰爵士時常與英政府持相左意見，甚而反對英政府的政策。在當時，由於英政府財政緊絀，所以對殖民地政府的事務，不論大小都愛一一過問，故此，貝璐就成了英政府眼中理想的港督人選。

### 香港總督

#### 社會建設

貝璐十分關注香港的社會建設，首先在醫療方面，他任內於1934年建成了[長洲醫院](../Page/長洲醫院.md "wikilink")，並且在1933年正式動土，在港島興建[遠東規模最大的](../Page/遠東.md "wikilink")[瑪麗醫院](../Page/瑪麗醫院.md "wikilink")。至於在[食水方面](../Page/食水.md "wikilink")，貝璐在1931年建成了[九龍副水塘與](../Page/九龍副水塘.md "wikilink")[香港仔水塘](../Page/香港仔水塘.md "wikilink")，其後又於1935年展開[城門水塘第三期工程](../Page/城門水塘.md "wikilink")。城門水塘第二期工程展開之時，時值英皇[佐治五世登基](../Page/佐治五世.md "wikilink")25週年，貝璐遂將城門水塘更名「銀禧水塘」。此外，在貝璐任內亦見證了香港[電話線路轉駁全面自動化](../Page/電話.md "wikilink")。

[Qmh1947.jpg](https://zh.wikipedia.org/wiki/File:Qmh1947.jpg "fig:Qmh1947.jpg")在貝璐任內動工興建。\]\]
香港的[交通在貝璐任內亦有很大發展](../Page/香港交通.md "wikilink")，在1933年，他正式批准[油蔴地小輪引入汽車渡輪服務](../Page/油蔴地小輪.md "wikilink")，從此[汽車可以在港](../Page/汽車.md "wikilink")、九兩地行駛；同年6月11日，他又分別向[九巴和](../Page/九巴.md "wikilink")[中巴兩所](../Page/中華巴士.md "wikilink")[巴士公司發放為期](../Page/巴士.md "wikilink")15年的[專營權](../Page/專營權.md "wikilink")，准許九巴專營[九龍](../Page/九龍.md "wikilink")、[新界和](../Page/新界.md "wikilink")[離島的公共巴士服務](../Page/離島.md "wikilink")，而中巴則專營於[香港島地區](../Page/香港島.md "wikilink")。發放專營權一事，結束香港公共巴士自由競爭的局面；與此同時，貝璐又借機會引入新的條款，加強監管本地公共巴士服務。當中，港府除規定兩所巴士公司都要把收益的某個百份比撥歸政府外，政府更對巴士車資作出監管。此外，巴士公司又須由英籍人士組成，而巴士也要購自[英國](../Page/英國.md "wikilink")。在[航空交通方面](../Page/航空.md "wikilink")，貝璐上任不久以後，[粵](../Page/粵.md "wikilink")、港兩地機場開始互航，而[啟德機場的](../Page/啟德機場.md "wikilink")[指揮塔和飛機庫亦在其任內落成](../Page/指揮塔.md "wikilink")。

在市政服務方面，由於香港的家居排污問題引起了社會關注，[立法局遂在](../Page/立法局.md "wikilink")1935年3月通過了《1935年市政局條例》，決定改組原潔淨局為[市政局](../Page/市政局.md "wikilink")。新的市政局要到貝璐卸任後，至1936年1月才告正式成立。市政局除大致擁有潔淨局的權力外，更加入了民選議員的席位。

#### 社會風氣

##### 賣淫活動

自[國際聯盟在](../Page/國際聯盟.md "wikilink")1919年成立以後，香港的社會風氣問題開始備受聯盟的關注，而且還成為了國際議題。國際聯盟一直希望香港政府禁絕境內的[賣淫活動](../Page/賣淫.md "wikilink")；可是，歷任的[港督均認為港府已訂立了相當的法例](../Page/港督.md "wikilink")，對黃色事業作十分有效的規管，而且秦樓楚館一類煙花之地在[中國古已有之](../Page/中國.md "wikilink")，所以不便禁絕香港傳統的賣淫活動。在這種背景之下，港督的意見得到[英國保守黨政府的默認](../Page/英國保守黨.md "wikilink")，所以終1921年代，有關禁絕賣淫的提議始終沒有在香港落實。

然而，在貝璐上任後，時值[英國工黨剛剛上台執政](../Page/英國工黨.md "wikilink")；而由於工黨政府與國際聯盟的立場一致，所以禁絕賣淫活動之聲再起。貝璐初時反對禁絕賣淫，指出港府對賣淫業的規管打理，「比起[倫敦政府處理國務還要井井有條](../Page/倫敦.md "wikilink")」；他又認為，[華人社會中的](../Page/華人.md "wikilink")[妓女並非如西方所想的街頭孤兒](../Page/妓女.md "wikilink")，她們往往能夠成為大戶人家的[妾侍](../Page/妾侍.md "wikilink")，所以禁絕賣淫並不可取。儘管貝璐的努力辯解，但工黨政府的態度仍然十分強硬，結果貝璐唯有屈從。經港府查證後，貝璐向英政府匯報，香港共有6所由[歐洲人經營的妓院](../Page/歐洲人.md "wikilink")，內面共有17名領有牌照的歐籍[妓女](../Page/妓女.md "wikilink")；此外，全港又有222所[中國和](../Page/中國.md "wikilink")[日本人開辦的妓院](../Page/日本.md "wikilink")，內面共計有2,657名領牌妓女。

資料發表後不久，國際聯盟在1931年派出一隊人口販賣調查團到香港。貝璐事後遂根據調查團的建議，在1932年6月勒令關閉所有由歐洲人經營的妓院，而歐籍妓女則被遞解離境；後到1934年6月，所有華人妓院亦遭到勒令關閉。不過，出乎西方輿論意料的是，賣淫活動被全面禁絕後，香港的社會風氣反而沒有得到改善。賣淫活動轉到地下發展，在[灣仔一帶更出現不少流鶯在街頭流連](../Page/灣仔.md "wikilink")，此外，缺乏了以往的監察，駐港英軍染[性病的比例亦由以往的](../Page/性病.md "wikilink")7%急升至24%，可見禁絕賣淫的政策並不成功。

##### 鴉片販賣

除了賣淫活動以外，工黨政府同時聯結了[英國反鴉片聯盟](../Page/英國反鴉片聯盟.md "wikilink")，向港府施壓要求禁售[鴉片](../Page/鴉片.md "wikilink")。在種種壓力下，貝璐遂在1931年開始立法禁煙，除吊銷公煙的專賣牌照外，又不得市民私藏與吸食鴉片。在當時，英商已不再是鴉片的主要供應商，相反，內地[軍閥為了籌集經費而迫令農民改植鴉片](../Page/軍閥.md "wikilink")，所以中國大陸成為了鴉片的主要來源地。有見及此，貝璐又特設了緝私隊追緝私煙。

總體上，禁售鴉片對香港帶來了一定的負面影響，因為這除了使私煙不絕外，市面上亦衍生不少非法煙館，不少人更因為吸食或販賣鴉片而被定罪，以致[監獄出現過份擠迫的問題](../Page/監獄.md "wikilink")；而禁售鴉片以後，[警察受賄的情況也有所惡化](../Page/警察.md "wikilink")。所以貝璐曾經抱怨禁售鴉片與當年[美國](../Page/美國.md "wikilink")[禁酒一樣](../Page/禁酒.md "wikilink")，將不會取得成功。

此外，禁售鴉片使港府的財政構成沉重打擊。在過往，銷售合法鴉片一直是港府的主要財政來源之一，但是在禁售鴉片以後，猖獗的私煙販賣使港府白白失去了這方面的財政收入，結果，貝璐唯有大幅增收[遺產稅](../Page/遺產稅.md "wikilink")、成藥稅和娛樂稅，以彌補禁售鴉片所導致的財政赤字，經過一番努力，到1936年的時候，鴉片收益已大幅減至政府總收入的百分之一。在此以後，港府繼續禁售鴉片，到1945年9月20日，港府更正式宣佈禁絕鴉片貿易。不過，鴉片在那時已經式微，[可卡因和](../Page/可卡因.md "wikilink")[海洛英一類的新興](../Page/海洛英.md "wikilink")[毒品已成地下市場的主流](../Page/毒品.md "wikilink")。

#### 經濟不景

在1929年，[美國](../Page/美國.md "wikilink")[華爾街爆發了](../Page/華爾街.md "wikilink")[全球性股災](../Page/華爾街股災.md "wikilink")，一時間使[歐洲和美國陷入了長久的](../Page/歐洲.md "wikilink")[大蕭條之中](../Page/大蕭條.md "wikilink")。身在[遠東的](../Page/遠東.md "wikilink")[香港最初仍能夠置身於事外](../Page/香港.md "wikilink")，可是隨著1931年國際[白銀市場崩潰](../Page/银.md "wikilink")，白銀的跌價立即導致奉行[銀本位的港元大幅](../Page/銀本位.md "wikilink")[貶值](../Page/貶值.md "wikilink")；以往在1920年代，每一港元大約可兌三[仙令](../Page/仙令.md "wikilink")，但是白銀跌價後，每一港元只可兌換小於一仙令，意味要多於20港元才能兌換到一[鎊](../Page/鎊.md "wikilink")。港元貶值後，[倫敦曾派一貨幣委員會來港](../Page/倫敦.md "wikilink")，事後建議一日[中國維持銀本位](../Page/中國.md "wikilink")，香港也應當跟從。可是，在1934年，由於[美國政府大舉搜購](../Page/美國.md "wikilink")[白銀](../Page/银.md "wikilink")，遂使中、港銀價大幅反彈，而[國民政府於](../Page/國民政府.md "wikilink")1935年宣佈放棄[銀本位後](../Page/銀本位.md "wikilink")，港府為免港元進一步升值，在同年11月宣佈放棄銀本位，但當時貝璐已經卸任。

面對經濟不景，貝璐曾在1932年設立一個[節約委員會](../Page/節約委員會.md "wikilink")（Retrenchment
Commission），以設法緊縮政府開支。該委員會建議政府應引入更多[華籍](../Page/華籍.md "wikilink")[公務員](../Page/公務員.md "wikilink")，以取代薪酬相對較高的外籍公務員。港府最初在市政和衛生部門執行有關建議，當中包括開始招聘華籍[護士到](../Page/護士.md "wikilink")[公立醫院工作](../Page/公立醫院.md "wikilink")。此外，有鑑於於[公務員薪俸以](../Page/公務員.md "wikilink")[英鎊結算](../Page/英鎊.md "wikilink")，貝璐亦曾下調公務員薪酬，以節省政府開支。到1934年，貝璐更發行了2,000萬元[公債](../Page/債券.md "wikilink")，用於地方建設之上，以收刺激就業之效。

總括而言，儘管港元因為大蕭條而貶值，但是香港的經濟卻沒有比想像中差，因為港元貶值期間增強殖民地的競爭力，貿易額亦因而上升。在這種背景下，貝璐任內港府的財政收入一直穩步增長，而且更首次逼近3,000萬港元的水-{平}-。

### 晚年

貝璐在1935年5月17日卸任港督之職，返回[英國](../Page/英國.md "wikilink")，展開[退休生活](../Page/退休.md "wikilink")。貝璐晚年退居於英格蘭[東薩西克斯郡的](../Page/東薩西克斯郡.md "wikilink")[貝克斯希爾](../Page/貝克斯希爾.md "wikilink")（Bexhill），後於1945年2月24日，即[生日的](../Page/生日.md "wikilink")3天前卒於[倫敦](../Page/倫敦.md "wikilink")，享年69歲。

## 家庭

貝璐在1911年與維爾勒·瑪麗·德雷克（Violet Mary
Drake）[結婚](../Page/結婚.md "wikilink")，她的父親則名W·D·萊恩（W.
D. Laing）。貝璐與貝夫人共生有兩名兒子，他們是：

  - [拿督約翰·貝璐爵士](../Page/威廉·約翰·貝璐.md "wikilink")（Dato Sir John
    Peel，1912年6月16日—2004年5月8日），[英國殖民地官員](../Page/英國.md "wikilink")，[保守黨政治家及](../Page/英國保守黨.md "wikilink")[歐洲議會成員](../Page/歐洲議會.md "wikilink")。[1](http://politics.guardian.co.uk/politicsobituaries/story/0,1441,1216588,00.html#article_continue)[2](http://www.thefreelibrary.com/Negara+Brunei+Darussalam:+obituaries+2004-a0134382050)[3](http://rulers.org/indexp2.html)
  - [R·L·貝璐](../Page/R·L·貝璐.md "wikilink")（Richard Laing
    Peel，1914年—2002年），[英國殖民地官員](../Page/英國.md "wikilink")。[4](https://web.archive.org/web/20070510183845/http://www.queens.cam.ac.uk/queens/Record/2002/Old%20Members/deaths.html)

## 雜記

  - 貝璐三父子皆於[劍橋大學](../Page/劍橋大學.md "wikilink")[皇后學院畢業](../Page/劍橋大學皇后學院.md "wikilink")，貝璐在1893年入讀，而其長子與次子則分別在1930年和1933年入讀。
  - 貝璐熱愛到郊外[騎馬和打](../Page/騎馬.md "wikilink")[高爾夫球](../Page/高爾夫球.md "wikilink")，任港督時更興建[粉嶺別墅](../Page/行政長官粉嶺別墅.md "wikilink")。總督別墅於1934年建成，貝璐每逢假日都愛到那裡避暑。
  - 貝璐曾於1934年10月17日為[香港上海滙豐銀行總行立下奠基石](../Page/香港上海滙豐銀行.md "wikilink")，該行是當時香港首幢安裝[冷氣系統的商業樓宇](../Page/冷氣.md "wikilink")。

## 榮譽

[HK_WC_修頓花園_Southorn_Garden_3.jpg](https://zh.wikipedia.org/wiki/File:HK_WC_修頓花園_Southorn_Garden_3.jpg "fig:HK_WC_修頓花園_Southorn_Garden_3.jpg")
[King_george_v_school_front_facade_2004.jpg](https://zh.wikipedia.org/wiki/File:King_george_v_school_front_facade_2004.jpg "fig:King_george_v_school_front_facade_2004.jpg")

### 勳銜

  - [C.M.G.](../Page/CMG.md "wikilink") （1926年）
  - [K.B.E.](../Page/KBE.md "wikilink") （1928年）
  - [K.C.M.G.](../Page/KCMG.md "wikilink") （1931年）
  - [K.St.J.](../Page/聖約翰美德勳章.md "wikilink") （1931年）

### 榮譽院士

  - [皇后學院](../Page/劍橋大學皇后學院.md "wikilink") （1932年）

### 榮譽法學博士

  - [香港大學](../Page/香港大學.md "wikilink") （1935年）

### 以他命名的事物

  - [貝璐道](../Page/貝璐道.md "wikilink")，連接[香港仔及](../Page/香港仔.md "wikilink")[太平山的道路](../Page/太平山_\(香港\).md "wikilink")，原名「鴨巴甸新路」，1960年改現名。

  - [貝夫人健康院](../Page/貝夫人健康院.md "wikilink")，原址位於[灣仔](../Page/灣仔.md "wikilink")[修頓中心](../Page/修頓中心.md "wikilink")，於2006年遷往[鄧肇堅醫院](../Page/鄧肇堅醫院.md "wikilink")。[5](http://www.info.gov.hk/gia/general/200606/05/P200606050193.htm)

  - [貝夫人美沙酮診所](../Page/貝夫人美沙酮診所.md "wikilink")，位於[灣仔](../Page/灣仔.md "wikilink")[柯布連道](../Page/柯布連道.md "wikilink")。[6](http://www.dh.gov.hk/tc_chi/tele/tele_chc/tele_chc_mc.html)

  - 貝璐樓，位於香港[英皇佐治五世學校內](../Page/英皇佐治五世學校.md "wikilink")。

  - [貝璐工程實驗室](../Page/貝璐工程實驗室.md "wikilink")，原於[香港大學內](../Page/香港大學.md "wikilink")，在1981年拆卸。

  - ，位於[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")[蕉賴的道路](../Page/蕉賴.md "wikilink")，[排檔食肆林立](../Page/排檔.md "wikilink")，為當地有名的[夜市和](../Page/夜市.md "wikilink")[食街](../Page/食街.md "wikilink")。

## 請參見

  - [銀本位](../Page/銀本位.md "wikilink")
  - [馬來亞](../Page/馬來亞.md "wikilink")
  - [香港總督](../Page/香港總督.md "wikilink")

## 參考資料

<div class="references-small">

  - *Who was Who, 1941-1950*，A & C Black。
  - *Excellency : the governors of Hong Kong*，Russell Spurr，Hong
    Kong：FormAsia，1995年。
  - *A History of Hong Kong*，Frank Welsh，HarperCollins。
  - *香港掌故趣聞小博士*，嚴吳嬋霞，新雅文化事業有限公司，1996年3月。
  - *[Eminent
    Alumni](https://web.archive.org/web/20080506084912/http://www.queens.cam.ac.uk/Queens/Misc/Eminent.html)*，劍橋大學皇后學院，2007年7月18日造訪。
  - *[Full Details for Query
    \#64226](http://www.cousinconnect.com/d/a/64226)*，CousinConnect.com，2007年7月18日造訪。
  - *[COUNCIL MEETING](http://www.legco.gov.hk/1933/h331012.pdf)*，HONG
    KONG LEGISLATIVE COUNCIL MEETING，1933年10月12日。
  - *[COUNCIL MEETING](http://www.legco.gov.hk/1934/h341018.pdf)*，HONG
    KONG LEGISLATIVE COUNCIL MEETING，1934年10月18日。
  - *[Malaysia](http://www.worldstatesmen.org/Malaysia.htm)*，WorldStatesmen.org，2007年7月18日造訪。
  - *[Malay
    States](http://www.worldstatesmen.org/Malay_states.htm)*，WorldStatesmen.org，2007年7月18日造訪。
  - *[香港貨幣制度的興替](http://www.info.gov.hk/hkma/eng/moneyhk/big5/moneypas/monetsys_2.htm)*，香港金融管理局，2007年7月18日造訪。

</div>

## 外部連結

  - **立法局會議記錄**（英文）
      - [1930年](http://www.legco.gov.hk/1930/yr1930.htm)
      - [1931年](http://www.legco.gov.hk/1931/yr1931.htm)
      - [1932年](http://www.legco.gov.hk/1932/yr1932.htm)
      - [1933年](http://www.legco.gov.hk/1933/yr1933.htm)
      - [1934年](http://www.legco.gov.hk/1934/yr1934.htm)
      - [1935年](http://www.legco.gov.hk/1935/yr1935.htm)

<!-- end list -->

  - **其他**
      - [香港大學贊辭](http://www3.hku.hk/eroonweb/hongrads/person.php?id=108)，1935年發佈
      - [貝璐爵士於港大領受學位後致辭](http://www3.hku.hk/eroonweb/hongrads/address.php?cong=26)

[P](../Category/香港總督.md "wikilink") [P](../Category/KBE勳銜.md "wikilink")
[P](../Category/剑桥大学王后学院校友.md "wikilink")
[P](../Category/香港大學名譽博士.md "wikilink")