在[代數拓撲學中](../Page/代數拓撲學.md "wikilink")，[拓撲空間之](../Page/拓撲空間.md "wikilink")**貝蒂數**
\(b_0, b_1, b_2, \ldots\) 是一族重要的不變量，取值為非負整數或無窮大。直觀地看，\(b_0\)
是[連通分支之個數](../Page/連通分支.md "wikilink")，\(b_1\)
是沿著閉曲線剪開空間而保持連通的最大剪裁次數。更高次的 \(b_k\)
可藉[同調群定義](../Page/同調.md "wikilink")。

「貝蒂數」一詞首先由[龐加萊使用](../Page/龐加萊.md "wikilink")，以義大利數學家[恩里科·貝蒂命名](../Page/恩里科·貝蒂.md "wikilink")。

## 定義

空間 \(X\) 的第 \(k\) 個貝蒂數（\(k\) 為非負整數）定義為

  -
    \(b_k = \dim H_k(X; \mathbb{Q})\)

上式的同調群可以任意[域為係數](../Page/域_\(數學\).md "wikilink")。

## 例子

  - 圓環 \(S^1\) 的貝蒂數依次為 \(1,1,0,0,0, \ldots\)。
  - 二維環面的貝蒂數依次為 \(1,2,1,0,0,0,\ldots\)。
  - 三維環面的貝蒂數依次為 \(1,3,3,1,0,0,0,\ldots\)。
  - 一般而言，\(n\)
    維環面的貝蒂數由[二項式係數給出](../Page/二項式係數.md "wikilink")，此命題可透過下節敘述的性質證明。
  - 無窮維空間可以有無窮多個非零的貝蒂數，例如無窮維複[射影空間](../Page/射影空間.md "wikilink")
    \(\mathbb{P}^\infty\) 的貝蒂數依次為 \(1,0,1,0,1,\ldots\)（週期為二）。

## 性質

閉曲面的第一個貝蒂數描述了曲面上的「洞」數。[環面之](../Page/環面.md "wikilink")
\(b_1 = 2\)；一般而言，閉曲面的 \(b_1\) 等於「洞」或「把手」個數之兩倍。可定向緊閉曲面可由其
\(b_1\) 完全分類。

有限[單純複形或](../Page/單純複形.md "wikilink")[CW複形的貝蒂數有限](../Page/CW複形.md "wikilink")。當
\(k\) 大於複形維度時，\(b_k=0\)。

對於有限 CW 複形，定義其**龐加萊多項式**為貝蒂數的[生成函數](../Page/生成函數.md "wikilink")

  -
    \(P_X(z) := \sum_k b_k z^k\)

對於任意 \(X, Y\)，有

\[P_{X\times Y}(z) = P_X(z) P_Y(z) .\]

對於 \(n\)-維可定向閉[流形](../Page/流形.md "wikilink")
\(X\)，[龐加萊對偶定理給出貝蒂數的對稱性](../Page/龐加萊對偶定理.md "wikilink")

  -
    \(b_k(X) = b_{n-k}(X)\)

## 貝蒂數與微分形式

在[微分幾何及](../Page/微分幾何.md "wikilink")[微分拓撲中](../Page/微分拓撲.md "wikilink")，所論的空間
\(X\) 通常是閉[流形](../Page/流形.md "wikilink")，此時拓撲不變量 \(b_k\)
可以由源自流形微分結構的[微分形式計算](../Page/微分形式.md "wikilink")。具體言之，考慮複形

  -
    \(0 \to A^0(X) \stackrel{d}{\to} A^1(X) \stackrel{d}{\to} A^2(X) \to \cdots\)

其中 \(A^k(X)\) 表 \(k\) 次微分形式構成的向量空間，\(d\)
為[外微分](../Page/外微分.md "wikilink")。則

  -
    \(b_k = \dim \dfrac{\mathrm{Ker}(d: A^k \to A^{k+1})}{\mathrm{Im}(d: A^{k-1} \to A^k)}\)

這是[德拉姆上同調理論的簡單推論](../Page/德拉姆上同調.md "wikilink")。

德拉姆上同調的不便之處，在於它考慮的是微分形式的[等價類](../Page/等價類.md "wikilink")，其間可差一個
\(\mathrm{Im}(d: A^{k-1} \to A^k)\) 之元素。設流形 \(X\)
具有[黎曼度量](../Page/黎曼度量.md "wikilink")，則可以定義微分形式的「長度」。我們若嘗試以[變分法在等價類中找最短元素](../Page/變分法.md "wikilink")，透過形式計算可知存在唯一最短元素
\(\omega \in A^k(X)\)，且為**調和形式**
：\(\Delta \omega = 0\)，在此[拉普拉斯算子](../Page/拉普拉斯算子.md "wikilink")
\(\Delta\)
依賴於流形的度量，在局部座標系下可表為橢圓偏微分算子。這套想法催生的[霍奇理論在複幾何中扮演關鍵角色](../Page/霍奇理論.md "wikilink")。

## 文獻

  - F.W. Warner, *Foundations of differentiable manifolds and Lie
    groups*, Springer (1983).
  - J.Roe, *Elliptic Operators, Topology, and Asymptotic Methods*,
    Second Edition (Research Notes in Mathematics Series **395**),
    Chapman and Hall (1998).

[Category:代數拓撲](../Category/代數拓撲.md "wikilink")
[Category:拓扑图论](../Category/拓扑图论.md "wikilink")