**貝里肯龍屬**（[學名](../Page/學名.md "wikilink")：*Blikanasaurus*）是[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍的一個](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生活於[三疊紀晚期的](../Page/三疊紀.md "wikilink")[南非](../Page/南非.md "wikilink")[開普省](../Page/開普省.md "wikilink")。[化石只是一塊左後腿骨](../Page/化石.md "wikilink")，發現於[下艾略特組](../Page/下艾略特組.md "wikilink")。貝里肯龍曾被分類為基礎[原蜥腳下目恐龍或基礎](../Page/原蜥腳下目.md "wikilink")[蜥腳下目恐龍](../Page/蜥腳下目.md "wikilink")。[模式種是](../Page/模式種.md "wikilink")**克氏貝里肯龍**（*Blikanasaurus
cromptoni*），是由[彼得·加爾東](../Page/彼得·加爾東.md "wikilink")（Peter Galton）及van
Heerden在1985年描述、命名的。

彼得·加爾東與van
Heerden同時建立[貝里肯龍科](../Page/貝里肯龍科.md "wikilink")（Blikanasauridae）[演化支](../Page/演化支.md "wikilink")。貝里肯龍科包含貝里肯龍與其近親。然而，目前不清楚有哪些其他屬列於本科內。貝里肯龍科不在現代分類學中被使用，[麥可·波頓](../Page/麥可·波頓.md "wikilink")（Michael
J.
Benton）在2004的恐龍科普書籍並沒有將貝里肯龍科列入。貝里肯龍科過去一度被認為屬於[原蜥腳下目](../Page/原蜥腳下目.md "wikilink")，但最近的研究顯示牠們可能是基礎[蜥腳下目恐龍](../Page/蜥腳下目.md "wikilink")。

## 外部連結

  - [蜥腳下目](https://web.archive.org/web/20090425151759/http://www.thescelosaurus.com/sauropoda.htm)
  - [貝里肯龍 - Paleobiology
    Database網站](http://paleodb.org/cgi-bin/bridge.pl?action=checkTaxonInfo&taxon_no=38651&is_real_user=0)
  - <http://web.me.com/dinoruss/de_4/5a6bd1d.htm>

[Category:蜥腳下目](../Category/蜥腳下目.md "wikilink")
[Category:非洲恐龍](../Category/非洲恐龍.md "wikilink")
[Category:三疊紀恐龍](../Category/三疊紀恐龍.md "wikilink")