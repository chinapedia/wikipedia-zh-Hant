**貞元**（976年七月十三至978年十一月廿九）是[日本的](../Page/日本.md "wikilink")[年號](../Page/年號.md "wikilink")。使用這年號之[日本天皇是](../Page/日本天皇.md "wikilink")[圓融天皇](../Page/圓融天皇.md "wikilink")。

## 改元

  - 天延四年七月十三（976年8月11日）：改元。
  - 貞元三年十一月廿九（978年12月31日）：改元[天元](../Page/天元_\(圓融天皇\).md "wikilink")。

## 出處

## 大事記

## 出生

## 逝世

## 紀年、干支、西曆對照表

| 貞元                             | 元年                             | 二年                             | 三年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元.md "wikilink") | 976年                           | 977年                           | 978年                           |
| [干支](../Page/干支.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 其他使用[貞元為紀年的政權](../Page/貞元.md "wikilink")
  - 同期存在的其他政權之紀年
      - [廣運](../Page/廣運_\(劉繼元\).md "wikilink")（974年至979年五月）：[北漢](../Page/北漢.md "wikilink")—[劉繼元之年號](../Page/劉繼元.md "wikilink")
      - [開寶](../Page/開寶.md "wikilink")（968年十一月至976年）：[北宋](../Page/北宋.md "wikilink")—太祖[趙匡胤之年號](../Page/趙匡胤.md "wikilink")
      - [太平興國](../Page/太平興國.md "wikilink")（976年十二月至984年十一月）：北宋—太宗[趙匡義之年號](../Page/趙匡義.md "wikilink")
      - [明政](../Page/明政_\(段素順\).md "wikilink")（969年至985年）：[大理國](../Page/大理國.md "wikilink")—[段素順之年號](../Page/段素順.md "wikilink")
      - [太平](../Page/太平_\(丁部領\).md "wikilink")（970年至980年）：[丁朝](../Page/丁朝.md "wikilink")—[丁部領](../Page/丁部領.md "wikilink")、[丁璿之年號](../Page/丁璿.md "wikilink")
      - [保寧](../Page/保寧.md "wikilink")（969年二月至979年十一月）：[遼](../Page/遼朝.md "wikilink")—[遼景宗耶律賢之年號](../Page/遼景宗.md "wikilink")
      - [元興](../Page/元興_\(定安國\).md "wikilink")（976年起）：[定安國](../Page/定安國.md "wikilink")—[烏元明之年號](../Page/烏元明.md "wikilink")
      - [天尊](../Page/天尊.md "wikilink")（967年至977年）：[于闐](../Page/于闐.md "wikilink")—[尉遲蘇拉之年號](../Page/尉遲蘇拉.md "wikilink")
      - [中興](../Page/中興_\(尉遲達磨\).md "wikilink")（978年至985年）：于闐—[尉遲達磨之年號](../Page/尉遲達磨.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129

[Category:10世纪日本年号](../Category/10世纪日本年号.md "wikilink")
[Category:970年代日本](../Category/970年代日本.md "wikilink")