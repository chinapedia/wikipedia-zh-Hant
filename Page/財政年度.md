**財政年度**，又稱**會計年度**，是指[公司或](../Page/公司.md "wikilink")[國家每年制定](../Page/國家.md "wikilink")[預算或計算](../Page/預算.md "wikilink")[收入的統計時間](../Page/收入.md "wikilink")。但每個國家或其[法例所轄的](../Page/法例.md "wikilink")[組織各有不同](../Page/组织_\(社会学\).md "wikilink")，大抵分成兩類：一是[曆年制](../Page/曆年制.md "wikilink")，一是[跨年制](../Page/跨年制.md "wikilink")。

## 曆年制

曆年制即是由1月1日起，使用曆年制有[中国](../Page/中国.md "wikilink")、[德国](../Page/德国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[波兰](../Page/波兰.md "wikilink")、[奥地利](../Page/奥地利.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[北朝鮮](../Page/北朝鮮.md "wikilink")、[韩国](../Page/韩国.md "wikilink")、[南斯拉夫](../Page/南斯拉夫.md "wikilink")、[中華民國等](../Page/中華民國.md "wikilink")。

## 跨年制

  - [美國](../Page/美國.md "wikilink")

[美國聯邦政府](../Page/美國聯邦政府.md "wikilink")2010年財政年度（FY10/FY09-10）如下：

  - 第一季：2009年10月1日-2009年12月31日
  - 第二季：2010年1月1日-2010年3月31日
  - 第三季：2010年4月1日-2010年6月30日
  - 第四季：2010年7月1日-2010年9月30日

<!-- end list -->

  - [加拿大](../Page/加拿大.md "wikilink")、印度、香港、澳門及日本

4月1日開始，3月31日結束

  -
    香港及澳門的中小學大多從9月1日開始，到翌年8月31日結束

<!-- end list -->

  - [澳大利亞](../Page/澳大利亞.md "wikilink")、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[紐西蘭](../Page/紐西蘭.md "wikilink")（政府）

7月1日開始，6月30日結束

## 各國/地區的財政年度列表

| 國家                                         |
| ------------------------------------------ |
| 國家                                         |
| [澳門](../Page/澳門.md "wikilink")             |
| 中小學                                        |
| [澳洲](../Page/澳洲.md "wikilink")             |
| [加拿大](../Page/加拿大.md "wikilink")           |
| [香港](../Page/香港.md "wikilink")             |
| 中小學                                        |
| [印度](../Page/印度.md "wikilink")             |
| [中華人民共和國](../Page/中華人民共和國.md "wikilink")   |
| [葡萄牙](../Page/葡萄牙.md "wikilink")           |
| [中華民國](../Page/中華民國.md "wikilink")         |
| [韩国](../Page/韩国.md "wikilink")             |
| [埃及](../Page/埃及.md "wikilink")             |
| [愛爾蘭](../Page/愛爾蘭共和國.md "wikilink")        |
| [日本](../Page/日本.md "wikilink")             |
| 企業以及個人                                     |
| [紐西蘭](../Page/紐西蘭.md "wikilink")           |
| 企業以及個人                                     |
| [巴基斯坦](../Page/巴基斯坦.md "wikilink")         |
| [南非](../Page/南非.md "wikilink")             |
| [瑞典](../Page/瑞典.md "wikilink")             |
| 企業                                         |
|                                            |
|                                            |
|                                            |
| [阿拉伯聯合大公國](../Page/阿拉伯聯合大公國.md "wikilink") |
| [英國](../Page/英國.md "wikilink")             |
| [美國](../Page/美國.md "wikilink")             |
| 國家                                         |

[Category:財務管理](../Category/財務管理.md "wikilink")