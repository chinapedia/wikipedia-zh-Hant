{{ Expand |time=2010-09-24}}
[貢丸米粉湯.jpg](https://zh.wikipedia.org/wiki/File:貢丸米粉湯.jpg "fig:貢丸米粉湯.jpg")。\]\]
[HK_Food_Yuen_Long_Fish_Ball.JPG](https://zh.wikipedia.org/wiki/File:HK_Food_Yuen_Long_Fish_Ball.JPG "fig:HK_Food_Yuen_Long_Fish_Ball.JPG")
**摃丸**俗寫白字**貢丸**為豬肉製做的一種肉丸，在中國南部的福建，廣東，台灣及香港都有此食品。在[台灣以](../Page/台灣.md "wikilink")[新竹市城隍廟一帶出產的最為有名](../Page/新竹次都會區.md "wikilink")，在[廣東和](../Page/廣東.md "wikilink")[香港等地亦非常普遍](../Page/香港.md "wikilink")。在香港，貢丸是指有[香菇碎粒的豬肉丸](../Page/香菇.md "wikilink")，而沒有香菇的只稱為豬肉丸。

## 製作及名稱

由於肉丸的重點步驟，就是「搥打」，而閩南語中搥打（摃）讀作「貢」，「摃丸」(kòng-uân)因而得名。

新竹的貢丸特別有名，嚴格來說，貢丸到處都有人會做，也非新竹一帶的豬肉特別鮮美。新竹對貢丸的貢獻，主要是科技上的：運用機器，令貢丸可大量生產。新竹的貢丸是以木棒或機器「摃（槌擊肉塊）」出來的。而新竹貢丸的材料必須用剛宰殺不久的新鮮[溫體豬肉製作](../Page/溫體豬肉.md "wikilink")，也就是所謂的「活肉」。

## 材料

主要是豬肉（或大豆蛋白，如果是素的），有時會加香菇，常用的添加物有鹽、胡椒粉、味精和糖。

## 料理方式

  - 煮湯
  - 滷製
  - 油炸
  - 炭烤
  - 火鍋食材之一

## 参考文献

## 外部連結

[Category:台灣食材](../Category/台灣食材.md "wikilink")
[Category:肉圓](../Category/肉圓.md "wikilink")
[Category:台灣小吃](../Category/台灣小吃.md "wikilink")
[Category:新竹縣市文化](../Category/新竹縣市文化.md "wikilink")