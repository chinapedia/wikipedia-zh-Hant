**貢寮區**是[中華民國](../Page/中華民國.md "wikilink")[新北市下轄的一個市轄區](../Page/新北市.md "wikilink")，是新北市、也是台灣本島最東端的行政區，區內的[三貂角是台灣本島的最東端](../Page/三貂角.md "wikilink")，東臨[太平洋](../Page/太平洋.md "wikilink")。

貢寮人口稀少、開發度低，因此境內的龍門被選為飽受爭議的[台電](../Page/台電.md "wikilink")[龍門核能發電廠](../Page/龍門核能發電廠.md "wikilink")（核四）之設置地點。

## 地理

貢寮區位居臺灣最東北角，東臨太平洋，為新北市唯一臨太平洋的行政區，南接[臺灣省](../Page/臺灣省.md "wikilink")[宜蘭縣](../Page/宜蘭縣.md "wikilink")[頭城鎮](../Page/頭城鎮.md "wikilink")，西接[雙溪區](../Page/雙溪區.md "wikilink")，北接[瑞芳區](../Page/瑞芳區.md "wikilink")，係新北市最東端的區鎮，總面積99.9680平方公里，為濱海丘陵地型，昔日以盛產海鮮名聞遐邇，近年來更以「反核四」名噪四方，其實本區『親山近水』美景，更是北臺灣最佳旅遊勝地。

本區地處[大陸沿岸流與](../Page/大陸沿岸流.md "wikilink")[菲律賓洋流交匯](../Page/菲律賓洋流.md "wikilink")[黑潮帶](../Page/黑潮.md "wikilink")，海洋資源非常豐富，且為[雪山山脈之起點](../Page/雪山山脈.md "wikilink")，境內群巒起伏，翠嶺層疊、景致極為優美，並有發人思古幽情、緬懷先人蓽路藍縷以開發蘭陽之[草嶺古道](../Page/草嶺古道.md "wikilink")，登高遠眺海天一色，近覽綿延長達30多公里海岸線，奇岩嶙峋、灣岬相間、蜿蜒有致，令人心曠神怡，沿岸鬼斧神工的海蝕地形與平坦岩灘交錯，更有長達三公里的金黃沙灘，不啻為登山、賞景、釣魚、[浮潛](../Page/浮潛.md "wikilink")、戲水的天堂。

### 地質地形

以三貂角為分界，三貂角以北的龍洞、鼻頭角至和平島的海岸屬沈降海岸地形，山脈與海岸線近乎呈垂直相交，受東北季風及強風巨浪之海蝕作用，多海岬、海灣及海崖。鼻頭角到三貂角，呈大型岬灣地形，如龍洞灣、龍洞岬、澳底、福隆和卯澳。三貂角以南至頭城海岸線，多為斷層海岸，東北、西南走向的地質構造，海岸線較平直，少海灣突岬，大部分為發育良好的海蝕平臺及礫石海岸。\[1\]

### 危險水域

  - 貢寮區危險水域有六處

## 歷史

本區古名**摃仔寮**，「摃仔」是臺灣北部的凱達格蘭[巴賽語](../Page/巴賽語.md "wikilink")「Kona」的漢譯，原意係捕捉野獸的「陷阱」，而為了等候捕捉山獸，
在陷阱附近搭建的草寮，就稱「摃仔寮」。

本區古為一片荒野，是巴賽族人聚落與狩獵之地，時名叫做「Ki-vanow-an」。

1626年，[西班牙海軍初到臺灣](../Page/西班牙海軍.md "wikilink")，航行到今日的三貂角一帶，在此地登陸，遂以[西班牙的](../Page/西班牙.md "wikilink")[聖人](../Page/聖人.md "wikilink")[聖迪雅哥](../Page/迪亞哥·德·阿爾卡拉.md "wikilink")（Santiago）為所登陸的岬角命名，現在習稱的三貂角就是「聖迪雅哥」音譯而成。但在西班牙統治這整個三貂地區，巴賽族人也不叫自己生活領域為「三貂社」而叫做「Kivanowan」（基瓦諾灣）。當時西班牙人在以貢寮區為主之地區設[省為](../Page/省.md "wikilink")「
哆囉滿」（Turoboan）。

在漢人勢力進入這地區以後，「Kivanowan」這個稱呼已不復存在，「三貂」這個名稱卻被保留下來，並用來當作整個區域的總稱，其他譯名也有稱做「山朝」或「三朝」者。
　
1773年至1795年，漳人[吳沙招募一批墾民來此開墾](../Page/吳沙.md "wikilink")。寄居本區丹裡（仁里里）因三十六社平埔族，散處在近港附近，時漢人極少進入，而吳沙通平埔族人語言，生性任俠、重信用、講義氣，與先住民通市甚得喜愛。日久一批窮困潦倒漢人前來投靠，並發給每人米一斗、斧一柄。入山採樵抽籐，於是投靠的人日漸增多，闢地也益廣，「槓仔寮」也一直沿用的地名。

1895年，[乙未戰爭之時](../Page/乙未戰爭.md "wikilink")，[日本的](../Page/日本.md "wikilink")[近衛師團由](../Page/近衛師團.md "wikilink")[北白川宮能久親王統率](../Page/北白川宮能久親王.md "wikilink")，於5月29日（5月6日）自澳底（現為鹽寮公園）登陸臺灣，並在澳底設立行宮（即仁和宮現址）。大正九年(1920年)實施地方官制改正，將「槓仔寮區」簡化改設**貢寮-{庄}-**。1945年終戰後改稱為貢寮鄉，2010年12月25日，臺北縣升格為[新北市後](../Page/新北市.md "wikilink")，貢寮鄉改稱**貢寮區**。

## 人口

## 文化

貢寮區在考古學上，存在許多台灣史前住民[巴賽族](../Page/巴賽族.md "wikilink")（Basai）的遺跡。

### 舊社遺址

1962年盛清沂地表調查發現舊社遺址，位處貢寮區龍門里雙溪出海口北岸之沙丘，當時採集了284件標本，包括陶質標本179件、石器9件。2005
年[國立歷史博物館曾再度進行發掘](../Page/國立歷史博物館.md "wikilink")，文化類型歸為十三行文化晚期舊社類型。台大[林朝棨教授於](../Page/林朝棨.md "wikilink")《凱達格蘭族之礦業》〉論述十三行遺址的鐵質石塊，乃煉鐵時所留下的半成品或鐵渣，推論該地居民會煉鐵，并推論福隆、舊社、仁里等遺址也有煉鐵處。另在發掘到的依附銅渣的陶片，推論有煉銅的能力。舊社遺址於過多次發掘歷年出土的遺物中，發現此地確實為居住區，文化層屬最早的舊社類型，推論居民從臺灣史前400年左右才開始居住，並有煉鐵廠的出現。此遺址古住民之習性及生活環境和馬賽人St.
Jago社相同，舊社遺址可能是馬賽人三貂社的舊社之一。\[2\]

### 仁里遺址

馬賽人仁里遺址，位於新北市貢寮區仁里里13鄰，是臺灣北部考古遺址之一。台灣北部之平埔族群，凱達格蘭族最為人知，實際上北台灣平埔族實為多群性，如馬賽人、雷朗人、龜崙人等，擅長從商的馬賽人是一個特殊的族群，在北台灣的族群交流中佔重要地位。在考古學上，馬賽人及噶瑪蘭人的居處，被分類為十三行文化舊社類型。1962年[盛清沂地表調查發現仁里遺址](../Page/盛清沂.md "wikilink")，共採集到標本共161件，其中包括陶質標本137件、近代瓷片3件、鐵渣17件、貝類3件、安南銅幣「光中通寶」半個。\[3\]1981年，陳玉美進行發掘，出土屈肢葬墓葬。[劉益昌教授將仁里遺址](../Page/劉益昌.md "wikilink")，文化類型歸為十三行文化中期福隆類型、晚期舊社類型。仁里遺址可能是三貂社的舊社之一，且有可能是凱達
格蘭族的登陸地及落腳居住之處。符合台灣古早住民的生業性格，海路方便漁貨多，陸路有腹地活動，許多條件都符合馬賽人的性格。\[4\]

### 十三姓遺址

1986年台大人類學系李光周教授地表調查發現十三姓遺址，位於貢寮區龍門里雙溪北岸階地上，為古沙丘地形。十三姓遺址的位置稍微靠內陸，目前可參考的標本量較少，但日常使用的物品齊全。\[5\]

### 吳沙墓

## 政治及行政區域

貢寮區在行政區域上共有11個里之區域劃分。\[6\]

<table style="width:580%;">
<colgroup>
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 80%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>里名</p></th>
<th><p>面積<br />
km²</p></th>
<th><p>鄰數</p></th>
<th><p>戶數</p></th>
<th><p>人口數</p></th>
<th><p>人口密度<br />
km²</p></th>
<th><p>里長<br />
(2013)</p></th>
<th><p>里辦公處</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/仁里里_(貢寮區).md" title="wikilink">仁里里</a></p></td>
<td><p>2.944</p></td>
<td><p>16</p></td>
<td><p>688</p></td>
<td><p>2,096</p></td>
<td><p>712</p></td>
<td><p>吳勝福</p></td>
<td><p>仁和路61號</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/真理里.md" title="wikilink">真理里</a></p></td>
<td><p>2.560</p></td>
<td><p>22</p></td>
<td><p>744</p></td>
<td><p>2,536</p></td>
<td><p>991</p></td>
<td><p>趙瑞昌</p></td>
<td><p>新港街26巷6之3號4樓</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/和美里_(貢寮區).md" title="wikilink">和美里</a></p></td>
<td><p>13.5680</p></td>
<td><p>21</p></td>
<td><p>352</p></td>
<td><p>1,184</p></td>
<td><p>87</p></td>
<td><p>吳金發</p></td>
<td><p>和美街21之9號1樓</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福隆里.md" title="wikilink">福隆里</a></p></td>
<td><p>12.5440</p></td>
<td><p>22</p></td>
<td><p>664</p></td>
<td><p>1,984</p></td>
<td><p>158</p></td>
<td><p>吳憲彰</p></td>
<td><p>興隆街10號</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/貢寮里.md" title="wikilink">貢寮里</a></p></td>
<td><p>3.840</p></td>
<td><p>16</p></td>
<td><p>400</p></td>
<td><p>1,056</p></td>
<td><p>275</p></td>
<td><p>楊張翠華</p></td>
<td><p>貢寮街134號</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福連里.md" title="wikilink">福連里</a></p></td>
<td><p>10.4960</p></td>
<td><p>16</p></td>
<td><p>328</p></td>
<td><p>1,032</p></td>
<td><p>98</p></td>
<td><p>吳文益</p></td>
<td><p>福興街37號之1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美豐里.md" title="wikilink">美豐里</a></p></td>
<td><p>12.2880</p></td>
<td><p>21</p></td>
<td><p>320</p></td>
<td><p>904</p></td>
<td><p>74</p></td>
<td><p>周貝芳</p></td>
<td><p>土地公嶺街55號</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雙玉里_(貢寮區).md" title="wikilink">雙玉里</a></p></td>
<td><p>13.44</p></td>
<td><p>23</p></td>
<td><p>328</p></td>
<td><p>880</p></td>
<td><p>65</p></td>
<td><p>吳永彬</p></td>
<td><p>田寮洋街6號</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吉林里_(貢寮區).md" title="wikilink">吉林里</a></p></td>
<td><p>16.2560</p></td>
<td><p>19</p></td>
<td><p>232</p></td>
<td><p>520</p></td>
<td><p>32</p></td>
<td><p>簡束卿</p></td>
<td><p>內寮街20之3號</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龍門里_(貢寮區).md" title="wikilink">龍門里</a></p></td>
<td><p>3.072</p></td>
<td><p>14</p></td>
<td><p>264</p></td>
<td><p>816</p></td>
<td><p>266</p></td>
<td><p>吳世揚</p></td>
<td><p>龍門街11號</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/龍崗里_(貢寮區).md" title="wikilink">龍崗里</a></p></td>
<td><p>8.960</p></td>
<td><p>12</p></td>
<td><p>152</p></td>
<td><p>416</p></td>
<td><p>46</p></td>
<td><p>黃朝銘</p></td>
<td><p>洋宮路35號</p></td>
</tr>
<tr class="even">
<td><p><strong>合計</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>貢寮區</p></td>
<td><p>99.9680</p></td>
<td><p>202</p></td>
<td><p>4,472</p></td>
<td><p>13,424</p></td>
<td><p>134</p></td>
<td><p>陳文俊(區長)</p></td>
<td><p>貢寮里朝陽街54號(區辦公廳)</p></td>
</tr>
</tbody>
</table>

  - 本區[人口多分布在](../Page/人口.md "wikilink")[仁里里](../Page/仁里里.md "wikilink")、[真理里](../Page/真理里.md "wikilink")、[和美里](../Page/和美里.md "wikilink")、[福隆里](../Page/福隆里.md "wikilink")、[貢寮里](../Page/貢寮里.md "wikilink")、[福連里](../Page/福連里.md "wikilink")，[人口約占全區](../Page/人口.md "wikilink")73.68%。
  - 本區[人口約占全](../Page/人口.md "wikilink")[新北市](../Page/新北市.md "wikilink")0.344%。

### 人口結構

人口比例：(2012年6月)

  - 幼年人口：約占全區10.18%
  - 壯年人口：約占全區71.18%
  - 老年人口：約占全區18.64%

### 歷任地方首長

  - [日治時期](../Page/日治時期.md "wikilink")[-{庄}-長](../Page/庄長.md "wikilink")\[7\]

<!-- end list -->

  - 1920年(大正9年) 呂來傳
  - 1923年(大正12年) 鎌田清熊
  - 1930年(昭和5年) 白澤傳吉
  - 1939年(昭和14年) 北條角馬
  - 1940年(昭和15年) 中村知三
  - 1941年(昭和16年) 川元實

<!-- end list -->

  - [貢寮鄉](../Page/貢寮鄉.md "wikilink")[鄉長](../Page/鄉長.md "wikilink")

<!-- end list -->

  - 第一屆 [趙國棟](../Page/趙國棟.md "wikilink")
  - 第二屆 [陳世男](../Page/陳世男.md "wikilink")
  - 第二屆代理 [張本賢](../Page/張本賢.md "wikilink")

<!-- end list -->

  - [貢寮區](../Page/貢寮區.md "wikilink")[區長](../Page/區長.md "wikilink")

<!-- end list -->

  - 第一屆 [徐溪祥](../Page/徐溪祥.md "wikilink")
  - 第二屆 [朱思戎](../Page/朱思戎.md "wikilink")
  - 第三屆 [陳文俊](../Page/陳文俊.md "wikilink")
  - 第四屆 [施玉祥](../Page/施玉祥.md "wikilink")

## 交通

### 鐵路

[2015_New_Gongliao_Station.JPG](https://zh.wikipedia.org/wiki/File:2015_New_Gongliao_Station.JPG "fig:2015_New_Gongliao_Station.JPG")\]\]
[TRA_FuLong_Station.jpg](https://zh.wikipedia.org/wiki/File:TRA_FuLong_Station.jpg "fig:TRA_FuLong_Station.jpg")\]\]

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [宜蘭線](../Page/宜蘭線.md "wikilink")：[貢寮車站](../Page/貢寮車站.md "wikilink")
    - [福隆車站](../Page/福隆車站.md "wikilink")

### 公路

  - [TW_PHW2.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2.svg "fig:TW_PHW2.svg")[台2線](../Page/台2線.md "wikilink")：[北部濱海公路](../Page/北部濱海公路.md "wikilink")。
      - [TW_PHW2c.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2c.svg "fig:TW_PHW2c.svg")[台2丙](../Page/台2線.md "wikilink")：基福公路。
  - [TW_CHW102.svg](https://zh.wikipedia.org/wiki/File:TW_CHW102.svg "fig:TW_CHW102.svg")[市道102號](../Page/市道102號.md "wikilink")。
      - [TW_CHW102a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW102a.svg "fig:TW_CHW102a.svg")[市道102甲](../Page/市道102號.md "wikilink")
        ：[雙澳公路](../Page/雙澳公路.md "wikilink")。
  - [TW_THWtp38.png](https://zh.wikipedia.org/wiki/File:TW_THWtp38.png "fig:TW_THWtp38.png")北38線：魚行道路。
  - [TW_THWtp40.png](https://zh.wikipedia.org/wiki/File:TW_THWtp40.png "fig:TW_THWtp40.png")北40線：貢澳公路。

## 公共設施

[台湾第四原子力発電所.jpg](https://zh.wikipedia.org/wiki/File:台湾第四原子力発電所.jpg "fig:台湾第四原子力発電所.jpg")與[鹽寮抗日紀念碑](../Page/鹽寮抗日紀念碑.md "wikilink")\]\]

  - 警政

<!-- end list -->

  - [新北市政府警察局瑞芳分局](../Page/新北市政府警察局.md "wikilink")
      - 瑞芳交通分隊
      - 貢寮分駐所
      - 和美派出所
      - 澳底派出所
      - 吉林派出所
      - 福隆派出所
      - 卯澳派出所

<!-- end list -->

  - 消防

<!-- end list -->

  - [新北市政府消防局第六大隊](../Page/新北市政府消防局.md "wikilink")
      - 貢寮分隊

<!-- end list -->

  - 其他

<!-- end list -->

  - [台電](../Page/台灣電力公司.md "wikilink")[龍門核能發電廠](../Page/龍門核能發電廠.md "wikilink")（核四）

<!-- end list -->

  - 漁港

<!-- end list -->

  - [龍洞漁港](../Page/龍洞漁港.md "wikilink")
  - [和美漁港](../Page/和美漁港.md "wikilink")
  - [美豔山漁港](../Page/美豔山漁港.md "wikilink")
  - [澳底漁港](../Page/澳底漁港.md "wikilink")
  - [澳仔漁港](../Page/澳仔漁港.md "wikilink")
  - [龍門漁港](../Page/龍門漁港.md "wikilink")
  - [福隆漁港](../Page/福隆漁港.md "wikilink")
  - [卯澳漁港](../Page/卯澳漁港.md "wikilink")
  - [馬崗漁港](../Page/馬崗漁港.md "wikilink")

<!-- end list -->

  - 建案

<!-- end list -->

  - 貢寮澳底捷年GENE
    21+：由知名商人[呂台年捷年集團](../Page/呂台年.md "wikilink")、[東森新聞台](../Page/東森新聞台.md "wikilink")[主播](../Page/主播.md "wikilink")[王佳婉共同開發建設](../Page/王佳婉.md "wikilink")
  - 龍洞四季灣：由東北角暨宜蘭海岸國家風景區管理處委外經營，經營單位置正是捷年集團

## 教育

  - 國民中學

<!-- end list -->

  - [新北市立貢寮國民中學](../Page/新北市立貢寮國民中學.md "wikilink")
  - [新北市立豐珠國民中學](http://www.fjjh.ntpc.edu.tw/)

<!-- end list -->

  -
    豐珠國民中學2004年才設校，主要作為「中途學校」要安置台中以北縣市、遭法院裁定就學的孩子，於此調整身心後再返原校就讀。前身是貢寮澳底國小分班，校地面積0.53公頃。\[8\]

<!-- end list -->

  - 國民小學

<!-- end list -->

  - [新北市貢寮區貢寮國民小學](http://www.kles.ntpc.edu.tw/)
  - [新北市貢寮區和美國民小學](http://www.hmps.ntpc.edu.tw/)
  - [新北市貢寮區福隆國民小學](http://www.flps.ntpc.edu.tw/)
  - [新北市貢寮區福連國民小學](http://www.fulps.ntpc.edu.tw)
  - [新北市貢寮區澳底國民小學](http://www.aodi.ntpc.edu.tw/)

<!-- end list -->

  - 圖書館

<!-- end list -->

  - [新北市立圖書館貢寮分館](../Page/新北市立圖書館.md "wikilink")：位於新北市貢寮區仁愛路79號3樓

## 旅遊

<center>

<File:三貂角燈塔FUJI7869.JPG>|[三貂角燈塔](../Page/三貂角燈塔.md "wikilink")
[File:三貂角燈塔裝置藝術FUJI7922b.jpg|三貂角燈塔裝置藝術](File:三貂角燈塔裝置藝術FUJI7922b.jpg%7C三貂角燈塔裝置藝術)
<File:Shandiaojiao> Lighthouse 1.jpg|三貂角燈塔 <File:貢寮卯澳漁村DSC>
6371b.jpg|貢寮卯澳漁村 <File:Hohaiyan> 2006 Smaller Band Shell Fulong
Beach.jpg|2006年[貢寮-{}-國際海洋音樂祭](../Page/貢寮國際海洋音樂祭.md "wikilink")
<File:Hu>
stele.JPG|[草嶺古道](../Page/草嶺古道.md "wikilink")[虎字碑](../Page/虎字碑.md "wikilink")
[File:四角窟觀景台.jpg|四角窟觀景台](File:四角窟觀景台.jpg%7C四角窟觀景台)

</center>

## 區域立法委員

  - 新北市
    [第十二選區](../Page/新北市第十二選舉區_\(2008年立法委員\).md "wikilink")：[黃國昌](../Page/黃國昌.md "wikilink")

## 出身人物 (名人)

  - [蔡昌憲](../Page/蔡昌憲.md "wikilink")：主持人、歌手、演員。
  - [郭雪芙](../Page/郭雪芙.md "wikilink")：藝人。
  - [江宜樺](../Page/江宜樺.md "wikilink")：前行政院長。
  - [鄭金隆](../Page/鄭金隆.md "wikilink")：前三重市民代表，現任[新北市](../Page/新北市.md "wikilink")（[三重](../Page/三重區.md "wikilink")、[蘆洲](../Page/蘆洲區.md "wikilink")）市議員、[民進黨新北市黨部執行委員](../Page/民主進步黨.md "wikilink")。

## 參考文獻

## 相關條目

  - [台灣漁港列表](../Page/台灣漁港列表.md "wikilink")
  - [巴賽族](../Page/巴賽族.md "wikilink")
  - [三貂角燈塔](../Page/三貂角燈塔.md "wikilink")
  - [草嶺古道](../Page/草嶺古道.md "wikilink")
  - [雄鎮蠻煙碑](../Page/雄鎮蠻煙碑.md "wikilink")
  - [虎字碑](../Page/虎字碑.md "wikilink")

## 外部連結

  - [新北市貢寮區公所](http://www.gongliao.ntpc.gov.tw/)

  - [新北市瑞芳戶政事務所-貢寮區所](https://www.ruifang.ris.ca.ntpc.gov.tw)

[Category:新北市行政區劃](../Category/新北市行政區劃.md "wikilink")
[Category:台灣之最](../Category/台灣之最.md "wikilink")
[貢寮區](../Category/貢寮區.md "wikilink")

1.  [交通部觀光局東北角暨宜蘭海岸國家風景區管理處《探索東北角》](http://www.necoast-nsa.gov.tw/user/Article.aspx?Lang=1&SNo=03000427)

2.

3.  盛清沂《台灣省北海岸史前遺址調查報告》台灣文獻 1962年

4.
5.
6.  [鄰里介紹](https://www.gongliao.ntpc.gov.tw/content/?parent_id=10023&type_id=10008).貢寮區公所

7.  [中研院台史所《臺灣總督府職員錄》](http://who.ith.sinica.edu.tw/mpView.action)

8.  中國時報《中輟孩子的家 豐珠入圍教學卓越獎》2007.09.09