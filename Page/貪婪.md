**貪婪**指一种攫取远超过自身需求的[金钱](../Page/金钱.md "wikilink")、物质财富或肉体满足等的強烈[欲望](../Page/欲望.md "wikilink")。貪婪也可以指得寸進尺，永不滿足，貪圖的東西可能是屬於别人的。貪心的個體為貪而貪，從不考慮自己是不是真的需要這些東西，也不理會其他個體的[动机](../Page/动机.md "wikilink")，常忽视其他人的[福利](../Page/福利.md "wikilink")，因此被社會視為"有害的"（[经济学的解释](../Page/经济学.md "wikilink")，假定[市场经济是](../Page/市场经济.md "wikilink")[零和](../Page/零和博弈.md "wikilink")[博弈](../Page/博弈.md "wikilink")）。貪心的人讓慾望主宰自己的思想和行動，結果貪慾就成為了近似「神」的替代精神。然而，贪婪渐渐为西方文化所接受，我們每天都被廣告電視電影网絡等電子媒体無所不包地貫輸欲望，製造成假需求的消費毒瘾，性慾的追求也是應當強推，於是此類毒品都擁有無上合法地位。

## 宗教

### 天主教

貪婪被列为[天主教的](../Page/天主教.md "wikilink")[七宗罪之一](../Page/七宗罪.md "wikilink")。如果贪婪被引申用来指对饮食的过分消费，即[暴食](../Page/貪食.md "wikilink")，同样被列为[天主教的](../Page/天主教.md "wikilink")[七宗罪之一](../Page/七宗罪.md "wikilink")。

### 佛教

[佛教徒认为](../Page/佛教.md "wikilink")，贪婪是基于将物质财富与快乐\[1\]错误地联系在一起所致。这种错误是由于被某一事物积极方面夸大的印象所迷惑引起的。

## 參見

  - [三毒](../Page/三毒.md "wikilink")
  - [貪食](../Page/貪食.md "wikilink")
  - [貪汙](../Page/貪汙.md "wikilink")
  - [詐騙](../Page/詐騙.md "wikilink")
  - [詐騙集團](../Page/詐騙集團.md "wikilink")
  - [台灣詐騙案](../Page/台灣詐騙案.md "wikilink")
  - [大陸妹 (貶稱)](../Page/大陸妹_\(貶稱\).md "wikilink")（大陸女子假結婚现象）
  - [貪 (佛教)](../Page/貪_\(佛教\).md "wikilink")
  - [貪欲 (佛教)](../Page/貪欲_\(佛教\).md "wikilink")

## 参考文献

[Category:心理学](../Category/心理学.md "wikilink")
[Category:道德惡](../Category/道德惡.md "wikilink")
[Category:七宗罪](../Category/七宗罪.md "wikilink")

1.  \[广超师教快乐知道 Ven Guang Chao Teaches The Way of Happiness \]