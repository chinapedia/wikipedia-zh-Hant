**貫葉連翹**（[學名](../Page/學名.md "wikilink")：**）， 又名**贯叶金絲桃**、**聖約翰草**（St
John's
wort），[金丝桃科](../Page/金丝桃科.md "wikilink")[金絲桃屬植物](../Page/金絲桃屬.md "wikilink")，是歐美的常用草藥，主要用於婦女調經，亦有寧神、平衡情緒的作用，臨床上發現對[抑鬱症患者有療效](../Page/抑鬱症.md "wikilink")\[1\]。

## 形态

[Saint_John's_wort_flowers.jpg](https://zh.wikipedia.org/wiki/File:Saint_John's_wort_flowers.jpg "fig:Saint_John's_wort_flowers.jpg")

多年生草木，高可达1m左右。茎直立，多分枝，枝皆腋生，茎两侧各有纵棱1条。单叶对生，长椭圆形或披针形，长1\~3cm，基部无柄，有时抱茎，散布透明腺点，叶缘有黑色腺点。聚伞花序顶生；花较大，黄色，萼片5；花瓣5；雄蕊多数，组成3束，花瓣边缘和花药均有黑色腺点，子房上位，1室，花柱3裂。蒴果长圆形，具泡状小突起，开裂。种子圆筒形。花期6\~7月，果期9\~10月。

## 药用

为植物贯叶金丝桃的干燥地上部分，主产于江西、四川、陕西。\[2\]

### 功效

性寒，味辛。能疏肝解郁，清热利湿，消肿通乳。用于肝气郁结，情志不畅，心胸郁闷，关节肿痛，乳痈，乳少。用量2\~3g。

### 采集炮制

夏、秋二季开花时采割，阴干或低温烘干。

### 化学成分

含有二蒽酮类，其主要成分为金丝桃素、伪金丝桃素、原金丝桃素等。还含有贯叶金丝桃素、加贯叶金丝桃素等间苯三酚衍生物，这是贯叶连翘的特征性成分，也是其抗抑郁作用的主要有效成分。尚含金丝桃苷、山奈酚等[黄酮类等](../Page/黄酮.md "wikilink")。

### 药理作用

抗抑郁、抗焦虑、抗感染作用。

貫葉連翹藥力強大，可能與其他西藥物互相牴觸，例如降低[避孕藥的療效](../Page/避孕藥.md "wikilink")，故服用前應先諮詢醫生意見。但其副作用亦較西藥為低。常見副作用包括腸胃不適、頭暈、混亂、疲倦、鎮定、口乾、煩躁及頭痛。其也可能導致[光照性皮炎](../Page/光照性皮炎.md "wikilink")。對躁鬱症患者可能會誘發躁症而精神分裂症患者可能會惡化其幻覺。

## 相關條目

  - [连翘](../Page/连翘.md "wikilink")

## 參考資料

## 外部連結

  - [貫葉連翹
    Guanyelianqiao](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01388)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  -
  -
  - [Species Profile — St. Johnswort (*Hypericum
    perforatum*)](http://www.invasivespeciesinfo.gov/plants/stjohnswort.shtml),
    National Invasive Species Information Center, [United States
    National Agricultural
    Library](../Page/United_States_National_Agricultural_Library.md "wikilink").
    Lists general information and resources for St John's wort.

[Category:金丝桃属](../Category/金丝桃属.md "wikilink")
[Category:歐洲植物](../Category/歐洲植物.md "wikilink")
[Category:爱沙尼亚植物](../Category/爱沙尼亚植物.md "wikilink")
[Category:英国植物](../Category/英国植物.md "wikilink")
[Category:欧洲园艺植物](../Category/欧洲园艺植物.md "wikilink")
[Category:美國原住民傳統藥用植物](../Category/美國原住民傳統藥用植物.md "wikilink")
[Category:抗抑郁药物](../Category/抗抑郁药物.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")
[Category:单胺氧化酶抑制剂](../Category/单胺氧化酶抑制剂.md "wikilink")

1.
2.