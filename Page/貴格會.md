**贵格会**（），又称**公谊会**或者**教友派**（），是[基督教](../Page/基督教.md "wikilink")[新教的一个派别](../Page/新教.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:2017-03-04_基督教台灣貴格會東園教會.jpg "fig:缩略图")

## 歷史

贵格会成立于17世纪的[英國](../Page/英國.md "wikilink")，创始人为[乔治·福克斯](../Page/乔治·福克斯.md "wikilink")，因一名早期领袖的号诫“听到上帝的话而发抖”而得名「貴格」（），中文意译为“震颤者”。但也有说法称在初期[宗教聚會中常有](../Page/宗教.md "wikilink")[教徒全身顫抖](../Page/教徒.md "wikilink")，因而得名。该教会坚决反对[奴隶制](../Page/奴隶制.md "wikilink")，在美国[南北战争前后的](../Page/南北战争.md "wikilink")[废奴运动中起过重要作用](../Page/废奴运动.md "wikilink")。贵格会在历史上提出过一些很进步的思想，其中一部分现在得到广泛接受。

贵格会的信徒曾经受到[英國国教迫害](../Page/英國国教.md "wikilink")，与[清教徒一起移民到](../Page/清教徒.md "wikilink")[美洲](../Page/美洲.md "wikilink")，但随后又受到清教徒的迫害，大批的贵格会教徒逃离[马萨诸塞州而定居在](../Page/马萨诸塞州.md "wikilink")[罗得岛州和](../Page/罗得岛州.md "wikilink")[宾夕法尼亚州等地](../Page/宾夕法尼亚州.md "wikilink")。由于宾西法尼亚州有大量贵格会教徒聚居，习惯上以「Quaker
City」作为[费城](../Page/费城.md "wikilink")（Philadelphia）的别名，因而费城人也被称为「Quaker」。

貴格會之後傳播到[美國](../Page/美國.md "wikilink")、[肯尼亞和](../Page/肯尼亞.md "wikilink")[玻利維亞](../Page/玻利維亞.md "wikilink")。贵格会也曾经传入[中国](../Page/中国.md "wikilink")。[美国差会](../Page/美国.md "wikilink")（[俄亥俄年议会](../Page/俄亥俄.md "wikilink")）在1887年派遣第一位传教士Esther
H.
Butler来华，1890年到[江寧府](../Page/江寧府.md "wikilink")，主要在[六合縣](../Page/六合縣.md "wikilink")（1898）工作（今日六合区基督教堂系源于貴格會传统），1953年迁往[台湾继续工作](../Page/台湾.md "wikilink")；[英国差会](../Page/英国.md "wikilink")（称为[公谊会](../Page/公谊会.md "wikilink")）曾经在[四川的](../Page/四川.md "wikilink")[重庆](../Page/重庆.md "wikilink")、[成都](../Page/成都.md "wikilink")、[三台等地工作](../Page/三台.md "wikilink")。

貴格會的人數现在大約有六十萬。

## 主張

该派反对任何形式的[战争和](../Page/战争.md "wikilink")[暴力](../Page/暴力.md "wikilink")，不尊称任何人也不要求别人尊称自己（即不使用“先生”、“女士”、“夫人”头衔，对任何人皆以名字相称呼），不起誓。主张任何人之间要像兄弟一样，主张[和平主义和](../Page/和平主义.md "wikilink")[宗教自由](../Page/宗教自由.md "wikilink")。

## 其他

由于許多贵格会信仰者居住于费城，因此费城人又称“贵格会信仰者”。

## 参考文献

[Category:创立于英国的基督教新教教派](../Category/创立于英国的基督教新教教派.md "wikilink")