**約翰·金·費爾班克**（，），[漢名](../Page/漢名.md "wikilink")**費正清**，[美國](../Page/美國.md "wikilink")[漢學家](../Page/漢學家.md "wikilink")、[歷史學家](../Page/歷史學家.md "wikilink")，[哈佛大學教授](../Page/哈佛大學.md "wikilink")，[哈佛大學](../Page/哈佛大學.md "wikilink")[東亞研究中心的創始人](../Page/費正清中國研究中心.md "wikilink")。

## 姓名

其漢名費正清，為[梁思成所起](../Page/梁思成.md "wikilink")。

## 生平

費正清1907年生於美國[南達科他州的](../Page/南達科他州.md "wikilink")[休倫湖](../Page/休倫湖_\(南達科他州\).md "wikilink")，是其父Arthur
Boyce
Fairbank唯一的孩子。先後求學於[威斯康辛大學麥迪遜分校及](../Page/威斯康辛大學麥迪遜分校.md "wikilink")[哈佛大學](../Page/哈佛大學.md "wikilink")。1929年獲[羅德獎學金](../Page/羅德獎學金.md "wikilink")，赴[英國](../Page/英國.md "wikilink")[牛津大學求學](../Page/牛津大學.md "wikilink")，研究題目為19世紀中英關係。

為了撰寫博士論文，費正清於1931年夏赴[中國調查進修](../Page/中華民國國民政府.md "wikilink")，考察海關貿易，在[华北协和华语学校學習中文](../Page/加利福尼亚学院_\(北平\).md "wikilink")，主要受教於[蔣廷黻](../Page/蔣廷黻.md "wikilink")，曾短期兼職[清華大學經濟史講師](../Page/清華大學.md "wikilink")。其間在[北平結識了](../Page/北平.md "wikilink")[北京大學校長](../Page/北京大學.md "wikilink")[胡適](../Page/胡適.md "wikilink")、[北平社會研究所所長](../Page/北平社會研究所.md "wikilink")[陶孟和](../Page/陶孟和.md "wikilink")、[中國地質調查所研究員](../Page/中国地质调查局.md "wikilink")[丁文江等人](../Page/丁文江.md "wikilink")，並與外文教授[葉公超](../Page/葉公超.md "wikilink")\[1\]、建築學家[梁思成與](../Page/梁思成.md "wikilink")[林徽因夫婦](../Page/林徽因.md "wikilink")、哲學家[金岳霖](../Page/金岳霖.md "wikilink")、政治學家[錢端升和物理學家](../Page/錢端升.md "wikilink")[周培源等結為好友](../Page/周培源.md "wikilink")。

1932年7月，在北平與未婚妻[費慰梅](../Page/費慰梅.md "wikilink")（Wilma Denio
Cannon，研究中國藝術和建築的美國學者）結婚。夫婦二人的中文名字皆是梁思成所取。二人收養了兩個女兒Holly和Laura。

1935年第一次離開中國。在取得牛津大學哲學博士學位後，於1936年回母校哈佛大學歷史系任教，並從1939年起與美國的[日本問題專家](../Page/日本.md "wikilink")[賴肖爾一起開設](../Page/賴肖爾.md "wikilink")[東亞文明課程](../Page/東亞.md "wikilink")。1941年被徵召至[美國情報協調局](../Page/美國情報協調局.md "wikilink")（1942年6月13日分裂为[美國戰略情報局与](../Page/美國戰略情報局.md "wikilink")[美国战时新闻局](../Page/美国战时新闻局.md "wikilink")）研究分析处，前往[華盛頓工作](../Page/華盛頓哥倫比亞特區.md "wikilink")。

1942年6月2日[美國情報協調局局长多诺万上校任命费正清为驻华首席代表](../Page/美國情報協調局.md "wikilink")。1942年8月21日从[迈阿密启程](../Page/迈阿密.md "wikilink")，搭乘[泛美航空公司飞机](../Page/泛美航空公司.md "wikilink")，经停[波多黎各](../Page/波多黎各.md "wikilink")、[西班牙港](../Page/西班牙港.md "wikilink")、[贝伦](../Page/贝伦_\(巴西\).md "wikilink")、[累西腓](../Page/累西腓.md "wikilink")、[阿森松岛](../Page/阿森松岛.md "wikilink")、[阿克拉](../Page/阿克拉.md "wikilink")、[拉各斯](../Page/拉各斯.md "wikilink")、[卡诺](../Page/卡诺.md "wikilink")、[迈杜古里](../Page/迈杜古里.md "wikilink")、[喀土穆](../Page/喀土穆.md "wikilink")、[开罗](../Page/开罗.md "wikilink")、[巴士拉](../Page/巴士拉.md "wikilink")、[卡拉奇](../Page/卡拉奇.md "wikilink")、[新德里](../Page/新德里.md "wikilink")、[阿拉哈巴德](../Page/阿拉哈巴德.md "wikilink")、[阿萨姆邦](../Page/阿萨姆邦.md "wikilink")、昆明，1942年9月25日飞抵重庆。1942年9月至1943年12月驻重庆，擔任美國[戰略情報局官員](../Page/戰略情報局.md "wikilink")，並兼[美國國務院文化關係司對華關係處文官和美國駐華大使特別助理](../Page/美國國務院.md "wikilink")。1945年10月至1946年7月再度來華，任美國新聞署駐華分署主任。

1946年返回哈佛大學任教。1955年在[福特基金會支持下主持成立東亞問題研究中心](../Page/福特基金會.md "wikilink")。該研究中心後來還獲得[洛克菲勒和](../Page/洛克菲勒.md "wikilink")[卡內基基金會的資助](../Page/卡內基.md "wikilink")，並於1961年更名為東亞研究中心，後於1977年更名為「[費正清東亞研究中心](../Page/費正清中國研究中心.md "wikilink")」以資紀念。在他直接或間接主持研究中心的幾十年裡，哈佛大學東亞研究中心成為美國東亞問題研究的學術重鎮，其本人也成為美國中國研究學界的領袖人物。

1949年，中华人民共和国建国后，费正清对中国共产党执政将来会遇到的问题作出预测，指出它们将来会遇到三个问题“一是人口问题，二是官僚腐败的问题，三是强调思想统一，使整个民族丧失创造力的问题”\[2\]。

他在40年代末是預測毛澤東與共產黨會獲勝的[中國通之一](../Page/中國通.md "wikilink")，主張與中共建立關係符合美國利益。但是許多美國人指責這些中國通出賣盟友，協助散播共產主義與蘇聯的影響。由于被认为过于“同情”共产主义，他在1952年申请去日签证被驳。在美國國會「[誰輸掉中國](../Page/誰輸掉中國.md "wikilink")」的調查中，他被要求在上作证，但由于他的学术地位而不受影响。而當美國右派的和[麥卡錫](../Page/約瑟夫·雷蒙德·麥卡錫.md "wikilink")[參議員則把矛頭指向三位](../Page/美國參議員.md "wikilink")[國務院著名](../Page/美國國務院.md "wikilink")「中國通」時，费正清也被列入“四個該為輸掉中國負責的”之一\[3\]\[4\]\[5\]，他們分別是[范宣德](../Page/范宣德.md "wikilink")（John
Carter Vincent, 1900-1972），[謝偉思](../Page/謝偉思.md "wikilink")（John
Service, 1909-1999）和[戴維斯](../Page/戴維斯.md "wikilink")（John P. Davies,
Jr., 1908-1999）<ref>

[Category:美国历史学家](../Category/美国历史学家.md "wikilink")
[Category:美國漢學家](../Category/美國漢學家.md "wikilink")
[Category:美國羅德學者](../Category/美國羅德學者.md "wikilink")
[Category:中國通](../Category/中國通.md "wikilink")
[Category:美国历史学会会长](../Category/美国历史学会会长.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:牛津大學貝利奧爾學院校友](../Category/牛津大學貝利奧爾學院校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:威斯康辛大學麥迪遜分校校友](../Category/威斯康辛大學麥迪遜分校校友.md "wikilink")
[Category:南達科他州人](../Category/南達科他州人.md "wikilink")
[Category:亚洲研究协会会长](../Category/亚洲研究协会会长.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")

1.
2.
3.
4.
5.