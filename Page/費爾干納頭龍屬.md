**費爾干納頭龍屬**（[學名](../Page/學名.md "wikilink")：*Ferganocephale*）可能是早期的[厚頭龍下目](../Page/厚頭龍下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。牠是生存於[侏羅紀中期的](../Page/侏羅紀.md "wikilink")[吉爾吉斯](../Page/吉爾吉斯.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")**齒飾費爾干納頭龍**（*F.
adenticulatum*），是在2005年被描述、命名，屬名是以[費爾干納盆地為名](../Page/費爾干納盆地.md "wikilink")。[化石只有一些](../Page/化石.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")，地質年代屬於[卡洛維階](../Page/卡洛維階.md "wikilink")\[1\]

費爾干納頭龍最初被分類於[厚頭龍科](../Page/厚頭龍科.md "wikilink")，是當時已知最古老的厚頭龍科恐龍之一。在2006年，[羅伯特·蘇利文](../Page/羅伯特·蘇利文.md "wikilink")（Robert
Sullivan）提出費爾干納頭龍的牙齒沒有鋸齒，只有很少厚頭龍類的牙齒特徵，所以很難分辨這些化石，並反對費爾干納頭龍屬於厚頭龍類\[2\]。蘇利文將費爾干納頭龍歸類為[疑名](../Page/疑名.md "wikilink")。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [Pachycephalosauria](https://web.archive.org/web/20080703060328/http://www.users.qwest.net/~jstweet1/pachycephalosauria.htm)
  - [Ferganocephale](https://web.archive.org/web/20160304133426/http://dinosaurier-info.de/animals/dinosaurs/pages_f/ferganocephale.php)

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:厚頭龍下目](../Category/厚頭龍下目.md "wikilink")

1.  A. O. Averianov, T. Martin, and A. A. Bakirov, 2005, "Pterosaur and
    dinosaur remains from the Middle Jurassic Balabansai Svita in the
    northern Fergana depression, Kyrgyzstan (central Asia)",
    *Palaeontology* **48**(1): 135-155
2.