**費爾法克斯市**（**Fairfax**  )；官方稱呼為**City of
Fairfax**）是[美國](../Page/美國.md "wikilink")[維吉尼亞州北部的一個獨立城市](../Page/維吉尼亞州.md "wikilink")。費爾法克斯市原为由原先[费尔法克斯县所辖的镇](../Page/費爾法克斯縣_\(維吉尼亞州\).md "wikilink")，1961年6月30日升级为独立市\[1\]。尽管费尔法克斯市和费尔法克斯县为平级行政区且互不隶属，但費爾法克斯縣的部分政府机关位于市境。[喬治梅森大學位於此地](../Page/喬治梅森大學.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  -

[F](../Category/弗吉尼亚州城市.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.