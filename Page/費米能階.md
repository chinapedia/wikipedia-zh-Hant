**費米能階**（\[1\]），通常標示為「*µ*
」或「*E*<sub>F</sub>」\[2\]。\[3\]\[4\]\[5\]\[6\]\[7\]也稱為「」\[8\]\[9\]。

## 定義

[Fermi.gif](https://zh.wikipedia.org/wiki/File:Fermi.gif "fig:Fermi.gif")

  - 絕對零度下，電子能夠填到最高的那一個能階便是費米能階。\[10\]\[11\]\[12\]\[13\]\[14\]
  - 用[-{A](../Page/费米–狄拉克统计.md "wikilink")，就是無論任何溫度，電子佔據該能階的[機率皆為二分之一](../Page/機率.md "wikilink")，此能階稱為費米能階。\[15\]\[16\]\[17\]\[18\]\[19\]

\[(V_{\mathrm{A}}-V_{\mathrm{B}}) = -(\mu_{\mathrm{A}}-\mu_{\mathrm{B}})/e\]
<small>*-e*為[基本电荷](../Page/基本电荷.md "wikilink")。</small>

  - [包利不相容原理](../Page/包利不相容原理.md "wikilink")

费米迪拉克分布可由下式表述：

\[f(\epsilon) = \frac{1}{e^{(\epsilon-\mu) / (k T)} + 1}\]
其结果如右图所示，f越接近1，表示该能量被电子占据的概率越大，f越接近于0，表示该能量被电子占据的概率越小。如图所示，费米能级为0.55eV,
费米能级之上越高，f越接近于0；费米能级之下，能量越低，f越接近于1。

## 參考資料

[Category:凝聚体物理学](../Category/凝聚体物理学.md "wikilink")
[F](../Category/固体物理学.md "wikilink")
[F](../Category/统计力学.md "wikilink")

1.

2.

3.
4.
5.
6.
7.
8.

9.  The use of the term "Fermi energy" as synonymous with [Fermi
    level](../Page/Fermi_level.md "wikilink") (a.k.a. [electrochemical
    potential](../Page/electrochemical_potential.md "wikilink")) is
    widespread in semiconductor physics. For example: [*Electronics
    (fundamentals And
    Applications)*](https://books.google.com/books?id=n0rf9_2ckeYC&pg=PA49)
    by D. Chattopadhyay, [*Semiconductor Physics and
    Applications*](https://books.google.com/books?id=lmg13dHPKg8C&pg=PA113)
    by Balkanski and Wallis.

10.
11.
12.
13.
14.
15.
16.
17.
18.
19.