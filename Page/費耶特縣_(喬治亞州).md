**費耶特縣**（）是[美國](../Page/美國.md "wikilink")[喬治亞州西北部的一個縣](../Page/喬治亞州.md "wikilink")。面積516平方公里。根據[美國人口調查局](../Page/美國人口調查局.md "wikilink")2000年統計，費耶特縣人口共有91,263人，其中[白人占](../Page/白人.md "wikilink")83.87%、[非裔美國人占](../Page/非裔美國人.md "wikilink")11.47%。縣治為[費耶特維爾
(喬治亞州)](../Page/費耶特維爾_\(喬治亞州\).md "wikilink") （）。

費耶特縣成立於1821年5月15日，而縣名是紀念[美國獨立戰爭英雄](../Page/美國獨立戰爭.md "wikilink")、[法國大革命領導者](../Page/法國大革命.md "wikilink")，[拉法葉侯爵](../Page/拉法葉侯爵.md "wikilink")。

[F](../Category/佐治亚州行政区划.md "wikilink")