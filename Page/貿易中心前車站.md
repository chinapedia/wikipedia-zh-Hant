**貿易中心前車站**（）是一個位於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[住之江區南港北](../Page/住之江區.md "wikilink")、由[大阪市高速電氣軌道](../Page/大阪市高速電氣軌道.md "wikilink")（大阪地下鐵）所經營的[鐵路車站](../Page/鐵路車站.md "wikilink")，是該公司旗下的[中運量大眾運輸系統](../Page/中運量大眾運輸系統.md "wikilink")（新電車）路線[南港港城線沿線車站之一](../Page/南港港城線.md "wikilink")，車站編號P10。因鄰近的之舊名「大阪世界貿易中心大樓」（Osaka
World Trade Center Building，或又稱為WTC Cosmo Tower）而得名。

## 車站構造

貿易中心前是一個[高架車站](../Page/高架車站.md "wikilink")，但也可被視為一個架在高架路線上的[跨線式站房](../Page/跨線式站房.md "wikilink")（橋上站房）車站，因此其[車站大廳與](../Page/車站大廳.md "wikilink")[剪票口很特殊地是設在最高的三樓處](../Page/剪票口.md "wikilink")，二樓才是真正的[月台與路線層](../Page/月台.md "wikilink")。月台區採一座[島式月台兩條乘車線的配置](../Page/島式月台.md "wikilink")。

### 月台配置

| 月台 | 路線                                                                     | 目的地                                                                               |
| -- | ---------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| 1  | [9P.png](https://zh.wikipedia.org/wiki/File:9P.png "fig:9P.png") 南港港城線 | [中埠頭](../Page/中埠頭站_\(大阪府\).md "wikilink")、[住之江公園方向](../Page/住之江公園站.md "wikilink") |
| 2  | 往[宇宙廣場](../Page/宇宙廣場站.md "wikilink")                                   |                                                                                   |

貿易中心前站月台

## 历史

  - 1997年（[平成](../Page/平成.md "wikilink")9年）12月18日：（Osaka port Transport
    System，OTS）新電車科技港線（）宇宙廣場～中埠頭間的路段通車，本站也同步啟用。
  - 2005年（平成17年）7月1日：OTS將運輸系統營運部份的業務轉移由大阪市交通局管理，本站也隨之成為大阪市交通局下轄的車站。
  - 2018年（平成30年）4月1日：大阪市交通局民營化，成立非上市公司大阪市高速電氣軌道經營原交通局所擁有的各路線，本站也隨之成為該公司旗下的車站。

## 相鄰車站

  - 大阪市高速電氣軌道（大阪地下鐵）
    [9P.png](https://zh.wikipedia.org/wiki/File:9P.png "fig:9P.png")
    南港港城線（新電車）
      -
        [宇宙廣場](../Page/宇宙廣場站.md "wikilink")（P-09）－**貿易中心前（P-10）**－[中埠頭](../Page/中埠頭站_\(大阪府\).md "wikilink")（P-11）

## 外部連結

  - [車站導覽：貿易中心前站](http://www.osakametro.co.jp/station-guide/P/p10.html)
    - Osaka Metro

[Reedosentaa](../Category/日本鐵路車站_To.md "wikilink")
[Category:住之江區鐵路車站](../Category/住之江區鐵路車站.md "wikilink")
[Category:南港港城線車站](../Category/南港港城線車站.md "wikilink")
[Category:1997年啟用的鐵路車站](../Category/1997年啟用的鐵路車站.md "wikilink")
[Category:以建築物命名的鐵路車站](../Category/以建築物命名的鐵路車站.md "wikilink")