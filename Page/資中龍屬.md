**資中龍屬**（屬名：*Zizhongosaurus*）是種原始[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於早[侏儸紀的](../Page/侏儸紀.md "wikilink")[中國](../Page/中國.md "wikilink")。資中龍的大體型[草食性恐龍](../Page/草食性.md "wikilink")，擁有長頸部。

[模式種是](../Page/模式種.md "wikilink")*' 船城資中龍*'（*Z.
chuanchengensis*），是由[董枝明等人在](../Page/董枝明.md "wikilink")1983年所敘述、命名。化石發現於[中國](../Page/中國.md "wikilink")[四川省](../Page/四川省.md "wikilink")[資中縣](../Page/資中縣.md "wikilink")。[全模標本包含](../Page/全模標本.md "wikilink")：一節部分背部[背椎](../Page/背椎.md "wikilink")（編號V9067.1）、[肱骨](../Page/肱骨.md "wikilink")（編號V9067.2）、以及[恥骨](../Page/恥骨.md "wikilink")（編號V9067.3）\[1\]。這三個標本可能都來自於同一個體。

在1999年的一份期刊，李奎使用黃石版資中龍（*Z.
huangshibanensis*）這名稱\[2\]但之後沒有經過正式研究、命名，目前是個[無資格名稱](../Page/無資格名稱.md "wikilink")（Nomen
nudum）。

資中龍最初被歸類於[鯨龍科](../Page/鯨龍科.md "wikilink")。之後被歸類於[火山齒龍科](../Page/火山齒龍科.md "wikilink")，被認為是[巨腳龍的近親](../Page/巨腳龍.md "wikilink")；或是鯨龍科的[蜀龍亞科](../Page/蜀龍亞科.md "wikilink")。資中龍目前長被認為是[疑名](../Page/疑名.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [*Zizhongosaurus* at the Dinosaur
    Encyclopedia](http://web.me.com/dinoruss/de_4/5a6c1a6.htm)
  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。

[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:蜥腳下目](../Category/蜥腳下目.md "wikilink")

1.  Dong, Z., Zhou, S., Zhang, Y. (1983). "Dinosaurs from the Jurassic
    of Sichuan". *Palaeontologica Sinica* 162 New Series C 23. Science
    Press Peking: pp. 1-136
2.  K. Li, Y. Zhang, K. Cai, 1999, *The Characteristics of the
    Composition of the Trace Elements in Jurassic Dinosaur Bones and Red
    Beds in Sichuan Basin*, Geological Publishing House, Beijing