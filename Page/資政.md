**资政**，是[中華民國](../Page/中華民國.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[緬甸等國的一項高級官銜](../Page/緬甸.md "wikilink")，乃使退職之高级官員或者是德高望重者继续參與、輔助國政的崇高職銜。

## 起源

[古代中国曾經設有名稱相近的官职](../Page/古代中国.md "wikilink")，例如[資政大夫](../Page/資政大夫.md "wikilink")、[資政殿大學士](../Page/資政殿大學士.md "wikilink")。

## 中華民國

在中華民國，[總統府資政簡稱資政](../Page/中華民國總統府資政.md "wikilink")，与[国策顾问](../Page/中華民國總統府國策顧問.md "wikilink")、[戰略顧問於民間雅稱為](../Page/中華民國總統府戰略顧問.md "wikilink")「[三公](../Page/三公.md "wikilink")」，來自各專業領域、為國建言。三者皆是[总统必要时请益](../Page/总统.md "wikilink")、[谘询](../Page/谘询.md "wikilink")[公共政策的人士](../Page/公共政策.md "wikilink")。一般任期为一年，為無給薪[榮譽職](../Page/榮譽.md "wikilink")。\[1\]

依據《中華民國總統府組織法》第十五條之規定，總統府資政為無給職，聘期不得逾越總統任期，人數不得逾三十人。\[2\]

## 新加坡

新加坡的资政分为[内阁资政](../Page/内阁资政.md "wikilink")（Minister
，原译高级部长）和[国务资政](../Page/国务资政.md "wikilink")（Senior
Minister）。其中内阁资政祇设一位，由[李光耀任职](../Page/李光耀.md "wikilink")；李光耀因此受[新加坡人尊称为李资政](../Page/新加坡.md "wikilink")。资政是[总理或国家領導人退職後方能擔任](../Page/总理.md "wikilink")，是仅次于国家[总统](../Page/总统.md "wikilink")、[总理之下的国家](../Page/总理.md "wikilink")[领袖](../Page/领袖.md "wikilink")。

[新加坡的资政是参照](../Page/新加坡.md "wikilink")[中華民國的职级](../Page/中華民國.md "wikilink")，赋予了“资政”新内涵，於是成為[新加坡和](../Page/新加坡.md "wikilink")[中華民國的通用名词](../Page/中華民國.md "wikilink")。

## 中華人民共和國

[改革开放之初](../Page/改革开放.md "wikilink")，曾經有[中央顾问委员会由老](../Page/中央顾问委员会.md "wikilink")[干部所組成](../Page/干部.md "wikilink")，中顾委的委员亦是当时现任的黨和國家領導們諮詢國政的重要對象。目前部分超龄退休的地方政府官员会转任资政发挥余热。

## 缅甸

[国务资政是](../Page/国务资政.md "wikilink")[缅甸自](../Page/缅甸.md "wikilink")2016年4月起设立的职务，任期与[缅甸总统一样为五年](../Page/缅甸总统.md "wikilink")，被外界视为相当于实行[总统制前的](../Page/总统制.md "wikilink")[缅甸总理职务](../Page/缅甸总理.md "wikilink")，首任资政为[昂山素季](../Page/昂山素季.md "wikilink")。

## 參見

  - [中華民國總統府資政](../Page/中華民國總統府資政.md "wikilink")
  - [資政大夫](../Page/資政大夫.md "wikilink")
  - [国务资政](../Page/国务资政.md "wikilink")
  - [内阁资政](../Page/内阁资政.md "wikilink")

## 參考文獻

[category:中華民國政治](../Page/category:中華民國政治.md "wikilink")
[category:新加坡政治](../Page/category:新加坡政治.md "wikilink")

1.  {{ cite web | title = 中華民國總統府 | url =
    <https://www.president.gov.tw/Page/109> | language = zh-tw |
    accessdate = 2018-07-17 }}
2.  {{ cite web | title = 中華民國總統府組織法 | url =
    <https://www.president.gov.tw/Page/284/1459/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B%E7%B8%BD%E7%B5%B1%E5%BA%9C%E7%B5%84%E7%B9%94%E6%B3%95>
    | language = zh-tw | accessdate = 2018-07-17 }}