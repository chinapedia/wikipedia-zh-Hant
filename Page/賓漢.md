是一部於1959年上映的[美國電影](../Page/美國電影.md "wikilink")，劇情改編自小說《》，由影史大師[威廉·惠勒執導](../Page/威廉·惠勒.md "wikilink")，[查爾登·希士頓主演](../Page/查爾登·希士頓.md "wikilink")，榮獲[AFI百年百大电影第](../Page/AFI百年百大电影.md "wikilink")72名。此片是第一部大受欣赏，获得11項[奧斯卡獎的電影](../Page/奧斯卡金像獎.md "wikilink")（此紀錄保持長達40年後才被《[鐵達尼號](../Page/鐵達尼號_\(1997年電影\).md "wikilink")》、《[魔戒](../Page/魔戒电影三部曲.md "wikilink")》追平）。《賓漢》是1907年及1925年黑白片同名電影的重拍版。

## 劇情

透過一個生活於[羅馬帝國](../Page/羅馬帝國.md "wikilink")，名為「賓漢」的貴族觀察及交友，此電影敘述帝國之富盛時期[耶穌的事跡](../Page/耶穌.md "wikilink")。

在耶路撒冷有個猶太人的富戶貴族：賓漢。其中有年輕的猶大·賓漢。猶大的兒時好朋友馬生拉从罗马回到犹大所在的省当执政官，并且想要通过严苛镇压当地犹太反对罗马统治的思想以获得凯撒的青睐。马生拉希望犹大能够以犹太贵族的身份说服反对罗马统治的的犹太思想，同时也能揭发那些顽固的反罗马势力以便镇压。
但是犹大拒绝出卖自己的人民。两人也因此不再往来。 不久新任罗马派来的长官到了犹大和 马生拉所在省份上任。 遊行耶路撒冷的街道时，
犹大和妹妹也站在自己家城楼上看。 由于屋瓦年久松动，
犹大妹妹不小心碰落一块，不巧砸中了經過「賓漢府」的罗马长官。
犹大，母亲和妹妹全部被抓走。 馬生拉负责調查此事，他知道這是意外，但是为了展现他秉公执法，连好友也绝不开恩，
同时也顺便铲除不愿意为自己揭发同胞的犹大，
马生拉将犹大未经审判就直接流放去做苦役。 在离开家乡去流放的路上，
押解的士兵也对犹大刻意刁难。
犹大百般受苦，就在他快要被押解士兵活活渴死的时候，一个人：耶稣喂了他一杯凉水，使得犹大得以活到流放地，并有活下去的力量。
犹大被分配到海中为戰船划槳做苦役。在一次海戰中，犹大所在战船被毁，猶大救了在船上督戰，職位僅次於皇帝的執政官，并阻止了以为战败的執政官自杀。没想到船雖沉没，但是战役却取得了胜利。執政官也因此得到了凯撒大帝的嘉奖。
作为回报，犹大在執政官的帮助下被赦免了苦役，并被執政官收作乾兒子。猶大決定回耶路撒冷報仇并寻找母亲和妹妹。回到耶路撒冷以后，犹大以为母亲和妹妹已经死亡。他知道马生拉会参加战马车比赛，由于比赛风险极高，常常死伤惨重，于是想要通过比赛来复仇。。。

## 角色

  - [卻爾登·希斯頓](../Page/卻爾登·希斯頓.md "wikilink") 飾

  - 飾

  - 飾 米里亞姆

  - [Cathy O'Donnell](../Page/Cathy_O'Donnell.md "wikilink") 飾 Tirzah
    Bat-Hur

  - [Haya Harareet](../Page/Haya_Harareet.md "wikilink") 飾

  - 飾 西蒙尼德

  - 飾 Quintus Arrius

  - [Terence Longdon](../Page/Terence_Longdon.md "wikilink") 飾 德魯甦斯

  - 飾

  - [Frank Thring](../Page/Frank_Thring.md "wikilink") 飾
    [本丟·彼拉多](../Page/本丟·彼拉多.md "wikilink")

  - [Claude Heater](../Page/Claude_Heater.md "wikilink") （未掛名） 飾
    [耶穌](../Page/耶穌.md "wikilink")

  - [Marina Berti](../Page/Marina_Berti.md "wikilink") 飾 弗拉維亞

  - [Jose Greci](../Page/Jose_Greci.md "wikilink") （未掛名） 飾
    [馬利亞](../Page/馬利亞_\(耶穌的母親\).md "wikilink")

  - [Laurence Payne](../Page/Laurence_Payne.md "wikilink") （未掛名） 飾
    [若瑟](../Page/聖若瑟.md "wikilink")

  - [Richard Hale](../Page/Richard_Hale.md "wikilink") （未掛名） 飾 加斯帕

  - [Reginald Lal Singh](../Page/Reginald_Lal_Singh.md "wikilink") 飾
    梅爾基奧爾

  - 邁克爾·杜根 （未掛名） 飾 水手

  - 芬利·柯里 飾
    [Balthasar](../Page/东方三博士.md "wikilink")/[旁白](../Page/旁白.md "wikilink")

## 票房

在首映期間，這部電影在北美租賃（分銷商在票房的分額）獲得了3360萬美元，且收穫了7470萬美元的票房銷售額。在其他地區（北美以外），《賓漢》收穫了3250萬的租金（票房約7220萬美元），全球總共有6610萬美元的租金收入，票房則收入1.469億美元\[1\]，使這部電影成為[1959年票房收入最高的一部電影](../Page/全球最高電影票房.md "wikilink")\[2\]。《賓漢》的成功，拯救了正困於金融災難中的[米高梅](../Page/米高梅.md "wikilink")\[3\]。

## 榮譽

  - 获得[第32届奥斯卡](../Page/第32届奥斯卡金像奖.md "wikilink")11项大奖：
      - 最佳影片
      - 最佳导演-[威廉·惠勒](../Page/威廉·惠勒.md "wikilink")
      - 最佳男主角-[卻爾登·希斯頓](../Page/卻爾登·希斯頓.md "wikilink")
      - 最佳男配角-
      - 最佳艺术指导
      - 最佳摄影
      - 最佳视觉效果
      - 最佳服装设计
      - 最佳剪辑
      - 最佳原创音乐
      - 最佳混音
      - 提名-最佳改编剧本

<!-- end list -->

  - 金球奖
      - 最佳影片
      - 最佳导演
      - 最佳男配角-
      - 特殊奖（directing the chariot race sequence）-Andrew Marton

<!-- end list -->

  - 英国学院奖（BAFTA）最佳影片

## 參見

  -
## 參考資料

## 外部連結

  -
  -
  -
  -
  -
[Category:1959年電影](../Category/1959年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:重拍電影](../Category/重拍電影.md "wikilink")
[Category:三小時以上電影](../Category/三小時以上電影.md "wikilink")
[Category:美國劇情片](../Category/美國劇情片.md "wikilink")
[Category:美国史诗片](../Category/美国史诗片.md "wikilink")
[Category:威廉·惠勒电影](../Category/威廉·惠勒电影.md "wikilink")
[Category:米高梅電影](../Category/米高梅電影.md "wikilink")
[Category:史詩宗教電影](../Category/史詩宗教電影.md "wikilink")
[Category:耶稣基督相关电影作品](../Category/耶稣基督相关电影作品.md "wikilink")
[Category:古羅馬背景電影](../Category/古羅馬背景電影.md "wikilink")
[Category:金球獎最佳劇情片](../Category/金球獎最佳劇情片.md "wikilink")
[Category:英国电影学院奖最佳影片](../Category/英国电影学院奖最佳影片.md "wikilink")
[Category:奧斯卡最佳影片](../Category/奧斯卡最佳影片.md "wikilink")
[Category:奧斯卡最佳導演獲獎電影](../Category/奧斯卡最佳導演獲獎電影.md "wikilink")
[Category:奧斯卡最佳男主角獲獎電影](../Category/奧斯卡最佳男主角獲獎電影.md "wikilink")
[Category:奧斯卡最佳男配角獲獎電影](../Category/奧斯卡最佳男配角獲獎電影.md "wikilink")
[Category:奧斯卡最佳美術指導獲獎電影](../Category/奧斯卡最佳美術指導獲獎電影.md "wikilink")
[Category:奧斯卡最佳攝影獲獎電影](../Category/奧斯卡最佳攝影獲獎電影.md "wikilink")
[Category:奥斯卡最佳视觉效果获奖电影](../Category/奥斯卡最佳视觉效果获奖电影.md "wikilink")
[Category:奥斯卡最佳服装设计奖获奖电影](../Category/奥斯卡最佳服装设计奖获奖电影.md "wikilink")
[Category:奥斯卡最佳剪辑获奖电影](../Category/奥斯卡最佳剪辑获奖电影.md "wikilink")
[Category:奧斯卡最佳原創配樂獲獎電影](../Category/奧斯卡最佳原創配樂獲獎電影.md "wikilink")
[Category:痲瘋題材作品](../Category/痲瘋題材作品.md "wikilink")
[Category:奥斯卡最佳音响效果获奖电影](../Category/奥斯卡最佳音响效果获奖电影.md "wikilink")

1.  Block and Wilson, p. 324.
2.  Stempel, p. 23.
3.  Malone, p. 23.