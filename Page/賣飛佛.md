**賣飛佛**，是****的[港式英語讀音](../Page/港式英語.md "wikilink")，意思是「我的最愛」。

## 出處

2005年[香港演員](../Page/香港演員.md "wikilink")[吳卓羲為](../Page/吳卓羲.md "wikilink")[泰林電器拍攝的電視廣告](../Page/泰林電器.md "wikilink")，[吳卓羲以非常重的](../Page/吳卓羲.md "wikilink")[港式英語口音讀出](../Page/港式英語.md "wikilink")「（泰林，我的最愛）」，當中「」這一句講得尤其難聽，就好像用廣東話讀「賣飛佛」這三個字一樣，所以「賣飛佛」的意思等同為「我的最愛」。

## 《[迎妻接福](../Page/迎妻接福.md "wikilink")》

[2007年無綫電視賀歲劇](../Category/2007年無綫電視劇集.md "wikilink")《[迎妻接福](../Page/迎妻接福.md "wikilink")》中曾出現「賣飛佛」這句對白，但字幕卻把之錯寫成「買飛佛」。

## 《[學警出更](../Page/學警出更.md "wikilink")》

而在吳卓羲主演的《[學警出更](../Page/學警出更.md "wikilink")》其中一段宣傳片中，[黎萱在末段用準確的](../Page/黎萱.md "wikilink")[英語說出](../Page/英語.md "wikilink")「**My
Favourite**」，有部分人認為這是對[港式英語及吳卓羲本人的嘲諷](../Page/港式英語.md "wikilink")。

## 參考

  - [蘋果日報 - 娛樂 - 20060821 -
    吳卓羲普通話宣傳「癲屍豬」網民笑爆嘴](https://web.archive.org/web/20070930184311/http://www1.appledaily.atnext.com//template/apple/art_main.cfm?iss_id=20060821&sec_id=462&subsec_id=830&art_id=6237654)
  - [蘋果日報 - 娛樂 - 20061022 - 瘀氣籠罩娛樂圈
    吳卓羲 10級"火羅"王　郭羨妮施肥惡](https://web.archive.org/web/20070824173306/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20061022&sec_id=462&subsec_id=830&art_id=6431285)
  - [蘋果日報 - 娛樂 - 20061122 - 資料庫
    藝人英語發音嚇親人](https://web.archive.org/web/20070930203848/http://www1.appledaily.atnext.com//template/apple/art_main.cfm?iss_id=20061122&sec_id=462&subsec_id=830&art_id=6546860)
  - [蘋果日報 - 娛樂 - 20061231
    - 06年娛圈金句](http://www1.appledaily.atnext.com//template/apple/art_main.cfm?iss_id=20061231&sec_id=462&subsec_id=830&art_id=6670691)
  - [蘋果日報 - 娛樂 - 20070426 - 娛樂資料庫
    亂up一通成笑柄](https://web.archive.org/web/20070514134431/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070426&sec_id=462&subsec_id=830&art_id=7039898)

## 外部連結

  - [泰林電器電視廣告(中文版)](http://www.youtube.com/watch?v=NSoHQQ8oX1c)

[Category:香港俚語](../Category/香港俚語.md "wikilink")
[Category:惡搞文化](../Category/惡搞文化.md "wikilink")