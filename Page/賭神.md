是1989年上映的[香港電影](../Page/香港電影.md "wikilink")。[王晶導演](../Page/王晶.md "wikilink")，由[周潤發和](../Page/周潤發.md "wikilink")[劉德華主演](../Page/劉德華.md "wikilink")，香港[票房總收入](../Page/票房.md "wikilink")3706萬[港元](../Page/港元.md "wikilink")，成為1989年香港電影票房冠軍。

## 劇情

賭壇至高無上高手，人稱「賭神」的**高進**（周潤發飾），喜歡吃德國Feodora[巧克力和佩戴瑞士](../Page/巧克力.md "wikilink")[歐米茄蝶飛](../Page/歐米茄錶公司.md "wikilink")（Omega
De
Ville）[手錶](../Page/手錶.md "wikilink")。他賭技深不可测，包括聽[骰子及變牌等](../Page/骰子.md "wikilink")，其賭牌的心理戰術更是無人能敵。在片頭一開始接受[日本人](../Page/日本人.md "wikilink")**上山宏次**（鹿村泰祥飾）的對賭，在[麻將及擲骰子比試皆獲勝](../Page/麻將.md "wikilink")。

上山宏次的父親因被新加坡賭王老千**陳金城**（鮑漢琳飾）詐賭，憤而自殺，上山為報父仇，演了一場[苦肉計](../Page/苦肉計.md "wikilink")，請託高進幫他報父仇，打敗陳金城。上山宏次為此派了一個貼身[保鑣](../Page/保鑣.md "wikilink")，曾任[南越特種部隊上尉的](../Page/越南共和國.md "wikilink")**龍五**（向華強飾）保護高進的安全。

高進回到香港後，因在一次賭局中為了保護朋友，親自出馬贏了一名黑幫首領南哥（楊澤霖飾）的錢而遭到追殺。在[火車上龍五及時出現解決了高進的麻煩](../Page/火車.md "wikilink")，然而高進下火車時卻一不小心从山上跌落，被平時遊手好閒的混混**陳刀仔**（劉德華飾）與**烏鴉**（黃斌飾）所救。

頭部受到撞擊的高進患上[失憶症](../Page/失憶症.md "wikilink")，智商因而退化到與孩童無異。不過依舊保持著喜歡吃德國Feodora巧克力習慣，所以陳刀仔的女友**阿珍**（王祖賢飾）幫他取名為「巧克力」。不認識賭神的陳刀仔發現了他超卓的賭技，而開始加以利用。在與**大口九**（成奎安飾）玩[梭哈的時候](../Page/梭哈.md "wikilink")，高進憑藉著過去吃巧克力的習慣不經意地回復了以前「賭神」的能力，變出了一張3，反敗為勝贏了錢，讓陳刀仔瞠目結舌。陳刀仔見到有利可圖，與阿珍及烏鴉終日帶著「朱古力」穿梭大小賭場，渡過了一陣快樂的時光。在連串的贏錢之後，陳刀仔更相信這個傻瓜其實是「[賭博天才](../Page/賭博.md "wikilink")」，因而胃口變大跑去向財務公司老闆「**花柳成**」（吳孟達飾）借[高利貸想大撈一筆](../Page/高利貸.md "wikilink")。

但「朱古力」與陳刀仔口角，一怒之下，把借來的錢都輸光了，陳刀仔見狀非常惱怒，又不知怎麼償還債款，想要拋棄「朱古力」，卻又於心不忍，為了躲債，陳刀仔安排了大家先住進好朋友排骨（羅青浩飾）的銷魂別墅[時鐘酒店避風頭](../Page/時鐘酒店.md "wikilink")。

此時「賭神」意外失蹤的消息令上山宏次感到焦急，想盡辦法奔走尋找其下落。高進的堂弟**高義**（龍方飾），平日即嫉妒堂哥的風采并垂涎堂嫂**珍妮**（張敏飾）的美色和堂哥的钱财，在此時顯露禍心。高義试图强暴珍妮，珍妮激烈抵抗，被高義失手推下楼而死亡，然而高義与珍妮的对话，却被她以[隨身聽录下](../Page/隨身聽.md "wikilink")，他烧毁[录音带後](../Page/录音带.md "wikilink")，憤而對珍妮[姦屍](../Page/姦屍.md "wikilink")，后来他私下與陳金城勢力結合，並且封鎖有關高進的消息意圖取代其地位。

當陳刀仔帶「朱古力」做腦部檢查時，卻被「花柳成」逮著，陳刀仔在大廈外牆[棚架與](../Page/棚架.md "wikilink")「花柳成」的嘍囉們對決，但還是被擄走了。阿珍意外發現「朱古力」的背影很像「賭神」，於是就把「朱古力」打扮成「賭神」的樣子去營救陳刀仔。在「花柳成」的面前，「朱古力」展露了一番賭技與耍刀的功夫，阿珍見機不可失，立刻以刀子刺傷並挾持「花柳成」，把陳刀仔救出。

高義及龍五皆得知高進的位置，於是雙方展開了一場「搶人大戰」，高義打算殺死高進，而陳刀仔亦意外看見高義正用槍指著高進。在激烈的槍戰中，高進受到了相當大的驚嚇，當龍五被槍打中的那一刻，強烈的刺激使高進短暫恢復了神智清醒了過來，他撿起地上的[手槍開槍還擊救了龍五](../Page/手槍.md "wikilink")。但隨後在馬路上被[汽車撞擊使他被送進](../Page/汽車.md "wikilink")[醫院](../Page/醫院.md "wikilink")。高進撞傷時，恢復了之前的狀態並保留了從山坡下受傷後的記憶，但為了保護自己的安全及準備向高義還擊，只好裝作忘記了陳刀仔等人。

高進履行與上山宏次的承諾與陳金城在[公海上一戰](../Page/公海.md "wikilink")。而陳刀仔想告訴高進，其實高義是幕後黑手，高進憂心陳刀仔的安危，因此讓陳刀仔上船，卻刻意不與他相認。

賭局開始前，高義偷偷地與陳金城密謀合作一同擊敗高進，提供了液晶體顯影[眼鏡及隱形藥水的策略](../Page/眼鏡.md "wikilink")。此時陳金城也發現了在近500場的[撲克牌梭哈賭局中](../Page/撲克牌.md "wikilink")，只要賭神想“[偷雞](../Page/偷雞.md "wikilink")”（）時必定會先摸摸手上的[戒指](../Page/戒指.md "wikilink")。在這兩大破綻之下，陳金城認為自己有必勝把握。賭局開始，高進不再是那麼的意氣風發連輸了幾場，把帶來的錢都輸光了。在賭上自己身家的最後一局中，高進以四張A，贏了陳金城，幫上山宏次出了一口氣。

原來高進早已猜中陳金城的盤算，自承「偷雞」的摸戒指動作，是他在最近五百次牌局中故意加上去的。另外對於高義，高進早就知道他的惡行劣跡，用隱形液晶體顯影眼鏡反將了高義一軍，並製造了陳金城在香港[海域殺了高義的結局](../Page/海域.md "wikilink")（高進在高義手中黏上仿真槍的玩具手槍，令陳金城誤殺高義），其實高進發現一卷沒燒完的錄音帶，早就得知珍妮是被高義所害。

由於依[萬國公法](../Page/萬國公法.md "wikilink")，在公海上的輪船司法管轄權在輪船註冊國手上，而陳金城又與巴拿馬總統有點交情，故陳金城原以為高枕無憂，但因高進計謀，輪船未駛出公海，仍在香港水域，最後陳金城被[香港警隊拘捕](../Page/香港警隊.md "wikilink")，並被法庭判處[誤殺罪名成立](../Page/誤殺.md "wikilink")，入獄7年。

陳刀仔對於賭神不認得他耿耿於懷，他失望地回到老家在桌上不斷地揣摩洗牌的技巧。高進意外地出現了在他的面前，並解說了當時不相認的原因：「若我當時認了你，那傢伙便不會中計了。」因此可證明高進仍記得跟陳刀仔所發生的事情，只是礙於報復計劃而不得不裝作遺忘。

最後賭神收了陳刀仔為弟子，並帶他去美國[拉斯維加斯學習賭術](../Page/拉斯維加斯.md "wikilink")。本部續集為[赌侠](../Page/赌侠.md "wikilink")。

## 演員

|                                       |                                                                                                                                                                      |
| ------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **演員**                                | **角色**                                                                                                                                                               |
| 周潤發                                   | 高進／朱古力，賭神                                                                                                                                                            |
| 劉德華                                   | 陳刀仔 （粵語配音：[黃志成](../Page/黃志成.md "wikilink")）                                                                                                                          |
| [向華強](../Page/向華強.md "wikilink")      | 龍五 越南華僑，越南[阮文紹時期](../Page/阮文紹.md "wikilink")[南越陸軍](../Page/越南共和國陸軍.md "wikilink")19連[上尉](../Page/上尉.md "wikilink")，擁有優秀的槍法和武術（粵語配音：[梁政平](../Page/梁政平.md "wikilink")） |
| [張敏](../Page/張敏_\(演員\).md "wikilink") | Janet，高進妻子 （粵語配音：[李司棋](../Page/李司棋.md "wikilink")）                                                                                                                   |
| 王祖賢                                   | 珍，陳刀仔女朋友（粵語配音：[沈小蘭](../Page/沈小蘭.md "wikilink")）                                                                                                                      |
| [成奎安](../Page/成奎安.md "wikilink")      | 大口九，非法賭場首領（粵語配音：[盧雄](../Page/盧雄.md "wikilink")）                                                                                                                      |
| [吳孟達](../Page/吳孟達.md "wikilink")      | 花柳成，財務公司老闆                                                                                                                                                           |
| 龍方                                    | 高義，高進的堂弟（粵語配音：[劉江](../Page/劉江.md "wikilink")）                                                                                                                        |
| [黃斌](../Page/黃斌.md "wikilink")        | 烏鴉，陳刀仔義氣小弟                                                                                                                                                           |
| [陳立品](../Page/陳立品.md "wikilink")      | 陳小刀奶奶 （粵語配音：[陶碧儀](../Page/陶碧儀.md "wikilink")）                                                                                                                        |
| [黃新](../Page/黃新.md "wikilink")        | 珍父（客串）（粵語配音：[金剛](../Page/金剛_\(播音員\).md "wikilink")）                                                                                                                  |
| [上官玉](../Page/上官玉.md "wikilink")      | 珍母（客串）（粵語配音：[林丹鳳](../Page/林丹鳳.md "wikilink")）                                                                                                                        |
| [鹿村泰祥](../Page/鹿村泰祥.md "wikilink")    | 上山宏次                                                                                                                                                                 |
| [西協美智子](../Page/西協美智子.md "wikilink")  | 菊子                                                                                                                                                                   |
| [楊澤霖](../Page/楊澤霖.md "wikilink")      | 南哥，黑幫首領（粵語配音：[黃子敬](../Page/黃子敬.md "wikilink")）                                                                                                                       |
| [鮑漢琳](../Page/鮑漢琳.md "wikilink")      | 陳金城，新加坡賭王（粵語配音：金剛）                                                                                                                                                   |
| [周文健](../Page/周文健.md "wikilink")      | 美國三藩市賭場經理（客串）（粵語配音：梁政平）                                                                                                                                              |
| 王晶                                    | 銷魂別墅時鐘酒店嫖客（客串）                                                                                                                                                       |
| [陳國新](../Page/陳國新.md "wikilink")      | 醫生（粵語配音：黃子敬）                                                                                                                                                         |
| [江道海](../Page/江道海.md "wikilink")      | 高進與陳金城在公海賭局中的[荷官](../Page/荷官.md "wikilink")\[1\]                                                                                                                     |
| [楊玉梅](../Page/楊玉梅.md "wikilink")      | 南哥別墅服務生，臨時住職荷官                                                                                                                                                       |
| [羅青浩](../Page/羅青浩.md "wikilink")      | （粵語配音：[羅君左](../Page/羅君左.md "wikilink")）排骨（銷魂別墅時鐘酒店負責人，小刀在[九龍的好朋友](../Page/九龍.md "wikilink")）                                                                         |
| [伊凡威](../Page/伊凡威.md "wikilink")      | 老柯，高進朋友（客串）                                                                                                                                                          |

## 系列電影及群起模仿

本片的成功帶動了第二波賭博片的熱潮：像是[周星馳的](../Page/周星馳.md "wikilink")**[賭聖](../Page/賭聖.md "wikilink")**。而後許多以搞笑為主的賭片也都以周星馳為主角，周潤發後來也接演了**賭神2**。（第一波熱潮為八零年代初，同樣是王晶導演的《千王鬥千霸》引領）

### 賭神系列

  - **賭神**（God of Gamblers，1989年12月14日）

<!-- end list -->

  -
    主線：賭神高進對新加坡賭王陳金城 。

<!-- end list -->

  - **[賭俠](../Page/賭俠.md "wikilink")**（God of Gamblers II，1990年12月13日）

<!-- end list -->

  -
    本片接續著**賭神**和賭聖發展，這次劉德華與周星馳合作，而周潤發只在結局[客串](../Page/客串.md "wikilink")。
    主線：賭俠陳刀仔與賭聖星仔對賭魔陳金城契仔-{侯塞因}-（台譯-{海珊}-）和特異功能高手「大軍」。

<!-- end list -->

  - **[賭俠2之上海灘賭聖](../Page/賭俠2之上海灘賭聖.md "wikilink")**（God of Gamblers
    III: Back To Shanghai，1991年8月2日）

<!-- end list -->

  -
    這部片是賭俠的的續集作品，不過只有部分《賭俠》中的演員參與。
    主線：賭聖星仔對（上海灘時代）法國賭神 Pierre Cashon 。

<!-- end list -->

  - **[賭神2](../Page/賭神2.md "wikilink")**（God of Gamblers
    Returns，1994年12月15日）

<!-- end list -->

  -
    這部片是賭神續集。
    主線：賭神高進對仇笑痴（[吳興國飾](../Page/吳興國.md "wikilink")）。

<!-- end list -->

  - **[賭神3之少年賭神](../Page/賭神3之少年賭神.md "wikilink")**（God of Gamblers 3:
    The Early Stage，1996年12月14日）

<!-- end list -->

  -
    賭神前傳，由[黎明飾演高進](../Page/黎明.md "wikilink")。屬人物回顧作。
    主線：少年賭神高進對師兄高傲（[吳鎮宇飾](../Page/吳鎮宇.md "wikilink")）。

### 延伸作品：

  - **[賭聖](../Page/賭聖.md "wikilink")**（All for the Winner，1990年8月18日）

<!-- end list -->

  -
    **周星馳**飾演**賭聖**。
    主線：賭聖星仔對香港賭王/洪爺 。

<!-- end list -->

  - **[賭王](../Page/賭王.md "wikilink")**（King of Gambler，1990年11月24日）

<!-- end list -->

  -
    **[林威](../Page/林威.md "wikilink")**飾演**賭王**。

<!-- end list -->

  - **[賭尊](../Page/賭尊.md "wikilink")**（All for the Gamblers，1991年1月24日）

<!-- end list -->

  -
    **[萬梓良](../Page/萬梓良.md "wikilink")**飾演**賭尊**。

<!-- end list -->

  - **[賭霸](../Page/賭聖延續篇：賭霸.md "wikilink")**（The Top
    Bet，賭聖延續篇）（1991年3月7日）

<!-- end list -->

  -
    **賭聖**一片衍伸出來的[電影](../Page/電影.md "wikilink")，只有幾位賭聖的[演員參與演出](../Page/演員.md "wikilink")，周星馳只有一小段的戲份。演員包括[梅艷芳及](../Page/梅艷芳.md "wikilink")[鄭裕玲](../Page/鄭裕玲.md "wikilink")。（梅艷芳飾演賭聖之姐姐，吳孟達飾演**三叔**。）
    主線：賭霸有喜 ( 鄭裕玲飾 ) 對香港賭王洪光 。

<!-- end list -->

  - **[賭魔](../Page/賭魔.md "wikilink")**（Devil Gambler，1991年）

<!-- end list -->

  -
    [邵萱](../Page/邵萱.md "wikilink")、[谷峰主演](../Page/谷峰.md "wikilink")。

<!-- end list -->

  - **[賭煞](../Page/賭煞.md "wikilink")**（The Mighty Gambler，1992年5月15日）

<!-- end list -->

  -
    [胡慧中](../Page/胡慧中.md "wikilink")、[萬梓良主演](../Page/萬梓良.md "wikilink")。

<!-- end list -->

  - **[神龍賭聖之旗開得勝](../Page/神龍賭聖之旗開得勝.md "wikilink")**（1994年）

<!-- end list -->

  -
    [梁朝偉](../Page/梁朝偉.md "wikilink")、[鄭伊健主演](../Page/鄭伊健.md "wikilink")。

<!-- end list -->

  - **[賭聖2之街頭賭聖](../Page/賭聖2之街頭賭聖.md "wikilink")**（Saint of
    Gamblers，1995年6月28日）

<!-- end list -->

  -
    **賭聖**系列的另一衍生作，只有吳孟達飾演相同角色「三叔」，[葛民輝飾演](../Page/葛民輝.md "wikilink")「蒙面賭聖」阿葛(God
    bless you)。
    主線：「蒙面賭聖」阿葛 ( 葛民輝飾 ) 對[澳門賭霸雷泰](../Page/澳門.md "wikilink")。

<!-- end list -->

  - **[賭俠1999](../Page/賭俠1999.md "wikilink")**（The Conman，1998年12月18日）

<!-- end list -->

  -
    劉德華、張家輝、[朱茵主演](../Page/朱茵.md "wikilink")。
    主線：阿King ( 劉德華飾 ) 對賭場大亨馬交文 。

<!-- end list -->

  - **[賭俠大戰拉斯維加斯](../Page/賭俠大戰拉斯維加斯.md "wikilink")**（The Conmen In
    Vegas，1999年6月25日）

<!-- end list -->

  -
    劉德華、[陳百祥](../Page/陳百祥.md "wikilink")、[萬梓良主演](../Page/萬梓良.md "wikilink")。

<!-- end list -->

  - **[賭聖3之無名小子](../Page/賭聖3之無名小子.md "wikilink")**（My Name Is
    Nobody，2000年1月26日）

<!-- end list -->

  -
    由[張家輝飾演無名](../Page/張家輝.md "wikilink")，[舒淇飾演苦兒](../Page/舒淇.md "wikilink")。
    主線：無名 ( 張家輝飾 ) 對叛徒阿駿 。

<!-- end list -->

  - **[中華賭俠](../Page/中華賭俠.md "wikilink")**（Conman in Tokyo，2000年8月31日）

<!-- end list -->

  -
    [古天樂](../Page/古天樂.md "wikilink")、[林國坤](../Page/林國坤.md "wikilink")、張家輝、朱茵、[倉田保昭主演](../Page/倉田保昭.md "wikilink")。
    主線:「神、聖、俠」（賭神、賭聖、賭俠）退隱後，新一代賭俠「亞酷」對決亞洲第一「鐵男」與奸鬼師兄「陽光」死戰。

<!-- end list -->

  - **[賭神之神](../Page/賭神之神.md "wikilink")**（Return from the Other
    World，2002年4月25日）

<!-- end list -->

  -
    [王傑](../Page/王傑.md "wikilink")、[陳松伶](../Page/陳松伶.md "wikilink")、[李子雄主演](../Page/李子雄.md "wikilink")。

<!-- end list -->

  - **[賭俠2002](../Page/賭俠2002.md "wikilink")**（The Conman
    2002，2002年11月7日）

<!-- end list -->

  -
    張家輝、[馮德倫](../Page/馮德倫.md "wikilink")、[王秀琳主演](../Page/王秀琳.md "wikilink")。

<!-- end list -->

  - **[賭俠之人定勝天](../Page/賭俠之人定勝天.md "wikilink")**（Fate Fighter，2003年6月4日）

<!-- end list -->

  -
    [楊恭如和張家輝主演](../Page/楊恭如.md "wikilink")

<!-- end list -->

  - **[賭城風雲](../Page/賭城風雲_\(2014年電影\).md "wikilink")**（From Vegas to
    Macau，2014年1月30日）

<!-- end list -->

  -
    周潤發和[謝霆鋒主演](../Page/謝霆鋒.md "wikilink")

<!-- end list -->

  - **[賭城風雲II](../Page/賭城風雲II.md "wikilink")**（From Vegas to Macau
    Ⅱ，2015年2月19日）

<!-- end list -->

  -
    周潤發和張家輝主演

<!-- end list -->

  - **[賭城風雲III](../Page/賭城風雲III.md "wikilink")**（From Vegas to Macau
    Ⅲ，2016年2月6日）

<!-- end list -->

  -
    周潤發、張家輝和劉德華主演

## 註解

<references />

## 外部連結

  - {{@movies|fGkr20236587}}

  -
  -
  -
  -
  -
  -
[Category:王晶電影](../Category/王晶電影.md "wikilink")
[9](../Category/1980年代香港電影作品.md "wikilink")
[Category:1980年代喜劇片](../Category/1980年代喜劇片.md "wikilink")
[Category:香港喜劇動作片](../Category/香港喜劇動作片.md "wikilink")
[Category:赌博片](../Category/赌博片.md "wikilink")
[Category:永盛电影](../Category/永盛电影.md "wikilink")
[Category:卢冠廷配乐电影](../Category/卢冠廷配乐电影.md "wikilink")

1.  江道海亦是本片的賭術顧問。