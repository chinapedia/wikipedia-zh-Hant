**賴水清**（Samson Lai
Shui-Ching，），[香港](../Page/香港.md "wikilink")[導演](../Page/導演.md "wikilink")，曾任香港[麗的電視及其後的](../Page/麗的電視.md "wikilink")[亞洲電視編導及監製](../Page/亞洲電視.md "wikilink")。賴水清出身於1975年香港[無綫電視第](../Page/無綫電視.md "wikilink")4期藝員訓練班\[1\]，其後轉為幕後並於1978年加入麗的電視。1983年離開亞洲電視後，先後到[新加坡及](../Page/新加坡.md "wikilink")[台灣發展](../Page/台灣.md "wikilink")；1986年到[中國內地發展](../Page/中國內地.md "wikilink")，是第一批北上發展的香港導演之一。1993年擔任[中華電視公司](../Page/中華電視公司.md "wikilink")[綜藝節目](../Page/綜藝節目.md "wikilink")《[連環泡](../Page/連環泡.md "wikilink")》之短劇單元【黃飛鴻會紅】導演。1997年憑藉[臺灣電視公司](../Page/臺灣電視公司.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")《[新龍門客棧](../Page/新龍門客棧_\(電視劇\).md "wikilink")》榮獲[第32屆金鐘獎](../Page/第32屆金鐘獎.md "wikilink")[電視金鐘獎最佳導演獎](../Page/電視金鐘獎.md "wikilink")。

賴水清家有七兄弟姊妹\[2\]。

## 電視劇演出

  - 1975-1976年：[民間傳奇](../Page/民間傳奇.md "wikilink")
  - 1980年：[大内群英](../Page/大内群英.md "wikilink") 飾
    [胤祥](../Page/胤祥.md "wikilink")
  - 1989年：[芙蓉鎮](../Page/芙蓉鎮_\(電視劇\).md "wikilink") 飾 黎桂桂
  - 2013年：[天龍八部](../Page/天龍八部_\(2013年電視劇\).md "wikilink") 飾
    [汪劍通](../Page/汪劍通.md "wikilink")
  - 2014年：[鹿鼎記](../Page/鹿鼎記_\(消歧義\).md "wikilink") 飾
    [鰲拜](../Page/鰲拜.md "wikilink")

## 電影演出

  - 2017年：[香港大師](../Page/香港大師.md "wikilink")　飾 香港大師王天一

## 作品列表

### 助理編導 ([無綫電視](../Page/無綫電視.md "wikilink"))

  - 1977年：[家變](../Page/家變_\(無綫電視劇\).md "wikilink")

### 編導 ([麗的電視](../Page/麗的電視.md "wikilink"))

  - 1979年：[沈勝衣](../Page/沈勝衣.md "wikilink")
  - 1980年：[湖海爭霸錄](../Page/湖海爭霸錄.md "wikilink")
  - 1980年：[大內群英](../Page/大內群英.md "wikilink")
  - 1980年：[風塵淚](../Page/風塵淚.md "wikilink")
  - 1980年：[彩雲深處](../Page/彩雲深處.md "wikilink")
  - 1981年：[浴血太平山](../Page/浴血太平山.md "wikilink")
  - 1981年：[再會太平山](../Page/再會太平山.md "wikilink")
  - 1981年：[甜甜廿四味](../Page/甜甜廿四味.md "wikilink")

### 監製 ([麗的電視](../Page/麗的電視.md "wikilink")/[亞洲電視](../Page/亞洲電視.md "wikilink"))

  - 1981年：[甜甜廿四味](../Page/甜甜廿四味.md "wikilink")
  - 1981年：[我要高飛](../Page/我要高飛_\(電視劇\).md "wikilink")
  - 1982年：[大將軍](../Page/大將軍_\(電視劇\).md "wikilink")
  - 1982年：[天堂有路](../Page/天堂有路.md "wikilink")
  - 1982年：[烽火情仇](../Page/烽火情仇.md "wikilink")
  - 1983年：[一江春水向東流](../Page/一江春水向東流_\(1983年電視劇\).md "wikilink")
  - 1983年：[浮生三記](../Page/浮生三記.md "wikilink")
  - 1983年：[園中緣](../Page/園中緣.md "wikilink")

### 電視劇 (其他)

  - 1984《[霧鎖南洋](../Page/霧鎖南洋.md "wikilink")》
  - 1987《[还君明珠](../Page/还君明珠_\(电视剧\).md "wikilink")》
  - 1988《[八月桂花香](../Page/八月桂花香.md "wikilink")》
  - 1989《[芙蓉鎮](../Page/芙蓉鎮_\(電視劇\).md "wikilink")》
  - 1990《[末代儿女情](../Page/末代儿女情.md "wikilink")》
  - 1991《[碧海情天](../Page/碧海情天.md "wikilink")》
  - 1991《[京城四少](../Page/京城四少.md "wikilink")》
  - 1991《[末代皇孫](../Page/末代皇孫.md "wikilink")》
  - 1993《[英雄少年](../Page/英雄少年.md "wikilink")》
  - 1994《[倚天屠龙记](../Page/倚天屠龙记.md "wikilink")》
  - 1995《[今生今世](../Page/今生今世.md "wikilink")》
  - 1996《[新龍門客棧](../Page/新龍門客棧_\(電視劇\).md "wikilink")》
  - 1997《[儂本多情](../Page/儂本多情_\(1997年電視劇\).md "wikilink")》
  - 1998《[江山美人](../Page/江山美人_\(1998年電視劇\).md "wikilink")》
  - 1998《[女巡按](../Page/女巡按.md "wikilink")》
  - 1998《[神雕侠侣](../Page/神雕侠侣.md "wikilink")》
  - 1999《[花木兰](../Page/花木蘭_\(1999年電視劇\).md "wikilink")》
  - 1999《[小双侠闯江湖](../Page/小双侠闯江湖.md "wikilink")》
  - 1999《[绝世双骄](../Page/绝世双骄.md "wikilink")》
  - 2000《东西奇遇结良缘》
  - 2000《笑傲江湖》
  - 2001《[青蛇与白蛇](../Page/青蛇与白蛇.md "wikilink")》
  - 2002《绝世双骄之绝世双雄》
  - 2003《[倚天屠龙记](../Page/倚天屠龍記_\(2003年電視劇\).md "wikilink")》
  - 2004《神医侠侣》
  - 2004《边城小子》
  - 2004《皇后驾到》
  - 2004《[秦王李世民](../Page/秦王李世民_\(电视剧\).md "wikilink")》，导演/编剧
  - 2005《[刁蠻公主](../Page/刁蠻公主.md "wikilink")》
  - 2005《雪域迷城》
  - 2006《新[昨夜星辰](../Page/昨夜星辰.md "wikilink")》，又名《生死孽恋》
  - 2007《绣娘兰馨》
  - 2007《富贵在天》，又名《爱你一生不后悔》
  - 2008《爱无悔》
  - 2008《新情义无价》
  - 2008《[船娘雯蔚](../Page/船娘雯蔚.md "wikilink")》
  - 2009《[再見艷陽天](../Page/再見艷陽天_\(2010年電視劇\).md "wikilink")》
  - 2009《源》
  - 2010《[菩提树下](../Page/菩提树下.md "wikilink")》
  - 2011《[迷雾围城](../Page/迷雾围城.md "wikilink")》
  - 2011《[天涯明月刀](../Page/天涯明月刀.md "wikilink")》
  - 2012《咏春传奇》
  - 2013《[非緣勿擾](../Page/非緣勿擾.md "wikilink")》
  - 2013《[天龍八部](../Page/天龍八部_\(2013年電視劇\).md "wikilink")》饰 汪剑通
  - 2014《[鹿鼎記](../Page/鹿鼎記_\(2014年電視劇\).md "wikilink")》
  - 2017《[花謝花飛花滿天](../Page/花謝花飛花滿天.md "wikilink")》
  - 2019《[重耳传奇](../Page/重耳传奇.md "wikilink")》
  - 待播《[万水千山总是情](../Page/万水千山总是情_\(中国电视剧\).md "wikilink")》

## 參考來源

## 外部連結

  -
  -
  -
  -
  -
[Category:前亞洲電視監製](../Category/前亞洲電視監製.md "wikilink")
[Category:前麗的電視監製](../Category/前麗的電視監製.md "wikilink")
[Category:香港电视剧导演](../Category/香港电视剧导演.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:無綫電視藝員訓練班](../Category/無綫電視藝員訓練班.md "wikilink")
[Category:香港客家人](../Category/香港客家人.md "wikilink")
[Category:鶴山人](../Category/鶴山人.md "wikilink")
[Shui](../Category/賴姓.md "wikilink")

1.  [中華民國剪輯協會〈勇奪金鐘獎導演獎-賴水清導演
    〉，1997年](http://neweforu.weebly.com/uploads/1/0/4/3/104317871/%E8%B3%B4%E6%B0%B4%E6%B8%85.pdf)
2.  [Dumont-Sullivan Funeral Homes & Cremation Services*Obituary for
    Shuyung
    Lai*，2016](http://www.dumontsullivan.com/obituaries/Shuyung-Lai/#!/Obituary)