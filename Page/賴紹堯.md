**賴紹堯**（[日治時期著名](../Page/日治時期.md "wikilink")[詩人](../Page/詩人.md "wikilink")。因其妻為[霧峰林家三小姐林選](../Page/霧峰林家.md "wikilink")，故與[霧峰林家有姻親之誼](../Page/霧峰林家.md "wikilink")。賴氏嗜詩酒，與[林朝崧交情深厚](../Page/林朝崧.md "wikilink")。

## 生平

  - 1906年5月，任[彰化廳員林支廳大庄區庄長](../Page/彰化廳.md "wikilink")。
  - 1910年至1917年，擔任[臺中廳](../Page/臺中廳.md "wikilink")[員林支廳](../Page/員林支廳.md "wikilink")[大庄區區長](../Page/大庄區.md "wikilink")。
  - 1911年11月獲授紳章。

## 櫟社

賴氏為[櫟社創社四人之一](../Page/櫟社.md "wikilink")。櫟社乃日治時期臺灣三大詩社之一，1902年由臺灣中部的古典詩人所組成。即素有大阮小阮之稱的霧峰林家下厝的[林朝崧](../Page/林朝崧.md "wikilink")（[林癡仙](../Page/林癡仙.md "wikilink")、[林俊堂](../Page/林俊堂.md "wikilink")）、[林幼春叔姪及](../Page/林幼春.md "wikilink")[苑裡](../Page/苑裡鎮.md "wikilink")[蔡振豐](../Page/蔡振豐.md "wikilink")（[蔡啟運](../Page/蔡啟運.md "wikilink")）與賴氏共同創立。社長[蔡啟運去世後](../Page/蔡啟運.md "wikilink")，被選為社長。

## 著述

  - 《逍遙詩草》，收錄於《櫟社第一集》1922年秋出版。
  - 《悔之詩抄》，與[連橫合輯](../Page/連橫_\(人名\).md "wikilink")，現有文本可至《臺灣先賢詩文集彙刊》或是《[台灣文獻叢刊續編](http://www.greatman.com.tw/twc1.htm)》查詢

## 評價

  - 賴紹堯詩作，多寫處身異族統治下個人出處進退的衝突矛盾，兼具豪放曠達與柔媚婉約之風格，詩藝甚佳，但生前作品並未結集。（廖振富撰）

## 詩作

<poem> 弱水\[1\] 隔絕蓬山日倚閭，迢迢弱水一封書。 上言努力加餐飯，中有相思慰索居。 獨唱愔愔誰和汝，綺懷渺渺轉愁余。
無端錯把洪喬怨，欲託靈犀當鯉魚。

  - 冬日登八卦山\[2\]

晨登八卦山，信美風景別。 氣和天宇澄，茲遊固佳絕。 冬令行春溫，萬彙忘慘冽。 嘉木蔚成林，欣欣相媚悅。 澗草復青青，山花況未歇。
誰知造化心，正氣有肅殺。 羨彼蘭蕙姿，含芬老巖穴。 時來苟不榮，運傾或未折。 感喟遂成章，持以奉明哲。
</poem>

## 註釋

## 參考文獻

  - [傅錫祺](../Page/傅錫祺.md "wikilink")，《櫟社沿革志略》，1931
  - [廖振富](../Page/廖振富.md "wikilink")，《[櫟社研究新論](http://www2.read.com.tw/epublish/hypage.cgi?HYPAGE=./search/search_detail.hpg&dtd_id=6&sysid=00002842)》台北：鼎文書局股份有限公司，2006年。
    ISBN 986-0045-66-6
  - [王國璠](../Page/王國璠.md "wikilink")、[高志彬與](../Page/高志彬.md "wikilink")[黃哲永主編](../Page/黃哲永.md "wikilink")，《臺灣先賢詩文集彙刊》台北：龍文出版社股份有限公司，2001年。ISBN
    957-8988-91-5
  - [賴振興編](../Page/賴振興.md "wikilink")，《賴氏大宗譜》嘉義:嘉義賴性宗親會，1982年。

## 參照

  - [櫟社](../Page/櫟社.md "wikilink")
  - [霧峰林家](../Page/霧峰林家.md "wikilink")

## 外部連結

  - [國家文化資料庫](https://web.archive.org/web/20120626092727/http://nrch.cca.gov.tw/ccahome/index.jsp)
    中，可搜尋賴紹堯的作品。
  - [櫟社研究新論](http://www2.read.com.tw/epublish/hypage.cgi?HYPAGE=./search/search_detail.hpg&dtd_id=6&sysid=00002842)，有專章討論賴紹堯。

[L](../Category/台灣日治時期人物.md "wikilink")
[L](../Category/台灣詩人.md "wikilink")
[Category:賴姓](../Category/賴姓.md "wikilink")
[Category:大村人](../Category/大村人.md "wikilink")
[Category:台灣紳章附與人物](../Category/台灣紳章附與人物.md "wikilink")

1.  題注：發表於1911年作。
2.  登於《臺灣日日新報》、《漢文臺灣日日新報》均題作〈登八卦山〉。