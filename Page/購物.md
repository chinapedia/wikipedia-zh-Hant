**購物**指在[零售商揀選和](../Page/零售商.md "wikilink")／或[購買貨品或](../Page/購買.md "wikilink")[服務的行為](../Page/服務.md "wikilink")，可視為一種[經濟和](../Page/經濟.md "wikilink")[休閒活動](../Page/休閒.md "wikilink")。至於瘋狂購物、搶購、有發洩傾向的購物，也會用英文shopping的諧音「血拼」來形容。除了血拼的音譯一詞外，仲有另一叫法為「掃貨」。亦有另一以粵語發音為基礎的「濕平」音譯\[1\]。澳門實行零關稅政策，貨品價廉物美，因此被稱為“購物天堂”。

## 古代社會之購物

購物在不同的古代文明早已出現。在[古羅馬](../Page/古羅馬.md "wikilink")，[圖拉真市場](../Page/圖拉真市場.md "wikilink")（[Trajan's
Market](../Page/:en:Trajan's_Market.md "wikilink")）的[taberna就是一種小零售商店](../Page/:en:taberna.md "wikilink")，而在[哈德良长城上發現羅馬人為士兵而寫的](../Page/哈德良长城.md "wikilink")[購物清單](../Page/購物清單.md "wikilink")，歷史可追溯至公元75至125年。\[2\]在中國，很早已有大型的交易市場，稱為[市集](../Page/市集.md "wikilink")。

## 參與者

### 購物者

[Window_shopping_at_Eaton's.jpg](https://zh.wikipedia.org/wiki/File:Window_shopping_at_Eaton's.jpg "fig:Window_shopping_at_Eaton's.jpg")，1937年\]\]
對很多人來說，購物是一種休閒活動，可以逛不同的商店選購[產品](../Page/產品.md "wikilink")。同時又出現了所謂的「櫥窗購物」（Window
shopping），意指瀏覽商店櫥窗中的貨品，未必會真的購買。但對於一些人，購物可能是困擾的。例如買到的產品貨不對辦，甚至被騙，需要退貨。近年出現購物[上癮](../Page/上癮.md "wikilink")，即所謂的[购物狂](../Page/购物狂.md "wikilink")，這些購物者不受控制的購物，可能會引起個人和社會問題。

### 出售者

市場上有不同的出售者，如[供應商](../Page/供應商.md "wikilink")、[零售商和](../Page/零售商.md "wikilink")[推銷員等](../Page/推銷員.md "wikilink")。

### 出售者所使用之科技

為方便客戶更快更準確付款,
商戶都已使用[銷售時點情報系統](../Page/銷售時點情報系統.md "wikilink")（POS）。在北美地區的各大超市，設有全自動收銀系統，完全不經收銀員。[Microsoft
RMS系統成本低](../Page/Microsoft_RMS.md "wikilink")，在北美非常流行。北美地區的POS系統經銷商有[Retail
& Hospitality POS Solutions 零售、飲食系統](http://www.possystem.ca/)、[IBM POS
Systems](http://www-03.ibm.com/products/retail/products/pos/)和[NCR POS
Systems](http://www.ncr.com/)。

## 購物場地

### 購物集中地

[購物中心](../Page/購物中心.md "wikilink")、[廣場](../Page/廣場.md "wikilink")、[跳蚤市場和](../Page/跳蚤市場.md "wikilink")[巴剎集中了大量店舖或零售者](../Page/巴剎.md "wikilink")，售賣不同產品；購物中心通常設在城市中較大規模的商業區內。

### 店舖

店舖可按其出售的貨品分類，如[書店](../Page/書店.md "wikilink")、[精品店](../Page/精品店.md "wikilink")、[藥房](../Page/藥房.md "wikilink")、[寵物品](../Page/寵物品.md "wikilink")、[超級市場等](../Page/超級市場.md "wikilink")。[旅行社則可視為出售服務的商店](../Page/旅行社.md "wikilink")。店舖也可按其目標顧客的[可支配收入](../Page/可支配收入.md "wikilink")（[disposable
income](../Page/:en:disposable_income.md "wikilink")）分類。

有些商店售賣[二手貨](../Page/二手貨.md "wikilink")，部分會接收貨品。有些商店是[非謀利性質的](../Page/非牟利機構.md "wikilink")，部分由接受公眾捐贈的[慈善機構營運](../Page/慈善機構.md "wikilink")。[當舖出售的貨品來自](../Page/當舖.md "wikilink")[抵押貨品的客人](../Page/抵押.md "wikilink")。

不少店舖是[連鎖式經營](../Page/連鎖式.md "wikilink")，有相同的[商標和](../Page/商標.md "wikilink")[品牌](../Page/品牌.md "wikilink")，裝潢和出售的貨品相類似。這些店舖可能由集團擁有，也可能是[特許經營](../Page/特許經營.md "wikilink")，如香港的[7-11便利店](../Page/7-11便利店.md "wikilink")。

### 家中購物

隨着電視、電話和互聯網的普及，消費者可通過[電子商務和](../Page/電子商務.md "wikilink")[B2C系統加上](../Page/B2C.md "wikilink")[郵遞服務](../Page/郵遞.md "wikilink")，足不出戶留在家中購物（home
shopping）；也可以在家中發出購買指令，然後到門市取貨。購物渠道包括[網路購物](../Page/網路購物.md "wikilink")（[購物網站](../Page/購物網站.md "wikilink")）、電話、郵寄等。

### [自動販賣機](../Page/自動販賣機.md "wikilink")

散佈不同地點的售賣機，出售零食、飲料、酒類、香煙及彩票等產品。

## 購物季節

購物高峰期通常出現在[假日前](../Page/假日.md "wikilink")，如[聖誕節](../Page/聖誕節.md "wikilink")、[春節等](../Page/春節.md "wikilink")；和假日後：[美國零售聯合會](../Page/美國零售聯合會.md "wikilink")（[National
Retail
Federation](../Page/:en:National_Retail_Federation.md "wikilink")）則曾指出，長假期後復課期是僅次於假期購物高峰期的時段，消費者會為學童購買衣服、文具和書籍等。\[3\]該會估計，美國人在2006年為學童復課的消費額度達170億美元。\[4\]

## 定價和議價

大部分零售商以成本為基準作定價，即在[成本上加以若干溢價出售貨品](../Page/成本.md "wikilink")。另一種定價方法是「[建議售價](../Page/建議售價.md "wikilink")」（[MSRP](../Page/:en:MSRP.md "wikilink")），貨品根據其上面標示的價格出售。

零售價格通常定為與整數價格相差極少，例如$6.95，形成[心理價格](../Page/心理價格.md "wikilink")（psychological
price 或 odd
price），讓人產生還未到7元的感覺。在中國社會則流行使用幸運號碼作為價格，如$7.8（8與「[發](../Page/發.md "wikilink")」[諧音](../Page/諧音.md "wikilink")），這會形成[價格層級](../Page/價格層級.md "wikilink")（[price
points](../Page/:en:price_points.md "wikilink")）。

通常貨品按其價錢牌的售價出售，不過有店舖會採用[價格歧視的策略](../Page/價格歧視.md "wikilink")，向某些客人收取較高價錢。這時，零售商會就客人的出價能力、對產品的認識程度、購買產品的意欲等因素決定價格。價格歧視會導致[議價](../Page/議價.md "wikilink")，經濟學家認為，通過議價，交易的「剩餘」（surplus）可被分為[消费者和生产者剩余](../Page/消费者和生产者剩余.md "wikilink")。任何一方都沒有明顯優勢，因為交易可能隨時拉倒，於是雙方的剩餘會消失。

## 相關

  - [鳩嗚](../Page/鳩嗚.md "wikilink")

## 參考文獻

[Category:消費者行為](../Category/消費者行為.md "wikilink")
[Category:零售](../Category/零售.md "wikilink")
[Category:商店](../Category/商店.md "wikilink")

1.  饒原生著。《港粵口頭禪趣解》，洪波出版公司，2007年。第241-242頁。ISBN 9789626713983。
2.
3.
4.