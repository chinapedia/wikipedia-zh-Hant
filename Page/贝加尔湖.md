**贝加尔湖**（；,
拉丁转写：，）位於[俄羅斯](../Page/俄羅斯.md "wikilink")[西伯利亚](../Page/西伯利亚.md "wikilink")[伊爾庫茨克州及](../Page/伊爾庫茨克州.md "wikilink")[布里亞特共和國境內](../Page/布里亞特共和國.md "wikilink")，距離[蒙古国邊界](../Page/蒙古国.md "wikilink")111公里，是世界上水容量最大的淡水湖，為[北亚地区不少部族的世居领地](../Page/北亚.md "wikilink")，例如[布里亚特人居住在湖泊東側地區](../Page/布里亚特人.md "wikilink")，於該地豢養[山羊](../Page/山羊.md "wikilink")、[駱駝](../Page/駱駝.md "wikilink")、[牛隻及](../Page/牛.md "wikilink")[綿羊](../Page/綿羊.md "wikilink")\[1\]\[2\]。另外，貝加爾湖有「西伯利亞明珠」之稱\[3\]\[4\]\[5\]\[6\]。

貝加爾湖為[大陸裂谷湖](../Page/裂谷湖.md "wikilink")，有逾1,700種動物及植物棲息其中，而且2/3為[特有種](../Page/特有種.md "wikilink")\[7\]，[聯合國教科文組織於](../Page/聯合國教科文組織.md "wikilink")1996年將貝加爾湖登錄為[世界自然遺產](../Page/世界自然遺產.md "wikilink")\[8\]。

## 地理

[DEM_Baikal_lake.png](https://zh.wikipedia.org/wiki/File:DEM_Baikal_lake.png "fig:DEM_Baikal_lake.png")\]\]
[Yenisei_basin_7.png](https://zh.wikipedia.org/wiki/File:Yenisei_basin_7.png "fig:Yenisei_basin_7.png")流域地圖，並標示[迪克森](../Page/迪克森.md "wikilink")、[杜金卡](../Page/杜金卡.md "wikilink")、[图鲁汉斯克](../Page/图鲁汉斯克.md "wikilink")、[克拉斯諾亞爾斯克及](../Page/克拉斯諾亞爾斯克.md "wikilink")[伊爾庫茨克等城市](../Page/伊爾庫茨克.md "wikilink")\]\]

### 水文

貝加爾湖呈新月形，湖泊位於[盆地地形中](../Page/盆地.md "wikilink")，周圍有[山脈及](../Page/山脈.md "wikilink")[丘陵環繞](../Page/丘陵.md "wikilink")\[9\]，湖泊南北长680公里，东西宽40至50公里（最宽处达80公里）\[10\]，面積31,494平方公里，是[亞洲第一大](../Page/亞洲.md "wikilink")[淡水湖](../Page/淡水湖.md "wikilink")，也是世界第七大[湖](../Page/湖.md "wikilink")，屬於斷層湖，沿岸[地震頻繁](../Page/地震.md "wikilink")、多[溫泉](../Page/溫泉.md "wikilink")，流域面積560,000平方公里，有多達336條河流注入\[11\]，其中最大的河為[色楞格河](../Page/色楞格河.md "wikilink")。而其外流河為[葉尼塞河的支流](../Page/葉尼塞河.md "wikilink")[安加拉河](../Page/安加拉河.md "wikilink")，出水口位於湖西南側，往北流入[北極海](../Page/北極海.md "wikilink")，另外在湖的西側尚有另一條大河[勒拿河的源頭](../Page/勒拿河.md "wikilink")，距湖僅7公里，不過被高達1640公尺的貝加爾山脈中部所阻隔，湖中有22島，最大的[奧爾洪島長達](../Page/奧爾洪島.md "wikilink")72公里，湖面每年1月至5月結冰\[12\]。

貝加爾湖是世界最深的湖泊，最大水深为1634米至1741米，平均深度758米，為世界上平均深度最深的湖泊\[13\]，湖面[海拔](../Page/海拔.md "wikilink")456米，最深處湖床海拔-1181米\[14\]。因深度深，其體積達23,600立方公里，佔全球流动的河川、淡水湖總水量的17%\[15\]至20%\[16\]\[17\]，比整個[北美洲](../Page/北美洲.md "wikilink")[五大湖](../Page/五大湖.md "wikilink")（22,560立方公里）或[北歐](../Page/北歐.md "wikilink")[波羅的海](../Page/波羅的海.md "wikilink")（21,000立方公里）的水量還多，但只有[裏海](../Page/裏海.md "wikilink")
（78,200立方公里）的三分之一。貝加爾湖也是世界最古老的湖泊，據研究，它已經在地球上存在超過2500萬年\[18\]，同時貝加爾湖也是世界上最清澈的湖泊之一\[19\]。

### 氣候

貝加爾湖位於歐亞大陸內陸，屬[柯本气候分类法的](../Page/柯本气候分类法.md "wikilink")[大陸性氣候](../Page/大陸性氣候.md "wikilink")\[20\]，湖面1月至5月結冰\[21\]，冰層厚度約70至115公分，冬季時，可於湖面行駛汽車至奧爾洪島\[22\]。貝加爾湖東側地區冬季最低溫可達，夏季最高溫則可達\[23\]。

### 生物

貝加爾湖生物多樣性相當豐富，共有1,085種植物及1,550種動物以之為棲息地，其中80%為特有種\[24\]，並有許多世界僅有的淡水特有種生物，被稱為「俄羅斯的[加拉巴哥群島](../Page/加拉巴哥群島.md "wikilink")」；其中[貝加爾海豹與](../Page/貝加爾海豹.md "wikilink")[貝加爾鱘為世界上唯一的淡水](../Page/貝加爾鱘.md "wikilink")[海豹和](../Page/海豹.md "wikilink")[鱘](../Page/鱘.md "wikilink")，而貝加爾海豹的棲息範圍幾乎遍布整個貝加爾湖\[25\]。

## 名稱

[汉朝人称之为](../Page/汉朝.md "wikilink")「**翰海**\[26\]」，五胡十六国時北朝叫「**于巳尼大水**」，隋唐叫「**小海**」，18世纪初期的《[异域录](../Page/异域录.md "wikilink")》称之为“**柏海儿湖**”，《[大清一统志](../Page/大清一统志.md "wikilink")》称为「白哈儿湖」，[蒙古称为](../Page/蒙古.md "wikilink")「**达赖诺尔**」，意为“海一样的湖”、“自然之湖”\[27\]；一说名称来源于“贝音嘎　嘎拉”（蒙古语意为不灭的火焰）\[28\]
，17世纪20年代的[沙皇俄国探索者亦称之为](../Page/沙皇俄国.md "wikilink")“圣海”。

## 歷史

古中國史籍稱貝加爾湖為**于巳尼大水**、**小海**、**大泽**或**北海**，先后自前3世纪起先后属于[匈奴所属的丁零部落领地](../Page/匈奴.md "wikilink")、[鲜卑](../Page/鲜卑.md "wikilink")+[乌桓所属的丁零和坚昆部落的领地](../Page/乌桓.md "wikilink")、[柔然](../Page/柔然.md "wikilink")、[突厥所属的](../Page/突厥.md "wikilink")[铁勒部落领地](../Page/铁勒.md "wikilink")、[安北都护府](../Page/安北都护府.md "wikilink")、[回鹘](../Page/回鹘.md "wikilink")、[黠嘎斯](../Page/黠嘎斯.md "wikilink")+[阻卜](../Page/阻卜.md "wikilink")、[辽朝](../Page/辽朝.md "wikilink")（契丹）、[尼伦和](../Page/尼伦.md "wikilink")[迭列斤两大部落组成的](../Page/迭列斤.md "wikilink")[蒙兀国人的活動範圍](../Page/蒙兀国.md "wikilink")。著名的[蘇武牧羊故事就發生在貝加爾湖一帶](../Page/蘇武.md "wikilink")。這些遊牧部民長期存在，有時跟南部的中國發生戰爭（比如[汉匈战争](../Page/汉匈战争.md "wikilink")、[北魏与柔然的戰爭](../Page/北魏.md "wikilink")），有時臣服於中國（如隋唐的全盛時期）。

13世纪起，這裡成為[成吉思汗建立的](../Page/成吉思汗.md "wikilink")[蒙古帝国的领土](../Page/蒙古帝国.md "wikilink")。

14世纪晚期的1388年，明军在[捕鱼儿海](../Page/捕鱼儿海.md "wikilink")（捕鱼儿海又名贝尔湖，不是貝加爾湖）之战中打败[北元军队](../Page/北元.md "wikilink")，随即北元后主[脱古思帖木儿被](../Page/脱古思帖木儿.md "wikilink")[也速迭儿刺杀](../Page/也速迭儿.md "wikilink")，之后裂解为[鞑靼和](../Page/鞑靼.md "wikilink")[瓦剌](../Page/瓦剌.md "wikilink")；貝加爾湖直到1620年代之前都属于[瓦剌的领土](../Page/瓦剌.md "wikilink")。

[沙皇俄国的探索队於](../Page/俄罗斯沙皇国.md "wikilink")1628年扩张貝加爾湖一帶，為俄国征服西伯利亞的一部份\[29\]，而[庫爾巴特·伊凡諾夫則於](../Page/庫爾巴特·伊凡諾夫.md "wikilink")1643年帶領第一支到達貝加爾湖的探險隊\[30\]。

清康熙三十六年（1697年）和乾隆二十二年（1757年），[喀爾喀蒙古和](../Page/喀爾喀蒙古.md "wikilink")[準噶爾汗国先後被](../Page/準噶爾汗国.md "wikilink")[清朝征服](../Page/清朝.md "wikilink")。康熙二十八年（1689年）跟俄国簽訂的《[尼布楚條約](../Page/尼布楚條約.md "wikilink")》中，曾屬於蒙古的貝加爾湖以東地區被康熙皇帝最終承認属于俄國；清雍正五年（1727年）的《[布連斯奇條約](../Page/布連斯奇條約.md "wikilink")》和《[恰克圖條約](../Page/恰克圖條約.md "wikilink")》，亦划定貝加爾湖地区归[俄羅斯帝國所有](../Page/俄羅斯帝國.md "wikilink")。1908年6月30日，在湖西北方800公里處發生了[通古斯大爆炸](../Page/通古斯大爆炸.md "wikilink")，部分影響了湖附近的森林。

1919年冬，[俄国内战期间](../Page/俄国内战.md "wikilink")，白军領導人[高爾察克從](../Page/亞歷山大·瓦西里耶維奇·高爾察克.md "wikilink")[鄂木斯克與](../Page/鄂木斯克.md "wikilink")[托木斯克撤退](../Page/托木斯克.md "wikilink")，在接近[伊爾庫茨克的貝加爾湖畔受阻](../Page/伊爾庫茨克.md "wikilink")，高爾察克在1920年1月15日被駐守[西伯利亞鐵路的](../Page/西伯利亞鐵路.md "wikilink")[捷克斯洛伐克軍團交給伊爾庫茨克的](../Page/捷克斯洛伐克軍團.md "wikilink")[布爾什維克](../Page/布爾什維克.md "wikilink")。指揮官[卡普佩爾將軍為營救高爾察克](../Page/弗拉基米爾·奧斯卡洛維奇·卡普佩爾.md "wikilink")，從[克拉斯諾亞爾斯克頂著零下](../Page/克拉斯諾亞爾斯克.md "wikilink")50度低溫[向東行軍1000公里](../Page/西伯利亞冰雪大行軍.md "wikilink")，於1月26日在伊爾庫茨克近郊因[凍瘡與](../Page/凍瘡.md "wikilink")[肺炎病逝](../Page/肺炎.md "wikilink")，高爾察克也在2月7日被[布爾什維克處決](../Page/布爾什維克.md "wikilink")。在後有追兵的情況下，剩餘的軍人、眷屬、傷患約3萬人在1920年2月在零下40度暴風雪、缺乏適當衣物、糧食補給、絕望疲憊的情況下強行穿越結冰的貝加爾湖，許多人被凍死於湖面。他們的遺體與遺物在整個冬天維持冰封的狀態，直到湖冰溶解，沉入上千米深的湖底。有傳聞指出，高爾察克殘部曾獲得[沙皇政府的黃金儲備](../Page/沙皇.md "wikilink")，但在逃亡途中掉入貝加爾湖。近年有人宣稱可能發現落入湖中的黃金\[31\]\[32\]。

## 圖集

<File:Baikal> ol'chon ri südosten.jpg|湖的東南部
[File:Baikal-S1998162044804.jpg|貝加爾湖衛星圖](File:Baikal-S1998162044804.jpg%7C貝加爾湖衛星圖)
<File:Olchon> prom.jpg|湖中最大的奧爾洪島一景
<File:ferroviacircolarebaikal.jpg>|[西伯利亞鐵路經過湖南岸](../Page/西伯利亞鐵路.md "wikilink")
[File:26_swiatoinos.jpg|從貝加爾湖的湖浪可和常见的海浪相比](File:26_swiatoinos.jpg%7C從貝加爾湖的湖浪可和常见的海浪相比)
[File:Bajkal_zima1.jpg|結冰的湖面](File:Bajkal_zima1.jpg%7C結冰的湖面)

## 注释

## 参考文献

## 外部連結

  - [Lake Baikal Information](http://www.lakebaikal.org)
  - [Baikal Club International (magazine about Lake Baikal, maps,
    photos, videos and stories)](http://baikalclub.com)

## 参见

  - [湖](../Page/湖.md "wikilink")：[構造湖](../Page/構造湖.md "wikilink")
  - [捕鱼儿海](../Page/捕鱼儿海.md "wikilink")
  - [西伯利亚](../Page/西伯利亚.md "wikilink")

{{-}}

[Category:俄罗斯湖泊](../Category/俄罗斯湖泊.md "wikilink")
[贝加尔湖](../Category/贝加尔湖.md "wikilink")
[Category:湖泊之最](../Category/湖泊之最.md "wikilink")
[Category:俄罗斯地理的世界之最](../Category/俄罗斯地理的世界之最.md "wikilink")
[Category:俄羅斯世界遺產](../Category/俄羅斯世界遺產.md "wikilink")
[Category:俄羅斯國家公園](../Category/俄羅斯國家公園.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.
13.

14.
15.

16.

17.

18. [Fact Sheet: Lake Baikal — A Touchstone for Global Change and Rift
    Studies](http://marine.usgs.gov/fact-sheets/baikal/) , July 1993
    (accessed December 04, 2007)

19.

20.
21.

22.

23.

24. [Зоопланктон в экосистеме озера Байкал / О Байкале.ру — Байкал.
    Научно и популярно](http://baikal.mobi/)

25. Peter Saundry. 2010. [*Baikal
    seal*](http://www.eoearth.org/wiki/Baikal_seal). Encyclopedia of
    Earth. Topic ed. C. Michael Hogan, Ed. in chief C. NCSE, Washington
    DC

26. 「票騎之出代二千餘里，與左王接戰，漢兵得胡首虜凡七萬餘人，左王將皆遁走。票騎封於狼居胥山，禪姑衍，臨**翰海**而還。」

27. Altangerel Damdinsuren, English to Mongolian Dictionary (1998)
    *Silverland: A Winter Journey Beyond the Urals*, London, John
    Murray, page 173

28. [《苍狼大地》　铁穆尔](http://www.cnxbsww.com/ShanwShow.asp?AcrriID=214&WXClassID=20)


29. George V. Lantzeff and Richard A. Price, 'Eastward to Empire', 1973.

30.

31. [貝加爾湖底
    傳發現沙皇千億黃金](http://www.libertytimes.com.tw/2010/new/sep/2/today-int3.htm)


32. [貝加爾湖驚現沙俄寶藏 1600噸黃金或重見天日](http://tech.ifeng.com/discovery/detail_2010_09/02/2418002_0.shtml)