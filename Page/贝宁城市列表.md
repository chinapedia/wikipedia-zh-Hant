[贝宁城市列表如下](../Page/贝宁.md "wikilink")，其中粗体为[首都](../Page/首都.md "wikilink")。

  - [阿波美](../Page/阿波美.md "wikilink")（）
  - [阿波美卡拉维](../Page/阿波美卡拉维.md "wikilink")（）
  - [阿拉达](../Page/阿拉达.md "wikilink")（）
  - [阿普拉惠](../Page/阿普拉惠.md "wikilink")（）
  - [阿蒂梅埃](../Page/阿蒂梅埃.md "wikilink")（）
  - [巴尼夸拉](../Page/巴尼夸拉.md "wikilink")（）
  - [巴西拉](../Page/巴西拉.md "wikilink")（）
  - [本贝雷凯](../Page/本贝雷凯.md "wikilink")（）
  - （）
  - [博希孔](../Page/博希孔.md "wikilink")（）
  - [博里](../Page/博里.md "wikilink")（）
  - （）
  - [科美](../Page/科美.md "wikilink")（）
  - [科托努](../Page/科托努.md "wikilink")（）
  - [科韦](../Page/科韦.md "wikilink")（）
  - [达萨祖梅](../Page/达萨祖梅.md "wikilink")（）
  - [朱古](../Page/朱古.md "wikilink")（）
  - （）
  - [冈维埃](../Page/冈维埃.md "wikilink")（）
  - （）
  - [康迪](../Page/康迪.md "wikilink")（）
  - [凯鲁](../Page/凯鲁.md "wikilink")（）
  - [凯图](../Page/凯图.md "wikilink")（）
  - [宽代](../Page/宽代.md "wikilink")（）
  - [洛科萨](../Page/洛科萨.md "wikilink")（）
  - [马朗维尔](../Page/马朗维尔.md "wikilink")（）
  - [纳蒂廷古](../Page/纳蒂廷古.md "wikilink")（）
  - [恩达利](../Page/恩达利.md "wikilink")（）
  - [尼基](../Page/尼基.md "wikilink")（）
  - [维达](../Page/维达_\(贝宁\).md "wikilink")（）
  - [帕拉库](../Page/帕拉库.md "wikilink")（）
  - （）
  - [波贝](../Page/波贝.md "wikilink")（）
  - （）
  - **[波多诺伏](../Page/波多诺伏.md "wikilink")**（）
  - [萨凯泰](../Page/萨凯泰.md "wikilink")（）
  - [萨瓦卢](../Page/萨瓦卢.md "wikilink")（）
  - [萨韦](../Page/萨韦.md "wikilink")（）
  - [塞巴纳](../Page/塞巴纳.md "wikilink")（）
  - [唐吉埃塔](../Page/唐吉埃塔.md "wikilink")（）
  - [乔鲁](../Page/乔鲁.md "wikilink")（）

## 十大城市

1.  [科托努](../Page/科托努.md "wikilink")（） - 818,100人
2.  [波多诺伏](../Page/波多诺伏.md "wikilink")（） - 234,300人
3.  [帕拉库](../Page/帕拉库.md "wikilink")（） - 227,900人
4.  [朱古](../Page/朱古.md "wikilink")（） - 206,500人
5.  [博希孔](../Page/博希孔.md "wikilink")（） - 164,700人
6.  [康迪](../Page/康迪.md "wikilink")（） - 149,900人
7.  [阿波美](../Page/阿波美.md "wikilink")（） - 126,800人
8.  [纳蒂廷古](../Page/纳蒂廷古.md "wikilink")（） - 119,900人
9.  [洛科萨](../Page/洛科萨.md "wikilink")（） - 111,000人
10. [维达](../Page/维达_\(贝宁\).md "wikilink")（） - 97,000人

## 参看

  - [各国城市列表](../Page/各国城市列表.md "wikilink")

[\*](../Category/贝宁城市.md "wikilink")
[Bei](../Category/非洲城市列表.md "wikilink")