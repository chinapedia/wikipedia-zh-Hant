中文譯名為**倍有福**，是位于[美国西北](../Page/美国.md "wikilink")[华盛顿州](../Page/华盛顿州.md "wikilink")[金县的一座城市](../Page/金县_\(华盛顿州\).md "wikilink")，与[西雅图隔](../Page/西雅图.md "wikilink")[华盛顿湖相望](../Page/华盛顿湖.md "wikilink")，是华州第五大城（按人口计）。舊譯名為**貝爾維尤**（），自2015年起，當地華人教會的**王克強**牧師提倡美國各城市的華文名稱翻譯，盡量用更美、更有中國文化意義的字詞來取代，故推動譯為**倍有福**，中間也有提倡譯**貝勒府**之建議，但多數華人覺得這個譯名太過霸氣，也可能與府、宅等住家名詞產生混淆，戲稱之可以，但若是美國城市的華文譯名，則不夠莊重，故最終定為**倍有福**，優雅並且帶有中國人最喜歡的**福氣**。

倍市名源于法语，意為“大美之境”。2010年全美人口普查，貝市人口122,363人，[亞裔占兩成以上](../Page/亞裔美國人.md "wikilink")，有不少華人與韓國人定居於此。倍市現任市長為John
Stokes。

## 教育

## 地理

北纬47°35′51″，西经122°9′33″。西临[华盛顿湖](../Page/华盛顿湖.md "wikilink")，东临Sammamish湖，面积87.8平方公里，其中水面8.2平方公里。

主要高速公路有I-90、I-405、SR-520。两座跨越[华盛顿湖连接](../Page/华盛顿湖.md "wikilink")[西雅图的浮桥先后建成于](../Page/西雅图.md "wikilink")1940和1963年。

## 人口

倍有福 (华盛顿州) 的居民非常多样化。根据2006年的统计数据
\[1\]，該市人口119,678，其中三成生於美國以外地區，22%为亚裔，[家户平均收入为](../Page/:en:Median_household_income.md "wikilink")$76,757美元。於2006年
CNN
的一项調查报告中\[2\]，根据罪案与人口的比值，贝拉维尤被评为全美国最安全的城市之一（另包括華人聚居的南加州[爾灣市](../Page/爾灣市.md "wikilink")、[核桃市等](../Page/核桃市.md "wikilink")）。

## 经济

市区内高楼林立，工商业发达。主要的购物中心有：
\* Bellevue Square

  - Bellevue Place
  - Lincoln Square
  - Bellevue Galleria
  - Factoria Mall
  - Crossroads Mall
  - Overlake Shopping District
  - The Shops At Bravern

主要的公司有：
\* [Expedia](../Page/Expedia公司.md "wikilink") – 在线旅游公司

  - InfoSpace - A growing Internet private-label search engine and
    online directory that survived the dot.com bust of the 1990s
  - [微软](../Page/微软.md "wikilink") - 在Lincoln
    Square租有15层楼作为其美洲销售总部，在City
    Center和Bravern都有在线服务研发办公室。
  - QFC - Quality Food Centers, a Washington and Oregon chain of upscale
    grocery stores.
  - [T-Mobile](../Page/T-Mobile.md "wikilink") – 其美国总部位于该市的Factoria区
  - [Valve](../Page/维尔福软件公司.md "wikilink") –
    著名电子游戏开发公司，产品有《[半条命](../Page/半条命.md "wikilink")》、《[反恐精英](../Page/反恐精英.md "wikilink")》

## 友好城市

  - [平顶山市](../Page/平顶山市.md "wikilink")

  - [八尾市](../Page/八尾市.md "wikilink")

  - [克拉德诺](../Page/克拉德诺.md "wikilink")

  - [利耶帕亚](../Page/利耶帕亚.md "wikilink")

  - [花蓮市](../Page/花蓮市.md "wikilink")

  - [汕尾市](../Page/汕尾市.md "wikilink")

## 參考文獻

[B](../Category/华盛顿州城市.md "wikilink")

1.  [1](http://factfinder.census.gov)
2.  [2](http://money.cnn.com/2006/10/30/real_estate/Most_dangerous_cities/index.htm)