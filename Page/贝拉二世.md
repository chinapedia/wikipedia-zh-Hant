**贝拉二世（瞎子）**（[匈牙利语](../Page/匈牙利语.md "wikilink")：**II. (Vak)
Béla**，1108年\~1141年2月13日）[阿尔帕德王朝的](../Page/阿尔帕德王朝.md "wikilink")[匈牙利国王](../Page/匈牙利.md "wikilink")（1131年\~1141年在位）。

贝拉二世是匈牙利亲王[阿爾莫什之子](../Page/阿爾莫什_\(匈牙利\).md "wikilink")。其父发动了一场不成功的政变企图推翻国王“书虫”[卡爾曼](../Page/卡爾曼_\(匈牙利\).md "wikilink")（即贝拉二世的叔叔），结果失败；贝拉与父亲都被卡爾曼弄瞎了眼睛（此事大约发生在1112年）。贝拉二世逃亡到[拜占庭帝国首都](../Page/拜占庭帝国.md "wikilink")[君士坦丁堡避难](../Page/君士坦丁堡.md "wikilink")，后来被卡爾曼的儿子[伊什特萬二世召回国并在](../Page/伊什特萬二世.md "wikilink")1131年继承了王位。

贝拉二世的对外政策十分活跃：他与[巴本堡王朝的](../Page/巴本堡王朝.md "wikilink")[奥地利公爵](../Page/奥地利.md "wikilink")[利奥波德三世以及](../Page/利奥波德三世_\(奥地利\).md "wikilink")[波希米亚国王](../Page/波希米亚.md "wikilink")[索别斯拉夫一世都进行了联姻](../Page/索别斯拉夫一世.md "wikilink")。1133年，他从[威尼斯手中夺取了](../Page/威尼斯.md "wikilink")[达尔马提亚的部分领土](../Page/达尔马提亚.md "wikilink")；1136年他又迫使[波斯尼亚的王公臣服于他的权威](../Page/波斯尼亚.md "wikilink")。

贝拉二世在其整个统治期间都在与科罗曼的一个私生子鲍里斯进行斗争，后者受到[波兰的支持](../Page/波兰.md "wikilink")。1132年，他击退了由波兰大公[波列斯瓦夫三世领导的一次企图将鲍里斯推上王位的入侵](../Page/波列斯瓦夫三世.md "wikilink")。

1139年，贝拉二世干预[罗斯王公内讧](../Page/罗斯.md "wikilink")，他派遣3万军队去支援[基辅大公](../Page/基辅.md "wikilink")[亚罗波尔克二世·弗拉基米罗维奇反对](../Page/亚罗波尔克二世·弗拉基米罗维奇.md "wikilink")[切尔尼戈夫王公](../Page/切尔尼戈夫.md "wikilink")[弗谢沃洛德·奥利戈维奇](../Page/弗谢沃洛德二世·奥利戈维奇.md "wikilink")，但最终未能阻止弗谢沃洛德夺取[基辅大公的公位](../Page/基辅大公.md "wikilink")。

贝拉二世死于1141年，死因大概是过度[酗酒](../Page/酗酒.md "wikilink")。

## 家庭

<center>

</center>

  - 妻子：[塞尔维亚公主伊洛娜](../Page/塞尔维亚.md "wikilink")，1127年结婚
  - 子女：

<!-- end list -->

1.  [盖萨二世](../Page/盖萨二世.md "wikilink")
2.  [拉斯洛二世](../Page/拉斯洛二世.md "wikilink")
3.  [斯蒂芬四世](../Page/斯蒂芬四世_\(匈牙利\).md "wikilink")
4.  阿尔莫什
5.  索菲娅
6.  埃尔瑟贝特

## 參考

## 参考文献

  - 苏联大百科全书·人物卷，1966
  - Engel, Pat. *Realm of St. Stephen : A History of Medieval Hungary*,
    2001

[B](../Category/匈牙利国王.md "wikilink")
[B](../Category/1141年逝世.md "wikilink")
[Category:盲人王室和贵族](../Category/盲人王室和贵族.md "wikilink")