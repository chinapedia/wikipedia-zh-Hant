**负数**，在[数学上指](../Page/数学.md "wikilink")[小于](../Page/小于.md "wikilink")[0的](../Page/0.md "wikilink")[实数](../Page/实数.md "wikilink")，如−2、−3.2、−807.5等，与[正数相对](../Page/正数.md "wikilink")。和[实数一样](../Page/实数.md "wikilink")，[负數也是一個](../Page/负數.md "wikilink")[不可數的](../Page/不可數.md "wikilink")[無限集合](../Page/無限集合.md "wikilink")。這個[集合在](../Page/集合.md "wikilink")[数学上通常用](../Page/数学.md "wikilink")[粗體](../Page/粗體.md "wikilink")**R<sup>−</sup>**或\(\mathbb{R}^-\)来表示。负数与[0统称非正数](../Page/0.md "wikilink")。

## 负数的历史

[负整数可以被认为是](../Page/负整数.md "wikilink")[自然数的扩展](../Page/自然数.md "wikilink")，使得等式\(x-y=z\)对任意\(x\)和\(y\)都有意义。相对而言，其他数的集合都是从自然数通过逐步扩展得到的。

负数在表示小于 0
的值的时候非常有用。例如，在[会计学上](../Page/会计学.md "wikilink")，它可以被用来表示[負債](../Page/债务.md "wikilink")，而且通常以紅色表示（若不帶負數符號則加上括號），所以又稱「赤字」。

自从公元前4世纪，中国数学家就已经了解負數和零的概念了。\[1\]
公元1世纪的《[九章算術](../Page/九章算術.md "wikilink")》说“正負術曰：同名相除，異名相益，正無入負之，負無入正之。其異名相除，同名相益，正無入正之，負無入負之。”（這段話的大意是“减法：遇到同符号数字应相减其数值，遇到异符号数字应相加其数值，零减正数的差是負數，零减負數的差是正数。”）以上文字里的“無入”通常被数学历史家认为是零的概念。（全文见维基文库的《[九章算術](https://zh.wikisource.org/wiki/%E4%B9%9D%E7%AB%A0%E7%AE%97%E8%A1%93)》）

尽管中国古人首先发现并应用了负数，但却并没有从理性方面讨论负数存在的意义和本质，这可能是文化习惯导致的。对负数精确的定义，和其根本属性的讨论，是由近代西方数学家首先完成的。\[2\]

西方最早在数学上使用负数的是一本印度数学文献，[Brahmagupta写于](../Page/Brahmagupta.md "wikilink")628年的
*BrahmaSphuta-Sidd'hanta*。它的出现是为了表示负资产或债务。在很大程度上，欧洲数学家直到17世纪才接受负数的概念。

## 符号函数

在实数上可以定义这样一个函数\(\sgn (x)\)，它对正数取值为 1，负数取值为 −1，0 取值为
0。这个函数通常被称为[符号函数](../Page/符号函数.md "wikilink")：

\[\sgn(x)=\left\{\begin{matrix} -1 & : x < 0 \\ \;0 & : x = 0 \\ \;1 & : x > 0 \end{matrix}\right.\]

当\(x\)不为 0 时，则有：

\[\sgn(x) = \frac{x}{|x|} = \frac{|x|}{x} = \frac{d{|x|}}{d{x}} = 2H(x)-1.\]

这里，\(\left \vert x \right \vert\)为\(x\)的[绝对值](../Page/绝对值.md "wikilink")，\(H(x)\)为[单位阶跃函数](../Page/单位阶跃函数.md "wikilink")。请参见[导数](../Page/导数.md "wikilink")。

## 负数的四則運算

|                                                                                                                        |                                                                                                                                            |
| ---------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| 口訣                                                                                                                     | 釋義                                                                                                                                         |
| 加法                                                                                                                     | 減法                                                                                                                                         |
| 被乘數                                                                                                                    | 乘數                                                                                                                                         |
| <span style="color: red;">**正**</span><span style="color: green;">**正**</span>得<span style="color: blue;">**正**</span> | *a* <span style="color: red;">**+**</span> (<span style="color: green;">**+**</span>*b*) = *a* <span style="color: blue;">**+**</span> *b* |
| <span style="color: red;">**正**</span><span style="color: green;">**負**</span>得<span style="color: blue;">**負**</span> | *a* <span style="color: red;">**+**</span> (<span style="color: green;">**−**</span>*b*) = *a* <span style="color: blue;">**−**</span> *b* |
| <span style="color: red;">**負**</span><span style="color: green;">**正**</span>得<span style="color: blue;">**負**</span> | －                                                                                                                                          |
| <span style="color: red;">**負**</span><span style="color: green;">**負**</span>得<span style="color: blue;">**正**</span> | －                                                                                                                                          |

負數四則運算口訣

|        |        |
| ------ | ------ |
| 兩個符號一樣 | 兩個符號不同 |
| 得正     | 得負     |

負數四則運算口訣簡單版

-----

### 加法

[加上一个负数相当于](../Page/加.md "wikilink")[减去其](../Page/减.md "wikilink")[相反數](../Page/相反數.md "wikilink")：

\[6 + ({\color{Violet}-3}) = 6 - {\color{Orange}3} = 3 \,\]

\[-2 + ({\color{Violet}-5}) = -2 - {\color{Orange}5} = -7 \,\]

### 减法

一个较大的正数减去一个较小的正数将得到一个正数

一个较小的正数减去一个较大的正数将得到一个负数：

\[6 - 3 = 3 \,\]

\[4 - 6 = -2 \,\]

\[0 - 5 = -5 \,\]

任意负数减去一个正数总得到一个负数：

\[-6 - 3 = - (6 + 3) = -9 \,\]

减去一个负数相当于加上相应的正数：

\[5 - (-2) = 5 + 2 = 7 \,\]

\[(-6) - (-3) = -(6 - 3) = -3 \,\]

### 乘法

一个负数和一个正数相[乘得到一个负数](../Page/乘.md "wikilink")：\((-2)\times 3=-6\)。这里，乘法可以被看作是多次加法的重复：\((-2)\times 3=(-2)+(-2)+(-2)=-6\)。

两个负数相乘得到一个正数：\((-3)\times(-4)=12\)。这里，乘法不能再被看作是多次加法的重复了，而是为了使乘法满足[分配律](../Page/分配律.md "wikilink")：

\[\bigg[3 + (-3)\bigg] \times (-4) = 3 \times (-4) + (-3) \times (-4). \,\]

等式的左边为\(0\times(-4)=0\)。等式的右边为\(-12+(-3)\times(-4)\)。为了使两边相等，必须要\((-3)\times(-4)=12\)。

### 除法

[除法和乘法类似](../Page/除法.md "wikilink")。若[被除数和](../Page/被除数.md "wikilink")[除数有不同的符号](../Page/除数.md "wikilink")，结果是一个负数：

\[\; 8 \;\div\; (-2) = (-4) \,\]

\[(-10) \;\div\; 2 = (-5) \,\]

若被除数和除数有相同的符号（就算他们均为负），结果是一个正数：

\[(-6) \;\div\; (-3) = 6 \;\div\;3=2 \,\]

## 参见

  - [实数](../Page/实数.md "wikilink")
  - [超实数](../Page/超实数.md "wikilink")
  - [負整數](../Page/負整數.md "wikilink")

## 參考資料及註釋

<references />

[Category:實數](../Category/實數.md "wikilink")
[Category:算术](../Category/算术.md "wikilink")

1.
2.  [HPM通訊第二期](http://math.ntnu.edu.tw/~horng/letter/vol1no2c.htm)