**贡榜王朝**（，英語：Konbaung
Dynasty）為[緬甸最後的王朝](../Page/緬甸.md "wikilink")，為[雍笈牙創立](../Page/雍笈牙.md "wikilink")，因此也稱為雍笈牙王朝。貢榜王朝存在於1752年到1885年，期間不但統一全緬甸，也对外四处用兵擴張疆土，於1758年在右擊敗[暹羅](../Page/暹羅.md "wikilink")[大城王朝及](../Page/大城王朝.md "wikilink")[中南半島各國](../Page/中南半島.md "wikilink")，曾雄霸[東南亞](../Page/東南亞.md "wikilink")，並多次和[清國发生战争](../Page/清國.md "wikilink")。19世紀中期，南緬甸受到[大英帝国入侵](../Page/大英帝国.md "wikilink")，該王朝前後與英爆發三次[英緬戰爭](../Page/英緬戰爭.md "wikilink")，終於在1885年遭受英國全面佔領，貢榜王朝亦覆滅。其領土則被併入[英屬印度](../Page/英屬印度.md "wikilink")。

## 君主列表

<table>
<caption><strong>貢榜王朝國王</strong></caption>
<thead>
<tr class="header">
<th><p>中文譯名</p></th>
<th><p>緬甸語名字</p></th>
<th><p>關係</p></th>
<th><p>在位期間<br />
（公元）</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/雍笈牙.md" title="wikilink">雍笈牙</a></p></td>
<td></td>
<td></td>
<td><p>1752-1760</p></td>
<td><p>用七年時間統一緬甸。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/囊陀基.md" title="wikilink">囊陀基</a></p></td>
<td></td>
<td></td>
<td><p>1760-1763</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/辛標信.md" title="wikilink">辛標信</a><br />
又譯猛白、孟駁</p></td>
<td></td>
<td></td>
<td><p>1763-1776</p></td>
<td><p>多次侵犯<a href="../Page/雲南.md" title="wikilink">雲南邊境</a>，引發<a href="../Page/清緬戰爭.md" title="wikilink">清緬戰爭</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/辛古王.md" title="wikilink">辛古王</a></p></td>
<td></td>
<td></td>
<td><p>1776-1781</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莽莽.md" title="wikilink">莽莽</a></p></td>
<td></td>
<td></td>
<td><p>1781</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波道帕耶.md" title="wikilink">波道帕耶</a><br />
又譯孟雲</p></td>
<td></td>
<td><p>雍笈牙四子</p></td>
<td><p>1782-1819</p></td>
<td><p>較注意發展國內經濟。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴基道.md" title="wikilink">巴基道</a><br />
又譯孟既</p></td>
<td></td>
<td></td>
<td><p>1819-1837</p></td>
<td><p>被迫與<a href="../Page/英國.md" title="wikilink">英國簽訂</a>《<a href="../Page/揚達波條約.md" title="wikilink">揚達波條約</a>》<br />
（Treaty of Yandabo）。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沙耶瓦底王.md" title="wikilink">沙耶瓦底王</a><br />
又譯孟坑</p></td>
<td></td>
<td></td>
<td><p>1837-1846</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蒲甘王.md" title="wikilink">蒲甘王</a></p></td>
<td></td>
<td></td>
<td><p>1846-1853</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/敏東.md" title="wikilink">敏東</a></p></td>
<td></td>
<td></td>
<td><p>1853-1879</p></td>
<td><p>推行改革。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/錫袍.md" title="wikilink">錫袍</a></p></td>
<td></td>
<td><p>子</p></td>
<td><p>1878-1885</p></td>
<td><p>被英軍俘虜，貢榜王朝滅亡。</p></td>
</tr>
</tbody>
</table>

## 皇家儀式

有14种皇家奉獻仪式（abhiseka）：

  - Rajabhiseka（ရာဇဘိသိက်） - 国王加冕
  - Muddhabhiseka（မုဒ္ဓဘိသိက်） - 正式由国王誓言为Sasana（佛陀的教诲）的传播工作;入世后举行5年
  - Uparajabhiseka（ဥပရာဇဘိသေက） - 安装王储
  - Mahesibhiseka（မဟေသီဘိသေက） - 王后加冕
  - Mangalabhiseka（မင်္ဂလာဘိသေက） - 庆祝白象藏
  - Siriyabhiseka（သီရိယဘိသေက） - 续约国王的荣耀，召开之际
  - Ayudigabhiseka（အာယုဒီဃဘိသေက） - 获得长寿，召开之际
  - Jeyyabhiseka（ဇေယျာဘိသေက） - 以确保战争的胜利和成功
  - Mahabhiseka（မဟာဘိသေက） - 以增加经济繁荣，入世举办七年之后
  - Sakalabhiseka（သကလာဘိသေက） - 以确保和平的国度
  - Vijayabhiseka（ဝိဇယဘိသေက） - 征服敌人
  - Mandabhiseka（Manda beittheit） - 候选嫁给王室血统的王后
  - Singabhiseka（Thenga beittheit） - 召开再犯国王由有法可依，届时该国的政府和行政授权礼

## 參閱條目

  - [東籲王朝](../Page/東籲王朝.md "wikilink")
  - [緬甸歷史](../Page/緬甸歷史.md "wikilink")
  - [清緬戰爭](../Page/清緬戰爭.md "wikilink")
  - [英緬戰爭](../Page/英緬戰爭.md "wikilink")

## 參考

  - 《緬甸－佛光普照的稻米之國》香港城市大學出版社 ISBN 962-937-101-4

## 脚注

[Category:緬甸歷史政權](../Category/緬甸歷史政權.md "wikilink")
[Category:清朝屬國](../Category/清朝屬國.md "wikilink")
[Category:东南亚古国](../Category/东南亚古国.md "wikilink")