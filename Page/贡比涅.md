<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><big><strong>贡比涅<br />
Compiègne</strong></big></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>大区</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>省</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>面积</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>人口</strong><br />
- 总人口<br />
- <a href="../Page/人口密度.md" title="wikilink">人口密度</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>坐标</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>网站</strong></p></td>
</tr>
</tbody>
</table>

**貢比涅**（或譯**康白尼**，；旧译：**康边**）是一座[法國的城市](../Page/法國.md "wikilink")，位于[上法兰西大区](../Page/上法兰西大区.md "wikilink")[瓦兹河畔](../Page/瓦兹河.md "wikilink")，離[首都](../Page/首都.md "wikilink")[巴黎東北](../Page/巴黎.md "wikilink")80公里，是[瓦兹省的一个副省会](../Page/瓦兹省.md "wikilink")。1430年[聖女貞德在這裡被擒獲](../Page/聖女貞德.md "wikilink")，轉賣給了[英國人](../Page/英國.md "wikilink")。[1918年德国向法国投降的协议和](../Page/康边停战协定.md "wikilink")[1940年法国向德国投降的协议](../Page/第二次贡比涅停战协定.md "wikilink")，都是在该城郊区森林的一个小火车站签订的。

## 地理

贡比涅位于巴黎以北80千米处，位于瓦兹河畔，[埃纳河注入瓦兹河的地方](../Page/埃纳河.md "wikilink")。

## 历史

[Compiegne_Chateau_Parkseite.jpg](https://zh.wikipedia.org/wiki/File:Compiegne_Chateau_Parkseite.jpg "fig:Compiegne_Chateau_Parkseite.jpg")
早在[墨洛溫王朝时期贡比涅就已经有一座行宫了](../Page/墨洛溫王朝.md "wikilink")。603年[克洛塔尔二世被](../Page/克洛塔尔二世.md "wikilink")[休德伯特二世和他的兄弟](../Page/休德伯特二世.md "wikilink")[提奧德里克二世战败后签署了贡比涅和约](../Page/提奧德里克二世.md "wikilink")。克洛塔尔二世的儿子[希爾德里克二世选贡比涅的行宫为他的都府](../Page/希爾德里克二世.md "wikilink")。[路易二世和](../Page/路易二世_\(西法兰克\).md "wikilink")[路易五世被埋葬在这里的教堂中](../Page/路易五世_\(西法兰克\).md "wikilink")。1380年[查理五世下令在当地建造一座](../Page/查理五世_\(法兰西\).md "wikilink")[要塞](../Page/要塞.md "wikilink")。

1635年法国和[瑞典在](../Page/瑞典.md "wikilink")[贡比涅条约中就他们在](../Page/贡比涅条约_\(1635年\).md "wikilink")[三十年战争中在](../Page/三十年战争.md "wikilink")[德国的利益达成协议](../Page/德国.md "wikilink")。

从1751年至1788年[路易十五在贡比涅造了一座](../Page/路易十五.md "wikilink")[古典主義宫殿](../Page/古典主義.md "wikilink")。[拿破仑·波拿巴下令维修该宫殿](../Page/拿破仑·波拿巴.md "wikilink")。

1810年, 在兩人婚禮之後, [拿破仑·波拿巴在此地首度見到他的妻子](../Page/拿破仑·波拿巴.md "wikilink"),
奧地利的瑪麗·路易斯。

[拿破仑三世常常把它用作秋宫](../Page/拿破仑三世.md "wikilink")。1870年帝国结束后它被改为一座[博物馆](../Page/博物馆.md "wikilink")。

从1917年4月至1918年3月同盟国的总部设立在贡比涅，这里还有一个巨大的战地医院。

德国和法国在贡比涅两次签署[停戰協定](../Page/停戰協定.md "wikilink")：

  - 1918年11月11日在贡比涅森林的一辆火车车厢中[第一次世界大战结束](../Page/第一次世界大战.md "wikilink")。法国[元帅](../Page/元帅.md "wikilink")[费迪南·福煦和德国政治家](../Page/费迪南·福煦.md "wikilink")[马提亚·艾尔兹贝格签署了](../Page/马提亚·艾尔兹贝格.md "wikilink")[康边停战协定](../Page/康边停战协定.md "wikilink")。
  - 1940年6月22日[纳粹德国与法国再次签署停火协议](../Page/纳粹德国.md "wikilink")。德国将军[威廉·凱特爾与法国将军](../Page/威廉·凱特爾.md "wikilink")[查尔斯·亨茨盖签署了](../Page/查尔斯·亨茨盖.md "wikilink")[第二次贡比涅停战协定](../Page/第二次贡比涅停战协定.md "wikilink")。

从1941年6月至1944年8月纳粹在贡比涅设立了一座集中营。1942年7月6日第一批政治犯被运往[奥斯威辛集中营](../Page/奥斯威辛集中营.md "wikilink")。

## 名胜

[Compiegne_Parc.jpg](https://zh.wikipedia.org/wiki/File:Compiegne_Parc.jpg "fig:Compiegne_Parc.jpg")

### 帝国剧院

1866年拿破仑三世在一[加尔默罗会修道院原址上在王宫附近建造了一座](../Page/加尔默罗会.md "wikilink")[剧院](../Page/剧院.md "wikilink")。1870年帝国覆没后还没有建成的剧院工地停工。从1987年开始剧院逐渐被整修，1991年终于建成。剧院经常演奏很少被上演的法国歌剧和轻歌剧。

### 王宫

贡比涅王宫是一座新古典式建筑，于1751年至1788年建成。王宫可以作为博物馆参观。王宫周围有一座很大的公园。

拿破仑曾经住在这里并于1810年3月23日首次接见他后来的妻子。后来宫殿有被整修和重新装饰。

后来拿破仑三世也非常喜欢贡比涅宫，他也经常使用王宫周围的猎场。

### 雅各教堂

圣雅各教堂本来是市内的两座牧师教堂之一，它是12世纪建造的，在装饰上体现了许多关于[聖雅各伯和去](../Page/聖雅各伯.md "wikilink")[孔波斯特拉的聖地牙哥朝圣的题材](../Page/孔波斯特拉的聖地牙哥.md "wikilink")。教堂的钟楼是15世纪造的，17世纪加高，在上层有14名圣人的塑像。由于它就位于王宫变上，因此是国王来做礼拜的教堂，18世纪它被整体改装并获得了非常奢侈的装饰。从1998年开始此教堂是[联合国教育、科学及文化组织](../Page/联合国教育、科学及文化组织.md "wikilink")[世界遗产中](../Page/世界遗产.md "wikilink")[通往圣地亚哥-德孔波斯特拉之路在法国部分的一部分](../Page/通往圣地亚哥-德孔波斯特拉之路.md "wikilink")。

### 第一次世界大战纪念碑

贡比涅东北有一座面积140平方公里的[贡比涅森林](../Page/贡比涅森林.md "wikilink")。

在贡比涅以东约六千米处，就在法国国道31以北几米远的地方，在埃纳河的一个转弯处有一个林间空地。1918年和1940年的停火协定就是在这里签署的。

在这座林间空地上有一座福煦元帅的纪念碑和一座纪念德国1918年战败的纪念碑（一把击破德国鹰的剑）以及签署停战协定的火车车厢的复制品。

## 交通

贡比涅是法国最早实现免费公交线路的城市。

[贡比涅站每天开行前往](../Page/贡比涅站.md "wikilink")[亚眠](../Page/亚眠站.md "wikilink")、[圣康坦](../Page/圣康坦站.md "wikilink")、[巴黎及](../Page/巴黎北站.md "wikilink")[克雷伊等地的列车](../Page/克雷伊站.md "wikilink")。

## 高校

  - [贡比涅技术大学](../Page/贡比涅技术大学.md "wikilink")
  - 贡比涅高等商业学校

## 名人

  - [苏珊·朗格伦](../Page/苏珊·朗格伦.md "wikilink")，网球运动员
  - [艾伯特·罗毕德](../Page/艾伯特·罗毕德.md "wikilink")，作家
  - [卢卡·迪巴格](../Page/卢卡·迪巴格.md "wikilink")，钢琴家

## 体育

从1977年开始每年一度的[巴黎–魯貝自行车赛从贡比涅开始](../Page/巴黎–魯貝.md "wikilink")。

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg" title="fig:Flag_of_Germany.svg">Flag_of_Germany.svg</a><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/兰茨胡特.md" title="wikilink">兰茨胡特</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Belgium.svg" title="fig:Flag_of_Belgium.svg">Flag_of_Belgium.svg</a><a href="../Page/比利时.md" title="wikilink">比利时</a><a href="../Page/于伊.md" title="wikilink">于伊</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg" title="fig:Flag_of_the_United_States.svg">Flag_of_the_United_States.svg</a><a href="../Page/美国.md" title="wikilink">美国</a><a href="../Page/罗利_(北卡罗来纳州).md" title="wikilink">罗利</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Israel.svg" title="fig:Flag_of_Israel.svg">Flag_of_Israel.svg</a><a href="../Page/以色列.md" title="wikilink">以色列</a><a href="../Page/提夫翁村.md" title="wikilink">提夫翁村</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg" title="fig:Flag_of_Italy.svg">Flag_of_Italy.svg</a><a href="../Page/意大利.md" title="wikilink">意大利</a><a href="../Page/阿罗纳.md" title="wikilink">阿罗纳</a></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Luxembourg.svg" title="fig:Flag_of_Luxembourg.svg">Flag_of_Luxembourg.svg</a><a href="../Page/卢森堡.md" title="wikilink">卢森堡</a><a href="../Page/菲安登.md" title="wikilink">菲安登</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg" title="fig:Flag_of_Japan.svg">Flag_of_Japan.svg</a><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/白河市.md" title="wikilink">白河</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Poland.svg" title="fig:Flag_of_Poland.svg">Flag_of_Poland.svg</a><a href="../Page/波兰.md" title="wikilink">波兰</a><a href="../Page/埃尔布隆格.md" title="wikilink">埃尔布隆格</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_England.svg" title="fig:Flag_of_England.svg">Flag_of_England.svg</a><a href="../Page/英格兰.md" title="wikilink">英格兰</a><a href="../Page/贝里圣埃德蒙兹.md" title="wikilink">贝里圣埃德蒙兹</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 參看

  - [康比涅城堡](../Page/康比涅城堡.md "wikilink")
  - 《貢比涅/康边停戰協定》：[第一次（一战）](../Page/康边停战协定.md "wikilink")；[第二次（二战）](../Page/第二次贡比涅停战协定.md "wikilink")

## 外部链接

  - [贡比涅市议会网站](http://www.mairie-compiegne.fr/)
  - [贡比涅技术大学](http://www.utc.fr/)

[Category:貢比涅](../Category/貢比涅.md "wikilink")
[C](../Category/瓦兹省市镇.md "wikilink")