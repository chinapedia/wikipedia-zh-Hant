**《财富》全球论坛**由美国Meredith公司所属的《[财富](../Page/财富_\(杂志\).md "wikilink")》杂志1995年创办，每16到18个月在世界上选一个具有吸引力的“热门”地点举行一次，邀请全球[跨国公司的](../Page/跨国公司.md "wikilink")[董事长](../Page/董事长.md "wikilink")、[总裁](../Page/总裁.md "wikilink")、[首席执行官](../Page/首席执行官.md "wikilink")、世界知名的[政治家](../Page/政治家.md "wikilink")、政府官员和[经济学者参加](../Page/经济学.md "wikilink")，共同探讨全球经济所面临的问题。迄止2017年，《财富》全球论坛已举办13届。两任[中共中央总书记](../Page/中共中央总书记.md "wikilink")[江泽民](../Page/江泽民.md "wikilink")（1999上海、2001香港）、[胡锦涛](../Page/胡锦涛.md "wikilink")（2005北京）做出了往届开幕讲话。

## 历次论坛

<table>
<thead>
<tr class="header">
<th><p>届数</p></th>
<th><p>时间</p></th>
<th><p>地点</p></th>
<th><p>主题</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一届</p></td>
<td><p>1995年3月8日至10日</p></td>
<td></td>
<td><p>“同一个商业世界”</p></td>
</tr>
<tr class="even">
<td><p>第二届</p></td>
<td><p>1996年3月5日至7日</p></td>
<td><p><a href="../Page/巴塞罗那.md" title="wikilink">巴塞罗那</a></p></td>
<td><p>“全球竞争新秩序”</p></td>
</tr>
<tr class="odd">
<td><p>第三届</p></td>
<td><p>1997年3月24日至26日</p></td>
<td><p><a href="../Page/曼谷.md" title="wikilink">曼谷</a></p></td>
<td><p>“保持奇迹”</p></td>
</tr>
<tr class="even">
<td><p>第四届</p></td>
<td><p>1998年9月23日至25日</p></td>
<td><p><a href="../Page/布达佩斯.md" title="wikilink">布达佩斯</a></p></td>
<td><p>“在新的全球经济中创造财富”</p></td>
</tr>
<tr class="odd">
<td><p>第五届</p></td>
<td><p>1999年9月27日至29日</p></td>
<td><p><a href="../Page/上海.md" title="wikilink">上海</a></p></td>
<td><p>“中国：未来的50年”</p></td>
</tr>
<tr class="even">
<td><p>第六届</p></td>
<td><p>2000年6月14日至16日</p></td>
<td><p><a href="../Page/巴黎.md" title="wikilink">巴黎</a></p></td>
<td><p>“电子——欧洲”</p></td>
</tr>
<tr class="odd">
<td><p>第七届</p></td>
<td><p>2001年5月8日至10日</p></td>
<td></td>
<td><p>“亚洲新貌”</p></td>
</tr>
<tr class="even">
<td><p>第八届</p></td>
<td><p>2002年11月11日至13日</p></td>
<td><p><a href="../Page/华盛顿哥伦比亚特区.md" title="wikilink">华盛顿</a></p></td>
<td><p>“领袖的力量——应对新的形势”</p></td>
</tr>
<tr class="odd">
<td><p>第九届</p></td>
<td><p>2005年5月16日至18日</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a></p></td>
<td><p>“中国和新的亚洲世纪”</p></td>
</tr>
<tr class="even">
<td><p>第十届</p></td>
<td><p>2007年10月29至31日</p></td>
<td><p><a href="../Page/新德里.md" title="wikilink">新德里</a></p></td>
<td><p>“操控全球经济”</p></td>
</tr>
<tr class="odd">
<td><p>第十一届</p></td>
<td><p>2010年6月26至28日</p></td>
<td><p><a href="../Page/开普敦.md" title="wikilink">开普敦</a></p></td>
<td><p>“新的全球机遇”</p></td>
</tr>
<tr class="even">
<td><p>第十二届</p></td>
<td><p>2013年6月6日至8日</p></td>
<td><p><a href="../Page/成都.md" title="wikilink">成都</a>[1]</p></td>
<td><p>“中国的新未来”</p></td>
</tr>
<tr class="odd">
<td><p>第十三届</p></td>
<td><p>2015年11月2日至4日</p></td>
<td><p><a href="../Page/三藩市.md" title="wikilink">三藩市</a></p></td>
<td><p>“赢在颠覆性世纪”</p></td>
</tr>
<tr class="even">
<td><p>第十四届</p></td>
<td><p>2017年12月6日至8日</p></td>
<td><p><a href="../Page/广州.md" title="wikilink">广州</a>[2]</p></td>
<td><p>“开放与创新：构建经济新格局”</p></td>
</tr>
<tr class="odd">
<td><p>第十五届</p></td>
<td><p>2018年10月15日至17日</p></td>
<td><p><a href="../Page/多伦多.md" title="wikilink">多伦多</a>[3]</p></td>
<td><p>“经济势在必行：让成果惠及每个人”</p></td>
</tr>
</tbody>
</table>

## 参考文献

[Category:财富 (杂志)](../Category/财富_\(杂志\).md "wikilink")
[Category:国际会议](../Category/国际会议.md "wikilink")
[Category:经济会议](../Category/经济会议.md "wikilink")
[Category:1995年建立](../Category/1995年建立.md "wikilink")

1.
2.
3.