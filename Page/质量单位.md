**质量单位**用于计量质量的[标准](../Page/标准.md "wikilink")，[国际单位制的基本](../Page/国际单位制.md "wikilink")[质量](../Page/质量.md "wikilink")[单位为](../Page/單位_\(度量衡\).md "wikilink")「[千克](../Page/千克.md "wikilink")」，又名「-{zh-cn:公斤;
zh-tw:千克;
zh-hk:千克;}-」，符号是kg。其它的质量单位还有[公吨](../Page/公吨.md "wikilink")、[克](../Page/克.md "wikilink")、[毫克等](../Page/毫克.md "wikilink")。

常用的質量單位依質量從大到小排列是：

  - [公吨](../Page/公吨.md "wikilink") (metric ton)
  - [公斤](../Page/公斤.md "wikilink") (kg)
  - [克](../Page/克.md "wikilink") (公克) (g)
  - [毫克](../Page/毫克.md "wikilink") (mg)
  - [微克](../Page/微克.md "wikilink") (µg)
  - 另有[公擔](../Page/公擔.md "wikilink")、[公兩](../Page/公兩.md "wikilink")、[公錢](../Page/公錢.md "wikilink")。

## 華人質量單位與公制換算

華人所用單位依照數量大到小，舊有「[擔](../Page/擔.md "wikilink")」(「石」)、「[衡](../Page/衡_\(單位\).md "wikilink")」、「[斤](../Page/斤.md "wikilink")」、「[兩](../Page/兩.md "wikilink")」、「錢」等等單位（*參見[度量衡](../Page/度量衡.md "wikilink")*），因此公制單位[千克早期挪用舊稱加上公制的限定語](../Page/千克.md "wikilink")，稱為「公斤」。

以常用的「斤」按照各地使用習慣，與公制有如下換算：

  - [台灣市集常用](../Page/台灣.md "wikilink")-{[台制](../Page/台制.md "wikilink")}-：1台斤
    = 0.6[千克](../Page/千克.md "wikilink") = 600
    [克](../Page/克.md "wikilink")，日本的一斤與臺灣一斤同重
  - [中國大陸現行](../Page/中國大陸.md "wikilink")[市制](../Page/市制.md "wikilink")：1市斤
    = 0.5 千克 = 500 克
  - [香港及](../Page/香港.md "wikilink")[澳門](../Page/澳門.md "wikilink")[司馬斤](../Page/司馬斤.md "wikilink")：1司馬斤
    =
    604.79克，[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[越南的一斤都接近](../Page/越南.md "wikilink")604克

## 單位列表

### [公制](../Page/公制.md "wikilink")

| 名稱                                                               | 符號 | 與[克的換算](../Page/千克.md "wikilink") |
| ---------------------------------------------------------------- | -- | --------------------------------- |
| 佑克、堯克（yottagram）                                                 | Yg | 10<sup>24</sup>                   |
| 皆克、澤克（zettagram）                                                 | Zg | 10<sup>21</sup>                   |
| 艾克（exagram）                                                      | Eg | 10<sup>18</sup>                   |
| 拍克（petagram）                                                     | Pg | 10<sup>15</sup>                   |
| 兆克、太克                                                            | Tg | 10<sup>12</sup>                   |
| 吉克                                                               | Gg | 10<sup>9</sup>                    |
| [公噸](../Page/公噸.md "wikilink")、兆克                                | Mg | 10<sup>6</sup>                    |
| [公斤](../Page/公斤.md "wikilink")、-{zh-cn:公斤; zh-tw:千克; zh-hk:千克;}- | kg | 10<sup>3</sup>                    |
| [公克](../Page/公克.md "wikilink")                                   | g  | 1                                 |
| [毫克](../Page/毫克.md "wikilink")                                   | mg | 10<sup>-3</sup>                   |
| 微克                                                               | µg | 10<sup>-6</sup>                   |
| 納克                                                               | ng | 10<sup>-9</sup>                   |
| 皮克                                                               | pg | 10<sup>-12</sup>                  |
| 飛克                                                               | fg | 10<sup>-15</sup>                  |
| 阿克                                                               | ag | 10<sup>-18</sup>                  |
| 介克                                                               | zg | 10<sup>-21</sup>                  |
| 攸克                                                               | yg | 10<sup>-24</sup>                  |

## 参考文献

## 参见

  - [度量衡](../Page/度量衡.md "wikilink")
  - [国际千克原器](../Page/国际千克原器.md "wikilink")
  - [地球质量](../Page/地球质量.md "wikilink")

{{-}}

[Category:计量单位](../Category/计量单位.md "wikilink")
[质量单位](../Category/质量单位.md "wikilink")