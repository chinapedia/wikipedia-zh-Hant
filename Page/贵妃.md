**贵妃**，中国古代[皇帝](../Page/皇帝.md "wikilink")[妃嫔的封号](../Page/妃嬪.md "wikilink")。

## 历史

[汉](../Page/汉朝.md "wikilink")、[晋时代](../Page/晋朝.md "wikilink")，贵妃偶用于非正式场合对高级妃嫔的尊称，并非正式爵位。

[南朝宋](../Page/刘宋.md "wikilink")[孝武帝](../Page/宋孝武帝.md "wikilink")，始正式置贵妃为后宫封号，\[1\]位比[相国](../Page/相国.md "wikilink")，与[貴嬪](../Page/貴嬪.md "wikilink")、[贵人号称](../Page/贵人.md "wikilink")[三夫人](../Page/夫人_\(位号\).md "wikilink")。后代多沿用其名。

[唐时](../Page/唐朝.md "wikilink")，贵妃是仅次于[皇后的](../Page/皇后.md "wikilink")[封号](../Page/封号.md "wikilink")。唐初，皇后之下设贵妃、[淑妃](../Page/淑妃.md "wikilink")、[德妃](../Page/德妃.md "wikilink")、[贤妃](../Page/贤妃.md "wikilink")，并称[四夫人](../Page/夫人_\(位号\).md "wikilink")，爵位正一品。[玄宗](../Page/唐玄宗.md "wikilink")[开元年间后宫封号改制](../Page/开元.md "wikilink")，设[惠妃](../Page/惠妃.md "wikilink")、[丽妃](../Page/丽妃.md "wikilink")、[华妃三妃取代四妃](../Page/华妃.md "wikilink")。开元十二年（724年），[王皇后被废](../Page/王皇后_\(唐玄宗\).md "wikilink")，玄宗特赐[武氏为惠妃](../Page/貞順皇后_\(唐玄宗\).md "wikilink")，另有[丽妃赵氏](../Page/趙麗妃.md "wikilink")、[华妃刘氏](../Page/刘华妃.md "wikilink")。开元二十三年（735年），[皇甫德儀逝世被追赠为淑妃](../Page/皇甫德儀.md "wikilink")\[2\]。其后，[天宝年间又册](../Page/天宝_\(唐朝\).md "wikilink")[杨氏为贵妃](../Page/杨贵妃.md "wikilink")。后世的唐朝皇帝，册封妃子的封号仍为贵妃、淑妃、德妃、贤妃。

[明朝时](../Page/明朝.md "wikilink")，宮中[皇妃的封号有很多](../Page/妃.md "wikilink")，如[顺妃](../Page/顺妃.md "wikilink")、[宁妃等](../Page/寧妃.md "wikilink")，但贵妃仍是最高级的封号。明中后期又有[皇贵妃的封号](../Page/皇贵妃.md "wikilink")，贵妃位降一级。

[清朝的后宫品级为](../Page/清朝.md "wikilink")：[皇后](../Page/皇后.md "wikilink")、[皇贵妃](../Page/皇贵妃.md "wikilink")、贵妃，[妃](../Page/妃.md "wikilink")、[嫔](../Page/嬪.md "wikilink")、[贵人](../Page/贵人.md "wikilink")、[常在](../Page/常在.md "wikilink")、[答应](../Page/答应.md "wikilink")、[官女子](../Page/官女子.md "wikilink")。贵妃是后宫第三等封号。

## 相关条目

### 北周

1.  北周宣帝：[元贵妃](../Page/元樂尚.md "wikilink")、[尉迟贵妃](../Page/尉遲熾繁.md "wikilink")

### 南朝

1.  宋孝武帝：[殷贵妃](../Page/殷淑儀.md "wikilink")（追赠）
2.  宋明帝：[陈贵妃](../Page/陳妙登.md "wikilink")
3.  齐武帝：[范贵妃](../Page/范贵妃.md "wikilink")
4.  陈宣帝：[钱贵妃](../Page/錢貴妃_\(南朝陳宣帝\).md "wikilink")
5.  陈后主：[张贵妃](../Page/張麗華.md "wikilink")

### 唐朝

1.  唐高祖：[万贵妃](../Page/萬貴妃_\(唐朝\).md "wikilink")
2.  唐太宗：[韦贵妃](../Page/韋珪.md "wikilink")、[杨贵妃](../Page/杨贵妃_\(唐太宗\).md "wikilink")
3.  唐睿宗：[豆卢贵妃](../Page/豆卢贵妃.md "wikilink")、[崔贵妃](../Page/崔贵妃_\(唐睿宗\).md "wikilink")
4.  唐玄宗：[杨贵妃](../Page/杨贵妃.md "wikilink")、[董贵妃](../Page/董贵妃.md "wikilink")
5.  唐代宗：[独孤贵妃](../Page/貞懿皇后_\(唐朝\).md "wikilink")、[崔贵妃](../Page/崔贵妃_\(唐代宗\).md "wikilink")
6.  唐宪宗：[郭贵妃](../Page/懿安皇后.md "wikilink")
7.  唐穆宗：[武贵妃](../Page/武才人_\(唐穆宗\).md "wikilink")（追赠）
8.  唐敬宗：[郭贵妃](../Page/郭贵妃_\(唐敬宗\).md "wikilink")
9.  唐懿宗：[王贵妃](../Page/惠安皇后.md "wikilink")、[杨贵妃](../Page/楊貴妃_\(唐懿宗\).md "wikilink")（追赠）

### 五代十国

1.  后周太祖：[张贵妃](../Page/張貴妃_\(後周太祖\).md "wikilink")
2.  前蜀高祖：[张贵妃](../Page/張貴妃_\(前蜀高祖\).md "wikilink")
3.  前蜀后主：[钱贵妃](../Page/钱贵妃_\(王衍\).md "wikilink")
4.  南汉后主：[李贵妃](../Page/李貴妃_\(南漢後主\).md "wikilink")
5.  后蜀高祖：[李贵妃](../Page/李太后_\(後蜀高祖\).md "wikilink")

### 宋朝

1.  宋太宗：[臧贵妃](../Page/臧贵妃.md "wikilink")（追尊）
2.  宋真宗：[沈贵妃](../Page/昭靜貴妃.md "wikilink")（尊封）、[杜贵妃](../Page/杜琼真.md "wikilink")（追尊）
3.  宋仁宗：[张贵妃](../Page/溫成皇后.md "wikilink")、[苗贵妃](../Page/昭節貴妃.md "wikilink")（尊封）、[周贵妃](../Page/昭淑贵妃.md "wikilink")（尊封）、[张贵妃](../Page/昭懿贵妃_\(宋仁宗\).md "wikilink")（追尊）
4.  宋神宗：[邢贵妃](../Page/邢貴妃.md "wikilink")（尊封）
5.  宋哲宗：[慕容贵妃](../Page/慕容贵妃.md "wikilink")（尊封）
6.  宋徽宗：[郑贵妃](../Page/顯肅皇后.md "wikilink")、[王贵妃](../Page/大王貴妃_\(宋徽宗\).md "wikilink")、[王贵妃](../Page/懿肅貴妃.md "wikilink")、[王贵妃](../Page/小王貴妃_\(宋徽宗\).md "wikilink")、[乔贵妃](../Page/喬貴妃_\(宋徽宗\).md "wikilink")、[刘贵妃](../Page/明達皇后.md "wikilink")、[刘贵妃](../Page/明節皇后.md "wikilink")
7.  宋高宗：[吴贵妃](../Page/宪圣慈烈皇后.md "wikilink")、[刘贵妃](../Page/劉賢妃_\(宋高宗\).md "wikilink")、[张贵妃](../Page/張貴妃_\(宋高宗\).md "wikilink")
8.  宋孝宗：[谢贵妃](../Page/成肅皇后.md "wikilink")、[蔡贵妃](../Page/蔡貴妃_\(宋孝宗\).md "wikilink")
9.  宋寧宗：[楊贵妃](../Page/恭聖皇后.md "wikilink")
10. 宋光宗：[黄贵妃](../Page/黃貴妃_\(宋光宗\).md "wikilink")、[张贵妃](../Page/张贵妃_\(宋光宗\).md "wikilink")
11. 宋理宗：[谢贵妃](../Page/謝道清.md "wikilink")、[贾贵妃](../Page/賈貴妃_\(宋理宗\).md "wikilink")、[阎贵妃](../Page/閻貴妃_\(宋理宗\).md "wikilink")

### 辽朝

1.  辽景宗：[萧贵妃](../Page/萧绰.md "wikilink")
2.  辽圣宗：[萧贵妃](../Page/蕭皇后_\(遼聖宗\).md "wikilink")、[萧贵妃](../Page/萧贵妃_\(辽圣宗\).md "wikilink")（[耶律燕哥母](../Page/耶律燕哥_\(辽圣宗\).md "wikilink")）
3.  辽兴宗：[萧贵妃](../Page/蕭三蒨.md "wikilink")

### 金朝

1.  金太祖：[萧贵妃](../Page/萧崇妃.md "wikilink")
2.  金熙宗：[裴满贵妃](../Page/悼平皇后.md "wikilink")
3.  海陵王：[大贵妃](../Page/大元妃_\(金海陵王\).md "wikilink")、[唐括贵妃](../Page/唐括定哥.md "wikilink")
4.  金世宗：[李贵妃](../Page/李妃_\(金世宗\).md "wikilink")

### 明朝

1.  明太祖：[孙贵妃](../Page/成穆貴妃.md "wikilink")
2.  明成祖：[王贵妃](../Page/昭獻貴妃.md "wikilink")、[张贵妃](../Page/张贵妃_\(明成祖\).md "wikilink")
3.  明仁宗：[郭贵妃](../Page/郭贵妃_\(明仁宗\).md "wikilink")
4.  明宣宗：[孙贵妃](../Page/孝恭章皇后.md "wikilink")、[何贵妃](../Page/何贵妃.md "wikilink")（追尊）
5.  明英宗：[周贵妃](../Page/孝肅皇后.md "wikilink")
6.  明宪宗：[万贵妃](../Page/萬貞兒.md "wikilink")、[邵贵妃](../Page/孝惠皇后_\(明朝\).md "wikilink")
7.  明世宗：[沈贵妃](../Page/沈貴妃_\(明世宗\).md "wikilink")、[文贵妃](../Page/文贵妃_\(明世宗\).md "wikilink")、[王贵妃](../Page/王贵妃_\(明世宗\).md "wikilink")、[阎贵妃](../Page/阎贵妃_\(明世宗\).md "wikilink")、[马贵妃](../Page/馬貞妃_\(明世宗\).md "wikilink")
8.  明穆宗：[李贵妃](../Page/孝定皇太后.md "wikilink")
9.  明神宗：[郑贵妃](../Page/孝宁太皇太后.md "wikilink")
10. 明思宗：[袁贵妃](../Page/袁貴妃_\(明思宗\).md "wikilink")、[田贵妃](../Page/田秀英.md "wikilink")
11. 弘光帝：[金贵妃](../Page/金貴妃_\(明安宗\).md "wikilink")

### 清朝

1.  清太宗：[贵妃博尔济吉特氏](../Page/懿靖大貴妃.md "wikilink")
2.  清圣祖：[贵妃佟佳氏](../Page/孝懿仁皇后.md "wikilink")、[贵妃佟佳氏](../Page/悫惠皇贵妃.md "wikilink")、[温僖贵妃钮祜禄氏](../Page/温僖贵妃.md "wikilink")、[皇考贵妃瓜尔佳氏](../Page/惇怡皇貴妃.md "wikilink")
3.  清世宗：[贵妃年氏](../Page/敦肅皇貴妃.md "wikilink")、[熹贵妃钮祜禄氏](../Page/孝聖憲皇后.md "wikilink")、[皇考裕贵妃耿氏](../Page/純懿皇貴妃.md "wikilink")（尊封）
4.  清高宗：[贵妃高氏](../Page/慧賢皇貴妃.md "wikilink")、[娴贵妃輝發那拉氏](../Page/清高宗继皇后.md "wikilink")、[令贵妃魏氏](../Page/孝儀純皇后.md "wikilink")、[纯贵妃苏氏](../Page/純惠皇貴妃.md "wikilink")、[庆贵妃陆氏](../Page/慶恭皇貴妃.md "wikilink")、[嘉贵妃金氏](../Page/淑嘉皇貴妃.md "wikilink")、[婉贵妃陈氏](../Page/婉貴太妃.md "wikilink")（尊封）、[颖贵妃巴林氏](../Page/颖贵妃.md "wikilink")、[忻贵妃戴佳氏](../Page/忻貴妃.md "wikilink")（追封）、[愉贵妃珂里叶特氏](../Page/愉貴妃.md "wikilink")（追封）、[循贵妃伊尔根觉罗氏](../Page/循貴妃.md "wikilink")（追封）
5.  清仁宗：[贵妃钮祜禄氏](../Page/孝和睿皇后.md "wikilink")、[諴贵妃刘佳氏](../Page/和裕皇贵妃.md "wikilink")、[皇考如贵妃钮祜禄氏](../Page/恭顺皇贵妃.md "wikilink")
6.  清宣宗：[全贵妃钮祜禄氏](../Page/孝全成皇后.md "wikilink")、[静贵妃博尔济吉特氏](../Page/孝静成皇后.md "wikilink")、[琳贵妃乌雅氏](../Page/莊順皇貴妃.md "wikilink")、[彤贵妃舒穆禄氏](../Page/彤贵妃.md "wikilink")、[佳贵妃郭佳氏](../Page/佳貴妃.md "wikilink")（尊封）、[成贵妃钮祜禄氏](../Page/成貴妃.md "wikilink")（尊封）
7.  清文宗：[贞贵妃钮祜禄氏](../Page/慈安太后.md "wikilink")、[懿贵妃叶赫那拉氏](../Page/慈禧太后.md "wikilink")、[祺贵妃佟佳氏](../Page/端恪皇贵妃.md "wikilink")（尊封）、[玫贵妃徐佳氏](../Page/玟贵妃.md "wikilink")（尊封）、[婉贵妃索绰络氏](../Page/婉贵妃_\(咸丰帝\).md "wikilink")（尊封）
8.  清穆宗：[瑨贵妃西林觉罗氏](../Page/敦惠皇貴妃.md "wikilink")、[珣贵妃阿鲁特氏](../Page/恭肅皇貴妃.md "wikilink")、[瑜贵妃赫舍里氏](../Page/敬懿皇贵妃.md "wikilink")
9.  清德宗：[兼祧皇考瑾贵妃他他拉氏](../Page/瑾妃.md "wikilink")（尊封）、[珍贵妃他他拉氏](../Page/珍妃.md "wikilink")（追封）
10. [溥仪](../Page/溥仪.md "wikilink")：[明贤贵妃谭玉龄](../Page/譚玉齡.md "wikilink")（追赠）

## 参考资料

  - 《[南史](../Page/南史.md "wikilink") 卷十一 列传第一》
  - [中國皇后及妃嬪列表](../Page/中國皇后及妃嬪列表.md "wikilink")

## 注释

[\*](../Category/貴妃.md "wikilink")

1.  《[南史](../Page/南史.md "wikilink") 卷十一
    列传第一》及孝武[孝建三年](../Page/孝建.md "wikilink")，省[夫人](../Page/夫人_\(位号\).md "wikilink")，置贵妃，位比[相国](../Page/相国.md "wikilink")。进[贵嫔比](../Page/貴嬪.md "wikilink")[丞相](../Page/丞相.md "wikilink")，[贵人比](../Page/贵人.md "wikilink")[三司](../Page/三司官.md "wikilink")，以为[三夫人](../Page/夫人_\(位号\).md "wikilink")。
2.