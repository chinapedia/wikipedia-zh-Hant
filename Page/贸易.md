[Silkroutes.jpg](https://zh.wikipedia.org/wiki/File:Silkroutes.jpg "fig:Silkroutes.jpg")跨國貿易\]\]
[Free_Trade_Areas.PNG](https://zh.wikipedia.org/wiki/File:Free_Trade_Areas.PNG "fig:Free_Trade_Areas.PNG")
[Canal_de_Panamá_Mayo_2008_342.jpg](https://zh.wikipedia.org/wiki/File:Canal_de_Panamá_Mayo_2008_342.jpg "fig:Canal_de_Panamá_Mayo_2008_342.jpg")
**贸易**是自願的貨品或服務交換。貿易也被稱為[商業](../Page/商業.md "wikilink")。貿易是在一個市場裏面進行的。最原始的貿易形式是以物易物，即直接交換貨品或服務。現代的貿易則普遍以一種[媒介作討價還價](../Page/媒介.md "wikilink")，如[金錢](../Page/金錢.md "wikilink")。[金錢的出現](../Page/金錢.md "wikilink")（以及後來的[信用證](../Page/信用證.md "wikilink")、[鈔票以及](../Page/鈔票.md "wikilink")[非實體金錢](../Page/非實體金錢.md "wikilink")）大大簡化和促進了貿易。兩個貿易者之間的貿易稱為[雙邊貿易](../Page/雙邊貿易.md "wikilink")，多於兩個貿易者的則稱為[多邊貿易](../Page/多邊貿易.md "wikilink")。

貿易出現的原因眾多。由於[勞動力的專門化](../Page/勞動力.md "wikilink")，個體只會從事一個小範疇的工作，所以他們必須以貿易來獲取生活的日用品。兩個地區之間的貿易往往是因為一地在生產某產品上有相對優勢，如有較佳的[技術](../Page/技術.md "wikilink")、較易獲取原材料等。

## 贸易简史

西汉时[張騫和東漢時](../Page/張騫.md "wikilink")[班超出使](../Page/班超.md "wikilink")[西域开辟的以](../Page/西域.md "wikilink")[长安](../Page/长安.md "wikilink")（今[西安](../Page/西安.md "wikilink")）为起点，经[甘肃](../Page/甘肃.md "wikilink")、[新疆](../Page/新疆.md "wikilink")，到[中亚](../Page/中亚.md "wikilink")、[西亚](../Page/西亚.md "wikilink")，并联结地中海各国的陆上通道，开辟了中外交流的新纪元。

明初需要大量馬匹。洪武二十年（1387）九月，撒馬兒罕駙馬[帖木兒遣回回滿剌哈非思等來朝貢馬十五匹](../Page/帖木兒.md "wikilink")、駝二隻。洪武二十三年（1390），撒馬兒罕回回舍怯兒阿裏義等以馬六百七十匹抵涼州互市。洪武二十七年，帖木兒遣酋長[迭力必失等來朝貢馬三百匹](../Page/迭力必失.md "wikilink")。

建文四年（1402年）九月壬辰，[陝西行都司奏](../Page/陝西.md "wikilink")：“回回可古思于寧夏市馬，請官市之，以資邊用”。\[1\]永樂元年十一月，兀良哈頭目哈兒兀歹遣部屬脫古思等貢馬，詔“命禮部賜鈔幣襲衣並償其馬直，上馬每匹鈔五十錠，中馬四十錠，下馬三十錠；每匹仍與彩幣表裏一”。\[2\]

## 注釋

## 货币贸易

  - [貨幣](../Page/貨幣.md "wikilink")
  - [外匯](../Page/外匯.md "wikilink")
  - [外匯存底](../Page/外匯存底.md "wikilink")
  - [貿易戰](../Page/貿易戰.md "wikilink")

## 参阅

  - [国际贸易](../Page/国际贸易.md "wikilink")
  - [國際貿易年表](../Page/國際貿易年表.md "wikilink")
  - [公平贸易](../Page/公平贸易.md "wikilink")
  - [反不正当竞争](../Page/反不正当竞争.md "wikilink")
  - [贸易壁壘](../Page/贸易壁壘.md "wikilink")
  - [聯合國貿易和發展會議](../Page/聯合國貿易和發展會議.md "wikilink")
  - [WTO](../Page/WTO.md "wikilink")
  - [关税](../Page/关税.md "wikilink")
  - [雙邊貿易](../Page/雙邊貿易.md "wikilink")
  - [全球化](../Page/全球化.md "wikilink")
  - [走私](../Page/走私.md "wikilink")
  - [交易](../Page/交易.md "wikilink")

[Category:商業](../Category/商業.md "wikilink")
[Category:货币](../Category/货币.md "wikilink")
[Category:国际贸易](../Category/国际贸易.md "wikilink")

1.  《明太宗實錄》卷12下，洪武三十五年九月壬辰（十二日）條
2.  《明實錄·太宗實錄》卷19，永樂元年四月壬戌條，第343頁。