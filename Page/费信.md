**费信**，字公晓，号玉峰松岩生\[1\]，[明](../Page/明.md "wikilink")[吴郡](../Page/苏州.md "wikilink")[昆山人](../Page/昆山.md "wikilink"),[洪武二十一年](../Page/洪武.md "wikilink")（1388年）生\[2\]。其兄为太苍卫。年十四，代兄参军。年少好学，因家贫常借书阅读，又自学通[阿拉伯文](../Page/阿拉伯文.md "wikilink")。永乐七年（1409年）二十二岁。[永乐](../Page/永乐.md "wikilink")、[宣德年间曾任翻译官随三宝太监](../Page/宣德.md "wikilink")[郑和四次下西洋](../Page/郑和.md "wikilink")：

  - 第一次 永乐七年（1409年）
  - 第二次 永乐十年（1412年）
  - 第三次 永乐十三年（1415年）
  - 第四次
    [明](../Page/明.md "wikilink")[宣德五年](../Page/宣德.md "wikilink")（1430年）

费信在[正统元年](../Page/正统_\(年号\).md "wikilink")（1436年）著《[星槎胜览](../Page/星槎胜览.md "wikilink")》上下卷，纪录下西洋时所见所闻各国风土人情，上卷[占城国](../Page/占城.md "wikilink")、[交栏山](../Page/交栏山.md "wikilink")、[暹逻国](../Page/暹逻.md "wikilink")、[爪哇国](../Page/爪哇.md "wikilink")、[旧港](../Page/三佛齐.md "wikilink")、[满刺加国](../Page/马六甲.md "wikilink")、[木骨都束等二十二国是亲身经历](../Page/木骨都束.md "wikilink")，下卷多从《[岛夷志略](../Page/岛夷志略.md "wikilink")》摘录。

费信著《星槎胜览》、[马欢著](../Page/马欢.md "wikilink")《[瀛涯胜览](../Page/瀛涯胜览.md "wikilink")》和[巩珍著](../Page/巩珍.md "wikilink")《[西洋番国志](../Page/西洋番国志.md "wikilink")》是关于[郑和下西洋的第一手资料](../Page/郑和.md "wikilink")，他们都先后随[郑和访问西洋各国](../Page/郑和.md "wikilink")。
除了《星槎胜览》费信还著有《[天心纪行录](../Page/天心纪行录.md "wikilink")》一卷，已失。

## 费信岛

为了纪念航海家费信，[南沙群岛中有一岛名为](../Page/南沙群岛.md "wikilink")[费信岛](../Page/费信岛.md "wikilink")。

## 註釋

## 參考文獻

  - 《[郑和下西洋考](../Page/郑和下西洋考.md "wikilink")》(法)[伯希和著](../Page/伯希和.md "wikilink"),[冯承钧译](../Page/冯承钧.md "wikilink")
    中华书局 ISBN 7-101-03511-6
  - [向达](../Page/向达.md "wikilink")
    《关于[三宝太监下西洋的几种资料](../Page/三宝太监.md "wikilink")》，收入《郑和研究百年论文选》ISBN
    7-301-07154-X

[F费](../Page/category:明朝外交官.md "wikilink")
[F费](../Page/category:中國旅行家.md "wikilink")
[F费](../Page/category:崑山人.md "wikilink")

[F费](../Category/中国航海家.md "wikilink")
[Category:中国旅游作家](../Category/中国旅游作家.md "wikilink")
[Category:郑和下西洋](../Category/郑和下西洋.md "wikilink")
[Category:中外交通史人名](../Category/中外交通史人名.md "wikilink")
[C](../Category/費姓.md "wikilink")

1.  [伯希和著](../Page/伯希和.md "wikilink") [冯承钧译](../Page/冯承钧.md "wikilink")
    《[郑和下西洋考](../Page/郑和下西洋考.md "wikilink")》 中华书局 35页 ISBN
    7-101-03511-6
2.  冯承钧 《星槎胜览校注》 《郑和下西洋文选》 173页