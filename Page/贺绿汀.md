[位于上海音乐学院内的贺绿汀雕像.jpg](https://zh.wikipedia.org/wiki/File:位于上海音乐学院内的贺绿汀雕像.jpg "fig:位于上海音乐学院内的贺绿汀雕像.jpg")
[Street_Angel_(1937_film).ogv](https://zh.wikipedia.org/wiki/File:Street_Angel_\(1937_film\).ogv "fig:Street_Angel_(1937_film).ogv")

**贺绿汀**（），名**楷**，[号](../Page/号.md "wikilink")**抱真**，[湖南省](../Page/湖南省.md "wikilink")[邵东县人](../Page/邵东县.md "wikilink")，中國作曲家、音乐理论家、音乐教育家。

历任[中国共产党第八次全国代表大会代表](../Page/中国共产党第八次全国代表大会.md "wikilink")，第一届[全国人大代表](../Page/全国人大.md "wikilink")，第三、四、七届[全国政协委员](../Page/全国政协.md "wikilink")，第五、六届全国政协常务委员，[中国文联副主席](../Page/中国文联.md "wikilink")、名誉委员，[中华人民共和国教育部艺术教育委员会委员](../Page/中华人民共和国教育部.md "wikilink")，[中国音协](../Page/中国音协.md "wikilink")、上海音协名誉主席，[上海音乐学院院长](../Page/上海音乐学院.md "wikilink")、名誉院长。1982年，[国际音乐理事会第](../Page/国际音乐理事会.md "wikilink")20届年会上，被选为国际音乐理事会终生荣誉会员，是中国目前唯一获此殊荣的音乐家。

贺绿汀从事音乐工作76年，谱写了3部大合唱、6部不同形式的[歌剧](../Page/歌剧.md "wikilink")(其中两部与人合作)、5部[话剧配乐](../Page/话剧.md "wikilink")、近200首歌曲、25首[合唱曲](../Page/合唱曲.md "wikilink")、5首[钢琴曲](../Page/钢琴曲.md "wikilink")、7首[管弦乐曲](../Page/管弦乐曲.md "wikilink")、27部[电影音乐和一些](../Page/电影音乐.md "wikilink")[器乐独奏曲](../Page/器乐独奏曲.md "wikilink")。1934年所作钢琴曲《[牧童短笛](../Page/牧童短笛.md "wikilink")》和《[摇篮曲](../Page/摇篮曲.md "wikilink")》在[亚历山大·齐尔品举办的](../Page/亚历山大·齐尔品.md "wikilink")“征求中国风味的钢琴曲”评选中获第一奖。歌曲《[游击队歌](../Page/游击队歌.md "wikilink")》被评为“20世纪华人音乐经典”。

2003年9月为纪念贺绿汀而建造的[贺绿汀音乐厅在](../Page/贺绿汀音乐厅.md "wikilink")[上海音乐学院正式开业](../Page/上海音乐学院.md "wikilink")。音乐厅建筑面积4324平方米。其中剧场面积537平方米，层高14米。开畅式的专业[音乐厅](../Page/音乐厅.md "wikilink")[舞台面积为](../Page/舞台.md "wikilink")125平方米。可容纳三[管制乐队演奏](../Page/管制乐队.md "wikilink")。舞台台宽（台口）14.9米，台深8.4米，音乐厅观众席742座，为一层观众席和二层两侧包厢席。\[1\]

## 简历

1917年，考入[长沙的湖南省立甲种工业学校机械科](../Page/长沙.md "wikilink")，半工半读，并在两年后，插班考入宝庆([邵阳旧称](../Page/邵阳.md "wikilink"))县中，毕业后在仙槎小学当教员。1923年春考入长沙[岳云中学艺术专修科](../Page/岳云中学.md "wikilink")，毕业后留校任音乐教师。1926年回到宝庆县中、县立师范及母校循程小学教音乐、美术，同年加入[中国共产党](../Page/中国共产党.md "wikilink")，并于次年12月参加[广州起义](../Page/廣州暴動.md "wikilink")。

1931年考入[上海音乐专科学校](../Page/上海音乐专科学校.md "wikilink")，師從音樂家[黃自](../Page/黃自.md "wikilink")。1934年加入上海[明星影片公司](../Page/明星影片公司.md "wikilink")，任音乐科科长。1936年参加[左翼歌曲作者协会](../Page/左翼歌曲作者协会.md "wikilink")。1937年参与组织上海文化界抗日救亡演剧第一队。為《[四季歌](../Page/四季歌_\(马路天使\).md "wikilink")》、《[天涯歌女](../Page/天涯歌女.md "wikilink")》作曲，為《[馬路天使](../Page/馬路天使.md "wikilink")》的主題曲。

1938年到[武汉](../Page/武汉.md "wikilink")，在[国民政府军委政治部第三厅所属的](../Page/国民政府.md "wikilink")[中国电影制片厂任音乐科长兼该厂](../Page/中国电影制片厂.md "wikilink")[中国合唱团的总干事](../Page/中国合唱团.md "wikilink")。1938年8月到[重庆](../Page/重庆.md "wikilink")，曾任重庆[中央广播电台音乐科科长](../Page/中央广播电台.md "wikilink")、[重庆育才中学音乐教员](../Page/重庆育才中学.md "wikilink")。

1943年到[延安](../Page/延安.md "wikilink")，后去[东北](../Page/東北三省.md "wikilink")。1946年回延安，任[中共中央党校文艺工作研究室音乐组长](../Page/中共中央党校.md "wikilink")，并与[李伯钊等负责筹建](../Page/李伯钊.md "wikilink")[中央管弦乐团](../Page/中央管弦乐团.md "wikilink")，并任团长兼合唱队长。

1949年2月，任[北平师大音乐系主任及筹建中的国立音乐学院](../Page/北平师大.md "wikilink")([中央音乐学院前身](../Page/中央音乐学院.md "wikilink"))副院长，还被选为全国文联常务理事及中国音协副主席。1949年9月任[上海音乐学院院长](../Page/上海音乐学院.md "wikilink")。

### 文化大革命期间

1966年6月10日，中共上海市委召开“文化大革命动员大会”，上海市长[曹荻秋在动员报告中点名指控](../Page/曹荻秋.md "wikilink")8名艺术家、作家、编辑、和教授是“反党反社会主义分子”，贺绿汀是其中之一。贺绿汀也在全国性报纸上受到点名批判攻击。教育界和艺术界都是文革规划好的重点打击对象，贺绿汀身为音乐家以及大学校长，身兼二罪，在劫难逃。[红卫兵运动兴起以后](../Page/红卫兵.md "wikilink")，贺绿汀进而遭到肉体攻击和折磨。9月16日，贺绿汀及妻子[姜瑞芝被该校红卫兵绑架到学校](../Page/姜瑞芝.md "wikilink")。贺绿汀被一块黑布蒙住头，被皮带抽打，直打得身上的衣服稀烂，与血水混合在一起。姜瑞芝也被逼在墙角打得满身是伤。这一群人打过以后，另一群人围上来，将他们分别绑架，又整整折磨一夜。他们的家被抄，家里的东西都被砸碎\[2\]。――文革中，只有三百多人的上海音乐学院发生了十七起所谓“非正常死亡”。这些死亡就发生在这样的暴力虐待的基础上。1968年3月21日，贺绿汀被上海当局关进正式监狱。他被关了5年，1973年才被释放。在这期间，他多次被押到“斗争大会”上遭到“斗争”，其中包括在电视上被“斗争”。“斗争会”一直开到了电视上，也算是上海对高科技的特别使用，尽管当时私人很少人有电视机，在上海之外的地方也没有电视节目播送。在文革中，他的三个女儿因拒绝与父亲划清界限而被定为“翻案集团骨干”，次女贺晓秋在受打击后在家中开煤气自杀。

### 晚年

1979年，贺绿汀复任上海音乐学院院长，1984年退任[上海音乐学院名誉院长](../Page/上海音乐学院.md "wikilink")。1999年4月27日20时15分，贺绿汀在上海逝世，享耆寿96岁\[3\]。

## 代表作

  - 钢琴曲《[牧童短笛](../Page/牧童短笛.md "wikilink")》、《[摇篮曲](../Page/摇篮曲.md "wikilink")》。
  - 管弦乐曲《[森吉德玛](../Page/森吉德玛.md "wikilink")》、《[晚会](../Page/晚会.md "wikilink")》。
  - 电影歌曲《[春天里](../Page/春天里.md "wikilink")》、《[四季歌](../Page/四季歌_\(马路天使\).md "wikilink")》、《[天涯歌女](../Page/天涯歌女.md "wikilink")》。
  - 歌曲《[游击队歌](../Page/游击队歌.md "wikilink")》、《[垦春泥](../Page/垦春泥.md "wikilink")》、《[嘉陵江上](../Page/嘉陵江上.md "wikilink")》。

## 参见

  - [古典音乐作曲家列表](../Page/古典音乐作曲家列表.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

  - 贺小秋的故事 <http://spaceking.blog.edu.cn/2006/193463.html>
  - 贺绿汀的苦难 <http://news.163.com/09/0522/16/59UCICT1000120GR.html>

[Category:邵东人](../Category/邵东人.md "wikilink")
[Category:中国作曲家](../Category/中国作曲家.md "wikilink")
[Category:20世纪作曲家](../Category/20世纪作曲家.md "wikilink")
[Category:中国共产党党员
(1926年入党)](../Category/中国共产党党员_\(1926年入党\).md "wikilink")
[Category:第一届全国人大代表](../Category/第一届全国人大代表.md "wikilink")
[Category:第三届全国政协委员](../Category/第三届全国政协委员.md "wikilink")
[Category:第四届全国政协委员](../Category/第四届全国政协委员.md "wikilink")
[Category:第五届全国政协委员](../Category/第五届全国政协委员.md "wikilink")
[Category:第六届全国政协委员](../Category/第六届全国政协委员.md "wikilink")
[Category:第七届全国政协委员](../Category/第七届全国政协委员.md "wikilink")
[L綠](../Category/賀姓.md "wikilink")
[Category:中國音樂家協會主席](../Category/中國音樂家協會主席.md "wikilink")
[Category:中國音樂家協會副主席](../Category/中國音樂家協會副主席.md "wikilink")
[Category:上海音乐学院院长](../Category/上海音乐学院院长.md "wikilink")

1.  [上海文化信息网,贺绿汀音乐厅](http://www.culture.sh.cn/venue_detail.asp?venueid=85)

2.
3.