**布莱克本（Blackburn）B-24“贼鸥”（Skua）式战斗轰炸机**是[英国海军航空兵（FAA）在](../Page/英国海军航空兵.md "wikilink")1940年代初期所使用的一种单发双座舰载飞机。该机系按照0.27/34要求设计，是英国海军航空兵所拥有的第一种全金属结构的单翼飞机。“贼鸥”式装备了可收放的起落架以及全封闭座舱，在当时来说是非常先进的设计。相比之下，同时代研制的[剑鱼式鱼雷轰炸机仍然是双翼结构](../Page/剑鱼式鱼雷轰炸机.md "wikilink")，采用固定起落架和敞开座舱。

由于发动机马力不足，“贼鸥”的速度相对较低，但机翼内4挺机枪以及後座的活动机枪却使得该机在格斗中处于有利的位置。当作为俯冲轰炸机使用时，机身中线下特制的叉形挂弹架上可以挂载一枚500磅的炸弹。

## 作戰紀錄

[皇家方舟号](../Page/皇家方舟号.md "wikilink")[航空母舰上的](../Page/航空母舰.md "wikilink")3架贼鸥於1939年9月26日在北海上空击落了一架[德国的](../Page/德国.md "wikilink")[道尼尔](../Page/道尼尔.md "wikilink")[Do
18飞艇](../Page/Do_18飞艇.md "wikilink")，因此成了二战中第一款击落敌机的英国飞机。
1940年4月10日，16架从奥克尼群岛起飞的贼鸥式击沉了入侵挪威的德国[巡洋舰](../Page/巡洋舰.md "wikilink")[柯尼斯堡号](../Page/柯尼斯堡号.md "wikilink")，而该舰也是盟军在二战中击沉的第一艘敌舰。

尽管贼鸥式在[挪威和](../Page/挪威.md "wikilink")[地中海对抗敌方轰炸机时表现良好](../Page/地中海.md "wikilink")，但在面对现代化的战斗机（如德军的[Bf
109](../Page/Bf_109.md "wikilink")）时却损失惨重。因此，该机从1941年起逐步退出了第一线的战斗任务，由火力更强且速度更快的飞机，如[费瑞的](../Page/费瑞.md "wikilink")[管鼻鸌](../Page/管鼻鸌.md "wikilink")（Fulmar）予以取代。

## 主要型号

### Skua MK.I 型

原型机，采用了一台840马力的布里斯托尔“水星”IX 发动机作为动力。

### Skua MK.II 型

生产型，共生产190架。由于“水星”发动机需要优先用于布里斯托尔“布伦海姆”轰炸机的生产线，“贼鸥”的生产型换装了890马力的布里斯托尔“英仙座”XII套筒分气式发动机。

## 使用国家

[英国皇家海军航空兵](../Page/英国皇家海军航空兵.md "wikilink")

## 装备该机的部队

  - [755 Naval Air
    Squadron](../Page/755_Naval_Air_Squadron.md "wikilink")
  - [757 Naval Air
    Squadron](../Page/757_Naval_Air_Squadron.md "wikilink")
  - [758 Naval Air
    Squadron](../Page/758_Naval_Air_Squadron.md "wikilink")
  - [759 Naval Air
    Squadron](../Page/759_Naval_Air_Squadron.md "wikilink")
  - [760 Naval Air
    Squadron](../Page/760_Naval_Air_Squadron.md "wikilink")
  - [767 Naval Air
    Squadron](../Page/767_Naval_Air_Squadron.md "wikilink")
  - [769 Naval Air
    Squadron](../Page/769_Naval_Air_Squadron.md "wikilink")
  - [770 Naval Air
    Squadron](../Page/770_Naval_Air_Squadron.md "wikilink")
  - [771 Naval Air
    Squadron](../Page/771_Naval_Air_Squadron.md "wikilink")
  - [772 Naval Air
    Squadron](../Page/772_Naval_Air_Squadron.md "wikilink")
  - [774 Naval Air
    Squadron](../Page/774_Naval_Air_Squadron.md "wikilink")
  - [776 Naval Air
    Squadron](../Page/776_Naval_Air_Squadron.md "wikilink")
  - [778 Naval Air
    Squadron](../Page/778_Naval_Air_Squadron.md "wikilink")
  - [779 Naval Air
    Squadron](../Page/779_Naval_Air_Squadron.md "wikilink")
  - [780 Naval Air
    Squadron](../Page/780_Naval_Air_Squadron.md "wikilink")
  - [782 Naval Air
    Squadron](../Page/782_Naval_Air_Squadron.md "wikilink")
  - [787 Naval Air
    Squadron](../Page/787_Naval_Air_Squadron.md "wikilink")
  - [788 Naval Air
    Squadron](../Page/788_Naval_Air_Squadron.md "wikilink")
  - [789 Naval Air
    Squadron](../Page/789_Naval_Air_Squadron.md "wikilink")
  - [791 Naval Air
    Squadron](../Page/791_Naval_Air_Squadron.md "wikilink")
  - [792 Naval Air
    Squadron](../Page/792_Naval_Air_Squadron.md "wikilink")
  - [794 Naval Air
    Squadron](../Page/794_Naval_Air_Squadron.md "wikilink")
  - [797 Naval Air
    Squadron](../Page/797_Naval_Air_Squadron.md "wikilink")
  - [800 Naval Air
    Squadron](../Page/800_Naval_Air_Squadron.md "wikilink")
  - [801 Naval Air
    Squadron](../Page/801_Naval_Air_Squadron.md "wikilink")
  - [803 Naval Air
    Squadron](../Page/803_Naval_Air_Squadron.md "wikilink")
  - [806 Naval Air
    Squadron](../Page/806_Naval_Air_Squadron.md "wikilink")

## 主要技术数据

### 主要技术特征

  - 乘员: 2
  - 全长: 35 英尺 7 英寸 (10.8 米)
  - 翼展: 46 英尺 2 英寸 (14.1 米)
  - 全高: 14 英尺 2 英寸 (4.3 米)
  - 翼面积: 312 平方英尺 (29.0 平方米)
  - 空重: 5,490 磅 (2,490 公斤)
  - 满载重量: 8,228 磅 (3,730 公斤)
  - 动力: 1× 布里斯托尔 “[英仙座](../Page/英仙座.md "wikilink")（Perseus）” XII 星型发动机,
    890 马力 (675 千瓦)

### 性能

  - 最大速度: 225 英里/小时 (195 节, 约360 公里/小时) 6,500 英尺 (2,000 米)
  - 航程: 800 英里 (700 海里, 1,300 公里)
  - 升限: 20,200 英尺 (6,150 米)
  - 翼载荷: 26.4 磅/平方英尺 (128 公斤/平方米)
  - 推重比: 0.11 马力/磅 (180 瓦/公斤)

### 武器装备

  - 固定武装:
      - 4× 0.303 in (7.7 mm)[白朗宁机枪](../Page/白朗宁机枪.md "wikilink")（机翼）
      - 1×
        [路易斯机枪或](../Page/路易斯机枪.md "wikilink")[維克斯机枪](../Page/維克斯机枪.md "wikilink")（后座）
  - 炸弹: 1× 500 磅 (227 公斤) [炸弹](../Page/炸弹.md "wikilink")

## 參見

  - [剑鱼式鱼雷轰炸机](../Page/剑鱼式鱼雷轰炸机.md "wikilink")
  - [SBD轟炸機](../Page/SBD轟炸機.md "wikilink")
  - [Ju 87俯衝轟炸機](../Page/Ju_87俯衝轟炸機.md "wikilink")

[Category:英國轟炸機](../Category/英國轟炸機.md "wikilink")
[Category:二戰俯衝轟炸機](../Category/二戰俯衝轟炸機.md "wikilink")
[Category:舰载机](../Category/舰载机.md "wikilink")
[Category:布雷克本飛機](../Category/布雷克本飛機.md "wikilink")