**贾维斯·詹姆斯·海耶斯**（，），生于[乔治亚州](../Page/乔治亚州.md "wikilink")[亚特兰大](../Page/亚特兰大.md "wikilink")，[美国职业篮球运动员](../Page/美国.md "wikilink")，司职[小前锋](../Page/小前锋.md "wikilink")。

身高2.01米（6英尺7英寸），体重220磅，在大学期间是乔治亚大学的小前锋，他在[2003年NBA选秀中第一轮第](../Page/2003年NBA选秀.md "wikilink")10顺位被[华盛顿巫師队选中](../Page/华盛顿巫師队.md "wikilink")。

在他的职业生涯中得到场均8.8分，海耶斯有着不错的得分能力但是防守能力欠佳。

2007年8月15日，海耶斯离开效力四个赛季的巫師队和底特律活塞队签约。\[1\]

2013年他以歸化球員身分，加入[卡達國家男子籃球隊](../Page/卡達國家男子籃球隊.md "wikilink")，並在[2013年亞洲籃球錦標賽中出賽](../Page/2013年亞洲籃球錦標賽.md "wikilink")。

## 参考资料

<references/>

## 外部链接

[Category:非洲裔美國籃球運動員](../Category/非洲裔美國籃球運動員.md "wikilink")
[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:華盛頓奇才隊球員](../Category/華盛頓奇才隊球員.md "wikilink")
[Category:底特律活塞队球员](../Category/底特律活塞队球员.md "wikilink")
[Category:新泽西网队球员](../Category/新泽西网队球员.md "wikilink")

1.  [Pistons sign free-agent forward
    Hayes](http://sports.espn.go.com/nba/news/story?id=2978003). Posted
    August 16, 2007