**赋**，古代[文体名](../Page/文体.md "wikilink")，是一種押韻的文體，由[楚辭演變而來](../Page/楚辭.md "wikilink")。賦這種體制是較為特殊的。由外表看去，是非詩非文，而其內卻又有詩有文，可以說是一種半詩半文的混合體。

## 流變

《[毛诗序](../Page/毛诗序.md "wikilink")》把“赋”作为《[诗经](../Page/诗经.md "wikilink")》“[六义](../Page/六义.md "wikilink")”之一，解作「铺」的意思，指「铺陈言志」的手法。「赋」也有朗誦之意，如《[汉书](../Page/汉书.md "wikilink")·艺文志》“不歌而诵谓之赋。”

[战国后期](../Page/战国.md "wikilink")[赵人](../Page/赵.md "wikilink")[荀卿](../Page/荀卿.md "wikilink")《[赋篇](../Page/赋篇.md "wikilink")》，最早以“赋”名篇，“赋”开始被用作文体的名称。

汉代人看到了[戰國](../Page/戰國.md "wikilink")[屈原](../Page/屈原.md "wikilink")、[宋玉等楚國文人的](../Page/宋玉.md "wikilink")[楚辭與](../Page/楚辭.md "wikilink")[賦的密切关系](../Page/賦.md "wikilink")，把[楚辞和](../Page/楚辞.md "wikilink")[荀卿的赋统称为](../Page/荀卿.md "wikilink")“辞赋”，并把屈原看作“辞赋之祖”。

西漢[贾谊](../Page/贾谊.md "wikilink")、[枚乘](../Page/枚乘.md "wikilink")、[司马相如代表的](../Page/司马相如.md "wikilink")[汉赋](../Page/汉赋.md "wikilink")，多長篇鉅製，又稱「古賦」、「大賦」。

東漢[三國延續張衡以後的短小篇幅](../Page/三國.md "wikilink")，題材擴大了，也更富有抒情的成分。代表性作品是[曹植的](../Page/曹植.md "wikilink")《[洛神賦](../Page/洛神賦.md "wikilink")》、[王粲的](../Page/王粲.md "wikilink")《[登樓賦](../Page/登樓賦.md "wikilink")》。

[西晉時](../Page/西晉.md "wikilink")，[陸機的](../Page/陸機.md "wikilink")《[文賦](../Page/文賦.md "wikilink")》，是中國[文學批評的重要著作](../Page/文學批評.md "wikilink")，而[左思的](../Page/左思.md "wikilink")《[三都賦](../Page/三都賦.md "wikilink")》，一-{出}-現即造成了「洛陽紙貴」的效應。[向秀的](../Page/向秀.md "wikilink")《[思舊賦](../Page/思舊賦.md "wikilink")》為悼[嵇康而作](../Page/嵇康.md "wikilink")，深切而感人。到了[東晉](../Page/東晉.md "wikilink")，[陶淵明著名的](../Page/陶淵明.md "wikilink")《[歸去來辭](../Page/歸去來辭.md "wikilink")》是辭賦中的名篇，沒有半點雕琢，真實描繪了脫離官場、回歸田園懷抱的喜悅心境。

到了[齊](../Page/齊.md "wikilink")、[梁](../Page/梁.md "wikilink")，由於[沈約](../Page/沈約.md "wikilink")、[王融聲律學的鼓吹](../Page/王融.md "wikilink")，且駢文流行，因此出現[骈赋](../Page/骈赋.md "wikilink")。代表作品有[庾信](../Page/庾信.md "wikilink")《[哀江南賦](../Page/哀江南賦.md "wikilink")》、《[小園賦](../Page/小園賦.md "wikilink")》、[鮑照](../Page/鮑照.md "wikilink")《[蕪城賦](../Page/蕪城賦.md "wikilink")》、[江淹](../Page/江淹.md "wikilink")《[別賦](../Page/別賦.md "wikilink")》。

[唐代盛行](../Page/唐代.md "wikilink")[科举考试专用的](../Page/科举.md "wikilink")[律赋](../Page/律赋.md "wikilink")，律賦過份注重音韻的協調，與[對偶的工整](../Page/對偶.md "wikilink")，文學價值不高。[中唐至](../Page/中唐.md "wikilink")[宋朝](../Page/宋朝.md "wikilink")[古文運動興起](../Page/古文運動.md "wikilink")，則趋向散文化的「文赋」，又稱「散賦」，比較有文學價值，文賦的特點是：廢棄駢律的限制，駢散結合，形成一種自由的體裁。精彩的作品有[杜牧的](../Page/杜牧.md "wikilink")《[阿房宮賦](../Page/阿房宮賦.md "wikilink")》、[歐陽脩的](../Page/歐陽脩.md "wikilink")《[秋聲賦](../Page/秋聲賦.md "wikilink")》、[蘇軾的前後](../Page/蘇軾.md "wikilink")《[赤壁賦](../Page/赤壁賦.md "wikilink")》。

明代[八股文盛行](../Page/八股文.md "wikilink")，賦又發展出了「股賦」。

## 解析

汉初，逐渐形成一种特定的体制。它继承《[楚辞](../Page/楚辞.md "wikilink")》形式上一些特点，讲究[文采](../Page/文采.md "wikilink")、[韵律和](../Page/韵律.md "wikilink")[节奏](../Page/节奏.md "wikilink")，又吸收了战国[纵横家铺张的手法](../Page/纵横家.md "wikilink")\[1\]，内容上着力“体物”，也注意到“写志”，即通过摹写事物来抒发情志。\[2\]此外，趋于[散文化](../Page/散文.md "wikilink")，经常使用[排比](../Page/排比.md "wikilink")、[对偶的整齐](../Page/对偶.md "wikilink")[句法](../Page/句法.md "wikilink")，既自由又谨严，兼具[诗歌和散文的性质](../Page/诗歌.md "wikilink")。

古人寫賦大都是虛構。漢朝的賦更是充滿[神話色彩](../Page/神話.md "wikilink")，[桓谭的](../Page/桓谭.md "wikilink")《仙赋》对[神仙的生活进行了生动的描述](../Page/神仙.md "wikilink")：“夫[王乔](../Page/王子喬.md "wikilink")[赤松](../Page/赤松子.md "wikilink")，呼则出故，翕则纳新。天矫经引，积气关元。精神周洽，鬲塞流通。乘凌虚无，洞达幽明。诸物皆见，玉女在旁。仙道既成，神灵攸迎。乃骖驾青龙……”。[刘歆的](../Page/刘歆.md "wikilink")《甘泉宫赋》描写：“回天门而凤举，蹑[黄帝之明庭](../Page/黄帝.md "wikilink")。冠高山而为居，乘昆仑而为宫。按轩辕之旧处，居[北辰之闳中](../Page/北辰.md "wikilink")。背[共工之](../Page/共工.md "wikilink")[幽都](../Page/幽都.md "wikilink")，向[炎帝之](../Page/炎帝.md "wikilink")[祝融](../Page/祝融.md "wikilink")。”[张衡的](../Page/张衡.md "wikilink")《西京赋》稱“清渊洋洋，神山峨峨。列瀛洲与方丈，夹蓬莱而骈罗”。[司马相如曾言](../Page/司马相如.md "wikilink")：“合纂组以成文，列绵绣而为质，一经一纬，一宫一商，此赋之迹也。赋家之心，包括宇宙，总揽人物，斯乃得之于内，不可得而传。”\[3\][姚鼐稱](../Page/姚鼐.md "wikilink")《大人赋》说：“此赋多取于《远游》。《远游》先访中国仙人之居，乃至天帝之宫，又下周览天地之间，自于微闾以下，分东西南北四段。此赋自横厉飞泉以正东以下，分东西南北四段，而求仙人之居，意即载其间。末六句与《远游》语同，然屈子意，在远去世之沈浊。”\[4\]。汉赋的构篇方式還有一個特點是一問一答，所謂“遂客主以首引，极声貌以穷文”。

[杜牧的](../Page/杜牧.md "wikilink")《[阿房宫赋](../Page/阿房宫赋.md "wikilink")》即通过夸张的手法来展现[阿房宫](../Page/阿房宫.md "wikilink")，甚至說“一日之内，一宫之间，而气候不齐。”。[鲍照的](../Page/鲍照.md "wikilink")《鹤舞赋》中认为[鹤是](../Page/鹤.md "wikilink")[胎生](../Page/胎生.md "wikilink")。《[日知录](../Page/日知录.md "wikilink")》卷十九云：“古人为赋，多假设之辞，序述往事，以为点缀，不必一一符同也。”[钱鍾書亦表示](../Page/钱鍾書.md "wikilink")“词章凭空，异乎文献征信，未宜刻舟求剑”。\[5\]

## 注釋

## 參考文獻

  - 康达维：〈[论赋体的源流](http://www.nssd.org/articles/article_read.aspx?id=1002687232)〉。
  - 苏瑞隆：〈[汉魏六朝俳谐赋初探](http://www.nssd.org/articles/article_read.aspx?id=35470872)〉。
  - 苏瑞隆：〈[魏晋六朝赋中戏剧型式对话的转变](http://www.nssd.org/articles/article_read.aspx?id=1003277259)〉。
  - 稻畑耕一郎：〈[赋的小品化初探（下）——赋的表现论之一](http://www.nssd.org/articles/article_read.aspx?id=662405816)〉。

[赋](../Category/赋.md "wikilink")
[Category:韻文](../Category/韻文.md "wikilink")

1.  章学诚《文史通义·诗教上》：“京都诸赋。苏张纵横六国，侈陈形势之遗也；《上林》、《羽猎》，安陵之从田，龙阳之同钓也；《客难》、《解嘲》，屈原之《渔父》、《卜居》。庄周、惠施问难也……孟子问齐王之大欲，历举轻暖肥甘，声音采色，《七林》之所启也，而或以为创之枚乘，忘其祖矣。”
2.  《史通·杂说篇》曰：“自战国以下，词人属文，皆伪立主客，假相酬答。至于屈原《离骚》辞，称遇渔父于江诸；宋玉《高唐赋》，云梦神女于阳台。夫言并文章，句结音韵，以兹叙事，足验凭虚。……采为逸事，编诸史籍，疑误后学，不其甚耶？”
3.  《西京杂记》
4.  《史记会注考证·司马相如传》
5.  《管锥编》（中华书局1979年版）第1296\~1297页