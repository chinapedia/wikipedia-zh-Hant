**Celeron
M**，属于[赛扬](../Page/赛扬.md "wikilink")[处理器家族的移动处理器系列](../Page/处理器.md "wikilink")。具体说来，采用Northwood之后核心，到目前为止的移动版赛扬处理器都属于Celeron
M。

## 诞生

正如赛扬处理器的存在一般，最早的Celeron M处理器基于当时的[Pentium
M经过一些](../Page/Pentium_M.md "wikilink")“精简”而来。
常见的手段包括但不限于：缩减二级缓存，取消节电技术等。

## 历代核心

### Banias

由同期的*Banias* [Pentium M改造而来](../Page/Pentium_M.md "wikilink")。

  - L1缓存：32 + 32 KB（数据＋指令）
  - L2缓存：512KB
  - 插座：[Socket 479](../Page/Socket_479.md "wikilink")
  - 发布日期：2004年，1月
  - 制程：130 nm
  - 最大消耗功率：5～21W
  - 型号：
      - 310：
      - 320：
      - 330：
      - 340：

### Dothan

由同期的*Dothan* [Pentium M改造而来](../Page/Pentium_M.md "wikilink")。

  - L1缓存：32 + 32 KB（数据＋指令）
  - L2缓存：512KB \~ 1024KB
  - 插座：[Socket 479](../Page/Socket_479.md "wikilink")
  - 发布日期：2004年，6月
  - 制程：90 nm
  - 最大消耗功率：5.5～27W
  - 型号：

390 ，380 ，370 ， 360J°，360 ，350J°，350, 353

### Yonah

由同期的*Yonah* [Intel Core改造而来](../Page/Intel_Core.md "wikilink")。

  - L1缓存：32 + 32 KB（数据＋指令）
  - L2缓存：1024KB
  - 插座：[Socket 479](../Page/Socket_479.md "wikilink")
  - 发布日期：2006年，4月
  - 制程：65 nm
  - 最大消耗功率：5.5～27W
  - 型号：
      - 410：
      - 420：
      - 430：
      - 440：
      - 450：

### Merom

由同期的*Merom* [Intel Core
2改造而来](../Page/Intel_Core_2.md "wikilink")。*Merom* Core
2本身支持[EM64T](../Page/EM64T.md "wikilink")，Intel保持这一特性，使得这核心的Celeron
M成为第一款支持64位技术的移动赛扬处理器。

  - L1缓存：32 + 32 KB（数据＋指令）
  - L2缓存：1024KB
  - 插座：[Socket 479](../Page/Socket_479.md "wikilink")
  - 发布日期：2007年，1月
  - 制程：65 nm
  - 最大消耗功率：30W
  - 型号：
      - 520：
      - 530：
      - 540：
      - 550：

## 命名

同一时期，桌面版本的赛扬处理器推出新系列[Celeron
D](../Page/Celeron_D.md "wikilink")。揣测Intel的意图，**Celeron**应当区分桌面产品和移动产品，即分别对应的**Celeron
D**与**Celeron M**

## 参见

  - [Celeron](../Page/Celeron.md "wikilink")
  - [Pentium M](../Page/Pentium_M.md "wikilink")
  - [Core](../Page/Intel_Core.md "wikilink")
  - [Core 2](../Page/Intel_Core_2.md "wikilink")
  - [迅驰](../Page/迅驰.md "wikilink")

## 外部链接

  - [产品概略](http://www.intel.com/products/processor/celeron_m/index.htm)

[Category:Intel x86处理器](../Category/Intel_x86处理器.md "wikilink")