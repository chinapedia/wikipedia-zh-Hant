**赞斯塔德**（）是[荷兰](../Page/荷兰.md "wikilink")[北荷兰省的一个基层政权](../Page/北荷兰省.md "wikilink")，人口约14万（2007年）。主要的镇为[赞丹](../Page/赞丹.md "wikilink")（Zaandam）。

[Category:北荷兰省基层政权](../Category/北荷兰省基层政权.md "wikilink")
[Category:北荷兰省城镇和村庄](../Category/北荷兰省城镇和村庄.md "wikilink")