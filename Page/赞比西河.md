**赞比西河**（）是[非洲第四长的](../Page/非洲.md "wikilink")[河流](../Page/河流.md "wikilink")，也是从非洲大陆流入[印度洋的最长河流](../Page/印度洋.md "wikilink")，当地[方言意为](../Page/方言.md "wikilink")“巨大的河流”。

赞比西河是非洲南部的大河，源于[赞比亚西北部](../Page/赞比亚.md "wikilink")，距[刚果河的源头约](../Page/刚果河.md "wikilink")1千米；河流上游有一部分經過[安哥拉](../Page/安哥拉.md "wikilink")，但大部分位於赞比亚境内海拔约1000米的[巴罗茨高原](../Page/巴罗茨高原.md "wikilink")，经过[纳米比亚及](../Page/纳米比亚.md "wikilink")[博茨瓦纳的边境后](../Page/博茨瓦纳.md "wikilink")，到达长130多千米的[巴托卡峡谷](../Page/巴托卡峡谷.md "wikilink")（Batoka
Canyon）时形成了著名的[维多利亚瀑布](../Page/维多利亚瀑布.md "wikilink")，然后沿赞比亚与[津巴布韦的边境线冲过](../Page/津巴布韦.md "wikilink")[卡-{里}-巴峡谷](../Page/卡里巴峡谷.md "wikilink")（Kariba
Canyon）进入[莫桑比克境内的平原](../Page/莫桑比克.md "wikilink")，水流开始趋缓，成为非洲东南部主要的[航运河道](../Page/河運.md "wikilink")，最后在[欣代附近注入](../Page/欣代.md "wikilink")[莫桑比克海峡](../Page/莫桑比克海峡.md "wikilink")。

赞比西河全长2574千米，流域面积139万平方千米，年均流量立方米/秒，干流河道中共有72处[瀑布](../Page/瀑布.md "wikilink")，河流水力的总蕴藏量约1.37亿千瓦。

## 經濟

尚比西河谷大約有3200萬人。約八成的居民仰賴[農業為生](../Page/農業.md "wikilink")，上游的氾濫平原提供了適於農耕的土地。

[贊比西河](../Category/贊比西河.md "wikilink")
[Category:非洲跨國河流](../Category/非洲跨國河流.md "wikilink")
[Category:纳米比亚河流](../Category/纳米比亚河流.md "wikilink")
[Category:莫三比克海峽](../Category/莫三比克海峽.md "wikilink")
[Category:納米比亞-贊比亞邊界](../Category/納米比亞-贊比亞邊界.md "wikilink")
[Category:波札那-贊比亞邊界](../Category/波札那-贊比亞邊界.md "wikilink")
[Category:贊比亞-津巴布韋邊界](../Category/贊比亞-津巴布韋邊界.md "wikilink")