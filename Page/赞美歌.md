《**赞美歌**》（），亦称《**祖国之神**》（Ó Guð vors
lands）、《**千年颂**》，是[冰岛共和国的](../Page/冰岛共和国.md "wikilink")[国歌](../Page/国歌.md "wikilink")，是1874年纪念冰岛被发现一千周年而创作的。[马提亚斯·尤库姆松](../Page/马提亚斯·尤库姆松.md "wikilink")（Matthías
Jochumsson）作词，[斯魏因比昂·斯魏因比昂逊](../Page/斯魏因比昂·斯魏因比昂逊.md "wikilink")（Sveinbjörn
Sveinbjörnsson）作曲。歌词原有三节，但现在只有一节被唱奏。

## 歌词

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/冰岛语.md" title="wikilink">冰岛语</a></p></th>
<th><p>译文</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt></dt>
<dd>Ó, guð vors lands! Ó, lands vors guð!
</dd>
<dd>Vér lofum þitt heilaga, heilaga nafn!
</dd>
<dd>Úr sólkerfum himnanna hnýta þér krans
</dd>
<dd>þínir herskarar, tímanna safn.
</dd>
<dd>Fyrir þér er einn dagur sem þúsund ár
</dd>
<dd>og þúsund ár dagur, ei meir:
</dd>
<dd>eitt eilífðar smáblóm með titrandi tár,
</dd>
<dd>sem tilbiður guð sinn og deyr.
</dd>
<dd>Íslands þúsund ár,
</dd>
<dd>Íslands þúsund ár,
</dd>
<dd>eitt eilífðar smáblóm með titrandi tár,
</dd>
<dd>sem tilbiður guð sinn og deyr.
</dd>
</dl></td>
<td><dl>
<dt></dt>
<dd>啊，岛屿之神，啊，神的岛屿，
</dd>
<dd>你崇高的名字为我们所颂赞。
</dd>
<dd>在久远的年代里，
</dd>
<dd>你的子孙把太阳镶上你的王冠。
</dd>
<dd>对于你一天就是一千年，
</dd>
<dd>一千年就是一天。
</dd>
<dd>啊，永恒的花，含着虔诚的泪，
</dd>
<dd>恭恭敬敬辞别人间。
</dd>
<dd>冰岛已千年，
</dd>
<dd>冰岛已千年，
</dd>
<dd>啊，永恒的花，含着虔诚的泪，
</dd>
<dd>恭恭敬敬辞别人间。
</dd>
</dl></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [The Icelandic National Anthem](http://www.musik.is/Lof/E/lofe.html)

## 参见

  - [冰岛文化](../Page/冰岛文化.md "wikilink")
  - [冰岛国旗](../Page/冰岛国旗.md "wikilink")
  - [冰岛国徽](../Page/冰岛国徽.md "wikilink")

{{-}}

[Category:冰岛](../Category/冰岛.md "wikilink")
[Category:国歌](../Category/国歌.md "wikilink")