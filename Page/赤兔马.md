[Guan_yu_-Summer_Palace,_Beijing.JPG](https://zh.wikipedia.org/wiki/File:Guan_yu_-Summer_Palace,_Beijing.JPG "fig:Guan_yu_-Summer_Palace,_Beijing.JPG")。\]\]
[黃龜理「關聖帝君」木雕.jpg](https://zh.wikipedia.org/wiki/File:黃龜理「關聖帝君」木雕.jpg "fig:黃龜理「關聖帝君」木雕.jpg")「關聖帝君」木雕，騎乘赤兔馬、手持[青龍偃月刀的關羽](../Page/青龍偃月刀.md "wikilink")。\]\]
**赤兔马**是《[三国志](../Page/三国志.md "wikilink")》以及《[三国演义](../Page/三国演义.md "wikilink")》中登场的良[馬](../Page/馬.md "wikilink")，有“人中[吕布](../Page/吕布.md "wikilink")，马中赤兔”\[1\]之称。

依《三国志·吕布传》，吕布“有良马曰赤兔”，当吕布在[袁绍的手下时](../Page/袁绍.md "wikilink")，被託进攻[张燕](../Page/張飛燕.md "wikilink")。吕布骑着赤兔良马衝入敌阵，击败了张燕。自從呂布被逮以後，再也沒赤兔馬的消息了。

在《三国演义》中，是与西方交易得来、能夠一日千里的罕见[汗血马](../Page/汗血马.md "wikilink")，原屬[董卓所有](../Page/董卓.md "wikilink")，董卓遣人送予呂布，慫恿呂布殺了其義父[丁原投靠董卓](../Page/丁原.md "wikilink")，之後呂布在[徐州戰敗](../Page/徐州.md "wikilink")，為[曹操所殺](../Page/曹操.md "wikilink")，赤兔馬便為曹操所得。之後曹操進攻取得徐州的[劉備](../Page/劉備.md "wikilink")，劉備戰敗，劉備手下大將[關羽為曹操所擒](../Page/關羽.md "wikilink")，曹操為了要使關羽加入，將赤兔馬轉送關羽。後來關羽敗走[麥城](../Page/麥城.md "wikilink")，被[潘璋部將](../Page/潘璋.md "wikilink")[馬忠生擒](../Page/馬忠_\(東吳\).md "wikilink")，為[呂蒙所殺](../Page/呂蒙.md "wikilink")，赤兔馬被賜予馬忠，遂絕食而死。

依[長沙](../Page/長沙.md "wikilink")[馬王堆三號漢墓出土](../Page/馬王堆.md "wikilink")[帛書](../Page/帛書.md "wikilink")《相馬經》：「得兔與狐，鳥與魚，得此四物，毋相其餘。」「欲得兔之頭與其肩，欲得狐之周草與其耳，……欲得鳥目與頸膺，欲得魚之鰭與脊。」可知兔頭、兔肩為[漢代相名馬的標準之一](../Page/漢代.md "wikilink")，赤兔馬是達到此標準的馬匹，非單一馬的名稱，與擁有鳥目的白鶴、白鵠、白鴿
及有狐耳的黃耳情形相同。

赤兔另一義為傳說中一種紅毛兔的[瑞獸](../Page/瑞獸.md "wikilink")。[唐朝](../Page/唐朝.md "wikilink")[徐堅](../Page/徐堅.md "wikilink")《[初學記](../Page/初學記.md "wikilink")》·卷二十九·兔第十二·敘事：「瑞應圖曰：赤兔者瑞獸，王者盛德則至。」

还有一说：赤兔乃赤菟之误，“菟”者，[虎也](../Page/虎.md "wikilink")，[春秋時](../Page/春秋時期.md "wikilink")[楚人称虎为](../Page/楚國.md "wikilink")“[於菟](../Page/於菟.md "wikilink")”。

小說《[水浒传](../Page/水浒传.md "wikilink")》裡也有赤兔马，关羽的后裔关胜骑一匹，反派角色钱振鹏骑卷毛赤兔马。

## 注释

<div class="references-small">

<references />

</div>

[RH](../Category/中國傳說生物.md "wikilink")
[Category:三國名馬](../Category/三國名馬.md "wikilink")

1.  [裴松之注](../Page/裴松之.md "wikilink")《三国志》，引《[曹瞒传](../Page/曹瞒传.md "wikilink")》：“时人语曰：‘人中有吕布，马中有赤兔。’”