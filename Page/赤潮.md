[La-Jolla-Red-Tide.780.jpg](https://zh.wikipedia.org/wiki/File:La-Jolla-Red-Tide.780.jpg "fig:La-Jolla-Red-Tide.780.jpg")
**赤潮**，又叫**紅潮**，是一种[水华现象](../Page/水华.md "wikilink")。它是[海洋灾害的一种](../Page/海洋灾害.md "wikilink")，是指[海洋水体中某些微小的](../Page/海洋.md "wikilink")[浮游植物](../Page/浮游植物.md "wikilink")、[原生动物或细菌](../Page/原生動物.md "wikilink")，在一定的环境条件下突发性增殖和聚集，引发一定范围和一段时间内水体变色现象。赤潮是一个历史沿用名詞，并不一定都是红色，而是许多類似現象的统称；发生赤潮时，通常根据引发赤潮的生物的数量、种类而使得海洋水体呈红、黄、绿和褐色等。

## 成因

赤潮是一种自然现象，也有人为因素引起的，但不一定是一种有害生态现象。引起赤潮的原因主要有：

  - 海域水体富营养化；有特殊物质作为[诱发因素](../Page/诱发因素.md "wikilink")，已知的有[维生素B](../Page/维生素.md "wikilink")1、B12、[铁](../Page/铁.md "wikilink")、[锰和](../Page/锰.md "wikilink")[脱氧核糖核酸](../Page/脱氧核糖核酸.md "wikilink")（DNA）；
  - 大量工农业或養豬業工廠排放大量豬糞[废水和生活污水排入海](../Page/废水.md "wikilink")，特别是未经处理直接排入而导致近海、港湾[富营养化程度日趋严重](../Page/富营养化.md "wikilink")
  - 海洋开发、[水產業带来了海洋](../Page/水產業.md "wikilink")[生态环境和](../Page/生态环境.md "wikilink")[养殖业自身](../Page/养殖业.md "wikilink")[污染问题](../Page/污染.md "wikilink")
  - 全球[海运业发展导致外来有害赤潮种类的引入](../Page/海运业.md "wikilink")
  - [气候变暖导致赤潮发生](../Page/气候变暖.md "wikilink")

## 生物類型

赤潮的生物類型頗為多樣，环境条件如水温、盐度等會决定赤潮的[生物类型](../Page/生物.md "wikilink")。引发赤潮的生物类型主要为[藻类](../Page/藻类.md "wikilink")，目前已发现有63种[浮游生物](../Page/浮游生物.md "wikilink")，[硅藻有](../Page/硅藻.md "wikilink")24种，[甲藻](../Page/甲藻.md "wikilink")32种、[蓝藻](../Page/蓝藻.md "wikilink")3种、金藻1种、隐藻2种、[原生动物](../Page/原生动物.md "wikilink")1种。

## 危害

有害赤潮发生后，导致海洋[食物链的局部中断](../Page/食物链.md "wikilink")；有些赤潮生物分泌[毒素被海洋食物链中的某些生物摄入](../Page/毒素.md "wikilink")，会导致[中毒甚至死亡](../Page/中毒.md "wikilink")。

  - 這些赤潮破坏海洋[生态结构](../Page/生态结构.md "wikilink")；
  - 這些赤潮生物的[分泌物妨碍海洋](../Page/分泌物.md "wikilink")[鱼类](../Page/鱼类.md "wikilink")、[虾类](../Page/虾.md "wikilink")、[贝类的正常](../Page/贝类.md "wikilink")[呼吸而导致窒息死亡](../Page/呼吸.md "wikilink")；
  - 含有毒素的赤潮生物被[海洋生物摄食后能引起中毒死亡](../Page/海洋生物.md "wikilink")；人类食用含有毒素的海产品也会导致食物中毒；
  - 這些赤潮生物死亡后尸骸的分解过程中要大量消耗海水中的溶解氧，造成缺氧环境，引起虾、贝类的大量死亡。

产生危害的主要方式是\[1\]：

  - 赤潮发生时，海水中高密度的赤潮生物覆盖或粘附在海洋动物的呼吸器官上，造成海洋动物呼吸困难和窒息死亡；
  - 大量赤潮生物的呼吸作用（尤其在夜间，无光合作用产生氧气）和死亡细胞分解过程中消耗海水中大量溶解氧，使水体严重缺氧，导致海洋生物死亡；
  - 赤潮衰败过程中还会释放出大量有害气体（如[硫化氫](../Page/硫化氫.md "wikilink")）和毒素，严重污染海洋环境，甚至导致海洋动物死亡；
  - 有的赤潮种类，如[杀鱼费氏藻](../Page/杀鱼费氏藻.md "wikilink")（學名：），不但会释放毒素毒害鱼类，而且会直接接触鱼体噬食鱼肉；
  - 有些赤潮生物体内含有鱼毒或贝毒，虽然对摄食它们的鱼类或贝类无害，但会在摄食者体内累积，使取食这些鱼类或贝类的海洋捕食者和人类发生中毒。目前已知的赤潮生物毒素有麻痹性贝毒（PSP）、神经性贝毒（NSP）、泻痢性贝毒（DSP）、失忆症贝毒（ASP）和雪茄鱼毒（CFP）等。

## 另見

  - [綠潮](../Page/綠潮.md "wikilink")

## 文內注釋

## 外部链接

  - [中国水产网](https://web.archive.org/web/20040523000435/http://www.china-fishery.net/)
  - [中国海洋信息网](https://web.archive.org/web/20040523232342/http://www.coi.gov.cn/)
  - [香港紅潮資訊網絡](http://www.hkredtide.org/)
  - [臺灣海洋神經毒](http://www.kmuh.org.tw/www/kmcj/data/8803/4187.htm)

[pl:Zakwit wody](../Page/pl:Zakwit_wody.md "wikilink")

[Category:环境保护](../Category/环境保护.md "wikilink")
[Category:海洋](../Category/海洋.md "wikilink")
[Category:生態學](../Category/生態學.md "wikilink")
[Category:藻类](../Category/藻类.md "wikilink")

1.