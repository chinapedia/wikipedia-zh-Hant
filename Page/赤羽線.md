[JR_East_E233-7000_Series_Saikyō_Line_at_Jūjō_Station.jpg](https://zh.wikipedia.org/wiki/File:JR_East_E233-7000_Series_Saikyō_Line_at_Jūjō_Station.jpg "fig:JR_East_E233-7000_Series_Saikyō_Line_at_Jūjō_Station.jpg")的[E233系7000番台電聯車](../Page/JR東日本E233系電力動車組.md "wikilink")（攝於2018年3月2日）\]\]

**赤羽線**（）是一條由[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）所經營、連結位於[東京都](../Page/東京都.md "wikilink")[豐島區的](../Page/豐島區.md "wikilink")與位於東京都[北區的](../Page/北區_\(東京都\).md "wikilink")，長度只有5.5公里的鐵路線。雖然距離很短，但赤羽線在[JR的鐵路系統中卻是屬於幹線等級的主要路線](../Page/JR.md "wikilink")。在營運系統上該線是[埼京線的一部份](../Page/埼京線.md "wikilink")，因此在包括列車時刻表之類的資料中，大都以埼京線稱呼之，很少會見到直接標示正式名稱「赤羽線」的情況。包括起訖車站在內，赤羽線全線都位於JR東日本的管轄範圍內。

## 概要

全線根據定為，也根據該規則包括在[大都市近郊區間的](../Page/大都市近郊區間_\(JR\).md "wikilink")「東京近郊區間」，與[IC](../Page/IC卡.md "wikilink")[乘車卡](../Page/乘車卡.md "wikilink")「[Suica](../Page/Suica.md "wikilink")」的首都圈地域。

## 路線資料

  - 管轄（事業類別）：東日本旅客鐵道（[第一種鐵道事業者](../Page/鐵路公司.md "wikilink")）

  - 路段（）：池袋－赤羽 5.5公里

  - 軌距：1067毫米

  - 站數：4個（包括起終點站）

      - 若只限屬於赤羽線的車站，排除屬於山手線的池袋站與屬於東北本線的赤羽站\[1\]則為2個。

  - [複線路段](../Page/複線鐵路.md "wikilink")：全線

  - 電氣化路段：全線（直流1500V）

  - 運行方式：[ATC方式](../Page/閉塞_\(鐵路\)#列車間の間隔を確保する装置による方法.md "wikilink")

  - 保安裝置：[ATC-6](../Page/列車自動控制系統#ATC-6形.md "wikilink")

  - 最高速度：90公里/小時

  - ：東京綜合指令室

  - ：[東京圈輸送管理系統](../Page/東京圈輸送管理系統.md "wikilink")（ATOS）

全線由管轄。

## 車站列表

  - [特定都區市內制度適用範圍車站](../Page/特定都區市內.md "wikilink")：[JR_area_YAMA.svg](https://zh.wikipedia.org/wiki/File:JR_area_YAMA.svg "fig:JR_area_YAMA.svg")\]：[東京山手線內](../Page/東京山手線內.md "wikilink")、[JR_area_KU.svg](https://zh.wikipedia.org/wiki/File:JR_area_KU.svg "fig:JR_area_KU.svg")\]：東京都區內
  - 所有車站均位於[東京都內](../Page/東京都.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>中文站名</p></th>
<th><p>日文站名</p></th>
<th><p>英文站名</p></th>
<th><p>站間營業距離</p></th>
<th><p>累計營業距離</p></th>
<th><p>所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:JR_area_YAMA.svg" title="fig:JR_area_YAMA.svg">JR_area_YAMA.svg</a>]<a href="https://zh.wikipedia.org/wiki/File:JR_area_KU.svg" title="fig:JR_area_KU.svg">JR_area_KU.svg</a>]</p></td>
<td><p><a href="../Page/池袋站.md" title="wikilink">池袋</a></p></td>
<td></td>
<td></td>
<td><p>-</p></td>
<td><p>0.0</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:JR_area_KU.svg" title="fig:JR_area_KU.svg">JR_area_KU.svg</a>]</p></td>
<td><p><a href="../Page/板橋站_(日本).md" title="wikilink">板橋</a></p></td>
<td></td>
<td></td>
<td><p>1.8</p></td>
<td><p>1.8</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:JR_area_KU.svg" title="fig:JR_area_KU.svg">JR_area_KU.svg</a>]</p></td>
<td><p><a href="../Page/十條站_(東京都).md" title="wikilink">十條</a></p></td>
<td></td>
<td></td>
<td><p>1.7</p></td>
<td><p>3.5</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:JR_area_KU.svg" title="fig:JR_area_KU.svg">JR_area_KU.svg</a>]</p></td>
<td><p><a href="../Page/赤羽站.md" title="wikilink">赤羽</a></p></td>
<td></td>
<td></td>
<td><p>2.0</p></td>
<td><p>5.5</p></td>
</tr>
</tbody>
</table>

### 廢站

  - [十條站](../Page/十條站_\(東京都\)#歷史.md "wikilink")（初代）：1906年廢除，池袋站起計3.0公里附近\[2\]。

## 參考資料

[Category:東日本旅客鐵道路線](../Category/東日本旅客鐵道路線.md "wikilink")
[Category:日本國有鐵道路線](../Category/日本國有鐵道路線.md "wikilink")
[Category:關東地方鐵路路線](../Category/關東地方鐵路路線.md "wikilink")
[Category:1885年啟用的鐵路線](../Category/1885年啟用的鐵路線.md "wikilink")
[Category:日本鐵道](../Category/日本鐵道.md "wikilink")

1.  『停車場変遷大事典 国鉄・JR編』[JTB](../Page/ジェイティービー.md "wikilink") 1998年 ISBN
    978-4533029806

2.