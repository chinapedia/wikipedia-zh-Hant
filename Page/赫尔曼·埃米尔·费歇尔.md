**赫尔曼·埃米尔·费歇尔**（，），[德国](../Page/德国.md "wikilink")[有机化学家](../Page/有机化学.md "wikilink")。他合成了[苯肼](../Page/苯肼.md "wikilink")，引入[肼类作为研究](../Page/肼类.md "wikilink")[糖类结构的有力手段](../Page/糖类.md "wikilink"),并合成了多种糖类,在理论上搞清了[葡萄糖的结构](../Page/葡萄糖.md "wikilink")，总结阐述了糖类普遍具有的立体异构现象，用[费歇尔投影式描述之](../Page/费歇尔投影式.md "wikilink")。他确定了[咖啡因](../Page/咖啡因.md "wikilink")、[茶碱](../Page/茶碱.md "wikilink")、[尿酸等物质都是](../Page/尿酸.md "wikilink")[嘌呤的衍生物](../Page/嘌呤.md "wikilink")，合成了嘌呤。他开拓了对蛋白质的研究，确定了[氨基酸通过](../Page/氨基酸.md "wikilink")[肽键形成](../Page/肽键.md "wikilink")[多肽](../Page/多肽.md "wikilink")，并成功合成了多肽。1902年他费歇尔因对嘌呤和糖类的合成研究被授予[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。

## 童年与教育

费歇尔出生在德国[科隆地区的](../Page/科隆.md "wikilink")[奥伊斯基兴小镇](../Page/奥伊斯基兴.md "wikilink")，父亲是一个商人。以优异成绩从中学毕业后，他本想进入大学学习[自然科学特别是物理学](../Page/自然科学.md "wikilink")，但他的父亲强迫他从事家族生意，直到确定他的儿子不合适经商，不得不说“这个孩子太蠢成不了商人，只能去读书”\[1\]。费歇尔从而进入[波恩大学学习](../Page/波恩大学.md "wikilink")[化学](../Page/化学.md "wikilink")，曾经上过[凯库勒等人的化学课程](../Page/凯库勒.md "wikilink")。1872年他转学到德国在[阿尔萨斯-洛林地区建立的威廉皇帝大学](../Page/阿尔萨斯-洛林.md "wikilink")（现今的[斯特拉斯堡大学](../Page/斯特拉斯堡大学.md "wikilink")，目前该大学[化学系的一间阶梯教室以](../Page/化学.md "wikilink")[费歇尔命名](../Page/费歇尔.md "wikilink")）以求继续学习物理，却在[阿道夫·冯·拜尔的影响下](../Page/阿道夫·冯·拜尔.md "wikilink")，决定终生从事[化学](../Page/化学.md "wikilink")。1874年以对[荧光黄和](../Page/荧光黄.md "wikilink")[酚酞染料的的性质进行研究获得](../Page/酚酞.md "wikilink")[博士学位](../Page/博士.md "wikilink")，并被任命为助理讲师。

## 研究工作

### 肼类与嘌呤研究

[Hermann_Emil_Fischer2.jpg](https://zh.wikipedia.org/wiki/File:Hermann_Emil_Fischer2.jpg "fig:Hermann_Emil_Fischer2.jpg")
在斯特拉斯堡的临时讲师任上，费歇尔作出了自己在化学上的第一个重要发现，使用[亚硫酸盐还原](../Page/亚硫酸盐.md "wikilink")[重氮苯](../Page/重氮苯.md "wikilink")，合成了[苯肼](../Page/苯肼.md "wikilink")（C<sub>6</sub>H<sub>5</sub>NHNH<sub>2</sub>）。\[2\]以[苯肼为起点](../Page/苯肼.md "wikilink")，他和他的表弟奥托·费歇尔一起研究[肼类的性质](../Page/肼.md "wikilink")，他们提出了从[三苯甲烷生产](../Page/三苯甲烷.md "wikilink")[染料的新合成路线](../Page/染料.md "wikilink")，并通过实验证明了这一方法的正确。1875年[阿道夫·冯·拜尔被邀请前往](../Page/阿道夫·冯·拜尔.md "wikilink")[慕尼黑大学接替](../Page/慕尼黑大学.md "wikilink")1873年去世的[李比希留下的化学系教授的教职](../Page/李比希.md "wikilink")，费歇尔跟随[阿道夫·冯·拜尔前往](../Page/阿道夫·冯·拜尔.md "wikilink")，成为[阿道夫·冯·拜尔在有机化学研究上的一名助手](../Page/阿道夫·冯·拜尔.md "wikilink")。

[Purine_chemical_structure.png](https://zh.wikipedia.org/wiki/File:Purine_chemical_structure.png "fig:Purine_chemical_structure.png")
1879年费歇尔被慕尼黑大学任命为[分析化学的副教授](../Page/分析化学.md "wikilink")，并拒绝了来自[亚琛工业大学的担任化学系主任的邀请](../Page/亚琛工业大学.md "wikilink")。1881年他被[埃尔朗根-纽伦堡大学任命为正教授](../Page/埃尔朗根-纽伦堡大学.md "wikilink")，对[茶叶](../Page/茶叶.md "wikilink")、[咖啡和](../Page/咖啡.md "wikilink")[可可等饮料的组分进行研究](../Page/可可.md "wikilink")，分离并分析了茶碱、咖啡因和[可可碱等](../Page/可可碱.md "wikilink")，进一步阐明了这些化合物和[尿酸都是一个杂环化合物的衍生物](../Page/尿酸.md "wikilink")\[3\]。这个化合物便是[嘌呤](../Page/嘌呤.md "wikilink")，是由一个[嘧啶环和一个](../Page/嘧啶.md "wikilink")[咪唑环杂合的杂环化合物](../Page/咪唑.md "wikilink")，是重要的代谢物之一。

### 糖类和蛋白质研究

1883年他接受[巴登](../Page/巴登.md "wikilink")[苯胺](../Page/苯胺.md "wikilink")[苏打厂](../Page/苏打.md "wikilink")（[巴斯夫股份公司的前身](../Page/巴斯夫.md "wikilink")）的邀请，前往担任其实验室负责人。期间他开始了对[糖类的研究](../Page/糖类.md "wikilink")。1880年以前，人们已经测出葡萄糖的化学式是C<sub>6</sub>H<sub>12</sub>O<sub>6</sub>，并通过[葡萄糖可以发生](../Page/葡萄糖.md "wikilink")[银镜反应和](../Page/银镜反应.md "wikilink")[裴林反应推测葡萄糖中存在](../Page/裴林反应.md "wikilink")[醛基](../Page/醛基.md "wikilink")。费歇尔结合前人的成就和自己对肼类的研究进行了大量的实验。他首先研究了葡萄糖的性质，如葡萄糖被氧化为[葡萄糖酸](../Page/葡萄糖酸.md "wikilink")，葡萄糖被还原为[醇](../Page/醇.md "wikilink")，糖类与苯肼的反应形成[苯腙和](../Page/苯腙.md "wikilink")[脎](../Page/脎.md "wikilink")，后者成为确定糖类的特征鉴别反应。

1888年到1892年他成为[维尔茨堡大学化学系教授](../Page/维尔茨堡大学.md "wikilink")，这是他觉得很快乐的一段时间，他喜欢去附近的[黑森林散步](../Page/黑森林.md "wikilink")，对其中生长的[地衣进行了研究](../Page/地衣.md "wikilink")，这一阶段他最大的贡献是提出了有机化学中描述立体构型的重要方法—费歇尔投影式，竖直线代表远离观察者的[化学键](../Page/化学键.md "wikilink")，水平线代表朝向观察者的化学键，这样将三维结构的分子用二维形式表达出来，使得研究者便于互相交流。\[4\]1892年他接替刚刚去世的[奥古斯特·威廉·冯·霍夫曼任](../Page/奥古斯特·威廉·冯·霍夫曼.md "wikilink")[柏林大学化学系主任一直到](../Page/柏林大学.md "wikilink")1919年去世。在柏林，费歇尔总结当时所有已知糖的立体构型他接受了[雅各布斯·亨里克斯·范托夫的葡萄糖中存在四个](../Page/雅各布斯·亨里克斯·范托夫.md "wikilink")[手性碳原子的观点](../Page/手性碳原子.md "wikilink")，确定了葡萄糖的链状结构，并认为[葡萄糖应该有](../Page/葡萄糖.md "wikilink")2的四次方=16种立体异构体。并且自己合成了其中的[异葡萄糖](../Page/异葡萄糖.md "wikilink")、[甘露糖和](../Page/甘露糖.md "wikilink")[伊杜糖](../Page/伊杜糖.md "wikilink")。

1899年到1908年费歇尔对[蛋白质的组成和性质进行了开创性的研究](../Page/蛋白质.md "wikilink")。在费歇尔之前，李比希等人试图像小分子一样用简单的化学式来描述蛋白质，但遇到了困难。费歇尔首先提出[氨基酸通过](../Page/氨基酸.md "wikilink")[肽键](../Page/肽键.md "wikilink")(-CONH-)结合所形成的[多肽](../Page/多肽.md "wikilink")，[多肽正是蛋白质的](../Page/多肽.md "wikilink")[水解产物](../Page/水解.md "wikilink")。在实验上，费歇尔改进了测试[氨基酸的办法](../Page/氨基酸.md "wikilink")，发现了新的环状氨基酸[脯氨酸和氧脯氨酸](../Page/脯氨酸.md "wikilink")。他还尝试使用光反应来让氨基酸合成蛋白质。并合成了二肽，三肽和多肽（含18个氨基酸\[5\]）。给后来[桑格等人对蛋白质结构的进一步研究奠定了方法基础](../Page/桑格.md "wikilink")。

费歇尔的后半生得到了很多荣誉。他是[剑桥大学](../Page/剑桥大学.md "wikilink")、[曼彻斯特大学和](../Page/曼彻斯特大学.md "wikilink")[布鲁塞尔自由大学的](../Page/布鲁塞尔自由大学.md "wikilink")[荣誉博士](../Page/荣誉博士.md "wikilink")。他还荣获[普鲁士秩序勋章和](../Page/普鲁士秩序勋章.md "wikilink")[马克西米利安艺术和科学勋章](../Page/马克西米利安艺术和科学勋章.md "wikilink")。在1902年，他因对糖和嘌呤的合成被授予[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。但生活是悲惨的，他的一个儿子在[第一次世界大战中阵亡](../Page/第一次世界大战.md "wikilink")。另一个儿子在25岁时因忍受不了征兵的严厉训练而自杀。费歇尔因此陷入抑郁之中，并于1919年在柏林[自杀](../Page/自杀.md "wikilink")。\[6\]\[7\]费歇尔的长子\[8\]
[Hermann Otto Laurenz
Fischer在](../Page/:en:Hermann_Otto_Laurenz_Fischer.md "wikilink")[加州大学伯克利分校任教](../Page/加州大学伯克利分校.md "wikilink")，从1948年直到他于1960年逝世，他对[有机化学和](../Page/有机化学.md "wikilink")[生物化学有一定贡献](../Page/生物化学.md "wikilink")。\[9\]

## 以费歇尔命名的贡献

  - [费歇尔吲哚合成](../Page/费歇尔吲哚合成.md "wikilink")
  - [费歇尔投影式](../Page/费歇尔投影式.md "wikilink")
  - [费歇尔恶唑合成](../Page/费歇尔恶唑合成.md "wikilink")
  - [费歇尔肽合成](../Page/费歇尔肽合成.md "wikilink")
  - [Fischer糖苷化反应](../Page/Fischer糖苷化反应.md "wikilink")
  - [Fischer酯化反应](../Page/Fischer酯化反应.md "wikilink")

## 参考文献

## 延伸阅读

1.  John Hudson，The Hsitory of Chemistry，Chapman & Hall，New York 1992.
    ISBN 0-412-03641-X
2.  《中国大百科全书·化学卷》费舍尔条目。
3.  [诺贝尔官方网站关于赫尔曼·埃米尔·费歇尔传记](http://nobelprize.org/nobel_prizes/chemistry/laureates/1902/fischer-bio.html)

[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:德国生物化学家](../Category/德国生物化学家.md "wikilink")
[Category:有機化學家](../Category/有機化學家.md "wikilink")
[Category:柏林洪堡大學教師](../Category/柏林洪堡大學教師.md "wikilink")
[Category:維爾茨堡大學教師](../Category/維爾茨堡大學教師.md "wikilink")
[Category:埃爾朗根-紐倫堡大學教師](../Category/埃爾朗根-紐倫堡大學教師.md "wikilink")
[Category:慕尼黑大學教師](../Category/慕尼黑大學教師.md "wikilink")
[Category:斯特拉斯堡大學校友](../Category/斯特拉斯堡大學校友.md "wikilink")
[Category:波恩大學校友](../Category/波恩大學校友.md "wikilink")
[Category:德國自殺者](../Category/德國自殺者.md "wikilink")
[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")
[Category:丹麦皇家科学院院士](../Category/丹麦皇家科学院院士.md "wikilink")
[Category:自殺科學家](../Category/自殺科學家.md "wikilink")
[Category:立体化学家](../Category/立体化学家.md "wikilink")
[Category:戴维奖章](../Category/戴维奖章.md "wikilink")

1.  [诺贝尔奖官方网页](http://nobelprize.org/nobel_prizes/chemistry/laureates/1902/fischer-bio.html)
2.  Fischer, E. "Ueber aromatische Hydrazinverbindungen" Ber. Dtsch.
    Chem. Ges., **1875**, band 8, 589-594.
3.  E. Fischer, Ber. Dtsch. Chem. Ges. 1890, 23, 799 ± 805.
4.  Smith March, March’s Advanced Organic Chemistry, 5th
    Edition，2003，ISBN 0-471-58589-0
5.  《中国大百科全书·化学卷》费舍尔条目
6.
7.
8.  Poster next to bust of Fischer, Biosciences Library, UC Berkeley
9.