**赫尔穆特·科尔**（，），[德国政治家](../Page/德国.md "wikilink")。曾任[莱茵兰-普法尔茨州州长](../Page/莱茵兰-普法尔茨.md "wikilink")（1969年－1976年），[德国基督教民主联盟主席](../Page/德国基督教民主联盟.md "wikilink")（1973年－1998年），[德国总理](../Page/德国总理.md "wikilink")（1982年－1998年，1990年后统治全德）。

科尔在[两德统一的进程起到关键作用](../Page/两德统一.md "wikilink")，同时也对[欧洲一体化进程作出了很多贡献](../Page/欧洲一体化.md "wikilink")。但由於晚年捲入非法[政治獻金醜聞](../Page/政治獻金.md "wikilink")，使其歷史評價尚有爭議。\[1\]

## 生平

[Bundesarchiv_Bild_183-P0819-306,_Leipzig,_Besuch_Helmut_Kohl_mit_Familie.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-P0819-306,_Leipzig,_Besuch_Helmut_Kohl_mit_Familie.jpg "fig:Bundesarchiv_Bild_183-P0819-306,_Leipzig,_Besuch_Helmut_Kohl_mit_Familie.jpg")市區逛街，\]\]
赫尔穆特·科尔的父亲汉斯·科尔（1887年出生——1975年）是[巴伐利亚自由州州财政部的一名公务员](../Page/巴伐利亚自由州.md "wikilink")，母亲為Cäcilie（1890年出生——1979年）。整个家庭信奉[罗马天主教](../Page/罗马天主教.md "wikilink")。科尔本人是全家的第三个孩子，他有个哥哥在[第二次世界大战時喪生](../Page/第二次世界大战.md "wikilink")。在战争即将结束的时候，科尔加入了空军防空志愿组织，未参加战鬥。

他的幼年时期几乎是在[路德维希港市的Hohenzollernstraße大街度过的](../Page/路德维希港.md "wikilink")。他在那里就读小学（Ruprechtschule）之后进入[马克斯—普朗克—文理高中就读](../Page/马克斯—普朗克—文理高中.md "wikilink")。

1950年科尔来到[法兰克福上大学](../Page/法兰克福.md "wikilink")，学习[法学](../Page/法学.md "wikilink")。1951年他转入[海德堡大学](../Page/海德堡大学.md "wikilink")，学习[历史](../Page/历史.md "wikilink")、[社会科学](../Page/社会科学.md "wikilink")。

1956年毕业后，科尔到[海德堡大学的](../Page/海德堡大学.md "wikilink")[阿尔弗雷德—韦伯—研究院](../Page/阿尔弗雷德—韦伯—研究院.md "wikilink")（Alfred-Weber-Institut）做一名助理。1958年，他和Walther
Peter Fuchs（1905年出生——1997年）共同研究的論题《1945年后普法尔茨的政治发展、产生的党派（Die politische
Entwicklung in der Pfalz und das Wiedererstehen der Parteien nach
1945）》，讓科尔获得哲学博士之學位，而他随后进入[路德维希港钢铁铸造厂的管理层](../Page/路德维希港.md "wikilink")，担任助理职务。1959年任[路德维希港化学工业协会的负责人职务](../Page/路德维希港化学工业协会.md "wikilink")。这段时间里，科尔与认识於1948年的外语秘书Hannelore
Renner（1933年—2001年）结婚，婚后育有两个儿子:[瓦尔特·科尔](../Page/瓦尔特·科尔.md "wikilink")（1963年出生）與[皮特·科尔](../Page/皮特·科尔.md "wikilink")（1965年出生）。

## 政治生涯

1946年当科尔还是学生时就已加入[基民盟](../Page/基民盟.md "wikilink")，不久之后的1947年他还在家乡[路德维希港建立了](../Page/路德维希港.md "wikilink")[德国青年联盟](../Page/德国青年联盟.md "wikilink")（Jungen
Union——JN）。在他读大学时，便开始參與许多政治活动。1953年他成为[莱茵兰-普法尔茨州基民盟经济工商业协会会员](../Page/莱茵兰-普法尔茨.md "wikilink")，1954年担任[莱茵兰-普法尔茨州青年联盟](../Page/莱茵兰-普法尔茨州.md "wikilink")（Jungen
Union）代理主席，1955年成为[莱茵兰-普法尔茨州](../Page/莱茵兰-普法尔茨州.md "wikilink")[基民盟高层成员](../Page/基民盟.md "wikilink")。1959年担任[路德维希港地区基民盟主席](../Page/路德维希港.md "wikilink")，1960年——1969年成为[基民盟联邦高层成员](../Page/基民盟.md "wikilink")，1969年担任[基民盟联邦代理主席](../Page/基民盟.md "wikilink")。这其中对[赫尔穆特·科尔提供资助的有依靠](../Page/赫尔穆特·科尔.md "wikilink")[二战](../Page/二战.md "wikilink")、[纳粹积累起财富的工业资本家](../Page/纳粹党.md "wikilink")[弗里茨·瑞斯](../Page/弗里茨·瑞斯.md "wikilink")（Fritz
Ries）等。

### 担任州长

[Bundesarchiv_B_145_Bild-F028914-0003,_Ludwigshafen,_CDU-Kongress,_Helmut_Kohl.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_B_145_Bild-F028914-0003,_Ludwigshafen,_CDU-Kongress,_Helmut_Kohl.jpg "fig:Bundesarchiv_B_145_Bild-F028914-0003,_Ludwigshafen,_CDU-Kongress,_Helmut_Kohl.jpg")
当科尔在1966年被选为[莱茵兰-普法尔茨](../Page/莱茵兰-普法尔茨.md "wikilink")[基民盟主席后](../Page/基民盟.md "wikilink")，他被确定为時任州长Peter
Altmeiers的继任者。在州选举结束后，结果Altmeiers被再次选为州长，但是在1969年5月19日，科尔接替他当选州长。在科尔执政期最大的施政决定是：实行区域改革、建立了**特里尔——凯撒斯劳滕大学**（即：今天的[特里尔大学](../Page/特里尔大学.md "wikilink")、[凯撒斯劳滕工业大学](../Page/凯撒斯劳滕工业大学.md "wikilink")）。

1971年科尔开始竞选基民盟主席，可惜败给了当时的Rainer Barzel。1973年就在Rainer
Barzel对当时现任[总理](../Page/总理.md "wikilink")[维利·勃兰特进行不信任案投票仅仅一年后](../Page/维利·勃兰特.md "wikilink")，科尔取代Barzel成为基民盟主席，并一直将任职期长達25年——直至1998年11月7日。

### 反对党领袖

在1976年联邦议会选举中，科尔首次被推举为基民盟总理候选人。虽然在这次选举中，[基民盟/基社盟联盟以](../Page/基民盟/基社盟.md "wikilink")48.6%成为第一大党，但以微弱劣势败给由[社民黨及](../Page/社民黨.md "wikilink")[自民黨的](../Page/自民黨.md "wikilink")[社会自由联盟](../Page/社会自由联盟.md "wikilink")（Sozialliberale
Koalition），但这个大选结果却是[基民盟/基社盟截至目前第二高的投票记录](../Page/基民盟/基社盟.md "wikilink")。大选后，科尔继续担任州长，同时在位于[波恩的](../Page/波恩.md "wikilink")[德国联邦议院担任](../Page/德国联邦议院.md "wikilink")[基联盟/基社盟组成的反对党领袖](../Page/基联盟/基社盟.md "wikilink")。1976年12月2日Bernhard
Vogel成为他的继任者，担任[莱茵兰-普法尔茨州长](../Page/莱茵兰-普法尔茨.md "wikilink")。当大选失败后，[基社盟决定取消与](../Page/基社盟.md "wikilink")[基民盟的联盟政策](../Page/基民盟.md "wikilink")，但科尔反对基社盟主席[弗朗茨·约瑟夫·施特劳斯这项舉措](../Page/弗朗茨·约瑟夫·施特劳斯.md "wikilink")。在1980年进行的联邦大选中，科尔必须支持[施特劳斯競选联邦总理](../Page/施特劳斯.md "wikilink")，因为大选后科尔將会继续担任反对党领袖，而施特劳斯回到[巴伐利亚担任州长](../Page/巴伐利亚.md "wikilink")。而後科尔从1976年—2002年一直担任[德国联邦议会议员](../Page/德国联邦议会.md "wikilink")。

### 西德总理

#### 解散联邦议会及第一个任期

1982年9月17日，时任德国总理的[赫尔穆特·施密特政府](../Page/赫尔穆特·施密特.md "wikilink")（社会自由联盟）分裂之后，其主要源自于社民党及自民党对未来德国联邦的经济政策产生了严重的意见分歧。9月20日，[自民党同基联盟](../Page/德国自由民主党.md "wikilink")/基社盟讨论组成新的中右翼联盟。这次诱因是由于[自民党](../Page/自民党.md "wikilink")（FDP）内部成员Otto
Graf
Lambsdorff修改的一份战略草案中包含有关于改革[就业市场的](../Page/就业市场.md "wikilink")[新自由主义计划](../Page/新自由主义.md "wikilink")。（详见：→
[改变](http://de.wikipedia.org/wiki/Wende_\(Bundesrepublik_Deutschland\))）

1982年10月1日，联邦议会历史上第一次通过有计划的[不信任动议将时任第六届德国总理](../Page/不信任动议.md "wikilink")[赫尔穆特·施密特趕下台](../Page/赫尔穆特·施密特.md "wikilink")，同时科尔被提名为德国联邦总理。[德国联邦外交部长的人选同之前的社会](../Page/德国联邦外交部长.md "wikilink")—自由—联盟内阁相同，由Hans-Dietrich
Genscher（FDP）继续担任。在[自民党内部](../Page/自民党.md "wikilink")，很多人对这次联盟倒戈非常有异议。因为在1980年举行大选时自民党承诺组成共同联盟对[社民党有益](../Page/社民党.md "wikilink")。但在这件事情上，被指责缺少材料性的证明，但这一点在形式上却是符合宪法。此外，因为[科尔的总理身份并非由正常的联邦议会选举产生](../Page/科尔.md "wikilink")，所以为了去除掉这一缺点，科尔实施了一个十分有争议的处理措施：在联邦议会中，在年12月17日表决的对他內閣的信任投票。当執政联盟通过《1983年联邦预算案》，他们也将得到与政府联盟代表协商后的多数票，因此即将到来所希望的结果是：如果联邦总理没有得到多数票，将建议[联邦总统解散议会](../Page/德国联邦总统.md "wikilink")。在经过长时间的踌躇之后，联邦总统[卡尔·卡斯滕斯决定在](../Page/卡尔·卡斯滕斯.md "wikilink")1983年1月解散[联邦议会](../Page/德國联邦议会.md "wikilink")，并於3月6日进行大选。与此同时，一些议员代表对此上诉到联邦宪法法庭。但是，这个解散联邦议会的决定符合宪法的规定。

#### 第二個任期

[Bundesarchiv_B_145_Bild-F065160-0031,_Bonn,_CDU-Bundestagswahlparty,_Kohl,_Geißler.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_B_145_Bild-F065160-0031,_Bonn,_CDU-Bundestagswahlparty,_Kohl,_Geißler.jpg "fig:Bundesarchiv_B_145_Bild-F065160-0031,_Bonn,_CDU-Bundestagswahlparty,_Kohl,_Geißler.jpg")
[Photograph_of_the_G-7_Economic_Summit_in_Williamsburg,_Virginia_(left_to_right)_Pierre_Trudeau,_Gaston_Thorn,_Helmut..._-_NARA_-_198538.jpg](https://zh.wikipedia.org/wiki/File:Photograph_of_the_G-7_Economic_Summit_in_Williamsburg,_Virginia_\(left_to_right\)_Pierre_Trudeau,_Gaston_Thorn,_Helmut..._-_NARA_-_198538.jpg "fig:Photograph_of_the_G-7_Economic_Summit_in_Williamsburg,_Virginia_(left_to_right)_Pierre_Trudeau,_Gaston_Thorn,_Helmut..._-_NARA_-_198538.jpg")峰會\]\]
[Bundesarchiv_B_145_Bild-F073617-0004,_Mainz,_CDU-Bundesparteitag,_Kohl.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_B_145_Bild-F073617-0004,_Mainz,_CDU-Bundesparteitag,_Kohl.jpg "fig:Bundesarchiv_B_145_Bild-F073617-0004,_Mainz,_CDU-Bundesparteitag,_Kohl.jpg")
在1983年3月6日举行的联邦大选中，由[基民盟](../Page/基民盟.md "wikilink")、[基社盟和](../Page/基社盟.md "wikilink")[自民党组成的联盟获得了联邦议会中的多数席位](../Page/自民党.md "wikilink")，其中[基民盟/基社盟得票](../Page/基民盟/基社盟.md "wikilink")（48.8%，增加4.2%），自民党得票（7%，减少3.6%）。[赫尔穆特·科尔显示出最佳的竞选结果](../Page/赫尔穆特·科尔.md "wikilink")（基民盟历史上第二好的得票记录）。[社民党方面](../Page/社民党.md "wikilink")，竞选对手是曾经担任[司法部部长和](../Page/德国司法部部长.md "wikilink")[慕尼黑市市长Hans](../Page/慕尼黑.md "wikilink")-Jochen
Vogel。

[社民党因结束与自民党的联盟关系](../Page/社民党.md "wikilink")，雖然已有预期的落败，但当时剛成立不久的左翼[环保政党](../Page/綠色政治.md "wikilink")[德國綠党跨越](../Page/德國綠党.md "wikilink")5%得票門鑑進入联邦议会，此后的联邦和州议会選舉，基民盟/基社盟联盟与自民党结合成[中間偏右联盟](../Page/中間偏右.md "wikilink")，而社民党则和綠党结合成[中間偏左联盟](../Page/中間偏左.md "wikilink")，維持至今。

第一年的总理任期中，科尔排除了一切[和平运动所带来的阻力](../Page/和平运动.md "wikilink")，坚决实施了上届[施密特政府确定的](../Page/施密特.md "wikilink")《[北约革新决议](../Page/北约革新决议.md "wikilink")》。

1984年9月22日科尔同[法国总统](../Page/法国总统.md "wikilink")[弗朗索瓦·密特朗在](../Page/弗朗索瓦·密特朗.md "wikilink")[凡尔登战役纪念遗址会面](../Page/凡尔登战役.md "wikilink")，共同纪念双方在两次世界大战中死去的人们。[科尔和](../Page/科尔.md "wikilink")[密特朗的那张数分钟牽手的照片被视作](../Page/密特朗.md "wikilink")[德](../Page/德.md "wikilink")[法两国关系调和的标志](../Page/法.md "wikilink")。儘管政治立场相左，但这之后的几年中，科尔同密特朗被形容为拥有非常紧密的关系。他们共同将：《[欧洲合同军](../Page/欧洲合同军.md "wikilink")》（Eurokorps）和[ARTE电视台等项目带入轨道](../Page/ARTE.md "wikilink")，以及经德法双方共同协作，缔结了对欧洲一体化进程起到进步作用的《[马斯特里赫特条约](../Page/马斯特里赫特条约.md "wikilink")》和之后引入[欧元的政策](../Page/欧元.md "wikilink")。

由于科尔也卷入了[弗利克](../Page/弗利克.md "wikilink")[康采恩集团向德国政治家秘密设置的账户中非法汇款的](../Page/康采恩.md "wikilink")[弗利克丑闻案](../Page/弗利克丑闻.md "wikilink")。在这之后，科尔对由[联邦议会和](../Page/联邦议会.md "wikilink")[美因茨市议会组成的调查小组叙述此事](../Page/美因茨.md "wikilink")，认为這是Staatsbürgerlichen
Vereinigung[基联盟内部协会捐赠以采购设备](../Page/基联盟.md "wikilink")。最终，科尔仅仅因一个由[奥托·席利提出上诉的](../Page/奥托·席利.md "wikilink")[虚假陈述罪名被传唤](../Page/虚假陈述.md "wikilink")。不久[科尔的党内朋友Heiner](../Page/科尔.md "wikilink")
Geißler帮助他，在公开场合说：“他有个幸福的低潮。”（Er habe wohl einen Blackout gehabt.）

1984年1月24日，科尔在[以色列国会引用](../Page/以色列国会.md "wikilink")[Günter
Gaus的一句话](../Page/Günter_Gaus.md "wikilink")「宽恕这些后代。」（Gnade der
späten Geburt.）。

1985年5月5日，科尔与[美国总统](../Page/美国总统.md "wikilink")[罗纳德·里根一同向位于](../Page/罗纳德·里根.md "wikilink")[Bitburg的士兵墓地敬献花圈](../Page/Bitburg.md "wikilink")。之后在[德国](../Page/德国.md "wikilink")、[美国部分公共场合激烈讨论了这次鲜花的行为](../Page/美国.md "wikilink")，因为那里还同时埋葬有[武裝親衛隊的成员](../Page/武裝親衛隊.md "wikilink")。

[ReaganBerlinWall.jpg](https://zh.wikipedia.org/wiki/File:ReaganBerlinWall.jpg "fig:ReaganBerlinWall.jpg")[羅納德雷根在](../Page/羅納德雷根.md "wikilink")[勃兰登堡门前演講](../Page/勃兰登堡门.md "wikilink")，時任德國總理[科爾在後旁](../Page/科爾.md "wikilink")。\]\]
[Helmut_Kohl_in_Krzyzowa.jpg](https://zh.wikipedia.org/wiki/File:Helmut_Kohl_in_Krzyzowa.jpg "fig:Helmut_Kohl_in_Krzyzowa.jpg")城市Krzyżowa，在柏林牆倒塌前。\]\]
在1987年的联邦大选中，[科尔第二度连任](../Page/科尔.md "wikilink")。社民党的总理候选人是[北莱茵-威斯特法伦州长](../Page/北莱茵-威斯特法伦.md "wikilink")[约翰内斯·劳](../Page/约翰内斯·劳.md "wikilink")。

#### 第三個任期：统一德国的总理

[KohlModrowMomperBrandenburgerTor.jpg](https://zh.wikipedia.org/wiki/File:KohlModrowMomperBrandenburgerTor.jpg "fig:KohlModrowMomperBrandenburgerTor.jpg")前。\]\]
[Bundesarchiv_B_145_Bild-F086820-0012,_Bonn,_Bundespräsident_ernennt_Bundeskanzler_Kohl.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_B_145_Bild-F086820-0012,_Bonn,_Bundespräsident_ernennt_Bundeskanzler_Kohl.jpg "fig:Bundesarchiv_B_145_Bild-F086820-0012,_Bonn,_Bundespräsident_ernennt_Bundeskanzler_Kohl.jpg")\]\]
当[东德的局势越来越显示出恶化以及](../Page/东德.md "wikilink")[柏林墙在](../Page/柏林墙.md "wikilink")1989年11月9日倒塌，在未与任何联盟成员和西方盟国成员提前商议的情况下，1989年11月[赫尔穆特·科尔在联邦议会上提出了关于破除目前处于分裂中的德国](../Page/赫尔穆特·科尔.md "wikilink")、欧洲的《[十点计划](../Page/十点计划.md "wikilink")》（Zehn-Punkte-Programm）。在1990年5月18日与[东德签署了关于货币](../Page/东德.md "wikilink")、经济和社会联盟的政府条约。针对[联邦银行主席Karl](../Page/联邦银行.md "wikilink")_Otto_Pöhl的阻力，[科尔通过在工资](../Page/科尔.md "wikilink")、收入、房租以及退休金领域使[东德马克按照](../Page/东德马克.md "wikilink")1:1的比例兑换[西德马克](../Page/德国马克.md "wikilink")。不久之后，由于这一政策的推出，被证明东德企业承受了沉重的负担。然后科尔同外交部长Hans-Dietrich
Genscher一同合作与二战四大战胜国进行了[二加四会谈](../Page/二加四会谈.md "wikilink")（Zwei-plus-Vier-Gesprächen），最终达成了在《[二加四条约](../Page/最终解决德国问题条约.md "wikilink")》形式下实现[两德统一](../Page/两德统一.md "wikilink")、[北约驻扎在统一后的](../Page/北约.md "wikilink")[德国](../Page/德国.md "wikilink")。

[德国的统一对科尔之后的总理轨迹起到积极的影响](../Page/德国.md "wikilink")。因为1989年[科尔于](../Page/科尔.md "wikilink")[不来梅召开的党代会成功将党内](../Page/不来梅.md "wikilink")“异己”排除，例如Heiner
Geißler、Rita Süssmuth、Lothar Späth等。

### 德国統一后的首任总理

#### 第四个任期

[Bundesarchiv_Bild_183-1990-0916-021,_Dresden,_CDU-Wahlveranstaltung,_Helmut_Kohl.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-1990-0916-021,_Dresden,_CDU-Wahlveranstaltung,_Helmut_Kohl.jpg "fig:Bundesarchiv_Bild_183-1990-0916-021,_Dresden,_CDU-Wahlveranstaltung,_Helmut_Kohl.jpg")
1990年底举行的联邦议会大选，科尔战胜当年的社民党候选人[萨尔州州长](../Page/萨尔.md "wikilink")[奥斯卡·拉方丹](../Page/奥斯卡·拉方丹.md "wikilink")，第三次当选德国总理。他也同时成为[兩德統一后的第一任总理](../Page/兩德統一.md "wikilink")。这次選舉亦是德國自1933年來，首次全国自由選舉。为確保勝出大選，这次選舉按原來东、西德分开選舉。

#### 第五个任期

1994年联邦议会大选，科尔以微弱优势再度当选联邦总理，这一次他战胜了时任[莱茵-普法尔茨州州长的社民党总理候选人](../Page/莱茵-普法尔茨.md "wikilink")[鲁道夫·沙尔平](../Page/鲁道夫·沙尔平.md "wikilink")。他成為自[奥托·冯·俾斯麦以來德國在任最久的总理](../Page/奥托·冯·俾斯麦.md "wikilink")。在之后的几年中，德国在外交事务中获得了空前成功（如：[法兰克福成为](../Page/法兰克福.md "wikilink")[欧洲中央银行的总部](../Page/欧洲中央银行.md "wikilink")，引入[欧元](../Page/欧元.md "wikilink")）。後來由于社民党控制的[德国联邦参议院限制了联邦政府的执政效力](../Page/德国联邦参议院.md "wikilink")，使得在内政上显示出一个低潮，并因此在1998年联邦议会大选中落败。

[Helmut_Kohl_und_William_Cohen.jpg](https://zh.wikipedia.org/wiki/File:Helmut_Kohl_und_William_Cohen.jpg "fig:Helmut_Kohl_und_William_Cohen.jpg")
[27_ClintonKohl_Bachhaus.jpg](https://zh.wikipedia.org/wiki/File:27_ClintonKohl_Bachhaus.jpg "fig:27_ClintonKohl_Bachhaus.jpg")[比爾·克林頓會面](../Page/比爾·克林頓.md "wikilink")\]\]
1998年联邦议会大選，社民党及綠党的[中間偏左聯盟赢得大选](../Page/中間偏左.md "wikilink")，时任[下萨克森州州长](../Page/下萨克森州.md "wikilink")[格哈特·施罗德当选为新一任联邦总理](../Page/格哈特·施罗德.md "wikilink")，結束科尔的[中間偏右聯盟長達](../Page/中間偏右.md "wikilink")16年的管治。10月26日科爾向联邦总统[罗曼·赫尔佐克辭去总理](../Page/罗曼·赫尔佐克.md "wikilink")。10月27日正式卸任。

### 讽刺、调侃和模仿

德国的所有政治家中，还没有一人像赫尔穆特·科尔这样多的在政治、非政治领域被讽刺、调侃过。这其中以[德国讽刺类杂志](../Page/德国.md "wikilink")[Titanic](http://de.wikipedia.org/wiki/Titanic_\(Magazin\))所树立的讽刺角色最为长久，杂志形容总理“[鸭梨](../Page/鸭梨.md "wikilink")”状的脸型如同以前法国国王[路易·菲利浦一样](../Page/路易-菲利普一世.md "wikilink")，之后“鸭梨”一词成为了科尔的标志。

媒体常常瞄准他一般的外语水平、特大的身材和他来自的小城市出身。科尔的模仿者经常使用他带有普法尔茨色彩的[口音](../Page/口音_\(社會語言學\).md "wikilink")，如将"Geschichte"，读成
"Gechichte"。

## 晚年

2001年7月5日，科尔68岁的妻子[汉内洛蕾·科尔自杀身亡](../Page/汉内洛蕾·科尔.md "wikilink")。自杀原因据推测可能由于多年患有[光照性皮炎](../Page/光照性皮炎.md "wikilink")。

2004年3月4日，正巧在他离开总理职位的5年半的那天，赫尔穆特·科尔推出了《回忆录》的第一部分，记录“1930年——1982年”，在这里详细阐述了从1930年开始直到他第一任总理任期开始前的事情。2005年11月3日出版的第二部分，是关于从1982年到1990年的执政期。2007年11月16日出版是直到1994年的执政期。目前，第四部正在筹划中。

在2007年的春天和秋天科尔動了两次大的膝盖手术，换上人造关节。2008年秋天在家中严重摔伤，造成颅脑外伤。从此在公开场合出现时，他都使用轮椅。对于摔伤事故后的艰难时期，他说：“如果没有我的太太，我现在不会仍然活着，如果她未曾陪伴在我的身边，这会是我生命中的重大损失。”

2008年5月8日在海德堡大学医院的一间小礼堂，科尔同他在2005年正式确定关系的女友Maike
Richter（1964年出生）结婚。男伴郎中有德国著名的媒体集团总裁[Leo
Kirch](../Page/Leo_Kirch.md "wikilink")、[图片报总编](../Page/图片报.md "wikilink")[Kai
Diekmann](../Page/Kai_Diekmann.md "wikilink")。婚后Maike
Richter改名为Kohl-Richter，她是1994年—1998年间在[德國总理府的经济部任职期间同科尔结识的](../Page/德國总理府.md "wikilink")。在1998年[格哈特·施罗德大选获胜后](../Page/格哈特·施罗德.md "wikilink")，她转为担任CDU的经济顾问。由于她曾经担任[德国经济周刊的记者](../Page/德国经济周刊.md "wikilink")，Kohl-Richter目前作为领导任职于[德国联邦经济科技部](../Page/德国联邦经济科技部.md "wikilink")（Bundesministerium
für Wirtschaft und Technologie）地区经济政策、城市建设、管理处。

2011年初，柯爾之子[華特·科爾出版了關於其父親的自傳](../Page/華特·科爾.md "wikilink")，於書中指出其母親晚年因患憂鬱症而常有輕生念頭，並多次強調科爾對於家庭漠不關心，最終憤而與父親斷絕父子關係。他在書中強調「他（柯爾）真正的家是基民黨，他娶的不是我媽而是黨」。柯爾慣例從不對其子的書籍有任何表態，評論家曾說，柯爾常會逃避和延遲做重大決定。

赫尔穆特·科尔居住在首都[柏林和路德维希港的Oggersheim区](../Page/柏林.md "wikilink")。

2017年6月16日星期五早上，赫尔穆特·科尔在家中過世，享年八十七歲。\[2\]\[3\]

## 科尔內閣

### 第一届政府内阁

1982年10月4日－1983年3月29日

  - 赫尔穆特·科尔 ([CDU](../Page/德国基督教民主联盟.md "wikilink")) - 联邦总理

  - [汉斯·迪特里希·根舍](../Page/汉斯·迪特里希·根舍.md "wikilink")
    ([FDP](../Page/德国自由民主党.md "wikilink")) - 副总理兼外交部长

  - (CDU) - 国防部长

  - Friedrich Zimmermann ([CSU](../Page/巴伐利亚基督教社会联盟.md "wikilink")) -
    内务部长

  - (CDU) - 财政部长

  - Hans Engelhard (FDP) - 司法部长

  - (FDP) - 经济部长

  - (CDU) - 劳动和社会事务部长

  - Josef Ertl (FDP) - 食品、农业和森林部长

  - Werner Dollinger (CSU) - 交通部长

  - Oscar Schneider (CSU) - 建设部长

  - (CDU) - 青年、家庭和健康部长

  - Heinz Riesenhuber (CDU) - 研究和技术部长

  - Dorothee Wilms (CDU) - 教育科学部长

  - Jürgen Warnke (CSU) - 经济合作部长

  - Christian Schwarz-Schilling (CDU) - 邮政和通讯部长

  - (CDU) - 国内关系部长

### 第二届政府内阁

1983年3月29日－1987年3月11日

  - 赫尔穆特·科尔 (CDU) - 联邦总理

  - [汉斯·迪特里希·根舍](../Page/汉斯·迪特里希·根舍.md "wikilink") (FDP) -副总理兼外交部长

  - (CDU) - 国防部长

  - Friedrich Zimmermann (CSU) - 内务部长

  - (CDU) - 财政部长

  - Hans Engelhard (FDP) - 司法部长

  - (FDP) -经济部长

  - (CDU) - 劳动和社会事务部长

  - Ignaz Kiechle (CSU) - 食品、农业和森林部长

  - Werner Dollinger (CSU) - 交通部长

  - Oscar Schneider (CSU) - 建设部长

  - (CDU) - 青年、家庭和健康部长

  - Heinz Riesenhuber (CDU) -研究和技术部长

  - Dorothee Wilms (CDU) - 教育科学部长

  - Jürgen Warnke (CSU) - 经济合作部长

  - Christian Schwarz-Schilling (CDU) -邮政和通讯部长

  - Heinrich Windelen (CDU) - 国内关系部长

**人事变动**

  - 1984年6月27日 - Martin Bangemann (FDP) succeeds Lambsdorff as -经济部长
  - 1984年11月15日 - [沃尔夫冈·朔伊布勒](../Page/沃尔夫冈·朔伊布勒.md "wikilink") (CDU)
    进入内阁任特别事务部长
  - 1985年9月26日 - Rita Süssmuth (CDU)继Geissler 任青年、家庭和健康部长
  - 1986年6月6日 - Rita Süssmuth (CDU) 任青年、家庭、妇女和健康部长。Walter Wallmann
    (CDU)进入内阁任 环境、自然保护和核安全部长。

### 第三届政府内阁

1987年3月12日－1991年1月17日

  - 赫尔穆特·科尔(CDU) - 联邦总理

  - [汉斯·迪特里希·根舍](../Page/汉斯·迪特里希·根舍.md "wikilink") (FDP) - 副总理兼外交部长

  - (CDU) - 国防部长

  - Friedrich Zimmermann (CSU) -内务部长

  - (CDU) - 财政部长

  - Hans Engelhard (FDP) - 司法部长

  - Martin Bangemann (FDP) - 经济部长

  - (CDU) - 劳动和社会事务部长

  - Ignaz Kiechle (CSU) - 食品、农业和森林部长

  - Jürgen Warnke (CSU) - 交通部长

  - Oscar Schneider (CSU) - 建设部长

  - Rita Süssmuth (CDU) - 青年、家庭、妇女核健康部长

  - Heinz Riesenhuber (CDU) -研究和技术部长

  - (FDP) - 教育科学部长

  - Hans Klein (CSU) - 经济合作部长

  - Walter Wallmann (CDU) - 环境、自然保护和核安全部长

  - [沃尔夫冈·朔伊布勒](../Page/沃尔夫冈·朔伊布勒.md "wikilink") (CDU) - 特别事务部长

  - Christian Schwarz-Schilling (CDU) - 邮政和通讯部长

  - Dorothee Wilms (CDU) - 国内关系部长

**人事变动**

  - 1987年4月22日 - Klaus Töpfer (CDU) 接替 Wallmann 出任 环境、自然保护和核安全部长
  - 1988年5月18日 - Rupert Scholz (CDU) 接替Wörner任国防部长
  - 1988年12月9日- Helmut Haussmann (FDP) 接替Bangemann出任经济部长.
  - 1989年4月21日 -  (CDU) 接替Scholz 任国防部长. Theo Waigel
    (CSU)接替Stoltenberg任财政部长。 Jürgen Warnke (CSU) 接替Klein
    出任经济合作部长。Friedrich Zimmermann (CSU) 接替 Warnke出任交通部长。
    [Wolfgang Schäuble](../Page/Wolfgang_Schäuble.md "wikilink") (CDU)
    接替Zimmermann出任内务部长。Gerda Hasselfeldt (CSU)
    接替Schneider出任建设部长。Hans Klein (CSU) 和Rudolf Seiters
    (CDU) 出任特别事务部长。
  - 1990年10月3日 - 5位东德部长 - [洛塔尔·德梅齐埃](../Page/洛塔尔·德梅齐埃.md "wikilink")
    (CDU), [萨宾娜·伯格曼-普尔](../Page/萨宾娜·伯格曼-普尔.md "wikilink") (CDU), Günther
    Krause (CDU), Rainer Ortleb (FDP)和 Hans Joachim Walther
    ([DSU](../Page/German_Social_Union.md "wikilink")) -进入内阁任特别事务部长。
  - 1990年12月19日- De Maizière 离开此届内阁.

### 第四届政府内阁

1991年1月18日－1994年11月15日

  - 赫尔穆特·科尔 (CDU) - 联邦总理
  - [汉斯·迪特里希·根舍](../Page/汉斯·迪特里希·根舍.md "wikilink") (FDP) - 副总理兼外交部长
  - [Gerhard Stoltenberg](../Page/Gerhard_Stoltenberg.md "wikilink")
    (CDU) - 国防部长
  - [沃尔夫冈·朔伊布勒](../Page/沃尔夫冈·朔伊布勒.md "wikilink") (CDU) - 内务部长
  - Theo Waigel (CSU) - 财政部长
  - [Klaus Kinkel](../Page/Klaus_Kinkel.md "wikilink") (FDP) - 司法部长
  - [Jürgen Möllemann](../Page/Jürgen_Möllemann.md "wikilink") (FDP) -
    经济部长
  - [Norbert Blüm](../Page/Norbert_Blüm.md "wikilink") (CDU) - 劳动和社会事务部长
  - Ignaz Kiechle (CSU) - 食品、农业和森林部长
  - Günter Krause (CDU) - 交通部长
  - Irmgard Adam-Schwaetzer (CSU) - 建设部长
  - [Hannelore Rönsch](../Page/Hannelore_Rönsch.md "wikilink") (CDU) -
    Minister of Family and Senior Citizens
  - [安格拉·默克尔](../Page/安格拉·默克尔.md "wikilink") (CDU) - 妇女青年部长
  - Gerda Hasselfeldt (CDU) - 卫生部长
  - Heinz Riesenhuber (CDU) - 研究和技术部长
  - Rainer Ortleb (FDP) - 教育科学部长
  - Carl-Dieter Spranger (CSU) - 经济合作部长
  - Klaus Töpfer (CDU) -环境、自然保护和核安全部长
  - Rudolf Seiters (CDU) - 特别事务部长
  - Christian Schwarz-Schilling (CDU) -邮政和通讯部长

**人事变动**

  - 1991年11月26日 - Rudolf Seiters (CDU) 继Schäuble任内务部长。Friedrich Bohl
    (CDU)继Seiters任特别事务部长
  - 1992年 4月1日 - [Volker Rühe](../Page/Volker_Rühe.md "wikilink")
    (CDU)继Stoltenberg任国防部长
  - 1992年5月6日 - Horst Seehofer (CSU) 继Hasselfeldt 任卫生部长.
  - 1992年5月18日 - [Jürgen
    Möllemann](../Page/Jürgen_Möllemann.md "wikilink") (FDP)
    继Genscher任副总理兼经济部长 [Klaus
    Kinkel](../Page/Klaus_Kinkel.md "wikilink")
    (FDP)继Genscher任外交部长。Sabine Leutheusser-Schnarrenberger
    (FDP)继 Kinkel任司法部长。
  - 1992年12月17日 - Wolfgang Bötsch (CSU)继Schwarz-Schilling任邮政和通讯部长
  - 1993年1月21日 - [Klaus Kinkel](../Page/Klaus_Kinkel.md "wikilink")
    (FDP)继Möllemann任副总理兼外交部长。[Günter
    Rexrodt](../Page/Günter_Rexrodt.md "wikilink")
    (FDP)继Möllemann任经济部长。Jochen Borchert (CDU) 继Kiechle
    任食品、农业和森林部长。[Matthias
    Wissmann](../Page/Matthias_Wissmann.md "wikilink")
    (CDU)继Riesenhuber任教育科学部长 。Carl-Dieter Spranger
    (CSU)任经济合作和开发部长（原经济合作部长）。
  - 1993年5月13日 - [Matthias
    Wissmann](../Page/Matthias_Wissmann.md "wikilink") (CDU)
    继Krause任交通部长。Paul Krüger (CDU)继Wissmann任研究和技术部长。
  - 1993年7月7日 - Manfred Kanther (CDU)继Seiters任内务部长。
  - 1994年2月4日 - Karl-Hans Laermann (FDP)继Ortleb任教育科学部长。

### 第五届政府内阁

1994年11月15日－1998年10月27日

  - 赫尔穆特·科尔(CDU) - 总理
  - [Klaus Kinkel](../Page/Klaus_Kinkel.md "wikilink") (FDP) - 副总理兼外交部长
  - [Volker Rühe](../Page/Volker_Rühe.md "wikilink") (CDU) - 国防部长
  - Manfred Kanther (CDU) - 内务部长
  - Theo Waigel (CSU) - 财政部长
  - Sabine Leutheusser-Schnarrenberger (FDP) - 司法部长
  - [Günter Rexrodt](../Page/Günter_Rexrodt.md "wikilink") (FDP) - 经济部长
  - [Norbert Blüm](../Page/Norbert_Blüm.md "wikilink") (CDU) -劳动和社会事务部长
  - Jochen Borchert (CDU) - 食品、农业和森林部长
  - [Matthias Wissmann](../Page/Matthias_Wissmann.md "wikilink") (CDU) -
    交通部长
  - Klaus Töpfer (CDU) - 建设部长
  - Claudia Nolte (CDU) - Minister of Family, Senior Citizens, Women,
    and Youth
  - Horst Seehofer (CSU) - 卫生部长.
  - [Jürgen Rüttgers](../Page/Juergen_Ruettgers.md "wikilink") (CDU) -
    教育科学研究和技术部长
  - Carl-Dieter Spranger (CSU) - 经济合作和开发部长
  - [安格拉·默克尔](../Page/安格拉·默克尔.md "wikilink") (CDU) - 环境、自然保护和核安全部长
  - Friedrich Bohl (CDU) - 特别事务部长
  - Wolfgang Bötsch (CSU) - 邮政和通讯部长

**人事变动**

  - 1996年1月17日- Edzard Schmidt-Jortzig
    (FDP)继Leutheusser-Schnarrenberger任司法部长
  - 1997年 12月31日- 邮政和通讯部撤销
  - 1998年1月14日- Eduard Oswald 继 Töpfer 任建设部长

## 參考資料

<references />

[Category:德国总理](../Category/德国总理.md "wikilink")
[Category:冷戰人物](../Category/冷戰人物.md "wikilink")
[Category:海德堡大學校友](../Category/海德堡大學校友.md "wikilink")
[Category:法蘭克福大學校友](../Category/法蘭克福大學校友.md "wikilink")
[Category:萊因蘭-普法爾茲人](../Category/萊因蘭-普法爾茲人.md "wikilink")
[Category:查理曼獎得主](../Category/查理曼獎得主.md "wikilink")
[Category:德国天主教徒](../Category/德国天主教徒.md "wikilink")
[Category:德国第二次世界大战军事人物](../Category/德国第二次世界大战军事人物.md "wikilink")
[Category:德国反共主义者](../Category/德国反共主义者.md "wikilink")
[Category:圣米迦勒及圣乔治勋章荣誉爵级大十字勋章持有人](../Category/圣米迦勒及圣乔治勋章荣誉爵级大十字勋章持有人.md "wikilink")
[Category:总统自由勋章获得者](../Category/总统自由勋章获得者.md "wikilink")
[Category:欧洲理事会主席](../Category/欧洲理事会主席.md "wikilink")
[Category:伦敦荣誉市民](../Category/伦敦荣誉市民.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")

1.
2.
3.