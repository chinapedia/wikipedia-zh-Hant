[thumb創作於約公元前四世紀](../Page/圖像:Hercules_Farnese_3637104088_9c95d7fe3c_b.jpg.md "wikilink")，原作為[青銅像](../Page/青銅.md "wikilink")，羅馬[大理石](../Page/大理石.md "wikilink")[複製品稱為](../Page/複製品.md "wikilink")[休息的赫拉克勒斯](../Page/休息的赫拉克勒斯.md "wikilink")，現收藏於意大利[那不勒斯國立美術館](../Page/那不勒斯國立美術館.md "wikilink")\]\]

**赫拉克勒斯**（，，引申自「[赫拉](../Page/赫拉.md "wikilink")」和kleos「榮耀」，即赫拉克勒斯被稱為赫拉的榮耀\[1\]，[轉寫](../Page/羅馬化.md "wikilink")：[Heracles](../Page/Heracles.md "wikilink")，音譯**赫剌克勒斯**、**海格-{}-力斯**、**海克-{}-力士**），是[希腊神话最伟大的](../Page/希腊神话.md "wikilink")[半神](../Page/半神.md "wikilink")[英雄](../Page/英雄.md "wikilink")，男性的傑出典範，偉大的[赫拉克勒斯后裔祖先](../Page/赫拉克勒斯后裔.md "wikilink")。赫拉克勒斯相當于[罗马神话里的](../Page/罗马神话.md "wikilink")[赫丘利](../Page/赫丘利.md "wikilink")（海克力斯），後來的[羅馬皇帝](../Page/羅馬皇帝.md "wikilink")，比如[康茂德和](../Page/康茂德.md "wikilink")[馬克西米安都常常以其自居](../Page/馬克西米安.md "wikilink")。羅馬人基本採用希臘人對赫拉克勒斯生活和事蹟刻畫的版本，並零星的補充一些羅馬風格的細節，有些甚至將其和統一[地中海的英雄聯繫起來](../Page/地中海.md "wikilink")。在羅馬也曾大行對赫丘利的崇拜之風。

赫拉克勒斯是[宙斯诱姦](../Page/宙斯.md "wikilink")[珀耳修斯的孙女](../Page/珀耳修斯.md "wikilink")、[底比斯大將](../Page/底比斯.md "wikilink")[安菲特律翁之妻](../Page/安菲特律翁.md "wikilink")[阿尔克墨涅后](../Page/阿尔克墨涅.md "wikilink")，与安菲特律翁的儿子[伊菲克勒斯一同诞生的](../Page/伊菲克勒斯.md "wikilink")[双胞胎兄弟](../Page/双胞胎.md "wikilink")。因此他既是[珀耳修斯的](../Page/珀耳修斯.md "wikilink")[曾外孫又是他的同父異母兄弟](../Page/曾外孫.md "wikilink")。半人半神的他自幼在名师的传授下，学会了各种武艺和技能，能勇善战，成为众人皆知的大力士。

赫拉克勒斯不僅具有非凡的實力、[勇氣](../Page/勇氣.md "wikilink")、智慧、技能和技巧，在他的性格屬性中還同時具有男性和女性特徵。雖然他未必有[奧德修斯或](../Page/奧德修斯.md "wikilink")[內斯特那麼聰明](../Page/內斯特.md "wikilink")，赫拉克勒斯卻非常善於使用自己的智慧彌補其能力上的不足：比如將[安泰俄斯舉起以戰勝他](../Page/安泰俄斯.md "wikilink")，或是用一個小小的詭計讓[阿特拉斯將天空重新放回自己的肩膀](../Page/阿特拉斯.md "wikilink")。\[2\]他和[赫耳墨斯同為古希臘的](../Page/赫耳墨斯.md "wikilink")[體育館和](../Page/体育馆_\(古希腊\).md "wikilink")[角力學校的庇護人和保護者](../Page/角力學校.md "wikilink")。\[3\]他的象徵物為肩上的[獅皮和手上的橄榄木棒](../Page/涅墨亚狮子.md "wikilink")。同時他又具有愛好嬉戲的性格，常常用遊戲來使自己從常年的勞役中放鬆，還喜歡和孩子們打賭開玩笑。\[4\]由於他征服了很多古老而危險的原始力量，赫拉克勒斯被稱為「讓人類取得世界安全」的恩人。\[5\]赫拉克勒斯是個熱情而性情化的人物，他可以為了朋友出生入死（比如為了報答[艾德梅塔斯王子的盛情款待而和](../Page/艾德梅塔斯.md "wikilink")[塔納托斯角鬥](../Page/塔納托斯.md "wikilink")，或是幫助被驅逐的[斯巴達國王](../Page/斯巴達.md "wikilink")[廷達瑞俄斯重返王位](../Page/廷達瑞俄斯.md "wikilink")），他同時又是那些背叛他的敵人（如[奧革阿斯](../Page/奧革阿斯.md "wikilink")，[涅琉斯和](../Page/涅琉斯.md "wikilink")[拉俄墨冬](../Page/拉俄墨冬.md "wikilink")）的噩夢。

## 起源和形象

赫拉克勒斯一生充滿傳奇，最著名的無非是他的十二項英雄偉績。古希臘時代居住在[亞歷山大港的詩人們用充滿詩意和悲劇氣氛的文字記錄了他的一生](../Page/亞歷山大港.md "wikilink")。\[6\]他的形象起源是[近東地區的鬥獅者](../Page/近東.md "wikilink")，在[伊特魯里亞文化中可以找到和他對應的人物赫丘勒](../Page/伊特魯里亞文化.md "wikilink")（Hercle），兩位主神[丁尼亞](../Page/丁尼亞.md "wikilink")（Tinia，對應希臘神話的宙斯）和[烏尼的兒子](../Page/烏尼.md "wikilink")。\[7\]\[8\]

赫拉克勒斯是古希臘時代[克托尼俄斯的英雄中最偉大](../Page/克托尼俄斯.md "wikilink")，但不同於其他的英雄，他同為英雄和神（即他死後成為神的事實），[品達稱他為理論上的英雄](../Page/品達.md "wikilink")，因此當古希臘祭祀他的時候會先向地府獻上美酒（以告慰他作為人類英雄之靈），然後再向聖壇獻上美酒（以祭祀他的神靈），當然他很多時候也被稱為「[半神](../Page/半神.md "wikilink")」。\[9\][瓦爾特·伯科特已經證明赫拉克勒斯故事的核心內容起源於](../Page/瓦爾特·伯科特.md "wikilink")[新石器時代的捕獵文化以及](../Page/新石器時代.md "wikilink")[薩滿教傳入平民階層時產生的傳說](../Page/薩滿教.md "wikilink")。\[10\]

### 英雄還是神祇？

赫拉克勒斯作為英雄的形象，在古典時代被[奧林匹斯十二主神所承認](../Page/奧林匹斯十二主神.md "wikilink")。赫拉克勒斯死後成為神的這個主題有可能為虛構的，《[奧德賽](../Page/奧德賽.md "wikilink")》的第十一章（the
Nekuia，意為招魂術）中出現了一處比較蹩腳的描述，[奧德修斯在地府遇到了赫拉克勒斯](../Page/奧德修斯.md "wikilink")：

  -
    ''And next I caught a glimpse of powerful Heracles—
    ''His ghost I mean: the man himself delights
    ''in the grand feasts of the deathless gods on high...
    ''Around him cries of the dead rang out like cries of birds
    *scattering left and right in horror as on he came like
    night..."*\[11\]
    ''「其後，我見著了強有力的赫拉克勒斯，
    ''　當然，是他的影像，他自己則置身不死的神明之中…
    ''　他的四周噪響著一陣陣喧叫，死人的精靈，像一群鳥兒，
    *　四散飛躲；他來了，像烏黑的夜晚…」*

古代的評論家們認為這段關於赫拉克勒斯認出奧德修斯並向他致敬的旁白的問題在於其破壞了整段文字的鮮明而完整的描述，而現代評論家則認為有理由相信這句詩的開始：*當然，是他的影像……*並非原文，Friedrich
Solmsen認為「當人們都知道赫拉克勒斯得到眾神的允許生活在聖山，
他們不會允許他出現在地府」\[12\]，並認為這段突然插入的文字明顯是為了勉強折中關於赫拉克勒斯靈魂所在地的矛盾。

也有一說為當赫拉克勒斯死的時候將沾滿毒血的皮膚脫下，皮膚落入了地府，而他則升上了神之位。

## 傳奇一生

[Herakles_snake_Musei_Capitolini_MC247.jpg](https://zh.wikipedia.org/wiki/File:Herakles_snake_Musei_Capitolini_MC247.jpg "fig:Herakles_snake_Musei_Capitolini_MC247.jpg")
[Hera_suckling_Herakles_BM_VaseF107.jpg](https://zh.wikipedia.org/wiki/File:Hera_suckling_Herakles_BM_VaseF107.jpg "fig:Hera_suckling_Herakles_BM_VaseF107.jpg")為還是嬰兒的赫拉克勒斯脯乳，她的左邊是[雅典娜](../Page/雅典娜.md "wikilink")（視圖之外）和[阿佛洛狄忒](../Page/阿佛洛狄忒.md "wikilink")，右邊是她的信使，手持帶翼[雙蛇杖的](../Page/雙蛇杖.md "wikilink")[伊里斯](../Page/伊里斯.md "wikilink")，阿普尼亞紅彩裝飾瓶細節，公元前360到350年\]\]

### 出生和童年時期

引發圍繞著赫拉克勒斯的悲劇的最主要的因素是[宙斯的妻子眾神之](../Page/宙斯.md "wikilink")-{后}-[赫拉對他的憎恨](../Page/赫拉.md "wikilink")。赫拉克勒斯作為宙斯的私生子的身份，是赫拉對他的無盡的折磨的源泉。赫拉克勒斯是宙斯和人類女子[阿爾克墨涅的兒子](../Page/阿爾克墨涅.md "wikilink")。宙斯化身為阿爾克墨涅的丈夫[安菲特律翁](../Page/安菲特律翁.md "wikilink")，和她交合。當時安菲特律翁正在從戰場回來的路上，阿爾克墨涅同時也懷上了他的兒子（這是個非常罕見的[同期復孕現象](../Page/同期復孕.md "wikilink")，即一名女性懷上了分別來自不同兩位男性的雙胞胎）。因此，赫拉克勒斯的存在證明了宙斯的濫交，赫拉為了報復宙斯的不忠，常常會折磨宙斯的人類後代。赫拉克勒斯的雙胞兄弟，安菲特律翁之子，[伊菲克勒斯是他的戰車御者](../Page/伊菲克勒斯.md "wikilink")[伊奧勞斯的父親](../Page/伊奧勞斯.md "wikilink")。

在這對雙胞胎出生的那個晚上，赫拉在知道宙斯通姦行径的情況下，勸說他發出誓言，在這天夜裡最早出生的一名[珀耳修斯的後代將成為至尊國王](../Page/珀耳修斯.md "wikilink")。因為赫拉知道赫拉克勒斯將作為珀耳修斯的後代出生，但和他幾乎同時出生[伊菲克勒斯也具有這樣的身份](../Page/伊菲克勒斯.md "wikilink")。當她得到這個誓言後，赫拉立即強迫生育女神[厄勒梯亞將阿爾克墨涅的衣帶打結以推遲赫拉克勒斯的出生](../Page/厄勒梯亞.md "wikilink")。同時赫拉讓歐律斯透斯提早出生，讓他取代了赫拉克勒斯的至尊國王的位置。如果不是因為受到了阿爾克墨涅的朋友和僕人[該林西亞斯的欺騙](../Page/該林西亞斯.md "wikilink")，赫拉甚至差點讓赫拉克勒斯胎死腹中：該林西亞斯欺騙厄勒梯亞說阿爾克墨涅已經產下了一個男孩，厄勒梯亞大吃一驚，放開了打結的手，讓阿爾克墨涅可以順利的產下雙胞胎。

赫拉克勒斯的父母最初為他命名為阿爾喀得斯，意为“阿尔开俄斯的后续”。阿尔开俄斯是安菲特律翁的父亲。他们恐惧于赫拉的愤怒，将赫拉克勒斯放在荒野中，赫拉和雅典娜经过时看到了这个长得很好的孩子，雅典娜怂恿自己的女伴给孩子喂奶，赫拉此时并不知道这就是她仇敌的孩子，也不知道自己神圣的奶将成就他的不朽。他也得到赫拉克勒斯（赫拉的荣耀）這個名字。赫拉克勒斯吃奶时咬痛了赫拉，她把孩子推开，奶水洒出来，就成为天上的银河。后来，赫拉发现了自己的错误，放了兩條毒蛇到他的搖籃中想殺害他。赫拉克勒斯卻一隻手抓了蛇，並活活掐死它們，將它們的屍體當做玩具。

### 青年時期

在他無意間用[里拉琴誤殺了他的音樂老師](../Page/里拉琴.md "wikilink")[利諾斯後](../Page/利諾斯.md "wikilink")，赫拉克勒斯被他在人間的父親[安菲特律翁派去山上放牧](../Page/安菲特律翁.md "wikilink")。在那兒，根據詭辨家[普羅底庫斯](../Page/普羅底庫斯.md "wikilink")（大約公元前400年）的一則寓言，「赫拉克勒斯的選擇」，他遇到了兩位女神，享乐女神和美德女神，她們讓他在享乐而簡單和艱辛但光榮的兩條生活之路之間選擇一條，他選擇了後者。

後來在[底比斯](../Page/底比斯_\(希臘\).md "wikilink")，赫拉克勒斯和國王[克瑞翁的女兒](../Page/克瑞翁.md "wikilink")[墨伽拉結婚](../Page/墨伽拉.md "wikilink")。但是由於[赫拉的詛咒](../Page/赫拉.md "wikilink")，他在瘋狂中殺害了自己和墨伽拉所生下的小孩。當[安提庫拉的建造者](../Page/安提庫拉.md "wikilink")[安提庫瑞烏薩用](../Page/安提庫瑞烏薩.md "wikilink")[鐵筷子草治好赫拉克勒斯的瘋狂後](../Page/鐵筷子屬.md "wikilink")，\[13\]他認識到自己做的如此邪惡的事情，並遵照得到的預言逃跑到了[德爾斐](../Page/德爾斐.md "wikilink")。他並不知道，這個預言是赫拉故意給予他的，根據這個預言，他必須為國王[歐律斯透斯完成十個任務](../Page/歐律斯透斯.md "wikilink")，並實現他所有的要求。歐律斯透斯在赫拉克勒斯完成十個任務後認為赫拉克勒斯有作弊行為，因此又增加了兩個任務，這些就是後來為人稱頌的赫拉克勒斯的十二偉業。

[thumb](../Page/圖像:Herakles_lion_Louvre_F33.jpg.md "wikilink")

### 赫拉克勒斯的十二偉業

因為[赫拉的詛咒](../Page/赫拉.md "wikilink")，赫拉克勒斯在瘋狂中殺害了自己的孩子。為了消除這個罪業，他被要求為他的最主要的敵人、佔了本該屬於他的王位的[歐律斯透斯完成十個任務](../Page/歐律斯透斯.md "wikilink")。根據預言，如果他成功完成歐律斯透斯交給他的十個任務，他的罪將被清除並獲得不朽的名聲。赫拉克勒斯完成了這些任務，但歐律斯透斯不承認清洗[奧革阿斯的牛廄的任務](../Page/奧革阿斯.md "wikilink")，因為赫拉克勒斯為這個任務索要了報酬；他也不接受赫拉克勒斯殺死「九頭蛇」海德拉的任務，因為赫拉克勒斯是在他的侄兒[伊奧勞斯的幫助下完成這個任務的](../Page/伊奧勞斯.md "wikilink")。因此歐律斯透斯又提出了兩個新的任務，赫拉克勒斯也成功的完成了它們，並讓自己的偉業達到了十二項。

不同的作者給出了不同的順序。[阿波羅多洛斯](../Page/雅典的阿波羅多洛斯.md "wikilink")（2.5.1-2.5.12）以以下順序給出了赫拉克勒斯的十二偉業：
[right的金蘋果](../Page/圖像:Hercules_Musei_Capitolini_MC1265_n2.jpg.md "wikilink")（鍍金青銅像，羅馬藝術品，公元2世紀）。\]\]

1.  殺死[涅墨亞獅子](../Page/涅墨亞獅子.md "wikilink")（獅子成為[獅子座](../Page/獅子座.md "wikilink")）
2.  殺死[九頭蛇海德拉](../Page/九頭蛇.md "wikilink")（還順便擊殺了欲攻擊海克力斯的巨泥[蟹](../Page/蟹.md "wikilink")，成為了[巨蟹座](../Page/巨蟹座.md "wikilink")，而九頭蛇為[長蛇座](../Page/長蛇座.md "wikilink")）
3.  捕獲「月亮女神」[阿耳忒彌斯的](../Page/阿耳忒彌斯.md "wikilink")[刻律涅牝鹿](../Page/刻律涅牝鹿.md "wikilink")
4.  活捉[厄律曼托斯山野豬](../Page/厄律曼托斯山的野豬.md "wikilink")
5.  清洗[奧革阿斯的牛廄](../Page/奧革阿斯.md "wikilink")
6.  殺死[斯廷法利斯湖怪鳥](../Page/斯廷法利斯湖怪鳥.md "wikilink")
7.  制伏[克里特公牛](../Page/克里特公牛.md "wikilink")
8.  制伏[狄俄墨得斯牝馬](../Page/狄俄墨得斯牝馬.md "wikilink")
9.  奪取[亞馬遜女王](../Page/亞馬遜人.md "wikilink")[希波吕忒的腰帶](../Page/希波吕忒.md "wikilink")
10. 牽回巨人[革律翁的牛群](../Page/革律翁.md "wikilink")
11. 摘取[赫斯珀裡得斯的金蘋果](../Page/赫斯珀裡得斯.md "wikilink")
12. 活捉「地獄三頭犬」[刻耳柏洛斯](../Page/刻耳柏洛斯.md "wikilink")

### 海拉斯

在完成了這些任務後，當他在野外行走時，赫拉克勒斯被[德律俄珀斯人所包圍](../Page/德律俄珀斯.md "wikilink")。他殺死了他們的王[忒俄達瑪斯](../Page/忒俄達瑪斯.md "wikilink")，其他人則投降並將他們的王子[海拉斯交給了他](../Page/海拉斯.md "wikilink")。他讓海拉斯成為他的副手並愛上了這個年輕人。幾年後，赫拉克勒斯和海拉斯登上了[阿爾戈號](../Page/阿爾戈號.md "wikilink")。作為[阿爾戈英雄](../Page/阿爾戈英雄.md "wikilink")，他們只是參加了部分的旅程。他們拯救了女英雄們，征服了[特洛伊並幫助眾神同](../Page/特洛伊.md "wikilink")[癸幹忒斯戰鬥](../Page/癸幹忒斯.md "wikilink")。在[密細亞](../Page/密細亞.md "wikilink")，海拉斯去找水时被水中的[寧芙綁架](../Page/寧芙.md "wikilink")。赫拉克勒斯找了很久也没找到，其他阿尔戈的英雄就先开船走了。在另一些版本中海拉斯最後被淹死了。不論怎樣，赫拉克勒斯在密細亞離開了阿爾戈號。

### 進一步的冒險

之後，赫拉克勒斯愛上了[俄卡利亞的公主](../Page/俄卡利亞.md "wikilink")[伊俄勒](../Page/伊俄勒.md "wikilink")。俄卡利亞之王[歐律托斯宣布](../Page/歐律托斯.md "wikilink")，能夠在箭術比賽中勝過他的兒子的人可以迎娶伊俄勒。赫拉克勒斯獲得了勝利，但歐律托斯卻食言了，他和他的兒子們（除了伊俄勒的兄弟[伊菲托斯](../Page/伊菲托斯.md "wikilink")）藐視赫拉克勒斯的優點。赫拉克勒斯殺死了國王和他除了伊菲托斯之外的兒子，並誘拐了伊俄勒。後來伊菲托斯成了赫拉克勒斯最好的朋友。可惜，[赫拉再度讓赫拉克勒斯陷入瘋狂](../Page/赫拉.md "wikilink")，他將伊菲托斯拋向城牆造成了其的死亡。赫拉克勒斯為了贖罪，開始了為[呂底亞女王](../Page/呂底亞.md "wikilink")[翁法勒的三年奴役](../Page/翁法勒.md "wikilink")。

[250px](../Page/圖像:Francois_Lemoine_Heracles_and_Omphale.jpg.md "wikilink")，因此被罰給女王[翁法勒做三年奴隸](../Page/翁法勒.md "wikilink")。在服役中，他成了女王的情人，性情也發生了很大改變。他開始好穿女人服飾，同翁法勒的侍女們紡著羊毛線，而女王則披著他的獅皮。《赫拉克勒斯和翁法勒》，Francois
Lemoine所繪。\]\]

### 翁法勒

[翁法勒是](../Page/翁法勒.md "wikilink")[呂底亞的女王](../Page/呂底亞.md "wikilink")，作為對他的兇殺的懲罰，赫拉克勒斯成為了她的奴隸。他被要求穿上女人的衣服和做女人的工作，而女王則披上了他的[獅皮](../Page/涅墨亞獅子.md "wikilink")，手持他的橄欖木棒。一段時間後，翁法勒給予了赫拉克勒斯自由，並嫁給了他。一些文獻中提到他們生下了一個兒子。其間，[克可佩欺](../Page/克可佩欺.md "wikilink")，愛好惡作劇的森林精靈偷了赫拉克勒斯的武器，為了懲罰它們，赫拉克勒斯將它們倒綁在一個柱子上，讓他們的臉一直朝下。

### 拯救普羅米修斯

在[赫西俄德的](../Page/赫西俄德.md "wikilink")《[神譜](../Page/神譜.md "wikilink")》和[埃斯庫羅斯的](../Page/埃斯庫羅斯.md "wikilink")《解放了的普羅米修斯》中都提到了赫拉克勒斯射死了[宙斯派去折磨](../Page/宙斯.md "wikilink")[普羅米修斯的獵鷹並將他從鎖鏈中解救下來](../Page/普羅米修斯.md "wikilink")，因此普羅米修斯為赫拉克勒斯做了預言。

### 特洛伊的拉俄墨冬

在[特洛伊戰爭開始前](../Page/特洛伊戰爭.md "wikilink")，[波塞冬曾派了一隻海怪襲擊](../Page/波塞冬.md "wikilink")[特洛伊](../Page/特洛伊.md "wikilink")。這個故事和《[伊利亞特](../Page/伊利亞特.md "wikilink")》的幾個枝節有關係（7.451-453,
20.145-148,
21.442-457），《[書庫](../Page/書庫.md "wikilink")》對其也有記載（2.5.9）。[拉俄墨冬本來計劃將他自己的女兒](../Page/拉俄墨冬.md "wikilink")[赫西俄涅獻給波塞冬以獲得寬恕](../Page/赫西俄涅.md "wikilink")。赫拉克勒斯正好和[忒拉蒙以及](../Page/忒拉蒙.md "wikilink")[俄伊克勒斯一起路過特洛伊](../Page/俄伊克勒斯.md "wikilink")，他以拉俄墨冬從[宙斯那裡得到的作為綁架](../Page/宙斯.md "wikilink")[伽倪墨得斯的賠償的神馬為報酬同意殺掉海怪](../Page/伽倪墨得斯.md "wikilink")。當他殺死海怪後，拉俄墨冬卻食言了。因此，在後來的探險中，赫拉克勒斯和他的同伴攻擊並洗劫了特洛伊。他們殺死了拉俄墨冬几乎所有的兒子（不包括[提托诺斯](../Page/提托诺斯.md "wikilink")），[普里阿摩斯則用一張赫西俄涅親手織的黃金面紗向赫拉克勒斯贖得了自己的生命](../Page/普里阿摩斯.md "wikilink")。忒拉蒙則將赫西俄涅作為戰利品，他們後來結了婚，並生下了[透克洛](../Page/透克洛.md "wikilink")。

[thumb](../Page/圖像:Herakles_och_Antaios,_Nordisk_familjebok.png.md "wikilink")\]\]

### 其他重要的事迹

赫拉克勒斯为了帮助自己的朋友总是不余其力，比如他击败了由国王[米哥多尼所领导的](../Page/米哥多尼.md "wikilink")[比提尼亞人并将他们的土地给了](../Page/比提尼亞.md "wikilink")[达斯柯罗斯的儿子](../Page/达斯柯罗斯.md "wikilink")，[密细亚的](../Page/密细亚.md "wikilink")[吕科斯王子](../Page/吕科斯.md "wikilink")。又如他在[阿德墨托斯的妻子](../Page/阿德墨托斯.md "wikilink")[阿尔刻斯提斯愿意代替她的丈夫去死的那个晚上出现在阿德墨托斯家](../Page/阿尔刻斯提斯.md "wikilink")，并躲藏在阿尔刻斯提斯，狠狠地惊吓了死神并从他手上夺回了阿尔刻斯提斯，将她还给她的丈夫。

他除掉了很多其他邪恶的君主，仇视他自己以及反对主神的人，比如盗贼[特米鲁斯](../Page/特米鲁斯.md "wikilink")，[多罗皮亚的国王](../Page/多罗皮亚.md "wikilink")[阿明托耳](../Page/阿明托耳.md "wikilink")（因为其不让他进入其国家），[阿拉伯的国王](../Page/阿拉伯半岛.md "wikilink")[厄玛提翁](../Page/厄玛提翁.md "wikilink")，[利堤厄耳塞斯](../Page/利堤厄耳塞斯.md "wikilink")（在和其比赛摔跤胜利后将其杀掉），[皮罗斯的](../Page/皮罗斯.md "wikilink")[波里克吕墨诺斯](../Page/波里克吕墨诺斯.md "wikilink")，[阿拉斯托耳和其兄弟](../Page/阿拉斯托耳.md "wikilink")，[希波孔和其儿子](../Page/希波孔.md "wikilink")（希波孔篡夺了赫拉克勒斯的好友[斯巴达国王](../Page/斯巴达.md "wikilink")[廷达瑞俄斯的王位](../Page/廷达瑞俄斯.md "wikilink")），巨人[库克诺斯](../Page/库克诺斯.md "wikilink")、[波耳费里翁和](../Page/波耳费里翁.md "wikilink")[弥玛斯以及](../Page/弥玛斯.md "wikilink")[安泰俄斯](../Page/安泰俄斯.md "wikilink")（被他高高举起，离开其母[盖亚](../Page/盖亚.md "wikilink")）。当[奥革阿斯拒绝付给他清洗牛廄的报酬时](../Page/奥革阿斯.md "wikilink")，赫拉克勒斯对其开战，但奥革阿斯的大将[摩利翁尼得斯让赫拉克勒斯生病](../Page/摩利翁尼得斯.md "wikilink")。最后他还是成功的杀死了摩利翁尼得斯，并洗劫了[伊利斯](../Page/伊利斯.md "wikilink")，屠杀了奥革阿斯和其儿子。但由于他天生神力，又多次受到[赫拉的诅咒](../Page/赫拉.md "wikilink")，赫拉克勒斯也几次失手误杀了好友或是在比赛中失手杀害了对手，比如他的音乐老师[利諾斯以及著名的拳击手](../Page/利諾斯.md "wikilink")[西西里岛的](../Page/西西里岛.md "wikilink")[厄律克斯](../Page/厄律克斯.md "wikilink")。

赫拉克勒斯一生跟随过很多老师，所以他多才多艺。他曾和[安托尔一起去拜访过](../Page/安托尔.md "wikilink")[伊范德](../Page/伊范德.md "wikilink")，后来[安托尔就留在了意大利](../Page/安托尔.md "wikilink")。他被认为建立了意大利的城市[塔兰托](../Page/塔兰托.md "wikilink")。他向[奥托吕科斯学习过摔角](../Page/奥托吕科斯.md "wikilink")。他有时很孩子气，比如他曾向酒神[狄俄倪索斯挑战喝酒](../Page/狄俄倪索斯.md "wikilink")，结果惨败，不得不当了一段时间的酒宴御者。

在[阿里斯托芬的喜剧](../Page/阿里斯托芬.md "wikilink")《[青蛙](../Page/青蛙_\(戲劇\).md "wikilink")》中，狄俄倪索斯向赫拉克勒斯请教通往冥界的道路，赫拉克勒斯被狄俄倪索斯大大的逗乐了，并在告诉他真正的方法前，开玩笑的告诉了他几种包括自杀在内的“特殊方法”。

[希羅多德认为赫拉克勒斯是](../Page/希羅多德.md "wikilink")[锡西厄的建造者](../Page/锡西厄.md "wikilink")。当赫拉克勒斯在野外露宿时，有一个半蛇半女性的生物偷走了他的马。后来他找到了这个生物，但她却拒绝归还他的马匹，直到他同意和她发生性关系。这之后他重新得到自己的马，但在他离开之前却将自己的弓箭送给了这个生物，并嘱咐她让他们的后代在锡西厄建立一个新的王国。

[thumb綁架](../Page/圖像:Guido_Reni_038.jpg.md "wikilink")[德伊阿妮拉](../Page/德伊阿妮拉.md "wikilink")([圭多·雷尼](../Page/圭多·雷尼.md "wikilink")，1620-1621，[卢浮宫](../Page/卢浮宫.md "wikilink"))\]\]

### 赫拉克勒斯和女性

在他的一生中，赫拉克勒斯結了四次婚。

他的第一段和[墨伽拉的婚姻因為他在瘋狂中殺害了他們的孩子而告終](../Page/墨伽拉.md "wikilink")。根據《[書庫](../Page/書庫.md "wikilink")》，墨伽拉後來改嫁給[伊奧勞斯](../Page/伊奧勞斯.md "wikilink")，而[歐里庇得斯則認為赫拉克勒斯也將墨伽拉殺害了](../Page/歐里庇得斯.md "wikilink")。

他的第二位妻子是將他當成奴隸的[呂底亞女王](../Page/呂底亞.md "wikilink")[翁法勒](../Page/翁法勒.md "wikilink")。

他的第三次婚姻則是[德伊阿妮拉](../Page/德伊阿妮拉.md "wikilink")，為了她，他和河神[阿克洛俄斯摔跤](../Page/阿克洛俄斯.md "wikilink")，河神变成牛来顶赫拉克勒斯，没想到赫拉克勒斯正是训牛的好手。他抓住牛角，力量太大把牛角掰断了。最后河神输了，并用“丰饶之角”换回来自己的角。在結婚不久後，他們橫渡一條大河，[半人馬](../Page/半人馬.md "wikilink")[涅索斯將德伊阿妮拉馱在背上送她過河](../Page/涅索斯.md "wikilink")，走到河中心的时候要非礼她。赫拉克勒斯看到了，等他们上了岸，用沾了[許德拉的毒血的箭射死了涅索斯](../Page/九頭蛇.md "wikilink")。涅索斯為了復仇，在死前要德伊阿妮拉收集自己的血液，欺騙她这些血如果涂在男人的衣服上可以讓變心的男人回心轉意。

後來，德伊阿妮拉認為赫拉克勒斯愛上了[伊俄勒](../Page/伊俄勒.md "wikilink")，她將那些血液塗在了一件罩衫上。赫拉克勒斯的僕人[利卡斯將這件有毒的罩衫交給了他](../Page/利卡斯.md "wikilink")，當他穿上它時，那些復仇的血液開始吞噬他的皮膚。雖然他試圖脫下罩衫，但它已經緊緊的黏在他的骨頭上。最後，赫拉克勒斯自願選擇死去，他讓人為他建了一個柴堆，讓熊熊的烈火為他除去痛苦。他死后，眾神將接到[奧林匹斯山](../Page/奧林匹斯山.md "wikilink")，他成了永生的神，和青春女神[赫柏結婚](../Page/赫柏.md "wikilink")。

在一些版本中，[塞斯比亞的國王](../Page/塞斯比亞.md "wikilink")[忒斯庇俄斯希望赫拉克勒斯去殺死](../Page/忒斯庇俄斯.md "wikilink")[西塞隆的獅子](../Page/西塞隆的獅子.md "wikilink")，作為回報赫拉克勒斯可以在一天晚上內和他的50個女兒發生關係。赫拉克勒斯完成了這個“艱鉅的任務”，並讓她們全部懷孕並生下了大量的男孩。這有時被認為是赫拉克勒斯的第十三項偉業。很多古希臘的國王都自認為他們的血統來自此，特別是[斯巴達和](../Page/斯巴達.md "wikilink")[馬其頓王國的國王們](../Page/馬其頓王國.md "wikilink")。

### 赫拉克勒斯與男同性戀者

[thumb](../Page/圖像:Hercules_and_Iolaus_mosaic_-_Anzio_Nymphaeum.jpg.md "wikilink")（[安濟奧的羅馬式噴泉馬賽克畫](../Page/安濟奧.md "wikilink")）\]\]
由於赫拉克勒斯作為英雄的標誌，很多男同性戀者於神話創作中視為赫拉克勒斯的男性情人。

[普魯塔克在他的](../Page/普魯塔克.md "wikilink")《愛的對話》中提到，被添加作為赫拉克勒斯的男性情人十分的多。其中和赫拉克勒斯關係最密切的是[底比斯的](../Page/底比斯_\(希臘\).md "wikilink")[伊奧勞斯](../Page/伊奧勞斯.md "wikilink")。根據一則被認為是古神話起源的故事，伊奧勞斯是赫拉克勒斯的戰車御者以及護衛。赫拉克勒斯最終幫伊奧勞斯找了一名妻子。根據普魯塔克的記錄，一直到他的那個時代，男性同性戀人會成雙成對的到伊奧勞斯的墓前，發誓效忠於這位英雄以及對方。\[14\]\[15\]

赫拉克勒斯的另一位男性情人，[海拉斯](../Page/海拉斯.md "wikilink")，常常出現在古希臘和現代的藝術作品中。雖然他出現在比伊奧勞斯更加晚的文獻中（最早出現在公元3世紀）。\[16\]

[來古格士也提出證明](../Page/來古格士.md "wikilink")，赫拉克勒斯和[伊拉卡塔斯是一對同性戀人](../Page/伊拉卡塔斯.md "wikilink")，他們之間的愛情神話似乎非常古老。\[17\]與[阿布得臘齊名的英雄](../Page/阿布得臘_\(色雷斯\).md "wikilink")，[阿布得羅斯也被認為是赫拉克勒斯的男性情人之一](../Page/阿布得羅斯.md "wikilink")。據說他被委託捕捉[狄俄墨得斯牝馬卻被它們殺害](../Page/狄俄墨得斯牝馬.md "wikilink")。赫拉克勒斯為了紀念他而在[色雷斯建立了城市阿布得臘](../Page/色雷斯.md "wikilink")，因為他曾在那兒參加運動會且取得優勝。\[18\]

其他的還有關於[伊菲托斯的神話](../Page/伊菲托斯.md "wikilink")；\[19\]以及赫拉克勒斯和[尼柔斯之間的愛情](../Page/尼柔斯.md "wikilink")，尼柔斯被譽為「來自伊利昂（即[特洛伊](../Page/特洛伊.md "wikilink")）的最美的男子」（《[伊利亞特](../Page/伊利亞特.md "wikilink")》，673）。但[托勒密補充到](../Page/托勒密.md "wikilink")，一些作者將尼柔斯當做了赫拉克勒斯的兒子。\[20\]

還有一些有記載的赫拉克勒斯的男性情人其實是後期的虛構產物或完全的文學上的空想，比如曾經參加過[卡呂冬野豬狩獵的](../Page/卡呂冬野豬.md "wikilink")[阿德墨托斯](../Page/阿德墨托斯.md "wikilink")\[21\]、[阿多尼斯](../Page/阿多尼斯.md "wikilink")\[22\]、[科里索斯](../Page/科里索斯.md "wikilink")\[23\]以及[涅斯托爾](../Page/涅斯托尔_\(神话\).md "wikilink")。據說赫拉克勒斯因為他的智慧而愛上他，他作為情人的身份可以解釋為什麼他是[涅琉斯的唯一沒有被赫拉克勒斯殺掉的兒子](../Page/涅琉斯.md "wikilink")。\[24\]

[thumb](../Page/圖像:Herakles_and_Telephos_Louvre_MR219.jpg.md "wikilink")

### 後代

  - [忒勒福斯是赫拉克勒斯和](../Page/忒勒福斯.md "wikilink")[奧格生下的兒子](../Page/奧格.md "wikilink")。
  - [許羅斯是他和](../Page/許羅斯.md "wikilink")[得伊阿尼拉或者](../Page/得伊阿尼拉.md "wikilink")[墨利忒生下的兒子](../Page/墨利忒.md "wikilink")。
  - 他和[赫柏的後代是](../Page/赫柏.md "wikilink")[阿勒克西阿瑞斯和阿尼刻托斯](../Page/阿勒克西阿瑞斯和阿尼刻托斯.md "wikilink")。

在某些版本中，赫拉克勒斯在旅途中偶遇半蛇女[厄客德娜](../Page/厄客德娜.md "wikilink")，並和她產生了後代[德拉孔提代](../Page/德拉孔提代.md "wikilink")，即[卡德摩斯王朝的祖先](../Page/卡德摩斯.md "wikilink")。

[thumb](../Page/圖像:Giambologna-_Hercules_beating_Centaur_Nesso-Loggia_dei_Lanzi.jpg.md "wikilink")[涅索斯](../Page/涅索斯.md "wikilink")([詹波隆那的大理石雕刻](../Page/詹波隆那.md "wikilink")，[佛羅倫薩](../Page/佛羅倫薩.md "wikilink"))\]\]

### 死亡

赫拉克勒斯的死亡記載於[奧維德的](../Page/奧維德.md "wikilink")《[變形記](../Page/變形記_\(奧維德\).md "wikilink")》的第九卷。在和河神[阿克洛俄斯激戰並取得勝利後](../Page/阿克洛俄斯.md "wikilink")，赫拉克勒斯和[得伊阿尼拉結了婚](../Page/得伊阿尼拉.md "wikilink")。他們在[梯林斯的旅程中](../Page/梯林斯.md "wikilink")，[半人馬](../Page/半人馬.md "wikilink")[涅索斯假裝願意駝得伊阿尼拉橫跨一條流速很快的河流](../Page/涅索斯.md "wikilink")，這樣赫拉克勒斯可以游泳過去。但涅索斯實際上是本性邪惡的半人馬，並打算趁赫拉克勒斯還在水里時綁架得伊阿尼拉。當赫拉克勒斯到了對岸並發現真相時，他憤怒的用蘸過[許德拉毒血的箭射殺了涅索斯](../Page/九頭蛇.md "wikilink")。為了復仇，涅索斯在臨死前將自己的血交給得伊阿尼拉，欺騙她自己的血可以讓變心的男人回心轉意。\[25\]

幾年後，得伊阿尼拉聽信了赫拉克勒斯愛上別人的[謠言](../Page/謠言.md "wikilink")，為了挽回他的心，她將涅索斯的血塗在一件罩衫上。[利卡斯](../Page/利卡斯.md "wikilink")，赫拉克勒斯的傳令官，將這件有毒的罩衫交給了他。當他穿上這件罩衫時，那些來自許德拉的毒血開始腐蝕他的血肉，併吞噬他的骨骼。在他死之前，赫拉克勒斯將利卡斯拋入海中，認為其是想要毒害他的人（根據一些文獻，利卡斯變成了同名的石像）。之後，赫拉克勒斯讓人為他建造了一個火葬堆，由[菲羅克忒忒斯的父親](../Page/菲羅克忒忒斯.md "wikilink")[波亞斯親自為他點火](../Page/波亞斯.md "wikilink")。當他的軀體燃燒時，他作為神的部分與做為鬼的部分被分別保留了下來，[宙斯將他神的部分](../Page/宙斯.md "wikilink")[神化](../Page/神化.md "wikilink")，因此他升上了[奧林匹斯山與](../Page/奧林匹斯山.md "wikilink")[赫拉和解成為眾神狩獵時的搬運工與她的養子](../Page/赫拉.md "wikilink")，而身為鬼的部分那狂暴的兇獸則被哈迪斯重用行走於在冥界眾鬼的行列中，頭披著獅皮圍著自身命運織就的錦袍為哈迪斯管理眾鬼的行列。

夜空中八十八個星座裡面，在天龍座頭部上方的武仙座就是以赫拉克勒斯的形象命名。

一些版本中是他的好友菲羅克忒忒斯為他點燃火堆，因此菲羅克忒忒斯（或波亞斯）繼承了他的弓箭，這副武器在之後的[特洛伊戰爭中起到了至關重要的作用](../Page/特洛伊戰爭.md "wikilink")。菲羅克忒忒斯和[帕里斯對陣的時候](../Page/帕里斯.md "wikilink")，就是用這副弓箭射中了帕里斯，由於許德拉的毒血，帕里斯立刻死在箭下。這成為了特洛伊戰爭的最後轉折點。

[thumb時期的](../Page/圖像:Bistoon_Kermanshah.jpg.md "wikilink")[瑣羅亞斯德教徒用赫拉克勒斯的形像對其鬥戰神進行刻畫](../Page/瑣羅亞斯德教.md "wikilink")。雕刻於公元前153年，[克爾曼沙赫](../Page/克爾曼沙赫.md "wikilink")，[伊朗](../Page/伊朗.md "wikilink")\]\]
[thumb的護衛](../Page/圖像:Museum_für_Indische_Kunst_Dahlem_Berlin_Mai_2006_015.jpg.md "wikilink")[金剛手菩薩](../Page/金剛手菩薩.md "wikilink")，被認為是赫拉克勒斯的一個[希臘式佛教化身](../Page/希臘式佛教.md "wikilink")。（[健馱邏國](../Page/健馱邏國.md "wikilink")，公元1世紀）\]\]

## 其他文化中的赫拉克勒斯

通過[希臘式佛教文化](../Page/希臘式佛教.md "wikilink")，赫拉克勒斯的形像被傳播到了遠東。一個保留至今的例子是日本寺廟前的守護神[哼哈二將](../Page/哼哈二將.md "wikilink")。[希羅多德將赫拉克勒斯和](../Page/希羅多德.md "wikilink")[腓尼基的神](../Page/腓尼基.md "wikilink")[麥勒卡特以及](../Page/麥勒卡特.md "wikilink")[埃及的神](../Page/埃及.md "wikilink")[舒聯繫起來](../Page/舒_\(埃及神祇\).md "wikilink")。赫拉克勒斯的神廟遍布[地中海沿岸的國家](../Page/地中海.md "wikilink")，比如**赫拉克勒斯神殿**（**Heracles
Monoikos**），建立在離它鄰近的市鎮很遠的現在被稱為[蔚藍海岸的海岬上](../Page/蔚藍海岸.md "wikilink")，它賦予了這個地區現在的名稱：[摩納哥](../Page/摩納哥.md "wikilink")。

地中海進入[大西洋的入口](../Page/大西洋.md "wikilink")，即[西班牙的最南端和](../Page/西班牙.md "wikilink")[摩洛哥的最北端遙遙相望的兩個海岬](../Page/摩洛哥.md "wikilink")，從古典主義的方面被認為是[赫拉克勒斯之柱的所在地](../Page/海格力斯之柱.md "wikilink")，這個說法來自赫拉克勒斯將兩根寬大的石柱立於此，以穩定這個海域和保護從這裡過往的船隻的安全的故事。

希臘的足球俱樂部[塞薩洛尼基赫拉克勒斯即是用赫拉克勒斯的名字命名的](../Page/塞薩洛尼基赫拉克勒斯.md "wikilink")。

赫拉克勒斯還被[阿萊斯特·克勞利封為](../Page/阿萊斯特·克勞利.md "wikilink")[天主真知教會的聖徒](../Page/天主真知教會.md "wikilink")。

在[DC漫畫](../Page/DC漫畫.md "wikilink")《[神奇女俠](../Page/神奇女俠.md "wikilink")》中的[赫拉克勒斯開始是亞馬遜的敵人](../Page/海格力斯_\(DC漫畫\).md "wikilink")，不過他後來成為了亞馬遜的盟友。在[漫威漫畫中](../Page/漫威漫畫.md "wikilink")，[赫拉克勒斯為一名超級英雄](../Page/海格力斯_\(驚奇漫畫\).md "wikilink")，也是[復仇者的成員](../Page/復仇者.md "wikilink")，他聲稱自己是從[奧林匹斯山降臨的力量之神](../Page/奧林匹斯山.md "wikilink")。

赫拉克勒斯也是多部電影的重要角色，如迪士尼的卡通電影《[大力士](../Page/大力士_\(1997年電影\).md "wikilink")》（以他的神話故事改編）、1963年的電影《[傑遜王子戰群妖](../Page/傑遜王子戰群妖.md "wikilink")》（他作為尋找[金羊毛的](../Page/金羊毛.md "wikilink")[阿爾戈英雄之一](../Page/阿爾戈英雄.md "wikilink")），以及[阿诺·施瓦辛格所主演的](../Page/阿诺·施瓦辛格.md "wikilink")《[大力神在紐約](../Page/大力神在紐約.md "wikilink")》（描寫赫拉克勒斯來到現代的紐約）。在電視系列中，赫拉克羅斯是[加拿大卡通](../Page/加拿大.md "wikilink")《》中的角色亨利海格力斯的祖先和指導者。

## 世系图

\[26\]

## 大眾文化

  - 《[大力士](../Page/大力士_\(1997年电影\).md "wikilink")》：[迪士尼于](../Page/迪士尼.md "wikilink")1997年发行的一部[卡通](../Page/卡通.md "wikilink")[电影](../Page/电影.md "wikilink")。根据戏中的情节和背景设置，主角「Hercules」（-{海格力斯}-）的[原型就是](../Page/原型.md "wikilink")[希腊神话里的赫拉克勒斯](../Page/希腊神话.md "wikilink")，却以[罗马神话中的](../Page/罗马神话.md "wikilink")「赫丘利」为名。
  - 《[英雄时代](../Page/英雄時代_\(動畫\).md "wikilink")》：[日本](../Page/日本.md "wikilink")[XEBEC制作的一部](../Page/XEBEC.md "wikilink")[电视](../Page/电视.md "wikilink")[动画作品](../Page/动画.md "wikilink")，当中英雄族的契约者之一为「赫拉克勒斯」。
  - 《[Fate/stay
    night](../Page/Fate/stay_night.md "wikilink")》：日本PC平台[文字冒險遊戲](../Page/文字冒險遊戲.md "wikilink")。以從者「Berserker」的職階登場，配音員為[西前忠久](../Page/西前忠久.md "wikilink")
  - 《[Fate/Grand
    Order](../Page/Fate/Grand_Order.md "wikilink")》：日本手機遊戲。沿用了《Fate/stay
    night》的設定和配音。
  - 《[Hercules
    武士屠龍](../Page/武士屠龍.md "wikilink")》：是一部1983年義大利與美國合拍的奇幻冒險片，由[Lou
    Ferrigno主演](../Page/卢·弗里基诺.md "wikilink")。並在1985年推出續集《大力士2》。
  - 《[海克力士](../Page/宙斯之子：赫拉克勒斯.md "wikilink")》：[好萊塢](../Page/好萊塢.md "wikilink")2014年上映的[3D](../Page/3D.md "wikilink")[冒險片](../Page/冒險片.md "wikilink")，由[巨石強森所主演](../Page/巨石強森.md "wikilink")，改編自[史蒂夫·摩爾的漫畫](../Page/史蒂夫·摩爾.md "wikilink")《[赫拉克勒斯：色雷斯人的戰爭](../Page/赫拉克勒斯：色雷斯人的戰爭.md "wikilink")》。
  - 《[時空之門](../Page/時空之門.md "wikilink")》：手機遊戲。5星海力克斯，進化6星大力神 海克里斯。

## 注释

## 參考文獻

{{-}}

[Category:古希臘英雄](../Category/古希臘英雄.md "wikilink")
[Category:希腊神话人物](../Category/希腊神话人物.md "wikilink")
[Category:宙斯的後裔](../Category/宙斯的後裔.md "wikilink")
[Category:阿爾戈英雄](../Category/阿爾戈英雄.md "wikilink")

1.  Becking, Bob, *et al.*. *Dictionary of deities and demons*. ed.
    Toorn,Karel van der. Wm. B. Eerdmans Publishing. 1999

2.  希腊神话集，第20章，赫拉克勒斯的故事

3.  Pausanias, Guide to Greece, 4.32.1

4.  Aelian, Varia Historia, 12.15

5.  Aelian, Varia Historia, 5.3

6.  瓦尔特·伯科特（Walter Burkert）：《希腊宗教》(Greek Religion)1985, pp. 208-9

7.  R.布洛赫：《伊达拉里亚人》（R.Bloch, The Etruscans），伦敦1965年版

8.  M.帕罗蒂诺：《伊达拉里亚的起源》（M.Pallottion, L’Origine degli
    Eturschi），罗马1947年版；Fr.阿尔台姆：《伊达拉里亚人的起源》（Fr. Altheim，Der
    Ursprung der Etrusker），德国巴登—巴登1950年版

9.
10. 瓦尔特·伯科特（Walter Burkert）：《希腊宗教》(Greek Religion)1985, pp. 208-212

11. Robert Fagles' translation, 1996:269.

12. Friedrich Solmsen, "The Sacrifice of Agamemnon's Daughter in
    Hesiod's' Ehoeae" *The American Journal of Philology* **102**.4
    (Winter 1981, pp. 353–358), p. 355.

13. Pausanias Χ 3.1 , 36.5. Ptolemaeus, Geogr. Hyph. ΙΙ 184. 12.
    Stephanus of Byzantium, sv «Aντίκυρα»

14. Plutarch, *Erotikos,* 761d.The tomb of Iolaus is also mentioned by
    Pindar.

15. Pindar, *Olympian Odes,* 9.98-99.

16. Apollonius of Rhodes, *Argonautica,* 1.1177-1357; Theocritus,
    *Idyll* 13.

17. Sosibius, in Hesychius of Alexandria's *Lexicon*

18. Apollodorus 2.5.8; Ptolemaeus Chennus, 147b, in Photios I of
    Constantinople's *Bibliotheca*

19. Ptolemaeus Chennus, in Photius' *Bibliotheca*.

20. Ptolemaeus Chennus, 147b.

21. Plutarch, *Erotikos,* 761e.

22. Ptolemaeus Chennus

23.
24. Ptolemaeus Chennus, 147e; Philostratus, *Heroicus* 696, per Sergent,
    1986, p. 163.

25. Ovid, *Metamorphoses*, IX l.132-3

26. Morford, MPO, Lenardon RJ（2007）*Classical Mythology*. pp. 865
    Oxford: Oxford University Press.