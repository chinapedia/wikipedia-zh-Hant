[HeLa_Hoechst_33258.jpg](https://zh.wikipedia.org/wiki/File:HeLa_Hoechst_33258.jpg "fig:HeLa_Hoechst_33258.jpg")細胞，中間與右邊的細胞正經歷[分裂間期](../Page/分裂間期.md "wikilink")，可見整個細胞核都呈藍色。而左方的細胞正在進行[有絲分裂](../Page/有絲分裂.md "wikilink")，其細胞核正在崩解，準備接下來要進行的分裂。\]\]

**赫斯特染色**（英語：**Hoechst
stains**）是[螢光染色中的一種類型](../Page/螢光染色.md "wikilink")，專門用來在螢光顯微觀察下標示[DNA](../Page/DNA.md "wikilink")。由於可將DNA標示，此種染色方式也可以用來辨識內含DNA的[細胞核及](../Page/細胞核.md "wikilink")[粒線體的位置](../Page/粒線體.md "wikilink")。

常用的兩種染劑為**Hoechst 33258**與**Hoechst 33342**\[1\]
，這兩種染劑可在波長350奈米的[紫外光下激化](../Page/紫外光.md "wikilink")。不過赫斯特染劑是潛在的[突變原](../Page/突變原.md "wikilink")，處置上需要較為小心。


## 參考資料

[Category:染色着色剂](../Category/染色着色剂.md "wikilink")
[Category:荧光染料](../Category/荧光染料.md "wikilink")
[Category:DNA结合物质](../Category/DNA结合物质.md "wikilink")

1.