**赫里福德郡**（，\[1\]），[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[西米德蘭茲的](../Page/西米德蘭茲.md "wikilink")[名譽郡](../Page/名譽郡.md "wikilink")、[單一管理區](../Page/單一管理區.md "wikilink")，西接[威爾斯的邊界](../Page/威爾斯.md "wikilink")。以人口計算，[赫里福德是该郡最大](../Page/赫里福德.md "wikilink")[城市](../Page/英國的城市地位.md "wikilink")（亦是[郡治](../Page/郡治.md "wikilink")），[萊姆斯特是第](../Page/萊姆斯特.md "wikilink")1大鎮。

因為赫里福德郡沒有下轄任何[單一管理區](../Page/單一管理區.md "wikilink")（但它本身是單一管理區），無論把它看待成[名譽郡還是單一管理區](../Page/名譽郡.md "wikilink")，它都有177,800[人口](../Page/人口.md "wikilink")，[佔地](../Page/面積.md "wikilink")2,180[平方公里](../Page/平方公里.md "wikilink")。

## 歷史

《[1972年地方政府法案](../Page/1972年地方政府法案.md "wikilink")》在1974年4月1日生效，赫里福德郡與相鄰的[伍斯特郡合併成](../Page/伍斯特郡.md "wikilink")[赫里福德-伍斯特郡](../Page/赫里福德-伍斯特郡.md "wikilink")，成為[非都市郡](../Page/非都市郡.md "wikilink")。。1992年成立的[英格蘭地方政府委員會](../Page/英格蘭地方政府委員會_\(1992年\).md "wikilink")（Local
Government Commission for
England）建議赫里福德-伍斯特郡分拆回原狀，赫里福德郡的地位變成[單一管理區](../Page/單一管理區.md "wikilink")，範圍大概與先前的相約。

## 地理

下圖顯示英格蘭48個名譽郡的分佈情況。赫里福德郡，東與[伍斯特郡相鄰](../Page/伍斯特郡.md "wikilink")，東南與[格洛斯特郡相鄰](../Page/格洛斯特郡.md "wikilink")，西與[威爾斯的](../Page/威爾斯.md "wikilink")[波伊斯郡相鄰](../Page/波伊斯郡.md "wikilink")，北與[什羅普郡相鄰](../Page/什羅普郡.md "wikilink")。

## 人口統計

### 增長

1991年至2011年期間，赫里福德郡的人口增長為14.4%，比英國的平均比率10.0%為高。

| 年份   | 人口      | 增長率 % |
| ---- | ------- | ----- |
| 1991 | 160,400 |       |
| 2001 | 174,871 | 9.0%  |
| 2011 | 183,477 | 4.9%  |

### 種族

赫里福德郡的人口有98.2%是白人，亞洲人佔0.8%，混血的為0.7%，0.2%黑人，0.1%其他\[2\]。

## 註釋

[Category:英格蘭的郡](../Category/英格蘭的郡.md "wikilink")
[Category:英格蘭的單一管理區](../Category/英格蘭的單一管理區.md "wikilink")
[禧福郡](../Category/禧福郡.md "wikilink")

1.  見[香港](../Page/香港.md "wikilink")[九龍塘街道](../Page/九龍塘.md "wikilink")[禧福道](../Page/禧福道.md "wikilink")
    *Hereford Road*
2.