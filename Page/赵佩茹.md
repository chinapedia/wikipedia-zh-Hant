**趙佩茹**，[满族](../Page/满族.md "wikilink")\[1\]\[2\]，[相声艺人](../Page/相声.md "wikilink")，捧哏大师。拜相声前辈焦少海为师，学说相声。为同代相声演员中最早拜师学艺者，即“门长”。从1930年代起开始崭露头角，因其功底深厚，使活细致入微，倍受同行敬佩。

所表演作品超过110段，其中包括《[批三国](../Page/批三国.md "wikilink")》、《[写对子](../Page/写对子.md "wikilink")》；贯口类如《[大戏魔](../Page/大戏魔.md "wikilink")》、《[八扇屏](../Page/八扇屏.md "wikilink")》、《[大保镖](../Page/大保镖.md "wikilink")》；展现基本功类，如《[学四省](../Page/学四省.md "wikilink")》、《[学四相](../Page/学四相.md "wikilink")》、《[捧逗争哏](../Page/捧逗争哏.md "wikilink")》；子母哏类，如《[五红图](../Page/五红图.md "wikilink")》；歪唱类，如《[黄鹤楼](../Page/黄鹤楼.md "wikilink")》、《[全德报](../Page/全德报.md "wikilink")》；以及叙事类相声《[揭瓦](../Page/揭瓦.md "wikilink")》、《[醉点灯](../Page/醉点灯.md "wikilink")》等。

“[文革](../Page/文革.md "wikilink")”前，他与[马三立为搭档](../Page/马三立.md "wikilink")。

## 相声作品

  - 《[大戏魔](../Page/大戏魔.md "wikilink")》
  - 《[八扇屏](../Page/八扇屏.md "wikilink")》
  - 《[学四省](../Page/学四省.md "wikilink")》
  - 《[学四相](../Page/学四相.md "wikilink")》
  - 《[捧逗争哏](../Page/捧逗争哏.md "wikilink")》
  - 《[五红图](../Page/五红图.md "wikilink")》
  - 《[黄鹤楼](../Page/黄鹤楼.md "wikilink")》
  - 《[全德报](../Page/全德报.md "wikilink")》

## 參考資料

<div class="references-small">

<references />

</div>

`|width=30% align=center|`**`从师`**
`|width=40% align=center|`**`辈分`**
`|width=30% align=center|`**`收徒`**
`|-`

|width=30% align=center|[焦寿海](../Page/焦寿海.md "wikilink")

`|width=40% align=center|`**`宝`**
` |width=30% align=center |`[`李伯祥`](../Page/李伯祥.md "wikilink")`、`[`张伯华`](../Page/张伯华.md "wikilink")`、`[`马伯林`](../Page/马伯林.md "wikilink")`、`[`崔伯光`](../Page/崔伯光.md "wikilink")`、`[`杨伯英`](../Page/杨伯英.md "wikilink")`、`[`高英培`](../Page/高英培.md "wikilink")`、`[`常贵田`](../Page/常贵田.md "wikilink")`、`[`刘英华`](../Page/刘英华.md "wikilink")`、`[`杨英彩`](../Page/杨英彩.md "wikilink")`、`[`李世增`](../Page/李世增.md "wikilink")`、`[`李英杰`](../Page/李英杰.md "wikilink")`、`[`王祥林`](../Page/王祥林.md "wikilink")`、`[`马志存`](../Page/马志存.md "wikilink")`、`[`徐德奎`](../Page/徐德奎.md "wikilink")`、`[`张继英`](../Page/张继英.md "wikilink")`、`[`任鸣起`](../Page/任鸣起.md "wikilink")`、`[`刘英奇`](../Page/刘英奇.md "wikilink")`、`[`张奎清`](../Page/张奎清.md "wikilink")`、`[`张义勤`](../Page/张义勤.md "wikilink")`、`[`李浩然`](../Page/李浩然.md "wikilink")`、`[`郭士中`](../Page/郭士中.md "wikilink")`、`[`侯耀文`](../Page/侯耀文.md "wikilink")`、`[`许秀林`](../Page/许秀林.md "wikilink")`、`[`刘国器`](../Page/刘国器.md "wikilink")

|-

[Z趙](../Category/满族人.md "wikilink") [6](../Category/相聲演員.md "wikilink")
[P佩](../Category/趙姓.md "wikilink")

1.  《金启孮谈北京的满族》第151页，金启孮著，中华书局2009年版
2.  [吉祥满族：赵佩如](http://www.manchus.cn/plus/view.php?aid=2302)