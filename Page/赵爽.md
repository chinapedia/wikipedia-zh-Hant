**赵爽**，一名**婴**，字**君卿**，是[中国在](../Page/中国.md "wikilink")[三国时期](../Page/三国.md "wikilink")[吴国的](../Page/东吴.md "wikilink")[数学家](../Page/数学家.md "wikilink")。生卒年不详，是否生活在三国时代其实也受质疑，著有《[周髀算經注](../Page/周髀算經注.md "wikilink")》，即对《[周髀算經](../Page/周髀算經.md "wikilink")》的详细注释。

[缩略图](https://zh.wikipedia.org/wiki/File:Xtcn.jpg "fig:缩略图")

## 生平

依记载赵爽曾研究过[东汉](../Page/东汉.md "wikilink")[张衡关于](../Page/张衡.md "wikilink")[天文学的著作](../Page/天文学.md "wikilink")《[灵宪](../Page/灵宪.md "wikilink")》和[刘洪的](../Page/刘洪.md "wikilink")《[乾象历](../Page/乾象历.md "wikilink")》。

约在公元222年，赵爽深入研究《周牌算经》，并写了序言及详细注释，其中有530余字对《勾股圆方图》的注文，即《勾股圆方图说》，是数学史上具有价值的文献。

## 数学上的贡献

[Chinese_pythagoras.jpg](https://zh.wikipedia.org/wiki/File:Chinese_pythagoras.jpg "fig:Chinese_pythagoras.jpg")
（一）[周朝的](../Page/周朝.md "wikilink")《周髀算經》内有勾股定理及《勾股圆方图》，但没有证明定理。而赵爽在《[周髀算經注](../Page/周髀算經注.md "wikilink")》中有《勾股圆方图说》，解释并证明了[勾股定理](../Page/勾股定理.md "wikilink")。

《勾股圆方图说》的内容有：

  - “勾股各自乘，併之，为弦实。开方除之，即弦。”

解:

  - **“勾”**、**“股”**为[直角三角形的二](../Page/直角三角形.md "wikilink")[直角边](../Page/直角.md "wikilink")[边长](../Page/边长.md "wikilink")。现代[数学多以](../Page/数学.md "wikilink")\(\ a\)及\(\ b\)代表。
  - **“勾股各自乘，併之，为弦实。”**是指\(\ a^2+b^2=c^2\)，即现代的勾股定理[公式](../Page/公式.md "wikilink")。
  - **“弦”**为直角三角形的斜边边长；现代[数学多以](../Page/数学.md "wikilink")\(\ c\)表示。
  - **“开方除之，即弦。”**，开方是找出[平方根](../Page/平方根.md "wikilink")，全句是指\(\sqrt{c^2} = c\)。

证明方法为“按弦图，又可以勾股相乘为朱实二，倍之为朱实四，以勾股之差自相乘为中黄实，加差实，亦成弦实。”

  - 即是 \(\ 2ab+(b-a)^2=c^2\) 进行演算后将形成 \(\ a^2+b^2=c^2\)

[缩略图](https://zh.wikipedia.org/wiki/File:Phzscn.gif "fig:缩略图")

（二）创新[二次方程解法](../Page/二次方程.md "wikilink")，比[法国](../Page/法国.md "wikilink")[数学家](../Page/数学家.md "wikilink")[韋達创立类似的](../Page/法蘭西斯·韋達.md "wikilink")《[韦达定理](../Page/韦达定理.md "wikilink")》早了1300余年。

（三）将《[九章算术](../Page/九章算术.md "wikilink")》中的[分数](../Page/分数.md "wikilink")[运算整理成](../Page/四则运算.md "wikilink")[理论](../Page/理论.md "wikilink")；并创出《齐同术》，即是当分数进行[加](../Page/加.md "wikilink")[减运算时](../Page/减.md "wikilink")，将异分母化成同分母，然后以分子进行加减运算。

## 参考资料

  - 《中国古代文化知识辞典》（1991年4月第一版，江西教育出版社[出版](../Page/出版.md "wikilink")）

[Z赵](../Category/中国数学家.md "wikilink")
[Z赵](../Category/東吳人.md "wikilink")
[Category:趙姓](../Category/趙姓.md "wikilink")