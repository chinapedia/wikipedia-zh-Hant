**赵玉明**（，）是[罗马天主教](../Page/罗马天主教.md "wikilink")[主教](../Page/主教.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")[魁北克外方傳教會](../Page/魁北克外方傳教會.md "wikilink")（P.M.E.）会士。

## 生平

1914年10月25日，赵玉明出生于[法国](../Page/法国.md "wikilink")。1938年6月29日成为[神父](../Page/神父.md "wikilink")。1946年11月28日被任命为中国东北[天主教林东监牧区宗座监牧](../Page/天主教林东监牧区.md "wikilink")。由于当地为共产党占领，赵玉明无法开展教务。教廷公使[黎培理在上海创立全国天主教教务协进会](../Page/黎培理.md "wikilink")，任命他为副秘书长。

1951年9月6日，赵玉明与[陳哲敏](../Page/陳哲敏.md "wikilink")、[沈士賢](../Page/沈士賢.md "wikilink")、侯之正，高樂康（Le
Grand）同時在上海天主教協進會被捕。1954年被驱逐出境\[1\]，1956年11月11日，教宗庇護十二任命他為[祕魯](../Page/祕魯.md "wikilink")[普兰尔帕代牧區主教](../Page/普兰尔帕.md "wikilink")，1957年1月6日祝圣。1989年10月23日退休，住在加拿大魁北克外方傳教會會院。2005年11月13日去世，享年91岁\[2\]。

## 参考文献

[Z](../Category/在华天主教传教士.md "wikilink")
[Z](../Category/法裔加拿大人.md "wikilink")

1.  述良：[陳哲敏神父的迴響](http://www.catholic.org.tw/cathlife/2002/2475/22.htm)
2.  [林东监牧区](http://www.gcatholic.com/dioceses/diocese/lint1.htm)