[Li_Mu1.jpg](https://zh.wikipedia.org/wiki/File:Li_Mu1.jpg "fig:Li_Mu1.jpg")

**赵郡李氏**，是[中国歷史的中古时代一个以](../Page/中国歷史.md "wikilink")[赵郡](../Page/赵郡.md "wikilink")（含今[河北](../Page/河北.md "wikilink")[赵县](../Page/赵县.md "wikilink")、[隆尧县](../Page/隆尧县.md "wikilink")、[元氏县](../Page/元氏县.md "wikilink")、[高邑县](../Page/高邑县.md "wikilink")、[赞皇县](../Page/赞皇县.md "wikilink")、[临城县等地](../Page/临城县.md "wikilink")）为郡望的著名[士族](../Page/士族.md "wikilink")。赵郡李氏以战国名将[李牧为始祖](../Page/李牧.md "wikilink")\[1\]。

赵郡李氏在[南北朝官位显赫](../Page/南北朝.md "wikilink")，在唐朝被列为[七姓十家的禁婚家](../Page/七姓十家.md "wikilink")，有多人出任[宰相](../Page/宰相.md "wikilink")。

## 简介

魏晋南北朝时期赵郡李氏与[陇西李氏](../Page/陇西李氏.md "wikilink")、[清河崔氏](../Page/清河崔氏.md "wikilink")、[博陵崔氏](../Page/博陵崔氏.md "wikilink")、[范阳卢氏](../Page/范阳卢氏.md "wikilink")、[荥阳郑氏](../Page/荥阳郑氏.md "wikilink")、[太原王氏并称](../Page/太原王氏.md "wikilink")[五姓](../Page/五姓.md "wikilink")，支派繁多，文化鼎盛，官位显赫。到了唐高宗时颁布法令禁止这五姓七家自行婚娶。

赵郡李氏东南西三房始祖李楷，生有五子：李辑、李晃、李棨、李劲、李睿，定居在常山，兄弟分居：老五李睿的儿子李勖兄弟，居住在巷东；老四李劲的儿子李盛兄弟及老三李棨一家，居住在巷西；老大李辑与老二李晃两房子孙，则南徙故垒。因而，以此三个方位，李睿又被奉为赵郡李氏的东祖；李棨与李劲被奉为西祖；李辑与李晃则被奉为南祖。

## 郡望

赵郡治所位于今河北赵县。赵郡，其初建置于后魏，此后各朝有沿置，其治在今河北之赵县。

## 房支

  - 南祖
  - 東祖
  - 西祖

## 争议

李唐自称出自[陇西李氏](../Page/陇西李氏.md "wikilink")，以西凉李皓的嫡裔自居，但[陈寅恪](../Page/陈寅恪.md "wikilink")《唐代政治史述论稿》以及《李唐氏族推测》等三文，认为李唐冒称陇西，实为赵郡李氏破落户。今河北[隆尧唐祖陵亦为佐证](../Page/隆尧唐祖陵.md "wikilink")。

[朱希祖认为](../Page/朱希祖.md "wikilink")[李熙曾镇戍武川](../Page/李熙.md "wikilink")，后来其子[李天锡为避](../Page/李天锡.md "wikilink")[六镇兵乱](../Page/六镇起义.md "wikilink")，携父遺骨南迁于赵郡广阿，因以为家，不久亦卒。其子李虎将父祖合葬，即所谓[唐祖陵](../Page/隆尧唐祖陵.md "wikilink")。李氏并非出身赵郡李氏，而确系为[隴西李氏](../Page/隴西李氏.md "wikilink")\[2\]。

汪荣祖《陈寅恪评传》认为朱希祖对陈寅恪观点的批判大多在枝节上做文章，没有举出对立的新资料，而陈演恪晚年也不满意李唐先世三论的研究，出于汰芜存精，将之删除屏弃\[3\]。

## 唐朝宰相

  - [李嶠](../Page/李嶠.md "wikilink")
  - [李懷遠](../Page/李懷遠.md "wikilink")
  - [李游道](../Page/李游道.md "wikilink")
  - [李珏](../Page/李珏.md "wikilink")
  - [李藩](../Page/李藩.md "wikilink")
  - [李固言](../Page/李固言.md "wikilink")
  - [李紳](../Page/李紳.md "wikilink")
  - [李吉甫](../Page/李吉甫.md "wikilink")
  - [李德裕](../Page/李德裕.md "wikilink")
  - [李絳](../Page/李絳.md "wikilink")

## 參見

  - [李楷](../Page/李楷.md "wikilink")
  - [李頎](../Page/李頎.md "wikilink")

## 参考

[趙郡李氏](../Category/趙郡李氏.md "wikilink")

1.
2.  参见朱希祖〈驳李唐为胡姓说〉〈再驳李唐氏族出于李初古拔及赵郡说〉，收入《中国史学通论》，商务印书馆，2015-07-01。
3.  汪荣祖《陈寅恪评传》，第七章 为不古不今之学——唐史研究，百花洲文艺出版社，1997年12月