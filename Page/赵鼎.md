[Tianya_Haijiao_009.JPG](https://zh.wikipedia.org/wiki/File:Tianya_Haijiao_009.JPG "fig:Tianya_Haijiao_009.JPG")
**趙鼎**，字元鎮，自號得全居士，[解州聞喜](../Page/解州.md "wikilink")（今[山西省](../Page/山西省.md "wikilink")[聞喜縣](../Page/聞喜縣.md "wikilink")）人，[宋高宗時](../Page/宋高宗.md "wikilink")[宰相](../Page/宰相.md "wikilink")。與[李綱](../Page/李綱.md "wikilink")、[胡銓](../Page/胡銓.md "wikilink")、[李光並稱](../Page/李光.md "wikilink")「南宋四名臣」。

## 生平

[宋神宗](../Page/宋神宗.md "wikilink")[元豐八年](../Page/元丰_\(北宋\).md "wikilink")（1085年）出生，早孤，由母樊氏教之。[宋徽宗崇寧五年](../Page/宋徽宗.md "wikilink")（1106年）進士，對策斥[章惇誤國](../Page/章惇.md "wikilink")。曾任[河南府](../Page/河南府.md "wikilink")[洛陽令](../Page/洛陽縣.md "wikilink")、[開封府士曹等職](../Page/開封府.md "wikilink")。

後隨[宋高宗南渡](../Page/宋高宗.md "wikilink")，累官[殿中侍御史](../Page/殿中侍御史.md "wikilink")，陳四十事。绍兴四年（1134年）任[尚书右仆射兼知](../Page/尚书右仆射.md "wikilink")[枢密院事](../Page/枢密院.md "wikilink")，隔年为宰相。因力主抗金而被去職，後被[秦檜陷害](../Page/秦檜.md "wikilink")，貶謫至[泉州](../Page/泉州.md "wikilink")（即今[福建](../Page/福建.md "wikilink")[泉州](../Page/泉州.md "wikilink")）、[潮州](../Page/潮州.md "wikilink")（今屬[廣東](../Page/廣東.md "wikilink")[潮州](../Page/潮州.md "wikilink")），再貶至[海南岛吉陽軍](../Page/海南岛.md "wikilink")，知州[王惕](../Page/王惕.md "wikilink")“假肩与以送”，秦桧闻知，谪王惕[金州](../Page/金州.md "wikilink")。流寓三年，“深居簡出，杜門謝訪”，以抗議秦檜的迫害，秦檜深感膽寒，“此老倔强犹者”。後有[張宗元遣人持诗书](../Page/張宗元.md "wikilink")、药石前往慰问\[1\]，不久张宗元被調離[廣南西路](../Page/廣南西路.md "wikilink")。十七年（1147年）八月，絕食而死，临终前自书墓石：「身騎箕尾歸天上，氣作山河壯本朝」，葬於[昌化縣舊縣村](../Page/昌化縣.md "wikilink")。二十六年（1156年）正月，追复特进、观文殿大学士\[2\]\[3\]。[孝宗即位](../Page/宋孝宗.md "wikilink")，追諡**忠簡**，封豐國公。有《得全集》。

## 注釋

## 參考書目

  - 昌彼得等編，《宋人傳記資料索引》（臺北：鼎文書局，1975），第四冊，頁3355－3357。
  - 徐邦達，〈趙鼎郡寄帖〉，《古書畫過眼要錄》（長沙：湖南美術出版社，1987），頁422－423。
  - [岳飛](../Page/岳飛.md "wikilink")《與趙忠簡書》

[Category:宋朝宰相](../Category/宋朝宰相.md "wikilink")
[Category:南宋國公](../Category/南宋國公.md "wikilink")
[Category:宋朝進士](../Category/宋朝進士.md "wikilink")
[Category:死于绝食的南宋人](../Category/死于绝食的南宋人.md "wikilink")
[Category:諡忠簡](../Category/諡忠簡.md "wikilink")
[Category:闻喜人](../Category/闻喜人.md "wikilink")
[d](../Category/赵姓.md "wikilink")

1.  《续资治通鉴·宋纪一百二十七》：“鼎在吉陽三年，故吏門人皆不敢通問。廣南西路經略安抚使張宗元時遣使渡海，以醪米饋之。”
2.  《建炎以来系年要录》卷一七一
3.  《宋会要辑稿 》职官七六之追复旧官