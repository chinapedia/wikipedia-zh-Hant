《**超人力霸王THE
NEXT**》（原題：ULTRAMAN），是2004年12月18日在[日本上映](../Page/日本.md "wikilink")，由[圓谷製作與](../Page/圓谷製作.md "wikilink")[松竹映畫所製作的](../Page/松竹映畫.md "wikilink")[超人力霸王電影作品](../Page/超人力霸王.md "wikilink")，「ULTRAMAN
N PROJECT」作品之一。

## 劇情簡介

海上自衛隊隊員有働貴文，在調查未確認飛行物體時突然與藍色發光體「BLUE」衝撞，之後竟化為兇惡的怪獸「THE ONE」。

從小憧憬著[戰鬥機](../Page/戰鬥機.md "wikilink")，而成為[自衛隊飛官的真木舜一](../Page/自衛隊.md "wikilink")，為了自己因血液病變隨時會死亡的兒子而提前退役，但是在退役前的最後一天，突然發生緊急升空迎擊的緊急事態，他的戰鬥機與不明紅色發光體「RED」撞擊，而在模糊的意識中，他看到了銀色巨人。而奇蹟般生還的真木，卻被將他視為危險人物的特殊機關「BCST」盯上，並進行強制監視。

這時THE ONE開始現身襲擊，但面對進化的怪獸卻無反擊之力。這時，逃出監視的真木現身了。雖然在THE
ONE的攻擊下撞上牆壁，但他卻並未喪命，且變身為超人「ULTRAMAN
THE NEXT」，兩者展開了對決。但因為NEXT的變身型態並不完全無法發揮全力，結果消耗了大量體力並讓THE ONE逃逸。

而後真木發現兒子繼夢病倒，說服了BCST前往醫院探視兒子的病情。真木與繼夢父子間訂下了一個約定。而THE
ONE這時卻也在附於老鼠身上之後巨大化，就在人類面臨危機時，成為完全體的ULTRAMAN
THE NEXT出現了，以新宿為舞台，與THE ONE展開殊死戰。

## 電影特色

本片擺脫以往[超人力霸王](../Page/超人力霸王.md "wikilink")[超級英雄式的劇情](../Page/超級英雄.md "wikilink")，以寫實手法呈現，主角不是熱血少年，而是只想與妻兒安心過日子的平凡中年男子。以往圓谷公司較弱的[CG合成](../Page/CG.md "wikilink")，在本作表現有顯著改善，特別是空戰特效部份，作品邀請到在動畫界以「[板野馬戲團](../Page/板野馬戲團.md "wikilink")」手法著稱的[板野一郎執導](../Page/板野一郎.md "wikilink")。

## 本作特色

本部作品以大量的電腦特效製作出空戰的場景，也是超人力霸王史上經歷最久的空中戰鬥。地面戰較少，且超人力霸王的身體紋路不是用普通的色彩而是用肌肉紋路取代，讓人感到充滿力量。

## 製作群

  - 監修：[圓谷一夫](../Page/圓谷一夫.md "wikilink")
  - 監製：鈴木清
  - 編劇：長谷川圭一
  - 音樂監製：玉川靜
  - 音樂：小澤正澄、池田大介、鐮田真吾
  - [攝影](../Page/攝影.md "wikilink")‧[VFX顧問](../Page/VFX.md "wikilink")：大岡新一
  - 美術：大澤折三
  - 剪輯：松木朗
  - 音樂監督：TAK MATSUMOTO(松本孝弘／[B'z](../Page/B'z.md "wikilink"))
  - [空戰飛行畫面導演](../Page/空戰.md "wikilink")：[板野一郎](../Page/板野一郎.md "wikilink")
  - 特技監督：菊池雄一
  - 導演：小中和哉

## 主要演員

  - 真木舜一：別所哲也
  - 水原沙羅：遠山景織子
  - 有働貴文：大澄賢也
  - 真木蓉子：裕木奈江
  - 真木継夢：広田亮平
  - 倉島剛：永澤俊矢
  - 曽我部一佐：[隆大介](../Page/隆大介.md "wikilink")
  - \-{万}-城目社長：草刈正雄
  - 新聞主播：駒田健吾
  - \-{超人}-（配音）：田中秀幸
  - THE ONE（配音）：大澄賢也、[小西克幸](../Page/小西克幸.md "wikilink")

## 相关条目

  - [超人力霸王系列](../Page/超人力霸王系列.md "wikilink")
  - [超人力霸王納克斯](../Page/超人力霸王納克斯.md "wikilink")

## 外部链接

  - [電影官網](https://web.archive.org/web/20060828120454/http://www.ultraman-movie.com/ultraman/)

[Category:ULTRA N PROJECT](../Category/ULTRA_N_PROJECT.md "wikilink")
[Category:超人力霸王系列電影作品](../Category/超人力霸王系列電影作品.md "wikilink")
[Category:2004年電影](../Category/2004年電影.md "wikilink")
[Category:新宿背景電影](../Category/新宿背景電影.md "wikilink")
[Category:重製特攝電影](../Category/重製特攝電影.md "wikilink")
[Category:日本自衛隊電影](../Category/日本自衛隊電影.md "wikilink")
[Category:小中和哉電影](../Category/小中和哉電影.md "wikilink")