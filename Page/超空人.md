《**超空人**》（原題：*Captain Scarlet and the Mysterons*）為傑瑞‧安德森（[Gerry
Anderson](../Page/Gerry_Anderson.md "wikilink")）的[Century 21
Productions製作](../Page/Century_21_Productions.md "wikilink")，ITC發行，1967年9月29日～1968年5月14日於[英國ITV電視台所播出的](../Page/英國.md "wikilink")[科幻](../Page/科幻.md "wikilink")[人偶](../Page/人偶.md "wikilink")[影集](../Page/影集.md "wikilink")，共32集。[台灣最早於](../Page/台灣.md "wikilink")1968年（[民國57年](../Page/民國57年.md "wikilink")）10月27日至1969年（[民國58年](../Page/民國58年.md "wikilink")）6月8日間，於每週日在[台視播出](../Page/台視.md "wikilink")。後來[中視在](../Page/中視.md "wikilink")1973年（[民國62年](../Page/民國62年.md "wikilink")）3月5日，於每週一～五、日再放映時，易名為《[雷霆機](../Page/雷霆機.md "wikilink")\[1\]》，並於同年12月15日至1974年（[民國63年](../Page/民國63年.md "wikilink")）1月4日間，於每週一～五、日間配上[國語](../Page/國語.md "wikilink")[配音重播](../Page/配音.md "wikilink")；2004年[巨圖科技](http://www.catalyst.com.tw/)在台發行DVD譯名仍延用前者。2005年由原製作人傑瑞‧安德森全新製作CG動畫版本《新超空人》（Gerry
Anderson's New Captain Scarlet）於[英國播出兩季結束](../Page/英國.md "wikilink")。

## 故事概要

在一百年以後的廿一世紀。那時地球上有一個「宇宙情報局（USS＝Universal Secret
Service）」，專司策劃蒐集宇宙間各星球之情報，隸屬其下的
「七彩諜報隊」（Spectrum）則負責保護地球人類的安全。該隊以「雲霄基地（Cloud
Base）」為中心，經常對地球和[外太空作例行偵巡任務](../Page/外太空.md "wikilink")，找尋可能對地球不利之陰謀份子。

[火星偵察行計中](../Page/火星.md "wikilink")，終於發生了不幸事件。黑上尉誤以為[火星隱形人是敵人](../Page/火星.md "wikilink")，而主動摧毀他們在[火星的基地](../Page/火星.md "wikilink")，導致**[火星隱形人](../Page/火星.md "wikilink")**（Mysterons）的統治者認為[地球人具有敵意](../Page/地球.md "wikilink")，企圖侵略他們，於是主動在[地球展開一連串報復性的攻擊不斷發動心理性的神經戰](../Page/地球.md "wikilink")，於是「[地球](../Page/地球.md "wikilink")」與「[火星](../Page/火星.md "wikilink")」之間的[間諜戰自此揭開](../Page/間諜.md "wikilink")。

戰事的進行是龐大而複雜的，包括「有形」的，「無形」的「[神經](../Page/神經.md "wikilink")」戰，其特色是無形的威脅感強烈。地球方面的主力，是以「七彩諜報隊」為骨幹，其組成份子有諜報員與飛行員。諜報員全屬[男性](../Page/男性.md "wikilink")，顧名思義，每一諜報員以七彩中的一種色彩作為代號。而飛行員則由來自美、英、法、中的五位佳麗擔任。她們所駕駛的「天使」戰鬥機隊為七彩諜報取最堅強的一支空中武力。她們與諜報員密切配合，把地球的防禦工作做得天衣無縫。火星方面的主力僅將原屬七彩諜報隊之隊員黑上尉予以控制作為火星間諜，在地球上從事有形的破壞。大部份戰鬥均由**[火星隱形人](../Page/火星.md "wikilink")**所操縱。他來去無蹤，行動飄忽，而且能直接從火星向地球發動[神經戰](../Page/神經戰.md "wikilink")。

## 主要角色

### 七彩諜報隊（Spectrum）

  - **白上校**（Colonel White）

<!-- end list -->

  -
    *本名：查理·葛雷（Charles
    Grey）*。是七彩諜報隊的指揮官。[英國人](../Page/英國.md "wikilink")。曾受高等教育，對[計算機](../Page/計算機.md "wikilink")。[航行學及](../Page/航行學.md "wikilink")[理工造諳很深](../Page/理工.md "wikilink")。在世界[海軍服務期間](../Page/海軍.md "wikilink")，素以敢作敢為，頭腦冷靜及領導有方著稱。

<!-- end list -->

  - **紅上尉**（Captain Scarlet）

<!-- end list -->

  -
    *本名：保羅·麥特凱夫（Paul
    Metcalfe）*。七彩諜報隊的首席諜報員。他原本也受到火星人控制，卻在意外中恢復了自我意識，從此成為不死之身，大凡所有艱險任務均由他擔任，是直接對抗火星隱形人陰謀的領導人物。為[英國名將後裔](../Page/英國.md "wikilink")，曾獲[工程](../Page/工程.md "wikilink")、[歷史及](../Page/歷史.md "wikilink")[應用數學等學位](../Page/應用數學.md "wikilink")。並曾接受嚴格之軍事訓練。眼神看似冰冷，其實私下十分有幽默感。

<!-- end list -->

  - **藍上尉**（Captain Blue）

<!-- end list -->

  -
    *本名：亞當·沙文生（Adam
    Svenson）*。七彩諜報隊的首席諜報員。出生在[美國](../Page/美國.md "wikilink")[波士頓城](../Page/波士頓.md "wikilink")。是[經濟學](../Page/經濟學.md "wikilink")、[理工](../Page/理工.md "wikilink")、[計算機](../Page/計算機.md "wikilink")、[應用力學和](../Page/應用力學.md "wikilink")[空氣動力學專家](../Page/空氣動力學.md "wikilink")，曾擔任[試飛員](../Page/試飛員.md "wikilink")，智勇雙全，在世界航空學會裏享有盛譽。後來調往安全機構任情報員，因建功而被遴選為七彩諜報隊隊員，他個性突出。忍耐力特強，喜愛戶外生活。

<!-- end list -->

  - **黑上尉**（Captain Black）

<!-- end list -->

  -
    *本名：康拉德·透納（Conrad
    Turner）*。原為七彩隊員，其機智才幹不下於紅上尉，因心神為火星隱形人所控制，淪為替火星工作。出賣地球的間諜。

<!-- end list -->

  - **棕上尉**（Captain Ochre）

<!-- end list -->

  -
    *本名：李察·福雷瑟（Richard
    Fraser）*。[美國人](../Page/美國.md "wikilink")，但沒有像其他同僚們一樣顯赫的學歷。他醉心於飛行，十六歲就取得飛行執照，曾任職警界。他機警，善表達。愛講笑話，暇時愛玩[模型飛機](../Page/模型.md "wikilink")，對漂亮女孩有吸引力。

<!-- end list -->

  - **赤上尉**（Captain Magenta）

<!-- end list -->

  -
    *本名：派翠克·唐納（Patrick
    Donaghue）*。原籍[愛爾蘭](../Page/愛爾蘭.md "wikilink")，生在[紐約貧民區](../Page/紐約.md "wikilink")，受母親鼓勵，得以工讀方式在[耶魯大學完成學業](../Page/耶魯大學.md "wikilink")，專攻[物理](../Page/物理.md "wikilink")、[電機及](../Page/電機.md "wikilink")[理工](../Page/理工.md "wikilink")，曾因與極端份子有牽連而淪為[黑社會首腦](../Page/黑社會.md "wikilink")。[紐約大部份](../Page/紐約.md "wikilink")[黑社會組織都在他掌握之下](../Page/黑社會.md "wikilink")。七彩諜報隊因為他在[黑社會中聲望高](../Page/黑社會.md "wikilink")，能直接深入反犯罪惡活動中心。所以竭力爭取。他對七彩隊根忠誠，七彩隊對其也很器重。

<!-- end list -->

  - **灰上尉**（Captain Grey）

<!-- end list -->

  -
    *本名：布雷迪·荷登（Bradley
    Holden）*。[美國](../Page/美國.md "wikilink")[芝加哥人](../Page/芝加哥.md "wikilink")，原服務海軍，曾因戰績輝煌，調職[海底安全巡邏隊](../Page/海底安全巡邏隊.md "wikilink")，任「[霹靂艇](../Page/霹靂艇.md "wikilink")」艦長。因故受傷停職，兩年後傷癒，乃加入七彩諜報隊。他的特長，是處理棘手問題井然有序。思想敏捷，富敵情觀念，反應迅速，善於預測危機，喜歡游泳。

<!-- end list -->

  - **？上尉**（Captain Brown）

<!-- end list -->

  -
    *本名：不明……*

<!-- end list -->

  - **？上尉**（Captain Indigo）

<!-- end list -->

  -
    *本名：不明……*

<!-- end list -->

  - **綠中尉**（Lieutenant Green）

<!-- end list -->

  -
    *本名：西摩·葛瑞菲（Seymour
    Griffiths）*。出生[波多黎各的黑人](../Page/波多黎各.md "wikilink")，是白上校的左右手。所有白上校的命令指示都經由他發佈，大學畢業，為人機警。鎮定、風趣，對[音樂造諳很高](../Page/音樂.md "wikilink")。

<!-- end list -->

  - **黃大夫**（Doctor Fawn ）

<!-- end list -->

  -
    *本名：愛德華·威爾基（Edward
    Wilkie）*。[澳洲人](../Page/澳洲.md "wikilink")，為七彩隊醫務處長，負責隊員保健及太空醫學之研究工作。曾獲[生物及](../Page/生物.md "wikilink")[醫學學位](../Page/醫學.md "wikilink")。

#### 天使戰鬥機隊

  - **戴妮**（Destiny Angel，台譯**命運**天使機）

<!-- end list -->

  -
    *本名：茱麗葉·朋頓（Juliette
    Pontoin）。*[法國人](../Page/法國.md "wikilink")，褐瞳，亞麻色頭髮，常紮成雙束。帶法國人特有的口音。

<!-- end list -->

  - **申妮**（Symphony Angel，台譯**交響曲**天使機）

<!-- end list -->

  -
    *本名：凱玲·溫萊特（Karen
    Wainwright）*。[美國人](../Page/美國.md "wikilink")，綠瞳，金褐色頭髮，常變換髮型。藍上尉的女友。

<!-- end list -->

  - **梅妮**（Rhapsody Angel，台譯**狂想曲**天使機）

<!-- end list -->

  -
    *本名：戴安娜·西蒙（Dianne Simms）*。[美國人](../Page/美國.md "wikilink")，藍瞳，橘色長髮。

<!-- end list -->

  - **雷妮**（Melody Angel，台譯**旋律**天使機）

<!-- end list -->

  -
    *本名：梅格羅莉亞·瓊斯（Magnolia
    Jones）*。[英國人](../Page/英國.md "wikilink")，黑瞳，黑色短髮，黑皮膚。

<!-- end list -->

  - **哈妮**（Harmony Angel，台譯**和諧**天使機）

<!-- end list -->

  -
    *本名：陳光（Chan
    Kwan）*。[華人](../Page/華人.md "wikilink")，出生於[日本](../Page/日本.md "wikilink")，黑瞳，黑色短髮，黃皮膚。

## 主要裝備

  - **雲霄基地**（Cloud Base）

<!-- end list -->

  -

::\***天使攔截機**（Angel Interceptor）

  -

      -

::\***七彩驅逐車**（Spectrum Pursuit Vehicle）

  -

      -

::\***七彩雅座車**（Spectrum Saloon Car）

  -

      -

::\***七彩偵測車**（Spectrum Detector Truck）

  -

      -

::\***七彩直昇機**（Spectrum Helicopter）

  -

      -

::\***七彩氣墊船**（Spectrum Hovercraft）

  -

      -

::\***七彩噴射客機**（Spectrum Passenger Jet）

  -

      -

::\***極上安全車**（Maximum Security Vehicle）

  -

      -

::\***超級直昇機**（Magnacopter）

  -

      -

::\***黃狐狸安全車**（Yellow Fox Security Tanker）

  -

      -

## 劇集列表

<small><sup>※</sup>以下排列為最初於[英國首播之際的順序](../Page/英國.md "wikilink")；在[台灣放映時](../Page/台灣.md "wikilink")[電視台均有再各自施以調整](../Page/電視台.md "wikilink")。之後本作在2003年由Carlton公司推出《超空人》的〔數位復刻版〕DVD全集，集數與英國首播時之排列有所不同。</small>

| [英國](../Page/英國.md "wikilink") | [台灣](../Page/台灣.md "wikilink") |
| ------------------------------ | ------------------------------ |
| 集序                             | 原題                             |
| 1                              | The Mysterons                  |
| 2                              | Winged Assassin                |
| 3                              | Big Ben Strikes Again          |
| 4                              | Manhunt                        |
| 5                              | Avalanche                      |
| 6                              | White As Snow                  |
| 7                              | The Trap                       |
| 8                              | Operation Time                 |
| 9                              | Spectrum Strikes Back          |
| 10                             | Special Assignment             |
| 11                             | The Heart Of New York          |
| 12                             | Lunarville 7                   |
| 13                             | Point 783                      |
| 14                             | Model Spy                      |
| 15                             | Seek And Destroy               |
| 16                             | Renegade Rocket                |
| 17                             | Crater 101                     |
| 18                             | Shadow Of Fear                 |
| 19                             | Dangerous Rendezvous           |
| 20                             | Fire At Rig 15                 |
| 21                             | Treble Cross                   |
| 22                             | Flight 104                     |
| 23                             | Place Of Angels                |
| 24                             | Noose Of Ice                   |
| 25                             | Expo 2068                      |
| 26                             | The Launching                  |
| 27                             | Codename Europa                |
| 28                             | Inferno                        |
| 29                             | Traitor                        |
| 30                             | Flight To Atlantica            |
| 31                             | Attack On Cloudbase            |
| 32                             | The Inquisition                |
|                                |                                |

## 註釋

## 相關作品

以下均為傑瑞‧安德森於1960～70年代所製作的[超級人偶劇](../Page/超級人偶劇.md "wikilink")（Supermarionation）系列。

  - 《[萬能車](../Page/萬能車.md "wikilink")》（Supercar）
  - 《[雷霆機](../Page/雷霆機.md "wikilink")》（Fireball XL5）
  - 《[霹靂艇](../Page/霹靂艇.md "wikilink")》（Stingray）
  - 《[雷鳥神機隊](../Page/雷鳥神機隊.md "wikilink")》（Thunderbirds）
  - 《[小飛諜](../Page/小飛諜.md "wikilink")》（Joe 90'）
  - 《[袖珍密諜](../Page/袖珍密諜.md "wikilink")》（The Secret Service）

## 翻拍版本

  - 《新超空人》（Gerry Anderson's New Captain Scarlet）

　本作的CG動畫重拍版本，仍由傑瑞‧安德森製作。

## 延伸閱讀

  - [雷霆發威](../Page/雷霆發威.md "wikilink")

<!-- end list -->

  -

## 相關條目

  - [特攝](../Page/特攝.md "wikilink")

## 外部連結

  - [巨圖科技](http://www.catalyst.com.tw/)
  - [五年級生的往日時光－超空人](https://web.archive.org/web/20071009101106/http://w2.t1catv.com.tw/jackie/voidzone/page1.htm)
  - [台灣電視資料庫－超空人](http://tv.nccu.edu.tw/radioProgram_preview.htm?PROGRAMNAME=%B6W%AA%C5%A4H&STATION=&YEAR=1964&MONTH=1&DAY=1&YEAR2=2002&MONTH2=12&DAY2=31&CATALOG=&COUNTRY=)
  - [台灣電視資料庫－雷霆機（中視）](http://tv.nccu.edu.tw/radioProgram_preview.htm?PROGRAMNAME=%B9p%BE%5E%BE%F7&STATION=%A4%A4%B5%F8&YEAR=1964&MONTH=1&DAY=1&YEAR2=2002&MONTH2=12&DAY2=31&CATALOG=&COUNTRY=&Login=%ACd%B8%DF)

[Category:週刊少年Sunday](../Category/週刊少年Sunday.md "wikilink")
[Category:英國電視劇](../Category/英國電視劇.md "wikilink")
[Category:傀儡戲](../Category/傀儡戲.md "wikilink")
[Category:1967年電視劇集](../Category/1967年電視劇集.md "wikilink")
[超空人](../Category/1960年代特攝作品.md "wikilink")
[Category:ITV電視節目](../Category/ITV電視節目.md "wikilink")
[Category:未來題材電視劇](../Category/未來題材電視劇.md "wikilink")
[Category:航空题材电视剧](../Category/航空题材电视剧.md "wikilink")
[Category:外星生命入侵地球題材電視劇](../Category/外星生命入侵地球題材電視劇.md "wikilink")
[Category:月球背景作品](../Category/月球背景作品.md "wikilink")
[Category:火星背景作品](../Category/火星背景作品.md "wikilink")
[Category:1960年代英国电视剧](../Category/1960年代英国电视剧.md "wikilink")

1.  早於[中視重播本劇之前](../Page/中視.md "wikilink")，[台視在](../Page/台視.md "wikilink")1967年（[民國56年](../Page/民國56年.md "wikilink")）播出《[Fireball
    XL5](../Page/w:Fireball_XL5.md "wikilink")》時，就已曾使用過「[雷霆機](../Page/雷霆機.md "wikilink")」一名。