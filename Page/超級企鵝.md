是一款模仿《[超级马里奥兄弟](../Page/超级马里奥兄弟.md "wikilink")》的電腦遊戲。最初是由Bill
Kendrick所開發，現在則由超級企鵝開發小組負責維護。

在這款遊戲中，主角是[Linux的](../Page/Linux.md "wikilink")[吉祥物](../Page/吉祥物.md "wikilink")[企鵝](../Page/tux.md "wikilink")。遊戲中的大量圖畫是由Ingo
Ruhnke所創作，他同時是[Pingus的創作者](../Page/Pingus.md "wikilink")。《超級企鵝》這款遊戲完全使用[GPL發行的](../Page/GNU_General_Public_License.md "wikilink")，這代表別人可隨意由這遊戲製作衍生遊戲，只要該衍生遊戲亦以GPL协议發佈。

## 歷史

在2003年4月，遊戲的首個版本- 0.0.4 正式發佈。而因為[Linux Game
Tome在](../Page/Linux_Game_Tome.md "wikilink")2004年3月所作出的表揚和貢獻，所以超級企鵝在2004年5月2日邁向了首個里程碑。

0.1.3
於2005年7月9日正式發佈，包括了一個主要大陸（共26關卡）及兩個次大陸（分別有22個及28個關卡），它們有的是主要开发者製作，有的是此遊戲的支持者所製作的。

在2005年初，超級企鵝的主站由[SourceForge移至BerliOS](../Page/SourceForge.md "wikilink")，因為部分團隊的成員認為使用[Subversion比使用](../Page/Subversion.md "wikilink")[CVS更好](../Page/CVS.md "wikilink")。

此遊戲支持各種不同的平台，如[Linux](../Page/Linux.md "wikilink")，[Windows](../Page/Microsoft_Windows.md "wikilink")，
[Mac OS X](../Page/Mac_OS_X.md "wikilink")，原始碼及相應的安裝檔可於官方網站取得。

## 敌人列表

|Angry Stone Image:Bouncing Snowball.png|Bouncing Snowball
Image:Fish.png|Fish Image:Flying Snowball.png|Flying Snowball
Image:Jumpy.png|Jumpy Image:MrBomb.png|Mr. Bomb Image:MrIceBlock.png|Mr.
Ice Block Image:Snowball.png|Snowball Image:Spiky.png|Spiky

## 其他

  - [超级马里奥兄弟](../Page/超级马里奥兄弟.md "wikilink")
  - [Secret Maryo
    Chronicles](../Page/Secret_Maryo_Chronicles.md "wikilink")

## 外部連結

  - [超級企鵝官方網站](http://supertux.lethargik.org/) 和
    [維基页面](http://supertux.berlios.de/wiki/index.php)
  - [超級企鵝支持者製作的網站](http://www.supertux.info/)
  - [超級企鵝的計劃網站](https://web.archive.org/web/20051217185417/http://developer.berlios.de/projects/supertux/)（用于问题反馈和邮件列表）
  - [超級企鵝官方IRC聊天室](irc://irc.freenode.net/supertux)

### 參與中文翻譯

  - [超級企鵝翻譯計劃首頁](https://www.transifex.com/projects/p/supertux/language/zh_TW/)→超級企鵝官方使用
    [Transifex](../Page/Transifex.md "wikilink")
    翻譯平台（[相關說明](http://chakra-zh.blogspot.tw/2012/11/chakra.html)）

[S](../Category/动作游戏.md "wikilink") [S](../Category/开源游戏.md "wikilink")
[S](../Category/Linux游戏.md "wikilink")
[S](../Category/MacOS遊戲.md "wikilink")
[S](../Category/Windows游戏.md "wikilink")
[S](../Category/自由软件.md "wikilink")
[S](../Category/免费游戏.md "wikilink")