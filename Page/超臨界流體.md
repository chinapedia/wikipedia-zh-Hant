**超臨界流體**（,
SCF）是一種[物質狀態](../Page/物質狀態.md "wikilink")，當物質在超過[臨界溫度及](../Page/臨界溫度.md "wikilink")[臨界壓力以上](../Page/臨界壓力.md "wikilink")，[氣體與](../Page/氣體.md "wikilink")[液體的性質會趨近於類似](../Page/液體.md "wikilink")，最後會達成一個[均勻相之](../Page/均勻相.md "wikilink")[流體現象](../Page/流體.md "wikilink")。超臨界流體類似氣體具有[可壓縮性](../Page/壓縮性.md "wikilink")，可以像氣體一樣發生[瀉流](../Page/瀉流.md "wikilink")，而且又兼具有類似液體的流動性，[密度一般都介於](../Page/密度.md "wikilink")0.1到1.0g/ml之間。

接近臨界點時，[壓力或者](../Page/壓力.md "wikilink")[溫度的小變化會導致密度發生很大變化](../Page/溫度.md "wikilink")，因此使得超臨界流體的許多特性可以被「精細調整」。超臨界流體適合作為工業和實驗室過程中的溶劑，而且可以取代許多[有機溶劑](../Page/有機溶劑.md "wikilink")。[二氧化碳和](../Page/二氧化碳.md "wikilink")[水是最常用的超臨界流體](../Page/水.md "wikilink")，分別被用於去除[咖啡因和](../Page/咖啡因.md "wikilink")[發電](../Page/發電.md "wikilink")。

## 特性

總體而言，超臨界流體的屬性介於氣體和液體之間。在表1顯示，顯示一些常用作超臨界流體的化合物之臨界性質。

| 物质                                                                                     | 分子质量                           | 临界温度                                                                          | 临界压力             | 临界密度  |
| -------------------------------------------------------------------------------------- | ------------------------------ | ----------------------------------------------------------------------------- | ---------------- | ----- |
| 克/摩尔                                                                                   | [K](../Page/开尔文.md "wikilink") | [百萬帕](../Page/帕斯卡_\(单位\).md "wikilink")（[标准大气压](../Page/标准大气压.md "wikilink")） | g/cm<sup>3</sup> |       |
| [二氧化碳](../Page/二氧化碳.md "wikilink")（CO<sub>2</sub>）                                     | 44.01                          | 304.1                                                                         | 7.38 (72.8)      | 0.469 |
| [水](../Page/水分子.md "wikilink")（H<sub>2</sub>O，依[IAPWS資料](../Page/IAPWS.md "wikilink")） | 18.015                         | 647.096                                                                       | 22.064 (217.755) | 0.322 |
| [甲烷](../Page/甲烷.md "wikilink")（CH<sub>4</sub>）                                         | 16.04                          | 190.4                                                                         | 4.60 (45.4)      | 0.162 |
| [乙烷](../Page/乙烷.md "wikilink")（C<sub>2</sub>H<sub>6</sub>）                             | 30.07                          | 305.3                                                                         | 4.87 (48.1)      | 0.203 |
| [丙烷](../Page/丙烷.md "wikilink")（C<sub>3</sub>H<sub>8</sub>）                             | 44.09                          | 369.8                                                                         | 4.25 (41.9)      | 0.217 |
| [乙烯](../Page/乙烯.md "wikilink")（C<sub>2</sub>H<sub>4</sub>）                             | 28.05                          | 282.4                                                                         | 5.04 (49.7)      | 0.215 |
| [丙烯](../Page/丙烯.md "wikilink")（C<sub>3</sub>H<sub>6</sub>）                             | 42.08                          | 364.9                                                                         | 4.60 (45.4)      | 0.232 |
| [甲醇](../Page/甲醇.md "wikilink")（CH<sub>3</sub>OH）                                       | 32.04                          | 512.6                                                                         | 8.09 (79.8)      | 0.272 |
| [乙醇](../Page/乙醇.md "wikilink")（C<sub>2</sub>H<sub>5</sub>OH）                           | 46.07                          | 513.9                                                                         | 6.14 (60.6)      | 0.276 |
| [丙酮](../Page/丙酮.md "wikilink")（C<sub>3</sub>H<sub>6</sub>O）                            | 58.08                          | 508.1                                                                         | 4.70 (46.4)      | 0.278 |

表1—— 各种化学物质的临界压力、温度和密度（Reid et al., 1987）

表2示出了典型的液體，氣體和超臨界流體的密度，[擴散係數和](../Page/擴散係數.md "wikilink")[粘度](../Page/粘度.md "wikilink")。

|       | 密度（kg/m<sup>3</sup>） | [粘度](../Page/粘度.md "wikilink")（µPa∙s） | [擴散係數](../Page/擴散係數.md "wikilink")（mm²/s） |
| ----- | -------------------- | ------------------------------------- | ----------------------------------------- |
| 氣體    | 1                    | 10                                    | 1–10                                      |
| 超臨界流體 | 100–1000             | 50–100                                | 0.01–0.1                                  |
| 液體    | 1000                 | 500–1000                              | 0.001                                     |
|       |                      |                                       |                                           |

液體，氣體和超臨界流體的比较\[1\]

在超臨界流體中沒有液體及氣體之間的相界限，因此不存在[表面張力](../Page/表面張力.md "wikilink")，藉由改變流體的壓力和溫度，可以微調超臨界流體的特性，使其更類似液體或是氣體。物質在流體中的[溶解度即為重要特性之一](../Page/溶解度.md "wikilink")，在固定温度条件下，溶解度會隨流體密度增加而增加。由於密度也是隨壓力增加而增加，因此在壓力增加時，溶解度也會增加。溶解度和溫度的關係比較複雜，在固定密度条件下，溶解度會隨溫度增加而增加，但靠近臨界點時，溫度輕微的增加會造成密度的大幅下降。因此靠近臨界點時，隨著溫度上昇，溶解度會先下降，然後再上昇\[2\]。

二種以上的超臨界流體，只要溫度及壓力超過其臨界點，二者均可以[混溶](../Page/混溶.md "wikilink")，形成單一相的混合物。二元混合物的臨界點可以用二超臨界流體的臨界溫度及臨界壓力，再配合[加權平均求得](../Page/加權平均.md "wikilink")：

  -
    *T<sub>c(mix)</sub>* = （A的莫耳分率）x A的*T*<sub>c</sub> + （B的莫耳分率）x
    B的*T*<sub>c</sub>

若要有更高的準確度，臨界點可以用像是彭-羅賓遜物態方程式之類的[状态方程求得](../Page/状态方程.md "wikilink")，或是用基团贡献（group
contribution）法求得，像密度之類的其他性質，也可以用状态方程來計算\[3\]。

## 相圖

[Carbon_dioxide_pressure-temperature_phase_diagram.svg](https://zh.wikipedia.org/wiki/File:Carbon_dioxide_pressure-temperature_phase_diagram.svg "fig:Carbon_dioxide_pressure-temperature_phase_diagram.svg")
[Carbon_dioxide_density-pressure_phase_diagram.jpg](https://zh.wikipedia.org/wiki/File:Carbon_dioxide_density-pressure_phase_diagram.jpg "fig:Carbon_dioxide_density-pressure_phase_diagram.jpg")

图1是二氧化碳的[相圖](../Page/相圖.md "wikilink")，在壓力-溫度的相圖中，液態和氣態為不同的二相，但若超過臨界點，液態和氣態的相態都消失了，變成單一相態的超臨界流體。圖2是二氧化碳密度-压力的相圖，其中也可以看到類似的現象，當溫度遠低於臨界溫度280K時，隨著壓力的增加，氣體會被壓縮，最後（約40[巴壓力時](../Page/巴.md "wikilink")）會凝結成密度高很多的液體，因此相圖中有表示不連續的垂直虛線。此系統包括高密度的液體及低密度的氣體，二者達到[平衡狀態](../Page/化学平衡.md "wikilink")。隨著溫度的升高，氣體的密度會增加，而液體的密度會減少。在臨界點304.1
K及7.38
MPa（73.8巴）時，液體和氣體的密度相同，因此變成一均勻相（[超臨界二氧化碳](../Page/超臨界二氧化碳.md "wikilink")）。

溫度高於臨界溫度的氣體無法單純用加壓的方式液化。若溫度略高於臨界溫度（310K），壓力在臨界壓力附近時，密度對压力的曲線幾乎是垂直線，因此微小的壓力變化就會使超臨界流體的密度有很大的改變。像[黏度](../Page/黏度.md "wikilink")、[相对电容率及溶劑強度等物理量都和密度有關](../Page/相对电容率.md "wikilink")，在臨界點附近這些物理量也會隨壓力有很大的變化。在溫度更高時，超臨界流體的行為開始類似氣體，當溫度到400K時，隨著壓力的變化，密度幾乎呈線性的變化。

許多壓縮氣體其實都是超臨界流體。例如氮的臨界點是126.2 K（- 147 °C）及3.4
MPa（34巴），因此若壓力容器中的氮氣壓力及溫度大於臨界點，氮氣就已變為超臨界流體
。在室溫下，氮氣和氧氣的溫度遠高於臨界點，因此其特性類似氣體。不過這些氣體除非冷卻到其臨界溫度以下，否則是無法液化的。

## 自然界的超臨界流體

### 海底火山

[Blacksmoker_in_Atlantic_Ocean.jpg](https://zh.wikipedia.org/wiki/File:Blacksmoker_in_Atlantic_Ocean.jpg "fig:Blacksmoker_in_Atlantic_Ocean.jpg")的黑色煙柱\]\]

[海底火山常見於海底的地表](../Page/海底火山.md "wikilink")。有些在較淺海域的海底活火山會釋放蒸汽及岩石碎片，而且噴出至較海平面高很多的高度。但許多海底火山在很深的海域，巨大的海水壓力使蒸汽和氣體無法爆炸性釋放。例如深度超過3000公尺的海域，其壓力到300個大氣壓，已超過臨界壓力的218大氣壓，而且噴口的水溫也會超過攝氏375度，因此噴口最熱的水會变成超臨界流體。

### 行星大气

金星大氣層含有96.5％的二氧化碳和3.5％的氮。金星表面壓力為9.3百萬帕（93[巴](../Page/巴.md "wikilink")），表面溫度為735
K，壓力及溫度都高於主要成分的臨界點，因此使金星表面大氣成为超臨界流體。

[太阳系中的](../Page/太阳系.md "wikilink")[气态巨行星内部大气的主要成分是氢与氦](../Page/气态巨行星.md "wikilink")，其温度远远高于临界点。[木星和](../Page/木星.md "wikilink")[土星的大气](../Page/土星.md "wikilink")，在星球表面为气态，往星球内部逐渐变为液体。但[海王星和](../Page/海王星.md "wikilink")[天王星上](../Page/天王星.md "wikilink")，这种转变还不太为人所了解。[太阳系外行星](../Page/太阳系外行星.md "wikilink")[巨蟹座55e和](../Page/巨蟹座55e.md "wikilink")[格利泽876d的理论模型](../Page/格利泽876d.md "wikilink")，二者都沉积了以超临界水形式存在的海洋，底部是一层高压冰。

## 用途

### 超臨界流體萃取

因為超臨界流體具有低[粘度和高](../Page/粘度.md "wikilink")[擴散係數](../Page/擴散係數.md "wikilink")，因此相較於一般的液體[萃取](../Page/萃取.md "wikilink")，[超臨界流體萃取可以有較快的萃取速度](../Page/超臨界流體萃取.md "wikilink")。藉由調整超臨界流體的密度，可以進行選擇性的萃取，而且在不加壓的情形下，超臨界流體會蒸發，殘留溶劑的量非常少，甚至沒有殘留。二氧化碳是最常用的超臨界流體溶劑，大量用在生咖啡豆的製程，或是在啤酒製造中用來萃取[啤酒花中的成份](../Page/啤酒花.md "wikilink")\[4\]，或是從植物中提煉[精油及藥品](../Page/精油.md "wikilink")。許多[實驗室的](../Page/實驗室.md "wikilink")已將超臨界流體萃取作為萃取的方式之一，取代使用傳統溶劑的萃取法\[5\]\[6\]\[7\]。

### 超臨界流體分解

利用超臨界水可氣化[生物質的特性](../Page/生物質.md "wikilink")，可以進行生物質的分解。\[8\]
這種生物質的氣化可以用來製造烴類燃料，用在高效的燃燒裝置中，或用於產生[燃料電池所需要的氫氣](../Page/燃料電池.md "wikilink")。在後者的應用，由於在水蒸汽轉化過程中，水也可以參與反應，提供氫原子，因此氫氣產量可以遠高於生物質中的氫含量。

### 乾洗

超临界二氧化碳可以代替[干洗中的](../Page/干洗.md "wikilink")[四氟乙烯等传统有机溶剂](../Page/四氟乙烯.md "wikilink")，传统溶剂会污染环境，而超临界二氧化碳溶剂对环境友好，对健康无害，易于循环利用。超临界二氧化碳用于干洗的一缺点是，泄压过程中会对[鈕扣](../Page/鈕扣.md "wikilink")、[拉链等造成损害](../Page/拉链.md "wikilink")。\[9\]
溶于超临界二氧化碳的清洁剂可以提高超临界二氧化碳的溶解能力。\[10\]

### 超临界流体色谱

超临界流体色谱兼有[气相色谱和](../Page/气相色谱.md "wikilink")[高效液相色谱的优点](../Page/高效液相色谱.md "wikilink")。超临界流体色谱可以分析易挥发和热不稳定的分析物（气相色谱无法做到），还可以分析[火焰离子化检测器](../Page/火焰离子化检测器.md "wikilink")（液相色谱无法做到），超临界流体色谱另外一个优点是能够产生比较窄的峰。不過實務上超临界流体色谱还不能完全取代广泛使用的气相色谱和液相色谱，只有在像[手性分离及高分子量碳水化合物分析才會使用超临界流体色谱](../Page/手性.md "wikilink")\[11\]。
制造业上已经有了装置。\[12\] 最终产物的纯度非常高，但成本也非常高，只适用于制备如药物等高价值的材料。

### 化学反应

改变反應溶剂的条件，可以使反应体系为均匀相，便於反应进行，或者使体系发生相分离，以便分离反应产物。快速扩散能加快的进行。溫度及壓力可以調整反應依特定的反應路徑進行，以產生特定的[手性](../Page/手性.md "wikilink")[異構物](../Page/同分异构.md "wikilink")\[13\]。超臨界流體對環境的影響也比傳統的有機溶劑要小。

### 制备药物共晶

超临界流体是制备[活性药物成分的新异晶体结构](../Page/原料药.md "wikilink")——[药物共晶的新介质](../Page/药物共晶.md "wikilink")。药物共晶粒子可以通过超临界流体技术一步合成，这是传统方法很难甚至不可能做到的。超临界流体的多种性质（如超临界二氧化碳强溶解性、抗溶剂效应、雾化增强）可用于制备纯的和干燥的新药物共晶——由活性药物成分、由一种或多种晶格[构象异构体组成的结晶分子络合物](../Page/构象异构体.md "wikilink")。\[14\]\[15\]

### 制备纳米和微米粒子

制备窄分布粒径的小粒子是[制药业和其他工业技术上非常重要的过程](../Page/制药.md "wikilink")。应用超临界流体可以有多种方法使溶质快速超过[饱和点](../Page/飽和_\(化學\).md "wikilink")，比如通过稀释、减压，或二者兼而用之。这些过程在超临界流体里要比在通常的液体里发生得更快，使[成核或](../Page/成核.md "wikilink")压过[晶体生长](../Page/晶体生长.md "wikilink")，产生非常小的、粒径均匀的粒子。超临界流体可以制出粒径大小在5—2000纳米范围内的粒子\[16\]。

### 超臨界干燥

是一种去除溶剂并且不引起表面张力效应的方法。随着液体的蒸发，表面张力会拖曳固体中的小结构，造成固体扭曲或收缩。在超临界条件下，不存在表面张力，清除超临界流体不会影响固体形状。超临界干燥用于制造[气凝胶](../Page/气凝胶.md "wikilink")，干燥一些易被破坏的材料，如考古样品或是用于[电子显微镜实验的样品](../Page/电子显微镜.md "wikilink")\[17\]。

### 超临界水氧化

以超临界水作为介质，氧化危险废物和清除有毒燃烧产物。

### 超臨界流體發電

[热机的](../Page/热机.md "wikilink")[热效率和高低溫熱源之間的溫度差有關](../Page/热效率.md "wikilink")。若要調高[發電廠的效率就需要調高工作溫度](../Page/發電廠.md "wikilink")。若用超臨界的水做為工作流体，现在的技术可提昇热机效率，由非临界條件下的39%提高到45%。\[18\]
[超临界水反应堆是一种很有發展性的核系统](../Page/超临界水反应堆.md "wikilink")，也可以依類似原理提高热效率，核系统中也可以使用超临界二氧化碳，有類似的效果。\[19\]。燃煤發電的[超临界機組已经非常常见](../Page/超臨界鍋爐.md "wikilink")，比传统的火电厂效率提高很多。

## 歷史

1822年，[男爵在進行實驗時發現超臨界流體的特性](../Page/男爵.md "wikilink")，他將炮管密封，其中加入不同溫度的流體，再放入[燧石的小球](../Page/燧石.md "wikilink")，球在炮管中滾動時會有聲音的不連續變化；但當溫度超過臨界溫度時，聲音的不連續變化消失了，炮管的流體中液體和氣體的密度變得相同，變成一個超臨界流體的相，因此也沒有二相之間的相界限\[20\]。

## 参看

  -
  - [临界点 (热力学)](../Page/临界点_\(热力学\).md "wikilink")

  -
## 參考資料

## 外部連結

  - [Handy
    calculator](https://web.archive.org/web/20080505235710/http://www.criticalprocesses.com/Calculation%20of%20density,%20enthalpy%20and%20entropy%20of%20carbon%20dioxide.htm)
    for density, enthalpy, entropy and other thermodynamic data of
    supercritical CO2
  - [Food Product
    Design](https://web.archive.org/web/20071002181701/http://www.foodproductdesign.com/archive/1997/0597NT.html)
  - [CO<sub>2</sub> as a natural refrigerant -
    FAQs](https://web.archive.org/web/20121029103527/http://www.r744.com/faq.php)
  - [animated presentation describing what a supercritical fluid
    is](https://web.archive.org/web/20081206023912/http://www.appliedseparations.com/ASInteractive/Overviews/SCF/sfe/player.html)
  - [NewScientist Environment FOUND:The hottest water on
    Earth](http://environment.newscientist.com/article/dn14456)

[C](../Category/凝聚体物理学.md "wikilink")
[C](../Category/物质状态.md "wikilink")
[C](../Category/气体.md "wikilink")

1.
2.
3.
4.
5.  U.S.EPA Method 3560 Supercritical Fluid Extraction of Total
    Recoverable Hydrocarbons. <http://www.epa.gov/SW-846/pdfs/3560.pdf>
6.  U.S.EPA Method 3561 Supercritical Fluid Extraction of Polycyclic
    Aromatic Hydrocarbons. <http://www.epa.gov/SW-846/pdfs/3561.pdf>
7.  Use of Ozone Depleting Substances in Laboratories. TemaNord
    2003:516.
8.
9.
10.
11.
12.
13.
14. L. Padrela, M.A. Rodrigues, S.P. Velaga, H.A. Matos and E.G. Azevedo
    (2009). "Formation of indomethacin–saccharin cocrystals using
    supercritical fluid technology". *European Journal of Pharmaceutical
    Sciences*. **38**, pp. 9–17.
15. L. Padrela, M.A. Rodrigues, S.P. Velaga, H.A. Matos and E.G. Azevedo
    (2009). "Screening for pharmaceutical cocrystals using the
    supercritical fluid enhanced atomization process". *Journal of
    Supercritical Fluids*. article in press, corrected proof.
16.
17. <http://dx.doi.org/10.1016/j.cemconres.2017.05.005>
18.
19.
20.