{{Distinguish2|'''[
[F-18-diamondback_blast.jpg](https://zh.wikipedia.org/wiki/File:F-18-diamondback_blast.jpg "fig:F-18-diamondback_blast.jpg")[海軍](../Page/海軍.md "wikilink")[F/A-18E/F黃蜂式戰鬥攻擊機進行](../Page/F/A-18黃蜂式戰鬥攻擊機.md "wikilink")[跨音速飛行](../Page/跨音速.md "wikilink")。](../Page/超聲波.md "wikilink")
**超音速**（）簡單說，是指超過環境中[音速的速度](../Page/音速.md "wikilink")。在[海平面高度](../Page/海平面.md "wikilink")，氣溫[攝氏](../Page/攝氏.md "wikilink")空氣中，音速大約是343[米](../Page/公尺.md "wikilink")／秒（約等於1,125呎／秒、768[英里](../Page/英里.md "wikilink")／[小時或](../Page/小時.md "wikilink")1,235千米／小時），換算驗證，如。

音速，基本單位定義為1[馬赫](../Page/馬赫.md "wikilink")（Mach），因此，超音速常以音速倍數——[馬赫數為量度單位](../Page/馬赫.md "wikilink")。超過5馬赫的速度有時候稱為**[超高音速](../Page/超高音速.md "wikilink")**（）。物體-{只}-有一些部份（例如轉子葉片的末梢）其周遭空氣是超過音速的情形稱為**[穿音速](../Page/穿音速.md "wikilink")**（）；出現這種情況，常見的物體速度值是介於0.8馬赫與1.2馬赫之間。單位換算，如。

[聲音是在彈性介質中行進的](../Page/聲音.md "wikilink")[振動](../Page/振動.md "wikilink")（[壓力波](../Page/壓力波.md "wikilink")）。在氣體中，聲波是一種[縱波](../Page/縱波.md "wikilink")，以不同速度行進，其中最相關的影響因素是氣體的分子量與溫度（氣體壓力影響較小）。既然氣體溫度與組成隨著[海拔改變甚鉅](../Page/海拔.md "wikilink")，飛行器的馬赫數可以在空速未有改變下有所變動。在室溫的水中，速度超過可被視為超音速。在固體中，聲波可以是[縱波或](../Page/縱波.md "wikilink")[橫波](../Page/橫波.md "wikilink")，而且傳播速度更快。

## 超音速物體

許多現代[戰鬥機可以做超音速飛行](../Page/戰鬥機.md "wikilink")，但[協和式客機](../Page/協和式客機.md "wikilink")（Concorde）以及[圖波列夫系列](../Page/圖波列夫.md "wikilink")（Tupolev）[Tu-144是僅有的](../Page/Tu-144.md "wikilink")[超音速客機](../Page/超音速客機.md "wikilink")。自從2003年11月26日協和號退役前的最後一次飛行之後，就不再有任何載客飛行。一些大型[轟炸機](../Page/轟炸機.md "wikilink")，譬如圖波列夫系列的[Tu-160與](../Page/Tu-160.md "wikilink")[羅克韋爾國際公司](../Page/羅克韋爾國際公司.md "wikilink")／[波音的](../Page/波音.md "wikilink")[B-1B也可做超音速飛行](../Page/B-1槍騎兵戰略轟炸機.md "wikilink")。[F-22及](../Page/F-22.md "wikilink")[颱風戰鬥機](../Page/颱風戰鬥機.md "wikilink")（EF-2000）是戰機中可以較長時間維持超音速飛行而不需用到[後燃器的佼佼者](../Page/後燃器.md "wikilink")。美國的[SR-71黑鸟式侦察机機最高的速度可達到](../Page/SR-71黑鸟式侦察机.md "wikilink")3.3馬赫。多国实验中的高超音速飞行器，如中国的[WU-14高超音速飞行器设计速度在](../Page/WU-14高超音速飞行器.md "wikilink")5马赫以上。

## 外部連結

  - ["Can We Ever Fly Faster Speed of Sound", October 1944, Popular
    Science](http://books.google.com/books?id=PyEDAAAAMBAJ&pg=PA72&dq=motor+gun+boat&hl=en&ei=LxTqTMfGI4-bnwfEyNiWDQ&sa=X&oi=book_result&ct=result&resnum=6&ved=0CEIQ6AEwBQ#v=onepage&q=motor%20gun%20boat&f=true)
    one of the earliest articles on shock waves and flying the speed of
    sound
  - ["Britain Goes Supersonic", January 1946, Popular
    Science](http://books.google.com/books?id=NSEDAAAAMBAJ&pg=PA114&dq=popular+science+January+1946&hl=en&ei=vb7kTIbiC4uQnwfUwpT4BQ&sa=X&oi=book_result&ct=result&resnum=2&sqi=2&ved=0CDQQ6AEwAQ#v=onepage&q=popular%20science%20January%201946&f=true)
    1946 article trying to explain supersonic flight to the general
    public
  - [MathPages - The Speed of
    Sound](http://www.mathpages.com/home/kmath109/kmath109.htm)
  - [Sound](http://online.cctt.org/physicslab/content/phy1/lessonnotes/Sound/lessonsound.asp)
  - [Supersonic sound pressure
    levels](http://www.makeitlouder.com/Decibel%20Level%20Chart.txt)

[Category:空氣動力學](../Category/空氣動力學.md "wikilink")
[Category:声学](../Category/声学.md "wikilink")
[Category:航空航天工程](../Category/航空航天工程.md "wikilink")