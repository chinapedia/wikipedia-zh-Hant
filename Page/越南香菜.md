**越南香菜**（[学名](../Page/学名.md "wikilink")：**）又名**越南芫荽**、**叻沙葉**，是蓼科春蓼屬的一種[香料植物](../Page/香料.md "wikilink")，在[東南亞地區常會用它的](../Page/東南亞.md "wikilink")[葉子來烹調食物](../Page/葉子.md "wikilink")。

在[越南料理裡常會將越南香菜拿來做生菜](../Page/越南料理.md "wikilink")[沙拉或是加在](../Page/沙拉.md "wikilink")[春捲裡食用](../Page/春捲.md "wikilink")。在煮牛肉[河粉時也會添加越南香菜來配色及增添風味](../Page/河粉.md "wikilink")。在吃[鴨仔蛋時也常會加上越南香菜一起食用](../Page/鴨仔蛋.md "wikilink")。

在[新加坡與](../Page/新加坡.md "wikilink")[馬來西亞會以切碎的越南香菜來製做一種稱為](../Page/馬來西亞.md "wikilink")[叻沙的香辣湯底](../Page/叻沙.md "wikilink")，這種湯底是以越南香菜做為主要材料，因此在當地稱越南香菜為叻沙葉。

[澳洲正在研究以越南香菜為原料來提煉一種稱為](../Page/澳洲.md "wikilink")「kesom
oil」的[精油](../Page/精油.md "wikilink")。

## 性狀

越南香菜為[多年生植物](../Page/多年生植物.md "wikilink")，葉子的顏色為暗綠色，靠近基部有勃艮第紅色的塊斑，上有栗色的斑點。在[熱帶及](../Page/熱帶.md "wikilink")[亞熱帶地區溫暖潮濕的環境下生長的最好](../Page/亞熱帶.md "wikilink")，在適合生長的環境下，植株高度可以生長至15-30公分高。越南香菜無法生長於緯度高於32度以上的地區或是土壤水份太多的地方，在冬季或是溫度太高時會使植株萎凋枯死。在[越南可以看到用人工栽種於田間的越南香菜](../Page/越南.md "wikilink")，也可以在野外看到呈野生狀態的越南香菜。

## 成份

越南香菜精油中所含的成份主要是[醛類與](../Page/醛.md "wikilink")
[倍半萜類化合物](../Page/倍半萜.md "wikilink")，其中醛類化合物有[癸醛](../Page/癸醛.md "wikilink")（28%）、[十二醛](../Page/十二醛.md "wikilink")（44%）及alcohol
1-decanol（11%）。倍半萜類化合物（15%）有[α-葎草烯與](../Page/葎草烯.md "wikilink")[β-石竹烯](../Page/石竹烯.md "wikilink")。

## 藥用

根據越南專家的說法，越南香菜味苦、辛，無毒，可解毒消腫，他們認為越南香菜可以用來治療腫脹、[粉刺](../Page/粉刺.md "wikilink")、消化不良、腸胃脹氣及[胃痛](../Page/胃痛.md "wikilink")。

越南University of Natural
Sciences的研究顯示，食用越南香菜會使[老鼠的](../Page/老鼠.md "wikilink")[精子數量減少](../Page/精子.md "wikilink")，對於[人類可能也有類似的效果](../Page/人類.md "wikilink")。在許多越南的草藥療法中，會使用越南香菜來抑制性慾，在越南有一種說法是「越南香菜，豆芽菜」，意思是說越南香菜有降低生育的能力，而豆芽菜則有相反的效果。也有另一種說法是「越南香菜可以消除性慾」。

## 外部連結

  - [Vietnamese Coriander (Persicaria odorata (Lour.) Soják)
    page](http://gernot-katzers-spice-pages.com/engl/Pers_odo.html?spicenames=zh)
    from Gernot Katzer's Spice Pages
  - Kesom Oil – a New Essential Oil for the International Flavour and
    Fragrance Industry *in* [First Australian New Crops Conference 1996
    –
    Volume 2](https://web.archive.org/web/20060919135700/http://www.rirdc.gov.au/reports/NPP/UQ33A2.pdf)

[Category:蓼科](../Category/蓼科.md "wikilink")
[Category:香草](../Category/香草.md "wikilink")