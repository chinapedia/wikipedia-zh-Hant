《**越女剑**》是作家[金庸的一部](../Page/金庸.md "wikilink")[短篇](../Page/短篇小說.md "wikilink")[武俠小說](../Page/武俠小說.md "wikilink")，是金庸所有的武侠創作里面历史背景最早的，亦是篇幅最短的。據金庸自己的說法，《[鹿鼎記](../Page/鹿鼎記.md "wikilink")》是他武侠小说收山之作，《越女剑》只算是一個獨立短篇，同樣亦是封筆之作。英文版名為“Sword
of the Yue Maiden”。

## 出版情况

原於《明報晚報》1970年創刊時連載。在《明報晚報》開始刊登時，《鹿鼎記》尚在連載中。

## 故事背景

时间是在[春秋战国时期中的](../Page/春秋战国.md "wikilink")[吳越爭霸](../Page/春秋五霸.md "wikilink")。真实人物越王[勾践](../Page/勾践.md "wikilink")、吴王[夫差](../Page/夫差.md "wikilink")、[范蠡](../Page/范蠡.md "wikilink")、[西施](../Page/西施.md "wikilink")。

## 劇情梗概

[吳國差遣劍士挑釁](../Page/吳國.md "wikilink")[越國](../Page/越國.md "wikilink")，越國重臣[范蠡偶遇劍術高手](../Page/范蠡.md "wikilink")——[阿青](../Page/阿青.md "wikilink")，阿青憑精湛劍術，將前來[越國挑釁的吳國劍士擊敗](../Page/越國.md "wikilink")。阿青在與一頭深山白猿多番交手後，自行領悟一套精湛劍術。范蠡十分賞識阿青並讓其訓練越國士兵，阿青在訓練過程中不知不覺地喜歡上范蠡。越王勾踐擊敗吳國後，范蠡終於與心愛的[西施重逢](../Page/西施.md "wikilink")，阿青因而出現妒忌之心並想將西施殺掉。當阿青正想下手殺掉西施時，阿青驚艷於西施的美貌而下不了手。可是，西施胸口被阿青無意中發出的內勁擊中，西施皺起眉頭、捧著心口的神情，美得奪人魂魄，成為千古傳頌的「西子捧心」。

這套零散劍法的傳人是南宋[江南七怪之](../Page/江南七怪.md "wikilink")“越女劍”[韓小瑩](../Page/韓小瑩.md "wikilink")，“越女劍法”有一招名為“技击白猿”（《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》）。

## 小說人物

  - [范蠡](../Page/范蠡.md "wikilink")，越王勾踐之臣，與西施互相愛慕，到最後才明白阿青愛慕自己。
  - [阿青](../Page/阿青.md "wikilink")，擁有一身由白猿身上學來的奇異劍術，愛慕范蠡，想致西施於死地，最終震懾於西施的美麗而放棄，但不小心用劍氣傷了西施。
  - [西施](../Page/西施.md "wikilink")，傳說是越王用來對吳王夫差使美人計之美人，故事最終被阿青的劍氣所傷，故有西子捧心的形象。
  - [白公公](../Page/白公公.md "wikilink")，一隻通曉人性喜歡以竹棒和阿青打鬧的白色猿猴，舞動竹棒間的動作帶有劍法的神隨，可說是傳授阿青劍法的師傅，初見范蠡時欲攻擊之，阿青不解向前抵擋，而與其打鬥，敗後遁入山林間。
  - [勾踐](../Page/勾踐.md "wikilink")，越國之王，因越國被吳王所滅，引申出臥薪嚐膽的故事。
  - [吳國劍士](../Page/吳國.md "wikilink")，喝醉酒後殺死阿青的羊，後又想調戲阿青，被阿青高明的劍術擊敗。
  - [越國劍士](../Page/越國.md "wikilink")，曾見識並學習阿青的劍術，雖無人習得其高超之處，但每個人僅學到一些影子就能打敗吳國劍士。
  - [文種](../Page/文種.md "wikilink")，越國大夫，與范蠡一同為勾踐獻計攻吳。
  - [薛燭](../Page/薛燭.md "wikilink")、[風-{胡}-子](../Page/風胡子.md "wikilink")，鑄劍人。

## 改編作品

### 電視劇

  - 《**[越女劍](../Page/越女劍_\(電視劇\).md "wikilink")**》1986，電視劇，[香港亞洲電視本港台](../Page/亞洲電視.md "wikilink")，[李賽鳳飾](../Page/李賽鳳.md "wikilink")[阿青](../Page/阿青.md "wikilink")，[岳華飾](../Page/岳華.md "wikilink")[范蠡](../Page/范蠡.md "wikilink")，監製：[王心慰](../Page/王心慰.md "wikilink")。\[1\]

## 参看

  - [武侠小说](../Page/武侠小说.md "wikilink")

## 參考資料

[Category:金庸小說](../Category/金庸小說.md "wikilink")
[Y越](../Category/中國戰國時代背景小說.md "wikilink")
[Y越](../Category/改編成電影的香港小說.md "wikilink")
[Y越](../Category/蘇州背景小說.md "wikilink")
[Y越](../Category/紹興背景小說.md "wikilink")
[\*](../Category/越女劍.md "wikilink")
[Category:平行世界題材小說](../Category/平行世界題材小說.md "wikilink")
[Category:1970年短篇小說](../Category/1970年短篇小說.md "wikilink")

1.  [金庸影視資訊年表](http://www.gulongbbs.com/html/jyys20110212.htm)