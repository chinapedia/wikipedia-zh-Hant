**越窑**是[唐朝](../Page/唐朝.md "wikilink")、[五代时](../Page/五代.md "wikilink")[浙江](../Page/浙江.md "wikilink")[绍兴](../Page/绍兴.md "wikilink")[越州的瓷窑](../Page/越州.md "wikilink")，窑址主要分佈于[慈溪的](../Page/慈溪.md "wikilink")[上林湖一带](../Page/上林湖.md "wikilink")。[隋朝](../Page/隋朝.md "wikilink")、[唐朝时](../Page/唐朝.md "wikilink")[绍兴叫](../Page/绍兴.md "wikilink")“越州”，因此得名为“越窑”。越窑烧制的青瓷器在唐代很出名。唐代[陆羽在](../Page/陆羽.md "wikilink")《茶经》中写道：“碗，越州为上。其瓷类玉、类冰。”

## 秘色瓷

晚唐、[五代时最上品的越窑千峰翠色青瓷专门作为贡品](../Page/五代.md "wikilink"),臣民不得用,因此称为“**秘色**”。[五代时](../Page/五代.md "wikilink")[闽国王](../Page/闽_\(十国\).md "wikilink")[王审知曾派](../Page/王审知.md "wikilink")[徐寅贡秘色瓷](../Page/徐寅.md "wikilink"),徐寅作诗:《贡余秘色瓷器》：“捩翠融青瑞色新，陶成先得贡吾君；巧剜明月染春水，轻旋薄冰盛绿云；古镜破苔当席上，嫩荷涵露别江濆；中山竹叶醅初发，多病那堪中十分?”

[宋](../Page/宋.md "wikilink")[赵彦衡](../Page/赵彦衡.md "wikilink")《[云麓漫钞](../Page/云麓漫钞.md "wikilink")》说越窑[秘色瓷艾色](../Page/秘色瓷.md "wikilink")，又引[唐诗人](../Page/唐.md "wikilink")[陆龟蒙](../Page/陆龟蒙.md "wikilink")《进越器诗》曰："九秋风露越窑开，夺得千峰翠色来；好向中霄盛沆瀣，共稽中散斗遗杯”。

1987年4月3日，[法门寺真身宝塔的地宫被打开](../Page/法门寺.md "wikilink")，出土大量珍贵文物，其中包括十三件[秘色瓷器现存法门寺博物馆](../Page/秘色瓷.md "wikilink")。

## 青瓷

## 参考资料

  - （清）兰浦原著、[郑廷桂增补](../Page/郑廷桂.md "wikilink")，连冕 编注
    《[景德镇陶录图说](../Page/景德镇陶录.md "wikilink")》山东画报出版社
    2005年 ISBN 7806037926
  - (宋)赵彦衡《[云麓漫钞](../Page/云麓漫钞.md "wikilink")》 中华书局 1996 ISBN 7101012256

## 参见

  - [上林湖越窑遗址](../Page/上林湖越窑遗址.md "wikilink")

[category:浙江历史](../Page/category:浙江历史.md "wikilink")
[category:窯](../Page/category:窯.md "wikilink")
[category:文物博物馆](../Page/category:文物博物馆.md "wikilink")