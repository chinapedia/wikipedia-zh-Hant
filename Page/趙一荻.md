**赵一荻**（），又名**绮霞**，出生于[香港](../Page/香港.md "wikilink")，[國民政府將領](../Page/國民政府.md "wikilink")[張學良](../Page/張學良.md "wikilink")，第二任妻子，人称「赵四小姐」。在台灣期間，跟隨[張學良定居於](../Page/張學良.md "wikilink")[高雄](../Page/高雄.md "wikilink")[壽山](../Page/壽山.md "wikilink")、[高雄](../Page/高雄.md "wikilink")[西子灣](../Page/西子灣.md "wikilink")、[新竹](../Page/新竹.md "wikilink")[清泉](../Page/清泉.md "wikilink")。

## 簡介

[Zhaoyidi_former_residence.JPG](https://zh.wikipedia.org/wiki/File:Zhaoyidi_former_residence.JPG "fig:Zhaoyidi_former_residence.JPG")赵一荻故居\]\]

1928年（一说1927年）与當時已婚的[张学良相识于](../Page/张学良.md "wikilink")[天津](../Page/天津.md "wikilink")，被張學良舉薦進入東北大學讀書，兼職張學良的私人秘書，自此與其漸生情愫。

1928年3月，北洋政府交通部次長趙慶華登報發表聲明，在《大公報》，聲稱要與四女趙一荻斷絕父女關係：過了幾天，趙慶華又聲言自身慚愧，旋即辭去交通部次長的職務，從此不再為官，退隱而居。之後，赵一荻跪求张妻[-{于}-凤至接纳](../Page/于凤至.md "wikilink")，并许诺终生不要名份。后住在-{于}-凤至出资修建的“金屋”以秘书身份伴张。

张学良1928年冬“[东北易帜](../Page/东北易帜.md "wikilink")”，後又力挺[中原大戰情勢危殆的蔣介石](../Page/中原大戰.md "wikilink")，1936年12月12日[西安事變兵谏](../Page/西安事變.md "wikilink")[蒋介石](../Page/蒋介石.md "wikilink")，张妻-{于}-凤至和赵多有襄助。1940年，-{于}-凤至因患[乳腺癌](../Page/乳腺癌.md "wikilink")，不得不与丈夫分离到美国治病，赵一荻陪伴张左右。

1964年，为断绝张远走美国这一退路，蒋介石借张入[基督教](../Page/基督教.md "wikilink")，按教义一名男子不能同时与两名女子保持夫妻关系，想逼迫张与-{于}-凤至离婚，赵一荻亦放弃不要名份的誓言，遣人游说-{于}-凤至，称张学良主动要求与-{于}-凤至离婚并和自己结婚，-{于}-凤至为张学良安全考虑，同意在形式上和张离婚，但坚持“离婚”是被逼无奈，并非夫妻情断。之后赵一荻和张学良正式成为法定夫妻。

二人有一子[张闾琳](../Page/张闾琳.md "wikilink")。

## 外部链接

  - [史詩式的愛情故事:
    赵一荻與張學良](https://web.archive.org/web/20070928000400/http://www.aladding.com/view/postDetail.cfm?lv=big5&topicid=110&postid=322204)
  - [赵一荻的兒子张闾琳](https://web.archive.org/web/20071113161123/http://big5.china.com/gate/big5/lianzai.china.com/books/html/1440/6938/92378.html)

[Category:張作霖家族](../Category/張作霖家族.md "wikilink")
[Category:中華民國新教徒](../Category/中華民國新教徒.md "wikilink")
[Category:移民美國的中華民國人](../Category/移民美國的中華民國人.md "wikilink")
[Category:香港裔台灣人](../Category/香港裔台灣人.md "wikilink")
[Category:香港人](../Category/香港人.md "wikilink")
[Y](../Category/趙姓.md "wikilink")
[Category:葬于夏威夷州](../Category/葬于夏威夷州.md "wikilink")