**趙志華**（），字**興邦**，[河北](../Page/河北.md "wikilink")[通州人](../Page/通州.md "wikilink")，一說[黑龍江省](../Page/黑龍江省.md "wikilink")[龍江縣人](../Page/龍江縣.md "wikilink")，[北京清華大學肄業](../Page/北京清華大學.md "wikilink")，[黃埔軍校](../Page/黃埔軍校.md "wikilink")10期畢業，曾入[美國](../Page/美國.md "wikilink")[維吉尼亞軍校與](../Page/維吉尼亞軍校.md "wikilink")[蔣緯國同時進修](../Page/蔣緯國.md "wikilink")。後擔任代理裝甲兵司令，[1964年發動](../Page/1964年.md "wikilink")[湖口兵變](../Page/湖口兵變.md "wikilink")，但隨即遭屬下制伏，移送法辦，判處[死刑](../Page/死刑.md "wikilink")，後[蔣介石病逝](../Page/蔣介石.md "wikilink")，遇赦，改為[無期徒刑](../Page/無期徒刑.md "wikilink")。

## 簡介

[二次大戰期間](../Page/二次大戰.md "wikilink")，1944年任[中國駐印軍戰車第一營](../Page/中國駐印軍.md "wikilink")[中校副](../Page/中校.md "wikilink")[營長](../Page/營長.md "wikilink")，三月與上校營長[趙振宇共同參與了](../Page/趙振宇.md "wikilink")[瓦魯班戰役](../Page/瓦魯班戰役.md "wikilink")，於此役中，共擊斃[日本](../Page/日本.md "wikilink")[第56聯隊長山崎大佐](../Page/步兵第56聯隊.md "wikilink")、作戰課課長石川中佐等官兵數百人，毀戰甲汽車多輛、戰防砲多門；並擄獲日本[第18師團部](../Page/第18師團.md "wikilink")[關防](../Page/關防.md "wikilink")、[軍旗及完整的](../Page/軍旗.md "wikilink")[裝甲車兩輛](../Page/裝甲車.md "wikilink")、[卡車](../Page/卡車.md "wikilink")、指揮車各一輛及大量的[輕兵器](../Page/輕兵器.md "wikilink")、[彈藥](../Page/彈藥.md "wikilink")、[糧食等物資](../Page/糧食.md "wikilink")。其後繼趙振宇任第一營營長。

[日本投降後歷任戰一團第一營營長](../Page/日本投降.md "wikilink")、戰一團副團長、戰一團團長。

[國共戰爭](../Page/國共戰爭.md "wikilink")[徐蚌會戰中被俘](../Page/徐蚌會戰.md "wikilink")。趙志華逃離，歸建國軍後，在[蔣緯國全力保薦之下復職](../Page/蔣緯國.md "wikilink")。來台後歷任裝甲兵旅第二總隊總隊長，裝甲兵第一師師長，[裝甲兵學校校長](../Page/裝甲兵學校.md "wikilink")，繼而又晉陞為[少將裝甲兵副司令](../Page/少將.md "wikilink")，代理司令。

因於[徐蚌會戰中](../Page/徐蚌會戰.md "wikilink")，聽聞統帥部有匪諜滲入，一直有興兵面蔣「[清君側](../Page/清君側.md "wikilink")」之意，1964年1月21日，趙志華集合了裝甲第一師進行裝備武器檢查，檢查完畢後，趙志華持[手槍在現場訓話](../Page/手槍.md "wikilink")，內中有國家動盪、[將領](../Page/將領.md "wikilink")[貪腐等激進發言](../Page/貪腐.md "wikilink")，並舉[彭孟緝貪污荒淫](../Page/彭孟緝.md "wikilink")、[總統府參軍長](../Page/總統府參軍長.md "wikilink")[周至柔](../Page/周至柔.md "wikilink")[上將家中的一條狼狗](../Page/上將.md "wikilink")，每月伙食費比一個連的官兵還多，煽動軍心，要求台下官兵一同北上「[掃除奸佞](../Page/清君側.md "wikilink")」。

裝甲第一師工兵指揮部中校[輔導長](../Page/輔導長.md "wikilink")[朱寶康假意附和](../Page/朱寶康.md "wikilink")，躍上司令臺，趙氏與朱握手，朱卻乘機將趙志華摔倒，另一手搶得其槍。朱將趙壓制在地，一把將趙抱住，控制局勢後，命憲兵將趙逮捕，一般多稱之為「[湖口兵變](../Page/湖口兵變.md "wikilink")」，部分相關人士認為此事應稱之為「湖口事件」，因全師官兵並未有所異動，且此案屬未遂，並未成真。官方紀錄為：「趙員於民國五十三年一月二十一日，於裝甲兵副司令任期內因煽動湖口基地兵變未果，被捕撤職。」

蔣緯國曾表示：「趙志華因購屋，急需新臺幣三萬元週轉，預借支薪水，已依規定呈報告給裝甲兵司令官[郭東暘中將](../Page/郭東暘.md "wikilink")，但郭對此置之不理長達兩個多月，趙認為受到不公平待遇，怒而發動兵變。」

煽動兵變失敗被捕後，趙志華被[軍事法庭判處](../Page/軍事法庭.md "wikilink")[死刑](../Page/死刑.md "wikilink")，但是一直未執行，1975年因[蔣介石逝世](../Page/蔣介石.md "wikilink")，趙志華被改判[無期徒刑](../Page/無期徒刑.md "wikilink")，1978年出於蔣緯國授意，由昔日戰友暨同鄉[劉奎斗出面擔保](../Page/劉奎斗.md "wikilink")，遂至[臺北陸軍醫院](../Page/臺北陸軍醫院.md "wikilink")[保外就醫](../Page/保外就醫.md "wikilink")，後於1983年過世。

## 參見

  - [湖口兵變](../Page/湖口兵變.md "wikilink")
  - [中國遠征軍](../Page/中國遠征軍.md "wikilink")
  - [驼峰航线](../Page/驼峰航线.md "wikilink")
  - [飛虎隊](../Page/飛虎隊.md "wikilink")
  - [滇緬公路](../Page/滇緬公路.md "wikilink")
  - [新六军](../Page/新六军.md "wikilink")
  - [杜聿明](../Page/杜聿明.md "wikilink")
  - [孙立人](../Page/孙立人.md "wikilink")
  - [廖耀湘](../Page/廖耀湘.md "wikilink")
  - [麥瑞爾突擊隊](../Page/麥瑞爾突擊隊.md "wikilink")
  - [第75游騎兵團](../Page/第75游騎兵團.md "wikilink")
  - [美國陸軍遊騎兵部隊](../Page/美國陸軍遊騎兵部隊.md "wikilink")
  - [密支那](../Page/密支那.md "wikilink")
  - [久留米師團](../Page/久留米師團.md "wikilink") = 第18師團

[Category:中華民國陸軍少將](../Category/中華民國陸軍少將.md "wikilink")
[Category:台灣戰後河北移民](../Category/台灣戰後河北移民.md "wikilink")
[Category:中华民国死刑犯](../Category/中华民国死刑犯.md "wikilink")
[Category:國立清華大學校友](../Category/國立清華大學校友.md "wikilink")
[Category:黄埔军校第十期](../Category/黄埔军校第十期.md "wikilink")
[Category:弗吉尼亚军事学院校友](../Category/弗吉尼亚军事学院校友.md "wikilink")
[Category:通州人](../Category/通州人.md "wikilink")
[Category:趙姓](../Category/趙姓.md "wikilink")