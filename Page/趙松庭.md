**趙松庭**（）生於[浙江](../Page/浙江.md "wikilink")[東陽市](../Page/東陽市.md "wikilink")，[中國](../Page/中國.md "wikilink")[笛子演奏家](../Page/笛子.md "wikilink")。

趙松庭揉合了笛子[南派笛子和](../Page/南派笛子.md "wikilink")[北派笛子的吹奏技巧](../Page/北派笛子.md "wikilink")，並借鑑[長笛的技巧而創出剛柔並濟的](../Page/長笛.md "wikilink")[浙派笛子](../Page/浙派笛子.md "wikilink")。並將[嗩吶的循環換氣法應用到笛子上](../Page/嗩吶.md "wikilink")。

## 演藝事業

趙松庭9歲開始學習笛子，14歲任業餘劇團「正吹」。16歲中學畢業後，加入昆曲坐唱班跟隨[葉小苟學習](../Page/葉小苟.md "wikilink")。1947年應父命考入[上海法學院攻讀法律](../Page/上海法學院.md "wikilink")。

1949年放棄修讀法律加入[文工團](../Page/文工團.md "wikilink")。其後參與[朝鮮戰爭](../Page/朝鲜战争.md "wikilink")，1954年於中國東北養傷其間創作《[早晨](../Page/早晨.md "wikilink")》、《[三五七](../Page/三五七.md "wikilink")》等笛子曲，並錄制了《早晨》、《和平鴿》、《牧羊歌》等唱片。

1956年，加入[浙江民間歌舞團及](../Page/浙江民間歌舞團.md "wikilink")[中國音樂家協會](../Page/中國音樂家協會.md "wikilink")，並於第1屆全國音樂周上演奏《早晨》取得成功。同年[周恩來邀請趙松庭](../Page/周恩來.md "wikilink")、[傅聰](../Page/傅聰.md "wikilink")、[黃虹](../Page/黃虹.md "wikilink")、[周小燕等文藝界名人周恩來的家中作客](../Page/周小燕.md "wikilink")，被周恩來提名參加中國青年藝術家代表團到西歐訪問。

1957年，從[俄羅斯](../Page/俄羅斯.md "wikilink")[世界青年聯歡節回國後被指控為](../Page/世界青年聯歡節.md "wikilink")「笛子指揮黨」，並被強迫送往農村勞動。其間，趙松庭從不間斷笛子練習，並在此時創作《歡樂的山谷》及《婺江風光》等曲。

1962年，在[周恩來及省委的干涉下](../Page/周恩來.md "wikilink")，趙松庭得以回到浙江民間歌舞團。

在1964年的第5屆[上海之春音樂會上](../Page/上海之春.md "wikilink")，趙松庭以自行研製的排笛吹奏《婺江風光》和《采茶忙》而獲得好評。上海《[文匯報](../Page/文匯報.md "wikilink")》稱讚《婺江風光》是一首「以樂器寫心靈」的傑作。

1966年[文化大革命期間](../Page/文化大革命.md "wikilink")，趙松庭被剝奪表演權利。轉而從事笛子研究，在其物理學家弟弟[趙松齡的協助下](../Page/趙松齡.md "wikilink")，對計算笛子的音波頻率及其應用、溫度、音準等問題進行研究，為笛子制作提供嚴謹的數據。據估計，趙松庭在整個文革期間共製做了近2000支笛子。

1979年，趙松庭被獲准表演，並演奏其新作《幽蘭逢春》。

## 教學事業

1956年，趙松庭出版《趙松庭的笛子》。

1972年，根據笛子科研的成果，在《樂器科技》中發表《橫笛頻率計算與應用》一文。

1973年，在被調回浙江民間歌舞團不久，趙松庭被打為[右傾翻案份子](../Page/右傾翻案份子.md "wikilink")，被分配看門、掃地、燒開水等工作。其後培訓出[蔣國基](../Page/蔣國基.md "wikilink")、[杜如松](../Page/杜如松.md "wikilink")、[張維良](../Page/張維良.md "wikilink")、[詹永明](../Page/詹永明.md "wikilink")、[戴亞等浙派笛子演奏家](../Page/戴亞.md "wikilink")。

1976年，趙松庭往浙江省藝術學校任教，其後任該校名譽校長。1980年後，趙松庭分別於[上海音樂學院](../Page/上海音樂學院.md "wikilink")、[中國音樂學院任教](../Page/中國音樂學院.md "wikilink")，先後於[香港](../Page/香港.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[南京](../Page/南京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[武漢](../Page/武漢.md "wikilink")、[濟南](../Page/濟南.md "wikilink")、[成都](../Page/成都.md "wikilink")、[重慶](../Page/重慶.md "wikilink")、[內蒙古以及](../Page/內蒙古.md "wikilink")[東南亞講學](../Page/東南亞.md "wikilink")。

1982年，應[中央人民廣播電台繳請舉辦](../Page/中央人民廣播電台.md "wikilink")《笛子演奏技巧》講座。

## 參考資料

  - [趙松庭(笛韻)](http://www.diyun.com/zst.htm)
  - [趙松庭(中國民樂)](https://web.archive.org/web/20040711085226/http://www.china.org.cn/chinese/minyue/432119.htm)

[Category:中国音乐家](../Category/中国音乐家.md "wikilink")
[Category:笛演奏家](../Category/笛演奏家.md "wikilink")
[Category:金华人](../Category/金华人.md "wikilink")
[Category:赵姓](../Category/赵姓.md "wikilink")
[Category:上海音乐学院教授](../Category/上海音乐学院教授.md "wikilink")
[Category:中国音乐家协会成员](../Category/中国音乐家协会成员.md "wikilink")