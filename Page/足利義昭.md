**足利義昭**（）是[日本](../Page/日本.md "wikilink")[足利幕府末代](../Page/室町幕府.md "wikilink")[將軍](../Page/征夷大將軍.md "wikilink")，足利義昭為前任將軍[足利義輝之弟](../Page/足利義輝.md "wikilink")，年輕時曾遁入[空門](../Page/空門.md "wikilink")，法號**覺慶**。足利義輝被叛臣弒殺後，被[细川藤孝等擁立為將軍而還俗](../Page/细川藤孝.md "wikilink")，改名**義秋**，後改名義昭。足利義昭曾經流落於[南近江的](../Page/近江國.md "wikilink")[六角義賢](../Page/六角義賢.md "wikilink")、[若狹的](../Page/若狹國.md "wikilink")[武田義統與](../Page/武田義統.md "wikilink")[越前的](../Page/越前國.md "wikilink")[朝倉義景等](../Page/朝倉義景.md "wikilink")[大名處苦苦哀求幫助他復興室町幕府](../Page/大名.md "wikilink")，但都沒有成功，而且被蔑稱為「**貧乏公方**」。後來才在[尾張的](../Page/尾張國.md "wikilink")[織田信長幫助之後而得於](../Page/織田信長.md "wikilink")1568年上洛而正式成為室町幕府第十五代[征夷大將軍](../Page/征夷大將軍.md "wikilink")。\[1\]

但是之後[織田信長便擅自擴大權力並限制足利義昭的行動](../Page/織田信長.md "wikilink")，足利義昭便與[甲斐的](../Page/甲斐國.md "wikilink")[武田信玄](../Page/武田信玄.md "wikilink")、[安藝的](../Page/安藝國.md "wikilink")[毛利元就](../Page/毛利元就.md "wikilink")、[越後的](../Page/越後國.md "wikilink")[上杉謙信](../Page/上杉謙信.md "wikilink")、[越前的](../Page/越前國.md "wikilink")[朝倉義景](../Page/朝倉義景.md "wikilink")、[北近江的](../Page/近江國.md "wikilink")[淺井長政](../Page/淺井長政.md "wikilink")、[攝津](../Page/攝津國.md "wikilink")[大坂的](../Page/大阪府.md "wikilink")[本願寺顯如等大名聯合反制](../Page/本願寺顯如.md "wikilink")[織田信長](../Page/織田信長.md "wikilink")，形成針對[織田信長的](../Page/織田信長.md "wikilink")[信長包圍網](../Page/信長包圍網.md "wikilink")；1573年叛臣[織田信長](../Page/織田信長.md "wikilink")（信長1559年出仕室町幕府）率軍將足利義昭驅逐出[京都並放逐到](../Page/京都市.md "wikilink")[河內](../Page/河內國.md "wikilink")[三好義繼居城若江城](../Page/三好義繼.md "wikilink")，[足利幕府因反抗信長而毀滅](../Page/室町幕府.md "wikilink")。

足利義昭被信長趕出[京都後](../Page/京都.md "wikilink")，但他的將軍的官位並未真正解除，被流放的義昭前往投奔[安藝的](../Page/安藝.md "wikilink")[毛利輝元](../Page/毛利輝元.md "wikilink")。之後投靠[羽柴秀吉](../Page/豐臣秀吉.md "wikilink")；平民出身的[羽柴秀吉曾經要求成為足利義昭養子](../Page/豐臣秀吉.md "wikilink")，以便取得武家身份建立[幕府政權](../Page/幕府.md "wikilink")，但被足利義昭拒絕（此历史记载于《丰臣秀吉谱》，真实性尚未确认）。1588年義昭出家，辭去[征夷大將軍一職](../Page/征夷大將軍.md "wikilink")，[准三宮宣下](../Page/准三宮.md "wikilink")，足利義昭於1597年逝世。

## 生平

### 將軍之路

足利義昭是室町幕府的第12代將軍[足利義晴的次子](../Page/足利義晴.md "wikilink")，其母親是[近衛尚通的女兒](../Page/近衛尚通.md "wikilink")[慶壽院](../Page/慶壽院.md "wikilink")。他也是第13代將軍[足利義輝的同胞弟弟](../Page/足利義輝.md "wikilink")。足利義昭出生後，被外祖父近衛尚通收為[猶子](../Page/猶子.md "wikilink")；而兄長義輝早已被立為嗣子，根據當時[足利將軍家的慣例](../Page/足利氏.md "wikilink")，未能獲得嗣子地位的將軍之子都要出家，因此被送入佛門，進入[興福寺的](../Page/興福寺.md "wikilink")[一乘院](../Page/一乘院.md "wikilink")，法名**覺慶**。

覺慶法師在興福寺擔任[權少僧都的高級官職](../Page/權少僧都.md "wikilink")，本來他一生應該是在擔任高級僧官中度過的。但在1565年（[永祿](../Page/永祿.md "wikilink")8年）發生了[永祿之變](../Page/永祿之變.md "wikilink")，第13代將軍[足利義輝連同其母](../Page/足利義輝.md "wikilink")[慶壽院一起](../Page/慶壽院.md "wikilink")，被[松永久秀](../Page/松永久秀.md "wikilink")、[三好三人眾弑殺](../Page/三好三人眾.md "wikilink")；擔任[鹿苑院院主的弟弟](../Page/鹿苑院.md "wikilink")[周暠也被久秀誘殺](../Page/足利周暠.md "wikilink")。由於害怕殺害覺慶會引發興福寺僧眾的敵對情緒，松永久秀沒有立刻殺害覺慶，而是將其暫時監禁在興福寺。但不久忠於足利義輝的[一色藤長](../Page/一色藤長.md "wikilink")、[和田惟政](../Page/和田惟政.md "wikilink")、[仁木義政](../Page/六角氏綱.md "wikilink")、[三淵藤英](../Page/三淵藤英.md "wikilink")、[細川藤孝](../Page/細川藤孝.md "wikilink")，以及[大覺寺](../Page/大覺寺.md "wikilink")[門跡](../Page/門跡.md "wikilink")[義俊](../Page/大覺寺義俊.md "wikilink")（近衛尚通之子）等幫助覺慶逃出了監牢。

在此期間的文書中，覺慶被稱作「將軍家的當主」、「矢島的武家御所」等。4月21日被敘為[從五位下](../Page/從五位下.md "wikilink")[左馬頭的官位](../Page/左馬頭.md "wikilink")。經過[奈良](../Page/奈良.md "wikilink")、[木津川逃到](../Page/木津川.md "wikilink")[伊賀後](../Page/伊賀國.md "wikilink")（伊賀國的守護是將軍的近臣[仁木義政](../Page/六角氏綱.md "wikilink")，該國國人[服部氏後來追隨義昭](../Page/服部氏.md "wikilink")），通過[南近江的](../Page/近江國.md "wikilink")[六角義賢的許可後才暫時居住在了](../Page/六角義賢.md "wikilink")[甲賀郡的](../Page/甲賀郡.md "wikilink")[和田城](../Page/和田城.md "wikilink")（和田惟政的居城，位於伊賀和近江交界之處）。

覺慶試圖恢復衰弱不堪的足利將軍家的實權，永祿9年（1566年）2月17日，覺慶還俗，改名**足利義秋**。在[和田惟政](../Page/和田惟政.md "wikilink")（甲賀住人）和[仁木義政](../Page/六角氏綱.md "wikilink")（伊賀住人）的斡旋下，[六角義賢](../Page/六角義賢.md "wikilink")、[義治父子同意讓足利義秋以](../Page/六角義治.md "wikilink")[野洲郡矢島村](../Page/野洲郡.md "wikilink")（今[守山市](../Page/守山市.md "wikilink")[矢島町](../Page/矢島町.md "wikilink")）為住所。

居住在[矢島御所的足利義秋](../Page/矢島御所.md "wikilink")，積極同[管領](../Page/管領.md "wikilink")[畠山高政](../Page/畠山高政.md "wikilink")、[關東管領](../Page/關東管領.md "wikilink")[上杉輝虎](../Page/上杉謙信.md "wikilink")、[能登](../Page/能登國.md "wikilink")[守護](../Page/守護.md "wikilink")[畠山義綱](../Page/畠山義綱.md "wikilink")（居住在近江[滋賀郡](../Page/滋賀郡.md "wikilink")）取得聯絡，希望得到支持。[河內國的畠山高政積極支持義秋](../Page/河內國.md "wikilink")，其弟弟[秋高也表示支持](../Page/畠山秋高.md "wikilink")。得知此事後，[三好三人眾率](../Page/三好三人眾.md "wikilink")3000騎襲擊了矢島御所；[大草氏等](../Page/大草氏.md "wikilink")[奉公眾](../Page/奉公眾.md "wikilink")（將軍親衛隊）浴血奮戰擊退了襲擊。但義秋發現自己視為心腹的南近江國主[六角義治暗中與三好三人眾勾結](../Page/六角義治.md "wikilink")，義秋只得在8月前往[若狹投靠](../Page/若狹國.md "wikilink")[武田義統](../Page/武田義統.md "wikilink")，。但當時[若狹武田氏正處於家督爭奪和重臣謀反的內亂中](../Page/武田氏#若狹武田氏.md "wikilink")，無力支援義秋[上洛](../Page/上洛.md "wikilink")；武田義統僅派了弟弟[武田信景前往幫助義秋](../Page/武田信景.md "wikilink")。足利義秋只得前往[越前](../Page/越前國.md "wikilink")，哀求[朝倉義景](../Page/朝倉義景.md "wikilink")（仁木義政的親族）出兵上洛。足利義秋上奏朝廷，將義景的母親封為[從二位官位](../Page/從二位.md "wikilink")。但義景只有對足利將軍家的連枝[鞍谷公方](../Page/鞍谷公方.md "wikilink")[足利嗣知](../Page/足利嗣知.md "wikilink")（[足利義嗣的子孫](../Page/足利義嗣.md "wikilink")）扶上將軍之位有興趣，對還俗的義秋ㄧ點興趣也沒有。足利義秋長期滯留越前，[上野清延](../Page/上野清延.md "wikilink")、[大館晴忠等幕府遺臣紛紛前往越前參見將軍](../Page/大館晴忠.md "wikilink")。

永祿11年（1568年）4月15日，足利義秋在越前舉行[元服禮](../Page/元服.md "wikilink")，由[朝倉義景擔任加冠役](../Page/朝倉義景.md "wikilink")。同時由於「秋」字不吉利，改名**義昭**。在朝倉家重臣[明智光秀的介紹下](../Page/明智光秀.md "wikilink")，前往[尾張](../Page/尾張國.md "wikilink")，請求管領[斯波家的有力家臣](../Page/斯波氏.md "wikilink")[織田信長的幫助](../Page/織田信長.md "wikilink")。

### 再興幕府

[Ashikaga_yoshiaki.jpg](https://zh.wikipedia.org/wiki/File:Ashikaga_yoshiaki.jpg "fig:Ashikaga_yoshiaki.jpg")藏）\]\]
永祿11年（1568年）9月，織田信長擁護足利義昭上洛，途中又受到[北近江](../Page/近江國.md "wikilink")[淺井家的支持](../Page/淺井氏.md "wikilink")，在織田信長、[淺井長政等警護下進軍](../Page/淺井長政.md "wikilink")[京都](../Page/京都市.md "wikilink")。在途中受到[南近江](../Page/近江國.md "wikilink")[六角家](../Page/六角義賢.md "wikilink")[六角義賢](../Page/六角義賢.md "wikilink")[六角義治父子的阻礙](../Page/六角義治.md "wikilink")，但由於[六角家不敵](../Page/六角義賢.md "wikilink")[織田家的軍力而向](../Page/織田信長.md "wikilink")[伊賀撤退](../Page/伊賀國.md "wikilink")（[觀音寺城之戰](../Page/觀音寺城之戰.md "wikilink")），在父親[足利義晴所建的](../Page/足利義晴.md "wikilink")[桑實寺駐紮](../Page/桑實寺.md "wikilink")，順利到達京都。[三好三人眾退出京都](../Page/三好三人眾.md "wikilink")。10月18日朝廷封足利義昭為[征夷大將軍](../Page/征夷大將軍.md "wikilink")，同時敘[從四位下](../Page/從四位下.md "wikilink")[參議兼](../Page/參議.md "wikilink")[左近衛權中將的官位](../Page/左近衛權中將.md "wikilink")。

就任將軍的義昭下令將對弒殺兄長義輝持縱容態度、慫恿[天皇將封](../Page/正親町天皇.md "wikilink")[足利義榮為將軍的](../Page/足利義榮.md "wikilink")[近衛前久流放](../Page/近衛前久.md "wikilink")，並讓[二條晴良復任](../Page/二條晴良.md "wikilink")[關白之職](../Page/關白.md "wikilink")。義昭又將自己的[偏諱授予](../Page/偏諱.md "wikilink")[管領](../Page/管領.md "wikilink")[細川昭元](../Page/細川昭元.md "wikilink")、[畠山昭高](../Page/畠山昭高.md "wikilink")，以及關白家的[二條昭實](../Page/二條昭實.md "wikilink")，試圖鞏固自己的統治，掌握了兄長義輝所擁有的[山城國](../Page/山城國.md "wikilink")[御料所](../Page/御料所.md "wikilink")。同時在山城國設置守護，令[三淵藤英據守](../Page/三淵藤英.md "wikilink")[伏見城](../Page/伏見城.md "wikilink")。在政務上，義昭同兄長義輝一樣，任命[攝津晴門為](../Page/攝津晴門.md "wikilink")[政所執事](../Page/政所執事.md "wikilink")，任命[飯尾昭連](../Page/飯尾昭連.md "wikilink")、[松田藤弘等為](../Page/松田藤弘.md "wikilink")[奉公眾](../Page/奉公眾.md "wikilink")，再開幕政。之前因反叛兄長義輝而被滅的[伊勢氏](../Page/伊勢氏.md "wikilink")，其末裔[伊勢貞興被義昭允許復任官職](../Page/伊勢貞興.md "wikilink")。

當時足利義昭暫時以[山城國](../Page/山城國.md "wikilink")[本圀寺](../Page/本圀寺.md "wikilink")（位於今[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[山科區](../Page/山科區.md "wikilink")）為居住地。永祿12年（1569年）正月，當[信長率軍返回](../Page/織田信長.md "wikilink")[美濃](../Page/美濃國.md "wikilink")、[尾張時](../Page/尾張國.md "wikilink")，[三好三人眾趁機反撲](../Page/三好三人眾.md "wikilink")，突襲本圀寺（[本圀寺之變](../Page/本圀寺之變.md "wikilink")）。奉公眾和[明智光秀所率的織田軍奮勇抵抗](../Page/明智光秀.md "wikilink")，北近江的[淺井長政以及](../Page/淺井長政.md "wikilink")[攝津的](../Page/攝津國.md "wikilink")[池田勝正](../Page/池田勝正.md "wikilink")、[和田惟政等也奮戰](../Page/和田惟政.md "wikilink")，擊退了三好三人眾的進攻。

這次事件暴露了本圀寺守備不足的現狀，因此足利義昭向請求[信長重建了足利義輝建造的烏丸中御門第](../Page/織田信長.md "wikilink")（舊[二條城](../Page/二條城.md "wikilink")），以之為將軍邸。重修的烏丸中御門第新挖了兩道護城河，並翻修加高了石垣，增強了防禦機能。只有世代擔任室町幕府[奉公眾的武士和高級](../Page/奉公眾.md "wikilink")[守護大名才能進入拜見](../Page/守護大名.md "wikilink")。

### 與[信長的對立](../Page/織田信長.md "wikilink")

[Makishimaj02.jpg](https://zh.wikipedia.org/wiki/File:Makishimaj02.jpg "fig:Makishimaj02.jpg")
足利義昭依據當初的許諾，就任將軍之後，在10月24日給予[信長](../Page/織田信長.md "wikilink")「御父織田彈正忠（信長）殿」的尊稱，以請求信長對[幕府的支持](../Page/室町幕府.md "wikilink")。

[信長將義昭扶上將軍之位後](../Page/織田信長.md "wikilink")，其對[尾張](../Page/尾張國.md "wikilink")、[美濃的領有權得到了認可](../Page/美濃國.md "wikilink")，並被封為[和泉](../Page/和泉國.md "wikilink")[守護](../Page/守護.md "wikilink")，得到昔日[三好家的領地](../Page/三好長慶.md "wikilink")[和泉國](../Page/和泉國.md "wikilink")（繁榮的[堺港在該國轄下](../Page/堺.md "wikilink")）。同時義昭對其他武將論功行賞，[池田勝正被任命為](../Page/池田勝正.md "wikilink")[攝津](../Page/攝津國.md "wikilink")[守護](../Page/守護.md "wikilink")，[畠山高政](../Page/畠山高政.md "wikilink")、[三好義繼各被封為](../Page/三好義繼.md "wikilink")[河內半國守護](../Page/河內國.md "wikilink")。[信長被任命為](../Page/織田信長.md "wikilink")[管領代](../Page/管領代.md "wikilink")，其地位與管領相當；此後足利義昭又向朝廷推薦[信長擔任](../Page/織田信長.md "wikilink")[副將軍](../Page/副將軍.md "wikilink")，但被[信長推辭了](../Page/織田信長.md "wikilink")，改封[彈正忠](../Page/彈正忠.md "wikilink")。

然而足利義昭一心想要復興[足利幕府](../Page/室町幕府.md "wikilink")，而[信長則有用武力統一天下的野心](../Page/織田信長.md "wikilink")，因此兩者最終關係逐漸惡化。[信長為了限制](../Page/織田信長.md "wikilink")[將軍的權力](../Page/征夷大將軍.md "wikilink")，於永祿12年（1569年）正月頒佈了《[殿中御掟](../Page/殿中御掟.md "wikilink")》9條，\[2\]並逼迫義昭承認。這使[幕府將軍的行動受到了很大的制約](../Page/征夷大將軍.md "wikilink")。翌年正月又追加了5條，信長更進一步限制了幕府的權力。[元龜元年](../Page/元龜.md "wikilink")（1570年）4月，織田信長討伐[越前大名](../Page/越前國.md "wikilink")[朝倉義景](../Page/朝倉義景.md "wikilink")，但就在此時[織田家的同盟](../Page/織田氏.md "wikilink")[淺井長政反叛信長](../Page/淺井長政.md "wikilink")，導致信長大敗。早已對信長的專橫十分不滿的足利義昭趁機與信長決裂，元龜2年（1571年）左右向[越後](../Page/越後國.md "wikilink")[上杉輝虎](../Page/上杉謙信.md "wikilink")（謙信）、向[安藝](../Page/安藝國.md "wikilink")[毛利輝元](../Page/毛利輝元.md "wikilink")、[大坂](../Page/大阪市.md "wikilink")[本願寺顯如](../Page/本願寺顯如.md "wikilink")、向[甲斐](../Page/甲斐國.md "wikilink")[武田信玄](../Page/武田信玄.md "wikilink")、\[3\][六角義賢等大名發出](../Page/六角義賢.md "wikilink")[御內書](../Page/御內書.md "wikilink")，下令討伐信長。這些大名加上與信長敵對的朝倉義景、淺井長政、[松永久秀](../Page/松永久秀.md "wikilink")、[三好義繼以及](../Page/三好義繼.md "wikilink")[三好三人眾](../Page/三好三人眾.md "wikilink")、[延曆寺](../Page/延曆寺.md "wikilink")[僧兵等勢力](../Page/僧兵.md "wikilink")，形成了[信長包圍網](../Page/信長包圍網.md "wikilink")。

元龜3年（1572年）10月，信長向義昭送達了17條責問狀，批評了義昭的一些舉措。\[4\]此時東方的[武田信玄率兵進軍京都](../Page/武田信玄.md "wikilink")，12月22日在[三方原之戰中擊敗了信長的盟友](../Page/三方原之戰.md "wikilink")[三河的](../Page/三河國.md "wikilink")[德川家康](../Page/德川家康.md "wikilink")，信長陷入窘境。足利義昭趁機任命寵臣[山岡景友](../Page/山岡景友.md "wikilink")（[六角義賢的重臣以及幕府的](../Page/六角義賢.md "wikilink")[奉公眾](../Page/奉公眾.md "wikilink")）為山城半國守護。翌年正月，信長欲遣子入質於義昭以求和解，但遭義昭的拒絕。足利義昭在[近江的](../Page/近江國.md "wikilink")[今堅田城和](../Page/今堅田城.md "wikilink")[石山城為幕府軍的據點](../Page/石山城.md "wikilink")，舉起反對信長的旗幟，但數日後兩城皆被攻陷。同時東方戰線的武田信玄在[上洛的路途中病重](../Page/上洛.md "wikilink")，武田軍於4月開始撤回本國[甲斐](../Page/甲斐國.md "wikilink")，12日信玄在回國的路途中在[信濃駒場病逝](../Page/信濃國.md "wikilink")。

信長殺入京都，在[知恩院佈開陣勢](../Page/知恩院.md "wikilink")。幕臣[細川藤孝](../Page/細川藤孝.md "wikilink")、[荒木村重等人見大勢已去](../Page/荒木村重.md "wikilink")，投降了信長。但是不知道武田信玄已經病逝的足利義昭還愚蠢的死守在自己的居城[烏丸中御門第想繼續抵抗](../Page/二條城.md "wikilink")。信長再次提出和解，但義昭認為信長沒有信用，斷然拒絕。信長威脅幕臣以及義昭的支持者，聲稱若不投降，自己將會攻打上京並將此整個地區焚為焦土。同時包圍了義昭的居城烏丸中御門第。另一方面，信長尋求朝廷的支持，4月5日，在[正親町天皇敕命下](../Page/正親町天皇.md "wikilink")，雙方講和。

但是足利義昭於7月3日宣佈背棄和約，在[三淵藤英](../Page/三淵藤英.md "wikilink")、[伊勢貞興以及公家奉公眾的護衛下逃往南山城的要害](../Page/伊勢貞興.md "wikilink")[槇島城](../Page/槇島城.md "wikilink")（[山城國的守護所](../Page/山城國.md "wikilink")）舉兵。槇島城建在[宇治川和](../Page/宇治川.md "wikilink")[巨椋池水系交叉處的島上](../Page/巨椋池.md "wikilink")，是足利義昭近臣[真木島昭光的居城](../Page/真木島昭光.md "wikilink")，為兵家必爭之地。烏丸中御門第的守軍於3日投降了信長，信長率7萬大軍包圍了槇島城。7月18日織田軍開始攻城，槇島城的建築被破壞，足利義昭以自己的兒子義尋為人質，向信長乞降。

### 被逐出京都

信長將足利義昭逐出京都後，並將足利將軍家在[山城](../Page/山城國.md "wikilink")、[丹波](../Page/丹波國.md "wikilink")、[近江](../Page/近江國.md "wikilink")、[若狹等地的御料所據為己有](../Page/若狹國.md "wikilink")。信長以天下人（掌握日本實權的人）自居，挾持幕府將軍和天皇支配京都周圍的地區，並充當各地大名紛爭的調停人。放逐將軍後，信長保持了天下人的地位，\[5\]現在日本的歷史教科書一般認為此時[足利幕府已滅亡](../Page/室町幕府.md "wikilink")。但事實上根據《公卿補任》的記載，此後的義昭依然長期保持著[征夷大將軍的官位](../Page/征夷大將軍.md "wikilink")。[天正元年](../Page/天正.md "wikilink")8月（即1573年。元龜4年7月28日，改元天正），織田信長毀滅[越前](../Page/越前國.md "wikilink")[朝倉家](../Page/朝倉氏.md "wikilink")，9月毀滅[北近江](../Page/近江國.md "wikilink")[淺井氏](../Page/淺井氏.md "wikilink")，信長包圍網完全崩解。信長於次年任命[塙直政為](../Page/塙直政.md "wikilink")[山城](../Page/山城國.md "wikilink")、[大和](../Page/大和國.md "wikilink")[守護](../Page/守護.md "wikilink")，鞏固了[織田家在近畿內的支配權](../Page/織田氏.md "wikilink")。

足利義昭被放逐後，退往枇杷莊（今[京都府](../Page/京都府.md "wikilink")[城陽市](../Page/城陽市.md "wikilink")），在[本願寺顯如的介紹下](../Page/本願寺顯如.md "wikilink")，被信長家臣[羽柴秀吉護送到](../Page/豐臣秀吉.md "wikilink")[河內](../Page/河內國.md "wikilink")[三好義繼的居城若江城](../Page/三好義繼.md "wikilink")，足利義昭仍然在此開設幕政，以[伊勢氏](../Page/伊勢氏.md "wikilink")、[高氏](../Page/高氏.md "wikilink")、[一色氏](../Page/一色氏.md "wikilink")、[上野氏](../Page/上野氏.md "wikilink")、[細川氏](../Page/細川氏.md "wikilink")、[大館氏](../Page/大館氏.md "wikilink")、[飯尾氏](../Page/飯尾氏.md "wikilink")、松田氏、大草氏等勢力為幕府的中樞，組成了[奉公眾和](../Page/奉公眾.md "wikilink")[奉行眾](../Page/奉行眾.md "wikilink")，並授予近臣和大名室町幕府中的官職。\[6\]除了[近畿周邊的信長勢力圈以外](../Page/近畿地方.md "wikilink")（[北陸地方](../Page/北陸道.md "wikilink")、[中國地方](../Page/中國地方.md "wikilink")、[九州地方](../Page/九州島.md "wikilink")），足利義昭依然保持著被流放以前的權力，甚至對[京都五山的住持還有任命權](../Page/京都五山.md "wikilink")。在[信長與](../Page/織田信長.md "wikilink")[三好義繼的關係惡化後](../Page/三好義繼.md "wikilink")，義昭於11月5日移駕到[和泉國的](../Page/和泉國.md "wikilink")[堺](../Page/堺.md "wikilink")。足利義昭曾提出要返回[京都](../Page/京都市.md "wikilink")，但[信長提出送交人質的條件](../Page/織田信長.md "wikilink")，最終交涉破裂。

1574年移駕[紀伊國](../Page/紀伊國.md "wikilink")[興國寺](../Page/興國寺.md "wikilink")，又移駕[泊城](../Page/泊城.md "wikilink")。當時紀伊國是[管領](../Page/管領.md "wikilink")[畠山氏的領地](../Page/畠山氏.md "wikilink")，值得一提的是[畠山高政的重臣](../Page/畠山高政.md "wikilink")[湯川直春的勢力在當時很強大](../Page/湯川直春.md "wikilink")，直春的父親[湯川直光擔任河內](../Page/湯川直光.md "wikilink")[守護代一職](../Page/守護代.md "wikilink")。

1576年（天正4年），足利義昭移駕到[毛利輝元領地內的](../Page/毛利輝元.md "wikilink")[備後國的](../Page/備後國.md "wikilink")[鞆](../Page/鞆町.md "wikilink")。鞆是當年[足利尊氏接受](../Page/足利尊氏.md "wikilink")[光嚴上皇追討](../Page/光嚴天皇.md "wikilink")[新田義貞](../Page/新田義貞.md "wikilink")[院宣的地方](../Page/院宣.md "wikilink")，對於足利將軍家可謂是有淵源的地方。第十代將軍[足利義材在此處受到](../Page/足利義材.md "wikilink")[大內家的支持](../Page/大內氏.md "wikilink")，從而回到了京都。因此此地被足利將軍家當作吉祥之地。此後義昭在此地開設流亡幕府，史稱「鞆幕府」。足利義昭在鞆以[備中國的御料所所進獻的](../Page/備中國.md "wikilink")[年貢](../Page/年貢.md "wikilink")，以及足利將軍家任命[京都五山住持所獲得的](../Page/京都五山.md "wikilink")[禮錢維持生活](../Page/禮錢.md "wikilink")，同時收到[宗家](../Page/宗氏.md "wikilink")、[島津家通過](../Page/島津氏.md "wikilink")[明日貿易獲得的財政收入](../Page/明日貿易.md "wikilink")，近畿、[東海以外支持足利將軍家的武家很多](../Page/東海道.md "wikilink")。鑒於此，足利義昭向全國大名下達了討伐信長的[御內書](../Page/御內書.md "wikilink")，天正4年[甲斐的](../Page/甲斐國.md "wikilink")[武田家](../Page/武田氏.md "wikilink")、[相模國的](../Page/相模.md "wikilink")[北條家](../Page/後北條氏.md "wikilink")、[越後的](../Page/越後國.md "wikilink")[上杉家結為同盟](../Page/上杉氏.md "wikilink")，但對信長的影響成效甚微。

### 信長的死以及回到京都

1577年[上杉謙信在](../Page/上杉謙信.md "wikilink")[手取川之戰擊敗織田軍](../Page/手取川之戰.md "wikilink")，但不久上杉謙信就在1578年（天正6年）病逝，上杉軍勢退回北陸，隨後其繼承人因繼承問題爆發內戰，即[御館之亂](../Page/御館之亂.md "wikilink")，從此上杉家國力大傷，再無與織田家一較高下之實力。

1580年（天正8年）[一向宗本山](../Page/一向宗.md "wikilink")[石山本願寺不堪長年圍困於城中](../Page/石山本願寺.md "wikilink")，最後接受信長方面的議和條件，退出本願寺，不再干涉世俗，實質上向信長投降，一向宗一揆暴動就此瓦解。此時日本的經濟、政治核心地帶及周邊再無對織田家的威脅，其勢力達到鼎盛，並劍指西方大國毛利家。

然而1582年（天正10年）[明智光秀發動](../Page/明智光秀.md "wikilink")[本能寺之變](../Page/本能寺之變.md "wikilink")，[信長及其嗣子](../Page/織田信長.md "wikilink")[信忠皆戰死](../Page/織田信忠.md "wikilink")。此時足利義昭身在備後國的鞆。而明智光秀麾下的家臣，如[伊勢貞興](../Page/伊勢貞興.md "wikilink")、[蜷川貞周等](../Page/蜷川貞周.md "wikilink")，多為昔日室町幕府的幕臣。

足利義昭趁此好機欲返回京都，尋求[毛利輝元的支持](../Page/毛利輝元.md "wikilink")；另一方面尋求[羽柴秀吉和](../Page/豐臣秀吉.md "wikilink")[柴田勝家的支持](../Page/柴田勝家.md "wikilink")。[毛利家中親秀吉的](../Page/毛利氏.md "wikilink")[小早川隆景等極力反對送義昭到京都](../Page/小早川隆景.md "wikilink")，認為這樣會導致羽柴家和毛利家之間關係惡劣。但1583年（天正11年）毛利輝元、柴田勝家、[德川家康著手籌備支持義昭上洛](../Page/德川家康.md "wikilink")。同年毛利輝元臣服於羽柴秀吉。1586年羽柴秀吉成為[關白](../Page/關白.md "wikilink")[太政大臣](../Page/太政大臣.md "wikilink")。此後進入了「關白秀吉、將軍義昭」並立的2年時期。在此兩年裡，豐臣秀吉逐漸統一天下。

1587年（天正15年），豐臣秀吉在[九州征伐期間途經](../Page/九州征伐.md "wikilink")[備後國](../Page/備後國.md "wikilink")[沼隈郡的津之郡村](../Page/沼隈郡.md "wikilink")，在田邊寺訪問了足利義昭，二人交換了[太刀](../Page/太刀.md "wikilink")。次年[薩摩國的](../Page/薩摩國.md "wikilink")[島津家向秀吉臣服](../Page/島津氏.md "wikilink")，義昭回到京都，1588年2月9日（天正16年1月13日）與關白豐臣秀吉一起參見了[正親町天皇](../Page/正親町天皇.md "wikilink")，辭去了征夷大將軍之職並出家，法號**昌山**。朝廷給予了[准三后的待遇](../Page/准三后.md "wikilink")。

### 晚年

豐臣秀吉將[山城國的](../Page/山城國.md "wikilink")[槇島城給予了他當作居城](../Page/槇島城.md "wikilink")，並給予了義昭一萬石的領地。由於足利義昭是前將軍的緣故，豐臣秀吉才給予他這樣高的待遇。在豐臣秀吉[侵略朝鮮時](../Page/萬曆朝鮮戰爭.md "wikilink")，義昭率軍200人來到[肥前](../Page/肥前.md "wikilink")[名護屋](../Page/名護屋.md "wikilink")，參加了戰鬥。

晚年與[斯波義銀](../Page/斯波義銀.md "wikilink")、[山名豐國等](../Page/山名豐國.md "wikilink")，成為[太閣豐臣秀吉](../Page/太閣.md "wikilink")[御伽眾的成員](../Page/御伽眾.md "wikilink")，受到太閣的親近。1597年（[慶長](../Page/慶長.md "wikilink")2年）在大坂逝世，享壽61歲。

## 人物與逸話

  - 一些日本的逸話記載：實現日本統一的[豐臣秀吉](../Page/豐臣秀吉.md "wikilink")，因是平民出身，曾要求成為足利義昭的養子，以便取得武家身份建立幕府政權，但為足利義昭拒絕。這個說法最早出自日本儒學家[林羅山](../Page/林羅山.md "wikilink")，但沒有證據表明這是事實。2011年的[大河劇](../Page/大河劇.md "wikilink")《[江～公主們的戰國～](../Page/江～公主們的戰國～.md "wikilink")》中採用了這個傳說，將足利義昭稱為「先將軍」，這與史實不符。
  - 一些以[信長及其家臣為主人公的作品中](../Page/織田信長.md "wikilink")，把足利義昭描繪成身體殘疾的一個昏君。事實上足利義昭身體健全，而日益壯大的[信長包圍網顯示他的政治水平](../Page/信長包圍網.md "wikilink")。
  - 兄長足利義輝死後，義昭在幕臣的擁護下四處流浪；被[信長驅逐出](../Page/織田信長.md "wikilink")[京都後四處流浪](../Page/京都市.md "wikilink")，投靠諸大名，因此被譏諷為「貧乏公方」。
  - 足利義昭入京就任將軍的時候，曾向朝廷奏請將年號改為[元龜](../Page/元龜.md "wikilink")。但[信長認為這樣會讓將軍恢復權力](../Page/織田信長.md "wikilink")，因此反對改元。[正親町天皇最終也沒有接受其改元意見](../Page/正親町天皇.md "wikilink")。\[7\]但足利義昭在4月23日織田信長出征討伐[越前](../Page/越前國.md "wikilink")[朝倉家時](../Page/朝倉氏.md "wikilink")，將年號改為元龜。

## 官歷

  - [永禄](../Page/永禄.md "wikilink")5年（1562年）成為[南都](../Page/奈良.md "wikilink")[一乘院](../Page/一乘院.md "wikilink")[門跡](../Page/門跡.md "wikilink")。
  - 永禄9年（1566年）敘[從五位下](../Page/從五位下.md "wikilink")，任[左馬頭](../Page/左馬頭.md "wikilink")。（叙位任官時期存在著疑問）
  - 永禄11年（1568年）10月18日昇敘[從四位下](../Page/從四位下.md "wikilink")，補任[参議](../Page/参議.md "wikilink")，兼任[左近衛中将](../Page/左近衛中将.md "wikilink")，封[征夷大将軍](../Page/征夷大将軍.md "wikilink")。
  - 永禄12年（1569年）6月22日昇敘[從三位](../Page/從三位.md "wikilink")，榮進**[權大納言](../Page/大納言.md "wikilink")**。
  - [天正](../Page/天正.md "wikilink")2年（1574年）被逐出京都，在[備後](../Page/備後國.md "wikilink")[鞆建立了流亡幕府](../Page/鞆町.md "wikilink")（鞆幕府）。
  - 天正16年（1588年）1月13日回到京都，辭去[將軍一職出家](../Page/征夷大将軍.md "wikilink")。受封為[准三宮](../Page/准三宮.md "wikilink")，接受與皇族同等的待遇。
  - [慶長](../Page/慶長.md "wikilink")2年（1597年）8月28日逝世。法號靈陽院昌山道休。

## 接受義昭偏諱人物

  - [石川昭光](../Page/石川昭光.md "wikilink")
  - [石川義宗](../Page/石川義宗.md "wikilink")
  - [二條昭實](../Page/二條昭實.md "wikilink")
  - [畠山昭高](../Page/畠山昭高.md "wikilink")
  - [細川昭元](../Page/細川信良.md "wikilink")
  - [細川昭賢](../Page/細川昭賢.md "wikilink")
  - [一色昭秀](../Page/一色昭秀.md "wikilink")
  - [畠山昭清](../Page/畠山昭清.md "wikilink")

<!-- end list -->

  - [畠山昭賢](../Page/畠山昭賢.md "wikilink")
  - [一色昭孝](../Page/唐橋在通.md "wikilink")
  - [一色昭辰](../Page/一色昭辰.md "wikilink")
  - [一色昭国](../Page/一色昭国.md "wikilink")
  - [大館昭長](../Page/大館昭長.md "wikilink")
  - [飯尾昭連](../Page/飯尾昭連.md "wikilink")
  - [畠山義繼](../Page/二本松義繼.md "wikilink")

<!-- end list -->

  - [宗昭景](../Page/宗義智.md "wikilink")
  - [有馬義純](../Page/有馬義純.md "wikilink")
  - [島津義弘](../Page/島津義弘.md "wikilink")
  - [大友義統](../Page/大友義統.md "wikilink")
  - [三淵秋豪](../Page/三淵秋豪.md "wikilink")
  - [三淵昭知](../Page/三淵昭知.md "wikilink")
  - [朽木昭貞](../Page/朽木昭貞.md "wikilink")
  - [朽木昭長](../Page/朽木昭長.md "wikilink")

<!-- end list -->

  - [飯川秋共](../Page/飯川秋共.md "wikilink")
  - [一色秋家](../Page/一色秋家.md "wikilink")
  - [一色秋教](../Page/一色秋教.md "wikilink")
  - [一色秋成](../Page/一色秋成.md "wikilink")
  - [真木島昭光](../Page/真木島昭光.md "wikilink")
  - [大草秋長](../Page/大草秋長.md "wikilink")
  - [最上義康](../Page/最上義康.md "wikilink")

## 其他家臣

  - [和田惟政](../Page/和田惟政.md "wikilink")（甲賀国人）
  - [山岡景友](../Page/山岡景友.md "wikilink")（甲賀国人）

## 登場作品

  - 小說

<!-- end list -->

  - 陰謀將軍（[新潮社](../Page/新潮社.md "wikilink")、[松本清張著](../Page/松本清張.md "wikilink")）
  - 御所車 最後的將軍・足利義昭（[文藝春秋](../Page/文藝春秋.md "wikilink")、著）
  - 足利義昭 流徙公方記（学陽書房、[水上勉著](../Page/水上勉.md "wikilink")）
  - 義輝異聞・遺恩（[德間書店](../Page/德間書店.md "wikilink")、著）

<!-- end list -->

  - 影視劇

<!-- end list -->

  - [太閤記](../Page/:JA:太閤記_\(NHK大河ドラマ\).md "wikilink")（1965年、[NHK大河劇](../Page/NHK大河劇.md "wikilink")、演：市村家橘）

  - [天與地](../Page/天與地_\(大河劇\).md "wikilink")（1969年、NHK大河劇、演：）

  - （1973年、NHK大河劇、演：[伊丹十三](../Page/伊丹十三.md "wikilink")）

  - （1973年、NET、演：）

  - （1978年、NHK大河劇、演：）

  - [戰國自衛隊](../Page/戰國自衛隊.md "wikilink")（1979年、東寶、演：）

  - [女太閤記](../Page/女太閤記.md "wikilink")（1981年、NHK大河劇、演：）

  - [德川家康](../Page/德川家康_\(大河劇\).md "wikilink")（1983年、NHK大河劇、演：[篠原大作](../Page/篠原大作.md "wikilink")）

  - [太閤記](../Page/:JA:太閤記_\(1987年のテレビドラマ\).md "wikilink")（1987年、TBS、演：）

  - [武田信玄](../Page/武田信玄_\(大河劇\).md "wikilink")（1988年、NHK大河劇、演：）

  - [織田信長](../Page/:JA:織田信長_\(1989年のテレビドラマ\).md "wikilink")（1989年、TBS、演：）

  - [信長KING OF
    ZIPANGU](../Page/信長KING_OF_ZIPANGU.md "wikilink")（1992年、NHK大河劇、演：）

  - [織田信長](../Page/:JA:織田信長_\(1994年のテレビドラマ\).md "wikilink")（1994年、TX、演：[京本政樹](../Page/京本政樹.md "wikilink")）

  - （1995年、TX、演：）

  - [秀吉](../Page/秀吉_\(大河劇\).md "wikilink")（1996年、NHK大河劇、演：[玉置浩二](../Page/玉置浩二.md "wikilink")）

  - [利家與松](../Page/利家與松.md "wikilink")（2002年、NHK大河劇、演：）

  - （2005年、TX、演：）

  - （2006年、EX、演：[京本政樹](../Page/京本政樹.md "wikilink")）

  - [功名十字路](../Page/功名十字路_\(大河劇\).md "wikilink")（2006年、NHK大河劇、演：[三谷幸喜](../Page/三谷幸喜.md "wikilink")）

  - （2007年、CX、演：[谷原章介](../Page/谷原章介.md "wikilink")）

  - [寧寧：女太閤記](../Page/寧寧：女太閤記.md "wikilink")（2009年、TX、演：）

  - [江\~公主們的戰國\~](../Page/江~公主們的戰國~.md "wikilink")（2011年、NHK大河劇、演：）

  - （2011年、TX、演：）

  - [女信長](../Page/女信長.md "wikilink")（2013年、CX、演：[佐藤二朗](../Page/佐藤二朗.md "wikilink")）

  - [信長的主廚](../Page/信長的主廚.md "wikilink")（2013年、EX、演：）

  - [濃姬](../Page/:JA:濃姫_\(テレビドラマ\).md "wikilink")（2013年、EX、演：[池田政典](../Page/池田政典.md "wikilink")）

  - [軍師官兵衛](../Page/軍師官兵衛.md "wikilink")
    （2014年、NHK大河劇、演：[吹越滿](../Page/吹越滿.md "wikilink")）

  - [信長協奏曲](../Page/信長協奏曲#電視劇.md "wikilink")（2014年、CX、演：）

## 相關條目

  - [鞆城](../Page/鞆城.md "wikilink")
  - [堀城](../Page/堀城.md "wikilink")
  - [八木城 (丹波国)](../Page/八木城_\(丹波国\).md "wikilink")
  - [觀音寺城之戰](../Page/觀音寺城之戰.md "wikilink")
  - [本圀寺之變](../Page/本圀寺之變.md "wikilink")
  - [野田城・福島城之戰](../Page/野田城・福島城之戰.md "wikilink")
  - [槙島城之戰](../Page/槙島城之戰.md "wikilink")
  - [有岡城之戰](../Page/有岡城之戰.md "wikilink")
  - [室町幕府將軍列表](../Page/室町幕府將軍列表.md "wikilink")

## 腳註

## 参考文献與傳記

  - [奧野高廣](../Page/奧野高廣.md "wikilink")《足利義昭》（[吉川弘文館人物叢書](../Page/吉川弘文館.md "wikilink")、1996年新装版）
    ISBN 4-642-05182-1
  - [桑田忠親](../Page/桑田忠親.md "wikilink")《流浪将軍
    足利義昭》（[講談社](../Page/講談社.md "wikilink")、1985年）
    ISBN 4-06-201850-0

[Yoshiaki](../Category/室町幕府將軍.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Yoshiaki](../Category/將軍足利氏.md "wikilink")
[Category:還俗人物](../Category/還俗人物.md "wikilink")

1.  國史大辭典（吉川弘文館）
2.  2日後追加了7条，總計16条。
3.  武田信玄於永祿11年攻打[今川家的領地](../Page/今川氏.md "wikilink")（[駿河侵攻](../Page/駿河侵攻.md "wikilink")），為了對抗[甲斐](../Page/甲斐國.md "wikilink")[武田家](../Page/武田氏.md "wikilink")，[相模的](../Page/相模國.md "wikilink")[後北條家與](../Page/後北條氏.md "wikilink")[越後的](../Page/越後國.md "wikilink")[上杉家結為同盟](../Page/上杉氏.md "wikilink")（[越相同盟](../Page/越相同盟.md "wikilink")）。同年，回到京都的足利義昭為三方勢力調停。翌年（永祿12年）武田信玄接受足利義昭和[織田信長的調停](../Page/織田信長.md "wikilink")，與[上杉謙信和解](../Page/上杉謙信.md "wikilink")（[甲越和與](../Page/甲越和與.md "wikilink")）。
4.  抄本被傳送到了日本各地，寫本在各地都有發現
5.  戰國時期的「天下」觀，參見[神田千里](../Page/神田千里.md "wikilink")「」《東洋大学文学部紀要》2002、同《》中央公論社、2002）。
6.  離開京都後仍然擁有統治天下權力的足利將軍還有[足利義詮](../Page/足利義詮.md "wikilink")、[足利義尚](../Page/足利義尚.md "wikilink")、[足利義稙](../Page/足利義稙.md "wikilink")、[足利義晴](../Page/足利義晴.md "wikilink")、[足利義輝等人](../Page/足利義輝.md "wikilink")。
7.  三年後，信長奏請改元「[天正](../Page/天正.md "wikilink")」。