**足厥陰肝經**（）是一條[經脈](../Page/經脈.md "wikilink")，[十二正经之一](../Page/十二正经.md "wikilink")，与[足少陽膽經相表里](../Page/足少陽膽經.md "wikilink")。本經起於[大敦](../Page/大敦穴.md "wikilink")，止於[期門](../Page/期門穴.md "wikilink")，左右各14個[腧穴](../Page/腧穴.md "wikilink")。在中医学上，肝经脉为诸筋之主导经脉。

## 经脉循行

起于足大趾上毫毛部（[大敦](../Page/大敦.md "wikilink")），经内踝前向上至内踝上八寸外处交出于[足太阴脾经之后](../Page/足太阴脾经.md "wikilink")，上行沿股内侧，进入阴毛中，绕阴器，上达小腹，挟胃旁，属肝络胆，过膈，分布于胁肋，沿喉咙后面，向上入鼻[咽部](../Page/咽.md "wikilink")\[1\]，连接于[目系](../Page/目系.md "wikilink")，上出于前额，与[督脉会合于巅顶](../Page/督脉.md "wikilink")。

目系支脉：下行颊里、环绕唇内。

肝部支脉：从肝分出，过膈，向上流注于肺，与[手太阴肺经相接](../Page/手太阴肺经.md "wikilink")。\[2\]\[3\]

## 主治概要

主治肝病、妇科、前阴病及经脉曲泉循行部位的其他病症。

## 腧穴

1.  [大敦](../Page/大敦穴.md "wikilink")，[井穴](../Page/井穴.md "wikilink")
2.  [行間](../Page/行間穴.md "wikilink")，[滎穴](../Page/滎穴.md "wikilink")
3.  [太衝](../Page/太衝穴.md "wikilink")，[輸穴](../Page/輸穴.md "wikilink")，[原穴](../Page/原穴.md "wikilink")
4.  [中封](../Page/中封穴.md "wikilink")，[經穴](../Page/經穴.md "wikilink")
5.  [蠡溝](../Page/蠡溝穴.md "wikilink")，[絡穴](../Page/絡穴.md "wikilink")
6.  [中都](../Page/中都穴.md "wikilink")，[郄穴](../Page/郄穴.md "wikilink")
7.  [膝關](../Page/膝關穴.md "wikilink")
8.  [曲泉](../Page/曲泉穴.md "wikilink")，[合穴](../Page/合穴.md "wikilink")
9.  [陰包](../Page/陰包穴.md "wikilink")
10. [足五里](../Page/足五里穴.md "wikilink")
11. [陰廉](../Page/陰廉穴.md "wikilink")
12. [急脈](../Page/急脈穴.md "wikilink")
13. [章門](../Page/章門穴.md "wikilink")
14. [期門](../Page/期門穴.md "wikilink")，肝[募穴](../Page/募穴.md "wikilink")

## 註解與参考文献

[Category:十二正經](../Category/十二正經.md "wikilink")

1.  上鼻[咽的頏顙](../Page/咽.md "wikilink")（）為鼻腔與口腔之通道。
2.
3.  《[靈樞](../Page/靈樞.md "wikilink")·經脈》：肝足厥陰之脈，起於大指從毛之際，上循足跗上廉，去內踝一寸，上踝八寸，交出太陰之後，上膕內廉，循股陰，入毛中，環陰器，抵小腹，夾胃，屬肝，絡胆，上貫膈，布胁肋，循喉嚨之後，上入頏顙，連目系，上出額，與目系下頰裡，環唇內。其支者，復從肝別貫膈，上注肺。