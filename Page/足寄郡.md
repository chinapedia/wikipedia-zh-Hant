**足寄郡**（）為[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[十勝綜合振興局的郡](../Page/十勝綜合振興局.md "wikilink")，位於十勝綜合振興局東北部。

## 轄區

轄區包含2町：

  - [足寄町](../Page/足寄町.md "wikilink")
  - [陸別町](../Page/陸別町.md "wikilink")

## 沿革

[Kasai-shicho.png](https://zh.wikipedia.org/wiki/File:Kasai-shicho.png "fig:Kasai-shicho.png")

  - 1869年8月15日：北海道設置11國86郡，[釧路國足寄郡成立](../Page/釧路國.md "wikilink")。
  - 1897年11月：北海道實施支廳制，釧路國足寄郡隸屬[釧路支廳之管轄](../Page/釧路支廳.md "wikilink")，此時足寄郡下轄足寄村、螺灣村、淕別村、利別村。\[1\]（4村）
  - 1923年4月1日：（2村）
      - 足寄村、螺灣村合併為足寄村，並成為北海道二級村。
      - 淕別村、利別村合併為淕別村，並成為北海道二級村。
  - 1943年6月1日：北海道一級二級町村制廢止，足寄村和淕別村改為內務省指定村。
  - 1946年10月5日：指定町村制廢止。
  - 1948年10月20日：由隸屬釧路國支廳（現[釧路支廳](../Page/釧路支廳.md "wikilink")）改隸屬[十勝支廳](../Page/十勝支廳.md "wikilink")。
  - 1949年8月1日：淕別村改制並改名為陸別町。（1町1村）
  - 1951年4月1日：[中川郡](../Page/中川郡_\(十勝支廳\).md "wikilink")[西足寄町的部份區域被併入陸別町](../Page/西足寄町.md "wikilink")。
  - 1955年4月1日：中川郡西足寄町與足寄村[合併為新設立的](../Page/市町村合併.md "wikilink")[足寄町](../Page/足寄町.md "wikilink")，並隸屬足寄郡之管轄。（2町）

## 參考資料

[Category:釧路國](../Category/釧路國.md "wikilink")
[Category:北海道的郡](../Category/北海道的郡.md "wikilink")

1.