**跨網站指令碼**（，通常簡稱為：XSS）是一種網站應用程式的安全漏洞攻擊，是[代码注入的一种](../Page/代码注入.md "wikilink")。它允許惡意使用者將程式碼注入到網頁上，其他使用者在觀看網頁時就會受到影響。這類攻擊通常包含了[HTML以及使用者端](../Page/HTML.md "wikilink")[腳本語言](../Page/腳本語言.md "wikilink")。

**XSS**攻击通常指的是通过利用网页开发时留下的漏洞，通过巧妙的方法注入恶意指令代码到网页，使用户加载并执行攻击者恶意制造的网页程序。这些恶意网页程序通常是[JavaScript](../Page/JavaScript.md "wikilink")，但实际上也可以包括[Java](../Page/Java.md "wikilink")，[VBScript](../Page/VBScript.md "wikilink")，[ActiveX](../Page/ActiveX.md "wikilink")，[Flash或者甚至是普通的](../Page/Flash.md "wikilink")[HTML](../Page/HTML.md "wikilink")。攻击成功后，攻击者可能得到更高的权限（如执行一些操作）、私密网页内容、[会话和](../Page/会话.md "wikilink")[cookie等各种内容](../Page/cookie.md "wikilink")。

## 背景和现状

當[網景](../Page/網景.md "wikilink")（Netscape）最初推出[JavaScript語言時](../Page/JavaScript.md "wikilink")，他們也察覺到准許[網頁伺服器傳送可執行的程式碼給一個](../Page/網頁伺服器.md "wikilink")[瀏覽器的安全風險](../Page/瀏覽器.md "wikilink")（即使僅是在一個瀏覽器的[沙盒裡](../Page/沙盒_\(計算機安全\).md "wikilink")）。它所造成的一個關鍵的問題在於使用者同時開啟多個瀏覽器[視窗時](../Page/視窗.md "wikilink")，在某些例子裡，[網頁裡的片斷程式碼被允許從另一個網頁或](../Page/網頁.md "wikilink")[物件取出資料](../Page/物件.md "wikilink")，而因為惡意的[網站可以用這個方法來嘗試竊取機密資訊](../Page/網站.md "wikilink")，所以在某些情形，這應是完全被禁止的。為了解決這個問題，瀏覽器採用了同源决策——僅允許來自相同網域名稱系統和使用相同協定的物件與網頁之間的任何互動。這樣一來，惡意的網站便無法藉由JavaScript在另一個瀏覽器竊取機密資料。此後，為了保護使用者免受惡意的危害，其他的瀏覽器與伺服端指令語言採用了類似的[存取控制決策](../Page/存取控制.md "wikilink")。

**XSS**漏洞可以追溯到1990年代。大量的网站曾遭受**XSS**漏洞攻击或被发现此类漏洞，如[Twitter](../Page/Twitter.md "wikilink")\[1\]，[Facebook](../Page/Facebook.md "wikilink")\[2\]，[MySpace](../Page/MySpace.md "wikilink")，[Orkut](../Page/Orkut.md "wikilink")\[3\]\[4\]
,[新浪微博](../Page/新浪微博.md "wikilink")\[5\]和[百度贴吧](../Page/百度贴吧.md "wikilink")
。研究表明\[6\]，最近几年**XSS**已经超过[缓冲区溢出成为最流行的攻击方式](../Page/缓冲区溢出.md "wikilink")，有68%的网站可能遭受此类攻击。根据开放网页应用安全计划（Open
Web Application Security
Project）公布的2010年统计数据，在Web安全威胁前10位中，XSS排名第2，仅次于[代码注入](../Page/代码注入.md "wikilink")（Injection）。\[7\]

## 縮寫

的英文首字母縮寫本應為**CSS**，但因為[CSS在](../Page/CSS_\(消歧义\).md "wikilink")[網頁設計領域已經被廣泛指](../Page/網頁設計.md "wikilink")[層疊樣式表](../Page/層疊樣式表.md "wikilink")（Cascading
Style Sheets），所以將Cross（意为“交叉”）改以交叉形的**X**做為縮寫。但早期的文件還是會使用CSS表示Cross-site
scripting。

## 检测方法

通常有一些方式可以測試網站是否有正確處理特殊字元：

  - <code>\>
    <script>
    alert(document.cookie)
    </script>
    </code>
  - <code>='\>
    <script>
    alert(document.cookie)
    </script>
    </code>
  - <code>"\>
    <script>
    alert(document.cookie)
    </script>
    </code>
  - <code>
    <script>
    alert(document.cookie)
    </script>
    </code>
  - <code>
    <script>
    alert (vulnerable)
    </script>
    </code>
  - `%3Cscript%3Ealert('XSS')%3C/script%3E`
  - <code>
    <script>
    alert('XSS')
    </script>
    </code>
  - <img src="javascript:alert('XSS')">
  - `<img src="http://888.888.com/999.png" onerror="alert('XSS')">`
  - `<div
    style="height:expression(alert('XSS'),1)"></div>`（這個僅於IE7(含)之前有效）

## 攻击手段和目的

攻击者使被攻击者在浏览器中执行脚本后，如果需要收集来自被攻击者的数据（如cookie或其他敏感信息），可以自行架设一个网站，让被攻击者通过JavaScript等方式把收集好的数据作为参数提交，随后以[数据库等形式记录在攻击者自己的服务器上](../Page/数据库.md "wikilink")。

常用的**XSS**攻击手段和目的有：

  - 盗用cookie，获取敏感信息。
  - 利用植入Flash，通过[crossdomain权限设置进一步获取更高权限](../Page/crossdomain.md "wikilink")；或者利用Java等得到类似的操作。
  - 利用iframe、frame、XMLHttpRequest或上述Flash等方式，以（被攻击）用户的身份执行一些管理动作，或执行一些一般的如发[微博](../Page/微博.md "wikilink")、加好友、发私信等操作。
  - 利用可被攻击的域受到其他域信任的特点，以受信任来源的身份请求一些平时不允许的操作，如进行不当的投票活动。
  - 在访问量极大的一些页面上的XSS可以攻击一些小型网站，实现[DDoS攻击的效果](../Page/DDoS.md "wikilink")。

## 漏洞的防御和利用

### 过滤特殊字符

避免XSS的方法之一主要是將使用者所提供的內容進行過濾，許多語言都有提供對[HTML的過濾](../Page/HTML.md "wikilink")：

  - [PHP的](../Page/PHP.md "wikilink")`htmlentities()或是htmlspecialchars()`。
  - [Python的](../Page/Python.md "wikilink")`cgi.escape()`。
  - [ASP的](../Page/ASP.md "wikilink")`Server.HTMLEncode()`。
  - [ASP.NET的](../Page/ASP.NET.md "wikilink")`Server.HtmlEncode()`或功能更強的[Microsoft
    Anti-Cross Site Scripting
    Library](http://msdn.microsoft.com/en-us/library/aa973813.aspx)
  - [Java的](../Page/Java.md "wikilink")[xssprotect (Open Source
    Library)](http://code.google.com/p/xssprotect/)。
  - [Node.js的node](../Page/Node.js.md "wikilink")-validator。

### 使用[HTTP头指定类型](../Page/HTTP头字段.md "wikilink")

很多时候可以使用[HTTP头指定内容的类型](../Page/HTTP头字段.md "wikilink")，使得输出的内容避免被作为HTML解析。如在[PHP语言中使用以下代码](../Page/PHP.md "wikilink")：

    <?php
       header('Content-Type: text/javascript; charset=utf-8');
    ?>

即可强行指定输出内容为文本/JavaScript脚本（顺便指定了内容编码），而非可以引发攻击的HTML。

### 使用者方面

包括[Internet Explorer](../Page/Internet_Explorer.md "wikilink")、[Mozilla
Firefox在內的大多数瀏覽器皆有關閉JavaScript的选项](../Page/Mozilla_Firefox.md "wikilink")，但關閉功能并非是最好的方法，因為許多網站都需要使用JavaScript語言才能正常运作。通常来说，一個經常有安全更新推出的瀏覽器，在使用上會比很久都没有更新的浏览器更为安全。

## 参阅

  - [苍月浏览器](../Page/苍月浏览器.md "wikilink")—过滤跨網站指令碼（XSS）的网络浏览器
  - [SGML实体](../Page/SGML实体.md "wikilink")
  - [Metasploit](../Page/Metasploit.md "wikilink")—一个包含XSS测试的开源渗透测试工具

## 外部連結

  - [CERT® Advisory CA-2000-02 Malicious HTML Tags Embedded in Client
    Web Requests](http://www.cert.org/advisories/CA-2000-02.html)
  - [Cross Site Scripting
    Info](http://httpd.apache.org/info/css-security/)
  - [The Same Origin
    Policy](http://www.mozilla.org/projects/security/components/same-origin.html)

  - [XSS (Cross Site Scripting) Cheat
    Sheet](https://web.archive.org/web/20120911044754/http://ha.ckers.org/xss.html)


### 脚注

<references />

[Category:网络安全](../Category/网络安全.md "wikilink")
[Category:注入漏洞](../Category/注入漏洞.md "wikilink")

1.
2.
3.
4.
5.
6.
7.