**路嘉欣**（**Jozie
Lu**，），[台灣](../Page/台灣.md "wikilink")[女歌手](../Page/女歌手.md "wikilink")、[女演員](../Page/女演員.md "wikilink")，暱稱「小路」。2002年以歌手身分出道，發行了首張專輯《你不懂》，同名歌曲「你不懂」以及「黑色天空」經典傳唱至今。戲劇演出橫跨大小銀幕及舞台－包含偶像劇、電影、舞台劇，近年常與[非常林奕華劇團合作](../Page/非常林奕華.md "wikilink")。
2017年演出電影[3904英呎](../Page/3904英呎.md "wikilink")，入圍第一屆塞班電影節[最佳女配角](../Page/最佳女配角.md "wikilink")。小路平時亦致力於提倡「以領養代替購買」相關流浪動物議題，並著有《帶你回家的小路》一書，由[時報出版發行](../Page/時報出版.md "wikilink")。

## 作品

### 專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《你不懂》</p>
<ul>
<li>唱片公司：</li>
<li>發行日期：2002年</li>
<li>語言：<a href="../Page/國語.md" title="wikilink">國語</a></li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 單曲

  - 2003年 《手牽手》（抗SARS之歌）
  - 2007年 《Fly》、《穿牆人》（收錄於《穿牆人》電影原聲帶）
  - 2009年
    《多虧有你》（與[陳亦飛合唱](../Page/陳亦飛.md "wikilink")，收錄於《[協奏曲](../Page/協奏曲_\(電視劇\).md "wikilink")》電影原聲帶）
  - 2009年
    《生日快樂OP.1》、《河岸漂流》、《生日快樂OP.5》（收錄於《[一席之地](../Page/一席之地.md "wikilink")》電影原聲帶）
  - 2011年 《第一首歌》（收錄於《[電哪吒](../Page/電哪吒.md "wikilink")》電影原聲帶）
  - 2012年 《你的歌》（收錄於歌手Easy a.k.a 沈簡單首張專輯專輯《預言》）
  - 2013年 《最後》（與朱家儀、賴盈螢、楊芷瓔合唱，舞台劇《三國》(What is Success?)巾幗無雙版主題曲）
  - 2014年 《你值得我慷慨》《[妹妹](../Page/妹妹.md "wikilink")》插曲

### EP

  - 2013年 《幸福 前進吧！》

### 音樂錄影帶

  - 2001年 《停》（收錄於專輯《[Orange](../Page/Orange_\(張震嶽專輯\).md "wikilink")》）
  - 2007年 《不讓你走》（[阿杜](../Page/阿杜.md "wikilink")《撒野》專輯）
  - 2015年 《以後要做的事》（[林俊傑](../Page/林俊傑.md "wikilink")《因你而在》專輯）
  - 2018年 《有火》（[謝霆鋒](../Page/謝霆鋒.md "wikilink")）

### 電影

| 年份    | 片名                                       | 導演                               | 飾演  | 備註           |
| ----- | ---------------------------------------- | -------------------------------- | --- | ------------ |
| 2018年 | 《[黯夜](../Page/黯夜.md "wikilink")》         | [游翰庭](../Page/游翰庭.md "wikilink") | 丁曉涵 | 緯來製作電視電影     |
| 2017年 | 《[3904英呎](../Page/3904英呎.md "wikilink")》 | [王重正](../Page/王重正.md "wikilink") | 沈慧蓉 | 入圍塞班電影節最佳女配角 |
| 2016年 | 《[我的蛋男情人](../Page/我的蛋男情人.md "wikilink")》 | [傅天余](../Page/傅天余.md "wikilink") | 客串  |              |
| 2012年 | 《距離感》                                    |                                  |     |              |
| 2009年 | 《[一席之地](../Page/一席之地.md "wikilink")》     | [樓一安](../Page/樓一安.md "wikilink") | 凱西  |              |
| 2008年 | 《[花吃了那女孩](../Page/花吃了那女孩.md "wikilink")》 | [陳宏一](../Page/陳宏一.md "wikilink") | 小沁  |              |
| 2007年 | 《穿牆人》                                    | [鴻鴻](../Page/鴻鴻.md "wikilink")   | 雅紅  |              |
|       |                                          |                                  |     |              |

### 電視劇

| 年份    | 頻道                                                                  | 劇名                                               | 飾演     |
| ----- | ------------------------------------------------------------------- | ------------------------------------------------ | ------ |
| 2006年 | [中視](../Page/中視.md "wikilink")／[東森戲劇台](../Page/東森戲劇台.md "wikilink") | 《[轉角＊遇到愛](../Page/轉角＊遇到愛.md "wikilink")》         | 蔡小陽    |
| 2006年 | [八大綜合台](../Page/八大綜合台.md "wikilink")                                | 《[東方茱麗葉](../Page/東方茱麗葉.md "wikilink")》           | 甘理沙    |
| 2006年 | [衛視中文台](../Page/衛視中文台.md "wikilink")                                | 《驚異傳奇之重複的人生》                                     | 心嵐     |
| 2008年 | [民視](../Page/民視.md "wikilink")／[衛視中文台](../Page/衛視中文台.md "wikilink") | 《[原來我不帥](../Page/原來我不帥.md "wikilink")》           | 沈嫚嫚    |
| 2009年 | [台視](../Page/台視.md "wikilink")                                      | 《[協奏曲](../Page/協奏曲_\(電視劇\).md "wikilink")》       | 毛家敏    |
| 2010年 | [中視](../Page/中視.md "wikilink")                                      | 《[女王不下班](../Page/女王不下班.md "wikilink")》           | 章若依、旁白 |
| 2010年 | [三立](../Page/三立.md "wikilink")                                      | 《[偷心大聖PS男](../Page/偷心大聖PS男.md "wikilink")》       | 艾咪     |
| 2011年 | [安徽衛視](../Page/安徽衛視.md "wikilink")/[中視](../Page/中視.md "wikilink")   | 《[幸福三顆星](../Page/幸福三顆星.md "wikilink")》           | 宋欣薪    |
| 2015年 | [中視](../Page/中視.md "wikilink")                                      | 《[失去你的那一天](../Page/失去你的那一天.md "wikilink")》       | 小惠     |
| 2016年 | [公視](../Page/公視.md "wikilink")                                      | 人生劇展《[當你微笑時](../Page/當你微笑時.md "wikilink")》       | 陸太太    |
| 2017年 | [公視](../Page/公視.md "wikilink")                                      | 《[你的孩子不是你的孩子](../Page/你的孩子不是你的孩子.md "wikilink")》 | 芝芝     |
|       |                                                                     |                                                  |        |

### 舞台劇

| 年份    | 劇團                                   | 劇名                                                    | 飾演     |
| ----- | ------------------------------------ | ----------------------------------------------------- | ------ |
| 2011年 | [非常林奕華](../Page/非常林奕華.md "wikilink") | 《[賈寶玉](../Page/賈寶玉_\(舞台劇\).md "wikilink")》(Awakening) | 林黛玉、旁白 |
| 2012年 | [非常林奕華](../Page/非常林奕華.md "wikilink") | 《三國》(What is Success?)                                |        |
| 2014年 | [非常林奕華](../Page/非常林奕華.md "wikilink") | 《梁祝的繼承者們》                                             |        |
| 2016年 | [非常林奕華](../Page/非常林奕華.md "wikilink") | 《福爾摩斯.心之偵探》                                           |        |
| 2017年 | [非常林奕華](../Page/非常林奕華.md "wikilink") | 《聊齋》                                                  |        |

### 文字作品

| 出版日期  | 書名                                     | 出版社                                | ISBN               |
| ----- | -------------------------------------- | ---------------------------------- | ------------------ |
| 2015年 | 《帶你回家的小路：路嘉欣尋訪獸醫師、中途之家等，最專業的「流浪貓領養筆記」》 | [時報出版](../Page/時報出版.md "wikilink") | ISBN 9789571363318 |
|       |                                        |                                    |                    |

### 品牌代言

  - 2012年 台灣網路平價服裝品牌《[Double](../Page/Double.md "wikilink")》年度代言人

## 參考資料

## 外部連結

  -
  -
  -
  -
  -
  -
  -
[K嘉](../Page/分類:路姓.md "wikilink")
[分類:台灣女歌手](../Page/分類:台灣女歌手.md "wikilink")
[分類:淡江大學校友](../Page/分類:淡江大學校友.md "wikilink")

[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Category:臺北市私立再興高級中學校友](../Category/臺北市私立再興高級中學校友.md "wikilink")
[Category:臺灣舞台劇演員](../Category/臺灣舞台劇演員.md "wikilink")