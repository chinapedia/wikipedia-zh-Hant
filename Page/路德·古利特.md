**路德·古利特**（**Ruud
Gullit**，），是一名已退役荷蘭[足球運動員及現任足球領隊](../Page/足球.md "wikilink")，是1987年[歐洲足球先生和](../Page/歐洲足球先生.md "wikilink")1987年、1989年[世界足球先生得主](../Page/世界足球先生.md "wikilink")。荷蘭足球傳奇巨星之一。

## 生平

古利特早年出身於荷蘭聯賽一些下游球會，1982年加盟[费耶诺德](../Page/飛燕諾.md "wikilink")，三年後轉投[PSV燕豪芬](../Page/PSV燕豪芬.md "wikilink")。當時古利特已是荷蘭國家隊成員，但荷蘭卻未打入世界杯決賽週。

1987年古利特加盟意大利球會[AC米蘭](../Page/AC米蘭.md "wikilink")，配合後來的[里杰卡尔德和](../Page/弗兰克·里杰卡尔德.md "wikilink")[范巴斯滕](../Page/馬爾科·范巴斯滕.md "wikilink")，組成了著名的[荷蘭三剑客](../Page/荷蘭三剑客.md "wikilink")。在三人坐鎮AC米蘭期間，AC米蘭贏得了三個意甲冠軍、兩個欧洲冠军杯冠軍。1988年「荷蘭三剑客」還一手将荷蘭國家隊带入[欧洲足球锦标赛決賽并贏得冠軍](../Page/欧洲足球锦标赛.md "wikilink")。早在1987年古列治便當選為[歐洲足球先生](../Page/歐洲足球先生.md "wikilink")，同年和1989年也當選為世界足球先生。

1995年7月古利特轉赴英超加盟[切尔西](../Page/切尔西足球俱乐部.md "wikilink")，其後還兼任領隊。期間他為切尔西贏得1997年[英格蘭足總杯](../Page/英格蘭足總杯.md "wikilink")。後來他與球會高層發生爭執，結果被辭退。1998年他轉教英超另一支球隊[纽卡斯尔联](../Page/纽卡斯尔联足球俱乐部.md "wikilink")，哪知執教首季成績差劣，僅能贏得聯賽第十一名。翌季纽卡斯尔联竟连续八场不胜，同時還與球隊多名主力发生爭執，結果他又一次被辭退。

2004年古烈治執掌[飛燕諾帥印](../Page/飛燕諾.md "wikilink")，但只執教一年便辭職，原因是沒有贏得獎項。

2007年成為美國[洛杉磯銀河隊的領隊](../Page/洛杉磯銀河.md "wikilink")，2008年8月11日，与洛杉矶银河尚有合同在身的古利特突然宣布因私人原因辞职，之后他一直赋闲在家。

2011年1月18日，他轉往俄羅斯的[格羅茲尼執教](../Page/格羅茲尼捷列克足球俱樂部.md "wikilink")，結果在2011年6月14日被辭退，被辭退前他只帶領球隊贏得三場賽事。

## 榮譽

  - 欧洲足球锦标赛冠军：1988年
  - 荷兰联赛冠軍：1984年、1986年,1987年
  - 荷兰杯赛冠军：1984年
  - 意大利联赛冠军：1988年、1992年,1993年
  - 意大利足协杯冠军：1988年、1994年
  - 欧洲冠军杯冠军：1989年,1990年
  - 欧洲超级杯冠军：1989年、1990年
  - 丰田杯冠军：1989年,1990年
  - [歐洲足球先生](../Page/歐洲足球先生.md "wikilink")：1987年
  - [「足球世界」世界足球先生](../Page/世界足球.md "wikilink")：1987年、1989年
  - [歐洲國家杯](../Page/歐洲國家杯.md "wikilink")：1988年(冠軍)
  - [FIFA 100成員](../Page/FIFA_100.md "wikilink")：2003

## 参考資料

[Category:荷兰足球运动员](../Category/荷兰足球运动员.md "wikilink")
[Category:蘇里南裔荷蘭人](../Category/蘇里南裔荷蘭人.md "wikilink")
[Category:费耶诺德球員](../Category/费耶诺德球員.md "wikilink")
[Category:PSV燕豪芬球員](../Category/PSV燕豪芬球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:森多利亞球員](../Category/森多利亞球員.md "wikilink")
[Category:切尔西球員](../Category/切尔西球員.md "wikilink")
[Category:荷蘭足球主教練](../Category/荷蘭足球主教練.md "wikilink")
[Category:切尔西領隊](../Category/切尔西領隊.md "wikilink")
[Category:紐卡素領隊](../Category/紐卡素領隊.md "wikilink")
[Category:英超領隊](../Category/英超領隊.md "wikilink")
[Category:飛燕諾主教練](../Category/飛燕諾主教練.md "wikilink")
[Category:洛杉磯銀河教練](../Category/洛杉磯銀河教練.md "wikilink")
[Category:美職教練](../Category/美職教練.md "wikilink")
[Category:歐洲足球先生](../Category/歐洲足球先生.md "wikilink")
[Category:FIFA 100](../Category/FIFA_100.md "wikilink")