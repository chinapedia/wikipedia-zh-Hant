**路德维希一世**（**Ludwig I**，1786年8月25日-1868年2月29日），全名*路德维希·卡尔·奥古斯特*（*LUDWIG
Karl
August*），[巴伐利亚](../Page/巴伐利亚.md "wikilink")[国王](../Page/国王.md "wikilink")（1825年-1848年3月20日）。[巴伐利亚第一任](../Page/巴伐利亚.md "wikilink")[国王](../Page/国王.md "wikilink")[马克西米利安一世的长子](../Page/马克西米利安一世_\(巴伐利亚国王\).md "wikilink")，母为[黑森-达姆施塔特公主奥古斯塔](../Page/黑森-达姆施塔特.md "wikilink")·威廉敏娜。1786年8月25日生于[斯特拉斯堡](../Page/斯特拉斯堡.md "wikilink")。路德维希这个名字来源于他的[教父](../Page/教父.md "wikilink")[法兰西国王](../Page/法国君主列表.md "wikilink")[路易十六](../Page/路易十六.md "wikilink")。

1810年10月12日，当时还是[王储的他与](../Page/王储.md "wikilink")[萨克森-希尔德伯格豪森公主特雷莎结婚](../Page/萨克森-希尔德伯格豪森.md "wikilink")，为这场婚姻在[慕尼黑举行的庆祝活动演变成了一年一度的](../Page/慕尼黑.md "wikilink")[慕尼黑啤酒节](../Page/慕尼黑啤酒节.md "wikilink")（Oktoberfest）。1825年，路德维希的父亲[马克西米利安一世去世](../Page/马克西米利安一世_\(巴伐利亚国王\).md "wikilink")，他继承了他父亲的[巴伐利亚王位](../Page/巴伐利亚统治者列表.md "wikilink")。为了加快新增加的国土与原巴伐利亚国土的一体化，他在[巴伐利亚国王的头衔前又附加了](../Page/巴伐利亚.md "wikilink")**法兰克尼亚和施瓦本公爵**的称号。

路德维希一世是一位古典艺术的爱好者。在他的王储时代，就开始收购艺术品，尤其是古[希腊](../Page/希腊.md "wikilink")、古[罗马的艺术珍宝](../Page/罗马.md "wikilink")，并与当时许多艺术大家保持联系。婚后不久，他倡议兴建了格里陶德博物馆（Glyptothek）用于展览他收藏的古[希腊](../Page/希腊.md "wikilink")、古[罗马](../Page/罗马.md "wikilink")[雕塑](../Page/雕塑.md "wikilink")。登基后他将[慕尼黑建设成一个](../Page/慕尼黑.md "wikilink")[艺术的城市](../Page/艺术.md "wikilink")：他将当时著名的艺术家和建筑家如利奥·冯·克棱茨（Leo
von Klenze，格里陶德博物馆的设计者）、[彼得·冯·柯内留斯](../Page/柯内留斯.md "wikilink")（Peter
von
Cornelius）等招揽至[慕尼黑](../Page/慕尼黑.md "wikilink")；修建了路德维希大道和慕尼黑王宫；1853年又兴建了新美术馆（Neue
Pinakothek）；并于1826年将[巴伐利亚国立大学从](../Page/巴伐利亚.md "wikilink")[兰茨胡特迁到了](../Page/兰茨胡特.md "wikilink")[慕尼黑](../Page/慕尼黑.md "wikilink")。现今[慕尼黑的许多建筑都是路德维希一世时代的产物](../Page/慕尼黑.md "wikilink")。

路德维希一世也鼓励[工业的发展](../Page/工业.md "wikilink")。他下令建造了[德国的第一条](../Page/德国.md "wikilink")[铁路](../Page/鐵路運輸.md "wikilink")（1835年，从[纽伦堡至](../Page/纽伦堡.md "wikilink")[菲尔特](../Page/菲尔特.md "wikilink")），取名“巴伐利亚路德维希铁路”（Bayerische
Ludwigsbahn）；随后建立了第二条，从路德维希港到贝克斯巴赫的“普法尔茨路德维希铁路”（Pfälzische
Ludwigsbahn）。他还发起了连接[美因河和](../Page/美因河.md "wikilink")[多瑙河的路德维希运河的修建](../Page/多瑙河.md "wikilink")，从而将[北海和](../Page/北海_\(大西洋\).md "wikilink")[黑海连接了起来](../Page/黑海.md "wikilink")。

路德维希一世执政之初，他的政治倾向较为宽松。路德维希一世支持[希腊的独立战争](../Page/希腊.md "wikilink")，1832年，他的次子[奥托被选为](../Page/奥托一世_\(希腊\).md "wikilink")[希腊国王](../Page/希腊君主列表.md "wikilink")。1830年[法国](../Page/法国.md "wikilink")[七月革命以及](../Page/七月革命.md "wikilink")[欧洲](../Page/欧洲.md "wikilink")[革命运动日益发展后](../Page/革命.md "wikilink")，他的政治倾向日益反动。[普法尔茨人民与](../Page/普法尔茨.md "wikilink")[巴伐利亚当局的矛盾日益加深](../Page/巴伐利亚.md "wikilink")，于1832年在普法尔茨爆发了汉巴赫节起义。路德维希一世加强了书刊、信件的检查机制，这遭到了人民的普遍反对。与情妇罗拉·蒙特斯的绯闻加深了人民的反抗情绪。最终路德维希一世在[1848年革命中被迫退位](../Page/1848年革命.md "wikilink")，让位给长子[马克西米利安二世](../Page/马克西米利安二世_\(巴伐利亚国王\).md "wikilink")。

路德维希一世于1868年2月29日在[法国](../Page/法国.md "wikilink")[尼斯去世](../Page/尼斯.md "wikilink")，终年81岁。

路德维希一世与王后特雷莎有四子五女：

  - 长子[马克西米利安·约瑟夫](../Page/马克西米利安二世_\(巴伐利亚国王\).md "wikilink")
    （*Maximilian
    Joseph*，1811年11月28日-1864年3月10日），1848年-1864年任[巴伐利亚国王](../Page/巴伐利亚统治者列表.md "wikilink")
  - 长女玛蒂尔德·卡罗林·腓德烈卡·威廉明娜·夏洛特 （*Mathilde Caroline Friederike Wilhelmine
    Charlotte*，1813年8月30日-1862年5月25日），
    1833年与[黑森](../Page/黑森.md "wikilink")[大公](../Page/黑森统治者列表.md "wikilink")[路德维希三世结婚](../Page/路德维希三世_\(黑森大公\).md "wikilink")。
  - 次子[奥托·腓德烈·路德维希](../Page/奥托一世_\(希腊\).md "wikilink")（*Otto Friedrich
    Ludwig*，1815年1月1日-1867年7月26日），
    1832年-1862年任[希腊国王](../Page/希腊.md "wikilink")。
  - 次女泰奥德林·夏洛特·路易丝 （*Theodelinde Charlotte Luise*，1816年-1817年）
  - 三子[柳特波德·卡尔·约瑟夫·威廉·路德维希](../Page/柳特波德_\(巴伐利亚\).md "wikilink")（*Luitpold
    Karl Joseph Wilhelm Ludwig*，1821年3月12日-1912年12月12日），
    1886年起任[巴伐利亚王国摄政](../Page/巴伐利亚.md "wikilink")。
  - 三女阿德尔贡德·奥古斯塔·夏洛特·伊莉莎白·阿玛利亚·玛利·苏菲·路易丝（*Adelgunde Auguste Charlotte
    Caroline Elisabeth Amalie Marie Sophie
    Luise*1823年3月19日-1914年10月28日），
    1842年与[摩德纳公爵](../Page/摩德纳.md "wikilink")[弗朗切斯科五世结婚](../Page/弗朗切斯科五世_\(摩德纳\).md "wikilink")。
  - 四女希尔德嘉德·路易丝·夏洛特·特雷莎·腓德烈卡（*Hildegard Luise Charlotte Theresia
    Friederike*，1825年6月10日-1864年4月2日），
    [奥地利阿尔布莱西特大公之妻](../Page/奥地利.md "wikilink")。
  - 五女亚历山德拉·阿玛利亚（*Alexandra Amalie*，1826年-1875年）
  - 四子阿达尔伯特·威廉·格奥尔格·路德维希（*Adalbert Wilhelm Georg
    Ludwig*，1828年-1875年），1856年娶[西班牙公主阿玛利亚](../Page/西班牙.md "wikilink")。

[Category:巴伐利亚国王](../Category/巴伐利亚国王.md "wikilink")
[Category:维特尔斯巴赫王朝](../Category/维特尔斯巴赫王朝.md "wikilink")
[L](../Category/1786年出生.md "wikilink")
[L](../Category/1868年逝世.md "wikilink")