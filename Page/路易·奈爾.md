{{ infobox scientist | box_width = 220px | name = 路易·奈尔  | image =
Louis Neel 1970.jpg | image_size = | caption = | birth_date =
1904年11月22日 | birth_place =
[法國](../Page/法國.md "wikilink")[里昂](../Page/里昂.md "wikilink")
| death_date = 2000年11月17日 | death_place =
法國[布里夫拉蓋亞爾德](../Page/布里夫拉蓋亞爾德.md "wikilink")
| residence = 法国[巴黎](../Page/巴黎.md "wikilink") | nationality =  | fields
= [物理](../Page/物理.md "wikilink") | workplaces = | alma_mater =
[巴黎高等师范学院](../Page/巴黎高等师范学院.md "wikilink") |
doctoral_advisor = | doctoral_students = | known_for = | awards =
[Nobel_prize_medal.svg](https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg "fig:Nobel_prize_medal.svg")
[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink") (1970年) }}

**路易·奈尔**（[法语](../Page/法语.md "wikilink")：，），全名，[法国物理学家](../Page/法国.md "wikilink")，1970年获[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

## 參閱

  - [奈尔溫度](../Page/奈尔溫度.md "wikilink")
  - [反鐵磁性](../Page/反鐵磁性.md "wikilink")
  - [亞鐵磁性](../Page/亞鐵磁性.md "wikilink")

## 参考资料

  - 诺贝尔官方网站关于路易·奈尔的[简介](http://nobelprize.org/nobel_prizes/physics/laureates/1970/neel-bio.html)

[Category:法国物理学家](../Category/法国物理学家.md "wikilink")
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:巴黎高等師範學院校友](../Category/巴黎高等師範學院校友.md "wikilink")