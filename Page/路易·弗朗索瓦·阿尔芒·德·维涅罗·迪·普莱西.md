[The_Maréchal-Duke_of_Richelieu_after_Jean_Marc_Nattier_(The_Wallace_Collection).jpg](https://zh.wikipedia.org/wiki/File:The_Maréchal-Duke_of_Richelieu_after_Jean_Marc_Nattier_\(The_Wallace_Collection\).jpg "fig:The_Maréchal-Duke_of_Richelieu_after_Jean_Marc_Nattier_(The_Wallace_Collection).jpg")
**路易·弗朗索瓦·阿尔芒·迪普莱西，黎塞留公爵**(**Louis François Armand du Plessis, duc de
Richelieu**)
()[法国元帅](../Page/法国元帅.md "wikilink")，[枢机主教](../Page/枢机主教.md "wikilink")[黎塞留的侄孙](../Page/黎塞留.md "wikilink")。

法国[路易十四是他的](../Page/路易十四.md "wikilink")[教父](../Page/教父.md "wikilink")。在他早期曾三次被关进[巴士底狱](../Page/巴士底狱.md "wikilink")：1711年因为他的继父，1716年在一次决斗后，和1719年他参加[西班牙枢机主教](../Page/西班牙.md "wikilink")[朱利奥·阿尔韦罗尼](../Page/朱利奥·阿尔韦罗尼.md "wikilink")(Giulio
Alberoni)阴谋反对[奥尔良公爵腓力二世为](../Page/奥尔良公爵.md "wikilink")[路易十五摄政王](../Page/路易十五.md "wikilink")。

他作为外交官和将军，在任驻[维也纳大使时期](../Page/维也纳.md "wikilink")(1725～1729)，他于1727年确定了媾和条约。

他是[法蘭西學院歷史上在任最久的院士](../Page/法蘭西學院_\(機構\).md "wikilink")（68年）。

## 军事事业

1733-1734年他参加了在[莱茵河戰役](../Page/莱茵河.md "wikilink")，分别在[德廷根](../Page/德廷根.md "wikilink")(Dettingen)和[丰特努瓦](../Page/丰特努瓦.md "wikilink")(Fontenoy)战斗，那里他指挥用[葡萄弹攻击英军整列列队](../Page/葡萄弹.md "wikilink")，并且三年之后他做了精采的[热那亚防御](../Page/热那亚.md "wikilink")，1756年他从[米诺卡岛Minorca逐出了英军](../Page/米诺卡岛.md "wikilink")，夺取了[圣费利佩San](../Page/圣费利佩.md "wikilink")
Felipe的要塞。1757-1758年他从那些掠夺的战斗中结束了他的军事生涯，在[汉诺威他获得了petit](../Page/汉诺威.md "wikilink")
père de la maraude的外号。

## 政治事业和阴谋

他真正的政治生涯开始于十年前他在莱茵的戰役，在战争以后他再次陷入了法院阴谋。最初他是[路易十五国王最好的朋友](../Page/路易十五.md "wikilink")，與國王自幼相識。但是当他反对路易的情妇[蓬帕杜尔侯爵夫人时](../Page/蓬帕杜尔侯爵夫人.md "wikilink")，关系以后冷却了一点。

1764年在蓬帕杜尔死了以后，法庭恢复了他的职位，他与国王最后的情妇[杜巴利伯爵夫人有著親密的友谊](../Page/杜巴利伯爵夫人.md "wikilink")。1774年[路易十五的孙子](../Page/路易十五.md "wikilink")[路易十六继承王位后](../Page/路易十六.md "wikilink")，他再次失势。这归结于实际上的新王后[玛丽·安托瓦内特烦恶杜巴利伯爵夫人和黎塞留的侄孙](../Page/玛丽·安托瓦内特.md "wikilink")，过度雄心勃勃的[艾吉永公爵](../Page/埃曼纽尔·阿尔芒·德·维内罗·杜·普莱西·德·黎塞留_\(艾吉永公爵\).md "wikilink")。

## 婚姻

他结婚三次：第一次在十四岁违背他的意志和[安妮·凯瑟琳·德·诺阿耶](../Page/安妮·凯瑟琳·德·诺阿耶.md "wikilink")(Anne
Catherine de
Noailles)，第二次，在1734年由[伏尔泰的阴谋](../Page/伏尔泰.md "wikilink")，和玛丽·德·吉塞(Marie
Elisabeth Sophie de
Guise)，第三次当他八十四岁时和[爱尔兰夫人](../Page/爱尔兰.md "wikilink")。

[勃利夫人也是他的好友](../Page/勃利夫人.md "wikilink")。在1729年他和[沙特萊侯爵夫人开始戀愛](../Page/沙特萊侯爵夫人.md "wikilink")，虽然后来结束了，他们在十年继续频繁通讯。他并且是著名[高等妓女和小说家](../Page/高等妓女.md "wikilink")[克劳汀·盖因·德·唐森](../Page/克劳汀·盖因·德·唐森.md "wikilink")(Claudine
Guérin de Tencin)的恋人。

[R](../Category/法国元帅.md "wikilink") [R](../Category/法国公爵.md "wikilink")
[R](../Category/法蘭西學院院士.md "wikilink")