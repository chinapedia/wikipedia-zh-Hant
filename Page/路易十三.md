[0_Louis_XIII_en_costume_de_deuil_-_Frans_Pourbus_le_Jeune_(2).JPG](https://zh.wikipedia.org/wiki/File:0_Louis_XIII_en_costume_de_deuil_-_Frans_Pourbus_le_Jeune_\(2\).JPG "fig:0_Louis_XIII_en_costume_de_deuil_-_Frans_Pourbus_le_Jeune_(2).JPG")

**路易十三**（**Louis
XIII**，）是[法国](../Page/法国.md "wikilink")[波旁王朝国王](../Page/波旁王朝.md "wikilink")（1610年－1643年在位）。是[亨利四世的长子](../Page/亨利四世_\(法兰西\).md "wikilink")，生于[枫丹白露](../Page/枫丹白露.md "wikilink")。幼年由其母[玛丽·德·美第奇攝政](../Page/玛丽·德·美第奇.md "wikilink")。1615年与同是孩子的[西班牙公主](../Page/西班牙.md "wikilink")[奥地利的安妮结婚](../Page/奥地利的安妮.md "wikilink")。路易十三親自執政後先任命[呂伊內公爵為首席大臣](../Page/呂伊內公爵.md "wikilink")，之後（1624年）長期依赖[红衣主教](../Page/红衣主教.md "wikilink")[黎須留的帮助](../Page/黎須留.md "wikilink")，開始了[法國的專制統治](../Page/法國.md "wikilink")（1627年[拉羅歇爾之圍的勝利是轉捩點](../Page/拉羅歇爾之圍.md "wikilink")）。在其统治期间，欧洲爆发了一场决定性的争霸战争——[三十年战争](../Page/三十年战争.md "wikilink")（1618年\~1648年），即欧洲最强大的两个王朝——法国[波旁王朝与](../Page/波旁王朝.md "wikilink")[伊比利和](../Page/伊比利.md "wikilink")[德意志的](../Page/德意志.md "wikilink")[哈布斯堡王朝](../Page/哈布斯堡王朝.md "wikilink")——之间的最后较量。最终法国取得胜利，结束了长达三个世纪的[哈布斯堡王朝霸权](../Page/哈布斯堡王朝.md "wikilink")，成为新的欧洲霸主。1643年5月14日因骑马落水引起的[肺炎而去世](../Page/肺炎.md "wikilink")。

## 路易十三的家庭成员

妻子：1615年11月24日同[奥地利的安妮](../Page/奥地利的安妮.md "wikilink")（1601年9月22日－1666年1月20日）结婚。

子女：

  - [路易十四](../Page/路易十四.md "wikilink")（1638年9月5日－1715年9月1日）
  - [菲利普一世](../Page/菲利普一世_\(奥尔良公爵\).md "wikilink")（奥尔良公爵，1640年9月21日－1701年6月9日）

[L](../Category/法国君主.md "wikilink") [L](../Category/纳瓦拉君主.md "wikilink")
[L](../Category/巴塞罗那伯爵.md "wikilink")
[L](../Category/法国波旁王朝.md "wikilink")
[L](../Category/法国王太子.md "wikilink")
[L](../Category/罹患肺结核逝世者.md "wikilink")