**（海外归来者）路易四世**（****，）是[西法兰克王国](../Page/西法兰克王国.md "wikilink")[加洛林王朝的](../Page/加洛林王朝.md "wikilink")[国王](../Page/国王.md "wikilink")（936年—954年在位），父亲是西法兰克国王（常等同于[法国国王](../Page/法国国王.md "wikilink")）[夏尔三世](../Page/查理三世_\(西法兰克\).md "wikilink")，母亲是[英格兰国王](../Page/英格兰.md "wikilink")[长者爱德华之女](../Page/长者爱德华.md "wikilink")[威塞克斯的艾吉芙](../Page/威塞克斯的艾吉芙.md "wikilink")。

## 生平

### 早年与流亡

路易四世生于920年9月10日，当他只有两岁大时（922年），他的父王[夏尔三世被](../Page/查理三世_\(法兰西\).md "wikilink")[贵族们废黜](../Page/贵族.md "wikilink")，王位由[罗贝尔一世继承](../Page/罗贝尔一世_\(西法兰克\).md "wikilink")。当他三岁大时（923年），罗贝尔一世在与废王夏尔三世的作战中丧生，王位归于罗贝尔一世的女婿、[勃艮第公爵](../Page/勃艮第公爵.md "wikilink")[拉乌尔](../Page/拉乌尔一世_\(西法兰克\).md "wikilink")。同为[加洛林王朝王族成员的](../Page/加洛林王朝.md "wikilink")[韦芒杜瓦伯爵](../Page/韦芒杜瓦伯爵.md "wikilink")[赫伯特二世却背叛了本家族](../Page/赫伯特二世_\(韦芒杜瓦\).md "wikilink")，与身为[博索尼德王朝成员的拉乌尔结盟](../Page/博索尼德王朝.md "wikilink")，俘虏了夏尔三世。小路易的母亲不得不带着他渡过[拉芒什海峡](../Page/拉芒什海峡.md "wikilink")（[英吉利海峡](../Page/英吉利海峡.md "wikilink")），逃到了娘家[英格兰王国以保平安](../Page/英格兰王国.md "wikilink")。后来他获得“海外归来者”的[绰号也正因为他是从海外](../Page/绰号.md "wikilink")（指英格兰）归国的。

### 归国与即位

929年，夏尔三世在监禁中死去，但是拉乌尔的统治一直持续到936年[驾崩](../Page/驾崩.md "wikilink")。拉乌尔驾崩后，贵族们一致同意召回在英格兰流亡的先王夏尔三世之子小路易。尤其是重臣[巴黎伯爵](../Page/巴黎伯爵.md "wikilink")[伟大的于格](../Page/伟大的于格.md "wikilink")，他同意召回小路易可能是为了防止赫伯特二世或者拉乌尔的弟弟、勃艮第公爵[黑于格继承王位](../Page/黑于格.md "wikilink")。

936年6月19日，16岁的路易在出生地[拉昂由](../Page/拉昂.md "wikilink")[兰斯大主教](../Page/兰斯大主教.md "wikilink")[阿尔托](../Page/阿尔托_\(兰斯大主教\).md "wikilink")[加冕为](../Page/加冕.md "wikilink")[西法兰克王国的](../Page/西法兰克王国.md "wikilink")[国王](../Page/国王.md "wikilink")，是为路易四世，正式[称号为](../Page/称号.md "wikilink")“[法兰克人的国王](../Page/法兰克国王列表.md "wikilink")”\[1\]。[加冕礼是在拉昂的](../Page/加冕礼.md "wikilink")[圣文森特修道院举行的](../Page/圣文森特修道院.md "wikilink")，拉昂也是西法兰克国王统治的中心地区。

### 统治与驾崩

尽管路易四世的有效统治仅仅局限于拉昂和西法兰克北部的一些地方，他在[卢瓦尔河以南的地区没有任何权威可言](../Page/卢瓦尔河.md "wikilink")，但他所表现出来的政治敏锐感甚至获得了与他不和的贵族们的赞赏。

但是，路易四世的治世也充满了与贵族的斗争，尤其是与重臣、法兰克[公爵兼](../Page/公爵.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[伯爵伟大的于格](../Page/伯爵.md "wikilink")。于格是西法兰克王国和[勃艮第真正的统治者](../Page/勃艮第.md "wikilink")，虽然他是把路易四世迎回国的第一大功臣，但同年（936年）他又迎娶了[东法兰克国王国王](../Page/德国君主列表.md "wikilink")[捕鸟者亨利的女儿](../Page/亨利一世_\(德意志\).md "wikilink")，便与路易四世反目成仇，因为路易四世和东法兰克王国是敌对的。终路易四世一生，他都在和于格斗争，不过由于实力相差悬殊，于格始终没有被打倒，其后代（即[卡佩家族](../Page/卡佩家族.md "wikilink")）终于取[加洛林王朝而代之](../Page/加洛林王朝.md "wikilink")。

在939年，路易四世与[罗马人民的国王兼东法兰克国王](../Page/神圣罗马帝国皇帝.md "wikilink")[奥托一世由于](../Page/奥托一世.md "wikilink")[洛林的归属问题发生了冲突](../Page/洛林.md "wikilink")，但不久，两国媾和，路易四世迎娶奥托的妹妹[萨克森的格波加为](../Page/萨克森的格波加.md "wikilink")[王后](../Page/王后.md "wikilink")，放弃洛林。路易和格波加生了七个孩子，其中两个儿子和一个女儿长大成人。

954年9月10日，路易四世正在[兰斯骑马](../Page/兰斯.md "wikilink")，不慎从[马上摔下](../Page/马.md "wikilink")，当天[驾崩](../Page/驾崩.md "wikilink")，享年34岁，葬于兰斯的[圣雷米教堂](../Page/圣雷米教堂.md "wikilink")、[圣雷米的](../Page/圣雷米.md "wikilink")[坟墓旁](../Page/坟墓.md "wikilink")。

## 祖先、婚姻与家庭

**路易四世**娶[萨克森的格波加](../Page/萨克森的格波加.md "wikilink")，生了7个孩子，其中2子1女活到了[成年](../Page/成年.md "wikilink")：

1.  长子[洛泰尔一世](../Page/洛泰尔一世_\(西法兰克\).md "wikilink")（941年—986年），[西法兰克国王](../Page/法国君主列表.md "wikilink")（954年—986年在位）
2.  长女[勃艮第的玛蒂尔德](../Page/勃艮第的玛蒂尔德.md "wikilink")（943年—992年）
3.  次子[查理或](../Page/查理.md "wikilink")[卡洛曼](../Page/卡洛曼.md "wikilink")（945年生，953年之前死）
4.  幼女（947年，未取名），[夭折](../Page/夭折.md "wikilink")
5.  第三子[路易](../Page/路易.md "wikilink")（948年─954年）
6.  第四子[下洛林的查理](../Page/下洛林的查理.md "wikilink")（953年—991年），[下洛林公爵](../Page/下洛林公爵.md "wikilink")
7.  幼子[亨利](../Page/亨利.md "wikilink")（953年），下洛林的查理的[孪生弟弟](../Page/孪生.md "wikilink")

## 参见

  - [法兰克人](../Page/法兰克人.md "wikilink")
  - [法兰克国王列表](../Page/法兰克国王列表.md "wikilink")
  - 其他名为[路易四世的君主](../Page/路易四世.md "wikilink")

## 注释与参考资料

<div class="references-small">

<references/>

</div>

  -
  - 王忠和《法国王室》，[百花文艺出版社](../Page/百花文艺出版社.md "wikilink")2007年出版 ISBN
    9787530645567

  - 让-夏尔·沃克曼《法国国王世系一览》（法文：） ISBN 2-877472086

  - 米歇尔·穆勒《小穆勒的历史辞典》（法文：） ISBN 203519265

[L](../Category/法国君主.md "wikilink") [L](../Category/天主教君主.md "wikilink")
[Category:卡洛林王朝](../Category/卡洛林王朝.md "wikilink")
[Category:欧洲意外身亡者](../Category/欧洲意外身亡者.md "wikilink")

1.  [拉丁文为](../Page/拉丁文.md "wikilink")****，实际仅是[西法兰克的国王](../Page/西法兰克.md "wikilink")。