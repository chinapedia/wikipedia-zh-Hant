[Brandeisl.jpg](https://zh.wikipedia.org/wiki/File:Brandeisl.jpg "fig:Brandeisl.jpg")
**路易士·布蘭戴斯**（，）[美國](../Page/美國.md "wikilink")[律師](../Page/律師.md "wikilink")，1916年獲[伍德羅·威爾遜](../Page/伍德羅·威爾遜.md "wikilink")[總統提名為](../Page/美國總統.md "wikilink")[美國最高法院](../Page/美國最高法院.md "wikilink")[大法官](../Page/美國最高法院大法官.md "wikilink")，直到1939年。是第一位擔任此職的猶太裔人士。最為人知的是他將實驗室方法帶入法庭。

1939年退休，[威廉·道格拉斯接任他的席位](../Page/威廉·道格拉斯.md "wikilink")。

## 早年生涯

### 家庭生活

### 大學時期

## 早期的職業生涯

### 律師事務所

### 隱私法

## 個人生活和婚姻

## 进步主义者

## 公共倡议者

### 針對壟斷

### 針對大企業

### 針對大眾消費

## 成為“人民律師”

### 開發新的生活保險制度

### 防止J. P.摩根鐵路的壟斷

### Upholding workplace laws with the "Brandeis Brief"

### Brandeis Brief

## 支持威尔逊总统

## 被提名為[大法官](../Page/美國最高法院大法官.md "wikilink")

## 主要案件

### Gilbert v. Minnesota (1920) - Freedom of speech

### Whitney v. California (1927) - Freedom of speech

### Olmstead v. United States (1928) - Right of privacy

### Packer Corporation v. Utah (1932) - Captive audience and free speech

## [新政時案件](../Page/羅斯福新政.md "wikilink")

### Louisville v. Radford (1935) - limiting presidential discretion

### Schechter Brothers v. The United States (1935) - NIRA is unconstitutional

### Erie Railroad Co. v. Tompkins (1938) - Federal versus state laws

## [錫安主義首領](../Page/錫安主義.md "wikilink")

## 死亡和遺產

## 同名機構

  - [布蘭迪斯大學](../Page/布蘭迪斯大學.md "wikilink")，位於美國[麻薩諸塞州沃爾瑟姆市的私立大學](../Page/麻薩諸塞州.md "wikilink")
  - [路易斯維爾大學的](../Page/路易斯維爾大學.md "wikilink")
  - 另有多所學校、民權組職、猶太宗教組織(部分位於[以色列](../Page/以色列.md "wikilink"))等

## 選擇意見

## 參見

## 參考文獻

## 外部連結

[B布](../Page/category:美國律師.md "wikilink")

[B布](../Category/美国最高法院大法官.md "wikilink")
[B](../Category/美國猶太人.md "wikilink")
[B](../Category/哈佛大學校友.md "wikilink")