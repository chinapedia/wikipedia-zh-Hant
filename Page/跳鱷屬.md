**跳鱷屬**（屬名：*Saltoposuchus*）意為「跳躍的鱷魚」，是種小型、長尾巴的[爬行動物](../Page/爬行動物.md "wikilink")，屬於[鱷形超目](../Page/鱷形超目.md "wikilink")[喙頭鱷亞目](../Page/喙頭鱷亞目.md "wikilink")，跳鱷身長約1到1.5公尺，體重估計約10到15公斤，生存於上[三疊紀](../Page/三疊紀.md "wikilink")[諾利階的](../Page/諾利階.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")\[1\]。跳鱷的前肢短，而後肢長。跳鱷很明顯的是敏捷的二足奔跑者，但也可以四足方式行走。跳鱷是[肉食性動物](../Page/肉食性.md "wikilink")，擁有尖狀牙齒，背部有兩排骨板。

Clark等人在2000年提出[陸鱷與跳鱷代表同一個屬的不同成長階段](../Page/陸鱷.md "wikilink")\[2\]。在這種使用狀況下，跳鱷對陸鱷具有優先權，但這在古生物學領域中並沒有得到討論。

跳鱷並不是著名的動物，但牠們常在大眾書籍中被提到與恐龍祖先有接近關係，不然就是被描述成恐龍祖先。跳鱷外外表上類似小型[獸腳亞目恐龍](../Page/獸腳亞目.md "wikilink")，而其中一個跳鱷的頭顱骨曾被歸類於[獸腳亞目的](../Page/獸腳亞目.md "wikilink")[原美頜龍](../Page/原美頜龍.md "wikilink")。目前的科學文獻並不認為跳鱷是恐龍的祖先。而目前已知最早的恐龍，例如[始盜龍](../Page/始盜龍.md "wikilink")，生存年代早於跳鱷至少1500萬年。跳鱷過去曾類於[槽齒目](../Page/槽齒目.md "wikilink")，但這個目已經是個無效的分類。在某些大眾書籍當中，跳鱷仍被錯誤地分類於恐龍。

## 參考資料

<div class="references-small">

<references>

</references>

  - [Dino Russ](http://www.dinosaur.com/de_4/5a6eed.htm)
  - [Palaeos](https://web.archive.org/web/20070311011626/http://www.palaeos.com/Mesozoic/Triassic/Norian.4.htm)
  - [Mikko's Phylogeny
    Archive](https://web.archive.org/web/20070206134116/http://www.fmnh.helsinki.fi/users/haaramo/Metazoa/Deuterostoma/Chordata/Archosauria/Crocodylia/Sphenosuchia_1.htm)

</div>

[Category:三疊紀鱷形類](../Category/三疊紀鱷形類.md "wikilink")
[Category:喙頭鱷亞目](../Category/喙頭鱷亞目.md "wikilink")

1.  <https://sites.google.com/site/paleofilescom/saltoposuchus>
2.