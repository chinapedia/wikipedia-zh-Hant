**踝節目**（Condylarthra）是已[滅絕的有](../Page/滅絕.md "wikilink")[胎盤类](../Page/胎盤.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")，主要生存於[古新世及](../Page/古新世.md "wikilink")[始新世](../Page/始新世.md "wikilink")。\[1\]踝節目是古新世最有特色哺乳動物，並顯示了古新世哺乳動物群的[演化程度](../Page/演化.md "wikilink")。

與食蟲类动物的祖先相比，踝節目成員顯示了演化成[雜食性或](../Page/雜食性.md "wikilink")[草食性的最初跡象](../Page/草食性.md "wikilink")。大型的陸上草食性動物自從[恐龍的滅絕後就消失了](../Page/恐龍.md "wikilink")，食性的轉移引發了踝節目的[演化輻射](../Page/演化輻射.md "wikilink")，在[澳洲以外的陸地上](../Page/澳洲.md "wikilink")，生成了[新生代不同類別的草食性](../Page/新生代.md "wikilink")[有蹄動物](../Page/有蹄動物.md "wikilink")。故此最原始的踝節目其實是某些现代有蹄類的共同祖先。雖然有些踝節目有細小的蹄，但最原始的形態是有爪的。

最近的[分子及](../Page/分子.md "wikilink")[DNA研究重整了哺乳動物的演化情況](../Page/DNA.md "wikilink")。[近蹄類及](../Page/近蹄類.md "wikilink")[管齒目不再是](../Page/管齒目.md "wikilink")[奇蹄目](../Page/奇蹄目.md "wikilink")、[偶蹄目及](../Page/偶蹄目.md "wikilink")[鯨目的近親](../Page/鯨目.md "wikilink")\[2\]\[3\]，故有蹄類最少是從兩個不同的分支獨立演化而成的。這亦有可能由于踝節目本身是一個[多系群](../Page/多系群.md "wikilink")，即踝節目內的成員未必是近親。

除了[南蹄目及現今的有蹄類外](../Page/南蹄目.md "wikilink")，踝節目被認為是其他己滅絕的哺乳動物（如[中爪獸目及](../Page/中爪獸目.md "wikilink")[恐角目](../Page/恐角目.md "wikilink")）的祖先。\[4\]\[5\]

## 參考

[Category:勞亞獸總目](../Category/勞亞獸總目.md "wikilink")
[\*](../Category/踝節目.md "wikilink")
[C](../Category/古新世哺乳類.md "wikilink")
[C](../Category/始新世哺乳類.md "wikilink")

1.
2.
3.
4.
5.