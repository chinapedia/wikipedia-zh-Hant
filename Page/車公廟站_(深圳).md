**車公廟站**是[深圳地鐵](../Page/深圳地鐵.md "wikilink")[1号綫](../Page/深圳地鐵1号綫.md "wikilink")、[7号綫](../Page/深圳地鐵7号綫.md "wikilink")、[9号綫](../Page/深圳地鐵9号綫.md "wikilink")、[11号綫的一个换乘站](../Page/深圳地鐵11号綫.md "wikilink")，位於[深圳市香蜜湖路西側的](../Page/深圳市.md "wikilink")[深南大道地底](../Page/深南大道.md "wikilink")，於2004年12月28日随[1号綫的开通而啟用](../Page/深圳地鐵1号綫.md "wikilink")。車站設計以白色和橙色為主。

本站在深圳地铁三期通車後，成为深圳首個4線[轉乘站](../Page/換乘站.md "wikilink")，即[1号綫](../Page/深圳地鐵1号綫.md "wikilink")、[7号綫](../Page/深圳地鐵7号綫.md "wikilink")、[9号綫](../Page/深圳地鐵9号綫.md "wikilink")、[11号綫的四线大型换乘枢纽站](../Page/深圳地鐵11号綫.md "wikilink")。
未來[20號綫](../Page/深圳地鐵20號綫.md "wikilink")（福永綫）通車後,車公廟站便會成為深圳市首個5線交匯的巨型交通樞紐。

## 車站結構

车公庙站整体可分为三部分：1号线部分、11号线部分、9、7号线共用部分。1号线部分与11号线部分紧密相连，11号线站台布置在原有1号线站台南侧，均沿东西方向布置。7、9号线共用部分位于1、11号线部分以东，与1、11号线部分大致垂直。

### 楼层分布

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面（L1）</strong></p></td>
<td></td>
<td><p>出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一層（B1）</strong></p></td>
<td><p>站厅</p></td>
<td><p>站厅、售票機、1、7、9、11號綫轉乘通道、出入口（連接地下街）</p></td>
</tr>
<tr class="odd">
<td><p><strong>地下二層（B2）</strong></p></td>
<td><p>轉乘層</p></td>
<td><p>7、9號綫轉乘通道</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong></strong> <strong>往机场东</strong><small>（<a href="../Page/竹子林站.md" title="wikilink">竹子林</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，左邊車門將會開啟</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong></strong> <strong>往罗湖 </strong><small>（<a href="../Page/香蜜湖站.md" title="wikilink">香蜜湖</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong></strong> <strong>往碧頭</strong><small>（<a href="../Page/紅樹灣南站.md" title="wikilink">紅樹湾南</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，左邊車門將會開啟</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong></strong> <strong>往福田</strong> <small>（<a href="../Page/福田站.md" title="wikilink">福田</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>地下三層（B3）</strong></p></td>
<td></td>
<td><p><strong></strong> <strong>往西麗湖 </strong><small>（<a href="../Page/農林站.md" title="wikilink">農林</a>）</small></p></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，右邊車門將會開啟</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong> </strong> <strong>往紅樹灣南</strong><small>（<a href="../Page/下沙站.md" title="wikilink">下沙</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong></strong> <strong>往太安</strong><small>（<a href="../Page/上沙站.md" title="wikilink">上沙</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，左邊車門將會開啟</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong></strong> <strong>往文錦</strong> <small>（<a href="../Page/香梅站.md" title="wikilink">香梅</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 1、11號綫部分

车公庙站1号线部分位於[深南大道下方](../Page/深南大道.md "wikilink")，主体结构为单柱双层双跨结构与双柱双层三跨结构。采用暗挖法施工，总建筑面积12288平方米。车站地下一层为站厅层，中间站厅为乘客办理进出站手续的集中活动场所，两端站厅主要布置管理用房和部分设备用房。车站地下二层为站台层，中间站台为乘客候车与乘车场所，两端布置设备用房，站台采用[岛式站台](../Page/岛式站台.md "wikilink")，屏蔽门长136.5米。车站总长230.5米。

车公庙站11号线部分位于深南大道下方，位于1号线部分南侧，为地下两层结构，总长约369米，车站标准段宽26.8米，底板底埋深约18.35米。\[1\]11号线采用[岛式站台](../Page/岛式站台.md "wikilink")，有效部分长186米，总长414.3米，宽13.5米，为双柱结构。\[2\]

11号线站厅平行布置于1号线站厅南侧，两站厅均位于地下一层。站厅相互独立，但之间相连通，乘客可通过站厅换乘。 {{ gallery |
<File:Shenzhen> Metro Line 1 Chegongmiao Sta Platform.jpg|1号线站台 |
<File:Shenzhen> Metro Line 11 Chegongmiao Sta Platform.jpg|11号线站台 |
<File:Shenzhen> Metro Line 1 Chegongmiao Sta Hall.jpg|1号线站厅 |
<File:Shenzhen> metro Chegongmiao Station.jpg|11号线 | <File:Shenzhen>
metro Chegongmiao Station 1.jpg|11号线月台站名标志 }}

### 7、9號綫部分

7、9号线部分位于[香蜜湖路西侧](../Page/香蜜湖路.md "wikilink")，为地下三层结构，总长315米，标准段宽41.3米，底板底埋深约25.5米。\[3\]

7、9号线共用站厅位于地下一层，其北端与11号线站厅东端相连，并设有[自动人行道以便换乘](../Page/自动人行道.md "wikilink")。

7號線和9號線設有[跨月台轉乘](../Page/跨月台轉乘.md "wikilink")\[4\]，提供東、西向同方向跨月台轉乘。如乘客需要在7、9號綫進行相反方向轉乘，可利用扶手電梯／升降機到車站大堂和月台之間的轉乘層轉乘，不需要上兩層到車站大堂轉乘，節省時間。两个岛式站台均位于地下三层，沿南北向并排布置，5、7号站台位于西侧，6、8号站台位于东侧，主体面积总共约11500平方米，总长315米，宽度均为11.5米。
{{ gallery | <File:Shenzhen> Metro Line 7 Chegongmiao Sta Platform
5.jpg|7号线5号站台 | <File:Platform> of Chegongmiao Station (Meilin
Line).jpg|9号线8号站台 | <File:Shenzhen> Metro Line 7&9 Chegongmiao Sta
Concourse facing 1&11.jpg|位于地下一层的7、9号线站厅（面向11号线部分） | <File:Shenzhen>
Metro Line 7&9 Chegongmiao Sta Concourse facing
7&9.jpg|位于地下一层的7、9号线站厅（自北向南望） |
<File:Shenzhen> Metro Line 7&9 Chegongmiao Sta Transferring
Platform.jpg|7、9号线换乘平台（地下二层） }}

</center>

## 运营时刻表

<table>
<thead>
<tr class="header">
<th><p>行驶方向</p></th>
<th><p>首班车</p></th>
<th><p>末班车</p></th>
<th><p>所属线路</p></th>
<th><p>高峰间隔</p></th>
<th><p>平峰间隔</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>罗湖</p></td>
<td><p>06:32（工作日）<br />
06:39（休息日及节假日）</p></td>
<td><p>23:45（工作日及休息日）<br />
00:45（节假日）</p></td>
<td></td>
<td><p>2分45秒（工作日）<br />
3分钟（节假日及休息日）</p></td>
<td><p>5分30秒（工作日）<br />
6分钟（节假日及休息日）</p></td>
</tr>
<tr class="even">
<td><p>机场东</p></td>
<td><p>06:51</p></td>
<td><p>23:21（工作日及休息日）<br />
00:21（节假日）</p></td>
<td></td>
<td><p>2分45秒（工作日）<br />
3分钟（节假日及休息日）</p></td>
<td><p>5分30秒（工作日）<br />
6分钟（节假日及休息日）</p></td>
</tr>
<tr class="odd">
<td><p>西丽湖</p></td>
<td><p>06:45</p></td>
<td><p>23:35</p></td>
<td></td>
<td><p>5分钟</p></td>
<td><p>10分钟</p></td>
</tr>
<tr class="even">
<td><p>太安</p></td>
<td><p>06:30</p></td>
<td><p>23:21</p></td>
<td></td>
<td><p>5分钟</p></td>
<td><p>10分钟</p></td>
</tr>
<tr class="odd">
<td><p>红树湾南</p></td>
<td><p>06:46</p></td>
<td><p>23:36</p></td>
<td></td>
<td><p>5分钟</p></td>
<td><p>10分钟</p></td>
</tr>
<tr class="even">
<td><p>文錦</p></td>
<td><p>06:30</p></td>
<td><p>23:11</p></td>
<td></td>
<td><p>5分钟</p></td>
<td><p>10分钟</p></td>
</tr>
<tr class="odd">
<td><p>福田</p></td>
<td><p>06:30</p></td>
<td><p>23:52（工作日及休息日）<br />
00:52（节假日）</p></td>
<td></td>
<td><p>5分钟（工作日）<br />
7分50秒（节假日及休息日）</p></td>
<td><p>7分50秒</p></td>
</tr>
<tr class="even">
<td><p>碧头</p></td>
<td><p>06:33</p></td>
<td><p>23:03（工作日及休息日）<br />
00:03（节假日）</p></td>
<td></td>
<td><p>5分钟（工作日）<br />
7分50秒（节假日及休息日）</p></td>
<td><p>7分50秒</p></td>
</tr>
</tbody>
</table>

## 出口

車公廟站设九個出口。

<table>
<thead>
<tr class="header">
<th><p>width:70px | 出口编号</p></th>
<th><p>width:70px | <small>无障碍设施</small></p></th>
<th><p>建议前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p>通往<a href="../Page/深南大道.md" title="wikilink">深南大道北側</a>、<a href="../Page/香蜜湖路.md" title="wikilink">香蜜湖路</a>、財富廣場、東海花園等地。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tactile_Paved.svg" title="fig:Tactile_Paved.svg">Tactile_Paved.svg</a><a href="https://zh.wikipedia.org/wiki/File:Lift.svg" title="fig:Lift.svg">Lift.svg</a></p></td>
<td><p>通往深南大道北側、農園路、工商物價大廈、深圳市市場監督管理局、<a href="../Page/深圳高級中學.md" title="wikilink">深圳高級中學</a>、<a href="../Page/深圳圣安多尼堂.md" title="wikilink">天主教深圳聖安多尼堂</a>、<a href="../Page/招商银行大厦.md" title="wikilink">招商銀行大廈</a>、深圳東海朗廷酒店、東莞銀行深圳分行、<a href="../Page/深圳東海花園.md" title="wikilink">深圳東海花園</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>通往深南大道南側、泰然九路、<a href="../Page/泰然工業區.md" title="wikilink">泰然工業區</a>、喜年中心、<a href="../Page/深圳希瑪林順潮眼科醫院.md" title="wikilink">深圳希瑪林順潮眼科醫院</a>、<a href="../Page/深圳電氣科學研究所.md" title="wikilink">深圳電氣科學研究所</a>、<a href="../Page/丰盛町.md" title="wikilink">丰盛町</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>通往香蜜湖路、本元大廈、中國有色大廈</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>通往泰然二路、深南大道南側、中國郵政、<a href="../Page/天安數碼時代.md" title="wikilink">天安數碼時代</a>、<a href="../Page/天安數碼城.md" title="wikilink">天安數碼城</a>、<a href="../Page/永亨銀行.md" title="wikilink">永亨銀行車公廟支行</a>、純K等地</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>通往泰然七路、滙通大廈</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>通往香蜜湖路西側、泰然四路、泰然六路</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Lift.svg" title="fig:Lift.svg">Lift.svg</a></p></td>
<td><p>通往香蜜湖路東側、天安文化廣場、深圳<a href="../Page/天安數碼城.md" title="wikilink">天安數碼城</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>通往香蜜湖路東側、泰然四路、創新科技廣場</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>通往深南大道南侧、香蜜湖路东侧、中联大厦、安徽大厦创展中心</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>通往深南大道北侧、香蜜湖路东侧、香蜜湖立交</p></td>
</tr>
<tr class="even">
<td><p>注：<a href="https://zh.wikipedia.org/wiki/File:Tactile_Paved.svg" title="fig:Tactile_Paved.svg">Tactile_Paved.svg</a> 設有盲人引導徑 {{!}}<a href="https://zh.wikipedia.org/wiki/File:Lift.svg" title="fig:Lift.svg">Lift.svg</a> 設有<a href="../Page/升降機.md" title="wikilink">升降機</a> {{!}} <a href="https://zh.wikipedia.org/wiki/File:Stairlift_Sign.svg" title="fig:Stairlift_Sign.svg">Stairlift_Sign.svg</a> 設有輪椅升降台</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 接驳交通

## 车站设计

<center>

{{ gallery | width = 300 | <File:Shenzhen> Metro Line 11 Chegongmiao Sta
Painting.jpg|11号线车公庙站的壁画 | <File:Shenzhen> Metro Line 7&9 Chegongmiao
Sta Concourse Cultural Wall.jpg|位于7、9号线换乘平台的艺术墙 }}

</center>

车公庙站設計以白色和橙色為主，11号线部分为“无装修”设计，即墙面上没有任何涂料。

車站設有多組藝術牆。因應[車公的歷史背景](../Page/車公.md "wikilink")，[11號線的藝術牆以](../Page/深圳地鐵11號線.md "wikilink")[宋代為創作背景](../Page/宋代.md "wikilink")，包括《宋代科技》、《蠶織圖》等\[5\]。7、9號綫轉乘層設有兩組藝術牆，分別為介紹[車公的](../Page/車公.md "wikilink")《車公歷史》，及有關鐵路歷史的《軌道交通史》。

## 車站設施

本站在車站大堂設有洗手間。另外在5、7號月台及6、8號月台南端各設有洗手間。

## 历史

2004年12月28日，車公廟站随[深圳地铁1号线开通而啟用](../Page/深圳地铁1号线.md "wikilink")。

2013年7月，車站封閉C、D出口，并在早晚高峰采取客流疏导措施，以進行[7号綫](../Page/深圳地鐵7号綫.md "wikilink")、[9号綫](../Page/深圳地鐵9号綫.md "wikilink")、[11号綫的轉乘通道興建工程](../Page/深圳地鐵11号綫.md "wikilink")。\[6\]

2016年6月28日，位於1號線站廳南方的11號線新站廳正式啟用。

2016年10月28日，地下三层的7、9号线启用。

2017年6月13日，[強烈熱帶風暴苗柏正面吹襲](../Page/強烈熱帶風暴苗柏_\(2017年\).md "wikilink")[深圳](../Page/深圳.md "wikilink")，帶來連場暴雨，[深圳地鐵車公廟站嚴重水浸](../Page/深圳地鐵.md "wikilink")，站廳一度水深至[小腿](../Page/小腿.md "wikilink")，黃泥水像[瀑布般沿樓梯湧往站台](../Page/瀑布.md "wikilink")，本車站一度關閉，其間四條地鐵綫不停此站。\[7\]

## 未來發展

[深圳地鐵20號綫或會東延到市區](../Page/深圳地鐵20號綫.md "wikilink")，本站為其中一個規劃中的車站，如有關計劃落實，本站將成為深圳市內第一個5綫轉乘車站。

## 鄰近車站

## 參考資料

## 外部連結

  - [深圳地铁官方网站](http://www.szmc.net/page/service/service.html?code=2103&site=0111&newsSearchToken=b9d6bfe4-74fa-4161-88d6-4208b0daca66)

[Category:福田區](../Category/福田區.md "wikilink")
[Category:2004年啟用的鐵路車站](../Category/2004年啟用的鐵路車站.md "wikilink")
[Category:深圳地铁换乘站](../Category/深圳地铁换乘站.md "wikilink")
[Category:以宗教場所命名的鐵路車站](../Category/以宗教場所命名的鐵路車站.md "wikilink")

1.
2.

3.
4.

5.

6.

7.