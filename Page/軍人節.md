**軍人節**是很多國家紀念[軍人的特定節日](../Page/軍人.md "wikilink")。

##

在阿根廷，是軍人的紀念日如下：

  - 5月17日：阿根廷海軍節
  - 5月29日：阿根廷陸軍節
  - 8月10日：阿根廷空軍節

##

Բանակիօր（中文：建軍節）。是慶祝1月28日，以紀念在1992年新獨立的的創建。

##

[澳紐軍團日是](../Page/澳新军团日.md "wikilink")及國定假日，紀念[澳大利亞及紐西蘭軍團](../Page/澳新军团.md "wikilink")（ANZAC）1915年4月25日在[加利波利戰役中登陸於](../Page/加里波利之战.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")[加里波利半島的某一不明灣澳](../Page/加里波利.md "wikilink")，戰後該地命名為，以紀念是次軍事行動。

##

SilahlıQüvvələrGünü（中文：武裝部隊日）於6月26日慶祝。

##

孟加拉國定11月21日為建軍節以紀念反對佔領巴基斯坦部隊在解放戰爭時期， 1971年三服務聯合行動之際。這一天開始於一個花圈鋪設在'Sikha
Anirban' （永恆之火）在達卡兵營由總統，總理和服務的酋長。在下午於Senakunja舉行招待會
，達卡兵營所在的總理，部長，反對黨領袖和其他高文武官員參加。在其他的軍營裡，海軍基地，空軍基地，類似招待會舉行。一個特殊的電視節目艾米班是在播出不同的電視頻道的前一天晚上，和特殊報紙副刊發表與全國性日報。招待會由總理和服務酋長的英勇獎自由戰士獎的收件人還舉行。特別餐家庭成員曾在所有軍事電台。武裝部隊司還帶出了一個特殊的出版物，有關獨立戰爭的文章和軍隊。

##

在巴西，是軍人的紀念日如下：

  - Marinha do Brasil（中文：巴西海軍）：
      - 6月11日 - Data Magna da Marinha do Brasil（中文：巴西海軍節）
      - 12月13日 - Dia do Marinheiro（中文：水手節）
  - Exército Brasileiro（中文：巴西陸軍）：
      - 4月19日 - Dia do Exército（中文：巴西陸軍節）
      - 8月25日 - Dia do Soldado（中文：軍人節）
  - Força Aérea Brasileira（中文：巴西空軍）：
      - 10月23日 - Dia do Aviador（中文：飛行員節）

##

英勇日，於每年的5月6日紀念

##

Tatmadaw Nay（中文：軍人節）於3月27日慶祝，紀念在1945年緬甸軍隊對日本的抵抗。

##

Canadian Forces Day（中文：加拿大軍人節）是在六月的第一個星期日。加拿大軍人節並非公眾假期。

##

在智利，Día de las Glorias del Ejército 是一個在9月19日（獨立日的隔天）的國定假日。

##

類似的節日有[八一建軍節](../Page/八一建軍節.md "wikilink")，紀念由共产党领导的[南昌起义于](../Page/南昌起义.md "wikilink")[1927年](../Page/1927年.md "wikilink")[8月1日爆发](../Page/8月1日.md "wikilink")。南昌起义被认为是中国共产党独立领导革命战争、创立人民军队和武装夺取政权的开端。

##

軍人節為[中華民國國軍的紀念節日](../Page/中華民國國軍.md "wikilink")，早期依據軍種不同而有不同的慶祝時間，如[7月7日陸軍節](../Page/七七事變.md "wikilink")、[8月14日空軍節](../Page/八一四空戰.md "wikilink")、[12月12日憲兵節](../Page/西安事變.md "wikilink")、[4月1日聯勤節](../Page/聯勤.md "wikilink")。[1955年](../Page/1955年.md "wikilink")（[民國44年](../Page/民國44年.md "wikilink")），[中華民國國防部為了統一各軍種節日](../Page/中華民國國防部.md "wikilink")，因此選擇[第二次世界大戰對日戰爭勝利紀念日](../Page/第二次世界大戰對日戰爭勝利紀念日.md "wikilink")9月3日作為[中華民國的軍人節](../Page/中華民國.md "wikilink")，該日各軍種均有慶祝活動。依據中華民國《[紀念日及節日實施辦法](../Page/s:紀念日及節日實施辦法_\(民國101年\).md "wikilink")》\[1\]，軍人節依國防部規定放假。軍人節同時也是[中華民國政府舉行中樞](../Page/中華民國政府.md "wikilink")[秋祭的日子](../Page/秋祭.md "wikilink")，依照慣例，[總統](../Page/中華民國總統.md "wikilink")、五院院長等中央政府要員親臨[國民革命忠烈祠](../Page/忠烈祠.md "wikilink")，向奉祀祠內的眾多殉職軍人以及對國家有功的人員表達最深的敬意。

##

[韓國的軍人節稱為](../Page/韓國.md "wikilink")，是紀念[1950年](../Page/1950年.md "wikilink")[10月1日](../Page/10月1日.md "wikilink")，[大韓民國陸軍](../Page/大韓民國陸軍.md "wikilink")越過[三八線進行](../Page/三八線.md "wikilink")[北伐的日子](../Page/平壤-元山戰役.md "wikilink")。

##

4月25日是[朝鮮人民軍建軍日](../Page/朝鮮人民軍.md "wikilink")。

##

[伊拉克軍人節是](../Page/伊拉克.md "wikilink")[1月6日](../Page/1月6日.md "wikilink")。

##

[馬里軍人節是](../Page/馬里共和國.md "wikilink")[1月20日](../Page/1月20日.md "wikilink")。\[2\]

##

[墨西哥军人节](../Page/墨西哥.md "wikilink")（）是2月19日，从1950年开始庆祝，为纪念1917年建军。

##

[美國軍人節](../Page/美國.md "wikilink")（[英語](../Page/英語.md "wikilink")：**Armed
Forces
Day**）是5月第三個星期六。[1949年](../Page/1949年.md "wikilink")[8月31日](../Page/8月31日.md "wikilink")，美國國防部宣佈以5月第三個星期六作爲全體美軍共同的節日。

## 参考文献

## 参见

  - [建军节](../Page/建军节.md "wikilink")
  - [军人](../Page/军人.md "wikilink")

[Category:纪念日](../Category/纪念日.md "wikilink")
[Category:主题日](../Category/主题日.md "wikilink")
[Category:軍事人物](../Category/軍事人物.md "wikilink")

1.
2.