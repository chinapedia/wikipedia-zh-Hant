**軍團菌**是一类[革兰氏阴性菌](../Page/革兰氏阴性菌.md "wikilink")，包括造成[退伍軍人症的嗜肺军团菌](../Page/退伍軍人症.md "wikilink")。

軍團菌在許多環境中都很常见，已确定的至少有50个種和70個[血清型](../Page/血清型.md "wikilink")。该属菌細胞壁[取代基所攜帶的基团标识了其体抗原](../Page/取代基.md "wikilink")。這些取代基的化學构成，包括其成分的不同以及糖原的不同排列方式，決定了其表面體抗原或是[O-抗原的性质](../Page/脂多糖.md "wikilink")，而這正是血清學中对許多革兰氏阴性菌分類的基本手段。

軍團菌的名字源於1976年7月在[費城爆发的一种](../Page/費城.md "wikilink")“神秘疾病”，该病共造成221人患病，34人死亡，患者大都为[美國退伍軍人](../Page/美國.md "wikilink")，因而被冠名為[退伍軍人症](../Page/退伍軍人症.md "wikilink")。[退伍軍人症被廣泛宣傳而造成一定程度的恐慌](../Page/退伍軍人症.md "wikilink")。1977年1月18日，该病的病原體被確定為一种不為人知的[細菌](../Page/細菌.md "wikilink")，隨後被命名為軍團菌。

## 參見

  - [殺菌劑](../Page/殺菌劑.md "wikilink")
  - [環境微生物學](../Page/環境微生物學.md "wikilink")
  - [醫院感染](../Page/醫院感染.md "wikilink")

## 參考資料

## 外部連結

  - [CDC Division of bacterial and Mycotic Diseases:
    Legionellosis](http://www.cdc.gov/ncidod/dbmd/diseaseinfo/legionellosis_t.htm)
  - [Directors of Health Promotion and Education page on
    Legionellosis](http://www.astdhpphe.org/infect/legion.html)
  - [European Working Group for Legionella
    Infections](http://www.ewgli.org/)
  - [Legionnaires' disease outbreaks](http://www.q-net.net.au/~legion)
  - [世界卫生组织
    军团病](http://www.who.int/mediacentre/factsheets/fs285/zh/index.html)

### 維護指引

  - [Centers for Disease Control and
    Prevention](http://www.cdc.gov/ncidod/dhqp/pdf/guidelines/Enviro_guide_03.pdf)
    - Procedure for Cleaning Cooling Towers and Related Equipment (pages
    239 and 240 of 249)
  - [Cooling Technology
    Institute](https://web.archive.org/web/20060627200918/http://www.cti.org/downloads/legion_2000.pdf)
    - Best Practices for Control of Legionella
  - [California Energy
    Commission](http://www.energy.ca.gov/2005publications/CEC-700-2005-025/CEC-700-2005-025.PDF)
    - Cooling Water Management Program Guidelines For Wet and Hybrid
    Cooling Towers at Power Plants
  - [ASHRAE
    Guideline](http://www.ashrae.org/publications/detail/14891#Gdl12/)
  - [Guidelines for Control of Legionella in Ornamental
    Fountains](http://www.legionellae.org/guidelines/guidelines.htm)

[Category:变形菌门](../Category/变形菌门.md "wikilink")
[Category:细菌性疾病](../Category/细菌性疾病.md "wikilink")
[Category:工业卫生](../Category/工业卫生.md "wikilink")
[Category:革兰氏阴性菌](../Category/革兰氏阴性菌.md "wikilink")