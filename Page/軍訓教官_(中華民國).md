**軍訓教官**，是指普設於[中華民國各](../Page/中華民國.md "wikilink")[高級中等學校或](../Page/高級中等學校.md "wikilink")[大專院校的現役軍官](../Page/台灣大專院校列表.md "wikilink")，主要業務包含維護校園安全、學生之軍事訓練等。軍訓教官主管單位為[教育部](../Page/中華民國教育部.md "wikilink")[學生事務及特殊教育司](../Page/教育部學生事務及特殊教育司.md "wikilink")，因此未享有由[國防部發放的軍人薪餉](../Page/中華民國國防部.md "wikilink")，而是由學校（公立學校教官）或教育部（私立學校教官）編列預算。高級中學及大專軍訓教官待遇法源由**國軍志願役軍官俸額表**定訂\[1\]
\[2\]。公立高中職教官，與部隊軍官相比較，短少志願役加給每月1萬元，故其薪資通常較部隊少。

台灣於1997年才有[社會工作師法](../Page/社會工作師法.md "wikilink")\[3\]，於2001年才有[心理師法](../Page/心理師法.md "wikilink")\[4\]。於此之前全台灣皆沒有合法的專業[心理師](../Page/心理師.md "wikilink")、[社工師](../Page/社工師.md "wikilink")。[高中](../Page/高中.md "wikilink")、[高職](../Page/高職.md "wikilink")、[技職學校](../Page/技職學校.md "wikilink")、[專科學校](../Page/專科學校.md "wikilink")、[大學皆仰賴大量](../Page/大學.md "wikilink")[超時工作的](../Page/過勞.md "wikilink")[軍訓教官擔任](../Page/軍訓教官.md "wikilink")[社工師](../Page/社工師.md "wikilink")、[心理師](../Page/心理師.md "wikilink")、[輔導老師](../Page/輔導老師.md "wikilink")、[保全](../Page/保全.md "wikilink")、[警衛](../Page/警衛.md "wikilink")、[舍監等](../Page/舍監.md "wikilink")，原本應該由[專業人員執行的工作](../Page/專業人員.md "wikilink")。教官依法雖身著[軍服](../Page/軍服.md "wikilink")，但實際上並沒有[警察權](../Page/警察權.md "wikilink")\[5\]、與[台灣軍隊也沒有直接隸屬關係](../Page/台灣軍隊.md "wikilink")。[台灣政府長期未](../Page/台灣教育部.md "wikilink")[編列足夠的教育](../Page/財政部.md "wikilink")[預算](../Page/政府預算.md "wikilink")，各級[學校長期仰賴](../Page/學校.md "wikilink")[服從性高的軍人代理各種校務](../Page/軍事訓練.md "wikilink")，甚至部分設有[國中部的高級中學會指派軍訓教官協助國中的學生事務](../Page/國中.md "wikilink")。\[6\]

## 歷史沿革

### 臨時、北洋政府時期

實施多年的軍訓教官制度沿襲自[中華民國於中國大陸的各級政府](../Page/中華民國.md "wikilink")。1912年，新成立的中華民國政府陸續發表新教育政策。其中，[教育總長](../Page/教育總長.md "wikilink")[蔡元培以](../Page/蔡元培.md "wikilink")[德國教育為藍本](../Page/德國教育.md "wikilink")，發表「[軍國民教育](../Page/軍國民教育.md "wikilink")」主張。該主張強調新中國應學習德國教育現況，對高級中學以上學生實施積極性的「[軍事教育](../Page/軍事教育.md "wikilink")」。經中國教育部門相關部會決議通過後，該實施辦法送至中國[北洋政府教育部實行](../Page/北洋政府.md "wikilink")。這裡面部分實施該教育的中國中等學校，其內容為講授[軍事學大要](../Page/軍事學.md "wikilink")，實施[兵式操](../Page/兵式操.md "wikilink")、[射擊及](../Page/射擊.md "wikilink")[軍事體能等](../Page/軍事體能.md "wikilink")。

1919年，[第一次世界大戰結束](../Page/第一次世界大戰.md "wikilink")，[中德關係生變](../Page/中德關係.md "wikilink")。另因提倡軍國民教育的[德國成為戰敗國](../Page/德國.md "wikilink")，因此將軍事教育融入中等學校的教育思想也因之式微。1924年，[江蘇省教育會設](../Page/江蘇省教育會.md "wikilink")「[學校軍事研究會](../Page/學校軍事研究會.md "wikilink")」，將原有軍國民教育思維改名為「學校軍事教育」並以「實施軍事教育，以養成強健身體」，為教育[宗旨之一](../Page/宗旨.md "wikilink")。

將軍事教育融入[地方政府自治事項的](../Page/地方自治.md "wikilink")[江蘇省](../Page/江蘇省.md "wikilink")，以軍事教育可養成健全[體格](../Page/體格.md "wikilink")、軍事教育可養成整齊[習慣](../Page/習慣.md "wikilink")、軍事教育可養成[尚武精神](../Page/尚武精神.md "wikilink")、軍事教育可養成[自衛能力為理由](../Page/自衛.md "wikilink")，江蘇省境內[中等學校以上均必須加入軍事教育](../Page/中等學校.md "wikilink")，並由現役軍官予以輔導。

### 國民政府時期

1928年，[國民革命軍北伐勝利後的](../Page/國民革命軍北伐.md "wikilink")[國民政府以](../Page/國民政府.md "wikilink")[五三慘案為由](../Page/五三慘案.md "wikilink")，通過《[中等以上學校軍事教育方案](../Page/中等以上學校軍事教育方案.md "wikilink")》，該方案以[江蘇省實施軍訓為藍本](../Page/江蘇省.md "wikilink")，強制全國所屬高中以上學校學生，應以軍事科為必修，女生應習[護理學](../Page/護理學.md "wikilink")。其中，省市教育廳局內設立的[國民軍事訓練委員會首見以軍事訓練或軍訓一詞](../Page/國民軍事訓練委員會.md "wikilink")，而該委員會首設的軍訓教官編制，則訂定軍事學校畢業學生為軍訓教官的遴選方針。

1930年代的軍訓教官，對高中以上學校學生的授習內容約為每期至少四小時的男學生軍訓知識，女學生之軍事護理教習。除此，也教授傾向軍事方面的重[軍事地理](../Page/軍事地理.md "wikilink")、[軍事地圖](../Page/軍事地圖.md "wikilink")、[民族競爭史](../Page/民族競爭.md "wikilink")、[國恥史](../Page/國恥史.md "wikilink")、[軍用機械軍事工程](../Page/軍用機械.md "wikilink")、個人勇氣薰陶等等。

根據中國教育會議通過「中等以上學校軍訓教育方案」，1937年[中日戰爭爆發前](../Page/中日戰爭.md "wikilink")，國民政府繼續以軍事教官為種子，實施高級中學的軍事工作，也詳訂由現役軍人為轉任原則的《[軍事教官任用章程](../Page/軍事教官任用章程.md "wikilink")》、《[軍訓教官服務條例](../Page/軍訓教官服務條例.md "wikilink")》等。這段期間軍訓教官除軍事教育之外，也開始負責政治思想教育任務。諸如教育救國、自強圖存、負責任、明禮義、鼓舞愛國情操，激勵學生民心士氣、蓄積抗戰國力等方針。這裡面，以「[十萬青年十萬軍](../Page/十萬青年十萬軍.md "wikilink")」的[知識青年從軍為最主要歷程](../Page/知識青年.md "wikilink")。

1945年[八年抗戰勝利後](../Page/八年抗戰.md "wikilink")，中國國民黨的軍訓教育受到以[中國共產黨為主的人士質疑](../Page/中國共產黨.md "wikilink")。他們認軍訓教官為國民黨制止各大中學[罷課](../Page/罷課.md "wikilink")、[示威](../Page/示威.md "wikilink")、[遊行的](../Page/遊行.md "wikilink")[工作人員](../Page/工作人員.md "wikilink")，並以「反[訓導](../Page/訓導.md "wikilink")」、「反[徵兵](../Page/徵兵.md "wikilink")」為由，全面反對校園內軍訓教官設置，致使中國大陸各校紛紛停止設置軍訓教官。之後以中國國民黨為主的國民政府將[昆明](../Page/昆明.md "wikilink")[121血案](../Page/121血案.md "wikilink")、[重慶](../Page/重慶.md "wikilink")[校場口事件](../Page/校場口事件.md "wikilink")、[北平](../Page/北平.md "wikilink")[沈崇事件](../Page/沈崇事件.md "wikilink")、1947年[南京](../Page/南京.md "wikilink")[五二〇運動等校園動盪歸咎於學生軍訓教官的取消與式微](../Page/五二〇運動.md "wikilink")。

### 行憲、政府遷臺時期

1951年，[中華民國政府於](../Page/中華民國政府.md "wikilink")[臺灣地區全面恢復學生軍訓教育](../Page/臺灣地區.md "wikilink")。首先由[中華民國國防部選拔優秀軍官幹部](../Page/中華民國國防部.md "wikilink")18員擔任軍訓教官。經8所[師範學校試辦成功後](../Page/師範學校.md "wikilink")，翌年4月[中華民國教育部頒布](../Page/中華民國教育部.md "wikilink")《[高中以上學校學生軍事精神體格及技能訓練綱要](../Page/高中以上學校學生軍事精神體格及技能訓練綱要.md "wikilink")》，並由[蔣經國主持的](../Page/蔣經國.md "wikilink")[中國青年反共救國團與國防部主導教官遴選配置](../Page/中國青年反共救國團.md "wikilink")。1960年，學生軍訓教官任用權移歸教育部主導，正式納入教育體系，不過軍訓教官仍由現役軍人轉任，並由國防部協助考核遴選。另一方面，因為教官具軍人身份，在管理訓導方面故仍適用[軍法體系](../Page/軍法.md "wikilink")。

### 戒嚴時期與解嚴後

早期任務則注重於中華民國高級中學與大專學院內的[軍事訓練](../Page/軍事訓練.md "wikilink")、學生品行管理與[思想政治教育](../Page/思想政治教育.md "wikilink")、並推展黨務（加入[中國國民黨](../Page/中國國民黨.md "wikilink")）。在學校實施一些軍事教育、效仿國軍精神，並在學校的旗杆上面倣傚軍隊設置三根旗杆，中間為[中華民國國旗](../Page/中華民國國旗.md "wikilink")、左邊為[中國青年反共救國團團旗](../Page/中國青年反共救國團.md "wikilink")、右邊為學校校旗。

1987年解嚴、2000年[民進黨執政後](../Page/民進黨.md "wikilink")，教官功能、體制及性質均有重大改變。軍訓教官的功能定位於負責學生[生活輔導及](../Page/生活輔導.md "wikilink")[校園安全維護等工作](../Page/校園安全.md "wikilink")，而其編制或業務工作明細也都列於各學校組織法等。

2013年[十二年國教](../Page/十二年國教.md "wikilink")《高級中等教育法》，通過附帶決議，在2023年8月1日讓教官全面退出校園\[7\]。

## 現況

### 職責

軍訓教官在學校任務：平時是維護校園安全、校園秩序、校外活動安全、教授國防通識課程，災害時指揮學生、老師避難與救助；戰時編組學生民防團能力（民防法：高中（職）、大專院校之在校學生，應參加各該學校防護團編組支援服勤、防空避難指揮，各校並有軍械室編制，配有M1步槍（儀隊表演槍）以及教育用[T65K2步槍](../Page/T65突擊步槍#T65K2突擊步槍.md "wikilink")。但均為教育訓練用途，會將[撞針刮除](../Page/撞針.md "wikilink")，無法正常射擊，僅戰時有需，會將撞針重裝，作為後備武器。

現在大多數高中仍然存在的軍訓文化舉例如下：

  - 軍訓檢閱：在大慶典的校慶中、模仿軍隊檢閱方式，檢閱官為校長、學務處主任與主任教官等重要人士。
  - [軍歌比賽](../Page/軍歌.md "wikilink")：演唱軍歌並進行競賽，以學習[軍人的](../Page/軍人.md "wikilink")[黃埔精神](../Page/黃埔精神.md "wikilink")、雄壯氣魄和展團結一致的精神。
  - 軍樂隊：學校出資訓練，在重要典禮、週會的配樂，吹奏[國歌](../Page/中華民國國歌.md "wikilink")、[國旗歌](../Page/中華民國國旗歌.md "wikilink")、恭迎[國旗](../Page/中華民國國旗.md "wikilink")、頒獎樂……。
  - [儀隊](../Page/中華民國儀隊.md "wikilink")：比照[中華民國國軍](../Page/中華民國國軍.md "wikilink")[三軍儀隊的模式](../Page/中華民國儀隊.md "wikilink")、放入學校當中。
  - 服儀糾察隊：比照[國軍之](../Page/中華民國國軍.md "wikilink")[憲兵與](../Page/中華民國憲兵.md "wikilink")[軍事糾察隊權力進行服裝儀容糾舉](../Page/軍事糾察隊.md "wikilink")，展現學生整齊劃一的服儀。

### 服裝

除了總教官外，通常教官之軍階在[中尉至](../Page/中尉.md "wikilink")[上校之間](../Page/上校.md "wikilink")。而正因為仍為現役軍員，依照《軍訓教官服裝規定》、《軍訓教官服裝製補作業計畫》、《陸海空軍服制條例》等規定，所有[教官制服均需依照不同](../Page/教官制服.md "wikilink")[軍種](../Page/軍種.md "wikilink")、[性別來製作](../Page/性別.md "wikilink")[服裝](../Page/服裝.md "wikilink")，並須於校園教學與服勤期間穿著服制規定之乙式軍便服。例如[陸軍軍種教官須穿著](../Page/陸軍.md "wikilink")[綠色](../Page/綠色.md "wikilink")[軍便服](../Page/軍便服.md "wikilink")，[空軍軍種的女軍訓教官則須穿著](../Page/空軍.md "wikilink")[藍色制服及](../Page/藍色.md "wikilink")[窄裙](../Page/窄裙.md "wikilink")，而[海軍陸戰隊轉任的男教官需要穿](../Page/中華民國海軍陸戰隊.md "wikilink")[領帶時](../Page/領帶.md "wikilink")，則須使用[黃色領帶](../Page/黃色.md "wikilink")。

### 人數

根據教育部資料，2016年時全國共有約3,500名軍訓教官，其中2600名服務於高中職、900名於大專院校，大學之中有14所無教官\[8\]。在沒有教官的14所大學中，其中9所是2006年後新設學校或宗教大學，因此在設立時並無核給教官名額。\[9\]
蔡政府上台後，2017年起，高中、大學教官開始「遇缺不補」，待其離退後人數慢慢遞減，2018年全臺灣教官仍有3,005人。

### 遴選

經過多年沿革，今中華民國高級中學以上學校的軍訓教官遴選，除筆試外還須經過跨部會的「軍訓教官遴選小組」嚴格審核。除了詳訂由經國防部遴選、介派及遷調外，也仍強調為現役軍官的身份。一般來說，其最主要身份資格為：

  - 服軍官役期滿八年以上。
  - 役期在各階現役最大年限前二年以上。
  - 具學士以上學位，中校以上各階並應具[碩士以上學位或指參學資](../Page/碩士.md "wikilink")。
  - 最近三年考績績等均評列甲等以上。
  - [智力測驗成績在一百分以上者](../Page/智力測驗.md "wikilink")。
  - 最近五年內，未曾受[有期徒刑判決確定](../Page/有期徒刑.md "wikilink")、[懲戒處分或一次懲處達](../Page/懲戒處分.md "wikilink")[大過一次以上處分](../Page/大過.md "wikilink")。
  - 最近三年內，未曾因體罰部屬、擅離職守、不貫徹命令、財務操守或不正當男女關係等事由，受一次懲處達記過一次以上處分。
  - 六個月內，經[國軍醫院依](../Page/國軍醫院.md "wikilink")[國軍人員體格分類作業程序](../Page/國軍人員體格分類作業程序.md "wikilink")，體格檢查核判體格為編號一或二。
  - 大學軍訓總教官為[少將或](../Page/少將.md "wikilink")[上校](../Page/上校.md "wikilink")，其餘教官為[上校](../Page/上校.md "wikilink")、[中校](../Page/中校.md "wikilink")、[少校](../Page/少校.md "wikilink")。
  - 高中高職軍訓主任教官為中校軍階，其餘教官為[中校](../Page/中校.md "wikilink")、[少校](../Page/少校.md "wikilink")、[上尉](../Page/上尉.md "wikilink")。

## 出身名人

  - [湯曜明](../Page/湯曜明.md "wikilink")：[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[一級上將](../Page/一級上將.md "wikilink")，曾任[實踐大學軍訓教官](../Page/實踐大學.md "wikilink")，回軍後曾任[國防部長](../Page/中華民國國防部部長.md "wikilink")、[參謀總長](../Page/中華民國參謀總長.md "wikilink")、[陸軍總司令](../Page/中華民國陸軍司令.md "wikilink")、[陸軍副](../Page/陸軍.md "wikilink")[總司令](../Page/中華民國陸軍司令.md "wikilink")、[陸軍八軍團司令](../Page/陸軍八軍團.md "wikilink")、[陸軍](../Page/陸軍.md "wikilink")43軍長、[陸軍參謀長](../Page/國防部陸軍司令部.md "wikilink")、[陸軍](../Page/陸軍.md "wikilink")[步兵](../Page/步兵.md "wikilink")226師長。是[中華民國國軍歷史上第一位](../Page/中華民國國軍.md "wikilink")[臺灣本省籍](../Page/本省人.md "wikilink")、[客家人出身的](../Page/客家人.md "wikilink")[陸軍總司令](../Page/陸軍總司令.md "wikilink")、[參謀總長](../Page/參謀總長.md "wikilink")（[一級上將](../Page/一級上將.md "wikilink")）與[國防部長](../Page/國防部長.md "wikilink")。
  - [戴伯特](../Page/戴伯特.md "wikilink")：[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[二級上將](../Page/二級上將.md "wikilink")，曾任[臺北市立成功高中軍訓教官](../Page/臺北市立成功高中.md "wikilink")，回軍後曾任[陳水扁](../Page/陳水扁.md "wikilink")[總統府戰略顧問](../Page/總統府戰略顧問.md "wikilink")、[聯勤司令](../Page/聯勤.md "wikilink")、[國防部軍情局局長](../Page/國防部軍情局.md "wikilink")、[陸軍副](../Page/陸軍.md "wikilink")[總司令](../Page/中華民國陸軍司令.md "wikilink")、[陸軍金防部司令](../Page/金門防衛司令部.md "wikilink")、[陸軍六軍團司令](../Page/陸軍六軍團.md "wikilink")、[陸軍後勤司令](../Page/陸軍後勤指揮部.md "wikilink")、[陸軍砲校校長](../Page/陸軍砲兵訓練指揮部.md "wikilink")、[陸軍人事署長](../Page/陸軍.md "wikilink")、[陸軍計畫署長](../Page/陸軍.md "wikilink")、[陸軍](../Page/陸軍.md "wikilink")[步兵](../Page/步兵.md "wikilink")127師長、[陸軍花防部參謀長](../Page/陸軍花東防衛指揮部.md "wikilink")、[陸軍](../Page/陸軍.md "wikilink")[步兵](../Page/步兵.md "wikilink")58師防砲連連長。

## 註釋

## 參考資料

<div style="font-size: 90%">

  -
  - 中華民國，《[高級中學法](../Page/高級中學法.md "wikilink")》

  - 中華民國，《[高級中等以上學校軍訓教官資格遴選介派遷調辦法](../Page/高級中等以上學校軍訓教官資格遴選介派遷調辦法.md "wikilink")》

  - [謝元熙](../Page/謝元熙.md "wikilink")，《軍訓教官輔導學生的基本觀念與要領》，臺北，1987年，中華民國教育部軍訓處。

  - [吳鼎](../Page/吳鼎.md "wikilink")，《輔導原理》，1983年，臺北，[國立編譯館](../Page/國立編譯館.md "wikilink")。

  - [吳建國](../Page/吳建國.md "wikilink")，《軍訓教官校園角色的定位與調適》，臺北，1988年，教育部軍訓處。

  - 中華民國國防部：《軍訓教官服裝規定》、《軍訓教官服裝製補作業計畫》、《陸海空軍服制條例》

  - [軍訓教官工作性質之認識](http://tw.myblog.yahoo.com/jw!cFx1Vn6eAAF2Oxbc8YqdAg--/article?mid=151)

</div>

[D](../Category/台灣國民教育.md "wikilink")
[D](../Category/中华民国高等教育.md "wikilink")
[職](../Category/中華民國軍事.md "wikilink")
[C](../Category/中華民國教育部.md "wikilink")
[Category:1928年建立](../Category/1928年建立.md "wikilink")
[Category:2023年廢除](../Category/2023年廢除.md "wikilink")

1.
2.  [國軍志願役軍官俸額表 -
    國立清華大學人事室](http://person.web.nthu.edu.tw/files/14-1138-7982,r913-1.php)
3.
4.
5.
6.
7.  [【教官再見】教育部：4年內教官全部退出校園](http://www.appledaily.com.tw/realtimenews/article/new/20170114/1034945/)
8.  [自由時報-教部次長：努力讓教官在5年內退出校園](http://news.ltn.com.tw/news/life/breakingnews/1789534)
9.   柯必安
    資深教育記者|accessdate=2016-08-16|date=2016-08-11|last=Media|first=平傳媒
    Fair|language=zh-TW}}