**載寧
龍二**（，）是[日本的男性](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。出身於[廣島縣](../Page/廣島縣.md "wikilink")[東廣島市](../Page/東廣島市.md "wikilink")，身高182公分，血型O型。他於多年前在[東廣島市立八本松中學校](../Page/東廣島市立八本松中學校.md "wikilink")、[近畿大學附屬東廣島高等學校及](../Page/近畿大學附屬東廣島中學校・高等學校.md "wikilink")[明治大學](../Page/明治大學.md "wikilink")[政治經濟學部畢業](../Page/政治經濟學部.md "wikilink")。現在他是[Horipro旗下的男藝人](../Page/Horipro.md "wikilink")。而在2002年，接拍[電視劇系列](../Page/電視劇.md "wikilink")《[極道鮮師](../Page/極道鮮師.md "wikilink")》，飾演仙石龍二；以及在2004年，接拍[特攝](../Page/特攝.md "wikilink")[超級戰隊系列的](../Page/超級戰隊系列.md "wikilink")《[特搜戰隊刑事連者](../Page/特搜戰隊刑事連者.md "wikilink")》，飾演刑事紅（赤座伴番）。後改藝名為「さいねい
龍二」（與本名讀法相同）。

## 參與作品

### 電視劇

  - [極道鮮師](../Page/極道鮮師.md "wikilink")（2002年，[日本電視台](../Page/日本電視台.md "wikilink")）
    - 飾演 仙石龍二
  - 極道鮮師SPECIAL（2003年3月26日，日本電視台） - 飾演 仙石龍二
  - [富豪刑事](../Page/富豪刑事.md "wikilink")（2005年，朝日電視台） - 飾演 西島誠一
  - [はるか17](../Page/はるか17.md "wikilink")（2005年，朝日電視台） - 飾演 森山
  - [Ns'あおい](../Page/Ns'あおい.md "wikilink")（2006年，[富士電視台](../Page/富士電視台.md "wikilink")）
    - 飾演 パンチョ羽澤
  - Ns'あおい スペシャル（2006年9月26日，富士電視台） - 飾演 パンチョ羽澤
  - 富豪刑事デラックス（2006年，[朝日放送](../Page/朝日放送.md "wikilink")・朝日電視台） - 飾演 西島誠一
  - [土曜ワイド劇場](../Page/土曜ワイド劇場.md "wikilink")30周年特別企画
    XXⅢ（2006年10月8日，朝日電視台） - 飾演 成宮俊作
  - [桜2号](../Page/桜2号.md "wikilink")（2006年，朝日放送） - 飾演 朝見鐵矢
  - [火曜ドラマゴールド](../Page/火曜ドラマゴールド.md "wikilink")
    潜入刑事らんぼう2（2007年2月6日，日本電視台） - 飾演
    三好亮太（源氏名：亮太）
  - [月曜ゴールデン](../Page/月曜ゴールデン.md "wikilink")「[和田アキ子殺人事件](../Page/和田アキ子殺人事件.md "wikilink")」（2007年2月12日，[TBS](../Page/東京廣播公司.md "wikilink")）
    - 飾演 [田代さやかのマネージャー](../Page/田代さやか.md "wikilink")
  - [折翼的天使第](../Page/折翼的天使.md "wikilink")4夜「商品」（2007年3月1日，富士電視台） - 飾演
    澤田啓介
  - [恋のから騒ぎドラマスペシャル](../Page/恋のから騒ぎ.md "wikilink")Ⅳ「ナマイキな女」（2007年11月30日，日本電視台）
  - [腐女子デカ](../Page/腐女子デカ.md "wikilink")（2008年，朝日電視台） - 飾演 麻宮翔太
  - [新・科捜研の女SP](../Page/新・科捜研の女.md "wikilink")「真夜中の大爆発！狙われた京都サミット\!\!SPがSPを射殺\!?
    」（2008年3月13日，朝日電視台）
  - [ドラマ8](../Page/ドラマ8.md "wikilink")・[七瀬ふたたび](../Page/七瀬ふたたび.md "wikilink")（2008年10月9日－，[NHK](../Page/日本放送協會.md "wikilink")）
    - 飾演 江藤刑事
  - [SPEC～警視廳公安部公安第五課
    未詳事件特別對策係事件簿～](../Page/SPEC～警視廳公安部公安第五課_未詳事件特別對策係事件簿～.md "wikilink")
    (2010年10月8日- 、TBS) - 飾演 豬俁宗次

### 綜藝節目

  - [虎之門](../Page/虎之門.md "wikilink")（2004年，朝日電視台） - 91代目MC
  - [XX Pro](../Page/XX_Pro.md "wikilink")（2005年4月－2006年3月，朝日電視台） - 飾演
    藝能Production「×× Pro」社長
  - [ひろしま菜'S](../Page/ひろしま菜'S.md "wikilink")（2005年4月－，[廣島HOME電視台](../Page/廣島HOME電視台.md "wikilink")）
  - [ひらめ筋GOLD](../Page/ひらめ筋GOLD.md "wikilink")（2005年10月－2006年3月，日本電視台） -
    SAMURAI Heroes
  - 優香・中島知子のニューカレドニア プチセレ楽園リゾート紀行（2006年9月17日、朝日電視台）
  - [TV☆Lab](../Page/TV☆Lab.md "wikilink")（2007年，[BS富士](../Page/BS富士.md "wikilink")）
    - 都市傳說特搜部

### 電影

  - [棒たおし\!](../Page/棒たおし!.md "wikilink")（2003年） - 飾演 鴨志田
  - [特搜戰隊刑事連者 THE MOVIE Full Blast
    Action](../Page/特搜戰隊刑事連者_THE_MOVIE_Full_Blast_Action.md "wikilink")（2004年）
    - 飾演 DekaRed／赤座伴番
  - カチコミ刑事 オンドリャー\!大捜査線 心斎橋を封鎖せよ（2006年） - 飾演 真上正義
  - [貓目小僧](../Page/貓目小僧_\(電影\).md "wikilink")（2006年） - 飾演 關根雄次
  - [網球王子](../Page/網球王子.md "wikilink")（2006年5月13日公開） - 飾演
    [跡部景吾](../Page/跡部景吾.md "wikilink")
  - 山路飄移2.0（2006年7月1日公開） - 飾演 東雄一郎
  - むずかしい恋（2008年）
  - [劇場版 幪面超人Wizard in Magic
    Land](../Page/劇場版_幪面超人Wizard_in_Magic_Land.md "wikilink")
    - 飾演 近衞兵隊長 / 幪面超人Mage

### 特攝

  - [特搜戰隊刑事連者](../Page/特搜戰隊刑事連者.md "wikilink")（2004年－2005年，[朝日電視台](../Page/朝日電視台.md "wikilink")）
    - 飾演 刑事紅／赤座伴番
  - [海賊戰隊豪快者](../Page/海賊戰隊豪快者.md "wikilink")（2011年
    第5話，[朝日電視台](../Page/朝日電視台.md "wikilink")） - 飾演
    赤座伴番
  - [非公認戰隊秋葉原連者](../Page/非公認戰隊秋葉原連者.md "wikilink")（2012年
    第2話，[朝日電視台](../Page/朝日電視台.md "wikilink")） - 飾演 赤座伴番
  - [宇宙戰隊九連者](../Page/宇宙戰隊九連者.md "wikilink")（2017年
    第18話，[朝日電視台](../Page/朝日電視台.md "wikilink")） - 飾演
    刑事紅／赤座伴番

### 動畫(配音)

  - [三國演義](../Page/三國演義_\(動畫\).md "wikilink") - 飾演
    [趙雲](../Page/趙雲.md "wikilink")

### DVD

  - [特搜戰隊DekaRanger VS
    AbaRanger](../Page/特搜戰隊DekaRanger_VS_AbaRanger.md "wikilink")（2005年，[東映](../Page/東映.md "wikilink")）
    - 飾演 DekaRed／赤座伴番
  - [魔法戰隊MagiRanger VS
    DekaRanger](../Page/魔法戰隊MagiRanger_VS_DekaRanger.md "wikilink")（2006年，東映）
    - 飾演 DekaRed／赤座伴番
  - [超忍者隊INAZUMA\!](../Page/超忍者隊INAZUMA!.md "wikilink")（2006年，東映）- 飾演
    倉田宮敬直
  - [超忍者隊INAZUMA\!\!SPARK](../Page/超忍者隊INAZUMA!!SPARK.md "wikilink")（2007年，東映）-
    飾演 倉田宮敬直

### PV

  - [TWILIGHT](../Page/Twilight_\(GOING_UNDER_GROUND\).md "wikilink")（2003年，[GOING
    UNDER GROUND](../Page/GOING_UNDER_GROUND.md "wikilink")）
  - Triangle Life（2006年，[オオゼキタク](../Page/オオゼキタク.md "wikilink")）

### 電台

  - [ハイパーナイト
    〜Men's魂〜メンたま](../Page/HYPER_NIGHT.md "wikilink")（2003年秋－2004年秋・[CBC
    Radio](../Page/中部日本放送.md "wikilink")）

### 舞台

  - [山口百惠トレビュートミュージカル](../Page/山口百惠.md "wikilink")『プレイバックpart2〜屋上の天使』（2005年）-
    飾演 ガブリエル（Tameo）
  - クイックドロウ（2007年）- 飾演 [Billy The
    Kid](../Page/Billy_The_Kid.md "wikilink")
  - [最遊記歌劇伝 -Go to the West-](../Page/最遊記.md "wikilink")（2008年） - 飾演 豬八戒
  - [七武士](../Page/七武士_\(動畫\).md "wikilink")（2008年） - 飾演 シチロージ
  - [鐵人28號](../Page/鐵人28號.md "wikilink")（2009年）

### CM

  - [ORONAMIN C](../Page/ORONAMIN_C.md "wikilink")（2003年）
  - [日清合味道杯麵](../Page/合味道.md "wikilink")（2004年）
  - [牛角](../Page/牛角.md "wikilink")（2004年，只限特搜戰隊刑事連者播映時間）

## 關連項目

  - [日本演員](../Page/日本演員.md "wikilink")

[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:廣島縣出身人物](../Category/廣島縣出身人物.md "wikilink")
[Category:Horipro](../Category/Horipro.md "wikilink")
[Category:超級戰隊系列主演演員](../Category/超級戰隊系列主演演員.md "wikilink")