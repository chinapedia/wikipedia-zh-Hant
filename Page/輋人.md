[HKWoCheEstoverview_20070828.jpg](https://zh.wikipedia.org/wiki/File:HKWoCheEstoverview_20070828.jpg "fig:HKWoCheEstoverview_20070828.jpg")[沙田一屋邨](../Page/沙田.md "wikilink")[禾輋邨的](../Page/禾輋邨.md "wikilink")「輋」字之起源與輋族的耕作方法有關。\]\]
**人**或**輋民**是[中國](../Page/中國.md "wikilink")[廣東和](../Page/廣東.md "wikilink")[江西](../Page/江西.md "wikilink")[瑤族的一個分支](../Page/瑤族.md "wikilink")。**輋**字以“**山**”“**大**”为义符，“**车**”为声符\[1\]。

## 歷史

輋民可能來源自瑤族。

[文天祥在](../Page/文天祥.md "wikilink")《知潮州寺丞東巖先生洪公行狀》說：「[潮與](../Page/潮州.md "wikilink")[漳](../Page/漳州.md "wikilink")、汀接壤，鹽寇**輋**民群聚」。

輋民來贛之原由，《[江西通志稿](../Page/江西通志稿.md "wikilink")》中說道：「明以前難為確定之論斷，在明代則為自廣東移徙而來。輋徭即古代山越，在廣東東部散處最廣。」《江西通志稿》又說：「輋人之主要目的在求得耕地以遂其生耳，不得平原之地則但求山地，以施其刀耕火種之勤，其情殊可憫焉。」守仁和邢絢韻詩說：「處處山田盡入輋，可憐黎庶半無家。」。

[清代大學問家](../Page/清代.md "wikilink")[李調元於](../Page/李調元.md "wikilink")《齋瑣錄》說：「輋音斜，耕山嶽之地曰輋」。[民國三十八年史學家](../Page/民國.md "wikilink")[吳宗慈主編的](../Page/吳宗慈.md "wikilink")《江西通志考》的〈江西輋族考〉中說：「夫輋即苗即徭也。」，又說：「畬徭當以廣東東部為最多，其先世民族實為[山越](../Page/山越.md "wikilink")。」

宋代時居於[香港](../Page/香港.md "wikilink")[大嶼山一帶的輋民鹽戶曾在](../Page/大嶼山.md "wikilink")[南宋](../Page/南宋.md "wikilink")[慶元三年](../Page/慶元.md "wikilink")（1197年）的一次暴亂中襲擊[廣州地區](../Page/廣州.md "wikilink")，當時被稱為[大奚山傜亂](../Page/大奚山鹽民起義.md "wikilink")。[明](../Page/明朝.md "wikilink")[正德十二年丁丑](../Page/正德.md "wikilink")（1517年）巡撫南贛都御史[王守仁曾重兵擊剿](../Page/王守仁.md "wikilink")，此畬徭之患得以解除。倖存的輋民緊縮於深山，陸續往[閩](../Page/閩.md "wikilink")、[粵山區遷徙](../Page/粵.md "wikilink")，[贛](../Page/贛.md "wikilink")（江西）境內的輋民幾乎消聲匿跡。王守仁的《王文成公全書》〈請立崇義縣治疏〉說：「其初畬賊原系廣東流來，先年奉巡撫都御史金澤行令安插於址。」這同時說明了廣東畬民是由東北遷徙而入江西的。

在千多年來與漢族的融合及同化下，廣東沿海一帶輋人的身份幾乎已絕跡，輋人可說是已融入其他民族裡，一些輋人成為[客家人和](../Page/客家人.md "wikilink")[蜑家人](../Page/蜑家.md "wikilink")。

## 耕作方法與生活

輋族遷徙到山上定居後，他們先選擇較平坦的斜坡，運用遷移式（或稱「[刀耕火耨](../Page/刀耕火耨.md "wikilink")」）的方式，以[火將](../Page/火.md "wikilink")[野草](../Page/野草.md "wikilink")、[樹木燒燬](../Page/樹木.md "wikilink")，再利用所造成的[草木灰作](../Page/草木灰.md "wikilink")[肥料](../Page/肥料.md "wikilink")，然後將[禾稻種子撒在土地上](../Page/禾稻.md "wikilink")，任由它們各自生長，至禾稻成熟時收割，這種在斜坡上的耕作方式稱為「輋」或「畬」，與田種有別。部份輋族曾參與鹽的製造及貿易，他們在[鹽田透過蒸發](../Page/鹽田.md "wikilink")[海水提取](../Page/海水.md "wikilink")[食鹽](../Page/食鹽.md "wikilink")。

## 語言

輋族說的方言是[苗瑤語族的一種](../Page/苗瑤語族.md "wikilink")。在族群融合的過程中，他們的母語慢慢地和[客家話和](../Page/客家話.md "wikilink")[畲語融合](../Page/畲語.md "wikilink")。

## 地名

近代歷史學者[徐松石的](../Page/徐松石.md "wikilink")《粵江流域人民史》〈廣東畬人節〉曾記載：「畬徭即古代所謂山越，在廣東東部散處最廣，……現仍有許多輋字地名。……所以在廣東區域內所謂舊越人或先來之土著當系輋徭部族。」

今天，輋族在沿海地區生活過痕跡可謂已不能復見，但從一些地名仍然保留著他們存在的證明：

  - 「**峒**」或「**洞**」在[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")，特別是[船灣和](../Page/船灣.md "wikilink")[西貢北約一帶多處被用作地名](../Page/西貢北約.md "wikilink")，然而這些地方一帶並無洞穴，這是因為這些字在輋族中指山上的居處，非指山洞。例如：

<!-- end list -->

1.  [上水](../Page/上水.md "wikilink")**[古洞](../Page/古洞.md "wikilink")**
2.  [大埔](../Page/大埔_\(香港\).md "wikilink")**[沙螺洞](../Page/沙螺洞.md "wikilink")**
3.  [大埔](../Page/大埔_\(香港\).md "wikilink")**[洞梓](../Page/洞梓.md "wikilink")**
4.  [粉嶺](../Page/粉嶺.md "wikilink")**[萊峒](../Page/萊峒.md "wikilink")**
5.  [十四鄉](../Page/十四鄉.md "wikilink")**[大峒](../Page/大峒.md "wikilink")**
6.  **[觀音峒](../Page/觀音峒.md "wikilink")**
7.  **[黃地峒](../Page/黃地峒.md "wikilink")**
8.  **[雞麻峒](../Page/雞麻峒.md "wikilink")**
9.  **[南山峒](../Page/南山峒.md "wikilink")**
10. **[尖光峒](../Page/尖光峒.md "wikilink")**
11. **[鹿湖峒](../Page/鹿湖峒.md "wikilink")**

<!-- end list -->

  - 「**輋**」指則他們使用的耕作方法，也可以指居住在山上的輋族居民。例如香港的：

<!-- end list -->

1.  [沙田](../Page/沙田.md "wikilink")**[禾輋](../Page/禾輋.md "wikilink")、[大輋](../Page/大輋.md "wikilink")**
2.  [打鼓嶺](../Page/打鼓嶺.md "wikilink")**[坪輋](../Page/坪輋.md "wikilink")**
3.  [東平洲](../Page/東平洲.md "wikilink")**[輋腳下](../Page/輋腳下.md "wikilink")**
4.  [大嶼山](../Page/大嶼山.md "wikilink")**[藍輋](../Page/藍輋.md "wikilink")、[大輋峒](../Page/大輋峒.md "wikilink")**
5.  [西貢](../Page/西貢.md "wikilink")**[菠蘿輋](../Page/菠蘿輋.md "wikilink")、[大輋嶺](../Page/大輋嶺.md "wikilink")、[東龍二輋](../Page/東龍二輋.md "wikilink")、[下輋](../Page/下輋_\(西貢\).md "wikilink")、[莫遮輋](../Page/莫遮輋.md "wikilink")、[橫輋](../Page/橫輋.md "wikilink")、[輋徑篤](../Page/輋徑篤.md "wikilink")、[輋頂村](../Page/輋頂村.md "wikilink")**
6.  [林村](../Page/林村.md "wikilink")**[大芒輋](../Page/大芒輋.md "wikilink")（[大陽輋](../Page/大陽輋.md "wikilink")）、[大輋地](../Page/大輋地.md "wikilink")**
7.  [大埔](../Page/大埔_\(香港\).md "wikilink")**[圍頭村](../Page/圍頭村_\(大埔\).md "wikilink")[輋地](../Page/輋地_\(大埔\).md "wikilink")、[輋腳下](../Page/輋腳下.md "wikilink")、[輋下](../Page/輋下.md "wikilink")、[大輋嶺墩](../Page/大輋嶺墩.md "wikilink")**
8.  [八鄉](../Page/八鄉.md "wikilink")**[上輋](../Page/上輋.md "wikilink")、[下輋](../Page/下輋_\(八鄉\).md "wikilink")**
9.  [荃灣](../Page/荃灣.md "wikilink")**[大輋峒](../Page/大輋峒.md "wikilink")**

## 輋族、瑤族與畬族

《說蠻》載：「輋，巢居也」，「輋人亦徭種也」。《南越筆記》說：「賦以刀為準者曰徭，徭所止曰輋，亦曰輋，是為畬蠻之類。」。《嶺表紀蠻》說：「畬與輋同音，蓋以同一種族，故音同字異」。[明](../Page/明朝.md "wikilink")[清時期](../Page/清.md "wikilink")[顧炎武在](../Page/顧炎武.md "wikilink")《天下郡國利病書》稱：「輋當作畬，實錄謂之畬蠻。」。[屈大均](../Page/屈大均.md "wikilink")《[廣東新語](../Page/廣東新語.md "wikilink")》亦指出輋民與[傜民本為同族](../Page/傜民.md "wikilink")，其區別在於前者由山長管理，不繳[賦稅](../Page/賦稅.md "wikilink")；後者接受官府管轄，須繳稅輕[徭](../Page/徭役.md "wikilink")\[2\]。

可見，「輋族」、「畬族」與[瑤族同為一類](../Page/瑤族.md "wikilink")。「輋族」與「畬族」的含義是有差異的，是指兩個不同之族群。畬族多指[福建](../Page/福建.md "wikilink")、[江西畬族](../Page/江西.md "wikilink")，是「刀耕火種，崖棲(洞居)谷汲」的土著居民，又稱峒蠻、峒獠。

[顧炎武在](../Page/顧炎武.md "wikilink")《天下郡國利病書》又說：「粵人以山林中結竹木障覆居息為輋，故稱徭所止曰輋。自信為狗王后。家有畫像，犬首人服，歲時祝祭。其姓為[盤](../Page/盤姓.md "wikilink")、[藍](../Page/藍姓.md "wikilink")、[雷](../Page/雷姓.md "wikilink")、[鍾](../Page/鍾姓.md "wikilink")、[苟](../Page/苟姓.md "wikilink")，自相婚」。「輋」字在此作為族稱是側重於其居住的形式，指在廣東山林裡搭棚而居的族群。如此說明了[明代時廣東一帶輋族的](../Page/明代.md "wikilink")[生活](../Page/生活.md "wikilink")、[習俗](../Page/習俗.md "wikilink")、[信仰與及](../Page/信仰.md "wikilink")[姓氏](../Page/姓氏.md "wikilink")，均與現代的畬族相若，可證畬族與輋族、瑤族三者之間的密切關係。

## 參考文獻

## 參見

  - [中國少數民族列表](../Page/中國少數民族列表.md "wikilink")
  - [瑤族](../Page/瑤族.md "wikilink")
  - [畬族](../Page/畬族.md "wikilink")
  - [盤瓠](../Page/盤瓠.md "wikilink")

[Category:中華民族](../Category/中華民族.md "wikilink")
[Category:廣東族群](../Category/廣東族群.md "wikilink")
[Category:香港歷史](../Category/香港歷史.md "wikilink")

1.  [畲 - 中央大學客家學院 -
    國立中央大學](http://hakka.ncu.edu.tw/hakkastudy/periodical/wordpress/wp-content/uploads/2018/07/8-2-2.pdf)
2.  屈大均。輋人\[A\]。廣東新語\[M\]。北京：中華書局，1997。