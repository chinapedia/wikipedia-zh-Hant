[Antigen_presentation_alt.svg](https://zh.wikipedia.org/wiki/File:Antigen_presentation_alt.svg "fig:Antigen_presentation_alt.svg")

**辅助T细胞**（），又稱為**助手型T细胞**，是一种[T细胞](../Page/T细胞.md "wikilink")（[白細胞的一种](../Page/白細胞.md "wikilink")），它的表面有[抗原受体](../Page/抗原.md "wikilink")，可以辨識[抗原提呈細胞的MHC](../Page/抗原提呈細胞.md "wikilink")-II类分子呈獻的抗原片段。

一旦受到抗原刺激，Th细胞就會增殖和分化成作用性Th细胞（effector Th）和记忆Th細胞（memory Th）。

  - 活化Th细胞分泌[细胞因子](../Page/细胞因子.md "wikilink")、[蛋白质或](../Page/蛋白质.md "wikilink")[缩氨酸用来調控其它的](../Page/缩氨酸.md "wikilink")[免疫細胞](../Page/免疫細胞.md "wikilink")；最通常是[白介素](../Page/白介素.md "wikilink")（Interleukin），这可以使得[B细胞分化成](../Page/B细胞.md "wikilink")[浆细胞](../Page/浆细胞.md "wikilink")。
  - 记忆Th细胞对第一次接触的抗原进行特化，这可以在被称为“[二次免疫应答](../Page/二次免疫应答.md "wikilink")”中起作用。

很多Th细胞在细胞表面表現CD4蛋白質。这种亲和力使得Th细胞和靶细胞可以在抗原特化反应中紧密的贴在一起。带有CD4分子的Th细胞称为CD4+
T细胞。輔助T细胞主要可區分為Th1、Th2、Th17及Thαβ等四種。

## TH1輔助細胞

TH1輔助細胞主要作用為對抗細胞內[細菌及](../Page/細菌.md "wikilink")[原蟲的免疫反應](../Page/原蟲.md "wikilink")，其主要為白介素12（IL-12）所驅動誘發，其主要的執行的細胞因子是伽馬干擾素（IFNγ），其最重要的執行細胞為[巨噬細胞](../Page/巨噬細胞.md "wikilink")（Macrophage），另外還有殺手CD8-T細胞、產生IgG的[B細胞以及分泌IFNγ的CD](../Page/B細胞.md "wikilink")4-T細胞等、其主要的轉錄因子為STAT4，另外還有T-bet等等
。CD4-T細胞分泌的IFNγ會活化巨噬細胞，使其能夠吞噬並消化掉細胞內細菌及原蟲，另外IFNγ也會活化iNOS而放出等[自由基而直接殺死細胞內細菌及原蟲](../Page/自由基.md "wikilink")。TH1免疫反應對應的是*第四型自體免疫疾病*（Type4
delayed type
hypersensitivity），也就是過度的TH1表現將會導致巨噬細胞自體免疫疾病，比如[麻風病或](../Page/麻風病.md "wikilink")[結核菌素過度反應以及](../Page/結核菌.md "wikilink")[第一型糖尿病等都屬此類](../Page/第一型糖尿病.md "wikilink")。\[1\]

## TH2輔助細胞

TH2輔助細胞主要作用為對抗細胞外多細胞寄生蟲的免疫反應，主要為[白介素4](../Page/白介素4.md "wikilink")（IL-4）所驅動誘發，而主要作用的細胞因子是[IL-4](../Page/IL-4.md "wikilink")、[IL-5和](../Page/IL-5.md "wikilink")[IL-13](../Page/IL-13.md "wikilink")；其最重要的執行細胞為[肥大細胞](../Page/肥大細胞.md "wikilink")（Mast
cell）、[嗜酸細胞](../Page/嗜酸性粒細胞.md "wikilink")（Eosinophil）及[嗜鹼細胞](../Page/嗜鹼性粒細胞.md "wikilink")（Basophil）；另外還有產生[IgE的](../Page/IgE.md "wikilink")[B細胞以及分泌](../Page/B細胞.md "wikilink")[IL-4](../Page/IL-4.md "wikilink")/[IL-5的](../Page/IL-5.md "wikilink")[CD4](../Page/CD4.md "wikilink")-[T細胞等](../Page/T細胞.md "wikilink")；其主要的[轉錄因子為](../Page/轉錄因子.md "wikilink")[STAT6和](../Page/STAT6.md "wikilink")[GATA](../Page/GATA.md "wikilink")。CD4-T細胞分泌的IL-5會活化嗜酸細胞，使其能夠攻擊細胞外寄生蟲；另外IL-4和IgE會活化肥大細胞而放出[組織胺](../Page/組織胺.md "wikilink")（histamine）、[血清素](../Page/血清素.md "wikilink")（serotonin）等，造成氣管收縮、腹瀉及腸蠕動而排出寄生蟲。TH2免疫反應對應的是*第一型過敏反應-IgE調節性反應和一般過敏*（Type1
IgE mediate hypersensitivity &
allergy），也就是過分的TH2激活將會導致[肥大細胞及](../Page/肥大細胞.md "wikilink")[嗜酸細胞過敏疾病](../Page/嗜酸性粒細胞.md "wikilink")，比如[過敏性鼻炎](../Page/過敏性鼻炎.md "wikilink")、[氣喘及](../Page/氣喘.md "wikilink")[異位性皮膚炎等都屬此類](../Page/異位性皮膚炎.md "wikilink")。\[2\]

## TH17輔助細胞

TH17輔助細胞主要作用為對抗細胞外[細菌及](../Page/細菌.md "wikilink")[黴菌的免疫反應](../Page/黴菌.md "wikilink")，其主要為白介素6（IL-6）及TGFβ所驅動誘發，其主要的執行的細胞因子是白介素1（IL-1）和白介素6（IL-6）以及TNFα，其最重要的執行細胞為[中性球](../Page/中性球.md "wikilink")（Neutrophil），另外還有產生IgG/IgA/IgM的[B細胞以及分泌IL](../Page/B細胞.md "wikilink")-17的CD4-[T細胞等](../Page/T細胞.md "wikilink")，其主要的[轉錄因子為STAT](../Page/轉錄.md "wikilink")3，另外還有RORγ等等。CD4-T細胞分泌的IL-17和TNFα會活化[中性球](../Page/中性球.md "wikilink")，使其能夠吞噬並消化掉細胞外細菌及黴菌，另外IL6等也會活化[補體反應而直接殺死細胞外細菌及黴菌](../Page/補體.md "wikilink")。TH17免疫反應對應的是*第三型過敏反應-免疫複合體及補體性反應*（Type3
Immune complex & complement
hypersensitivity），也就是過分的TH17激活將會導致中性球自體免疫疾病，比如[類風濕性關節炎或](../Page/類風濕性關節炎.md "wikilink")[超敏反應](../Page/超敏反應.md "wikilink")（Arthus
reaction）等都屬此類。\[3\]

## THαβ輔助細胞

THαβ輔助細胞主要作用為對抗細胞內病毒的免疫反應，其主要為阿爾發或貝他[干擾素](../Page/干擾素.md "wikilink")（IFNα/β）所驅動誘發，其主要的執行的細胞因子是白介素10（IL-10），其最重要的執行細胞為[自然殺手細胞](../Page/自然殺手細胞.md "wikilink")（NK
cells），另外還有殺手CD8-T細胞、產生IgG的[B細胞以及分泌IL](../Page/B細胞.md "wikilink")10的CD4-T細胞等，其主要的轉錄因子為STAT1和STAT3，另外還有IRF等。CD4-T細胞分泌的IL10會活化自然殺手細胞，使其能夠啟動[ADCC殺死受病毒感染之細胞](../Page/ADCC.md "wikilink")，使其发生凋亡而將細胞內[病毒的](../Page/病毒.md "wikilink")[DNA分解掉](../Page/DNA.md "wikilink")，另外IFNα/β也會抑制細胞[轉譯和](../Page/轉譯.md "wikilink")[轉錄活化以防止病毒繼續感染](../Page/轉錄.md "wikilink")。THαβ免疫反應對應的是*第二型過敏反應-抗體依賴性細胞毒性反應*（Type2
antibody dependent cytotoxic hypersensitivity），也就是過分
的THαβ激活將會導致[自然殺手細胞自體免疫疾病](../Page/自然殺手細胞.md "wikilink")，比如[重症肌無力等屬此類](../Page/重症肌無力.md "wikilink")。\[4\]

## 参见

  - [T细胞](../Page/T细胞.md "wikilink")
  - [细胞毒性T细胞](../Page/细胞毒性T细胞.md "wikilink")
  - [白细胞](../Page/白细胞.md "wikilink")
  - [人類免疫缺陷病毒](../Page/人類免疫缺陷病毒.md "wikilink")

## 參考文獻

{{-}}

[Category:T细胞](../Category/T细胞.md "wikilink")

1.
2.
3.
4.