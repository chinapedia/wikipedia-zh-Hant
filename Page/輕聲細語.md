《**輕聲細語**》（）是1998年的電影，由[勞勃·瑞福自導自演](../Page/勞勃·瑞福.md "wikilink")，改編自[尼古拉斯·埃文斯](../Page/尼古拉斯·埃文斯.md "wikilink")（[Nicholas
Evans](../Page/:en:Nicholas_Evans.md "wikilink")）1995年的同名[小說](../Page/小說.md "wikilink")。瑞福扮演主角
Tom
Booker，一個傑出的[訓馬師](../Page/訓馬師.md "wikilink")，被僱用照料一位在意外中受傷的少女（[史嘉蕾·喬韓森飾演](../Page/史嘉蕾·喬韓森.md "wikilink")）以及她的馬兒恢復健康。

[Category:1995年小說](../Category/1995年小說.md "wikilink")
[Category:1998年電影](../Category/1998年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:美國劇情片](../Category/美國劇情片.md "wikilink")
[Category:馬題材作品](../Category/馬題材作品.md "wikilink")
[Category:蒙大拿州取景電影](../Category/蒙大拿州取景電影.md "wikilink")
[Category:湯瑪斯·紐曼配樂電影](../Category/湯瑪斯·紐曼配樂電影.md "wikilink")