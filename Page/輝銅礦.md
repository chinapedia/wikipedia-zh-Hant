**輝銅礦**（Chalcocite）是一種[黑色到灰色的](../Page/黑色.md "wikilink")[礦物](../Page/礦物.md "wikilink")，[分子式為](../Page/分子式.md "wikilink")[Cu<sub>2</sub>S](../Page/硫化亞銅.md "wikilink")，通常可以在[沉積岩中發現](../Page/沉積岩.md "wikilink")。輝銅礦是煉[銅主要的礦砂](../Page/銅.md "wikilink")，因為[銅佔它的成分比例很高](../Page/銅.md "wikilink")(在輝銅礦礦裡有67%的原子是銅[原子](../Page/原子.md "wikilink")，而幾乎佔了80%的重量。)而且很容易就可以把[銅和](../Page/銅.md "wikilink")[硫分開](../Page/硫.md "wikilink")。目前輝銅礦主要是由在[英國的](../Page/英國.md "wikilink")[康沃爾和](../Page/康沃爾.md "wikilink")[大不列顛島供應](../Page/大不列顛島.md "wikilink")。目前為止，完全純的輝銅礦結晶非常少見。和[斑铜矿](../Page/斑铜矿.md "wikilink")、[石英](../Page/石英.md "wikilink")、[方解石](../Page/方解石.md "wikilink")、[蓝铜矿](../Page/蓝铜矿.md "wikilink")、[黄铜矿等共生于热液矿脉](../Page/黄铜矿.md "wikilink")。

## 參考資料

  - [mindat.org](http://www.mindat.org/min-962.html)
  - [Webmineral.com](http://webmineral.com/data/Chalcocite.shtml)
  - [Mineral
    Galleries](https://web.archive.org/web/20041013111535/http://mineral.galleries.com/minerals/sulfides/chalcoci/chalcoci.htm)

[Category:含铜矿物](../Category/含铜矿物.md "wikilink")
[Category:硫化物礦物](../Category/硫化物礦物.md "wikilink")