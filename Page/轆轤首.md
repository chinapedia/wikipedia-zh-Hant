**轆轤首**是長頸妖怪的一種，在[日本的](../Page/日本.md "wikilink")[江戶時代流傳甚廣](../Page/江戶時代.md "wikilink")，通常以女性的形象出現，特徵是脖子可以伸縮自如，與井邊打水時控制汲水吊桶的轆轤把，性質上頗為相似，故稱之為「轆轤首」。

與長頸妖怪相關的記載有干寶的《[搜神記](../Page/搜神記.md "wikilink")》，這本書是中國晉代著名的奇譚異聞錄，其中提到南方的「[落頭氏](../Page/落頭氏.md "wikilink")」一族，是這種妖怪的原型，又名「[飛頭蠻](../Page/飛頭蠻.md "wikilink")」（），此特徵與轆轤首並不相同，轆轤首的脖子會突然伸長，但不是像飛頭蠻那樣，整顆頭從脖子的部位與身體徹底分離。
[Hokusai_rokurokubi.jpg](https://zh.wikipedia.org/wiki/File:Hokusai_rokurokubi.jpg "fig:Hokusai_rokurokubi.jpg")
繪）\]\]

長頸妖怪依據精神的自主性，大致區分為兩種，一種可以隨自己的意念遊走，自由飛翔的長頸妖怪；另一種則是無法控制自己會飄到何處，在無意識狀態下浮遊的長頸妖怪。

可以隨自己的意念遊走的長頸妖怪，會潛入民宅將正在睡眠中的人勒斃，然後用尖銳的牙齒將對方啃蝕殆盡，甚至好幾隻聚在一起集體行動。由於長頸妖怪的頭部和身體經常分離，在脖子的地方，有纏繞著紅絲線的習慣，所以地方上有個不成文的禁忌「看到脖子纏紅線的女人千萬不能娶」，究其原因應該是害怕娶進門的老婆很可能是長頸妖怪的化身。

有些人並不知道自己就是長頸妖怪，所以是在[無意識的狀態下受到某種外力驅使](../Page/無意識.md "wikilink")，儘管白天看起來同一般人沒什麼兩樣，一旦入夜之後，等到眾人進入了夢鄉，才會顯出妖怪的真面目。首先是脖子突然伸長，接著頭部從脖子的部位和身體徹底分離，這時候頭部和身體會重新結合在一起，醒來後就像正常人一樣行動。本人往往不記得妖怪形態時看到了什麼，或做過了什麼事。

## 參看

  - [飛頭](../Page/飛頭.md "wikilink")
  - [飞头蛮](../Page/飞头蛮.md "wikilink")
  - [民俗学](../Page/民俗学.md "wikilink")
  - [妖怪](../Page/妖怪.md "wikilink")
  - [日本妖怪列表](../Page/日本妖怪列表.md "wikilink")
  - [搜神記](../Page/搜神記.md "wikilink")
  - [落頭氏](../Page/落頭氏.md "wikilink")
  - [倩女離魂](../Page/倩女離魂.md "wikilink")

## 相关登场作品

  - [仁王](../Page/仁王_\(游戏\).md "wikilink")

  - [鬼太郎](../Page/鬼太郎.md "wikilink")

  - [哆啦A梦](../Page/哆啦A梦.md "wikilink")

  - [靈異教師神眉](../Page/靈異教師神眉.md "wikilink")

  - [妖怪大战争](../Page/妖怪大战争.md "wikilink")

  - [地狱老师](../Page/地狱老师.md "wikilink")

  - [狐魅家族日記簿](../Page/狐魅家族日記簿.md "wikilink")

  - [天保異聞 妖奇士](../Page/天保異聞_妖奇士.md "wikilink")

  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")

  - [忍者戰隊隱連者](../Page/忍者戰隊隱連者.md "wikilink")

  - [侍戰隊真劍者](../Page/侍戰隊真劍者.md "wikilink")

  - [東方輝針城 ～ Double Dealing
    Character.](../Page/東方輝針城_～_Double_Dealing_Character..md "wikilink")

  - [百日紅](../Page/百日紅.md "wikilink")

  - [潮與虎](../Page/潮與虎.md "wikilink")

  - [我的姐姐是恋妖怪](../Page/我的姐姐是恋妖怪.md "wikilink")

  - [Re:從零開始的異世界生活](../Page/Re:從零開始的異世界生活.md "wikilink")

  - [火影忍者](../Page/火影忍者.md "wikilink")

  - [天舞法少女](../Page/天舞法少女.md "wikilink")

  - [神奇寶貝](../Page/神奇寶貝.md "wikilink")

  - [口袋精靈](../Page/口袋精靈.md "wikilink")

  - [口袋怪物](../Page/口袋怪物.md "wikilink")

  - [滑頭鬼之孫](../Page/滑頭鬼之孫.md "wikilink")

  -
  - [妖怪合租](../Page/妖怪合租.md "wikilink")

  - [假面骑士響鬼](../Page/假面骑士響鬼.md "wikilink")

  - [妖怪少女](../Page/妖怪少女.md "wikilink")

  - [飞头魔女](../Page/飞头魔女.md "wikilink")

## 参考资料

《日本妖怪奇谭》ISBN 9787224085297

[Category:日本妖怪](../Category/日本妖怪.md "wikilink")
[Category:神話傳說中的人型生物](../Category/神話傳說中的人型生物.md "wikilink")
[Category:神話傳說中的吸血生物](../Category/神話傳說中的吸血生物.md "wikilink")
[Category:女性傳說生物](../Category/女性傳說生物.md "wikilink")
[Category:神話傳說中的詭計者](../Category/神話傳說中的詭計者.md "wikilink")