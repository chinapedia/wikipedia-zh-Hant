**首次轟炸新加坡**是日軍對[新加坡的轟炸任務](../Page/新加坡.md "wikilink")，由[日本帝國海軍航空隊的](../Page/日本帝國海軍.md "wikilink")17架轟炸機實施，轟炸行動開始於大約早上4時，此時日軍剛剛在[馬來亞北部的](../Page/馬來亞.md "wikilink")[哥打巴魯實施登陸行動](../Page/日軍入侵馬來亞.md "wikilink")。

雖然已經嚮起空襲警報，但街道上仍然燈火通明，令領航員很容易便找到轟炸目標，盟軍的[高射炮立即還火](../Page/高射炮.md "wikilink")，[皇家海軍的](../Page/皇家海軍.md "wikilink")[戰列艦](../Page/戰列艦.md "wikilink")[*威爾斯親王號*及](../Page/威尔士亲王号战列舰.md "wikilink")[*卻敵號*亦有開火](../Page/声望级战列巡洋舰.md "wikilink")，但沒有任何日軍轟炸機被擊落，部份原因是雲層被遮蔽。

所有轟炸機安全返回在[法屬中南半島](../Page/法屬中南半島.md "wikilink")[西貢的空軍基地](../Page/胡志明市.md "wikilink")，島上3個空軍基地均受到轟炸，轟炸共做成61人死亡及超過700人受傷，受傷的大部份是駐島上的[印度第11步兵師的士兵](../Page/印度第11步兵師.md "wikilink")。

Image:SgBofors.jpg|[Bofors 40 mm
gun](../Page/Bofors_40_mm_gun.md "wikilink") displayed outside the
Central Manpower Base, Singapore Image:G3M Type 96 Attack Bomber Nell
G3M-26s.jpg|[Mitsubishi G3M](../Page/Mitsubishi_G3M.md "wikilink")
*Nell* bombers Image:Women & dead child.jpg|Two women grief over a child
killed in a Japanese air raid on 3 February 1942

## 外部链接

  - [First bomb raid on
    Singapore](https://web.archive.org/web/20080221144647/http://infopedia.nlb.gov.sg/articles/SIP_7_2005-01-25.html)

## 参考资料

  - Lee, G. B. (1992). Syonan: Singapore under the Japanese 1942-1945
    (pp. 18, 24). Singapore: Singapore Heritage Society.
  - Singapore: An illustrated history 1941 - 1984 (p.16). (1984).
    Singapore: Information Division, Ministry of Culture
  - Tan, B. L. (1996). The Japanese Occupation 1942 - 1945: A pictorial
    record of Singapore during the war (pp. 16, 26-27). Singapore: Times
    Editions

[B](../Category/第二次世界大戰太平洋戰場戰役.md "wikilink")
[Category:英國戰役](../Category/英國戰役.md "wikilink")
[Category:日本戰役](../Category/日本戰役.md "wikilink")