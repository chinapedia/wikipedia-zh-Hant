《**輪台诏**》是前89年（[征和四年](../Page/征和.md "wikilink")）[汉武帝所下達的](../Page/汉武帝.md "wikilink")[诏书](../Page/诏书.md "wikilink")，公開向臣民反省过错，使到[西汉的统治方针发生了急劇转变](../Page/西汉.md "wikilink")，重新回到了与民休息及重视发展经济的轨道，从而可以延续数十年国祚，避免了像[秦朝般迅速败亡的结局](../Page/秦朝.md "wikilink")。\[1\]

《輪台诏》是[中国历史上第一份内容丰富及保存完整的](../Page/中国历史.md "wikilink")[罪己诏](../Page/罪己诏.md "wikilink")，此后每当朝廷出现危机、遭受天灾及政权处于安危时，帝王多有发布罪己诏。

## 背景

[漢武帝.jpg](https://zh.wikipedia.org/wiki/File:漢武帝.jpg "fig:漢武帝.jpg")
汉武帝刘彻是[西汉第七位皇帝](../Page/西汉.md "wikilink")，雄才大略，建樹廣闊；惟到晚年，性格大變，迷信多疑。先建[明堂](../Page/明堂.md "wikilink")，後垒高坛，树“[太一](../Page/太一.md "wikilink")”尊神，舉辦顶礼膜拜，并且靡费巨资，多次[封禅出游](../Page/封禅.md "wikilink")，命令大批人民入海求助[蓬莱真神](../Page/蓬莱.md "wikilink")。为了通神求仙，誤信[方士](../Page/方士.md "wikilink")，將[宫廷被服都改變怪模怪样](../Page/宫廷.md "wikilink")，命人製造30丈高的[仙人](../Page/仙人.md "wikilink")[銅像](../Page/銅像.md "wikilink")，以搜集[甘露和玉屑饮之](../Page/甘露.md "wikilink")，以为可以长生不老。并且任用侫臣[江充](../Page/江充.md "wikilink")，最终酿成[巫蛊之祸](../Page/巫蛊之祸.md "wikilink")，逼死[太子](../Page/太子.md "wikilink")[刘据和](../Page/刘据.md "wikilink")[卫皇后](../Page/卫皇后.md "wikilink")，受诛连者数万人，導致政治混乱。

[经济上](../Page/经济.md "wikilink")，由于汉武帝连年对外用兵和肆意挥霍，国库空虚。汉武帝以[桑弘羊执掌全国财政](../Page/桑弘羊.md "wikilink")，将[盐](../Page/盐.md "wikilink")[铁实行](../Page/铁.md "wikilink")[垄断](../Page/垄断.md "wikilink")[专卖](../Page/专卖.md "wikilink")，并且出卖[爵位](../Page/爵位.md "wikilink")，允许以钱赎罪，使到[吏治腐败](../Page/吏治.md "wikilink")。广大贫苦农民不堪[官府和豪强的双重压榨](../Page/官府.md "wikilink")，于汉武帝统治的中后期接连爆发[起义](../Page/起义.md "wikilink")，并且愈演愈烈。

[军事上](../Page/军事.md "wikilink")，前90年，在贰师将军[李广利受命出兵五原](../Page/李广利.md "wikilink")（今[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[五原县](../Page/五原县.md "wikilink")）讨伐[匈奴的前夕](../Page/匈奴.md "wikilink")，丞相[刘屈牦与李广利合谋立昌邑王](../Page/刘屈牦.md "wikilink")[刘髆为太子](../Page/刘髆.md "wikilink")。后刘屈牦被[腰斩](../Page/腰斩.md "wikilink")，李广利妻被下狱。此时李广利正在乘胜追击，听到消息恐遭祸，欲再击匈奴取得胜利，以期汉武帝饶其不死。但是兵败，李广利只得投降匈奴。

以上种种打击使汉武帝心灰意冷，对自己过去的所作所为颇有悔意。前89年，[桑弘羊等人上书汉武帝](../Page/桑弘羊.md "wikilink")，建议在[轮台](../Page/轮台县.md "wikilink")（今[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[轮台县](../Page/轮台县.md "wikilink")）戍兵以备匈奴，汉武帝驳回桑等人的建议，下诏反思自己，又称“当今务在禁苛暴，止擅赋，力本农。修马政复令以补缺，毋乏武备而已”，史称“轮台罪己诏”。\[2\]\[3\]

## 内容

  - 罪己詔翻譯成現在的文字就是：

## 参考文献

{{-}}

[罪](../Category/中国诏令文书.md "wikilink")
[Category:西汉政府文件](../Category/西汉政府文件.md "wikilink")
[Category:汉武帝](../Category/汉武帝.md "wikilink")
[Category:前80年代中国政治](../Category/前80年代中国政治.md "wikilink")
[Category:前89年](../Category/前89年.md "wikilink")

1.
2.  《汉武帝与轮台罪己诏》 王也扬 炎黄春秋杂志2005年第4期

3.  《[史记卷十二](../Page/史记.md "wikilink")·孝武本纪第十二》