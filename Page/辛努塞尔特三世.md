**辛努塞尔特三世 Senusret III** （[希腊人用的称呼是](../Page/希腊.md "wikilink")**塞索斯特利斯三世
Sesostris III**，另一埃及名为**森沃斯勒
Senwosret**，意为“沃斯雷特女神的子民”）[古埃及](../Page/古埃及.md "wikilink")[第十二王朝](../Page/埃及第十二王朝法老列表.md "wikilink")[法老](../Page/法老.md "wikilink")（[前苏联历史界认为](../Page/前苏联.md "wikilink")，他可能在公元前1884年—公元前1849年在位）。

辛努塞尔特三世巩固埃及统治，积极促进改革，加强中央政权，削弱贵族的权势和影响。在[尼罗河第二瀑布上游修建堡垒](../Page/尼罗河第二瀑布.md "wikilink")。为保证埃及本土与努比亚之间交通顺畅，辛努塞尔特三世开凿了一条绕过[尼罗河第一瀑布的](../Page/尼罗河第一瀑布.md "wikilink")[蘇伊士运河](../Page/蘇伊士运河.md "wikilink")。据记载，辛努塞尔特三世曾远征到[尼罗河第三瀑布地方](../Page/尼罗河第三瀑布.md "wikilink")。成为埃属[努比亚的保护神](../Page/努比亚.md "wikilink")。

在[达舒尔地区](../Page/达舒尔.md "wikilink")，考古学家发现了辛努塞尔特三世的[金字塔](../Page/金字塔.md "wikilink")。\[1\]

## 注释

<references/>

[Category:第十二王朝法老](../Category/第十二王朝法老.md "wikilink")

1.  \[埃\]阿·费克里著，《埃及古代史》，高望之等译，商务印书馆1973年版。