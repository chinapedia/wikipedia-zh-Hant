**津巴布韋共和國**（）
通稱**辛巴威**，是位於[非洲南部的](../Page/非洲南部.md "wikilink")[內陸國家](../Page/內陸國家.md "wikilink")，地处[赞比西河与](../Page/赞比西河.md "wikilink")[林波波河之间](../Page/林波波河.md "wikilink")，南接[南非](../Page/南非.md "wikilink")，西部和西南与[博兹瓦纳接壤](../Page/博兹瓦纳.md "wikilink")，西北与[赞比亚接壤](../Page/赞比亚.md "wikilink")，东部和东北与[莫桑比克相邻](../Page/莫桑比克.md "wikilink")，尽管津巴布韦与[纳米比亚无领土接壤](../Page/纳米比亚.md "wikilink")，但是在[赞比西河河岸两国仅相隔](../Page/赞比西河.md "wikilink")200余米相望。津巴布韦于1980年4月18日独立建国，面积39万平方公里，最大城市和首都为[哈拉雷](../Page/哈拉雷.md "wikilink")。

津巴布韦人口的組成在立國後變化巨大，獨立時總人口690萬，其中約30萬（當時約佔5.5%）是白人統治階級，但因為[土地改革政策之後](../Page/土地改革.md "wikilink")，目前白人出現持續下降的現象，已經僅剩不到數萬人，且多在歐美境外居留並大量移民他國。由於黑人的[出生率極高](../Page/出生率.md "wikilink")，津巴布韦人口暴增來到约1415万（2013年），[黑人已經占总人口的](../Page/黑人.md "wikilink")99%以上，主要有[绍纳族](../Page/绍纳族.md "wikilink")（占79%）和[恩德贝莱族](../Page/恩德贝莱族.md "wikilink")（占17%）两大民族。

津巴布韦有16种[官方语言](../Page/官方语言.md "wikilink")，其中[英语是使用最广泛的语言](../Page/英语.md "wikilink")，其他主要语言还有[修纳语和](../Page/修纳语.md "wikilink")[恩德贝莱语](../Page/恩德贝莱语.md "wikilink")。\[1\]

津巴布韋原本是名為[羅德西亞的由白人统治的國家](../Page/羅德西亞.md "wikilink")，這个名字源自於替[英國在這个地區建立](../Page/英國.md "wikilink")[殖民地的](../Page/殖民地.md "wikilink")[塞西尔·罗兹](../Page/塞西尔·罗兹.md "wikilink")，進入20世紀後由[伊安·史密斯領導](../Page/伊安·史密斯.md "wikilink")，接受南非政權的援助，展開白人少數統治並抵抗共產游擊隊而知名。羅德西亞因天候宜人，當時的物產多以農產品為主，當時白人農場主因此地富饒，該國有「阿非利加麵包籃」美譽，並建立了歐洲式首都[哈拉雷](../Page/哈拉雷.md "wikilink")，原名索尔兹伯里，是罗兹在[1890年時建立的貿易集散城市](../Page/1890年.md "wikilink")，為非洲中部經濟大城之一。在1980年之後白人少數統治被推翻，進入了津巴布韦時代，但同時經濟卻遭遇嚴重[通貨膨脹](../Page/通貨膨脹.md "wikilink")，產量已經下跌不足以供養國民的溫飽，今日辛巴威的食品以從外國進口為主，並向南非提供外勞，對於該國二十世紀末以來陷入的經濟困難，官方視殖民是導致津巴布韦持續貧窮的主要原因。

## 歷史

### 南部非洲的文明

津巴布韋是南部非洲重要的文明發源地，在中世紀時代，該地曾存在一個[紹納人](../Page/紹納人.md "wikilink")（Shona，屬於[班圖族的一支](../Page/班圖.md "wikilink")）建立的文明，並且遺留下不少文化遺跡，其中最重要的莫過於[大津巴布韋古城](../Page/大津巴布韋古城.md "wikilink")（也是津巴布韋的命名由來），以此城為首都的[马蓬古布韦王国透過與來自](../Page/马蓬古布韦王国.md "wikilink")[印度洋岸的](../Page/印度洋.md "wikilink")[回教商隊貿易](../Page/回教.md "wikilink")，在11世紀時漸漸強盛，利用當地生產的[黃金](../Page/黃金.md "wikilink")、[象牙與](../Page/象牙.md "wikilink")[銅礦等重要物資](../Page/黃銅.md "wikilink")，交換來自[波斯灣地區的](../Page/波斯灣.md "wikilink")[布料與](../Page/布.md "wikilink")[玻璃等生產品](../Page/玻璃.md "wikilink")，15世紀時，已經成為非洲南部最大的邦國。

紹納文明的強盛在19世紀時邁入尾聲，1837年時，紹納人被屬於[祖魯族的](../Page/祖魯族.md "wikilink")[恩德貝勒人](../Page/恩德貝勒人.md "wikilink")（Ndebele）征服，而來自英國與來自南邊的[波爾人](../Page/波爾人.md "wikilink")（荷裔[南非人](../Page/南非.md "wikilink")）開始逐漸蠶食這個地區。

### 英國殖民地时期（1888年－1965年）

1888年，[大英帝國](../Page/大英帝國.md "wikilink")[殖民者](../Page/殖民者.md "wikilink")[赛西尔·罗兹從恩德貝勒國王手上取得他們領土內的採礦權](../Page/赛西尔·罗兹.md "wikilink")，隨後在1889年替[英屬南非公司取得這裡的領土權](../Page/英屬南非公司.md "wikilink")，並且在1895年時正式建立[殖民國家](../Page/殖民國家.md "wikilink")「[羅德西亞](../Page/羅德西亞.md "wikilink")」，这一名称是以罗兹的名字命名的。1896年到1897年，紹納人和恩德貝勒人开始武力反抗英国的殖民统治，爆发，约有450名白人被打死\[2\]。

[羅德西亞在](../Page/羅德西亞.md "wikilink")1911年時分開成為[北羅德西亞](../Page/北羅德西亞.md "wikilink")（今日的[尚比亞](../Page/尚比亞.md "wikilink")）與[南羅德西亞](../Page/南羅德西亞.md "wikilink")，後者在1922年時成為一個英屬自治殖民地，就是今日的津巴布韦。1953年時，英国不顾非洲人民的反对，将南北羅德西亞以及[尼亚萨兰](../Page/尼亚萨兰.md "wikilink")（今[马拉维](../Page/马拉维.md "wikilink")）聯合成為一個[聯邦](../Page/聯邦.md "wikilink")，命名為[羅德西亞與尼亞薩蘭聯邦](../Page/羅德西亞與尼亞薩蘭聯邦.md "wikilink")（簡稱羅尼聯邦）。在三地人民的反对下，特别是尼亚萨兰的反对下，联邦於十年后的1963年解散。羅尼聯邦解散后不久，1965年11月，南羅德西亞总理[伊安·史密斯单方面宣布該國脱离英国的管辖獨立](../Page/伊安·史密斯.md "wikilink")，成立南羅德西亞，但這份聲明並不受境內大部分由北羅德西亞與尼亞薩蘭所控制的[黑人族群所認可](../Page/黑人.md "wikilink")。負責託管這地區的英國聲稱此獨立宣言是種叛亂行為不予承認，但因為同屬白人，英國方也沒有實際使用武力來恢復控制權。辛巴威在英治時期有「非洲糧倉」的美譽。

### 国家独立戰爭（1965年－1980年）

[Mutare_aerial.jpg](https://zh.wikipedia.org/wiki/File:Mutare_aerial.jpg "fig:Mutare_aerial.jpg")\]\]
[Harare_secondst.jpg](https://zh.wikipedia.org/wiki/File:Harare_secondst.jpg "fig:Harare_secondst.jpg")\]\]
[Chimanimani_Village_-_Bus_Station_-_Zimbabwe.jpg](https://zh.wikipedia.org/wiki/File:Chimanimani_Village_-_Bus_Station_-_Zimbabwe.jpg "fig:Chimanimani_Village_-_Bus_Station_-_Zimbabwe.jpg")
在1966年至1968年的國際斡旋工作失敗後，英國要求[聯合國對羅德西亞進行](../Page/聯合國.md "wikilink")[經濟制裁](../Page/經濟制裁.md "wikilink")，但一意孤行的[伊恩·史密斯白人政權](../Page/伊恩·史密斯.md "wikilink")（獲同樣為南非的白人政府支持）仍然在1970年時宣布成立共和國政體，也沒有獲得任何[國際上的承認](../Page/國際.md "wikilink")。

在白人執政的這段期間，津巴布韋境內的黑人團體一直在進行激烈的[游擊戰試圖推翻當權者](../Page/游擊戰.md "wikilink")。1978年，在國際制裁與黑人武裝運動的交相夾擊下，白人政府終於被迫與包括[津巴布韋非洲民族聯盟](../Page/津巴布韋非洲民族聯盟.md "wikilink")（ZAPU）及[津巴布韋非洲國家聯盟](../Page/津巴布韋非洲國家聯盟.md "wikilink")（ZANU）在內的諸多黑人勢力簽訂了協約，共同推派[衛理公會](../Page/衛理公會.md "wikilink")[主教](../Page/主教.md "wikilink")[穆佐列瓦擔任](../Page/穆佐列瓦.md "wikilink")[津巴布韋羅德西亞臨時政府的首相](../Page/津巴布韋羅德西亞.md "wikilink")。在英國的監督下，黑人游擊組織終於願意卸除武裝結束內戰，在1980年時舉行了該國有史以來的第一次民主選舉，雖然穆佐列瓦有原本羅德西亞白人政權與南非政府的支持，但卻在本國大部分的黑人族裔中缺乏信賴，他的過渡政府並沒有在大選中獲得持續執政的機會，而是由[罗伯特·穆加贝與他領導的津巴布韋非洲民族聯盟](../Page/罗伯特·穆加贝.md "wikilink")（ZAPU）獲得政權。

在國家政權順利轉移到黑人政府的手上之後，於1980年4月18日津巴布韋共和國正式獨立建國。在此之後[穆加贝連續贏得多次競選](../Page/穆加贝.md "wikilink")，直到[2017年津巴布韦政变后下台](../Page/2017年津巴布韦政变.md "wikilink")。

### 现况（1980年－至今）

津巴布韋的國政自獨立後轉移到了黑人的手上，但是穆加比通过暴力将欧裔白人的土地没收。20世纪末期，經濟動蕩，許多基礎民生需求匱乏。

2000年開始，津巴布韋政府以白人所持有之土地係在殖民時代從黑人原居民手中非法取得為理由，進行[土地改革](../Page/土地改革.md "wikilink")，強制沒收大部分白人擁有的土地。此舉造成境內大量白人農民出走，糧食產量大減，[经济陷入混乱](../Page/经济.md "wikilink")\[3\]\[4\]。

2002年，穆加比第四度獲得連任。反對黨與勞工組織紛紛發動罷工與抗爭，要求當時已執政22年的穆加比提早下台退休，而政府則以軍警部隊鎮壓。[國際貨幣基金組織](../Page/國際貨幣基金組織.md "wikilink")（IMF）為首的資金援助債權國，控告穆加比政權藐視人權與在大選中舞弊，紛紛擱置援助計畫。而[英聯邦則在](../Page/英聯邦.md "wikilink")2002年時宣佈對津巴布韋停權一年處分，津巴布韋則於不久之後主動退出該協會作為回應。

由於長期積欠外債，政府又不願意實施經濟上的穩定措施，使得IMF暫停了對津巴布韋的經濟援助，而該國政府則以大量印製新鈔來填補財政上的赤字作為回應，卻導致嚴重的通貨膨脹與幣值急貶。津巴布韋官方公佈2007年11月份的[通脹率](../Page/通脹率.md "wikilink")，高達26,000%，而真實數字可能更高\[5\]。由於失業率高達80%，大量人民為了生計被逼逃到鄰國。

2008年大選，最大反對黨民主改革運動黨（MDC）的[茨萬吉拉伊第一輪投票的得票率領先穆加比](../Page/摩根·茨萬吉拉伊.md "wikilink")，但茨萬吉拉伊的得票率未達半數，因此於6月27日舉行次輪選舉，卻由於茨萬吉拉伊因選舉暴力而被迫退選，最終穆加比勝出。

另一方面國內的經濟情況未見起色。2008年2月，通貨膨脹率達165,000%；2008年6月，通貨膨脹率達200,000%，該國央行並於2008年7月21日發行面值1000億元的[津巴布韦元鈔票](../Page/津巴布韦元.md "wikilink")\[6\]\[7\]\[8\]。津巴布韦从2008年8月1日起货币改制，100亿舊津巴布韋元相当于1新津巴布韋元。2009年一月消息，津巴布韦将于近日发行一套世界上最大面额的新钞，这套面额在万亿以上的新钞包括10兆、20兆、50兆和100兆津元四种。

在2009年4月12日時，該國政府更宣佈因為已經難以維持貨幣價值，將停用[本國貨幣一年](../Page/津巴布韋元.md "wikilink")。\[9\]

2013年8月大選，執政33年的穆加比通過選舉舞弊的手段，擊敗反對派領袖茨萬吉拉伊，繼續連任總統，成為非洲最年長的國家元首。

2015年6月，政府宣佈棄用本國貨幣，使用[美元](../Page/美元.md "wikilink")。民眾可將存在銀行帳戶內或持有現鈔的辛巴威元，向津巴布韋儲備銀行換成美元，匯率是35,000,000,000,000,000津元兌1美元。銀行戶頭超過175千兆辛巴威元的部分，以35千兆辛巴威元換1美元，未超過175千兆辛巴威元的部分，則一律換得5美元。\[10\]現在，所有交易均使用鄰國和大國的[貨幣來進行](../Page/貨幣.md "wikilink")，包括：[南非蘭特](../Page/南非蘭特.md "wikilink")、[博茨瓦納普拉](../Page/博茨瓦納普拉.md "wikilink")、[印度盧比](../Page/印度盧比.md "wikilink")、[歐元](../Page/歐元.md "wikilink")、[日元](../Page/日元.md "wikilink")、[人民幣](../Page/人民幣.md "wikilink")、[澳洲元](../Page/澳洲元.md "wikilink")、[英鎊及](../Page/英鎊.md "wikilink")[美元](../Page/美元.md "wikilink")。

2015年12月21日，根据和[中国人民银行达成的协议](../Page/中国人民银行.md "wikilink")，津巴布韦在2016年初开始，将[人民币列为和美元同等在国内法定流通的货币](../Page/人民币.md "wikilink")，这被视为中国免除该国大约4000万美元到期债务的回报。\[11\]\[12\]

2016年12月17日，穆加比在執政黨[津巴布韋非洲民族聯盟－愛國陣線](../Page/津巴布韋非洲民族聯盟－愛國陣線.md "wikilink")（ZANU-PF）的黨大會上被提名為2018年總統選舉候選人，準備挑戰全世界第一位「百歲總統」。

[2017年辛巴威政變](../Page/2017年辛巴威政變.md "wikilink")，軍方發動政變，11月19日，執政黨[津巴布韋非洲民族聯盟－愛國陣線](../Page/津巴布韋非洲民族聯盟－愛國陣線.md "wikilink")（ZANU-PF）通過罷黜穆加比的黨主席及第一書記職權。21日穆加比辭職下台。

## 地理

津巴布韦国土面积39萬餘平方公里，位於[非洲東南部](../Page/非洲.md "wikilink")，是一個內陸國家。津巴布韦東鄰[莫桑比克](../Page/莫桑比克.md "wikilink")，南接[南非](../Page/南非.md "wikilink")，西和西北與[波札那](../Page/波札那.md "wikilink")、[尚比亞相連](../Page/尚比亞.md "wikilink")。

津巴布韦大部分地区是[高原地形](../Page/高原.md "wikilink")，平均海拔1000餘米。地形分高草原、中草原和低草原3種。津巴布韦東部[伊尼揚加尼山海拔](../Page/伊尼揚加尼山.md "wikilink")2,592米，為全國最高點。主要河流有[讚比西河和](../Page/讚比西河.md "wikilink")[林波波河](../Page/林波波河.md "wikilink")，分別是與鄰國尚比亞和南非的[界河](../Page/界河.md "wikilink")。[維多利亞瀑布](../Page/維多利亞瀑布.md "wikilink")、三比西河、三比西河上游的[卡-{里}-巴水壩與大壩攔阻河水積蓄而成的](../Page/卡里巴水壩.md "wikilink")[卡-{里}-巴湖共同圍成津巴布韋北邊的疆界](../Page/卡里巴湖.md "wikilink")，與尚比亞相鄰。津巴布韋的東邊[國界全部與莫三比克相鄰](../Page/國界.md "wikilink")，西南為波札那，南境則有一部分與南非相連，以[林波波河為界](../Page/林波波河.md "wikilink")。

津巴布韦气候属于[熱帶草原氣候](../Page/熱帶草原氣候.md "wikilink")，年均氣溫[攝氏](../Page/攝氏.md "wikilink")22度，10月份溫度最高，達32度，7月份溫度最低，約13～17度。

## 人口

[Zi-map.png](https://zh.wikipedia.org/wiki/File:Zi-map.png "fig:Zi-map.png")
[Students_of_St_George's_College,_Harare.jpg](https://zh.wikipedia.org/wiki/File:Students_of_St_George's_College,_Harare.jpg "fig:Students_of_St_George's_College,_Harare.jpg")
津巴布韋人口約為16,743,000（2018年10月估計值），其中非裔黑人佔了98%，混血與亞裔人種佔1%，白人則佔1%不到。在黑人族裔中最主要的一支為[紹納人](../Page/紹納人.md "wikilink")（Shona，佔總人口82%），居次為[恩德貝萊人](../Page/恩德貝萊人.md "wikilink")（Ndebele，佔總人口14%）。

[英語是津巴布韋的](../Page/英語.md "wikilink")[官方語言](../Page/官方語言.md "wikilink")，並與[紹納語和](../Page/紹納語.md "wikilink")[恩德貝萊語並列為主要](../Page/恩德貝萊語.md "wikilink")[語言](../Page/語言.md "wikilink")。

津巴布韦絕大部分的人口信奉[基督教及](../Page/基督教.md "wikilink")[民間信仰](../Page/民間信仰.md "wikilink")，至於[伊斯蘭教與其他的](../Page/伊斯蘭教.md "wikilink")[宗教信仰](../Page/宗教.md "wikilink")，則佔了該國不到1%的人口\[13\]。

## 重要城镇

  - [卡里巴](../Page/卡里巴.md "wikilink")（Kariba）
  - [哈拉雷](../Page/哈拉雷.md "wikilink")（Harare）－首都
  - [圭鲁](../Page/圭鲁.md "wikilink")（Gweru）
  - [布拉瓦约](../Page/布拉瓦约.md "wikilink")（Bulawayo）
  - [兹维沙瓦內](../Page/兹维沙瓦內.md "wikilink")（Zvishavane）
  - [津巴布韦市](../Page/大津巴布韦.md "wikilink")（Zimbabwe）

## 经济

根據[美國中央情報局發表的](../Page/美國中央情報局.md "wikilink")[世界概況](../Page/世界概況.md "wikilink")，津巴布韋國民生產總值位列榜尾。

津巴布韦在21世纪初开始经历[恶性通货膨胀](../Page/恶性通货膨胀.md "wikilink")，按2008年7月的官方統計，通胀率达到231,000,000%\[14\]\[15\]。时任津巴布韦总统[穆加贝指责西方发达国家对津巴布韦实施包括冻结援助贷款和冻结津巴布韦在西方的存款和资产的经济](../Page/穆加贝.md "wikilink")[制裁](../Page/制裁.md "wikilink")，而这些制裁是导致目前[经济危机的最主要原因](../Page/经济危机.md "wikilink")\[16\]。而穆加贝的反对者则认为穆加贝推行的[土地改革](../Page/土地改革.md "wikilink")（強買白人農場主的土地），政府对价格的控制不力，以及[艾滋病流行等因素是造成当前经济危机的主要原因](../Page/艾滋病.md "wikilink")，并称目前津巴布韦正面临独立以来最严重的[人道主义危机](../Page/人道主义.md "wikilink")。\[17\]

2015年6月，津巴布韋政府宣佈正式棄用[津巴布韋元](../Page/津巴布韋元.md "wikilink")，民眾可向津巴布韋儲備銀行將所持有的辛巴威元以35千兆元換一美元的匯率兌換為美元\[18\]。

## 教育

[Literacy_map2014.png](https://zh.wikipedia.org/wiki/File:Literacy_map2014.png "fig:Literacy_map2014.png")
自独立以来，政府对津巴布韋的教育事业投资巨大，因而有了非洲最高的成人识字率。在2013年达到90.70%\[19\]，略低于2010年的92%\[20\]\[21\]。

## 文化

[Shona_witch_doctor_(Zimbabwe).jpg](https://zh.wikipedia.org/wiki/File:Shona_witch_doctor_\(Zimbabwe\).jpg "fig:Shona_witch_doctor_(Zimbabwe).jpg")
津巴布韦主要节日以及法定假日包括：[新年](../Page/新年.md "wikilink")（1月1日）、青年节（2月21日）、[复活节](../Page/复活节.md "wikilink")、[独立日](../Page/独立日.md "wikilink")（4月18日）、[劳动节](../Page/劳动节.md "wikilink")（5月1日）、[非洲日](../Page/非洲日.md "wikilink")（5月25日）、[英雄日](../Page/英雄日.md "wikilink")（8月11日）、[建軍節](../Page/軍人節.md "wikilink")（8月12日）、[国家团结日](../Page/国家团结日.md "wikilink")（12月22日）、[圣诞节](../Page/圣诞节.md "wikilink")（12月25-26日）等。

[足球是津巴布韋國內最流行的運動之一](../Page/足球.md "wikilink")。雖然[橄欖球和](../Page/橄欖球.md "wikilink")[板球也很受歡迎](../Page/板球.md "wikilink")，但傳統上主要流行於佔少數的[白人當中](../Page/白人.md "wikilink")。津巴布韋曾在[奧運當中得到](../Page/奧運.md "wikilink")8面獎牌，分別是1980年的[莫斯科奧運](../Page/1980年夏季奧林匹克運動會.md "wikilink")「陸上曲棍球」項目得到1面[金牌](../Page/金牌.md "wikilink")；2004年的[雅典奧運得到金銀銅各](../Page/2004年夏季奧林匹克運動會.md "wikilink")1面獎牌；2008年的[北京奧運得到](../Page/2008年夏季奧林匹克運動會.md "wikilink")1金3銀。值得一提的是，雅典及北京奧運共7面獎牌皆是由白人女運動員[柯丝蒂·考文垂一人於](../Page/柯丝蒂·考文垂.md "wikilink")[游泳項目所獲得](../Page/游泳.md "wikilink")。

## 参考文献

## 外部連結

  - [津巴布韋政府網站](https://web.archive.org/web/20081024195135/http://www.gta.gov.zw/)

{{-}}

[津巴布韦](../Category/津巴布韦.md "wikilink")
[Category:非洲国家](../Category/非洲国家.md "wikilink")
[Category:內陸國家](../Category/內陸國家.md "wikilink")
[Category:共和國](../Category/共和國.md "wikilink")
[Category:前英國殖民地](../Category/前英國殖民地.md "wikilink")
[Category:英語國家地區](../Category/英語國家地區.md "wikilink")
[Category:班圖語國家地區](../Category/班圖語國家地區.md "wikilink")
[Category:1980年建立的國家或政權](../Category/1980年建立的國家或政權.md "wikilink")
[Category:前大英國協國家](../Category/前大英國協國家.md "wikilink")

1.  [津巴布韦概况](http://news.xinhuanet.com/ziliao/2002-05/24/content_410169.htm)
    ，[新华网](../Page/新华网.md "wikilink")。
2.  [“津巴布韦历史简述”](http://zimbabwe.all-africa.net/jgaikuang/123030187.htm)
    ，非洲商务网，2006年2月23日
3.
4.
5.  [美国中央情报局报告](https://www.cia.gov/library/publications/the-world-factbook/geos/zi.html)
6.  Ō[AFPBB News](http://www.afpbb.com/article/economy/2420115/3147564)
7.  [津巴布韦发行面额100亿钞票创下世界之最](http://art.people.com.cn/GB/41067/41121/8566516.html)
8.  [Reserve Bank of Zimbabwe $100,000,000,000 Special Agro
    Cheque](http://www.garrysue.net/Zim/100bilagro08.htm) Garrysue.net
9.  [津巴布韋政府宣布暫停使用本國貨幣](http://news.bbc.co.uk/chinese/trad/hi/newsid_7990000/newsid_7995900/7995902.stm)
    2009-04-12 [BBC中文網](../Page/BBC中文網.md "wikilink")
10.
11. [津巴布韦：明年初将用人民币作流通货币](http://news.china.com/international/1000/20151222/20987184_all.html)

12. [Zimbabwe says China to cancel $40 mln debt, increase yuan
    use](http://af.reuters.com/article/investingNews/idAFKBN0U412520151221)
13. [津巴布韋介紹（CIA World Fact
    Book）](https://www.cia.gov/library/publications/the-world-factbook/geos/zi.html)
14. [津巴布韋儲備銀行的2008上半年金融政策報告](http://www.rbz.co.zw/pdfs/2008Julymps/mps.pdf)
    （英文）
15. [津巴布韦通货膨胀率已达100500%](http://news.163.com/08/0307/10/46E5IOK60001121M.html)，[环球网](../Page/环球网.md "wikilink")，2008年3月7日
16. [欧美缘何制裁津巴布韦](http://news.xinhuanet.com/newscenter/2002-03/05/content_302206.htm)
    ，新华网，2002年3月5日。
17. [津巴布韦当前的人道主义局势](http://www.alertnet.org/thefacts/reliefresources/112239842449.htm)（英语）。
18.
19.
20. ["Unlicensed and outdoors or no school at
    all"](http://ww.irinnews.org/Report.aspx?ReportId=89947) , *IRIN*,
    23 July 2010
21. ["Zimbabwe: Country Leads in Africa Literacy
    Race"](http://allafrica.com/stories/201007150032.html),
    *AllAfrica.com*, 14 July 2010