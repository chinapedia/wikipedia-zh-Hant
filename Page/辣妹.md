[Glam_crop.jpg](https://zh.wikipedia.org/wiki/File:Glam_crop.jpg "fig:Glam_crop.jpg")
**辣妹**（、[英語](../Page/英語.md "wikilink")：gal）是始於1970年代[日本當時身穿新的時尚服裝女性之總稱](../Page/日本.md "wikilink")，在[泡沫經濟時期時成為意指年輕女性的詞語](../Page/日本泡沫經濟.md "wikilink")，原宿是其起源地之一。

現在主要是用來稱呼穿戴「辣妹[服飾](../Page/服裝.md "wikilink")」的10～20多歲的女性。1990年代時曾特別稱這樣的[女高中生為](../Page/女高中生.md "wikilink")**小辣妹**（），而這個世代的女性維持原嗜好成為大人，現在則是不分年代都稱為辣妹。《[egg](../Page/Egg_\(雜誌\).md "wikilink")》、《》及《[Cawaii\!](../Page/Cawaii!.md "wikilink")》等[雜誌為辣妹文化帶來了莫大的影響](../Page/雜誌.md "wikilink")。而日本人知道辣妹的意思以后不觉得辣妹等於gal。

## 時尚型態

  - 小辣妹（）
    从1990年代中期开始出现的流行语。最初见于1993年左右的《[Friday](../Page/星期五_\(杂志\).md "wikilink")》（）等杂志上,不过，大量地经常性被使用是从1996年左右开始。头发一般為染成[茶色](../Page/茶色.md "wikilink")（）或者浅色系的挑染，身着[校服](../Page/校服.md "wikilink")，脚穿[泡泡襪和没有](../Page/泡泡襪.md "wikilink")的[皮鞋](../Page/皮鞋.md "wikilink")（）。在90年代末期也出现了虽然已经毕业，但仍穿着制服在路上聚在一起的轻浮女生。

## 關聯條目

  -
  - [辣妹文字](../Page/辣妹文字.md "wikilink")

  -
  - [109百貨](../Page/109百貨.md "wikilink")

  -
  -
  -
[de:Kogal](../Page/de:Kogal.md "wikilink")

[Category:日本流行语](../Category/日本流行语.md "wikilink")
[Category:女性称谓](../Category/女性称谓.md "wikilink")