**辣根**（學名：*Armoracia
rusticana*），又称**西洋山嵛菜**、**马萝卜**、**山蘿蔔**、**粉山葵、西洋山葵**，原产东南欧，是[十字花科](../Page/十字花科.md "wikilink")[辣根属多年生宿根耐寒植物](../Page/辣根属.md "wikilink")。可以做为蔬菜使用，具有刺激鼻窦的香辣味道。辣根用于欧洲国家的烤牛肉等菜肴的佐料以外，因削皮後是白的，加入食用色素後作为仿制[山葵调料的材料](../Page/山葵.md "wikilink")。

<file:Kren> Verkauf.jpg| <file:Armoracia> rusticanaAHA.jpg|
<file:ArmoraciaRusticana.jpg>|

## 参见

  - [山葵](../Page/山葵.md "wikilink")

## 参考文献

<div class="references-small">

  -

</div>

  - 《台灣蔬果實用百科第二輯》，薛聰賢 著，薛聰賢出版社，2001年

[Category:蔬菜](../Category/蔬菜.md "wikilink")
[Category:辣根属](../Category/辣根属.md "wikilink")
[Category:调味料](../Category/调味料.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")