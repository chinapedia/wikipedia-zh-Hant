**辣薄荷**（[学名](../Page/学名.md "wikilink")：，又名**胡椒薄荷**，）唇形科年生草本植物，是一種被廣泛種植的混種[薄荷](../Page/薄荷.md "wikilink")，原产于欧洲，可用於食物的[調味料](../Page/調味料.md "wikilink")。这个薄荷品种是由[绿薄荷](../Page/绿薄荷.md "wikilink")（*Mentha
spicata*，）与[水薄荷](../Page/水薄荷.md "wikilink")（*Mentha aquatica*，）混種而成的.

辣薄荷最早是由瑞典生物學家[卡尔·林奈](../Page/卡尔·林奈.md "wikilink")（）在英国采集样本时发现并命名；林奈认为此植物是一个新物种，但是现在學術界已达成共识，将辣薄荷归类于[杂交植物](../Page/杂交植物.md "wikilink")。

## 圖片

<File:Mentha> piperita - Pfefferminze.jpg|植株 <File:Flowers> of Mentha ×
piperita.jpg|花 <File:Pfefferminze> natur peppermint.jpg|頂芽 <File:Mentha>
piperita 0.1 R.jpg|葉 <File:Mentha> piperata seeds.jpg|種子 <File:Menthae>
piperitae folium 008182.jpg|乾燥葉 <File:Teebeutel> Polylactid
2009.jpg|薄荷[茶包](../Page/茶包.md "wikilink")

[Category:薄荷属](../Category/薄荷属.md "wikilink")
[Category:香草](../Category/香草.md "wikilink")