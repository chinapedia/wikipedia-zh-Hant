[缩略图](https://zh.wikipedia.org/wiki/File:Marx_and_Engels.jpg "fig:缩略图")和[恩格斯](../Page/恩格斯.md "wikilink")\]\]

**辩证唯物主义**（）是一種以[馬克思和](../Page/馬克思.md "wikilink")[恩格斯學說來研究現實的](../Page/恩格斯.md "wikilink")[哲學方法](../Page/哲學.md "wikilink")\[1\]，是用“辩证的观点”和“[唯物论的观点](../Page/唯物论.md "wikilink")”解释和认识世界的[理论](../Page/理论.md "wikilink")。辩证唯物主义[批判地继承了](../Page/批判.md "wikilink")[费尔巴哈的](../Page/路德维希·安德列斯·费尔巴哈.md "wikilink")[机械唯物主义](../Page/机械唯物主义.md "wikilink")\[2\]。

## 基本观点

1.  [唯物主义认为](../Page/唯物主义.md "wikilink")：[物质是第一性的](../Page/物质.md "wikilink")，[意识是第二性的](../Page/意识.md "wikilink")。世界的本源是物质，世界的万事万物都是物质派生出来的。
2.  [物质世界是按照它本身所固有的规律运动](../Page/物质.md "wikilink")、变化和发展的，[规律是](../Page/规律.md "wikilink")[客观的](../Page/客观.md "wikilink")，是不以人的主观意志为转移的。
3.  辩证的唯物主义观点：这是相对于[机械唯物主义而言的](../Page/机械唯物主义.md "wikilink")，即将[辩证法与](../Page/辩证法.md "wikilink")[唯物主义相结合](../Page/唯物主义.md "wikilink")，主要观点参见[唯物辩证法](../Page/唯物辩证法.md "wikilink")。

## 运动观

1.  世界是[物质的](../Page/物质.md "wikilink")，[物质是运动的](../Page/物质.md "wikilink")，整个世界就是永恒运动着的物质世界，这是辩证唯物主义对世界最基本的看法。
2.  [运动是绝对的](../Page/運動_\(物理學\).md "wikilink")，[静止是相对的](../Page/静止.md "wikilink")。事物在绝对的运动中存在着相对静止，在一定条件下保持相对稳定状态。静止是运动的表现形式。

## 物质观与意识观

1.  [意识是一种对客观世界](../Page/意识.md "wikilink")（物质）的反映，任何物质都有反映的能力。意识分为初级的感觉和高级的思维。
    1.  [感知是](../Page/感知.md "wikilink")[感官](../Page/感官.md "wikilink")[器官对外界事物刺激而形成的](../Page/器官.md "wikilink")，感觉是对客观事物的主观映像，是意识的低级形式。对客观事物的直接反应。如我们对苹果的印象：闻起来是香的，看起来是红的，尝起来是甜的，嚼起来是脆脆的，摸到它是硬的。这些感觉综合凑在一起，我们才在脑海中出现了苹果的映像。如果没有感觉到苹果，也就谈不上对苹果的任何认识。如果没有感觉能力，那大脑中就是一片空白，也就没有了意识，感觉是意识的起点。
    2.  [思维是在表象](../Page/思维.md "wikilink")、概念的基础上进行分析、综合、判断、推理等认识活动的过程,是人类特有的一种[精神活动](../Page/精神.md "wikilink")，是[意识的高级形式](../Page/意识.md "wikilink")。感觉给予人的，是具体事物的个别特性。[思维给予人的](../Page/思维.md "wikilink")，是同类事物的一般特性。我们能摸到一个个梨、苹果、葡萄、荔枝，却摸不到水果。思维告诉人们一种事物的本质特征。思维具有抽象和概括的特点，不是对客观事物的直接反映，是一种间接的反映。在思维与客观事物之间，存在着感觉这个连接点，思维依赖于感觉提供的材料，经过一系列抽象过程，才形成某种概念。我们虽然摸不到水果，但一想到水果，就会想到一个个梨、苹果、葡萄、荔枝。如果人感觉不到苹果、梨等客观事物，也就不会形成水果这个概念。
2.  任何意识都是客观世界的反映，依赖于客观世界，不能脱离客观世界，物质是决定意识的。但是意识也反作用于物质，如人类对于改造世界的行动。物质与意识是对立统一的。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - Gustav A.Wetter. *Dialectical Materialism*. 1973年. ISBN 0837167272
  - ''Dialectical Materialism: Its Laws, Categories, and Practice, Ira
    Gollobin, Petras Press, NY, 1986.
  - V.G.Afanasyev. *Historical Materialism*. 1987年. ISBN 0717806375
  - *Dialectics for the New Century*, ed. Bertell Ollman and Tony Smith,
    Palgrave Macmillan, England, 2008.

## 参见

  - [矛盾](../Page/矛盾.md "wikilink")
  - [辩证法](../Page/辩证法.md "wikilink")
  - [自然辩证法](../Page/自然辩证法.md "wikilink")
  - [唯物論](../Page/唯物論.md "wikilink")
  - [历史唯物主义](../Page/历史唯物主义.md "wikilink")
  - [马克思主义哲学](../Page/马克思主义哲学.md "wikilink")
  - [毛泽东思想](../Page/毛泽东思想.md "wikilink")
  - [唯物主义](../Page/唯物主义.md "wikilink")
  - [唯物辩证法](../Page/唯物辩证法.md "wikilink")

{{-}}

[Category:马克思主义哲学](../Category/马克思主义哲学.md "wikilink")
[B](../Category/哲学基本问题.md "wikilink")
[B](../Category/历史理论.md "wikilink")
[B](../Category/社会学.md "wikilink")
[Category:辩证法](../Category/辩证法.md "wikilink")
[Category:唯物論](../Category/唯物論.md "wikilink")

1.  [辩的含义与解释 汉字详解 字典大全 -- 词典
    成语字典:](http://www.zd9999.com/zi/detail.asp?id=628)
2.  [辩证唯物主义(dialectical
    materialism)_生物谷_科普频道:](http://www.bioon.com/popular/information/b/200506/139450.html)