[CiXiu_park_Taishan_District_b_2018.jpg](https://zh.wikipedia.org/wiki/File:CiXiu_park_Taishan_District_b_2018.jpg "fig:CiXiu_park_Taishan_District_b_2018.jpg")
[Taishan_District_Aboriginal_Cutural_Centre_in_CiXiu_park_2018.jpg](https://zh.wikipedia.org/wiki/File:Taishan_District_Aboriginal_Cutural_Centre_in_CiXiu_park_2018.jpg "fig:Taishan_District_Aboriginal_Cutural_Centre_in_CiXiu_park_2018.jpg")
**辭修公園**，前名「陳誠紀念公園」，位於[臺灣](../Page/臺灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[泰山區同榮里尖凍山丘陵](../Page/泰山區_\(新北市\).md "wikilink")，佔地5.5公頃，原為[中華民國故副總統](../Page/中華民國副總統.md "wikilink")[陳誠與夫人](../Page/陳誠.md "wikilink")[譚祥的合葬墓園](../Page/譚祥.md "wikilink")，1995年陳家後人將遺骨遷葬至[高雄縣](../Page/高雄縣.md "wikilink")[大樹鄉](../Page/大樹鄉.md "wikilink")（今[高雄市](../Page/高雄市.md "wikilink")[大樹區](../Page/大樹區.md "wikilink")）[佛光山萬壽園納骨塔](../Page/佛光山.md "wikilink")\[1\]，該園交由泰山鄉公所管理，墓園經過改建更名為辭修公園\[2\]\[3\]。

## 園區介紹

辭修公園整座公園設立在面向[臺北盆地的山坡上](../Page/臺北盆地.md "wikilink")，是[林口臺地陡降丘陵的一部份](../Page/林口臺地.md "wikilink")\[4\]，由入口處向公園內部仰望，兩百多階的中央石階通往中央雕像平臺。平臺面向臺北盆地，視野廣闊，並立有紀念石碑，記述陳誠生平事蹟及[蔣中正褒揚石碑](../Page/蔣中正.md "wikilink")\[5\]。園內曾經有一間「陳　故副總統紀念館」及一座高六台尺半，基座高七台尺半，以青銅一噸鑄成的「陳　故副總統銅像」。此處目前已建設為一兼具親子休閒、生態教育、運動休憩的場所。階梯兩旁林相茂密，人工種植了[台灣山櫻](../Page/台灣山櫻.md "wikilink")、[龍柏](../Page/龍柏.md "wikilink")、[南洋杉](../Page/南洋杉.md "wikilink")、[樟樹](../Page/樟樹.md "wikilink")、[蒲葵](../Page/蒲葵.md "wikilink")，依地形設置木棧道、涼亭、草坪，使的原本肅穆的墓園成為[生態教育](../Page/生態教育.md "wikilink")、運動、休憩及親子活動空間。

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

[山](../Page/category:新北市公園.md "wikilink")
[分類:新北市墓葬](../Page/分類:新北市墓葬.md "wikilink")

[Category:泰山區 (臺灣)](../Category/泰山區_\(臺灣\).md "wikilink")

1.  臺灣地區地名查詢系統 辭修公園

2.  陳誠，字辭修

3.  辭修公園入口處告示：泰山鄉辭修公園原為已故副總統陳誠墓園，設於1964年，1994年遷移。原址交由泰山鄉公所管理。[臺灣省政府於](../Page/臺灣省政府.md "wikilink")1997年補助4000萬元新臺幣整修，設置木棧道，涼亭，中央平臺活動區，青少年及兒童遊戲區，石板步道等設施供民眾休憩之用。使原肅穆之墓園景象轉變為富朝氣活力之公園，並命名為辭修公園。

4.  泰山鄉公所網站\>遊憩與休閒\>辭修公園和尖凍山步道

5.