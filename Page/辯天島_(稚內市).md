[宗谷岬弁天島.JPG](https://zh.wikipedia.org/wiki/File:宗谷岬弁天島.JPG "fig:宗谷岬弁天島.JPG")
[Bentenjima_Wakkanai.jpg](https://zh.wikipedia.org/wiki/File:Bentenjima_Wakkanai.jpg "fig:Bentenjima_Wakkanai.jpg")

**辯天島**（）是[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[宗谷岬西北方約](../Page/宗谷岬.md "wikilink")1[公里外的](../Page/公里.md "wikilink")[無人島](../Page/無人島.md "wikilink")，行政上屬於[稚內市管轄](../Page/稚內市.md "wikilink")。辩天島面積0.005[平方公里](../Page/平方公里.md "wikilink")，島周長0.5[公里](../Page/公里.md "wikilink")，島上最高點為海拔20公尺，是日本實際控制領土的最北端。由於在該島上祭祀[佛教中的女神](../Page/佛教.md "wikilink")「[辩才天](../Page/辩才天.md "wikilink")」（也就是[印度教中的](../Page/印度教.md "wikilink")[辯才天女](../Page/辯才天女.md "wikilink")），因而得名。

辩天島周圍的岩礁和暗礁是海鳥、[海獅的棲息地](../Page/海獅.md "wikilink")，也有許多[海帶和](../Page/海帶.md "wikilink")[海膽](../Page/海膽.md "wikilink")。

## 所在地

  - 经纬度：北纬45度31分35秒，东经141度55分9秒\[1\]
  - 邮政编码：098-6758
  - 地址：北海道稚内市宗谷岬

## 相關連結

  - [辯天島](../Page/辯天島.md "wikilink")：其他同名島嶼。

## 外部連結

  - [辩天島的北海獅調查](http://www.fishexp.pref.hokkaido.jp/SHIKENIMA/551TO600/571/No.571todo.htm)（北海道海洋網）

## 參考文獻與註釋

[Category:北海道島嶼](../Category/北海道島嶼.md "wikilink")
[Category:稚內市](../Category/稚內市.md "wikilink")
[Category:日本無人島](../Category/日本無人島.md "wikilink")
[Category:日本海島嶼](../Category/日本海島嶼.md "wikilink")

1.