**辯機**，[唐初學問僧](../Page/唐朝.md "wikilink")，因与[唐太宗女](../Page/唐太宗.md "wikilink")[高陽公主淫亂被杀](../Page/高陽公主.md "wikilink")。

## 生平

根據《[大唐西域記](../Page/大唐西域記.md "wikilink")》卷末《記贊》所載，辯機十五歲時隨[道岳大師於](../Page/道岳.md "wikilink")[大總持寺出家](../Page/大總持寺.md "wikilink")，曾抄寫大量流傳至今的[佛教經典](../Page/佛教.md "wikilink")，以[佛法修為高深](../Page/佛法.md "wikilink")、[文學程度高及擅於撰文著稱](../Page/文學.md "wikilink")。\[1\]

辯機二十六歲時成為唐[貞觀十九年所成立的譯經場之九大](../Page/貞觀.md "wikilink")[綴文之一](../Page/綴文.md "wikilink")。一生中最大的成就是協助從[印度取](../Page/印度.md "wikilink")[梵文經書至中國的高僧唐](../Page/梵文.md "wikilink")[玄奘抄寫](../Page/玄奘.md "wikilink")[佛經及書撰其親身經歷](../Page/佛經.md "wikilink")《大唐西域記》。當年有此殊榮者全中國不滿三十位，辯機以二十六歲之姿擠入其中，可見其才華超群。他還協助玄奘譯出了《[顯揚聖教論頌](../Page/顯揚聖教論頌.md "wikilink")》1卷、《[六門陀羅尼經](../Page/六門陀羅尼經.md "wikilink")》1卷、《[佛地經](../Page/佛地經.md "wikilink")》1卷、《[天請問經](../Page/天請問經.md "wikilink")》1卷等重要經典\[2\]。

正當其貢獻達到高峰，由於曾經與[唐太宗第十七女](../Page/唐太宗.md "wikilink")[高陽公主通姦](../Page/高陽公主.md "wikilink")，被朝廷察觉，於唐貞觀二十三年被皇帝下詔處以[腰斬](../Page/腰斬.md "wikilink")，死時年方三十左右。

但也有學者認為，辯機其實沒有與高陽公主私通，是後世人穿鑿附會加上了私通之說\[3\]。

## 參考資料

## 外部連結

  - 辯機撰文《[大唐西域記讚](https://zh.wikisource.org/zh-hant/%E5%A4%A7%E5%94%90%E8%A5%BF%E5%9F%9F%E8%A8%98%E8%AE%9A)》

[B辩](../Category/唐朝僧人.md "wikilink")
[Category:中国旅游作家](../Category/中国旅游作家.md "wikilink")

1.  辯機《[大唐西域記讚](https://zh.wikisource.org/zh-hant/%E5%A4%A7%E5%94%90%E8%A5%BF%E5%9F%9F%E8%A8%98%E8%AE%9A)》
2.
3.