**農民曆**是以農業社會的時節與日常生活的參考用書，為中國古代[祖先在流傳下來的經歷及體驗](../Page/祖先.md "wikilink")，經千年累月撰寫與添加各項內容後而完成現今流傳農民曆。\[1\]

## 內容

農民曆主要內容包含[中國](../Page/中國.md "wikilink")[農曆帶有許多表示當天吉凶的](../Page/農曆.md "wikilink")[黃曆](../Page/黃曆.md "wikilink")。並附上[二十四節氣的日期表](../Page/二十四節氣.md "wikilink")，每天的吉凶宜忌、生肖運程等。不少古代曆法都是由[月亮算起](../Page/月亮.md "wikilink")，一個推測是黑夜中的月亮特別容易觀察，月亮盈虧一目了然，直至天文技術成熟後，才能觀察到太陽在曆法中的作用\[2\]。古代[農業](../Page/農業.md "wikilink")[經濟中](../Page/經濟.md "wikilink")，春天播種、秋天收耕，中國古代更以農耕為民生之主要，中國的農曆因此是結合了[陰曆與](../Page/陰曆.md "wikilink")[陽曆兩者的](../Page/陽曆.md "wikilink")[陰陽曆](../Page/陰陽曆.md "wikilink")，以應四[季與諸](../Page/季節.md "wikilink")[節氣](../Page/節氣.md "wikilink")。也因此，農曆中需[閏月](../Page/閏月.md "wikilink")，以合乎一年共四季、約365日的長度。

農民曆之內容多數記載農事相關，除了[陽曆](../Page/陽曆.md "wikilink")、[陰曆](../Page/陰曆.md "wikilink")、[時令節氣按年照月地順序排列](../Page/節氣.md "wikilink")，每日各有劃欄記載吉時凶辰、卦爻、節慶、沖煞之事，因此不僅可作為[年曆](../Page/年曆.md "wikilink")、[月曆](../Page/月曆.md "wikilink")、[日曆觀看](../Page/日曆.md "wikilink")，並可對於[婚事](../Page/結婚.md "wikilink")、[喪禮](../Page/喪禮.md "wikilink")、[祭祀](../Page/祭祀.md "wikilink")、[掃墓](../Page/掃墓.md "wikilink")、探病……等重要之事參考做為擇時挑日的依據，在生活上也會供年歲、[生肖](../Page/生肖.md "wikilink")、卦事查詢，通常在末頁附有[安太歲符咒與安太歲方法](../Page/安太歲.md "wikilink")。

## 影響性

農民曆在對於[農民來說是十分重要](../Page/農民.md "wikilink")，在中國傳統農業社會裡幾乎每戶人家會擁有一冊。在[台灣](../Page/台灣.md "wikilink")，主要是由各鄉鎮[農會發行提供](../Page/農會.md "wikilink")，封面多為黃色底[福祿壽三星圖像](../Page/福祿壽.md "wikilink")，背面則為[食物相剋中毒圖解](../Page/食物相剋中毒圖解.md "wikilink")。在台灣相當普遍成為台灣生活指南。

## 爭議

[黃曆版本眾多](../Page/黃曆.md "wikilink")，有官方版本的[通書](../Page/通書.md "wikilink")、道教編寫的版本、民間版本的農民曆，

官方版的通書以及道教均衍生許多流派，民間版本則是由平民或商人參考官方與道教版本而製作出的農民曆，

導致現代民眾在查詢擇日書籍時，常常發生爭端。

## 参見

  - [黃曆](../Page/黃曆.md "wikilink")
  - [陰曆](../Page/陰曆.md "wikilink")
  - [春牛圖](../Page/春牛圖.md "wikilink")
  - [安太歲習俗](../Page/安太歲習俗.md "wikilink")

## 参考文献

## 外部链接

  - [簡單農民曆](https://web.archive.org/web/20070831002241/http://you168.idv.tw/lm.htm)

  - [台灣大百科-農民曆](http://nrch.culture.tw/twpedia.aspx?id=12188)

[历](../Category/曆法.md "wikilink")
[Category:生活用品](../Category/生活用品.md "wikilink")
[Category:出版品](../Category/出版品.md "wikilink")

1.
2.  [The Mathematics of the Chinese
    Calendar](http://www.math.nus.edu.sg/aslaksen/calendar/cal.pdf)