**边和市**（）是[越南南部](../Page/越南.md "wikilink")[同奈省的一個市](../Page/同奈省.md "wikilink")，為該省之省会。

邊和市位于[胡志明市以东](../Page/胡志明市.md "wikilink")30公里，越南1号公路沿线。2009年人口70万（是年人口普查），是越南除[胡志明市等五大直辖市外](../Page/胡志明市.md "wikilink")，人口最多的省辖市。

## 历史

边和市是作为[南越首都](../Page/南越.md "wikilink")[西贡](../Page/西贡.md "wikilink")（今[胡志明市](../Page/胡志明市.md "wikilink")）的郊区发展起来。[法越战争之后](../Page/法越战争.md "wikilink")，数万名越南北部和中部难民（其中许多是[天主教徒](../Page/天主教.md "wikilink")）逃难到边和。

[越南战争期间](../Page/越南战争.md "wikilink")，[美国空军在此建立](../Page/美国空军.md "wikilink")，是除了[新山一空軍基地外離西貢最近的空軍基地](../Page/新山一空軍基地.md "wikilink")。不过，该市有不少市民是[越南南方民族解放阵线成员](../Page/越南南方民族解放阵线.md "wikilink")、或同情越共。

## 行政区划

边和市下辖23坊7社。
[Emblem_of_Bienhoa_City.svg](https://zh.wikipedia.org/wiki/File:Emblem_of_Bienhoa_City.svg "fig:Emblem_of_Bienhoa_City.svg")

  - 安平坊（Phường An Bình）
  - 平多坊（Phường Bình Đa）
  - 保和坊（Phường Bửu Hòa）
  - 保隆坊（Phường Bửu Long）
  - 和平坊（Phường Hòa Bình）
  - 呼狔坊（Phường Hố Nai）
  - 隆平坊（Phường Long Bình）
  - 隆平新坊（Phường Long Bình Tân）
  - 光荣坊（Phường Quang Vinh）
  - 决胜坊（Phường Quyết Thắng）
  - 三协坊（Phường Tam Hiệp）
  - 三和坊（Phường Tam Hòa）
  - 新边坊（Phường Tân Biên）
  - 新协坊（Phường Tân Hiệp）
  - 新和坊（Phường Tân Hòa）
  - 新梅坊（Phường Tân Mai）
  - 新丰坊（Phường Tân Phong）
  - 新进坊（Phường Tân Tiến）
  - 新万坊（Phường Tân Vạn）
  - 清平坊（Phường Thanh Bình）
  - 统一坊（Phường Thống Nhất）
  - 浪曳坊（Phường Trảng Dài）
  - 忠勇坊（Phường Trung Dũng）
  - 安和社（Xã An Hòa）
  - 协和社（Xã Hiệp Hòa）
  - 化安社（Xã Hóa An）
  - 隆兴社（Xã Long Hưng）
  - 福新社（Xã Phước Tân）
  - 三福社（Xã Tam Phước）
  - 三行社（Xã Tân Hạnh）

## 现状

边和市是越南南部的一个工业中心，日本、新加坡、美国、瑞士商人在附近投资兴建许多工厂和仓库，在娱乐业方面兴建一些游乐园、[夜总会和](../Page/夜总会.md "wikilink")[餐馆](../Page/餐馆.md "wikilink")。

## 參見

  - [陳上川](../Page/陳上川.md "wikilink")
  - [鄭懷德](../Page/鄭懷德.md "wikilink")

[Category:同奈省](../Category/同奈省.md "wikilink")
[Category:越南城市](../Category/越南城市.md "wikilink")
[Category:越南省会城市](../Category/越南省会城市.md "wikilink")