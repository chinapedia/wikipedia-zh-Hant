**邊界**，亦稱**疆界**，指用於劃分不同[政權所轄](../Page/政權.md "wikilink")[區域](../Page/區域.md "wikilink")、[領地的](../Page/領地.md "wikilink")[地理分界線](../Page/地理.md "wikilink")，進而可標示該區域的範圍。\[1\]边界与国界不是同义词，例如，深圳与香港之间的界线可称为边界，依法实施边界管理、边防检查、边防禁区等行政措施，但不是国界。

## 概要

國家之間的邊界概念，可通過[條約作書面上的规定](../Page/條約.md "wikilink")，或以實體的界標作實地规定。通常[國家](../Page/國家.md "wikilink")、[政治實體之間的边界地区有](../Page/政治實體.md "wikilink")[軍隊](../Page/軍隊.md "wikilink")[駐防](../Page/駐防.md "wikilink")。

国界有人为和天然的两种，人为的例如[界碑](../Page/界碑.md "wikilink")、界墙、[运河等](../Page/运河.md "wikilink")，天然的例如山脉河川等，但事实上即使有了天然国界，也还需要划定人为界线。用条约方式划定国界，要经过“定界”和“划界”两种手续，定界是概念上用条约文字来确定国界的走向和地位，绘制有国界线的地形详图，并作国界通过地点的简单记录等，划界是到现场实地去划定實體的界线，通常由缔约国共同组织进行的。对于根据有关国家的双边条约划定的国界，单方面不得加以改变。
\[2\]

設有邊防檢查的邊界地區稱為[口岸](../Page/口岸.md "wikilink")，在主要通商的口岸设有[海关](../Page/海关.md "wikilink")、[出入境管理及](../Page/出入境管理.md "wikilink")[檢疫設施](../Page/檢疫.md "wikilink")，也就是[CIQ](../Page/CIQ.md "wikilink")。

一般而言，[人口和](../Page/人口.md "wikilink")[貨物均不能完全自由地穿越邊界](../Page/貨物.md "wikilink")。人口需要[護照](../Page/護照.md "wikilink")、[簽證等文件以獲得在特定的邊界進出有關國家的權利](../Page/簽證.md "wikilink")。運輸[貨物越過邊界時亦可能需要繳納](../Page/貨物.md "wikilink")[關稅](../Page/關稅.md "wikilink")。人口在未經授權下穿越邊界稱為[偷渡](../Page/偷渡.md "wikilink")；貨物在未經授權下穿越邊界稱為[走私](../Page/走私.md "wikilink")。但也有例外，例如：歐洲的[申根公約](../Page/申根公約.md "wikilink")，和[愛爾蘭共和國及英國的](../Page/愛爾蘭共和國.md "wikilink")[北愛爾蘭之間的邊界](../Page/北愛爾蘭.md "wikilink")；但實際上，人口和貨物穿越上述兩者邊界時，已經獲得默許通過邊界。

## 边界与边境、国界与国境的区别

  - 边界：仅指地理分界线，可以指一个政权范围内的[行政分区分界线](../Page/行政区划.md "wikilink")，也可以指国界。
  - [国界](../Page/国界.md "wikilink")：指国家与国家之间的行政分界线。
  - [边境](../Page/边境.md "wikilink")：指临近边界的区域范围；
  - [国境](../Page/国境.md "wikilink")：指临近国界的区域范围，这类地区汉语也通常称为“**边疆**”。

然而，有部分的[島嶼國家由於沒有直接與週邊國家聯繫的陸上通道](../Page/島嶼國家.md "wikilink")，因此沒有兩國領土接壤的邊界，如[澳大利亞](../Page/澳大利亞.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[巴林](../Page/巴林.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[古巴](../Page/古巴.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[日本等等](../Page/日本.md "wikilink")；但部分島國設有往其他國家的陸路交通，如[英國有與](../Page/英國.md "wikilink")[法國聯繫的](../Page/法國.md "wikilink")[英法海底隧道](../Page/英法海底隧道.md "wikilink")，以及與[愛爾蘭接壤的邊界](../Page/愛爾蘭.md "wikilink")；[新加坡雖為島國](../Page/新加坡.md "wikilink")，但有與[馬來西亞聯繫的橋樑](../Page/馬來西亞.md "wikilink")，故有馬新之間的邊防檢查。

## 著名的歷史邊界

  - [萬里長城](../Page/萬里長城.md "wikilink")
    （[秦朝](../Page/秦朝.md "wikilink")/[汉朝与](../Page/汉朝.md "wikilink")[匈奴](../Page/匈奴.md "wikilink")；[隋朝](../Page/隋朝.md "wikilink")/[唐朝与](../Page/唐朝.md "wikilink")[東突厥](../Page/東突厥.md "wikilink")、[回紇](../Page/回紇.md "wikilink")；[明朝与](../Page/明朝.md "wikilink")[北元](../Page/北元.md "wikilink")、[韃靼](../Page/韃靼.md "wikilink")、[建州女真](../Page/建州女真.md "wikilink")、[朝鮮國边界](../Page/朝鮮國.md "wikilink")）
  - [哈德良長城](../Page/哈德良長城.md "wikilink")
  - [大同江](../Page/大同江.md "wikilink")（[唐朝与](../Page/唐朝.md "wikilink")[新罗](../Page/新罗.md "wikilink")、[渤海国与新罗边界](../Page/渤海国.md "wikilink")）
  - [六盘山](../Page/陇山.md "wikilink")（[唐朝与吐蕃边界](../Page/唐朝.md "wikilink")）
  - [大渡河](../Page/大渡河.md "wikilink")（[宋朝与](../Page/宋朝.md "wikilink")[大理国边界](../Page/大理国.md "wikilink")）
  - [白溝河](../Page/白溝河.md "wikilink")（[北宋与](../Page/北宋.md "wikilink")[契丹](../Page/契丹.md "wikilink")/[遼朝边界](../Page/遼朝.md "wikilink")）
  - [秦嶺淮河線](../Page/秦嶺淮河線.md "wikilink")（[金朝与](../Page/金朝.md "wikilink")[南宋边界](../Page/南宋.md "wikilink")）
  - [奄美大島與橫當島間海峽](../Page/奄美大島.md "wikilink")（日本与琉球边界）
  - [柏林圍牆](../Page/柏林圍牆.md "wikilink")、[德國國內邊界](../Page/德國國內邊界.md "wikilink")
    (東西德)
  - [一七線](../Page/一七線.md "wikilink")（南北越邊界）
  - 北纬50度线（[大日本帝國与](../Page/大日本帝國.md "wikilink")[俄羅斯帝国在](../Page/俄羅斯帝国.md "wikilink")[库页岛的分界线](../Page/库页岛.md "wikilink")）
  - [鐵幕](../Page/鐵幕.md "wikilink")（從[東德到](../Page/東德.md "wikilink")[南斯拉夫边界](../Page/南斯拉夫.md "wikilink")）
  - [竹幕](../Page/竹幕.md "wikilink")
  - [麥克馬洪線](../Page/麥克馬洪線.md "wikilink")（中國及印度边界，中國拒絕承認）
  - [寇松线](../Page/寇松线.md "wikilink")
  - [噴赤河](../Page/噴赤河.md "wikilink")

## 現有邊界

### 歐洲

  - [阿尔卑斯山脉](../Page/阿尔卑斯山脉.md "wikilink")（与边界）
  - [比利牛斯山脉](../Page/比利牛斯山脉.md "wikilink")（与边界）
  - [日内瓦湖](../Page/日内瓦湖.md "wikilink")（与边界）
  - [奥德河-尼斯河线](../Page/奥德河-尼斯河线.md "wikilink") （与边界）
  - [斯庫台湖](../Page/斯庫台湖.md "wikilink")（與邊界）
  - [高加索山脈](../Page/高加索山脈.md "wikilink")（與、、邊界）

### 美洲

  - [北纬49度线](../Page/北纬49度线.md "wikilink")、[美加邊界](../Page/美加邊界.md "wikilink")、[彩虹橋
    (尼亞加拉)](../Page/彩虹橋_\(尼亞加拉\).md "wikilink")（北部与边界）
  - [美墨邊界](../Page/美墨邊界.md "wikilink")（南部与边界）

### 亞洲

  - [綠线](../Page/綠线_\(以色列\).md "wikilink")（與、、边界）
  - [阿拉干山脉](../Page/阿拉干山脉.md "wikilink")（缅甸与孟加拉国边界）
  - [喜马拉雅山脉](../Page/喜马拉雅山脉.md "wikilink")（中国西藏与尼泊尔边界）
  - [长山山脉](../Page/长山山脉.md "wikilink")（越南与老挝边界）
  - [黑龙江](../Page/阿穆爾河.md "wikilink")、[乌苏里江](../Page/乌苏里江.md "wikilink")（与東段边界）
  - [鸭绿江](../Page/鸭绿江.md "wikilink")、[图们江](../Page/图们江.md "wikilink")、[中朝友誼橋](../Page/中朝友誼橋.md "wikilink")（与边界）
  - [北仑河](../Page/北仑河.md "wikilink")（中国与越南边界）
  - [幼发拉底河](../Page/幼发拉底河.md "wikilink")（伊朗与伊拉克边界）
  - [阿姆河](../Page/阿姆河.md "wikilink")（阿富汗与塔吉克斯坦、乌兹别克斯坦边界）
  - [三十八度線](../Page/三十八度線.md "wikilink")( 与边界)
  - [杜兰线](../Page/杜兰线.md "wikilink")（与边界）
  - [湄公河](../Page/湄公河.md "wikilink")（与边界）
  - [友誼關](../Page/友誼關.md "wikilink")（与邊界）
  - [臺灣海峽中線](../Page/臺灣海峽中線.md "wikilink")、金廈海峽（與實際軍事分界線）
  - [香港邊境禁區](../Page/香港邊境禁區.md "wikilink")（[深圳河](../Page/深圳河.md "wikilink")、[沙頭角河](../Page/沙頭角河.md "wikilink")、[中英街](../Page/中英街.md "wikilink")）（与邊界，歷史上是与/邊界）
  - [鸭涌河](../Page/鸭涌河.md "wikilink")（与邊界，歷史上是与/邊界）

### 非洲地區

  - [北纬22度线](../Page/北纬22度线.md "wikilink")（与部分边界）

## 相册

<File:Baarle-Nassau> frontière
café.jpg|[荷蘭](../Page/荷蘭.md "wikilink")（圖左）與[比利時](../Page/比利時.md "wikilink")（圖右）在[巴勒納紹的國界線](../Page/巴勒納紹.md "wikilink")（地面上的白十字記號）
<File:International> border at Wagah - evening flag lowering
ceremony.jpg|[印度与](../Page/印度.md "wikilink")[巴基斯坦边界的降旗仪式](../Page/巴基斯坦.md "wikilink")
<File:Peace> Arch, U.S.-Canada
border.jpg|位于[美国](../Page/美国.md "wikilink")[华盛顿州](../Page/华盛顿州.md "wikilink")[布莱恩与](../Page/布莱恩.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[卑诗省](../Page/卑诗省.md "wikilink")[素里市之间边境上的](../Page/素里市.md "wikilink")[和平拱门](../Page/和平拱门.md "wikilink")（Peace
Arch）。

## 参见

  - [邊境](../Page/邊境.md "wikilink")
  - [關境](../Page/關境.md "wikilink")
  - [緩衝區](../Page/緩衝區.md "wikilink")
  - [邊境管制](../Page/邊境管制.md "wikilink")
  - [海關](../Page/海關.md "wikilink")
  - [軍事分界線](../Page/軍事分界線.md "wikilink")
  - [非軍事區](../Page/非軍事區.md "wikilink")

## 参考文献

[边境分界线](../Category/边境分界线.md "wikilink")

1.  [藝術與建築索引典—界限](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300055590)
    於2011年4月11日查閱
2.  [李广民 国际法
    參見94頁](http://books.google.com/books?id=UnnxJeFq0hcC&pg=PA88&dq=%22%E9%A0%98%E5%9C%9F%22&hl=zh-TW&rview=1&cd=2#v=onepage&q=%22%E9%82%8A%E7%95%8C%E7%9A%84%22&f=false)