**辻步美**（）是[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。[Mausu
Promotion所屬](../Page/Mausu_Promotion.md "wikilink")，[愛媛縣出身](../Page/愛媛縣.md "wikilink")，[血型O型](../Page/血型.md "wikilink")。高中畢業後，到[東京入讀](../Page/東京.md "wikilink")[Pro-Fit聲優養成所的第一期生](../Page/Pro-Fit聲優養成所.md "wikilink")。第一個主要角色是《[極上生徒會](../Page/極上生徒會.md "wikilink")》中的桂美奈萌。2017年7月7日宣佈與同屬聲優的[佐藤ミチル結婚](../Page/佐藤ミチル.md "wikilink")。\[1\]

## 作品列表

主角、主要角色為**粗體**。

### 電視動畫

**2004年**

  - [忘卻的旋律](../Page/忘卻的旋律.md "wikilink")（美少女C）

**2005年**

  - [極上生徒会](../Page/極上生徒会.md "wikilink")（**桂美奈萌**）
  - [韋駄天翔](../Page/韋駄天翔.md "wikilink")（男子）
  - [增血鬼果林](../Page/增血鬼果林.md "wikilink")（女子）

**2006年**

  - [歡迎加入NHK\!](../Page/歡迎加入NHK!.md "wikilink")（女子、青梅竹馬、義妹、女僕、其他）

  - [鍵姬物語 永久愛麗絲輪舞曲](../Page/鍵姬物語_永久愛麗絲輪舞曲.md "wikilink")（女子高中生）

  - （****）

  - [BLACK BLOOD
    BROTHERS](../Page/BLACK_BLOOD_BROTHERS.md "wikilink")（陳）

  - [夜明前的琉璃色 〜Crescent Love〜](../Page/夜明前的琉璃色.md "wikilink")（少女）

  - [血色花園](../Page/血色花園.md "wikilink")（**蘿絲·西迪**）

**2007年**

  - [Venus Versus
    Virus](../Page/Venus_Versus_Virus.md "wikilink")（**蘿拉、萊拉**）

  - [校園烏托邦 學美向前衝！](../Page/校園烏托邦_學美向前衝！.md "wikilink")（女子）

  - [風之聖痕](../Page/風之聖痕.md "wikilink")（鈴原花音）

  - [天翔少女](../Page/天翔少女.md "wikilink")（**艾莉婕·馮·黛德麗**）

  - [向陽素描](../Page/向陽素描.md "wikilink")（電視聲音2（\#4））

  - [PRISM ARK](../Page/PRISM_ARK.md "wikilink")（）

  - （****）

  - [南家三姊妹](../Page/南家三姊妹.md "wikilink")（千秋的同學）

  - （）

  - [羅密歐×茱麗葉](../Page/羅密歐×茱麗葉.md "wikilink")（女子（\#9））

**2008年**

  - [PERSONA -trinity soul-](../Page/女神異聞錄3.md "wikilink")（茅野友哉）
  - [CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")（**折原梢**）
  - [地下城與勇士](../Page/地下城與勇士.md "wikilink")（**伊柯茜亞·淳**）
  - [零之使魔\~三美姬的輪舞](../Page/零之使魔.md "wikilink")（**ベアトリス・イヴォンヌ・フォン・クルデンホルフ**）

**2009年**

  - [Slap-up party 亞蘭特戰記](../Page/地下城与勇士.md "wikilink")（**伊莉希亞·淳**）
  - [戀愛班長](../Page/戀愛班長.md "wikilink")（）
  - [秀逗魔導士EVOLUTION-R](../Page/秀逗魔導士_\(動畫\).md "wikilink")（少女）
  - [迷宮塔 ～烏魯克之劍～](../Page/迷宮塔_\(動畫\).md "wikilink")（女子）
  - [BLEACH 斬魄刀異聞篇](../Page/BLEACH_\(動畫\).md "wikilink")（雀蜂）

**2010年**

  - （**越野光**）

**2011年**

  - [純白交響曲](../Page/純白交響曲.md "wikilink")（雨石由香里、少女、打工者）

**2012年**

  - [零之使魔F](../Page/零之使魔.md "wikilink")（ベアトリス・イヴォンヌ・フォン・クルデンホルフ）
  - [人類衰退之後](../Page/人類衰退之後.md "wikilink")（妖精）

**2013年**

  - [你好 七葉](../Page/你好_七葉.md "wikilink")
  - [探險托里蘭托 -千年的真寶-](../Page/探險托里蘭托.md "wikilink")（女兒）
  - [我的腦內戀礙選項](../Page/我的腦內戀礙選項.md "wikilink")（**遊王子謳歌**）

**2014年**

  - [咲-Saki- 全國篇](../Page/咲-Saki-.md "wikilink")（薄墨初美）
  - [鐵金剛少女Z](../Page/鐵金剛少女Z.md "wikilink")（巴拉達克〈小巴拉〉）
  - [CROSSANGE
    天使與龍的輪舞](../Page/CROSSANGE_天使與龍的輪舞.md "wikilink")（可可/可可.李維）
  - [巴哈姆特之怒 GENESIS](../Page/巴哈姆特之怒.md "wikilink")（拉法耶爾）

**2015年**

  - [星夢學園](../Page/星夢學園_\(動畫\).md "wikilink")（綿貫美美）
  - [Fate/stay night \[Unlimited Blade
    Works](../Page/Fate/stay_night.md "wikilink")\] （）

### OVA

  - （女子）

  - [Pinky:st](../Page/Pinky:st.md "wikilink")（）

  - [Dead Girls](../Page/RED_GARDEN.md "wikilink")（****）

  - [夢想夏鄉 -A Summer Day's
    Dream-](../Page/夢想夏鄉_-A_Summer_Day's_Dream-.md "wikilink")（）

### 電子遊戲

  - [CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")（**折原梢**）

  - [極上生徒會](../Page/極上生徒會.md "wikilink")（**桂美奈萌**）

  - [悠久之櫻](../Page/悠久之櫻.md "wikilink")（）

  - [BLOOD+ 〜Final Piece〜](../Page/BLOOD+.md "wikilink")（）

  - （**水木·J·大神**）

  - （****）

  - [聖靈之心](../Page/聖靈之心.md "wikilink")（**えこ**）

  - [東京七姊妹](../Page/東京七姊妹.md "wikilink")（**遊佐メモル**）

### CD

  - （水木「wish」） / LACA-5388 / ASIN B0008JH70Y / 2005.5.25

  - [極上生徒會](../Page/極上生徒會.md "wikilink") 精選專輯 極上音樂集 （桂美奈萌「Baby Cat's
    eye」） / GBCA-3 / ASIN B000B6FEZO / 2005.11.10

  - / KICA-789 / ASIN B000F6YRFI / 2006.5.24

  - 片頭曲 DRAMATIC☆GIRLY
    （[神田朱未](../Page/神田朱未.md "wikilink")、[大原沙耶香](../Page/大原沙耶香.md "wikilink")、[葉月繪理乃](../Page/葉月繪理乃.md "wikilink")、[關山美沙紀](../Page/關山美沙紀.md "wikilink")、辻步美）
    / KICM-3126 / ASIN B000FUTZHY / 2006.7.26

  - / KICA-797 / ASIN B000GG4CSO / 2006.8.23

  - [Venus Versus Virus](../Page/Venus_Versus_Virus.md "wikilink") 角色曲專輯
    （） / ASIN B000N3SWJ0 / 2007.3.21

  - [天翔少女](../Page/天翔少女.md "wikilink") 角色曲單曲1 （「Youthful
    Days」「LIEBE～愛～」「FLY AWAY」） / ASIN B000VOOM3Q /
    2007.10.24

### 廣播劇CD

  - （）

  - （）

### 廣播

  - （2007年6月～2008年3月）

  - [BBQR網上廣播](../Page/BBQR.md "wikilink")「」（2007年10月～12月）

  - [Filn帝國的興亡](../Page/Filn帝國的興亡.md "wikilink")（2007年10月～2008年1月）

  - [Filn帝国的逆襲](../Page/Filn帝国的逆襲.md "wikilink")（2008年2月～2008年6月）

## 外部連結

  - [官方網站「」](http://ayumino-kobeya.littlestar.jp/index.html)

  -
  - [](http://ameblo.jp/ayu1130ayu/) （官方blog）

  - [官方個人檔案](https://web.archive.org/web/20080509062921/http://profit-v.com/pro/talent_tsuji.html)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:愛媛縣出身人物](../Category/愛媛縣出身人物.md "wikilink")

1.  \[[https://twitter.com/tsuji_ayumi/status/883158670346821632/photo/1?ref_src=twsrc%5Etfw\&ref_url=http%3A%2F%2Fhkacger.com%2Farchives%2F41045\]私事でまことに恐縮ですが、本日、T's](https://twitter.com/tsuji_ayumi/status/883158670346821632/photo/1?ref_src=twsrc%5Etfw&ref_url=http%3A%2F%2Fhkacger.com%2Farchives%2F41045%5D私事でまことに恐縮ですが、本日、T's)
    Factory所属の佐藤ミチルさんと入籍致しましたコトをご報告いたします。まだまだ未熟な私ですが、これからも声優として成長できるよう努力してまいりますので、どうぞよろしくお願いいたします☆２０１７年７月７日辻
    あゆみ