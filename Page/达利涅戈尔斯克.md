**達利涅戈爾斯克**（），是[俄罗斯](../Page/俄罗斯.md "wikilink")[滨海边疆区的一座城市](../Page/滨海边疆区.md "wikilink")。

## 歷史

[清朝時名叫](../Page/清朝.md "wikilink")**野豬河**，原是中國領土，1860年因[北京条约割让給](../Page/北京条约.md "wikilink")[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")。俄語原名，即「野豬河」的音譯。于在[1972年俄羅斯遠東地區地名變更中改为今名](../Page/1972年俄羅斯遠東地區地名變更.md "wikilink")，俄语意为“遥远的山城”。2010年时有人口37,503人。

该地以而闻名于世。

## 外部链接

  - [达利涅戈尔斯克官方网站](http://www.dalnegorsk.ru)

[Д](../Category/滨海边疆区城市.md "wikilink")
[达](../Category/外满洲城市.md "wikilink")