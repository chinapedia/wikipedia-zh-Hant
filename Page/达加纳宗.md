[BhutanDagana.png](https://zh.wikipedia.org/wiki/File:BhutanDagana.png "fig:BhutanDagana.png")

**達加納宗**，（）（
དར་དཀར་ན་རྫོང་ཁག་）是[不丹二十個宗](../Page/不丹.md "wikilink")(dzongkhag)之一，地處不丹西南部，面積達1,534km<sup>2</sup>，人口大約27,292。

傳說達加納人在17世紀曾是不遵守法律的集團，[夏忠](../Page/夏忠.md "wikilink")（Shabdrung）需要派遣士兵來控制當地。

**宗**轄下有十一個格窩：

  - [Dorona
    Gewog](https://web.archive.org/web/20050816075047/http://www.dop.gov.bt/fyp/09/dg_Dorona.pdf)
  - [Drujegang
    Gewog](https://web.archive.org/web/20050530201732/http://www.dop.gov.bt/fyp/09/dg_Drujegang.pdf)
  - [Gesarling
    Gewog](https://web.archive.org/web/20050817021203/http://www.dop.gov.bt/fyp/09/dg_Gesarling.pdf)
  - [Goshi
    Gewog](https://web.archive.org/web/20041022214603/http://www.dop.gov.bt/fyp/09/dg_Goshi.pdf)
  - [Kana
    Gewog](https://web.archive.org/web/20041022224503/http://www.dop.gov.bt/fyp/09/dg_Kana.pdf)
  - [Khebisa
    Gewog](https://web.archive.org/web/20050816160616/http://www.dop.gov.bt/fyp/09/dg_Khebisa.pdf)
  - [Lajab
    Gewog](https://web.archive.org/web/20050817045217/http://www.dop.gov.bt/fyp/09/dg_Lajab.pdf)
  - [Tashiding
    Gewog](https://web.archive.org/web/20050816130441/http://www.dop.gov.bt/fyp/09/dg_Tashiding.pdf)
  - [Tsangkha
    Gewog](https://web.archive.org/web/20050817073451/http://www.dop.gov.bt/fyp/09/dg_Tsangkha.pdf)
  - [Tsendagana
    Gewog](https://web.archive.org/web/20050816162558/http://www.dop.gov.bt/fyp/09/dg_Tsendagana.pdf)
  - [Tseza
    Gewog](https://web.archive.org/web/20041022224929/http://www.dop.gov.bt/fyp/09/dg_Tseza.pdf)

## 參見

  - [不丹行政區劃](../Page/不丹行政區劃.md "wikilink")

## 外部链接

  - [達加納宗](http://www.dop.gov.bt/gpis/DzProfile/dzoProfilemain.asp?dzoCode=3&profYear=2003)

[Category:不丹行政區劃](../Category/不丹行政區劃.md "wikilink")