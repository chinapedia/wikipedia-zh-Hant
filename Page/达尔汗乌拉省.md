[Map_mn_darkhan-uul_aimag.png](https://zh.wikipedia.org/wiki/File:Map_mn_darkhan-uul_aimag.png "fig:Map_mn_darkhan-uul_aimag.png")
**达尔汗乌拉省**（），位於[蒙古國中北部](../Page/蒙古國.md "wikilink")，四周為[色楞格省包圍](../Page/色楞格省.md "wikilink")。面積3,275平方公里，人口94,625
(2011年)。首府[达尔汗](../Page/达尔汗.md "wikilink")。
原為蒙古兩個[直轄市之一](../Page/直轄市.md "wikilink")（另一個是首都[烏蘭巴托](../Page/烏蘭巴托.md "wikilink")）。

## 参考文献

[达尔汗乌拉省](../Category/达尔汗乌拉省.md "wikilink")
[Darkhan-Uul](../Category/蒙古国省份.md "wikilink")