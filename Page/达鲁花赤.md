**达鲁花赤**（），一作“**達嚕噶齊**”，是[蒙古语](../Page/蒙古语.md "wikilink")，意为“掌印者”，是[蒙古帝国歷史上一种特殊的](../Page/蒙古帝国.md "wikilink")[职官](../Page/职官.md "wikilink")[称谓](../Page/称谓.md "wikilink")，與它同源的有另一字**答魯合剌禿孩**，意謂「[提調](../Page/提調.md "wikilink")」，另有宣差，[持節之意](../Page/持節.md "wikilink")。

**达鲁花赤**最初为[成吉思汗所设](../Page/成吉思汗.md "wikilink")，曾广泛通行于[蒙古帝国和](../Page/蒙古帝国.md "wikilink")[元朝](../Page/元朝.md "wikilink")。达鲁花赤原意为“掌[印者](../Page/印.md "wikilink")”，后来成为[长官或首长的通称](../Page/长官.md "wikilink")。在元朝的各级地方政府裡面，均设有达鲁花赤一职，掌握地方[行政和](../Page/行政.md "wikilink")[军事实权](../Page/军事.md "wikilink")，是地方各级的最高长官。在元朝[中央政府裡面](../Page/中央政府.md "wikilink")，也有某些部门设置达鲁花赤官职。达鲁花赤一般必须由[蒙古人或](../Page/蒙古人.md "wikilink")[色目人担任](../Page/色目人.md "wikilink")，这种做法被认为具有强烈的[民族主義](../Page/民族主義.md "wikilink")[差別待遇色彩](../Page/差別待遇.md "wikilink")。但是，也有其他民族的人担任达鲁花赤的记载。在[金帳汗國](../Page/金帳汗國.md "wikilink")，他們與[八思哈有關](../Page/八思哈.md "wikilink")，但有不同。

[明朝以后](../Page/明朝.md "wikilink")，达鲁花赤官职被废除。

## 释义和起源

达鲁花赤在[蒙古语中原意是](../Page/蒙古语.md "wikilink")“掌印者”。1222年，[成吉思汗开始在占领区设置达鲁花赤官职](../Page/成吉思汗.md "wikilink")，负责监治地方军政事务。

当时，蒙古[军队征服一个地区之后](../Page/军队.md "wikilink")，无力独立维持在当地的统治，因此委托当地人治理，而设置达鲁花赤来进行监督。达鲁花赤掌握象征决策拍板权力的官员[印玺](../Page/印章_\(東亞\).md "wikilink")，所以实际上是当地的最高统治者。

这是[蒙古帝国维持对于被其征服地区统治权的措施](../Page/蒙古帝国.md "wikilink")。在[巴格达与](../Page/巴格达.md "wikilink")[弗拉基米尔也有达鲁花赤](../Page/弗拉基米尔.md "wikilink")。

## 设置

[元朝建立后](../Page/元朝.md "wikilink")，达鲁花赤的设置沿袭了下来，并普遍地在不同部门和不同层次的位置上设置达鲁花赤。

此时，达鲁花赤已经不是一种官职的名称，而是指代一类官职，即某部门的最高[长官](../Page/长官.md "wikilink")。

此外，蒙古帝国的各[汗国在与元帝国分立之后](../Page/蒙古四大汗国.md "wikilink")，也在当地[汗国设置达鲁花赤官职](../Page/汗国.md "wikilink")。

### 地方政府

[元朝地方的](../Page/元朝.md "wikilink")[诸路万户府](../Page/诸路万户府.md "wikilink")、[路](../Page/路.md "wikilink")、[府](../Page/府.md "wikilink")、[州](../Page/州.md "wikilink")、[县各级](../Page/县.md "wikilink")，都在原来的[总管](../Page/总管.md "wikilink")、[府尹](../Page/府尹.md "wikilink")、[知府](../Page/知府.md "wikilink")、[知州](../Page/知州.md "wikilink")、[知县之外](../Page/知县.md "wikilink")，设置达鲁花赤一员，虽然[品秩与原地方长官相同](../Page/品秩.md "wikilink")，但权力在地方长官之上。

在统辖地方军队的各级[万户府](../Page/万户府.md "wikilink")、[元帅府](../Page/元帅府.md "wikilink")、[千户所](../Page/千户所.md "wikilink")，以及监管军民的[宣抚司](../Page/宣抚司.md "wikilink")、[招讨司等部门也设置达鲁花赤官职](../Page/招讨司.md "wikilink")。

[路的達魯花赤是正三品](../Page/路.md "wikilink")，[散府的是正四品](../Page/散府.md "wikilink")，與中央[六部](../Page/六部.md "wikilink")[尚書同](../Page/尚書.md "wikilink")，[州的達魯花赤由戶口決定](../Page/州.md "wikilink")，大的是從四品，小的是從五品，[縣的達魯花赤](../Page/縣.md "wikilink")，大的是從六品，小的是從七品。

### 中央政府

元朝也在[朝廷的许多部门设置达鲁花赤的官职](../Page/朝廷.md "wikilink")，同样为该部门的最高长官。例如，诸路宝钞都[提举司](../Page/提举司.md "wikilink")、运粮提举司、各大[寺院总管府](../Page/寺院.md "wikilink")、营缮司等部门及其各级机构，也都普遍设置有达鲁花赤一职。

[诸侯王下的各级](../Page/诸侯.md "wikilink")[属官](../Page/属官.md "wikilink")，也设置达鲁花赤，由诸[王自行指派](../Page/王.md "wikilink")。

### 民族限制

达鲁花赤一员，虽然[品秩与原地方长官相同](../Page/品秩.md "wikilink")，但权力在地方[长官之上](../Page/长官.md "wikilink")，因此是实际的最高长官，幾乎全是[蒙古人充任](../Page/蒙古人.md "wikilink")，加強了其民族統治。

[元武宗](../Page/元武宗.md "wikilink")[至大二年](../Page/至大.md "wikilink")（1309年），[汉人](../Page/汉人.md "wikilink")、[南人](../Page/南人.md "wikilink")、[契丹人和](../Page/契丹人.md "wikilink")[女真人和其他](../Page/女真人.md "wikilink")[少数民族被明令禁止擔任](../Page/少数民族.md "wikilink")，并规定达鲁花赤必须委由蒙古人，如果蒙古人中无合适人选，[公家可在](../Page/公家.md "wikilink")[色目人中选任](../Page/色目人.md "wikilink")；凡是[汉族或其他民族人士担任达鲁花赤的](../Page/汉族.md "wikilink")，被发现一律追回撤销，而且此人將永不叙用。不過，[皇帝可以賜給](../Page/皇帝.md "wikilink")[汉族或其他民族人士](../Page/汉族.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")[姓氏](../Page/姓氏.md "wikilink")，該員隨即成為蒙古人，如[賀太平即擔任過达鲁花赤](../Page/賀太平.md "wikilink")。

但是，也有由于当地条件恶劣，蒙古人不願擔任达鲁花赤，而直接让[汉人充任的例子](../Page/汉人.md "wikilink")。根据《[元史](../Page/元史.md "wikilink")》，也有[汉族和其他](../Page/汉族.md "wikilink")[少数民族未經改變](../Page/少数民族.md "wikilink")[民族](../Page/民族.md "wikilink")[籍貫](../Page/籍貫.md "wikilink")，即担任达鲁花赤的记载。

### 废除

[朱元璋建立](../Page/朱元璋.md "wikilink")[明朝之后](../Page/明朝.md "wikilink")，全面废除了达鲁花赤此一官職。

## 参看

  - [蒙古帝国](../Page/蒙古帝国.md "wikilink")
  - [职官](../Page/职官.md "wikilink")

[Category:蒙古官制](../Category/蒙古官制.md "wikilink")
[Category:監察官](../Category/監察官.md "wikilink")
[Category:元朝地方官制](../Category/元朝地方官制.md "wikilink")
[Category:元朝军事](../Category/元朝军事.md "wikilink")
[Category:蒙古语借词](../Category/蒙古语借词.md "wikilink")