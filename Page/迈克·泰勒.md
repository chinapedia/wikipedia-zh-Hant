**迈克·泰勒**（，），[北愛爾蘭](../Page/北愛爾蘭.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，司職[守門員](../Page/守門員.md "wikilink")。

## 球會生平

米克·泰萊於1997年由[修咸頓加盟](../Page/南安普顿足球俱乐部.md "wikilink")[富咸](../Page/富勒姆足球俱乐部.md "wikilink")，協助球隊從第四級的[丙組升級到](../Page/英格蘭足球聯賽.md "wikilink")[英超](../Page/英超.md "wikilink")。2001年7月富咸從[祖雲達斯收購](../Page/尤文图斯足球俱乐部.md "wikilink")[雲達沙](../Page/埃德文·范德萨.md "wikilink")，米克泰萊的正選地位不保\[1\]。11月米克泰萊透露希望離開富咸\[2\]。但12月雲達沙因斷腳而長期缺陣，米克泰萊重獲正選並且有出色的表現\[3\]。

2003年8月8日米克·泰萊以外借身份加盟[伯明翰城一年](../Page/伯明翰足球俱乐部.md "wikilink")\[4\]，2004年3月22日他與球會簽下正式合約，轉會費150萬[英鎊](../Page/英鎊.md "wikilink")，簽約至2008年\[5\]。他加盟伯明翰後一直有非常穩定的發揮\[6\]\[7\]，獲選2003/04年球季北愛爾蘭FWA國際球員\[8\]，並且入選了英超最佳陣容。

2007年初[-{zh-hans:科林·多伊尔;
zh-hk:杜爾;}-一度取代米克](../Page/科林·多伊尔.md "wikilink")·泰萊在伯明翰城的正選位置\[9\]，但在重返[英超後的伯明翰城再次起用米克](../Page/英超.md "wikilink")·泰萊擔任正選\[10\]。2008年6月14日米克泰萊續約兩年\[11\]。

[2009/10年球季伯明翰城從](../Page/2009年至2010年英格蘭超級聯賽.md "wikilink")[曼城借用](../Page/曼彻斯特城足球俱乐部.md "wikilink")[祖·赫特](../Page/乔·哈特.md "wikilink")，整季一直佔據正選席位，米克·泰萊僅在聯賽上陣兩場，均是對祖·赫特因借用條款未能上陣的母會曼城。[翌季伯明翰城又從另一支](../Page/2010年至2011年英格蘭超級聯賽.md "wikilink")[曼徹斯特球隊](../Page/曼徹斯特.md "wikilink")[曼聯正式羅致](../Page/曼徹斯特聯足球俱樂部.md "wikilink")[-{zh-hans:本·福斯特;zh-hk:賓·科士打;}-](../Page/賓·科士打_\(足球運動員\).md "wikilink")，季初續約一年的米克·泰萊繼續擔任板凳球員，僅獲派在盃賽上陣4場，但在[聯賽盃決賽亦只能屈居後備席廂](../Page/2011年英格蘭聯賽盃決賽.md "wikilink")。隨著球隊於球季結束後降班，米克·泰萊成為6名約滿被放棄的球員之一\[12\]。

2011年11月24日年屆40歲的米克·泰萊以短約形式加盟[英冠球會](../Page/英冠.md "wikilink")[列斯聯](../Page/列斯聯足球會.md "wikilink")，為期至翌年1月\[13\]。未有機會為列斯聯上陣，米克·泰萊於2012年3月12日獲緊急借用到另一支[英冠球隊](../Page/英冠.md "wikilink")[米禾爾直至球季結束](../Page/米尔沃尔足球俱乐部.md "wikilink")\[14\]。

## 國家隊

米克泰萊的父親是[英國人](../Page/英國.md "wikilink")，而母親是[德國人](../Page/德國.md "wikilink")<small>\[15\]</small>，由於在海外出生而領取[英國](../Page/英國.md "wikilink")[護照](../Page/護照.md "wikilink")，因而可以選擇代表[聯合王國中任何一地](../Page/聯合王國.md "wikilink")，現時是[北愛爾蘭的常規門將](../Page/北愛爾蘭足球代表隊.md "wikilink")，只有[-{zh-hans:罗伊·卡罗尔;
zh-hk:卡路爾;}-](../Page/萊·卡路爾.md "wikilink")（Roy
Carroll）對他的地位稍有威脅<small>\[16\]</small>。代表出賽超過80次。於1999年首次披上國家隊球衣出戰[德國](../Page/德國國家足球隊.md "wikilink")，以0-3見負。2008年8月20日作客對[蘇格蘭](../Page/蘇格蘭足球代表隊.md "wikilink")，因隊長[艾朗·曉士受傷缺陣](../Page/艾朗·曉士.md "wikilink")，米克泰萊獲選戴上隊長臂章<small>\[17\]</small>。

## 榮譽

  - 法茵堡（Farnborough Town）\[18\]

<!-- end list -->

  - [南部联赛超级组冠軍](../Page/英格兰南部足球联赛超级组.md "wikilink")：1993–94

<!-- end list -->

  - [富咸](../Page/富勒姆足球俱乐部.md "wikilink")\[19\]

<!-- end list -->

  - [英乙冠軍](../Page/英格蘭足球聯賽.md "wikilink")：1998–99
  - [英甲冠軍](../Page/英格蘭足球聯賽.md "wikilink")：2000–01

<!-- end list -->

  - [伯明翰城](../Page/伯明翰足球會.md "wikilink")\[20\]

<!-- end list -->

  - [英冠亚军](../Page/英格蘭足球冠軍聯賽.md "wikilink")：2006–07
  - 英冠亚军：2008–09
  - [英格蘭聯賽盃冠軍](../Page/英格蘭聯賽盃.md "wikilink")：[2011](../Page/2011年英格蘭聯賽盃決賽.md "wikilink")

## 參考資料

## 外部連結

  -
  - [Profile on Birmingham City
    website](https://web.archive.org/web/20080517053427/http://www.blues.premiumtv.co.uk/page/ProfilesDetail/0,,10412~8076,00.html)

  - [Profile at The Official Website for the Irish Football
    Association](http://www.irishfa.com/squad-profiles/128/senior/maik-taylor-goalkeeper/)

[Category:德國裔英國人](../Category/德國裔英國人.md "wikilink")
[Category:北愛爾蘭足球運動員](../Category/北愛爾蘭足球運動員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:班列特球員](../Category/班列特球員.md "wikilink")
[Category:修咸頓球員](../Category/修咸頓球員.md "wikilink")
[Category:富咸球員](../Category/富咸球員.md "wikilink")
[Category:伯明翰城球員](../Category/伯明翰城球員.md "wikilink")
[Category:列斯聯球員](../Category/列斯聯球員.md "wikilink")
[Category:米禾爾球員](../Category/米禾爾球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")

1.  [Taylor Premiership hopes in
    doubt](http://news.bbc.co.uk/sport2/hi/northern_ireland/1467614.stm)

2.  [Taylor to quit the
    Cottage](http://news.bbc.co.uk/sport2/hi/northern_ireland/1656218.stm)

3.  [Taylor could quit
    Cottagers](http://news.bbc.co.uk/sport2/hi/football/teams/f/fulham/2864395.stm)

4.  [Blues finally land
    Taylor](http://news.bbc.co.uk/sport2/hi/football/teams/b/birmingham_city/3126051.stm)

5.  [Blues seal Taylor
    deal](http://news.bbc.co.uk/sport2/hi/football/teams/b/birmingham_city/3547587.stm)

6.  [Bruce salutes
    Taylor](http://news.bbc.co.uk/sport2/hi/football/teams/b/birmingham_city/3592705.stm)

7.  [Bruce in Taylor
    tribute](http://news.bbc.co.uk/sport2/hi/football/teams/b/birmingham_city/3628789.stm)

8.  [Blues dominate
    awards](http://news.bbc.co.uk/sport2/hi/football/irish/3698221.stm)

9.  [Doyle signs new Birmingham
    deal](http://news.bbc.co.uk/sport2/hi/football/teams/b/birmingham_city/6582831.stm)

10. [Chelsea 3-2
    Birmingham](http://news.bbc.co.uk/sport2/hi/football/eng_prem/6931067.stm)

11. [Taylor agrees new Birmingham
    deal](http://news.bbc.co.uk/sport2/hi/football/teams/b/birmingham_city/7453823.stm)

12.

13.

14.

15. [Family ties for Taylor at
    Windsor](http://news.bbc.co.uk/sport2/hi/football/irish/4607577.stm)

16. [Taylor eyes start against
    Danes](http://news.bbc.co.uk/sport2/hi/football/internationals/5412326.stm)

17. [Taylor to captain NI in
    Glasgow](http://news.bbc.co.uk/sport2/hi/football/internationals/7571078.stm)

18.
19.
20.