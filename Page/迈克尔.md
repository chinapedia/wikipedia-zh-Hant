|         |
| ------- |
| **迈克尔** |
| 性别：     |
| 起源：     |
| 含义：     |

**迈克尔**（）是源于[希伯来语的](../Page/希伯来语.md "wikilink")[名字](../Page/名字.md "wikilink")，同時也是[基督徒使用的](../Page/基督徒.md "wikilink")[聖名之一](../Page/聖名.md "wikilink")。

## 含义

“迈克尔”源自[犹太教圣典](../Page/犹太教.md "wikilink")[旧约全书中的](../Page/旧约全书.md "wikilink")[大天使](../Page/大天使.md "wikilink")[圣米迦勒](../Page/圣米迦勒.md "wikilink")，按照[塔木德中的传统解释](../Page/塔木德.md "wikilink")，意思是“似神的”，“与神相似者”（希伯来语词尾[el的意思是神](../Page/el结尾名字列表.md "wikilink")）。米迦勒在[伊斯兰教中的对应者是](../Page/伊斯兰教.md "wikilink")[米卡伊来](../Page/米卡伊来.md "wikilink")，他是清洗先知[穆罕默德](../Page/穆罕默德.md "wikilink")[心臟的天使之一](../Page/心臟.md "wikilink")。

## 缩略形式

在英语中的常见缩略形式（昵称和小名）有Mike（迈克）、Mick（-{米克}-）、Mikiel、Mikey、Mikael、Mic和Mickey（[米奇](../Page/米奇.md "wikilink")）等，Maicol则是Michael的异体。在[德语中的缩略形式有Meik与Maik](../Page/德语.md "wikilink")，[俄语的对应名字](../Page/俄语.md "wikilink")[米哈伊尔的常见爱称为](../Page/米哈伊尔.md "wikilink")（米沙）。

## 各种语言中的对应形式

  - [希伯来语](../Page/希伯来语.md "wikilink")：מיכאל（[米迦勒](../Page/米迦勒.md "wikilink")）

  - （[米哈伊尔](../Page/米哈伊尔.md "wikilink")）

  - [希腊语](../Page/希腊语.md "wikilink")：Μιχαήλ（米海尔）

  - [拉丁语](../Page/拉丁语.md "wikilink")：Michaelus（米卡埃卢斯）

  - [英语](../Page/英语.md "wikilink")：Michael（迈克尔）

  - [俄语](../Page/俄语.md "wikilink")：Михаи́л（[米哈伊尔](../Page/米哈伊尔.md "wikilink")）

  - [西班牙语](../Page/西班牙语.md "wikilink")：Miguel（米格尔）

  - [葡萄牙语](../Page/葡萄牙语.md "wikilink")：Miguel（米格尔）

  - [法语](../Page/法语.md "wikilink")：Michel（米歇尔）

  - [德语](../Page/德语.md "wikilink")：Michael（米夏埃尔）

  - [意大利语](../Page/意大利语.md "wikilink")：Michele（米凯莱）

  - [塞尔维亚语](../Page/塞尔维亚语.md "wikilink")：Mihajlo（米哈伊洛）

  - [罗马尼亚语](../Page/罗马尼亚语.md "wikilink")：Mihai（米哈伊）

  - [荷兰语](../Page/荷兰语.md "wikilink")：Michaël（米哈埃尔）

  - [波兰语](../Page/波兰语.md "wikilink")：Michał（米哈乌）

  - [匈牙利语](../Page/匈牙利语.md "wikilink")：Mihály（米哈伊）

  - [瑞典语](../Page/瑞典语.md "wikilink")：Mikael（米卡埃尔）

  - [丹麦语](../Page/丹麦语.md "wikilink")：Mikael（米卡埃尔）

  - [挪威语](../Page/挪威语.md "wikilink")；Mikal（米卡尔）

  - [捷克语](../Page/捷克语.md "wikilink")：Michal（米哈尔）

  - [斯洛伐克语](../Page/斯洛伐克语.md "wikilink")：Michal（米哈尔）

  - [斯洛文尼亚语](../Page/斯洛文尼亚语.md "wikilink")：Mihael（米哈埃尔）

  - [乌克兰语](../Page/乌克兰语.md "wikilink")：Михайло（米哈伊洛）

  - [克罗地亚语](../Page/克罗地亚语.md "wikilink")：Mihovil（米霍维尔）

  - [芬兰语](../Page/芬兰语.md "wikilink")：Michael（米凯尔）

  - [亚美尼亚语](../Page/亚美尼亚语.md "wikilink")：Միխաիլ（米卡埃良）

  - [格鲁吉亚语](../Page/格鲁吉亚语.md "wikilink")：მიხეილი（米赫伊利）

  - [加泰罗尼亚语](../Page/加泰罗尼亚语.md "wikilink")：Miquel（米克尔）

  - [威尔士语](../Page/威尔士语.md "wikilink")：Meical（梅卡尔）

## 著名人士

注意：在汉语中译为米哈伊尔、米哈伊洛和米格尔的人物不列入下表。关于这些人物，请分别参见主题条目：

  - [米哈伊尔](../Page/米哈伊尔.md "wikilink")
  - [米哈伊洛](../Page/米哈伊洛.md "wikilink")
  - [米格尔](../Page/米格尔.md "wikilink")

### 君士坦丁堡牧首

  - [米海尔一世
    (君士坦丁堡牧首)](../Page/米海尔一世_\(君士坦丁堡牧首\).md "wikilink")，[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")[牧首](../Page/牧首.md "wikilink")
  - [米海尔二世 (君士坦丁堡牧首)](../Page/米海尔二世_\(君士坦丁堡牧首\).md "wikilink")，君士坦丁堡牧首
  - [米海尔三世 (君士坦丁堡牧首)](../Page/米海尔三世_\(君士坦丁堡牧首\).md "wikilink")，君士坦丁堡牧首
  - [米海尔四世 (君士坦丁堡牧首)](../Page/米海尔四世_\(君士坦丁堡牧首\).md "wikilink")，君士坦丁堡牧首

### 拜占庭帝国

  - [米海尔一世](../Page/米海尔一世.md "wikilink")，[拜占庭帝国](../Page/拜占庭帝国.md "wikilink")[皇帝](../Page/拜占庭帝国皇帝.md "wikilink")
  - [米海尔二世](../Page/米海尔二世.md "wikilink")，拜占庭帝国皇帝
  - [米海尔三世](../Page/米海尔三世.md "wikilink")，拜占庭帝国皇帝
  - [米海尔四世](../Page/米海尔四世.md "wikilink")，拜占庭帝国皇帝
  - [米海尔五世](../Page/米海尔五世.md "wikilink")，拜占庭帝国皇帝
  - [米海尔六世](../Page/米海尔六世.md "wikilink")，拜占庭帝国皇帝
  - [米海尔七世](../Page/米海尔七世.md "wikilink")，拜占庭帝国皇帝
  - [米海尔八世](../Page/米海尔八世.md "wikilink")，拜占庭帝国皇帝
  - [米海尔九世](../Page/米海尔九世.md "wikilink")，拜占庭帝国王储和共治皇帝

### 瓦拉几亚

  - [勇敢的米哈伊](../Page/勇敢的米哈伊.md "wikilink")，[瓦拉几亚](../Page/瓦拉几亚.md "wikilink")[大公](../Page/大公.md "wikilink")
  - [米哈伊·拉科维策](../Page/米哈伊·拉科维策.md "wikilink")，瓦拉几亚大公
  - [米哈伊·苏茨乌](../Page/米哈伊·苏茨乌.md "wikilink")，瓦拉几亚大公

### 罗马尼亚

  - [米哈伊一世](../Page/米哈伊一世_\(羅馬尼亞\).md "wikilink")，[罗马尼亚国王](../Page/罗马尼亚.md "wikilink")

### 波兰

  - [米哈乌·克雷布特·维希尼奥维茨基](../Page/米哈乌·克雷布特·维希尼奥维茨基.md "wikilink")，[波兰国王](../Page/波兰.md "wikilink")

### 体育界

  - [迈克尔·泰森](../Page/迈克尔·泰森.md "wikilink")，[美国拳击运动员](../Page/美国.md "wikilink")
  - [迈克尔·乔丹](../Page/迈克尔·乔丹.md "wikilink")，美国篮球运动员
  - [迈克尔·芬利](../Page/迈克尔·芬利.md "wikilink")，美国篮球运动员
  - [迈克尔·菲尔普斯](../Page/迈克尔·菲尔普斯.md "wikilink")，美国游泳运动员
  - [迈克尔·舒马赫](../Page/迈克尔·舒马赫.md "wikilink")，[德国赛车运动员](../Page/德国.md "wikilink")
  - [迈克尔·巴拉克](../Page/迈克尔·巴拉克.md "wikilink")，德国足球运动员
  - [迈克尔·欧文](../Page/迈克尔·欧文.md "wikilink")，[英国足球运动员](../Page/英国.md "wikilink")

### 文化界

  - [迈克
    (作家)](../Page/迈克_\(作家\).md "wikilink")，[香港寫作人](../Page/香港.md "wikilink")

### 娱乐界

  - [迈克尔·杰克逊](../Page/迈克尔·杰克逊.md "wikilink")，美国流行歌手
  - [迈克尔·基特](../Page/迈克尔·基特.md "wikilink")，美国演員
  - [迈克尔·贝](../Page/迈克尔·贝.md "wikilink")，美国导演
  - [迈克尔·道格拉斯](../Page/迈克尔·道格拉斯.md "wikilink")，美国演員
  - [麥克比特](../Page/麥克比特.md "wikilink")，美国演員
  - [迈克瑞恩](../Page/迈克瑞恩.md "wikilink")，在华美国演员
  - [瑞恩·迷嘉尔](../Page/瑞恩·迷嘉尔.md "wikilink") (MICHAÉL)，美国流行歌手
  - [徐宗祥喜劇演員](../Page/徐宗祥.md "wikilink")

### 設計師

  - [迈克尔·索涅](../Page/迈克尔·索涅.md "wikilink")，德国產品設計師
  - [迈克尔·葛瑞夫](../Page/迈克尔·葛瑞夫.md "wikilink")，美国產品設計師

[Category:名](../Category/名.md "wikilink")