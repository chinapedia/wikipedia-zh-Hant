**邁阿密-戴德縣**（），是位於[美國](../Page/美國.md "wikilink")[佛羅里達州東南部的一個縣](../Page/佛羅里達州.md "wikilink")。面積6,297平方公里。根據[美國人口調查局](../Page/美國人口調查局.md "wikilink")2000年統計，共有人口2,253,362人，是該州人口最多的一個縣。2005年估計人口達2,376,014，躍居全美第八大縣。縣治[邁阿密](../Page/邁阿密.md "wikilink")。

成立於1836年2月4日。舊稱**戴德縣**，紀念在[第二次塞米諾爾戰爭中被殺的](../Page/第二次塞米諾爾戰爭.md "wikilink")[法蘭西斯·蘭霍恩·戴德](../Page/法蘭西斯·蘭霍恩·戴德.md "wikilink")
(Francis Langhorne
Dade)。現縣名在1997年7月22日以[公民投票形式通過](../Page/公民投票.md "wikilink")。

[M](../Category/佛羅里達州行政區劃.md "wikilink")
[M](../Category/迈阿密.md "wikilink")