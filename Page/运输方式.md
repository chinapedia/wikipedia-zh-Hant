[Hydrogen_bicycle.jpg](https://zh.wikipedia.org/wiki/File:Hydrogen_bicycle.jpg "fig:Hydrogen_bicycle.jpg")
[Rutan-Defiant-N57KS.jpg](https://zh.wikipedia.org/wiki/File:Rutan-Defiant-N57KS.jpg "fig:Rutan-Defiant-N57KS.jpg")
[Cruise_Night,_Lexington_MA.jpg](https://zh.wikipedia.org/wiki/File:Cruise_Night,_Lexington_MA.jpg "fig:Cruise_Night,_Lexington_MA.jpg")
[QueenMary2_in_Hamburg.jpg](https://zh.wikipedia.org/wiki/File:QueenMary2_in_Hamburg.jpg "fig:QueenMary2_in_Hamburg.jpg")郵輪\]\]

## 交通工具的種類

交通工具的種類大致可分為陸上、海上及空中的交通工具。

### 陸上的交通工具

  - [車輛](../Page/車輛.md "wikilink")
      - [腳踏車](../Page/腳踏車.md "wikilink")
      - [人力車](../Page/人力車.md "wikilink")
      - [摩托車](../Page/摩托車.md "wikilink")
          - [速克達](../Page/速克達.md "wikilink")
      - [汽車](../Page/汽車.md "wikilink")
          - [貨車](../Page/貨車.md "wikilink")
          - [計程車](../Page/計程車.md "wikilink")
          - [公車](../Page/公車.md "wikilink")
          - [公共小型公車](../Page/公共小型公車.md "wikilink")
          - [公車捷運](../Page/公車捷運.md "wikilink")
      - [火車](../Page/火車.md "wikilink")

### 水上的交通工具

  - [船](../Page/船.md "wikilink")
  - [獨木舟](../Page/獨木舟.md "wikilink")
  - [竹筏](../Page/竹筏.md "wikilink")
  - [水上電單車](../Page/水上電單車.md "wikilink")
  - [輪船](../Page/輪船.md "wikilink")
  - [雙體船](../Page/雙體船.md "wikilink")
  - [氣墊船](../Page/氣墊船.md "wikilink")
  - [潛水艇](../Page/潛水艇.md "wikilink")

### 空中的交通工具

  - [熱氣球](../Page/熱氣球.md "wikilink")
  - [飛船](../Page/飛船.md "wikilink")
  - [飛機](../Page/飛機.md "wikilink")
  - [直升機](../Page/直升機.md "wikilink")
  - [無人飛機](../Page/無人飛機.md "wikilink")
  - [旋翼機](../Page/旋翼機.md "wikilink")
  - [撲翼機](../Page/撲翼機.md "wikilink")
  - [滑翔機](../Page/滑翔機.md "wikilink")

### 軌道上的交通工具

[44458_Water_Orton.jpg](https://zh.wikipedia.org/wiki/File:44458_Water_Orton.jpg "fig:44458_Water_Orton.jpg")
[GinzaLine1379.jpg](https://zh.wikipedia.org/wiki/File:GinzaLine1379.jpg "fig:GinzaLine1379.jpg")[銀座線](../Page/東京地下鐵銀座線.md "wikilink")\]\]

  - [重型鐵路系統](../Page/重型鐵路系統.md "wikilink")
  - [中型鐵路系統](../Page/中型鐵路系統.md "wikilink")
  - [輕軌運輸系統](../Page/輕軌運輸系統.md "wikilink")


\* [地鐵](../Page/地鐵.md "wikilink")

  -   - [捷運](../Page/捷運.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）

  - [輕軌](../Page/輕軌.md "wikilink")

  - [高速鐵路](../Page/高速鐵路.md "wikilink")

  - [城際鐵路](../Page/城際鐵路.md "wikilink")

  - [通勤鐵路](../Page/通勤鐵路.md "wikilink")

  - [齒軌鐵路](../Page/齒軌鐵路.md "wikilink")

  - [纜索鐵路](../Page/纜索鐵路.md "wikilink")

  - [產業鐵路](../Page/產業鐵路.md "wikilink")

  - [貨運鐵路](../Page/貨運鐵路.md "wikilink")

  - [庭園鐵路](../Page/庭園鐵路.md "wikilink")

  - [遊園鐵路](../Page/遊園鐵路.md "wikilink")

  - [有軌電車](../Page/有軌電車.md "wikilink")

  - [磁浮列車](../Page/磁浮列車.md "wikilink")

  - [台車](../Page/台車.md "wikilink")

  - [空中纜車](../Page/空中纜車.md "wikilink")

  - [單軌列車](../Page/單軌列車.md "wikilink")

### 太空中的交通工具

  - [太空船](../Page/太空船.md "wikilink")
  - [火箭](../Page/火箭.md "wikilink")
  - [登月艇](../Page/登月艇.md "wikilink")
  - [月球探測車](../Page/月球探測車.md "wikilink")
  - [人造衛星](../Page/人造衛星.md "wikilink")
  - [太空梭](../Page/太空梭.md "wikilink")
  - [輔助火箭](../Page/輔助火箭.md "wikilink")
  - [太空探測器](../Page/太空探測器.md "wikilink")

[Category:交通](../Category/交通.md "wikilink")