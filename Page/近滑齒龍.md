**近滑齒龍**（屬名：*Plesioliopleurodon*）意為「接近[滑齒龍](../Page/滑齒龍.md "wikilink")」，是種已滅絕[海生爬行動物](../Page/海生爬行動物.md "wikilink")。近滑齒龍屬於[上龍亞目](../Page/上龍亞目.md "wikilink")，生存於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。近滑齒龍是由[肯尼思·卡彭特](../Page/肯尼思·卡彭特.md "wikilink")（Kenneth
Carpenter）命名。化石是一個完整的頭骨、下頜、[頸椎](../Page/頸椎.md "wikilink")、右[鳥喙骨](../Page/鳥喙骨.md "wikilink")，發現於[懷俄明州的](../Page/懷俄明州.md "wikilink")[貝爾富什](../Page/貝爾富什.md "wikilink")[頁岩層](../Page/頁岩.md "wikilink")，年代為[森諾曼階下層](../Page/森諾曼階.md "wikilink")。卡彭特認為近滑齒龍的最近親是[殘酷滑齒龍](../Page/滑齒龍.md "wikilink")（化石發現於[歐洲的](../Page/歐洲.md "wikilink")[牛津階](../Page/牛津階.md "wikilink")），故得名。\[1\]

近滑齒龍具有8對牙齒，牙齒的[橫剖面接近圓形](../Page/橫剖面.md "wikilink")，牙齒的外側平滑，頸部的[肋骨只有單頭](../Page/肋骨.md "wikilink")（[侏儸紀上龍類具有兩個頭](../Page/侏儸紀.md "wikilink")），[鳥喙骨之間有修長的胸間骨棒](../Page/鳥喙骨.md "wikilink")。

## 參考資料

[Category:上龍亞目](../Category/上龍亞目.md "wikilink")
[Category:白堊紀爬行動物](../Category/白堊紀爬行動物.md "wikilink")

1.