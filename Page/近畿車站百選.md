**近畿車站百選**（）是為了配合[日本的](../Page/日本.md "wikilink")[鐵道日](../Page/鐵道日.md "wikilink")（）紀念活動，透過公開募集再經選考委員評選決定，所選出100個能代表[近畿地方特色的](../Page/近畿地方.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。該活動在2000年至2003年間，以每年一回共分四回的方式，自[國土交通省近畿運輸局管轄的範圍內](../Page/國土交通省.md "wikilink")（包括[京都府](../Page/京都府.md "wikilink")、[大阪府](../Page/大阪府.md "wikilink")、[滋賀縣](../Page/滋賀縣.md "wikilink")、[兵庫縣](../Page/兵庫縣.md "wikilink")、[奈良縣與](../Page/奈良縣.md "wikilink")[和歌山縣](../Page/和歌山縣.md "wikilink")）選出100個車站。整個名單的評選方式，基本上是仿效自關東運輸局在1997年至2001年間所舉辦的[關東車站百選](../Page/關東車站百選.md "wikilink")（）評選活動。

## 入選車站一覽

\-{H|車站=\>zh-cn:站;}- -{H|車站=\>zh-hk:站;車站=\>zh-mo:站;}-

### 第1回

<table>
<thead>
<tr class="header">
<th><p>-{zh-tw:車站;zh-cn:车站;zh-hk:車站;}-</p></th>
<th><p>隸屬業者</p></th>
<th><p>所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/西日本旅客鐵道.md" title="wikilink">西日本旅客鐵道</a></p></td>
<td><p><a href="../Page/大阪府.md" title="wikilink">大阪府</a><a href="../Page/大阪府.md" title="wikilink">大阪府</a><a href="../Page/北區_(大阪市).md" title="wikilink">北區</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/阪急電鐵.md" title="wikilink">阪急電鐵</a></p></td>
<td><p>大阪府大阪府北區</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/南海電氣鐵道.md" title="wikilink">南海電氣鐵道</a></p></td>
<td><p>大阪府<a href="../Page/堺市.md" title="wikilink">堺市</a><a href="../Page/西區_(堺市).md" title="wikilink">西區</a></p></td>
</tr>
<tr class="even">
<td><p>（）</p></td>
<td><p>西日本旅客鐵道、南海電氣鐵道</p></td>
<td><p>大阪府<a href="../Page/泉南郡.md" title="wikilink">泉南郡</a><a href="../Page/田尻町.md" title="wikilink">田尻町</a></p></td>
</tr>
<tr class="odd">
<td><p><br />
（入選後改名「」）</p></td>
<td></td>
<td><p>大阪府<a href="../Page/貝塚市.md" title="wikilink">貝塚市</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/萬博紀念公園車站_(大阪府).md" title="wikilink">{{ST</a></p></td>
<td><p><a href="../Page/大阪高速鐵道.md" title="wikilink">大阪高速鐵道</a></p></td>
<td><p>大阪府<a href="../Page/吹田市.md" title="wikilink">吹田市</a></p></td>
</tr>
<tr class="odd">
<td><p>（）</p></td>
<td><p><a href="../Page/大阪市交通局.md" title="wikilink">大阪市交通局</a></p></td>
<td><p>大阪府大阪府<a href="../Page/住之江區.md" title="wikilink">住之江區</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>大阪市交通局</p></td>
<td><p>大阪府大阪府<a href="../Page/中央區_(大阪市).md" title="wikilink">中央區</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>西日本旅客鐵道</p></td>
<td><p><a href="../Page/京都府.md" title="wikilink">京都府</a><a href="../Page/京都市.md" title="wikilink">京都市</a><a href="../Page/下京區.md" title="wikilink">下京區</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>西日本旅客鐵道</p></td>
<td><p><a href="../Page/京都府.md" title="wikilink">京都府</a><a href="../Page/京都市.md" title="wikilink">京都市</a><a href="../Page/伏見區.md" title="wikilink">伏見區</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/京阪電氣鐵道.md" title="wikilink">京阪電氣鐵道</a></p></td>
<td><p><a href="../Page/京都府.md" title="wikilink">京都府</a><a href="../Page/宇治市.md" title="wikilink">宇治市</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/叡山電鐵.md" title="wikilink">叡山電鐵</a></p></td>
<td><p><a href="../Page/京都府.md" title="wikilink">京都府</a><a href="../Page/京都市.md" title="wikilink">京都市</a><a href="../Page/左京區.md" title="wikilink">左京區</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/北近畿丹後鐵道.md" title="wikilink">北近畿丹後鐵道</a></p></td>
<td><p><a href="../Page/京都府.md" title="wikilink">京都府</a><a href="../Page/宮津市.md" title="wikilink">宮津市</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>西日本旅客鐵道</p></td>
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a><a href="../Page/豐岡市.md" title="wikilink">豐岡市</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>西日本旅客鐵道</p></td>
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a><a href="../Page/神戸市.md" title="wikilink">神戸市</a><a href="../Page/長田區.md" title="wikilink">長田區</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伊丹車站_(阪急).md" title="wikilink">{{ST</a></p></td>
<td><p>阪急電鐵</p></td>
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a><a href="../Page/伊丹市.md" title="wikilink">伊丹市</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阪神電氣鐵道.md" title="wikilink">阪神電氣鐵道</a></p></td>
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a><a href="../Page/西宮市.md" title="wikilink">西宮市</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/山陽電氣鐵道.md" title="wikilink">山陽電氣鐵道</a></p></td>
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a><a href="../Page/神戶市.md" title="wikilink">神戶市</a><a href="../Page/須磨區.md" title="wikilink">須磨區</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/神戶電鐵.md" title="wikilink">神戶電鐵</a></p></td>
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a><a href="../Page/神戶市.md" title="wikilink">神戶市</a><a href="../Page/北區_(神戶市).md" title="wikilink">北區</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/六甲摩耶鐵道.md" title="wikilink">六甲摩耶鐵道</a></p></td>
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a><a href="../Page/神戶市.md" title="wikilink">神戶市</a><a href="../Page/灘區.md" title="wikilink">灘區</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/近畿日本鐵道.md" title="wikilink">近畿日本鐵道</a></p></td>
<td><p><a href="../Page/奈良縣.md" title="wikilink">奈良縣</a><a href="../Page/橿原市.md" title="wikilink">橿原市</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/學園前車站_(奈良縣).md" title="wikilink">{{ST</a></p></td>
<td><p>近畿日本鐵道</p></td>
<td><p><a href="../Page/奈良縣.md" title="wikilink">奈良縣</a><a href="../Page/奈良市.md" title="wikilink">奈良市</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/坂本車站_(滋賀縣).md" title="wikilink">{{ST</a></p></td>
<td><p>京阪電氣鐵道</p></td>
<td><p><a href="../Page/滋賀縣.md" title="wikilink">滋賀縣</a><a href="../Page/大津市.md" title="wikilink">大津市</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/近江鐵道.md" title="wikilink">近江鐵道</a></p></td>
<td><p><a href="../Page/滋賀縣.md" title="wikilink">滋賀縣</a><a href="../Page/東近江市.md" title="wikilink">東近江市</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>西日本旅客鐵道</p></td>
<td><p><a href="../Page/和歌山縣.md" title="wikilink">和歌山縣</a><a href="../Page/海南市.md" title="wikilink">海南市</a></p></td>
</tr>
</tbody>
</table>

### 第2回

| \-{zh-tw:車站;zh-cn:车站;zh-hk:車站;}-             | 隸屬業者                                     | 所在地                                                                                                      |
| -------------------------------------------- | ---------------------------------------- | -------------------------------------------------------------------------------------------------------- |
| [環球影城車站](../Page/環球影城車站.md "wikilink")（）     | 西日本旅客鐵道                                  | 大阪府大阪府[此花區](../Page/此花區.md "wikilink")                                                                   |
| [大阪阿部野橋車站](../Page/大阪阿部野橋車站.md "wikilink")   | 近畿日本鐵道                                   | 大阪府大阪府[阿倍野區](../Page/阿倍野區.md "wikilink")                                                                 |
| [萱島車站](../Page/萱島車站.md "wikilink")           | 京阪電氣鐵道                                   | 大阪府[寢屋川市](../Page/寢屋川市.md "wikilink")                                                                    |
| [枚方市車站](../Page/枚方市車站.md "wikilink")         | 京阪電氣鐵道                                   | 大阪府[枚方市](../Page/枚方市.md "wikilink")                                                                      |
| [難波車站](../Page/難波車站.md "wikilink")           | 南海電氣鐵道                                   | 大阪府大阪府[中央區](../Page/中央區_\(大阪市\).md "wikilink")                                                           |
| [堺車站](../Page/堺車站.md "wikilink")             | 南海電氣鐵道                                   | 大阪府[堺市](../Page/堺市.md "wikilink")[堺區](../Page/堺區.md "wikilink")                                          |
| [千里中央車站](../Page/千里中央車站.md "wikilink")       | [北大阪急行電鐵](../Page/北大阪急行電鐵.md "wikilink") | 大阪府[豐中市](../Page/豐中市.md "wikilink")                                                                      |
| [住吉公園車站](../Page/住吉公園車站.md "wikilink")       | [阪堺電氣軌道](../Page/阪堺電氣軌道.md "wikilink")   | 大阪府大阪府[住吉區](../Page/住吉區.md "wikilink")                                                                   |
| [西舞鶴車站](../Page/西舞鶴車站.md "wikilink")         | 西日本旅客鐵道、北近畿丹後鐵道                          | [京都府](../Page/京都府.md "wikilink")[舞鶴市](../Page/舞鶴市.md "wikilink")                                         |
| [網野車站](../Page/網野車站.md "wikilink")           | 北近畿丹後鐵道                                  | [京都府](../Page/京都府.md "wikilink")[京丹後市](../Page/京丹後市.md "wikilink")                                       |
| [嵐山車站](../Page/嵐山車站_\(阪急\).md "wikilink")    | 阪急電鐵                                     | [京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[西京區](../Page/西京區.md "wikilink")         |
| [小火車保津峽車站](../Page/小火車保津峽車站.md "wikilink")（） | [嵯峨野觀光鐵道](../Page/嵯峨野觀光鐵道.md "wikilink") | [京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[西京區](../Page/西京區.md "wikilink")         |
| [烏丸御池車站](../Page/烏丸御池車站.md "wikilink")       | [京都市交通局](../Page/京都市交通局.md "wikilink")   | [京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[中京區](../Page/中京區.md "wikilink")         |
| [神戶車站](../Page/神戶車站_\(兵庫縣\).md "wikilink")   | 西日本旅客鐵道                                  | [兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")[中央區](../Page/中央區_\(神戶市\).md "wikilink") |
| [舞子車站](../Page/舞子車站.md "wikilink")           | 西日本旅客鐵道                                  | [兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")[垂水區](../Page/垂水區.md "wikilink")         |
| [寶塚車站](../Page/寶塚車站.md "wikilink")           | 阪急電鐵                                     | [兵庫縣](../Page/兵庫縣.md "wikilink")[寶塚市](../Page/寶塚市.md "wikilink")                                         |
| [蘆屋車站](../Page/蘆屋車站_\(阪神\).md "wikilink")    | 阪神電氣鐵道                                   | [兵庫縣](../Page/兵庫縣.md "wikilink")[蘆屋市](../Page/蘆屋市.md "wikilink")                                         |
| [風城中央車站](../Page/風城中央車站.md "wikilink")（）     | [神戶電鐵](../Page/神戶電鐵.md "wikilink")       | [兵庫縣](../Page/兵庫縣.md "wikilink")[三田市](../Page/三田市.md "wikilink")                                         |
| [日生中央車站](../Page/日生中央車站.md "wikilink")       | [能勢電鐵](../Page/能勢電鐵.md "wikilink")       | [兵庫縣](../Page/兵庫縣.md "wikilink")[川邊郡](../Page/川邊郡.md "wikilink")[豬名川町](../Page/豬名川町.md "wikilink")       |
| [港元町車站](../Page/港元町車站.md "wikilink")（）       | [神戶市交通局](../Page/神戶市交通局.md "wikilink")   | [兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")[中央區](../Page/中央區.md "wikilink")         |
| [北宇智車站](../Page/北宇智車站.md "wikilink")         | 西日本旅客鐵道                                  | [奈良縣](../Page/奈良縣.md "wikilink")[五條市](../Page/五條市.md "wikilink")                                         |
| [福神車站](../Page/福神車站.md "wikilink")           | 近畿日本鐵道                                   | [奈良縣](../Page/奈良縣.md "wikilink")[吉野郡](../Page/吉野郡.md "wikilink")[大淀町](../Page/大淀町.md "wikilink")         |
| [長濱車站](../Page/長濱車站.md "wikilink")           | 西日本旅客鐵道                                  | [滋賀縣](../Page/滋賀縣.md "wikilink")[長濱市](../Page/長濱市.md "wikilink")                                         |
| [纜車嚴曆寺車站](../Page/纜車嚴曆寺車站.md "wikilink")（）   | [比叡山鐵道](../Page/比叡山鐵道.md "wikilink")     | [滋賀縣](../Page/滋賀縣.md "wikilink")[大津市](../Page/大津市.md "wikilink")                                         |
| [那智車站](../Page/那智車站.md "wikilink")           | 西日本旅客鐵道                                  | [和歌山縣](../Page/和歌山縣.md "wikilink")[東牟婁郡](../Page/東牟婁郡.md "wikilink")[那智勝浦町](../Page/那智勝浦町.md "wikilink") |

### 第3回

| \-{zh-tw:車站;zh-cn:车站;zh-hk:車站;}-           | 隸屬業者                                         | 所在地                                                                                                    |
| ------------------------------------------ | -------------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| [天王寺車站](../Page/天王寺車站.md "wikilink")       | 西日本旅客鐵道                                      | 大阪府大阪府[天王寺區](../Page/天王寺區.md "wikilink")                                                               |
| [上本町車站](../Page/大阪上本町車站.md "wikilink")     | 近畿日本鐵道                                       | 大阪府大阪府[天王寺區](../Page/天王寺區.md "wikilink")                                                               |
| [岬公園車站](../Page/岬公園車站.md "wikilink")（）     | 南海電氣鐵道                                       | 大阪府[泉南郡](../Page/泉南郡.md "wikilink")[岬町](../Page/岬町.md "wikilink")                                      |
| [私市車站](../Page/私市車站.md "wikilink")         | 京阪電氣鐵道                                       | 大阪府[交野市](../Page/交野市.md "wikilink")                                                                    |
| [十三車站](../Page/十三車站.md "wikilink")         | 阪急電鐵                                         | 大阪府大阪府[淀川區](../Page/淀川區.md "wikilink")                                                                 |
| [鶴見綠地車站](../Page/鶴見綠地車站.md "wikilink")     | 大阪市交通局                                       | 大阪府大阪府[鶴見區](../Page/鶴見區_\(大阪市\).md "wikilink")                                                         |
| [和泉中央車站](../Page/和泉中央車站.md "wikilink")     | [大阪府都市開發](../Page/大阪府都市開發.md "wikilink")     | 大阪府[和泉市](../Page/和泉市.md "wikilink")                                                                    |
| [二-{条}-車站](../Page/二条車站.md "wikilink")     | 西日本旅客鐵道                                      | [京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[中京區](../Page/中京區.md "wikilink")       |
| [國際會館車站](../Page/國際會館車站.md "wikilink")     | [京都市交通局](../Page/京都市交通局.md "wikilink")       | [京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[左京區](../Page/左京區.md "wikilink")       |
| [御室仁和寺車站](../Page/御室仁和寺車站.md "wikilink")   | [京福電氣鐵道](../Page/京福電氣鐵道.md "wikilink")       | [京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[右京區](../Page/右京區.md "wikilink")       |
| [大江車站](../Page/大江車站_\(京都府\).md "wikilink") | 北近畿丹後鐵道                                      | [京都府](../Page/京都府.md "wikilink")[福知山市](../Page/福知山市.md "wikilink")                                     |
| [播州赤穗車站](../Page/播州赤穗車站.md "wikilink")     | 西日本旅客鐵道                                      | [兵庫縣](../Page/兵庫縣.md "wikilink")[赤穗市](../Page/赤穗市.md "wikilink")                                       |
| [餘部車站](../Page/餘部車站.md "wikilink")         | 西日本旅客鐵道                                      | [兵庫縣](../Page/兵庫縣.md "wikilink")[美方郡](../Page/美方郡.md "wikilink")[香美町](../Page/香美町.md "wikilink")       |
| [川西能勢口車站](../Page/川西能勢口車站.md "wikilink")   | 阪急電鐵、[能勢電鐵](../Page/能勢電鐵.md "wikilink")      | [兵庫縣](../Page/兵庫縣.md "wikilink")[川西市](../Page/川西市.md "wikilink")                                       |
| [武庫川車站](../Page/武庫川車站.md "wikilink")       | 阪神電氣鐵道                                       | [兵庫縣](../Page/兵庫縣.md "wikilink")[西宮市](../Page/西宮市.md "wikilink")                                       |
| [人丸前車站](../Page/人丸前車站.md "wikilink")       | [山陽電氣鐵道](../Page/山陽電氣鐵道.md "wikilink")       | [兵庫縣](../Page/兵庫縣.md "wikilink")[明石市](../Page/明石市.md "wikilink")                                       |
| [惠比須車站](../Page/惠比須車站.md "wikilink")       | [神戶電鐵](../Page/神戶電鐵.md "wikilink")           | [兵庫縣](../Page/兵庫縣.md "wikilink")[三木市](../Page/三木市.md "wikilink")                                       |
| [西神中央車站](../Page/西神中央車站.md "wikilink")     | [神戸市交通局](../Page/神戸市交通局.md "wikilink")       | [兵庫縣](../Page/兵庫縣.md "wikilink")[神戸市](../Page/神戸市.md "wikilink")[西區](../Page/西區_\(神戶市\).md "wikilink") |
| [摩耶纜車車站](../Page/摩耶纜車車站.md "wikilink") （）  | [神戶市都市整備公社](../Page/神戶市都市整備公社.md "wikilink") | [兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")[灘區](../Page/灘區.md "wikilink")         |
| [奈良車站](../Page/奈良車站.md "wikilink")         | 西日本旅客鐵道                                      | [奈良縣](../Page/奈良縣.md "wikilink")[奈良市](../Page/奈良市.md "wikilink")                                       |
| [大和高田車站](../Page/大和高田車站.md "wikilink")     | 近畿日本鐵道                                       | [奈良縣](../Page/奈良縣.md "wikilink")[大和高田市](../Page/大和高田市.md "wikilink")                                   |
| [濱大津車站](../Page/濱大津車站.md "wikilink")       | 京阪電氣鐵道                                       | [滋賀縣](../Page/滋賀縣.md "wikilink")[大津市](../Page/大津市.md "wikilink")                                       |
| [信樂車站](../Page/信樂車站.md "wikilink")         | [信樂高原鐵道](../Page/信樂高原鐵道.md "wikilink")       | [滋賀縣](../Page/滋賀縣.md "wikilink")[甲賀市](../Page/甲賀市.md "wikilink")                                       |
| [周參見車站](../Page/周參見車站.md "wikilink")       | 西日本旅客鐵道                                      | [和歌山縣](../Page/和歌山縣.md "wikilink")[西牟婁郡](../Page/西牟婁郡.md "wikilink")[周參見町](../Page/周參見町.md "wikilink") |
| [高野山車站](../Page/高野山車站.md "wikilink")       | 南海電氣鐵道                                       | [和歌山縣](../Page/和歌山縣.md "wikilink")[伊都郡](../Page/伊都郡.md "wikilink")[高野町](../Page/高野町.md "wikilink")     |

### 第4回

| \-{zh-tw:車站;zh-cn:车站;zh-hk:車站;}-                 | 隸屬業者                                       | 所在地                                                                                                      |
| ------------------------------------------------ | ------------------------------------------ | -------------------------------------------------------------------------------------------------------- |
| [新大阪車站](../Page/新大阪車站.md "wikilink")             | [JR東海](../Page/JR東海.md "wikilink")、西日本旅客鐵道 | 大阪府大阪府[淀川區](../Page/淀川區.md "wikilink")                                                                   |
| [大阪城公園車站](../Page/大阪城公園車站.md "wikilink")         | 西日本旅客鐵道                                    | 大阪府大阪府[中央區](../Page/中央區_\(大阪市\).md "wikilink")                                                           |
| [京橋車站](../Page/京橋車站_\(大阪府\).md "wikilink")       | 京阪電氣鐵道                                     | 大阪府大阪府[城東區](../Page/城東區_\(大阪市\).md "wikilink")、[都島區](../Page/都島區.md "wikilink")                          |
| [諏訪之森車站](../Page/諏訪之森車站.md "wikilink")（）         | 南海電氣鐵道                                     | 大阪府[堺市](../Page/堺市.md "wikilink")[西區](../Page/西區_\(堺市\).md "wikilink")                                   |
| [梅田車站](../Page/梅田車站.md "wikilink")               | 大阪市交通局                                     | 大阪府大阪府北區                                                                                                 |
| [天王寺站前車站](../Page/天王寺站前車站.md "wikilink")（天王寺駅前駅） | 阪堺電氣軌道                                     | 大阪府大阪府[阿倍野區](../Page/阿倍野區.md "wikilink")                                                                 |
| [東舞鶴車站](../Page/東舞鶴車站.md "wikilink")             | 西日本旅客鐵道                                    | [京都府](../Page/京都府.md "wikilink")[舞鶴市](../Page/舞鶴市.md "wikilink")                                         |
| [嵐山車站](../Page/嵐山車站_\(京福電氣鐵道\).md "wikilink")    | [京福電氣鐵道](../Page/京福電氣鐵道.md "wikilink")     | [京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[右京區](../Page/右京區.md "wikilink")         |
| [山科車站](../Page/山科車站.md "wikilink")               | [京都市交通局](../Page/京都市交通局.md "wikilink")     | [京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[山科區](../Page/山科區.md "wikilink")         |
| [姬路車站](../Page/姬路車站.md "wikilink")               | 西日本旅客鐵道                                    | [兵庫縣](../Page/兵庫縣.md "wikilink")[姬路市](../Page/姬路市.md "wikilink")                                         |
| [日本肚臍公園車站](../Page/日本肚臍公園車站.md "wikilink")（）     | 西日本旅客鐵道                                    | [兵庫縣](../Page/兵庫縣.md "wikilink")[西脇市](../Page/西脇市.md "wikilink")                                         |
| [尼崎車站](../Page/尼崎車站_\(JR西日本\).md "wikilink")     | 西日本旅客鐵道                                    | [兵庫縣](../Page/兵庫縣.md "wikilink")[尼崎市](../Page/尼崎市.md "wikilink")                                         |
| [三之宮車站](../Page/三之宮車站.md "wikilink")（）           | 西日本旅客鐵道                                    | [兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")[中央區](../Page/中央區_\(神戶市\).md "wikilink") |
| [夙川車站](../Page/夙川車站.md "wikilink")               | 阪急電鐵                                       | [兵庫縣](../Page/兵庫縣.md "wikilink")[西宮市](../Page/西宮市.md "wikilink")                                         |
| [甲陽園車站](../Page/甲陽園車站.md "wikilink")             | 阪急電鐵                                       | [兵庫縣](../Page/兵庫縣.md "wikilink")[西宮市](../Page/西宮市.md "wikilink")                                         |
| [香櫨園車站](../Page/香櫨園車站.md "wikilink")             | 阪神電氣鐵道                                     | [兵庫縣](../Page/兵庫縣.md "wikilink")[西宮市](../Page/西宮市.md "wikilink")                                         |
| [平福車站](../Page/平福車站.md "wikilink")               | [智頭急行](../Page/智頭急行.md "wikilink")         | [兵庫縣](../Page/兵庫縣.md "wikilink")[佐用郡](../Page/佐用郡.md "wikilink")[佐用町](../Page/佐用町.md "wikilink")         |
| [新開地車站](../Page/新開地車站.md "wikilink")             | [神戶高速鐵道](../Page/神戶高速鐵道.md "wikilink")     | [兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")[兵庫區](../Page/兵庫區.md "wikilink")         |
| [島中心車站](../Page/島中心車站.md "wikilink")（）           | [神戶新交通](../Page/神戶新交通.md "wikilink")       | [兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")[東灘區](../Page/東灘區.md "wikilink")         |
| [吉野車站](../Page/吉野車站_\(奈良縣\).md "wikilink")       | 近畿日本鐵道                                     | [奈良縣](../Page/奈良縣.md "wikilink")[吉野郡](../Page/吉野郡.md "wikilink")[吉野町](../Page/吉野町.md "wikilink")         |
| [生駒車站](../Page/生駒車站.md "wikilink")               | 近畿日本鐵道                                     | [奈良縣](../Page/奈良縣.md "wikilink")[生駒市](../Page/生駒市.md "wikilink")                                         |
| [鳥居本車站](../Page/鳥居本車站.md "wikilink")             | 近江鐵道                                       | [滋賀縣](../Page/滋賀縣.md "wikilink")[彥根市](../Page/彥根市.md "wikilink")                                         |
| [彥根車站](../Page/彥根車站.md "wikilink")               | 西日本旅客鐵道                                    | [滋賀縣](../Page/滋賀縣.md "wikilink")[彥根市](../Page/彥根市.md "wikilink")                                         |
| [和歌山市車站](../Page/和歌山市車站.md "wikilink")           | 南海電氣鐵道                                     | [和歌山縣](../Page/和歌山縣.md "wikilink")[和歌山市](../Page/和歌山市.md "wikilink")                                     |
| [林間田園都市車站](../Page/林間田園都市車站.md "wikilink")       | 南海電氣鐵道                                     | [和歌山縣](../Page/和歌山縣.md "wikilink")[橋本市](../Page/橋本市.md "wikilink")                                       |

## 相關條目

  - [東北-{zh-tw:車站;zh-cn:车站;zh-hk:車站;}-百選](../Page/東北車站百選.md "wikilink")
  - [關東-{zh-tw:車站;zh-cn:车站;zh-hk:車站;}-百選](../Page/關東車站百選.md "wikilink")
  - [中部-{zh-tw:車站;zh-cn:车站;zh-hk:車站;}-百選](../Page/中部車站百選.md "wikilink")
  - [大阪日日新聞](../Page/大阪日日新聞.md "wikilink")

## 外部連結

  - [近畿-{zh-tw:車站;zh-cn:车站;zh-hk:車站;}-100選（大阪日日新聞）](https://web.archive.org/web/20070927030008/http://www.nnn.co.jp/dainichi/rensai/machinoeki/index.html)

[近畿地方鐵路車站](../Category/近畿地方鐵路車站.md "wikilink")