**近藤Emma**（**近藤艾瑪,近藤エマ,こんどうえま 本名：近藤瑛茉ジャスミン**
）是出身於[澳大利亞的日本](../Page/澳大利亞.md "wikilink")[童星](../Page/童星.md "wikilink")[talent](../Page/talent.md "wikilink")。屬於[Ｊｏｂｂｙ
Ｋｉｄｓ](../Page/Ｊｏｂｂｙ_Ｋｉｄｓ.md "wikilink")
[ジョビィキッズ的一員](../Page/ジョビィキッズ.md "wikilink")。身高152公分、體重35公斤（2004年10月時）。血型是Ｏ型。

## 人物介紹

  - 母親是日本人、父親是澳大利亞人的混血兒歸國子女。
  - 興趣是收集閃閃發亮的東西。
  - 特技是一輪車的空中乘車・倒退前進、橋牌、網球、占卜、按摩等等。
  - 2003年4月開始作為[天才兒童MAX中的](../Page/天才兒童MAX.md "wikilink")[TV戰士經常性參與演出](../Page/TV戰士.md "wikilink")。2006年3月畢業。
  - 2003年4月の新人介紹中以[ナウシカ的](../Page/ナウシカ.md "wikilink")「遠い日々」為背景音樂、穿著背後有妖精(?)翅膀的粉紅色洋裝,騎著一輪車登場,甚至在編輯時背景還添加了閃閃發亮的☆緩緩降下。因此主持人[TIM的](../Page/TIM.md "wikilink")2個人表演了一段搞笑:「真是煩人啊\!是妖精吧\!一看就知道了吧\!因為背後有翅膀啊\!」以強硬的態度裝傻、[Red吉田則吐嘈](../Page/Red吉田.md "wikilink")「你之前沒說你知道那是妖精啊」。
  - 曾經用特技的占卜找到迷路哥哥的所在位置。也提過自己還具有其他超能力。
  - 一直以來都是個受觀眾喜愛的野丫頭,也讓人有「近藤Emma真讓人畏懼啊」的強烈印象.但最近似乎變得像女孩子多了.但是不服輸的個性至今依然健在。
  - 2005年度在[紙飛機達陣賽中與](../Page/天TV競賽區.md "wikilink")[高橋郁哉](../Page/高橋郁哉.md "wikilink")、[藤田Ryan一起組成蒸氣騎士團トオボエ而活躍](../Page/藤田Ryan.md "wikilink")。附帶一提的是,藤田Ryan與近藤Emma同樣是屬於ジョビィキッズ。
  - 喜歡吃的東西是乳酪.2003年的TTK HEADLINE NEWS中報導過,她曾經任性的打開朋友家的冰箱,悄悄的一次吃掉12片乳酪.

## 主要演出作品

### 電視

  - [ティンティンTOWN\!](../Page/ティンティンTOWN!.md "wikilink")（[日本電視台](../Page/日本電視台.md "wikilink")）2002準Regular
  - [天才兒童MAX](../Page/天才兒童MAX.md "wikilink")（[NHK](../Page/日本放送協會.md "wikilink")）2003～2005TV戰士Regular

### 廣告

  - 「洗濯乾燥機 白い約束」（[日立](../Page/日立.md "wikilink")）2001～2002
  - 「国際ボランティア貯金」（[郵便貯金](../Page/郵便貯金.md "wikilink")）

### 電影

  - [ローレライ](../Page/ローレライ.md "wikilink")（[富士電視台](../Page/富士電視台.md "wikilink")・[東寶](../Page/東寶.md "wikilink")）2005
    パウラ幼少役

### 雜誌

  - purepure ピュア☆ピュアVol.21（辰巳出版）2003
    與[櫻井結花對談](../Page/櫻井結花.md "wikilink")

### 錄影帶

  - こどもちゃれんじ ほっぷビデオ だいすき！うた・おどり（[ベネッセ](../Page/ベネッセ.md "wikilink")）2001

## 外部連結

  - [Ｊｏｂｂｙ Ｋｉｄｓ ジョビィキッズプロダクション](http://jobbykids.jp/)

[Category:TV戰士](../Category/TV戰士.md "wikilink")
[Category:日裔混血儿](../Category/日裔混血儿.md "wikilink")
[Category:歐洲裔混血兒](../Category/歐洲裔混血兒.md "wikilink")