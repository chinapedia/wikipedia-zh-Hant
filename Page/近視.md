近視（英語：Near-sightedness, 或稱
myopia），係指[眼睛視覺成像未能聚焦於](../Page/眼.md "wikilink")[視網膜上](../Page/視網膜.md "wikilink")，而是聚焦於視網膜之前的情形\[1\]。患者在目視遠物會模糊，而視近物相對清楚
。其他症狀包含[頭痛跟眼睛疲勞](../Page/頭痛.md "wikilink")\[2\]，嚴重的近視會增加[視網膜剝離](../Page/視網膜剝離.md "wikilink")、[白內障及](../Page/白內障.md "wikilink")[青光眼的風險](../Page/青光眼.md "wikilink")。\[3\]

目前相信潛在肇因來自遺傳與環境因素\[4\]，風險因子為做事時需聚焦於近物、長時間待在室內及家族病史等\[5\]\[6\]，近視也和高[社經地位相關](../Page/社經地位.md "wikilink")\[7\]。手機的普及使用，使得人類的眼睛遭受新的挑戰，也可能會導致近視的增加\[8\]。潛在的構造問題是眼球直徑過長，或[水晶體太缺乏彈性](../Page/水晶體.md "wikilink")，但後者較少見\[9\]\[10\]。近視是一種[眼屈光不正](../Page/眼屈光不正.md "wikilink")
，確診方式是做[視力測試](../Page/視力測試.md "wikilink")。\[11\]

目前已有實驗性的例證指出，盡量讓兒童待在戶外可預防兒童近視\[12\]\[13\]，有可能是因為他們接觸到自然光\[14\]。近視可經由配戴[眼鏡](../Page/眼鏡.md "wikilink")、[隱形眼鏡或接受](../Page/隱形眼鏡.md "wikilink")[手術來矯正](../Page/手術.md "wikilink")
。配戴眼鏡是最簡單也最安全的
；隱形眼鏡則可以提供較寬闊的[視野](../Page/視野.md "wikilink")，但也伴隨著感染的風險；而接受屈光手術可以永久改變眼角膜的形狀。\[15\]

近視是最常見的眼睛問題，據估計約有 15 億人患有近視（約佔全球人口 22%）\[16\]\[17\]
，不同地區的近視人口比例差距甚大\[18\]，約佔成人的
15\~49%\[19\]\[20\]，男女比例倒是相去無幾\[21\]。近視的兒童在尼泊爾鄉間僅佔 1.2%，在南非佔 4%，在美國佔
12%，但卻在中國部分大城市卻高達 37%\[22\]\[23\]。自 1950
年代起，近視人口比例持續增加\[24\]。近視卻未經矯正是視力喪失的常見原因，放諸四海皆準，其他原因包含白內障、[黃斑部退化以及缺乏](../Page/黃斑部退化.md "wikilink")[維他命
A](../Page/維他命A.md "wikilink")。\[25\]

## 患者分布

近视是现时其中一个最常见的已知疾病。全球各地都会有人患上近视，但患者以亚洲人佔较多数。当中又以[香港](../Page/香港.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")、[南韩人为主](../Page/大韓民國.md "wikilink")。其次为[日本](../Page/日本.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[澳門等](../Page/澳門.md "wikilink")。

而现时欧美国家患上近视的人亦越来越多。

## 近视的预防

预防近视（非遗传性近视）是许多学生除学习之外的首要事务之一，但是如何才能预防[近视却是一个有争议的话题](../Page/近视.md "wikilink")。这一方面是因为近视形成的过程与原因较为复杂，[医学上对其具体的](../Page/医学.md "wikilink")[生物学过程还没有透彻的认识](../Page/生物学.md "wikilink")。近视的预防是可行的，至少可減緩加深的速度。其主要方法是既包括良好的用眼习惯的培养。

要預防近視，一定要自小做起，由幼兒時期開始便要讓兒童有正確的閱讀及用眼習慣，令發育期中的眼睛不要受到大的不良刺激。即使成年人也要留意這幾方面\[26\]，以減慢視力衰退的速度。事實上，配戴凹透鏡便能矯正因近視導致的模糊視力，其問題是在於兒童近視是會因應眼球成長而不斷加深，深近視度數(600度或以上)已被證實會增加患不同眼疾(例如:
青光眼、白內障和視網膜脱落)的風險，所以控制兒童近視不斷快速地加深是必需的。

### 理解近视成因对预防近视的重要意义

自己理解近视的成因对于预防近视是很重要的。由于商业利益的影响，人们从[眼科](../Page/眼科.md "wikilink")[医生](../Page/医生.md "wikilink")、[眼镜店与](../Page/眼镜.md "wikilink")[互联网上得到关于对待近视的方法并不一定都是正确的](../Page/互联网.md "wikilink")（当然也可能是正确的）。只有自己深刻理解了近视的成因，才能从科学的角度辨明事实的真相，并在日常生活中注意从多方面保护自己的眼睛，预防近视。

### 户外活动

多参加[户外活动](../Page/户外活动.md "wikilink")。有研究表明，青少年户外活动的时间与近视加深速度成反比\[27\]。

### 近视主要相关产品及注意事项

  - 本段落应合并到[\#治疗](../Page/#治疗.md "wikilink")。

<!-- end list -->

1.  **近视眼镜。**近视之后，眼睛将无法看清楚远处，所以需要配戴近视眼镜才能看清楚远处。但是，使用近视眼镜长期看近处又会加重用眼负担并加深近视。因此，不应该使用近视眼镜（特别是度数较高的近视眼镜）长期看近处\[28\]。
2.  **使用阅读镜（远视眼镜）。**和近视眼镜相反，远视眼镜可以减轻看近处的负担，从而预防近视。这种预防近视的方法被称为近[雾视法](../Page/雾视法.md "wikilink")。\[29\]\[30\]雾视镜应按验光时的雾视值选配。
3.  **使用OK（[orthokeratology](../Page/orthokeratology.md "wikilink")）镜（[角膜塑形镜](../Page/角膜塑形.md "wikilink")）。**OK镜类似于一般的[隐形眼镜](../Page/隐形眼镜.md "wikilink")，但是不是用来看清远处的。它可以在长期使用中（夜晚）改变[角膜的形状](../Page/角膜.md "wikilink")，从而在一定程度上逆转真性近视或防止近视加深\[31\]。但是，也需要注意用眼卫生，否则可能导致严重的眼部感染\[32\]\[33\]。
4.  **使用[阿托品](../Page/阿托品.md "wikilink")（[atropine](../Page/atropine.md "wikilink")）[眼药水](../Page/眼药水.md "wikilink")\[34\]。**阿托品（或其类似的药物，如[哌仑西平与](../Page/哌仑西平.md "wikilink")[托吡卡胺等M受体拮抗剂](../Page/托吡卡胺.md "wikilink")）可以麻痹睫状肌，放松其痉挛，从而预防近视。多项研究表明，阿托品眼药水对减缓近视的加深是有效的\[35\]。但是这类[药物也是有](../Page/药物.md "wikilink")[副作用的](../Page/副作用.md "wikilink")，包括暂时无法看清近处、对强光极为敏感等。
5.  **激光手术**
    是目前唯一能够快速逆转近视的方法，对部分成年人是适用的。但它通常并不能使人恢复正常人的视力，并其可能的副作用也是明显的，包括[干眼症](../Page/干眼症.md "wikilink")、炫光、夜视力下降等，甚至可能在若干年后视力严重下降。激光手术在[角膜上留下的](../Page/角膜.md "wikilink")[伤口永远都不会真正](../Page/伤口.md "wikilink")[愈合](../Page/愈合.md "wikilink")，可能受到创伤而错位。事实上，美国[FDA通过LASIK技术认证的前任主管Morris](../Page/FDA.md "wikilink")
    Waxler最近发现，LASIK厂商及其合伙人（包括眼科医生）在申请FDA认证时隐瞒与伪造了大量关于LASIK的安全性与有效性的数据\[36\]。而眼科医师则认为，利用新技术的手术的安全性更高，尽管Waxler的资料认为新技术的副作用是同样的。因此，激光手术的长期安全性仍然是有争议的。
6.  **使用遠距成像放大器(Project Air)\[37\]閱讀** 。長時間近距離用眼是誘發近視的主要原因,
    近距離用眼,如讀書、寫字、使用手機、平板與電腦時,以遠距成像放大器做視覺輔具,可以將原本15\~50cm的用眼距離,延長到100\~250cm,
    距離遠了眼睛的負擔就減輕,藉此達到預防近視度數增加的效果.

## 近视的遗传基因

至今，人们已经定位了一些可能诱发近视眼的基因位点，收入《人类孟德尔遗传》中，下表列出了这些基因\[38\]\[39\]：

| 基因名称  | 位置            | 发现者                 | 近视眼种类 | 遗传方式     | MIM编号                                             |
| ----- | ------------- | ------------------- | ----- | -------- | ------------------------------------------------- |
| MYP1  | Xq28          | Schwartz，1990       | 高度近视  | X连锁隐性遗传  | [310460](http://www.ncbi.nlm.nih.gov/omim/310460) |
| MYP2  | 18p11.31      | Young，1998          | 高度近视  | 常染色体显性遗传 | [160700](http://www.ncbi.nlm.nih.gov/omim/160700) |
| MYP3  | 12q21-q23     | Young，1998          | 高度近视  | 常染色体显性遗传 | [603221](http://www.ncbi.nlm.nih.gov/omim/603221) |
| MYP4  | 7q36          | Naiglin，2002        | 高度近视  | 常染色体显性遗传 | [608367](http://www.ncbi.nlm.nih.gov/omim/608367) |
| MYP5  | 17q21-q22     | Paluru，2003         | 高度近视  | 常染色体显性遗传 | [608474](http://www.ncbi.nlm.nih.gov/omim/608474) |
| MYP6  | 22q12         | Stambolian，2004     | 近视    |          | [608908](http://www.ncbi.nlm.nih.gov/omim/608908) |
| MYP7  | 11q13         | Hammond，2004        | 近视    |          | [609256](http://www.ncbi.nlm.nih.gov/omim/609256) |
| MYP8  | 3q26          | Hammond，2004        | 近视    |          | [609257](http://www.ncbi.nlm.nih.gov/omim/609257) |
| MYP9  | 4q12          | Hammond，2004        | 近视    |          | [609258](http://www.ncbi.nlm.nih.gov/omim/609258) |
| MYP10 | 8p23          | Hammond，2004        | 近视    |          | [609259](http://www.ncbi.nlm.nih.gov/omim/609259) |
| MYP11 | 4q22-q27      | 张清烔，2005            | 高度近视  | 常染色体显性遗传 | [609994](http://www.ncbi.nlm.nih.gov/omim/609994) |
| MYP12 | 2q37.1        | Paluru，2005         | 高度近视  | 常染色体显性遗传 | [609995](http://www.ncbi.nlm.nih.gov/omim/609995) |
| MYP13 | Xq23-q25      | 张清烔，2006            | 高度近视  | X连锁隐性遗传  | [300613](http://www.ncbi.nlm.nih.gov/omim/300613) |
| MYP14 | 1q36          | Wojciechowbski，2006 | 近视    |          | [610320](http://www.ncbi.nlm.nih.gov/omim/610320) |
| MYP15 | 10q21.1       | Nallasamy，2007      | 高度近视  | 常染色体显性遗传 | [612717](http://www.ncbi.nlm.nih.gov/omim/612717) |
| MYP16 | 5p15.33-p15.2 | Lam，2008            | 高度近视  | 常染色体显性遗传 | [612554](http://www.ncbi.nlm.nih.gov/omim/612554) |

## 高度近視

。

。

。

## 治療

[Myopia-2-3.svg](https://zh.wikipedia.org/wiki/File:Myopia-2-3.svg "fig:Myopia-2-3.svg")

  - 框架[眼鏡](../Page/眼鏡.md "wikilink")
      - 近视后要看清远处，最简单、最便宜的方法便是戴眼鏡。配眼鏡需要到醫院排除其他疾病後，通過正規的驗光得到眼鏡度數、瞳距等參數，再到眼鏡店去配鏡。需要指出的是，兒童和青年，因為具有很強的調節能力，所以需要散瞳，以麻痹[睫狀肌](../Page/睫狀肌.md "wikilink")，去除調節力以後再驗光。
      - 常見的眼鏡處方：-3.00DS()-1.50DCx180-\>1.0，意思是，300度的近視，150度的近視[散光](../Page/散光.md "wikilink")，其散光軸在180度，戴鏡後視力可以矯正到1.0
  - [隱形眼鏡](../Page/隱形眼鏡.md "wikilink")（[角膜接觸鏡](../Page/角膜接觸鏡.md "wikilink")）
  - [手術](../Page/手術.md "wikilink")[治療](../Page/治療.md "wikilink")：
      - 作用於[角膜的](../Page/角膜.md "wikilink")[激光矯視手術](../Page/激光矯視.md "wikilink")：[PRK](../Page/PRK.md "wikilink")、[LASIK](../Page/LASIK.md "wikilink")、[LASEK](../Page/LASEK.md "wikilink")、[TransPRK](../Page/TransPRK.md "wikilink")
        等。
      - 有[晶體眼的](../Page/晶體.md "wikilink")[人工晶體植入](../Page/人工晶體.md "wikilink")

## 注释

## 参考文献

## 參見

  - [遠視](../Page/遠視.md "wikilink")
  - [散光](../Page/散光.md "wikilink")
  - [老花](../Page/老花.md "wikilink")
  - [眼球](../Page/眼球.md "wikilink")

{{-}}

[Category:眼球外肌、双眼运动、调焦与屈光疾病](../Category/眼球外肌、双眼运动、调焦与屈光疾病.md "wikilink")
[Category:視覺](../Category/視覺.md "wikilink")

1.
2.

3.
4.
5.
6.
7.

8.

9.
10.

11.
12.

13.

14.

15.
16.
17.

18.
19.

20.
21.
22.
23.
24.
25.

26.

27. Dirani M, Tong L, Gazzard G, Zhang X, Chia A, Young TL, Rose KA,
    Mitchell P, Saw SM. Outdoor activity and myopia in Singapore teenage
    children. Br J Ophthalmol 2009;93:997-1000
    <doi:10.1136/bjo.2008.150979>

28. 梁彦康、韩柏新：《你戴错眼镜啦！》2004年4月

29. [雾视法：使用远视眼镜（阅读镜）防治近视](http://www.sitance.com/prevention/fogging_method.php)

30.
31. [OK镜（角膜塑形镜）预防近视的作用与局限](http://www.sitance.com/prevention/orthokeratology_lenses.php)

32. [Orthokeratology contact lenses cause permanent vision loss in
    children](http://www.medicalnewstoday.com/medicalnews.php?newsid=6245)
    – American Academy of Ophthalmology media release, 1 March 2004

33. [Acanthamoeba keratitis and overnight
    orthokeratology](http://www.hc-sc.gc.ca/dhp-mps/medeff/bulletin/carn-bcei_v16n2_e.html#4)
    Case presentation: Canadian Adverse Reaction Newsletter Volume 16,
    Issue 2, April 2006

34. [阿托品眼药水防治近视的作用和局限](http://www.sitance.com/prevention/atropine.php)

35. Walline JJ, Lindsley K, Vedula SS, Cotter SA, Mutti DO, Twelker JD
    (2011). Interventions to slow progression of myopia in children.
    Cochrane Database Syst Rev12: CD004916.

36. <http://www.scribd.com/doc/46440409/Waxler-Petition-FDA-Stop-LASIK-6Jan11>

37.

38.

39. 胡诞宁，周翔天. 近视病因和分子遗传学研究近况与展望. 中华眼视光学与视觉科学杂志. 2010年4月，第12卷第二期.