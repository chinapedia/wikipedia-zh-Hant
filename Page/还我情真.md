**还我情真**是由[马来西亚电视台](../Page/马来西亚.md "wikilink")[ntv7和](../Page/ntv7.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")[新传媒联合制作的](../Page/新传媒.md "wikilink")[马来西亚剧集](../Page/马来西亚.md "wikilink")。本剧描述以经营[海味生意为基础所发展的爱恨情仇](../Page/海味.md "wikilink")、错综复杂的[家庭伦理奇情剧](../Page/家庭.md "wikilink")，是采用全新剧本拍摄，本剧没有影射[香港无线电视重头剧](../Page/香港无线电视.md "wikilink")《[溏心风暴](../Page/溏心风暴.md "wikilink")》。

## 演员

### 主要演员

<table>
<tbody>
<tr class="odd">
<td><p><strong>演员</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>关系</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/钟晓玉.md" title="wikilink">钟晓玉</a></p></td>
<td><p>梁紫凌</p></td>
<td><p>梁耀国之妹<br />
陈天佑青梅竹马女友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/章缜翔.md" title="wikilink">章缜翔</a></p></td>
<td><p>陈天佑</p></td>
<td><p>梁耀国好友<br />
梁紫凌青梅竹马男友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黄启铭.md" title="wikilink">黄启铭</a></p></td>
<td><p>梁耀国</p></td>
<td><p>梁紫凌之兄<br />
林美琪之男友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/辛伟廉.md" title="wikilink">辛伟廉</a></p></td>
<td><p>陈天保</p></td>
<td><p>陈天佑同父异母之兄<br />
梁紫凌爱慕</p></td>
</tr>
<tr class="even">
<td><p>李美玲</p></td>
<td><p>林美琪</p></td>
<td><p>陈天慧大学同学<br />
梁耀国之女友</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 其他演员

<table>
<tbody>
<tr class="odd">
<td><p><strong>演员</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>关系</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃明慧.md" title="wikilink">黃明慧</a></p></td>
<td><p>陈天慧</p></td>
<td><p>陈天佑之妹<br />
从小暗恋梁耀国</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林豪俊.md" title="wikilink">林豪俊</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王俊.md" title="wikilink">王　俊</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/释福如.md" title="wikilink">释福如</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 歌曲

主题曲：《青苔》James
片尾曲：《I Need Somebody》黄启铭
插曲　：《太天真》钟晓玉

[H還](../Page/category:ntv7中文剧.md "wikilink")

[H還](../Category/马来西亚电视剧.md "wikilink")
[H還](../Category/新傳媒合拍劇.md "wikilink")
[H還](../Category/2008年電視劇集.md "wikilink")
[H還](../Category/家庭劇.md "wikilink")