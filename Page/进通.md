**進通**（623年）是[唐朝初期領袖](../Page/唐朝.md "wikilink")[王摩沙的](../Page/王摩沙.md "wikilink")[年号](../Page/年号.md "wikilink")。

## 大事记

## 出生

## 逝世

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|進通||元年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||623年 |-
style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[癸未](../Page/癸未.md "wikilink")
|}

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [永隆](../Page/永隆_\(梁師都\).md "wikilink")（618年—628年四月）：隋朝末期唐朝初期領袖，[梁政權](../Page/梁.md "wikilink")[梁師都年号](../Page/梁師都.md "wikilink")
      - [始兴](../Page/始兴_\(高开道\).md "wikilink")（618年十二月—624年二月）：隋朝末期唐朝初期領袖，[燕政權](../Page/燕.md "wikilink")[高开道年号](../Page/高开道.md "wikilink")
      - [天造](../Page/天造.md "wikilink")（622年正月—623年正月）：唐朝初期領袖，[刘黑闼年号](../Page/刘黑闼.md "wikilink")
      - [天明](../Page/天明_\(輔公祏\).md "wikilink")（623年八月—624年三月）：唐朝初期領袖，[輔公祏年号](../Page/輔公祏.md "wikilink")
      - [武德](../Page/武德.md "wikilink")（618年五月—626年十二月）：[唐朝政权唐高祖](../Page/唐朝.md "wikilink")[李淵年号](../Page/李淵.md "wikilink")
      - [重光](../Page/重光.md "wikilink")（620年—623年）：[高昌君主](../Page/高昌.md "wikilink")[麴伯雅政权年号](../Page/麴伯雅.md "wikilink")
      - [建福](../Page/建福_\(新羅真平王\).md "wikilink")（584年—634年）：[新羅](../Page/新羅.md "wikilink")[真平王之年號](../Page/新羅真平王.md "wikilink")

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:唐朝民变政权年号](../Category/唐朝民变政权年号.md "wikilink")
[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:620年代中国政治](../Category/620年代中国政治.md "wikilink")
[Category:623年](../Category/623年.md "wikilink")