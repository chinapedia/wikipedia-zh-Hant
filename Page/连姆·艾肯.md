**连姆·艾肯**（Liam
Aiken，）是一名曾在多部影片担任过主角的[美国演员](../Page/美国演员.md "wikilink")。其比较有名的作品有《[-{zh-hans:外星神犬;zh-hk:靈靈狗;zh-tw:我愛一嘴毛;}-](../Page/外星神犬.md "wikilink")》，和《[雷蒙·斯尼奇的不幸历险](../Page/雷蒙·斯尼奇的不幸历险.md "wikilink")》

## 传记

### 早期生活

连姆·艾肯出生在[纽约的](../Page/纽约.md "wikilink")[曼哈顿](../Page/曼哈顿.md "wikilink")，是[比尔·艾肯的独生子](../Page/比尔·艾肯.md "wikilink")。连姆·艾肯的父亲是[MTV电视台的制片人](../Page/MTV.md "wikilink")，她的母亲Moya
Aiken是美籍的[爱尔兰人](../Page/爱尔兰.md "wikilink")。连姆的初次登台是在他七岁时在[百老汇中演出](../Page/百老汇.md "wikilink")[A
Doll's
House](../Page/A_Doll's_House.md "wikilink")，然后他的第一部电影是在1997年的《Henry
Fool》中饰演[Ned
Grim](../Page/Ned_Grim.md "wikilink")。为了获得大学学业筹备资金,她的母亲十分鼓励他成为一名演员。同时连姆·艾肯也十分喜爱表演。所以他决定继续演下去。

### 演艺生涯

连姆·艾肯扮演的第一个重要角色是在《[继母](../Page/继母\(电影\).md "wikilink")》中的[本·哈里斯](../Page/本·哈里斯.md "wikilink")。他在2002年的《[毁灭之路](../Page/毁灭之路.md "wikilink")》又再次成为了[汤姆·汉克斯的儿子](../Page/汤姆·汉克斯.md "wikilink")。他在1998年拒绝了在电影《[第六感](../Page/第六感_\(电影\).md "wikilink")》中扮演主角[科尔·希尔](../Page/科尔·希尔.md "wikilink")，因为他的母亲认为这部电影对他来说年龄还太小，于是《[第六感](../Page/第六感_\(电影\).md "wikilink")》中的[科尔·希尔最终由](../Page/科尔·希尔.md "wikilink")[海利·乔·奥斯蒙扮演](../Page/海利·乔·奥斯蒙.md "wikilink")。他在2003年电影《[-{zh-hans:外星神犬;zh-hk:靈靈狗;zh-tw:我愛一嘴毛;}-](../Page/外星神犬.md "wikilink")》中扮演欧文·贝克。那时他第一次在电影中扮演男主角。曾被[克里斯·哥伦布相中并考虑饰演](../Page/克里斯·哥伦布.md "wikilink")[哈利·波特这一角色](../Page/哈利·波特.md "wikilink")\[1\]，但是不幸的是,
因连姆·艾肯不是[英国人而他最终没有获得这一角色](../Page/英国.md "wikilink")。于是[哈利·波特这一角色最终由](../Page/哈利·波特.md "wikilink")[丹尼尔·拉德克利夫扮演](../Page/丹尼尔·拉德克利夫.md "wikilink")。
连姆于2004年在电影《[雷蒙·斯尼奇的不幸历险](../Page/雷蒙·斯尼奇的不幸历险.md "wikilink")》中扮演了一个12岁的天才男孩,
[克劳斯·波特莱尔](../Page/克劳斯·波特莱尔.md "wikilink") 。

### 个人生活

连姆·艾肯于2008年在[Dwight-Englewood
School毕业](../Page/Dwight-Englewood_School.md "wikilink")。喜爱[朋克音乐](../Page/朋克.md "wikilink"),弹奏吉他。他立志成为像[Tom
Morello一样的吉他手](../Page/Tom_Morello.md "wikilink")。他曾是康涅狄格圣利瓦伊乐队的主吉他手和歌手。他的第一只宠物狗是一只意大利[格雷伊猎犬](../Page/格雷伊猎犬.md "wikilink")，名叫
Kes。

## 作品

| 媒介     | 名称                                                                                   | 年份   | 饰演角色                                       | 导演                                         |
| ------ | ------------------------------------------------------------------------------------ | ---- | ------------------------------------------ | ------------------------------------------ |
| 舞台/百老汇 | *A Doll's House*                                                                     | 1997 | 博比·海耳默尔                                    | 安东尼·佩奇                                     |
| 电影     | *Henry Fool*                                                                         | 1997 | Ned Grim                                   | Hal Hartley                                |
| 电影     | *The Object of My Affection*                                                         | 1998 | Nathan                                     | Nicholas Hytner                            |
| 电视     | *Law and Order*                                                                      | 1998 | 捷克·爱立信                                     | 多导演                                        |
| 电影     | 《[继母](../Page/继母\(电影\).md "wikilink")》                                               | 1998 | 本·哈里斯                                      | [克里斯·哥伦布](../Page/克里斯·哥伦布.md "wikilink")   |
| 电影     | 《[非洲之梦](../Page/非洲之梦.md "wikilink")》                                                 | 2000 | 小伊曼纽尔                                      | Hugh Hudson                                |
| 电影     | 《[甜蜜的十一月](../Page/甜蜜的十一月.md "wikilink")》                                             | 2001 | Abner                                      | 帕特·欧·康诺尔                                   |
| 电影     | 《The Rising Place》                                                                   | 2001 | Emmett Wilder                              | Tom Rice                                   |
| 电影     | 《[毁灭之路](../Page/毁灭之路.md "wikilink")》                                                 | 2002 | 彼得·奥苏利文                                    | Sam Mendes                                 |
| 电视     | 《[Law & Order: Criminal Intent](../Page/Law_&_Order:_Criminal_Intent.md "wikilink")》 | 2002 | Robbie Bishop                              | Frank Prinzi                               |
| 电影     | 《[-{zh-hans:外星神犬;zh-hk:靈靈狗;zh-tw:我愛一嘴毛;}-](../Page/外星神犬.md "wikilink")》              | 2003 | 欧文·贝克                                      | John Hoffman                               |
| 电影     | 《[雷蒙·斯尼奇的不幸历险](../Page/雷蒙·斯尼奇的不幸历险.md "wikilink")》                                   | 2004 | [克劳斯·波特莱尔](../Page/克劳斯·波特莱尔.md "wikilink") | [布莱德·谢尔伯林](../Page/布莱德·谢尔伯林.md "wikilink") |
| 电影     | *Fay Grim*                                                                           | 2006 | Ned Grim                                   | Hal Hartley                                |
| 电视     | *Law & Order*                                                                        | 2007 | Tory Quinlann                              | Various                                    |
| 电影     | *Airborn*                                                                            | 2008 | Matt Cruse                                 |                                            |

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [First Liam Aiken
    Site](https://web.archive.org/web/20091023083337/http://www.liam-aiken.us/)

  -
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美國兒童演員](../Category/美國兒童演員.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美国舞台男演员](../Category/美国舞台男演员.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")

1.  [Guardian
    UK](http://film.guardian.co.uk/harrypotter/news/0,,540822,00.html)
    (accessed [June 24](../Page/June_24.md "wikilink"), 2007)