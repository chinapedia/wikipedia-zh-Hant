**连翘**（[学名](../Page/学名.md "wikilink")：），香港俗稱**一串金**，是[木犀科](../Page/木犀科.md "wikilink")[连翘属植物](../Page/连翘属.md "wikilink")。

## 形态

[Forsythia_suspensa_4090392_レンギョウ（連翹）.JPG](https://zh.wikipedia.org/wiki/File:Forsythia_suspensa_4090392_レンギョウ（連翹）.JPG "fig:Forsythia_suspensa_4090392_レンギョウ（連翹）.JPG")
為落叶灌木，高2\~4m。枝细长，开展或伸长，小枝稍四棱形；节间中空无髓。单叶对生，叶片完整或三全裂，具柄；叶片卵形、长卵形、广卵形至圆形，先端尖，基部楔形或圆形，边缘有不整齐锯齿，半草质。花先叶开放，腋生；花萼绿色，裂片4，长圆形或长圆状椭圆形，边缘有毛；花冠黄色，裂片4，倒卵状椭圆形，基部联合成筒，花冠内有橘红色条纹；雄蕊2，着生于花冠基部，花丝极短；花柱细长，柱头2裂。蒴果狭卵形略扁，先端有短喙，成熟时2瓣裂；种子多数，狭椭圆形，棕色，一侧有翅。花期3\~5月；果期7\~9月。

## 药用

連翹出自《[神农本草经](../Page/神农本草经.md "wikilink")》，其别名又有旱连子、空翘、落翘、黄奇丹。为植物连翘Forsythia
suspensa (Thunb.)
Vahl的干燥果实。在中國大陸，分布于河北、山西、陕西、甘肃、宁夏、山东、江苏、河南、江西、湖北、四川及云南等省区。

### 性味归经及功效

性味苦凉微寒，入肺、心、胆经。既善[清热解毒](../Page/清热解毒.md "wikilink")，消痈散结，治[疮痈](../Page/疮痈.md "wikilink")、瘰疬。有“疮家圣药”之称；且清解热毒兼可升浮宣散透热，常用治外感风热、温病初起。脾胃虚寒或气虚疮疡脓稀者慎用。

连翘心善清心泻火，多用治热入心包之高热烦躁，为清疏兼能、表里气血两清之品。

### 采集炮制

[白露前采青绿果实](../Page/白露.md "wikilink")，晒干，称“青翹”；[寒露前采](../Page/寒露.md "wikilink")，熟透的果实，晒干，称“老翹”。生用。
青翘以色绿，无枝梗，不开裂者为佳。老翘以色黄，壳厚，瓣大，无种子，纯净无杂质者为佳。

### 古籍選錄

  - 《[神农本草经](../Page/神农本草经.md "wikilink")》：“主寒热，鼠瘘，瘰疬，痈肿恶疮，瘿瘤，结热，[蛊毒](../Page/蛊毒.md "wikilink")。”
  - 《[珍珠囊](../Page/珍珠囊.md "wikilink")》：“连翘之用有三：泻心经客热，一也；去上焦诸热，二也；为疮疡须用，三也。”
  - 《[医学衷中参西录](../Page/医学衷中参西录.md "wikilink")》：“连翘，具升浮宣散之力，流通气血，治十二经血凝气聚，为疮家要药。能透肌解表，清热逐风，又为治风热要药。”

### 化学成分

含有[挥发油](../Page/精油.md "wikilink")，其主要成分为β-蒎烯、α-蒎烯等。尚含连翘酚等多种苯乙醇类、连翘苷等木脂类、三萜类及[香豆素等](../Page/香豆素类药物.md "wikilink")。

### 药理作用

对[金黄色葡萄球菌](../Page/金黄色葡萄球菌.md "wikilink")、[肺炎双球菌](../Page/肺炎双球菌.md "wikilink")、[痢疾杆菌](../Page/痢疾杆菌.md "wikilink")、[人型结核杆菌](../Page/人型结核杆菌.md "wikilink")、[百日咳杆菌](../Page/百日咳杆菌.md "wikilink")，以及[流感病毒](../Page/流感病毒.md "wikilink")、[鼻病毒](../Page/鼻病毒.md "wikilink")、[真菌等均有抑制作用](../Page/真菌.md "wikilink")。能明显抑制炎性渗出和增强小鼠吞噬炎症细胞能力。水溶液能降压，所含芦丁能增强[毛细血管致密度](../Page/毛细血管.md "wikilink")。此外，还有解热、镇吐、利尿、抗肝损伤等作用。

## 参考文献

## 外部链接

  - [連翹
    Lianqiao](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00407)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [連翹
    Lianqiao](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00190)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [連翹 Lian
    Qiao](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00418)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

  - [連翹苷
    Forsythin](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00202)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [連翹苷元
    Phillygenin](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00487)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  -
[灌](../Category/藥用植物.md "wikilink") [连翘属](../Category/连翘属.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")