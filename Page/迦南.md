[CanaanMap.jpg](https://zh.wikipedia.org/wiki/File:CanaanMap.jpg "fig:CanaanMap.jpg")

**迦南**（[天主教譯為](../Page/天主教.md "wikilink")**客納罕**）（，，[腓尼基语](../Page/腓尼基语.md "wikilink")：，，）原意为“低”，指地中海東岸的沿海低地，是一个古代地区名称，大致相当于今日[以色列](../Page/以色列.md "wikilink")、[西岸和](../Page/西岸.md "wikilink")[加沙](../Page/加沙.md "wikilink")，加上临近的[黎巴嫩和](../Page/黎巴嫩.md "wikilink")[叙利亚的临海部分](../Page/叙利亚.md "wikilink")。

## 参考文献

  - Bernard W Lewis *[The Arabs in
    History](http://books.google.it/books?vid=ISBN0192803107)* \[1993\]
    Oxford University Press ISBN 0-19-280310-7

  - Peter Briggs *[Testing the Factuality of the Conquest of Ai
    Narrative in the Book of
    Joshua](https://web.archive.org/web/20071203021912/http://www.trinitysem.edu/Student/Journal/3-3/factuality.pdf)*
    (2001) (This paper is derived from the doctoral dissertation Briggs
    2001; A paper presented at the [annual meeting of the Evangelical
    Theological
    Society](http://www.google.it/url?sa=t&ct=res&cd=2&url=http%3A%2F%2Fwww.actsconferenceproducts.com%2FMerchant%2FEV0100B.pdf&ei=YZYLRr26IIv0nQOM2fSvBg&usg=__M-499UJYkgTOoPH4qq30Ae7fmPE=&sig2=lrHOmUTKv31T88UzW3d1OQ))

  - \- *this article needs updating with modern research results.*

## 延伸阅读

  - Jonathan N. Tubb, *Canaanites*, Norman (Oklahoma) 1998. ISBN
    0-8061-3108-X

## 外部链接

  - [The Origin of the Jewish People and the Land of
    Canaan](https://web.archive.org/web/20070302175524/http://www.globalpolitician.com/articleshow.asp?ID=1765&cid=2&sid=1)
    by David Storobin.
  - [Canaan & Ancient
    Israel](http://www.museum.upenn.edu/Canaan/index.html), University
    of Pennsylvania Museum of Archaeology and Anthropology. Explores
    their identities (land-time, daily life, economy & religion) in
    pre-historical times through the material remains that they have
    left behind.
  - [Catholic Encyclopedia](http://www.newadvent.org/cathen/03569b.htm).
  - [Canaan](http://www.allaboutgod.com/truth-topics/canaan.htm):
    reviews every scripture on Canaan in the Bible.
  - [Antiquities of the
    Jews](http://www.gutenberg.org/catalog/world/readfile?fk_files=2359&pageno=1)
    by Flavius Josephus.
  - [The Amarna Letters
    Encyclopedia](http://www.specialtyinterests.net/eae.html), excellent
    source on Egyptian province of Canaan.

## 参见

  - [新月沃土](../Page/新月沃土.md "wikilink")
  - [應許之地](../Page/應許之地.md "wikilink")
  - [黎凡特](../Page/黎凡特.md "wikilink")

{{-}}

[迦南](../Category/迦南.md "wikilink")
[Category:以色列历史](../Category/以色列历史.md "wikilink")
[Category:古地名](../Category/古地名.md "wikilink")