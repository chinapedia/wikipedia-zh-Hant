**迪·布朗**（，），[美國](../Page/美國.md "wikilink")[籃球運動員](../Page/籃球運動員.md "wikilink")。畢業於[伊利諾伊大學](../Page/伊利諾伊大學.md "wikilink")，參加[2006年NBA選秀被](../Page/2006年NBA選秀.md "wikilink")[猶他爵士隊以第二轮第](../Page/猶他爵士隊.md "wikilink")46顺位選中。

## 大學時代

### 新生時期

布朗於[伊利諾伊大學籃球隊第一年已經於](../Page/伊利諾伊大學.md "wikilink")32場比賽中的31場擔任正選。他在[十大聯盟的比賽中每場有](../Page/十大聯盟.md "wikilink")4.9個[助攻及](../Page/助攻.md "wikilink")1.9個抄截。僅次於其隊友[迪朗·威廉士](../Page/迪朗·威廉士.md "wikilink")。他們二人合力幫助球隊於十大聯盟奪得第2名。

### 二年生時期

在第二年，布朗與[德隆·威廉士輪流擔任](../Page/德隆·威廉士.md "wikilink")[得分後衛及](../Page/得分後衛.md "wikilink")[控球後衛](../Page/控球後衛.md "wikilink")。而他們皆是球隊的正選球員。場均[得分](../Page/得分.md "wikilink")13.3分、[助攻](../Page/助攻.md "wikilink")4.5次(隊中僅次於德隆·威廉士。他們幫助球隊於[十大聯盟於以](../Page/十大聯盟.md "wikilink")13勝3負完成賽季。而且自1952年後首次奪得冠軍。

在2004年[NCAA籃球賽中](../Page/NCAA.md "wikilink")。布朗協助球隊於首輪以72:53淘汰[Murray
State](../Page/Murray_State.md "wikilink")。次輪面對[University of
Cincinnati](../Page/University_of_Cincinnati.md "wikilink")，布朗獲得14、8助攻協助球隊以92-68擊敗對手。在準決賽中終以62:72敗於[杜克大學](../Page/杜克大學.md "wikilink")。

### 第三年

在2004年-2005年球季，布朗、威廉士、[盧瑟·海德皆成為](../Page/盧瑟·海德.md "wikilink")[伊利諾伊大學三大](../Page/伊利诺伊大学香槟分校.md "wikilink")[後衛](../Page/後衛.md "wikilink")。以37勝2負的成績完成球季。在[NCAA籃球賽中排名第](../Page/NCAA.md "wikilink")2。

本來布朗、威廉士、海德準備一同參加[2005年NBA選秀](../Page/2005年NBA選秀.md "wikilink")。但布朗於訓練營中受傷，結果不能參加選秀會。結果[德隆·威廉士於首輪第](../Page/德隆·威廉士.md "wikilink")3種子被[猶他爵士隊選中成為探花](../Page/猶他爵士隊.md "wikilink")。而[盧瑟·海德則於首輪第](../Page/盧瑟·海德.md "wikilink")24位被[休斯頓火箭選中](../Page/休斯頓火箭.md "wikilink")。布朗則重回校園。

### 最後一年

在大學的最後一年，布朗與[詹姆斯·奧古斯汀合作為球隊得到](../Page/詹姆斯·奥古斯丁.md "wikilink")26勝7負的成績。前3年由於德隆·威廉士擔任球隊中的[控球後衛](../Page/控球後衛.md "wikilink")，因此布朗轉為擔任[得分後衛](../Page/得分後衛.md "wikilink")。但隨著威廉士加盟[NBA](../Page/NBA.md "wikilink")。布朗亦轉回控球後衛位置.他的最後一場比賽是NCAA籃球賽第二輪中以64:67敗於[華盛頓大學](../Page/華盛頓大學.md "wikilink")。然後他便參加了[2006年NBA選秀](../Page/2006年NBA選秀.md "wikilink")。

## NBA生涯

在[2006年NBA選秀](../Page/2006年NBA選秀.md "wikilink")，布朗以第2輪46順位被猶他爵士選中。不過球隊沒有為他提供一份合約。他參加了爵士隊的訓練營，在那裡他需要與其他無參加選秀的球員競爭一紙合約。最終他成功獲得合約，亦代表他可再之與德隆·威廉士合作。

他首場的職業球賽是於2006年11月14日對[洛杉磯快艇隊](../Page/洛杉磯快艇隊.md "wikilink")。他透過罰球奪得職業賽第一分。在第一個月他於爵士隊的17場比賽中打了8場。

在爵士隊3名新秀中，他獲得的出場機會最少的。因為球隊及球壇對於第一輪選中的[羅尼·布魯爾有較大期望](../Page/羅尼·布魯爾.md "wikilink")。而比布朗後一順位選中的[保羅·米爾薩普有驚人的表現](../Page/保羅·米爾薩普.md "wikilink")。所以於第一季中只有1.9分及1.7次助攻。在[2007年NBA季後賽中](../Page/2007年NBA季後賽.md "wikilink")，他本沒有太多出場機會，但在第二輪對[金州勇士第一場的比賽中](../Page/金州勇士.md "wikilink")，由於[德里克·費舍爾缺陣及](../Page/德里克·費舍爾.md "wikilink")[戈登·吉里塞克表現不隱](../Page/戈登·吉里塞克.md "wikilink")。總教練[傑里·斯隆決定起用羅尼](../Page/傑里·斯隆.md "wikilink")·布魯爾及布朗於各節末段上陣，以減輕其他球員的上陣時間。在最後一節，由於德隆·威廉士已有5次犯規，斯隆決定找布朗擔當其位置，在短短數分鐘內布朗作出了一次重要助攻給予[梅米特·奧庫](../Page/梅米特·奧庫.md "wikilink")。亦於關鍵時間在多個敵方防守下成功[上籃拿下](../Page/上籃.md "wikilink")4分，而且其中一球為一打二快攻，顯出了其籃球技術。

但可惜於二日後的對[金州勇士第二場的比賽中](../Page/金州勇士.md "wikilink")。當布朗協防對手時被撞跌，不巧撞向正在爭搶[籃板球的隊友梅米特](../Page/籃板球.md "wikilink")·奧庫。結果奧庫向前倒下而且壓向布朗的頸部，其頸部受創而且表現非常痛苦，在球場上躺了數分鐘後由擔架抬走。幸其神經測試反應正常，不致永久性傷害。但於該球季後，由於猶他爵士再簽下3名後衛，布朗認為自己的上陣機會更微，因此轉投了一[土耳其球會](../Page/土耳其.md "wikilink")。

## 其他

  - 布朗身上有一個[NBA標誌的](../Page/NBA.md "wikilink")[紋身](../Page/紋身.md "wikilink")。

## 参考资料

## 外部連結

  - [球員檔案@NBA.com](http://www.nba.com/playerfile/daniel_brown/index.html)
  - [個人簡介@fightingillini.com](https://web.archive.org/web/20051128040538/http://fightingillini.collegesports.com/sports/m-baskbl/mtt/brown_dee00.html)
  - [球員檔案@ESPN.com](http://sports.espn.go.com/ncb/player/profile?playerId=10945)

[Category:非洲裔美國篮球运动员](../Category/非洲裔美國篮球运动员.md "wikilink")
[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:猶他爵士隊球員](../Category/猶他爵士隊球員.md "wikilink")
[Category:華盛頓奇才隊球員](../Category/華盛頓奇才隊球員.md "wikilink")
[Category:菲尼克斯太阳队球员](../Category/菲尼克斯太阳队球员.md "wikilink")