**迪士尼樂園度假區**（**Disneyland
Resort**）又稱為**加州迪士尼度假區**、**安納罕迪士尼度假區**，是一座位於[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[洛杉磯地区](../Page/洛杉磯.md "wikilink")[橙县的](../Page/橙县.md "wikilink")[安纳海姆的](../Page/安纳海姆.md "wikilink")[迪士尼度假區](../Page/迪士尼度假區.md "wikilink")。它也是世界上第一個度假區，迪士尼樂園度假區包括三個旅館、[迪士尼小鎮和兩個主題公園](../Page/迪士尼小鎮.md "wikilink")：

  - [迪士尼樂園](../Page/迪士尼樂園.md "wikilink")
  - [迪士尼加州冒險樂園](../Page/迪士尼加州冒險樂園.md "wikilink")

## 2012年加價

2012年5月，加州迪士尼樂園宣佈入場門票加價，幅度最高達三成，迪士尼指樂園下月[反斗車王新園區落成](../Page/反斗車王.md "wikilink")，加上美國經濟好轉，認為訪客可以負擔得起較貴的門票，需要增加收入來補貼建築成本。新收費全日任玩門票加價一成，由六百二十多港元增至近六百八十港元，至於最貴的年票加幅達到三成，由約三千九百港元增至約五千港元，引起許多美國人對加價深表不滿。\[1\]

## 入場人次

2013年，AECOM和TEA的報告表示，迪士尼樂園和迪士尼加洲冒險樂園分別吸引15,963,000和7,775,000人次進場。合計入場人次達到23,738,000人次。迪士尼加洲冒險樂園的入場人次對比2011年更是大幅上升了22.6%。\[2\]

## 參考資料

## 外部連結

  - [迪士尼樂園官方網站](https://disneyland.disney.go.com/destinations/disneyland/)

[Category:迪士尼樂園度假區](../Category/迪士尼樂園度假區.md "wikilink")
[D](../Category/迪士尼度假區及主題公園.md "wikilink")
[D](../Category/華特迪士尼公司子公司.md "wikilink")
[Category:1955年加利福尼亞州建立](../Category/1955年加利福尼亞州建立.md "wikilink")

1.  [加州迪士尼加價30%惹不滿](http://news.hkheadline.com/dailynews/content_in/2012/05/28/192382.asp)
    *頭條日報*. 2012年5月28日.
2.