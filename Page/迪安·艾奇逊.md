[Dean_Acheson.jpg](https://zh.wikipedia.org/wiki/File:Dean_Acheson.jpg "fig:Dean_Acheson.jpg")
**迪安·古德哈姆·艾奇逊**（**Dean Gooderham
Acheson**，），生於[康涅狄格州](../Page/康涅狄格州.md "wikilink")[米德尔敦](../Page/米德尔敦_\(康涅狄格州\).md "wikilink")，1971年10月12日逝世于[马里兰州](../Page/马里兰州.md "wikilink")[桑德斯普林斯](../Page/桑德斯普林斯_\(马里兰州\).md "wikilink")，[美国律师](../Page/美国.md "wikilink")、政治家，第51任[美国国务卿](../Page/美国国务卿.md "wikilink")，在其任内主导编写了《[中美关系白皮书](../Page/中美关系白皮书.md "wikilink")》。

1949年1月26日，艾奇遜發表聲明，謂「美國對華政策不變」\[1\]。

1949年美國總統杜魯門批准了由國務卿艾奇遜於國安會提出的一份秘密文件，指出小心隱藏我們將台灣從大陸的掌控中分離的願景，交由聯合國託管或小心推動台獨或自治運動是對美國最好的結果。\[2\]

## 相關條目

  - [艾奇遜防線](../Page/艾奇遜防線.md "wikilink")（1950年1月12日）

## 参考文献

{{-}}

[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:普利茲歷史獎獲得者](../Category/普利茲歷史獎獲得者.md "wikilink")
[Category:美國文理科學院院士](../Category/美國文理科學院院士.md "wikilink")
[Category:美国国务卿](../Category/美国国务卿.md "wikilink")
[Category:美國國務次卿](../Category/美國國務次卿.md "wikilink")
[Category:康乃狄克州政治人物](../Category/康乃狄克州政治人物.md "wikilink")
[Category:美國民主黨員](../Category/美國民主黨員.md "wikilink")
[Category:美國政治作家](../Category/美國政治作家.md "wikilink")
[Category:美國律師](../Category/美國律師.md "wikilink")
[Category:律師出身的政治人物](../Category/律師出身的政治人物.md "wikilink")
[Category:美國韓戰人物](../Category/美國韓戰人物.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:格羅頓公學校友](../Category/格羅頓公學校友.md "wikilink")
[Category:美國聖公宗教徒](../Category/美國聖公宗教徒.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:康乃狄克州人](../Category/康乃狄克州人.md "wikilink")
[Category:罹患中風逝世者](../Category/罹患中風逝世者.md "wikilink")

1.  [蔣經國](../Page/蔣經國.md "wikilink")<危急存亡之秋>，刊《風雨中的寧靜》，[台北](../Page/台北.md "wikilink")，[正中書局](../Page/正中書局.md "wikilink")，1988年，第143頁。
2.  陶涵<蔣介石與現代中國的奮鬥>，第518頁。