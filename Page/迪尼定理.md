在数学中，**迪尼定理**叙述如下：设 *X*
是一个[紧致的](../Page/紧致.md "wikilink")[拓扑空间](../Page/拓扑空间.md "wikilink")，
*f*(*n*) 是 *X*
上的一个[单调递增的](../Page/单调递增.md "wikilink")[连续实值](../Page/连续.md "wikilink")[函数列](../Page/函数.md "wikilink")（即使得对任意
*n* 和 *X* 中的任意 *x*
都有\(\scriptstyle f_n(x) \leq f_{n+1}(x)\)）。如果这个函数列[逐点收敛到一个连续的函数](../Page/逐点收敛.md "wikilink")
*f* ，那么这个函数列[一致收敛到](../Page/一致收敛.md "wikilink") *f*
。这个定理以意大利数学家[乌利塞·迪尼命名](../Page/乌利塞·迪尼.md "wikilink")。

对于[单调递减的函数列](../Page/单调递减.md "wikilink")，定理同样成立。这个定理是少数的由[逐点收敛可推出](../Page/逐点收敛.md "wikilink")[一致收敛的例子之一](../Page/一致收敛.md "wikilink")，原因是由单调性这个更强的条件。

注意定理中的 *f* 一定要是连续的，否则可以构造反例。比如说在区间 \[0,1\] 上的函数列
{*x*<sup>n</sup>}。这是一个单调递减函数，逐点收敛到函数 *f* ：当 *x* 属于 \[0,1)
时*f*(*x*) 等于 0 ，*f*(*1*) 等于
1。但这个函数列不是[一致收敛的](../Page/一致收敛.md "wikilink")，因为
*f* 不连续。

## 证明

我们对单调递增的函数列作证明：对于任意 \(\varepsilon > 0\) ，对每个 *n* ，设 \(\ g_n = f - f_n\)
再设\(\ E_n\)为使得\(\ g_n(x) < \varepsilon.\) 的\(x \in X\)。显然每个\(g_n\)
都连续，于是每个\(E_n\)
都是[开集](../Page/开集.md "wikilink")（在[拓扑空间中](../Page/拓扑空间.md "wikilink")，连续函数被定义为使得开集的原像都是开集的函数，可以证明这种定义和一般的连续定义是等价的，而\([0,\varepsilon )\)是正实数集中的开集）。函数列{\(g_n\)}
是单调递减的，因此\(E_n\) 是\(E_{n+1}\) 的子集。又由于 \(\ f_n\)
[逐点收敛到](../Page/逐点收敛.md "wikilink") *f* ，所有（\(E_n\)）
的[并集是](../Page/并集.md "wikilink") *X*
的一个[开覆盖](../Page/开覆盖.md "wikilink")。但是 *X*
是一个[紧集于是存在正整数](../Page/紧集.md "wikilink") *N*
使得\(E_N = X\)。因此对所有 \(n > N\)，对所有的 \(x \in X\)，都有
\(0< g_n(x) = f(x) - f_n(x) < \varepsilon\)，于是{\(f_n\)}
[一致收敛于](../Page/一致收敛.md "wikilink") *f* 。

## 参见

  - [拓扑空间](../Page/拓扑空间.md "wikilink")
  - [一致收敛](../Page/一致收敛.md "wikilink")

[D](../Category/函数.md "wikilink") [D](../Category/数学定理.md "wikilink")
[D](../Category/数学分析.md "wikilink") [D](../Category/微积分.md "wikilink")