**迪拜國際機場**（；）是[阿拉伯聯合酋長國](../Page/阿拉伯聯合酋長國.md "wikilink")[迪拜的主要機場](../Page/迪拜.md "wikilink")，為[阿聯酋航空公司的樞紐港](../Page/阿聯酋航空公司.md "wikilink")。可起降目前所有的機型，是中東地區重要的航空中途站之一，許多來往於[亞洲](../Page/亞洲.md "wikilink")、[歐洲及](../Page/歐洲.md "wikilink")[非洲間的飛機中停於此](../Page/非洲.md "wikilink")。機場内擁有眾多的商店，因此它成了在阿聯酋購買免税商品的主要場所。

2010年完工的[馬克統國際機場](../Page/馬克統國際機場.md "wikilink")，現已作為迪拜國際機場的輔助機場。

## 历史

建设迪拜国际机场的设想于1959年提出，1960年举行了落成典礼。

1988年7月3日，[伊朗航空655號班機在执行](../Page/伊朗航空655號班機.md "wikilink")[德黑兰](../Page/德黑兰.md "wikilink")－[阿巴斯港](../Page/阿巴斯港.md "wikilink")－[迪拜飞行途中](../Page/杜拜.md "wikilink")，被[美国军舰文森号用导弹击落](../Page/美国军舰文森号.md "wikilink")，290名乘客遇难。

2004年9月28日，三号航站楼在建设过程中发生部分坍塌。航站楼是由法国设计师[保罗·安德鲁设计的](../Page/保罗·安德鲁.md "wikilink")，他设计的[夏尔·戴高乐国际机场在](../Page/夏尔·戴高乐国际机场.md "wikilink")2004年3月23日也发生了同样的情况。

## 主要航空公司

### 以迪拜国际机场為主要基地的航空公司

  - [阿聯酋航空是迪拜国际机场及國內最大的航空公司](../Page/阿聯酋航空.md "wikilink")，擁有超過200架[空中巴士和](../Page/空中巴士.md "wikilink")[波音公司出產的](../Page/波音.md "wikilink")[廣體飛機](../Page/廣體飛機.md "wikilink")，並提供[中東](../Page/中東.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[亞洲](../Page/亞洲.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[北美洲](../Page/北美洲.md "wikilink")、[南美洲](../Page/南美洲.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[新西蘭的航空服務](../Page/新西蘭.md "wikilink")。3號航客運大樓為其專用大樓。
  - [阿聯酋貨運航空](../Page/阿聯酋貨運航空.md "wikilink")
    為[阿聯酋航空的子公司](../Page/阿联酋航空.md "wikilink")，運營杜拜與世界其他地區之間的所有貨運服務。
  - [杜拜航空為一所](../Page/杜拜航空.md "wikilink")[低成本航空公司](../Page/低成本航空.md "wikilink")，擁有超過100架民航飛機，並從杜拜提供定期飛往中東、非洲、歐洲和南亞的客運服務，並主要於2號客運大樓營運。

## 航廈

[Dubai_-_International_(DXB_-_OMDB)_AN0919771.jpg](https://zh.wikipedia.org/wiki/File:Dubai_-_International_\(DXB_-_OMDB\)_AN0919771.jpg "fig:Dubai_-_International_(DXB_-_OMDB)_AN0919771.jpg")

### 第一航廈

T1為杜拜機場最早落成的航廈，主要服務大部分的外籍航空公司，並有地下通道連結T1航廈主體與候機廊，T1候機廊同時也跟T3相接。

### 第二航廈

T2在T1與T3的另一側，中間有兩條跑道隔著；主要服務[杜拜航空](../Page/杜拜航空.md "wikilink")、其他[低成本航空公司與一部分前往中亞](../Page/低成本航空公司.md "wikilink")、西亞與非洲的班機。

### 第三航廈

T3為杜拜機場最嶄新的航廈，亦為世界目前最大的航廈，主要服務所有[阿酋航空的班次](../Page/阿酋航空.md "wikilink")，跟T1一樣有地下甬道連接航廈本體與候機廊；此外也安裝了能供[空中巴士A380使用的雙層](../Page/空中巴士A380.md "wikilink")[空橋](../Page/空橋.md "wikilink")。

## 航点

在迪拜国际机场开设定期航班的航空公司包括：

## 货运航空公司

[Cargo_Terminal_and_T2,_DXB.jpg](https://zh.wikipedia.org/wiki/File:Cargo_Terminal_and_T2,_DXB.jpg "fig:Cargo_Terminal_and_T2,_DXB.jpg")

## 外部链接

  - <http://archive.wikiwix.com/cache/20110223160932/http://www.dubaiairport.com>

[Category:阿聯酋机场](../Category/阿聯酋机场.md "wikilink")
[Category:杜拜](../Category/杜拜.md "wikilink")