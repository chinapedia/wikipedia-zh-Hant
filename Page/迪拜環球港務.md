**迪拜環球港務集團**（、Dubai Ports
World），又稱**杜拜環球港務集團**，是[阿拉伯聯合酋長國的](../Page/阿拉伯聯合酋長國.md "wikilink")[國有企業](../Page/國有企業.md "wikilink")，成立於2005年，由是**迪拜港務局**和**迪拜港口國際公司**合併而成。\[1\]此後，它以3.3億[英鎊收購](../Page/英鎊.md "wikilink")[英國](../Page/英國.md "wikilink")[鐵行公司](../Page/鐵行輪船公司.md "wikilink")。\[2\]它目前是全球第三大[集裝箱](../Page/集裝箱.md "wikilink")[碼頭營運商](../Page/碼頭.md "wikilink")，擁有52個集裝箱碼頭公司、4個[自由貿易區和](../Page/自由貿易.md "wikilink")3個[物流中心](../Page/物流.md "wikilink")，[辦事處遍布全球](../Page/辦事處.md "wikilink")30個國家。\[3\]其[母公司是](../Page/母公司.md "wikilink")[迪拜世界集團](../Page/迪拜世界集團.md "wikilink")，公司[主席是](../Page/主席.md "wikilink")。

迪拜環球港務目前在[中國經營五個集裝箱碼頭](../Page/中國.md "wikilink")，包括[香港的](../Page/香港.md "wikilink")[葵涌3號及8號(西)碼頭](../Page/葵青貨櫃碼頭.md "wikilink")、[煙臺環球碼頭](../Page/煙臺.md "wikilink")、[青島](../Page/青島.md "wikilink")[前灣集裝箱碼頭和](../Page/前湾港.md "wikilink")[天津東方海陸集裝箱碼頭](../Page/天津.md "wikilink")。\[4\]

2013年3月7日和記港口信託以總代價39.17億元向迪拜環球港務持有55%及[新加坡港務局持有](../Page/新加坡港務局.md "wikilink")45%收購持有葵涌8號貨櫃碼頭西冀的[亞洲貨櫃碼頭的ACT](../Page/亞洲貨櫃碼頭.md "wikilink")
Holdings全部股權，並支付7.5億元以為其償還債務。

2013年3月7日迪拜環球港務以35億元港幣，約合4.5億美元的作價出售位於香港葵涌貨櫃碼頭的亞洲貨櫃物流中心25%的權益和三號碼頭海陸貨櫃碼頭公司75%的權益售給澳洲嘉民集團旗下的嘉民香港物流基金

## 連結

  - [迪拜環球港務集團](http://www.dpworld.com/)
  - [迪拜世界中文網站](https://web.archive.org/web/20080405190716/http://www.dubaiworldchina.cn/)

## 參考

<references />

[Category:航運公司](../Category/航運公司.md "wikilink")
[Category:阿拉伯聯合大公國交通](../Category/阿拉伯聯合大公國交通.md "wikilink")
[Category:杜拜](../Category/杜拜.md "wikilink")
[Category:阿拉伯聯合大公國公司](../Category/阿拉伯聯合大公國公司.md "wikilink")

1.  [迪拜港口收购案诱发美国安全偏执](http://www.singtaonet.com/global/america/t20060321_170257.html)
2.  [迪拜世界港口公司收购英国铁行轮船](http://cms.jctrans.com/xinwen/zhwl/gj/2005121189135.shtml)

3.  [黃興國會見迪拜環球港務集團董事會主席蘇爾坦](http://economy.big5.enorth.com.cn/system/2006/07/07/001351008.shtml)
4.  [迪拜世界推出中文网站](http://www.55188.net/news/gongsi/183919378.htm)