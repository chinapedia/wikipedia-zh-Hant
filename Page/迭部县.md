**迭部县**（）是[中国](../Page/中国.md "wikilink")[甘肃](../Page/甘肃.md "wikilink")[甘南藏族自治州下辖的一个县](../Page/甘南藏族自治州.md "wikilink")。位于[秦岭西部](../Page/秦岭.md "wikilink")，[岷山](../Page/岷山.md "wikilink")、[迭山之间的峡谷之中](../Page/迭山.md "wikilink")，毗邻[四川](../Page/四川.md "wikilink")。北靠[卓尼县](../Page/卓尼县.md "wikilink")，东连[舟曲县](../Page/舟曲县.md "wikilink")，东北与[定西市](../Page/定西市.md "wikilink")[岷县](../Page/岷县.md "wikilink")、[陇南市](../Page/陇南市.md "wikilink")[宕昌县毗邻](../Page/宕昌县.md "wikilink")，西南与四川[阿坝藏族羌族自治州的](../Page/阿坝藏族羌族自治州.md "wikilink")[若尔盖县](../Page/若尔盖县.md "wikilink")、[九寨沟县接壤](../Page/九寨沟.md "wikilink")。

面积5108平方公里，人口5.64万。森林覆盖率54%以上，还有[大熊猫出没](../Page/大熊猫.md "wikilink")。目前甘南州和迭部县正积极争取将这里列为中国大熊猫第14个自然保护区。

## 歷史

迭部(ཐེ་བོ་རྫོང)在[标准藏语的意思是指大](../Page/标准藏语.md "wikilink")[拇指](../Page/拇指.md "wikilink")。當初，神仙[涅甘达娃路过此地](../Page/涅甘达娃.md "wikilink")，用拇指从山中摁开一条通道，因此典故而得名。

迭部在[汉代称为](../Page/汉代.md "wikilink")**沓中**，隸屬於漢中郡。[蜀汉大将](../Page/蜀汉.md "wikilink")[姜维为了躲避与](../Page/姜维.md "wikilink")[宦官](../Page/宦官.md "wikilink")[黃皓的政治争斗而在此屯垦避祸](../Page/黃皓.md "wikilink")。民国时，为藏族杨姓[卓尼土司辖区](../Page/卓尼土司.md "wikilink")。这里也是[红军](../Page/红军.md "wikilink")[长征进入甘肃的第一个县](../Page/长征.md "wikilink")。

1935年8月9日，[红一方面军经](../Page/红一方面军.md "wikilink")[草地到达俄界](../Page/草地.md "wikilink")（今达拉乡高吉村），[中共中央召开](../Page/中共中央.md "wikilink")[俄界会议](../Page/俄界会议.md "wikilink")，[毛泽东在会上作了](../Page/毛泽东.md "wikilink")《关于与四方面军领导的争论及今后的战略方针》，作出了《关于[张国焘同志的错误决定](../Page/张国焘.md "wikilink")》，发出《为北上抗日同志书》，确定在[陕](../Page/陕西.md "wikilink")、[甘割据的方针](../Page/甘肅.md "wikilink")。9月14日红军到[旺藏乡达茨日那村](../Page/旺藏乡.md "wikilink")[旺藏寺](../Page/旺藏寺.md "wikilink")。毛泽东在这里指挥了[腊子口战役](../Page/腊子口战役.md "wikilink")。迭部[俄界會議舊址](../Page/俄界會議舊址.md "wikilink")、达茨日那村毛泽东故居等是[红色旅游的目的地](../Page/红色旅游.md "wikilink")。

1939年，[中华民国政府设置](../Page/中华民国政府.md "wikilink")[卓尼设治局](../Page/卓尼设治局.md "wikilink")，为其辖区。1962年，[中华人民共和国政府设置迭部县](../Page/中华人民共和国政府.md "wikilink")。

## 行政区划

下辖5个[镇](../Page/镇.md "wikilink")、6个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

[迭部县](../Category/迭部县.md "wikilink")
[县](../Category/甘南州县市.md "wikilink")
[甘南州](../Category/甘肃省县份.md "wikilink")
[Category:红色旅游](../Category/红色旅游.md "wikilink")