**迷你樂一通：我如何度過暑假**是[華納兄弟於](../Page/華納兄弟.md "wikilink")1991年製作的[動畫片](../Page/動畫片.md "wikilink")，1992年與以[錄影帶首映形式推出](../Page/錄影帶首映.md "wikilink")。這套影片由各主要的[迷你樂一通成員演出](../Page/迷你樂一通.md "wikilink")，並細分多條線路去記述他們的不同故事。而在世界各地，這[動畫片普遍被評定為適合任何人士收看](../Page/動畫片.md "wikilink")。另外，原裝的[動畫片長達](../Page/動畫片.md "wikilink")80分鐘，而全套都加入了字幕。

在1993年，《我如何度過暑假》被分拆為4集，在電視輪流播放。

## 故事處境

  - ****和****一起打水戰，而規模亦越來越大。最後，他們使整個城淹沒，更被沖到美國南部，進行一連串野外歷險。
  - ****跟著****一家到主題公園去。然而，這一段漫長的路程使**Plucky Duck**感到苦痛。
  - **Fifi Le Fume**希望和電影明星Johnny
    Pew會面，於是跟蹤他到其酒店。最後，他們終於約會，只可惜那不是一個理想約會。
  - ****拉著****去看一套自己重看100多次的電影，而且不斷說話，煩擾其他觀眾。
  - ****享受自己在公園的自然生活，嚇壞了不少動物。

## 參見

  - [迷你樂一通](../Page/迷你樂一通.md "wikilink")

## 外部連結

  -
  - [TV.com第一集資訊](http://www.tv.com/tiny-toon-adventures/how-i-spent-my-vacation-1/episode/54233/summary.html)

  - [TV.com第二集資訊](http://www.tv.com/tiny-toon-adventures/how-i-spent-my-vacation-2/episode/54234/summary.html)

  - [TV.com第三集資訊](http://www.tv.com/tiny-toon-adventures/how-i-spent-my-vacation-3/episode/54233/summary.html)

  - [TV.com第四集資訊](http://www.tv.com/tiny-toon-adventures/how-i-spent-my-vacation-4/episode/54233/summary.html)

[Category:樂一通](../Category/樂一通.md "wikilink")
[Category:1992年電影](../Category/1992年電影.md "wikilink")
[Category:美國動畫電影](../Category/美國動畫電影.md "wikilink")
[Category:華納兄弟動畫](../Category/華納兄弟動畫.md "wikilink")
[Category:英语电影](../Category/英语电影.md "wikilink")
[Category:動畫跨界作品](../Category/動畫跨界作品.md "wikilink")
[Category:超人動畫電影](../Category/超人動畫電影.md "wikilink")
[Category:安培林娛樂電影](../Category/安培林娛樂電影.md "wikilink")