**送肉粽**（[臺羅](../Page/臺羅.md "wikilink")：sàng
bah-tsàng，[白話字](../Page/白話字.md "wikilink")：sàng
bah-chàng），又名**送煞**或**喫麵線**，是種民間的傳統除煞儀式，為[臺灣喪葬習俗之一](../Page/臺灣.md "wikilink")，所謂「肉粽」、「麵線」都是[臺語的](../Page/臺語.md "wikilink")[黑話](../Page/黑話.md "wikilink")，指[上吊](../Page/上吊.md "wikilink")。先民認為[吊頸](../Page/上吊.md "wikilink")[自殺的死者怨氣最重](../Page/自殺.md "wikilink")，只要有人上吊[枉死](../Page/枉死.md "wikilink")，會一而再地[尋人為替](../Page/捉交替.md "wikilink")，以求自己的[超度](../Page/超度.md "wikilink")[轉世](../Page/轉世.md "wikilink")。所以藉由法會將[繩索](../Page/繩索.md "wikilink")（代表吊死鬼的冤魂）送到海邊或是在河流的出海口燒掉，以達驅邪除煞之效。

以鹿港的作法為例，當地的廟宇會聯合舉辦法會，從屍體發現處的鄰近之廟宇開始，規劃一條路線，最後從[福鹿溪或其他路線](../Page/福鹿溪.md "wikilink")，將吊死鬼的冤魂送出海（現多以[彰濱工業區鹿港區送出海](../Page/彰濱工業區.md "wikilink")）。目前此儀式以鹿港為主，各地作法不同，亦有許多地方有類似的儀式，且異於鹿港。彰化有些鄉鎮舉辦法會時會遷就現況，將肉粽只送到最近的河流或大排水溝給燒掉\[1\]。

## 名稱與緣起

風俗起於[中國](../Page/中國.md "wikilink")[福建](../Page/福建.md "wikilink")[泉州](../Page/泉州.md "wikilink")，隨著[閩南人移民至臺灣](../Page/閩南人.md "wikilink")，這種**送煞**儀式文化也被帶入臺灣，在各地的閩南泉州移民村落都有這樣的習俗\[2\]，但後來只在[中臺灣泉州移民集中的](../Page/中臺灣.md "wikilink")[鹿港才較常繼續執行](../Page/鹿港鎮.md "wikilink")，不過，今日由於[媒体與](../Page/媒体.md "wikilink")[網際網路的發達](../Page/網際網路.md "wikilink")，範圍已經廣及[彰化縣臨近鄉鎮](../Page/彰化縣.md "wikilink")：[福興](../Page/福興鄉_\(臺灣\).md "wikilink")、[和美](../Page/和美鎮.md "wikilink")、[伸港](../Page/伸港鄉.md "wikilink")、[線西](../Page/線西鄉.md "wikilink")，甚至於原無此俗的內陸鄉鎮，比如[彰化市](../Page/彰化市.md "wikilink")\[3\]、[員林市](../Page/員林市.md "wikilink")\[4\]一帶，甚至已延長到[新竹縣](../Page/新竹縣.md "wikilink")[竹北](../Page/竹北.md "wikilink")\[5\]亦辦起「送肉粽」法會。\[6\]

最初是鹿港人為了尊重亡者，不蔑稱[縊死之人為](../Page/吊死.md "wikilink")「吊死鬼」，聯想到[端午節在製作](../Page/中華端午節.md "wikilink")[肉粽時](../Page/肉粽.md "wikilink")，係以細繩縛粽（臺羅：pa̍k
tsàng），懸吊於壁上之景象，故利用[雙關語意來暗喻上吊](../Page/雙關.md "wikilink")，而改以「肉粽」做為代稱，除煞儀式就叫「送肉粽」，沿用至今。

也有人稱所謂「送肉粽」，「肉粽」指的是「煞氣」，並不是死者靈魂，是死者生前的怨恨，與死亡過程中那段痛苦的[意識](../Page/意識.md "wikilink")，因為這些不堪的[記憶](../Page/記憶.md "wikilink")，會在亡者離世的一瞬間，交織在一起，累積為「煞氣」。要把這種「煞氣」送走，才不會影響社區的詳和\[7\]。

## 事前準備

在彰化一帶，活動開始之前，廟宇會通知當地居民路線及時間，儀式通常在送煞當晚21至23時舉行，辦的廟宇會在舉行前通知里民。活動當晚20時左右，壇方人員會聚集在目的地的宅前，並在各路口前會設置「祭送，迴避」或者「前有法事，敬請改道」之類字樣的路障，以利法事順利進行，在各路口處亦擺桌祭拜。此夜，各家各戶緊閉門窗，足不出戶，並在門窗上張貼[符令](../Page/符令.md "wikilink")，以免[吊煞受逐而遁入](../Page/吊煞.md "wikilink")。送肉粽是極其重大的要事，必須公告鄰里，使家家戶戶各自緊閉門窗、貼上符令以戒備，如不通知，將有可能遭到民眾阻撓儀式。\[8\]

## 儀式過程

以鹿港為例，通常法事一開始會先「[跳鍾馗](../Page/跳鍾馗.md "wikilink")」作為開場，壇方會在目的宅旁屋內陳列四輦一座、[三太子](../Page/三太子.md "wikilink")、[五營神等神像](../Page/五營神.md "wikilink")，以及[鹽](../Page/食鹽.md "wikilink")、[米各些許](../Page/白米.md "wikilink")，[柳枝](../Page/柳樹.md "wikilink")、[雞](../Page/雞.md "wikilink")、[鴨各一](../Page/鴨.md "wikilink")，並搭起「天臺桌」。角頭內須事先釘「青竹符」於各路口鎮守，[竹材須以刺竹](../Page/竹.md "wikilink")，上以黑、白線各七條，縛上[金紙一束](../Page/金紙.md "wikilink")。在儀式中要送走亡者上吊所用到的繩子、橡木及樓梯等任何碰觸過的代表性物品。所有儀式中使用法器及送走物品，要以筆頭沾雞、鴨血點過。送出宅後，須一路不停放[鞭炮表示驅邪止煞](../Page/鞭炮.md "wikilink")，最後從福鹿溪送出海，今多送至鹿港彰濱海邊。

各地區或者各派別送肉粽的作法不一定相同，鹿港之外的其它地方作法大多都是請比較資深的[道長](../Page/道長.md "wikilink")、[釋教師父或](../Page/釋教.md "wikilink")「黑頭」（[閭山教](../Page/閭山教.md "wikilink")[法主真君派](../Page/法主真君.md "wikilink")）法師來處理送煞神的事宜，「送肉粽」則只是驅邪送煞的相關活動之一，一般的儀式共包括有回駕、[暗訪](../Page/暗訪_\(民俗\).md "wikilink")、送煞、拜散魂等，而這些儀式的主角[神明大都是](../Page/神明.md "wikilink")[王爺](../Page/王爺信仰.md "wikilink")。王爺則藉由[乩童](../Page/乩童.md "wikilink")、桌頭來指示，並由壇方人員及法師依指示進行所有法事。諸如常見的安[五營](../Page/五營.md "wikilink")（[閭山五](../Page/閭山.md "wikilink")[法主](../Page/法主.md "wikilink")）和拜散魂，以及不定期驅逐[邪靈的](../Page/邪靈.md "wikilink")「暗訪」，其目的也在於送煞。\[9\]

## 習俗禁忌

俗信與送肉粽隊伍正面相遇，會遇煞氣，對生人相當不利。故每遇此刻，家戶閉門不出。如不得已相遇，可以側身面向民宅迴避，心中默念[彌陀](../Page/阿彌陀佛.md "wikilink")、[地藏等等佛號](../Page/地藏.md "wikilink")，不要直視。\[10\]若已經直接相遇，一定要跟著隊伍走完全程，表示送死者一程，回家前，再去[廟宇求取](../Page/廟宇.md "wikilink")[香火護身即可](../Page/香火.md "wikilink")。\[11\]

最好再用「淨符」化於水中潔身，而「[淨符](../Page/淨符.md "wikilink")」可於廟宇索討，或去[金紙店購買](../Page/金紙.md "wikilink")。然民間相信，取「淨符」向廟宇或者家中奉祀之[神佛禱告](../Page/神佛.md "wikilink")，繞過香爐三圈，方生效力。如實在無法取得，亦可自製「簡易淨符」。「淨符」畫法簡單，取「[壽金](../Page/壽金.md "wikilink")」或「[刈金](../Page/刈金.md "wikilink")」，或「[金白錢](../Page/金白錢.md "wikilink")」，甚至黃紙一張，在正面上以黑筆書「**奉佛法旨[唵啞吽](../Page/唵啞吽.md "wikilink")**」，或「**[佛法僧寶唵啞吽](../Page/佛法僧.md "wikilink")**」，亦有人書「**[唵嘛呢叭咪吽清淨](../Page/唵嘛呢叭咪吽.md "wikilink")**」，向神明請求，將此化作「淨符」，並繞過香爐三圈，即可成為「淨符」。\[12\]\[13\]\[14\]

## 相關條目

  - [路祭](../Page/路祭.md "wikilink")
  - 《[粽邪](../Page/粽邪.md "wikilink")》

## \-{注}-解

## 參考資料

[Category:喪葬](../Category/喪葬.md "wikilink")
[Category:道教儀式](../Category/道教儀式.md "wikilink")
[Category:彰化縣文化](../Category/彰化縣文化.md "wikilink")
[Category:台灣漢族民間信仰](../Category/台灣漢族民間信仰.md "wikilink")

1.  [生人迴避！ 彰化週五晚間「送肉粽」化煞](https://video.udn.com/news/334479)

2.
3.
4.  [迴避「送肉粽」習俗　員林家商首度提前放學](http://news.tvbs.com.tw/life/711128)

5.  [新竹縣也要「送肉粽」！ 今晚8點生人迴避](https://udn.com/news/story/7324/3667736)

6.  [遇到「送肉粽」怎麼辦？專家告訴你這樣保平安](https://udn.com/news/story/3/2319342)

7.  杜尚澤，[「送肉粽」最恐怖的是流言蜚語，深入理解才能不再恐懼](https://opinion.udn.com/opinion/story/11373/2640774)

8.  [和美送肉粽，驅厲鬼遇阻　亂丟天地帚擋煞變帶衰](http://www.ettoday.net/news/20130326/182329.htm?from=fb_et_news)

9.  [鹿港鎮公所：頭前厝「送煞」](http://www.lukang.gov.tw/title-2/about/ceremony/ceremony_10.htm)

10. [彰化建國科大「送肉粽」　臉書PO文呼籲生人迴避](http://www.ettoday.net/news/20120424/41581.htm)

11. [防對沖
    背對隊伍即可](http://www.libertytimes.com.tw/2009/new/oct/18/today-so10-2.htm)

12. [彰化習俗送肉粽 教你簡易撇步防對沖](https://udn.com/news/story/3/2315919)

13. [民俗解密　路遇「送肉粽」​你應該這樣做…](http://www.appledaily.com.tw/realtimenews/article/new/20150625/635798/)

14. [送肉粽-民間對吊死鬼的送煞驅邪儀式](http://www.mypearl.cc/modules/newbb/viewtopic.php?topic_id=3074)