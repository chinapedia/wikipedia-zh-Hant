**逆滲透**（Reverse osmosis、又稱
RO）、**反滲透**，是一種[淨化水的辦法](../Page/水淨化.md "wikilink")。原理是利用滲透作用，將清水（低張溶液）和鹹水（高張溶液）置於一管中，中間以一支允許水通過的[半透膜分隔開來](../Page/半透膜.md "wikilink")，可見到水從[滲透壓低](../Page/滲透壓.md "wikilink")（[低張溶液](../Page/低張溶液.md "wikilink")）的地方流向滲透壓高（[高張溶液](../Page/高張溶液.md "wikilink")）的地方。然若在高張溶液處施予[力](../Page/力.md "wikilink")，則可見水由滲透壓高的地方流向滲透壓低的地方。逆滲透是“正[滲透](../Page/滲透.md "wikilink")”的反向，通常比正滲透的自然過程，耗費更多的能量。[正滲透分離技術](../Page/正滲透.md "wikilink")，逐漸成為新趨勢。

## 原理

顧名思義，逆滲透濾膜技術就是利用滲透的方法來從[海水中提取出純淨的](../Page/海水.md "wikilink")[飲用水](../Page/飲用水.md "wikilink")。

滲透的英文“Osmosis”一詞來源於希臘文“Osmos”，意思是一種推動力。1748年，[萊諾對滲透現象進行了研究](../Page/萊諾.md "wikilink")，並首次作了實驗紀錄，發現用[膀胱把水和](../Page/膀胱.md "wikilink")[酒精隔開後](../Page/酒精.md "wikilink")，水可以通過膀胱進入酒精，但酒精不能進入水。

根據科學的定義，滲透是某種[溶劑通過半透膜](../Page/溶劑.md "wikilink")（一種溶劑分子能通過，而溶質分子不能通過的薄膜）從低濃度到高濃度轉移的現象。通過那種被稱作“半透膜”的[薄膜](../Page/薄膜.md "wikilink")，我們可以對滲透現象看得更清楚。

半透膜是一種對透過物質有選擇性的薄膜。利用一张只准[水分子透過](../Page/水分子.md "wikilink")，不許[鹽分子透過的半透膜](../Page/鹽.md "wikilink")，將一個容器分成兩半。在膜的兩侧分別注入純水和含鹽水，讓兩側的高度一樣高。過了一段時間後，鹽水的液面升高，純水的液面下降，這是由於純水一側的水分子透過半透膜在向鹽水那側轉移。當滲透達到平衡時，鹽水一側的水柱高於純淨水一側的水柱，這種現象產生於所謂的“滲透壓”，這滲透壓的高低取決於鹽水濃度的高低。

### 重要特征

逆渗透能降低被处理水的[总溶解固体](../Page/总溶解固体.md "wikilink")（TDS），这是其他水处理原理所不具备的特征。逆滲透的水是目前[水处理中最乾淨](../Page/水处理.md "wikilink")，水中除水分子外無任何[礦物質或](../Page/礦物質.md "wikilink")[金屬](../Page/金屬.md "wikilink")。[導電性差](../Page/導電性.md "wikilink")。

### 脱鹽率

脫鹽率等於脱去的鹽量與進水含鹽量的比值，即：

  -

      -
        脱鹽率（%）＝( 1 - 產水TDS/進水TDS )×100%

準確的脫鹽率要通過對產水和進水進行化學分析，測定相應的TDS 含量才能計算出来，但是這樣比較麻煩，一般採用電導率轉換為TDS 來計算脱鹽率。

具體的轉換公式如下\[1\]：

  -

      -
        TDS = K × EC25

其中

  -

      -
        TDS 單位是mg/L 即ppm
        EC25 是經溫度校正到25℃的電導率，单位μs/cm
        EC25 所有鹽類都當成NaCl 且不考慮CO<sub>2</sub> 的透過性

## 其他

[滲透壓公式如下](../Page/滲透壓.md "wikilink")：

\[P = iMRT\]

其中

  -

      -
        i為[凡荷夫因子](../Page/凡荷夫.md "wikilink")，其值視[離子](../Page/離子.md "wikilink")[解離度而定](../Page/解離.md "wikilink")
        P為[滲透壓](../Page/滲透壓.md "wikilink")，但若於其上施加[壓力](../Page/壓力.md "wikilink")，則亦可算入其中
        M為溶液的[重量](../Page/重量.md "wikilink")[莫耳濃度](../Page/莫耳.md "wikilink")
        R為[理想氣體](../Page/理想氣體.md "wikilink")[常數](../Page/常數.md "wikilink")
        T為[絕對溫度](../Page/絕對溫度.md "wikilink")

## 参见

  - [渗透](../Page/渗透.md "wikilink")
  - [渗透压](../Page/渗透压.md "wikilink")
  - [淤塞密度指数](../Page/淤塞密度指数.md "wikilink")

## 参考资料

## 參考文獻

  -
[es:Ósmosis\#Ósmosis
inversa](../Page/es:Ósmosis#Ósmosis_inversa.md "wikilink")

[S](../Category/水处理.md "wikilink")
[Category:过滤器](../Category/过滤器.md "wikilink")
[Category:水处理工艺](../Category/水处理工艺.md "wikilink")
[Category:膜技术](../Category/膜技术.md "wikilink")

1.  <http://blog.tech110.net/?uid-175-action-viewspace-itemid-4053>