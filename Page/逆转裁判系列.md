是[Capcom制作的法庭战斗](../Page/Capcom.md "wikilink")[冒险游戏](../Page/冒险游戏.md "wikilink")，游戏中玩家扮演辩护律师，通过侦查收集证据，然后在假想规则的日本法庭上质疑检察官和证人的证词来为嫌疑人获得「无罪」的判决作为胜利目的。（也有少许例外，如《逆转裁判2》最终篇「再见，逆转」）。

《逆轉檢事》是逆轉裁判的衍生系列，在《逆轉裁判》中的主要對手檢察官御劍-{怜}-侍作為主角活躍在《逆轉檢事》的故事之中。和《逆轉裁判》不同的是，由於沒有法庭部分，調查階段与論爭阶段在同一場景輪流進行，而论争对手则是其他檢察官、法官或案件的目击者等。

## 概述

逆转裁判是CAPCOM公司于2001年至今在GBA、NDS以及3DS平台上推出的法庭辩论AVG型（推理類）系列游戏。游戏者将扮演辩护律师，进行一系列的搜证和推理，并在法庭上与证人对峙，与罪犯周旋，从各种证词中找出案件的关键矛盾，透過抽丝剥茧地推理，最终找出真凶。由于其新颖的游戏方式和游戏带来的紧张刺激的投入感，受到了许多游戏玩家的好评。

系列前3代、“复苏的逆转”和第5、6代的主角都是律师**成步堂龙一**（）。第4代的主角则是**王泥喜法介**（），第5、6代仍为玩家操作的对象。

游戏分为透過反复移动和对话收集法庭的情报和证据的“侦探部分”和利用收集的证据为武器为被告人辩护的“法庭部分”。在法庭上，自己手上的情报和证据会与检察方的证据不断会合、听取证人的证词、寻问出其中与事实不符的部分，即找出所谓的“矛盾”是这个游戏系统的原则。当矛盾不断暴露后，事件的“真相”也就渐渐地明朗，最终的目的是为委托人的无罪立证。

## 基本规则

游戏主要分为“法庭部分”和“侦探部分”。玩家扮演律师交替进行两个部分，引导事件的解决。

### 法庭部分

聽取證人的證詞，然後玩家（律師）進行「尋問」。尋問中可以直接對證詞進行威懾（ゆさぶる），也可以提出能夠證明證詞與事實有矛盾的證物（つきつける）。在1代和『復蘇的逆轉』中生命值有5點，選錯部分關鍵選項或提交錯誤的證物會扣1點，失去全部點數則會獲得有罪判決。2代以後改為血槽，根據迷題難易程度，扣血量會有所增減，一些重要迷題選錯更會導致血槽大幅減少。1代中通過一段法庭部分血量就會全回復，2代以後同一天的法庭部分中不會回復血槽，而是在正确的解除游戏中角色的“心锁”后回复。血槽在6代中又改回如1代和『復蘇的逆轉』中的5點生命值。

《逆轉檢事》中不存在「法庭部分」，改為與事件關係者爭論的「對決部分」，威懾、舉證的方法一樣。

  -
    玩家在證詞中有矛盾的部分選擇「指證」，再選擇與之矛盾的證物。2代和3代中也可以選擇人物檔案。如果選擇正確則故事繼續，否則則會受到懲罰並要求重來一次。

<!-- end list -->

  -
    對證詞選擇「威懾」，就能追問出詳細的情報。有時不使用「指證」僅用威懾也能推進劇情。威懾之後有時會追加新的證詞，追問中動搖的證人有時也會作出矛盾的證詞，一般來說威懾是不會被懲罰的，但有時也會懲罰，此時審判長會事先給予警告。

<!-- end list -->

  -
    在4、5、6代中登場。對特定的證詞選擇手鐲，看穿並指出證人撒謊時的「小動作」而推進劇情。看穿時，證人的語速會變得非常慢，更容易暴露出「小動作」。如果錯過了，還可以再聽一次。即使錯誤也不會被懲罰。

<!-- end list -->

  -
    5代及6代中登場。藉由分析證人的證詞在情感上出現的矛盾，以推論出正確的證詞。

<!-- end list -->

  -
    6代中登場，藉由姬巫女所跳之「奉納舞」，能夠看見死者死前最後的記憶，並透過姬巫女進行主觀的解釋。律師需透過死者最後感受的五感以及物件去修正姬巫女的解釋，引導出最正確的記憶。過程中姬巫女能夠透過磨礪死者的五感，去進一步感受更確實的記憶，如此修正模糊的感官。

<!-- end list -->

  -
    《大逆轉裁判系列》登場，在大英帝國法庭中被遺忘的賦予律師的權力，在所有陪審員都給予有罪判決時能夠使用。陪審員需給出訂定被告有罪的依據或理由，律師能夠試圖說服評審員將判決改為無罪，只要能顛覆超過一半的判決，就能強行繼續進行審理。需注意的是律師一般無法透過自己的言語說服評審員，但能透過攻擊兩位評審員話語間的矛盾改變判決。

除上述操作外，有時也會對辯護人的看法、方針進行提問。提問的形式多種多樣，選出正確選項、提示證物（或人物）、指示圖片或影像等。設問時如果出現血槽，一旦答錯就要受懲罰。

### 侦探部分

不断地从各种各样的人手中获取情报，现场取证收集证据。收集法庭上的必要线索是这部分的目的。这部分一般不设惩罚（但解除心灵枷锁时还是会有惩罚）。此外，为了防止由于侦探部分错过调查导致无法進入法庭部分，游戏被设定为“不收集好全部必要的证物和情报就不能进入下一个法庭部分”。

  - 心灵枷锁
    2、3、4、5代及6代中登场，对有心灵枷锁的人出示勾玉，会被问几个问题，玩家需分别出示正确的证物。选错会受到惩罚，血槽归零时会回复极少量并强制退出追问、而不会导致遊戲結束。解除成功后就可以询问锁的相关情报并回复一半的血槽。此外心灵枷锁在发现时一般并不能马上用手头上的证物解除，一般都必须在其他场景深入调查获得新证物和情报之后返回才能解除。
    分成紅色與黑色兩種：紅色為當事人刻意隱瞞某事而出現的枷鎖，可直接解除；黑色為當事人無意識產生的枷鎖，若強行解開可能會有生命危險，只能放著不管或是根據遊戲劇情逐步解開。
  - 金属探测器和电波探测器
    2、3代中登场。1代中[金属探测器也有登场](../Page/金属探测器.md "wikilink")，但只是作为事件道具，带到特定的场景会发生事件，而不能进行操作。探测器的光标停在有电波或金属反应的地方会变成红色发出“滴滴”的声音（NDS版中会出现CHECK字样），可以调查所在处的物品。不断检查就能找到目标证物。探测器对所有电波或金属的发生源都有反应。
  - 科学搜查
    利用了NDS的触控屏。於“复苏的逆转”、4代及6代中使用。
      - 3D证物 : 证物采用3D表现形式，可以对XY轴放大缩小，用手写笔从不同角度调查。
        [鲁米诺試劑](../Page/鲁米诺.md "wikilink") : 首度出現於“复苏的逆转”，用于检测证物的血迹反应。
        [指纹检验](../Page/指纹.md "wikilink") :
        首度出現於“复苏的逆转”。用手写笔在有可能有指纹的地方撒上采集粉，通过向麦克风吹气吹走多余的采集粉提取指纹，然后对比与指纹一致的人物。
        足迹检验 :
        4代第2话登场。点击脚印（鞋印）涂抹[石膏](../Page/石膏.md "wikilink")，用吹风机烘干后刷上墨提取脚印。最后进行脚印对比。
        毒物检测 : 4代第4话登场。与血液检测类似，这次是对毒物有反应。
        X線解析装置 :
        4代第4话登场。使用[X射线对物体进行透视调查](../Page/X射线.md "wikilink")，可观察物体内部断层，利用这点可以调查信封裡面和油画的底稿。
  - 邏輯與推理的實驗劇場
    大逆轉裁判系列中登場，由福爾摩斯與成步堂龍之介共同進行，首先由福爾摩斯給出基本的推理方向，但會故意用錯誤的推理，主要是考驗步堂龍之介，之後再由成步堂龍之介對該推理進行檢討，並修正某些錯誤的關鍵詞推導出正確的推理。

### 搜查部分

  - 邏輯
    《逆轉檢事》登場。御劍蒐集在現場搜查中判明瞭的事實和感到疑問的事等。按L按鈕或觸摸「邏輯」面板，想起集到的情報，然後透過情報和情報的關聯性，「整合」後便得出新的情報。
      - 整合 :
        在邏輯模式中選擇2個有關聯的情報，按X按鈕或觸摸「整合」面板來整合一起，便會得到新的情報。如果整合了沒有關聯的情報，真相量就會減少。
  - 情報再現
    《逆轉檢事》登場。輸入情報和證物等的數據到特殊的機器——盗乐宝，便能把當時的事件現場的狀況再現。一邊確認輸入的資料是否錯誤或不足，一邊逼近被隱藏了的真相。根據輸入情報的不同，再現的狀況也會不同。

## 系列一覽

系列在[GBA](../Page/GBA.md "wikilink")、[NDS](../Page/NDS.md "wikilink")、[3DS上發售](../Page/3DS.md "wikilink")。逆轉裁判1、2、3、復蘇的導演和劇本由[巧舟擔任](../Page/巧舟.md "wikilink")。以下如無特別說明，都是指日版。重製版將會在[智能手機平台](../Page/智能手機.md "wikilink")[IOS和](../Page/IOS.md "wikilink")[Android開發](../Page/Android.md "wikilink")。到目前為止，于2019年2月发售在[NS](../Page/任天堂Switch.md "wikilink")、[PS4](../Page/PS4.md "wikilink")、[X1以及](../Page/Xbox_One.md "wikilink")[PC上的](../Page/PC.md "wikilink")《逆转裁判123
成步堂精选集》将会通过更新的方式追加繁简中文内容以及中文语音；这是系列首次官方中文化。\[1\]

  - [逆转裁判](../Page/逆转裁判_\(游戏\).md "wikilink")（GBA 2001年10月12日发售）
  - [逆轉裁判2](../Page/逆轉裁判2.md "wikilink")（GBA 2002年10月18日发售、DS
    2006年10月26日发售）
  - [逆轉裁判3](../Page/逆轉裁判3.md "wikilink")（GBA 2004年1月23日发售、DS
    2007年8月23日发售） - [CERO評級](../Page/CERO.md "wikilink")12歲
  - 逆轉裁判 復蘇的逆轉（DS 2005年9月15日发售） -
    CERO評級12歲以上、[PEGI評級](../Page/Pan_European_Game_Information.md "wikilink")7歲以上
  - [逆轉裁判4](../Page/逆轉裁判4.md "wikilink")（DS 2007年4月12日发售） -
    限定版CERO評級12歲以上
    ([IOS和](../Page/IOS.md "wikilink")[Android已分別在](../Page/Android.md "wikilink")[Apps
    Store和](../Page/App_Store_\(iOS\).md "wikilink")[Google
    Play上架](../Page/Google_Play.md "wikilink")) \[2\]\[3\]
  - [逆转检事](../Page/逆转检事.md "wikilink")（DS 2009年5月28日发售）
  - [逆轉檢事2](../Page/逆轉檢事2.md "wikilink")（DS 2011年2月3日发售）
  - [雷頓教授VS逆轉裁判](../Page/雷頓教授VS逆轉裁判.md "wikilink")( 3DS
    2012年11月29日發售\[4\])
  - [逆轉裁判5](../Page/逆轉裁判5.md "wikilink")（3DS
    2013年7月25日發售\[5\]\[6\]，有iOS版\[7\]）
  - 逆轉裁判123HD重製（[IOS和](../Page/IOS.md "wikilink")[Android已分別在](../Page/Android.md "wikilink")[Apps
    Store和](../Page/App_Store_\(iOS\).md "wikilink")[Google
    Play上架](../Page/Google_Play.md "wikilink")\[8\]）
  - [大逆转裁判 -成步堂龍之介的冒險-](../Page/大逆转裁判_-成步堂龍之介的冒險-.md "wikilink")（3DS
    2015年7月9日发售）
  - [逆转裁判6](../Page/逆转裁判6.md "wikilink")（3DS 2016年6月9日发售）
  - [大逆转裁判2 成步堂龍之介的覺悟](../Page/大逆转裁判2_成步堂龍之介的覺悟.md "wikilink")（3DS
    2017年8月3日发售）

<!-- end list -->

  - 以下是移植作品：
      - 逆轉裁判 PC（Windows 2005年12月23日）2,970日元（含税）
      - 逆轉裁判2 PC（Windows 2006年3月31日）2,970日元（含税）
      - 逆轉裁判3 PC（Windows 2006年3月31日）2,970日元（含税）
      - 逆轉裁判 PC 1·2·3套装（Windows 2006年3月31日）6,980日元（含税）

## 角色介紹

  - 每部作品均有单独的情节，但人物关系始终贯穿整个系列。

<!-- end list -->

  - 在[1代](../Page/逆轉裁判.md "wikilink")、[2代](../Page/逆轉裁判2.md "wikilink")、[3代作品中](../Page/逆轉裁判3.md "wikilink")，主要的可玩角色是刚从法学院毕业的新手律师[成步堂龙一](../Page/成步堂龙一.md "wikilink")，他在几年前曾帮助自己获得无罪的律师绫里千寻的帮助下，得以在她的律师事务所工作。千寻遇害后，成步堂在千寻的妹妹绫里真宵的帮助下接管事务所，将其更名为“成步堂律师事务所”。真宵和他年轻的表妹春美时常利用绫里家族的灵媒能力，召唤千寻的灵魂，帮助成步堂赢得案件。成步堂常跟御剑-{怜}-恃在法庭上交战，并经常获胜（连同战胜狩魔豪那一次），从而让法庭上第三位经常将御剑看作自己弟弟的检察官狩魔冥（尽管她比御剑小幾岁），她决心一洗父亲和御剑的耻辱。而到了3代，成步堂在法庭上的主要对手是戈德，这位神秘的检察官对成步堂心存芥蒂。在成步堂的表述下，千寻的身世及绫里家其他人物的神秘面纱被陆续揭开，并且回放此前案件中的人物，正式结束成步堂篇故事。

<!-- end list -->

  - [4代时隔前作七年](../Page/逆轉裁判4.md "wikilink")，因误将伪造的证据呈上而被取消律师资格的成步堂龙一，堕落为钢琴演奏者。他还收养了一位魔术师的女儿，把她改名为“成步堂美贯”，美贯将养父的事务所改名为“成步堂演艺事务所”。

## 遊戲产品列表

<table>
<thead>
<tr class="header">
<th><p>作品名称</p></th>
<th><p>操作平台</p></th>
<th><p>语言</p></th>
<th><p>发售日期</p></th>
<th><p>定价</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/逆转裁判_(游戏).md" title="wikilink">逆转裁判</a></p>
<p>逆转裁判：复苏的逆转</p>
<p>Phoenix Wright: Ace Attorney</p></td>
<td><p><a href="../Page/GBA.md" title="wikilink">GBA</a> <a href="../Page/PC.md" title="wikilink">PC</a></p>
<p><a href="../Page/NDS.md" title="wikilink">NDS</a></p>
<p><a href="../Page/IOS.md" title="wikilink">IOS</a></p></td>
<td><p>日文 英文</p>
<p>法文</p></td>
<td><p>2001年10月12日 2005年12月23日</p>
<p>2005年9月12日</p>
<p>2006年3月31日(歐版)</p>
<p>2006年10月12日(英)</p>
<p>2009年12月21日</p></td>
<td><p>¥3,129 ¥2,970</p>
<p>¥3,129</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逆转裁判2.md" title="wikilink">逆转裁判2</a></p>
<p>Phoenix Wright Ace Attorney: Justice For All</p></td>
<td><p><a href="../Page/GBA.md" title="wikilink">GBA</a> <a href="../Page/PC.md" title="wikilink">PC</a></p>
<p><a href="../Page/NDS.md" title="wikilink">NDS</a></p></td>
<td><p>日文 英文</p>
<p>法文</p></td>
<td><p>2002年10月18日 2006年3月31日</p>
<p>2006年10月26日</p>
<p>2007年1月16日(英)</p>
<p>2007年3月26日(歐版)</p></td>
<td><p>¥2,970</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/逆转裁判3.md" title="wikilink">逆转裁判3</a></p>
<p>Phoenix Wright Ace Attorney: Trials and Tribulations</p></td>
<td><p><a href="../Page/GBA.md" title="wikilink">GBA</a> <a href="../Page/PC.md" title="wikilink">PC</a></p>
<p><a href="../Page/NDS.md" title="wikilink">NDS</a></p></td>
<td><p>日文 英文</p>
<p>法文<br />
西班牙文</p>
<p>意大利文<br />
德文</p></td>
<td><p>2004年1月21日 2006年3月31日</p>
<p>2007年8月23日</p>
<p>2007年10月23日(英)</p>
<p>2008年10月(歐版)</p></td>
<td><p>$34.90 ¥2,970</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逆转裁判4.md" title="wikilink">逆转裁判4</a> Apollo Justice: Ace Attorney</p></td>
<td><p><a href="../Page/NDS.md" title="wikilink">NDS</a></p>
<p><a href="../Page/IOS.md" title="wikilink">IOS</a>/<a href="../Page/Android.md" title="wikilink">安卓</a></p></td>
<td><p>日文 英文</p></td>
<td><p>2007年4月12日 2008年1月</p>
<p>2016年12月</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/逆转检事.md" title="wikilink">逆转检事</a> (限定版¥9490) Miles Edgeworth: Ace Attorney Investigations</p></td>
<td><p><a href="../Page/NDS.md" title="wikilink">NDS</a></p>
<p><a href="../Page/IOS.md" title="wikilink">IOS</a></p></td>
<td><p>日文 英文</p></td>
<td><p>2009年5月28日 2010年2月16日</p>
<p>2017年12月8日</p></td>
<td><p>¥5,080 ¥9,490</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逆转检事2.md" title="wikilink">逆转检事2</a> 典藏版</p>
<p>限定版</p></td>
<td><p><a href="../Page/NDS.md" title="wikilink">NDS</a><br />
<a href="../Page/IOS.md" title="wikilink">IOS</a></p></td>
<td><p>日文</p></td>
<td><p>2011年2月3日</p>
<p>2017年12月21日(日區)</p></td>
<td><p>¥5,040 ¥6,090</p>
<p>¥8,000</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/逆转裁判5.md" title="wikilink">逆转裁判5</a></p></td>
<td><p><a href="../Page/N3DS.md" title="wikilink">N3DS</a> <a href="../Page/IOS.md" title="wikilink">IOS</a></p>
<p><a href="../Page/Android.md" title="wikilink">安卓</a></p></td>
<td><p>日文</p></td>
<td><p>2013年7月25日 2014年8月14日</p>
<p>2017年5月23日</p></td>
<td><p>¥5,990</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大逆轉裁判_-成步堂龍之介的冒險-.md" title="wikilink">大逆轉裁判 -成步堂龍之介的冒險-</a></p></td>
<td><p>N3DS</p></td>
<td><p>日文</p></td>
<td><p>2015年7月9日</p></td>
<td><p>¥5,800</p></td>
</tr>
<tr class="odd">
<td><p>逆转裁判123 成步堂合集</p></td>
<td><p><a href="../Page/IOS.md" title="wikilink">IOS</a>/<a href="../Page/Android.md" title="wikilink">安卓</a> N3DS</p>
<p><a href="../Page/PlayStation_4.md" title="wikilink">PS4</a>/<a href="../Page/任天堂Switch.md" title="wikilink">Switch</a></p>
<p>/<a href="../Page/Xbox_One.md" title="wikilink">Xbox One</a></p>
<p><a href="../Page/PC.md" title="wikilink">PC</a></p></td>
<td><p>日文<br />
英文<br />
中文 <small>韓文</small></p></td>
<td><p>2012年2月7日(手機) 2014年4月17日(N3DS)</p>
<p>2019年2月21日(主機)</p>
<p>2019年4月10日(PC)</p></td>
<td><p>¥3,990 ¥3,056</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逆转裁判6.md" title="wikilink">逆转裁判6</a></p></td>
<td><p>N3DS <a href="../Page/IOS.md" title="wikilink">IOS</a>/<a href="../Page/Android.md" title="wikilink">安卓</a></p></td>
<td><p>日文</p></td>
<td><p>2016年6月9日 2017年12月21日</p></td>
<td><p>¥5,800</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大逆轉裁判2_成步堂龍之介的覺悟.md" title="wikilink">大逆轉裁判2 成步堂龍之介的覺悟</a></p></td>
<td><p>N3DS</p></td>
<td><p>日文</p></td>
<td><p>2017年8月3日</p></td>
<td><p>¥5,800</p></td>
</tr>
</tbody>
</table>

  - 注：“¥”表示日元，“$”表示美元

## 制作团队

**配音人员**

|                                                                                                                    |                                       |
| ------------------------------------------------------------------------------------------------------------------ | ------------------------------------- |
| **角色**                                                                                                             | **配音人员**                              |
| <SPAN lang=JA style="FONT-FAMILY: Book Antiqua">成歩堂 龍一</SPAN>                                                      | 巧 舟                                   |
| <SPAN lang=JA style="FONT-FAMILY: Book Antiqua">成歩堂 龍一（特典动画）</SPAN>                                                | 近藤 孝行                                 |
| <SPAN lang=JA style="FONT-FAMILY: Book Antiqua">御剣 怜侍</SPAN>                                                       | 岩元 辰郎                                 |
| <SPAN lang=JA style="FONT-FAMILY: Book Antiqua">御剣 怜侍（特典动画）</SPAN>                                                 | 竹本 英史                                 |
| <SPAN lang=JA style="FONT-FAMILY: Book Antiqua">亜内 武文</SPAN>                                                       | 浜 亘                                   |
| 狩魔 豪                                                                                                               | 杉森 雅和                                 |
| 狩魔 冥                                                                                                               | 諏訪部 ゆかり                               |
| 狩魔 冥（特典动画）                                                                                                         | 沢城 みゆき                                |
| <SPAN lang=JA style="FONT-FAMILY: Book Antiqua">綾里 千尋</SPAN>                                                       | 河原 深雪                                 |
| 戈德                                                                                                                 | 神谷 英樹                                 |
| <SPAN lang=ja>王泥喜 法介</SPAN>                                                                                        | 荻原光之郎                                 |
| <SPAN lang=ja>王泥喜 法介（特典动画）</SPAN>                                                                                  | KENN                                  |
| <SPAN lang=JA style="FONT-FAMILY: Book Antiqua">牙琉<SPAN lang=JA style="FONT-FAMILY: Book Antiqua">霧人</SPAN></SPAN> | 山本亮治                                  |
| <FONT face="Book Antiqua">牙琉</FONT><SPAN lang=JA style="FONT-FAMILY: Book Antiqua">霧人（特典动画）</SPAN>                 | 津田健次郎                                 |
| 牙琉 響也                                                                                                              | 山本亮治                                  |
| 牙琉 響也（特典动画）                                                                                                        | <FONT face="Book Antiqua">楠田敏之</FONT> |
| 成步堂 美贯（特典动画）                                                                                                       | 樋口智恵子                                 |
| 宝月 茜（特典动画）                                                                                                         | 花村怜美                                  |
| Phoenix Wright                                                                                                     | Ben "Gromin" Judd                     |
| Miles Edgeworth                                                                                                    | Seon King                             |
| Mia Fey                                                                                                            | Christina Kitano                      |
| Franziska von Karma                                                                                                | Janet Hsu                             |
| Godot                                                                                                              | James T. Wilson                       |
| Winston Payne                                                                                                      | David Crislip                         |

## 舞台版

於2009年2月5日至2月15日（寶塚BOW
HALL）、2月24日至3月2日（日本青年館大HALL）由[寶塚歌劇團宙組演出逆轉裁判系列第一個舞台化作品](../Page/寶塚歌劇團.md "wikilink")「逆轉裁判～復甦的真實～（逆転裁判～蘇る真実～）」，主角フェニックス・ライト（Phoenix
Wright/成步堂龍一）由宙組的二番手男役[蘭寿とむ飾演](../Page/蘭寿とむ.md "wikilink")，導演/腳本為[鈴木圭](../Page/鈴木圭.md "wikilink")。故事為逆轉裁判一代「復甦的逆轉」篇改編的劇本，劇中有部分遊戲主要角色登場、歌曲也使用遊戲本身裡有的音樂進行創作，背景則設在美國紐約\[9\]。

因為大受好評，所以在「復甦的真實」千秋樂當天發表「逆轉裁判2」舞台化消息，並定名為「逆轉裁判2～復甦的真實，再臨～（逆転裁判2～蘇る真実、再び～）」，於2009年8月20日至8月31日於寶塚BOW
HALL、9月5日至9月15日於東京赤阪ACT劇場公演。由於飾演「逆轉裁判」女主角レオナ的美羽あさひ及御劍（エッジワース）的七帆ひかる退團，レオナ並不會出現在逆轉裁判2裡，御劍也改由悠未ひろ擔任。故事則是逆轉裁判2第四話「再會了，逆轉」為主軸，遊戲中新登場的主要角色狩魔
冥檢察官也會在本次公演中出現。

**「逆轉裁判～復甦的真實～」主要演员**

  - フェニックス・ライト(成歩堂 龍一) / [蘭寿とむ](../Page/蘭寿とむ.md "wikilink")
  - レオナ・クライド(女主角･原創角色)/[美羽あさひ](../Page/美羽あさひ.md "wikilink")（案件中是遊戲版宝月巴的戲份）
  - マイルズ・エッジワース(御剣 -{怜}-侍) / [七帆ひかる](../Page/七帆ひかる.md "wikilink")
  - マヤ・フェイ(綾里 真宵) / [すみれ乃麗](../Page/すみれ乃麗.md "wikilink")
  - ディック・ガムシュー(糸鋸 圭介) / [春風弥里](../Page/春風弥里.md "wikilink")
  - ラリー・バッツ(矢張 政志) / [鳳翔大](../Page/鳳翔大.md "wikilink")
  - ミラー・アーサー(紐約州知事･原創角色) / [寿つかさ](../Page/寿つかさ.md "wikilink")（厳徒 海慈）
  - ロッタ・ハート(大沢木ナツミ) / [美風舞良](../Page/美風舞良.md "wikilink")
  - 裁判長 / [風莉じん](../Page/風莉じん.md "wikilink")
  - モニカ・クライド(レオナ之妹･原創角色) / [純矢ちとせ](../Page/純矢ちとせ.md "wikilink")（宝月茜）

**「逆轉裁判2～復甦的真實，再臨～」主要演员**

  - フェニックス・ライト(成歩堂 龍一) / [蘭寿とむ](../Page/蘭寿とむ.md "wikilink")
  - ルーチェ・アレイア(女主角･原創角色)/[純矢ちとせ](../Page/純矢ちとせ.md "wikilink")
  - マイルズ・エッジワース(御剣 -{怜}-侍) / [悠未ひろ](../Page/悠未ひろ.md "wikilink")
  - フランジスカ・ヴォン・カルマ(狩魔 冥) / [藤咲えり](../Page/藤咲えり.md "wikilink")
  - マヤ・フェイ(綾里 真宵) / [すみれ乃麗](../Page/すみれ乃麗.md "wikilink")
  - ディック・ガムシュー(糸鋸 圭介) / [春風弥里](../Page/春風弥里.md "wikilink")
  - ロッタ・ハート(大沢木ナツミ) / [美風舞良](../Page/美風舞良.md "wikilink")
  - 裁判長 / [風莉じん](../Page/風莉じん.md "wikilink")
  - ローズ・アレイア(ルーチェ之母，被告･原創角色) / [光あけみ](../Page/光あけみ.md "wikilink")
  - ローランド・スミス(ルーチェ的戀人･原創角色) / [七海ひろき](../Page/七海ひろき.md "wikilink")（王都楼
    真悟）

## 电影

[電影版於](../Page/逆轉裁判_\(電影\).md "wikilink")2012年2月11日上映，由[三池崇史執導](../Page/三池崇史.md "wikilink")\[10\]。

**演员表**

  - 成步堂 龍一 / [成宫宽贵](../Page/成宫宽贵.md "wikilink")
  - 綾里 真宵 / [桐谷美玲](../Page/桐谷美玲.md "wikilink")
  - 御劍 -{怜}-侍 / [斋藤工](../Page/斋藤工.md "wikilink")

## 电视动画

在2015年9月17日开幕的TGS[东京电玩展上](../Page/东京电玩展.md "wikilink")，[卡普空公布了](../Page/卡普空.md "wikilink")《逆转裁判》TV动画化的决定，动画由[A-1
Pictures制作](../Page/A-1_Pictures.md "wikilink")，于2016年4月2日在日本电视台、读卖电视台等机构放送\[11\]\[12\]。
动画第二季將在2018年10月6日開始播放\[13\]。

## 其他趣聞

  - 動畫《[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")》第十一集「孤島症候群·後編」的後半段，春日在進行推理的場景，有着模仿逆轉裁判內人物動作的戲份。被模仿的角色主要是成步堂
    龍一（[涼宮春日](../Page/涼宮春日.md "wikilink")）和御劍
    -{怜}-侍（古泉一樹）。朝比奈於後段也有稍稍模仿綾里
    真宵的頷首動作。（在小片段「涼宮的逆轉」中，涼宮叫「等」和「反對」的配音則是綾里 千尋）
  - 在「再會，然後逆轉」中，日版的遊戲狩魔 冥是日裔美國人，葉中 長閑開的車是美國車；而美版的遊戲將狩魔 冥改成德國人，將葉中
    長閑開的車改成英國車（美國的駕駛座在左邊，而日本和英國的駕駛座在右邊）。
  - 動畫《[NO GAME NO LIFE
    遊戲人生](../Page/NO_GAME_NO_LIFE_遊戲人生.md "wikilink")》第四集「國王（Grand
    Master）」的前半段，空和白在爭執王位時，出現了「等」和「反對」的效果畫面；背景音乐也有模仿法庭争论时的背景音乐。台上主持儀式的人也長得跟審判長十分相似。

## 相關商品

### 音樂CD

  - 逆転裁判+逆転裁判2 オリジナル・サウンドトラック（2004年3月31日發售）\[14\]\[15\]
  - 逆転裁判3 オリジナル・サウンドトラック（2004年3月31日發售）\[16\]
  - 逆転裁判 蘇る逆転 オリジナル・サウンドトラック（2006年3月31日發售）
  - 逆転裁判 オーケストラアルバム 〜GYAKUTEN MEETS
    [ORCHESTRA](../Page/管弦樂團.md "wikilink")〜（2006年9月30日發售）\[17\]As
    the name implies, the album consists of jazz arrangements. The CDs
    were originally scheduled for Japanese release only,\[18\]\[19\]
  - 逆転裁判 ジャズアルバム 〜GYAKUTEN MEETS [JAZZ](../Page/爵士樂.md "wikilink")
    SOUL〜（2007年3月31日發售）
  - 逆転裁判4 オリジナル・サウンドトラック（2007年6月27日發售）
  - 逆転裁判 特別法廷2008 オーケストラコンサート 〜GYAKUTEN MEETS ORCHESTRA〜（2008年7月16日發售）
  - 逆転検事 オリジナル・サウンドトラック（2009年6月24日發售）

### 書籍

  - 逆転裁判真相解明マニュアル
  - 逆転裁判2 真相解明マニュアル
  - 逆転裁判3 真相解明マニュアル
  - 逆転裁判 蘇る逆転 真相解明マニュアル
  - 逆転裁判画集 THE ART OF GYAKUTENSAIBAN
  - 逆転裁判4 公式ガイドブック

## 外部連結

  - [逆轉裁判系列](https://web.archive.org/web/20080208155936/http://www.capcom.co.jp/gyakutensaiban/)

## 參考文獻

[逆转裁判系列](../Category/逆转裁判系列.md "wikilink")
[Category:卡普空遊戲](../Category/卡普空遊戲.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink") [Category:週刊Young
Magazine](../Category/週刊Young_Magazine.md "wikilink")
[Category:電子遊戲系列](../Category/電子遊戲系列.md "wikilink")
[Category:法律題材作品](../Category/法律題材作品.md "wikilink")

1.  [《逆转裁判123
    成步堂精选集》将在2019年夏季通过更新支持中文](http://www.vgtime.com/topic/1048470.jhtml).VGtime游戏时光.2019-02-27.\[2019-02-27\].
2.  [1](https://itunes.apple.com/us/developer/capcom/id308807391)
3.  [Apollo Justice: Ace Attorney for DS - Apollo Justice: Ace Attorney
    Nintendo DS - Apollo Justice: Ace Attorney DS
    Game](http://www.gamespot.com/ds/adventure/gyakutansaiban4/index.html)
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17. [たのみこむ「逆転裁判」](http://www.tanomi.com/saiban/)
18. [逆転裁判シリーズ公式サイト](http://www.capcom.co.jp/gyakutensaiban/topics/060818orchestra/index.html)

19. [GAF - News - Phoenix Wright leaves courtroom, enters
    orchestra](http://www.gamesarefun.com/news.php?newsid=6807)