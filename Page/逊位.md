**逊位**，或称**退位**，是指[君主或其他统治者](../Page/君主.md "wikilink")（通常特指[世袭产生的统治者](../Page/世袭.md "wikilink")）放弃自己的职务和地位的行为，但並不等同於交出權力。

## 歷史

在19世纪以前，各国的[王公除非是战争失败或者政治失勢](../Page/诸侯.md "wikilink")，否则多半不会放弃自己的权力；因而鲜有君主主动逊位的情况。例如[法國皇帝](../Page/法国君主列表.md "wikilink")[拿破崙一世因為戰爭失敗](../Page/拿破崙一世.md "wikilink")，而導致兩次退位並流放。

在近代和现代，[立宪君主和](../Page/君主立宪制.md "wikilink")[专制君主会因许多不同的原因而逊位](../Page/君主專制.md "wikilink")。20世紀，則有[英國君主](../Page/英國君主.md "wikilink")[愛德華八世為了婚姻問題退位](../Page/愛德華八世.md "wikilink")，改稱「溫莎公爵」。

进入21世纪，年老是君主退位的主要原因之一。2013年，[荷兰女王](../Page/荷兰君主列表.md "wikilink")[贝娅特丽克丝退位](../Page/贝娅特丽克丝_\(荷兰\).md "wikilink")，她的儿子[威廉-亚历山大继位](../Page/威廉-亞歷山大_\(荷蘭\).md "wikilink")，当时是欧洲最年少的君主。次年，[西班牙国王](../Page/西班牙国王.md "wikilink")[胡安·卡洛斯一世退位](../Page/胡安·卡洛斯一世.md "wikilink")，其子[費利佩六世继位并取代威廉](../Page/費利佩六世.md "wikilink")-亚历山大成为欧洲最年轻的君主。在2017年，[日本天皇](../Page/天皇.md "wikilink")[明仁因年事已高及健康问题也宣佈即將於](../Page/明仁.md "wikilink")2019年4月30日退位。

[教廷也有](../Page/教廷.md "wikilink")[退位的情況](../Page/退位.md "wikilink")，2013年2月11日，[梵蒂岡宣布](../Page/梵蒂岡.md "wikilink")[本篤十六世因健康因素決定退位](../Page/本篤十六世.md "wikilink")，成為繼1415年[格列高利十二世退位後](../Page/格列高利十二世.md "wikilink")，六個世紀以來，首位退位的[教皇](../Page/教皇.md "wikilink")，引起[德國及天主教國家民眾和領袖的廣大反響](../Page/德國.md "wikilink")。

[人均寿命自](../Page/人均寿命.md "wikilink")20世纪初有了大幅提高，现代，如在君主[老死后才](../Page/老死.md "wikilink")[继位](../Page/即位.md "wikilink")，则[储君等待继位时间将变得非常漫长](../Page/储君.md "wikilink")。2013年时，[英国王储](../Page/英國王位繼承.md "wikilink")[查尔斯虽然已达英国](../Page/威爾斯親王查爾斯.md "wikilink")[法定退休年龄](../Page/退休.md "wikilink")\[1\]，但是至2019年2月時，都未能继承英国王位。

## 注释

## 參見

  - [禅让制](../Page/禅让制.md "wikilink")

  - [退位君主列表](../Page/退位君主列表.md "wikilink")

  -
[逊位](../Category/逊位.md "wikilink")
[Category:王位继承](../Category/王位继承.md "wikilink")

1.