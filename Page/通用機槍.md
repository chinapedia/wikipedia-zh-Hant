[MG34.jpg](https://zh.wikipedia.org/wiki/File:MG34.jpg "fig:MG34.jpg")是世界上第一種被定位為通用機槍的中型機槍，[彈鏈供彈](../Page/彈鏈.md "wikilink")、可快速更換槍管。\]\]
**通用機槍**（[英文](../Page/英文.md "wikilink")：），也叫轻重两用机枪，是一種多用途的[機槍](../Page/機槍.md "wikilink")，可執行[輕機槍及](../Page/輕機槍.md "wikilink")[中型機槍的任務](../Page/中型機槍.md "wikilink")，輕型設定時可由單人攜帶。近代的通用機槍採用氣泠設計，發射[7.62×51
NATO或](../Page/7.62×51_NATO.md "wikilink")[7.62×54R中口徑全威力彈藥](../Page/7.62×54R子彈.md "wikilink")，附有兩腳架亦可裝在三腳架上或車輛上。

## 歷史

丹麦生产的[麥德森輕機槍首创通用机枪的原型](../Page/麥德森輕機槍.md "wikilink")，但由于麦德森机枪采用弹匣供弹，多数被当作轻机枪使用。

在[一戰之后](../Page/一戰.md "wikilink")，由於[凡爾賽條約中規定德國軍隊不得擁有重机枪](../Page/凡爾賽條約.md "wikilink")（当时对重机枪的定义是使用[彈鏈供彈](../Page/彈鏈.md "wikilink")，有三脚架，水冷而能长时间射击的機關槍），為了迴避條約的規定以及降低西方國家的質疑，[德國受](../Page/德國.md "wikilink")[麥德森輕機槍的启发](../Page/麥德森輕機槍.md "wikilink")，研發得以進行長時間連射而重量輕於以往重機槍的新型機槍；研發的成果首先於1926年採用了[MG13並確定了通用機槍的研發路線](../Page/MG13通用機槍.md "wikilink")；隨後於1934年制造了[MG34通用機槍](../Page/MG34通用機槍.md "wikilink")（），一種多用途的中口徑機槍，取代[MG08及MG15](../Page/MG08重機槍.md "wikilink")，MG34採用氣泠設計、[彈鏈以及](../Page/彈鏈.md "wikilink")[彈鼓的選擇式供彈](../Page/彈鼓.md "wikilink")、可快速更換槍管、附有兩腳架以及仿自麦德森机枪的软式三腳架，兩腳架時為步兵使用之[輕機槍](../Page/輕機槍.md "wikilink")，換裝三腳架時即可成為[重機槍使用](../Page/重機槍.md "wikilink")，用途廣泛。

[二戰時期](../Page/二戰.md "wikilink")，德軍於步兵單位以及車輛上裝備的[MG34在戰場上效果良好](../Page/MG34通用機槍.md "wikilink")，深受士兵愛戴，後來更以MG34改良出[MG42](../Page/MG42通用機槍.md "wikilink")，MG42更是二戰中最具作戰能力的通用機槍。

由于德国[MG34通用機槍是二战著名武器](../Page/MG34通用機槍.md "wikilink")，因此很多人定性其为第一种通用机枪。

1960年代出現的[FN
MAG](../Page/FN_MAG通用機槍.md "wikilink")（MAG-58）、[MG3](../Page/MG3通用機槍.md "wikilink")、[PK／PKM亦是著名的通用機槍之一](../Page/PK通用機槍.md "wikilink")，在各國軍隊中最為常見。美軍以[M249](../Page/M249班用自動武器.md "wikilink")、[M60](../Page/M60通用機槍.md "wikilink")／[M240](../Page/M240通用機槍.md "wikilink")、[白朗寧M2以組成輕中重火力系统](../Page/白朗寧M2重機槍.md "wikilink")，其他國家亦有相似的火力組合。

總括而言，通用機槍就是可由單人攜帶、氣泠設計、彈鏈供彈、可快速更換槍管、附有兩腳架亦可裝在三腳架上或車輛上的中型機槍。

## 图片

<File:MAG-latrun-exhibition-1.jpg>|[比利時制造的](../Page/比利時.md "wikilink")[FN
MAG](../Page/FN_MAG通用機槍.md "wikilink")。
<File:M240G-0167-2004-01.jpg>|[美軍的](../Page/美軍.md "wikilink")[M240G通用機槍](../Page/M240通用機槍.md "wikilink")。
<File:PEO> M240B
Profile.jpg|[美軍的](../Page/美軍.md "wikilink")[M240B通用機槍](../Page/M240通用機槍.md "wikilink")。
<File:PEO> M240L MG Telescoping Stock Right
Side.jpg|[美軍的](../Page/美軍.md "wikilink")[M240L通用機槍](../Page/M240通用機槍.md "wikilink")。
<File:M60> machine gun
DM-ST-90-01312.jpg|三腳架上的[M60E3通用機槍](../Page/M60通用機槍.md "wikilink")。
[File:M60iraq2003.jpg|固定在車輛上的](File:M60iraq2003.jpg%7C固定在車輛上的)[M60E4／Mk
43通用機槍](../Page/M60通用機槍.md "wikilink")。 <File:Mk> 48 PEO Soldier.jpg|[Mk
48輕量化機槍](../Page/Mk_48輕量化機槍.md "wikilink")。 <File:A> Kurdish Peshmerga
officer fires a PKM in Mosul,
.jpg|[俄羅斯](../Page/俄羅斯.md "wikilink")／[蘇聯的](../Page/蘇聯.md "wikilink")[PKM通用機槍](../Page/PK通用機槍.md "wikilink")。
<File:Демонстрация> стрельбы из пулеметов ПКП Печенег - 4-й гвардейской
Кантемировской танковой дивизии
01.jpg|[Pecheneg通用機槍](../Page/Pecheneg通用機槍.md "wikilink")。
<File:ChineseType672MG.jpg>|[中華人民共和國的](../Page/中華人民共和國.md "wikilink")[67-2式機槍](../Page/67式機槍.md "wikilink")。
<File:VOA> Arrott - A View of Syria, Under Government Crackdown
08.jpg|[80式通用機槍](../Page/80式通用機槍.md "wikilink")。 <File:Mitrailleuse-IMG>
1731.jpg|[法國的](../Page/法國.md "wikilink")[AA-52通用機槍](../Page/AA-52通用機槍.md "wikilink")。
<File:Mg3idef2007.JPG>|[德國的](../Page/德國.md "wikilink")[MG3通用機槍](../Page/MG3通用機槍.md "wikilink")（以MG42改進而成）。
<File:T74_machine_gun.jpg>|[中華民國的](../Page/中華民國.md "wikilink")[T74排用機槍](../Page/T74排用機槍.md "wikilink")。
<File:UKM2000P>
REMOV.jpg|[波兰的](../Page/波兰.md "wikilink")[UKM-2000通用機槍](../Page/UKM-2000通用機槍.md "wikilink")。

## 參考

  - [輕機槍](../Page/輕機槍.md "wikilink")
  - [中型機槍](../Page/中型機槍.md "wikilink")
  - [重機槍](../Page/重機槍.md "wikilink")

[it:Mitragliatrice media\#Mitragliatrici ad uso
generalizzato](../Page/it:Mitragliatrice_media#Mitragliatrici_ad_uso_generalizzato.md "wikilink")

[\*](../Category/通用機槍.md "wikilink")