**通用磨坊**（**General
Mills**，）是一間[財富500強](../Page/財富500強.md "wikilink")[企業](../Page/企業.md "wikilink")，主要從事[食品加工業務](../Page/食品加工.md "wikilink")，為世界第六大食品公司，總部設於[美國](../Page/美國.md "wikilink")[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")[明尼阿波利斯](../Page/明尼阿波利斯.md "wikilink")[黃金谷](../Page/黃金谷.md "wikilink")。現時其產品廣銷全球，主要品牌包括[哈根达斯](../Page/哈根达斯.md "wikilink")、[Betty
Crocker](../Page/Betty_Crocker.md "wikilink")、[綠巨人
(品牌)及](../Page/綠巨人_\(品牌\).md "wikilink")[灣仔碼頭水餃等](../Page/灣仔碼頭水餃.md "wikilink")，以及多款[早餐穀物品牌](../Page/早餐穀物.md "wikilink")。

## 历史

创建于1866年美國明尼蘇達州[密西西比河兩岸上的麵粉工廠](../Page/密西西比河.md "wikilink")；他們的創新，徹底改革了麵粉研磨工業，成為今天通用磨坊的基礎。至今，通用磨坊遍佈100多個國家，經營60多種品牌，全球員工總人數超過40,000人，是全世界十大食品製造公司之一。

2011年3月21日，通用磨坊以6億歐元（约22億美元）收購全球第二大酸奶制造商[優沛蕾](../Page/優沛蕾.md "wikilink")（Yoplait）。

2014年9月5日，通用磨坊以8.2億美元现金收購有機食品生產商安妮天然食品公司Annie's Homegrown

2018年2月23日，通用磨坊宣布斥資80億美元收購寵物飼料製造商Blue Buffalo Pet Food。

## 外部連結

  - [通用磨坊官方網站](https://www.generalmills.com)\<\!--舊域名http://www.genmills.com/error.aspx?item=%2f\&user=sitecore%5cAnonymous\&site=scheduler

<http://web.archive.org/web/20130402111812/http://genmills.com/-->\>

  - [台灣通用磨坊官方網站](http://www.generalmills.com.tw)

[通用磨坊](../Category/通用磨坊.md "wikilink")
[Category:美國食品公司](../Category/美國食品公司.md "wikilink")
[Category:總部在美國的跨國公司](../Category/總部在美國的跨國公司.md "wikilink")
[Category:明尼蘇達州公司](../Category/明尼蘇達州公司.md "wikilink")
[Category:食品品牌](../Category/食品品牌.md "wikilink")
[Category:纽约证券交易所上市公司](../Category/纽约证券交易所上市公司.md "wikilink")
[Category:1866年成立的公司](../Category/1866年成立的公司.md "wikilink")