**速度滑冰**，是冰雪运动中历史最悠久，开展最广泛的体育项目之一。男子速滑比赛在1924年就被列为[冬季奥运会的比赛项目](../Page/冬季奥运会.md "wikilink")，女子比赛在1960年被列为冬季奥运会的比赛项目。

比赛的赛道周长为400[米](../Page/米.md "wikilink")，跑道内外两道，道宽5米。比赛时两人同时滑跑，每滑一圈交换一次内外道。

## 世界紀錄

<small>更新日期於：2014年3月1日</small>

### 男子紀錄

<table>
<thead>
<tr class="header">
<th><p>競賽項目</p></th>
<th><p>紀錄保持人</p></th>
<th><p>國籍</p></th>
<th><p>紀錄</p></th>
<th><p>日期</p></th>
<th><p>地點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>500米</p></td>
<td><p><a href="../Page/杰里米·沃瑟斯普.md" title="wikilink">杰里米·沃瑟斯普</a></p></td>
<td></td>
<td><p>34.03</p></td>
<td><p>2007年11月9日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="even">
<td><p>2×500米</p></td>
<td><p><a href="../Page/杰里米·沃瑟斯普.md" title="wikilink">杰里米·沃瑟斯普</a></p></td>
<td></td>
<td><p>1:08.17</p></td>
<td><p>2007年11月11日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="odd">
<td><p>1000米</p></td>
<td><p><a href="../Page/沙尼·戴维斯.md" title="wikilink">沙尼·戴维斯</a></p></td>
<td></td>
<td><p>1:06.42</p></td>
<td><p>2009年3月9日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="even">
<td><p>1500米</p></td>
<td><p><a href="../Page/沙尼·戴维斯.md" title="wikilink">沙尼·戴维斯</a></p></td>
<td></td>
<td><p>1:41.04</p></td>
<td><p>2009年12月11日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="odd">
<td><p>3000米</p></td>
<td><p><a href="../Page/艾斯科爾·爾維克.md" title="wikilink">艾斯科爾·爾維克</a></p></td>
<td></td>
<td><p>3:37.28</p></td>
<td><p>2009年12月11日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="even">
<td><p>5000米</p></td>
<td><p><a href="../Page/斯文·克雷默.md" title="wikilink">斯文·克雷默</a></p></td>
<td></td>
<td><p>6:03.32</p></td>
<td><p>2007年11月17日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="odd">
<td><p>10000米</p></td>
<td><p><a href="../Page/斯文·克雷默.md" title="wikilink">斯文·克雷默</a></p></td>
<td></td>
<td><p>12:41.69</p></td>
<td><p>2007年3月10日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="even">
<td><p>短距離全能</p></td>
<td><p><a href="../Page/麦克·穆德.md" title="wikilink">麦克·穆德</a></p></td>
<td></td>
<td><p>136.790分</p></td>
<td><p>2013年1月27日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="odd">
<td><p>小全能</p></td>
<td><p><a href="../Page/埃尔本·文内马尔斯.md" title="wikilink">埃尔本·文内马尔斯</a></p></td>
<td></td>
<td><p>146.365分</p></td>
<td><p>2005年8月13日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="even">
<td><p>大全能</p></td>
<td><p><a href="../Page/沙尼·戴维斯.md" title="wikilink">沙尼·戴维斯</a></p></td>
<td></td>
<td><p>145.742分</p></td>
<td><p>2006年3月19日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="odd">
<td><p>集體滑（8圈）</p></td>
<td><p><a href="../Page/斯文·克雷默.md" title="wikilink">斯文·克雷默</a><br />
<a href="../Page/卡尔·维尔赫扬.md" title="wikilink">卡尔·维尔赫扬</a><br />
<a href="../Page/埃尔本·文内马尔斯.md" title="wikilink">埃尔本·文内马尔斯</a></p></td>
<td></td>
<td><p>3:37.80</p></td>
<td><p>2007年3月11日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
</tbody>
</table>

### 女子紀錄

<table>
<thead>
<tr class="header">
<th><p>競賽項目</p></th>
<th><p>紀錄保持人</p></th>
<th><p>國籍</p></th>
<th><p>紀錄</p></th>
<th><p>日期</p></th>
<th><p>地點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>500米</p></td>
<td><p><a href="../Page/李相花.md" title="wikilink">李相花</a></p></td>
<td></td>
<td><p>36.36</p></td>
<td><p>2013年11月16日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="even">
<td><p>2×500米</p></td>
<td><p><a href="../Page/李相花.md" title="wikilink">李相花</a></p></td>
<td></td>
<td><p>1:12.93</p></td>
<td><p>2013年11月16日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="odd">
<td><p>1000米</p></td>
<td><p><a href="../Page/布列塔尼·鲍威.md" title="wikilink">布列塔尼·鲍威</a></p></td>
<td></td>
<td><p>1:12.58</p></td>
<td><p>2013年11月17日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="even">
<td><p>1500米</p></td>
<td><p><a href="../Page/辛迪·克拉森.md" title="wikilink">辛迪·克拉森</a></p></td>
<td></td>
<td><p>1:51.79</p></td>
<td><p>2005年11月20日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="odd">
<td><p>3000米</p></td>
<td><p><a href="../Page/辛迪·克拉森.md" title="wikilink">辛迪·克拉森</a></p></td>
<td></td>
<td><p>3:53.34</p></td>
<td><p>2006年3月18日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="even">
<td><p>5000米</p></td>
<td><p><a href="../Page/瑪爾蒂娜·薩布利科娃.md" title="wikilink">瑪爾蒂娜·薩布利科娃</a></p></td>
<td></td>
<td><p>6:42.66</p></td>
<td><p>2011年2月18日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="odd">
<td><p>10000米</p></td>
<td><p><a href="../Page/瑪爾蒂娜·薩布利科娃.md" title="wikilink">瑪爾蒂娜·薩布利科娃</a></p></td>
<td></td>
<td><p>13:48.33</p></td>
<td><p>2007年3月15日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="even">
<td><p>短距離全能</p></td>
<td><p><a href="../Page/海瑟·理查德森.md" title="wikilink">海瑟·理查德森</a></p></td>
<td></td>
<td><p>147.735分</p></td>
<td><p>2013年1月20日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="odd">
<td><p>迷你全能</p></td>
<td><p><a href="../Page/辛迪·克拉森.md" title="wikilink">辛迪·克拉森</a></p></td>
<td></td>
<td><p>155.576分</p></td>
<td><p>2007年11月11日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="even">
<td><p>小全能</p></td>
<td><p><a href="../Page/辛迪·克拉森.md" title="wikilink">辛迪·克拉森</a></p></td>
<td></td>
<td><p>154.580分</p></td>
<td><p>2006年3月19日</p></td>
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
</tr>
<tr class="odd">
<td><p>集體滑（6圈）</p></td>
<td><p><a href="../Page/克利斯蒂·内斯比特.md" title="wikilink">克利斯蒂·内斯比特</a><br />
<a href="../Page/克莉絲蒂娜·格罗夫斯.md" title="wikilink">克莉絲蒂娜·格罗夫斯</a><br />
<a href="../Page/布里特尼·舒斯勒.md" title="wikilink">布里特尼·舒斯勒</a></p></td>
<td></td>
<td><p>2:55.79</p></td>
<td><p>2009年12月6日</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
</tbody>
</table>

## 參考資料

## 相关条目

  - [梁洪涛](../Page/梁洪涛.md "wikilink")

## 外部連結

  - [Roller Sports C. I. C. - Sport
    regulations](http://www.rollersports.org/public/pagein/File/CIC/2009_CIC_SPORTS_RULES.pdf),
    regulations of inline speed skating
  - [International Skating Union](http://www.isu.org/speed-skating)

[category:速度滑冰](../Page/category:速度滑冰.md "wikilink")

[Category:冬季運動](../Category/冬季運動.md "wikilink")
[Category:荷兰发明](../Category/荷兰发明.md "wikilink")