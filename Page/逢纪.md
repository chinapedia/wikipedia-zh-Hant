**逄紀**（），字**元圖**，[東漢末](../Page/東漢.md "wikilink")[南陽郡人](../Page/南陽郡.md "wikilink")\[1\]。

## 生平

逄纪早年曾为[何进的谋士](../Page/何进.md "wikilink")。何进欲除[蹇硕](../Page/蹇硕.md "wikilink")，亲信[张津劝他选贤良为国除害](../Page/张津.md "wikilink")，后又广征智谋之士逢纪、[何顒](../Page/何顒.md "wikilink")、[許攸等为心腹](../Page/許攸.md "wikilink")。

### 獻計取冀

[袁紹逃離](../Page/袁紹.md "wikilink")[董卓勢力範圍時](../Page/董卓.md "wikilink")，與逄纪、[許攸同到](../Page/許攸.md "wikilink")[冀州](../Page/冀州.md "wikilink")。袁紹甚為賞識逄纪的智謀而重用。袁紹初到冀州，依賴[韓馥提供糧食](../Page/韓馥.md "wikilink")。[初平二年](../Page/初平.md "wikilink")（191年），逄纪提議利用[公孫瓚攻擊韓馥](../Page/公孫瓚.md "wikilink")，實則提議韓馥和袁紹共同防禦冀州，結果袁紹成功鵲巢鳩占。[官渡之戰中令逄纪統軍事](../Page/官渡之戰.md "wikilink")。[建安五年](../Page/建安.md "wikilink")（200年），袁紹在官渡之戰敗回，後悔當初不聽[田豐之計](../Page/田豐.md "wikilink")，逄纪進言稱田豐知道袁紹兵敗後取笑袁紹不聽其言，使田豐被殺。[審配與逄纪有私怨](../Page/審配.md "wikilink")，官渡之戰後審配兩位兒子被虜，有人對審配有讒言，但逄纪肯定審配的節氣，袁紹以逄纪公私分明大加讚賞，逄纪與審配成為好友。

### 輔助袁尚

建安七年（202年）袁紹憂憤而死。袁紹以[袁尚美貌及後妻劉氏所喜愛而欲立為繼承人](../Page/袁尚.md "wikilink")，但未正式表態。眾人-{欲}-以長子[袁譚立為繼承人](../Page/袁譚.md "wikilink")，但逄紀、審配一派與[辛評](../Page/辛評.md "wikilink")、[郭圖](../Page/郭圖.md "wikilink")、袁譚一派不和，逄紀等因為懼怕袁譚即位後加害，私下改袁紹遺命，立袁尚繼位。袁譚不能繼位，自稱[車騎將軍](../Page/車騎將軍.md "wikilink")，屯[黎陽](../Page/黎陽.md "wikilink")。袁尚不給袁譚多兵，使逄紀隨之。袁譚要求配兵為審配拒絕，一怒之下殺了逄紀。

## 評價

  - [孔融曰](../Page/孔融.md "wikilink")：「審配、逢紀，盡忠之臣也，任其事」
  - [荀彧曰](../Page/荀彧.md "wikilink")：「逢紀果而自用」

## 参考文献

  - 《[三国志](../Page/三国志.md "wikilink")·[魏書六·董二袁劉傳](../Page/s:三國志/卷06.md "wikilink")》
  - 《[三国志](../Page/三国志.md "wikilink")·[魏書十·荀彧荀攸賈詡傳](../Page/s:三國志/卷10.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀五十二](../Page/s:資治通鑑/卷060.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀五十五](../Page/s:資治通鑑/卷063.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀五十六](../Page/s:資治通鑑/卷064.md "wikilink")》
  - 《[后汉书](../Page/后汉书.md "wikilink")·窦何列传第五十九》

[F](../Category/東漢政治人物.md "wikilink")
[F](../Category/三國政治人物.md "wikilink")
[F](../Category/三國被處決者.md "wikilink")
[F](../Category/南陽人.md "wikilink")
[J紀](../Category/逢姓.md "wikilink")

1.  《資治通鑑·卷60》：及南陽許攸、**逢紀**、潁川荀諶皆爲謀主。