在[拓撲學和](../Page/拓撲學.md "wikilink")[數學的相關領域裡](../Page/數學.md "wikilink")，**連續函數**是指在[拓撲空間之間的一種](../Page/拓撲空間.md "wikilink")[態射](../Page/態射.md "wikilink")。直觀上來說，其為一個函數*f*，其中每一群在*f(x)*附近的點都會含有在*x*附近的一群點之[值](../Page/值域.md "wikilink")。對一個一般的拓撲空間來說，這是指*f(x)*的[鄰域總會包含著](../Page/鄰域.md "wikilink")*x*之鄰域的值。

在一個[度量空間](../Page/度量空間.md "wikilink")(如[實數](../Page/實數.md "wikilink"))裡，這是指在*f(x)*一定距離內的點總會包含著在*x*某些距離內的所有點。

## 定義

因為有若干個[對拓撲結構的等價定義存在](../Page/拓撲空間範疇的特性描述.md "wikilink")，所以亦存在若干種定義連續函數的方法。

### 開集與閉集定義

拓撲中最常見的連續概念之定義為將其定義為一個其[開集之](../Page/開集.md "wikilink")[前像亦為](../Page/值域.md "wikilink")[開集的函數](../Page/開集.md "wikilink")。類似開集的公式化，亦有一**閉集公式化**，其將連續函數定義為其[閉集之](../Page/閉集.md "wikilink")[前像亦為](../Page/值域.md "wikilink")[閉集的函數](../Page/閉集.md "wikilink")。

### 鄰域定義

以前像為基底之定義時常很難直接地被使用。替代地，設有一由*X*至*Y*的函數*f*，其中的*X*和*Y*都是拓撲空間。則*f*會被稱為是**在*x*為連續的**，其中*x*為*X*的元素，若對於任一*f*(*x*)的[鄰域](../Page/鄰域.md "wikilink")*V*，都存在一個能使\(f(U) \subseteq V\)之*x*的鄰域*U*。雖然此一定義看起來很複雜，其在直覺上是指不論*V*變得多「小」，總會可以找到一個包含可映射至*V*內之*x*的*U*。若*f*在*X*內的每一個元素*x*都會連續，則簡稱*f*是連續的。

<center>

[continuity_topology.svg](https://zh.wikipedia.org/wiki/File:continuity_topology.svg "fig:continuity_topology.svg")

</center>

在一[度量空間內](../Page/度量空間.md "wikilink")，則其會等價於將所有鄰域替換成考量以*x*和*f*(*x*)為中心之[開球的](../Page/球_\(數學\).md "wikilink")[邻域系统](../Page/邻域系统.md "wikilink")。這會導致在實分析中對[連續函數的標準定義](../Page/連續函數.md "wikilink")，其敘述著一個函數若為連續時，則其靠近*x*的所有點都會映射至靠近*f*(*x*)的點上。這只在度量空間中有意義，因為只有在度量空間中有距離的概念。

### 數列和網

在一些文章中，空間的拓撲會被簡便地以[極限點來描述](../Page/極限點.md "wikilink")。

[L](../Category/拓扑学.md "wikilink") [L](../Category/连续映射.md "wikilink")