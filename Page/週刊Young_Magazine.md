《**週刊Young
Magazine**》（ヤングマガジン，簡稱“Yanmaga”）是[日本](../Page/日本.md "wikilink")[講談社每](../Page/講談社.md "wikilink")[週一發行的](../Page/週一.md "wikilink")[青年漫畫](../Page/青年漫畫.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")，1980年6月23日創刊。創刊時的[編輯長為](../Page/編輯長.md "wikilink")[宮原照夫](../Page/宮原照夫.md "wikilink")。

## 連載過的作品

  - 《[頭文字D](../Page/頭文字D.md "wikilink")》（[重野秀一](../Page/重野秀一.md "wikilink")）
  - 《[Kiss×sis](../Page/Kiss×sis.md "wikilink")》（[Ditama某](../Page/Ditama某.md "wikilink")）
  - 《[亞基拉](../Page/亚基拉.md "wikilink")》（[大友克洋](../Page/大友克洋.md "wikilink")）
  - 《[去吧！稻中兵團](../Page/去吧！稻中乒团.md "wikilink")》（[古谷實](../Page/古谷實.md "wikilink")）
  - 《[17歲青春遁走](../Page/17岁青春遁走.md "wikilink")》（[古谷實](../Page/古谷實.md "wikilink")）
  - 《[攻殼機動隊](../Page/攻殼機動隊.md "wikilink")》（[士郎正宗](../Page/士郎正宗.md "wikilink")）
  - 《[3×3
    EYES](../Page/三只眼.md "wikilink")》（[高田裕三](../Page/高田裕三.md "wikilink")）
  - 《[Chobits](../Page/Chobits.md "wikilink")》（[CLAMP](../Page/CLAMP.md "wikilink")）
  - 《[×××HOLiC](../Page/×××HOLiC.md "wikilink")》（[CLAMP](../Page/CLAMP.md "wikilink")）
  - 《[戰國天正記](../Page/戰國天正記.md "wikilink")》（[宮下英樹](../Page/宮下英樹.md "wikilink")）
  - 《[MF
    GHOST](../Page/MF_GHOST.md "wikilink")》（[重野秀一](../Page/重野秀一.md "wikilink")）
  - 《[賭博默示錄](../Page/賭博默示錄.md "wikilink")》（[福本伸行](../Page/福本伸行.md "wikilink")）
  - 《[少女不十分](../Page/少女不十分.md "wikilink")》（[西尾维新](../Page/西尾维新.md "wikilink")）

## 參考資料

<references/>

## 外部連結

  - [Young
    Magazine官方網站](http://kc.kodansha.co.jp/magazine/index.php/02888)

[\*](../Category/週刊Young_Magazine.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:青年漫畫雜誌](../Category/青年漫畫雜誌.md "wikilink")
[Category:週刊漫畫雜誌](../Category/週刊漫畫雜誌.md "wikilink")
[Y](../Category/講談社的漫畫雜誌.md "wikilink")
[Category:1980年建立的出版物](../Category/1980年建立的出版物.md "wikilink")
[Category:1980年日本建立](../Category/1980年日本建立.md "wikilink")