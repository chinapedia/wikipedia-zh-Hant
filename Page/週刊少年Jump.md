《**週刊少年Jump**》（）由日本[集英社發行](../Page/集英社.md "wikilink")，于1968年（[昭和](../Page/昭和.md "wikilink")43年）7月創刊，當時是[雙週刊](../Page/雙週刊.md "wikilink")，於1969年轉為[週刊](../Page/週刊.md "wikilink")，每周一发售。刊载作品以动作冒险类为主，多带有幻想味道，并刻意张扬个性，追求情节的峰回路转。间或也有部分恋爱、运动及历史题材的作品。多数作品人物造型唯美，因此亦拥有大量少女读者。

为日本發行量最高的连载[漫畫雜誌](../Page/漫畫雜誌.md "wikilink")，位列三大週刊[少年漫畫雜誌](../Page/少年漫画.md "wikilink")（《週刊少年Jump》、《[週刊少年Sunday](../Page/週刊少年Sunday.md "wikilink")》和《[週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")》）之首。於1993年每期發行量突破600萬冊。[台灣的](../Page/台灣.md "wikilink")《[寶島少年](../Page/寶島少年.md "wikilink")》（[東立出版](../Page/東立.md "wikilink")）、[香港的](../Page/香港.md "wikilink")《[EX-am](../Page/EX-am.md "wikilink")》（[文化傳信出版](../Page/文化傳信.md "wikilink")，已停刊）和
中国大陆《漫画行》（[翻翻动漫出版](../Page/翻翻动漫.md "wikilink")） 則是該雜誌漫畫連載的中文版。

## 历史

1968年7月11日，本刊以《少年Jump》名義發行8月1日號，正式創刊。創刊時為雙週刊形式，於每月第二及第四週的星期四發售。1969年10月，宣佈形式轉為週刊，並改於毎週星期二發售，直至2003年5月再改為毎週星期一正式發售。吉祥物標記為Jump海盜。

創刊以來，不少漫畫家都為它撰稿連載而人氣急升，刊登過許多著名的漫畫。1995年，以653万部的紀錄創下日本漫畫雜誌的最高銷售紀錄。現時刊物以初中學生為主要讀者群，當中亦包括接近的小學高年級和高中學生。

### 1968年創刊～1970年代

1968年創刊。創刊號發行數量10萬5000本。連載《[破廉恥學園](../Page/破廉恥學園.md "wikilink")》、《[一條漢子小子大將](../Page/一條漢子小子大將.md "wikilink")》（1968年-）、《[硬頭皮的精神gaeru](../Page/硬頭皮的精神gaeru.md "wikilink")》、《[廁所博士](../Page/廁所博士.md "wikilink")》（1970年-）、《[武士巨人](../Page/武士巨人.md "wikilink")》、《[荒野的少年isamu](../Page/荒野的少年isamu.md "wikilink")》（1971年-）、《[包丁人味平](../Page/包丁人味平.md "wikilink")》、《[-{zh-hans:魔神Z;zh-hk:鐵甲萬能俠;zh-tw:無敵鐵金剛;}-](../Page/無敵鐵金剛.md "wikilink")》（1972年-）、《[玩兒球](../Page/玩兒球.md "wikilink")》（1973年-）等。1971年發行銷量突破100萬本。

### 1970年代

連載《[1·2ahho\!\!](../Page/1·2ahho!!.md "wikilink")》（1975年～）、《[東大一直線](../Page/東大一直線.md "wikilink")》（1976年～）、《[前進\!\!pirate](../Page/前進!!pirate.md "wikilink")》（1977年～）、《[電路的狼](../Page/電路的狼.md "wikilink")》、《[杜伯曼犬刑警](../Page/杜伯曼犬刑警.md "wikilink")》（1975年～）、《[缺乏拳擊場](../Page/缺乏拳擊場.md "wikilink")》（1977年～）、《[眼鏡蛇](../Page/眼鏡蛇_\(漫畫\).md "wikilink")》（1978年～）、《[金肉人](../Page/金肉人.md "wikilink")》（1979年～）。

《[烏龍派出所](../Page/烏龍派出所.md "wikilink")》（1976年～2016年）為漫畫連載最長記錄。1978年發行銷量200萬本突破。

### 1980年代

1980年發行銷量突破300萬本。在重點放在少女的登場人物的作品大量登場。《[-{zh-hans:阿拉蕾;zh-tw:怪博士與機器娃娃;zh-hk:IQ博士}-](../Page/IQ博士.md "wikilink")》、《[高中！奇面組](../Page/高中！奇面組.md "wikilink")》（1980年～）、《[停止\!\!
雲雀](../Page/停止!!_雲雀.md "wikilink")》、《[貓眼](../Page/貓眼三姐妹.md "wikilink")》（1981年～1985年）、《[kickoff](../Page/kickoff.md "wikilink")》、《[舞台兩側的佈景男人](../Page/舞台兩側的佈景男人.md "wikilink")》（1983年～）、《[北斗之拳](../Page/北斗之拳.md "wikilink")》（1983年～1988年）、《[-{zh-hans:橙路;zh-tw:古靈精怪;zh-hk:橙路}-](../Page/橙路.md "wikilink")》（1984年）、《[足球小將翼](../Page/足球小將翼.md "wikilink")》、《[黑·enjeruzu](../Page/黑·enjeruzu.md "wikilink")》（1981年～）《[風魔小次郎](../Page/風魔小次郎.md "wikilink")》、《[請多關照機械船塢](../Page/請多關照機械船塢.md "wikilink")》（1982年～）、《[銀獠牙-流星銀-](../Page/銀獠牙-流星銀-.md "wikilink")》（1983年）等連載為例。發行銷量50萬本上升。

1985年銷量突破400萬。連載《[魁\!\!男塾](../Page/魁!!男塾.md "wikilink")》、《[-{zh-tw:七龍珠;zh-cn:七龙珠;zh-hk:龍珠}-](../Page/七龙珠.md "wikilink")》（1984年～1995年）、《[城市獵人](../Page/城市猎人.md "wikilink")》（1985年～1991年）、《[聖鬥士星矢](../Page/聖鬥士星矢.md "wikilink")》（1986年～1990年）、《[JoJo的奇妙冒險](../Page/JoJo的奇妙冒險.md "wikilink")》（1987年～）、《[暗黑破壞神](../Page/BASTARD!!_－暗黑的破坏神－.md "wikilink")》（1988年～）、《[-{zh-hans:秀逗泰山;zh-tw:秀逗泰山;zh-hk:不文泰山}-](../Page/秀逗泰山.md "wikilink")》（1988年～1995年）。

1989年銷量突破500萬。

### 1990年代

連載《[-{zh-hans:灌篮高手;zh-tw:灌籃高手;zh-hk:男兒當入樽}-](../Page/灌篮高手.md "wikilink")》、《[幽遊白書](../Page/幽遊白書.md "wikilink")》大流行。1991年，發行銷量突破600萬本。當年連載的作品有《[花之慶次](../Page/花之慶次.md "wikilink")》（1990年～）、《[靈異教師神眉](../Page/靈異教師神眉.md "wikilink")》、《[忍空](../Page/忍空.md "wikilink")》、《[幸運超人](../Page/幸運超人.md "wikilink")》（1993年～1997年）、《[-{zh-hans:浪客剑心;zh-tw:神劍闖江湖;zh-hk:浪客劍心}-](../Page/浪客剑心.md "wikilink")》、《[熱鬥小馬](../Page/熱鬥小馬.md "wikilink")》（1994年～1998年）。

發行銷量1995年3-4號653萬本最高紀錄達成。1996年27號《[-{zh-hans:灌篮高手;zh-tw:灌籃高手;zh-hk:男兒當入樽}-](../Page/灌篮高手.md "wikilink")》連載結束。

從1996年開始的發行量減少，到1997年減少至約230萬，也被稱作週刊少年Jump的黑暗期。在這段期間由《[-{zh-hans:浪客剑心;zh-tw:神劍闖江湖;zh-hk:浪客劍心}-](../Page/浪客剑心.md "wikilink")》、《[封神演義](../Page/封神演義_\(漫畫\).md "wikilink")》等作品支撐整個週刊的銷售量，成為這個時期的代表作。另一方面《[遊戲王](../Page/遊戲王.md "wikilink")》（1996年～）《[ONE
PIECE](../Page/ONE_PIECE.md "wikilink")》（1997年～）、《[獵人](../Page/獵人_\(動漫\).md "wikilink")》（1998年～）、《[火影忍者](../Page/火影忍者.md "wikilink")》（1999年～2014年）等熱門少年漫畫開始連載。這些作品成為2000年代的Jump週刊的代表性角色。

### 2000年代

在這時期開始裡面所有作品故事、題材及內容豐富，角色畫風開始走唯美路線，在傳統熱血外追加更多戀愛和女性元素。同時，也受《[ONE
PIECE](../Page/ONE_PIECE.md "wikilink")》、《[火影忍者](../Page/火影忍者.md "wikilink")》等題材影響角色描敘帶有牽引人的特色。

《[BLEACH](../Page/BLEACH.md "wikilink")》（2001～2016年）、《[Mr.FULLSWING強棒出擊](../Page/Mr.FULLSWING強棒出擊.md "wikilink")》（2001～2006年）、《[草莓100%](../Page/草莓100%.md "wikilink")》（2002～2005年）、《[-{zh-hans:光速蒙面侠21;zh-tw:光速蒙面俠21;zh-hk:Eyeshield
21高速達陣}-](../Page/光速蒙面俠21.md "wikilink")》（2002～2009年）、《[棋魂](../Page/棋魂.md "wikilink")》（1999～2003年）、《[網球王子](../Page/網球王子.md "wikilink")》（1999～2008年）、《[死亡筆記](../Page/死亡筆記.md "wikilink")》（2004～2006年）輩出受歡迎作品。之後，《[銀魂](../Page/银魂.md "wikilink")》（2004年～2018年）、《[家庭教師HITMAN
REBORN\!](../Page/家庭教師HITMAN_REBORN!.md "wikilink")》（2004～2012年）、《[驅魔少年](../Page/驅魔少年.md "wikilink")》（2004年～）、《[魔人偵探腦嚙涅羅](../Page/魔人偵探腦嚙涅羅.md "wikilink")》（2005～2009年）亦受歡迎。

這段時期，JUMP三大漫（海賊王、火影忍者、BLEACH）的觀念不斷提昇。然而也不乏新漫畫成為JUMP的中軸，如《[出包王女](../Page/出包王女.md "wikilink")》（2006～）、《[SKET
DANCE](../Page/SKET_DANCE.md "wikilink")》（2007～2013年）、《[妖怪少爺](../Page/妖怪少爺.md "wikilink")》（2008～2012年）、《[美食獵人](../Page/美食獵人.md "wikilink")》（2008～2016年）、《[爆漫王。](../Page/爆漫王。.md "wikilink")》（2008～2012年）。及後，也有《美食獵人》成為JUMP新三大的說法，但很快就被除名了。

### 2010年代

而《[影子籃球員](../Page/影子籃球員.md "wikilink")》（2009年～2014年）、《[惡魔奶爸](../Page/惡魔奶爸.md "wikilink")》（2009年～2014年）、《[偽戀](../Page/偽戀.md "wikilink")》（2011年～2016年）、《[排球少年\!\!](../Page/排球少年!!.md "wikilink")》（2012年～）《[齊木楠雄的Ψ難](../Page/齊木楠雄的Ψ難.md "wikilink")》（2012年～2018年）、《[暗殺教室](../Page/暗殺教室.md "wikilink")》（2012年～2016年）、《[食戟之靈](../Page/食戟之靈.md "wikilink")》（2012年～）、《[境界觸發者](../Page/境界觸發者.md "wikilink")》（2013年～）、《[我的英雄學院](../Page/我的英雄學院.md "wikilink")》（2014年～）、《[黑色五葉草](../Page/黑色五葉草.md "wikilink")》（2015年～）被稱作強勢新生代的中軸。

JUMP多部知名大作（[暗殺教室](../Page/暗殺教室.md "wikilink")、[偽戀](../Page/偽戀.md "wikilink")、[BLEACH](../Page/BLEACH.md "wikilink")、[烏龍派出所](../Page/烏龍派出所.md "wikilink")、[美食獵人](../Page/美食獵人.md "wikilink")）都陸續的結束連載。

## 特征

雜誌的關鍵字是「友情」、「努力」和「勝利」，所有得到[連載機會的](../Page/連載.md "wikilink")[作品都必須宣揚其中一個訊息](../Page/作品.md "wikilink")。亦因此，連載漫畫基本上都不外乎[格鬥](../Page/格鬥.md "wikilink")、[動作](../Page/動作.md "wikilink")、[喜劇](../Page/喜劇.md "wikilink")、[體育等較容易彰顯熱血的題材](../Page/體育.md "wikilink")。不過近年為吸納多元化的動漫社群，週刊亦加入了[科幻冒险](../Page/科幻冒险.md "wikilink")、[戀愛](../Page/戀愛.md "wikilink")、[社會話題及](../Page/社會話題.md "wikilink")[紀錄形式的新興作品](../Page/紀錄.md "wikilink")。

封面基本上是當時得令的一部連載作品，偶然則會以全明星陣容登場。不過在1970年代後半到1980年代前半，與及1990年代後半的時期，《Jump》亦曾仿傚其他公司的週刊少年漫畫雜誌，刊載運動員和偶像的相片作招徠。雜誌本身亦不時會發放運動員的特集和採訪報道。

## 編輯政策

### 新人專屬合約

創刊時期，由於不能確保受歡迎的漫畫家參與製作，所以編輯部決定所有新人都必須簽訂專屬契約，確定他們全力應付《Jump》的連載。訂下這個契約代表不論有沒有新作品構思，都不能跟其他漫畫媒體合作；即使擁有全盤計劃，亦要待有關編輯找上相關的掛單。除了極少數的人氣漫畫家外，一旦放棄契約，就難以重返這個舞台。基於《Jump》的受眾面比起其他雜誌龐大許多，不少新人漫畫家都情願慢慢等待，務求一躍成名。

### 調查至上主義

一般漫畫雜誌只會把讀者意見調查作為參考，惟本刊却特別強調，並作為人氣指數的指標。規則以新連載登場後的十期為基準，綜合期間每一期的讀者意見，再以此決定作品命運。每一期的數據會反映在漫畫排版的前後位置，惟第一位會經常保留予台柱或經歷特殊意義時刻的作品（如第一集登場及周年紀念），新連載亦會有約一至兩個月的時間得到寬鬆對待。若人氣指數持續排名末端，不管作品質素如何都會被腰斬，讓路于新連載，造成了許多只有10至20期的短期連載作品。不屬於以上兩類的作品，亦有中央彩頁的準則表示編輯部的支持，此舉一般會使作品處於比實際人氣稍高的位置。

一般只有中下游的作品可以主動向編輯部表示結束的意願，亦可能會加注條件，如必須在有限時間內推出超越或匹敵現時作品的連載。這些條件下結束可謂充滿風險，未能履行的最壞結果可能是永遠脫離《Jump》行列。

目前，編輯部旨在讓連載作品數量維持於20部。綜合排名最後兩位的作品會在考慮腰斬的行列，十數位的作品則可歸類為中下游。而進入2000年代之後，被腰斬的作品會由姊妹雜誌《赤丸Jump》或《Jump
NEXT》連載數集完結篇，緩和爛尾的問題，除此之外還有許多作者就年齡層和意願移藉到其他少年Jump雜誌的例子。

## 歷代總編輯長

1.  長野規（1968年～1974年）
2.  中野祐介（1974年～1978年）
3.  西村繁男（1978年～1986年）
4.  後藤广喜（1986年～1993年）
5.  堀江信彦（1993年～1996年）
6.  鳥嶋和彦（1996年～2001年第31期）
7.  高橋俊昌（2001年第32期～2003年第10期），2003年1月在任職期間辭世，由鳥嶋和彥暫代（2003年第11期）
8.  茨木政彥（2003年第12期～2008年第17期）
9.  佐佐木尚（2008年第18期～2011年第29期）
10. 瓶子吉久（2011年第30期～2017年第30期）
11. 中野博之（2017年第31期～），現任總編

## 連載中作品

  -

<table>
<thead>
<tr class="header">
<th><p>作品名稱（中文）</p></th>
<th><p>作品名稱（日文）</p></th>
<th><p>作者（作畫）</p></th>
<th><p>原作</p></th>
<th><p>起載號</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/ONE_PIECE.md" title="wikilink">-{zh-tw:航海王;zh-hk:ONE PIECE;zh-cn:海贼王}-</a></p></td>
<td></td>
<td><p><a href="../Page/尾田榮一郎.md" title="wikilink">尾田榮一郎</a></p></td>
<td><p>－</p></td>
<td><p>1997年34號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HUNTER×HUNTER.md" title="wikilink">HUNTER×HUNTER</a></p></td>
<td></td>
<td><p><a href="../Page/冨樫義博.md" title="wikilink">冨樫義博</a></p></td>
<td><p>－</p></td>
<td><p>1998年14號</p></td>
<td><p>自2006年11號後不定期連載</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/排球少年！！.md" title="wikilink">排球少年！！</a></p></td>
<td><p>{{lang|ja|ハイキュー</p></td>
<td><p>}}</p></td>
<td><p><a href="../Page/古舘春一.md" title="wikilink">古舘春一</a></p></td>
<td><p>－</p></td>
<td><p>2012年12號</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/食戟之靈.md" title="wikilink">食戟之靈</a></p></td>
<td></td>
<td><p><a href="../Page/佐伯俊.md" title="wikilink">佐伯俊</a> </p></td>
<td><p><a href="../Page/附田祐斗.md" title="wikilink">附田祐斗</a><br />
</p></td>
<td><p>2012年52號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/火之丸相撲.md" title="wikilink">火之丸相撲</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2014年26號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我的英雄學院.md" title="wikilink">我的英雄學院</a></p></td>
<td></td>
<td><p><a href="../Page/堀越耕平.md" title="wikilink">堀越耕平</a></p></td>
<td><p>－</p></td>
<td><p>2014年32號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黑色五葉草.md" title="wikilink">-{zh-hant:黑色五葉草;zh-hans:黑色四叶草}-</a></p></td>
<td></td>
<td><p><a href="../Page/田畠裕基.md" title="wikilink">田畠裕基</a></p></td>
<td><p>－</p></td>
<td><p>2015年12號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/搖曳莊的幽奈小姐.md" title="wikilink">搖曳莊的幽奈小姐</a></p></td>
<td></td>
<td><p><a href="../Page/三浦忠弘.md" title="wikilink">三浦忠弘</a></p></td>
<td><p>－</p></td>
<td><p>2016年10號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鬼滅之刃.md" title="wikilink">鬼滅之刃</a></p></td>
<td></td>
<td><p><a href="../Page/吾峠呼世晴.md" title="wikilink">吾峠呼世晴</a></p></td>
<td><p>－</p></td>
<td><p>2016年11號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BORUTO-火影新世代-NARUTO_NEXT_GENERATIONS-.md" title="wikilink">-{zh-hant:BORUTO-火影新世代-<br />
NARUTO NEXT GENERATIONS-;zh-hans:博人传-火影次世代}-</a></p></td>
<td></td>
<td><p><a href="../Page/池本幹雄.md" title="wikilink">池本幹雄</a></p></td>
<td><p><a href="../Page/岸本齊史.md" title="wikilink">岸本齊史</a><br />
</p></td>
<td><p>2016年23號</p></td>
<td><p>每月一話連載</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/約定的夢幻島.md" title="wikilink">約定的夢幻島</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>2016年35號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我們真的學不來！.md" title="wikilink">我們真的學不來！</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2017年10號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Dr.STONE_新石紀.md" title="wikilink">Dr.STONE 新石紀</a></p></td>
<td></td>
<td><p><a href="../Page/Boichi.md" title="wikilink">Boichi</a></p></td>
<td><p><a href="../Page/稻垣理一郎.md" title="wikilink">稻垣理一郎</a></p></td>
<td><p>2017年14號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/演員夜凪景_act-age.md" title="wikilink">演員夜凪景 act-age</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>2018年8號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/咒術迴戰.md" title="wikilink">咒術迴戰</a></p></td>
<td></td>
<td><p>芥見下下</p></td>
<td><p>－</p></td>
<td><p>2018年14號</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2018年42號</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>林聖二</p></td>
<td><p>－</p></td>
<td><p>2018年42號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/電鋸人.md" title="wikilink">電鋸人</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2019年01號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ne0;lation.md" title="wikilink">ne0;lation</a></p></td>
<td></td>
<td><p>依田瑞稀</p></td>
<td><p>平尾友秀</p></td>
<td><p>2019年02號</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>帆上夏希</p></td>
<td><p>－</p></td>
<td><p>2019年03號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/最後的西遊記.md" title="wikilink">最後的西遊記</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2019年14號</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/椎橋寬.md" title="wikilink">椎橋寬</a></p></td>
<td><p>－</p></td>
<td><p>2019年15號</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 增刊號與衍生雜誌

  - 衍生雜誌

<!-- end list -->

  - 別冊少年Jump→[月刊少年Jump](../Page/月刊少年Jump.md "wikilink")→[Jump
    Square](../Page/Jump_Square.md "wikilink")
  - [Fresh Jump](../Page/Fresh_Jump.md "wikilink")
  - [V Jump](../Page/V_Jump.md "wikilink")
  - [Super Jump](../Page/Super_Jump.md "wikilink")→Grand Jump
      - [Oh super jump](../Page/Oh_super_jump.md "wikilink")
  - [週刊YOUNG JUMP](../Page/週刊YOUNG_JUMP.md "wikilink")
      - [Business Jump](../Page/Business_Jump.md "wikilink")→[Grand
        Jump](../Page/Grand_Jump.md "wikilink")・[Grand Jump
        PREMIUM](../Page/Grand_Jump_PREMIUM.md "wikilink")
      - [Ultra Jump](../Page/Ultra_Jump.md "wikilink")
  - [最強Jump](../Page/最強Jump.md "wikilink")
  - [少年Jump +](../Page/週刊少年Jump的增刊號.md "wikilink")

## 外部链接

  - [官方网站](http://www.shonenjump.com/j/index.html)
  - [Jump（ジャンプ）](https://www.youtube.com/user/shonenjumpofficial)

[\*](../Category/週刊少年Jump.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")