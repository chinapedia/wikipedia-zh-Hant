《**週刊少年Sunday**》（日語：）是[日本](../Page/日本.md "wikilink")[小学馆发行的综合性少年](../Page/小学馆.md "wikilink")[漫画杂志](../Page/漫画杂志.md "wikilink")。該雜誌创刊於1959年，取名為Sunday只是為使讀者感受週日般快樂輕鬆的氣氛，雜誌本身是於每周三发行。拥有包括[安达充](../Page/安达充.md "wikilink")、[高桥留美子](../Page/高桥留美子.md "wikilink")、[青山刚昌在内的一批著名](../Page/青山刚昌.md "wikilink")[漫画家](../Page/漫画家.md "wikilink")。刊载作品从画技到情节都很优秀，题材有运动、科幻、魔幻、侦破、历史、恋爱等，呈现多样化格局。相較於強調“努力”“友情”“勝利”的《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》，该刊作品相对欠缺明显的整体风格，比較傾向於由作家自行發揮。比起《週刊少年JUMP》以[市調至上](../Page/市調.md "wikilink")，經常在市調不佳時中止漫畫，該刊作品也多由作家畫至故事告一段落時才結束（也因此有故事冗長之批評）。发行量较《週刊少年Jump》和《[週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")》為低。

台灣是由[青文出版社取得大部分漫畫的版權](../Page/青文出版社.md "wikilink")，曾在《[元氣少年](../Page/元氣少年.md "wikilink")》上進行連載，但該雜誌已於2009年5月5日停刊。

## 編輯部作風

  - 2008年，《[魔法少年賈修](../Page/魔法少年賈修.md "wikilink")》作者[雷句誠和小學館因為遺失原稿的事件而鬧上法院](../Page/雷句誠.md "wikilink")；[新条真由也在部落格上反映小學館和周刊少年Sunday编辑部的问题](../Page/新条真由.md "wikilink")，指出編輯部有著不重視原稿以及對創作者缺乏基本尊重等弊病\[1\]\[2\]。
  - 2013年，《[革神語](../Page/革神語.md "wikilink")》推出大幅度修改後的重製版，作者[渡瀨悠宇表示是因為當初與責編I的理念不合受於干涉](../Page/渡瀨悠宇.md "wikilink")，所以連載時的開端部分不能令她自己感到滿意\[3\]。
  - 2017年，[青山剛昌在小学馆出版的](../Page/青山剛昌.md "wikilink")《青山刚昌
    30周年书》中披露，《[名侦探柯南](../Page/名侦探柯南.md "wikilink")》连载初期，他曾经因为每周构思案件造成的劳累和编辑部之上某些人的干涉，想要结束连载，直到得知作品将被改编成电影时才改变想法\[4\]\[5\]\[6\]\[7\]。

## 連載中的作品

  -

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 17%" />
<col style="width: 17%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>中译标题</p></th>
<th><p>原文标题</p></th>
<th><p>作者</p></th>
<th><p>開始期号</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/魔術快斗.md" title="wikilink">魔術快斗</a></p></td>
<td></td>
<td><p><a href="../Page/青山刚昌.md" title="wikilink">青山刚昌</a></p></td>
<td><p>data-sort-value="1987.26" | 1987年26号</p></td>
<td><p>不定期連載</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/名偵探柯南.md" title="wikilink">名偵探柯南</a></p></td>
<td></td>
<td><p><a href="../Page/青山刚昌.md" title="wikilink">青山刚昌</a></p></td>
<td><p>data-sort-value="1994.05" | 1994年5号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楚楚可憐超能少女組.md" title="wikilink">-{zh-tw:楚楚可憐超能少女組;zh-hk:絕對可愛CHILDREN;zh-cn:绝对可怜CHILDREN}-</a></p></td>
<td></td>
<td><p><a href="../Page/椎名高志.md" title="wikilink">椎名高志</a></p></td>
<td><p>data-sort-value="2005.33" | 2005年33号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/革神語.md" title="wikilink">革神語</a></p></td>
<td></td>
<td><p><a href="../Page/渡瀨悠宇.md" title="wikilink">渡瀨悠宇</a></p></td>
<td><p>data-sort-value="2008.44" | 2008年44号</p></td>
<td><p>不定期連載</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/田中基之.md" title="wikilink">田中基之</a></p></td>
<td><p>data-sort-value="2011.09" | 2011年9号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/銀之匙_Silver_Spoon.md" title="wikilink">銀之匙 Silver Spoon</a></p></td>
<td></td>
<td><p><a href="../Page/荒川弘.md" title="wikilink">荒川弘</a></p></td>
<td><p>data-sort-value="2011.19" | 2011年19号</p></td>
<td><p>不定期連載</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/田边伊卫郎.md" title="wikilink">田边伊卫郎</a></p></td>
<td><p>data-sort-value="2013.33" | 2013年33号</p></td>
<td><p>每月一话连载</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/汤神君没有朋友.md" title="wikilink">汤神君没有朋友</a></p></td>
<td></td>
<td></td>
<td><p>data-sort-value="2013.48" | 2013年48号</p></td>
<td><p>从《》转籍<br />
每月一话连载</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/棒球大联盟.md" title="wikilink">棒球大联盟 2nd</a></p></td>
<td></td>
<td><p><a href="../Page/满田拓也.md" title="wikilink">满田拓也</a></p></td>
<td><p>data-sort-value="2015.15" | 2015年15号</p></td>
<td><p>《<a href="../Page/棒球大联盟.md" title="wikilink">棒球大联盟</a>》续作<br />
休载中</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/畑健二郎.md" title="wikilink">畑健二郎</a></p></td>
<td><p>data-sort-value="2015.40" | 2015年40号</p></td>
<td><p>每月一话连载<br />
休载中</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>data-sort-value="2016.03" | 2016年3号</p></td>
<td><p>从《》转籍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/破坏双亡亭.md" title="wikilink">破坏双亡亭</a></p></td>
<td></td>
<td><p><a href="../Page/藤田和日郎.md" title="wikilink">藤田和日郎</a></p></td>
<td><p>data-sort-value="2016.17" | 2016年17号</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>data-sort-value="2016.22" | 2016年22・23合并号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/在魔王城说晚安.md" title="wikilink">在魔王城说晚安</a></p></td>
<td></td>
<td><p><a href="../Page/熊之股鍵次.md" title="wikilink">熊之股鍵次</a></p></td>
<td><p>data-sort-value="2016.24" | 2016年24号</p></td>
<td><p>与同出版社的网络漫画网站《》共同连载</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/古见同学有交流障碍症.md" title="wikilink">古见同学有交流障碍症</a></p></td>
<td></td>
<td><p><a href="../Page/小田智仁.md" title="wikilink">小田智仁</a></p></td>
<td><p>data-sort-value="2016.25" | 2016年25号</p></td>
<td><p>曾以短篇形式刊载于2015年42号</p></td>
</tr>
<tr class="even">
<td><p>RYOKO</p></td>
<td></td>
<td><p>data-sort-value="2016.47" | 2016年47号</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>data-sort-value="2017.05" | 2017年5・6合并号</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>data-sort-value="2017.20" | 2017年20号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第九波涛</p></td>
<td></td>
<td></td>
<td><p>data-sort-value="2017.21" | 2017年21号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>妖怪戏画</p></td>
<td></td>
<td></td>
<td><p>data-sort-value="2017.22" | 2017年22・23合并号</p></td>
<td><p>刊登顺序基本固定为倒数第二</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/七月镜一.md" title="wikilink">七月镜一</a>（原作）<br />
杉山铁平（作画）</p></td>
<td><p>data-sort-value="2018.01" | 2018年1号</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>data-sort-value="2018.01" | 2018年1号</p></td>
<td><p>刊登顺序固定为最后</p></td>
</tr>
<tr class="odd">
<td><p>苍穹的阿里阿德涅</p></td>
<td></td>
<td><p><a href="../Page/八木教广.md" title="wikilink">八木教广</a></p></td>
<td><p>data-sort-value="2018.02" | 2018年2号</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/畑健二郎.md" title="wikilink">畑健二郎</a></p></td>
<td><p>data-sort-value="2018.12" | 2018年12号</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/松江名俊.md" title="wikilink">松江名俊</a></p></td>
<td><p>data-sort-value="2018.13" | 2018年13号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>篮球梦</p></td>
<td><p>switch</p></td>
<td><p>波切敦</p></td>
<td><p>data-sort-value="2018.20" | 2018年20号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5分後的世界</p></td>
<td></td>
<td></td>
<td><p>data-sort-value="2018.22" | 2018年22・23合并号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/名侦探柯南.md" title="wikilink">名侦探柯南 零的日常</a></p></td>
<td></td>
<td><p>（作者）<br />
<a href="../Page/青山刚昌.md" title="wikilink">青山刚昌</a>（原案协作）</p></td>
<td><p>data-sort-value="2018.24" | 2018年24号</p></td>
<td><p>《名侦探柯南》外传<br />
原为正常连载，自2018年33号开始仅在正篇休载时刊载。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/河本焰.md" title="wikilink">河本焰</a>（原作）<br />
武野光（原作）<br />
（作画）</p></td>
<td><p>data-sort-value="2018.49" | 2018年49号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>妹搜记录</p></td>
<td></td>
<td><p>西村啓</p></td>
<td><p>data-sort-value="2019.02" | 2019年2・3合并号</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>FIRE RABBIT</p></td>
<td></td>
<td></td>
<td><p>data-sort-value="2019.04" | 2019年4・5合并号</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/福井苇火.md" title="wikilink">福井苇火</a></p></td>
<td><p>data-sort-value="2019.06" | 2019年6号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>水之女神今天也恋爱了吗？</p></td>
<td></td>
<td><p>三簾真也</p></td>
<td><p>data-sort-value="2019.07" | 2019年7号</p></td>
<td><p>&lt;!--|-</p></td>
</tr>
</tbody>
</table>

## 連載過的作品

  - [結界師](../Page/結界師.md "wikilink")（[田边伊衛郎](../Page/田边伊衛郎.md "wikilink")）
  - [DEFESE
    DEVIL惡魔辯護士](../Page/DEFESE_DEVIL惡魔辯護士.md "wikilink")（原作：[尹仁完](../Page/尹仁完.md "wikilink")、漫畫：[梁慶一](../Page/梁慶一.md "wikilink")）
  - [烘焙王](../Page/烘焙王.md "wikilink")——[橋口隆志](../Page/橋口隆志.md "wikilink")
  - [美鳥日記](../Page/美鳥日記.md "wikilink")——[井上和郎](../Page/井上和郎.md "wikilink")
  - [城市風雲兒](../Page/城市風雲兒.md "wikilink")——[青山剛昌](../Page/青山剛昌.md "wikilink")
  - [福星小子](../Page/福星小子.md "wikilink")（うる星やつら）——[高桥留美子](../Page/高桥留美子.md "wikilink")
  - [乱马1/2](../Page/乱马1/2.md "wikilink")——[高桥留美子](../Page/高桥留美子.md "wikilink")
  - [犬夜叉](../Page/犬夜叉.md "wikilink")——[高桥留美子](../Page/高桥留美子.md "wikilink")
  - [GS美神極樂大作戰](../Page/GS美神極樂大作戰.md "wikilink")——[椎名高志](../Page/椎名高志.md "wikilink")
  - [機動警察](../Page/機動警察.md "wikilink")——[結城正美](../Page/結城正美.md "wikilink")
  - [魔法少年賈修](../Page/魔法少年賈修.md "wikilink")——[雷句誠](../Page/雷句誠.md "wikilink")
  - [潮与虎](../Page/潮与虎.md "wikilink")（港译：《魔力小马》）——[藤田和日郎](../Page/藤田和日郎.md "wikilink")
  - [魔偶马戏团](../Page/魔偶马戏团.md "wikilink")——[藤田和日郎](../Page/藤田和日郎.md "wikilink")
  - [四葉遊戲](../Page/四葉遊戲.md "wikilink")──[安達充](../Page/安達充.md "wikilink")
  - [棒球英豪](../Page/棒球英豪.md "wikilink")——[安达充](../Page/安达充.md "wikilink")
  - [H2](../Page/H2.md "wikilink")——[安达充](../Page/安达充.md "wikilink")
  - [ROUGH](../Page/我愛芳鄰_\(漫畫\).md "wikilink")——[安达充](../Page/安达充.md "wikilink")
  - [多羅羅](../Page/多羅羅.md "wikilink")——[手塚治虫](../Page/手塚治虫.md "wikilink")
  - [植木的法則](../Page/植木的法則.md "wikilink")——[福地翼](../Page/福地翼.md "wikilink")
  - [植木的法則
    PLUS](../Page/植木的法則_PLUS.md "wikilink")——[福地翼](../Page/福地翼.md "wikilink")
  - [MAR魔兵傳奇](../Page/MAR魔兵傳奇.md "wikilink")－－[安西信行](../Page/安西信行.md "wikilink")
  - [三星法廚](../Page/三星法廚.md "wikilink")——[谷古宇剛](../Page/谷古宇剛.md "wikilink")
  - [怪体真書Ø](../Page/怪体真書Ø.md "wikilink")（[険持ちよ](../Page/険持ちよ.md "wikilink")）
  - [棒球大聯盟](../Page/棒球大聯盟.md "wikilink")（[滿田拓也](../Page/滿田拓也.md "wikilink")）
  - [只有神知道的世界](../Page/只有神知道的世界.md "wikilink")（[若木民喜](../Page/若木民喜.md "wikilink")）
  - [史上最強弟子兼一](../Page/史上最強弟子兼一.md "wikilink")（[松江名俊](../Page/松江名俊.md "wikilink")）
  - [-{zh-tw:今際之國的闖關者;zh-hk:今際之國的有栖;zh-hans:弥留之国的爱丽丝}-](../Page/今际之国的闯关者.md "wikilink")
    （）

## 参见

  - [龙漫少年星期天](../Page/龙漫少年星期天.md "wikilink")

## 参考来源

## 外部链接

  - [官方网站Web Sunday](http://websunday.net/)

  -
  - [Sunday Webry](https://www.sunday-webry.com/)

[\*](../Category/週刊少年Sunday.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:週刊漫畫雜誌](../Category/週刊漫畫雜誌.md "wikilink")
[Category:小學館的漫畫雜誌](../Category/小學館的漫畫雜誌.md "wikilink")

1.
2.
3.
4.
5.
6.
7.