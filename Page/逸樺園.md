[The_Orchards.JPG](https://zh.wikipedia.org/wiki/File:The_Orchards.JPG "fig:The_Orchards.JPG")
 **逸樺園**（**The
Orchards**）是[香港一幢私人屋苑](../Page/香港.md "wikilink")，位於[港島](../Page/港島.md "wikilink")[東區](../Page/東區_\(香港\).md "wikilink")[鰂魚涌](../Page/鰂魚涌.md "wikilink")，由[太古地產所建](../Page/太古地產.md "wikilink")，於2003年7月落成。共有442個單位，面積介乎790平方呎至1,147平方呎，亦提供四個面積1,508平方呎至1,737平方呎的頂層Penthouse單位。

休閑娛樂設施包括一個會所、分別位於兩幢住宅大廈17樓及32樓的日式和英式空中花園，以及一個平台花園。

## 交通設施

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [英皇道](../Page/英皇道.md "wikilink")

<!-- end list -->

  - [香港電車](../Page/香港電車.md "wikilink")

<!-- end list -->

  - [船塢里站](../Page/船塢里.md "wikilink")

<!-- end list -->

  - [基利路](../Page/基利路.md "wikilink")

<!-- end list -->

  - [祐民街](../Page/祐民街.md "wikilink")

</div>

</div>

## 娛樂設施及商場

  - [MCL康怡](../Page/MCL康怡.md "wikilink")
  - [UA太古城戲院](../Page/UA太古城戲院.md "wikilink")
  - [太古城中心](../Page/太古城中心.md "wikilink")
  - [康怡廣場](../Page/康怡廣場.md "wikilink")
  - [吉之島](../Page/吉之島.md "wikilink")
  - [格蘭會](../Page/格蘭會.md "wikilink")

## 附近屋苑

  - [太古城](../Page/太古城.md "wikilink")
  - [康怡花園](../Page/康怡花園.md "wikilink")
  - [康山花園](../Page/康山花園.md "wikilink")
  - [南豐新邨](../Page/南豐新邨.md "wikilink")
  - [康蕙花園](../Page/康蕙花園.md "wikilink")

[Category:鰂魚涌](../Category/鰂魚涌.md "wikilink")
[Category:東區私人屋苑](../Category/東區私人屋苑.md "wikilink")
[Category:太古地產物業](../Category/太古地產物業.md "wikilink")