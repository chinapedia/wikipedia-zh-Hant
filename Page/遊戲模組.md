**遊戲模組**，英文多簡稱為「MOD」、「Mod」（-{zh-hans:全称;zh-hant:全寫;}-「Modification」），「修改」的[名詞含義](../Page/名詞.md "wikilink")。MOD通常對應可以修改的[電子遊戲](../Page/電子遊戲.md "wikilink")，因此以電腦遊戲為主。必須依賴與原作品方可執行遊玩。遊戲中的道具、武器、角色、敵人、事物、模式、故事情節等任意部分都可能屬於修改範疇，多見於著名電子遊戲作品，遊戲類型多數為[第一人稱射擊遊戲](../Page/第一人稱射擊遊戲.md "wikilink")、[即時戰略遊戲](../Page/即時戰略遊戲.md "wikilink")、[角色扮演游戏](../Page/電子角色扮演遊戲.md "wikilink")（非日式）、[動作遊戲](../Page/動作遊戲.md "wikilink")，著名實例包括[红色警戒2](../Page/红色警戒2.md "wikilink")、[毀滅戰士系列](../Page/毀滅戰士.md "wikilink")、[橫掃千軍](../Page/橫掃千軍.md "wikilink")、[雷神之鎚系列](../Page/雷神之鎚系列.md "wikilink")、[戰慄時空系列](../Page/戰慄時空系列.md "wikilink")、[Minecraft](../Page/Minecraft.md "wikilink")、[魔域幻境系列](../Page/魔域幻境.md "wikilink")、[无冬之夜系列](../Page/无冬之夜.md "wikilink")、[地牢围攻系列](../Page/地牢围攻.md "wikilink")、[上古卷軸5和](../Page/上古卷軸5.md "wikilink")[俠盜獵車手系列等](../Page/俠盜獵車手系列.md "wikilink")。

## 地域差異

在亚洲，電子遊戲至關重要的因素常常為其劇情，修改並無實際意義，玩家動手能力較差。遊戲開發廠商經營理念与欧美不同，往往極力遏制這種行為；同時玩家樂於因近似形式或者常見形式改造遊戲，以致[同人遊戲的盛行](../Page/同人遊戲.md "wikilink")。

## 形式

### 官方工具

力主MOD的遊戲開發者會提供[SDK或其他工具](../Page/SDK.md "wikilink")（如[地圖編輯器](../Page/地圖編輯器.md "wikilink")），允许并鼓励玩家自行創造，包括遊戲中的模型。

从[雷神之锤开始](../Page/雷神之锤系列.md "wikilink")，经常有着名游戏的MOD以完全不同於原作形式出现，SDK通常足以大规模修改，有些开发者贩卖[游戏引擎](../Page/游戏引擎.md "wikilink")（甚至[源代码](../Page/源代码.md "wikilink")）更加扩展。

[地圖編輯器則是較為簡便的形式](../Page/地圖編輯器.md "wikilink")，通常由遊戲開發者製作，[星際爭霸系列](../Page/星際爭霸系列.md "wikilink")、[絕冬城之夜系列和](../Page/絕冬城之夜.md "wikilink")[魔兽争霸III是這方面著名的例子](../Page/魔兽争霸III.md "wikilink")。

[Project_Revolution.jpg](https://zh.wikipedia.org/wiki/File:Project_Revolution.jpg "fig:Project_Revolution.jpg")

### 簡單修改

有些游戏配置文件公开利於修改，例如[侠盗猎车手III之後的GTA系列游戏](../Page/侠盗猎车手III.md "wikilink")。配置文件包括例如驾驶时的车辆操控、车辆颜色、武器属性、天气属性、[NPC属性等](../Page/NPC.md "wikilink")，都是以[纯文本存放的](../Page/纯文本.md "wikilink")，并保留开发者的参数[注释](../Page/注释.md "wikilink")。[车辆的](../Page/车辆.md "wikilink")[模型和](../Page/模型.md "wikilink")[贴图](../Page/贴图.md "wikilink")，人物的模型和贴图，地图的模型和贴图等都是打包在一个IMG或RPF文件内，且打包文件的格式和模型文件和贴图文件的格式通常在游戏发售后不到半年就被破解，并由玩家制作出各式各样的模組。

有些則為暴力破解，如[終極動員令系列](../Page/終極動員令系列.md "wikilink")。

## 發展道路

### 發行

不少MOD得以發行，例如《[戰慄時空](../Page/戰慄時空.md "wikilink")》的模組《[絕對武力](../Page/絕對武力.md "wikilink")》与《[決勝之日](../Page/決勝之日.md "wikilink")》等被玩家津津乐道的作品不久就獨立與原著發行，最知名的是《[戰慄時空](../Page/戰慄時空.md "wikilink")》的模組《[反恐精英](../Page/反恐精英.md "wikilink")》，後者名氣远远超越了原作。更早的例子，是[絕對武力前身](../Page/絕對武力.md "wikilink")，[雷神之鎚II的模組Action](../Page/雷神之鎚II.md "wikilink")
Quake
2（AQ2）作為[官方資料片的一部分發布](../Page/資料片.md "wikilink")，每名開發者獲得大約$5000的收益。

### 獨立

遊戲開發者最終將[遊戲引擎](../Page/遊戲引擎.md "wikilink")[原始碼公布](../Page/原始碼.md "wikilink")，如[自由空间2](../Page/FreeSpace2.md "wikilink")、[id
Software的绝大多数游戏引擎](../Page/id_Software.md "wikilink")，於是玩家可以把MOD改造成完全獨立的遊戲。

比如基于[Darkplaces](../Page/Darkplaces.md "wikilink")（基於[id Tech
1開發](../Page/id_Tech_1.md "wikilink")）的[Nexuiz](../Page/Nexuiz.md "wikilink")；[ioquake3引擎的](../Page/ioquake3.md "wikilink")[Urban
Terror](../Page/Urban_Terror.md "wikilink")、[Tremulous](../Page/Tremulous.md "wikilink")；基于[FreeSpace2源代码计划的](../Page/FreeSpace2源代码计划.md "wikilink")[The
Babylon
Project（巴比伦计划）和](../Page/The_Babylon_Project.md "wikilink")[Beyond
the Red Line](../Page/Beyond_the_Red_Line.md "wikilink")。

[電子藝界允許開發者使用旗下](../Page/電子藝界.md "wikilink")[SAGE引擎（Strategy Action Game
Engine）開發免費的](../Page/SAGE引擎.md "wikilink")[第一人稱射擊遊戲輔佐旗下的](../Page/第一人稱射擊遊戲.md "wikilink")[終極動員令系列](../Page/終極動員令系列.md "wikilink")，這些遊戲都具有相同背景，如對應[紅色警戒的](../Page/紅色警戒.md "wikilink")[Red
Alert: A Path
Beyond](https://web.archive.org/web/20100127101707/http://www.apathbeyond.com/)、對應[終極動員令：將軍的](../Page/終極動員令：將軍.md "wikilink")[Scud
Storm](http://www.battlefordune.co.uk/forums/)。

## 示例

販賣遊戲引擎促生了大量[獨立遊戲開發者](../Page/獨立遊戲.md "wikilink")，例如：

### 成功案例

1.  《[反恐精英](../Page/反恐精英.md "wikilink")》於1999年作為[Valve所開發的遊戲](../Page/維爾福.md "wikilink")《[戰慄時空](../Page/戰慄時空.md "wikilink")》遊戲模组推出。由於深受玩家的喜愛，2000年由[Valve購得版權發行為獨立遊戲](../Page/維爾福.md "wikilink")，並且聘用原開發者[Minh
    Le與](../Page/Minh_Le.md "wikilink")[Jess
    Cliffe繼續參與遊戲的後續開發](../Page/Jess_Cliffe.md "wikilink")\[1\]。
2.  2012年年初，[武装突袭2的模組](../Page/武装突袭2.md "wikilink")發佈后，造成的巨量的銷售量，僅僅數月便銷售了30萬套。現被武裝突擊的製作商[波希米亚互动工作室](../Page/波希米亚互动工作室.md "wikilink")（Bohemia
    Interactive）購買版權并發行了。\[2\]

### 失败案例

1.  2015年4月23日，[Steam开发商](../Page/Steam.md "wikilink")[Valve宣布模组付费功能](../Page/Valve.md "wikilink")，并在《[上古卷轴5：天际](../Page/上古卷轴5：天际.md "wikilink")》实行。次日，收费模组《Art
    of Catch》因未经许可使用了另一作者fore的免费模组《Fores New Idles in
    Skyrim》的代码引发纷争，而Valve的说法称收费模组可以使用免费模组的代码。此后收费系统更是乱象丛生，出现众多抄袭他人免费模组并上架收费的案例，很多作者因此下架自己的免费模组。4月27日，《上古卷轴》开发商[Bethesda解释为何实施模组收费模式](../Page/Bethesda.md "wikilink")，以及模组收入分配问题，相关分配方式和表达引发众多玩家和作者不满。4月28日，Valve叫停模组付费功能，并表示“对此事，我们完全不知道自己在做什么”\[3\]\[4\]\[5\]。

## 其他

  - [Mod DB](../Page/Mod_DB.md "wikilink")
  - [獨立遊戲](../Page/獨立遊戲.md "wikilink")
  - [開源遊戲](../Page/開源遊戲.md "wikilink")

## 參考文獻

[Category:電子遊戲研發](../Category/電子遊戲研發.md "wikilink")
[Category:電子遊戲術語](../Category/電子遊戲術語.md "wikilink")
[電子遊戲模組](../Category/電子遊戲模組.md "wikilink")

1.
2.
3.
4.
5.