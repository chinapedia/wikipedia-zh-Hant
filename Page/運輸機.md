[US_Air_Force_C-17_Globemaster_III_formation.jpg](https://zh.wikipedia.org/wiki/File:US_Air_Force_C-17_Globemaster_III_formation.jpg "fig:US_Air_Force_C-17_Globemaster_III_formation.jpg")环球霸王III机群在飞越弗吉尼亚州的蓝岭。\]\]
[Antonov.an26.fairford.arp.jpg](https://zh.wikipedia.org/wiki/File:Antonov.an26.fairford.arp.jpg "fig:Antonov.an26.fairford.arp.jpg")的An-26軍用運輸機\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:PLAAF_Ilyushin_Il-76_landing_at_Perth_Airport_-2.jpg "fig:缩略图")失蹤事件調查。\]\]
**運輸機（Military transport
aircraft）**狭义上指用於空運兵員、[武器裝備](../Page/武器.md "wikilink")、並能空投[傘兵和軍用裝備的](../Page/傘兵.md "wikilink")[軍用飛機](../Page/軍用飛機.md "wikilink")，它具有在複雜氣候條件下飛行和比較簡易的[機場上起降的能力](../Page/機場.md "wikilink")，有的還裝有自衛的武器及[電子干擾設備](../Page/電子干擾.md "wikilink")。軍用運輸機使用性質分為戰術和戰略兩種，按航程分為中程及遠程，按載重可以分為中型和重型。

民用运输机大多称为「[货机](../Page/货机.md "wikilink")」。

## 二戰中的運輸機

###

  - [C-47](../Page/C-47.md "wikilink")

###

  - [Ju 52](../Page/Ju_52.md "wikilink")
  - [Me 323](../Page/Me_323運輸機.md "wikilink")

## 現代運輸機

###

  - [C-2A](../Page/C-2灰狗式運輸機.md "wikilink")
  - [C-5](../Page/C-5運輸機.md "wikilink")
  - [C-17](../Page/C-17运输机.md "wikilink")
  - [C-119](../Page/C-119運輸機.md "wikilink")
  - [C-123](../Page/C-123運輸機.md "wikilink")
  - [C-130](../Page/C-130運輸機.md "wikilink")
  - [C-131](../Page/:en:C-131_Samaritan.md "wikilink")
  - [C-141](../Page/:en:C-141_Starlifter.md "wikilink")

### 及

  - [An-12](../Page/安-12.md "wikilink")
  - [An-14](../Page/:en:Antonov_An-14.md "wikilink")
  - [An-22](../Page/:en:Antonov_An-22.md "wikilink")
  - [An-30](../Page/安-30.md "wikilink")
  - [An-32](../Page/:en:Antonov_An-32.md "wikilink")
  - [An-38](../Page/安-38.md "wikilink")
  - [An-70](../Page/安托諾夫An-70.md "wikilink")
  - [An-72](../Page/安托諾夫An-72.md "wikilink")
  - [An-124](../Page/An-124運輸機.md "wikilink")
  - [An-225](../Page/An-225運輸機.md "wikilink")
  - [伊爾76](../Page/伊爾76.md "wikilink")

###

  - [運-7](../Page/運-7.md "wikilink")
  - [运-8](../Page/运-8.md "wikilink")
  - [运-9](../Page/运-9.md "wikilink")
  - [運-10](../Page/運-10.md "wikilink")
  - [運-11](../Page/運-11.md "wikilink")
  - [运-12](../Page/运-12.md "wikilink")
  - [运-20](../Page/运-20.md "wikilink")

###

  - [C-1](../Page/C-1運輸機.md "wikilink")
  - [XC-2](../Page/XC-2.md "wikilink")

###

  - [空客A330 MRTT](../Page/:en:Airbus_A330_MRTT.md "wikilink")
  - [A400M](../Page/A400M.md "wikilink")
  - [G.222](../Page/G.222運輸機.md "wikilink")

###

  - [KC-390](../Page/KC-390.md "wikilink")

## 參考文獻

{{-}}

[军用运输机](../Category/军用运输机.md "wikilink")