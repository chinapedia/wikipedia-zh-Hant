[To_Kwa_Wan_Motor_Vehicle_Inspection_Centre_2011.jpg](https://zh.wikipedia.org/wiki/File:To_Kwa_Wan_Motor_Vehicle_Inspection_Centre_2011.jpg "fig:To_Kwa_Wan_Motor_Vehicle_Inspection_Centre_2011.jpg")
[Transport_Department_Hong_Kong_Licensing_Office_201802.jpg](https://zh.wikipedia.org/wiki/File:Transport_Department_Hong_Kong_Licensing_Office_201802.jpg "fig:Transport_Department_Hong_Kong_Licensing_Office_201802.jpg")3樓運輸署香港牌照事務處\]\]

**運輸署**（[英文](../Page/英文.md "wikilink")：**Transport
Department**，[縮寫](../Page/縮寫.md "wikilink")：**TD**）是[香港特別行政區政府](../Page/香港特別行政區政府.md "wikilink")[運輸及房屋局轄下的](../Page/運輸及房屋局.md "wikilink")[部門](../Page/香港特別行政區政府部門.md "wikilink")，於1968年成立，專門負責規管[香港交通事宜](../Page/香港交通.md "wikilink")。

## 歷史

1968年前，**交通事務處**隸屬於[布政司署](../Page/布政司署.md "wikilink")；1968年12月1日成為獨立部門。1975年更改名稱為**運輸署**。1982年重組，負責管理運輸事宜。

### 隸屬

  - [布政司署運輸科](../Page/布政司署.md "wikilink")（1982年－1997年6月30日）
  - [運輸局](../Page/運輸局.md "wikilink")（1997年7月1日－2002年6月30日）
  - [環境運輸及工務局](../Page/環境運輸及工務局.md "wikilink")（2002年7月1日－2007年6月30日）
  - [運輸及房屋局](../Page/運輸及房屋局.md "wikilink")（2007年7月1日－）

## 職能

1.  管理[香港道路交通](../Page/香港道路.md "wikilink")
2.  進行各種車輛測試以判定其是否適合在馬路上行走，如[傾斜測試](../Page/傾斜測試.md "wikilink")
3.  委任獲授權人員監管香港[隧道](../Page/隧道.md "wikilink")、[公路](../Page/公路.md "wikilink")、[橋樑](../Page/橋樑.md "wikilink")、[幹線](../Page/香港幹線編號系統.md "wikilink")
4.  監管香港[公共交通機構](../Page/公共交通.md "wikilink")，包括[巴士](../Page/香港巴士.md "wikilink")、[小巴公司](../Page/香港小巴.md "wikilink")、[的士和渡輪](../Page/香港的士.md "wikilink")，規劃和管理公共交通網絡
5.  舉行駕駛考試、簽發[香港駕駛執照及](../Page/香港駕駛執照.md "wikilink")[車輛牌照事務](../Page/車輛牌照.md "wikilink")
6.  提高道路安全意識
7.  制訂香港長遠交通規劃
8.  為[長者及合資格殘疾人士公共交通票價優惠計劃作出核數](../Page/長者及合資格殘疾人士公共交通票價優惠計劃.md "wikilink")、管理、發還予有關公共交通機構等

該署亦設有[緊急事故交通協調中心](../Page/緊急事故交通協調中心.md "wikilink")，於因為突發事件（如[天災](../Page/天災.md "wikilink")、大型[交通意外](../Page/交通意外.md "wikilink")）或特定時期（如開課日、新交通系統啟用）而影響交通時運作，負責協調各種[交通工具的服務](../Page/交通工具.md "wikilink")。

## 辦事處

## 歷任署長

1.  [惠柳新](../Page/惠柳新.md "wikilink")（1972年－1974年）
2.  [麥法誠](../Page/麥法誠.md "wikilink")（1974年－1978年）
3.  [顏敦禮](../Page/顏敦禮.md "wikilink")（1978年－1982年）
4.  [李舒](../Page/李舒.md "wikilink")（1982年－1987年）
5.  [蘇燿祖](../Page/蘇燿祖.md "wikilink")（1987年－1989年）
6.  [蕭炯柱](../Page/蕭炯柱.md "wikilink")（1989年－1992年）
7.  [許仕仁](../Page/許仕仁.md "wikilink")（1992年－1995年）
8.  [任關佩英](../Page/任關佩英.md "wikilink")（1995年－1997年）
9.  [羅范椒芬](../Page/羅范椒芬.md "wikilink")（1997年－1998年）
10. [霍文](../Page/霍文.md "wikilink")（1998年－2005年）
11. [黃志光](../Page/黃志光.md "wikilink")（2005年－2009年）
12. [黎以德](../Page/黎以德.md "wikilink")（2009年－2012年）
13. [何淑兒](../Page/何淑兒.md "wikilink")（2012年7月－10月）
14. [楊何蓓茵](../Page/楊何蓓茵.md "wikilink")（2012年10月－2017年7月）
15. [陳美寶](../Page/陳美寶.md "wikilink")（2017年10月－）

## 参考文献

## 外部連結

  - [香港特別行政區政府 運輸署](http://www.td.gov.hk)

## 參見

  - [交通總部](../Page/交通總部.md "wikilink")
  - [香港車輛登記號碼](../Page/香港車輛登記號碼.md "wikilink")

{{-}}     |-    |-    |-

[運輸署](../Category/香港政府部門.md "wikilink")
[運輸署](../Category/香港交通.md "wikilink")
[Category:交通部門](../Category/交通部門.md "wikilink")