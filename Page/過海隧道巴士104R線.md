**過海隧道巴士104R線**是[香港的一條特別巴士路線](../Page/香港.md "wikilink")，由[中環5號碼頭往](../Page/中環碼頭巴士總站.md "wikilink")[旺角](../Page/旺角.md "wikilink")。主要為觀看[長洲太平清醮](../Page/長洲太平清醮.md "wikilink")「[搶包山](../Page/長洲搶包山.md "wikilink")」比賽活動後乘搭渡輪返回中環的乘客，提供直達九龍的服務。

## 歷史

  - 2005年5月16日（農曆四月初九日）凌晨：投入服務，以配合長洲太平清醮「搶包山」活動復辦。
  - 2008年[大除夕晚上](../Page/大除夕.md "wikilink")（即2009年[元旦](../Page/元旦.md "wikilink")）：配合國際金融中心「除夕[幻彩詠香江](../Page/幻彩詠香江.md "wikilink")」的倒數及煙火匯演，開始於元旦日凌晨（除夕夜）提供服務。\[1\]
  - 2013年1月1日：因「除夕幻彩詠香江」活動移師至灣仔會展舉行，取消在元旦日凌晨的服務。\[2\]

## 服務時間

佛誕翌日凌晨：01:10-02:30（客滿即開）

## 車站一覽

| [旺角方向](../Page/旺角.md "wikilink") |
| -------------------------------- |
| **車站**                           |
| 1                                |
| 2                                |
| 3                                |
| 4                                |
| 5                                |
| 6                                |
| 7                                |

## 事件

2011年2月4日（年初二），因當晚需要疏導煙花滙演後的大批人潮，[九巴特別在](../Page/九巴.md "wikilink")[灣仔區加強服務](../Page/灣仔區.md "wikilink")。但有九巴前線員工誤以為[104線加班車屬於](../Page/過海隧道巴士104線.md "wikilink")104R，於[八達通收費器顯示](../Page/八達通.md "wikilink")104R線的資料，導致部分乘坐104線的乘客多付車資。\[3\]

## 參考文獻

## 外部連結

  - 新巴

<!-- end list -->

  - [過海隧道巴士104R線](http://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=104R&route=104R&routetype=H&company=7&exactMatch=yes)
  - [過海隧道巴士104R線路線圖](http://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-NWFB-104R-104R-H.pdf)

<!-- end list -->

  - 其他

<!-- end list -->

  - [Cross Harbour Special Route 過海特別路線
    - 104R](http://www.681busterminal.com/104r.html)

[104R](../Category/過海隧道特別巴士路線.md "wikilink")
[104R](../Category/九龍巴士特別巴士路線.md "wikilink")
[104R](../Category/城巴及新世界第一巴士特別巴士路線.md "wikilink")
[104R](../Category/油尖旺區巴士路線.md "wikilink")
[104R](../Category/中西區巴士路線.md "wikilink")

1.  [城巴新巴平安夜及除夕特別服務及交通安排](http://www.nwstbus.com.hk/tc/uploadedPressRelease/2791_19122008_1-chi.pdf)，2008年12月19日。
2.  [新巴城巴平安夜及除夕特別服務及交通安排](http://www.nwstbus.com.hk/tc/uploadedPressRelease/5681_21122012_chi.pdf)，2012年12月21日。
3.  [誤開特別路線　九巴回水](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20110321&sec_id=4104&subsec=2753&art_id=15092923)，2011年3月21日