**過海隧道巴士115線**是[香港一條來往](../Page/香港.md "wikilink")[九龍城區的](../Page/九龍城區.md "wikilink")[九龍城碼頭及](../Page/九龍城碼頭.md "wikilink")[中西區的](../Page/中西區_\(香港\).md "wikilink")[中環](../Page/中環.md "wikilink")（[港澳碼頭](../Page/港澳碼頭_\(香港\).md "wikilink")）的巴士路線，提供[土瓜灣](../Page/土瓜灣.md "wikilink")、[黃埔花園](../Page/黃埔花園.md "wikilink")、[紅磡來往](../Page/紅磡.md "wikilink")[中環](../Page/中環.md "wikilink")、[灣仔的巴士服務](../Page/灣仔.md "wikilink")。

**過海隧道巴士115P線**是115線的特別班次。由[海逸豪園單](../Page/海逸豪園.md "wikilink")-{向}-開往中環（港澳碼頭），祇於星期一至六開出兩班。

## 歷史

  - 1995年1月16日，開辦，以取代119線（現[過海隧道巴士619線](../Page/過海隧道巴士619線.md "wikilink")）改經[東隧後於](../Page/東隧.md "wikilink")[土瓜灣及](../Page/土瓜灣.md "wikilink")[紅磡的服務](../Page/紅磡.md "wikilink")。
  - 1997年11月23日，提升至每天服務。
  - 1998年8月18日，增設特別班次**115P**，由[海逸豪園單程開往中環](../Page/海逸豪園.md "wikilink")（港澳碼頭），袛開平日0810,
    0825。
  - 1998年9月1日，因[中巴專營權結束](../Page/中華汽車有限公司.md "wikilink")，改由[九巴及](../Page/九巴.md "wikilink")[新巴合營](../Page/新巴.md "wikilink")。**115P**線因而成為中巴喪失專營權前最後開辦的一條巴士路線，由開辦直至專營權變更，前後只有13天。
  - 2011年4月1日，因[天星小輪來往紅磡至灣仔及中環的渡輪停辦](../Page/天星小輪.md "wikilink")，星期一至五於0750-0820加開四班特別車，由德民街開出\[1\]。
  - 2014年9月28日：受[佔領中環示威行動影響全線停駛近](../Page/讓愛與和平佔領中環.md "wikilink")5日，到同年10月3日有限度恢復九龍至港島[灣仔的服務](../Page/灣仔.md "wikilink")。

## 服務時間及班次

  - 115常規班次

| [九龍城碼頭開](../Page/九龍城碼頭.md "wikilink") | [中環](../Page/中環.md "wikilink")（[港澳碼頭](../Page/港澳碼頭_\(香港\).md "wikilink")）開 |
| ------------------------------------- | -------------------------------------------------------------------------- |
| **日期**                                | **服務時間**                                                                   |
| 星期一至五                                 | 06:45、07:00                                                                |
| 07:15-08:28                           | 4-9                                                                        |
| 08:28-10:30                           | 10-11                                                                      |
| 10:30-14:30                           | 12-20                                                                      |
| 14:30-22:55                           | 12-15                                                                      |
| 23:15                                 | 23:50                                                                      |
| 星期六                                   | 07:15-08:00                                                                |
| 08:00-09:55                           | 9-10                                                                       |
| 09:55-12:50                           | 12-20                                                                      |
| 12:50-16:15                           | 15-20                                                                      |
| 16:15-21:45                           | 12-15                                                                      |
| 21:45-23:15                           | 15-20                                                                      |
| 星期日及公眾假期                              | 07:15-08:00                                                                |
| 08:00-09:00                           | 20                                                                         |
| 09:00-21:00                           | 15                                                                         |
| 21:00-21:40                           | 20                                                                         |
| 22:00                                 |                                                                            |
| 22:15-23:15                           | 20                                                                         |

  - 115特別班次

<!-- end list -->

  - 德民街 → 中環（港澳碼頭）
      - 星期一至五：07:50
  - 黃埔花園 → 中環（港澳碼頭）　
      - 星期一至五：08:10、08:25、08:40

<!-- end list -->

  -
    星期六、日及公眾假期不設服務

<!-- end list -->

  - 115P

<!-- end list -->

  - [海逸豪園開](../Page/海逸豪園.md "wikilink")：
      - 星期一至六：08:10、08:25

<!-- end list -->

  -
    星期日及公眾假期不設服務

## 收費

全程：$9.8

  - 過紅磡海底隧道後往中環（港澳碼頭）／九龍城碼頭：$6.1

### 八達通巴士轉乘計劃

## 用車演變史

此路線開辦時已提供全空調服務，兩巴用車均以利蘭奧林比安11米（九巴：AL、中巴：LA）、富豪奧林比安11米（九巴：AV、中巴：VA）及丹尼士巨龍11米（九巴：AD、中巴：DA）為主。其中九巴於1997年加入12米3AD。

此路線於1998年交由九巴及新巴經營，踏入廿一世紀，新巴全線派出低地台巴士行走，用車以丹尼士三叉戟12米（10XX-11XX、30XX）為主。九巴用車亦以富豪超級奧林比安12米（3ASV）為主配以少數丹尼士三叉戟12米（ATR）。2006年加入直樓梯巴士行走，用車以配置Wright車身的富豪B9TL
12米（AVBW）及富豪超級奧林比安12米（AVW）為主。

2010年7月26日，此路線加入荔枝角車廠（L）派車，成為目前眾多過海路線中唯一一條有兩間車廠派車的過海路線\[11\]，初時派車為一輛富豪奧林比安（AV400／HR1554），只於平日星期一至五早上繁忙時間行走，其後改派Enviro500
12米（ATE）。

2011年8月，九龍灣車廠（K）改派三輛配置歐盟五型引擎的富豪B9TL（AVBWU）取代三輛配置Wright車身的富豪超級奧林比安（AVW）行走。至2013年被原1A線的Enviro500（ATE）取代。其後全線用車被改為原118線及113線的丹尼士三叉戟（ATR）及富豪超級奧林比安（3ASV）。

2014年12月，荔枝角車廠（L）改派高載客量的Enviro500 MMC
12米（ATENU）行走，而九龍灣車廠（K）亦在2015年4月改派同款巴士。惟約兩年後，荔枝角車廠（L）改派高載版富豪B9TL
12米（AVBWU），九龍灣車廠（K）亦在2017年4月先行加入一輛（高載版樣板車 SD9537）。2018年5月，兩廠均派出高載版富豪B9TL
12米（AVBWU）行走，九龍灣車廠（K）後來更改派「紅蛋」。2018年10月尾，九龍灣車廠派出的「紅蛋」與1A的「紅
FL」對調，使本線平均車齡下降至0.64年。

2018年5月，新巴獲准於此路線營運12.8米巴士，隨即加入Enviro500 MMC 12.8米（61XX）行走此路線。

## 使用車輛

### 九巴

現時九巴使用5輛[亞歷山大丹尼士Enviro 500
MMC及](../Page/亞歷山大丹尼士Enviro_500_MMC.md "wikilink")2輛[富豪B9TL](../Page/富豪B9TL.md "wikilink")12米。
\[2\]

### 新巴

現時新巴行走115線的車輛多為[富豪超級奧林比安](../Page/富豪超級奧林比安.md "wikilink")12米、[亞歷山大丹尼士Enviro
500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")12米及[亞歷山大丹尼士Enviro 500
MMC](../Page/亞歷山大丹尼士Enviro_500_MMC.md "wikilink")1 2米。

而115P線新巴車輛由將軍澳車廠派出，行走本線前先行走[796X線](../Page/新巴796X線.md "wikilink")，行駛完本線後返回115線繼續行走，因此用車與115線相同。

繁忙時間，新巴會安排原本行走[111線抽調來支援本線](../Page/過海隧道巴士111線.md "wikilink")。

## 行車路線

  - 115

**九龍城碼頭開**經：[新碼頭街](../Page/新碼頭街.md "wikilink")、[土瓜灣道](../Page/土瓜灣道.md "wikilink")、[馬頭圍道](../Page/馬頭圍道.md "wikilink")、[蕪湖街](../Page/蕪湖街.md "wikilink")、[德民街](../Page/德民街.md "wikilink")、[德安街](../Page/德安街.md "wikilink")、[黃埔花園公共運輸交匯處](../Page/黃埔花園公共運輸交匯處.md "wikilink")、[德康街](../Page/德康街.md "wikilink")、[紅磡道](../Page/紅磡道.md "wikilink")、[紅磡繞道](../Page/紅磡繞道.md "wikilink")、[紅樂道](../Page/紅樂道.md "wikilink")、[紅荔道](../Page/紅荔道.md "wikilink")、[紅磡南道](../Page/紅磡南道.md "wikilink")、[紅菱街](../Page/紅菱街.md "wikilink")、[暢通道](../Page/暢通道.md "wikilink")、[機利士南路](../Page/機利士南路.md "wikilink")、蕪湖街、[漆咸道北](../Page/漆咸道北.md "wikilink")、[康莊道](../Page/康莊道.md "wikilink")、[紅磡海底隧道](../Page/海底隧道_\(香港\).md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[夏愨道](../Page/夏愨道.md "wikilink")、[干諾道中及](../Page/干諾道中.md "wikilink")[港澳碼頭通道](../Page/港澳碼頭通道.md "wikilink")。

**中環（港澳碼頭）開**經：港澳碼頭通道、干諾道中、[林士街](../Page/林士街.md "wikilink")、[德輔道中](../Page/德輔道中.md "wikilink")、[金鐘道](../Page/金鐘道.md "wikilink")、[軒尼詩道](../Page/軒尼詩道.md "wikilink")、[菲林明道](../Page/菲林明道.md "wikilink")、[灣仔道](../Page/灣仔道.md "wikilink")、[摩理臣山道](../Page/摩理臣山道.md "wikilink")、[禮頓道](../Page/禮頓道.md "wikilink")、[堅拿道西](../Page/堅拿道西.md "wikilink")、[堅拿道天橋](../Page/堅拿道天橋.md "wikilink")、紅磡海底隧道、康莊道、漆咸道北、[蕪湖街隧道](../Page/蕪湖街隧道.md "wikilink")、機利士南路、紅磡南道、紅磡道、[庇利街](../Page/庇利街.md "wikilink")、馬頭圍道、土瓜灣道及新碼頭街。

  - 115P

**經**：[海逸道](../Page/海逸道.md "wikilink")、[大環道東](../Page/大環道東.md "wikilink")、紅磡道、德安街、黃埔花園公共運輸交匯處、德康街、紅磡道、紅磡繞道、紅鸞道、紅樂道、紅荔道、紅磡南道、暢通道、機利士南路、蕪湖街、漆咸道北、康莊道、紅磡海底隧道、告士打道、夏愨道、干諾道中及港澳碼頭通道。

### 沿線車站

[Crossharbor115RtMap.png](https://zh.wikipedia.org/wiki/File:Crossharbor115RtMap.png "fig:Crossharbor115RtMap.png")

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/九龍城碼頭.md" title="wikilink">九龍城碼頭</a>／<a href="../Page/海逸豪園.md" title="wikilink">海逸豪園開</a></p></th>
<th><p><a href="../Page/中環.md" title="wikilink">中環</a>（<a href="../Page/港澳碼頭_(香港).md" title="wikilink">港澳碼頭</a>）開</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>序號</strong></p></td>
<td><p><strong>車站名稱</strong></p></td>
</tr>
<tr class="even">
<td><p>1*</p></td>
<td><p><a href="../Page/九龍城碼頭巴士總站.md" title="wikilink">九龍城碼頭巴士總站</a></p></td>
</tr>
<tr class="odd">
<td><p>2*</p></td>
<td><p><a href="../Page/貴州街.md" title="wikilink">貴州街</a></p></td>
</tr>
<tr class="even">
<td><p>3*</p></td>
<td><p><a href="../Page/落山道.md" title="wikilink">落山道</a></p></td>
</tr>
<tr class="odd">
<td><p>4*</p></td>
<td><p><a href="../Page/啟明街.md" title="wikilink">啟明街</a></p></td>
</tr>
<tr class="even">
<td><p>5*</p></td>
<td><p><a href="../Page/庇利街.md" title="wikilink">庇利街</a></p></td>
</tr>
<tr class="odd">
<td><p>6*</p></td>
<td><p><a href="../Page/鶴園街.md" title="wikilink">鶴園街</a></p></td>
</tr>
<tr class="even">
<td><p>7*</p></td>
<td><p><a href="../Page/紅磡市政大廈.md" title="wikilink">紅磡街市</a></p></td>
</tr>
<tr class="odd">
<td><p>8*</p></td>
<td><p><a href="../Page/德民街.md" title="wikilink">德民街</a></p></td>
</tr>
<tr class="even">
<td><p>^</p></td>
<td><p><a href="../Page/海逸豪園.md" title="wikilink">海逸豪園巴士總站</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/黃埔花園.md" title="wikilink">黃埔花園巴士總站</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>紅樂道<br />
（<a href="../Page/海韻軒.md" title="wikilink">海韻軒</a>）</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/紅磡南道.md" title="wikilink">紅磡南道</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>機利士南路</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>紅磡海底隧道</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/伊利莎伯大廈.md" title="wikilink">伊利莎伯大廈</a></p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="../Page/舊灣仔警署.md" title="wikilink">舊灣仔警署</a></p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/分域街.md" title="wikilink">分域街</a></p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>皇后像廣場</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/砵典乍街.md" title="wikilink">砵典乍街</a></p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>林士街</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>中環（港澳碼頭）</p></td>
</tr>
</tbody>
</table>

  - 115P線停靠有 ^ 號之車站及往中環方向第9-20號車站，不設回程及不停有 \* 號之車站。

## 參考資料

  - 《二十世紀巴士路線發展史－－渡海、離島及機場篇》，容偉釗編著，BSI出版

## 外部連結

  - 新巴

<!-- end list -->

  - [過海隧道巴士115線](http://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=115&route=115&routetype=D&company=7&exactMatch=yes)
  - [過海隧道巴士115P線](http://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=115P&route=115P&routetype=M&company=7&exactMatch=yes)
  - [過海隧道巴士115線路線圖](http://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-NWFB-115-115-D.pdf)
  - [過海隧道巴士115P線路線圖](https://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-NWFB-115-115P-M.pdf)

<!-- end list -->

  - 九巴

<!-- end list -->

  - [過海隧道巴士115線](http://www.kmb.hk/tc/services/search.html?busno=115)
  - [過海隧道巴士115P線](http://www.kmb.hk/tc/services/search.html?busno=115P)
  - [681巴士總站－隧巴115線](http://www.681busterminal.com/115.html)

[115](../Category/城巴及新世界第一巴士路線.md "wikilink")
[115](../Category/九龍巴士路線.md "wikilink")
[115](../Category/九龍城區巴士路線.md "wikilink")
[115](../Category/中西區巴士路線.md "wikilink")
[115](../Category/前中華巴士路線.md "wikilink")

1.  [115加開德民街特車（三月二十八日最新消息）](http://www.hkitalk.net/HKiTalk2/viewthread.php?tid=600308&extra=page%3D2&page=1)，HKITALK.NET
2.  <http://1005.idv.hk/index.php?page=01&search=sch&word=115>