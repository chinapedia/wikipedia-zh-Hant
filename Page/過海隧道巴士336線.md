**過海隧道巴士336線**是[香港](../Page/香港.md "wikilink")[九龍巴士經營的一條早上繁忙時間巴士路線](../Page/九龍巴士.md "wikilink")，由[梨木樹單向開往](../Page/梨木樹.md "wikilink")[上環](../Page/上環.md "wikilink")。此路線已於2014年6月16日停辦，由當日開辦的[936線取代](../Page/過海隧道巴士936線.md "wikilink")。

## 歷史

  - 1992年5月18日：投入服務，由[中巴及](../Page/中華汽車有限公司.md "wikilink")[九巴聯合經營](../Page/九巴.md "wikilink")，中巴更在早上加開[金鐘至梨木樹的班次](../Page/金鐘.md "wikilink")，目的是免得空車入新界，使本路線成為雙向路線。
  - 1998年9月1日：中巴喪失專營權，本路線改為九巴獨自經營。

由於青山公路並沒有直接道路進入[西隧](../Page/西隧.md "wikilink")，本路線在西隧通車後並沒有改行西隧。不過本路線在葵青區路線取道西隧的大勢之下，仍然行走[紅隧](../Page/紅隧.md "wikilink")，容易因[加士居道平日繁忙時間紅隧及](../Page/加士居道.md "wikilink")[九龍灣方向交通擠塞延誤時間](../Page/九龍灣.md "wikilink")。

因此運輸署及九巴於《2013-14年度巴士路線發展計劃》建議本路線改行西區海底隧道，更改編號為[936](../Page/過海隧道巴士936線.md "wikilink")，以銅鑼灣（[棉花路](../Page/棉花路.md "wikilink")）作終站，並增設回程服務，加強梨木樹及上葵涌的過海隧巴服務，建議已獲荃灣區議會通過，有關建議於2014年6月16日生效，而336線已於2014年6月14日提供最後一日服務。

## 車費

  - 全程：$17.3
  - 過海底隧道後往上環：$5.7

## 派車

主要在早上抽調[36B和](../Page/九龍巴士36B線.md "wikilink")[243M線巴士行走](../Page/九龍巴士243M線.md "wikilink")。

車型不定，但主要為[丹尼士三叉戟](../Page/丹尼士三叉戟三型.md "wikilink")12米、[富豪超級奧林比安或](../Page/富豪超級奧林比安.md "wikilink")[亞歷山大丹尼士Enviro
500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")。

## 途經街道

  - [和宜合道](../Page/和宜合道.md "wikilink")、[青山公路](../Page/青山公路.md "wikilink")（葵涌段）、-{[蝴蝶谷道](../Page/蝴蝶谷道.md "wikilink")}-、[荔枝角道](../Page/荔枝角道.md "wikilink")、[西九龍走廊](../Page/西九龍走廊.md "wikilink")、[渡船街](../Page/渡船街_\(香港\).md "wikilink")、[加士居道天橋](../Page/加士居道.md "wikilink")、[漆咸道南](../Page/漆咸道南.md "wikilink")、[康莊道](../Page/康莊道.md "wikilink")、[紅磡海底隧道](../Page/紅磡海底隧道.md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[夏慤道](../Page/夏慤道.md "wikilink")、[紅棉路](../Page/紅棉路.md "wikilink")、[金鐘道及](../Page/金鐘道.md "wikilink")[德輔道中](../Page/德輔道中.md "wikilink")

### 車站一覽

| [上環方向](../Page/上環.md "wikilink") |
| -------------------------------- |
| **車站**                           |
| 1                                |
| 2                                |
| 3                                |
| 4                                |
| 5                                |
| 6                                |
| 7                                |
| 8                                |
| 9                                |
| 10                               |
| 11                               |
| 12                               |
| 13                               |
| 14                               |
| 15                               |

## 參考資料

  - 書名：二十世紀巴士路線發展史－－渡海、離島及機場篇，ISBN 9789628414680，容偉釗編著，BSI出版

## 外部連結

  - 九巴

<!-- end list -->

  - [過海隧道巴士336線](http://www.kmb.hk/tc/services/search.html?busno=336)

<!-- end list -->

  - 其他

<!-- end list -->

  - [KMB Crossing Harbour Route
    九巴過海路線－336](http://www.681busterminal.com/336.html)
  - [i-busnet.com－Harbour
    Rt. 336](https://web.archive.org/web/20080406033718/http://www.i-busnet.com/busroute/harbour/harbourr336.php)

[336](../Category/已取消九龍巴士路線.md "wikilink")
[336](../Category/已取消過海隧道巴士路線.md "wikilink")
[336](../Category/前中華巴士路線.md "wikilink")