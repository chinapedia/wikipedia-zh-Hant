**過海隧道巴士603線**是[香港](../Page/香港.md "wikilink")[東區海底隧道巴士路線](../Page/東區海底隧道.md "wikilink")，來往[平田及](../Page/平田邨.md "wikilink")[中環碼頭](../Page/中環碼頭.md "wikilink")。

本線於平日繁忙時間另設三條特別路線：**603A**、**603P**及**603S**，三線於九龍區之走線與主線603相同，但前者途經[銅鑼灣](../Page/銅鑼灣.md "wikilink")、[禮頓道及](../Page/禮頓道.md "wikilink")[皇后大道東](../Page/皇后大道東.md "wikilink")，二者不經銅鑼灣及[天后前往](../Page/天后.md "wikilink")[平田](../Page/平田邨.md "wikilink")，三者以干諾道中近機利文街（即本線舊有的終點站）為終點站，不前往中環碼頭。

由於本線是一條九巴獨營特快過海隧道巴士路線，申請車費加價無須配合對家巴士公司，所以全程車費（$12.1）較其他聯營特快過海隧道巴士路線302、621、641（$11.1）及101X、302A、606X（$11.7）的高。

自2002年8月28日九巴獨營過海路線693號線停辦，與691合併成692後，本路線曾是唯一一條九巴獨營的東區海底隧道全日服務路線，直至九巴獨營東隧過海路線[673於](../Page/過海隧道巴士373線.md "wikilink")2016年10月22日起提升至每天全日服務後，才結束這一個局面。

## 歷史

本路線早在1988年[東區海底隧道未通車時已獲](../Page/東區海底隧道.md "wikilink")[交通諮詢委員會准予開辦](../Page/交通諮詢委員會.md "wikilink")，其時定名為403線，計劃來往[油塘及](../Page/油塘.md "wikilink")[灣仔碼頭](../Page/灣仔碼頭.md "wikilink")，定於1991年末開辦，但東區海底隧道通車後，卻沒有立即開辦，直至藍田南部的碧雲道一帶開始發展，該處公屋及居屋開始入伙，才正式開辦，並正名為603線。

  - 1994年6月30日：**603**開辦，初時為中環至[藍田（北）](../Page/藍田（北）巴士總站.md "wikilink")，中環的總站設於[干諾道中近](../Page/干諾道中.md "wikilink")[永和街](../Page/永和街.md "wikilink")，去程途經告士打道及干諾道中，回程途經軒尼詩道、菲林明道、灣仔道及堅拿道天橋，只於平日繁忙時間提供單向服務。
  - 1995年8月29日：提升至平日繁忙時間雙向服務，往藍田方向改經怡和街、高士威道及興發街。
  - 1996年8月12日：提升至每天全日服務。
  - 2000年8月31日：加開短程班次**603P**，由金鐘道近統一中心開往藍田（北），祇於星期一至五18:00、18:20開出。
  - 2002年2月18日：603及603P往藍田（北）方向繞經[安田街及](../Page/安田街.md "wikilink")[平田街](../Page/平田街.md "wikilink")。
  - 2003年6月1日：603中環總站遷往[中環渡輪碼頭](../Page/中環渡輪碼頭.md "wikilink")。
  - 2003年7月6日：603及603P配合[平田巴士總站啟用而遷入作總站](../Page/平田巴士總站.md "wikilink")。
  - 2004年2月23日：加開短程班次**603S**，由平田開往干諾道中近永和街，袛於星期一至五早上開出三班（現已加至21班）。主要方便車輛到達中環干諾道中後，就可以立刻「私牌」返回平田，不用前往中環渡輪碼頭。
  - 2008年6月8日：本線全程收費提升至$10.5；同時，分段收費亦調整票價。
  - 2011年1月7日：加密班次，行走巴士數目增加至21輛。
  - 2011年5月15日：本線全程收費提升至$11.0。
  - 2012年11月30日：本線提早假日由平田開出頭車至0545，而603S延長尾車時間至0910。
  - 2013年3月17日：本線全程收費提升至$11.6；同時，部份分段收費亦調整票價。
  - 2013年7月1日；因應新巴新專營權（為期10年）於2013年7月1日起生效，本線及輔助線603S增加轉乘[新巴30X線的八達通轉乘優惠](../Page/新巴30X線.md "wikilink")，成為首批推出與港島區非過海路線的跨公司轉乘計劃的九巴獨營過海路線。
  - 2014年7月6日：本線全程收費提升至$12.1；同時，部份分段收費亦調整票價。
  - 2015年10月5日：提早平田頭班車開出時間至05:35及調整班次\[1\]。
  - 2015年10月5日：提早由平田開出的頭班車時間至05:35，並調整班次。同時將平日下午繁忙時間由林士街開出的非正式特別班次常規化。
  - 2016年6月：早上繁忙時間由平田開往金鐘（海富中心）的非正式特別班次常規化。
  - 2016年10月11日：603P改由中環渡輪碼頭開出，依主線行駛至軒尼詩道後，改經馬師道、鴻興道、告士打道及維園道直上東區走廊，不再途經銅鑼灣。2014年9月28日「雨傘革命」爆發，本線原本行經的怡和街成為佔領區之一，九巴因而於當年11月12日起至運動結束期間，安排此路線延長由中環渡輪碼頭開出，駛至軒尼詩道後改經馬師道、鴻興道、告士打道及維園道直上東區走廊，結果臨時改道定線因避開經常擠塞的銅鑼灣及天后一帶，反而比原有走線更快到達目的地，廣受在中環至灣仔區上班的油塘、藍田居民歡迎。銅鑼灣佔領區清場後本線恢復原有路線，惟九巴於《2016-2017年度巴士路線計劃》中提出將此路線在當年11月12日至雨傘革命結束期間的臨時改道永久實施，旋獲各區議會通過。
  - 2018年1月15日：603A線投入服務，祇於星期一至五上午繁忙時間由平田開出兩班往中環街市，途經[銅鑼灣](../Page/銅鑼灣.md "wikilink")、[皇后大道東](../Page/皇后大道東.md "wikilink")。同日起，603S班次減至17班\[2\]。
  - 2019年4月7日：603線由平田開出的頭班車時間提早至05:20\[3\]。

## 服務時間及班次

  - 603

| [平田開](../Page/平田邨.md "wikilink") | [中環渡輪碼頭開](../Page/中環碼頭.md "wikilink") |
| -------------------------------- | ------------------------------------- |
| **日期**                           | **服務時間**                              |
| 星期一至五                            | 05:20-06:05                           |
| 06:05-07:35                      | 10                                    |
| 07:35-07:53                      | 9                                     |
| 07:58、08:08、08:18                | 09:05-13:05                           |
| 08:30-09:00                      | 10                                    |
| 09:00-19:30                      | 15                                    |
| 19:30-23:30                      | 20                                    |
| 星期六                              | 05:20-06:05                           |
| 06:05-07:05                      | 12                                    |
| 07:14、07:22                      | 00:30                                 |
| 07:30-08:30                      | 6                                     |
| 08:30-09:00                      | 10                                    |
| 09:00-10:00                      | 12                                    |
| 10:00-20:30                      | 15                                    |
| 20:30-23:30                      | 20                                    |
| 星期日及公眾假期                         | 05:20-06:00                           |
| 06:00-11:00                      | 15                                    |
| 11:00-17:00                      | 12                                    |
| 17:00-19:30                      | 15                                    |
| 19:30-23:30                      | 20                                    |

  - 603特別班次

<!-- end list -->

  - 星期一至五：
      - 平田 → 金鐘（海富中心）：07:55
      - 林士街 → 平田：17:40、18:00、18:15、18:30、18:45、19:00

<!-- end list -->

  - 603A

<!-- end list -->

  - 平田開：
      - 星期一至五：07:40、07:48

<!-- end list -->

  - 603P

<!-- end list -->

  - 中環渡輪碼頭開：
      - 星期一至五：18:00、18:20

<!-- end list -->

  - 603S

| [平田開](../Page/平田邨.md "wikilink")    |
| ----------------------------------- |
| **日期**                              |
| 星期一至五                               |
| 07:50、07:55、08:00、08:04、08:13、08:23 |
| 08:27、08:35、08:45、08:55、09:10       |

  -
    所有特別班次、603A、603P及603S線逢星期六、日及公眾假期不設服務

## 收費

全程：$12.1

  - 過[東區海底隧道後往中環](../Page/東區海底隧道.md "wikilink")：$5.7
  - 過東區海底隧道後往平田：$6.4
  - 高怡邨往平田：$5.2

### 八達通轉乘優惠

乘客登上本線後指定時間內以同一張八達通卡轉乘以下路線，或從以下路線登車後指定時間內以同一張八達通卡轉乘本線，次程可獲車資折扣優惠：

  - 603(S)↔東區海底隧道路線，次程可獲$5折扣優惠，必須於東區海底隧道收費廣場轉乘，轉乘時限為150分鐘

<!-- end list -->

  - **603(S)往港島**↔[302](../Page/過海隧道巴士302線.md "wikilink")、[307(A/B/P)](../Page/過海隧道巴士307線.md "wikilink")、[3/673](../Page/過海隧道巴士373線.md "wikilink")、[601(P)](../Page/過海隧道巴士601線.md "wikilink")、[606(A/X)](../Page/過海隧道巴士606線.md "wikilink")、[619(P/X)](../Page/過海隧道巴士619線.md "wikilink")、[641](../Page/過海隧道巴士641線.md "wikilink")、[671](../Page/過海隧道巴士671線.md "wikilink")、[680(A/P/X)](../Page/過海隧道巴士680線.md "wikilink")、[681(P)](../Page/過海隧道巴士681線.md "wikilink")、[682(A/B/C/P)](../Page/過海隧道巴士682線.md "wikilink")、[690(P)或](../Page/過海隧道巴士690線.md "wikilink")[694往港島](../Page/過海隧道巴士694線.md "wikilink")

<!-- end list -->

  - 603(P/S)↔[613](../Page/過海隧道巴士613線.md "wikilink")，613線車資全免

<!-- end list -->

  - [613往筲箕灣](../Page/過海隧道巴士613線.md "wikilink") → '''603(S)往中環 '''
  - **603(P)**往平田/→[613往安達](../Page/過海隧道巴士613線.md "wikilink")
  - **603(S)往中環** → 613往筲箕灣
  - 613往安達 → **603(P)往平田**

<!-- end list -->

  - 603(P/S)↔新巴30X，次程可獲$1.5折扣優惠，轉乘時限為90分鐘

<!-- end list -->

  - **603(S)往中環** → 30X往數碼港
  - 30X往中環 → **603(P)往平田**

## 使用車輛

現時行走603線的巴士為12輛[富豪B9TL](../Page/富豪B9TL.md "wikilink")（AVBWU）及9輛[亞歷山大丹尼士Enviro
500MMC](../Page/亞歷山大丹尼士Enviro_500MMC.md "wikilink")（ATENU），均屬[九龍灣車廠](../Page/九巴九龍灣車廠.md "wikilink")。\[4\]

## 行車路線

**平田開**經：[安田街](../Page/安田街.md "wikilink")、[平田街](../Page/平田街.md "wikilink")、安田街、[藍田（北）巴士總站](../Page/藍田（北）巴士總站.md "wikilink")、[德田街](../Page/德田街.md "wikilink")、[連德道](../Page/連德道.md "wikilink")、[碧雲道](../Page/碧雲道.md "wikilink")、[高超道](../Page/高超道.md "wikilink")、[鯉魚門道](../Page/鯉魚門道.md "wikilink")、[東區海底隧道](../Page/東區海底隧道.md "wikilink")、[東區走廊](../Page/東區走廊.md "wikilink")、[維園道](../Page/維園道.md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[夏愨道](../Page/夏愨道.md "wikilink")、[干諾道中](../Page/干諾道中.md "wikilink")、*[民光街及](../Page/民光街.md "wikilink")[民耀街](../Page/民耀街.md "wikilink")*。

  -
    603S不經*斜體字*之路段。
    603A依603原線至東區走廊後，改經興發街、[永興街](../Page/永興街.md "wikilink")、[英皇道](../Page/英皇道.md "wikilink")、高士威道、[禮頓道](../Page/禮頓道.md "wikilink")、[摩理臣山道](../Page/摩理臣山道.md "wikilink")、[皇后大道東](../Page/皇后大道東.md "wikilink")、金鐘道及德輔道中。

**中環渡輪碼頭開**經：民耀街、[民祥街](../Page/民祥街.md "wikilink")、隧道、-{干}-諾道中、[林士街](../Page/林士街.md "wikilink")、[德輔道中](../Page/德輔道中.md "wikilink")、[金鐘道](../Page/金鐘道.md "wikilink")、[軒尼詩道](../Page/軒尼詩道.md "wikilink")、[怡和街](../Page/怡和街.md "wikilink")、[高士威道](../Page/高士威道.md "wikilink")、[興發街](../Page/興發街.md "wikilink")、東區走廊、東區海底隧道、鯉魚門道、高超道、碧雲道、連德道、德田街及安田街。

  -
    603P依603原線至軒尼詩道後，改經[馬師道](../Page/馬師道.md "wikilink")、[鴻興道](../Page/鴻興道.md "wikilink")、天橋、告士打道、維園道返回東區走廊603原線。

### 沿線車站

[CrossHarbor603RtMap.png](https://zh.wikipedia.org/wiki/File:CrossHarbor603RtMap.png "fig:CrossHarbor603RtMap.png")

  - 603(P/S)

| [平田開](../Page/平田邨.md "wikilink") | [中環渡輪碼頭開](../Page/中環碼頭.md "wikilink")   |
| -------------------------------- | --------------------------------------- |
| **序號**                           | **車站名稱**                                |
| 1                                | [平田巴士總站](../Page/平田邨.md "wikilink")     |
| 2                                | [藍田](../Page/藍田.md "wikilink")（北）       |
| 3                                | [康逸苑](../Page/康逸苑.md "wikilink")        |
| 4                                | [藍田公園](../Page/藍田_\(香港\).md "wikilink") |
| 5                                | [德田邨德樂樓](../Page/德田邨.md "wikilink")     |
| 6                                | 德田邨德隆樓                                  |
| 7                                | [廣田邨廣靖樓](../Page/廣田邨.md "wikilink")     |
| 8                                | [廣田商場](../Page/廣田邨#廣田商場.md "wikilink")  |
| 9                                | [高俊苑](../Page/高俊苑.md "wikilink")        |
| 10                               | [高怡邨](../Page/高怡邨.md "wikilink")        |
| 11                               | [鯉魚門廣場](../Page/鯉魚門廣場.md "wikilink")    |
| 12                               | [油塘邨](../Page/油塘邨.md "wikilink")        |
| 13                               | [聖安當女書院](../Page/聖安當女書院.md "wikilink")  |
| 14                               | [東區海底隧道](../Page/東區海底隧道.md "wikilink")  |
| 15                               | [舊灣仔警署](../Page/舊灣仔警署.md "wikilink")    |
| 16                               | [分域街](../Page/分域街.md "wikilink")        |
| 17@                              | 皇后像廣場                                   |
| 18@                              | [砵典乍街](../Page/砵典乍街.md "wikilink")      |
| 19@                              | [機利文街](../Page/機利文街.md "wikilink")      |
| 20\*                             | [中環三號碼頭](../Page/中環三號碼頭.md "wikilink")  |
| 21\*                             | 中環渡輪碼頭巴士總站                              |
|                                  | 22                                      |
| 23                               | 德田邨德康樓                                  |
| 24                               | 德田邨德敬樓                                  |
| 25                               | 康逸苑                                     |
| 26                               | [藍田綜合大樓](../Page/藍田綜合大樓.md "wikilink")  |
| 27                               | [平田邨平真樓](../Page/平田邨.md "wikilink")     |
| 28                               | 平田巴士總站                                  |

  -
    平田開出特別班次、603P/S線不停靠有 \* 號之車站。
    平田開出特別班次以夏愨道海富中心為總站，不停靠有 @ 號之車站。

<!-- end list -->

  - 603A

| 平田開    |
| ------ |
| **序號** |
| 1      |
| 2      |
| 3      |
| 4      |
| 5      |
| 6      |
| 7      |
| 8      |
| 9      |
| 10     |
| 11     |
| 12     |
| 13     |
| 14     |
| 15     |
| 16     |
| 17     |
| 18     |
| 19     |
| 20     |
| 21     |
| 22     |
| 23     |
| 24     |
| 25     |
| 26     |
| 27     |

## 參考資料

<references/>

  - 《二十世紀巴士路線發展史－－渡海、離島及機場篇》，容偉釗編著，BSI出版

## 外部連結

  - 九巴

<!-- end list -->

  - [九巴－隧巴603線](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=603)
  - [九巴－隧巴603A線](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=603A)
  - [九巴－隧巴603P線](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=603P)
  - [九巴－隧巴603S線](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=603S)

<!-- end list -->

  - 其他

<!-- end list -->

  - [681巴士總站 隧巴603線](http://www.681busterminal.com/603.html)
  - [九巴路線掛牌表](https://web.archive.org/web/20080505040532/http://ioo5.net/index.php)

[603](../Category/九龍巴士路線.md "wikilink")
[603](../Category/觀塘區巴士路線.md "wikilink")
[603](../Category/中西區巴士路線.md "wikilink")

1.  [九巴乘客通告](http://www.kmb.hk/tc/news/realtimenews.html?page=1443769188_4627_0.jpg)
2.  [九巴603A線宣傳單張](http://www.kmb.hk/lib/603A.pdf)[九巴乘客通告](http://search.kmb.hk/KMBWebSite/AnnouncementPicture.ashx?url=1515630249_9697_0.pdf)
3.  [603提早服務時間及修訂行車時間表](http://search.kmb.hk/KMBWebSite/AnnouncementPicture.ashx?url=1554276444_9732_0.pdf)
4.  <http://1005.idv.hk/index.php?page=01&search=sch&word=603>