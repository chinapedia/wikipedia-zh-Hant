**過海隧道巴士960A線**是[香港一條途經](../Page/香港.md "wikilink")[西區海底隧道的巴士路線](../Page/西區海底隧道.md "wikilink")，由[九巴營運](../Page/九巴.md "wikilink")，於平日星期一至五傍晚忙時間由中環單-{向}-往[洪水橋](../Page/洪水橋.md "wikilink")（[洪福邨](../Page/洪福邨.md "wikilink")）。

**過海隧道巴士960B線**同樣是星期一至五傍晚忙時間提供服務，由[鰂魚涌單](../Page/鰂魚涌.md "wikilink")-{向}-往洪水橋（洪福邨）。

**過海隧道巴士960X線**是[香港一條早上繁忙時間巴士路線](../Page/香港.md "wikilink")，[過海隧道巴士960線的特別班次](../Page/過海隧道巴士960線.md "wikilink")，由[洪水橋](../Page/洪水橋.md "wikilink")（洪元路）單-{向}-往[鰂魚涌](../Page/鰂魚涌.md "wikilink")（英皇道）。

## 歷史

  - 2004年4月13日：960A線投入服務，初時只前往屯門[建生巴士總站](../Page/建生巴士總站.md "wikilink")。運輸署為了針對日趨嚴重的非法屋邨巴士對[960線的競爭](../Page/過海隧道巴士960線.md "wikilink")，決定大力取締非法屋邨巴士，因而指示九巴開辦本線以舒緩960線在黃昏繁忙時間的壓力。
  - 2007年9月17日：延長至[洪水橋](../Page/洪水橋.md "wikilink")，加停[西區海底隧道收費廣場巴士站](../Page/西區海底隧道.md "wikilink")，並繞經[兆康苑及](../Page/兆康苑.md "wikilink")[富泰邨](../Page/富泰邨.md "wikilink")。
  - 2008年5月13日：配合[960B線開辦](../Page/過海隧道巴士960B線.md "wikilink")，取消19:30、19:50班次。
  - 2009年10月27日：調整班次至祇開18:30一班車。
  - 2012年8月6日：960X線投入服務\[1\]。
  - 2013年9月28日：加停屯門公路巴士轉乘站，並與其他路線提供轉乘優惠。
  - 2015年6月6日：修改行車路線，抵達青山公路後改經洪天路、洪水橋田心路、洪元路，並以[洪福邨為終站](../Page/洪福邨.md "wikilink")\[2\]。
  - 2018年4月16日：960X線加開06:50一班車。

## 服務時間及班次

  - 960A（中環（中華總商會大廈）開）

<!-- end list -->

  - 星期一至五：18:30

<!-- end list -->

  - 960B（鰂魚涌（英皇道）開）

<!-- end list -->

  - 星期一至五：18:30、18:45、19:00、19:15

<!-- end list -->

  -
    星期六、日及公眾假期不設服務

<!-- end list -->

  - 960X 洪水橋（洪元路）開：

<!-- end list -->

  - 星期一至五：06:50、07:00、07:12、07:24、07:36

<!-- end list -->

  -
    星期六、日及公眾假期不設服務

## 收費

  - 960A

<!-- end list -->

  - 全程：$20.8
      - 過[西區海底隧道後往](../Page/西區海底隧道.md "wikilink")[洪水橋](../Page/洪水橋.md "wikilink")（[洪福邨](../Page/洪福邨.md "wikilink")）：$15.2
      - [屯門市中心往洪水橋](../Page/屯門市中心.md "wikilink")（洪福邨）：$5

<!-- end list -->

  - 960B/960X

<!-- end list -->

  - 全程：$23.4
      - 過[東區走廊後往](../Page/東區走廊.md "wikilink")[洪水橋](../Page/洪水橋.md "wikilink")（[洪福邨](../Page/洪福邨.md "wikilink")）：$20.8
      - 過西區海底隧道後往洪水橋（洪福邨）：$15.2
      - 屯門市中心往洪水橋（洪福邨）：$5
      - [西區海底隧道收費廣場往鰂魚涌](../Page/西區海底隧道.md "wikilink")（[英皇道](../Page/英皇道.md "wikilink")）：$10.6
      - 過西區海底隧道後往鰂魚涌（[英皇道](../Page/英皇道.md "wikilink")）：$6.5

### 八達通轉乘優惠

  - 960A/960B→九巴屯門公路轉車站轉乘計劃路線

<!-- end list -->

  - **960A/960B往洪水橋** →
    52X、57M、58M、59M、59X、60M、60X、61M、66M、66X、67M、67X、258D、259D、260X、263往屯門，次程車資全免
  - **960A/960B往洪水橋** → 53、68A往元朗，次程補車資$0.6

## 派車

960A線主要是從[67M線抽調](../Page/九龍巴士67M線.md "wikilink")1部車行駛，用車不定。

而行駛960B/X線的車輛則由59M、67M、263線抽調行走，以[E500
MMC為主](../Page/亞歷山大丹尼士Enviro_500_MMC.md "wikilink")。

## 行車路線

  - 960A/960B

**經**：*[英皇道](../Page/英皇道.md "wikilink")、[東區走廊](../Page/東區走廊.md "wikilink")、維園道、告士打道、夏慤道*、-{[干諾道中](../Page/干諾道中.md "wikilink")}-、[林士街天橋](../Page/林士街.md "wikilink")、[西區海底隧道](../Page/西區海底隧道.md "wikilink")、[西九龍公路](../Page/西九龍公路.md "wikilink")、[青葵公路](../Page/青葵公路.md "wikilink")、[長青隧道](../Page/長青隧道.md "wikilink")、[長青公路](../Page/長青公路.md "wikilink")、[青衣西北交匯處](../Page/青衣西北交匯處.md "wikilink")、[青朗公路](../Page/青朗公路.md "wikilink")、[屯門公路](../Page/屯門公路.md "wikilink")、[屯門公路巴士轉乘站](../Page/屯門公路巴士轉乘站.md "wikilink")、屯門公路、[屯喜路](../Page/屯喜路.md "wikilink")、屯門公路、[青田路](../Page/青田路.md "wikilink")、[田景路](../Page/田景路.md "wikilink")、[鳴琴路](../Page/鳴琴路.md "wikilink")、[震寰路](../Page/震寰路.md "wikilink")、[青麟路](../Page/青麟路.md "wikilink")、[藍地交匯處](../Page/藍地交匯處.md "wikilink")、[青山公路](../Page/青山公路.md "wikilink")（嶺南段）、[屯貴路](../Page/屯貴路.md "wikilink")、青山公路（嶺南段、藍地段及洪水橋段）、[洪天路](../Page/洪天路.md "wikilink")、[洪水橋田心路](../Page/洪水橋田心路.md "wikilink")、[洪元路及洪水橋田心路](../Page/洪元路.md "wikilink")。

  -
    *斜體字*之路段祇限960B線駛經

  - 960X

**經**：[洪元路](../Page/洪元路.md "wikilink")、[洪志路](../Page/洪志路.md "wikilink")、[洪天路](../Page/洪天路.md "wikilink")、[青山公路](../Page/青山公路.md "wikilink")（洪水橋段、藍地段）、[藍地交匯處](../Page/藍地交匯處.md "wikilink")、[屯門公路](../Page/屯門公路.md "wikilink")、[青田路](../Page/青田路.md "wikilink")、[鳴琴路](../Page/鳴琴路.md "wikilink")、[田景路](../Page/田景路.md "wikilink")、青田路、屯門公路、[屯門公路巴士轉乘站](../Page/屯門公路巴士轉乘站.md "wikilink")、屯門公路、[青朗公路](../Page/青朗公路.md "wikilink")、[青衣西北交匯處](../Page/青衣西北交匯處.md "wikilink")、[長青公路](../Page/長青公路.md "wikilink")、[長青隧道](../Page/長青隧道.md "wikilink")、[青葵公路](../Page/青葵公路.md "wikilink")、[西九龍公路](../Page/西九龍公路.md "wikilink")、[西區海底隧道](../Page/西區海底隧道.md "wikilink")、[干諾道西](../Page/干諾道西.md "wikilink")、[林士街天橋](../Page/林士街天橋.md "wikilink")、[民寶街](../Page/民寶街.md "wikilink")、[民光街](../Page/民光街.md "wikilink")、[民耀街](../Page/民耀街.md "wikilink")、[龍和道](../Page/龍和道.md "wikilink")、[添華道](../Page/添華道.md "wikilink")、[夏慤道](../Page/夏慤道.md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[維園道](../Page/維園道.md "wikilink")、[東區走廊](../Page/東區走廊.md "wikilink")、[渣華道及](../Page/渣華道.md "wikilink")[英皇道](../Page/英皇道.md "wikilink")。

### 沿線車站

  - 960A/B

[960ABRtMap.png](https://zh.wikipedia.org/wiki/File:960ABRtMap.png "fig:960ABRtMap.png")

| [鰂魚涌](../Page/鰂魚涌.md "wikilink")（960B）／[中環](../Page/中環.md "wikilink")（[中華總商會大廈](../Page/中華總商會.md "wikilink")）（960A）開 |
| -------------------------------------------------------------------------------------------------------------------- |
| **序號**                                                                                                               |
| 1\*                                                                                                                  |
| 2\*                                                                                                                  |
| 3\*                                                                                                                  |
| 4                                                                                                                    |
| 5                                                                                                                    |
| 6                                                                                                                    |
| 7                                                                                                                    |
| 8                                                                                                                    |
| 9                                                                                                                    |
| 10                                                                                                                   |
| 11                                                                                                                   |
| 12                                                                                                                   |
| 13                                                                                                                   |
| 14                                                                                                                   |
| 15                                                                                                                   |
| 16                                                                                                                   |
| 17                                                                                                                   |
| 18                                                                                                                   |
| 19                                                                                                                   |
| 20                                                                                                                   |
| 21                                                                                                                   |
| 22                                                                                                                   |
| 23                                                                                                                   |
| 24                                                                                                                   |
| 25                                                                                                                   |
| 26                                                                                                                   |
| 27                                                                                                                   |

  -
    有 \* 號之車站祇限960B線停靠

  - 960X

[960XRtMap.png](https://zh.wikipedia.org/wiki/File:960XRtMap.png "fig:960XRtMap.png")

| [洪水橋](../Page/洪水橋.md "wikilink")（[洪元路](../Page/洪元路.md "wikilink")）開 |
| ------------------------------------------------------------------- |
| **序號**                                                              |
| 1                                                                   |
| 2                                                                   |
| 3                                                                   |
| 4                                                                   |
| 5                                                                   |
| 6                                                                   |
| 7                                                                   |
| 8                                                                   |
| 9                                                                   |
| 10                                                                  |
| 11                                                                  |
| 12                                                                  |
| 13                                                                  |
| 14                                                                  |
| 15                                                                  |
| 16                                                                  |
| 17                                                                  |
| 18                                                                  |
| 19                                                                  |
| 20                                                                  |
| 21                                                                  |
| 22                                                                  |
| 23                                                                  |
| 24                                                                  |

## 紀錄

960B開辦後創下多項紀錄：

1.  首批港島區總站為銅鑼灣以東的非機場西隧過海巴士路線之一
2.  首批由港島東直達新界西，且途經東區走廊的常規過海隧道巴士路線之一（另2條為同日開辦的962C及969C）
3.  第2條由港島單向開往[元朗](../Page/元朗.md "wikilink")，中途需在屯門區停站的過海隧巴路線（第一條為960A）
4.  繼1998年7月6日機場[通天巴士A5線因機場搬遷至赤鱲角而停辦後](../Page/九巴通天巴士A5線.md "wikilink")，九巴站牌再次於北角及鰂魚涌一帶出現。

## 參考文獻

  - 《巴士路線發展綱要(2)──荃灣．屯門．元朗》〈960系線〉，ISBN 9628414720003

## 外部連結

  - [九巴960A線官方網站路線資料](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=960A)
  - [九巴960B線官方網站路線資料](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=960B)
  - [Cross Harbour Route
    過海路線－960A](http://www.681busterminal.com/960a.html)
  - [過海路線－960B](http://www.681busterminal.com/960b.html)
  - [i-busnet.com－Harbour
    Rt. 960A](http://www.i-busnet.com/busroute/harbour/harbourr960a.php)
  - [i-busnet.com－Harbour
    Rt. 960B](http://www.i-busnet.com/busroute/harbour/harbourr960b.php)
  - [九巴960X線官方網站路線資料](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=960X)
  - [1](http://681busterminal.com/960x.html)

[960A](../Category/九龍巴士路線.md "wikilink")
[960A](../Category/過海隧道巴士路線.md "wikilink")
[960A](../Category/中西區巴士路線.md "wikilink")
[960B](../Category/香港東區巴士路線.md "wikilink")
[960A](../Category/元朗區巴士路線.md "wikilink")

1.  [九巴新聞稿：《九巴開辦960X及968X線》](http://www.kmb.hk/tc/news/press/archives/news201208021730.html)
2.  [九巴乘客通告](http://www.kmb.hk/tc/news/realtimenews.html?page=1433210035_2841_0.jpg)