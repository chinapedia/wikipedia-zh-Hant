**過海隧道巴士960C線**和**過海隧道巴士960S線**都是香港一條途經[西區海底隧道巴士路線](../Page/西區海底隧道.md "wikilink")，兩者皆由[屯門](../Page/屯門.md "wikilink")（[富泰邨](../Page/富泰邨.md "wikilink")）單-{向}-開往[灣仔北](../Page/灣仔北.md "wikilink")，由九巴獨自經營。

## 歷史

  - 2003年4月7日：**960S線**投入服務。本路線只在平日上午於富泰開出5班， 方便屯門東北居民前往港島上班。
  - 2004年5月24日：因應兆康居民需求，繞經[兆康站](../Page/兆康站_\(西鐵綫\).md "wikilink")。
  - 2013年9月28日：加停屯門公路巴士轉乘站，並與其他路線提供轉乘優惠。
  - 2015年5月17日：本線港島區總站由灣仔碼頭遷移往[灣仔北公共運輸交匯處](../Page/灣仔北公共運輸交匯處.md "wikilink")，以配合[灣仔碼頭公共運輸交匯處因](../Page/灣仔碼頭公共運輸交匯處.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[沙中綫工程而封閉](../Page/沙中綫.md "wikilink")\[1\]。
  - 2018年[4月16日](../Page/4月16日.md "wikilink")：**960C線**投入服務。本路線只在平日上午於富泰開出1班，
    方便[兆康苑](../Page/兆康苑.md "wikilink")、[建生邨](../Page/建生邨.md "wikilink")、[田景邨](../Page/田景邨.md "wikilink")、[良景邨及](../Page/良景邨.md "wikilink")[屯門市中心居民前往港島上班](../Page/屯門市中心.md "wikilink")。

## 服務時間及班次

  - 富泰邨開

<!-- end list -->

  - 星期一至六：07:10、**07:15**、07:20、07:30、07:40、07:50、08:00

<!-- end list -->

  -
    星期日及公眾假期不設服務
    **粗體字**班次為960C線

## 收費

全程：$20.8

  - [西區海底隧道收費廣場往](../Page/西區海底隧道.md "wikilink")[灣仔北](../Page/灣仔北.md "wikilink")：$9.8
  - 過西區海底隧道後往灣仔北：$5.7

### 八達通轉乘優惠

乘客登上本線後150分鐘內以同一張八達通卡轉乘以下路線，或從以下路線登車後150分鐘內以同一張八達通卡轉乘本線，次程可獲車資折扣優惠：

  - **960S往灣仔北** ←→
    904、905、914/914X、930/930A/930X、934、935、936、948/948/948X、961/961P、962/962A/962B/962P/962S/962X、967/967X、968/968X、969/969A/969B/969P/969X、970/970X、971、973、978/978A/978B、982X或E11往港島，於西區海底隧道收費廣場轉乘，次程可獲$5折扣優惠

<!-- end list -->

  - **960S往灣仔北** → 968往銅鑼灣，於過西區海底隧道後轉乘，次程車資全免

<!-- end list -->

  - 960S←→九巴屯門公路轉車站轉乘計劃路線

<!-- end list -->

  - 52X、59X、60X、67X往九龍 → **960S往灣仔北**，次程補車資$8
  - 53往荃灣西站 → **960S往灣仔北**，次程補車資$10.7（富泰邨前登車）／$12.1（富泰邨或之後登車）
  - 57M、58M或67M往葵涌 → **960S往灣仔北**，次程補車資$11.4
  - 59M、60M或66M往荃灣 → **960S往灣仔北**，次程補車資$12.4
  - 61M往荔景 → **960S往灣仔北**，次程補車資$11.7
  - 61X往九龍城碼頭 → **960S往灣仔北**，次程補車資$6.9
  - 63X往佐敦（渡華路） → **960S往灣仔北**，次程補車資$6.6
  - 68A往青衣站 → **960S往灣仔北**，次程補車資$10.3（元朗至藍地登車）／$11.1（虎地或之後登車）
  - 258D、259D往九龍 → **960S往灣仔北**，次程補車資$4.2
  - 260X往紅磡站 → **960S往灣仔北**，次程補車資$5.7
  - 263往沙田站 → **960S往灣仔北**，次程補車資$6
  - N260往美孚 → **960S往灣仔北**，次程補車資$9.7
  - **960S往灣仔北** →
    52X、53、57M、58M、59M、59X、60M、60X、61M、63X、66M、67M、67X、68A、258D、259D、260X、263、961/961P或N260往市區，次程車資全免
  - **960S往灣仔北** → E33往機場，次程補車資$9.9

## 用車

本線主要是由[66M](../Page/九龍巴士66M線.md "wikilink")、[66X及](../Page/九龍巴士66X線.md "wikilink")[960抽調車輛行走](../Page/過海隧道巴士960線.md "wikilink")。

## 行車路線

**經**：[屯貴路](../Page/屯貴路.md "wikilink")、、*青山公路—新墟段*、*[兆康站（南）公共運輸交匯處](../Page/兆康站（南）公共運輸交匯處.md "wikilink")*、*青山公路—新墟段*、*青山公路—青山灣段*、*[屯興路](../Page/屯興路.md "wikilink")*、（[藍地交匯處](../Page/藍地交匯處.md "wikilink")、[青麟路](../Page/青麟路.md "wikilink")、[震寰路](../Page/震寰路.md "wikilink")、[鳴琴路](../Page/鳴琴路.md "wikilink")、[田景路](../Page/田景路.md "wikilink")、[青田路](../Page/青田路.md "wikilink")、[屯門公路](../Page/屯門公路.md "wikilink")、[屯發路](../Page/屯發路.md "wikilink")、）屯門公路、[屯門公路巴士轉乘站](../Page/屯門公路巴士轉乘站.md "wikilink")、屯門公路、[青朗公路](../Page/青朗公路.md "wikilink")、[青衣西北交匯處](../Page/青衣西北交匯處.md "wikilink")、[長青公路](../Page/長青公路.md "wikilink")、[長青隧道](../Page/長青隧道.md "wikilink")、[青葵公路](../Page/青葵公路.md "wikilink")、[西九龍公路](../Page/西九龍公路.md "wikilink")、[西區海底隧道](../Page/西區海底隧道.md "wikilink")、[干諾道西](../Page/干諾道西.md "wikilink")、[干諾道中](../Page/干諾道中.md "wikilink")、[民吉街](../Page/民吉街.md "wikilink")、[統一碼頭道](../Page/統一碼頭道.md "wikilink")、民吉街、干諾道中、[紅棉路支路](../Page/紅棉路.md "wikilink")、[金鐘道](../Page/金鐘道.md "wikilink")、[軒尼詩道](../Page/軒尼詩道.md "wikilink")、[菲林明道](../Page/菲林明道.md "wikilink")、天橋、菲林明道、[會議道及](../Page/會議道.md "wikilink")[鴻興道](../Page/鴻興道.md "wikilink")。

  -
    960S線駛經斜體字之路段，960C線駛經括號內之路段。

### 沿線車站

[Crossharbor960RtMap.png](https://zh.wikipedia.org/wiki/File:Crossharbor960RtMap.png "fig:Crossharbor960RtMap.png")
[Crossharbor960C_RtMap.png](https://zh.wikipedia.org/wiki/File:Crossharbor960C_RtMap.png "fig:Crossharbor960C_RtMap.png")

| [屯門](../Page/屯門.md "wikilink")（[富泰邨](../Page/富泰邨.md "wikilink")）開 |
| ----------------------------------------------------------------- |
| **序號**                                                            |
| 1                                                                 |
| 2                                                                 |
| 3\*                                                               |
| 4\*                                                               |
| 5\*                                                               |
| 6\*                                                               |
| 7\*                                                               |
| 8\*                                                               |
| 9\*                                                               |
| 10\*                                                              |
| \#                                                                |
| [五柳路](../Page/五柳路.md "wikilink")                                  |
| [兆康路](../Page/兆康路.md "wikilink")                                  |
| [兆康苑兆賢閣](../Page/兆康苑.md "wikilink")                               |
| [屯門醫院](../Page/屯門醫院.md "wikilink")                                |
| [寶怡花園](../Page/寶怡花園.md "wikilink")                                |
| [盈豐園](../Page/盈豐園.md "wikilink")                                  |
| [田景邨田樂樓](../Page/田景邨.md "wikilink")                               |
| 田景邨田裕樓                                                            |
| [建生邨樂生樓](../Page/建生邨.md "wikilink")                               |
| [紅橋](../Page/紅橋_\(香港\).md "wikilink")                             |
| [屯仁街](../Page/屯仁街.md "wikilink")                                  |
| [華都花園](../Page/華都花園.md "wikilink")                                |
| 11                                                                |
| 12                                                                |
| 13                                                                |
| 14                                                                |
| 15                                                                |
| 16                                                                |
| 17                                                                |
| 18                                                                |
| 19                                                                |
| 20                                                                |

  -
    註：

<!-- end list -->

  - \* 號之車站祇適用於960S線
  - \# 號之車站祇適用於960C線

## 外部連結

  - 《二十世紀巴士路線發展史－－渡海、離島及機場篇》，ISBN 9789628414680，BSI (香港)，81頁，87頁
  - [九巴網站－960S資料](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=960S)
  - [960S路線介紹](http://www.681busterminal.com/960s.html)
  - [i-busnet.com－隧巴960S號路線 Harbour
    Rt. 960S](http://www.i-busnet.com/busroute/harbour/harbourr960s.php)

[960S](../Category/九龍巴士路線.md "wikilink")
[960S](../Category/過海隧道巴士路線.md "wikilink")
[960S](../Category/屯門區巴士路線.md "wikilink")
[960S](../Category/灣仔區巴士路線.md "wikilink")

1.  [九巴乘客通告](http://www.kmb.hk/tc/news/realtimenews.html?page=1431074819_9115_0.jpg)