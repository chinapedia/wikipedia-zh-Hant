**過海隧道巴士971線**是[香港一條由](../Page/香港.md "wikilink")[新世界第一巴士獨營的](../Page/新世界第一巴士.md "wikilink")[西區海底隧道巴士路線](../Page/西區海底隧道.md "wikilink")，來往[香港仔](../Page/香港仔.md "wikilink")（[石排灣](../Page/石排灣邨.md "wikilink")）及西九龍新區的[海麗邨](../Page/海麗邨.md "wikilink")。本線是新巴投得五條西九龍新區路線專營權當中，亦是唯一一條服務[域多利道一帶的全日過海隧道巴士路線](../Page/域多利道.md "wikilink")。\[1\]

**過海隧道巴士971R線**為[香港一條只在特定日子服務的過海隧道巴士路線](../Page/香港.md "wikilink")，以循環形式運作，並以[旺角為循環點](../Page/旺角.md "wikilink")，來往[數碼港至旺角](../Page/數碼港.md "wikilink")，主要服務對象為[掃墓人士](../Page/掃墓.md "wikilink")。

## 歷史

  - 2003年12月16日，配合九廣西鐵（現稱[港鐵](../Page/港鐵.md "wikilink")[西鐵綫](../Page/西鐵綫.md "wikilink")）即將通車而投入服務，來往[香港仔](../Page/香港仔.md "wikilink")[石排灣及](../Page/石排灣邨.md "wikilink")[南昌站](../Page/南昌站_\(香港\).md "wikilink")，並設有[九廣西鐵轉乘優惠](../Page/九廣西鐵.md "wikilink")。
  - 2006年1月8日，配合海麗邨落成，總站由南昌站延長至[海麗邨](../Page/海麗邨.md "wikilink")。
  - 2007年12月1日，因[兩鐵合併](../Page/兩鐵合併.md "wikilink")，原有九廣西鐵之轉乘優惠取消。
  - 2009年3月29日，971R線開辦
  - 2010年7月3日，本線往石排灣及海麗邨方向不再途經[亞皆老街與](../Page/亞皆老街.md "wikilink")[佐敦道之間一段的](../Page/佐敦道.md "wikilink")[上海街及](../Page/上海街.md "wikilink")[廣東道](../Page/廣東道.md "wikilink")／[新填地街](../Page/新填地街.md "wikilink")，改為途經[彌敦道](../Page/彌敦道.md "wikilink")，而服務時間、班次及收費則維持不變。該線改經彌敦道後，[眾坊街至亞皆老街一段的新填地街再也沒有任何日間巴士路線行走](../Page/眾坊街.md "wikilink")。\[2\]
  - 2012年9月29日：南昌站公共運輸交匯處因物業發展而停用，本線往石排灣方向不再繞經南昌站公共運輸交匯處\[3\]。
  - 2014年12月22日，往海麗邨方向加停深旺道[聚魚道分站](../Page/聚魚道.md "wikilink")\[4\]。
  - 2017年12月15日， 增設實時抵站時間
  - 2018年3月24日：增設[城巴E21線往大角咀方向及本線往](../Page/城巴E21線.md "wikilink")[海麗邨方向的免費轉乘優惠](../Page/海麗邨.md "wikilink")。\[5\]

## 服務時間及班次

| [石排灣開](../Page/石排灣邨.md "wikilink") | [海麗邨開](../Page/海麗邨.md "wikilink") |
| ---------------------------------- | --------------------------------- |
| **日期**                             | **服務時間**                          |
| 星期一至五                              | 06:30-08:10                       |
| 08:35                              | 08:30-09:30                       |
| 08:55-10:10                        | 15                                |
| 10:10-11:30                        | 20                                |
| 11:30-15:30                        | 30                                |
| 15:30-20:30                        | 20                                |
| 20:30-22:00                        | 30                                |
| 星期六                                | 06:30-20:30                       |
| 20:30-22:00                        | 30                                |
| 星期日及公眾假期                           | 06:30-14:30                       |
| 14:30-15:30                        | 20                                |
| 15:30-17:00                        | 30                                |
| 17:00-18:00                        | 20                                |
| 18:30-19:30                        | 20                                |
| 19:30-22:00                        | 30                                |

<div class="NavFrame collapsed" style="clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

詳細班次

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

| [海麗邨開](../Page/海麗邨.md "wikilink") |
| --------------------------------- |
| 星期一至五                             |
| 06:30                             |
| 09:10                             |
| 11:30                             |
| 15:30                             |
| 18:10                             |
| 21:00                             |
| 星期六                               |
| 06:30                             |
| 09:10                             |
| 11:50                             |
| 14:30                             |
| 17:10                             |
| 19:50                             |
| 星期日及公眾假期                          |
| 06:30                             |
| 10:30                             |
| 14:30                             |
| 17:40                             |
| 21:00                             |

| [石排灣邨開](../Page/石排灣邨.md "wikilink") |
| ----------------------------------- |
| 星期一至五                               |
| 06:30                               |
| 08:15                               |
| 10:45                               |
| 14:15                               |
| 16:50                               |
| 19:10                               |
| 21:30                               |
| 星期六                                 |
| 06:30                               |
| 08:50                               |
| 11:10                               |
| 13:30                               |
| 15:50                               |
| 18:10                               |
| 20:30                               |
| 星期六、日及公眾假期                          |
| 07:35                               |
| 10:45                               |
| 14:15                               |
| 17:45                               |
| 21:15                               |

</div>

</div>

## 收費

971

全程：$11.7

  - [沙宣道往](../Page/沙宣道.md "wikilink")[海麗邨](../Page/海麗邨.md "wikilink")：$10.4
  - [西寧街往海麗邨](../Page/西寧街.md "wikilink")：$9.8
  - [西區海底隧道收費廣場往海麗邨](../Page/西區海底隧道.md "wikilink")：$6.1
  - [山道往](../Page/山道.md "wikilink")[香港仔（石排灣）](../Page/石排灣邨.md "wikilink")：$6.9
  - [香港大學李嘉誠醫學院往香港仔](../Page/香港大學李嘉誠醫學院.md "wikilink")（石排灣）：$4.8

971R

全程：$15.4

### 八達通轉乘優惠

  - **971往海麗邨** → A10往機場，回贈首程車資，轉乘時限為90分鐘
  - A10往鴨脷洲邨 → **971往石排灣**，次程車資全免，轉乘時限為120分鐘
  - E21往大角咀 → **971往海麗邨**，次程車資全免
  - 971**往海麗邨**→20往大角咀,次程車資全免。
  - 971**往海麗邨**→20往啟德,次程車資全免。

## 使用車輛

現時行走971線的車輛多為[丹尼士三叉戟三型](../Page/丹尼士三叉戟三型.md "wikilink")12米（12\*\*）、B9TL
11.3米（45\*\*）、Enviro500 (MMC)
12米（5500-5582／5584-5719）及11.3米（40\*\*）行駛。其中一輛被披上西九龍中心全車身廣告的富豪B9TL
11.3米（4511／TP165）獲安排固定行走本線。

## 行車路線

**石排灣開**經：[漁光道](../Page/漁光道.md "wikilink")、[香港仔水塘道](../Page/香港仔水塘道.md "wikilink")、[香港仔大道](../Page/香港仔大道.md "wikilink")、天橋、[香港仔海旁道](../Page/香港仔海旁道.md "wikilink")、[石排灣道](../Page/石排灣道.md "wikilink")、[薄扶林道](../Page/薄扶林道.md "wikilink")、[域多利道](../Page/域多利道.md "wikilink")、[加多近街](../Page/加多近街.md "wikilink")、[吉席街](../Page/吉席街.md "wikilink")、[堅彌地城海傍](../Page/堅彌地城海傍.md "wikilink")、[德輔道西](../Page/德輔道西.md "wikilink")、[水街](../Page/水街.md "wikilink")、[西區海底隧道](../Page/西區海底隧道.md "wikilink")、[佐敦道](../Page/佐敦道.md "wikilink")、[彌敦道](../Page/彌敦道.md "wikilink")、[亞皆老街](../Page/亞皆老街.md "wikilink")、[櫻桃街](../Page/櫻桃街.md "wikilink")、隧道、櫻桃街、[海輝道](../Page/海輝道.md "wikilink")、[深旺道及](../Page/深旺道.md "wikilink")[海麗街](../Page/海麗街.md "wikilink")。

**海麗邨開**經：深旺道、櫻桃街、[大角咀道](../Page/大角咀道.md "wikilink")、櫻桃街、亞皆老街、新填地街、旺角道、彌敦道、佐敦道、天橋、西區海底隧道、干諾道西、[嘉安街](../Page/嘉安街.md "wikilink")、德輔道西、堅彌地城海傍、山市街、卑路乍街、域多利道、薄扶林道、石排灣道、[田灣街](../Page/田灣街.md "wikilink")、[田灣山道](../Page/田灣山道.md "wikilink")、石排灣道、香港仔海旁道、香港仔大道、香港仔水塘道及漁光道。

### 沿線車站

  - 971

[CrossHarbor971RtMap.png](https://zh.wikipedia.org/wiki/File:CrossHarbor971RtMap.png "fig:CrossHarbor971RtMap.png")

| [石排灣開](../Page/石排灣邨.md "wikilink") | [海麗邨開](../Page/海麗邨.md "wikilink")                        |
| ---------------------------------- | -------------------------------------------------------- |
| **序號**                             | **車站名稱**                                                 |
| 1                                  | [石排灣](../Page/石排灣邨.md "wikilink")                        |
| 2                                  | [漁暉道](../Page/漁暉道.md "wikilink")                         |
| 3                                  | [香港仔魚類批發市場](../Page/香港仔魚類批發市場.md "wikilink")             |
| 4                                  | [田灣街](../Page/田灣街.md "wikilink")                         |
| 5                                  | [華富道](../Page/華富道.md "wikilink")                         |
| 6                                  | [薄扶林消防局](../Page/香港消防局列表#西區分區_\(港島\).md "wikilink")      |
| 7                                  | [華翠街](../Page/華翠街.md "wikilink")                         |
| 8                                  | [舊薄扶林狗房](../Page/舊薄扶林狗房.md "wikilink")                   |
| 9                                  | [數碼港道](../Page/數碼港道.md "wikilink")                       |
| 10                                 | 上[碧瑤灣](../Page/碧瑤灣.md "wikilink")                        |
| 11                                 | 下碧瑤灣                                                     |
| 12                                 | [香港大學李嘉誠醫學院](../Page/香港大學李嘉誠醫學院.md "wikilink")           |
| 13                                 | [沙宣道](../Page/沙宣道.md "wikilink")                         |
| 14                                 | [碧荔道](../Page/碧荔道.md "wikilink")                         |
| 15                                 | [美景臺](../Page/美景臺.md "wikilink")                         |
| 16                                 | [香港華人基督教聯會薄扶林道墳場](../Page/香港華人基督教聯會薄扶林道墳場.md "wikilink") |
| 17                                 | [大口環道](../Page/大口環道.md "wikilink")                       |
| 18                                 | [西島中學](../Page/西島中學.md "wikilink")                       |
| 19                                 | [碧苑](../Page/碧苑.md "wikilink")                           |
| 20                                 | [摩星嶺徑](../Page/摩星嶺徑_\(巴士站\).md "wikilink")               |
| 21                                 | [明愛賽馬會宿舍](../Page/明愛賽馬會宿舍.md "wikilink")                 |
| 22                                 | [港島西廢物轉運站](../Page/港島西廢物轉運站.md "wikilink")               |
| 23                                 | [西寧街](../Page/西寧街.md "wikilink")                         |
| 24                                 | [西市街](../Page/西市街.md "wikilink")                         |
| 25                                 | [士美非路](../Page/士美非路.md "wikilink")                       |
| 26                                 | [西祥街](../Page/西祥街.md "wikilink")                         |
| 27                                 | [皇后大道西](../Page/皇后大道西.md "wikilink")                     |
| 28                                 | 山道                                                       |
| 29                                 | [均益大廈第二期](../Page/均益大廈.md "wikilink")                    |
| 30                                 | [水街](../Page/水街.md "wikilink")                           |
| 31                                 | 西區海底隧道收費廣場                                               |
| 32                                 | 柯士甸站                                                     |
| 33                                 | [上海街](../Page/上海街.md "wikilink")                         |
| 34                                 | [南京街](../Page/南京街_\(香港\).md "wikilink")                  |
| 35                                 | [眾坊街](../Page/眾坊街.md "wikilink")                         |
| 36                                 | 登打士街                                                     |
| 37                                 | [山東街](../Page/山東街.md "wikilink")                         |
| 38                                 | [旺角街市](../Page/旺角街市.md "wikilink")                       |
| 39                                 | 銘基書院                                                     |
| 40                                 | [浪澄灣](../Page/浪澄灣.md "wikilink")                         |
| 41                                 | [聚魚道](../Page/聚魚道.md "wikilink")                         |
| 42                                 | [南昌邨](../Page/南昌邨.md "wikilink")                         |
| 43                                 | 富昌邨                                                      |
| 44                                 | 海麗邨                                                      |
|                                    | 45                                                       |
| 46                                 | [崇文街](../Page/崇文街.md "wikilink")                         |
| 47                                 | [漁光村白沙樓](../Page/漁光村_\(香港\).md "wikilink")               |
| 48                                 | 漁光村海鷗樓                                                   |
| 49                                 | [扶康會康復中心](../Page/扶康會康復中心.md "wikilink")                 |
| 50                                 | [石排灣邨](../Page/石排灣邨.md "wikilink")                       |
| 51                                 | [漁暉苑日暉閣](../Page/漁暉苑.md "wikilink")                      |
| 52                                 | 石排灣                                                      |

  - 971R

| [數碼港方向](../Page/數碼港.md "wikilink") |
| ---------------------------------- |
| **車站**                             |
| 1                                  |
| 2                                  |
| 3                                  |
| 4                                  |
| 5                                  |
| 6                                  |
| 7                                  |
| 8                                  |
| 9                                  |
| 10                                 |
| 11                                 |
| 12                                 |
| 13                                 |
| 14                                 |
| 15                                 |
| 16                                 |
| 17                                 |
| 18                                 |
| 19                                 |
| 20                                 |
| 21                                 |
| 22                                 |
| 23                                 |
| 24                                 |
| 25                                 |
| 26                                 |
| 27                                 |
| 28                                 |
| 29                                 |
| 30                                 |
| 31                                 |
| 32                                 |
| 33                                 |
| 34                                 |
| 35                                 |
| 36                                 |
| 37                                 |

## 使用狀況

  - 由於[彌敦道巴士配額關係](../Page/彌敦道.md "wikilink")，因此本線開辦初期來回程於九龍區分別途經[上海街及](../Page/上海街.md "wikilink")[新填地街](../Page/新填地街.md "wikilink")，亦是繼[九龍巴士12線於](../Page/九龍巴士12線.md "wikilink")2000年12月17日起改經[富榮花園後](../Page/富榮花園.md "wikilink")，再次有巴士路線途經[眾坊街至](../Page/眾坊街.md "wikilink")[亞皆老街的一段新填地街](../Page/亞皆老街.md "wikilink")。不過由於遠離彌敦道人流集中的地帶，而上海街及新填地街入夜後人煙稀少，治安欠佳，因此晚間客量一直偏低。
  - 本線是唯一一條途經全條[域多利道的過海隧道巴士路線](../Page/域多利道.md "wikilink")，但沿線人口不多，因此平日非繁忙時間客量較低。不過由於可以直達[薄扶林](../Page/薄扶林.md "wikilink")[中華基督教墳場](../Page/中華基督教墳場.md "wikilink")，在春秋二祭期間客量會激增，因此會改派較大型巴士行走，和於2009年3月29日起開辦971R以分流[清明節及](../Page/清明節.md "wikilink")[重陽節期間來往](../Page/重陽節.md "wikilink")[掃墓的市民](../Page/掃墓.md "wikilink")，提供特快專線來往九龍區。
  - 為改善營運情況，於2010至2011年南區路線發展計劃中，建議本線來回程改經彌敦道，不再途經上海街及新填地街，以吸引乘客及分流原有乘搭[104號線來往堅尼地城至佐敦](../Page/過海隧道巴士104線.md "wikilink")、油麻地及旺角南的乘客改乘本線，建議已獲南區區議會通過\[6\]，但受油尖旺區議會反對\[7\]。經過一輪角力，本線改經彌敦道的建議終於在2010年7月3日起實施。改經彌敦道後的971出現龐大的客量增長，滿座次數亦逐漸增加（以往只限繁忙時間、春秋二祭）。

## 參考資料

## 外部連結

  - [新巴971線官方網站路線資料](https://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=971&route=971&routetype=D&company=7&exactMatch=yes)

[971](../Category/城巴及新世界第一巴士路線.md "wikilink")
[971](../Category/過海隧道巴士路線.md "wikilink")
[971](../Category/香港南區巴士路線.md "wikilink")
[971](../Category/深水埗區巴士路線.md "wikilink")

1.  [過海隧道巴士971線－681巴士總站](http://www.681busterminal.com/971.html)
2.  [新巴第971號線改經彌敦道](http://www.td.gov.hk/tc/traffic_notices/index_id_22138.html)
3.  [交通通告－因應永久封閉南昌站公共運輸交匯處的公共運輸安排](http://www.td.gov.hk/tc/traffic_notices/index_id_31137.html)，運輸署
4.  [新巴乘客通告](http://www.nwstbus.com.hk/en/uploadedFiles/evs_notice/C201402732a02.pdf)
5.  [新增八達通轉乘優惠](http://mobile.nwstbus.com.hk/pdf/E201800600a03.pdf)
6.  [2010-2011年度南區巴士路線發展計劃－香港交通資訊網](http://www.hkitalk.net/HKiTalk2/viewthread.php?tid=474115&extra=page%3D1)
7.  [2008至2011年度油尖旺區議會交通運輸委員會第13次會議記錄](http://www.districtcouncils.gov.hk/ytm_d/TTC%20minutes/TTC%20Minutes%202010/\(Font%2012\)_Minutes%20of%20TTC%20\(13th\)%20dd.%204.3.2010\(with%20amendment\).doc)（第141段）