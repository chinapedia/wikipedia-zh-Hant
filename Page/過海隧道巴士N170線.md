**過海隧道巴士N170線**是[香港一條來往](../Page/香港.md "wikilink")[沙田市中心及](../Page/沙田市中心.md "wikilink")[華富](../Page/華富邨.md "wikilink")（中）的通宵過海隧道巴士路線，由[九巴及](../Page/九龍巴士.md "wikilink")[城巴聯合經營](../Page/城巴.md "wikilink")，提供沙田市中心、[沙角邨](../Page/沙角邨.md "wikilink")/[博康邨](../Page/博康邨.md "wikilink")、[秦石邨](../Page/秦石邨.md "wikilink")、[大圍](../Page/大圍.md "wikilink")、[九龍塘及](../Page/九龍塘.md "wikilink")[何文田來往](../Page/何文田.md "wikilink")[銅鑼灣及](../Page/銅鑼灣.md "wikilink")[港島](../Page/港島.md "wikilink")[南區的通宵過海隧道巴士服務](../Page/南區_\(香港\).md "wikilink")。

## 歷史

  - 1996年8月19日：本線投入服務，城巴在其發展計劃中，計劃把旗下的過海隧道巴士線改為24小時服務，而[170線被列作試點之一](../Page/過海隧道巴士170線.md "wikilink")，後得運輸署批准而成功開辦，來往沙田火車站及華富（中），行車路線與日間的170線相約，但不經[培正道](../Page/培正道.md "wikilink")。
  - 1997年10月29日：華富（中）總站由呂明才中學對出遷往華富道[培英中學對面](../Page/香港培英中學.md "wikilink")。
  - 2001年3月18日：為方便市民在沙田轉車，本線總站遷往沙田市中心巴士總站\[1\]。
  - 2019年3月25日：華富總站由華富（二）邨商場遷至。\[2\]

## 服務時間（詳細班次）

| 每日[沙田市中心開](../Page/沙田市中心.md "wikilink") |
| --------------------------------------- |
| 00:00                                   |
| 02:00                                   |
| 04:00                                   |

| 每日開   |
| ----- |
| 00:00 |
| 02:00 |
| 04:00 |

## 收費

全程：$25.8

  - 過[獅子山隧道後往](../Page/獅子山隧道.md "wikilink")[華富](../Page/華富邨.md "wikilink")：$24.0
  - 過海底隧道後往華富：$10.9
  - 過香港仔隧道後往華富：$8.2
  - 過[厚德里後往](../Page/厚德里.md "wikilink")[沙田市中心](../Page/沙田市中心.md "wikilink")：$24.0
  - 過[海底隧道後往沙田市中心](../Page/紅磡海底隧道.md "wikilink")：$20.2
  - 過獅子山隧道後往沙田市中心：$9.8

## 使用車輛

現時九巴和城巴各派出3輛巴士行走本線。

## 行車路線

**[沙田市中心開](../Page/沙田市中心.md "wikilink")**經：[沙田正街](../Page/沙田正街.md "wikilink")、[橫壆街](../Page/橫壆街.md "wikilink")、[源禾路](../Page/源禾路.md "wikilink")、[沙田鄉事會路](../Page/沙田鄉事會路.md "wikilink")、[沙田圍路](../Page/沙田圍路.md "wikilink")、[沙角街](../Page/沙角街.md "wikilink")、[大涌橋路](../Page/大涌橋路.md "wikilink")、[車公廟路](../Page/車公廟路.md "wikilink")、[紅梅谷路](../Page/紅梅谷路.md "wikilink")、[獅子山隧道公路](../Page/獅子山隧道公路.md "wikilink")、[獅子山隧道](../Page/獅子山隧道.md "wikilink")、[窩打老道](../Page/窩打老道.md "wikilink")、[公主道](../Page/公主道.md "wikilink")、[康莊道](../Page/康莊道.md "wikilink")、[海底隧道](../Page/紅磡海底隧道.md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[維園道](../Page/維園道.md "wikilink")、天橋、告士打道、[波斯富街](../Page/波斯富街.md "wikilink")、[禮頓道](../Page/禮頓道.md "wikilink")、[摩利臣山道](../Page/摩利臣山道.md "wikilink")、[黃泥涌道](../Page/黃泥涌道.md "wikilink")、[香港仔隧道](../Page/香港仔隧道.md "wikilink")、[黃竹坑道](../Page/黃竹坑道.md "wikilink")、[香港仔大道](../Page/香港仔大道.md "wikilink")、[香港仔海旁道](../Page/香港仔海旁道.md "wikilink")、[石排灣道及](../Page/石排灣道.md "wikilink")[華富道](../Page/華富道.md "wikilink")。

**華富開**經：華富道、石排灣道、香港仔海旁道、香港仔大道、黃竹坑道、香港仔隧道、黃泥涌道、摩利臣山道、[天樂里](../Page/天樂里.md "wikilink")、[軒尼詩道](../Page/軒尼詩道.md "wikilink")、[怡和街](../Page/怡和街.md "wikilink")、[高士威道](../Page/高士威道.md "wikilink")、[興發街](../Page/興發街.md "wikilink")、維園道、告士打道、天橋、海底隧道、康莊道、公主道、窩打老道、獅子山隧道、獅子山隧道公路、紅梅谷路、車公廟路、大涌橋路、沙角街、[乙明邨街](../Page/乙明邨街.md "wikilink")、沙角街、沙田圍路、沙田鄉事會路及[大埔公路](../Page/大埔公路.md "wikilink")—沙田段。

### 沿線車站

[N170RtMap.png](https://zh.wikipedia.org/wiki/File:N170RtMap.png "fig:N170RtMap.png")

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/沙田市中心.md" title="wikilink">沙田市中心開</a></p></th>
<th><p>開</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>序號</strong></p></td>
<td><p><strong>車站名稱</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><a href="../Page/沙田市中心巴士總站.md" title="wikilink">沙田市中心巴士總站</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><a href="../Page/好運中心.md" title="wikilink">好運中心</a></p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="../Page/田園閣.md" title="wikilink">田園閣</a></p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><a href="../Page/博康邨.md" title="wikilink">博康邨</a></p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="../Page/救世軍田家炳學校.md" title="wikilink">田家炳學校</a></p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p><a href="../Page/秦石邨.md" title="wikilink">秦石邨</a></p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p><a href="../Page/沙田車公廟.md" title="wikilink">車公廟</a></p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p><a href="../Page/新翠邨.md" title="wikilink">新翠邨</a></p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p><a href="../Page/世界花園.md" title="wikilink">世界花園</a></p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p><a href="../Page/獅子山隧道.md" title="wikilink">獅子山隧道</a></p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p><a href="../Page/聯合道.md" title="wikilink">聯合道</a></p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p><a href="../Page/九龍塘站_(香港).md" title="wikilink">九龍塘站</a></p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p><a href="../Page/龍濤花園.md" title="wikilink">龍濤花園</a></p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p><a href="../Page/九龍醫院.md" title="wikilink">九龍醫院</a></p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p><a href="../Page/醫療輔助隊.md" title="wikilink">醫療輔助隊總部</a></p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p><a href="../Page/愛民邨.md" title="wikilink">愛民邨</a></p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p><a href="../Page/海底隧道_(香港).md" title="wikilink">海底隧道收費廣場</a></p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p><a href="../Page/景隆街.md" title="wikilink">景隆街</a></p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p><a href="../Page/利舞臺廣場.md" title="wikilink">利舞臺廣場</a></p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p><a href="../Page/紀利華木球會.md" title="wikilink">紀利華木球會</a></p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p><a href="../Page/跑馬地馬場.md" title="wikilink">跑馬地馬場</a></p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p>香港仔隧道收費廣場</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p><a href="../Page/香港仔運動場.md" title="wikilink">香港仔運動場</a></p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>業興街</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p><a href="../Page/南朗山道.md" title="wikilink">南朗山道</a></p></td>
</tr>
<tr class="odd">
<td><p>26</p></td>
<td><p><a href="../Page/勝利工廠大廈.md" title="wikilink">勝利工廠大廈</a></p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td><p><a href="../Page/逸港居.md" title="wikilink">逸港居</a></p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p><a href="../Page/香港仔海濱公園.md" title="wikilink">香港仔海濱公園</a></p></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p><a href="../Page/香港仔漁類批發市場.md" title="wikilink">香港仔漁類批發市場</a></p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td><p>田灣街</p></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td><p>華富邨華樂樓</p></td>
</tr>
<tr class="odd">
<td><p>32</p></td>
<td><p>華富商場</p></td>
</tr>
<tr class="even">
<td><p>33</p></td>
<td><p>華富邨華清樓</p></td>
</tr>
<tr class="odd">
<td><p>34</p></td>
<td><p>華富（中）</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>35</p></td>
</tr>
<tr class="odd">
<td><p>36</p></td>
<td><p><a href="../Page/沙角邨.md" title="wikilink">沙角邨</a></p></td>
</tr>
<tr class="even">
<td><p>37</p></td>
<td><p>田園閣</p></td>
</tr>
<tr class="odd">
<td><p>38</p></td>
<td><p>沙田市中心巴士總站</p></td>
</tr>
</tbody>
</table>

## 參考資料及注釋

  - 《二十世紀巴士路線發展史－－渡海、離島及機場篇》，容偉釗編著，BSI出版

## 外部連結

  - 九巴

<!-- end list -->

  - [過海隧道巴士N170線](http://www.kmb.hk/tc/services/search.html?busno=N170)

<!-- end list -->

  - 城巴

<!-- end list -->

  - [過海隧道巴士N170線](http://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=N170&route=N170&routetype=N&company=5&exactMatch=yes)
  - [過海隧道巴士N170線路線圖](http://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-CTB-N170-N170-N.pdf)

<!-- end list -->

  - 其他

<!-- end list -->

  - [過海隧道巴N170線－i-busnet.com](http://www.i-busnet.com/busroute/kmb/kmbrn170.php)
  - [過海隧道巴N170線－681巴士總站](http://www.681busterminal.com/n170.html)

[N170](../Category/九龍巴士路線.md "wikilink")
[N170](../Category/城巴及新世界第一巴士路線.md "wikilink")
[N170](../Category/沙田區巴士路線.md "wikilink")
[N170](../Category/香港南區巴士路線.md "wikilink")
[170](../Category/香港通宵巴士路線.md "wikilink")

1.  [681巴士總站](http://www.681busterminal.com/n170.html)
2.  [華富巴士總站永久遷移](http://mobile.nwstbus.com.hk/pdf/E201900747a03.pdf)