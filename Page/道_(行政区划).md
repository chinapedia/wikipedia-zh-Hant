**道**，是源於[中國的](../Page/中國.md "wikilink")[行政區劃单位](../Page/行政區劃.md "wikilink")，後成為[儒家文化圈國家普遍采用的行政區單位](../Page/儒家文化圈.md "wikilink")。

## 中國

### 起源

「道」這種行政區劃始創於[中國](../Page/中國.md "wikilink")。「道」在[漢朝開始出现](../Page/漢朝.md "wikilink")，起初跟[县同级别](../Page/县.md "wikilink")，专门使用于西南少数民族聚居的偏远地区的[益州刺史部](../Page/益州刺史部.md "wikilink")，《[汉书](../Page/汉书.md "wikilink")》地理志解释为“有蛮夷曰**道**”或者“县主蛮夷曰**道**”。

到了[隋](../Page/隋朝.md "wikilink")[唐时代](../Page/唐朝.md "wikilink")，出兵征战经常以方位路向加以命名，为“某某道”，该方面军主将称“某某道行军大总管”。例如攻打[高句丽时的](../Page/高句丽.md "wikilink")**平壤道**行军大总管、**辽东道**行军大总管、**浿江道**行军大总管等等。后来在内务民政上也使用此名词，在撤并郡县（或州县）之初，一级行政区较少时仅作为监察和地理单位，在后来州县增多之后逐渐成为实质性的一级行政区，由州－县二级制演变为道－州－县三级制。这种形式和演变过程，与20世纪的[法国相似](../Page/法国.md "wikilink")，法国起初把80多个省份按历史地理渊源划分为10几个“大区”，后来大区逐渐演变成为[省份的上级单位](../Page/省份.md "wikilink")。

### 中世

唐初，分天下为十道，为[州](../Page/州.md "wikilink")、[县之上的](../Page/县.md "wikilink")[一级行政区划](../Page/一级行政区划.md "wikilink")，之后迭有增加，至二十三道之多（[唐睿宗](../Page/唐睿宗.md "wikilink")[景云年间](../Page/景云.md "wikilink")）。但[唐玄宗皇帝的](../Page/唐玄宗.md "wikilink")[盛唐時期到](../Page/盛唐.md "wikilink")[安史之亂以後的](../Page/安史之亂.md "wikilink")[中晚唐](../Page/唐朝.md "wikilink")，[节度使逐漸掌握地方实权后](../Page/节度使.md "wikilink")，「道」也日渐演变为对一个节度使辖区的称呼，所到[中晚唐後期時](../Page/唐朝.md "wikilink")，甚至連[觀察使轄區也同節度使轄區一樣被稱為道](../Page/觀察使.md "wikilink")，已和[初唐](../Page/初唐.md "wikilink")、[盛唐时的意义有所不同](../Page/盛唐.md "wikilink")。如中晚唐時期，[李吉甫所編著的](../Page/李吉甫.md "wikilink")《[元和郡县志](../Page/元和郡縣圖志.md "wikilink")》一書，已將唐朝天下分為四十七道，即是包含了節度使與觀察使的轄區在內。
[贞观元年](../Page/贞观元年.md "wikilink")（627年），分天下为10道：

  - [关内道](../Page/关内道.md "wikilink")
  - [河南道](../Page/河南道.md "wikilink")
  - [河东道](../Page/河东道.md "wikilink")
  - [河北道](../Page/河北道.md "wikilink")
  - [山南道](../Page/山南道.md "wikilink")
  - [陇右道](../Page/陇右道.md "wikilink")
  - [淮南道](../Page/淮南道.md "wikilink")
  - [江南道](../Page/江南道.md "wikilink")
  - [剑南道](../Page/剑南道.md "wikilink")
  - [岭南道](../Page/岭南道.md "wikilink")

[开元二十一年](../Page/开元二十一年.md "wikilink")（733年），分天下为15道，即将山南道、江南道各分为东、西道，又增设了京畿道、都畿道和黔中道。此时全国設有：

  - 关内道
  - 河南道
  - 河东道
  - 河北道
  - [山南东道](../Page/山南东道.md "wikilink")
  - [山南西道](../Page/山南西道.md "wikilink")
  - 陇右道
  - 淮南道
  - [江南东道](../Page/江南东道.md "wikilink")
  - [江南西道](../Page/江南西道.md "wikilink")
  - 剑南道
  - 岭南道
  - [京畿道](../Page/京畿道_\(唐朝\).md "wikilink")
  - [都畿道](../Page/都畿道.md "wikilink")
  - [黔中道](../Page/黔中道.md "wikilink")

而[辽朝因循唐朝旧制](../Page/辽朝.md "wikilink")，仍使用「道」作为[一级行政区划](../Page/一级行政区划.md "wikilink")。[辽朝的行政区划大体为道](../Page/辽朝.md "wikilink")、[府](../Page/府.md "wikilink")（[州](../Page/州.md "wikilink")）、[县三级](../Page/县.md "wikilink")；辽全境分5个道，每个道有一个政治中心，称为“京”，并以京的名称来命名道；道下设府、州、[军](../Page/军.md "wikilink")、城4种政区，为同一级别。

  - [上京道](../Page/上京道.md "wikilink")
  - [中京道](../Page/中京道.md "wikilink")
  - [东京道](../Page/东京道.md "wikilink")
  - [南京道](../Page/南京道.md "wikilink")
  - [西京道](../Page/西京道.md "wikilink")

### 近世

[元朝建立後](../Page/元朝.md "wikilink")，[某某等处行中书省成為一级行政区划](../Page/某某等处行中书省.md "wikilink")，行省下設有道，道下有「[路](../Page/路_\(元朝行政區劃\).md "wikilink")」的行政区划单位。至[明](../Page/明朝.md "wikilink")[清時期](../Page/清朝.md "wikilink")，「道」成为[省之下军区的通称](../Page/省.md "wikilink")，如「[福建省](../Page/福建省.md "wikilink")[台廈道](../Page/台廈道.md "wikilink")」或「[台湾道](../Page/台湾道.md "wikilink")」，但明朝及清朝意义上稍有不同。也有人（例如《内政年鉴》）把清朝的道与省、府、县相提并论，称之为四级地方行政机构。

[民國](../Page/中華民國_\(大陸時期\).md "wikilink")[北洋政府時期](../Page/北洋政府.md "wikilink")，曾经在省下设「**道**」为次级行政区划，并曾計畫「廢省置道」，将全国划分为九十多个道。分為繁要缺、邊要缺、繁缺、邊缺、要缺、簡缺共6類三個等級。其中「繁要缺」是駐紮在省會的首道，地方形勢緊要，政務繁雜的道；「邊要缺」是地處邊陲，形勢緊要的的道；「繁缺」是轄縣較多、財政情況良好的道；「邊缺」是邊境地區或重要行政據點；「要缺」是境內轄有重要商埠的道；「簡缺」是轄縣較少、事務較簡、財政情況不佳的道。[京兆及稍後設置的](../Page/京兆.md "wikilink")[東省特別區](../Page/東省特別區.md "wikilink")，因政區特殊，均未設道。又[熱河](../Page/熱河.md "wikilink")、[察哈爾](../Page/察哈爾.md "wikilink")、[綏遠](../Page/綏遠.md "wikilink")、[川邊因縣級政區較少](../Page/川邊.md "wikilink")，均只各設1道。各省一般平均在3至4道，僅[甘肅及](../Page/甘肅.md "wikilink")[黑龍江省最多](../Page/黑龍江省.md "wikilink")，皆設置了7道。各道管轄縣數，一般在10至30縣之間，不過也存在了如黑龍江省[黑河道轄](../Page/黑河道.md "wikilink")3縣，[陝西省](../Page/陝西省.md "wikilink")[關中道轄](../Page/關中道.md "wikilink")40縣以上的道的特殊例子。

因為道制的等級與行政經費[預算的多少有關](../Page/預算.md "wikilink")，因此各省紛紛提出重評等級的要求，從民國三年（1914年）至四年（1915年）5月之間，[內務部先後批准](../Page/中華民國內務部.md "wikilink")[河南省](../Page/河南省_\(中華民國\).md "wikilink")[河洛道](../Page/河洛道.md "wikilink")、[江蘇省](../Page/江蘇省_\(中華民國\).md "wikilink")[徐海道](../Page/徐海道.md "wikilink")、[廣西省](../Page/廣西省_\(中華民國\).md "wikilink")[蒼梧道及](../Page/蒼梧道.md "wikilink")[鎮南道](../Page/鎮南道.md "wikilink")、[吉林省](../Page/吉林省_\(中華民國\).md "wikilink")[依蘭道的升等](../Page/依蘭道.md "wikilink")。同一時期內，[江蘇省](../Page/江蘇省_\(中華民國\).md "wikilink")[淮揚道](../Page/淮揚道.md "wikilink")、[浙江省](../Page/浙江省_\(中華民國\).md "wikilink")[甌海道](../Page/甌海道.md "wikilink")、[黑龍江省](../Page/黑龍江省_\(中華民國\).md "wikilink")[綏蘭道](../Page/綏蘭道.md "wikilink")、[廣東省](../Page/廣東省_\(中華民國\).md "wikilink")[潮循道](../Page/潮循道.md "wikilink")、[山東省](../Page/山東省_\(中華民國\).md "wikilink")[東臨道的升等要求](../Page/東臨道.md "wikilink")，因行政管轄區域不大，行政事務也不複雜，均被內務、財政兩部駁回。在1年半內，有如此多的道因經費問題而請求升等，引起了北洋政府的注意。內務部及[財政部於民國四年](../Page/中華民國財政部.md "wikilink")（1915年）6月大總統[袁世凱下令](../Page/袁世凱.md "wikilink")，今後若無特別的情況，不再重新評估核辦。但[地方军阀自行废置](../Page/民国军阀.md "wikilink")，譬如山东军阀[张宗昌将山东四道改为十一道](../Page/张宗昌.md "wikilink")\[1\]，

1927年，与北洋政府对峙的[國民政府宣布废除道制](../Page/國民政府.md "wikilink")。随着1928年[國民政府北伐胜利](../Page/國民政府北伐.md "wikilink")，[蔣介石通令由](../Page/蔣介石.md "wikilink")[省政府直接管辖](../Page/省政府.md "wikilink")[县](../Page/县.md "wikilink")，最终彻底废除道制\[2\]，但事實上，各省依然設置[行政督察區制度](../Page/行政督察區.md "wikilink")，亦相當於「道」，[蔣介石就曾經派他的長子](../Page/蔣介石.md "wikilink")[蔣經國擔任](../Page/蔣經國.md "wikilink")[江西省](../Page/江西省.md "wikilink")[贛南](../Page/贛南.md "wikilink")[行政督察區專員](../Page/行政督察區.md "wikilink")。

## 日本

[奈良時代](../Page/奈良時代.md "wikilink")（約7世紀）開始，日本分為5畿7道，後於1869年增設北海道。1872年，日本開始進行地方政區改革，至1889年後廢除其他7道，只保留北海道。

  - [五畿](../Page/五畿.md "wikilink")（畿內）
  - [東山道](../Page/東山道.md "wikilink")
  - [東海道](../Page/東海道.md "wikilink")
  - [北陸道](../Page/北陸道.md "wikilink")
  - [山陽道](../Page/山陽道.md "wikilink")
  - [山陰道](../Page/山陰道_\(日本\).md "wikilink")
  - [南海道](../Page/南海道.md "wikilink")
  - [西海道](../Page/西海道.md "wikilink")
  - [北海道](../Page/北海道.md "wikilink")

<!-- end list -->

  - （請參見[日本令制國列表](../Page/日本令制國列表.md "wikilink")）

## 朝鮮与韩国

約10世紀開始，當時的[高麗把全國分為八道](../Page/王氏高麗.md "wikilink")。這些道的名稱隨着道府所在地的變化而跟着變遷。直到[朝鮮王朝末年](../Page/朝鮮王朝.md "wikilink")。這八道的名稱是：

  - [咸镜道](../Page/咸镜道.md "wikilink")，后分为南、北两道
  - [黄海道](../Page/黄海道.md "wikilink")
  - [平安道](../Page/平安道.md "wikilink")，后分为南、北两道
  - [江原道](../Page/江原道.md "wikilink")，[韓戰後被](../Page/韓戰.md "wikilink")[停火线切分属于南北韓](../Page/三八線.md "wikilink")
  - [京畿道](../Page/京畿道.md "wikilink")
  - [忠清道](../Page/忠清道.md "wikilink")，后分为南、北两道
  - [庆尚道](../Page/庆尚道.md "wikilink")，后分为南、北两道
  - [全罗道](../Page/全罗道.md "wikilink")，后分为南、北两道

以上8个道称为**[朝鲜八道](../Page/朝鲜八道.md "wikilink")**，之後又把這八道再劃分為[13道](../Page/十三道制.md "wikilink")。这些名字沿用至今。


## 越南

[越南在唐朝时属于岭南道](../Page/越南.md "wikilink")，设[安南都督府](../Page/安南都督府.md "wikilink")，後改**[安南都護府](../Page/安南都護府.md "wikilink")**、[鎮南都護府](../Page/鎮南都護府.md "wikilink")，後又改回安南都護府。府下管辖12个州，[交州刺史充任](../Page/交州刺史.md "wikilink")[都护](../Page/都护.md "wikilink")，为长官。[五代十国时期](../Page/五代十国.md "wikilink")，[南汉设置静海节度使](../Page/南汉.md "wikilink")。

安南独立之初多沿袭唐朝、南汉旧制，[丁朝和](../Page/丁朝.md "wikilink")[前黎朝将辖区划分为十道](../Page/前黎朝.md "wikilink")，设将军，道下置州、府、县、社，而名称多不可考。[李朝时置](../Page/越南李朝.md "wikilink")24路，陈朝置12路或15路，[胡朝改路为镇](../Page/胡朝.md "wikilink")。

之後，[黎朝初期设](../Page/黎朝.md "wikilink")[东](../Page/东道.md "wikilink")、[西](../Page/西道.md "wikilink")、[南](../Page/南道.md "wikilink")、[北](../Page/北道.md "wikilink")、[海西等](../Page/海西道.md "wikilink")5道，后改为12承宣，分设3司，分别为[贊治承政使司](../Page/贊治承政使司.md "wikilink")（简称处）、[清刑宪察使司](../Page/清刑宪察使司.md "wikilink")（简称道）和[都总兵使司](../Page/都总兵使司.md "wikilink")（简称镇），道、镇也可用于指代行政区划。

[阮朝改为](../Page/阮朝.md "wikilink")27镇。[明命年间分设为](../Page/明命.md "wikilink")30省。[嗣德年间](../Page/嗣德.md "wikilink")，降[河静](../Page/河静省.md "wikilink")、[富安](../Page/富安省.md "wikilink")、[广治](../Page/广治省.md "wikilink")3省为道。嗣德后期又升为省，并增设[美德道](../Page/美德道.md "wikilink")、[新化道](../Page/新化道.md "wikilink")，主官为[管道](../Page/管道_\(越南\).md "wikilink")，增设[端雄道](../Page/端雄道.md "wikilink")、[谅江道](../Page/谅江道.md "wikilink")，主官为靖边副使。[同庆年间](../Page/同庆_\(阮朝\).md "wikilink")，增设[海宁道](../Page/海宁道.md "wikilink")。法属初期，殖民政府为镇压北圻的勤王运动，在北圻设置多处军区，越南人称作“官兵道”，兼管民政，主要有[荻林道](../Page/荻林道.md "wikilink")、[东潮道](../Page/东潮道.md "wikilink")、[澫慕道](../Page/澫慕道.md "wikilink")、[老街道](../Page/老街道.md "wikilink")、[安沛道](../Page/安沛道.md "wikilink")、[安边道](../Page/安边道.md "wikilink")、[永安道](../Page/永安道.md "wikilink")、[雅南道等](../Page/雅南道.md "wikilink")。1910年代，北圻局势稳定，官兵道固定为4处，分别是第一官兵道[海宁道](../Page/海宁道.md "wikilink")、第二官兵道[高平道](../Page/高平道.md "wikilink")、第三官兵道[河杨道和第四官兵道](../Page/河杨道.md "wikilink")[莱州道](../Page/莱州道.md "wikilink")。此外，殖民政府在今老挝设置第五官兵道（第五军区）。1920年代以后，官兵道也被称为省。

1900年代，殖民政府开始开发[西原](../Page/西原.md "wikilink")，在西原陆续设置5个省份，越南人称为道，分别是[崑嵩道](../Page/崑嵩道.md "wikilink")、[嘉莱道](../Page/嘉莱道.md "wikilink")、[得勒道](../Page/得勒道.md "wikilink")、[林园道和](../Page/林园道.md "wikilink")[同狔上道](../Page/同狔上道.md "wikilink")。此外，[平顺省](../Page/平顺省.md "wikilink")[宁顺府单独设立](../Page/宁顺府.md "wikilink")[宁顺道](../Page/宁顺道.md "wikilink")，法文称[潘郎省](../Page/潘郎省.md "wikilink")。

1945年越南独立后，北圻的官兵道和中圻的道均被统一为省。

## 注释

## 参考文献

  - 李國祁：〈[明清兩代地方行政制度中道的功能及其演變](http://www.docin.com/p-70235094.html)〉。

[\*](../Category/道.md "wikilink")
[Category:日本行政區劃](../Category/日本行政區劃.md "wikilink")
[Category:韩国行政区划单位](../Category/韩国行政区划单位.md "wikilink")
[Category:越南古代行政区划单位](../Category/越南古代行政区划单位.md "wikilink")
[Category:中国古代行政区划单位](../Category/中国古代行政区划单位.md "wikilink")
[Category:已不存在的國家的行政區劃](../Category/已不存在的國家的行政區劃.md "wikilink")
[Category:已不存在的行政领土实体](../Category/已不存在的行政领土实体.md "wikilink")

1.
2.