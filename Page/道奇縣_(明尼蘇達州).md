**道奇縣**（**Dodge County,
Minnesota**）是[美國](../Page/美國.md "wikilink")[明尼蘇達州東南部的一個縣](../Page/明尼蘇達州.md "wikilink")。面積1,139平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口19,355人。縣治[曼托維爾](../Page/曼托維爾.md "wikilink")
(Mantorville)。

成立於1855年2月22日，以紀念代表[州長](../Page/明尼蘇達州州長.md "wikilink")[亨利·道奇](../Page/亨利·道奇.md "wikilink")
(Henry Dodge)及其子[奧古斯都·凱撒·道奇](../Page/奧古斯都·凱撒·道奇.md "wikilink") (Augustus
Caesar Dodge)。

[D](../Category/明尼苏达州行政区划.md "wikilink")
[Category:1855年明尼蘇達領地建立](../Category/1855年明尼蘇達領地建立.md "wikilink")