**[道格拉斯DC](../Page/道格拉斯.md "wikilink")-2**是以全金屬機身，為一架14人座雙[發動機螺旋槳客機](../Page/發動機.md "wikilink")。DC-2係為12人座的[DC-1延長機身後之延伸型](../Page/DC-1.md "wikilink")。它的直接競爭對手是[波音247](../Page/波音247.md "wikilink")。1936年道格拉斯公司製造了更大更成功的飛機，為30人座的[DC-3](../Page/DC-3.md "wikilink")。

1920年代以[木材為材料的半硬殼設計使得飛機的結構具備更高的強度與載重](../Page/木材.md "wikilink")，在同一個時期各種[流線型降低阻力的設計和發動機出力的提升](../Page/流線型.md "wikilink")，使得民用航空公司期望以較低或者是相同的成本，取得載重量更高，飛行速度更快的機種。這個趨勢促使飛機公司增加金屬在使用材料的比例上。

[道格拉斯公司在設計出全金屬結構的](../Page/道格拉斯飛行器公司.md "wikilink")[DC-1之後](../Page/DC-1.md "wikilink")，將機身延長，加入兩個座位，同時引用當時最新的空氣動力與航空發動機設計的成果，這個結合的成品就是[DC-2客機](../Page/DC-2.md "wikilink")。

第一架DC-2於1934年5月11日進行試飛，成為道格拉斯公司首架商業化的機種。DC-2能夠搭載與[福特](../Page/福特.md "wikilink")**Trimotor**相同的乘客數目，以更高的平均速度飛行[泛洲及西方航空自](../Page/環球航空.md "wikilink")[紐約到](../Page/紐約.md "wikilink")[洛杉磯之間](../Page/洛杉磯.md "wikilink")18小時的航線。優異的性能讓DC-2很快受到美國航空公司的青睞。

1934年[荷兰皇家航空一架DC](../Page/荷兰皇家航空.md "wikilink")-2參加由倫敦到墨爾本，距離11300英里的MacRobertson獎盃競賽，在機上裝滿郵件與3位乘客的重量下取得第二名的成績，這個表現讓DC-2順利打開歐洲的市場，成為非常熱門的機種。

## 外部連結

  - [nationalmuseum.af.mil](https://web.archive.org/web/20081203111303/http://www.nationalmuseum.af.mil/shared/media/document/AFD-061222-016.pdf)
  - [DC-2
    Article](http://www.daveswarbirds.com/usplanes/aircraft/dc-2.htm)
  - [Centennial of Flight Commission on DC-1 and
    -2](https://web.archive.org/web/20020919230716/http://www.centennialofflight.gov/essay/Aerospace/Douglas-1930s/Aero28.htm)
  - [DC-2
    Article](http://www.rcaf.com/1939_1945_waryears/aircraft/transports/dc2/)
  - [DC-2 (cigarette
    cards)](https://web.archive.org/web/20051214114024/http://www.planefacts.co.uk/cards/douglas/pages/douglas_dc2_jpg.htm)

[Category:道格拉斯](../Category/道格拉斯.md "wikilink")
[Category:美國航空器](../Category/美國航空器.md "wikilink")