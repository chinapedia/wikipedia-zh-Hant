**道镜**（），[奈良时代僧人](../Page/奈良时代.md "wikilink")。俗姓弓削氏。河内国（在今[大阪府](../Page/大阪府.md "wikilink")）人。初从义渊学法相宗，后住[东大寺](../Page/东大寺.md "wikilink")，752年（天平胜宝三年）应[孝谦天皇招入宫中道场](../Page/孝谦天皇.md "wikilink")。761年（天平宝字五年）以看病禅师身份为女皇治病，受宠幸。764年受任大臣禅师，参与政事。765年（天平神户元年）又升任[太政大臣禅师](../Page/太政大臣.md "wikilink")。766年首次法王位。后因觊觎皇位，发生“宇佐神托事件”。[光仁天皇即位](../Page/光仁天皇.md "wikilink")，被贬为下野（今[栃木縣](../Page/栃木縣.md "wikilink")）药师寺[别当](../Page/别当.md "wikilink")，卒于此地。

## 參考

  - [《續日本紀》——卷三十　高野天皇　稱德天皇](https://web.archive.org/web/20060530151920/http://applepig.idv.tw/kuon/furu/text/syokki/syokki30.htm)

|-    |-

[Category:法相宗僧人](../Category/法相宗僧人.md "wikilink")
[Category:奈良時代僧人](../Category/奈良時代僧人.md "wikilink")
[Category:弓削氏](../Category/弓削氏.md "wikilink")
[Category:河內國出身人物](../Category/河內國出身人物.md "wikilink")