**達仁鄉**（[排灣語](../Page/排灣語.md "wikilink")：**Tadren**）位於[台灣](../Page/台灣.md "wikilink")[臺東縣南端](../Page/臺東縣.md "wikilink")，北臨[金峰鄉](../Page/金峰鄉_\(台灣\).md "wikilink")，東鄰[大武鄉](../Page/大武鄉_\(台灣\).md "wikilink")，東北連[太麻里鄉](../Page/太麻里鄉.md "wikilink")，東南濱[太平洋](../Page/太平洋.md "wikilink")，西鄰[屏東縣](../Page/屏東縣.md "wikilink")[來義鄉](../Page/來義鄉.md "wikilink")、[春日鄉及](../Page/春日鄉.md "wikilink")[獅子鄉](../Page/獅子鄉_\(台灣\).md "wikilink")，南接屏東縣[牡丹鄉](../Page/牡丹鄉.md "wikilink")。

本鄉地處[中央山脈南段](../Page/中央山脈.md "wikilink")，[大武山南麓](../Page/大武山.md "wikilink")，90%以上均為丘陵山坡地，[平原極少](../Page/平原.md "wikilink")，有[大武溪](../Page/大武溪.md "wikilink")、[安朔溪](../Page/安朔溪.md "wikilink")、[楓港溪等流經鄉境](../Page/楓港溪.md "wikilink")，氣候上屬[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")。

鄉內居民以[台灣原住民](../Page/臺灣原住民族.md "wikilink")[排灣族為主](../Page/排灣族.md "wikilink")，產業則以[農業為主](../Page/農業.md "wikilink")，地方通行語為[排灣語](../Page/排灣語.md "wikilink")\[1\]。

## 歷史

**達仁鄉**為排灣族原住民的活動範圍，在[清朝時期為](../Page/臺灣清朝時期.md "wikilink")[阿塱衛社的聚居地](../Page/阿塱衛社.md "wikilink")，其中以安朔村為最大，而「安朔」(阿朗壹
Aljungic)在排灣族語中便是指「最多人的社」。
[日治時期](../Page/台灣日治時期.md "wikilink")，[日本將本鄉原住民劃分為土哇巴樂](../Page/日本.md "wikilink")（土坂）、大板鹿（台坂）、大里力（森永）、就卡固來、阿塱衛（安朔）等五個社，先後劃歸[台東廳大武支廳](../Page/臺東廳.md "wikilink")、[台東郡管理](../Page/臺東郡.md "wikilink")，並在各社直接派駐[警察處理行政](../Page/警察.md "wikilink")、教育、衛生等日常政務。戰後之初劃歸[臺東縣](../Page/臺東縣.md "wikilink")[大武鄉](../Page/大武鄉_\(台灣\).md "wikilink")，1946年自[大武鄉分治](../Page/大武鄉_\(台灣\).md "wikilink")，當時官派之台東縣長建議鄉名為「大仁鄉」，可與[大武鄉保持有淵源關係之意涵](../Page/大武鄉_\(台灣\).md "wikilink")，但未被地方人士接納，認為「大」字難脫與[大武鄉的臍帶關係](../Page/大武鄉_\(台灣\).md "wikilink")，有無法獨立之感。當時首任官派鄉長葛良拜審酌上級與地方意見，將「大」改為「達」，經代表會決議後，轉呈[台東縣政府核備](../Page/台東縣政府.md "wikilink")\[2\]。也是[台東縣](../Page/臺東縣.md "wikilink")5個[山地原住民鄉之一](../Page/山地鄉.md "wikilink")。

## 行政區劃

本鄉劃分為6村56鄰。

  - [台坂村](../Page/台坂村.md "wikilink")（大坂鹿 Tjuamanges/Tjuaqau/Tjuavanaq ）
  - [土坂村](../Page/土坂村.md "wikilink")（土哇巴樂 Tjuluqalju/Tjuwabar）、
  - [新化村](../Page/新化村.md "wikilink")（Kuvareng）
  - [安朔村](../Page/安朔村.md "wikilink")（阿塱衛 Aljungic ），本鄉行政中心
  - [森永村](../Page/森永村.md "wikilink")（大里力 Tjarilik/Morinaga
    ），該村於日治時期為森永農場場址，後大里力社排灣族人遷村定居於此
  - [南田村](../Page/南田村_\(臺東縣\).md "wikilink")（斯路博吉 Ljupetje ）

<table>
<tbody>
<tr class="odd">
<td><p><font size="-1"><strong>達仁鄉行政區劃</strong></font></p></td>
</tr>
<tr class="even">
<td><div style="position: relative;font-size:100%">
<p><a href="https://zh.wikipedia.org/wiki/File:Daren_villages.svg" title="fig:Daren_villages.svg">Daren_villages.svg</a>      </p>
</div></td>
</tr>
</tbody>
</table>

## 人口

## 交通

### 鐵路

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [南迴線](../Page/南迴線.md "wikilink")
      - [菩安號誌站](../Page/菩安號誌站.md "wikilink")（已廢止）

### 公路

  - [省道](../Page/台灣省道.md "wikilink")

<!-- end list -->

  - [TW_PHW9.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9.svg "fig:TW_PHW9.svg")[台九線](../Page/台九線.md "wikilink")：南迴公路
  - [TW_PHW26.svg](https://zh.wikipedia.org/wiki/File:TW_PHW26.svg "fig:TW_PHW26.svg")[台26線](../Page/台26線.md "wikilink")（僅通至下南田）

<!-- end list -->

  - 縣道

<!-- end list -->

  - [TW_CHW198.svg](https://zh.wikipedia.org/wiki/File:TW_CHW198.svg "fig:TW_CHW198.svg")[縣道198號](../Page/縣道198號.md "wikilink")（未通車，2014年5月1日公告解編該路線）
  - [TW_CHW199.svg](https://zh.wikipedia.org/wiki/File:TW_CHW199.svg "fig:TW_CHW199.svg")[縣道199號](../Page/縣道199號.md "wikilink")

## 教育

### 國民小學

  - [臺東縣達仁鄉土坂國民小學](https://web.archive.org/web/20110827104719/http://php.tbps.ttct.edu.tw/)
  - [臺東縣達仁鄉台坂國民小學](http://www.tabps.ttct.edu.tw/)
  - [臺東縣達仁鄉安朔國民小學](http://www.asps.ttct.edu.tw/)
  - [臺東縣達仁鄉安朔國民小學新化分校](../Page/臺東縣達仁鄉安朔國民小學新化分校.md "wikilink")

## 旅遊

  - [大武山自然保留區](../Page/大武山自然保留區.md "wikilink")
  - [土坂溫泉](../Page/土坂溫泉.md "wikilink")

## 特產

  - [蝴蝶蘭](../Page/蝴蝶蘭.md "wikilink")
  - [百合](../Page/百合.md "wikilink")
  - [鵝卵石](../Page/鵝卵石.md "wikilink")
  - [小米](../Page/小米.md "wikilink")
  - [芋頭](../Page/芋頭.md "wikilink")
  - [花生](../Page/花生.md "wikilink")

## 注釋及參考資料

## 外部連結

  - [達仁鄉公所](http://www.ttdaren.gov.tw)

[Category:山地鄉](../Category/山地鄉.md "wikilink")
[Category:達仁鄉](../Category/達仁鄉.md "wikilink")
[Category:恆春半島](../Category/恆春半島.md "wikilink")

1.  55公所
    原民「地方通行語」公告了[1](http://news.ltn.com.tw/news/politics/breakingnews/2220153),自由時報
    2017-10-12.
2.  葛良拜1954年12月，達仁沿革手稿