**達叻府**\[1\]（，）是東[泰國的一個府](../Page/泰國.md "wikilink")，府都為[達叻市](../Page/達叻市.md "wikilink")。華人稱其為**桐艾府**。她在北面與西面是[莊他武里府](../Page/莊他武里府.md "wikilink")，東面與[柬埔寨接壤](../Page/柬埔寨.md "wikilink")，西南濱臨[泰國灣](../Page/泰國灣.md "wikilink")。人口超過22萬，[府尹為敬鐵](../Page/府尹.md "wikilink")（นาย
บุญช่วย เกิดสุคนธ์，Kaenphet Chuangrangsi）。

## 歷史

  - 達叻府始建於大城時代，在1767年，[鄭昭突破](../Page/鄭昭.md "wikilink")[緬甸侵略軍時](../Page/緬甸.md "wikilink")，曾在此駐紮[海軍](../Page/海軍.md "wikilink")，準確復國。
  - 在1903年，[法國與](../Page/法國.md "wikilink")[拉瑪五世簽約](../Page/拉瑪五世.md "wikilink")，將該府及其它島嶼割讓給[法國](../Page/法國.md "wikilink")，以換取[法軍撤出](../Page/法軍.md "wikilink")[莊他武里府](../Page/莊他武里府.md "wikilink")。
  - 三年後的1906年再簽約，[泰國割讓](../Page/泰國.md "wikilink")[馬德望](../Page/馬德望.md "wikilink")（柏達芒，Battambang）、辛拉（Samlout）、[詩梳風](../Page/詩梳風.md "wikilink")（史鎖蓬，Sisophon）等城市給[法國](../Page/法國.md "wikilink")，才取回該府的管轄。
  - [華僑愛稱此府為](../Page/華僑.md "wikilink")「桐艾府」。

## 地理

  - 達叻府的西南濱[暹羅灣](../Page/暹羅灣.md "wikilink")，東接[柬埔寨](../Page/柬埔寨.md "wikilink")，西鄰[莊他武里府](../Page/莊他武里府.md "wikilink")。
  - 達叻府有許多高山峻嶺，佈滿豐富的[森林及地下](../Page/森林.md "wikilink")[寶石](../Page/寶石.md "wikilink")，南部為沿海[平原](../Page/平原.md "wikilink")，海上有50多個大小島嶼，海產豐富，[工業多為農產品及](../Page/工業.md "wikilink")[海鮮加工廠](../Page/海鮮.md "wikilink")。

## 行政

達叻府的行政區共分為7個縣（Amphoe）；又進一步被劃分為38個區（Tambon）及254個村（Muban）。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>中文名</p></th>
<th><p>泰文名</p></th>
<th><p>英文名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>เมืองตราด</p></td>
<td><p>Mueang Trat</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>คลองใหญ่</p></td>
<td><p>Khlong Yai</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>เขาสมิง</p></td>
<td><p>Khao Saming</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>บ่อไร่</p></td>
<td><p>Bo Rai</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>แหลมงอบ</p></td>
<td><p>Laem Ngop</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/阁骨岛.md" title="wikilink">阁骨县</a></p></td>
<td><p>เกาะกูด</p></td>
<td><p>Ko Kut</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/阁昌岛.md" title="wikilink">阁昌县</a></p></td>
<td><p>เกาะช้าง</p></td>
<td><p>Ko Chang</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Amphoe_Trat.svg" title="fig:Amphoe_Trat.svg">Amphoe_Trat.svg</a></p></td>
</tr>
</tbody>
</table>

## 名勝古蹟

  - 廉愚碼頭：距達叻直轄縣市區約17公里，可在此乘船遊覽各島嶼，如大象島、閣咖哪島、閣叻島、閣骨島等。沿途風光優美，另在碼頭有土特產、紀念品出售。
  - 拗丹庫海灣：位於閣峬島，此[沙灘風光十分優美](../Page/沙灘.md "wikilink")，適合休閒消遣。
  - [閣昌島](../Page/閣昌島.md "wikilink")（大象島）國家公園：它是[泰國第四大的海上](../Page/泰國.md "wikilink")[公園](../Page/公園.md "wikilink")，也是僅次於普吉島的第二大島，位於廉愚縣，面積廣達429平方公里，該島上有「考沙拉碧高山」、「探瑪永大型[瀑布](../Page/瀑布.md "wikilink")」，連泰國君主[拉瑪五世](../Page/拉瑪五世.md "wikilink")、[拉瑪六世](../Page/拉瑪六世.md "wikilink")、[拉瑪七世及](../Page/拉瑪七世.md "wikilink")[拉瑪九世都曾來觀光](../Page/拉瑪九世.md "wikilink")，近年長期有[臺灣及](../Page/臺灣.md "wikilink")[香港的](../Page/香港.md "wikilink")[水上運動愛好者前來](../Page/水上運動.md "wikilink")。
  - 哈莎割沙灘、哈莎晏沙灘、哈它天沙灘、哈邁路沙灘、哈曼清沙灘：位於318公路約43公里處，為一望無際的美麗[沙灘](../Page/沙灘.md "wikilink")，風平浪靜，有椰林和果園，水清沙白，風景非常美麗，是嬉水的好去處。
  - 噠叻坡珠寶市場：位於莫賴縣，分別有「華通市場」、「空喲市場」、「曼農芒市場」、「曼沙艾市場」、「曼噠安市場」等，出售著名的[紅寶石](../Page/紅寶石.md "wikilink")，通常只在早上至10時才作交易。
  - 哇喲它尼密寺及城隍廟：位於達叻直轄縣裡，「哇喲它尼密寺」是[拉瑪三世在](../Page/拉瑪三世.md "wikilink")1834年所建之鎮府佛寺，內有精致的[佛教故事壁畫](../Page/佛教.md "wikilink")。附近有一[中國式](../Page/中國.md "wikilink")[建築](../Page/建築.md "wikilink")[藝術風格的城隍廟](../Page/藝術.md "wikilink")，據報由當年駐紮在此地的[鄭皇所建造](../Page/鄭皇.md "wikilink")。
  - 達叻府節慶：為紀念1906年3月23日，[法國歸還達叻府給](../Page/法國.md "wikilink")[暹羅](../Page/暹羅.md "wikilink")（[泰國](../Page/泰國.md "wikilink")），每年均在3月份舉行慶祝盛會。另在每年的5至6月份舉行水果節盛會。

## 參考書籍

  - 《暢遊[泰國七十六府](../Page/泰國.md "wikilink")》，林娟主編，「大同社出版有限公司」（[泰國](../Page/泰國.md "wikilink")[曼谷](../Page/曼谷.md "wikilink")[石龍軍路](../Page/石龍軍路.md "wikilink")690號），2000年5月，ISBN：974-89682-8-6

## 外部連結

  - [達叻府政府網址](http://www.trat.go.th)
  - [泰國王家旅游局達叻府關於專頁](https://web.archive.org/web/20051210171248/http://www.tourismthailand.org/destinationguide/list.aspx?provinceid=14)

<!-- end list -->

  - [達叻府地圖與徽章](http://www.thailex.info/THAILEX/THAILEXENG/LEXICON/Copy%20of%20Trat.htm)


[Category:泰國府份](../Category/泰國府份.md "wikilink")
[Category:達叻府](../Category/達叻府.md "wikilink")

1.