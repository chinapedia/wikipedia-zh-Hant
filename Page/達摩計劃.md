[Dharma_initiative_logo_video.jpg](https://zh.wikipedia.org/wiki/File:Dharma_initiative_logo_video.jpg "fig:Dharma_initiative_logo_video.jpg")》中「[任務方針](../Page/迷失_\(第二季\)#.E7.AC.AC3.E9.9B.86.md "wikilink")」（*Orientation*）劇集裡出現\]\]
[DHARMA.jpg](https://zh.wikipedia.org/wiki/File:DHARMA.jpg "fig:DHARMA.jpg")（Lost
Experience）的一部份釋出，顯示了達摩計劃的全名\]\]

**達摩計劃**（**DHARMA
Initiative**）是一個在[美國](../Page/美國.md "wikilink")[電視影集](../Page/電視影集.md "wikilink")《[迷失](../Page/迷失.md "wikilink")》（*Lost*）中出現的虛構研究計劃。計劃的全名為**D**epartment
of [**H**euristics](../Page/启发法.md "wikilink") **A**nd **R**esearch on
**M**aterial **A**pplications **Initiative** \[1\]
（直譯為：資料應用研究與[啟發法機構部門](../Page/啟發法.md "wikilink")）。是在影集[第二季](../Page/迷失_\(第二季\).md "wikilink")「[任務方針](../Page/迷失_\(第二季\)#.E7.AC.AC3.E9.9B.86.md "wikilink")」（*Orientation*）一集中首次登場。

## 背景

在小島上的地底[碉堡中找到的任務方針](../Page/碉堡.md "wikilink")（Orientation）影片，揭開了更多關於達摩計劃的詳細內容，其中講述了第三基地「天鵝」（The
Swan）的目的和運作方式。\[2\]影片最後[版權宣告中的日期為](../Page/著作權.md "wikilink")1980年，並有「6之3」（3
of 6）的標示。馬文·坎多博士（Marvin Candle，由柬埔寨裔演員François
Chau飾演）擔任這段影片的旁白，而影片中有多段內容明顯受損或經過剪接。

[Tv_lost_gerald_degroot.jpg](https://zh.wikipedia.org/wiki/File:Tv_lost_gerald_degroot.jpg "fig:Tv_lost_gerald_degroot.jpg")
這段影片提供了達摩計劃的背景歷史。其中宣稱該計劃是由[密歇根大学的博士候選人凱倫](../Page/密歇根大学.md "wikilink")（Karen）和傑洛德·高特（Gerald
de Groot）在1970年成立，並由[漢索基金會](../Page/漢索基金會.md "wikilink")（Hanso
Foundation）提供財務支援。達摩計劃集結來自全球的「科學家和自由思想家」在一個「大型的集體研究設施」，進行多方面的研究計劃，包含[氣象學](../Page/氣象學.md "wikilink")、[心理學](../Page/心理學.md "wikilink")、[超心理學](../Page/超心理學.md "wikilink")、[動物學](../Page/動物學.md "wikilink")、[電磁學](../Page/電磁學.md "wikilink")，此外還有一個在影片中以「[烏托邦社會](../Page/烏托邦.md "wikilink")…」作為開頭的項目，但在講完之前就被切斷。此外，影片中提及了美國[心理學家及](../Page/心理學家.md "wikilink")《》（*Walden
Two*）一書的作者[B.F.斯金纳](../Page/伯尔赫斯·弗雷德里克·斯金纳.md "wikilink")（B.F.
Skinner），宣稱他對傑洛德的工作帶來影響。

在機尾部份生還者居住的另一個廢棄基地中，[伊高先生找到了影片的另一部份](../Page/伊高·東迪.md "wikilink")。在「[陰魂不散](../Page/陰魂不散_\(迷失\).md "wikilink")」（*What
Kate
Did*）一集中，影片的一部份被藏在一本聖經後面，[約翰·洛克將它剪接進原本的任務方針影片中](../Page/約翰·洛克_\(迷失\).md "wikilink")。這段影片中表示不可以使用電腦系統與外界連絡。

在第二季的最後一集「[同生不共死](../Page/同生不共死.md "wikilink")」（*Live Together, Die
Alone*）中，揭露了這段影片是被之前居住在天鵝基地的人所剪掉。

在「[問號](../Page/問號_\(迷失\).md "wikilink")」一集中，艾可先生和洛克發現了第五基地「珍珠」，這座基地也有一卷任務方針影帶，描述珍珠基地的作用。同時影片中也有標示為1985年的版權宣告。旁白者明顯是馬文·坎多（Marvin
Candle），但在影片中他自稱馬克·維克曼博士（Dr. Mark
Wickmund）。\[3\]根據這段影片，珍珠基地是為了觀測並記錄島嶼上的其他基地。影片中解釋其他基地正在進行心理學的實驗，基地中的團隊人員認為他們自己正在進行一件非常重要的任務。

派駐在珍珠基地的達摩計劃成員，必須在監視站中記錄下「天鵝基地」中發生每一件事情。記載這些事項的筆記本是透過一個氣動傳輸管（pneumatic
tube）傳送出去。這根管子通往島上一個神祕偏遠的地方，在那裡堆放著大量似乎未被翻閱過的筆記本。

在「[迷失體驗](../Page/迷失體驗.md "wikilink")」（Lost
Experience）[虛擬現實遊戲](../Page/虛擬現實.md "wikilink")（ARG）中，達摩計劃是為了要找到改變「Valenzetti
Equation」的辦法，這是一個設計用來預測世界末日的數學方程式。然而，一名漢索基金會的成員湯瑪士·美泰爾沃克（Thomas
Mittelwerk）在稍後表示「達摩計劃宣告失敗」。

## 研究基地

達摩計劃在島嶼上及島外有數座[研究基地](../Page/科学方法.md "wikilink")，皆是隱藏起來或位於地底的設施。第一個被生還者發現的基地是「第三基地」或稱「天鵝」（The
Swan），他們稱呼這個基地為「密艙」（the hatch）。他們佔用了這座基地直到第二季基地被毀結束為止。此外還有其他不同基地陸續被發現。

分佈島上及島外的基地各有不同用途，暫時已經出現的達摩計劃基地有：

### 島內範圍

  - 「天鵝」（The Swan）
      - 「天鵝」站是用於對電磁力量研究、遏制。由[洛克](../Page/約翰·洛克_\(迷失\).md "wikilink")、[布恩所發現](../Page/布恩·卡利斯爾.md "wikilink")。現已被毀。
  - 「珍珠」（The Pearl）
      - 「珍珠」站是用於心理研究、觀察。先由[妮姬和保羅所發現](../Page/妮姬.md "wikilink")，後被洛克和[伊高先生所發現](../Page/伊高·東迪.md "wikilink")。現被廢棄。
  - 「箭頭」（The Arrow）
      - 「箭頭」站是用於情報收集、防禦策略研究。由機尾生還者所發現。現被廢棄。
  - 「權杖」（The Staff）
      - 「權杖」站是用於醫療，由[凱特](../Page/凱特·奧斯頓.md "wikilink")、[克萊爾和](../Page/克萊爾·李特頓.md "wikilink")[盧梭所發現](../Page/達妮艾爾·盧梭.md "wikilink")。現被廢棄。
  - 「火燄」（The Flame）
      - 「火燄」站用於對島內、島外通訊。由凱特、洛克、[薩伊德和盧梭所發現](../Page/扎伊爾德·賈拉.md "wikilink")。現已被毀。
  - 「窺鏡」（The Looking Glass）
      - 「窺鏡」站是用於封鎖訊號、作為聲納燈塔。由[查理和](../Page/查理·佩斯.md "wikilink")[德斯蒙德發現](../Page/德斯蒙德·夭米.md "wikilink")。現被廢棄，部份被毀。
  - 「蘭花」（The Orchid）
      - 「蘭花」站是用於對時空穿梭的研究、偽裝為對植物研究。由[班](../Page/班·連路斯.md "wikilink")、[赫利和洛克所發現](../Page/雨果·「赫利」·瑞斯.md "wikilink")。現被廢棄。
  - 「暴風雨」（The Tempest）
      - 「暴風雨」站是用於生產毒氣、偽裝成為島上的發電站。由[夏洛特](../Page/夏洛特·路易斯.md "wikilink")、[丹尼爾和](../Page/丹尼爾·法拉第.md "wikilink")[茱莉葉所發現](../Page/茱莉葉·柏克.md "wikilink")。現被廢棄。

### 島外範圍

  - 「九頭蛇」（The Hydra）
      - 位於副島、用於動物學研究。由[傑克](../Page/傑克·謝潑德.md "wikilink")、凱特和[索耶所發現](../Page/詹姆斯·「索耶」·福特.md "wikilink")。現被廢棄。
  - 「燈柱」（The Lamp Post）
      - 唯一已知在兩個島外的基地、用以在不同時間定位小島的位置。由傑克、凱特、班和[善華所發現](../Page/善華.md "wikilink")。現正被埃洛伊塞·霍金監管和使用。

每座基地都有一個與名稱相關的[標誌](../Page/標誌.md "wikilink")：一個以[八卦為基礎的](../Page/八卦.md "wikilink")[八角形](../Page/八角形.md "wikilink")，在中心位置有一個特殊的[符號](../Page/符號.md "wikilink")。

## 其他

  - 達摩計劃的標誌出現在「[漂流](../Page/迷失_\(第二季\)#.E7.AC.AC2.E9.9B.86.md "wikilink")」一集中攻擊[麥可和](../Page/麥可·道森.md "wikilink")[索耶的](../Page/詹姆斯·「索耶」·福特.md "wikilink")[鯊魚背脊上](../Page/鯊魚.md "wikilink")。在第二季DVD的特別收錄中解釋這僅是一個「[彩蛋](../Page/彩蛋_\(視覺\).md "wikilink")」，與故事主線並沒有任何關係，也不是刻意要被觀眾所看見。
  - 馬文·坎多的左手臂在整部影片中從未移動過，而影集製作人[戴蒙·林道夫](../Page/戴蒙·林道夫.md "wikilink")（Damon
    Lindelof）和執行製作[卡爾頓·克斯](../Page/卡爾頓·克斯.md "wikilink")（Carlton
    Cuse）證實坎多的左手是[義肢](../Page/義肢.md "wikilink")。\[4\]然而在第二季的[問號](../Page/問號_\(迷失\).md "wikilink")（*?*）一集中，他在第二支任務影片中出現，其中他自稱「Mark
    Wickmund」，而且雙手合掌，同時使用了兩隻手臂。
  - 在[虛擬現實遊戲](../Page/虛擬現實.md "wikilink")「[迷失體驗](../Page/迷失體驗.md "wikilink")」（Lost
    Experience）中，飾演漢索基金會人員休·麥可林泰瑞（Hugh
    McIntyre）的演員在電視節目《[吉米·坎摩尔直播秀](../Page/吉米·坎摩尔直播秀.md "wikilink")》接受專訪，他並宣稱達摩計劃在1987年就已經終止。\[5\]
  - [達摩](../Page/法_\(佛典\).md "wikilink")（Dharma）也是一個[梵語的單字](../Page/梵語.md "wikilink")，在[佛教和](../Page/佛教.md "wikilink")[印度教等宗教中有許多不同的含意](../Page/印度教.md "wikilink")，包含「自然法則」、「真實」、「事實」和「任務」等解釋。
  - 與梵語之間的關連在第二支任務方針影片中出現了更顯著的證據，影片中的解說者稱呼自己為「Mark
    Wickmund」，並合掌說出「[那摩斯戴](../Page/那摩斯戴.md "wikilink")」（Namaste），這是印度語/梵語中傳統的問候語。
  - 「其他人」（Others）進行的葬禮是印度的一種傳統，將死者的遺體放在水面上任其漂流。

## 注釋與資料來源

<div class="references-small">

<references />

</div>

[Category:迷失](../Category/迷失.md "wikilink")
[Category:虛構學術機構](../Category/虛構學術機構.md "wikilink")
[Category:虚构实验室](../Category/虚构实验室.md "wikilink")
[Category:虛構祕密結社](../Category/虛構祕密結社.md "wikilink")

1.  是在漢索基金會網站上遊玩記憶遊戲時所提供的訊息，原本可以在[此處](http://thehansofoundation.org/swf/extra/mha.swf?playDirect=true)找到，但由於該網站為電視影集的限時宣傳活動之一，目前時間已經結束，因此已無法連結上該頁面。在網站中（開放期間內）要察看全名必須達到42等級，是影集中[神祕數字](../Page/迷失神祕學#神祕數字.md "wikilink")（The
    numbers）的最大值。
2.  出自第三基地「天鵝」中的找到的任務方針影片，可在影集第二季中的[任務方針](../Page/迷失_\(第二季\)#.E7.AC.AC3.E9.9B.86.md "wikilink")」（*Orientation*）一集中找到。影片的部份內容可在[此處](http://cosmos.bcst.yahoo.com/scp_v3/viewer/index.php?pid=16469&rn=240896&cl=280242&ch=240960)
    觀看，是出自澳洲Yahoo\!
    網站。此外在飾演片中博士一角的演員網站上也有收錄這段影片的開頭部份，請參見[此處](http://www.francoischau.com/Video/lost.wmv)

3.  這個角色的英文姓氏（Wickmund）在影集中聽起來像是「Wickman」，但在美國[Comic-Con活動中](../Page/Comic-Con.md "wikilink")，美國廣播公司（ABC）的官方正式宣傳品上，記載的姓名為「Dr.
    Mark
    Wickmund」，參見[這份文宣品的照片](http://photos1.blogger.com/blogger/5754/2891/1600/comicon%20022.jpg)
4.  [LOST檔案的官方Podcast](http://abc.go.com/primetime/lost/podcasts/101694.html)
    2006年1月17日
5.  在*Jimmy Kimmel Live*節目中。