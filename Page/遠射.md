**遠射**，是[足球最常見的](../Page/足球.md "wikilink")[術語](../Page/術語.md "wikilink")，即在離門較遠的地方射門。通常在禁區外的射門都被稱為遠射。而一些擅長遠射的球員，則被稱為**遠射手**。

## 著名的遠射手

  - [碧咸](../Page/碧咸.md "wikilink") －
    現效力美國球會[洛杉磯銀河的](../Page/洛杉磯銀河.md "wikilink")[英格蘭球星](../Page/英格蘭.md "wikilink")，在1998-99球季的英超距門約60碼一腳笠死[溫布頓門將](../Page/溫布頓足球會.md "wikilink")，因此「一笠成名」。當然距門20-40碼都不會難倒他。
  - [卡路士](../Page/卡路士.md "wikilink") －
    [巴西左閘](../Page/巴西.md "wikilink")，曾與碧咸一起效力[皇馬](../Page/皇馬.md "wikilink")，他的遠射雷霆萬鈞，而且任何角度及距離都可以得手，在多季前的[西甲曾經在左路底線射入一記近乎零角度的金球](../Page/西甲.md "wikilink")。亦在1999年的[洲際杯以一記](../Page/洲際盃足球賽.md "wikilink")37碼外，時速119公里的「超級腳趾尾拉西罰球」射破[法國隊](../Page/法國國家足球隊.md "wikilink")。
  - [米赫洛域](../Page/米赫洛域.md "wikilink") －
    [意甲史上第一罰球手](../Page/意甲.md "wikilink")，在意甲共射入了27個任意球。1998年12月13日，米赫洛域代表[拉素在意甲聯賽中出戰](../Page/拉素.md "wikilink")[森多利亞時完成了任意球帽子戏法](../Page/森多利亞.md "wikilink")。
  - [謝拉特](../Page/謝拉特.md "wikilink") －
    [英格蘭中場](../Page/英格蘭.md "wikilink")，攻守兼備之餘還有極重的遠射腳頭，在2005-06球季[利物浦對](../Page/利物浦.md "wikilink")[韋斯咸的](../Page/韋斯咸.md "wikilink")[英格蘭足總杯決賽中](../Page/英格蘭足總杯.md "wikilink")35碼外窩利射入世界波，間接幫助紅軍奪標。
  - [祖高爾](../Page/祖高爾.md "wikilink") －
    [英格蘭翼鋒](../Page/英格蘭.md "wikilink")，曾在[2006年世界杯以一記](../Page/2006年世界杯.md "wikilink")35碼外的窩利遠射攻破了[瑞典](../Page/瑞典.md "wikilink")，可惜最後英格蘭被瑞典2比2打和。
  - [懷斯](../Page/懷斯.md "wikilink") －
    [挪威左閘](../Page/挪威.md "wikilink")，亦是謝拉特的利物浦隊友，遠射腳頭比謝拉特更重，曾經在對[曼聯的英足杯中射出砲彈式罰球意外弄傷](../Page/曼聯.md "wikilink")[阿倫史密夫](../Page/阿倫史密夫.md "wikilink")。
  - [雲邦美](../Page/雲邦美.md "wikilink") －
    [拜仁的](../Page/拜仁慕尼黑.md "wikilink")[荷蘭中場](../Page/荷蘭.md "wikilink")，其重砲在任何距離都能起作用。
  - [波歷克](../Page/波歷克.md "wikilink") －
    [車路士的](../Page/車路士.md "wikilink")[德國國腳](../Page/德國.md "wikilink")，射門腳頭極重，在禁區頂最能發揮威力。
  - [保馬](../Page/保馬.md "wikilink") －
    曾在[PSV燕豪芬效力](../Page/PSV燕豪芬.md "wikilink")，現時效力[阿士東維拉的](../Page/阿士東維拉.md "wikilink")[荷蘭中堅](../Page/荷蘭.md "wikilink")，雖然身材偏矮，但遠射是他的看家本領，距門20-30碼都是他的射程範圍。
  - [托迪](../Page/托迪.md "wikilink") －
    [羅馬的星級前鋒](../Page/羅馬.md "wikilink")，在任何距離都可以射門，而且間中都會射入一些高難度的入球，最叫人難忘的是離門約20碼及角度極窄的情況下接應[卡斯錫迪的長傳窩利抽入](../Page/卡斯錫迪.md "wikilink")，並成為2006-07球季十大金球總亞軍。
  - [迪亚曼蒂](../Page/亚历山德罗·迪亚曼蒂.md "wikilink") －
    由意大利丙組聯賽某球會轉投的中場球員，擁有極好的遠射能力，為利禾奴射進的4球全都是遠射和罰球。
  - [史坦高域](../Page/史坦高域.md "wikilink") －
    [國際米蘭的](../Page/國際米蘭.md "wikilink")[塞爾維亞進攻中場](../Page/塞爾維亞.md "wikilink")，也是[塞爾維亞國家隊的隊長](../Page/塞爾維亞國家足球隊.md "wikilink")，2006-07球季狀態極佳，射進6球，當中第4周對[基爾禾時一記](../Page/基爾禾.md "wikilink")30碼外重砲轟入死角，此球以一哥姿態上金球榜後，以非常緩慢的速度下降，結果在最後一周依然留在榜上，排名第9。

## 参见

  - [定點球](../Page/定點球.md "wikilink")
  - [直接任意球](../Page/直接任意球.md "wikilink")
  - [足球](../Page/足球.md "wikilink")

[Y](../Category/足球術語.md "wikilink")