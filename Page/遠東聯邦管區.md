**遠東聯邦管區**（）是[俄罗斯位於](../Page/俄罗斯.md "wikilink")[远东的](../Page/远东.md "wikilink")[聯邦區](../Page/俄羅斯聯邦管區.md "wikilink")。2017年的人口为618万人\[1\]，总统特使是。

该地区自古以来就是东亚民族的活动地区，这块土地上曾先后存在[肃慎](../Page/肃慎.md "wikilink")、[夫余](../Page/夫余.md "wikilink")、[靺鞨](../Page/靺鞨.md "wikilink")、[女真等民族](../Page/女真.md "wikilink")，其部分地区歷史上也曾被[中國历史上的](../Page/中國历史.md "wikilink")[唐](../Page/唐朝.md "wikilink")、[辽](../Page/辽朝.md "wikilink")、[金](../Page/金朝.md "wikilink")、[元](../Page/元朝.md "wikilink")、[明](../Page/明朝.md "wikilink")、[清六朝所統治](../Page/清朝.md "wikilink")。自从该地区歸[俄罗斯統治后](../Page/俄罗斯.md "wikilink")，除了俄罗斯民族外，尚有[赫哲族](../Page/赫哲族.md "wikilink")、[达斡尔族等当地居民](../Page/达斡尔族.md "wikilink")。另外該地原來的一些東亞民族，包括[漢族](../Page/漢族.md "wikilink")、[滿族](../Page/滿族.md "wikilink")、[朝鮮族和](../Page/朝鮮族.md "wikilink")[大和族](../Page/大和族.md "wikilink")，不是遭到後來的蘇聯政府屠殺或強制遷徙，就是被驅逐出境回到自己的母國。

## 地理

俄罗斯远东地区是俄罗斯滨临[太平洋的地区](../Page/太平洋.md "wikilink")，它包括[萨哈共和国](../Page/萨哈共和国.md "wikilink")、[犹太自治州和](../Page/犹太自治州.md "wikilink")[堪察加半島](../Page/堪察加半島.md "wikilink")。联邦区的首府是[海参崴](../Page/海参崴.md "wikilink")（人口59.2万）。

## 历史

1920年到1922年[远东共和国从俄罗斯独立](../Page/远东共和国.md "wikilink")，在[蘇聯与](../Page/蘇聯.md "wikilink")[大日本帝國之间造成了一个缓冲区](../Page/大日本帝國.md "wikilink")。

2018年11月5日，俄总统普京签署法令，[布里亚特共和国与](../Page/布里亚特共和国.md "wikilink")[外贝加尔边疆区从](../Page/外贝加尔边疆区.md "wikilink")[西伯利亚联邦管区划归远东联邦管区](../Page/西伯利亚联邦管区.md "wikilink")。

## 气候和动植物

俄罗斯远东地区属于[大陆性气候](../Page/大陆性气候.md "wikilink")，冬季非常寒冷，夏季短暂，在南部温暖，在北部也相当冷。大多数地区由北极[针叶林覆盖或者是](../Page/针叶林.md "wikilink")[冻原](../Page/冻原.md "wikilink")。冻原上只有矮的灌木、草和[苔藓植物](../Page/苔藓植物.md "wikilink")，地面往往终年不化。[农业只有在少数受保护的地方可以进行](../Page/农业.md "wikilink")，[林业是重要的经济部门](../Page/林业.md "wikilink")。

## 人口

### 聯邦主體

| [Far_Eastern_Federal_District_(numbered,_2018_composition).svg](https://zh.wikipedia.org/wiki/File:Far_Eastern_Federal_District_\(numbered,_2018_composition\).svg "fig:Far_Eastern_Federal_District_(numbered,_2018_composition).svg") |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| \#                                                                                                                                                                                                                                            |
| 1                                                                                                                                                                                                                                             |
| 2                                                                                                                                                                                                                                             |
| 3                                                                                                                                                                                                                                             |
| 4                                                                                                                                                                                                                                             |
| 5                                                                                                                                                                                                                                             |
| 6                                                                                                                                                                                                                                             |
| 7                                                                                                                                                                                                                                             |
| 8                                                                                                                                                                                                                                             |
| 9                                                                                                                                                                                                                                             |
| 10                                                                                                                                                                                                                                            |
| 11                                                                                                                                                                                                                                            |

註：粗體字為該聯邦管區行政中心

### 最大城市

從2002年全國[人口普查的資料顯示](../Page/俄羅斯人口普查_\(2002\).md "wikilink")，遠東聯邦管區各地人口為：

1.  [符拉迪沃斯托克](../Page/符拉迪沃斯托克.md "wikilink")（[海參崴](../Page/海參崴.md "wikilink")）：594,701
2.  [哈巴罗夫斯克](../Page/哈巴罗夫斯克.md "wikilink")（[伯力](../Page/伯力.md "wikilink")）：583,072
3.  [阿穆尔河畔共青城](../Page/阿穆尔河畔共青城.md "wikilink")([瓦倫](../Page/瓦倫.md "wikilink"))：271,600
4.  [布拉戈维申斯克](../Page/布拉戈维申斯克.md "wikilink")（[海蘭泡](../Page/海蘭泡.md "wikilink")）：219,221
5.  [雅庫茨克](../Page/雅庫茨克.md "wikilink")：210,642
6.  [堪察加彼得巴甫洛夫斯克](../Page/堪察加彼得巴甫洛夫斯克.md "wikilink")：198,028
7.  [南薩哈林斯克](../Page/南薩哈林斯克.md "wikilink")：173,600
8.  [纳霍德卡](../Page/纳霍德卡.md "wikilink")：148,826
9.  [馬加丹](../Page/馬加丹.md "wikilink")：99,399
10. [比羅比詹](../Page/比羅比詹.md "wikilink")：77,250
11. [阿納德爾](../Page/阿納德爾.md "wikilink")：11,038

<File:VladivostokGoldenHorn.jpg>|[海參崴](../Page/海參崴.md "wikilink")
<File:Khabarovsk_Old_Duma.jpg>|[伯力](../Page/伯力.md "wikilink")
<File:Komsomolsk_na_Amuru.jpg>|[瓦倫](../Page/瓦倫.md "wikilink")([阿穆尔河畔共青城](../Page/阿穆尔河畔共青城.md "wikilink"))
<File:%D0%91%D0%B8%D1%80%D0%BE%D0%B1%D0%B8%D0%B4%D0%B6%D0%B0%D0%BD,_%D0%B2%D0%BE%D0%BA%D0%B7%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D0%BF%D0%BB%D0%BE%D1%89%D0%B0%D0%B4%D1%8C.JPG>|[比罗比詹](../Page/比罗比詹.md "wikilink")
<File:Magadan_seen_from_mountain.jpg>|[馬加丹](../Page/馬加丹.md "wikilink")
<File:Stores_in_Nakhodka_Fishing_Port.JPG>|[纳霍德卡](../Page/纳霍德卡.md "wikilink")
<File:Anadyr_harbour3.jpg>|[阿纳德尔](../Page/阿纳德尔.md "wikilink")

## 参考文献

## 外部連結

  - [Meeting of Frontiers: Siberia, Alaska, and the American
    West](http://frontiers.loc.gov/) (includes materials on Russian Far
    East)
  - [Unofficial website of the Far Eastern Federal
    District](https://web.archive.org/web/20100614055853/http://www.dalfo.ru/)


## 參見

  - [遠東共和國](../Page/遠東共和國.md "wikilink")
  - [外滿洲](../Page/外滿洲.md "wikilink")
      - [1972年俄罗斯远东地区地名变更](../Page/1972年俄罗斯远东地区地名变更.md "wikilink")

{{-}}

[Category:俄羅斯聯邦管區](../Category/俄羅斯聯邦管區.md "wikilink")
[遠東聯邦管區](../Category/遠東聯邦管區.md "wikilink")
[Category:2000年俄羅斯建立](../Category/2000年俄羅斯建立.md "wikilink")

1.