**遺傳系譜學**或称**遺傳[家譜學](../Page/家譜學.md "wikilink")**（）是應用[遺傳學來研究](../Page/遺傳學.md "wikilink")[傳統的家譜學](../Page/系譜學.md "wikilink")。利用[DNA的分析](../Page/DNA.md "wikilink")，建立出個體之間的系譜關係。可用於追溯母系或父系祖先、[民族起源](../Page/民族.md "wikilink")、[生物地理學以及](../Page/生物地理學.md "wikilink")[人類遷徙](../Page/人類遷徙.md "wikilink")。[Y染色體與](../Page/Y染色體.md "wikilink")[粒線體DNA是常用的分析對象](../Page/粒線體DNA.md "wikilink")。

## 延伸閱讀

### 書籍

  - *Early book on adoptions, paternity and other relationship testing.
    Carmichael is a founder of GeneTree.*

  -
  -
  -
  -
  -
  - *Survey of major populations.*

  -
  - *Out of date but still worth reading.*

  - *Early guide for do-it-yourself genealogists.*

  -
  - *Guide to the subject of family medical history and genetic
    diseases.*

  - *Names the founders of Europe’s major female haplogroups
    [Helena](../Page/Haplogroup_H_\(mtDNA\).md "wikilink"),
    [Jasmine](../Page/Haplogroup_J_\(mtDNA\).md "wikilink"),
    [Katrine](../Page/Haplogroup_K_\(mtDNA\).md "wikilink"),
    [Tara](../Page/Haplogroup_T_\(mtDNA\).md "wikilink"),
    [Velda](../Page/Velda.md "wikilink"),
    [Xenia](../Page/Haplogroup_X_\(mtDNA\).md "wikilink"), and
    [Ursula](../Page/Haplogroup_U_\(mtDNA\).md "wikilink").*

  -
  -
  -
### 文件

### 期刊

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [International Society of Genetic
    Genealogy](http://www.isogg.org/wiki/)
  - [MSNBC](http://www.msnbc.msn.com/id/3038411/) – Genetic genealogy
    links from MSN
  - [Guide to ancestry testing from [Sense about
    Science](../Page/Sense_about_Science.md "wikilink")](https://web.archive.org/web/20160310041352/http://www.senseaboutscience.org/resources.php/119/sense-about-genetic-ancestry-testing)
  - ["Debunking Genetic Astrology" a set of webpages at [University
    College
    London](../Page/University_College_London.md "wikilink")](http://www.telegraph.co.uk/news/science/science-news/9912822/DNA-ancestry-tests-branded-meaningless.html)

[遗传系谱学](../Category/遗传系谱学.md "wikilink")
[Category:群体遗传学](../Category/群体遗传学.md "wikilink")
[Category:系譜學](../Category/系譜學.md "wikilink")
[Category:家世與傳承](../Category/家世與傳承.md "wikilink")
[Category:公众科学](../Category/公众科学.md "wikilink")