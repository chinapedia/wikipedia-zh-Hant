**避讳**是[中国歷史上](../Page/中国歷史.md "wikilink")，必須回避[君主](../Page/君主.md "wikilink")、尊長的「[名諱](../Page/名諱.md "wikilink")」\[1\]的一种要求，通常只限於[君主](../Page/君主.md "wikilink")、尊長之[本名](../Page/本名.md "wikilink")。[字](../Page/表字.md "wikilink")[號的避讳则较少见](../Page/號.md "wikilink")\[2\]。在言谈和书写时，遇到君主尊長的名諱一律要回避，可以用其他字代換，或是刻意將該字缺筆；取名时，不能取他们的名諱中字甚或同音的字；否則，可能觸犯[大不敬之罪](../Page/大不敬.md "wikilink")。[陆容](../Page/陆容.md "wikilink")《菽园杂记》稱，“民间俗讳各处有之，[吴中为甚](../Page/吴中.md "wikilink")”。這種要求也曾經適用於[漢字文化圈内中国的臨近國家](../Page/漢字文化圈.md "wikilink")，例如[日本](../Page/日本.md "wikilink")、[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[越南](../Page/越南.md "wikilink")、[琉球](../Page/琉球國.md "wikilink")。

## 避諱的起源

避讳始於[周朝](../Page/周朝.md "wikilink")\[3\]，《[左传](../Page/左传.md "wikilink")》说：“周人以讳事神，名，终将讳之。”\[4\]《[礼记](../Page/礼记.md "wikilink")·曲礼》载：“名子者不以国，不以日月，不以隐疾，不以山川。”明文规定取名之避。后来，《[左传](../Page/左传.md "wikilink")》又加上“不以畜牲，不以器帛”这一条款，遂产生“六避”。\[5\]但当时同音或近音的字不用回避。名諱兩字中，只有一个字相同，也不用回避。

## 避諱的種類

  - 回避[君主的名字](../Page/君主.md "wikilink")，叫做避**君讳**、**公讳**或**国讳**。一般適用於過去七世代以内。
  - 避讳自己家族的长辈的名字，与别人交往时则避对方的长辈之名讳，叫做避**家讳**、**私讳**。
  - 回避[聖人的名字](../Page/聖人.md "wikilink")，叫做避**聖人諱**。例如[邱姓就因避諱](../Page/邱姓.md "wikilink")[孔子的名字](../Page/孔子.md "wikilink")「丘」而產生；又例如《[三国演义](../Page/三国演义.md "wikilink")》中称[关羽为](../Page/关羽.md "wikilink")「关公」、「关某」；《[东周列国志](../Page/东周列国志.md "wikilink")》中称孔子为「孔某」。另古人讀經史時，碰到孔子的名字「丘」時，會將之讀作「某」，以示避諱，就連寫的時候也會缺筆。\[6\]\[7\]

## 避諱的方法與實例

[陳垣](../Page/陳垣.md "wikilink")《史諱舉例》曰：「避諱常用之法有三：曰**改字**，曰**空字**，曰**缺筆**。」到了[宋朝](../Page/宋朝.md "wikilink")，避諱之風日趨嚴格，又出現「**[拆字](../Page/拆字.md "wikilink")**」這一種避諱方法。

### 改字

改字法：遇到要避諱的字時使用其他別字代替。例如：

1.  避[秦始皇嬴政諱](../Page/秦始皇.md "wikilink")，“政月”改“正月”，「正」字改唸平聲，甚至改称“端月”。
2.  避[漢高祖劉邦諱](../Page/劉邦.md "wikilink")，“相邦”改“相國”。
3.  避[漢文帝劉恆音諱](../Page/漢文帝.md "wikilink")，“姮娥”改[嫦娥](../Page/嫦娥.md "wikilink")。
4.  避[漢景帝劉啟諱](../Page/漢景帝.md "wikilink")，“啟蟄”改[驚蟄](../Page/驚蟄.md "wikilink")、“[启封县](../Page/启封县.md "wikilink")”改[开封县](../Page/开封县.md "wikilink")。
5.  避[漢武帝劉徹諱](../Page/漢武帝.md "wikilink")，“[徹侯](../Page/徹侯.md "wikilink")”改通侯。
6.  避[漢昭帝劉弗陵諱](../Page/漢昭帝.md "wikilink")，否定詞“弗”改“不”。
7.  避[漢宣帝劉詢音諱](../Page/漢宣帝.md "wikilink")，“[荀卿](../Page/荀卿.md "wikilink")”改孫卿。
8.  避[漢光武帝劉秀諱](../Page/劉秀.md "wikilink")，东汉[孝廉](../Page/孝廉.md "wikilink")“[秀才](../Page/秀才.md "wikilink")”改“[茂才](../Page/茂才.md "wikilink")”。
9.  避[漢明帝劉莊讳](../Page/漢明帝.md "wikilink")，[莊姓者多半改成](../Page/莊姓.md "wikilink")[嚴姓](../Page/嚴姓.md "wikilink")。
10. 避[漢桓帝劉志讳](../Page/漢桓帝.md "wikilink")，《[東觀漢記](../Page/東觀漢記.md "wikilink")》的“志”改稱“意”。
11. 避[晉宣帝司馬懿](../Page/司馬懿.md "wikilink")（追尊）諱，[三國志將](../Page/三國志.md "wikilink")[吳懿改](../Page/吳懿.md "wikilink")“吳壹”。
12. 避[晉文帝司馬昭](../Page/司馬昭.md "wikilink")（追尊）諱，[王昭君改稱王明君](../Page/王昭君.md "wikilink")，[蔡昭姬改稱蔡文姬](../Page/蔡昭姬.md "wikilink")，[韋昭改稱](../Page/韋昭.md "wikilink")[韋曜](../Page/韋曜.md "wikilink")。
13. 避[晉愍帝司馬鄴音諱](../Page/晉愍帝.md "wikilink")，建業（今[南京市](../Page/南京市.md "wikilink")）改名為建康。
14. 避[唐高祖李淵祖父](../Page/唐高祖.md "wikilink")[李虎諱](../Page/李虎.md "wikilink")，漢朝以來稱作「虎子」的[馬桶改稱為](../Page/馬桶.md "wikilink")「馬子」。\[8\]
15. 避[唐高祖李淵諱](../Page/唐高祖.md "wikilink")，[陶淵明改稱陶泉明或陶深明](../Page/陶淵明.md "wikilink")。
16. 避[唐太宗李世民讳](../Page/唐太宗.md "wikilink")，「世」、「民」常改為「代」、「人」。例如：[董襲](../Page/董襲.md "wikilink")，字元世改為元代、[柳宗元](../Page/柳宗元.md "wikilink")《[捕蛇者说](../Page/捕蛇者说.md "wikilink")》中句子“以俟夫观人风者得焉”中的“人风”实际上是指“民风”，是为此而改。
17. 避[唐高宗李治諱](../Page/唐高宗.md "wikilink")，文章內“治”字改為“理”。
18. 避[武則天武曌諱](../Page/武則天.md "wikilink")，廬陵王（稍後的[唐中宗](../Page/唐中宗.md "wikilink")）之長子李重照改名[李重润](../Page/李重润.md "wikilink")。（曌為照的[则天文字](../Page/则天文字.md "wikilink")）
19. 避[晉高祖石敬瑭諱](../Page/石敬瑭.md "wikilink")，有些[敬姓人士改为](../Page/敬姓.md "wikilink")[端姓](../Page/端姓.md "wikilink")，或拆字變為[苟姓以及](../Page/苟姓.md "wikilink")[文姓](../Page/文姓.md "wikilink")。
20. 避[宋太祖趙匡胤諱](../Page/宋太祖.md "wikilink")，[宋太宗趙匡義改名为趙光義](../Page/宋太宗.md "wikilink")。
21. [孔子之名丘曾經是](../Page/孔子.md "wikilink")[金朝的](../Page/金朝.md "wikilink")**聖人諱**，所以有些[丘姓人士改为](../Page/丘姓.md "wikilink")[邱姓](../Page/邱姓.md "wikilink")。
22. 「元年」改為「始年」、「原年」，「元來」改為「原來」，非避[明太祖朱元璋諱](../Page/明太祖.md "wikilink")，明代所有名諱皆依古禮：「二名不偏諱，嫌名不諱」。而是避元朝之故。\[9\]蓋民間追恨元人，不欲書其國號也。\[10\]

<!-- end list -->

1.  避[清聖祖玄燁讳](../Page/康熙帝.md "wikilink")，[玄武門改](../Page/玄武門.md "wikilink")[神武門](../Page/神武門.md "wikilink")，而“玄孫”改“元孫”。
2.  避[清世宗胤禛諱](../Page/雍正帝.md "wikilink")，兄弟[字輩](../Page/字輩.md "wikilink")“胤”改“允”。趙匡胤通稱為「宋祖」甚至改稱趙匡續，王士禛改稱王士正或[王士禎](../Page/王士禎.md "wikilink")。[真定府改为](../Page/真定府.md "wikilink")[正定府](../Page/正定府.md "wikilink")。
3.  避清[清高宗弘曆諱](../Page/乾隆帝.md "wikilink")，-{曆}-常改為-{歷}-，而大清曆法[時憲曆改時憲書](../Page/時憲曆.md "wikilink")，[弘文館改](../Page/弘文館.md "wikilink")「宏文館」。
4.  避清[清宣宗旻寧讳](../Page/道光帝.md "wikilink")，[北京外城廣寧門改名](../Page/北京.md "wikilink")[廣安門](../Page/廣安門.md "wikilink")。

<!-- end list -->

  - 史上亦有皇帝即位時，將名字易為罕用字，使擾民程度降低。例：

<!-- end list -->

1.  [漢宣帝劉詢](../Page/漢宣帝.md "wikilink")，原名病已。
2.  [清仁宗顒琰](../Page/嘉慶帝.md "wikilink")（Yóngyǎn, ㄩㄥˊ ㄧㄢˇ），1796年即位前原名永琰。
3.  [清宣宗旻寧](../Page/道光帝.md "wikilink")，1820年即位前原名綿寧。

### 空字

空字法：遇到要避諱的字時不填寫。如[唐朝](../Page/唐朝.md "wikilink")[將領](../Page/將領.md "wikilink")[李世勣被改成](../Page/李世勣.md "wikilink")[李勣](../Page/李勣.md "wikilink")，以避[唐太宗李世民的讳](../Page/唐太宗.md "wikilink")。但最著名的例子是“[觀世音](../Page/觀世音.md "wikilink")[菩薩](../Page/菩薩.md "wikilink")”亦因避唐太宗諱，略作“[觀音](../Page/觀音.md "wikilink")”，但這個說法仍待商榷。

### 缺筆

[Omitting_a_stroke.png](https://zh.wikipedia.org/wiki/File:Omitting_a_stroke.png "fig:Omitting_a_stroke.png")
陳垣\[11\]認為避諱缺筆，始於[唐朝](../Page/唐朝.md "wikilink")，[貞觀十八年蓋的](../Page/貞觀.md "wikilink")[文達碑](../Page/文達碑.md "wikilink")，有「世子」二字，時[唐太宗稱](../Page/唐太宗.md "wikilink")[李世民](../Page/李世民.md "wikilink")，可見仍未有**避諱缺筆**。[乾封元年](../Page/乾封.md "wikilink")《[賜泰師孔宣公碑](../Page/賜泰師孔宣公碑.md "wikilink")》中，「愚智齊泯」中的「泯」作「汦」，因此他認為[唐高宗是避諱缺筆之始](../Page/唐高宗.md "wikilink")。到了[高宗](../Page/高宗.md "wikilink")[顯慶五年正月詔曰](../Page/顯慶.md "wikilink")：「至於朕名，或缺其點畫。」說明了當時已經有缺筆避諱。

一般缺筆是缺寫尾一筆。如「愚智泯」的「泯」字，因諱[唐太宗](../Page/唐太宗.md "wikilink")[李世民](../Page/李世民.md "wikilink")，而作「汦」。《于志宁碑》中「世武」把唐太宗的名字李世民中的「世」少寫一劃，作「卅武」。[舊唐書](../Page/舊唐書.md "wikilink")[穆宗紀](../Page/唐穆宗.md "wikilink")：「恒王房子孫改為汦王房。」汦王應為愍王，因避諱，改民為氏而誤。《[續漢郡國志](../Page/續漢郡國志.md "wikilink")》中載[敦煌郡有拼泉](../Page/敦煌.md "wikilink")，拼泉即淵泉，因[唐太祖](../Page/唐太祖.md "wikilink")[李淵缺筆的淵字缺尾的一筆](../Page/李淵.md "wikilink")，後訛為拼。

宋人避[宋聖祖](../Page/宋聖祖.md "wikilink")[趙玄朗諱](../Page/趙玄朗.md "wikilink")，「朗」改為「郎」。[劉焉](../Page/劉焉.md "wikilink")，字君朗改為君郎、《[新唐書](../Page/新唐書.md "wikilink")·后妃則天皇傳》：「前鋒左豹韜果毅[成三郎為](../Page/成三朗.md "wikilink")[唐之奇所殺](../Page/唐之奇.md "wikilink")。」

《[康熙字典](../Page/康熙字典.md "wikilink")》將康熙帝玄燁諱各缺一筆。

日本[明治時代](../Page/明治.md "wikilink")，對於[仁孝天皇](../Page/仁孝天皇.md "wikilink")（惠仁）、[孝明天皇](../Page/孝明天皇.md "wikilink")（統仁）、[明治天皇](../Page/明治天皇.md "wikilink")（睦仁）的名諱「惠」、「統」、「睦」缺一筆。

#### Unicode收录的避讳缺笔字

Unicode中收录了因避讳产生的缺笔字數個。

  - 𤣥
  - 𤍞
  - 𢎞

### 拆字

拆字法：當遇到要避諱的字時，將該字拆開，以描述其字的構造。

1.  [宋太宗諱趙炅](../Page/宋太宗.md "wikilink")，又名趙耿；「炅」、「耿」二字皆為國諱。宋朝人將「耿」字寫作「從耳從火」。\[12\]
2.  [宋真宗諱趙恒](../Page/宋真宗.md "wikilink")，「恒」為國諱，遂將「恒」寫作「從心從亘」。\[13\]
3.  [宋高宗諱趙構](../Page/宋高宗.md "wikilink")，「構」字甚至其音相近的二十餘字皆為國諱。時人為避諱，將與其音相近的「句」寫作「從勹從口」。\[14\]

## 各地情況

### 中國

周朝時避諱情況較為寬鬆，只有完全相同的字才要避諱，同音或近音字大致上不用。秦汉时期，大一统的政局形成并得到巩固，君主之尊无以复加，儒学在上层建筑中逐渐占统治优势，这种要求遂变得非常严格。[地名](../Page/地名.md "wikilink")、物名、[官名](../Page/官名.md "wikilink")、[人名都要回避當朝](../Page/人名.md "wikilink")[天子甚至](../Page/天子.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")[祖先的名字](../Page/祖先.md "wikilink")。[郑玄](../Page/郑玄.md "wikilink")《[毛诗笺注](../Page/毛诗.md "wikilink")》“天下邦国，至于困穷”言及[汉高祖](../Page/汉高祖.md "wikilink")（刘邦）名讳而并未受到查处，可见汉朝时对避讳的规制不如清代时严格。[司马迁](../Page/司马迁.md "wikilink")《史记·[张仪列传](../Page/张仪.md "wikilink")》“是故天下之游谈士，莫不日夜搤腕”言及其父（[司马谈](../Page/司马谈.md "wikilink")）名讳，但這亦有可能是司馬遷抄錄其父司馬談留下的資料，司馬談自然不需要避諱自己了。

[东晋](../Page/东晋.md "wikilink")[太元十三年](../Page/太元_\(东晋\).md "wikilink")（388年），侍中[孔安国](../Page/孔安国_\(东晋\).md "wikilink")[上表](../Page/上表.md "wikilink")，因[黄门郎王愉名犯其父](../Page/黄门郎.md "wikilink")[孔愉名讳](../Page/孔愉.md "wikilink")，不能共同签名，求解。[有司认为](../Page/有司.md "wikilink")：此前，尚书、安众男某某曾上表中兵曹郎王祐犯其父私讳。同意调换部门，这是朝廷制度之外的恩典。但此例一开，官员效仿，不知何时结束。于是断绝了此类请求\[15\]。但避讳却是日渐严格。避各类私讳，亦是后世官员注重的事项\[16\]。

到了[唐朝](../Page/唐朝.md "wikilink")，有一个字相同甚至是同音，也必须回避了。当时著名诗人[李贺就是因为其父名](../Page/李贺.md "wikilink")“晋肃”中，而不得参加[进士考试](../Page/进士.md "wikilink")，致使终身不得志；[韓愈為此事非常生氣](../Page/韓愈.md "wikilink")，为之作《[讳辩](../Page/s:諱辯.md "wikilink")》驳斥，“父名晋肃，子不得举进士；若父名仁，子不能为人乎！”[唐朝法律规定](../Page/唐律疏议.md "wikilink")，直呼皇帝的名字犯“[大不敬](../Page/大不敬.md "wikilink")”罪，不能赦免。然而[杜牧](../Page/杜牧.md "wikilink")《[阿房宫赋](../Page/阿房宫赋.md "wikilink")》“一旦不能有”、[柳宗元](../Page/柳宗元.md "wikilink")《[捕蛇者说](../Page/捕蛇者说.md "wikilink")》“岂若吾乡邻之旦旦有是哉”皆言及[唐睿宗](../Page/唐睿宗.md "wikilink")（李旦）名讳，[元稹](../Page/元稹.md "wikilink")《连昌宫词》“舞榭欹倾基尚在”又言及[唐玄宗](../Page/唐玄宗.md "wikilink")（李隆基）名讳而并未受到查处，可见唐朝时对避讳的规制亦不如清代时严格。

除了文字避諱，生活上亦有其他形式的名諱。例如[唐朝避諱李姓](../Page/唐朝.md "wikilink")，禁食[鯉魚](../Page/鯉魚.md "wikilink")，抓到鯉魚必須放掉，販賣鯉魚責打60杖。\[17\]惟此類規定有時流於形式。另明朝人不禁吃豬肉。\[18\]

據稱[五代十國時](../Page/五代十國.md "wikilink")[馮道喜人誦讀](../Page/馮道.md "wikilink")《[老子](../Page/道德經.md "wikilink")》，而誦書的[侍從為了避馮道的名諱](../Page/侍從.md "wikilink")，而將「道」這個字給讀成「不可說」，於是他便將《老子》的開頭給讀成了「不可說可不可說，非常不可說」，結果大家都覺得很好笑。\[19\]

宋代，讳法大兴，一个帝王的避讳字数量急剧增多，《容齋三筆·十一》云：「本朝尚文之習大盛，故禮官討論，每欲其多，廟諱有五十字者。舉場試卷，小涉疑似，士人輒不敢用，一或犯之，往往暗行黜落。」

元代是蒙古人入主中原，蒙古人本无避讳之事，因此元代不太在意避諱。忽必烈行汉法之后，曾下令避讳，但终元一代讳法不严，一部《元史》，惟《[程鉅夫列傳](../Page/程鉅夫.md "wikilink")》言：「鉅夫名文海，避武宗廟諱，以字行。」餘無所見\[20\]，程钜夫生于宋，故习惯避讳，而考察《元史·武宗纪》，武宗朝名中有“海”者多达七人，武宗朝奏章地名有“海”字者众多，并不避讳，故程钜夫避讳一事属于此人特例\[21\]。蒙古人中君臣同名者甚多。

明代因之，二字之名在用到其中某一字时不避讳，音近而有称名之嫌，亦不需避諱；所謂「二名不偏讳，嫌名不讳」，故親王可與皇帝共用[字輩](../Page/行輩.md "wikilink")。

满人早期亦不避讳，后来汉化加深，从玄烨开始\[22\]，才跟着汉人那样有了避讳之事。清乾隆二十二年（1757年），退休的[布政使](../Page/布政使.md "wikilink")[彭家屏在族譜](../Page/彭家屏.md "wikilink")《大彭統記》刊登[河南](../Page/河南.md "wikilink")[生員](../Page/生員.md "wikilink")[段昌緒家存](../Page/段昌緒.md "wikilink")[吳三桂文告時](../Page/吳三桂.md "wikilink")，[乾隆帝之名諱弘曆不缺筆](../Page/乾隆帝.md "wikilink")，成為彭家屏[自殺及段昌緒](../Page/自殺.md "wikilink")[斬首罪名之一](../Page/斬首.md "wikilink")。乾隆四十二年（1777年），江西書生[王錫侯在他刪](../Page/王錫侯.md "wikilink")[康熙字典寫的字典](../Page/康熙字典.md "wikilink")《[字貫](../Page/字貫.md "wikilink")》凡例寫入[康熙帝](../Page/康熙帝.md "wikilink")、[雍正帝](../Page/雍正帝.md "wikilink")、乾隆帝之名諱（玄燁、胤禛、弘曆），沒有缺筆避諱，也有批評康熙字典“然而穿貫之難”的内容，被認為非常不敬，而被依大逆律定罪處斬。\[23\]清朝「末代皇帝」[溥儀在位時](../Page/溥儀.md "wikilink")，“儀”曾經是避諱字。\[24\]

[清朝滅亡後](../Page/清朝滅亡.md "wikilink")，中國不再避國諱和圣人讳。现时一般中国人仍避家讳，但仅限于起名，日常使用是不避的，亦有连起名也不避讳的。

### 日本

日本避諱的相關記錄始見於[奈良時代](../Page/奈良時代.md "wikilink")。[延曆年間](../Page/延曆.md "wikilink")，[桓武天皇為避其父](../Page/桓武天皇.md "wikilink")[光仁天皇](../Page/光仁天皇.md "wikilink")（諱白壁王）的諱，將[常陸國的](../Page/常陸國.md "wikilink")「白壁郡」改名為「[真壁郡](../Page/真壁郡.md "wikilink")」。[淳和天皇即位的時候](../Page/淳和天皇.md "wikilink")，由於天皇名叫「大伴」，氏族「[大伴氏](../Page/大伴氏.md "wikilink")」便被改名為「伴氏」。

進入[幕府時期以後](../Page/幕府.md "wikilink")，[日本武士對主君的名字也要避諱](../Page/日本武士.md "wikilink")。當時[日本人名分為](../Page/日本人名.md "wikilink")「通字」和「偏諱」兩個部份。例如在「[德川家康](../Page/德川家康.md "wikilink")」的名字中，「家」字為通字，由家族時代相傳；「康」字為偏諱。通字無需避諱，但偏諱要避。在[大坂之役中](../Page/大坂之役.md "wikilink")，[豐臣秀賴沒有避德川家康的偏諱](../Page/豐臣秀賴.md "wikilink")，為德川家康出兵大坂提供了口實。不過有時候，日本武士也會將偏諱賜給屬下，以示恩寵。

日本直到[二戰結束之前依然要避諱](../Page/二戰.md "wikilink")，否則是對皇室的[不敬](../Page/不敬.md "wikilink")。例如，為避[仁孝天皇](../Page/仁孝天皇.md "wikilink")（惠仁）、[孝明天皇](../Page/孝明天皇.md "wikilink")（統仁）、[明治天皇](../Page/明治天皇.md "wikilink")（睦仁）的諱，將「惠」、「統」、「睦」三字都缺一筆。此外，對皇后也要避諱。例如，[大正天皇的皇后名叫](../Page/大正天皇.md "wikilink")[九條節子](../Page/貞明皇后.md "wikilink")（），[秩父宮雍仁親王的妃子叫](../Page/秩父宮雍仁親王.md "wikilink")[松平節子](../Page/松平勢津子.md "wikilink")（）；九條節子成為皇后之後，松平節子為了避諱，將名字改為同音的「勢津子」（）。

1947年（昭和22年）10月26日，日本國憲法廢除了對皇室不敬的罪名，不再將避諱列入法律條文之內。

### 琉球

受中國文化的影響，琉球王府也要求避國王的諱。其避諱的方式主要為改字。例如，為避[尚哲王諱](../Page/尚哲王.md "wikilink")，「哲」字改為「明」；\[25\]避[尚溫王諱](../Page/尚溫王.md "wikilink")，「溫」字改為「和」；\[26\]避[尚泰王諱](../Page/尚泰王.md "wikilink")，「泰」字改為「春」。\[27\]此外，琉球人也要避中國皇帝的諱。\[28\]\[29\]

琉球人要避國王的諱，但不必避國王使用的[名乘](../Page/名乘.md "wikilink")。例如，尚溫王的名乘為「朝克」，\[30\]但後世仍有一名[三司官名叫](../Page/三司官.md "wikilink")「[馬克承](../Page/馬克承.md "wikilink")」；馬克承的名乘為「小祿親方良泰」，尚泰王繼位後，為避諱，改為「良忠」。\[31\]

1609年[慶長琉球之役之後](../Page/慶長琉球之役.md "wikilink")，琉球成為日本[薩摩藩的附庸國](../Page/薩摩藩.md "wikilink")，薩摩藩在琉球設置[在番奉行所以監督內政](../Page/在番奉行所.md "wikilink")。1755年，[島津重豪就任薩摩藩藩主之後](../Page/島津重豪.md "wikilink")，薩摩藩曾一度要求琉球王府下令避[江戶幕府將軍的諱](../Page/江戶幕府.md "wikilink")。例如，[毛宣猷為避](../Page/毛宣猷.md "wikilink")[德川家宣的諱](../Page/德川家宣.md "wikilink")，改名為[毛廷柱](../Page/毛廷柱.md "wikilink")；\[32\][向宣謨的名乘原為](../Page/向宣謨.md "wikilink")「今歸仁朝忠」，為避[德川秀忠諱](../Page/德川秀忠.md "wikilink")，改為「今歸仁朝義」。\[33\]薩摩藩又禁止琉球人使用「德」、「千代」、「雄」、「敦」、「信」、「明」、「重」等字作為名字。不過在島津重豪死後，琉球便廢除了這個命令。為避諱而更改[名乘頭的家族](../Page/琉球人名.md "wikilink")，多獲准恢復使用原先的名乘頭；\[34\]但仍有家族的名乘頭未被改回。\[35\]

### 朝鮮半島

朝鮮半島的避諱可能始於[統一新羅時代](../Page/統一新羅.md "wikilink")。在《[文武王陵碑](../Page/文武王陵碑.md "wikilink")》的碑文里，「[丙午](../Page/丙午.md "wikilink")」被寫作「景午」。[唐高祖之父名叫](../Page/唐高祖.md "wikilink")[李昞](../Page/李昞.md "wikilink")，「昞」字和同音的「丙」字都是需避諱的字，故而[干支紀年中將丙字改作](../Page/干支.md "wikilink")「景」。但是，新羅當時剛剛從[中國引進干支紀年](../Page/唐朝.md "wikilink")，沒有證據顯示新羅是否也從中國引入了避諱制度。

不過在[高麗王朝時期的法律明確規定需要避高麗國王的諱](../Page/高麗王朝.md "wikilink")。高麗時代的避諱方法主要為缺筆，故而在《[高麗大藏經](../Page/高麗大藏經.md "wikilink")》中可以看見不少缺畫字。\[36\]此外也有改字的方法，例如[高麗惠宗名叫王武](../Page/高麗惠宗.md "wikilink")，便將「文武兩班」改稱「文虎兩班」，\[37\]後又改稱「龍虎兩班」。又如，《[三國遺事](../Page/三國遺事.md "wikilink")》中，為避[高麗定宗王堯諱](../Page/高麗定宗.md "wikilink")，將「堯」字改為「高」。

1750年，為了避免在[文廟祭祀中衝犯孔子的諱](../Page/文廟.md "wikilink")，[朝鮮英祖下令將](../Page/朝鮮英祖.md "wikilink")「大丘郡」改名為「[大邱郡](../Page/大邱廣域市.md "wikilink")」。\[38\]

[興宣大院君重建](../Page/興宣大院君.md "wikilink")[景福宮](../Page/景福宮.md "wikilink")[弘禮門時](../Page/弘禮門.md "wikilink")，為避清高宗弘曆諱，將「弘禮門」改名為「興禮門」。

### 越南

[Ti_huy_nha_nguyen.PNG](https://zh.wikipedia.org/wiki/File:Ti_huy_nha_nguyen.PNG "fig:Ti_huy_nha_nguyen.PNG")》中的避諱舉例\]\]
越南的避諱，除了改字、空字、缺筆、拆字之外，**變音**是其一大特色。

越南的避諱，最早見於[陳朝時期](../Page/陳朝_\(越南\).md "wikilink")。1232年（[建中八年](../Page/建中.md "wikilink")），[陳太宗下達避諱的命令](../Page/陳太宗.md "wikilink")。為了避陳朝皇帝祖先[陳李的諱](../Page/陳李.md "wikilink")，陳太宗下令將全國的[李氏全部改為](../Page/李姓.md "wikilink")[阮氏](../Page/阮姓.md "wikilink")。

在越南，中國的避諱方法改字、空字、缺筆、拆字都有。例如，在[成泰年間刊行的](../Page/成泰.md "wikilink")《[大南實錄](../Page/大南實錄.md "wikilink")》的「大南正編列傳初集」之中，為避[嗣德帝阮福時諱](../Page/嗣德帝.md "wikilink")，改「時」字為「辰」字；為避[成泰帝阮福昭諱](../Page/成泰帝.md "wikilink")，便將「昭」字缺筆寫作「召」。在《大南實錄》的「前編」中，歷代阮主的名諱皆空字。\[39\]\[40\]\[41\]在《[國朝正編撮要](../Page/國朝正編撮要.md "wikilink")》中，為避[嘉隆帝阮福](../Page/嘉隆帝.md "wikilink")-{暎}-諱，寫作「左從日右從英」。\[42\]

還有一種被稱為變音的避諱方法，即將皇帝名諱的讀音改變，與需避諱字的同音字也隨之變音。變音這種避諱方法為越南特色，始見於[後黎朝時代](../Page/後黎朝.md "wikilink")。[黎太祖諱黎利](../Page/黎太祖.md "wikilink")，「利」字在越南語中本讀作「lì」，為避諱，改為讀「lợi」。[阮朝時代](../Page/阮朝.md "wikilink")，為避歷代皇帝的[垫名](../Page/越南人名#垫名.md "wikilink")「福」之諱，將「福」字的發音從「phúc」改為「phước」，所有讀作「phúc」的字也統統改為讀「phước」。

此外，也存在口頭和書面上不同的避諱方法。例如[嗣德帝名](../Page/嗣德帝.md "wikilink")「阮福時」，在書面上若遇上「時」字，應將「時」字改寫作「辰」字；而在口頭上，「時」字本應讀作「thì」，為避諱改讀「thời」。

在[鄭阮紛爭時期](../Page/鄭阮紛爭.md "wikilink")，[廣南阮主轄境內的居民必須避阮主之諱](../Page/阮主.md "wikilink")。例如，為避[阮潢諱](../Page/阮潢.md "wikilink")，將「hoàng」讀作「huỳnh」。時至今日，越南南部的[黃姓居民仍將自己的姓氏讀作](../Page/黃姓.md "wikilink")「huỳnh」，但北部居民仍讀作「hoàng」。

阮朝[明命帝時代](../Page/明命帝.md "wikilink")，將眾多的阮主支系子孫删去姓氏，冠以「宗室」（）二字，标榜其地位。後來[紹治帝繼位](../Page/紹治帝.md "wikilink")，為避紹治帝阮福綿宗之諱，又將「宗室」改姓為近音的「尊室」（），這便是今日越南[尊室姓的由來](../Page/尊室.md "wikilink")。

以下是阮朝的變音避諱用字表。其中，被避諱之字以粗體標出。

<table>
<caption><strong>阮朝變音避諱用字</strong></caption>
<thead>
<tr class="header">
<th><p>正音<br />
（本音）</p></th>
<th><p>寨音<br />
（變音）</p></th>
<th><p>被避諱者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>cam</p></td>
<td><p>kim</p></td>
<td><p>肇祖<a href="../Page/阮淦.md" title="wikilink">阮<strong>淦</strong></a></p></td>
</tr>
<tr class="even">
<td><p>mai</p></td>
<td><p>mơi</p></td>
<td><p>靖皇后<a href="../Page/阮氏梅.md" title="wikilink">阮氏<strong>梅</strong></a></p></td>
</tr>
<tr class="odd">
<td><p>hoàng</p></td>
<td><p>huỳnh</p></td>
<td><p>太祖<a href="../Page/阮潢.md" title="wikilink">阮<strong>潢</strong></a></p></td>
</tr>
<tr class="even">
<td><p>nguyên</p></td>
<td><p>ngươn</p></td>
<td><p>熙宗<a href="../Page/阮福源.md" title="wikilink">阮福<strong>源</strong></a></p></td>
</tr>
<tr class="odd">
<td><p>lan</p></td>
<td><p>lang, lam</p></td>
<td><p>神宗<a href="../Page/阮福瀾.md" title="wikilink">阮福<strong>瀾</strong></a></p></td>
</tr>
<tr class="even">
<td><p>tần</p></td>
<td><p>tờn</p></td>
<td><p>太宗<a href="../Page/阮福瀕.md" title="wikilink">阮福<strong>瀕</strong></a></p></td>
</tr>
<tr class="odd">
<td><p>ngàn</p></td>
<td><p>nghìn</p></td>
<td><p>英宗<a href="../Page/阮福溙.md" title="wikilink">阮福溙又名阮福</a><strong>彦</strong></p></td>
</tr>
<tr class="even">
<td><p>thụy</p></td>
<td><p>thoại</p></td>
<td><p>保大帝<a href="../Page/阮福永瑞.md" title="wikilink">阮福永<strong>瑞</strong></a></p></td>
</tr>
<tr class="odd">
<td><p>lĩnh</p></td>
<td><p>lãnh</p></td>
<td><p>孝義皇后<a href="../Page/宋氏領.md" title="wikilink">宋氏<strong>領</strong></a></p></td>
</tr>
<tr class="even">
<td><p>chu</p></td>
<td><p>châu</p></td>
<td><p>顯宗<a href="../Page/阮福淍.md" title="wikilink">阮福<strong>淍</strong></a></p></td>
</tr>
<tr class="odd">
<td><p>được</p></td>
<td><p>đặng</p></td>
<td><p>孝明皇后<a href="../Page/宋氏特.md" title="wikilink">宋氏<strong>特</strong></a></p></td>
</tr>
<tr class="even">
<td><p>thụ</p></td>
<td><p>thọ</p></td>
<td><p>肅宗<a href="../Page/阮福澍.md" title="wikilink">阮福<strong>澍</strong></a></p></td>
</tr>
<tr class="odd">
<td><p>thư</p></td>
<td><p>thơ</p></td>
<td><p>孝寧皇后<a href="../Page/張氏書.md" title="wikilink">張氏<strong>書</strong></a></p></td>
</tr>
<tr class="even">
<td><p>dong</p></td>
<td><p>đông</p></td>
<td><p>孝武皇后<a href="../Page/張氏容.md" title="wikilink">張氏<strong>容</strong></a></p></td>
</tr>
<tr class="odd">
<td><p>hoàn</p></td>
<td><p>hườn</p></td>
<td><p>孝康皇后<a href="../Page/阮氏環_(阮興祖).md" title="wikilink">阮氏<strong>環</strong></a></p></td>
</tr>
<tr class="even">
<td><p>phúc</p></td>
<td><p>phước</p></td>
<td><p>阮<strong>福</strong>氏</p></td>
</tr>
<tr class="odd">
<td><p>ánh</p></td>
<td><p>yên, yếng, ảnh</p></td>
<td><p><a href="../Page/嘉隆帝.md" title="wikilink">嘉隆帝阮福</a><strong>-{暎}-</strong></p></td>
</tr>
<tr class="even">
<td><p>chủng</p></td>
<td><p>chưởng</p></td>
<td><p>阮福<strong>種</strong>（嘉隆帝的原名）</p></td>
</tr>
<tr class="odd">
<td><p>đang</p></td>
<td><p>đởm</p></td>
<td><p><a href="../Page/順天高皇后.md" title="wikilink">順天高皇后陳氏</a><strong>璫</strong></p></td>
</tr>
<tr class="even">
<td><p>cảnh</p></td>
<td><p>kiểng</p></td>
<td><p>順天高皇后又名<strong>敬</strong></p></td>
</tr>
<tr class="odd">
<td><p>đảm</p></td>
<td><p>đởm</p></td>
<td><p>阮福<strong>膽</strong>（明命帝原名）</p></td>
</tr>
<tr class="even">
<td><p>kiểu</p></td>
<td><p>cảo</p></td>
<td><p><a href="../Page/明命帝.md" title="wikilink">明命帝阮福</a><strong>晈</strong></p></td>
</tr>
<tr class="odd">
<td><p>hoa</p></td>
<td><p>huê</p></td>
<td><p><a href="../Page/佐天仁皇后.md" title="wikilink">佐天仁皇后胡氏</a><strong>華</strong></p></td>
</tr>
<tr class="even">
<td><p>thật</p></td>
<td><p>thiệt, thực</p></td>
<td><p><a href="../Page/佐天仁皇后.md" title="wikilink">佐天仁皇后胡氏</a><strong>實</strong>（嘉隆帝賜名）</p></td>
</tr>
<tr class="odd">
<td><p>miên</p></td>
<td><p>mân</p></td>
<td><p>阮福<strong>綿</strong>宗（紹治帝別名）</p></td>
</tr>
<tr class="even">
<td><p>chính</p></td>
<td><p>chánh</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>tông</p></td>
<td><p>tôn</p></td>
<td><p>阮福綿<strong>宗</strong>（紹治帝別名）</p></td>
</tr>
<tr class="even">
<td><p>dung</p></td>
<td><p>dong</p></td>
<td><p>阮福<strong>曧</strong>（紹治帝別名）</p></td>
</tr>
<tr class="odd">
<td><p>tuyền</p></td>
<td><p>toàn</p></td>
<td><p><a href="../Page/紹治帝.md" title="wikilink">紹治帝阮福</a><strong>暶</strong></p></td>
</tr>
<tr class="even">
<td><p>hằng</p></td>
<td><p>thường</p></td>
<td><p><a href="../Page/慈裕太后.md" title="wikilink">慈裕太后范氏</a><strong>姮</strong></p></td>
</tr>
<tr class="odd">
<td><p>hạo</p></td>
<td><p>hiệu</p></td>
<td><p><a href="../Page/建福帝.md" title="wikilink">建福帝阮福</a><strong>昊</strong></p></td>
</tr>
<tr class="even">
<td><p>nhậm</p></td>
<td><p>nhiệm</p></td>
<td><p>阮福洪<strong>任</strong>（嗣德帝別名）</p></td>
</tr>
<tr class="odd">
<td><p>hồng</p></td>
<td><p>hường</p></td>
<td><p>阮福<strong>洪</strong>任（嗣德帝別名）</p></td>
</tr>
<tr class="even">
<td><p>thì</p></td>
<td><p>thời</p></td>
<td><p><a href="../Page/嗣德帝.md" title="wikilink">嗣德帝阮福</a><strong>時</strong></p></td>
</tr>
<tr class="odd">
<td><p>hài</p></td>
<td><p>hia</p></td>
<td><p>儷天英皇后<a href="../Page/武氏諧.md" title="wikilink">武氏<strong>諧</strong></a></p></td>
</tr>
<tr class="even">
<td><p>chân</p></td>
<td><p>chơn</p></td>
<td><p><a href="../Page/育德帝.md" title="wikilink">育德帝阮福膺</a><strong>禛</strong></p></td>
</tr>
<tr class="odd">
<td><p>đường</p></td>
<td><p>đàng</p></td>
<td><p><a href="../Page/同慶帝.md" title="wikilink">同慶帝阮福膺</a><strong>禟</strong></p></td>
</tr>
<tr class="even">
<td><p>lân</p></td>
<td><p>liên</p></td>
<td><p><a href="../Page/成泰帝.md" title="wikilink">成泰帝阮福寶</a><strong>嶙</strong></p></td>
</tr>
<tr class="odd">
<td><p>san</p></td>
<td><p>sơn</p></td>
<td><p><a href="../Page/維新帝.md" title="wikilink">維新帝阮福永</a><strong>珊</strong></p></td>
</tr>
<tr class="even">
<td><p>điều</p></td>
<td><p>đều</p></td>
<td><p>慈明惠皇后<a href="../Page/潘氏調.md" title="wikilink">潘氏<strong>調</strong></a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 現代

现在虽然已经不讲究这些避讳習俗了，但是仍然有部分人在取名时尽量不取同父辈或祖辈同名或同音的字作为名字。由于避讳的影响，现在的東亞人还是认为直呼长辈的名字为不敬，所以會特別-{注}-意親屬稱謂。

## 例外情況

雖然不取與父輩或祖輩名字相同的用字或同音字是傳統避諱方式之一，但亦有例外。例如[東晉時](../Page/東晉.md "wikilink")[王羲之的七個兒子](../Page/王羲之.md "wikilink")[王玄之](../Page/王玄之.md "wikilink")、[王凝之](../Page/王凝之.md "wikilink")、[王渙之](../Page/王渙之.md "wikilink")、[王肅之](../Page/王肅之.md "wikilink")、[王徽之](../Page/王徽之.md "wikilink")、[王操之](../Page/王操之.md "wikilink")、[王獻之都有](../Page/王獻之.md "wikilink")「之」字，据[陈寅恪考证](../Page/陈寅恪.md "wikilink")“之”字是[天师教信徒的标志](../Page/天师教.md "wikilink")；而汪婕指出，这仅仅是“二名不偏讳”，与天师教信仰无关\[43\]。

## 避讳的后果及用途

避讳徒增文章的理解难度，若不了解相应的避讳情况，可能无法正确认出原词，导致文章难以理解。

另外，避讳本身提供了一种文献断代方法，如果某书中避某字讳，则可以断定该书是相对应的某位皇帝继位后著述的。

## 评价

现代，避讳被认为是强化君主专制的手段而遭到批判。

## 漢字文化圈以外

### 猶太人

由於《[希伯来聖經](../Page/希伯来聖經.md "wikilink")》中的《[十誡](../Page/十誡.md "wikilink")》有「不可妄稱[YHWH你神的名](../Page/四字神名.md "wikilink")」的誡命，犹太教文獻中的四字神名（）不标[母音](../Page/母音.md "wikilink")，或标上"adonai"的母音作为提醒，而只写4個[子音字母](../Page/子音.md "wikilink")，一般稱為“4字神名”——英語的**YHWH**、法語和德語的**JHWH**、拉丁語的**IHVH**。[中世紀時猶太学者為希伯來語的](../Page/中世紀.md "wikilink")4字神名標上母音時，使用「adonai」（意為「我的主」）的母音符号，（希伯來語单詞的輔音写在中央而元音（母音）写在輔音的上面或下面，以不同组合的点和短线表示，所以可以在「YHWH」下写「adonai」的元音符号），提醒猶太裔讀者要念成「adonai」，不可以直呼神的名。而早期聖經譯者翻譯時，便直接地把輔音和元音拼在一起，而成了「YaHoWaH」（翻譯就是「耶和華」）。

### 大溪地人

在玻里尼西亞的許多地方，重要領導人的名字被認為是神聖的(至今有時依舊如此，詳情可見[Tapu](../Page/玻里尼西亞文化中的禁忌.md "wikilink"))，並因此對之需有一些適當的敬重([mana](../Page/瑪那.md "wikilink"))。為了避免冒犯，任何類似一個名字的字眼都不可使用，並直到該人死去為止，都需以一個含義相似的詞來代替被禁止使用的字眼。然若一個領袖碰巧活得非常長命的話，這暫時性的替代用法，可能會變成永久性的，進而對語言產生影響。

一個例子是在[大溪地語當中](../Page/大溪地語.md "wikilink")，一些單詞因為避諱的緣故，而被其他的單字給取代。在玻里尼西亞的其他地區，tū或其他同源的字之意為「站立」，但在大溪地語中，該字被以tiʻa取代，因為原本的詞根變成了[波马雷一世這位國王的名字中Tū](../Page/波马雷一世.md "wikilink")-nui-ʻēʻa-i-te-atua的部份；相似地，fetū這個意指「星星」的字眼，在大溪地語中為fetiʻa所取代；而aratū這個意指「柱子」的字眼，也為aratiʻa所取代。

儘管nui(意即「大的」)依舊出現於像Tahiti-nui等合成詞中，一般用以指稱「大」的字眼為rahi(此詞在玻里尼西亞語言中是個指稱「大」的常見字眼)；此外，ʻēʻa這個詞也不再使用，並為purūmu或porōmu取而代之，在現今，ʻēʻa之意為「路徑」，而purūmu之意則為「道路」。

Tū也有個暱稱，叫做Pō-mare (字面含意為「夜咳」)，波馬雷一世的王朝也因此而得名，也因此*pō*
(意即「夜晚」)這個字曾一度為*ruʻi*所取代(然今日ruʻi只用於聖經中，pō這個字根今日又重新變成了日常用語的單詞)；而*mare*
(字面之意為「咳嗽」)則已不可逆地為*hota*所取代。

其他範例如下：

  - *vai*(意即「水」)為 *pape*所取代，並可見於Papeari、Papenoʻo、Papeʻete等名稱中。
  - *moe*(意即「睡」)為*taʻoto*(其原始含意為「躺下」)所取代。

一些舊式的單詞依舊用於[背風群島一帶](../Page/背风群岛_\(社会群岛\).md "wikilink")。

### 其他

[JSFuck為了避諱](../Page/JSFuck.md "wikilink")[髒話](../Page/髒話.md "wikilink")[Fuck寫作](../Page/Fuck.md "wikilink")
JSF\*ck。

## 参考文献

### 引用

### 来源

  - [陳垣](../Page/陳垣.md "wikilink")：《史諱舉例》，[上海書店出版社](../Page/上海書店出版社.md "wikilink")，1997年6月出版。ISBN
    7806222529

  - 王建 編：《史諱辭典》，[汲古書院](../Page/汲古書院.md "wikilink")，1997年出版。ISBN
    476291049X

  - 井波陵一
    論文「」，收錄於[京都大學人文科學研究所附屬漢字情報研究中心編纂的](../Page/京都大學人文科學研究所附屬漢字情報研究中心.md "wikilink")《》，2008年

  - Ngô Đức Thọ *Nghiên cứu chữ huy Việt Nam qua các triều đại / Les
    Caractères Interdits au Vietnam à Travers l’Histoire*. (traduit et
    annoté par Emmanuel Poisson, Hà Nội: Nxb Văn hoá, 1997)……

## 外部連結

  - [唐以後帝王避諱名-{}-表](http://140.111.1.40/fulu/fu8/newpage1.htm)

## 参见

  - [名讳](../Page/名讳.md "wikilink")
  - [禁忌](../Page/禁忌.md "wikilink")

{{-}}

[Category:社会学](../Category/社会学.md "wikilink")
[Category:禮儀](../Category/禮儀.md "wikilink")
[Category:東亞傳統](../Category/東亞傳統.md "wikilink")
[Category:禁忌](../Category/禁忌.md "wikilink")

1.  [臺灣](../Page/臺灣.md "wikilink").[教育部](../Page/中華民國教育部.md "wikilink").國語辭典修訂本.【名諱】「尊長或所尊敬的人的名字。」

2.  如[唐玄宗兄长](../Page/唐玄宗.md "wikilink")[李成器避其母](../Page/李憲_\(寧王\).md "wikilink")[昭成皇后](../Page/昭成皇后.md "wikilink")[尊号](../Page/尊号.md "wikilink")，[改名李宪](../Page/改名.md "wikilink")。

3.  张世南《游宦纪闻》卷三说：“殷人以讳事神，而后有字。”钱希言在《戏瑕》中稱避讳之俗於夏商时已有，证据是《山海经》中往往把夏后启写作夏后开。但陈光坚《讳源略说》一文以为此說证据不足。洪迈《容斋三笔·帝王讳名》提出，避讳之制始于周代，但仅在本庙中避讳。赵翼《陔余丛考·避讳》主张避讳习俗始于东周，即春秋时期。陈光坚《讳源略说》亦稱避讳起自春秋时期。

4.  《左传·桓公六年》-{云}-：“周人以讳事神名，终将讳之。”孔颖达疏：“自殷以往，未有讳法。讳始于周。”

5.  《避讳研究》，王新华 著，齐鲁书社

6.

7.  <http://dict.variants.moe.edu.tw/yitia/lda/lda00015.htm>

8.

9.  清·[郝懿行](../Page/郝懿行.md "wikilink")《晉宋書故·元由》：「元，始初也；由，萌蘗也。論事所起，或言元起，或言元來，或言元故，或言元舊，皆是也。今人為書，元俱作原字……蓋起於前明初造，事涉元朝，文字簿書率皆易『元』為原。」

10. 明沈德符《萬曆野獲編》：「俱以原字代元字，蓋民間追恨元人，不欲書其國號也。」

11. 陳垣：《史諱舉例》，（北京：北京科學出版社，1958年），頁5。

12.
13. 《容齋三筆·卷十一·帝王諱名》：「太宗諱字內有從耳從火者，又有梗音，今為人姓如故。高宗諱從勹從口者亦然。真宗諱從心從亘，音胡登切。」

14.
15. 《[晋书](../Page/晋书.md "wikilink")·志第十·礼中》太元十三年，召孔安国为侍中。安国表以黄门郎王愉名犯私讳，不得连署，求解。有司议云：“名终讳之，有心所同，闻名心瞿，亦明前诰。而《礼》复云‘君所无私讳，大夫之所有公讳’，无私讳。又云‘诗书不讳，临文不讳’。岂非公义夺私情，王制屈家礼哉！尚书安众男臣先表中兵曹郎王祐名犯父讳，求解职，明诏爰发，听许换曹，盖是恩出制外耳。而顷者互相瞻式，源流既启，莫知其极。夫皇朝礼大，百僚备职，编官列署，动相经涉。若以私讳，人遂其心，则移官易职，迁流莫已，既违典法，有亏政体。请一断之。”从之。

16. 《[册府元龟](../Page/册府元龟.md "wikilink")·卷八百六十三》◎总录部·名讳

17. [s:酉陽雜俎/卷十七](../Page/s:酉陽雜俎/卷十七.md "wikilink")

18.

19. [論禪之"不可言說"](http://wwwold.hfu.edu.tw/~lbc/BC/5TH/BC0520.HTM)

20. [唐以後帝王避諱名-{}-表：元](http://140.111.1.40/fulu/fu8/page8.htm)

21. 《史讳举例》陈垣

22. 陳垣《史諱舉例》：清之避諱，自康熙帝之漢名始。雍乾之世，避諱至嚴，當時文字獄中，至以詩文筆記之對於廟諱御名有無敬避，為順逆憑證。

23. 《清代名人傳略》中冊頁143

24. [唐以後帝王避諱名-{}-表：清](http://140.111.1.40/fulu/fu8/page10.htm)

25. [《毛氏家譜（與世山家）·五世維基》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/kume/716.htm)：「次男宣哲（哲字因國禁改明）」

26. [《孫氏家譜（安座間家）·四世有温》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/kume/420.htm)：「四世有温（温字因國禁改謂有和）」

27. [《新参林姓家譜（佐久川家）·新参四世泰雄》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/naha/548.htm)：「新参四世泰雄（因泰之字國禁改春）」

28. [《陳姓家譜（仲本家）·十二世》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/kume/495.htm)：「十二世諱弘澤（弘字因禁止改宏）」【註：乾隆帝諱弘曆。】

29. [《曾姓家譜（中宗根家）·六世紫金大夫夔》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/kume/391.htm)：「男曆（曆之字因禁止改信）」【註：乾隆帝諱弘曆。】

30. 《[王代記](http://manwe.lib.u-ryukyu.ac.jp/cgi-bin/disp-img.cgi?page=49&file=iha0210)》，第49頁

31. [《馬姓家譜·小祿家·十二世馬克承》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/syuri/534.htm)：「十二世馬克承童名思龜名乘良泰（因泰之字國禁改忠）」

32. [《毛氏家譜（與世山家）·六世宣猷》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/kume/721.htm)：「六世宣猷（宣字因國禁改廷柱）」

33. [《向姓家譜（具志川家）·十世諱宣謨》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/syuri/265.htm)：「十世諱宣謨
    今歸仁王子 童名思德金 名乘朝忠（忠之字因禁止改義）」

34. [《平姓家譜（當間家）·七世重富》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/naha/468.htm)：「七世重富（重之字因禁止改季因免許如今）」

35. [《英姓家譜（渡名喜家）·六世恒隆》](http://www.tulips.tsukuba.ac.jp/limedio/dlam/B1241188/1/vol05/syuri/45.htm)：「六世恒隆（原是名乘頭重之字因御禁止子孫悉改用恒之字）」

36. [특수표식: 피휘, 각필,
    불심지](http://www.memorykorea.go.kr:7779/teukjing/teukjing1_02.html)

37. 見[鳳巖寺](../Page/鳳巖寺.md "wikilink")《靜眞大師塔碑文》

38. [《朝鮮王朝實錄·英祖實錄·二十六年十二月》](http://sillok.history.go.kr/inspection/insp_king.jsp?id=wua_12612002_002&tabid=w&mTree=0&inResult=0&indextype=1)

39. 《[大南實錄前編·凡例](http://lib.nomfoundation.org/collection/1/volume/179/page/25)》：「地名遇廟諱、尊字，或從今名，或缺一筆。至於人名，皆改別字，以示敬也。」

40. [《大南正編列傳初集·偽西列傳》](http://lib.nomfoundation.org/collection/1/volume/162/page/11)

41. 《[大南實錄前編·卷一](http://lib.nomfoundation.org/collection/1/volume/179/page/36)》

42. 《[國朝正編撮要·卷之一](http://lib.nomfoundation.org/collection/1/volume/336/page/1)》：「世祖高皇帝，諱左從日右從爰，又諱左從日右從英，又諱左從禾右從重。」

43. 汪婕 《对魏晋南北朝"父子不嫌同名"怪现象之思考》 井冈山大学学报·社会科学版 2009第6期