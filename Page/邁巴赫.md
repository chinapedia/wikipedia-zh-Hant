與是一曾經在1921年到1940年間活躍於[歐洲地區的](../Page/歐洲.md "wikilink")[德國超](../Page/德國.md "wikilink")[豪華汽車品牌與製造廠](../Page/豪華汽車.md "wikilink")，車廠創始人的父親[威廉·邁巴赫曾擔任](../Page/威廉·邁巴赫.md "wikilink")[戴姆勒發動機公司](../Page/戴姆勒發動機公司.md "wikilink")（今日[戴姆勒集團前身](../Page/戴姆勒集團.md "wikilink")）的首席技術總監，兩廠淵源甚深。1997年戴姆勒集團在[東京車展中展出一輛以Maybach為名的概念性超豪華四門](../Page/東京車展.md "wikilink")[轎車](../Page/轎車.md "wikilink")，正式讓這個德國汽車品牌在銷聲匿跡多年後再次復活。

今日，其車廠名已改為是戴姆勒汽車集團底下，等級最高的豪華房車專用品牌，但在車輛的機械結構方面，與同集團旗下的[梅賽德斯-賓士S系列有非常高度的共通性](../Page/梅賽德斯-賓士S系列.md "wikilink")。

## 早期歷史

[Maybach_Logo_New\&Old.jpg](https://zh.wikipedia.org/wiki/File:Maybach_Logo_New&Old.jpg "fig:Maybach_Logo_New&Old.jpg")
[Maybach_Zeppelin_Fri.jpg](https://zh.wikipedia.org/wiki/File:Maybach_Zeppelin_Fri.jpg "fig:Maybach_Zeppelin_Fri.jpg")
[Maybach_HL120.jpg](https://zh.wikipedia.org/wiki/File:Maybach_HL120.jpg "fig:Maybach_HL120.jpg")
[Maybach_62_&_Zeppelin_DS_8.jpg](https://zh.wikipedia.org/wiki/File:Maybach_62_&_Zeppelin_DS_8.jpg "fig:Maybach_62_&_Zeppelin_DS_8.jpg")
[Maybach-Limousine.jpg](https://zh.wikipedia.org/wiki/File:Maybach-Limousine.jpg "fig:Maybach-Limousine.jpg")
[Maybach_57S.jpg](https://zh.wikipedia.org/wiki/File:Maybach_57S.jpg "fig:Maybach_57S.jpg")
邁巴赫車廠的先驅威廉·梅巴赫是[梅賽德斯·賓士創廠先驅](../Page/梅賽德斯·賓士.md "wikilink")[戈特利布·戴姆勒長年的摯友與合作夥伴](../Page/戈特利布·戴姆勒.md "wikilink")，他同時也是第一輛汽車的實際製造者。1907年時，他辭去當時在[{{ruby的首席工程師職位](../Page/梅賽德斯·賓士#歷史.md "wikilink")，與他的兒子卡爾·邁巴赫開始合作製造一具他心目中自認最完美的[飛船引擎](../Page/飛船.md "wikilink")。他嘗試與著名的[齊柏林伯爵聯絡](../Page/齊柏林伯爵.md "wikilink")，說服這位人類航空業先驅的頭號人物，讓他了解邁巴赫引擎優秀的品質與卓越的性能。齊柏林接受了他們的遊說，共同在1909年時於[司圖加特近郊的](../Page/司圖加特.md "wikilink")設立了。

1912年時，這家合資設立的機械廠搬遷到更靠近[齊柏林飛船生產廠的](../Page/齊柏林飛船.md "wikilink")[腓特烈港](../Page/腓特烈港.md "wikilink")（弗里德里希港），直到1920年這段期間老威廉很持續地協助當時擔任新公司技術主任的兒子卡爾，開發製造以當時而言性能與穩定性都非常優秀的各式[汽油引擎](../Page/汽油引擎.md "wikilink")、[柴油引擎與](../Page/柴油引擎.md "wikilink")[變速箱](../Page/變速箱.md "wikilink")。

卡爾·邁巴赫在初期嘗試以DMG所提供的汽車底盤，成功地製造出第一輛[原型車Maybach](../Page/原型車.md "wikilink")
W1之後，1921年時他在斐德利希哈芬創立了梅巴赫引擎製造廠，開始打造屬於自己的新車款。在當時，一輛汽車的製作牽涉到兩個部分，汽車廠負責打造的其實是汽車的底盤、引擎與相關的機械硬體，而則負責打造汽車的車身內外造形。Maybach專攻汽車的[車架](../Page/車架.md "wikilink")、[懸吊系統](../Page/懸吊系統.md "wikilink")、[引擎](../Page/引擎.md "wikilink")、[變速箱](../Page/變速箱.md "wikilink")、散熱水箱與[防火牆等主要零件的組裝](../Page/防火牆.md "wikilink")，而車身造型的問題則委託給協力的車體廠，根據客戶的需求與意願進行細部的設計。當時與Maybach密切合作的車體廠裡面，最重要的是近郊的，除此之外包括[德勒斯登的](../Page/德勒斯登.md "wikilink")、司圖加特的、[柏林的](../Page/柏林.md "wikilink")與等都是曾與Maybach聯手創作過的專業車體廠。

正由於邁巴赫引擎突出的性能表現與平順的運轉品質，再加上合作的車體商手工打造出來的精緻車身，該品牌的新車很快地在歐洲的豪華汽車界竄起，其名號足以和當時市場上的既有的豪華車知名品牌如梅赫西迪、[勞斯萊斯](../Page/勞斯萊斯.md "wikilink")、[宾利與](../Page/宾利.md "wikilink")相提並論，可以說是當代品牌的巨擘之一。

但因為[大蕭條與](../Page/大蕭條.md "wikilink")[二戰關係](../Page/二戰.md "wikilink")，邁巴赫汽車只好停產，戰時被逼轉做別的產品，戰後被奔馳收購。

## 邁巴赫的復興

### 第一次復興

早在梅賽德斯-賓士的主要對手、同樣是德國主力汽車集團的[BMW與](../Page/BMW.md "wikilink")[福斯汽車利用收購與瓜分的方式](../Page/福斯汽車.md "wikilink")，分別獲得[英國頂級豪華轎車品牌](../Page/英國.md "wikilink")[勞斯萊斯與](../Page/勞斯萊斯.md "wikilink")[賓利之後](../Page/賓利.md "wikilink")，腳步稍有落後戴姆勒就一直積極在運作，希望能獲得一個定位上比母廠本身還高、足以跟兩個英國品牌相抗衡的品牌。

沒有使用像六七十年代的[舊賓士600](../Page/:EN:Mercedes-Benz_600.md "wikilink")，在第一代S系同期之上的設置一個真正最高車系。考量到超高等級的豪華車消費者通常是非常在意品牌傳承的一群，戴姆勒的高層並沒有考慮直接新創，卻反過來尋找一些曾經一度在歐洲市場頗負盛名的豪華品牌，於是過去曾與他們有過好一段合作關係的邁巴赫自然成為借用商標的對象。

邁巴赫品牌再度登場計畫的序章，是1997年在日本東京車展中出現的概念車，當時戴姆勒集團尚未確立以邁巴赫為新車品牌的作法，只是先把概念車展出來試探市場反應而已。第一輛懸掛邁巴赫「MM」商標的新世代梅巴赫車是在2002年秋天正式上市，為了彰顯這品牌的新車與一般豪華車的身段與等級還是不同，邁巴赫還是採取幾十年前老前輩的作法，依照顧客的需求搭配車上的裝備、採手工組裝的製作方式。復活後的邁巴赫已經不再於原本的斐德利希哈芬港生產，而是轉移到戴姆勒集團位於司圖加特南邊市郊的[辛德芬根](../Page/辛德芬根.md "wikilink")（Sindelfingen）工廠專屬的獨立生產線製造，根據原廠的規劃，該生產線一天只能製造七輛新車，預估的年產量為1500輛。

### 再度停產

2011年年底，戴姆勒集團宣布，鑑於邁巴赫的銷售狀況一直不佳，繼續跟進研發該車型並非明智之舉，將於2013年停產該品牌。[2012年8月邁巴赫停產](../Page/2012年8月.md "wikilink")，早於預期時間停產了邁巴赫。

邁巴赫重興時偏遇上[金融海嘯](../Page/金融海嘯.md "wikilink")，這時的最高級[豪華轎車滯銷](../Page/豪華轎車.md "wikilink")。而邁巴赫的售價是非常地高，直逼[賓利或](../Page/賓利.md "wikilink")[勞斯萊斯](../Page/勞斯萊斯.md "wikilink")，遠比同門以前最高級的[奔驰S系貴了三倍之多](../Page/梅赛德斯-奔驰S级.md "wikilink")，在推出當時已經達三十多萬[-{zh-hans:欧元;
zh-hant:歐元}-](../Page/欧元.md "wikilink")，到停產時在中國售價達五百萬至一千多萬[人民幣](../Page/人民幣.md "wikilink")。\[1\]。虽然當時的迈巴赫也不遜賓利或勞斯萊斯的奢華，但外觀卻像單純的奔驰S系的[加长轿车](../Page/加长轿车.md "wikilink")，所以較難吸引潛在的買家。

另一問題是邁巴赫的市場定位不明確，似有意仿效賓利以原勞斯萊斯屬下較冷門的品牌，因為其運動性能卓越而导致在分家後銷量大增到超過勞斯萊斯的地步。所以邁巴赫不只擁有奢華和精緻的車廂，更由[AMG人工調校過的機械系統](../Page/梅賽德斯-AMG.md "wikilink")，擁有像[跑車般的速度](../Page/跑車.md "wikilink")，所以造價也非常地貴的。可是他却忽略了賓利的最受歡迎產品，那就是真的雙門[轎跑車和](../Page/轎跑車.md "wikilink")[敞篷车](../Page/敞篷车.md "wikilink")，这两种车重量較輕且使用[2+2座位](../Page/2+2_\(汽车车身样式\).md "wikilink")，而邁巴赫實際上都是些傳統的四門五座位的豪華轎車，甚至还有六米長的加长轿车。對於這類車的司機來說駕馭这类车是一种不小的負擔。

### 第二次復興

2014年11月美國洛杉磯車展以及中國廣州車展上，發表了Mercedes-Maybach
S系列，邁巴赫品牌再度重生，更成為梅賽德斯-賓士旗下全新子品牌Mercedes-Maybach。Mercedes-Maybach
S系列並非以舊有邁巴赫品牌再度復活，而是以現行[賓士S系列為基礎](../Page/梅赛德斯-奔驰S级.md "wikilink")，軸距加長，車型修改，內裝升級而來，引擎則與一般版S系列無異，所以價格不致過分誇張，通常不貴過本來賓士S系的一倍。如此將Mercedes-AMG、Mercedes-Maybach兩個子品牌區隔，Mercedes-AMG為旗下性能旗艦，Mercedes-Maybach則為豪華旗艦。開發總監Hermann-Joseph
Storp表示，若Mercedes-Maybach
S系列是成功的，未來Mercedes-Maybach將延伸至S-Coupe、GLS，甚至中階E系列。

但直到2018年新的邁巴赫仍然集中較傳統的豪華轎車上。

## 歷代車系

  - 邁巴赫W1（1919年）
  - 邁巴赫W3（1921年）
  - 邁巴赫W5 / W5 SG（1926年／1928年）
  - 邁巴赫·齊柏林12（1929年）
  - 邁巴赫W6 / W6 DSG（1930年／1934年）
  - 邁巴赫·齊柏林DS 7 / DS 8（1930年／1931年）
  - 邁巴赫W6 DSH（1934年）
  - 邁巴赫SW 35 / SW 38 / SW 42（1935年－1939年）
  - 邁巴赫JW 61（1945年）
  - 邁巴赫57 / 62（2002年）
  - 邁巴赫57S / 62S（S代表「特別版」之意，搭載比標準版輸出更大的動力系統）
  - 邁巴赫Guard （邁巴赫防彈車）
  - [邁巴赫Exelero](../Page/邁巴赫Exelero.md "wikilink")（2005年[概念車](../Page/概念車.md "wikilink")，並無量產）
  - 邁巴赫62 Landaulet（2009年 62S[半敞篷車版](../Page/半敞篷車.md "wikilink")）
  - 邁巴赫 齊柏林（2009年紀念齊柏林特仕版）
  - 邁巴赫 Edition 125\!（2011年紀念汽車誕生125週年念版）
  - Xenatec Maybach 57S Coupé（以57S為基礎，與德國改裝廠Xenatec合作的雙門四座跑車）

## 現行車系

  - Mercedes Maybach S400
  - Mercedes Maybach S500
  - Mercedes Maybach S600
  - Mercedes Maybach S600 Pullman
  - Mercedes Maybach S650 Sedan/Cabriolet
  - Mercedes Maybach S560 4Matic
  - Mercedes Maybach S680
  - Mercedes Maybach G650 Landaulet

## 相關條目

  - [梅賽德斯-賓士](../Page/梅賽德斯-賓士.md "wikilink")
  - [威廉·邁巴赫](../Page/威廉·邁巴赫.md "wikilink")
  - [齊柏林飛船](../Page/齊柏林飛船.md "wikilink")

## 參考資料

  - [Maybach
    Manufaktur官方網站（Tradition章節）](https://web.archive.org/web/20061210003918/http://www.maybach-manufaktur.com/index.htm)
  - DaimlerChrysler集團媒體專用新聞稿（來自[DaimlerChrysler集團媒體網站](https://web.archive.org/web/20061210122758/http://www.media.daimlerchrysler.com/gms_frame)）

## 外部連結

  - [Daimler集團全球首頁](http://www.daimler.com/)
  - [Maybach
    Manufaktur官方網站](https://web.archive.org/web/20090302073953/http://www.maybach-manufaktur.com/)
  - [德國Maybach愛好者俱樂部](https://www.webcitation.org/6EPowCgbV?url=http://www.maybach.de/)

[Category:德國汽車品牌](../Category/德國汽車品牌.md "wikilink")
[Category:戴姆勒](../Category/戴姆勒.md "wikilink")
[Category:德國軍事工業](../Category/德國軍事工業.md "wikilink")

1.  世界名車鑒賞 isbn 9787510446795 p.208