[Deng_Zhicheng.jpg](https://zh.wikipedia.org/wiki/File:Deng_Zhicheng.jpg "fig:Deng_Zhicheng.jpg")
**邓之诚**（），[字](../Page/表字.md "wikilink")**文如**，[号](../Page/号.md "wikilink")**明斋**，又号**五石斋**，[江苏江宁](../Page/江苏.md "wikilink")（[南京](../Page/南京.md "wikilink")）人，[中华民国及](../Page/中华民国.md "wikilink")[中华人民共和国](../Page/中华人民共和国.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")。\[1\]\[2\]

## 生平

幼入[私塾](../Page/私塾.md "wikilink")，隨父亲到[雲南任所](../Page/雲南.md "wikilink")，習六代史。1903年，考入[雲南兩級師範學堂](../Page/雲南兩級師範學堂.md "wikilink")。1907年畢業後，担任[滇報社編輯](../Page/滇報.md "wikilink")。1910年，任[昆明第一中學堂史地教員](../Page/昆明第一中學堂.md "wikilink")。[武昌起義後](../Page/武昌起義.md "wikilink")，仍然兼報社工作，宣傳革命。1917年，應[北京大學聘请任教授](../Page/北京大學.md "wikilink")。到北京後，被國史編纂處任命爲民國史纂輯員。後来又兼任北平《新晨報》社總編輯。1927年起，專任北京大學史学系教授，兼任[北平师范大学](../Page/北京师范大学.md "wikilink")、[北平大学女子文理学院](../Page/北平大学.md "wikilink")、[燕京大学史学教授](../Page/燕京大学.md "wikilink")。1931年起，专任燕京大学历史系教授，讲授中国通史和秦汉、魏晋南北朝、隋唐、明清各断代史。其间，曾经兼任北平师范大学、[辅仁大学史学课程](../Page/辅仁大学.md "wikilink")。1941年遭到日军逮捕。1945年燕京大学复校，回到燕京大学任教，支持学生的爱国运动。1952年[院系调整](../Page/院系调整.md "wikilink")，以北京大学历史系教授的身份全薪退休。曾任[中国科学院哲学社会科学部历史考古专门委员](../Page/中国科学院哲学社会科学部.md "wikilink")。\[3\]

邓之诚培育出许多文史学者，如[黄现璠](../Page/黄现璠.md "wikilink")、[王重民](../Page/王重民.md "wikilink")、[朱士嘉](../Page/朱士嘉.md "wikilink")、[谭其骧](../Page/谭其骧.md "wikilink")、[王钟翰等等](../Page/王钟翰.md "wikilink")。

1960年1月6日逝世，享年73岁。1960年1月10日安葬在北京[东北义园](../Page/东北义园.md "wikilink")。\[4\]

## 著作

《中华二千年史》、《清诗纪事初编》、《骨董琐记全编》等。

## 参考文献

## 延伸阅读

  - 王锺翰、邓同合著，邓文如传略，中国当代社会科学家，1983年第4期。
  - 王锺翰、邓嗣禹、周一良合著，邓之诚传，中国史学家评传·第3册，1985年
  - [邓之诚:"旧学"风范](http://confucianism.com.cn/Show.asp?id=3366)

[Category:中華民國歷史學家](../Category/中華民國歷史學家.md "wikilink")
[Category:中华人民共和国历史学家](../Category/中华人民共和国历史学家.md "wikilink")
[Category:中华民国教育家](../Category/中华民国教育家.md "wikilink")
[Category:中华人民共和国教育家](../Category/中华人民共和国教育家.md "wikilink")
[Category:輔仁大學教授](../Category/輔仁大學教授.md "wikilink")
[Category:善后会议代表](../Category/善后会议代表.md "wikilink")
[Category:南京人](../Category/南京人.md "wikilink")

1.  中国人名大词典，上海辞书出版社，1992年，第242页
2.  中华当代文化名人大辞典，中国广播电视出版社，1992年，第502页
3.  徐友春主编，民国人物大辞典，河北人民出版社，1991年，第1494页
4.  冯其利，东北义园名人补遗，北京档案史料2007年第4期