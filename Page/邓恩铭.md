**邓恩铭**（），又名恩明，[字仲尧](../Page/表字.md "wikilink")，[贵州](../Page/贵州.md "wikilink")[荔波人](../Page/荔波.md "wikilink")，[水族](../Page/水族.md "wikilink")。[中國共產黨創始人之一](../Page/中國共產黨.md "wikilink")。

## 生平

[清](../Page/清.md "wikilink")[光緒二十六年](../Page/光緒.md "wikilink")（1900年）11月15日，邓恩铭出生于一个贫苦农民家庭。6岁上[私塾](../Page/私塾.md "wikilink")，10岁进入到荔泉书院读书。16岁离开[贵州](../Page/贵州.md "wikilink")、到了[山东](../Page/山东.md "wikilink")。1918年依靠在山东的亲戚资助，邓恩铭考入济南省立第一中学。[五四运动爆发后](../Page/五四运动.md "wikilink")，邓恩铭积极响应[北京学生爱国运动](../Page/北京.md "wikilink")，被选为学生自治会领导人兼出版部部长，主编校报，组织学生参加[罢课运动](../Page/罢课.md "wikilink")。1920年11月，他与[王尽美等组织励新学会](../Page/王尽美.md "wikilink")，介绍[俄国](../Page/俄国.md "wikilink")[十月革命](../Page/十月革命.md "wikilink")。

1921年春，邓恩铭参与发起建立[济南的中共早期组织](../Page/济南.md "wikilink")，抨击社会现状；同年7月，鄧恩銘和王盡美作為[濟南代表出席在](../Page/濟南.md "wikilink")[上海召開的](../Page/上海.md "wikilink")[中共一大](../Page/中共一大.md "wikilink")，他是唯一參與中共一大的[少數民族代表](../Page/中國少數民族.md "wikilink")。1922年1月，邓恩铭赴[莫斯科参加远东各国共产党和民族革命团体第一次代表大会](../Page/莫斯科.md "wikilink")，受到[列宁的接见](../Page/列宁.md "wikilink")。同年底，邓恩铭赴[青岛](../Page/青岛.md "wikilink")，创建党组织，先后任中共直属青岛支部书记、[中共青岛市委书记](../Page/中共青岛市委.md "wikilink")。[大革命时期](../Page/国民革命军北伐.md "wikilink")，邓恩铭先后领导[胶济铁路工人大罢工和青岛全市工人大罢工](../Page/胶济铁路.md "wikilink")，组织成立青岛市各界联合会和青岛市总工会。1927年4月，邓恩铭赴[武汉出席](../Page/武汉.md "wikilink")[中共五大](../Page/中共五大.md "wikilink")；回山东后，任[中共山东省执行委员会书记](../Page/中共山东省执行委员会.md "wikilink")。大革命失败后，邓恩铭辗转山东各地，领导中共组织开展斗争。1928年12月在[济南被捕](../Page/济南.md "wikilink")，1931年4月5日被[韩复-{榘}-治下的](../Page/韩复榘.md "wikilink")[山东省临时军法会审委员会判处死刑并枪决](../Page/山东省临时军法会审委员会.md "wikilink")，同时遇难22人。

## 后世纪念

1987年，在[山东省](../Page/山东省.md "wikilink")[济南市](../Page/济南市.md "wikilink")[青年公园内建](../Page/青年公园_\(济南市\).md "wikilink")“五四烈士纪念碑”，以纪念邓恩铭等22名中共党员。

2009年9月14日，邓恩铭被[中央宣传部](../Page/中央宣传部.md "wikilink")、[中央组织部](../Page/中央组织部.md "wikilink")、[中央统战部](../Page/中央统战部.md "wikilink")、[共青团中央](../Page/共青团中央.md "wikilink")、[解放军总政治部等](../Page/解放军总政治部.md "wikilink")11个部门联合组织评为100位为新中国成立作出突出贡献的英雄模范人物之一。

[Category:水族人](../Category/水族人.md "wikilink")
[Category:荔波人](../Category/荔波人.md "wikilink")
[Category:中共一大代表](../Category/中共一大代表.md "wikilink")
[Category:山东政治人物](../Category/山东政治人物.md "wikilink")
[Category:被處決的中華民國人](../Category/被處決的中華民國人.md "wikilink")
[Category:中华民国大陆时期中共烈士](../Category/中华民国大陆时期中共烈士.md "wikilink")
[E恩铭](../Category/邓姓.md "wikilink")