**那卡西**是一種源自[日本的賣唱模式](../Page/日本.md "wikilink")，乃[日文](../Page/日文.md "wikilink")「」的音譯，賣唱者如同[水一般流動](../Page/水.md "wikilink")，在各個[旅館](../Page/酒店.md "wikilink")、[餐廳](../Page/餐廳.md "wikilink")、[夜總會之間或替客人伴奏](../Page/夜總會.md "wikilink")、或接受客人點歌演唱，故而稱之。

## 概要

那卡西的表演型態多半是表演者帶著歌譜、[吉他](../Page/吉他.md "wikilink")（亦有[手風琴](../Page/手風琴.md "wikilink")、原木震盪器〔[Vibraslap](../Page/:en:Vibraslap.md "wikilink")〕或其他[樂器等](../Page/樂器.md "wikilink")），遊走在各餐廳、夜總會等聲色場所替客人伴奏，亦或是接受客人點唱表演。和[卡拉OK最大的不同](../Page/卡拉OK.md "wikilink")，那卡西藝人可視客人的歌聲變換旋律，就算客人是音癡亦能對應。不過那卡西藝人最大的苦惱便是應付爛醉如泥、死纏爛打的客人，對這種客人收錢簡直難上加難。

## 在日本的發展

[Strolling_accordionist_and_guitarists_in_the_1953_Japanese_drama_film_Tokyo_Story.jpg](https://zh.wikipedia.org/wiki/File:Strolling_accordionist_and_guitarists_in_the_1953_Japanese_drama_film_Tokyo_Story.jpg "fig:Strolling_accordionist_and_guitarists_in_the_1953_Japanese_drama_film_Tokyo_Story.jpg")導演的電影《[東京物語](../Page/東京物語.md "wikilink")》(1953年)\]\]
1960年代日本的經濟起飛，[卡拉OK機器尚未發明](../Page/卡拉OK.md "wikilink")，[新宿黃金街](../Page/新宿.md "wikilink")、[歌舞伎町](../Page/歌舞伎町.md "wikilink")、新宿三丁目等一帶開設許多[小吃店](../Page/小吃店.md "wikilink")、[酒吧](../Page/酒吧.md "wikilink")、[居酒屋等](../Page/居酒屋.md "wikilink")，許多酒客在酒酣耳熱之際難免會引吭高歌、炒熱氣氛，那卡西藝人便應運而生。這些那卡西樂團帶著一、兩千曲的歌譜、木吉他等器材，在各店之間幫客人伴奏或接受點唱。以1998年新宿黃金街為例，當時的行情大約伴奏一曲500[日圓](../Page/日圓.md "wikilink")、三曲1千日圓。甚至有些頗有名氣的那卡西團組接受按[小時計酬](../Page/小時.md "wikilink")，至於費用則雙方商量議定。

演唱或伴奏的曲風多變，[軍歌](../Page/軍歌.md "wikilink")、[民謠](../Page/民謠.md "wikilink")、[歌謠曲](../Page/日本歌謠曲.md "wikilink")、[流行音樂等皆有](../Page/日本流行音樂.md "wikilink")，但畢竟客人年齡層偏高，所演唱的歌曲也以懷舊復古為主流。有許多[演歌歌手都是從那卡西出身](../Page/演歌.md "wikilink")，諸如[北島三郎](../Page/北島三郎.md "wikilink")、[五木宏](../Page/五木宏.md "wikilink")、[渥美二郎](../Page/渥美二郎.md "wikilink")、[遠藤實](../Page/遠藤實.md "wikilink")、[諏訪親治等](../Page/諏訪親治.md "wikilink")；其中遠藤實後來成為了[作曲家](../Page/作曲家.md "wikilink")\[1\]。

## 在臺灣的發展

那卡西在[臺灣的起源與](../Page/臺灣.md "wikilink")[溫泉息息相關](../Page/溫泉.md "wikilink")，[臺北](../Page/臺北市.md "wikilink")[北投以](../Page/北投區.md "wikilink")[溫泉出名](../Page/北投溫泉.md "wikilink")，故被視為發源地。[日治時期日本人在當地經營的溫泉旅館收費較高](../Page/台灣日治時期.md "wikilink")，[藝妓的表演以](../Page/藝妓.md "wikilink")[三味線](../Page/三味線.md "wikilink")、[能樂為主](../Page/能樂.md "wikilink")；至於臺灣人經營的收費則較低廉，表演節目也以[南管樂曲](../Page/南管.md "wikilink")、本土歌謠為主。最早的那卡西表演型態以自彈自唱居多，一人演奏[樂器](../Page/樂器.md "wikilink")、一人吟唱，後來才有接受客人點唱的服務，巔峰時期常見兩個樂手搭配一個女歌手的組合。至於樂器方面，最早先是[三味線](../Page/三味線.md "wikilink")，逐漸轉變成[手風琴](../Page/手風琴.md "wikilink")、[吉他](../Page/吉他.md "wikilink")、[薩克斯風等具西洋風味的樂器](../Page/薩克斯風.md "wikilink")，後來演進成[電吉他](../Page/電吉他.md "wikilink")、[電子琴等電子樂器](../Page/電子琴.md "wikilink")。

在1980年代的極盛時期，北投每家溫泉旅館都擁有3至7個那卡西樂團，整個溫泉區則多達上百團。除了當時流行的創作歌謠外，也有從日語歌曲重新填詞的[音樂作品](../Page/音樂.md "wikilink")。那卡西藝人必須熟記上千首歌謠以應付客人點歌，包括[國語](../Page/中華民國國語.md "wikilink")、[臺語](../Page/臺灣話.md "wikilink")、[粵語](../Page/粵語.md "wikilink")、[英語](../Page/英語.md "wikilink")、日語等；客人點歌後則迅速找到該樂譜開始演唱。此外尚有待命支援的那卡西樂團，[電話一叫服務馬上就到](../Page/電話.md "wikilink")，以應付溫泉旅館的急需。促成這些那卡西樂團在各溫泉旅館間移動的最大功臣，當屬限時專送的[機車隊](../Page/機車.md "wikilink")\[2\]。由於北投一帶的道路狹窄彎曲，[汽車無法深入](../Page/汽車.md "wikilink")，故盛行機車接送服務。

1990年代之後隨著實施廢娼制度、掃蕩[八大行業政策](../Page/台灣性產業.md "wikilink")、經濟不景氣的影響，北投溫泉業開始沒落；再加上[卡拉OK](../Page/卡拉OK.md "wikilink")、KTV興起，那卡西這個行業逐漸式微，機車接送服務也跟著每況愈下。

著名的那卡西歌手有下述：

  - [黃乙玲](../Page/黃乙玲.md "wikilink")
  - [金門王](../Page/金門王.md "wikilink")、[李炳輝](../Page/李炳輝.md "wikilink")
  - [江蕙](../Page/江蕙.md "wikilink")
  - [江淑娜](../Page/江淑娜.md "wikilink")
  - [陳明章](../Page/陳明章.md "wikilink")
  - [張蓉蓉](../Page/張蓉蓉.md "wikilink")

## 參考資料

### 腳註

### 文獻

  - [臺北市北投區公所 ─
    那卡西](http://www.btdo.taipei.gov.tw/ct.asp?xItem=18360492&ctNode=51505&mp=124081)
  - [MOOK景點家：沿著北投那卡西足跡，踏上溫泉鄉美好舊時光](http://www.mook.com.tw/article.php?op=articleinfo&articleid=7867)

[Category:演歌](../Category/演歌.md "wikilink")
[Category:音樂相關職業](../Category/音樂相關職業.md "wikilink")
[Category:日本音樂風格](../Category/日本音樂風格.md "wikilink")
[Category:台灣音樂風格](../Category/台灣音樂風格.md "wikilink")

1.  請見[遠藤実プロフィール](http://minoru-endo.com/profile.htm)。
2.  當然，機車隊載送的也包含[特種營業小姐](../Page/性工作者.md "wikilink")（俗稱「貓仔」）。