[Nayantu.jpg](https://zh.wikipedia.org/wiki/File:Nayantu.jpg "fig:Nayantu.jpg")

**那彦图**（），字**矩甫**，[博尔济吉特氏](../Page/博尔济吉特氏.md "wikilink")，[蒙古族](../Page/蒙古族.md "wikilink")，[外蒙古](../Page/外蒙古.md "wikilink")[賽音諾顏部中左末旗札薩克和碩親王](../Page/賽音諾顏部.md "wikilink")。清初功臣[策棱嫡裔](../Page/策棱.md "wikilink")，世代常驻北京。\[1\]

## 生平

1874年那彦图袭中左末旗札萨克亲王。1893年（[光緒十九年](../Page/光緒.md "wikilink")），那彦图任镶白旗蒙古都統兼[領侍衛内大臣](../Page/領侍衛内大臣.md "wikilink")。1898年（光緒24年），那彦图改任正紅旗满洲都統。1903年（光緒29年），那彦图改任镶黄旗满洲都統。1908年（光緒34年），那彦图任[健鋭營都統](../Page/健鋭營.md "wikilink")。1912年（[民国元年](../Page/民国紀元.md "wikilink")），他升任[烏里雅蘇台将軍](../Page/烏里雅蘇台将軍.md "wikilink")（未到任）。\[2\]他還是[資政院外藩王公世爵議員](../Page/資政院.md "wikilink")（代表[齐齐尔里克盟](../Page/三音诺颜部.md "wikilink")）\[3\]\[4\]，曾任御前大臣\[5\]。[武昌起义后](../Page/武昌起义.md "wikilink")，起初那彦图等成立[蒙古王公联合会](../Page/蒙古王公联合会.md "wikilink")，阻挠清帝退位；复通电拥戴[袁世凯](../Page/袁世凯.md "wikilink")。\[6\]

[中華民国成立後](../Page/中華民国.md "wikilink")，他歷任[共和党](../Page/共和党_\(中国\).md "wikilink")、[進步党理事](../Page/進步党_\(中国\).md "wikilink")，並曾任[北京臨時參議院議員](../Page/北京臨時參議院.md "wikilink")。1913年（民国2年），他任[政治会議議員](../Page/政治会議.md "wikilink")。\[7\]\[8\]1914年他任[参政院参政](../Page/参政院.md "wikilink")\[9\]、[约法会议议员](../Page/约法会议.md "wikilink")\[10\]。1914年9月，他被[北洋將軍府授予](../Page/北洋將軍府.md "wikilink")“綏威将軍”。他还任镶黄旗满洲都統\[11\]\[12\]。他获授一等嘉禾章\[13\]。1917年（民国6年），他任[臨時参議院副議長](../Page/中華民國臨時参議院.md "wikilink")。1925年（民国14年），他任[善後会議会員](../Page/善後会議.md "wikilink")。1934年（民国23年）3月，他任[蒙古地方自治政務委員会委員](../Page/蒙古地方自治政務委員会.md "wikilink")。\[14\]

1938年（民国27年）4月，那彦图去世，享年66嵗。\[15\]

## 家庭

他是[奕劻的女婿](../Page/奕劻.md "wikilink")。

## 参考文献

### 引用

### 来源

  -
  -
  - 謝彬『民国政党史』1924年（中華書局版、2007年，ISBN 978-7-101-05531-3）

{{-}}

[Category:外藩蒙古亲王](../Category/外藩蒙古亲王.md "wikilink")
[Category:赛音诺颜部人](../Category/赛音诺颜部人.md "wikilink")
[Category:清朝蒙古人](../Category/清朝蒙古人.md "wikilink")
[Category:中国蒙古族高级官员](../Category/中国蒙古族高级官员.md "wikilink")
[Category:北洋将军府冠字将军](../Category/北洋将军府冠字将军.md "wikilink")
[Category:資政院議員](../Category/資政院議員.md "wikilink")
[Category:北京臨時參議院議員](../Category/北京臨時參議院議員.md "wikilink")
[Category:博尔济吉特氏](../Category/博尔济吉特氏.md "wikilink")
[Category:政治会议议员](../Category/政治会议议员.md "wikilink")
[Category:约法会议议员](../Category/约法会议议员.md "wikilink")
[Category:参政院参政](../Category/参政院参政.md "wikilink")
[Category:第二次中华民国临时参议院议员](../Category/第二次中华民国临时参议院议员.md "wikilink")
[Category:善后会议会员](../Category/善后会议会员.md "wikilink")
[Category:鑲黃旗滿洲都統](../Category/鑲黃旗滿洲都統.md "wikilink")
[Category:正紅旗滿洲都統](../Category/正紅旗滿洲都統.md "wikilink")

1.  《民国人物大辞典 増訂版》

2.
3.
4.  苏钦、吴贤萍，试析清末资政院少数民族议员的产生及其意义，中央社会主义学院学报2008年第6期

5.  《约法会议记录》，第119页

6.
7.  《民国政党史》

8.
9.
10.
11.
12.
13.
14.
15.