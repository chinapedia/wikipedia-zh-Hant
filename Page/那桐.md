**那桐**（），字琴轩，[叶赫那拉氏](../Page/叶赫那拉氏.md "wikilink")，[内务府满洲](../Page/内务府满洲.md "wikilink")[镶黄旗人](../Page/镶黄旗.md "wikilink")。[清末朝廷重臣](../Page/清.md "wikilink")，官至[军机大臣](../Page/军机大臣.md "wikilink")、[内阁大学士及皇族内阁的协理大臣](../Page/内阁大学士.md "wikilink")。他长达36年（1890-1925）的日记于2006年出版，有一定历史价值。

## 生平

生于[咸丰七年七月二十三日](../Page/咸丰.md "wikilink")（公历1857年9月11日）。[光绪十一年](../Page/光绪.md "wikilink")（1885年）乙酉科顺天[乡试](../Page/乡试.md "wikilink")，中试第112名[举人](../Page/举人.md "wikilink")。由[户部](../Page/户部.md "wikilink")[主事历保四品京堂](../Page/主事.md "wikilink")，授[鸿胪寺卿](../Page/鸿胪寺卿.md "wikilink")，迁[内阁学士](../Page/内阁学士.md "wikilink")。二十六年（1900年）兼直[总理各国事务衙门](../Page/总理各国事务衙门.md "wikilink")，晋[理藩院](../Page/理藩院.md "wikilink")[左侍郎](../Page/左侍郎.md "wikilink")。[八国联军攻陷](../Page/八国联军.md "wikilink")[北京后](../Page/北京.md "wikilink")，任留京办事大臣，随[奕劻](../Page/奕劻.md "wikilink")、[李鸿章与联军议和](../Page/李鸿章.md "wikilink")。次年以户部右侍郎，赏给头品顶戴，授为专使大臣，前往[日本](../Page/日本.md "wikilink")。1902年又派充赴日本观博览会大臣。在参观博览会之余，率领随员留心考察日本的警政、路政。

1903年升[户部尚书](../Page/户部尚书.md "wikilink")，调外务部兼步兵统领，管工巡局事，创办警务、开辟新式马路、兴办[东安市场](../Page/东安市场.md "wikilink")。并平反[王维勤冤狱](../Page/王维勤.md "wikilink")，商民颂之。1905年晋大学士，仍充外务省会办大臣。1906年，兼属民政部尚书，晋东阁大学士，督办税务大臣。宣统元年（1909年）初任[军机大臣](../Page/军机大臣.md "wikilink")。1911年为“[皇族内阁](../Page/立宪运动.md "wikilink")”协理大臣。同年爆发辛亥革命，与[奕劻](../Page/奕劻.md "wikilink")、[徐世昌一起保举启用已被罢职的](../Page/徐世昌.md "wikilink")[袁世凯](../Page/袁世凯.md "wikilink")。袁世凯内阁成立时，任[弼德院顾问大臣](../Page/弼德院.md "wikilink")。

[民国后寓居](../Page/民国.md "wikilink")[天津英租界](../Page/天津英租界.md "wikilink")，春夏回北京[金鱼胡同](../Page/金鱼胡同.md "wikilink")[那家花园](../Page/那家花园.md "wikilink")，1925年农历五月初八日（6月28日），病逝于北京那家花园寓所，享年69岁。葬于北京[那桐墓](../Page/那桐墓.md "wikilink")。

## 畫廊

现存于北京[清华大学中的](../Page/清华大学.md "wikilink")“清华园”、“清华学堂”匾额，就是那桐亲笔题写。

[File:TsinghuaUniversityGate.JPG|那桐題寫的](File:TsinghuaUniversityGate.JPG%7C那桐題寫的)[清華大學二校門上](../Page/清華大學二校門.md "wikilink")“清華園”題字
<File:Tsinghua>
School.JPG|那桐題寫的[清華學堂上的](../Page/清華學堂.md "wikilink")“清華學堂”題字

## 子女

  - 有子一人名[绍曾](../Page/绍曾.md "wikilink")，女八人，多嫁入王侯将相之家，包括庆亲王载振、袁世凯等。

## 參考文獻

  -
  - [那桐：庸臣岂知亡国痛](http://news.qq.com/a/20110928/000832.htm)

{{-}}

[N那](../Category/军机大臣.md "wikilink")
[N那](../Category/外務部會辦大臣.md "wikilink")
[Category:總理各國事務衙門大臣](../Category/總理各國事務衙門大臣.md "wikilink")
[Category:弼德院顧問大臣](../Category/弼德院顧問大臣.md "wikilink")
[N](../Category/編纂官制大臣.md "wikilink")
[N那](../Category/叶赫那拉氏.md "wikilink")
[N那](../Category/鑲黃旗满洲佐领下人.md "wikilink")
[Category:清朝戶部尚書](../Category/清朝戶部尚書.md "wikilink")
[Category:光緒十一年乙酉科順天鄉試舉人](../Category/光緒十一年乙酉科順天鄉試舉人.md "wikilink")
[Category:葬于北京](../Category/葬于北京.md "wikilink")