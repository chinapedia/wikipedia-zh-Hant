[
**數字**:**文數字**:](https://zh.wikipedia.org/wiki/File:Postal_codes_by_country.svg "fig: 數字:文數字:")

**郵遞區號**（-{zh-cn:又称**邮区编号**，[台湾称](../Page/台湾.md "wikilink")**邮递区号**;
zh-tw:或**郵區編號**，中國大陸稱**郵政編碼**;
zh-hk:又稱**郵遞區號**，中國大陸稱**郵政編碼**;zh-sg:中國大陸称**邮政编码**，[香港称](../Page/香港.md "wikilink")**邮区编号**}-，）是[国家或](../Page/国家.md "wikilink")[地區为实现邮件分拣](../Page/地區.md "wikilink")[自动化和邮政网络](../Page/自动化.md "wikilink")[数字化](../Page/数字化.md "wikilink")，加快邮件传递速度，而把全国划分的编码方式。郵區編號制度已成为衡量一个国家通信技术和邮政服务水平的标准之一。郵遞區號最早是由[烏克蘭發明](../Page/烏克蘭.md "wikilink")、於1932年12月啟用，但在第三年被放棄使用，后来[德國将其再次採用](../Page/德國.md "wikilink")。

大多數國家或地區的[郵政系統都使用郵區編號](../Page/郵政.md "wikilink")，但有少數例外。[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[牙買加和](../Page/牙買加.md "wikilink")[香港都不使用郵區編號](../Page/香港.md "wikilink")，而[紐西蘭只在投寄大量郵件時才使用](../Page/紐西蘭.md "wikilink")。一些網站必須輸入郵區編號，如果居住地根本沒有使用，可以填寫0000。

許多國家或地區的郵區編號是用數字，但如[英國及](../Page/英國.md "wikilink")[加拿大也有使用](../Page/加拿大.md "wikilink")[羅馬字及](../Page/羅馬字.md "wikilink")[數字混用編碼](../Page/數字.md "wikilink")。使用數字編號的國家或地區也用不同位數，如[美國使用](../Page/美國.md "wikilink")5或9位數字，[墨西哥使用](../Page/墨西哥.md "wikilink")5位數字，[澳大利亞使用](../Page/澳大利亞.md "wikilink")4位數字，[印度使用](../Page/印度.md "wikilink")6位數字。編碼格式最為複雜的為[英國](../Page/英國.md "wikilink")。

## 各國家、地區的郵遞區號

### 阿根廷

阿根廷編號為7個字，稱為CPA，是“”的縮寫。其中第一個字母為省代碼，接下來4位數字表郵區，後接3個字母表建築物區面。例如**B1636FDA**，其中B為省代碼，1636是郵區碼，FDA表建築物區面。其中省代碼如下

  - C：Capital Federal（[聯邦首都區](../Page/布宜諾斯艾利斯.md "wikilink")）
  - B：Buenos Aires（[布宜諾斯艾利斯省](../Page/布宜諾斯艾利斯省.md "wikilink")）
  - K：Catamarca（[卡塔馬卡省](../Page/卡塔馬卡省.md "wikilink")）
  - H：Chaco（[查科省](../Page/查科省.md "wikilink")）
  - U：Chubut（[丘布特省](../Page/丘布特省.md "wikilink")）
  - X：Córdoba（[科爾多瓦省](../Page/科爾多瓦省_\(阿根廷\).md "wikilink")）
  - W：Corrientes（[科連特斯省](../Page/科連特斯省.md "wikilink")）
  - E：Entre Ríos（[恩特雷里奧斯省](../Page/恩特雷里奧斯省.md "wikilink")）
  - P：Formosa（[福爾摩沙省](../Page/福爾摩沙省.md "wikilink")）
  - Y：Jujuy（[胡胡伊省](../Page/胡胡伊省.md "wikilink")）
  - L：La Pampa（[拉潘帕省](../Page/拉潘帕省.md "wikilink")）
  - F：La Rioja（[拉里奧哈省](../Page/拉里奧哈省_\(阿根廷\).md "wikilink")）
  - M：Mendoza（[門多薩省](../Page/門多薩省.md "wikilink")）
  - N：Misiones（[米西奧內斯省](../Page/米西奧內斯省.md "wikilink")）
  - Q：Neuquén（[內烏肯省](../Page/內烏肯省.md "wikilink")）
  - R：Río Negro（[內格羅河省](../Page/內格羅河省_\(阿根廷\).md "wikilink")）
  - A：Salta（[薩爾塔省](../Page/薩爾塔省.md "wikilink")）
  - J：San Juan（[聖胡安省](../Page/聖胡安省_\(阿根廷\).md "wikilink")）
  - D：San Luis（[聖路易省](../Page/聖路易省.md "wikilink")）
  - Z：Santa Cruz（[聖克魯斯省](../Page/聖克魯斯省_\(阿根廷\).md "wikilink")）
  - S：Santa Fe（[聖菲省](../Page/聖菲省.md "wikilink")）
  - G：Santiagodel
    Estero（[聖地亞哥-德爾埃斯特羅省](../Page/聖地亞哥-德爾埃斯特羅省.md "wikilink")）
  - V：Tierra del Fuego（[火地省](../Page/火地省_\(阿根廷\).md "wikilink")）
  - T：Tucumán（[圖庫曼省](../Page/圖庫曼省.md "wikilink")）

### 澳洲

澳洲郵區編號為4位數字，因為澳洲中部為無人（或少人）居住的[沙漠](../Page/沙漠.md "wikilink")，所以有些部分郵區編號跨越2個州。

### 加拿大

加拿大的郵區編號為3加3共6個字的拉丁字母及數字混用編號，書寫時前3字和後3字通常以空白分開，其中[奇數位](../Page/奇數.md "wikilink")（第1字、第3字、第5字）為英文A-Z字母，[偶數位](../Page/偶數.md "wikilink")（第2字、第4字、第6字）為阿拉伯數字0-9。例如“V0T
1H0”。

### 中華人民共和國

中国的郵區編號采用4级6位编码制，首2位表示省、市、自治区，第3位代表邮区，第4位代表县、市，最后2位代表投递邮局。此編碼不適用於均有各自的郵政機關的香港、澳門，且香港和澳門未使用郵政編碼。

#### 香港

目前香港政府決定香港內部繼續不用郵政編碼系統。在實際操作中，從國外寄香港的國際郵件，只需要在信封上的詳細地址最後一行寫上英文「HONG
KONG」即可，無需寫上郵政編碼。對[中國大陸而言](../Page/中國大陸.md "wikilink")，香港的郵政編碼為999077，以免直接使用0000或不用而產生誤會。\[1\]

### 捷克

捷克的郵區編號為5碼數字編號。

### 法国

[法国的郵區編號始于](../Page/法国.md "wikilink")1972年，共5个数字：首2位代表省，后3位分别代表城市、地区和邮政分局。比如北部省的阿斯克新城，邮编是59650，59是北部省的区号，其首府里爾的邮编是59000。

### 印度

印度郵區編號稱作“Pincode”（），為6位數字編號。

### 義大利

義大利的郵區編號稱作“”，為5碼數字編號。

### 日本

自1968年7月1日開始，日本使用3至5位數字作郵區編號（****），如「新東京郵便局」（137）、「東京国際郵便局」（100-31）。
1998年2月2日開始改為7位數字（例如：137-8799），舊有的3位數字編號要加上「-8799」，5位數字則在尾後加上「99」。

### 马来西亚

马来西亚的邮政编码由法国邮政协助研发，共5个数字。其中首2位代表州、城市或几个[县](../Page/马来西亚县份.md "wikilink")，后3位则为投递邮局。比如[大山脚邮编是](../Page/大山脚.md "wikilink")14000，14是[威中县东部和](../Page/威中县.md "wikilink")[威南县的区号](../Page/威南县.md "wikilink")，而使用X9XXX通常为新兴城镇或旅游胜地、如[浮罗交怡](../Page/浮罗交怡.md "wikilink")、[云顶高原](../Page/云顶高原.md "wikilink")、[依斯干达公主城等](../Page/依斯干达公主城.md "wikilink")。政府机构和信箱则有各自的邮编。

### 墨西哥

墨西哥郵區編號為5位數字，[西班牙文稱為](../Page/西班牙文.md "wikilink")“Código Postal”。

### 俄羅斯

### 新加坡

[新加坡的邮政编码为](../Page/新加坡.md "wikilink")6位编码，前两位对应邮区（全国分为28个邮区），每个邮区可能有2个到6个这样的2位数号码，第3位对应一条路或一个小的街区。政府[组屋通常由](../Page/组屋.md "wikilink")[大牌号表示其地址](../Page/大牌号.md "wikilink")，邮政编码的后3位通常即是大牌号。对于没有大牌号的地址，根据邮政编码，通常可以精确定位到一幢建筑。

### 西班牙

西班牙郵區編號為5位數字，[西班牙文稱為](../Page/西班牙文.md "wikilink")“”，縮寫成“”。

### 瑞典

瑞典自1968年5月12日起使用代表地理位置的5個數字來分類郵件。10-19開首的編號屬於[斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink")；其他開首的編號中，較大的數字表示較北的城市。

書寫瑞典地址時，第一行是收件人姓名，第二行是（城市內的）街名和街號或（不在城市內的）房屋名字和號碼，第三行是郵區編號和地區名稱（城市或小村落）。
典型的地位是這樣的：

  -
    Erik Svensson（收件人姓名）
    Solrosstigen 1a（街道和街號）
    123 45 STAD（郵區編號、城市）

| 首碼 | 區域                                                                                                                                                                                                                               | 擁有專屬首兩碼的城市                                                                                                                 |
| -- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| 1  | [斯德哥爾摩省的部份](../Page/斯德哥爾摩省.md "wikilink")                                                                                                                                                                                        | [斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink")（10–11和其他零散的小範圍）                                                                       |
| 2  | [斯科訥省](../Page/斯科訥省.md "wikilink")、[克魯努貝里省的部份](../Page/克魯努貝里省.md "wikilink")、[布萊金厄省的部份](../Page/布萊金厄省.md "wikilink")                                                                                                             | [馬爾默](../Page/馬爾默.md "wikilink")（20–21）、[隆德](../Page/隆德.md "wikilink")（22）、[赫爾辛堡](../Page/赫爾辛堡.md "wikilink")（25）          |
| 3  | [延雪平省的部份](../Page/延雪平省.md "wikilink")、[克魯努貝里省的部份](../Page/克魯努貝里省.md "wikilink")、[卡爾馬省的部份](../Page/卡爾馬省.md "wikilink")、[布萊金厄省的部份](../Page/布萊金厄省.md "wikilink")、[哈蘭省的部份](../Page/哈蘭省.md "wikilink")                                | [哈爾姆斯塔德](../Page/哈爾姆斯塔德.md "wikilink")（30）、[韋克舍](../Page/韋克舍.md "wikilink")（35）、[卡爾馬](../Page/卡爾馬.md "wikilink")（39）       |
| 4  | [西約塔蘭省的部份](../Page/西約塔蘭省.md "wikilink")、[哈蘭省的部份](../Page/哈蘭省.md "wikilink")                                                                                                                                                      | [哥德堡](../Page/哥德堡.md "wikilink")（40–41）                                                                                    |
| 5  | [東約特蘭省的部份](../Page/東約特蘭省.md "wikilink")、[延雪平省的部份](../Page/延雪平省.md "wikilink")、[卡爾馬省的部份](../Page/卡爾馬省.md "wikilink")、[西約塔蘭省的部份](../Page/西約塔蘭省.md "wikilink")                                                                      | [布羅斯](../Page/布羅斯.md "wikilink")（50）、[延雪平](../Page/延雪平.md "wikilink")（55）、[林雪平](../Page/林雪平.md "wikilink")（58）             |
| 6  | [南曼蘭省](../Page/南曼蘭省.md "wikilink")、[哥特蘭省](../Page/哥特蘭省.md "wikilink")、[韋姆蘭省](../Page/韋姆蘭省.md "wikilink")、[東約特蘭省的部份](../Page/東約特蘭省.md "wikilink")、[西約塔蘭省的部份](../Page/西約塔蘭省.md "wikilink")、[厄勒布魯省的部份](../Page/厄勒布魯省.md "wikilink") | [北雪平](../Page/北雪平.md "wikilink")（60）、[埃斯基爾斯蒂納](../Page/埃斯基爾斯蒂納.md "wikilink")（63）、[卡爾斯塔德](../Page/卡爾斯塔德.md "wikilink")（65） |
| 7  | [烏普薩拉省](../Page/烏普薩拉省.md "wikilink")、[西曼蘭省](../Page/西曼蘭省.md "wikilink")、[達拉納省](../Page/達拉納省.md "wikilink")、[斯德哥爾摩省的部份](../Page/斯德哥爾摩省.md "wikilink")、[厄勒布魯省的部份](../Page/厄勒布魯省.md "wikilink")                                     | [厄勒布魯](../Page/厄勒布魯.md "wikilink")（70）、[韋斯特羅斯](../Page/韋斯特羅斯.md "wikilink")（72）、[烏普薩拉](../Page/烏普薩拉.md "wikilink")（75）     |
| 8  | [耶夫勒堡省](../Page/耶夫勒堡省.md "wikilink")、[西諾爾蘭省](../Page/西諾爾蘭省.md "wikilink")、[耶姆特蘭省](../Page/耶姆特蘭省.md "wikilink")                                                                                                                   | [耶夫勒](../Page/耶夫勒.md "wikilink")（80）、[松茲瓦爾](../Page/松茲瓦爾.md "wikilink")（85）                                                |
| 9  | [西博滕省](../Page/西博滕省.md "wikilink")、[北博滕省](../Page/北博滕省.md "wikilink")                                                                                                                                                            | [-{于}-默奧](../Page/于默奧.md "wikilink")（90）、[呂勒奧](../Page/呂勒奧.md "wikilink")（97）                                              |

### 中華民國（台灣）

台灣的郵區編號，採行3+2制度。前3碼為[臺灣](../Page/臺灣.md "wikilink")368個鄉鎮市區專用碼（[新竹市與](../Page/新竹市.md "wikilink")[嘉義市其下雖有分區](../Page/嘉義市.md "wikilink")，但郵區編號不分碼），加上[南海諸島](../Page/南海諸島.md "wikilink")[東沙](../Page/東沙群島.md "wikilink")、[南沙](../Page/南沙群島.md "wikilink")（以上兩區皆隸屬[高雄市](../Page/高雄市.md "wikilink")[旗津區管轄](../Page/旗津區.md "wikilink")）及[釣魚臺列嶼](../Page/釣魚臺列嶼.md "wikilink")（臺灣將該島劃為[宜蘭縣](../Page/宜蘭縣.md "wikilink")[頭城鎮的行政區](../Page/頭城鎮.md "wikilink")，目前由[日本實際管轄](../Page/日本.md "wikilink")），後2碼為大量用戶專用碼或是投遞責任區碼。普通寫前3碼即可，例如[新竹市為](../Page/新竹市.md "wikilink")300，但若寫5碼可加快郵件遞送速度。大部分地區的房屋門牌有註明所在地鄉、鎮、市、區名及其郵區編號，例外如[臺北市全市門牌無標示區名和郵區編號](../Page/臺北市.md "wikilink")。[中華郵政建議在國内直式標準信封背面加印郵區編號一覽表](../Page/中華郵政.md "wikilink")，以便查閱。台灣除[臺北市及其他](../Page/臺北市.md "wikilink")[省轄市地址有時不寫區名外](../Page/省轄市.md "wikilink")（例如[臺北市](../Page/臺北市.md "wikilink")[中正區中山南路簡為](../Page/中正區_\(臺北市\).md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[中山南路](../Page/臺北市主要道路列表.md "wikilink")），郵政單位仍建議各市地址應寫區名及郵區編號，以減少查閱及分揀的麻煩。

臺、澎、金、馬的大郵遞區（3+2碼最左邊的一碼）如下：

1.  [臺北市](../Page/臺北市.md "wikilink")
2.  [基隆市](../Page/基隆市.md "wikilink")、[新北市](../Page/新北市.md "wikilink")、[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[連江縣](../Page/連江縣_\(中華民國\).md "wikilink")（[馬祖](../Page/馬祖.md "wikilink")）、[釣魚臺列嶼](../Page/釣魚臺列嶼.md "wikilink")
3.  [新竹市](../Page/新竹市.md "wikilink")、[新竹縣](../Page/新竹縣.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")、[苗栗縣](../Page/苗栗縣.md "wikilink")
4.  [臺中市](../Page/臺中市.md "wikilink")
5.  [彰化縣](../Page/彰化縣.md "wikilink")、[南投縣](../Page/南投縣.md "wikilink")
6.  [嘉義市](../Page/嘉義市.md "wikilink")、[嘉義縣](../Page/嘉義縣.md "wikilink")、[雲林縣](../Page/雲林縣.md "wikilink")
7.  [臺南市](../Page/臺南市.md "wikilink")
8.  [高雄市](../Page/高雄市.md "wikilink")、[澎湖縣](../Page/澎湖縣.md "wikilink")、[南海諸島](../Page/南海諸島.md "wikilink")、[金門縣](../Page/金門縣.md "wikilink")
9.  [屏東縣](../Page/屏東縣.md "wikilink")、[臺東縣](../Page/臺東縣.md "wikilink")、[花蓮縣](../Page/花蓮縣.md "wikilink")

### 英國

英國郵區編號稱為「Postcode」，為英文字及數字混用編碼。

### 美國

美國郵遞區號（ZIP
Code）是[美國郵政使用的一種郵區編號](../Page/美國郵政.md "wikilink")，一般常以大寫「ZIP」表示。

## 参考文献

## 外部連結

  - [聯合郵政聯盟會員國郵遞地址規格](http://www.upu.int/post_code/en/postal_addressing_systems_member_countries.shtml)
  - [聯合郵政聯盟各會員國郵遞编碼查詢網址](http://www.upu.int/post_code/en/list_of_sites_by_country.html)

### 各地郵政編碼查詢

  - [中華郵政郵遞區號查詢](http://www.post.gov.tw/post/internet/f_searchzone/index.jsp?ID=190102)（臺灣）
      - [中華郵政郵遞區號下載](https://web.archive.org/web/20070126081839/http://www.post.gov.tw/post/internet/down/index.jsp?ID=190108#1808)
  - [中国邮政编码查询](http://www.yp.net.cn/schinese/search/areapostcode.asp)（中国大陸）
  - [加拿大郵局郵遞编碼查詢](http://www.canadapost.ca/cpotools/apps/fpc/personal/findByCity?execution=e1s1)
      - [加拿大郵局郵遞编碼查詢](http://www.canadapost.ca/tools/pcl/bin/quick-f.asp)
      - [加拿大郵局郵遞编碼分區地圖下載](http://www.canadapost.ca/cpc2/addrm/hh/current/indexp/cpALL-e.asp)
  - [澳大利亞郵局郵遞编碼查詢及資料庫下載](https://web.archive.org/web/20071003113236/http://www1.auspost.com.au/postcodes/)
  - [西班牙郵局郵遞编碼（Códigos
    Postales）查詢](http://www.correos.es/ENG/13-MenuRec2/04-MenuRec24/1010_s-CodPostal.asp)
  - [西班牙郵局郵遞编碼（Códigos
    Postales）查詢](http://www.correos.es/contenido/13-MenuRec2/04-MenuRec24/1010_s-CodPostal.asp)
  - [印度郵局郵遞编碼（PINCODE）分區](http://www.indiapost.gov.in/Netscape/Pincode.html)
      - [印度郵局郵遞编碼（PINCODE）分區地圖](http://www.indiapost.gov.in/Netscape/PincodeMap.html)
  - [英國皇家郵局郵遞區號查詢](http://www.royalmail.com/portal/rm/postcodefinder?catId=400145)
  - [墨西哥郵局郵遞编碼（Código
    Postal）查詢及資料庫下載](http://www.indiapost.gov.in/Netscape/PincodeMap.html)
  - [義大利郵局郵遞编碼（CAP）查詢](http://www.poste.it/online/cercacap/)
  - [捷克郵局郵遞编碼查詢](http://www.cpost.cz/jetspeed/portal/media-type/html/language/en/country/null/user/anon/page/default.psml/js_pane/sluzbyonline)
  - [日本郵便郵便番号查詢](http://www.post.japanpost.jp/zipcode/index.html)
  - [阿根廷郵局CPA查詢](http://www.correoargentino.com.ar/consulta_cpa/cons_.php)

{{-}}

[Category:邮政](../Category/邮政.md "wikilink")
[各國郵遞區號](../Category/各國郵遞區號.md "wikilink")
[Category:乌克兰发明](../Category/乌克兰发明.md "wikilink")
[Category:郵件](../Category/郵件.md "wikilink")

1.