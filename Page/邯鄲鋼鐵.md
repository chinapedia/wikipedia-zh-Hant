**邯鄲鋼鐵集團公司**，簡稱**邯鋼集團**，1958年於[中國](../Page/中國.md "wikilink")[河北](../Page/河北.md "wikilink")[邯鄲於成立](../Page/邯鄲.md "wikilink")。公司主要從事[鋼鐵業務](../Page/鋼鐵.md "wikilink")，包括[黑色金屬](../Page/黑色金屬.md "wikilink")[冶煉](../Page/冶煉.md "wikilink")、鋼坯、[鋼材軋製](../Page/鋼材.md "wikilink")、燒結礦冶煉、[焦炭及](../Page/焦炭.md "wikilink")[副產品製造](../Page/副產品.md "wikilink")、[銷售及](../Page/銷售.md "wikilink")[進出口業務](../Page/進出口.md "wikilink")。每年鋼材產量約900萬[噸](../Page/噸.md "wikilink")。\[1\]

[子公司](../Page/子公司.md "wikilink")**邯鄲鋼鐵股份有限公司**
(已併入**河北鋼鐵股份有限公司**，上交所除牌當時為600001.SS)，簡稱**邯郸钢铁**，在1996年成立，以及在1998年於[上海證券交易所上市](../Page/上海證券交易所.md "wikilink")。\[2\]

2008年，邯鋼與[河北](../Page/河北.md "wikilink")[唐山的](../Page/唐山市.md "wikilink")[唐鋼合併成](../Page/唐鋼.md "wikilink")「[河北鋼鐵集團](../Page/河北鋼鐵集團.md "wikilink")」，成為中國最大、世界第五大鋼鐵公司。\[3\]

## 連結

  - [邯鄲鋼鐵集團公司](http://www.hgjt.com.cn/)

## 參考

[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")
[Category:中国大陆钢铁公司](../Category/中国大陆钢铁公司.md "wikilink")
[Category:河北钢铁](../Category/河北钢铁.md "wikilink")
[Category:邯鄲公司](../Category/邯鄲公司.md "wikilink")
[Category:上海證券交易所已除牌公司](../Category/上海證券交易所已除牌公司.md "wikilink")
[Category:1958年成立的公司](../Category/1958年成立的公司.md "wikilink")
[Category:1958年中國建立](../Category/1958年中國建立.md "wikilink")

1.  [邯鋼簡介](http://www.hgjt.com.cn/web/about_us/template/index.jsp)
2.  [邯郸钢铁](http://money.finance.sina.com.cn/corp/go.php/vCI_CorpInfo/stockid/600001.phtml)
3.  [中国最大钢铁公司河北钢铁集团诞生记](http://stock.jrj.com.cn/2008/06/201658915690.shtml)