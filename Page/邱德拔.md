**[丹斯里邱德拔](../Page/马来西亚封衔#丹斯里.md "wikilink")**（**Tan Sri Khoo Teck
Puat**，），籍貫[中國](../Page/中國.md "wikilink")[福建海沧](../Page/福建.md "wikilink")，[馬來西亞出生](../Page/馬來西亞.md "wikilink")，生前是[新加坡的首富](../Page/新加坡.md "wikilink")。

邱德拔於2004年因[心臟病發逝世](../Page/心臟病.md "wikilink")，其曾為[英商](../Page/英商.md "wikilink")[渣打銀行最大的個人股東](../Page/渣打銀行.md "wikilink")，亦是[馬來亞銀行的創辦人](../Page/馬來亞銀行.md "wikilink")。在1986年當英國的[莱斯银行欲敵意收購渣打銀行時](../Page/莱斯银行.md "wikilink")，邱德拔和另外兩個金融投資家—香港的[包玉剛和](../Page/包玉剛.md "wikilink")[澳大利亞商人羅伯特](../Page/澳大利亞.md "wikilink")·侯姆（Robert
Holmes）共同聯手，在最後幾天以13億英鎊的出價擊敗勞埃德銀行，獲得渣打銀行37%的股份。邱因而成為最大股東，而且因為守護有功，更獲[英國政府授予](../Page/英國政府.md "wikilink")[爵士頭銜](../Page/爵士.md "wikilink")。不過在收購大批股份之後，其在[汶萊金融業的投資爆出丑聞指出其詐欺](../Page/汶萊.md "wikilink")，邱的長子更因此被汶萊[蘇丹軟禁](../Page/苏丹_\(称谓\).md "wikilink")。

邱在2003年被《[福布斯](../Page/福布斯.md "wikilink")》雜誌選為新加坡首富，總資產達26億美元。邱德拔在新加坡的三家上市公司，分別是良木園集團、中央產業有限公司和馬來西亞酒店有限公司。其中良木園集團即是新加坡知名的[良木園酒店的母公司](../Page/良木園酒店.md "wikilink")。而邱手上那龐大的渣打股票已在2006年初被[淡馬錫控股收購部分](../Page/淡馬錫控股.md "wikilink")，淡馬錫控股因而擁有渣打百分之11.5的股份。邱有兩個妻子，十四個孩子，三男十一女，其中么子[邱金海是新加坡知名導演](../Page/邱金海.md "wikilink")。

2006年11月，邱德拔的家族基金对[北京大学体育馆的建造捐赠](../Page/北京大学体育馆.md "wikilink")1.533亿元人民币，该馆遂命名为“北京大学邱德拔体育馆”。

## 参考文献

[Category:馬來西亞企業家](../Category/馬來西亞企業家.md "wikilink")
[Category:新加坡企業家](../Category/新加坡企業家.md "wikilink")
[Category:新加坡億萬富豪](../Category/新加坡億萬富豪.md "wikilink")
[Category:閩南裔新加坡人](../Category/閩南裔新加坡人.md "wikilink")
[Category:閩南裔馬來西亞人](../Category/閩南裔馬來西亞人.md "wikilink")
[Category:邱德拔家族](../Category/邱德拔家族.md "wikilink")
[D](../Category/邱姓.md "wikilink")
[Category:厦门人](../Category/厦门人.md "wikilink")