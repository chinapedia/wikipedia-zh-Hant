**邱義仁**（），生於[臺灣](../Page/臺灣.md "wikilink")[臺南](../Page/臺南.md "wikilink")，[政治人物](../Page/政治人物.md "wikilink")，[民主進步黨創黨人物之一](../Page/民主進步黨.md "wikilink")，曾任[民主進步黨秘書長](../Page/民主進步黨秘書長.md "wikilink")。與[吳乃仁](../Page/吳乃仁.md "wikilink")、[林濁水](../Page/林濁水.md "wikilink")、[洪奇昌等齊名](../Page/洪奇昌.md "wikilink")，合稱[新潮流派四大天王](../Page/新潮流派.md "wikilink")，為人精於謀略策劃，是民主進步黨過去[社會運動和](../Page/社會運動.md "wikilink")[選舉的](../Page/選舉.md "wikilink")[軍師型人物](../Page/軍師.md "wikilink")，在[陳水扁政府時期出任行政院](../Page/陳水扁政府.md "wikilink")、總統府和[國安會秘書長](../Page/國家安全會議_\(中華民國\).md "wikilink")、[行政院副院長等職](../Page/行政院副院長.md "wikilink")，也因為擔任過府院黨四大秘書長而有「永遠的秘書長」之稱。在行政院副院長任內捲入[巴紐外交公款侵吞案而辭去職務](../Page/巴紐外交公款侵吞案.md "wikilink")。現任[台灣日本關係協會會長](../Page/台灣日本關係協會.md "wikilink")、[新境界文教基金會執行長](../Page/新境界文教基金會.md "wikilink")。

## 生平

邱義仁在學生時代，就與[吳乃仁結為好友](../Page/吳乃仁.md "wikilink")。就讀[台南一中時](../Page/台南一中.md "wikilink")，因閱讀《[文星雜誌](../Page/文星雜誌.md "wikilink")》等[黨外運動刊物](../Page/黨外運動.md "wikilink")，對於政治開始感到興趣。就讀[國立台灣大學期間](../Page/國立台灣大學.md "wikilink")，愛好美國[搖滾樂](../Page/搖滾樂.md "wikilink")，喜愛閱讀[存在主義等哲學書籍](../Page/存在主義.md "wikilink")。1972年，於台大哲學系畢業時，因為爆發[台大哲學系事件](../Page/台大哲學系事件.md "wikilink")，哲學所停招，邱義仁改為考取台大政治學研究所。

1973年，台灣首次開放增額立委補選，[陳菊至](../Page/陳菊.md "wikilink")[國立台灣大學招募數名學生](../Page/國立台灣大學.md "wikilink")，至[宜蘭幫黨外候選人](../Page/宜蘭.md "wikilink")[郭雨新助選](../Page/郭雨新.md "wikilink")，邱義仁從此投入[黨外運動](../Page/黨外運動.md "wikilink")。在幫郭雨新助選時，邱義仁曾與[吳乃仁到](../Page/吳乃仁.md "wikilink")[瑞芳監票](../Page/瑞芳區.md "wikilink")，卻發生郭雨新[廢票達三萬張](../Page/廢票.md "wikilink")、而[中國國民黨的](../Page/中國國民黨.md "wikilink")[林榮三得票卻都算有效票的事件](../Page/林榮三.md "wikilink")。當邱義仁為此向投票所主任質疑時，卻遭到駐場的[警察當場賞了一巴掌](../Page/警察.md "wikilink")，造成邱對政治看法上的衝擊。\[1\]1977年，經[張俊宏邀請](../Page/張俊宏.md "wikilink")，邱義仁休學，擔任[臺灣省議員](../Page/臺灣省議員.md "wikilink")[張俊宏](../Page/張俊宏.md "wikilink")、[林義雄的聯合助理](../Page/林義雄.md "wikilink")。

1978年，邱義仁至美國[芝加哥大學攻讀政治學碩士](../Page/芝加哥大學.md "wikilink")。1979年，[美麗島事件爆發](../Page/美麗島事件.md "wikilink")。邱義仁在此時潛心研讀[中國共產黨歷史](../Page/中國共產黨歷史.md "wikilink")，開始主張，台灣需要有組織的[反對黨](../Page/反對黨.md "wikilink")，才能夠推動民主化運動。1981年，邱義仁取得碩士學位後，原擬繼續進修博士，但因為經濟因素，決定中止博士班學業，回到台灣。他加入[許榮淑創辦的](../Page/許榮淑.md "wikilink")《深耕雜誌》，並推動成立[黨外編輯作家聯誼會](../Page/黨外編輯作家聯誼會.md "wikilink")。1983年，邱義仁發動「批康運動」，對[康寧祥的議會運動路線提出批判](../Page/康寧祥.md "wikilink")，主張與國民黨劃清界線。

1984年，邱義仁與[吳乃仁等人一同創辦](../Page/吳乃仁.md "wikilink")《新潮流雜誌》，積極吸收成員，主張以組織化行動，成立反對黨，以加強民主化的力量。1986年，[民主進步黨成立](../Page/民主進步黨.md "wikilink")，邱義仁為創黨黨員之一。

1990年代，邱義仁曾長期擔任[民主進步黨祕書長](../Page/民主進步黨祕書長.md "wikilink")，與吳乃仁等人共同將民進黨由傳統的街頭抗議路線轉型為議會競選路線。在邱義仁籌劃下，民進黨在議會中的席次逐漸增加，由地方勢力進而成為立法院的主要反對黨；其所屬的新潮流系，也成為民進黨的重要派系。

1989年，邱義仁反對民進黨[美麗島系大老](../Page/美麗島系.md "wikilink")[張俊宏訪問](../Page/張俊宏.md "wikilink")[中國大陸](../Page/中國大陸.md "wikilink")，指責張俊宏訪問中國大陸是「出賣台灣」\[2\]；但1998年7月，時任民進黨秘書長的邱義仁應[廈門大學台灣研究所](../Page/廈門大學.md "wikilink")（[廈門大學台灣研究院前身](../Page/廈門大學台灣研究院.md "wikilink")）邀請參加研討會，返回臺灣後公開表示，「民進黨是選舉黨，選票就是答案，再現實不過」，所以在[臺灣海峽兩岸關係問題上](../Page/臺灣海峽兩岸關係.md "wikilink")「民進黨強扮黑臉是誤國誤民」\[3\]。1998年8月1日，在邱義仁到民進黨中央黨部參加[新舊主席交接典禮時](../Page/民主進步黨主席.md "wikilink")，[建國廣場開宣傳車到現場向邱義仁與主張](../Page/建國廣場.md "wikilink")[大膽西進的前主席](../Page/大膽西進.md "wikilink")[許信良表示抗議](../Page/許信良.md "wikilink")，抗議布條上寫著「賀邱義仁去中國沒死」九字\[4\]。

1999年1月3日，[民間全民電視公司舉辦](../Page/民間全民電視公司.md "wikilink")「跨世紀國家走向系列辯論會」，[沈富雄與](../Page/沈富雄.md "wikilink")[郭正亮說修正](../Page/郭正亮.md "wikilink")[台獨黨綱是民進黨誠實面對自己的作法](../Page/台獨黨綱.md "wikilink")，[林濁水與邱義仁反對修改台獨黨綱](../Page/林濁水.md "wikilink")。

[2000年總統大選中](../Page/2000年中華民國總統選舉.md "wikilink")，邱義仁成功使陳水扁當選總統，民進黨首度執政。[2004年總統大選中](../Page/2004年中華民國總統選舉.md "wikilink")，再度規劃選戰，使陳水扁順利連任。在[陳水扁政府期間](../Page/陳水扁政府.md "wikilink")，曾出任[行政院秘書長](../Page/行政院秘書長.md "wikilink")、[國家安全會議秘書長等職務](../Page/國家安全會議_\(中華民國\).md "wikilink")。2007年2月6日被任命為[總統府秘書長](../Page/總統府秘書長.md "wikilink")，2007年5月14日被任命為[行政院副院長](../Page/行政院副院長.md "wikilink")。

2000年，陳水扁與邱義仁下令[行政院大陸委員會主任委員](../Page/行政院大陸委員會.md "wikilink")[蔡英文](../Page/蔡英文.md "wikilink")，全力阻止民進黨天王兼[高雄市市長](../Page/高雄市市長.md "wikilink")[謝長廷進行](../Page/謝長廷.md "wikilink")[台海兩岸城市交流](../Page/台海兩岸.md "wikilink")\[5\]。

2004年4月13日，民進黨「立委輔選策略小組」第一次會議，邱義仁說，[2004年立法委員選舉](../Page/2004年中華民國立法委員選舉.md "wikilink")，民進黨要延續2004年總統大選「割喉」策略，「割到斷為止」\[6\]；此言引發[國親聯盟反彈](../Page/國親聯盟.md "wikilink")，選舉結果民進黨慘敗，邱義仁自此以「割喉邱」著稱\[7\]。

2008年間，邱義仁被指控涉入[巴紐外交公款侵吞案](../Page/巴紐外交公款侵吞案.md "wikilink")，於5月5日辭去行政院副院長一職，聲明退出民主進步黨與政壇\[8\]\[9\]。5月6日請辭獲-{zh-cn:准;
zh-tw:准;}-，[外交部長](../Page/中華民國外交部.md "wikilink")[黃志芳](../Page/黃志芳.md "wikilink")、[國防部副部長柯承亨也同步跟進請辭獲](../Page/中華民國國防部.md "wikilink")-{zh-cn:准;
zh-tw:准;}-\[10\]。在回到民間後，又因安亞專案、鐽震案等指控，遭受長期的司法調查。他至[高雄市](../Page/高雄市.md "wikilink")[內門區務農](../Page/內門區.md "wikilink")，檯面上遠離政治圈。

2012年，所涉案件法院陸續判決無罪後，邱義仁至日本[京都大學及](../Page/京都大學.md "wikilink")[北海道大學擔任](../Page/北海道大學.md "wikilink")[訪問學者](../Page/訪問學者.md "wikilink")，時間約一年半。在[蔡英文接任民進黨主席後](../Page/蔡英文.md "wikilink")，邱義仁以顧問身份提供意見\[11\]\[12\]。

2014年8月27日，《[新台灣新聞週刊](../Page/新台灣新聞週刊.md "wikilink")》發行人[詹錫奎](../Page/詹錫奎.md "wikilink")（老包）說，2005年2月陳水扁與邱義仁搞出陳水扁與[親民黨主席](../Page/親民黨.md "wikilink")[宋楚瑜的](../Page/宋楚瑜.md "wikilink")「扁宋會」，「堂堂[國家元首竟與自己國家的小黨主席簽](../Page/國家元首.md "wikilink")『條約』，煞有介事，好像在與外國元首簽[備忘錄](../Page/備忘錄.md "wikilink")；事後我去向扁質疑他踐踏國家名位，他只好坦承是邱『同意』他做的」。詹錫奎還說，陳水扁與邱義仁被[權力沖昏頭時](../Page/權力.md "wikilink")，不知天高地厚，與時任[美國在台協會台北辦事處處長的](../Page/美國在台協會.md "wikilink")[包道格交惡為敵](../Page/包道格.md "wikilink")，種下日後陳水扁被天羅地網的[艾格蒙聯盟包圍及揭發](../Page/艾格蒙聯盟.md "wikilink")[鉅款匯海外](../Page/陳水扁家庭密帳案.md "wikilink")[業障](../Page/業障.md "wikilink")，陳水扁身敗名裂，而且「每逢總統大選，包道格必運用影響力打擊綠營」；初始他曾試圖提醒陳水扁，替邱義仁修補與包道格的關係；陳水扁卻只聽信邱義仁之語，對包道格表示不屑，說包道格是[親中的](../Page/親中.md "wikilink")「[紅軍](../Page/紅軍.md "wikilink")」\[13\]。

[10.03_總統主持「執政決策協調會議_」_02.png](https://zh.wikipedia.org/wiki/File:10.03_總統主持「執政決策協調會議_」_02.png "fig:10.03_總統主持「執政決策協調會議_」_02.png")
[2016年大選](../Page/2016年中華民國總統選舉.md "wikilink")，蔡英文成為總統，民進黨也首度成為[立法院第一大黨](../Page/立法院.md "wikilink")。2016年5月27日，邱義仁出任[亞東關係協會會長](../Page/亞東關係協會.md "wikilink")\[14\]。2016年10月3日，蔡英文召開首次「執政決策協調會議」，成員納入邱義仁；同月4日，前總統府副秘書長[羅智強批評](../Page/羅智強.md "wikilink")，邱義仁是陳水扁政府時代倡議「割喉割到斷」的人，蔡英文重用邱義仁，可能代表未來政黨衝突將展開割喉大戰\[15\]。2018年3月29日，前[考試委員](../Page/考試委員.md "wikilink")[張正修披露](../Page/張正修.md "wikilink")，邱義仁被認為已經離開新潮流系，但蔡英文要與新潮流系溝通「主要是透過邱義仁」\[16\]。2018年5月24日，前民進黨中央黨部公共政策研究中心幹事[蔡百銓披露](../Page/蔡百銓.md "wikilink")，[台灣獨立運動人士](../Page/台灣獨立運動.md "wikilink")[林樹枝於](../Page/林樹枝.md "wikilink")2017年初對他說，蔡英文對邱義仁言聽計從，邱義仁才是真正實力人物\[17\]。2018年11月9日，前民進黨[桃園縣議員](../Page/桃園縣議員.md "wikilink")[梁新武表示](../Page/梁新武.md "wikilink")，[2018年九合一選舉](../Page/2018年九合一選舉.md "wikilink")，民進黨真正「出鬼點子」的人還是邱義仁\[18\]。

## 爭議事件

### 施壓撤專欄

2011年6月16日，前《[自由時報](../Page/自由時報.md "wikilink")》專欄作家、《[新台灣新聞週刊](../Page/新台灣新聞週刊.md "wikilink")》發行人[詹錫奎](../Page/詹錫奎.md "wikilink")（老包）說，他在1987年進入《自由時報》寫【老包專欄】；但1992年初，《自由時報》毫無預警、也未說明理由，即撤下【老包專欄】並強制接管他的版面，使他離職；1995年，他才被他的朋友告知，【老包專欄】被撤當天，時任民進黨副秘書長的邱義仁帶著民進黨秘書長[陳師孟與民進黨主席](../Page/陳師孟.md "wikilink")[許信良去和](../Page/許信良.md "wikilink")《自由時報》社長[密室協商](../Page/密室會議.md "wikilink")，當天晚上《自由時報》社長即撤下【老包專欄】並強制接管他的版面；雖然《自由時報》社長一直對他說會幫他升官加薪、只要他不寫專欄就好，但他依然離職；【老包專欄】被撤兩星期後，由於讀者群情激憤、不斷抗議，《自由時報》打電話請他回去寫專欄，但他拒絕\[19\]。

### 319槍擊案

[319槍擊案發生時](../Page/319槍擊案.md "wikilink")，各界消息不明，當時邱義仁曾出來召開記者會。在記者會上，記者問邱義仁「總統既然中彈，他是步行進醫院的嗎」，邱義仁露出笑意並回答：「可能嗎？」面對當時記者提問「總統身上有沒有子彈」時，邱義仁表示「有」。事後證實是彈頭沒有在[陳水扁的身體內取出](../Page/陳水扁.md "wikilink")，而是在陳水扁身上衣物內找到。另外，邱義仁於當晚宣佈啟動「國安機制」，在法律上沒有明確授權。

[泛藍陣營以這幾點](../Page/泛藍.md "wikilink")，強烈質疑邱義仁透過國家[公權力干預選舉](../Page/公權力.md "wikilink")，並做出很多[陰謀論的揣測](../Page/陰謀論.md "wikilink")\[20\]。

連戰陣營提出的選舉無效訴訟，後被法院判決敗訴。邱義仁也沒有遭受法律上的追訴。

### 巴紐外交款項案

[新加坡法院調查結果](../Page/新加坡.md "wikilink")，[黃志芳與邱義仁等人在](../Page/黃志芳.md "wikilink")[巴紐外交公款侵吞案中均未涉及收賄或有經費流入其帳戶](../Page/巴紐外交公款侵吞案.md "wikilink")\[21\]。

### [安亞專案外交款項案](../Page/安亞專案.md "wikilink")

2008年10月31日，邱義仁因涉嫌貪污治罪條例利用職務詐取財物罪，遭到[羈押禁見](../Page/羈押.md "wikilink")。[中華民國最高檢察署](../Page/中華民國最高法院檢察署.md "wikilink")[特偵組調查發現](../Page/特偵組.md "wikilink")，邱義仁疑似在2004年擔任[國安會秘書長期間](../Page/國安會.md "wikilink")，執行機密外交工作「[安亞專案](../Page/安亞專案.md "wikilink")」，在執行完畢後仍向[外交部表示為了繼續該](../Page/外交部.md "wikilink")[專案](../Page/專案.md "wikilink")，請求核撥五十萬元[美元](../Page/美元.md "wikilink")，外交部因此撥付五十萬元[旅行支票](../Page/旅行支票.md "wikilink")。事後有部分支票在海外賭場兌現\[22\]。

監察院調查後，彈劾國家安全會議前秘書長邱義仁逾越權限指示外交部部長黃志芳辦理中國與巴布亞紐幾內亞共和國建交事宜，嚴重斲傷政府形象，違失情節重大，於2009年8月17日發佈：經公務員懲戒委員會議決：「邱義仁、黃志芳均撤職，並各停止任用貳年。」\[23\]

但包括前[民進黨](../Page/民進黨.md "wikilink")[立委](../Page/立委.md "wikilink")[林濁水在內的許多](../Page/林濁水.md "wikilink")[泛綠人士都認為此案很可能非](../Page/泛綠.md "wikilink")[貪污](../Page/貪污.md "wikilink")，檢調單位調查有問題，並要求[法官公正判決](../Page/法官.md "wikilink")。

2011年8月30日，[台北地方法院宣判](../Page/台北地方法院.md "wikilink")「[安亞專案](../Page/安亞專案.md "wikilink")」外交機密款一案，前[國安會秘書長邱義仁](../Page/國安會.md "wikilink")、前[外交部次長高英茂](../Page/外交部.md "wikilink")**兩人均無罪**。法官於判決書指出：

1.  [旅行支票上所兌現的日期](../Page/旅行支票.md "wikilink")，邱義仁未出國。
2.  前[國安會副秘書長柯承亨確實於支票兌現日期出境](../Page/國安會.md "wikilink")，且對方經手者、中間聯繫人均有在收據上簽名，認定支票確實已交由收受人，未被侵占詐領。
3.  邱義仁並非[外交部人員](../Page/外交部.md "wikilink")、高英茂也非外交部的業務單位，查無證據能證明被告故意違反「外交部機密經費審核及未送審憑證保管作業注意事項」，難認定被告有施以詐術領取款項的犯行。\[24\]。

2012年6月，[臺灣高等法院再審理認定](../Page/臺灣高等法院.md "wikilink")，台灣加入[世界貿易組織後](../Page/世界貿易組織.md "wikilink")，希望[世界貿易組織秘書長蘇帕清協助](../Page/世界貿易組織.md "wikilink")[中華民國排除中國藉機矮化台灣](../Page/中華民國.md "wikilink")，該外交款項確實運用在[中華民國為確保參與](../Page/中華民國.md "wikilink")[國際組織時名稱不遭矮化](../Page/國際組織.md "wikilink")，屬公款公用，無貪污事證，仍判兩人無罪。邱義仁辯護律師高涌誠痛批，高院再判無罪，凸顯特偵組的起訴草率，將在本案無罪定讞後，針對邱義仁遭羈押部分，提出刑事補償聲請。\[25\]

### 鐽震案報告文件洩密案

在偵辦[安亞專案機密款案件時](../Page/安亞專案.md "wikilink")，[特偵組搜查邱義仁住處](../Page/特偵組.md "wikilink")，發現[鐽震案報告文件](../Page/鐽震案.md "wikilink")。當時邱義仁回簽，是在2008年卸任後，由[國防部長](../Page/國防部長.md "wikilink")[蔡明憲在住處交付](../Page/蔡明憲.md "wikilink")。2014年8月，高檢署認定[蔡明憲把](../Page/蔡明憲.md "wikilink")[國防部的軍事機密文件交給已卸任的邱義仁](../Page/國防部.md "wikilink")，依違反《國家機密保護法》起訴[蔡明憲與邱義仁](../Page/蔡明憲.md "wikilink")。[蔡明憲在法庭上表示](../Page/蔡明憲.md "wikilink")，在邱義仁卸任前，[鐽震案報告就已經提出](../Page/鐽震案.md "wikilink")，依規定提交給[行政院正](../Page/行政院.md "wikilink")、副院長以及[立法院長](../Page/立法院長.md "wikilink")，邱義仁當時仍擔任[行政院副院長](../Page/行政院副院長.md "wikilink")，非在卸任後才交付。2015年8月，[臺灣高等法院法官認為邱義仁當時記錯了](../Page/臺灣高等法院.md "wikilink")，判決兩人無罪。[高檢署上訴後](../Page/高檢署.md "wikilink")，2015年11月，[最高法院駁回上訴](../Page/最高法院.md "wikilink")，無罪定讞。

## 参考文献

<div style="font-size:89%;">

<references />

</div>

## 外部連結

|- |colspan="3" style="text-align:center;"| |-     |- |colspan="3"
style="text-align:center;"|**[ROC_Office_of_the_President_Emblem.svg](https://zh.wikipedia.org/wiki/File:ROC_Office_of_the_President_Emblem.svg "fig:ROC_Office_of_the_President_Emblem.svg")[總統府](../Page/總統府.md "wikilink")**
|-             |- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")[行政院](../Page/行政院.md "wikilink")**
|-

[Category:臺灣獨立運動者](../Category/臺灣獨立運動者.md "wikilink")
[Category:獲頒授景星勳章者](../Category/獲頒授景星勳章者.md "wikilink")
[Category:國立臺南第一高級中學校友](../Category/國立臺南第一高級中學校友.md "wikilink")
[Category:國立臺灣大學文學院校友](../Category/國立臺灣大學文學院校友.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[Category:中華民國總統府秘書長](../Category/中華民國總統府秘書長.md "wikilink")
[Category:前民主進步黨黨員](../Category/前民主進步黨黨員.md "wikilink")
[I](../Category/邱姓.md "wikilink")
[Category:台南市人](../Category/台南市人.md "wikilink")
[邱](../Category/閩南裔臺灣人.md "wikilink")
[Category:台灣白色恐怖時期異議人士](../Category/台灣白色恐怖時期異議人士.md "wikilink")

1.  《台灣政壇明日之星》, 吳如萍等26位合著, 台北：月旦出版社，1993年，頁101到102。

2.

3.

4.
5.

6.

7.

8.

9.

10. [邱義仁、黃志芳請辭獲准](http://www.epochtimes.com/b5/8/5/6/n2107512.htm)

11.

12.

13.

14.

15.
16.

17.

18.

19.

20.

21. [台灣巴紐建交經費案新加坡勝訴-BBC
    中文網](http://www.bbc.co.uk/zhongwen/trad/world/2010/05/100510_taiwan_courtruling.shtml)

22. [中廣新聞網：「詐取五十萬美金支票
    邱義仁收押禁見」2008.10.31](http://tw.news.yahoo.com/article/url/d/a/081031/1/18m2m.html)

23. [監察院公報【第2669期】](https://www.google.com/url?q=http://www.cy.gov.tw/AP_HOME/Op_Upload/eDoc/%25E5%2585%25AC%25E5%25A0%25B1/98/0980000402669.doc&sa=U&ved=0ahUKEwiOnPLMzeLRAhXFOCYKHbjSAMgQFggPMAU&client=internal-uds-cse&usg=AFQjCNEC9wofpXRkAedg5jKokfvYRWMeiQ)

24. [中央社：「北院：機密款公用
    邱義仁無罪」2011/08/30](http://www2.cna.com.tw/ShowNews/Detail.aspx?pNewsID=201108300098&pType0=aALL&pTypeSel=0)


25. [被控詐領外交費
    邱義仁二審仍無罪](http://www.libertytimes.com.tw/2012/new/jun/21/today-p8.htm?Slots=All),
    自由時報, 2012-6-21