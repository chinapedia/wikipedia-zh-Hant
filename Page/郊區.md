{{
Otheruses|subject=[城市外圍人口密集的區域](../Page/城市.md "wikilink")|other=「郊區」一詞的其他含意|郊區
(消歧義) }}
[Cincinnati-suburbs-tract-housing.jpg](https://zh.wikipedia.org/wiki/File:Cincinnati-suburbs-tract-housing.jpg "fig:Cincinnati-suburbs-tract-housing.jpg")[俄亥俄州的](../Page/俄亥俄州.md "wikilink")[辛辛那堤市附近的住宅區](../Page/辛辛那堤.md "wikilink")，是美國典型的郊區形態。\]\]
**郊區**（亦稱**鄰近都市區域**、**市郊**）是指[城市外圍](../Page/城市.md "wikilink")[人口較多的區域](../Page/人口.md "wikilink")。通常是[商業區較少](../Page/商業區.md "wikilink")，而以[住宅為主](../Page/住宅.md "wikilink")，或者還有相當程度農業活動但屬於都市行政轄區的地區。因此在[都市圈和](../Page/城市群.md "wikilink")[先進國家](../Page/已開發國家.md "wikilink")，有許多[人口居住郊區但就業和日常活動空間主要在城市裡](../Page/人口.md "wikilink")。

通常一個在城市的周邊、有一定程度的人口之自治區域，要被認定為是「郊區」的條件必須：具備[商業](../Page/商業.md "wikilink")、[行政的基本服務](../Page/行政.md "wikilink")、是連續區域（郊區的詳細定義，不同的[都市計畫師有不同見解](../Page/城市规划.md "wikilink")）。[人口密度普通](../Page/人口密度.md "wikilink")，但比都市中心周邊的住宅密集區域（[內都市](../Page/內都市.md "wikilink")）要來得低（若因為[政府或](../Page/政府.md "wikilink")[財團的計畫性開發](../Page/財閥.md "wikilink")，興建大規模的住宅，這時郊區的人口密度就會提高）。

郊區大多都是位在離城市較近的[平坦地區](../Page/平地.md "wikilink")。郊區出現以前，在[歐洲](../Page/歐洲.md "wikilink")、[亞洲的許多國家](../Page/亞洲.md "wikilink")，過去普遍都有許多居民在[城牆](../Page/城牆.md "wikilink")（將主要的城市包圍起來）外圍居住，並且有密集商業活動的情形。在沒有建設城牆習慣的[美國和](../Page/美國.md "wikilink")[日本](../Page/日本.md "wikilink")，城市外圍一定區域內普遍也有集合居住和商業發展。

以往城市的傳統定義，在[政治和](../Page/政治.md "wikilink")[經濟形態變化](../Page/經濟.md "wikilink")、人口增加的影響下，城市的界限已經漸漸模糊。此外由於[鐵路](../Page/鐵路.md "wikilink")、[巴士](../Page/巴士.md "wikilink")、[自有汽車和](../Page/汽車.md "wikilink")[高速公路等交通設施的出現](../Page/高速公路.md "wikilink")，讓人們能方便地從較遠的地區，前往人口和商業密集的都市中心工作（[通勤](../Page/通勤.md "wikilink")），引此產生了「郊區」。

## 字詞的來源

所謂「郊」，是指古代中國[都城之外](../Page/都城.md "wikilink")、或城鎮之外的地區。「郊」的[部首是](../Page/部首.md "wikilink")「邑」（阝），為「集合體」之意；而旁邊的「交」則有「交結」、「寬廣」的意思。整體的解釋為在「邑」（村落、都市）外的廣大區域。有謂「邑外為郊，郊外為[甸](../Page/甸.md "wikilink")」\[1\]。在[日本稱為](../Page/日本.md "wikilink")「」，其語源的解釋也相同。

[英語為](../Page/英語.md "wikilink")「」，根據[牛津英語詞典](../Page/牛津英語詞典.md "wikilink")，最早的使用記錄是1380年的「subarbis」，這個詞是來自[古法語中的](../Page/古法語.md "wikilink")「」，之後轉變為拉丁語中的「suburbium」（sub
= 下、urbs = 城市）。

在[美國](../Page/美國.md "wikilink")、[加拿大以及大部分的](../Page/加拿大.md "wikilink")[西歐地區](../Page/西歐.md "wikilink")，「suburb」一字是指離開中心城市、不屬於中心城市的自治區域。這是一個很明確的定義，例如在大衛·魯斯克1993年所著的《沒有郊區的城市》（Cities
Without
Suburbs）一書中，他提倡「大都市圈政府」的概念（將以往的市和郡合併，把大都市圈內全部區域的管轄權都交給所屬的地方政府）。在美國有時又簡稱為「'burb」。

在[英國](../Page/英國.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")，「**城區**」（suburb）單純是指都市中心附近人口聚集較多的地區。（例如：[-{zh-hans:布里斯托尔市;
zh-hant:布里斯托市;}-內人口密集的克里夫敦區](../Page/布里斯托尔.md "wikilink")，因為不屬於市中心內的區域，因此被稱為「suburb」）澳洲由於擁有大量的土地、不需要進行城市的防禦，再加上發達的鐵路，早在19世紀就開始發生[城市延伸](../Page/城市延伸.md "wikilink")（[urban
sprawl](../Page/:en:Urban_sprawl.md "wikilink")）現象。由於美國、英國和澳洲對「suburb」的定義不同，經常引起誤會。澳洲的「inner
suburb」是指[雪梨等大城市內人口密集的地區](../Page/雪梨_\(城市\).md "wikilink")，在美國稱為「neighborhood」（近鄰）。「outer
suburb」則是指大城市範圍外緣的區域，與美國所說的「suburb」意義相同。

[日本對於郊區](../Page/日本.md "wikilink")（郊外）的定義並不明確，約略是指大城市的市區和外圍之間、距市中心有一段距離、綠意較多且大多為獨棟住宅的區域。日本政府認定的大都市圈，由於在[國勢調查統計中發表的資料過於簡略](../Page/國勢調查.md "wikilink")，並未如同美國，由[白宮的](../Page/白宮.md "wikilink")[行政管理預算局](../Page/行政管理預算局.md "wikilink")（OMB）依據人口調查的基礎來統一定義「大都市圈」（metropolitan
area）的明確範圍，日本的情況則是有各式各樣的大都市圈定義標準，每個地方政府、研究人員和機構都獨立的進行調查，並未整合並定義出共同的標準。

## 現在郊區面對的問題

### 比市中心人口更多的郊區

[East_LA_Basin_from_Mulholland.jpg](https://zh.wikipedia.org/wiki/File:East_LA_Basin_from_Mulholland.jpg "fig:East_LA_Basin_from_Mulholland.jpg")的郊區，從[好萊塢市的山坡上往](../Page/好萊塢.md "wikilink")[穆赫兰大道方向觀看的郊區景色](../Page/穆赫兰大道.md "wikilink")（遠方看到的摩天大樓是[洛杉磯市中心](../Page/洛杉磯市中心.md "wikilink")）\]\]
傳統上在北美地區，郊區是指建立在商店街、學校附近的居住著整個家庭的獨棟房屋，並且能夠方便到達鐵路、高速公路等交通設施的住宅區。但是現在許多的大都市圈中，由於人口的遽增，使得郊區充斥人口密集的[公寓和](../Page/公寓.md "wikilink")[集合住宅](../Page/公寓大廈.md "wikilink")、複合辦公大樓和輕工業工廠、[購物中心和大賣場](../Page/購物中心.md "wikilink")。

## 参考文献

## 參見

  - [新市鎮](../Page/新市鎮.md "wikilink")
  - [WASP](../Page/WASP.md "wikilink")
  - [貧民區](../Page/貧民區.md "wikilink")

[Category:城镇](../Category/城镇.md "wikilink")
[Category:城市](../Category/城市.md "wikilink")
[Category:城市规划](../Category/城市规划.md "wikilink")
[Category:依類型劃分的聚居地](../Category/依類型劃分的聚居地.md "wikilink")

1.  《[左傳](../Page/左傳.md "wikilink")·昭公九年》：“伯父惠公歸自秦，而誘以來，使偪我諸姬，入我郊甸，則戎焉取之。」[杜預注](../Page/杜預.md "wikilink")：「邑外為郊，郊外為甸。」