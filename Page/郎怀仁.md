<table>
<tbody>
<tr class="odd">
<td><p>{{Infobox Person</p></td>
<td><p>name = 郎怀仁<br />
</p></td>
<td><p>image = Adrien Languillat.jpg</p></td>
<td><p>caption =</p></td>
<td><p>birth_date = 1808年9月28日</p></td>
<td><p>birth_place = </p></td>
<td><p>death_date = 1878年11月30日</p></td>
<td><p>death_place = 中国<a href="../Page/上海.md" title="wikilink">上海</a></p></td>
<td><p>occupation = 罗马天主教直隶东南代牧区宗座代牧<br />
江南代牧区宗座代牧</p></td>
<td><p>spouse = 无</p></td>
<td><p>parents = }}</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

**郎怀仁**（**Msgr.Adrien
Languillat，S.J.**，1808年9月28日—1878年11月30日），法国[耶稣会会士](../Page/耶稣会.md "wikilink")，[天主教直隶东南代牧区](../Page/天主教献县教区.md "wikilink")、[天主教江南代牧区主教](../Page/天主教南京教区.md "wikilink")。

1808年9月28日，郎怀仁出生于[法国东北部](../Page/法国.md "wikilink")[马恩省的村庄Chantemerle](../Page/马恩省.md "wikilink")\[1\]。1821年进入Chalons大修院，1831年10月2日晋升[神父](../Page/神父.md "wikilink")，10年后，1841年2月21日，32岁的郎怀仁在[比利时加入](../Page/比利时.md "wikilink")[耶稣会](../Page/耶稣会.md "wikilink")。

1843年12月，耶稣会派遣第二批前往中国的传教士，其中包括郎怀仁和另外4人，他们和法国公使同船前往中国。1844年10月15日，天主教在中国刚刚解禁，郎怀仁到达上海，被派到[浦东传教](../Page/浦东.md "wikilink")。1846年10月，郎怀仁又被派往山东省传教。1847年9月在平度州被捕，11月被押往上海。1852年代理江南耶稣会会长。1853年6月任[南京教区副主教](../Page/南京教区.md "wikilink")（至1854年8月）。

1856年5月30日，教廷将[直隶东南部和](../Page/直隶.md "wikilink")[江苏](../Page/江苏.md "wikilink")、[安徽](../Page/安徽.md "wikilink")2省划归耶稣会负责，郎怀仁出任[直隶东南代牧区首任代牧](../Page/天主教献县教区.md "wikilink")，领衔赛教波理（Sergiopolis）教区主教。直隶东南代牧区包括河间、大名、广平3府，冀、深2州、35个县，成立时有9500名信徒。1857年3月22日，郎怀仁在北京教区的主教驻地[保定安家庄](../Page/保定.md "wikilink")，由[孟振生](../Page/孟振生.md "wikilink")（Mouly）主教秘密祝圣。郎怀仁起初选定的主教座堂是在威县赵家庄，因为[白莲教盗匪横行](../Page/白莲教.md "wikilink")，1861年10月被迫北迁到[献县城东](../Page/献县.md "wikilink")1千米的张庄，1863年10月2日开始兴建哥特式大教堂—[献县张庄耶稣圣心主教座堂](../Page/献县张庄耶稣圣心主教座堂.md "wikilink")，到1866年3月28日祝圣。主教座堂长50米，宽21米，可容纳2000余人。主教座堂尚未建成，由于在1862年，来访的江南代牧区[年文思主教染](../Page/年文思.md "wikilink")[霍乱身亡](../Page/霍乱.md "wikilink")，于是在1864年9月9日，郎怀仁主教调任[江南代牧区](../Page/天主教上海教区.md "wikilink")。[杜巴尔](../Page/杜巴尔.md "wikilink")（Eduard
Dubar）接任直隶东南代牧区主教。

1865年3月，郎怀仁到达[上海](../Page/上海.md "wikilink")，就任江南代牧区主教。他乘坐法国军舰到长江沿岸的[安庆](../Page/安庆.md "wikilink")、[南京等地](../Page/南京.md "wikilink")，拜会当地的官员，要求在这些地方兴建天主教建筑。他在安庆受到冷落，在南京更是受到[李鸿章极不友善的对待](../Page/李鸿章.md "wikilink")。面对巨大的阻力，郎怀仁决心从收养弃婴着手在江南扩展天主教的事业。1867年3月，郎怀仁远赴罗马，参加庆祝[伯多禄](../Page/伯多禄.md "wikilink")、[保禄殉道](../Page/保禄.md "wikilink")1800周年，5月18日，受到[教宗](../Page/教宗.md "wikilink")[庇护九世赐给](../Page/庇护九世.md "wikilink")“宗座侍卫”称号。9月，郎怀仁前往法国[巴黎](../Page/巴黎.md "wikilink")，推动[拯亡会修女到中国服务](../Page/拯亡会.md "wikilink")。12月，郎怀仁带着第一批[拯亡会修女前往中国](../Page/拯亡会.md "wikilink")。到上海后，将她们安置在徐家汇耶稣会会院对面兴建的徐家汇圣母院，主要从事献堂会貞女的训导工作，后又管理育婴堂。这时，江南代牧区所收婴儿1312名，与中国其他各教区收养的孤儿总数之和相当\[2\]。1868年，安徽、江苏2省多处地方发生教案（参见[扬州教案](../Page/扬州教案.md "wikilink")），神父和教堂遭到袭击。甚至中外关系一向较為和睦的上海街头也出现仇视外国人的揭帖。耶穌会的法国传教士们深感不安，他们许愿：江南代牧区如能平安度过危难，将在佘山上建造一座大教堂。教案平息以后，他们发动教徒捐资和义务劳动，在山顶建造希腊式的大教堂。1871年5月24日的奠基典礼、1873年4月15日的祝圣典礼、5月1日和24日的朝圣仪式，均由郎怀仁主教主持。\[3\]。

1869年，郎怀仁再次前往罗马，参加[第一次梵蒂冈大公会议](../Page/第一次梵蒂冈大公会议.md "wikilink")，表示赞同教宗无谬误信条。

1874年夏季，郎怀仁主教中风卧床。1878年11月30日，郎怀仁主教在上海去世，享年70岁，遗体安葬在董家渡主教座堂。继任者是[倪懷綸主教](../Page/倪懷綸.md "wikilink")（1879年-1898年）。

## 参考文献

<div class="references-small">

<references />

</div>

[L](../Category/1808年出生.md "wikilink")
[L](../Category/1878年逝世.md "wikilink")
[L](../Category/在华天主教传教士.md "wikilink")
[Category:耶稣会](../Category/耶稣会.md "wikilink")

1.  [Bishop Adrien-Hyppolyte Languillat, S.J.
    †](http://www.catholic-hierarchy.org/bishop/blangui.html)
2.  [献县教区发展史目录](http://herder.noip.cn/xxstory.htm)
3.  史式微:江南传教史,上海土山湾孤儿院印书馆,1914年版