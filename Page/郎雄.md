**郎雄**，本名**郎益三**，[台灣](../Page/台灣.md "wikilink")[電影](../Page/電影.md "wikilink")[演員](../Page/演員.md "wikilink")，[满族](../Page/满族.md "wikilink")，[江蘇](../Page/江蘇.md "wikilink")[宿迁人](../Page/宿迁.md "wikilink")。郎雄的演藝生涯開始於軍中的劇團，演技成熟老練，深刻而富情感，多半演出父親或硬漢角色，在正反多種角色上都有精湛表現。郎雄參加[電視劇](../Page/電視劇.md "wikilink")、電影等多種演出，並致力於[宗教與公益活動](../Page/宗教.md "wikilink")。除電影作品外，郎雄也曾與[周迅等合作演出](../Page/周迅.md "wikilink")[公共電視連續劇](../Page/公共電視文化事業基金會.md "wikilink")《[人間四月天](../Page/人間四月天.md "wikilink")》，扮演[梁啟超](../Page/梁啟超.md "wikilink")。

## 生平

  - 1970年，郎雄正式進入[中國電視公司](../Page/中國電視公司.md "wikilink")（中視），演出多部連續劇。
  - 1974年，郎雄主演中視連續劇《一代暴君》，劇中飾演[秦始皇](../Page/秦始皇.md "wikilink")，因表現出色而成名。
  - 1976年，郎雄以《[狼牙口](../Page/狼牙口.md "wikilink")》一片獲得[第13屆金馬獎最佳男配角獎](../Page/第13屆金馬獎#最佳男配角.md "wikilink")\[1\]；同年，與中視導播[包珈結婚](../Page/包珈.md "wikilink")。
  - 1991年，郎雄擔任中視兒童節目《童話世界》第二代（末代）主持人（第一代主持人為[崔麗心](../Page/崔麗心.md "wikilink")），被節目中兒童尊稱為「郎爺爺」，獲得1991年[第26屆金鐘獎最佳兒童節目主持人獎](../Page/第26屆金鐘獎#兒童節目主持人獎.md "wikilink")。
  - 1991年，與[李安合作拍攝](../Page/李安.md "wikilink")《[推手](../Page/推手_\(電影\).md "wikilink")》，以60歲高齡獲選[第28屆金馬獎最佳男主角](../Page/第28屆金馬獎#最佳男主角.md "wikilink")。此後兩人也有多次精彩合作，1993年的《[囍宴](../Page/囍宴_\(電影\).md "wikilink")》讓他再次獲得[第30屆金馬獎最佳男配角獎](../Page/第30屆金馬獎#最佳男配角.md "wikilink")。
  - 1993年，[天主教](../Page/天主教.md "wikilink")[教宗](../Page/教宗.md "wikilink")[若望保祿二世冊封郎雄為](../Page/若望保祿二世.md "wikilink")「聖席爾維斯特騎士團爵士」\[2\]，為天主教友重要榮譽稱號。
  - 1997年，郎雄以《夢土》一劇獲得[第32屆金鐘獎最佳男主角獎](../Page/第32屆金鐘獎#男主角獎.md "wikilink")。
  - 2002年，郎雄病逝於[台北](../Page/台北.md "wikilink")，享年72歲，葬禮由教廷中華民國主教區樞機主教[單國璽主持](../Page/單國璽.md "wikilink")。[第39屆金馬獎晚會](../Page/第39屆金馬獎.md "wikilink")，推舉[李安與](../Page/李安.md "wikilink")[歸亞蕾頒發終身成就獎給郎雄](../Page/歸亞蕾.md "wikilink")，由包珈出席代領。

## 電影作品

  - 1975年，《梦湖》（[唐宝云主演](../Page/唐宝云.md "wikilink")）
  - 1975年，《[八百壮士](../Page/八百壯士_\(1975年電影\).md "wikilink")》（[林青霞主演](../Page/林青霞.md "wikilink")）
  - 1976年，《狼牙口》
  - 1976年，《黑龍會》
  - 1978年，《蒂蒂日记》
  - 1978年，《無情荒地有情天》
  - 1979年，《[早安台北](../Page/早安台北.md "wikilink")》（[钟镇涛主演](../Page/钟镇涛.md "wikilink")）
  - 1979年，《[汪洋中的一條船](../Page/汪洋中的一條船_\(電影\).md "wikilink")》
  - 1980年，《天凉好个秋》
  - 1980年，《[皇天后土](../Page/皇天后土.md "wikilink")》
  - 1981年，《皇天后土》
  - 1986年，《唐山过台湾》
  - 1987年，《跨越时空的小子》
  - 1988年，《[中国最后一个太监](../Page/中国最后一个太监.md "wikilink")》
  - 1990年, 《又見操場》
  - 1990年，《[異域](../Page/異域_\(電影\).md "wikilink")》
  - 1991年，《[推手](../Page/推手_\(电影\).md "wikilink")》
  - 1992年，《五湖四海》
  - 1993年，《[喜宴](../Page/喜宴_\(電影\).md "wikilink")》
  - 1994年，《[飲食男女](../Page/飲食男女_\(電影\).md "wikilink")》
  - 1994年，《祖孙情》
  - 1994年，《情人的情人》
  - 1996年，《今天不回家》
  - 1996年，《[鸦片战争](../Page/鸦片战争_\(电影\).md "wikilink")》
  - 1998年，《[不夜城](../Page/不夜城.md "wikilink")》
  - 1999年，《[春風得意梅龍鎮](../Page/春風得意梅龍鎮.md "wikilink")》
  - 2000年，《[臥虎藏龍](../Page/臥虎藏龍_\(電影\).md "wikilink")》
  - 2002年，《[雙瞳](../Page/雙瞳_\(電影\).md "wikilink")》
  - 2002年，《》

## 電視劇作品

  - [臺灣電視公司](../Page/臺灣電視公司.md "wikilink")：《末代兒女情》（播出期間：1990年9月24日～1990年11月21日）饰演赫天禄
  - [中國電視公司](../Page/中國電視公司.md "wikilink")：

《[長白山上](../Page/長白山上.md "wikilink")》（播出期間：1970年12月21日～1971年3 月13日）
《苦情花》（播出期間：1973年9月3日～1973年11月5日）
《一代暴君》（播出期間：1974年3月17日～1974年5月19日）
《武聖關公》（播出期間：1974年）
《一代紅顏》（播出期間：1975年2月3日～1975年4月11日）
《戰國風雲》（播出期間：1980年11月10日～1980年12月19日）
《天涯赤子情》（播出期間：1982年8月15日～1982年12月19日）
《書劍千秋》（播出期間：1984年9月27日～1984年11月2日）
《[一代女皇](../Page/一代女皇.md "wikilink")》（播出期間：1985年～1986年）饰演[长孙无忌](../Page/长孙无忌.md "wikilink")
《挑伕》（播出期間：1986年12月18日～1987年4月30日）飾演伍大豪
《[一代名臣范仲淹](../Page/一代名臣范仲淹.md "wikilink")》（播出期間：1989年8月28日～1989年9月1日）饰演[吕夷简](../Page/吕夷简.md "wikilink")
《俠客行》
《再回到從前》
《錦繡劇坊──讓我再愛一次》

  - [中華電視公司](../Page/中華電視公司.md "wikilink")：

《大將軍[郭子儀](../Page/郭子儀.md "wikilink")》（共21集，播出期間：1971年11月1日～1971年11月22日）
《婚姻的故事》（共70集，播出期間：1973年10月8日～1974年1月11日）
《[草地狀元](../Page/草地狀元_\(電視劇\).md "wikilink")》（共42集，播出期間：1991年12月5日～1992年1月31日）
《[家有仙妻](../Page/家有仙妻.md "wikilink")》客串董事

  - [中華電視台製作](../Page/中華電視台.md "wikilink")，三台聯播反共復國影集：《[寒流](../Page/寒流_\(影集\).md "wikilink")》（共60集，播出期間：1976年1月12日至1979年4月14日）
  - [台灣公共電視](../Page/台灣公共電視.md "wikilink")：《[人間四月天](../Page/人間四月天.md "wikilink")》（2000年）

## 主持電視節目

  - [中國電視公司兒童節目](../Page/中國電視公司.md "wikilink")《[童話世界](../Page/童話世界.md "wikilink")》〔第二代（末代）主持人〕

## 註解

## 參考來源

  - 書目

<!-- end list -->

  - ；[天主教
    聯合書城](http://books.catholic.tw/tw/store_index.php?publish_id=11)、[book](http://books.catholic.tw/tw/store_book.php?book_id=1474&publish_id=11)

<!-- end list -->

  - 引用

## 外部链接

  - [台灣電影筆記·人物特寫·演員·郎雄](https://web.archive.org/web/20060508060552/http://movie.cca.gov.tw/PEOPLE/people_inside.asp?rowid=61&id=2)

  - [國家文化資料庫：郎雄](http://nrch.cca.gov.tw/ccahome/search/search_meta.jsp?xml_id=0006397391)

  -
  -
  -
  -
  -
[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:臺灣電影男演員](../Category/臺灣電影男演員.md "wikilink")
[Category:台灣電視主持人](../Category/台灣電視主持人.md "wikilink")
[Category:金馬獎終身成就獎得主](../Category/金馬獎終身成就獎得主.md "wikilink")
[台](../Category/金馬獎最佳男主角獲得者.md "wikilink")
[Category:金馬獎最佳男配角獲得者](../Category/金馬獎最佳男配角獲得者.md "wikilink")
[Category:金鐘獎戲劇節目男主角得主](../Category/金鐘獎戲劇節目男主角得主.md "wikilink")
[Category:金鐘獎兒童少年電視節目主持人得主](../Category/金鐘獎兒童少年電視節目主持人得主.md "wikilink")
[Category:臺灣天主教徒](../Category/臺灣天主教徒.md "wikilink")
[Category:臺灣滿族](../Category/臺灣滿族.md "wikilink")
[Category:江蘇裔台灣人](../Category/江蘇裔台灣人.md "wikilink")
[Category:宿迁人](../Category/宿迁人.md "wikilink")
[X雄](../Category/郎姓.md "wikilink")

1.  [1976-10-30／段雲生／攝影](http://cna.moc.gov.tw/movies/images/d/d3/3001.jpg)
2.