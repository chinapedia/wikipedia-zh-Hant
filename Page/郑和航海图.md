[WuBeiZhi.jpg](https://zh.wikipedia.org/wiki/File:WuBeiZhi.jpg "fig:WuBeiZhi.jpg")
《**郑和航海图**》（全名《**自宝船厂开船从龙江关出水直抵外国著番图**》），原载[明](../Page/明.md "wikilink")[茅元仪编](../Page/茅元仪.md "wikilink")《[武备志](../Page/武备志.md "wikilink")》卷二百四十。《郑和航海图》又称《茅坤图》（Mao
Kun
map）。《武备志》成书于[明](../Page/明.md "wikilink")[崇祯元年](../Page/崇祯.md "wikilink")（1628年），而所收入的《郑和航海图》，据一些学者研究，应该是[郑和下西洋时所绘制发给郑和船队上的](../Page/郑和下西洋.md "wikilink")[舟师的航海图](../Page/舟师.md "wikilink")，根据如下：

1.  茅元仪序言中有字句：“当是时，臣为内监郑和，亦不辱命，其图列道里国土……载以昭来世，志武功也”。
2.  曾在明[宣德六年](../Page/宣德.md "wikilink")（1431年）至宣德八年（1433年）下西洋的[巩珍在](../Page/巩珍.md "wikilink")[宣德九年](../Page/宣德.md "wikilink")（1434年）所写的《[西洋番国志](../Page/西洋番国志.md "wikilink")·自序》中写道：下西洋前预先到[福建](../Page/福建.md "wikilink")[广东](../Page/广东.md "wikilink")[浙江招募有出海经验的船员](../Page/浙江.md "wikilink")，“用作船师，乃以针经图式付与领执，专一料理，事大责重，岂容怠忽”。《郑和航海图》正是这样的针经图式。

学者徐玉虎更将《郑和航海图》绘制的时间，定为1425年至1430年间。原因如下：

1.  航海图第一页有[静海寺](../Page/静海寺.md "wikilink")，此寺建成于明[洪熙元年](../Page/洪熙.md "wikilink")（1425年），可见航海图制作在1425年之后。
2.  明宣德五年（1430年）郑和第七次下西洋，命一队船从[古里](../Page/古里.md "wikilink")[拔都它港出发前往](../Page/拔都它.md "wikilink")[天方国](../Page/天方.md "wikilink")。然而《航海图》中没有拔都它港，可见航海图绘制在1430年前\[1\]。

## 内容

《郑和航海图》共20页航海地图、109条针路航线和4幅[过洋牵星图](../Page/过洋牵星术.md "wikilink")，航海地图高20.3厘米,全长560厘米，包含500个地名。

### 航海地图

[thumb](../Page/文件:郑和航海图五.jpg.md "wikilink")

  - 第2页：自宝船厂开船从龙江关出水图，上有[宝船厂](../Page/宝船厂.md "wikilink")、[静海寺](../Page/静海寺.md "wikilink")、[天妃宫](../Page/南京天妃宫.md "wikilink")、天地坛
  - 第3页、沿长江东航经过皇城、[钟山](../Page/紫金山.md "wikilink")、龙谭、观音门、燕子集、[瓜州](../Page/瓜州.md "wikilink")、[金山](../Page/金山_\(鎮江\).md "wikilink")、大港、[焦山](../Page/焦山.md "wikilink")、[镇江](../Page/镇江.md "wikilink")
  - 第4页
    沿长江东航经过东鞋山、[江阴](../Page/江阴.md "wikilink")、香山、龙王庙、天妃宫、巡司、蔡港、白茅港、[太仓卫](../Page/太仓.md "wikilink")
  - 第5页
    [吴淞江](../Page/吴淞江.md "wikilink")、天妃宫、[崇明州](../Page/崇明.md "wikilink")、茶山、召宝山、[南汇](../Page/南汇.md "wikilink")、金沙卫、[乍浦](../Page/乍浦.md "wikilink")、[海宁卫](../Page/海宁.md "wikilink")、灵山卫、昌国所、[普陀山](../Page/普陀山.md "wikilink")
  - 第6页
    大磨山、双屿门、东屿、东厨、大面山、郭巨千户所、大嵩千户所、东门山、三母山、[焦山](../Page/焦山.md "wikilink")
  - 第7页
    松门卫、大尖经、钱山、中界山、石塘、虎斗、[黄山](../Page/黄山.md "wikilink")、[温州卫](../Page/温州.md "wikilink")、[平阳卫](../Page/平阳.md "wikilink")
  - 第8页 虎礁、壮士千户所、满门千户所、芙蓉山、洪山、龟屿、大金门
  - 第9页
    古山、[福建布政司](../Page/福建.md "wikilink")、五虎山、[长乐](../Page/长乐.md "wikilink")、南山寺、东沙、乌邱山、[平海卫](../Page/平海.md "wikilink")、[兴化府](../Page/兴化.md "wikilink")
  - 第10页
    [泉州卫](../Page/泉州.md "wikilink")、崇武所、[漳州](../Page/漳州.md "wikilink")、[平湖屿](../Page/澎湖群島.md "wikilink")、大甘、[东莞](../Page/东莞.md "wikilink")、[大奚山](../Page/大奚山.md "wikilink")、[佛堂門](../Page/佛堂門.md "wikilink")、
  - 第11页
    南海卫、[广东](../Page/广东.md "wikilink")、[雷州](../Page/雷州.md "wikilink")、[高州](../Page/高州.md "wikilink")、[廉州](../Page/廉州.md "wikilink")、[琼州府](../Page/琼州.md "wikilink")、[七洲](../Page/七洲洋.md "wikilink")、[石塘](../Page/西沙群岛.md "wikilink")、[石星石塘](../Page/东沙群岛.md "wikilink")、[钦州](../Page/钦州.md "wikilink")、交洋、[福州](../Page/福州.md "wikilink")
  - 第12页
    [占城国](../Page/占城.md "wikilink")、[新洲港](../Page/新洲港.md "wikilink")、洋屿、小弯、赤坎、大弯
  - 第13页
    [占腊国](../Page/真腊.md "wikilink")、[竹里木](../Page/竹里木.md "wikilink")、八开港、铜鼓山、[万年屿](../Page/汶萊.md "wikilink")、佛山、[暹罗国](../Page/暹罗.md "wikilink")、笔架山、十二子山、[假里马达](../Page/加里曼丹.md "wikilink")
  - 第14页
    赤坎、马鞍山、佛山、海门山、石礁、[狼西加](../Page/狼西加.md "wikilink")、[孫姑那](../Page/孫姑那.md "wikilink")
    、[吉蘭丹港](../Page/吉蘭丹.md "wikilink")、长腰屿、[丁家下路](../Page/丁加奴.md "wikilink")、[爪哇](../Page/爪哇.md "wikilink")。
  - 第15页
    [龙牙门](../Page/龙牙门.md "wikilink")、[旧港](../Page/旧港.md "wikilink")、答那溪屿、[淡马锡](../Page/新加坡.md "wikilink")、[彭杭](../Page/彭亨.md "wikilink")、琵琶屿、凉伞屿、长腰屿、甘巴门、甘巴港
  - 第16页
    白沙、射箭山、[满剌加](../Page/满剌加.md "wikilink")、官厂、沙糖礁、鸡骨屿、龙牙加儿山、龙牙加儿山港、金屿
  - 第17页
    班卒、[槟榔屿](../Page/槟榔屿.md "wikilink")、[龙牙交椅](../Page/龙牙交椅.md "wikilink")、[古力由不洞](../Page/古力由不洞.md "wikilink")、单屿、[亚路](../Page/亚路.md "wikilink")、[苏门答剌](../Page/苏门答剌.md "wikilink")、官厂。
  - 第18页
    [八都马](../Page/八都马.md "wikilink")、打歪山、[虎尾礁](../Page/虎尾礁.md "wikilink")、[龙涎屿](../Page/龙涎屿.md "wikilink")
    [花面](../Page/那孤儿.md "wikilink")
  - 第19页
    [榜葛剌](../Page/孟加拉.md "wikilink")、竹牌礁、[锡兰山](../Page/锡兰.md "wikilink")、[门肥赤](../Page/门肥赤.md "wikilink")、[小葛兰](../Page/小葛兰.md "wikilink")、[高朗务](../Page/高朗务.md "wikilink")、[慢巴撒](../Page/慢巴撒.md "wikilink")、[麻林地](../Page/麻林地.md "wikilink")。
  - 第20页 [柯枝国](../Page/柯枝.md "wikilink")、
    [古里国](../Page/古里.md "wikilink")、[乌里舍城](../Page/乌里舍.md "wikilink")、[木骨都束](../Page/木骨都束.md "wikilink")
  - 第21页
    [左法儿](../Page/左法儿.md "wikilink")、阿胡那、[八思尼](../Page/八思尼.md "wikilink")、[木克郎](../Page/木克郎.md "wikilink")、[克瓦塔儿](../Page/克瓦塔儿.md "wikilink")、[苦思答儿](../Page/苦思答儿.md "wikilink")、[古里牙](../Page/古里牙.md "wikilink")。
  - 第21页 苦碌麻剌、[忽鲁谟斯](../Page/忽鲁谟斯.md "wikilink")

### 针路航线

109条针路，例如：

  - [太仓港口开船](../Page/太仓.md "wikilink")，用丹乙针（105°），一[更](../Page/更.md "wikilink")，平[吴淞江](../Page/吴淞江.md "wikilink")。（郑和航海图五）
  - 从[苏门答剌开船](../Page/苏门答剌.md "wikilink")，用乾戌针（307.5°），十二更，船平[龙涎屿](../Page/龙涎屿.md "wikilink")。
  - [官屿溜用庚酉针](../Page/官屿溜.md "wikilink")（262.5°），一百五十更，船收[木骨都束](../Page/木骨都束.md "wikilink")。

### 过洋牵星图

  - 四幅[过洋牵星图](../Page/过洋牵星术.md "wikilink"):

[Stellardiagram-Zhengho.jpg](https://zh.wikipedia.org/wiki/File:Stellardiagram-Zhengho.jpg "fig:Stellardiagram-Zhengho.jpg")

1.  [丁得把昔到](../Page/丁得把昔.md "wikilink")[忽鲁谟斯](../Page/忽鲁谟斯.md "wikilink")：从印度[代奥格尔](../Page/代奥格尔.md "wikilink")（Deogarh）到忽鲁谟斯；用[北辰星](../Page/北极星.md "wikilink")、[织女星](../Page/织女星.md "wikilink")、[灯笼骨星定位](../Page/灯笼骨星.md "wikilink")。
2.  锡兰山回[苏门答剌过洋牵星图](../Page/苏门答剌.md "wikilink")：用[北辰星](../Page/北极星.md "wikilink")、[织女星](../Page/织女星.md "wikilink")、华盖星、灯笼骨星定位。
3.  [龙涎屿往](../Page/龙涎屿.md "wikilink")[锡兰过洋牵星图](../Page/锡兰.md "wikilink")
4.  [忽鲁谟斯国回](../Page/忽鲁谟斯.md "wikilink")[古里国过洋牵星图](../Page/古里.md "wikilink")。

“忽鲁谟斯回来[沙姑马开洋](../Page/沙姑马.md "wikilink")，看北辰星十一指（水平线上
17度36分），看东边[织女星七指](../Page/织女星.md "wikilink")（水平线上
11度12分）为母，看西南[布司星八指](../Page/布司星.md "wikilink")（水平线上
12度48分）平[丁得把昔](../Page/丁得把昔.md "wikilink")，看[北辰星七指](../Page/北辰星.md "wikilink")，看东边织女星七指為母，看西北布司星八指。沙姑马开洋看北辰星十一指（水平线上
17度36分）平水。丁得把昔过洋看北辰星七指平水。

## 历史

《武备志》中《郑和航海图》在中国默默无闻二百多年。1885年英国学者[乔治·菲立浦](../Page/乔治·菲立浦.md "wikilink")
(George Philips)
在所著论文《印度和锡兰的海港》中首次将郑和航海图复制，并考证了其中一百多个地名。从此《郑和航海图》才引起学者门注意和研究。先后研究《郑和航海图》的学者有[伯希和](../Page/伯希和.md "wikilink")、[兑温达](../Page/兑温达.md "wikilink")、[向达](../Page/向达.md "wikilink")、[密尔斯等](../Page/密尔斯.md "wikilink")；兑温达提议称《郑和航海图》为《茅坤图》（Mao
Kun map）。

## 參考來源

<div class="references-small">

<references />

</div>

## 參考文獻

  - [向达校注](../Page/向达.md "wikilink") 《郑和航海图》中华书局 ISBN 710102025-9
    （每幅航海图仅7x9厘米，字小）
  - 明 [马欢原著](../Page/马欢.md "wikilink") 万明校注 明钞本
    《[瀛涯胜览](../Page/瀛涯胜览.md "wikilink")》校注 海洋出版社 2005
    ISBN 7-5027-6378-3
      - 附录七：茅元仪《武备志·郑和航海图》 （航海图较大，每幅9x12.5厘米）

(以上两种版本的航海图和[过洋牵星图的图板都较小](../Page/过洋牵星术.md "wikilink"))

  - Ma Huan：Ying yai sheng lan （The Overall survey of the ocean
    shores），trans. with notes

by J.V.G.Mills, Cambridge University Press, translated from the Chinese
text edited by Feng Chen jun. with Mao K'un Chart, White Lotus Press,
Bangkok 1997. ISBN 974-8496-78-3

  -   - Appendix 2:The Mao Kun Map.

（无航海图,但四幅过洋牵星图板大而清晰.）

1.  丁得把昔到忽鲁谟斯
2.  锡兰山回苏门答剌过洋牵星图
3.  龙延屿往锡兰过洋牵星图
4.  忽鲁谟斯国回古里国过洋牵星图

## 參見

  - [塞爾登地圖](../Page/塞爾登地圖.md "wikilink"):同類中國古航海圖。

## 外部链接

  - [National Geographic
    郑和航海图今析](http://ngmchina.com.cn/web/?action-viewnews-itemid-12874)

[category:中外交通史文献](../Page/category:中外交通史文献.md "wikilink")
[category:郑和下西洋](../Page/category:郑和下西洋.md "wikilink")

[Category:歷史地圖](../Category/歷史地圖.md "wikilink")
[Category:1420年代](../Category/1420年代.md "wikilink")

1.  徐玉虎，《郑和下西洋航海图考》，《郑和下西洋研究文选 》，530页。ISBN 7-5027-6377-5