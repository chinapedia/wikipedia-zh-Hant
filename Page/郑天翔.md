**郑天翔**（），[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")（原[绥远省](../Page/绥远省.md "wikilink")）凉城县人，曾用名郑庭祥。1936年12月加入[中国共产党](../Page/中国共产党.md "wikilink")；原[中华人民共和国最高人民法院院长](../Page/中华人民共和国最高人民法院.md "wikilink")；[中共第十二](../Page/中共.md "wikilink")、十三届中央顾问委员会委员。

## 简历

郑天翔早年就读于[國立中央大学农业化学系](../Page/國立中央大学.md "wikilink")；1935年转到[北平](../Page/北平.md "wikilink")[清华大学文学院外国文学系学习](../Page/清华大学.md "wikilink")，后转入该院哲学系，参加了当时[北平学生](../Page/北平.md "wikilink")-{}-发动的「[一二九运动](../Page/一二九运动.md "wikilink")」。

  - 1936年2月，加入[左翼作家联盟和民族解放先锋队](../Page/左翼作家联盟.md "wikilink")；并参与组织了晋绥旅京同学抗日联合会。
  - [抗日战争时期](../Page/抗日战争.md "wikilink")，1937年到[延安](../Page/延安.md "wikilink")[陕北公学学习](../Page/陕北公学.md "wikilink")；并先后在陕公生活指导委员会训育科和陕北公学同学会工作。1938年底，调晋察冀边区北岳区宣传部工作。1943年，任[阜平县委副书记兼任宣传部长](../Page/阜平县.md "wikilink")、[聂荣臻秘书](../Page/聂荣臻.md "wikilink")。1945年，参与组建中共塞北地委，并任宣传部长。
  - [解放战争时期](../Page/解放战争.md "wikilink")，历任绥南专员（后兼凉城县长）、绥南地委敌军部长，凉城中心县委书记、绥南工委副书记。1947年先后到右玉县西山搞土改，到晋绥党校学习。1948年，任中共中央华北局宣传部宣传科长。
  - 建国后，1949年12月起，先后任绥远军政委员会包头工作团团长，[中共](../Page/中共.md "wikilink")[包头市委副书记](../Page/包头.md "wikilink")、书记，兼任[包头市市长](../Page/包头市.md "wikilink")。
  - 1952年起，历任[中共](../Page/中共.md "wikilink")[北京市委常委兼秘书长](../Page/北京.md "wikilink")、市委副书记兼秘书长，市委书记处书记兼秘书长。在“[文革](../Page/文革.md "wikilink")”中受到冲击。
  - 1975年，任[北京市建委副主任](../Page/北京.md "wikilink")。1977年，任[中共](../Page/中共.md "wikilink")[北京市委书记](../Page/北京.md "wikilink")、市革委会副主任。
  - 1978年后历任第七机械工业部第一副部长、党组第一副书记，七机部部长、党组第一书记，七机部顾问等职。
  - 1983年，在第六届全国人民代表大会第一次会议上当选为[最高人民法院院长](../Page/最高人民法院.md "wikilink")。

郑天翔历任中国共产党第七、八、十二、十三届代表大会代表。在中共党的十二、十三届代表大会上，当选为中共中央顾问委员会委员。

  - 郑天翔，因病医治无效，于2013年10月10日20时在[北京逝世](../Page/北京.md "wikilink")，享年99岁。\[1\]\[2\]

## 评价

新华社10月11日发表讣告称郑天翔为“中国共产党的优秀党员，久经考验的忠诚的共产主义战士，无产阶级革命家，我国政法战线的杰出领导人”。

## 参考文献

<references />

{{-}}

[Category:中共中央顾问委员会委员](../Category/中共中央顾问委员会委员.md "wikilink")
[Category:中華人民共和國領導人](../Category/中華人民共和國領導人.md "wikilink")
[Category:中國共產黨黨員](../Category/中國共產黨黨員.md "wikilink")
[農](../Category/國立中央大学校友.md "wikilink")
[農](../Category/中央大学校友.md "wikilink")
[Category:南京大學校友](../Category/南京大學校友.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:國立清華大学校友](../Category/國立清華大学校友.md "wikilink")
[Category:凉城县人](../Category/凉城县人.md "wikilink")
[Tian天翔](../Category/郑姓.md "wikilink")

1.  [最高人民法院原院长郑天翔逝世](http://www.apdnews.com/news/37123.html)
    ，亚太日报，2013年10月12日
2.  [最高法原院长郑天翔10日逝世
    享年99岁](http://news.ifeng.com/mainland/detail_2013_10/11/30238871_0.shtml)，凤凰网。