**郭伯偉**爵士，[KBE](../Page/KBE.md "wikilink")，[CMG](../Page/CMG.md "wikilink")（，），[英國](../Page/英國.md "wikilink")[殖民地官員](../Page/殖民地.md "wikilink")，1961年至1971年出任[香港](../Page/香港.md "wikilink")[財政司](../Page/財政司.md "wikilink")。任內貫徹地推行「[自由放任](../Page/自由放任.md "wikilink")」政策，除了維持低[稅政策外](../Page/稅.md "wikilink")，還減少[政府對](../Page/政府.md "wikilink")[市場的干預](../Page/市場.md "wikilink")。他的經濟哲學被往後的財政司加以發揮，為香港政府後來的「[積極不干預主義](../Page/積極不干預主義.md "wikilink")」、審慎理財和[自由市場等方針立下基調](../Page/自由市場.md "wikilink")，亦促使[香港經濟急速發展](../Page/香港經濟.md "wikilink")，為七、八十年代的經濟起飛做好準備，而自1970年起，[加拿大費沙爾學會更每年都將香港評為](../Page/加拿大費沙爾學會.md "wikilink")「全球最自由的經濟體系」。

郭伯偉爵士的經濟哲學深受18世紀主張[自由貿易的哲學家](../Page/自由貿易.md "wikilink")[亞當·史密斯影響](../Page/亞當·史密斯.md "wikilink")；而他自己任內的經濟政策，也啟發了[英](../Page/英國.md "wikilink")、[美等國政府在八十年代的經濟方針](../Page/美國.md "wikilink")，起了承先啟後的作用。

## 生平

### 早年生涯

郭伯偉祖居[蘇格蘭](../Page/蘇格蘭.md "wikilink")，1915年4月25日生於[愛丁堡](../Page/愛丁堡.md "wikilink")\[1\]，父親與他同名約翰·詹姆士（John
James Cowperthwaite），在當地稅務部門工作；母親叫傑西·威姆斯·巴倫·賈維斯（Jessie Wemyss Barron
Jarvis
Cowperthwaite），祖籍[葡萄牙](../Page/葡萄牙.md "wikilink")[阿爾加威](../Page/阿爾加威.md "wikilink")\[2\]。郭伯偉另一有名胞弟名大衛·賈維斯（David
Jarvis
Cowperthwaite，1921年9月14日－2006年10月20日），他於1939年畢業於[牛津大學](../Page/牛津大學.md "wikilink")[艾克塞特學院](../Page/牛津大學艾克塞特學院.md "wikilink")，後來在蘇格蘭任[公務員](../Page/公務員.md "wikilink")，曾在當地內政及衛生部門出任[次官](../Page/常務次官.md "wikilink")\[3\]\[4\]。

郭伯偉早年入讀愛丁堡附近的[墨奇斯頓城堡公學](../Page/墨奇斯頓城堡公學.md "wikilink")，畢業後於1933年入讀[聖安德魯斯大學主修古典文學](../Page/聖安德魯斯大學.md "wikilink")，後以一級榮譽畢業\[5\]。取得學士學位後，他又升讀[劍橋大學](../Page/劍橋大學.md "wikilink")[基督學院修讀古典文學碩士學位](../Page/劍橋大學基督學院.md "wikilink")，並取得雙重一級榮譽\[6\]。[第二次世界大戰在](../Page/第二次世界大戰.md "wikilink")1939年爆發後，郭伯偉原本應募加入[蘇格蘭步兵團](../Page/蘇格蘭步兵團.md "wikilink")，但輪候期間返回聖安德魯斯大學修讀[經濟學](../Page/經濟學.md "wikilink")，並取得經濟學學位\[7\]。

### 殖民地生涯

郭伯偉在戰時沒有參與戰爭，相反，他在1941年獲殖民地部聘請到[香港任](../Page/香港.md "wikilink")[官學生](../Page/官學生.md "wikilink")。然而郭伯偉乘船出發至[南非](../Page/南非.md "wikilink")[開普敦時](../Page/開普敦.md "wikilink")，他得悉香港已被[日本侵佔淪陷](../Page/日本.md "wikilink")，因此他沒有到任\[8\]。在[倫敦當局發](../Page/倫敦.md "wikilink")[電報指示下](../Page/電報.md "wikilink")，郭伯偉遂臨時轉到英國另一[殖民地](../Page/殖民地.md "wikilink")[塞拉利昂的殖民地政府任](../Page/塞拉利昂.md "wikilink")[弗里敦民政專員](../Page/弗里敦.md "wikilink")\[9\]。

在1945年年初，有鑑於盟軍局勢日益明朗，郭伯偉獲召返倫敦，並在4月起於殖民地部的香港規劃工作小組中參與訂定有關英國重新對香港作出管治的計劃\[10\]。隨著香港在1945年8月[重光後](../Page/香港重光.md "wikilink")，他在同年11月抵達香港，並被派到政府的[貿易及經濟事務部工作](../Page/貿易及經濟事務部.md "wikilink")，此後獲得連番擢升。在1952年，郭伯偉獲擢升為助理財政司，後來又升任為[副財政司](../Page/副財政司.md "wikilink")\[11\]。在1961年4月17日，他接替[歧樂嘉任](../Page/歧樂嘉.md "wikilink")[財政司](../Page/香港財政司.md "wikilink")\[12\]，展開長達10年的財政司生涯。

### 財政司

#### 背景

受到著名哲學家[亞當·史密斯的思想影響](../Page/亞當·史密斯.md "wikilink")，郭伯偉是[自由貿易及](../Page/自由貿易.md "wikilink")[自由市場的忠實支持者](../Page/自由市場.md "wikilink")\[13\]\[14\]\[15\]；在他上任財政司的時候，香港剛經歷過港督[葛量洪爵士的管治](../Page/葛量洪.md "wikilink")，經濟基石由轉口貿易轉型成為以蓬勃的[輕工業為主導](../Page/輕工業.md "wikilink")，為其施政創造有利的背景。與其他[殖民地官員一樣](../Page/殖民地.md "wikilink")，郭伯偉是委任的官員，不需要考慮任何[選舉及選民的因素](../Page/選舉.md "wikilink")，亦無需要以短視的目光取悅市民\[16\]；再加上繼任港督[柏立基爵士爭取到為港府取得](../Page/柏立基.md "wikilink")「財政自主權」，以後港府無須再將其預算交予[倫敦審查](../Page/倫敦.md "wikilink")，這些條件促使郭伯偉可以在無拘無束的情況下，放心推行所謂的經濟「[自由放任](../Page/自由放任.md "wikilink")」政策\[17\]。

#### 低稅政策

在[1960年代的香港](../Page/香港1960年代.md "wikilink")，由於內地政局的不穩定，難民的湧入使到香港的貧困人口急升，為了改善香港普羅大眾的生活，郭伯偉對自由放任政策加以發揮\[18\]。當中，有見於香港天然資源缺乏，無論是日用品、食物以至食水都十分依賴[進口](../Page/進口.md "wikilink")，他任內積極維持極低的[關稅](../Page/關稅.md "wikilink")[政策](../Page/低稅政策.md "wikilink")，讓價廉物美的進口貨品流入香港，使市民無需購買成本昂貴的本地貨品\[19\]\[20\]。

郭伯偉任內亦將[薪俸稅維持在不多於個人總入息的百分之十五的水平](../Page/薪俸稅.md "wikilink")，又刪除一些舊有的稅項，以設法維持低稅的政策\[21\]\[22\]。郭伯偉的低稅政策曾經為[英國方面所反對](../Page/英國.md "wikilink")，而英國方面甚至曾建議香港應跟隨新加坡大幅上調薪俸稅\[23\]，但其政策始終得到柏立基爵士及繼任[港督的](../Page/港督.md "wikilink")[戴麟趾爵士所信用而沒有改變](../Page/戴麟趾.md "wikilink")\[24\]。另外，時任英國[國防大臣](../Page/國防大臣.md "wikilink")[丹尼士·希利曾建議過部份](../Page/丹尼士·希利.md "wikilink")[駐港英軍的軍費以稅收形式轉嫁香港市民](../Page/駐港英軍.md "wikilink")，但建議同樣被郭伯偉大力反對而泡湯\[25\]。

#### 減少干預

除了低稅政策以外，對市場干預減至最少也是郭伯偉的主要方針\[26\]。一方面，郭伯偉反對對市場進行任何資助或補貼，因此他不支持對本地工業進行補貼，也沒有應要求向興建中的[紅磡海底隧道提供資助](../Page/紅磡海底隧道.md "wikilink")\[27\]。有趣的是，郭伯偉任內甚至拒絕接受財政司用以翻新官邸的津貼\[28\]，反映出他在反對干預上作出的堅持；另一方面，他亦反對對市場作出過份規限，正因如此，他大力反對[英倫銀行在](../Page/英倫銀行.md "wikilink")1962年向港府建議立法限制[商業銀行的數量](../Page/商業銀行.md "wikilink")；在他的影響下，有關立法最終在1964年進行時，條例比原先的建議大幅放鬆\[29\]。

[HK_Cross_Harbour_Tunnel.jpg](https://zh.wikipedia.org/wiki/File:HK_Cross_Harbour_Tunnel.jpg "fig:HK_Cross_Harbour_Tunnel.jpg")提供資助。\]\]
儘管香港後來接續爆發過[1965年股災](../Page/香港1965年股災.md "wikilink")，以及[六六與](../Page/六六暴動.md "wikilink")[六七暴動](../Page/六七暴動.md "wikilink")，但這些短暫的動盪並無嚴重影響其政策\[30\]。及至1967年11月19日，[英國首相](../Page/英國首相.md "wikilink")[韋爾遜突然宣佈](../Page/韋爾遜.md "wikilink")[英鎊貶值百分之十四點三](../Page/英鎊.md "wikilink")，與英鎊掛勾的[港元隨即跟隨大幅](../Page/港元.md "wikilink")[貶值](../Page/貶值.md "wikilink")，郭伯偉在港元貶值後不出九十小時，成功使港元幣值回升百分之十，但事件使庫房損失約3,000萬鎊。郭伯偉有關行動是港府歷史上首次自行決定港元匯率，而七年以後，港元亦告脫離英鎊作自由浮動\[31\]\[32\]。

郭伯偉在任內反對政府對經濟狀況作出詳細[統計](../Page/統計學.md "wikilink")，也是其反對市場干預的要點之一\[33\]。郭伯偉曾言貧窮國家要脫貧，首要「應廢除國家統計部門」\[34\]；他認為對經濟狀況作統計十分危險，因為政府會不知不覺地根據數據作出干預，並阻礙市場自然復甦的機制。郭伯偉甚至曾言「國民經濟核算是學者而不是官方研究的課題」\[35\]，雖然這些意見曾受到英國政府的強烈反嚮，但這種態度在其任內沒有改變過。

值得注意的是，郭伯偉在任財政司並非完全沒有干預市場。比如在1960年代，港府著力開展所謂的「[廉租屋計劃](../Page/廉租屋計劃.md "wikilink")」，都反映出市場被一定的干擾。不過，從郭伯偉的觀點出發，「廉租屋計劃」本身並不代表[福利主義](../Page/福利主義.md "wikilink")，計劃本身出於政府有急切必要清除[寮屋區](../Page/寮屋區.md "wikilink")，而寮屋區的土地也有發展的必要\[36\]；至於所謂的「廉租」，則某義上是對寮屋居民徙置後的一種「補償」\[37\]。這種對干預可免則免的態度，到後來港督[麥理浩爵士](../Page/麥理浩.md "wikilink")（後為勳爵）在[1970年代推行](../Page/香港1970年代.md "wikilink")「[十年建屋計劃](../Page/十年建屋計劃.md "wikilink")」及「[居者有其屋計劃](../Page/居者有其屋計劃.md "wikilink")」才有明顯改變\[38\]。總言之，郭伯偉認為，只有貧民等等被市場機制忽略，而又有急切需要的時候，政府才應作出干預\[39\]。

在[教育方面](../Page/教育.md "wikilink")，同樣，儘管政府有繼續在1960年代興建新的官立學校，但總體上，香港主要的學校都是由[教會等私人辦學團體營運](../Page/教會.md "wikilink")，干預並不明顯\[40\]。此外，郭伯偉亦反對為香港引入[免費教育](../Page/免費教育.md "wikilink")，惟面對日益強烈的呼聲，他終在1971年卸任前夕勉強支持引入六年強迫[小學義務教育](../Page/小學.md "wikilink")\[41\]。

#### 影響

郭伯偉在1971年6月30日從政府退休，財政司一職由[夏鼎基](../Page/夏鼎基.md "wikilink")（後為爵士）接任\[42\]。郭伯偉卸任時，在他的政策推動下，[香港經濟出現了深刻的改變](../Page/香港.md "wikilink")。首先，在1961年時，香港人的人均收入僅約及[英國的四分之一](../Page/英國.md "wikilink")，政府儲備只有7,750萬；但到1971年，香港人的平均收入有百分之五十的實質增長，政府儲備亦暴增至6億4,000萬\[43\]\[44\]。後來在1990年代，香港的人均收入甚至超越了英國。另外，郭伯偉任內成功令香港的貧窮人口減少三分之二；對外[出口亦以每年百分之十四的幅度不斷增長](../Page/出口贸易.md "wikilink")，使香港轉型成區內的主要[製造業中心](../Page/製造業.md "wikilink")\[45\]；並為香港在70及80年代的經濟起飛立下重要基石\[46\]。

在經濟不斷改善的同時，郭伯偉貫徹奉行「[自由放任](../Page/自由放任.md "wikilink")」政策，使公共支出在其任內一直維持於甚低水平\[47\]。郭伯偉的經濟哲學對後世起了很大影響，面對宗主國英國受到[福利主義及](../Page/福利主義.md "wikilink")[社會主義思潮主導](../Page/社會主義.md "wikilink")，香港背靠的[中國大陸又提倡](../Page/中國大陸.md "wikilink")[共產主義的時候](../Page/共產主義.md "wikilink")\[48\]，郭伯偉的「自由放任」政策實具有革命性的意義。自他以後，繼任財政司[夏鼎基爵士從他的思想基礎上發展出](../Page/夏鼎基.md "wikilink")「[積極不干預主義](../Page/積極不干預主義.md "wikilink")」，讓經濟自由主義繼續在香港演進\[49\]；後來政府的「大市場，小政府」方針，都可以上溯至郭伯偉與夏鼎基爵士兩人的經濟哲學\[50\]。總言之，在郭伯偉萌發的「自由放任」政策影響下，香港逐漸成為了高度自由的經濟體系，而自[加拿大費沙爾學會在](../Page/加拿大費沙爾學會.md "wikilink")1970年開始發表《世界經濟自由度》報告以來，香港更每年都被評為「全球最自由的經濟體系」\[51\]。

除了香港以外，郭伯偉的經濟思想還下啟1980年代[美國總統](../Page/美國總統.md "wikilink")[列根及](../Page/列根.md "wikilink")[英國首相](../Page/英國首相.md "wikilink")[戴卓爾夫人所奉行的](../Page/戴卓爾夫人.md "wikilink")[新自由主義經濟政策](../Page/新自由主義.md "wikilink")，而中國大陸在1980年代推行的「[改革開放](../Page/改革開放.md "wikilink")」，也某程度上受到郭伯偉的影響\[52\]。郭伯偉在香港的經驗使香港成為[自由市場的一大典範](../Page/自由市場.md "wikilink")，不少知名學者如[佛利民和](../Page/佛利民.md "wikilink")[海耶克甚至視之為教科書的基礎例子](../Page/弗里德里克·哈耶克.md "wikilink")，來展示[自由的經濟政策如何帶來經濟的發展與繁榮](../Page/自由.md "wikilink")\[53\]。

### 晚年生涯

離開港府後，郭伯偉在1972年獲聘到香港[投資銀行](../Page/投資銀行.md "wikilink")[怡和富林明公司](../Page/怡和富林明.md "wikilink")，並出任國際顧問一職至1981年\[54\]，此後他主要在[蘇格蘭](../Page/蘇格蘭.md "wikilink")[法夫郡](../Page/法夫.md "wikilink")[聖安德魯斯與妻子過著退休生活](../Page/聖安德魯斯.md "wikilink")，並不時花時間環遊世界各地，探訪朋友。另外，郭伯偉熱衷於[高爾夫球](../Page/高爾夫球.md "wikilink")，是[皇家古老高爾夫球會的活躍會員](../Page/皇家古老高爾夫球會.md "wikilink")\[55\]。

作為傳統的殖民地官員，郭伯偉終身拒絕撰寫[回憶錄](../Page/回憶錄.md "wikilink")，即使在晚年亦拒絕訪問，並對其服務政府的經歷三緘其口\[56\]。郭伯偉在2006年1月21日卒於蘇格蘭[鄧迪的九井醫院](../Page/鄧迪.md "wikilink")（Ninewells
Hospital），終年90歲\[57\]。他死後，遺體在鄧迪火葬場火化\[58\]。

## 家庭

郭伯偉在1941年與希拉·瑪麗·湯姆森（Sheila Mary
Thomson，？－2006年2月13日[1](http://announcements.telegraph.co.uk/deaths/01-Feb-2006/28-Feb-2006/Cromwell~Hospital/2/announcement14381.aspx)）於[英國結婚](../Page/英國.md "wikilink")，希拉父親名亞歷山大·湯姆森（Alexander
Thomson），來自[阿伯丁](../Page/阿伯丁.md "wikilink")\[59\]。郭伯偉與希拉兩人在1944年於[塞拉利昂誕下一子](../Page/塞拉利昂.md "wikilink")，名約翰·詹姆士·哈米什（John
James Hamish
Cowperthwaite），哈米什為[香港著名](../Page/香港.md "wikilink")[建築師](../Page/建築師.md "wikilink")，妻子為香港執業[律師區妙寶](../Page/律師.md "wikilink")，惟哈米什後在2004年5月於[菲律賓不幸遭賊匪劫殺](../Page/菲律賓.md "wikilink")，享年59歲\[60\]。

## 評價

郭伯偉在香港貫徹推行「[自由放任](../Page/自由放任.md "wikilink")」的經濟政策，讓他取得極高的評價；有人甚至形容，他好比「全球最成功的企業財務總監──[CEO則是](../Page/CEO.md "wikilink")[英女皇](../Page/英國君主.md "wikilink")」\[61\]。在1971年6月23日，郭伯偉最後一次出席[立法局會議時](../Page/香港立法局.md "wikilink")，時任署理[輔政司對他留下這樣的評價](../Page/輔政司.md "wikilink")：

另外，末代[港督](../Page/港督.md "wikilink")[彭定康](../Page/彭定康.md "wikilink")（後為[勳爵](../Page/勳爵.md "wikilink")）任內亦曾稱讚郭伯偉是香港「這一切的建築師」\[62\]。而在他身後，時任特首[曾蔭權甚至讚揚](../Page/曾蔭權.md "wikilink")「他是創造香港這最自由市場經濟體系傳奇的重要人物」\[63\]。為了紀念他對香港的貢獻，香港《[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")》主席[黎智英於](../Page/黎智英.md "wikilink")2006年4月25日郭伯偉91歲冥壽當日，在[將軍澳的壹傳媒集團大樓大堂豎立了一座郭伯偉](../Page/將軍澳.md "wikilink")[銅像](../Page/銅像.md "wikilink")，以表揚其篤信[市場力量的信念](../Page/市場.md "wikilink")\[64\]。後來，郭伯偉銅像旁邊又加入了[佛利民與](../Page/佛利民.md "wikilink")[海耶克的銅像](../Page/弗里德里希·海耶克.md "wikilink")，標榜他們對[自由的貢獻](../Page/自由.md "wikilink")\[65\]。

[Portrait_of_Milton_Friedman.jpg](https://zh.wikipedia.org/wiki/File:Portrait_of_Milton_Friedman.jpg "fig:Portrait_of_Milton_Friedman.jpg")\]\]
著名美國經濟學家[佛利民與郭伯偉早於](../Page/佛利民.md "wikilink")1963年認識\[66\]，而佛利民很早就對他的經濟方針留下深刻印象。佛利民曾對郭伯偉有這樣的評價：

儘管多年來受到不少讚譽，但郭伯偉一直對此顯得十分謙虛。他曾自言「我只付出很少。我所做的，只在於嘗試防範危害成果的事發生」\[67\]。他又認為，香港經濟之所以取得成功，全靠香港人的努力\[68\]。

## 榮譽

  - [O.B.E.](../Page/OBE.md "wikilink") （1960年）
  - [C.M.G.](../Page/CMG.md "wikilink") （1964年）
  - [K.B.E.](../Page/KBE.md "wikilink") （1968年）

## 相關條目

  - [自由放任](../Page/自由放任.md "wikilink")
  - [積極不干預主義](../Page/積極不干預主義.md "wikilink")
  - [新古典自由主義](../Page/新古典自由主義.md "wikilink")
  - [凱恩斯主義](../Page/凱恩斯主義.md "wikilink")
  - [亞當·史密斯](../Page/亞當·史密斯.md "wikilink")

## 注腳

## 參考資料

### 英文資料

<div class="references-small">

  - *Who's Who*, A & C Black, 1969.
  - *Who's Who*, A & C Black, 2006.
  - "[OFFICIAL REPORT OF
    PROCEEDINGS](http://www.legco.gov.hk/yr70-71/h710623.pdf)", *Hong
    Kong Legislative Council*, 23 June, 1971.
  - Spurr, Russell, *Excellency: the governors of Hong Kong*, Hong Kong:
    FormAsia, 1995.
  - "[British architect murdered in Philippines
    'paradise'](http://www.timesonline.co.uk/article/0,,1-1097992,00.html)",
    *Times Online*, 4 May, 2004.
  - Wignall, Christian, "[THE CHAMPION OF HONG KONG' S
    FREEDOM](http://www.capstanglobal.com/id12.html)", *Hong Kong's
    Freedom*, January, 2005.
  - "[Sir John
    Cowperthwaite](http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2006/01/25/db2501.xml&sSheet=/portal/2006/01/25/ixportal.html)",
    *Telegraph*, 25 January, 2006.
  - Blundell, John, "[Farewell to the man who made Hong
    Kong](https://web.archive.org/web/20070813231650/http://www.iea.org.uk/record.jsp?type=news&ID=314)",
    *institute of economic affairs*, 29 January, 2006.
  - "[Sir John
    Cowperthwaite](http://www.timesonline.co.uk/article/0,,60-2022238_1,00.html)",
    "Obituaries", *Times Online*, 3 February, 2006.
  - "[OBITUARIES](https://web.archive.org/web/20081023155006/http://www.ox.ac.uk/gazette/2006-7/weekly/231106/coll.htm)",
    *Oxford University Gazette*, 23 November, 2006.
  - Singleton, Alex,
    "[Obituary](https://web.archive.org/web/20070702031543/http://www.freetheworld.com/2006/0EFW2006frntXsum.pdf)",
    *Dedication to Sir John Cowperthwaite*, 2006.
  - *[Sir John Cowperthwaite
    (1915-2006)](http://en-cowperthwaite.blogspot.com/)*, The Lion Rock
    Institute, 2007.

</div>

### 中文資料

<div class="references-small">

  - 〈港府財政司郭伯偉就任〉，《[大公報](../Page/大公報.md "wikilink")》第四版，1961年4月21日。
  - 〈夏鼎基任財政司〉，《[大公報](../Page/大公報.md "wikilink")》第四版，1971年6月15日。
  - 〈[行政長官聲明](http://www.info.gov.hk/gia/general/200601/25/P200601250159.htm)〉，《港府新聞公報》，2006年1月25日。
  - 〈郭伯偉蘇格蘭病逝〉，《[信報](../Page/信報.md "wikilink")》P03，2006年1月26日。
  - 楊懷康，〈郭伯偉：香港人的忠誠公僕〉，《[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")》A22，2006年1月26日。
  - [任志剛](../Page/任志剛.md "wikilink")，〈[郭伯偉爵士](https://web.archive.org/web/20070826145507/http://www.info.gov.hk/hkma/chi/viewpt/20060202c.htm)〉，《香港政府》，2006年2月2日。
  - 〈[郭伯偉智慧官場幾人曉？](https://web.archive.org/web/20081023083127/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060426&sec_id=4104&subsec_id=15333&art_id=5865164&cat_id=45&coln_id=20)〉，《蘋果日報》，2006年4月26日。
  - [曾蔭權](../Page/曾蔭權.md "wikilink")，〈[「大市場 小政府」──
    我們恪守的經濟原則](https://web.archive.org/web/20100326080127/http://www.ceo.gov.hk/chi/press/oped.htm)〉，《行政長官新聞稿》，2006年9月18日。
  - 〈[人文風景：政治文化](https://web.archive.org/web/20081026200012/http://www.rthk.org.hk/rthk/radio1/CultureTalk/20061216.html)〉，《[香港電台](../Page/香港電台.md "wikilink")》，2006年12月6日。
  - 〈[肥佬黎為自由飲杯](https://web.archive.org/web/20081023083132/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070109&sec_id=4104&subsec_id=15333&art_id=6698592)〉，《蘋果日報》，2007年1月9日。

</div>

## 外部連結

  - **一般資料**
      - [THE CHAMPION OF HONG KONG' S
        FREEDOM](http://www.capstanglobal.com/id12.html)
      - [香港行政長官聲明](http://www.info.gov.hk/gia/general/200601/25/P200601250159.htm)
      - [郭伯偉爵士紀念博客](http://en-cowperthwaite.blogspot.com/)，[獅子山學會](../Page/獅子山學會.md "wikilink")
      - [泰晤士報訃文](http://www.timesonline.co.uk/article/0,,60-2022238_1,00.html)
      - [每日電訊報訃文](http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2006/01/25/db2501.xml&sSheet=/portal/2006/01/25/ixportal.html)

<!-- end list -->

  - **財政預算案致辭**
      - [1962年2月28日](http://www.legco.gov.hk/1962/h620228.pdf)
      - [1963年2月27日](http://www.legco.gov.hk/1963/h630227.pdf)
      - [1964年2月26日](http://www.legco.gov.hk/1964/h640226.pdf)
      - [1965年2月25日](http://www.legco.gov.hk/1965/h650225.pdf)
      - [1966年2月24日](http://www.legco.gov.hk/1966/h660224.pdf)
      - [1967年3月1日](http://www.legco.gov.hk/1967/h670301.pdf)
      - [1968年2月28日](http://www.legco.gov.hk/1968/h680228.pdf)
      - [1969年2月26日](http://www.legco.gov.hk/1969/h690226.pdf)
      - [1970年2月25日](http://www.legco.gov.hk/yr69-70/h700225.pdf)
      - [1971年2月24日](http://www.legco.gov.hk/yr70-71/h710224.pdf)

[Category:劍橋大學校友](../Category/劍橋大學校友.md "wikilink")
[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:前香港行政局議員](../Category/前香港行政局議員.md "wikilink")
[Category:前香港立法局議員](../Category/前香港立法局議員.md "wikilink")
[Category:怡和](../Category/怡和.md "wikilink")
[Category:KBE勳銜](../Category/KBE勳銜.md "wikilink")

1.  *[Sir John Cowperthwaite
    (1915-2006)](http://en-cowperthwaite.blogspot.com/)*, The Lion Rock
    Institute, 2007.

2.  *Who's Who*, A & C Black, 1969.

3.
4.  "[OBITUARIES](http://www.ox.ac.uk/gazette/2006-7/weekly/231106/coll.htm)
    ", *Oxford University Gazette*, 23 November, 2006.

5.
6.
7.
8.  Wignall, Christian, "[THE CHAMPION OF HONG KONG' S
    FREEDOM](http://www.capstanglobal.com/id12.html)", *Hong Kong's
    Freedom*, January, 2005.

9.
10.
11.
12. 〈港府財政司郭伯偉就任〉，《[大公報](../Page/大公報.md "wikilink")》第四版，1961年4月21日。

13.
14.
15.
16.
17. Spurr, Russell, *Excellency: the governors of Hong Kong*, Hong Kong:
    FormAsia, 1995.

18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35. [任志剛](../Page/任志剛.md "wikilink")，〈[郭伯偉爵士](https://web.archive.org/web/20070826145507/http://www.info.gov.hk/hkma/chi/viewpt/20060202c.htm)〉，《香港政府》，2006年2月2日。

36.
37.
38.
39.
40.
41.
42. 〈夏鼎基任財政司〉，《[大公報](../Page/大公報.md "wikilink")》第四版，1971年6月15日。

43.
44. "[Sir John
    Cowperthwaite](http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2006/01/25/db2501.xml&sSheet=/portal/2006/01/25/ixportal.html)",
    *Telegraph*, 25 January, 2006.

45. Singleton, Alex,
    "[Obituary](http://www.freetheworld.com/2006/0EFW2006frntXsum.pdf)
    ", *Dedication to Sir John Cowperthwaite*, 2006.

46.
47. "[Sir John
    Cowperthwaite](http://www.timesonline.co.uk/article/0,,60-2022238_1,00.html)",
    "Obituaries", *Times Online*, 3 February, 2006.

48.
49. 〈[人文風景：政治文化](https://web.archive.org/web/20081026200012/http://www.rthk.org.hk/rthk/radio1/CultureTalk/20061216.html)〉，《[香港電台](../Page/香港電台.md "wikilink")》，2006年12月6日。

50. [曾蔭權](../Page/曾蔭權.md "wikilink")，〈[「大市場 小政府」──
    我們恪守的經濟原則](https://web.archive.org/web/20100326080127/http://www.ceo.gov.hk/chi/press/oped.htm)〉，《行政長官新聞稿》，2006年9月18日。

51.
52.
53.
54. *Who's Who*, A & C Black, 2006.

55.
56.
57. 〈郭伯偉蘇格蘭病逝〉，《[信報](../Page/信報.md "wikilink")》P03，2006年1月26日。

58. Blundell, John, "[Farewell to the man who made Hong
    Kong](https://web.archive.org/web/20070813231650/http://www.iea.org.uk/record.jsp?type=news&ID=314)",
    *institute of economic affairs*, 29 January, 2006.

59.
60. "[British architect murdered in Philippines
    'paradise'](http://www.timesonline.co.uk/article/0,,1-1097992,00.html)",
    *Times Online*, 4 May, 2004.

61.
62. 楊懷康，〈郭伯偉：香港人的忠誠公僕〉，《[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")》A22，2006年1月26日。

63. 〈[行政長官聲明](http://www.info.gov.hk/gia/general/200601/25/P200601250159.htm)〉，《港府新聞公報》，2006年1月25日。

64. 〈[郭伯偉智慧官場幾人曉？](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060426&sec_id=4104&subsec_id=15333&art_id=5865164&cat_id=45&coln_id=20)
    〉，《[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")》，2006年4月26日。

65. 〈[肥佬黎為自由飲杯](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070109&sec_id=4104&subsec_id=15333&art_id=6698592)
    〉，《[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")》，2007年1月9日。

66.
67.
68.