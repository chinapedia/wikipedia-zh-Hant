<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li><a href="../Page/國立成功大學.md" title="wikilink">國立成功大學材料工程系</a></li>
<li><a href="../Page/國立台灣師範大學附屬高級中學.md" title="wikilink">國立台灣師範大學附屬高級中學</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>陳水扁競選總統指揮中心組織部主任</li>
<li>民主進步黨中央黨部組織部主任</li>
<li>臺灣省議會（第十屆）議員</li>
<li>臺灣省議會農林委員會召集人</li>
<li>民主進步黨創黨黨員</li>
<li>廖永來競選縣長副總幹事</li>
<li>大甲溪生態環境維護協會會長</li>
<li>生命線協談老師</li>
<li>立法院（第五屆）委員<br />
<span style="color: blue;">(2002年-2005年)</span></li>
<li>民主進步黨客家事務部主任</li>
<li>台灣自來水公司董事長</li>
</ul></td>
</tr>
</tbody>
</table>

**郭俊銘**（**Kuo
Chun-ming**，），[台灣政治人物](../Page/台灣.md "wikilink")，郭俊銘首次參選為1994年省議員選舉並於[台中縣當選](../Page/台中縣.md "wikilink")，精省後參與[1998年（第四屆）立委選舉](../Page/1998年中華民國立法委員選舉.md "wikilink")，卻最高票落選，2004年捲土重來並當選[中華民國](../Page/中華民國.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")；[2008年（第七屆）立委選舉敗於同選區](../Page/2008年中華民國立法委員選舉.md "wikilink")[中國國民黨籍候選人](../Page/中國國民黨.md "wikilink")[楊瓊瓔](../Page/楊瓊瓔.md "wikilink")；[2012年（第八屆）立委選舉轉戰台中市第八選區](../Page/2012年中華民國立法委員選舉.md "wikilink")（[豐原](../Page/豐原區.md "wikilink")、[石岡](../Page/石岡區.md "wikilink")、[東勢](../Page/東勢區.md "wikilink")、[新社](../Page/新社區.md "wikilink")、[和平](../Page/和平區_\(臺中市\).md "wikilink")），但仍然敗於前新聞局長[江啟臣](../Page/江啟臣.md "wikilink")。

2016年7月接任[台灣自來水公司董事長](../Page/台灣自來水公司.md "wikilink"),2019年1月23日因被爆料密集和某電台高層楊姓人妻上摩鐵偷情，與蹺班看花博等爭議，發出聲明自認有損公司形象而辭職。

## 經歷

### 2008年立法委員選舉

  - [台中縣第五選區](../Page/台中縣.md "wikilink")（[后里](../Page/后里區.md "wikilink")、[神岡](../Page/神岡區.md "wikilink")、[大雅](../Page/大雅區.md "wikilink")、[潭子](../Page/潭子區.md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><strong><a href="../Page/楊瓊瓔.md" title="wikilink">楊瓊瓔</a></strong></p></td>
<td></td>
<td><p>72,270</p></td>
<td><p>57.69%</p></td>
<td><div align="center">
<p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/郭俊銘.md" title="wikilink">郭俊銘</a></p></td>
<td></td>
<td><p>53,012</p></td>
<td><p>42.31%</p></td>
<td></td>
</tr>
</tbody>
</table>

### 2012年立法委員選舉

  - [台中市第八選區](../Page/台中市.md "wikilink")（[豐原](../Page/豐原區.md "wikilink")、[石岡](../Page/石岡區.md "wikilink")、[東勢](../Page/東勢區.md "wikilink")、[新社](../Page/新社區.md "wikilink")、[和平](../Page/和平區_\(臺中市\).md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/郭俊銘.md" title="wikilink">郭俊銘</a></p></td>
<td></td>
<td><p>60,904</p></td>
<td><p>39.47%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/車淑娟.md" title="wikilink">車淑娟</a></p></td>
<td></td>
<td><p>10,714</p></td>
<td><p>5.67%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/齊蓮生.md" title="wikilink">齊蓮生</a></p></td>
<td></td>
<td><p>770</p></td>
<td><p>0.49%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/陳清龍.md" title="wikilink">陳清龍</a></p></td>
<td></td>
<td><p>14,789</p></td>
<td><p>9.57%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/江啟臣.md" title="wikilink">江啟臣</a></p></td>
<td></td>
<td><p>69,136</p></td>
<td><p>44.77%</p></td>
<td><div align="center">
<p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
</tbody>
</table>

## 相關條目

  - [2008年中華民國立法委員選舉區域暨原住民選舉區投票結果列表](../Page/2008年中華民國立法委員選舉區域暨原住民選舉區投票結果列表.md "wikilink")

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00125&stage=6)

|- |colspan="3"
style="text-align:center;"|**[Taiwan_Water_Corporation_Seal.svg](https://zh.wikipedia.org/wiki/File:Taiwan_Water_Corporation_Seal.svg "fig:Taiwan_Water_Corporation_Seal.svg")[台灣自來水公司](../Page/台灣自來水公司.md "wikilink")**
|-

[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:第10屆臺灣省議員](../Category/第10屆臺灣省議員.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:國立成功大學校友](../Category/國立成功大學校友.md "wikilink")
[Category:國立臺灣師範大學附屬高級中學校友](../Category/國立臺灣師範大學附屬高級中學校友.md "wikilink")
[Category:臺灣自來水公司董事長](../Category/臺灣自來水公司董事長.md "wikilink")
[Jun俊](../Category/郭姓.md "wikilink")