**郭力行**（，），[香港](../Page/香港.md "wikilink")[男歌手](../Page/男歌手.md "wikilink")，現為香港[萊奧娛樂有限公司旗下藝人](../Page/萊奧娛樂有限公司.md "wikilink")。

郭力行入行前曾跟[羅文學習唱歌](../Page/羅文.md "wikilink")\[1\]，但學成後三年才能發片\[2\]。至2007年，郭力行轉變歌唱形象，以搖滾曲風示人\[3\]。

2008年郭力行短暫加入[亞洲電視](../Page/亞洲電視.md "wikilink")2年，並灌錄新專輯\[4\]，但後來該專輯不了了之。

2010年郭力行回歸[無綫電視](../Page/無綫電視.md "wikilink")\[5\]，同時計畫在7月推出EP《郭力行叁》及於年底推出專輯\[6\]，邀得[黃榕客串其音樂錄影帶](../Page/黃榕.md "wikilink")\[7\]，不過最後仍未成事。

2012年郭力行曾項兒童教授武術，次年轉戰內地市場，4月簽約[大唐輝煌傳媒](../Page/大唐輝煌傳媒.md "wikilink")\[8\]，並參與電影《[地下凶猛](../Page/地下凶猛.md "wikilink")》、《[追爱计中计](../Page/追爱计中计.md "wikilink")》、電視劇《[毕有财](../Page/毕有财.md "wikilink")》等內地作品。

2017年郭力行至今參與了多套電影、電視劇等等，男一作品包括《[楊五郎血戰青盤鎮](../Page/楊五郎血戰青盤鎮.md "wikilink")》、《[絕色尾行](../Page/絕色尾行.md "wikilink")》、《[少林寺傳奇](../Page/少林寺傳奇.md "wikilink")》、《[功夫佛山](../Page/功夫佛山.md "wikilink")》、2018人氣網絡電影《[錦衣衛之日炎刀](../Page/錦衣衛之日炎刀.md "wikilink")》等等

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/Assignment_2005.md" title="wikilink">Assignment 2005</a></p></td>
<td style="text-align: left;"><p>迷你專輯</p></td>
<td style="text-align: left;"><p>2005年9月17日</p></td>
<td style="text-align: left;"><p><strong>AVCD</strong></p>
<ol>
<li>Computer Data</li>
<li>鐵人（MV）</li>
<li>鐵人 Behind the Scenes</li>
<li>其實我想講</li>
<li>鐵人</li>
<li>一撻即著</li>
<li>笨過人</li>
<li>我只是你哥</li>
<li>鐵人（劇場版）（Hidden Track）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/Real_Back.md" title="wikilink">Real Back</a></p></td>
<td style="text-align: left;"><p>迷你專輯</p></td>
<td style="text-align: left;"><p>2007年1月29日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>0423</li>
<li>廢鐵人</li>
<li>翹</li>
<li>黑白傘</li>
<li>Hey Girl</li>
<li>黑白傘' MISS</li>
<li>怕醜1/2</li>
</ol></td>
</tr>
</tbody>
</table>

### 單曲

  - 2005年：《少兒武術》
  - 2008年：《狂呼》、《撇我》
  - 2009年：《遇強越強》、《來世今生》（[呂晶晶合唱](../Page/呂晶晶.md "wikilink")）
  - 2010年：《巨人爸爸》、《燥火》
  - 2012年：《心感晴》
  - 2018年：《極品》

## 派台歌曲成績

| **派台歌曲成績**                                               |
| -------------------------------------------------------- |
| 唱片                                                       |
| **2005年**                                                |
| [Assignment 2005](../Page/Assignment_2005.md "wikilink") |
| Assignment 2005                                          |
| Assignment 2005                                          |
| **2006年**                                                |
| [Real Back](../Page/Real_Back.md "wikilink")             |
| **2007年**                                                |
| Real Back                                                |
| Real Back                                                |
| **2008年**                                                |
|                                                          |
|                                                          |
| **2009年**                                                |
|                                                          |
|                                                          |
| **2010年**                                                |
|                                                          |
|                                                          |
| **2018年**                                                |
|                                                          |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

(\*)表示仍在榜上
(X)表示其為亞洲電視合約藝員關係，歌曲被禁止於TVB派台。

## 演出作品

### 電影

  - 2003年：《[炮製女朋友](../Page/炮製女朋友.md "wikilink")》飾 小　汪
  - 2003年：《[賭俠之人定勝天](../Page/賭俠之人定勝天.md "wikilink")》
  - 2003年：《[雙雄](../Page/雙雄.md "wikilink")》飾 志（李文健下屬）
  - 2013年：《[地下凶猛](../Page/地下凶猛.md "wikilink")》
  - 2013年：《[追爱计中计](../Page/追爱计中计.md "wikilink")》
  - 2016年：《僵軍歸來》(網絡電影)
  - 2017年：男一作品《[楊五郎血戰青盤鎮](../Page/楊五郎血戰青盤鎮.md "wikilink")》、《[絕色尾行](../Page/絕色尾行.md "wikilink")》、《[少林寺傳奇](../Page/少林寺傳奇.md "wikilink")》、《[功夫佛山](../Page/功夫佛山.md "wikilink")》
  - 2018年：男一作品《[錦衣衛之日炎刀](../Page/錦衣衛之日炎刀.md "wikilink")》(網絡電影)

### 電視劇

  - 2004年：《[學警雄心](../Page/學警雄心.md "wikilink")》([無綫電視](../Page/無綫電視.md "wikilink"))
    飾 陳國強（Bobby）
  - 2005年：《[上海傳奇](../Page/上海傳奇.md "wikilink")》([無綫電視](../Page/無綫電視.md "wikilink"))
    飾 鄭　發
  - 2006年：《[功夫足球](../Page/功夫足球.md "wikilink")》 飾 毛無語
  - 2008年：《[法網群英](../Page/法網群英.md "wikilink")》([亞洲電視](../Page/亞洲電視.md "wikilink"))
    飾 -{余}-天佑（Lucas）
  - 2013年：《[畢有財](../Page/畢有財.md "wikilink")》(中國大陸) 飾 湯　姆
  - 2014年：《[不可思議的夏天](../Page/不可思議的夏天.md "wikilink") 單元:秘密郵購》(中國網絡劇) 飾 錢震
  - 2018年：《[幸福，我们在路上](../Page/幸福，我们在路上.md "wikilink")》(中國)

### 主持

  - 2005年：《[激優一族](../Page/激優一族.md "wikilink")》([無線電視](../Page/無線電視.md "wikilink"))
  - 2008年：《[新春金鼠全攻略](../Page/新春金鼠全攻略.md "wikilink")》([亞洲電視](../Page/亞洲電視.md "wikilink"))
  - 2008年：《[群星情報站](../Page/群星情報站.md "wikilink")》([亞洲電視](../Page/亞洲電視.md "wikilink"))

## 廣告作品

  - 2003年：[雪碧汽水](../Page/雪碧汽水.md "wikilink") (國內)
  - 2004-2006年：[優之良品](../Page/優之良品.md "wikilink") (香港)

## 曾獲獎項

  - 2005年：TVB8 金曲榜男新人銀獎
  - 2005年：新城勁爆兒歌金曲獎 (少年武術)

## 参考文献

## 外部連結

  -
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Rico](../Category/郭姓.md "wikilink")
[Category:陳瑞祺（喇沙）書院校友](../Category/陳瑞祺（喇沙）書院校友.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.