[Karl_Gutzlaff.jpg](https://zh.wikipedia.org/wiki/File:Karl_Gutzlaff.jpg "fig:Karl_Gutzlaff.jpg")
**郭士立**（），原名**卡爾·弗里德里希·奧古斯特·古特茨拉夫**（****），自称**郭士立**，又被译为**郭实腊**，是[普鲁士来华新教传教士](../Page/普鲁士.md "wikilink")，曾任[香港英治時期的高級官員](../Page/香港英治時期.md "wikilink")[撫華道](../Page/撫華道.md "wikilink")。

## 略歷

郭士立1803年7月8日出生于普鲁士的東部[波美拉尼亞](../Page/波美拉尼亞.md "wikilink")[斯德丁](../Page/斯德丁.md "wikilink")（今波兰[什切青](../Page/什切青.md "wikilink")）一名裁缝家庭。四岁丧母，少年时曾从铜匠为学徒。15岁学习[阿拉伯语和](../Page/阿拉伯语.md "wikilink")[土耳其语](../Page/土耳其语.md "wikilink")。18岁入[柏林神学院学习](../Page/柏林神学院.md "wikilink")，1823年到[鹿特丹入荷兰教会学习](../Page/鹿特丹.md "wikilink")。1826年7月受戒，9月启程前往[巴达维亚](../Page/巴达维亚.md "wikilink")，从[麦都思学汉语和](../Page/麦都思.md "wikilink")[马来语](../Page/马来语.md "wikilink")。1828年到[新加坡](../Page/新加坡.md "wikilink")。1829年脱离荷兰教会，在马六甲主持[伦敦会](../Page/伦敦会.md "wikilink")。同年，和英国女士Mary
Newwell结婚。1830年携妻同往[暹罗](../Page/暹罗.md "wikilink")。

1831年初搭帆船前往中国，6月中登陆，9月抵达[天津](../Page/天津.md "wikilink")，由此北上辽东湾，复南下，于12月抵达[澳门](../Page/澳门.md "wikilink")。他在澳门悬壶济世，入乡随俗，身穿唐装，取汉名为[郭实猎](../Page/郭实猎.md "wikilink")（不是郭实腊），他一面行医，一面传教。\[1\]
1832年2月，他陪同英国东印度公司商业间谍[胡夏米乘坐](../Page/胡夏米.md "wikilink")“[阿美士德号](../Page/阿美士德号.md "wikilink")”商船北上，經[廈門](../Page/廈門.md "wikilink")、[福州](../Page/福州.md "wikilink")、[舟山](../Page/舟山.md "wikilink")、[寧波](../Page/寧波.md "wikilink")、[威海衛等地](../Page/威海衛.md "wikilink")，远达[朝鲜](../Page/朝鲜.md "wikilink")、[日本](../Page/日本.md "wikilink")。9月返回澳门。同年10月，他乘鸦片商[查顿商船](../Page/查顿.md "wikilink")“气精号”（Sylph），再次北上，到达东北[牛庄](../Page/牛庄.md "wikilink")（今[营口](../Page/营口.md "wikilink")）。根据这三次旅行，他写作《[1831-1833年在中国沿海三次航行记](../Page/1831-1833年在中国沿海三次航行记.md "wikilink")》一书。

郭实猎第三次航行实际上是为了帮助查顿贩售鸦片。查顿向中国人销售鸦片，需要一个翻译，而郭是最佳的人选，但查顿也知道说服一个基督教的传教士帮助他销售鸦片是很难的；所以他给郭写了一封热情洋溢的邀请信，应许他很高的回报，为他正要编辑的刊物提供六个月的经费。郭实猎经过激烈的内心斗争之后，终于答应了查顿的邀请。他自己在三次航海记中说：「经过与许多人商量及头脑中的斗争之后，我登上了气精号商船。」但郭作为传教士，是清楚知道自己的做法是错误的。他后来在其主编的《东西洋考每月统记传》中就写过谴责鸦片毒害的文章，如在1835年写道：「鸦片流弊日甚，民穷才匮。无度生之法，会匪串同作弊矣。无恶不作，无弊不行，莫非严哉。既日增月益贻害，莫若作速结戒鸦片焉，尽绝恶弊矣。」此外在1837年二月号上再次刊登\[2\]。郭实猎写的谴责鸦片贸易的文章，是对自己之前帮助查顿售卖鸦片行为的一种否定。

1834年，他陪同英国东印度公司植茶问题研究委员会秘书哥登（G.J.Gordon），深入福建武夷山产茶区，访求栽茶和制茶的专家，并购买茶籽和茶苗。结果聘请雅州的茶师为指导，传习制茶方法，带回许多茶籽栽植于印度[大吉岭](../Page/大吉岭.md "wikilink")\[3\]\[4\]。

1840年[鸦片战争中](../Page/鸦片战争.md "wikilink")，他担任英军司令官的翻译和向导，以及英军占领下的[定海](../Page/定海.md "wikilink")[知县](../Page/知县.md "wikilink")、[宁波](../Page/宁波.md "wikilink")[知府](../Page/知府.md "wikilink")。其後，他參與及起草《[南京條約](../Page/南京條約.md "wikilink")》。他在治理宁波期间，因通中文且能断案而给当地人留下了深刻的印象。当时有人作诗描绘了郭实猎审案的情形：“临高台，郭爷（郭实猎）来，尔有事，僪缕开，枉事为尔超白，难事为尔安排。口通华语，眼识华字，郭爷真奇才。大事一牛，小事一鸡，为尔判断笔如飞。南山可动此案不可移。”\[5\]

1843年在[香港開埠之後](../Page/香港.md "wikilink")，担任首任[香港總督](../Page/香港總督.md "wikilink")[璞鼎查的中文秘書及撫華道](../Page/砵甸乍.md "wikilink")。1844年他在香港成立传教组织“[福汉会](../Page/福汉会.md "wikilink")”，[洪秀全的战友](../Page/洪秀全.md "wikilink")[冯云山曾为该会教徒](../Page/冯云山.md "wikilink")。同时还创立[中国传教会](../Page/中国传教会.md "wikilink")，在伦敦招募来华传教士。中国传教会把[戴德生送到中国](../Page/戴德生.md "wikilink")，后来戴德生成功地建立了[中国内地会](../Page/中国内地会.md "wikilink")。

郭实猎曾参与[马礼逊译本的修订工作](../Page/圣经马礼逊译本.md "wikilink")，与他同工的人有[麦都思](../Page/麦都思.md "wikilink")、[裨治文](../Page/裨治文.md "wikilink")、[马儒翰](../Page/马儒翰.md "wikilink")。在此修订工作中，麦都思、郭士立做主要的工作，裨治文和马儒翰的角色次要。麦都思主要负责新约，而郭实猎负责旧约。后来，郭实猎反复修订了麦都思的新约译本，前后至少有十六次\[6\]。太平天国的版本就是以郭士立翻译的旧约与他修订的新约为蓝本的。

[Gutzlaff.JPG](https://zh.wikipedia.org/wiki/File:Gutzlaff.JPG "fig:Gutzlaff.JPG")
[Gutzlaff_biography.JPG](https://zh.wikipedia.org/wiki/File:Gutzlaff_biography.JPG "fig:Gutzlaff_biography.JPG")郭实猎传\]\]
1851年8月9日郭士立在香港去世。香港[中環](../Page/中環.md "wikilink")[吉士笠街俗稱](../Page/吉士笠街.md "wikilink")「紅毛嬌街」，就是以他命名\[7\]
。

中国[上海外滩的](../Page/上海外滩.md "wikilink")[外滩信号台](../Page/外滩信号台.md "wikilink")（）也是以郭士立命名的。

## 著述

  - 中文

<!-- end list -->

  - 《大英国统志》 1834
  - 《上帝真教传》1834
  - 《耶稣神迹之传》 1836
  - 《福音之谏规》1836
  - 《东西洋考每月统记传》1833-1837，新加坡，广州。
  - 《摩西言行全传》1836 新加坡
  - 《保罗言行录》1837 新加坡
  - 《约翰言行录》1837 新加坡
  - 《彼得罗言行全传》1838 新加坡
  - 《圣书列祖全传》 1838 新加坡
  - 《世人救主》1838 新加坡
  - 《生命无限无疆》 1838 新加坡
  - 《古今万国纲鉴》1838 新加坡
  - 《万国地理全集》1838 新加坡
  - 《犹太国史》1839 新加坡
  - 《贸易通志》1840 新加坡
  - 《耶稣比喻注说》1841 新加坡
  - 《救世耶稣受死全传》1843
  - 《旧遗诏圣书》1855
  - 《救世主言行全传》 1855

<!-- end list -->

  - 日文

<!-- end list -->

  - 《约翰福音之传》
  - 《约翰上中下书》

<!-- end list -->

  - 暹罗文

《路加与约翰福音》

  - 英文

<!-- end list -->

  -
  - *Journal of Three Voyages along the Coast of China in 1831, 1832 and
    1833, with notices of Siam, Corea, and the Loo-Choo Islands* (Desert
    Island Books, Westcliff-on-Sea, 2002)

  - *A Sketch of Chinese History, Ancient and Modern* (London, 1834,
    German version in 1847)

  -
  -
## 参考文献

  - [伟烈亚力](../Page/伟烈亚力.md "wikilink")《1867年以前来华基督教传教士列传及著作目录》广西师范大学出版社

## 外部链接

  - [传教士郭实腊的中国经历](https://web.archive.org/web/20041116075116/http://www.china.org.cn/chinese/HIAW/572968.htm)

  - [Scanned version of the \<*1831-1833年在中國沿海三次航行記*\> at
    Singapore](https://web.archive.org/web/20050115091120/http://www.lib.nus.edu.sg/digital/3voyage.html)


  -
  - [University of Hong Kong library page about book and
    Gutzlaff](http://lib.hku.hk/cd/about/features.html)

{{-}}

[Category:清朝基督教新教人物](../Category/清朝基督教新教人物.md "wikilink")
[Category:在華基督教傳教士](../Category/在華基督教傳教士.md "wikilink")
[Category:香港基督教新教人物](../Category/香港基督教新教人物.md "wikilink")
[Category:汉学家](../Category/汉学家.md "wikilink")
[Category:香港政治人物](../Category/香港政治人物.md "wikilink")
[Category:波美拉尼亞人](../Category/波美拉尼亞人.md "wikilink")
[Category:德国基督教传教士](../Category/德国基督教传教士.md "wikilink")
[Category:在中国逝世的传教士](../Category/在中国逝世的传教士.md "wikilink")

1.  [伟烈亚力](../Page/伟烈亚力.md "wikilink") Memorials of Protestant
    Missionaries （《在华新教传教士纪念录》） 1867
2.  龚缨晏：《浙江早期基督教史》，杭州出版社，2010
3.  [我国制茶技术的发展](http://www.teanet.com.cn/chaye2.htm)
4.  [林立强：西方传教士与十九世纪福州的茶叶贸易](http://cn.qikan.com/Article/zjyj/zjyj200504/zjyj20050410.html)
5.  阿英：《鸦片战争文学集》
6.  尤思德(Jost Oliver Zetzsche)著，蔡錦圖译：《和合本与中文圣经翻译》。香港：国际圣经协会，2002年
7.  [中環吉士笠街以**郭士立**命名](http://historyofhongkong.blogspot.com/2007/03/blog-post_12.html)