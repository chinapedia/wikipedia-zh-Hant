**郭寄嶠**（），原名**光霱**，[安徽](../Page/安徽.md "wikilink")[合肥人](../Page/合肥.md "wikilink")\[1\]，[保定軍校第九期](../Page/保定軍校.md "wikilink")[砲兵科畢業](../Page/砲兵科.md "wikilink")。陆军二级上将军衔。

## 歷任

1929年任[国民革命军第45師參議](../Page/国民革命军第45師.md "wikilink")，1937年11月6日至1940年任[國民革命軍第九軍軍長](../Page/國民革命軍第九軍.md "wikilink")。其間1938年兼任[国民革命军第14集團軍參謀長](../Page/国民革命军第14集團軍.md "wikilink")。1942年至1943年任[重慶衛戍司令部副總司令](../Page/重慶衛戍司令部.md "wikilink")。1944年7月20日任[第一戰區副司令長官](../Page/第一戰區.md "wikilink")，1945年2月11日任[第五戰區副司令長官](../Page/第五戰區.md "wikilink")。1946年1月出任[國民政府軍事委員會委員長西北行營副主任](../Page/國民政府軍事委員會委員長西北行營.md "wikilink")，旋任國防部參謀次長。嗣後至1949年任[甘肅省政府主席兼](../Page/甘肅省政府.md "wikilink")[西北軍政長官公署副長官](../Page/西北軍政長官公署.md "wikilink")。1947年7月19日，國民政府派郭寄嶠為甘肅選舉事務所主任委員；國民政府同時派定其為[國民大會代表](../Page/國民大會代表.md "wikilink")、[立法院](../Page/立法院.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。\[2\]來臺灣後歷任[東南軍政長官公署副長官](../Page/東南軍政長官公署.md "wikilink")、[國防部參謀次長](../Page/國防部參謀次長.md "wikilink")、[國防部長](../Page/國防部長.md "wikilink")、[行政院政務委員](../Page/行政院政務委員.md "wikilink")、[國策顧問等職](../Page/國策顧問.md "wikilink")，為世界郭氏宗親總會的創會理事長\[3\]；中華民國前[行政院長](../Page/行政院院長.md "wikilink")[郝柏村是郭寄嶠的侄女婿](../Page/郝柏村.md "wikilink")，前任[台北市長](../Page/台北市長.md "wikilink")[郝龍斌即其侄外孫](../Page/郝龍斌.md "wikilink")。[郭逸民為其姪子](../Page/郭逸民.md "wikilink")，已由[中國科技大學教務長](../Page/中國科技大學_\(台灣\).md "wikilink")[退休](../Page/退休.md "wikilink")。[東吳大學物理系教授](../Page/東吳大學_\(台灣\).md "wikilink")[郭中一為其姪孫](../Page/郭中一.md "wikilink")，離職後返回合肥開辦[生態農場](../Page/生態農場.md "wikilink")。另一姪孫[郭中人](../Page/郭中人.md "wikilink")，目前任教於[中國文化大學建築系](../Page/中國文化大學.md "wikilink")。

## 戰功

郭寄嶠歷任排長、連長、營長、團長、師長、軍長、總司令，及旅部、師部、軍部、軍團總司令部各級參謀長。郭寄嶠曾參加[北伐](../Page/國民革命軍北伐.md "wikilink")、[剿共諸戰役](../Page/剿共.md "wikilink")，及轉戰河南、河北諸會戰。抗日戰爭爆發於1937年[忻口之役](../Page/忻口之役.md "wikilink")，郭寄嶠殲滅[日軍](../Page/日軍.md "wikilink")[板垣征四郎兵團大部](../Page/板垣征四郎.md "wikilink")，使華北得以從容佈置軍事。1938年至1940年間，郭寄嶠率部扼守[中條山](../Page/中條山.md "wikilink")、[太行山之間](../Page/太行山.md "wikilink")，擊退日軍13次猛攻，均獲致重大戰果。1944年，郭寄嶠參加豫西、陝東諸戰役，阻止日軍西進。1945年秋，新疆匪亂，[迪化危急](../Page/迪化.md "wikilink")，郭寄嶠孤軍馳往，平定亂事。1946年9月，郭寄嶠獲頒[青天白日勳章](../Page/青天白日勳章.md "wikilink")。1948年春，[中國人民解放軍進擾隴東](../Page/中國人民解放軍.md "wikilink")、窺伺[西安](../Page/西安.md "wikilink")；郭寄嶠部與解放軍[彭德懷部激戰](../Page/彭德懷.md "wikilink")，殲彭德懷部5萬人，造成著名之[隴東大捷](../Page/隴東大捷.md "wikilink")。1950年4月，郭寄嶠負責指揮[舟山國軍轉進部隊](../Page/舟山.md "wikilink")15餘萬人來臺灣，並在[中華民國中央政府來臺初期穩定軍政體系](../Page/中華民國中央政府.md "wikilink")，度過當時不安的軍事政局。

## 參考文獻

  - 張憲文、方慶秋、黃美真主編：《中華民國大辭典》，江蘇古籍出版社。
  - [國軍歷史文物館歷任國防部部長傳略](http://museum.mnd.gov.tw/Publish.aspx?cnid=1482&p=12184)

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:中華民國國防部部長](../Category/中華民國國防部部長.md "wikilink")
[Category:中華民國蒙藏委員會委員長](../Category/中華民國蒙藏委員會委員長.md "wikilink")
[Category:东北军将领](../Category/东北军将领.md "wikilink")
[Category:直军将领](../Category/直军将领.md "wikilink")
[Category:中華民國陸軍二級上將](../Category/中華民國陸軍二級上將.md "wikilink")
[Category:台灣戰後安徽移民](../Category/台灣戰後安徽移民.md "wikilink")
[Category:合肥人](../Category/合肥人.md "wikilink")
[J寄](../Category/郭姓.md "wikilink")
[Category:中華民國參謀本部人物](../Category/中華民國參謀本部人物.md "wikilink")

1.
2.
3.