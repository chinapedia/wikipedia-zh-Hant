**郭小川**，原名郭恩大，[河北省](../Page/河北省.md "wikilink")[丰宁县](../Page/丰宁县.md "wikilink")[凤山镇人](../Page/凤山镇_\(豐寧縣\).md "wikilink")，[中国近代著名](../Page/中国.md "wikilink")[诗人](../Page/诗人.md "wikilink")。

郭小川出生于河北省豐寧縣凤山镇，1933年(民國22年)，为逃避入侵[承德的日本侵略军](../Page/承德.md "wikilink")，随父母逃往[北平市](../Page/北平市.md "wikilink")，先后进入蒙藏学校、东北大学工学院学习，并积极参加抗日救亡学生运动。1937年(民國26年)7月7日发生[七七事变](../Page/七七事变.md "wikilink")([盧溝橋事變](../Page/盧溝橋事變.md "wikilink"))，他离家到[山西省参加](../Page/山西省.md "wikilink")[八路军](../Page/八路军.md "wikilink")，于同年11月参加[中国共产党](../Page/中国共产党.md "wikilink")。

他在部队写下大量[诗歌](../Page/诗歌.md "wikilink")，1941年(民國30年)被选送到[陝西省](../Page/陝西省.md "wikilink")[延安市中央研究院学习](../Page/延安市.md "wikilink")，1945年(民國34年)11月，担任[热河省](../Page/热河省.md "wikilink")[丰宁县县长](../Page/丰宁县.md "wikilink")，1946年(民國35年)6月，任热西专署民政科长。1948年(民國37年)后到新闻系统工作，先后担任《群众日报》副总编兼《大众日报》社社长、《天津日报》社编委兼编辑部主任。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，先后在中共中央中南局宣传部、中共中央宣传部等单位工作。并任[中国作家协会书记处书记](../Page/中国作家协会.md "wikilink")、秘书长及《诗刊》编委等职。

郭小川的诗歌立意深刻，他对诗的格式进行过各种尝试，曾创作了楼梯式、自由体、新辞赋体等，具有鲜明的时代光彩，被称为政治抒情诗，朗朗上口，韵味十足。主要作品有《女性的豪歌》、《夏》、《热河曲》、《我们歌唱黄河》、《甘蔗林-青纱帐》、《团泊洼的秋天》、《望星空》、《祝酒歌》等。

在[文化大革命期间](../Page/文化大革命.md "wikilink")，郭小川曾多次受到迫害，并下放到[河南](../Page/河南.md "wikilink")[林县劳动](../Page/林县.md "wikilink")。[四人帮被逮捕后](../Page/四人帮.md "wikilink")，被招回京，在[安阳招待所住宿时](../Page/安阳.md "wikilink")，夜晚由于睡前服用安眠药后又吸烟，烟头掉落在床上，引燃[塑料床垫导致产生大量有毒气体窒息而死](../Page/塑料.md "wikilink")。

## 外部链接

  - [郭小川的诗](http://www.lingshidao.com/xinshi/guoxiaochuan.htm)
  - [郭小川文集](http://www.my285.com/ddmj/gxc/index.htm)

[G](../Category/中国诗人.md "wikilink") [G郭](../Category/承德人.md "wikilink")
[X](../Category/郭姓.md "wikilink")
[Category:中国东北大学校友](../Category/中国东北大学校友.md "wikilink")
[G郭](../Category/中華人民共和國意外身亡者.md "wikilink")