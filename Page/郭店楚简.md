**郭店楚墓竹簡**，簡稱**郭店楚簡**，是指1993年10月於中國湖北省[沙洋縣](../Page/沙洋縣.md "wikilink")[紀山鎮編號為郭店一號的楚國墓室中發現的竹簡](../Page/紀山鎮.md "wikilink")。大概成書於戰國中期，有中國最早的書籍之稱。

## 简介

郭店墓室曾被盗墓者所扰，盗墓者两次挖至椁板并打开边箱取走部份文物，幸数量不多，简基本无损。\[1\]

郭店楚简共804枚，其中有字竹简726枚，共13000餘字，全部为[先秦时期的](../Page/先秦.md "wikilink")[儒家和](../Page/儒家.md "wikilink")[道家典籍](../Page/道家.md "wikilink")，共18篇。儒家典籍有《缁衣》、《鲁穆公问子思》、《穷达以时》、《[五行](../Page/五行.md "wikilink")》、《唐虞之道》、《忠信之道》、《成之闻之》、《尊德义》、《性自命出》、《六德》、《[语丛](../Page/语丛.md "wikilink")》（四篇）；道家著作有《[老子](../Page/道德經.md "wikilink")》（甲、乙、丙三篇）和《[太一生水](../Page/太一生水.md "wikilink")》。学者认为，“郭店简《缁衣》、《五行》出自[子思](../Page/子思.md "wikilink")，其它《[性自命出](../Page/性自命出.md "wikilink")》等也与曾子、子思一系有关系，甚至不妨视为《子思子》。”
\[2\]

郭店楚简所载内容与传世之儒道经典颇有不同，比如传世的《[道德经](../Page/道德经.md "wikilink")》第十九章为：“绝圣弃智，民利百倍；绝仁弃义，民复孝慈；绝巧弃利，盗贼无有。”简文《老子》甲的开头却是：“绝智弃辩，民利百倍；绝巧弃利，盗贼亡有；绝为弃作，民复孝慈。”

[秦始皇在](../Page/秦始皇.md "wikilink")[焚书坑儒中将大量先秦书籍焚毁](../Page/焚书坑儒.md "wikilink")，而郭店楚简则在地下得以保存，为研究先秦古文和思想提供珍贵的历史资料，對於古中國文獻之研究有甚大意義。

## 篇目

[TaiYiShengShui1.jpg](https://zh.wikipedia.org/wiki/File:TaiYiShengShui1.jpg "fig:TaiYiShengShui1.jpg")\]\]

<table class = "wikitable" height="100%" style="border-collapse: collapse" bordercolor="#111111" cellpadding="0" cellspacing="0" >

<tr>

<td height="100%">

01-03

</td>

<td align="center" height="100%">

老子甲、乙、丙

</td>

<tr>

<td height="100%">

04

</td>

<td align="center" height="100%">

太一生水

</td>

<tr>

<td height="100%">

05

</td>

<td align="center" height="100%">

緇衣

</td>

<tr>

<td height="100%">

06

</td>

<td align="center" height="100%">

魯穆公問子思

</td>

<tr>

<td height="100%">

07

</td>

<td align="center" height="100%">

窮達以時

</td>

<tr>

<td height="100%">

08

</td>

<td align="center" height="100%">

五行

</td>

<tr>

<td height="100%">

09

</td>

<td align="center" height="100%">

唐虞之道

</td>

<tr>

<td height="100%">

10

</td>

<td align="center" height="100%">

忠信之道

</td>

<tr>

<td height="100%">

11

</td>

<td align="center" height="100%">

成之聞之

</td>

<tr>

<td height="100%">

12

</td>

<td align="center" height="100%">

尊德義

</td>

<tr>

<td height="100%">

13

</td>

<td align="center" height="100%">

性自命出

</td>

<tr>

<td height="100%">

14

</td>

<td align="center" height="100%">

六德

</td>

<tr>

<td height="100%">

15-18

</td>

<td align="center" height="100%">

語叢一、二、三、四

</td>

</tr>

</table>

## 参考

  -
  -
  -
## 研究書目

  - Scott Cook（顧史考）：《郭店楚簡先秦儒書宏微觀》（上海：上海古籍出版社，2012）。
  - 邢文編譯：《郭店老子與太一生水》（北京：學苑出版社，2005）。
  - Sarah Allan & Crispin Williams編，邢文編譯：《郭店老子：東西方學者的對話》（北京：學苑出版社，2002）。
  - 柯馬丁：〈[引據與中國古代寫本文獻中的儒家經典《緇衣》研究](http://www.princeton.edu/~mkern/Quotation%20Chinese.pdf)〉。
  - 李學勤：〈[太一生水的數術解釋](http://www.daoist.org/BookSearch/BookSearch/list009/0486.pdf)〉。
  - 賀碧來：〈[論《太一生水》](http://www.daoist.org/BookSearch/BookSearch/list009/0490.pdf)〉。
  - 戴卡琳：〈[《太一生水》初探](http://www.daoist.org/BookSearch/BookSearch/list009/0491.pdf)〉。
  - 曹峰：〈[《太一生水》下半部分是一个独立完整的篇章](http://www.nssd.org/articles/article_read.aspx?id=48995013)〉。

## 參見

  - [出土文獻](../Page/出土文獻.md "wikilink")
  - [汲塚書](../Page/汲塚書.md "wikilink")

## 外部链接

  - [世界上最早的书—郭店楚简](http://enjoy.eastday.com/epublish/gb/paper451/2/class045100001/hwz1307566.htm)
  - [郭店楚簡資料庫](http://bamboo.lib.cuhk.edu.hk/)
  - [郭店楚简释文注释](https://web.archive.org/web/20140222155119/http://www.jianbo.org/guodian.htm)

[Category:沙洋县](../Category/沙洋县.md "wikilink")
[Category:荆门文物](../Category/荆门文物.md "wikilink")
[Category:楚简](../Category/楚简.md "wikilink")

1.  湖北省荆门市博物馆：《荆门郭店一号楚墓》，《文物》1997年第7期。
2.  [李学勤：孔孟之间与老庄之间](http://wen.org.cn/modules/article/view.article.php?2908/c18)