**郭庚茂**（
），[河北](../Page/河北.md "wikilink")[冀州人](../Page/冀州.md "wikilink")。1975年11月参加工作，1972年3月加入中国共产党。[北京大学国际政治系政治学专业专科毕业](../Page/北京大学.md "wikilink")，中央党校研究生学历。中共第十六届中央候补委员，第十七届、十八届中央委员。曾任[河北省人民政府省长](../Page/河北省人民政府.md "wikilink")，[河南省人民政府省长](../Page/河南省人民政府.md "wikilink")，[中共河南省委书记](../Page/中共河南省委.md "wikilink")。现任[第十二届全国人民代表大会农业与农村委员会副主任委员](../Page/第十二届全国人民代表大会常务委员会.md "wikilink")。

## 生平

郭庚茂早年就在家乡的[中共基层党组织工作](../Page/中共.md "wikilink")；曾历任中共冀县城关公社党委副书记，柏芽公社党委副书记、革委会代主任、主任、柏芽公社党委书记等职。1982年，他进入[北京大学国际政治系专修科学习](../Page/北京大学.md "wikilink")。1984年毕业后，又回到[河北](../Page/河北.md "wikilink")，继续在基层任职；历任[衡水市](../Page/衡水市.md "wikilink")[枣强县县长](../Page/枣强县.md "wikilink")、县委书记；1991年升任[邢台地委副书记](../Page/邢台.md "wikilink")，行署常务副专员。在1993年邢台由“[地区](../Page/地区.md "wikilink")”升格为“[地級市](../Page/地級市.md "wikilink")”后，郭庚茂又出任[中共邢台市委常委](../Page/中共邢台市委.md "wikilink")、副市长；一年后，出任[邢台市市长](../Page/邢台市.md "wikilink")。

1998年，郭庚茂由邢台市市长的位置上，直接升任[河北省副省长](../Page/河北省.md "wikilink")；两年后，他进入[中共河北省委常委领导班子](../Page/中共河北省委.md "wikilink")；2006年10月升任[中共河北省委副书记](../Page/中共河北省委.md "wikilink")，并接替[季允石出任](../Page/季允石.md "wikilink")[河北省省长](../Page/河北省.md "wikilink")。

2008年3月，任[中共河南省委副书记](../Page/中共河南省委.md "wikilink")，并提名出任河南省省长。2008年4月河南省人大常委會任命郭庚茂為副省長、代省長。2009年1月17日当選為河南省省長。

2013年3月20日，升任[中共河南省委委书记](../Page/中共河南省委.md "wikilink")。2013年4月当选河南省人大主任\[1\]。

2016年3月26日，由于年龄原因不再担任[中共河南省委书记](../Page/中共河南省委.md "wikilink")、常委、委员职务\[2\]\[3\]。

2016年4月，任[第十二届全国人民代表大会农业与农村委员会副主任委员](../Page/第十二届全国人民代表大会常务委员会.md "wikilink")\[4\]。2018年1月，当选[第十三届全国政协委员](../Page/中国人民政治协商会议第十三届全国委员会委员名单.md "wikilink")\[5\]。

## 参考文献

  - [郭庚茂简历](https://web.archive.org/web/20081226091734/http://news.xinhuanet.com/ziliao/2007-02/13/content_5734905.htm)
    新华网
  - [郭庚茂:把焦裕禄精神作为照镜子正衣冠的生动教材](http://district.ce.cn/newarea/roll/201307/02/t20130702_24531967.shtml)
    中国经济网 2014-05-09

{{-}}

[Category:河南省人大常委会主任](../Category/河南省人大常委会主任.md "wikilink")
[Category:中華人民共和國河北省副省長](../Category/中華人民共和國河北省副省長.md "wikilink")
[Category:中华人民共和国邢台市市长](../Category/中华人民共和国邢台市市长.md "wikilink")
[Category:中華人民共和國棗強縣縣長](../Category/中華人民共和國棗強縣縣長.md "wikilink")
[Category:中国共产党第十六届中央委员会候补委员](../Category/中国共产党第十六届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中国共产党第十八届中央委员会委员](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:中共河南省委书记](../Category/中共河南省委书记.md "wikilink")
[Category:中共河北省委副書記](../Category/中共河北省委副書記.md "wikilink")
[Category:中共河北省委常委](../Category/中共河北省委常委.md "wikilink")
[Category:中共棗強縣委書記](../Category/中共棗強縣委書記.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:冀州人](../Category/冀州人.md "wikilink")
[Geng庚](../Category/郭姓.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")

1.
2.
3.
4.
5.