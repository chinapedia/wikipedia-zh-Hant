**郭廷以**（），字**量宇**，出生於河南[舞陽](../Page/舞陽縣.md "wikilink")，中国[歷史學家](../Page/歷史學家.md "wikilink")，1949年前往台湾。他是[中國近代史學及中國](../Page/中國近代史.md "wikilink")[口述史學的開拓者之一](../Page/口述歷史.md "wikilink")，也是[中央研究院近代史研究所的共同創建者](../Page/中央研究院近代史研究所.md "wikilink")，並曾獲選為[中央研究院院士](../Page/中央研究院院士.md "wikilink")。\[1\]

## 生平

郭廷以于1904年1月12日出生於河南省[舞陽縣姜元店](../Page/舞陽縣.md "wikilink")。

於1926年畢業於[國立東南大學](../Page/國立東南大學.md "wikilink")[歷史系並獲頒](../Page/歷史系.md "wikilink")[文學士學位](../Page/文學士.md "wikilink")；他在該校曾受業於[柳詒-{徵}-](../Page/柳詒徵.md "wikilink")、[徐養秋](../Page/徐養秋.md "wikilink")、[羅家倫等](../Page/羅家倫.md "wikilink")[學者](../Page/學者.md "wikilink")。

此後，曾任教於[清華大學](../Page/清華大學.md "wikilink")、[河南大學](../Page/河南大學.md "wikilink")、[政治大學](../Page/政治大學.md "wikilink")，復於[中央大學歷史系執教兼系主任職務](../Page/國立中央大學.md "wikilink")。

在1949年前往[臺灣](../Page/臺灣.md "wikilink")，又在1955年應[中央研究院院長](../Page/中央研究院.md "wikilink")[朱家驊邀請開始籌設](../Page/朱家驊.md "wikilink")[中央研究院近代史研究所](../Page/中央研究院近代史研究所.md "wikilink")。

1965年，中央研究院近代史研究所成立，他隨後被聘任為該所所長；不久，因與[費正清等人往來而](../Page/費正清.md "wikilink")[政治風波](../Page/政治.md "wikilink")。

1968年，當選[中央研究院院士](../Page/中央研究院院士.md "wikilink")。

1969年，應[夏威夷大學東西中心邀請前往講學](../Page/夏威夷大學.md "wikilink")。\[2\]

1971年，離去中央研究院近代史研究所所長一職及該所；此後，長期滯留[美國](../Page/美國.md "wikilink")，並在艱困處境下努力研究撰述，完成《近代中國史綱》等大篇幅著作。\[3\]

1975年，因病逝世於[紐約聖路加醫院](../Page/紐約.md "wikilink")。\[4\]

## 著作

  - 《郭廷以先生訪問紀錄》
  - 《近代中國史》
  - 《[近代中國史綱](../Page/近代中國史綱.md "wikilink")》
  - 《近代中國史事日誌》（清季）
  - 《近代中國的變局》
  - 《[中華民國史事日誌](../Page/中華民國史事日誌.md "wikilink")》
  - 《太平天國史事日誌》
  - 《太平天國曆法考訂》
  - 《臺灣史事概說》\[5\]

## 相關作品

  - 《郭廷以先生門生故舊憶往錄》\[6\]

## 家人

知名華裔搖滾樂手 / 媒體人[郭怡廣](../Page/郭怡廣.md "wikilink") 是郭廷以先生的孫子\[7\]

## 參見

  - [蔣廷黻](../Page/蔣廷黻.md "wikilink")
  - [南港學派](../Page/南港學派.md "wikilink")

### 主要門生

  - [張玉法](../Page/張玉法.md "wikilink")
  - [唐德剛](../Page/唐德剛.md "wikilink")
  - 李守孔
  - 王爾敏
  - 劉鳳翰
  - 李恩涵
  - [李國祁](../Page/李國祁.md "wikilink")
  - 張朋園
  - 陳三井
  - 林明德\[8\]

## 参考文献

## 外部链接

  - [郭廷以 -
    師大維基](http://history.lib.ntnu.edu.tw/wiki/index.php/%E9%83%AD%E5%BB%B7%E4%BB%A5)

{{-}}

[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")
[Category:中央研究院近代史研究所所長](../Category/中央研究院近代史研究所所長.md "wikilink")
[Category:中華民國歷史學家](../Category/中華民國歷史學家.md "wikilink")
[文G](../Category/國立中央大學教授.md "wikilink")
[Category:國立清華大學教授](../Category/國立清華大學教授.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[Category:南京大學教授](../Category/南京大學教授.md "wikilink")
[Category:河南大學教授](../Category/河南大學教授.md "wikilink")
[文](../Category/國立中央大学校友.md "wikilink")
[文](../Category/中央大学校友.md "wikilink")
[Category:南京大學校友](../Category/南京大學校友.md "wikilink")
[Category:移民美國的中華民國人](../Category/移民美國的中華民國人.md "wikilink")
[Category:台灣戰後河南移民](../Category/台灣戰後河南移民.md "wikilink")
[Category:舞阳人](../Category/舞阳人.md "wikilink")
[T廷](../Category/郭姓.md "wikilink")
[Category:河南裔台灣人](../Category/河南裔台灣人.md "wikilink")

1.

2.

3.

4.
5.
6.

7.  Sinica Podcast
    2014/8/13"[1](https://soundcloud.com/chinafile/sinica-podcast-in-memory-of-jenkai-kuo)"

8.