**郭振乾**（），[河南省](../Page/河南省.md "wikilink")[洛宁人](../Page/洛宁.md "wikilink")；[中国人民大学贸易系毕业](../Page/中国人民大学.md "wikilink")，高级经济师。1949年加入[中国共产党](../Page/中国共产党.md "wikilink")，是[中共第十三届](../Page/中国共产党第十三届中央委员会委员列表.md "wikilink")、[十四届中央委员会委员](../Page/中国共产党第十四届中央委员会委员列表.md "wikilink")。曾任[湖北省省长](../Page/湖北省.md "wikilink")（1986年1月－1990年3月）、[中国人民银行副行长](../Page/中国人民银行.md "wikilink")、[中华人民共和国审计署审计长](../Page/中华人民共和国审计署.md "wikilink")（1994年5月—1998年3月），是第九届[全国人大常委会委员](../Page/全国人大常委会.md "wikilink")、财政经济委员会副主任委员。\[1\]

## 介绍

郭振乾是公安干警出身；曾在[河南](../Page/河南.md "wikilink")[嵩县公安局工作](../Page/嵩县.md "wikilink")。后来，进入[中国人民大学学习](../Page/中国人民大学.md "wikilink")，还曾担任学校[中共学生党支部书记](../Page/中共.md "wikilink")。1954年毕业后，郭振乾被分配到[湖北省商业厅工作](../Page/湖北省.md "wikilink")。此后，他历任商业厅教育科副科长，[湖北省商业学校副校长](../Page/湖北省.md "wikilink")、[中共](../Page/中共.md "wikilink")[湖北省委财贸办公室政治处副主任](../Page/湖北.md "wikilink")、主任，[湖北省财办副主任兼](../Page/湖北省.md "wikilink")[中国人民建设银行](../Page/中国人民建设银行.md "wikilink")[湖北省分行行长等职](../Page/湖北省.md "wikilink")。
1983年，郭振乾出任[湖北省副省长](../Page/湖北省.md "wikilink")；1985年任代省长；次年正式当选[湖北省](../Page/湖北省.md "wikilink")[省长](../Page/省长.md "wikilink")。前武汉大学校长刘道玉的《一个大学校长自白》之““官贵民贱”的真相”一文中提到的利用职权霸道占用本属于他人的火车卧铺的湖北省省长就是郭振乾\[2\]。

1990年4月，郭振乾上调中央，出任[中国人民银行副行长](../Page/中国人民银行.md "wikilink")、党组副书记。1993年8月转任审计署，任副审计长。1994年5月，在[李鹏第二任](../Page/李鹏.md "wikilink")“内阁”中，担任审计署审计长。1998年3月，届满离职；但同时，他又当选为第九届[全国人大财政经济委员会副主任委员](../Page/全国人大.md "wikilink")。

## 注释

[Category:中华人民共和国审计署审计长](../Category/中华人民共和国审计署审计长.md "wikilink")
[Category:中华人民共和国湖北省省长](../Category/中华人民共和国湖北省省长.md "wikilink")
[Category:中国共产党党员
(1949年入党)](../Category/中国共产党党员_\(1949年入党\).md "wikilink")
[Category:中国共产党第十三届中央委员会委员](../Category/中国共产党第十三届中央委员会委员.md "wikilink")
[Category:中国共产党第十四届中央委员会委员](../Category/中国共产党第十四届中央委员会委员.md "wikilink")
[Category:中国人民大学校友](../Category/中国人民大学校友.md "wikilink")
[Category:洛宁人](../Category/洛宁人.md "wikilink")
[Z振](../Category/郭姓.md "wikilink")
[Category:中国人民银行副行长](../Category/中国人民银行副行长.md "wikilink")
[Category:第九届全国人大常委会委员](../Category/第九届全国人大常委会委员.md "wikilink")

1.
2.  详细参看《一个大学校长自白》一书