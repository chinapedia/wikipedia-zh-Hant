[Gzq_lxy.jpg](https://zh.wikipedia.org/wiki/File:Gzq_lxy.jpg "fig:Gzq_lxy.jpg")
**郭振清**（），[中国著名](../Page/中国.md "wikilink")[电影演员](../Page/电影演员.md "wikilink")。毕业于华北职工干部学校，先后为天津总工会文工团演员，天津艺术剧院演员，长春电影制片厂演员。20世纪五十年代初期，因主演《平原游击队》蜚声影坛。

## 生平

郭振清生于[天津一个](../Page/天津.md "wikilink")[工人](../Page/工人.md "wikilink")[家庭](../Page/家庭.md "wikilink")。早年在私塾中读书，16岁时开始在电车公司当售票员，后担任电车公司的文教委员。此后又在华北职工干部学校学习，任[天津总工会文工团演员](../Page/天津总工会.md "wikilink")、[天津艺术剧院演员](../Page/天津艺术剧院.md "wikilink")。

1952年在[长春电影制片厂的](../Page/长春电影制片厂.md "wikilink")[影片](../Page/电影.md "wikilink")《[六号门](../Page/六号门.md "wikilink")》担纲主角胡二，这是郭振清出演的首个电影角色。1955年，在长春电影制片厂的《[平原游击队](../Page/平原游击队.md "wikilink")》中，扮演[游击队队长](../Page/游击队.md "wikilink")[李向阳](../Page/李向阳.md "wikilink")，一举成名。1957年荣获全国最受观众欢迎的演员荣誉称号。[郭沫若曾题词一首赠郭振清](../Page/郭沫若.md "wikilink")。词曰：“凤头鸠见桑葚，独立枝头有所思。自我陶醉不可耽，高飞四海颂和平，月桂菊可寻。”

此外，郭振清的[相声表演也广受欢迎](../Page/相声.md "wikilink")，他曾和[朱相臣合作表演](../Page/朱相臣.md "wikilink")《卖估衣》。1981年年郭振清回到天津，任天津市广播局演员。1988年离休，享受区、局级政治、生活待遇。1993年享受国务院颁发的专家政府特殊津贴。

## 主要电影作品

  - 《[六号门](../Page/六号门.md "wikilink")》 1952年
  - 《[英雄司机](../Page/英雄司机.md "wikilink")》 1954年
  - 《[平原游击队](../Page/平原游击队.md "wikilink")》 1955年
  - 《[暴风雨中的雄鹰](../Page/暴风雨中的雄鹰.md "wikilink")》 1957年
  - 《[花好月圆](../Page/花好月圆.md "wikilink")》 1958年
  - 《[换了人间](../Page/换了人间.md "wikilink")》 1959年
  - 《[羌笛颂](../Page/羌笛颂.md "wikilink")》 1960年
  - 《[我们这一代人](../Page/我们这一代人.md "wikilink")》 1960年
  - 《[独立大队](../Page/独立大队.md "wikilink")》 1964年
  - 《[英雄儿女](../Page/英雄儿女.md "wikilink")》 1964年
  - 《[钢铁巨人](../Page/钢铁巨人.md "wikilink")》 1974年
  - 《[严峻的历程](../Page/严峻的历程.md "wikilink")》 1978年
  - 《[游击战役](../Page/游击战役.md "wikilink")》
  - 《[排球之花](../Page/排球之花.md "wikilink")》 1980年
  - 《[荒娃](../Page/荒娃.md "wikilink")》 1989年
  - 《[艳阳天](../Page/艳阳天.md "wikilink")》 1973年
  - 《[决裂](../Page/决裂.md "wikilink")》 1975年
  - 《[魂荡东洋](../Page/魂荡东洋.md "wikilink")》 1987年
  - 《[四等小站](../Page/四等小站.md "wikilink")》 1984年
  - 《[济南战役](../Page/济南战役.md "wikilink")》 1979年
  - 《[山村新人](../Page/山村新人.md "wikilink")》 1976年
  - 《[万木春](../Page/万木春.md "wikilink")》 1961年
  - 《遗失的伴侣》 1988年

## 参考文献

## 外部链接

  -
  -
[G](../Category/中国电影男演员.md "wikilink") [Z](../Category/郭姓.md "wikilink")