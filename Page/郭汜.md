[Li_Jue_and_Guo_Si_sack_the_capital_at_Chang%27an.jpg](https://zh.wikipedia.org/wiki/File:Li_Jue_and_Guo_Si_sack_the_capital_at_Chang%27an.jpg "fig:Li_Jue_and_Guo_Si_sack_the_capital_at_Chang%27an.jpg")郭汜攻占首都\]\]
**郭汜**（）（「汜」音「寺」，一说“汜”是“氾”[通假字](../Page/通假字.md "wikilink")，正确读音为“fàn”，但“汜”与“氾”的通假关系是否确实，史上已被学者广泛考据过，比如顾颉刚曾就“汜水”古今位置的考证。\[1\]），幼名**阿多**，故稱**郭多**，[東漢末期](../Page/東漢.md "wikilink")[涼州](../Page/涼州.md "wikilink")[張掖人](../Page/張掖郡.md "wikilink")，原為盜馬賊\[2\]，後成為董卓女婿[牛輔部下](../Page/牛輔.md "wikilink")。

## 生平

### 牛輔部下

[初平三年](../Page/初平.md "wikilink")（192年），郭汜被[牛輔派往與](../Page/牛輔.md "wikilink")[李傕及](../Page/李傕.md "wikilink")[張濟領數萬兵討伐](../Page/张济_\(武威\).md "wikilink")[朱雋於](../Page/朱雋.md "wikilink")[中牟](../Page/中牟.md "wikilink")，戰勝，大掠[陳留](../Page/陳留郡.md "wikilink")、[潁川諸](../Page/潁川郡.md "wikilink")[郡](../Page/郡.md "wikilink")。

### 共掌長安

在[董卓與牛輔被殺之後](../Page/董卓.md "wikilink")，郭汜與[李傕等人乞求](../Page/李傕.md "wikilink")[王允](../Page/王允.md "wikilink")[赦免](../Page/赦免.md "wikilink")，被拒絕。眾人聽從[賈詡之見](../Page/賈詡.md "wikilink")，率軍攻入[長安](../Page/長安.md "wikilink")，殺王允，迫走[呂布](../Page/呂布.md "wikilink")，與李傕等人掌管朝政，被封為[揚烈將軍](../Page/揚烈將軍.md "wikilink")。[興平元年](../Page/兴平_\(东汉\).md "wikilink")（194年），[馬騰軍攻來](../Page/馬騰.md "wikilink")，被李傕派往戰於[長平觀下](../Page/長平.md "wikilink")，勝利，殺萬餘人。同年又破[馮翊等](../Page/馮翊.md "wikilink")[羌賊](../Page/羌.md "wikilink")。升為[後將軍](../Page/後將軍.md "wikilink")，聯同李傕、[樊稠綬與官員提拔權利](../Page/樊稠.md "wikilink")，與[三公同級](../Page/三公.md "wikilink")，合稱為六府。

### 互鬥連年

興平二年（195年），樊稠被李傕殺害，郭汜懷疑李傕欲毒害自己而與李傕互相攻打。[汉献帝遣使議和二人](../Page/汉献帝.md "wikilink")，遭拒。郭汜本欲脅持天子，但為李傕先發制人。汉献帝再遣使議和二人，被郭汜脅持作[使節的](../Page/使節.md "wikilink")[公卿](../Page/公卿.md "wikilink")。郭汜聯合李傕軍中[张苞攻營](../Page/張苞_\(李傕部將\).md "wikilink")，為[楊奉所救](../Page/楊奉.md "wikilink")。汉献帝再遣使[皇甫酈議和二人](../Page/皇甫酈.md "wikilink")，郭汜答允而李傕拒絕，皇甫酈因而大罵李傕。後楊奉領兵叛逃李傕，令其勢力大減，张济來到，和解二人，李傕便答允。

### 失去天子

張濟提議[天子東](../Page/天子.md "wikilink")[遷都](../Page/遷都.md "wikilink")，汉献帝亦欲回[洛城](../Page/洛城.md "wikilink")，升郭汜為[車騎將軍](../Page/車騎將軍.md "wikilink")，令眾人護送。郭汜本想脅持汉献帝回[郿](../Page/郿.md "wikilink")，為[楊定](../Page/楊定_\(東漢\).md "wikilink")、楊奉、[董承所知悉](../Page/董承.md "wikilink")，郭汜放棄此打算。後合李傕、張濟之力欲劫回汉献帝，大敗楊定等，但汉献帝成功逃出，為[張楊迎接](../Page/張楊_\(東漢\).md "wikilink")。

[建安二年](../Page/建安_\(东汉\).md "wikilink")（197年），郭汜被其部屬[伍習誅殺於郿](../Page/伍習.md "wikilink")，余部被李傕兼并。

## 轶闻

西晋良吏[鲁芝的父亲被郭汜杀害](../Page/鲁芝.md "wikilink")。\[3\]

## 艺术形象

### 影视形象

  - 台灣[華視電視劇](../Page/華視.md "wikilink")《[三國英雄傳之關公](../Page/三國英雄傳之關公.md "wikilink")》（1996年）：[李艺民](../Page/李艺民.md "wikilink")
  - 中國電視劇《[三国](../Page/三国_\(电视剧\).md "wikilink")》（2010年）：[龚志玺](../Page/龚志玺.md "wikilink")
  - 电视剧《[曹操](../Page/曹操_\(电视剧\).md "wikilink")》（2014年）：[王瞳](../Page/王瞳.md "wikilink")

## 评价

  - [刘艾](../Page/刘艾.md "wikilink")：“坚用兵不如李傕、郭汜。”“坚虽时见计，故自不如李傕、郭汜。”（《山阳公载记》《后汉纪》）
  - [朱儁](../Page/朱儁.md "wikilink")：“傕、汜小竖，樊稠庸儿，无他远略，又埶力相敌，变难必作。”（《后汉书·董卓列传第六十二》）
  - [李傕](../Page/李傕.md "wikilink")：“郭多，盗马虏耳。”（《后汉书·董卓列传第六十二》）
  - [沮俊](../Page/沮俊.md "wikilink")：“汝等凶逆，逼迫天子，乱臣贼子，未有如汝者！”（《后汉书·董卓列传第六十二》）
  - [董承](../Page/董承.md "wikilink")：“郭多有数百兵，坏李傕数万人。”（《献帝起居注》）
  - [毛宗岗](../Page/毛宗岗.md "wikilink")：“杨奉、贾诩，其于李傕，亦始合而终离。乃一离而不复合，是则能补过者也。若郭阿多反复无常，与二人正自霄壤。”（汇评三国志演义）

## 註釋

## 参考文献

  - 《[后汉书](../Page/后汉书.md "wikilink")·[卷七十二·董卓列傳第六十二](../Page/s:後漢書/卷72.md "wikilink")》
  - 《[三国志](../Page/三国志.md "wikilink")·[魏書六·董二袁劉傳](../Page/s:三國志/卷06.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀五十二](../Page/s:資治通鑑/卷060.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀五十三](../Page/s:資治通鑑/卷061.md "wikilink")》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")·[漢紀五十四](../Page/s:資治通鑑/卷062.md "wikilink")》

[G郭](../Category/東漢軍事人物.md "wikilink")
[G郭](../Category/三國軍事人物.md "wikilink")
[G郭](../Category/三國被處決者.md "wikilink")
[S汜](../Category/郭姓.md "wikilink")

1.  学者吴小如在讲座《治文学宜略通小学》中说到：对李傕和郭氾的读音“注：新华字典对此音有误，用在名字上，应读què。”，而另外一人郭氾的氾读fàn，即泛。
    2013年6月《百家讲坛》中复旦大学历史学系讲师姜鹏在讲《一个傀儡的力量·汉献帝》时也采用“fàn”的读音
2.  范书《董卓列传》記載李傕說郭汜乃“盗马虏耳”。
3.  《晋书·良吏传》：魯芝字世英，扶風郿人也。世有名德，為西州豪族。**父為郭氾所害**，芝襁褓流離，年十七，乃移居雍，耽思墳籍。