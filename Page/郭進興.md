**郭進興**[台灣](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手](../Page/選手.md "wikilink")。

綽號阿水（因為小時候喜歡玩水），三信家商畢業，打過日本社會人球隊。1993為中華職棒年度救援王，1995年11月23日在福岡巨蛋舉行的亞洲太平洋超級棒球賽，首戰大榮獲得勝投，並協助球隊拿下該年的總冠軍。

1996年改名為郭尚豪，連二年（1995年、1996年）奪下20勝的勝投王，是中華職棒唯一得過勝投王也得過救援王的選手。1997年涉及「打放水球」職棒賭博“黑鷹事件”案，2月21日遭中華職棒聯盟宣布“永不錄用”，1999年又改回原名。

女兒是[台語歌手](../Page/台語.md "wikilink")[郭婷筠](../Page/郭婷筠.md "wikilink")。

## 經歷

  - [高雄市鼓山國小少棒隊](../Page/高雄市.md "wikilink")（惠安）
  - [高雄市七賢國中青少棒隊](../Page/高雄市.md "wikilink")
  - [高雄市三信家商青棒隊](../Page/高雄市.md "wikilink")
  - 台灣電力棒球隊
  - 陸光棒球隊
  - 味全棒球隊（業餘甲組）
  - 伊藤榮堂紅丸棒球隊（日本社會人球隊）
  - [中華職棒](../Page/中華職棒.md "wikilink")[統一獅隊](../Page/統一獅.md "wikilink")

## 外部連結

[Category:郭姓](../Category/郭姓.md "wikilink")
[G](../Category/台灣棒球選手.md "wikilink")
[G](../Category/統一獅隊球員.md "wikilink")
[G](../Category/中華職棒勝投王.md "wikilink")
[G](../Category/中華職棒救援王.md "wikilink")
[G](../Category/高雄市人.md "wikilink")
[G](../Category/中華職棒投手金手套獎得主.md "wikilink")
[G](../Category/中華職棒單月最有價值球員獎得主.md "wikilink")
[G](../Category/中華職棒終身禁賽名單.md "wikilink")