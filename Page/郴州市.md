**州市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[湖南省下辖的](../Page/湖南省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于湖南省东南部，地处[南岭山脉与](../Page/南岭.md "wikilink")[罗霄山脉交错](../Page/罗霄山脉.md "wikilink")、[长江水系与](../Page/长江.md "wikilink")[珠江水系分流的地带](../Page/珠江.md "wikilink")。东界[江西省](../Page/江西省.md "wikilink")[赣州市](../Page/赣州市.md "wikilink")，南邻[广东省](../Page/广东省.md "wikilink")[韶关市](../Page/韶关市.md "wikilink")，西接湖南[永州市](../Page/永州市.md "wikilink")，北连湖南[衡阳市](../Page/衡阳市.md "wikilink")、[株洲市](../Page/株洲市.md "wikilink")，是湖南的“南大门”。

## 历史

郴州历史悠久、源远流长，已有两千多年有文字可考的历史。“郴”字为郴州所独有，由林、邑二字合成，意为“林中之城”，最早见于[秦朝](../Page/秦朝.md "wikilink")，[李斯改](../Page/李斯.md "wikilink")“菻”（lǐn）为“郴”，并始置郴县。

自秦以来，郴州建置历朝更迭，曾设置郡、府、军、州等。[项羽](../Page/项羽.md "wikilink")“徙[义帝于郴](../Page/义帝.md "wikilink")”，[赵子龙大战桂阳郡](../Page/赵子龙.md "wikilink")，[洪秀全屯兵郴州](../Page/洪秀全.md "wikilink")，史志皆有记载。[西汉元鼎四年](../Page/西汉.md "wikilink")（前113年）置[桂阳郡](../Page/桂阳郡.md "wikilink")，辖郴、临武、南平、耒阳、桂阳、阳山、阴山、曲江等11县。唐[开元二十三年](../Page/开元.md "wikilink")（735年），改桂阳郡为郴州。宋[乾德元年](../Page/乾德.md "wikilink")（963年），设[郴州军](../Page/郴州军.md "wikilink")。1949年11月25日，成立郴县专区。1950年11月，更名郴州专区，辖10县。1967年3月8日，专区改称地区。1994年12月17日撤销郴州地区，设立地级郴州市。

唐宋文人[王昌龄](../Page/王昌龄.md "wikilink")、[杜甫](../Page/杜甫.md "wikilink")、[韩愈](../Page/韩愈.md "wikilink")、[刘禹锡](../Page/刘禹锡.md "wikilink")、[秦观等](../Page/秦观.md "wikilink")，均在此留下了脍炙人口的诗文。

## 地理

郴州位于湖南省东南部，东界[江西](../Page/江西.md "wikilink")，南邻[广东](../Page/广东.md "wikilink")，是湖南省的南大门。郴州的地貌复杂多样，以山丘为主，岗平相当，水面较少。山地、[丘陵面积约占总面积的近](../Page/丘陵.md "wikilink")3/4。地势东南高西北低，东南部以山地为主体；西北部以丘陵、岗地、平原为主。

郴州分属长江和珠江两大流域，三大水系，即[赣江](../Page/赣江.md "wikilink")、[湘江和](../Page/湘江.md "wikilink")[北江](../Page/北江.md "wikilink")。境内河流发育，成放射状密布。集雨面积大于100平方公里的河流62条，大于1000平方公里的河流6条。

### 气候

郴州市属于[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，大陆性较强。四季分明，平地丘陵区的冬夏季长而春秋季短。山区则冬季长，而春、夏、秋季短。平地丘陵区，由冬入春和由春入夏，则南方早于北方2—4天。市区多年平均于3月上旬入春，5月上旬入夏，9月底10月初入秋，11月底12月初入冬。春季降水量是一年最多的季节，占全年降水量的37.3%。日照时数220—290小时，日照时数呈南少北多的分布特征。春季气候最显著的特征是开春早，气温回升快，降水丰沛，多阴雨及冰雹大风。夏季气候炎热，易发生盛夏干旱，也易出现暴雨洪涝，由于平均海拔高度在400米以上，因而丘陵区和山地与相邻市相比，透出凉爽的特点。山区的凉爽气候特征则更加突出。郴州市的秋季主要是以秋高气爽天气为主，日照强，降水少，晴日多，易发生秋旱，少数年份秋雨绵绵伴有寒露风。冬季气候的特征是少严寒，雨雪少，气温比邻近市要高。一年中，最冷的月份是1月，平均气温为6.5℃，最冷时段常在小寒前后和大寒前后。最热的月份是7月，平均气温为27.8℃，最热时段常在7月下旬至8月上旬。随着春季来到，气温在3、4月迅速升高。盛夏之后，气温随之下降，尤其是秋分过后，9－12月，每月降低5℃之多进入冬季。

## 政治

### 现任领导

<table>
<caption>郴州市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党郴州市委员会.md" title="wikilink">中国共产党<br />
郴州市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/郴州市人民代表大会.md" title="wikilink">郴州市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/郴州市人民政府.md" title="wikilink">郴州市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议郴州市委员会.md" title="wikilink">中国人民政治协商会议<br />
郴州市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/易鹏飞.md" title="wikilink">易鹏飞</a>[1]</p></td>
<td><p><a href="../Page/刘志仁.md" title="wikilink">刘志仁</a>[2]</p></td>
<td><p><a href="../Page/李评_(1960年).md" title="wikilink">李评</a>[3]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/湖南省.md" title="wikilink">湖南省</a><a href="../Page/益阳市.md" title="wikilink">益阳市</a></p></td>
<td><p>湖南省<a href="../Page/新邵县.md" title="wikilink">新邵县</a></p></td>
<td><p>湖南省<a href="../Page/嘉禾县.md" title="wikilink">嘉禾县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2015年4月</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市辖2个[市辖区](../Page/市辖区.md "wikilink")、8个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[北湖区](../Page/北湖区.md "wikilink")、[苏仙区](../Page/苏仙区.md "wikilink")
  - 县级市：[资兴市](../Page/资兴市.md "wikilink")
  - 县：[桂阳县](../Page/桂阳县.md "wikilink")、[宜章县](../Page/宜章县.md "wikilink")、[永兴县](../Page/永兴县.md "wikilink")、[嘉禾县](../Page/嘉禾县.md "wikilink")、[临武县](../Page/临武县.md "wikilink")、[汝城县](../Page/汝城县.md "wikilink")、[桂东县](../Page/桂东县.md "wikilink")、[安仁县](../Page/安仁县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>郴州市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[4]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>431000</p></td>
</tr>
<tr class="odd">
<td><p>431002</p></td>
</tr>
<tr class="even">
<td><p>431003</p></td>
</tr>
<tr class="odd">
<td><p>431021</p></td>
</tr>
<tr class="even">
<td><p>431022</p></td>
</tr>
<tr class="odd">
<td><p>431023</p></td>
</tr>
<tr class="even">
<td><p>431024</p></td>
</tr>
<tr class="odd">
<td><p>431025</p></td>
</tr>
<tr class="even">
<td><p>431026</p></td>
</tr>
<tr class="odd">
<td><p>431027</p></td>
</tr>
<tr class="even">
<td><p>431028</p></td>
</tr>
<tr class="odd">
<td><p>431081</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

2010年[第六次人口普查初步结果显示](../Page/中华人民共和国第六次全国人口普查.md "wikilink")，郴州市常住人口4,581,778人，占湖南省人口的6.98%居[第8位](../Page/湖南省各市州人口列表.md "wikilink")，人口密度237人/[公里<sup>2</sup>](../Page/平方千米.md "wikilink")；男女性别比为107.60比100。按受教育程度分，大学以上文化程度占总人口的5.80%，初中以上文化程度占总人口的
58.82%，文盲人口仅占2.75%。辖区内共有1,345,619个家庭户，家庭户人口4,411,236人，占总人口的96.28%；家庭平均人口规模3.28人。年龄构成上，14岁及以下人口1,003,967人，占总人口的21.91%；15－64年人口3,203,996人，占69.93%；65岁及以上老年人口373,815人，占总人口的8.16%\[5\]。

<div style="font-size:90%;">

| <font size=3>**郴州各区县（市）人口**（2010与2000年人口普查\[6\]\[7\]） |
| ----------------------------------------------------- |
| 区县                                                    |
| 常住人口                                                  |
| |城镇                                                   |
| 总计                                                    |
| 市辖区                                                   |
| [北湖区](../Page/北湖区.md "wikilink")                      |
| [苏仙区](../Page/苏仙区.md "wikilink")                      |
| 县区                                                    |
| [桂阳县](../Page/桂阳县.md "wikilink")                      |
| [宜章县](../Page/宜章县.md "wikilink")                      |
| [永兴县](../Page/永兴县.md "wikilink")                      |
| [嘉禾县](../Page/嘉禾县.md "wikilink")                      |
| [临武县](../Page/临武县.md "wikilink")                      |
| [汝城县](../Page/汝城县.md "wikilink")                      |
| [桂东县](../Page/桂东县.md "wikilink")                      |
| [安仁县](../Page/安仁县.md "wikilink")                      |
| [资兴市](../Page/资兴市.md "wikilink")                      |

</div>

### 民族

郴州以[汉族人口为主](../Page/汉族.md "wikilink")，少数民族呈大分散小杂居，世居少数民族有[瑶和](../Page/瑶族.md "wikilink")[畲族](../Page/畲族.md "wikilink")。2000年[第五次人口普查结果显示](../Page/中华人民共和国第五次全国人口普查.md "wikilink")，郴州全市共有[汉族人口](../Page/汉族.md "wikilink")4,247,076人，占地区人口的98.20%；少数民族77,736人，占全省少数民族人口的1.21%居**[第7位](../Page/湖南各市州民族构成列表.md "wikilink")**，占地区人口的1.80%；55个少数民族中有41个民族分布。瑶族为郴州仅次于汉族的第2大人口，共有70,513人，占全市总人口的1.63%居[第2位](../Page/湖南各市州民族构成列表.md "wikilink")，占全省瑶族人口的10.01%，占区域内少数民族人口的90.71%。除瑶族外，[土家](../Page/土家族.md "wikilink")、[苗](../Page/苗族.md "wikilink")、[蒙古和](../Page/蒙古族.md "wikilink")[畲族人口分别过千人](../Page/畲族.md "wikilink")，[侗](../Page/侗族.md "wikilink")、[壮等](../Page/壮族.md "wikilink")8个民族人口过百人，[维吾尔等](../Page/维吾尔族.md "wikilink")9个民族人口10人以上\[8\]。郴州少数民族主要分布于[汝城](../Page/汝城县.md "wikilink")、[苏仙](../Page/苏仙区.md "wikilink")、[资兴](../Page/资兴市.md "wikilink")、[宜章](../Page/宜章县.md "wikilink")、[桂阳](../Page/桂阳县.md "wikilink")、[临武](../Page/临武县.md "wikilink")、[北湖](../Page/北湖区.md "wikilink")、[桂东等](../Page/桂东县.md "wikilink")8个县市区。在原已建立的资兴市团结瑶族乡，宜章县莽山瑶族乡，北湖区月峰瑶族乡、大塘瑶族乡，桂阳县华山、杨柳瑶族乡的基础上，1991年12月，建立汝城县延寿、岭秀、盈洞3个瑶族乡和小垣、三江口2个瑶族镇；1993年7月建立资兴市连坪瑶族乡；1994年11月建立临武县西山瑶族乡，郴州辖区共建有13个瑶族乡、镇，50个民族村\[9\]。

### 语言

郴州地区方言众多，本土汉语方言为[湘南土话和](../Page/湘南土话.md "wikilink")[客家话](../Page/客家话.md "wikilink")。但後来外來的[西南官话的使用範圍越来越大](../Page/西南官话.md "wikilink")，在郴州市城區扎根，以至於有人把郴州[官話簡稱為](../Page/官話.md "wikilink")“郴州話”。[湘南土话的分布区域与](../Page/湘南土话.md "wikilink")[西南官话的分布区域在郴州境内有所重叠](../Page/西南官话.md "wikilink")，形成了独特的双方言区。[湘南土话是郴州名副其实的第一大语言](../Page/湘南土话.md "wikilink")，分布在郴州的大多数城镇和绝大多数乡村，以对内交流为主，[湘南土话在郴州各县境内的口音差异很大](../Page/湘南土话.md "wikilink")。另外，[湘南土话不仅与西南官话完全不同](../Page/湘南土话.md "wikilink")，与湖南省内占主导的[湘语也无法互通](../Page/湘语.md "wikilink")。郴州位于中国南北要冲，历来接纳了众多不同地方的外来移民。[西南官话在郴州市城区和部分城镇占主导地位](../Page/西南官话.md "wikilink")，为对外使用，成为郴州境内各种方言使用者相互交流的主要工具。外界所谓的“郴州话”其实只是郴州市城区所使用的[西南官话](../Page/西南官话.md "wikilink")。此外，[客家话分布在郴州东南部的](../Page/客家话.md "wikilink")[汝城等地](../Page/汝城.md "wikilink")。郴州的[瑶族使用](../Page/瑶族.md "wikilink")[勉语](../Page/勉语.md "wikilink")（瑶族语言的一种）。

## 经济

2010年郴州的GDP为1087.2亿元。

郴州拥有湖南唯一的国家级[出口加工区](../Page/出口加工区.md "wikilink")。引用外资能力十分强，仅次于省会[长沙](../Page/长沙.md "wikilink")。

2016年郴州市GDP完成2190.8亿元 增幅居全省第二位\[10\]

## 交通

  - 铁路：[京广铁路](../Page/京广铁路.md "wikilink")、[武广客运专线](../Page/武广客运专线.md "wikilink")
  - 公路：[107国道](../Page/107国道.md "wikilink")、[106国道](../Page/106国道.md "wikilink")、[京珠高速公路](../Page/京珠高速公路.md "wikilink")，[厦蓉高速公路](../Page/厦蓉高速公路.md "wikilink")，岳临高速
  - 航空：[郴州北湖机场](../Page/郴州北湖机场.md "wikilink")（在建）\[11\]

## 旅游

郴州是“中国优秀旅游城市”之一。境内有[国家AAAAA级旅游区](../Page/国家AAAAA级旅游区.md "wikilink")——[东江湖风景旅游区](../Page/东江湖.md "wikilink")、道教中的天下第十八福地[苏仙岭景区](../Page/苏仙岭.md "wikilink")，以及[万华岩](../Page/万华岩.md "wikilink")、[飞天山](../Page/飞天山.md "wikilink")、[王仙岭](../Page/王仙岭.md "wikilink")、[仰天湖](../Page/仰天湖.md "wikilink")、[便江等风景名胜](../Page/便江.md "wikilink")。

郴州也是[中国温泉之城](../Page/中国温泉之城.md "wikilink")。

## 名人

  - [邓中夏](../Page/邓中夏.md "wikilink")
  - [黄克诚](../Page/黄克诚.md "wikilink")
  - [肖克](../Page/肖克.md "wikilink")
  - [邓力群](../Page/邓力群.md "wikilink")
  - [易思玲](../Page/易思玲.md "wikilink")

## 友好城市

  - [拉雷多市](../Page/拉雷多市.md "wikilink")（2002年9月1日）

## 参考文献

## 外部链接

  - [郴州市人民政府门户网站](http://www.czs.gov.cn)

{{-}}

[Category:湖南地级市](../Category/湖南地级市.md "wikilink")
[郴州](../Category/郴州.md "wikilink")
[湘](../Category/中国中等城市.md "wikilink")

1.

2.

3.

4.

5.  2010年湖南与郴州人口普查数据分别出自[《郴州市2010年第六次全国人口普查主要数据公报》](http://www.cztj.gov.cn/Article/ShowArticle.asp?ArticleID=18152)与[《2010年湖南省第六次全国人口普查主要数据公报》](http://www.hntj.gov.cn/zhuanlan/6th_census/events/in_province/201105/t20110513_85176.htm)


6.
7.
8.  2000年第五次人口普查数据出自[湖南省统计局/人口服务网（子网）-第五次人口普查数据](http://www.hntj.gov.cn/vital_statistics/census/default.htm#part1)
    ，分县人口数据参考[国家统计局-第五次人口普查数据库](http://www.stats.gov.cn/tjsj/pcsj/rkpc/dwcrkpc/)

9.  郴州市民族人口区域分布参考：[郴州市政府网：郴州民族](http://www.czs.gov.cn/sitepublish/site200/zjcz/czmz/content_35318.html)


10.

11.