[Metro-Cammell.png](https://zh.wikipedia.org/wiki/File:Metro-Cammell.png "fig:Metro-Cammell.png")
**都城嘉慕**（，原名**The Metropolitan Cammell Carriage and
Wagon**，[縮寫](../Page/縮寫.md "wikilink")：**MCCW**）是[英國](../Page/英國.md "wikilink")[英格蘭一家生產](../Page/英格蘭.md "wikilink")[鐵路車輛的](../Page/鐵路車輛.md "wikilink")[公司](../Page/公司.md "wikilink")，於1929年由Metropolitan
Vickers Limited\[1\]及Cammell
Laird的[鐵路部門合併後成立](../Page/鐵路.md "wikilink")（當中包括Midland
Railway Carriage and Wagon
Company），[總部位於](../Page/總部.md "wikilink")[伯明翰](../Page/伯明翰.md "wikilink")。都城嘉慕亦生產[巴士車身](../Page/巴士.md "wikilink")，於1932年，Weymann
Motor
Bodies與都城嘉慕巴士部合組[都城嘉慕威曼公司](../Page/都城嘉慕威曼.md "wikilink")，兩者同屬Laird集團。

都城嘉慕曾經為英格蘭及海外地區生產列車，包括：

  - [倫敦地鐵C](../Page/倫敦地鐵.md "wikilink")69/C77 Stock、D78 Stock、1972
    Stock、1973 Stock
  - [港鐵都城嘉慕電動列車（直流電）](../Page/港鐵現代化列車.md "wikilink")，俗稱「白頭」、「M-Train」
  - [港鐵迪士尼綫列車](../Page/港鐵迪士尼綫列車.md "wikilink")，由[港鐵都城嘉慕電動列車（直流電）改裝](../Page/港鐵現代化列車.md "wikilink")。
  - [港鐵都城嘉慕電動列車（交流電）](../Page/港鐵中期翻新列車.md "wikilink")，俗稱「黃頭」、「烏蠅頭」、「MLR」
  - [英法海底隧道的鐵路](../Page/英法海底隧道.md "wikilink")[電動列車](../Page/電動列車.md "wikilink")
  - [西馬來西亞Keretapi](../Page/西馬來西亞.md "wikilink") Tanah Melayu

1989年，Laird集團陷入財政危機，同年5月，都城嘉慕被[法國](../Page/法國.md "wikilink")[通用電力阿爾斯松](../Page/阿爾斯通.md "wikilink")（現稱阿爾斯通公司）收購。最後一批於[伯明翰市Washwood](../Page/伯明翰市.md "wikilink")
Heath廠房建造的車輛，為[英鐵390型電力動車組](../Page/英鐵390型電力動車組.md "wikilink")，2005年製造最後一輛列車後正式關閉。

## 參考資料

<div class="references-big">

<references/>

</div>

[Category:英国汽车公司](../Category/英国汽车公司.md "wikilink")
[Category:英国铁路公司](../Category/英国铁路公司.md "wikilink")
[Category:已结业制造公司](../Category/已结业制造公司.md "wikilink")
[Category:巴士生產商](../Category/巴士生產商.md "wikilink")
[Category:鐵路車輛製造商](../Category/鐵路車輛製造商.md "wikilink")

1.  Electric Trains in Britain,B.K.Cooper,Ian Allan,1979