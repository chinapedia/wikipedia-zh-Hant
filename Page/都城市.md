**都城市**（）是位于[日本](../Page/日本.md "wikilink")[宮崎縣西南端的](../Page/宮崎縣.md "wikilink")[城市](../Page/城市.md "wikilink")，是宮崎縣內人口第二多的城市。於2006年1月1日，由舊[山之口町](../Page/山之口町.md "wikilink")、[高城町](../Page/高城町.md "wikilink")、[山田町](../Page/山田町_\(宮崎縣\).md "wikilink")、[高崎町與舊都城市合併而成](../Page/高崎町.md "wikilink")。

都城市位於[都城盆地](../Page/都城盆地.md "wikilink")，西側為[霧島山地](../Page/霧島山地.md "wikilink")、東側為[鰐塚山地](../Page/鰐塚山地.md "wikilink")，[大淀川從轄區中流過](../Page/大淀川.md "wikilink")。由於位於宮崎縣與[鹿兒島縣之間](../Page/鹿兒島縣.md "wikilink")，並為兩縣之間的交通中心，有國道、鐵路經過，[宮崎機場與](../Page/宮崎機場.md "wikilink")[鹿兒島機場都在](../Page/鹿兒島機場.md "wikilink")40公里的距離之內。與鄰近的[三股町](../Page/三股町.md "wikilink")、[曾於市](../Page/曾於市.md "wikilink")、[志布志市組成一個人口約](../Page/志布志市.md "wikilink")25萬人經濟圈，並為此經濟圈的中心城市。\[1\]

市名「都城」源自過去[島津氏的支族](../Page/島津氏.md "wikilink")[北鄉氏第二代](../Page/北鄉氏.md "wikilink")[北鄉義久在此所建的](../Page/北鄉義久.md "wikilink")[都之城](../Page/都之城.md "wikilink")。

## 歷史

[平安時代末期的](../Page/平安時代.md "wikilink")1026年，時任[大宰大監的](../Page/大宰大監.md "wikilink")[平季基在此建設了當時日本最大的](../Page/平季基.md "wikilink")[莊園](../Page/莊園.md "wikilink")「[島津莊](../Page/島津莊.md "wikilink")」，1185年當時的莊園管理者[惟宗忠久繼承了莊園的名字](../Page/惟宗忠久.md "wikilink")「島津」，成為[島津氏的始祖](../Page/島津氏.md "wikilink")。

[室町時代](../Page/室町時代.md "wikilink")，當地由島津氏的分家[北鄉氏統治](../Page/北鄉氏.md "wikilink")；[江戶時代也延續成為](../Page/江戶時代.md "wikilink")[薩摩藩的私領](../Page/薩摩藩.md "wikilink")，繼續由北鄉氏（又稱都城島津氏）統治。[戊辰戰爭後的藩政改革中改成本藩直轄領](../Page/戊辰戰爭.md "wikilink")，直到[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，設置[都城縣](../Page/都城縣_\(日本\).md "wikilink")，並成為縣政府所在地。不過僅一年後，都城縣便與北側的[美美津縣合併為新設置的](../Page/美美津縣.md "wikilink")[宮崎縣](../Page/宮崎縣.md "wikilink")。

### 年表

  - 1871年11月：設置都城縣。\[2\]
  - 1872年2月18日：設置都城縣政府。
  - 1873年1月15日：都城縣合併為宮崎縣，都城縣政府被廢除。
  - 1889年5月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區分屬[北諸縣郡轄下的都城町](../Page/北諸縣郡.md "wikilink")、[沖水村](../Page/沖水村.md "wikilink")、[五十市村](../Page/五十市村.md "wikilink")、[志和池村](../Page/志和池村.md "wikilink")、庄內村、[中鄉村](../Page/中鄉村.md "wikilink")、山之口村、高城村、山田村、高崎村。
  - 1891年7月4日：[西岳村從庄內村分村](../Page/西岳村.md "wikilink")。（1町11村）
  - 1924年4月1日：都城町改制為**都城市**。
  - 1924年5月1日：庄內村改制為[庄內町](../Page/庄內町_\(宮崎縣\).md "wikilink")。
  - 1934年2月11日：高城村改制為[高城町](../Page/高城町.md "wikilink")。
  - 1936年5月20日：沖水村和五十市村被併入都城市。
  - 1940年2月11日：高崎村改制為[高崎町](../Page/高崎町.md "wikilink")。
  - 1953年1月15日：山田村改制為[山田町](../Page/山田町_\(宮崎縣\).md "wikilink")。
  - 1956年7月15日：庄內町和西岳村[合併為](../Page/市町村合併.md "wikilink")[莊內町](../Page/莊內町.md "wikilink")。
  - 1957年3月1日：志和池村被併入都城市。
  - 1964年11月3日：山之口村改制為[山之口町](../Page/山之口町.md "wikilink")。
  - 1965年4月1日：莊內町被併入都城市。
  - 1967年3月3日：中鄉村被併入都城市。
  - 2006年1月1日：都城市、山之口町、高城町、山田町、高崎町合併為新設置的**都城市**。

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年5月1日</p></th>
<th><p>1889年 -1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>都城町</p></td>
<td><p>1924年4月1日<br />
改制為都城市</p></td>
<td><p>都城市</p></td>
<td><p>都城市</p></td>
<td><p>2006年1月1日<br />
合併為都城市</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>五十市村</p></td>
<td><p>1936年5月20日<br />
併入都城市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>| 沖水村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>志和池村</p></td>
<td><p>1957年3月1日<br />
併入都城市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>中鄉村</p></td>
<td><p>1967年3月3日<br />
併入都城市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>庄內村</p></td>
<td><p>1924年5月15日<br />
改制為庄內町</p></td>
<td><p>1956年7月15日<br />
合併為莊內町</p></td>
<td><p>1965年4月1日<br />
併入都城市</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1891年7月4日<br />
西岳村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>高城村</p></td>
<td><p>1934年2月11日<br />
改制為高城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>山之口村</p></td>
<td><p>1964年11月3日<br />
改制為山之口町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>高崎村</p></td>
<td><p>1940年2月11日<br />
改制為高崎町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>山田村</p></td>
<td><p>1953年1月15日<br />
改制為山田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [日豐本線](../Page/日豐本線.md "wikilink")：[青井岳車站](../Page/青井岳車站.md "wikilink")
        - [山之口車站](../Page/山之口車站.md "wikilink") -
        （[三股町轄區內](../Page/三股町.md "wikilink")：[餅原車站](../Page/餅原車站.md "wikilink")
        - [三股車站](../Page/三股車站.md "wikilink")） -
        [都城車站](../Page/都城車站.md "wikilink") -
        [西都城車站](../Page/西都城車站.md "wikilink") -
        [五十市車站](../Page/五十市車站.md "wikilink")
      - [吉都線](../Page/吉都線.md "wikilink")：[日向前田車站](../Page/日向前田車站.md "wikilink")
        - [高崎新田車站](../Page/高崎新田車站.md "wikilink") -
        [東高崎車站](../Page/東高崎車站.md "wikilink") -
        [萬塚車站](../Page/萬塚車站.md "wikilink") -
        [谷頭車站](../Page/谷頭車站.md "wikilink") -
        [日向庄內車站](../Page/日向庄內車站.md "wikilink") - 都城車站

### 道路

  - 高速道路

<!-- end list -->

  - [宮崎自動車道](../Page/宮崎自動車道.md "wikilink")：[日向高崎休息區](../Page/日向高崎休息區.md "wikilink")
    - [都城交流道](../Page/都城交流道.md "wikilink") -
    [山之口服務區](../Page/山之口服務區.md "wikilink")

## 觀光資源

[Kirishima_Takachihonomine_2.jpg](https://zh.wikipedia.org/wiki/File:Kirishima_Takachihonomine_2.jpg "fig:Kirishima_Takachihonomine_2.jpg")\]\]

  - [高千穗峰](../Page/高千穗峰.md "wikilink")
  - [高千穗牧場](../Page/高千穗牧場.md "wikilink")
  - 關之尾瀑布
  - 神柱宮（神柱公園）
  - 興玉神社
  - 都城市立美術館
  - 立花天文台
  - 島津寒天工場跡
  - 山之口古墳
  - 青井岳
  - 山田溫泉
  - 観音池公園
  - 月山日和城遺跡
  - 祝吉御所遺跡

## 教育

### 高等專門學校

都城工業高等專門學校

### 高等学校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>公立</dt>

</dl>
<ul>
<li><a href="../Page/宮崎縣立都城泉丘高等學校.md" title="wikilink">宮崎縣立都城泉丘高等學校</a></li>
<li><a href="../Page/宮崎縣立都城西高等學校.md" title="wikilink">宮崎縣立都城西高等學校</a></li>
<li><a href="../Page/宮崎縣立都城工業高等學校.md" title="wikilink">宮崎縣立都城工業高等學校</a></li>
<li><a href="../Page/宮崎縣立都城商業高等學校.md" title="wikilink">宮崎縣立都城商業高等學校</a></li>
<li><a href="../Page/宮崎縣立都城農業高等學校.md" title="wikilink">宮崎縣立都城農業高等學校</a></li>
<li><a href="../Page/宮崎縣立高城高等學校.md" title="wikilink">宮崎縣立高城高等學校</a></li>
</ul></td>
<td><dl>
<dt>私立</dt>

</dl>
<ul>
<li><a href="../Page/都城東高等學校.md" title="wikilink">都城東高等學校</a></li>
<li><a href="../Page/都城高等學校.md" title="wikilink">都城高等學校</a></li>
<li><a href="../Page/都城聖多明我學園高等學校.md" title="wikilink">都城聖多明我學園高等學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹、友好都市

### 海外

  - [江津區](../Page/江津區.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")
    [重慶市](../Page/重慶市.md "wikilink")）：在[二次大戰期間的](../Page/二次大戰.md "wikilink")1940年，江津市出身的[聶榮臻將軍救了來自都城市的栫美穗子](../Page/聶榮臻.md "wikilink")；之後在中日兩國建交後成為當時的話題，因此在都城市政府官員前往中國訪問時，聶榮臻提出了兩市締結友好城市的建議。直到1998年4月，聶榮臻的女兒聶力以[中華全國婦女聯合會副會長身分訪日時](../Page/中華全國婦女聯合會.md "wikilink")，再次提出了此意見。兩市最後在1999年11月締結友好交流都市。\[3\]

  - [烏蘭巴托](../Page/烏蘭巴托.md "wikilink")（[蒙古國](../Page/蒙古國.md "wikilink")）：由於過去曾經贈送[風力發電機給蒙古](../Page/風力發電機.md "wikilink")，開始了兩地之間的交流，最後於1999年11月締結友好交流都市。\[4\]

## 本地出身之名人

  - [東國原英夫](../Page/東國原英夫.md "wikilink")：宮崎縣[知事](../Page/知事.md "wikilink")、前[藝人](../Page/藝人.md "wikilink")，出身於[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[末吉町](../Page/末吉町.md "wikilink")（現以合併為[曾於市](../Page/曾於市.md "wikilink")），後成長於都城市。
  - [青島あきな](../Page/青島あきな.md "wikilink")：[藝人](../Page/藝人.md "wikilink")
  - [井之上正盛](../Page/井之上正盛.md "wikilink")：日本外交官、在[伊拉克遭槍擊而死](../Page/伊拉克.md "wikilink")。
  - [井上康生](../Page/井上康生.md "wikilink")：[2000年夏季奧林匹克運動會](../Page/2000年夏季奧林匹克運動會.md "wikilink")[柔道](../Page/柔道.md "wikilink")[金牌](../Page/金牌.md "wikilink")
  - [入來智](../Page/入來智.md "wikilink")：前[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [入來祐作](../Page/入來祐作.md "wikilink")：職業棒球選手
  - [池江泰郎](../Page/池江泰郎.md "wikilink")：[日本中央競馬會的](../Page/日本中央競馬會.md "wikilink")[訓碼師](../Page/訓碼師.md "wikilink")
  - [大田原隆太](../Page/大田原隆太.md "wikilink")：職業棒球選手
  - [岡元次郎](../Page/岡元次郎.md "wikilink")：俳優
  - [桐島優介](../Page/桐島優介.md "wikilink")：俳優、音樂家
  - [田中幸雄](../Page/田中幸雄.md "wikilink")：前職業棒球選手
  - [永井利光](../Page/永井利光.md "wikilink")：[鼓手](../Page/鼓手.md "wikilink")
  - [永瀨正敏](../Page/永瀨正敏.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [永友聖也](../Page/永友聖也.md "wikilink")：音樂家
  - [中山大三郎](../Page/中山大三郎.md "wikilink")：[作曲家](../Page/作曲家.md "wikilink")
  - [溫水洋一](../Page/溫水洋一.md "wikilink")：俳優
  - [福盛和男](../Page/福盛和男.md "wikilink")：職業棒球選手
  - [藤井治芳](../Page/藤井治芳.md "wikilink")：前日本[建設省](../Page/建設省.md "wikilink")[事務次官](../Page/事務次官.md "wikilink")、前[日本道路公團總裁](../Page/日本道路公團.md "wikilink")
  - [山崎十三](../Page/山崎十三.md "wikilink")：[漫畫編劇](../Page/漫畫.md "wikilink")
  - [若松俊秀](../Page/若松俊秀.md "wikilink")：俳優
  - [馬渡松子](../Page/馬渡松子.md "wikilink")：[歌手](../Page/歌手.md "wikilink")
  - [宮路一昭](../Page/宮路一昭.md "wikilink")：[吉他手](../Page/吉他手.md "wikilink")、[作曲家](../Page/作曲家.md "wikilink")、[編曲家](../Page/編曲家.md "wikilink")、音樂製作人
  - [蕨野友也](../Page/蕨野友也.md "wikilink")：俳優
  - [田鍋友時](../Page/田鍋友時.md "wikilink")：前世界最高齡男性

## 參考資料

## 外部連結

  - [都城市官方網頁](https://web.archive.org/web/20081002131318/http://www.city.miyakonojo.miyazaki.jp/shisei/kokusaikouryu/chinese/titlechinese.jsp)

  - [都城觀光協會](https://web.archive.org/web/20080305190140/http://aquz01.ddo.jp/test3/)

  - [都城市情報網站](http://miyakonjo.com/)

  - [都城商工會議所](http://www.miyazaki-cci.or.jp/miyakonjo/)

  - [都城市地區情報](http://www.geocities.jp/m_tosiro/)

  -
<!-- end list -->

1.
2.
3.
4.