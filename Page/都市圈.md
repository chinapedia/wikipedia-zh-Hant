[Albuquerque_aerial.jpg](https://zh.wikipedia.org/wiki/File:Albuquerque_aerial.jpg "fig:Albuquerque_aerial.jpg")[新墨西哥州](../Page/新墨西哥州.md "wikilink")[阿布奎基市的都會區到周邊郊區空照](../Page/阿布奎基_\(新墨西哥州\).md "wikilink")\]\]
**都市圈**（）又稱**都會區**、**都市区**，是指以[大都市为核心](../Page/大都市.md "wikilink")，向周围辐射构成城市的集合區域。都市圈的特点反映在城市之间[经济的紧密联系](../Page/经济.md "wikilink")、[产业的分工与合作](../Page/产业.md "wikilink")，[交通与](../Page/交通.md "wikilink")[社会生活](../Page/社会.md "wikilink")、[城市规划和](../Page/城市规划.md "wikilink")[基础设施建设相互影响](../Page/基础设施.md "wikilink")。

[北美地区的](../Page/北美.md "wikilink")“metropolitan
area”在中文里一般译作“**大都会**”、“**大都市区**”或“**都会区**”。

## 定義分歧

[缩略图](https://zh.wikipedia.org/wiki/File:Farviewshijiazhuang.jpg "fig:缩略图")以[衛星城市的角色與北京同屬一經濟圈](../Page/衛星城市.md "wikilink")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Shinjuku_2006-02-22_a.jpg "fig:缩略图")\]\]

大都市区包括了[市区](../Page/市区.md "wikilink")（连续的[建成区](../Page/建成区.md "wikilink")）和可能不具城市特色的区域，但这些区域通过就业或其他商业活动与中心紧密相连。这些边远区域有时被称为通勤带（commuter
belt），可能远远超出城市区域，延伸到其他行政区。例如，纽约长岛的[艾斯利普被认为是](../Page/艾斯利普.md "wikilink")[纽约大都会区的一部分](../Page/纽约大都会区.md "wikilink")。

实际使用中，官方和非官方的大都市区的界限都不尽相同。有时它们与市区（urban
area）差别不大，而有时它们覆盖的区域与单一的城市聚落没什么关系；大都市区的比较统计数据应当考虑到这一点。即使对同一个都市区，其人口统计数字之间也可能相差上百万。

英语中，自1950年采用以来，大都市区（metropolitan
areas）的基本概念没有发生明显变化，\[1\]不过地理分布则有明显变化，并且未来还可能会有更多变化。\[2\]由于“大都市统计区”（metropolitan
statistical area）一词的不稳定性，人们使用的更通俗的术语是“都市服务区”（metro service
area）、“都市区”（metro
area）或“MSA”，不仅包括城市，还包括郊区，远郊，有时还包括农村地区，这些都被认为会有影响。多中心都市区包含多个不连续发展的市区（urban
agglomeration(s)）。定义都市圈时，仅需一个或多个城市形成一个核心，而其他区域与之高度融合。

### 臺灣

在同一區域內，由一個或一個以上之中心都市為核心，連結與此中心都市在社會、經濟上合為一體之市、鎮、鄉（稱為衛星市鎮）所共同組成之地區，且其區內人口總數達三十萬人以上。

### 中国大陆

中國大陸過去對「都市圈」「都會區」「[城市群](../Page/城市群.md "wikilink")」「大都市圈」等概念並無嚴格區分，其中「城市群」为[中国大陆自](../Page/中国大陆.md "wikilink")1990年代以后常用的[地域经济用语](../Page/地缘经济.md "wikilink")，但也常直接借用[日文](../Page/日文.md "wikilink")“都市圈”来表示同一概念，[日文](../Page/日文.md "wikilink")“”即英文“metropolitan
coordinating
region”之含义。2019年2月19日，[国家发展改革委發佈](../Page/国家发展改革委.md "wikilink")《国家发展改革委关于培育发展现代化-{都市圈}-的指导意见》，其中「都市圈」被定義為「城市群内部以超大特大城市或辐射带动功能强的大城市为中心、以1小时通勤圈为基本范围的城镇化空间形态」\[3\]，從官方上定義了「都市圈」。

而事实上，一些更早的规划已经将“都市圈”等作为“城市群”的次一层级，例如《珠江三角洲城镇群协调发展规划（2004-2020）》《珠江三角洲城乡规划一体化规划（2009—2020年）》中将[珠江三角洲城市群分为广佛肇都市区](../Page/珠江三角洲城市群.md "wikilink")、深莞惠都市区、珠中江都市区\[4\]；2016年的《长江三角洲城市群发展规划》提出了“一核五圈四带”，其中“五圈”是指“推动[南京都市圈](../Page/南京都市圈.md "wikilink")、[杭州都市圈](../Page/杭州都市圈.md "wikilink")、[合肥都市圈](../Page/合肥都市圈.md "wikilink")、[苏锡常都市圈](../Page/苏锡常都市圈.md "wikilink")、[宁波都市圈的同城化发展](../Page/宁波都市圈.md "wikilink")”。\[5\]

《中国都市圈发展报告2018》指出，除港澳台之外，全国共有34个中心城市都市圈，占到全国总面积的24%，总人口的59%，GDP的77.8%。根据发展水平差异，报告将这些都市圈分为成熟型、发展型和培育型三个层级，其中，长三角和珠三角范围内的7个都市圈发展最为成熟，已经形成连绵成片、互相重叠的[都市连绵区](../Page/都市连绵区.md "wikilink")。

  - 成熟型都市圈：长三角连绵区包括[上海市](../Page/上海市.md "wikilink")、[杭州市](../Page/杭州市.md "wikilink")、[南京市](../Page/南京市.md "wikilink")、[宁波市为中心城市的城镇密集区域](../Page/宁波市.md "wikilink")，在都市圈发展水平、中心城市贡献度、同城化机制三个方面位于全国首位，都市圈联系强度方面位于全国第二。珠三角连绵区指以[广州市都市圈和](../Page/广州市.md "wikilink")[深圳市都市圈所在区域](../Page/深圳市.md "wikilink")，发展质量仅次于长三角，但在内部城市之间联系度上优于长三角。
  - 发展型都市圈：16个，除首都都市圈包含[北京和](../Page/北京.md "wikilink")[天津两个中心城市之外](../Page/天津.md "wikilink")，其余都市圈均为单一中心城市，这些中心城市分别为[合肥](../Page/合肥.md "wikilink")、[青岛](../Page/青岛.md "wikilink")、[成都](../Page/成都.md "wikilink")、[西安](../Page/西安.md "wikilink")、[郑州](../Page/郑州.md "wikilink")、[厦门](../Page/厦门.md "wikilink")、[济南](../Page/济南.md "wikilink")、[武汉](../Page/武汉.md "wikilink")、[石家庄](../Page/石家庄.md "wikilink")、[长春](../Page/长春.md "wikilink")、[太原](../Page/太原.md "wikilink")、[长沙](../Page/长沙.md "wikilink")、[贵阳](../Page/贵阳.md "wikilink")、[南宁](../Page/南宁.md "wikilink")、[沈阳](../Page/沈阳.md "wikilink")。
  - 培育型都市圈：11个，各自所在区域的中心城市分别为[南昌](../Page/南昌.md "wikilink")、[昆明](../Page/昆明.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[银川](../Page/银川.md "wikilink")、[哈尔滨](../Page/哈尔滨.md "wikilink")、[大连](../Page/大连.md "wikilink")、[兰州](../Page/兰州.md "wikilink")、[福州](../Page/福州.md "wikilink")、[呼和浩特](../Page/呼和浩特.md "wikilink")、[乌鲁木齐](../Page/乌鲁木齐.md "wikilink")、[西宁](../Page/西宁.md "wikilink")。

除去长三角和珠三角连绵区，一些发展型都市圈在一些单项排名中很突出。在都市圈发展水平排名中，[首都都市圈](../Page/首都都市圈.md "wikilink")、[青岛都市圈](../Page/青岛都市圈.md "wikilink")、[厦门都市圈排名靠前](../Page/厦门都市圈.md "wikilink")。报告认为，首都都市圈之所以低于长三角和珠三角主要在于北京市的辐射带动作用更多体现在全国尺度，其次，河北相关城市与北京和天津相比存在断层，拉低整体发展水平。\[6\]

### 香港

根據[香港政府的定義](../Page/香港政府.md "wikilink")，[香港的都會區指](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")（及鄰近島嶼）、[九龍半島和](../Page/九龍半島.md "wikilink")[荃灣新市鎮](../Page/荃灣新市鎮.md "wikilink")（即[葵青區及](../Page/葵青區.md "wikilink")[荃灣區扣除](../Page/荃灣區.md "wikilink")[馬灣和](../Page/馬灣.md "wikilink")[北大嶼山外的地區](../Page/北大嶼山.md "wikilink")）。

[新界地區如](../Page/新界.md "wikilink")[沙田區](../Page/沙田區.md "wikilink")、[大埔區](../Page/大埔區.md "wikilink")、[北區](../Page/北區_\(香港\).md "wikilink")、[西貢區](../Page/西貢區.md "wikilink")、[元朗區](../Page/元朗區.md "wikilink")、[屯門區](../Page/屯門區.md "wikilink")、[離島區以及屬於荃灣區的](../Page/離島區.md "wikilink")[馬灣及](../Page/馬灣.md "wikilink")[北大嶼山並不是香港的市中心](../Page/北大嶼山.md "wikilink")，因此不算是香港的都會區。

## 各地實例

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><dl>
<dt>北美：</dt>

</dl>
<ul>
<li>加拿大：（超過一百萬）
<ul>
<li><a href="../Page/多倫多.md" title="wikilink">多倫多</a></li>
<li><a href="../Page/蒙特婁.md" title="wikilink">蒙特婁</a></li>
<li><a href="../Page/溫哥華.md" title="wikilink">溫哥華</a></li>
<li><a href="../Page/渥太華.md" title="wikilink">渥太華</a></li>
</ul></li>
<li>美國：（超過4百萬）
<ul>
<li><a href="../Page/纽约.md" title="wikilink">紐約</a></li>
<li><a href="../Page/洛杉矶.md" title="wikilink">大洛杉磯</a></li>
<li><a href="../Page/芝加哥.md" title="wikilink">芝加哥</a></li>
<li><a href="../Page/達拉斯.md" title="wikilink">達拉斯和</a><a href="../Page/沃思堡.md" title="wikilink">沃思堡</a>（<a href="../Page/雙城.md" title="wikilink">雙城</a>）</li>
<li><a href="../Page/休斯敦.md" title="wikilink">休士頓</a></li>
<li><a href="../Page/华盛顿哥伦比亚特区.md" title="wikilink">華盛頓</a></li>
<li><a href="../Page/迈阿密.md" title="wikilink">邁阿密</a></li>
<li><a href="../Page/亚特兰大.md" title="wikilink">亞特蘭大</a></li>
<li><a href="../Page/波士顿.md" title="wikilink">波士頓</a></li>
<li><a href="../Page/旧金山.md" title="wikilink">舊金山灣區</a></li>
<li><a href="../Page/鳳凰城_(亞利桑那州).md" title="wikilink">鳳凰城</a></li>
<li><a href="../Page/底特律.md" title="wikilink">底特律</a></li>
</ul></li>
</ul>
<dl>
<dt>南美地區：</dt>

</dl>
<ul>
<li>巴西：
<ul>
<li><a href="../Page/聖保羅_(巴西).md" title="wikilink">聖保羅</a></li>
<li><a href="../Page/里约热内卢.md" title="wikilink">里約熱內盧</a></li>
<li><a href="../Page/贝洛奥里藏特.md" title="wikilink">美景市</a></li>
</ul></li>
<li>阿根廷：
<ul>
<li><a href="../Page/布宜諾斯艾利斯.md" title="wikilink">大布宜諾斯艾利斯</a></li>
</ul></li>
<li>秘魯：
<ul>
<li><a href="../Page/利馬.md" title="wikilink">利馬</a></li>
</ul></li>
<li>哥倫比亞：
<ul>
<li><a href="../Page/波哥大.md" title="wikilink">波哥大</a></li>
</ul></li>
</ul></td>
<td><dl>
<dt>歐洲地區：</dt>

</dl>
<ul>
<li>英國：
<ul>
<li><a href="../Page/伦敦.md" title="wikilink">倫敦</a></li>
<li><a href="../Page/伯明翰.md" title="wikilink">柏明罕</a></li>
<li><a href="../Page/曼彻斯特.md" title="wikilink">曼徹斯特</a></li>
<li><a href="../Page/利兹.md" title="wikilink">里茲</a></li>
<li><a href="../Page/格拉斯哥.md" title="wikilink">格拉斯哥</a></li>
</ul></li>
<li>法國：
<ul>
<li><a href="../Page/巴黎.md" title="wikilink">巴黎</a></li>
<li><a href="../Page/里昂.md" title="wikilink">里昂</a></li>
<li><a href="../Page/馬賽.md" title="wikilink">馬賽</a></li>
<li><a href="../Page/圖盧茲.md" title="wikilink">圖盧茲</a></li>
<li><a href="../Page/里爾.md" title="wikilink">里爾</a></li>
<li><a href="../Page/尼斯.md" title="wikilink">尼斯</a></li>
</ul></li>
<li>德國：
<ul>
<li><a href="../Page/萊茵-魯爾.md" title="wikilink">萊茵-魯爾城市群</a></li>
<li><a href="../Page/柏林/布蘭登堡大都市區.md" title="wikilink">柏林/布蘭登堡大都市區</a></li>
<li><a href="../Page/法蘭克福-萊茵-美茵.md" title="wikilink">法蘭克福-萊茵-美茵大都會區</a></li>
<li></li>
<li></li>
<li></li>
<li><a href="../Page/薩克森三角城市群.md" title="wikilink">薩克森三角城市群</a></li>
</ul></li>
<li>西班牙：
<ul>
<li><a href="../Page/瓦倫西亞.md" title="wikilink">瓦倫西亞</a></li>
<li></li>
<li></li>
<li><a href="../Page/穆尔爾西亞-阿利坎特城市帶.md" title="wikilink">穆尔爾西亞-阿利坎特城市帶</a></li>
</ul></li>
<li>義大利：
<ul>
<li><a href="../Page/熱諾瓦.md" title="wikilink">熱諾瓦</a></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></li>
<li>荷蘭：
<ul>
<li><a href="../Page/蘭斯台德.md" title="wikilink">蘭斯台德城市群</a>（<a href="../Page/阿姆斯特丹.md" title="wikilink">阿姆斯特丹</a>-<a href="../Page/海牙.md" title="wikilink">海牙</a>-<a href="../Page/鹿特丹.md" title="wikilink">鹿特丹</a>-<a href="../Page/烏特勒支.md" title="wikilink">烏特勒支</a>）</li>
</ul></li>
</ul></td>
<td><dl>
<dt>東亞地區：</dt>

</dl>
<ul>
<li>中華人民共和國（中國大陸）：
<ul>
<li><a href="../Page/长江三角洲城市群.md" title="wikilink">长江三角洲城市群</a>
<ul>
<li><a href="../Page/南京都市圈.md" title="wikilink">南京都市圈</a></li>
<li><a href="../Page/杭州都市圈.md" title="wikilink">杭州都市圈</a></li>
<li><a href="../Page/合肥都市圈.md" title="wikilink">合肥都市圈</a></li>
<li><a href="../Page/苏锡常都市圈.md" title="wikilink">苏锡常都市圈</a></li>
<li><a href="../Page/宁波都市圈.md" title="wikilink">宁波都市圈</a>[7]</li>
</ul></li>
<li><a href="../Page/珠江三角洲城市群.md" title="wikilink">珠江三角洲城市群</a>
<ul>
<li><a href="../Page/广佛肇都市区.md" title="wikilink">广佛肇都市区</a></li>
<li><a href="../Page/深莞惠都市区.md" title="wikilink">深莞惠都市区</a></li>
<li><a href="../Page/珠中江都市区.md" title="wikilink">珠中江都市区</a></li>
</ul></li>
<li><a href="../Page/京津冀城市群.md" title="wikilink">京津冀城市群</a>
<ul>
<li><a href="../Page/首都都市圈.md" title="wikilink">首都都市圈</a></li>
<li><a href="../Page/石家庄都市圈.md" title="wikilink">石家庄都市圈</a></li>
</ul></li>
<li><a href="../Page/山东半岛城市群.md" title="wikilink">山东半岛城市群</a>
<ul>
<li><a href="../Page/青岛都市圈.md" title="wikilink">青岛都市圈</a></li>
</ul></li>
<li><a href="../Page/辽中南城市群.md" title="wikilink">辽中南城市群</a>
<ul>
<li><a href="../Page/沈阳都市圈.md" title="wikilink">沈阳都市圈</a></li>
</ul></li>
<li><a href="../Page/海峡西岸城市群.md" title="wikilink">海峡西岸城市群</a>
<ul>
<li><a href="../Page/厦门都市圈.md" title="wikilink">厦门都市圈</a></li>
</ul></li>
<li><a href="../Page/长江中游城市群.md" title="wikilink">长江中游城市群</a>
<ul>
<li><a href="../Page/武汉都市圈.md" title="wikilink">武汉都市圈</a></li>
<li><a href="../Page/长沙都市圈.md" title="wikilink">长沙都市圈</a></li>
</ul></li>
<li><a href="../Page/川渝城市群.md" title="wikilink">川渝城市群</a>
<ul>
<li><a href="../Page/成都都市圈.md" title="wikilink">成都都市圈</a></li>
<li><a href="../Page/重庆都市圈.md" title="wikilink">重庆都市圈</a></li>
</ul></li>
</ul></li>
<li>中華民國（臺灣）：
<ul>
<li><a href="../Page/臺北都會區.md" title="wikilink">臺北都會區</a>（臺北市、新北市、基隆市、北桃園）</li>
<li><a href="../Page/臺中都會區.md" title="wikilink">臺中都會區</a>（臺中市、彰化縣）</li>
<li><a href="../Page/臺南都會區.md" title="wikilink">臺南都會區</a></li>
<li><a href="../Page/高雄都會區.md" title="wikilink">高雄都會區</a></li>
</ul></li>
<li>日本：
<ul>
<li><a href="../Page/太平洋工業帶.md" title="wikilink">太平洋沿岸城市群</a>
<ul>
<li><a href="../Page/日本首都圈.md" title="wikilink">日本首都圈</a></li>
<li><a href="../Page/京阪神.md" title="wikilink">京阪神</a></li>
<li></li>
</ul></li>
<li><a href="../Page/福岡都市圈.md" title="wikilink">福岡都市圈</a></li>
<li></li>
</ul></li>
<li>韓國：
<ul>
<li><a href="../Page/首爾.md" title="wikilink">大首爾都會區</a></li>
<li><a href="../Page/釜蔚慶.md" title="wikilink">釜蔚慶</a></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

## 參見

  - [城市群](../Page/城市群.md "wikilink")
  - [都市雇用圈](../Page/都市雇用圈.md "wikilink")
  - [全球城市](../Page/全球城市.md "wikilink")
  - [都市地理學](../Page/都市地理學.md "wikilink")

{{-}}

[Category:城市群](../Category/城市群.md "wikilink")

1.

2.  [Whitehouse.gov](http://www.whitehouse.gov/omb/rewrite/fedreg/msa.html)


3.

4.

5.

6.

7.