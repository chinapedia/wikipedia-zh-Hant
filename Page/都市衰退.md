[WhitmanHouse-CamdenNJ1.jpg](https://zh.wikipedia.org/wiki/File:WhitmanHouse-CamdenNJ1.jpg "fig:WhitmanHouse-CamdenNJ1.jpg")市的衰退街道，該市評比為美國最糟城市\]\]
**城市衰退**（）是一個[城市](../Page/城市.md "wikilink")、或城市的一部分，逐漸變得失修、荒廢的過程。通常伴隨這個過程產生的現象包括[去工業化](../Page/去工業化.md "wikilink")、去[城市化](../Page/城市化.md "wikilink")、[人口流失](../Page/人口不足.md "wikilink")、財產放棄、高[失業率與](../Page/失業率.md "wikilink")[犯罪率](../Page/犯罪.md "wikilink")、家庭分離等。整個城市也因為缺乏建設的關係，呈現出殘破寂寥之景。

造成城市衰退的原因有很多，這些原因彼此也相互影響。例如現代社會，新的[都市計畫](../Page/都市計畫.md "wikilink")\[1\]或[交通](../Page/交通.md "wikilink")[建設](../Page/建設.md "wikilink")\[2\]，使原本位於交通中途點的城市失去功能；或工業社會裡，原本具優勢的[產業逐漸蕭條](../Page/產業.md "wikilink")，而當地社會又未發展出其他足以支撐原經濟規模的新產業\[3\]；也可能是[戰爭或](../Page/戰爭.md "wikilink")[種族糾紛](../Page/種族糾紛.md "wikilink")\[4\]造成人口遷移。

## 參考文獻

<div class="references-small">

<references />

</div>

[sv:Förslumning](../Page/sv:Förslumning.md "wikilink")

[Category:都市衰退](../Category/都市衰退.md "wikilink")
[Category:城市规划](../Category/城市规划.md "wikilink")

1.  *[Urban Sores: On the Interaction Between Segregation, Urban Decay,
    and Deprived
    Neighbourhoods](http://books.google.com/books?id=uf0vS1A-0dkC)* By
    Hans Skifter Andersen. ISBN 0754633055. 2003.
2.  *\[<http://books.google.com/books?id=T4uWAAAACAAJ&dq=the+power+broker>:
    The Power Broker: Robert Moses and the Fall of New York\]* by Robert
    Caro. Page 522.
3.  Lupton, R. and Power, A. (2004) The Growth and Decline of Cities and
    Regions. CASE-Brookings Census Brief No.1
4.  [White Flight: Atlanta and the Making of Modern
    Conservatism](http://press.princeton.edu/chapters/i8043.html)