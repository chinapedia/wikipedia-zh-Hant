《**都柏林人**》（*Dubliners*）是[愛爾蘭籍作家](../Page/愛爾蘭.md "wikilink")[詹姆斯·喬伊斯於](../Page/詹姆斯·喬伊斯.md "wikilink")1914年出版的一本短篇小說集，包含了15篇故事，另有五篇只定了題目，卻未能寫出。前三篇寫童年，青年、成年、公務員時期則各四篇。最後一篇〈往生者〉篇幅最長，也最著名。喬伊斯本人最喜歡〈會議室裡的常春藤日〉〈護花使者〉，〈阿拉比〉則是公認的傑作。

研究愛爾蘭文學的[國立臺灣師範大學莊坤良教授曾提到](../Page/國立臺灣師範大學.md "wikilink")《都柏林人》的寫作基本上是一部「愛爾蘭的道德史」，目的是要「提供一面鏡子，叫愛爾蘭人好好看清楚自己的真實面貌」。\[1\]

## 短篇故事

  - [姐妹倆](../Page/姐妹倆.md "wikilink")（*The Sisters*）–敘述一位神父弗林（Father
    Flynn）死於“瘫痪”，随着深入的阅读會發現到死者其實並非死于肉体上的“瘫痪”，而是死于精神上的“瘫痪”。
  - [偶遇](../Page/偶遇.md "wikilink")（*An Encounter*）–一位逃學的學生面對著性愛的衝擊。
  - [阿拉比](../Page/阿拉比_\(小說\).md "wikilink")（Araby）–一個男孩暗戀朋友的姐姐，想在周末前往“阿拉比集市”為她選擇禮物，但遲遲沒拿到錢，最後也沒買到東西。
  - [伊夫琳](../Page/伊夫琳.md "wikilink")（*Eveline*）–一位年輕少女伊夫琳要和男友私奔，逃離讓她依戀又恐懼的家，前往新大陸開始新生活。但在上船前的一刻，她放棄了。
  - [賽車之後](../Page/賽車之後.md "wikilink")（*After the Race*）–一位學院學生吉米（Jimmy
    Doyle）試著去適應他那有錢的朋友。
  - [護花使者](../Page/護花使者.md "wikilink")（*Two
    Gallants*）–兩個騙子連罕（Lenehan）和可林（Corley）設計一位少女從她老闆身上偷窺東西。
  - [寄宿家庭](../Page/寄宿家庭_\(小说\).md "wikilink")（*The Boarding
    House*）–摩尼（Moorney）女士成功的策劃她的女兒波莉（Polly）跟杜蘭（Doran）先生結婚。
  - [小雲朵](../Page/小雲朵.md "wikilink")（*A Little Cloud*）–小錢德勒（Little
    Chandler）和他的老友蓋勒（Ignatius
    Gallaher）吃午餐，透過雲霧的朦朧，蓋勒對小錢說了異國風光的故事，小錢喝點[威士忌後](../Page/威士忌.md "wikilink")，開始回憶著他失敗的文學夢想（literary
    dreams）。小錢回家之後，感覺到都柏林的景象與生活方式，更顯得毫無生氣，他對著兒子吼叫，小嬰兒哇啦哭起來。這時妻子開門進來大罵，問他為什麼沒把小孩子照顧好，兒子在妻子的安慰下停止了哭聲。小錢這時流下悔恨的淚水。
  - [一對一](../Page/一對一_\(小說\).md "wikilink")（*Counterparts*）–華林頓（Farrington）一位窩囊的銷售員，在辦公室被主管責罵，在酒吧跟一位年輕人比腕力輸了，只好回家打小孩。他可憐的兒子湯姆（Tom）屁股被打得皮肉綻開，只能向萬福瑪麗亞祈禱。
  - [泥土](../Page/泥土_\(小說\).md "wikilink")（*Clay*）–一名少女瑪麗亞（Maria）跟她的過去的好友喬（Joe）與喬的朋友阿爾菲慶祝萬聖節。
  - [憾事一樁](../Page/憾事一樁.md "wikilink")（*A Painful
    Case*）–杜飛（Duffer）先生回絕了西寧可（Sinico）女士，四年後他才確定那是他失去一生中最重要的一次愛情。
  - [會議室裡的常春藤日](../Page/會議室裡的常春藤日.md "wikilink")（*Ivy Day in the
    Committee Room*）–一位愛爾蘭政客把大量的錢投入到愛爾蘭的無冕王巴內爾（Charles Parnell）的逝世紀念日上。
  - [一位母親](../Page/一位母親.md "wikilink")（*A
    Mother*）–卡內女士（Kearney）試著為她的女兒凱莉（Kathleen）舉辦一場成功的鋼琴演奏會。
  - [神的恩典](../Page/神的恩典.md "wikilink")（*Grace*）–柯南先生（Kernan）遭受到生活上的不如意，他的朋友試著讓他重新站起來。
  - [往生者 (短篇小說)](../Page/往生者_\(短篇小說\).md "wikilink")（*The
    Dead*）–描繪陸續抵達的客人在享用聖誕節大餐的人，主角加布里埃爾透過這場「愛爾蘭盛宴」的人雖然還活著，其實精神上已經癱瘓了，不過是些活死人罷了。一些批評家認為這篇故事，是要寫犯下[基督教七罪之一](../Page/基督教.md "wikilink")「饕餮」。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [Dubliners全中譯文](http://www.eng.ntnu.edu.tw/teacher/kunliang/)

[D](../Category/愛爾蘭小說.md "wikilink") [D](../Category/都柏林.md "wikilink")
[D](../Category/當代小說.md "wikilink")

1.  [愛爾蘭文學與國家想像](http://tw.myblog.yahoo.com/kunliang2006/article?mid=143&prev=152&next=142&l=f&fid=6)