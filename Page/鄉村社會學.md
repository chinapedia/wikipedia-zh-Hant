**鄉村社會學**是[社會學的分支之一](../Page/社會學.md "wikilink")，以非都市地區內的社會生活為研究範疇。更仔細來說，它是一個關於遠離人口密集地區或經濟活動地區的社會組織和行為的科學研究。正如其他社會學分支，鄉村社會學包括了統計數據檢查、[面談](../Page/面談.md "wikilink")、[社會理論](../Page/社會理論.md "wikilink")、觀察、調查研究和其他技巧。

與鄉村社會學相反的就是[都市社會學](../Page/都市社會學.md "wikilink")。

[農業綜合企業是鄉村社會學其中一個焦點](../Page/農業綜合企業.md "wikilink")，還有很多研究範疇針對農產品的經濟。其它研究包括了鄉村移民、[人口統計學](../Page/人口統計學.md "wikilink")、[環境社會學](../Page/環境社會學.md "wikilink")、[休閒度假城](../Page/休閒度假城.md "wikilink")、公共土地政策、[新興都市](../Page/新興都市.md "wikilink")、[社會瓦解](../Page/社會瓦解.md "wikilink")、鄉村健康護理及教育政策。

## 鄉村定義

社會學家定義鄉村為非市區地方。雖然美國鄉村社會學家常用美國人口調查局的定義，即人口密度低於每平方里一千人，但兩者分野仍然比較隨意。\[[https://web.archive.org/web/20131014182213/http://www.census.gov/geo/www/ua/ua_2k.html\]2000年的人口調查報告21%美國家庭（59,274,000人）位於鄉村](https://web.archive.org/web/20131014182213/http://www.census.gov/geo/www/ua/ua_2k.html%5D2000年的人口調查報告21%美國家庭（59,274,000人）位於鄉村)。[1](http://www.fhwa.dot.gov/planning/census/cps2k.htm)

## 美國鄉村問題

美國的鄉村經濟趨勢是十分複雜的，正如其他地區面對經濟下滑和[農村人口遷出](../Page/農村人口遷出.md "wikilink")，而海岸及山區則面對經濟刺激與新居民流入。雖然某些地區的很多傳統鄉村工業譬如採礦業、大牧場、農業等等被新興的[資訊科技](../Page/資訊科技.md "wikilink")、[休閒度假城](../Page/休閒度假城.md "wikilink")、[旅遊業](../Page/旅遊業.md "wikilink")、[藝術所取代](../Page/藝術.md "wikilink")，但大部分已經在經濟上不可行的。這些工業為了一些地區帶來經濟增長，但一些經濟衰退地區帶來[社會分化](../Page/社會分化.md "wikilink")。\[1\]\[2\]

近來，鄉村資本向市區或名為山間西部的鄉村縣、[歐薩克](../Page/歐薩克.md "wikilink")、海岸地區、[內布拉斯加州的](../Page/內布拉斯加州.md "wikilink")[I-80縣及](../Page/I-80.md "wikilink")[肯薩斯城市都會區](../Page/肯薩斯城市都會區.md "wikilink")。財富增長集中在市區、交通走廊。

## 中國鄉村問題

## 主要課題

  - [農業綜合企業](../Page/農業綜合企業.md "wikilink")
  - [角色同質](../Page/角色同質.md "wikilink")
  - [農村人口遷出](../Page/農村人口遷出.md "wikilink")
  - [鄉村社群發展](../Page/鄉村社群發展.md "wikilink")

## 參見

  - [鄉村社會學重要著作列表](../Page/鄉村社會學重要著作列表.md "wikilink")
  - [格蘭其運動](../Page/格蘭其運動.md "wikilink")
  - [摩里法案](../Page/摩里法案.md "wikilink") （Morrill Act）
  - [亨利·泰勒](../Page/亨利·泰勒.md "wikilink")
  - [喬納森·鮑德溫·杜爾哥](../Page/喬納森·鮑德溫·杜爾哥.md "wikilink")
  - [美國農業部](../Page/美國農業部.md "wikilink")
  - [喬治·沃倫](../Page/喬治·沃倫.md "wikilink")
  - [鄉村健康](../Page/鄉村健康.md "wikilink")

## 參考文獻

  - Agricultural Community to the North of the Tagus - Study of
    Agricultural Sociology, Moisés Espírito Santo, 1999 (1980). École
    des Hautes Études en Sciences Sociales, Paris; Association of Rural
    Studies of the New University of Lisbon; Portugal; European Union, .

## 外部連結

  - [The Rural Sociological Society](http://www.ruralsociology.org)

  - [The International Rural Sociology
    Society](http://www.irsa-world.org)

  - [The Department of Rural Sociology at The University of
    Wisconsin](https://web.archive.org/web/20140413143513/http://www.drs.wisc.edu/)

[Category:社會學分支](../Category/社會學分支.md "wikilink")
[Category:农村](../Category/农村.md "wikilink")

1.  [The Rural Rebound: Recent Nonmetropolitan Demographic Trends in the
    United
    States](http://www.luc.edu/depts/sociology/johnson/p99webn.html)
2.  [Amercian Center for the West](http://www.centerwest.org)