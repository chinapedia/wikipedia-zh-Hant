**鄧兆棠**（，），[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗](../Page/元朗.md "wikilink")[廈村](../Page/廈村.md "wikilink")[新圍](../Page/新圍_\(廈村\).md "wikilink")[原居民](../Page/新界原居民.md "wikilink")，鄉事派代表。現任[全國政協委員](../Page/全國政協.md "wikilink")，曾任[立法會議員](../Page/香港立法會.md "wikilink")、[元朗區議會主席](../Page/元朗區議會.md "wikilink")、[港事顧問](../Page/港事顧問.md "wikilink")、[推選委員會委員](../Page/推選委員會.md "wikilink")、[博愛醫院永遠顧問及前任主席](../Page/博愛醫院.md "wikilink")、市政服務上訴委員會委員、行政上訴委員會委員、[香港大學校董會成員](../Page/香港大學.md "wikilink")、[古物諮詢委員會成員](../Page/古物諮詢委員會.md "wikilink")。

## 簡歷

曾任[元朗青年商會創會會長](../Page/元朗青年商會.md "wikilink")（1977年）、元朗區議員（1980年 -
1991年）、[區域市政局議員](../Page/區域市政局.md "wikilink")（1986年 -
1988年）、立法局議員（1992年 -
1995年）、臨時區域市政局議員、[臨時立法會議員](../Page/臨時立法會.md "wikilink")、立法會議員（2000年
- 2004年）。2005年起任第十屆、十一屆[全國政協委員](../Page/全國政協.md "wikilink")。

在[第一屆香港立法會](../Page/第一屆香港立法會.md "wikilink")（1998年至2000年），鄧兆棠議員曾被裁定當選無效，補選的結果依然由鄧兆棠勝出。

他本身為註冊[西醫](../Page/西醫.md "wikilink")，在元朗區開設醫務所。

畢業於[元朗公立中學](../Page/元朗公立中學.md "wikilink")，並為校友會之永遠名譽會長。該校友會開辦之第一所中學以鄧兆棠冠名——[元朗公立中學校友會鄧兆棠中學](../Page/元朗公立中學校友會鄧兆棠中學.md "wikilink")。

## 名下馬匹

鄧兆棠是香港賽馬會的會員，名下馬匹包括勝利先生、風起雲揚、朗星。

## 榮譽

  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（1986年）
  - [銀紫荊星章](../Page/銀紫荊星章.md "wikilink")（2007年）

[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:香港醫學界人士](../Category/香港醫學界人士.md "wikilink")
[T](../Category/前香港立法局議員.md "wikilink")
[Category:前香港立法會議員](../Category/前香港立法會議員.md "wikilink")
[Category:全國政協香港委員](../Category/全國政協香港委員.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:香港鄧族](../Category/香港鄧族.md "wikilink")
[Category:前區域市政局議員](../Category/前區域市政局議員.md "wikilink")
[Category:前香港協進聯盟成員](../Category/前香港協進聯盟成員.md "wikilink")
[T](../Category/元朗公立中學校友.md "wikilink")
[S](../Category/鄧姓.md "wikilink")
[Category:民主建港協進聯盟成員](../Category/民主建港協進聯盟成員.md "wikilink")
[Category:鄉議局成員](../Category/鄉議局成員.md "wikilink")
[Category:香港新界原居民](../Category/香港新界原居民.md "wikilink")
[T](../Category/阿德雷得大學校友.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")