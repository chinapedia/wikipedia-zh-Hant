[Deng_BaoShan_1968.jpg](https://zh.wikipedia.org/wiki/File:Deng_BaoShan_1968.jpg "fig:Deng_BaoShan_1968.jpg")
**鄧寶珊**
（），原名邓瑜，[甘肅省](../Page/甘肅省.md "wikilink")[天水市人](../Page/天水市.md "wikilink")。中國近代政治人物、地方軍閥派系西北軍將領。

## 生平

16歲之時，在[新疆參加](../Page/新疆.md "wikilink")[同盟會](../Page/同盟會.md "wikilink")，17歲參加[伊犁起義](../Page/伊犁起義.md "wikilink")。在[陝西參加討伐](../Page/陝西.md "wikilink")[袁世凱](../Page/袁世凱.md "wikilink")。1918年在[陝西](../Page/陝西.md "wikilink")[三原跟](../Page/三原.md "wikilink")[胡景翼創立](../Page/胡景翼.md "wikilink")[靖國軍](../Page/靖國軍.md "wikilink")，又参加[馮玉祥的](../Page/馮玉祥.md "wikilink")[國民革命軍](../Page/國民革命軍.md "wikilink")，曾經招收[共產黨人訓練成幹部](../Page/中国共产党员.md "wikilink")，例如[鄧小平也曾受他賞識提拔](../Page/鄧小平.md "wikilink")。1932年任[新一軍](../Page/新一軍.md "wikilink")[軍長](../Page/軍長.md "wikilink")。

1936年[西安事變](../Page/西安事變.md "wikilink")，他支持[張學良與](../Page/張學良.md "wikilink")[楊虎城的](../Page/楊虎城.md "wikilink")[八項主張](../Page/八項主張.md "wikilink")。作為地方實力派軍閥，鄧控制地區與中共所謂陝甘寧邊區控制區接壤。1948年[平津戰役時期](../Page/平津戰役.md "wikilink")，鄧寶珊當[解放軍與](../Page/中国人民解放军.md "wikilink")[北平](../Page/北平.md "wikilink")[傅作義之間的調停人](../Page/傅作義.md "wikilink")，又當傅作義的全權代表，達成協議，北平開城投降，保住了北京歷史文化都城。

1949年[中华人民共和国成立後](../Page/中华人民共和国.md "wikilink")，历任[西北軍政委員會委員](../Page/西北軍政委員會.md "wikilink")、[國防委員會委員](../Page/中华人民共和国国防委员会.md "wikilink")、[甘肅省人民政府主席](../Page/甘肅省.md "wikilink")、省長、[中國國民黨革命委員會常委](../Page/中國國民黨革命委員會.md "wikilink")，獲一級[解放勛章](../Page/解放勛章.md "wikilink")。

[文化大革命時](../Page/文化大革命.md "wikilink")，遭受[紅衛兵的衝擊](../Page/紅衛兵.md "wikilink")。[1966年](../Page/1966年.md "wikilink")11月下旬，一批以北京中學生為主的紅衛兵，闖進鄧寶珊居住的「鄧園」，到各房間搜查後找到一把刻有「蔣中正贈」字樣的[佩劍](../Page/佩劍.md "wikilink")，當成[反動的證據](../Page/反動.md "wikilink")，把臥病在床的鄧寶珊從床上拉起來，罰他跪在地上，紅衛兵並拔出劍，架在鄧寶珊的脖子上進行[批鬥](../Page/批鬥.md "wikilink")，質問他「是不是[反動派](../Page/反動派.md "wikilink")？打過[紅軍沒有](../Page/紅軍.md "wikilink")？殺過共產黨沒有？」幾天後[周恩來總理知道此事](../Page/周恩來.md "wikilink")，派專機把鄧寶珊將軍接到北京，安排住進了醫院\[1\]\[2\]。

[1968年](../Page/1968年.md "wikilink")[11月27日](../Page/11月27日.md "wikilink")，鄧寶珊將軍在北京逝世，此後經周恩來努力，鄧寶珊的名字位列[八寶山革命公墓](../Page/八寶山革命公墓.md "wikilink")。

## 参考文献

## 個人生活

鄧寶珊雖然出身[行伍](../Page/行伍.md "wikilink")，只是念過[私塾](../Page/私塾.md "wikilink")，但是他愛知識，有[文學素養](../Page/文學.md "wikilink")，堅持自學，又時常與[文化人切磋心得](../Page/文化人.md "wikilink")，品[詩論](../Page/詩.md "wikilink")[畫](../Page/畫.md "wikilink")，所以有儒將之稱。鄧寶珊曾經出資支持天水學者出書，也曾捐資修繕[紀將軍祠](../Page/紀將軍祠.md "wikilink")、[麥積山](../Page/麥積山.md "wikilink")[石窟](../Page/石窟.md "wikilink")[棧道等](../Page/棧道.md "wikilink")。兰州邓家花园是其故居。

## 外部链接

  - [天水網相关报道](http://www.tianshui.com.cn/news/tianshui/200508120522139181.htm)

{{-}}

[D](../Category/中華民國大陸時期政治人物.md "wikilink")
[D](../Category/中華民國大陸時期軍事人物.md "wikilink")
[D](../Category/中华人民共和国国防委员会委员.md "wikilink")
[D](../Category/文革受难者.md "wikilink")
[D](../Category/天水人.md "wikilink")
[B宝珊](../Category/邓姓.md "wikilink")
[Category:甘肅省政協主席](../Category/甘肅省政協主席.md "wikilink")

1.  [邓园往事-中国社会科学网](http://www.cssn.cn/zgs/zgs_sb/201501/t20150109_1472591_1.shtml)
2.  [邓宝珊将军文革蒙难记](http://www.yhcqw.com/28/1725.html)