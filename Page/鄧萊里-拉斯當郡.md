**鄧萊里-拉斯當郡** （County of Dun
Laoghaire-Rathdown，[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：Dún
Laoghaire–Ráth an
Dúin），是[愛爾蘭的一郡](../Page/愛爾蘭共和國.md "wikilink")，歷史上屬[倫斯特省](../Page/倫斯特省.md "wikilink")[都柏林郡](../Page/都柏林郡.md "wikilink")，1994年成立。位於[愛爾蘭島東部](../Page/愛爾蘭島.md "wikilink")[愛爾蘭海岸](../Page/愛爾蘭海.md "wikilink")。面積127.31
km² （全國最小），2006年人口193,668人。首府[鄧萊里](../Page/鄧萊里.md "wikilink")。

## 外部連結

  - [Dún Laoghaire-Rathdown County Council](http://www.dlrcoco.ie/)
  - [Dún Laoghaire-Rathdown County Enterprise
    Board](http://www.dlrceb.ie/)
  - [Dún Laoghaire-Rathdown Tourism](http://www.dlrtourism.com/)

[\*](../Category/鄧萊里-拉斯當郡.md "wikilink")
[Category:都柏林郡](../Category/都柏林郡.md "wikilink")
[Category:愛爾蘭共和國的郡](../Category/愛爾蘭共和國的郡.md "wikilink")