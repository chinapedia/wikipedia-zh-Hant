**鄭海泉**（，），[香港銀行家](../Page/香港.md "wikilink")，曾任[香港上海滙豐銀行亞太區主席](../Page/香港上海滙豐銀行.md "wikilink")。他是[滙豐控股有限公司首位](../Page/滙豐控股有限公司.md "wikilink")[華人執行董事](../Page/華人.md "wikilink")。

## 簡歷

1948年鄭海泉出生於一個6口之家，是家中長子，父母為水果小販。3歲時，鄭海泉患上[小兒麻痹症](../Page/小兒麻痹症.md "wikilink")。

1970年代，鄭海泉考入[香港中文大學](../Page/香港中文大學.md "wikilink")[新亞書院經濟學系](../Page/新亞書院.md "wikilink")，其後往[紐西蘭](../Page/紐西蘭.md "wikilink")[奧克蘭大學修讀經濟學系哲學碩士學位](../Page/奧克蘭大學.md "wikilink")。

1978年，鄭海泉加入香港上海滙豐銀行集團財務部，後來其主管現居美國的胡先生透露一件鮮為人知的事，鄭面試職位時，本遜於另一位港大求職者，但鄭幸得一位曾在學生會共事的中大同學認為鄭該有社會良心，故暗中極力推薦，鄭才得以從此平步青雲；曾出任首席經濟研究員、經濟及業務策略研究部高級經理、財務主管及總經理等職位。

1988年10月9日，獲委任為[立法局委任議員](../Page/立法局.md "wikilink")。\[1\]

1995年，獲委任為滙控集團總經理及香港上海滙豐銀行執行董事，是首位出任該職位的華人。[港督](../Page/港督.md "wikilink")[彭定康委任鄭海泉為](../Page/彭定康.md "wikilink")[行政局議員](../Page/行政局.md "wikilink")。

1998年，調任[恒生銀行副董事長兼行政總裁](../Page/恒生銀行.md "wikilink")。

2005年5月，鄭海泉接替退休的[艾爾敦](../Page/艾爾敦.md "wikilink")，出任[香港上海滙豐銀行有限公司主席](../Page/香港上海滙豐銀行.md "wikilink")，成為滙控集團創立139年來，首位擔任此職務的華人，也是在滙控集團內職位最高的華人。同時，他亦出任滙控集團常務總監，以及[太古公司董事](../Page/太古公司.md "wikilink")。

2007年，[滙豐銀行（中國）有限公司成立](../Page/滙豐銀行\(中國\)有限公司.md "wikilink")，鄭海泉出任該行的[董事長](../Page/董事長.md "wikilink")。

2008年2月，他獲委任為滙豐控股有限公司執行董事。

2009年7月，他獲委任為[香港鐵路有限公司獨立非執行董事](../Page/香港鐵路有限公司.md "wikilink")。

2009年9月，他獲委任為[香港中文大學校董會主席](../Page/香港中文大學.md "wikilink")，同年10月24日就任。

2009年9月25日，滙豐銀行宣佈鄭海泉將卸任香港上海滙豐銀行亞太區主席，但仍然擔任執行董事、滙豐中國[董事長兼滙豐](../Page/董事長.md "wikilink")[台灣區主席](../Page/台灣.md "wikilink")。

2011年2月25日，滙豐銀行宣佈鄭海泉於同年五月退休。

2011年5月28日，鄭海泉正式退任执行董事职务\[2\]。

## 管理哲學

鄭海泉在恒生工作的6年半時間裏，恒生的業務穩步發展，股價由1998年的30多元猛漲至目前的100多元。鄭海泉的管理哲學是視僱員為老闆，提倡下屬評價老闆的工作。最令他自豪的是，即使經歷了香港的經濟低谷，恒生在他的領導下，卻沒有裁過一個僱員。他不斷為僱員提供內部培訓，以便他們能適應新的工作。
"Don't live in the past"是鄭海泉的格言。他認為即使身為CEO，也會作出過錯誤決定，最緊要千萬不可讓這些過錯變成絆腳石。

## 教席

他亦曾於2000年及2001年獲[浙江大學及](../Page/浙江大學.md "wikilink")[深圳大學委任為客座](../Page/深圳大學.md "wikilink")[教授](../Page/教授.md "wikilink")，2000年獲委為[西南財經大學榮譽教授](../Page/西南財經大學.md "wikilink")。

## 曾任公職

  - [香港政府](../Page/香港政府.md "wikilink")[中央政策組成員](../Page/中央政策組.md "wikilink")
    (1989年-1991年)
  - [立法局委任議員](../Page/立法局.md "wikilink") (1988年-1995年)
  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")[港事顧問](../Page/港事顧問.md "wikilink")
    (1994年-1997年)
  - [行政局議員](../Page/行政局.md "wikilink") (1995年-1997年)
  - [恒生商學書院校董會主席](../Page/恒生商學書院.md "wikilink") (1998年-2005年)
  - [香港特別行政區行政會議成員及立法會議員薪津獨立委員會成員](../Page/香港特別行政區行政會議.md "wikilink")
    (2002年-2006年)
  - [強制性公積金計劃諮詢委員會主席](../Page/強制性公積金計劃.md "wikilink")
  - [香港工商專業聯會副主席](../Page/香港工商專業聯會.md "wikilink")
  - [香港芭蕾舞團董事](../Page/香港芭蕾舞團.md "wikilink")
  - [創新科技委員會成員](../Page/創新科技委員會.md "wikilink")
  - [經濟諮詢委員會成員](../Page/經濟諮詢委員會.md "wikilink")
  - [工業及科技發展局成員](../Page/工業及科技發展局.md "wikilink")
  - [香港紅十字會顧問團委員](../Page/香港紅十字會.md "wikilink")
  - [香港中文大学校董会主席](../Page/香港中文大学.md "wikilink")(2009年－2015年)

## 現任公職

  - [香港中文大學](../Page/香港中文大學.md "wikilink")[新亞書院校董](../Page/新亞書院.md "wikilink")
    (1998年- )
  - 衛奕信勳爵文物信託受託人委員會主席 (2005年- )
  - [香港特別行政區行政會議成員及立法會議員薪津獨立委員會主席](../Page/香港特別行政區行政會議.md "wikilink")
    (2006年- )
  - 期貨事務監察委員會程序覆檢委員會主席
  - [證券及期貨事務監察委員會之程序覆檢委員會委員](../Page/證券及期貨事務監察委員會.md "wikilink")
  - 香港特別行政區政府之首長級薪俸及服務條件常務委員會委員
  - [香港銀行學會副會長](../Page/香港銀行學會.md "wikilink")
  - 北京市政協委員
  - [香港金融管理局土地基金諮詢委員會委員](../Page/香港金融管理局.md "wikilink")
  - [長遠房屋策略諮詢委員會委員](../Page/長遠房屋策略諮詢委員會.md "wikilink")
  - [選舉委員會](../Page/選舉委員會_\(香港\).md "wikilink")（金融界別分組）委員會成員
  - [香港公益金名譽副會長](../Page/香港公益金.md "wikilink")
  - [中央政策組](../Page/中央政策組.md "wikilink")[策略發展委員會成員](../Page/策略發展委員會.md "wikilink")
  - [香港復康會名譽副會長](../Page/香港復康會.md "wikilink")
  - [海港商界論壇發言人](../Page/海港商界論壇.md "wikilink")

## 獲得榮譽

  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")(JP)
  - [英帝國官佐勳章](../Page/英帝國官佐勳章.md "wikilink")(OBE) (1994年)
  - [香港董事學會頒發](../Page/香港董事學會.md "wikilink")「傑出董事獎」(2004年)
  - [金紫荊星章](../Page/金紫荊星章.md "wikilink")(GBS) (2005年)
  - [香港中文大學頒授榮譽院士](../Page/香港中文大學.md "wikilink") (2002年)
  - [香港公開大學頒授榮譽工商管理博士學位](../Page/香港公開大學.md "wikilink") (2005年)
  - [香港中文大學頒授榮譽社會科學博士學位](../Page/香港中文大學.md "wikilink") (2005年)

## 參考資料

<references />

## 外部連結

  - [積極開拓內地市場
    滙豐銀行任命首位華人主席](https://web.archive.org/web/20090821173901/http://news.xinhuanet.com/overseas/2004-12/10/content_2316967.htm)
  - [榮譽香港中文大學社會科學博士鄭海泉先生讚辭](http://www.cuhk.edu.hk/cpr/pressrelease/051208Cheng_c.htm)
  - [香港金管局鄭海泉簡歷](http://www.info.gov.hk/hkma/eng/hkma/advisory/vincent_b.htm)

[Category:OBE勳銜](../Category/OBE勳銜.md "wikilink")
[Category:香港財經界人士](../Category/香港財經界人士.md "wikilink")
[Category:香港銀行家](../Category/香港銀行家.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:香港公開大學榮譽博士](../Category/香港公開大學榮譽博士.md "wikilink")
[Category:汇丰人物](../Category/汇丰人物.md "wikilink")
[Category:全国政协香港委员](../Category/全国政协香港委员.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:北京市政协委员](../Category/北京市政协委员.md "wikilink")
[Category:前香港行政局議員](../Category/前香港行政局議員.md "wikilink")
[Category:前香港立法局議員](../Category/前香港立法局議員.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Category:奧克蘭大學校友](../Category/奧克蘭大學校友.md "wikilink")
[Category:九龍工業學校校友](../Category/九龍工業學校校友.md "wikilink")
[H](../Category/鄭姓.md "wikilink")

1.  《立法局議員逐個捉》，葉根銓，明報出版社，p100
2.  劉美儀,\[[https://hk.finance.appledaily.com/finance/daily/article/20110524/15279885"滙豐首位華人大班榮休](https://hk.finance.appledaily.com/finance/daily/article/20110524/15279885%22滙豐首位華人大班榮休)　鄭海泉話當年「俾浦偉士鬧　仍敬愛他」"\],*蘋果日報*,2011年5月24日