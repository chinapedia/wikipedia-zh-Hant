**鄭用錫**（；），[譜名](../Page/譜名.md "wikilink")**文衍**，又名**蕃**，字**在中**，號**祉亭**，[清朝政治人物](../Page/清朝.md "wikilink")，[臺灣](../Page/臺灣.md "wikilink")[淡水廳](../Page/淡水廳.md "wikilink")[竹塹](../Page/竹塹.md "wikilink")（今[新竹市](../Page/新竹市.md "wikilink")）人，祖籍[福建省](../Page/福建省.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[同安縣](../Page/同安縣.md "wikilink")[浯江](../Page/金門島.md "wikilink")（今[金門縣](../Page/金門縣.md "wikilink")）。道光初以科舉入仕，有「開臺進士」之譽。官至[禮部](../Page/禮部.md "wikilink")[員外郎](../Page/員外郎.md "wikilink")，後因功誥授二品[通奉大夫](../Page/清代散階封贈表.md "wikilink")，葬於竹子坑。

## 生平

鄭用錫之先祖於[明朝末年由福建](../Page/明朝.md "wikilink")[漳州府](../Page/漳州府.md "wikilink")[漳浦遷居](../Page/漳浦.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[同安縣](../Page/同安縣.md "wikilink")[金門](../Page/金門.md "wikilink")。[乾隆四十年](../Page/乾隆.md "wikilink")（1775年），其祖父鄭國唐攜子[崇和渡海至臺](../Page/鄭崇和.md "wikilink")，在[淡水廳](../Page/淡水廳.md "wikilink")[後龍](../Page/後龍鎮_\(臺灣\).md "wikilink")（今屬[苗栗縣](../Page/苗栗縣.md "wikilink")）居住。崇和後成[博士弟子員](../Page/博士弟子員.md "wikilink")。乾隆五十三年（1788年），鄭用錫生於後龍。[嘉慶十一年](../Page/嘉慶.md "wikilink")（1806年），全家遷居[竹塹](../Page/竹塹.md "wikilink")（今屬[新竹市](../Page/新竹市.md "wikilink")）。

鄭用錫自幼穎異，通曉[經](../Page/經.md "wikilink")[史](../Page/史.md "wikilink")，尤其精於《[易經](../Page/易經.md "wikilink")》。曾主[明志書院講席](../Page/明志書院.md "wikilink")。[嘉庆二十三年](../Page/嘉庆.md "wikilink")（1818年）戊寅科鄉試中[舉人](../Page/舉人.md "wikilink")。[道光三年](../Page/道光.md "wikilink")（1823年）赴京參加癸未科[會試中式](../Page/會試.md "wikilink")，[殿試位列三甲第一百零九名](../Page/殿試.md "wikilink")，賜同進士出身。[臺灣入清一百餘年來](../Page/台灣清治時期.md "wikilink")，臺灣[籍貫的考生首次登科](../Page/籍貫.md "wikilink")，當地以為盛事，鄭用錫遂有「開臺[進士](../Page/進士.md "wikilink")」或「開臺[黃甲](../Page/進士.md "wikilink")」之譽。

[Taiwan_Hsinchu_YinXi_Gate.JPG](https://zh.wikipedia.org/wiki/File:Taiwan_Hsinchu_YinXi_Gate.JPG "fig:Taiwan_Hsinchu_YinXi_Gate.JPG")
道光六年（1826年），鄭用錫與[臺灣府淡水撫民同知](../Page/臺灣府淡水撫民同知.md "wikilink")[李慎彝等稟請改建淡水廳城](../Page/李慎彝.md "wikilink")，即[竹塹城](../Page/竹塹城.md "wikilink")，并獲准將原來的土牆改爲石砌。工程於次年展開，由[臺灣道](../Page/臺灣道.md "wikilink")[道員](../Page/道員.md "wikilink")[孔昭虔親自履勘](../Page/孔昭虔.md "wikilink")，歷時兩年完成。用錫督建城池有功，加[同知銜](../Page/同知.md "wikilink")。

道光十四年（1834年）用錫[捐京官](../Page/捐納.md "wikilink")，再次前往[京師](../Page/京師.md "wikilink")，籖分[兵部武選司任職](../Page/兵部.md "wikilink")，補授[禮部鑄印局](../Page/禮部.md "wikilink")[員外郎](../Page/員外郎.md "wikilink")。因不習官場應酬，於道光十七年（1837年）侍親為由，請求歸養老母，返鄉後，用錫開始興建自宅，即「進士第」。

道光二十二年（1842年），[英國](../Page/英國.md "wikilink")[海軍侵犯大安口](../Page/海軍.md "wikilink")，鄭用錫率先募[鄉勇援救](../Page/鄉勇.md "wikilink")，受賞[花翎](../Page/花翎.md "wikilink")。不久又以剿敌之功加四品銜。

[咸豐元年](../Page/咸丰_\(年号\).md "wikilink")（1851年），鄭用錫開始在[竹塹城北修築](../Page/竹塹.md "wikilink")「[北郭園](../Page/北郭園.md "wikilink")」以自娛。

咸豐三年（1853年），[臺北發生](../Page/臺北.md "wikilink")[頂下郊拚等](../Page/頂下郊拚.md "wikilink")[械鬥](../Page/械鬥.md "wikilink")（1852年就已發生[泉漳械鬥](../Page/泉漳械鬥.md "wikilink")），用錫親與[臺北](../Page/臺北.md "wikilink")[仕紳](../Page/仕紳.md "wikilink")[陳維英一起主持和解事宜](../Page/陳迂谷.md "wikilink")，活人甚多。

咸豐四年（1854年），用錫奉旨與進士[施瓊芳等協辦](../Page/施瓊芳.md "wikilink")[團練](../Page/團練.md "wikilink")，并助捐米糧，獲得二品封典。並寫下《[勸和論](../Page/s:勸和論.md "wikilink")》，告諭臺灣百姓以和為貴，勿[分類械鬥](../Page/分類械鬥.md "wikilink")。

咸豐八年（1858年）病卒，年七十。入祀鄉賢祠。[同治](../Page/同治.md "wikilink")《淡水廳志》有傳。\[1\]

## 開臺進士

鄭用錫是[臺灣納入清朝版圖後第一位臺灣本籍](../Page/臺灣清治時期.md "wikilink")[進士](../Page/進士.md "wikilink")，有「開臺進士」、「開臺[黃甲](../Page/黃甲.md "wikilink")」之譽。在其之前，臺灣已有[陳夢球](../Page/陳夢球.md "wikilink")、[王克捷](../Page/王克捷.md "wikilink")、[莊文進三人考取進士](../Page/莊文進.md "wikilink")，然而陳夢球屬於[旗人](../Page/旗人.md "wikilink")，[籍貫為](../Page/籍貫.md "wikilink")[福建](../Page/福建.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[同安縣人](../Page/同安縣.md "wikilink")，以[漢軍](../Page/漢軍八旗.md "wikilink")[正白旗籍中舉](../Page/正白旗.md "wikilink")，故有一說稱王克捷為「開臺進士」，莊文進為「開鳳進士」，鄭用錫為「開淡進士」\[2\]\[3\]，但王克捷為[福建省](../Page/福建省.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[晉江縣籍](../Page/晉江縣.md "wikilink")，移居臺灣寄籍[諸羅縣](../Page/諸羅縣.md "wikilink")，莊文進以[臺灣鳳山縣籍登科後](../Page/臺灣鳳山縣.md "wikilink")，又回歸祖籍[晉江縣](../Page/晉江縣.md "wikilink")，王、莊都有冒籍和寄籍之嫌，因此「開臺進士」之說依然是以鄭用錫為主。

康熙三十三年（1694年）甲戌科武進士榜（[曹曰瑋榜](../Page/曹曰瑋.md "wikilink")），[臺灣府](../Page/臺灣府.md "wikilink")[臺灣縣人阮洪義](../Page/臺灣縣.md "wikilink")，考取第三甲第三十三名，為臺灣首位武進士。

## 鄉賢

清代臺灣的鄉賢入祀，只有五人，新竹鄭家就佔了三位，不僅是臺灣史上僅有，即使清代其他府縣，恐怕也少有其匹。鄭用錫是崇和之子，鄭用鑑則是崇和之侄。乃以「父子兄弟叔侄鄉賢」與「開臺黃甲」並傳為佳話。

[1](https://www.th.gov.tw/epaper/site/page/127/1829)

## 著作

鄭用錫精通[儒家](../Page/儒家.md "wikilink")[經學](../Page/經學.md "wikilink")，著有《周禮解疑》及《周易折中衍義》。因淡水廳尚未有[志書](../Page/地方誌.md "wikilink")，曾自纂《淡水廳初志稿》，雖未刊行，卻成為日後陳培桂編寫《淡水廳志》的重要參考。\[4\]用錫工詩文，好吟詠，與當地文士多有唱和。

同治九年（1870年），次子鄭如梁委託福建舉人楊浚整理用錫詩文遺稿，刊刻《[北郭園全集](../Page/北郭園全集.md "wikilink")》十卷行世。楊浚對其詩評價極高，稱「其品格在晉為[陶靖節](../Page/陶淵明.md "wikilink")、在唐為[白樂天](../Page/白居易.md "wikilink")、在宋為[邵堯夫](../Page/邵雍.md "wikilink")，間有逼肖[元遺山者](../Page/元好問.md "wikilink")。」\[5\]

鄭用錫以《[勸和論](../Page/s:勸和論.md "wikilink")》受到[臺灣文學界的肯定](../Page/臺灣文學.md "wikilink")，《[勸和論](../Page/s:勸和論.md "wikilink")》也被[中華民國教育部選為](../Page/中華民國教育部.md "wikilink")「三十篇文言文範文」之一，今日常被各教科書出版社選為高中國文課文。根據《百年見聞肚皮集》考証，《勸和論》是「咸豐四年作」（1854年）。但因為原本的註解誤植為「咸豐三年作」（1853年），被學者誤會。其實《勸和論》是為了咸豐三年的械鬥（即「[頂下郊拚](../Page/頂下郊拚.md "wikilink")」）所作，文章寫於咸豐四年。

## 家族

鄭用錫嚴於治家，曾編寫家規，子孫恪守。\[6\]\[7\]

## 遺跡

### 文庙

在尚未考取进士之前，曾与林绍贤等人捐资建造淡水厅文庙。《淡水廳志》卷十五‧附錄一文徵（上）〈捐造淡水學文廟碑記〉記載：「是役也，倡謀捐建不憚勤勞著正總理則有林璽、林紹賢等，副總理則有鄭用錫、郭成金等。若吳振利、羅秀麗、陳建興、吳金吉等共董其事，亦與有力焉。麋金二萬□千□百有奇，不費公帑一絲。肇工於嘉慶二十一年十二月十五日，告竣于道光四年四月初十日。蓋自夫子之廟成，而淡始獲睹整齊嚴肅之規也。」\[8\]

### 竹塹城

道光六年（1826年），鄭用錫與臺灣府淡水撫民同知李慎彝等稟請改建淡水廳城，即竹塹城，並獲准將原來的土牆改爲石砌。工程於次年展開，由臺灣道孔昭虔親自履勘，歷時兩年完成。用錫督建城池有功，加同知銜。

### 故居

[新竹進士第.JPG](https://zh.wikipedia.org/wiki/File:新竹進士第.JPG "fig:新竹進士第.JPG")
鄭用錫宅第，又稱**進士第**，位於竹塹城北門外，即今[新竹市](../Page/新竹市.md "wikilink")[北區北門街](../Page/北區_\(新竹市\).md "wikilink")。建築群建於[道光十七年](../Page/道光.md "wikilink")（1837年），共三開五進院落，整體風格與[金門民居相同](../Page/金門.md "wikilink")，特徵為[山牆馬背較大而弧度較緩](../Page/山牆.md "wikilink")。建築木雕精美，極具地方特色，整體建築特色——閩南磚打造成。[二戰期間](../Page/第二次世界大戰.md "wikilink")，後三進院落遭遇[美軍轟炸焚毀](../Page/美軍.md "wikilink")，僅前二進倖存至今。現為[二級古蹟](../Page/臺灣國家二級古蹟.md "wikilink")。\[9\]\[10\]

### 書院

道光九年（1829年），鄭用錫任院長。道光十四年（1834年）郑用锡赴京任官，由弟[郑用鉴接任](../Page/郑用鉴.md "wikilink")；咸丰二年（1852年）郑用锡归里再任。咸丰八年（1858年）郑用锡卒，由[郑如松接任](../Page/郑如松.md "wikilink")。

### 園林

北郭園為鄭用錫於[咸豐元年](../Page/咸豐.md "wikilink")（1851年）在竹塹城北興建，取[唐朝](../Page/唐朝.md "wikilink")[李白名句](../Page/李白.md "wikilink")「青山橫北郭」之意境。俗稱「外公館」。北郭園曾是臺灣最富盛名的庭園之一。鄭用錫身後，園林由家人繼續經營。[日治時期](../Page/臺灣日治時期.md "wikilink")，被迫開路。[中華民國接管臺灣後](../Page/臺灣戰後時期.md "wikilink")，最終於1978年夏拆除。\[11\]

### 宗祠

  - [東溪鄭氏家廟](../Page/東溪鄭氏家廟.md "wikilink")，位於[金門縣](../Page/金門縣.md "wikilink")[金沙鎮](../Page/金沙鎮.md "wikilink")[大洋里東溪](../Page/大洋里.md "wikilink")14號，由鄭用錫於道光十年（1830年）在祖居地創建。建築座東南、朝西北，是典型的清朝中葉[閩南特色建築](../Page/閩南.md "wikilink")。廟內的木製棟架、雀替、[斗栱等](../Page/斗栱.md "wikilink")，均為鏤空雕精品。廟內尚存進士匾額、石香爐、金瓜筒、趖瓜筒、獅子斗座、神龕看架等精美文物。\[12\]

<!-- end list -->

  - [新竹鄭氏家廟](../Page/新竹鄭氏家廟.md "wikilink")：位於新竹市[北區](../Page/北區_\(新竹市\).md "wikilink")，建於咸豐三年（1853年），隔壁依序為吉利第、春官第與進士第。

### 會館

鹿港金門館原稱浯江館，本為仕紳許樂三的宅邸，嘉慶十年（1805年）變賣後改為浯江館；道光十一年（1831年）時，鹿港遊擊溫兆鳳倡議重建，但未動工便升任艋舺迎參將，後來繼任的劉光彩繼續倡議，該工程由鄭用錫擔任總理，於道光十二年（1832年）二月動工，道光十四年（1834年）四月落成。

### 墓葬

[Grave_of_Zheng_Yongxi.jpg](https://zh.wikipedia.org/wiki/File:Grave_of_Zheng_Yongxi.jpg "fig:Grave_of_Zheng_Yongxi.jpg")
鄭用錫墓，俗稱「開臺進士墓」，在今[新竹市大眾廟山](../Page/新竹市.md "wikilink")，軍人公墓右側，建於[同治八年](../Page/同治.md "wikilink")（1869年），是臺灣現存規格較高的墓葬建築。

墓葬依鄭用錫生前品秩而建，坐東南朝西北，占地約638[坪](../Page/坪.md "wikilink")（2,112平方公尺）。墓塋呈橢圓形，神道兩側石像生保存尚好，有文武官像及石馬、石羊、石獅各一對。現為二級古蹟。\[13\]
。

## 註釋

## 參考書籍

  - 陳培桂，《淡水廳志》，刻本，清同治十年（1871年）。
  - 鄭藩派，《[開台進士——鄭用錫](http://www.kinmen.gov.tw/Layout/sub_E/AllInOne_Show.aspx?path=2985&guid=d701a1bf-2d43-47c9-9f4a-95fd43f42d08&lang=zh-tw)》，金門：金門縣政府文化局，2007。
  - 范文鳳，《淡水名紳鄭用錫暨其〈北郭園全集〉研究》，臺中：白象文化公司，2008。
  - 潘國正，《新竹文化地圖》，新竹：齊風堂出版，2007。

## 相關條目

  - [北門街商圈](../Page/北門街商圈.md "wikilink")
  - [北郭園](../Page/北郭園.md "wikilink")
  - [明志書院](../Page/明志書院.md "wikilink")
  - [鄭氏家廟](../Page/鄭氏家廟.md "wikilink")
  - [鄭用錫墓](../Page/鄭用錫墓.md "wikilink")
  - [鄭堅克](../Page/鄭堅克.md "wikilink")

## 外部連結

  - [鄭用錫](http://nrch.culture.tw/twpedia.aspx?id=22135) 臺灣大百科全書
  - [鄭用錫墓](http://www.hach.gov.tw/hach/frontsite/cultureassets/caseBasicInfoAction.do?method=doViewCaseBasicInfo&caseId=OA09602000314&version=1&assetsClassifyId=1.1)

[Category:嘉慶二十三年戊寅恩科舉人](../Category/嘉慶二十三年戊寅恩科舉人.md "wikilink")
[Category:清朝禮部員外郎](../Category/清朝禮部員外郎.md "wikilink")
[Category:清朝詩人](../Category/清朝詩人.md "wikilink")
[Category:新竹市人](../Category/新竹市人.md "wikilink")
[Y用](../Category/浯江鄭氏.md "wikilink")
[Category:漳浦人](../Category/漳浦人.md "wikilink")
[Category:同安人](../Category/同安人.md "wikilink")
[Category:金門人](../Category/金門人.md "wikilink")
[鄭](../Category/閩南裔臺灣人.md "wikilink")

1.  《淡水廳志·卷九·先正》頁十四～十五：鄭用錫，字在中，號祉亭，崇和子。少穎異，淹通經史百家，尤精於易，好吟詠。主明志書院講席，汲引後進。淡自開闢，志乘無書，乃纂稿藏之。嘉慶戊寅舉於鄉，道光癸未成進士。開臺二百餘年，通籍自用錫始。丁亥，督建塹城，功加同知銜，復捐京秩，籖分兵部武選司，補授禮部鑄印局員外郎，精勤稱職。旋因母老乞養。壬寅，洋船擾大安口，率先募勇赴援，以功賞花翎。繼獲土地公港草烏洋匪，加四品銜。甲寅，在籍協辦團練，助捐津米，給二品封典。曾捐榖三千，贍父黨母黨之貧乏者。南北漳泉粵各莊互鬬，用錫躬詣慰解，并手書勸告，輒止，存活尤多。凡倡修學宮、橋渡及賑饑恤寒，悉力為之。治家最嚴，所編家規，子孫猶恪守之。晚築[北郭園以自娛](../Page/北郭園.md "wikilink")。著述日富，有詩文若干卷。請祀鄉賢祠。

2.

3.

4.
5.  《北郭園全集》序

6.
7.

8.  [臺灣方志/一七二 淡水廳志/卷十五(上) 附錄一 文徵(上)/捐造淡水學文廟碑記
    吳性誠](http://140.112.30.230/Fangjr/T_Fangjr/Show_volume.php?Keyword=%E9%A6%AC&Reference_id=172&Page_order=196),
    臺灣方志

9.  《新竹市古跡簡介》

10.

11. 《新竹市古跡簡介》

12.

13.