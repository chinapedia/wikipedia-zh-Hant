**鄭蘭貞**（[諺文](../Page/諺文.md "wikilink")：，[朝鮮漢字](../Page/朝鮮漢字.md "wikilink")：，）是[朝鮮王朝時期人物](../Page/朝鮮王朝.md "wikilink")，[本貫](../Page/本貫.md "wikilink")[草溪鄭氏](../Page/草溪鄭氏.md "wikilink")，清溪君[鄭允謙的](../Page/鄭允謙.md "wikilink")[庶女](../Page/庶女.md "wikilink")，[母親為](../Page/母親.md "wikilink")[營婢出身](../Page/營婢.md "wikilink")\[1\]。她曾當過[妓生](../Page/妓生.md "wikilink")，後為[文定王后弟弟](../Page/文定王后.md "wikilink")[尹元衡](../Page/尹元衡.md "wikilink")\[2\]之[妾](../Page/妾.md "wikilink")。1551年，尹元衡正[妻金氏因為](../Page/妻.md "wikilink")[尹任關係被波及](../Page/尹任.md "wikilink")（有傳是被鄭蘭貞毒死），鄭蘭貞被文定王后破例扶正為正室（朝鮮王朝時期妾的身分是非常低微的，也規定妾不能被扶為正室），被封為正一品[貞敬夫人](../Page/貞敬夫人.md "wikilink")。

她在文定王后[攝政的八年間](../Page/攝政.md "wikilink")，言唆文定王后發起士禍，誣告[中宗之子](../Page/朝鮮中宗.md "wikilink")[鳳城君](../Page/鳳城君.md "wikilink")（[熙嬪洪氏所出](../Page/熙嬪洪氏.md "wikilink")）要[奪嫡](../Page/奪嫡.md "wikilink")，使得一些大臣被流放被殺害。

鄭蘭貞與[奉恩寺的](../Page/奉恩寺.md "wikilink")[僧侶](../Page/僧侶.md "wikilink")[普雨關係密切](../Page/普雨.md "wikilink")，並引入宮中，使得文定王后大興佛教，鄭蘭貞還利用其政治勢力，大擴對[商業之影響力](../Page/商業.md "wikilink")。之後金氏之母親姜氏上告鄭蘭貞毒殺尹元衡之正妻，但是有文定王后當靠山，所以不了了之。

起初鄭淑之子[鄭宗榮與鄭蘭貞不合](../Page/鄭宗榮.md "wikilink")，因此尹元衡欲陷害他，不過在鄭蘭貞生母的阻止下，讓鄭宗榮逃過一劫。\[3\]

文定王后死後，尹元衡失勢，[鄭蘭貞服毒自盡](../Page/鄭蘭貞.md "wikilink")，尹元衡本人亦飲恨而終。\[4\]\[5\]

與燕山君時淑容[張綠水](../Page/張綠水.md "wikilink")、肅宗時[張禧嬪](../Page/張禧嬪.md "wikilink")，或一說光海君時尚宮[金介屎](../Page/金介屎.md "wikilink")，合稱朝鮮三大妖女。

## 子女

  - 子：尹孝源，配崔氏\[6\]\[7\]。

## 曾饰演鄭蘭貞的艺人

|                                               |       |                                             |                                    |
| --------------------------------------------- | ----- | ------------------------------------------- | ---------------------------------- |
| 劇名                                            | 年份    | 電視台                                         | 演員                                 |
| 《女人列传》—《校洞夫人》                                 | 1981年 | [MBC电视剧](../Page/文化广播_\(韩国\).md "wikilink") | [金英蘭](../Page/金英蘭.md "wikilink") 飾 |
| 《[朝鲜王朝五百年](../Page/朝鲜王朝五百年.md "wikilink")》—《》 | 1984年 | [MBC电视剧](../Page/文化广播_\(韩国\).md "wikilink") | [金英蘭](../Page/金英蘭.md "wikilink") 飾 |
| 《[女人天下](../Page/女人天下.md "wikilink")》          | 2002年 | [SBS电视剧](../Page/SBS_\(韩国\).md "wikilink")  | [姜受延](../Page/姜受延.md "wikilink") 飾 |
| 《[獄中花](../Page/獄中花.md "wikilink")》            | 2016年 | [MBC电视剧](../Page/文化广播_\(韩国\).md "wikilink") | [朴珠美](../Page/朴珠美.md "wikilink") 飾 |
|                                               |       |                                             |                                    |

## 參看

  - [女人天下](../Page/女人天下.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [鄭蘭貞](http://kdaq.empas.com/koreandb/history/kpeople/person_view.html?id=0011933)（韓文）

[Category:朝鮮王朝政治人物](../Category/朝鮮王朝政治人物.md "wikilink")
[Category:郑姓](../Category/郑姓.md "wikilink")

1.  [《明宗實錄》31卷，明宗20年（1565年）9月8日紀錄二](http://sillok.history.go.kr/id/wma_12009008_002)
2.  [《明宗實錄》2卷，明宗即位年（1545年）9月1日紀錄一](http://sillok.history.go.kr/id/wma_10009001_001)
3.  [《宣祖修正實錄》23卷，宣祖22年（1589年）8月1日紀錄一](http://sillok.history.go.kr/id/wnb_12208001_001)
4.  [《明宗實錄》31卷，明宗20年（1565年）11月13日紀錄二](http://sillok.history.go.kr/id/kma_12011013_002)
5.  [《明宗實錄》31卷，明宗20年（1565年）11月18日紀錄五](http://sillok.history.go.kr/id/kma_12011018_005)
6.  [《宣祖實錄》8卷，宣祖7年2月13日第2條](http://sillok.history.go.kr/id/wna_10702013_002)
7.