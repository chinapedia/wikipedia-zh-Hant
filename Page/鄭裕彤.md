**鄭裕彤**<span style="font-size:smaller;">，DPMS，LLD（Hon），DBA（Hon），DSSc（Hon）</span>（，）。人稱**彤叔**、**鯊膽彤**，在[廣東](../Page/廣東.md "wikilink")[順德](../Page/順德.md "wikilink")[倫教出生](../Page/倫教.md "wikilink")，[香港](../Page/香港.md "wikilink")[商人](../Page/商人.md "wikilink")，[新世界發展第二任](../Page/新世界發展.md "wikilink")[董事會主席兼](../Page/董事會主席.md "wikilink")[創辦人之一](../Page/創辦人.md "wikilink")\[1\]，以及[周大福珠寶金行創辦人](../Page/周大福珠寶金行.md "wikilink")[周至元女婿](../Page/周至元.md "wikilink")。[福布斯雜誌公布他是](../Page/福布斯.md "wikilink")2016年香港第三大富豪\[2\]。

## 早期事跡

鄭裕彤的父親是[廣州的綢緞商人](../Page/廣州.md "wikilink")[鄭敬詒](../Page/鄭敬詒.md "wikilink")，與[黃金商人](../Page/黃金.md "wikilink")[周至元是同鄉兼好友](../Page/周至元_\(香港企業家\).md "wikilink")。鄭裕彤只讀過小學，在1938年[抗日戰爭爆發後](../Page/抗日戰爭.md "wikilink")，便輟學到[澳門其未來岳父周至元的周大福珠寶金行打工](../Page/澳門.md "wikilink")，1943年與周翠英結婚，並在1956年繼承了周大福。周大福首創四條九足金（即足金成色達999.9），1964年又在[南非購入一間擁有](../Page/南非.md "wikilink")[De
Beers鑽石石胚牌照的公司](../Page/De_Beers.md "wikilink")，入口[鑽石到香港](../Page/鑽石.md "wikilink")。

1950年代，鄭裕彤先後在香港[跑馬地興建藍塘别墅及](../Page/跑馬地.md "wikilink")[銅鑼灣興建](../Page/銅鑼灣.md "wikilink")[香港大廈](../Page/香港大廈.md "wikilink")。1967年，香港發生[六七暴動後](../Page/六七暴動.md "wikilink")，鄭裕彤在1968年起購買不少地產物業，亦與[恒生銀行保持良好關係](../Page/恒生銀行.md "wikilink")。1970年5月\[3\]，鄭裕彤與恒生銀行創辦人[何善衡及](../Page/何善衡.md "wikilink")[新鴻基地產創辦人](../Page/新鴻基地產.md "wikilink")[郭得勝聯手創立](../Page/郭得勝.md "wikilink")[新世界發展](../Page/新世界發展.md "wikilink")\[4\]，鄭裕彤佔股最多\[5\]，達57%並獲委任新世界董事總經理\[6\]，何善衡則出任主席\[7\]。1971年，新世界發展以1.3億港元從[太古洋行手中購入](../Page/太古洋行.md "wikilink")[九龍](../Page/九龍.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")[藍煙囪貨倉碼頭舊址](../Page/藍煙囪貨倉碼頭.md "wikilink")。1972年[港股上揚](../Page/港股.md "wikilink")，鄭將公司上市，奠定為本港四大地產商之一\[8\]。1982年1月何善衡退休\[9\]，鄭裕彤接任主席\[10\]，同月建成[新世界中心及麗晶酒店](../Page/新世界中心.md "wikilink")（即今日[香港洲際酒店](../Page/香港洲際酒店.md "wikilink")）。1980年代初與[胡應湘等香港商人在](../Page/胡應湘.md "wikilink")[廣州投資興建](../Page/廣州.md "wikilink")[中國大酒店](../Page/中國大酒店.md "wikilink")。

1980年代中期，新世界發展投資18億港元，與[香港貿易發展局合作](../Page/香港貿易發展局.md "wikilink")，在[灣仔填海區興建](../Page/灣仔.md "wikilink")[香港會議展覽中心](../Page/香港會議展覽中心.md "wikilink")，並在1988年5月1日在九龍東區議會建造連接新界東區議會的大老山隧道來解決獅子山隧道的交通壓力，而大老山隧道最後於3年後的7月1日完成。1988年6月與[林百欣合作購入](../Page/林百欣.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")，各佔47%股權，早期亦出任董事局主席。1989年，鄭裕彤曾經宣布退休，由長子[鄭家純接棒](../Page/鄭家純_\(企業家\).md "wikilink")，出任新世界發展[董事總經理](../Page/董事總經理.md "wikilink")。但鄭家純上任後便多番進行收購，令新世界的負債大幅增加。鄭裕彤需在1990年復出，並把部份業務出售以減輕負債，包括將亞洲電視股權轉售予[林百欣](../Page/林百欣.md "wikilink")。

## 晚年

1990年代中期，鄭裕彤投資成立[新世界電話及](../Page/新世界電話.md "wikilink")[新世界傳動網](../Page/新世界傳動網.md "wikilink")，1998年成立[新世界第一巴士](../Page/新世界第一巴士.md "wikilink")（新巴）經營原本由[中華巴士營運的](../Page/中華巴士.md "wikilink")[港島區巴士路線](../Page/港島.md "wikilink")，及[新世界第一渡輪](../Page/新世界第一渡輪.md "wikilink")（新渡輪）經營離島及香港至澳門渡輪航線。2000年代與[劉鑾雄合作購入香港](../Page/劉鑾雄.md "wikilink")[崇光百貨股權](../Page/崇光百貨.md "wikilink")。2004年以周大福名義收購[城巴](../Page/城巴.md "wikilink")，統一港島區巴士路線。

鄭裕彤也有在[澳門投資](../Page/澳門.md "wikilink")，持有[澳門旅遊娛樂有限公司](../Page/澳門旅遊娛樂有限公司.md "wikilink")5%股份。1985年，鄭裕彤以周大福名義購入[澳門自來水有限公司](../Page/澳門自來水有限公司.md "wikilink")42%股權及[澳門電力公司](../Page/澳門電力公司.md "wikilink")20%股權，1997年把權益注入[新世界基建](../Page/新世界基建.md "wikilink")。2002年亦參與競投澳門博彩牌照。2011年
12月15日鄭裕彤將私人持有的周大福珠寶於港交所上市。令其「浮面」私人財富增至千億港幣。2012年2月29日宣布退任新世界發展[董事會主席及](../Page/董事會主席.md "wikilink")[執行董事職位](../Page/執行董事.md "wikilink")，由兒子[鄭家純接任](../Page/鄭家純_\(企業家\).md "wikilink")。

透過萬利城有限公司持有信德船務12.9%。鄭裕彤於2012年3月退任董事局主席及執行董事，同年9月[中風入住](../Page/中風.md "wikilink")[養和醫院](../Page/養和醫院.md "wikilink")\[11\]，其後一直[昏迷](../Page/昏迷.md "wikilink")\[12\]，直至2016年9月29日晚上在家中安詳辭世\[13\]。

## 個人生活

鄭裕彤與周翠英是指腹為婚，即在出生前已由家長決定婚姻，在1943年正式結婚至今。鄭裕彤甚愛妻子，二人共育有兩子兩女，長子是[鄭家純](../Page/鄭家純_\(企業家\).md "wikilink")，次子[鄭家成](../Page/鄭家成.md "wikilink")。郑裕彤与[李嘉诚也是至交好友](../Page/李嘉诚.md "wikilink")，但他坦承，自己不像李嘉诚那么努力。在接受华商韬略编委会的那次专访中，他即表示，“我没有[李嘉诚那么忙](../Page/李嘉诚.md "wikilink")。他早上是8点多一点就上班，我是11点，差很远。我们两个喜欢打高尔夫球，但我现在就不大喜欢和他打，因为他早上6时就要开始，我还未起床哩！我现在上班，在办公室，基本上也不用做什么事，人在那就行，有什么人找我，我能在。他是整天世界各地跑来跑去，不停。”

鄭裕彤喜歡打[高爾夫球](../Page/高爾夫球.md "wikilink")，與多位富商包括[何鴻燊](../Page/何鴻燊.md "wikilink")、[劉鑾雄等關係良好](../Page/劉鑾雄.md "wikilink")。1980年代鄭裕彤在[車牌](../Page/香港車輛登記號碼.md "wikilink")[拍賣會上投得](../Page/拍賣.md "wikilink")8888車牌。鄭裕彤亦育有多匹以「鑽」字為名的佳駟，擁有馬匹包括「幸運鑽石」、「黃鑽石」、「鑽神」、「綠鑽石」、「紅鑽石」、「鑽王」、「鑽中寶」、「鑽之寶」等。

鄭裕彤父親[鄭敬詒卒於](../Page/鄭敬詒.md "wikilink")1965年。除鄭裕彤外，還有[鄭裕榮](../Page/鄭裕榮.md "wikilink")、[鄭裕培](../Page/鄭裕培.md "wikilink")、[鄭裕偉等兒女](../Page/鄭裕偉.md "wikilink")\[14\]。

## 公職

  - [港事顧問](../Page/港事顧問.md "wikilink")（1993年）
  - [全國人民代表大會](../Page/全國人民代表大會.md "wikilink")[香港特别行政區籌備委員會委員](../Page/香港特别行政區籌備委員會.md "wikilink")（1995年）
  - [香港特别行政區第一屆政府推選委員會委員](../Page/香港特别行政區第一屆政府推選委員會.md "wikilink")（1996年11月）

## 榮譽

  - [大紫荊勳章](../Page/大紫荊勳章.md "wikilink")（2008年）

## 命名

鄭裕彤曾經向多個機構捐獻，包括香港多所大學，所以香港有不少建築物以鄭裕彤命名。

  - [多倫多大學鄭裕彤東亞圖書館](../Page/多倫多大學.md "wikilink")
  - [香港大學鄭裕彤教學樓](../Page/香港大學.md "wikilink")
  - [香港中文大學鄭裕彤樓](../Page/香港中文大學.md "wikilink")
  - [香港科技大學鄭裕彤樓](../Page/香港科技大學.md "wikilink")\[15\]
  - [香港公開大學鄭裕彤樓](../Page/香港公開大學.md "wikilink")\[16\]
  - [順德聯誼總會鄭裕彤中學](../Page/順德聯誼總會鄭裕彤中學.md "wikilink")
  - 陳校長免費補習天地鄭裕彤葵涌學習中心\[17\]

## 家族

## 參考資料

  - 《黃金歲月--鄭裕彤傳》，陳雨，經濟日報出版社，ISBN 962-678-175-0

[分類:信德集團](../Page/分類:信德集團.md "wikilink")

[Category:香港順德人](../Category/香港順德人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:新世界發展](../Category/新世界發展.md "wikilink")
[Category:香港公開大學榮譽博士](../Category/香港公開大學榮譽博士.md "wikilink")
[Category:香港財經界人士](../Category/香港財經界人士.md "wikilink")
[Category:香港億萬富豪](../Category/香港億萬富豪.md "wikilink")
[Y](../Category/郑姓.md "wikilink")
[Category:鄭裕彤家族](../Category/鄭裕彤家族.md "wikilink")
[Category:香港慈善家](../Category/香港慈善家.md "wikilink")
[Category:中國慈善家](../Category/中國慈善家.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:香港理工大學榮譽博士](../Category/香港理工大學榮譽博士.md "wikilink")
[Category:1925年出生](../Category/1925年出生.md "wikilink")
[Category:2016年逝世](../Category/2016年逝世.md "wikilink")
[Category:珠寶商](../Category/珠寶商.md "wikilink")

1.
2.

3.

4.

5.
6.
7.
8.
9.
10.
11. [新世界名譽主席鄭裕彤病逝 終年91歲
    《蘋果日報》2016-09-30](http://hk.apple.nextmedia.com/realtime/finance/20160930/55714359)

12.

13.
14.

15.

16.

17. [鄭裕彤葵涌學習中心](http://hkcnc.org.hk/service_unit/kwai_chung/)