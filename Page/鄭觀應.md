[Zheng_Guanying.jpg](https://zh.wikipedia.org/wiki/File:Zheng_Guanying.jpg "fig:Zheng_Guanying.jpg")拜题”\]\]
[Zheng_Guanying2.jpg](https://zh.wikipedia.org/wiki/File:Zheng_Guanying2.jpg "fig:Zheng_Guanying2.jpg")[吴广霈拜题](../Page/吴广霈.md "wikilink")。”右上题款为“官商洁士，儒道通人。心殷救世，志在成真。[大埔](../Page/大埔縣.md "wikilink")[张振勳敬题](../Page/张振勳.md "wikilink")。”右下题款为“垂廉塞兑，抱一守中。欲铸神剑，遍访仙翁。[高要](../Page/高要.md "wikilink")[梁应锦敬题](../Page/梁应锦.md "wikilink")。”\]\]
**鄭觀應**（），原名**官應**，字**正翔**，號**陶齋**，又号**居易**、**杞忧生**，别号**待鹤山人**或**罗浮偫鹤山人**。[廣東](../Page/廣東.md "wikilink")[香山縣三鄉雍陌](../Page/香山縣.md "wikilink")（今[中山市](../Page/中山市.md "wikilink")）人。[中國近代著名](../Page/中國.md "wikilink")[文學家](../Page/文學家.md "wikilink")、[思想家和](../Page/思想家.md "wikilink")[實業家](../Page/實業家.md "wikilink")。他是[中國近代最早具有完整維新思想體系的理論家](../Page/中國.md "wikilink")，揭開[民主與](../Page/民主.md "wikilink")[科學序幕的啟蒙](../Page/科學.md "wikilink")[思想家](../Page/思想家.md "wikilink")，也是[實業家](../Page/實業家.md "wikilink")、[教育家](../Page/教育家.md "wikilink")、[文學家](../Page/文學家.md "wikilink")、[慈善家和熱忱的](../Page/慈善家.md "wikilink")[愛國者](../Page/愛國者.md "wikilink")。

## 生平

鄭觀應自小受身為讀書人的父親所教育，惜[童子試未中](../Page/童子試.md "wikilink")。[咸丰八年](../Page/咸丰.md "wikilink")（1858年）他16岁时奉父命從香山到[上海學習经商](../Page/上海.md "wikilink")。\[1\]17岁在上海外商洋行工作的时候，他入[英华书馆夜课班学习](../Page/英华书馆.md "wikilink")[英文两年](../Page/英文.md "wikilink")。\[2\]1860年成為[英國](../Page/英國.md "wikilink")[寶順洋行的买办](../Page/寶順洋行.md "wikilink")，並勤修英語，對西方的[政治和](../Page/政治.md "wikilink")[經濟等有更深之認識](../Page/經濟.md "wikilink")。\[3\]同治七年（1868年）起，他同别人合伙经营茶栈以及轮船公司等。\[4\]同治十二年（1873年）他受聘为英商[太古轮船公司总理](../Page/太古轮船公司.md "wikilink")。1880年他編訂《易言》一書，在自強運動期間，他提出“商戰”（即在[商業上學習西方](../Page/商業.md "wikilink")），並建議採取[君主立憲制](../Page/君主立憲制.md "wikilink")。同年，他離開太古，並受[直隸總督](../Page/直隸總督.md "wikilink")[李鴻章之托出任](../Page/李鴻章.md "wikilink")[上海機器織布局及](../Page/上海機器織布局.md "wikilink")[上海電報局總辦](../Page/上海電報局.md "wikilink")。\[5\]光绪七年四月（1881年），他任[上海电报局总办](../Page/上海电报局.md "wikilink")。任内，他建设了津沪电线，并组织翻译出版了《万国电报通例》和《测量浅说》，供各地电报局使用。他还因为[大北电报公司所用](../Page/大北电报公司.md "wikilink")[电码本字码过少](../Page/中文电码.md "wikilink")，而对电码本进行扩编，出版《四码电报新编》。\[6\]1883年他升任[輪船招商局](../Page/輪船招商局.md "wikilink")[總辦](../Page/總辦.md "wikilink")。\[7\]

1884年，他因[輪船招商局與](../Page/輪船招商局.md "wikilink")[太古輪船公司經營不善以及员工中飽私囊等所累](../Page/太古輪船公司.md "wikilink")，身心俱疲，遂退居[澳門](../Page/澳門.md "wikilink")，居住於其[鄭家大屋中](../Page/鄭家大屋.md "wikilink")，潛心修訂《易言》，亦即後來的名著《[盛世危言](../Page/盛世危言.md "wikilink")》。1891年，他出任[開平煤礦局總辦](../Page/开滦公司.md "wikilink")，翌年調任招商局[幫辦](../Page/幫辦.md "wikilink")。1894年，《盛世危言》完成。\[8\]

[武昌起義後](../Page/武昌起義.md "wikilink")[中华民國建立](../Page/中华民國.md "wikilink")，鄭觀應則專注於發展[教育事業](../Page/教育.md "wikilink")，歷任[招商局公學駐校董事](../Page/招商局公學.md "wikilink")、[上海商務中學名譽董事等職](../Page/上海商務中學.md "wikilink")。\[9\]

1922年5月下旬，他逝世於上海[提篮桥華德路](../Page/提篮桥.md "wikilink")（現為[长阳路](../Page/长阳路.md "wikilink")）[招商局公學駐校校董公寓](../Page/輪船招商局.md "wikilink")\[10\]。（但根據王学庄的引述上海《[申报](../Page/申报.md "wikilink")》[讣告考據](../Page/讣告.md "wikilink")，卒時應為1921年6月14日）\[11\]

## 影响

[Zheng_Guanying3.jpg](https://zh.wikipedia.org/wiki/File:Zheng_Guanying3.jpg "fig:Zheng_Guanying3.jpg")敬题。”右方题款为[许炳璈题](../Page/许炳璈.md "wikilink")“大德昭宣，危言彪炳。五愿宏持，七教兼併。此愿无尽，此身无尽。日月山河，千秋並永。”左方题款为“待鹤老人八十小影。婣世姪[许炳璈敬题](../Page/许炳璈.md "wikilink")。”\]\]

鄭氏世居[澳門](../Page/澳門.md "wikilink")，自幼受歐風薰陶，“究心政治、實業之學”，平生經驗鑄為不朽名句：“欲攘外，亟須自強；欲自強，必先致富；欲致富，必首在振工商；欲振工商，必先講求學校、速立[憲法](../Page/憲法.md "wikilink")、尊重[道德](../Page/道德.md "wikilink")、改良[政治](../Page/政治.md "wikilink")”。

鄭氏於[澳門的故居](../Page/澳門.md "wikilink")──[鄭家大屋撰寫的](../Page/鄭家大屋.md "wikilink")《[盛世危言](../Page/盛世危言.md "wikilink")》堪稱中國近代社會極具震撼力與影響力之巨著，自十九世紀九十年代刊行後，在思想界、[學術界乃至](../Page/學術界.md "wikilink")[政治](../Page/政治.md "wikilink")、[經濟](../Page/經濟.md "wikilink")、[軍事各界](../Page/軍事.md "wikilink")，都引起強烈反響。《[盛世危言](../Page/盛世危言.md "wikilink")》甫問世即啟惕[光緒皇帝](../Page/光緒皇帝.md "wikilink")，喚醒千百萬仁人志士，更深深影響數代偉人──如[康有為](../Page/康有為.md "wikilink")、[梁啟超](../Page/梁啟超.md "wikilink")、[孫中山](../Page/孫中山.md "wikilink")、[毛澤東等](../Page/毛澤東.md "wikilink")，為中國近代思想史寫下光輝一頁。

## 子女

大子潤朝，二子潤燊，三子潤鑫；四子景康（1904年-1978年）为[民国以至中共建政后著名摄影师](../Page/民国.md "wikilink")，曾为多位名人或领袖拍摄肖像照，如齊白石、天安门的毛泽东肖像。

## 著作

  - 盛世危言
  - 羅浮偫鶴山人詩草

其作品已集为《郑观应集》

## 参考文献

## 延伸阅读

  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>
  - 伍國：〈[改良的底线——“体用”、“道器”与郑观应思想](http://www.nssd.org/articles/Article_Read.aspx?id=11090803)〉。
  - 伍國：〈[郑观应思想中的“体用”与“道器”](http://www.nssd.org/articles/article_read.aspx?id=11445794)〉。

## 相關條目

  - [澳門鄭家大屋](../Page/澳門鄭家大屋.md "wikilink")
  - [盛世危言](../Page/盛世危言.md "wikilink")
  - [澳門博物館](../Page/澳門博物館.md "wikilink")

[Category:中国企业家](../Category/中国企业家.md "wikilink")
[Category:中国思想家](../Category/中国思想家.md "wikilink")
[Category:中国教育家](../Category/中国教育家.md "wikilink") [Category:香山县人
(中山)](../Category/香山县人_\(中山\).md "wikilink")
[G觀應](../Category/郑姓.md "wikilink")

1.

2.
3.

4.
5.
6.  上海地方誌辦公室‧邮电志：[郑观应](http://www.shtong.gov.cn/newsite/node2/node2245/node67155/node67181/node67383/node67391/userobject1ai63616.html)

7.
8.
9.
10.
11. 王學庄《鄭觀應的卒年》，[《近代史研究》1994年第6期](http://jds.cass.cn/Item/1130.aspx)