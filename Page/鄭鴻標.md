[丹斯里](../Page/馬來西亞封銜.md "wikilink")**鄭鴻標**（），籍貫广东省[潮州](../Page/潮州.md "wikilink")。现任[马来西亚](../Page/马来西亚.md "wikilink")[大众银行](../Page/大众银行.md "wikilink")[主席兼](../Page/主席.md "wikilink")[创始人](../Page/创始人.md "wikilink")。

## 生平

出生在[新加坡](../Page/新加坡.md "wikilink")，早年毕业于[英华学校](../Page/英华学校.md "wikilink")。他于1956年结婚并育有一儿三女\[1\]。

退役後，他于1950年开始从事[银行事业并于](../Page/银行.md "wikilink")[华侨银行担任职员并于](../Page/华侨银行_\(新加坡\).md "wikilink")5年内升至主管。他于1960年加入当时刚成立的[马来亚银行](../Page/马来亚银行.md "wikilink")，在1964年时升至该银行的[总经理](../Page/总经理.md "wikilink")，当时他只有34岁。之后，他于1966年退出马来亚银行并着手创办[大众银行](../Page/大众银行.md "wikilink")。[大众银行现今为马来西亚第三大国内银行](../Page/大众银行.md "wikilink")，这可显得马来西亚华商白手起家的独特风格。

身为马来西亚亿万富豪之一的郑鸿标，曾获三间大学([美国](../Page/美国.md "wikilink")[西太平洋大学](../Page/西太平洋大学.md "wikilink")、[科普顿大学及](../Page/科普顿大学.md "wikilink")[马来亚大学](../Page/马来亚大学.md "wikilink"))颁发学位，1983年受封丹斯里\[2\]。在2017年《[福布斯](../Page/福布斯.md "wikilink")》的调查中，郑鸿标预计拥有大约47亿[美元的资产](../Page/美元.md "wikilink")，使他成为排名世界第348的富豪，总资产在[马来西亚排名第四](../Page/马来西亚.md "wikilink")，\[3\]2018年資產增加至54億美元。\[4\]2017年7月，鄭鴻標宣布於2019年1月1日退下主席職務。\[5\]

## 参見

  - [大眾銀行](../Page/大眾銀行.md "wikilink")

## 参考

[Category:馬來西亞億萬富豪](../Category/馬來西亞億萬富豪.md "wikilink")
[Category:馬來西亞企業家](../Category/馬來西亞企業家.md "wikilink")
[Category:新加坡企業家](../Category/新加坡企業家.md "wikilink")
[Category:华人银行家](../Category/华人银行家.md "wikilink")
[Category:馬來西亞潮汕人](../Category/馬來西亞潮汕人.md "wikilink")
[Category:新加坡華人](../Category/新加坡華人.md "wikilink")
[Category:英华中学校友](../Category/英华中学校友.md "wikilink")
[H鴻](../Category/鄭姓.md "wikilink")

1.
2.
3.
4.
5.