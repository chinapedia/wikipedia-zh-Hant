**酋長龍屬**（[學名](../Page/學名.md "wikilink")：*Loncosaurus*）是[鳥腳下目](../Page/鳥腳下目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，化石發現於[阿根廷的](../Page/阿根廷.md "wikilink")[聖克魯斯省](../Page/聖克魯斯省_\(阿根廷\).md "wikilink")，地質年代為[上白堊紀](../Page/上白堊紀.md "wikilink")。[模式種是](../Page/模式種.md "wikilink")**阿根廷酋長龍**（*L.
argentinus*），是由阿根廷[古生物學家](../Page/古生物學家.md "wikilink")[佛羅倫天奴·阿米希諾](../Page/佛羅倫天奴·阿米希諾.md "wikilink")（Florentino
Ameghino）描述、命名，目前被認為是[疑名](../Page/疑名.md "wikilink")。酋長龍的學名意思並不明確，有可能是[阿勞坎語的](../Page/阿勞坎語.md "wikilink")「酋長」或[希臘文的](../Page/希臘文.md "wikilink")「長矛」，中文譯名則使用了前者的意思\[1\]。由於酋長龍曾長時間被誤認為是屬於[獸腳亞目](../Page/獸腳亞目.md "wikilink")，所以有關牠的資料經常存在矛盾。

## 歷史

[佛羅倫天奴·阿米希諾在](../Page/佛羅倫天奴·阿米希諾.md "wikilink")1898年\[2\]\[3\]\[4\]\[5\]或1899年\[6\]\[7\]\[8\]\[9\]\[10\]敘述、命名了酋長龍，化石包含一根[股骨近端](../Page/股骨.md "wikilink")（編號MACN-1629）及[牙齒](../Page/牙齒.md "wikilink")，發現於[聖克魯斯省的Cardiel地層或Matasiete地層](../Page/聖克魯斯省_\(阿根廷\).md "wikilink")\[11\]。

無論是哪一種說法，他都認為酋長龍是屬於[肉食性的](../Page/肉食性.md "wikilink")[斑龍科](../Page/斑龍科.md "wikilink")，而[休尼](../Page/休尼.md "wikilink")（Friedrich
von
Huene）也支持這個說法\[12\]。後來的研究則指酋長龍是屬於[虛骨龍科](../Page/虛骨龍科.md "wikilink")\[13\]，但當時未分類的小型肉食性恐龍，多被分類到虛骨龍科。因為牙齒[化石類似肉食性](../Page/化石.md "wikilink")[動物](../Page/動物.md "wikilink")，而造成這種鑑定結果。

經過數十年後，Ralph
Molnar重新研究這些化石\[14\]。他發現那牙齒不是來自於該股骨的同一動物個體。他更指出該股骨是屬於[稜齒龍類或](../Page/稜齒龍類.md "wikilink")[龜](../Page/龜.md "wikilink")。後來的研究發現並沒有不同的結果，只有股骨的長度數據不同，以及酋長龍是屬於[禽龍類而非稜齒龍類](../Page/禽龍類.md "wikilink")\[15\]。目前酋長龍被分類為[鳥腳下目的分類不明屬](../Page/鳥腳下目.md "wikilink")\[16\]、或禽龍類\[17\]\[18\]。奇怪的是有人指出酋長龍是屬於[銳頜龍屬](../Page/銳頜龍屬.md "wikilink")，但卻沒有解釋原因，這個分類法後來沒有被接納\[19\]。

## 古生物學

[羅多爾夫·科里亞](../Page/羅多爾夫·科里亞.md "wikilink")（Rodolfo
Coria）估計酋長龍長約5公尺\[20\]。這個體型在[鳥腳下目是屬於小至中等動物](../Page/鳥腳下目.md "wikilink")，所以酋長龍有可能是行動輕盈的雙足[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")\[21\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - <https://web.archive.org/web/20070817043110/http://www.users.qwest.net/~jstweet1/ornithopoda.htm>

[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")
[Category:鳥腳下目](../Category/鳥腳下目.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")

1.  [Dinosaur Translation Guide
    L](http://www.dinosauria.com/dml/names/dinol.htm)

2.

3.

4.

5.  George Olshevsky's [Dinosaur Genera
    List](https://web.archive.org/web/20010804111603/http://members.aol.com/Dinogeorge/dinolist.html)

6.

7.

8.

9.

10. [The Paleobiology
    Database](http://paleodb.org/cgi-bin/bridge.pl?user=Guest&action=displayHomePage)

11.
12.

13.

14.

15.
16.
17.
18.
19.

20.
21.