[配位化学中](../Page/配位化学.md "wikilink")，**配位数**指化合物中中心原子周围的配位原子个数，此概念首先由[阿尔弗雷德·维尔纳在](../Page/阿尔弗雷德·维尔纳.md "wikilink")1893年提出。\[1\]\[2\]
配位数通常为2-8，也有高达10以上的，如[铀和](../Page/铀.md "wikilink")[钍的](../Page/钍.md "wikilink")[双齿](../Page/双齿.md "wikilink")[簇状](../Page/原子簇.md "wikilink")[硝酸根离子U](../Page/硝酸根.md "wikilink")(NO<sub>3</sub>)<sub>6</sub><sup>2−</sup>、Th(NO<sub>3</sub>)<sub>6</sub><sup>2−</sup>，及2007年研究的PbHe<sub>15</sub><sup>2+</sup>离子（[铅的配位数至少为](../Page/铅.md "wikilink")15）\[3\]，2015年研究的CoB<sub>16</sub><sup>−</sup>（配位数为16）\[4\]。

此概念也可延伸至任何[化合物](../Page/化合物.md "wikilink")，也就是配位数等同于共价键键连数，例如，可以说[甲烷中](../Page/甲烷.md "wikilink")[碳的配位数为](../Page/碳.md "wikilink")4。这种说法通常不计[π键](../Page/π键.md "wikilink")。

[晶体学中](../Page/晶体学.md "wikilink")，**配位数**是[晶格中与某一原子相距最近的原子个数](../Page/晶格.md "wikilink")。配位数与[晶体结构或](../Page/晶体结构.md "wikilink")[晶胞类型有关](../Page/晶胞.md "wikilink")，且决定原子堆积的紧密程度，[體心立方堆積中原子配位数为](../Page/體心立方堆積.md "wikilink")8。最高的配位数为12，存在于[六角密堆和](../Page/六角密堆.md "wikilink")[面心立方结构中](../Page/面心立方.md "wikilink")。

## 参见

  - [牛顿数](../Page/牛顿数.md "wikilink")（）—有关n维空间中可与单元球接触的等体积球数

## 参考资料

[Category:化学键](../Category/化学键.md "wikilink")
[Category:晶体学](../Category/晶体学.md "wikilink")
[Category:配位化学](../Category/配位化学.md "wikilink")

1.  *De, A.K: "A Text Book of Inorganic Chemistry", page 88. New Age
    International Publishers, **2003**.*
2.  [IUPAC Compendium of Chemical Terminology 2nd Edition
    (**1997**);](http://www.iupac.org/goldbook/C01331.pdf)
3.  *The Search for the Species with the Highest Coordination Number*
    Andreas Hermann, Matthias Lein, and Peter Schwerdtfeger [Angew.
    Chem. Int. Ed.](../Page/Angew._Chem._Int._Ed..md "wikilink")
    **2007**, 46, 2444 –2447
4.