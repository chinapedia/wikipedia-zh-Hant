**酒井敏也**是[日本](../Page/日本.md "wikilink")[岐阜縣](../Page/岐阜縣.md "wikilink")[土岐市](../Page/土岐市.md "wikilink")[駄知町出身的](../Page/駄知町.md "wikilink")[男演員](../Page/男演員.md "wikilink")。

## 演出

### 連續劇

#### 1994年

  - [古畑任三郎](../Page/古畑任三郎.md "wikilink")－第四話
  - [為愛而活](../Page/為愛而活.md "wikilink")（客串）
  - [婦產科物語](../Page/婦產科物語.md "wikilink")－第二話

#### 1996年

  - [是誰造成今日的我](../Page/是誰造成今日的我.md "wikilink")－森末博美

#### 1997年

  - [快遞高手](../Page/快遞高手.md "wikilink")（客串）
  - [成田離婚](../Page/成田離婚.md "wikilink")

#### 1998年

  - [網路情人](../Page/網路情人.md "wikilink")（客串）
  - [戀はあせらず](../Page/戀はあせらず.md "wikilink")（客串）
  - [親愛的爸爸](../Page/親愛的爸爸.md "wikilink")（客串）
  - [甜蜜的惡魔](../Page/甜蜜的惡魔.md "wikilink")
  - [大女生日記](../Page/大女生日記.md "wikilink")（客串）

#### 1999年

  - [秀逗修女瑪莉亞](../Page/秀逗修女瑪莉亞.md "wikilink")（客串）
  - [蜜桃關係](../Page/蜜桃關係.md "wikilink")（客串）
  - [冰的世界](../Page/冰的世界.md "wikilink")（客串）
  - [OUT嬌妻犯罪檔案](../Page/OUT嬌妻犯罪檔案.md "wikilink")

#### 2000年

  - [相親結婚](../Page/相親結婚.md "wikilink")
  - [美少年警探](../Page/美少年警探.md "wikilink")（客串）
  - [執法先鋒](../Page/執法先鋒.md "wikilink")－花村大介

#### 2001年

  - [HERO](../Page/HERO_\(日本電視劇\).md "wikilink")（客串）
  - [叫她第一名](../Page/叫她第一名.md "wikilink")（客串）
  - [旅館美人甘芭茶](../Page/旅館美人甘芭茶.md "wikilink")－加賀谷學
  - [奉子成婚](../Page/奉子成婚.md "wikilink")
  - [醫家姊妹](../Page/醫家姊妹.md "wikilink")（客串）

#### 2002年

  - [銀座之戀](../Page/銀座之戀.md "wikilink")
  - [甜蜜婚禮俏佳人](../Page/甜蜜婚禮俏佳人.md "wikilink")
  - [午餐女王](../Page/午餐女王.md "wikilink")
  - [天才柳澤教授的生活](../Page/天才柳澤教授的生活.md "wikilink")－郵便局員

#### 2003年

  - [幸福的王子](../Page/幸福的王子.md "wikilink")（客串）
  - [Stand UP\!\!](../Page/Stand_UP!!.md "wikilink")－宇田川信人

#### 2005年

  - [極道鮮師2](../Page/極道鮮師.md "wikilink")－犬塚太一

#### 2006年

  - [西遊記](../Page/西遊記_\(2006年電視劇\).md "wikilink")－大福（客串）
  - [夜王\~YAOH\~](../Page/夜王~YAOH~.md "wikilink")－秋山店長
  - [那個男人副署長](../Page/那個男人副署長.md "wikilink")－鈴木豊

#### 2007年

  - [求婚大作戰](../Page/求婚大作戰.md "wikilink")（客串）－[計程車司機](../Page/計程車.md "wikilink")

#### 2008年

  - [鹿男與奈良](../Page/鹿男與奈良.md "wikilink")－名取良一
  - [佐佐木之戰](../Page/佐佐木之戰.md "wikilink")－刑事（第四話）

#### 2009年

  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")－芹沢
  - [Untouchable](../Page/Untouchable_\(電視劇\).md "wikilink")－城之內仁

[Category:日本男演員](../Category/日本男演員.md "wikilink")