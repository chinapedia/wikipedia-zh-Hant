**酵母**（[拼音](../Page/拼音.md "wikilink")：中國大陆：jiàomǔ、台灣：xiàomǔ；[注音](../Page/注音.md "wikilink")：中國大陆：ㄐㄧㄠˋ
ㄇㄨˇ、台灣：ㄒㄧㄠˋ
ㄇㄨˇ；[英文](../Page/英文.md "wikilink")：Yeast）是非分类学术语，泛指能发酵糖類的各种单细胞真菌，不同的酵母菌在进化和分类地位上有异源性。酵母菌种类很多，已知的约有56属500多种\[1\]。一些酵母菌能夠通過[出芽的方式進行](../Page/出芽生殖.md "wikilink")[無性生殖](../Page/無性生殖.md "wikilink")，也可以通過形成[孢子的形式進行](../Page/孢子.md "wikilink")[有性生殖](../Page/有性生殖.md "wikilink")。酵母經常被用於[酒精釀造或者](../Page/酒精.md "wikilink")[麵包烘培行業](../Page/麵包.md "wikilink")。目前已知有1500多[種酵母](../Page/種_\(生物\).md "wikilink")，大部分被分類到[子囊菌門](../Page/子囊菌门.md "wikilink")。\[2\]酵母菌屬[兼性厭氧菌](../Page/兼性厭氧菌.md "wikilink")。\[3\]

## 歷史

4000年前，古埃及人已經開始利用酵母釀酒與製作麵包了\[4\]
；中國的[殷商時期](../Page/商朝.md "wikilink")（約3500年前），古人利用酵母釀造米酒，而酵母饅頭、餅等開始於[漢朝時期](../Page/漢朝.md "wikilink")。\[5\]

1680年，[荷蘭科學家](../Page/荷蘭.md "wikilink")[安東尼·范·列文虎克首次利用](../Page/安東尼·范·列文虎克.md "wikilink")[顯微鏡觀察到酵母](../Page/顯微鏡.md "wikilink")，但當時並沒有將其當作一個生物體看待。\[6\]

1857年，法國科學家[路易·巴斯德首次發現釀造](../Page/路易·巴斯德.md "wikilink")[酒精來之酵母體的](../Page/酒精.md "wikilink")[發酵作用](../Page/發酵.md "wikilink")，而並非簡單的化學催化。\[7\]\[8\]巴斯德曾經將空氣通進釀酒液中，發現酵母的細胞量增加了，但是酒精的生成量減少，後來人們將此現​​象稱為“[巴斯德效應](../Page/巴斯德效应.md "wikilink")”。\[9\]

酵母的工業化生產與商業化依賴於乾燥與壓濾技術的發展。1846年，[歐洲實現酵母的工業化生產](../Page/歐洲.md "wikilink")。\[10\][美國酵母的工業與商業化是隨著](../Page/美國.md "wikilink")1876年[費城百年博覽會的舉辦展開的](../Page/費城百年博覽會.md "wikilink")。中國酵母的現代化生產開始於20世纪80年代中期。\[11\]

## 細胞形態與結構

[Yeast_agar_plate-01.jpg](https://zh.wikipedia.org/wiki/File:Yeast_agar_plate-01.jpg "fig:Yeast_agar_plate-01.jpg")酵母細胞明顯比大多數細菌大，細胞大小約為
2～5 ×
5～30μm（短軸×長軸）。\[12\]酵母多數為單細胞生物，常呈卵圓形或者圓柱形。\[13\]實際上，每種酵母確實具有自己特有的形態模式，但會隨著菌齡與環境不斷變化。\[14\]一般平板培養基上的酵母菌落呈白色凸起粒狀，常帶有酒香味。

酵母属于真核微生物，除没有鞭毛外\[15\]，一般都具有细胞壁、细胞膜、线粒体、核糖体、液泡等细胞器。

  - **細胞壁**：厚度為0.1～0.3[μm](../Page/μm.md "wikilink")，不如細菌的堅韌；主要成分為[葡聚醣](../Page/葡聚糖.md "wikilink")、[甘露聚醣等](../Page/甘露糖.md "wikilink")。\[16\]酵母[細胞壁呈](../Page/細胞壁.md "wikilink")“三明治”形：內層葡聚醣、外層甘露聚醣以及中間蛋白層。\[17\]有研究表明，葡聚醣是維持細胞壁內壁強度最主要的物質。\[18\]
  - **細胞膜**：[細胞膜為磷脂雙分子層](../Page/細胞膜.md "wikilink")，與其他生物一樣都是雙膜中間鑲嵌著蛋白質。此外，酵母細胞膜中還含有[甾醇](../Page/固醇.md "wikilink")，其中以[麥角甾醇最為常見](../Page/麦角固醇.md "wikilink")。\[19\]

[Yeast_cell.svg](https://zh.wikipedia.org/wiki/File:Yeast_cell.svg "fig:Yeast_cell.svg")

  - **細胞核**：酵母具有成形的[細胞核](../Page/細胞核.md "wikilink")，不同種的酵母染色體數不同，且細胞核的形態會隨著細胞分裂週期而變化。細胞核是酵母菌[遺傳信息的主要儲存與](../Page/遺傳.md "wikilink")[轉錄場所](../Page/轉錄.md "wikilink")，其[DNA量佔總細胞DNA的絕大部分](../Page/DNA.md "wikilink")。此外還有兩個“[細胞器](../Page/細胞器.md "wikilink")”含有DNA：線粒體與“[2μm質粒](../Page/2μm質粒.md "wikilink")”。

<!-- end list -->

  - **線粒體**：[線粒體為酵母細胞能量的主要提供場所](../Page/線粒體.md "wikilink")，酵母線粒體要比高等動物的小，其大小為0.3～1μm
    × 0.5～3μm。一般在厭氧或高糖([葡萄糖](../Page/葡萄糖.md "wikilink")
    5％～10％)條件下，酵母菌的線粒體前體發育較差，不具有[氧化磷酸化的能力](../Page/氧化磷酸化.md "wikilink")。\[20\]
  - **核醣體**：與真核生物一樣，酵母菌[核醣體為](../Page/核糖体.md "wikilink")80S型的。
  - **液泡**：大多數酵母菌都具有[液泡](../Page/液泡.md "wikilink")，其主要用於儲藏一些營養物質或​​者[水解酶前體物](../Page/水解酶.md "wikilink")，另外還有調劑滲透壓的作用。\[21\]

## 营养与生长

酵母菌廣泛生活於潮濕且富含糖分的物體表層，例如果皮表層、[土壤](../Page/土壤.md "wikilink")、植物表面、植物分泌物（如[仙人掌的汁](../Page/仙人掌.md "wikilink")），甚至[空氣中也有分佈](../Page/空氣.md "wikilink")。此外，有研究發現酵母還能寄生於[人類身上與一些](../Page/人類.md "wikilink")[昆蟲](../Page/昆蟲.md "wikilink")[腸道內](../Page/腸道.md "wikilink")。\[22\]

酵母菌屬於化能[異養](../Page/異營生物.md "wikilink")、兼性厭氧型微生物，能夠直接吸收利用多種[單醣分子](../Page/單醣.md "wikilink")，比如[葡萄糖](../Page/葡萄糖.md "wikilink")、[果糖等](../Page/果糖.md "wikilink")。一些酵母菌還能代謝利用[五碳糖](../Page/五碳糖.md "wikilink")\[23\]、[乙醇或者](../Page/乙醇.md "wikilink")[有机酸](../Page/有机酸.md "wikilink")。一部分[双糖](../Page/双糖.md "wikilink")，例如[蔗糖](../Page/蔗糖.md "wikilink")，能在[胞外酶作用下水解为单糖被吸收利用](../Page/胞外酶.md "wikilink")。\[24\]酵母菌不能直接利用淀粉等多糖类物质。因此，在啤酒酿制过程中，原料麦必须经过[糖化才能被酿酒酵母进一步发酵利用](../Page/糖化.md "wikilink")。\[25\]

許多酵母營[專性或](../Page/厭氧生物#專性厭氧生物.md "wikilink")[兼性](../Page/厭氧生物#兼性厭氧生物.md "wikilink")[好氧的生活方式](../Page/好氧生物.md "wikilink")，目前尚未發現[專性厭氧的酵母](../Page/厭氧生物#專性厭氧生物.md "wikilink")。在缺乏[氧氣時](../Page/氧氣.md "wikilink")，發酵型的酵母會進行缺氧[呼吸作用](../Page/呼吸作用.md "wikilink")，當中通過[糖酵解作用將葡萄糖轉化成丙酮酸](../Page/糖酵解.md "wikilink")，其後丙酮酸經脫碳作用脫去碳原子，形成乙醛，同時釋出CO<sub>2</sub>，乙醛再被於糖酵解作用產生的NADH<sub>2</sub>還原成乙醇並產生能量([ATP](../Page/ATP.md "wikilink"))。

  -
    C<sub>6</sub>H<sub>12</sub>O<sub>6</sub>→2C<sub>2</sub>H<sub>5</sub>OH
    + 2CO<sub>2</sub> + 2ATP

在[釀酒過程中](../Page/酿酒.md "wikilink")，乙醇被保留下來；在烤麵包或蒸饅頭的過程中，CO<sub>2</sub>將麵團發起，而酒精則揮發。在有氧條件下，酵母將葡萄糖經有氧呼吸(糖酵解→三羧酸循環)代謝生成CO<sub>2</sub>和H<sub>2</sub>O。

  -
    C<sub>6</sub>H<sub>12</sub>O<sub>6</sub> +
    6O<sub>2</sub>→6CO<sub>2</sub> + 6H<sub>2</sub>O + 30(32)ATP

有氧條件下，酵母菌往往能夠迅速出芽繁殖。

酵母菌的最適生長溫度各異，在自然pH或弱酸環境中生長生活力最高。畢竟酵母能在低pH(pH≈3)條件下生長。\[26\]

## 繁殖

酵母具有无性繁殖和有性繁殖两种方式。

### 无性繁殖

1.  出芽生殖（budding）
    在生长环境良好时，酵母菌迅速生长，几乎每个细胞外面都会产生芽体，而且芽体上会产生新的芽体。芽体逐渐长大成熟后与母体分离。
2.  分裂生殖（fission） 少数酵母菌如裂殖酵母属（Schizosaccharomyces）具有与细菌一样的二分裂繁殖方式。
3.  孢子生殖
    部分少数酵母菌如掷孢酵母属（Sporobolomyces）能在其营养细胞上长出小梗，小梗上产生掷孢子。孢子成熟后，通过一种特有的喷射机制将孢子喷出。\[27\]

一些酵母，如[假絲酵母](../Page/假丝酵母属.md "wikilink")（或稱[念珠菌](../Page/念珠菌.md "wikilink")，*Candida*）不能進行有性生殖，只能進行無性生殖。

### 有性繁殖

酵母以[子囊和](../Page/子囊.md "wikilink")[子囊孢子的形式进行](../Page/子囊孢子.md "wikilink")[有性生殖](../Page/有性生殖.md "wikilink")。一般通过临近的两个形态相同而性别不同的细胞各自伸出一个管状的原生质突起，经过接触、融合形成一条通道，通过质配、核配和减数分裂形成4个或8个核。然后他们分别与周围的原生质结合在一起，形成成熟的子囊孢子，原有的营养细胞变成了子囊。\[28\]

## 用途

酵母具备许多诱人的特征，广泛应用于工业、商品生产、环保以及科学研究领域。酿造酒精与面包烘培是酵母菌最常见、最古老的利用方式。此外,
许多酵母还能用于生产各类[饲料以及工业营养物](../Page/饲料.md "wikilink")，比如[SCP](../Page/SCP.md "wikilink")（Single
Cell
Protein）、[酵母提取物等](../Page/酵母提取物.md "wikilink")。某些酵母耐酸、耐高渗透、分解吸收有毒物质，同时被广泛应用于[污水处理领域](../Page/污水.md "wikilink")。\[29\]在科学研究上，[酿酒酵母](../Page/酿酒酵母.md "wikilink")（*Saccharomyces
cerevisiae*）作为[模式生物被使用](../Page/模式生物.md "wikilink")\[30\]；另外一些酵母已经被开发为[异源蛋白表达系统使用](../Page/异源蛋白表达系统.md "wikilink")，利用基因技术在酵母细胞内表达[外源蛋白质](../Page/外源蛋白质.md "wikilink")。

### 酒精饮料

酵母菌被广泛应用于[酒精饮料](../Page/酒精饮料.md "wikilink")，例如[啤酒](../Page/啤酒.md "wikilink")、[果酒](../Page/果酒.md "wikilink")、[蒸馏酒的生产中](../Page/蒸馏酒.md "wikilink")，酵母菌在无氧条件或低氧浓度条件下，消耗谷物、[水果等](../Page/水果.md "wikilink")[碳水化合物原料](../Page/碳水化合物.md "wikilink")，为自身提供[能量并产生](../Page/能量.md "wikilink")[酒精与](../Page/酒精.md "wikilink")[二氧化碳](../Page/二氧化碳.md "wikilink")。最常见的用于[啤酒与](../Page/啤酒.md "wikilink")[果酒酿造的](../Page/果酒.md "wikilink")[菌种为](../Page/菌种.md "wikilink")[酿酒酵母](../Page/酿酒酵母.md "wikilink")（*Saccharomyces
cerevisiae*）。\[31\]

#### 啤酒

[2009-03-21_Beer_brewing_bubbles.jpg](https://zh.wikipedia.org/wiki/File:2009-03-21_Beer_brewing_bubbles.jpg "fig:2009-03-21_Beer_brewing_bubbles.jpg")

用于酿造啤酒的酵母菌，根据[发酵类型的不同](../Page/发酵.md "wikilink")，主要分为两大类：**[爱尔酵母](../Page/爱尔酵母.md "wikilink")**（ale
yeast）与**[拉格酵母(窖藏酵母)](../Page/拉格酵母\(窖藏酵母\).md "wikilink")**（lager
yeast）。\[32\]爱尔酵母发酵期间会慢慢上升至啤酒表层，因此又称[顶層发酵酵母](../Page/顶層发酵.md "wikilink")（top
fermenting
yeast）。最常用的爱尔酵母为[啤酒酵母](../Page/啤酒酵母.md "wikilink")（*Saccharomyces
cerevisiae*）。\[33\]
由爱尔酵母发酵的啤酒有：[爱尔啤酒](../Page/爱尔啤酒.md "wikilink")、[麦啤](../Page/小麥啤酒.md "wikilink")、[司陶特](../Page/司陶特啤酒.md "wikilink")（stouts）等。

拉格酵母(窖藏酵母)用于[底层发酵](../Page/底层发酵.md "wikilink")（bottom
fermentation）。与[顶層发酵方法相比](../Page/顶層发酵.md "wikilink")，底层发酵往往采用较低的发酵[温度](../Page/温度.md "wikilink")，发酵时间较长。到发酵末期，酵母菌下沉于酒桶底部，由此啤酒酒色也较为透明。\[34\][卡尔斯博酵母](../Page/卡尔斯博酵母.md "wikilink")（*Saccharomyces
cerevisiae*）是一种典型与比较常用的拉格酵母(窖藏酵母)之一。现在，爱尔酵母与拉格酵母(窖藏酵母)已被重新归类于*S.
cerevisae*菌属。\[35\]

此外，还有许多种类的酵母菌应用在[酒精酿制中](../Page/酒精.md "wikilink")，以适应不同工艺与口感风味上的需要。目前。各种各样的育种技术被引进到优良菌种的选育中；基因工程菌技术的加入，赋予了酵母菌自然菌种所不具备的新特性。\[36\]有研究称，转入黑曲霉菌葡萄糖淀粉酶基因的酵母工程菌，能够更高效的分解利用原来中的淀粉。\[37\]

#### 葡萄酒

[Yeast_on_grapes.jpg](https://zh.wikipedia.org/wiki/File:Yeast_on_grapes.jpg "fig:Yeast_on_grapes.jpg")\]\]

传统[葡萄酒的酿造](../Page/葡萄酒.md "wikilink")，便是利用粘附于果皮上的天然酵母菌来酿制，此方法亦成为[自然发酵法](../Page/自然发酵.md "wikilink")。这些果皮上的菌种，其实是许多[微生物的](../Page/微生物.md "wikilink")“混合体”，某种程度上可认为其增加了酿酒过程及产品质量的许多不确定因素。因此，现在越来越多的[酿酒师和](../Page/酿酒师.md "wikilink")[酿酒厂会选择经](../Page/酿酒厂.md "wikilink")[分离纯化后的纯菌种进行发酵](../Page/分离菌种.md "wikilink")。\[38\]不同的酵母菌，可形成不同风味的[葡萄酒](../Page/葡萄酒.md "wikilink")。

### 外源蛋白表达系统

作为[真核生物](../Page/真核生物.md "wikilink")，[毕赤酵母具有高等真核表达系统的许多优点](../Page/毕赤酵母.md "wikilink")：如[糖基化](../Page/糖基化.md "wikilink")、[信号肽追加等](../Page/信号肽.md "wikilink")[后转译能力](../Page/后转译.md "wikilink")，且实验操作简单。它比[杆状病毒或](../Page/杆状病毒.md "wikilink")[哺乳动物组织培养等其它真核表达系统更快捷](../Page/哺乳动物.md "wikilink")、简单、廉价，且表达水平更高。同为酵母，毕赤酵母具有与[酿酒酵母相似的分子及遗传操作优点](../Page/酿酒酵母.md "wikilink")，且它的[外源蛋白](../Page/外源蛋白.md "wikilink")[表达水平是后者的十倍以至百倍](../Page/表达.md "wikilink")。这些使得毕赤酵母成为非常有用的蛋白表达系统。

### 烘焙面包

将酵母与面粉混合，加水加糖揉和，发酵30分钟左右。发酵后做成面包形状，再发酵30分钟左右，放入烤箱烘烤熟，[面包就可以食用了](../Page/面包.md "wikilink")。\[39\]

### 污水处理

## 致病性

[C_albicans_en.jpg](https://zh.wikipedia.org/wiki/File:C_albicans_en.jpg "fig:C_albicans_en.jpg")的显微镜照片
正在伸延的菌丝以及其他一些形态特征。\]\]

一般酵母菌被指认为是一种[条件致病菌](../Page/条件致病菌.md "wikilink")，特别容易对免疫力低下的病人造成感染。酵母菌感染属于[真菌感染中的一种形式](../Page/真菌感染.md "wikilink")。

[白色念珠菌](../Page/白色念珠菌.md "wikilink")（*Candida
albicans*）能够引起[鹅口疮以及](../Page/鹅口疮.md "wikilink")[尿道炎等感染疾病](../Page/尿道炎.md "wikilink")。白色念珠菌在人类身上主要出现在[口腔](../Page/口腔.md "wikilink")、[肠道](../Page/肠道.md "wikilink")、[尿道等部位的粘膜上](../Page/尿道.md "wikilink")，小部分生活在皮肤表面。正常情况下，念珠菌以[酵母细胞型存在](../Page/酵母细胞型.md "wikilink")，没有致病性；在一些因素的诱导下，比如免疫力缺陷、过量使用抗生素等，白色念珠菌大量转化为[菌丝生长型](../Page/菌丝型.md "wikilink")，并大量繁殖，入侵患者[粘膜系统](../Page/粘膜.md "wikilink")，引起[炎症而发病](../Page/炎症.md "wikilink")。在怀孕晚期服用避孕药的妇女中，极易感染[尿道炎](../Page/尿道炎.md "wikilink")，其中一个可能的诱因便是身体上的激素出现了失衡。\[40\]

[白色隐球菌](../Page/白色隐球菌.md "wikilink")（*Cryptococcus
albidus*）是一种一般对人类无害的出芽型酵母菌。但在[免疫系统缺陷者身上](../Page/免疫系统.md "wikilink")，可能感染病人引起一种名为[隐球菌病](../Page/隐球菌病.md "wikilink")（cryptococcosis）的疾病。\[41\]
另外，有案例显示，一位进行[免疫抑制治疗的病人肺部受到白色隐球菌的感染后](../Page/免疫抑制.md "wikilink")，导致出现[急性呼吸窘迫综合症](../Page/急性呼吸窘迫综合症.md "wikilink")(ARDS)的病症。\[42\]

[酿酒酵母](../Page/酿酒酵母.md "wikilink")（*Saccharomyces
sereviciae*）一般不被认为是条件性致病菌，但是也有少量的报告显示出酿酒酵母具有致病的能力。\[43\]\[44\]

## 益生酵母菌

虽然有些酵母菌是条件性致病菌，但是有益的[酵母菌属的](../Page/酵母屬.md "wikilink")[布拉酵母菌种](../Page/布拉酵母菌.md "wikilink")（学名：*Saccharomyces
boulardii*）可以防止甚至治疗一些[细菌导致的腹泻和感染性](../Page/细菌.md "wikilink")[肠炎](../Page/肠炎.md "wikilink")。\[45\]

## 參見

  - [焙烤](../Page/焙烤.md "wikilink")
  - [苏打](../Page/苏打.md "wikilink")
  - [基因重複](../Page/基因重複.md "wikilink")

## 参考文献

## 外部链接

  - [White Labs, 详细酿酒发酵菌种查询，
    各类酵母菌种与酿酒服务提供商](https://web.archive.org/web/20110903140230/http://www.whitelabs.com/index.html)
  - [BeerAdvovate, 详细酿酒发酵菌种查询， 各类酵母菌种与啤酒服务提供商](http://beeradvocate.com/)

[Category:膨鬆劑](../Category/膨鬆劑.md "wikilink")
[Category:酵母菌](../Category/酵母菌.md "wikilink")
[Category:藥用真菌](../Category/藥用真菌.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.
11.
12.

13.

14.

15.

16.
17.

18.

19.
20.
21.

22.

23.

24.

25.
26.

27.

28.
29.

30.

31.
32.

33.

34.
35.
36.

37.

38.

39.

40.

41.

42.

43.

44.

45.