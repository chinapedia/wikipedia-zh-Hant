**醉猿**（*Dionysopithecus*）是一屬史前的[靈長目](../Page/靈長目.md "wikilink")，生存在[中新世的](../Page/中新世.md "wikilink")[中國及](../Page/中國.md "wikilink")[巴基斯坦](../Page/巴基斯坦.md "wikilink")。

## 化石發現

醉猿的[化石最初是由](../Page/化石.md "wikilink")[李傳夔於](../Page/李傳夔.md "wikilink")1978年在[中國](../Page/中國.md "wikilink")[江蘇的](../Page/江蘇.md "wikilink")[雙溝發現](../Page/雙溝.md "wikilink")，化石包括了一個左[上頜骨連同幾顆](../Page/上頜骨.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")。\[1\]由於當時李傳夔是住在雙溝的一間酒廠內，故將這些化石以[希臘神話的酒神](../Page/希臘神話.md "wikilink")[狄俄倪索斯來命名](../Page/狄俄倪索斯.md "wikilink")，並以雙溝為[種小名](../Page/種小名.md "wikilink")。李傳夔同時間亦發現了[寬齒猿的化石](../Page/寬齒猿.md "wikilink")。

後來於1990年發現了另一些醉猿化石，並名為[東方醉猿](../Page/東方醉猿.md "wikilink")。在[巴基斯坦亦有發現一些醉猿的遺骸](../Page/巴基斯坦.md "wikilink")。\[2\]

## 分類問題

醉猿最初被認為是屬於[原康修爾猿科](../Page/原康修爾猿科.md "wikilink")，但更多的[化石發現顯示醉猿的](../Page/化石.md "wikilink")[牙齒更像](../Page/牙齒.md "wikilink")[樹猿](../Page/樹猿.md "wikilink")，故被分類在[上猿科中](../Page/上猿科.md "wikilink")。

## 演化歷史

醉猿的[化石屬於](../Page/化石.md "wikilink")[中新世早期](../Page/中新世.md "wikilink")，比[原康修爾猿的更早](../Page/原康修爾猿.md "wikilink")。不過學者一般都不認為醉猿就是現今[長臂猿的祖先](../Page/長臂猿.md "wikilink")，而是介乎[猿與](../Page/猿.md "wikilink")[人類之間的分支](../Page/人類.md "wikilink")。有些學者認為醉猿的祖先是生活在[非洲](../Page/非洲.md "wikilink")，並遷徙至[歐洲](../Page/歐洲.md "wikilink")[演化為](../Page/演化.md "wikilink")[上猿](../Page/上猿.md "wikilink")。而上猿亦進而擴散至[亞洲](../Page/亞洲.md "wikilink")，成為醉猿及[寬齒猿](../Page/寬齒猿.md "wikilink")。

## 參考

[Category:上猿科](../Category/上猿科.md "wikilink")

1.
2.