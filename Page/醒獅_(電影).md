是一部[香港電影](../Page/香港電影.md "wikilink")，2007年4月26日在[香港上映](../Page/香港.md "wikilink")。由[吳鎮宇和](../Page/吳鎮宇.md "wikilink")[麥子善擔任導演](../Page/麥子善.md "wikilink")。\[1\]
[編劇是](../Page/編劇.md "wikilink")[林超榮](../Page/林超榮.md "wikilink")。\[2\]
演員有[吳鎮宇](../Page/吳鎮宇.md "wikilink")、[黃秋生](../Page/黃秋生.md "wikilink")、[毛舜筠](../Page/毛舜筠.md "wikilink")、[林苑及](../Page/林苑.md "wikilink")[林子聰等](../Page/林子聰.md "wikilink")。

## 演員

  - [吳鎮宇](../Page/吳鎮宇.md "wikilink") 飾 阿雞
  - [黃秋生](../Page/黃秋生.md "wikilink") 飾 舅公
  - [毛舜筠](../Page/毛舜筠.md "wikilink") 飾 三妹
  - [林子聰](../Page/林子聰.md "wikilink") 飾 阿九
  - [林　苑](../Page/林苑.md "wikilink") 飾 嘉碧

### 客串

  - [鄭中基](../Page/鄭中基.md "wikilink") 飾 打火基，令壽堂舞獅者
  - [森　美](../Page/森美.md "wikilink") 飾 油占多，架勢堂舞獅者
  - [張敬軒](../Page/張敬軒.md "wikilink") 飾 小張，電視台記者

## 参考文献

## 外部連結

  - [《醒獅》官方網站](https://web.archive.org/web/20070713164643/http://www.dancinglion.hk/)

  - {{@movies|fdhk21008039|醒獅}}

  -
  -
  -
  -
  -
  -
[Category:香港喜劇片](../Category/香港喜劇片.md "wikilink")
[7](../Category/2000年代香港電影作品.md "wikilink")
[Category:舞獅題材作品](../Category/舞獅題材作品.md "wikilink")

1.  [《醒獅》官方網站](http://www.dancinglion.hk/)
2.  [吳鎮宇執導的《醒獅》又名《搶錢家族
    (香港)》。](http://www.xyent.cn/012007/14/110617.html)