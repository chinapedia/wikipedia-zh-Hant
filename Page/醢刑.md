**醢刑**，一作**酼刑**，也称**菹醢**，是[中國古代的一種](../Page/中國.md "wikilink")[酷刑之一](../Page/酷刑.md "wikilink")，指將是把犯人殺死後，把屍體剁成醢（，即[肉醬](../Page/肉醬.md "wikilink")）。相傳這種刑罰是由[商紂王所創](../Page/商紂王.md "wikilink")，用以對付[九侯](../Page/九侯.md "wikilink")。但也有對於活人使用者。

## 著名受刑人

  - [伯邑考](../Page/伯邑考.md "wikilink")：[西伯之子](../Page/西伯.md "wikilink")，因激怒[妲己](../Page/妲己.md "wikilink")，而被[商紂王所殺](../Page/商紂王.md "wikilink")；死後，屍體處以醢刑。
  - [九侯](../Page/九侯.md "wikilink")：殷商時諸侯，又稱「鬼侯」，[商纣王](../Page/商纣王.md "wikilink")[三公之一](../Page/三公.md "wikilink")，據說他覺得其女兒很美，於是送入紂王宮中當妃子，但紂王卻覺得她很醜，不甘受騙，殺了九侯，還把他的屍體處以醢刑\[1\]。
  - [南宮萬](../Page/南宮萬.md "wikilink")：[春秋時期](../Page/春秋時期.md "wikilink")[宋國政治人物](../Page/宋國.md "wikilink")，殺死[宋閔公](../Page/宋閔公.md "wikilink")，被宋人醢之。
  - [子路](../Page/子路.md "wikilink")：[孔子弟子](../Page/孔子.md "wikilink")，[衛後莊公與其子](../Page/衛後莊公.md "wikilink")[衛出公爭位](../Page/衛出公.md "wikilink")，挾持孔悝，子路為了救[孔悝而與](../Page/孔悝.md "wikilink")[衛後莊公的家臣戰鬥](../Page/衛後莊公.md "wikilink")，戰死。屍首處以醢刑\[2\]。
  - [子之](../Page/子之.md "wikilink")，燕國相國，受[燕王噲之](../Page/燕王噲.md "wikilink")[禪讓](../Page/禪讓.md "wikilink")，公元前314年，齊國攻擊燕國，子之出逃，被齊軍俘虜，被處以醢刑（剁成肉醬）。
  - [彭越](../Page/彭越.md "wikilink")：[汉高祖时](../Page/汉高祖.md "wikilink")[梁王](../Page/梁王.md "wikilink")，[消滅異姓王風潮中被](../Page/消滅異姓王風潮.md "wikilink")[流放](../Page/流放.md "wikilink")，他向[呂后求情](../Page/呂后.md "wikilink")，呂后卻將他[斬首](../Page/斬首.md "wikilink")，屍首處以醢刑。
  - [兰京](../Page/兰京.md "wikilink")：北齐文襄帝[高澄的厨师](../Page/高澄.md "wikilink")，谋杀高澄，被处以醢刑。
  - [来俊臣](../Page/来俊臣.md "wikilink")：[武曌时大臣](../Page/武曌.md "wikilink")，被武曌殺死，屍體處以醢刑。
  - [张钧](../Page/张钧_\(金\).md "wikilink")：[金熙宗時](../Page/金熙宗.md "wikilink")[文字狱受害者](../Page/文字狱.md "wikilink")\[3\]。

## 參見

  - [凌遲](../Page/凌遲.md "wikilink")

## 注释

[Category:處決方法](../Category/處決方法.md "wikilink")
[Category:中国古代刑罚](../Category/中国古代刑罚.md "wikilink")

1.  《戰國策•趙策三》：「昔者鬼侯、鄂侯、文王，紂之三公也。鬼侯有子而好，故入之於紂。以為惡，醢鬼侯。」
2.  《禮記‧檀弓上》：『孔子哭子路於中庭。有人吊者，而夫子拜之。既哭，進使者而問故。使者曰：「醢之矣。」遂命覆醢。』
3.  《[金史](../Page/金史.md "wikilink")·佞幸列传》：『萧肄，本奚人，有宠于熙宗，复谄事悼后，累官参知政事。皇统九年四月壬申夜，大风雨，雷电震坏寝殿鸱尾，有火自外入，烧内寝帏幔。帝徙别殿避之，欲下诏罪己。翰林学士张钧视草。钧意欲奉答天戒，当深自贬损，其文有曰：「惟德弗类，上干天威」及「顾兹寡昧眇予小子」等语。肄译奏曰：「弗类是大无道，寡者孤独无亲，昧则于人事弗晓，眇则目无所见，小子婴孩之称，此汉人托文字以詈主上也。」帝大怒，命卫士拽钧下殿，榜之数百，不死。以手剑剺其口而醢之。』