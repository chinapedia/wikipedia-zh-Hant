**釀酒酵母**（[学名](../Page/学名.md "wikilink")：**，又稱**麵包酵母**或者**啤酒酵母**，**出芽酵母**。釀酒酵母是與人類關係最廣泛的一種[酵母](../Page/酵母.md "wikilink")，不僅因爲傳統上它用於製作[麵包和](../Page/麵包.md "wikilink")[饅頭等食品及](../Page/饅頭.md "wikilink")[釀酒](../Page/釀酒.md "wikilink")，在現代[分子和](../Page/分子生物學.md "wikilink")[細胞生物學中用作](../Page/細胞生物學.md "wikilink")[真核](../Page/真核生物.md "wikilink")[模式生物](../Page/模式生物.md "wikilink")，其作用相當於[原核的模式生物](../Page/原核.md "wikilink")[大腸桿菌](../Page/大腸桿菌.md "wikilink")。釀酒酵母是[發酵中最常用的生物種類](../Page/發酵.md "wikilink")。釀酒酵母的細胞爲球形或者卵形，直徑5–10
[μm](../Page/微米.md "wikilink")。其繁殖的方法爲[出芽生殖](../Page/出芽生殖.md "wikilink")\[1\]。

許多在人類生物學中重要的[蛋白質被發現是首先通過在酵母中研究它們的](../Page/蛋白質.md "wikilink")[同源蛋白質](../Page/同源.md "wikilink");
這些蛋白質包括[細胞週期蛋白](../Page/細胞週期.md "wikilink")，[信號蛋白和蛋白質加工](../Page/细胞信号传送.md "wikilink")[酶等](../Page/酶.md "wikilink")。

## 生活史

酵母的細胞有兩種生活形態，[單倍體和](../Page/單倍體.md "wikilink")[二倍體](../Page/二倍體.md "wikilink")。單倍體的生活史較簡單，通過[有絲分裂繁殖](../Page/有絲分裂.md "wikilink")。在環境壓力較大時通常則死亡。二倍體細胞（酵母的優勢形態）也通過簡單的有絲分裂繁殖，但在外界條件不佳時能夠進入[減數分裂](../Page/減數分裂.md "wikilink")，生成一系列單倍體的[孢子](../Page/孢子.md "wikilink")。單倍體可以[交配](../Page/交配.md "wikilink")，重新形成二倍體。酵母有兩種交配類型，稱作**a**和**α**，是一種原始的性別分化，因此很有研究價值。

## 基因組

釀酒酵母是第一個完成基因組[測序的真核生物](../Page/測序.md "wikilink")\[2\]，其測序结果已经於1996年4月24日公布到[公有领域](../Page/公有领域.md "wikilink")。此后又有许多酵母测序结果计入，这一数据库包含有酵母基因組的詳細[註釋](../Page/註釋.md "wikilink")(annotation)，是研究真核細胞遺傳學和生理學的重要工具。另一個重要的釀酒酵母數據庫\[[http://mips.gsf.de/genre/proj/yeast/index.jsp\]由慕尼黑蛋白質序列信息中心（MIPS）維護](http://mips.gsf.de/genre/proj/yeast/index.jsp%5D由慕尼黑蛋白質序列信息中心（MIPS）維護)。

釀酒酵母的[基因組包含大約](../Page/基因組.md "wikilink")12,156,677个[鹼基對](../Page/鹼基對.md "wikilink")，分成16組[染色體](../Page/染色體.md "wikilink")，共有6275個基因，其中可能約有5800個真正具有功能。据估計其基因約有31%與[人類](../Page/人.md "wikilink")[同源](../Page/同源.md "wikilink")\[3\]。

## 在科學中的作用

因爲釀酒酵母與同爲真核生物的[動物和](../Page/動物.md "wikilink")[植物細胞具有很多相同的結構](../Page/植物.md "wikilink")，又容易培養，酵母被用作研究真核生物的模式生物，也是目前被人們了解最多的生物之一。在人體中重要的[蛋白質很多都是在酵母中先被發現其](../Page/蛋白質.md "wikilink")[同源物的](../Page/同源.md "wikilink")，其中包括有關[細胞周期的蛋白](../Page/細胞周期.md "wikilink")、信號蛋白和蛋白質加工酶。

釀酒酵母也是製作[培養基中常用成分](../Page/培養基.md "wikilink")[酵母提取物的主要原料](../Page/酵母提取物.md "wikilink")。

## 營養酵母

由於釀酒酵母在日常飲食中的廣泛應用，不少營養食品生產商把食用的釀酒酵母製成營養產品，被稱為「營養酵母」。例如：**啤酒酵母**就是其中一種最常見的營養酵母，被認為可作[維他命B群的供給](../Page/維他命B群.md "wikilink")。

## 参考资料

## 外部連結

  - [釀酒酵母基因組數據庫](http://www.yeastgenome.org/)

  - [慕尼黑蛋白質序列信息中心](http://mips.gsf.de/genre/proj/yeast/index.jsp)

  - [描述酵母基因組文章的摘要](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&list_uids=8849441&dopt=Abstract)

  - [酵母的信息概要](https://web.archive.org/web/20090226151906/http://www.yeastgenome.org/VL-what_are_yeast.html)

  - [UniProt](../Page/UniProt.md "wikilink") –
    [釀酒酵母](http://www.uniprot.org/uniprot/?query=organism:4932+AND+reviewed:yes)

  -
[Category:细菌学](../Category/细菌学.md "wikilink")
[Category:消化系统](../Category/消化系统.md "wikilink")
[Category:益生菌](../Category/益生菌.md "wikilink")
[Category:酵母屬](../Category/酵母屬.md "wikilink")
[Category:模式生物](../Category/模式生物.md "wikilink")
[Category:嗜高渗生物](../Category/嗜高渗生物.md "wikilink")
[Category:烘焙](../Category/烘焙.md "wikilink")
[Category:酿造用酵母](../Category/酿造用酵母.md "wikilink")
[Category:膨鬆劑](../Category/膨鬆劑.md "wikilink")
[Category:酿酒学](../Category/酿酒学.md "wikilink")
[Category:微生物学](../Category/微生物学.md "wikilink")
[Category:已测序的基因组](../Category/已测序的基因组.md "wikilink")

1.
2.
3.