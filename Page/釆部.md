[釆-order.gif](https://zh.wikipedia.org/wiki/File:釆-order.gif "fig:釆-order.gif")
**釆部**，為漢字索引中的[部首之一](../Page/部首.md "wikilink")，[康熙字典](../Page/康熙字典.md "wikilink")214個部首中的第一百六十五個（七劃的則為第十九個）。就[繁體和](../Page/繁體.md "wikilink")[簡體中文中](../Page/簡體.md "wikilink")，釆部歸於七劃部首。釆部只以左方為部字。且無其他部首可用者將部首歸為釆部。

## 部首單字解釋

辨別。獸類的蹄爪分別。辨的本字。見[說文](../Page/說文.md "wikilink")。

  - 補充：「采」字从爪从木，象手在樹上採摘果實之形。\[1\]「釆」字說文以為象獸爪，學者或以為从手从少（沙），象手在沙中摸索分辨之形\[2\]；或以為从力从數點，象農具與種子，會播種之義等。\[3\]雖「釆」字尚無定說，但可以確定的是，和「采」字完全不同。

## 字形

[File:釆-oracle.svg|甲骨文](File:釆-oracle.svg%7C甲骨文)
[File:釆-bronze.svg|金文](File:釆-bronze.svg%7C金文)
[File:釆-bigseal.svg|大篆](File:釆-bigseal.svg%7C大篆)
[File:釆-seal.svg|小篆](File:釆-seal.svg%7C小篆)

## 字例

（依[Unicode排名](../Page/Unicode.md "wikilink")）

| 除部首外之筆劃 | 字例 -{ |
| :-----: | :---: |
|   \+0   |   釆   |
|   \+1   |   采   |
|   \+4   |   釈   |
|   \+5   |  釉释   |
|   \+7   |   𨤔   |
|  \+13   | 釋 }-  |

## 資料來源

[Category:部首](../Category/部首.md "wikilink")

1.
2.
3.