**里加**（；）是[拉脫維亞的](../Page/拉脫維亞.md "wikilink")[首都](../Page/首都.md "wikilink")，[北欧地区](../Page/北欧.md "wikilink")[波羅的海國家中最大](../Page/波羅的海國家.md "wikilink")、最繁忙的[城市](../Page/城市.md "wikilink")，位於[波羅的海岸邊](../Page/波羅的海.md "wikilink")，[道加瓦河口](../Page/道加瓦河.md "wikilink")。

里加是[波羅的海國家主要的政治](../Page/波羅的海國家.md "wikilink")、經濟、文化、教育和工商業中心。里加是历史上[汉萨同盟主要商业城市](../Page/汉萨同盟.md "wikilink")，其歷史中心被列入[世界遺產名录](../Page/世界遺產.md "wikilink")，[聯合國教科文組織特別認為里加豐富的](../Page/聯合國教科文組織.md "wikilink")[新藝術運動](../Page/新藝術運動.md "wikilink")[建築是世界上獨一無二的](../Page/建築.md "wikilink")\[1\]。

[欧盟欧洲电子通讯监管机构总部以及](../Page/欧盟.md "wikilink")[北大西洋公约组织的战略通讯中心设在里加](../Page/北大西洋公约组织.md "wikilink")。里加是[欧洲城市组织及欧洲首都联盟组织成员](../Page/欧洲城市组织.md "wikilink")。2014年[欧洲文化之都](../Page/欧洲文化之都.md "wikilink")，[世界合唱比赛](../Page/世界合唱比赛.md "wikilink")，2006年北大西洋公约组织峰会，2003年[欧洲歌唱大赛](../Page/欧洲歌唱大赛.md "wikilink")，2006年世界男子冰球锦标赛，2003年世界女子冰壶球锦标赛等活动的举办地。

## 歷史

[Riga_wodcut_1575.gif](https://zh.wikipedia.org/wiki/File:Riga_wodcut_1575.gif "fig:Riga_wodcut_1575.gif")
里加位於古代[利沃尼亞人](../Page/利沃尼亞人.md "wikilink")（屬於[芬蘭族](../Page/芬蘭族.md "wikilink")）的居住地上，位於[道加瓦河與](../Page/道加瓦河.md "wikilink")[里加河的交會處](../Page/里加河.md "wikilink")，並曾存在過一個稱為里加湖的景觀，雖然它現在已經不復存在了\[2\]。里加這個名稱也是從里加河而來的\[3\]。

歷史學家認為現代的里加是由[德國商人傭兵與](../Page/德國.md "wikilink")[十字軍在](../Page/十字軍.md "wikilink")12世紀後期抵達此地所奠基的，當時因為這個地區人口稀少，於是對於[傳教士傳播當地人民](../Page/傳教士.md "wikilink")[基督教是一個良好的機會](../Page/基督教.md "wikilink")。德國商人則是在1158年建立與[波羅的人貿易的前哨站](../Page/波羅的人.md "wikilink")，而[奧斯定會僧侶則在](../Page/奧斯定會.md "wikilink")1190年在此設立[修道院](../Page/修道院.md "wikilink")。

[里加的亞伯特在](../Page/里加的亞伯特.md "wikilink")1199年成為[利沃尼亞教區的主教](../Page/利沃尼亞教區.md "wikilink")，他在1200年帶領23艘船艦及500名[十字軍抵達里加](../Page/十字軍.md "wikilink")，並成立[聖劍兄弟騎士團](../Page/聖劍兄弟騎士團.md "wikilink")（Livonian
Brothers of the Sword）\[4\]。

里加提供了西歐與波羅的人或[俄羅斯的貿易路線](../Page/俄羅斯.md "wikilink")，後來里加也在1282年加入[漢薩同盟](../Page/漢薩同盟.md "wikilink")。漢薩同盟也讓里加的政治與經濟更加穩定，讓里加可以度過後來的政治動盪。

[Riga_1650.jpg](https://zh.wikipedia.org/wiki/File:Riga_1650.jpg "fig:Riga_1650.jpg")

里加在1522年接受了[宗教改革的主張](../Page/宗教改革.md "wikilink")，結束了大主教的統治權力。隨著[條頓騎士團在](../Page/條頓騎士團.md "wikilink")1561年沒落，里加成為自由城市達20年之久。直到1581年時，里加受到[波蘭立陶宛聯邦的影響](../Page/波蘭立陶宛聯邦.md "wikilink")。里加在1621年受到[瑞典國王](../Page/瑞典.md "wikilink")[古斯塔夫二世](../Page/古斯塔夫二世.md "wikilink")(當時他參加了[三十年戰爭](../Page/三十年戰爭.md "wikilink"))的統治。在1656年至1658年的[俄羅斯-瑞典戰爭期間](../Page/俄羅斯-瑞典戰爭.md "wikilink")，里加受到俄羅斯的攻擊。直到1710年時，里加都是瑞典最大的城市，並且保持自治的地位。不過在1710年，在[大北方戰爭的影響之下](../Page/大北方戰爭.md "wikilink")，里加受到[沙皇](../Page/沙皇.md "wikilink")[彼得大帝的侵犯](../Page/彼得大帝.md "wikilink")。俄羅斯與瑞典後來簽訂[尼斯塔德條約](../Page/尼斯塔德條約.md "wikilink")，這也象徵瑞典統治時期的結束，俄羅斯霸權的開始。因為尼斯塔德條約，里加成為[俄羅斯帝國的領土](../Page/俄羅斯帝國.md "wikilink")，變成一座工業化的港口都市。這樣的狀態持續到了[第一次世界大戰後才結束](../Page/第一次世界大戰.md "wikilink")。

雖然波羅的海地區經過長久的改變與戰爭，[波羅的海德國人仍然在里加擁有優越的地位](../Page/波羅的海德國人.md "wikilink")。在幾個世紀裡，里加都使用[德語為官方語言](../Page/德語.md "wikilink")，直到[俄語在](../Page/俄語.md "wikilink")1891年成為新官方語言為止（因為俄羅斯帝國推行[俄羅斯化的緣故](../Page/俄羅斯化.md "wikilink")）。於是，拉脫維亞人從19世紀中葉開始取代德國人成為里加人口最多的種族。也因為拉脫維亞[資產階級的崛起使得里加成為](../Page/資產階級.md "wikilink")[拉脫維亞民族復興的中心](../Page/拉脫維亞民族復興.md "wikilink")。1905年發生的[俄羅斯革命](../Page/俄羅斯革命.md "wikilink")（由[拉脫維亞社會民主工人黨](../Page/拉脫維亞社會民主工人黨.md "wikilink")
（Latvian Social Democratic Workers'
Party）所主導）則達到[社會主義活動的巔峰](../Page/社會主義.md "wikilink")。

里加緊接著受到[第一次世界大戰與](../Page/第一次世界大戰.md "wikilink")[1917年俄國革命的影響](../Page/1917年俄國革命.md "wikilink")。德國軍隊在1917年進入里加，而1918年簽訂的[布列斯特-立陶夫斯克條約則將](../Page/布列斯特-立陶夫斯克條約.md "wikilink")[拉脫維亞割讓給德國](../Page/拉脫維亞.md "wikilink")。不過因為1918年11月簽訂的[康邊停戰協定](../Page/康邊停戰協定.md "wikilink")，德國放棄拉脫維亞的主權，促使拉脫維亞與其他波羅的海國家宣布獨立。拉脫維亞在1918年11月18日正式宣布獨立，里加於是成為拉脫維亞的首都。

[Riga_old.jpg](https://zh.wikipedia.org/wiki/File:Riga_old.jpg "fig:Riga_old.jpg")
在兩次世界大戰之間（1918年-1940年），拉脫維亞將注意力從俄國轉向[西歐國家](../Page/西歐.md "wikilink")，建立了一個民主的議會制政府。[英國與](../Page/英國.md "wikilink")[德國取代俄羅斯成為拉脫維亞主要的貿易夥伴](../Page/德國.md "wikilink")。

在[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，里加在1941年被[蘇聯所佔領](../Page/蘇聯.md "wikilink")，而[納粹德國則在](../Page/納粹德國.md "wikilink")1941年至1944年間佔領這個城市。在這段時期，[波羅的海德國人被強制送回德國](../Page/波羅的海德國人.md "wikilink")，而[猶太人則被迫進入猶太人社區](../Page/猶太人.md "wikilink")，並在[開瑟瓦德](../Page/開瑟瓦德.md "wikilink")（Kaiserwald）與Salaspils建造[集中營](../Page/集中營.md "wikilink")。

[紅軍後來再度於](../Page/苏联红军.md "wikilink")1945年佔領里加，當第二次世界大戰結束時，里加幾乎失去3分之一的人口。里加也開始[工業化](../Page/工業化.md "wikilink")，並從拉托維亞其他地區湧入大量的人口進入里加，特別是[俄羅斯人](../Page/俄羅斯人.md "wikilink")，並因此改變人口結構。

[廉價航空公司航線在](../Page/廉價航空公司.md "wikilink")2004年開始服務里加，並因此逐漸吸引大量的觀光客進入\[5\]。

## 氣候

里加處於[大陸性濕潤氣候区域](../Page/大陸性濕潤氣候.md "wikilink")，一、二月是最寒冷的季節，平均[溫度為](../Page/溫度.md "wikilink")-6 °C，不過每年最寒冷的時候幾乎都會達到-20 °C
to
-25 °C之間。因為靠近[海洋](../Page/海洋.md "wikilink")，所以[秋季多霧](../Page/秋季.md "wikilink")，降雨也比較頻繁。里加的[夏季則是溫暖潮濕](../Page/夏季.md "wikilink")，平均[溫度為](../Page/溫度.md "wikilink")18 °C，最高溫通常會超過30 °C。

## 經濟

商業與旅遊業對於里加近年來的經濟成長有相當重要的影響。大部分前往里加的觀光客是經由[里加國際機場](../Page/里加國際機場.md "wikilink")，這是波羅的海國家中最大的[機場](../Page/機場.md "wikilink")，它曾在2001年因慶祝里加建城800年而進行整修與現代化。里加在最近幾年將會產生重大的改變。[拉脫維亞國家圖書館](../Page/拉脫維亞國家圖書館.md "wikilink")（Latvijas
Nacionālā
bibliotēka）開始於2007年秋天建造，並預計在2010年完工\[6\]。對於道加瓦河左岸中心區域的開發計畫也在市議會進行討論。而在Ķīpsala地區所進行的摩天大樓建設則產生相當大的爭議。達文西複合建築（25層）與2棟稱為Z塔（30層）的[高層建築已經開始進行建設](../Page/高層建築.md "wikilink")\[7\]\[8\]。拉脫維亞幾乎所有重要的金融機構都位於里加，包括拉脫維亞的中央銀行－[拉脫維亞銀行](../Page/拉脫維亞銀行.md "wikilink")（Latvijas
Banka）。近年來，外國的商業貿易活動在里加也逐漸增加，尤其是在2004年5月1日拉脫維亞加入[歐盟之後](../Page/歐盟.md "wikilink")。拉脫維亞有超過50%的公司在里加進行登記\[9\]。里加港也是一個重要的貨物航運中心。它是波羅的海地區一個主要的港口，而且在最近幾年內預期會因與其他波羅的海國家的貿易而成長。

## 交通

[里加國際機場是里加唯一](../Page/里加國際機場.md "wikilink")[機場](../Page/機場.md "wikilink")，並有提供商業服務。從1993年至2004年間，里加的空中交通成長了2倍。渡輪航線往返里加、[基爾](../Page/基爾.md "wikilink")、[呂貝克與](../Page/呂貝克.md "wikilink")[斯德哥爾摩之間](../Page/斯德哥爾摩.md "wikilink")。里加也是木材運輸的主要轉運[港口](../Page/港口.md "wikilink")，也是拉脫維亞的[鐵路運輸中心](../Page/鐵路.md "wikilink")。里加南部地區一座新的[橋樑在](../Page/橋樑.md "wikilink")2008年完工，並在11月7日通車\[10\]。這座橋樑是波羅的海國家近年來最大的建築計畫，將可以減少城市中心的交通阻塞\[11\]。另一個大型的建築計畫是在北邊的走廊上\[12\]，預計在2010年開始。

## 人口

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>人口</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1767年</p></td>
<td><p>19,500</p></td>
</tr>
<tr class="even">
<td><p>1800年</p></td>
<td><p>29,500</p></td>
</tr>
<tr class="odd">
<td><p>1840年</p></td>
<td><p>60,000</p></td>
</tr>
<tr class="even">
<td><p>1867年</p></td>
<td><p>102,600</p></td>
</tr>
<tr class="odd">
<td><p>1881年</p></td>
<td><p>169,300</p></td>
</tr>
<tr class="even">
<td><p>1897年</p></td>
<td><p>282,200</p></td>
</tr>
<tr class="odd">
<td><p>1913年</p></td>
<td><p>517,500</p></td>
</tr>
<tr class="even">
<td><p>1920年</p></td>
<td><p>¹185,100</p></td>
</tr>
<tr class="odd">
<td><p>1930年</p></td>
<td><p>377,900</p></td>
</tr>
<tr class="even">
<td><p>1940年</p></td>
<td><p>353,800</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>人口</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1941年</p></td>
<td><p>335,200</p></td>
</tr>
<tr class="even">
<td><p>1945年</p></td>
<td><p>²228,200</p></td>
</tr>
<tr class="odd">
<td><p>1950年</p></td>
<td><p>482,300</p></td>
</tr>
<tr class="even">
<td><p>1955年</p></td>
<td><p>566,900</p></td>
</tr>
<tr class="odd">
<td><p>1959年</p></td>
<td><p>580,400</p></td>
</tr>
<tr class="even">
<td><p>1965年</p></td>
<td><p>665,200</p></td>
</tr>
<tr class="odd">
<td><p>1970年</p></td>
<td><p>731,800</p></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td><p>795,600</p></td>
</tr>
<tr class="odd">
<td><p>1979年</p></td>
<td><p>835,500</p></td>
</tr>
<tr class="even">
<td><p>1987年</p></td>
<td><p>900,300</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>人口</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1990年</p></td>
<td><p>909,135</p></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p>900,455</p></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p>889,741</p></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p>863,657</p></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p>843,552</p></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p>824,988</p></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p>810,172</p></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p>797,947</p></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p>786,612</p></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p>776,008</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>人口</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2000年</p></td>
<td><p>764,329</p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p>756,627</p></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>747,157</p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>739,232</p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>735,241</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>731,762</p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>727,578</p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>722,485</p></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p>719,613</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

里加在2008年的人口為719,613人，也是波羅的海國家中最大的城市，雖然自從1991年開始人口就逐漸減少\[13\]。造成人口減少的主要因素是人口外移與低的生育率。有些人估計里加的人口在2050年時可能只剩下目前的50%\[14\]。根據2008年資料顯示，拉脫維亞人佔總人口的42.3%，俄羅斯人則佔41.7%，[白俄羅斯人佔](../Page/白俄羅斯人.md "wikilink")4.3%，[烏克蘭人佔](../Page/烏克蘭人.md "wikilink")3.9%，[波蘭人為](../Page/波蘭人.md "wikilink")2.0%，其他人種合計為5.8%\[15\]。而2006年時的人口組成則是：拉脫維亞人佔總人口的59%，俄羅斯人則佔28.5%，白俄羅斯人佔3.8%，烏克蘭人佔2.5%，波蘭人2.0%，1.4%則是[立陶宛人](../Page/立陶宛人.md "wikilink")，其他人種合計為2.4%\[16\]。因為拉脫維亞在1991年獨立的緣故，蘇俄時代的移民不再自動取得公民權，其中一些人於是遷居至其他地區，造成人口減少。從1989年至2007年之間，因為一些移民的遷徙造成俄羅斯人從47.3%降低到42.1%，而拉脫維亞人則從36.5%上升到42.3%。拉脫維亞人在2006年成為里加人口最多的種族\[17\]。

## 教育

里加擁有的众多高等[學術教育機構](../Page/學術.md "wikilink")，
[拉脫維亞大學](../Page/拉脫維亞大學.md "wikilink")，
[拉脱维亚美术学院](../Page/拉脱维亚美术学院.md "wikilink") ([Latvijos
dailės akademija](../Page/Latvijos_dailės_akademija.md "wikilink"))，
[里加工業大學](../Page/里加工業大學.md "wikilink")，
[斯特拉金什大学](../Page/斯特拉金什大学.md "wikilink")，
[斯德哥爾摩經濟學院-{}-里加分校](../Page/斯德哥爾摩經濟學院里加分校.md "wikilink")（Rigas
Ekonomikas Augstskola）， [里加法律高等研究院](../Page/里加法律高等研究院.md "wikilink")
(Rīgas Juridiskā augstskola)， [商业和金融学院](../Page/商业和金融学院.md "wikilink")
( Banku Augstskola )， [图立巴大学](../Page/图立巴大学.md "wikilink") (Turiba
University)

## 著名景點

  - [里加主教座堂](../Page/里加主教座堂.md "wikilink")（Dome
    Cathedral）：[新教的主教座堂](../Page/新教.md "wikilink")，可能是波羅的海國家中最大的[教堂](../Page/教堂.md "wikilink")，建於13世紀，歷史上曾經數次改建。它擁有一台1844年的華麗[管風琴](../Page/管風琴.md "wikilink")。
  - [里加聖雅各伯主教座堂](../Page/里加聖雅各伯主教座堂.md "wikilink")：[罗马天主教的主教座堂](../Page/罗马天主教.md "wikilink")。
  - [里加圣诞主教座堂](../Page/里加圣诞主教座堂.md "wikilink")：[东正教的主教座堂](../Page/东正教.md "wikilink")。
  - [里加聖彼得教堂](../Page/里加聖彼得教堂.md "wikilink")：擁有123[公尺高的塔樓](../Page/公尺.md "wikilink")。
  - 聖約翰教堂（St. John's Church）：一棟13世紀小教堂，位於聖彼得教堂的後方。
  - [里加城堡](../Page/里加城堡.md "wikilink")（Rīgas
    Pils）：內部為[拉脫維亞歷史博物館](../Page/拉脫維亞歷史博物館.md "wikilink")、[外國藝術博物館及總統官邸](../Page/外國藝術博物館.md "wikilink")。
  - 火藥塔（Pulvertornis）：里加古城牆上惟一保留下來的塔樓，內部是[拉脫維亞戰爭博物館](../Page/拉脫維亞戰爭博物館.md "wikilink")。
  - 木造建築
  - [拉脫維亞被佔領時期博物館](../Page/拉脫維亞被佔領時期博物館.md "wikilink")（Latvijas
    okupācijas muzejs）：保存1940年至1991年間佔領拉脫維亞各種勢力的相關文件。
  - [里加廣播電視塔](../Page/里加廣播電視塔.md "wikilink")（Rīgas radio un televīzijas
    tornis）：為[歐洲第](../Page/歐洲.md "wikilink")3高塔。
  - [里加動物園](../Page/里加動物園.md "wikilink")
  - [里加馬戲團](../Page/里加馬戲團.md "wikilink")：唯一一個永久設立在波羅的海國家的[馬戲團](../Page/馬戲團.md "wikilink")。
  - [里加歷史及航海博物館](../Page/里加歷史及航海博物館.md "wikilink")（Rīgas vēstures un
    kuģniecības muzejs）：是里加及波羅的海國家中最大及最古老的博物館。
  - [自由纪念碑](../Page/自由纪念碑.md "wikilink")（Brīvības
    piemineklis），1935年揭幕，为纪念在拉脱维亚独立战争（1918年－1920年）期间阵亡的军人而兴建，是拉脱维亚的自由、独立和主权的重要象征。
  - 在[阿尔伯特街等街道](../Page/阿尔伯特街_\(里加\).md "wikilink")，拥有世界一流的[新艺术运动风格的精美建筑](../Page/新艺术运动.md "wikilink")。

## 知名人物

  - [以赛亚·伯林](../Page/以赛亚·伯林.md "wikilink") （Sir Isaiah Berlin)
    20世纪著名哲学家及观念史学家
  - [米哈伊·巴雷什尼科夫](../Page/米哈伊·巴雷什尼科夫.md "wikilink")（Mikhail Baryshnikov）
    著名[芭蕾](../Page/芭蕾.md "wikilink")[舞蹈家](../Page/舞蹈家.md "wikilink")
  - [謝爾蓋·愛森斯坦](../Page/謝爾蓋·愛森斯坦.md "wikilink")（Sergei Eisenstein）
    著名俄羅斯[電影](../Page/電影.md "wikilink")[導演](../Page/導演.md "wikilink")
  - [威廉·奧斯特瓦爾德](../Page/威廉·奧斯特瓦爾德.md "wikilink")（Wilhelm Ostwald）
    1909年[諾貝爾化學獎得獎者](../Page/諾貝爾化學獎.md "wikilink")
  - [瓦爾特·察普](../Page/瓦爾特·察普.md "wikilink")（Walter
    Zapp）[密諾斯](../Page/密諾斯.md "wikilink")[微型相機的發明人](../Page/微型相機.md "wikilink")
  - [米夏·麥斯基](../Page/米夏·麥斯基.md "wikilink")（Mischa Maisky）
    著名[大提琴家](../Page/大提琴家.md "wikilink")
  - [米哈伊尔·塔尔](../Page/米哈伊尔·塔尔.md "wikilink") (Mihails Tāls）
    国际象棋选手，曾获得第八届国际象棋世界冠军。
  - [雅各布·W·戴維斯](../Page/雅各布·W·戴維斯.md "wikilink")
    世界上第一條鉚釘粗布斜紋褲发明者，這就是後來[李維·斯特勞斯店裏出品的藍色牛仔褲](../Page/李維·斯特勞斯.md "wikilink")。
  - [萊拉·弗賴瓦爾茲](../Page/萊拉·弗賴瓦爾茲.md "wikilink")（Laila
    Freivalds）[瑞典社會民主黨政治家](../Page/瑞典.md "wikilink")，曾擔任瑞典司法部大臣,瑞典外交部大臣及瑞典副首相。
  - [弗里德里希·灿德尔](../Page/弗里德里希·灿德尔.md "wikilink")（Frīdrihs
    Canders）苏联航天学家，[波罗的海德国人](../Page/波罗的海德国人.md "wikilink")，[液体火箭发动机发明者](../Page/液体火箭发动机.md "wikilink")。
  - [约翰·吉奥格·哈曼](../Page/约翰·吉奥格·哈曼.md "wikilink") [Johann Georg
    Hamann](../Page/Johann_Georg_Hamann.md "wikilink")
    18世纪的德国哲学家,其学生[约翰·戈特弗里德·赫尔德](../Page/约翰·戈特弗里德·赫尔德.md "wikilink")（Johann
    Gottfried Herder）为德国浪漫主义[狂飙突进运动的基础](../Page/狂飙突进运动.md "wikilink")。
  - [尼古拉·哈特曼](../Page/尼古拉·哈特曼.md "wikilink")（Nicolai
    Hartmann），波罗的海德国哲学家。批判实在论[Critical realism
    (philosophy of
    perception的代表人物](../Page/Critical_realism_\(philosophy_of_perception.md "wikilink")，以及二十世纪最重要的[形而上学学者之一](../Page/形而上学.md "wikilink")。
  - [尤里斯·哈特马尼斯](../Page/尤里斯·哈特马尼斯.md "wikilink") （Juris
    Hartmanis）美国杰出的理论计算机科学家。
  - [保罗·瓦尔登](../Page/保罗·瓦尔登.md "wikilink") ([Paul
    Walden](../Page/Paul_Walden.md "wikilink"))
    著名化学家，[瓦尔登翻转发现者](../Page/瓦尔登翻转.md "wikilink")，著有化学简史。
  - [湯姆斯·博爾科夫斯基斯](../Page/Tobu_\(音樂製作人\).md "wikilink")(Toms
    Burkovskis)綽號[Tobu](../Page/Tobu_\(音樂製作人\).md "wikilink")，著名音樂製作人。

## 友好城市

下面列示里加擁有的友好城市\[18\]：

|                                                                                                                                                        |                                                                        |                                                                                                                                                                                    |                                                                             |
| ------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------- |
| [Flag_of_Denmark.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Denmark.svg "fig:Flag_of_Denmark.svg")                                              | [丹麥](../Page/丹麥.md "wikilink")[奧爾堡](../Page/奧爾堡.md "wikilink")         | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg")                                                                                | [義大利](../Page/義大利.md "wikilink")[佛羅倫斯](../Page/佛羅倫斯.md "wikilink")          |
| [Flag_of_Kazakhstan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kazakhstan.svg "fig:Flag_of_Kazakhstan.svg")                                     | [哈薩克斯坦](../Page/哈薩克斯坦.md "wikilink")[阿拉木圖](../Page/阿拉木圖.md "wikilink") | [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")                                                                             | [法國](../Page/法國.md "wikilink")[加萊](../Page/加萊.md "wikilink")                |
| [Flag_of_the_Netherlands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Netherlands.svg "fig:Flag_of_the_Netherlands.svg")                     | [荷蘭](../Page/荷蘭.md "wikilink")[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")     | [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg")                                                                    | [澳大利亞](../Page/澳大利亞.md "wikilink")[凯恩斯](../Page/凯恩斯_\(昆士兰州\).md "wikilink") |
| [Flag_of_Kazakhstan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kazakhstan.svg "fig:Flag_of_Kazakhstan.svg")                                     | [哈薩克斯坦](../Page/哈薩克斯坦.md "wikilink")[阿斯塔納](../Page/阿斯塔納.md "wikilink") | [Flag_of_Ukraine.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg "fig:Flag_of_Ukraine.svg")                                                                          | [烏克蘭](../Page/烏克蘭.md "wikilink")[基輔](../Page/基輔.md "wikilink")              |
| [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")                                                 | [法國](../Page/法國.md "wikilink")[波爾多](../Page/波爾多.md "wikilink")         | [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg")                                                                                | [日本](../Page/日本.md "wikilink")[神戶](../Page/神戶.md "wikilink")                |
| [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg")                                              | [德國](../Page/德國.md "wikilink")[不來梅](../Page/不來梅.md "wikilink")         | [Flag_of_Russia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Russia.svg "fig:Flag_of_Russia.svg")                                                                             | [俄羅斯](../Page/俄羅斯.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")            |
| [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg")              | [美國](../Page/美國.md "wikilink")[達拉斯](../Page/達拉斯.md "wikilink")         | [Flag_of_Belarus.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Belarus.svg "fig:Flag_of_Belarus.svg")                                                                          | [白俄羅斯](../Page/白俄羅斯.md "wikilink")[明斯克](../Page/明斯克.md "wikilink")          |
| [Flag_of_Sweden.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sweden.svg "fig:Flag_of_Sweden.svg")                                                 | [瑞典](../Page/瑞典.md "wikilink")[北雪平](../Page/北雪平.md "wikilink")         | [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg") | [中華人民共和國](../Page/中華人民共和國.md "wikilink")[北京](../Page/北京.md "wikilink")      |
| [Flag_of_Finland.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Finland.svg "fig:Flag_of_Finland.svg")                                              | [芬蘭](../Page/芬蘭.md "wikilink")[波里](../Page/波里.md "wikilink")           | [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg")                                                                          | [德國](../Page/德國.md "wikilink")[羅斯托克](../Page/羅斯托克.md "wikilink")            |
| [Flag_of_Russia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Russia.svg "fig:Flag_of_Russia.svg")                                                 | [俄羅斯](../Page/俄羅斯.md "wikilink")[聖彼德堡](../Page/聖彼德堡.md "wikilink")     | [Flag_of_Chile.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Chile.svg "fig:Flag_of_Chile.svg")                                                                                | [智利](../Page/智利.md "wikilink")[聖地牙哥](../Page/聖地牙哥.md "wikilink")            |
| [Flag_of_Sweden.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sweden.svg "fig:Flag_of_Sweden.svg")                                                 | [瑞典](../Page/瑞典.md "wikilink")[斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink")     | [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg") | [中華人民共和國](../Page/中華人民共和國.md "wikilink")[蘇州](../Page/蘇州.md "wikilink")      |
| [Flag_of_the_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Republic_of_China.svg "fig:Flag_of_the_Republic_of_China.svg") | [中華民國](../Page/中華民國.md "wikilink")[臺北](../Page/臺北.md "wikilink")       | [Flag_of_Estonia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Estonia.svg "fig:Flag_of_Estonia.svg")                                                                          | [愛沙尼亞](../Page/愛沙尼亞.md "wikilink")[塔林](../Page/塔林.md "wikilink")            |
| [Flag_of_Lithuania.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Lithuania.svg "fig:Flag_of_Lithuania.svg")                                        | [立陶宛](../Page/立陶宛.md "wikilink")[維爾紐斯](../Page/維爾紐斯.md "wikilink")     | [Flag_of_Poland.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Poland.svg "fig:Flag_of_Poland.svg")                                                                             | [波蘭](../Page/波蘭.md "wikilink")[華沙](../Page/華沙.md "wikilink")                |

## 參見

  - [里加總主教](../Page/里加總主教.md "wikilink")
  - [里加國際機場](../Page/里加國際機場.md "wikilink")

## 參考資料

## 外部連結

  - [Riga Municipality
    portal](http://www.riga.lv/EN/Channels/About_Riga/default.htm)

  - [Other riga - The home page about Riga](http://www.citariga.lv/EN/)

  - [Reliable illustrated guide to Riga City travel, hotels, business
    and entertainment](http://www.bestriga.com)

  -
  - [Map of Greater
    Riga](http://www.latviaphoto.com/rigamaps/map_greater_riga_100x2400.jpg)

  - [Riga](http://www.latvians.com/en/Reading/RigaNatlEncy/riga1897.php)—entry
    (article and engraving) from *The National Encyclopedia: A
    Dictionary of Universal Knowledge*, circa 1900

  - ["Riga/Рига" photo
    album](http://www.latvians.com/en/Reading/Riga19thC/Riga-01-19thC.php),
    circa 1910

  - ["Foreign Corn
    Ports-Riga"](http://www.latvians.com/en/Reading/CornPorts/cornports-01-homepage.php)—*The
    Illustrated London News*, news article with engraved illustrations,
    published 1847\]

  - [Riga Daily Photos and
    Webcams](https://web.archive.org/web/20090506004402/http://riga.in/)

  - [Riga City
    Council](https://web.archive.org/web/20101009035016/http://www.rigacitycouncil.lv/)

  - [Riga Municipality portal](http://www.riga.lv/)

  - [Riga City Tourism Portal](http://www.rigatourism.com/)

  - [Riga's Own
    Wiki](https://web.archive.org/web/20050706073527/http://www.riga.lt/riga/)

[\*](../Category/里加.md "wikilink") [R](../Category/歐洲首都.md "wikilink")
[R](../Category/拉脱维亚城市.md "wikilink")
[R](../Category/波羅的海沿海城市.md "wikilink")
[R](../Category/汉萨同盟.md "wikilink")
[R](../Category/拉脫維亞行政區劃.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.

16.
17.

18.