**里奧哈龍科**（Riojasauridae）是一群外表類似[蜥腳類的恐龍](../Page/蜥腳類.md "wikilink")，生存於上[三疊紀](../Page/三疊紀.md "wikilink")。對於里奧哈龍科的研究主要來自於[里奧哈龍](../Page/里奧哈龍.md "wikilink")，以及[優脛龍](../Page/優脛龍.md "wikilink")。

## 發現地點

  - 優脛龍：[南非](../Page/南非.md "wikilink")[奧蘭治自由邦](../Page/奧蘭治自由邦.md "wikilink")[下艾略特組](../Page/下艾略特組.md "wikilink")。
  - 里奧哈龍：[阿根廷](../Page/阿根廷.md "wikilink")[拉里奧哈省](../Page/拉里奧哈省_\(阿根廷\).md "wikilink")[伊斯巨拉斯托](../Page/伊斯巨拉斯托.md "wikilink")（Ischigualasto）地區。

<center>

<small>基礎[蜥腳形亞目](../Page/蜥腳形亞目.md "wikilink")[演化樹](../Page/演化樹.md "wikilink")，取自於亞當·耶茨的2007年研究</small>

</center>

{{-}}

## 參考資料

  - Yates, Adam M. (2007). "The first complete skull of the Triassic
    dinosaur *Melanorosaurus* Haughton (Sauropodomorpha: Anchisauria)",
    in Barrett, Paul M. & Batten, David J., *Special Papers in
    Palaeontology*, vol. 77, p. 9–55, ISBN 9781405169332.

## 外部链接

  - [蜥腳形亞目的演化樹與各屬簡介](https://web.archive.org/web/20090728192854/http://www.thescelosaurus.com/sauropodomorpha.htm)
    - *Thescelosaurus\!*

[\*](../Category/原蜥腳下目.md "wikilink")