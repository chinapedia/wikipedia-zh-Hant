**里斯本鱷屬**（屬名：*Lisboasaurus*）是[中生代](../Page/中生代.md "wikilink")[鱷形超目的一屬](../Page/鱷形超目.md "wikilink")，是種小型動物，身長約40公分。化石的保存狀態不良好，只有牙齒與下頜碎片。目前已經有兩個種被敘述。里斯本鱷曾被認為是中[侏儸紀的](../Page/侏儸紀.md "wikilink")[初鳥類](../Page/初鳥類.md "wikilink")、[傷齒龍科](../Page/傷齒龍科.md "wikilink")、[蛇蜥類](../Page/蛇蜥類.md "wikilink")（
Anguimorph）的[蜥蜴](../Page/蜥蜴.md "wikilink")。兩個種目前都被歸類於[鱷形超目](../Page/鱷形超目.md "wikilink")，其中一種則被歸類於[盧西塔尼鱷](../Page/盧西塔尼鱷.md "wikilink")。

在1960年代，[柏林自由大學的](../Page/柏林自由大學.md "wikilink")[古生物學家在](../Page/古生物學家.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")[萊里亞附近的Guimarota](../Page/萊里亞.md "wikilink")[褐煤礦找到新的](../Page/褐煤.md "wikilink")[脊椎動物化石地點](../Page/脊椎動物.md "wikilink")。在1991年，Milner與Evans將這個褐煤礦的年代，定年於中[侏儸紀](../Page/侏儸紀.md "wikilink")[巴統階到晚](../Page/巴統階.md "wikilink")[侏儸紀](../Page/侏儸紀.md "wikilink")[牛津階](../Page/牛津階.md "wikilink")。在2004年，Schwarz與Fechner則將該地區，定年為晚[侏儸紀](../Page/侏儸紀.md "wikilink")。該地發現了大量的化石碎片。

Seiffert將里斯本鱷敘述成[鱗龍超目](../Page/鱗龍超目.md "wikilink")[蛇蜥類的一屬](../Page/蛇蜥類.md "wikilink")，包含兩個種：*L.
estesi*、*L. mitracostatus*。他在第一份研究中將*L.
mitracostatus*分成兩個亞種，但第二份研究中則沒有。在1983年，Estes將里斯本鱷列為[蜥蜴亞目的未定位物種](../Page/蜥蜴亞目.md "wikilink")。在1991年，Milner與Evans將*L.
estesi*重新敘述成[手盜龍類](../Page/手盜龍類.md "wikilink")，特別是早期[初鳥類或](../Page/初鳥類.md "wikilink")[傷齒龍科](../Page/傷齒龍科.md "wikilink")；他們也將保存狀態更差的*L.
miracostatus*列為[疑名](../Page/疑名.md "wikilink")。Buscalioni與Evans等人修正這個分類，他們發現*L.
estesi*與一個早[白堊紀的](../Page/白堊紀.md "wikilink")[鱷形超目化石](../Page/鱷形超目.md "wikilink")（編號LH
7991）有接近親緣關係；編號LH 7991化石是在[西班牙Las](../Page/西班牙.md "wikilink")
Hoyas發現的。他們也支持將*L. miracostatus*列為疑名。

在2004年，Schwarz與Fechner證實*L. mitracostatus*的牙齒與破碎化石是在Porto
Dinheiro所發現，而且在1973年到1982年間於Guimarota發現了新的顱頂與下頜化石。他們利用所有的*L.
mitracostatus*標本建立起新屬，[盧西塔尼鱷](../Page/盧西塔尼鱷.md "wikilink")（*Lusitanisuchus
mistracostatus*）。

在2008年，Schwarz與Fechner敘述一個發現於[西班牙](../Page/西班牙.md "wikilink")[昆卡省Uña煤礦層的](../Page/昆卡省.md "wikilink")[齒骨](../Page/齒骨.md "wikilink")。從牙齒證實它屬於盧西塔尼鱷，年代屬於白堊紀早期的[巴列姆階](../Page/巴列姆階.md "wikilink")。科學家從這個齒骨得知盧西塔尼鱷屬於[新鱷類](../Page/新鱷類.md "wikilink")。

## 參考資料

  - Buscalioni, A.D., Ortega, F., Pérez-Moreno, B.P., and Evans, S.E.
    (1996). "The Upper Jurassic maniraptoran theropod *Lisboasaurus
    estesi* (Guimarota, Portugal) reinterpreted as a crocodylomorph".
    *Journal of Vertebrate Paleontology* **16**(2): 358–362.
  - Estes, R. (1983). Sauria terrestria. Amphisbaenia. In *Handbuch der
    Paläoherpetologie*. Teil 10A. Edited by P. Wellnhofer. Gustav
    Fischer Verlag, Stuttgart, pp. 1–249.
  - Milner, A.R., and Evans, S.E. (1991). "The Upper Jurassic diapsid
    *Lisboasaurus estesi* — a maniraptoran theropod". *Palaeontology*
    **34**: 503–513.
  - Schwarz, D. and Fechner, R. (2004). "*Lusitanisuchus*, a new generic
    name for *Lisboasaurus mitracostatus* (Crocodylomorpha:
    Mesoeucrocodylia), with a description of new remains from the Upper
    Jurassic (Kimmeridgian) and Lower Cretaceous (Berriasian) of
    Portugal". *Canadian Journal of Earth Sciences* **41**: 1259–1271.
  - Seiffert, J. (1970). "Oberjurassische Lacertilier aus der
    Kohlengrube Guimarota bei Leiria (Mittel Portugal)". Unpublished
    Inaugural-Dissertation, Freie Universität, Berlin, Germany.
  - Seiffert, J. (1973). "Upper Jurassic Lizards from Central Portugal".
    *Memória dos Servicos Géologicos de Portugal (N.S.)* **22**: 7–88.
  - Schwarz, D., Fechner, R. (2008). "The first dentary of
    *Lisboasaurus* (Crocodylomorpha, ?Mesoeucrocodylia) from the Lower
    Cretaceous of Uña, Cuenca Province, Spain". *Journal of vertebrate
    Paleontology* **28**(1): 264-268.

[Category:鱷形超目](../Category/鱷形超目.md "wikilink")
[Category:侏羅紀鱷形類](../Category/侏羅紀鱷形類.md "wikilink")