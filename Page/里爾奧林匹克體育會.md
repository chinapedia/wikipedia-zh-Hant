**里爾奧林匹克體育會**（，，簡稱 **Lille OSC**、 **LOSC
Lille**或更簡單的**Lille**）乃[法國一家足球會](../Page/法國.md "wikilink")，位處法國北部北部最大的城市，[北部-加莱海峡大区首府和](../Page/北部-加来海峡.md "wikilink")[诺尔省省会](../Page/诺尔省.md "wikilink")[里爾市](../Page/里爾.md "wikilink")。它是在1944年由兩家法國球會通過併購中成立，現時在[法國甲組足球聯賽作賽](../Page/法國甲組足球聯賽.md "wikilink")。

## 球會歷史

[Lille_osc.png](https://zh.wikipedia.org/wiki/File:Lille_osc.png "fig:Lille_osc.png")
里尔乃在1944年由兩家法國球會Olympique Lillois（1902年創立）和SC
Fives（1901年成立）通過併購中成立。早期其主要競爭對手乃[朗斯](../Page/朗斯足球俱乐部.md "wikilink")，兩隊曾在早年因地區文化原因而形成「世仇」關係。不過里尔一直以來都沒有任何豐功偉績幹出來，長期都只是在法國較低組別聯賽打滾，並一直都活在朗斯的陰影下。

不過自從2000年里尔升上法國甲組聯賽後，里尔在甲組打滾了三年，其成績卻已比同地區各支球會的佳。如在2004-05年球季里尔取得了法國甲組聯賽亞軍，排名不但比朗斯高，還比法甲近代班霸如[馬賽和](../Page/马赛足球俱乐部.md "wikilink")[摩納哥為高](../Page/摩纳哥足球俱乐部.md "wikilink")。翌年里尔闖進[歐冠杯](../Page/歐冠杯.md "wikilink")，在分組賽更打敗了1999年歐冠杯冠軍[曼聯](../Page/曼聯.md "wikilink")，間接令曼聯十年來首次於歐冠杯分組賽出局。

## 球會榮譽

  - '''[法國甲組足球聯賽](../Page/法國甲組足球聯賽.md "wikilink") '''

<!-- end list -->

  -
    冠軍：1933年, 1946年, 1954年,
    [2011年](../Page/2010年至2011年法國足球甲級聯賽.md "wikilink")
    亞軍：1936年, 1948年, 1949年, 1950年, 1951年, 2005年

<!-- end list -->

  - **[法國乙組足球聯賽](../Page/法乙.md "wikilink")**

<!-- end list -->

  -
    冠軍：1964年, 1974年, 1978年, 2000年

<!-- end list -->

  - **[法國盃](../Page/法國盃.md "wikilink")**

<!-- end list -->

  -
    冠軍：1946年, 1947年, 1948年, 1953年, 1955年, 2011年
    亞軍：1939年, 1945年, 1949年

<!-- end list -->

  - [拉丁盃](../Page/拉丁盃.md "wikilink")

<!-- end list -->

  -
    亞軍：1951年

<!-- end list -->

  - **[圖圖盃](../Page/歐洲足協圖圖盃.md "wikilink")**

<!-- end list -->

  -
    冠軍：2004年

## 著名球星

  - [埃里克·阿比达尔](../Page/埃里克·阿比达尔.md "wikilink") (Éric Abidal)
  - [安高馬](../Page/安高馬.md "wikilink")()
  - [Erwin Vandenbergh](../Page/Erwin_Vandenbergh.md "wikilink")
  - [Djezon Boutoille](../Page/Djezon_Boutoille.md "wikilink")
  - [布鲁诺·谢鲁](../Page/布鲁诺·谢鲁.md "wikilink") (Bruno Cheyrou)
  - [帕斯卡尔·西甘](../Page/帕斯卡尔·西甘.md "wikilink") (Pascal Cygan)
  - [Kim Vilfort](../Page/Kim_Vilfort.md "wikilink")
  - [Abedi Pelé](../Page/Abedi_Pelé.md "wikilink")
  - [西别尔斯基](../Page/安托万·西别尔斯基.md "wikilink")（Antoine Sibierski）
  - [艾登·阿扎尔](../Page/艾登·阿扎尔.md "wikilink")（Eden Hazard）

## 參考文獻

## 外部連結

  - [里爾官方網站（法文）](http://www.losc.fr/)

[分類:1902年建立的足球俱樂部](../Page/分類:1902年建立的足球俱樂部.md "wikilink")

[L](../Category/法國足球俱樂部.md "wikilink")
[Category:里尔](../Category/里尔.md "wikilink")