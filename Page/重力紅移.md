[重力紅移.png](https://zh.wikipedia.org/wiki/File:重力紅移.png "fig:重力紅移.png")

**重力紅移**（Gravitational
redshift）或稱**重力紅位移**指的是[光波或者其他波動從](../Page/光波.md "wikilink")[重力場源](../Page/重力場.md "wikilink")（如巨大星體或[黑洞](../Page/黑洞.md "wikilink")）遠離時，整體[頻譜會往紅色端方向偏移](../Page/頻譜.md "wikilink")，亦即發生「[頻率變低](../Page/頻率.md "wikilink")，[波長增長](../Page/波長.md "wikilink")」的現象。

## 定義

**重力紅移**的程度常標記為變數*z*：

\(z=\frac{\lambda_o-\lambda_e}{\lambda_e}\)

其中\(\lambda_o\,\)是極遠處觀測者所測量到的[光子波長](../Page/光子.md "wikilink")；\(\lambda_e\,\)是重力源如星球，其上的光源發出時所測量到的光子波長。

重力紅移的現象可以從[廣義相對論預測](../Page/廣義相對論.md "wikilink")：

\(z_{approx}=\frac{KM}{c^2r}\)

其中

  - \(z_{approx}\,\)是被自由空間中，極遠處觀察者所測到因重力而產生的譜線位移量。
  - \(K\,\)是牛頓[重力常數](../Page/重力常數.md "wikilink")（[愛因斯坦本身所用的標記](../Page/愛因斯坦.md "wikilink")；常用標記是\(G\,\)）。
  - \(M\,\)是光所逃離的星體[質量](../Page/質量.md "wikilink")。
  - \(c\,\)是真空中[光速](../Page/光速.md "wikilink")。
  - \(r\,\)是從星體中心算起的徑向距離。

## 幾項要點

  - 光線的接收端（遠方的觀察者）必須處在較高的[重力勢才能觀察到紅移](../Page/重力勢.md "wikilink")。一般討論下，觀察者處在[無限遠處](../Page/無限遠.md "wikilink")，重力勢定為0，是高於星球表面的重力勢的。

<!-- end list -->

  - 許多大學的實驗結果支持重力紅移的存在。

<!-- end list -->

  - 重力紅移不僅僅是[廣義相對論獨有的預測](../Page/廣義相對論.md "wikilink")。其他重力理論也支持重力紅移，雖然解釋上會有所不同。

<!-- end list -->

  - 重力紅移並未要求一定是[愛因斯坦方程式的](../Page/愛因斯坦方程式.md "wikilink")[史瓦西解](../Page/史瓦西解.md "wikilink")——在這解中，變數\(M\,\)不能代表旋轉或帶電星體的質量。

## 最早的證實

1959年展示了譜線重力紅移的存在。此由[哈佛大學](../Page/哈佛大學.md "wikilink")[萊曼物理實驗室的科學家所記載](../Page/萊曼物理實驗室.md "wikilink")。

## 應用

由於如[地球等行星質量並不算大](../Page/地球.md "wikilink")，以致於重力紅移現象不顯著，故近地通訊並沒有針對重力紅移的修正需求。

重力紅移的主要應用是在[天文學研究上](../Page/天文學.md "wikilink")，透過一些特定原子光譜的紅移，可以估計星球質量。

## 精確解

**重力紅移**的精確解（exact solution）條列如下表:

|     |                                                                                |                                                               |
| --- | ------------------------------------------------------------------------------ | ------------------------------------------------------------- |
|     | 不旋轉                                                                            | 旋轉                                                            |
| 不帶電 | [史瓦西度規](../Page/史瓦西度規.md "wikilink")                                           | [克爾度規 (Kerr metric)](../Page/克爾度規.md "wikilink")              |
| 帶電  | [萊斯納-諾德斯特洛姆度規 (Reissner-Nordström metric)](../Page/萊斯納-諾德斯特洛姆度規.md "wikilink") | [克爾-紐曼度規 (Kerr-Newman metric)](../Page/克爾-紐曼度規.md "wikilink") |

較常用到的重力紅移精確解是針對非轉動、不帶電、球對稱的質量體（即對應於[史瓦西度規](../Page/史瓦西度規.md "wikilink")）。
方程式的形式是：

\(z=\frac{1}{\sqrt{1 - \left(\frac{2GM}{rc^2}\right)}} - 1\)，

其中

  - \(G\,\)是[重力常數](../Page/重力常數.md "wikilink")，

<!-- end list -->

  - \(M\,\)是產生[重力場之物體的](../Page/重力場.md "wikilink")[質量](../Page/質量.md "wikilink")，

<!-- end list -->

  - \(r\,\)是觀測者的徑向坐標（類比於牛頓力學中從物體中心算起的距離，但事實上是[史瓦西坐標](../Page/史瓦西坐標.md "wikilink")），

<!-- end list -->

  - \(c\,\)是真空中[光速](../Page/光速.md "wikilink")。

## 重力紅移 與 重力時間展長

若利用[狹義相對論的](../Page/狹義相對論.md "wikilink")[相對論性多普勒關係](../Page/相對論性多普勒效應.md "wikilink")，來計算能量與頻率的變動（假設沒有令情況更複雜的[路徑相依效應](../Page/路徑相依效應.md "wikilink")，比如[旋轉黑洞的](../Page/旋轉黑洞.md "wikilink")[參考系拖曳效應](../Page/參考系拖曳效應.md "wikilink")），則重力紅移和[藍移頻率比值會互為倒數](../Page/藍移.md "wikilink")，提示了所見的頻率改變對應於[不同處時鐘速率不同](../Page/重力時間展長.md "wikilink")。

參考系拖曳效應造成的路經相依效應，若被考慮進來，則可能使這種分析方法失效，並且使得要建立起[廣域皆認同的各處時鐘速率差異變得困難](../Page/廣域.md "wikilink")，雖然並非不能達到。

重力紅移所指的是觀察到的，而[引力時間膨脹](../Page/引力時間膨脹.md "wikilink")，則是用以指**背後發生機制的推論**（處於重力場中的發光源，由於它的時系比較慢，故它發出來的光頻，本來就會比較低）。

## 参见

  - [零測地線](../Page/零測地線.md "wikilink")（null geodesic）
  - [廣義相對論的精確解](../Page/廣義相對論的精確解.md "wikilink")（exact solutions in
    general relativity）。

## 外部連結

  - 阿爾伯特·愛因斯坦「相對論：狹義與廣義理論。」<u>*古騰堡計畫*</u>
    [1](http://www.gutenberg.org/etext/5001)。

[Z](../Category/廣義相對論.md "wikilink") [Z](../Category/天文学.md "wikilink")
[Category:物理现象](../Category/物理现象.md "wikilink")
[Z](../Category/阿尔伯特·爱因斯坦.md "wikilink")