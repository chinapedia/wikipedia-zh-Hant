本列表是[中国全国重点文物保护单位的子列表](../Page/中国全国重点文物保护单位.md "wikilink")，列举在[重庆市境内的全国重点文物保护单位](../Page/重庆市.md "wikilink")。

<center>

<table>
<thead>
<tr class="header">
<th><p>名称</p></th>
<th><p>编号</p></th>
<th><p>分类</p></th>
<th><p>所在</p></th>
<th><p>时代</p></th>
<th><p>图片</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/八路军重庆办事处旧址.md" title="wikilink">八路军重庆办事处旧址</a><br />
含：</p>
<ul>
<li><a href="../Page/曾家岩50號.md" title="wikilink">曾家岩50號</a></li>
<li><a href="../Page/紅岩村13號.md" title="wikilink">紅岩村13號</a></li>
<li><a href="../Page/《新華日報》營業部舊址.md" title="wikilink">《新華日報》營業部舊址</a></li>
<li><a href="../Page/中共代表團駐地舊址.md" title="wikilink">中共代表團駐地舊址</a></li>
</ul></td>
<td><p>1-28</p></td>
<td><p>革命遗址及革命纪念建筑物</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1938—1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chongqing_Zengjiayan_50_hao_2014.04.21_10-48-10.jpg" title="fig:Chongqing_Zengjiayan_50_hao_2014.04.21_10-48-10.jpg">Chongqing_Zengjiayan_50_hao_2014.04.21_10-48-10.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北山摩崖造像.md" title="wikilink">北山摩崖造像</a><br />
含：</p>
<ul>
<li><a href="../Page/大足多宝塔.md" title="wikilink">大足多宝塔</a></li>
<li><a href="../Page/南山摩崖造像.md" title="wikilink">南山摩崖造像</a></li>
<li><a href="../Page/石篆山摩崖造像.md" title="wikilink">石篆山摩崖造像</a></li>
</ul></td>
<td><p>1-45</p></td>
<td><p>石窟寺</p></td>
<td><p><a href="../Page/大足县.md" title="wikilink">大足县</a></p></td>
<td><p><a href="../Page/唐.md" title="wikilink">唐</a>、<a href="../Page/宋朝.md" title="wikilink">宋</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dazu_rock_carvings_beishan_2.JPG" title="fig:Dazu_rock_carvings_beishan_2.JPG">Dazu_rock_carvings_beishan_2.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宝顶山摩崖造像.md" title="wikilink">宝顶山摩崖造像</a><br />
含：</p>
<ul>
<li><a href="../Page/石门山摩崖造像.md" title="wikilink">石门山摩崖造像</a></li>
</ul></td>
<td><p>1-46</p></td>
<td><p>石窟寺</p></td>
<td><p><a href="../Page/大足县.md" title="wikilink">大足县</a></p></td>
<td><p><a href="../Page/宋朝.md" title="wikilink">宋</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dazu.jpg" title="fig:Dazu.jpg">Dazu.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/“中美合作所”集中营旧址.md" title="wikilink">“中美合作所”集中营旧址</a></p></td>
<td><p>3-163</p></td>
<td><p>古建筑及历史纪念建筑物</p></td>
<td><p><a href="../Page/沙坪坝区.md" title="wikilink">沙坪坝区</a></p></td>
<td><p>1943—1949年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:ChongqingZhazidong.jpg" title="fig:ChongqingZhazidong.jpg">ChongqingZhazidong.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:ChongqingBaigongguan.jpg" title="fig:ChongqingBaigongguan.jpg">ChongqingBaigongguan.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白鹤梁题刻.md" title="wikilink">白鹤梁题刻</a></p></td>
<td><p>3-172</p></td>
<td><p>石刻及其他</p></td>
<td><p><a href="../Page/涪陵区.md" title="wikilink">涪陵区</a></p></td>
<td><p><a href="../Page/唐.md" title="wikilink">唐</a>—<a href="../Page/清.md" title="wikilink">清</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:白鹤梁石鱼.jpg" title="fig:白鹤梁石鱼.jpg">白鹤梁石鱼.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龙骨坡遗址.md" title="wikilink">龙骨坡遗址</a></p></td>
<td><p>4-1</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/巫山县.md" title="wikilink">巫山县</a></p></td>
<td><p><a href="../Page/更新世.md" title="wikilink">更新世</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/钓鱼城遗址.md" title="wikilink">钓鱼城遗址</a></p></td>
<td><p>4-55</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/合川区.md" title="wikilink">合川区</a></p></td>
<td><p><a href="../Page/宋朝.md" title="wikilink">宋</a>、<a href="../Page/元朝.md" title="wikilink">元</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Fishingtown.jpg" title="fig:Fishingtown.jpg">Fishingtown.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高家镇遗址.md" title="wikilink">高家镇遗址</a></p></td>
<td><p>5-102</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/丰都县.md" title="wikilink">丰都县</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:高家镇遗址照片.jpg" title="fig:高家镇遗址照片.jpg">高家镇遗址照片.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/张桓侯庙.md" title="wikilink">张桓侯庙</a></p></td>
<td><p>5-380</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/云阳县.md" title="wikilink">云阳县</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/石宝寨.md" title="wikilink">石宝寨</a></p></td>
<td><p>5-381</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/忠县.md" title="wikilink">忠县</a></p></td>
<td><p>明、清</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Shibaozhai_leaning_red_pavilion_and_temple.jpg" title="fig:Shibaozhai_leaning_red_pavilion_and_temple.jpg">Shibaozhai_leaning_red_pavilion_and_temple.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/丁房阙.md" title="wikilink">丁房阙</a>-<a href="../Page/㽏井溝無銘闕.md" title="wikilink">无铭阙</a></p></td>
<td><p>5-382</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/忠县.md" title="wikilink">忠县</a></p></td>
<td><p>东汉</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/桂园.md" title="wikilink">桂园</a></p></td>
<td><p>5-506</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>民国</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Guiyuan.JPG" title="fig:Guiyuan.JPG">Guiyuan.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/赵世炎故居.md" title="wikilink">赵世炎故居</a></p></td>
<td><p>5-507</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/酉阳县.md" title="wikilink">酉阳县</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/白帝城.md" title="wikilink">白帝城</a></p></td>
<td><p>6-692</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/奉节县.md" title="wikilink">奉节县</a></p></td>
<td><p>明至清</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bai_di_cheng.jpg" title="fig:Bai_di_cheng.jpg">Bai_di_cheng.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重庆湖广会馆.md" title="wikilink">重庆湖广会馆</a></p></td>
<td><p>6-693</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>清</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chongqing_Huguang_Huiguan_2014.04.21_13-14-42.jpg" title="fig:Chongqing_Huguang_Huiguan_2014.04.21_13-14-42.jpg">Chongqing_Huguang_Huiguan_2014.04.21_13-14-42.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潼南大佛寺摩崖造像.md" title="wikilink">潼南大佛寺摩崖造像</a>，含<small></p>
<ul>
<li><a href="../Page/千佛寺摩崖造像.md" title="wikilink">千佛寺摩崖造像</a></li>
</ul>
<p></small></p></td>
<td><p>6-851</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/潼南县.md" title="wikilink">潼南县</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/涞滩二佛寺摩崖造像.md" title="wikilink">涞滩二佛寺摩崖造像</a></p></td>
<td><p>6-852</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/合川区.md" title="wikilink">合川区</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杨氏民宅.md" title="wikilink">杨氏民宅</a></p></td>
<td><p>6-1034</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/潼南县.md" title="wikilink">潼南县</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中国西部科学院旧址.md" title="wikilink">中国西部科学院旧址</a></p></td>
<td><p>6-1035</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/北碚区.md" title="wikilink">北碚区</a></p></td>
<td><p>1930年代</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Beibei_Museum.jpg" title="fig:Beibei_Museum.jpg">Beibei_Museum.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/育才学校旧址.md" title="wikilink">育才学校旧址</a></p></td>
<td><p>6-1036</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/合川区.md" title="wikilink">合川区</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天生城遗址.md" title="wikilink">天生城遗址</a></p></td>
<td><p>7-0401</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/万州区.md" title="wikilink">万州区</a></p></td>
<td><p>南宋至清</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/老鼓楼衙署遗址.md" title="wikilink">老鼓楼衙署遗址</a></p></td>
<td><p>7-0402</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>南宋至清</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:老鼓楼衙署遗址照片.jpg" title="fig:老鼓楼衙署遗址照片.jpg">老鼓楼衙署遗址照片.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重庆冶锌遗址群.md" title="wikilink">重庆冶锌遗址群</a></p></td>
<td><p>7-0403</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/丰都县.md" title="wikilink">丰都县</a>、<a href="../Page/石柱土家族自治县.md" title="wikilink">石柱土家族自治县</a></p></td>
<td><p>明至清</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:重庆冶锌遗址群沙溪嘴遗址照片.jpg" title="fig:重庆冶锌遗址群沙溪嘴遗址照片.jpg">重庆冶锌遗址群沙溪嘴遗址照片.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/荆竹坝岩棺群.md" title="wikilink">荆竹坝岩棺群</a></p></td>
<td><p>7-0643</p></td>
<td><p>古墓葬</p></td>
<td><p><a href="../Page/巫溪县.md" title="wikilink">巫溪县</a></p></td>
<td><p>战国至汉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/汇南墓群.md" title="wikilink">汇南墓群</a></p></td>
<td><p>7-0644</p></td>
<td><p>古墓葬</p></td>
<td><p><a href="../Page/丰都县.md" title="wikilink">丰都县</a></p></td>
<td><p>汉至六朝</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:汇南墓群照片.jpg" title="fig:汇南墓群照片.jpg">汇南墓群照片.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/独柏寺正殿.md" title="wikilink">独柏寺正殿</a></p></td>
<td><p>7-1294</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/潼南县.md" title="wikilink">潼南县</a></p></td>
<td><p>元</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重庆古城墙.md" title="wikilink">重庆古城墙</a></p></td>
<td><p>7-1295</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>明</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chongqing_Dongshuimen_Chengqiang_2014.04.21_12-51-38.jpg" title="fig:Chongqing_Dongshuimen_Chengqiang_2014.04.21_12-51-38.jpg">Chongqing_Dongshuimen_Chengqiang_2014.04.21_12-51-38.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彭氏宗祠.md" title="wikilink">彭氏宗祠</a></p></td>
<td><p>7-1296</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/云阳县.md" title="wikilink">云阳县</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/双桂堂.md" title="wikilink">双桂堂</a></p></td>
<td><p>7-1297</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/梁平县.md" title="wikilink">梁平县</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/石门大佛寺摩崖造像.md" title="wikilink">石门大佛寺摩崖造像</a></p></td>
<td><p>7-1573</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/江津区.md" title="wikilink">江津区</a></p></td>
<td><p>宋至元</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瞿塘峽摩崖石刻.md" title="wikilink">瞿塘峽摩崖石刻</a></p></td>
<td><p>7-1574</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/奉节县.md" title="wikilink">奉节县</a></p></td>
<td><p>南宋至民国</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/弹子石摩崖造像.md" title="wikilink">弹子石摩崖造像</a></p></td>
<td><p>7-1575</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/南岸区.md" title="wikilink">南岸区</a></p></td>
<td><p>元至清</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:弹子石摩崖造像照片.jpg" title="fig:弹子石摩崖造像照片.jpg">弹子石摩崖造像照片.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刘伯承故居.md" title="wikilink">刘伯承故居</a></p></td>
<td><p>7-1863</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/开县.md" title="wikilink">开县</a></p></td>
<td><p>1897年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/聂荣臻故居.md" title="wikilink">聂荣臻故居</a></p></td>
<td><p>7-1864</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/江津区.md" title="wikilink">江津区</a></p></td>
<td><p>1899年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/嘉陵江三峡乡村建设旧址群.md" title="wikilink">嘉陵江三峡乡村建设旧址群</a></p>
<ul>
<li><a href="../Page/峽防局舊址.md" title="wikilink">峽防局舊址</a></li>
<li><a href="../Page/北碚紅樓.md" title="wikilink">紅樓舊址</a></li>
<li><a href="../Page/清涼亭舊址.md" title="wikilink">清涼亭舊址</a></li>
<li><a href="../Page/農莊舊址.md" title="wikilink">農莊舊址</a></li>
<li><a href="../Page/磐室舊址.md" title="wikilink">磐室舊址</a></li>
<li><a href="../Page/竹樓舊址.md" title="wikilink">竹樓舊址</a></li>
<li><a href="../Page/柏林樓.md" title="wikilink">柏林樓</a></li>
<li><a href="../Page/數帆樓舊址.md" title="wikilink">數帆樓舊址</a></li>
<li><a href="../Page/中國鄉村建設學院舊址.md" title="wikilink">中國鄉村建設學院舊址</a></li>
<li><a href="../Page/花房子.md" title="wikilink">花房子</a></li>
</ul></td>
<td><p>7-1865</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/北碚区.md" title="wikilink">北碚区</a></p></td>
<td><p>民国</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:嘉陵江三峡清凉亭照片.JPG" title="fig:嘉陵江三峡清凉亭照片.JPG">嘉陵江三峡清凉亭照片.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/特园.md" title="wikilink">特园</a></p></td>
<td><p>7-1866</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1931~1946年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/世界佛学苑汉藏教理院旧址.md" title="wikilink">世界佛学苑汉藏教理院旧址</a></p></td>
<td><p>7-1867</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/北碚区.md" title="wikilink">北碚区</a></p></td>
<td><p>1932年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:世界佛学苑汉藏教理院旧址照片.jpg" title="fig:世界佛学苑汉藏教理院旧址照片.jpg">世界佛学苑汉藏教理院旧址照片.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南腰界红三军司令部旧址.md" title="wikilink">南腰界红三军司令部旧址</a></p></td>
<td><p>7-1868</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/酉阳土家族苗族自治县.md" title="wikilink">酉阳土家族苗族自治县</a></p></td>
<td><p>1934年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/国民政府立法院、司法院及蒙藏委员会旧址.md" title="wikilink">国民政府立法院、司法院及蒙藏委员会旧址</a></p></td>
<td><p>7-1869</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1937~1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chongqing_Guomin_Zhengfu_Lifayuan_Sifayuan_Mengzang_Weiyuanhui_Jiuzhi_2014.04.21_16-23-40.jpg" title="fig:Chongqing_Guomin_Zhengfu_Lifayuan_Sifayuan_Mengzang_Weiyuanhui_Jiuzhi_2014.04.21_16-23-40.jpg">Chongqing_Guomin_Zhengfu_Lifayuan_Sifayuan_Mengzang_Weiyuanhui_Jiuzhi_2014.04.21_16-23-40.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/国民政府军事委员会政治部旧址.md" title="wikilink">国民政府军事委员会政治部旧址</a></p></td>
<td><p>7-1870</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/沙坪坝区.md" title="wikilink">沙坪坝区</a></p></td>
<td><p>1938~1945年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:国民政府军事委员会政治部旧址照片.JPG" title="fig:国民政府军事委员会政治部旧址照片.JPG">国民政府军事委员会政治部旧址照片.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重慶国民政府外交部旧址.md" title="wikilink">重慶国民政府外交部旧址</a></p></td>
<td><p>7-1871</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1938~1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:重庆国民政府外交部旧址照片.jpg" title="fig:重庆国民政府外交部旧址照片.jpg">重庆国民政府外交部旧址照片.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/重庆抗战金融机构旧址群.md" title="wikilink">重庆抗战金融机构旧址群</a></p>
<ul>
<li><a href="../Page/中央銀行舊址.md" title="wikilink">中央銀行舊址</a></li>
<li><a href="../Page/中國銀行舊址.md" title="wikilink">中國銀行舊址</a></li>
<li><a href="../Page/美豐銀行舊址.md" title="wikilink">美豐銀行舊址</a></li>
<li><a href="../Page/交通银行旧址_(重庆).md" title="wikilink">交通銀行舊址</a></li>
<li><a href="../Page/川康平民商業銀行舊址.md" title="wikilink">川康平民商業銀行舊址</a></li>
<li><a href="../Page/聚興誠銀行舊址.md" title="wikilink">聚興誠銀行舊址</a></li>
</ul></td>
<td><p>7-1872</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1938~1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chongqing_Jiaotong_Yinhang_Jiuzhi_2014.04.21_12-18-03.jpg" title="fig:Chongqing_Jiaotong_Yinhang_Jiuzhi_2014.04.21_12-18-03.jpg">Chongqing_Jiaotong_Yinhang_Jiuzhi_2014.04.21_12-18-03.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/国民参政会旧址.md" title="wikilink">国民参政会旧址</a></p></td>
<td><p>7-1873</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1938~1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:国民参政会旧址照片.jpg" title="fig:国民参政会旧址照片.jpg">国民参政会旧址照片.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/重慶林園.md" title="wikilink">林園</a></p></td>
<td><p>7-1874</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/沙坪坝区.md" title="wikilink">沙坪坝区</a></p></td>
<td><p>1938~1946年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/国民政府军事委员会政治部第三厅暨文化工作委员会旧址.md" title="wikilink">国民政府军事委员会政治部第三厅暨文化工作委员会旧址</a></p></td>
<td><p>7-1875</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a>、<a href="../Page/沙坪坝区.md" title="wikilink">沙坪坝区</a></p></td>
<td><p>1938~1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:国民政府军事委员会政治部第三厅暨文化工作委员会旧址照片.JPG" title="fig:国民政府军事委员会政治部第三厅暨文化工作委员会旧址照片.JPG">国民政府军事委员会政治部第三厅暨文化工作委员会旧址照片.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/重庆黄山抗战旧址群.md" title="wikilink">重庆黄山抗战旧址群</a></p></td>
<td><p>7-1876</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/南岸区.md" title="wikilink">南岸区</a></p></td>
<td><p>1938~1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:重庆黄山抗战旧址群云岫楼照片.jpg" title="fig:重庆黄山抗战旧址群云岫楼照片.jpg">重庆黄山抗战旧址群云岫楼照片.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/同盟国驻渝外交机构旧址群.md" title="wikilink">同盟国驻渝外交机构旧址群</a></p>
<ul>
<li><a href="../Page/蘇聯大使館舊址.md" title="wikilink">蘇聯大使館舊址</a></li>
<li><a href="../Page/蘇聯大使館武官處舊址.md" title="wikilink">蘇聯大使館武官處舊址</a></li>
<li><a href="../Page/美國大使館舊址.md" title="wikilink">美國大使館舊址</a></li>
<li><a href="../Page/英國大使館舊址.md" title="wikilink">英國大使館舊址</a></li>
<li><a href="../Page/中英聯絡處舊址.md" title="wikilink">中英聯絡處舊址</a></li>
<li><a href="../Page/法國領事館舊址.md" title="wikilink">法國領事館舊址</a></li>
<li><a href="../Page/澳大利亞公使館舊址.md" title="wikilink">澳大利亞公使館舊址</a></li>
<li><a href="../Page/土耳其公使館舊址.md" title="wikilink">土耳其公使館舊址</a></li>
<li><a href="../Page/南山蘇聯大使館舊址.md" title="wikilink">南山蘇聯大使館舊址</a></li>
<li><a href="../Page/法國大使館舊址.md" title="wikilink">法國大使館舊址</a></li>
<li><a href="../Page/美國大使館海軍武官處舊址.md" title="wikilink">美國大使館海軍武官處舊址</a></li>
<li><a href="../Page/美軍招待所.md" title="wikilink">美軍招待所</a></li>
<li><a href="../Page/法國水師兵營舊址.md" title="wikilink">法國水師兵營舊址</a></li>
<li><a href="../Page/印度專員公署舊址.md" title="wikilink">印度專員公署舊址</a></li>
</ul></td>
<td><p>7-1877</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/南岸区.md" title="wikilink">南岸区</a>、<a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1938~1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:南山苏联大使馆旧址照片.JPG" title="fig:南山苏联大使馆旧址照片.JPG">南山苏联大使馆旧址照片.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南泉抗战旧址群.md" title="wikilink">南泉抗战旧址群</a></p>
<ul>
<li><a href="../Page/林森別墅舊址.md" title="wikilink">林森別墅舊址</a></li>
<li><a href="../Page/孔祥熙官邸舊址.md" title="wikilink">孔祥熙官邸舊址</a></li>
<li><a href="../Page/校長官邸舊址.md" title="wikilink">校長官邸舊址</a></li>
<li><a href="../Page/陳立夫、陳果夫官邸舊址.md" title="wikilink">陳立夫、陳果夫官邸舊址</a></li>
<li><a href="../Page/中央政治學校研究部舊址.md" title="wikilink">中央政治學校研究部舊址</a></li>
</ul></td>
<td><p>7-1878</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/巴南区.md" title="wikilink">巴南区</a></p></td>
<td><p>1938~1946年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:南泉抗战旧址群孔祥熙官邸旧址照片.jpg" title="fig:南泉抗战旧址群孔祥熙官邸旧址照片.jpg">南泉抗战旧址群孔祥熙官邸旧址照片.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重慶国民政府行政院旧址.md" title="wikilink">重慶国民政府行政院旧址</a></p></td>
<td><p>7-1879</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1938~1947年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/重庆抗战兵器工业旧址群.md" title="wikilink">重庆抗战兵器工业旧址群</a></p>
<ul>
<li><a href="../Page/兵工署第五十兵工廠抗戰生產洞.md" title="wikilink">兵工署第五十兵工廠抗戰生產洞</a></li>
<li><a href="../Page/兵工署第十兵工廠舊址.md" title="wikilink">兵工署第十兵工廠舊址</a></li>
<li><a href="../Page/兵工署第一兵工廠抗戰生產洞.md" title="wikilink">兵工署第一兵工廠抗戰生產洞</a></li>
<li><a href="../Page/鋼鐵廠遷建委員會生產車間舊址.md" title="wikilink">鋼鐵廠遷建委員會生產車間舊址</a></li>
<li><a href="../Page/航空委員會第二飛機製造廠海孔洞生產車間.md" title="wikilink">航空委員會第二飛機製造廠海孔洞生產車間</a></li>
<li><a href="../Page/兵工署第二十四兵工廠舊址.md" title="wikilink">兵工署第二十四兵工廠舊址</a></li>
<li><a href="../Page/兵工署第二十五兵工廠舊址.md" title="wikilink">兵工署第二十五兵工廠舊址</a></li>
</ul></td>
<td><p>7-1880</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/江北区_(重庆市).md" title="wikilink">江北区</a>、<a href="../Page/沙坪坝区.md" title="wikilink">沙坪坝区</a>、<a href="../Page/九龙坡区.md" title="wikilink">九龙坡区</a>、<a href="../Page/大渡口区.md" title="wikilink">大渡口区</a>、<a href="../Page/綦江区.md" title="wikilink">綦江区</a></p></td>
<td><p>1939~1945年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/同盟国中国战区统帅部参谋长官邸旧址.md" title="wikilink">同盟国中国战区统帅部参谋长官邸旧址</a></p></td>
<td><p>7-1881</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1942~1944年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:同盟国中国战区统帅部参谋长官邸旧址照片.jpg" title="fig:同盟国中国战区统帅部参谋长官邸旧址照片.jpg">同盟国中国战区统帅部参谋长官邸旧址照片.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/保卫中国同盟总部旧址.md" title="wikilink">保卫中国同盟总部旧址</a></p></td>
<td><p>7-1882</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1942~1945年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chongqing_Baowei_Zhongguo_Tongmeng_Zongbu_Jiuzhi_2014.04.21_09-59-07.jpg" title="fig:Chongqing_Baowei_Zhongguo_Tongmeng_Zongbu_Jiuzhi_2014.04.21_09-59-07.jpg">Chongqing_Baowei_Zhongguo_Tongmeng_Zongbu_Jiuzhi_2014.04.21_09-59-07.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重庆谈判旧址群.md" title="wikilink">重庆谈判旧址群</a></p></td>
<td><p>7-1883</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1945年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:重庆谈判旧址群吴铁城官邸照片.jpg" title="fig:重庆谈判旧址群吴铁城官邸照片.jpg">重庆谈判旧址群吴铁城官邸照片.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/抗战胜利纪功碑暨人民解放纪念碑.md" title="wikilink">抗战胜利纪功碑暨人民解放纪念碑</a></p></td>
<td><p>7-1884</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1947年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Jiefangbei2010.jpg" title="fig:Jiefangbei2010.jpg">Jiefangbei2010.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重庆市人民大礼堂.md" title="wikilink">重庆市人民大礼堂</a></p></td>
<td><p>7-1885</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/渝中区.md" title="wikilink">渝中区</a></p></td>
<td><p>1954年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chongqingdlt.jpg" title="fig:Chongqingdlt.jpg">Chongqingdlt.jpg</a></p></td>
</tr>
</tbody>
</table>

</center>

## 相关条目

  - [重庆市文物保护单位](../Page/重庆市文物保护单位.md "wikilink")

{{-}}

[渝](../Category/中国全国重点文物保护单位列表.md "wikilink")
[\*](../Category/重庆全国重点文物保护单位.md "wikilink")
[Category:重慶列表](../Category/重慶列表.md "wikilink")
[Category:重庆旅游景点](../Category/重庆旅游景点.md "wikilink")