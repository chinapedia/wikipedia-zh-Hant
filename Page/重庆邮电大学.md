**重庆邮电大学**，位于[重庆市](../Page/重庆市.md "wikilink")[南岸区黄桷垭镇](../Page/南岸区.md "wikilink")，南山风景区内，是一所以信息科学技术为特色和优势，以[工科](../Page/工程学.md "wikilink")、[理科](../Page/理科.md "wikilink")、[商科为主](../Page/商科.md "wikilink")，兼顾[人文学科](../Page/人文.md "wikilink")、[社会学科](../Page/社会.md "wikilink")、[生物等学科门类的教学科研型高等学校](../Page/生物.md "wikilink")，为[重庆市与](../Page/重庆市.md "wikilink")[工业和信息化部共建高校](../Page/工业和信息化部.md "wikilink")。入选“[中西部高校基础能力建设工程](../Page/中西部高校基础能力建设工程.md "wikilink")”。

## 历史

重庆邮电大学成立于1950年3月,曾在1970年4月～1979年7月期间，被改为重庆电信总局529厂[邮电部第九研究所](../Page/中华人民共和国邮电部.md "wikilink")，后重新恢复重庆邮电学院的名称并对外招生。

### 前身

  - 1950年3月：东川邮政管理局邮政人员培训班
  - 1951年1月：西南邮电分校
  - 1953年3月：重庆邮电学校
  - 1955年5月：重庆电信学校
  - 1959年3月：重庆邮电学院
  - 1970年4月：电信总局529厂
  - 1973年7月：邮电部第九研究所
  - 1979年5月：重庆邮电学院
  - 2006年3月：重庆邮电大学

## 教育发展

### 教育部第四轮学科评估

| 一级学科代码代码 | 一级学科名称   | 评估结果 | 参评高校数量 |
| -------- | -------- | ---- | ------ |
| 0305     | 马克思主义理论  | C+   | 231    |
| 0809     | 电子科学与技术  | C+   | 106    |
| 0810     | 信息与通信工程  | B+   | 137    |
| 0811     | 控制科学与工程  | B-   | 162    |
| 0812     | 计算机科学与技术 | B+   | 238    |
| 0835     | 软件工程     | B-   | 165    |
| 1201     | 管理科学与工程  | C    | 187    |

## 组织机构

### 历任校长

1.  [郭长波](../Page/郭长波.md "wikilink") (1959年1月－1970年6月）
2.  [刘宜伦](../Page/刘宜伦.md "wikilink") (1982年2月－1983年5月）
3.  [赵祖锡](../Page/赵祖锡.md "wikilink") (1983年10月－1985年12月）
4.  [胡文立](../Page/胡文立.md "wikilink") (1986年12月－1990年5月）
5.  [聂能](../Page/聂能.md "wikilink") (2006年3月－2006年8月）
6.  [陈流汀](../Page/陈流汀.md "wikilink") (2006年8月－2011年7月）
7.  [李银国](../Page/李银国.md "wikilink") (2011年7月－2016年4月)
8.  [李林](../Page/李林.md "wikilink") (2016年4月至今)

### 二级学院

  - [通信与信息工程学院](http://scie.cqupt.edu.cn)
  - [经济管理学院](http://www.cqupt.edu.cn/jjglxy/)
  - [计算机科学技术学院](http://cs.cqupt.edu.cn)
  - [理学院](https://web.archive.org/web/20120411002625/http://slxy.cqupt.edu.cn/)
  - [网络空间安全与信息安全法学院](https://web.archive.org/web/20150221083805/http://law.cqupt.edu.cn/)
  - [生物信息学院](http://www.cqupt.edu.cn/swxy/)
  - [光电工程学院](https://web.archive.org/web/20120411010708/http://deptweb.cqupt.edu.cn/gdgcxy/)
  - [自动化学院](http://ac.cqupt.edu.cn)
  - [传媒艺术学院](http://cmys.cqupt.edu.cn/)
  - [移通学院](http://www.cqyti.com/)
  - [体育学院](https://web.archive.org/web/20120514201254/http://deptweb.cqupt.edu.cn/tyxy/)
  - [软件工程学院](http://software.cqupt.edu.cn/)
  - [成人教育学院](http://cjy.cqupt.edu.cn/)
  - [外国语学院](https://web.archive.org/web/20120406025355/http://cfl.cqupt.edu.cn/cfl/)

### 学生组织

  - [校学生会](http://hongyan.cqupt.edu.cn/xsh/)
  - 校学生社团联合会
  - 校学生科技联合会
  - [红岩网校工作站](http://hongyan.cqupt.edu.cn/)
  - [校学生宿舍自我管理与服务委员会](http://weibo.com/u/2716773121)

## 外部链接

  - [重庆邮电大学官方网站](http://www.cqupt.edu.cn)
  - [幽幽黄桷兰](https://web.archive.org/web/20110707024141/http://bbs.cqupt.edu.cn/)

[Category:1950年創建的教育機構](../Category/1950年創建的教育機構.md "wikilink")
[Category:邮电院校](../Category/邮电院校.md "wikilink")
[Category:南岸区](../Category/南岸区.md "wikilink")
[Category:重庆邮电大学](../Category/重庆邮电大学.md "wikilink")