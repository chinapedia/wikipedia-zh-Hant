[Molecular_Tweezers_JACS_6637_2004.jpg](https://zh.wikipedia.org/wiki/File:Molecular_Tweezers_JACS_6637_2004.jpg "fig:Molecular_Tweezers_JACS_6637_2004.jpg")（molecular
tweezers，紅色）中，兩者經由芳香性重疊作用結合在一起\[1\]。\]\]

**重疊**（英語：**Stacking**，又譯**堆積**）在[超分子化學中是指](../Page/超分子化學.md "wikilink")[芳香性分子的一類排列堆積形式](../Page/芳香性.md "wikilink")。例如DNA中連續性鹼基的堆積系統，或是某些具有兩個非極性環的酵素，會以π[軌域重疊的方式而堆積在一起](../Page/軌域.md "wikilink")。

## 生物學上的重疊

[DNA分子中的](../Page/DNA.md "wikilink")**π重疊**出現在相連的[核苷酸之間](../Page/核苷酸.md "wikilink")，可加強分子結構的穩定性。核苷酸中的含氮[鹼基皆擁有芳香環](../Page/鹼基.md "wikilink")。在DNA分子中，這些芳香環是以面對面的平行方式排列，使鹼基之間得以發生芳香交互作用（aromatic
interactions）。在此交互作用中，雙鍵中的π鍵會與另一個π鍵重疊。

## 參見

  - [嵌入 (化學)](../Page/嵌入_\(化學\).md "wikilink")

## 外部連結

  - [The physical basis of nucleic acid base stacking in
    water.](http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=1301220)

## 參考資料

[Category:超分子化學](../Category/超分子化學.md "wikilink")
[Category:芳香化合物](../Category/芳香化合物.md "wikilink")

1.