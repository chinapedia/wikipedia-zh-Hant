**野上
彰**（），日本[職業摔角手與](../Page/職業摔角手.md "wikilink")[演員](../Page/演員.md "wikilink")，目前效力於[新日本職業摔角](../Page/新日本職業摔角.md "wikilink")（NJPW）。妻子為女演員[佐伯玲子](../Page/佐伯玲子.md "wikilink")

## 職業生涯

1984年進入[新日本](../Page/新日本.md "wikilink")，隔年和[橋本真也](../Page/橋本真也.md "wikilink")，[武藤敬司](../Page/武藤敬司.md "wikilink")，[蝶野正洋一同出道](../Page/蝶野正洋.md "wikilink")。1989年和橋本搭檔挑戰[IWGP雙打對抗](../Page/IWGP.md "wikilink")[長州力與](../Page/長州力.md "wikilink")[飯塚孝之](../Page/飯塚孝之.md "wikilink")(飯塚高史)的搭檔組合，但不幸落敗。之後到歐洲遠征，
在[WCW和](../Page/WCW.md "wikilink")[獸神打了一場單打賽落敗](../Page/獸神.md "wikilink")，不過凱旋歸國後，挑戰當時的IWGP次重量級王者獸神成功。這也是他目前生涯唯一一次獲得此頭銜，但於同年輸掉此頭銜。

之後AKIRA轉換跑道開始參與重量級的抗爭，並且和[馳浩搭檔參加WCW主辦的](../Page/馳浩.md "wikilink")[NWA世界雙打巡迴賽](../Page/NWA.md "wikilink")，在第一回戰中打敗了Headhunters。1993年和飯塚孝之搭檔組成了
J.J.JACKS(另一成員為大武士)，此團體於1995年解散。之後加入了由[越中詩郎率領的](../Page/越中詩郎率領.md "wikilink")[平成維震軍](../Page/平成維震軍.md "wikilink")。1998年左眼被橋本真也踢傷後，暫停了摔角事業以進行休養，也同時開始了演藝生涯。
1999年復歸和蝶野正洋，[nWo Sting](../Page/nWo_Sting.md "wikilink")(Super
J)[敦福萊](../Page/敦福萊.md "wikilink")，[麥克羅頓特同組](../Page/麥克羅頓特.md "wikilink")[TEAM2000](../Page/TEAM2000.md "wikilink")，和武藤敬司率領的[nWo
Japan展開整整一年的抗爭](../Page/nWo_Japan.md "wikilink")。2000年轉回打次重量級， 挑戰當時的IWGP
JR王者[田中稔失敗](../Page/田中稔.md "wikilink")，2002年和[金本浩二搭檔](../Page/金本浩二.md "wikilink")，挑戰當時的IWGP
JR 雙打冠軍組合：獸神&田中稔。同年年底新日本次重量級重新編制，挑戰當時持有IWGP JR
腰帶的金本浩二敗退，2003年和HEAT(田中稔)搭檔，隔年退出新日本，開始參戰[全日本](../Page/全日本.md "wikilink")。04年挑戰KAZU
HAYASHI的世界次重量級敗退，2006年挑戰[近藤修司的世界次重量級](../Page/近藤修司.md "wikilink")
依舊敗下仗來，但其優異的節奏感及平衡感獲得了高度的評價，也開始參戰[無我](../Page/無我.md "wikilink")，[Hustle](../Page/Hustle.md "wikilink")。

野上彰有著很成功的演員事業，但依舊展現其速度平衡感技巧在摔角擂台上。
其目前是由長州力率領的"LEGEND"的成員，並且和獸神雷霆萊卡共同擁有IWGP次重量級雙打冠軍腰帶。

## 招式

橋型飛龍原爆

龍捲腳

腳部四字固定

Old Boy(STF&永田鎖三)

太空飛鼠身體撲擊

## 冠軍頭銜與成就

IWGP次重量級(一次)

IWGP次重量級雙打(一次)

[Category:日本職業摔角選手](../Category/日本職業摔角選手.md "wikilink")