**野台開唱**（**Formoz
Festival**）是[台灣頗具歷史與國際知名度的](../Page/台灣.md "wikilink")[音樂活動](../Page/音樂節.md "wikilink")，從1995年開始舉行以來，已成為每年台灣規模最大的音樂活動之一。

## 沿革

野台開唱為[台灣創作音樂人](../Page/台灣.md "wikilink")、樂團、樂迷們大集合的三天兩夜年度盛會，以及廿卅組國外藝人、千名[日本](../Page/日本.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[新加坡等台灣鄰近國家樂迷](../Page/新加坡.md "wikilink")、數十個國際音樂廠牌一起參與。近幾年以來，野台開唱更成為[台灣累積邀請最多國際知名藝人](../Page/台灣.md "wikilink")、吸引最多國際樂迷、媒體來台的音樂活動。

野台開唱，一切從一個舞台聚集台灣僅有的十個創作樂團的1995年開始，十幾年來野台開唱與台灣的[獨立音樂](../Page/獨立音樂.md "wikilink")、創作樂團們生息與共，一起推廣獨立思考、多元創作，紮根基層成長至今，成為十大主題區逾百個國內外樂團、藝人演出的巨大規模，野台開唱不只是一個表演的機會，更是樂團、樂迷、工作團隊們一起實現的音樂夢想。

由於諸多原因，野台開唱的主辦單位在2008年活動舉行前宣布，野台開唱於隔年（2009年）停辦\[1\]，2013年復辦。\[2\]

## 活動簡介

野台開唱的會場座落在[台北市立兒童育樂中心](../Page/台北市立兒童育樂中心.md "wikilink") (TCRC Center,
Taipei)，自[圓山山腳到半山腰廣大的腹地](../Page/圓山.md "wikilink")，區域主題分明，以《[孫子兵法](../Page/孫子兵法.md "wikilink")》的用兵四大元素「風林火山」與釋普濟《[五燈會元](../Page/五燈會元.md "wikilink")》的「電光石火」為各區主題，形塑東方氣氛。

「風舞台」位於[東方風味的傳統建築圍繞的廣場之中](../Page/東方.md "wikilink")，齊聚國內外知名的樂團及藝人演出；「後林之丘」則隱藏於山坡上的樹林密徑，知名藝人在這展現不為人知的口技與說唱藝術；「火舞台」除了是[台灣新生代樂團報名演出的試煉場以外](../Page/台灣.md "wikilink")，也將在重要時段推出特色重量級樂團；位於半山腰鳥瞰[台北市夜景的](../Page/台北市.md "wikilink")「山大王」舞台區則將提供沁涼的音樂讓觀眾在和風徐徐的夏夜中徜徉。

環繞在小山坡樹叢與池塘之間的「電世界」，以[電子音樂與獨立流行為主題](../Page/電子音樂.md "wikilink")；異地至城堡區的「光影城」將第四年推出新的主題影展；囊括[重金屬](../Page/重金屬音樂.md "wikilink")、[龐克](../Page/龐克.md "wikilink")、[硬核音樂等熱汗淋漓音樂風格的](../Page/硬核.md "wikilink")「石敢當」舞台區，2003年首次推出就成為活動爆點；而連年佔據話題版面的「火炎擂台」摔角運動區，今年當然繼續狂摔猛撞。

除了「風林火山」、「電光石火」八大主題區外，今年美食餐飲攤位將邁向集中精緻化，以俗語「呷飯皇帝大」定名為「皇帝大」特區；而多年來在野台開唱擺設攤位的各類民間公共團體，2006年亦將規劃聚集為「公共議題村」，給予青年一個投身社會公共領域的視野。

## 曾經參與之藝人及團體

### 外國

[The
XX](../Page/The_xx.md "wikilink")、[Suede](../Page/Suede.md "wikilink")、、[Orange
Range](../Page/Orange_Range.md "wikilink")、
[卡莉怪妞](../Page/卡莉怪妞.md "wikilink")、[天空爆炸](../Page/天空爆炸.md "wikilink")、、[Moby](../Page/Moby.md "wikilink")、[Lisa
Loeb](../Page/Lisa_Loeb.md "wikilink")、[Michelle
Shocked](../Page/Michelle_Shocked.md "wikilink")、[麥加帝斯](../Page/麥加帝斯.md "wikilink")、[Yo
La
Tengo](../Page/Yo_La_Tengo.md "wikilink")、[Biohazard](../Page/Biohazard.md "wikilink")、[Yellow
Machinegun](../Page/Yellow_Machinegun.md "wikilink")、[Garlic
Boys](../Page/Garlic_Boys.md "wikilink")、[LMF](../Page/LMF.md "wikilink")、[氣志團](../Page/氣志團.md "wikilink")、[Sex
Machineguns](../Page/Sex_Machineguns.md "wikilink")、[S.O.B.U.T.](../Page/S.O.B.U.T..md "wikilink")、[Kemuri](../Page/Kemuri.md "wikilink")、[Elekibass](../Page/Elekibass.md "wikilink")、[Album
Leaf](../Page/Album_Leaf.md "wikilink")、[American Analog
Set](../Page/American_Analog_Set.md "wikilink")、[Club
8](../Page/Club_8.md "wikilink")、[BRAHMAN](../Page/BRAHMAN.md "wikilink")、[Sugar
Plant](../Page/Sugar_Plant.md "wikilink")、、[Ra:IN](../Page/Ra:IN.md "wikilink")、[King
Lychee](../Page/King_Lychee.md "wikilink")、[Soft
Ball](../Page/Soft_Ball.md "wikilink")、[The Back
Horn](../Page/The_Back_Horn.md "wikilink")、[The Konki
duet](../Page/The_Konki_duet.md "wikilink")、[Caribu](../Page/Caribu.md "wikilink")、[at17](../Page/at17.md "wikilink")、[PixelToy](../Page/PixelToy.md "wikilink")、[The
Vickers](../Page/The_Vickers.md "wikilink")、[Last
Target](../Page/Last_Target.md "wikilink")、[Pink
Loop](../Page/Pink_Loop.md "wikilink")、[Mr.Funkey](../Page/Mr.Funkey.md "wikilink")、[Zombie
Ritual](../Page/Zombie_Ritual.md "wikilink")、[Magane](../Page/Magane.md "wikilink")、[Impiety](../Page/Impiety.md "wikilink")、[MP
Family](../Page/MP_Family.md "wikilink")、[Jack Or
Jive](../Page/Jack_Or_Jive.md "wikilink")、[Opposition
Party](../Page/Opposition_Party.md "wikilink")、[Grim
Force](../Page/Grim_Force.md "wikilink"), , [80
kids](../Page/80_kids.md "wikilink"),
[10-FEET](../Page/10-FEET.md "wikilink"),
[Caribou](../Page/Caribou.md "wikilink"),
[miaou](../Page/miaou.md "wikilink"), , [Her Space
Holiday](../Page/Her_Space_Holiday.md "wikilink"),
[LM.C](../Page/LM.C.md "wikilink"), [naan](../Page/naan.md "wikilink"),
[Plastic Tree](../Page/Plastic_Tree.md "wikilink"),
[te'](../Page/Té.md "wikilink"), [OK GO](../Page/OK_Go.md "wikilink"),
[土屋安娜](../Page/土屋安娜.md "wikilink"), [Buffalo
Daughter](../Page/Buffalo_Daughter.md "wikilink"),
[INORAN](../Page/INORAN.md "wikilink"),
[Akiakane](../Page/Akiakane.md "wikilink"),
[Quruli](../Page/Quruli.md "wikilink"),
[Jealkb](../Page/Jealkb.md "wikilink"), [Asobi
Seksu](../Page/Asobi_Seksu.md "wikilink"), [Teriyaki
Boys](../Page/Teriyaki_Boys.md "wikilink"),
[RIZE](../Page/RIZE.md "wikilink"),
[Back-On](../Page/Back-On.md "wikilink"),
[Testament](../Page/Testament.md "wikilink"), [RAM
RIDER](../Page/RAM_RIDER.md "wikilink"), [Sylvain
Chauveau](../Page/Sylvain_Chauveau.md "wikilink"), [Yndi
Halda](../Page/Yndi_Halda.md "wikilink"), [Spangle call Lilli
line](../Page/Spangle_call_Lilli_line.md "wikilink"), [Yum\! Yum\!
Orange](../Page/Yum!_Yum!_Orange.md "wikilink"), [DJ
Swamp](../Page/DJ_Swamp.md "wikilink"), [Steve
Aoki](../Page/Steve_Aoki.md "wikilink"), [Dirty Pretty
Things](../Page/Dirty_Pretty_Things.md "wikilink"), [Super Furry
Animals](../Page/Super_Furry_Animals.md "wikilink"),
[envy](../Page/envy.md "wikilink"),…等逾百組。

### 臺灣

[宇宙人](../Page/宇宙人_\(樂團\).md "wikilink")、[何欣穗](../Page/何欣穗.md "wikilink")、[林宥嘉](../Page/林宥嘉.md "wikilink")、[陳昇](../Page/陳昇.md "wikilink")、[陳珊妮](../Page/陳珊妮.md "wikilink")、[糯米團](../Page/糯米團.md "wikilink")、[陳綺貞](../Page/陳綺貞.md "wikilink")、[五月天](../Page/五月天.md "wikilink")、[四分衛樂團](../Page/四分衛樂團.md "wikilink")、[閃靈](../Page/閃靈樂團.md "wikilink")、[恕](../Page/恕.md "wikilink")、[董事長](../Page/董事長樂團.md "wikilink")、[濁水溪公社](../Page/濁水溪公社.md "wikilink")、[林曉培](../Page/林曉培.md "wikilink")、[骨肉皮](../Page/骨肉皮.md "wikilink")、[潑猴](../Page/潑猴_\(樂團\).md "wikilink")、[OverDose](../Page/OverDose.md "wikilink")、[旺福](../Page/旺福.md "wikilink")、[Tizzy
Bac](../Page/Tizzy_Bac.md "wikilink")、[文夏](../Page/文夏.md "wikilink")、[豬頭皮](../Page/豬頭皮.md "wikilink")、[1976](../Page/1976.md "wikilink")、[熊寶貝樂團](../Page/熊寶貝樂團.md "wikilink")、[Nipples](../Page/Nipples.md "wikilink")、[脫拉庫](../Page/脫拉庫.md "wikilink")、[好客樂隊](../Page/好客樂隊.md "wikilink")、[林生祥](../Page/林生祥.md "wikilink")、[夾子電動大樂隊](../Page/夾子電動大樂隊.md "wikilink")、[張懸](../Page/張懸.md "wikilink")、[幻日](../Page/幻日樂團.md "wikilink")、[光景消逝](../Page/光景消逝.md "wikilink")、[表兒](../Page/表兒.md "wikilink")、[Triple
Six 666](../Page/Triple_Six_666.md "wikilink")、[INFERNAL
CHAOS](../Page/INFERNAL_CHAOS.md "wikilink")、[Beyond
Cure](../Page/Beyond_Cure.md "wikilink")、[KoOk](../Page/KoOk.md "wikilink")、[滅火器樂團](../Page/滅火器樂團.md "wikilink")、[馬猴樂隊](../Page/馬猴樂隊.md "wikilink")、[害羞踢蘋果](../Page/害羞踢蘋果.md "wikilink")、[橘娃娃](../Page/橘娃娃.md "wikilink")、[甜梅號](../Page/甜梅號.md "wikilink")、[強辯樂團](../Page/強辯樂團.md "wikilink")....等數百組。

## 參考資料

## 外部連結

  - [野台開唱網站](http://formoz.com/)
  - [野台開唱官方部落格](http://mypaper.pchome.com.tw/news/formoz/)

[Category:搖滾音樂節](../Category/搖滾音樂節.md "wikilink")
[Category:台北市文化](../Category/台北市文化.md "wikilink")
[Category:台北市旅遊景點](../Category/台北市旅遊景點.md "wikilink")
[Category:台灣音樂節](../Category/台灣音樂節.md "wikilink")
[Category:1995年台灣建立](../Category/1995年台灣建立.md "wikilink")

1.  [野台開唱網站 \> 最新消息 \> 野台開唱明年停辦公告](http://formoz.com/news.php?id=90)
2.  [野台開唱睽違四年再搞怪](http://www.awakeningtw.com/awakening/news_center/show.php?itemid=42862)