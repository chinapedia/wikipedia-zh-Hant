**野犬式全方位防護運輸車**（[德文](../Page/德文.md "wikilink")：**A**llschutz-**T**ransport-**F**ahrzeug
Dingo，[縮寫](../Page/縮寫.md "wikilink")：**ATF
Dingo**），是[德国联邦国防军现役的一款军用](../Page/德国联邦国防军.md "wikilink")[装甲车](../Page/装甲车.md "wikilink")

## 设计和特点

“野犬”式全防護車由德国[克劳斯-玛菲·威格曼公司设计生产](../Page/克劳斯-玛菲·威格曼.md "wikilink")，使用[烏尼莫克底盘](../Page/烏尼莫克.md "wikilink")。Allschutz-Transport-Fahrzeug的[德文意思解作](../Page/德文.md "wikilink")“全方位防护运输车辆”，顾名思义，良好的防卫性能是该车的一大特点。根据“野犬”的设计，该车能够承受恶劣的路况、[机枪扫射](../Page/机枪.md "wikilink")、小型[反坦克武器的攻击以及通过](../Page/反坦克武器.md "wikilink")[大规模杀伤性武器防护测试认证](../Page/大规模杀伤性武器.md "wikilink")。

## 改型

“野犬2型”是“野犬”的改进型。和“野犬1”相比，“野犬2”仍旧采用了 [烏尼莫克U](../Page/烏尼莫克.md "wikilink")
5000底盘，但提高了防护能力（“野犬2”可以加挂模块式附加装甲）和载荷，並配備了后视摄像机，有利于在城市环境下驾驶车辆。除此之外，野犬2还降低了红外信号特征，在[紅外線熱像儀前面具有一定的隐身能力](../Page/紅外線熱像儀.md "wikilink")。“野犬2型”推出了两个版本：一款为3,250mm[轴距](../Page/轴距.md "wikilink")（3.5吨有效负载），另一款为3,850mm轴距（4吨有效负载）。“野犬2”能容纳8名乘员。目前KMW正在研发内部容纳空间更大的“野犬2
GFF”。

由于载荷和内部空间得到提高，相比起“野犬1”，“野犬2”能够执行更多任务，目前已開發出人员输送车、救护车、货车、指挥控制车、防空车和前线观察车合共6种车型。作为前线观察车时，“野犬2”可安装可伸缩桅杆，桅杆上安装有传感組件、白光/热成像观察设备和护眼型激光测距仪等设备
。\[1\]

## 武装

“野犬”的预设[武器为一挺置于车顶的](../Page/武器.md "wikilink")7.62
mm[遥控](../Page/遥控.md "wikilink")[机枪](../Page/机枪.md "wikilink")，这挺机枪也可以用[12.7
mm机枪或](../Page/白朗寧M2重機槍.md "wikilink")[HK
GMG自动](../Page/HK_GMG自動榴彈發射器.md "wikilink")[榴弹发射器取代](../Page/榴弹发射器.md "wikilink")。

## 实战纪录

2005年6月3日，一辆隶属联邦德国国防军的“野犬”在波斯尼亚进行[巡罗时遭受一枚](../Page/巡罗.md "wikilink")6千克[反坦克地雷攻击](../Page/反坦克地雷.md "wikilink")，但车内乘员安然无恙。

## 使用国

  - [奥地利](../Page/奥地利.md "wikilink")- 35辆 “野犬2型”
  - [比利时](../Page/比利时.md "wikilink")– 220辆 “野犬2型”（+32份订单）
  - [德国](../Page/德国.md "wikilink") - 147辆 “野犬1型” 及
    240辆“野犬2型”（至2011年才全部交货）加上另外78辆具[地面雷达监控平台之](../Page/地面雷达监控平台.md "wikilink")“野犬”
  - [捷克](../Page/捷克.md "wikilink") - 21辆“野犬2型”， 2辆已订购（2008年2月交货）
  - [卢森堡](../Page/卢森堡.md "wikilink") - 48辆“野犬2型”，2辆已订购（2009\~2010年间交货）

## 相關圖片

Image:Fennek-vorn (hell).jpg|一輛隸屬聯邦德國國防軍的野犬式全防护车，停在一輛Fennek裝甲偵察車後面
Image:ATF1 Dingo.jpg|駐紮在科索沃地區的一輛德軍野犬1型全防护车 <File:Bundeswehr> Dingo
outside of Mazar-e-Sharif.jpg|德軍野犬2型，部署於阿富汗Image:ATF Dingo 2 mit
BÜR.jpg|裝有地面監視雷達(BÜR)的德軍野犬2型 Image:ATFDingo2.jpg|

## 相關條目

  - [KMW Grizzly](../Page/KMW_Grizzly.md "wikilink") -
    [德國聯邦國防軍另一款](../Page/德國聯邦國防軍.md "wikilink")[装甲车](../Page/装甲车.md "wikilink")
  - [Mungo ESK](../Page/Mungo_ESK.md "wikilink")
  - [Rheinmetall YAK](../Page/Rheinmetall_YAK.md "wikilink")
  - [Boxer裝甲車](../Page/Boxer裝甲車.md "wikilink") -
    （战斗[装甲车](../Page/装甲车.md "wikilink")）
  - [AGF](../Page/AGF.md "wikilink")
    -（轻型[步兵车](../Page/步兵.md "wikilink")）

## 参考资料

<references />

## 外部链接

  - [Krauss-Maffei Wegmann](http://www.kmweg.de/) - 制造商
  - [Dingo 2 in the Austrian Armed
    Forces](http://www.doppeladler.com/oebh/rad/dingo_2.htm)

[Category:裝甲偵察車](../Category/裝甲偵察車.md "wikilink")
[Category:輕型裝甲車輛](../Category/輕型裝甲車輛.md "wikilink")
[Category:越野车](../Category/越野车.md "wikilink")
[Category:裝甲運兵車](../Category/裝甲運兵車.md "wikilink")

1.  [1](http://www.hudong.com/wiki/%E8%A3%85%E7%94%B2%E6%B1%BD%E8%BD%A6)