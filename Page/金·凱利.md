**尤金·柯倫·凱利**（，），生于[美国](../Page/美国.md "wikilink")[匹茲堡](../Page/匹茲堡.md "wikilink")，[美國](../Page/美國.md "wikilink")[電影](../Page/電影.md "wikilink")[演員](../Page/演員.md "wikilink")、[舞者](../Page/舞者.md "wikilink")、[製作人](../Page/製作人.md "wikilink")、[導演與](../Page/導演.md "wikilink")[歌手](../Page/歌手.md "wikilink")，曾經主演過許多知名[電影](../Page/電影.md "wikilink")，是美國電影史上相當重要的一位演員。1999年，他被美國電影學會選為[百年來最偉大的男演員第](../Page/百年百大明星.md "wikilink")15名。

## 早年生活

[GKellyPittSeniorYB.jpg](https://zh.wikipedia.org/wiki/File:GKellyPittSeniorYB.jpg "fig:GKellyPittSeniorYB.jpg")1933年年鉴上的照片\]\]

## 作品

  - 主演：

<!-- end list -->

  - 1942年：《[for me and my gal](../Page/for_me_and_my_gal.md "wikilink")》
  - 1944年：《[封面女郎](../Page/封面女郎_\(電影\).md "wikilink")》
  - 1945年：《[Anchors Aweigh](../Page/Anchors_Aweigh.md "wikilink")》 -
    提名[奧斯卡最佳男主角](../Page/奧斯卡最佳男主角.md "wikilink")
  - 1948年：《[The pirate](../Page/The_pirate.md "wikilink")》
  - 1949年：《[錦城春色](../Page/錦城春色.md "wikilink")》
  - 1951年：《[花都舞影](../Page/花都舞影.md "wikilink")》 -
    提名[金球獎最佳音樂及喜劇類電影男主角](../Page/金球獎最佳音樂及喜劇類電影男主角.md "wikilink")
  - 1951年：《[It's a big
    country](../Page/It's_a_big_country.md "wikilink")》
  - 1952年：《[萬花嬉春](../Page/萬花嬉春.md "wikilink")》

<!-- end list -->

  - 導演：

<!-- end list -->

  - 1952年：《[萬花嬉春](../Page/萬花嬉春.md "wikilink")》
  - 1969年：《[你好\!多莉\!](../Page/你好!多莉!.md "wikilink")》 -
    提名[金球獎最佳導演](../Page/金球獎最佳導演.md "wikilink")

## 奖项

## 参考资料

## 扩展阅读

  -
## 外部連結

  -
  -
  -
  - [The Gene Kelly Awards - University of
    Pittsburgh](https://web.archive.org/web/20070219070910/http://www.pittsburghclo.org/outreach/gene_kelly.cfm)

  - [Obituary, NY Times,
    February 3, 1996](http://www.nytimes.com/learning/general/onthisday/bday/0823.html)

  - [Naval Intelligence File on Gene
    Kelly](https://web.archive.org/web/20100513033150/http://www.thememoryhole.org/mil/navy/gene_kelly.htm)

  - [Gene Kelly - An American Life -
    PBS](http://www.pbs.org/wnet/americanmasters/database/kelly_g_homepage.html)

[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:美國男舞者](../Category/美國男舞者.md "wikilink")
[Category:匹茲堡大學校友](../Category/匹茲堡大學校友.md "wikilink")
[Category:加利福尼亚州民主党人](../Category/加利福尼亚州民主党人.md "wikilink")
[Category:宾夕法尼亚州民主党人](../Category/宾夕法尼亚州民主党人.md "wikilink")
[Category:奥斯卡荣誉奖获得者](../Category/奥斯卡荣誉奖获得者.md "wikilink")
[Category:美国不可知论者](../Category/美国不可知论者.md "wikilink")
[Category:传统流行音乐歌手](../Category/传统流行音乐歌手.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:前天主教徒](../Category/前天主教徒.md "wikilink")
[Category:歌舞雜耍表演者](../Category/歌舞雜耍表演者.md "wikilink")
[Category:金球獎終身成就獎獲得者](../Category/金球獎終身成就獎獲得者.md "wikilink")
[Category:凱撒電影獎獲得者](../Category/凱撒電影獎獲得者.md "wikilink")
[Category:美國國家藝術獎章獲得者](../Category/美國國家藝術獎章獲得者.md "wikilink")
[Category:美國海軍軍官](../Category/美國海軍軍官.md "wikilink")