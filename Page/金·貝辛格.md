{{ Infobox entertainer | type=女演員 | image = Kim Basinger (2106547618)
crop.JPG | imagesize = 230px | caption = 1990年時的金·貝辛格 | birthdate =  |
location = [雅典](../Page/雅典_\(喬治亞州\).md "wikilink") | birthname = Kimila
Ann Basinger | spouse =
| partner=[戴爾·羅比內特](../Page/戴爾·羅比內特.md "wikilink") |
children=[艾爾蘭·鮑德溫](../Page/艾爾蘭·鮑德溫.md "wikilink") |
academyawards=**[最佳女配角](../Page/奧斯卡最佳女配角獎.md "wikilink")**
1997年 《[洛城机密](../Page/洛城机密.md "wikilink")》 |
goldenglobeawards=**[最佳電影女配角](../Page/金球獎最佳電影女配角.md "wikilink")**
1997年 《[洛城机密](../Page/洛城机密.md "wikilink")》 |
sagawards=**[最佳女配角](../Page/美國演員工會獎最佳女配角.md "wikilink")**
1997年 《[洛城机密](../Page/洛城机密.md "wikilink")》 }} **金·貝辛格**（**Kim
Basinger**，）是一位[美國](../Page/美國.md "wikilink")[電影與](../Page/電影.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")[演員](../Page/演員.md "wikilink")，也曾是一位时尚[模特](../Page/模特.md "wikilink")。出生在[美國](../Page/美國.md "wikilink")[喬治亞州](../Page/喬治亞州.md "wikilink")[雅典](../Page/雅典_\(喬治亞州\).md "wikilink")，她的成名作是1983年的[007系列间谍片外传](../Page/007.md "wikilink")《007外传之巡弋飞弹》，她在片中饰演女主角，邦女郎Domino
Petachi。而1997年上映的电影《[洛城机密](../Page/洛城机密.md "wikilink")》则为她赢得了第70届[奧斯卡最佳女配角獎](../Page/奧斯卡最佳女配角獎.md "wikilink")。

## 生平

[缩略图](https://zh.wikipedia.org/wiki/File:Kim_Basinger24.JPG "fig:缩略图")

## 電影作品

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>作品譯名</p></th>
<th><p>作品名稱</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1981年</p></td>
<td></td>
<td><p>Hard Country</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1982年</p></td>
<td><p><a href="../Page/地獄谷_(電影).md" title="wikilink">地獄谷</a></p></td>
<td><p>Mother Lode</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1983年</p></td>
<td><p><a href="../Page/巡弋飛彈_(電影).md" title="wikilink">巡弋飛彈</a></p></td>
<td><p>Never Say Never Again</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛上女人的男人.md" title="wikilink">愛上女人的男人</a></p></td>
<td><p>The Man Who Loved Women</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td><p><a href="../Page/天生好手.md" title="wikilink">天生好手</a></p></td>
<td><p>The Natural</p></td>
<td><p>提名<a href="../Page/金球獎最佳電影女配角.md" title="wikilink">金球獎最佳電影女配角</a></p></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td><p><a href="../Page/愛情傻子.md" title="wikilink">愛情傻子</a></p></td>
<td><p>Fool for Love</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1986年</p></td>
<td><p><a href="../Page/愛你九週半.md" title="wikilink">愛你九週半</a></p></td>
<td><p>9½ Weeks</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/絕不留情.md" title="wikilink">絕不留情</a></p></td>
<td><p>No Mercy</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p><a href="../Page/盲目約會.md" title="wikilink">盲目約會</a></p></td>
<td><p>Blind Date</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/防彈的愛情.md" title="wikilink">防彈的愛情</a></p></td>
<td><p>Nadine</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1988年</p></td>
<td><p><a href="../Page/我的繼母是外星人.md" title="wikilink">我的繼母是外星人</a></p></td>
<td><p>My Stepmother Is An Alien</p></td>
<td><p>提名<a href="../Page/土星獎.md" title="wikilink">土星獎最佳女主角</a></p></td>
</tr>
<tr class="even">
<td><p>1989年</p></td>
<td><p><a href="../Page/蝙蝠俠_(1989年電影).md" title="wikilink">蝙蝠俠</a></p></td>
<td><p>Batman</p></td>
<td><p>提名<a href="../Page/土星獎.md" title="wikilink">土星獎最佳女配角</a></p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td></td>
<td><p>The Marrying Man</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/辣手美人心.md" title="wikilink">辣手美人心</a></p></td>
<td><p>Final Analysis</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美女闖天關.md" title="wikilink">美女闖天關</a></p></td>
<td><p>Cool World</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神偷俏佳人/霹靂神偷.md" title="wikilink">神偷俏佳人/霹靂神偷</a></p></td>
<td><p>The Real McCoy</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/反斗智多星2.md" title="wikilink">反斗智多星2</a></p></td>
<td><p>Wayne's World 2</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Mary Jane's Last Dance</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p><a href="../Page/世紀電影.md" title="wikilink">世紀電影</a></p></td>
<td><p>A Century of Cinema</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賭命鴛鴦.md" title="wikilink">賭命鴛鴦</a></p></td>
<td><p>The Getaway</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雲裳風暴.md" title="wikilink">雲裳風暴</a></p></td>
<td><p>Ready to Wear</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p><a href="../Page/鐵面特警隊.md" title="wikilink">鐵面特警隊</a></p></td>
<td><p>L.A. Confidential</p></td>
<td><p><a href="../Page/奧斯卡最佳女配角獎.md" title="wikilink">奧斯卡最佳女配角獎</a><br />
<a href="../Page/金球獎最佳電影女配角.md" title="wikilink">金球獎最佳電影女配角</a><br />
<a href="../Page/美國演員工會獎最佳女配角.md" title="wikilink">美國演員工會獎最佳女配角</a><br />
提名<a href="../Page/英國電影學院獎.md" title="wikilink">英國電影學院獎最佳女主角</a></p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/夢遊非洲.md" title="wikilink">夢遊非洲</a></p></td>
<td><p>I Dreamed of Africa</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/靈異總動員.md" title="wikilink">靈異總動員</a></p></td>
<td><p>Bless The Child</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/街頭痞子.md" title="wikilink">街頭痞子</a></p></td>
<td><p>8 Mile</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/致命人脈.md" title="wikilink">致命人脈</a></p></td>
<td><p>People I Know</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/寡居的一年.md" title="wikilink">寡居的一年</a></p></td>
<td><p>The Door in the Floor</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戀上冒牌貨.md" title="wikilink">戀上冒牌貨</a></p></td>
<td><p>Elvis Has Left the Building</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/駁命來電.md" title="wikilink">駁命來電</a></p></td>
<td><p>Cellular</p></td>
<td><p>提名<a href="../Page/土星獎.md" title="wikilink">土星獎最佳女配角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/特勤組_(電影).md" title="wikilink">特勤組</a></p></td>
<td><p>The Sentinel</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/賭徒本色.md" title="wikilink">賭徒本色</a></p></td>
<td><p>Even Money</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/絕命驚狂.md" title="wikilink">絕命驚狂</a></p></td>
<td><p>While She Was Out</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/性、謀殺、天使城.md" title="wikilink">性、謀殺、天使城</a></p></td>
<td><p>The Informers</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/燃燒的平原.md" title="wikilink">燃燒的平原</a></p></td>
<td><p>The Burning Plain</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/生死情緣.md" title="wikilink">生死情緣</a></p></td>
<td><p>Charlie St. Cloud</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/情慾三重奏.md" title="wikilink">情慾三重奏</a></p></td>
<td><p>Third Person</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/進擊的大佬.md" title="wikilink">進擊的大佬</a></p></td>
<td><p>Grudge Match</p></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  -
[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:瑞典裔美國人](../Category/瑞典裔美國人.md "wikilink")
[Category:喬治亞大學校友](../Category/喬治亞大學校友.md "wikilink")
[Category:美国女性模特儿](../Category/美国女性模特儿.md "wikilink")
[Category:奧斯卡最佳女配角獎獲獎演員](../Category/奧斯卡最佳女配角獎獲獎演員.md "wikilink")
[Category:金球獎最佳電影女配角獲得者](../Category/金球獎最佳電影女配角獲得者.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:喬治亞州人](../Category/喬治亞州人.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")