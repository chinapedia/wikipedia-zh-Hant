**金·露華**（，），本名：**瑪麗蓮·寶琳·露華**（）是一位美國電影演員，也是1950年代最受歡迎的演員之一，最知名的演出是[-{zh-cn:阿尔弗雷德·希区柯克;zh-sg:阿尔弗雷德·希区柯克;zh-hk:亞弗列·希治閣;zh-mo:亞弗列·希治閣;zh-tw:亞佛烈德·希區考克;}-所執導的](../Page/亞弗列·希治閣.md "wikilink")《[迷魂記](../Page/迷魂記.md "wikilink")》。

## 生平

## 外部連結

  -
  -
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:美國女性模特兒](../Category/美國女性模特兒.md "wikilink")
[Category:芝加哥藝術學院校友](../Category/芝加哥藝術學院校友.md "wikilink")
[Category:捷克裔美國人](../Category/捷克裔美國人.md "wikilink")
[Category:芝加哥人](../Category/芝加哥人.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")
[Category:美国画家](../Category/美国画家.md "wikilink")