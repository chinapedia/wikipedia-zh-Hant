**金侖珍**（，），[韓裔美籍](../Page/韓裔美國人.md "wikilink")[女演員](../Page/演員.md "wikilink")，1983年移民[美國](../Page/美國.md "wikilink")，畢業於[波士頓大學公演藝術學系](../Page/波士頓大學.md "wikilink")，大學時至[英國留學](../Page/英國.md "wikilink")（英美戲劇學會British
American drama
academy），演出1999年[韓國賣座電影](../Page/韓國.md "wikilink")《[鱼](../Page/鱼_\(电影\).md "wikilink")》而於韓國竄紅，及後更在[ABC電視台影集](../Page/ABC電視台.md "wikilink")《[迷失](../Page/迷失.md "wikilink")》中演出。其名字常被音譯成**金允珍**。

## 演出作品

### 電影

  - 1997年：《》
  - 1997年：《》飾演 女演員
  - 1999年：《[魚](../Page/生死諜變.md "wikilink")》飾演 李明賢
  - 2000年：《》飾演 燕
  - 2000年：《Rush》
  - 2002年：《》飾演 智莉
  - 2002年：《》飾演 盧熙秀
  - 2002年：《[密愛](../Page/密愛.md "wikilink")》飾演 美欣
  - 2005年：《》飾演 徐允熙
  - 2007年：《》飾演 劉智妍
  - 2007年：《鳳仙花》飾演 侖珍
  - 2010年：《》飾演 洪正惠
  - 2011年：《》飾演 蔡妍熙
  - 2012年：《[鄰居](../Page/鄰居_\(電影\).md "wikilink")》飾演 宋慶熙
  - 2014年：《[國際市場](../Page/國際市場.md "wikilink")》飾演 英子
  - 2017年：《》飾演 美熙

### 電視劇

#### 韓國電視劇

  - 1996年：[MBC](../Page/文化廣播公司.md "wikilink")《》飾演 姜美琳
  - 1997年：MBC《》飾演 張世英
  - 1997年：[KBS2](../Page/KBS第2頻道.md "wikilink")《》飾演 尹真雅
  - 1999年：KBS《》飾演 張熙珠
  - 2018年：SBS《[Ms.Ma，復仇的女神](../Page/Ms.Ma，復仇的女神.md "wikilink")》飾演 Ms.Ma

#### 美國電視劇

  - 2004年：[ABC](../Page/美國廣播公司.md "wikilink")《[LOST檔案](../Page/LOST檔案.md "wikilink")》（2004年－2010年，第1－6季，共出演88集）
  - 2013年：ABC《》（2013年－至今，第1季起）

### MV

  - 2006年：張慧真《不要離開》（與[全美善](../Page/全美善.md "wikilink")）

## 獲獎

  - 1999年：第36屆[大鐘獎](../Page/大鐘獎.md "wikilink")－女新人獎（生死諜變）
  - 1999年：第22屆黃金攝影獎－女新人獎（生死諜變）
  - 2002年：第23屆[青龍獎](../Page/青龍獎.md "wikilink")－最佳女主角（密愛）
  - 2002年：第5屆Director's Cut Awards－年度演員獎（密愛）
  - 2008年：第45屆大鐘獎－最佳女主角（七天）

## 外部連結

  - [EPG](https://web.archive.org/web/20070828163112/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=1042)


[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[Category:韓國電影演員](../Category/韓國電影演員.md "wikilink")
[Category:美國女歌手](../Category/美國女歌手.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:美國舞者](../Category/美國舞者.md "wikilink")
[Category:波士頓大學校友](../Category/波士頓大學校友.md "wikilink")
[Category:归化美国公民的韩国人](../Category/归化美国公民的韩国人.md "wikilink")
[Category:首爾特別市出身人物](../Category/首爾特別市出身人物.md "wikilink")
[Y](../Category/移民美國的韓國人.md "wikilink")
[Category:韩国女演员](../Category/韩国女演员.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:美國女配音演員](../Category/美國女配音演員.md "wikilink")
[Category:美国女歌手](../Category/美国女歌手.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")