[Vajrasattva_Tibet.jpg](https://zh.wikipedia.org/wiki/File:Vajrasattva_Tibet.jpg "fig:Vajrasattva_Tibet.jpg")像\]\]
[Vajra.jpg](https://zh.wikipedia.org/wiki/File:Vajra.jpg "fig:Vajra.jpg")

**金剛**（；；），漢語音譯為**嚩日羅（或啰）**、**伐折羅（或啰）**、**伐闍羅（或啰）**、**伐折啰**、**跋折啰**、**耽啰**等，藏語音譯為**多傑**（Dojre）、**班杂**（benza）。[梵文名詞](../Page/梵文.md "wikilink")，最早是印度神話中[因陀羅的武器](../Page/因陀羅.md "wikilink")，也是[鑽石以及](../Page/鑽石.md "wikilink")[閃電的梵文名稱](../Page/閃電.md "wikilink")。在大乘佛法經論中，金剛係指法界中有一法是堅固無能截斷者，但又因沒有另一法可替代或毀壞的緣故，稱這不可被毀壞、替換之法為金剛\[1\]。在宗教儀式中使用的法器，也稱金剛，中文又將它譯為**[金剛杵](../Page/金剛杵.md "wikilink")**、**降魔杵**。

## 概論

### 來源

這個名稱最早出現在婆羅門教《[梨俱吠陀](../Page/梨俱吠陀.md "wikilink")》，是[因陀羅的武器名稱](../Page/因陀羅.md "wikilink")，名叫金剛（vájra），即是[閃電](../Page/閃電.md "wikilink")，有人認為[北歐神話的雷神之錘](../Page/北歐神話.md "wikilink")[妙爾尼爾与](../Page/妙爾尼爾.md "wikilink")[印度神話的金剛同源](../Page/印度神話.md "wikilink")。

相傳因陀羅與[阿修羅戰鬥時](../Page/阿修羅.md "wikilink")，猛力擊打阿修羅頭部，碎裂的閃電成為[鑽石](../Page/鑽石.md "wikilink")（一說是阿修羅的屍體成為鑽石），因此鑽石也被稱為金剛（vájra）。

### 文化影響

在宗教儀式中，使用一種小型的[杵形武器](../Page/杵.md "wikilink")，來象徵因陀羅的雷電。它的大小類似於日本[柔道棒](../Page/柔道棒.md "wikilink")（Yawara），也被當成是一種[護身符](../Page/護身符.md "wikilink")。[印度教](../Page/印度教.md "wikilink")、[藏傳佛教與](../Page/藏傳佛教.md "wikilink")[耆那教](../Page/耆那教.md "wikilink")，皆以金鋼杵來作為宗教器具，認為它代表了一種精神上的力量。

在西藏佛教[密宗中](../Page/密宗.md "wikilink")，金剛杵是常見的法器，常為[護法神手持](../Page/護法神.md "wikilink")，象徵能夠摧伏[外道](../Page/外道.md "wikilink")、擊敗[惡魔的力量](../Page/惡魔.md "wikilink")。這些護法神被稱為[執金剛神](../Page/執金剛神.md "wikilink")，[金剛力士或](../Page/金剛力士.md "wikilink")[密跡金剛](../Page/密跡金剛.md "wikilink")，簡稱為金剛\[2\]。也常有佛菩薩以金剛為名號者，如[金剛不動佛](../Page/阿閦佛.md "wikilink")（[阿閦佛](../Page/阿閦佛.md "wikilink")）、[金剛薩埵](../Page/金剛薩埵.md "wikilink")。

[大乘佛教](../Page/大乘佛教.md "wikilink")，用此「金剛」來形容[如來藏空性心](../Page/如來藏.md "wikilink")、無心相心、非心心，不取六塵萬法，無可摧毀。因此性無可毀壞，性如金剛，即使集於百萬億佛之力亦無法毀壞祂，所以稱指「金剛心」堅固無比，能破斥常見外道以見聞覺知心為真我，及破斥斷見外道以一切法皆是空無了不可得，無因無果，死後墮於灰滅空無者的邪說謬論。所以經常以金剛來作為[般若空慧的象徵](../Page/般若.md "wikilink")，代表它能夠擊破一切[邪見與](../Page/邪見.md "wikilink")[結縛](../Page/結縛.md "wikilink")。

[藏密中](../Page/藏密.md "wikilink")，金剛也作為無堅不摧、無可摧破的特性而廣泛運用。譬如：“[金剛乘](../Page/金剛乘.md "wikilink")”，就是“[密宗](../Page/密宗.md "wikilink")”，是形容無堅不摧的“密乘”；“金剛禪”，就是指“密宗禪法”，是形容戰勝外道禪法的“密法”。

## 註釋

[V](../Category/佛教术语.md "wikilink") [V](../Category/傳說兵器.md "wikilink")
[Category:密宗術語](../Category/密宗術語.md "wikilink")

1.  《佛說寶積三昧文殊師利菩薩問法身經》卷1：“佛問。何謂金剛。答言無能截斷者。以故名曰金剛。佛不可議。諸法亦不可議。以是為金剛。”(CBETA,
    T12, no. 356, p. 238, a4-6)
2.  《[初刻拍案驚奇](../Page/初刻拍案驚奇.md "wikilink")》：「壇中有一重[菩薩](../Page/菩薩.md "wikilink")，外有一重金甲神人，又外有一重**金剛**圍著，聖賢比肩，環繞甚嚴。」