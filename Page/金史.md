《**金史**》為[元朝](../Page/元朝.md "wikilink")[脱脱等撰之](../Page/脱脱.md "wikilink")[纪传体](../Page/纪传体.md "wikilink")[金代史](../Page/金朝.md "wikilink")，共135卷。包括[本纪](../Page/本纪.md "wikilink")19卷、[志](../Page/志.md "wikilink")39卷、[表](../Page/表.md "wikilink")4卷、[列传](../Page/列传.md "wikilink")71卷，记述了从[女真族的兴起到金朝建立和灭亡](../Page/女真.md "wikilink")。後附〈金國語解〉一卷。[清朝](../Page/清朝.md "wikilink")[施国祁有](../Page/施国祁.md "wikilink")《[金史詳校](../Page/金史詳校.md "wikilink")》10卷。

## 過程

《金史》纂修始於[元世祖](../Page/元世祖.md "wikilink")[忽必烈时期](../Page/忽必烈.md "wikilink")。[中统二年](../Page/中统.md "wikilink")（1261年），议修[辽](../Page/辽朝.md "wikilink")、金二史。灭[宋後又议修辽](../Page/宋朝.md "wikilink")、金、宋三史。但终因体例无法确定而不能成书。[元顺帝](../Page/元顺帝.md "wikilink")[至正三年](../Page/至正.md "wikilink")（1343年）三月，诏修辽、金、宋三史，以中书右丞相脱脱为[都总裁官](../Page/都总裁官.md "wikilink")，翰林学士[欧阳玄等六人为](../Page/欧阳玄.md "wikilink")[总裁官](../Page/总裁官.md "wikilink")，主持修纂。五年十月，辽、金、宋三史告成。元人修《金史》所依据的资料主要有金代[实录](../Page/实录.md "wikilink")，包括[完颜勖所撰](../Page/完颜勖.md "wikilink")《始祖以下十帝实录》三卷，以及《太祖实录》、《太宗实录》、《熙宗实录》、《海陵实录》、《世宗实录》、《章宗实录》、《宣宗实录》等。唯卫绍王无实录，现存的材料是由[王鹗多方收集而来](../Page/王鹗.md "wikilink")。金末的事迹则多取材於[元好问的](../Page/元好问.md "wikilink")《[壬辰杂编](../Page/壬辰杂编.md "wikilink")》和[刘祁](../Page/刘祁.md "wikilink")《[归潜志](../Page/归潜志.md "wikilink")》、[王鹗](../Page/王鹗.md "wikilink")《[汝南遗事](../Page/汝南遗事.md "wikilink")》、[杨奂](../Page/杨奂.md "wikilink")《[天兴近鉴](../Page/天兴近鉴.md "wikilink")》等。由于《金史》依据比较完整的《实录》，经元好问、王鹗等补充，元初以来又经幾度修撰，实际上是经营已久，与《宋史》、《辽史》二史仓卒成书不同，故在三史之中号称最善。

## 內容

体例上，《金史》的本纪第一卷是以《世纪》为开始，追述阿骨打以前十代的事迹。本纪的最后一卷，又增《世纪补》，用以记述熙宗之父景宣帝、金世宗之父睿宗、金章宗之父显宗。传记中有关诸人參與的同一事件，则在首要人物的传中详见事实本末，其他传中则旁参侧见，避免重述。《金史》有交聘表，详细记载金与宋﹑西夏﹑高丽的使臣往来。

《金史》也存在一些记事互相矛盾、重叠，史实错误、疏略，年次颠倒脱舛，人地名混乱和记述战事则扬胜讳败的现象，例如金初建策阿骨打称帝的渤海人[杨朴是重要人物](../Page/杨朴.md "wikilink")，“诸事革创，朝仪制度，皆出其手”，《金史》卻沒有記載此人。《金史》為[张邦昌立傳](../Page/张邦昌.md "wikilink")，似有小題大作之嫌，邦昌雖受金封冊，但隨即反正，又為宋廷所殺。《宋史》已立傳，《金史》實不用再立傳。清人[施国祁著](../Page/施国祁.md "wikilink")《[金史详校](../Page/金史详校.md "wikilink")》十卷，[校勘和考订](../Page/校勘.md "wikilink")《金史》四千餘条。另外[钱大昕著有](../Page/钱大昕.md "wikilink")《宋辽金元四史朔闰考》，[倪灿](../Page/倪灿.md "wikilink")、[卢文弨著](../Page/卢文弨.md "wikilink")《补辽金元艺文志》，[金门诏著](../Page/金门诏.md "wikilink")《补三史艺文志》等。

## 版本

《金史》於元至正年间刻成。[明代有](../Page/明朝.md "wikilink")[南北两监本](../Page/南北两监本.md "wikilink")、清有[武英殿本](../Page/武英殿本.md "wikilink")。[乾隆间](../Page/乾隆.md "wikilink")，四库馆臣[校勘](../Page/校勘.md "wikilink")[武英殿本](../Page/武英殿.md "wikilink")，将地名、人名等译名任意改译，造成混乱。1935年，[商务印书馆出版的](../Page/商务印书馆.md "wikilink")[百衲本](../Page/百衲本.md "wikilink")《金史》，是以元至正刊一百三十五卷本（其中八十卷是初刻，五十五卷是后来的复刻本）影印的。1975年，[中华书局出版了](../Page/中华书局.md "wikilink")《金史》标点[校勘本](../Page/校勘.md "wikilink")，以百衲本为[底本](../Page/底本.md "wikilink")，并与监本、殿本参考，又参考有关史料进行[校勘](../Page/校勘.md "wikilink")，吸取了前人的考订成果，是目前较好的版本。

## 篇目

### 本紀

1.  本紀第一 - 世紀
2.  本紀第二 - [太祖](../Page/阿骨打.md "wikilink")
3.  本紀第三 - [太宗](../Page/吴乞買.md "wikilink")
4.  本紀第四 - [熙宗](../Page/金熙宗.md "wikilink")
5.  本紀第五 - [海陵](../Page/海陵王.md "wikilink")
6.  本紀第六 - [世宗上](../Page/金世宗.md "wikilink")
7.  本紀第七 - 世宗中
8.  本紀第八 - 世宗下
9.  本紀第九 - [章宗一](../Page/金章宗.md "wikilink")
10. 本紀第十 - 章宗二
11. 本紀第十一- 章宗三
12. 本紀第十二 - 章宗四
13. 本紀第十三 - [衛紹王](../Page/衛紹王.md "wikilink")
14. 本紀第十四 - [宣宗上](../Page/金宣宗.md "wikilink")
15. 本紀第十五 - 宣宗中
16. 本紀第十六 - 宣宗下
17. 本紀第十七 - [哀宗上](../Page/金哀宗.md "wikilink")
18. 本紀第十八 - 哀宗下
19. 本紀第十九 - 世紀補

### 志

1.  志第一 - 天文
2.  志第二 - 历上
3.  志第三 - 历下
4.  志第四 - 五行
5.  志第五 - 地理上
6.  志第六 - 地理中
7.  志第七 - 地理下
8.  志第八 - 河渠
9.  志第九 - 礼一
10. 志第十 - 礼二
11. 志第十一 - 礼三
12. 志第十二 - 礼四
13. 志第十三 - 礼五
14. 志第十四 - 礼六
15. 志第十五 - 礼七
16. 志第十六 - 礼八
17. 志第十七 - 礼九
18. 志第十八 - 礼十
19. 志第十九 - 礼十一
20. 志第二十 - 乐上
21. 志第二十一 - 乐下
22. 志第二十二 - 儀衛上
23. 志第二十三 - 儀衛下
24. 志第二十四 - 輿服
25. 志第二十五 - 兵
26. 志第二十六 - 刑
27. 志第二十七 - 食貨一
28. 志第二十八 - 食貨二
29. 志第二十九 - 食貨三
30. 志第三十 - 食貨四
31. 志第三十一 - 食貨五
32. 志第三十二 - 選举一
33. 志第三十三 - 選举二
34. 志第三十四 - 選举三
35. 志第三十五 - 選举四
36. 志第三十六 - 百官一
37. 志第三十七 - 百官二
38. 志第三十八 - 百官三
39. 志第三十九 - 百官四

### 表

1.  表第一 - 宗室表
2.  表第二 - 交聘表上
3.  表第三 - 交聘表中
4.  表第四 - 交聘表下

### 列傳

1.  列傳第一 后妃上 -
    [始祖明懿皇后](../Page/明懿皇后.md "wikilink")・[德帝思皇后](../Page/思皇后_\(金德帝\).md "wikilink")・[安帝節皇后](../Page/節皇后.md "wikilink")・[献祖恭靖皇后](../Page/恭靖皇后.md "wikilink")・[昭祖威順皇后](../Page/徒單烏古論都葛.md "wikilink")・[景祖昭肅皇后](../Page/唐括多保真.md "wikilink")・[世祖翼簡皇后](../Page/翼簡皇后.md "wikilink")・[肅宗靖宣皇后](../Page/靖宣皇后.md "wikilink")・[穆宗貞惠皇后](../Page/貞惠皇后_\(金穆宗\).md "wikilink")・[康宗敬僖皇后](../Page/敬僖皇后.md "wikilink")・[太祖聖穆皇后](../Page/欽憲皇后.md "wikilink")・[太宗欽仁皇后](../Page/欽仁皇后.md "wikilink")・[熙宗悼平皇后](../Page/悼平皇后.md "wikilink")・[海陵嫡母徒单氏](../Page/哀皇后_\(金德宗\).md "wikilink")
2.  列傳第二 后妃下 -
    [睿宗欽慈皇后](../Page/欽慈皇后_\(金朝\).md "wikilink")・[世宗昭德皇后](../Page/明德皇后_\(金朝\).md "wikilink")・[显宗孝懿皇后](../Page/孝懿皇后_\(金顯宗\).md "wikilink")・[章宗欽怀皇后](../Page/欽懷皇后.md "wikilink")・[衛紹王后徒单氏](../Page/徒單皇后_\(金衛紹王\).md "wikilink")・[宣宗皇后王氏](../Page/王皇后_\(金宣宗\).md "wikilink")・[哀宗皇后徒单氏](../Page/徒單皇后_\(金哀宗\).md "wikilink")
3.  列傳第三 始祖以下諸子 -
    [完顏斡魯](../Page/完顏斡魯.md "wikilink")・[完顏輩魯](../Page/完顏輩魯.md "wikilink")・[完顏謝庫德](../Page/完顏謝庫德.md "wikilink")・[完顏謝夷保](../Page/完顏謝夷保.md "wikilink")・[完顏拔達](../Page/完顏拔達.md "wikilink")・[完顏謝里忽](../Page/完顏謝里忽.md "wikilink")・[完顏烏古出](../Page/完顏烏骨出.md "wikilink")・[完顏跋黑](../Page/完顏跋黑.md "wikilink")・[完顏劾孫](../Page/完顏劾孫.md "wikilink")・[完顏麻頗](../Page/完顏麻頗.md "wikilink")・[完顏謾都訶](../Page/完顏謾都訶.md "wikilink")・[完顏斡帶](../Page/完顏斡帶.md "wikilink")・[完顏斡賽](../Page/完顏斡賽.md "wikilink")・[完顏斡者](../Page/完顏斡者.md "wikilink")・[完顏昂](../Page/完顏昂.md "wikilink")
4.  列傳第四 宗室 -
    [完顏勗](../Page/完顏勗.md "wikilink")・[完顏宗秀](../Page/完顏宗秀.md "wikilink")・[完顏隈可](../Page/完顏隈可.md "wikilink")・[完顏胡十門](../Page/完顏胡十門.md "wikilink")・[完顏合住](../Page/完顏合住.md "wikilink")・[完顏掴保](../Page/完顏掴保.md "wikilink")・[完顏衷](../Page/完顏衷.md "wikilink")・[完顏齐](../Page/完顏齐.md "wikilink")・[完顏朮魯](../Page/完顏朮魯.md "wikilink")・[完顏胡石改](../Page/完顏胡石改.md "wikilink")・[完顏宗賢](../Page/完顏宗賢.md "wikilink")・[完顏撻懶](../Page/完颜昌.md "wikilink")・[完顏卞](../Page/完顏卞.md "wikilink")・[完顏膏](../Page/完顏膏.md "wikilink")・[完顏弈](../Page/完顏弈.md "wikilink")・[完顏阿喜](../Page/完顏阿喜.md "wikilink")
5.  列傳第五 -
    [石顯](../Page/石显_\(金\).md "wikilink")・[桓赧](../Page/桓赧.md "wikilink")・[散達](../Page/散達.md "wikilink")・[烏春](../Page/烏春.md "wikilink")・[溫敦蒲刺](../Page/溫敦蒲刺.md "wikilink")・臘醅・[麻產](../Page/麻產.md "wikilink")・[鈍恩](../Page/鈍恩.md "wikilink")・[留可](../Page/留可.md "wikilink")・[阿疎](../Page/阿疎.md "wikilink")・[奚王回离保](../Page/回离保.md "wikilink")
6.  列傳第六 -
    [完顏欢都](../Page/完顏欢都.md "wikilink")・[完顏冶訶](../Page/完顏冶訶.md "wikilink")
7.  列傳第七 太祖諸子 -
    [完顏宗雋](../Page/完顏宗雋.md "wikilink")・[完顏宗傑](../Page/完顏宗傑.md "wikilink")・[完顏宗強](../Page/完顏宗強.md "wikilink")・[完顏宗敏](../Page/完顏宗敏.md "wikilink")
8.  列傳第八 -
    [完顏撒改](../Page/完顏撒改.md "wikilink")・[完顏習不失](../Page/完顏習不失.md "wikilink")・[完顏石土門](../Page/完顏石土門.md "wikilink")
9.  列傳第九 -
    [完顏斡魯](../Page/完顏斡魯.md "wikilink")・[完顏斡魯](../Page/完顏斡魯.md "wikilink")・[完顏婆盧火](../Page/完顏婆盧火.md "wikilink")・[完顏闍母](../Page/完顏闍母.md "wikilink")
10. 列傳第十 -
    [完顏婁室](../Page/完顏婁室.md "wikilink")・[完顏銀朮可](../Page/完顏銀朮可.md "wikilink")・[完顏麻吉](../Page/完顏麻吉.md "wikilink")・[完顏拔离速](../Page/完顏拔离速.md "wikilink")・[完顏習古廼](../Page/完顏習古廼.md "wikilink")
11. 列傳第十一 -
    [完顏阿离合懣](../Page/完顏阿离合懣.md "wikilink")・[完顏宗道](../Page/完顏宗道.md "wikilink")・[完顏宗雄](../Page/完顏宗雄.md "wikilink")・[完顏希尹](../Page/完顏希尹.md "wikilink")・[完顏谷神](../Page/完顏谷神.md "wikilink")
12. 列傳第十二 -
    [完顏宗翰](../Page/完顏宗翰.md "wikilink")・[完顏宗望](../Page/完顏宗望.md "wikilink")
13. 列傳第十三 -
    [盧彦倫](../Page/盧彦倫.md "wikilink")・[毛子廉](../Page/毛子廉.md "wikilink")・[李三錫](../Page/李三錫.md "wikilink")・[孔敬宗](../Page/孔敬宗.md "wikilink")・[李師夔](../Page/李師夔.md "wikilink")・[沈璋](../Page/沈璋.md "wikilink")・[左企弓](../Page/左企弓.md "wikilink")・[虞仲文](../Page/虞仲文.md "wikilink")・[左泌](../Page/左泌.md "wikilink")
14. 列傳第十四 -
    [完顏宗磐](../Page/完顏宗磐.md "wikilink")・[完顏宗固](../Page/完顏宗固.md "wikilink")・[完顏宗本](../Page/完顏宗本.md "wikilink")・[完顏杲](../Page/完顏杲.md "wikilink")・[完顏宗幹](../Page/完顏宗幹.md "wikilink")
15. 列傳第十五 -
    [完顏宗弼](../Page/完顏宗弼.md "wikilink")・[張邦昌](../Page/張邦昌.md "wikilink")・[劉豫](../Page/劉豫.md "wikilink")・[完顏昌](../Page/完顏昌.md "wikilink")
16. 列傳第十六 -
    [劉彦宗](../Page/劉彦宗.md "wikilink")・[時立愛](../Page/時立愛.md "wikilink")・[韓企先](../Page/韓企先.md "wikilink")
17. 列傳第十七 -
    [酈瓊](../Page/酈瓊.md "wikilink")・[李成](../Page/李成_\(金\).md "wikilink")・[孔彦舟](../Page/孔彦舟.md "wikilink")・[徐文](../Page/徐文.md "wikilink")・[施宜生](../Page/施宜生.md "wikilink")・[張中孚](../Page/張中孚.md "wikilink")・[張中彦](../Page/張中彦.md "wikilink")・[宇文虚中](../Page/宇文虚中.md "wikilink")・[王倫](../Page/王倫.md "wikilink")
18. 列傳第十八 -
    [完顏济安](../Page/完顏济安.md "wikilink")・[完顏道济](../Page/完顏道济.md "wikilink")・[斜卯阿里](../Page/斜卯阿里.md "wikilink")・[突合速](../Page/突合速.md "wikilink")・[烏延浦盧渾](../Page/烏延浦盧渾.md "wikilink")・[赤盞暉](../Page/赤盞暉.md "wikilink")・[大㚖](../Page/大㚖.md "wikilink")・[撻不野](../Page/撻不野.md "wikilink")・[完顏阿离補](../Page/完顏阿离補.md "wikilink")
19. 列傳第十九 -
    [鶻謀琶](../Page/鶻謀琶.md "wikilink")・[迪姑迭](../Page/迪姑迭.md "wikilink")・[阿徒罕](../Page/阿徒罕.md "wikilink")・[夾谷謝奴](../Page/夾谷謝奴.md "wikilink")・[阿勒根没都魯](../Page/阿勒根没都魯.md "wikilink")・[黄掴敵古本](../Page/黄掴敵古本.md "wikilink")・[蒲察胡盞](../Page/蒲察胡盞.md "wikilink")・[夾谷吾里補](../Page/夾谷吾里補.md "wikilink")・[王伯龍](../Page/王伯龍.md "wikilink")・[高彪](../Page/高彪.md "wikilink")・[温迪罕蒲里特](../Page/温迪罕蒲里特.md "wikilink")・[伯德特离補](../Page/伯德特离補.md "wikilink")・[耶律怀義](../Page/耶律怀義.md "wikilink")・[蕭王家奴](../Page/蕭王家奴.md "wikilink")・[田顥](../Page/田顥.md "wikilink")・趙隇
20. 列傳第二十 -
    [郭藥師](../Page/郭藥師.md "wikilink")・[耶律塗山](../Page/耶律塗山.md "wikilink")・[烏延胡里改](../Page/烏延胡里改.md "wikilink")・[烏延吾里補](../Page/烏延吾里補.md "wikilink")・[蕭恭](../Page/蕭恭.md "wikilink")・[完顏習不主](../Page/完顏習不主.md "wikilink")・[紇石烈胡剌](../Page/紇石烈胡剌.md "wikilink")・[耶律恕](../Page/耶律恕.md "wikilink")・[郭企忠](../Page/郭企忠.md "wikilink")・[烏孫訛論](../Page/烏孫訛論.md "wikilink")・[顏盞門都](../Page/顏盞門都.md "wikilink")・[僕散渾坦](../Page/僕散渾坦.md "wikilink")・[鄭建充](../Page/鄭建充.md "wikilink")・[烏古論三合](../Page/烏古論三合.md "wikilink")・[移剌温](../Page/移剌温.md "wikilink")・[蕭仲恭](../Page/蕭仲恭.md "wikilink")・[高松](../Page/高松_\(金\).md "wikilink")・[完顏光英](../Page/完顏光英.md "wikilink")・[完顏元寿](../Page/完顏元寿.md "wikilink")・[完顏矧思阿補](../Page/完顏矧思阿補.md "wikilink")
21. 列傳第二十一 -
    [張通古](../Page/張通古.md "wikilink")・[張浩](../Page/張浩.md "wikilink")・[張玄素](../Page/張玄素.md "wikilink")・[耶律安礼](../Page/耶律安礼.md "wikilink")・[納合椿年](../Page/納合椿年.md "wikilink")・[祁宰](../Page/祁宰.md "wikilink")
22. 列傳第二十二 -
    [完顏杲](../Page/完顏杲.md "wikilink")・[耨怨温敦思忠](../Page/耨怨温敦思忠.md "wikilink")・[完顏昂](../Page/完顏昂.md "wikilink")・[高楨](../Page/高楨.md "wikilink")・[白彦敬](../Page/白彦敬.md "wikilink")・[張景仁](../Page/張景仁.md "wikilink")
23. 列傳第二十三 世宗諸子 -
    [完顏永中](../Page/完顏永中.md "wikilink")・[完顏永蹈](../Page/完顏永蹈.md "wikilink")・[完顏永功](../Page/完顏永功.md "wikilink")・[完顏永德](../Page/完顏永德.md "wikilink")・[完顏永成](../Page/完顏永成.md "wikilink")・[完顏永升](../Page/完顏永升.md "wikilink")
24. 列傳第二十四 -
    [李石](../Page/李石.md "wikilink")・[完顏福寿](../Page/完顏福寿.md "wikilink")・[独吉義](../Page/独吉義.md "wikilink")・[烏延蒲离黑](../Page/烏延蒲离黑.md "wikilink")・[烏延蒲轄奴](../Page/烏延蒲轄奴.md "wikilink")・[李師雄](../Page/李師雄.md "wikilink")・[尼厖古鈔兀](../Page/尼厖古鈔兀.md "wikilink")・[孛朮魯定方](../Page/孛朮魯定方.md "wikilink")・[夾谷胡剌](../Page/夾谷胡剌.md "wikilink")・[蒲察斡論](../Page/蒲察斡論.md "wikilink")・[夾谷査剌](../Page/夾谷査剌.md "wikilink")
25. 列傳第二十五 -
    [紇石烈志寧](../Page/紇石烈志寧.md "wikilink")・[僕散忠義](../Page/僕散忠義.md "wikilink")・[徒单合喜](../Page/徒单合喜.md "wikilink")
26. 列傳第二十六 -
    [紇石烈良弼](../Page/紇石烈良弼.md "wikilink")・[完顏守道](../Page/完顏守道.md "wikilink")・[石琚](../Page/石琚.md "wikilink")・[唐括安礼](../Page/唐括安礼.md "wikilink")・[移剌道](../Page/移剌道.md "wikilink")
27. 列傳第二十七 -
    [蘇保衡](../Page/蘇保衡.md "wikilink")・[翟永固](../Page/翟永固.md "wikilink")・[魏子平](../Page/魏子平.md "wikilink")・[孟浩](../Page/孟浩.md "wikilink")・[梁肅](../Page/梁肅.md "wikilink")・[移剌慥](../Page/移剌慥.md "wikilink")・[移剌子敬](../Page/移剌子敬.md "wikilink")
28. 列傳第二十八 -
    [趙元](../Page/趙元.md "wikilink")・[移剌道](../Page/移剌道.md "wikilink")・[高德基](../Page/高德基.md "wikilink")・[馬諷](../Page/馬諷.md "wikilink")・[完顏兀不喝](../Page/完顏兀不喝.md "wikilink")・[劉徽柔](../Page/劉徽柔.md "wikilink")・[賈少沖](../Page/賈少沖.md "wikilink")・[移剌斡里朶](../Page/移剌斡里朶.md "wikilink")・[阿勒根彦忠](../Page/阿勒根彦忠.md "wikilink")・[張九思](../Page/張九思.md "wikilink")・[高衎](../Page/高衎.md "wikilink")・[楊邦基](../Page/楊邦基.md "wikilink")・[丁暐仁](../Page/丁暐仁.md "wikilink")
29. 列傳第二十九 -
    [完顏撒改](../Page/完顏撒改.md "wikilink")・[龐迪](../Page/龐迪.md "wikilink")・[温迪罕移室懣](../Page/温迪罕移室懣.md "wikilink")・[神土懣](../Page/神土懣.md "wikilink")・[移剌成](../Page/移剌成.md "wikilink")・[石抹卞](../Page/石抹卞.md "wikilink")・[楊仲武](../Page/楊仲武.md "wikilink")・[蒲察世傑](../Page/蒲察世傑.md "wikilink")・[蕭怀忠](../Page/蕭怀忠.md "wikilink")・[移剌按荅](../Page/移剌按荅.md "wikilink")・[孛朮魯阿魯罕](../Page/孛朮魯阿魯罕.md "wikilink")・[趙興祥](../Page/趙興祥.md "wikilink")・[石抹荣](../Page/石抹荣.md "wikilink")・[敬嗣暉](../Page/敬嗣暉.md "wikilink")
30. 列傳第三十 -
    [毛碩](../Page/毛碩.md "wikilink")・[李上達](../Page/李上達.md "wikilink")・[曹望之](../Page/曹望之.md "wikilink")・[大怀貞](../Page/大怀貞.md "wikilink")・[盧孝俭](../Page/盧孝俭.md "wikilink")・[盧庸](../Page/盧庸.md "wikilink")・[李偲](../Page/李偲.md "wikilink")・[徒单克寧](../Page/徒单克寧.md "wikilink")
31. 列傳第三十一 -
    [鄆王琮](../Page/完顏琮.md "wikilink")・[瀛王瓌](../Page/完顏瓌.md "wikilink")・[霍王从彝](../Page/完顏从彝.md "wikilink")・[瀛王从憲](../Page/完顏从憲.md "wikilink")・[温王玠](../Page/完顏玠.md "wikilink")・[完顏洪裕](../Page/完顏洪裕.md "wikilink")・[完顏洪靖](../Page/完顏洪靖.md "wikilink")・[完顏洪熙](../Page/完顏洪熙.md "wikilink")・[完顏洪衍](../Page/完顏洪衍.md "wikilink")・[完顏洪輝](../Page/完顏洪輝.md "wikilink")・[完顏忒隣](../Page/完顏洪烈.md "wikilink")・[完顏按辰](../Page/完顏按辰.md "wikilink")・[完顏从恪](../Page/完顏从恪.md "wikilink")・[完顏守忠](../Page/完顏守忠.md "wikilink")・[完顏玄龄](../Page/完顏玄龄.md "wikilink")・[完顏守純](../Page/完顏守純.md "wikilink")・[独吉思忠](../Page/独吉思忠.md "wikilink")・[完顏承裕](../Page/完顏承裕.md "wikilink")・[僕散揆（臨喜）](../Page/僕散揆.md "wikilink")・抹撚史扢搭・[完顏宗浩](../Page/完顏宗浩.md "wikilink")
32. 列傳第三十二 -
    [夾谷清臣](../Page/夾谷清臣.md "wikilink")・[完顏襄](../Page/完顏襄.md "wikilink")・[夾谷衡](../Page/夾谷衡.md "wikilink")・[完顏安国](../Page/完顏安国.md "wikilink")・[瑤里孛迭](../Page/瑤里孛迭.md "wikilink")
33. 列傳第三十三 -
    [移剌履](../Page/移剌履.md "wikilink")・[張萬公](../Page/張萬公.md "wikilink")・[蒲察通](../Page/蒲察通.md "wikilink")・[粘割斡特剌](../Page/粘割斡特剌.md "wikilink")・[程輝](../Page/程輝.md "wikilink")・[劉瑋](../Page/劉瑋.md "wikilink")・[董師中](../Page/董師中.md "wikilink")・[王蔚](../Page/王蔚.md "wikilink")・[馬惠迪](../Page/馬惠迪.md "wikilink")・[馬琪](../Page/馬琪.md "wikilink")・[楊伯通](../Page/楊伯通.md "wikilink")・[尼厖古鑑](../Page/尼厖古鑑.md "wikilink")
34. 列傳第三十四 -
    [黄久約](../Page/黄久約.md "wikilink")・[李晏](../Page/李晏.md "wikilink")・[李愈](../Page/李愈.md "wikilink")・[王賁](../Page/王賁_\(金\).md "wikilink")・[許安仁](../Page/許安仁.md "wikilink")・[梁襄](../Page/梁襄.md "wikilink")・[路伯達](../Page/路伯達.md "wikilink")
35. 列傳第三十五 -
    [裴满亨](../Page/裴满亨.md "wikilink")・[斡勒忠](../Page/斡勒忠.md "wikilink")・[張大節](../Page/張大節.md "wikilink")・[張亨](../Page/張亨.md "wikilink")・[韓錫](../Page/韓錫.md "wikilink")・[鄧儼](../Page/鄧儼.md "wikilink")・[巨構](../Page/巨構.md "wikilink")・[賀揚庭](../Page/賀揚庭.md "wikilink")・[閻公貞](../Page/閻公貞.md "wikilink")・[焦旭](../Page/焦旭.md "wikilink")・[劉仲洙](../Page/劉仲洙.md "wikilink")・[李完](../Page/李完.md "wikilink")・[馬百禄](../Page/馬百禄.md "wikilink")・[楊伯元](../Page/楊伯元.md "wikilink")・劉璣・[康元弼](../Page/康元弼.md "wikilink")・[移剌益](../Page/移剌益.md "wikilink")
36. 列傳第三十六 -
    [完顏匡](../Page/完顏匡.md "wikilink")・[完顏綱](../Page/完顏綱.md "wikilink")
37. 列傳第三十七 -
    [徒单鎰](../Page/徒单鎰.md "wikilink")・[賈鉉](../Page/賈鉉.md "wikilink")・[孫鐸](../Page/孫鐸.md "wikilink")・[孫即康](../Page/孫即康.md "wikilink")・[李革](../Page/李革.md "wikilink")
38. 列傳第三十八 -
    [孟鑄](../Page/孟鑄.md "wikilink")・[宗端脩](../Page/宗端脩.md "wikilink")・[完顏閭山](../Page/完顏閭山.md "wikilink")・[路鐸](../Page/路鐸.md "wikilink")・[完顏伯嘉](../Page/完顏伯嘉.md "wikilink")・[朮虎筠寿](../Page/朮虎筠寿.md "wikilink")・[張煒](../Page/張煒.md "wikilink")・[高竑](../Page/高竑.md "wikilink")・[李復亨](../Page/李復亨.md "wikilink")
39. 列傳第三十九 -
    [完顏承暉](../Page/完顏承暉.md "wikilink")・[抹撚尽忠](../Page/抹撚尽忠.md "wikilink")・[僕散端](../Page/僕散端.md "wikilink")・[耿端義](../Page/耿端義.md "wikilink")・[李英](../Page/李英.md "wikilink")・[孛朮魯德裕](../Page/孛朮魯德裕.md "wikilink")・[烏古論慶寿](../Page/烏古論慶寿.md "wikilink")
40. 列傳第四十 -
    [僕散安貞](../Page/僕散安貞.md "wikilink")・[田琢](../Page/田琢.md "wikilink")・[完顏弼](../Page/完顏弼.md "wikilink")・[蒙古綱](../Page/蒙古綱.md "wikilink")・[必蘭阿魯帶](../Page/必蘭阿魯帶.md "wikilink")
41. 列傳第四十一 -
    [完顏仲元](../Page/完顏仲元.md "wikilink")・[完顏阿隣](../Page/完顏阿隣.md "wikilink")・[完顏霆](../Page/完顏霆.md "wikilink")・[烏古論長寿](../Page/烏古論長寿.md "wikilink")・[完顏佐](../Page/完顏佐.md "wikilink")・[石抹仲温](../Page/石抹仲温.md "wikilink")・[烏古論礼](../Page/烏古論礼.md "wikilink")・[浦察阿里](../Page/浦察阿里.md "wikilink")・[奧屯襄](../Page/奧屯襄.md "wikilink")・[完顏蒲剌都](../Page/完顏蒲剌都.md "wikilink")・[夾谷石里哥](../Page/夾谷石里哥.md "wikilink")・[朮甲臣嘉](../Page/朮甲臣嘉.md "wikilink")・[紇石烈桓端](../Page/紇石烈桓端.md "wikilink")・[完顏阿里不孫](../Page/完顏阿里不孫.md "wikilink")・[完顏鉄哥](../Page/完顏鉄哥.md "wikilink")・[納蘭胡魯剌](../Page/納蘭胡魯剌.md "wikilink")
42. 列傳第四十二 -
    [納坦謀嘉](../Page/納坦謀嘉.md "wikilink")・[鄒谷](../Page/鄒谷.md "wikilink")・[高霖](../Page/高霖.md "wikilink")・[孟奎](../Page/孟奎.md "wikilink")・[烏林荅與](../Page/烏林荅與.md "wikilink")・[郭俁](../Page/郭俁.md "wikilink")・[温迪罕達](../Page/温迪罕達.md "wikilink")・[王擴](../Page/王擴.md "wikilink")・[移剌福僧](../Page/移剌福僧.md "wikilink")・[奧屯忠孝](../Page/奧屯忠孝.md "wikilink")・[蒲察思忠](../Page/蒲察思忠.md "wikilink")・[紇石烈胡失門](../Page/紇石烈胡失門.md "wikilink")・[完顏宇](../Page/完顏宇.md "wikilink")・[斡勒合打](../Page/斡勒合打.md "wikilink")・[蒲察移剌都](../Page/蒲察移剌都.md "wikilink")
43. 列傳第四十三 -
    [程寀](../Page/程寀.md "wikilink")・[任熊祥](../Page/任熊祥.md "wikilink")・孔璠・[范拱](../Page/范拱.md "wikilink")・[張用直](../Page/張用直.md "wikilink")・[劉枢](../Page/劉枢.md "wikilink")・王翛・[楊伯雄](../Page/楊伯雄.md "wikilink")・[蕭貢](../Page/蕭貢.md "wikilink")・[温迪罕締達](../Page/温迪罕締達.md "wikilink")・[張翰](../Page/張翰_\(金國\).md "wikilink")・[任天寵](../Page/任天寵.md "wikilink")
44. 列傳第四十四 -
    [張暐](../Page/張暐.md "wikilink")・[賈益謙](../Page/賈益謙.md "wikilink")・[劉炳](../Page/劉炳.md "wikilink")・[朮虎高琪](../Page/朮虎高琪.md "wikilink")・[移剌塔不也](../Page/移剌塔不也.md "wikilink")
45. 列傳第四十五 -
    [高汝礪](../Page/高汝礪.md "wikilink")・[張行信](../Page/張行信.md "wikilink")
46. 列傳第四十六 -
    [胥鼎](../Page/胥鼎.md "wikilink")・[侯摯](../Page/侯摯.md "wikilink")・[把胡魯](../Page/把胡魯.md "wikilink")・[師安石](../Page/師安石.md "wikilink")
47. 列傳第四十七 -
    [完顏素蘭翼](../Page/完顏素蘭翼.md "wikilink")・[陳規](../Page/陳規.md "wikilink")・[許古](../Page/許古.md "wikilink")
48. 列傳第四十八 -
    [楊雲翼](../Page/楊雲翼.md "wikilink")・[趙秉文](../Page/趙秉文.md "wikilink")・[韓玉](../Page/韓玉.md "wikilink")・[馮璧](../Page/馮璧.md "wikilink")・[李献甫](../Page/李献甫.md "wikilink")・[雷淵](../Page/雷淵.md "wikilink")・[程震](../Page/程震.md "wikilink")
49. 列傳第四十九 -
    [古里甲石倫](../Page/古里甲石倫.md "wikilink")・[完顏訛可](../Page/完顏訛可.md "wikilink")・[撒合輦](../Page/撒合輦.md "wikilink")・[強伸](../Page/強伸.md "wikilink")・[烏林荅胡土](../Page/烏林荅胡土.md "wikilink")・[思烈](../Page/思烈.md "wikilink")・[紇石烈牙吾塔](../Page/紇石烈牙吾塔.md "wikilink")
50. 列傳第五十 -
    [完顏合達](../Page/完顏合達.md "wikilink")・[移剌蒲阿](../Page/移剌蒲阿.md "wikilink")
51. 列傳第五十一 -
    [完顏賽不](../Page/完顏賽不.md "wikilink")・[白撒](../Page/白撒.md "wikilink")・[赤盞合喜](../Page/赤盞合喜.md "wikilink")
52. 列傳第五十二 -
    [白華](../Page/白華.md "wikilink")・[斜卯愛實](../Page/斜卯愛實.md "wikilink")・[石抹世勣](../Page/石抹世勣.md "wikilink")
53. 列傳第五十三 -
    [完顏奴申](../Page/完顏奴申.md "wikilink")・[崔立](../Page/崔立.md "wikilink")・[李琦](../Page/李琦.md "wikilink")・[聶天驥](../Page/聶天驥.md "wikilink")・[赤盞尉忻](../Page/赤盞尉忻.md "wikilink")
54. 列傳第五十四 -
    [徒单兀典](../Page/徒单兀典.md "wikilink")・[石盞女魯欢](../Page/石盞女魯欢.md "wikilink")・[蒲察官奴](../Page/蒲察官奴.md "wikilink")・[承立](../Page/承立.md "wikilink")
55. 列傳第五十五 -
    [徒单益都](../Page/徒单益都.md "wikilink")・[粘哥荊山](../Page/粘哥荊山.md "wikilink")・[王賓](../Page/王賓.md "wikilink")・[国用安](../Page/国用安.md "wikilink")・[時青](../Page/時青.md "wikilink")
56. 列傳第五十六 -
    [苗道潤](../Page/苗道潤.md "wikilink")・[王福](../Page/王福.md "wikilink")・[移剌众家奴](../Page/移剌众家奴.md "wikilink")・[武仙](../Page/武仙.md "wikilink")・[張甫](../Page/張甫.md "wikilink")・[張進](../Page/張進.md "wikilink")・[靖安民](../Page/靖安民.md "wikilink")・[郭文振](../Page/郭文振.md "wikilink")・[胡天作](../Page/胡天作.md "wikilink")・[張開](../Page/張開.md "wikilink")・[燕寧](../Page/燕寧.md "wikilink")
57. 列傳第五十七 -
    [粘葛奴申](../Page/粘葛奴申.md "wikilink")・[完顏婁室三人](../Page/完顏婁室.md "wikilink")・[烏古論鎬](../Page/烏古論鎬.md "wikilink")・[張天綱](../Page/張天綱.md "wikilink")・[完顏仲德](../Page/完顏仲德.md "wikilink")
58. 列傳第五十八 世戚 -
    [石家奴](../Page/石家奴.md "wikilink")・[裴满達](../Page/裴满達.md "wikilink")・[徒单恭](../Page/徒单恭.md "wikilink")・[烏古論蒲魯虎](../Page/烏古論蒲魯虎.md "wikilink")・[唐括德温](../Page/唐括德温.md "wikilink")・[烏古論粘没曷](../Page/烏古論粘没曷.md "wikilink")・[蒲察阿虎迭](../Page/蒲察阿虎迭.md "wikilink")・[烏林荅暉](../Page/烏林荅暉.md "wikilink")・[蒲察鼎壽](../Page/蒲察鼎壽.md "wikilink")・[徒單思忠](../Page/徒單思忠.md "wikilink")・[徒單繹](../Page/徒單繹.md "wikilink")・[烏林荅復](../Page/烏林荅復.md "wikilink")・[烏古論元忠](../Page/烏古論元忠.md "wikilink")・[唐括貢](../Page/唐括貢.md "wikilink")・[烏林荅琳](../Page/烏林荅琳.md "wikilink")・[徒单公弼](../Page/徒单公弼.md "wikilink")・[徒单銘](../Page/徒单銘.md "wikilink")・[徒单四喜](../Page/徒单四喜.md "wikilink")
59. 列傳第五十九 忠義一 -
    [胡沙補](../Page/胡沙補.md "wikilink")・[特虎](../Page/特虎.md "wikilink")・[僕忽得](../Page/僕忽得.md "wikilink")・[粘割韓奴](../Page/粘割韓奴.md "wikilink")・[曹珪](../Page/曹珪.md "wikilink")・[溫蒂罕蒲睹](../Page/溫蒂罕蒲睹.md "wikilink")・[訛里也](../Page/訛里也.md "wikilink")・[納蘭綽赤](../Page/納蘭綽赤.md "wikilink")・[魏全](../Page/魏全.md "wikilink")・[鄯陽](../Page/鄯陽.md "wikilink")・[夾穀守中](../Page/夾穀守中.md "wikilink")・[石抹元毅](../Page/石抹元毅.md "wikilink")・[伯德梅和尚](../Page/伯德梅和尚.md "wikilink")・[烏古孫兀屯](../Page/烏古孫兀屯.md "wikilink")・[高守約](../Page/高守約.md "wikilink")・[和速嘉安禮](../Page/和速嘉安禮.md "wikilink")・[王維翰](../Page/王維翰.md "wikilink")・[移剌古與涅](../Page/移剌古與涅.md "wikilink")・[宋扆](../Page/宋扆.md "wikilink")・[烏古論榮祖](../Page/烏古論榮祖.md "wikilink")・[烏古論仲溫](../Page/烏古論仲溫.md "wikilink")・[九住](../Page/九住.md "wikilink")・[李演](../Page/李演.md "wikilink")・[劉德基](../Page/劉德基.md "wikilink")・[王毅](../Page/王毅.md "wikilink")・[王晦](../Page/王晦.md "wikilink")・[齊鷹揚](../Page/齊鷹揚.md "wikilink")・[朮甲法心](../Page/朮甲法心.md "wikilink")・[高錫](../Page/高錫.md "wikilink")
60. 列傳第六十 忠義二 -
    [吳僧哥](../Page/吳僧哥.md "wikilink")・[烏古論德升](../Page/烏古論德升.md "wikilink")・[張順](../Page/張順.md "wikilink")・[馬驤](../Page/馬驤.md "wikilink")・[伯德窊哥](../Page/伯德窊哥.md "wikilink")・[奧屯醜和尚](../Page/奧屯醜和尚.md "wikilink")・[從坦](../Page/從坦.md "wikilink")・[孛朮魯福壽](../Page/孛朮魯福壽.md "wikilink")・[吳邦傑](../Page/吳邦傑.md "wikilink")・[納合蒲剌都](../Page/納合蒲剌都.md "wikilink")・[女奚烈斡出](../Page/女奚烈斡出.md "wikilink")・[時茂先](../Page/時茂先.md "wikilink")・[溫蒂罕老兒](../Page/溫蒂罕老兒.md "wikilink")・[梁持勝](../Page/梁持勝.md "wikilink")・[賈邦獻](../Page/賈邦獻.md "wikilink")・[移剌阿里合](../Page/移剌阿里合.md "wikilink")・[完顏六斤](../Page/完顏六斤.md "wikilink")・[紇石烈鶴壽](../Page/紇石烈鶴壽.md "wikilink")・[蒲察婁室](../Page/蒲察婁室.md "wikilink")・[女奚烈資祿](../Page/女奚烈資祿.md "wikilink")・[趙益](../Page/趙益.md "wikilink")・[侯小叔](../Page/侯小叔.md "wikilink")・[王佐](../Page/王佐.md "wikilink")・[黃摑九住](../Page/黃摑九住.md "wikilink")・[烏林答乞住](../Page/烏林答乞住.md "wikilink")・[陀滿斜烈](../Page/陀滿斜烈.md "wikilink")・[尼龐古蒲魯虎](../Page/尼龐古蒲魯虎.md "wikilink")・[兀顏畏可](../Page/兀顏畏可.md "wikilink")・[兀顏訛出虎](../Page/兀顏訛出虎.md "wikilink")・[粘割貞](../Page/粘割貞.md "wikilink")
61. 列傳第六十一 忠義三 -
    [徒單航](../Page/徒單航.md "wikilink")・[完顏陳和尚](../Page/完顏陳和尚.md "wikilink")・[楊沃衍](../Page/楊沃衍.md "wikilink")・[烏古論黑漢](../Page/烏古論黑漢.md "wikilink")・[陀滿胡土門](../Page/陀滿胡土門.md "wikilink")・[姬汝作](../Page/姬汝作.md "wikilink")・[愛申](../Page/愛申.md "wikilink")・[馬肩龍](../Page/馬肩龍.md "wikilink")・[禹顯](../Page/禹顯.md "wikilink")・[張邦憲](../Page/張邦憲.md "wikilink")・[劉全](../Page/劉全.md "wikilink")
62. 列傳第六十二 忠義四 -
    [馬慶祥](../Page/馬慶祥.md "wikilink")・[商衡](../Page/商衡.md "wikilink")・[朮甲脫魯灰](../Page/朮甲脫魯灰.md "wikilink")・[楊達夫](../Page/楊達夫.md "wikilink")・[馮延登](../Page/馮延登.md "wikilink")・[烏古孫仲端](../Page/烏古孫仲端.md "wikilink")・[烏古孫奴申](../Page/烏古孫奴申.md "wikilink")・[蒲察琦](../Page/蒲察琦.md "wikilink")・[蔡八兒](../Page/蔡八兒.md "wikilink")・[溫敦昌孫](../Page/溫敦昌孫.md "wikilink")・[完顏絳山](../Page/完顏絳山.md "wikilink")・[畢資倫](../Page/畢資倫.md "wikilink")・[郭蝦蟆](../Page/郭蝦蟆.md "wikilink")
63. 列傳第六十三 文艺上 -
    [韓昉](../Page/韓昉.md "wikilink")・[蔡松年](../Page/蔡松年.md "wikilink")・[吴激](../Page/吴激.md "wikilink")・[馬定国](../Page/馬定国.md "wikilink")・[任詢](../Page/任詢.md "wikilink")・[趙可](../Page/趙可.md "wikilink")・[郭長倩](../Page/郭長倩.md "wikilink")・[蕭永祺](../Page/蕭永祺.md "wikilink")・[胡礪](../Page/胡礪.md "wikilink")・[王競](../Page/王競.md "wikilink")・[楊伯仁](../Page/楊伯仁.md "wikilink")・鄭子聃・[党怀英](../Page/党怀英.md "wikilink")
64. 列傳第六十四 文艺下 -
    [趙渢](../Page/趙渢.md "wikilink")・[周昂](../Page/周昂.md "wikilink")・[王庭筠](../Page/王庭筠.md "wikilink")・[劉昂](../Page/劉昂.md "wikilink")・[李經](../Page/李經.md "wikilink")・[劉从益](../Page/劉从益.md "wikilink")・[呂中孚](../Page/呂中孚.md "wikilink")・[張建](../Page/張建.md "wikilink")・[李純甫](../Page/李純甫.md "wikilink")・[王郁](../Page/王郁.md "wikilink")・[宋九嘉](../Page/宋九嘉.md "wikilink")・[龐鑄](../Page/龐鑄.md "wikilink")・[李献能](../Page/李献能.md "wikilink")・[王若虚](../Page/王若虚.md "wikilink")・[王元節](../Page/王元節.md "wikilink")・[麻九疇](../Page/麻九疇.md "wikilink")・[李汾](../Page/李汾.md "wikilink")・[元德明](../Page/元德明.md "wikilink")
65. 列傳第六十五 孝友 -
    [溫蒂罕斡魯補](../Page/溫蒂罕斡魯補.md "wikilink")・[陳顏](../Page/陳顏.md "wikilink")・[劉瑜](../Page/劉瑜.md "wikilink")・[孟興](../Page/孟興.md "wikilink")・[王震](../Page/王震.md "wikilink")・[劉政](../Page/劉政.md "wikilink")
    隠逸 -
    [褚承亮](../Page/褚承亮.md "wikilink")・[王去非](../Page/王去非.md "wikilink")・[趙質](../Page/趙質.md "wikilink")・[杜時升](../Page/杜時升.md "wikilink")・[郝天挺](../Page/郝天挺.md "wikilink")・[薛繼元](../Page/薛繼元.md "wikilink")・[高仲振](../Page/高仲振.md "wikilink")・[張潛](../Page/張潛.md "wikilink")・[王汝梅](../Page/王汝梅.md "wikilink")・[宋可](../Page/宋可.md "wikilink")・[辛願](../Page/辛願.md "wikilink")・[王予可](../Page/王予可.md "wikilink")
66. 列傳第六十六 循吏 -
    [盧克忠](../Page/盧克忠.md "wikilink")・[牛德昌](../Page/牛德昌.md "wikilink")・[范承吉](../Page/范承吉.md "wikilink")・[王政](../Page/王政.md "wikilink")・[張奕](../Page/張奕.md "wikilink")・[李瞻](../Page/李瞻.md "wikilink")・[劉敏行](../Page/劉敏行.md "wikilink")・[傅慎微](../Page/傅慎微.md "wikilink")・[劉煥](../Page/劉煥.md "wikilink")・[高昌福](../Page/高昌福.md "wikilink")・[孫德淵](../Page/孫德淵.md "wikilink")・[趙鑒](../Page/趙鑒.md "wikilink")・[蒲察鄭留](../Page/蒲察鄭留.md "wikilink")・[女奚烈守愚](../Page/女奚烈守愚.md "wikilink")・[石抹元](../Page/石抹元.md "wikilink")・[張彀](../Page/張彀.md "wikilink")・[趙重福](../Page/趙重福.md "wikilink")・[武都](../Page/武都.md "wikilink")・[紇石烈德](../Page/紇石烈德.md "wikilink")・[張特立](../Page/張特立.md "wikilink")・[王浩](../Page/王浩.md "wikilink")
67. 列傳第六十七 酷吏 佞幸 -
    [高閭山](../Page/高閭山.md "wikilink")・[蒲察合住](../Page/蒲察合住.md "wikilink")・[蕭肄](../Page/蕭肄.md "wikilink")・[張仲軻](../Page/張仲軻.md "wikilink")・[李通](../Page/李通_\(金\).md "wikilink")・[馬欽](../Page/馬欽.md "wikilink")・[高怀貞](../Page/高怀貞.md "wikilink")・[蕭裕](../Page/蕭裕.md "wikilink")・[胥持国](../Page/胥持国.md "wikilink")
68. 列傳第六十八 列女 -
    [阿鄰妻](../Page/阿鄰妻.md "wikilink")・[李寶信妻](../Page/李寶信妻.md "wikilink")・[韓慶民妻](../Page/韓慶民妻.md "wikilink")・[雷婦師氏](../Page/雷婦師氏.md "wikilink")・[康住住](../Page/康住住.md "wikilink")・[李文妻](../Page/李文妻.md "wikilink")・[李英妻](../Page/李英妻.md "wikilink")・[相琪妻](../Page/相琪妻.md "wikilink")・[阿魯真](../Page/阿魯真.md "wikilink")・[撒合輦妻](../Page/撒合輦妻.md "wikilink")・[許古妻](../Page/許古妻.md "wikilink")・[馮妙真](../Page/馮妙真.md "wikilink")・[蒲察氏](../Page/蒲察氏.md "wikilink")・[烏古論氏](../Page/烏古論氏.md "wikilink")・[素蘭妻](../Page/素蘭妻.md "wikilink")・[忙哥妻](../Page/忙哥妻.md "wikilink")・[尹氏](../Page/尹氏.md "wikilink")・[白氏](../Page/白氏.md "wikilink")・[聶孝女](../Page/聶孝女.md "wikilink")・[仲德妻](../Page/仲德妻.md "wikilink")・[寶符李氏](../Page/寶符李氏.md "wikilink")・[張鳳奴](../Page/張鳳奴.md "wikilink")
69. 列傳第六十九 宦者 方伎 -
    [梁珫](../Page/梁珫.md "wikilink")・[宋珪](../Page/宋珪.md "wikilink")・[劉完素](../Page/劉完素.md "wikilink")・[張从正](../Page/張从正.md "wikilink")・[李慶嗣](../Page/李慶嗣.md "wikilink")・[紀天錫](../Page/紀天錫.md "wikilink")・[張元素](../Page/張元素.md "wikilink")・[馬貴中](../Page/馬貴中.md "wikilink")・[武禎](../Page/武禎.md "wikilink")・[李懋](../Page/李懋.md "wikilink")・[胡德新](../Page/胡德新.md "wikilink")
70. 列傳第七十 逆臣 -
    [完顏秉德](../Page/完顏秉德.md "wikilink")・[唐括辯](../Page/唐括辯.md "wikilink")・[完顏言](../Page/完颜乌带.md "wikilink")・[大興国](../Page/大興国.md "wikilink")・[徒单阿里出虎](../Page/徒单阿里出虎.md "wikilink")・[僕散師恭](../Page/僕散師恭.md "wikilink")・[徒单貞](../Page/徒单貞.md "wikilink")・[李老僧](../Page/李老僧.md "wikilink")・[完顏元宜](../Page/完顏元宜.md "wikilink")・[紇石烈執中](../Page/紇石烈執中.md "wikilink")
71. 列傳第七十一 叛臣 -
    [張覺](../Page/張覺.md "wikilink")・[耶律余睹](../Page/耶律余睹.md "wikilink")・[移剌窩幹](../Page/移剌窩幹.md "wikilink")
72. 列傳第七十二 外国上 - [西夏](../Page/西夏.md "wikilink")
73. 列傳第七十三 外国下 - [高麗](../Page/高麗.md "wikilink")

### 金国語解

### 附録

## 補編

後人據相關史料補作志表，補志方面，有民國[楊家駱](../Page/楊家駱.md "wikilink")《[新補金史藝文志](../Page/新補金史藝文志.md "wikilink")》12卷；補表方面，有清代[萬斯同](../Page/萬斯同.md "wikilink")《金諸帝統系圖》1卷、《金將相大臣年表》1卷、《金衍慶宮功臣錄》1卷，[杭世駿](../Page/杭世駿.md "wikilink")《金史補》不分卷，[黃大華](../Page/黃大華.md "wikilink")《金宰輔年表》1卷，民國[吳廷燮](../Page/吳廷燮.md "wikilink")《金方鎭年表》1卷，近代[陳述](../Page/陳述.md "wikilink")《金史氏族表》6卷、《女真漢姓考》2卷、《金賜姓表》2卷、《金史同姓名表》1卷、《金史異名表》1卷；考証注釋方面，有清代[丁謙](../Page/丁謙.md "wikilink")《金史外國傳地理考證》1卷，[盧文弨](../Page/盧文弨.md "wikilink")《金史太宗諸子傳補脱》1卷、《金史禮志補脫》1卷，[施國祁](../Page/施國祁.md "wikilink")《[金史詳校](../Page/金史詳校.md "wikilink")》10卷、《[金源劄記](../Page/金源劄記.md "wikilink")》2卷。

## 參見

  - [金史人物列表](../Page/金史人物列表.md "wikilink")
  - [金人漢名](../Page/金人漢名.md "wikilink")

## 外部連結

  - [《金史》全文(繁体)](http://www.sidneyluo.net/a/a22/a22.htm)
  - [《金史》全文(简体)](http://www.guoxue.com/shibu/24shi/jingshi/jsml.htm)
  - 陳學霖：〈[《壬辰雜編》與《金史》史源](http://140.112.142.79/publish/abstract.asp?paper=%A1m%A4%D0%A8%B0%C2%F8%BDs%A1n%BBP%A1m%AA%F7%A5v%A1n%A5v%B7%BD)〉。

[Category:史部正史類](../Category/史部正史類.md "wikilink")
[Category:纪传体](../Category/纪传体.md "wikilink")
[Category:二十四史](../Category/二十四史.md "wikilink")
[Category:1340年代书籍](../Category/1340年代书籍.md "wikilink")
[Category:金朝史书](../Category/金朝史书.md "wikilink")
[Category:元朝官修典籍](../Category/元朝官修典籍.md "wikilink")