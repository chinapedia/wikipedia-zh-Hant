**金喜愛**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")。1996年9月與電腦實業家[李昌鎮結婚](../Page/李昌鎮.md "wikilink")，男方畢業於[首爾大學機械工學系](../Page/首爾大學.md "wikilink")。1998年6月至1998年12月擔任[水原科學大學放送演藝學系兼任教授](../Page/水原科學大學.md "wikilink")。

## 演出作品

### 電視劇

  - 1986年：[KBS](../Page/韓國放送公社.md "wikilink")《[女心](../Page/女心.md "wikilink")》
  - 1987年：[KBS](../Page/韓國放送公社.md "wikilink") 日日劇《媽媽》
  - 1987年：[KBS](../Page/韓國放送公社.md "wikilink") 新年特輯電視劇
  - 1987年：KBS《[愛情的條件](../Page/愛情的條件_\(1987年電視劇\).md "wikilink")》飾演 媛美
  - 1988年：MBC 週末連續劇《[明天忘記你](../Page/明天忘記你.md "wikilink")》飾演 徐茵愛
  - 1990年：MBC 水木連續劇《[您敬酒](../Page/您敬酒.md "wikilink")》飾演 全愛玉
  - 1990年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[我愛你](../Page/我愛你_\(電視劇\).md "wikilink")》
  - 1990年：MBC《[朝鮮王朝五百年](../Page/朝鮮王朝五百年.md "wikilink")－[大院君](../Page/大院君_\(電視劇\).md "wikilink")》飾演
    [明成皇后](../Page/明成皇后.md "wikilink")
  - 1990年：MBC《[冬季外星人](../Page/冬季外星人.md "wikilink")》
  - 1991年：MBC《[離別的開始](../Page/離別的開始.md "wikilink")》飾演 鄭海靜
  - 1991年：MBC《[越過那座山](../Page/越過那座山.md "wikilink")》飾演 姜明愛
  - 1992年：MBC《[憤怒的王國](../Page/憤怒的王國.md "wikilink")》飾演 在京
  - 1992年：MBC《[兒子與女兒](../Page/兒子與女兒.md "wikilink")》飾演 李後南
  - 1993年：MBC《[暴風的季節](../Page/暴風的季節.md "wikilink")》飾演 李洪珠
  - 1994年：MBC《[卡雷司基](../Page/卡雷司基.md "wikilink")》
  - 1995年：MBC《[愛與結婚](../Page/愛與結婚.md "wikilink")》飾演 尹秀彬
  - 1995年：MBC《[戀愛的基礎](../Page/戀愛的基礎.md "wikilink")》飾演 婷希
  - 1999年：MBC《[唯一的你](../Page/唯一的你.md "wikilink")》飾演
  - 2003年：KBS《[妻子](../Page/妻子_\(電視劇\).md "wikilink")》飾演 金娜英
  - 2003年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[完整的愛](../Page/完整的愛.md "wikilink")》飾演
    夏英愛
  - 2004年：KBS《[說不出的愛](../Page/致父親母親.md "wikilink")》飾演 安誠實
  - 2006年：SBS《[雪花](../Page/雪花_\(韓國電視劇\).md "wikilink")》飾演 李康愛
  - 2007年：SBS《[我男人的女人](../Page/我男人的女人.md "wikilink")》飾演 李華英
  - 2011年：SBS《[Midas](../Page/Midas_\(電視劇\).md "wikilink")》飾演 劉仁惠
  - 2012年：[JTBC](../Page/JTBC.md "wikilink")《[妻子的資格](../Page/妻子的資格.md "wikilink")》飾演
    尹書萊
  - 2014年：JTBC《[密會](../Page/密會.md "wikilink")》飾演 吳慧媛
  - 2015年：SBS《[Mrs. Cop](../Page/Mrs._Cop.md "wikilink")》飾演 崔英珍
  - 2016年：SBS《[倒數第二次戀愛](../Page/倒數第二次戀愛_\(韓國電視劇\).md "wikilink")》飾演 姜敏珠

### 電影

  - 1984年：《[My Love Jjang-gu](../Page/My_Love_Jjang-gu.md "wikilink")》
  - 1984年：《[不意回想](../Page/不意回想.md "wikilink")》
  - 1987年：《[英雄返回](../Page/英雄返回.md "wikilink")》
  - 1993年：《[The 101st
    Proposition](../Page/The_101st_Proposition.md "wikilink")》
  - 2014年：《[優雅的謊言](../Page/優雅的謊言.md "wikilink")》
  - 2014年：《[如此美好](../Page/如此美好.md "wikilink")》
  - 2018年：《[消屍的夜晚](../Page/消屍的夜晚.md "wikilink")》
  - 2018年：《[她的故事](../Page/她的故事_\(韓國電影\).md "wikilink")》
  - 2019年：《[滿月](../Page/滿月_\(電影\).md "wikilink")》

## 綜藝節目

  - 2013年：[tvN](../Page/TVN.md "wikilink")《[花樣姐姐](../Page/花樣姐姐.md "wikilink")》
  - 2014年：[SBS](../Page/SBS株式會社.md "wikilink")《[Healing
    Camp](../Page/Healing_Camp.md "wikilink")》
  - 2016年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[無限挑戰](../Page/無限挑戰.md "wikilink")》
  - 2016年：[SBS](../Page/SBS株式會社.md "wikilink")《[Running
    Man](../Page/Running_Man.md "wikilink")》

## 獲獎

  - 1986年：[KBS演技大賞](../Page/KBS演技大賞.md "wikilink")－新人獎
  - 1987年：第23屆[百想藝術大賞](../Page/百想藝術大賞.md "wikilink")－新人獎
  - 1990年：[MBC演技大賞](../Page/MBC演技大賞.md "wikilink")－最優秀獎
  - 1991年：MBC演技大賞－大賞（越過那座山）
  - 1993年：MBC演技大賞－大賞（兒子與女兒）
  - 1993年：電視雜誌年度明星獎
  - 1993年：第29屆[百想藝術大賞](../Page/百想藝術大賞.md "wikilink")－TV部門
    大獎、最優秀女演員獎（兒子與女兒）
  - 1994年：第6屆韓國PD大賞
  - 1996年：第8屆韓國PD大賞
  - 2003年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－十大明星獎、SBSi獎（妻子）
  - 2003年：第39屆百想藝術大賞－TV部門 最優秀女演員獎（妻子）
  - 2003年：第16届[画梅奖](../Page/韩国画梅奖.md "wikilink") 最佳女演员（完整的愛）
  - 2004年：第16屆韓國PD大賞
  - 2004年：第40屆百想藝術大賞－TV部門 大獎（完整的愛）
  - 2007年：SBS演技大賞－大賞、十大明星獎（我男人的女人）
  - 2007年：第1屆[韓國電視劇節](../Page/韓國電視劇節.md "wikilink")－演技大獎（我男人的女人）
  - 2008年：第1屆[Style Icon
    Awards](../Page/Style_Icon_Awards.md "wikilink")(SIA韓國時尚偶像)－電視本賞
  - 2013年：[第49屆百想藝術大賞](../Page/第49屆百想藝術大賞.md "wikilink")－TV部門
    女子最優秀演技獎（妻子的資格）\[1\]
  - 2014年：[第50屆百想藝術大賞](../Page/第50屆百想藝術大賞.md "wikilink")－LF時尚之星獎
  - 2014年：第9屆[首爾國際電視節](../Page/首爾國際電視節.md "wikilink")－最佳女演員獎（密會）
  - 2014年：第7屆[Style Icon
    Awards](../Page/Style_Icon_Awards.md "wikilink")(SIA韓國時尚偶像)－時尚偶像賞
  - 2014年：第3屆[大田電視劇節](../Page/大田電視劇節.md "wikilink")－中篇電視劇 女子最優秀演技賞（密會）

## 參考來源

## 外部連結

  - [NAVER](http://people.search.naver.com/search.naver?where=nexearch&query=%EA%B9%80%ED%9D%AC%EC%95%A0&sm=tab_txc&ie=utf8&key=PeopleService&os=94192)

[K](../Category/韓國電視演員.md "wikilink")
[K](../Category/韓國電影演員.md "wikilink")
[K](../Category/韓國中央大學校友.md "wikilink")
[K](../Category/濟州特別自治道出身人物.md "wikilink")
[K](../Category/金姓.md "wikilink")

1.