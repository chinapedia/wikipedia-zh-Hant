**金士頓科技公司**（）為華人[杜紀川](../Page/杜紀川.md "wikilink")、[孫大衛所創辦的一家](../Page/孫大衛.md "wikilink")[美國](../Page/美國.md "wikilink")[記憶體產品生產商](../Page/記憶體.md "wikilink")，總部及生產設於[加州](../Page/加州.md "wikilink")[芳泉谷](../Page/芳泉谷_\(加利福尼亞州\).md "wikilink")，在美國、[英國](../Page/英國.md "wikilink")、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[台灣與](../Page/台灣.md "wikilink")[中國大陸擁有後勤工廠](../Page/中國大陸.md "wikilink")\[1\]。

金士頓是現今全球最大的[DRAM記憶體模組獨立生產商及供應商](../Page/DRAM.md "wikilink")\[2\]，2017年DRAM全球市場市占率超過72%\[3\]。台灣記憶體市場上的主力記憶體生產廠商如[力晶](../Page/力晶.md "wikilink")、茂矽、茂德、[華邦電](../Page/華邦.md "wikilink")、[南亞科等都是金士頓的供應商](../Page/南亞科技.md "wikilink")，此外，金士頓還在[快閃記憶體控制IC領域方面](../Page/快閃記憶體.md "wikilink")，陸續投資群聯（8299-TW）、鑫創（3259-TW）、點序（6485-TW）與擎泰等公司，在記憶體封測廠方面，也入股[力成](../Page/力成.md "wikilink")（6239-TW）及華泰（2329-TW）\[4\]；而記憶體模組廠則投資品安（8088-TW）\[5\]。金士頓可能是第二大的[快閃記憶體生產商](../Page/快閃記憶體.md "wikilink")，根據研究機構[Gartner評估](../Page/Gartner.md "wikilink")，金士頓為全球第二大隨身碟供應商。

2004年金士頓科技擁有2,400名雇員，2005年營收首次突破30億[美元](../Page/美元.md "wikilink")。以性能為主的DRAM產品線名為HyperX，擁有終生保固（質保）。

金士頓曾贊助[2004年中華民國總統選舉重新驗票的費用](../Page/2004年中華民國總統選舉.md "wikilink")，總數為1,695萬元[新台幣](../Page/新台幣.md "wikilink")\[6\]。

2014年，金士頓在[富比士雜誌](../Page/富比士雜誌.md "wikilink")“全美500大私人企業”中名列\#69；在科技硬體及設備公司的分類中名列\#2。

## 歷史

[Kingston_Technology_Far_East_20120910_night.jpg](https://zh.wikipedia.org/wiki/File:Kingston_Technology_Far_East_20120910_night.jpg "fig:Kingston_Technology_Far_East_20120910_night.jpg")

  - 1987年10月17日，公司以出產單一產品創立（創辦人：杜紀川、孫大衛）。
  - 1995年於[德國](../Page/德國.md "wikilink")[慕尼黑設立首個](../Page/慕尼黑.md "wikilink")[歐洲分部](../Page/歐洲.md "wikilink")。

現時金士頓的歐洲總部位於[英國](../Page/英國.md "wikilink")[森伯里](../Page/森伯里.md "wikilink")，亞洲總部位於[台灣](../Page/台灣.md "wikilink")[新竹市](../Page/新竹市.md "wikilink")\[7\]\[8\]，在美国加州、台灣、[馬來西亞](../Page/馬來西亞.md "wikilink")、[中国](../Page/中華人民共和國.md "wikilink")[上海與](../Page/上海.md "wikilink")[深圳設有生產工廠](../Page/深圳.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:光華數位新天地_金士頓科技授權換修中心_20080723.jpg "fig:缩略图")

## 產品用途

[Kingston_SDcard_1GB.jpg](https://zh.wikipedia.org/wiki/File:Kingston_SDcard_1GB.jpg "fig:Kingston_SDcard_1GB.jpg")，容量为1[GB](../Page/Gigabyte.md "wikilink")\]\]

  - [快閃記憶體](../Page/快閃記憶體.md "wikilink") -
    [SD卡](../Page/Secure_Digital_card.md "wikilink")、[CF卡](../Page/CompactFlash.md "wikilink")、[闪存盘與各種其他形態的產品](../Page/闪存盘.md "wikilink")
  - [個人電腦](../Page/個人電腦.md "wikilink") -
    特定系統記憶體升級與系統製造商及OEM廠商使用的"ValueRAM"系列[DDR](../Page/DDR.md "wikilink")
    [SDRAM](../Page/SDRAM.md "wikilink")
  - [伺服器](../Page/伺服器.md "wikilink") -
    品牌（如[IBM及](../Page/IBM.md "wikilink")[惠普等](../Page/惠普公司.md "wikilink")）與無品牌伺服器的記憶體
  - 電子遊戲玩家 - Hyper X DDR SDRAM
  - [印表機](../Page/印表機.md "wikilink") -
    惠普LaserJet內置的記憶體、[Lexmark印表機內部記憶體等](../Page/Lexmark.md "wikilink")
  - [數位音頻播放器](../Page/數位音頻播放器.md "wikilink") -
    [MiniSD卡](../Page/MiniSD.md "wikilink")、[MicroSD卡](../Page/MicroSD.md "wikilink")、[MultiMedia卡](../Page/MultiMedia卡.md "wikilink")
  - [手提電話](../Page/手提電話.md "wikilink") -
    [MiniSD卡](../Page/MiniSD.md "wikilink")、[MicroSD卡](../Page/MicroSD.md "wikilink")、[MultiMedia卡](../Page/MultiMedia卡.md "wikilink")
  - [MP3或個人娛樂裝置](../Page/MP3.md "wikilink") - K-PEX 100

## 参考資料

## 外部連結

  - [金士頓網站](http://www.kingston.com)
  - [ValueRam網站](http://www.valueram.com)
  - [金士頓線上商店](http://www.shop.kingston.com)
  - [2013年度廣告 A Memory to Remember 記憶月台
    (完整版)](https://web.archive.org/web/20140202103718/http://www.tracevideo.com/%E3%80%90kingston%E9%87%91%E5%A3%AB%E9%A0%93%E3%80%912013%E5%B9%B4%E5%BA%A6%E5%BB%A3%E5%91%8A-a-memory-to-remember-%E8%A8%98%E6%86%B6%E6%9C%88%E5%8F%B0-%E5%AE%8C%E6%95%B4%E7%89%88-video_f029f70c3.html)

[Category:美國電子公司](../Category/美國電子公司.md "wikilink")
[Category:跨國公司](../Category/跨國公司.md "wikilink")
[Category:電腦儲存媒體公司](../Category/電腦儲存媒體公司.md "wikilink")

1.  洪友芳，[金士頓小檔案](http://news.ltn.com.tw/news/business/paper/885736)，自由時報，2015-06-02
2.  楊伶雯，[金士頓穩居去年記憶體模組廠龍頭
    威剛搶下台灣第一](https://news.cnyes.com/news/id/3572609)，鉅亨網，2016-10-12
3.  [全球内存模组厂营收最新排名出炉！金士顿稳居第一！](http://www.esmchina.com/news/article/201708051431)
4.  陳昱光，[金士頓每股15元
    收購華泰5.44萬張](http://www.chinatimes.com/newspapers/20160830000195-260206)，中時電子報，2016-08-30
5.  范中興，[金士頓取得品安3席董事](http://www.appledaily.com.tw/appledaily/article/finance/20120921/34523230/)，蘋果日報，2012-09-21
6.  [當選無效之訴 最慢一年結案](http://www.epochtimes.com/b5/6/12/11/n1552526.htm)
7.  [快閃記憶卡商機
    封測廠搶進](http://www.libertytimes.com.tw/2007/new/jun/29/today-e3.htm)

8.  [金士頓擴大外包
    華泰樂翻](http://tw.nextmedia.com/applenews/article/art_id/32129654/IssueID/20091201)