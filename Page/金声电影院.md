[Canton_Grand_Theatre.jpg](https://zh.wikipedia.org/wiki/File:Canton_Grand_Theatre.jpg "fig:Canton_Grand_Theatre.jpg")
[金聲戲院.JPG](https://zh.wikipedia.org/wiki/File:金聲戲院.JPG "fig:金聲戲院.JPG")
**金声电影院**（，原名**金声戏院**）是廣州的一間老牌戲院，位於[广州市](../Page/广州市.md "wikilink")[西关](../Page/西关.md "wikilink")[恩宁路](../Page/恩宁路.md "wikilink")（与[宝华路交界处](../Page/宝华路.md "wikilink")），是一栋六层高的骑楼建筑，外墙为灰褐色。在[恩寧路舊城改造中结业](../Page/恩宁路.md "wikilink")，拆卸半毀後形似[大三巴](../Page/大三巴牌坊.md "wikilink")，具一定知名度。

## 历史

金声由1932年曾侨居[美国的](../Page/美国.md "wikilink")[台山](../Page/台山.md "wikilink")[商人](../Page/商人.md "wikilink")[朱荫桥](../Page/朱荫桥.md "wikilink")、[朱家藩创建](../Page/朱家藩.md "wikilink")。戲院由[林克明設計](../Page/林克明.md "wikilink")，為楼高六层的[骑楼](../Page/骑楼.md "wikilink")，建筑面积为2500多[平方米](../Page/平方米.md "wikilink")，1934年2月14日开业，名**金声戏院**，并取英文名GRANDTHEATER，意即**大戏院**\[1\]**，**以放映美国[电影为主](../Page/电影.md "wikilink")。

中共建政後，1956年被强制[公私合营](../Page/公私合营.md "wikilink")。[文化大革命期间被改名作](../Page/文化大革命.md "wikilink")**东方电影院**。1970年代末戏院达到辉煌期，并于1992年、1995年分别投资700多万元[人民币进行改造装修](../Page/人民币.md "wikilink")。

后来业绩开始下滑，逐渐沦为广州的二线影院。为挽救颓势曾兼营录像带放映、[卡拉OK](../Page/卡拉OK.md "wikilink")、[咖啡厅](../Page/咖啡厅.md "wikilink")、[电子游戏机厅及](../Page/电子游戏机.md "wikilink")[商场等](../Page/商场.md "wikilink")。2007年11月29日，因“消防未达标”无钱整改而结业。

2010年3月，金声的主楼被清拆\[2\]，原戲院放映機亦不知所終，现仅存写着“金声电影院”金漆大字的牌楼，情形类似[澳门](../Page/澳门.md "wikilink")[大三巴牌坊](../Page/大三巴牌坊.md "wikilink")。地下一楼现变成杂货铺。拆遷前後，[恩寧路關注人士曾在其空地播映剪輯片](../Page/恩宁路.md "wikilink")「金聲淚」，以悼念本土文化歷史的消解。

## 参见

  - [恩宁路](../Page/恩宁路.md "wikilink")
  - [廣州被清拆史蹟列表](../Page/廣州被清拆史蹟列表.md "wikilink")

## 外部链接

  - [金声影院"消防未达标"无钱整改昨突然结业](https://web.archive.org/web/20160304120830/http://news.dayoo.com/guangzhou/news/2007-11/30/content_3159252_2.htm)
    - 信息时报

[Category:廣州保育運動](../Category/廣州保育運動.md "wikilink")
[Category:廣州戲院](../Category/廣州戲院.md "wikilink")
[Category:1932年廣州建立](../Category/1932年廣州建立.md "wikilink")
[Category:西關](../Category/西關.md "wikilink")
[Category:强制拆迁](../Category/强制拆迁.md "wikilink")
[Category:广州地标](../Category/广州地标.md "wikilink")

1.
2.