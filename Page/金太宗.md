**金太宗完顏晟**（），[金朝第二位](../Page/金朝.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")（1123年9月27日—1135年2月9日在位）。女真名**吳乞買**，[金太祖之弟](../Page/金太祖.md "wikilink")，身材魁梧，力大無比，能親手搏熊刺虎。在位12年，终年61岁。先后滅[遼朝及](../Page/遼朝.md "wikilink")[北宋](../Page/北宋.md "wikilink")。

## 生平

完颜吴乞买出生于1075年11月25日。[天會三年二月二十日](../Page/天会_\(金朝\).md "wikilink")（1125年3月26日），[辽天祚帝在](../Page/辽天祚帝.md "wikilink")[应州被金朝将领](../Page/应州.md "wikilink")[完颜娄室等所俘](../Page/完颜娄室.md "wikilink")，八月被解送[金上京](../Page/金上京.md "wikilink")，被降为海滨王，[辽朝灭亡](../Page/辽朝.md "wikilink")。

天會三年（1125年）十月，发动[宋金战争](../Page/宋金战争.md "wikilink")，令諳班[勃極烈](../Page/勃極烈.md "wikilink")[完颜斜也為都元帥](../Page/完颜斜也.md "wikilink")，統領金軍，兵分東、西兩路，逼進[北宋首都](../Page/北宋.md "wikilink")[汴京](../Page/汴京.md "wikilink")，由於[李綱頑強抵抗](../Page/李綱.md "wikilink")，金兵一時不能得逞，雙方訂「[城下之盟](../Page/城下之盟.md "wikilink")」。天會四年（1126年）八月，經過半年的休整，金太宗再次命宗望、宗翰兩路軍大舉[南侵](../Page/靖康之变.md "wikilink")，[汴京再度被包圍](../Page/汴京.md "wikilink")，破郭京「六甲法」，汴京城陷。天會五年二月初六（1127年3月20日），金太宗下詔廢徽、欽二帝，貶為庶人，俘虏二帝北上，并携带掠夺来的大量财宝和皇室大臣宫女等15000人，[北宋滅亡](../Page/北宋.md "wikilink")。天會六年（1128年）八月二十四日，吳乞買封[宋徽宗為昏德公](../Page/宋徽宗.md "wikilink")，[宋欽宗為重昏侯](../Page/宋欽宗.md "wikilink")，移遷五國城（今[黑龍江省](../Page/黑龍江.md "wikilink")[依蘭縣城北舊古城](../Page/依蘭縣.md "wikilink")）。

他在位时期创建了各种典章制度，奠定金代经国规模，晚年改变兄终弟及的旧制，立太祖孙[完颜亶](../Page/完颜亶.md "wikilink")（金熙宗）为继承人。

天會十三年正月二十五日（1135年2月9日），太宗病死於明德宮，終年六十一歲。遺體葬和陵。其后代全被[海陵王完颜亮所杀](../Page/海陵王.md "wikilink")，海陵王遷都後，改葬於大房山，稱**[金恭陵](../Page/金恭陵.md "wikilink")**。

他死後，於天會十三年三月七日上[諡號](../Page/諡號.md "wikilink")**文烈皇帝**，[廟號](../Page/廟號.md "wikilink")**太宗**。[皇統五年閏十一月增諡体元应运世德昭功哲惠仁圣](../Page/皇統.md "wikilink")**文烈皇帝**。

## 家庭

### 妻妾

  - [欽仁皇后唐括氏](../Page/欽仁皇后.md "wikilink")

### 子女

  - 長子宋国王[完顏宗磐](../Page/完顏宗磐.md "wikilink")，名 蒲魯虎
  - 豳王[完顏宗固](../Page/完顏宗固.md "wikilink")，名 胡魯
  - 代王[完顏宗雅](../Page/完顏宗雅.md "wikilink")，名 斛魯補
  - 徐王[完顏宗順](../Page/完顏宗順.md "wikilink")，名 阿魯帶
  - 四子\[1\]虞王[完颜宗伟](../Page/完颜宗伟.md "wikilink")，名 阿魯補
  - 三子\[2\]滕王[完顏宗英](../Page/完顏宗英.md "wikilink")，名 斛沙虎
  - 薛王[完顏宗懿](../Page/完顏宗懿.md "wikilink")，名 阿鄰
  - 原王[完顏宗本](../Page/完顏宗本.md "wikilink")，名 阿魯
  - 翼王[完顏鶻懶](../Page/完顏鶻懶.md "wikilink")
  - 豐王[完顏宗美](../Page/完顏宗美.md "wikilink")，名 胡里甲
  - 鄆王[完顏神土門](../Page/完顏神土門.md "wikilink")
  - 霍王[完顏斛孛束](../Page/完顏斛孛束.md "wikilink")
  - 蔡王[完顏斡烈](../Page/完顏斡烈.md "wikilink")
  - 畢王[完顏宗哲](../Page/完顏宗哲.md "wikilink")，名 鶻沙

## 評價

  - [元朝官修](../Page/元朝.md "wikilink")[正史](../Page/正史.md "wikilink")《[金史](../Page/金史.md "wikilink")》[脱脱等的評價是](../Page/脱脱.md "wikilink")：“天辅草创，未遑礼乐之事。太宗以斜也、宗干知国政，以宗翰、宗望总戎事。既灭辽举宋，即议礼制度，治历明时，缵以武功，述以文事，经国规摹，至是始定。在位十三年，宫室苑籞无所增益。末听大臣计，传位熙宗，使太祖世嗣不失正绪，可谓行其所甚难矣！”<ref>

[《金史·太宗本紀》](http://www.guoxue.com/shibu/24shi/jingshi/js_003.htm)</ref>

## 注釋

<div style="font-size:small">

<references/>

</div>

## 參考文獻

  - 《金史·列传第十四》

[Category:金代皇帝](../Category/金代皇帝.md "wikilink")
[Category:太宗](../Category/太宗.md "wikilink")
[S](../Category/完顏氏.md "wikilink")
[金](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")

1.  《[松漠紀聞](../Page/松漠紀聞.md "wikilink")》：「長子曰宗磐，為宋王、太傅，領尚書省事，與滕王、虞王皆為悟室所誅。次曰賢，為沂王，燕京留守。次曰滕王、虞王。」

2.