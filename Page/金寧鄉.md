[Jincheng_Branch,_Kinmen_County_Fire_Bureau_20050706.jpg](https://zh.wikipedia.org/wiki/File:Jincheng_Branch,_Kinmen_County_Fire_Bureau_20050706.jpg "fig:Jincheng_Branch,_Kinmen_County_Fire_Bureau_20050706.jpg")
**金寧鄉**是隸屬[中華民國](../Page/中華民國.md "wikilink")[福建省](../Page/福建省_\(中華民國\).md "wikilink")[金門縣的一個鄉](../Page/金門縣.md "wikilink")，位於金門島西北隅，三面環海，東與[金湖鎮相鄰](../Page/金湖鎮.md "wikilink")，西南方連接[金城鎮](../Page/金城鎮_\(金門縣\).md "wikilink")，總面積為29.8540平方公里。
1949年[古寧頭戰役後](../Page/古寧頭戰役.md "wikilink")，成立古寧、金盤兩區公所，隸屬金門民政處；1951年，兩區公所合併，更名為金寧區公所，隸屬金門軍管區行政公署，1953年將古寧、安美、湖埔、榜林、盤山、后盤等六個行政區合併為金寧鄉。

## 行政區域

下轄-{后}-盤村、安美村、湖埔村、古寧村、榜林村、盤山村等六個行政村（由36個自然村組成），分為151鄰，

1.  古寧村(28鄰)：轄有南山、北山、林厝、慈湖農莊等四個村落。
2.  安美村(25鄰)：轄有西浦頭、山灶、安岐、東堡、西堡、中堡、湖南七個村落。
3.  湖埔村(31鄰)：轄有湖下、東坑、頂埔下、下埔下、埔邊、埔後、香格里拉等七個村落。
4.  榜林村(30鄰)：轄有榜林、東洲、昔果山、頂後垵、下後垵、後湖、龍門大鎮、議會山莊、甜蜜家園等九個村落。
5.  盤山村(26鄰)：轄有頂堡、下堡、前厝、仁愛新村﹙下田村﹚、柏昱新居、[國立金門大學](../Page/國立金門大學.md "wikilink")、薩爾斯堡等七個村落。
6.  \-{后}-盤村(11鄰)：轄有后盤山、后沙、西山、嚨口等四個自然村。

## 人口

## 教育

### 大專院校

  - [國立金門大學](../Page/國立金門大學.md "wikilink")
  - [銘傳大學金門校區](../Page/銘傳大學.md "wikilink")
  - [國立高雄大學金門校區](../Page/國立高雄大學.md "wikilink")

### 國民中小學

  - [金門縣立金寧國民中小學](http://www.cnjh.km.edu.tw/)

### 國民小學

  - [金門縣金寧鄉金鼎國民小學](http://www.cdps.km.edu.tw/)
  - [金門縣金寧鄉古寧國民小學](http://www.gnes.km.edu.tw/)
  - [金門縣金寧鄉湖埔國民小學](https://web.archive.org/web/20060208230035/http://www.hbes.km.edu.tw/)

## 交通

## 旅遊

[金寧鄉下堡聚落.jpg](https://zh.wikipedia.org/wiki/File:金寧鄉下堡聚落.jpg "fig:金寧鄉下堡聚落.jpg")。\]\]
[古宁头播音墙.JPG](https://zh.wikipedia.org/wiki/File:古宁头播音墙.JPG "fig:古宁头播音墙.JPG")。\]\]

  - [中山紀念林](../Page/中山紀念林.md "wikilink")
  - 古寧頭振威第
  - 東一點紅
  - [慈湖](../Page/慈湖_\(金門縣\).md "wikilink")
  - 盤山坑道
  - 水尾塔
  - [李光前將軍廟](../Page/李光前將軍廟.md "wikilink")
  - 風雞咬令箭
  - 慈堤
  - 雙鯉湖遊客中心
  - [北山古洋樓](../Page/北山古洋樓.md "wikilink")
  - 北山播音牆
  - 北山古洋樓
  - 楊清廉紀念館

### 戰役紀念

  - 北山斷崖
  - [古寧頭戰史館](../Page/古寧頭戰史館.md "wikilink")

## 外部連結

  - [金寧鄉公所](https://jinning.kinmen.gov.tw/Default.aspx)

[Category:金門縣行政區劃](../Category/金門縣行政區劃.md "wikilink")
[金寧鄉](../Category/金寧鄉.md "wikilink")