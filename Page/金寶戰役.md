**金寶戰役**是[第二次世界大戰中](../Page/第二次世界大戰.md "wikilink")[印度第11步兵師的英军和英属印度部队及日軍](../Page/印度第11步兵師.md "wikilink")[第5師團之間一次主要戰役](../Page/第5師團.md "wikilink")，戰事發生自1941年12月30日至1942年1月2日，是日軍在[馬來亞戰役中的首次戰敗](../Page/馬來亞戰役.md "wikilink")。日軍本來希望在[元旦日攻佔](../Page/元旦日.md "wikilink")[金寶作為送給](../Page/金寶.md "wikilink")[日本天皇](../Page/日本天皇.md "wikilink")[裕仁的新年禮物](../Page/裕仁.md "wikilink")，但盟軍堅守此城至1月2日。

## 背景

在[山下奉文将军指挥下的](../Page/山下奉文.md "wikilink")[侵马日军即](../Page/侵马日军.md "wikilink")[大日本帝国第二十五军第三印度师团在马来北部的胜利使得英军及其盟军被迫南撤](../Page/大日本帝国第二十五军.md "wikilink")。而[英军马来西亚指挥部所发出的撤退命令导致了其](../Page/英军马来西亚指挥部.md "wikilink")[印度第11步兵師在](../Page/印度第11步兵師.md "wikilink")[日得拉之战](../Page/日得拉之战.md "wikilink")、[Krohcol](../Page/克罗科之战.md "wikilink")、[亚罗士打](../Page/亚罗士打之战.md "wikilink")、[峨仑(Gurun)的溃败](../Page/峨仑之战.md "wikilink")，整个11师的英籍和印度籍营几近合并。吉打州失陷后，[印军第12步兵旅](../Page/印军第12步兵旅.md "wikilink")，一支久已在马来丛林训练、备战的部队，被派往接替11师。该部队成功撤退至霹雳州金宝的据守工事，并痛击了日军先头部队。
\[1\] The 12th Brigade's job was to buy time for the re-organisation of
the 11th Division and the preparation of defences at Kampar.\[2\]

## 部队序列

### 日军

日军的作战部队来自[松井拓郎](../Page/松井拓郎.md "wikilink") (Takuro Matsui)
的大日本帝国陆军第五师。其中先头部队为[河村三郎](../Page/河村三郎.md "wikilink")(Saburo
Kawamura)少将的第9营第41步兵团，大约4000名新兵。第九营还有渡边上校的第11团和冈部关一的第41步兵团\[3\]。

### 英军

第11师的3个旅中[印军第6步兵旅](../Page/印军第6步兵旅.md "wikilink")、[印军第15步兵旅在营长](../Page/印军第15步兵旅.md "wikilink")[亨利·莫汉德](../Page/亨利·莫汉德.md "wikilink")(Henry
Moorhead)命令下组成第15/6混合旅。其中有第一[皇家莱斯特旅](../Page/皇家莱斯特旅.md "wikilink")(Royal
Leicesters
Regiment)和第二[东萨利旅两个英籍士兵旅的幸存者和](../Page/东萨利旅.md "wikilink")[旁遮普贾特人旅](../Page/旁遮普贾特人旅.md "wikilink")(Jat-Punjab
Regiment)的幸存战士。其余的部队则有[第8旁遮普旅](../Page/第8旁遮普旅.md "wikilink")、[第14旁遮普旅](../Page/第14旁遮普旅.md "wikilink")、[第9旁遮普贾特人旅](../Page/第9旁遮普贾特人旅.md "wikilink")。即便如此，第15/6混合旅仅有1600人。

雷少碧(Ray
Selby)旅长指挥下的[第28廓尔喀旅下辖](../Page/第28廓尔喀旅.md "wikilink")3个廓尔喀营，不但缺乏训练，战力匮乏，更在[吉达](../Page/吉达之战.md "wikilink")、[怡保](../Page/怡保之战.md "wikilink")、[古润中新败](../Page/古润之战.md "wikilink")，士气低落。\[4\]

阿基·帕里斯(Archie Paris)少将临时指挥第11师，他面临防卫特洛·安森(Telok
Anson)的海岸线至金宝城的任务。城东的金宝丘上，英军部队可以俯瞰日军的挺近，而在丛林的掩护下不被发现。帕里斯在西翼安置了火炮哨，并由15/6混合旅掩护，第28廓尔喀旅掩护东侧右翼。\[5\]两翼皆布置配有[QF25磅达重炮](../Page/QF25磅达重炮.md "wikilink")(Ordnance
QF 25 pounder)的第88和第155野战炮兵团火力支持。

第12旅曾经行金宝，执行了掩护海岸和撤退的任务。\[6\]

## 战斗经过

12月30日，川村旅与英军部队开始接触。翌日渡边的第11团与第28廓尔喀旅在右翼试探性交火冲突。在叢林中掩護的廓爾喀旅被發現後，日軍隨即展開攻擊。第155苏格兰兰纳卡新(Lanarkshire
Yeomanry)野战炮兵团亦對日軍展開猛烈炮擊。\[7\] 直至31日，日軍第11團的進攻都被廓爾喀旅和支援炮火擊退。
元旦午夜，第155团瞄准日军开火12发表示新年祝福。\[8\]

元旦当日上午7点，川村强袭西侧阵地。\[9\] 第41团的进攻直指艾思蒙（Esmond
Morrison）的英籍营。在迫击炮的掩护下，强袭直入英军驻地。\[10\]随后两军拉锯，虽然日军火力占优几乎压制了制动权，但前线步兵死伤甚多，\[11\]以致松冈动用了预备役部队。而丛林掩护中的第15/6混合旅在88野战团的支援掩护下成功守住了其防线。\[12\]

在拉锯中，中尉埃德加·纽兰兹(Edgar
Newland)率领30名莱塞斯特兵在防线最突出部成功扼守两天，随后他获得了[軍功十字勳章](../Page/軍功十字勳章.md "wikilink")。\[13\]

东部日军试图抢占汤普森山岭的战壕，但2度为英籍D连和旁遮普贾特营击退。随后一个旁遮普贾特营的顾佳和锡克混成连60人意图重夺战壕，由上尉约翰·昂萨罗·格拉罕(John
Onslow Graham)和中尉查理斯·道格拉斯·兰姆(Charles Douglas
Lamb)率领向日军阵地冲锋，其中兰姆在内的33人阵亡在日军火力下，格拉罕继续冲锋，但自己身中手雷，膝盖以下全部炸去，但仍然呐喊掷弹。共34名印军阵亡，战壕被夺回。随后几日内格拉罕伤重身亡。
\[14\]

### 安顺的战斗

松冈部队伤亡过大而放弃了西侧的进攻，山下将军随即命部队在金宝南部的西海岸登陆，希望自后方突破防线。登陆成功了，2日，部队与[英军第3骑兵师](../Page/英军第3骑兵师.md "wikilink")、第1独立连发生战斗。此战英军随败，但拖延了日军进军步伐。由于后方失守，帕里斯下令放弃了防线，第12旅掩护11师退至细河的防线，展开[细河之战](../Page/细河之战.md "wikilink")。
\[15\]

## 后续

自1941年12月30日至翌年1月2日的战斗中，第11师成功扼守，并重挫了日军主力，
直接导致此后[新加坡之役中日军第](../Page/入侵新加坡.md "wikilink")41步兵团无法参战。日本报纸称日军战死500人，敌方则为150人。但两国军方均没有公布统计。\[16\]
此役是日军侵马以来最严重的一次挫败。尽管英军成功守卫防线，但没有后继兵员和后方失守迫使英军迎接细河战役。\[17\]\[18\]

## 参见

  - [英军马来司令部](../Page/英军马来司令部.md "wikilink")
  - [马来西亚战役](../Page/马来西亚战役.md "wikilink")

## 注释

## 参考文献

  -
  -
  -
  -
## 延展阅读

  -
## 外链

  - [The Battles of Kampar by Chye Kooi
    Loong](https://web.archive.org/web/20091027151639/http://www.geocities.com/alfix7/valour.htm)
  - [Remembering the Battle of Kampar : The Forgotten Heroes of The
    British
    Battalion.](http://www.nmbva.co.uk/remembering_the_battle_of_kampar.htm)

## 外部連結

  - [The Battles of Kampar by Chye Kooi
    Loong](http://www.geocities.com/alfix7/valour.htm)
  - [Remembering the Battle of Kampar : The Forgotten Heroes of The
    British
    Battalion.](http://www.nmbva.co.uk/remembering_the_battle_of_kampar.htm)

[Category:1941的战争](../Category/1941的战争.md "wikilink")
[Category:1941的马来西亚](../Category/1941的马来西亚.md "wikilink")
[Category:二次大战中东南亚的战事](../Category/二次大战中东南亚的战事.md "wikilink")
[Category:英国参与的战争](../Category/英国参与的战争.md "wikilink")
[Category:日本参与的战争](../Category/日本参与的战争.md "wikilink")
[Category:第二次世界大戰太平洋戰場戰役](../Category/第二次世界大戰太平洋戰場戰役.md "wikilink")
[Category:英國战役](../Category/英國战役.md "wikilink")
[Category:日本战役](../Category/日本战役.md "wikilink")
[Category:澳大利亚战役](../Category/澳大利亚战役.md "wikilink")
[Category:印度战役](../Category/印度战役.md "wikilink")

1.
2.

3.  Thompson pg. 155 and 187

4.
5.
6.
7.
8.  Smith pg. 304–305

9.
10.
11.
12.
13.
14.

15.
16.

17. Thompson pg. 187–188

18. Owen pg. 94–95