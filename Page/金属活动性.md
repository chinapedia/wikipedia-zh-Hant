<table>
<thead>
<tr class="header">
<th><p><a href="../Page/金属.md" title="wikilink">金属</a></p></th>
<th><p><a href="../Page/离子.md" title="wikilink">离子</a></p></th>
<th><p>活动性</p></th>
<th><p>提取法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/铯.md" title="wikilink">Cs</a></p></td>
<td><p>Cs<sup>+</sup></p></td>
<td><p>与水反应</p></td>
<td><p><a href="../Page/电解.md" title="wikilink">电解</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/铷.md" title="wikilink">Rb</a></p></td>
<td><p>Rb<sup>+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/钾.md" title="wikilink">K</a></p></td>
<td><p>K<sup>+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/钠.md" title="wikilink">Na</a></p></td>
<td><p>Na<sup>+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/锂.md" title="wikilink">Li</a></p></td>
<td><p>Li<sup>+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/钡.md" title="wikilink">Ba</a></p></td>
<td><p>Ba<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/锶.md" title="wikilink">Sr</a></p></td>
<td><p>Sr<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/钙.md" title="wikilink">Ca</a></p></td>
<td><p>Ca<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/镁.md" title="wikilink">Mg</a></p></td>
<td><p>Mg<sup>2+</sup></p></td>
<td><p>与酸反应</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/铝.md" title="wikilink">Al</a></p></td>
<td><p>Al<sup>3+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鈦.md" title="wikilink">Ti</a></p></td>
<td><p>Ti<sup>4+</sup></p></td>
<td><p>與濃的無機酸反應</p></td>
<td><p>火法冶煉在克羅爾過程中可用<br />
鎂、鈣、氫或其他不太常見的鹼金屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/锰.md" title="wikilink">Mn</a></p></td>
<td><p>Mn<sup>2+</sup></p></td>
<td><p>与酸反应</p></td>
<td><p>用<a href="../Page/碳热反应.md" title="wikilink">碳热还原</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/锌.md" title="wikilink">Zn</a></p></td>
<td><p>Zn<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/铬.md" title="wikilink">Cr</a></p></td>
<td><p>Cr<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/铁.md" title="wikilink">Fe</a></p></td>
<td><p>Fe<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/镉.md" title="wikilink">Cd</a></p></td>
<td><p>Cd<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/钴.md" title="wikilink">Co</a></p></td>
<td><p>Co<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/镍.md" title="wikilink">Ni</a></p></td>
<td><p>Ni<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/锡.md" title="wikilink">Sn</a></p></td>
<td><p>Sn<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/铅.md" title="wikilink">Pb</a></p></td>
<td><p>Pb<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/锑.md" title="wikilink">Sb</a></p></td>
<td><p>Sb<sup>3+</sup></p></td>
<td><p>与强氧化性酸反应</p></td>
<td><p>热分解或物理方法</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/铋.md" title="wikilink">Bi</a></p></td>
<td><p>Bi<sup>3+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/铜.md" title="wikilink">Cu</a></p></td>
<td><p>Cu<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/汞.md" title="wikilink">Hg</a></p></td>
<td><p>Hg<sub>2</sub><sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/銀.md" title="wikilink">Ag</a></p></td>
<td><p>Ag<sup>+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金.md" title="wikilink">Au</a></p></td>
<td><p>Au<sup>3+</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/铂.md" title="wikilink">Pt</a></p></td>
<td><p>Pt<sup>2+</sup></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

**金属活动性**（又稱活性序）是指[金属在](../Page/金属.md "wikilink")[溶液或](../Page/溶液.md "wikilink")[化学反应中的活泼程度](../Page/化学反应.md "wikilink")。它最初是由化学家根据金属间的[置换反应](../Page/置换反应.md "wikilink")，还有金属跟水和各种[酸](../Page/酸.md "wikilink")、[碱的反应总结而成](../Page/碱.md "wikilink")。这个序列体现了金属在[溶液中活动性的大小关系](../Page/溶液.md "wikilink")。在判断[溶液中的置换反应能否发生时](../Page/溶液.md "wikilink")，使用它是一种很简便的办法。

## 含义

1.  排在前面的金属可以将排在后面的金属从它们的盐[溶液中置换出来](../Page/溶液.md "wikilink")
2.  理论上讲，排在[氢](../Page/氢.md "wikilink")（H<sub>2</sub>）前的金属才能和[酸反应](../Page/酸.md "wikilink")，置换出[氢](../Page/氢.md "wikilink")

## 金属活动性顺序表

  - 此表中金属活动性由上至下逐渐减小。
  - 在[氢之前的金属可以将](../Page/氢.md "wikilink")[酸中的氢置换出来](../Page/酸.md "wikilink")
  - 表中某金属可以把它后面的金属从它的[盐溶液中置换出来](../Page/盐.md "wikilink")

常見金属活动性排列通常是这样的：

<div style="text-align: center;">

[Cs](../Page/銫.md "wikilink") \> [Rb](../Page/銣.md "wikilink") \>
[K](../Page/钾.md "wikilink") \>[Na](../Page/钠.md "wikilink")\>
[Li](../Page/锂.md "wikilink") \> [Ra](../Page/鐳.md "wikilink") \>
[Ba](../Page/鋇.md "wikilink") \> [Sr](../Page/鍶.md "wikilink") \>
[Ca](../Page/钙.md "wikilink")\> [Mg](../Page/鎂.md "wikilink") \>
[Al](../Page/鋁.md "wikilink") \> [Ti](../Page/鈦.md "wikilink") \>
[Mn](../Page/锰.md "wikilink") \> [Zn](../Page/鋅.md "wikilink") \>
[Cr](../Page/鉻.md "wikilink")\> [Fe](../Page/鐵.md "wikilink") \>
[Cd](../Page/鎘.md "wikilink") \> [Co](../Page/鈷.md "wikilink") \>
[Ni](../Page/镍.md "wikilink") \> [Sn](../Page/錫.md "wikilink") \>
[Pb](../Page/鉛.md "wikilink") \>
([H<sub>2</sub>](../Page/氢.md "wikilink")) \>
[Sb](../Page/銻.md "wikilink") \> [Bi](../Page/鉍.md "wikilink") \>
[Cu](../Page/銅.md "wikilink") \> [W](../Page/鎢.md "wikilink") \>
[Hg](../Page/汞.md "wikilink") \> [Ag](../Page/銀.md "wikilink")\>
[Pt](../Page/鉑.md "wikilink") \> [Au](../Page/金.md "wikilink")
\[1\]\[2\]

</div>

  - 常温下能與[水迅速進行置換反應](../Page/水.md "wikilink")：

<!-- end list -->

1.  [钾](../Page/钾.md "wikilink")（K）
2.  [钠](../Page/钠.md "wikilink")（Na）
3.  [鋰](../Page/鋰.md "wikilink")（Li）
4.  [钙](../Page/钙.md "wikilink")（Ca）

<!-- end list -->

  - 能與[酸進行置換反應](../Page/酸.md "wikilink")(也能與水進行，只是較緩慢)：

<!-- end list -->

1.  [镁](../Page/镁.md "wikilink")（Mg）
2.  [铝](../Page/铝.md "wikilink")（Al）
3.  [锰](../Page/锰.md "wikilink")（Mn）
4.  [锌](../Page/锌.md "wikilink")（Zn）
5.  [鉻](../Page/鉻.md "wikilink")（Cr）
6.  [铁](../Page/铁.md "wikilink")（Fe）
7.  [镍](../Page/镍.md "wikilink")（Ni）
8.  [锡](../Page/锡.md "wikilink")（Sn）
9.  [铅](../Page/铅.md "wikilink")（Pb）
10. **[氢](../Page/氢.md "wikilink")（H<sub>2</sub>）**

<!-- end list -->

  - 不能與[水或](../Page/水.md "wikilink")[酸進行置換反應](../Page/酸.md "wikilink")：

<!-- end list -->

1.  [铜](../Page/铜.md "wikilink")（Cu）
2.  [汞](../Page/汞.md "wikilink")（Hg）
3.  [银](../Page/银.md "wikilink")（Ag）
4.  [金](../Page/金.md "wikilink")（Au）
5.  [铂](../Page/铂.md "wikilink")（Pt）

### 活泼金属和酸置换产生氢气时生成的离子

| 活泼金属 | 和酸发生置换反应产生的离子         | 颜色                                                              |
| ---- | --------------------- | --------------------------------------------------------------- |
| K    | K<sup>+</sup>         | [无色](../Page/无色.md "wikilink")                                  |
| Na   | Na<sup>+</sup>        | 无色                                                              |
| Li   | Li<sup>+</sup>        | 无色                                                              |
| Ca   | Ca<sup>2+</sup>       | 无色                                                              |
| Mg   | Mg<sup>2+</sup>       | 无色                                                              |
| Al   | Al<sup>3+</sup>       | 无色                                                              |
| Mn   | Mn<sup>2+</sup>       | [淡粉红色](../Page/淡粉红色.md "wikilink")                              |
| Zn   | Zn<sup>2+</sup>       | 无色                                                              |
| Fe   | Fe<sup>2+</sup>(亚铁离子) | [浅绿色](../Page/浅绿色.md "wikilink")                                |
| Co   | Co<sup>2+</sup>       | [红色至](../Page/红色.md "wikilink")[紫红色](../Page/紫红色.md "wikilink") |
| Ni   | Ni<sup>2+</sup>       | [碧绿色](../Page/碧绿色.md "wikilink")                                |
| Sn   | Sn<sup>2+</sup>       | 无色                                                              |
| Pb   | Pb<sup>2+</sup>       | 无色                                                              |

## 与标准电极电势比较

金属活动性顺序有时也根据标准电极电势反向列出：

<div style="text-align: center;">

[Li](../Page/锂.md "wikilink") \> [Cs](../Page/銫.md "wikilink") \>
[Rb](../Page/銣.md "wikilink") \> [K](../Page/钾.md "wikilink") \>
[Ra](../Page/鐳.md "wikilink") \> [Ba](../Page/鋇.md "wikilink") \>
[Sr](../Page/鍶.md "wikilink") \> [Ca](../Page/钙.md "wikilink") \>
[Na](../Page/钠.md "wikilink") \> [Mg](../Page/鎂.md "wikilink") \>
[Al](../Page/鋁.md "wikilink") \> [Ti](../Page/鈦.md "wikilink") \>
[Mn](../Page/锰.md "wikilink") \> [Zn](../Page/鋅.md "wikilink") \>
[Cr](../Page/鉻.md "wikilink")\> [Fe](../Page/鐵.md "wikilink") \>
[Cd](../Page/鎘.md "wikilink") \> [Co](../Page/鈷.md "wikilink") \>
[Ni](../Page/镍.md "wikilink") \> [Sn](../Page/錫.md "wikilink") \>
[Pb](../Page/鉛.md "wikilink") \>
([H<sub>2</sub>](../Page/氢.md "wikilink")) \>
[Sb](../Page/銻.md "wikilink") \> [Bi](../Page/鉍.md "wikilink") \>
[Cu](../Page/銅.md "wikilink") \> [W](../Page/鎢.md "wikilink") \>
[Hg](../Page/汞.md "wikilink") \> [Ag](../Page/銀.md "wikilink") \>
[Pt](../Page/鉑.md "wikilink") \>
[Au](../Page/金.md "wikilink")\[3\]\[4\]\[5\]

</div>

这样[锂和](../Page/锂.md "wikilink")[钠活动性顺序相反](../Page/钠.md "wikilink")，[金和](../Page/金.md "wikilink")[铂顺序也相反](../Page/铂.md "wikilink")。后者实际上并无碍，因为两种金属都相当不活泼。

标准电极电势定量衡量还原剂的强弱，不像金属活动性顺序那样是定性考虑。然而标准电极电势只在[标准状况下有效](../Page/标准状况.md "wikilink")，特别是只适用于室温下一定浓度的水溶液。即便如此，锂、钠等金属的标准电极电势顺序仍然是反常的。从它们和水的反应剧烈程度以及金属表面在空气中失去光泽的速度来看，这些金属的活动性顺序是

钾\>钠\>锂\>碱土金属，

和它们气态时的[电离能的反序一样](../Page/电离能.md "wikilink")。这也和电解[氯化锂和](../Page/氯化锂.md "wikilink")[氯化钾](../Page/氯化钾.md "wikilink")[共晶系统的结果一致](../Page/共晶系统.md "wikilink")：在阴极生成的是锂不是钾。

## 参见

  - [非金属性](../Page/非金属性.md "wikilink")
  - [金属性](../Page/金属性.md "wikilink")

## 參考資料

[Category:金属](../Category/金属.md "wikilink")
[Category:化學列表](../Category/化學列表.md "wikilink")
[Category:无机化学](../Category/无机化学.md "wikilink")

1.
2.  <http://www.cod.edu/people/faculty/jarman/richenda/1551_hons_materials/Activity%20series.htm>
3.  [Standard Redox Potential
    Table](http://issuu.com/time-to-wake-up/docs/electrochemical_redox_potential)
4.
5.