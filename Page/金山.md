**金山**可以指：

## 地名

  - [金山郡](../Page/金山郡.md "wikilink")，[中国古代的](../Page/中国.md "wikilink")[郡](../Page/郡.md "wikilink")，在今[四川省境](../Page/四川省.md "wikilink")
  - [金山区
    (上海市)](../Page/金山区_\(上海市\).md "wikilink")，中国大陸[上海市的](../Page/上海市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")
  - [金山區
    (新北市)](../Page/金山區_\(新北市\).md "wikilink")，[臺灣](../Page/臺灣.md "wikilink")[新北市的](../Page/新北市.md "wikilink")[區](../Page/區_\(中華民國\).md "wikilink")，舊名金山鄉
  - [金山-{里}-
    (關西鎮)](../Page/金山里_\(關西鎮\).md "wikilink")，臺灣[新竹縣](../Page/新竹縣.md "wikilink")[關西鎮的](../Page/關西鎮_\(臺灣\).md "wikilink")[-{里}-](../Page/里_\(中華民國\).md "wikilink")
  - [金山-{里}-
    (新竹市)](../Page/金山里_\(新竹市\).md "wikilink")，臺灣[新竹市](../Page/新竹市.md "wikilink")[東區的村](../Page/東區_\(新竹市\).md "wikilink")-{里}-
  - [旧金-{}-山](../Page/旧金山.md "wikilink")，[美國](../Page/美國.md "wikilink")[加利福尼亚州的](../Page/加利福尼亚州.md "wikilink")[城市](../Page/城市.md "wikilink")，又称为“聖佛朗西斯科”或“三藩-{}-市”
  - [新金山](../Page/新金山.md "wikilink")，為[澳大利亚城市](../Page/澳大利亚.md "wikilink")[墨爾本的中譯舊稱](../Page/墨爾本.md "wikilink")
  - [金山町](../Page/金山町.md "wikilink")
  - [金山县](../Page/金山县_\(消歧义\).md "wikilink")
  - [金山街道](../Page/金山街道.md "wikilink")
  - [金山镇](../Page/金山镇.md "wikilink")
  - [金山鄉](../Page/金山鄉.md "wikilink")
  - [金山
    (鐵嶺)](../Page/金山_\(鐵嶺\).md "wikilink")，[遼寧省](../Page/遼寧省.md "wikilink")[鐵嶺市](../Page/鐵嶺市.md "wikilink")[昌圖縣金山堡一帶](../Page/昌圖縣.md "wikilink")

## 公司名

  - [金山软件公司](../Page/金山软件公司.md "wikilink")，中国大陸软件企业
  - [金山工業](../Page/金山工業.md "wikilink")，亞洲跨國集團，主要生產電池等

## 山名

  - [金山 (群马)](../Page/金山_\(群马\).md "wikilink")，日本群马县太田市的一座山
  - [金山 (镇江)](../Page/金山_\(镇江\).md "wikilink")，中国大陸江苏省镇江市的一座山
  - [金山 (香港)](../Page/金山_\(香港\).md "wikilink")，位于香港新界的金山郊野公园
  - [阿尔泰山](../Page/阿尔泰山.md "wikilink")，古称“金山”，蒙古和中国新疆交界处的一座山脉
  - [金山 (山东)](../Page/金山_\(山东\).md "wikilink")，位于中国大陸山东省菏泽市巨野县
  - [大金山](../Page/大金山.md "wikilink")、[小金山](../Page/小金山.md "wikilink")，位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[金山区](../Page/金山区_\(上海市\).md "wikilink")[山阳镇](../Page/山阳镇_\(上海市\).md "wikilink")
  - [玉泉山](../Page/玉泉山.md "wikilink")，北京市的一座山，亦稱金山
  - [金山
    (馬來西亞)](../Page/金山_\(馬來西亞\).md "wikilink")，[馬來西亞](../Page/馬來西亞.md "wikilink")[柔佛州的一座山](../Page/柔佛州.md "wikilink")

## 人名

  - [耶律金山](../Page/耶律金山.md "wikilink")（？－1217年），中国後遼的一个皇帝

  - [金山
    (演员)](../Page/金山_\(演员\).md "wikilink")（1911年－1982年），原名赵默，中国话剧与电影演员、导演

  - [金山 (香港演员)](../Page/金山_\(香港演员\).md "wikilink")（1945年－），香港資深演員，後來改名甘山

  - （1905-1938年），韩国独立运动家

  - [金山 (姓氏)](../Page/金山_\(姓氏\).md "wikilink")，日本姓氏

## 其他

  - [金山溫泉](../Page/金山溫泉.md "wikilink")，位於臺灣新北市金山區的[温泉區](../Page/温泉.md "wikilink")
  - [金山車站
    (愛知縣)](../Page/金山車站_\(愛知縣\).md "wikilink")，位於[日本](../Page/日本.md "wikilink")[名古屋市](../Page/名古屋市.md "wikilink")[熱田區的](../Page/熱田區.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")
  - [金山 (明朝)](../Page/金山_\(明朝\).md "wikilink")，明朝皇家墓葬区，在今北京市境
  - [金山国](../Page/金山国.md "wikilink")，即张承奉担任[归义军节度使时的政权名称](../Page/归义军.md "wikilink")

## 参见

  -
[de:Gold\#Gewinnung](../Page/de:Gold#Gewinnung.md "wikilink")