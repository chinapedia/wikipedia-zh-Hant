[金时习《诗稿》.jpg](https://zh.wikipedia.org/wiki/File:金时习《诗稿》.jpg "fig:金时习《诗稿》.jpg")
**金時習**（），為[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")[朝鮮王朝时期的](../Page/朝鮮王朝.md "wikilink")[詩人](../Page/詩人.md "wikilink")、[小說家](../Page/小說家.md "wikilink")。本贯[江原道](../Page/江原道.md "wikilink")[江陵](../Page/江陵市.md "wikilink")，字悅卿，號梅月堂、東峰、清寒子、碧山，法號雪岑，[諡號清簡](../Page/諡號.md "wikilink")。[生六臣之一](../Page/生六臣.md "wikilink")。

## 生平

金時習出生於[漢陽](../Page/漢陽.md "wikilink")（今[首爾](../Page/首爾.md "wikilink")）一[兩班貴族家庭](../Page/兩班.md "wikilink")，幼時聰穎過人，三歲能詩，詩曰：「無雨雷聲何處動，黃雲片片四方分」。此事在其五歲時，傳入[世宗耳裡](../Page/李祹.md "wikilink")，被譽為神童，備受世宗寵愛。曾隨名師金泮學習四書五經及百家著作，十五歲至三角山讀書堂學習。後逢母喪，父染重病，與訓鍊院都正[南孝禮的女兒結婚](../Page/南孝禮.md "wikilink")。

1455年[世祖篡位後](../Page/李瑈.md "wikilink")，金時習對世祖暴政不滿，撕毀儒服，削髮為僧，雲遊四處，並開始其文學創作的生涯。1464年起，於[慶州](../Page/慶州.md "wikilink")[金鰲山居住六年](../Page/金鰲山.md "wikilink")，此時期撰寫了[韓國第一部小說](../Page/韓國.md "wikilink")《[金鰲新話](../Page/金鰲新話.md "wikilink")》。

1471年拒世祖招請，後移居[京畿道](../Page/京畿道.md "wikilink")[楊州郡水洛山](../Page/楊州郡.md "wikilink")，於1481年還俗。晚年在[江原道](../Page/江原道.md "wikilink")[雪嶽山隱居](../Page/雪嶽山.md "wikilink")，1493年逝世於[慶州鴻山無量寺](../Page/慶州.md "wikilink")。

## 作品

金時習著有詩集《宕遊關西錄》、《宕遊關東錄》、《宕遊湖南錄》、《宕遊金鰲錄》、《關東日錄》、《溟州日錄》，以及短集小說集《金鰲新話》。於其逝世後，集結有《梅月堂文集》19卷及《梅月堂詩集》15卷。

## 文學地位

金時習是韓國文學史上第一個小說家，他於金鰲山居住期間創作的漢文短篇小說集《[金鰲新話](../Page/金鰲新話.md "wikilink")》，在人物的描寫和情節的安排上，都突破了以往稗說作品的侷限，具備小說體裁的特徵，成為韓國的第一部小說作品。其創作來源可溯自韓國古代說話和[高麗末的詩話](../Page/高麗.md "wikilink")、假傳等題材，且受到[明代瞿佑](../Page/明朝.md "wikilink")《[剪燈新話](../Page/剪燈新話.md "wikilink")》的直接影響。《金鰲新話》的出現，開創了韓國小說的先河，可說是韓國文學史上劃時代的成就。

[K](../Category/朝鮮王朝作家.md "wikilink")
[K](../Category/朝鮮王朝人物.md "wikilink")
[K](../Category/朝鮮王朝詩人.md "wikilink")
[K](../Category/江原道出身人物.md "wikilink")
[K](../Category/首爾特別市出身人物.md "wikilink")
[Si](../Category/金姓.md "wikilink")
[Category:還俗人物](../Category/還俗人物.md "wikilink")