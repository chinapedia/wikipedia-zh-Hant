**金智淑**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")。現任[成均館大學演劇映像學部兼任教授](../Page/成均館大學.md "wikilink")。

## 演出作品

### 電視劇

  - 2005年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[天國的階梯](../Page/天國的階梯.md "wikilink")》飾演
    閔瑞賢
  - 2008年：SBS《[明星的戀人](../Page/明星的戀人.md "wikilink")》飾演 李寶英（哲秀母）
  - 2010年：SBS《[秘密花園](../Page/秘密花園_\(韓國電視劇\).md "wikilink")》飾演 文妍紅
  - 2011年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[我也是花](../Page/我也是花.md "wikilink")》飾演
    金道美
  - 2012年：[TV朝鮮](../Page/TV朝鮮.md "wikilink")《[韓半島](../Page/韓半島_\(電視劇\).md "wikilink")》
  - 2012年：[JTBC](../Page/JTBC.md "wikilink")《[無子無懮](../Page/無子無懮.md "wikilink")》飾演
    仁哲母（特別演出）
  - 2013年：SBS《[奇怪的保姆](../Page/奇怪的保姆.md "wikilink")》飾演 福女的婆婆
  - 2014年：MBC《[薔薇色戀人們](../Page/薔薇色戀人們.md "wikilink")》飾演 泰植母（特別演出）
  - 2015年：MBC《[女王之花](../Page/女王之花.md "wikilink")》飾演 裴秀賢

### 電影

  - 1984年：《[藍色的天空銀河](../Page/藍色的天空銀河.md "wikilink")》
  - 1985年：《[最後的夏天](../Page/最後的夏天.md "wikilink")》
  - 1987年：《[風吹的日花也被告](../Page/風吹的日花也被告.md "wikilink")》
  - 1988年：《[Dopodomani](../Page/Dopodomani.md "wikilink")》
  - 2004年：《[冬話](../Page/冬話.md "wikilink")》（未播出）
  - 2005年：《[85度瘋狂之愛](../Page/85度瘋狂之愛.md "wikilink")》

## 外部連結

  - [NAVER](http://people.search.naver.com/search.naver?sm=tab_txc&where=people_profile&ie=utf8&query=%EA%B9%80%EC%A7%80%EC%88%99&os=161006)

[K](../Category/韓國電視演員.md "wikilink")
[K](../Category/韓國電影演員.md "wikilink")
[K](../Category/韓國舞台演員.md "wikilink")
[K](../Category/首爾大學校友.md "wikilink")
[K](../Category/高麗大學校友.md "wikilink")
[J](../Category/金姓.md "wikilink")