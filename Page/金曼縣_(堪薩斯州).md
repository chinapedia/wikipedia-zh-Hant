**金曼縣**（，簡稱**KM**）是[美國](../Page/美國.md "wikilink")[堪薩斯州南部的一個縣](../Page/堪薩斯州.md "wikilink")。面積2,245平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口8,673人。縣治[金曼](../Page/金曼_\(堪薩斯州\).md "wikilink")（Kingman）。

成立於1872年2月29日，縣政府成立於1874年2月27日。縣名紀念[制憲會議成員](../Page/堪薩斯州憲法.md "wikilink")、[州最高法院首席法官](../Page/堪薩斯州最高法院.md "wikilink")[薩穋爾·A·金曼](../Page/薩穋爾·A·金曼.md "wikilink")\[1\]。

## 参考文献

[Category:堪萨斯州行政区划](../Category/堪萨斯州行政区划.md "wikilink")
[金曼縣_(堪薩斯州)](../Category/金曼縣_\(堪薩斯州\).md "wikilink")
[Category:1872年堪薩斯州建立](../Category/1872年堪薩斯州建立.md "wikilink")
[Category:1872年建立的聚居地](../Category/1872年建立的聚居地.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.