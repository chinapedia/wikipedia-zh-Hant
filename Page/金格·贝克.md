**彼德·爱德华·金格·贝克**（，）藝名金格·貝克()是一个[英格兰](../Page/英格兰.md "wikilink")[鼓手和](../Page/鼓手.md "wikilink")[歌手](../Page/歌手.md "wikilink")。他在20世紀60年代的作品為他贏得了“搖滾的第一位超級巨星鼓手”的稱讚，儘管他的個人風格融合了[爵士樂背景](../Page/爵士樂.md "wikilink")，並對非洲節奏感興趣。貝克被譽為[融合爵士樂](../Page/融合爵士樂.md "wikilink")，[重金屬和](../Page/重金屬.md "wikilink")[世界音樂等類型的鼓手的先驅](../Page/世界音樂.md "wikilink")。\[1\]

1966年到1968年，他是[奶油樂隊成员之一](../Page/奶油樂隊.md "wikilink")\[2\]。1969年，他和[史蒂夫·温伍德](../Page/史蒂夫·温伍德.md "wikilink")、[呂克·格雷奇一起加入了](../Page/呂克·格雷奇.md "wikilink")[埃里克·克莱普顿创建的](../Page/埃里克·克莱普顿.md "wikilink")[盲目信仰乐團](../Page/盲目信仰乐團.md "wikilink")。1970年代初期，他组建了。

贝克精湛技艺，风格华丽，表演力强，他率先在表演中使用了两个低音贝斯鼓。他还会演奏其它的[打击乐器](../Page/打击乐器.md "wikilink")。1993年以[奶油樂團的成員入選](../Page/奶油樂隊.md "wikilink")[搖滾名人堂](../Page/搖滾名人堂.md "wikilink")
\[3\]，2008年入選[現代鼓手的名人堂](../Page/現代鼓手.md "wikilink")
\[4\]，2016年入選[經典鼓手名人堂](../Page/經典鼓手.md "wikilink")\[5\]，
2016年《[滾石雜誌](../Page/滾石雜誌.md "wikilink")》史上最偉大的鼓手排名第3名。\[6\]

## 參考資料

## 外部連結

  - [Official Site and Online Store](http://www.gingerbaker.com)

  - [The Ginger Baker Site](http://www.ginger-baker.eu)

  - [Beware Of Mr. Baker site](http://www.bewareofmrbaker.com)

  - [Biography &
    Discography](http://www.musicianguide.com/biographies/1608000595/Ginger-Baker.html)
    at Musicianguide.com

  -
  - [Ginger Baker Article by Jay
    Bulger](http://www.independent.ie/entertainment/music/ginger-baker-the-devil-looks-after-his-own-1879629.html)

  - ["Ginger Baker: 'I came off heroin something like 29
    times'"](https://www.theguardian.com/music/2013/jan/05/ginger-baker-cream-interview),
    Edward Helmore, *[The
    Observer](../Page/The_Observer.md "wikilink")*, 2013-01-05

  - [贝克乐队的贡献](https://web.archive.org/web/20020908150411/http://members.tripod.com/rant58/id368.htm)

[B](../Category/搖滾名人堂入選者.md "wikilink")
[Category:倫敦人](../Category/倫敦人.md "wikilink")
[Category:英國音樂家](../Category/英國音樂家.md "wikilink")
[Category:英國鼓手](../Category/英國鼓手.md "wikilink")
[Category:搖滾樂鼓手](../Category/搖滾樂鼓手.md "wikilink")
[Category:英格蘭歌曲作家](../Category/英格蘭歌曲作家.md "wikilink")

1.  Adam Budofski, *The Drummer: 100 Years of Rhythmic Power and
    Invention*, Hal Leonard Corporation, 2010
2.
3.
4.
5.
6.