**金毓嶂**，[中國](../Page/中國.md "wikilink")[清朝](../Page/清朝.md "wikilink")[皇室](../Page/皇室.md "wikilink")[愛新覺羅後裔](../Page/愛新覺羅.md "wikilink")，醇親王[載灃長孫](../Page/載灃.md "wikilink")，[溥任長子](../Page/溥任.md "wikilink")，也是中國末代皇帝[溥儀侄子](../Page/溥儀.md "wikilink")，也是目前醇親王家族最近支的嫡系後裔。2015年父親[溥任過世後](../Page/溥任.md "wikilink")，金毓嶂身為家族長孫，名義上成為爱新觉罗宗族首領。\[1\]

## 個人經歷

  - 早年就讀於[北京地质学院](../Page/北京地质学院.md "wikilink")，[1968年大学毕业後](../Page/1968年.md "wikilink")，被分配到[青海省地矿局第二地质队工作](../Page/青海省.md "wikilink")。
  - [1985年才从青海调回北京](../Page/1985年.md "wikilink")[崇文区环保局做技术工作](../Page/崇文区.md "wikilink")。
  - [1999年开始担任](../Page/1999年.md "wikilink")[北京市](../Page/北京市.md "wikilink")[政協常委及](../Page/中國人民政治協商會議.md "wikilink")[崇文區政協副主席](../Page/崇文區.md "wikilink")，並兼任北京市民族事务委员会副主任。

他有一個女兒金鑫（1976年—），畢業於大學[電腦相關科目](../Page/電腦.md "wikilink")，現於中國電子製品有限公司工作。而他的弟弟[金毓峑現任](../Page/金毓峑.md "wikilink")[北京工業大學環境與能源工程學院副主席](../Page/北京工業大學.md "wikilink")。

## 祖先

<center>

</center>

### 直系血脈

1.  [錫寶齊篇古](../Page/錫寶齊篇古.md "wikilink")
2.  清肇祖[孟特穆](../Page/孟特穆.md "wikilink")
3.  清興祖[福滿](../Page/福滿.md "wikilink")
4.  清景祖[覺昌安](../Page/覺昌安.md "wikilink"), 1526－1583
5.  清顯祖[塔克世](../Page/塔克世.md "wikilink"), 1543－1583
6.  清太祖[努爾哈赤](../Page/努爾哈赤.md "wikilink"), 1559-1626
7.  清太宗[皇太極](../Page/皇太極.md "wikilink"), 1592-1643
8.  清世祖[順治帝](../Page/順治.md "wikilink")[福臨](../Page/福臨.md "wikilink"),
    1638-1661
9.  清聖祖[康熙帝](../Page/康熙.md "wikilink")[玄燁](../Page/玄燁.md "wikilink"),
    1654-1722
10. 清世宗[雍正帝](../Page/雍正.md "wikilink")[胤禛](../Page/胤禛.md "wikilink"),
    1678-1735
11. 清高宗[乾隆帝](../Page/乾隆.md "wikilink")[弘曆](../Page/弘曆.md "wikilink"),
    1711-1799
12. 清仁宗[嘉慶帝](../Page/嘉慶.md "wikilink")[顒琰](../Page/顒琰.md "wikilink"),
    1760-1820
13. 清宣宗[道光帝](../Page/道光.md "wikilink")[旻寧](../Page/旻寧.md "wikilink"),
    1782–1850
14. 七子[和碩醇賢親王](../Page/和碩親王.md "wikilink")[奕譞](../Page/奕譞.md "wikilink"),
    1840–1891
15. 五子[攝政](../Page/攝政.md "wikilink")[醇親王](../Page/醇親王.md "wikilink")[載灃](../Page/載灃.md "wikilink"),
    1883–1951
16. 四子[溥任](../Page/溥任.md "wikilink")（金友之）, 1918–2015
17. 長子金毓嶂, 1942至今

## 關聯作品

《生正逢时》：金毓嶂口述/[北京市](../Page/北京市.md "wikilink")[东城区档案局馆整理](../Page/东城区.md "wikilink")，[人民出版社出版](../Page/人民出版社.md "wikilink")，[2013年](../Page/2013年.md "wikilink")[2月1日](../Page/2月1日.md "wikilink")。

## 關聯條目

  - [醇親王](../Page/醇親王.md "wikilink")
  - [現有王位覬覦者列表\#亞洲](../Page/現有王位覬覦者列表#亞洲.md "wikilink")

## 参考文献

## 外部連結

  - [記愛新覺羅·載灃的後代（文/何大章）（中文）](http://blog.sina.com.cn/s/blog_483b3bae010089b1.html)
  - [Life in colour of the Imperial Family's
    descendants](https://web.archive.org/web/20060525025248/http://news.china.com/zh_cn/history/all/11025807/20050325/12193586.html)

  - [清朝皇室后裔](https://web.archive.org/web/20061018154307/http://www.bj.xinhuanet.com/bjpd_sdwm/2006-07/04/content_7422818.htm)

  - [改换皇姓隐身角落　末代皇族淡定生活](https://web.archive.org/web/20070929093023/http://pdf.sznews.com/hkcd/2000/1218/newsfile/n7-5.htm)

  - [《生正逢时》在豆瓣上的介紹](https://book.douban.com/subject/21452678/)

[J](../Category/清朝宗室后裔.md "wikilink")
[J](../Category/中国地质大学校友.md "wikilink")
[J](../Category/北京人.md "wikilink")
[J](../Category/愛新覺羅氏.md "wikilink")

1.