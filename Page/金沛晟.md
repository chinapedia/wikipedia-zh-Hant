**金沛晟**（1976年8月22日），曾用名**金沛辰**，原名**莫昌成**。
[馬來西亞男藝人](../Page/馬來西亞.md "wikilink")，能講流利的[英語](../Page/英語.md "wikilink")、[華語](../Page/華語.md "wikilink")、[粵語](../Page/粵語.md "wikilink")、[馬來語](../Page/馬來語.md "wikilink")、[客家語](../Page/客家語.md "wikilink")，屬於紅元素娛樂、橞霙經紀工作室旗下藝人。

## 作品

### 廣告

  - 1997年[百事可樂](../Page/百事可樂.md "wikilink") （馬來西亞電視廣告片）
  - 1998年 swatch （平面）

### 戲劇作品

  - 2000年《[七條命](../Page/七條命.md "wikilink")》
    （與[李修賢合演](../Page/李修賢.md "wikilink")）
  - 《[黑社會.com](../Page/黑社會.com.md "wikilink")》
    （與[呂頌賢合演](../Page/呂頌賢.md "wikilink")）
  - 《[鬼同你有緣](../Page/鬼同你有緣.md "wikilink")》
    （與[黃秋生合演](../Page/黃秋生.md "wikilink")）
  - 1998年《龍本多情》 （與[陳小春合演](../Page/陳小春.md "wikilink")）
  - 《[一帆風順](../Page/一帆風順.md "wikilink")》
    （與[尹天照合演](../Page/尹天照.md "wikilink")）
  - 2001年[中視](../Page/中視.md "wikilink")《[新蜀山劍俠](../Page/新蜀山劍俠.md "wikilink")》
  - 2002年[亞視](../Page/亞視.md "wikilink")《[暴風型警](../Page/暴風型警.md "wikilink")》飾演莫昌誠
  - 2003年[亞視](../Page/亞視.md "wikilink")《[萬家燈火](../Page/萬家燈火.md "wikilink")》飾演高進
  - 2003年[亞視](../Page/亞視.md "wikilink")《[俠膽醫神](../Page/俠膽醫神.md "wikilink")》飾演梁挺
  - 2004年[衛視中文台](../Page/衛視中文台.md "wikilink")《[第8號當舖](../Page/第8號當舖.md "wikilink")》飾演高寒
  - 2004年[亞視](../Page/亞視.md "wikilink")《[我和殭屍有個約會](../Page/我和殭屍有個約會.md "wikilink")3》飾演徐流星，Nick
  - 2005年《[白色情人夢](../Page/白色情人夢.md "wikilink")》飾演童宇翔
  - 2005年[中國大陸](../Page/中國大陸.md "wikilink")《[徽娘宛心](../Page/徽娘宛心.md "wikilink")》飾演吳慧明
  - 2006年[衛視中文台](../Page/衛視中文台.md "wikilink")《[天使情人](../Page/天使情人.md "wikilink")》
  - 2006年[三立](../Page/三立.md "wikilink")《[愛情經紀約](../Page/愛情經紀約.md "wikilink")》飾演太司
  - 2008年[大愛](../Page/大愛.md "wikilink")《[情緣路](../Page/情緣路.md "wikilink")》
  - 2009年[NTV7](../Page/NTV7.md "wikilink")、[新傳媒](../Page/新傳媒.md "wikilink")
    《[快樂一家](../Page/快樂一家.md "wikilink")》
  - 2010年[新傳媒](../Page/新傳媒.md "wikilink")《[過好年](../Page/過好年.md "wikilink")》
  - 2010年[公視](../Page/公視.md "wikilink")《[藍海1加1](../Page/藍海1加1.md "wikilink")》
  - 2011年[台視](../Page/台視.md "wikilink")《[犀利人妻](../Page/犀利人妻.md "wikilink")》（客串）
  - 2012年[民視](../Page/民視.md "wikilink")《[廉政英雄](../Page/廉政英雄.md "wikilink")》飾
    楊明泰
  - 2012年[NTV7](../Page/NTV7.md "wikilink")《[庭外和解](../Page/庭外和解.md "wikilink")》飾
    洛有為
  - 2012年[三立](../Page/三立.md "wikilink")《[阿爸的願望](../Page/阿爸的願望_\(電視劇\).md "wikilink")》飾
    趙毅仙(杜敬學)
  - 2012年[中視](../Page/中視.md "wikilink")《[求愛365](../Page/求愛365.md "wikilink")》飾
    楊博文
  - 2014年[中視](../Page/中視.md "wikilink")《[月亮上的幸福](../Page/月亮上的幸福.md "wikilink")》飾
    李志瑋&周志玮

### 監製兼編劇作品

  - 2012年[中視週五偶像劇](../Page/中視.md "wikilink")《[求愛365](../Page/求愛365.md "wikilink")》

### 執導電影作品

  - 2015年《[野狼與瑪莉](../Page/野狼與瑪莉.md "wikilink")》(與[鄭健和聯合執導](../Page/鄭健和.md "wikilink"))
  - 2016年《[最完美的女孩](../Page/最完美的女孩.md "wikilink")》

### 專輯

  - 2003年 友一點愛《俠膽醫神-插曲》 （粵語） 演唱：[陳煒
    (香港)](../Page/陳煒_\(香港\).md "wikilink")/**金沛辰**
  - 2006年《愛情經紀約》-《想戀愛》 ([許紹洋](../Page/許紹洋.md "wikilink")/**金沛晟**)

## 外部連結

  -
  -
  -
  -
[category:馬來西亞男演員](../Page/category:馬來西亞男演員.md "wikilink")
[category:前亞洲電視藝員](../Page/category:前亞洲電視藝員.md "wikilink")
[C](../Page/category:莫姓.md "wikilink")

[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:海南裔馬來西亞人](../Category/海南裔馬來西亞人.md "wikilink")