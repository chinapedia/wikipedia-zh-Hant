**金獅湖**，原名**大埤**或**覆鼎金陂**，位於[高雄市](../Page/高雄市.md "wikilink")[三民區](../Page/三民區.md "wikilink")[鼎金里](../Page/鼎金里.md "wikilink")，[獅頭山下](../Page/林內山_\(三民區\).md "wikilink")，故名之。是[高雄](../Page/高雄.md "wikilink")[愛河上游一座湖泊](../Page/愛河.md "wikilink")，水域面積約12[甲](../Page/甲_\(單位\).md "wikilink")（11.6[公頃](../Page/公頃.md "wikilink")）\[1\]，雨季時有調節水位的重要功能。金獅湖水位受乾雨季影響，湖岸已闢成金獅湖公園，金獅湖不但是當地居民的休憩運動場所，也是高雄市民假日闔家休閒的去處之一。

## 發展沿革

金獅湖水源來自曹公新圳系的覆鼎金圳，在清代及日治初期時於西南面注入北圳、中圳、店子圳、尾厝圳等四圳，此四圳於船仔頭流入愛河，另外向南流入新陂（今本和里滯洪池一帶），再沿本館圳（今[明誠路](../Page/明誠路.md "wikilink")）流入愛河，金獅湖灌溉面積68甲（約66公頃），產[菱角與](../Page/菱角.md "wikilink")[芡實](../Page/芡.md "wikilink")\[2\]\[3\]。

[覆鼎金早期居民稱今](../Page/覆鼎金.md "wikilink")[高雄道德院後方之丘陵為虎頭山仔](../Page/高雄道德院.md "wikilink")，後來演變成獅山，或[獅頭山](../Page/林內山_\(三民區\).md "wikilink")，而大埤則座落在獅頭山西面與南面，故獅頭山又稱下水之金獅（因其為黃土丘陵，華人傳統以黃土喻金，含有吉祥之意）。[二戰後](../Page/臺灣戰後時期.md "wikilink")，政府將此地闢為風景區，居民逐稱呼此湖為金獅湖，稱此地為金獅湖風景區\[4\]\[5\]\[6\]\[7\]。

金獅湖公園為現行都市計畫灣子內地區的六號公園預定地\[8\]，包括水域面積約11公頃、陸域面積約14公頃，共25公頃\[9\]，園內有[高雄道德院](../Page/高雄道德院.md "wikilink")、[覆鼎金保安宮](../Page/覆鼎金保安宮.md "wikilink")、[獅頭山](../Page/獅頭山_\(三民區\).md "wikilink")、蝴蝶園等景點，1960年代曾有水上飯店及划船的遊憩資源，近年因夜間照明不足、噪音、水質欠佳等因素，使高雄市政府於2008年開始進行改善，包括停車場、親水設施、自行車道及步道、夜間照明、景觀美化等，另結合鄰近的[澄清湖及當時剛完工的](../Page/澄清湖.md "wikilink")[檨仔林埤濕地公園形成延續性的生態廊道](../Page/檨仔林埤濕地公園.md "wikilink")\[10\]。

## 周邊景點

  - 高雄道德院
  - 覆鼎金保安宮
  - [金獅湖蝴蝶園](http://chyfun.com/butterfly-garden-khh)

## 參考資料

[J](../Page/category:高雄市湖泊.md "wikilink")
[category:三民區](../Page/category:三民區.md "wikilink")

[J](../Category/高雄市旅遊景點.md "wikilink")

1.

2.
3.

4.
5.
6.  高雄市文獻委員會（1983）。高雄市舊地名探索。高雄市：高雄市政府民政局。

7.

8.  [擬定高雄市灣子內地區（旅館別墅區）細部計畫並配合變更主要計畫案](http://plan.kcg.gov.tw/KaoPlan_book/AB038107/AB038107.htm)


9.
10.