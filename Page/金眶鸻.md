**小環頸鸻**（学名*Charadrius
dubius*，鸻：音同“恒”）是一种小型[鸻科鸟](../Page/鸻科.md "wikilink")，候鸟，在[非洲过冬](../Page/非洲.md "wikilink")，其它时候则在[欧洲和](../Page/欧洲.md "wikilink")[亚洲西部栖息繁殖](../Page/亚洲.md "wikilink")。

## 特征

棕灰色的背和翅膀，白胸白肚子，脖子上围着一个黑圈，眼眶部位上有一个金色的圈。

## 食物

以[昆虫和蠕虫为食](../Page/昆虫.md "wikilink")。

## 繁殖

一般在淡水附近的石子地筑巢，巢边很少有植物。

## 图片

Charadrius_dubius_1_(Marek_Szczepanek).jpg
Charadrius_dubius_2_(Marek_Szczepanek).jpg
Charadrius_dubius_3_(Marek_Szczepanek).jpg Charadrius dubius
curonicus MHNT.jpg|

## 参考

  - Database entry includes justification for why this species is of
    least concern

## 外部链接

  - [金眶鸻](http://www.ebepe.com/html/redshank.html) 图片文档

[Category:鸻科](../Category/鸻科.md "wikilink")