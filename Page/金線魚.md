**金线鱼**，俗名金線鰱、黃線、紅三、红衫鱼，為[辐鳍鱼纲](../Page/辐鳍鱼纲.md "wikilink")[鲈形目](../Page/鲈形目.md "wikilink")[金线鱼科的其中一个](../Page/金线鱼科.md "wikilink")[种](../Page/种.md "wikilink")。

## 特徵

本魚軀體橢圓稍延長，體被小型櫛鱗。側線僅一條，完整，中位略呈弧形。胸鰭有鰭條6－7枚。眼後方，胸鰭基底上方有一塊朱紅色小斑點。尾鰭上葉延長為絲狀，但幼魚期較無明顯的延長。眼後至尾柄基部上方有一明險的金黃色縱帶。胸鰭基底甚長，硬棘不甚堅固。背鰭硬棘10枚、軟條9－10枚；臀鰭硬棘3枚、軟條6－8枚。體長可達35公分。

## 生態

本魚生活于水深40－220公尺。棲息在具沙泥底質海域，屬肉食性，以[甲殼類](../Page/甲殼類.md "wikilink")、[頭足類為食](../Page/頭足類.md "wikilink")。

## 分布

本魚分布於西[太平洋區](../Page/太平洋.md "wikilink")，包括[日本](../Page/日本.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國](../Page/中國.md "wikilink")[東海](../Page/東海.md "wikilink")、[南海](../Page/南海.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[越南](../Page/越南.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[澳洲等海域](../Page/澳洲.md "wikilink")。

## 經濟利用

金線魚為一具有相當經濟價值的[可食用魚類](../Page/可食用魚類.md "wikilink")，於其出產地區（典型例子為嶺南地區）為相當大眾化的食用魚（於廣東被稱為紅衫魚），適合包括清蒸、香煎、紅燒、煮湯在內各式各樣的烹調方式。但由於其捕撈方式主要為海底[拖网](../Page/拖网.md "wikilink")，族群數量在十年之間急劇下降三成，再加上因需求增加而[過度捕撈以及](../Page/過度捕撈.md "wikilink")[棲息地遭受污染](../Page/棲息地.md "wikilink")，其於[世界自然基金會出版的](../Page/世界自然基金會.md "wikilink")《海鮮選擇指引》之級別已由「想清楚才可食用」升至「避免食用」。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/chi/species.php?id=358_008)

[Category:食用魚](../Category/食用魚.md "wikilink")
[Category:澳洲魚類](../Category/澳洲魚類.md "wikilink")
[Category:中國魚類](../Category/中國魚類.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:印尼動物](../Category/印尼動物.md "wikilink")
[Category:日本魚類](../Category/日本魚類.md "wikilink")
[Category:韓國動物](../Category/韓國動物.md "wikilink")
[Category:澳門動物](../Category/澳門動物.md "wikilink")
[Category:馬來西亞動物](../Category/馬來西亞動物.md "wikilink")
[Category:菲律賓動物](../Category/菲律賓動物.md "wikilink")
[Category:台灣動物](../Category/台灣動物.md "wikilink")
[Category:越南動物](../Category/越南動物.md "wikilink")
[virgatus](../Category/金線魚屬.md "wikilink")