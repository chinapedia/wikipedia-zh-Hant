[Jin_weiying.jpg](https://zh.wikipedia.org/wiki/File:Jin_weiying.jpg "fig:Jin_weiying.jpg")
**金维映**（）\[1\]，清[宁波府](../Page/宁波府.md "wikilink")[定海县](../Page/定海县.md "wikilink")（现属[浙江省](../Page/浙江省.md "wikilink")[岱山县](../Page/岱山县.md "wikilink")）人。

## 早年

1904年8月16日，生於浙江省岱山縣。\[2\]1908年，由於天災，父親經營米店破產，後移居定海縣，其父在一招待所當賬房。\[3\]1913年，金維映進入縣立女子第一小學讀書。\[4\]少年時天資過人，好讀書，擅長演戲、演說，受到校長激進思想影響，從小嚮往革命。\[5\]

## 經歷

1926年，加入[中国共产党](../Page/中国共产党.md "wikilink")，为中共定海独立支部发起人之一。\[6\]当时宁波地委称其为“定海女将”。她是[红一方面军参加](../Page/红一方面军.md "wikilink")[长征的](../Page/长征.md "wikilink")30位女红军之一。1931年，和[邓小平相愛並結婚](../Page/邓小平.md "wikilink")。\[7\]是邓小平第二任妻子。1933年，因[“邓、毛、谢、古事件”和個人理由](../Page/“邓、毛、谢、古事件”.md "wikilink")，金維映与邓小平离婚，又與时任[中共中央组织部部长](../Page/中共中央组织部.md "wikilink")[李维汉結婚](../Page/李维汉.md "wikilink")。\[8\]令鄧小平痛苦萬分，在鄧小平生前，沒有人敢在鄧前提“金维映”三個字；鄧也只字未提金维映。金与李維漢在[長征途中育有一子名叫](../Page/長征.md "wikilink")[李铁映](../Page/李铁映.md "wikilink")。\[9\]

## 逝世及身後

[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[苏德战争爆发后](../Page/苏德战争.md "wikilink")，1941年在[莫斯科的](../Page/莫斯科.md "wikilink")[精神病院里](../Page/精神病院.md "wikilink")的金维映被德軍[轟炸機](../Page/轟炸機.md "wikilink")[空襲炸彈炸死](../Page/空襲.md "wikilink")，享年37歲。

出生地岱山縣[高亭镇後街弄](../Page/高亭镇.md "wikilink")14号（黄家）仍保留其故居。

## 參考資料

## 參見

  - [鄧小平](../Page/鄧小平.md "wikilink")
  - [李維漢](../Page/李維漢.md "wikilink")
  - [李鐵映](../Page/李鐵映.md "wikilink")

## 外部链接

  - [邓小平的第二任妻子金维映牺牲在莫斯科](http://www.beelink.com/20060223/2032317.shtml)，百灵网转载新浪文化
  - [钩沉：目睹贺子珍金维映在苏联学习的不幸经历](http://news.china.com/zh_cn/history/all/11025807/20051008/12141028.html)，中华网

[Category:中华人民共和国党和国家领导人的妻子](../Category/中华人民共和国党和国家领导人的妻子.md "wikilink")
[Category:中国共产党党员
(1926年入党)](../Category/中国共产党党员_\(1926年入党\).md "wikilink")
[Category:中华民国大陆时期中国共产党女党员](../Category/中华民国大陆时期中国共产党女党员.md "wikilink")
[Category:中华民国大陆时期战争身亡者](../Category/中华民国大陆时期战争身亡者.md "wikilink")
[Category:死在苏联的中华民国人](../Category/死在苏联的中华民国人.md "wikilink")
[W维映](../Category/金姓.md "wikilink")
[Category:定海人](../Category/定海人.md "wikilink")
[Category:岱山人](../Category/岱山人.md "wikilink")
[Category:邓小平家族](../Category/邓小平家族.md "wikilink")
[Category:李维汉家族](../Category/李维汉家族.md "wikilink")

1.  [《缅怀奶奶金维映烈士》“1904年8月16日，奶奶（金維映）出生在浙江舟山群岛第二大岛屿岱山的高亭镇（现属岱山县）。”](http://dangshi.people.com.cn/n/2015/0921/c85037-27613962.html)

2.  寒山碧原著，伊藤潔縮譯，唐建宇、李明翻譯：《鄧小平傳》，香港：東西文化事業公司，1993年1月

3.
4.
5.
6.
7.
8.
9.