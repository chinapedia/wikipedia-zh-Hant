**金色病毒科**（[學名](../Page/學名.md "wikilink")：Chrysoviridae），又译**黄色病毒科**，是一个第三型（class
III）的[科](../Page/科_\(生物\).md "wikilink")，屬於[真菌病毒](../Page/真菌病毒.md "wikilink")，主要宿主是[青黴菌属的真菌](../Page/青黴菌.md "wikilink")。本科的学名“Chrysoviridae”源自希腊语“”，意指黄绿色，病毒形狀為[二十面體且沒有包膜](../Page/二十面體.md "wikilink")，其基因組共長12.5kbp，由三個線型雙股RNA組成，共編碼四個蛋白質。金色病毒科有一个属**金色病毒屬**（*Chrysovirus*），一共包含9个[物种](../Page/物种.md "wikilink")，其中[模式種為青黴菌金色病毒](../Page/模式種.md "wikilink")（Penicillium
chrysogenum virus）\[1\]\[2\]\[3\]。

## 参考资料

[category:RNA病毒](../Page/category:RNA病毒.md "wikilink")

1.
2.
3.