**金莲花**（学名：*Trollius
chinensis*）属[毛茛科](../Page/毛茛科.md "wikilink")[金莲花属的一种植物](../Page/金莲花属.md "wikilink")。

## 形态

多年生[草本](../Page/草本.md "wikilink")，株高30cm－70cm。基生叶具长柄，五角形叶片，三全裂，中央裂片菱形，再三裂近中部，二回裂片有少数小裂片和锐齿。夏季开黄色花，花单生或2朵－3朵成聚伞花序，花瓣状萼片8枚－19枚，花瓣与萼片等长，狭条形。花期7月－8月。

## 分布

金莲花为稀有野生植物。在中国很多地方都有生长，但河北省承德市围场县坝上草原的金莲花色泽艳丽，花朵硕大，品相上乘。
内蒙古产金莲花花朵偏小，安徽黄山的金莲花花朵色泽相对偏暗。

喜冷凉湿润环境，多生长在海拔1800米以上的高山草甸或疏林地带。

## 用途

### 藥用

金莲花的最早药用记载始于[清](../Page/清朝.md "wikilink")[赵学敏所著](../Page/赵学敏.md "wikilink")《[本草纲目拾遗](../Page/本草纲目拾遗.md "wikilink")》，称其“治喉肿口疮、浮热牙宣、耳痛目疼”，“明目、解岚瘴”\[1\]。

### 工業用途

工业上可以此提取[黄酮类化合物作为](../Page/黄酮.md "wikilink")[抗氧化剂](../Page/抗氧化剂.md "wikilink")\[2\]。

## 其他

在台灣俗稱的金蓮花，實際上是[旱金莲](../Page/旱金莲.md "wikilink")。

## 参考文献

## 外部連結

  - [金蓮花1](http://www.pikespeakphoto.com/trollius.html)
  - [金蓮花2](http://www.flickr.com/photos/vsny/168689145/)
  - [金蓮花,
    Jinlianhua](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00644)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[category:毛茛科](../Page/category:毛茛科.md "wikilink")

[Category:草本花卉](../Category/草本花卉.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")

1.  [金莲花属植物的研究进展](http://www.cnki.com.cn/Article/CJFDTOTAL-SJKX200604009.htm)
    作者：朱殿龙； 丁万隆； 陈士林；《世界科学技术》2006年04期
2.  [金莲花中黄酮类化合物的提取及其抗氧化性研究](http://xuewen.cnki.net/CJFD-SPKX200306018.html)中国知网
    《食品科学》 2003年06期 作者：唐津忠； 鲁晓翔； 陈瑞芳； 天津商学院工学院实验中心