[Chicago_bot.jpg](https://zh.wikipedia.org/wiki/File:Chicago_bot.jpg "fig:Chicago_bot.jpg")的交易大廳\]\]

**金融衍生工具**（**Derivative**）是一种特殊类别买卖的[金融工具统称](../Page/金融工具.md "wikilink")。

这种买卖的回报率是根据一些其他[金融要素的表现情况衍生出来的](../Page/金融.md "wikilink")。比如[资产](../Page/资产.md "wikilink")（[商品](../Page/商品.md "wikilink")、[股票或](../Page/股票.md "wikilink")[债券](../Page/债券.md "wikilink")）、[利率](../Page/利率.md "wikilink")、[汇率或者各种指数](../Page/汇率.md "wikilink")（[股票指数](../Page/股票指数.md "wikilink")、[消費者物價指數以及天气指数](../Page/消費者物價指數.md "wikilink")）等。这些要素的表现将会决定一个衍生工具的回报率和回报时间。

衍生工具的主要类型有[期货](../Page/期货.md "wikilink")、[期权](../Page/期权.md "wikilink")、[权证](../Page/权证.md "wikilink")、[远期合约](../Page/远期合约.md "wikilink")、[掉期交易等](../Page/掉期交易.md "wikilink")，這些[期货](../Page/期货.md "wikilink")、[期权合約都能在市場上買賣](../Page/期权.md "wikilink")。

对此类金融工具进行买卖投资者需要十分谨慎，因为由其引起的损失有可能大于投资者最初投放于其中的资金。同时由于其本身并不代表任何资产，其买卖也不应该被视作投资。

## 用途

### 對沖

衍生金融工具其中一項用途，是用來轉移[風險](../Page/風險.md "wikilink")：採取跟[标的资产](../Page/标的资产.md "wikilink")（underlying
asset）相反的立場。例如，小麥農夫和磨坊主人訂定[期貨合約](../Page/期貨.md "wikilink")，在未來以現金買小麥。雙方因此都能減低風險：小麥農夫能確定價格，而磨坊主人則能確定小麥供應。

此外，[股票指數期貨及](../Page/股票指數.md "wikilink")[期權被稱為衍生產品](../Page/期權.md "wikilink")，因為他們的存在，是從市場的實際指標衍生出來，並沒有自己的內在特點。不僅如此，有些人認為他們會導致更大的市場波動，原因之一是，只要利用相對少量的保證金或認股證費用，就能控制一個龐大金額的證券。衍生工具的流行原因之一是，它們的交易在某些經濟體系之下，可以不在[資產負債表中列出來](../Page/資產負債表.md "wikilink")。

### 投機和套利

炒家除了和其他炒家交易之外，也會和對冲的人交易。除了直接投機之外，衍生工具交易也能提供[套利機會](../Page/套利.md "wikilink")──不同衍生工具有相同或極為相似的指定證券。

炒家除了會投機升跌，還會投機[波幅](../Page/波幅.md "wikilink")。期權多用作炒波幅。

在1995年時，衍生金融工具投機得到臭名遠播。[霸菱銀行的一名交易員](../Page/霸菱銀行.md "wikilink")[尼克·李森](../Page/尼克·李森.md "wikilink")，造成14億美元的損失，令這家有數百年歷史的金融機構破產。

## 衍生工具的種類

### 場外交易和交易所買賣

大致來說，場外交易和交易所買賣是兩個截然不同的的衍生工具合約，区別在於他們的交易場所：

#### 場外交易衍生工具

[場外交易衍生工具](../Page/場外交易.md "wikilink")，是買賣雙方之間（私下協商）直接交易的合約，無須通過交易場或其他中介機構。產品如[掉期交易](../Page/掉期交易.md "wikilink")、[遠期利率協議以及](../Page/遠期利率協議.md "wikilink")（exotic
options，相对于vanilla options普通期权）幾乎都是用場外交易這種交易方式。

#### 交易所買賣之衍生工具（ETD）

交易所買賣之衍生工具（ETD），顧名思義，是通過[期貨交易所或其他交易所買賣的衍生工具](../Page/期貨交易所.md "wikilink")。衍生品交易所是一個中介機構，向交易雙方收取[孖展](../Page/孖展.md "wikilink")（基本按金）作為擔保。

## 参看

  - [资本市场](../Page/资本市场.md "wikilink")
  - [金融工程学](../Page/金融工程学.md "wikilink")
  - [计量金融](../Page/计量金融.md "wikilink")
  - [投资银行](../Page/投资银行.md "wikilink")
  - [股本衍生工具](../Page/股本衍生工具.md "wikilink")
  - [連動式債券](../Page/連動式債券.md "wikilink")

## 参考资料

  - [浅析我国金融衍生品发展状况](http://www.ce.cn/cysc/newmain/yc/jsxw/201211/29/t20121129_21292941.shtml)
    中国经济网 2014-05-25

[Category:金融工程学](../Category/金融工程学.md "wikilink")
[\*](../Category/金融衍生工具.md "wikilink")