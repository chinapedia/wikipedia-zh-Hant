**金蟾桂**，[中國](../Page/中國.md "wikilink")[清朝武官官員](../Page/清朝.md "wikilink")，[江蘇](../Page/江蘇.md "wikilink")[松江府](../Page/松江府.md "wikilink")[华亭县](../Page/华亭县.md "wikilink")（今属[上海市](../Page/上海市.md "wikilink")）人。

## 生平

乾隆十三年（1748年）戊辰科武進士出身。歷官金門鎮標右營守衛\[1\]、福建水師提標後營遊擊\[2\]，乾隆三十五年（1770年）任督標水師營[參將](../Page/參將.md "wikilink")\[3\]，護理閩安協副將\[4\]，乾隆三十八年（1773年）升福建台灣協水師副將\[5\]。乾隆四十年（1775年）升福建金門鎮總兵\[6\]。乾隆四十七年（1782年）任福建[台灣鎮總兵](../Page/台灣鎮.md "wikilink")\[7\]。次年，則因無法制止[漳泉械鬥遭](../Page/漳泉械鬥.md "wikilink")[閩浙總督](../Page/閩浙總督.md "wikilink")[富勒浑彈劾](../Page/富勒浑.md "wikilink")，革職查辦。\[8\]

## 註釋

## 參考文獻

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。

{{-}}

[Category:台灣水師協副將](../Category/台灣水師協副將.md "wikilink")
[Category:台灣鎮總兵](../Category/台灣鎮總兵.md "wikilink")
[Category:松江人](../Category/松江人.md "wikilink")
[Chan](../Category/金姓.md "wikilink")

1.  中央研究院歷史語言研究所內閣大庫檔案 ,032337號
2.  中央研究院歷史語言研究所內閣大庫檔案 ,032337號
3.  中央研究院歷史語言研究所內閣大庫檔案 ,082889號
4.  中央研究院歷史語言研究所內閣大庫檔案 ,029141號
5.  高宗純皇帝實錄 ,984卷
6.  高宗純皇帝實錄 ,1154卷
7.  明清史料 ,己10 ,950
8.  《清史稿·卷332》：（富勒浑）复授闽浙总督。台湾漳、泉民械斗，劾总兵金蟾桂、知府苏泰等，并夺官。