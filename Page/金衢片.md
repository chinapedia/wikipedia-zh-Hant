**金衢片**，是[吴语的一个方言片](../Page/吴语.md "wikilink")，主要分布在[金衢盆地](../Page/金衢盆地.md "wikilink")，人口620万，占吴语人口总数的12.4%。
在舊分区法中，**金衢片**的大部分地区属于[婺州片](../Page/婺州片.md "wikilink")，其余地区属于[处衢片](../Page/处衢片.md "wikilink")。

**金衢片**內部差異較大，有些縣之間不能通話\[1\]。其中[金华话和](../Page/金华话.md "wikilink")[兰溪话较接近](../Page/兰溪话.md "wikilink")\[2\]。

## 使用区域

金衢片方言的使用人口约595万，分佈於以下地区\[3\]： 　　

  - [杭州市](../Page/杭州市.md "wikilink")：[建德市的](../Page/建德市.md "wikilink")[乾潭鎮姚村](../Page/乾潭鎮.md "wikilink")、[大慈岩镇及](../Page/大慈岩镇.md "wikilink")[航头镇的梅岭](../Page/航头镇_\(建德市\).md "wikilink")、珏塘、石木岭、宙坞源等村，[淳安县南部与](../Page/淳安县.md "wikilink")[衢州市](../Page/衢州市.md "wikilink")[衢江区交界的少数村](../Page/衢江区.md "wikilink")，如[安阳乡乌龙](../Page/安阳乡_\(淳安县\).md "wikilink")、黄家源、红山岙、㟪岭村，[大墅镇田门宅村](../Page/大墅镇_\(淳安县\).md "wikilink")。
    　　
  - [金华市](../Page/金华市.md "wikilink")：市辖区（[婺城区](../Page/婺城区.md "wikilink")、[金东区](../Page/金东区.md "wikilink")），[兰溪市](../Page/兰溪市.md "wikilink")，[永康市](../Page/永康市.md "wikilink")，[义乌市](../Page/义乌市.md "wikilink")（[大陈镇北部的红峰](../Page/大陈镇_\(义乌市\).md "wikilink")、燕窝二村除外），[东阳市](../Page/东阳市.md "wikilink")，[浦江县](../Page/浦江县.md "wikilink")，[武义县](../Page/武义县.md "wikilink")（原[宣平县地区除外](../Page/宣平縣_\(浙江\).md "wikilink")），[磐安县](../Page/磐安县.md "wikilink")（[胡宅乡](../Page/胡宅乡.md "wikilink")、[方前镇](../Page/方前镇.md "wikilink")、[高二乡除外](../Page/高二乡.md "wikilink")）。
    　　
  - [衢州市](../Page/衢州市.md "wikilink")：市辖区（[柯城区](../Page/柯城区.md "wikilink")、[衢江区](../Page/衢江区.md "wikilink")），[龙游县](../Page/龙游县.md "wikilink")。
    　　
  - [丽水市](../Page/丽水市.md "wikilink")：[缙云县](../Page/缙云县.md "wikilink")。

## 分区理论

**金衢片**这个概念在[曹志耘的](../Page/曹志耘.md "wikilink")《南部吴语语音研究》一书中首次提出。旧分区理论把[金华](../Page/金华.md "wikilink")、[衢州](../Page/衢州.md "wikilink")、[丽水地区的方言分为](../Page/丽水.md "wikilink")[婺州片](../Page/婺州片.md "wikilink")、[处衢片](../Page/处衢片.md "wikilink")，而曹志耘将其分为**金衢片**和[上丽片](../Page/上丽片.md "wikilink")。

据《衢州市志》（1994），衢州地区的方言可以分为两小片，一是龙游、衢州，二是[开化](../Page/开化.md "wikilink")、[常山](../Page/常山.md "wikilink")、[江山](../Page/江山.md "wikilink")，各片内部十分接近（衢州城里和乡下的方言有差别）。曹志耘认为：从具体的语言特征来看，龙游、衢州、缙云方言具有许多重要的跟金华地区相同相近，但跟上丽片不同的地方。因而把龙游、衢州、缙云划为金衢片，更加符合这三个方言的自身特点\[4\]。

## 语音特点

### 帮端母

金衢片帮端母特殊读音的特点是以读鼻音\[m n/ȵ\]声母为主，也有\[ʔb
ʔd/l\]等特殊声母。读鼻音声母（不包括“打”字读\[n\]声母）的只分布于金华绝大部分乡下、[汤溪](../Page/汤溪县.md "wikilink")、[义乌](../Page/义乌.md "wikilink")、[东阳](../Page/东阳.md "wikilink")、[磐安](../Page/磐安.md "wikilink")、[永康](../Page/永康市.md "wikilink")、[武义和](../Page/武义.md "wikilink")[缙云](../Page/缙云.md "wikilink")。[浦江](../Page/浦江.md "wikilink")、金华朱基头只有“打”一字读\[n\]声母。在金华城里、[兰溪](../Page/兰溪.md "wikilink")、[龙游](../Page/龙游.md "wikilink")、[衢州](../Page/衢州.md "wikilink")，帮端母字没有特殊读音\[5\]。

### 尖团

金衢片大部分地区都分[尖团](../Page/尖团音.md "wikilink")，只有[龙游](../Page/龙游.md "wikilink")、[衢州](../Page/衢州.md "wikilink")、金华城里年轻人、[武义城里不分尖团](../Page/武义.md "wikilink")\[6\]。

## 注释

## 来源

<references>

\[7\] \[8\] \[9\]

</references>

## 相关条目

  - [吴语](../Page/吴语.md "wikilink")
  - [吴语方言](../Page/吴语方言.md "wikilink")
  - [浙江方言](../Page/浙江方言.md "wikilink")

[Category:吳語](../Category/吳語.md "wikilink")

1.
2.
3.
4.
5.
6.
7.

8.

9.