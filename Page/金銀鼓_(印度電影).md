***金銀鼓***（***Dhol***）是2007年的[印度電影](../Page/印度.md "wikilink")。
主要由一群年輕演員出演，該影片的Malayalam語版本獲得成功，而[印地語版本則表現平平](../Page/印地語.md "wikilink")。

## 故事梗概

有四個游手好閑的朋友們，整天幻想不消奮鬥即發財， 有一天「機會」終於等來啦！他們住處的鄰居搬來了一個財團的大老闆，還有個美麗的外孫女。

於是四人開始即合作又互相拆臺的行動，想要其中一人得到闊老闆的孫女麗塗（Ritu）。麗塗她們搬來的目的卻是要找到失縱了半年的哥哥拉胡（Rahul）的下落，於是她利用他們去尋哥哥下落，直到一個舞女提供了情報，他們總算接近事實的真相，最後犯罪集團出現了，他們合為一群勇敢鬥爭，終於保護了證據、抓到壞人，而且麗塗姑娘還為他們每個人留下一些錢作生意資本，說一年后回來看哪個最有錢就嫁給誰。。。

## 卡士表

  - [Sharman Joshi](../Page/Sharman_Joshi.md "wikilink")
  - [Tusshar Kapoor](../Page/Tusshar_Kapoor.md "wikilink")
  - [Kunal Khemu](../Page/Kunal_Khemu.md "wikilink")
  - [Rajpal Yadav](../Page/Rajpal_Yadav.md "wikilink")
  - [Payal Rohatgi](../Page/Payal_Rohatgi.md "wikilink")
  - [鄲奴什麗·杜塔](../Page/鄲奴什麗·杜塔.md "wikilink") ... [Tanushree
    Dutta](../Page/Tanushree_Dutta.md "wikilink") ... 扮演麗塗（Ritu）

## 電影歌曲

  - *O Yaara dhol bajake* - [Mika
    Singh](../Page/Mika_Singh.md "wikilink")
  - *Namakool*- [Shaan](../Page/Shaan.md "wikilink") and [Kunal
    Ganjawala](../Page/Kunal_Ganjawala.md "wikilink")
  - *O Yaara dhol bajake* - [Labh
    Janjua](../Page/Labh_Janjua.md "wikilink") \[Remixed by [DJ
    Nikhil](../Page/DJ_Nikhil.md "wikilink") and [DJ
    Naved](../Page/DJ_Naved.md "wikilink")\]
  - *Haadsa*- [Sunidhi Chauhan](../Page/Sunidhi_Chauhan.md "wikilink")
    and [Akriti Kakkar](../Page/Akriti_Kakkar.md "wikilink")
  - ''Bheega Aasman- [Shaan](../Page/Shaan.md "wikilink") and [Vijay
    Yesudas](../Page/Vijay_Yesudas.md "wikilink")
  - *All Night Long*- [Usha Uthup](../Page/Usha_Uthup.md "wikilink")
  - ''Dil Liya Dil Liya - [Shreya
    Ghoshal](../Page/Shreya_Ghoshal.md "wikilink")
  - *O Yaara dhol bajake* - [Soham
    Chakraborty](../Page/Soham_Chakraborty.md "wikilink") and [Suhail
    Kaul](../Page/Suhail_Kaul.md "wikilink")

## 外部連結

  -
  - [**Dhol**的影評一則](http://www.indicine.com/movies/bollywood/dhol-movie-review/)

[D](../Category/印度電影作品.md "wikilink")
[D](../Category/2007年電影.md "wikilink")