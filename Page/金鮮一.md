**金鮮一**（****，一些中文媒體錯誤譯作「**金善日**」；），[韓國公民](../Page/韓國.md "wikilink")，在为[美军提供服务的](../Page/美军.md "wikilink")[韩国公司中擔任](../Page/大韩民国.md "wikilink")[英语翻译](../Page/英语.md "wikilink")，他在[伊拉克遭到武装分子劫持](../Page/伊拉克.md "wikilink")，在韩国政府拒绝武装分子的撤军要求后遭到[斬首](../Page/斬首.md "wikilink")。

金鮮一是家里8个孩子中唯一的儿子，排行第七，為一名虔誠[基督徒](../Page/基督徒.md "wikilink")。他曾经就讀[龙山大学及](../Page/龙山大学.md "wikilink")[庆星大学](../Page/庆星大学.md "wikilink")，入讀神學院。神學畢業後，2003年2月入讀[韓國外國語大學修讀](../Page/韓國外國語大學.md "wikilink")[阿拉伯語學士學位](../Page/阿拉伯語.md "wikilink")。他在2003年6月15日前往[中東擔任](../Page/中東.md "wikilink")[宣教士](../Page/宣教士.md "wikilink")，以傳譯員身份進入伊拉克工作，並向伊拉克人傳播基督教。

2004年5月30日，他在距離[巴格達市以西](../Page/巴格達.md "wikilink")50公里的[費盧杰被俘](../Page/費盧杰.md "wikilink")。其後縛匪多次向韓國政府要求撤銷將[宰桐部隊派至伊拉克的決定](../Page/宰桐部隊.md "wikilink")，但消息都在中途被扣留，結果金鮮一於2004年6月22日被害。他死後，韓國限制該國公民前往中東等伊斯蘭教國家宣傳基督教。\[1\]

金鮮一的喪禮於2004年6月30日於[釜山舉行](../Page/釜山.md "wikilink")，遺照旁掛著一塊以英文、阿拉伯文和韓文寫成的橫額：「我愛伊拉克」。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 参见

  - [香田証生](../Page/香田証生.md "wikilink")
  - [2007年塔利班挾持韓國人質事件](../Page/2007年塔利班挾持韓國人質事件.md "wikilink")

[Category:韓國新教徒](../Category/韓國新教徒.md "wikilink")
[Category:被處決的韓國人](../Category/被處決的韓國人.md "wikilink")
[Category:基督教殉道者](../Category/基督教殉道者.md "wikilink")
[Category:被斬首者](../Category/被斬首者.md "wikilink")
[Sun](../Category/金姓.md "wikilink")
[Category:人质](../Category/人质.md "wikilink")

1.  [韓國要嚴管公民海外傳教 環球時報](http://www.mzb.com.cn/html/report/183710-2.htm)