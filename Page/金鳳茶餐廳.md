[HK_Wan_Chai_春園街_Spring_Garden_Lane_night_金鳳茶餐廳_Kam_Fung_Cafe_1.jpg](https://zh.wikipedia.org/wiki/File:HK_Wan_Chai_春園街_Spring_Garden_Lane_night_金鳳茶餐廳_Kam_Fung_Cafe_1.jpg "fig:HK_Wan_Chai_春園街_Spring_Garden_Lane_night_金鳳茶餐廳_Kam_Fung_Cafe_1.jpg")
[HK_WC_Spring_Garden_Lane_Kam_Fung_Cafe_Milk_Tea.jpg](https://zh.wikipedia.org/wiki/File:HK_WC_Spring_Garden_Lane_Kam_Fung_Cafe_Milk_Tea.jpg "fig:HK_WC_Spring_Garden_Lane_Kam_Fung_Cafe_Milk_Tea.jpg")
**金鳳茶餐廳**，位於[香港](../Page/香港.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[春園街](../Page/春園街.md "wikilink")41號，是一家歷史悠久的傳統港式[茶餐廳](../Page/茶餐廳.md "wikilink")，於1956年開業。

金鳳原來位於[太原街](../Page/太原街.md "wikilink")，20世紀90年代遷往現址。

金鳳供應一般傳統茶餐廳食品，包括[蛋撻](../Page/蛋撻.md "wikilink")、[椰撻](../Page/椰撻.md "wikilink")、[雞批](../Page/雞批.md "wikilink")、[菠蘿包等](../Page/菠蘿包.md "wikilink")，尤其以不加[冰的凍](../Page/冰.md "wikilink")[奶茶](../Page/奶茶.md "wikilink")、[咖啡最為出名](../Page/咖啡.md "wikilink")。

金鳳的奶茶沿用金玫瑰紅茶配合[立頓錫蘭紅茶](../Page/立頓.md "wikilink")，茶葉沖泡一遍即棄，避免茶味變澀。淡奶採用荷蘭黑白奶\[1\]。

## 資料來源

## 站外連結

  - [開飯喇 - 金鳳茶餐廳](http://www.openrice.com/big5/sr2.htm?shopid=1549)
  - [搵食易 -
    金鳳茶餐廳食評](https://web.archive.org/web/20070930154731/http://www.foodeasy.com/hk/rest_detail.php?id_rest=4603)
  - [金鳳茶餐廳。叮叮-
    香港電車地圖。香港旅遊發展局網站](https://web.archive.org/web/20081205134251/http://www.discoverhongkong.com/tramguide/tc/merchant_detail.jsp?spot_id=51)

[Category:香港茶餐廳](../Category/香港茶餐廳.md "wikilink")
[Category:灣仔](../Category/灣仔.md "wikilink")

1.