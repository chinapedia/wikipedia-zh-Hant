'''釜山IPark
'''（）是由[大宇集团於](../Page/大宇集团.md "wikilink")1983年在[韓國](../Page/韓國.md "wikilink")[釜山成立的](../Page/釜山.md "wikilink")。球隊原稱「大宇」（后更名大宇皇家），是[K聯賽的創始俱樂部之一](../Page/K聯賽.md "wikilink")。[1985–86年亚洲球会锦标赛](../Page/1985–86年亚洲球会锦标赛.md "wikilink")，大宇皇家以3-1擊敗[沙烏地阿拉伯的](../Page/沙烏地阿拉伯.md "wikilink")[-{zh-hans:阿赫利;
zh-hk:吉達艾阿里; zh-tw:
阿爾阿里;}-](../Page/吉達艾阿里體育會.md "wikilink")，成為韓國職業足球隊中，第一個奪下[亞洲冠軍盃的球隊](../Page/亞洲冠軍聯賽.md "wikilink")。

## 历史

球队前身为大宇队，他们在1983年K聯賽第一個賽季奪下亞軍，排名次於[哈利路亞](../Page/安山哈利路亞FC.md "wikilink")。球隊在1984年轉形成職業足球俱樂部；並且改名為「大宇皇家」。改名之後的大宇隊立刻奪下1984年的聯賽冠軍，球隊在1987年及1991年再度奪下聯賽冠軍。在1995年為了響應K聯賽的地方化，球隊在大宇皇家前面冠上「釜山」兩字，成為「釜山大宇皇家」。二度更名後的球隊在1997年拿下隊史上第四座聯賽冠軍。

由於大宇集團在90年代末遭逢財務危機，球隊經營權就易主給I'Park建設，這是一家韓國國內的建設公司，屬於[現代集團旗下](../Page/現代集團.md "wikilink")。球隊也因為經營權易主所以再度更名，成為「釜山I'Cons」（「Cons」代表[英文的](../Page/英文.md "wikilink")「建設」，而该球队名称的误译**釜山偶像**是自“icons”一词翻译而来）。2005年賽季球隊更名为「釜山I'Park」，在2012年賽季球隊再度更名，這一次則是變成了今天所使用的名字「釜山IPark」。

釜山隊在2000年之後的賽季遭遇許多不順，不過2004年在[蘇格蘭裔教練](../Page/蘇格蘭.md "wikilink")[伊恩·波特菲尔德帶領之下](../Page/伊恩·波特菲尔德.md "wikilink")，球隊贏得[韓國足總盃冠軍](../Page/韓國足總盃.md "wikilink")。六個月之後，釜山隊接著之前的氣勢贏得2005年賽季上半季的冠軍。不過下半季卻發生大逆轉，球隊積分墊底。球隊最後在季後賽四強也不敵[仁川聯](../Page/仁川聯.md "wikilink")。雖然2005年在國內下半季成績慘澹，但2005年釜山隊打入到[亚洲冠军联赛四強](../Page/亚洲冠军联赛.md "wikilink")。

釜山隊於2015年降班至[挑戰K聯賽](../Page/挑戰K聯賽.md "wikilink")。

## 榮譽

  - [K聯賽](../Page/K聯賽.md "wikilink")：1984, 1987, 1991, 1997
  - [韓國足總杯](../Page/韓國足總杯.md "wikilink")：2004
  - [adidas杯](../Page/adidas.md "wikilink")：1997
  - [韓國聯賽杯](../Page/韓國聯賽杯.md "wikilink")：1997, 1998
  - [亚洲冠军联赛](../Page/亚洲冠军联赛.md "wikilink")：1986

## 贊助商

**體育用品**

  - 1997-1999：[Rapido](../Page/Rapido.md "wikilink")
  - 2000-2003：[Nike](../Page/Nike.md "wikilink")
  - 2004：[Kappa](../Page/Kappa_\(公司\).md "wikilink")
  - 2005-2007：[Hummel](../Page/Hummel.md "wikilink")

**球衣廣告**

  - 1997-1999：[Leganza](../Page/Leganza.md "wikilink")
  - 2000-2003：[I'Park](../Page/I'Park.md "wikilink")
  - 2004：Dynamic Busan（活力[釜山](../Page/釜山.md "wikilink")）
  - 2005：Dynamic
    Busan、[釜山](../Page/釜山.md "wikilink")[APEC](../Page/APEC.md "wikilink")2005年及[I'Park](../Page/I'Park.md "wikilink")
  - 2006：[I'Park](../Page/I'Park.md "wikilink")

## 外部連結

  - [官方網站](http://www.busanipark.com/)

  - [Facebook](http://www.facebook.com/bsipark)

  - [Twitter](http://twitter.com/BusaniparkFC)

[Category:韩国足球俱乐部](../Category/韩国足球俱乐部.md "wikilink")
[Category:1983年建立的足球俱樂部](../Category/1983年建立的足球俱樂部.md "wikilink")