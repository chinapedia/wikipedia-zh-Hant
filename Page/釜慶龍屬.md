**釜慶龍屬**（[屬名](../Page/屬.md "wikilink")：）是[巨龍形類](../Page/巨龍形類.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[下白堊紀的](../Page/下白堊紀.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")。牠的[化石是在](../Page/化石.md "wikilink")[韓國南部的](../Page/韓國.md "wikilink")[慶善盆地發現](../Page/慶善盆地.md "wikilink")，包括一系列[頸椎及](../Page/頸椎.md "wikilink")[背椎](../Page/背椎.md "wikilink")。釜慶龍與[盤足龍是近親](../Page/盤足龍.md "wikilink")。

[模式種](../Page/模式種.md "wikilink")**千禧釜慶龍**（*P.
millenniumi*）是由[中國的](../Page/中國.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[董枝明](../Page/董枝明.md "wikilink")、韓國的[白仁成及](../Page/白仁成.md "wikilink")[金賢珠在](../Page/金賢珠.md "wikilink")2000年所描述、命名。

## 參考

  - Dong Zhiming, Paik In Sung & Kim Hyun Joo (2001). "A preliminary
    report on a sauropod from the Hasandong Formation (Lower
    Cretaceous), Korea." *Proceedings of the Eighth Annual Meeting of
    the Chinese Society of Vertebrate Paleontology* (eds.: Deng Tao &
    Wang Yuan): 41-53, 6 figs., 3 pls.; Beijing (China Ocean Press).

## 外部連結

  - [恐龍博物館──釜慶龍](https://web.archive.org/web/20071109012423/http://www.dinosaur.net.cn/museum/Pukyongosaurus.htm)

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:大鼻龍類](../Category/大鼻龍類.md "wikilink")