**針尾蛇屬**（[學名](../Page/學名.md "wikilink")：*Uropeltis*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[盾尾蛇科下的一個](../Page/盾尾蛇科.md "wikilink")[屬](../Page/屬.md "wikilink")，主要包括分布於[印度及](../Page/印度.md "wikilink")[斯里蘭卡一帶的無毒蛇種](../Page/斯里蘭卡.md "wikilink")。目前共有23個品種已被確認。\[1\]

## 地理分布

針尾蛇主要分布於南[印度及](../Page/印度.md "wikilink")[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")。\[2\]

## 品種

| 品種\[3\]                                                                 | 學名及命名者\[4\]                                         | 異稱      | 地理分布\[5\]                          |
| ----------------------------------------------------------------------- | --------------------------------------------------- | ------- | ---------------------------------- |
| **[窄頭針尾蛇](../Page/窄頭針尾蛇.md "wikilink")**                                | Uropeltis arcticeps，<small>Günther，1875</small>     |         |                                    |
| **[白氏針尾蛇](../Page/白氏針尾蛇.md "wikilink")**                                | Uropeltis beddomii，<small>Günther，1862</small>      |         |                                    |
| **[南印針尾蛇](../Page/南印針尾蛇.md "wikilink")**                                | Uropeltis broughami，<small>Beddome，1878</small>     |         |                                    |
| **[針尾蛇](../Page/針尾蛇.md "wikilink")**<font size="-1"><sup>T</sup></font> | Uropeltis ceylanica，<small>Cuvier，1829</small>      | 卡氏針尾蛇   | 南[印度一帶](../Page/印度.md "wikilink")  |
| **[丁迪古爾針尾蛇](../Page/丁迪古爾針尾蛇.md "wikilink")**                            | Uropeltis dindigalensis，<small>Beddome，1877</small> |         |                                    |
| **[馬都拉針尾蛇](../Page/馬都拉針尾蛇.md "wikilink")**                              | Uropeltis ellioti，<small>Gray，1858</small>          |         |                                    |
| **[馬拉巴爾針尾蛇](../Page/馬拉巴爾針尾蛇.md "wikilink")**                            | Uropeltis liura，<small>Günther，1875</small>         | 甘氏針尾蛇   |                                    |
| **[大鱗針尾蛇](../Page/大鱗針尾蛇.md "wikilink")**                                | Uropeltis macrolepis，<small>Peters，1862</small>     | 孟買針尾蛇   |                                    |
| **[大吻針尾蛇](../Page/大吻針尾蛇.md "wikilink")**                                | Uropeltis macrorhyncha，<small>Beddome，1877</small>  |         | 南[印度](../Page/印度.md "wikilink")    |
| **[塊斑針尾蛇](../Page/塊斑針尾蛇.md "wikilink")**                                | Uropeltis maculata，<small>Beddome，1878</small>      |         |                                    |
| **[黑腹針尾蛇](../Page/黑腹針尾蛇.md "wikilink")**                                | Uropeltis melanogaster，<small>Gray，1858</small>     | 格雷針尾蛇   |                                    |
| **[密氏針尾蛇](../Page/密氏針尾蛇.md "wikilink")**                                | Uropeltis myhendrae，<small>Beddome，1886</small>     | 條紋針尾蛇   |                                    |
| **[閃光針尾蛇](../Page/閃光針尾蛇.md "wikilink")**                                | Uropeltis nitida，<small>Beddome，1878</small>        | 交趾針尾蛇   | 南印度                                |
| **[眼斑針尾蛇](../Page/眼斑針尾蛇.md "wikilink")**                                | Uropeltis ocellata，<small>Beddome，1863</small>      | 尼爾吉里針尾蛇 | 南印度                                |
| **[彼得針尾蛇](../Page/彼得針尾蛇.md "wikilink")**                                | Uropeltis petersi，<small>Beddome，1878</small>       |         |                                    |
| **[菲力浦針尾蛇](../Page/菲力浦針尾蛇.md "wikilink")**                              | Uropeltis phillipsi，<small>Nicholls，1929</small>    |         |                                    |
| **[菲普遜針尾蛇](../Page/菲普遜針尾蛇.md "wikilink")**                              | Uropeltis phipsonii，<small>Mason，1888</small>       |         |                                    |
| **[普爾尼針尾蛇](../Page/普爾尼針尾蛇.md "wikilink")**                              | Uropeltis pulneyensis，<small>Beddome，1863</small>   |         |                                    |
| **[紅線針尾蛇](../Page/紅線針尾蛇.md "wikilink")**                                | Uropeltis rubrolineata，<small>Günther，1875</small>  |         | 南印度                                |
| **[紅斑針尾蛇](../Page/紅斑針尾蛇.md "wikilink")**                                | Uropeltis rubromaculata，<small>Beddome，1867</small> |         | 南印度                                |
| **[斯里蘭卡針尾蛇](../Page/斯里蘭卡針尾蛇.md "wikilink")**                            | Uropeltis ruhunae，<small>Deraniyagala，1954</small>  |         | [斯里蘭卡](../Page/斯里蘭卡.md "wikilink") |
| **[史密斯針尾蛇](../Page/史密斯針尾蛇.md "wikilink")**                              | Uropeltis smithi，<small>Gans，1966</small>           | 紫針尾蛇    |                                    |
| **[伍氏針尾蛇](../Page/伍氏針尾蛇.md "wikilink")**                                | Uropeltis woodmasoni，<small>Theobald，1876</small>   |         |                                    |
|                                                                         |                                                     |         |                                    |

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：針尾蛇屬](http://reptile-database.reptarium.cz/search.php?&genus=Uropeltis&submit=Search)

  - [Systema
    Naturae 2000：針尾蛇屬](http://sn2000.taxonomy.nl/Main/Classification/49909.htm)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:盾尾蛇科](../Category/盾尾蛇科.md "wikilink")

1.

2.
3.
4.
5.