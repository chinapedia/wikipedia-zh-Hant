**釣魚翁郊遊徑**是[香港的一條郊遊徑](../Page/香港.md "wikilink")，位於[清水灣郊野公園之內](../Page/清水灣郊野公園.md "wikilink")，亦為該公園中其中一條[漁農自然護理署認可的行山路徑](../Page/漁農自然護理署.md "wikilink")（另一條為[龍蝦灣郊遊徑](../Page/龍蝦灣郊遊徑.md "wikilink")）。釣魚翁郊遊徑建於[清水灣半島眾山的](../Page/清水灣半島.md "wikilink")[山脊和](../Page/山脊.md "wikilink")[山腰上](../Page/山腰.md "wikilink")，北起[五塊田](../Page/五塊田.md "wikilink")，南至[大廟坳](../Page/大廟坳.md "wikilink")，途經[上洋山](../Page/上洋山.md "wikilink")、[下洋山](../Page/下洋山.md "wikilink")、[廟仔墩](../Page/廟仔墩.md "wikilink")、[釣魚翁](../Page/釣魚翁.md "wikilink")、[岩下堂](../Page/岩下堂.md "wikilink")、[蝦山篤和](../Page/蝦山篤.md "wikilink")[田下山](../Page/田下山.md "wikilink")。

郊遊徑大部分路段相當平坦好走，惟末段需登上田下山及下降到大廟坳，較需費勁。由於五塊田至蝦山篤一段的部分路段跟[釣魚翁越野單車徑重疊](../Page/釣魚翁越野單車徑.md "wikilink")，遊人如在山徑上遇到[單車的話要小心和禮讓](../Page/單車.md "wikilink")。郊遊徑中途有多個退出點，可返回[清水灣道](../Page/清水灣道.md "wikilink")、[孟公屋和](../Page/孟公屋.md "wikilink")[大坳門道](../Page/大坳門道.md "wikilink")。路徑的沿途[流動電話網絡接收良好](../Page/流動電話.md "wikilink")。遊人如中途感到體力不支或不適，可自行提早離開或致電求救。

釣魚翁郊遊徑沿途景觀開揚，東面可見[西貢區](../Page/西貢區.md "wikilink")[牛尾海至](../Page/牛尾海.md "wikilink")[清水灣一帶的優美風景](../Page/清水灣.md "wikilink")，西面則為[將軍澳的市區景色](../Page/將軍澳.md "wikilink")，兩者形成強烈的對比。在登上田下山的一段，可近距離看到[大赤沙運作中的](../Page/大赤沙.md "wikilink")[新界東南堆填區](../Page/新界東南堆填區.md "wikilink")，令該段郊遊徑失色不少。雖然郊遊徑並未途經廟仔墩和釣魚翁的山頂，但途中有支路可達。不過由於那些是沒有維修的山徑，漁農自然護理署於山徑入口處豎立了「懸崖危險，切勿前進」警告牌，作出[免責聲明](../Page/免責聲明.md "wikilink")。釣魚翁山脊上的山徑崎嶇難行，只宜有經驗及裝備良好的人士前往，天氣惡劣時切勿嘗試，需加倍小心。

雖然並非釣魚翁郊遊徑的一部分，遊人到達大廟坳後，多會順道參觀[大廟灣旁俗稱](../Page/大廟灣.md "wikilink")「大廟」的[佛堂門天后古廟](../Page/佛堂門天后古廟.md "wikilink")，或到[布袋澳享用](../Page/布袋澳.md "wikilink")[海鮮](../Page/海鮮.md "wikilink")。

## 交通接駁

[五塊田](../Page/五塊田.md "wikilink")、[大坳門](../Page/大坳門.md "wikilink")、[蝦山篤](../Page/蝦山篤.md "wikilink")：

  - [九龍巴士91線](../Page/九龍巴士91線.md "wikilink")──[清水灣第二灣至](../Page/清水灣第二灣.md "wikilink")[鑽石山站](../Page/鑽石山站公共運輸交匯處.md "wikilink")
  - [新界區專線小巴16線](../Page/新界區專線小巴16線.md "wikilink")──[布袋澳至](../Page/布袋澳.md "wikilink")[寶林](../Page/寶林公共運輸交匯處.md "wikilink")
  - [新界區專線小巴103線](../Page/新界區專線小巴103線.md "wikilink")──[清水灣第二灣至](../Page/清水灣第二灣.md "wikilink")[觀塘碼頭](../Page/觀塘碼頭巴士總站.md "wikilink")
  - [新界區專線小巴103M線](../Page/新界區專線小巴103M線.md "wikilink")──[清水灣第二灣至](../Page/清水灣第二灣.md "wikilink")[將軍澳站](../Page/將軍澳站公共運輸交匯處.md "wikilink")

[布袋澳](../Page/布袋澳.md "wikilink")：

  - [新界區專線小巴16線](../Page/新界區專線小巴16線.md "wikilink")──[布袋澳至](../Page/布袋澳.md "wikilink")[寶林](../Page/寶林公共運輸交匯處.md "wikilink")

[大廟灣](../Page/大廟灣.md "wikilink")：

  - [大廟灣公眾碼頭](../Page/大廟灣公眾碼頭.md "wikilink")

## 參考資料

  -
  -
  -
  -
[Category:香港郊遊徑](../Category/香港郊遊徑.md "wikilink")
[Category:清水灣半島](../Category/清水灣半島.md "wikilink")