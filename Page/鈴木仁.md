**鈴木仁**（原名**鈴木啄也**，是[日本](../Page/日本.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")、[香港](../Page/香港.md "wikilink")[藝人](../Page/藝人.md "wikilink")。

## 到港發展

鈴木仁在2003年到香港發展後，便開始學習[廣東話及](../Page/廣東話.md "wikilink")[普通話](../Page/普通話.md "wikilink")。曾擔任[官恩娜及](../Page/官恩娜.md "wikilink")[關心妍的MV男主角](../Page/關心妍.md "wikilink")，與[胡杏兒合作拍廣告時亦曾傳出緋聞](../Page/胡杏兒.md "wikilink")。曾與[台灣模特兒](../Page/台灣.md "wikilink")[林嘉綺夜蒲](../Page/林嘉綺.md "wikilink")[蘭桂坊而傳出](../Page/蘭桂坊.md "wikilink")[緋聞](../Page/緋聞.md "wikilink")；與[薛凱琪合作](../Page/薛凱琪.md "wikilink")[電影](../Page/電影.md "wikilink")《[新紮師妹3](../Page/新紮師妹3.md "wikilink")》時也傳出緋聞。

## 戀情

曾經和前[Cookies成員](../Page/Cookies.md "wikilink")[區文詩拍拖](../Page/區文詩.md "wikilink")，更曾在維港灣同居,
可惜三年情之後就分手收場.

## 遇襲事件

2007年5月19日凌晨，鈴木仁與日籍男模[中村祖](../Page/中村祖.md "wikilink")（原名[大住彰文](../Page/大住彰文.md "wikilink")）在[中環](../Page/中環.md "wikilink")[蘭桂坊Volar](../Page/蘭桂坊.md "wikilink")[酒吧消遣](../Page/酒吧.md "wikilink")，當時他欲進入其中一間貴賓房，惟被職員所拒，當他轉身離開時，肩膀卻碰撞到[陳鴻業](../Page/陳鴻業.md "wikilink")（[施念慈的前夫](../Page/施念慈.md "wikilink")），陳鴻業突以香檳酒杯襲擊鈴木仁的左臉，[玻璃碎片飛插他的左眼](../Page/玻璃.md "wikilink")。鈴木仁隨後到[瑪麗醫院求醫並向](../Page/瑪麗醫院.md "wikilink")[日本駐香港總領事館求助](../Page/日本駐香港總領事館.md "wikilink")。

2007年10月2日，在[東區裁判法庭作刑事案開審](../Page/東區裁判法庭.md "wikilink")，辯方律師[清洪指稱根據酒吧提供的](../Page/清洪.md "wikilink")[閉路電視片段](../Page/閉路電視.md "wikilink")，顯示鈴木仁臉上根本沒流[血](../Page/血.md "wikilink")，他在離開酒吧後更在店外逗留22分鐘，足證明他未有痛極至急送醫院。鈴木仁雖然承認案發前曾喝過紅酒，但辯稱並沒喝得很厲害，又強調自己是受害人，不明白為何遭酒吧的[保安員驅趕](../Page/保安員.md "wikilink")，懷疑自己並非[香港人而被](../Page/香港人.md "wikilink")「特別對待」\[1\]。

## 電影

  - 《[新紮師妹3](../Page/新紮師妹3.md "wikilink")》飾演 鈴木伊豆
  - 《[軍雞](../Page/軍雞_\(電影\).md "wikilink")》

## 外部連結

<references />

  - [日籍模特兒鈴木仁，又名為「日版](http://www.metrohk.com.hk/news.php?startDate=21062006&newscat=9)
    [吳彥祖](../Page/吳彥祖.md "wikilink")」
  - [LoveHKFfilm -
    新紮師妹3](http://www.lovehkfilm.com/reviews_2/love_undercover_3.htm)。
  - [蘋果日報：鈴木仁遇襲
    求助領事館](https://web.archive.org/web/20070523075453/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070521&sec_id=462&subsec_id=830&art_id=7122502)
  - [香港電台
    中文新聞頻道：日籍藝人鈴木仁在港遇襲案件開審](http://www.rthk.org.hk/rthk/news/expressnews/news.htm?expressnews&20071002&55&435788)

[Category:日本男性模特兒](../Category/日本男性模特兒.md "wikilink")
[Category:日本電影演員](../Category/日本電影演員.md "wikilink")
[Category:在香港的日本人](../Category/在香港的日本人.md "wikilink")

1.  [鈴木仁稱受襲 影帶證無血](http://hk.news.yahoo.com/071002/60/2gqut.html)