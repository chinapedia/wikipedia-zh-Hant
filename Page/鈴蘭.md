**鈴蘭**（[學名](../Page/學名.md "wikilink")：**，英文：Lily of the Valley，法文：Muguet
de
mai），也称**山谷百合**、**風鈴草**、**君影草**，是[铃兰属中的唯一](../Page/铃兰属.md "wikilink")[種](../Page/種.md "wikilink")，味甜，高毒性。原產[北半球](../Page/北半球.md "wikilink")[溫帶](../Page/溫帶.md "wikilink")，[歐](../Page/歐洲.md "wikilink")、[亞及](../Page/亞洲.md "wikilink")[北美洲和](../Page/北美洲.md "wikilink")[中國的](../Page/中國.md "wikilink")[东北](../Page/中国东北地区.md "wikilink")、[華北地區](../Page/華北.md "wikilink")[海拔](../Page/海拔.md "wikilink")850～2500處均有野生分佈。经典法语歌曲***le
temps du muguet***即是指铃兰花。

## 形态

鈴蘭是[多年生草本植物](../Page/多年生草本植物.md "wikilink")，株高20－30cm。地下具横生而分枝的根状茎，茎端具肥大的地下芽。叶2－3枚，基部互抱成鞘状，卵圆形或窄卵圆形，具孤形叶脉，有光泽。偏向一侧的总状花序，高15－20cm，夏季开钟状白色花，小花约10朵，径约8mm，下垂并具芳香。果实为[浆果](../Page/浆果.md "wikilink")，熟时红色，直径5－7毫米，其中包含数个较大的淡白或淡褐色种子，种子干燥，珠状，清澈半透明，宽1－3毫米。\[1\]

## 分类

铃兰共分为3个变种，而部分植物学家认为这3个变种应该成为独立的物种\[2\]：

  - *Convallaria majalis* [var.](../Page/栽培品种.md "wikilink")
    *keiskei*：分布于[中国和](../Page/中国.md "wikilink")[日本](../Page/日本.md "wikilink")，有红色的果实和[铃形的花朵](../Page/铃.md "wikilink")。\[3\]\[4\]
  - *Convallaria majalis* var.
    *majalis*：分布于[欧亚大陆](../Page/欧亚大陆.md "wikilink")，花上有白色中脉。
  - *Convallaria majalis* var.
    *montana*：分布于[美国](../Page/美国.md "wikilink")，花上有淡绿色中脉。
  - *Convallaria majalis* var. *rosea*：花朵为粉红色，不少学者承认这一变种。\[5\]

## 象征

  - 铃兰在法国是“拥有幸福”的象征。因其在每年的4月到5月间开放，所以在每年的5月1日，法国人有互赠铃兰互相祝愿一年幸福的习俗，获赠人通常将花挂在房间里保存全年，象征幸福永驻，[比利时](../Page/比利时.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[瑞士和](../Page/瑞士.md "wikilink")[安道尔等也有同样的习俗](../Page/安道尔.md "wikilink")。有人认为只有一株刚好13朵小花的铃兰才会带来好运。
  - 法国民间习俗“铃兰婚礼”上常常可以看到这种花，象征结婚13年。
  - 铃兰的花语为“幸福的回归”，也有一個較為具貶意的花語：“忌妒”。
  - 自1967年，其成为[芬兰国花](../Page/芬兰.md "wikilink")\[6\]。

## 毒性

[Lillyvalleystriiped.jpg](https://zh.wikipedia.org/wiki/File:Lillyvalleystriiped.jpg "fig:Lillyvalleystriiped.jpg")
[C._m._cv._Plena.JPG](https://zh.wikipedia.org/wiki/File:C._m._cv._Plena.JPG "fig:C._m._cv._Plena.JPG")
[Convallaria_majalis_0003.JPG](https://zh.wikipedia.org/wiki/File:Convallaria_majalis_0003.JPG "fig:Convallaria_majalis_0003.JPG")
鈴蘭含有[强心苷类有毒物質](../Page/强心苷.md "wikilink")[铃兰毒苷](../Page/铃兰毒苷.md "wikilink")（Convallatoxin）、[铃兰苦苷](../Page/铃兰苦苷.md "wikilink")（Convallamarin）和[铃兰糖苷](../Page/铃兰糖苷.md "wikilink")（Convalloside）等，全草有毒，花、根毒性比較強。
部分人服用其乾品或製劑，造成[噁心](../Page/噁心.md "wikilink")、[嘔吐](../Page/嘔吐.md "wikilink")、流涎、[腹瀉等症狀](../Page/腹瀉.md "wikilink")，亦可能出現[頭暈](../Page/頭暈.md "wikilink")、[頭痛](../Page/頭痛.md "wikilink")、心律不整、[心衰竭等病症](../Page/心衰竭.md "wikilink")。

鈴蘭全株及果实均是高[毒性的](../Page/毒性.md "wikilink")\[7\]\[8\]，目前在铃兰中已发现大概38种不同的[强心苷类毒素](../Page/强心苷类.md "wikilink")，包括：

  - [铃兰苷](../Page/铃兰苷.md "wikilink")（convallarin）
  - [铃兰苦苷](../Page/铃兰苦苷.md "wikilink")（convallamarin）
  - [铃兰毒苷](../Page/铃兰毒苷.md "wikilink")（convallatoxin）
  - convallotoxoloside
  - [铃兰毒原苷](../Page/铃兰毒原苷.md "wikilink")（convalloside）
  - [新铃兰毒原苷](../Page/新铃兰毒原苷.md "wikilink")（neoconvalloside）
  - [葡萄糖铃兰毒原苷](../Page/葡萄糖铃兰毒原苷.md "wikilink")（glucoconvalloside）
  - [铃兰种苷](../Page/铃兰种苷.md "wikilink")（majaloside）
  - convallatoxon
  - [铃兰总苷](../Page/铃兰总苷.md "wikilink")（corglycon）
  - [卡诺醇](../Page/卡诺醇.md "wikilink")-3-*O*-α-<small>L</small>-[鼠李糖苷](../Page/鼠李糖苷.md "wikilink")（cannogenol-3-*O*-α-<small>L</small>-rhamnoside）
  - 卡诺醇-3-*O*-β-<small>D</small>-[甲基阿洛糖苷](../Page/甲基阿洛糖苷.md "wikilink")（cannogenol-3-*O*-β-<small>D</small>-allomethyloside）
  - 卡诺醇-3-*O*-6-脱氧-β-<small>D</small>-[阿洛糖苷](../Page/阿洛糖苷.md "wikilink")-β-<small>D</small>-[葡萄糖苷](../Page/葡萄糖苷.md "wikilink")（cannogenol-3-*O*-6-deoxy-β-<small>D</small>-allosido-β-<small>D</small>-glucoside）
  - 卡诺醇-3-*O*-6-脱氧-β-<small>D</small>-阿洛糖苷-α-<small>L</small>-鼠李糖苷（cannogenol-3-*O*-6-deoxy-β-<small>D</small>-allosido-α-<small>L</small>-rhamnoside）
  - [毒毛旋花子苷元](../Page/毒毛旋花子苷元.md "wikilink")-3-*O*-6-[脱氧](../Page/脱氧.md "wikilink")-β-<small>D</small>-阿洛糖苷-α-<small>L</small>-鼠李糖苷（strophanthidin-3-*O*-6-deoxy-β-<small>D</small>-allosido-α-<small>L</small>-rhamnoside）
  - 毒毛旋花子苷元-3-*O*-6-脱氧-β-<small>D</small>-阿洛糖苷-α-<small>L</small>-阿拉伯糖苷（strophanthidin-3-*O*-6-deoxy-β-<small>D</small>-allosido-α-<small>L</small>-arabinoside）
  - 毒毛旋花子苷元-3-*O*-α-<small>L</small>-鼠李糖苷-2-β-D-葡萄糖苷（strophanthidin-3-*O*-α-<small>L</small>-rhamnosido-2-β-D-glucoside）
  - [沙门苷元](../Page/沙门苷元.md "wikilink")-3-*O*-6-脱氧-β-<small>D</small>-阿洛糖苷-α-<small>L</small>-鼠李糖苷（sarmentogenin-3-*O*-6-deoxy-β-<small>D</small>-allosido-α-<small>L</small>-rhamnoside）
  - 沙门苷元-3-*O*-6-脱氧-β-<small>D</small>-[古洛糖苷](../Page/古洛糖苷.md "wikilink")（sarmentogenin-3-*O*-6-deoxy-β-<small>D</small>-guloside）
  - 19-羟基沙门苷元-3-*O*-α-<small>L</small>-鼠李糖苷（19-hydroxy-sarmentogenin-3-*O*-α-<small>L</small>-rhamnoside）
  - 19-羟基沙门苷元（19-hydroxy-sarmentogenin）
  - [阿拉伯糖苷](../Page/阿拉伯糖苷.md "wikilink")-6-[脱氧阿洛糖](../Page/脱氧阿洛糖.md "wikilink")（[arabinosido](../Page/arabinosido.md "wikilink")-6-[deoxyallose](../Page/deoxyallose.md "wikilink")）
  - locundioside

该植物还包括[皂苷](../Page/皂苷.md "wikilink")。虽然铃兰是致死性的，但在民间医学中仍会适量使用\[9\]。

## 传说

  - 在森林守护神[圣雷欧纳德死亡的土地上](../Page/圣雷欧纳德.md "wikilink")，开出了白色又具有香味的铃兰。铃兰绽放在那块冰凉的土地上，就是圣雷欧纳德的化身。
  - 铃兰是[聖母瑪利亞哀悼基督的眼淚變成的](../Page/聖母瑪利亞.md "wikilink")，把铃兰稱為「Our Lady's
    Tears」即聖母之淚，很多人也譯為女人的眼淚。
  - 烏克兰有個美麗的傳說：很久以前有一位美麗的姑娘，痴心等待遠征的愛人，思念的淚水滴落在林間草地，變成芳馨四溢的铃兰。
  - 铃兰是[白雪公主斷了的珍珠項鏈灑落的珠子](../Page/白雪公主.md "wikilink")，還有人說那是7個小矮人的小小燈籠。
  - 一個叫「琅得什」（俄文：，意为铃兰）的少年，為了他的愛人「維絲娜」（俄文：，意为春天）離他而去而傷心欲絕，少年的淚水變成了白色的花朵，而少年破碎的心流出的鮮血變成了鈴蘭豔紅的漿果。

## 图集

Convallaria-oliv-r2.jpg|鈴蘭 <File:Convallaria_majalis_l.jpg>
<File:Convallaria> majalis0.jpg
[File:C.m._cv._RoseaII.JPG|淡粉色铃兰](File:C.m._cv._RoseaII.JPG%7C淡粉色铃兰)
<File:Maigloeckchen> 2.jpg <File:Bukiet-konwalii.jpg> <File:Land05.jpg>
<File:Convallaria_majalis_IP0404087.jpg> <File:C.m>. cv. Rosea.JPG
<File:Stamp> of Moldova 429.gif <File:Pennä> reverse Anu
1990.jpg|1990年芬兰[盆尼硬币上的铃兰](../Page/芬蘭馬克.md "wikilink")

## 参考文献

## 外部連結

  - [鈴蘭,
    Linglan](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00369)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[藤](../Category/藥用植物.md "wikilink")
[Category:假叶树科](../Category/假叶树科.md "wikilink")
[Category:有毒植物](../Category/有毒植物.md "wikilink")
[Category:阿塞拜疆植物](../Category/阿塞拜疆植物.md "wikilink")
[Category:球根花卉](../Category/球根花卉.md "wikilink")
[花](../Category/芬蘭國家象徵.md "wikilink")

1.  Ohara, Masashi; Araki, Kiwakoi; Yamada, Etsukoi; Kawano, Shoichi,
    Life-history monographs of Japanese plants, 6: *Convallaria keiskei*
    Miq. (Convallariaceae), *Plant Species Biology*, Vol 21, No 2,
    August 2006, pp. 119–126(8), Blackwell Publishing

2.  <http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=107908>

3.

4.
5.
6.

7.  <http://health.nytimes.com/health/guides/poison/lily-of-the-valley/overview.html>

8.

9.  Cantell, Sulo; Saarnio, Väinö, 1936. Suomen myrkylliset ja
    lääkekasvit（译名：芬兰有毒植物及药用植物）