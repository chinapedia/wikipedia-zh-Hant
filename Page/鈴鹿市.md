**铃鹿市**（）是位于[日本](../Page/日本.md "wikilink")[三重县北部](../Page/三重县.md "wikilink")，与[四日市市](../Page/四日市市.md "wikilink")、[津市](../Page/津市_\(日本\).md "wikilink")、[龟山市相接壤的一座城市](../Page/龟山市.md "wikilink")。距离[名古屋西南约](../Page/名古屋.md "wikilink")50千米。

[铃鹿赛道是铃鹿市的著名标志之一](../Page/铃鹿赛道.md "wikilink")。

## 地理

坐落于[三重县的北部](../Page/三重县.md "wikilink")，从东边的[伊势湾向西边的](../Page/伊势湾.md "wikilink")[铃鹿山脉地势逐渐变宽](../Page/铃鹿山脉.md "wikilink")。与对岸的[中部国际机场相隔](../Page/中部国际机场.md "wikilink")[伊势湾](../Page/伊势湾.md "wikilink")。从前延续下来的市区中城下町的神户（近铁铃鹿市站前）、渔港的白子、有军用机场的平田这分散的3处正在沿着中央道路开发，现在西条和庄野羽山也正在进行开发。

### 自然

  - 河流：[铃鹿河](../Page/铃鹿河.md "wikilink")、金泽河、掘切河、中之河
  - 山：[入道山](../Page/入道山.md "wikilink")、野登山
  - [铃鹿国定公园](../Page/铃鹿国定公园.md "wikilink")
  - [伊势海县立自然公园](../Page/伊势海县立自然公园.md "wikilink")

### 接壤自治体

  - [四日市市](../Page/四日市市.md "wikilink")
  - [津市](../Page/津市_\(日本\).md "wikilink")
  - [龟山市](../Page/龟山市.md "wikilink")
  - [滋贺县](../Page/滋贺县.md "wikilink")[甲贺市](../Page/甲贺市.md "wikilink")

## 历史

  - 1942年12月1日 2个町12个村[合併成为铃鹿市](../Page/市町村合併.md "wikilink")。
  - 1954年8月1日 河藝郡荣村、天名村、合川村编入铃鹿市。
  - 1954年12月1日 龟山市井田川地区部分地区编入铃鹿市。
  - 1957年4月15日 [铃鹿郡三铃村部分地区编入铃鹿市](../Page/铃鹿郡.md "wikilink")。
  - 1957年6月15日 铃鹿郡铃峰村境界变更。
  - 1967年4月1日 铃鹿郡铃峰村编入铃鹿市。
  - 1986年1月1日 市民宪章制定
  - 2006年3月　人口突破20万人

## 行政

### 市长

  - [川岸光男](../Page/川岸光男.md "wikilink")：现任市长（第六任）

:\*[奥田茂造](../Page/奥田茂造.md "wikilink")：第一任市长

:\*[杉本龙造](../Page/杉本龙造.md "wikilink")：第二任市长、第一位名誉市民

:\*[野村仲三郎](../Page/野村仲三郎.md "wikilink")：第三任市长

:\*[衣裴贤让](../Page/衣裴贤让.md "wikilink")：第四任市长

:\*[加藤荣](../Page/加藤荣.md "wikilink")：第五任市长

### 地区

该市由以下23个地区组成

  - 国府（Kou）
  - 庄野（Shouno）
  - 加左登（Kasado）
  - 牧田（Makita）
  - 石药师（Ishiyakushi）
  - 白子（Shiroko）
  - 稻生（Inou）
  - 饭野（Iino）
  - 河曲（Kawano）
  - 一之宫（Ichinomiya）
  - 箕田（Mida）
  - 玉桓（Tamagaki）
  - 若松（Wakamatsu）
  - 神户（Kanbe）
  - 荣（Sakae）
  - 天名（Amana）
  - 合川（Aikawa）
  - 井田川（Idagawa）
  - 久间田（Kumada）
  - 椿（Tsubaki）
  - 深伊泽（Fukaizawa）
  - 铃峰（Reihou）
  - 庄内（Shounai）