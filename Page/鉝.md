{{Elementbox |number=116 |symbol=Lv |name=鉝 |enname=Livermorium
|left=[镆](../Page/镆.md "wikilink") |right=\[\[鿬|
**鉝**\[1\]\[2\]（，**Lv**）是原子序為116的[人造元素](../Page/人造元素.md "wikilink")。正式命名前的臨時名稱為**Ununhexium**（**Uuh**），現名於2012年5月30日經[國際純粹與應用化學聯合會批准後正式使用](../Page/國際純粹與應用化學聯合會.md "wikilink")。\[3\]

鉝是[元素週期表](../Page/元素週期表.md "wikilink")[16族最重的元素](../Page/16族元素.md "wikilink")，位於[釙之下](../Page/釙.md "wikilink")，但由於沒有足夠穩定的[同位素](../Page/同位素.md "wikilink")，因此目前無法用實驗來研究它的特性。

科學家於2000年發現鉝，至今成功合成約30個原子。這些原子都是直接合成或是[鿫衰變的產物](../Page/鿫.md "wikilink")。已合成的鉝同位素質量數介乎290至293，其中<sup>293</sup>Lv是最穩定的，[半衰期約為](../Page/半衰期.md "wikilink")60毫秒。

## 歷史

### 發現

2000年7月19日，位於[俄羅斯](../Page/俄羅斯.md "wikilink")[杜布納](../Page/杜布納.md "wikilink")[聯合核研究所](../Page/聯合核研究所.md "wikilink")（JINR）的科學家使用<sup>48</sup>[Ca離子撞擊](../Page/钙.md "wikilink")<sup>248</sup>[Cm目標](../Page/锔.md "wikilink")，探測到鉝原子的一次α衰變，能量為10.54
MeV。結果於2000年12月發佈。\[4\]由於<sup>292</sup>Lv的衰變產物和已知的<sup>288</sup>[Fl關聯](../Page/鈇.md "wikilink")，因此這次衰變起初被認為源自<sup>292</sup>Lv。然而其後科學家把<sup>288</sup>Fl更正為<sup>289</sup>Fl，所以衰變來源<sup>292</sup>Lv也順應更改到<sup>293</sup>Lv。他們於2001年4至5月進行了第二次實驗，再發現兩個鉝原子。\[5\]

\[\,^{48}_{20}\mathrm{Ca} + \,^{248}_{96}\mathrm{Cm} \to \,^{296}_{116}\mathrm{Lv} ^{*} \to \,^{293}_{116}\mathrm{Lv} + 3\,^{1}_{0}\mathrm{n}\]

在同樣的實驗裏，研究人員探測到[鈇的衰變](../Page/鈇.md "wikilink")，並將此次衰變活動指定到<sup>289</sup>Fl。\[6\]在重複進行相同的實驗後，他們並沒有觀測到該衰變反應。這可能是來自鉝的同核異能素<sup>293b</sup>Lv的衰變，或是<sup>293a</sup>Lv的一條較罕見的衰變支鏈。這須進行進一步研究才能確認。

研究團隊在2005年4月至5月重複進行實驗，並探測到8個鉝原子。衰變數據證實所發現的[同位素是](../Page/同位素.md "wikilink")<sup>293</sup>Lv。同時他們也通過4n通道第一次觀測到<sup>292</sup>Lv。\[7\]

2009年5月，聯合工作組在報告中指明，發現了的[鎶同位素包括](../Page/鎶.md "wikilink")<sup>283</sup>Cn。\[8\]<sup>283</sup>Cn是<sup>291</sup>Lv的衰變產物，因此該報告意味著<sup>291</sup>Lv也被正式發現（見下）。

2011年6月11日，[IUPAC證實了鉝的存在](../Page/IUPAC.md "wikilink")。\[9\]

### 命名

#### 原文名稱

鉝的原文名稱Livermorium（Lv），是[IUPAC在](../Page/國際純粹與應用化學聯合會.md "wikilink")2012年5月30日正式命名的\[10\]。之前IUPAC根据[系統命名法将之命名为](../Page/IUPAC元素系統命名法.md "wikilink")*Ununhexium*（Uuh）\[11\]。科學家通常稱之為“元素116”（或E116）。

此前鉝被提議以俄羅斯[莫斯科州](../Page/莫斯科州.md "wikilink")（Moscow
Oblast）名为Moscovium，但由于元素114和116是俄罗斯和美国[劳伦斯利福摩尔国家实验室研究人员合作的产物](../Page/劳伦斯利福摩尔国家实验室.md "wikilink")，而元素114已经根据俄罗斯的要求命名，因此元素116最后以实验室所在地[美国](../Page/美国.md "wikilink")[利弗莫尔市](../Page/利佛摩_\(加利福尼亚州\).md "wikilink")（Livermore）命名为Livermorium（Lv）\[12\]\[13\]。

#### 中文名稱

2012年6月2日，台灣的[國家教育研究院的](../Page/國家教育研究院.md "wikilink")[化學名詞審譯委員會將此元素暫譯為](../Page/化學名詞審譯委員會.md "wikilink")**鉝**。\[14\]\[15\]2013年7月，中華人民共和國[全國科學技術名詞審定委員會通過以](../Page/全國科學技術名詞審定委員會.md "wikilink")**鉝**為中文定名。\[16\]

### 目前及未來的實驗

位於杜布納的團隊表示有意利用<sup>244</sup>[Pu和](../Page/钚.md "wikilink")<sup>50</sup>[Ti的核反應合成鉝](../Page/钛.md "wikilink")。通過這項實驗，他們可以研究是否可能以原子序大於20的發射體來合成原子序大於118的超重元素。雖然原定計劃在2008年進行，但這項實驗至今仍未開始。\[17\]

研究團隊也有計劃使用不同發射體能量來重複<sup>248</sup>Cm反應，以進一步了解2n通道，從而發現新的[同位素](../Page/同位素.md "wikilink")<sup>294</sup>Lv。另外，他們計劃在未來完成4n通道產物<sup>292</sup>Lv的[激發函數](../Page/激發函數.md "wikilink")，並估量N=184[核殼層對產生蒸發殘留物的穩定效應](../Page/核殼層.md "wikilink")。

## 同位素與核特性

### 核合成

\====能產生Z=116复核的目標、發射體組合==== 下表列出各種可用以產生116號元素的目標、發射體組合。

<table>
<thead>
<tr class="header">
<th><p>目標</p></th>
<th><p>發射體</p></th>
<th><p>CN</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><sup>208</sup>Pb</p></td>
<td><p><sup>82</sup>Se</p></td>
<td><p><sup>290</sup>Lv</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>232</sup>Th</p></td>
<td><p><sup>58</sup>Fe</p></td>
<td><p><sup>290</sup>Lv</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>238</sup>U</p></td>
<td><p><sup>54</sup>Cr</p></td>
<td><p><sup>292</sup>Lv</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>244</sup>Pu</p></td>
<td><p><sup>50</sup>Ti</p></td>
<td><p><sup>294</sup>Lv</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>250</sup>Cm</p></td>
<td><p><sup>48</sup>Ca</p></td>
<td><p><sup>298</sup>Lv</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>248</sup>Cm</p></td>
<td><p><sup>48</sup>Ca</p></td>
<td><p><sup>296</sup>Lv</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>246</sup>Cm</p></td>
<td><p><sup>48</sup>Ca</p></td>
<td><p><sup>294</sup>Lv</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>245</sup>Cm</p></td>
<td><p><sup>48</sup>Ca</p></td>
<td><p><sup>293</sup>Lv</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>249</sup>Cf</p></td>
<td><p><sup>40</sup>Ar</p></td>
<td><p><sup>289</sup>Lv</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 冷聚變

##### <sup>208</sup>Pb(<sup>82</sup>Se,*x*n)<sup>290−*x*</sup>Lv

1998年，[重離子研究所嘗試了輻射俘獲產物](../Page/重離子研究所.md "wikilink")（*x*=0）以合成<sup>290</sup>Lv。他們限制截面為4.8
pb，並未發現任何原子。

#### 熱聚變

##### <sup>238</sup>U(<sup>54</sup>Cr,*x*n)<sup>292−*x*</sup>Lv

有粗略的證據顯示重離子研究所在2006年曾經嘗試過這個反應。他們沒有發布實驗結果，表示很可能並沒有發現任何原子。\[18\]

\=====<sup>248</sup>Cm(<sup>48</sup>Ca,*x*n)<sup>296−*x*</sup>Lv
(*x*=3,4)===== 1977年Ken
Hulet和他的團隊在[勞倫斯利福摩爾國家實驗室首次進行合成鉝的實驗](../Page/勞倫斯利福摩爾國家實驗室.md "wikilink")。他們並未發現任何鉝原子。\[19\][尤里·奥加涅相和他的團隊在Flerov核反應實驗室之後在](../Page/尤里·奥加涅相.md "wikilink")1978年嘗試了這個反應，但最終失敗。1985年，伯克利實驗室和在重離子研究所的團隊進行了實驗，結果依然是失敗的，計算出來的截面限度為10至100
pb。\[20\]

2000年，杜布納的俄羅斯科學家終於成功探測到一個鉝原子，指向到同位素<sup>292</sup>Lv。\[21\]2001年，他們重複了這一個反應，再次合成了2個原子，驗證了此前的實驗結果。另外也不確定地探測到一個<sup>293</sup>Lv原子，因為其首次α衰變違背探測到。\[22\]2004年4月，團隊又再使用較高能量重複實驗，並發現了一條新的衰變鏈，指向到<sup>292</sup>Lv。根據這個發現，原先的數據就被重新指向到<sup>293</sup>Lv。不確定的衰變鏈因此可能是這個同位素的稀有的一條分支。這個反應另外有產生了2個<sup>293</sup>Lv原子。\[23\]

\=====<sup>245</sup>Cm(<sup>48</sup>Ca,xn)<sup>293−x</sup>116
(x=2,3)=====
為了找出合成出的鉝同位素的原子量，在2003年3月至5月期間杜布納的團隊用<sup>48</sup>Ca離子撞擊<sup>245</sup>Cm目標。他們觀察到了兩個新的同位素：<sup>291</sup>Lv和<sup>290</sup>Lv。\[24\]這個實驗在2005年2月至3月成功重複進行，其中合成了10個原子，其衰變數據與2003年實驗報告中的相符。\[25\]

#### 作為衰變產物

鉝也在[鿫的衰變中被探測到](../Page/鿫.md "wikilink")。2006年10月，在一個用<sup>48</sup>Ca離子撞擊<sup>249</sup>Cf的實驗中，3個[鿫原子被發現](../Page/鿫.md "wikilink")，並迅速衰變成鉝。\[26\]

觀察到<sup>290</sup>Lv，意味著成功合成了<sup>294</sup>[鿫](../Page/鿫.md "wikilink")，也證明了成功合成元素[鿫](../Page/鿫.md "wikilink")。

#### 原子量為116的复核的裂變

位於杜布納的Flerov核反應實驗室在2000至2006年進行了一系列的實驗，研究<sup>296,294,290</sup>Lv复核的裂變特性。實驗使用了4條核反應：<sup>248</sup>Cm+<sup>48</sup>Ca、<sup>246</sup>Cm+<sup>48</sup>Ca、<sup>244</sup>Pu+<sup>50</sup>Ti和<sup>232</sup>Th+<sup>58</sup>Fe。結果反映了這種原子核裂變的方式主要為放出閉殼原子核，如<sup>132</sup>Sn
(Z=50,
N=82)。另一發現為，使用<sup>48</sup>Ca和<sup>58</sup>Fe發射體的聚變裂變路徑產量相似，說明在未來合成超重元素時，可以使用<sup>58</sup>Fe發射體。另外，比較使用<sup>48</sup>Ca和<sup>50</sup>Ti發射體合成<sup>294</sup>Lv的實驗，如果用<sup>50</sup>Ti，聚變裂變產量約少3倍，表示未來能用於合成超重元素。\[27\]

#### 撤回的同位素

##### <sup>289</sup>Lv

1999年，[勞倫斯伯克利國家實驗室在](../Page/勞倫斯伯克利國家實驗室.md "wikilink")《物理評論快報》中宣布成功合成<sup>293</sup>Og（見[Og](../Page/Og.md "wikilink")）。\[28\]所指的同位素<sup>289</sup>Lv經過了11.63
MeV能量的α衰變，半衰期為0.64
ms。翌年，他們宣布撤回此前的發現，因為其他研究人員未能複製實驗結果。\[29\]2002年6月，實驗室主任公佈，原先這兩個元素的發現結果是建立在編造的實驗數據上的。

#### 同位素發現時序

| 同位素              | 發現年份  | 核反應                                        |
| ---------------- | ----- | ------------------------------------------ |
| <sup>290</sup>Lv | 2002年 | <sup>249</sup>Cf(<sup>48</sup>Ca,3n)\[30\] |
| <sup>291</sup>Lv | 2003年 | <sup>245</sup>Cm(<sup>48</sup>Ca,2n)\[31\] |
| <sup>292</sup>Lv | 2004年 | <sup>248</sup>Cm(<sup>48</sup>Ca,4n)\[32\] |
| <sup>293</sup>Lv | 2000年 | <sup>248</sup>Cm(<sup>48</sup>Ca,3n)\[33\] |

### 同位素產量

#### 熱聚變

下表列出直接合成鉝的熱聚變核反應的截面和激發能量。粗體數據代表從激發函數算出的最大值。+代表觀測到的出口通道。

| 發射體             | 目標               | CN               | 2n                         | 3n                          | 4n                      | 5n |
| --------------- | ---------------- | ---------------- | -------------------------- | --------------------------- | ----------------------- | -- |
| <sup>48</sup>Ca | <sup>248</sup>Cm | <sup>296</sup>Lv |                            | 1.1 pb, 38.9 MeV\[34\]      | 3.3 pb, 38.9 MeV \[35\] |    |
| <sup>48</sup>Ca | <sup>245</sup>Cm | <sup>293</sup>Lv | **0.9 pb, 33.0 MeV**\[36\] | **3.7 pb, 37.9 MeV** \[37\] |                         |    |

### 理論計算

#### 衰變特性

利用量子穿隧模型的理論計算支持合成<sup>293,292</sup>Lv的實驗數據。\[38\]\[39\]

#### 蒸發殘留物截面

下表列出各種目標-發射體組合，並給出最高的預計產量。

DNS = 雙核系統； σ = 截面

| 目標               | 發射體             | CN               | 通道（產物）                | σ<sub>max</sub> | 模型  | 參考資料   |
| ---------------- | --------------- | ---------------- | --------------------- | --------------- | --- | ------ |
| <sup>208</sup>Pb | <sup>82</sup>Se | <sup>290</sup>Lv | 1n (<sup>289</sup>Lv) | 0.1 pb          | DNS | \[40\] |
| <sup>208</sup>Pb | <sup>79</sup>Se | <sup>287</sup>Lv | 1n (<sup>286</sup>Lv) | 0.5 pb          | DNS | \[41\] |
| <sup>238</sup>U  | <sup>54</sup>Cr | <sup>292</sup>Lv | 2n (<sup>290</sup>Lv) | 0.1 pb          | DNS | \[42\] |
| <sup>250</sup>Cm | <sup>48</sup>Ca | <sup>298</sup>Lv | 4n (<sup>294</sup>Lv) | 5 pb            | DNS | \[43\] |
| <sup>248</sup>Cm | <sup>48</sup>Ca | <sup>296</sup>Lv | 4n (<sup>292</sup>Lv) | 2 pb            | DNS | \[44\] |
| <sup>247</sup>Cm | <sup>48</sup>Ca | <sup>295</sup>Lv | 3n (<sup>292</sup>Lv) | 3 pb            | DNS | \[45\] |
| <sup>245</sup>Cm | <sup>48</sup>Ca | <sup>293</sup>Lv | 3n (<sup>290</sup>Lv) | 1.5 pb          | DNS | \[46\] |

## 化學屬性

### 推算的化學屬性

#### 氧化態

鉝預計為7p系[非金屬的第](../Page/非金屬.md "wikilink")4個元素，並是元素週期表中16族（VIA）最重的成員，位於[釙之下](../Page/釙.md "wikilink")。這一族的氧化態為+VI，除了缺少d-[軌域的](../Page/軌域.md "wikilink")[氧外](../Page/氧.md "wikilink")。[硫](../Page/硫.md "wikilink")、[硒](../Page/硒.md "wikilink")、[碲及](../Page/碲.md "wikilink")[釙的氧化態都是](../Page/釙.md "wikilink")+IV，穩定性由S(IV)和Se(IV)的還原性到Po(IV)的氧化性。Te(IV)是碲最穩定的氧化態。這表示較高氧化態穩定性較低，因此鉝應有氧化性的+IV態，以及更穩定的+II態。同族其他元素亦能產生−II態，如[氧化物](../Page/氧化物.md "wikilink")、[硫化物](../Page/硫化物.md "wikilink")、[硒化物](../Page/硒化物.md "wikilink")、[碲化物和](../Page/碲化物.md "wikilink")[釙化物](../Page/釙化物.md "wikilink")。

#### 化學特性

鉝的化學特性能從[釙的特性推算出來](../Page/釙.md "wikilink")。因此，它應在[氧化後產生二氧化鉝](../Page/氧化.md "wikilink")（LvO<sub>2</sub>）。三氧化鉝（LvO<sub>3</sub>）也有可能產生，但可能性較低。在氧化鉝（LvO）中，鉝會展現出+II氧化態的穩定性。[氟化後它可能會產生四氟化鉝](../Page/氟化.md "wikilink")（LvF<sub>4</sub>）和/或二氟化鉝（LvF<sub>2</sub>）。[氯化和](../Page/氯化.md "wikilink")[溴化後會產生二氯化鉝](../Page/溴化.md "wikilink")（LvCl<sub>2</sub>）和二溴化鉝（LvBr<sub>2</sub>）。[碘對其氧化後一定不會產生比二碘化鉝](../Page/碘.md "wikilink")（LvI<sub>2</sub>）更重的化合物，甚至可能完全不發生反應。

## 參見

  - [穩定島](../Page/穩定島.md "wikilink")

## 參考資料

[Category:氧族元素](../Category/氧族元素.md "wikilink")
[Category:人工合成元素](../Category/人工合成元素.md "wikilink")
[7d](../Category/第7周期元素.md "wikilink")
[7d](../Category/化学元素.md "wikilink")

1.
2.

3.

4.

5.  ["Confirmed results of the
    <sup>248</sup>Cm(<sup>48</sup>Ca,4n)<sup>292</sup>116
    experiment"](https://e-reports-ext.llnl.gov/pdf/302186.pdf), *Patin
    et al.*, *LLNL report (2003)*. Retrieved 2008-03-03

6.
7.

8.

9.  [IUPAC - Discovery of the Elements with Atomic Number 114
    and 116](http://www.iupac.org/web/nt/2011-06-01_elements_114_116)

10.

11.

12.

13.

14.

15.
16.
17. [Flerov
    Lab.](http://flerovlab.jinr.ru/flnr/programme_synth_2008.html)

18. ["List of
    experiments 2000-2006"](http://opal.dnp.fmph.uniba.sk/~beer/experiments.php)


19.

20.

21.
22.
23.
24.

25.

26.
27. see [Flerov lab annual
    reports 2000-2006](http://www1.jinr.ru/Reports/Reports_eng_arh.html)

28.

29.

30. 見[Og](../Page/Og.md "wikilink")

31.
32.
33.
34.
35.
36.
37.
38.

39.

40.

41.
42.

43.
44.
45.
46.