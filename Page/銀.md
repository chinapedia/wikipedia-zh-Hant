**银**（）是[化学元素](../Page/化学元素.md "wikilink")，[化学符号](../Page/化学符号.md "wikilink")**Ag**（来自）\[1\]，[原子序数](../Page/原子序数.md "wikilink")47，是柔软有白色光泽的[过渡金属](../Page/过渡金属.md "wikilink")，在所有金属中[导电率](../Page/导电率.md "wikilink")、[导热率和](../Page/导热率.md "wikilink")[反射率最高](../Page/反射率.md "wikilink")。銀在自然界中的存在方式有纯净的游离态单质（[自然银](../Page/自然银.md "wikilink")），与[金等其他金属的](../Page/金.md "wikilink")[合金](../Page/合金.md "wikilink")，还有含银矿石（如[辉银矿和](../Page/辉银矿.md "wikilink")[角银矿](../Page/角银矿.md "wikilink")）。大部分银都是[精炼](../Page/精炼.md "wikilink")[铜](../Page/铜.md "wikilink")、金、[铅和](../Page/铅.md "wikilink")[锌的副产品](../Page/锌.md "wikilink")。

银不易受化學藥品腐蝕，长久以来被视为[贵金属](../Page/贵金属.md "wikilink")。银比金来源更丰富，在现代以前的货币体系中作为[硬币使用](../Page/投资型硬币.md "wikilink")，有时甚至和[金一道使用](../Page/金银复本位.md "wikilink")。除了[货币之外](../Page/货币.md "wikilink")，银的用途还有[太阳能电池板](../Page/太阳能电池板.md "wikilink")、[净水器](../Page/净水器.md "wikilink")、[珠宝和装饰品](../Page/珠宝.md "wikilink")、高价餐具和器皿（[银器](../Page/银器.md "wikilink")），[银币和](../Page/银币.md "wikilink")还可用于投资。银在工业上用于和[导体](../Page/导体.md "wikilink")、特制镜子、窗膜和化学反应的[催化剂](../Page/催化剂.md "wikilink")。银的化合物用于[胶片和](../Page/胶片.md "wikilink")[X光](../Page/X光.md "wikilink")。稀[硝酸银溶液等银化合物会产生](../Page/硝酸银.md "wikilink")，可以[消毒和消灭](../Page/消毒.md "wikilink")[微生物](../Page/微生物.md "wikilink")，用于[绷带](../Page/绷带.md "wikilink")、伤口敷料、导管等[医疗器械](../Page/医疗器械.md "wikilink")。

## 性质

纯白银颜色白，金属光泽，质软，掺有杂质后变硬，颜色呈灰、红色。纯白银比重为10.5，熔点960.5℃，导电性能佳，溶于硝酸、浓硫酸中。

### 物理性质

银是[11族元素](../Page/11族元素.md "wikilink")，[延展性好](../Page/延展性.md "wikilink")（仅次于[金](../Page/金.md "wikilink")），有明亮的银白色金属光泽，[抛光度高](../Page/抛光度.md "wikilink")。\[2\]在受保护的环境中，银对波长450[纳米以上的光波](../Page/纳米.md "wikilink")[反射率比](../Page/反射率.md "wikilink")[铝高](../Page/铝.md "wikilink")\[3\]，对波长450纳米以下的光波反射率不如铝，对波长310纳米的光波反射率降为零。\[4\]

银的[导电性在所有金属中最高](../Page/导电性.md "wikilink")，比铜还高\[5\]，但在电气中由于价格高昂，应用并不广。但是个例外，特别是在[甚高频以上的频段](../Page/甚高频.md "wikilink")，镀银能够显著增加元件和导线整体的导电性，因为[高频电流会集中在导体的表面而非内部](../Page/集肤效应.md "wikilink")。[二战中美国生产浓缩](../Page/二战.md "wikilink")[铀的](../Page/铀.md "wikilink")[电磁铁用了](../Page/电磁铁.md "wikilink")13450吨银，这是因为战时缺铜。\[6\]\[7\]\[8\]

纯银在金属中[导热性最高](../Page/导热性.md "wikilink")，但低于非金属中的[碳](../Page/碳.md "wikilink")（[金刚石](../Page/金刚石.md "wikilink")）和。\[9\]

密度：10.5克/厘米<sup>3</sup> 熔点：961.93℃ 沸点：2213℃
其他性质：富延展性，是导热、导电性能很好的金属。第一电离能7.576电子伏。化学性质稳定，对水与大气中的氧都不起作用；易溶于稀硝酸、热的浓硫酸和盐酸、熔融的氢氧化碱。晶体结构：晶胞为面心立方晶胞，每个晶胞含有4个金属原子。晶胞参数：a
= 408.53 pm b = 408.53 pm c = 408.53 pm α = 90° β = 90° γ = 90°

### 化学性质

银是古代发现的金属之一。银在自然界中虽然也有单质存在，但绝大部分是以化合态的形式存在。

银具有很高的延展性，因此可以碾压成只有0.00003厘米厚的透明箔，1克重的银粒就可以拉成约两公里长的细丝。

银的导热性和导电性在金属中名列前茅。

银的特征氧化数为+1，其化学性质比铜差，常温下，甚至加热时也不与水和空气中的氧作用，但久置空气中能变黑，失去银白色的光泽，这是因为银和空气中的硫化氫(H<sub>2</sub>S)化合成黑色硫化銀(Ag<sub>2</sub>S)的缘故。其化学反应方程式为：

  -
    4Ag + 2H<sub>2</sub>S + O<sub>2</sub> = 2Ag<sub>2</sub>S +
    2H<sub>2</sub>O

银不能与稀盐酸或稀硫酸反应放出氢气，但银能溶解在硝酸或热的浓硫酸中：

  -
    2Ag + 2H<sub>2</sub>SO<sub>4</sub>（浓） —<u><sup>Δ</sup></u>→
    Ag<sub>2</sub>SO<sub>4</sub> + SO<sub>2</sub>↑ + 2H<sub>2</sub>O

银在常温下与卤素反应很慢，在加热的条件下即可生成卤化物：

  -
    2Ag + F<sub>2</sub> —<u><sup>473 K</sup></u>→ 2AgF暗棕色
    2Ag + Cl2 —<u><sup>Δ</sup></u>→ 2AgCl白色
    2Ag + Br2 —<u><sup>Δ</sup></u>→ 2AgBr黄色
    2Ag + I2 —<u><sup>Δ</sup></u>→ 2AgI橙色

银对硫有很强的亲合势，加热时可以与硫直接化合成Ag<sub>2</sub>S：

  -
    2Ag + S =Δ= Ag<sub>2</sub>S

类似地，银和[硒](../Page/硒.md "wikilink")、[碲的反应为](../Page/碲.md "wikilink")：

  -
    2 Ag + Se → Ag<sub>2</sub>Se
    2 Ag + Te → Ag<sub>2</sub>Te

## 同位素

自然界存在的銀有两种[稳定同位素](../Page/稳定同位素.md "wikilink")：<sup>107</sup>Ag和<sup>109</sup>Ag，其中前者的[豐度略高](../Page/豐度.md "wikilink")（51.839%）。銀的两种[同位素的豐度幾乎相同](../Page/同位素.md "wikilink")，這在元素周期表中十分罕見（[溴是另一個例子](../Page/溴.md "wikilink")）。銀的[原子量是](../Page/原子量.md "wikilink")107.8682
(2)
[克/摩爾](../Page/摩尔质量.md "wikilink")。\[10\]\[11\]已确定銀的二十八個[放射性同位素的特性](../Page/放射性同位素.md "wikilink")，其中最穩定的依次是<sup>105</sup>Ag（半衰期41.29天），<sup>111</sup>Ag（半衰期7.45天），<sup>112</sup>Ag（半衰期3.13小時）。銀有很多[亚稳态核素](../Page/核同质异能素.md "wikilink")，其中最穩定的依次是<sup>108m</sup>Ag（半衰期418年），<sup>110m</sup>Ag（半衰期為249.79天），<sup>106m</sup>Ag（半衰期8.28天）。其餘的[放射性同位素的半衰期皆短於一小時](../Page/放射性.md "wikilink")，大部分短於三分鐘。

銀的同位素原子量从92.950（<sup>94</sup>Ag）到129.950（<sup>130</sup>Ag）不等。\[12\]\[13\]丰度最高的稳定同位素（<sup>107</sup>Ag）之前的同位素的[衰变类型主要是](../Page/放射性#衰变类型.md "wikilink")[電子捕獲](../Page/電子捕獲.md "wikilink")，生成[钯](../Page/钯.md "wikilink")（46号元素）的同位素，而<sup>107</sup>Ag之后的同位素的衰变类型则主要是[β衰變](../Page/β衰變.md "wikilink")，生成[镉](../Page/镉.md "wikilink")（48号元素）的同位素。\[14\]

<sup>107</sup>Pd
β衰變成<sup>107</sup>Ag的半衰期為650萬年。[鐵隕石是仅有的](../Page/鐵隕石.md "wikilink")「鈀-銀比」高到可以測量<sup>107</sup>Ag富度變化的物体。由放射性产生的<sup>107</sup>Ag首次发现于1978年美國[聖塔克拉拉的隕石](../Page/聖塔克拉拉.md "wikilink")。\[15\]發現者提出，一些小型鐵核的行星與其異體，可能是在一千多萬年前的[核合成事件中產生的](../Page/核合成.md "wikilink")。從這熔化過的星球本體中，觀察到的<sup>107</sup>Pd–<sup>107</sup>Ag比值，反映出早期[太陽系的](../Page/太陽系.md "wikilink")[吸積中應存在著不穩定的核種](../Page/吸積.md "wikilink")。\[16\]

## 特點

  - 性質穩定，活躍性低
  - 氧氣相對其他[氣體能更容易溶解於銀](../Page/氣體.md "wikilink")。
  - 导熱，導電率高
  - 不易受化學藥品腐蝕（但仍然能被[硫](../Page/硫.md "wikilink")、[硒](../Page/硒.md "wikilink")、[硫化物](../Page/硫化物.md "wikilink")、[硝酸](../Page/硝酸.md "wikilink")、[氫氟酸](../Page/氫氟酸.md "wikilink")、[氢碘酸](../Page/氢碘酸.md "wikilink")、[氯气等腐蚀](../Page/氯气.md "wikilink")）
  - 质軟
  - 富有延展性

## 應用

  - 银600-800美元每千克（工业应用必考虑成本，2013年春，相比较铜的价格在8\~12美元每千克）。
  - 製造高價值的物件如[銀元](../Page/銀元.md "wikilink")[貨幣](../Page/貨幣.md "wikilink")、首飾，並用於製造[勋章](../Page/勋章.md "wikilink")、獎座、盃、牌和種種裝飾。
  - 與汞、錫等其他金屬在室溫混合成的混合物，被廣泛用於牙醫上。
  - 製造控制棒來控制[核連鎖反應](../Page/核連鎖反應.md "wikilink")。
  - 用作[催化劑](../Page/催化劑.md "wikilink")，是一種對工業非常重要的催化劑，化學實驗室中也會使用。
  - 用作電線等導電體，常見於音響設備及鍵盤。
  - 加入[鎳](../Page/鎳.md "wikilink")、[銅以增加硬度](../Page/銅.md "wikilink")。
  - 在[電子工業上是重要的](../Page/電子工業.md "wikilink")[導電材料](../Page/導電.md "wikilink")。
  - 制造[合金](../Page/合金.md "wikilink")、[硝酸银和其它银的化合物等](../Page/硝酸银.md "wikilink")。
  - 用作製造[鏡子反光面](../Page/鏡子.md "wikilink")。
  - 飾品、精品、[工藝品皆有使用](../Page/工藝品.md "wikilink")。較好的材質為925銀，即92.5%[銀加入](../Page/銀.md "wikilink")7.5％的[銅](../Page/銅.md "wikilink")，為
    [Tiffany & Co.](../Page/Tiffany_&_Co..md "wikilink") 所開創的標準。
  - 銀能對硫等元素反應，也對某些微生物有殺菌功效卻對人體無害，加上有美觀價值，因此常被做為高級餐具或食物容器。古代也曾有利用這種特性而出現「銀針探毒」的驗毒技術，但今日已證實銀僅對部分元素、化合物及微生物有反應，部分食物如[雞蛋等因含硫即使無毒亦會有反應](../Page/雞蛋.md "wikilink")，驗毒功效並非百分之百。

## 名稱來源

銀拉丁原名為*argentum*，是其化學符號的來源。

因為銀的活躍性低，其元素型態易被發現亦易提取，故此在古時的中國和西方分別已被認定為[五金和](../Page/五金.md "wikilink")[煉金術七金之二](../Page/煉金術.md "wikilink")，僅於**[金](../Page/金.md "wikilink")**之後一名。

古代西方的[煉金術和](../Page/煉金術.md "wikilink")[占星術也有將金屬中的銀與七曜中的](../Page/占星術.md "wikilink")[月連結](../Page/月亮.md "wikilink")，又為[金和](../Page/金.md "wikilink")[日之後一名](../Page/太陽.md "wikilink")。

[File:Silver1.jpg|用碳氧焰燒成熔融的銀](File:Silver1.jpg%7C用碳氧焰燒成熔融的銀)。
[File:1-peso-de-Campo-Morado-de-1914-(canto-estriado)(plata)(01).jpg|以白銀作為原料鑄造的貨幣](File:1-peso-de-Campo-Morado-de-1914-\(canto-estriado\)\(plata\)\(01\).jpg%7C以白銀作為原料鑄造的貨幣)（[墨西哥銀圓](../Page/墨西哥銀圓.md "wikilink")）

## 化合物

### \+1价态化合物

银在化合物中主要以+1价的形式存在。

银溶于[硝酸](../Page/硝酸.md "wikilink")（），生成[硝酸银](../Page/硝酸银.md "wikilink")（）。硝酸银是一种透明晶体，有感光性，且易溶于水。硝酸银是合成许多其他银化合物的原料，也可作为[防腐剂](../Page/防腐剂.md "wikilink")，还用于[彩色玻璃中的黄色添加剂](../Page/花窗玻璃.md "wikilink")。银不易与硫酸反应，因此硫酸在珠宝制造中用于清洗[银焊及](../Page/硬焊.md "wikilink")[退火后留下的氧化铜](../Page/退火.md "wikilink")。银易与[硫以及](../Page/硫.md "wikilink")[硫化氢](../Page/硫化氢.md "wikilink")（）反应生成黑色的[硫化银](../Page/硫化银.md "wikilink")（），这在失去光泽的[银币或其他物品上很常见](../Page/银币.md "wikilink")。当银制在富含硫化氢的环境下工作时，触点上的硫化银还会生成[银晶须](../Page/晶鬚.md "wikilink")。

  -
    4 Ag + O<sub>2</sub> + 2 H<sub>2</sub>S → 2 Ag<sub>2</sub>S + 2
    H<sub>2</sub>O

[Cessna_210_Hagelflieger_Detail.jpg](https://zh.wikipedia.org/wiki/File:Cessna_210_Hagelflieger_Detail.jpg "fig:Cessna_210_Hagelflieger_Detail.jpg")装备了碘化银發生器\]\]

向硝酸银溶液中加入[氯离子会沉淀出](../Page/氯离子.md "wikilink")[氯化银](../Page/氯化银.md "wikilink")（），同样地，加入[溴盐或](../Page/溴化物.md "wikilink")[碘盐可以沉淀出用于制造](../Page/碘化物.md "wikilink")的其他[卤化银](../Page/卤化银.md "wikilink")。氯化银用于制造检测[pH值和测量](../Page/pH值.md "wikilink")[电位的](../Page/电位.md "wikilink")，以及用于玻璃的透明[水泥](../Page/水泥.md "wikilink")。将[碘化银](../Page/碘化银.md "wikilink")
（）撒入云层以[人工降雨](../Page/人工降雨.md "wikilink")。卤化银在水溶液中高度不溶（除了[氟化银](../Page/氟化银.md "wikilink")），因而常用于[重量分析](../Page/分析化学#.E9.87.8D.E9.87.8F.E5.88.86.E6.9E.90.md "wikilink")。

向硝酸银溶液加入[碱](../Page/碱.md "wikilink")，沉淀得到[氧化银](../Page/氧化银.md "wikilink")
()。氧化银用作[纽扣电池的](../Page/纽扣电池.md "wikilink")[正极](../Page/阴极.md "wikilink")。向硝酸银溶液加入[碳酸钠](../Page/碳酸钠.md "wikilink")
()，沉淀得[碳酸银](../Page/碳酸银.md "wikilink")()。\[17\]

  -
    2 AgNO<sub>3</sub> + 2 OH<sup>−</sup> → Ag<sub>2</sub>O +
    H<sub>2</sub>O + 2 NO<sub>3</sub><sup>−</sup>
    2 AgNO<sub>3</sub> + Na<sub>2</sub>CO<sub>3</sub> →
    Ag<sub>2</sub>CO<sub>3</sub> + 2 NaNO<sub>3</sub>

[雷酸银](../Page/雷酸银.md "wikilink")()是一种强烈的、对碰撞敏感的[炸药](../Page/炸药.md "wikilink")，是银与硝酸在[乙醇](../Page/乙醇.md "wikilink")()的存在下反应得到的，用于[雷管](../Page/雷管.md "wikilink")。其他危险易爆的银化合物包括[叠氮化银](../Page/叠氮化银.md "wikilink")
()，由硝酸银与[叠氮化钠](../Page/叠氮化钠.md "wikilink")
()反应得到，\[18\]还有[乙炔银](../Page/乙炔银.md "wikilink")()，由硝酸银或[银氨溶液与](../Page/银氨溶液.md "wikilink")[乙炔](../Page/乙炔.md "wikilink")()反应得到。

卤化银晶体曝光后形成的经[还原剂](../Page/还原剂.md "wikilink")，如[氢醌](../Page/氢醌.md "wikilink")、[米吐尔](../Page/米吐尔.md "wikilink")(4-(甲氨基)苯酚硫酸氢盐)或[抗坏血酸的](../Page/抗坏血酸.md "wikilink")[碱性溶液显影处理后](../Page/碱性.md "wikilink")，曝光的卤化银被还原成金属银。硝酸银的碱性溶液（银氨溶液）可被[还原糖](../Page/还原糖.md "wikilink")，如[葡萄糖等还原为金属银](../Page/葡萄糖.md "wikilink")，这个反应用于制造银[镜](../Page/镜.md "wikilink")，以及玻璃的内表面。卤化银可溶于[硫代硫酸钠](../Page/硫代硫酸钠.md "wikilink")()溶液，因此硫代硫酸钠可作为，去除显影后感光乳剂上多余的卤化银。\[19\]

在[溴化钾](../Page/溴化钾.md "wikilink")()的存在下，金属银可被强[氧化剂如](../Page/氧化剂.md "wikilink")[高锰酸钾](../Page/高锰酸钾.md "wikilink")()或[重铬酸钾](../Page/重铬酸钾.md "wikilink")()侵蚀；这些化合物在摄影中用于[漂白可见影像](../Page/漂白.md "wikilink")，将其转化为卤化银，既可以被硫代硫酸钠去除，又可以重新显影以加强原始的影像。在过量的[氰根离子](../Page/氰化物.md "wikilink")(CN<sup>-</sup>)存在下，[氰化银](../Page/氰化银.md "wikilink")()可以形成可溶于水的氰[配合物](../Page/配合物.md "wikilink")(<sup>-</sup>)。银的氰配合物溶液用于[电镀银](../Page/电镀.md "wikilink")。\[20\]

### 其它价态化合物

银还能形成其它价态的化合物，如[氟化亚银](../Page/氟化亚银.md "wikilink")（Ag<sub>2</sub>F）、[二氟化银](../Page/二氟化银.md "wikilink")（AgF<sub>2</sub>）、[一氧化银](../Page/一氧化银.md "wikilink")（AgO）等。

## 在生物中作用

银的离子以及化合物对某些[细菌](../Page/细菌.md "wikilink")、[病毒](../Page/病毒.md "wikilink")、[藻类以及](../Page/藻类.md "wikilink")[真菌显现出毒性](../Page/真菌.md "wikilink")，但对人体却几乎是完全无害的。银的这种杀菌效应使得它在活体外就能够将生物杀死。然而，银制品的测试以及标准化却存在很大难度。

[希波克拉底曾经有描述银在治疗和防止疾病方面的功用](../Page/希波克拉底.md "wikilink")。[腓尼基人曾经用银瓶子来盛放](../Page/腓尼基人.md "wikilink")[水](../Page/水.md "wikilink")、[酒和](../Page/酒.md "wikilink")[醋](../Page/醋.md "wikilink")，以此防止这些液体變壞。20世纪初期，人们也曾把银币放在[牛奶](../Page/牛奶.md "wikilink")，以此来延长牛奶的保鲜期。银的杀菌机制长期以来一直为人们所争论探讨，但至此还没有确凿的定论。其中一个很好的例子是微动力效应，成功的解释了银离子对[微生物的作用](../Page/微生物.md "wikilink")，但却不能解释其对病毒的作用。

[凝胶以及](../Page/凝胶.md "wikilink")[绷带大量使用銀](../Page/绷带.md "wikilink")。银的抗菌性来源于银离子。由于银离子可以和一些微生物用于呼吸的物质（比如一些含有[氧](../Page/氧.md "wikilink")、[硫](../Page/硫.md "wikilink")、[氮](../Page/氮.md "wikilink")[元素的](../Page/元素.md "wikilink")[分子](../Page/分子.md "wikilink")）形成强烈的[结合键](../Page/结合键.md "wikilink")，以此使得这些物质不能为微生物所利用，从而使得微生物[窒息而亡](../Page/窒息.md "wikilink")。

在[抗生素發明之前](../Page/抗生素.md "wikilink")，银的[相关化合物曾在](../Page/銀化合物.md "wikilink")[第一次世界大战时用于防止感染](../Page/第一次世界大战.md "wikilink")。

银作为效用广泛的抗菌剂正在进行新的应用。其中一方面就是将[硝酸银溶于](../Page/硝酸银.md "wikilink")[海藻酸盐中](../Page/海藻酸盐.md "wikilink")，用于防止伤口的感染，尤其是烧伤伤口的感染。2007年，一个公司设计出一种表面镀上银的玻璃杯，这种杯子号称具有良好的抗菌性。除此之外，美国食品和药品管理协会（FDA）最近也审批通过了一种内层镀银的导气管的应用，因为研究表明这种导气管能够有效的降低[导气管型肺炎](../Page/导气管型肺炎.md "wikilink")。

銀並不會對人的身體產生毒性，但長期接觸銀金屬和無毒銀化合物也會引致[銀質沉著症](../Page/銀質沉著症.md "wikilink")(Argyria)。因為身體[色素產生變化](../Page/色素.md "wikilink")，[皮膚表面會顯出灰藍色](../Page/皮膚.md "wikilink")，雖無毒性，但會影響外觀。

## 參見

  - [銀子](../Page/銀子.md "wikilink")

## 参考资料

[Category:过渡金属](../Category/过渡金属.md "wikilink")
[银](../Category/银.md "wikilink")
[5K](../Category/第5周期元素.md "wikilink")
[5K](../Category/化学元素.md "wikilink")
[Category:电导体](../Category/电导体.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.

16.

17.
18.

19.

20.