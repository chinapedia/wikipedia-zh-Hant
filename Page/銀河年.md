[Deathvalleysky_nps_big.jpg](https://zh.wikipedia.org/wiki/File:Deathvalleysky_nps_big.jpg "fig:Deathvalleysky_nps_big.jpg")

**銀河年**（），也稱為**宇宙年**（），是[太陽系在軌道上繞著](../Page/太陽系.md "wikilink")[銀河系中心](../Page/銀河系.md "wikilink")[公轉一周的時間](../Page/公轉.md "wikilink")\[1\]，估計在2.25億至2.5億“[地球年](../Page/地球年.md "wikilink")”之間\[2\]。

**銀河年**相較於[地球年](../Page/年.md "wikilink")（地球在軌道上繞著太陽轉一圈的時間間隔）和[月](../Page/月.md "wikilink")（月球在軌道上繞著地球轉一圈的時間間隔），可以提供一個更“易於理解”的單位來說明[宇宙和地質時代的期間](../Page/宇宙.md "wikilink")。相較之下，“十億年”（）的尺度對於[地質是太大的單位而不適用](../Page/地質.md "wikilink")，“百萬年”（）又太小會呈現相當大的數字。\[3\]

## 銀河年的歷史年表

這份年表開始於[太陽系的誕生](../Page/太陽系.md "wikilink")，現在被認為是第20個銀河年。

  - **4 GY**：地球出現海洋
  - **5 GY**：[生物出現](../Page/生物.md "wikilink")
  - **6 GY**：[原核生物出現](../Page/原核生物.md "wikilink")
  - **7 GY**：[細菌出現](../Page/細菌.md "wikilink")
  - **10 GY**：穩定的[陸地出現](../Page/陸地.md "wikilink")
  - **13 GY**：[真核生物出現](../Page/真核生物.md "wikilink")
  - **16 GY**：[多細胞生物體出現](../Page/多細胞.md "wikilink")
  - **17.8 GY**：[寒武紀大爆發](../Page/寒武紀大爆發.md "wikilink")
  - **19 GY**：[大滅絕](../Page/大滅絕.md "wikilink")
  - **19.6 GY**：[白垩纪-第三纪灭绝事件](../Page/白垩纪-第三纪灭绝事件.md "wikilink")
  - **20.0 GY**：現代

## 相關條目

  - [特別的測量單位列表](../Page/特別的測量單位列表.md "wikilink")

## 參考資料

[Category:銀河系](../Category/銀河系.md "wikilink")
[Category:時間單位](../Category/時間單位.md "wikilink")

1.  [Astronomy Knowledge
    Base](http://www.csi.uottawa.ca:4321/astronomy/index.html#cosmicyear)

2.
3.  [Geologic Time
    Scale](http://www.vendian.org/mncharity/dir3/geologic_time_galactic/)