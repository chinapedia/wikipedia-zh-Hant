[Ngan_Wan_Estate_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Ngan_Wan_Estate_\(Hong_Kong\).jpg "fig:Ngan_Wan_Estate_(Hong_Kong).jpg")
[Ngan_Wan_Estate_Open_Space.jpg](https://zh.wikipedia.org/wiki/File:Ngan_Wan_Estate_Open_Space.jpg "fig:Ngan_Wan_Estate_Open_Space.jpg")
[Ngan_Wan_Estate_Sitting_Area.jpg](https://zh.wikipedia.org/wiki/File:Ngan_Wan_Estate_Sitting_Area.jpg "fig:Ngan_Wan_Estate_Sitting_Area.jpg")
**銀灣邨**（）是[香港的一個第二代鄉村型設計的](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，項目編號RH02，位於[新界](../Page/新界.md "wikilink")[大嶼山東的](../Page/大嶼山.md "wikilink")[梅窩](../Page/梅窩.md "wikilink")，並於1988年入伙。由房屋署總建築師設計，並現由卓安顧問有限公司負責屋邨管理。

## 屋邨資料

### 樓宇

| 樓宇名稱（座號）\[1\] | 樓宇類型 | 落成年份 |
| ------------- | ---- | ---- |
| 銀日樓（第1座）      | 鄉村型  | 1988 |
| 銀月樓（第2座）      |      |      |
| 銀星樓（第3座）      |      |      |
| 銀虹樓（第4座）      |      |      |

## 商舖

  - 邨內只有一間不定時開業的[士多](../Page/士多.md "wikilink")（位於銀月樓地下），但銀灣邨距離碼頭及[梅窩鄉事會路商舖均不多於](../Page/梅窩鄉事會路.md "wikilink")10分鐘路程，故購物亦甚為方便
  - [香港家庭福利會分會](../Page/香港家庭福利會.md "wikilink")（銀月樓地下102及116室）

## 設施

  - 升降機（銀星樓除外）
  - 天橋，連接各座樓宇
  - 單車停泊處
  - 遊樂場1個
  - 健體區1個
  - 卵石路步行徑
  - 廣場，位於銀月樓及銀星樓之間，平日用作停泊單車，節慶時則為慶祝活動（如中秋晚會）場地
  - 垃圾站（銀虹樓旁）
  - 巴士站（只有進梅窩舊墟的班次會經此站）

Ngan Wan Estate Playground.jpg|兒童遊樂場 Ngan Wan Estate Gym Zone and Pebble
Walking Trail.jpg|健體區及卵石路步行徑

## 鄰近建築

  - [銀蔚苑](../Page/銀蔚苑.md "wikilink")
  - [銀河苑](../Page/銀河苑.md "wikilink")
  - [銀河灣畔](../Page/銀河灣畔.md "wikilink")
  - [梅窩消防局](../Page/梅窩消防局.md "wikilink")
  - [銀景中心](../Page/銀景中心.md "wikilink")
  - [梅窩政府合署](../Page/梅窩政府合署.md "wikilink")
  - [富瑤小築 銀濤軒](../Page/富瑤小築_銀濤軒.md "wikilink")
  - [梅窩市政大廈](../Page/梅窩市政大廈.md "wikilink")
  - [梅窩游泳池](../Page/梅窩游泳池.md "wikilink")
  - [銀礦廣場](../Page/銀礦廣場.md "wikilink")
  - [銀河花園](../Page/銀河花園.md "wikilink")
  - [銀河](../Page/銀河_\(香港\).md "wikilink")

## 交通

## 站外鏈結

  - [房屋署銀灣邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2832)

[en:Public housing estates on Lantau Island\#Ngan Wan
Estate](../Page/en:Public_housing_estates_on_Lantau_Island#Ngan_Wan_Estate.md "wikilink")

[Category:梅窩](../Category/梅窩.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")
[Category:香港使用中央石油氣的屋苑](../Category/香港使用中央石油氣的屋苑.md "wikilink")

1.  [1](http://www.rvd.gov.hk/doc/tc/nt.pdf)