[Jetco_logo_2011.gif](https://zh.wikipedia.org/wiki/File:Jetco_logo_2011.gif "fig:Jetco_logo_2011.gif")
[Jetco_Logo.jpg](https://zh.wikipedia.org/wiki/File:Jetco_Logo.jpg "fig:Jetco_Logo.jpg")
**銀通**（Joint Electronic Teller Services Limited,
JETCO），全名為**銀聯通寶有限公司**，是[香港和](../Page/香港.md "wikilink")[澳門最大的銀行](../Page/澳門.md "wikilink")[櫃員機網絡系統](../Page/自動櫃員機.md "wikilink")，現時有超過3,000部櫃員機支援銀通，它們彼此互通，銀行客戶可在別家銀行的銀通櫃員機提款，並在同一城市內均可免收手續費，以免卻只限在自家銀行櫃員機提款的不便。不過，大部分銀行對客戶於本行以外之銀通櫃員機每次提款設有下限，比如是中銀香港要求客戶在本行以外之銀通櫃員機提款每次最少提取300元，否則會於輸入銀碼後取消交易。而於本行的櫃員機雖然沒有下限，但大部分在香港的自動櫃員機所提供的最少值面鈔均是100元。

銀通成立於1982年，由香港5間銀行所組成，計有[中國銀行香港分行](../Page/中國銀行.md "wikilink")（現為[中銀香港](../Page/中銀香港.md "wikilink")）、[東亞銀行](../Page/東亞銀行.md "wikilink")、[浙江第一銀行](../Page/浙江第一銀行.md "wikilink")（已併入[華僑永亨銀行之內](../Page/華僑永亨銀行.md "wikilink")）、[上海商業銀行及](../Page/上海商業銀行.md "wikilink")[招商永隆銀行](../Page/永隆銀行.md "wikilink")。除[香港上海匯豐銀行和](../Page/香港上海匯豐銀行.md "wikilink")[恆生銀行自設](../Page/恆生銀行.md "wikilink")[易通財](../Page/易通財.md "wikilink")（ETC）提款網絡、[AEON](../Page/永旺百貨.md "wikilink")、澳門[匯業銀行和少數在港澳規模較細的外資銀行](../Page/匯業銀行.md "wikilink")（如香港分行）自設網絡外，全香港和澳門絕大部份的櫃員機均支援銀通服務。

銀通曾與[中國大陸的](../Page/中國大陸.md "wikilink")[銀聯網絡互通](../Page/銀聯.md "wikilink")，當時銀通提款卡用戶能夠從國內各大城市的銀聯櫃員機提取[人民幣現金](../Page/人民幣.md "wikilink")，亦能從香港的銀通櫃員機每日提取不多於4,500港元現金。但从2006年1月1日起，香港银通卡已不能在大陆的银联提款机上使用，若要在中国大陆的柜员机上使用香港银行卡，需要使用香港很多银行发行的[银联卡](../Page/银联.md "wikilink")，部分中资银行的香港分支机构（比如建设银行、招商银行）亦有发行银通／银联通用的“两地卡”。

2012年5月21日銀聯入股銀通成為策略伙伴，開拓中港澳銀行卡的跨境支付服務，銀聯將會佔銀通1/32股權。

2013年9月2日，銀聯通寶有限公司（Joint Electronic Teller Services
Limited）以7,000萬元，按單位總建築面積7,334方呎計算，平均呎價9,545元。購入觀塘宏基資本大廈5樓3至8號室。

## 會員銀行

香港

  - [中國銀行（香港）有限公司](../Page/中國銀行（香港）有限公司.md "wikilink")
  - [交通銀行股份有限公司](../Page/交通銀行.md "wikilink")
  - [中信銀行（國際）有限公司](../Page/中信銀行（國際）.md "wikilink")
  - [中國建設銀行（亞洲）股份有限公司](../Page/中國建設銀行（亞洲）股份有限公司.md "wikilink")
  - [招商銀行香港分行](../Page/招商銀行.md "wikilink")
  - [集友銀行有限公司](../Page/集友銀行有限公司.md "wikilink")
  - [創興銀行有限公司](../Page/創興銀行有限公司.md "wikilink")
  - [花旗銀行（香港）有限公司](../Page/花旗銀行（香港）有限公司.md "wikilink")
  - [大新銀行有限公司](../Page/大新銀行有限公司.md "wikilink")
  - [星展銀行（香港）有限公司](../Page/星展銀行（香港）有限公司.md "wikilink")
  - [富邦銀行（香港）有限公司](../Page/富邦銀行（香港）有限公司.md "wikilink")
  - [中國工商銀行（亞洲）有限公司](../Page/中國工商銀行（亞洲）有限公司.md "wikilink")
  - [南洋商業銀行有限公司](../Page/南洋商業銀行有限公司.md "wikilink")
  - [華僑永亨銀行股份有限公司](../Page/華僑永亨銀行.md "wikilink")
  - [大眾銀行（香港）有限公司](../Page/大眾銀行（香港）有限公司.md "wikilink")
  - [上海商業銀行](../Page/上海商業銀行.md "wikilink")
  - [渣打銀行（香港）有限公司](../Page/渣打銀行_\(香港\).md "wikilink")
  - [東亞銀行有限公司](../Page/東亞銀行有限公司.md "wikilink")
  - [招商永隆銀行有限公司](../Page/永隆銀行.md "wikilink")

澳門

  - [澳門商業銀行](../Page/澳門商業銀行.md "wikilink")
  - [澳門大西洋銀行](../Page/大西洋銀行.md "wikilink")
  - [澳門華僑永亨銀行股份有限公司](../Page/澳門華僑永亨銀行.md "wikilink")
  - [中國銀行澳門分行](../Page/中國銀行澳門分行.md "wikilink")
  - [中國建設銀行股份有限公司澳門分行](../Page/中國建設銀行澳門分行.md "wikilink")
  - [中國工商銀行（澳門）股份有限公司](../Page/工銀澳門.md "wikilink")
  - [澳門國際銀行](../Page/澳門國際銀行.md "wikilink")
  - [大豐銀行](../Page/大豐銀行.md "wikilink")
  - [東亞銀行澳門分行](../Page/東亞銀行澳門分行.md "wikilink")

## 外部連結

  - [「銀通」官方網站](http://www.jetco.com.hk)

[Category:香港金融公司](../Category/香港金融公司.md "wikilink")
[Category:中國銀行](../Category/中國銀行.md "wikilink")
[Category:香港購物](../Category/香港購物.md "wikilink")
[Category:1982年成立的公司](../Category/1982年成立的公司.md "wikilink")