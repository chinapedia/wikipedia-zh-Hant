**銀霞**（，\[1\]），本名**章家興**，[臺灣](../Page/中華民國.md "wikilink")[玉女歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")、主持人。1977年主唱電影主題曲紅遍台灣及星馬地區，1979年以《回答》（《蘭花草》）專輯銷售破百萬成為台灣知名[校園民歌歌手](../Page/校園民歌.md "wikilink")，也贏得1980年代「最受歡迎校園民謠歌手」頭銜。1980年[春節](../Page/春節.md "wikilink")[台灣電視公司與香港](../Page/台灣電視公司.md "wikilink")[TVB首次合作](../Page/電視廣播有限公司.md "wikilink")[除夕特別節目](../Page/除夕.md "wikilink")，銀霞代表台視與[沈殿霞代表TVB聯合主持](../Page/沈殿霞.md "wikilink")，當時「雙霞」是港台當紅藝人，創下港台電視首次同時轉播。1980年三月，銀霞接拍第一部電影《妳那好冷小手》，擔任女主角。

銀霞演唱成名的歌曲有、、、、、、、、、、、、、、、、、、、等。

## 生平

銀霞出身於[軍官世家](../Page/軍官.md "wikilink")，[祖父](../Page/祖父.md "wikilink")[章鴻春是](../Page/章鴻春.md "wikilink")[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[騎兵學校](../Page/陸軍騎兵學校.md "wikilink")[中將校長](../Page/中將.md "wikilink")，也曾是[軍政部](../Page/軍政部.md "wikilink")[中將](../Page/中將.md "wikilink")、司令長官部中將高級參謀、南京要塞司令、駐[日本首席](../Page/日本.md "wikilink")[武官等軍職](../Page/武官.md "wikilink")；父親[章沛霖是](../Page/章沛霖.md "wikilink")[日本士官學校畢業](../Page/日本士官學校.md "wikilink")[上校](../Page/上校.md "wikilink")，曾任[中華民國駐日本大使館武官](../Page/中華民國駐日本大使館.md "wikilink")；母親張鳳琴，年輕時在大陸曾經也是演員；姊姊是台灣影壇第一代玉女巨星[甄珍](../Page/甄珍.md "wikilink")。銀霞自[初中起由台灣至](../Page/初中.md "wikilink")[美國求學長大](../Page/美國.md "wikilink")，1977年回到台灣灌第一張個人專輯唱片《秋詩篇篇》踏入[演藝圈](../Page/演藝圈.md "wikilink")。曾就讀[台北美國學校](../Page/台北美國學校.md "wikilink")，高中畢業於舊金山天主教女子高中，大學唸[舊金山馬林大學](../Page/舊金山.md "wikilink")。

銀霞出道時的學生造型齊眉瀏海、萬年不變的長度，甚至她淡出演藝圈後，網友分享在路上與她巧遇經驗時表示「連髮型都是清湯掛麵的學生頭」。其實身為當年大明星甄珍的妹妹，銀霞唸書時期已有明星樣，由於雙親不贊成銀霞走入演藝界，總以「年紀太小」來推託對銀霞的邀約，僅讓她錄製《秋詩篇篇》唱片後就連忙將她帶回美國唸書。命運就是這麼奇妙，當時銀霞利用學校放暑假返台錄製校園民歌唱片，其主打歌就是傳唱至今的，因父親突然過世，她沒有宣傳《回答》專輯，就返美奔喪；反而自顧成長茁壯，成為當年最流行暢銷歌曲。之後，在電影作曲家[左宏元的遊說之下](../Page/左宏元.md "wikilink")，1980年銀霞開始嘗試演電影，前後共拍攝十幾部電影。

銀霞與母親一直相依為命，相互扶持。母親是銀霞精神上和生活中最大依靠，也是她演藝事業上給她最多幫助的人。但2008年，她母親因被台北市[宏恩綜合醫院王豐明醫師的](../Page/宏恩綜合醫院.md "wikilink")[醫療疏失耽誤病情過世](../Page/醫療疏失.md "wikilink")。據悉，銀霞過度悲傷及思念母親而患[憂鬱症](../Page/憂鬱症.md "wikilink")。2011年1月22日《[時報周刊](../Page/時報周刊.md "wikilink")》報導，銀霞認為[台北地檢署刑事判決有不公正之處](../Page/台北地檢署.md "wikilink")，極度受傷而足不出戶，也不和外界往來。2012年9月12日，[台北地方法院判決宏恩綜合醫院及王豐明醫師確實有疏失](../Page/台北地方法院.md "wikilink")，必須連帶賠償[新台幣](../Page/新台幣.md "wikilink")88萬餘元\[2\]。原本決定次年復出，因接對方再上訴，復出計劃延後\[3\]。2013年5月22日，雙方在高等法院協調達成和解\[4\]。

長達六年訴訟，銀霞身心疲憊：據2015年8月15日《[中國時報](../Page/中國時報.md "wikilink")》刊載，銀霞因母親過世等因素，一直處於憂鬱悲傷狀態。據悉2014年她個人因台大醫院陳姓主任醫師所進行之微創手術嚴重疏失，造成她在2017年又動第3次重大手術至今身心受創嚴重，而無法復出。\[5\]。

## 節目主持人

銀霞英語流利，氣質高雅、玉女形像無緋聞，自1980年至1983年[台灣電視公司](../Page/台灣電視公司.md "wikilink")（台視）[除夕特別節目都由她擔任主持](../Page/除夕.md "wikilink")，她曾在台視製作三次個人電視專輯節目《銀霞專輯》。

1980年開始，銀霞屢屢奉派主持大型頒獎典禮。曾主持1980年[第15屆金鐘獎](../Page/第15屆金鐘獎.md "wikilink")、1983年[亞太影展](../Page/亞太影展.md "wikilink")、1987年[金鼎獎](../Page/金鼎獎.md "wikilink")、[第28屆亞太影展擔任頒獎人及](../Page/第28屆亞太影展.md "wikilink")1989年主持[第26屆金馬獎](../Page/第26屆金馬獎.md "wikilink")。1986年在[中華電視台](../Page/中華電視台.md "wikilink")（華視）綜藝節目《[週末派](../Page/週末派.md "wikilink")》中主持〈光鮮生活〉單元。1987年她在台視主持電視專輯《銀霞-單身貴族》。1988年至1989年在中華電視台與[周華健主持](../Page/周華健.md "wikilink")《勁運大觀》，又主持1987－1989年度國際佳樂小姐選美晚會由華視實況轉播。1989年中華電視台再度邀她與香港[無綫電視的](../Page/無綫電視.md "wikilink")[曾志偉搭檔主持除夕夜特別節目](../Page/曾志偉.md "wikilink")，在香港[紅磡體育館錄製](../Page/香港體育館.md "wikilink")，她跑遍香港電影片廠訪問巨星[周潤發](../Page/周潤發.md "wikilink")、[成龍](../Page/成龍.md "wikilink")、[劉德華等](../Page/劉德華.md "wikilink")。她主持完[第26屆金馬獎便逐漸淡出](../Page/第26屆金馬獎.md "wikilink")[演藝圈](../Page/演藝圈.md "wikilink")。2013年她一人低調出席在[第50屆金馬獎頒獎典禮觀賞其姊領](../Page/第50屆金馬獎.md "wikilink")[金馬獎終身成就獎](../Page/金馬獎終身成就獎.md "wikilink")。2014年5月在[國立國父紀念館以舞台總策劃身份製作一場慈善愛心晚會](../Page/國立國父紀念館.md "wikilink")。

銀霞當年主演的電影和主唱歌曲在台灣及[星](../Page/新加坡.md "wikilink")[馬地區頗受歡迎](../Page/馬來西亞.md "wikilink")。一直有製作公司邀她復出開演唱會，而前幾年她被媒體報導是她與宏恩醫院在法院火拚的「小蝦米對大鯨魚訴訟」。近期民歌演唱會確未見她的出現，觀眾都期待她的個人演唱會。

## 音樂專輯

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>專輯名稱</strong></p></td>
<td><p><strong>發行公司/機構</strong></p></td>
<td><p><strong>語言</strong></p></td>
<td><p><strong>曲目</strong></p></td>
</tr>
<tr class="even">
<td><p>1977年</p></td>
<td><p>《秋詩篇篇》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1977年</p></td>
<td><p>《台北66》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1977年</p></td>
<td><p>《日落北京城》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1979年</p></td>
<td><p>《回答》（《蘭花草》）</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1980年</p></td>
<td><p>《西風的故鄉》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1980年</p></td>
<td><p>《妳那好冷的小手》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td><p>《雲知道你是誰》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1981年</p></td>
<td><p>《期待》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1982年</p></td>
<td><p>《結-{zh-tw:‧;zh-cn:·}-Say Yes My Boy》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1983年</p></td>
<td><p>《愛迷惑我》</p></td>
<td><p>海山唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p>《昨夜沒有留下夢》</p></td>
<td><p>東尼機構</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td><p>《風鈴寄情》</p></td>
<td><p>東尼機構</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1986年</p></td>
<td><p>《單身貴族》</p></td>
<td><p>銀河唱片</p></td>
<td><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 電影

|        |           |        |
| ------ | --------- | ------ |
| **年代** | **片名**    | **角色** |
| 1980   | 《你那好冷的小手》 |        |
| 1981   | 《雲知道妳是誰》  |        |
| 1981   | 《瘋狂世界》    |        |
| 1981   | 《江湖小浪子》   |        |
| 1981   | 《第二次一對一》  |        |
| 1981   | 《摩登女生》    |        |
| 1982   | 《第二次偶然》   |        |
| 1982   | 《霹靂大妞》    |        |
| 1982   | 《神勇女煞星》   |        |
| 1982   | 《瘋狂少女營》   |        |
| 1983   | 《金門女兵》    |        |
| 1984   | 《寒江秋水》    |        |
| 1984   | 《洪隊長》     |        |
| 1984   | 《獵豔高手》    |        |
|        |           |        |

## 廣告代言

  - [佳麗寶化妝品](../Page/花王.md "wikilink")（1981）
  - JoJoBa洗髮精（1982）
  - JoJoBa洗髮精（1986）
  - [上海商業儲蓄銀行](../Page/上海商業儲蓄銀行.md "wikilink")（1992）

## 參考資料

## 外部連結

  -
  -
  -
  - [中文電影資料庫─銀霞](http://www.dianying.com/ft/person/YinXia)

[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:台灣校園民歌歌手](../Category/台灣校園民歌歌手.md "wikilink")
[Category:台灣電視主持人](../Category/台灣電視主持人.md "wikilink")
[Category:台灣電影女演員](../Category/台灣電影女演員.md "wikilink")
[Category:文藝片演員](../Category/文藝片演員.md "wikilink")
[Category:浙江](../Category/浙江.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[S](../Category/章姓.md "wikilink")
[Category:亞太影展獲獎者](../Category/亞太影展獲獎者.md "wikilink")
[Category:臺灣新教徒](../Category/臺灣新教徒.md "wikilink")

1.
2.  張文川、林相美.[3度診斷沒查出～甄珍、銀霞母癌逝
    宏恩判賠88萬](http://news.ltn.com.tw/news/life/paper/614609).自由時報.2012-09-13

3.  李志展、高子航.[銀霞
    續打亡母官司復出喊卡「這幾年只有慘」](http://ent.appledaily.com.tw/section/article/headline/20120930/34543171).蘋果日報.2012-09-30

4.  丁牧群、李志展.[銀霞6年官司累了
    亡母誤診案和解落幕](http://www.appledaily.com.tw/appledaily/article/headline/20130523/35035428/).蘋果日報.2013-05-23

5.  張怡文.[星期人物》銀霞憂鬱重病纏身
    返歌壇夢已遠](http://www.chinatimes.com/realtimenews/20150815002399-260404).中時電子報.2015-08-15