**銀龍屬**（[屬名](../Page/學名.md "wikilink")：）是[蜥腳下目](../Page/蜥腳下目.md "wikilink")[薩爾塔龍科的一](../Page/薩爾塔龍科.md "wikilink")[屬](../Page/屬.md "wikilink")，是種[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生活於[上白堊紀的](../Page/白堊紀.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")（[阿根廷與](../Page/阿根廷.md "wikilink")[烏拉圭](../Page/烏拉圭.md "wikilink")），約9000萬年前。牠是最大型的[恐龍之一](../Page/恐龍.md "wikilink")，身長估計約20-30公尺長，約8公尺高，體重約80[公噸](../Page/公噸.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")「*A.
superbus*」，由[理查德·莱德克](../Page/理查德·莱德克.md "wikilink")（Richard
Lydekker）在1893年根據一塊巨型前肢化石而正式命名\[1\]。屬名的*argyros*意為「銀」，由於牠們的[化石是在阿根廷被發現](../Page/化石.md "wikilink")，而阿根廷的國名又有「銀」的意思，故被稱為銀龍；種名在[拉丁文意為](../Page/拉丁文.md "wikilink")「輝煌的」。

[正模標本](../Page/正模標本.md "wikilink")（編號MLP
77-V-29-1）是一個巨型左前肢骨頭。之後發現的一些化石，也被編入於銀龍，例如：肩帶、前肢、[恥骨](../Page/恥骨.md "wikilink")、[股骨](../Page/股骨.md "wikilink")、以及一些[背椎與](../Page/背椎.md "wikilink")[尾椎](../Page/尾椎.md "wikilink")\[2\]。

## 參考資料

## 外部連結

  - [*Argyrosaurus* page at the Dinosaur
    Encyclopedia](http://web.me.com/dinoruss/de_4/5a6d7a7.htm)
  - [An image of
    *Argyrosaurus*](http://lesdinos.free.fr/argyrosaurus.jpg)
  - [*Argyrosaurus* in the Paleobiology
    Database](http://paleodb.org/cgi-bin/bridge.pl?action=checkTaxonInfo&taxon_no=38686)
  - [Artist's impression of *Argyrosaurus*, with scaled holotype
    forelimb](http://2.bp.blogspot.com/_QC0xe56GvUo/Sv3AMqdgRuI/AAAAAAAADaA/r00FuWf-RCc/s1600-h/Argyrosaurus+0.7+BLUE+fingersBIG.JPG)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:薩爾塔龍科](../Category/薩爾塔龍科.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")

1.  Lydekker, R. (1893). "Contributions to the study of the fossil
    vertebrates of Argentina. I. The dinosaurs of Patagonia", *Anales
    del Museo de la Plata, Seccion de Paleontologia* **2**: 1-14
2.   *Argyrosaurus* at DinoData