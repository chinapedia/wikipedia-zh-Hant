**銅長尾雉**（[學名](../Page/學名.md "wikilink")：**）是[雉科](../Page/雉科.md "wikilink")[長尾雉屬](../Page/長尾雉屬.md "wikilink")[鳥類的一種](../Page/鳥類.md "wikilink")，為日本的[特有种](../Page/特有种.md "wikilink")，但在當地仍非常罕見，[日語中常稱其為](../Page/日語.md "wikilink")**山鳥**。

銅長尾雉的體長達136釐米。分佈于日本的山地森林中，以昆蟲、樹根、樹葉和穀物為食。

## 亞種

[Naturalis_Biodiversity_Center_-_ZMA.AVES.56823_-_Syrmaticus_soemmerringii_Temminck,_1830_Temminck,_1830_-_Phasianidae_-_skin_specimen.jpg](https://zh.wikipedia.org/wiki/File:Naturalis_Biodiversity_Center_-_ZMA.AVES.56823_-_Syrmaticus_soemmerringii_Temminck,_1830_Temminck,_1830_-_Phasianidae_-_skin_specimen.jpg "fig:Naturalis_Biodiversity_Center_-_ZMA.AVES.56823_-_Syrmaticus_soemmerringii_Temminck,_1830_Temminck,_1830_-_Phasianidae_-_skin_specimen.jpg")[自然生物多樣性中心收藏的雄鳥標本](../Page/自然生物多樣性中心.md "wikilink")\]\]
目前認為銅長尾雉有五個亞種，皆分布於日本：

  - **銅長尾雉南九州亞種**（，學名： <small>(Dresser,
    1902)</small>）：分布於[九州島南部](../Page/九州島.md "wikilink")。
  - **銅長尾雉西南亞種**（，學名： <small>([Kuroda](../Page/黑田長禮.md "wikilink"),
    1919)</small>）：分布於[本州島西部與](../Page/本州島.md "wikilink")[四國島](../Page/四國島.md "wikilink")。
  - **銅長尾雉北部亞種**（，學名： <small>(Gould, 1866)</small>）：分布於本州島北半部。
  - **銅長尾雉指名亞種**（，學名： <small>(Temminck, 1830)</small>）：分布於九州島北部和中部。
  - **銅長尾雉南本州亞種**（，學名： <small>([Kuroda](../Page/黑田長禮.md "wikilink"),
    1919)</small>）：本州島太平洋側南部
    （[房總半島](../Page/房總半島.md "wikilink")、[伊豆半島和](../Page/伊豆半島.md "wikilink")[紀伊半島等地](../Page/紀伊半島.md "wikilink")）。

## 參考

  -
[Category:长尾雉属](../Category/长尾雉属.md "wikilink")
[Category:日本鳥類](../Category/日本鳥類.md "wikilink")