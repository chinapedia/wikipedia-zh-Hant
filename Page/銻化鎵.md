**銻化鎵**為[半導體材料之一](../Page/半導體.md "wikilink")，其中[分子裡包含了](../Page/分子.md "wikilink")[銻和](../Page/銻.md "wikilink")[鎵](../Page/鎵.md "wikilink")，屬於[III](../Page/硼族元素.md "wikilink")-[V族](../Page/氮族元素.md "wikilink")，其[能隙為](../Page/能隙.md "wikilink")
0.726[ev](../Page/電子伏特.md "wikilink")，[晶格常數是](../Page/晶格常數.md "wikilink")0.61
nm。銻化鎵通常可以用來做、紅外[發光二極體](../Page/發光二極體.md "wikilink")、[電晶體](../Page/電晶體.md "wikilink")、[雷射二極體等用具](../Page/雷射二極體.md "wikilink")。

## 參見

  - [銻化鋁](../Page/銻化鋁.md "wikilink")
  - [碘化锑](../Page/碘化锑.md "wikilink")

## 参考文献

  - [NSM](http://www.ioffe.rssi.ru/SVA/NSM/Semicond/GaSb/)
  - [National Compound Semiconductor
    Roadmap](https://web.archive.org/web/20071206070335/http://www.onr.navy.mil/sci_tech/information/312_electronics/ncsr/materials/gasb.asp)

[Category:銻化物](../Category/銻化物.md "wikilink")
[Category:镓化合物](../Category/镓化合物.md "wikilink")
[Category:III-V半導體材料](../Category/III-V半導體材料.md "wikilink")