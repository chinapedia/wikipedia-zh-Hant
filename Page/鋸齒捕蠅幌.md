**鋸齒捕蠅幌**（學名：*Roridula
dentata*），一般直接稱作捕蟲樹。為[前食肉植物](../Page/:en:protocarnivorous_plant.md "wikilink")，原生於[南非](../Page/南非.md "wikilink")。

## 外部連結

  - [Roridula dentata -
    塔內植物園](http://www.tbg.org.tw/~tbgweb/cgi-bin/topic.cgi?forum=19&topic=7921&show=625)

  - [Roridula und Pameridea - Kultur und Beschreibung der
    Taupflanze](http://www.drosophyllum.com/deutsch/roridula.htm)介紹捕蟲樹的德文網頁

[Category:非洲食虫植物](../Category/非洲食虫植物.md "wikilink")
[Category:捕蠅幌科](../Category/捕蠅幌科.md "wikilink")
[Category:杜鵑花目](../Category/杜鵑花目.md "wikilink")