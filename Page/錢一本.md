**錢一本**（），字國瑞，號啟新。[南直隸](../Page/南直隸.md "wikilink")[武進](../Page/武進.md "wikilink")（今[江蘇](../Page/江蘇.md "wikilink")[常州](../Page/常州.md "wikilink")）人。[明朝政治人物](../Page/明朝.md "wikilink")。

## 生平

應天府鄉試第三十九名，[萬曆十一年](../Page/萬曆.md "wikilink")（1583年）[進士](../Page/進士.md "wikilink")\[1\]\[2\]\[3\]。任[廬陵](../Page/廬陵.md "wikilink")[知縣](../Page/知縣.md "wikilink")，授[福建道](../Page/福建道.md "wikilink")[御史](../Page/御史.md "wikilink")，劾[江西](../Page/江西.md "wikilink")[巡按](../Page/巡按.md "wikilink")[祝大舟](../Page/祝大舟.md "wikilink")。又曾劾[張居正假](../Page/張居正.md "wikilink")[聖旨以塞言路](../Page/聖旨.md "wikilink")，因上《論相》、《建儲》二疏論政弊，觸怒[神宗](../Page/明神宗.md "wikilink")，被削職為民。築經正堂，研究六經，尤邃於《易》，曾與[顧憲成](../Page/顧憲成.md "wikilink")、[高攀龍等分主](../Page/高攀龍.md "wikilink")[東林講席](../Page/東林書院.md "wikilink")，人稱“東林八君子”。[天啟初追贈](../Page/天啟.md "wikilink")[太僕寺卿](../Page/太僕寺卿.md "wikilink")。\[4\]

## 著作

著有《像像管見》九卷、《像抄》六卷、《續像抄》二卷、《四聖一心錄》六卷。

## 家族

曾祖父[錢銓](../Page/錢銓.md "wikilink")，曾任壽官；祖父[錢稔](../Page/錢稔.md "wikilink")；父親[錢繪](../Page/錢繪.md "wikilink")。母陳氏\[5\]。

## 参考文献

[Category:萬曆十一年癸未科進士](../Category/萬曆十一年癸未科進士.md "wikilink")
[Category:明朝廬陵縣知縣](../Category/明朝廬陵縣知縣.md "wikilink")
[Category:明朝監察御史](../Category/明朝監察御史.md "wikilink")
[Category:常州人](../Category/常州人.md "wikilink")
[Category:明朝經學家](../Category/明朝經學家.md "wikilink")
[Y](../Category/武進段莊錢氏.md "wikilink")

1.
2.
3.
4.  《明史·卷231》：錢一本，字國瑞，武進人。萬曆十一年進士。除廬陵知縣，徵授御史。入臺即發原任江西巡按祝大舟貪墨狀，大舟至遣戍。已，論請從祀曹端、陳真晟、羅倫、羅洪先於文廟。出按廣西。帝以張有德請備大禮儀物，復更冊立東宮期，而申時行柄國，不能匡救。一本上論相、建儲二疏。其論相曰：……疏入，留中。時廷臣相繼爭國本，惟一本言最戇直。帝銜之。無何，杖給事中孟養浩。中旨以養浩所逞之詞根托一本，造言誣君，搖亂大典，遂斥為民。屢薦，卒不用。一本既罷歸，潛心六經，濂、洛諸書，尤研精易學。與顧憲成輩分主東林講席，學者稱啟新先生。里居二十五年，預剋卒日，賦詩誌之，如期而逝。天啟初，贈太僕寺少卿。
5.