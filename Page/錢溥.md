**錢溥**（），[字](../Page/表字.md "wikilink")**原溥**，[直隸](../Page/南直隶.md "wikilink")[松江府](../Page/松江府.md "wikilink")[華亭縣人](../Page/华亭县_\(唐朝\).md "wikilink")，民籍。[明朝政治人物](../Page/明朝.md "wikilink")。

## 生平

[應天府](../Page/应天府_\(明朝\).md "wikilink")[鄉試第七十二名](../Page/鄉試.md "wikilink")。[正統四年](../Page/正统_\(年号\).md "wikilink")（1439年），參加己未科[會試](../Page/會試.md "wikilink")，得[貢士第四名](../Page/貢士.md "wikilink")。[殿試登](../Page/殿試.md "wikilink")[進士第二甲第二名](../Page/進士.md "wikilink")，授翰林院[檢討](../Page/檢討.md "wikilink")，遷[左贊善](../Page/左贊善.md "wikilink")。

[天順六年](../Page/天顺_\(明朝\).md "wikilink")（1462年），出使安南[黎灝為王](../Page/黎灝.md "wikilink")\[1\]，返出知[廣東](../Page/廣東.md "wikilink")[順德縣](../Page/順德縣.md "wikilink")。[成化年間](../Page/成化.md "wikilink")，累官至[南京吏部尚書](../Page/南京吏部尚書.md "wikilink")。工書法，兼楷、行、草，以小楷著稱。諡**文通**。著有《劉公遺愛記》\[2\]。

曾祖父[錢德玉](../Page/錢德玉.md "wikilink")，祖父[錢華仲](../Page/錢華仲.md "wikilink")，父亲[錢惟慶](../Page/錢惟慶.md "wikilink")\[3\]，弟[钱博](../Page/钱博.md "wikilink")\[4\]

## 参考

[Category:明朝翰林院檢討](../Category/明朝翰林院檢討.md "wikilink")
[Category:明朝左春坊左赞善](../Category/明朝左春坊左赞善.md "wikilink")
[Category:明朝翰林院侍讀學士](../Category/明朝翰林院侍讀學士.md "wikilink")
[Category:明朝順德縣知縣](../Category/明朝順德縣知縣.md "wikilink")
[Category:南京吏部尚書](../Category/南京吏部尚書.md "wikilink")
[Category:明朝書法家](../Category/明朝書法家.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[P溥](../Category/錢姓.md "wikilink")
[Category:諡文通](../Category/諡文通.md "wikilink")

1.  [明](../Page/明朝.md "wikilink")[程敏政編](../Page/程敏政.md "wikilink")，[明文衡卷二十八](../Page/明文衡.md "wikilink")‧書：與安南國王書七，一（委廣西南寜府差官齎至本國界），欽差襲封正使翰林院侍讀學士錢溥
    副使禮科給事中王豫 會同欽差司禮監太監柴昇 奉御張榮端肅致書扵攝安南國王...
2.  漢籍電子文獻資料庫-明人傳記資料索引
    ,9118：錢溥，字原溥，號九峰，一號瀛洲遺叟，松江華亭人。正統四年進士，試薔薇露詩，稱旨，授檢討，遷左贊善。天順六年使安南，封黎灝為王，返出知廣東順德縣，成化中累官至南京吏部尚書，致仕卒，年八十一，諡文通。有使交錄、秘閣書目。
3.  《天一阁藏明代科举录选刊·登科录》（《正統四年进士登科录》）
4.  钱唐[倪涛撰](../Page/倪涛.md "wikilink")《[六艺之一录](../Page/六艺之一录.md "wikilink")·卷三百六十五》钱溥字原溥华亭人正统四年进士试蔷薇露诗大见称赏授检讨成化中为南京吏部尚书谥文通【列卿纪】溥小楷行草俱工【书史防要】原溥书盖宋仲温派也硁硁负峭骨所乏者姿耳【王世贞三吴楷法跋】
    钱原溥书饶右丞论书帖行楷书【见式古堂书画彚攷】 钱博 钱愽字原愽溥之弟......