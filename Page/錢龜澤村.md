**錢龜澤村**（）是過去位於[北海道](../Page/北海道.md "wikilink")[渡島支廳東南部的一個](../Page/渡島支廳.md "wikilink")[村落](../Page/村落.md "wikilink")，已於1966年12月1日[併入](../Page/市町村合併.md "wikilink")[函館市](../Page/函館市.md "wikilink")，過去的轄區相當於現在的函館市錢龜澤支所的負責範圍。

## 歷史

  - 1902年4月1日：[龜田郡錢龜澤村](../Page/龜田郡.md "wikilink")、根崎村、志苔村、石崎村合併為錢龜澤村，並成為北海道二級村。\[1\]
  - 1966年12月1日：錢龜澤村被[併入](../Page/市町村合併.md "wikilink")[函館市](../Page/函館市.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

[Category:函館市](../Category/函館市.md "wikilink")
[Category:渡島管內](../Category/渡島管內.md "wikilink")

1.