**錦江町**（）是位於[日本](../Page/日本.md "wikilink")[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[大隅半島南部的一個](../Page/大隅半島.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[肝属郡](../Page/肝属郡.md "wikilink")。成立于2005年3月22日，由舊[大根占町與](../Page/大根占町.md "wikilink")[田代町合併而成](../Page/田代町.md "wikilink")。

由於接鄰[鹿兒島灣](../Page/鹿兒島灣.md "wikilink")（又名**錦江灣**），因此在合併時便以此做為新町的名稱。\[1\]

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬[南大隅郡大根占村和田代村](../Page/南大隅郡.md "wikilink")。
  - 1897年4月1日：南大隅郡被併入[肝屬郡](../Page/肝屬郡.md "wikilink")。
  - 1933年8月1日：大根占村改制為大根占町。\[2\]
  - 1961年4月1日：田代村改制為田代町。
  - 2005年3月22日：大根占町和田代町[合併為](../Page/市町村合併.md "wikilink")**錦江町**。\[3\]

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1910年</p></th>
<th><p>1910年-1930年</p></th>
<th><p>1930年-1950年</p></th>
<th><p>1950年-1970年</p></th>
<th><p>1970年-1990年</p></th>
<th><p>1990年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>南大隅郡<br />
大根占村</p></td>
<td><p>1896年3月29日<br />
肝屬郡大根占村</p></td>
<td><p>1933年8月1日<br />
改制為大根占町</p></td>
<td><p>2005年3月22日<br />
合併為錦江町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南大隅郡<br />
田代村</p></td>
<td><p>1896年3月29日<br />
肝屬郡田代村</p></td>
<td><p>1961年4月1日<br />
改制為田代町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 本地出身之名人

  - [三遊亭歌之介](../Page/三遊亭歌之介.md "wikilink")：[落語家](../Page/落語家.md "wikilink")
  - [川越美和](../Page/川越美和.md "wikilink")：[女演員](../Page/女演員.md "wikilink")
  - [中島啓江](../Page/中島啓江.md "wikilink")：歌劇歌手
  - [大雄辰實](../Page/大雄辰實.md "wikilink")：前[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")

## 參考資料

## 外部連結

  - [大根占町、田代町合併協議會](https://web.archive.org/web/20160305081625/http://www.town.kinko.lg.jp/o-t_gappei/)

  - [舊大根占町官方網頁](https://web.archive.org/web/20160205005428/http://www.town.kinko.lg.jp/onejime/)

  - [舊田代町官方網頁](https://web.archive.org/web/20130630040444/http://www.town.kinko.lg.jp/tashiro/)

<!-- end list -->

1.
2.
3.