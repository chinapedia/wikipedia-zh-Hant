<table>
<thead>
<tr class="header">
<th><p>錫爾河州<br />
Sirdaryo viloyati</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sirdaryo_Viloyati_in_Uzbekistan.svg" title="fig:Sirdaryo_Viloyati_in_Uzbekistan.svg">Sirdaryo_Viloyati_in_Uzbekistan.svg</a></p></td>
</tr>
<tr class="even">
<td><p>基本統計資料</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/首府.md" title="wikilink">首府</a>:</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/面積.md" title="wikilink">面積</a> :</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人口.md" title="wikilink">人口</a>(2005):<br />
 · <a href="../Page/人口密度.md" title="wikilink">密度</a> :</p></td>
</tr>
<tr class="even">
<td><p>主要<a href="../Page/民族.md" title="wikilink">民族</a>:</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ISO_3166-2.md" title="wikilink">行政區代碼</a>:</p></td>
</tr>
</tbody>
</table>

**錫爾河州**（[烏茲別克語](../Page/烏茲別克語.md "wikilink")：Sirdaryo
viloyati）是[烏茲別克十二個州份之一](../Page/烏茲別克.md "wikilink")。它涵蓋5,100平方公里，有人口648,100。錫爾河州下轄9個縣，[首府設於](../Page/首府.md "wikilink")[古利斯坦市](../Page/古利斯坦.md "wikilink")。

## 地理位置

錫爾河州位於[烏茲別克東部](../Page/烏茲別克.md "wikilink")。它與以下州份或國家相連（從北開始逆時針）：[哈薩克](../Page/哈薩克.md "wikilink")、[吉扎克州](../Page/吉扎克州.md "wikilink")、[塔吉克](../Page/塔吉克.md "wikilink")、[塔什干州](../Page/塔什干州.md "wikilink")。公路全長2,000公里，另外[鐵路](../Page/鐵路.md "wikilink")400公里。

## 經濟

錫爾河州的經濟主要依賴[棉花和](../Page/棉花.md "wikilink")[穀物生產](../Page/穀物.md "wikilink")。在過去的幾年中，不少的[農田已加上](../Page/農田.md "wikilink")[水泵以及其他農業器材](../Page/水泵.md "wikilink")。在[棉花和](../Page/棉花.md "wikilink")[穀物之外](../Page/穀物.md "wikilink")，錫爾河州亦出產[葡萄](../Page/葡萄.md "wikilink")、[甜瓜和](../Page/甜瓜.md "wikilink")[馬鈴薯等蔬果](../Page/馬鈴薯.md "wikilink")。[蓄牧業也在發展](../Page/蓄牧業.md "wikilink")。工業方面主要出產[建築材料和](../Page/建築材料.md "wikilink")[農業生產器材](../Page/農業.md "wikilink")。另外，[烏茲別克其中一個最大的](../Page/烏茲別克.md "wikilink")[水力發電站坐落在錫爾河州內](../Page/水力發電站.md "wikilink")，為全國提供約三份之一的[電力](../Page/電力.md "wikilink")。

## 參考

  - Umid World

[Category:烏茲別克行政區劃](../Category/烏茲別克行政區劃.md "wikilink")