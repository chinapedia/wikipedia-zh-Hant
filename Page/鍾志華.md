**鍾志华**（），[湖南](../Page/湖南.md "wikilink")[湘阴人](../Page/湘阴.md "wikilink")，[中国工程院院士](../Page/中国工程院院士.md "wikilink")。

## 生平

1978年9月，在[湖南大学机械系学习](../Page/湖南大学.md "wikilink")。1982年9月至1984年1月，在出国预备培训，分别在广州外语学院、北京语言学院学习英语和瑞典语。1984年1月，在瑞典律勒欧大学攻读副博士学位。1987年2月，在瑞典林雪平大学机械工程专业学习，获工学博士学位。1988年5月，在瑞典林雪平大学做博士后研究。1989年12月，任瑞典林雪平大学助理教授。1992年6月，任瑞典林雪平大学副教授（终身职位），湖南大学机械与汽车工程学院教授。

1995年10月，任湖南大学机械与汽车工程学院副院长。1999年7月加入中国共产党。1997年9月，任湖南大学机械与汽车工程学院院长。2004年1月，任湖南大学校长助理，机械与汽车工程学院院长。2004年4月，任湖南大学副校长、党委常委。2005年7月，任[湖南大学校长](../Page/湖南大学.md "wikilink")、党委常委\[1\]。

2010年12月，任[中共重庆市委科技工作委员会书记](../Page/中共重庆市委.md "wikilink")，重庆[两江新区管理委员会副主任](../Page/两江新区.md "wikilink")（兼）。2011年2月，任[重庆市科学技术委员会主任](../Page/重庆市科学技术委员会.md "wikilink")、重庆市委科技工作委员会书记，重庆两江新区管理委员会副主任（兼）。2011年4月，又兼任[重庆市科学技术协会主席](../Page/重庆市科学技术协会.md "wikilink")。

2014年10月，任[中国工程院党组成员](../Page/中国工程院.md "wikilink")、秘书长\[2\]。

2016年9月，任[同济大学校长](../Page/同济大学.md "wikilink")\[3\]。

2002年获[国家科技进步一等奖](../Page/国家科技进步一等奖.md "wikilink")。2004年获[国家科技进步二等奖](../Page/国家科技进步二等奖.md "wikilink")。2005年12月，当选为[中国工程院院士](../Page/中国工程院院士.md "wikilink")。2018年1月，当选[第十三届全国政协委员](../Page/中国人民政治协商会议第十三届全国委员会委员名单.md "wikilink")\[4\]。

## 参考

{{-}}

[ZHI](../Category/鍾姓.md "wikilink")
[Category:湘阴人](../Category/湘阴人.md "wikilink")
[Category:湖南大学校友](../Category/湖南大学校友.md "wikilink")
[Category:林雪平大学校友](../Category/林雪平大学校友.md "wikilink")
[Category:中国工程院机械与运载工程学部院士](../Category/中国工程院机械与运载工程学部院士.md "wikilink")
[Category:第九届全国人大代表](../Category/第九届全国人大代表.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:湖南大学教授](../Category/湖南大学教授.md "wikilink")
[Category:林雪平大学教师](../Category/林雪平大学教师.md "wikilink")
[Category:湖南大学校长](../Category/湖南大学校长.md "wikilink")
[Category:同济大学校长](../Category/同济大学校长.md "wikilink")
[Category:湖南科学家](../Category/湖南科学家.md "wikilink")
[Category:中国工程院秘书长](../Category/中国工程院秘书长.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")

1.
2.
3.
4.