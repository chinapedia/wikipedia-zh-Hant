**鍾承祐**（），原名**鍾承佑**，外號**yoyo**，為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/运动员.md "wikilink")，於[2007年季末選秀會被](../Page/中華職棒歷年選秀會#職棒十九年（2008年）.md "wikilink")[La
new熊隊以第三輪第](../Page/La_new熊.md "wikilink")17順位選進，目前效力於[中華職棒](../Page/中華職棒.md "wikilink")[Lamigo桃猿隊](../Page/Lamigo桃猿.md "wikilink")，守備位置為[外野手](../Page/外野手.md "wikilink")。近年則苦於傷痛，為此於2014年改為現名，希望身體健康\[1\]。

2012年10月於[總冠軍戰對上](../Page/2012年中華職棒總冠軍賽.md "wikilink")[統一7-ELEVEn獅隊的比賽第四戰中](../Page/統一7-ELEVEn獅.md "wikilink")，右手遭後來曾為隊友的[蔡璟豪觸身球擊中](../Page/蔡璟豪.md "wikilink")，造成右手指舟狀骨線性骨折，後來[Lamigo桃猿拿到總冠軍](../Page/Lamigo桃猿.md "wikilink")，但後續的[亞洲職棒大賽卻無法上場](../Page/亞洲職棒大賽.md "wikilink")。隔年則受到前一年的[觸身球受傷影響](../Page/觸身球.md "wikilink")，表現不如預期，2014年季中又因跑壘時釘鞋卡住導致左腳踝內側扭傷，休息兩個月後才回歸賽場，台灣大賽時也由於外野手新人卡位而未列在28人名單內。

2015年下半季由於右手[肘](../Page/肘.md "wikilink")[骨刺](../Page/骨贅.md "wikilink")，最後開刀，被移出60人名單，該季報銷\[2\]。2016年3月29日在對上[中信兄弟的比賽時被洋投魔力](../Page/中信兄弟.md "wikilink")(Robert
Morey)的觸身球擊中，直到下半季中段才復出\[3\]。近年由於球隊補進[王柏融](../Page/王柏融.md "wikilink")、[陽耀勳等好手](../Page/陽耀勳.md "wikilink")，加上[朱育賢經常移防左外野先發](../Page/朱育賢.md "wikilink")，上場機會已不如以往。

## 經歷

  - [高雄縣](../Page/高雄縣.md "wikilink")[橋頭鄉仕隆國小少棒隊](../Page/橋頭區.md "wikilink")
  - 高雄縣橋頭國中青少棒隊
  - [高雄縣高苑工商青棒隊](../Page/高雄市私立高苑高級工商職業學校.md "wikilink")
  - [國立體院](../Page/國立體育大學.md "wikilink")（台灣啤酒）棒球隊
  - 中華職棒La New二軍借將（2007年）
  - 中華職棒La New熊隊代訓球員
  - 中華職棒La New熊、[Lamigo桃猿隊](../Page/Lamigo桃猿.md "wikilink")（2008年－）

## 職棒生涯成績

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>球隊</p></th>
<th><p>出賽</p></th>
<th><p>打數</p></th>
<th><p>安打</p></th>
<th><p>全壘打</p></th>
<th><p>打點</p></th>
<th><p>盜壘</p></th>
<th><p>四死球</p></th>
<th><p>三振</p></th>
<th><p>壘打數</p></th>
<th><p>雙殺打</p></th>
<th><p>打擊率</p></th>
<th><p>上壘率</p></th>
<th><p>長打率</p></th>
<th><p>OPS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/La_New熊.md" title="wikilink">La New熊</a></p></td>
<td><p>42</p></td>
<td><p>131</p></td>
<td><p>33</p></td>
<td><p>3</p></td>
<td><p>17</p></td>
<td><p>2</p></td>
<td><p>12</p></td>
<td><p>25</p></td>
<td><p>51</p></td>
<td><p>3</p></td>
<td><p>0.252</p></td>
<td><p>0.315</p></td>
<td><p>0.389</p></td>
<td><p>0.704</p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/La_New熊.md" title="wikilink">La New熊</a></p></td>
<td><p>86</p></td>
<td><p>286</p></td>
<td><p>82</p></td>
<td><p>7</p></td>
<td><p>45</p></td>
<td><p>7</p></td>
<td><p>19</p></td>
<td><p>43</p></td>
<td><p>128</p></td>
<td><p>10</p></td>
<td><p>0.287</p></td>
<td><p>0.329</p></td>
<td><p>0.448</p></td>
<td><p>0.777</p></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/La_New熊.md" title="wikilink">La New熊</a></p></td>
<td></td>
<td><p>466</p></td>
<td><p>134</p></td>
<td><p>10</p></td>
<td><p>64</p></td>
<td><p>9</p></td>
<td><p>20</p></td>
<td><p>80</p></td>
<td><p>208</p></td>
<td><p>13</p></td>
<td><p>0.288</p></td>
<td><p>0.314</p></td>
<td><p>0.446</p></td>
<td><p>0.760</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>114</p></td>
<td><p>444</p></td>
<td><p>141</p></td>
<td><p>15</p></td>
<td><p>73</p></td>
<td><p>2</p></td>
<td><p>45</p></td>
<td><p>63</p></td>
<td><p>221</p></td>
<td><p>8</p></td>
<td><p>0.318</p></td>
<td><p>0.378</p></td>
<td><p>0.498</p></td>
<td><p>0.876</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>115</p></td>
<td><p>441</p></td>
<td><p>134</p></td>
<td><p>9</p></td>
<td><p>72</p></td>
<td><p>2</p></td>
<td><p>34</p></td>
<td><p>79</p></td>
<td><p>205</p></td>
<td><p>15</p></td>
<td><p>0.304</p></td>
<td><p>0.349</p></td>
<td><p>0.465</p></td>
<td><p>0.814</p></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>114</p></td>
<td><p>372</p></td>
<td><p>96</p></td>
<td><p>4</p></td>
<td><p>49</p></td>
<td><p>1</p></td>
<td><p>20</p></td>
<td><p>73</p></td>
<td><p>139</p></td>
<td><p>10</p></td>
<td><p>0.258</p></td>
<td><p>0.288</p></td>
<td><p>0.374</p></td>
<td><p>0.662</p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>67</p></td>
<td><p>153</p></td>
<td><p>34</p></td>
<td><p>4</p></td>
<td><p>20</p></td>
<td><p>1</p></td>
<td><p>18</p></td>
<td><p>38</p></td>
<td><p>57</p></td>
<td><p>3</p></td>
<td><p>0.222</p></td>
<td><p>0.302</p></td>
<td><p>0.373</p></td>
<td><p>0.675</p></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>57</p></td>
<td><p>170</p></td>
<td><p>47</p></td>
<td><p>4</p></td>
<td><p>22</p></td>
<td><p>10</p></td>
<td><p>20</p></td>
<td><p>39</p></td>
<td><p>74</p></td>
<td><p>6</p></td>
<td><p>0.276</p></td>
<td><p>0.345</p></td>
<td><p>0.435</p></td>
<td><p>0.780</p></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>27</p></td>
<td><p>76</p></td>
<td><p>21</p></td>
<td><p>1</p></td>
<td><p>11</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
<td><p>20</p></td>
<td><p>32</p></td>
<td><p>2</p></td>
<td><p>0.276</p></td>
<td><p>0.329</p></td>
<td><p>0.421</p></td>
<td><p>0.750</p></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>24</p></td>
<td><p>43</p></td>
<td><p>9</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>7</p></td>
<td><p>17</p></td>
<td><p>11</p></td>
<td><p>1</p></td>
<td><p>0.209</p></td>
<td><p>0.320</p></td>
<td><p>0.256</p></td>
<td><p>0.576</p></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>23</p></td>
<td><p>51</p></td>
<td><p>10</p></td>
<td><p>0</p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>9</p></td>
<td><p>11</p></td>
<td><p>1</p></td>
<td><p>0.196</p></td>
<td><p>0.236</p></td>
<td><p>0.216</p></td>
<td><p>0.452</p></td>
</tr>
<tr class="even">
<td><p>2019</p></td>
<td><p><a href="../Page/Lamigo桃猿.md" title="wikilink">Lamigo桃猿</a></p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
</tr>
<tr class="odd">
<td><p>合計</p></td>
<td><p>12年</p></td>
<td><p>789</p></td>
<td><p>2633</p></td>
<td><p>741</p></td>
<td><p>57</p></td>
<td><p>383</p></td>
<td><p>36</p></td>
<td><p>205</p></td>
<td><p>486</p></td>
<td><p>1137</p></td>
<td><p>72</p></td>
<td><p>0.281</p></td>
<td><p>0.329</p></td>
<td><p>0.432</p></td>
<td><p>0.761</p></td>
</tr>
</tbody>
</table>

## 特殊事蹟

### 得獎紀錄

  - 2010-2012年連續三年[中華職棒](../Page/中華職棒.md "wikilink")[外野手最佳十人獎](../Page/外野手.md "wikilink")
  - 2011-2012年連續兩年[中華職棒](../Page/中華職棒.md "wikilink")[外野手金手套獎](../Page/外野手.md "wikilink")
  - [總冠軍](../Page/中華職棒總冠軍賽.md "wikilink")：2012

### [La new熊時期](../Page/La_new熊.md "wikilink")

  - 2008年5月4日對戰[兄弟象時](../Page/兄弟象.md "wikilink")，[朱鴻森打出後來有名的](../Page/朱鴻森.md "wikilink")「再見沒有全壘打」，當時鍾承佑守[左外野](../Page/左外野.md "wikilink")，由其長傳二壘造成出局\[4\]。
  - 2008年[讀賣巨人來台與](../Page/讀賣巨人.md "wikilink")[La
    New熊四場交流比賽中](../Page/La_New熊.md "wikilink")，共擊出三支[全壘打表現優異](../Page/全壘打.md "wikilink")。
  - 2009年5月23日於[台中棒球場對戰](../Page/台中棒球場.md "wikilink")[興農牛時](../Page/興農牛.md "wikilink")，於十局上面對[蔡明晉打出斷棒兩分打點](../Page/蔡明晉.md "wikilink")[全壘打](../Page/全壘打.md "wikilink")，締造[完全打擊](https://www.youtube.com/watch?v=amjCfGmnlMk)紀錄，為[中華職棒史上第六人](../Page/中華職棒.md "wikilink")，本土第三人，同時也是最年輕的締造者\[5\]。
  - 2009年6月18日對戰[統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink")，單局兩安打五分打點，成為本土球員單局最多打點記錄\[6\]。
  - 2009年9月26日對戰[兄弟象於四局下半發生](../Page/兄弟象.md "wikilink")[烏龍觸身球事件](https://www.youtube.com/watch?v=JSya9SkcRy0)，當時投手[李濠任投出普通壞球](../Page/李濠任.md "wikilink")，主審王俊宏卻因鍾承佑動作影響，誤判為頸部以上觸身球，將李濠任按當時規則\[7\]驅逐出場\[8\]。後來聯盟賽務部決議王俊宏裁判禁判二場，且罰款一萬二千元。
  - 2009年12月6日參加「有愛挺你」[八八水災棒球義演賽](../Page/八八水災.md "wikilink")，入選西隊成員。
  - 2010年9月16日於[屏東棒球場對戰](../Page/屏東縣立棒球場.md "wikilink")[兄弟象隊](../Page/兄弟象.md "wikilink")，從[杜寅傑手中擊出全壘打](../Page/杜寅傑.md "wikilink")，為該季第十發全壘打，生涯首度達成單季十轟。

### [Lamigo桃猿時期](../Page/Lamigo桃猿.md "wikilink")

  - 2011年3月20日於[台中洲際棒球場對戰](../Page/臺中市洲際棒球場.md "wikilink")[興農牛隊](../Page/興農牛.md "wikilink")，是[Lamigo桃猿隊改名後新球季的首場比賽](../Page/Lamigo桃猿.md "wikilink")，擔任先發第六棒[右外野手](../Page/右外野手.md "wikilink")，五局上從[陳煥揚手中擊出兩分打點全壘打](../Page/陳煥揚.md "wikilink")，是該隊改名後首位第六棒、首位右外野手、首位擊出全壘打的打者。
  - 2011年6月7日於[台中棒球場對戰](../Page/台中棒球場.md "wikilink")[興農牛隊](../Page/興農牛.md "wikilink")，從[陳煥揚手中擊出首局首打席全壘打](../Page/陳煥揚.md "wikilink")。
  - 2011年7月23日拿下[中華職棒全壘打大賽冠軍](../Page/中華職棒全壘打大賽.md "wikilink")，隔年再度連霸，為聯盟首位蟬聯冠軍的球員。
  - 2011年9月16日於[台南市立棒球場出戰](../Page/台南市立棒球場.md "wikilink")[統一7-ELEVEn獅隊時達成個人職棒生涯首次三響砲紀錄](../Page/統一7-ELEVEn獅.md "wikilink")，為中華職棒史上第12人、第13次完成三響砲紀錄。
  - 2012年4月13日於[桃園國際棒球場對戰](../Page/桃園國際棒球場.md "wikilink")[統一7-ELEVEn獅隊](../Page/統一7-ELEVEn獅.md "wikilink")，於十局下從[林岳平手中擊出個人職棒生涯首支再見安打](../Page/林岳平.md "wikilink")。
  - 2014年8月26日於[桃園國際棒球場從](../Page/桃園國際棒球場.md "wikilink")[義大犀牛洋投路易士](../Page/義大犀牛.md "wikilink")(Rommie
    Lewis)\[9\]手中擊出一發兩分全壘打，為個人生涯第50轟。

## 参考資料與註釋

<references/>

## 外部連結

  -
  -
[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:中華成棒隊球員](../Category/中華成棒隊球員.md "wikilink")
[Category:Lamigo桃猿隊球員](../Category/Lamigo桃猿隊球員.md "wikilink")
[Category:高苑高級工商職業學校校友](../Category/高苑高級工商職業學校校友.md "wikilink")
[Category:國立體育大學校友](../Category/國立體育大學校友.md "wikilink")
[Category:高雄市人](../Category/高雄市人.md "wikilink")
[Category:橋頭人](../Category/橋頭人.md "wikilink")
[Category:鍾姓](../Category/鍾姓.md "wikilink")

1.  [〈CPBL〉鍾承祐改名音沒變　祈求身體健康](http://www.tsna.com.tw/index.php?q=node/56764)
2.  [中職》鍾承祐將動手術
    遭移出六十人名單](http://sports.ltn.com.tw/news/breakingnews/1420078)
3.  [中職》鍾承祐開刀 下半季復出](http://sports.ltn.com.tw/news/breakingnews/1650824)
4.  [完投9局 阿鈣做白工](http://sports.ltn.com.tw/news/paper/208843)
5.  鍾承佑當時[完全打擊順序為二壘打](../Page/完全打擊.md "wikilink")→三壘打→一壘打→[全壘打](../Page/全壘打.md "wikilink")。
6.  單局六分打點紀錄是由[統一獅前洋將羅偉締造](../Page/統一獅.md "wikilink")。
7.  當時投手投出頸部以上觸身球，必須被驅逐出場。
8.  [中職》觸身球惹議中職也有
    但這兩個最經典...](http://sports.ltn.com.tw/news/breakingnews/1354901)
9.  2013年亦曾效力於[Lamigo桃猿隊](../Page/Lamigo桃猿.md "wikilink")，當時名為雷弋士。