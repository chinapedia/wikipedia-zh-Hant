**鍾離牧**，字**子幹**，[三国时](../Page/三国.md "wikilink")[會稽郡](../Page/會稽.md "wikilink")[山陰县人](../Page/山陰.md "wikilink")，[东吴将領](../Page/东吴.md "wikilink")，汉[尚書仆射](../Page/尚書仆射.md "wikilink")[鍾離意七世孙](../Page/鍾離意.md "wikilink")。

## 生平

### 早年事跡

鍾離牧早年曾在[永興縣居住](../Page/永興縣.md "wikilink")，並在當地親自耕種二十畝[稻米](../Page/稻米.md "wikilink")。稻米將近成熟時，就有一個縣民向鍾離牧聲稱土地是他的，要他交出土地和稻米；鍾離牧自願交出。永興縣長聽後，即收捕該名縣民，要依法處罰他，但鍾離牧卻為他請罪。縣長說：「君慕[承宮](../Page/承宮.md "wikilink")，自行義事，僕為民主，當以法率下，何得寢公憲而從君邪？（你想要倣效承宮行義舉，但我是人民的首長，應當以法治縣，怎可以枉法順從你？）」鍾離牧卻說：「此是郡界，緣君意願，故來蹔住。今以少稻而殺此民，何心復留？（這裏是會稽郡內，我因著你的意願才來暫住。你現在卻因少許稻米而殺這個人，我還怎能有留在這兒的心？）」鍾離牧說後即收拾行裝而回故鄉，縣長親自追回，並將那人釋放。那人感到十分慚愧和畏懼，親自將那些稻米收成所得的六十[斛白米都送還鍾離牧](../Page/斛.md "wikilink")，但鍾離牧不收。那人就將米放在路旁，沒有人敢擅取。鍾離牧的名聲由此而起。

### 安定山越

後鍾離牧任[郎中](../Page/郎中.md "wikilink")。[赤烏五年](../Page/赤烏.md "wikilink")（242年），補[太子](../Page/太子.md "wikilink")[孫和的太子輔義都尉](../Page/孫和.md "wikilink")，遷[南海](../Page/南海.md "wikilink")[太守](../Page/太守.md "wikilink")，任內曾越郡界討伐[高涼縣的盗贼](../Page/高涼縣.md "wikilink")[仍弩和招安在](../Page/仍弩.md "wikilink")[揭陽縣擾攘了十多年的](../Page/揭陽縣.md "wikilink")[曾夏盜賊集團](../Page/曾夏.md "wikilink")。後來任[丞相](../Page/丞相.md "wikilink")[長史](../Page/長史.md "wikilink")，再轉[司直](../Page/司直.md "wikilink")，後又遷[中書令](../Page/中書令.md "wikilink")。此時[建安](../Page/建安.md "wikilink")、[鄱陽和](../Page/鄱陽.md "wikilink")[新都三郡山越人作亂](../Page/新都.md "wikilink")\[1\]，鍾離牧被任命為監軍使者，出兵討平。亂民首領[黃亂](../Page/黃亂.md "wikilink")、[常俱等都派出他們的部眾為東吳服兵役](../Page/常俱.md "wikilink")。鍾離牧因功封**秦亭侯**，官拜[越騎校尉](../Page/越騎校尉.md "wikilink")。

### 安定邊境

[永安六年](../Page/永安_\(孫休\).md "wikilink")（263年），[曹魏攻滅](../Page/曹魏.md "wikilink")[蜀漢](../Page/蜀漢.md "wikilink")，當時有人認為在吳蜀邊境[武陵郡的](../Page/武陵郡.md "wikilink")[五谿夷族可能會叛亂](../Page/五谿.md "wikilink")，鍾離牧於是被任命為平魏將軍，領武陵[太守](../Page/太守.md "wikilink")，到當地鎮守。此時曹魏亦派[漢葭縣長](../Page/漢葭.md "wikilink")[郭純試守武陵太守](../Page/郭純.md "wikilink")，並領[涪陵人民入](../Page/涪陵.md "wikilink")[遷陵縣境](../Page/遷陵.md "wikilink")，並駐屯[赤沙](../Page/赤沙.md "wikilink")，用以引誘五谿夷族；部份夷族響應郭純，更進攻[酉陽縣](../Page/酉陽縣.md "wikilink")，震驚武陵郡。鍾離牧於是領兵連夜依山險行，斬殺叛民魁帥和其支黨共千多人，郭純的部眾因五谿變民被擊潰而解散，成功平定五谿的叛亂。平定之后转[公安督](../Page/公安縣.md "wikilink")、揚武將軍，封**都鄉侯**。後又徙任[濡須督](../Page/濡須.md "wikilink")。後又以前將軍假節，复領武陵太守。後在任內逝世，死時家無餘財，士民都思念他。

## 評價

  - 《[三国志](../Page/三国志.md "wikilink")》评论为：“鐘離牧蹈長者之規”。
  - 鍾離駰：「牧必勝我，不可輕也。」
  - [始興太守](../Page/始興.md "wikilink")[羊衜评论为](../Page/羊衜.md "wikilink")：“锺离子幹吾昔知之不熟，定见其在南海，威恩部伍，智勇分明，加操行清纯，有古人之风。”

## 家庭

### 父親

  - [鍾離緒](../Page/鍾離緒.md "wikilink")，東吳樓船都尉。

### 兄弟

  - [鍾離駰](../Page/鍾離駰.md "wikilink")，鍾離牧之兄。少年时与[謝贊](../Page/謝贊.md "wikilink")、[顧譚齐名](../Page/顧譚.md "wikilink")。

### 子女

  - [鍾離禕](../Page/鍾離禕.md "wikilink")，嗣子。鍾離牧死後代他領兵。
  - [鍾離盛](../Page/鍾離盛.md "wikilink")，鍾離牧次子，東吳[尚書郎](../Page/尚書郎.md "wikilink")。
  - [鍾離徇](../Page/鍾離徇.md "wikilink")，東吳偏將軍，鎮守[西陵](../Page/西陵.md "wikilink")。[晉滅吳之戰時領水军抵抗](../Page/晉滅吳之戰.md "wikilink")，戰死。

## 參考文獻

  - 《三國志·吳書·鍾離牧傳》

[Z钟](../Category/东吴军事人物.md "wikilink")
[Z钟](../Category/东吴列侯.md "wikilink")
[M](../Category/钟离姓.md "wikilink")

1.  時間為太平二年（257年），《三國志·吳志·孫亮傳》：八月，會稽南部反，殺都尉。鄱陽、新都民為亂，廷尉[丁密](../Page/丁密.md "wikilink")、步兵校尉鄭冑、**將軍鐘離牧**率軍討之。