**鍾麗淇**（英文名：，），[加拿大](../Page/加拿大.md "wikilink")[多倫多出生](../Page/多倫多.md "wikilink")，前[香港](../Page/香港.md "wikilink")[無綫電視藝員](../Page/無綫電視.md "wikilink")。

## 簡介

曾參加1995年度[多倫多華裔小姐競選](../Page/多倫多華裔小姐競選.md "wikilink")，在1996年[無綫電視藝員訓練班畢業後加入](../Page/無綫電視藝員訓練班.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")，隨即加入拍攝首部剧集[刑事偵缉檔案III](../Page/刑事偵缉檔案III.md "wikilink")。

曾於《[刑事侦缉档案III](../Page/刑事侦缉档案III.md "wikilink")》、《[鑑證實錄](../Page/鑑證實錄.md "wikilink")》、《[医神华佗](../Page/医神华佗.md "wikilink")》系列劇集等擔任重要角色。拍攝完《[心慌·心郁·逐个捉](../Page/心慌·心郁·逐个捉.md "wikilink")》即沒有在[無綫電視繼續推出新作品](../Page/無綫電視.md "wikilink")，近年較少演出，專注[瑜伽](../Page/瑜伽.md "wikilink")[教練的工作](../Page/教練.md "wikilink")。

2017年11月，鍾麗淇剛為小女兒Michela慶祝兩歲生日，接受訪問時透露女兒小小年紀已經懂得照顧小姊姊
Isabella，家姊因為患上罕見的基因病，手腳軟弱無力，坐的時候身體會傾側倒下，兩歲女兒只要姊姊靠在身邊，她就會撐着凳子，用健壯的身體承托着，以免姊姊倒下來，媽媽看在眼裏既感動又安慰\[1\]。

### 感情生活

鍾麗淇曾于1996年起與藝人[梁榮忠相戀](../Page/梁榮忠.md "wikilink")，兩人於2006年因[朱慧敏介入而](../Page/朱慧敏.md "wikilink")[分手](../Page/分手.md "wikilink")。

2010年7月，與中意混血男友Nardone Ruggero結婚，現育有兩女。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p>劇名</p></th>
<th><p>角色</p></th>
<th><p>性質</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1997年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刑事偵緝檔案III.md" title="wikilink">刑事偵緝檔案III</a></p></td>
<td><p>李思龍</p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鑑證實錄.md" title="wikilink">鑑證實錄</a></p></td>
<td><p>聶津津</p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="odd">
<td><p><strong>1998年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/網上有情人.md" title="wikilink">網上有情人</a></p></td>
<td><p>莊慧蘭</p></td>
<td><p>第二女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狀王宋世傑II.md" title="wikilink">狀王宋世傑II</a></p></td>
<td><p>楚楚</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong>1999年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無業樓民.md" title="wikilink">無業樓民</a></p></td>
<td><p>Bonnie</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雙面伊人.md" title="wikilink">雙面伊人</a></p></td>
<td><p>Kelly</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鑑證實錄II.md" title="wikilink">鑑證實錄II</a></p></td>
<td><p>聶津津</p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="even">
<td><p><strong>2000年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/醫神華佗.md" title="wikilink">醫神華佗</a></p></td>
<td><p>華玉</p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廟街·媽·兄弟.md" title="wikilink">廟街·媽·兄弟</a></p></td>
<td><p>洛詠琪</p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2001年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公私戀事多.md" title="wikilink">公私戀事多</a></p></td>
<td><p>Clara</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2003年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑夜彩虹.md" title="wikilink">黑夜彩虹</a></p></td>
<td><p>王秀兒（KiKi）</p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美麗在望.md" title="wikilink">美麗在望</a></p></td>
<td><p>鐘碧儀（Betty）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沖上雲霄.md" title="wikilink">沖上雲霄</a></p></td>
<td><p>丘惠琪（Vicky）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Joan</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2004年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天涯俠醫.md" title="wikilink">天涯俠醫</a></p></td>
<td><p>蘇惠珊（Susan）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/下一站彩虹.md" title="wikilink">下一站彩虹</a></p></td>
<td><p>Donna</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2006年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/心慌·心郁·逐個捉.md" title="wikilink">心慌·心郁·逐個捉</a></p></td>
<td><p>賈婉君</p></td>
<td><p>第三女主角</p></td>
</tr>
</tbody>
</table>

### 电影

  - 1998年：《[男人胸女人HOME](../Page/男人胸女人HOME.md "wikilink")》
  - 2002年：《[別戀](../Page/別戀.md "wikilink")》 飾 Annie
  - 2015年：《[哪一天我們會飛](../Page/哪一天我們會飛.md "wikilink")》 飾 Cindy

### 節目主持

  - 康泰最緊要好玩 韓國旅遊新路線（1998）
  - 再創高峯（1999）
  - 食都要高清（2008）

### 參與節目

  - [今日VIP](../Page/今日VIP.md "wikilink")（2014年9月）
  - [我愛香港](../Page/我愛香港_\(電視節目\).md "wikilink")（2016年10月16日）

## 參考

## 外部連結

  -
[Lai](../Category/鍾姓.md "wikilink")
[Z](../Category/华裔加拿大人.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港瑜伽教師](../Category/香港瑜伽教師.md "wikilink")
[Category:香港電視女演員](../Category/香港電視女演員.md "wikilink")
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")

1.  [【獨家專訪】鍾麗淇笑容化解歧視　女兒照顧患罕見病姊姊](http://bka.mpweekly.com/interview/%E5%A8%9B%E6%A8%82123/20171118-91109)