**鐮刀龍科**（Therizinosauridae）意為「鐮刀[蜥蜴](../Page/蜥蜴.md "wikilink")」是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[鐮刀龍超科的一科](../Page/鐮刀龍超科.md "wikilink")。鐮刀龍科是群[草食性或](../Page/草食性.md "wikilink")[肉食性的動物](../Page/肉食性.md "wikilink")，生存於[白堊紀中到晚期](../Page/白堊紀.md "wikilink")，化石已在[中國](../Page/中國.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[美國等地發現](../Page/美國.md "wikilink")。

## 敘述

鐮刀龍科具有大型前肢，長指爪，身體寬廣，腳部具有四個趾爪。牠們的頭部小型，頸部長，嘴部前端缺乏牙齒，嘴部後段的牙齒平坦。前背部的脊椎具有高的神經弓，[背肋寬](../Page/背肋.md "wikilink")，臀部結構類似[鳥類](../Page/鳥類.md "wikilink")。寬廣的臀部顯示，牠們可能具有長的消化系統，以用來消化[植物](../Page/植物.md "wikilink")。根據[系統發生學的位置](../Page/系統發生學.md "wikilink")，以及在[北票龍身上發現的類似](../Page/北票龍.md "wikilink")[羽毛的覆蓋物](../Page/羽毛.md "wikilink")，鐮刀龍科（或鐮刀龍超科）可能都具有類似的覆蓋物。

## 分類

鐮刀龍科是由[葉甫根尼·馬列夫](../Page/葉甫根尼·馬列夫.md "wikilink")（Evgeny
Maleev）在1954年建立，以包含新發現的[鐮刀龍](../Page/鐮刀龍.md "wikilink")，但當時馬列夫認為牠們是種類似[烏龜的巨大動物](../Page/烏龜.md "wikilink")\[1\]。長期以來，鐮刀龍科是認為是群奇特的獸腳類恐龍，可能是[肉食性](../Page/肉食性.md "wikilink")。隨者更多鐮刀龍科近親的化石被發現，原本觀念遭到質疑。這些新化石是草食性動物、具有許多奇異特徵，外表介於[鳥臀目與](../Page/鳥臀目.md "wikilink")[原蜥腳下目之間](../Page/原蜥腳下目.md "wikilink")。這些新發現物種被歸類於[慢龍科](../Page/慢龍科.md "wikilink")（Segnosauridae），意指牠們的沉重身體、短後肢，可能採取類似[樹櫴的生活方式](../Page/樹櫴.md "wikilink")\[2\]。後來的研究發現鐮刀龍不屬於鳥臀目、原蜥腳下目，是種奇特的獸腳類恐龍，屬於[草食性](../Page/草食性.md "wikilink")，其近親是同樣奇特的[慢龍](../Page/慢龍.md "wikilink")。由於鐮刀龍科的建立早於慢龍科，因此具有優先權，而慢龍科成為鐮刀龍科的[次異名](../Page/次異名.md "wikilink")\[3\]。

在1998年，[保羅·塞里諾](../Page/保羅·塞里諾.md "wikilink")（Paul
Sereno）首次對鐮刀龍科提出[系統發生學定義](../Page/系統發生學.md "wikilink")，定義為：親緣關係接近[死神龍](../Page/死神龍.md "wikilink")，而離[似鳥龍較遠的所有物種](../Page/似鳥龍.md "wikilink")。隨者更多大型[鐮刀龍類化石的發現](../Page/鐮刀龍類.md "wikilink")，鐮刀龍科與其他獸腳類恐龍的演化關係更為明確，鐮刀龍科的範圍更為狹窄。在2005年，保羅·塞里諾將鐮刀龍科重新定義為：包含鐮刀龍、[懶爪龍](../Page/懶爪龍.md "wikilink")、[內蒙古龍在內的最小](../Page/內蒙古龍.md "wikilink")[演化支](../Page/演化支.md "wikilink")\[4\]。

### 分類學

  - **鐮刀龍科 Therizinosauridae**
      - [秘龍](../Page/秘龍.md "wikilink") *Enigmosaurus*
      - [二連龍](../Page/二連龍.md "wikilink") *Erliansaurus*
      - [死神龍](../Page/死神龍.md "wikilink") *Erlikosaurus*
      - [南雄龍](../Page/南雄龍.md "wikilink") *Nanshiungosaurus*
      - [內蒙古龍](../Page/內蒙古龍.md "wikilink") *Neimongosaurus*
      - [懶爪龍](../Page/懶爪龍.md "wikilink") *Nothronychus*
      - [慢龍](../Page/慢龍.md "wikilink") *Segnosaurus*
      - [肅州龍](../Page/肅州龍.md "wikilink") *Suzhousaurus*
      - [鐮刀龍](../Page/鐮刀龍.md "wikilink") *Therizinosaurus*
        <small>([模式屬](../Page/模式屬.md "wikilink"))</small>

### 系統發生學

以下[演化樹是根據](../Page/演化樹.md "wikilink")[菲力·森特](../Page/菲力·森特.md "wikilink")（Phil
Senter）在2007年的研究\[5\]：

## 參考資料

<div class="references-small">

<references>

</references>

  - Zanno, L. 2004. The pectoral girdle and forelimb of a primitive
    therizinosauroid (Theropoda: Maniraptora): new information on the
    phylogenetics and evolution of therizinosaurs. Journal of Vertebrate
    Paleontology. Abstracts of papers. Sixty-fourth annual meeting
    Society of vertebrate paleontology, 24, Supplement to number 3,
    134A.

</div>

## 外部連結

  - [鐮刀龍科的分類歷史](https://web.archive.org/web/20080405234611/http://www.taxonsearch.org/dev/taxon_edit.php?Action=View&tax_id=308)
    - TaxoSearch.org.
  - [鐮刀龍科](https://web.archive.org/web/20080110010118/http://home.myuw.net/eoraptor/Therizinosauroidea.htm#Therizinosauridae)
    - 獸腳亞目資料庫

[\*](../Category/鐮刀龍超科.md "wikilink")

1.  Maleev, E. (1954). "New turtle-like reptile in Mongolia." *Priroda*,
    1954: 106-108.
2.  Perle, A. (1979). "Segnosauridae—a new family of theropods from the
    Late Cretaceous of Mongolia." *Trans. Joint Soviet–Mongolian
    Palaeontological Expedition*, **8**: 45–55.
3.  Clark, J. M., Perle, A. & Norell, M. A. (1994). "The skull of
    *Erlicosaurus andrewsi*, a Late Cretaceous ‘‘Segnosaur’’ (Theropod:
    Therizinosauridae) from Mongolia." *American Museum Novitates*,
    **3115**: 1–39.
4.  Sereno, P. C. 2005. [Stem
    Archosauria—TaxonSearch](http://www.taxonsearch.org/Archive/stem-archosauria-1.0.php)
     \[version 1.0, 2005 November 7\]
5.  Senter, P. (2007). "A new look at the phylogeny of Coelurosauria
    (Dinosauria: Theropoda)." *Journal of Systematic Palaeontology*, ().