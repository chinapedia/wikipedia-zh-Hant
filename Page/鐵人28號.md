《**鐵人28號**》（****）是[橫山光輝](../Page/橫山光輝.md "wikilink")1956年開始連載的日本[漫畫作品](../Page/漫畫.md "wikilink")，\[1\]，後來陸續改編電視動畫、廣播劇、電視劇、電影等相關作品。

## 故事概要

在[太平洋戰爭末期](../Page/太平洋戰爭.md "wikilink")，為了解決[日本軍士兵不足的問題](../Page/日军.md "wikilink")，軍部要求金田博士和敷島博士製作可以讓日本軍起死回生的秘密兵器，巨大機械人「鐵人28號」。
但是機械人完成後，戰爭卻結束了。於是「鐵人28號」又被鎖起來。後來金田博士的兒子，金田正太郎得到28號的無線操控器，而展開了故事。

## 衍生作品

  - 廣播劇《鐵人28號》於1959年由播放[日本放送電台播放](../Page/日本放送.md "wikilink")。
  - 黑白電視劇《鐵人28號》於1960年2月1日到4月25日播放共13集。\[2\]
  - 黑白電視動畫《鐵人28號》於1963年10月20日到1966年5月27日期間播放共96集，日本動畫史上首部巨大[機械人動畫](../Page/機械人動畫.md "wikilink")。\[3\]
  - 電視動畫《[鐵超人](../Page/鐵超人.md "wikilink")》（）於1980年10月3日至1981年9月25日期間播放共51集。
  - 動畫電影《[鐵人28號 神秘魔術團
    海底基地](../Page/鐵人28號_神秘魔術團_海底基地.md "wikilink")》（）於1964年7月21日上映。\[4\]
  - 電視動畫《[超電動機械人
    鐵人28號FX](../Page/超電動機械人_鐵人28號FX.md "wikilink")》（）於1992年4月5日至1993年3月30日期間播放共47集。
  - 電視動畫《鐵人28號》2004年4月7日到9月29日期間播放共26集，將第一作黑白電視動畫以復古風重拍彩色版本，獲選為2004年第八回[日本](../Page/日本.md "wikilink")[文部省](../Page/文部省.md "wikilink")[文化廳媒體藝術祭動畫部門推薦的作品](../Page/文化廳媒體藝術祭.md "wikilink")。
  - 漫畫《[鐵人28號
    皇帝的紋章](../Page/鐵人28號_皇帝的紋章.md "wikilink")》（）2004年由[長谷川裕一作畫](../Page/長谷川裕一.md "wikilink")，在《月刊マガジンZ》連載，單行本共3冊。
  - 電影《鐵人28號》於2005年3月19日上映，由日本[松竹電影公司製作](../Page/松竹.md "wikilink")。
  - 漫畫《[鐵人奪還作戰](../Page/鐵人奪還作戰.md "wikilink")》（）於2006年由[佐藤文也崇畫新版鐵人](../Page/佐藤文也.md "wikilink")28號，在[講談社的](../Page/講談社.md "wikilink")《[Magazine
    SPECIAL](../Page/Magazine_SPECIAL.md "wikilink")》連載。
  - 動畫電影《[鐵人28號
    白晝的殘月](../Page/鐵人28號_白晝的殘月.md "wikilink")》（）於2007年3月31日上映，由[今川泰宏導演所執導的新版動畫電影](../Page/今川泰宏.md "wikilink")。
  - 短篇電視動畫《[鐵人28號GAO\!](../Page/鐵人28號GAO!.md "wikilink")》（）於2013年4月6日到2016年3月26日播放共151集。

## 參考來源

## 外部連結

  - [橫山光輝官方網站](http://www.yokoyama-mitsuteru.com/)

[鐵人28號](../Category/鐵人28號.md "wikilink")
[Category:橫山光輝](../Category/橫山光輝.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:科幻漫畫](../Category/科幻漫畫.md "wikilink")
[Category:機器人漫畫](../Category/機器人漫畫.md "wikilink")
[Category:1963年日本電視動畫](../Category/1963年日本電視動畫.md "wikilink")
[Category:富士電視台動畫](../Category/富士電視台動畫.md "wikilink")
[Category:2004年東京電視網動畫](../Category/2004年東京電視網動畫.md "wikilink")
[Category:1960年代特攝作品](../Category/1960年代特攝作品.md "wikilink")
[Category:机器人超级英雄](../Category/机器人超级英雄.md "wikilink")
[Category:2004年日本劇場動畫](../Category/2004年日本劇場動畫.md "wikilink")
[Category:2007年日本劇場動畫](../Category/2007年日本劇場動畫.md "wikilink")
[Category:机器人动画](../Category/机器人动画.md "wikilink")
[Category:日本漫畫改編真人電影](../Category/日本漫畫改編真人電影.md "wikilink")
[Category:1960年電視劇集](../Category/1960年電視劇集.md "wikilink")
[Category:日本漫畫改編日本電視劇](../Category/日本漫畫改編日本電視劇.md "wikilink")
[Category:虚构第二次世界大战题材电视节目](../Category/虚构第二次世界大战题材电视节目.md "wikilink")

1.  [Tetsujin 28 Gou
    (manga)](http://www.animenewsnetwork.com/encyclopedia/manga.php?id=6965)[Anime
    News Network](../Page/Anime_News_Network.md "wikilink")
2.  [鉄人28号(1960)](http://www.allcinema.net/prog/show_c.php?num_c=85750)Aallcinema
3.  [鉄人28号(1963～1965)](http://www.allcinema.net/prog/show_c.php?num_c=89675)Aallcinema
4.  [鉄人28号 ミラクル魔術団/海底基地](http://eiga.com/movie/70153/)映画.com