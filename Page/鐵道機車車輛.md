**鐵道機車車輛**（[英文](../Page/英文.md "wikilink")：**Rolling
stock**）是一個集合名詞，稱呼所有在[鐵路運輸上的](../Page/鐵路運輸.md "wikilink")[車輛](../Page/車輛.md "wikilink")。通常包含了有動力及無動力兩種，像是[鐵路機車](../Page/鐵路機車.md "wikilink")、[鐵路車輛](../Page/鐵路車輛.md "wikilink")、[鐵路客車](../Page/鐵路客車.md "wikilink")\[1\]\[2\]\[3\]\[4\].

鐵道機車車輛在某些情況下代表非動力車輛，尤其是鐵路機車以外的另兩種。 \[5\].鐵道機車車輛與固定財產（fixed
stock）是相當不同的，固定財產通常是指軌道、信號、車站以及建築物等足以使鐵路運作的物品。不過固定財產一詞日漸少使用了。

## 參考資料

[Category:鐵路車輛](../Category/鐵路車輛.md "wikilink")

1.
2.
3.
4.
5.