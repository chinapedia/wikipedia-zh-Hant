**鑲嵌踝類主龍**（Crurotarsi）是一個[主龍類](../Page/主龍類.md "wikilink")[演化支](../Page/演化支.md "wikilink")，由[保羅·塞里諾](../Page/保羅·塞里諾.md "wikilink")（Paul
Sereno）在1991年建立，用來取代舊的[偽鱷亞目](../Page/偽鱷亞目.md "wikilink")。塞里諾在2005年作出更準確的[種系發生學定義](../Page/種系發生學.md "wikilink")：親緣關係較親近於[尼羅鱷](../Page/尼羅鱷.md "wikilink")，而離[家麻雀較遠的所有物種](../Page/家麻雀.md "wikilink")；也就是親緣關係較親近於[鱷魚](../Page/鱷魚.md "wikilink")，而離[鳥類較遠的所有物種](../Page/鳥類.md "wikilink")。所以，鑲嵌踝類主龍是[鳥頸類主龍](../Page/鳥頸類主龍.md "wikilink")（親緣關係較親近於鳥類，而離鱷魚較遠）的姐妹演化支。與舊式的傳統分類相比，鑲嵌踝類主龍或多或少符合[槽齒目去掉](../Page/槽齒目.md "wikilink")[古鱷亞目](../Page/古鱷亞目.md "wikilink")，再加上[鱷目](../Page/鱷目.md "wikilink")。

## 敘述

鑲嵌踝類主龍是[主龍形下綱中的兩個主要演化支之一](../Page/主龍形下綱.md "wikilink")。牠們的頭骨通常是厚重的，尤其是與[鳥頸類主龍相比](../Page/鳥頸類主龍.md "wikilink")；鑲嵌踝類主龍的口鼻部通常是長而狹窄的，頸部短而強壯，四肢的結構介於典型[爬行動物的往兩側延伸到](../Page/爬行動物.md "wikilink")[恐龍或](../Page/恐龍.md "wikilink")[哺乳類的直立四肢之間](../Page/哺乳類.md "wikilink")（雖然恐龍與哺乳類的姿態不同）。身體通常由兩排或更多排的甲板保護者。鑲嵌踝類主龍的體形大多數相當大，可達3公尺甚至更長。

## 演化

鑲嵌踝類主龍似乎在[三疊紀早期](../Page/三疊紀.md "wikilink")（[奧倫尼克階晚期](../Page/奧倫尼克階.md "wikilink")）出現，到三疊紀中期（[拉丁階](../Page/拉丁階.md "wikilink")）成為陸地[肉食性優勢動物](../Page/肉食性.md "wikilink")。牠們的全盛期是三疊紀晚期，在這時代牠們分為直立四肢的[勞氏鱷目](../Page/勞氏鱷目.md "wikilink")、長相類似鱷魚的[植龍目](../Page/植龍目.md "wikilink")、[草食性帶有裝甲的](../Page/草食性.md "wikilink")[堅蜥目](../Page/堅蜥目.md "wikilink")、大型狩獵動物[波波龍科](../Page/波波龍科.md "wikilink")、小型敏捷的[喙頭鱷亞目](../Page/喙頭鱷亞目.md "wikilink")、還有其他族群。

在[三疊紀-侏羅紀滅絕事件中](../Page/三疊紀-侏羅紀滅絕事件.md "wikilink")，所有大型的鑲嵌踝類主龍絕種，而恐龍繼承牠們的陸地優勢動物地位。只有小型的[喙頭鱷亞目與](../Page/喙頭鱷亞目.md "wikilink")[原鱷亞目存活下來](../Page/原鱷亞目.md "wikilink")。

隨者[中生代前進](../Page/中生代.md "wikilink")，原鱷類演化得更像典型的鱷魚，當恐龍稱霸於陸地的時候，[鱷形超目繁盛於河流](../Page/鱷形超目.md "wikilink")、沼澤、海洋，比現代[鱷魚還更具多樣性](../Page/鱷魚.md "wikilink")。

當[白堊紀末滅絕事件發生時](../Page/白堊紀末滅絕事件.md "wikilink")，情況被翻轉了，大部分[鳥頸類主龍滅亡](../Page/鳥頸類主龍.md "wikilink")，只有[鳥類繼續存活](../Page/鳥類.md "wikilink")，而鑲嵌踝類主龍的鱷魚也繼續存活至今。

現在，[鱷魚](../Page/鱷魚.md "wikilink")、[短吻鱷](../Page/短吻鱷.md "wikilink")、[長吻鱷是這群古老且成功的](../Page/長吻鱷.md "wikilink")[演化支中仍然存活的物種](../Page/演化支.md "wikilink")。

## 系統發生學

## 參考資料

  - Michael Benton (2004), *Vertebrate Paleontology*, 3rd ed. Blackwell
    Science Ltd
  - Sereno, P. (1991), "Basal archosaurs: phylogenetic relationships and
    functional implications". *Journal of Vertebrate Paleontology*
    (Suppl.) **11**: 1-51.

## 外部連結

  - [Palaeos](https://web.archive.org/web/20050306102719/http://www.palaeos.com/Vertebrates/Units/270Archosauromorpha/270.510.html)

  - [taxonomic hierarchy according to
    Benton 2004](http://palaeo.gly.bris.ac.uk/benton/vertclass.html)

  - [Mikko's
    Phylogeny](http://www.helsinki.fi/~mhaaramo/metazoa/deuterostoma/chordata/archosauria/pseudosuchia/crurotarsi.html)

  -
[鑲嵌踝類主龍](../Category/鑲嵌踝類主龍.md "wikilink")