**釔**（）是[化學元素](../Page/化學元素.md "wikilink")，符號為**Y**，[原子序為](../Page/原子序.md "wikilink")39，是銀白色[過渡金屬](../Page/過渡金屬.md "wikilink")，化學性質與[鑭系元素相近](../Page/鑭系元素.md "wikilink")，且常歸為[稀土金屬](../Page/稀土金屬.md "wikilink")。\[1\]釔在自然中並不單獨出現，而是和鑭系元素結合出現在[稀土礦中](../Page/稀土.md "wikilink")。<sup>89</sup>Y是釔的唯一一種穩定[同位素和自然同位素](../Page/同位素.md "wikilink")。

1787年，在瑞典[伊特比附近發現了一種新的](../Page/伊特比.md "wikilink")[礦石](../Page/矿石.md "wikilink")，即，並根據發現地村落的名稱將它命名為「Ytterbite」。[約翰·加多林在](../Page/約翰·加多林.md "wikilink")1789年於阿列紐斯的礦物樣本中，發現了[氧化釔](../Page/氧化釔.md "wikilink")。\[2\]把這一氧化物命名為「Yttria」。[弗里德里希·維勒在](../Page/弗里德里希·維勒.md "wikilink")1828年首次分離出釔的單質。\[3\]

釔的最大用途在於[磷光體的生產](../Page/磷光體.md "wikilink")，特別是紅色[LED和電視機](../Page/LED.md "wikilink")[陰極射線管](../Page/陰極射線管.md "wikilink")（CRT）顯示屏的紅色磷光體。\[4\]釔元素也被用於[電極](../Page/電極.md "wikilink")、[電解質](../Page/電解質.md "wikilink")、[電子濾波器](../Page/電子濾波器.md "wikilink")、[激光器和](../Page/激光器.md "wikilink")[超導體中](../Page/超導體.md "wikilink")，也有多項醫學和材料科學上的應用。釔沒有已知的[生物用途](../Page/生物.md "wikilink")，人類接觸釔元素可導致[肺病](../Page/呼吸系統疾病.md "wikilink")。\[5\]

## 性質

釔是一種質軟、帶光澤的銀白色金屬晶體，在[元素週期表中屬於](../Page/元素週期表.md "wikilink")[3族](../Page/3族元素.md "wikilink")。根據週期表的趨勢，它的[電負性比前面的元素](../Page/電負性.md "wikilink")、[鈧和](../Page/鈧.md "wikilink")[5族中的下一個元素](../Page/5族元素.md "wikilink")[鋯都要低](../Page/鋯.md "wikilink")。由於[鑭系收縮現象](../Page/鑭系收縮.md "wikilink")，釔的電負性和[鑥相近](../Page/鑥.md "wikilink")。\[6\]\[7\]釔也是第五週期中的首個[d區塊元素](../Page/d區塊.md "wikilink")。

成塊的純釔在空氣中會在表面形成保護性氧化層（），這種“[鈍化](../Page/鈍化.md "wikilink")”過程使它相對穩定。在[水汽中加熱至](../Page/水汽.md "wikilink")750 °C時，保護層的厚度可達10微米。\[8\]不過釔粉末在空氣中很不穩定，其金屬屑都可以在400 °C以上的溫度在空氣中被點燃。\[9\]釔金屬在[氮氣中加熱至](../Page/氮.md "wikilink")1000 °C後會形成（YN）。\[10\]

  -
    4 Y + 3 O<sub>2</sub> → 2 Y<sub>2</sub>O<sub>3</sub>

<!-- end list -->

  -
    2 Y + N<sub>2</sub> → 2 YN

### 與鑭系元素的相似性

釔元素的性質和[鑭系元素十分相似](../Page/鑭系元素.md "wikilink")，所以在歷史上曾一同被歸為[稀土元素](../Page/稀土元素.md "wikilink")。\[11\]自然中的釔一定與鑭系元素共同出現在[稀土礦中](../Page/稀土金属.md "wikilink")。\[12\]

在化學屬性上，鑭系元素比釔旁邊的[鈧更接近釔](../Page/鈧.md "wikilink")。\[13\]如果以物理屬性對[原子序作圖](../Page/原子序.md "wikilink")，根據趨勢，釔的原子序像是在64.5和67.5之間，即位於鑭系元素[釓和](../Page/釓.md "wikilink")[鉺之間](../Page/鉺.md "wikilink")。\[14\]

釔的反應級數也一般在這個區間之內，\[15\]化學反應活性也與[鋱和](../Page/鋱.md "wikilink")[鏑相近](../Page/鏑.md "wikilink")。\[16\]它的大小與屬於「釔族」的重鑭系元素幾乎相同，所以它們的離子在溶液中的屬性十分接近。\[17\]\[18\]雖然所有鑭系元素在元素週期表中都位於釔以下的一行，但釔在多方面都卻與其極為相似，這是由於[鑭系收縮現象](../Page/鑭系收縮.md "wikilink")。\[19\]

釔和鑭系元素間最大的差異在於，釔幾乎只會形成[三價化合物或](../Page/化合價.md "wikilink")[離子](../Page/离子.md "wikilink")，但大約半數鑭系元素都可以形成三價以外的價態。\[20\]

### 化合物及反應

釔可以形成各種[無機化合物](../Page/無機化學.md "wikilink")，氧化態一般為+3，其中釔原子失去其3顆[價電子](../Page/價電子.md "wikilink")。\[21\]例如白色、固態的[氧化釔(III)](../Page/氧化釔.md "wikilink")（）就是一種六[配位的三價釔化合物](../Page/配位鍵.md "wikilink")。\[22\]

釔可以形成不溶於水的[氟化物](../Page/氟化物.md "wikilink")、[氫氧化物和](../Page/氫氧化物.md "wikilink")[草酸鹽](../Page/草酸鹽.md "wikilink")，以及[可溶於水的](../Page/水溶性.md "wikilink")[溴化物](../Page/溴化物.md "wikilink")、[氯化物](../Page/氯化物.md "wikilink")、[碘化物](../Page/碘化物.md "wikilink")、[硝酸鹽和](../Page/硝酸鹽.md "wikilink")[硫酸鹽](../Page/硫酸鹽.md "wikilink")。\[23\]Y<sup>3+</sup>離子在溶液中無色，因為它的d和f[電子殼層中缺乏電子](../Page/電子殼層.md "wikilink")。\[24\]

釔及其化合物會和水產生反應，形成。\[25\]濃[硝酸和](../Page/硝酸.md "wikilink")[氫氟酸不會對釔產生快速侵蝕](../Page/氫氟酸.md "wikilink")，但其他的強酸則可以快速侵蝕釔，产生钇盐。\[26\]

在200 °C以上溫度，釔可以和各種[鹵素形成三](../Page/鹵素.md "wikilink")[鹵化物](../Page/鹵化物.md "wikilink")，如[三氟化釔](../Page/三氟化釔.md "wikilink")（）、[三氯化釔](../Page/三氯化釔.md "wikilink")（）和（）。\[27\][碳](../Page/碳.md "wikilink")、[磷](../Page/磷.md "wikilink")、[硒](../Page/硒.md "wikilink")、[矽和](../Page/矽.md "wikilink")[硫在高溫下也都可以和釔形成](../Page/硫.md "wikilink")[二元化合物](../Page/二元化合物.md "wikilink")。\[28\]

釔的有機化合物中都含有碳﹣釔鍵，其中一些化合物中的釔呈0氧化態。\[29\]\[30\]（科學家在氯化釔熔體中曾觀測到+2態，\[31\]以及在釔氧原子簇中觀測到+1態。\[32\]）有機釔化合物可以[催化某些三聚反應](../Page/催化.md "wikilink")。\[33\]這些化合物的合成過程都從開始，而則是經與濃[盐酸和](../Page/盐酸.md "wikilink")[氯化銨進行反應所得](../Page/氯化銨.md "wikilink")。\[34\]\[35\]

[哈普托數指中心原子對於周邊](../Page/哈普托數.md "wikilink")[配位體原子的配位數](../Page/配位體.md "wikilink")，符號為η。科學家首次在釔配合物中發現[碳硼烷配位體能以η](../Page/碳硼烷.md "wikilink")<sup>7</sup>哈普托數與d<sup>0</sup>金屬中心原子進行配位。\[36\][石墨層間化合物石墨](../Page/石墨層間化合物.md "wikilink")-Y和石墨-在氣化後會產生內嵌[富勒烯](../Page/富勒烯.md "wikilink")，例如Y@C<sub>82</sub>。\[37\][電子自旋共振研究顯示](../Page/電子自旋共振.md "wikilink")，這種富勒烯是由Y<sup>3+</sup>和(C<sub>82</sub>)<sup>3−</sup>離子對所組成的。\[38\]Y<sub>3</sub>C、Y<sub>2</sub>C和YC<sub>2</sub>等碳化物在[水解後會形成](../Page/水解.md "wikilink")[烴](../Page/烴.md "wikilink")。\[39\]

### 核合成及同位素

[太陽系中的釔元素是在](../Page/太陽系.md "wikilink")[恒星核合成過程中產生的](../Page/恒星核合成.md "wikilink")，大部份經[S-過程](../Page/S-過程.md "wikilink")（約72%），其餘的經[R-過程](../Page/R-過程.md "wikilink")（約28%）。\[40\]在R-過程中，輕元素在[超新星爆炸中進行快](../Page/超新星.md "wikilink")[中子捕獲](../Page/中子捕獲.md "wikilink")；而在S-過程中，輕元素在[紅巨星脈動時](../Page/紅巨星.md "wikilink")，在星體內部進行慢中子捕獲。\[41\]

[Mira_1997.jpg](https://zh.wikipedia.org/wiki/File:Mira_1997.jpg "fig:Mira_1997.jpg")。\]\]
在核爆炸和核反應爐中，釔同位素是[鈾](../Page/鈾.md "wikilink")[裂變過程中的一大產物](../Page/裂變.md "wikilink")。在[核廢料的處理上](../Page/核廢料.md "wikilink")，最重要的釔同位素為<sup>91</sup>Y和<sup>90</sup>Y，半衰期分別為58.51天和64小時。\[42\]雖然<sup>90</sup>Y的半衰期短，但它與其母同位素[鍶-90](../Page/鍶-90.md "wikilink")（<sup>90</sup>Sr）處於長期平衡狀態（即產生率接近衰變率），實際半衰期為29年。\[43\]

所有3族元素的[原子序都是奇數](../Page/原子序.md "wikilink")，所以穩定[同位素很少](../Page/同位素.md "wikilink")。\[44\]釔只有一種穩定同位素<sup>89</sup>Y，這也是它唯一一種自然同位素。在S-過程當中，經其他途徑產生的同位素有足夠時間進行[β衰變](../Page/β衰變.md "wikilink")（[中子轉換為](../Page/中子.md "wikilink")[質子](../Page/質子.md "wikilink")，並釋放[電子和](../Page/電子.md "wikilink")[反微中子](../Page/反微中子.md "wikilink")）。\[45\]中子數為50、82和126的[原子核](../Page/原子核.md "wikilink")（[原子量分別為](../Page/原子量.md "wikilink")90、138和208）特別穩定\[46\]，所以這種慢速過程使這些同位素能夠保持其較高的豐度。\[47\]<sup>89</sup>Y的質量數和中子數分別靠近90和50，所以其豐度也較高。

釔的人工合成同位素已知至少有32種，原子[質量數在](../Page/質量數.md "wikilink")76和108之間。\[48\]其中最不穩定的同位素為<sup>106</sup>Y，[半衰期只有](../Page/半衰期.md "wikilink")\>150[納秒](../Page/納秒.md "wikilink")（<sup>76</sup>Y的半衰期為\>200納秒）；最穩定的則為<sup>88</sup>Y，半衰期為106.626天。\[49\]<sup>91</sup>Y、<sup>87</sup>Y和<sup>90</sup>Y的半衰期分別為58.51天、79.8小時和64小時，而其餘所有人造同位素的半衰期都在一天以下，大部份甚至不到一小時。\[50\]

質量數在88或以下的釔同位素的主要衰變途徑是[正電子發射](../Page/正電子發射.md "wikilink")（質子→中子），形成[鍶](../Page/鍶.md "wikilink")（原子序為38）的同位素；\[51\]質量數在90或以上的則進行電子發射（中子→質子），形成[鋯](../Page/鋯.md "wikilink")（原子序為40）的同位素。\[52\]另外質量數在97或以上的同位素亦會進行少量β<sup>−</sup>緩發[中子發射](../Page/中子發射.md "wikilink")。\[53\]

釔的[同核異構體至少有](../Page/同核異構體.md "wikilink")20種，質量數在78和102之間。\[54\]\[55\]<sup>80</sup>Y和<sup>97</sup>Y的同核異構體超過一個。\[56\]釔的大部份同核異構體的穩定性都比[基態更低](../Page/基態.md "wikilink")，但<sup>78m</sup>Y、<sup>84m</sup>Y、<sup>85m</sup>Y、<sup>96m</sup>Y、<sup>98m1</sup>Y、<sup>100m</sup>Y和<sup>102m</sup>Y的半衰期都比它們的基態更高。這是因為這些同核異構體都進行β衰變，而不進行[同核異構體轉換](../Page/核同质异能素#衰变过程.md "wikilink")。\[57\]

## 歷史

1787年，同時為陸軍中尉和兼職化學家的卡爾·阿克塞爾·阿列紐斯（Carl Axel
Arrhenius）在瑞典[伊特比村](../Page/伊特比.md "wikilink")（現屬於[斯德哥爾摩群島](../Page/斯德哥爾摩群島.md "wikilink")）附近的一處舊採石場發現了一塊黑色大石。\[58\]他認為這是一種未知礦石，含有當時新發現的[鎢元素](../Page/鎢.md "wikilink")，\[59\]並將其命名為「Ytterbite」。\[60\]樣本被送往多個化學家作進一步分析。\[61\]

[Johan_Gadolin.jpg](https://zh.wikipedia.org/wiki/File:Johan_Gadolin.jpg "fig:Johan_Gadolin.jpg")\]\]
 的約翰·加多林（Johan
Gadolin）於1789年在阿列紐斯的樣本中發現了一種新的氧化物，並於1794發佈完整的分析結果。\[62\]\[63\]安德斯·古斯塔夫·埃克貝格（Anders
Gustaf
Ekeberg）在1797年證實了這項發現，並把氧化物命名為「Yttria」。\[64\]在[安東萬·拉瓦節提出首個近代](../Page/安東萬·拉瓦節.md "wikilink")[化學元素定義之後](../Page/化學元素.md "wikilink")，人們認為氧化物都能夠還原成元素，所以發現新氧化物就等同於發現新元素。對應於Yttria的元素因此被命名為「Yttrium」。\[65\]

1843年，卡爾·古斯塔夫·莫桑德（Carl Gustaf
Mosander）發現，該樣本中其實含有三種氧化物：白色的[氧化釔](../Page/氧化釔.md "wikilink")（Yttria）、黃色的[氧化鋱](../Page/七氧化四鋱.md "wikilink")（Erbia）以及玫紅色的[氧化鉺](../Page/氧化鉺.md "wikilink")（Terbia）。\[66\]\[67\]1878年，讓-夏爾·加利薩·德馬里尼亞（Jean
Charles Galissard de
Marigna）分離出第四種氧化物[氧化鐿](../Page/氧化鐿.md "wikilink")。\[68\]這四種氧化物所含的新元素都以伊特比命名，除釔以外還有[鐿](../Page/鐿.md "wikilink")（Ytterbium）、[鋱](../Page/鋱.md "wikilink")（Terbium）和[鉺](../Page/鉺.md "wikilink")（Erbium）。\[69\]在接下來的數十年間，科學家又在加多林的礦石樣本中發現了7種新元素。\[70\]馬丁·海因里希·克拉普羅特（Martin
Heinrich
Klaproth）後將這種礦物命名為加多林礦（Gadolinite，即），以紀念加多林為發現這些新元素所做出的貢獻。\[71\]

1828年，[弗里德里希·維勒把無水](../Page/弗里德里希·維勒.md "wikilink")[三氯化釔和](../Page/三氯化釔.md "wikilink")[鉀一同加熱](../Page/鉀.md "wikilink")，首次產生了釔金屬：\[72\]\[73\]

  -
    YCl<sub>3</sub> + 3 K → 3 KCl + Y

釔的化學符號最初是Yt，直到1920年代初才開始轉為Y。\[74\]

1987年，科學家發現[釔鋇銅氧具有](../Page/釔鋇銅氧.md "wikilink")[高溫超導性質](../Page/高溫超導.md "wikilink")。\[75\]它是第二種被發現擁有這種性質的物質，\[76\]而且是第一種能在[氮的沸點以上達到](../Page/氮.md "wikilink")[超導現象的物質](../Page/超導現象.md "wikilink")。\[77\]

## 存量

[Xenotímio1.jpeg](https://zh.wikipedia.org/wiki/File:Xenotímio1.jpeg "fig:Xenotímio1.jpeg")含有釔，圖為磷釔礦晶體。\]\]

### 豐度

釔元素出現在大部份\[78\]和某些[鈾礦中](../Page/鈾.md "wikilink")，但從不以單質出現。\[79\]釔在地球地殼中的豐度約為百萬分之31，\[80\]在所有元素中[排第28位](../Page/地球的地殼元素豐度列表.md "wikilink")，是[銀豐度的](../Page/銀.md "wikilink")400倍。\[81\]泥土中的釔含量介乎百萬分之10至150間（去水後平均重量佔百萬分之23），在海水中含量為一兆（萬億）分之9。\[82\]美國[阿波羅計劃期間從](../Page/阿波羅計劃.md "wikilink")[月球採得的岩石樣本中含有較高的釔含量](../Page/月球.md "wikilink")。\[83\]

釔元素沒有已知的生物用途，但幾乎所有生物體內都存在少量的釔。進入人體後，釔主要積累在肝、腎、脾、肺和骨骼當中。\[84\]一個人體內一共只有約0.5毫克的釔，而[人乳則含有百萬分之](../Page/人乳.md "wikilink")4的釔。\[85\]在食用植物中，釔的含量在百萬分之20至100之間（鮮重），其中以[捲心菜為最高](../Page/捲心菜.md "wikilink")；\[86\][木本植物種子中的含量為百萬分之](../Page/木本植物.md "wikilink")700，是植物中已知最高的。\[87\]

### 生產

釔的化學性質與鑭系元素非常相似，所以經過各種自然過程，這些元素都一同出現在稀土礦中。\[88\]\[89\]

[Yttrium_1.jpg](https://zh.wikipedia.org/wiki/File:Yttrium_1.jpg "fig:Yttrium_1.jpg")
稀土元素共有四種來源：\[90\]

  - 含碳酸鹽和氟化物的礦石，如（\[([Ce](../Page/鈰.md "wikilink"),
    [La](../Page/鑭.md "wikilink"),
    …)(CO<sub>3</sub>)F\]），平均釔含量為0.1%。\[91\]\[92\]1960年代至1990年代間，氟碳鈰礦的主要來源是美國[加州山口](../Page/加州.md "wikilink")（Mountain
    Pass）稀土礦場，因此美國是這段時期稀土元素的最大產國。\[93\]\[94\]
  - [獨居石](../Page/獨居石.md "wikilink")（即磷鈰鑭礦，\[(Ce, La,
    …)[PO<sub>4</sub>](../Page/磷酸鹽.md "wikilink")\]）是一種[漂沙沉積物](../Page/漂沙沉積.md "wikilink")，為[花崗岩移動及重力分離之後的產物](../Page/花崗岩.md "wikilink")。獨居石含2%\[95\]（或3%）\[96\]的釔。20世紀初的最大礦藏位於印度和巴西，兩國當時是最大產國。\[97\]\[98\]
  - [磷釔礦是一種含有稀土元素的磷酸鹽礦物](../Page/磷釔礦.md "wikilink")，其中包括[磷酸釔](../Page/磷酸釔.md "wikilink")（YPO<sub>4</sub>），礦物的釔含量約為60%。\[99\]最大礦藏是位於中國[內蒙古的](../Page/內蒙古.md "wikilink")[白雲鄂博鐵礦](../Page/白雲鄂博鐵礦.md "wikilink")。在1990年代山口稀土礦場關閉之後，中國繼而成為目前稀土元素的最大產國。\[100\]\[101\]
  - 離子吸附型粘土是花崗岩的風化產物，含1%的稀土元素。\[102\]處理後的精礦的釔含量可以達到8%。離子吸附型粘土主要在中國南部開採生產。\[103\]\[104\]\[105\]釔也出現在和中。\[106\]

從混合氧化物礦中提取純釔的其中一種方法是把樣本溶於[硫酸](../Page/硫酸.md "wikilink")，再以[離子交換](../Page/離子交換.md "wikilink")[層析法進行分離](../Page/層析法.md "wikilink")。加入[草酸後](../Page/草酸.md "wikilink")，草酸釔會沉澱出來。草酸釔在氧氣中加熱，會轉化為[氧化釔](../Page/氧化釔.md "wikilink")，再與[氟化氫反應後變為](../Page/氟化氫.md "wikilink")[氟化釔](../Page/氟化釔.md "wikilink")。\[107\]使用[季銨鹽作為萃取劑](../Page/季銨鹽.md "wikilink")，釔會維持水溶狀態。以硝酸鹽作抗衡離子，可以去除輕鑭系元素；以[硫氰酸鹽作抗衡離子](../Page/硫氰酸鹽.md "wikilink")，可以去除重鑭系元素。這種過程可以產生純度為99.999%的釔。一般釔佔重鑭系元素混合物的三分之二，所以為了方便分離其他的元素，須先移除釔元素。

全球氧化釔年產量在2001年達到600噸，儲備量估計有9百萬噸。\[108\][鈣](../Page/鈣.md "wikilink")[鎂合金可以把三氟化釔還原成海綿狀釔金屬](../Page/鎂.md "wikilink")，如此生產出的釔金屬每年不到10噸。[電弧爐所達到的](../Page/電弧爐.md "wikilink")1,600 °C溫度足以熔化釔金屬。\[109\]\[110\]

## 應用

### 日用品

[Aperture_Grille.jpg](https://zh.wikipedia.org/wiki/File:Aperture_Grille.jpg "fig:Aperture_Grille.jpg")電視機螢屏中紅色[磷光體的元素](../Page/磷光體.md "wikilink")。\]\]
[氧化釔](../Page/氧化釔.md "wikilink")（）可以做[摻](../Page/摻雜_\(半導體\).md "wikilink")[Eu<sup>3+</sup>過程中所用的主體晶格](../Page/銪.md "wikilink")，以及[正釩酸釔Y](../Page/釩酸釔.md "wikilink")[VO<sub>4</sub>](../Page/釩酸鹽.md "wikilink"):Eu<sup>3+</sup>或氧硫化釔:Eu<sup>3+</sup>磷光體的反應劑。這些磷光體在彩色電視機的顯像管中能產生紅光。\[111\]\[112\]實際上紅光是銪所產生的，釔只是把的能量傳遞到磷光體上。\[113\]釔化合物還可以為不同鑭系元素陽離子做摻雜過程的主體晶格，除了Eu<sup>3+</sup>外，還有能發出綠光的摻[Tb<sup>3+</sup>磷光體](../Page/鋱.md "wikilink")。氧化釔可以在多孔[氮化矽的生產過程中作](../Page/氮化矽.md "wikilink")[燒結添加劑](../Page/燒結.md "wikilink")。\[114\]它還是[材料科學中的常用原料](../Page/材料科學.md "wikilink")，許多釔化合物的合成也需要從氧化釔開始。

釔同位素可以[催化](../Page/催化.md "wikilink")[乙烯的](../Page/乙烯.md "wikilink")[聚合反應](../Page/聚合反應.md "wikilink")。\[115\]一些高性能[火花塞的電極以釔金屬作為材料](../Page/火花塞.md "wikilink")。\[116\]在[丙烷燈網罩的生產過程中](../Page/丙烷.md "wikilink")，釔可以代替具有[放射性的](../Page/放射性.md "wikilink")[釷元素](../Page/釷.md "wikilink")。\[117\]

釔穩定氧化鋯是一種正在研發當中的材料，可以做固態[電解質](../Page/電解質.md "wikilink")，以及在汽車排氣系統中用於探測氧含量。\[118\]

### 石榴石

[Yag-rod.jpg](https://zh.wikipedia.org/wiki/File:Yag-rod.jpg "fig:Yag-rod.jpg")
釔可以用來生產各種合成[石榴石](../Page/石榴石.md "wikilink")。\[119\]（，簡稱YIG）是十分有效的[微波](../Page/微波.md "wikilink")[電子濾波器](../Page/電子濾波器.md "wikilink")，生產就需用到氧化釔。\[120\]釔、[鐵](../Page/鐵.md "wikilink")、[鋁和](../Page/鋁.md "wikilink")[釓石榴石](../Page/釓.md "wikilink")（如Y<sub>3</sub>(Fe,Al)<sub>5</sub>O<sub>12</sub>和Y<sub>3</sub>(Fe,Ga)<sub>5</sub>O<sub>12</sub>）具有重要的[磁性質](../Page/磁性.md "wikilink")。\[121\]釔鐵石榴石是一種高效聲能發射器和傳感器。\[122\][釔鋁石榴石](../Page/釔鋁石榴石.md "wikilink")（，簡稱YAG）的[莫氏硬度為](../Page/莫氏硬度.md "wikilink")8.5，能當寶石作首飾之用（人造[鑽石](../Page/鑽石.md "wikilink")）。\[123\]摻[鈰的釔鋁石榴石](../Page/鈰.md "wikilink")（YAG:Ce）晶體可用在白色[發光二極體的磷光體中](../Page/發光二極體.md "wikilink")。\[124\]\[125\]\[126\]

釔鋁石榴石、氧化釔、[氟化釔鋰](../Page/氟化釔鋰.md "wikilink")（）和[正釩酸釔](../Page/釩酸釔.md "wikilink")（）可以用在近[紅外線](../Page/紅外線.md "wikilink")[激光器中](../Page/激光器.md "wikilink")，可用的摻雜劑包括[釹](../Page/釹.md "wikilink")、[鉺和](../Page/鉺.md "wikilink")[鐿](../Page/鐿.md "wikilink")。\[127\]\[128\]釔鋁石榴石激光器能夠在大功率下運作，可應用在金屬鑽孔和切割上。\[129\]單個釔鋁石榴石晶體一般是經由[柴可拉斯基法生產出來的](../Page/柴可拉斯基法.md "wikilink")。\[130\]

### 材料增強

添加少量的釔（0.1%至0.2%）可以降低[鉻](../Page/鉻.md "wikilink")、[鉬](../Page/鉬.md "wikilink")、[鈦和](../Page/鈦.md "wikilink")[鋯的晶粒度](../Page/鋯.md "wikilink")。\[131\]它也可以增強[鋁合金和](../Page/鋁.md "wikilink")[鎂合金的材料強度](../Page/鎂.md "wikilink")。\[132\]在合金中加入釔，可以降低加工程序的難度，使材料能抵抗高溫再結晶，並且大大提高對高溫[氧化的抵禦能力](../Page/氧化.md "wikilink")。\[133\]

釔還能對[釩以及其他](../Page/釩.md "wikilink")[非鐵金屬進行去氧](../Page/非鐵金屬.md "wikilink")。\[134\]氧化釔可以穩定[立方氧化鋯的結構](../Page/立方氧化鋯.md "wikilink")，使它適合作為首飾。\[135\]

科學家正在研究釔的球化性質，這可能有助生產[球墨鑄鐵](../Page/球墨鑄鐵.md "wikilink")。如此生產出來的[鑄鐵具有較高的](../Page/鑄鐵.md "wikilink")[延展性](../Page/延展性.md "wikilink")（[石墨形成小球](../Page/石墨.md "wikilink")，而非薄片）。\[136\]氧化釔[熔點高](../Page/熔點.md "wikilink")，可抵抗衝擊，且[熱膨脹係數也較低](../Page/熱膨脹係數.md "wikilink")，因此能用來製造[陶瓷和](../Page/陶瓷.md "wikilink")[玻璃](../Page/玻璃.md "wikilink")，\[137\]例如某些照相機[鏡頭](../Page/鏡頭.md "wikilink")。\[138\]

[Yttrium-90_isotope_simulation_test.jpg](https://zh.wikipedia.org/wiki/File:Yttrium-90_isotope_simulation_test.jpg "fig:Yttrium-90_isotope_simulation_test.jpg")

### 醫學

釔-90是一種[放射性同位素](../Page/放射性同位素.md "wikilink")，被用在及等抗癌藥物中，可治療[淋巴癌](../Page/淋巴癌.md "wikilink")、[白血病](../Page/白血病.md "wikilink")、[卵巢癌](../Page/卵巢癌.md "wikilink")、[大腸癌](../Page/大腸癌.md "wikilink")、[胰腺癌和](../Page/胰腺癌.md "wikilink")等等。\[139\]該藥物會附在[單克隆抗體上](../Page/單克隆抗體.md "wikilink")，與癌症細胞結合後以釔-90的強烈[β輻射把癌細胞中的DNA產生變異](../Page/β粒子.md "wikilink")，經過半衰期間內的放射曝露，之後經由生物轉殖的特性，致使癌細胞DNA無法繼續往下轉錄繁衍，一般被仍定為成功的治療，約需經過3-6個月的觀察週期而論。不過釔90仍舊屬於局部[放射療法之一](../Page/放射療法.md "wikilink")，仍舊可能帶給治療患者不可預期的傷害，例如：急性肝衰竭。\[140\]

用釔-90做的針頭可以比解剖刀更加精確，可用於割斷[脊髓裡的疼痛](../Page/脊髓.md "wikilink")[神經](../Page/神經.md "wikilink")。\[141\]在治療[類風濕性關節炎時](../Page/類風濕性關節炎.md "wikilink")，釔-90還能用在發炎關節的滑膜切除術中，特別針對膝蓋部位。\[142\]

曾有實驗在犬類身上用摻釹的釔鋁石榴石激光來進行[前列腺切除術](../Page/前列腺.md "wikilink")，手術由機械人協助，能夠降低對周邊神經等組織的損傷。\[143\]摻鉺的釔鋁石榴石則開始被用在磨皮整容手術上。\[144\]

### 超導體

[YBCO-modified.jpg](https://zh.wikipedia.org/wiki/File:YBCO-modified.jpg "fig:YBCO-modified.jpg")超導體\]\]
1987年，[阿拉巴馬大學和](../Page/阿拉巴馬大學.md "wikilink")[休斯頓大學研發了](../Page/休斯頓大學.md "wikilink")[釔鋇銅氧](../Page/釔鋇銅氧.md "wikilink")（YBa<sub>2</sub>Cu<sub>3</sub>O<sub>7</sub>，又稱YBCO或1-2-3）[超導體](../Page/超導體.md "wikilink")。\[145\]它可以在93 K溫度下運作，比[液氮的沸點](../Page/液氮.md "wikilink")（77.1 K）要高。\[146\]其他超導體都必須使用價格更高的[液氦降溫](../Page/液氦.md "wikilink")，所以這項發現能降低成本。

實際超導材料的化學式為YBa<sub>2</sub>Cu<sub>3</sub>O<sub>7–*d*</sub>，其中*d*必須低於0.7才會使材料成為超導體。具體原因未知，但目前科學家知道在晶體內只有某些位置會出現空缺，即位於氧化銅平面和鏈上。這造成銅原子擁有奇特的氧化態，這再因某種原因引致了超導性質。

[BCS理論在](../Page/BCS理論.md "wikilink")1957年被發佈之後，人們對低溫超導的認知已經非常詳盡了。這種現象與兩顆電子在一個晶格當中的特殊交互作用相關。然而高溫超導卻在這一理論的解釋範圍外，其確切原理仍是未知的。實驗所得出的結果指出，材料中氧化銅份量必須十分準確才能帶出超導性質。\[147\]

這一物質呈黑綠色，為一多晶、多相態礦物。科學家正在研究一類成份比例不同的物質，稱為[鈣鈦礦](../Page/鈣鈦礦.md "wikilink")，並希望能最終研發出一種更為實用的[高溫超導體](../Page/高溫超導體.md "wikilink")。\[148\]

## 安全性

水溶釔化合物具微毒性，但非水溶化合物則不具毒性\[149\]。[動物實驗顯示](../Page/動物實驗.md "wikilink")，釔及其化合物會造成肝和肺的破壞，但不同化合物的毒性程度各異。老鼠在吸入[檸檬酸釔後](../Page/檸檬酸.md "wikilink")，產生和[呼吸困難](../Page/呼吸困難.md "wikilink")，吸入氯化釔後則有肝性水腫、及肺充血等症狀。\[150\]

釔化合物對人類可引致肺病。\[151\]釩酸釔銪飄塵會對人的眼部、皮膚和上呼吸道有輕微的刺激，但這可能是飄塵的釩成份所導致的，而不是釔。\[152\]短期暴露在大量釔化合物中，會引致呼吸急促、咳嗽、胸部疼痛以及[發紺](../Page/發紺.md "wikilink")。\[153\][美國國家職業安全衛生研究所](../Page/美國國家職業安全衛生研究所.md "wikilink")（NIOSH）所建議的允許暴露限值為1 mg/m<sup>3</sup>，超過500 mg/m<sup>3</sup>時屬於「即時對生命或健康造成危險」。\[154\]雖然成塊的釔金屬在空氣中相對穩定，但釔金屬粉末卻屬於易燃物。\[155\]

## 備註

<references group="注" />

## 參考資料

## 書目

  - <cite id=Daane1968>

  - <cite id=Emsley2001></cite>

  - <cite id=Gadolin1794></cite>

  -
  - <cite id=Stwertka1998></cite>

  - <cite id=Krogt></cite>

## 外部連結

  - [Yttrium](http://www.periodicvideos.com/videos/039.htm) at *The
    Periodic Table of Videos*（諾丁漢大學）

[Category:过渡金属](../Category/过渡金属.md "wikilink")
[\*](../Category/钇.md "wikilink")
[5C](../Category/第5周期元素.md "wikilink")
[5C](../Category/化学元素.md "wikilink")

1.
2.
3.
4.
5.
6.
7.

8.
9.
10.
11.

12. [Emsley 2001](../Page/#Emsley2001.md "wikilink"), p. 498

13. [Daane 1968](../Page/#Daane1968.md "wikilink"), p. 810

14. [Daane 1968](../Page/#Daane1968.md "wikilink"), p. 815

15.
16.
17.
18.

19.

20. [Daane 1968](../Page/#Daane1968.md "wikilink"), p. 817

21.

22.

23.
24.
25.
26.
27.
28.
29.

30.

31.

32.

33.
34.

35.

36.
37.
38.
39.
40.

41.

42.
43.
44.

45.
46. 參見：[幻數](../Page/幻數.md "wikilink")。這些原子核的[中子捕獲截面很低](../Page/中子截面.md "wikilink")，所以穩定性異常高。這些同位素不易發生β衰變，所以擁有較高的豐度。

47.

48.

49.
50.
51.
52.
53.

54.
55. 同核異構體亦稱亞穩態，其能量比處於[基態的原子核更高](../Page/基態.md "wikilink")。亞穩態在釋放[伽馬射線或](../Page/伽馬射線.md "wikilink")之後，才會回到基態。亞穩態以同位素質量數旁的「m」表示。

56.
57.
58.
59. [Emsley 2001](../Page/#Emsley2001.md "wikilink"), p. 496

60. 「Ytterbite」取自發現地村名「Ytterby」，而「-bite」則是礦物的通用後綴。

61. [Van der Krogt 2005](../Page/#Krogt.md "wikilink")

62. [Gadolin 1794](../Page/#Gadolin1794.md "wikilink")

63. [Stwertka 1998](../Page/#Stwertka1998.md "wikilink"), p.
    115稱加多林在1789年發現該氧化物，但未指何時發佈。[Van der Krogt
    2005引用原文獻](../Page/#Krogt.md "wikilink")，并註明[1794年加多林著](../Page/#Gadolin1794.md "wikilink")。

64.

65. 氧化物名稱均以「-a」結尾，而新元素名則一般以「-ium」結尾。

66.

67. [鋱和](../Page/鋱.md "wikilink")[鉺的名稱分別是Terbium和Erbium](../Page/鉺.md "wikilink")，但兩者的氧化物卻分別稱為「Erbium」和「Terbium」，拼法相反。

68.

69.
70.
71.
72.

73.

74.

75.
76.

77. 釔鋇銅氧的超導臨界溫度（T<sub>c</sub>）為93 K，而氮的沸點為77 K。

78.
79.

80.

81.
82.
83. [Stwertka 1998](../Page/#Stwertka1998.md "wikilink"), p. 115

84.

85.
86.
87.
88.

89.

90.

91.
92.
93.
94.
95.
96. [Stwertka 1998](../Page/#Stwertka1998.md "wikilink"), p. 116

97.
98.
99.
100.
101.
102.
103.
104.
105.

106. [Emsley 2001](../Page/#Emsley2001.md "wikilink"), p. 497

107.
108.
109.
110.

111.
112.
113. [Daane 1968](../Page/#Daane1968.md "wikilink"), p. 818

114.

115.
116.

117.

118.
119.

120.
121.
122.

123.
124.

125.

126.

127.

128.

129.
130.

131.

132.
133.
134.
135.

136.
137.
138.
139. [Emsley 2001](../Page/#Emsley2001.md "wikilink"), p. 495

140.

141.
142.

143.

144.
145.
146.
147.

148.
149.
150. （公有領域）

151.
152.
153.
154.

155.