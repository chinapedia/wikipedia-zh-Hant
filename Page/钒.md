**钒**（Vanadium），元素符号**V**，[化学元素之一](../Page/化学元素.md "wikilink")，原子序数为23。钒音译自英语*Vanadium*，其词根源于**日耳曼神话**中古日耳曼语的女神名字。这名字源于钒有许多色彩鲜艳的化合物。

钒為有韌性及延展性之堅硬銀灰[过渡金属](../Page/过渡金属.md "wikilink")，在自然界僅以化合態存在，一般用於材料工程作为合金成分。

## 特性

钒為一中等硬度可延展的銀灰過渡金屬，有些描述形容它很「軟」，應是因為它的延展性。不易腐蚀，在[碱](../Page/碱.md "wikilink")、[硫酸和](../Page/硫酸.md "wikilink")[盐酸中它相当稳定](../Page/盐酸.md "wikilink")。在933K（660
℃）以上的温度中它氧化为[五氧化二钒](../Page/五氧化二钒.md "wikilink")（V<sub>2</sub>O<sub>5</sub>）。钒的结构强度相当高。

在氧化物中钒一般显+5价，但也有+2、+3和+4价的氧化物存在，不过它们比较容易过渡为+5价的[氧化物](../Page/氧化物.md "wikilink")。2价和3价的钒氧化物是[碱性的](../Page/碱性.md "wikilink")，4价的氧化物是[两性的](../Page/两性.md "wikilink")，5价的氧化物是[酸性的](../Page/酸性.md "wikilink")。

一个很有趣的试验是用[锌来还原无色的](../Page/锌.md "wikilink")[钒酸铵](../Page/钒酸铵.md "wikilink")（NH<sub>4</sub>VO<sub>3</sub>）。在试验的过程中钒相继被还原成蓝色的四价钒、绿色的三价钒、紫蓝色的二价钒，随后低价的钒又会被[空气中的](../Page/空气.md "wikilink")[氧氧化为金黃色的五价钒](../Page/氧.md "wikilink")。由于钒的价数很容易改变，它也经常被用做[催化剂](../Page/催化剂.md "wikilink")。+1价的钒很少出现。理论上0、-1和-3价的钒也有可能。

## 应用

大约80%的钒和[铁一起作为](../Page/铁.md "wikilink")[钢裡的合金元素](../Page/钢.md "wikilink")。含钒的钢很硬很坚实，但一般其钒含量少于1%。

  - 含钒的合金有：
      - 运用在医疗器械中的特别的[不锈钢](../Page/不锈钢.md "wikilink")
      - 运用在工具中的不锈钢
      - 与[铝一起作为](../Page/铝.md "wikilink")[钛合金物运用在高速飞机的涡轮喷气发动机中](../Page/钛.md "wikilink")
      - 含钒的钢经常被用在轴、齿轮等关键的机械部分中
  - 钒吸收[裂变](../Page/裂变.md "wikilink")[中子的截面很小](../Page/中子.md "wikilink")，因此被用在**核工业**中
  - 在炼钢过程中钒被用来促进碳化物的形成
  - 在给钢涂钛的时候钒往往被作为中介层
  - 钒与[镓的合金可以用来制作超导电磁铁](../Page/镓.md "wikilink")，其磁强度可达175,000[高斯](../Page/高斯_\(单位\).md "wikilink")
  - 在制造**[缩苹果酸酐](../Page/缩苹果酸酐.md "wikilink")**和**[硫酸](../Page/硫酸.md "wikilink")**的过程中[五氧化二钒被用来做催化剂](../Page/五氧化二钒.md "wikilink")
  - 五氧化二钒（V<sub>2</sub>O<sub>5</sub>）被用来制做特殊的陶瓷作为催化剂
  - 可做藍色顏料，稱為「釩鋯藍」或「土耳其藍」

## 历史

1801年西班牙矿物学家[安德烈·曼纽尔·德·里奥在](../Page/安德烈·曼纽尔·德·里奥.md "wikilink")[墨西哥城在一个铅矿中首先发现了钒](../Page/墨西哥城.md "wikilink")，但他错误地以为他所发现的只不过是一种不纯的铬。1831年瑞典化学家[尼尔斯·加布里埃尔·塞夫斯特瑞姆在与铁矿做试验时重新发现了钒](../Page/尼尔斯·加布里埃尔·塞夫斯特瑞姆.md "wikilink")，同年[弗里德里希·维勒证实了德](../Page/弗里德里希·维勒.md "wikilink")·里欧的发现。1867年**亨利·英弗尔德·罗斯科**用[氢还原亚氯酸化钒](../Page/氢.md "wikilink")（III）首次得到了纯的钒。

塞夫斯特瑞姆给钒按日尔曼神话中美丽女神的名字起了名，因为钒的化合物色彩缤纷。

## 生理

在生物体内钒是一些[酶的必要组成部分](../Page/酶.md "wikilink")。一些固氮的微生物使用含钒的酶来固定空气中的[氮](../Page/氮.md "wikilink")。

鼠和鸡也需要少量的钒，缺钒会阻碍它们的生长和繁殖。含钒的血红蛋白存在于[海鞘类动物中](../Page/被囊动物亚门.md "wikilink")。

一些含钒的物质具有类似[胰岛素的效应](../Page/胰岛素.md "wikilink")，也许可以用来治疗[糖尿病](../Page/糖尿病.md "wikilink")。

## 同位素

釩共有31種同位素，其中<sup>51</sup>V穩定。

## 来源

在大自然中钒一般以化合物存在。约65种钒的化合物在自然中出现，其中有

  - 硫化钒 （VS<sub>4</sub>）

  - 绿硫钒矿 （VS<sub>2</sub>或V<sub>2</sub>S<sub>4</sub>）

  - [钒铅矿](../Page/钒铅矿.md "wikilink")
    \[Pb<sub>5</sub>(VO<sub>4</sub>)<sub>3</sub>Cl\]

  - 钒云母
    \[KV<sub>2</sub>（AlSi<sub>3</sub>O<sub>10</sub>）（OH）<sub>2</sub>\]

  - 钒酸钾铀矿
    \[K<sub>2</sub>(UO<sub>2</sub>)<sub>2</sub>(VO<sub>4</sub>)<sub>2</sub>·3H<sub>2</sub>O\]

  -
  - 钒钛磁铁矿也是是钒的来源之一

在[矾土和](../Page/矾土.md "wikilink")[石油](../Page/石油.md "wikilink")、[煤](../Page/煤.md "wikilink")、[油页岩中也含有大量钒](../Page/油页岩.md "wikilink")，特别是[委内瑞拉和](../Page/委内瑞拉.md "wikilink")[加拿大的石油中能找到钒](../Page/加拿大.md "wikilink")。光谱分析发现在太阳和一些恒星的表面也有钒。

## 生产

纯的金属钒一般是用[钾在高压下将五氧化二钒还原而得到的](../Page/钾.md "wikilink")。大多数钒是其它矿物加工时的副产品。工业上也可以以铝，焦炭还原五氧化二钒生产纯钒。

## 化合物

五氧化二釩是釩最重要的化合物，常被用来做催化剂、染料和固色剂。五氧化二钒加熱可放出氧氣，且這個反應是可逆的。五氧化二钒的性質可催化[二氧化硫](../Page/二氧化硫.md "wikilink")、[苯和](../Page/苯.md "wikilink")[萘的氧化反應](../Page/萘.md "wikilink")，在工業上用來製造[硫酸](../Page/硫酸.md "wikilink")、鄰苯二甲酐和順丁烯二酐。五氧化二钒是橙色的，具有毒性，不同于大多数金属氧化物，五氧化二钒微溶于水。它是[兩性化合物](../Page/兩性_\(化學\).md "wikilink")，可以与酸和碱反应。它也是[氧化劑](../Page/氧化劑.md "wikilink")。

[Vanadiumoxidationstates.jpg](https://zh.wikipedia.org/wiki/File:Vanadiumoxidationstates.jpg "fig:Vanadiumoxidationstates.jpg")

## 注意

  - 钒很易燃。
  - 钒的化合物毒性很高。
  - 含钒的尘埃被吸入后会导致[肺癌](../Page/肺癌.md "wikilink")。

[Category:过渡金属](../Category/过渡金属.md "wikilink")
[钒](../Category/钒.md "wikilink")
[4E](../Category/第4周期元素.md "wikilink")
[4E](../Category/化学元素.md "wikilink")