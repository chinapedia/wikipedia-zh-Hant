**钟开莱**（，），浙江杭州人，生于[上海](../Page/上海.md "wikilink")，卒于菲律宾罗哈斯。华裔[数学家](../Page/数学家.md "wikilink")、世界著名[概率专家](../Page/概率.md "wikilink")，“概率学界学术教父”。

## 生平

钟开莱1917年生于上海，浙江杭州人\[1\]。1936年考入[清华大学](../Page/清华大学.md "wikilink")，先在[物理系学习](../Page/物理.md "wikilink")，后因与时任[西南联合大学](../Page/西南联合大学.md "wikilink")（随着抗日西遷[重慶](../Page/重慶.md "wikilink")，清华等合并为西南联大，重组于云南[昆明](../Page/昆明.md "wikilink")）理学院院长的[吴有训](../Page/吴有训.md "wikilink")“有所不快”转入数学系\[2\]，1940年数学系本科毕业。研究生期间，先师从[华罗庚学习数论](../Page/华罗庚.md "wikilink")，后因与之“有所不快”\[3\]，转投中国概率论与数理统计研究开拓者[许宝騄学习概率论](../Page/许宝騄.md "wikilink")\[4\]。1942年清华大学（西南联大）数学系研究生毕业。1942年至1945年任[昆明](../Page/昆明.md "wikilink")[西南联合大学](../Page/西南联合大学.md "wikilink")
北京大学讲师（他毕业后西南联大-清华大学聘他为助教， 而西南联大-北京大学聘他当讲师。
于是他应了北大之聘）。1944年同时考取第六届\[庚子赔款\]公费留英和清华公费留美奖学金，他最终选择了赴美学习，1945年底赴[美国普林斯顿大学留学](../Page/美国.md "wikilink")，用两年时间于1947年获[普林斯顿大学](../Page/普林斯顿大学.md "wikilink")[博士学位](../Page/博士.md "wikilink")，并曾参与过爱因斯坦的研究科题，师从当时正在普林斯顿大学访问的瑞典斯德哥尔摩大学教授、统计理论巨匠[哈拉尔德·克拉梅尔](../Page/哈拉尔德·克拉梅尔.md "wikilink")，博士论文答辩委员会成员还有著名统计学家、快速傅里叶变换共同发明人[约翰·图基](../Page/约翰·图基.md "wikilink")。

20世纪六十年代后任斯坦福大学数学系教授、系主任、名誉教授。在此之前，还任教于芝加哥大学、哥伦比亚大学、加州伯克利大学、康奈尔大学和、[雪城大學](../Page/雪城大學.md "wikilink")。除此之外，还在多所世界著名学府拥有客座席位（visiting
appointments），如法国[斯特拉斯堡大学](../Page/斯特拉斯堡大学.md "wikilink")，意大利[比萨大学](../Page/比萨大学.md "wikilink")，瑞士[苏黎世联邦理工学院和](../Page/苏黎世联邦理工学院.md "wikilink")[伊利诺伊大学厄巴纳-尚佩恩分校](../Page/伊利诺伊大学厄巴纳-尚佩恩分校.md "wikilink")。在斯坦福大学期间，他在[布朗运动](../Page/布朗运动.md "wikilink")（常用于预测股票市场的无规则运动）和[马尔可夫链的一般数学理论中做出了基础性和显著性的工作](../Page/马尔可夫链.md "wikilink")。1981年联合发起了随机过程研讨会，为学术思想交流和年轻人培养提供了专业社区平台。\[5\]

钟开莱为世界知名概率学家，人称“概率学界学术教父”，据不完全统计，他直接指导的博士生有15位，间接的学术晚辈则超过四百多个\[6\]，被誉为二十世纪后半叶的概率学界领袖之一\[7\]。

钟开莱有十余部著作，其清晰的逻辑和严谨的叙述，使他的概率论教材已经成为享誉世界的经典，被世界75%以上的大学的数万名学生使用，影响了几代概率论学生。在2008年出版了他跨越七十年的研究论文部分选集\[8\]，由曾经的合作者和博士生主编，以庆祝他的九十岁生日。

1978年，钟开莱和[鞅理论发展人Joseph](../Page/鞅.md "wikilink")
Doob等人访问中国，促进了概率论中国研究者和世界学者的交流，此后又多次回到中国开设短期课程和讲座，帮助年轻的中国学生有机会到美国继续深造。

此外，钟开莱广泛涉猎文学、音乐和京剧，退休后还学习了意大利语，并把一本概率论英文教材翻译为了俄语。

2009年6月1日在[菲律宾](../Page/菲律宾.md "wikilink")[罗哈斯市睡梦中自然去世](../Page/罗哈斯市.md "wikilink")，享年岁。

2010年在中国[北京大学举办了一次纪念他的国际会议](../Page/北京大学.md "wikilink")\[9\]。2012年钟开莱的家属将其数学藏书捐给北大数学学院，总计超过三百本\[10\]。

## 轶事

普林斯顿大学概率学家辛勒（）教授為其益友，双方书信交流长达四十多年，钟开莱晚年受眼疾困扰，最后两年几近失明，仍然坚持着把多行交叉在一起、模糊不可认的书信寄送给辛勒。

## 家庭

钟开莱妻子是菲律宾人Lilia，育有两个子女Daniel和Marilda，和四个孙辈Alex, Adam, Davison和Vanessa.

## 著作

  - 2005\. *Markov Processes, Brownian Motion, and Time Symmetry*, 2nd.
    with John B. Walsh. Springer
  - 2003\. ''Introduction to Random Time and Quantum Randomness ''. with
    Jean-Claude Zambrini. WSPC
  - 2002\. *Elementary probability theory: with stochastic processes and
    an introduction to mathematical finance*, 4th. with Farid Aitsahlia.
    Springer
  - 1979\. ''Elementary probability theory with stochastic processes ''.
    Springer-Verlag
  - 2002\. *Green, Brown, & Probability and Brownian Motion on the
    Line*. WSPC.
  - 2001\. *A course in probability theory*, 3rd. with Farid Aitsahlia.
    Academic Press
  - 2001\. *From Brownian motion to Schrödinger's Equation*. with
    Zhongxin Zhao. Springer
  - 1990\. *Introduction to stochastic integration*, 2nd. with Ruth J.
    Williams. Springer
  - 1982\. *Lectures from Markov Processes to Brownian Motion*. Springer
  - 1967\. *Markov chains with stationary transition probabilities*.
    Springer

<!-- end list -->

  - 1985\. *Seminar on Stochastic Processes*. Ed. with E. Cinlar and
    R.K. Getoor

<!-- end list -->

  - 2008\. *Selected works of Kai Lai Chung*. Ed. Farid Aitsahlia, Farid
    Aitsahlia, Elton Hsu, Ruth Williams. WSPC

## 参考资料

## 外部連結

  - [KAI LAI CHUNG -
    obituary](http://www.math.ucsd.edu/~williams/chung/obit.html)

[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:华人数学家](../Category/华人数学家.md "wikilink")
[Category:美国数学家](../Category/美国数学家.md "wikilink")
[Category:清華大學校友](../Category/清華大學校友.md "wikilink")
[Category:國立清華大學校友](../Category/國立清華大學校友.md "wikilink")
[Category:国立西南联合大学校友](../Category/国立西南联合大学校友.md "wikilink")
[Category:普林斯頓大學校友](../Category/普林斯頓大學校友.md "wikilink")
[Category:雪城大学教师](../Category/雪城大学教师.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")
[Category:美国科学作家](../Category/美国科学作家.md "wikilink")
[Category:中国科学作家](../Category/中国科学作家.md "wikilink")
[Category:庚子赔款留学生](../Category/庚子赔款留学生.md "wikilink")
[Category:杭州人](../Category/杭州人.md "wikilink")
[K](../Category/鍾姓.md "wikilink")

1.
2.
3.
4.  杨舰，戴吾三主编：《清华大学与中国近现代科技》（清华大学出版社2006），頁288。
5.
6.
7.
8.
9.
10.