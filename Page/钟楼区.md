**钟楼区**是[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[常州市所辖的一个](../Page/常州市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。位處[常州市区西部](../Page/常州市.md "wikilink"),四周與[天宁区](../Page/天宁区.md "wikilink")、[武进区](../Page/武进区.md "wikilink")、[新北区接壤相連](../Page/新北区.md "wikilink")。总面积为72平方公里，2010年人口为50万。

## 行政区划

钟楼区辖9个街道：五星、永红、西林、北港、荷花池、怀德路、南大街、西新桥、马公桥，2个镇：[新闸镇](../Page/新闸镇.md "wikilink")、[邹区镇](../Page/邹区镇.md "wikilink")。

## 名勝古跡

荆川公园、青枫公园、瞿秋白纪念馆

## 特產

常州[梳篦](../Page/梳篦.md "wikilink")、[乱针绣](../Page/乱针绣.md "wikilink")

## 外部链接

  - [常州市钟楼区政府网站](http://www.czzl.gov.cn/)

## 参考文献

[钟楼区](../Category/钟楼区.md "wikilink") [区](../Category/常州区市.md "wikilink")
[常州](../Category/江苏市辖区.md "wikilink")