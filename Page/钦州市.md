**钦州市**（[邮政式拼音](../Page/邮政式拼音.md "wikilink")：、）位于[廣東西端](../Page/廣東.md "wikilink")，广西南部，[北部湾沿岸](../Page/北部湾.md "wikilink")。西界[防城港市](../Page/防城港市.md "wikilink")，北接[南宁市](../Page/南宁市.md "wikilink")，东邻[玉林市](../Page/玉林市.md "wikilink")，南临[北海市及](../Page/北海市.md "wikilink")[北部湾](../Page/北部湾.md "wikilink")。前身是廣東下四府八鄉之一的[欽縣](../Page/钦县.md "wikilink")。1965年後被當局劃[廣西管轄](../Page/廣西.md "wikilink")，現時是[广西壮族自治区管辖的](../Page/广西壮族自治区.md "wikilink")[地级市](../Page/地级市.md "wikilink")，地处桂南台地[丘陵区](../Page/丘陵.md "wikilink")，北有罗阳山，东北为[六万大山](../Page/六万大山.md "wikilink")，西北为[十万大山](../Page/十万大山.md "wikilink")。[钦江自东北向西南流经市区后出北部湾](../Page/钦江.md "wikilink")。总面积1.09万平方公里，人口320.93万。钦州为区内重要[交通枢纽](../Page/交通枢纽.md "wikilink")，是[中国—东盟自由贸易区前沿城市](../Page/中国—东盟自由贸易区.md "wikilink")，[南防铁路](../Page/南防铁路.md "wikilink")、[钦北铁路和](../Page/钦北铁路.md "wikilink")[钦防铁路于市区交汇](../Page/钦防铁路.md "wikilink")，[钦州港为北部湾重要](../Page/钦州港.md "wikilink")[港口](../Page/港口.md "wikilink")。

## 历史

钦州古称**古越**、**安州**，有1400多年的历史。[先秦时期](../Page/先秦.md "wikilink")，两广地区为[百越民族所居住](../Page/百越.md "wikilink")。[秦始皇最早在全国范围内推行](../Page/秦始皇.md "wikilink")[郡县制](../Page/郡县制.md "wikilink")，统一[岭南地区](../Page/岭南.md "wikilink")（今两广）后设立[象郡](../Page/象郡.md "wikilink")，并将现在的钦州一带划归象郡管辖。[汉朝时期开始设立](../Page/汉朝.md "wikilink")“[州](../Page/州.md "wikilink")”的建制，两广一带归[交州管辖](../Page/交州.md "wikilink")，今钦州一带归交州[合浦郡管辖](../Page/合浦郡.md "wikilink")。[三国时期](../Page/三国.md "wikilink")，交州是[吴国辖地](../Page/吴_\(三国\).md "wikilink")，一直到[晋朝](../Page/晋朝.md "wikilink")，今钦州都属于[合浦郡管辖](../Page/合浦郡.md "wikilink")。[南北朝时](../Page/南北朝.md "wikilink")，[宋](../Page/刘宋.md "wikilink")[元嘉年间设](../Page/元嘉.md "wikilink")[宋寿郡](../Page/宋寿郡.md "wikilink")，[南齐时为](../Page/南齐.md "wikilink")[宋寿县地](../Page/宋寿县.md "wikilink")，属[交州管辖](../Page/交州.md "wikilink")；[梁](../Page/梁朝.md "wikilink")、[陈时成为](../Page/陈朝.md "wikilink")[安京郡地](../Page/安京郡.md "wikilink")，属[安州管辖](../Page/安州.md "wikilink")。

[隋](../Page/隋朝.md "wikilink")[开皇九年](../Page/开皇.md "wikilink")（589年）废宋寿郡，安京郡改置[安京县](../Page/安京县.md "wikilink")。开皇十八年（598年）将安州改为钦州，取“钦顺”之义，亦为“取钦江为名”（《[元和郡县志](../Page/元和郡县志.md "wikilink")》），这是钦州的最早得名，治所为[钦江县](../Page/钦江县.md "wikilink")（今钦州市东北）。[大业初又改置为](../Page/大业.md "wikilink")[宁越郡](../Page/宁越郡.md "wikilink")。[唐时推行](../Page/唐朝.md "wikilink")[道的建制](../Page/道.md "wikilink")，今两广地区设立[岭南道](../Page/岭南道.md "wikilink")。[武德四年](../Page/武德.md "wikilink")（621年）撤宁越郡改置钦州。[至德二年](../Page/至德.md "wikilink")（757年）改安京县为[保京县](../Page/保京县.md "wikilink")，辖今广西钦州、[灵山](../Page/灵山.md "wikilink")（今灵山县西），后数次更名。[五代十国时期](../Page/五代十国.md "wikilink")，两广地区为[南汉国辖地](../Page/南汉.md "wikilink")。[宋朝统一中国后推行](../Page/宋朝.md "wikilink")[路的建制](../Page/路.md "wikilink")，今广西、海南等地归[广南西路管辖](../Page/广南西路.md "wikilink")，同时今钦州一带仍设置钦州。[北宋初保京县复为安京县](../Page/北宋.md "wikilink")，[开宝五年](../Page/开宝.md "wikilink")（972年）废钦江县，[景德三年](../Page/景德.md "wikilink")（1006年）改安京县为[安远县](../Page/安远县.md "wikilink")。[南宋移治安远](../Page/南宋.md "wikilink")（今钦州）。

[元朝时始设](../Page/元朝.md "wikilink")[行省](../Page/行省.md "wikilink")，钦州一带归[湖广行省管辖](../Page/湖广行省.md "wikilink")，同时将钦州改为[钦州路](../Page/钦州路.md "wikilink")。[明朝时钦州划归](../Page/明朝.md "wikilink")[广东布政使司管辖](../Page/广东布政使司.md "wikilink")，[洪武二年](../Page/洪武二年.md "wikilink")（1369年）为[钦州府](../Page/钦州府.md "wikilink")，[洪武七年](../Page/洪武.md "wikilink")（1374年）降为钦州，废[安远县并入州](../Page/安远县.md "wikilink")；洪武十四年（1381年）属[廉州府](../Page/廉州府.md "wikilink")。[清初时钦州继续归广东管辖](../Page/清朝.md "wikilink")，同时钦州不设县。[光绪十四年](../Page/光绪十四年.md "wikilink")（1888年）划出钦州西部，设置[防城县](../Page/防城县.md "wikilink")。[光绪中钦州升为](../Page/光绪.md "wikilink")[直隶州](../Page/直隶州.md "wikilink")，辖今钦州、防城两地，仍隶属[广东省](../Page/广东省.md "wikilink")。1912年改州为县，1914年属[钦廉道](../Page/钦廉道.md "wikilink")。

[民国时期至共和国建立初期](../Page/民国.md "wikilink")，钦州一带仍属广东省辖。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，属广东省[南路专区](../Page/南路专区.md "wikilink")，1950年改[钦廉专区](../Page/钦廉专区.md "wikilink")，1951年改称[钦州专区](../Page/钦州专区.md "wikilink")。1952年钦州专区划归[广西省](../Page/广西省.md "wikilink")。1955年改为[合浦专区](../Page/合浦专区.md "wikilink")，并复归广东省。1959年改属[湛江专区](../Page/湛江专区.md "wikilink")。1964年，设立钦州[僮族](../Page/僮族.md "wikilink")（[壮族的旧称](../Page/壮族.md "wikilink")）自治县（1963年批准）。1965年，钦州、防城港一带划归广西壮族自治区，设立钦州专区，钦州壮族自治县改为[钦州县](../Page/钦州县.md "wikilink")。1971年改称[钦州地区](../Page/钦州地区.md "wikilink")。1983年划出[北海市](../Page/北海市.md "wikilink")。1987年将合浦县划归北海市。1993年划出[防城](../Page/防城.md "wikilink")、[上思两县](../Page/上思.md "wikilink")。1994年6月28日，国务院批准，撤销钦州地区，改设[地级钦州市](../Page/地级.md "wikilink")。

## 地理

北与[南宁](../Page/南宁.md "wikilink")，西与[防城港](../Page/防城港.md "wikilink")，东与[玉林](../Page/玉林.md "wikilink")，东北与[贵港](../Page/贵港.md "wikilink")，东南与[北海相邻](../Page/北海市.md "wikilink")。位于北纬20°54′至22°41′，东经107°27′至109°56′之间。其陆地海岸线长311.44公里，东北和西北分别有[六万大山和](../Page/六万大山.md "wikilink")[十万大山](../Page/十万大山.md "wikilink")，海拔均过千米。

### 气候

钦州属南[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，气候温暖，降水充沛。冬季短而温和，夏季长而闷热。1月平均气温13.6℃，极端最低气温-1.8℃（1955年1月12日）。7月平均气温28.4℃，极端最高气温37.9℃（2005年7月19日）。年平均气温22.2℃。降雨集中在5月\~8月，降水量占全年的三分之二。年均降水量2150.4毫米。年均日照时数1721.1小时。冬春季节日照较少，夏秋则较为晴朗。\[1\]

## 政治

### 现任领导

<table>
<caption>钦州市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党钦州市委员会.md" title="wikilink">中国共产党<br />
钦州市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/钦州市人民代表大会.md" title="wikilink">钦州市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/钦州市人民政府.md" title="wikilink">钦州市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议钦州市委员会.md" title="wikilink">中国人民政治协商会议<br />
钦州市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/王革冰.md" title="wikilink">王革冰</a>（女）[2]</p></td>
<td><p><a href="../Page/谭丕创.md" title="wikilink">谭丕创</a>[3]</p></td>
<td><p><a href="../Page/黄若萍.md" title="wikilink">黄若萍</a>（女）[4]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p><a href="../Page/壮族.md" title="wikilink">壮族</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河南省.md" title="wikilink">河南省</a><a href="../Page/唐河县.md" title="wikilink">唐河县</a></p></td>
<td><p>广西壮族自治区<a href="../Page/贵港市.md" title="wikilink">贵港市</a></p></td>
<td><p>广西壮族自治区<a href="../Page/大化县.md" title="wikilink">大化县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年4月</p></td>
<td><p>2017年5月</p></td>
<td><p>2018年2月</p></td>
<td><p>2015年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

钦州市下辖2个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[钦南区](../Page/钦南区.md "wikilink")、[钦北区](../Page/钦北区.md "wikilink")
  - 县：[灵山县](../Page/灵山县.md "wikilink")、[浦北县](../Page/浦北县.md "wikilink")

钦州市还设立以下管理区：[钦州港经济技术开发区](../Page/钦州港经济技术开发区.md "wikilink")（[国家级](../Page/国家级经济技术开发区.md "wikilink")）、[三娘湾旅游管理区](../Page/三娘湾旅游管理区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>钦州市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>450700</p></td>
</tr>
<tr class="odd">
<td><p>450702</p></td>
</tr>
<tr class="even">
<td><p>450703</p></td>
</tr>
<tr class="odd">
<td><p>450721</p></td>
</tr>
<tr class="even">
<td><p>450722</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>钦州市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>钦州市</p></td>
<td><p>3079721</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>钦南区</p></td>
<td><p>617047</p></td>
<td><p>20.04</p></td>
</tr>
<tr class="even">
<td><p>钦北区</p></td>
<td><p>581381</p></td>
<td><p>18.88</p></td>
</tr>
<tr class="odd">
<td><p>灵山县</p></td>
<td><p>1152674</p></td>
<td><p>37.43</p></td>
</tr>
<tr class="even">
<td><p>浦北县</p></td>
<td><p>728619</p></td>
<td><p>23.66</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")307.97万人\[8\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加10.49万人，增加3.53%。年平均增加0.35%。其中，男性人口为163.83万人，占53.20%；女性人口为144.14万人，占46.80%。人口性别比（以女性为100）为113.66。0－14岁人口为87.52万人，占28.42%；15－59岁人口为182.09万人，占59.13%；60岁以上人口为38.36万人，占12.46%；其中，65岁及以上人口为27.57万人，占8.95%。

### 民族

全市常住人口中，汉族人口为275.46万人，占89.44%；各[少数民族人口为](../Page/少数民族.md "wikilink")32.51万人，占10.56%。

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [壮族](../Page/壮族.md "wikilink") | [瑶族](../Page/瑶族.md "wikilink") | [苗族](../Page/苗族.md "wikilink") | [侗族](../Page/侗族.md "wikilink") | [京族](../Page/京族.md "wikilink") | [布依族](../Page/布依族.md "wikilink") | [彝族](../Page/彝族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | [回族](../Page/回族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | -------------------------------- | ------------------------------ | -------------------------------- | ------------------------------ | ---- |
| 人口数          | 2754644                        | 314413                         | 4343                           | 2293                           | 681                            | 523                            | 504                              | 298                            | 272                              | 256                            | 1494 |
| 占总人口比例（%）    | 89.44                          | 10.21                          | 0.14                           | 0.07                           | 0.02                           | 0.02                           | 0.02                             | 0.01                           | 0.01                             | 0.01                           | 0.05 |
| 占少数民族人口比例（%） | \---                           | 96.72                          | 1.34                           | 0.71                           | 0.21                           | 0.16                           | 0.16                             | 0.09                           | 0.08                             | 0.08                           | 0.46 |

**钦州市民族构成（2010年11月）**\[9\]

## 物产

  - [北部湾大陆架蕴藏总储量约](../Page/北部湾.md "wikilink")5亿吨的石油和丰富的天然气。有总储量约1930万吨的石英砂、储量约30亿吨的陶土。
  - 大垌煤矿
  - 农作物有水稻、花生、香蕉、沙柑、扁柑、沙梨、甘蔗、火龙果、荔枝、龙眼、百香果和各类蔬菜等。
  - 钦州所濒临的北部湾是中国沿海四大渔场之一。主要鱼类500多种，虾类43种，另有蟹类、贝类、头足类、爬行类及哺乳类等种类繁多的海生动物和藻类等多种植物，海特产有珍珠、海参、文蛤、沙虫
    、鱿鱼、地鱼 、红鱼、鳝肚、对虾、大蚝、海蛰等。

## 经济

  - 工业有化肥、机械、制糖、农机、采矿、生物科技、药材加工以及临海石油工业。
  - 农业以渔业、养殖业和水稻种植业为主。
  - 并积极发展对外贸易。

## 交通運輸

  - 鐵路：[南防鐵路](../Page/南防鐵路.md "wikilink")、[欽北鐵路](../Page/欽北鐵路.md "wikilink")、[黎欽鐵路](../Page/黎欽鐵路.md "wikilink")、[邕北線](../Page/邕北線.md "wikilink")（[南欽鐵路](../Page/南欽鐵路.md "wikilink")、[钦北高速鐵路](../Page/钦北高速鐵路.md "wikilink")）、[欽防鐵路](../Page/欽防鐵路.md "wikilink")

## 文化

### 语言

钦州为[粤語区](../Page/粤語.md "wikilink")，俗稱[白话](../Page/白话.md "wikilink")，通行[钦廉片粤语](../Page/w:en:Qin-Lian_Yue.md "wikilink")（又謂**欽州話**），与[省港澳通行之](../Page/省港澳.md "wikilink")[廣州話相通](../Page/廣州話.md "wikilink")。辖区内同时并存[客家话](../Page/客家话.md "wikilink")、[平话以及](../Page/广西平话.md "wikilink")[壮语](../Page/壮语.md "wikilink")。

### 名人

钦州近代名人有清末著名军事家，[黑旗军领袖](../Page/黑旗军.md "wikilink")[刘永福](../Page/刘永福.md "wikilink")、[冯子材](../Page/冯子材.md "wikilink")，北宋诗人[苏东坡](../Page/苏东坡.md "wikilink")、清末民初著名画家[齐白石和现代诗人](../Page/齐白石.md "wikilink")[田汉都到过钦州](../Page/田汉.md "wikilink")，并有佳作为证。

### 特色小吃

  - [钦州猪脚粉](../Page/钦州猪脚粉.md "wikilink")
  - [桂林米粉](../Page/桂林米粉.md "wikilink")
  - [老友粉](../Page/老友粉.md "wikilink")
  - [灵山烧鸭粉](../Page/灵山烧鸭粉.md "wikilink")
  - [灵山大粽](../Page/灵山大粽.md "wikilink")
  - [灵山刮粉](../Page/灵山刮粉.md "wikilink")
  - [猪脚粉](../Page/猪脚粉.md "wikilink")
  - [白薯饐](../Page/白薯饐.md "wikilink")
  - [垃圾饐](../Page/垃圾饐.md "wikilink")
  - [黄瓜皮](../Page/黄瓜皮.md "wikilink")
  - [叉烧粉](../Page/叉烧粉.md "wikilink")
  - [浦北云吞面](../Page/浦北云吞面.md "wikilink")
  - [小董麻通](../Page/小董麻通.md "wikilink")
  - [小董狗肉](../Page/小董狗肉.md "wikilink")
  - [沙坪芝麻饼](../Page/沙坪芝麻饼.md "wikilink")
  - [武利牛巴](../Page/武利牛巴.md "wikilink")

## 风景旅游

钦州名胜古迹众多，是桂南的旅游胜地，

  - [三娘湾](../Page/三娘湾.md "wikilink")
  - [麻兰岛](../Page/麻兰岛.md "wikilink")
  - [五皇岭](../Page/五皇岭.md "wikilink")
  - [八寨沟](../Page/八寨沟.md "wikilink")
  - [六峰山](../Page/六峰山.md "wikilink")
  - [越州天湖](../Page/越州天湖.md "wikilink")
  - [刘永福故居](../Page/刘永福故居.md "wikilink")
  - [冯子材故居](../Page/冯子材故居.md "wikilink")
  - [大芦村](../Page/大芦村.md "wikilink")
  - 七十二泾

## 教育

### 高等院校

  - 公办本科院校：[北部湾大学](../Page/北部湾大学.md "wikilink")
  - 民办专科院校：[广西英华国际职业学院](../Page/广西英华国际职业学院.md "wikilink")

### 重点中学

  - [钦州市第二中学](../Page/钦州市第二中学.md "wikilink")
  - [灵山县灵山中学](../Page/灵山县灵山中学.md "wikilink")
  - [钦州市第一中学](../Page/钦州市第一中学.md "wikilink")
  - [浦北中学](../Page/浦北中学.md "wikilink")
  - [钦州市第三中学](../Page/钦州市第三中学.md "wikilink")
  - [灵山县实验中学](../Page/灵山县实验中学.md "wikilink")
  - [钦北区长滩中学](../Page/钦北区长滩中学.md "wikilink")
  - [钦州市外国语学校](../Page/钦州市外国语学校.md "wikilink")（钦州市第二中学初中部）
  - [钦州市第一中学初中部](../Page/钦州市第一中学初中部.md "wikilink")
  - [灵山外国语学校](../Page/灵山外国语学校.md "wikilink")（灵山中学初中部）
  - [浦北二中](../Page/浦北二中.md "wikilink")
  - [钦州市第七中学](../Page/钦州市第七中学.md "wikilink")

## 注释

## 参考文献

### 引用

### 来源

  - 《钦州市志》
  - 《钦州坭兴陶》
  - 《北部湾战略》

## 外部链接

  - [钦州市人民政府](http://www.qinzhou.gov.cn)
  - [钦州市环保局](http://www.qzhb.gov.cn/)

## 参见

  - [钦州 (古代)](../Page/钦州_\(古代\).md "wikilink")

{{-}}

[Category:广西地级市](../Category/广西地级市.md "wikilink")
[钦州](../Category/钦州.md "wikilink")
[Category:南中國海沿海城市](../Category/南中國海沿海城市.md "wikilink")
[Category:北部灣](../Category/北部灣.md "wikilink")
[Category:1994年建立的行政區劃](../Category/1994年建立的行政區劃.md "wikilink")

1.  [中国气象科学数据共享服务网](http://cdc.cma.gov.cn/home.do)
2.
3.
4.
5.
6.
7.
8.
9.