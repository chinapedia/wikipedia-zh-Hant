**钯**是化学[元素](../Page/元素.md "wikilink")，[化学符号為](../Page/化学符号.md "wikilink")**Pd**，[原子序数](../Page/原子序数.md "wikilink")46。鈀的拉丁名稱palladium是以[小行星](../Page/小行星.md "wikilink")[智神星來命名的](../Page/智神星.md "wikilink")，另一種以小行星來命名的元素是[鈰](../Page/鈰.md "wikilink")。

鈀是罕見的、有光澤的銀白色[金屬](../Page/金屬.md "wikilink")，與[鉑](../Page/鉑.md "wikilink")、[銠](../Page/銠.md "wikilink")、[釕](../Page/釕.md "wikilink")、[銥](../Page/銥.md "wikilink")、[鋨形成一組](../Page/鋨.md "wikilink")[鉑族金屬的元素家族](../Page/鉑族金屬.md "wikilink")。鉑族金屬化學性質相似，但鈀的[熔點最低](../Page/熔點.md "wikilink")，是這些[貴金屬中](../Page/貴金屬.md "wikilink")[密度最低的一种](../Page/密度.md "wikilink")。

在[实验室裡](../Page/实验室.md "wikilink")，经常把[一氧化碳通入稀](../Page/一氧化碳.md "wikilink")[氯化钯](../Page/氯化钯.md "wikilink")[溶液中来制取钯](../Page/溶液.md "wikilink")：\(\mathrm{PdCl}_{2} + \mathrm{CO} +\mathrm{H}_{2}\mathrm{O} = \mathrm{Pd}\downarrow + \mathrm{CO}_{2}+ \mathrm{2HCl\,(aq)}\)

[钯是](../Page/钯.md "wikilink")[过渡金属](../Page/过渡金属.md "wikilink")，性质像[铂](../Page/铂.md "wikilink")，可在[铜及](../Page/铜.md "wikilink")[镍的矿石中提煉](../Page/镍.md "wikilink")，主要用作工业上的[催化剂](../Page/催化剂.md "wikilink")（[钯催化偶联反应](../Page/钯催化偶联反应.md "wikilink")）、[內燃機](../Page/內燃機.md "wikilink")[車輛的](../Page/車輛.md "wikilink")[觸媒轉換器及](../Page/觸媒轉換器.md "wikilink")[白金首飾](../Page/白金_\(合金\).md "wikilink")，另外，它可以吸收比自身體積大900倍的[氫氣](../Page/氫氣.md "wikilink")。

## 物理性质

钯是银白色具有延展性的金属，对[氢有巨大的](../Page/氢.md "wikilink")[亲和力](../Page/亲和力.md "wikilink")，比其他任何金属都能吸收更多的氢，\[1\]在室温和1大气压下所吸取的氢可达钯本身体积的800多倍。利用鈀銀或鈀合金能夠將化學重組反應的氫氣分離出來供應至燃料電池使用。

## 化学性质

钯比其他[铂系金属更容易被氧化](../Page/铂系金属.md "wikilink")。[硝酸能溶解钯](../Page/硝酸.md "wikilink")。在炽热的温度下，钯能和[氟](../Page/氟.md "wikilink")、[氯反应](../Page/氯.md "wikilink")；而在空气中将钯加热到暗红色，可以生成一层紫色的[氧化膜](../Page/氧化.md "wikilink")，[铂却无此性质](../Page/铂.md "wikilink")。低温下，惰性介质中，钯可以和[一氧化碳反应](../Page/一氧化碳.md "wikilink")，生成Pd(CO)<sub>n</sub>(n=1\~4)。\[2\]

## 化合物

钯化合物通常以0价和+2价两种价态存在，其它价态的化合物也是已知的。总体上来说，与其它元素相比，钯化合物的性质和铂更接近。

## 应用

[Aufgeschnittener_Metall_Katalysator_für_ein_Auto.jpg](https://zh.wikipedia.org/wiki/File:Aufgeschnittener_Metall_Katalysator_für_ein_Auto.jpg "fig:Aufgeschnittener_Metall_Katalysator_für_ein_Auto.jpg")
[25_rubles_palladium_1989_Ivan_III.jpg](https://zh.wikipedia.org/wiki/File:25_rubles_palladium_1989_Ivan_III.jpg "fig:25_rubles_palladium_1989_Ivan_III.jpg")
钯在现代最大的用途是[催化转换器](../Page/催化转换器.md "wikilink")\[3\]，也能用作珠宝、[牙科材料](../Page/牙科.md "wikilink")、\[4\]\[5\]手表配件、血糖试纸、飞机[火花塞](../Page/火花塞.md "wikilink")、手术器械和电接触点，\[6\]或者用于制作专业的横向长笛。\[7\]；也能用作商品，如钯金的[ISO货币代码是XPD及](../Page/ISO_4217.md "wikilink")964，其余有此类代码的金属还有[金](../Page/金.md "wikilink")、[银和](../Page/银.md "wikilink")[铂](../Page/铂.md "wikilink")。\[8\]钯能够吸收氢气，因此它也是1989年开始的有争议的[冷聚变实验的关键组分](../Page/冷聚变.md "wikilink")。

诺里尔斯克镍公司（Norilsk Nickel）是俄罗斯最大的，同时也是世界最大的有色金属和贵金属生产商之一，其生产的钯占全球产量的66%。

### 催化

钯均匀负载时，如得到[钯碳催化剂](../Page/钯碳催化剂.md "wikilink")，可以用作多功能的[催化剂](../Page/催化剂.md "wikilink")，它可以加速[氢化](../Page/氢化.md "wikilink")、[脱氢以及](../Page/脱氢.md "wikilink")[裂化反应](../Page/裂化反应.md "wikilink")。有机化学中大部分[碳－碳键的偶合反应都借助于钯化合物催化剂来完成](../Page/碳－碳键.md "wikilink")，如[赫克反应和](../Page/赫克反应.md "wikilink")[铃木反应](../Page/铃木反应.md "wikilink")。

当钯分散到导电材料上时，可以得到优良的电催化剂，用于催化在碱性介质氧化伯醇。\[9\]在2010年，钯催化的有机反应获得了[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。另外，钯可用于均相催化，和多种[配体结合](../Page/配体.md "wikilink")，完成高选择性的化学转换，如钯催化剂用于催化的C−F键的反应。\[10\]

钯也是[林德拉催化剂的重要成分](../Page/林德拉催化剂.md "wikilink")。\[11\]
[Kumada_Catalytic_Cycle.png](https://zh.wikipedia.org/wiki/File:Kumada_Catalytic_Cycle.png "fig:Kumada_Catalytic_Cycle.png")

钯催化剂主要用于有机化学以及工业应用，它在[合成生物学上也是有较好的前景](../Page/合成生物学.md "wikilink")。在2017年，钯[纳米颗粒在](../Page/纳米颗粒.md "wikilink")[哺乳动物体内被证实了有治疗疾病的催化活性](../Page/哺乳动物.md "wikilink")。\[12\]

### 电子学

钯在电子学中的第二大应用便是用于[电容器中](../Page/电容器.md "wikilink")，\[13\]其中钯以及钯银合金用作电极。\[14\]钯（或钯镍合金）可以用作消费电子产品中的连接部分、配件组分或焊接材料。\[15\]\[16\]根据2006年[莊信萬豐的报告](../Page/莊信萬豐.md "wikilink")，电子行业在当年耗用了107万[金衡盎司](../Page/金衡制.md "wikilink")（合33.2吨）的金属钯。\[17\]

### 储氢

钯在室温可以很容易地吸收氢气，形成氢化钯PdH<sub>x</sub>（x＜1）。\[18\]虽然很多过渡金属也有这种性质，但钯可以高效地吸收氢气，并且在x接近1之前不会失去延展性。\[19\]钯的这一性质被用于高效、相对廉价且安全的氢储存设备的研究中，但钯本身的价格昂贵是必须要考虑的因素。\[20\]钯中的氢含量影响着[磁化率](../Page/磁化率.md "wikilink")，随着氢含量的增高，磁化率降低，并且在形成PdH<sub>0.62</sub>时变为零。在任意更高的比例下，固溶体变为具有[抗磁性](../Page/抗磁性.md "wikilink")。\[21\]

### 牙医材料

钯在一些有少量使用（约0.5%），它可用于减少腐蚀并增加最终修复体的金属光泽。\[22\]

## 注释

## 参考文献

{{-}}

[Category:过渡金属](../Category/过渡金属.md "wikilink")
[钯](../Category/钯.md "wikilink")
[5J](../Category/第5周期元素.md "wikilink")
[5J](../Category/化学元素.md "wikilink")

1.  《无机化学》丛书.第九卷.*28.6* 钯.*6.1* 钯的化学概要(P<sub>545</sub>)

2.  《无机化学》丛书第九卷.*28.6* 钯.*6.3.1* 羰基化合物(P<sub>552</sub>)

3.

4.
5.

6.

7.

8.

9.

10.

11.

12.

13.

14.
15.

16.

17.

18.

19.

20.

21. Mott, N. F. and Jones, H. (1958) *The Theory of Properties of metals
    and alloys*. Oxford University Press. . p. 200

22.