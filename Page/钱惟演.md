**钱惟演**（），[字希圣](../Page/表字.md "wikilink")，钱塘（今[浙江](../Page/浙江.md "wikilink")[杭州](../Page/杭州.md "wikilink")）人，[中国](../Page/中国.md "wikilink")[北宋政治家](../Page/北宋.md "wikilink")、诗人，[西崑体领袖](../Page/西崑体.md "wikilink")，[吴越末代国王](../Page/吴越.md "wikilink")[钱俶第十四子](../Page/钱俶.md "wikilink")，[劉娥之義兄](../Page/劉娥.md "wikilink")[劉美的](../Page/劉美.md "wikilink")[妻舅](../Page/妻舅.md "wikilink")。有三子[钱暧](../Page/钱暧.md "wikilink")、[钱晦](../Page/钱晦.md "wikilink")、[钱暄](../Page/钱暄.md "wikilink")。另外，[盛度為其](../Page/盛度.md "wikilink")[女婿](../Page/女婿.md "wikilink")。

## 生平

[宋太宗](../Page/宋太宗.md "wikilink")[太平兴国三年](../Page/太平兴国.md "wikilink")（978年），随父归宋，封[团练使](../Page/团练使.md "wikilink")，累遷左神武將軍\[1\]。[宋真宗时](../Page/宋真宗.md "wikilink")，改授文职，召試學士院，直秘閣，預修《[冊府元龜](../Page/冊府元龜.md "wikilink")》，历任[知制诰](../Page/知制诰.md "wikilink")、給事中。大中祥符八年（1015年），為[翰林学士](../Page/翰林学士.md "wikilink")，天禧四年（1020年）爲[枢密使](../Page/枢密使.md "wikilink")、仁宗即位，爲樞密使，不久罷知河陽。天聖三年（1025年），[同中书门下平章事判許州](../Page/同中书门下平章事.md "wikilink")\[2\]、八年，判陳州，明道二年（1033年）以泰寧軍節度使判河南府\[3\]，又曾任[洛阳](../Page/洛阳.md "wikilink")[留后](../Page/留后.md "wikilink")，仿唐驛馬傳送[荔枝給](../Page/荔枝.md "wikilink")[楊貴妃事](../Page/楊貴妃.md "wikilink")，傳送洛陽[牡丹品種](../Page/牡丹.md "wikilink")「姚黃」供[內廷玩賞](../Page/內廷.md "wikilink")，人称**洛阳使相**。[蘇軾曾寫](../Page/蘇軾.md "wikilink")《荔枝嘆》諷刺之。歐陽修《[歸田錄](../Page/歸田錄.md "wikilink")》記載，錢惟演家藏一座珊瑚筆格，常置於几案上欣賞，一日遭竊，惟演不惜以一萬錢將之贖回\[4\]。

[宋仁宗即位后](../Page/宋仁宗.md "wikilink")，太后[刘娥佐政](../Page/刘娥.md "wikilink")，钱惟演受太后宠信，為人趨炎附勢，以裙帶關係依附皇族。亦好招徠名士，獎掖後進，[歐陽修即受到他的敬重](../Page/歐陽修.md "wikilink")。好讀書，“坐則讀經史，臥則讀小說，上廁則閱小詞，蓋未嘗頃刻釋卷也”\[5\]，其詩宗[李商隱](../Page/李商隱.md "wikilink")，見於《西崑酬唱集》，內容略顯貧乏。

仁宗亲政後，罷黜[皇后](../Page/皇后.md "wikilink")[郭清悟](../Page/郭清悟.md "wikilink")，惟演又與郭皇后有姻親關係，即被贬，谪居汉东，景祐元年（1034年）卒，[谥](../Page/谥号.md "wikilink")**思**，改諡**文僖**。著有《典懿集》、《樞庭擁旄》前後集、《伊川漢上集》等\[6\]，皆佚。《[宋朝事實類苑](../Page/宋朝事實類苑.md "wikilink")》有輯錄。

## 注釋

<div class="references-small">

<references />

</div>

[Q钱](../Category/杭州人.md "wikilink")
[Q钱](../Category/宋朝政治人物.md "wikilink")
[Q钱](../Category/中国诗人.md "wikilink")
[W](../Category/钱姓.md "wikilink")
[Category:諡文僖](../Category/諡文僖.md "wikilink")
[Category:吳越國王室](../Category/吳越國王室.md "wikilink")

1.  《東都事略》卷二四本傳、《隆平集》卷一二
2.  《續資治通鑑長編》卷一○三
3.  《續資治通鑑長編》卷一○九
4.  欧阳修對錢頗有好感，《归田录》卷一一稱“钱思公生长富贵，而性俭约，闺门用度，为法甚谨”。卷二亦稱“钱思公生长富贵，而少所嗜好”，“钱思公官简将相，阶、勋、品皆第一”。
5.  歐陽修《歸田錄》
6.  《東都事略》、《隆平集》