**钱树根**（），[江苏](../Page/江苏.md "wikilink")[无锡石塘湾天授村人](../Page/无锡.md "wikilink")。[中国人民解放军上将](../Page/中国人民解放军上将.md "wikilink")。

## 生平

1985年12月至1987年6月，钱树根在担任某集团军军长期间，曾率部参加[云南](../Page/云南.md "wikilink")[两山战役等边境防御战争](../Page/两山战役.md "wikilink")，战功突出。1988年9月，被授予少将军衔。1992年11月，任[兰州军区参谋长](../Page/兰州军区.md "wikilink")。1994年12月后，任[中国人民解放军总参谋长助理](../Page/中国人民解放军.md "wikilink")、副总参谋长，晋升为中将，2000年6月被授予上将军衔\[1\]。

中共第十三屆、十四屆中央候補委員，第十五屆、十六屆中央委員。

## 參考文献

{{-}}

[Category:无锡人](../Category/无锡人.md "wikilink")
[S](../Category/錢姓.md "wikilink")
[Category:中国人民解放军上将](../Category/中国人民解放军上将.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十五届中央委员会委员](../Category/中国共产党第十五届中央委员会委员.md "wikilink")
[Category:中国共产党第十四届中央委员会候补委员](../Category/中国共产党第十四届中央委员会候补委员.md "wikilink")
[Category:中国人民解放军陆军第四十七集团军军长](../Category/中国人民解放军陆军第四十七集团军军长.md "wikilink")
[Category:中国人民解放军陆军第二十一集团军军长](../Category/中国人民解放军陆军第二十一集团军军长.md "wikilink")
[Category:兰州军区参谋长](../Category/兰州军区参谋长.md "wikilink")
[Category:中国人民解放军总参谋长助理](../Category/中国人民解放军总参谋长助理.md "wikilink")
[Category:中国人民解放军副总参谋长](../Category/中国人民解放军副总参谋长.md "wikilink")

1.