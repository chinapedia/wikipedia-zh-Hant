**钱玄同**（），原名**钱夏**，字**德潜**，号**疑古**，[浙江](../Page/浙江.md "wikilink")[吴兴](../Page/吴兴.md "wikilink")（现浙江[湖州市](../Page/湖州市.md "wikilink")）人。现代[文字学家](../Page/文字学.md "wikilink")，是[新文化运动的先驱者之一](../Page/新文化运动.md "wikilink")，曾主张废除[汉字](../Page/汉字.md "wikilink")。

## 生平

钱玄同的父亲是清朝同治年间进士[钱振常](../Page/钱振常.md "wikilink")。钱玄同早年赴[日本](../Page/日本.md "wikilink")[留学](../Page/留学.md "wikilink")，入[早稻田大学](../Page/早稻田大学.md "wikilink")。钱玄同在日本拜見[章太炎](../Page/章太炎.md "wikilink")（炳麟）於《民報》社，章太炎介紹他加入[中國同盟會](../Page/中國同盟會.md "wikilink")，他同時聽章太炎講[文學](../Page/文學.md "wikilink")、[音韻學](../Page/音韻學.md "wikilink")。钱玄同結識的許多章門弟子，後來都成了著名學者，包括[黄侃](../Page/黄侃.md "wikilink")、[鲁迅](../Page/鲁迅.md "wikilink")、[周作人](../Page/周作人.md "wikilink")。钱玄同回国后，1913年到[北京](../Page/北京.md "wikilink")，任国立北京高等师范学校及附属中学国文、经学教员。后又长期在国立[北京大学兼课](../Page/北京大学.md "wikilink")。1917年，钱玄同加入[中华民国国语研究会为会员](../Page/中华民国国语研究会.md "wikilink")，兼任[中华民国教育部](../Page/中华民国教育部.md "wikilink")[国语统一筹备会常驻干事](../Page/国语统一筹备会.md "wikilink")，致力[国语运动](../Page/国语运动.md "wikilink")。

钱玄同从1913年到[北京高等师范执教](../Page/北京高等师范.md "wikilink")，连续在北京师范大学任专任教授二十余年。他讲授的课程，以[音韵学为主](../Page/音韵学.md "wikilink")，还有“说文研究”、“经学史略”、“周至唐及清代思想概要”、“先秦古书真伪略说”等。并长期任北京师范大学国文系主任。1917年，他向[陈独秀主办的](../Page/陈独秀.md "wikilink")《[新青年](../Page/新青年.md "wikilink")》杂志投稿，倡导文学革命，成为“五四”新文化运动的揭幕人之一。1918年至1919年的《新青年》杂志，钱玄同是轮流编辑之一；在这期间，他曾动员[鲁迅给](../Page/鲁迅.md "wikilink")《新青年》写文章。鲁迅的小说《[狂人日记](../Page/狂人日记.md "wikilink")》就是钱玄同催促他写出的头一篇作品，并且头一次用“鲁迅”作笔名；众所周知，《狂人日记》不但是篇白话文，而且是攻击“吃人的礼教”的第一炮。钱玄同在教学和学术研究方面的贡献也是很显著的。他所著的《文字学音篇》是中国高等学校最早的音韵学教科书。数十年来，影响颇大，迄今仍为音韵学家所称引。

当代许多音韵学家如[罗常培](../Page/罗常培.md "wikilink")、[魏建功](../Page/魏建功.md "wikilink")、[白涤洲](../Page/白涤洲.md "wikilink")、[赵荫棠](../Page/赵荫棠.md "wikilink")、[王静如](../Page/王静如.md "wikilink")、[丁声树等](../Page/丁声树.md "wikilink")，或是錢玄同的学生，或受过錢玄同的教益。名史家[黄现璠曾是錢玄同的](../Page/黄现璠.md "wikilink")[研究生](../Page/研究生.md "wikilink")。钱玄同对于[经学创见甚多](../Page/经学.md "wikilink")，他有两句名言：“考古务求其真，致用务求其适。”他发表在《古史辨》上讨论上古历史和[儒家经书的文章](../Page/儒家.md "wikilink")，独见很多，影响很大。[郭沫若对钱玄同在古史研究方面的一些观点非常赞赏](../Page/郭沫若.md "wikilink")，说：“这些见解，与鄙见不期而同，但都是先我而发的。”他早年积极宣传汉语改用拼音文字，曾采用[国际音标制定](../Page/国际音标.md "wikilink")[汉语拼音字母](../Page/汉语拼音.md "wikilink")。后来他和[赵元任](../Page/赵元任.md "wikilink")、[黎锦熙等数人共同制定](../Page/黎锦熙.md "wikilink")“[国语罗马字拼音法式](../Page/国语罗马字.md "wikilink")”。1935年，他抱病坚持起草了《[第一批简体字表](../Page/第一批简体字表.md "wikilink")》，為[中國共產黨執政後推行](../Page/中國共產黨.md "wikilink")「[簡化字](../Page/簡化字.md "wikilink")」立下先例。

錢玄同曾有一句驚人之語：「四十歲以上的人都應該槍斃。」但是他自己過了四十歲還沒有死。到了他四十一歲生日的時候、[胡適便為他作了一首叫](../Page/胡適.md "wikilink")《亡友錢玄同先生成仁周年紀念歌》的六言語體歌。

## 家庭

### 父

[钱振常](../Page/钱振常.md "wikilink")，同治十年（1871年）进士，官吏部主事。

### 兄

  - [钱恂](../Page/钱恂.md "wikilink")，曾任清朝出使荷兰大臣和意大利大臣，主持湖北[自强学堂](../Page/自强学堂.md "wikilink")。

### 子女

  - [钱三强是著名](../Page/钱三强.md "wikilink")[物理学家](../Page/物理.md "wikilink")。

## 參考文獻

  - He Jiuying 何九盈 (1995). *Zhongguo xiandai yuyanxue shi* (中囯现代语言学史 "A
    history of modern Chinese linguistics"). Guangzhou: Guangdong jiaoyu
    chubanshe.
  - Wu Rui 吳銳 (1996). *Qian Xuantong pingzhuan* (钱玄同评传 "A Biography of
    Qian Xuantong"). Nanchang: Baihuazhou wenyi chubanshe.

## 外部連結

  -
[category:北京师范大学教授](../Page/category:北京师范大学教授.md "wikilink")

[Category:中國漢字學家](../Category/中國漢字學家.md "wikilink")
[Category:中華民國語言學家](../Category/中華民國語言學家.md "wikilink")
[Category:北京大學教授](../Category/北京大學教授.md "wikilink")
[Category:早稻田大學校友](../Category/早稻田大學校友.md "wikilink")
[Category:湖州人](../Category/湖州人.md "wikilink")
[X](../Category/钱姓.md "wikilink")
[Category:中國世界語者](../Category/中國世界語者.md "wikilink")
[Category:吴越钱氏](../Category/吴越钱氏.md "wikilink")
[Category:中華民國持不同政見者](../Category/中華民國持不同政見者.md "wikilink")
[Category:葬于北京市福田公墓](../Category/葬于北京市福田公墓.md "wikilink")
[Category:新文化运动人物](../Category/新文化运动人物.md "wikilink")