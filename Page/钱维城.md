[Yandang_Mountain.jpg](https://zh.wikipedia.org/wiki/File:Yandang_Mountain.jpg "fig:Yandang_Mountain.jpg")
**錢維城**（），[字](../Page/表字.md "wikilink")**宗盘**，[号](../Page/号.md "wikilink")**纫庵**。[江南](../Page/江南.md "wikilink")[武进](../Page/武进.md "wikilink")（今[江苏](../Page/江苏.md "wikilink")[常州](../Page/常州.md "wikilink")）人。

父钱人麟。生于[康熙五十九年](../Page/康熙.md "wikilink")（1720年），十岁能为诗。[乾隆十年](../Page/乾隆.md "wikilink")（1745年）[状元](../Page/状元.md "wikilink")。\[1\]授[翰林院](../Page/翰林院.md "wikilink")[修撰](../Page/修撰.md "wikilink")。官至[刑部侍郎](../Page/刑部侍郎.md "wikilink")，入值[南书房](../Page/南书房.md "wikilink")。擅长作画，得[董邦達真傳](../Page/董邦達.md "wikilink")，\[2\]有《雁蕩圖》傳世。与[张宗苍](../Page/张宗苍.md "wikilink")、[方薰等人供奉宫廷而称为画中十哲](../Page/方薰.md "wikilink")。《[石渠寶笈](../Page/石渠寶笈.md "wikilink")》收录其作品達160多幅，可知乾隆帝对钱维城作品的赏识。素有消渴疾，至是增剧，身體羸弱。\[3\]乾隆三十四年（1769），赴贵州，查审[威寧州知州刘标亏帑案](../Page/威寧州.md "wikilink")。参与镇压古州苗民起义。[乾隆三十七年](../Page/乾隆.md "wikilink")（1772年），正月以丁父憂歸鄉，不久亦卒，[谥](../Page/谥.md "wikilink")**文敏**。著有《茶山集》。\[4\]

## 家庭及關聯

  - 正室：金氏（1720－1782）。官至貴州[按察使吳縣](../Page/按察使.md "wikilink")[金祖靜之女](../Page/金祖靜.md "wikilink")。

有子女三人。

  - 女：[錢孟鈿](../Page/錢孟鈿.md "wikilink")（1739－1806），女詩人。適[乾隆二十六年進士官湖北](../Page/模板:乾隆二十六年辛巳恩科殿試金榜.md "wikilink")[荊宜施道](../Page/荊宜施道.md "wikilink")[崔龍見](../Page/崔龍見.md "wikilink")，[康熙五十七年進士官至](../Page/模板:康熙五十七年戊戌科殿試金榜.md "wikilink")[湖北巡撫](../Page/湖北巡撫.md "wikilink")[崔紀之子](../Page/崔紀.md "wikilink")。
  - 長子：[錢中銑](../Page/錢中銑.md "wikilink")（1744－1779），乾隆三十九年欽賜[舉人](../Page/舉人.md "wikilink")，官[內閣中書](../Page/內閣中書.md "wikilink")。娶莊氏莊環玦（1743－1785），[雍正五年進士官浙江](../Page/模板:雍正五年丁未科殿試金榜.md "wikilink")[海防道](../Page/海防道.md "wikilink")[莊柱孫女](../Page/莊柱.md "wikilink")、[乾隆十九年狀元官翰林院](../Page/模板:乾隆十九年甲戌科殿試金榜.md "wikilink")[侍講學士](../Page/侍講學士.md "wikilink")[莊培因之女](../Page/莊培因.md "wikilink")。
  - 次子：[錢中鈺](../Page/錢中鈺.md "wikilink")（1749－1779），監生，官[中書科](../Page/中書科.md "wikilink")[中書](../Page/中書.md "wikilink")。娶劉氏（1751－1806），[康熙五十一年進士官](../Page/模板:康熙五十一年壬辰科殿試金榜.md "wikilink")[太子少保](../Page/太子少保.md "wikilink")[協辦大學士](../Page/協辦大學士.md "wikilink")[劉於義孫女](../Page/劉於義.md "wikilink")、四川[布政使](../Page/布政使.md "wikilink")[劉益之女](../Page/劉益_\(清朝\).md "wikilink")。\[5\]

## 注釋

## 参考文献

  - [王昶](../Page/王昶_\(清朝\).md "wikilink")，《資政大夫刑部左侍郎贈尚書錢文敏公維城神道碑銘》
  - [李桓辑](../Page/李桓.md "wikilink")《国朝耆献类征初编》卷八十八《卿贰四十八·钱维城传》，周骏富辑《清代传记丛刊》宗录类七，第十九册，1985年台湾明文书局出版。

[Category:乾隆十年乙丑科進士](../Category/乾隆十年乙丑科進士.md "wikilink")
[Category:清朝狀元](../Category/清朝狀元.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝刑部侍郎](../Category/清朝刑部侍郎.md "wikilink")
[Category:清朝画家](../Category/清朝画家.md "wikilink")
[Category:常州人](../Category/常州人.md "wikilink")
[W](../Category/钱姓.md "wikilink")
[Category:諡文敏](../Category/諡文敏.md "wikilink")
[W](../Category/武進段莊錢氏.md "wikilink")

1.  《清实录·高宗纯（乾隆）皇帝实录》卷之二百四十
2.  《[清史稿](../Page/清史稿.md "wikilink")·列传·钱维城》
3.  《文钞》卷四《三弟竹初诗稿序》：“予今年五十有三矣，髭发半就白，牙齿亦多动摇，复苦消症（糖尿病），偶一构思，竟夕不得寐，即欲精进，而精神衰落。……时乾隆壬辰六月望后一日。”
4.  《中國人名異稱大辭典（綜合卷）》1262：錢維城，字幼安，一字宗磐，號紉庵，一號茶山，又號稼軒，謚文敏。清江蘇武進（今江蘇常州）人。乾隆十年一甲一名進士，由修撰累官刑部左侍郎。工繪事，擅書法。著有《茶山集》。
5.  《段莊錢氏族譜》（咸豐五年重修）卷八·西五房世表·葉六十三。