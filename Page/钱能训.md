[Qian_Nengxun.jpg](https://zh.wikipedia.org/wiki/File:Qian_Nengxun.jpg "fig:Qian_Nengxun.jpg")
**钱能训**（）\[1\]，字**幹丞**、**幹臣**，[浙江省](../Page/浙江省.md "wikilink")[嘉興府](../Page/嘉興府.md "wikilink")[嘉善县](../Page/嘉善县.md "wikilink")[魏塘镇人](../Page/魏塘镇.md "wikilink")。清末民初政治人物。

## 生平

钱能训生于清同治八年（1869年）。他自幼好學，博覽群書，於[光绪二十四年](../Page/光绪.md "wikilink")（1898年）中戊戌科二甲第18位进士，選[庶吉士](../Page/庶吉士.md "wikilink")，同年五月，授[刑部主事](../Page/刑部主事.md "wikilink")\[2\]，任刑部主事，后升任[员外郎](../Page/员外郎.md "wikilink")、郎中。光緒二十九年（1903年）改[監察御史](../Page/監察御史.md "wikilink")，历任河南、江西各道监察御史。次年成为[巡警部尚書](../Page/巡警部.md "wikilink")（后改民政部尚書）[徐世昌的僚属](../Page/徐世昌.md "wikilink")，历任巡警部左参议、左丞，民政部左丞。光緒三十三年（1907年），徐世昌出任[東三省总督](../Page/東三省总督.md "wikilink")，銭能訓随其前往[東三省赴任](../Page/東三省.md "wikilink")，任东三省左右参赞，还曾署[顺天府府尹](../Page/顺天府.md "wikilink")。\[3\]\[4\]

宣統二年（1910年），经[軍機大臣徐世昌推薦](../Page/軍機大臣.md "wikilink")，銭能訓任[陝西](../Page/陝西省.md "wikilink")[布政使](../Page/承宣布政使.md "wikilink")，任内護理[陝西巡撫](../Page/陝西巡撫.md "wikilink")。銭能訓大肆搜捕革命党。後見大勢已去，自戕未遂，由革命军医治後送出[潼关](../Page/潼关.md "wikilink")。\[5\]\[6\]

[中華民国成立后](../Page/中華民国.md "wikilink")，銭能訓因为和徐世昌的关系而得到[袁世凱重用](../Page/袁世凱.md "wikilink")。[民国二年](../Page/民国紀元.md "wikilink")（1913年）10月，出任[熊希齡内閣内務部次長](../Page/熊希齡内閣.md "wikilink")。民国三年（1914年）4月，當選蒙古、西藏、青海三省区選出的[約法会議議員](../Page/約法会議.md "wikilink")。同年5月，徐世昌任国務卿兼[礼制館總裁](../Page/礼制館.md "wikilink")，銭能訓任[政事堂右丞兼礼制館副總裁](../Page/政事堂.md "wikilink")。民國四年（1915年）1月，由[袁世凯授中卿](../Page/袁世凯.md "wikilink")，还曾获得二等嘉禾章。\[7\]\[8\]10月，銭能訓署理[平政院院長兼](../Page/平政院.md "wikilink")[文官高等懲戒委員会委員長](../Page/文官高等懲戒委員会.md "wikilink")，翌年4月，正式就任平政院院長。民国六年（1917年）7月[张勋复辟期間](../Page/张勋复辟.md "wikilink")，被任命为农工部左侍郎。\[9\]

同年12月，銭能訓出任[王士珍内閣内務總長](../Page/王士珍内閣.md "wikilink")。次年2月辞职。此後曾代理国務總理。民国七年（1918年）3月，[第三次段祺瑞內閣成立](../Page/第三次段祺瑞內閣.md "wikilink")，銭能訓再任内務總長。10月，徐世昌任大總統期间，他再度代理国務總理。12月14日，正式任国務總理，组织[钱能训内阁](../Page/钱能训内阁.md "wikilink")，并在翌年1月兼任内務總長。\[10\]

銭能訓任职期間，[皖系和](../Page/皖系.md "wikilink")[直系間的對立加劇](../Page/直系.md "wikilink")，其舉步維艱，民国八年（1919年），[五四運動發生](../Page/五四運動.md "wikilink")，銭能訓於6月引咎辞职。此後，他歷任蘇浙太湖水利工程督辦、外交部顧問。\[11\]

民国十三年（1924年）6月5日，銭能訓在北京寓所病逝。享年55嵗。\[12\]

1924年7月，钱能训靈柩回到嘉善，葬於小桥村卞家桥钱氏故茔，[徐世昌赋诗哀悼并撰写](../Page/徐世昌.md "wikilink")[墓志铭](../Page/墓志铭.md "wikilink")。1986年，嘉善县人民政府拨款重修钱能训墓，并定为县级文物保护单位。

## 逸事

[Axiom_victory_memorial_arch_of_Peking.jpg](https://zh.wikipedia.org/wiki/File:Axiom_victory_memorial_arch_of_Peking.jpg "fig:Axiom_victory_memorial_arch_of_Peking.jpg")
1919年[克林德碑被重新组装](../Page/克林德碑.md "wikilink")，改为[公理战胜坊](../Page/公理战胜坊.md "wikilink")，钱能训题写了“公理战胜”四个大字。\[13\]

## 註釋

## 参考文献

  - <span style="font-size:90%;">張樹勇「銭能訓」</span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;">[钱家駺，我爷爷当过北洋总理，北青网，2003-08-28](https://archive.today/20120707120907/http://bjyouth.ynet.com/article.jsp?oid=2532752)</span>
  - <span style="font-size:90%;"></span>

## 延伸阅读

  - 何明，北洋政府总理的最后结局，北京：中共党史出版社，2008年

## 参见

  - [钱能训临时内阁](../Page/钱能训临时内阁.md "wikilink")
  - [钱能训内阁](../Page/钱能训内阁.md "wikilink")

{{-}}

| （[北京政府](../Page/北京政府.md "wikilink")） |
| ------------------------------------ |

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林院编修](../Category/清朝翰林院编修.md "wikilink")
[Category:清朝監察御史](../Category/清朝監察御史.md "wikilink")
[Category:清朝順天府府尹](../Category/清朝順天府府尹.md "wikilink")
[Category:清朝陝西布政使](../Category/清朝陝西布政使.md "wikilink")
[Category:清朝陝西巡撫](../Category/清朝陝西巡撫.md "wikilink")
[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:中華民國國務總理](../Category/中華民國國務總理.md "wikilink")
[Category:平政院院长](../Category/平政院院长.md "wikilink")
[Category:约法会议议员](../Category/约法会议议员.md "wikilink")
[Category:嘉善人](../Category/嘉善人.md "wikilink")
[N](../Category/钱姓.md "wikilink")

1.  其生年据[钱家駺，我爷爷当过北洋总理，北青网，2003-08-28](http://bjyouth.ynet.com/article.jsp?oid=2532752)
    。《约法会议纪录》作其年四十七岁，按1915年47岁，则生于约1868年，与此相差不大。

2.  《大清德宗同天崇运大中至正经文纬武仁孝睿智端俭宽勤景皇帝实录》（卷四百十九）：光绪二十四年。戊戌。五月。癸丑朔。……○引见新科进士。得旨……户部候补员外郎王廷材、著以员外郎即用。刑部候补主事钱能训、工部候补主事暴翔云、均著以主事即用。内阁即补侍读中书朱彭寿、著仍归原班补用。中书赵椿年、杨廷玑、国子监助教崇方、均著以原班用。内阁候补中书龙学泰、著仍以中书候补。分发江苏补用知府江忠振、著以知府发往江苏补用。陕西候补知府王世相、著以知府发往陕西补用。分发补用同知李士麟、签分甘肃大挑知县陈易奇、湖南大挑知县鲁晋、湖北大挑知县刘汉云、均著以原班用。余著归班铨选。

3.  见 張樹勇《銭能訓》、《民国人物大辞典 増訂版》、《民国職官年表》

4.  《约法会议纪录》第126页

5.
6.
7.
8.
9.
10.
11.
12.
13.