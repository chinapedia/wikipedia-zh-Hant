**钱锺韩**（），[工程热物理和](../Page/工程热物理.md "wikilink")[自动化专家](../Page/自动化.md "wikilink")。中国机电结合的动力工程学科的创建者。[江苏](../Page/江苏.md "wikilink")[无锡人](../Page/无锡.md "wikilink")。\[1\]

## 生平

钱锺韩1927年7月毕业于[无锡](../Page/无锡.md "wikilink")[辅仁中学](../Page/辅仁中学.md "wikilink")。1933年7月毕业于[上海](../Page/上海.md "wikilink")[交通大学电机系](../Page/交通大学.md "wikilink")。1934年赴[英国](../Page/英国.md "wikilink")[伦敦大学理工学院深造](../Page/伦敦大学.md "wikilink")，1936年6月毕业。回国后，1937年任[贵州](../Page/贵州.md "wikilink")[浙江大学机械系教授](../Page/浙江大学.md "wikilink")。1945年任[昆明](../Page/昆明.md "wikilink")[西南联合大学电机系教授](../Page/西南联合大学.md "wikilink")。1946年抗战胜利后，任[南京](../Page/南京.md "wikilink")[中央大学机械系教授](../Page/國立中央大學.md "wikilink")，1949年[中央大学改名](../Page/中央大学.md "wikilink")[南京大学后](../Page/南京大学.md "wikilink")，任[南京大学工学院院长](../Page/南京大学工学院.md "wikilink")。1952年，南京大学工学院独立组建[南京工学院](../Page/南京工学院.md "wikilink")，任南京工学院教授，曾兼自动化研究所所长，并任南京工学院副院长。1981年任[南京工学院院长](../Page/南京工学院.md "wikilink")。1988年南京工学院改名[东南大学后](../Page/东南大学.md "wikilink")，任东南大学名誉校长。曾任国务院学位委员会工学学科评议组成员兼自动控制学科评议组组长，江苏省第五、六届政协主席
（1983—1989）。1981年当选为[中国科学院技术科学部](../Page/中国科学院.md "wikilink")[学部委员](../Page/学部委员.md "wikilink")（院士）。

## 家族

钱锺韩是[钱基厚之子](../Page/钱基厚.md "wikilink")，[钱锺书的堂弟](../Page/钱锺书.md "wikilink")。

## 著作

  - 《模拟技术》
  - 《自动调节原理》

## 注释

{{-}}

[Category:钱锺书](../Category/钱锺书.md "wikilink")
[Category:交通大学校友](../Category/交通大学校友.md "wikilink")
[Category:伦敦大学校友](../Category/伦敦大学校友.md "wikilink")
[Category:吳越錢氏](../Category/吳越錢氏.md "wikilink")
[ZHONG](../Category/钱姓.md "wikilink")
[Category:中国物理学家](../Category/中国物理学家.md "wikilink")
[Category:中国工程师](../Category/中国工程师.md "wikilink")
[Category:无锡人](../Category/无锡人.md "wikilink")
[Category:国立西南联合大学教授](../Category/国立西南联合大学教授.md "wikilink")
[Category:浙江大学教授](../Category/浙江大学教授.md "wikilink")
[Category:南京工学院教授](../Category/南京工学院教授.md "wikilink")
[Category:东南大学校长](../Category/东南大学校长.md "wikilink")
[Category:九三学社社员](../Category/九三学社社员.md "wikilink")
[Category:江蘇省政協主席](../Category/江蘇省政協主席.md "wikilink")
[Category:南京工学院院长](../Category/南京工学院院长.md "wikilink")

1.