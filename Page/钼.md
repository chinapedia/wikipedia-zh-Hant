钼（**Molybdenum**）是一种化学元素，它的化学符号是**Mo**，它的原子序数是42，是一种灰色的过渡金属。因为一開始钼矿石与铅矿石被混淆了，因此Molybdenum之名来自[新拉丁语](../Page/新拉丁语.md "wikilink")
*molybdaenum*，后者来自[古希臘語](../Page/古希臘語.md "wikilink")
**，意思是铅。\[1\]钼矿石在历史上被人们所熟知，但该元素的发现(即从其它金属中区分出来)是在1778年，由
[卡尔·威廉·舍勒识别出来](../Page/卡尔·威廉·舍勒.md "wikilink")。该金属在1781年第一次被彼得·雅各·耶尔姆分离得出。

钼在地球上没有自然金属的形态，但是在矿物中以各种氧化物的形式出现。在单体元素形式中，钼是一种灰色金属，呈灰口铸铁颜色，是所有元素中熔点排名第六高。它很容易在合金中形成坚硬、稳定的碳化物，因此，世界上大多数钼产品（约80%）都被用作某种铁合金，包括高强度合金和高温合金。

大多数钼化合物在水中微溶，但是当含钼的矿物与氧气和水接触时可以形成钼离子。在工业上，钼化合物（世界上约有14%的产品）被用于高压和高温应用品，如色素或催化剂等。

目前，一些细菌在打破大气氮分子的[化学键上最常用的催化剂是含钼酶](../Page/化学键.md "wikilink")，能起到生物[固氮作用](../Page/固氮作用.md "wikilink")。在细菌和动物中，虽然只有细菌和蓝藻酶会参与到固氮活动中，但已知的含钼酶至少有50种。这些[固氮酶含钼的形式与其它含钼酶不同](../Page/固氮酶.md "wikilink")，但都有氧化形式的钼，用以搭配钼辅因子。由于钼的各种辅因子酶的多样功能，钼成为所有高于[真核生物组织的](../Page/真核生物.md "wikilink")[膳食矿物质](../Page/膳食矿物质.md "wikilink")，虽然并非所有细菌都用到钼。

## 鉬的發現史

在18世紀，[輝鉬礦往往被認為是鉛礦](../Page/輝鉬礦.md "wikilink")。1778年[瑞典的](../Page/瑞典.md "wikilink")[卡尔·威廉·舍勒從輝鉬礦中提取出了](../Page/卡尔·威廉·舍勒.md "wikilink")[氧化鉬](../Page/氧化鉬.md "wikilink")，根據舍勒的啟發，1781年他的朋友，同是瑞典人的把鉬土用「[碳還原法](../Page/碳.md "wikilink")」[分離出新的](../Page/分離.md "wikilink")[金屬鉬](../Page/金屬.md "wikilink")。

## 性质

### 物理性质

在純物質的狀態下，鉬是銀灰色的金屬，莫氏硬度為5.5。它的熔點為2,623 °C，沸點為4639°C(4,753
°F)，在天然存在的元素中，只有鉭，鋨，錸，鎢和碳有有高於鉬的熔點。\[2\]鉬的弱氧化起始於300
°C (572
°F)。在商用金屬中，鉬是[熱膨脹係數最低的一種](../Page/热膨胀系数.md "wikilink")。\[3\]當鉬絲的直徑從約50-100
nm減小到10 nm時，鉬絲的拉伸強度增加三倍(10 GPa\~30 GPa)。\[4\]

### 同位素

目前有35種已知的[鉬同位素](../Page/鉬的同位素.md "wikilink")，原子量介於83到117之間，其中有四種亞穩態[同核異構物](../Page/同核異構體.md "wikilink")(nuclear
isomers)，天然存在的同位素有七種，其原子量為92, 94, 95, 96, 97, 98, and
100。在天然存在中的七種同位素，只有鉬-100是不穩定的。\[5\]

鉬-98是含量最高的同位素，佔鉬總比例的24.14%
。鉬-100擁有1019年的半衰期，並會經過[雙重β衰變後變成釕](../Page/雙β衰變.md "wikilink")-100
，質量數介於111和117之間的鉬同位素都擁有約150ns的半衰期。\[6\]\[7\]所有鉬的不穩定同位素會衰變成鈮，鎝和釕的同位素。\[8\]

最常見的同位素應用為鉬-99，鉬-99為衰變後的產物，它是短壽命伽瑪放射性同位素[鎝-99m的母體同位素](../Page/:en:Technetium-99m.md "wikilink")，應用於醫學成像。\[9\]於2008年the
[Delft University of
Technology申請了專利](../Page/:en:Delft_University_of_Technology.md "wikilink")，內容為以鉬-98為基礎生產鉬-99。\[10\]

### 化合物以及化學性質

鉬是一種過渡金屬，其電負度為2.16，標準原子量為95.95 g/mol。\[11\]\[12\]

在室溫下，鉬不會與氧氣或水發生明顯反應，在600 °C以上會氧化，並生成[三氧化鉬](../Page/三氧化钼.md "wikilink")。

2 Mo + 3  → 2

高溫下，三氧化鉬具有揮發性並會昇華。這可以避免形成連續性的保護性氧化層(氧化層可以保護金屬，避免進一步氧化到內部)。\[13\]

鉬有多個氧化態，最穩定存在的為+4和+6。鉬化合物的化學性質比較相似於鎢而非鉻。例如：鉬三價與鎢三價化合物具有相似的不穩定性，而鉻三價化合物則穩定性較好。

鉬化合物的最高氧化態為六價，化合物為三氧化鉬。常見的硫化物為二硫化鉬。\[14\]

三氧化鉬可溶於強鹼性的水溶液中，並形成[鉬酸鹽類](../Page/钼酸盐.md "wikilink")，鉬酸鹽的氧化性比鉻酸鹽較弱，但兩者會在低pH值環境下，縮合成氧錯離子(complex
oxyanions)，像是\[Mo<sub>7</sub>O<sub>24</sub>\]<sup>6−</sup> and
\[Mo<sub>8</sub>O<sub>26</sub>\]<sup>4−</sup>，多鉬酸鹽可以加入其他金屬離子，形成多金屬氧酸鹽([polyoxometalates](../Page/:en:Polyoxometalate.md "wikilink"))。\[15\]

P\[Mo<sub>12</sub>O<sub>40</sub>\]<sup>3−</sup>為深藍色化合物，用於磷的光譜檢測中。\[16\]鉬的各種氧化價數，可從各種鉬的氯化物中見得。\[17\]

<table>
<thead>
<tr class="header">
<th><p>氧化物<br />
形态</p></th>
<th><p>例子[18]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>−2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>0</p></td>
<td><p><a href="../Page/六羰基钼.md" title="wikilink">{{chem</a></p></td>
</tr>
<tr class="odd">
<td><p>+1</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>+2</p></td>
<td><p><a href="../Page/Molybdenum(II)_chloride.md" title="wikilink">{{chem</a></p></td>
</tr>
<tr class="odd">
<td><p>+3</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>+4</strong></p></td>
<td><p><a href="../Page/二硫化钼.md" title="wikilink">{{chem</a></p></td>
</tr>
<tr class="odd">
<td><p>+5</p></td>
<td><p><a href="../Page/Molybdenum(V)_chloride.md" title="wikilink">{{chem</a></p></td>
</tr>
<tr class="even">
<td><p><strong>+6</strong></p></td>
<td><p><a href="../Page/Molybdenum(VI)_fluoride.md" title="wikilink">{{chem</a></p></td>
</tr>
</tbody>
</table>

[Phosphotungstate-3D-polyhedra.png](https://zh.wikipedia.org/wiki/File:Phosphotungstate-3D-polyhedra.png "fig:Phosphotungstate-3D-polyhedra.png")\]\]

## 鉬礦與生產

### 發展歷史

## 應用

### 合金

钼主要用于钢铁工业。
0.3％的钼添加剂可提高几种钢种的铸铁强度和耐腐蚀性\[19\]。耐锈和耐酸的钼钢合金含有0.4至3.5％的钼。表面处理可以提高含钼钢的机械强度\[20\]。一些快速钢的钼含量也可达到14.5％。钼替代某些钢种的镍。在这种情况下，获得Cr-Mo钢代替Cr-Ni钢。目前，钼还用于生产耐热超级合金。

### 物質狀態的其他應用

### 化合物應用

MoO3催化剂用于许多有机化学过程，例如重整过程，石油馏分的脱硫\[21\]，邻苯二甲酸酐，马来酸酐和蒽醌等。产生。其混合氧化物用作丙烯醛和丙烯酸生产中的催化剂\[22\]\[23\]\[24\]\[25\]。钼化合物用于颜料，染料，试剂，润滑剂，催化剂，缓蚀剂，陶瓷助剂，微量元素等。产生。硼化钼，碳化物，硅化物具有半导体特性。

## 生物學層面

### 固氮催化

### 鉬作為輔酶

### 人體新陳代謝與缺乏

### 疾病

### 銅-鉬拮抗作用

## 飲食建議與食物來源

## 注意事項

  - [Lu Le Laboratory
    鉬金屬的介紹](http://www.youtube.com/watch?v=m5JTNvbJ0As)

## 注釋

[Category:过渡金属](../Category/过渡金属.md "wikilink")
[\*](../Category/钼.md "wikilink")
[5F](../Category/第5周期元素.md "wikilink")
[5F](../Category/化学元素.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.
9.  Armstrong, John T. (2003). "Technetium". Chemical & Engineering
    News. Retrieved 2009-07-07

10. Wolterbeek, Hubert Theodoor; Bode, Peter "A process for the
    production of no-carrier added 99Mo". European Patent EP2301041 (A1)
    ― 2011-03-30. Retrieved on 2012-06-27.

11.

12.

13.

14.

15.

16.

17.
18.

19.

20.

21.

22.

23.

24.

25.