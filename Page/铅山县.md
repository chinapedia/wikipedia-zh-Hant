**山县**\[1\]位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[江西省东北部](../Page/江西省.md "wikilink")、[信江中游](../Page/信江.md "wikilink")，[武夷山脉北麓](../Page/武夷山脉.md "wikilink")，是[上饶市所辖的一个](../Page/上饶市.md "wikilink")[县](../Page/县.md "wikilink")。

## 历史沿革

[南唐](../Page/南唐.md "wikilink")[保大十一年](../Page/保大.md "wikilink")（953年），析[弋阳](../Page/弋阳.md "wikilink")、[上饶置铅山县](../Page/上饶.md "wikilink")，因[永平西四里有铅山](../Page/永平镇.md "wikilink"),遂以山名县。治所在永平，隶[信州](../Page/信州.md "wikilink")。

[元](../Page/元.md "wikilink")[至元二十九年](../Page/至元.md "wikilink")(1292年)，升县为州，名铅山州（一名永平州），隶属[浙江行中书省](../Page/浙江行中书省.md "wikilink")。

[明](../Page/明.md "wikilink")[洪武二年](../Page/洪武.md "wikilink")(1360年)复为县，隶[广信府](../Page/广信府.md "wikilink")。

1914年属豫章道，1931年属第六行政区，1949年5月以后隶属上饶地(专)区。

自建县起至1949年6月，县治一直设在[永平镇](../Page/永平镇.md "wikilink")。1949年7月，县治迁[河口镇](../Page/河口镇_\(铅山县\).md "wikilink")。\[2\]

## 行政区划

铅山县7个镇、10个乡（其中2个民族乡）。县人民政府驻河口镇，永平镇为第二大镇。

  - [镇](../Page/镇.md "wikilink")：[河口镇](../Page/河口镇_\(铅山县\).md "wikilink")、[永平镇](../Page/永平镇_\(铅山县\).md "wikilink")、[石塘镇](../Page/石塘镇_\(铅山县\).md "wikilink")、[鹅湖镇](../Page/鹅湖镇_\(铅山县\).md "wikilink")、[湖坊镇](../Page/湖坊镇_\(铅山县\).md "wikilink")、[武夷山镇](../Page/武夷山镇.md "wikilink")、[汪二镇](../Page/汪二镇.md "wikilink")
  - [乡](../Page/乡.md "wikilink")：陈坊乡、虹桥乡、新滩乡、葛仙山乡、稼轩乡、英将乡、紫溪乡、太源畲族乡、天柱山乡、篁碧畲族乡

## 地理

### 区位

铅山县位于江西东北部、武夷山脉北麓，东为[上饶县](../Page/上饶县.md "wikilink")，北为[横峰县](../Page/横峰县.md "wikilink")，西为[弋阳县和](../Page/弋阳县.md "wikilink")[贵溪市](../Page/贵溪市.md "wikilink")，南邻福建省的[武夷山市和](../Page/武夷山市.md "wikilink")[光泽县](../Page/光泽县.md "wikilink")。\[3\]

### 地形地貌

北部信江沿岸有少量[平原](../Page/平原.md "wikilink")，中部多[丘陵](../Page/丘陵.md "wikilink")，南部为[山地](../Page/山地.md "wikilink")。基本地貌特征呈南高北低。

### 山脉山峰

[武夷山脉主峰](../Page/武夷山脉.md "wikilink")[黄岗山位于铅山与福建](../Page/黄岗山.md "wikilink")[武夷山市交界处](../Page/武夷山市.md "wikilink")，海拔2157米，为[中国大陆东南最高峰](../Page/中国.md "wikilink")。其它较高的山峰有[独竖尖](../Page/独竖尖.md "wikilink")（海拔2128米，江西第二高峰）、[过风坳](../Page/过风坳.md "wikilink")（海拔1887米）。

### 水系

全县均属[信江水系](../Page/信江.md "wikilink")，信江干流横贯，在县内主要有两大[支流](../Page/支流.md "wikilink")：[铅山河和](../Page/铅山河.md "wikilink")[陈坊河](../Page/陈坊河.md "wikilink")，铅山河在[河口镇东汇入信江](../Page/河口镇_\(铅山县\).md "wikilink")，陈坊河在[汪二镇北汇入信江](../Page/汪二镇.md "wikilink")。建有中型[水库两座](../Page/水库.md "wikilink")：[丰产水库](../Page/丰产水库.md "wikilink")、[铁炉水库](../Page/铁炉水库.md "wikilink")。\[4\]

## 交通

  - 铁路：[橫南铁路](../Page/橫南铁路.md "wikilink")
  - 公路：[202省道](../Page/202省道.md "wikilink")、[204省道](../Page/204省道.md "wikilink")、[宁上高速公路](../Page/宁上高速公路.md "wikilink")（G1514，在建）

## 经济

全国第二大铜矿[永平铜矿坐落境内](../Page/永平铜矿.md "wikilink")。2007年，全县实现GDP32.5亿元，财政总收入3.06亿元。\[5\]

## 人口

绝大部分居民为[汉族](../Page/汉族.md "wikilink")，少数民族包括[畲族](../Page/畲族.md "wikilink")（总人口为3700余人）和少量的蒙、回、藏、苗、壮、满、黎族。\[6\]\[7\]

## 文化

[宋](../Page/宋.md "wikilink")[淳熙二年](../Page/淳熙.md "wikilink")（1175年）四月，[朱熹](../Page/朱熹.md "wikilink")、[吕祖谦](../Page/吕祖谦.md "wikilink")、[陆九龄](../Page/陆九龄.md "wikilink")、[陆九渊在鹅湖寺聚会](../Page/陆九渊.md "wikilink")，史称“[鹅湖之会](../Page/鹅湖之会.md "wikilink")”。后在寺西建有“四贤祠”。

[淳熙十五年](../Page/淳熙.md "wikilink")（1188年）冬，[辛弃疾与](../Page/辛弃疾.md "wikilink")[陈亮会于鹅湖寺](../Page/陈亮.md "wikilink")。[庆元二年](../Page/庆元.md "wikilink")（1196年），辛弃疾在上饶[带湖的住宅失火](../Page/带湖.md "wikilink")，遂移居铅山瓢泉。[开禧三年](../Page/开禧.md "wikilink")（1207年）九月十日，辛弃疾病逝，终年68岁，葬虎头门彭家湾洋源山。

[淳祐十年](../Page/淳祐.md "wikilink")（1250年），宋理宗为鹅湖“四贤祠”改名“文宗书院”，现称[鹅湖书院](../Page/鹅湖书院.md "wikilink")。

蒋士铨（1725—1784）清代戏曲家，文学家。字心馀、苕生，号藏园，又号清容居士，晚号定甫。铅山（今属江西）人。乾隆二十二年进士，官翰林院编修。乾隆二十九年辞官后主持蕺山、崇文、安定三书院讲席。精通戏曲，工诗古文，与袁枚、赵翼合称江右三大家。士铨所著《忠雅堂诗集》存诗二千五百六十九首，存于稿本的未刊诗达数千首，其戏曲创作存《红雪楼九种曲》等四十九种。

古代以[連四紙產地聞名](../Page/連四紙.md "wikilink")。

## 风景名胜

[Ehu_shuyuan_0227.jpg](https://zh.wikipedia.org/wiki/File:Ehu_shuyuan_0227.jpg "fig:Ehu_shuyuan_0227.jpg")

  - 高山：[黄岗山](../Page/黄岗山.md "wikilink")、[独竖尖](../Page/独竖尖.md "wikilink")、[过风坳](../Page/过风坳.md "wikilink")
  - 道教名山：[葛仙山](../Page/葛仙山.md "wikilink")
  - [鹅湖山国家森林公园](../Page/鹅湖山国家森林公园.md "wikilink")，包括[鹅湖书院](../Page/鹅湖书院.md "wikilink")（[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")）、[辛弃疾墓](../Page/辛弃疾墓.md "wikilink")（[江西省文物保护单位](../Page/江西省文物保护单位.md "wikilink")）、[辛弃疾故居](../Page/辛弃疾故居.md "wikilink")——瓢泉，[蒋仕铨墓](../Page/蒋仕铨墓.md "wikilink")（江西省文物保护单位），[峰顶山等](../Page/峰顶山.md "wikilink")\[8\]
  - 其它[江西省文物保护单位](../Page/江西省文物保护单位.md "wikilink")：[澄波桥](../Page/澄波桥.md "wikilink")、[费宏墓](../Page/费宏墓.md "wikilink")
  - 古镇：[河口镇](../Page/河口镇_\(铅山县\).md "wikilink")、[石塘镇](../Page/石塘镇_\(铅山县\).md "wikilink")、[湖坊镇](../Page/湖坊镇_\(铅山县\).md "wikilink")、[陈坊乡](../Page/陈坊乡.md "wikilink")

## 参考文献

## 外部链接

  - [铅山县政府网站](http://www.jxyanshan.gov.cn/)
  - [铅山在线](http://www.jxys.ccoo.cn/)
  - [同治铅山县志](http://jadecolour.wordpress.com/)

[铅山县](../Page/category:铅山县.md "wikilink")
[县](../Page/category:上饶区县市.md "wikilink")
[上饶](../Page/category:江西省县份.md "wikilink")

1.

2.

3.

4.
5.

6.
7.

8.