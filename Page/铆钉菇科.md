**铆钉菇科**（[學名](../Page/學名.md "wikilink")：Gomphidiaceae）是[担子菌门](../Page/担子菌门.md "wikilink")[牛肝菌目的一個](../Page/牛肝菌目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，其下有四個[屬](../Page/屬_\(生物\).md "wikilink")。有別於典型牛肝菌，本科類群中除了[單型屬](../Page/單型.md "wikilink")為[腹菌外](../Page/腹菌.md "wikilink")，均為，產孢的[子實層位於](../Page/子實層.md "wikilink")[蕈褶上](../Page/蕈褶.md "wikilink")，而非[子實體上的孔狀構造](../Page/子實體_\(真菌\).md "wikilink")。的學名與俗名均與本科相似，但與本科並無關聯。

## 分類

铆钉菇科過去與其他傘菌一起被歸入[傘菌目中](../Page/傘菌目.md "wikilink")，但孢子形狀等許多顯微特徵都顯示其與牛肝菌目有關，[分子種系發生學的研究結果證實了此項理論](../Page/分子種系發生學.md "wikilink")，發現铆钉菇科應歸屬牛肝菌目，而本科的[蕈褶是](../Page/蕈褶.md "wikilink")[趨同演化的結果](../Page/趨同演化.md "wikilink")，與傘菌目類群的蕈褶無關。分子證據顯示在牛肝菌目中，本科與[乳牛肝菌科](../Page/乳牛肝菌科.md "wikilink")、[須腹菌科與](../Page/須腹菌科.md "wikilink")[Truncocolumellacae等類群的親緣關係較為接近](../Page/Truncocolumellacae.md "wikilink")\[1\]，與的研究結果吻合\[2\]。有研究將本科與這三科共同歸屬於乳牛肝菌亞目（Suillineae）\[3\]。

## 生態

铆钉菇科的物種傳統上被認為會與樹木形成[互利共生的](../Page/互利共生.md "wikilink")，但有研究發現本科蕈類的[子實體經常與其他種蕈類一起出現](../Page/子實體_\(真菌\).md "wikilink")，本身很少形成複雜的[菌絲體](../Page/菌絲體.md "wikilink")，在植物根部皮層細胞中形成[吸器](../Page/吸器.md "wikilink")，無法獨立於其他物種進行分離培養等特徵，顯示本科蕈類的營養方式，可能是[寄生於外菌根上的其他真菌或植物](../Page/寄生.md "wikilink")\[4\]。

## 註釋

## 參考資料

[Category:牛肝菌目](../Category/牛肝菌目.md "wikilink")

1.
2.

3.

4.