**铠甲动物门**（[學名](../Page/學名.md "wikilink")：）是[动物界的一](../Page/动物界.md "wikilink")[门](../Page/门_\(生物\).md "wikilink")，其下的[物種生活在](../Page/物種.md "wikilink")[海洋中](../Page/海洋.md "wikilink")，这个门在1983年被[莱因霍尔德发现](../Page/莱因霍尔德.md "wikilink")。目前尚未發現任何鎧甲動物的[化石](../Page/化石.md "wikilink")。

牠们的近亲是[动吻动物门和](../Page/动吻动物门.md "wikilink")[鳃曳动物门](../Page/鳃曳动物门.md "wikilink")，此三門統稱為[有棘動物](../Page/有棘動物.md "wikilink")（Scalidophora）。但有些分子證據顯示牠們的親緣可能與[泛節肢動物更加親近](../Page/泛節肢動物.md "wikilink")。\[1\]

## 形態

这种动物有[头](../Page/头.md "wikilink")，[嘴和](../Page/嘴.md "wikilink")[消化系统](../Page/消化系统.md "wikilink")，没有[循环系统和](../Page/循环系统.md "wikilink")[内分泌系统](../Page/内分泌系统.md "wikilink")。是[假体腔动物](../Page/假体腔动物.md "wikilink")（有嘴和[肛门的动物](../Page/肛门.md "wikilink")）。[雌雄同体](../Page/雌雄同体.md "wikilink")，[卵生](../Page/卵生.md "wikilink")，生活史異常複雜。2010年發現該門有[無氧呼吸的物種](../Page/無氧呼吸.md "wikilink")。发育中经过一独特的[希金斯](../Page/希金斯.md "wikilink")（Higgins）幼虫期，很像成体，但它有一对[趾用于运动](../Page/趾.md "wikilink")。\[2\]

## 分類

鎧甲動物門目前没有准确分类。
种类：

  -   - [Nanaloricus
        mysticus](../Page/:enNanaloricus_mysticus.md "wikilink")

  -   -
## 參考資料

  - [The first metazoa living in permanently anoxic
    conditions](http://www.biomedcentral.com/content/pdf/1741-7007-8-30.pdf)

[Category:动物](../Category/动物.md "wikilink")
[Category:蛻皮動物](../Category/蛻皮動物.md "wikilink")
[Category:无脊椎动物](../Category/无脊椎动物.md "wikilink")
[\*](../Category/铠甲动物门.md "wikilink")

1.
2.