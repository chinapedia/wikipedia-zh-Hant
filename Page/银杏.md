**銀杏**（[学名](../Page/学名.md "wikilink")：**），[落葉喬木](../Page/落葉喬木.md "wikilink")，[壽命可達](../Page/壽命.md "wikilink")3000年以上。又名**公孙树**、**鴨腳樹**、**鴨腳子**等\[1\]，其裸露的種子稱為**白果**。属[裸子植物](../Page/裸子植物.md "wikilink")[銀杏門唯一現存](../Page/銀杏門.md "wikilink")[物種](../Page/物種.md "wikilink")，和它同[門的所有其他物種都已](../Page/門_\(生物\).md "wikilink")[灭绝](../Page/灭绝.md "wikilink")，因此被称為[植物界的](../Page/植物界.md "wikilink")“[活化石](../Page/活化石.md "wikilink")”。已发现的化石可以追溯到2.7亿年前。銀杏原產於中國，現廣泛種植於全世界，并被早期引入人类历史。它有多種用途，可作為[傳統醫學用途和食物](../Page/傳統醫學.md "wikilink")。

## 演化

[Ginkgo_biloba_MacAbee_BC.jpg](https://zh.wikipedia.org/wiki/File:Ginkgo_biloba_MacAbee_BC.jpg "fig:Ginkgo_biloba_MacAbee_BC.jpg")葉片[化石](../Page/化石.md "wikilink"),
[加拿大](../Page/加拿大.md "wikilink")[卑詩省](../Page/卑詩省.md "wikilink")\]\][Fossil_Plant_Ginkgo.jpg](https://zh.wikipedia.org/wiki/File:Fossil_Plant_Ginkgo.jpg "fig:Fossil_Plant_Ginkgo.jpg")[銀杏屬葉片化石](../Page/銀杏屬.md "wikilink")，[英格蘭](../Page/英格蘭.md "wikilink")\]\]
和它相亲的銀杏類植物在两亿七千万年前的[二疊紀時就已经生成](../Page/二疊紀.md "wikilink")，属于银杏門。晚[三疊紀時](../Page/三疊紀.md "wikilink")，銀杏類植物快速發展，之後的[侏羅紀和早](../Page/侏羅紀.md "wikilink")[白堊紀達到了鼎盛時期](../Page/白堊紀.md "wikilink")，銀杏類的五個[科同時存在](../Page/科.md "wikilink")，除[赤道外廣泛分布於世界各大洲](../Page/赤道.md "wikilink")。但白堊紀後期[被子植物迅速崛起時](../Page/被子植物.md "wikilink")，銀杏類像其它[裸子植物一樣也急劇衰落](../Page/裸子植物.md "wikilink")。晚白堊紀後除個別發現外，銀杏科以外的銀杏類植物已基本絕跡。晚白堊紀和古近紀，銀杏（主要為銀杏屬Ginkgo
和似銀杏屬Ginkgoites）在歐亞大陸和北美高緯度地區呈環北極分佈，[漸新世時由於寒冷氣候不斷向南遷徙](../Page/漸新世.md "wikilink")，並在此之後不斷衰落。銀杏在[中新世末在](../Page/中新世.md "wikilink")[北美消失](../Page/北美.md "wikilink")，[上新世晚期在](../Page/上新世.md "wikilink")[歐洲消失](../Page/歐洲.md "wikilink")。250多萬年前發生[第四紀冰河時期](../Page/第四紀冰河時期.md "wikilink")，令銀杏數量繼續減少，面臨[絕滅的危機](../Page/絕滅.md "wikilink")，而中國南部因地理位置適合和氣候溫和，成為銀杏的最後棲息地。\[2\]中國的銀杏大化石紀錄始於[始新世](../Page/始新世.md "wikilink")；日本直至[上新世](../Page/上新世.md "wikilink")，甚至[更新世早期都有銀杏葉化石發現](../Page/更新世.md "wikilink")，但沒有發現繁殖器官。而现在的银杏是这个门的植物中生存至今的唯一成员，因此又被称为“[活化石（孑遗植物）](../Page/活化石.md "wikilink")”。\[3\]

## 歷史

[中国人种植银杏历史悠久](../Page/中国.md "wikilink")，在[邳州發現一棵千年古銀杏](../Page/邳州.md "wikilink")，千年銀杏樹植於[北魏](../Page/北魏.md "wikilink")[正光年間](../Page/正光.md "wikilink")，已有近1500年的歷史，被稱為徐州最古老的銀杏樹。\[4\]。因为[佛教认为银杏是圣树](../Page/佛教.md "wikilink")，所以[僧侣们在庙裡的栽种](../Page/僧侣.md "wikilink")，而且养护上千年。[欧洲人在](../Page/欧洲.md "wikilink")1691年第一次见到银杏种在一个日本佛庙花园。因为银杏在[儒学和](../Page/儒学.md "wikilink")[佛教都很有名](../Page/佛教.md "wikilink")，日本和朝鲜也有很多人种植银杏。1712年，一个德国医生把它引种到欧洲。虽然银杏到处都有，直到近代才在中国[浙江省发现野生银杏](../Page/浙江省.md "wikilink")。但是西方有人认为这些银杏可能不是野生的，而是一千多年前的僧侣种植的。

银杏树一般寿命很长，有“千年银杏”之称。树可以长到很高大，[贵州李家湾有一棵](../Page/贵州.md "wikilink")40米高，在[甘肃还有一棵](../Page/甘肃.md "wikilink")60米高。[山东莒县莒县浮来山下](../Page/山东莒县.md "wikilink")，有一棵树龄达三千多年的银杏树。传说这棵银杏树是西周初期周公东征时所栽。史载周公东征曾到过“龟蒙”，那么踏足浮来山也就有几分可信。这棵银杏树生命力极强，至今仍枝叶茂盛，当代书法家曹宇寬先生挥毫为之写下了“天下银杏第一树”的题字。

銀杏沒有與它相近的物種，與它最接近的現存植物是與銀杏為姊妹群的[蘇鐵門](../Page/蘇鐵門.md "wikilink")。\[5\]
\[6\]

### 銀杏及已絕滅的同屬物種对比

<File:Ginkgo> yimaensis.jpg|†[義馬銀杏](../Page/義馬銀杏.md "wikilink") *Ginkgo
yimaensis*\[7\] <File:Ginkgo> apodes.jpg|†*Ginkgo apodes*\[8\]
<File:Ginkgo> adiantoides - G. cranii.jpg|†*Ginkgo adiantoides* 或
†*Ginkgo cranei*\[9\] <File:Ginkgo> biloba (new form).jpg|銀杏 *Ginkgo
biloba*\[10\]

### 名称

这种植物在[中文的古称为](../Page/中文.md "wikilink")「银果」，如今通常称为「白果」或「银杏」。「白果」这个名称直接借入[越南语](../Page/越南语.md "wikilink")，依[汉越音发音为](../Page/汉越音.md "wikilink")「」；「银杏」这个名称则借入[朝鲜语和](../Page/朝鲜语.md "wikilink")[日语](../Page/日语.md "wikilink")，根据韓語的汉字音分别读作「」（eunhaeng）和「」（ginnan）。

学名的「Ginkgo」来源于日本民间，[汉字在日语常有多种读音](../Page/汉字.md "wikilink")，而「」也可以发音为「」（ginkyō）。1690年，德国植物学者恩格柏特·坎普法（Engelbert
Kaempfer[1](https://en.wikipedia.org/wiki/Engelbert_Kaempfer)）成为了第一个发现该物种的西方人，在其著作《异域采风记》（Amoenitates
Exoticae，1712年）记录了该物种的发音。
一般认为，他写的「y」被误读成了「g」，而这个误读后来被瑞典植物学家[林奈继承](../Page/林奈.md "wikilink")，并一直沿用至今\[11\]\[12\]。

但也有学者认为，在德语中读作“y”的音通常写作“j”，而坎普法来自德国北部的[莱姆戈](../Page/莱姆戈.md "wikilink")，在当地方言中会用“g”代替“j”\[13\]。
还有学者认为，坎普法的一位助手（，Genemon Imamura
Eisei，也作Ichibei）来自长崎，而“kgo”这一表记，刚好准确地展示了当时（17世纪末）长崎地区的日语方言读音\[14\]。

## 形态

[GinkgoLeaves.jpg](https://zh.wikipedia.org/wiki/File:GinkgoLeaves.jpg "fig:GinkgoLeaves.jpg")
[Ginkgo_Biloba_Leaves_-_Black_Background.jpg](https://zh.wikipedia.org/wiki/File:Ginkgo_Biloba_Leaves_-_Black_Background.jpg "fig:Ginkgo_Biloba_Leaves_-_Black_Background.jpg")
[Ginkgo_biloba_MHNT.BOT.2010.13.1.jpg](https://zh.wikipedia.org/wiki/File:Ginkgo_biloba_MHNT.BOT.2010.13.1.jpg "fig:Ginkgo_biloba_MHNT.BOT.2010.13.1.jpg")
银杏树为[裸子植物中唯一的中型闊葉](../Page/裸子植物.md "wikilink")[落叶](../Page/落叶植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")，可以长到25－40米高，胸径可达4米，幼树的树皮比较平滑，呈浅灰色，大树树皮呈灰褐色，表面有不规则纵裂，有长枝与生长缓慢的锯状短枝。有着较为消瘦的树冠，枝杈有些不规则。一年生枝為淡褐黃色，二年生枝粗短，為灰色，短枝黑灰色，有細縱裂紋。冬芽為黃褐色，多為卵圓形，先端鈍尖。\[15\]

银杏叶子在种子植物中很特别，是[裸子植物中唯一一種闊葉落葉喬木](../Page/裸子植物.md "wikilink")，叶子是扇形，呈二分裂或全緣，叶脉和叶子平行，無中脈。在一年生枝上，葉螺旋狀散生，在短枝上3至8片葉呈簇生狀。

成年銀杏的扇形葉片主要有全緣、二分裂或多裂形態，但銀杏幼株的葉片多數為二分裂。

雄球花4至6枚，花藥黃綠色，花粉球形。萌發時產生具兩個纖毛的游動精子。雌球花有長梗，梗端分為二叉，少有3至5叉或不分叉。

银杏具有一定观赏价值。因其枝条平直，树冠呈较规整的[圆锥形](../Page/圆锥.md "wikilink")，大量种植的银杏林在视觉效果上具有整体美感。银杏叶在秋季会变成金黄色，在秋季低角度阳光的照射下比较美观，常被摄影者用作背景。

银杏为[裸子植物](../Page/裸子植物.md "wikilink")，只有种子的构造，尚未演化出[被子植物的](../Page/被子植物.md "wikilink")[果实](../Page/果实.md "wikilink")，但银杏种子的种皮发达，看起来与被子植物的果实相似。银杏种子是1.5-2厘米，包在2-3厘米的棕黄色的种皮裡。银杏的种子稱為白果，有点像杏子，因为含有很多[丁酸](../Page/丁酸.md "wikilink")，闻起来像是腐败的[奶油](../Page/奶油.md "wikilink")。也有人对果浆中的成分过敏，发痒长水泡，洗果子的时候需要戴手套。种子剥出烧熟可以吃，是[中国和](../Page/中国.md "wikilink")[日本的传统](../Page/日本.md "wikilink")[食物](../Page/食物.md "wikilink")。
[Ginkgo_fruit.jpg](https://zh.wikipedia.org/wiki/File:Ginkgo_fruit.jpg "fig:Ginkgo_fruit.jpg")

## 變種

  - 垂枝銀杏（*Ginkgo Biloba* 'Pendula'）枝條下垂。
  - 塔形銀杏（*Ginkgo Biloba* 'Fastigiata'）枝向上伸，形成圓柱形或尖塔形的樹冠。
  - 斑葉銀杏（*Ginkgo Biloba* 'Variegata'）葉有黃斑。
  - 黃葉銀杏（*Ginkgo Biloba* 'Aurea'）葉為偏黃色。
  - 裂葉銀杏（*Ginkgo Biloba* 'Laciniata'）葉較大，有深裂。
  - 葉籽銀杏（*Ginkgo Biloba* 'Epiphylla'）部分種子著生在葉片上，種柄和葉柄合生，種子小而形狀多變。\[16\]

## 繁殖

银杏是雌雄异株，但有极少的银杏是雌雄同株。銀杏花期3月下旬至4月中旬，種子9至10月成熟。\[17\]繁殖方式有[种子繁殖和](../Page/种子.md "wikilink")[扦插繁殖](../Page/扦插.md "wikilink")，种子繁殖很容易成活，扦插繁殖可以用银杏树根部的小芽进行。
種子11月成熟時採收；將採得的種子，堆放地上或浸在水中，使外種皮腐爛或直接除去肉質外種皮，洗淨，曬乾。\[18\]种子繁殖在溫暖的條件下很快會發芽，在低溫條件下發芽慢。雖然銀杏種子的抗寒力很強，但過低的溫度對銀杏種子的發芽不利。播種時需保持泥土濕潤，不要積水也不要過乾，以及保持較高濕度。破開堅硬內種皮可加速銀杏種子發芽。

而扦插繁殖一般在春季3月中下旬扦插，選擇20年生以下幼樹上1至3年生枝條作穗條。枝齡越大，生根率越低，實生樹枝條的生根率高於嫁接樹枝條的生根率。取得枝條後，將枝條剪成15至20厘米長，含3個以上飽滿芽，剪好的插條上端為平口，下端為斜口。之後浸泡在100ppm的[1-萘乙酸溶液中](../Page/1-萘乙酸.md "wikilink")1小時，下端浸入5至7厘米。而秋冬季採集的枝條，則要進行沙藏越冬待翌年春天扦插。扦插期間濕度控制在85至90%，需遮蔭避免陽光直接照射。\[19\]

<File:Ginkgo> biloba male flower.jpg|雄球花 <File:Ginkgo> biloba female
flower.jpg|雌性[胚珠](../Page/胚珠.md "wikilink")

## 種植和用途

[Ginkgo_Seed.JPG](https://zh.wikipedia.org/wiki/File:Ginkgo_Seed.JPG "fig:Ginkgo_Seed.JPG")
[Ginkgo_and_coconut_dessert.jpg](https://zh.wikipedia.org/wiki/File:Ginkgo_and_coconut_dessert.jpg "fig:Ginkgo_and_coconut_dessert.jpg")

[Ginkgo_tree_with_yellow_leaf_at_Akihabara,_Tokyo-P1000750.JPG](https://zh.wikipedia.org/wiki/File:Ginkgo_tree_with_yellow_leaf_at_Akihabara,_Tokyo-P1000750.JPG "fig:Ginkgo_tree_with_yellow_leaf_at_Akihabara,_Tokyo-P1000750.JPG")

### 種植

銀杏喜光照，不能生長在陰暗的地方，但幼苗儘量不要暴曬，否則可能會曬傷葉子。銀杏喜歡排水良好的酸性、中性和鹼性乾燥或潮濕的土壤，能容忍[空氣污染](../Page/空氣污染.md "wikilink")，亦能忍受[乾旱](../Page/乾旱.md "wikilink")，遇到長期高溫高熱環境下會出現「假死」現象，葉會枯萎落下並停上生長，以減少水份散失，當環境合適時會重新萌發新葉。\[20\]
\[21\]在輕度水分脅迫下（-0.3MPa），銀杏樹能正常生長；在中度水分脅迫下（-0．9～-0．5MPa），銀杏樹能維持健康1至2個月，2個月後大部分葉子枯黃；在嚴重水分脅迫下（-2．0MPa），銀杏樹能維持18日，以後干枯死亡或休眠。\[22\]銀杏[秋](../Page/秋.md "wikilink")[冬季氣溫轉冷時進入休眠期並且落葉](../Page/冬季.md "wikilink")，此時減少澆水，直到[春季萌芽後才作正常水份管理](../Page/春季.md "wikilink")。

银杏树能抵抗城市污染，别的树长不活的地方也能长，用来作用城市环美很合适，因此中國及日本都常用銀杏作為[行道樹](../Page/行道樹.md "wikilink")。银杏也可以栽成[盆景作摆设](../Page/盆景.md "wikilink")。以种子繁殖银杏需要20-30年才会结果，故称“公孙树”，是说祖父种的树，到孙子才能收获。

銀杏是適應性極為廣泛，抗逆力十分強大的樹種，而且不容易受害蟲或病菌感染。銀杏十分耐寒、耐熱及較為抗旱，因此很多地方都能栽種。在中國，北至[黑龍江](../Page/黑龍江.md "wikilink")，南達[海南島均能栽種](../Page/海南島.md "wikilink")，在年平均溫度在8至20℃範圍內都適合銀杏生長，但以16℃最為適合。大多數銀杏生產地區的年平均溫度都在14至18℃之間。銀杏可忍受極端最低氣溫為零下23.4度以上，極端最高氣溫在41度以下。\[23\]
\[24\]

### 食用

銀杏種子可以食用，在中國被稱為白果，白果去殼後可煮熟直接食用，製作糖水配料等。

## 醫學

銀杏能改善血管血液循環，也是一種[抗氧化劑](../Page/抗氧化劑.md "wikilink")，可以改善靜脈和眼睛的健康。雖然不是所有的研究都同意銀杏可以治療[阿兹海默病和間歇性跛行](../Page/阿兹海默病.md "wikilink")，或腿部的血液循環不良，但它可以保持老年人的記憶。銀杏葉有兩種類型的化學物質（[黃酮類和](../Page/黃酮類.md "wikilink")[萜類化合物](../Page/萜類化合物.md "wikilink")）都是抗氧化劑，在體內有助清除有害物質及[自由基](../Page/自由基.md "wikilink")，減少[DNA的破壞和保護](../Page/DNA.md "wikilink")[細胞](../Page/細胞.md "wikilink")。
但要注意的是在少數情況下或者有副作用，例如胃部不適，頭痛，皮膚反應，頭暈。在服用銀杏之前，應諮詢醫生的意見。
而患有[癲癇症的人不宜服用銀杏](../Page/癲癇症.md "wikilink")，因為它可能會導致癲癇發作。孕婦及哺乳期的婦女不宜服用。有[糖尿病的人](../Page/糖尿病.md "wikilink")，應諮詢醫生的意見，然後才服用銀杏。\[25\]

以[中醫角度來說](../Page/中醫.md "wikilink")，据《[本草纲目](../Page/本草纲目.md "wikilink")》记载：「**白果**小苦微甘，性溫有小毒，多食令人腹脹」；银杏的果实内含小量[氢氰酸毒素](../Page/氢氰酸.md "wikilink")，[性温](../Page/温_\(中醫\).md "wikilink")，多食令人腹胀，遇热毒性减少，所以生吃或大量進食易引起中毒；多见于小儿；有呕吐、精神萎靡、发热、抽搐等徵状。又說“熟食温肺、益气、定喘嗽、缩小便，止白浊，生食降痰，消毒杀虫，嚼浆涂鼻面手足，去鼻疽疱黑干黯皴皱及疥癣疳虫阴虱”。\[26\]而銀杏的種籽，即果仁有暖肺、止喘嗽及減少痰量之功效。特別是對於[哮喘](../Page/哮喘.md "wikilink")、慢性氣管及[支氣管炎及](../Page/支氣管炎.md "wikilink")[肺結核等病症有明顯的療效](../Page/肺結核.md "wikilink")。而且對補助[泌尿系統有好處](../Page/泌尿系統.md "wikilink")，滋陰益腎，可改善尿頻。\[27\]

## 代表物

銀杏葉的外型亦成為[日本](../Page/日本.md "wikilink")[東京都及](../Page/東京都.md "wikilink")[東京大學的標誌](../Page/東京大學.md "wikilink")。銀杏亦成為中國[成都市](../Page/成都市.md "wikilink")、[丹東市](../Page/丹東市.md "wikilink")、[湖州市及](../Page/湖州市.md "wikilink")[臨沂市的](../Page/臨沂市.md "wikilink")[市樹](../Page/市樹.md "wikilink")。

### 廣島

銀杏生命十分強韌，1945年8月6日[廣島原爆後一個月](../Page/廣島原爆.md "wikilink")，人員在原爆點只有介乎1130米至2160米範圍共發現六棵仍然生存的銀杏樹，並長出新芽。而這六棵銀杏仍然生存至今。因此銀杏被當地人視為希望的象徵。
\[28\]

### 成都

[成都市的](../Page/成都.md "wikilink")[市树就是银杏](../Page/市树.md "wikilink")，该市的大小街道上皆是银杏，出租车涂装也以银杏为主题。\[29\]

### 首爾

2014年1月，[南韓](../Page/南韓.md "wikilink")[首爾市政府將種植在](../Page/首爾市.md "wikilink")[成均館](../Page/成均館.md "wikilink")[大成殿前](../Page/大成殿.md "wikilink")2株樹齡達500歲的銀杏樹，列為首爾市紀念物。成均館內共有4株銀杏樹，其中位在明倫堂門前的銀杏，五十年代已被列為天然紀念物，加以保護。最新被列入紀念物的分別在大成殿神三門東西二側的銀杏。首爾市府估計，這二株銀杏應是在[朝鮮中宗](../Page/朝鮮中宗.md "wikilink")26年，即西元1531年所種\[30\]。[成均館大學的校徽以銀杏葉做為象徵](../Page/成均館大學.md "wikilink")。

### 國樹

銀杏現時暫未成為任何國家的[國樹](../Page/國樹.md "wikilink")。1995年，中共全國人大代表曾兩次提出「將銀杏定為國樹」的議案。其後在2005年，國家林業局下屬單位中國林學會在網上啟動公民國樹評選投票。結果在包括銀杏、[水杉](../Page/水杉.md "wikilink")、[珙桐](../Page/珙桐.md "wikilink")、[國槐](../Page/國槐.md "wikilink")、[杜仲](../Page/杜仲.md "wikilink")、[側柏](../Page/側柏.md "wikilink")、[樟樹](../Page/樟樹.md "wikilink")7大候選樹種中，銀杏的得票率高居榜首。但銀杏一直未能獲得中國官方認可而成為國樹。\[31\]
\[32\]

## 其他

### 银杏之乡

中国江苏省[泰兴市被许多人称为](../Page/泰兴市.md "wikilink")“银杏之乡”。泰兴土壤肥沃，雨量充沛，四季分明，物产丰富，当地盛产银杏。1996年中国[国务院发展研究中心授予泰兴市](../Page/国务院.md "wikilink")“中国银杏之乡”称号，2000年被中国国家林业局命名为首批“中国名特优经济林银杏之乡”。泰兴银杏以产量高、品质好而闻名，全市共定植银杏451.2万株，其中挂果树29.81万株，白果常年产量在3000吨左右，占中国总产量的三分之一以上。

除泰兴外，江苏省邳州市于2000年3月，被国家林业局首批命名为“中国名特优经济林——银杏之乡”。

#### 银杏树交易市场

中国比较集中的银杏树交易市场主要位于江苏省邳州市，山东省郯城县，江苏省泰兴市。[](http://www.slyxs.com/news_type.asp?id=880)

### 李鸿章树

[清朝](../Page/清朝.md "wikilink")，[李鸿章在凭吊](../Page/李鸿章.md "wikilink")[格兰特墓时](../Page/格兰特墓.md "wikilink")，委托时任驻美公使[杨儒代为种树](../Page/杨儒.md "wikilink")。后杨儒代李鸿章所种之树即选用了银杏这一中国特有树种。\[33\]

## 參考文献

  -
## 外部链接

  - [THE GINKGO PAGES](http://kwanten.home.xs4all.nl/history.htm)

  - [加州大学古生物博物馆](http://www.ucmp.berkeley.edu/seedplants/ginkgoales/ginkgo.html)

  - [银杏博物馆，德国 Weimar](http://www.Planet-Weimar.de)

  - [銀杏
    Yinxin](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00017)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [銀杏葉
    Yinxingye](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00337)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [白果
    Baiguo](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00153)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [白果 Bai
    Guo](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00595)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

  - [莽草酸 Shikimic
    acid](https://web.archive.org/web/20150518221438/http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code%3DP00045)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [銀杏內酯
    Bilobalide](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00275)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [銀杏內酯A Ginkgolide
    A](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00211)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [銀杏內酯B Ginkgolide
    B](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00212)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [銀杏內酯C Ginkgolide
    C](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00235)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [Ginkgoales: Fossil
    Record](http://www.ucmp.berkeley.edu/seedplants/ginkgoales/ginkgofr.html)
    銀杏：化石記錄

{{-}}

[樹](../Category/藥用植物.md "wikilink")
[Category:銀杏科](../Category/銀杏科.md "wikilink")
[Category:银杏属](../Category/银杏属.md "wikilink")
[Category:孑遗植物](../Category/孑遗植物.md "wikilink")
[Category:浙江植物](../Category/浙江植物.md "wikilink")
[Category:中國特有種植物](../Category/中國特有種植物.md "wikilink")
[Category:中国国家一级保护植物](../Category/中国国家一级保护植物.md "wikilink")
[Category:雌雄異株植物](../Category/雌雄異株植物.md "wikilink")
[Category:觀賞植物](../Category/觀賞植物.md "wikilink")
[Category:行道樹](../Category/行道樹.md "wikilink")
[Category:活化石](../Category/活化石.md "wikilink")
[Category:植物保護](../Category/植物保護.md "wikilink")
[Category:樹](../Category/樹.md "wikilink")
[Category:落葉植物](../Category/落葉植物.md "wikilink")
[Category:單種屬植物](../Category/單種屬植物.md "wikilink")

1.  [Ginkgo biloba
    L.](http://www.eflora.cn/spzwz/Ginkgo%20biloba)1978《中國植物誌》第7卷018頁

2.  [樹木活化石、長壽樹
    銀杏](http://www.forest.gov.tw/public/Attachment/512211621771.pdf)


3.  刘秀群，《辽西中生代银杏目和茨康目植物生殖器官研究》，中国科学院研究生院博士学位论文。2005年。

4.  [徐州最古老銀杏樹植於北魏正光年間 -
    人民网江苏视窗](http://js.people.com.cn/html/2013/03/29/217190.html)

5.  [學者印證
    銀杏蘇鐵為姊妹群-台灣立報](http://www.lihpao.com/?action-viewnews-itemid-126984)

6.  [植物遺傳演化學者再度驗證　「活化石」銀杏與蘇鐵植物的姊妹群關係-中央研究院](http://www.sinica.edu.tw/manage/gatenews/showsingle.php?_op=?rid:5691)

7.  Approximate reconstructions by B. M. Begović Bego and Z. Zhou,
    2010/2011. Source: B.M. Begović Bego, (2011). Nature's Miracle
    *Ginkgo biloba*, Book 1, Vols. 1–2, pp. 60–61.

8.
9.
10.
11. [日本九州大学语言文化学院](http://www.flc.kyushu-u.ac.jp/~michel/serv/ek/amoenitates/ginkgo/ginkgo.html)

12. [2](http://catalog.lib.kyushu-u.ac.jp/handle/2324/2898/Ginkgo_biloba2_revised_2011.pdf)

13. [3](http://chinesesites.library.ingentaconnect.com/content/iapt/tax/2015/00000064/00000001/art00011)

14. [4](http://chinesesites.library.ingentaconnect.com/content/iapt/tax/2015/00000064/00000001/art00011)

15. [銀杏科銀杏屬](http://www.bhl-china.org/data/index.php?pid=175@338)浙江植物志,v1
    338

16. [Ginkgo biloba L.](http://www.eflora.cn/sp/Ginkgo%20biloba)eFlora
    中国在线植物志

17. [21-銀杏](http://www.eflora.cn/spgdzw/Ginkgo%20biloba)2000 《中國高等植物》
    第3卷

18. [中國經濟植物誌-10. 銀杏 ( yinxing
    )](http://www.eflora.cn/spjjzw/Ginkgo%20biloba)《中國經濟植物誌》第448 頁

19. [銀杏扦插繁殖技術及管理方法](http://wenku.baidu.com/view/564b3e0a581b6bd97f19eaa0.html)

20. [Ginkgo biloba L. Plants For A
    Future](http://www.pfaf.org/user/plant.aspx?LatinName=Ginkgo+biloba)

21. [天天高溫不耐旱的銀杏扛不牢了杭州很多地方銀杏樹葉枯黃大家很心痛](http://hzdaily.hangzhou.com.cn/dskb/html/2013-07/23/content_1541209.htm)

22. [銀杏樹的耐旱性-中國銀杏網](http://www.zgyx.com/news_type.asp?id=15177)

23. [银杏的生态学特性](http://www.yinxing668.com/news_type.asp?id=882)

24. [土壤、氣候溫度對銀杏苗木生長有什麼影響](http://www.kyyxm.com/news_type.asp?id=1043)

25. [馬里蘭大學醫學中心-銀杏](http://umm.edu/health/medical/altmed/herb/ginkgo-biloba)

26. [癌症患者食物參考（銀杏篇）](http://hd.stheadline.com/living/living_content.asp?contid=147687&srctype=g)

27. [健康之寶-銀杏樹](http://www.3phk.com/v5article2.asp?id=2422&folder=hot_topics&section=healthinfo&issue=)《醫．藥．人》
    第 122 期 撰文：吳澤森

28. [A-bombed Ginkgo trees in Hiroshima,
    Japan](http://kwanten.home.xs4all.nl/hiroshima.htm)

29. [成都的市树和市花](http://torchrelay.beijing2008.cn/cn/journey/chengdu/chengdunews/n214275078.shtml)


30. [2株500歲銀杏　列首爾紀念物-蘋果日報 2014年01月18日](http://www.appledaily.com.tw/realtimenews/article/international/20140118/329214/2%E6%A0%AA500%E6%AD%B2%E9%8A%80%E6%9D%8F%E3%80%80%E5%88%97%E9%A6%96%E7%88%BE%E7%B4%80%E5%BF%B5%E7%89%A9)

31. [銀杏能否成為國樹?-人民網](http://scitech.people.com.cn/GB/25895/3605324.html)

32. [大陸專家建議將銀杏定為國樹](http://www.epochtimes.com/b5/1/8/29/c4453.htm)

33.