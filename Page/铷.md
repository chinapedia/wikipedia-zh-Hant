**銣**是一種[化學元素](../Page/化學元素.md "wikilink")，符號為**Rb**，[原子序数為](../Page/原子序数.md "wikilink")37。銣是種質軟、呈銀白色的[金屬](../Page/金屬.md "wikilink")，屬於[鹼金屬](../Page/鹼金屬.md "wikilink")，[原子量為](../Page/原子量.md "wikilink")85.4678。單質銣的反應性極高，其性質與其他鹼金屬相似，例如會在空氣中快速[氧化](../Page/氧化.md "wikilink")。自然出現的銣元素由兩種[同位素組成](../Page/同位素.md "wikilink")：<sup>85</sup>Rb是唯一一種穩定同位素，佔72%；<sup>87</sup>Rb具微[放射性](../Page/放射性.md "wikilink")，佔28%，其[半衰期為](../Page/半衰期.md "wikilink")490億年，超過[宇宙年齡的三倍](../Page/宇宙年齡.md "wikilink")。

德國化學家[羅伯特·威廉·本生和](../Page/羅伯特·威廉·本生.md "wikilink")[古斯塔夫·基爾霍夫於](../Page/古斯塔夫·基爾霍夫.md "wikilink")1861年利用當時的新技術[火焰光譜法發現了銣元素](../Page/原子發射光譜法.md "wikilink")。

銣化合物有一些化學和電子上的應用。銣金屬能夠輕易氣化，而且它有特殊的吸收光譜範圍，所以常被用在[原子的](../Page/原子.md "wikilink")[激光操控技術上](../Page/激光.md "wikilink")。

銣並沒有已知的生物功用。但生物體對銣[離子的處理機制和](../Page/離子.md "wikilink")[鉀離子相似](../Page/鉀.md "wikilink")，因此銣離子會被主動運輸到植物和動物[細胞中](../Page/細胞.md "wikilink")。

## 歷史

[Kirchhoff_Bunsen_Roscoe.jpg](https://zh.wikipedia.org/wiki/File:Kirchhoff_Bunsen_Roscoe.jpg "fig:Kirchhoff_Bunsen_Roscoe.jpg")（左）和[羅伯特·威廉·本生](../Page/羅伯特·威廉·本生.md "wikilink")（中）通過光譜法發現了銣元素。\]\]
1861年，[羅伯特·威廉·本生和](../Page/羅伯特·威廉·本生.md "wikilink")[古斯塔夫·基爾霍夫在德國](../Page/古斯塔夫·基爾霍夫.md "wikilink")[海德堡](../Page/海德堡.md "wikilink")，利用[光譜儀在](../Page/光譜儀.md "wikilink")[鋰雲母中發現了銣元素](../Page/鋰雲母.md "wikilink")。由於其[發射光譜呈現出多條鮮明的紅線](../Page/發射光譜.md "wikilink")，所以他們選擇了[拉丁文中意為](../Page/拉丁文.md "wikilink")「深紅色」的「rubidus」一詞為它命名。\[1\]\[2\]

銣是鋰雲母中的一種次要成分。基爾霍夫和本生所處理的150公斤鋰雲母中，只含有0.24%的氧化銣（Rb<sub>2</sub>O）。鉀和銣都會和[氯鉑酸形成不可溶鹽](../Page/氯鉑酸.md "wikilink")，但在熱水中，兩種鹽的可溶性有小許差異。可溶性稍低的六氯鉑酸銣（Rb<sub>2</sub>PtCl<sub>6</sub>）可以經分級結晶的方法取得。用[氫對六氯鉑酸銣進行](../Page/氫.md "wikilink")[還原後](../Page/還原反應.md "wikilink")，基爾霍夫和本生獲得了0.51克的[氯化銣](../Page/氯化銣.md "wikilink")。兩人之後對銣和銫進行的首次大型萃取工序用到了4萬4千升礦物水，並一共提取出7.3克[氯化銫和](../Page/氯化銫.md "wikilink")9.2克氯化銣。\[3\]\[4\]基爾霍夫和本生在發明光譜儀僅僅一年後就發現了銣元素。銣因此成為繼銫以後第二個通過[光譜學方法發現的元素](../Page/光譜學.md "wikilink")。\[5\]

兩人用提取出的氯化銣來估計銣的[原子量](../Page/原子量.md "wikilink")，得出的數值為85.36（目前受認可的數值為85.47）。\[6\]他們試圖對熔融氯化銣進行[電解以取得單質銣](../Page/電解.md "wikilink")，但他們取得了一種藍色的均勻物質，且「無論在肉眼還是顯微鏡下都無法看出絲毫的金屬成分」。他們推測這種物質是[低價氯化銣](../Page/非整比化合物.md "wikilink")（），不過它其實更可能是銣金屬和氯化銣的[膠體狀混合物](../Page/膠體.md "wikilink")。\[7\]之後，本生進行了第二次嘗試，對炭化了的[酒石酸銣加熱](../Page/酒石酸.md "wikilink")，成功還原了銣金屬。儘管蒸餾出的銣會在空氣中[自燃](../Page/自燃.md "wikilink")，但本生仍能夠測量出銣的密度和熔點。1860年代所取得的密度值，與今天認可的數值只相差0.1 g/cm<sup>3</sup>，熔點值的偏差也在1 °C以內。\[8\]

科學家在1908年發現了銣的微放射性，但同位素理論在1910年代才被建立起來，加上放射性銣的半衰期超過10<sup>10</sup>年，所以當時對這一現象的解釋尤為困難。有關銣的衰變方式的爭論一直持續到1940年代末。目前已證實，<sup>87</sup>Rb會經[β衰變成為穩定的](../Page/β衰變.md "wikilink")<sup>87</sup>Sr。\[9\]\[10\]

1920年代以前，銣還沒有工業用途。\[11\]此後，銣的最大應用在於化學和電子範疇的研究和開發。1995年，[埃里克·康奈爾](../Page/埃里克·康奈爾.md "wikilink")、[卡爾·埃德溫·威曼和](../Page/卡爾·埃德溫·威曼.md "wikilink")[沃爾夫岡·克特勒用銣](../Page/沃爾夫岡·克特勒.md "wikilink")-87實現了[玻色–愛因斯坦凝聚](../Page/玻色–愛因斯坦凝聚.md "wikilink")，\[12\]並因此獲得了2001年的[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")。\[13\]

## 性質

銣是一種質軟、[可塑性高的銀白色金屬](../Page/可塑性.md "wikilink")。\[14\]在所有非放射性鹼金屬元素中，銣的[電負性排行第二](../Page/電負性.md "wikilink")。其熔點為39.3
°C。銣金屬會在水中劇烈反應，它會和[汞產生](../Page/汞.md "wikilink")[汞齊](../Page/汞齊.md "wikilink")，並且會和[金](../Page/金.md "wikilink")、[鐵](../Page/鐵.md "wikilink")、[銫](../Page/銫.md "wikilink")、[鈉和](../Page/鈉.md "wikilink")[鉀形成](../Page/鉀.md "wikilink")[合金](../Page/合金.md "wikilink")（但不會和[鋰形成合金](../Page/鋰.md "wikilink")，儘管鋰和銣同屬鹼金屬）。這些屬性都和其他的鹼金屬相似。\[15\]與反應性稍低的鉀和反應性稍高的銫一樣，銣和水所產生的劇烈反應通常足以燃起所釋放出來的[氫氣](../Page/氫氣.md "wikilink")。它也可以在空氣中自燃。\[16\]銣的[電離能很低](../Page/電離能.md "wikilink")，只有406 kJ/mol。\[17\]

### 化合物

[Rb9O2_cluster.png](https://zh.wikipedia.org/wiki/File:Rb9O2_cluster.png "fig:Rb9O2_cluster.png")\]\]
[氯化銣](../Page/氯化銣.md "wikilink")（RbCl）是最常用的銣化合物之一。在生物化學中，它可以用來促使細胞吸取[DNA](../Page/DNA.md "wikilink")。由於生物體內的銣極少，且銣會被活細胞吸收而代替鉀，所以它能用作一種生物標記物。[氫氧化銣](../Page/氫氧化銣.md "wikilink")（RbOH）具有腐蝕性，能作為大部份用到銣的化學反應的初始化合物。其他銣化合物還包括用在某些眼鏡鏡片中的[碳酸銣](../Page/碳酸銣.md "wikilink")（Rb<sub>2</sub>CO<sub>3</sub>），以及硫酸銣銅（Rb<sub>2</sub>SO<sub>4</sub>·CuSO<sub>4</sub>·6H<sub>2</sub>O）等。[碘化銣銀](../Page/碘化銣銀.md "wikilink")（RbAg<sub>4</sub>I<sub>5</sub>）是所有已知[離子晶體中](../Page/離子晶體.md "wikilink")，[室溫](../Page/室溫.md "wikilink")[電導率最高的](../Page/電導率.md "wikilink")。在製造薄膜[電池時可以利用這一屬性](../Page/電池.md "wikilink")。\[18\]\[19\]

銣的[氧化物有若干種](../Page/氧化物.md "wikilink")，包括氧化銣（Rb<sub>2</sub>O）、Rb<sub>6</sub>O和Rb<sub>9</sub>O<sub>2</sub>，后两种低氧化物可以在空气中燃烧。銣暴露在空氣中即會產生這些氧化物。在氧氣過剩的環境下，則會形成[超氧化物](../Page/超氧化物.md "wikilink")（RbO<sub>2</sub>）。銣和[鹵化物形成鹽](../Page/鹵化物.md "wikilink")，例如[氟化銣](../Page/氟化銣.md "wikilink")、[氯化銣](../Page/氯化銣.md "wikilink")、[溴化銣及](../Page/溴化銣.md "wikilink")[碘化銣等](../Page/碘化銣.md "wikilink")。\[20\]

### 同位素

雖然銣是一種[單一同位素元素](../Page/單一同位素元素.md "wikilink")（即只有一種穩定同位素），但自然界中的銣元素卻由兩種同位素組成：穩定的<sup>85</sup>Rb（佔72.2%）以及[放射性同位素](../Page/放射性同位素.md "wikilink")<sup>87</sup>Rb（佔27.8%）。\[21\]因此自然界中的銣具有放射性，比活性約為670
[Bq](../Page/貝克勒爾.md "wikilink")/g。這樣的輻射水平可以在110天內於照相[底片上留下影像](../Page/底片.md "wikilink")。\[22\]\[23\]除<sup>85</sup>Rb和<sup>87</sup>Rb之外，還有30種非自然生成的同位素。它們具有放射性，半衰期都在3個月以內。\[24\]

銣-87的[半衰期為](../Page/半衰期.md "wikilink")年，這是[宇宙年齡](../Page/宇宙年齡.md "wikilink")年的三倍有餘。\[25\]它是一種[原生核素](../Page/原生核素.md "wikilink")，在地球形成時便已存在。在[礦物中](../Page/礦物.md "wikilink")，銣常會代替[鉀元素的位置](../Page/鉀.md "wikilink")，所以其分佈廣泛。<sup>87</sup>Rb在釋放一個負[β粒子之後](../Page/β粒子.md "wikilink")，會衰變成穩定的[<sup>87</sup>Sr](../Page/鍶.md "wikilink")，這可以用於[測定岩石的年齡](../Page/放射性定年法.md "wikilink")。在地球內部的[分級結晶過程中](../Page/分級結晶.md "wikilink")，鍶會集中在[斜長石中](../Page/斜長石.md "wikilink")，留下處於液態的銣。因此，在殘餘[岩漿中銣對鍶的比例會隨時間增加](../Page/岩漿.md "wikilink")，經[活成分異作用形成銣](../Page/活成分異作用.md "wikilink")/鍶比例較高的岩石。[偉晶岩中的銣](../Page/偉晶岩.md "wikilink")/鍶比例最高（10以上）。如果可以測得或推算出最初的鍶含量，那麼通過測量目前銣和鍶的含量以及<sup>87</sup>Sr/<sup>86</sup>Sr比例，就可以算出該岩石樣本的年齡。這一年齡只有在岩石不曾受變動的情況下才等於真實的年齡值
。\[26\]\[27\]

銣-82是其中一種人造同位素，可經鍶-82的[電子捕獲衰變過程產生](../Page/電子捕獲.md "wikilink")，反應的半衰期為25.36年。銣-82會再經[正電子發射衰變為穩定的](../Page/正電子發射.md "wikilink")[氪](../Page/氪.md "wikilink")-82，半衰期為76秒。\[28\]

### 存量

銣在地球[地殼中的](../Page/地殼.md "wikilink")[豐度在所有元素中](../Page/豐度.md "wikilink")[排第23位](../Page/地球的地殼元素豐度列表.md "wikilink")，與[鋅相近](../Page/鋅.md "wikilink")，比[銅更常見](../Page/銅.md "wikilink")。\[29\]它自然出現在[白榴石](../Page/白榴石.md "wikilink")、[銫榴石](../Page/銫榴石.md "wikilink")、[光鹵石和](../Page/光鹵石.md "wikilink")[鐵鋰雲母等礦物之中](../Page/鐵鋰雲母.md "wikilink")，氧化銣大約佔這些礦物的1%。[鋰雲母中的銣含量在](../Page/鋰雲母.md "wikilink")0.3%和3.5%之間，是銣的主要商業來源。\[30\]某些含[鉀礦物和](../Page/鉀.md "wikilink")[氯化鉀都會含有不少的銣元素](../Page/氯化鉀.md "wikilink")，有商業開採價值。\[31\]

銣在[海水中的濃度平均為](../Page/海水.md "wikilink")125 µg/L。相比之下，鉀的濃度則高得多（408 mg/L），銫則低得多（0.3 µg/L）。\[32\]

由於[離子半徑較大](../Page/離子半徑.md "wikilink")，所以銣屬於所謂的「[不相容成分](../Page/不相容成分.md "wikilink")」。\[33\]在[熔岩結晶過程中](../Page/分級結晶.md "wikilink")，銣和更重的同族元素銫聚集在一起，處於液態，是最後一個結晶的成分。因此，含有銣和銫的最大礦藏，都是經由這種濃縮過程所形成的[偉晶岩礦帶](../Page/偉晶岩.md "wikilink")。由於銣會在結晶時代替[鉀的位置](../Page/鉀.md "wikilink")，所以其濃縮的程度遠低於銫。從含有銫榴石的偉晶岩中可開採出銫，從鋰雲母中可開採出鋰，過程中也會產生銣作副產品。\[34\]

銣的主要礦藏包括：位於加拿大[曼尼托巴省](../Page/曼尼托巴省.md "wikilink")[伯尼克湖的](../Page/伯尼克湖.md "wikilink")[銫榴石礦藏](../Page/銫榴石.md "wikilink")，以及意大利[厄爾巴島上的](../Page/厄爾巴島.md "wikilink")[銣長石](../Page/銣長石.md "wikilink")（(Rb,K)AlSi<sub>3</sub>O<sub>8</sub>）礦藏，其銣含量高達17.5%。\[35\]

## 生產

雖然銣在地殼中比銫更常見，但其應用不廣，加上缺乏一種富含銣的礦物，所以各種銣化合物的年產量只有2至4噸。\[36\]分離鉀、銣和銫的方法有若干種。對銣[礬和銫礬重複進行分級結晶](../Page/礬.md "wikilink")，30次以後便可獲得純銣礬。另外兩種方法分別利用氯錫酸鹽和亞鐵氫化物。\[37\]\[38\]

1950至1960年代，鉀生產過程中一種稱為「Alkarb」的副產品曾經是銣元素的主要來源。Alkarb含21%的銣，其餘大部份是鉀，另有少量銫。\[39\]如今，銣是銫開採過程中的一种副產品，加拿大曼尼托巴省的銫榴石礦就是其中一例。\[40\]

[Die_Flammenfärbung_des_Rubidium.jpg](https://zh.wikipedia.org/wiki/File:Die_Flammenfärbung_des_Rubidium.jpg "fig:Die_Flammenfärbung_des_Rubidium.jpg")

## 應用

[USNO_rubidium_fountain.jpg](https://zh.wikipedia.org/wiki/File:USNO_rubidium_fountain.jpg "fig:USNO_rubidium_fountain.jpg")的銣噴泉[原子鐘](../Page/原子鐘.md "wikilink")。\]\]
銣化合物有時會被添加在[煙花當中](../Page/煙花.md "wikilink")，使它發出紫光。\[41\]銣可以用在[磁流體發動機和](../Page/磁流體力學.md "wikilink")[熱傳導發電機中](../Page/熱傳導發電機.md "wikilink")：高溫下形成的銣離子經過[磁場](../Page/磁場.md "wikilink")，\[42\]作用就像發電機中的[電樞](../Page/電樞.md "wikilink")，因而產生[電流](../Page/電流.md "wikilink")。用它製成的[激光二極體價廉](../Page/激光二極體.md "wikilink")，且激光[波長範圍適宜](../Page/波長.md "wikilink")，維持高[蒸氣壓所需的溫度也在中等範圍內](../Page/蒸氣壓.md "wikilink")，所以銣（特別是<sup>87</sup>Rb）是[激光冷卻和](../Page/激光冷卻.md "wikilink")[玻色–愛因斯坦凝聚應用上最常用的一種原子](../Page/玻色–愛因斯坦凝聚.md "wikilink")。\[43\]\[44\]

科學家曾用銣對[<sup>3</sup>He進行極化](../Page/氦-3.md "wikilink")，這樣產生的<sup>3</sup>He氣體擁有單一方向，而不是隨機方向的核[自旋](../Page/自旋.md "wikilink")。激光對銣氣體進行光抽運，極化了的銣就會通過[超精細交互作用使](../Page/超精細結構.md "wikilink")<sup>3</sup>He極化。\[45\]這樣[自旋極化了的](../Page/自旋極化.md "wikilink")<sup>3</sup>He氣體可以用在[中子極化測量中](../Page/中子.md "wikilink")，或用於製造極化中子作其他用途。\[46\]

[原子鐘的共振元件可以利用銣的能級的](../Page/原子鐘.md "wikilink")[超精細結構](../Page/超精細結構.md "wikilink")，因此銣已被應用在高精度計時上。[全球定位系統](../Page/全球定位系統.md "wikilink")（GPS）常利用銣頻率標準來生成一個比銫頻率標準更精准、成本更低的「主頻率標準」。\[47\]\[48\]這種銣頻率標準在[電信工業中有大規模的生產](../Page/電信.md "wikilink")。\[49\]

銣的其他潛在應用包括：蒸汽[渦輪中的工作流體](../Page/渦輪.md "wikilink")、[真空管中的](../Page/真空管.md "wikilink")[吸氣劑以及](../Page/吸氣劑.md "wikilink")[光度感應器元件等](../Page/光度感應器.md "wikilink")。\[50\]銣是一些特殊玻璃的成分，也可用於製造[超氧化物](../Page/超氧化物.md "wikilink")。它能夠在細胞中代替鉀的位置，所以能被用來研究[離子通道](../Page/離子通道.md "wikilink")。銣氣體還被用於原子[磁強計中](../Page/磁強計.md "wikilink")。\[51\]科學家正在用<sup>87</sup>Rb，連同其他鹼金屬，來開發無自旋交換弛豫（SERF）原子磁強計。\[52\]

銣-82可用於[正電子發射電腦斷層掃描](../Page/正電子發射電腦斷層掃描.md "wikilink")。銣和鉀相似，所以含有大量鉀的生物組織也會積聚具放射性的銣元素。這一原理主要應用在心肌灌注成像。銣-82的半衰期只有76秒，所以必須從靠近病人的鍶-82衰變而得。\[53\]由於[腦腫瘤的](../Page/腦腫瘤.md "wikilink")[血腦屏障有所變異](../Page/血腦屏障.md "wikilink")，所以腫瘤會比正常腦組織更容易積累銣。[核醫學可以利用這一原理對腫瘤進行定位和照相](../Page/核醫學.md "wikilink")。\[54\]

科學家曾做過實驗，以研究銣對患有[躁鬱症和](../Page/躁鬱症.md "wikilink")[抑鬱症的病人有何影響](../Page/抑鬱症.md "wikilink")。\[55\]\[56\][透析治療期間患上抑鬱症的病人體內缺少銣](../Page/透析.md "wikilink")，所以補充銣元素可能可以舒緩抑鬱症。\[57\]在某些試驗中，病人須連續60天攝入720 mg的氯化銣。\[58\]\[59\]

## 安全

銣金屬會和水發生劇烈的反應，甚至會著火；即使放在煤油中，也会缓慢反应，并被溶解的少量氧给氧化。因此，铷一般保存在真空安瓿或充有稀有气体（如氩气）的安瓿中。只要接觸到少許的空氣，包括滲入油中的氧氣，銣就會變成過氧化銣。因此它的安全措施和鉀金屬相似。\[60\]

與鈉和鉀一樣，銣溶在水中的時候幾乎永遠呈+1[氧化態](../Page/氧化態.md "wikilink")，在所有生物體內的銣也一樣。Rb<sup>+</sup>離子在人的體內似乎和鉀離子無異，所以主要積聚在[細胞內液中](../Page/細胞內液.md "wikilink")。\[61\]銣離子並沒有明顯的毒性：一個70公斤重的人體內平均含有0.36克的銣，而在這一數值提升50甚至100倍之後，也沒有對試驗對象造成任何明顯的負面影響。\[62\]銣在人體內的[生物半衰期為](../Page/生物半衰期.md "wikilink")31至46天。\[63\]在老鼠身上進行的實驗指出，如果體內一小部份鉀替換成銣，身體是能夠承受的，但一旦肌肉組織內一半的鉀都換成銣，老鼠便會死亡。\[64\]\[65\]

## 參考資料

## 延伸閱讀

  - Meites, Louis (1963). *Handbook of Analytical Chemistry* (New York:
    McGraw-Hill Book Company, 1963)

  -
## 外部連結

  - [Rubidium](http://www.periodicvideos.com/videos/037.htm) at *The
    Periodic Table of Videos*（諾丁漢大學）

[5](../Category/化学元素.md "wikilink")
[Category:碱金属](../Category/碱金属.md "wikilink")
[\*](../Category/铷.md "wikilink")
[Category:第5周期元素](../Category/第5周期元素.md "wikilink")
[Category:还原剂](../Category/还原剂.md "wikilink")

1.

2.

3.
4.
5.

6.
7.

8.

9.

10.

11.

12.

13.

14.
15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.
29.

30.

31.

32.

33.

34.
35.

36.
37.
38.

39.

40.
41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.
53.

54.

55.
56.

57.

58.

59.

60.

61.

62.

63.

64.

65.