**銻化鋁**（Aluminium
antimonide）是一種[半導體材料](../Page/半導體.md "wikilink")，為III-V
族，其中分子裡包含了[銻和](../Page/銻.md "wikilink")[鋁](../Page/鋁.md "wikilink")，而它的[晶格常數為](../Page/晶格常數.md "wikilink")0.61 nm，[能隙在](../Page/能隙.md "wikilink")300K時是1.6
[eV](../Page/電子伏特.md "wikilink")，至於它的[直接能隙有](../Page/直接能隙.md "wikilink")2.22[eV](../Page/電子伏特.md "wikilink")。

## 其他化合物

  - [銻化鎵](../Page/銻化鎵.md "wikilink")
  - [銻化铟](../Page/銻化铟.md "wikilink")
  - [砷化鋁](../Page/砷化鋁.md "wikilink")

## 参考资料

[Category:銻化物](../Category/銻化物.md "wikilink")
[Category:铝化合物](../Category/铝化合物.md "wikilink")
[Category:III-V半導體材料](../Category/III-V半導體材料.md "wikilink")