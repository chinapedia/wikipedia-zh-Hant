**锘**是一种人工获得的[放射性元素](../Page/放射性元素.md "wikilink")(1957年)，它的[化学符号是](../Page/化学符号.md "wikilink")**No**，它的[原子序数是](../Page/原子序数.md "wikilink")102，属于[锕系元素之一](../Page/锕系元素.md "wikilink")。

锘的拼音名称是以[瑞典](../Page/瑞典.md "wikilink")[化学家](../Page/化学家.md "wikilink")[阿尔弗雷德·诺贝尔而命名](../Page/阿尔弗雷德·诺贝尔.md "wikilink")。他亦是创建[诺贝尔奖的人](../Page/诺贝尔奖.md "wikilink")。

**锘-261**是最稳定的[同位素](../Page/同位素.md "wikilink")，[半衰期有](../Page/半衰期.md "wikilink")170[分钟](../Page/分钟.md "wikilink")。其次是**锘-259**，[半衰期有](../Page/半衰期.md "wikilink")58[分钟](../Page/分钟.md "wikilink")。

锘-254的半衰期是55秒。

[Category:锕系元素](../Category/锕系元素.md "wikilink")
[Category:人工合成元素](../Category/人工合成元素.md "wikilink")
[7P](../Category/第7周期元素.md "wikilink")
[7P](../Category/化学元素.md "wikilink")