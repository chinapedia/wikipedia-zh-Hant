**锡林郭勒盟**-{（，意思是“高原上的河流”）}-是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[内蒙古自治区下辖的](../Page/内蒙古自治区.md "wikilink")[盟](../Page/盟.md "wikilink")，位于内蒙古自治区中部偏东。辖境东接[兴安盟](../Page/兴安盟.md "wikilink")、[通辽市](../Page/通辽市.md "wikilink")、[赤峰市](../Page/赤峰市.md "wikilink")，南邻[河北省](../Page/河北省.md "wikilink")[承德市](../Page/承德市.md "wikilink")、[张家口市](../Page/张家口市.md "wikilink")，西界[乌兰察布市](../Page/乌兰察布市.md "wikilink")，北与[蒙古国接壤](../Page/蒙古国.md "wikilink")。地处[内蒙古高原中部](../Page/内蒙古高原.md "wikilink")，地势南高北低，盟境东缘为[大兴安岭西麓](../Page/大兴安岭.md "wikilink")，中部为[乌拉盖盆地](../Page/乌拉盖盆地.md "wikilink")，西部为[乌兰察布高原东段](../Page/乌兰察布高原.md "wikilink")。境内河流多为季节性[内陆河](../Page/内陆河.md "wikilink")，主要河流有[乌拉盖河和](../Page/乌拉盖河.md "wikilink")[锡林河](../Page/锡林河.md "wikilink")。全盟总面积19.99万平方公里，人口104.26万，[蒙古族人口比例约三成](../Page/蒙古族.md "wikilink")，盟行政公署驻[锡林浩特市](../Page/锡林浩特市.md "wikilink")。锡林郭勒盟是全区重要畜产品生产基地，草原辽阔，牧马产业发达，有“中国马都”之称，位于辖境西北部边境的[二连浩特是中蒙边境的重要口岸](../Page/二连浩特.md "wikilink")。

## 歷史

[1965-01_锡林郭勒盟草原.jpg](https://zh.wikipedia.org/wiki/File:1965-01_锡林郭勒盟草原.jpg "fig:1965-01_锡林郭勒盟草原.jpg")
[清时各旗会盟于](../Page/清.md "wikilink")[锡林郭勒河而得名](../Page/锡林郭勒河.md "wikilink")，屬[漠南蒙古](../Page/漠南蒙古.md "wikilink")；[民國後曾與](../Page/民國.md "wikilink")[察哈爾盟共屬](../Page/察哈爾盟.md "wikilink")[察哈爾省](../Page/察哈爾省.md "wikilink")。

[抗戰期間曾為日軍佔領](../Page/抗戰.md "wikilink")，並為日人扶植的[德王](../Page/德王.md "wikilink")[蒙疆政府統治](../Page/蒙疆.md "wikilink")，[中国共產黨曾設](../Page/中国共產黨.md "wikilink")[晉察冀解放區統治](../Page/晉察冀解放區.md "wikilink")。

[中华人民共和国成立後](../Page/中华人民共和国.md "wikilink")，撤銷察哈爾省，劃入[內蒙古自治區至今](../Page/內蒙古自治區.md "wikilink")。

## 地理

地形为[高原](../Page/高原.md "wikilink")，海拔1000米左右。南部较高，草原广阔，北部主要为[戈壁](../Page/戈壁.md "wikilink")。[温带半干旱气候](../Page/温带半干旱气候.md "wikilink")，冬冷夏热，降水较少。

## 畜牧资源

境内大部为畜牧区，盛产马、牛、骆、驼、绵羊、山羊，以产**乌珠穆沁马**、**乌珠穆沁肥尾羊**驰名。

## 政治

### 现任领导

<table>
<caption>锡林郭勒盟四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党锡林郭勒盟委员会.md" title="wikilink">中国共产党<br />
锡林郭勒盟委员会</a><br />
<br />
书记</p></th>
<th><p><br />
<a href="../Page/内蒙古自治区人民代表大会.md" title="wikilink">内蒙古自治区人民代表大会</a><br />
常务委员会<br />
锡林郭勒盟工作委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/锡林郭勒盟行政公署.md" title="wikilink">锡林郭勒盟行政公署</a><br />
<br />
<br />
盟长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议锡林郭勒盟委员会.md" title="wikilink">中国人民政治协商会议<br />
锡林郭勒盟委员会</a><br />
<br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/罗虎在.md" title="wikilink">罗虎在</a>[1]</p></td>
<td><p><a href="../Page/武国栋_(1963年).md" title="wikilink">武国栋</a>[2]</p></td>
<td><p><a href="../Page/霍照良.md" title="wikilink">霍照良</a>[3]</p></td>
<td><p><a href="../Page/其其格.md" title="wikilink">其其格</a>（女）[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p><a href="../Page/蒙古族.md" title="wikilink">蒙古族</a></p></td>
<td><p>蒙古族</p></td>
<td><p>蒙古族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/内蒙古自治区.md" title="wikilink">内蒙古自治区</a><a href="../Page/卓资县.md" title="wikilink">卓资县</a></p></td>
<td><p>内蒙古自治区<a href="../Page/察右后旗.md" title="wikilink">察右后旗</a></p></td>
<td><p><a href="../Page/辽宁省.md" title="wikilink">辽宁省</a><a href="../Page/阜新市.md" title="wikilink">阜新市</a></p></td>
<td><p>内蒙古自治区<a href="../Page/赤峰市.md" title="wikilink">赤峰市</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年3月</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年7月</p></td>
<td><p>2013年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖2个[县级市](../Page/县级市.md "wikilink")、1个[县](../Page/县_\(中华人民共和国\).md "wikilink")、9个[旗](../Page/旗_\(行政区划\).md "wikilink")。

  - 县级市：[二连浩特市](../Page/二连浩特市.md "wikilink")、[锡林浩特市](../Page/锡林浩特市.md "wikilink")
  - 县：[多伦县](../Page/多伦县.md "wikilink")
  - 旗：[阿巴嘎旗](../Page/阿巴嘎旗.md "wikilink")、[苏尼特左旗](../Page/苏尼特左旗.md "wikilink")、[苏尼特右旗](../Page/苏尼特右旗.md "wikilink")、[东乌珠穆沁旗](../Page/东乌珠穆沁旗.md "wikilink")、[西乌珠穆沁旗](../Page/西乌珠穆沁旗.md "wikilink")、[太仆寺旗](../Page/太仆寺旗.md "wikilink")、[镶黄旗](../Page/镶黄旗_\(内蒙古\).md "wikilink")、[正镶白旗](../Page/正镶白旗.md "wikilink")、[正蓝旗](../Page/正蓝旗.md "wikilink")

其中，二连浩特市为自治区准地级[计划单列市](../Page/计划单列市.md "wikilink")。

此外，锡林郭勒盟还设立由国营乌拉盖农牧场管理局改制而成立的[乌拉盖管理区](../Page/乌拉盖管理区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>区划代码[5]</p></th>
<th><p>区划名称<br />
蒙古文</p></th>
<th><p>汉语拼音<br />
拉丁转写[6]</p></th>
<th><p>面积[7]<br />
（平方公里）</p></th>
<th><p>政府驻地</p></th>
<th><p>邮政编码</p></th>
<th><p>行政区划[8]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>街道</p></td>
<td><p>镇</p></td>
<td><p>乡</p></td>
<td><p>苏木</p></td>
<td><p>社区</p></td>
<td><p>行政村</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>152500</p></td>
<td><p>锡林郭勒盟<br />
</p></td>
<td></td>
<td><p>199,883.52</p></td>
<td><p><a href="../Page/锡林浩特市.md" title="wikilink">锡林浩特市</a></p></td>
<td><p>026000</p></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td><p>152501</p></td>
<td><p>二连浩特市<br />
</p></td>
<td></td>
<td><p>4,013.05</p></td>
<td><p><a href="../Page/乌兰街道.md" title="wikilink">乌兰街道</a></p></td>
<td><p>011100</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p>152502</p></td>
<td><p>锡林浩特市<br />
</p></td>
<td></td>
<td><p>14,780.18</p></td>
<td><p><a href="../Page/希日塔拉街道.md" title="wikilink">希日塔拉街道</a></p></td>
<td><p>026000</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td><p>152522</p></td>
<td><p>阿巴嘎旗<br />
</p></td>
<td></td>
<td><p>27,473.88</p></td>
<td><p><a href="../Page/别力古台镇.md" title="wikilink">别力古台镇</a></p></td>
<td><p>011400</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>152523</p></td>
<td><p>苏尼特左旗<br />
</p></td>
<td></td>
<td><p>34,240.18</p></td>
<td><p><a href="../Page/满都拉图镇.md" title="wikilink">满都拉图镇</a></p></td>
<td><p>011300</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>152524</p></td>
<td><p>苏尼特右旗<br />
</p></td>
<td></td>
<td><p>22,455.47</p></td>
<td><p><a href="../Page/赛汉塔拉镇.md" title="wikilink">赛汉塔拉镇</a></p></td>
<td><p>011200</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>152525</p></td>
<td><p>东乌珠穆沁旗<br />
</p></td>
<td></td>
<td><p>45,574.91</p></td>
<td><p><a href="../Page/乌里雅斯太镇.md" title="wikilink">乌里雅斯太镇</a></p></td>
<td><p>026300</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>152526</p></td>
<td><p>西乌珠穆沁旗<br />
</p></td>
<td></td>
<td><p>22,459.38</p></td>
<td><p><a href="../Page/巴拉嘎尔高勒镇.md" title="wikilink">巴拉嘎尔高勒镇</a></p></td>
<td><p>026200</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>152527</p></td>
<td><p>太仆寺旗<br />
</p></td>
<td></td>
<td><p>3,426.14</p></td>
<td><p><a href="../Page/宝昌镇.md" title="wikilink">宝昌镇</a></p></td>
<td><p>027000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>152528</p></td>
<td><p>镶黄旗<br />
</p></td>
<td></td>
<td><p>5,137.27</p></td>
<td><p><a href="../Page/新宝拉格镇.md" title="wikilink">新宝拉格镇</a></p></td>
<td><p>013200</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>152529</p></td>
<td><p>正镶白旗<br />
</p></td>
<td></td>
<td><p>6,253.09</p></td>
<td><p><a href="../Page/明安图镇.md" title="wikilink">明安图镇</a></p></td>
<td><p>013800</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>152530</p></td>
<td><p>正蓝旗<br />
</p></td>
<td></td>
<td><p>10,206.29</p></td>
<td><p><a href="../Page/上都镇.md" title="wikilink">上都镇</a></p></td>
<td><p>027200</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>152531</p></td>
<td><p>多伦县<br />
</p></td>
<td></td>
<td><p>3,863.69</p></td>
<td><p><a href="../Page/多伦诺尔镇.md" title="wikilink">多伦诺尔镇</a></p></td>
<td><p>027300</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>注：东乌珠穆沁旗数字包含乌拉盖管理区所辖巴音胡硕镇。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>锡林郭勒盟各市（旗、县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[9]（2010年11月）</p></th>
<th><p>户籍人口[10]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="odd">
<td><p>锡林郭勒盟</p></td>
<td><p>1028022</p></td>
<td><p>100</p></td>
</tr>
<tr class="even">
<td><p>二连浩特市</p></td>
<td><p>74179</p></td>
<td><p>7.22</p></td>
</tr>
<tr class="odd">
<td><p>锡林浩特市</p></td>
<td><p>245886</p></td>
<td><p>23.92</p></td>
</tr>
<tr class="even">
<td><p>阿巴嘎旗</p></td>
<td><p>43574</p></td>
<td><p>4.24</p></td>
</tr>
<tr class="odd">
<td><p>苏尼特左旗</p></td>
<td><p>33652</p></td>
<td><p>3.27</p></td>
</tr>
<tr class="even">
<td><p>苏尼特右旗</p></td>
<td><p>71063</p></td>
<td><p>6.91</p></td>
</tr>
<tr class="odd">
<td><p>东乌珠穆沁旗</p></td>
<td><p>93962</p></td>
<td><p>9.14</p></td>
</tr>
<tr class="even">
<td><p>西乌珠穆沁旗</p></td>
<td><p>87614</p></td>
<td><p>8.52</p></td>
</tr>
<tr class="odd">
<td><p>太仆寺旗</p></td>
<td><p>112339</p></td>
<td><p>10.93</p></td>
</tr>
<tr class="even">
<td><p>镶黄旗</p></td>
<td><p>28450</p></td>
<td><p>2.77</p></td>
</tr>
<tr class="odd">
<td><p>正镶白旗</p></td>
<td><p>54443</p></td>
<td><p>5.30</p></td>
</tr>
<tr class="even">
<td><p>正蓝旗</p></td>
<td><p>81967</p></td>
<td><p>7.97</p></td>
</tr>
<tr class="odd">
<td><p>多伦县</p></td>
<td><p>100893</p></td>
<td><p>9.81</p></td>
</tr>
<tr class="even">
<td><p>注：东乌珠穆沁旗的常住人口数据中包含乌拉盖管理区24128人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全盟[常住人口为](../Page/常住人口.md "wikilink")1028022人\[11\]，同[第五次全国人口普查](../Page/第五次全国人口普查.md "wikilink")2000年11月1日零时的993400人相比，十年共增加34622人，增长3.49%。年平均增长率为0.34%。其中，男性人口为536361人，占52.17%；女性人口为491661人，占47.83%。常住人口性别比（以女性为100）为109.09。0－14岁人口为141788人，占13.79%；15－64岁人口为817616人，占79.53%；65岁及以上人口为68618人，占6.67%。

### 民族

常住人口中，[汉族人口为](../Page/汉族.md "wikilink")681876人，占66.33%；[蒙古族人口为](../Page/蒙古族.md "wikilink")309764人，占30.13%；其他[少数民族人口为](../Page/少数民族.md "wikilink")36382人，占3.54%。

{{-}}

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [蒙古族](../Page/蒙古族.md "wikilink") | [满族](../Page/满族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [达斡尔族](../Page/达斡尔族.md "wikilink") | [朝鲜族](../Page/朝鲜族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | [彝族](../Page/彝族.md "wikilink") | [苗族](../Page/苗族.md "wikilink") | [鄂温克族](../Page/鄂温克族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | -------------------------------- | ------------------------------ | ------------------------------ | ---------------------------------- | -------------------------------- | -------------------------------- | ------------------------------ | ------------------------------ | ---------------------------------- | ---- |
| 人口数          | 681876                         | 309764                           | 24002                          | 10825                          | 686                                | 133                              | 91                               | 76                             | 72                             | 55                                 | 442  |
| 占总人口比例（%）    | 66.33                          | 30.13                            | 2.33                           | 1.05                           | 0.07                               | 0.01                             | 0.01                             | 0.01                           | 0.01                           | 0.01                               | 0.04 |
| 占少数民族人口比例（%） | \---                           | 89.49                            | 6.93                           | 3.13                           | 0.20                               | 0.04                             | 0.03                             | 0.02                           | 0.02                           | 0.02                               | 0.13 |

**锡林郭勒盟民族构成（2010年11月）**\[12\]

## 注释

## 参考资料

[Category:锡林郭勒](../Category/锡林郭勒.md "wikilink")
[Category:内蒙古自治区的盟](../Category/内蒙古自治区的盟.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  （[第二次全国土地调查数据](../Page/第二次全国土地调查.md "wikilink")）
8.
9.
10.
11.
12.