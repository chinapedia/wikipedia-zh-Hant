[Sikhote-Alin.png](https://zh.wikipedia.org/wiki/File:Sikhote-Alin.png "fig:Sikhote-Alin.png")
**锡霍特山脉**（）是位于[俄罗斯](../Page/俄罗斯.md "wikilink")[远东地方的一个山脉](../Page/远东.md "wikilink")。又称**希霍特-阿林山脉**、**老爷岭**、**内兴安岭**。它位于俄罗斯太平洋港口城市[海参崴东北](../Page/海参崴.md "wikilink")，纵断了[哈巴罗夫斯克边疆区和](../Page/哈巴罗夫斯克边疆区.md "wikilink")[滨海边疆区](../Page/滨海边疆区.md "wikilink")，长约1200公里，宽200-250公里。山脉中的主要山峰包括了[科尼克山](../Page/科尼克山.md "wikilink")、[托尔多基-亚尼山](../Page/托尔多基-亚尼山.md "wikilink")（2077米，最高峰）等。

[老爺嶺隕石是在](../Page/老爺嶺隕石.md "wikilink")1947年墬落在錫霍特阿蘭山脈的隕石，這批墬落隕石的總量是最近的歷史中最大的。

锡霍特山脉是地球上[温带地区最独特的地方之一](../Page/温带.md "wikilink")。在这里北方[針葉林特有的物种如](../Page/針葉林.md "wikilink")[驯鹿和](../Page/驯鹿.md "wikilink")[棕熊与热带物种如](../Page/棕熊.md "wikilink")[远东豹](../Page/远东豹.md "wikilink")、[东北虎和](../Page/东北虎.md "wikilink")[亞洲黑熊共存](../Page/亞洲黑熊.md "wikilink")。由于在這裡虎的生存竞争對手只有很少量的[狼](../Page/狼.md "wikilink")\[1\]。这里最老的树是一株树龄一千年左右的[紫杉](../Page/紫杉.md "wikilink")\[2\]。

[弗拉迪米尔·阿尔谢尼耶夫在](../Page/弗拉迪米尔·阿尔谢尼耶夫.md "wikilink")1910和20年代里对锡霍特山脉进行了仔细的考察，并把他的经历写在几本书里，其中包括《[德尔苏·乌扎拉](../Page/德尔苏·乌扎拉.md "wikilink")》（1923年），这部书于1975年被[黑澤明拍成电影并获得奥斯卡奖](../Page/黑澤明.md "wikilink")。1935年当地设立野生保护区来保护这里不寻常的野生生物。2001年[联合国教科文组织把锡霍特山脉列入](../Page/联合国教科文组织.md "wikilink")[世界遗产](../Page/世界遗产.md "wikilink")，理由是其“对于[中华秋沙鸭](../Page/中华秋沙鸭.md "wikilink")、[毛腿渔鸮和东北虎等珍稀物种幸存](../Page/毛腿渔鸮.md "wikilink")”的重要意义。整个世界遗产面积16319平方公里，其中中心保护区面积3985平方公里。

锡霍特山脉中植被茂盛，自然资源丰富，有[东北虎等珍稀动物栖息](../Page/东北虎.md "wikilink")。被列入了联合国[世界自然遗产名录](../Page/世界自然遗产.md "wikilink")\[3\]。中心地区只有在保护人员随同下才允许进入。

## 参考资料

## 外部链接

  - [世界遗产](http://whc.unesco.org/en/list/766)

[Category:俄羅斯山脈](../Category/俄羅斯山脈.md "wikilink")
[Category:濱海邊疆區地理](../Category/濱海邊疆區地理.md "wikilink")
[Category:哈巴羅夫斯克邊疆區地理](../Category/哈巴羅夫斯克邊疆區地理.md "wikilink")
[Category:俄罗斯世界遗产](../Category/俄罗斯世界遗产.md "wikilink")

1.
2.
3.