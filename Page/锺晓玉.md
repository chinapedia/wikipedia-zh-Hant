**鍾曉玉**（**Stella
Chung**，），為[馬來西亞著名](../Page/馬來西亞.md "wikilink")[華語女](../Page/華語.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")、[主持人](../Page/主持人.md "wikilink")。2007年，簽約加入大馬獨立唱片公司—東烽音樂，成為公司一姐，並推出首張個人專輯《[Be
Myself](../Page/Be_Myself.md "wikilink")》。鐘曉玉其實是童星出道，當時是六千金組合的成員。

## 简介

锺晓玉是[马来西亚的中年艺人](../Page/马来西亚.md "wikilink")，她是从[戏剧方面发展](../Page/戏剧.md "wikilink")。直到2006年，被著名音乐制作人[李乃刚赏识她的歌声](../Page/李乃刚.md "wikilink")，和[东烽音乐签下六年的唱片合约和经理人合约](../Page/东烽音乐.md "wikilink")，还有在[戏剧方面发展](../Page/戏剧.md "wikilink")。演艺事业未有大红大紫。钟晓玉的哥哥[钟盛忠也是艺人](../Page/钟盛忠.md "wikilink")。2012年9月16日钟晓玉与交往7年的男友共结连理。

## 音樂作品

個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯</p></th>
<th style="text-align: left;"><p>名稱</p></th>
<th style="text-align: left;"><p>發行時間</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>1st</strong></p></td>
<td style="text-align: left;"><p>Be Myself</p></td>
<td style="text-align: left;"><p>2007</p></td>
<td style="text-align: left;"><p>東烽音樂</p></td>
<td style="text-align: left;"><ol>
<li>Intro-Kick Off!</li>
<li>小小可愛</li>
<li>傻傻的</li>
<li>喜歡這樣想念</li>
<li>卸妝</li>
<li>只要你微笑</li>
<li>我試著</li>
<li>水壺</li>
<li>在乎</li>
<li>甜品</li>
<li>小雨</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2nd</strong></p></td>
<td style="text-align: left;"><p>Be Myself網遊代言慶功版</p></td>
<td style="text-align: left;"><p>2007</p></td>
<td style="text-align: left;"><p>東烽音樂</p></td>
<td style="text-align: left;"><ol>
<li>Be Myself</li>
<li>暗黑騎兵</li>
<li>我試著</li>
<li>天空又亮起來</li>
<li>小小可愛</li>
<li>傻傻的</li>
<li>喜歡這樣想念</li>
<li>卸妝</li>
<li>只要你微笑</li>
<li>在乎</li>
<li>甜品</li>
<li>小雨</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>3rd</strong></p></td>
<td style="text-align: left;"><p>Stellar</p></td>
<td style="text-align: left;"><p>2008</p></td>
<td style="text-align: left;"><p>東烽音樂</p></td>
<td style="text-align: left;"><ol>
<li>Stellar</li>
<li>暗戀我</li>
<li>太天真</li>
<li>糖衣</li>
<li>回到原點</li>
<li>請排隊</li>
<li>Sarikei</li>
<li>Be Myself</li>
<li>轉世</li>
<li>天空又亮起來</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>4th</strong></p></td>
<td style="text-align: left;"><p>Chapter 3 第三章</p></td>
<td style="text-align: left;"><p>2011</p></td>
<td style="text-align: left;"><p>東烽音樂</p></td>
<td style="text-align: left;"><ol>
<li>因為你</li>
<li>任性</li>
<li>什麼都願意</li>
<li>最佳主角</li>
<li>因為你(MMO)</li>
<li>任性(MMO)</li>
<li>什麼都願意(MMO)</li>
<li>最佳主角(MMO)</li>
</ol></td>
</tr>
</tbody>
</table>

鍾盛忠合體專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯</p></th>
<th style="text-align: left;"><p>名稱</p></th>
<th style="text-align: left;"><p>發行時間</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>1st</strong></p></td>
<td style="text-align: left;"><p>最懂我的人 EP</p></td>
<td style="text-align: left;"><p>2013</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>最懂我的人</li>
<li>Do Do Do</li>
<li>最懂我的人(鋼琴版)</li>
<li>最懂我的人(MMO)</li>
<li>Do Do Do(MMO)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2nd</strong></p></td>
<td style="text-align: left;"><p>新年新希望</p></td>
<td style="text-align: left;"><p>2014</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>新年新希望</li>
<li>掛紅燈+掛彩燈</li>
<li>快樂年</li>
<li>春花齊放</li>
<li>新年來到多熱鬧</li>
<li>唱出一個好春天</li>
<li>發大財</li>
<li>萬年紅</li>
<li>恭喜恭喜</li>
<li>招財進寶財神到 + 製作花絮</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>3rd</strong></p></td>
<td style="text-align: left;"><p>請你相信 EP</p></td>
<td style="text-align: left;"><p>2014</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>請你相信</li>
<li>DO DO DO</li>
<li>最懂我的人</li>
<li>請你相信(珍惜鋼琴版)</li>
<li>請你相信(傷感鋼琴版)</li>
<li>請你相信(王老師自彈自唱吉他版)</li>
<li>請你相信(MMO)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>4th</strong></p></td>
<td style="text-align: left;"><p>新年好</p></td>
<td style="text-align: left;"><p>2015</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>新年好</li>
<li>正月裡來是新春</li>
<li>發財</li>
<li>福星高照</li>
<li>除夕合家歡</li>
<li>炮竹一聲大地春</li>
<li>財神老爺下凡了</li>
<li>新年迎春花</li>
<li>新年頌</li>
<li>向歌友們拜年 + 製作花絮</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>5th</strong></p></td>
<td style="text-align: left;"><p>一定要再見 EP</p></td>
<td style="text-align: left;"><p>2015</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>一定要再見</li>
<li>飛</li>
<li>一定要再見(戲劇版)</li>
<li>一定要再見(鋼琴開心版)</li>
<li>一定要再見(鋼琴傷感版)</li>
<li>一定要再見(MMO)</li>
<li>飛(MMO)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>6th</strong></p></td>
<td style="text-align: left;"><p>新年團圓</p></td>
<td style="text-align: left;"><p>2016</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>新年團圓</li>
<li>金獅拜年(feat.阿妮&amp;王雪晶)</li>
<li>恭喜大家新年好</li>
<li>馬到功成(feat.雁卿&amp;郭美君)</li>
<li>新年花鼓歌(feat.阿妮)</li>
<li>好今年更好</li>
<li>喜氣洋洋(feat.最爛學生?3演員)</li>
<li>招財進寶 + 製作花絮</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>7th</strong></p></td>
<td style="text-align: left;"><p>想你的時候 EP</p></td>
<td style="text-align: left;"><p>2016</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>想你的時候</li>
<li>Give Me A Call</li>
<li>想你的時候(鋼琴開心版)</li>
<li>想你的時候(鋼琴悲傷版)</li>
<li>想你的時候(MMO)</li>
<li>Give Me A Call(MMO)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>8th</strong></p></td>
<td style="text-align: left;"><p>美好新年</p></td>
<td style="text-align: left;"><p>2017</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>美好新年</li>
<li>歡樂歌聲滿人間(feat.莊學忠)</li>
<li>新年好(feat.蔡常勇&amp;蔡常樂&amp;Benny&amp;林佩姍&amp;溫舒森&amp;Dennis&amp;Bryan Wee)</li>
<li>祭祖先</li>
<li>年節時景(feat.謝采妘)</li>
<li>衷心祝福你</li>
<li>嘻嘻哈哈過新年(feat.蔡常勇&amp;蔡常樂&amp;溫舒森)</li>
<li>來來過新年+製作花絮</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>9th</strong></p></td>
<td style="text-align: left;"><p>還愛著你 新歌+精选专辑</p></td>
<td style="text-align: left;"><p>2017</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>還愛著你</li>
<li>我們在一起好嗎？</li>
<li>想你的時候</li>
<li>Give Me A Call</li>
<li>一定要再見</li>
<li>飛</li>
<li>請你相信</li>
<li>最懂我的人</li>
<li>Do Do Do</li>
<li>一定要再見(戲劇版)</li>
<li>還愛著你 (MMO)</li>
<li>我們在一起好嗎？(MMO)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>10th</strong></p></td>
<td style="text-align: left;"><p>新年無限好</p></td>
<td style="text-align: left;"><p>2017</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>新年無限好</li>
<li>大家唱首新年歌 (feat. 阿妮 &amp; 郭美君)</li>
<li>拜年 (feat. 愛在校園 演員)</li>
<li>大吉大利中国（欢乐）年</li>
<li>賀新年 (feat.蔡常勇&amp;蔡常樂&amp;溫舒森@爱在校园 演员)</li>
<li>龍馬精神</li>
<li>新年來團聚 (feat. 愛在校園 演员)</li>
<li>新一年新感覺 (feat. 阿妮) + 製作花絮</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>11st</strong></p></td>
<td style="text-align: left;"><p>刚好，遇见你。</p></td>
<td style="text-align: left;"><p>2017</p></td>
<td style="text-align: left;"><p>Milk 6 Production</p></td>
<td style="text-align: left;"><ol>
<li>刚好，遇见你</li>
<li>凉凉</li>
<li>小幸运</li>
<li>我只在乎你</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

合輯

| 專輯名稱         | 發行時間 | 發行公司           |
| :----------- | :--- | :------------- |
| 大城情事電視原聲帶    | 2008 | 東烽音樂           |
| 添福添旺新姿態”皇帽賀歲 | 2010 | Carlsberg      |
| 鴻福聚滿堂”賀歲專輯   | 2011 | Hong Fook Tong |
|              |      |                |

## 影視作品

  - 《春風吻上我的臉》（2004）
  - 《草莓公主》（2004）
  - 《望情樹》（2004）
  - 《刁蠻公主小矮人》（2004）
  - 《Kisah Benar “Lintah Darat” 》（2005）
  - 《天機變》（2005）
  - 《陽光少年》（2006）
  - 《原點》（2006）
  - 《大城情事》（2008）
  - 《還我情真》（2008）
  - 《稽查專用2》（2011）
  - 《羽過天晴》（2012）

電影作品

  - 《Ronnie Got Lucky(Independent Short Film)》（2010）
  - 《23:59》（2011）
  - 《阿炳-心想事成》（2012）
  - 《[最爛學生?](../Page/最爛學生?.md "wikilink")》（2013）
  - 《[最爛學生?2](../Page/最爛學生?2.md "wikilink")》（2014）
  - 《[爱存在](../Page/爱存在.md "wikilink")》客串（2015）
  - 《[最爛學生?3](../Page/最爛學生?3.md "wikilink")》（2015）
  - 《[最猛學生](../Page/最猛學生.md "wikilink")》（2016）
  - 《[爱在校园](../Page/爱在校园.md "wikilink")》（2017）

## 主持作品

《名人約我》（2007）

## 演唱會

  - [“一定要再見”2015大馬演唱會](../Page/“一定要再見”2015大馬演唱會.md "wikilink")（2015-12）

## 外部連結

  -
  -
  -
[Category:馬來西亞電視演員](../Category/馬來西亞電視演員.md "wikilink")
[Category:馬來西亞女歌手](../Category/馬來西亞女歌手.md "wikilink")
[Category:馬來西亞華語歌手](../Category/馬來西亞華語歌手.md "wikilink")
[Category:马来西亚電視主持人](../Category/马来西亚電視主持人.md "wikilink")
[Category:马来西亚客家人](../Category/马来西亚客家人.md "wikilink")
[Category:砂拉越人](../Category/砂拉越人.md "wikilink")
[X](../Category/鍾姓.md "wikilink")