**镇沅彝族哈尼族拉祜族自治县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[普洱市下属的一个](../Page/普洱市.md "wikilink")[自治县](../Page/自治县.md "wikilink")，把边江、阿墨江流贯。

[明朝时置](../Page/明朝.md "wikilink")[镇沅州](../Page/镇沅州.md "wikilink")，后置[镇沅府](../Page/镇沅府.md "wikilink")。[清朝改为镇沅州](../Page/清朝.md "wikilink")，后又改为[镇沅直隶厅](../Page/镇沅直隶厅.md "wikilink")，1913年改名镇沅县，1990年成立镇沅彝族哈尼族拉祜族自治县。

## 行政区划

下辖：\[1\] 。

## 参考文献

[镇沅彝族哈尼族拉祜族自治县](../Category/镇沅彝族哈尼族拉祜族自治县.md "wikilink")
[Category:普洱区县](../Category/普洱区县.md "wikilink")
[云](../Category/中国彝族自治县.md "wikilink")
[云](../Category/中国哈尼族自治县.md "wikilink")
[云](../Category/中国拉祜族自治县.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.