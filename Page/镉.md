\-{zh-hans:; zh-hant:;}-
**镉**（，），是性质柔软的蓝白色有毒[过渡金属](../Page/过渡金属.md "wikilink")，[化学符号为](../Page/化学符号.md "wikilink")**Cd**，[原子序数为是](../Page/原子序数.md "wikilink")48。镉能在[锌矿找到](../Page/锌.md "wikilink")。镉和锌均可用作[电池材料](../Page/电池.md "wikilink")。镉可制作[鎳鎘電池](../Page/鎳鎘電池.md "wikilink")、用于塑膠製造和金屬[電鍍](../Page/電鍍.md "wikilink")，生产[顏料](../Page/顏料.md "wikilink")、[油漆](../Page/油漆.md "wikilink")、[染料](../Page/染料.md "wikilink")、[印刷](../Page/印刷.md "wikilink")[油墨等中某些](../Page/油墨.md "wikilink")[黃色顏料](../Page/黃色.md "wikilink")、制作[車胎](../Page/車胎.md "wikilink")、某些發光電子組件和[核子反應爐原件](../Page/核子反應爐.md "wikilink")。\[1\]\[2\]

## 历史

1817年，化学家[卡尔·塞缪尔·莱贝雷希特·赫尔曼](../Page/卡尔·塞缪尔·莱贝雷希特·赫尔曼.md "wikilink")、[弗里德里希·施特罗迈尔和罗洛夫几乎同时发现了镉元素](../Page/弗里德里希·施特罗迈尔.md "wikilink")。\[3\]

## 鎘污染

镉對健康有不良的影響，被列为可致[癌物](../Page/癌.md "wikilink")。

1930-1960年代，[日本](../Page/日本.md "wikilink")[富山縣](../Page/富山縣.md "wikilink")[神通川流域部分鎘污染](../Page/神通川.md "wikilink")，发生[痛痛病](../Page/痛痛病.md "wikilink")，事緣煉鋅廠排放的含鎘廢水污染了周圍的耕地和水源。

[廣州中山大學生物科技學院聯同](../Page/廣州中山大學.md "wikilink")[香港浸會大學生物系在](../Page/香港浸會大學.md "wikilink")2006年3至4月期間，抽查化驗中港兩地市面出售的[楊桃](../Page/楊桃.md "wikilink")，51%鎘含量屬嚴重超標。\[4\]

<File:Cadmium> elec.jpg|可由電解氯化鎘水溶液製取鎘金屬 <File:Homemade>
Cadmium.jpg|電解法製出的鎘

## 各地對鎘安全標準

### 台灣

根据《中華民國国家标准污水综合排放标准\[5\]》，镉属于第一类污染物,其最高允许排放浓度为0.1mg/L。\[6\]

### 歐盟

[歐盟将鎘列為高危害有毒物質和可致癌物質并予以規管](../Page/歐盟.md "wikilink")。\[7\]

### 美國

[美國環境保護署限制排入](../Page/美國環境保護署.md "wikilink")[湖](../Page/湖.md "wikilink")、[河](../Page/河.md "wikilink")、[棄置場和](../Page/棄置場.md "wikilink")[農田的鎘量并禁止](../Page/農田.md "wikilink")[殺蟲劑中含有鎘](../Page/殺蟲劑.md "wikilink")。美國環境保護署允許[飲用水含有](../Page/飲用水.md "wikilink")10[ppb的鎘](../Page/ppb.md "wikilink")，並打算把限制減到5ppb。[美國食品和藥物管理局规定](../Page/美國食品和藥物管理局.md "wikilink")[食用色素的含鎘量為不得多于](../Page/食用色素.md "wikilink")15[ppm](../Page/百萬分率.md "wikilink")。[美國職業安全衛生署规定工作環境空氣中镉含量在](../Page/美國職業安全衛生署.md "wikilink")[煙霧為](../Page/煙霧.md "wikilink")100微克/立方公尺，在[鎘塵為](../Page/鎘塵.md "wikilink")200微克/
立方公尺。美國職業安全衛生署计划将空气中所有鎘化合物含量限制在1到5微克/
立方公尺[美國國家職業安全和衛生研究所希望讓工人盡量少呼吸到鎘以防止](../Page/美國國家職業安全和衛生研究所.md "wikilink")[肺癌](../Page/肺癌.md "wikilink")\[8\]。

## 参考资料

\&choose_order{{=}}\&exactphrase{{=}}\&lang{{=}}_e\&region{{=}}\&searchfor{{=}}unece\&subject{{=}}\&theme{{=}}\&withoutwords{{=}}
英國環境部關於鎘的資料\]

  - [國立台灣大學職業醫學與工業衛生研究所\~關於鎘的資料](http://140.112.119.150/creod/iosh/articles/Cd.htm)
  - \[<http://www.epa.gov.tw/b/b0100.asp?Ct_Code>{{=}}06X0001696X0003265\&L
    台灣行政院環境保護署\~關於鎘的資料\]
  - [蘇州大學附属兒童童醫院](http://engine.cqvip.com/content/r/97998x/2004/015/001/yy04_r1_9162806.pdf)
  - [中國地質調查局\~2007關於青島的鎘的情況資料](http://www.cgs.gov.cn/NEWS/Geology%20News/2007/20070307/35.pdf)}}
  - [Lu Le Laboratory
    鎘金屬的介紹影片](http://www.youtube.com/watch?v=l6tULv-ja8w)

[Category:过渡金属](../Category/过渡金属.md "wikilink")
[镉](../Category/镉.md "wikilink")
[5L](../Category/第5周期元素.md "wikilink")
[5L](../Category/化学元素.md "wikilink")

1.  <http://140.112.119.150/creod/iosh/articles/Cd.htm>
2.
3.
4.  <http://www.mingpaonews.com/20070922/goa1.htm>
5.
6.  [GB 8978-1996
    污水综合排放标准](http://zh.wikisource.org/zh/GB_8978-1996_%E6%B1%A1%E6%B0%B4%E7%BB%BC%E5%90%88%E6%8E%92%E6%94%BE%E6%A0%87%E5%87%86)
7.  <http://www.environment-agency.gov.uk/business/444255/446867/255244/substances/29/?any_all=&choose_order=&exactphrase=&lang=_e&region=&searchfor=unece&subject=&theme=&withoutwords=>
8.  <http://www.epa.gov.tw/b/b0100.asp?Ct_Code=06X0001696X0003265&L>