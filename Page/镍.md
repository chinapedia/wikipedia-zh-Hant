****是一種[化學元素](../Page/化學元素.md "wikilink")，[化學符號為](../Page/化學符號.md "wikilink")**Ni**，[原子序數為](../Page/原子序數.md "wikilink")28。它是一種有光澤的銀白色[金屬](../Page/金屬.md "wikilink")，其銀白色帶一點淡金色。鎳屬於[過渡金屬](../Page/過渡金屬.md "wikilink")，質硬，具延展性。純鎳的化學活性相當高，這種活性可以在反應[表面積最大化的粉末狀態下看到](../Page/表面積.md "wikilink")，但大塊的鎳金屬與周圍的空氣反應緩慢，因為其表面已形成了一層帶保護性質的[氧化物](../Page/氧化物.md "wikilink")。即使如此，由於鎳與氧之間的活性夠高，所以在地球表面還是很難找到自然的金屬鎳。地球表面的自然鎳都被封在較大的[鎳鐵隕石裏面](../Page/鐵隕石.md "wikilink")，這是因為隕石在太空的時候接觸不到氧氣的緣故。在地球上，這種自然鎳總會和[鐵結合在一起](../Page/鐵.md "wikilink")，這點反映出它們都是[超新星核合成主要的最終產物](../Page/超新星核合成.md "wikilink")。一般認為地球的[地核就是由鎳鐵混合物所組成的](../Page/地核.md "wikilink")\[1\]。

鎳的使用（天然的隕鎳鐵合金）最早可追溯至公元前3500年。[阿克塞尔·弗雷德里克·克龙斯泰特於](../Page/阿克塞尔·弗雷德里克·克龙斯泰特.md "wikilink")1751年最早分離出鎳，並將它界定為化學元素，儘管他最初把鎳[礦石誤認為銅的礦物](../Page/礦石.md "wikilink")。鎳的外語名字來自德國礦工傳說中同名的淘氣妖精（Nickel，與英語中魔鬼別稱"Old
Nick"相近），這是由於鎳銅礦不能用煉銅的方法煉出銅來，所以被比擬成妖魔。鎳最經濟的主要來源為鐵礦石[褐鐵礦](../Page/褐鐵礦.md "wikilink")，含鎳量一般為1-2%。鎳的其他重要礦物包括[硅鎂鎳礦及](../Page/硅鎂鎳礦.md "wikilink")[鎳黃鐵礦](../Page/鎳黃鐵礦.md "wikilink")。鎳的主要生產地包括[加拿大的](../Page/加拿大.md "wikilink")[索德柏立區](../Page/索德柏立盆地.md "wikilink")（一般認為該處是[隕石撞擊坑](../Page/隕石.md "wikilink")）、[太平洋的](../Page/太平洋.md "wikilink")[新喀里多尼亞及](../Page/新喀里多尼亞.md "wikilink")[俄羅斯的](../Page/俄羅斯.md "wikilink")[諾里爾斯克](../Page/諾里爾斯克.md "wikilink")。

由於鎳在室溫時的[氧化緩慢](../Page/氧化.md "wikilink")，所以一般視為具有耐腐蝕性。歷史上，因為這一點鎳被用作[電鍍各種表面](../Page/電鍍.md "wikilink")，例如金屬（如鐵及[黃銅](../Page/黃銅.md "wikilink")）、化學裝置內部及某些需要保持閃亮銀光的合金（例如[鎳銀](../Page/鎳銀.md "wikilink")）。世界鎳生產量中的約6%仍被用於抗腐蝕純鎳電鍍。鎳曾經是[硬幣的常見成份](../Page/硬幣.md "wikilink")，但現時這方面已大致上被較便宜的鐵所取代，尤其是因為有些人的皮膚對鎳[過敏](../Page/過敏.md "wikilink")。儘管如此，英國還是在皮膚科醫生的反對下，於2012年開始再使用鎳鑄造錢幣\[2\]。

只有四種元素在室溫時具有[鐵磁性](../Page/鐵磁性.md "wikilink")，鎳就是其中一種。含鎳的[鋁鎳鈷合金永久磁鐵](../Page/鋁鎳鈷合金.md "wikilink")，其磁力強度介乎於含鐵的永久磁鐵與[稀土磁鐵之間](../Page/稀土磁鐵.md "wikilink")。鎳在現代世界的的地位主要來自於它的各種[合金](../Page/合金.md "wikilink")。全世界鎳產量中的約60%被用於生產各種鎳鋼（特別是[不鏽鋼](../Page/不鏽鋼.md "wikilink")）。其他常見的合金，還有一些的新的[高溫合金](../Page/高溫合金.md "wikilink")，就幾乎就佔盡了餘下的世界鎳用量。用於製作化合物的化學用途只佔了鎳產量的不到3%\[3\]。作為化合物，鎳在化學製造有好幾種特定的用途，例如作為[氫化反應的催化劑](../Page/雷尼鎳.md "wikilink")。某些微生物和植物的[酶用鎳作為](../Page/酶.md "wikilink")[活性位點](../Page/活性位點.md "wikilink")，因此鎳是它們重要的養分。

## 特性

### 原子及物理性質

鎳是一種有光澤的銀白色[金屬](../Page/金屬.md "wikilink")，其銀白色帶一點淡金色，可被高度磨光。只有四種元素在室溫或其附近具有鐵磁性，鎳就是其中一種，其餘三種為鐵、[鈷及](../Page/鈷.md "wikilink")[釓](../Page/釓.md "wikilink")。其[居里溫度為](../Page/居里溫度.md "wikilink")355
°C，即大塊的鎳在這個溫度以上就會失去磁性\[4\]。。

### 電子排佈的爭議

鎳原子共有兩種[電子排佈](../Page/電子排佈.md "wikilink")：\[Ar\] 4s<sup>2</sup>
3d<sup>8</sup>及\[Ar\] 4s<sup>1</sup>
3d<sup>9</sup>，而兩者的能量非常接近（符號\[Ar\]指的是其核心結構與氬相似）。對於哪一種排佈的能量較低仍存在分歧\[5\]。化學教科書引用的鎳電子排佈為\[Ar\]
3d<sup>8</sup> 4s<sup>2</sup>\[6\]或與前者相同的\[Ar\] 4s<sup>2</sup>
3d<sup>8</sup>\[7\]。這種排佈遵從[馬德隆能量排序規則](../Page/構造原理.md "wikilink")，預測4s的位置被填滿後才開始填3d的位置。這一點是有實驗支持的，鎳原子最低的能量態為4s<sup>2</sup>
3d<sup>8</sup>能階，更確切來說是3d<sup>8</sup>(<sup>3</sup>F) 4s<sup>2</sup>
<sup>3</sup>F的J = 4能階\[8\]

然而，這兩種排佈實際上都會各自衍生出一系列不同能量的態\[9\]。這兩組能量互相交疊，而排佈\[Ar\] 4s<sup>1</sup>
3d<sup>9</sup>的各態平均能量比\[Ar\] 4s<sup>2</sup>
3d<sup>8</sup>的要低。因此，原子計算的研究文獻引用鎳的基態排佈時用的是\[Ar\]
4s<sup>1</sup> 3d<sup>9</sup>\[10\]。

### 同位素

天然鎳共有五種穩定的[同位素](../Page/同位素.md "wikilink")：<sup>58</sup>Ni、<sup>60</sup>Ni、<sup>61</sup>Ni、<sup>62</sup>Ni和<sup>64</sup>Ni。其中<sup>58</sup>Ni的[豐度最高](../Page/豐度.md "wikilink")（68.077%）。[<sup>62</sup>Ni是現存元素中每核子束縛能最高的](../Page/鎳-62.md "wikilink")[核素](../Page/核素.md "wikilink")，其束縛能比[<sup>58</sup>Fe及](../Page/鐵的同位素.md "wikilink")[<sup>56</sup>Fe還要高](../Page/鐵的同位素.md "wikilink")，而<sup>56</sup>Fe很多時候被誤以為是擁有束縛能最高的原子核\[11\]。已被發現的鎳[放射性同位素共有](../Page/放射性同位素.md "wikilink")18種，其中最穩定的三種為<sup>59</sup>Ni（半衰期76000年）、<sup>63</sup>Ni（半衰期100.1年）和<sup>56</sup>Ni（半衰期6.077天）。其他餘下的放射性同位素半衰期皆少於60小時，其中大部份的半衰期更少於30秒。此元素擁有一種[亞穩態](../Page/核同质异能素.md "wikilink")\[12\]。

恆星“死亡”過程中的[矽燃燒過程會產生鎳](../Page/矽燃燒過程.md "wikilink")-56，在之後的Ia型[超新星爆炸時會大量放出鎳](../Page/超新星.md "wikilink")-56。這些超新星在中期到後期時，其[光變曲線的形狀顯示的正是鎳](../Page/光變曲線.md "wikilink")-56的衰變，經電子捕獲而衰變成鈷-56，並最終衰變成鐵-56\[13\]。鎳-59是一種長命的[宇宙源](../Page/宇宙源核素.md "wikilink")[放射性同位素](../Page/放射性同位素.md "wikilink")，其半衰期為76000年。<sup>59</sup>Ni在[同位素地質學中有多種用途](../Page/同位素地質學.md "wikilink")：它被用於鋻定隕石的着陸年份，和判定冰與[沉積物中外太空塵埃的豐度](../Page/沉積物.md "wikilink")。鎳-60是鐵-60的子體衰變產物，而鐵-60是一種已絕跡的放射性核素，其半衰期為260萬年。由於<sup>60</sup>Fe的半衰期是如此的長，所以如果[太陽系的物質含有足夠高濃度的](../Page/太陽系.md "wikilink")<sup>60</sup>Fe，那麼它的耐久性就很有可能會影響到<sup>60</sup>Ni的同位素構成測量結果。因此，外太空物質中的鎳-60豐度，可能會為太陽系的起源及其早期歷史提供線索。[<sup>62</sup>Ni的每核子束縛能比其他任何元素的任何同位素都高](../Page/鎳-62.md "wikilink")（每核子8.7946
[MeV](../Page/電子伏.md "wikilink")）\[14\]。任何比<sup>62</sup>Ni重的同位素，都不能在不損失能量的情況下，通過[核融合來進行合成](../Page/核融合.md "wikilink")。1999年發現的<sup>48</sup>Ni是已知重金屬同位素的核子中質子比率最高的。鎳-48含質子28個，中子20個，故具有雙[幻數](../Page/幻數_\(物理學\).md "wikilink")（跟<sup>208</sup>[Pb一樣](../Page/鉛.md "wikilink")），因此性質異常穩定\[15\]\[16\]。

在各種鎳同位素的原子質量中，原子質量最輕的只有48[u](../Page/原子質量單位.md "wikilink")（<sup>48</sup>Ni），最重的則有78u（<sup>78</sup>Ni）。最近的測量結果指出，鎳-78的半衰期為0.11秒；科學家們相信，鎳-78這種同位素在[超新星核合成過程中合成比鐵重的元素時具有重要作用](../Page/超新星核合成.md "wikilink")\[17\]。

### 產狀

[Widmanstatten_hand.jpg](https://zh.wikipedia.org/wiki/File:Widmanstatten_hand.jpg "fig:Widmanstatten_hand.jpg")，可見上面有由兩種鎳鐵──錐紋石和鎳紋石所組成的[魏德曼花紋](../Page/魏德曼花紋.md "wikilink")。\]\]
鎳在地球上最常見的產狀有：與[硫和鐵組成的](../Page/硫.md "wikilink")[鎳黃鐵礦](../Page/鎳黃鐵礦.md "wikilink")、與硫組成的[針硫鎳礦](../Page/針硫鎳礦.md "wikilink")、與[砷組成的](../Page/砷.md "wikilink")[紅砷鎳礦及與砷和硫組成的鎳](../Page/紅砷鎳礦.md "wikilink")[方鉛礦](../Page/方鉛礦.md "wikilink")\[18\]。

鎳估計蘊藏量最高的地區是[澳洲和](../Page/澳洲.md "wikilink")[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")（共佔45%）\[19\]。

就世界資源方面來說，平均含鎳量達1%的已知陸上資源最少蘊含13億公噸的鎳（約為已知蘊含量的兩倍）。其中六成磚紅壤礦床，另外四成為硫化物礦床\[20\]。

根據[地球物理學的證據](../Page/地球物理學.md "wikilink")，有假說指出地球上大部份的鎳都集中在地球的[外核和](../Page/外核.md "wikilink")[內核](../Page/內核.md "wikilink")。[錐紋石和](../Page/錐紋石.md "wikilink")[鎳紋石是兩種天然產生的鎳鐵](../Page/鎳紋石.md "wikilink")[合金](../Page/合金.md "wikilink")。鐵鎳在錐紋石中的比例一般在90:10與95:5之間，同時也有可能存在雜質（如[鈷或](../Page/鈷.md "wikilink")[碳](../Page/碳.md "wikilink")）；而鎳紋石的含鎳量則在20%至65%之間。這兩種礦物基本上都只能在[鎳鐵隕石中找到](../Page/鐵隕石.md "wikilink")\[21\]。

## 化合物

[Nickel-carbonyl-2D.png](https://zh.wikipedia.org/wiki/File:Nickel-carbonyl-2D.png "fig:Nickel-carbonyl-2D.png")
最常見的鎳[氧化態為](../Page/氧化態.md "wikilink")+2，但Ni<sup>0</sup>、Ni<sup>+</sup>和Ni<sup>3+</sup>的化合物都有名，此外還有三種奇特的氧化態Ni<sup>2-</sup>、Ni<sup>1-</sup>和Ni<sup>4+</sup>\[22\]。

### 鎳(0)

[四羰基鎳](../Page/四羰基鎳.md "wikilink")（Ni(CO)<sub>4</sub>）是由[路德維希·蒙德所發現的](../Page/路德維希·蒙德.md "wikilink")\[23\]。它在室溫下是一種具揮發性的液體，而且毒性猛烈。四羰基鎳在加熱後（180
°C）會分解成鎳與一氧化碳。\[24\]

  -
    Ni(CO)<sub>4</sub>  Ni + 4 CO

蒙德法就利用了上述這一過程來精煉鎳。[配合物](../Page/配合物.md "wikilink")[雙-(1，5環辛二烯)鎳是鎳氧化態也是](../Page/雙-\(1，5環辛二烯\)鎳.md "wikilink")0，由於它的配位子[1,5-環辛二烯很容易就能被置換在](../Page/1,5-環辛二烯.md "wikilink")[有機鎳化學中是一種很有用的催化劑](../Page/有機鎳化學.md "wikilink")。\[25\]

### 鎳(I)

鎳(I)配合物並不常見，其中一個例子是四面體配合物NiBr(PPh<sub>3</sub>)<sub>3</sub>。此氧化態很多時候會含有Ni-Ni鍵，例如K<sub>4</sub>\[Ni<sub>2</sub>(CN)<sub>6</sub>\]，此化合物呈暗紅色，具[抗磁性](../Page/抗磁性.md "wikilink")，用[鈉汞齊還原K](../Page/鈉汞齊.md "wikilink")<sub>2</sub>\[Ni<sub>2</sub>(CN)<sub>6</sub>\]可得，在水中會產生氧化反應，同時會放出氫\[26\]。

[Structure_of_hexacyanodinickelate(I)_ion.png](https://zh.wikipedia.org/wiki/File:Structure_of_hexacyanodinickelate\(I\)_ion.png "fig:Structure_of_hexacyanodinickelate(I)_ion.png")

### 鎳(II)

[Color_of_various_Ni(II)_complexes_in_aqueous_solution.jpg](https://zh.wikipedia.org/wiki/File:Color_of_various_Ni\(II\)_complexes_in_aqueous_solution.jpg "fig:Color_of_various_Ni(II)_complexes_in_aqueous_solution.jpg"))<sub>3</sub>\]<sup>2+</sup>、\[NiCl<sub>4</sub>\]<sup>2-</sup>和\[Ni(H<sub>2</sub>O)<sub>6</sub>\]<sup>2+</sup>。\]\]

[Nickel(II)-sulfate-hexahydrate-sample.jpg](https://zh.wikipedia.org/wiki/File:Nickel\(II\)-sulfate-hexahydrate-sample.jpg "fig:Nickel(II)-sulfate-hexahydrate-sample.jpg")[硫酸鎳晶體](../Page/硫酸鎳.md "wikilink")\]\]
鎳(II)能與所有常見的陰離子生成化合物，即硫化物、硫酸鹽、碳酸鹽、氫氧化物、羧酸鹽及鹵化物。把鎳金屬或其氧化物溶在硫酸裏，就能大量生產出[硫酸鎳(II)](../Page/硫酸鎳.md "wikilink")。它有六水合物及七水合物\[27\]，

一些鎳(II)的四配位配合物（如[雙-(三苯基膦)氯化镍](../Page/雙-\(三苯基膦\)氯化镍.md "wikilink")）有着兩種不同的分子幾何形式──四面體及平面四方。四面體配合物具[順磁性](../Page/順磁性.md "wikilink")，而平面四方配合物則具[抗磁性](../Page/抗磁性.md "wikilink")。鎳配合物中的這種平面─四面體平衡，還有八面體結構，是其他較重的[10族金屬](../Page/10族元素.md "wikilink")[鈀](../Page/鈀.md "wikilink")(II)與[鉑](../Page/鉑.md "wikilink")(II)的二價電子配合物中所沒有的，因為它們基本上只有平面四方結構\[28\]。

[二茂鎳的](../Page/二茂鎳.md "wikilink")[反键轨道上填充有电子](../Page/反键轨道.md "wikilink")，使其结构的稳定性降低，易被氧化，也易分解。\[29\]
[Nickel_antimonide.jpg](https://zh.wikipedia.org/wiki/File:Nickel_antimonide.jpg "fig:Nickel_antimonide.jpg")

### 鎳(III)及鎳(IV)

鎳(III)及鎳(IV)氧化態的簡單化合物只有氟化物及氧化物，而唯一例外就是KNiIO<sub>6</sub>，可算是[過碘酸根離子](../Page/過碘酸鹽.md "wikilink")\[IO<sub>6</sub>\]<sup>5-</sup>的正式鹽\[30\]。混合氧化物BaNiO<sub>3</sub>中含有鎳(IV)，而[氧化鎳(III)中則含有鎳](../Page/三氧化二鎳.md "wikilink")(III)，它們及鎳的其他氧化物都可被用作各種[蓄電池的陰極](../Page/蓄電池.md "wikilink")，種類包括[鎳鎘](../Page/鎳鎘電池.md "wikilink")、[鎳鐵](../Page/鎳鐵電池.md "wikilink")、[氫鎳](../Page/氫鎳電池.md "wikilink")（用氫氣的）和[鎳氫](../Page/鎳氫電池.md "wikilink")（用[金屬氫化物的](../Page/金屬氫化物.md "wikilink")），也有一些生產商會用鎳氧化物來作[鋰離子電池的陰極](../Page/鋰離子電池.md "wikilink")\[31\]。σ-予體配位子（如[硫醇及](../Page/硫醇.md "wikilink")[磷化氫](../Page/磷化氫.md "wikilink")）可用於穩定鎳(III)\[32\]。

## 歷史

由於鎳礦石很容易被誤認為銀礦石，因此對這種金屬的認識和使用是相對近期的事。然而，偶然使用到鎳是一件自古已有的事，可追溯至公元前3500年。從現今[敘利亞境內出土的](../Page/敘利亞.md "wikilink")[青銅含鎳量可高至](../Page/青銅.md "wikilink")2%\[33\]。此外，中國有文獻指出當地在公元前1700至1400年期間已經有使用[白銅](../Page/白銅.md "wikilink")（一種銅鎳合金）。英國早在17世紀就已經向中國進口這種白銅，但這種合金含鎳的事實要到1822年才被發現\[34\]。

中世紀的德國人在[厄爾斯山脈發現了一種跟銅礦石很像的紅色礦物](../Page/厄爾斯山脈.md "wikilink")。然而，礦工們卻未能從中提煉到銅，因此他們就把這種困擾歸咎於他們傳說中的妖精Nickel（與英語中魔鬼別稱"Old
Nick"相近）。他們把這種礦石命名為“銅妖”（Kupfernickel，其中Kupfer是銅的意思）\[35\]\[36\]\[37\]\[38\]。這種礦石就是現在的[紅砷鎳礦](../Page/紅砷鎳礦.md "wikilink")，它是一種鎳的砷化物。1751年，[阿克塞尔·弗雷德里克·克龙斯泰特男爵嘗試從銅妖礦石中煉出銅來](../Page/阿克塞尔·弗雷德里克·克龙斯泰特.md "wikilink")──但卻煉出一種白色的金屬，因此他用為礦石命名的妖精名字，來為這種金屬命名\[39\]。

鎳在被发现以後的唯一來源就是罕見的銅妖礦石。直至1822年，才開始從製作[鈷藍色染料的副產品中取得鎳](../Page/鈷藍色.md "wikilink")。最早大規模生產鎳的國家是挪威，他們自1848年開始就從本地含鎳量高的[磁黃鐵礦生產鎳](../Page/磁黃鐵礦.md "wikilink")。鐵的生產在1889年中引入了鎳，因此鎳的需求量增加。[新喀里多尼亞的鎳礦床在](../Page/新喀里多尼亞.md "wikilink")1865年被發現，於1875年至1915年間為全世界提供了大部份的鎳。之後發現了更多大型的鎳礦床，使得真正的大規模生產鎳變得可行，這些礦床為1883年發現的加拿大[索德柏立盆地](../Page/索德柏立盆地.md "wikilink")，1920年發現的俄羅斯[諾里爾斯克](../Page/諾里爾斯克.md "wikilink")-[塔爾納赫和](../Page/塔爾納赫.md "wikilink")1924年發現的南非[梅倫斯基暗礁](../Page/梅倫斯基暗礁.md "wikilink")（Merensky
Reef）\[40\]。

[Nickel2.jpg](https://zh.wikipedia.org/wiki/File:Nickel2.jpg "fig:Nickel2.jpg")。\]\]
鎳從十九世紀開始就成為了鑄造硬幣的材料。在美國，Nickel（鎳，或其簡稱Nick）這個暱稱原本指的是由銅及鎳鑄成的[1美分飛鷹硬幣](../Page/1美分飛鷹硬幣.md "wikilink")，這種硬幣在1857-58年間把純銅的成份中的12%換成了的鎳。之後1859-64年流通的[印第安頭像硬幣也用了一樣的合金成份](../Page/印第安頭像硬幣.md "wikilink")，因此也用上了這個暱稱。要注意的是在之後1865年，在鎳成份提高至21%後，這個暱稱就被改作稱呼[3美分硬幣](../Page/3美分硬幣.md "wikilink")。1866年，[5美分盾牌硬幣名正言順地以](../Page/5美分盾牌硬幣.md "wikilink")25%的鎳含量（其餘75%為銅）承繼了這個暱稱。時至今日，5美分硬幣當年的合金比例與暱稱仍然在美國通用。瑞士於1881年最早使用幾乎以純鎳鑄造的硬幣，而當中最有名的鎳幣當數1922年至1981年非大戰期間，由加拿大（當時世界最大的鎳生產國）鑄造含鎳量達99.9%的[5加分硬幣](../Page/5加分硬幣.md "wikilink")，而高含鎳量就使得這些硬幣帶磁性\[41\]。第二次世界大戰期間的1942-45年，由於鎳在裝甲中的功用使得它成了戰爭資源，所以美國和加拿大都把硬幣中的大部分或全部的鎳成份換掉\[42\]\[43\]。
{{-}}

## 世界生產

[Nickel_world_production_zh.svg](https://zh.wikipedia.org/wiki/File:Nickel_world_production_zh.svg "fig:Nickel_world_production_zh.svg")
[美國地質調查局的報告指出](../Page/美國地質調查局.md "wikilink")，鎳最大的生產國為菲律賓、印尼、俄羅斯、加拿大及澳洲\[44\]。在俄羅斯以外的歐洲地區中，最大的鎳礦床位於[芬蘭和](../Page/芬蘭.md "wikilink")[希臘](../Page/希臘.md "wikilink")。平均含鎳量達1%的已知陸上資源最少蘊含13億公噸的鎳（約為已知蘊含量的兩倍）。其中六成磚紅壤礦床，另外四成為硫化物礦床。此外，在大面積的海床上有含鎳資源的錳殼及礦瘤，尤其是在太平洋的海床上\[45\]

[俄勒岡州的](../Page/俄勒岡州.md "wikilink")[里德爾市](../Page/里德爾_\(俄勒岡州\).md "wikilink")（Riddle）是美國唯一在本土對鎳進行過商業開採的地方，當地有一個面積為幾平方英里的矽鎂鎳礦表層礦床。該礦場於1987年關閉\[46\]\[47\]。[鷹礦計劃打算在](../Page/鷹礦計劃.md "wikilink")[密歇根州的](../Page/密歇根州.md "wikilink")[上半島處開發一個新的鎳礦場](../Page/密西根上半島.md "wikilink")\[48\]。

| 礦場產量及蘊藏量\[49\]                           | 2012年     | 2011年     | 蘊藏量        |
| ---------------------------------------- | --------- | --------- | ---------- |
| [澳大利亚](../Page/澳大利亚.md "wikilink")       | 230,000   | 215,000   | 20,000,000 |
| [博茨瓦納](../Page/博茨瓦納.md "wikilink")       | 26,000    | 26,000    | 490,000    |
| [巴西](../Page/巴西.md "wikilink")           | 140,000   | 209,000   | 7,500,000  |
| [加拿大](../Page/加拿大.md "wikilink")         | 220,000   | 220,000   | 3,300,000  |
| [中國](../Page/臺灣.md "wikilink")           | 91,000    | 89,800    | 3,000,000  |
| [哥倫比亞](../Page/哥倫比亞.md "wikilink")       | 80,000    | 76,000    | 1,100,000  |
| [古巴](../Page/古巴.md "wikilink")           | 72,000    | 71,000    | 5,500,000  |
| [多明尼加共和國](../Page/多明尼加共和國.md "wikilink") | 24,000    | 21,700    | 970,000    |
| [印尼](../Page/印尼.md "wikilink")           | 320,000   | 290,000   | 3,900,000  |
| [馬達加斯加](../Page/馬達加斯加.md "wikilink")     | 22,000    | 5,900     | 1,600,000  |
| [新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")   | 140,000   | 131,000   | 12,000,000 |
| [菲律賓](../Page/菲律賓.md "wikilink")         | 330,000   | 270,000   | 1,100,000  |
| [俄羅斯](../Page/俄羅斯.md "wikilink")         | 270,000   | 267,000   | 6,100,000  |
| [南非](../Page/南非.md "wikilink")           | 42,000    | 44,000    | 3,700,000  |
| 其他國家                                     | 120,000   | 103,000   | 4,600,000  |
| 世界總和（公噸，準確至1,000公噸）                      | 2,100,000 | 1,940,000 | 75,000,000 |

{{-}}

## 提取與精煉

傳統上，大部份硫礦石都要經過[高溫冶金技巧](../Page/高溫冶金學.md "wikilink")，來造出一種[硫滓](../Page/硫滓.md "wikilink")，以作精煉之用。由於近來[濕法冶金學的進展](../Page/濕法冶金學.md "wikilink")，所以現時不少的鎳精煉都用這些方法來進行。硫礦床傳統上是用[泡沫浮選法按濃度處理](../Page/泡沫浮選法.md "wikilink")，再經高溫冶金提取金屬。而在濕法冶金的過程中，鎳礦石經浮選法處理後（若Ni-Fe比率太低則改用微差浮選法），就被送上熔煉。在產出硫滓以後，就用[謝里特-戈登法](../Page/謝里特-戈登法.md "wikilink")（）處理\[50\]。首先，加入[硫化氫將銅移除](../Page/硫化氫.md "wikilink")，留下只剩鈷及鎳的精礦。之後使用溶劑萃取法，把鈷及鎳分開，最終的鎳成品純度高於99.9%。

[Nickel_electrolytic_and_1cm3_cube.jpg](https://zh.wikipedia.org/wiki/File:Nickel_electrolytic_and_1cm3_cube.jpg "fig:Nickel_electrolytic_and_1cm3_cube.jpg")處理過的鎳礦瘤，從圖右礦瘤上的孔中可見有由鎳[電解質構成的綠色鹽晶體](../Page/電解質.md "wikilink")。\]\]

### 電精煉

第二種常見的精煉方法就是，把金屬的硫滓瀝取到鎳的鹽溶液中，然後對鎳溶液使用電解冶金法，這樣就能在陰極的表面上形成電解鎳\[51\]。

### 蒙德法

[Nickel_kugeln.jpg](https://zh.wikipedia.org/wiki/File:Nickel_kugeln.jpg "fig:Nickel_kugeln.jpg")所作的高純度鎳球。\]\]

要從氧化鎳中取得最高純度的鎳就要用到[蒙德法](../Page/蒙德法.md "wikilink")，它可將鎳精礦的純度提升至高於99.99%\[52\]。這種方法的[專利由出生於德國的英國化學家](../Page/專利.md "wikilink")[路德維希·蒙德](../Page/路德維希·蒙德.md "wikilink")（Ludwig
Mond）取得，並於20世紀開始前就已經被工業生產所使用。鎳在蒙德法中於40–80 °C的溫度下與[一氧化碳反應](../Page/一氧化碳.md "wikilink")，生成[四羰基鎳](../Page/四羰基鎳.md "wikilink")。鐵也會在同樣的反應中生成[五羰基鐵](../Page/五羰基鐵.md "wikilink")，但反應速度緩慢。如有需要的話，可用蒸餾法分離。這過程中也會生成[八羰基二鈷](../Page/八羰基二鈷.md "wikilink")，但它在反應溫度下會分解成[十二羰基四鈷](../Page/十二羰基四鈷.md "wikilink")，一種不具揮發性的固體\[53\]。

有兩種方法可以從四羰基鎳中再提取鎳。第一種方法，把四羰基鎳在高溫下傳進反應室，反應室內有數萬粒的鎳珠，一直被持續攪拌。然後四羰基鎳就會分解出純鎳，並依附到鎳珠的表面上。第二種方法，把四羰基鎳在230 °C的溫度下傳進較小的反應室，它會分解出細粉末狀的純鎳。分解副產品一氧化碳在蒙德法中會被循環再用。用這方法生成的高純度鎳被稱為“羰基鎳”\[54\]。

### 金屬價值

鎳的市場價格於2006年至2007年初期一直大輻攀升；以2007年4月5日為準，鎳的交易價格為每[公噸](../Page/公噸.md "wikilink")52,300美元，或每[盎司](../Page/盎司.md "wikilink")1.47美元\[55\]。價格在這高峰過後又大幅回落，以2013年9月19日為準，鎳的交易價格則為每公噸13,788美元，或每盎司0.39美元\[56\]

[5美分硬幣含有](../Page/5美分硬幣.md "wikilink")1.25克的鎳（0.04盎司），以2007年4月的價格結算，值6.5美分；再加上3.75克的銅，值3美分；所以這個硬幣的金屬值9美分。由於5美分硬幣面值只有5美分，所以很多人想把硬幣熔掉賺錢。然而，[美國鑄幣局有見及此](../Page/美國鑄幣局.md "wikilink")，已於2006年12月14日開始執行法例，並有30天公眾諮詢期，凡熔掉或出口1美分或5美分硬幣即屬違法\[57\]。最高判處罰款一萬美元及／或入獄五年。

以2013年9月19日為準，5美分硬幣（含鎳及銅）熔掉後的價值為0.0450258美元，為面值的90%\[58\]。

## 應用

[Turbinenschaufel_RB199.jpg](https://zh.wikipedia.org/wiki/File:Turbinenschaufel_RB199.jpg "fig:Turbinenschaufel_RB199.jpg")（RB189型）的渦輪機葉片\]\]
現時美國鎳用途佔產量的比例如下：46%用於生產鎳鋼，34%用於生產非鐵[合金及](../Page/合金.md "wikilink")[高溫合金](../Page/高溫合金.md "wikilink")，14%用於電鍍，剩下的6%則屬其他用途\[59\]\[60\]。

鎳被用於各種特定及容易認出的工業品及消費品，其中包括[不鏽鋼](../Page/不鏽鋼.md "wikilink")、[鋁鎳鈷磁鐵](../Page/鋁鈷鎳合金.md "wikilink")、[硬幣](../Page/硬幣.md "wikilink")、[蓄電池](../Page/蓄電池.md "wikilink")、[電吉他弦線](../Page/電吉他.md "wikilink")、[麥克風收音盒及多種特殊合金](../Page/麥克風.md "wikilink")。特別需要強調的是，鎳是一種合金金屬，它的主要用途是鎳鋼及鎳[鑄鐵](../Page/鑄鐵.md "wikilink")，而它們的種類繁多。鎳還被廣泛用於其他合金，例如鎳黃銅及鎳青銅，及含有各種金屬元素的其他合金（如[英高鎳](../Page/英高鎳.md "wikilink")、[英高合金](../Page/英高合金.md "wikilink")、[莫內爾合金及](../Page/莫內爾合金.md "wikilink")[鎳蒙克合金](../Page/鎳蒙克合金.md "wikilink")），而各種合金元素則包括銅、鉻、鋁、鉛、鈷、銀及金\[61\]。

[MagnetEZ.jpg](https://zh.wikipedia.org/wiki/File:MagnetEZ.jpg "fig:MagnetEZ.jpg")製作的“馬蹄磁鐵”。鋁鎳鈷合金的成份一般為8-12%鋁，15-26%鎳，5-24%鈷，最多1%的鈦，而餘下的則用鐵。其後發現了鐵、鈷、鎳的一種合金的[矯頑性比當時最好磁鐵高出一倍後](../Page/矯頑性.md "wikilink")，鋁鎳鈷合金的研發就在1931年開始了。鋁鎳鈷合金磁鐵現時在多種應用上正被[稀土磁鐵所取代](../Page/稀土磁鐵.md "wikilink")。\]\]

由於鎳具有良好的抗腐蝕性，所以以前的人偶爾會用鎳來代替裝飾用的銀。1859年開始，有些國家偶爾會把鎳用作便宜的鑄幣原料（見上文），但到了20世紀後期硬幣中的鎳基本已被較便宜[不鏽鋼](../Page/不鏽鋼.md "wikilink")（即[鐵](../Page/鐵.md "wikilink")）所取代，而美國硬幣則是這趨勢中重要的例外。

對某些貴金屬而言，鎳是一種極佳的合金用劑，因此鎳被用於所謂的火試金法，專門探收各種[鉑系元素](../Page/鉑系元素.md "wikilink")\[62\]。就這一點而言，鎳能夠從鉑系元素的礦石中探收到全部六種的元素，甚至還能稍微地探收到一點金。高通量的鎳礦也可能從事其他鉑系元素的開採（主要是[鉑和](../Page/鉑.md "wikilink")[鈀](../Page/鈀.md "wikilink")），這類礦場的例子有俄羅斯的諾里爾斯克和加拿大的索德柏立盆地。

[發泡鎳及網格鎳可被用於](../Page/發泡金屬.md "wikilink")[鹼性燃料電池的](../Page/鹼性燃料電池.md "wikilink")[氣體擴散電極](../Page/氣體擴散電極.md "wikilink")\[63\]\[64\]。

鎳及其合金常被用作[氫化反應的催化劑](../Page/氫化.md "wikilink")。[雷尼鎳是一種常用的鎳催化劑形式](../Page/雷尼鎳.md "wikilink")，它是一種有多孔結構的鎳鋁合金，但很多時候也會用其他催化劑，例如相關的“雷尼型”催化劑。

鎳是一種天然的磁致伸縮材料，亦即是說，在[磁場下這種材料的長度會有少許改變](../Page/磁場.md "wikilink")\[65\]。而就鎳的個案而言，長度的變化是減少的（即材料收縮），又稱負磁致伸縮，輻度約為一百萬分之五十。

鎳也被用於燒結[碳化鎢或其他硬金屬工業品](../Page/碳化鎢.md "wikilink")，用量約為重量的6-12%。鎳可使碳化鎢帶磁性，並為燒結碳化鎢部件提供抗腐蝕性，不過它的硬度就比燒結用的鈷要低\[66\]。

[鎳氫電池](../Page/镍氢电池.md "wikilink")，可充電重複使用的環保電池。

## 在生物中的用途

儘管到1970年代才被確認，但鎳在微生物和植物的生理上有着重要的角色\[67\]\[68\]。植物酶[脲酶](../Page/脲酶.md "wikilink")（一種促進[尿素水解的酶](../Page/尿素.md "wikilink")）中就含有鎳。鎳鐵類[氫化酶除含有](../Page/氫化酶.md "wikilink")[鐵硫簇以外還含有鎳](../Page/鐵硫簇.md "wikilink")。這種鎳鐵類氫化酶的特性就是能使氫氧化。有一種含鎳的[四吡咯](../Page/四吡咯.md "wikilink")[輔酶](../Page/輔酶.md "wikilink")──輔因子F430，可在甲基[輔酶M還原酶中找到](../Page/輔酶M.md "wikilink")，該還原酶是產甲烷[古菌的能量來源](../Page/古菌.md "wikilink")。其中一種的一氧去氫酶含有鐵鎳硫簇\[69\]。其他含鎳的酶包括一種罕見的細菌類[超氧化物歧化酶](../Page/超氧化物歧化酶.md "wikilink")\[70\]，和存在於細菌及幾種寄生於[錐體蟲的真核寄生體中的](../Page/錐體蟲.md "wikilink")[乙二醛酶I](../Page/乙二醛酶I.md "wikilink")\[71\]（在如酵母菌及哺乳類等較高等生物中的這種酶所用的是二價電子的[鋅](../Page/鋅.md "wikilink")，Zn<sup>2+</sup>\[72\]\[73\]\[74\]\[75\]\[76\]）。

## 毒性

美國政府為鎳及其化合物設定了的最低風險量，其量為在15-364天期間吸入0.2 µg/m<sup>3</sup>\[77\]。一般相信[硫化鎳的煙霧及塵埃為](../Page/硫化鎳.md "wikilink")[致癌物質](../Page/致癌物質.md "wikilink")，及其他各種鎳的化合物也有可能是致癌的\[78\]\[79\]。[四羰基鎳](../Page/四羰基鎳.md "wikilink")\[Ni(CO)<sub>4</sub>\]是一種毒性很強的氣體。金屬羰基化合物的毒性取決於該金屬本身的毒性，及該羧基化合物釋出劇毒[一氧化碳氣的能力](../Page/一氧化碳.md "wikilink")，而四羰基鎳也不例外；而且四羰基鎳在空氣中會爆炸\[80\]\[81\]。

美國規定的每天鎳飲食攝取最大耐受量為1000 µg\[82\]，而估計的平均鎳攝取量為每天69-162 µg\[83\]。相對大量的鎳（與[鉻](../Page/鉻.md "wikilink")）會在煮食過程中從不鏽鋼廚具[瀝取到食物中](../Page/瀝取.md "wikilink")，其量與每天平均攝取量相若。例如在煮過10次後，一份蕃茄醬的含鎳量就有88 µg\[84\]\[85\]。

過敏體質的人可能會對鎳有[過敏反應](../Page/過敏反應.md "wikilink")，造成皮膚過敏，即[皮膚炎](../Page/皮膚炎.md "wikilink")。而[汗皰疹的患者可能也會對鎳過敏](../Page/汗皰疹.md "wikilink")。鎳是接觸性過敏的一大來源，部份成因是作[耳環用的珠寶首飾上的鍍鎳](../Page/耳環.md "wikilink")\[86\]。受鎳過敏形響的耳洞一般會發紅並變癢。由於這個問題，所以現時不少耳環都採用了不含鎳的材料。對於會於與人體皮膚接觸的產品，其最大可含鎳量是由[歐盟所管制的](../Page/歐盟.md "wikilink")。在2002年，研究人員發現1歐元及2歐元的硬幣含鎳量遠高於標準。相信是由[電鍍反應所造成的](../Page/電鍍.md "wikilink")\[87\]。鎳在2008年獲美國接觸性皮膚炎協會選為年度過敏原。\[88\]

報告指出，缺氧誘導因子（HIF-1）的鎳誘導活化和缺氧誘導基因的調升，都是由細胞的[抗壞血酸鹽的水平低下所引致](../Page/抗壞血酸鹽.md "wikilink")。在培養基中加入抗壞血酸鹽後，細胞內的抗壞血酸鹽水平增加，然後由金屬誘導穩定化的HIF-1與取決於HIF-1α的基因表象都有了逆轉\[89\]\[90\]。

## 另見

  - [:分類:鎳化合物](../Page/:分類:鎳化合物.md "wikilink")
  - [雷尼鎳](../Page/雷尼鎳.md "wikilink")
  - [高溫合金](../Page/高溫合金.md "wikilink")

## 參考資料

[Category:过渡金属](../Category/过渡金属.md "wikilink")
[镍](../Category/镍.md "wikilink")
[4J](../Category/第4周期元素.md "wikilink")
[4J](../Category/化学元素.md "wikilink")
[Category:铁磁性材料](../Category/铁磁性材料.md "wikilink")

1.

2.

3.

4.

5.
6.  G.L. Miessler and D.A. Tarr, "Inorganic Chemistry" (2nd ed.,
    Prentice–Hall 1999) p.38

7.  R.H. Petrucci et al “General Chemistry” (8th ed., Prentice–Hall
    2002) p.950

8.  [NIST Atomic Spectrum
    Database](http://physics.nist.gov/PhysRefData/ASD/levels_form.html)
    要看鎳的原子能階的話，請於能譜查詢盒內輸入"Ni I"然後按讀取資料。

9.
10.

11. Fewell, M. P.. *The atomic nuclide with the highest mean binding
    energy. American Journal of Physics* 63 (7): 653–58. .
    URL:<http://adsabs.harvard.edu/abs/1995AmJPh>..63..653F. Accessed:
    2011-03-22. (Archived by WebCite® at
    <http://www.webcitation.org/5xNHry2gq>)

12.

13.

14.

15.
16.

17.

18. [National Pollutant Inventory – Nickel and compounds Fact
    Sheet](http://www.npi.gov.au/substances/nickel/index.html).
    Npi.gov.au. Retrieved on 2012-01-09.

19.

20.
21.

22.

23.

24. Lascelles, K.; Morgan, L. G.; Nicholls, D.; Beyersmann, D. (2005),
    "Nickel Compounds", Ullmann's Encyclopedia of Industrial Chemistry,
    Weinheim: Wiley-VCH, <doi:10.1002/14356007.a17_235.pub2>

25. "The Extraction of Nickel from its Ores by the Mond Process".
    *Nature*. **59** (1516): 63–64. 1898.
    [Bibcode](https://en.wikipedia.org/wiki/Bibcode):\[<http://adsabs.harvard.edu/abs/1898Natur>..59...63.
    1898Natur..59...63.\].
    [doi](https://en.wikipedia.org/wiki/Digital_object_identifier):[10.1038/059063a0](https://doi.org/10.1038%2F059063a0).

26.

27. Keith Lascelles, Lindsay G. Morgan, David Nicholls, Detmar
    Beyersmann “Nickel Compounds” in Ullmann's Encyclopedia of
    Industrial Chemistry 2005, Wiley-VCH, Weinheim.

28.
29. 李琪, 乔庆东. 有机电化学法合成二茂镍的研究\[J\]. 石油化工高等学校学报, 2010. 23(3): 58-61, 67

30.
31.

32.
33.

34.

35. *Chambers Twentieth Century Dictionary*, p888, W\&R Chambers Ltd,
    1977.

36.

37.

38.

39.

40.
41.

42.
43.  Canada used nickel plating on its five-cent coins in 1945

44.
45.
46.

47.

48.

49.

50.
51.
52.

53.
54.

55.

56. [Nickel Prices and Nickel Price
    Charts](http://www.infomine.com/investment/metal-prices/nickel/)

57. [United States Mint Moves to Limit Exportation & Melting of
    Coins](http://www.usmint.gov/pressroom/index.cfm?action=press_release&ID=724),
    The United States Mint, press release, December 14, 2006

58.

59.
60.

61.

62.

63.

64.

65. [UCLA – Magnetostrictive Materials
    Overview](http://aml.seas.ucla.edu/research/areas/magnetostrictive/overview.htm)
    . Aml.seas.ucla.edu. Retrieved on 2012-01-09.

66.

67.

68.  electronic-book ISBN 978-94-007-5561-1  electronic-

69.

70.

71.

72.

73.

74.

75.

76.

77. [ToxGuideTM for
    Nickel](http://www.atsdr.cdc.gov/toxguides/toxguide-15.pdf). U.S.
    Department of Health and Human Services. Agency for Toxic Substances
    and Disease Registry

78.

79.

80.

81.

82.

83.

84.

85.

86.

87.

88.

89.

90.