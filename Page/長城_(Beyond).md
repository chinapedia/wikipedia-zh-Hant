《**長城**》是一首由[香港殿堂級搖滾樂隊](../Page/香港.md "wikilink")[Beyond創作的歌曲](../Page/Beyond.md "wikilink")，原曲為[粵語](../Page/粵語.md "wikilink")，由[黃家駒作曲及擔當主音](../Page/黃家駒.md "wikilink")、[劉卓輝作詞](../Page/劉卓輝.md "wikilink")；日語版由[真名杏樹作詞](../Page/真名杏樹.md "wikilink")，[梁邦彥編曲](../Page/梁邦彥.md "wikilink")；國語版歌詞由[詹德茂改編](../Page/詹德茂.md "wikilink")。粵語版本收錄於[Beyond第](../Page/Beyond.md "wikilink")8張專輯《[繼續革命](../Page/繼續革命.md "wikilink")》，並為該大碟之主打歌；日語版本收錄於《[超越](../Page/超越.md "wikilink")》，國語版本收錄於《[信念](../Page/信念.md "wikilink")》。

## 意義

黃家駒在一段由香港[無綫電視為此曲製作的音樂錄像表示](../Page/無綫電視.md "wikilink")，寫此歌是要「描寫中國人一貫的民族意識」。在歌詞裡，長城反映一個封閉的國度，是強權暴政的產物，是犧牲了無數血肉之軀築成的，然而後人大多-{只}-會以它為榮，無視值得反思之處。歌詞是借物描寫這種民族思想和境況，並借古諷今，並非-{只}-是寫長城和遠古的中國。

黃家駒弟弟、同時亦為Beyond低音結他手的[黃家強則在香港電台](../Page/黃家強.md "wikilink")《不死傳奇》中表示，黃家駒創作此曲是想寫1988年《[大地](../Page/大地.md "wikilink")》的延續篇，因為《長城》與《大地》在題材與音樂風格均非常相似，充滿中國色彩，並描述中國人的矛盾。\[1\]

## 參考資料

[Category:1992年歌曲](../Category/1992年歌曲.md "wikilink")
[Category:香港歌曲](../Category/香港歌曲.md "wikilink")

1.