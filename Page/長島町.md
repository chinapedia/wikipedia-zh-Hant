**長島町**（）是位於[日本](../Page/日本.md "wikilink")[鹿兒島縣西北部的一的](../Page/鹿兒島縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，轄區包括[長島本島](../Page/長島_\(鹿兒島縣\).md "wikilink")、[諸浦島](../Page/諸浦島.md "wikilink")、[伊唐島](../Page/伊唐島.md "wikilink")、[獅子島及周邊共](../Page/獅子島.md "wikilink")23個[島嶼](../Page/島嶼.md "wikilink")\[1\]；成立于2006年3月20日，由舊長島町與[東町合併而成](../Page/東町_\(鹿兒島県\).md "wikilink")。

## 歷史

[鎌倉時代長島為](../Page/鎌倉時代.md "wikilink")[長島氏的領地](../Page/長島氏.md "wikilink")，並築有[堂崎城](../Page/堂崎城.md "wikilink")，在戰國時代成為其在海上活耀的據點。1565年[島津氏攻下長島](../Page/島津氏.md "wikilink")，成為島津的領地。[江戶時代長島為](../Page/江戶時代.md "wikilink")[薩摩藩的外城](../Page/薩摩藩.md "wikilink")，並設置長島鄉；[廢藩置縣後屬於鹿兒島縣](../Page/廢藩置縣.md "wikilink")，並在實施町村制後，分屬東長島村和西長島村。\[2\]

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，線在的轄區在當時分屬東長島村和西長島村。
  - 1956年7月10日：東長島村改制並改名為[東町](../Page/東町_\(鹿兒島縣\).md "wikilink")。
  - 1960年1月1日：西長島村改制並改名為長島町。
  - 2006年3月20日：長島町和東町合併為新設置的**長島町**。

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1910年</p></th>
<th><p>1910年-1930年</p></th>
<th><p>1930年-1950年</p></th>
<th><p>1950年-1970年</p></th>
<th><p>1970年-1990年</p></th>
<th><p>1990年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>東長島村</p></td>
<td><p>1956年7月10日<br />
改制並改名為東町</p></td>
<td><p>2006年3月20日<br />
合併為長島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>西長島村</p></td>
<td><p>1960年1月1日<br />
改制並改名為長島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

轄區內無[機場也無](../Page/機場.md "wikilink")[鐵路通過](../Page/鐵路.md "wikilink")，靠[黑之瀨戶大橋與](../Page/黑之瀨戶大橋.md "wikilink")[九州相連](../Page/九州_\(日本\).md "wikilink")。

## 姊妹、友好都市

### 海外

  - [吉祥面](../Page/吉祥面.md "wikilink")（[大韓民國](../Page/大韓民國.md "wikilink")[仁川廣域市](../Page/仁川廣域市.md "wikilink")[江華郡](../Page/江華郡.md "wikilink")）：於1994年5月30日締結姊妹都市

## 參考資料

## 外部連結

  - [長島地區合併協議會](https://web.archive.org/web/20080623222549/http://www.town.nagashima.lg.jp/nagashima99/nag/index.html)

  - [舊長島町網頁](https://web.archive.org/web/20080311003909/http://www.town.nagashima.lg.jp/nagashima99/nagashima/default.asp)

  - [舊東町網頁](https://web.archive.org/web/20080623222409/http://www.town.nagashima.lg.jp/nagashima99/azuma/index.htm)

  - [東町商工會](http://azuma.kashoren.or.jp/)

  - [東町漁協](http://www.azuma.or.jp/)

<!-- end list -->

1.
2.