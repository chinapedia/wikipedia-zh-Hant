**長崎放送株式會社**（****，英文名稱**Nagasaki Broadcasting
Company**，簡稱**NBC**）是日本的一家電視臺兼廣播電臺。其中波廣播放送範圍為[長崎縣和](../Page/長崎縣.md "wikilink")[佐賀縣](../Page/佐賀縣.md "wikilink")，電視放送範圍為長崎縣。電視服務為[JNN聯播網成員](../Page/日本新聞網_\(TBS\).md "wikilink")，廣播服務為[JRN](../Page/JRN.md "wikilink")・[NRN聯播網成員](../Page/NRN.md "wikilink")。電視呼出訊號為JOUR-DTV，廣播呼出訊號為JOUR。
電視服務開始于1959年1月1日，廣播服務開始于1953年3月1日。

## 外部連結

  - [長崎放送](http://www.nbc-nagasaki.co.jp/)
  - [NBC廣播佐賀](http://www.nbc-saga.jp/)

[Category:日本媒體](../Category/日本媒體.md "wikilink")
[N](../Category/日本新聞網_\(TBS\).md "wikilink")