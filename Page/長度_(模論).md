在[數學中](../Page/數學.md "wikilink")，設 \(A\)
為[環](../Page/環.md "wikilink")，一個
\(A\)-[模](../Page/模.md "wikilink")
之**長度**是一個[整數](../Page/整數.md "wikilink")（包括無窮大），它推廣了[向量空間的](../Page/向量空間.md "wikilink")[維度](../Page/維度.md "wikilink")。有限長度的模與有限維向量空間有許多共通性。

## 動機

[單模是除了零和本身外沒有子模的](../Page/單模.md "wikilink")[模](../Page/模.md "wikilink")，這種模有時也稱為*不可約模*。例如不可約的向量空間（視為[域或](../Page/域.md "wikilink")[除環上的模](../Page/除環.md "wikilink")）是一條直線。對於單模，我們只可能造出一種嚴格遞增的子模鏈：

  -
    \(\{0\} \subsetneq M\)

單模是容易處理的對象。對於一個[環](../Page/環.md "wikilink") \(A\) 上的 \(A\)-模
\(M\)，如果我們能找到一條嚴格遞增的子模鏈：

  -
    \(M_0 = \{0\} \subsetneq M_1 \subsetneq \cdots \subsetneq M_{n-1} \subsetneq M_n = M\)

使得每個子商 \(M_k/M_{k-1}\) 都是單模，那麼此鏈將是極大的——我們無法插入新的子模。根據以下將闡述的定義，這時 \(M\)
將是有限長度的模，其長度 \(\ell_R(M)\)恰為 \(n\)。

因此單模正好是長度為一的模。另一個例子：設 \(E\) 是域 \(k\) 上的有限維向量空間，那麼一個極大的子模鏈是一族子空間
\((E_k)_{0 \leq k}\)，使得維度在每一步都加一：

  -
    \(E_0 = \{0\} \subsetneq E_1 \subsetneq \cdots \subsetneq E_{n-1} \subsetneq E_n = E\)

而此時 \(\dim_k E = \ell_k(E)\)，這種資料稱作**旗**。

## 定義

設 \(A\) 為一個[環](../Page/環.md "wikilink")（可能非交換）， 一個 \(A\)-模 \(M\)
的**長度**定義為嚴格遞增的子模鏈長度的[上確界](../Page/上確界.md "wikilink")：此即最大可能的整數
\(n\)（可能是無窮大），使得 \(M\) 中存在嚴格遞增的子模鏈
\(M_0 \subsetneq M_1 \subsetneq \cdots \subsetneq M_n\)。模 \(M\) 的長度記為
\(\ell_A(M)\)，不致混淆時也逕寫作 \(\ell(M)\)。

## 例子

  - 模 \(M\) 是[單模的充要條件是長度為一](../Page/單模.md "wikilink")。
  - 對於向量空間，長度等於維度。
  - 整數環 \(\Z\) 視為 \(\Z\)-模，則其長度為無窮大，因為存在任意長的子模鏈
    \(2^n \Z \subsetneq 2^{n-1} \Z \subsetneq \cdots \subsetneq 2 \Z \subsetneq \Z\)。
  - 設正整數 \(n\) 的素因數分解為 \(n = \prod_p p^{n_p}\)，則有

<!-- end list -->

  -
    \(\ell_\Z(\Z / n\Z) = \sum_p n_p\)

## 性質

有限長的模具有許多類似有限維向量空間的性質。例如：若 \(M\) 為有限長模，則其子模皆有限長，設 \(N, P\)
為兩個子模，\(\ell(N) = \ell(P)\) 且 \(N \subseteq P\)，則 \(N=P\)。

我們有 Grassman 公式：

  -
    \(\ell(N + P) + \ell(N \cap P) = \ell(N) + \ell(P)\)

對於有限長模 \(M\)，一個極大的子模鏈
\(\{0\} = M_0 \subsetneq \cdots \subsetneq M_n = M\)
稱為一個[合成列](../Page/合成列.md "wikilink")，其長度 \(n\)
是固定的，且合成因子 \(M_i/M_{i+1}\)
在至多差一個[置換與同構的意義下唯一](../Page/置換.md "wikilink")。

此外，一個模是有限長模若且唯若它同時是[阿廷模與](../Page/阿廷模.md "wikilink")[諾特模](../Page/諾特模.md "wikilink")。

## 文獻

  - Serge Lang, *Algebra* (2002), Graduate Texts in Mathematics 211,
    Springer. ISBN 0-387-95385-X

[C](../Category/交換代數.md "wikilink") [C](../Category/模論.md "wikilink")
[Category:长度](../Category/长度.md "wikilink")