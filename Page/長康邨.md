[Cheung_Hong_Estate_Phase_2_2008.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Hong_Estate_Phase_2_2008.jpg "fig:Cheung_Hong_Estate_Phase_2_2008.jpg")
[Cheung_Hong_Estate_Open_Space.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Hong_Estate_Open_Space.jpg "fig:Cheung_Hong_Estate_Open_Space.jpg")
[Ching_Shing_Court_Gym_Zone.jpg](https://zh.wikipedia.org/wiki/File:Ching_Shing_Court_Gym_Zone.jpg "fig:Ching_Shing_Court_Gym_Zone.jpg")
[Cheung_Hong_Estate_Car_Park.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Hong_Estate_Car_Park.jpg "fig:Cheung_Hong_Estate_Car_Park.jpg")

**長康邨**（英語：**Cheung Hong
Estate**）是香港的一個[公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[葵青區](../Page/葵青區.md "wikilink")[青衣島南部](../Page/青衣島.md "wikilink")，[長青邨以西](../Page/長青邨.md "wikilink")，屬青衣島內最大型的公共屋邨，由[香港房屋委員會負責屋邨管理](../Page/香港房屋委員會.md "wikilink")。初期屬[長青邨發展計劃的擴展部分](../Page/長青邨.md "wikilink")，直至較新的部分於1979年落成，因屋邨管理上的需要，遂把較新落成的H至N座樓宇，分拆並易名為長康邨及長康商場方便管理；其後到1985至86年間，長康邨P至S座的Y型大廈部分、O座青盛苑及青華苑的落成，有大量居民遷入，而房委會亦預計到該部分的居民對商場及社康設施的需求大幅增加，原有設施也顯得不敷應用，故此在長康邨Y型部分，增設長康第二商場、一系列社康設施（如：學校、熟食亭、社福機構租用單位、室內體育館及有蓋巴士總站等），還設一個行人天橋網絡，貫通各座Y型樓宇、青盛苑、青華苑及以小徑及天橋連接長康邨原有部分的康和樓6樓，以全盤計劃去一次過解決居民上下山及社康設施不足的問題。

長康邨三期亦是26座問題公屋中葵青區受影響屋邨居民的指定安置屋邨（另一個是沙田[顯徑邨](../Page/顯徑邨.md "wikilink")，已於2000年起拆售）。

**青盛苑**是[居者有其屋的屋苑](../Page/居者有其屋.md "wikilink")，為青衣島首個居屋屋苑，為長康邨發展計劃的O座樓宇，在入伙前獲選為居屋屋苑發售，此屋苑位於長康邨康豐樓的旁邊，與康豐樓一樣為Y2型設計，但用料則比較高級，在長康邨設有一道天橋連接青盛苑二樓大堂及長康邨其他Y型樓宇、以至長康第二商場，全苑只有一座34層高樓宇，地下、1樓及2樓的17-24室為社會福利機構租賃單位，於1985年落成。

## 屋邨資料

### 樓宇

#### 長康邨

| 樓宇名稱（座號） | 樓宇類型                               | 落成年份 | 期數（不設一期）                                                                                           |
| -------- | ---------------------------------- | ---- | -------------------------------------------------------------------------------------------------- |
| 康華樓（I座）  | [雙工字型](../Page/雙工字型.md "wikilink") | 1979 | 二期；雙工字型樓宇為[26座問題公屋醜聞中](../Page/26座問題公屋醜聞.md "wikilink")[葵芳邨受影響居民指定接收樓宇](../Page/葵芳邨.md "wikilink") |
| 康富樓（J座）  |                                    |      |                                                                                                    |
| 康安樓（N1座） | 1984                               |      |                                                                                                    |
| 康盛樓（N2座） |                                    |      |                                                                                                    |
| 康平樓（M座）  | 單工字型                               |      |                                                                                                    |
| 康榮樓（H座）  | 1980                               |      |                                                                                                    |
| 康貴樓（K座）  | [舊長型](../Page/舊長型.md "wikilink")   |      |                                                                                                    |
| 康和樓（L1座） |                                    |      |                                                                                                    |
| 康泰樓（L2座） | 1982                               |      |                                                                                                    |
| 康祥樓（Q座）  | [Y1型](../Page/Y1型.md "wikilink")   | 1985 | 三期；[26座問題公屋醜聞受影響居民指定接收樓宇](../Page/26座問題公屋醜聞.md "wikilink")                                         |
| 康豐樓（P座）  | [Y2型](../Page/Y2型.md "wikilink")   |      |                                                                                                    |
| 康順樓（R座）  | Y1型                                | 1986 |                                                                                                    |
| 康美樓（S座）  | Y2型                                |      |                                                                                                    |
|          |                                    |      |                                                                                                    |

（註：因發展計劃內的O座已經轉作居者有其屋計劃屋苑的關係，部分座號被略去。上述座號是以房屋署Estate Property 的紀錄為依歸。）

#### 青盛苑

| 樓宇名稱（座號）\[1\] | 樓宇類型 | 落成年份 |
| ------------- | ---- | ---- |
| 青盛苑（O座）       | Y2型  | 1985 |
|               |      |      |

## 教育及福利設施

[Po_Leung_Kuk_Chan_Yat_Primary_School.jpg](https://zh.wikipedia.org/wiki/File:Po_Leung_Kuk_Chan_Yat_Primary_School.jpg "fig:Po_Leung_Kuk_Chan_Yat_Primary_School.jpg")

### 中學

  - [保良局羅傑承（一九八三）中學](../Page/保良局羅傑承（一九八三）中學.md "wikilink")
  - [樂善堂梁植偉紀念中學](../Page/樂善堂梁植偉紀念中學.md "wikilink")

### 小學

  - [保良局陳溢小學](http://www.plkcy.edu.hk)（1984年創辦）
  - [青衣商會小學](../Page/青衣商會小學.md "wikilink")（1984年創辦）

### 幼稚園

  - [保良局田家炳幼稚園](http://www.kids-club.net/edu/tkpkg)（1985年創辦）（位於長康邨青盛苑地下第1層）
  - [世佛會文殊幼兒學校](http://www.wfb.edu.hk/2/home.php)（1987年創辦）（位於長康邨康安樓地下）

<!-- end list -->

  - *已結束*

<!-- end list -->

  - 神召會華惠幼稚園（長康）（1985年創辦）（位於長康邨康平樓地下）
  - 新界婦孺福利會長康邨兒童樂園（位於長康邨康美樓地下）

### 綜合家庭服務中心

  - 社會福利署南青衣綜合家庭服務中心（位於長康邨康美樓A翼地下）

### 兒童及青年中心

  - [明愛長康兒童及青少年中心](http://chcy.caritas.org.hk/)（位於長康邨青盛苑地下第2層）

### 安老院

  - [仁濟醫院香港半島獅子會安老院](http://www.ychss.org.hk/elder/elderly-hostelmeal/plc)（位於長康邨康順樓3樓至4樓）

### 長者鄰舍中心

  - [萬國宣道浸信聯會長康浸信會長者鄰舍中心](http://www.abwe.org.hk/fellowship.php?pkey=2&cat_pkey=)（位於長康邨康順樓C翼7-12號地下）

### 家舍

  - [扶康會長康之家](https://www.fuhong.org/PageInfo.aspx?md=20000&cid=274)（1996年創辦）（位於長康邨康和樓2樓211至40號室）

## 設施

### 第二期

長康邨第二期設有多項設施，包括兒童遊樂場、羽毛球場、足球場、籃球場、乒乓球檯、停車場、健體區及休憩區。

Cheung Hong Estate Football Field.jpg|足球場 Cheung Hong Estate Basketball
Court.jpg|籃球場 Cheung Hong Estate Table Tennis Zone.jpg|乒乓球檯 Cheung Hong
Estate Playground and Badminton Court.jpg|兒童遊樂場及羽毛球場 Cheung Hong Estate
Gym Zone and Sitting Area.jpg|健體及休憩區 Cheung Hong Estate Gym Zone (4) and
Pebble Walking Trail.jpg|健體區（2）及卵石路步行徑

### 第三期

長康邨第三期採用平台設計，康祥樓及康豐樓之間的基座用作停車場，2樓設有蓋籃球場。而康順樓及康美樓之間的基座用作長康第二商場及長康第二街市，2樓設有蓋遊樂場。其化設施包括2個[冬菇亭](../Page/冬菇亭.md "wikilink")、兒童遊樂場、籃球場、健體區及休憩區。

Cheung Hong Estate Phase 3 Covered Games Area 201707.jpg|康順樓及康美樓之間的有蓋遊樂場
Cheung Hong Estate Phase 3 Podium Access 201707.jpg|平台 Cheung Hong
Estate Phase 3 Garden view 2017.jpg|休憩區 Cheung Hong Estate Phase 3 Wall
2017.jpg|特色外牆 Cheung Hong Estate Playground and Gym Zone
(2).jpg|兒童遊樂場及健體區 Cheung Hong Estate Playground (3,
brighter).jpg|兒童遊樂場（2） Cheung Hong Estate Gym Zone (3,
brighter).jpg|健體區（2）

### 商場

長康邨設有13萬平方呎商場及700個車位停車場。分為第一商場和第二商場，但兩處並不連結，需要7-8分鐘路程才可到達。

#### 長康第一商場

[Cheung_Hong_Estate_Shopping_Arcade_201705.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Hong_Estate_Shopping_Arcade_201705.jpg "fig:Cheung_Hong_Estate_Shopping_Arcade_201705.jpg")\]\]
[Cheung_Hong_Restaurant_Interior_201705.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Hong_Restaurant_Interior_201705.jpg "fig:Cheung_Hong_Restaurant_Interior_201705.jpg")

長康第一商場樓高4層，以小商店為主。地下設[惠康超級市場](../Page/惠康超級市場.md "wikilink")、藥房、文具店、髮型屋、青衣郵政局及[麥當勞等](../Page/麥當勞.md "wikilink")。1樓為長康酒樓、2樓為青衣長康普通科門診診所、3樓為保良局長康青少年發展中心，設天橋往[長青巴士總站](../Page/長青巴士總站.md "wikilink")。

長康商場2005年至2016年由領展持有，到12月推出物業招標。資深投資者林子峰與其他投資者組財團，以11.55億元購入青衣長康商場。\[2\]新業主上場後，自1981年開業的長康酒樓不獲續租，邨內唯一的酒樓於2017年5月16日結業。街坊概嘆失去聚舊地方。\[3\]。該處現時為喜悦皇宮酒樓，但開業同年的[冬至卻發生超額訂座致部分食客無法入座的事件](../Page/冬至.md "wikilink")\[4\]。

地下設街市以及熟食檔。

Cheung Hong Restaurant Entrance void
201705.jpg|結業前的長康酒樓入口大堂，梯邊的牆上擺放八仙過海圖
Cheung Hong Restaurant Dragon Decoration
201705.jpg|結業前的長康酒樓盡頭設紅幕上一對大龍鳳，[王晶新戲](../Page/王晶.md "wikilink")《[追龍](../Page/追龍.md "wikilink")》在這對大龍鳳下取景
Cheung Hong Estate Shopping Arcade Level 2 Clicnic 2017.jpg|青衣長康普通科門診診所
Cheung Kwai House Shops.jpg|康貴樓地下商店 Cheung Hong Estate Hong Tai House GF
Shops 201705.jpg|康泰樓地下商店 Cheung Hong Market No. 1.jpg|長康第一街市 Cheung Hong
Estate Cooked Food Stall.jpg|[冬菇亭熟食檔](../Page/冬菇亭.md "wikilink")

#### 長康第二商場

[Cheung_Hong_Estate_Commercial_Centre_No.2_Void_2017.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Hong_Estate_Commercial_Centre_No.2_Void_2017.jpg "fig:Cheung_Hong_Estate_Commercial_Centre_No.2_Void_2017.jpg")
長康第二商場分為主座及「康順樓及康美樓商場」兩部分。主座租戶包括小食店、麵包西餅店、髮廊、中國銀行、[萬寧](../Page/萬寧.md "wikilink")、[萬家便利超市](../Page/萬家便利超市.md "wikilink")、翠湖茶餐廳及基督教宣道會青衣堂暨好鄰舍中心。康順樓及康美樓商場環繞著長康第二街市，商戶以經營多年的民生小店為主，包括永發超級市場、德富興糧食雜貨公司、五金舖、藥行、祥興電業公司、祥發咖啡小食及金豐粉麵咖啡屋。不過商場有不少商店空置中。

Cheung Hong Estate Commercial Centre No.2 shops 2017.jpg|康順樓及康美樓商場 North
of Cheung Hong Commercial Centre No. 2 (revised).jpg|主座商場北面外貌 Cheung
Hong Estate Commercial Centre No.2 3rd Floor Lower Block.jpg|主座商場3樓平台東面
Cheung Hong Estate Commercial Centre No.2 3rd Floor Higher
Block.jpg|主座商場3樓平台西面 Cheung Hong Commercial Centre No.
2.jpg|長康第二商場南面及中庭（2014年） Cheung Hong Estate Market
No.2.jpg|長康第二街市魚檔及菜檔 Cheung Hong Market No.
2.jpg|長康第二街市[燒味檔及凍肉檔](../Page/燒味.md "wikilink")

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [房屋署長康邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2815)

[en:Public housing estates on Tsing Yi Island\#Cheung Hong
Estate](../Page/en:Public_housing_estates_on_Tsing_Yi_Island#Cheung_Hong_Estate.md "wikilink")

[Category:青衣島](../Category/青衣島.md "wikilink")

1.  [青盛苑](http://www.housingauthority.gov.hk/b5/interactivemap/court/0,,3-0-6_5747,00.html)
2.
3.
4.  [青衣酒樓冬至overbooked
    近百食客塞爆門口無飯開](https://hk.news.appledaily.com/breaking/realtime/article/20171222/57615991)