**長瀨智也**（），[日本](../Page/日本.md "wikilink")[偶像](../Page/偶像.md "wikilink")、[男演員](../Page/男演員.md "wikilink")，於[傑尼斯事務所旗下樂團型態的偶像團體](../Page/傑尼斯事務所.md "wikilink")[TOKIO中擔任](../Page/TOKIO.md "wikilink")[主唱](../Page/主唱.md "wikilink")，擅長詞曲創作，[神奈川縣出身](../Page/神奈川縣.md "wikilink")。

## 簡歷

長瀨於1990年進入傑尼斯事務所，1994年9月21日以[TOKIO團員的身份正式出道](../Page/TOKIO.md "wikilink")。

長瀨於1993年首度演出電視連續劇，1996年起主演的電視劇《[白線流](../Page/白線流.md "wikilink")》系列為其代表作之一，歷年主演包括《女婿大人》、《[池袋西口公園](../Page/池袋西口公園_\(小說\).md "wikilink")》、《[My☆Boss
My☆Hero](../Page/我的老大_我的英雄.md "wikilink")》等連續劇都有不錯的成績，也曾以《女婿大人》劇中主角「[櫻庭裕一郎](../Page/櫻庭裕一郎.md "wikilink")」的身份發行過2張個人單曲。2002年，長瀨憑著首部電影《絕命七十二小時》（ソウル）榮獲第15屆《[石原裕次郎新人獎](../Page/石原裕次郎.md "wikilink")》。

## 演出

### 連續劇

  - 1993 雙胞胎教師（ツインズ教師）[朝日電視台](../Page/朝日電視台.md "wikilink")
  - 1994 再見了螞蟻（アリよさらば）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 1994 任性的老師（先生はワガママ）[朝日電視台](../Page/朝日電視台.md "wikilink")
  - 1994 最喜歡爸爸了（好きやねん父ちゃん\!）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 1995 愛與野心的獨眼龍---伊達政宗（愛と野望の獨眼竜
    伊達政宗）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 1995 白雪情緣（最高の片想い）[富士電視台](../Page/富士電視台.md "wikilink")
  - 1995 私奔計畫（カケオチのススメ）[朝日電視台](../Page/朝日電視台.md "wikilink")
  - 1995 戀人啊（戀人よ）[富士電視台](../Page/富士電視台.md "wikilink")
  - 1996 親愛的女人
  - 1996
    [白線流](../Page/白線流.md "wikilink")（白線流し）[富士電視台](../Page/富士電視台.md "wikilink")
  - 1996 Dear Woman（Dear ウーマン）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 1997 長不齊的蘋果們４（ふぞろいの林檎たちⅣ）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 1997 超能偵查員（DxD）[日本電視台](../Page/日本電視台.md "wikilink")
  - 1998 成人禮（Days）[富士電視台](../Page/富士電視台.md "wikilink")
  - 1998 愛與情慾（ラブとエロス）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 1999
    [惡靈貞子](../Page/惡靈貞子.md "wikilink")（リング～最終章～）[富士電視台](../Page/富士電視台.md "wikilink")
  - 1999 砂上的戀人們（砂の上の戀人たち）[富士電視台](../Page/富士電視台.md "wikilink")
  - 2000
    [池袋西口公園](../Page/池袋西口公園_\(電視劇\).md "wikilink")（池袋ウエストゲートパーク）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 2001
    [女婿大人](../Page/女婿大人.md "wikilink")（ムコ殿）[富士電視台](../Page/富士電視台.md "wikilink")
  - 2001 半個醫生（ハンドク\!\!\!）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 2002 [BIG
    MONEY](../Page/金融小子.md "wikilink")（ビッグマネー\!～浮世の沙汰は株しだい～）[富士電視台](../Page/富士電視台.md "wikilink")
  - 2002 年輕爸爸（やんパパ）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 2003
    [女婿大人](../Page/女婿大人.md "wikilink")2003（ムコ殿2003）[富士電視台](../Page/富士電視台.md "wikilink")
  - 2004 她已不在人世（彼女が死んじゃった。）[日本電視台](../Page/日本電視台.md "wikilink")
  - 2005
    [虎與龍](../Page/虎與龍_\(電視劇\).md "wikilink")（タイガー&ドラゴン）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 2006 [My Boss, My Hero](../Page/我的老大_我的英雄.md "wikilink")（マイ★ボス
    マイ★ヒーロー）[日本電視台](../Page/日本電視台.md "wikilink")
  - 2007 歌姫 [TBS電視台](../Page/TBS電視台.md "wikilink")
  - 2009 [華麗間諜](../Page/華麗間諜.md "wikilink")
    （華麗なるスパイ）[日本電視台](../Page/日本電視台.md "wikilink")
  - 2010 [自戀刑警](../Page/自戀刑警.md "wikilink")
    （うぬぼれ刑事）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 2013 [小原不哭](../Page/小原不哭.md "wikilink")
    （泣くな、はらちゃん）[日本電視台](../Page/日本電視台.md "wikilink")
  - 2013 [黑河內](../Page/黑河內.md "wikilink")
    （クロコーチ）[TBS電視台](../Page/TBS電視台.md "wikilink")
  - 2016 [FRAGILE](../Page/FRAGILE.md "wikilink")
    （フラジャイル）[富士電視台](../Page/富士電視台.md "wikilink")
  - 2017 [對不起，我愛你](../Page/對不起，我愛你_\(日本電視劇\).md "wikilink")
    （ごめん､愛してる）[TBS電視台](../Page/TBS電視台.md "wikilink")

### 特別篇

  - 1995 甦醒吧，孩子（息子よ蘇れ\!）富士電視台
  - 1995 最喜歡爸爸了２（好きやねん父ちゃん\!2）TBS電視台
  - 1995 最喜歡爸爸了３（好きやねん父ちゃん\!3）TBS電視台
  - 1996 戀物語（戀物語 せつない夜は逢いたくて～その人の匂い）TBS電視台
  - 1997 阪本龍馬（竜馬がゆく）TBS電視台
  - 1997 Dear Woman特別篇（Dear ウーマンスペシャル）TBS電視台
  - 1997 白線流---十九歲的春天（白線流し－19の春－）富士電視台
  - 1999 白線流---二十歲的風（白線流し－二十歳の風－）富士電視台
  - 2000 三億元事件（3億円事件 20世紀最後の謎）富士電視台
  - 2001 白線流---啟程的詩（白線流し－旅立ちの詩－）富士電視台
  - 2003 白線流---二十五歲（白線流し－二十五歳）富士電視台
  - 2003 池袋西口公園特別篇（池袋ウエストゲートパークスペシャル）TBS電視台
  - 2003 兩個人---我們選擇的路（ふたり～私たちが選んだ道～）日本電視台
  - 2004 弟（弟）朝日電視台
  - 2005 虎與龍---三枚起請之回（タイガー&ドラゴン 三枚起請）TBS電視台
  - 2005 明智小五郎與金田一耕助（明智小五郎VS金田一耕助）朝日電視台
  - 2005 白線流---舊事如夢（白線流し－夢見る頃を過ぎても）富士電視台
  - 2010 美穗的小酒窩（みぽりんのえくぼ）[24小時電視](../Page/24小時電視.md "wikilink")33 日本電視台

### 電影

  - 2002
    [-{zh:漢城72小時;zh-hans:汉城72小时;zh-hant:漢城72小時;zh-hk:漢城72小時;zh-mo:漢城72小時;zh-tw:絕命72小時;}-](../Page/漢城72小時.md "wikilink")（ソウル）-
    主演・早瀬祐太郎 役
  - 2005 午夜駭喀浪人（真夜中の彌次さん喜多さん）- 主演・栃面屋弥次郎兵衛 役
  - 2009 天堂之門 （ヘブンズ‧ドア） - 主演・青山勝人 役
  - 2016 TOO YOUNG TO DIE\! 若くして死ぬ - 主演・キラーK 役
  - 2018 飛上天空的輪胎 (空飛ぶタイヤ) - 主演・赤松徳郎 役

### 動畫電影

  - 2007 異邦人 無皇刃譚 - 主演・名無し 役

### 舞台劇

  - 1992 PLAYZONE－SHOW劇 "MASK～少年隊篇 "接替小島啟
  - 1993 公主的緞帶（姫ちゃんのリボン）
  - 1996 只要藍天仍在（青空のある限り）

### 音樂

在[TOKIO名義之外發行的音樂作品包括](../Page/TOKIO.md "wikilink")------
1.【以Tomoya with 3T 的名義】（SONY）

:\* 1997 單曲『ETERNAL FLAME 』 2.【以櫻庭裕一郎（桜庭裕一郎）的名義】（Universal）

:\* 2001 單曲『メッセージ／ひとりぼっちのハブラシ』（Message／一個人的牙刷），與TOKIO合作的雙A面單曲

:\* 2003 單曲『お前やないとあかんねん』（不能沒有你） 　 3.【以彌次與喜多（Yaji x Kita）的名義】（J-Storm）

:\* 2005『東海道で行こう』 收錄在『真夜中の彌次さん喜多さん』電影原聲帶中

:\* 2005『真夜中の彌次さん喜多さん 』　收錄在『真夜中の彌次さん喜多さん』電影原聲帶中

参加作品

  - 關西傑尼斯8『ドヤ顔人生』（2014年11月5日）

<!-- end list -->

  -
    作詞・作曲・編曲

### 綜藝節目

  - TOKIOカケル　富士电视台
  - 鐵腕DASH（ザ\!鉄腕\!DASH\!\!）　日本電視台
  - SportsPartyただいま夢中\!　
  - Toki-Kin（和KinKi Kids一起主持)
  - Mentore Training（メントレG的前身）
  - 東京小子（メントレG） 富士電視台
  - 5LDK　富士电视台

### 廣播

  - TOKIOナイトクラブtko.tom（日本放送）

<!-- end list -->

  -
    與松岡昌宏輪流主持的30分鐘廣播節目

### 廣告

【個人廣告】

:\* [江崎固力果](../Page/江崎固力果.md "wikilink")・「新PAPICO」

:\* House好侍・「佛蒙特咖哩」

:\* POLA化妝品・「イーガー」

:\* [SONY](../Page/SONY.md "wikilink")・「電池」

:\* [SUZUKI](../Page/SUZUKI.md "wikilink")・「STREETMAGIC」

:\* [SUZUKI](../Page/SUZUKI.md "wikilink")・「Cultus」

:\* 泰國國際航空・「タイは若いうちに行け」宣傳活動

:\* [博士倫](../Page/博士倫.md "wikilink")・「雷朋太陽眼鏡」

:\* [博士倫](../Page/博士倫.md "wikilink")・「Medalist」

:\* [博士倫](../Page/博士倫.md "wikilink")・「ReNu」

:\* [朝日飲料](../Page/朝日飲料.md "wikilink")・「kafeo」

:\* au・「手機A5304T」

:\* [SEIKO](../Page/SEIKO.md "wikilink")・「WIRED」

:\* [UNIQLO](../Page/UNIQLO.md "wikilink")・「DRY-DESIGN」宣傳活動

:\* [SUNTORY](../Page/SUNTORY.md "wikilink")・「立頓檸檬茶」

:\* [SUNTORY](../Page/SUNTORY.md "wikilink")・「Magnum Dry」（サントリー・マグナムドライ）

:\* 三共・「Regain」

:\* NHN Japan・「手機遊戲」

:\* [Subaru](../Page/Subaru.md "wikilink")・「Forester」

:\* [馬自達](../Page/馬自達.md "wikilink")・「100m走れば、MAZDAはわかる。」宣傳活動

:\* GUNZE・「BODY WILD/YG-X」

:\* NTTDoCoMo・「DoCoMo 2.0」

:\*
[富士軟片](../Page/富士軟片.md "wikilink")・「富士數位彩色沖印」（富士寫真フイルム・FUJICOLORデジカメプリント）

:\* [富士軟片](../Page/富士軟片.md "wikilink")・「即拍相機」

:\* [LOTTE](../Page/LOTTE.md "wikilink")・「Toppo」（ロッテ・トッポ）

:\* [LOTTE](../Page/LOTTE.md "wikilink")・「COOLISH」（ロッテ・クーリッシュ）

:\* [LOTTE](../Page/LOTTE.md "wikilink")・「charlotte i」（シャルロッテ・アイ）

:\* 日清炒麵U.F.O.・「まっすぐ」編

:\* [普利司通](../Page/普利司通.md "wikilink")・「TAIYA CAFE、輪胎館」

:\* [HITACHI](../Page/HITACHI.md "wikilink")・「WOOO」

:\* [朝日啤酒](../Page/朝日啤酒.md "wikilink")・「STYLE FREE」

:\* Orico Card

【TOKIO的廣告】

:\* [Yamato運輸](../Page/Yamato運輸.md "wikilink")・「企業」「宅急便服務」

:\* 福島縣・縣產農林水產宣傳事業

:\* [SUZUKI](../Page/SUZUKI.md "wikilink")「Solio」「Solio BANDIT」

:\* [Fumakilla](../Page/Fumakilla.md "wikilink")「電蚊香系列」

:\* [JAPANET TAKATA](../Page/JAPANET_TAKATA.md "wikilink")「30週年開跑活動」

:\* [Xbox 360](../Page/Xbox_360.md "wikilink")・「do\!do\!do\!
しようぜ。」キャンペーン

:\* [Glico 固力果](../Page/江崎固力果.md "wikilink")・アーモンドクラッシュボール

:\* [Glico 固力果](../Page/江崎固力果.md "wikilink")・クリスプ

:\* [Glico 固力果](../Page/江崎固力果.md "wikilink")・ニューパピコ

:\* 進研講座（ベネッセコーポレーション・進研ゼミ）—樂曲提供

:\* [COMPAQ 康柏電腦](../Page/康柏電腦.md "wikilink")・Presario

:\* 麒麟啤酒・キリンラガービール

:\* [JR東海](../Page/東海旅客鐵道.md "wikilink")・AMBITIOUS JAPAN\!キャンペーン

:\* 新日本石油・ENEOS

:\* BEPPU電蚊香系列（フマキラー・ベープ）

:\* 農林水産省・FOOD ACTION NIPPON「食べて応援しよう！」

:\* [GREE](../Page/GREE.md "wikilink")・探検ドリランド

:\* SUKIYA家

:\* 日本中央競馬会・企業CM

### 獎項

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>名稱</p></th>
<th><p>獎項</p></th>
<th><p>作品</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/第16回日劇學院賞.md" title="wikilink">第16回日劇學院賞</a></p></td>
<td><p>男主角獎</p></td>
<td><p>成人禮</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/第20回日劇學院賞.md" title="wikilink">第20回日劇學院賞</a></p></td>
<td><p>男配角獎</p></td>
<td><p><a href="../Page/惡靈貞子.md" title="wikilink">惡靈貞子</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/第25回日劇學院賞.md" title="wikilink">第25回日劇學院賞</a></p></td>
<td><p>男主角獎</p></td>
<td><p><a href="../Page/池袋西口公園.md" title="wikilink">池袋西口公園</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/第29回日劇學院賞.md" title="wikilink">第29回日劇學院賞</a></p></td>
<td><p><a href="../Page/女婿大人.md" title="wikilink">女婿大人</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/第31回日劇學院賞.md" title="wikilink">第31回日劇學院賞</a></p></td>
<td><p>半個醫生</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p>日刊體育電影大賞</p></td>
<td><p>石原裕次郎新人賞</p></td>
<td><p>漢城72小時</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/第37回日劇學院賞.md" title="wikilink">第37回日劇學院賞</a></p></td>
<td><p>男主角獎</p></td>
<td><p>女婿大人2003</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/第45回日劇學院賞.md" title="wikilink">第45回日劇學院賞</a></p></td>
<td><p><a href="../Page/虎與龍.md" title="wikilink">虎與龍</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/第50回日劇學院賞.md" title="wikilink">第50回日劇學院賞</a></p></td>
<td><p><a href="../Page/My_Boss_My_Hero.md" title="wikilink">My Boss My Hero</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/第55回日劇學院賞.md" title="wikilink">第55回日劇學院賞</a></p></td>
<td><p>歌姬</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>47th ACC CM FESTIVAL</p></td>
<td><p>演技獎</p></td>
<td><p>獲ったどー!</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/第66回日劇學院賞.md" title="wikilink">第66回日劇學院賞</a></p></td>
<td><p>男主角獎</p></td>
<td><p><a href="../Page/刑警自戀狂.md" title="wikilink">刑警自戀狂</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/第76回日劇學院賞.md" title="wikilink">第76回日劇學院賞</a></p></td>
<td><p><a href="../Page/不准哭，小原.md" title="wikilink">不准哭，小原</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/第79回日劇學院賞.md" title="wikilink">第79回日劇學院賞</a></p></td>
<td><p><a href="../Page/黑河內.md" title="wikilink">黑河內</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p>第3回<a href="../Page/CONFiDENCE日劇大獎.md" title="wikilink">CONFiDENCE日劇大獎</a></p></td>
<td><p><a href="../Page/Fragile.md" title="wikilink">Fragile</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參看

  - [傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")
  - [J-FRIENDS](../Page/J-FRIENDS.md "wikilink")
  - [TOKIO](../Page/TOKIO.md "wikilink")

[Category:東京小子](../Category/東京小子.md "wikilink")
[Category:傑尼斯事務所所屬藝人](../Category/傑尼斯事務所所屬藝人.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")
[Category:日本男性偶像](../Category/日本男性偶像.md "wikilink")
[Category:日劇學院賞最佳男主角得主](../Category/日劇學院賞最佳男主角得主.md "wikilink")
[Category:日本廣播主持人](../Category/日本廣播主持人.md "wikilink")
[Category:CONFiDENCE日劇大獎最佳男主角得主](../Category/CONFiDENCE日劇大獎最佳男主角得主.md "wikilink")
[Category:石原裕次郎新人獎得主](../Category/石原裕次郎新人獎得主.md "wikilink")