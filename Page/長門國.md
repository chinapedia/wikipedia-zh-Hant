**長門國**（），[日本古代的](../Page/日本.md "wikilink")[令制國之一](../Page/令制國.md "wikilink")，屬[山陽道](../Page/山陽道.md "wikilink")，又稱**長州**。長門國的領域大約為現在[山口縣的西北半部](../Page/山口縣.md "wikilink")。

## 歷代守護

### 鎌倉幕府

  - 1186年－？：[佐佐木高綱](../Page/佐佐木高綱.md "wikilink")
  - 1193年－？：[佐佐木定綱](../Page/佐佐木定綱.md "wikilink")
  - ？－1221年：[佐佐木廣綱](../Page/佐佐木廣綱.md "wikilink")
  - 1221年：[小鹿島公業](../Page/小鹿島公業.md "wikilink")
  - 1222年－？：[天野政景](../Page/天野政景.md "wikilink")
  - 1242年－？：[天野義景](../Page/天野義景.md "wikilink")
  - 1252年？－1276年：[二階堂行忠](../Page/二階堂行忠.md "wikilink")
  - 1276年－1279年：[北條宗賴](../Page/北條宗賴.md "wikilink")
  - 1279年－1280年：[北條兼時](../Page/北條兼時.md "wikilink")
  - 1281年－？：[北條師時](../Page/北條師時.md "wikilink")
  - 1282年－？：[北條忠時](../Page/北條忠時.md "wikilink")
  - 1284年－1296年：[北條實政](../Page/北條實政.md "wikilink")
  - 1298年－1299年：[北條時仲](../Page/北條時仲.md "wikilink")
  - 1300年－1305年：[北條時村](../Page/北條時村.md "wikilink")
  - 1305年－？：[北條熙時](../Page/北條熙時.md "wikilink")
  - 1307年－1319年：北條時仲
  - 1323年－1333年：[北條時直](../Page/北條時直.md "wikilink")

### 室町幕府

  - 1333年？：[二條師基](../Page/二條師基.md "wikilink")
  - 1334年－1348年：[厚東武實](../Page/厚東武實.md "wikilink")
  - 1348年－1349年：[厚東武村](../Page/厚東武村.md "wikilink")
  - 1349年－1351年：[足利直冬](../Page/足利直冬.md "wikilink")
  - 1351年－1353年：[厚東武直](../Page/厚東武直.md "wikilink")
  - 1354年－1358年：[厚東義武](../Page/厚東義武.md "wikilink")
  - 1358年－1374年：[大內弘世](../Page/大內弘世.md "wikilink")
  - 1375年－1399年：[大內義弘](../Page/大內義弘.md "wikilink")
  - 1400年－1401年：[大內弘茂](../Page/大內弘茂.md "wikilink")
  - 1401年－1431年：[大內盛見](../Page/大內盛見.md "wikilink")
  - 1431年－？：[大內持盛](../Page/大內持盛.md "wikilink")
  - 1432年－1441年：[大內持世](../Page/大內持世.md "wikilink")
  - 1441年－1465年：[大內教弘](../Page/大內教弘.md "wikilink")
  - 1465年－1495年：[大內政弘](../Page/大內政弘.md "wikilink")
  - 1495年－1528年：[大內義興](../Page/大內義興.md "wikilink")
  - 1528年－1551年：[大內義隆](../Page/大內義隆.md "wikilink")
  - 1562年－1563年：[毛利隆元](../Page/毛利隆元.md "wikilink")

## 郡

  - [厚狹郡](../Page/厚狹郡.md "wikilink")
  - [豐浦郡](../Page/豐浦郡.md "wikilink")
  - [美祢郡](../Page/美祢郡.md "wikilink")
  - [大津郡](../Page/大津郡.md "wikilink")
  - [阿武郡](../Page/阿武郡.md "wikilink")
  - [見島郡](../Page/見島郡.md "wikilink")

## 相關項目

  - [日本令制國列表](../Page/日本令制國列表.md "wikilink")

[Category:令制國](../Category/令制國.md "wikilink")
[\*](../Category/長門國.md "wikilink")
[Category:山口縣歷史](../Category/山口縣歷史.md "wikilink")