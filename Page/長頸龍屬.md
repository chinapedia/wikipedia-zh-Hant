**長頸龍屬**（屬名：*Tanystropheus*）是種生存於中[三疊紀的](../Page/三疊紀.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")，身長約6公尺（20呎）。主要的特徵是極長的頸部，頸部長3公尺（10呎），比身體與尾巴相加還長。儘管頸部如此長，但頸部只有12個脊椎骨，每個脊椎骨都相當長。長頸龍的化石發現於[歐洲與](../Page/歐洲.md "wikilink")[中東](../Page/中東.md "wikilink")。

在[義大利的Besano地層](../Page/義大利.md "wikilink")，發現許多長頸龍的完整幼年標本，地質年代為三疊紀晚期的[卡尼階](../Page/卡尼階.md "wikilink")，約2億3200萬年前\[1\]。長頸龍的屬名意為「長的脊椎」。

## 長頸龍的可能生活方式

[Tanystropheus_longobardicus.JPG](https://zh.wikipedia.org/wiki/File:Tanystropheus_longobardicus.JPG "fig:Tanystropheus_longobardicus.JPG")
[Tanystropheus_longobardicus_4.JPG](https://zh.wikipedia.org/wiki/File:Tanystropheus_longobardicus_4.JPG "fig:Tanystropheus_longobardicus_4.JPG")
因為長頸龍極長而僵硬的頸部，牠們經常被假設是水生或半水生爬行動物，並且以此假設重建牠們。這個理論因長頸龍化石在半水生地點極常發現而獲得支持，而半水生地點很少發現陸地動物化石。大部分理論根據牠們長而狹窄的口鼻部，以及銳利而交錯的牙齒，而認為長頸龍是[魚食性動物](../Page/魚食性.md "wikilink")。在幾個幼年標本的頜部發現了三尖頭的頰部牙齒，顯示牠們也可能是[食蟲性](../Page/食蟲性.md "wikilink")，然而在[真雙型齒翼龍](../Page/真雙型齒翼龍.md "wikilink")、[倫巴底蜥](../Page/倫巴底蜥.md "wikilink")（*Langobardisaurus*）身上也發現相同的牙齒型態，牠們兩者都被認為是魚食性。此外，在某些標本的腹部附近發現了[頭足類的骨勾](../Page/頭足類.md "wikilink")，還有可能是魚鱗的東西。

長頸龍被認為是重回兩棲生活的爬行動物之一，牠們在岸邊使用長頸部與銳利牙齒，捕抓淺水裡的[魚類與其他海生動物](../Page/魚類.md "wikilink")。不對稱的頸部造成生活方式的幾個問題。有些科學家爭論說不對稱的頸部使得長頸龍的重心位在手臂前方，讓牠們將頸部伸出時，頭會貼近地面。基於這個理由，[大衛·彼德斯](../Page/大衛·彼德斯.md "wikilink")（David
Peters）提出長頸龍是陸地動物，牠們以後肢用二足方式站立，而頸部垂直於地面使身體保持平衡。支持這個論點的人認為長頸龍在樹的底下等待，並使用長頸部與小頭，補抓樹枝上的小型樹棲動物。彼德斯與其他科學家認為[原蜥形目是](../Page/原蜥形目.md "wikilink")[翼龍類的祖先](../Page/翼龍類.md "wikilink")，因此認為原蜥形目動物是優勢陸地動物。

長頸龍的另一個[次異名是](../Page/次異名.md "wikilink")*Procerosaurus*，該屬有兩個種，其中一種曾被歸類於[禽龍](../Page/禽龍.md "wikilink")，之後在2000年被[喬治·奧利舍夫斯基](../Page/喬治·奧利舍夫斯基.md "wikilink")（George
Olshevsky）建立為[壞骨龍](../Page/壞骨龍.md "wikilink")；另外一種是在1902年由[弗雷德里克·馮·休尼](../Page/弗雷德里克·馮·休尼.md "wikilink")（Friedrich
von Huene）命名，目前是長頸龍的次異名。

在2006年，Silvio
Renesto博士在[瑞士發現了一個標本](../Page/瑞士.md "wikilink")，推翻原本的長頸龍半水生生活方式論點。這標本第一次發現了長頸龍的軟組織，包括皮膚的非方形、部分重疊鱗片\[2\]。這標本透露出在尾巴基部的不尋常黑色物質，包含數個由[碳酸鈣構成的小球](../Page/碳酸鈣.md "wikilink")，顯示有相當數量的肌肉位在長頸龍的臀部上。除了強壯的後肢肌肉，巨大的臀部能將長頸龍的重心移往後方，在長頸龍將3公尺長的頸部伸出時穩定重心\[3\]
。

## 分類

[Tribelesodon.jpg](https://zh.wikipedia.org/wiki/File:Tribelesodon.jpg "fig:Tribelesodon.jpg")
在1886年，[三麗齒龍](../Page/三麗齒龍.md "wikilink")（*Tribelesodon*）被Francesco
Bassani歸類於[翼龍類](../Page/翼龍類.md "wikilink")，但現在被認為是長頸龍的[次同物異名](../Page/次同物異名.md "wikilink")（Junior
Synonym）。最著名的種是*T. longobardicus*，其他已被確認的種包括：*T. conspicuus*與*T.
meridensis*\[4\]。

在2002年，在[中國西南部的](../Page/中國.md "wikilink")[三疊紀海相沉積層發現了近親](../Page/三疊紀.md "wikilink")[恐頭龍](../Page/恐頭龍.md "wikilink")。恐頭龍身長2.7公尺，而頸部與頭部佔了1.7公尺。恐頭龍的標本在2004年被敘述、命名。

## 大眾文化

長頸龍短暫出現在[BBC](../Page/BBC.md "wikilink")《[與恐龍共舞](../Page/與恐龍共舞.md "wikilink")》（*Walking
with Dinosaurs*）電視節目第一輯，以及《[海底霸王](../Page/海底霸王.md "wikilink")》（*Sea
Monsters*）。在節目裡，[奈吉爾·馬文](../Page/奈吉爾·馬文.md "wikilink")（Nigel
Marven）在[三疊紀海洋游泳時](../Page/三疊紀.md "wikilink")，他在遇到一隻長頸龍、兩隻[上龍類](../Page/上龍類.md "wikilink")、一隻[杯椎魚龍](../Page/杯椎魚龍.md "wikilink")。他尾隨長頸龍游泳，並測量長頸龍的斷尾，但斷尾是根據對於[蜥蜴的幻想](../Page/蜥蜴.md "wikilink")。

[匹茲堡的](../Page/匹茲堡.md "wikilink")[卡內基美術館在](../Page/卡內基美術館.md "wikilink")2000年引進了一個長頸龍小型模型。模型呈綠色與棕色，下方呈藍色，以及可動式頸部。在2007年，這個長頸龍模型被更換，身體呈咖啡色，頭部、頸部呈綠色。

## 參考資料

  - [George Olshevsky expands on the history of "P."
    *exogyrarum*](http://dml.cmnh.org/2000May/msg00176.html), on the
    Dinosaur Mailing List
  - [*Procerosaurus* in The Dinosaur
    Encyclopaedia](http://web.me.com/dinoruss/de_4/5a92289.htm) at Dino
    Russ's Lair
  - Huene, 1902. "Übersicht über die Reptilien der Trias" \[Review of
    the Reptilia of the Triassic\]. *Geologische und Paläontologische
    Abhandlungen*. 6, 1-84.
  - Fritsch, 1905. "Synopsis der Saurier der böhm. Kreideformation"
    \[Synopsis of the saurians of the Bohemian Cretaceous formation\].
    *Sitzungsberichte der königlich-böhmischen Gesellschaft der
    Wissenschaften*, II Classe. **1905**(8), 1-7.

## 外部連結

  - [BBC的長頸龍重建圖（假設長頸龍為水生動物）](http://www.bbc.co.uk/science/seamonsters/factfiles/tanystropheus.shtml?img1)
  - [米蘭大學的長頸龍頁面](http://users.unimi.it/vertpal/galleriafossili/best-rettili/tanystropheus.htm)
    - 包含樣本與形態資訊
  - [Hairy Museum of Natural History's coverage of the discovery of
    the 2006 specimen with the large backside, contains
    illustrations.](http://www.hmnh.org/archives/category/mesozoic/triassic/)

[Category:原蜥形目](../Category/原蜥形目.md "wikilink")
[Category:三疊紀爬行動物](../Category/三疊紀爬行動物.md "wikilink")

1.  Dal Sasso, C. and Brillante, G. (2005). *Dinosaurs of Italy*.
    Indiana University Press. ISBN 0253345146, 9780253345141.

2.
3.  Renesto, S. (2005). "A new specimen of *Tanystropheus* (Reptilia
    Protorosauria) from the Middle Triassic of Switzerland and the
    ecology of the genus." *Rivista Italiana di Paleontologia e
    Stratigrafia*, **111**(3): 377–394.

4.  [*Tanystropheus*](http://users.unimi.it/vertpal/galleriafossili/best-rettili/tanystropheus.htm).
    Vertebrate Palaeontology at Milano University. Retrieved 2007-02-19.