**長鼻跳鼠**（*Leptictidium*），又名*两脚猬*，是一屬細小的史前[動物](../Page/動物.md "wikilink")，生存在[始新世](../Page/始新世.md "wikilink")。牠們廣泛分佈在[歐洲](../Page/歐洲.md "wikilink")，但約於4000萬年前消失，並沒有留下後裔。

長鼻跳鼠的前肢細小，但後肢很大，像縮小了的[袋鼠](../Page/袋鼠.md "wikilink")。根據牠們的[骨骼結構研究](../Page/骨骼.md "wikilink")，顯示牠們是以跳躍來移動。同時期，基於[頭顱骨的研究發現牠們有長及可行動的鼻子](../Page/頭顱骨.md "wikilink")，這有點像[象鼩](../Page/象鼩.md "wikilink")。牠們大約60-90厘米長。

長鼻跳鼠的完整[化石是在](../Page/化石.md "wikilink")[德國](../Page/德國.md "wikilink")[麥塞爾化石坑發現](../Page/麥塞爾化石坑.md "wikilink")。牠們毛皮的輪廓及[胃部內的食物亦被保留了下來](../Page/胃部.md "wikilink")，得知牠們是[肉食性的](../Page/肉食性.md "wikilink")，以[昆蟲](../Page/昆蟲.md "wikilink")、[蜥蜴及其他細小的哺乳動物](../Page/蜥蜴.md "wikilink")。

於始新世末期，[地球的](../Page/地球.md "wikilink")[氣候開始漸冷](../Page/氣候.md "wikilink")，最終很多[熱帶](../Page/熱帶.md "wikilink")[森林亦消失](../Page/森林.md "wikilink")。長鼻跳鼠不能適應這個環境而滅絕。

[Leptictidium_Artist's_Impression.png](https://zh.wikipedia.org/wiki/File:Leptictidium_Artist's_Impression.png "fig:Leptictidium_Artist's_Impression.png")

## 參考

## 外部連結

[BBC Online Science and
Nature](https://web.archive.org/web/20050407181747/http://www.bbc.co.uk/beasts/evidence/prog1/page5_2.shtml)

[Category:麗蝟目](../Category/麗蝟目.md "wikilink")
[Category:始新世哺乳類](../Category/始新世哺乳類.md "wikilink")