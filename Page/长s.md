，又名**長S**，是[小写字母](../Page/小写字母.md "wikilink")*[s](../Page/s.md "wikilink")*
的變體，用於字的開首或中段，而不會用來結尾。例如：  就是“sinfulness”一詞的過去寫法。

## 历史

[S_long_serif_et_sans_serif.svg](https://zh.wikipedia.org/wiki/File:S_long_serif_et_sans_serif.svg "fig:S_long_serif_et_sans_serif.svg")
长s很容易与[f混淆](../Page/f.md "wikilink")，尤其是在一些直体字型中（譬如Courier
New字体），它们的中部有一个与f相似的横划。在斜体字中没有这个横划，有时它们下延，向左弯。这个横划实际上是一个向上指的楔子，其最宽处正好位于横划的位置，它本来向上并向右转，有点像[k](../Page/k.md "wikilink")。过去[b](../Page/b.md "wikilink")、[h和](../Page/h.md "wikilink")[l也有类似的楔子](../Page/l.md "wikilink")，但后来只有长s保留了这个楔子。

长s一般与si、ss和st用在连体字中。

19世纪末长s已经在直体和斜体字中不再使用，在大多数国家里连体字也消失了。[法语的一些字型中还使用类似于长s的st的连体字](../Page/法语.md "wikilink")。在[德语中的](../Page/德语.md "wikilink")[ß本来也是一个ss的连体字](../Page/ß.md "wikilink")。在[希腊语中](../Page/希腊语.md "wikilink")[Σ有σ和ς两种写法](../Page/Σ.md "wikilink")，在文艺复兴时期的欧洲s也有类似希腊文的两种写法，这可能是长s的来源。

## 现代用途

[积分符号](../Page/积分符号.md "wikilink")\(\int\)实际上是一个长s：[戈特弗里德·威廉·莱布尼茨使用](../Page/戈特弗里德·威廉·莱布尼茨.md "wikilink")[拉丁语中](../Page/拉丁语.md "wikilink")“和”（*summa*）的第一个字母来表示积分是一个计算极限的和的算法。在[国际音标中用长s的变体](../Page/国际音标.md "wikilink")[ʃ来表示英语中sh的发音](../Page/ʃ.md "wikilink")。

由於**長s**和**f**的外型非常相似，所以不少人以此作為笑話的題材。最常見的笑話，是利用“suck”或“sucking”這個字來作[粗口的暗示](../Page/粗口.md "wikilink")，例如：“sucking
pig”。又例如，在[本尼·希尔的節目裡](../Page/本尼·希尔.md "wikilink")，有一集他的歌有這麼一句歌詞：“Fad-Eyed
Fal”，其實是“Sad-Eyed Sal”。

[提琴等樂器的](../Page/提琴.md "wikilink")[F孔也與長s相近](../Page/F孔.md "wikilink")。

## 字符编码

長S在[Unicode裡有自己的編碼](../Page/Unicode.md "wikilink")，其16數位值是U+017F。在[HTML很多時都會用](../Page/HTML.md "wikilink")`ſ`來標示長S這個字母。

## 参看

  - <span lang="en" style="font-size:120%;">[ß](../Page/ß.md "wikilink")</span>
  - <span lang="el" style="font-size:120%;">[ς](../Page/ς.md "wikilink")</span>

## 外部链接

  - [The Straight Dope on long
    *s*](http://www.straightdope.com/classics/a1_110.html)

[Sſ](../Category/中古拉丁文字變體.md "wikilink")