**长兴**（930年二月-933年）是[后唐明宗](../Page/后唐.md "wikilink")[李嗣源的年号](../Page/李嗣源.md "wikilink")，共计4年。

[吳越世宗](../Page/吳越.md "wikilink")[錢元瓘用此年号](../Page/錢元瓘.md "wikilink")（932年四月-933年，长兴三年-四年）。[马楚衡阳王](../Page/马楚.md "wikilink")[馬希聲和文昭王](../Page/馬希聲.md "wikilink")[馬希範用此年号](../Page/馬希範.md "wikilink")（930年十一月-933年）；[闽惠宗](../Page/闽.md "wikilink")[王延钧用此年号](../Page/王延钧.md "wikilink")（930年二月-932年）；[荆南文獻王](../Page/荆南.md "wikilink")[高從誨亦用此年号](../Page/高從誨.md "wikilink")（930年二月-933年）\[1\]。

## 纪年

| 长兴                               | 元年                             | 二年                             | 三年                             | 四年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 930年                           | 931年                           | 932年                           | 933年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚寅](../Page/庚寅.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [大和](../Page/大和_\(楊溥\).md "wikilink")（929年十月至935年八月）：吳—楊溥之年號
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：南漢—劉龑之年號
      - [龍啟](../Page/龍啟.md "wikilink")（933年至934年）：[閩](../Page/闽_\(十国\).md "wikilink")—[王延鈞之年號](../Page/王延鈞.md "wikilink")
      - [大明](../Page/大明_\(楊干真\).md "wikilink")（931年至937年）：[大義寧](../Page/大義寧.md "wikilink")—[楊干真之年號](../Page/楊干真.md "wikilink")
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年—933年）：[高麗](../Page/高麗.md "wikilink")—[高麗太祖王建之年號](../Page/高麗太祖.md "wikilink")
      - [延長](../Page/延長_\(醍醐天皇\).md "wikilink")（923年閏四月十一日至931年四月二十六日）：日本[醍醐天皇年号](../Page/醍醐天皇.md "wikilink")
      - [承平](../Page/承平_\(朱雀天皇\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本[朱雀天皇年号](../Page/朱雀天皇.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:后唐年号](../Category/后唐年号.md "wikilink")
[Category:吴越年号](../Category/吴越年号.md "wikilink")
[Category:马楚年号](../Category/马楚年号.md "wikilink")
[Category:闽年号](../Category/闽年号.md "wikilink")
[Category:荆南年号](../Category/荆南年号.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月，147，149，151，156 ISBN 7101025129