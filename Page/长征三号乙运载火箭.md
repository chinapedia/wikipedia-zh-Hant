**長征三號乙火箭**是在[長征三號甲和](../Page/長征三號甲.md "wikilink")[長征二號捆火箭的基礎上研製的大型三級液體捆綁火箭](../Page/長征二號捆.md "wikilink")，芯級基本上與長征三號甲火箭相同，助推器及其捆綁結構則基本與長征二號捆火箭相同，表现为在长征三号甲的第一级火箭周围捆绑了4个与长征二号捆相同的液体火箭助推器並增長第二级火箭。

长征三号系列中的“长征三号乙”火箭是中国目前发射高轨道卫星的主力运载火箭。\[1\]本型号的增強型火箭的[地球同步轨道运载能力提高到](../Page/地球同步轨道.md "wikilink")5500公斤，使中国的运载火箭进入了世界大型火箭的行列。

长三乙火箭的主要任務是發射[地球同步轉移軌道的重型衛星和進行輕型衛星的一箭多星的發射](../Page/地球同步轉移軌道.md "wikilink")。發射價格約為7千萬美元。1998年8月，该火箭成功将重达3770公斤的亚洲功率最大的通信卫星[菲律宾](../Page/菲律宾.md "wikilink")[马部海一号通信卫星送入预定轨道](../Page/马部海一号通信卫星.md "wikilink")，并在此后承担了众多国内国际的通信卫星发射任务。\[2\]

## 技術諸元

[缩略图](https://zh.wikipedia.org/wiki/File:Chang_zheng_3b.png "fig:缩略图")
长征三号乙火箭除使用4000F型整流罩的长征三号乙标准型（已停产）外，还衍生了长征三号乙改一型、长征三号乙改二型、长征三号乙改三型、长征三号乙/远征一号火箭和长征三号改五型，其中后4者属于增强型火箭。增强型与标准型相比，芯一级加长1.488米、助推器加长0.768米、推进剂质量增加近20吨，此外还改变了整流罩尺寸等，从而扩展了运载能力和任务适应能力。长征三号乙改一型使用直径3.7米的3700Z加长型双星整流罩，长征三号乙改二型使用直径4米的4000F型整流罩，长征三号乙改三型使用直径4.2米的4200F型整流罩，长征三号乙/远征一号使用4200Z型整流罩，长征三号乙改五型则在长征三号乙/远征一号火箭基础上将整流罩加长了900毫米。

  - 全長：54.835米（增强型56.326米）
  - 離地質量：425.8吨（增强型458.97吨）

<!-- end list -->

  - 地球同步軌道：5.1吨（改進Ⅱ型5.5吨）
  - 推力：604.387吨
  - 第一節燃料：[四氧化二氮](../Page/四氧化二氮.md "wikilink")+[偏二甲肼](../Page/偏二甲肼.md "wikilink")
  - 第二節燃料：四氧化二氮+偏二甲肼
  - 第三節燃料：[液態氧](../Page/液態氧.md "wikilink")+[液態氫](../Page/液態氫.md "wikilink")

## 历史

长征三号乙火箭的研发始于1986年，其目标是在[长征系列火箭的基础上提高其地球同步轨道的运载能力](../Page/长征系列火箭.md "wikilink")，以满足日益增长的国际卫星发射市场的需求——尤其是大功率[通信卫星的发射](../Page/通信卫星.md "wikilink")。

[Chang_zheng_3b_and_intelsat708_failure.jpg](https://zh.wikipedia.org/wiki/File:Chang_zheng_3b_and_intelsat708_failure.jpg "fig:Chang_zheng_3b_and_intelsat708_failure.jpg")
1996年2月15日，长三乙在其首次发射中失利，起飞约2秒后由于导航系统故障导致火箭在飞行中倾斜翻側，约22秒后头部撞到离发射架1850米处的山坡上，发生猛烈爆炸\[3\]。在事故中星箭全损，基本没有留下大件残骸，并造成6人死亡，57人受傷\[4\]，毁伤發射基地居住區房屋80余间\[5\]，是为中国航天史上伤亡最严重的事故。故障原因后查明为惯性基准大回路里的一个电子元件失效，造成惯性基准无输出。

1997年8月20日，长三乙成功将亚洲地区功率最大，重达3770kg的菲律宾马部海一号通讯卫星送入轨道。其后长三乙还承担了众多通信卫星的发射任务，为中国、尼日利亚、委内瑞拉、印度尼西亚、巴基斯坦、法国等地研发的卫星提供了发射服务。

长三乙火箭的最近一次故障发生在2009年8月31日负责发射[印度尼西亚](../Page/印度尼西亚.md "wikilink")[帕拉帕D](../Page/帕拉帕D通信卫星.md "wikilink")(Palapa-D)[通信卫星的途中](../Page/通信卫星.md "wikilink")。当天[北京时间](../Page/北京时间.md "wikilink")17时28分，长三乙火箭从中国[西昌卫星发射中心发射升空](../Page/西昌卫星发射中心.md "wikilink")，火箭一、二级飞行正常，三级二次[点火后出现异常情况](../Page/点火.md "wikilink")，未能将该卫星送入预定[轉移轨道](../Page/地球同步轉移軌道.md "wikilink")。一二级残骸于17点36分坠落在[湖南省](../Page/湖南省.md "wikilink")[绥宁县境内](../Page/绥宁县.md "wikilink")，未造成人员伤亡\[6\]。所幸卫星于当日23时捕获，状态正常，通过在9月1日后实施变轨到达预定轨道。\[7\]

2013年12月2日1时30分，长三乙改进Ⅲ型火箭实现零窗口发射，成功将中国首个月面软着陆器[嫦娥三号与月球车](../Page/嫦娥三号.md "wikilink")[玉兔號送入近地点](../Page/玉兔號月球車.md "wikilink")210公里，远地点约37万公里的[地月转移轨道](../Page/霍曼轉移軌道.md "wikilink")。中国中央电视台直播了此次发射，这也是自1996年发射失败之后长三乙的首次电视直播发射。\[8\]

长三乙增強型火箭从2007年首次发射以来取得23次发射23次成功的佳绩，重新树立了该火箭的口碑。

2013年12月21日0时42分，中国在西昌卫星发射中心用长征三号乙改进Ⅱ型运载火箭，成功将玻利维亚通信卫星发射升空，卫星顺利进入预定轨道。玻利维亚通信卫星是玻利维亚拥有的第一颗卫星。[玻利维亚总统](../Page/玻利维亚总统.md "wikilink")[莫拉莱斯专程来到西昌卫星发射中心](../Page/埃沃·莫拉莱斯.md "wikilink")，现场观看了卫星发射。这是外国国家元首首次到中国航天发射场观看卫星发射。\[9\]

2017年6月19日，中國航天科技集團公司公布，中国先前在西昌卫星发射中心用长征三号乙运载火箭发射中星9A广播电视直播卫星，發射過程中，火箭三級工作異常，衛星未能進入預定軌道，據悉衛星還能操作，已經先採取了處置措施。\[10\]

## 发射记录

<table>
<thead>
<tr class="header">
<th><p>飞行次序</p></th>
<th><p>火箭序号</p></th>
<th><p>火箭型号</p></th>
<th><p>上面級</p></th>
<th><p>起飞时间（UTC+8）</p></th>
<th><p>发射工位</p></th>
<th><p>有效载荷</p></th>
<th><p>卫星轨道</p></th>
<th><p>发射结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>遥一</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>1996年2月15日</p></td>
<td><p><a href="../Page/西昌卫星发射中心.md" title="wikilink">西昌卫星发射中心</a>2号工位</p></td>
<td><p><a href="../Page/国际通信卫星708号.md" title="wikilink">国际通信卫星708号</a></p></td>
<td><p><a href="../Page/地球同步转移轨道.md" title="wikilink">地球同步转移轨道</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>遥二</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>1997年8月20日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/马部海一号.md" title="wikilink">马部海一号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>遥三</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>1997年10月17日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/亚太二号R.md" title="wikilink">亚太二号R</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>遥五</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>1998年5月30日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中卫一号.md" title="wikilink">中星五号A</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>遥四</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>1998年7月18日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/鑫诺一号.md" title="wikilink">中星五号B</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>遥六</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>2005年4月12日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/亚太六号.md" title="wikilink">亚太六号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>遥七</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>2006年10月29日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/鑫诺二号.md" title="wikilink">鑫诺二号</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>遥九</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2007年5月14日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/尼日利亚通信卫星一号.md" title="wikilink">通信卫星一号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>遥十</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>2007年6月30日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星6B.md" title="wikilink">中星六号B</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>遥十一</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>2008年6月9日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星九号.md" title="wikilink">中星九号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>遥十二</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2008年10月30日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/委内瑞拉通信卫星一号.md" title="wikilink">通信卫星一号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>遥八</p></td>
<td><p>標準型</p></td>
<td></td>
<td><p>2009年8月31日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/帕拉帕-D.md" title="wikilink">帕拉帕-D</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>遥十三</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2010年9月5日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/鑫诺六号.md" title="wikilink">中星六号A</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>遥二十</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2011年6月21日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星十号.md" title="wikilink">中星十号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>遥十九</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2011年8月12日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/巴基斯坦通信卫星1R.md" title="wikilink">通信卫星1R</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>遥十六</p></td>
<td><p>改進Ⅲ型</p></td>
<td></td>
<td><p>2011年9月19日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星一号A.md" title="wikilink">中星一号A</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>遥十八</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2011年10月7日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/欧洲通信卫星W3C.md" title="wikilink">欧洲通信卫星W3C</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>遥二十一</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2011年12月20日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/尼日利亚通信卫星1R.md" title="wikilink">通信卫星1R</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>遥二十一</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2012年3月31日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/亚太七号.md" title="wikilink">亚太七号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>遥十四</p></td>
<td><p>改進Ⅰ型</p></td>
<td></td>
<td><p>2012年4月30日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗二号M3</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗二号M4</a></p></td>
<td><p><a href="../Page/中地球轨道.md" title="wikilink">中地球转移轨道</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>遥十七</p></td>
<td><p>改進Ⅲ型</p></td>
<td></td>
<td><p>2012年5月26日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星二号A.md" title="wikilink">中星二号A</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>遥十五</p></td>
<td><p>改進Ⅰ型</p></td>
<td></td>
<td><p>2012年9月19日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗二号M5</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗二号M6</a></p></td>
<td><p>中地球转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>遥二十四</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2012年11月27日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星十二号.md" title="wikilink">中星十二号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>遥二十五</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2013年5月2日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星十一号.md" title="wikilink">中星十一号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>遥二十三</p></td>
<td><p>改進Ⅲ型</p></td>
<td></td>
<td><p>2013年12月2日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/嫦娥三号.md" title="wikilink">嫦娥三号</a></p></td>
<td><p><a href="../Page/地月转移轨道.md" title="wikilink">地月转移轨道</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p>遥二十七</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2013年12月21日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/玻利维亚通信卫星.md" title="wikilink">通信卫星</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>遥二十六</p></td>
<td><p>改進ⅢZ型</p></td>
<td><p><a href="../Page/远征一号.md" title="wikilink">远征一号</a><br />
遥二</p></td>
<td><p>2015年7月25日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M1-S</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M2-S</a></p></td>
<td><p>中地球轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p>遥三十二</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2015年9月12日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/通信技术试验卫星一号.md" title="wikilink">通信技术试验卫星一号</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p>遥三十三</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2015年9月30日</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号I2-S</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p>遥三十六</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2015年10月17日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/亚太九号.md" title="wikilink">亚太九号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td><p>遥三十四</p></td>
<td><p>改進Ⅲ型</p></td>
<td></td>
<td><p>2015年11月4日</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/中星二号C.md" title="wikilink">中星二号C</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p>遥三十八</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2015年11月21日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/老挝通信卫星一号.md" title="wikilink">通信卫星一号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td><p>遥三十一</p></td>
<td><p>改進Ⅲ型</p></td>
<td></td>
<td><p>2015年12月10日</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/中星一号C.md" title="wikilink">中星一号C</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td><p>遥三十七</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2015年12月29日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/高分四号.md" title="wikilink">高分四号</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td><p>遥二十九</p></td>
<td><p>改進Ⅱ型</p></td>
<td></td>
<td><p>2016年1月16日</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/白俄罗斯通信卫星一号.md" title="wikilink">通信卫星一号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td><p>遥三十五</p></td>
<td><p>改進Ⅲ型</p></td>
<td></td>
<td><p>2016年8月6日</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/天通一号01星.md" title="wikilink">天通一号01星</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td><p>遥四十二</p></td>
<td><p>改进Ⅲ型</p></td>
<td></td>
<td><p>2016年12月11日<br />
00时11分04秒</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/风云四号A星.md" title="wikilink">风云四号A星</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td><p>[11]</p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td><p>遥三十九</p></td>
<td><p>改进Ⅱ型</p></td>
<td></td>
<td><p>2017年1月5日<br />
23时18分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/通信技术试验卫星二号.md" title="wikilink">通信技术试验卫星二号</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td><p>[12]</p></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p>遥四十三</p></td>
<td><p>改进Ⅱ型</p></td>
<td></td>
<td><p>2017年4月12日<br />
19时04分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星十六号.md" title="wikilink">中星十六号</a></p></td>
<td><p>超同步转移轨道</p></td>
<td><p>[13]</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p>遥二十八</p></td>
<td><p>改进Ⅱ型</p></td>
<td></td>
<td><p>2017年6月19日</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星9A.md" title="wikilink">中星九号A</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td><p>[14]</p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td><p>遥四十六</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥四</p></td>
<td><p>2017年11月5日<br />
19时45分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M1</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M2</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[15]</p></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td><p>遥四十</p></td>
<td><p>改进Ⅱ型</p></td>
<td></td>
<td><p>2017年12月11日<br />
00时40分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/阿尔及利亚一号通信卫星.md" title="wikilink">一号通信卫星</a></p></td>
<td><p>超同步转移轨道</p></td>
<td><p>[16]</p></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td><p>遥四十五</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥五</p></td>
<td><p>2018年1月12日<br />
07时18分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M7</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M8</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[17]</p></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td><p>遥四十七</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥六</p></td>
<td><p>2018年2月12日<br />
13时03分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M3</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M4</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[18]</p></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td><p>遥四十八</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥七</p></td>
<td><p>2018年3月30日<br />
01时56分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M9</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M10</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[19]</p></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td><p>遥五十五</p></td>
<td><p>改进Ⅱ型</p></td>
<td></td>
<td><p>2018年5月4日<br />
00时06分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/亚太6C.md" title="wikilink">亚太6C</a></p></td>
<td><p>超同步转移轨道</p></td>
<td><p>[20]</p></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td><p>遥四十九</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥八</p></td>
<td><p>2018年7月29日<br />
09时48分</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M5</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M6</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[21]</p></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td><p>遥五十</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥九</p></td>
<td><p>2018年8月25日<br />
07时52分</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M11</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M12</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[22]</p></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td><p>遥五十一</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥十</p></td>
<td><p>2018年9月19日<br />
22时07分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M13</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M14</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[23]</p></td>
</tr>
<tr class="even">
<td><p>50</p></td>
<td><p>遥五十二</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥十一</p></td>
<td><p>2018年10月15日<br />
12时23分</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M15</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M16</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[24]</p></td>
</tr>
<tr class="odd">
<td><p>51</p></td>
<td><p>遥四十一</p></td>
<td><p>改进Ⅲ型</p></td>
<td></td>
<td><p>2018年11月1日<br />
23时57分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号G1</a></p></td>
<td><p>地球静止轨道</p></td>
<td><p>[25]</p></td>
</tr>
<tr class="even">
<td><p>52</p></td>
<td><p>遥五十三</p></td>
<td><p>改进ⅢZ型</p></td>
<td><p>远征一号<br />
遥十二</p></td>
<td><p>2018年11月19日<br />
02时07分</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M17</a><br />
<a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号M18</a></p></td>
<td><p>中地球轨道</p></td>
<td><p>[26]</p></td>
</tr>
<tr class="odd">
<td><p>53</p></td>
<td><p>遥三十</p></td>
<td><p>改进Ⅱ型</p></td>
<td></td>
<td><p>2018年12月8日<br />
02时23分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/嫦娥四号.md" title="wikilink">嫦娥四号</a></p></td>
<td><p>地月转移轨道</p></td>
<td><p>[27]</p></td>
</tr>
<tr class="even">
<td><p>54</p></td>
<td><p>遥五十六</p></td>
<td><p>改进Ⅲ型</p></td>
<td></td>
<td><p>2019年1月11日<br />
01时11分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/中星2D.md" title="wikilink">中星2D</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td><p>[28]</p></td>
</tr>
<tr class="odd">
<td><p>55</p></td>
<td><p>遥五十四</p></td>
<td><p>改进Ⅲ型</p></td>
<td></td>
<td><p>2019年3月10日<br />
00时28分</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/中星6C.md" title="wikilink">中星6C</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td><p>[29]</p></td>
</tr>
<tr class="even">
<td><p>56</p></td>
<td><p>遥四十四</p></td>
<td><p>改进Ⅱ型</p></td>
<td></td>
<td><p>2019年3月31日<br />
23时51分</p></td>
<td><p>西昌卫星发射中心2号工位</p></td>
<td><p><a href="../Page/天链二号01星.md" title="wikilink">天链二号01星</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td><p>[30]</p></td>
</tr>
<tr class="odd">
<td><p>57</p></td>
<td><p>遥五十九</p></td>
<td><p>改进Ⅱ型</p></td>
<td></td>
<td><p>2019年4月20日<br />
22时41分</p></td>
<td><p>西昌卫星发射中心3号工位</p></td>
<td><p><a href="../Page/北斗卫星导航系统.md" title="wikilink">北斗三号IGSO1</a></p></td>
<td><p>地球同步转移轨道</p></td>
<td><p>[31]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注释

<references group="註"/>

## 参考文献

## 外部链接

  - [长征三号乙运载火箭 (LM-3B) ---
    中国长城工业集团有限公司](http://cn.cgwic.com/LaunchServices/LaunchVehicle/LM3B.html)
  - [长征三号甲系列运载火箭用户手册](https://web.archive.org/web/20150717190345/http://www.cgwic.com/LaunchServices/Download/manual/LM-3A%20Series%20Launch%20Vehicles%20User%27s%20Manual%20Issue%202011.pdf)

[3B](../Category/长征火箭.md "wikilink")

1.
2.
3.  [长三乙1996年2月事故录像](http://pbma.nasa.gov/docs/public/pbma/vits/Long_March.avi)

4.
5.
6.  [印尼通信卫星火箭残骸坠落湖南绥宁(组图)](http://news.qq.com/a/20090831/002680.htm)
7.
8.  [嫦娥三号发射全记录](http://news.163.com/13/1202/02/9F2BG1N900014JB5.html)
9.  [中国成功发射玻利维亚通信卫星](http://www.chinanews.com/gn/2013/12-21/5647043.shtml)
10. [中國今發射中星廣播衛星失敗　未能入軌](http://www.appledaily.com.tw/realtimenews/article/international/20170619/1143434/%E4%B8%AD%E5%9C%8B%E4%BB%8A%E7%99%BC%E5%B0%84%E4%B8%AD%E6%98%9F%E5%BB%A3%E6%92%AD%E8%A1%9B%E6%98%9F%E5%A4%B1%E6%95%97%E3%80%80%E6%9C%AA%E8%83%BD%E5%85%A5%E8%BB%8C/)
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.