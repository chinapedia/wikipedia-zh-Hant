**长沙王**为[中国历代在](../Page/中国.md "wikilink")[长沙所分封的国王或者藩王](../Page/长沙.md "wikilink")。[西汉开国功臣](../Page/西汉.md "wikilink")[吴芮被封为第一任长沙王](../Page/吴芮.md "wikilink")，封国为[长沙国](../Page/长沙国.md "wikilink")。

[汉朝的长沙国被撤消后](../Page/汉朝.md "wikilink")，长沙王却还出现在其他朝代中。如[西晋的](../Page/西晋.md "wikilink")[八王之乱的长沙王](../Page/八王之乱.md "wikilink")[司马乂等](../Page/司马乂.md "wikilink")。

[明朝时期在长沙也分封过长沙王](../Page/明朝.md "wikilink")。在长沙郊区的望城坡蚂蚁山曾发掘过一座明代的长沙王墓。

## 汉朝

  - 第一次册封
      - 长沙文王 [吴芮](../Page/吴芮.md "wikilink") 前202年在位
      - 长沙成王 [吴臣](../Page/吴臣.md "wikilink") 前201年-前193年在位
      - 长沙哀王 [吴回](../Page/吴回.md "wikilink") 前193年-前186年在位
      - 长沙共王 [吴右](../Page/吴右.md "wikilink") 前186年-前178年在位
      - 长沙靖王 [吴著](../Page/吴著.md "wikilink") 前178年-前157年在位
  - 第二次册封
      - 长沙定王 [刘发](../Page/刘发.md "wikilink") 前155年-前127年在位
      - 长沙戴王 [刘庸](../Page/刘庸.md "wikilink") 前127年-前100年在位
      - 长沙顷王 [刘附朐](../Page/刘附朐.md "wikilink") 前100年-前83年在位
      - 长沙剌王 [刘建德](../Page/刘建德.md "wikilink") 前83年-前49年在位
      - 长沙煬王 [刘旦](../Page/刘旦.md "wikilink") 前49年-前47年在位
      - 长沙孝王 [刘宗](../Page/刘宗.md "wikilink") 前45年-前42年在位
      - 长沙缪王 [刘鲁人](../Page/刘鲁人.md "wikilink") 前42年-7年在位
      - 长沙王 [刘舜](../Page/刘舜.md "wikilink") 7年-9年在位
  - 第三次册封
      - 长沙王 [刘兴](../Page/刘兴.md "wikilink") 26年-38年在位

## 東吳

  - 長沙桓王[孫策](../Page/孫策.md "wikilink")，吳大帝[孫權追謚](../Page/孫權.md "wikilink")。

## 晋朝

  - 长沙厉王[司马乂](../Page/司马乂.md "wikilink")，289年始封，不久贬为常山王，301年复封，304年薨。
  - 长沙王[司马硕](../Page/司马硕.md "wikilink")，309年袭封，至[刘聪掠夺](../Page/刘聪.md "wikilink")[洛阳时没](../Page/洛阳.md "wikilink")。

## 南朝

### 刘宋

  - 长沙景王　[刘道怜](../Page/刘道怜.md "wikilink") 420年-422年在位
  - 长沙成王　[刘义欣](../Page/刘义欣.md "wikilink") 439年薨
  - 长沙悼王　[刘瑾](../Page/刘瑾.md "wikilink") 453年薨
  - 长沙王　　[刘纂](../Page/刘纂.md "wikilink") 478年薨

### 萧齐

  - 长沙威王　[萧晃](../Page/萧晃.md "wikilink") 481年-490年在位

### 萧梁

  - 长沙宣武王[萧懿](../Page/萧懿.md "wikilink")（追封）
  - 长沙嗣王　[萧渊业](../Page/萧渊业.md "wikilink") 502年-526年在位

### 南陈

## 明朝

属于吉王分支。

  - 长沙恭简王[朱翊鋋](../Page/朱翊鋋.md "wikilink")，1557年—1661年在位。
  - 长沙昭靖王[朱常淠](../Page/朱常淠.md "wikilink")，1661年—1648年在位。
  - 长沙王[朱由楫](../Page/朱由楫_\(长沙王\).md "wikilink")，1649年—1662年在位。
  - 长沙王[朱由栉](../Page/朱由栉.md "wikilink")，不知何年袭封。

## 参见

  - [长沙国](../Page/长沙国.md "wikilink")
  - [长沙历史](../Page/长沙历史.md "wikilink")

## 参考资料

  - [后汉书](../Page/后汉书.md "wikilink")

[C](../Category/汉朝诸侯王封号.md "wikilink")
[Category:晋朝诸侯王封号](../Category/晋朝诸侯王封号.md "wikilink")
[Category:明朝诸侯王封号](../Category/明朝诸侯王封号.md "wikilink")
[Category:长沙历史](../Category/长沙历史.md "wikilink")