**长鼻目**（学名：）
是[哺乳动物一个](../Page/哺乳动物.md "wikilink")[目](../Page/目_\(生物\).md "wikilink")，只包含一个现存生物的[科](../Page/科_\(生物\).md "wikilink")，象科，即[大象](../Page/大象.md "wikilink")。包括[普通非洲象](../Page/普通非洲象.md "wikilink")
(Loxodonta africana)、[非洲森林象](../Page/非洲森林象.md "wikilink") (Loxodonta
cyclotis)和[亚洲象](../Page/亚洲象.md "wikilink")（Elephas maximus）三个物种\[1\]。

在[冰川期有很多现已灭绝的物种](../Page/冰川期.md "wikilink")，
包括与大象相似的[猛犸](../Page/猛犸.md "wikilink")(長毛象)、[乳齿象](../Page/乳齿象.md "wikilink")、[恐象](../Page/恐象.md "wikilink")、[铲齿象](../Page/铲齿象.md "wikilink")、[變齒象](../Page/變齒象.md "wikilink")。
已知的最早的长鼻目动物是，以及後來的[始祖象](../Page/始祖象.md "wikilink")。

[更新世時期的](../Page/更新世.md "wikilink")[納瑪象為最大型的象](../Page/納瑪象.md "wikilink")，同時也是有史以來體型最大的陸生哺乳動物\[2\]。

## 参考文献

[\*](../Category/長鼻目.md "wikilink")
[Category:近蹄類](../Category/近蹄類.md "wikilink")

1.
2.