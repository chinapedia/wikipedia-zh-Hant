**門迪**（**Mendi**），[巴布亚新几内亚](../Page/巴布亚新几内亚.md "wikilink")[南高地省省會](../Page/南高地省_\(巴布亞紐幾內亞\).md "wikilink")，位于[新几内亚岛巴布亚新几内亚一侧的中部](../Page/新几内亚岛.md "wikilink")，人口4.3万余人。

城镇坐落与海拔1675米高的门迪河谷，由西向东的石灰岩山上。基科里河（Kikori）發源於門迪所在的山區，埃拉韦河（Erave）與斯特里克兰河（Strickland）河则流经其轄境的巴布亚新几内亚第二高峰Giluwe山。

门迪的人口较为稠密，经济也相对较好。城镇拥有[蔬菜和](../Page/蔬菜.md "wikilink")[咖啡的种植以及](../Page/咖啡.md "wikilink")[茶园](../Page/茶.md "wikilink")，还有一家锯木厂。门迪的交通大都依赖空运，巴布亚新几内亚重干公路，海兰兹高速公路（Highlands
Highway）也经通门迪，东面通往[芒特哈根以及临海的](../Page/芒特哈根.md "wikilink")[莱城](../Page/莱城_\(巴布亚新几内亚\).md "wikilink")。

## 參考來源

  - [www.pacificislandtravel.com](https://web.archive.org/web/20061022160247/http://www.pacificislandtravel.com/png/about_destin/mendi.html)

[Mendi](../Category/巴布亚新几内亚城市.md "wikilink")
[Category:南高地省](../Category/南高地省.md "wikilink")