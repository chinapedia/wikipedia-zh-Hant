**閃電獸龍屬**（[學名](../Page/學名.md "wikilink")：**）是原始[鳥腳下目](../Page/鳥腳下目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[下白堊紀](../Page/下白堊紀.md "wikilink")（[阿爾比階](../Page/阿爾比階.md "wikilink")）的[澳洲](../Page/澳洲.md "wikilink")。閃電獸龍被分類為[稜齒龍科](../Page/稜齒龍科.md "wikilink")，身長約1到1.5公尺，是種。

化石發現於[澳洲](../Page/澳洲.md "wikilink")[新南威爾斯州的閃電山脈](../Page/新南威爾斯州.md "wikilink")，該地因出產[貓眼石礦區與恐龍化石而著名](../Page/貓眼石.md "wikilink")。[正模標本](../Page/正模標本.md "wikilink")（編號BMNH
R.3719）包含[頭顱骨](../Page/頭顱骨.md "wikilink")、[股骨](../Page/股骨.md "wikilink")、以及牙齒。

[模式種是](../Page/模式種.md "wikilink")**南方閃電獸龍**（*F.
australe*），由Huene在1932年正式描述、命名。屬名的*fulgur*在[拉丁文意為](../Page/拉丁文.md "wikilink")「閃電」，*therion*在[古希臘文意為](../Page/古希臘文.md "wikilink")「野獸」\[1\]。

Huene最初將閃電獸龍歸類於[獸腳亞目的](../Page/獸腳亞目.md "wikilink")[似鳥龍科](../Page/似鳥龍科.md "wikilink")\[2\]。之後，閃電獸龍被歸類於[鳥腳下目的](../Page/鳥腳下目.md "wikilink")[稜齒龍類](../Page/稜齒龍類.md "wikilink")\[3\]。但由於牠的[化石有可能是來自幾個鳥腳下目的](../Page/化石.md "wikilink")[物種](../Page/物種.md "wikilink")，所以閃電獸龍被認為是一個[嵌合體](../Page/嵌合體.md "wikilink")。閃電獸龍目前常被認為是[疑名](../Page/疑名.md "wikilink")。

## 參考資料

  -
## 外部連結

  - <https://web.archive.org/web/20130927064547/http://www.thescelosaurus.com/ornithopoda.htm>

[Category:稜齒龍科](../Category/稜齒龍科.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:大洋洲恐龍](../Category/大洋洲恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.  F. v. Huene, 1932, "Die fossile Reptil-Ordnung Saurischia, ihre
    Entwicklung und Geschichte", *Monographien zur Geologie und
    Palaeontologie, serie 1* **4**(1-2): 1-361
2.  F. v. Huene, 1944, "Aussichtsreiche Fundgegenden für künftige
    Sauriergrabungen", *Neues Jahrbuch für Geologie und Paläontologie
    Monatshefte, Abteilung B* **88**: 441-451
3.  R. E. Molnar and P. M. Galton, 1986, "Hypsilophodontid dinosaurs
    from Lightning Ridge, New South Wales, Australia", *Géobios*
    **19**(2): 231-239