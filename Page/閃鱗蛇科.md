**閃鱗蛇科**是[蛇亞目下的一個](../Page/蛇亞目.md "wikilink")[單型科](../Page/單型.md "wikilink")，包含以分佈於[東南亞的閃鱗蛇屬為主的蛇類](../Page/東南亞.md "wikilink")。\[1\]目前閃鱗蛇科內有兩個被確認的品種，牠們的特色是在陽光照射下，其鱗片與體紋會反映出有如[金屬或](../Page/金屬.md "wikilink")[浮油般斑爛的色彩](../Page/油.md "wikilink")。\[2\]（見[參考資料3](../Page/閃鱗蛇科#參考資料.md "wikilink")）

## 特徵

成年的閃鱗蛇約有1[米長](../Page/米_\(單位\).md "wikilink")，頭部的鱗片較為大片，這一點與[新蛇科頗為相似](../Page/新蛇科.md "wikilink")。牠們腹部的鱗片有退化的跡象，[盆骨亦已不存在](../Page/盆骨.md "wikilink")。閃鱗蛇背部的斑紋多呈[紅](../Page/紅.md "wikilink")[棕色或偏](../Page/棕.md "wikilink")[黑色](../Page/黑色.md "wikilink")；腹部沒有特定的紋理，呈[灰](../Page/灰.md "wikilink")[白色](../Page/白.md "wikilink")。\[3\]鱗片色彩斑爛，在光線的照耀下會顯得相當華麗。

閃鱗蛇具有洞棲性，平常都會潛藏於地下。牠們多數只於天色昏暗的時候，才會出現捕食[蛙類](../Page/蛙類.md "wikilink")、其它蛇類，或者小型的[哺乳類動物](../Page/哺乳類.md "wikilink")。\[4\]

## 地理分佈

閃鱗蛇主要分佈於[東南亞地區](../Page/東南亞.md "wikilink")，包括[華南地區](../Page/中國.md "wikilink")（如[海南島](../Page/海南島.md "wikilink")）、[安達曼群島](../Page/安達曼群島.md "wikilink")、[尼科巴群島](../Page/尼科巴群島.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[越南](../Page/越南.md "wikilink")、[馬來半島](../Page/馬來半島.md "wikilink")、[蘇拉威西島與及](../Page/蘇拉威西島.md "wikilink")[菲律賓](../Page/菲律賓.md "wikilink")。\[5\]

## 品種

| 品種\[6\]                                                                 | 學名及命名者\[7\]                                        | 異名  | 地理分佈\[8\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ----------------------------------------------------------------------- | -------------------------------------------------- | --- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **[海南閃鱗蛇](../Page/海南閃鱗蛇.md "wikilink")**                                | Xenopeltis hainanensis，<small>Hu及Zhao，1972</small> |     | [中國內的](../Page/中國.md "wikilink")[浙江至](../Page/浙江.md "wikilink")[廣西](../Page/廣西.md "wikilink")，南至[海南島](../Page/海南島.md "wikilink")                                                                                                                                                                                                                                                                                                                                                                                            |
| **[閃鱗蛇](../Page/閃鱗蛇.md "wikilink")**<font size="-1"><sup>T</sup></font> | Xenopeltis unicolor，<small>Reinwardt，1827</small>  | 日光蛇 | [緬甸](../Page/緬甸.md "wikilink")、[安達曼群島](../Page/安達曼群島.md "wikilink")、[尼科巴群島](../Page/尼科巴群島.md "wikilink")、華南地區（[廣東及](../Page/廣東.md "wikilink")[雲南](../Page/雲南.md "wikilink")）、[泰國](../Page/泰國.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[越南](../Page/越南.md "wikilink")、[馬來西亞西部](../Page/馬來西亞.md "wikilink")、[檳榔嶼](../Page/檳榔嶼.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[砂拉越及](../Page/砂拉越.md "wikilink")[印尼](../Page/印尼.md "wikilink")、[菲律賓國內的多個地區](../Page/菲律賓.md "wikilink")|- |

## 被捕捉的閃鱗蛇

閃鱗蛇並不是常見的寵物，這是因為被飼養的閃鱗蛇死亡率十分高。捕捉閃鱗蛇後的首六個月，與及以海運方式運送牠們的期間，都是非常緊張的時期，閃鱗蛇往往容易死亡。同時，閃鱗蛇相當抗拒被觸摸及把玩，牠們會因此過度緊張甚至夭亡。飼育者必須為閃鱗蛇提供充份的溫度及鬆軟的泥土，方便牠們活於地洞中，另外閃鱗蛇亦只能獨立飼養，而不宜與其它動物一起共同飼育。

## 參考資料

  - [TIGR爬蟲類資料庫：閃鱗蛇](http://reptile-database.reptarium.cz/species.php?genus=Xenopeltis&species=unicolor)

  - [爬行天下資料庫：閃鱗蛇](https://web.archive.org/web/20070807101339/http://www.pxtx.com/sis/classinfo.asp?classid=2854)

  - [圖片：在光線下鱗甲呈彩色的閃鱗蛇](http://reptile.helzone.com/reptile/uploads/reptile/snakes/10865133131.jpg)

## 備註

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:閃鱗蛇科](../Category/閃鱗蛇科.md "wikilink")

1.

2.

3.  Mehrtens JM：《Living Snakes of the World in
    Color》頁480，[紐約](../Page/紐約.md "wikilink")：Sterling
    Publishers，1987年。ISBN 0-8069-6460-X

4.
5.
6.
7.
8.