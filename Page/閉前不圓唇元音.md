**閉前不圓唇母音**是[母音的一種](../Page/母音.md "wikilink")，使用於許多口語[語言之中](../Page/語言.md "wikilink")，其[國際音標的符號為](../Page/國際音標.md "wikilink")
，相對應的[X-SAMPA符號則為](../Page/X-SAMPA.md "wikilink") 。

此母音非常普遍，幾乎在所有超過三個母音的語言中都存在著。

## 特徵

## 此音出現於

<table>
<thead>
<tr class="header">
<th><p>語言</p></th>
<th><p>詞</p></th>
<th><p><a href="../Page/國際音標.md" title="wikilink">IPA</a></p></th>
<th><p>意義</p></th>
<th><p>註記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿布哈茲語.md" title="wikilink">阿布哈茲語</a></p></td>
<td><p><a href="../Page/阿布哈茲字母.md" title="wikilink">ажьырныҳәа</a></p></td>
<td></td>
<td><p>一月</p></td>
<td><p>見<a href="../Page/阿布哈茲語音系.md" title="wikilink">阿布哈茲語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南非語.md" title="wikilink">南非語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>dank<strong>ie</strong></em></a></p></td>
<td></td>
<td><p>謝謝</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿爾巴尼亞語.md" title="wikilink">阿爾巴尼亞語</a></p></td>
<td><p><a href="../Page/阿爾巴尼亞語字母.md" title="wikilink"><em>mal<strong>i</strong></em></a></p></td>
<td><p>/mali/</p></td>
<td><p>山</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿拉伯語.md" title="wikilink">阿拉伯語</a></p></td>
<td><p><a href="../Page/阿拉伯語字母.md" title="wikilink">دين</a></p></td>
<td></td>
<td><p>宗教</p></td>
<td><p>見<a href="../Page/阿拉伯語音系.md" title="wikilink">阿拉伯語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亞美尼亞語.md" title="wikilink">亞美尼亞語</a></p></td>
<td><p><a href="../Page/亞美尼亞語字母.md" title="wikilink"><strong>ի</strong>մ</a></p></td>
<td><p>/im/</p></td>
<td><p>我的</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞塞拜然語.md" title="wikilink">亞塞拜然語</a></p></td>
<td><p><a href="../Page/亞塞拜然語字母.md" title="wikilink"><em>d<strong>i</strong>l<strong>i</strong></em></a></p></td>
<td><p>/dili/</p></td>
<td><p>樹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴斯克語.md" title="wikilink">巴斯克語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>b<strong>i</strong>zar</em></a></p></td>
<td></td>
<td><p>鬍鬚</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孟加拉語.md" title="wikilink">孟加拉語</a></p></td>
<td><p><a href="../Page/孟加拉文#字母.md" title="wikilink">আমি</a></p></td>
<td><p>/ami/</p></td>
<td><p>我</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/緬甸語.md" title="wikilink">緬甸語</a></p></td>
<td><p><a href="../Page/緬甸文字.md" title="wikilink">?</a></p></td>
<td></td>
<td><p>我正在吃</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/粵語.md" title="wikilink">粵語</a></p></td>
<td><p><a href="../Page/漢字.md" title="wikilink">詩</a>/<a href="../Page/香港語言學學會粵語拼音方案.md" title="wikilink"><em>s<strong>i</strong>1</em></a></p></td>
<td></td>
<td><p>詩</p></td>
<td><p>見<a href="../Page/廣州話.md" title="wikilink">廣州話</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/加泰羅尼亞語.md" title="wikilink">加泰羅尼亞語</a>[1]</p></td>
<td><p><a href="../Page/加泰羅尼亞語正字法.md" title="wikilink"><em>s<strong>i</strong>s</em></a></p></td>
<td><p>/sis/</p></td>
<td><p>六</p></td>
<td><p>見<a href="../Page/加泰羅尼亞語音系.md" title="wikilink">加泰羅尼亞語音系</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>lh<strong>i</strong>nko</em></a></p></td>
<td></td>
<td><p>肥</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克羅埃西亞語.md" title="wikilink">克羅埃西亞語</a></p></td>
<td><p><a href="../Page/蓋伊拉丁字母.md" title="wikilink"><em>v<strong>i</strong>no</em></a></p></td>
<td></td>
<td><p>啤酒</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/捷克語.md" title="wikilink">捷克語</a></p></td>
<td><p><a href="../Page/捷克語正字法.md" title="wikilink"><em>b<strong>í</strong>l<strong>ý</strong></em></a></p></td>
<td></td>
<td><p>白色</p></td>
<td><p>見<a href="../Page/捷克語音系.md" title="wikilink">捷克語音系</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>胖的</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丹麥語.md" title="wikilink">丹麥語</a></p></td>
<td><p><a href="../Page/丹麥語和挪威語字母.md" title="wikilink"><em>b<strong>i</strong>l<strong>i</strong>st</em></a></p></td>
<td></td>
<td><p>司機</p></td>
<td><p>見<a href="../Page/丹麥語音系.md" title="wikilink">丹麥語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/荷蘭語.md" title="wikilink">荷蘭語</a></p></td>
<td><p><a href="../Page/荷蘭語正字法.md" title="wikilink"><em>b<strong>ie</strong>t</em></a></p></td>
<td><p>/bit/</p></td>
<td><p>甜菜</p></td>
<td><p>見<a href="../Page/荷蘭語音系.md" title="wikilink">荷蘭語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英語.md" title="wikilink">英語</a></p></td>
<td><p><a href="../Page/英語正字法.md" title="wikilink"><em>b<strong>ee</strong>t</em></a></p></td>
<td></td>
<td><p>甜菜</p></td>
<td><p>見<a href="../Page/英語音系.md" title="wikilink">英語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛沙尼亞語.md" title="wikilink">愛沙尼亞語</a></p></td>
<td><p><a href="../Page/愛沙尼亞字母.md" title="wikilink"><em>t<strong>ii</strong>k</em></a></p></td>
<td></td>
<td><p>池塘</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法羅語.md" title="wikilink">法羅語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em><strong>i</strong>l</em></a></p></td>
<td></td>
<td><p>腳底</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/芬蘭語.md" title="wikilink">芬蘭語</a></p></td>
<td><p><a href="../Page/芬蘭字母.md" title="wikilink"><em>v<strong>ii</strong>s<strong>i</strong></em></a></p></td>
<td></td>
<td><p>五</p></td>
<td><p>見<a href="../Page/芬蘭語音系學.md" title="wikilink">芬蘭語音系學</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法語.md" title="wikilink">法語</a>[2]</p></td>
<td><p><a href="../Page/法語正字法.md" title="wikilink"><em>f<strong>i</strong>n<strong>i</strong></em></a></p></td>
<td><p>/fi'ni/</p></td>
<td><p>結束</p></td>
<td><p>見<a href="../Page/法語音系.md" title="wikilink">法語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/喬治亞語.md" title="wikilink">喬治亞語</a>[3]</p></td>
<td><p><a href="../Page/喬治亞字母.md" title="wikilink">სამ<strong>ი</strong></a></p></td>
<td></td>
<td><p>三</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德語.md" title="wikilink">德語</a></p></td>
<td><p><a href="../Page/德語正字法.md" title="wikilink"><em>Z<strong>ie</strong>l</em></a></p></td>
<td></td>
<td><p>目的</p></td>
<td><p>見<a href="../Page/德語音系學.md" title="wikilink">德語音系學</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/希臘語.md" title="wikilink">希臘語</a></p></td>
<td><p><a href="../Page/希臘字母.md" title="wikilink"><strong>υ</strong>γ<strong>ιει</strong>ν<strong>ή</strong></a></p></td>
<td></td>
<td><p>衛生</p></td>
<td><p>也可表示為<οι>及<υι>。見<a href="../Page/現代希臘語音系.md" title="wikilink">現代希臘語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瓜拉尼語.md" title="wikilink">瓜拉尼語</a></p></td>
<td><p><a href="../Page/瓜拉尼語字母.md" title="wikilink"><em>ha’ukur<strong>i</strong></em></a></p></td>
<td></td>
<td><p>瓜尼拉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈特語.md" title="wikilink">哈特語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>g<strong>ii</strong></em></a></p></td>
<td></td>
<td><p>'?'</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏威夷語.md" title="wikilink">夏威夷語</a></p></td>
<td><p><a href="../Page/夏威夷語.md" title="wikilink"><em>makan<strong>i</strong></em></a></p></td>
<td><p>/makani/</p></td>
<td><p>微風</p></td>
<td><p>見<a href="../Page/夏威夷語音系.md" title="wikilink">夏威夷語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/印地語.md" title="wikilink">印地語</a></p></td>
<td><p><a href="../Page/天城文.md" title="wikilink">तीन</a></p></td>
<td></td>
<td><p>三</p></td>
<td><p>見<a href="../Page/印地語和烏爾都語音系.md" title="wikilink">印地語和烏爾都語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/匈牙利語.md" title="wikilink">匈牙利語</a></p></td>
<td><p><a href="../Page/匈牙利語#字母.md" title="wikilink"><strong><em>í</em></strong>v</a></p></td>
<td></td>
<td><p>拱形</p></td>
<td><p>見<a href="../Page/匈牙利語音系.md" title="wikilink">匈牙利語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/冰島語.md" title="wikilink">冰島語</a></p></td>
<td><p><a href="../Page/冰島語#字母.md" title="wikilink"><em>l<strong>í</strong>ka</em></a></p></td>
<td></td>
<td><p>也</p></td>
<td><p>見<a href="../Page/冰島語音系.md" title="wikilink">冰島語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印尼語.md" title="wikilink">印尼語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><strong><em>i<strong>n</strong>i</em></strong></a></p></td>
<td><p>/ini/</p></td>
<td><p>這</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛爾蘭語.md" title="wikilink">愛爾蘭語</a></p></td>
<td><p><a href="../Page/愛爾蘭語正字法.md" title="wikilink"><em>s<strong>í</strong></em></a></p></td>
<td></td>
<td><p>她</p></td>
<td><p>見<a href="../Page/愛爾蘭語音系.md" title="wikilink">愛爾蘭語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/義大利語.md" title="wikilink">義大利語</a>[4]</p></td>
<td><p><a href="../Page/義大利字母.md" title="wikilink"><em>b<strong>i</strong>le</em></a></p></td>
<td></td>
<td><p>狂暴</p></td>
<td><p>見<a href="../Page/義大利語音系.md" title="wikilink">義大利語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/日語.md" title="wikilink">日語</a></p></td>
<td><p>-{<a href="../Page/日文漢字.md" title="wikilink">銀</a>}-（<a href="../Page/假名.md" title="wikilink">ぎん</a>）/<a href="../Page/日語羅馬字.md" title="wikilink"><em>g<strong>i</strong>n</em></a></p></td>
<td></td>
<td><p>銀</p></td>
<td><p>見<a href="../Page/日語音系.md" title="wikilink">日語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朝鮮語.md" title="wikilink">朝鮮語</a></p></td>
<td><p><a href="../Page/諺文.md" title="wikilink">시장</a>/<a href="../Page/朝鮮語羅馬字表記法.md" title="wikilink"><em>s<strong>i</strong>jang</em></a></p></td>
<td></td>
<td><p>市場</p></td>
<td><p>見<a href="../Page/朝鮮語音系.md" title="wikilink">朝鮮語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/庫德語.md" title="wikilink">庫德語</a></p></td>
<td><p><a href="../Page/庫德字母.md" title="wikilink"><em>z<strong>î</strong>ndu</em></a></p></td>
<td></td>
<td><p>存活</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬其頓語.md" title="wikilink">馬其頓語</a></p></td>
<td><p><a href="../Page/馬其頓字母.md" title="wikilink">јаз<strong>и</strong>к</a></p></td>
<td><p>/jazik/</p></td>
<td><p>舌頭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬爾他語.md" title="wikilink">馬爾他語</a></p></td>
<td><p><a href="../Page/馬爾他字母.md" title="wikilink"><em>b<strong>ie</strong>b</em></a></p></td>
<td></td>
<td><p>門</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/漢語.md" title="wikilink">漢語</a></p></td>
<td><p><a href="../Page/官話.md" title="wikilink">北京</a>/<a href="../Page/漢語拼音方案.md" title="wikilink"><em>Běij<strong>ī</strong>ng</em></a></p></td>
<td></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a></p></td>
<td><p>見<a href="../Page/現代標準漢語.md" title="wikilink">現代標準漢語</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/納瓦霍語.md" title="wikilink">納瓦霍語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink">{{Unicode</a></p></td>
<td></td>
<td><p>仙人掌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/挪威語.md" title="wikilink">挪威語</a></p></td>
<td><p><a href="../Page/丹麥語和挪威語字母.md" title="wikilink"><em><strong>i</strong>s</em></a></p></td>
<td></td>
<td><p>冰</p></td>
<td><p>見<a href="../Page/挪威語音系.md" title="wikilink">挪威語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奧克語.md" title="wikilink">奧克語</a></p></td>
<td><p><a href="../Page/北奧克語.md" title="wikilink">北奧克語和</a><a href="../Page/南奧克語.md" title="wikilink">南奧克語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>m<strong>i</strong>ralhar</em></a></p></td>
<td></td>
<td><p>反射</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加斯科語.md" title="wikilink">加斯科語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>pol<strong>i</strong>da</em></a></p></td>
<td></td>
<td><p>漂亮</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/普什圖語.md" title="wikilink">普什圖語</a></p></td>
<td><p><a href="../Page/阿拉伯字母.md" title="wikilink">ﭙﺎﻧﻴﺮ</a></p></td>
<td></td>
<td><p>起司</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波斯語.md" title="wikilink">波斯語</a></p></td>
<td><p><a href="../Page/波斯字母.md" title="wikilink">کی</a></p></td>
<td></td>
<td><p>誰</p></td>
<td><p>見<a href="../Page/波斯語音系.md" title="wikilink">波斯語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皮拉罕語.md" title="wikilink">皮拉罕語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>baíx<strong>i</strong></em></a></p></td>
<td></td>
<td><p>父母</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波蘭語.md" title="wikilink">波蘭語</a>[5]</p></td>
<td><p><a href="../Page/波蘭語字母.md" title="wikilink"><em>m<strong>i</strong>ś</em></a></p></td>
<td></td>
<td><p>玩具熊</p></td>
<td><p>見<a href="../Page/波蘭語音系.md" title="wikilink">波蘭語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葡萄牙語.md" title="wikilink">葡萄牙語</a>[6]</p></td>
<td><p><a href="../Page/葡萄牙語正字法.md" title="wikilink"><em>l<strong>i</strong></em></a></p></td>
<td></td>
<td><p>讀</p></td>
<td><p>見<a href="../Page/葡萄牙語音系.md" title="wikilink">葡萄牙語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蓋丘亞語.md" title="wikilink">蓋丘亞語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>all<strong>i</strong>n</em></a></p></td>
<td></td>
<td><p>好</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅馬尼亞語.md" title="wikilink">羅馬尼亞語</a></p></td>
<td><p><a href="../Page/羅馬尼亞字母.md" title="wikilink"><em><strong>i</strong>nsulă</em></a></p></td>
<td></td>
<td><p>島</p></td>
<td><p>見<a href="../Page/羅馬尼亞語音系.md" title="wikilink">羅馬尼亞語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/俄語.md" title="wikilink">俄語</a>[7]</p></td>
<td><p><a href="../Page/俄語正字法.md" title="wikilink">л<strong>и</strong>ст</a></p></td>
<td></td>
<td><p>树叶</p></td>
<td><p>只出現於詞的開頭或<a href="../Page/顎音化.md" title="wikilink">顎音化字音之後</a>。見<a href="../Page/俄语音韵学.md" title="wikilink">俄语音韵学</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇格蘭蓋爾語.md" title="wikilink">蘇格蘭蓋爾語</a></p></td>
<td><p><a href="../Page/蘇格蘭蓋爾字母.md" title="wikilink"><em>ch<strong>ì</strong></em></a></p></td>
<td></td>
<td><p>見</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞爾維亞語.md" title="wikilink">塞爾維亞語</a></p></td>
<td><p><a href="../Page/西里爾字母.md" title="wikilink">м<strong>и</strong>л<strong>и</strong>на</a>/<a href="../Page/拉丁字母.md" title="wikilink"><em>m<strong>i</strong>l<strong>i</strong>na</em></a></p></td>
<td><p>/milina/</p></td>
<td><p>享受</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Seri_alphabet.md" title="wikilink"><em>cm<strong>ii</strong>que</em></a></p></td>
<td></td>
<td><p>人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/信德語.md" title="wikilink">信德語</a></p></td>
<td><p><a href="../Page/阿拉伯字母.md" title="wikilink">سنڌي</a></p></td>
<td></td>
<td><p>信德</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇族語.md" title="wikilink">蘇族語</a></p></td>
<td><p>[8][9]</p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink">{{Unicode</a></p></td>
<td></td>
<td><p>棕色</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯洛伐克語.md" title="wikilink">斯洛伐克語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>chlap<strong>i</strong></em></a></p></td>
<td><p>/xlapi/</p></td>
<td><p>男人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西班牙語.md" title="wikilink">西班牙語</a>[10]</p></td>
<td><p><a href="../Page/西班牙語正字法.md" title="wikilink"><em>t<strong>i</strong>po</em></a></p></td>
<td></td>
<td><p>類型</p></td>
<td><p>也可表示為 <y> 。見<a href="../Page/西班牙語音系.md" title="wikilink">西班牙語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯瓦希里語.md" title="wikilink">斯瓦希里語</a></p></td>
<td><p><a href="../Page/Latin_script.md" title="wikilink"><em>m<strong>i</strong>t<strong>i</strong></em></a></p></td>
<td><p>/miti/</p></td>
<td><p>樹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瑞典語.md" title="wikilink">瑞典語</a></p></td>
<td><p><a href="../Page/瑞典字母.md" title="wikilink"><em><strong>i</strong>s</em></a></p></td>
<td></td>
<td><p>冰</p></td>
<td><p>見<a href="../Page/瑞典語音系.md" title="wikilink">瑞典語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/他加祿語.md" title="wikilink">他加祿語</a></p></td>
<td><p><a href="../Page/菲律賓語正字法.md" title="wikilink"><em>s<strong>i</strong>lya</em></a></p></td>
<td></td>
<td><p>椅子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塔吉克語.md" title="wikilink">塔吉克語</a></p></td>
<td><p><a href="../Page/塔吉克字母.md" title="wikilink">{{Unicode</a></p></td>
<td></td>
<td><p>鼻子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/土耳其語.md" title="wikilink">土耳其語</a></p></td>
<td><p><a href="../Page/土耳其语字母.md" title="wikilink"><em><strong>i</strong>p</em></a></p></td>
<td><p>/ip/</p></td>
<td><p>繩子</p></td>
<td><p>見<a href="../Page/土耳其語音系.md" title="wikilink">土耳其語音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尤比克語.md" title="wikilink">尤比克語</a></p></td>
<td></td>
<td><p>心</p></td>
<td><p>在顎音化子音後的同位音。見<a href="../Page/尤比克語音系.md" title="wikilink">尤比克語音系</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上索布語.md" title="wikilink">上索布語</a></p></td>
<td></td>
<td></td>
<td><p>擊敗</p></td>
<td><p>見</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/越南語.md" title="wikilink">越南語</a></p></td>
<td><p><a href="../Page/越南語字母.md" title="wikilink"><em>t<strong>y</strong></em></a></p></td>
<td></td>
<td><p>局</p></td>
<td><p>見<a href="../Page/越南語音系.md" title="wikilink">越南語音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佛羅語.md" title="wikilink">佛羅語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>kirotas</em></a></p></td>
<td></td>
<td><p>寫</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/威爾斯語.md" title="wikilink">威爾斯語</a></p></td>
<td><p><a href="../Page/威爾斯字母.md" title="wikilink"><em>h<strong>i</strong>r</em></a></p></td>
<td></td>
<td><p>十二月</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西菲士蘭語.md" title="wikilink">西菲士蘭語</a></p></td>
<td></td>
<td></td>
<td><p>十二月</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/祖魯語.md" title="wikilink">祖魯語</a></p></td>
<td><p><a href="../Page/拉丁字母.md" title="wikilink"><em>umuz<strong>i</strong></em></a></p></td>
<td></td>
<td><p>村</p></td>
<td></td>
</tr>
</tbody>
</table>

## 註釋

## 參考文獻

  -
  -
  -
  -
  -
{{-}}

[Category:元音](../Category/元音.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.  [Lakota Language Consortium](http://www.lakhota.org) (2004). [Lakota
    letters and sounds](http://www.lakhota.org/ALPHABET/alphabet.htm).
10.