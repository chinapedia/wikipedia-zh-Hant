**開成**（836年正月—840年十二月）是[唐文宗的](../Page/唐文宗.md "wikilink")[年號](../Page/年號.md "wikilink")，共计5年。

開成五年正月[唐武宗李瀍即位沿用](../Page/唐武宗.md "wikilink")。

## 大事記

## 出生

## 逝世

## 紀年

| 開成                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 836年                           | 837年                           | 838年                           | 839年                           | 840年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 同期存在的其他政权年号
      - [承和](../Page/承和_\(仁明天皇\).md "wikilink")（834年正月三日至848年六月十三日）：平安時代[仁明天皇之年號](../Page/仁明天皇.md "wikilink")
      - [咸和](../Page/咸和_\(大彝震\).md "wikilink")（831年至857年）：[渤海宣王](../Page/渤海國.md "wikilink")[大彝震之年號](../Page/大彝震.md "wikilink")
      - [保和](../Page/保和.md "wikilink")（824年至839年）：[南詔領袖](../Page/南詔.md "wikilink")[勸豐祐之年號](../Page/勸豐祐.md "wikilink")
      - [天啓](../Page/天启_\(劝丰佑\).md "wikilink")（840年至859年）：南詔領袖勸豐祐之年號
      - [彝泰](../Page/彝泰.md "wikilink")（815年至838年）：[吐蕃可黎可足](../Page/吐蕃.md "wikilink")[贊普](../Page/贊普.md "wikilink")[赤祖德贊之年號](../Page/赤祖德贊.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:9世纪中国年号](../Category/9世纪中国年号.md "wikilink")
[Category:830年代中国政治](../Category/830年代中国政治.md "wikilink")
[Category:840年代中国政治](../Category/840年代中国政治.md "wikilink")