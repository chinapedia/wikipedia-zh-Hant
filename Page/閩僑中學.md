[Man_Kiu_College_campus_201305.JPG](https://zh.wikipedia.org/wiki/File:Man_Kiu_College_campus_201305.JPG "fig:Man_Kiu_College_campus_201305.JPG")
[HK_North_Point_Cloud_View_Road_Man_Kiu_School_Nov-2015_DSC.JPG](https://zh.wikipedia.org/wiki/File:HK_North_Point_Cloud_View_Road_Man_Kiu_School_Nov-2015_DSC.JPG "fig:HK_North_Point_Cloud_View_Road_Man_Kiu_School_Nov-2015_DSC.JPG")
**閩僑中學**（）位於[香港](../Page/香港.md "wikilink")[北角雲景道](../Page/北角.md "wikilink")81號，創校於1977年\[1\]，是一間屬於港島[東區津貼全日制男女子中學](../Page/東區_\(香港\).md "wikilink")。

## 校舍

閩僑小學校舍位於[寶馬山上](../Page/寶馬山.md "wikilink")。佔地7萬平方呎，全校課室、特別室、禮堂等均有空調設備，並設有電腦、LCD投影機及屏幕。設有升降機，供教職員及有需要的學生使用。

校舍內的設施如下：

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>32間課室</li>
</ul>
<ul>
<li>4間<a href="../Page/實驗室.md" title="wikilink">實驗室</a>
<ul>
<li><a href="../Page/生物.md" title="wikilink">生物實驗室</a></li>
<li><a href="../Page/物理.md" title="wikilink">物理實驗室</a></li>
<li><a href="../Page/化學.md" title="wikilink">化學實驗室</a></li>
<li><a href="../Page/科學.md" title="wikilink">科學實驗室</a></li>
</ul></li>
</ul>
<ul>
<li><a href="../Page/廣播.md" title="wikilink">廣播室</a></li>
</ul>
<ul>
<li>4間<a href="../Page/電腦.md" title="wikilink">電腦室</a>
<ul>
<li><a href="../Page/電腦.md" title="wikilink">電腦輔助教學室</a></li>
<li>七樓<a href="../Page/電腦.md" title="wikilink">電腦室</a></li>
<li><a href="../Page/語言.md" title="wikilink">語言教學室</a></li>
<li><a href="../Page/多媒體.md" title="wikilink">多媒體</a><a href="../Page/學習.md" title="wikilink">學習中心</a></li>
</ul></li>
</ul>
<ul>
<li><a href="../Page/地理.md" title="wikilink">地理室</a></li>
<li><a href="../Page/家政.md" title="wikilink">家政室</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/音樂.md" title="wikilink">音樂室</a></li>
<li><a href="../Page/美術.md" title="wikilink">美術室</a></li>
<li><a href="../Page/設計.md" title="wikilink">設計與</a><a href="../Page/科技.md" title="wikilink">科技室</a></li>
</ul>
<ul>
<li>2間<a href="../Page/聖約翰救傷隊.md" title="wikilink">聖約翰救傷隊專用室</a>
<ul>
<li><a href="../Page/聖約翰救傷隊.md" title="wikilink">聖約翰救傷隊儲物室</a></li>
<li><a href="../Page/聖約翰救傷隊.md" title="wikilink">聖約翰救傷隊急救室</a></li>
</ul></li>
</ul>
<ul>
<li><a href="../Page/小食.md" title="wikilink">小食部</a></li>
<li>飯堂</li>
<li><a href="../Page/學生會.md" title="wikilink">學生會辦事處</a></li>
<li>學生活動中心（多用途學習室）</li>
<li>舞蹈室</li>
<li><a href="../Page/胡文虎.md" title="wikilink">胡文虎紀念堂</a>（多用途禮堂，雙層）</li>
<li>自修室</li>
</ul></td>
<td><ul>
<li>總務處（校務處）</li>
</ul>
<ul>
<li>4間教員室
<ul>
<li>四樓教員室</li>
<li>一樓教員室</li>
<li>2間教員休息室</li>
<li>輔導組教員室</li>
<li>訓導組教員室</li>
<li>訓導處會客室</li>
</ul></li>
</ul>
<ul>
<li>4個學生運動場所
<ul>
<li>多用途籃球場</li>
<li>4個羽毛球場</li>
<li>小足球場</li>
<li>有蓋操場</li>
</ul></li>
<li><a href="../Page/圖書館.md" title="wikilink">圖書館</a></li>
<li>英文室</li>
<li>會議室</li>
<li>社工室</li>
</ul>
<p>　</p></td>
</tr>
</tbody>
</table>

## 辦學宗旨

以「孝悌忠信」為校訓，為學生提供德智體群美五育並重全人教育，培育學生健全人格，成為良好公民，熱愛家庭，為社會、國家和世界作出貢獻。

## 課外活動

閩僑中學在不同的組別上舉辦了多元化的課外及學術常識活動。以下是課外活動的分佈：

### 服務組別

  - 學長會
  - [聖約翰救傷隊](http://www.mkc.edu.hk/club/service/stjohn/2006/index.htm)
  - 圖書館學長會
  - 童軍
  - 公益少年團
  - 步操樂隊

### 學術組別

  - 英文學會
  - 電腦學會
  - 數理學會
  - 文史學會
  - 社會科學學會

### 興趣組別

  - 音樂學會
  - 攝影學會
  - 普通話學會
  - 視覺藝術學會
  - 舞蹈組
  - 設計與科技學會

### 體育組別

  - 田徑隊
  - 足球隊
  - 籃球隊
  - 羽毛球隊
  - 乒 乓球隊

## 著名/傑出校友

  - 黃旭：[香港理工大學管理及市場學系教授](../Page/香港理工大學.md "wikilink")
  - 魏濤：[香港中文大學化學系副教授](../Page/香港中文大學.md "wikilink")
  - 謝子豪：美國[喬治亞大學工程學院助理教授](../Page/喬治亞大學.md "wikilink")
  - [陳仕燊](../Page/陳仕燊.md "wikilink")：樂隊[鐵樹蘭](../Page/鐵樹蘭.md "wikilink")、[Supper
    Moment主音](../Page/Supper_Moment.md "wikilink")
  - [李任燊](../Page/李任燊.md "wikilink")：香港男演員\[2\]\[3\][閩僑中學《2007/08年度下學期校訊》](http://docsplayer.com/27481852-Microsoft-word-%E4%B8%8B%E5%AD%B8%E6%9C%9F%E6%A0%A1%E8%A8%8A-doc.htmlf)</ref>
  - [邵家輝](../Page/邵家輝.md "wikilink")：香港立法會議員（批發及零售界）、自由黨副主席及東區寶馬山選區民選區議員
  - 程瑤博士: 仁美清敘慈善機構主席、大舞臺節目及傳訊有限公司總監，於2017年獲世界傑出華人青年企業家獎 \[4\]\[5\]

## 鄰近設施

  - [天后廟道配水庫遊樂場](../Page/天后廟道配水庫.md "wikilink")
  - [賽西湖公園及其快餐亭](../Page/賽西湖公園.md "wikilink")
  - [賽西湖商場](../Page/賽西湖商場.md "wikilink")
  - 寶馬山[消防局暨](../Page/消防局.md "wikilink")[救護站](../Page/救護站.md "wikilink")
  - [北角站](../Page/北角站.md "wikilink")
  - [炮台山站](../Page/炮台山站.md "wikilink")

## 注釋

## 資料來源

## 外部連結

  - [閩僑中學](http://www.mkc.edu.hk)
  - [LifeinHK:閩僑中學](http://www.lifein.hk/Education/SecondarySchDet.aspx?id=593&name=%E9%96%A9%E5%83%91%E4%B8%AD%E5%AD%B8)

[Category:北角](../Category/北角.md "wikilink")
[M](../Category/香港東區中學.md "wikilink")
[Category:寶馬山](../Category/寶馬山.md "wikilink")
[Category:1977年創建的教育機構](../Category/1977年創建的教育機構.md "wikilink")

1.

2.  [閩僑中學《第十屆至第十三屆學生會幹事名單》](http://www.mkc.edu.hk/club/su/1-19.pdf)

3.
4.  [東網](http://hk.on.cc/hk/bkn/cnt/entertainment/20180612/bkn-20180612221132066-0612_00862_001.html)

5.  [香港01](https://www.hk01.com/%E5%8D%B3%E6%99%82%E5%A8%9B%E6%A8%82/199494/%E7%A8%8B%E7%91%A4%E6%94%9C%E5%AD%90%E9%87%8D%E8%BF%94%E6%AF%8D%E6%A0%A1%E4%BB%BB%E7%95%A2%E6%A5%AD%E5%85%B8%E7%A6%AE%E5%98%89%E8%B3%93-%E7%88%86%E5%A4%A7%E4%BB%94%E5%8F%97%E6%AD%A1%E8%BF%8E-%E5%A5%B3%E5%90%8C%E5%AD%B8%E7%88%AD%E4%BD%8F%E5%90%88%E7%85%A7)