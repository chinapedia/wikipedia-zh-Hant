**闊德城** （Quad
Cities）是[美國一個位於](../Page/美國.md "wikilink")[伊利諾伊州西北部和](../Page/伊利諾伊州.md "wikilink")[愛阿華州東南部的都會區](../Page/愛阿華州.md "wikilink")，由四個縣組成。2006年人口377,291人。\[1\]

五個中心城市為別為：

  - [達文波特](../Page/達文波特_\(愛阿華州\).md "wikilink")
  - [羅克艾蘭](../Page/羅克艾蘭_\(伊利諾伊州\).md "wikilink")
  - [莫林](../Page/莫林.md "wikilink")
  - [東莫林](../Page/東莫林.md "wikilink")
  - [貝頓多夫](../Page/貝頓多夫.md "wikilink")

## 交通

[密西西比河和](../Page/密西西比河.md "wikilink")[80號州際公路在此交匯](../Page/80號州際公路.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:伊利諾伊州城市](../Category/伊利諾伊州城市.md "wikilink")
[Category:艾奧瓦州城市](../Category/艾奧瓦州城市.md "wikilink")
[Category:伊利诺伊州大都市区](../Category/伊利诺伊州大都市区.md "wikilink")

1.