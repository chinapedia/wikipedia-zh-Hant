**闊步龍屬**（[學名](../Page/學名.md "wikilink")：*Hypsibema*）是[鴨嘴龍超科的一](../Page/鴨嘴龍超科.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於[上白堊紀](../Page/上白堊紀.md "wikilink")[坎帕階的](../Page/坎帕階.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")，約7500萬年前，但目前對其所知甚少。牠的[化石是在](../Page/化石.md "wikilink")[美國](../Page/美國.md "wikilink")[北卡羅來納州及](../Page/北卡羅來納州.md "wikilink")[密蘇里州發現](../Page/密蘇里州.md "wikilink")。雖然在密蘇里州發現的化石最初被認為是屬於小型的[蜥腳下目](../Page/蜥腳下目.md "wikilink")，但目前推測牠是屬於[鴨嘴龍超科](../Page/鴨嘴龍超科.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")**粗尾闊步龍**（*H.
crassicauda*），是由[愛德華·德林克·科普](../Page/愛德華·德林克·科普.md "wikilink")（Edward
Drinker Cope）於1869年描述、命名。另一個[物種密蘇里闊步龍](../Page/物種.md "wikilink")（*H.
missouriensis*）[是密蘇里州的](../Page/是密蘇里州.md "wikilink")[州恐龍](../Page/州恐龍.md "wikilink")。由於兩個物種都只有化石碎片，故被認為是[疑名](../Page/疑名.md "wikilink")。

## 參考

  - [恐龍博物館──闊步龍](https://web.archive.org/web/20071109033924/http://www.dinosaur.net.cn/museum/Hypsibema.htm)
  - <http://web.me.com/dinoruss/de_4/5a8bb11.htm>

[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:鴨嘴龍超科](../Category/鴨嘴龍超科.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")