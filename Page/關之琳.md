**關之琳**（，），[瓜尔佳氏](../Page/瓜尔佳氏.md "wikilink")\[1\]，[满洲](../Page/满洲八旗.md "wikilink")[正白旗后裔](../Page/正白旗.md "wikilink")\[2\]，本名**關家慧**，[香港著名](../Page/香港.md "wikilink")[電影](../Page/電影.md "wikilink")[女演員](../Page/女演員.md "wikilink")。

## 簡介

關之琳誕生於演藝家庭，父親[關山籍貫](../Page/關山_\(演員\).md "wikilink")[遼寧](../Page/遼寧.md "wikilink")[瀋陽](../Page/瀋陽.md "wikilink")，是香港1950年代及1960年代著名電影、電視演員；母親是香港[長城電影公司的著名女演員](../Page/長城電影公司.md "wikilink")[張冰茜](../Page/張冰茜.md "wikilink")，另有一个弟弟。

關之琳曾經就讀九龍聖德肋撒英文學校、[瑪利諾修院學校](../Page/瑪利諾修院學校.md "wikilink")、[德雅中學](../Page/德雅中學.md "wikilink")。因為父母都是電影工作者，1981年，關之琳中學畢業後加入[麗的電視成為藝員](../Page/麗的電視.md "wikilink")，首部電視劇作品是《[甜甜廿四味](../Page/甜甜廿四味.md "wikilink")》，同劇中男主角有[張國榮](../Page/張國榮.md "wikilink")、[鍾保羅](../Page/鍾保羅.md "wikilink")、[莫少聰及](../Page/莫少聰.md "wikilink")[蔡鎮川等](../Page/蔡鎮川.md "wikilink")。1982年關之琳參與首部電影《再見江湖》的演出，獲得一致好評，之後便開始了她的演藝歷程。1990年代以与[李连杰合作的](../Page/李连杰.md "wikilink")[黃飛鴻系列中的](../Page/黃飛鴻.md "wikilink")[十三姨一角走紅於中港台三地](../Page/十三姨.md "wikilink")，亦被评价为香港电影史上最经典的女性角色之一；同时也与[成龙](../Page/成龙.md "wikilink")、[劉德華等巨星有过多次合作](../Page/劉德華.md "wikilink")。

2005年後逐漸淡出演藝圈但同時運用人脈積極投入[房地產](../Page/房地產.md "wikilink")，2007年斥资1.13亿港元购入位于南湾道面对香港[浅水湾的The](../Page/浅水湾.md "wikilink")
Nautilus独立豪宅，2010年房價已經上漲一倍，她另還有四處豪宅，估計值超過5億港元。

2010年關之琳宣布息影，并且将重心转入更多的[慈善公益活动中](../Page/慈善.md "wikilink")。她也喜欢收藏[手表](../Page/手表.md "wikilink")。\[3\]

2018年，關之琳推出個人護膚品牌，接受訪問時透露自己的健康飲食及護膚習慣\[4\]。

## 感情生活

關之琳的感情不時成為香港傳媒追逐的話題，其對象多是有婦之夫。1981年，關之琳不顾[關山和張冰茜的反對](../Page/關山_\(演員\).md "wikilink")，與認識僅兩個月的香港富商[王國旌在](../Page/王國旌.md "wikilink")[美國](../Page/美國.md "wikilink")[拉斯維加斯閃電](../Page/拉斯維加斯.md "wikilink")[結婚](../Page/結婚.md "wikilink")，但這段關係僅維持了9個月，之後关之琳提出離婚返港。

1984年，關之琳與地產富商[馬清偉傳出緋聞](../Page/馬清偉.md "wikilink")，成為馬清偉與TVB女藝人[陳美琪離婚及陳美琪小產的導火線](../Page/陳美琪.md "wikilink")，但关之琳始终否认主动介入。

1985年，與[成龍拍攝電影](../Page/成龍.md "wikilink")《[龍兄虎弟](../Page/龍兄虎弟_\(1987年電影\).md "wikilink")》時，亦傳出地下戀情。

1987年，與[劉德華在](../Page/劉德華.md "wikilink")《[群龍奪寶](../Page/群龍奪寶.md "wikilink")》首次合作，其後兩人多次合作演出情侣角色成為公认的一对「銀幕情侶」，也曾傳出緋聞，而当双方始终表示是关系甚佳的好友。

1991年，與當時已婚的富商[劉鑾雄交往大约三年](../Page/劉鑾雄.md "wikilink")。（
傳聞關之琳曾經被劉鑾雄性暴力對待）但關不願意提起

2000年，38歲的關之琳與名模特兒[黃家諾戀情曝光](../Page/黃家諾.md "wikilink")，由於當時女方比男方年長8年，一直被外間指為[姊弟戀](../Page/姊弟戀.md "wikilink")，到2004年分手。

2004年，關之琳又傳出與馬清偉的緋聞，但最後无果。

2009年，在台灣代言、拍廣告時多次與當時已婚的國巨企業董事長陳泰銘温馨同行，其感情生活再度掀起話題，隨後陳泰銘離婚。2015年11月突然向傳媒宣佈自己已與陳泰銘離婚。\[5\]\[6\]

## 電視劇（[麗的電視](../Page/麗的電視.md "wikilink")）

|                                        |                                          |        |                                                                                                                                                                                                                                                                                                                                                   |            |
| -------------------------------------- | ---------------------------------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------- |
| **年份**                                 | **劇名**                                   | **角色** | **合作演員**                                                                                                                                                                                                                                                                                                                                          | **備註**     |
| 1981                                   | [甜甜廿四味](../Page/甜甜廿四味.md "wikilink")     | 艾明     | [張國榮](../Page/張國榮.md "wikilink")、[倪詩蓓](../Page/倪詩蓓.md "wikilink")、[鍾保羅](../Page/鍾保羅.md "wikilink")、[莫少聰](../Page/莫少聰.md "wikilink")、[蔡鎮川](../Page/蔡鎮川.md "wikilink")、[文雪兒](../Page/文雪兒.md "wikilink")                                                                                                                                             | 其角色在第4集才登場 |
| 1981                                   | [阿拉擔梯](../Page/阿拉擔梯.md "wikilink")       |        | [喬宏](../Page/喬宏.md "wikilink")、[馮真](../Page/馮真.md "wikilink")、[顧紀筠](../Page/顧紀筠.md "wikilink")、[林國雄](../Page/林國雄_\(演員\).md "wikilink")、[蔣金](../Page/蔣金.md "wikilink")、[黃植森](../Page/黃植森.md "wikilink")、[許英秀](../Page/許英秀.md "wikilink")                                                                                                           |            |
| 1982                                   | [大將軍](../Page/大將軍_\(電視劇\).md "wikilink") | 李潤儀    | [何家勁](../Page/何家勁.md "wikilink")、[黎漢持](../Page/黎漢持.md "wikilink")、[文雪兒](../Page/文雪兒.md "wikilink")、[林國雄](../Page/林國雄_\(演員\).md "wikilink")、[蔣金](../Page/蔣金.md "wikilink")、[張瑛](../Page/張瑛.md "wikilink")、[王偉](../Page/王偉_\(香港\).md "wikilink")、[羅樂林](../Page/羅樂林.md "wikilink")、[梁舜燕](../Page/梁舜燕.md "wikilink")、[梁漢威](../Page/梁漢威.md "wikilink") |            |
| 1982                                   | [天堂有路](../Page/天堂有路.md "wikilink")       |        | [岳華](../Page/岳華.md "wikilink")、[良鳴](../Page/良鳴.md "wikilink")、[張瑛](../Page/張瑛.md "wikilink")、[苗金鳳](../Page/苗金鳳.md "wikilink")、[李燕燕](../Page/李燕燕.md "wikilink")、[邵音音](../Page/邵音音.md "wikilink")、[梁舜燕](../Page/梁舜燕.md "wikilink")                                                                                                                  |            |
| 1982                                   | [再見狂牛](../Page/再見狂牛.md "wikilink")       | 宋以寧    | [何家勁](../Page/何家勁.md "wikilink")、[蔡瓊輝](../Page/蔡瓊輝.md "wikilink")、[曹達華](../Page/曹達華.md "wikilink")、[王偉](../Page/王偉_\(香港\).md "wikilink")、[容惠雯](../Page/容惠雯.md "wikilink")                                                                                                                                                                         |            |
| **[無綫電視](../Page/無綫電視.md "wikilink")** |                                          |        |                                                                                                                                                                                                                                                                                                                                                   |            |
| 1992                                   | 摩登小男人                                    | Rosa   | [鄭丹瑞](../Page/鄭丹瑞.md "wikilink")                                                                                                                                                                                                                                                                                                                  | 演出第1集      |

## 電影

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>片名</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>合作演員</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p>1982</p></td>
<td><p><a href="../Page/獵頭_(1982年電影).md" title="wikilink">再見江湖</a></p></td>
<td><p>柯佩琳</p></td>
<td><p><a href="../Page/周潤發.md" title="wikilink">周潤發</a>、<a href="../Page/陳欣健.md" title="wikilink">陳欣健</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1983</p></td>
<td><p><a href="../Page/風水二十年.md" title="wikilink">風水二十年</a></p></td>
<td><p>莎莉</p></td>
<td><p><a href="../Page/甄珍.md" title="wikilink">甄珍</a>、<a href="../Page/胡慧中.md" title="wikilink">胡慧中</a>、<a href="../Page/秦漢_(演員).md" title="wikilink">秦漢</a>、<a href="../Page/柯俊雄.md" title="wikilink">柯俊雄</a>、<a href="../Page/陳觀泰.md" title="wikilink">陳觀泰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984</p></td>
<td><p><a href="../Page/青蛙王子_(電影).md" title="wikilink">青蛙王子</a></p></td>
<td><p>雲飄飄</p></td>
<td><p><a href="../Page/鍾鎮濤.md" title="wikilink">鍾鎮濤</a>、<a href="../Page/鍾楚紅.md" title="wikilink">鍾楚紅</a>、<a href="../Page/張曼玉.md" title="wikilink">張曼玉</a>、<a href="../Page/陳百祥.md" title="wikilink">陳百祥</a>、<a href="../Page/曹查理.md" title="wikilink">曹查理</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1984</p></td>
<td><p><a href="../Page/遊俠情.md" title="wikilink">遊俠情</a></p></td>
<td><p>沐婉儿</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1985</p></td>
<td><p><a href="../Page/夏日福星.md" title="wikilink">夏日福星</a></p></td>
<td><p>王依青</p></td>
<td><p><a href="../Page/洪金寶.md" title="wikilink">洪金寶</a>、<a href="../Page/胡慧中.md" title="wikilink">胡慧中</a>、<a href="../Page/岑建勳.md" title="wikilink">岑建勳</a>、<a href="../Page/曾志偉.md" title="wikilink">曾志偉</a>、<a href="../Page/苗喬偉.md" title="wikilink">苗喬偉</a><br />
<a href="../Page/馮淬帆.md" title="wikilink">馮淬帆</a>、<a href="../Page/吳耀漢.md" title="wikilink">吳耀漢</a>、<a href="../Page/戚美珍.md" title="wikilink">戚美珍</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1986</p></td>
<td><p><a href="../Page/富貴列車_(電影).md" title="wikilink">富貴列車</a></p></td>
<td><p>阿芝</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1987</p></td>
<td><p><a href="../Page/龍兄虎弟_(1987年電影).md" title="wikilink">龍兄虎弟</a></p></td>
<td><p>羅拉</p></td>
<td><p><a href="../Page/成龍.md" title="wikilink">成龍</a>、<a href="../Page/譚詠麟.md" title="wikilink">譚詠麟</a>、<a href="../Page/蘿拉芳華.md" title="wikilink">蘿拉芳華</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987</p></td>
<td><p><a href="../Page/A計劃續集.md" title="wikilink">A計劃續集</a></p></td>
<td><p>白影紅</p></td>
<td><p><a href="../Page/成龍.md" title="wikilink">成龍</a>、<a href="../Page/呂良偉.md" title="wikilink">呂良偉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p><a href="../Page/群鶯亂舞.md" title="wikilink">群鶯亂舞</a></p></td>
<td><p>陸千千</p></td>
<td><p><a href="../Page/鄭少秋.md" title="wikilink">鄭少秋</a>、<a href="../Page/利智.md" title="wikilink">利智</a>、<a href="../Page/王小鳳_(演員).md" title="wikilink">王小鳳</a>、<a href="../Page/秦沛.md" title="wikilink">秦沛</a>、<a href="../Page/劉嘉玲.md" title="wikilink">劉嘉玲</a>、<a href="../Page/鄭丹瑞.md" title="wikilink">鄭丹瑞</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1988</p></td>
<td><p><a href="../Page/群龍奪寶.md" title="wikilink">群龍奪寶</a></p></td>
<td><p>老狐狸長女</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/泰迪羅賓.md" title="wikilink">泰迪羅賓</a>、<a href="../Page/林憶蓮.md" title="wikilink">林憶蓮</a>、<a href="../Page/徐少強.md" title="wikilink">徐少強</a></p></td>
<td><p>台譯：獵犬神鎗老狐狸</p></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p><a href="../Page/血衣天使.md" title="wikilink">血衣天使</a></p></td>
<td><p>李娟娟</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1988</p></td>
<td><p><a href="../Page/三人世界.md" title="wikilink">三人世界</a></p></td>
<td><p>蘇碧琪</p></td>
<td><p><a href="../Page/林子祥.md" title="wikilink">林子祥</a>、<a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>、<a href="../Page/周慧敏.md" title="wikilink">周慧敏</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p><a href="../Page/最佳損友闖情關.md" title="wikilink">最佳損友闖情關</a></p></td>
<td><p>霍小蔻</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/邱淑貞.md" title="wikilink">邱淑貞</a>、<a href="../Page/陳百祥.md" title="wikilink">陳百祥</a>、<a href="../Page/馮淬帆.md" title="wikilink">馮淬帆</a>、<a href="../Page/曹查理.md" title="wikilink">曹查理</a>、<a href="../Page/吳啟華.md" title="wikilink">吳啟華</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/傲氣雄鷹.md" title="wikilink">傲氣雄鷹</a></p></td>
<td><p>Jennifer</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/苗僑偉.md" title="wikilink">苗僑偉</a>、<a href="../Page/吳鎮宇.md" title="wikilink">吳鎮宇</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1989</p></td>
<td><p><a href="../Page/鬼媾人.md" title="wikilink">鬼媾人</a></p></td>
<td><p>冰琪</p></td>
<td><p><a href="../Page/王晶_(導演).md" title="wikilink">王晶</a>、<a href="../Page/夏文汐.md" title="wikilink">夏文汐</a>、<a href="../Page/莫少聰.md" title="wikilink">莫少聰</a></p></td>
<td><p>台譯：<a href="../Page/最佳損友闖鬼屋.md" title="wikilink">最佳損友闖鬼屋</a></p></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/至尊無上.md" title="wikilink">至尊無上</a></p></td>
<td><p>BoBo</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/譚詠麟.md" title="wikilink">譚詠麟</a>、<a href="../Page/陳玉蓮.md" title="wikilink">陳玉蓮</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1989</p></td>
<td><p><a href="../Page/瀟灑先生.md" title="wikilink">瀟灑先生</a></p></td>
<td><p>Mona</p></td>
<td><p><a href="../Page/鄭則士.md" title="wikilink">鄭則士</a>、<a href="../Page/張國強_(香港).md" title="wikilink">張國強</a>、<a href="../Page/柏安妮.md" title="wikilink">柏安妮</a>、<a href="../Page/戚美珍.md" title="wikilink">戚美珍</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/專釣大鱷.md" title="wikilink">專釣大鱷</a></p></td>
<td><p>阿全之妻</p></td>
<td><p><a href="../Page/萬梓良.md" title="wikilink">萬梓良</a></p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p>1989</p></td>
<td><p><a href="../Page/說謊的女人.md" title="wikilink">說謊的女人</a></p></td>
<td><p>夏安</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/我愛唐人街.md" title="wikilink">我愛唐人街</a></p></td>
<td><p>Michelle</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p><a href="../Page/洗黑錢.md" title="wikilink">洗黑錢</a></p></td>
<td><p>张文慧</p></td>
<td><p><a href="../Page/甄子丹.md" title="wikilink">甄子丹</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td><p><a href="../Page/一咬OK.md" title="wikilink">一咬OK</a></p></td>
<td><p>馮安娜</p></td>
<td><p><a href="../Page/林子祥.md" title="wikilink">林子祥</a>、<a href="../Page/徐少強.md" title="wikilink">徐少強</a>、<a href="../Page/鄭柏林.md" title="wikilink">鄭柏林</a></p></td>
<td><p>台譯：一咬兩百年</p></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p><a href="../Page/聖戰風雲.md" title="wikilink">聖戰風雲</a></p></td>
<td><p>Annie</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td><p><a href="../Page/小男人週記_II_錯在新宿.md" title="wikilink">小男人週記 II 錯在新宿</a></p></td>
<td><p>溫敏儀</p></td>
<td><p><a href="../Page/鄭丹瑞.md" title="wikilink">鄭丹瑞</a>、<a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>、<a href="../Page/文雋.md" title="wikilink">文雋和</a><a href="../Page/黎彼得.md" title="wikilink">黎彼得</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p><a href="../Page/喋血風雲.md" title="wikilink">喋血風雲</a></p></td>
<td><p>祖儿</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td><p><a href="../Page/我老婆唔係人.md" title="wikilink">我老婆唔係人</a></p></td>
<td><p>郭嘉嘉</p></td>
<td><p><a href="../Page/梁家輝.md" title="wikilink">梁家輝</a>、<a href="../Page/黎彼得.md" title="wikilink">黎彼得</a>、<a href="../Page/胡楓.md" title="wikilink">胡楓</a>、<a href="../Page/陳雅倫.md" title="wikilink">陳雅倫</a>、<a href="../Page/周文健.md" title="wikilink">周文健</a></p></td>
<td><p>台譯：人鬼情未了</p></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p><a href="../Page/婚姻勿語.md" title="wikilink">婚姻勿語</a></p></td>
<td><p>珍妮/Janice</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td><p><a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a></p></td>
<td><p>十三姨</p></td>
<td><p><a href="../Page/李連杰.md" title="wikilink">李連杰</a>、<a href="../Page/莫少聰.md" title="wikilink">莫少聰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td><p><a href="../Page/整蠱專家.md" title="wikilink">整蠱專家</a></p></td>
<td><p>程樂兒</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/周星馳.md" title="wikilink">周星馳</a>、<a href="../Page/邱淑貞.md" title="wikilink">邱淑貞</a>、<a href="../Page/吳孟達.md" title="wikilink">吳孟達</a>、<a href="../Page/鮑漢琳.md" title="wikilink">鮑漢琳</a>、<a href="../Page/李子雄.md" title="wikilink">李子雄</a></p></td>
<td><p>台譯：整人專家<br />
角色台譯：程樂樂</p></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td><p><a href="../Page/神探馬如龍.md" title="wikilink">神探馬如龍</a></p></td>
<td><p>Julia</p></td>
<td><p><a href="../Page/鄭丹瑞.md" title="wikilink">鄭丹瑞</a>、<a href="../Page/劉松仁.md" title="wikilink">劉松仁</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td><p><a href="../Page/豪門夜宴_(1991年電影).md" title="wikilink">豪門夜宴</a></p></td>
<td><p>阿芝</p></td>
<td><p><a href="../Page/曾志偉.md" title="wikilink">曾志偉</a>、<a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>、<a href="../Page/吳耀漢.md" title="wikilink">吳耀漢</a>、<a href="../Page/梁家輝.md" title="wikilink">梁家輝</a>、<a href="../Page/林子祥.md" title="wikilink">林子祥</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p><a href="../Page/一刀傾城.md" title="wikilink">一刀傾城</a></p></td>
<td><p>顾夕/奕亲王妃</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/舞男情未了.md" title="wikilink">舞男情未了</a></p></td>
<td><p>陳卓玲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p><a href="../Page/黃飛鴻之二：男兒當自強.md" title="wikilink">男兒當自強</a></p></td>
<td><p>十三姨</p></td>
<td><p><a href="../Page/李連杰.md" title="wikilink">李連杰</a>、<a href="../Page/莫少聰.md" title="wikilink">莫少聰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/笑傲江湖二之東方不敗.md" title="wikilink">東方不敗</a></p></td>
<td><p>任盈盈</p></td>
<td><p><a href="../Page/李連杰.md" title="wikilink">李連杰</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p><a href="../Page/機Boy小子之真假威龍.md" title="wikilink">機Boy小子之真假威龍</a></p></td>
<td><p>之琳(台译：静音)</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/郭富城.md" title="wikilink">郭富城</a>、<a href="../Page/吳孟達.md" title="wikilink">吳孟達</a>、<a href="../Page/吳君如.md" title="wikilink">吳君如</a>、<a href="../Page/陳慧儀.md" title="wikilink">陳慧儀</a></p></td>
<td><p>台譯：龍神太子</p></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/九二神雕之痴心情長劍.md" title="wikilink">痴心情長劍</a></p></td>
<td><p>玉女寒冰/姑姑</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a></p></td>
<td><p>台譯：新神鵰俠侶2</p></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p><a href="../Page/明月照尖東.md" title="wikilink">明月照尖東</a></p></td>
<td><p>翠兒</p></td>
<td><p><a href="../Page/黎明.md" title="wikilink">黎明</a>、<a href="../Page/张学友.md" title="wikilink">张学友</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/俠聖_(1992年電影).md" title="wikilink">俠聖</a></p></td>
<td><p>Yvonne</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/黃飛鴻之三：獅王爭霸.md" title="wikilink">獅王爭霸</a></p></td>
<td><p>十三姨</p></td>
<td><p><a href="../Page/李連杰.md" title="wikilink">李連杰</a>、<a href="../Page/莫少聰.md" title="wikilink">莫少聰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/花田囍事.md" title="wikilink">花田囍事</a></p></td>
<td><p>白雪仙</p></td>
<td><p><a href="../Page/張國榮.md" title="wikilink">張國榮</a>、<a href="../Page/吳孟達.md" title="wikilink">吳孟達</a>、<a href="../Page/許冠傑.md" title="wikilink">許冠傑</a>、<a href="../Page/盧敏儀.md" title="wikilink">盧敏儀</a>、<a href="../Page/黃霑.md" title="wikilink">黃霑</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/太子傳說.md" title="wikilink">太子傳說</a></p></td>
<td><p>翠兒</p></td>
<td><p><a href="../Page/張學友.md" title="wikilink">張學友</a>、<a href="../Page/劉嘉玲.md" title="wikilink">劉嘉玲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/愛在黑社會的日子.md" title="wikilink">愛在黑社會的日子</a></p></td>
<td><p>郭凱琳</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/新仙鶴神針.md" title="wikilink">新仙鶴神針</a></p></td>
<td><p>藍小蝶</p></td>
<td><p><a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a>、<a href="../Page/梁朝偉.md" title="wikilink">梁朝偉</a>、<a href="../Page/吳啟華.md" title="wikilink">吳啟華</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/殺人者唐斬.md" title="wikilink">殺人者唐斬</a></p></td>
<td><p>阿遥</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/笑八仙.md" title="wikilink">笑八仙</a></p></td>
<td><p>何仙姑</p></td>
<td><p><a href="../Page/鄭少秋.md" title="wikilink">鄭少秋</a>、<a href="../Page/吳君如.md" title="wikilink">吳君如</a>、<a href="../Page/吳孟達.md" title="wikilink">吳孟達</a>、<a href="../Page/溫兆倫.md" title="wikilink">溫兆倫</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/異域之末路英雄.md" title="wikilink">異域之末路英雄</a></p></td>
<td><p>缅甸女</p></td>
<td><p><a href="../Page/梁朝偉.md" title="wikilink">梁朝偉</a>、<a href="../Page/庹宗華.md" title="wikilink">庹宗華</a>、<a href="../Page/吳孟達.md" title="wikilink">吳孟達</a>、<a href="../Page/呂良偉.md" title="wikilink">呂良偉</a>、<a href="../Page/林志穎.md" title="wikilink">林志穎</a>、<a href="../Page/葉全真.md" title="wikilink">葉全真</a>、<a href="../Page/柯俊雄.md" title="wikilink">柯俊雄</a></p></td>
<td><p>台譯：異域2：孤軍</p></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/夏日情未了.md" title="wikilink">夏日情未了</a></p></td>
<td><p>Michelle</p></td>
<td><p><a href="../Page/郭富城.md" title="wikilink">郭富城</a>、<a href="../Page/黃一山.md" title="wikilink">黃一山</a></p></td>
<td><p>台譯：夏日情人夢</p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/西楚霸王_(电影).md" title="wikilink">西楚霸王</a></p></td>
<td><p>虞姬</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p><a href="../Page/楚漢爭霸.md" title="wikilink">楚漢爭霸</a></p></td>
<td><p>虞姬</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/黃飛鴻之五：龍城殲霸.md" title="wikilink">龍城殲霸</a></p></td>
<td><p>十三姨</p></td>
<td><p><a href="../Page/趙文卓.md" title="wikilink">趙文卓</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p><a href="../Page/錦繡前程.md" title="wikilink">錦繡前程</a></p></td>
<td><p>Winnie</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/狂野生死戀.md" title="wikilink">狂野生死戀</a></p></td>
<td><p>徐柳仙</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/大冒險家_(電影).md" title="wikilink">大冒險家</a></p></td>
<td><p>Mona</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/秦沛.md" title="wikilink">秦沛</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/1/2次同床.md" title="wikilink">1/2次同床</a></p></td>
<td><p>李琳琳</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a>、<a href="../Page/李婉華.md" title="wikilink">李婉華</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
<td><p><a href="../Page/冒險王.md" title="wikilink">冒險王</a></p></td>
<td><p>Monica/加美子</p></td>
<td><p><a href="../Page/李連杰.md" title="wikilink">李連杰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p><a href="../Page/黃飛鴻之西域雄獅.md" title="wikilink">西域雄獅</a></p></td>
<td><p>十三姨</p></td>
<td><p><a href="../Page/李連杰.md" title="wikilink">李連杰</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/衛斯理藍血人.md" title="wikilink">衛斯理藍血人</a></p></td>
<td><p>方天涯</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p><a href="../Page/大腕.md" title="wikilink">大腕</a></p></td>
<td><p>Lucy</p></td>
<td><p><a href="../Page/葛优.md" title="wikilink">葛优</a>、<a href="../Page/英达.md" title="wikilink">英达</a>、<a href="../Page/Donald_Sutherland.md" title="wikilink">Donald Sutherland</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/绝世好B.md" title="wikilink">绝世好B</a></p></td>
<td><p>Sabrina/沈小莎</p></td>
<td><p><a href="../Page/刘青云.md" title="wikilink">刘青云</a>、<a href="../Page/古天乐.md" title="wikilink">古天乐</a>、<a href="../Page/梁咏琪.md" title="wikilink">梁咏琪</a>、<a href="../Page/张柏芝.md" title="wikilink">张柏芝</a>、<a href="../Page/刘嘉玲.md" title="wikilink">刘嘉玲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/做頭.md" title="wikilink">做頭</a></p></td>
<td><p>爱妮</p></td>
<td><p><a href="../Page/霍建华.md" title="wikilink">霍建华</a>、<a href="../Page/吴镇宇.md" title="wikilink">吴镇宇</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>《相約到永久》（她唯一的唱片，10首歌曲）</p></td>
<td style="text-align: left;"><p>1994年</p></td>
<td style="text-align: left;"><p>巨石音樂</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>相约到永久（与劉德華对唱）</li>
<li>还是要孤单</li>
<li>流泪</li>
<li>今夜你会很寂寞吗</li>
<li>你的眼睛</li>
<li>真心情人</li>
<li>失心女子</li>
<li>爱你是罗网自投</li>
<li>一见倾心</li>
<li>姻缘歌</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

## MV演出

  - 个人专辑MV ：《相约到永久》和《还是要孤单》
  - [黎明](../Page/黎明.md "wikilink")：《但愿不只是朋友》　
  - [梁家辉](../Page/梁家辉.md "wikilink")：《自己走》
  - [霍建华](../Page/霍建华.md "wikilink")：《你的第一》
  - [Wax](../Page/Wax.md "wikilink")：《关系》　
  - [刘德华](../Page/刘德华.md "wikilink")
      - 《[共你伤心过](../Page/共你伤心过.md "wikilink")》(1989)
      - 《一起走过[红场的日子](../Page/红场.md "wikilink")》(音乐电影/1991)
      - 《潮水》(1996)
      - 《热情的沙漠2000》(1998)
      - 《[心只有你](../Page/心只有你.md "wikilink")》(2000)

## 代言广告

  - 大印象减肥茶
  - [皇朝家私](../Page/皇朝家私.md "wikilink")
  - 黛富妮家纺
  - 米皇羊绒
  - 凯撒服饰
  - 帕兰朵(Plandoo)内衣
  - 樱雪沐浴露
  - 申旺集成卫浴
  - [SK-II](../Page/SK-II.md "wikilink")
  - [爱立信手机](../Page/爱立信.md "wikilink")
  - 澳伦皮鞋
  - [卫康眼镜](../Page/卫康眼镜.md "wikilink")
  - 三角轮胎
  - 格尔森地板
  - 比乐木业
  - Der(德尔)木地板
  - [Marjorie Bertagne](../Page/Marjorie_Bertagne.md "wikilink")
  - [Marie France](../Page/Marie_France.md "wikilink")

## 獎項

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>頒獎典禮</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p><strong>影片</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/第11屆香港電影金像獎.md" title="wikilink">第11屆香港電影金像獎</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《婚姻勿語》</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

<references/>

## 外部連結

  -
  -
  -
  -
  -
  -
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:前麗的電視藝員](../Category/前麗的電視藝員.md "wikilink")
[Category:香港天主教徒](../Category/香港天主教徒.md "wikilink")
[Category:九龍聖德肋撒英文學校校友](../Category/九龍聖德肋撒英文學校校友.md "wikilink")
[Category:瑪利諾修院學校校友](../Category/瑪利諾修院學校校友.md "wikilink")
[Category:德雅中學校友](../Category/德雅中學校友.md "wikilink")
[Category:瀋陽人](../Category/瀋陽人.md "wikilink")
[Category:瓜尔佳氏](../Category/瓜尔佳氏.md "wikilink")
[Category:香港滿族](../Category/香港滿族.md "wikilink")
[S](../Category/關姓.md "wikilink")
[Category:20世纪演员](../Category/20世纪演员.md "wikilink")

1.
2.  《[今夜不設防第](../Page/今夜不設防.md "wikilink")1輯第8集：小泉今日子、關芝琳》第35分鐘，主持[黃霑問](../Page/黃霑.md "wikilink")「哪一旗？鑲白旗還是正白旗」，關之琳回答正白旗人。
3.
4.  [Rosamund Kwan 關之琳
    青春常駐](https://mings.mpweekly.com/life/20150904-11507/1)
5.  [關之琳去年嫁陳泰銘　難忍當祕密人妻喊離](http://www.appledaily.com.tw/realtimenews/article/new/20151104/725200/)
6.  [關之琳宣佈與百億富豪陳泰銘離婚](http://news.wenweipo.com/2015/11/04/IN1511040046.htm)