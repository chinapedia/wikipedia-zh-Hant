[關帝廟.JPG](https://zh.wikipedia.org/wiki/File:關帝廟.JPG "fig:關帝廟.JPG")\]\]
[Interieur_d'une_Pagode,_Pekin.jpg](https://zh.wikipedia.org/wiki/File:Interieur_d'une_Pagode,_Pekin.jpg "fig:Interieur_d'une_Pagode,_Pekin.jpg")
**關帝廟**，又稱**武廟**、**武聖廟**、**文衡廟**、**協天宮**、**恩主公廟**等，是祭祀[中國](../Page/中國.md "wikilink")[三國時代將領](../Page/三国.md "wikilink")[關羽的祠廟](../Page/關羽.md "wikilink")。而**關帝**之稱來自[明朝皇帝授予關羽的](../Page/明朝.md "wikilink")「[關聖帝君](../Page/關聖帝君.md "wikilink")」封號。關羽為中國[佛教](../Page/佛教.md "wikilink")、[道教神明](../Page/道教.md "wikilink")、圣贤中最多祠廟的一位。關帝廟不僅遍佈[中國大陸](../Page/中國大陸.md "wikilink")、[港澳及](../Page/港澳.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，且在[漢字文化圈內各地區包括](../Page/漢字文化圈.md "wikilink")[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[日本](../Page/日本.md "wikilink")、[越南等](../Page/越南.md "wikilink")，以至南洋地區如[馬來西亞](../Page/馬來西亞.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[印尼等地也有供奉](../Page/印尼.md "wikilink")。今日僅在[中國老](../Page/中國.md "wikilink")[北京城裏](../Page/北京城.md "wikilink")，就有一百多座專供或兼供關公的廟宇。每年關公節都會有多間武館到那表現。

## 關帝祠廟之起源

[TAINANTEMPLE.JPG](https://zh.wikipedia.org/wiki/File:TAINANTEMPLE.JPG "fig:TAINANTEMPLE.JPG")

中國古代祭祀的[戰神本是](../Page/戰神.md "wikilink")「[兵主](../Page/兵主.md "wikilink")」[蚩尤](../Page/蚩尤.md "wikilink")。就[公家的祭祀言](../Page/公家.md "wikilink")，[唐初開始便有武廟](../Page/唐朝.md "wikilink")，但[主祀的是](../Page/主祀.md "wikilink")[周朝的政治人物兼將領](../Page/周朝.md "wikilink")[姜子牙](../Page/姜子牙.md "wikilink")，而關羽則為[從祀](../Page/從祀.md "wikilink")。至遲[宋朝末年](../Page/宋朝.md "wikilink")，民間供奉關羽的廟宇已經「郡國州縣、鄉邑間井皆有」\[1\]。[北宋](../Page/北宋.md "wikilink")[宣和四年](../Page/宣和_\(北宋\).md "wikilink")（1122年）重修的[山西阳泉关王庙](../Page/阳泉关王庙.md "wikilink")，被认为是中国现存最早的关王庙\[2\]。[元代朝廷對各種宗教信仰兼收並蓄](../Page/元朝.md "wikilink")，因此民間對關羽的崇信有增無減，元朝皇帝也曾遣使致祭。[明](../Page/明朝.md "wikilink")[清以降](../Page/清朝.md "wikilink")，供奉關羽的廟宇已遍佈中國各地。由于民间传说關羽死后“头在洛阳，身在当阳，魂归故鄉”，因此在[洛阳](../Page/洛阳.md "wikilink")、[当阳及故里](../Page/当阳.md "wikilink")[解州有规模较大的關帝庙](../Page/解州.md "wikilink")，其中[解州關帝庙规模最大](../Page/解州關帝庙.md "wikilink")。

[清代](../Page/清代.md "wikilink")，軍隊對關羽的崇拜達到極盛。不僅在關內各省，在[藩部各地凡有](../Page/藩部.md "wikilink")[八旗](../Page/八旗.md "wikilink")、[綠營駐軍之處](../Page/綠營.md "wikilink")，也普遍修建關帝廟。蒙古[庫倫](../Page/大庫倫.md "wikilink")、[科布多城](../Page/科布多城.md "wikilink")、[乌里雅苏台城](../Page/乌里雅苏台城.md "wikilink")，新疆[伊犁](../Page/伊犁.md "wikilink")、[乌鲁木齐](../Page/乌鲁木齐.md "wikilink")，西藏[拉萨等地都建有關帝廟供军队祭祀](../Page/拉萨.md "wikilink")。在拉萨，關帝信仰与[格薩爾融为一体](../Page/格薩爾.md "wikilink")，因此帕玛日关帝庙又被藏人称为[格薩爾拉康](../Page/格薩爾拉康.md "wikilink")。

因為關羽不僅受到[儒家的崇祀](../Page/儒家.md "wikilink")，同時又受到[道教](../Page/道教.md "wikilink")、[佛家的膜拜](../Page/佛家.md "wikilink")，所以關羽是橫貫[儒](../Page/儒.md "wikilink")、[道](../Page/道.md "wikilink")、[佛三大中國教派的神祇](../Page/佛.md "wikilink")。但其中以儒家的關羽體現更多關羽的本色。

隨著關羽地位變得顯赫，關羽更被尊稱為「武王」、「武聖人」，與[孔子並肩而立](../Page/孔子.md "wikilink")，有[山西夫子之稱](../Page/山西夫子.md "wikilink")。也正因為關羽如此顯赫，除了[軍人](../Page/軍人.md "wikilink")、[警察](../Page/警察.md "wikilink")、武師，奉他為行業神崇拜外，就連[描金業](../Page/描金業.md "wikilink")、[煙業](../Page/煙業.md "wikilink")、[香燭業](../Page/香燭業.md "wikilink")、[教育業](../Page/教育業.md "wikilink")、[命相家等等不相干的行業也推崇關羽](../Page/命相家.md "wikilink")，所以也將他變成武[財神](../Page/財神.md "wikilink")，又是[五文昌之一](../Page/五文昌.md "wikilink")，稱為[文衡聖帝](../Page/文衡聖帝.md "wikilink")、[文衡帝君](../Page/文衡帝君.md "wikilink")。

[扶鸞信仰中](../Page/扶鸞.md "wikilink")，有所謂的「[恩主](../Page/恩主.md "wikilink")」信仰，「恩主」是「對民有恩的主神」，另說是「救世主」的意思。台灣一般所謂的「恩主」共有關羽、[呂洞賓](../Page/呂洞賓.md "wikilink")、[張單](../Page/張單.md "wikilink")、[王善](../Page/王善.md "wikilink")、[岳飛](../Page/岳飛.md "wikilink")。而關羽為「五恩主」之首，所以台灣一般民眾亦稱關羽為「恩主公」，也因此順勢稱關帝廟為「恩主公廟」。

## 從祀

  - [關平](../Page/關平.md "wikilink")（關平[太子](../Page/太子.md "wikilink")）
  - [周倉](../Page/周倉.md "wikilink")（周倉[將軍](../Page/將軍.md "wikilink")）
  - [赤兔馬](../Page/赤兔馬.md "wikilink")

## 各地關帝廟

### 中國大陸

[Tomb_of_Guanyu_at_Luoyang.JPG](https://zh.wikipedia.org/wiki/File:Tomb_of_Guanyu_at_Luoyang.JPG "fig:Tomb_of_Guanyu_at_Luoyang.JPG")
依托關帝古陵建成的廟殿：
河南洛陽[關林](../Page/關林.md "wikilink")，厚葬關羽之首，這裏漢稱“關冢”，明稱“關王廟”，清道光年間始稱“關林”。林內翠柏森森，造像高大，關帝古林高10米，占地250多平方米，是海內外朝拜者心中的聖地。

[當陽關陵位于當陽市城區西](../Page/當陽關陵.md "wikilink")2公里處，是中國歷史上蜀漢名將關羽的陵墓。史載，當陽關陵是埋葬關羽身軀的地方，故當地民間流傳有“頭枕洛陽，身臥當陽”之說。關陵，原稱“大王冢”，墓建于東漢末年。南宋淳熙十年（1183年），襄陽太守王铢在墓前修築祭亭。明代成化三年（1467年）始建廟宇。群體建築則落成于明嘉靖十五年（1536年）。關陵，占地45000平方米。采用中軸對稱式帝陵規制，中軸線上，有八座古代建築物，依次是神道碑亭、“漢室忠良”石牌坊、三元門、馬殿、拜殿、正殿、寢殿、陵基。兩旁有華表、鍾樓、鼓樓、碑廊、齋堂、來止軒、聖像亭、伯子祠、啓聖宮、佛堂、春秋閣等。正殿大門上方有清同治皇帝禦筆“威震華夏”金字匾額壹塊。

[荊州關帝廟雄踞在荊州古城的南紀門內](../Page/荊州關帝廟.md "wikilink")。史料記載：這裏曾是蜀漢大將關羽鎮守荊州的府邸故基，關羽曾在這裏運籌帷幄，總督荊襄九郡諸事十年有余，留下了許多可歌可泣的英雄事迹。關羽死後，這裏成了他後裔世襲江陵的襲地。到了明洪武二十九年（公元1396年），荊州城最早、最大的關廟在這裏建成，後因種種原因，古殿宇毀失殆盡。現在大家看到的關帝廟是1987年，經原江陵縣人民政府及國家旅遊局籌資，按清乾隆縣志載古關廟建築布局圖樣在原關廟遺址上複建的。

[解州關帝廟](../Page/解州關帝廟.md "wikilink")：山西運城市解州鎮常平村，是關羽家鄉，鄉人依祖墳立廟，稱“關王故里”，關帝殿、娘娘殿、聖祖殿簇擁在青煙翠柏之中。從關王故里西行10公里
，有解州關帝廟，因廟貌古樸宏麗，且佔地 200余畝，被譽爲“武廟之祖”。

[东山关帝庙](../Page/东山关帝庙.md "wikilink")：位于中国[福建省](../Page/福建省.md "wikilink")[东山县](../Page/东山县.md "wikilink")[铜陵镇岵嵝山东麓](../Page/铜陵镇.md "wikilink")，又常称为**铜陵关帝庙**、**铜陵武庙**，是[台湾众多关帝庙挂香分灵入台的祖庙](../Page/台湾.md "wikilink")。

### 潮汕

[厂前街关爷宫1.jpg](https://zh.wikipedia.org/wiki/File:厂前街关爷宫1.jpg "fig:厂前街关爷宫1.jpg")[達濠廠前街關爺宫](../Page/達濠.md "wikilink")
2009.7.29\]\]

在[潮汕](../Page/潮汕.md "wikilink")，關帝廟俗稱作關爺宮。關羽是忠義勇武的象徵，同[三山國王](../Page/三山國王.md "wikilink")、[雙忠一樣具有守護一方平安的功能](../Page/雙忠.md "wikilink")。

### 香港

在[香港](../Page/香港.md "wikilink")，雖然[警察與](../Page/警察.md "wikilink")[三合會成員是對立的](../Page/三合會.md "wikilink")，但都推崇、尊敬關羽，稱他為「關二爺」、「關二哥」、「關公」、「關帝」等。他們相信，關公是會保衛盡忠盡義之人，所以雙方都不認為存在信仰上的衝突。

而香港也建有不少的祠廟供奉關羽，以香港[上環](../Page/上環.md "wikilink")[-{荷李活道}-的](../Page/荷李活道.md "wikilink")[文武廟最為有名](../Page/文武廟.md "wikilink")。

其他關帝廟：

  - [旺角雨傘運動關帝廟](../Page/旺角關帝廟.md "wikilink")
  - [沙頭角](../Page/沙頭角.md "wikilink")[南涌](../Page/南涌.md "wikilink")[協天宮](../Page/南涌.md "wikilink")
  - [深水埗武帝廟](../Page/深水埗武帝廟.md "wikilink")
  - [蓮麻坑](../Page/蓮麻坑.md "wikilink")[關帝宮](../Page/關帝宮.md "wikilink")
  - [荔枝窩](../Page/荔枝窩.md "wikilink")[協天宮](../Page/荔枝窩協天宮.md "wikilink")
  - [元朗](../Page/元朗.md "wikilink")[輞井圍玄關帝廟](../Page/輞井圍.md "wikilink")
  - 元朗舊墟[玄關二帝廟](../Page/玄關二帝廟.md "wikilink")
  - [屯門](../Page/屯門.md "wikilink")[掃管笏](../Page/掃管笏.md "wikilink")[關帝廟](../Page/掃管笏關帝廟.md "wikilink")
  - [大埔](../Page/大埔_\(香港\).md "wikilink")[樟樹灘](../Page/樟樹灘村.md "wikilink")[協天宮古廟](../Page/樟樹灘村協天宮.md "wikilink")
  - [西貢關帝古廟](../Page/西貢墟天后古廟及協天宮.md "wikilink")
  - [荃灣關帝廟](../Page/荃灣關帝廟.md "wikilink")
  - [大澳關帝廟](../Page/大澳關帝廟.md "wikilink")
  - [長洲](../Page/長洲_\(香港\).md "wikilink")[關公忠義亭](../Page/關公忠義亭.md "wikilink")

### 澳門

早期[澳門的商人在](../Page/澳門.md "wikilink")[三街會館奉祀關帝](../Page/三街會館.md "wikilink")。

### 臺灣

[行天宮1.jpg](https://zh.wikipedia.org/wiki/File:行天宮1.jpg "fig:行天宮1.jpg")\]\]
在[台灣](../Page/台灣.md "wikilink")，祭祀關羽的廟宇也相當普遍，除了以往官定的[武廟](../Page/武廟.md "wikilink")、移民或鄉里所立的關帝廟、私人所立的神壇將[關羽作為主祀外](../Page/關羽.md "wikilink")，也有稱為恩主公廟的大型關帝廟，如[台北市](../Page/台北市.md "wikilink")[行天宮等](../Page/行天宮.md "wikilink")。

所謂的「[恩主](../Page/恩主.md "wikilink")」是[扶鸞信仰的名詞](../Page/扶鸞.md "wikilink")，也就是「救世主」的意思。台灣所謂的恩主神祇一般有關羽，[呂洞賓](../Page/呂洞賓.md "wikilink")，[張單](../Page/張單.md "wikilink")，[王善](../Page/王善.md "wikilink")，[岳飛等神](../Page/岳飛.md "wikilink")。因為五恩主以關雲長為首，因此台灣一般民眾亦直稱關雲長為恩主公，也因此順勢稱關帝廟為「恩主公廟」。

臺灣許多地名源起關帝廟，現存者例如[關廟區](../Page/關廟區.md "wikilink")（臺南市）等。

著名廟宇：

  - [台南白河區](../Page/台南白河區.md "wikilink")[馬稠後關帝廟](../Page/馬稠後關帝廟.md "wikilink")
  - [台北行天宮](../Page/台北行天宮.md "wikilink")
  - [北投行天宮](../Page/北投行天宮.md "wikilink")
  - [淡水](../Page/淡水.md "wikilink")[行天武聖宮](../Page/行天武聖宮.md "wikilink")
  - [三峽行修宮](../Page/三峽行修宮.md "wikilink")
  - [大溪普濟堂](../Page/大溪普濟堂.md "wikilink")
  - [頭城協天宮](../Page/頭城協天宮.md "wikilink")
  - [礁溪](../Page/礁溪.md "wikilink")[協天廟](../Page/協天廟.md "wikilink")
  - [羅東](../Page/羅東.md "wikilink")[協天宮](../Page/協天宮.md "wikilink")
  - [新莊武聖廟](../Page/新莊武聖廟.md "wikilink")
  - [桃園明倫三聖宮](../Page/桃園明倫三聖宮.md "wikilink")
  - [桃園威天宮](../Page/桃園威天宮.md "wikilink")
  - [桃園關帝廟](../Page/桃園關帝廟.md "wikilink")
  - [龍潭南天宮](../Page/龍潭南天宮.md "wikilink")
  - [新竹關帝廟](../Page/新竹關帝廟.md "wikilink")
  - [古奇峰普天宮](../Page/古奇峰普天宮.md "wikilink")
  - [苗栗玉清宮](../Page/苗栗玉清宮.md "wikilink")
  - [白沙屯](../Page/白沙屯.md "wikilink")[東龍宮](../Page/東龍宮.md "wikilink")
  - [日月潭文武廟](../Page/日月潭文武廟.md "wikilink")
  - [臺中南天宮](../Page/臺中南天宮.md "wikilink")
  - [台中醒修宮](../Page/台中醒修宮.md "wikilink")
  - [彰化關帝廟](../Page/彰化關帝廟.md "wikilink")
  - [鹿港南靖宮](../Page/鹿港南靖宮.md "wikilink")
  - [四湖參天宮](../Page/四湖參天宮.md "wikilink")
  - [四湖](../Page/四湖鄉.md "wikilink")[保長湖保安宮](../Page/保長湖保安宮.md "wikilink")
  - [斗六南聖宮](../Page/斗六南聖宮.md "wikilink")
  - [嘉義](../Page/嘉義.md "wikilink")[鎮天宮](../Page/鎮天宮.md "wikilink")
  - [嘉義](../Page/嘉義.md "wikilink")[南開宮](../Page/南開宮.md "wikilink")
  - [嘉義](../Page/嘉義.md "wikilink")[嘉邑鎮天宮](../Page/嘉邑鎮天宮.md "wikilink")
  - [大林](../Page/大林.md "wikilink")[聖賢宮](../Page/聖賢宮.md "wikilink")
  - [水上粗溪關帝廟](../Page/水上粗溪關帝廟.md "wikilink")
  - [太保](../Page/太保.md "wikilink")[慧明社醒善堂](../Page/慧明社醒善堂.md "wikilink")
  - [笨港](../Page/笨港.md "wikilink")[協天宮](../Page/協天宮.md "wikilink")
  - [朴子](../Page/朴子.md "wikilink")[春秋武廟](../Page/春秋武廟.md "wikilink")
  - [關廟](../Page/關廟區.md "wikilink")[山西宮](../Page/山西宮.md "wikilink")
  - [鹽水武廟](../Page/鹽水武廟.md "wikilink")
  - [開基武廟](../Page/開基武廟原正殿.md "wikilink")
  - [祀典武廟](../Page/祀典武廟.md "wikilink")
  - [台南關帝殿](../Page/台南關帝殿.md "wikilink")
  - [台南八吉境關帝廳](../Page/台南八吉境關帝廳.md "wikilink")
  - [麻豆文衡殿](../Page/麻豆文衡殿.md "wikilink")
  - [後甲關帝殿](../Page/後甲關帝殿.md "wikilink")
  - [漚汪](../Page/將軍區.md "wikilink")[文衡殿](../Page/文衡殿.md "wikilink")
  - [高雄梓官同安宮](../Page/高雄梓官同安宮.md "wikilink")
  - [東照山關帝廟](../Page/東照山關帝廟.md "wikilink")
  - [高雄關帝廟](../Page/高雄關帝廟.md "wikilink")
  - [高雄市](../Page/高雄市.md "wikilink")[五塊厝關帝殿](../Page/五塊厝關帝殿.md "wikilink")
  - [高雄大義宮](../Page/高雄大義宮.md "wikilink")
  - [高雄](../Page/高雄市.md "wikilink")[鳳山](../Page/鳳山區.md "wikilink")[赤山文衡殿](../Page/赤山文衡殿.md "wikilink")
  - [內惟](../Page/內惟.md "wikilink")[啟安堂](../Page/啟安堂.md "wikilink")
  - [左營](../Page/左營.md "wikilink")[啟明堂](../Page/啟明堂.md "wikilink")
  - [高雄意誠堂](../Page/高雄意誠堂.md "wikilink")
  - [鹽埕](../Page/鹽埕區.md "wikilink")[文武聖殿](../Page/文武聖殿.md "wikilink")
  - [前鎮鎮南宮](../Page/前鎮鎮南宮.md "wikilink")
  - [高雄鼎金聖何宮](../Page/高雄鼎金聖何宮.md "wikilink")
  - [高吉武聖殿](../Page/高吉武聖殿.md "wikilink")
  - [高雄明聖殿](../Page/高雄明聖殿.md "wikilink")
  - [屏東聖帝廟](../Page/屏東聖帝廟.md "wikilink")
  - [車城統埔鎮安宮](../Page/車城鄉.md "wikilink")
  - [臺東](../Page/臺東縣.md "wikilink")[鹿野協天宮](../Page/鹿野協天宮.md "wikilink")
  - [東河](../Page/東河.md "wikilink")[協天宮](../Page/協天宮.md "wikilink")
  - [卑南文衡殿](../Page/卑南文衡殿.md "wikilink")
  - [花蓮聖天宮](../Page/花蓮聖天宮.md "wikilink")
  - [花蓮](../Page/花蓮縣.md "wikilink")[玉里協天宮](../Page/玉里協天宮.md "wikilink")
  - [澎湖縣](../Page/澎湖縣.md "wikilink")[西嶼](../Page/西嶼.md "wikilink")[大義宮](../Page/大義宮.md "wikilink")
  - [烏崁靖海宮](../Page/烏崁靖海宮.md "wikilink")

### 日本

[Kanteibyo_Kobe02s5s3200.jpg](https://zh.wikipedia.org/wiki/File:Kanteibyo_Kobe02s5s3200.jpg "fig:Kanteibyo_Kobe02s5s3200.jpg")關帝廟\]\]
而據[小松田直](../Page/小松田直.md "wikilink")《圖解世界史》，在[日本也有為數眾多的關帝廟](../Page/日本.md "wikilink")，奉關羽為學問與生意之神。

  - [神户](../Page/神户市.md "wikilink")[關帝廟](../Page/神户關帝廟.md "wikilink")
  - [横滨關帝廟](../Page/横滨市.md "wikilink")
  - [长崎關帝廟](../Page/长崎市.md "wikilink")
  - [函館關帝廟](../Page/函館市.md "wikilink")
  - [和歌山關帝廟](../Page/和歌山县.md "wikilink")
  - [京都關帝廟](../Page/京都府.md "wikilink")
  - [大阪關帝廟](../Page/大阪府.md "wikilink")
  - [東京關帝廟](../Page/東京都.md "wikilink")
  - [久米村關帝廟](../Page/久米村.md "wikilink")

### 朝鮮半島

  - [首爾](../Page/首爾.md "wikilink")[東廟](../Page/東廟.md "wikilink")

### 漢字文化圈外

#### 西藏

  - [拉薩](../Page/拉薩.md "wikilink")[格薩爾拉康](../Page/格薩爾拉康.md "wikilink")（帕玛日关帝庙）

#### 新疆

  - [伊犁惠遠城關帝廟](../Page/伊犁.md "wikilink")

#### 蒙古

  - [庫倫關帝廟](../Page/大庫倫.md "wikilink")
  - [科布多城關帝廟](../Page/科布多城.md "wikilink")

#### 柬埔寨

  - [柬埔寨潮州会馆](../Page/柬埔寨潮州会馆.md "wikilink")[協天大帝庙](../Page/協天大帝.md "wikilink")（[关帝庙](../Page/关帝庙.md "wikilink")）

<!-- end list -->

  - [柬埔寨福建会馆](../Page/柬埔寨福建会馆.md "wikilink")[关帝庙](../Page/关帝庙.md "wikilink")

<!-- end list -->

  - [柬埔寨](../Page/柬埔寨.md "wikilink")[关平庙](../Page/关平庙.md "wikilink")

#### 越南

  - [義安會館](../Page/義安會館.md "wikilink")

#### 马来西亚

  - [关丹](../Page/关丹.md "wikilink")[關帝廟](../Page/關帝廟.md "wikilink")
  - [吉隆坡](../Page/吉隆坡.md "wikilink")[關帝廟](../Page/關帝廟.md "wikilink")

#### 新加坡

  - [新加坡](../Page/新加坡.md "wikilink")[忠邦联合宫 -
    关帝庙](../Page/忠邦联合宫_-_关帝庙.md "wikilink")  

### 其他

[洛杉矶](../Page/洛杉矶.md "wikilink")[中国城潮州会馆关帝庙](../Page/唐人街.md "wikilink")\[3\]

[加拿大](../Page/加拿大.md "wikilink")[本拿比天金堂](../Page/本那比.md "wikilink")\[4\]

## 參考文獻

## 外部連結

  - [關帝閣](http://www.lyguandige.com/)

  - [關公網](http://www.edu-100.com/)

  -
[\*](../Category/關羽.md "wikilink") [關帝廟](../Category/關帝廟.md "wikilink")

1.  [郝經](../Page/郝經.md "wikilink")1223年-1275年《陵川文集》
2.
3.
4.