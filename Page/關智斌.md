**關智斌**（**Kenny Kwan Chi
Pun**，），香港歌手及演員，其男子歌唱組合[Boy'z成員的身份最為人知](../Page/Boy'z.md "wikilink")。家中有兩姊及一妹。曾就讀[聖公會諸聖小學和](../Page/聖公會諸聖小學.md "wikilink")[九龍鄧鏡波學校](../Page/九龍鄧鏡波學校.md "wikilink")，後來於[加拿大留學](../Page/加拿大.md "wikilink")，並於[香港專業教育學院修讀](../Page/香港專業教育學院柴灣分校.md "wikilink")[電子工程系](../Page/電子工程.md "wikilink")。

關智斌因參與[香港電台電視劇](../Page/香港電台.md "wikilink")《[Y2K+01](../Page/Y2K+01.md "wikilink")》而獲得[英皇娛樂賞識](../Page/英皇娛樂.md "wikilink")，簽約成為旗下歌手。2003年和[張致恆組成組合](../Page/張致恆.md "wikilink")[Boy'z出道](../Page/Boy'z.md "wikilink")。2005年起獨立發展，至今共推出過五張個人專輯。2011年，與[張致恆重組](../Page/張致恆.md "wikilink")[Boy'z](../Page/Boy'z.md "wikilink")。2013年，主演中國大陸古裝神話劇《[追魚傳奇](../Page/追魚傳奇.md "wikilink")》，人氣迅速上升。\[1\]

## 音樂作品

### 唱片

#### Boy'z

  - 2003年：
      - 《LaLa世界》
      - 《LaLa世界》（第二版）
      - 《一起喝采》
      - 《A Year to Remember》（AVEP）

<!-- end list -->

  - 2004年：
      - 《A Year To Remember》（第三版）
      - 《Boy'zone》
      - 《Boy'zone》（第二版）
      - 《Boy'z Can Cook》

<!-- end list -->

  - 2011年：《Ready to Go 新曲＋精選》
  - 2018年：《大男孩 新曲＋精選》

#### 個人唱片

  - 2005年：
      - 《[Oncoming](../Page/Oncoming.md "wikilink")》
      - 《Oncoming》（第二版）
      - 《[Musick](../Page/Musick.md "wikilink")》

<!-- end list -->

  - 2006年：
      - 《[尋找美惠](../Page/尋找美惠.md "wikilink")》
      - 《尋找美惠》（第二版）

<!-- end list -->

  - 2007年：《[In Progress](../Page/In_Progress.md "wikilink")》

<!-- end list -->

  - 2008年：《[Kenny's
    Essentials](../Page/Kenny's_Essentials.md "wikilink") 新曲＋精選》

<!-- end list -->

  - 2016年：《[角色](../Page/角色_\(專輯\).md "wikilink") 新曲＋精選》

#### 合輯

  -   - 2004年《Joy to the World Christmas Hits》
      - 2005年《星Mobile超時空接觸演唱會》
      - 2005年《八星報囍賀賀囍》
      - 2006年《愛你想妳2030》
      - 2006年《[夢幻組合](../Page/夢幻組合.md "wikilink")》（英皇群星）
      - 2006年《愛 回憶 108》
      - 2007年《流行音樂潮拜》
      - 2007年《EEG音樂甜點精選卡拉OK》

## 派台歌曲成績

| **派台歌曲成績**                                                     |
| -------------------------------------------------------------- |
| 唱片                                                             |
| **2005年**                                                      |
| [Oncoming](../Page/Oncoming.md "wikilink")                     |
| [Oncoming](../Page/Oncoming.md "wikilink")                     |
| [Oncoming](../Page/Oncoming.md "wikilink")                     |
| [Oncoming](../Page/Oncoming.md "wikilink")                     |
| 芝See菇Bi Family 《三十六計》                                          |
| [Musick](../Page/Musick.md "wikilink")                         |
| **2006年**                                                      |
| [Musick](../Page/Musick.md "wikilink")                         |
| [Musick](../Page/Musick.md "wikilink")                         |
| [尋找美惠](../Page/尋找美惠.md "wikilink")                             |
| **2007年**                                                      |
| [In Progress](../Page/In_Progress.md "wikilink")               |
| [In Progress](../Page/In_Progress.md "wikilink")               |
| [In Progress](../Page/In_Progress.md "wikilink")               |
| **2008年**                                                      |
| [Kenny's Essentials](../Page/Kenny's_Essentials.md "wikilink") |
| [Kenny's Essentials](../Page/Kenny's_Essentials.md "wikilink") |
| **2016年**                                                      |
| [角色](../Page/角色_\(專輯\).md "wikilink")                          |
| **2017年**                                                      |
| [愛∙回憶108 新曲+精選](../Page/愛∙回憶108_新曲+精選.md "wikilink")           |
|                                                                |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **1**       |

### 合唱歌曲

  - 男仔歌（與[苦榮合唱](../Page/苦榮.md "wikilink")，收錄於《芝See菇Bi Family 《三十六計》）
  - 愛神之箭（與[梁洛施合唱](../Page/梁洛施.md "wikilink")，收錄於《[Isabella](../Page/Isabella.md "wikilink")》）
  - 良心發現（與[鍾欣桐合唱](../Page/鍾欣桐.md "wikilink")，收錄於《[Musick](../Page/Musick.md "wikilink")》）
  - 非愛不可（與鍾欣桐合唱，電視劇《[牛郎織女](../Page/牛郎織女_\(電視劇\).md "wikilink")》主題曲）
  - 光與影（與梁洛施合唱，收錄於《[尋找美惠](../Page/尋找美惠.md "wikilink")》）
  - 3+1=1（與[Sun Boy'z合唱](../Page/Sun_Boy'z.md "wikilink")，收錄於《[All for
    1](../Page/All_for_1.md "wikilink")》）
  - 小學館（與[吳浩康](../Page/吳浩康.md "wikilink")、[泳兒及](../Page/泳兒.md "wikilink")[鍾舒漫合唱](../Page/鍾舒漫.md "wikilink")，收錄於《[In
    Progress](../Page/In_Progress.md "wikilink")》，內地電台南方生活廣播「流行南方粵語兒歌大匯」冠軍歌曲（上榜中）
  - 邁向夢想的天空（與[吳浩康及](../Page/吳浩康.md "wikilink")[吳卓羲合唱](../Page/吳卓羲.md "wikilink")，電視劇《[學警出更](../Page/學警出更.md "wikilink")》主題曲）

### 音樂創作

  - 《[Oncoming](../Page/Oncoming.md "wikilink")》
      - 無聲訊號（作曲、作詞）
  - 《[Musick](../Page/Musick.md "wikilink")》
      - 環遊四方（作詞）
  - 《[尋找美惠](../Page/尋找美惠.md "wikilink")》
      - 鎢絲燈（作詞）
      - 助聽器（作詞）
      - 成人禮（作曲、作詞）
  - 《[In Progress](../Page/In_Progress.md "wikilink")》
      - 戰場上的羅密歐（作詞）
      - 世界花園（作詞）
      - 勇氣情歌（作詞）

## 演出作品

### 電影

  - 2003年：
      - 《[古宅心慌慌](../Page/古宅心慌慌.md "wikilink")》飾 丁武
      - 《[海底奇兵](../Page/海底奇兵.md "wikilink")》（配音）

<!-- end list -->

  - 2004年：
      - 《[鬼馬狂想曲](../Page/鬼馬狂想曲.md "wikilink")》飾 魔法學校接線生
      - 《[這個阿爸真爆炸](../Page/這個阿爸真爆炸.md "wikilink")》飾 阿橙
      - 《[愛作戰](../Page/愛作戰.md "wikilink")》飾 志波
      - 《[新警察故事](../Page/新警察故事.md "wikilink")》飾 紅毛（客串）
      - 《[大無謂](../Page/大無謂.md "wikilink")》飾 細兜

<!-- end list -->

  - 2005年：
      - 《[蟲不知](../Page/蟲不知.md "wikilink")》飾 小智
      - 《[情癲大聖](../Page/情癲大聖.md "wikilink")》飾 豬八戒

<!-- end list -->

  - 2006年：
      - 《[時光·倒流的話](../Page/時光·倒流的話.md "wikilink")》飾
        一輝（2009年10月17日21：00于香港衛視電影台上映）
      - 《[荒村客棧](../Page/荒村客棧.md "wikilink")》飾—孟凡（2008年8月上映）

<!-- end list -->

  - 2007年
      - 《[破事兒](../Page/破事兒.md "wikilink")》飾 阿池
      - 《[閃閃的紅星之紅星小勇士](../Page/閃閃的紅星之紅星小勇士.md "wikilink")》（配音）

<!-- end list -->

  - 2008年：
      - 《[黑道之無悔今生](../Page/黑道之無悔今生.md "wikilink")》 飾 阿宏
      - 《[猩戰前傳](../Page/猩戰前傳.md "wikilink")》（配音）

<!-- end list -->

  - 2012年:
      - 《[半夜不要照鏡子](../Page/半夜不要照鏡子.md "wikilink")》 飾 江桓

<!-- end list -->

  - 2014年:
      - 《[救火英雄](../Page/救火英雄.md "wikilink")》 飾 小僑

<!-- end list -->

  - 2017年:開拍中
      - 《[血戰銅鑼灣3](../Page/血戰銅鑼灣3.md "wikilink")》(網絡電影)飾 孝天

### 電視劇

  - 2001年:
      - [香港電台](../Page/香港電台.md "wikilink")《[Y2K+01](../Page/Y2K+01.md "wikilink")》飾
        阿斌
  - 2003年:
      - [now電視網劇](../Page/now電視.md "wikilink")《一起喝采》飾 Kenny
      - 《[功夫足球](../Page/功夫足球.md "wikilink")》飾 花弄月
      - [無綫電視](../Page/無綫電視.md "wikilink")《[當四葉草碰上劍尖時](../Page/當四葉草碰上劍尖時.md "wikilink")》飾
        高嵐（客串）
  - 2004年:
      - 《[伙頭智多星](../Page/伙頭智多星.md "wikilink")》飾 易小東
      - [無綫電視](../Page/無綫電視.md "wikilink")《[赤沙印記@四葉草.2](../Page/赤沙印記@四葉草.2.md "wikilink")》飾
        錢國偉、陳鳳珊（客串）
  - 2005年:
      - 《[中華小當家](../Page/中華小當家.md "wikilink")》飾 狼哥
  - 2006年:
      - 《[過渡青春](../Page/過渡青春.md "wikilink")》飾 Kenny（客串）
  - 2007年:
      - 《[學警出更](../Page/學警出更.md "wikilink")》飾 袁家富
  - 2008年:
      - 《[大案組](../Page/大案組.md "wikilink")》飾 崔偉業
      - 《[青春舞台](../Page/青春舞台.md "wikilink")》飾 阿波
  - 2009年:
      - 《[蓮花雨](../Page/蓮花雨.md "wikilink")》飾 明家安
  - 2011年:
      - 《[秦香蓮](../Page/秦香蓮_\(電視劇\).md "wikilink")》飾
        [趙禎](../Page/趙禎.md "wikilink")
      - 《[剑侠情缘之藏剑山庄](../Page/剑侠情缘之藏剑山庄.md "wikilink")》飾 趙無忌
  - 2012年:
      - 《[成人記](../Page/成人記.md "wikilink")》(第一季)飾 章楠 (網劇)
  - 2013年:
      - 《[追魚傳奇](../Page/追魚傳奇.md "wikilink")》飾 張珍
  - 2014年:
      - 《[妻子的秘密](../Page/妻子的秘密.md "wikilink")》飾 雨軒、高翔
      - 《[新济公活佛之状告济公](../Page/新济公活佛.md "wikilink")》飾 程财
      - 《[美人制造](../Page/美人制造.md "wikilink")》飾 曾文远
  - 2015年:
      - 《[爱你，万缕千丝](../Page/爱你，万缕千丝.md "wikilink")》飾 方天续
      - 《[天才在左，疯子在右](../Page/天才在左，疯子在右.md "wikilink")》(網絡劇)
      - 《[好想听你说爱我](../Page/好想听你说爱我.md "wikilink")》飾 夏烈
  - 2016年:
      - 《[天天有喜之人间有爱](../Page/天天有喜之人间有爱.md "wikilink")》飾 王俊
      - 《[山海经之赤影传说](../Page/山海经之赤影传说.md "wikilink")》飾 上官锦
      - 《[秀麗江山之長歌行](../Page/秀麗江山之長歌行.md "wikilink")》飾 鄧禹
      - 《[飞刀又见飞刀](../Page/飞刀又见飞刀_\(2016年电视剧\).md "wikilink")》飾 龙逸
  - 2017年：
      - 《[拜見宮主大人](../Page/拜見宮主大人.md "wikilink")》(網絡劇)飾 秦斩
  - 2019年：
      - 《[拜見宮主大人](../Page/拜見宮主大人.md "wikilink")2》(網絡劇)飾 秦斩

<!-- end list -->

  - 待播
      - 《[失忆之城](../Page/失忆之城.md "wikilink")》飾 林小刀

### 廣播劇

  - [商業電台](../Page/商業電台.md "wikilink")《[36計](../Page/36計.md "wikilink")》
  - [新城電台](../Page/新城電台.md "wikilink")《[十分愛](../Page/十分愛.md "wikilink")》
  - 商業電台《[你我他的生日故事](../Page/你我他的生日故事.md "wikilink")》
  - 商業電台《[戀愛樂園](../Page/戀愛樂園.md "wikilink")》
  - 商業電台《[妹妹妹妹實況劇場](../Page/妹妹妹妹#妹妹妹妹實況劇場.md "wikilink")》

### 舞台劇

  - 《[再見理想](../Page/再見理想.md "wikilink")》 飾
    子健（2008年9月19日至21日，[香港理工大學賽馬會綜藝館](../Page/香港理工大學.md "wikilink")）

### MV

  - [I Love You Boyz](../Page/I_Love_You_Boyz.md "wikilink")：差人咪郁
  - [達明一派](../Page/達明一派.md "wikilink")：達明一派對
  - [Twins](../Page/Twins.md "wikilink")：眼紅紅、夏日狂嘩、風箏與風、愛情突擊
  - [鍾舒漫](../Page/鍾舒漫.md "wikilink")：請你合作
  - [鍾汶](../Page/鍾汶.md "wikilink")：難題
  - [VRF](../Page/VRF.md "wikilink")：妄想（剧场版）

### 音樂會及演唱會

#### Boy'z時期

  - 2002年：Twins Ichiban興奮演唱會（9月13日－9月15日，表演嘉賓，紅磡香港體育館）

<!-- end list -->

  - 2003年：
      - 松日Twins廣州興奮演唱會（1月18日－1月19日，表演嘉賓，廣州）
      - 一夜成名隊名成立之夜（2月10日，九龍灣國際展覽中心）
      - 能得利903大晒會之我要做我偶像（6月2日，第一次總決賽，大學會堂）
      - 謝霆鋒佛山演唱會（8月16日，表演嘉賓，佛山）
      - 謝霆鋒深圳演唱會（9月30日，表演嘉賓，深圳）
      - Show Up\! Neway容祖兒演唱會（10月20日，表演嘉賓，紅磡香港體育館）
      - Twins零4好玩演唱會（2003年12月31日－2004年1月4日，表演嘉賓，紅磡香港體育館）

<!-- end list -->

  - 2004年：
      - 成雙成對演唱會（2月1日，香港會議展覽中心）
      - 新城容祖兒流行女皇音樂會Feel the Pop（4月4日，表演嘉賓，香港會議展覽中心）
      - 星Mobile潮爆舞林大派對（4月9日，表演嘉賓，九龍灣國際展覽中心）
      - Twins零4好玩廣州演唱會（5月1日，表演嘉賓，廣州）
      - 新城熱火潮Twins零舍大派對（7月12日，表演嘉賓，香港會議展覽中心）
      - Twins零4好玩東莞演唱會（7月17日，表演嘉賓，東莞）
      - Twins零4好玩中山演唱會（7月24日，表演嘉賓，中山）
      - 星Mobile超時空接觸演唱會（12月19日，香港會議展覽中心）

<!-- end list -->

  - 2018年：
      - HINSIDEOUT張敬軒演唱會（6月17日，表演嘉賓及6月19日，即興表演，紅磡香港體育館）
      - BOYZ The Unboxing Live (10月16日，香港九展Star Hall)

#### 個人時期

  - 2005年：Reflection of Joey's Live容祖兒演唱會2005（表演嘉賓）

<!-- end list -->

  - 2006年：
      - Twins一時無兩演唱會2006（表演嘉賓）
      - Budweiser Bar
        Pacific迎接FIFA世界盃2006演唱會（5月21日，九龍灣[國際展貿中心](../Page/國際展貿中心.md "wikilink")）
      - 為中國喝采：慶祝[香港回歸九周年綜藝晚會](../Page/香港回歸.md "wikilink")（6月29日，[香港紅磡體育館](../Page/香港紅磡體育館.md "wikilink")）
      - Twins馬來西亞演唱會（8月18日－8月19日，表演嘉賓，馬來西亞雲頂雲星劇場）
      - Reflection of Joey's Live @ Atlantic City 2006
        容祖兒演唱會（11月22日－11月23日，表演嘉賓，大西洋城印度宮殿大賭場體育館）
      - Reflection of Joey's Live @ Las Vegas 2006
        容祖兒演唱會（11月25日，表演嘉賓，Mirage Events
        Centre）
      - Reflection of Joey's Live 2006 容祖兒演唱會（11月27日，表演嘉賓，華瑪娛樂演奏廳）

<!-- end list -->

  - 2007年：
      - 2007 SUCCESS Fundraising Gala
        中僑慈善群星夜（2月25日，[通用體育館](../Page/通用體育館.md "wikilink")）
      - [星光熠熠耀保良](../Page/星光熠熠耀保良.md "wikilink")（6月16日，[香港紅磡體育館](../Page/香港紅磡體育館.md "wikilink")）

<!-- end list -->

  - 2017年:
      - 2017決戰食神 Music Tasting 英皇超級巨星演唱會 -
        廣州站（1月14日，谢霆锋、容祖儿、Twins、Vincy泳儿、林峯、洪卓立Ken、鍾舒漫SherSher）

## 獎項

### Boy'z時期

#### 2003年

  - 無綫電視《勁歌金曲第一季季選》：新星試打三屆台主
  - 無綫電視《勁歌金曲第二季季選》：最有潛質男新人獎
  - 廣州電視台《勁歌王-- 2003季選頒獎典禮》：最佳新人組合
  - PM第二屆夏日人氣頒獎典禮：PM夏日魅力人氣新進男子組合
  - TVB 2003兒歌金曲頒獎禮：兒歌金曲《LALA世界》
  - 「雪碧」我的選擇中國原創音樂流行榜2002-2003年總選典禮：優秀新人獎（香港）
  - 「雪碧」我的選擇中國原創音樂流行榜2002-2003年總選典禮：最佳廣告歌曲獎《總有一站愛上你》）
  - Yahoo！搜尋人氣大獎頒獎禮：Yahoo！搜尋人氣歌手獎
  - 香港電台《第26屆十大中文金曲頒獎禮》：最有前途新人獎（組合金獎）
  - 新城《新城勁爆頒獎禮2003》：新城勁爆新人王
  - 商業電台《03年度叱吒樂壇流行榜頒獎禮》：叱吒樂壇新力軍組合（金獎）

#### 2004年

  - 加拿大中文電台《加拿大至Hit中文歌曲排行榜 - 03年度全國樂迷投票》：最受歡迎新組合（粵，金獎）
  - 無綫電視《2003年度十大勁歌金曲頒獎典禮》：新星試打金曲《LALA世界》
  - 廣東電視台《音樂先鋒榜》2003年度總選（香港）新晉組合獎
  - IPFI 香港唱片銷量大獎2003：最暢銷本地新人組合
  - 2003年度勁歌王頒獎典禮：最有前途新人組合（金獎）
  - 2003年度勁歌王頒獎典禮：網絡人氣組合
  - 2004粵港未來巨星頒獎典禮：未來巨星大獎
  - TVB 2004兒歌金曲頒獎禮：十大兒歌金曲《繼續跅跅步哈姆太郎》
  - TVB 2004兒歌金曲頒獎禮：兒歌金曲銅獎《繼續跅跅步哈姆太郎》
  - 廣州電台《金曲金榜》金曲年度飛躍男組合
  - 無綫電視《2004年勁歌金曲優秀選第二回》最受歡迎廣告歌曲獎《超時空接觸》
  - 新城勁爆頒獎禮2004：勁爆組合
  - 新城勁爆頒獎禮2004：勁爆跳舞歌曲《超時空接觸》

#### 2005年

  - 無綫電視《2004年度十大勁歌金曲頒獎典禮》最受歡迎組合（銅獎）
  - 無綫電視《2004年度十大勁歌金曲頒獎典禮》最受歡迎廣告歌曲獎（銅獎）《超時空接觸》
  - 《音樂先鋒榜頒獎典禮 2004》香港最受歡迎男组合（內地，金獎）

#### 2018年

  - Yahoo！搜尋人氣大獎頒獎禮：人氣票選組合

### 個人時期

#### 2005年

  - 《新城國語力頒獎禮2005》：新城國語力熱播新聲音
  - 《Yahoo！搜尋人氣大獎》：新人薦場飆星獎
  - 新城《新城勁爆頒獎禮2005》：新城勁爆新人王

#### 2006年

  - 2005年度「雪碧」我的選擇中國原創音樂流行榜：飛躍進步獎
  - 無綫電視《2005年度十大勁歌金曲頒獎典禮》：最受歡迎男新人（銅獎）
  - 香港電台《第28屆十大中文金曲頒獎禮》：最有前途新人獎（男歌手銅獎）
  - 新浪《Sina Music樂壇民意指數音樂頒獎禮》：我最喜愛男新人（銀獎）
  - 雅虎《Yahoo\!搜尋人氣大獎》樂壇新勢力
  - Roadshow《Roadshow至尊音樂頒獎禮》男新人獎
  - 《第三屆勁歌王總選》最有前途新人獎（男歌手）金獎（內地）
  - 《9+2音樂先鋒榜》音樂先鋒（港台）男新人金獎
  - 《IFPI香港唱片銷量大獎2005頒獎禮》最暢銷本地男新人
  - Jessica雜誌《6 周年生日派對》 健康活力大獎
  - TVB 2006兒歌金曲頒獎禮：十大兒歌金曲《月光光》
  - HMC雜誌《HMC In Style》獎項
  - 新城《新城勁爆頒獎禮2006》新城勁爆人氣男歌手

#### 2007年

  - Roadshow《Roadshow至尊音樂頒獎禮》：至尊人氣歌手
  - 《第四屆勁歌王總選》：網絡人氣歌手（內地）
  - 新城《新城勁爆頒獎禮2007》：新城勁爆合唱歌曲《3+1=1》
  - RoadShow《至尊音樂頒獎禮2007》 ：至尊合唱歌曲《3+1=1》

#### 2018年

  - 第24屆華鼎獎「觀眾最喜愛影視明星」

## 參考資料

## 外部連結

  -
  -
  - [關智斌新浪博客](http://blog.sina.com.cn/kennykwanjr)

[C](../Category/關姓.md "wikilink")
[Category:開平人](../Category/開平人.md "wikilink")
[Category:菲律賓華人](../Category/菲律賓華人.md "wikilink")
[Category:21世紀歌手](../Category/21世紀歌手.md "wikilink")
[Category:21世紀男演員](../Category/21世紀男演員.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:粵語流行音樂歌手](../Category/粵語流行音樂歌手.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港天主教徒](../Category/香港天主教徒.md "wikilink")
[Category:鄧鏡波學校校友](../Category/鄧鏡波學校校友.md "wikilink")
[Category:香港專業教育學院校友](../Category/香港專業教育學院校友.md "wikilink")

1.