[Kanto_plain.png](https://zh.wikipedia.org/wiki/File:Kanto_plain.png "fig:Kanto_plain.png")
[Right](https://zh.wikipedia.org/wiki/File:KantoHeiya.jpg "fig:Right")及日光連山，左方是[三國山脈](../Page/三國山脈.md "wikilink")。由[東京國際機場上空拍攝](../Page/東京國際機場.md "wikilink")\]\]

**關東平原**（）是位於[日本](../Page/日本.md "wikilink")[關東地方的](../Page/關東地方.md "wikilink")[平原](../Page/平原.md "wikilink")，也是日本最大的平原，其范围包括关东地方的一都六县（不含[房總半島的](../Page/房總半島.md "wikilink")），总面积约17000平方公里。

## 地理

關東平原的範圍包括了[群馬縣](../Page/群馬縣.md "wikilink")、[栃木縣](../Page/栃木縣.md "wikilink")、[茨城縣](../Page/茨城縣.md "wikilink")、[千葉縣](../Page/千葉縣.md "wikilink")、[埼玉縣](../Page/埼玉縣.md "wikilink")、[東京都](../Page/東京都.md "wikilink")、[神奈川縣等七個](../Page/神奈川縣.md "wikilink")[自治體](../Page/日本行政區劃.md "wikilink")。平原內的主要[城市則有](../Page/城市.md "wikilink")[東京特別區](../Page/東京特別區.md "wikilink")、[橫濱市](../Page/橫濱市.md "wikilink")、[川崎市](../Page/川崎市.md "wikilink")、[千葉市及](../Page/千葉市.md "wikilink")[埼玉市](../Page/埼玉市.md "wikilink")。

平原的北側，由東到西，分別為[榛名山](../Page/榛名山.md "wikilink")、[赤城山](../Page/赤城山_\(日本\).md "wikilink")、[足尾山地](../Page/足尾山地.md "wikilink")、[日光連山](../Page/日光.md "wikilink")、[高原山](../Page/高原山.md "wikilink")、[那須連山](../Page/那須.md "wikilink")、[八溝山地](../Page/八溝山地.md "wikilink")、[阿武隈山地環繞](../Page/阿武隈山地.md "wikilink")。平原的西界是[關東山地](../Page/關東山地.md "wikilink")、東界為[鹿島灘及](../Page/鹿島灘.md "wikilink")[九十九里濱](../Page/九十九里濱.md "wikilink")。南邊則是被[房總半島及](../Page/房總半島.md "wikilink")[三浦半島的](../Page/三浦半島.md "wikilink")[丘陵和](../Page/丘陵.md "wikilink")[東京灣](../Page/東京灣.md "wikilink")、[相模灣包圍](../Page/相模灣.md "wikilink")。

平原上的[河川多發源自北方及西方的山地](../Page/河川.md "wikilink")，流向東方或南方。主要的河川有[利根川](../Page/利根川.md "wikilink")、[渡良瀨川](../Page/渡良瀨川.md "wikilink")、[鬼怒川](../Page/鬼怒川.md "wikilink")、[那珂川](../Page/那珂川_\(關東\).md "wikilink")、[荒川](../Page/荒川_\(關東\).md "wikilink")、[多摩川及](../Page/多摩川.md "wikilink")[相模川](../Page/相模川.md "wikilink")。其中最為重要的是利根川，其流域面積占了關東平原的一半以上。

[台地的部分](../Page/台地.md "wikilink")，則有[武藏野台地](../Page/武藏野台地.md "wikilink")、[相模野台地](../Page/相模野台地.md "wikilink")（相模原台地）、[大宮台地及](../Page/大宮台地.md "wikilink")[下總台地等幾乎都被名為](../Page/下總台地.md "wikilink")[關東壤土層的火山灰土壤覆蓋](../Page/關東壤土層.md "wikilink")。至於[丘陵的部分](../Page/丘陵.md "wikilink")，則有第三紀時隆起的[多摩丘陵等](../Page/多摩丘陵.md "wikilink")。

土地利用方面，平原上以[東京為中心的](../Page/東京.md "wikilink")[首都圏幾乎全為都市用地所覆蓋](../Page/首都圏.md "wikilink")。在周邊的[沖積平原上有一部分的](../Page/沖積平原.md "wikilink")[稻作農業存在](../Page/稻.md "wikilink")，而在[台地則多為旱田](../Page/台地.md "wikilink")。

## 主要地形

  - 台地：武藏野台地、下末吉台地、相模野台地、下总台地、常陸台地、那須野原
  - 湿地：渡良瀬遊水地、見沼田んぼ、東京湾干潟
  - 丘陵：多摩丘陵、房総丘陵、大磯丘陵、塩那丘陵、宇都宮丘陵、比企丘陵、狭山丘陵、三浦丘陵
  - 河川：利根川、烏川、渡良瀬川、思川、鬼怒川、田川、小貝川、五行川、多摩川、秋川、相模川、荒川、江戸川、隅田川、中川、那珂川、久慈川
  - 湖泊：霞浦湖、北浦、印旛沼、手賀沼、牛久沼、谷中湖

## 參考資料

  - [關東平原的介紹，來自埼玉縣幸手市網頁](https://web.archive.org/web/20061006120108/http://www.city.satte.saitama.jp/kankyou-navi/tikei/kantouheiya.htm)

  - [關東平原的基盤構造](https://web.archive.org/web/20070912143554/http://staff.aist.go.jp/msk.takahashi/Subsurface.html)

[Category:日本平原](../Category/日本平原.md "wikilink")
[Category:關東地方地理](../Category/關東地方地理.md "wikilink")