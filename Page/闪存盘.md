**闪存盘**（，-{zh-cn:又称; zh-hant:又稱}-**-{随身碟}-**、**-{zh-hans:USB随身碟;
zh-hk:USB記憶體; zh-tw:USB隨身碟}-**、**-{zh-hans:快闪随身碟;
zh-hant:閃存盤}-**、**-{zh-hans:USB记忆体; zh-hk:快閃隨身碟;
zh-tw:USB記憶體}-**、**-{zh-hans:优盘; zh-hant:優盤}-**、**-{zh-hans:U盘;
zh-hant:U盤}-**、**-{zh-hans:电子盘; zh-hant:電子盤}-**、**-{zh-hans:随身碟;
zh-hant:隨身碟}-**、**-{zh-hans:记忆棒;
zh-hant:記憶棒}-**、**-{手指}-**、**随存**、**大姆哥**、**快閃記憶體**），發明者尚有争議。它是一種使用[USB接口連接计算機](../Page/USB.md "wikilink")，并通常通過[闪存來进行數据存储的小型便携存储设备](../Page/闪存.md "wikilink")。一般U盘體積極小、重量輕、可重複寫入，面世後迅速普及並取代傳統的軟碟及軟碟機。有時[讀卡器也會被歸類為隨身碟](../Page/讀卡器.md "wikilink")，但這類設備的記憶晶片並不是內建的，而是可以抽換的[記憶卡](../Page/記憶卡.md "wikilink")。

相較於其他可攜式儲存裝置（尤其是[軟碟](../Page/軟碟.md "wikilink")），U盘有許多優點：較不佔空間、能儲存較多資料、讀寫速度較快、不會因刮傷或[發霉而無法使用](../Page/發霉.md "wikilink")。這類的USB存储设备使用[USB大量儲存裝置標準](../Page/USB.md "wikilink")，在近代的[作業系統如](../Page/作業系統.md "wikilink")[Windows](../Page/Windows.md "wikilink")、[OS
X](../Page/OS_X.md "wikilink")、[Linux與](../Page/Linux.md "wikilink")[Unix中皆有內建支援](../Page/Unix.md "wikilink")。

U盘通常使用塑膠或金屬外殼，內部含有一張小的[印刷電路板](../Page/印刷電路板.md "wikilink")，讓U盘尺寸小到像[鑰匙圈飾物一樣能夠放到口袋中](../Page/鑰匙圈.md "wikilink")，或是串在頸繩上。只有USB連接頭突出於保護殼外，且通常被一個小蓋子蓋住。大多數的U盘使用標準的[Type-A
USB接頭](../Page/USB#.E6.9C.BA.E6.A2.B0.E5.92.8C.E7.94.B5.E6.B0.94.E6.A0.87.E5.87.86.md "wikilink")，這使得它們可以直接插入個人電腦上的USB接口中。

要存取U盘的資料，就必須把U盘連接到電腦；無論是直接連接到電腦內建的USB控制器或是一個[USB集線器都可以](../Page/USB集線器.md "wikilink")。只有當被插入[USB埠時](../Page/USB.md "wikilink")，U盘才會啟動，而所需的電力也由[USB連接埠供給DC](../Page/USB.md "wikilink")+5V。

## 歷史

[USB_Flash_Drive_animated.gif](https://zh.wikipedia.org/wiki/File:USB_Flash_Drive_animated.gif "fig:USB_Flash_Drive_animated.gif")闪存盘;zh-hant:[USB隨身碟](../Page/USB.md "wikilink")}-\]\]
自1998年至2000年，有很多公司声称自己是第一个发明了U盘。还有一些不同的公司声称是他们第一个设想了或详细解释了、或制造了、或申请了[专利](../Page/专利.md "wikilink")；甚至有的公司声称是自己第一个在市场上销售这种
U盘。

[Trek是已知的第一个在市场上销售这种以闪存为介质的USB数据存儲器的公司](../Page/Trek.md "wikilink")。但是他们所申请的专利并没有详细说明这种全新意义的USB闪存儲存器，而只是普遍意义上的数据存儲设备。

群聯電子[潘健成被許多人認為是](../Page/潘健成.md "wikilink")「隨身碟之父」。\[1\]\[2\]2001年台灣群聯電子在[工研院育成中心地下室生產出全球第一顆USB快閃記憶體的系統控制單晶片](../Page/工業技術研究院.md "wikilink")，一時Pen
Drive成為隨身碟的代名詞。

[中国的](../Page/中国.md "wikilink")（Netac Technology Co.,
Ltd.）在1999年声称自己研发的U盘为全球第一款\[3\]。
[Rio_su40.jpg](https://zh.wikipedia.org/wiki/File:Rio_su40.jpg "fig:Rio_su40.jpg")
[USB_flash_drive.jpg](https://zh.wikipedia.org/wiki/File:USB_flash_drive.jpg "fig:USB_flash_drive.jpg")（PNY）闪存盘剛好10cm長\]\]

来自[以色列的M](../Page/以色列.md "wikilink")-Systems公司（即现在的[SanDisk](../Page/SanDisk.md "wikilink")）自1998年起就开始着手研发这种设备。他们于1999年的10月份注册了diskonkey.com的域名，并明确地指出这种全新意义的USB闪存设备正在被研发。2000年，丹·哈克比加盟M-System团队并领导DiskOnKey的开发。由Ziba完成工业设计的产品赢得了2001年的IDEA大奖。M-System在专利描述中严格地为这种新设备做了全面的叙述。

[IBM的發明文件RPS](../Page/IBM.md "wikilink")8-1999-0201（1999年9月）是目前已知最早精確並完整地描述了USB-FD的文件，但也僅止於USB-FD。M-Systems幫IBM[貼牌生產了DiskOnKey](../Page/貼牌生產.md "wikilink")，而IBM最早於2000年晚期在北美銷售這項產品。

Trek宣稱他們是第一個構思並製造拇指碟（ThumbDrive）的公司。該公司的執行長亨因·丹曾說：「當我們在2000年上半年首次推出拇指碟這項產品時，我們相信這小小的裝置能夠改變全世界顧客儲存及交換資料的方式。」、「它的潛力讓Trek願意投入投資並且保護關於它的智慧財產權。」目前Trek在[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[南韓](../Page/南韓.md "wikilink")、[英國](../Page/英國.md "wikilink")、[紐西蘭以及](../Page/紐西蘭.md "wikilink")[新加坡擁有拇指碟的專利權](../Page/新加坡.md "wikilink")。\[4\]
[英國法院則推翻Trek的一項英國專利](../Page/英國.md "wikilink")。\[5\]
雖然如此，U盘的專利擁有權仍然有廣大的爭議。據[海峽時報報導](../Page/海峽時報.md "wikilink")，在當時已有許多其它公司開始銷售相似的裝置。M-Systems（在[那斯達克上市的一家公司](../Page/那斯達克.md "wikilink")）稱它們的產品為DiskOnKey以及Diskey。Electec是M-System的進口商，而FE
Global是它在新加坡的經銷商。Trek控告四家廠商侵犯它的專利，而這四家廠商提出反擊，要求廢除該無效的專利。\[6\]

同時[Lexar也宣稱了自己首創了U盘產品](../Page/Lexar.md "wikilink")——2000年時他們推出了一款內建USB功能的[CompactFlash卡](../Page/CompactFlash.md "wikilink")。Lexer也提供一組讀卡機及USB連接線，使得使用者不需準備另外的USB集線器。

最後新加坡法庭確認了Trek在拇指碟專利的有效性，在海峽時報所刊出的判決文中，認為它是「新奇且有創造性的」。新加坡的最高法庭同時也駁回了四家公司——以色列公司M-Systems、Electec、FE
Global Electronics與新加坡公司Ritronics
Components——的請求，並且禁止它們繼續銷售類似的裝置。這項判決預期會影響Trek集團正在英國、日本與台灣進行的法律訴訟。

追溯U盘发展历史，在2000年全球至少有5家企业在销售U盘，包括以色列的M-system、新加坡Track、朗科优盘、鲁文易盘和韩国Flash
Driver。

U盘速度会受限于U盘主控性能、双层MLC NAND性能以及所支持的USB协议。USB
2.0理论传输速度480Mbit/S，但受限于传输协议，实际速度不会超过35MByte/S。USB
3.0可以支持更高的传输速度，但受限于主控和MLC闪存性能，2013年度市面上普通USB 3.0
U盘写入速度一般不超过20MB/S。市面上一些高速USB 3.0
U盘，通过使用多通道控制器或SATA转USB控制器，配合更快的NAND晶片等技術，已超越前述的速度限制。

USB 3.0 U盘若插在USB 2.0接口上，只能通过USB 2.0模式进行数据传输，但是由于USB 3.0
U盘采用了更高频率的晶振和更快速度的闪存，即便插在USB
2.0接口上一般也比普通USB 2.0
U盘快。U盘的实际传输速度，与所传输的文件亦有相关，零碎文件需要更长的传输时间。

U盘已經成為某種「流行印象」\[7\]，有點類似[iPod的白色耳塞式耳機](../Page/iPod.md "wikilink")。

## 裝置元件

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Usbkey_internals.jpg" title="fig:Usbkey_internals.jpg">Usbkey_internals.jpg</a><br />
<strong>隨身碟內部結構</strong><br />
<small>（照片中為Seitec的USB1.1隨身碟）</small></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>1</small></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><small>2</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>3</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><small>4</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>5</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><small>6</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>7</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><small>8</p></td>
</tr>
</tbody>
</table>

裝置的一端為一個Type-A
USB插頭。在塑膠殼內有一張印刷電路板。電路板上除了有簡單的電源電路外，還有一些焊接於電路板上的[積體電路](../Page/積體電路.md "wikilink")（IC）。一般來說，U盘上至少有两个IC，其中较大的、离USB口远的是闪存芯片，通常采用TSOP48或BGA封装，另一个是U盘主控，供电电路和晶振有时候会被集成于该主控内部。

### 必要元件

通常一個隨身碟有三個部份：

  - Type-A USB插頭
    提供連接到電腦的介面
  - USB大量儲存裝置控制器
    提供USB裝置控制器及與快閃記憶體溝通的介面。此控制器含有一個[RISC的微處理器及一些](../Page/RISC.md "wikilink")[ROM](../Page/唯讀記憶體.md "wikilink")（Read-Only
    Memory）與[RAM](../Page/隨機存取記憶體.md "wikilink")（Random Access Memory）。
  - ; 闪存
    :
    用以儲存資料。[闪存也常用於](../Page/闪存.md "wikilink")[數位相機](../Page/數位相機.md "wikilink")。U盘的固件和文件系统信息通常也位于[闪存内](../Page/闪存.md "wikilink")。U盘出厂前，厂家批量对U盘写入固件（In
    System
    Programing）和初始化，此过程称为开卡或量产。目前市面上的绝大多数U盘使用的[记忆芯片均与](../Page/闪存.md "wikilink")[固态硬盘内的闪存相似](../Page/固态硬盘.md "wikilink")。
    [石英振盪器](../Page/石英晶体谐振器.md "wikilink")
    供應裝置運作所需的12MHz（USB 2.0）/30MHz（USB
    3.0）[時序信號](../Page/時序信號.md "wikilink")，並且控制裝置的資料輸出。

### 額外元件

隨身碟也可能含有：

  - Micro USB/USB Type-C插头
    部分U盘带有Micro USB或USB
    Type-C插头，可在支持[OTG的移动设备](../Page/OTG.md "wikilink")（如手机、平板电脑）上使用。市面上也有相应转接头或转接线出售。
  - [跳線與測試接腳](../Page/跳線.md "wikilink")
    在生產時可進行測試，或用來將程式碼載入到微處理器中。
  - [發光二極體](../Page/發光二極體.md "wikilink")（LED）
    提示正在傳輸資料或是正在進行讀取或寫入動作。
  - 寫入保護開關
    提供「寫入保護模式」功能，防止資料被覆寫。
  - 未使用空間
    提供放置第二個記憶體晶片的空間，讓製造商能夠利用舊有的印刷電路板來製造不同儲存大小的隨身碟，以滿足市場的需要。
  - USB連接頭的蓋子
    減少因為[靜電而損壞隨身碟的機率](../Page/靜電.md "wikilink")，並且讓整體外觀更美觀。有些隨身碟不提供蓋子，而是將連接頭作成隱藏式，要用的時候才取出。另外有些隨身碟提供連接在本體上的旋轉蓋，以避免蓋子的遺失。
  - 小環
    有時在本體或蓋子上有一個小環，能夠讓[鑰匙圈](../Page/鑰匙圈.md "wikilink")、頸鏈或其它幫忙攜帶與儲存隨身碟的東西穿過它。

### 闪存类型

> 主条目：[闪存](../Page/闪存.md "wikilink")

  - SLC = Single-Level Cell
    1bit/cell，速度快壽命長，價格貴（約MLC 3倍以上的價格），約10萬次擦寫壽命。

<!-- end list -->

  - MLC = Multi-Level Cell
    2bit/cell，速度一般壽命一般，價格一般，約3000－10000次擦寫壽命。

<!-- end list -->

  - TLC = Triple-Level Cell
    3bit/cell，也有Flash廠家叫8LC，速度慢壽命短，價格便宜，通常約500－1000次擦寫壽命。

目前三者差別甚大，而通常廠家不將晶片種類標出，同时，厂商常常使用降级片来制作U盘。

## 容量

在[2006年11月時](../Page/2006年11月.md "wikilink")，市面上販售的隨身碟的儲存容量介於32[MB到](../Page/Megabyte.md "wikilink")1[TB之間](../Page/Terabyte.md "wikilink")\[8\]。容量大小因為快閃記憶體目前的密度而被限制。

## 一般用途

[Sony_Micro_Vault_Tiny_4_GB.jpg](https://zh.wikipedia.org/wiki/File:Sony_Micro_Vault_Tiny_4_GB.jpg "fig:Sony_Micro_Vault_Tiny_4_GB.jpg")隨身碟\]\]

  - 個人資料儲存
    隨身碟最常見的應用即是個人用以儲存及交換檔案，例如文件、圖片與影像。
  - 電腦修復
    隨身碟在個人電腦修復領域十分成功，因為它們可以將恢復或掃毒軟體送到被感染的電腦上，並且讓部份的資料在這種緊急狀態下仍然可以保存。
  - 系統管理
    隨身碟在系統與網路管理者中也很流行。他們可以將設定資料及用來管理系統、除錯與回復的軟體放在隨身碟上。
  - 攜帶應用程式
    隨身碟可用來攜帶應用程式到其它的電腦，不需經過安裝程序即可使用。由隨身碟製造商支持的U3在支援U3的隨身碟上提供了一套[應用程式設計介面](../Page/應用程式設計介面.md "wikilink")（API）來提供攜帶應用程式的功能。另外，也有免費與[開放原始碼的平台](../Page/開放原始碼.md "wikilink")[Portableapps](../Page/Portableapps.md "wikilink")\[9\]讓不支援U3的隨身碟也能夠使用類似U3的功能。airWRX\[10\]是另一個在隨身碟上執行，並讓電腦與鄰近的電腦進入一個多視窗、類似網頁介面的應用程式架構。[Mozilla
    Firefox與](../Page/Mozilla_Firefox.md "wikilink")[Opera](../Page/Opera.md "wikilink")\[11\]\[12\]網頁瀏覽器也有讓使用者能在隨身碟上執行的設定值。

隨身碟選單软件有：
[Creative_MuVo_TX_FM_256MB.jpg](https://zh.wikipedia.org/wiki/File:Creative_MuVo_TX_FM_256MB.jpg "fig:Creative_MuVo_TX_FM_256MB.jpg")是一個作成隨身碟的[數位音樂播放器](../Page/數位音樂播放器.md "wikilink")\]\]

  - airWRX[<sup>官網</sup>](http://networkimprov.net/airwrx/)
  - ASuite[<sup>官網</sup>](https://web.archive.org/web/20091110090100/http://www.salvadorsoftware.com/software/asuite)
  - Portable Start Menu[<sup>官網</sup>](http://www.aignes.com/psmenu.htm)
  - [PortableApps](../Page/PortableApps.md "wikilink")\[<http://www.portableapps.com/><sup>官網</sup>\]
  - PStart[<sup>官網</sup>](http://www.pegtop.net/start/)
  - SyMenu[<sup>官網</sup>](https://web.archive.org/web/20091031061836/http://www.ugmfree.it/symenu.aspx)
  - U3[<sup>官網</sup>](http://www.u3.com/)
  - UPP\[<http://www.5680.tw/modules/wfdownloads/><sup>官網</sup>\]
  - WinPenPack[<sup>官網</sup>](http://www.winpenpack.com/main/news.php)

<!-- end list -->

  - 音樂播放
    許多廠商製造的小尺寸[固態磁碟式](../Page/固態磁碟.md "wikilink")[數位音樂播放器](../Page/數位音樂播放器.md "wikilink")，基本上就是一個隨身碟加上音效輸出與簡單的使用者介面。最有名的當屬[蘋果電腦推出的](../Page/蘋果電腦.md "wikilink")[iPod
    shuffle](../Page/iPod_shuffle.md "wikilink")，與[創新科技的MuVo](../Page/創新科技.md "wikilink")。
  - 啟動與安裝[作業系統](../Page/作業系統.md "wikilink")
    與[LiveCD類似](../Page/LiveCD.md "wikilink")。使用者可以在一個可開機的隨身碟上執行任何一個作業系統，這稱為[Live
    USB](../Page/Live_USB.md "wikilink")。[Mac OS
    X從](../Page/Mac_OS_X.md "wikilink")[10.7版起](../Page/OS_X_Lion.md "wikilink")、[Windows從](../Page/Windows.md "wikilink")[Windows
    10開始](../Page/Windows_10.md "wikilink")，在市面上零售的彩盒實體版僅提供隨身碟，不再像以往提供光碟或磁碟片。

## 優缺點

[USB_flash_drives.jpg](https://zh.wikipedia.org/wiki/File:USB_flash_drives.jpg "fig:USB_flash_drives.jpg")隨身碟}-\]\]

  - U盘较不容易讓水或灰塵滲入（尤其是黑胶体U盘），也不怕被刮傷及發霉，而這些在舊式的攜帶式儲存裝置（例如[光碟](../Page/光碟.md "wikilink")、軟碟片）等是嚴重的問題。而隨身碟所使用的固態儲存設計讓它們能夠较大程度抵抗無意間的外力撞擊。這些優點使得隨身碟非常適合用來從某地把個人資料或是工作檔案攜帶到另一地，例如從家中到辦公室，或是一般來說需要攜帶到並存取個人資料的各種地點。由於USB在現今的個人電腦中幾乎無所不在，因而到處都可以使用隨身碟。不過，小尺寸的隨身碟也讓它們常常被放錯地方、忘掉或遺失。

<!-- end list -->

  - 隨身碟雖然小，但相對來說卻有较大的儲存容量。目前市售隨身碟皆能夠儲存比一片CD（700MB）還多的資料，2019年，最便宜的16GB隨身碟儲存快要直追一片雙面雙層[DVD](../Page/DVD.md "wikilink")（17GB）的資料了。目前市售随身碟最大容量可高达1TB\[13\]，读取速度超过250MB/s\[14\]

<!-- end list -->

  - 隨身碟使用USB大量儲存裝置的類-{}-別，這表示大多數現代的[作業系統都可以在不需要另外安裝](../Page/作業系統.md "wikilink")[驅動程式的情況下讀取及寫入隨身碟](../Page/驅動程式.md "wikilink")。隨身碟在作業系統裡面顯示成區塊式的邏輯單元，隱藏內部快閃記憶體所需的複雜細節。作業系統可以使用任何[檔案系統或是區塊定址的方式](../Page/檔案系統.md "wikilink")。有些電腦也可以利用隨身碟來[開機](../Page/開機.md "wikilink")。

<!-- end list -->

  - 與其它的快閃記憶體裝置相同，隨身碟在總讀取與寫入次數上也有限制。當隨身碟變舊時，寫入的動作會更耗費時間。當我們用隨身碟來執行應用程式或作業系統時，便不能不考慮這點。有些程式開發者特別針對這個特性以及容量的限制，為隨身碟撰寫了特別版本的作業系統（例如[Linux](../Page/Linux.md "wikilink")）与软件（例如[Portable
    Apps](http://portableapps.com)）。它們通常對使用空間做最佳化，並且將暫存檔儲存在電腦的主記憶體中，而不是隨身碟裡。

<!-- end list -->

  - 许多隨身碟支援寫入保護的機制。這種在外殼上的開關可以防止電腦寫入或修改磁碟上的資料。寫入保護可以防止[電腦病毒檔案寫入隨身碟](../Page/電腦病毒.md "wikilink")，以防止該病毒的傳播。

<!-- end list -->

  - 隨身碟比起機械式的磁碟來說更能容忍外力的撞擊，但仍然可能因為嚴重的物理損壞而故障或遺失資料。在-{zh-hans:组装;zh-hant:自組;}-電腦中，錯誤的USB連接埠接線也可能損壞隨身碟的電路。相对其他外设来说，U盘是比较容易出现故障的一类，所以应做好数据备份。

## 保养

闪存盘在拔出电脑前，应从[操作系统中移除设备](../Page/操作系统.md "wikilink")（如在[Windows中点电脑屏幕任务栏下方右面的](../Page/Windows.md "wikilink")“安全移除/Safely
Remove Hardware and Eject
Media”），等闪存盘自身的提示灯（如有）灭掉后才拔出，这样可以延长其使用寿命\[15\]\[16\]。U盘处于操作状态而强行拔出，有可能导致故障。

## 與其他可攜儲存裝置的比較

[Secure_digital_card_usb-adapter_2.jpg](https://zh.wikipedia.org/wiki/File:Secure_digital_card_usb-adapter_2.jpg "fig:Secure_digital_card_usb-adapter_2.jpg")
-{zh-hans:USB闪存盘;zh-hant:USB隨身碟}-\]\]
快閃記憶體儲存裝置常常被拿來跟其他常見可攜式的資料儲存裝置（例如[軟碟片](../Page/软盘.md "wikilink")、[Zip碟片](../Page/Zip_Drive.md "wikilink")、LS-120、[miniCD](../Page/Mini_CD.md "wikilink")／[miniDVD](../Page/miniDVD.md "wikilink")、[CD-R](../Page/CD-R.md "wikilink")／[CD-RW與](../Page/CD-RW.md "wikilink")[DVD-RW](../Page/DVD-RW.md "wikilink")）做比較。

軟碟是第一個普遍的檔案傳輸媒介，但它馬上因為低容量、慢速度與低使用寿命而不受歡迎。目前所有的新電腦都不內建軟碟機，但都有USB埠；蘋果電腦的[iMac是第一個這樣的電腦](../Page/iMac.md "wikilink")。許多擴充軟碟標準的嘗試（例如[Imation的](../Page/Imation.md "wikilink")[SuperDisk](../Page/SuperDisk.md "wikilink")）都不成功。這是因為評價後不可靠或是缺乏給個人電腦製造商遵循的標準所致。有部份的使用者使用Iomega
Zip磁碟機，但並沒有達到在電腦上無所不在的地步。另外，容量較大的Zip磁碟（目前最大750MB）並不能在舊的磁碟機上使用。除非使用者隨身攜帶外接磁碟機，否則在攜帶資料的用途來說它們是有限制的。另外，每MB的成本也十分高昂。由於使用了機械裝置，且Zip碟片儲存媒體與軟碟片的類似，所以Zip碟片比起隨身碟來說有較高的故障危險與資料遺失風險。容量更大的可攜式儲存媒體（例如Iomega的[Jaz磁碟](../Page/Jaz磁碟.md "wikilink")），磁碟機與碟片的成本更高，且更不普及；但在2007年，這些技術跟軟碟一樣仍然有人使用。

DVD-R\\DVD-RW\\DVD+R\\DVD+RW與CD-R\\CD-RW是另一種可攜式的儲存媒體。與Zip和軟碟機不同的是，DVD與CD燒錄器在現今的個人電腦系統上十分普及。CD-R只能夠寫入一次，而CD-RW只能重覆寫入約1000次，但現在的NAND快閃記憶體製的隨身碟可以重複寫入500,000或更多次。光學儲存裝置通常比起快閃記憶體儲存裝置慢。CD也不像隨身碟一樣方便攜帶。因此，CD與DVD是便宜地紀錄資料的好方法，但當要對大量資料中的少部份做小改變時並不適合；這是隨身碟主要的優點。

## 安全性

有些隨身碟提供資料加密的功能。通常在檔案系統的下層使用[全磁碟加密來達到](../Page/全磁碟加密.md "wikilink")。加密功能防止未經授權的人存取隨身碟上的資料；缺點則是因為沒有統一的標準，此隨身碟只能在少部份安裝有相容加密軟體的電腦上使用。

有些加密軟體可以不需安裝即可執行。這些軟體的執行檔可以跟加密的檔案一起儲存在隨身碟上。加密的檔案可以在執行[微軟](../Page/微軟.md "wikilink")[Windows的電腦上存取](../Page/Microsoft_Windows.md "wikilink")。其它的隨身碟允許使用者設定不同大小的加密與公開分割區。而為了Windows、MAC、Linux設計的加密軟體執行檔可以放在公開分割區中。有些較差的加密軟體可能需要管理者權限才能執行並存取資料。

新款的隨身碟支援[生物](../Page/生物.md "wikilink")[指紋功能來確認使用者的身份](../Page/指紋.md "wikilink")。在2005年年中，在隨身碟上支援指紋功能比起標準的密碼保護來說還是較貴的選擇。大多數的指紋掃描裝置必須依賴作業系統中的驅動程式來檢驗指紋，因而這些隨身碟就只能用在執行微軟Windows作業系統的電腦。

有些廠商將隨身碟作為執行認證的裝置之一。其方式是使用隨身碟內的加密金鑰或是與目標系統內的軟體溝通來控制敏感系統的存取權限。這些系統被設計為除非隨身碟被接上主機，否則系統不會運作。有些「電腦鎖」被插入電腦時，表現得也像隨身碟一樣。

所有資料保護的方式也增加了合法使用者（資料擁有者）無法存取資料的風險。

隨身碟對大型組織來說是很大的安全性挑戰。因為它們的小體積以及方便性，讓沒有權限的訪客或是有惡意的員工能夠在幾乎不會被發現的情況下偷走機密的資料。在2008年，香港及澳門的多家醫院發生多起閃存盤的遺失及失竊事件，引起病人隱私的危機。同樣地，共用或是公共的電腦也容易被攻擊者使用隨身碟放入[惡意軟體](../Page/惡意軟體.md "wikilink")，例如[rootkit或是](../Page/rootkit.md "wikilink")[封包擷取軟體等](../Page/封包擷取軟體.md "wikilink")。為了防止這些問題，有些組織禁止使用隨身碟，而有些電腦禁止一般使用者掛載USB大量儲存裝置（這是Windows
XP Service Pack
2的一個功能）。也有電腦使用第三方軟體來控制USB的使用。也有組織使用非科技的解決辦法：拔掉主機內部USB埠的連接線或乾脆用樹脂堵住USB埠。

部分闪存盘的控制器其实是一枚可编程微控制器，其固件可以被恶意重写，黑客通过此方式改变闪存盘的工作方式，例如将闪存盘伪装为经USB端口连接的键盘及鼠标，并执行内置的恶意代码导致用户的计算机被黑客控制和监视、数据被偷走，甚至损坏。此类攻击最先与Black
Hat USA 2014会议上展示\[17\]，被称之为BadUSB。

## 其它名稱

目前-{zh-hant:USB隨身碟;zh-hans:USB闪存盘}-是這些裝置事實上的標準名稱。許多主要的廠商，如[SanDisk](../Page/SanDisk.md "wikilink")、[Lexar](../Page/Lexar.md "wikilink")、[Kingston與經銷商使用隨身碟來描述它們](../Page/Kingston.md "wikilink")。然而，從過去到現在有數不清的品牌名稱與術語來描述隨身碟，讓廠商不容易[行銷產品](../Page/行銷.md "wikilink")，且顧客也不容易尋找適合的產品，有些常常被使用的名字事實上是某公司的註冊商標（例如disgo\[18\]）。

## 相关文化

  - 2004年時，[德國](../Page/德國.md "wikilink")[龐克樂團](../Page/龐克.md "wikilink")[WIZO將它們的音樂使用MP](../Page/WIZO.md "wikilink")3格式儲存在隨身碟中發表，稱為「WIZO
    Stick-EP」，是第一個使用隨身碟發表作品的樂團\[19\]。
  - 電影[CIA追緝令](../Page/CIA追緝令.md "wikilink")（The
    Recruit）與[落日殺神](../Page/落日殺神.md "wikilink")（Collateral）中，隨身碟在劇情裡扮演重要角色。
  - 有些隨身碟即使掉入水中，甚至被洗衣機洗過後，資料依然存在不會消失\[20\]。在通電之前確定隨身碟已經乾了的話，隨身碟不會有任何問題，依然可以正常使用。
  - 在[星艦奇航記](../Page/星艦奇航記.md "wikilink")：[銀河飛龍中出現的](../Page/銀河飛龍.md "wikilink")[等線性光學晶片](../Page/等線性光學晶片.md "wikilink")，是類似隨身碟（設計與功能）的一種虛構裝置。等線性光學晶片比真實世界的相似物早了十年出現。
  - [魔鬼阿諾](../Page/魔鬼阿諾.md "wikilink")（The Running
    Man，1987年）中，[玛利亚·肯奇塔·阿龙索飾演的安柏](../Page/玛利亚·肯奇塔·阿龙索.md "wikilink")·門德斯在她工作的工作室尋找「貝克爾斯菲大屠殺」的影片。影片儲存於一小片[快閃記憶體卡片中](../Page/快閃記憶體.md "wikilink")（類似[CompactFlash](../Page/CompactFlash.md "wikilink")，但為黑色粗糙表面）。由於目前先進的[編碼技術](../Page/編解碼器.md "wikilink")，今日我們已經可以在這種媒體中儲存高品質的影片了。
  - [-{zh-cn:南方公园; zh-tw:南方公園;
    zh-hk:衰仔樂園;}-的](../Page/南方公園.md "wikilink")《[要愛，不要魔獸世界](../Page/要愛，不要魔獸世界.md "wikilink")》這集中，使用了1GB的隨身碟儲存被[魔獸世界移除的](../Page/魔獸世界.md "wikilink")“万千真理之剑”（the
    Sword of a Thousand Truths）。
  - 在[-{zh-cn:街机; zh-tw:電動玩具;
    zh-hk:街機;}-遊戲](../Page/街机.md "wikilink")——[In the
    Groove與](../Page/In_the_Groove.md "wikilink")[In the Groove
    2中](../Page/In_the_Groove_2.md "wikilink")，使用者可用隨身碟來儲存最高分數、[螢幕攝](../Page/螢幕攝.md "wikilink")、舞步、連續技（combo）等，但是必須要支援[Linux的隨身碟才可以使用](../Page/Linux.md "wikilink")，否則在某些遊戲機上會出問題。這些資料也可以上傳到[Groovestats網站](http://www.groovestats.com/)上。

## 參見

  - [快閃記憶體](../Page/快閃記憶體.md "wikilink")

  - [外接硬碟](../Page/外接硬碟.md "wikilink")

  - [硬碟](../Page/硬碟.md "wikilink")

  - [檔案同步](../Page/檔案同步.md "wikilink")

  - [備份軟體列表](../Page/備份軟體列表.md "wikilink")

  - [可攜式軟體列表](../Page/綠色軟體#著名綠色／可攜式軟體列表.md "wikilink")

  - [Live USB](../Page/Live_USB.md "wikilink") - 於隨身碟上執行的作業系統

  - [移動硬碟](../Page/移动硬盘.md "wikilink")

  - [攜帶型媒體](../Page/攜帶型媒體.md "wikilink")

  -
  - [USB大量儲存裝置](../Page/USB大量儲存裝置.md "wikilink")

  - [磁碟加密軟體](../Page/磁碟加密軟體.md "wikilink")

  - [U3](../Page/U3.md "wikilink")

  - [隨身碟選單软件](../Page/隨身碟選單软件.md "wikilink")

  - [隨身碟選單软件列表](../Page/隨身碟選單软件列表.md "wikilink")

## 參考資料

## 外部連結

  - [USB for DOS](http://www.fracassi.net/iw2evk/)
  - [用蘋果電腦Shuffle的快閃記憶體單元建立一個RAID陣列](http://www.wrightthisway.com/Articles/000154.html)
  - [如何加密拇指碟與建立自動播放](http://glosoli.blogspot.com/2005/09/encrypted-thumb-drive-and-autoplay.html)
  - [在Windows 98
    SE作業系統上安裝USB隨身碟](http://www.technical-assistance.co.uk/kb/usbmsd98.php)

[Category:電腦貯存裝置](../Category/電腦貯存裝置.md "wikilink")
[Category:固態電腦儲存媒體](../Category/固態電腦儲存媒體.md "wikilink")
[Category:USB](../Category/USB.md "wikilink")
[Category:2000年面世](../Category/2000年面世.md "wikilink")

1.

2.

3.  [朗科跨国专利官司再赢一局　专利案尘埃落定](http://ip.people.com.cn/GB/11316864.html)

4.

5.  <https://www.ipo.gov.uk/patent/p-decisionmaking/p-challenge/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/318/06>

6.
7.

8.  [Kingston Introduces 'World's First' 1TB USB 3.0 Flash
    Drive](http://mashable.com/2013/01/08/kingston-1tb-usb-flash-drive/)

9.  [Portableapps網站](http://www.portableapps.com/)

10. [airWRX網站](http://networkimprov.net/airwrx/)

11.

12.

13. <http://www.cnet.com/news/kingston-behold-the-1-terabyte-flash-drive/>

14. <http://www.maximumpc.com/fast-usb-thumb-drive-2014/>

15. [U盘的一些事一些情，太平洋女性网](http://goods.pclady.com.cn/pc-fittings/0811/341337.html)

16. [U盘的保养](http://www.onedisc.cn/About-17.html)

17.

18. ['disgo'](http://www.mydisgo.com)

19.

20.