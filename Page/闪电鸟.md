**閃電鳥**（，香港舊譯**雷鳥**）是[寶可夢中的一種](../Page/寶可夢.md "wikilink")，全國編號145，與[急冻鸟](../Page/急冻鸟.md "wikilink")、[火焰鸟同為傳說中的三聖鳥](../Page/火焰鸟.md "wikilink")，最早出現於遊戲精靈寶可夢紅版及綠版，黃色的身體代表閃電。

## 出場

在動畫中閃電鳥的出場集數為城都聯盟第243集「サンダーとクリスタル！みずうみのひみつ！」，電影中的登場機會有在[神奇寶貝劇場版：夢幻之神奇寶貝
洛奇亞爆誕](../Page/神奇寶貝劇場版：夢幻之神奇寶貝_洛奇亞爆誕.md "wikilink")(まぼろしのポケモン
ルギアばくたん)中，和其他兩隻聖鳥寶可夢一起出現。

[神奇寶貝劇場版：神奇寶貝保育家與蒼海的王子
瑪納霏的片尾部分載著](../Page/神奇寶貝劇場版：神奇寶貝保育家與蒼海的王子_瑪納霏.md "wikilink")**傑克·渥卡**離去。

台灣[東森幼幼台版的閃電鳥由](../Page/東森幼幼台.md "wikilink")[王瑞芹配音](../Page/王瑞芹.md "wikilink")。

台灣[MOMO親子台版的閃電鳥由](../Page/MOMO親子台.md "wikilink")[于正昇配音](../Page/于正昇.md "wikilink")。

[es:Anexo:Pokémon de la primera
generación\#Zapdos](../Page/es:Anexo:Pokémon_de_la_primera_generación#Zapdos.md "wikilink")
[fi:Luettelo Pokémon-lajeista
(121–151)\#Zapdos](../Page/fi:Luettelo_Pokémon-lajeista_\(121–151\)#Zapdos.md "wikilink")
[simple:Legendary
Pokémon\#Kanto](../Page/simple:Legendary_Pokémon#Kanto.md "wikilink")

[Category:第一世代寶可夢](../Category/第一世代寶可夢.md "wikilink")
[Category:虛構鳥類](../Category/虛構鳥類.md "wikilink")
[Category:電屬性寶可夢](../Category/電屬性寶可夢.md "wikilink")
[Category:飛行屬性寶可夢](../Category/飛行屬性寶可夢.md "wikilink")
[Category:1996年推出的電子遊戲角色](../Category/1996年推出的電子遊戲角色.md "wikilink")