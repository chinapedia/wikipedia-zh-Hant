**闽北语**（），又稱**闽北话**（），是[漢語族](../Page/漢語族.md "wikilink")[閩語支之下的一种语言](../Page/閩語支.md "wikilink")，通行于福建北部地区（[闽北地区](../Page/闽北地区.md "wikilink")），包括[武夷山市](../Page/武夷山市.md "wikilink")、[建瓯市](../Page/建瓯市.md "wikilink")、[松溪县](../Page/松溪县.md "wikilink")、[政和县](../Page/政和县.md "wikilink")，[浦城县南部](../Page/浦城县.md "wikilink")，以及[南平市区大部分](../Page/南平市.md "wikilink")、[顺昌县的大部分地区](../Page/顺昌县.md "wikilink")。

## 历史

福建古为[百越的](../Page/百越.md "wikilink")[七闽地](../Page/七闽.md "wikilink")。[战国末期](../Page/战国.md "wikilink")，[越国为](../Page/越国.md "wikilink")[楚国所灭](../Page/楚国.md "wikilink")，其王族率部众迁来福建，与当地原住民融合，形成[闽越族](../Page/闽越族.md "wikilink")，建立[闽越国](../Page/闽越国.md "wikilink")。\[1\]这些福建原住民所操的语言是[闽越语](../Page/闽越语.md "wikilink")。根据现代语言学家的[语言比较可知](../Page/比较语言学.md "wikilink")，古代闽越语与现代[壮侗语系存在一定血缘关系](../Page/壮侗语系.md "wikilink")。今日各闽语都存在大量“有音无字”的词汇，不少是从壮侗语系语言中保留下来的词汇底层。\[2\]

历史上，闽越国曾一度是中原政权东南方最强大的一支势力，而闽越文明的发祥地正位于闽北的[武夷山脚下](../Page/武夷山.md "wikilink")。今[武夷山市](../Page/武夷山市.md "wikilink")[兴田镇的](../Page/兴田镇.md "wikilink")[城村汉城遗址](../Page/城村汉城遗址.md "wikilink")，是闽越历史上第一个都城的遗迹，由此可以证明闽北地区是福建最早被开发的一块区域。

前110年，[汉武帝派兵灭亡闽越国](../Page/汉武帝.md "wikilink")，将闽越人举国迁徙到[江](../Page/长江.md "wikilink")[淮流域一带](../Page/淮河.md "wikilink")。在迁徙路途中，不少闽越人成群结队的逃匿于山间深处，形成后来的[山越民族](../Page/山越.md "wikilink")。\[3\]此后，汉朝驻军在这片土地上定居，成为来福建的第一批汉人。这些汉军多为江东的[吴人和江西的](../Page/吴越民系.md "wikilink")[楚人](../Page/湖湘民系.md "wikilink")，他们将自己的母语——[古吴语和](../Page/吴语.md "wikilink")[古楚语](../Page/湘语.md "wikilink")（古湘语）带入了福建。此后，避难百姓、亡命者以及流放者相继来到福建，其中，吴人多自[仙霞岭经](../Page/仙霞岭.md "wikilink")[浦城进入闽北](../Page/浦城.md "wikilink")，而楚人多越过武夷山进入闽北和闽中。这些汉人在与山越人接触中，将闽越语元素融入了自身的语言中，最后形成了[原始闽语](../Page/原始闽语.md "wikilink")。\[4\]

[东汉末年](../Page/东汉.md "wikilink")，山越人起兵反抗朝廷，[会稽太守](../Page/会稽.md "wikilink")[孙策派贺齐入福建讨伐](../Page/孙策.md "wikilink")，平定叛乱。203年（[建安八年](../Page/建安.md "wikilink")），在今[建瓯之地设立](../Page/建瓯.md "wikilink")[建安郡](../Page/建安郡.md "wikilink")。此后，建瓯成为闽北的政治文化中心。\[5\][西晋末年](../Page/西晋.md "wikilink")，发生[永嘉之乱](../Page/永嘉之乱.md "wikilink")，大量中原的汉人迁入福建避难，史称“[衣冠南渡](../Page/衣冠南渡.md "wikilink")，[八姓入闽](../Page/八姓入闽.md "wikilink")”。建安郡人口激增，新的移民带来了大量中原汉语的音素。[唐朝末年](../Page/唐朝.md "wikilink")，[河南](../Page/河南.md "wikilink")[固始人](../Page/固始.md "wikilink")[王审潮](../Page/王审潮.md "wikilink")、[王审知兄弟率农民军攻入福建](../Page/王审知.md "wikilink")，后建立[闽国割据政权](../Page/闽国.md "wikilink")。王审知在位期间实行仁政，使大量避难中原人来到福建定居，这些人带来了许多[中古汉语的音素](../Page/中古汉语.md "wikilink")。943年，闽国宗室[王延政割据](../Page/王延政.md "wikilink")[建州](../Page/建州.md "wikilink")，自称殷王，与[福州的闽王分庭抗礼](../Page/福州.md "wikilink")。这些事件都对闽北语的形成产生深远的影响。

原始闽语分化为各闽语的具体时间不详，今日的语言学界存在很大的争议。不过，从对《[集韵](../Page/集韵.md "wikilink")》中语音的分析可以发现，闽语分化的时间在宋朝初年以前，当时建州（闽北）、福州（闽东）、[泉州](../Page/泉州.md "wikilink")（闽南）三地的方言已经出现了明显的差异。学者[李如龙认为各闽语的分化时间是在唐末五代时期](../Page/李如龙.md "wikilink")。\[6\]

學術界一般認為[建甌話](../Page/建甌話.md "wikilink")（芝城話）是閩北語的標準音。[建瓯古时为建安郡](../Page/建瓯.md "wikilink")、建州、建宁府的治所在地，为闽北政治文化中心，因此建甌話在閩北語諸方言中影響力最大，被學者當做闽北語的代表方言，而建甌話又以城關腔（城關話）為標準音。[宋](../Page/宋.md "wikilink")、[元期间](../Page/元.md "wikilink")，由于政局动荡，抚州、信州一带的大量[江西人越过武夷山进入福建的](../Page/江西.md "wikilink")[邵武](../Page/邵武.md "wikilink")、[将乐一带](../Page/将乐.md "wikilink")，使得当地的闽北语被[赣语化](../Page/赣语.md "wikilink")，最终脱离闽北语，形成[邵将语](../Page/邵将语.md "wikilink")。

1795年（清乾隆六十年），[建寧府刊行韻書](../Page/建寧府.md "wikilink")《建州八音字義便覽》，簡稱《[建州八音](../Page/建州八音.md "wikilink")》。該書由[福清人](../Page/福清.md "wikilink")[林端材模仿](../Page/林端材.md "wikilink")[閩東語](../Page/閩東語.md "wikilink")[福州話的韻書](../Page/福州話.md "wikilink")《[戚林八音](../Page/戚林八音.md "wikilink")》編寫而成，是歷史上第一部建甌話韻書。1900年，西方傳教士參照此韻書，發明了[建寧府土腔羅馬字](../Page/建寧羅馬字.md "wikilink")（）來表記閩北語。

近代以来，闽北地区人口变动较大，闽北语诸方言之间发生了较大的差异。加上建瓯的逐渐没落，建瓯话的影响力大不如前，各地方言之间沟通存在一定困难。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，由于政府[推广普通话](../Page/推广普通话.md "wikilink")，再加上[延平区](../Page/延平区.md "wikilink")[南平官话](../Page/南平官话.md "wikilink")（[官话在南平境内的](../Page/官话.md "wikilink")[方言岛](../Page/方言岛.md "wikilink")）的长期影响，普通话在南平全境已经大体通行。相对地，闽北语的使用范围则越来越小。\[7\]

今日的闽北语中保留了古汉语中众多的语音特点和古汉语词汇和构词方法。例如，在闽北语[建瓯话中](../Page/建瓯话.md "wikilink")，称“漂亮”为“雅视”（，亦写作“雅式”）、称“锅”为“鼎”（）、称“筷子”为“箸”（），皆是古代汉语的说法。

## 分布地区

使用范围为福建省中北部[闽北地区的](../Page/闽北地区.md "wikilink")7个县、市、区，包括[松溪](../Page/松溪.md "wikilink")、[政和两县和](../Page/政和.md "wikilink")[建瓯](../Page/建瓯.md "wikilink")、[武夷山两市的全部](../Page/武夷山.md "wikilink")；[南平市大多数乡镇](../Page/南平市.md "wikilink")（市区一部分及樟湖、太平二乡除外）；[浦城县南部的石陂](../Page/浦城县.md "wikilink")、水北、濠村、山下乡（约占全县三分之一）；[顺昌县的高阳](../Page/顺昌县.md "wikilink")、大力、岚下、际会、仁寿、洋墩、埔上七乡的多数地区（接近半个县）。此外，[宁德的](../Page/宁德.md "wikilink")[周宁](../Page/周宁.md "wikilink")、[屏南二县西部边界的少数村落也通行闽北语](../Page/屏南.md "wikilink")。

闽北语在[东南亚地区如](../Page/东南亚.md "wikilink")[马来西亚](../Page/马来西亚.md "wikilink")、[新加坡和](../Page/新加坡.md "wikilink")[印尼有分布](../Page/印尼.md "wikilink")。使用者均是闽北移民的后裔。近年来因为闽北语的使用范围缩小，许多海外[闽北人转而使用使用范围较广的](../Page/闽北人.md "wikilink")[现代标准汉语或者](../Page/现代标准汉语.md "wikilink")[闽南语](../Page/闽南语.md "wikilink")。

## 方言

閩北語之下的方言可分為東西兩片，[松溪](../Page/松溪.md "wikilink")、[建溪流域的](../Page/建溪.md "wikilink")[松溪縣](../Page/松溪縣.md "wikilink")、[政和縣](../Page/政和縣.md "wikilink")、[建甌市](../Page/建甌市.md "wikilink")、[南平市為東溪片](../Page/南平市.md "wikilink")，以[松溪話為代表](../Page/松溪話.md "wikilink")；[崇陽溪流域的](../Page/崇陽溪.md "wikilink")[建陽市](../Page/建陽市.md "wikilink")、[武夷山市為西溪片](../Page/武夷山市.md "wikilink")，以[建陽話為代表](../Page/建陽話.md "wikilink")。

建甌話與東片各地的口音比較接近，西片口音在建甌市西部也有一定反映。建甌市區通行的芝城口音，與東、西兩片的口音存在著一定的差異。\[8\]

[邵武](../Page/邵武.md "wikilink")、[將樂一帶通行的邵將話是閩北語和](../Page/將樂.md "wikilink")[贛語混合型土語](../Page/贛語.md "wikilink")。語言學界曾一度將其劃為閩北語或贛語的方言，今日已被單獨劃出，直接隸屬於[閩語](../Page/閩語.md "wikilink")，稱為[邵將語](../Page/邵將語.md "wikilink")（或稱閩贛語）。

## 音韻體系

在閩北語各方言音韻體系都不盡相同，就連聲母、韻母和聲調的數目都差別迥異。《建州八音》將閩北語建甌話定為15個聲母、34個韻母和7個聲調。不過，根據福建學者在1984年的調查，建甌話已經僅存6個聲調。\[9\]但在建陽話裡，卻有18個聲母和8個聲調。\[10\]

| 分片  | colspan= 4 align="center" |東溪片 | colspan= 2 align="center" |西溪片 |
| :-: | ------------------------------ | ------------------------------ |
| 方言  | 建甌                             | 松溪                             |
| 聲母數 | 15                             | 15                             |
| 韻母數 | 34                             | 30                             |
| 聲調數 | 6                              | 8                              |
|     |                                |                                |

**閩北語音韻數量對照表**

閩北語兩個片區的聲調差別迥異，但總體而言，除了上聲不分陰陽之外，其他聲調皆可區分陰陽；唯一些方言的部份聲調發生了歸併現象，例如：建甌話的陽平和石陂話的陽入，就被併入了陰去。有些方言中，陽平聲調發生了分化，產生了兩種不同的聲調，分別稱為陽平甲、陽平乙（又作陽平一、陽平二）。

| rowspan= 2 |各方言的調值 | rowspan= 2 |陰平 | colspan= 2 |陽平    | rowspan= 2 |上聲 | rowspan= 2 |陰去 | rowspan= 2 |陽去 | rowspan= 2 |陰入 | rowspan= 2 |陽入 |
| ------------------ | -------------- | ----------------- | -------------- | -------------- | -------------- | -------------- | -------------- |
| 甲                  | 乙              |                   |                |                |                |                |                |
| 建甌                 | 54             | colspan= 2 | 併入陰去 | 21             | 33             | 44             | 24             | 42             |
| 松溪                 | 53             | 44                | 21             | 213            | 33             | 45             | 24             |
| 政和                 | 54             | 33                | 21             | 212            | 42             | 44             | 24             |
| 浦城石陂               | 53             | colspan= 2 | 42   | 21             | 33             | 45             | 214            | 併入陰去           |
| 建陽                 | 53             | 334               | 41             | 21             | 332            | 43             | 214            |
| 崇安（武夷山）            | 52             | colspan= 2 |33    | 21             | 22             | 55             | 35             | 5              |
|                    |                |                   |                |                |                |                |                |

**閩北語聲調對照表**

建甌話沒有明顯的變調和輕聲。有些上聲字組成的雙音詞，在連讀時，前字讀作陰去調。例如「老虎」為，快讀時可讀作，但不可以普遍類推，讀作原調亦可。名詞詞尾「仔」必須輕讀，但與其上聲的原調值並無明顯差異。\[11\]

## 腳註

## 參考資料

  - 《建甌方言詞典》（現代漢語方言大詞典分卷），李榮主編，李如龍、潘渭水編纂，江蘇教育出版社，1998年出版。ISBN
    7-5343-3411-X
  - [《建瓯县志》](http://www.fjsq.gov.cn/ShowText.asp?ToBook=3149&index=1662&)（请参见“第三十六篇
    方 言”一节）
  - [《建阳县志》](http://www.fjsq.gov.cn/showtext.asp?ToBook=3206&index=1475&)（请参见“第三十一篇
    方 言”一节）

## 外部連結

  - [Min Dialect
    Classification](http://www.glossika.com/en/dict/classification/min/index.php)
  - [闽北语ISO代码](http://www.ethnologue.com/show_language.asp?code=mnp)

[Category:閩語](../Category/閩語.md "wikilink")
[Category:福建語言](../Category/福建語言.md "wikilink")
[Category:閩北語](../Category/閩北語.md "wikilink")

1.  [福建省情综述 -
    历史文化](http://www.fjsq.gov.cn/ReadNews.asp?NewsID=2908&BigClassName=%CA%A1%C7%E9%D7%DB%CA%F6&SmallClassName=%CA%A1%C7%E9%D7%DB%CA%F6&SpecialID=0)

2.  侯精一主編，《現代漢語方言概論》，上海教育出版社2002年出版，208\~209頁

3.  徐晓望，福建通史，福建人民出版社，2006年

4.
5.  [《福建省志·方言志》概　　述
    一、福建方言的形成](http://www.fjsq.gov.cn/ShowText.asp?ToBook=196&index=7&)

6.
7.  [福建概览
    方言](http://www.fjsq.gov.cn/ReadNews.asp?NewsID=4607&BigClassName=福建概览&SmallClassName=福建概览&SpecialID=0&Style=2)

8.  《建甌方言詞典·引論》，第4頁

9.  [建瓯县志·第三十六篇·第一节](http://www.fjsq.gov.cn/showtext.asp?ToBook=3149&index=1664&)　声韵调

10. [建阳县志·第三十一篇·方言](http://www.fjsq.gov.cn/showtext.asp?ToBook=3206&index=1475&)

11. 《建甌方言詞典·引論》，第7頁