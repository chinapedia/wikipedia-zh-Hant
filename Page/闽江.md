**闽江**（；），是[中国](../Page/中国.md "wikilink")[福建省境内的最大河流](../Page/福建省.md "wikilink")。全长577公里，流域面积60992平方公里，约占全省面积一半。

## 流域

[River_Min,_Fukien_province,_China_Wellcome_L0055951.jpg](https://zh.wikipedia.org/wiki/File:River_Min,_Fukien_province,_China_Wellcome_L0055951.jpg "fig:River_Min,_Fukien_province,_China_Wellcome_L0055951.jpg")
[River_Min,_Fukien_province,_China._Wellcome_L0055865.jpg](https://zh.wikipedia.org/wiki/File:River_Min,_Fukien_province,_China._Wellcome_L0055865.jpg "fig:River_Min,_Fukien_province,_China._Wellcome_L0055865.jpg")
闽江上游有三源：

  - 北源[建溪](../Page/建溪.md "wikilink")，出自[武夷山与](../Page/武夷山脉.md "wikilink")[仙霞岭接界处的三尖峰西麓](../Page/仙霞岭.md "wikilink")，称[南浦溪](../Page/南浦溪.md "wikilink")，至[建瓯丰乐与来自](../Page/建瓯.md "wikilink")[武夷山市的](../Page/武夷山市.md "wikilink")[崇阳溪相汇](../Page/崇阳溪.md "wikilink")，往南至建瓯有松溪汇入始称建溪。
  - 中源[富屯溪](../Page/富屯溪.md "wikilink")，出自[赣](../Page/江西省.md "wikilink")、[闽交界的](../Page/闽.md "wikilink")[光泽县交溪口](../Page/光泽县.md "wikilink")，经[邵武到](../Page/邵武.md "wikilink")[顺昌与](../Page/顺昌.md "wikilink")[金溪相汇](../Page/金溪_\(富屯溪\).md "wikilink")。
  - 南源[沙溪](../Page/沙溪.md "wikilink")，上游九龙溪，发源于福建省[宁化县](../Page/宁化县.md "wikilink")，经[清流](../Page/清流.md "wikilink")、[永安与西南来的支流相汇](../Page/永安市.md "wikilink")，经[三明](../Page/三明.md "wikilink")、[沙县至](../Page/沙县.md "wikilink")[南平的沙溪口](../Page/南平.md "wikilink")，与来自西北的富屯溪相汇，然后经[南平市与建溪汇合后始称闽江](../Page/南平.md "wikilink")。

闽江自西北向东南方向流经福建省中北部，在[福州](../Page/福州市.md "wikilink")[南台岛淮安附近分南北两支](../Page/南台岛.md "wikilink")（北仍称闽江或白龙江，南称乌龙江），至[罗星塔再度匯合](../Page/罗星塔.md "wikilink")，折向东北，在[马尾区](../Page/马尾区.md "wikilink")[亭江附近又分为两支](../Page/亭江镇.md "wikilink")（长门水道、梅花水道），绕[琅岐岛注入](../Page/琅岐岛.md "wikilink")[台湾海峡](../Page/台湾海峡.md "wikilink")。

支流有[尤溪](../Page/尤溪_\(河流\).md "wikilink")、[古田溪](../Page/古田溪.md "wikilink")、大樟溪等。上游干支流均循地质构造线或横切地质构造线、多直角相交成方格状水系。

## 水文

[1962-05_1962年_闽江旁的福建南平.jpg](https://zh.wikipedia.org/wiki/File:1962-05_1962年_闽江旁的福建南平.jpg "fig:1962-05_1962年_闽江旁的福建南平.jpg")\]\]
[1962-05_1962年_闽江梯田.jpg](https://zh.wikipedia.org/wiki/File:1962-05_1962年_闽江梯田.jpg "fig:1962-05_1962年_闽江梯田.jpg")
闽江流域降水丰沛，水量与[黄河流域的水量相近](../Page/黄河.md "wikilink")，居全国第七位。流域年径流总量达623.70亿立方米，径流年际变化比较稳定，为重要水运通道。闽江源短流急，平均约三年就要发生一次超2万立方米每秒的较大洪水。中、上游滩多水急，水力资源丰富，理论蕴藏量641.8万千瓦，占全省河流水力资源理论蕴藏量的60%。可开发水力装机容量约468万千瓦。目前闽江流域已建成大中型水电站
23个，装机容量达316万千瓦。其中[水口水电站装机容量](../Page/水口水电站.md "wikilink")140万千瓦，为[华东地区最大的常规电站](../Page/华东.md "wikilink")。

闽江[洪水就暴雨成因而论](../Page/洪水.md "wikilink")，主要有[梅雨型和](../Page/梅雨.md "wikilink")[台风雨型两种](../Page/台风.md "wikilink")。梅雨型洪水是中纬度天气系统和低纬度天气系统相互作用的结果，主要是由[锋面暴雨所形成的](../Page/锋面.md "wikilink")，出现时间一般是4～6月，谓之前汛期洪水。台风雨型洪水是由台风天气系统的暴雨所形成的，出现时间多在7～9月，谓之后汛期洪水。

## 桥梁和隧道

闽江上位于福州的桥梁有[淮安大桥](../Page/淮安大桥_\(福州\).md "wikilink")、[洪山桥](../Page/洪山大桥_\(福州\).md "wikilink")、[金山大桥](../Page/金山大桥.md "wikilink")、[尤溪洲大桥](../Page/尤溪洲大桥.md "wikilink")、[三县洲大桥](../Page/三县洲大桥.md "wikilink")、[解放大桥](../Page/解放大桥_\(福州\).md "wikilink")（闽江一桥）、[闽江大桥](../Page/闽江大桥.md "wikilink")（闽江二桥）、[鳌峰大桥](../Page/鳌峰大桥.md "wikilink")（闽江三桥）、[鼓山大桥](../Page/鼓山大桥.md "wikilink")、[魁岐大桥](../Page/魁岐大桥.md "wikilink")、[福厦铁路闽江特大桥](../Page/福厦铁路闽江特大桥.md "wikilink")、[马尾大桥](../Page/马尾大桥.md "wikilink")、[青洲大桥](../Page/青洲大桥.md "wikilink")、[琅岐闽江大桥](../Page/琅岐闽江大桥.md "wikilink")、[长门大桥等](../Page/长门大桥.md "wikilink")。

[福州地铁1号线的过江隧道位于解放大桥和闽江大桥之间](../Page/福州地铁1号线.md "wikilink")。

## 支流

**南平以上**

  -
    沙溪、富屯溪、[崇阳溪](../Page/崇阳溪.md "wikilink")、[麻阳溪](../Page/麻阳溪.md "wikilink")、[南浦溪](../Page/南浦溪.md "wikilink")、[松溪](../Page/松溪_\(河流\).md "wikilink")、建溪。

**南平以下**

  -
    尤溪、古田溪、[梅溪](../Page/梅溪.md "wikilink")、[大樟溪](../Page/大樟溪.md "wikilink")。

## 沿岸城市

闽江流经福建省的35个县市，沿岸主要城市有：邵武市、武夷山市、[建阳市](../Page/建阳市.md "wikilink")、建瓯市、[永安市](../Page/永安市.md "wikilink")、三明市、南平市、福州市、[长乐市](../Page/长乐市.md "wikilink")。

## 題詠

<poem> 自閩海入閩江作（清•[洪繻](../Page/洪棄生.md "wikilink")）　 海天一色雲瞢騰，雲開何乃見長城！
長城非城千山青，中有山門五虎橫。 山門蕩蕩連海門，千迴百折長江奔。 中流江峽成海峽，束縛蛟龍留潮痕。
潮來潮去山重疊，錢塘潮水不足論。 舟人金牌長門裏，重重鎖鑰江海水。
宛轉亭頭又館頭，羅星塔山連雲起。 我自東北歷東南，港門無此青巉巖。
大沽黃浦劇深通，平原兩岸蔑峰嵐。 山東海港有山險，長流難得長山掩。 江心隨處況有磯，豈比金焦山兩點！
堪痛甲申邊釁開，如此江山釀禍胎。 兵備空傳藏艦浦，洋氛竟及釣龍臺！ 於今江面澄如練，馬頭馬尾滄桑變。
不堪迴首念昔遊，閩中山水依稀見。 </poem>

## 参看

  - [闽江公园](../Page/闽江公园.md "wikilink")

## 外部链接

  - [闽江流域介绍（太湖网）](https://web.archive.org/web/20070927185149/http://www.tba.gov.cn/lygk/05.asp?s=115)
  - [闽江下游防汛信息网](https://web.archive.org/web/20050309215801/http://www.minriver.gov.cn/)
  - [福建省水利信息网](https://web.archive.org/web/20070608072855/http://www.fjwater.gov.cn/ztbd/mj1.asp)
  - [福建水口水电站](http://online.hhu.edu.cn/MultiMediaLesson/ShuiLiShuiDianGongChengXueYuan/ShuiLiGongChengShiGong/Project/shuikou/shuikou.htm)

[\*](../Category/闽江.md "wikilink")