**阎立本**（），[雍州](../Page/雍州.md "wikilink")[万年县](../Page/万年县_\(北周\).md "wikilink")（今[陕西](../Page/陕西.md "wikilink")[临潼](../Page/临潼.md "wikilink")）人。隋朝画家[阎毗之子](../Page/阎毗.md "wikilink")，[阎立德之弟](../Page/阎立德.md "wikilink")，母亲是北周武帝宇文邕的女儿[清都公主](../Page/清都公主.md "wikilink")。[唐代著名](../Page/唐代.md "wikilink")[画家](../Page/画家.md "wikilink")、官员，官至[右相/中书令](../Page/中书令.md "wikilink")（宰相），谥**文贞**。

## 生平

[缩略图](https://zh.wikipedia.org/wiki/File:Yushan_Yan_Liben_Mu_2018.01.03_13-58-56.jpg "fig:缩略图")\]\]
[貞觀年間任](../Page/贞观_\(唐太宗\).md "wikilink")[主爵郎中](../Page/主爵郎中.md "wikilink")、[刑部侍郎](../Page/刑部侍郎.md "wikilink")、将作少监；[显庆初年升](../Page/显庆.md "wikilink")[工部尚书](../Page/工部尚书.md "wikilink")。任工部尚书时，曾兼任[河南道](../Page/河南道.md "wikilink")[黜陟使](../Page/黜陟使.md "wikilink")。其时，推荐[狄仁杰作了](../Page/狄仁杰.md "wikilink")[并州都督府](../Page/并州.md "wikilink")[法曹](../Page/法曹.md "wikilink")\[1\]。[總章元年](../Page/總章.md "wikilink")（668年），以司平太常伯（[工部尚书](../Page/工部尚书.md "wikilink")）拜[右相](../Page/右相.md "wikilink")、博陵县男。[咸亨元年](../Page/咸亨.md "wikilink")（670年），官职恢复旧名，为中书令。

## 绘画

他父亲阎毗和兄长阎立德都善长于绘画、工艺、建筑，阎立本亦秉承其家学，并师法[张僧繇](../Page/张僧繇.md "wikilink")、郑法士，而能“变古通今”，传说他曾见僧繇画，寝卧对之，十日不能去。他善画人物、车马、台阁，尤擅長於肖像画与歷史人物画。他的绘画，线条刚劲有力，神采如生，色彩古雅沉着，筆觸較[顧愷之細緻](../Page/顧愷之.md "wikilink")，人物神态刻画细致，其作品倍受当世推重，被时人列为“神品”。曾為[唐太宗畫](../Page/唐太宗.md "wikilink")〈[秦府十八學士](../Page/十八學士.md "wikilink")〉、〈[凌煙閣功臣二十四人圖](../Page/凌煙閣功臣二十四人圖.md "wikilink")〉等，為當時稱譽。今有〈[古帝王图](../Page/古帝王图.md "wikilink")〉、〈[职贡图](../Page/职贡图.md "wikilink")〉、〈[步辇图](../Page/步辇图.md "wikilink")〉、〈[萧翼赚兰亭图](../Page/萧翼赚兰亭图.md "wikilink")〉等相傳作品與宋摹本传世。

〈步辇图〉贞观十五年（641）唐太宗下嫁文成公主与吐蕃王松赞干布联姻事件。描绘唐太宗坐在步辇上接见迎亲使者禄东赞。重人物个性描绘，劲健的线条加以深沉的设色，多使朱砂、石绿，有时还使用金银等贵重矿物质颜料。动态较拘谨，重面部的刻画〈古帝王图〉描绘历史上十三位君王，作为"兴废之戒"。

### 画作\[2\]

  - 《[秦府十八學士](../Page/十八學士.md "wikilink")》
  - 《[凌煙閣功臣二十四人圖](../Page/凌煙閣功臣二十四人圖.md "wikilink")》
  - 《[古帝王图](../Page/古帝王图.md "wikilink")》
  - 《[职贡图](../Page/职贡图.md "wikilink")》
  - 《[步辇图](../Page/步辇图.md "wikilink")》
  - 《[萧翼赚兰亭图](../Page/萧翼赚兰亭图.md "wikilink")》
  - 《[度人经](../Page/度人经.md "wikilink")》（[褚遂良书字](../Page/褚遂良.md "wikilink")，阎立本画图）
  - 《[昭陵六骏](../Page/昭陵六骏.md "wikilink")》
  - 《[杨枝观音](../Page/杨枝观音碑.md "wikilink")》\[3\]

{{-}}

## 评价

阎立本雖因善繪事而貴為右相，卻無宰相器宇，缺乏政治才幹，[姜恪因战功升为左相](../Page/姜恪.md "wikilink")，时人评论说：“左相宣威沙漠，右相驰誉丹青。”

## 注释

## 参考资料

  - 《[旧唐书](../Page/旧唐书.md "wikilink")·卷七十七·列传第二十七》阎立本传
  - 《[新唐书](../Page/新唐书.md "wikilink")·卷一百·列传第二十五》阎立本传

[Category:唐朝畫家](../Category/唐朝畫家.md "wikilink")
[Category:唐朝詩人](../Category/唐朝詩人.md "wikilink")
[Category:唐朝吏部郎中](../Category/唐朝吏部郎中.md "wikilink")
[Category:唐朝刑部侍郎](../Category/唐朝刑部侍郎.md "wikilink")
[Category:唐朝将作少监](../Category/唐朝将作少监.md "wikilink")
[Category:唐朝将作大匠](../Category/唐朝将作大匠.md "wikilink")
[Category:唐朝工部尚书](../Category/唐朝工部尚书.md "wikilink")
[Category:黜陡使](../Category/黜陡使.md "wikilink")
[Category:唐朝中书令](../Category/唐朝中书令.md "wikilink")
[Category:唐朝县男](../Category/唐朝县男.md "wikilink")
[Y阎](../Category/西安人.md "wikilink")
[L立](../Category/阎姓.md "wikilink")
[Category:諡文貞](../Category/諡文貞.md "wikilink")

1.  《[旧唐书](../Page/旧唐书.md "wikilink")·卷八十九·列传第三十九》狄仁杰，字怀英……时工部尚书阎立本为河南道黜陟使，仁杰为吏人诬告，立本见而谢曰：“仲尼云：‘观过知仁矣’足下可谓海曲之明珠，东南之遗宝。”荐授并州都督府法曹。
2.  [隋唐 -
    閻立本](http://vr.theatre.ntu.edu.tw/fineart/painter-ch/yanliben/yanliben.htm)
3.  [杨枝观音碑](http://www.putuoshan.gov.cn/Detail/32/104070.html)