**阮奇山**（1887年-1956年），享壽69歲，隨父居[佛山朝觀里](../Page/佛山.md "wikilink")，其父[阮寵明在](../Page/阮寵明.md "wikilink")[民國初期從事](../Page/中華民國.md "wikilink")[化工顏料業](../Page/化學工業.md "wikilink")，於佛山永安路開店。因阮奇山排行第五，人稱「**阮老楂**」。小時候，阮父以重金禮聘[霍保全教授阮家兄弟](../Page/霍保全.md "wikilink")[蛇形拳術](../Page/蛇拳.md "wikilink")。先後有廣西欽州詠春名家[郝寶全](../Page/郝寶全.md "wikilink")，後隨年老的[馮少青學習](../Page/馮少青.md "wikilink")[紅船永春拳](../Page/紅船永春拳.md "wikilink")。可惜只有短暫時間。教授[阮奇山和其兄](../Page/阮奇山.md "wikilink")[阮濟雲](../Page/阮濟雲.md "wikilink")。[嚴詠春夫婦以及](../Page/嚴詠春.md "wikilink")[梁贊曾盤桓在](../Page/梁贊.md "wikilink")[廣西參與](../Page/廣西.md "wikilink")[天地會的活動](../Page/天地會.md "wikilink")，故廣西也有[詠春拳流傳](../Page/詠春拳.md "wikilink")。在清末民初廣西的[詠春名家有兩個](../Page/詠春名家.md "wikilink")，一是「神拳」[羅晩恭](../Page/羅晩恭.md "wikilink")，另一個便是[郝寶全](../Page/郝寶全.md "wikilink")，據說郝寶全善使[蝴蝶雙刀](../Page/蝴蝶雙刀.md "wikilink")，當時人稱「單刀孫玉峰，雙刀郝寶全」，而[孫玉峰是上海精武會著名教練](../Page/孫玉峰.md "wikilink")，北派羅漢拳宗師，人稱「[七省刀王](../Page/七省刀王.md "wikilink")」，能與孫玉峰齊名的，郝寶全自然不是泛泛之輩。而另一個被請到阮
家做客卿的詠春名家[馮少青](../Page/馮少青.md "wikilink")，也是來頭不小。馮少青是佛山順德人，他的詠春拳師承紅船戲班高手[陸錦](../Page/陸錦.md "wikilink")（大花面錦），學成後投奔晩清名臣[路秉章做過旗下提刑按察司府總捕頭](../Page/路秉章.md "wikilink")，阮家伺奉馮少青頤養天年直至七八歲去世。阮奇山深得兩廣詠春名家精髓併兼具所長，功夫自成一派。

1939年[日本侵華時](../Page/日本侵華.md "wikilink")，其兄[阮濟雲等移居](../Page/阮濟雲.md "wikilink")[越南](../Page/越南.md "wikilink")，阮奇山及[姚才的後人稱他們在此時追隨](../Page/姚才.md "wikilink")[吳仲素在](../Page/吳仲素.md "wikilink")[普君墟綫香街開設之醫館學習](../Page/普君墟.md "wikilink")[詠春拳](../Page/詠春拳.md "wikilink")。很多富家子弟，如有記[盲公餅](../Page/盲公餅.md "wikilink")[何兆初](../Page/何兆初.md "wikilink")，[李眾勝堂少東](../Page/李眾勝堂.md "wikilink")[李賜豪](../Page/李賜豪.md "wikilink")，英聚茶樓司庫[梁福初等也曾慕名求學](../Page/梁福初.md "wikilink")。

在廿、三十年代，阮奇山時常留連於姚才、姚林在[石路頭街開設之俱樂部](../Page/石路頭街.md "wikilink")（[大煙館](../Page/大煙.md "wikilink")）中。[葉問先生曾對一些徒弟提及阮奇山從來未有正式追隨吳仲素學習詠春拳](../Page/葉問.md "wikilink")。只是友好式偷偷學習，而吳仲素及葉問從來不在他們面前授徒。故所謂阮奇山或姚材詠春拳只有[小練頭及散式](../Page/小念頭.md "wikilink")。

## 承傳

其后傳至岑能,岑能在廣州弟子眾多.其徒弟有梁大釗,王錦,黃乃石,徐廣林,周賜喜,張勇,郭運平等。

## 歷史

由於葉問之父擔任過運送鴉片的船長，葉家房子在一次與當地居民的糾紛中被大火吞噬。阮寵明與葉問之父之間友誼甚篤，便將葉家接至阮家，並讓葉問跟隨幼子阮奇山練習他在吳仲素門下尚未學過的[黐手](../Page/黐手.md "wikilink")。雖然阮奇山起初為了葉問的吳仲素黐手式詠春並非同系而對這個提議頗有微言，但在他父親的堅持下還是將黐手教了葉問。阮奇山曾交代葉問勿將他所學的黐手輕易公諸於世，但葉問後來卻公開的運用黐手打敗了本武官師兄。這場比試也打出了「[詠春三雄](../Page/詠春三雄.md "wikilink")」：阮奇山、[葉問和](../Page/葉問.md "wikilink")[姚才的知名度](../Page/姚才.md "wikilink")。

## 《葉問前傳》之爭論

電影《[葉問前傳](../Page/葉問前傳.md "wikilink")》將阮奇山描寫為武功遜於葉問的小師弟。

## “詠春三雄”高下之爭

姚永强(姚才之孫)表示，當年在佛山，祖父姚才，葉問，和阮奇山三人並稱「詠春三雄」，三人齊名，武功不相上下。

[Category:中國武術家](../Category/中國武術家.md "wikilink")
[Category:阮姓](../Category/阮姓.md "wikilink")
[Category:1887年出生](../Category/1887年出生.md "wikilink")
[Category:1956年逝世](../Category/1956年逝世.md "wikilink")
[Category:佛山人](../Category/佛山人.md "wikilink")
[Category:詠春拳](../Category/詠春拳.md "wikilink")