**阳泉市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山西省下辖的](../Page/山西省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于山西省东部，[太行山脉西侧](../Page/太行山脉.md "wikilink")。山西东部经济，文化，政治中心。

## 历史

[旧石器时代](../Page/旧石器时代.md "wikilink")，今阳泉市辖区境内已有人类在此居住。平定县西北部的枣烟、大梁丁以及盂县西烟镇均曾发现过旧石器\[1\]。

[西周至](../Page/西周.md "wikilink")[春秋时期](../Page/春秋.md "wikilink")，阳泉属于[晋国](../Page/晋国.md "wikilink")。

[战国时期](../Page/战国.md "wikilink")，阳泉属于[赵国](../Page/赵国.md "wikilink")。

[秦统一中国后](../Page/秦朝.md "wikilink")，阳泉归太原郡管辖。

[西汉](../Page/西汉.md "wikilink")[建元元年](../Page/建元_\(西汉\).md "wikilink")（公元前140年），设上艾县，县治在今天的平定县张庄镇新城村，阳泉归辖于上艾县。

[唐](../Page/唐.md "wikilink")[天宝元年](../Page/天宝_\(唐朝\).md "wikilink")（742年），上艾县更名广阳县，县治迁至今[昔阳县广阳村](../Page/昔阳县.md "wikilink")。

[北宋](../Page/北宋.md "wikilink")[太平兴国四年](../Page/太平兴国.md "wikilink")（979年），广阳县更名为平定县，县治迁回平定县。

[金](../Page/金朝.md "wikilink")，平定县改称平定州。

1900年前，现在的阳泉市区是平定州境内的一个名叫“漾泉”的小山村，也有人认为，这里只是一片名为沙江口的荒滩。

1903年[石太铁路通车后](../Page/石太线.md "wikilink")，采矿业随之兴起。

[民国初年](../Page/民国.md "wikilink")，改平定州为平定县，阳泉归辖于平定县。

1936年，阳泉附近形成了3万余人的小镇。

1947年，中共设立阳泉市，但辖区不包括平定县和盂县。

1983年，[平定县](../Page/平定县.md "wikilink")、[盂县归辖于阳泉市](../Page/盂县.md "wikilink")。

## 地理

## 政治

### 现任领导

<table>
<caption>阳泉市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党阳泉市委员会.md" title="wikilink">中国共产党<br />
阳泉市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/阳泉市人民代表大会.md" title="wikilink">阳泉市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/阳泉市人民政府.md" title="wikilink">阳泉市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议阳泉市委员会.md" title="wikilink">中国人民政治协商会议<br />
阳泉市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/关建勋.md" title="wikilink">关建勋</a>[2]</p></td>
<td><p><a href="../Page/王旭明.md" title="wikilink">王旭明</a>[3]</p></td>
<td><p><a href="../Page/雷健坤.md" title="wikilink">雷健坤</a>（女）[4]</p></td>
<td><p><a href="../Page/杨永生.md" title="wikilink">杨永生</a>[5]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/山西省.md" title="wikilink">山西省</a><a href="../Page/临猗县.md" title="wikilink">临猗县</a></p></td>
<td><p>山西省<a href="../Page/临县.md" title="wikilink">临县</a></p></td>
<td><p>山西省<a href="../Page/大同市.md" title="wikilink">大同市</a></p></td>
<td><p>山西省<a href="../Page/临猗县.md" title="wikilink">临猗县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年2月</p></td>
<td><p>2017年3月</p></td>
<td><p>2018年1月</p></td>
<td><p>2017年3月</p></td>
</tr>
</tbody>
</table>

### 行政区划

阳泉市现辖3个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[城区](../Page/城区_\(阳泉市\).md "wikilink")、[矿区](../Page/矿区_\(阳泉市\).md "wikilink")、[郊区](../Page/郊区_\(阳泉市\).md "wikilink")
  - 县：[平定县](../Page/平定县.md "wikilink")、[盂县](../Page/盂县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>阳泉市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[6]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>140300</p></td>
</tr>
<tr class="odd">
<td><p>140302</p></td>
</tr>
<tr class="even">
<td><p>140303</p></td>
</tr>
<tr class="odd">
<td><p>140311</p></td>
</tr>
<tr class="even">
<td><p>140321</p></td>
</tr>
<tr class="odd">
<td><p>140322</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>阳泉市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[7]（2010年11月）</p></th>
<th><p>户籍人口[8]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>阳泉市</p></td>
<td><p>1368502</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>城　区</p></td>
<td><p>193106</p></td>
<td><p>14.11</p></td>
</tr>
<tr class="even">
<td><p>矿　区</p></td>
<td><p>242994</p></td>
<td><p>17.76</p></td>
</tr>
<tr class="odd">
<td><p>郊　区</p></td>
<td><p>286055</p></td>
<td><p>20.90</p></td>
</tr>
<tr class="even">
<td><p>平定县</p></td>
<td><p>335265</p></td>
<td><p>24.50</p></td>
</tr>
<tr class="odd">
<td><p>盂　县</p></td>
<td><p>311082</p></td>
<td><p>22.73</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")1368502人\[9\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加95386人，增长7.49%。其中，男性人口为710304人，占51.9%；女性人口为658198人，占48.1%。总人口性别比（以女性为100）为107.92。0－14岁人口为214449人，占15.67%；15－64岁人口为1044127人，占76.30%；65岁及以上人口为109926人，占8.03%。

### 方言

市区及平定县方言属于[晋语](../Page/晋语.md "wikilink")[大包片](../Page/大包片.md "wikilink")，盂县方言属于[晋语](../Page/晋语.md "wikilink")[并州片](../Page/并州片.md "wikilink")。但也有学者认为，阳泉方言整体属于并州片下的阳泉片（也称平辽小片）\[10\]。

## 物产

矿藏主要有[无烟煤](../Page/无烟煤.md "wikilink")、[铝矾土](../Page/铝矾土.md "wikilink")、[硫铁矿](../Page/硫铁矿.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、耐火粘土、[铁矿石](../Page/铁矿石.md "wikilink")、[大理石](../Page/大理石.md "wikilink")、[石膏](../Page/石膏.md "wikilink")、[石棉](../Page/石棉.md "wikilink")、[石英](../Page/石英.md "wikilink")、[云母](../Page/云母.md "wikilink")、[长石](../Page/长石.md "wikilink")、[硅石](../Page/硅石.md "wikilink")、[花岗石](../Page/花岗石.md "wikilink")、[蛭石](../Page/蛭石.md "wikilink")、[水晶等](../Page/水晶.md "wikilink")，还有[镍](../Page/镍.md "wikilink")、[铬](../Page/铬.md "wikilink")、[锑](../Page/锑.md "wikilink")、[铜](../Page/铜.md "wikilink")、[钠](../Page/钠.md "wikilink")、[磷等稀有元素](../Page/磷.md "wikilink")，阳泉人形象地以黑、白、黄矿为豪（黑煤，白铝矾土，黄硫铁矿）。

## 经济

[阳泉网通大厦.jpg](https://zh.wikipedia.org/wiki/File:阳泉网通大厦.jpg "fig:阳泉网通大厦.jpg")

  - 工业以煤炭开采、炼铁为主，兼有矿山机械、[电力](../Page/电力.md "wikilink")、[化工](../Page/化工.md "wikilink")、电子仪表、[纺织等行业](../Page/纺织.md "wikilink")。
  - 农村的乡镇工业成为全市经济的支柱。

## 交通

[石太复线贯穿该市](../Page/石太线.md "wikilink")，铁路支线四通八达，是[华北地区最大的铁路货运站](../Page/华北地区.md "wikilink")。公路网也很发达，[太旧高速从境内穿过](../Page/太旧高速.md "wikilink")。

## 名胜古迹

有唐代平阳公主率军驻守的[娘子关](../Page/娘子关.md "wikilink")、[陽泉關王廟](../Page/陽泉關王廟.md "wikilink")(陽泉關帝廟)、赵简子古城遗址、汉淮阴侯韩信驻军遗址、藥林寺、烈女祠、百团大战纪念碑、清凉寺、摩崖石刻、蒲台阁、梁家寨温泉、[藏山等](../Page/藏山.md "wikilink")。

## 文化教育

### 高等教育

  - [山西工程技術學院](../Page/山西工程技術學院.md "wikilink")
  - 阳泉师范高等专科学校
  - 阳泉职业技术学院

### 中等教育

  - 省级重点高中[阳泉一中](../Page/阳泉一中.md "wikilink")
  - 荫营中学
  - [阳泉十一中](../Page/阳泉十一中.md "wikilink")
  - [阳泉十五中](../Page/阳泉十五中.md "wikilink")

## 友好城市\[11\]

### 国际友好城市

  - [切斯特菲尔德市](../Page/切斯特菲尔德市.md "wikilink")（[切斯特菲爾德](../Page/切斯特菲爾德.md "wikilink")）

  - [华伦市](../Page/弗农山_\(纽约州\).md "wikilink")（[弗農山](../Page/弗農山_\(紐約州\).md "wikilink")）

### 国内友好城市

  - [天津市](../Page/天津市.md "wikilink")[塘沽区](../Page/塘沽区.md "wikilink")
  - [山东省](../Page/山东省.md "wikilink")[潍坊市](../Page/潍坊市.md "wikilink")
  - [山东省](../Page/山东省.md "wikilink")[淄博市](../Page/淄博市.md "wikilink")
  - [江苏省](../Page/江苏省.md "wikilink")[镇江市](../Page/镇江市.md "wikilink")
  - [河北省](../Page/河北省.md "wikilink")[石家庄市](../Page/石家庄市.md "wikilink")
  - [河北省](../Page/河北省.md "wikilink")[沧州市](../Page/沧州市.md "wikilink")
  - [四川省](../Page/四川省.md "wikilink")[德阳市](../Page/德阳市.md "wikilink")
  - [江苏省](../Page/江苏省.md "wikilink")[盐城市](../Page/盐城市.md "wikilink")
  - [浙江省](../Page/浙江省.md "wikilink")[湖州市](../Page/湖州市.md "wikilink")
  - [江苏省](../Page/江苏省.md "wikilink")[昆山市](../Page/昆山市.md "wikilink")
  - [四川省](../Page/四川省.md "wikilink")[乐山市](../Page/乐山市.md "wikilink")
  - [辽宁省](../Page/辽宁省.md "wikilink")[盘锦市](../Page/盘锦市.md "wikilink")
  - [湖南省](../Page/湖南省.md "wikilink")[岳阳市](../Page/岳阳市.md "wikilink")
  - [河北省](../Page/河北省.md "wikilink")[秦皇岛市](../Page/秦皇岛市.md "wikilink")
  - [四川省](../Page/四川省.md "wikilink")[广安市](../Page/广安市.md "wikilink")
  - [海南省](../Page/海南省.md "wikilink")[海口市](../Page/海口市.md "wikilink")
  - [江苏省](../Page/江苏省.md "wikilink")[徐州市](../Page/徐州市.md "wikilink")
  - [山东省](../Page/山东省.md "wikilink")[临沂市](../Page/临沂市.md "wikilink")

## 著名人物

阳泉著名人物有：

  - [百度董事长](../Page/百度.md "wikilink")[李彦宏](../Page/李彦宏.md "wikilink")
  - [三體
    (小說)作者](../Page/三體_\(小說\).md "wikilink")[劉慈欣](../Page/劉慈欣.md "wikilink")
  - [劉維開](../Page/劉維開.md "wikilink")，祖籍[山西省](../Page/山西省.md "wikilink")[盂縣](../Page/盂縣.md "wikilink")，中華民國[歷史學家](../Page/歷史學家.md "wikilink")，[国立政治大学教授](../Page/国立政治大学.md "wikilink")，專長[民國政治](../Page/民國.md "wikilink")、軍事、制度史，為[中华民國史研究權威之一](../Page/中华民國史.md "wikilink")。
  - [暴风影音董事长](../Page/暴风影音.md "wikilink")[冯鑫](../Page/冯鑫.md "wikilink")
  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")[公安部副部长](../Page/公安部.md "wikilink")[刘京](../Page/刘京_\(中国官员\).md "wikilink")
  - 原中華人民共和國[江西省省长](../Page/江西省.md "wikilink")[趙增益](../Page/趙增益.md "wikilink")

## 参考文献

{{-}}

[阳泉](../Category/阳泉.md "wikilink")
[Category:山西地级市](../Category/山西地级市.md "wikilink")
[晋](../Category/国家园林城市.md "wikilink")
[晋](../Category/中国大城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.