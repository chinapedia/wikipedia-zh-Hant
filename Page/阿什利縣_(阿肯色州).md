**阿什利县**（**Ashley County,
Arkansas**）是[美國](../Page/美國.md "wikilink")[阿肯色州東南部的一個縣](../Page/阿肯色州.md "wikilink")，南鄰[路易斯安那州](../Page/路易斯安那州.md "wikilink")。面積2,432平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口24,209人。縣治[漢堡](../Page/漢堡_\(阿肯色州\).md "wikilink")（Hamburg）。

成立於1848年11月30日。縣名紀念[參議員](../Page/美國參議院.md "wikilink")[切斯特·阿什利](../Page/切斯特·阿什利.md "wikilink")。本縣為一[禁酒的縣](../Page/禁酒.md "wikilink")。


[A](../Category/阿肯色州行政区划.md "wikilink")
[Category:1848年建立的聚居地](../Category/1848年建立的聚居地.md "wikilink")