**阿伯丁**（**Aberdeen**）是[美国](../Page/美国.md "wikilink")[爱达荷州](../Page/爱达荷州.md "wikilink")[宾厄姆县的一座城市](../Page/宾厄姆县_\(爱达荷州\).md "wikilink")。根据[美国2000年人口普查](../Page/美国2000年人口普查.md "wikilink")，共有人口1,840人。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:愛達荷州城市](../Category/愛達荷州城市.md "wikilink") [Category:宾厄姆县
(爱达荷州)](../Category/宾厄姆县_\(爱达荷州\).md "wikilink")

1.