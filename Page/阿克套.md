[Aktau_p.svg](https://zh.wikipedia.org/wiki/File:Aktau_p.svg "fig:Aktau_p.svg")
**阿克套**（）是[哈薩克斯坦西部](../Page/哈薩克斯坦.md "wikilink")、[裏海沿岸城市](../Page/裏海.md "wikilink")，也是[曼格斯套州首府](../Page/曼格斯套州.md "wikilink")。2006年人口約為175,000人。城名在[哈薩克語的意思是](../Page/哈薩克語.md "wikilink")「白色山地」的意思，因为这是从里海眺望城市的景色。
城市是因[石油而建立的](../Page/石油.md "wikilink")，由於起初是軍事重鎮，當地的街道沒有名字，要寄信到當地就改為填寫小區編號、大廈編號和單元號。

## 历史

古代有[斯基泰人活动](../Page/斯基泰人.md "wikilink")。這地方本是[土庫曼人活動的地方](../Page/土庫曼人.md "wikilink")，[諾蓋人](../Page/諾蓋人.md "wikilink")，[卡爾梅克人先後把他們逐出此地](../Page/卡爾梅克.md "wikilink")。
由于极端缺少淡水，当地从来没有居民点出现。

1958年，铀地质工作者在此安家，以当地海湾**Melovoye** () 命名.\[1\]
1961年作为铀矿冶，成为[保密行政区](../Page/保密行政区.md "wikilink")，称为**Guryev-20**
().\[2\]。1963年称阿克套镇。1964至1992年間被改名為**舍甫琴柯**以紀念[烏克蘭詩人](../Page/烏克蘭.md "wikilink")[塔拉斯·舍甫琴科在](../Page/塔拉斯·舍甫琴科.md "wikilink")1850-1857年政治流放到西北方100km的(Novopetrovskoye)。

一座钠冷[快中子增殖反应堆](../Page/快中子增殖反应堆.md "wikilink")1973年入役，生產[鈈元素](../Page/鈈.md "wikilink")，並為城市提供電力和[海水淡化](../Page/海水淡化.md "wikilink")。1999年停堆关闭.

## 地理与气候

位于[里海东岸](../Page/里海.md "wikilink")[曼格斯拉克半岛](../Page/曼格斯拉克半岛.md "wikilink")。位于[裏海盆地](../Page/裏海盆地.md "wikilink")，当地海拔实际上在海平面以下。寒冷[沙漠气候](../Page/沙漠气候.md "wikilink")。

## 姐妹城市

  - [波季](../Page/波季.md "wikilink")

  - [戈爾甘](../Page/戈爾甘.md "wikilink")

[tt:Актау (Баймак
районы)](../Page/tt:Актау_\(Баймак_районы\).md "wikilink")

[Category:哈薩克斯坦城市](../Category/哈薩克斯坦城市.md "wikilink")

1.

2.