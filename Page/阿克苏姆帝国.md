**阿克苏姆**是公元前或公元初在东北[非洲的国家](../Page/非洲.md "wikilink")，首都为[阿克蘇姆城](../Page/阿克蘇姆.md "wikilink")（今属[埃塞俄比亚的](../Page/埃塞俄比亚.md "wikilink")[提格雷省](../Page/提格雷省.md "wikilink")）。公元1世纪，一位[古希腊商人写的](../Page/古希腊.md "wikilink")《[红海环航记](../Page/红海环航记.md "wikilink")》裡首次提到它。国王[埃扎納在位时](../Page/阿克蘇姆的埃扎納.md "wikilink")（320年－360年），征服埃塞俄比亚高原、[麦罗埃和南阿拉伯](../Page/麦罗埃.md "wikilink")，与[罗马帝国皇帝](../Page/罗马帝国.md "wikilink")[君士坦丁缔结同盟条约](../Page/君士坦丁.md "wikilink")，国势极盛，被称为“众王之王”。埃扎纳还皈依[基督教](../Page/基督教.md "wikilink")，推行新拼音文字，使阿克苏姆成为非洲第一个以基督教为国教的国家。

[阿杜利斯是阿克苏姆最重要的贸易中心](../Page/阿杜利斯.md "wikilink")：它地近[曼德海峡](../Page/曼德海峡.md "wikilink")，控制[红海的航运](../Page/红海.md "wikilink")；西距[阿特巴拉河不远](../Page/阿特巴拉河.md "wikilink")，沿河北上可至[尼罗河中游](../Page/尼罗河.md "wikilink")，所以又是内陆贸易的集散地。来自[意大利](../Page/意大利.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[拜占廷](../Page/拜占廷.md "wikilink")、[印度的铁器](../Page/印度.md "wikilink")、棉布、酒和装饰品，源源运进阿杜利斯，运出的是[黄金](../Page/黄金.md "wikilink")、[象牙](../Page/象牙.md "wikilink")、[香料](../Page/香料.md "wikilink")、[犀角和](../Page/犀角.md "wikilink")[玳瑁](../Page/玳瑁_\(宝石\).md "wikilink")。使用铸有国王头像的金、银、铜币。阿克苏姆人民修建梯田和灌溉工程，种植小麦、葡萄、或放牧牛羊，驯猎野象。技术高超的匠人在山巅开凿（不是砌造）教堂和碉堡，堪称一绝。

六世纪中叶，波斯[萨珊王朝攻占](../Page/萨珊王朝.md "wikilink")[也门](../Page/也门.md "wikilink")，阿克苏姆被挤出南阿拉伯。七世纪初叶阿拉伯帝国兴起，垄断从印度到地中海的商路，阿克苏姆衰落。八世纪，阿杜利斯被黄沙淹没。九世纪起，埃塞俄比亚政治中心南移今首都[亚的斯亚贝巴一带](../Page/亚的斯亚贝巴.md "wikilink")。十世纪，阿克苏姆帝国灭亡。

[Ethio_w4.jpg](https://zh.wikipedia.org/wiki/File:Ethio_w4.jpg "fig:Ethio_w4.jpg")[雙耳瓶](../Page/雙耳瓶.md "wikilink")\]\]

[Category:衣索比亞歷史](../Category/衣索比亞歷史.md "wikilink")
[A](../Category/已不存在的非洲君主國.md "wikilink")
[Category:已不存在的帝國](../Category/已不存在的帝國.md "wikilink")