[Al_aqsa_moschee_2.jpg](https://zh.wikipedia.org/wiki/File:Al_aqsa_moschee_2.jpg "fig:Al_aqsa_moschee_2.jpg")

**阿克萨清真寺**（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：，，，[英語](../Page/英語.md "wikilink")：），位于[耶路撒冷旧城](../Page/耶路撒冷旧城.md "wikilink")[聖殿山](../Page/聖殿山.md "wikilink")。“阿克萨”在[阿拉伯语中意为](../Page/阿拉伯语.md "wikilink")“遥远”，故本寺又名**远寺**。虽然《[古蘭经](../Page/古蘭经.md "wikilink")》第17章第7节指[禁寺上有犹太人的](../Page/禁寺.md "wikilink")[圣殿](../Page/耶路撒冷圣殿.md "wikilink")（我说：『如果你们行善，那末，你们是为自己而行善，如果你们作恶，那么，你们是为自己而作恶。』当第二次作乱的约期来临的时候，（我又派遣他们），以便他们使你们变成为愁眉苦脸的，以便他们像头一次那样再入禁寺，以便他们把自己所占领的地方加以摧毁。）

阿克萨紧邻著名的[圆顶清真寺](../Page/圆顶清真寺.md "wikilink")（又称：金顶清真寺、岩石清真寺、登霄石清真寺）相传为[穆罕默德](../Page/穆罕默德.md "wikilink")[夜行登霄之处](../Page/夜行登霄.md "wikilink")。

## 历史

该寺由[倭马亚王朝第](../Page/倭马亚王朝.md "wikilink")5代[哈里发](../Page/哈里发.md "wikilink")[阿布杜勒·马利克及其子在公元](../Page/阿布杜勒·马利克.md "wikilink")705年至709年间建造在原圣殿教堂的残基之上。780年毁于地震，932年由阿巴斯王朝第19任哈里发重建。11世纪加盖了圆顶，改建后的清真寺高至88米。1099年，该寺在[第一次十字军东征中被入侵的十字军改造成教堂](../Page/第一次十字军东征.md "wikilink")、兵器库和兵营。1187年耶路撒冷被[萨拉丁易主后又进行了修复](../Page/萨拉丁.md "wikilink")。

2017年7月14日，3名以色列警察在[圣殿山外遭以色列籍阿拉伯人枪击](../Page/圣殿山.md "wikilink")，其中2人身亡，袭击者随后逃到圣殿山。以色列警方在事发后封锁了圣殿山上的阿克萨清真寺，当天正是周五[主麻日](../Page/主麻.md "wikilink")，这是半世纪来[穆斯林第一次不被允许周五在阿克萨清真寺祷告](../Page/穆斯林.md "wikilink")。2017年7月16日下午，以色列警方重开[耶路撒冷老城内之前遭关闭的商铺及被封锁的交通](../Page/耶路撒冷.md "wikilink")，并重开阿克萨清真寺，但在阿克萨清真寺入口处新设的金属探测器及安全摄像头遭穆斯林抵制。2017年7月18日，据“阿拉伯新闻报”网站援引[阿拉伯语媒体Elaph的报道称](../Page/阿拉伯语.md "wikilink")，[沙特阿拉伯国王](../Page/沙特阿拉伯国王.md "wikilink")[萨勒曼经私人途径通过](../Page/萨勒曼·本·阿卜杜勒-阿齐兹·阿勒沙特.md "wikilink")[美国高级别官员斡旋获得了](../Page/美国.md "wikilink")[以色列总理](../Page/以色列总理.md "wikilink")[内塔尼亚胡](../Page/本雅明·内塔尼亚胡.md "wikilink")“重开阿克萨清真寺”的承诺。但2017年7月19日晚，以色列警方再度关闭阿克萨清真寺。正在[中国进行国事访问的](../Page/中華人民共和國.md "wikilink")[巴勒斯坦国总统](../Page/巴勒斯坦国总统.md "wikilink")[阿巴斯决定提前结束访问回国](../Page/马哈茂德·阿巴斯.md "wikilink")\[1\]。

## 布局

礼拜大殿长90米，高8米，宽36米，内有53根大理石圆柱和49根方柱。

## 参考文献

## 外部链接

  - [Noble Sanctuary: Al-Aqsa
    Mosque](http://www.noblesanctuary.com/AQSAMosque.html)
  - [Muslim WikiPedia Al-Aqsa
    Mosque](https://web.archive.org/web/20070312052248/http://www.muslimwikipedia.com/mw/index.php/Al-Aqsa_Mosque/)
  - [Report of the 1969
    conflict](https://web.archive.org/web/20070524054119/http://www.camera.org/index.asp?x_article=190&x_context=2)
  - [Al-Aqsa Mosque Architectural
    Review](http://www.islamicarchitecture.org/architecture/alaqsamosque.html)
  - [360° view of the inside of the Mosque by Visual
    Dhikr](https://web.archive.org/web/20070429132830/http://www.visualdhikr.com/extra/aqsa_pano.htm)
  - [Jerusalem Photos
    Archive](http://www.jerusalemshots.com/img_en23_1645p1.html):
    Al-Aqsa Mosque
  - [History of
    Al-Aqsa](https://web.archive.org/web/20070329101325/http://www.hweb.org.uk/content/view/4/3/):
    hWeb
  - [Site surrounding the controversy over the excavations made by the
    Waqf](http://www.har-habayt.org/index.html)

## 参见

  - [夜行登霄](../Page/夜行登霄.md "wikilink")
  - [圣殿山](../Page/圣殿山.md "wikilink")

[Category:耶路撒冷清真寺](../Category/耶路撒冷清真寺.md "wikilink")
[Category:圣殿山](../Category/圣殿山.md "wikilink")

1.