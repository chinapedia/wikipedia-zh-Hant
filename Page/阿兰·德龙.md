**阿兰·德龙**（[法语](../Page/法语.md "wikilink")：****，），出生于[法國](../Page/法國.md "wikilink")[上塞纳省](../Page/上塞纳省.md "wikilink")，1999年取得[瑞士](../Page/瑞士.md "wikilink")[國籍](../Page/國籍.md "wikilink")，他是六、七零年代最受欢迎的法国演员，迄今依舊是[美男子的代名詞](../Page/美男子.md "wikilink")。

## 早年生活

阿兰·德龙的父母在他刚出生不久便离婚，自己被寄养在养父母身边。他在6所不同的学校逃学，最终在养父母去世后被送到一所寄宿制学校。不久在17岁时参军，在[第一次印度支那战争作为一名](../Page/第一次印度支那战争.md "wikilink")[伞兵作战](../Page/伞兵.md "wikilink")。1956年他回到法国，在一家名叫的巴黎蔬菜市场工作，距离演员学校非常近。

[Alain_&_Anouchka_Delon_Cannes_2010.jpg](https://zh.wikipedia.org/wiki/File:Alain_&_Anouchka_Delon_Cannes_2010.jpg "fig:Alain_&_Anouchka_Delon_Cannes_2010.jpg")

## 從影生涯

1959年亞蘭德倫藉由[雷尼·克萊曼](../Page/雷尼·克萊曼.md "wikilink")（René Clément）執導的
《[陽光普照](../Page/陽光普照.md "wikilink")》（Plein
soleil）及[盧奇諾·維斯孔蒂](../Page/盧奇諾·維斯孔蒂.md "wikilink")（Luchino
Visconti）的《[洛可兄弟](../Page/洛可兄弟.md "wikilink")》（Rocco e i suoi
fratelli）而聲名大噪，成為法國影壇巨星。阿兰·德龙在1975年上映的法義合拍影片“蘇洛“飾演蘇洛，在世界影坛反响平平，但在1979年中国上映后却大受欢迎，家喻户晓。部分原因是，这是中国当时少数引入的西方电影之一，也因上海译制厂的出色配音。英俊帅气、身手矫健的阿兰·德龙也凭借此片在中国赢得了无数粉丝。80年代中期之後阿兰·德龙淡出銀幕，將重心發展以他的名字為品牌的化妝品等商品。90年代復出，1997年與[让-保罗·贝尔蒙多合演](../Page/让-保罗·贝尔蒙多.md "wikilink")《[1/2的機會](../Page/1/2的機會.md "wikilink")》。2000年之後他將重心轉往電視圈，鮮少有電影作品演出。

## 外部链接

  - [alaindelon.ch](https://web.archive.org/web/20120328050314/http://www.alaindelon.ch/),
    his official website

  -
[Category:1935年出生](../Category/1935年出生.md "wikilink")
[Category:法国电影演员](../Category/法国电影演员.md "wikilink")
[Category:20世纪演员](../Category/20世纪演员.md "wikilink")
[Category:凱撒電影獎獲得者](../Category/凱撒電影獎獲得者.md "wikilink")
[Category:法国舞台演员](../Category/法国舞台演员.md "wikilink")
[Category:法国电视演员](../Category/法国电视演员.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:瑞士演员](../Category/瑞士演员.md "wikilink")