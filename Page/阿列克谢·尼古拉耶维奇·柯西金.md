**阿列克谢·尼古拉耶维奇·柯西金**（，），[苏联政治人物](../Page/苏联.md "wikilink")，1964年10月15日至1980年10月23日任[苏联部长会议主席](../Page/苏联部长会议主席.md "wikilink")，任期长达16年，是[苏联历史上任期最长的总理](../Page/苏联历史.md "wikilink")，也是苏共中央总书记[勃列日涅夫任内最重要的副手之一](../Page/勃列日涅夫.md "wikilink")。

柯西金1919年加入[苏联红军](../Page/苏联红军.md "wikilink")，参加了[苏联国内战争](../Page/苏联国内战争.md "wikilink")。战后，柯西金被送往[列宁格勒接受培训](../Page/列宁格勒.md "wikilink")，其后赴[西伯利亚工作](../Page/西伯利亚.md "wikilink")，1927年加入[苏联共产党](../Page/苏联共产党.md "wikilink")。1930年代，柯西金进入学习，毕业后成为当地一家纺织厂厂长。1938年，柯西金出任列宁格勒人民委员会主席（相当于市长）。

1939年，柯西金进入[苏联共产党中央委员会](../Page/苏联共产党中央委员会.md "wikilink")，并担任苏联中央政府纺织人民委员（相当于部长）。1940年，柯西金被任命为人民委员会（即[苏联部长会议的前身](../Page/苏联部长会议.md "wikilink")）副主席，主管消费品生产工作。1943年起，他还兼任[俄罗斯苏维埃联邦社会主义共和国部长会议主席](../Page/俄罗斯苏维埃联邦社会主义共和国.md "wikilink")。

1946年，柯西金成为[苏联共产党中央政治局候补委员](../Page/苏联共产党中央政治局.md "wikilink")，1948年成为政治局正式委员。

1964年，[赫鲁晓夫下台后](../Page/赫鲁晓夫.md "wikilink")，[勃列日涅夫继任](../Page/勃列日涅夫.md "wikilink")[苏共中央第一书记](../Page/苏共中央第一书记.md "wikilink")，而柯西金接任[苏联部长会议主席](../Page/苏联部长会议主席.md "wikilink")（相当于总理）一职，直到1980年去世前夕方才卸任。

## 參考文獻

  - *Андриянов В. И.* Косыгин. Серия: [Жизнь замечательных
    людей](../Page/Жизнь_замечательных_людей.md "wikilink"): Серия
    биографий. — М.: Молодая гвардия, 2003. — 368 с. — ISBN
    5-235-02623-3.

  - *Гвишиани А. Д.* Феномен Косыгина. Записки внука. Мнения
    современников. — М.: Фонд культуры «Екатерина», 2004. —
    312 с. — ISBN 5-86863-191-9.

  - [Большая Советская энциклопедия](../Page/苏联大百科全书.md "wikilink")

  - Dicţionarul Enciclopedic Român,Editura Politică, Bucureşti, 1965,
    vol.3, p. 37

  - Енчиклопедия Советикэ Молдовеняскэ, вол.3. Кишинэу, п.462, 1972
    (Enciclopedia Sovietică Moldovenească,vol.3, Chişinău, p. 462, 1972)


  - Le Robert Micro Poche, Dicorobest Inc, 1990.

  - Большой Российский Энциклопедический словарь. Золотой фонд.
    Репринтное издание, М. Научное изд-во Большая Российская
    Энциклопедия, 2009.

  - Энциклопедия АИФ. Издание АСТ, Астрель, 2010—2011.

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 參見

  -
## 外部連結

  -
  - [М. Антонов. Цена реформаторства // Красная звезда, 25 Апреля 2007
    года](https://web.archive.org/web/20080325073546/http://www.redstar.ru/2007/04/25_04/5_05.html)

  - [Михаил АНТОНОВ. РЕФОРМА ЛИБЕРМАНА — КОСЫГИНА — «РЕВОЛЮЦИЯ
    ОБЫВАТЕЛЕЙ»](http://m-antonov.chat.ru/capital/ant_glava_7.htm)

  - [Судьба наркома: Неизвестный премьер // КТО есть КТО,
    № 1 1998](https://web.archive.org/web/20050119075906/http://whoiswho.ru/russian/Password/journals/11998/KOSYGINR.HTM)

  - [О драматических страницах жизни советского премьера рассказывает
    его внучка Т. Гвишиани-Косыгина //
    РГ](http://www.rg.ru/2003/08/24/NeizvestnyjKosygin.html)

  - [Алексей Гвишиани: «Не надо жалеть Косыгина\!» // Правда.
    Ру](http://www.pravda.ru/society/fashion/models/47012-kosygin-0)

  - [Тайны истории: Премьер СССР Алексей
    Косыгин](http://www.lsg.ru/index.php?page=histore&art=1587)

  - [О «Косыгинских реформах» (выдержки из книги
    Андриянова)](https://web.archive.org/web/20070527074112/http://vif2ne.ru/nvz/forum/archive/179/179300.htm)

  - [О Косыгинской автомобильной политике (выдержки из книги
    Андриянова)](https://web.archive.org/web/20070926225423/http://vif2ne.ru/nvz/forum/archive/179/179302.htm)

  - [Социальные аспекты «косыгинской» реформы середины 1960—70-х
    годов.](http://www.hist.msu.ru/Labs/Ecohist/OB8/slavkina.htm)

  - [статья «Алексей Косыгин: политик или технократ», «Российские
    вести»](http://rosvesty.ru/1947/interes/5389-aleksei-kosigin-politik-ili-tehnokrat/)

{{-}}

[Category:社會主義勞動英雄](../Category/社會主義勞動英雄.md "wikilink")
[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:十月革命勳章獲得者](../Category/十月革命勳章獲得者.md "wikilink")
[Category:紅旗勳章獲得者](../Category/紅旗勳章獲得者.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")
[Category:蘇聯部長會議主席](../Category/蘇聯部長會議主席.md "wikilink")
[Category:蘇聯部長會議第一副主席](../Category/蘇聯部長會議第一副主席.md "wikilink")
[Category:蘇聯財政部長](../Category/蘇聯財政部長.md "wikilink")
[Category:蘇聯政治人物](../Category/蘇聯政治人物.md "wikilink")
[Category:俄羅斯蘇維埃聯邦社會主義共和國政治人物](../Category/俄羅斯蘇維埃聯邦社會主義共和國政治人物.md "wikilink")
[Category:蘇聯共產黨中央政治局委員](../Category/蘇聯共產黨中央政治局委員.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:蘇聯工程師](../Category/蘇聯工程師.md "wikilink")
[Category:蘇聯第二次世界大戰人物](../Category/蘇聯第二次世界大戰人物.md "wikilink")
[Category:俄國內戰人物](../Category/俄國內戰人物.md "wikilink")
[Category:安葬於克里姆林宮紅場墓園者](../Category/安葬於克里姆林宮紅場墓園者.md "wikilink")
[Category:聖彼得堡人](../Category/聖彼得堡人.md "wikilink")