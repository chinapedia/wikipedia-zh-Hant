**阿利安卓·崗札雷·伊納利圖**（
<small>西语發音：</small>，）是一位[墨西哥男導演](../Page/墨西哥.md "wikilink")，生於[墨西哥](../Page/墨西哥.md "wikilink")[墨西哥城](../Page/墨西哥城.md "wikilink")，「阿利安卓」是他的名字，「崗札雷」是父親的家族姓，「伊納利圖」是母親的家族姓。

阿利安卓21歲時從事廣播業，主持搖滾樂節目備受歡迎。1990年進入[墨西哥最大電視網](../Page/墨西哥.md "wikilink")「[Televisa](../Page/Televisa.md "wikilink")」擔任藝術指導，藉此執導了一些廣告影片。1991年成立「Zeta
Films」製片公司，專門製作電視節目。後來阿利安卓開始和[吉勒莫·亞瑞格合作編寫劇本](../Page/吉勒莫·亞瑞格.md "wikilink")，據說花了兩年的時間寫出36個版本才為《[愛情像母狗](../Page/愛情像母狗.md "wikilink")》定稿。製片預算精簡，只有350萬[美元](../Page/美元.md "wikilink")，本片首先在2000年的[戛纳电影节](../Page/戛纳电影节.md "wikilink")「國際影評人週」單元獲評審團大獎，隔年再入圍[美國](../Page/美國.md "wikilink")[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")[最佳外語片](../Page/奧斯卡最佳外語片獎.md "wikilink")。由於此片頗獲好評，阿利安卓遂被邀往美國執導電影。迄今，阿利安卓的劇情長片的劇本經常由[吉列尔莫·亚利亚格負責](../Page/吉勒莫·亞瑞格.md "wikilink")。

[2000年之作品](../Page/2000年電影.md "wikilink")《愛情像母狗》入圍2001年美國[奧斯卡最佳外語片](../Page/奧斯卡最佳外語片.md "wikilink")，[2003年之作品](../Page/2003年電影.md "wikilink")《[靈魂的重量](../Page/靈魂的重量.md "wikilink")》入圍2003年[威尼斯影展正式競賽](../Page/威尼斯影展.md "wikilink")，男主角[西恩·潘獲最佳男演員獎](../Page/西恩·潘.md "wikilink")。[2006年作品](../Page/2006年电影.md "wikilink")《[火線交錯](../Page/火線交錯.md "wikilink")》獲2006年[戛纳电影节最佳導演](../Page/第59屆康城影展.md "wikilink")、2007年美國[金球獎](../Page/第64屆金球獎.md "wikilink")[戲劇類最佳影片](../Page/金球獎最佳戲劇類影片.md "wikilink")。[2015年和](../Page/2015年電影.md "wikilink")[2016年各以作品](../Page/2016年電影.md "wikilink")《[鳥人](../Page/鳥人_\(2014年電影\).md "wikilink")》（Birdman）和《[神鬼獵人](../Page/神鬼獵人.md "wikilink")》（The
Revenant）連續勇奪[第87屆和](../Page/第87屆奧斯卡金像獎.md "wikilink")[第88屆](../Page/第88屆奧斯卡金像獎.md "wikilink")[奥斯卡最佳导演奖殊榮](../Page/奥斯卡最佳导演奖.md "wikilink")，他也成為繼[約翰·福特](../Page/约翰·福特.md "wikilink")（第13、14屆）以及[約瑟夫·孟威茲](../Page/约瑟夫·曼凯维奇.md "wikilink")（第22、23屆）之後，史上第3位成功「連莊」此獎的導演。

## 導演作品

  - [2000年](../Page/2000年电影.md "wikilink")：《[愛情像母狗](../Page/愛情像母狗.md "wikilink")》（*Amores
    perros*）
      - [英國電影學院獎最佳外語片](../Page/英國電影學院獎最佳外語片.md "wikilink")
      - 提名[第73屆](../Page/第73屆奧斯卡金像獎.md "wikilink")[奧斯卡最佳外語片獎](../Page/奧斯卡最佳外語片獎.md "wikilink")
      - 提名[金球獎最佳外語片](../Page/金球獎最佳外語片.md "wikilink")
  - [2002年](../Page/2002年电影.md "wikilink")：《》（*11'09"01 September
    11*）片段：「墨西哥」
  - [2003年](../Page/2003年电影.md "wikilink")：《[靈魂的重量](../Page/靈魂的重量.md "wikilink")》（*21
    Grams*）
  - [2006年](../Page/2006年电影.md "wikilink")：《[火線交錯](../Page/火線交錯.md "wikilink")》（*Babel*）
      - [第64屆金球獎](../Page/第64屆金球獎.md "wikilink")[最佳劇情片](../Page/金球奖最佳剧情片.md "wikilink")
      - 提名[第79屆](../Page/第79屆奧斯卡金像獎.md "wikilink")[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")
      - 提名[奧斯卡最佳影片獎](../Page/奧斯卡最佳影片獎.md "wikilink")
      - 提名[英國電影學院獎最佳導演](../Page/英國電影學院獎最佳導演.md "wikilink")
      - 提名[英國電影學院獎最佳影片](../Page/英國電影學院獎最佳影片.md "wikilink")
      - 提名[金球獎最佳導演](../Page/金球獎最佳導演.md "wikilink")
  - [2010年](../Page/2010年电影.md "wikilink")：《》（*Biutiful*）
      - 提名[第83屆](../Page/第83届奥斯卡金像奖.md "wikilink")[奧斯卡最佳外語片獎](../Page/奧斯卡最佳外語片獎.md "wikilink")
      - 提名[英國電影學院獎最佳外語片](../Page/英國電影學院獎最佳外語片.md "wikilink")
      - 提名[第68屆](../Page/第68屆金球獎.md "wikilink")[金球獎最佳外語片](../Page/金球獎最佳外語片.md "wikilink")
  - [2014年](../Page/2014年電影.md "wikilink")：《[鳥人](../Page/鳥人_\(2014年電影\).md "wikilink")》（*Birdman*）
      - [第71届威尼斯电影节开幕片和主竞赛片](../Page/第71届威尼斯电影节.md "wikilink")
      - [纽约电影节闭幕片](../Page/纽约电影节.md "wikilink")\[1\]
      - [第72屆金球獎](../Page/第72屆金球獎.md "wikilink")[最佳劇本](../Page/金球獎最佳劇本.md "wikilink")
      - [第87屆奧斯卡](../Page/第87屆奧斯卡金像獎.md "wikilink")[最佳影片](../Page/奧斯卡最佳影片獎.md "wikilink")
      - [第87屆奧斯卡](../Page/第87屆奧斯卡金像獎.md "wikilink")[最佳原創劇本](../Page/奧斯卡最佳原創劇本獎.md "wikilink")
      - [第87屆奧斯卡](../Page/第87屆奧斯卡金像獎.md "wikilink")[最佳導演](../Page/奧斯卡最佳導演獎.md "wikilink")
  - [2015年](../Page/2015年電影.md "wikilink")：《[神鬼獵人](../Page/神鬼獵人.md "wikilink")》（*The
    Revenant*）
      - [第73屆金球獎](../Page/第73屆金球獎.md "wikilink")[最佳導演](../Page/金球獎最佳導演.md "wikilink")
      - [第73屆金球獎](../Page/第73屆金球獎.md "wikilink")[最佳劇情片](../Page/金球奖最佳剧情片.md "wikilink")
      - [第88屆奧斯卡](../Page/第88屆奧斯卡金像獎.md "wikilink")[最佳導演](../Page/奧斯卡最佳導演獎.md "wikilink")

## 參考資料與註釋

## 外部連結

  - [阿利安卓·崗札雷·伊納利圖](https://web.archive.org/web/20141217030123/http://www.biosstars-mx.com/alejandro-gonzalez-inarritu/)，Biosstars
    International

  -
  -
[Category:墨西哥城人](../Category/墨西哥城人.md "wikilink")
[Category:墨西哥電影導演](../Category/墨西哥電影導演.md "wikilink")
[Category:墨西哥编剧](../Category/墨西哥编剧.md "wikilink")
[Category:墨西哥电影监制](../Category/墨西哥电影监制.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:金球獎最佳劇本獲得者](../Category/金球獎最佳劇本獲得者.md "wikilink")
[Category:奧斯卡最佳導演獎獲獎者](../Category/奧斯卡最佳導演獎獲獎者.md "wikilink")
[Category:金球獎最佳導演獲得者](../Category/金球獎最佳導演獲得者.md "wikilink")
[Category:英国电影学院奖最佳外语片获奖导演](../Category/英国电影学院奖最佳外语片获奖导演.md "wikilink")
[Category:美国导演工会奖获得者](../Category/美国导演工会奖获得者.md "wikilink")
[Category:英语电影导演](../Category/英语电影导演.md "wikilink")
[Category:英国电影学院奖最佳导演得主](../Category/英国电影学院奖最佳导演得主.md "wikilink")
[Category:奥斯卡特别成就奖获奖者](../Category/奥斯卡特别成就奖获奖者.md "wikilink")

1.  [《鸟人》发布预告
    过气“英雄”的中年危机](http://v.ent.163.com/video/2014/8/8/9/VA3BBUH89.html)