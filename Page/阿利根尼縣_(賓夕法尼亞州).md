**阿利根尼縣**（）是[美國](../Page/美國.md "wikilink")[賓夕法尼亞州西部的一個縣](../Page/賓夕法尼亞州.md "wikilink")。面積1,929平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口1,281,666。縣治[匹茲堡](../Page/匹茲堡.md "wikilink")
（Pittsburgh）是該州第二大城市。

成立於1788年9月24日。縣名指一個叫Allegwi的部落 （意思是「美麗的溪流」）\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[\*](../Category/賓夕法尼亞州阿利根尼縣.md "wikilink")
[A](../Category/賓夕法尼亞州行政區劃.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.