**阿卡迪亞縣**（**Acadia Parish,
Louisiana**）是[美國](../Page/美國.md "wikilink")[路易斯安那州西南部的一個縣](../Page/路易斯安那州.md "wikilink")。面積1,703平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口58,861人。縣治[克羅利](../Page/克羅利_\(路易斯安那州\).md "wikilink")
(Crowley)。

本縣成立於1888年6月30日，因殖民者來自[加拿大的](../Page/加拿大.md "wikilink")[阿卡迪亞而名](../Page/阿卡迪亞_\(加拿大\).md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[A](../Category/路易斯安那州行政區劃.md "wikilink")

1.  [Acadia
    Parish](http://www.dhh.louisiana.gov/OPH/PHP%202005/PDF/Acadia/Parish%20Acadia.pdf)