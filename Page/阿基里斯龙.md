**阿基里斯龍屬**（[屬名](../Page/屬.md "wikilink")：）又譯**阿氏英雄龍**，是[馳龍科](../Page/馳龍科.md "wikilink")[馳龍亞科的一個](../Page/馳龍亞科.md "wikilink")[屬](../Page/屬.md "wikilink")，生活在[上白堊紀的](../Page/白堊紀.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")。牠可能是活躍的雙足獵食動物，以牠後腳第二腳趾上那巨大似鐮刀的趾爪來獵食。牠是屬於大型的馳龍科[恐龍](../Page/恐龍.md "wikilink")，根據目前發現的唯一標本，牠們的身長約6公尺長\[1\]。

## 發現及物種

[Achillobator_scale.png](https://zh.wikipedia.org/wiki/File:Achillobator_scale.png "fig:Achillobator_scale.png")
屬名是由著名的[古希臘](../Page/古希臘.md "wikilink")[特洛伊戰爭英雄](../Page/特洛伊戰爭.md "wikilink")[阿奇里斯及](../Page/阿奇里斯.md "wikilink")[蒙古語的](../Page/蒙古語.md "wikilink")「英雄」一字而來，這是由於阿基里斯龙在使用鐮刀爪捕食時，需要強大的[跟腱](../Page/跟腱.md "wikilink")。[種被命名為](../Page/種.md "wikilink")**巨大阿基里斯龙**（學名*A.
giganticus*）是因牠的體型較其他[馳龍科為大](../Page/馳龍科.md "wikilink")。於1999年，牠的名字首先被[蒙古](../Page/蒙古.md "wikilink")[古生物學家Altangerel](../Page/古生物學家.md "wikilink")
Perle及[美國的](../Page/美國.md "wikilink")[馬克·諾瑞爾](../Page/馬克·諾瑞爾.md "wikilink")（Mark
Norell）及[吉姆·克拉克所描述](../Page/吉姆·克拉克.md "wikilink")\[2\]\[3\]。

阿基里斯龍的[化石骨骼發現時大部份都是脫落的](../Page/化石.md "wikilink")，包括一個上頜碎片及牙齒、大部分的脊椎骨、肋骨、[肩胛骨](../Page/肩胛骨.md "wikilink")、骨盆、前肢及後肢骨。這些化石是發現於[蒙古](../Page/蒙古.md "wikilink")[東戈壁省的Bayan](../Page/東戈壁省.md "wikilink")
Shireh組地層，年代估計可追溯至[上白堊紀](../Page/白堊紀.md "wikilink")。由於有兩套不同的假說，確實的日期卻不能肯定。與其他地層比較，這個地層似乎是在上白堊紀的[土侖階至](../Page/土侖階.md "wikilink")[坎帕階的早期](../Page/坎帕階.md "wikilink")，約為9,300萬到8,000萬年前\[4\]。但是，從[磁性地層劃分的測量中發現](../Page/磁性地層劃分.md "wikilink")，該地層應位於[白堊紀正長距](../Page/白堊紀正長距.md "wikilink")，即[桑托階至](../Page/桑托階.md "wikilink")[森諾曼階](../Page/森諾曼階.md "wikilink")，約9,800萬到8,300萬年前\[5\]。於此地層發現的其他[恐龍](../Page/恐龍.md "wikilink")，包含：[獨龍](../Page/獨龍.md "wikilink")、[慢龍](../Page/慢龍.md "wikilink")、[籃尾龍及](../Page/籃尾龍.md "wikilink")[巴克龍](../Page/巴克龍.md "wikilink")。

## 分類

阿基里斯龍可能是[馳龍科的](../Page/馳龍科.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，馳龍科現時被認為是[鳥類的近親](../Page/鳥類.md "wikilink")。雖然有關馳龍科與其他[獸腳亞目恐龍的關係較明白](../Page/獸腳亞目.md "wikilink")，但是馳龍科內部的演化發展卻不明朗。與同在[蒙古發現的](../Page/蒙古.md "wikilink")[惡靈龍](../Page/惡靈龍.md "wikilink")、[北美洲的](../Page/北美洲.md "wikilink")[猶他盜龍](../Page/猶他盜龍.md "wikilink")、[馳龍及](../Page/馳龍.md "wikilink")[蜥鳥盜龍為近親](../Page/蜥鳥盜龍.md "wikilink")。[恐爪龍及](../Page/恐爪龍.md "wikilink")[伶盜龍都是馳龍科恐龍](../Page/伶盜龍.md "wikilink")，但卻是另一個[伶盜龍亞科](../Page/伶盜龍亞科.md "wikilink")\[6\]。在2009年，惡靈龍、蜥鳥盜龍分別被歸類到伶盜龍亞科、[蜥鳥盜龍亞科](../Page/蜥鳥盜龍亞科.md "wikilink")。

## 嵌合體爭議

與其他[馳龍科](../Page/馳龍科.md "wikilink")[恐龍比較](../Page/恐龍.md "wikilink")，阿基里斯龍的骨盆帶有[蜥臀目的祖徵](../Page/蜥臀目.md "wikilink")。例如[恥骨是直立並且有大的底部](../Page/恥骨.md "wikilink")，這卻與大部份馳龍科有小型的底部不同。此外，恥骨與[坐骨同是指向後方](../Page/坐骨.md "wikilink")，這種情況可見於無關聯的[鳥盤目恐龍及鳥類](../Page/鳥盤目.md "wikilink")。

由於上述的不同，使得有學者認為阿基里斯龍是[古生物學上的一個](../Page/古生物學.md "wikilink")[嵌合體](../Page/嵌合體.md "wikilink")\[7\]。但是，其他研究卻反對這種說法。他們認為阿基里斯龍的化石只是半脫離，縱然將上述的分別計算在內，在分支系統學分析中明顯地仍是屬於[馳龍科的](../Page/馳龍科.md "wikilink")\[8\]。

## 參考

## 外部連結

  - [See entry on *Achillobator* at Dinodata (registration required,
    free)](http://www.dinodata.org/index.php)
  - \[<https://web.archive.org/web/20070311001420/http://www.nhm.ac.uk/jdsml/nature-online/dino-directory/detail.dsml?Genus=Achillobator>*Achillobator*\]
    in the [Dino
    Directory](http://www.nhm.ac.uk/jdsml/nature-online/dino-directory/)

[Category:馳龍亞科](../Category/馳龍亞科.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")

1.  Holtz, Thomas R. Jr. (2010) *Dinosaurs: The Most Complete,
    Up-to-Date Encyclopedia for Dinosaur Lovers of All Ages,*
    [Winter 2010
    Appendix.](http://www.geol.umd.edu/~tholtz/dinoappendix/HoltzappendixWinter2010.pdf)
2.  Perle A, Norell MA & Clark J. 1999. A new maniraptoran theropod -
    *Achillobator giganticus* (Dromaeosauridae) - from the Upper
    Cretaceous of Burkhant, Mongolia. *Contributions of the
    Mongolian-American Paleontological Project* 101: 1–105.
3.  Poling, Jeff (1996). "[Dinosauria Translation and Pronunciation
    Guide](http://www.dinosauria.com/dml/names/dinoa.htm) " Last
    accessed 2008-07-07.
4.  Jerzykiewicz T & Russell DA. 1991. Late Mesozoic stratigraphy and
    vertebrates of the Gobi Basin. *Cretaceous Research* 12(4): 345-377.
5.  Hicks JF, Brinkman DL, Nichols DJ & Watabe M. 1999. Paleomagnetic
    and palynological analyses of Albian to Santonian strata at Bayn
    Shireh, Burkhant, and Khuren Dukh, eastern Gobi Desert, Mongolia.
    *Cretaceous Research* 20(6): 829-850.
6.  Makovicky JA, Apesteguía S & Agnolín FL. 2005. The earliest
    dromaeosaurid theropod from South America. *Nature* 437: 1007-1011.
7.  Burnham DA, Derstler KL, Currie PJ, Bakker RT, Zhou Z, & Ostrom JH.
    2000. Remarkable new birdlike dinosaur (Theropoda: Maniraptora) from
    the Upper Cretaceous of Montana. *University of Kansas
    Paleontological Contributions* 13: 1-14.
8.  Norell MA & Makovicky JA. 2004. Dromaeosauridae. In Weishampel,
    D.B., P. Dodson, & H. Osmolska (eds.). *The Dinosauria* (2nd
    Edition). Berkeley: University of California Press. Pp. 196-209.