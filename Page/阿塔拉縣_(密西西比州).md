1.
**-{zh-hans:阿塔拉县;zh-hk:阿塔拉縣;zh-tw:阿塔拉郡;}-**（**Attala County,
Mississippi**）是[美國](../Page/美國.md "wikilink")[密西西比州中部的一個縣](../Page/密西西比州.md "wikilink")。面積1,909平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口19,661人。縣治[科西阿斯科](../Page/科西阿斯科_\(密西西比州\).md "wikilink")（Kosciusko）。

阿塔拉縣成立於1833年12月23日。縣名是[法國作家](../Page/法國.md "wikilink")[夏多布里昂子爵筆下的一名印第安女子](../Page/夏多布里昂子爵.md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[A](../Category/密西西比州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.