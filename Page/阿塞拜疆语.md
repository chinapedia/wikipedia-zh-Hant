**阿塞拜疆语**（Azərbaycan dili , Azəri dil , Азәрбајҹан дили , آذربايجان ديلی
, Azərbaycan-türkcəsi ,
آذربایجان-تورکجەسی）。属[阿尔泰语系南支](../Page/阿尔泰语系.md "wikilink")[突厥语族](../Page/突厥语族.md "wikilink")，是[阿塞拜疆的](../Page/阿塞拜疆.md "wikilink")[官方语言](../Page/官方语言.md "wikilink")。其与[土耳其语](../Page/土耳其语.md "wikilink")，[土庫曼語和](../Page/土庫曼語.md "wikilink")[克里米亞韃靼語有很大的联系](../Page/克里米亞韃靼語.md "wikilink")，可互通。在2.3至3千万阿塞拜疆语使用者中，大概1.6至2.3千万人在[伊朗](../Page/伊朗.md "wikilink")、7百万人在阿塞拜疆，在其他较少的社群也有约80万人使用阿塞拜疆语。

在伊朗使用的阿塞拜疆语，受到波斯语的影响，在《民族语》中称为“南阿塞拜疆语”，而阿塞拜疆用的被称为“北阿塞拜疆语”。

## 语音

  - **[辅音](../Page/辅音.md "wikilink")**

|                                       |                                  |                                |                                   |                                  |                                  |                                |
| ------------------------------------- | -------------------------------- | ------------------------------ | --------------------------------- | -------------------------------- | -------------------------------- | ------------------------------ |
|                                       | [双唇音](../Page/双唇音.md "wikilink") | [齿音](../Page/齿音.md "wikilink") | [齿槽音](../Page/齒齦後音.md "wikilink") | [软颚音](../Page/软颚音.md "wikilink") | [小舌音](../Page/小舌音.md "wikilink") | [喉音](../Page/喉音.md "wikilink") |
|                                       |                                  |                                |                                   |                                  |                                  |                                |
| [塞音](../Page/塞音.md "wikilink")        | [清音](../Page/清音.md "wikilink")   | p                              | t                                 |                                  | k                                | q                              |
| [浊音](../Page/浊音.md "wikilink")        | b                                | d                              |                                   | g                                |                                  |                                |
| [塞擦音](../Page/塞擦音.md "wikilink")      | [清音](../Page/清音.md "wikilink")   |                                |                                   | ʧ                                |                                  |                                |
| [浊音](../Page/浊音.md "wikilink")        |                                  |                                | ʤ                                 |                                  |                                  |                                |
| [擦音](../Page/擦音.md "wikilink")        | [清音](../Page/清音.md "wikilink")   | f                              | s                                 | ʃ                                | x                                |                                |
| [浊音](../Page/浊音.md "wikilink")        | v                                | z                              | ʒ                                 | ɣ                                |                                  |                                |
| [鼻音](../Page/鼻音_\(辅音\).md "wikilink") | m                                | n                              |                                   |                                  |                                  |                                |
| [边音](../Page/边音.md "wikilink")        |                                  | l                              |                                   |                                  |                                  |                                |
| [r音](../Page/R系輔音.md "wikilink")      |                                  | r                              |                                   |                                  |                                  |                                |

  - **[元音](../Page/元音.md "wikilink")**

|                                |                                |                                |                                |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
|                                | [前](../Page/前元音.md "wikilink") | [央](../Page/央元音.md "wikilink") | [後](../Page/後元音.md "wikilink") |
| 非圓唇                            | 圓唇                             | 非圓唇                            | 圓唇                             |
| [高](../Page/高元音.md "wikilink") | i                              | y                              |                                |
| [中](../Page/中元音.md "wikilink") | e                              | œ                              |                                |
| [低](../Page/低元音.md "wikilink") | æ                              |                                | a                              |

## 字母

[South_Azeri_Turks_(in_Iran)_uses_the_Arabic_script.jpg](https://zh.wikipedia.org/wiki/File:South_Azeri_Turks_\(in_Iran\)_uses_the_Arabic_script.jpg "fig:South_Azeri_Turks_(in_Iran)_uses_the_Arabic_script.jpg")所用语言）使用的[阿拉伯字母](../Page/阿拉伯字母.md "wikilink")\]\]
在阿塞拜疆共和国，现行阿塞拜疆语官方采用[土耳其语字母为基础](../Page/土耳其语字母.md "wikilink")，但是以[西里尔字母为基础的阿塞拜疆语同样广泛通用](../Page/西里尔字母.md "wikilink")。南阿塞拜疆語使用阿拉伯字母。阿塞拜疆语有32个字母，分别如下：

**1929年前** 使用[阿拉伯字母](../Page/阿拉伯字母.md "wikilink")。

  - **由1929年至1939年**
      - <span lang="en">Aa, Bb, Cc, [Çç](../Page/Ç.md "wikilink"), Dd,
        Ee, [Əə](../Page/Ə.md "wikilink"), Ff, Gg,
        [{{unicode](../Page/Ƣ.md "wikilink"), Hh, Ii,
        [Ьь](../Page/Ь.md "wikilink"), Jj, Kk, Qq, Ll, Mm, Nn, Oo,
        [Ɵɵ](../Page/Ɵ.md "wikilink"), Pp, Rr, Ss,
        [Şş](../Page/Ş.md "wikilink"), Tt, Uu,
        [Üü](../Page/Ü.md "wikilink"), Vv, Xx, Yy, Zz</span>

<!-- end list -->

  - **由1939年至1991年**
      - <span lang="ru">[Аа](../Page/А.md "wikilink"),
        [Бб](../Page/Б.md "wikilink"), [Вв](../Page/В.md "wikilink"),
        [Гг](../Page/Г.md "wikilink"), [Ғғ](../Page/Ғ.md "wikilink"),
        [Дд](../Page/Д.md "wikilink"), [Әә](../Page/Ә.md "wikilink"),
        [Жж](../Page/Ж.md "wikilink"), [Зз](../Page/З.md "wikilink"),
        [Ии](../Page/И.md "wikilink"), [Ыы](../Page/Ы.md "wikilink"),
        [Јј](../Page/Ј.md "wikilink"), [Кк](../Page/К.md "wikilink"),
        [Ҝҝ](../Page/Ҝ.md "wikilink"), [Лл](../Page/Л.md "wikilink"),
        [Мм](../Page/М.md "wikilink"), [Оо](../Page/О.md "wikilink"),
        [Өө](../Page/Ө.md "wikilink"), [Пп](../Page/П.md "wikilink"),
        [Рр](../Page/Р.md "wikilink"), [Сс](../Page/С.md "wikilink"),
        [Тт](../Page/Т.md "wikilink"), [Уу](../Page/У.md "wikilink"),
        [Үү](../Page/Ү.md "wikilink"), [Фф](../Page/Ф.md "wikilink"),
        [Хх](../Page/Х.md "wikilink"), [Һh](../Page/Һ.md "wikilink"),
        [Чч](../Page/Ч.md "wikilink"), [Ҹҹ](../Page/Ҹ.md "wikilink"),
        [Шш](../Page/Ш.md "wikilink"), -{’}-</span> (apostrophe)

<!-- end list -->

  - **自1991年起**
      - <span lang="en">Aa, Bb, Cc, [Çç](../Page/Ç.md "wikilink"), Dd,
        Ee, [Əə](../Page/Ə.md "wikilink"), Ff, Gg,
        [Ğğ](../Page/Ğ.md "wikilink"), Hh, Xx,
        [Iı](../Page/带点与不带点I.md "wikilink"),
        [İi](../Page/带点与不带点I.md "wikilink"), Jj, Kk, Qq, Ll, Mm,
        Nn, Oo, [Öö](../Page/Ö.md "wikilink"), Pp, Rr, Ss,
        [Şş](../Page/Ş.md "wikilink"), Tt, Uu,
        [Üü](../Page/Ü.md "wikilink"), Vv, Yy, Zz</span>

### 各種字母間的转写一覽

|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - |
| A | B | C | Ç | D | E | Ə | F | G | Ğ | H | X | I | İ | J | K | Q | L | M | N | O | Ö | P | R | S | Ş | T | U | Ü | V | Y | Z |   |
| a | b | c | ç | d | e | ә | f | g | ğ | h | x | ı | i | j | k | q | l | m | n | o | ö | p | r | s | ş | t | u | ü | v | y | z | ' |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| А | Б | Ҹ | Ч | Д | Е | Ә | Ф | Ҝ | Ғ | Һ | Х | Ы | И | Ж | К | Г | Л | М | Н | О | Ө | П | Р | С | Ш | Т | У | Ү | В | J | З |   |
| а | б | ҹ | ч | д | е | ә | ф | ҝ | ғ | һ | x | ы | и | ж | к | г | л | м | н | о | ө | п | р | с | ш | т | у | ү | в | j | з | ' |

阿拉伯、拉丁、西里尔字母表的顺序不同，下表按拉丁字母表排序。

| 阿塞拜疆語字母转写表                         |
| ---------------------------------- |
| [阿拉伯](../Page/阿拉伯字母.md "wikilink") |
| —1929                              |
| ﺍ                                  |
| ﺏ                                  |
| ﺝ                                  |
| چ                                  |
| ﺩ                                  |
|                                    |
| ﻉ                                  |
| ﻑ                                  |
| گ                                  |
| ﻍ                                  |
| ﺡ,ﻩ                                |
| ﺥ                                  |
|                                    |
| ﻱ                                  |
| ﺝ                                  |
| ﻙ                                  |
| ﻕ                                  |
| ﻝ                                  |
| ﻡ                                  |
| ﻥ                                  |
| ﻭ                                  |
|                                    |
| پ                                  |
| ﺭ                                  |
| ﺙ,ﺱ,ﺹ                              |
| ﺵ                                  |
| ﺕ,ﻁ                                |
| ﻭ                                  |
| ﻭ                                  |
| ﻭ                                  |
| ﻱ                                  |
| ﺫ,ﺯ,ﺽ,ﻅ                            |
|                                    |

## 文学

经典的阿塞拜疆语文学在15世纪形成\[1\]，以多种在早期中世纪的[大不里士和](../Page/大不里士.md "wikilink")[希尔凡方言为基础](../Page/希尔凡.md "wikilink")。这些方言为阿塞拜疆经典的作家，如[富祖里](../Page/富祖里.md "wikilink")、[伊斯玛仪一世等人所使用](../Page/伊斯玛仪一世.md "wikilink")。阿塞拜疆的现代文学主要基于希尔凡方言，而在伊朗则主要是基于大不里士方言。第一份阿塞拜疆语报纸于1875年出版。

在19世纪中期，阿塞拜疆文学在[巴库](../Page/巴库.md "wikilink")、[占贾](../Page/占贾.md "wikilink")、[舍基](../Page/舍基.md "wikilink")、[第比利斯](../Page/第比利斯.md "wikilink")、[叶里温的学校均有教授](../Page/叶里温.md "wikilink")。自1845年后，[俄国的](../Page/俄国.md "wikilink")[圣彼得堡国立大学也开始有教授](../Page/圣彼得堡国立大学.md "wikilink")。

## 註解

## 註釋

## 參考文獻

  -
## 外部链接

  - [azeri.org](http://www.azeri.org/)

  - [Azerbaijani Language by
    Ethnologue](http://www.ethnologue.com/show_language.asp?code=azb)

  - [Pre-Islamic
    roots](https://web.archive.org/web/20060209020322/http://www.masterliness.com/a/Azerbaijanis.htm)

  - [Azerbaijani-Turkish language in Iran by Ahmad
    Kasravi](https://www.webcitation.org/query?id=1256522164474196&url=www.geocities.com/evan_j_siegel/IranTurk/IranTurk.html)

  - [A blog on Azerbaijani language resources and
    translations](http://www.learnazeri.blogspot.com/)

  - [A blog about the Azerbaijani language and
    lessons](http://www.azyaz.ru/)

  - [azeri.org](http://www.azeri.org/), Azerbaijani literature and
    English translations.

  - [Online bidirectional Azerbaijani-English
    Dictionary](http://azerdict.com/)

  - [Learn Azerbaijani](http://learn101.org/azerbaijani.php) at
    learn101.org.

  - [Azerbaijan-Turkish language in
    Iran](https://web.archive.org/web/20071014220553/http://miejipang-jpn2.net/untitled6.html)
    by Ahmad Kasravi.

  - including sound file.

  - [Azerbaijani\<\>Turkish dictionary](http://ctle.pau.edu.tr/aztr)
    (Pamukkale University)

  - [Azerbaijan Language with
    Audio](http://www.funkyazerbaijani.com/learnazerbaijani/)

  - [Azerbaijani thematic
    vocabulary](http://azerbaijani.azeri.free.fr/index_uk.php)

  - [AzConvert](http://azconvert.sourceforge.net/), an [open
    source](../Page/open_source.md "wikilink") Azerbaijani
    [transliteration](../Page/转写.md "wikilink") program.

  - [Azerbaijani Alphabet and Language in
    Transition](http://www.azer.com/aiweb/categories/magazine/81_folder/81_articles/81_index.html),
    the entire issue of *Azerbaijan International*, Spring 2000 (8.1) at
    azer.com.

      - [Editorial](http://azer.com/aiweb/categories/magazine/81_folder/81_articles/81_editorial.html)
      - [Chart: Four Alphabet Changes in Azerbaijan in the 20th
        century](http://azer.com/aiweb/categories/magazine/81_folder/81_articles/81_alphabet_changes1.html)
      - [Chart: Changes in the Four Azerbaijan Alphabet Sequence in
        the 20th
        century](http://azer.com/aiweb/categories/magazine/81_folder/81_articles/81_alphabet_sequence1.html)
      - [Baku’s Institute of Manuscripts: Early Alphabets in
        Azerbaijan](http://azer.com/aiweb/categories/magazine/81_folder/81_articles/81_manuscripts.html)

[阿塞拜疆语](../Category/阿塞拜疆语.md "wikilink")
[Category:突厥語族](../Category/突厥語族.md "wikilink")
[Category:亞塞拜然語言](../Category/亞塞拜然語言.md "wikilink")
[Category:伊朗語言](../Category/伊朗語言.md "wikilink")
[Category:俄罗斯语言](../Category/俄罗斯语言.md "wikilink")
[Category:格魯吉亞語言](../Category/格魯吉亞語言.md "wikilink")
[Category:土耳其語言](../Category/土耳其語言.md "wikilink")
[Category:伊拉克語言](../Category/伊拉克語言.md "wikilink")

1.  Mark R.V. Southern. [Mark R V Southern (2005) *Contagious couplings:
    transmission of expressives in Yiddish echo
    phrases*](http://books.google.az/books?id=dGhFQFJZUIYC&pg=PA65),
    Praeger, Westport, Conn. ISBN 978-0-31306-844-7