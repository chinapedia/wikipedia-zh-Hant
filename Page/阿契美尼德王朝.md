**阿契美尼德王朝**（[古代波斯语](../Page/古波斯语.md "wikilink")：
；[现代波斯语](../Page/现代波斯语.md "wikilink")：
；前550年－前330年），也稱**波斯第一帝國**，是[古波斯地区第一个把領土擴張到大部份](../Page/古波斯.md "wikilink")[中亞和](../Page/中亞.md "wikilink")[西亚領域的王朝](../Page/西亚.md "wikilink")，也是第一個橫跨歐亞非三洲的帝國。极盛时期的領土疆域東起[印度河平原](../Page/印度河平原.md "wikilink")，西至[小亚细亚](../Page/小亚细亚.md "wikilink")、欧洲的[巴尔干半岛的](../Page/巴尔干半岛.md "wikilink")[色雷斯](../Page/色雷斯.md "wikilink")，西南至[埃及](../Page/埃及.md "wikilink")、利比亚、[努比亚和](../Page/努比亚.md "wikilink")[阿比西尼亚](../Page/阿比西尼亚.md "wikilink")。公元前480年的波斯帝国，领土面积约为600万平方公里，人口峰值约为1800万。

## 歷史

### 王國的建立与扩张

創建者為[阿契美尼斯](../Page/阿契美尼斯.md "wikilink")；當時整個波斯地區還是在[米底王國的統治之下](../Page/米底王國.md "wikilink")。阿契美尼斯死後，他的兒子[泰斯帕斯繼任王位](../Page/泰斯帕斯.md "wikilink")，並在米底王國[斯基泰時期帶領阿契美尼德脫離米底王國的統治](../Page/斯基泰.md "wikilink")。泰斯帕斯死後將土地分給他的兩個兒子：[居魯士一世和](../Page/居魯士一世.md "wikilink")[阿里亞拉姆尼斯](../Page/阿里亞拉姆尼斯.md "wikilink")。他們死後將王位分別傳給他們的兒子：[岡比西斯一世和](../Page/岡比西斯一世.md "wikilink")[阿爾沙米斯](../Page/阿爾沙米斯.md "wikilink")。公元前559年，[居魯士二世繼承岡比西斯一世的王位](../Page/居魯士二世.md "wikilink")，並在其後承襲了阿爾沙米斯的王位，重新聯合阿契美尼德王國。

[居魯士二世隨後開始擴張領土](../Page/居魯士二世.md "wikilink")。他首先公開反叛[米底亚王国](../Page/米底王国.md "wikilink")，並在公元前553—550年之间將其推翻。其後，他與[迦勒底王国結盟](../Page/迦勒底王国.md "wikilink")，以確保王國的後方不會受威脅；然後再起兵西進小亚细亚西部的[呂底亞](../Page/呂底亞.md "wikilink")，並於公元前547年在薩迪斯（呂底亞的國都）將其攻占。打败呂底亞後，居魯士二世在前546—540年之间收复了之前属于米底亚王国的东部领土疆域（阿拉霍西亚、格德罗西亚、德兰吉亚那、乾陀啰、巴克特里亚、马尔基安娜、锡尔河与阿姆河之间的[中亚河中地区的索格狄亚那](../Page/中亚河中地区.md "wikilink")、赫尔尼卡斯），之后乘著迦勒底王国國內政局不穩之時揮軍向其進攻。公元前539年，[巴比倫城陷落](../Page/新巴比伦王国.md "wikilink")，居魯士二世入城，並握住巴比倫守護神[馬爾杜克塑像的手](../Page/馬爾杜克.md "wikilink")，以表示願意以巴比倫人的身份來統治這個地方。自此，阿契美尼德王朝的勢力擴至埃及邊界。

### 征服埃及与王国的巩固

[Map_achaemenid_empire_en.png](https://zh.wikipedia.org/wiki/File:Map_achaemenid_empire_en.png "fig:Map_achaemenid_empire_en.png")

[居魯士二世去世後](../Page/居魯士二世.md "wikilink")，其子[岡比西斯二世繼承王位](../Page/岡比西斯二世.md "wikilink")。他随即對[古埃及發動了大規模的戰爭](../Page/第二十六王朝.md "wikilink")，攻破其王都[孟菲斯](../Page/孟菲斯.md "wikilink")，更將[法老擄至](../Page/法老.md "wikilink")[蘇薩](../Page/蘇薩.md "wikilink")。其後，他又對[迦太基](../Page/迦太基.md "wikilink")、[阿蒙绿洲及](../Page/阿蒙绿洲.md "wikilink")[努比亞發動征戰](../Page/努比亞.md "wikilink")，但因种种原因获胜不大。公元前522年，阿契美尼德王朝內部發生叛亂，岡比西斯二世急忙班師回朝，卻死在途中。遠支[宗室將領](../Page/宗室.md "wikilink")[大流士一世續率兵回國](../Page/大流士一世.md "wikilink")，並成功平定叛亂，更最後獲得王位，並娶了居魯士之女[阿托莎](../Page/阿托莎.md "wikilink")，以獲得貴族的支持。

[大流士一世為](../Page/大流士一世.md "wikilink")[阿爾沙米斯的孫兒](../Page/阿爾沙米斯.md "wikilink")，但其奪得王位的方式，引發了貴族們的反叛，他對內採取了嚴厲的措施，镇压了贵族们的叛亂，巩固了王位。對外他則積極推行擴張。他在即位的第二年（前521年）成功把[印度河平原的大片地區納入疆土](../Page/印度河平原.md "wikilink")。其後，他在[希波战争於前](../Page/希波战争.md "wikilink")499年揮軍至[小亞細亞與](../Page/小亞細亞.md "wikilink")[希臘之間的](../Page/希臘.md "wikilink")[赫勒斯滂海峽](../Page/赫勒斯滂海峽.md "wikilink")，前492年派其女婿[馬多尼奧斯進攻](../Page/馬多尼奧斯.md "wikilink")[色雷斯和](../Page/色雷斯.md "wikilink")[馬其頓](../Page/馬其頓.md "wikilink")，但於公元前490年在入侵希臘時的[馬拉松戰役中失利](../Page/馬拉松戰役.md "wikilink")。

大流士一世於公元前486年去世，此时阿契美尼德王朝统治着600万平方千米的土地和1800万人口。他的兒子[薛西斯一世接續稱王](../Page/薛西斯一世.md "wikilink")，並於前485年成功鎮壓在[埃及發生的一場叛亂](../Page/埃及.md "wikilink")。其後，他为惩罚[希臘而發起多次進攻](../Page/希臘.md "wikilink")，最初獲勝並佔領希臘北部地區。但在之後的幾場戰役中均失利，[馬多尼奧斯更於](../Page/馬多尼奧斯.md "wikilink")[普拉提亞一役中戰死](../Page/普拉提亞.md "wikilink")。自此，薛西斯一世灰心失志，不再理會政事，沈緬酒色，前465年被[宰相](../Page/宰相.md "wikilink")[阿爾塔班](../Page/阿爾塔班.md "wikilink")[刺殺](../Page/刺殺.md "wikilink")，其子[阿爾塔薛西斯一世即位](../Page/阿爾塔薛西斯一世.md "wikilink")。

### 王國的停滯

[阿爾塔薛西斯一世即位後](../Page/阿爾塔薛西斯一世.md "wikilink")，把阿爾塔班殺死，自掌大權，並與[雅典簽訂了](../Page/雅典.md "wikilink")[加利亞斯條約](../Page/加利亞斯條約.md "wikilink")，宣告了[希波戰爭的結束](../Page/希波戰爭.md "wikilink")，死後由薛西斯二世，薛西斯二世執政只有45天，就在酒後被人謀殺。薛西斯二世之弟[蘇底亞即位](../Page/蘇底亞.md "wikilink")，半年後[大流士二世將蘇底亞殺死而自立為王](../Page/大流士二世.md "wikilink")，死後由[阿爾塔薛西斯二世接任](../Page/阿爾塔薛西斯二世.md "wikilink")，成功策動[雅典和](../Page/雅典.md "wikilink")[斯巴達的戰爭](../Page/斯巴達.md "wikilink")，更最終簽署了《國王和約》，從而得以插手[希臘的事務](../Page/希臘.md "wikilink")。公元前401年，小居魯士從[小亞細亞起兵叛亂](../Page/小亞細亞.md "wikilink")，率領約一萬一千七百名希臘[僱傭兵東進爭奪王位](../Page/僱傭兵.md "wikilink")，被阿爾塔薛西斯二世所敗；但他所僱的希臘僱傭兵卻成功退卻回希臘本土（參見[色諾芬](../Page/色諾芬.md "wikilink")《長征記》）。

### 衰亡

此後的幾年間，阿契美尼德王國本土發生了多次的叛亂；雖然最終都獲平定，但卻削弱了王國本身的军力。公元前359年，[阿爾塔薛西斯三世登基](../Page/阿爾塔薛西斯三世.md "wikilink")。在其统治下，阿契美尼德王朝得到中兴，人口达到顶点1850万人。但是由於他拒絕幫助[雅典對抗北面的](../Page/雅典.md "wikilink")[馬其頓](../Page/馬其頓.md "wikilink")，與[雅典鬧翻](../Page/雅典.md "wikilink")。[馬其頓王国其後征服全](../Page/馬其頓王国.md "wikilink")[希臘](../Page/希臘.md "wikilink")，更於公元前334年—前330年在[亞歷山大三世的率领下迅速攻入](../Page/亚历山大大帝.md "wikilink")[波斯波利斯](../Page/波斯波利斯.md "wikilink")。同年，阿契美尼德末代國王[大流士三世被部下所殺](../Page/大流士三世.md "wikilink")，阿契美尼德王國灭亡。

## 行政制度

[大流士一世加強](../Page/大流士一世.md "wikilink")[中央集權](../Page/中央集權.md "wikilink")，把全國分為大約20個省，每省設一位[總督](../Page/總督_\(古波斯\).md "wikilink")，負責地方行政事務，又另設一位將軍管理省內駐軍及另一位官員管理稅收，避免總督一人獨大。總督身邊另有一位國王任命的秘書，負起協助國王監視地方的作用。\[1\]不過有些總督也兼有軍權。\[2\]

## 阿契美尼德王及領袖

:\* [泰斯帕斯](../Page/泰斯帕斯.md "wikilink")，阿契美尼斯之子

:\* [居魯士一世](../Page/居魯士一世.md "wikilink")，泰斯帕斯之子

:\* [岡比西斯一世](../Page/岡比西斯一世.md "wikilink")，居魯士一世之子

:\*
[居魯士二世](../Page/居魯士二世.md "wikilink")（[古列王](../Page/古列.md "wikilink")/[居魯士大帝](../Page/居魯士大帝.md "wikilink")），岡比西斯一世之子，約前550年–前530年在位（約前559年起統治[安善](../Page/安善.md "wikilink")，前550年征服米底王國）

:\* [岡比西斯二世](../Page/岡比西斯二世.md "wikilink")，居魯士大帝之子，前529年–前522年在位

:\* [高墨達](../Page/高墨達.md "wikilink")，據稱是居魯士大帝之子，前522年在位（可能是篡位者）

:\* [大流士一世](../Page/大流士大帝.md "wikilink")（大帝），阿爾沙米斯之孫，前521年–前486年在位

:\* [薛西斯一世](../Page/薛西斯一世.md "wikilink")，大流士一世之子，前485年–前465年在位

:\* [阿爾塔薛西斯一世](../Page/阿爾塔薛西斯一世.md "wikilink")，薛西斯一世之子，前465年–前424年在位

:\* [薛西斯二世](../Page/薛西斯二世.md "wikilink")，阿爾塔薛西斯一世之子，前424年在位

:\* [塞基狄亚努斯](../Page/塞基狄亚努斯.md "wikilink")，前424年–前423年在位

:\* [大流士二世](../Page/大流士二世.md "wikilink")，前423年–前405年在位

:\* [阿爾塔薛西斯二世](../Page/阿爾塔薛西斯二世.md "wikilink")，大流士二世之子，前404年–前359年在位

:\* [阿爾塔薛西斯三世](../Page/阿爾塔薛西斯三世.md "wikilink")，阿爾塔薛西斯二世之子，前358年–前338年在位

:\* [阿爾塞斯](../Page/阿爾塞斯.md "wikilink")，阿爾塔薛西斯三世之子，前338年–前336年在位

:\* [大流士三世](../Page/大流士三世.md "wikilink")，大流士二世之曾孫，前336年–前330年在位

## 王朝世系图

## 腳註

## 參考資料

  - Stronach, David "Darius at Pasargadae: A Neglected Source for the
    History of Early Persia," Topoi
  - Stronach, David "Anshan and Parsa: Early Achaemenid History, Art and
    Architecture on the Iranian Plateau". In: John Curtis, ed.,
    Mesopotamia and Iran in the Persian Period: Conquest and Imperialism
    539-331, 35-53. London: British Museum Press 1997.

## 延伸閱讀

  - *Ancient Persia* Josef Wiesehofer
  - *Forgotten Empire: The World of Ancient Persia* J. E Curtis and N.
    Tallis
  - *From Cyrus to Alexander: A History of the Persian Empire* [Pierre
    Briant](../Page/Pierre_Briant.md "wikilink")
  - *The Greco-Persian Wars* Peter Green
  - *The Greek and Persian Wars 499-386 BC* Philip De Souza
  - *The Heritage of Persia* [Richard N.
    Frye](../Page/Richard_N._Frye.md "wikilink")
  - *History of the Persian Empire* A.T. Olmstead
  - *The Persian Empire* Lindsay Allen
  - *The Persian Empire* J.M. Cook
  - *Persian Fire: The First World Empire and the Battle for the West*
    Tom Holland
  - *Pictorial History of Iran: Ancient Persia Before Islam 15000
    B.C.-625 A.D.* Amini Sam
  - *Timelife Persians: Masters of the Empire (Lost Civilizations)*
  - Dandamaev, M.A. *A Political History of the Achaemenid Empire*.
    Leiden: Brill Academic Publishers, 1989 (ISBN 978-90-04-09172-6).

## 外部連結

  - [Persian
    History](http://www.persiansarenotarabs.com/persian-history)
  - [Livius.org on
    Achaemenids](http://www.livius.org/aa-ac/achaemenians/achaemenians.html)
  - [ČIŠPIŠ](http://www.iranica.com/articles/cispis-opers)
  - [The Behistun
    Inscription](http://www.livius.org/be-bm/behistun/behistun01.html)
  - [Livius.org on Achaemenid Royal
    Inscriptions](http://www.livius.org/aa-ac/achaemenians/inscriptions.html)

[Category:伊朗歷史](../Category/伊朗歷史.md "wikilink")
[Category:已不存在的亞洲國家](../Category/已不存在的亞洲國家.md "wikilink")
[Category:阿契美尼德帝國](../Category/阿契美尼德帝國.md "wikilink")
[Category:已不存在的帝國](../Category/已不存在的帝國.md "wikilink")

1.  [大流士一世改革与波斯帝国的全盛](http://www.scopsr.gov.cn/whsh/lsbl/ggdf/201308/t20130801_233680.html)
2.  晏绍祥，[波斯帝国的统治图景：有“集权”无“专制”](http://news.xinhuanet.com/book/2013-07/01/c_124935370.htm)
    。