**阿富汗地理位置**位于[中亚](../Page/中亚.md "wikilink")，北方各与[土库曼](../Page/土库曼.md "wikilink")、[乌兹别克](../Page/乌兹别克.md "wikilink")、[塔吉克三国交接](../Page/塔吉克.md "wikilink")，东方和[中国接壤](../Page/中国.md "wikilink")，东南方与[巴基斯坦交界](../Page/巴基斯坦.md "wikilink")，西方和[伊朗交界](../Page/伊朗.md "wikilink")。阿富汗是一个[内陆国家](../Page/内陆国家.md "wikilink")，从东北部到西南部横贯的[兴都库什山脉将国家分开](../Page/兴都库什山脉.md "wikilink")，形成一个[瓦罕走廊](../Page/瓦罕走廊.md "wikilink")。
[阿富汗位于中亚](../Page/阿富汗.md "wikilink")、或者中亚的心脏；具体来讲，她在[伊朗高原之上](../Page/伊朗高原.md "wikilink")，面积647,500[平方公里](../Page/平方公里.md "wikilink")。这国家就像是被陆地给锁住一样，而且多[山](../Page/山.md "wikilink")，境内包含了大部份的[兴都库什山](../Page/兴都库什山.md "wikilink")。国内有四条主要的河流：[阿姆河](../Page/阿姆河.md "wikilink")、[哈里路得河](../Page/哈里路得河.md "wikilink")、[喀布尔河和](../Page/喀布尔河.md "wikilink")[赫尔曼德河](../Page/赫尔曼德河.md "wikilink")。还有几条较小的河流以及**一对小湖**。

**地理坐标:**

  - 33 00 N, 65 00 E

## 面积

**总面积：**

  - 647,500 平方公里

**水域面积:**

  - 0 平方公里

**陆地面积:**

  - 647,500 平方公里

**陆地边界线:**

  - 共计: 5,529 公里
  - 交界国家: [中国](../Page/中国.md "wikilink")76 公里,
    [伊朗](../Page/伊朗.md "wikilink")936 公里,
    [巴基斯坦](../Page/巴基斯坦.md "wikilink")2,430 公里,
    [塔吉克斯坦](../Page/塔吉克斯坦.md "wikilink")1,206 公里,
    [土库曼斯坦](../Page/土库曼斯坦.md "wikilink")744 公里,
    [乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")137 公里

## 气候

阿富汗属于[亚热带干旱半干旱气候](../Page/亚热带干旱半干旱气候.md "wikilink")，干燥少雨，冬季较凉，夏季酷热。

## 地形

[Northwestern_Afghanistan.jpg](https://zh.wikipedia.org/wiki/File:Northwestern_Afghanistan.jpg "fig:Northwestern_Afghanistan.jpg")
[Afghan_topo_en.jpg](https://zh.wikipedia.org/wiki/File:Afghan_topo_en.jpg "fig:Afghan_topo_en.jpg")
[Mountains_of_Afghanistan.jpg](https://zh.wikipedia.org/wiki/File:Mountains_of_Afghanistan.jpg "fig:Mountains_of_Afghanistan.jpg")
大部分地区属[伊朗高原](../Page/伊朗高原.md "wikilink")，[平原分布在西南部和北部](../Page/平原.md "wikilink")。

  - <span style="text-decoration: underline;">農業態勢</span>
      - **自然災害**

且大部份是崎岖的山脉—[兴都库什山脉以及其延伸的范围](../Page/兴都库什山脉.md "wikilink")；平原位于北部和西南部以及靠近南部与[巴基斯坦交界上的广大的红黄色沙漠](../Page/巴基斯坦.md "wikilink")。發生在[興都庫什山脈的破壞性](../Page/興都庫什山脈.md "wikilink")[地震](../Page/地震.md "wikilink")；而在南部和西南部，则有[洪灾](../Page/洪灾.md "wikilink")、
[乾旱的发生](../Page/乾旱.md "wikilink")。陆地封锁性，兴都库什山一路从东北向西南延伸，分割了国家内的一些北部[省份](../Page/省.md "wikilink")；最高的点，位于[瓦罕走廊](../Page/瓦罕走廊.md "wikilink")。

  -   - **灌溉土地**

<!-- end list -->

  -

      -
        23,860平方公里（1998年）

<!-- end list -->

  -   - **土地使用**
          - 可耕地:12.13%
          - 可常穫:0.22%
          - 其他:87.65%（2001年）

<!-- end list -->

  -   - **海拔极限:**
          - 最低点: [阿姆河](../Page/阿姆河.md "wikilink")( Amu Darya ) 258 公尺
          - 最高峰: [诺夏克峰](../Page/诺夏克峰.md "wikilink") （Nowshak） 7,485 公尺



## 自然资源

[天然气](../Page/天然气.md "wikilink")、[石油](../Page/石油.md "wikilink")、[煤炭](../Page/煤炭.md "wikilink")、[铜](../Page/铜.md "wikilink")、[铬铁矿](../Page/铬铁矿.md "wikilink")、[滑石](../Page/滑石.md "wikilink")、[重晶石](../Page/重晶石.md "wikilink")、[硫磺](../Page/硫磺.md "wikilink")、[铅](../Page/铅.md "wikilink")、[锌](../Page/锌.md "wikilink")、[铁矿](../Page/铁.md "wikilink")、[盐](../Page/食盐.md "wikilink"),
[云母及](../Page/云母.md "wikilink")[绿宝石](../Page/绿宝石.md "wikilink")。

**土地使用:**

  - [耕地](../Page/耕地.md "wikilink"): 12.13%
  - 永久性耕地: 0.22%
  - 其他: 87.65% (2001年)

**水田:**

  - 23,860 平方公里(1998年)

## 环境问题

有限的自然[淡水资源](../Page/淡水.md "wikilink");
没有充分的[饮用水供应](../Page/饮用水.md "wikilink"); 土壤退化;
[过度放牧](../Page/过度放牧.md "wikilink");
砍伐森林(许多原始森林被作为[燃料和](../Page/燃料.md "wikilink")[建筑材料砍伐](../Page/建筑材料.md "wikilink"));
[沙漠化](../Page/沙漠化.md "wikilink");
[空气和](../Page/空气.md "wikilink")[水污染](../Page/水污染.md "wikilink")。

## 外部連結

  - [阿富汗地形圖 1:300 000](http://www.cesty.in/afghanistan#mapy_afghanistanu)
    www.cesty.in

[\*](../Category/阿富汗地理.md "wikilink")