“[塔利班](../Page/塔利班.md "wikilink")”上台之前，**阿富汗的政治体系**已土崩瓦解。所有的政坛关键人物都拥有私人武装，而且在军事上大施影响。随着[纳吉布拉的倒台](../Page/纳吉布拉.md "wikilink")，整个国家便由[阿富汗圣战伊斯兰联盟所控制](../Page/阿富汗圣战伊斯兰联盟.md "wikilink")。而1995年起，“塔利班”武装控制了[阿富汗伊斯兰酋长国大部分地域](../Page/阿富汗伊斯兰酋长国.md "wikilink")，随后又对政府进行攻击，直至1996年夺取政权。

## 国名

[阿富汗全称阿富汗伊斯兰共和国](../Page/阿富汗.md "wikilink")。

**前政权：**

  - [阿富汗共和国](../Page/阿富汗共和国.md "wikilink")

**政体：**

  - [总统制](../Page/总统制.md "wikilink")[伊斯兰共和国](../Page/伊斯兰共和国.md "wikilink")

**首都:**

  - [喀布尔](../Page/喀布尔.md "wikilink")

## 行政区划

全国划分为34个省（velayat），各省名称：[喀布尔省](../Page/喀布尔省.md "wikilink")、[巴达赫尚省](../Page/巴达赫尚省.md "wikilink")、[塔哈尔省](../Page/塔哈尔省.md "wikilink")、[昆都士省](../Page/昆都士省.md "wikilink")、[巴尔赫省](../Page/巴尔赫省.md "wikilink")、[朱兹詹省](../Page/朱兹詹省.md "wikilink")、[法利亚布省](../Page/法利亚布省.md "wikilink")、[巴德吉斯省](../Page/巴德吉斯省.md "wikilink")、[赫拉特省](../Page/赫拉特省.md "wikilink")、[古尔省](../Page/古尔省.md "wikilink")、[萨尔普勒省](../Page/萨尔普勒省.md "wikilink")、[萨曼甘省](../Page/萨曼甘省.md "wikilink")、[巴格兰省](../Page/巴格兰省.md "wikilink")、[巴米扬省](../Page/巴米扬省.md "wikilink")、[帕尔旺省](../Page/帕尔旺省.md "wikilink")、[瓦尔达克省](../Page/瓦尔达克省.md "wikilink")、[卡比萨省](../Page/卡比萨省.md "wikilink")、[拉格曼省](../Page/拉格曼省.md "wikilink")、[努尔斯坦省](../Page/努尔斯坦省.md "wikilink")、[库纳尔省](../Page/库纳尔省.md "wikilink")、[楠格哈尔省](../Page/楠格哈尔省.md "wikilink")、[洛加尔省](../Page/洛加尔省.md "wikilink")、[加兹尼省](../Page/加兹尼省.md "wikilink")、[乌鲁兹甘省](../Page/乌鲁兹甘省.md "wikilink")、[法拉省](../Page/法拉省.md "wikilink")、[尼姆鲁兹省](../Page/尼姆鲁兹省.md "wikilink")、[赫尔曼德省](../Page/赫尔曼德省.md "wikilink")、[坎大哈省](../Page/坎大哈省.md "wikilink")、[扎布尔省](../Page/扎布尔省.md "wikilink")、[帕克蒂亚省](../Page/帕克蒂亚省.md "wikilink")、[帕克蒂卡省](../Page/帕克蒂卡省.md "wikilink")、[霍斯特省](../Page/霍斯特省.md "wikilink")、[潘杰希尔省](../Page/潘杰希尔省.md "wikilink")、[戴孔迪省](../Page/戴孔迪省.md "wikilink")。

## 独立

1919年8月19日从[英国控制下独立](../Page/英国.md "wikilink")

## 国庆节

  - [独立日](../Page/独立日.md "wikilink")8月19日(1919年)

## 法律

**宪法:**

  - 新[宪法在](../Page/宪法.md "wikilink")2003年12月14日- 2004年1月4日起草;
    2004年1月16日签字

**法律体系：**

  - 根据新宪法,
    [法律不应该](../Page/法律.md "wikilink")"违背[伊斯兰教](../Page/伊斯兰教.md "wikilink")";
    在[社会正义](../Page/社会正义.md "wikilink")，保护人的尊严,
    保护[人权](../Page/人权.md "wikilink"),
    [民主政治的实施](../Page/民主政治.md "wikilink"),
    以及保证全国团结和所有[族群和](../Page/族群.md "wikilink")[部落之间平等的基础上](../Page/部落.md "wikilink")，努力创建一个繁荣和进步的社会;
    国家将遵守阿富汗签署了的[联合国宪章](../Page/联合国宪章.md "wikilink")、[国际公约和条约](../Page/国际公约.md "wikilink"),
    [世界人权宣言](../Page/世界人权宣言.md "wikilink")。

选举权：

  - 18岁；[普选权](../Page/普选权.md "wikilink")

## 行政机构

**[国家元首](../Page/国家元首.md "wikilink"):**

  - [阿富汗伊斯兰共和国总统](../Page/阿富汗总统.md "wikilink")：现任总统为[阿什拉夫·加尼·艾哈迈德扎伊](../Page/阿什拉夫·加尼·艾哈迈德扎伊.md "wikilink");
    总统既是国家元首又是政府首脑;
  - [阿富汗前国王](../Page/阿富汗君主列表.md "wikilink")[查希尔被尊称为](../Page/查希尔.md "wikilink")"国父"并象征性地主持某些场合,
    但没有任何控制权力; 尊称不能继承。

**[首席执行官](../Page/首席执行官.md "wikilink"):**

  - [阿富汗伊斯兰共和国总统为](../Page/阿富汗总统.md "wikilink")[国家元首兼任](../Page/国家元首.md "wikilink")[政府首脑](../Page/政府首脑.md "wikilink")，因此实行[总统制](../Page/总统制.md "wikilink")，阿富汗特设[首席执行官作为非法定的政府代表](../Page/阿富汗首席执行官.md "wikilink")。
  - [阿富汗首席执行官职务相当于之前废除的](../Page/阿富汗首席执行官.md "wikilink")[阿富汗总理](../Page/阿富汗总理.md "wikilink")

**内阁:**

  - 27位部长; 根据新宪法, 各部部长由总统任命并[国民议会批准](../Page/国民议会.md "wikilink")。

**竞选:** 总统和二位副总统由直接投票选举产生，五年一届; 如果在第一轮投票中候选人没有得到50%以上的投票,
二名候选人在第二轮进行多数表决; 总统最多只能任两届;
最后一次选举在2004年10月9日举行，(下一次选举将在2009年举行)
（参看[2004年阿富汗总统选举](../Page/2004年阿富汗总统选举.md "wikilink")）

**2004年总统选举的结果:**

  - [哈米德·卡尔扎伊被选为总统](../Page/哈米德·卡尔扎伊.md "wikilink"); 投票百分比：哈米德·卡尔扎伊-
    55.4%, [尤努斯·卡努尼](../Page/尤努斯·卡努尼.md "wikilink") - 16.3%, Mohammad
    MOHAQEQ - 11.6%, [拉希德·杜斯塔姆](../Page/拉希德·杜斯塔姆.md "wikilink") 10.0%,
    [佩德拉姆](../Page/佩德拉姆.md "wikilink") -
    1.4%,[马苏达·贾拉尔](../Page/马苏达·贾拉尔.md "wikilink")
    - 1.2%

## 立法机构

根据新宪法,
[两院制的](../Page/两院制.md "wikilink")[国民议会由](../Page/国民议会.md "wikilink")[人民院](../Page/人民院.md "wikilink")（下院）和[长老院](../Page/长老院.md "wikilink")（上院）组成(不超过249个席位)。5年一次直接选举,
[支尔格大会或](../Page/支尔格大会.md "wikilink")[长老院](../Page/长老院.md "wikilink")(102个席位,
三分之一从各省、区管理委员会成员中选出，为期4 年, 三分之一从地方区自治会选出，为期三年, 还有三分之一由总统任命，为期五年;
总统任命包括2个Kuchis代表和二个残疾人代表; 总统任命者中一半将是妇女)

在极少数情况下政府也可以召集支尔格大会，商讨关于独立、国家主权和领土完整的问题; 它可以提出修正宪法和弹劾总统;
它由省和区自治会的国民议会的成员和主席组成

**竞选:**
2005年春季（参看[2005年阿富汗国民议会选举](../Page/2005年阿富汗国民议会选举.md "wikilink")）

## 司法机关

新宪法建立了一个有九名成员的Stera Mahkama或最高法院(其九名法官由总统任命，为期10年，并由下院认可)， 还有地方法院和上诉法院;
还有司法部长;
有一个独立的阿富汗独立人权委员以确保由[波恩协议建立责成调查践踏人权和战争罪行](../Page/波恩协议.md "wikilink")

## 政党

**政党和领导人:**

  - 只包括被司法部批准的政党: Afghan Millat \[Anwarul Haq AHADI\]; De Afghanistan De
    Solay Ghorzang Gond \[Shahnawaz TANAI\]; De Afghanistan De Solay
    Mili Islami Gond \[Shah Mahmood Polal ZAI\];
    [阿富汗伊斯兰革命运动](../Page/阿富汗伊斯兰革命运动.md "wikilink")
    \[Mohammad Asif MOHSINEE\]; Hezb-e-Aarman-e-Mardum-e-Afghanistan
    \[Iihaj Saraj-u-din ZAFAREE\]; Hezb-e-Aazadee Afghanistan \[Abdul
    MALIK\]; Hezb-e-Adalat-e-Islami Afghanistan \[Mohammad Kabeer
    MARZBAN\]; Hezb-e-Afghanistan-e-Wahid \[Mohammad Wasil RAHEEMEE\];
    Hezb-e-Afghan Watan Islami Gond \[NA leader\]; Hezb-e-Congra-e-Mili
    Afghanistan \[Lateef PIDRAM\]; Hezb-e-Falah-e-Mardum-e-Afghanistan
    \[Mohammad ZAREEF\]; Hezb-e-Libral-e-Aazadee
    Khwa-e-Mardum-e-Afghanistan \[Ajmal SOHAIL\]; Hezb-e-Hambastagee
    Mili Jawanan-e-Afghanistan \[Mohammad Jamil KARZAI\];
    Hezb-e-Hamnbatagee-e-Afghanistan \[Abdul Khaleq NEMAT\];
    Hezb-e-Harakat-e-Mili Wahdat-e-Afghanistan \[Moahammad Nadir
    AATASH\]; Hezb-e-Harak-e-Islami Mardum-e-Afghanistan \[Ilhaj Said
    Hssain ANWARY\]; Hezb-e-Ifazat Az Uqoq-e-Bashar Wa
    Inkishaf-e-Afghanistan \[Baryalai NASRATEE\];
    Hezb-e-Istiqlal-e-Afghanistan \[Dr. Gh. Farooq NIJZRABEE\];
    Hezb-e-Jamhoree Khwahan \[Sibghatullah SANJAR\]; Hezb-e-Kar Wa
    Tawsiha-e-Afghanistan \[Zulfiar OMID\]; Hezb-e-Mili Afghanistan
    \[Abdul Rasheed AARYAN\]; Hezb-e-Mili Wahdat-e-Aqwam-e-Islami
    Afghanistan \[Mohammad Shah KHOGYANEE\]; Hezb-e-Nuhzhat-e-Mili
    Afghanistan \[Ahmad Wali MASOUD\]; Hezb-e-Paiwand-e-Mili Afghanistan
    \[Said Mansoor NADIRI\]; Hezb-e-Rastakhaiz-e-Islami
    Mardum-e-Afghanistan \[Said ZAHIR\];
    Hezb-e-Refah-e-Mardum-e-Afghanistan \[Mia Gul WASEEQ\];
    Hezb-e-Risalat-e-Mardum-e-Afghanistan \[Noor Aqa ROEEN\];
    Hezb-e-Sahadat-e-Mardum-e-Afghanistan \[Mohammad Zubair PAIROZ\];
    Hezb-e-Sahadat-e-Mili Wa Islami Afghanistan \[Mohammad Usman
    SALIGZADA\]; Hezb-e-Sulh-e-Mili Islami Aqwam-e-Afghanistan \[Abdul
    Qahir SHARYATEE\]; Hezb-e-Sulh Wa Wahdat-e-Mili Afghanistan \[Abdul
    Qadir IMAMEE\]; Hezb-e-Tafahum-e-Wa Democracy Afghanistan \[Ahamad
    SHAHEEN\]; [阿富汗伊斯兰统一党](../Page/阿富汗伊斯兰统一党.md "wikilink") \[
    [卡里姆·哈利利](../Page/卡里姆·哈利利.md "wikilink") \];
    Hezb-e-Wahdat-e-Islami Mardum-e-Afghanistan \[Haji Mohammad
    MUHAQIQ\]; Hezb-e-Wahdat-e-Mili Afghanistan \[Abdul Rasheed
    Jalili\]; Jamahat-ul-Dahwat ilal Qurhan-wa-Sunat-ul-Afghanistan
    \[Mawlawee Samiullah NAJEEBEE\];
    [阿富汗伊斯兰民族运动](../Page/阿富汗伊斯兰民族运动.md "wikilink")
    \[ [阿卜杜尔·拉希德·杜斯塔姆](../Page/阿卜杜尔·拉希德·杜斯塔姆.md "wikilink") \];
    Mahaz-e-Mili Islami Afghanistan \[Said Ahmad GAILANEE\];
    Majmah-e-Mili Fahaleen-e-Sulh-e-Afghanistan \[Shams ul Haq Noor
    SHAMS\]; Nuhzat-e-Aazadee Wa democracy Afghanistan \[Abdul Raqeeb
    Jawid KUHISTANEE\]; Nuhzat-e-Hambastagee Mili Afghanistan \[Peer
    Said Ishaq GAILANEE\]; Sazman-e-Islami Afghanistan-e-Jawan \[Siad
    Jawad HUSSAINEE\]; Tahreek Wahdat-e-Mili \[Sultan Mahmood DHAZI\]
    (30 Sep 2004)

**政治压力团体和领导人:**

  - [阿富汗伊斯兰促进会](../Page/阿富汗伊斯兰促进会.md "wikilink"), \[
    前总统[布尔汉努丁·拉巴尼](../Page/布尔汉努丁·拉巴尼.md "wikilink")
    \]; [阿富汗圣战者伊斯兰联盟](../Page/阿富汗圣战者伊斯兰联盟.md "wikilink"), \[
    [阿卜杜尔·拉苏尔·萨亚夫](../Page/阿卜杜尔·拉苏尔·萨亚夫.md "wikilink")
    \]; 还有少数的君主主义者、[共产主义者](../Page/共产主义.md "wikilink"), 民主主义团体

**参与国际组织:** [亚洲开发银行](../Page/亚洲开发银行.md "wikilink")，
[CP](../Page/CP.md "wikilink"), [经济合作组织](../Page/经济合作组织.md "wikilink"),
[粮食与农业组织](../Page/粮食与农业组织.md "wikilink"),
[G-77](../Page/G-77.md "wikilink"),
[古阿姆集團](../Page/古阿姆集團.md "wikilink"),
[国际能源机构](../Page/国际能源机构.md "wikilink"),
[国际复兴开发银行](../Page/国际复兴开发银行.md "wikilink"),
[国际民间航空组织](../Page/国际民间航空组织.md "wikilink"),
[ICCt](../Page/ICCt.md "wikilink"), [ICRM](../Page/ICRM.md "wikilink"),
[国际开发协会](../Page/国际开发协会.md "wikilink"),
[IDB](../Page/IDB.md "wikilink"), [IFAD](../Page/IFAD.md "wikilink"),
[国际金融组织](../Page/国际金融组织.md "wikilink"),
[IFRCS](../Page/IFRCS.md "wikilink"),
[国际劳工组织](../Page/国际劳工组织.md "wikilink"),
[国际货币基金组织](../Page/国际货币基金组织.md "wikilink"),
[国际警察组织](../Page/国际警察组织.md "wikilink"),
[国际奥林匹克委员会](../Page/国际奥林匹克委员会.md "wikilink"),
[IOM](../Page/IOM.md "wikilink"),
[国际电信联盟](../Page/国际电信联盟.md "wikilink"),
[MIGA](../Page/MIGA.md "wikilink"), [NAM](../Page/NAM.md "wikilink"),
[北约](../Page/北约.md "wikilink"),
[伊斯兰会议组织](../Page/伊斯兰会议组织.md "wikilink"),
[OPCW](../Page/OPCW.md "wikilink"), [OSCE](../Page/OSCE.md "wikilink"),
[SACEP](../Page/SACEP.md "wikilink"), [联合国](../Page/联合国.md "wikilink"),
[联合国贸易与发展会议](../Page/联合国贸易与发展会议.md "wikilink"),
[科教文组织](../Page/科教文组织.md "wikilink"),
[联合国工业开发组织](../Page/联合国工业开发组织.md "wikilink"),
[万国邮政联盟](../Page/万国邮政联盟.md "wikilink"),
[WCO](../Page/WCO.md "wikilink"),
[世界劳工组织](../Page/世界劳工组织.md "wikilink"),
[世界卫生组织](../Page/世界卫生组织.md "wikilink"),
[世界气象组织](../Page/世界气象组织.md "wikilink"),
[世界贸易组织](../Page/世界贸易组织.md "wikilink") (观察员),
[WToO](../Page/WToO.md "wikilink")

## 国旗

*`参看`[`阿富汗国旗`](../Page/阿富汗国旗.md "wikilink")*

三条相等的垂直条纹,
分别是[黑色](../Page/黑色.md "wikilink")、[红色](../Page/红色.md "wikilink"),
和[绿色](../Page/绿色.md "wikilink"),
红色条纹中间是金色的[国徽](../Page/国徽.md "wikilink");
国徽上一个花圈从左到右围绕着一座象征寺庙结构的建筑，还有伊斯兰教的题字在上面

## 国际问题

联合国已经将超过200万的阿富汗难民遣送回国，但是仍然有数百万的难民根据自己的选择露营在伊朗和巴基斯坦。伊朗和巴基斯坦仍然在部落控制的边境地区巡逻，已阻止[恐怖分子和其他非法的穿越边境的活动](../Page/恐怖分子.md "wikilink")。regular
meetings between Pakistani and Coalition allies aim to resolve periodic
claims of boundary encroachments; occasional conflicts over
water-sharing arrangements with Amu Darya and Helmand River states

## 难民和国内流离失所者

  - 167,000 - 200,000 （2004年）

## 违禁药物

阿富汗是世界上最大的鸦片生产者。在2004年[罂粟的种植达到了](../Page/罂粟.md "wikilink")206,700公顷的空前规模。解瘾药的努力相当不成功；潜在的鸦片生产达到了4950吨；如果全部的鸦片生产出[海洛因可能有](../Page/海洛因.md "wikilink")582吨；大麻的来源地；遍及全国的麻醉药品实验室，不稳定的毒品贩卖源头以及一些反政府组织的资金来源，在[欧洲](../Page/欧洲.md "wikilink")80-90%的海洛因消耗来自于阿富汗的鸦片；通过非正式的金融网络洗钱

## 參見

  - [法齊婭·古菲](../Page/法齊婭·古菲.md "wikilink")

[阿富汗政治](../Page/category:阿富汗政治.md "wikilink")