|          |
| -------- |
| **阿尔伯特** |
| 性别:      |
| 起源:      |
| 含义:      |
|          |

**阿尔伯特**（）是一個常見的男子名，亦可作为姓氏，通常在歐美文化圈使用，英文讀音譯為中文為阿爾伯特，法文讀音譯為中文為**阿爾貝**，在此列出名為阿尔伯特的著名人士或動物：

## 美国人名

  - [阿尔伯特·爱因斯坦](../Page/阿尔伯特·爱因斯坦.md "wikilink")（Albert
    Einstein）[美籍](../Page/美国.md "wikilink")[德裔](../Page/德国.md "wikilink")[理論物理學家](../Page/理論物理學.md "wikilink")，1921年[诺贝尔物理学奖获得者](../Page/诺贝尔物理学奖.md "wikilink")，[相对论的创立者](../Page/相对论.md "wikilink")
  - [阿尔伯特·迈克耳孙](../Page/阿尔伯特·迈克耳孙.md "wikilink")（Albert Abraham
    Michelson）美籍德裔物理学家，1907年诺贝尔物理学奖获得者
  - [艾伯特·乔治·威尔逊](../Page/艾伯特·乔治·威尔逊.md "wikilink")（Albert George
    Wilson）美国[天文学家](../Page/天文学家.md "wikilink")
  - [艾伯特·戈尔](../Page/艾伯特·戈尔.md "wikilink")（Albert Arnold Gore
    Jr.）美国政治家，1993－2001年[美国副总统](../Page/美国副总统.md "wikilink")
  - [阿尔伯特·魏德迈](../Page/阿尔伯特·魏德迈.md "wikilink")（Albert Coady
    Wedemeyer）美国军人，1944年任盟军中国战区参谋长

## 法国人名

  - [阿尔贝·勒布伦](../Page/勒布伦.md "wikilink")（Albert
    Lebrun）[法兰西第三共和国](../Page/法兰西第三共和国.md "wikilink")1932-1940年总统
  - [雅克-维克托-阿尔贝，第四世布罗伊公爵](../Page/阿尔贝·德·布罗伊_\(第四代布罗伊公爵\).md "wikilink")（Jacques-Victor-Albert）法国政治家，两任[法国总理](../Page/法国总理.md "wikilink")
  - [阿尔贝·加缪](../Page/阿尔贝·加缪.md "wikilink")（Albert
    Camus）1957年[諾貝爾文學獎得主](../Page/諾貝爾文學獎.md "wikilink")
  - [艾尔伯·费尔](../Page/艾尔伯·费尔.md "wikilink")（Albert
    Fert）2007年[诺贝尔物理学奖得主](../Page/诺贝尔物理学奖.md "wikilink")

## 著名人士

  - [阿尔贝二世
    (比利时)](../Page/阿尔贝二世_\(比利时\).md "wikilink")：[比利時前](../Page/比利時.md "wikilink")(退位)國王。
  - [阿尔贝二世
    (摩纳哥)](../Page/阿尔贝二世_\(摩纳哥\).md "wikilink")：[摩纳哥現任君主](../Page/摩纳哥.md "wikilink")。
  - [阿爾伯特事件簿中的阿爾伯特](../Page/阿爾伯特事件簿.md "wikilink")(Albert in Albert's
    Diary)
  - [艾尔伯图斯·麦格努斯](../Page/艾尔伯图斯·麦格努斯.md "wikilink")：中世纪神学家，[天主教](../Page/天主教.md "wikilink")[教会圣师](../Page/教会圣师.md "wikilink")

## 瑞士人名

  - [艾伯特·霍夫曼](../Page/艾伯特·霍夫曼.md "wikilink")（Albert
    Hofmann）[瑞士化学家](../Page/瑞士.md "wikilink")

## 德国人名

  - [阿尔伯特·斯佩尔](../Page/阿尔伯特·斯佩尔.md "wikilink")（Berthold Konrad Hermann
    Albert
    Speer）德国[建筑师](../Page/建筑师.md "wikilink")，[纳粹德国装备部长](../Page/纳粹德国.md "wikilink")，[第二次世界大战战犯](../Page/第二次世界大战.md "wikilink")

## 著名動物

  - [阿尔伯特 (猴子)](../Page/阿尔伯特_\(猴子\).md "wikilink")：第一隻猴子太空員，但在飛行途中即窒息而死。
  - [阿尔伯特二世 (猴子)](../Page/阿尔伯特二世_\(猴子\).md "wikilink")：第一隻成功登上太空的猴子。

## 地名

  - [艾伯特湖](../Page/艾伯特湖.md "wikilink")（Lake Albert）非洲第七大湖
  - [艾伯特縣](../Page/艾伯特縣.md "wikilink")（Elbert County）：[艾伯特縣
    (科羅拉多州)](../Page/艾伯特縣_\(科羅拉多州\).md "wikilink")、[艾伯特縣
    (喬治亞州)](../Page/艾伯特縣_\(喬治亞州\).md "wikilink")

## 参看

  - [艾伯塔](../Page/艾伯塔.md "wikilink")（Alberta）
  - [阿尔贝蒂纳](../Page/阿尔贝蒂纳.md "wikilink")（Albertina）
  - [阿尔布雷希特](../Page/阿尔布雷希特.md "wikilink")（Albrecht）
  - [阿爾伯特事件簿](../Page/阿爾伯特事件簿.md "wikilink")(Albert's Diary)

[category:名](../Page/category:名.md "wikilink")