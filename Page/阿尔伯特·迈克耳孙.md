**阿尔伯特·亚伯拉罕·迈克耳孙**（，），又譯「邁克生」、「迈克耳逊」，[波蘭裔](../Page/波蘭人.md "wikilink")[美国藉](../Page/美国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，以测量[光速而闻名](../Page/光速.md "wikilink")，尤其是[迈克耳孙-莫雷实验](../Page/迈克耳孙-莫雷实验.md "wikilink")。1907年[诺贝尔物理学奖获得者](../Page/诺贝尔物理学奖.md "wikilink")。

## 生平

迈克耳孙出生在[波兰的小镇](../Page/波兰.md "wikilink")[史翠诺](../Page/史翠诺.md "wikilink")（当时属于[普鲁士帝国的](../Page/普鲁士帝国.md "wikilink")[波兹南省](../Page/波兹南省.md "wikilink")）的一个犹太商人家庭。在他还只有两岁时全家移民[美国](../Page/美国.md "wikilink")，跟随着作为商人的父亲，他的成长大致上经历了[加利福尼亚的矿业小镇Murphy](../Page/加利福尼亚.md "wikilink")、内华达州的弗吉尼亚城。

1869年，迈克耳孙进入[美国海军学院](../Page/美国海军学院.md "wikilink")（位于[马里兰州首府](../Page/马里兰州.md "wikilink")[安纳波利斯](../Page/安纳波利斯.md "wikilink")），并于1873年毕业。迈克耳孙早先就着迷于科学，特别是[光速的测量问题](../Page/光速.md "wikilink")。1881年，海军委派他到欧洲学习两年。1883年，他接受了[凱斯西儲大學](../Page/凱斯西儲大學.md "wikilink")（位于俄亥俄州[克利夫兰](../Page/克里夫蘭_\(俄亥俄州\).md "wikilink")）的邀请，成为那里的物理教授，并专心研究改进[干涉仪](../Page/干涉仪.md "wikilink")。1887年他利用这种干涉仪，和[爱德华·莫雷共同进行了著名的](../Page/爱德华·莫雷.md "wikilink")[迈克耳孙-莫雷实验](../Page/迈克耳孙-莫雷实验.md "wikilink")，这个试验排除了[以太的存在](../Page/以太.md "wikilink")。后来，他又转而用[天文光学干涉测量法进行对恒星的直径和双星间距离的测量](../Page/天文光学干涉测量法.md "wikilink")。

1889年开始，他在麻萨诸塞州伍斯特的[克拉克大学任教授](../Page/克拉克大学.md "wikilink")。1892年被指派到一个全新的大学——[芝加哥大学任物理学系第一任主任](../Page/芝加哥大学.md "wikilink")。1907年，迈克耳孙因为“发明光学干涉仪并使用其进行光谱学和基本度量学研究”而成为美国第一个[诺贝尔物理学奖获得者](../Page/诺贝尔物理学奖.md "wikilink")；同年，他获得了[科普利奖章](../Page/科普利奖章.md "wikilink")。1916年和1923年，他还分别获得了[亨利·德雷伯獎章和](../Page/亨利·德雷伯獎章.md "wikilink")[皇家天文学会金质奖章](../Page/皇家天文学会金质奖章.md "wikilink")。

月球上的[迈克耳孙环形山是以他的名字命名](../Page/迈克耳孙环形山.md "wikilink")。

迈克耳孙于1931年5月9日逝世于加利福尼亚的[帕萨迪纳](../Page/帕萨迪纳.md "wikilink")。

## 光速的测量

### 早期

早在1877年，还是[美国海军办事员时](../Page/美国海军.md "wikilink")，迈克耳孙就开始改进[萊昂·傅科的旋转镜片法](../Page/萊昂·傅科.md "wikilink")，以测量光速。1878年，他大大改进了仪器，并进行了一些初步测量。他的这些研究引起了当时的航海星曆局（Nautical
Almanac
Office）主任[西蒙·紐康的注意](../Page/西蒙·紐康.md "wikilink")。紐康自己在这方面已经有比较深入的研究计划。1879年，迈克耳孙公布了他的测量结果（299,910±50 km/s），随后就前往[华盛顿特区加入紐康](../Page/华盛顿特区.md "wikilink")，开始了他们的长时间的专业合作和个人友谊。

此时紐康的科研资金充足，又有迈克耳孙的加入，很快得出了新的观测值（299,860±30 km/s），正好在迈克耳孙的结果的（误差）临界值上。迈克耳孙也不断改进他的方法，在1883年发表了新的测量结果
299,853±60 km/s，与他导师的数字非常接近。

### 威尔逊山

1906年，美国国家标准局的 E. B. Rosa 和 N. E. Dorsey 利用一种新型的电学方法获得了299,781±10
km/s的光速值。虽然这个结果后来被证明是（由于当时的电气标准）有严重偏颇的，但这似乎也开始了测量值宁低勿高的风气。

从1920年起，迈克开始规划从威尔逊山天文台的最终测量，所用的基线延伸到Lookout
Mountain，一个约22英里远的位于圣安东尼奥山（“老秃子”）南部山脊的突出的肿块。

1922年，开始了为期两年的辛苦的基线测量，使用市面上新近研发的[不胀钢钢条](../Page/不胀钢.md "wikilink")。利用1924年建立的基线长度，光速测量在接下来的两年内得以进行，获得299,796±4
km/s的公布值。<ref>

` `</ref>

此次光速测量十分著名，但也饱受问题困扰，其中最大的问题是森林火灾导致的烟雾，模糊了反射的镜像。另外，深入细致的大地测量，尽管估计误差小于百万分之一，也很可能由于1925年6月29日的圣巴巴拉地震（约里氏6.3级）导致的基线偏移而受到了影响。

今日耳熟能详的[迈克尔逊-莫雷实验在当时就对学界有很大的影响](../Page/迈克尔逊-莫雷实验.md "wikilink")；[爱因斯坦就是使用相似的光学仪器](../Page/爱因斯坦.md "wikilink")，证实了他的[广义相对论和](../Page/广义相对论.md "wikilink")[狭义相对论](../Page/狭义相对论.md "wikilink")。这些精密的仪器以及相关的合作，离不开同时代物理学家[代顿·米勒](../Page/代顿·米勒.md "wikilink")，[亨德里克·洛伦兹和](../Page/亨德里克·洛伦兹.md "wikilink")[罗伯特·森兰的参与](../Page/罗伯特·森兰.md "wikilink")。

### 迈克耳孙、皮斯和皮尔森

## 天文干涉仪

1920年到1921年间，迈克耳孙和[皮斯在人类史上首次测量出太阳以外的星体的直径](../Page/弗朗西斯·G·皮斯.md "wikilink")。他们在[威尔逊山天文台用](../Page/威尔逊山天文台.md "wikilink")[天文光学干涉仪测量出红巨星](../Page/迈克耳孙测星干涉仪.md "wikilink")[參宿四的直径](../Page/參宿四.md "wikilink")。在此之后，迈克耳孙继续在对恒星直径和双星间距离的测量上花费了更多的精力。

## 引用和注释

## 参考资料

  - Livingston, D. M. (1973). *The Master of Light: A Biography of
    Albert A. Michelson* ISBN 0-226-48711-3 - biography by Michelson's
    daughter
  - [诺贝尔奖官方网站关于阿尔伯特·迈克耳孙生平介绍](http://nobelprize.org/nobel_prizes/physics/laureates/1907/michelson-bio.html)
  - 迈克耳孙著作的电子版可以从[古登堡计划获得](../Page/古登堡计划.md "wikilink")：*[Experimental
    Determination of the Velocity of
    Light](http://www.gutenberg.net/browse/BIBREC/BR11753.HTM)*

## 外部链接

  - [Albert A.
    Michelson 1852-1931](https://web.archive.org/web/20040807173118/http://math.amu.edu.pl/~zbzw/ph/sci/aam.htm)
  - [Albert Abraham Michelson National Academy of
    Science](http://books.nap.edu/books/0309025184/html/282.html)
  - [more info about Albert Abraham
    Michelson](http://www.nobel-winners.com/Physics/albert_michelson.html)
  - [Michaelson's Life and Works from the American Institute of
    Physics](http://www.aip.org/history/gap/Michelson/Michelson.html)

[M](../Category/美国物理学家.md "wikilink")
[M](../Category/诺贝尔物理学奖获得者.md "wikilink")
[M](../Category/美國諾貝爾獎獲得者.md "wikilink")
[M](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[M](../Category/英國皇家天文學會金質獎章獲得者.md "wikilink")
[M](../Category/凱斯西儲大學教師.md "wikilink")
[M](../Category/美國海軍學院校友.md "wikilink")
[M](../Category/柏林洪堡大學校友.md "wikilink")
[Category:1852年出生](../Category/1852年出生.md "wikilink")
[Category:1931年逝世](../Category/1931年逝世.md "wikilink")
[Category:亨利·德雷伯奖章获得者](../Category/亨利·德雷伯奖章获得者.md "wikilink")
[Category:马泰乌奇奖章获得者](../Category/马泰乌奇奖章获得者.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")
[Category:拉姆福德奖获得者](../Category/拉姆福德奖获得者.md "wikilink")