**阿尔及利亚战争**是1954年至1962年期間[阿爾及利亞爭取獨立的武裝力量與](../Page/阿爾及利亞.md "wikilink")[法國之間的戰爭](../Page/法國.md "wikilink")，最終法國同意阿爾及利亞獨立。

## 背景

1830年法国佔領了阿爾及利亞，建立[法属阿尔及利亚殖民地](../Page/法属阿尔及利亚.md "wikilink")，於1834年宣佈阿爾及利亞為法國[屬地](../Page/属地.md "wikilink")。在法國統治期間，大批歐洲人來到阿爾及利亞定居。1954年時，大約有100萬歐洲裔人口在阿爾及利亞生活，當中包括大量世代居於當地的[法裔居民](../Page/法兰西人.md "wikilink")。對許多法國人來說，放棄阿爾及利亞是難以接受的。

## 過程

1954年11月1日，爭取阿爾及利亞獨立的「[民族解放陣線](../Page/民族解放陣線_\(阿爾及利亞\).md "wikilink")」開始武裝反抗法國人的統治，游擊隊在該日凌晨在境內多個地點襲擊軍事設施、警崗、貨倉、通訊設施及公共設施。民族解放陣線的武裝力量稱「民族解放軍」，開始時僅有數百名武裝人員，到1957年已發展為一支人數近4萬人的武裝部隊。當中逾3萬人是匿藏在[摩洛哥及](../Page/摩洛哥.md "wikilink")[突尼西亞接壤阿爾及利亞邊境地區的外部單位](../Page/突尼西亞.md "wikilink")，他們的主要作用是分散[法軍兵力](../Page/法國軍事.md "wikilink")，迫使法軍在邊境佈置兵力阻截敵軍滲透。民族解放陣線的主要戰鬥由阿爾及利亞境內的內部成員擔當，有人估計這些內部成員有6000人，也有人估計逾25000人，另有數千人屬兼職性質的非正式成員。

在1956年至1957年間，民族解放陣線採取打了就跑的[游擊戰術](../Page/游击战.md "wikilink")，以[伏擊和](../Page/埋伏.md "wikilink")[夜襲為主](../Page/夜襲.md "wikilink")，避免與裝備較佳的法軍交戰。他們也發動一些[綁架事件](../Page/綁架.md "wikilink")，對象包括法軍、法國[僑民](../Page/僑民.md "wikilink")、懷疑通敵者或叛徒，後來更擴大至政府僱員甚至是一些不肯跟他們合作的農民。

到1956年時，法國在阿爾及利亞已投入超過40萬法軍。攻擊敵軍的戰鬥任務主要由精銳的殖民地步兵空降單位及[法國外籍兵團擔當](../Page/法國外籍兵團.md "wikilink")。有大約17萬阿爾及利亞穆斯林在法國正規軍中服役，大多數是自願加入。

在1950年代後期，法國人把200萬以上的阿爾及利亞人從家鄉（大部份在山區）強迫遷徙到平原地區，防止他們支援叛軍。

## 結果

1962年3月18日，法國與阿爾及利亞民族解放陣線雙方簽署了[埃維昂協議](../Page/埃維昂協議.md "wikilink")，同意阿爾及利亞舉行[公民投票決定是否獨立](../Page/公民投票.md "wikilink")。1962年7月1日，阿爾及利亞選民在[投票中幾乎一面倒地贊成獨立](../Page/1962年阿尔及利亚独立公投.md "wikilink")。法國在7月3日承認阿爾及利亞為一個獨立國家。

關於這次戰爭的總死亡人數，民族解放陣線估計有30萬人在這場戰爭中因戰事死亡，有些阿爾及利亞方面的數字估計為100萬人，法國官方數字則為35萬人。法軍傷亡方面，法國公佈有。BBC的一篇文章指戰爭造成3萬法國人及最少50萬阿爾及利亞人喪生。\[1\]

## 歐裔居民及哈基人的逃亡

大量歐裔人口（法文稱他們為pied-noir，意思是「[黑腳](../Page/黑腳.md "wikilink")」），包括[猶太人](../Page/猶太人.md "wikilink")，在阿爾及利亞獨立之際逃離當地，僅在1962年的幾個月內就有90萬人離開。

一些本地的阿爾及利亞[穆斯林站在法國一方](../Page/穆斯林.md "wikilink")，稱為[哈基人](../Page/哈基人.md "wikilink")。根據法國官方數字，在1962年有236000阿爾及利亞穆斯林為法國陸軍服務，有些人在正規單位，另一些則是非正規人員。有些人認為忠於法國的穆斯林及其家人多達100萬人，不過40萬人這個數字較多被引用。

在1962年，雖然法國官方政策反對，仍有約91000哈基人逃到法國。許多阿爾及利亞人視哈基人為叛徒，許多留在阿爾及利亞的哈基人在當地獨立後受到報復，法國歷史學者估計有5萬至15萬哈基人及其家人在阿爾及利亞被殺。雖然法國在[雅克·希拉克總統任內確認了哈基人所受的苦難](../Page/雅克·希拉克.md "wikilink")，不過法國對哈基人的處理方式至今仍存有爭議。

## 影視

  - **《阿爾及爾之戰》 The Battle of Algiers，Gillo Pontecorvo，Algeria /
    Italy，1965。**

## 参考文献

## 外部連結

  - [Algerian War
    Reading](https://web.archive.org/web/20050415041553/http://www.usfca.edu/fac-staff/webberm/algeria.htm)

  - [Algerian Independence Archive at
    marxists.org](http://www.marxists.org/history/algeria/index.htm)

  - [The Colonial System and Algerian
    Nationalism](http://dostoevskiansmiles.blogspot.com/2009/06/colonial-system-and-algerian.html)

  - [Photos of the Algerian War of
    Independence](http://www.ecpad.fr/tag/fonds-guerre-dalgerie) (ECPAD)

  - [Algerian National
    Liberation](http://www.globalsecurity.org/military/world/war/algeria.htm)

  - [*Pacification in Algeria: 1956–1958* by David
    Galula](http://www.rand.org/pubs/monographs/MG478-1/)

  - [Audiovisual National Institute's declassified Algeria War
    archives](http://www.ina.fr/archivespourtous/index.php?full=guerre%20d'algérie&action=ft&x=0&y=0)
    (hundreds of free video: news rushes, interviews, official speeches,
    retrospectives, etc.)

  - [Algerian War
    Retrospective](http://www.ina.fr/archivespourtous/index.php?vue=notice&from=fulltext&full=guerre+d%27alg%E9rie&num_notice=7&total_notices=763)

  - The [African Activist Archive
    Project](http://africanactivist.msu.edu/) website has material
    related to Algeria including a 1960 photograph of [Mary-Louise
    Hooper with the FLN
    underground.](http://africanactivist.msu.edu/image.php?objectid=802)
    Go to Browse and under Africa Coverage choose Algeria.

[分類:獨立戰爭](../Page/分類:獨立戰爭.md "wikilink")

[Category:法国战争](../Category/法国战争.md "wikilink")
[Category:阿爾及利亞歷史](../Category/阿爾及利亞歷史.md "wikilink")
[Category:反法情緒](../Category/反法情緒.md "wikilink")

1.  [France's 'brutal colonial
    war'](http://news.bbc.co.uk/2/hi/europe/1678557.stm)，BBC
    News，2001年11月27日。