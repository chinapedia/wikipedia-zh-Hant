**阿爾圖納**（）位於[美國](../Page/美國.md "wikilink")[賓夕法尼亞州中部](../Page/賓夕法尼亞州.md "wikilink")，是[布萊爾縣最大的城市](../Page/布萊爾縣.md "wikilink")。2000年人口49,523人。

建於1849年，1854年設鎮，1868年設市。\[1\]自2011年4月27日起，為期60天，該市以25000美元的代價改名為POM
Wonderful Presents: The Greatest Movie Ever Sold作為對同名電影的廣告。\[2\]
[Altoona_AFD_Station4.JPG](https://zh.wikipedia.org/wiki/File:Altoona_AFD_Station4.JPG "fig:Altoona_AFD_Station4.JPG")
{{-}}

## 姐妹城市

  - [聖珀爾滕](../Page/聖珀爾滕.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[A](../Category/賓夕法尼亞州城市.md "wikilink")

1.  [City of Altoona, Blair County, PA: History of
    Altoona](http://www.altoonapa.gov/altoona/cwp/view.asp?Q=503276&a=3&altoonaNav=%7C)

2.  <http://news.yahoo.com/s/ap/20110426/ap_on_en_mo/us_sponsored_city>