**阿尔本·威廉·巴克利**（**Alben William
Barkley**，），[美国律师](../Page/美国.md "wikilink")、政治家。

曾任四屆美國[肯塔基州](../Page/肯塔基州.md "wikilink")[聯邦參議員](../Page/美國參議院.md "wikilink")，在[1948年大选中作为](../Page/1948年美国总统选举.md "wikilink")[哈利·S·杜魯門的搭档参选并获胜利](../Page/哈利·S·杜魯門.md "wikilink")，1949年至1953年間出任第35任[美国副总统](../Page/美国副总统.md "wikilink")。

[Category:美国民主党联邦参议员](../Category/美国民主党联邦参议员.md "wikilink")
[Category:肯塔基州律师](../Category/肯塔基州律师.md "wikilink")
[Category:肯塔基州民主党人](../Category/肯塔基州民主党人.md "wikilink")
[Category:肯塔基州县法官](../Category/肯塔基州县法官.md "wikilink")
[Category:埃默里大學校友](../Category/埃默里大學校友.md "wikilink")
[Category:弗吉尼亚大学法学院校友](../Category/弗吉尼亚大学法学院校友.md "wikilink")
[Category:美国民主党副总统](../Category/美国民主党副总统.md "wikilink")
[Category:美國民主黨副總統候選人](../Category/美國民主黨副總統候選人.md "wikilink")
[Category:美国禁酒活动家](../Category/美国禁酒活动家.md "wikilink")