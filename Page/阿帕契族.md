[Group_of_Apaches.jpg](https://zh.wikipedia.org/wiki/File:Group_of_Apaches.jpg "fig:Group_of_Apaches.jpg")
[Apachean_ca.18-century.png](https://zh.wikipedia.org/wiki/File:Apachean_ca.18-century.png "fig:Apachean_ca.18-century.png")
[Goyaale.jpg](https://zh.wikipedia.org/wiki/File:Goyaale.jpg "fig:Goyaale.jpg")\]\]
**阿帕契族**是數個[文化上有關連的](../Page/文化.md "wikilink")[美國原住民部族的一個總稱](../Page/美國原住民.md "wikilink")，阿帕契族的語言是[阿帕切語系](../Page/阿帕切語.md "wikilink")。以現時的角度排除了有關的[納瓦約族](../Page/納瓦約族.md "wikilink")，但納瓦約族與其他阿帕契族在文化和[語言上有關連](../Page/語言.md "wikilink")，所以也被認為是阿帕契族。阿帕契人以前分散在[亞利桑那州東部](../Page/亞利桑那州.md "wikilink")、[墨西哥西北](../Page/墨西哥.md "wikilink")、[新墨西哥](../Page/新墨西哥.md "wikilink")、部分[德克薩斯州及小部分在平原上](../Page/德克薩斯州.md "wikilink")。

阿帕契族之間在政治上只有少許一致。部族使用七種不同的語言。現在阿帕契族分類包括[納瓦約族](../Page/納瓦約族.md "wikilink")、[西阿帕契族](../Page/西阿帕契族.md "wikilink")、[Chiricahua](../Page/Chiricahua.md "wikilink")、[Mescalero](../Page/Mescalero.md "wikilink")、[Jicarilla](../Page/Jicarilla.md "wikilink")、[Lipan及](../Page/Lipan.md "wikilink")[Plains
Apache](../Page/Plains_Apache.md "wikilink")（以前的[Kiowa-Apache](../Page/Kiowa-Apache.md "wikilink")）。阿帕契族現時居住在[俄克拉何馬州](../Page/俄克拉何馬州.md "wikilink")、[德克薩斯州以及](../Page/德克薩斯州.md "wikilink")[亞利桑那州的保留地](../Page/亞利桑那州.md "wikilink")。納瓦約族則居住在[美國的一大片保留地](../Page/美國.md "wikilink")。部分阿帕契人已經遷居到例如紐約等大都市。

阿帕契部族歷史上相當有勢力，與[白人抗爭達數世紀](../Page/白人.md "wikilink")。美軍在不同的衝突中發現他們是兇猛的戰士和有謀的軍事家。阿帕奇族是最后一个向美国政府投降的部落，最后一位首领是[傑羅尼莫](../Page/傑羅尼莫.md "wikilink")\[1\]（1829年6月16日-1909年2月17日）\[2\]。

[美軍](../Page/美軍.md "wikilink")[海豹六隊在](../Page/海豹六隊.md "wikilink")2011年5月1日深夜在[巴基斯坦](../Page/巴基斯坦.md "wikilink")[阿伯塔巴德展開的](../Page/阿伯塔巴德.md "wikilink")[追擊奥萨马·本·拉登代號就是](../Page/奧薩瑪·賓·拉登之死.md "wikilink")「傑羅尼莫行動」\[3\]。在1885年當年動用了超過5000名士兵去捉拿傑羅尼莫\[4\]。後因遭到阿帕契族抗議，美軍將此行動改稱為「海神之矛行動」。

## 參考文獻

  - Hammond, George P. and Rey, Agapito (editors). *Narratives of the
    Coronado Expedition 1540-1542.* Albuquerque: University of New
    Mexico Press, 1940.
  - Henderson, Richard. “*Replicating Dog Travois Travel on the Northern
    Plains.*” Plains Anthropologist, V39:145-59, 1994.
  - Plog, Stephen. *Ancient Peoples of the American Southwest.* Thames
    and London, LTD, London, England, 1997. ISBN 0-500-27939-X.

## 外部連結

  - [Tonto Apache Tribe (Arizona Intertribal
    Council)](https://web.archive.org/web/20041210013654/http://www.itcaonline.com/tribes_tonto.html)
  - [Yavapai-Apache Nation (Arizona Intertribal
    Council)](https://web.archive.org/web/20110709171818/http://www.itcaonline.com/tribes_campverd.html)
  - [White Mountain Apache Tribe (Arizona Intertribal
    Council)](https://web.archive.org/web/20110927131821/http://www.itcaonline.com/tribes_whitemtn.html)
  - [San Carlos Apache Tribe (Arizona Intertribal
    Council)](https://web.archive.org/web/20041210012103/http://www.itcaonline.com/tribes_sancarl.html)
  - [official Yavapai-Apache Nation
    website](http://www.yavapai-apache.org/)
  - [about San Carlos
    Apache](https://web.archive.org/web/20091028051522/http://www.geocities.com/~zybt/apache.htm)
  - [White Mountain Apache
    photographs](https://web.archive.org/web/20011102025642/http://www.geocities.com/coqrico/apachealbum1.html)
  - [photos, facts, opinions on White Mountain Apaches & the Fort Apache
    Reservation](https://web.archive.org/web/20010428022241/http://www.geocities.com/coqrico/)
  - [Puberty Ceremony of White Mountain Apaches (information &
    photo)](https://web.archive.org/web/20010405044733/http://www.geocities.com/coqrico/apachedance.html)
  - [US government's plan to exterminate
    Apaches](https://web.archive.org/web/20020220155744/http://www.geocities.com/coqrico/apachewren1.html)
  - [Apache Language
    Sample](http://www.language-museum.com/a/apache.php)
  - [a nice bibliography from Kevin
    Reeve](http://koransky.com/Trackers/Other/Apache.html)
  - [Indians of Arizona (from History of
    Arizona)](http://southwest.library.arizona.edu/hav7/body.1_div.1.html)
  - [The Apache (from History of
    Arizona)](http://southwest.library.arizona.edu/hav7/body.1_div.2.html)

## 参考

[A](../Category/美國原住民部落.md "wikilink")
[A](../Category/美洲原住民.md "wikilink")

1.  [Criminal Minds](../Page/犯罪心理.md "wikilink") S01E16
2.
3.
4.