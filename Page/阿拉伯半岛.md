**阿拉伯半岛**（阿拉伯文：شبه الجزيرة
العربية）位于[亚洲和](../Page/亚洲.md "wikilink")[非洲之间](../Page/非洲.md "wikilink")，它从[中东向东南方伸入](../Page/中东.md "wikilink")[印度洋](../Page/印度洋.md "wikilink")。面积约300万平方公里，是世界上最大的[半岛](../Page/半岛.md "wikilink")。向西它与非洲的边界是[苏伊士运河](../Page/苏伊士运河.md "wikilink")、[红海和](../Page/红海.md "wikilink")[曼德海峡](../Page/曼德海峡.md "wikilink")。向南它伸入[阿拉伯海和](../Page/阿拉伯海.md "wikilink")[印度洋](../Page/印度洋.md "wikilink")。向东它与[伊朗隔](../Page/伊朗.md "wikilink")[波斯湾和](../Page/波斯湾.md "wikilink")[阿曼湾相望](../Page/阿曼湾.md "wikilink")。[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")、[也门](../Page/也门.md "wikilink")、[阿曼](../Page/阿曼.md "wikilink")、[阿拉伯联合酋长国](../Page/阿拉伯联合酋长国.md "wikilink")、[卡塔尔和](../Page/卡塔尔.md "wikilink")[科威特](../Page/科威特.md "wikilink")、[约旦](../Page/约旦.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[以色列位于阿拉伯半岛上](../Page/以色列.md "wikilink")。其中以[沙特阿拉伯为最大](../Page/沙特阿拉伯.md "wikilink")。

阿拉伯半岛常年受[副高壓帶及](../Page/副高壓帶.md "wikilink")[信风带控制](../Page/信风带.md "wikilink")，非常干燥，几乎整个半岛都是[热带沙漠气候区并有面积较大的](../Page/热带沙漠气候.md "wikilink")[无流区](../Page/无流区.md "wikilink")，本区有七个没有河流的国家。半岛沿[波斯湾周围有大量](../Page/波斯湾.md "wikilink")[石油储藏](../Page/石油.md "wikilink")，给阿拉伯半岛上临[波斯湾的国家带来了巨大的财富](../Page/波斯湾.md "wikilink")。

阿拉伯半岛是[伊斯兰教的诞生地](../Page/伊斯兰教.md "wikilink")。伊斯兰教的创教人[穆罕默德在这里出生和生活](../Page/穆罕默德.md "wikilink")。半岛上的[麦加是伊斯兰教的圣地](../Page/麦加.md "wikilink")。以阿拉伯半岛为中心的[阿拉伯帝国曾横跨](../Page/阿拉伯帝国.md "wikilink")[欧亚非大陆](../Page/欧亚非大陆.md "wikilink")。今天半岛上所有国家都以伊斯兰教为[国教](../Page/国教.md "wikilink")。

## 参考文献

## 外部链接

  - ["阿拉伯海/红海沿岸，以及穿越霍尔木兹海峡的巴索拉海的波斯湾，到印度的古吉拉特以及科摩林角"](http://www.wdl.org/zh/item/2914/)
    是阿拉伯半岛的地图从1707世界数字图书馆

## 参见

  - [阿拉伯世界](../Page/阿拉伯世界.md "wikilink")

{{-}}

[A](../Category/亞洲半島.md "wikilink")
[阿拉伯半島](../Category/阿拉伯半島.md "wikilink")
[A](../Category/地理之最.md "wikilink") [A](../Category/分裂地區.md "wikilink")