**阿拉伯文字**是一种用于书写[阿拉伯语](../Page/阿拉伯语.md "wikilink")、[曼丁哥语方言](../Page/曼丁哥语.md "wikilink")、[中库尔德语](../Page/中库尔德语.md "wikilink")、[Luri](../Page/Luri.md "wikilink")、[波斯语](../Page/波斯语.md "wikilink")、[乌尔都语](../Page/乌尔都语.md "wikilink")、[普什图语及其他亚非语言的](../Page/普什图语.md "wikilink")[书写系统](../Page/书写系统.md "wikilink")。\[1\]在16世纪还被用于书写西班牙语。\[2\]阿拉伯文字是世界上第二大广泛使用的书写系统，也是继[拉丁文字和](../Page/拉丁文字.md "wikilink")[汉字之后使用人数第三的文字](../Page/汉字.md "wikilink")。\[3\]

阿拉伯文字以[手写体方式右起横书](../Page/手写体.md "wikilink")。其字母多数情况下表示辅音或者有少数元音的辅音，因此多数阿拉伯文字母表为[辅音音素文字](../Page/辅音音素文字.md "wikilink")。

阿拉伯文字最初用于书写阿拉伯语，尤其是用于[伊斯兰教经典](../Page/伊斯兰教.md "wikilink")《[古兰经](../Page/古兰经.md "wikilink")》。随着伊斯兰教的传播被用于书写多个语族的语言，产生了新的字母和其他符号，在[库尔德语](../Page/库尔德语字母#Sorani_alphabet.md "wikilink")、[维吾尔语及古代](../Page/老维文.md "wikilink")[波斯尼亚文成为](../Page/Arebica.md "wikilink")[元音附标文字或者](../Page/元音附标文字.md "wikilink")[全音素文字](../Page/全音素文字.md "wikilink")。阿拉伯文字也是[阿拉伯书法的基础](../Page/伊斯兰书法.md "wikilink")。

## 参见

  - [阿拉伯文字历史](../Page/阿拉伯文字历史.md "wikilink")
  - [东阿拉伯数字](../Page/东阿拉伯数字.md "wikilink")（阿拉伯文字常用的数字）
  - [阿拉伯文罗马化](../Page/阿拉伯文罗马化.md "wikilink")
  - [小儿经](../Page/小儿经.md "wikilink")

## 参考资料

## 外部链接

  - [Why the right side of your brain doesn't like
    Arabic](http://edition.cnn.com/2010/LIVING/09/06/arabic.difficulty.learning/)
  - [SIL Non-Roman Script
    Initiative的阿拉伯文字体](http://software.sil.org/arabicfonts/)

[阿拉伯字母系统](../Category/阿拉伯字母系统.md "wikilink")

1.  Mahinnaz Mirdehghan. 2010. Persian, Urdu, and Pashto: A comparative
    orthographic analysis. *Writing Systems Research* Vol. 2, No. 1,
    9–23.
2.
3.