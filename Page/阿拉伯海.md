[Kannurfort1.JPG](https://zh.wikipedia.org/wiki/File:Kannurfort1.JPG "fig:Kannurfort1.JPG")
**阿拉伯海**（）為[印度洋的一部分](../Page/印度洋.md "wikilink")。位于[亚洲南部的](../Page/亚洲.md "wikilink")[阿拉伯半岛同](../Page/阿拉伯半岛.md "wikilink")[印度半岛之间](../Page/印度半岛.md "wikilink")。北部为[波斯湾和](../Page/波斯湾.md "wikilink")[阿曼湾](../Page/阿曼湾.md "wikilink")，西部经[亚丁湾通](../Page/亚丁湾.md "wikilink")[红海](../Page/红海.md "wikilink")。面积为386万平方公里。为世界性交通要道。

## 气候

阿拉伯海处热带季风区，气温高。中部海域6月、11月表层水温常在28℃以上；1月、2月温度转低，为24～25℃。临近[阿拉伯半岛海面由于陆地干热气流的](../Page/阿拉伯半岛.md "wikilink")“烘烤”，水温可达30℃以上。

11月～翌年3月吹东北季风，降水稀少，为干季；4～10月吹西南季风，降水丰沛，为雨季，夏秋之交常发生[热带气旋](../Page/热带气旋.md "wikilink")，伴有巨浪、暴雨。海流随季风变化，夏季受西南季风影响呈顺时针方向，冬季受东北季风作用呈反时针方向。海水盐度一般在雨季小于35‰，在旱季大于36‰。

[\*](../Category/阿拉伯海.md "wikilink")
[Category:印度洋陸緣海](../Category/印度洋陸緣海.md "wikilink")
[Category:印度海域](../Category/印度海域.md "wikilink")
[Category:巴基斯坦海域](../Category/巴基斯坦海域.md "wikilink")
[Category:伊朗海域](../Category/伊朗海域.md "wikilink")
[Category:阿曼海域](../Category/阿曼海域.md "wikilink")
[Category:葉門海域](../Category/葉門海域.md "wikilink")
[Category:印度-巴基斯坦邊界](../Category/印度-巴基斯坦邊界.md "wikilink")
[Category:伊朗-巴基斯坦邊界](../Category/伊朗-巴基斯坦邊界.md "wikilink")
[Category:阿曼-葉門邊界](../Category/阿曼-葉門邊界.md "wikilink")