**阿拉佛拉瘰鱗蛇**（[學名](../Page/學名.md "wikilink")：*Acrochordus
arafurae*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[瘰鱗蛇科下的一個](../Page/瘰鱗蛇科.md "wikilink")[水棲蛇種](../Page/水.md "wikilink")，主要分布於[澳洲北部及](../Page/澳洲.md "wikilink")[新畿內亞一帶](../Page/新畿內亞.md "wikilink")，目前未有任何亞種。\[1\]

## 特徵

阿拉佛拉瘰鱗蛇的鱗皮相當鬆弛，是其特色之一。牠們主要進食如[鰻尾鯰一類的大型](../Page/鯰魚.md "wikilink")[魚類](../Page/魚類.md "wikilink")。雌性的阿拉佛拉瘰鱗蛇體型較雄性為大，而且生產時平均能誕下17條幼蛇。澳洲北部的原居民經常捕獵牠們，由於阿拉佛拉瘰鱗蛇一旦脫離水源就幾乎喪失活動能力，所以當地土著都會把捕捉到的瘰鱗蛇隨意放置，再一直出外捕蛇及把獲蛇集中，而這期間牠們都不能自行逃生。在[新畿內亞](../Page/新畿內亞.md "wikilink")，阿拉佛拉瘰鱗蛇的蛇皮會用來製[鼓](../Page/鼓.md "wikilink")。

## 備註

## 外部連結

  - [瘰鱗蛇專門網站](http://www.acrochordus.com/)

  - [TIGR爬蟲類資料庫：阿拉佛拉瘰鱗蛇](http://reptile-database.reptarium.cz/species.php?genus=Acrochordus&species=arafurae)

[ru:Яванская бородавчатая
змея](../Page/ru:Яванская_бородавчатая_змея.md "wikilink")

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:瘰鱗蛇科](../Category/瘰鱗蛇科.md "wikilink")

1.