**阿拉米達**（英文：)是一座位於[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[舊金山灣區東部的城市](../Page/舊金山灣區.md "wikilink")，在同名縣份[阿拉米達縣境內](../Page/阿拉米達縣_\(美國加州\).md "wikilink")，鄰接[奧克蘭市](../Page/奧克蘭_\(加州\).md "wikilink")。阿拉米達市包含了整個同名島嶼[阿拉米達島和部分的](../Page/阿拉米達島.md "wikilink")[灣田島](../Page/灣田島.md "wikilink")（）；[奧克蘭國際機場佔了灣田島的其他部分](../Page/奥克兰国际机场_\(美国\).md "wikilink")。根據2000年的[人口普查](../Page/人口普查.md "wikilink")，阿拉米達總共有人口7萬2259人。

## 歷史

阿拉米達（Alameda）最早的名稱是Encinal。「Encinal」和「Alameda」兩字在[西班牙文裡分別是](../Page/西班牙.md "wikilink")「[橡樹林](../Page/橡樹.md "wikilink")」「[白楊樹林](../Page/白楊.md "wikilink")」的意思，透露著這塊土地在歐洲人到來之前的風貌。根據出土文物判斷，[歐隆尼族](../Page/歐隆尼族.md "wikilink")（Ohlone）[美洲原住民最早在三千多年前已經在舊金山灣東岸的地區聚居生息](../Page/美洲原住民.md "wikilink")。

1772年，[西班牙探險隊首次來到](../Page/西班牙.md "wikilink")[舊金山灣](../Page/舊金山灣.md "wikilink")，西班牙於1779年宣布佔領舊金山灣周圍的地方。1818年，加利福尼亞總督把包括阿拉米達在內的從[柏克萊](../Page/柏克萊.md "wikilink")（Berkeley）到[聖利安卓](../Page/聖利安卓.md "wikilink")（San
Leandro）的一片土地賞賜給西班牙軍官路易．裴若塔（Luis
Peralta）。當時阿拉米達還是與東灣奧克蘭地區相連的一個半島。其後，阿拉米達的所有權先在裴若塔家族內繼承，後於1851年被Chipman與Gideon
Aughinbaugh兩兄弟以一萬四千元的代價購買作為種植桃樹之用，乃為英語系白人經營阿拉米達之始。

[淘金熱使得](../Page/淘金熱.md "wikilink")[舊金山以及週邊人口遽增](../Page/舊金山.md "wikilink")，1853年6月6日阿拉米達的居民通過[公投確定了阿拉米達的名稱](../Page/公投.md "wikilink")，1884年12月27日阿拉米達正式設市，並於隔年1月18日通過採用*Prosperitas
terra mari que*為市標語，意即「從山到海一片繁榮」。

1902年，出於[航運目的](../Page/航運.md "wikilink")，阿拉米達半島連接[奧克蘭的一小段陸地被開鑿成航道](../Page/奥克兰_\(加利福尼亚州\).md "wikilink")，以連通奧克蘭港與聖利安卓灣，負責此項工程的[美國陸軍工兵團](../Page/美國陸軍工兵團.md "wikilink")（Corps
of
Engineers）取得航道的擁有權和管轄權。阿拉米達從此四面環水，成為[舊金山灣內的一個島嶼](../Page/舊金山灣.md "wikilink")。

1917年，一座類似[紐約](../Page/紐約.md "wikilink")[康尼島的](../Page/康尼島.md "wikilink")「海王星沙灘」海水浴場建立起來，成為20年代和30年代的一個著名的度假遊樂中心，從此開啟了阿拉米達的黃金時代。阿拉米達成為[舊金山上層人士的度假別墅區以及熱門旅遊區](../Page/舊金山.md "wikilink")；一棟棟[維多利亞式豪宅被興建](../Page/維多利亞式.md "wikilink")，林立於一條條林蔭大道兩旁，這些優美的建築物至今仍存在，受到市政府建築法規的嚴格保護，不得隨意拆除改建。穿梭其中，彷彿置身[童話世界](../Page/童話.md "wikilink")。

度假遊樂的相關建設如帆船碼頭、海灘、遊樂園也隨之興起。[帆船運動逐漸在阿拉米達紮根](../Page/帆船.md "wikilink")，發展為阿拉米達的一個主要經濟支柱。

[二戰時期阿拉米達被選中建立](../Page/二戰.md "wikilink")[軍港以及](../Page/軍港.md "wikilink")[海岸防衛隊基地](../Page/海岸防衛隊.md "wikilink")，更加深化了阿拉米達的海洋氛圍，也為阿拉米達的社區傳統注入鮮明的軍眷文化。每逢國定假日，阿拉米達總是旗海飄揚，每年七月四日[國慶日](../Page/國慶日.md "wikilink")，阿拉米達都舉行盛大的遊行慶祝。

20世紀後半，阿拉米達逐漸從繁華轉變為舊金山都會區的一個寧靜小鎮，在居民的努力下營造出小城風味的精緻生活環境。阿拉米達著名的低[犯罪率](../Page/犯罪率.md "wikilink")，優秀[學區](../Page/學區.md "wikilink")，平坦地勢，以及悠閒步調吸引了許多有小孩的家庭前來落戶。

[WaterFrontHomesAlameda.jpg](https://zh.wikipedia.org/wiki/File:WaterFrontHomesAlameda.jpg "fig:WaterFrontHomesAlameda.jpg")

阿拉米達是舊金山灣內極少數仍然擁有濱水民居的城市；高街上開閘橋的南北兩排民居家家戶戶的後院都建有私人碼頭，船舶可入舊金山灣和太平洋；有些建築物直接坐落在水面，提供特殊的水上人家生活情調。還有許多水上房屋，住屋本身就是一艘船。

1990年代[美國海軍的阿拉米達](../Page/美國海軍.md "wikilink")[海軍基地關閉](../Page/海軍基地.md "wikilink")，[軍港區劃歸民用](../Page/軍港.md "wikilink")，社區經濟結構再次發生巨大變化；按照新的規劃，阿拉米達朝著成為[高科技重鎮的目標重新出發](../Page/高科技.md "wikilink")。

現在的阿拉米達是全美國第二大的帆船運動都市，擁有十多處船塢碼頭，三千多個船位，僅次於南加州[Marina Del
Ray市](../Page/Marina_Del_Ray.md "wikilink")。帆船運動每年為阿拉米達市政府帶來上千萬元的直接和間接經濟利益。

## 人口組成

根據2000年[人口普查](../Page/人口普查.md "wikilink")，阿拉米達擁有居民72,259口，住宅30,226戶，17,863個家庭，[人口密度每平方英里](../Page/人口密度.md "wikilink")6,693.4人。住宅單位總數31,644，平均每平方英里2,931.2個單位。人口[種族分布為白人](../Page/種族.md "wikilink")56.95%，[非洲裔](../Page/非洲裔.md "wikilink")6.21%，[印地安原住民](../Page/印地安.md "wikilink")0.67%，[亞裔](../Page/亞裔.md "wikilink")26.15%，[太平洋島嶼裔](../Page/太平洋島嶼裔.md "wikilink")0.60%，其他3.29%，兩種以上族裔6.13%。來自各族裔自認是[西班牙語裔](../Page/西班牙語裔.md "wikilink")／[拉丁語裔者](../Page/拉丁語裔.md "wikilink")9.31%。

27.7%的住宅戶有十八歲以下的小孩，43.7%的人已婚，11.4%為女性主持的家庭，32.2%戶只有個人，9.4%的住宅戶有六十五歲以上的老人同住。每住宅戶平均有2.35人，每家庭平均有3.04人。

本市人口按照年齡層分類為：21.5%的人18以下，7.0%的人從18歲到24歲，33.6%的人從25歲到44歲，24.6%的人從45歲到64歲，13.3%的人65以上。平均年齡為38歲，男女比例為92.3比100，十八歲以上的男女比例為89.5比100。

住宅戶[平均收入為](../Page/平均收入.md "wikilink")$56,285，家庭[中間收入為](../Page/中間收入.md "wikilink")$68,625，男性中間收入為$49,174，女性中間收入為$40,165。

## 交通運輸

[High_Street_Bridge_over_Oakland_Alameda_Estuary_2.jpg](https://zh.wikipedia.org/wiki/File:High_Street_Bridge_over_Oakland_Alameda_Estuary_2.jpg "fig:High_Street_Bridge_over_Oakland_Alameda_Estuary_2.jpg")

阿拉米達透過四條橋和一條隧道對外相連。連接奧克蘭的三條橋分別位於公園街，福德維爾街，以及高街。灣田橋連通灣田島（Bay Farm
Island）。四條橋皆為開閘橋，橋塔全天候都有職員值班，可開橋供船舶通行。上午和下午通勤尖峰時段不開橋。

位於委士打街和哈利臣街的單向[水底隧道連接奧克蘭市中心](../Page/水底隧道.md "wikilink")，奧克蘭端出口即為[華埠](../Page/華埠.md "wikilink")。此通道最早亦為開閘橋，在一宗船撞橋事件之後，鑒於此段水路交通繁忙，決定捨橋改建[隧道](../Page/隧道.md "wikilink")，一勞永逸的排除船隻撞擊意外以及活橋開閉的麻煩。

阿拉米達還有通勤[渡輪連接奧克蘭和舊金山](../Page/渡輪.md "wikilink")，以及[通勤](../Page/通勤.md "wikilink")[巴士直達](../Page/巴士.md "wikilink")[舊金山客運終點站](../Page/舊金山.md "wikilink")。

離阿拉米達最近的[灣區捷運有](../Page/灣區捷運.md "wikilink")和。

## 旅遊景點、活動、與節慶

[Architecture_11.JPG](https://zh.wikipedia.org/wiki/File:Architecture_11.JPG "fig:Architecture_11.JPG")

阿拉米達島上保留了一千五百多棟優美的[維多利亞式建築](../Page/維多利亞式.md "wikilink")，在[舊金山灣區無出其右](../Page/舊金山灣區.md "wikilink")；尤以Gold
Coast地段最為集中，建築規模最大，建造也最精美。

島的西南邊是Crown海灘，每年夏天吸引大量的弄潮遊客，沙灘外的水面上經常可以看到運動愛好者享受[風帆運動](../Page/風帆.md "wikilink")。從該處海灘也可以眺望[舊金山和](../Page/舊金山.md "wikilink")[奧克蘭市中心的天際線](../Page/奧克蘭.md "wikilink")。

海灘南邊是一個野鳥棲息[保護區](../Page/保護區.md "wikilink")，有眾多種類的水鳥棲息，建有眺望台。在此棲息的包括翼展超過七呎的[鹈鶘鳥](../Page/鹈鶘.md "wikilink")，牠們成群結隊，數以百計，蔚為壯觀。

[阿拉米達歷史博物館](../Page/阿拉米達歷史博物館.md "wikilink")(Alameda Historical
Museum)收藏本地生活的文物，可以讓訪問者了解阿拉米達的轉變。

已退役的[美國海軍](../Page/美國海軍.md "wikilink")[黃蜂號](../Page/黃蜂號.md "wikilink")[航空母艦停泊在舊軍港](../Page/航空母艦.md "wikilink")，今日已經成為一座博物館。

[3_Foot_California_halibut.jpg](https://zh.wikipedia.org/wiki/File:3_Foot_California_halibut.jpg "fig:3_Foot_California_halibut.jpg")

沙灘北邊舊軍港的[防波堤就是在金山灣區釣客之間享有盛名的](../Page/防波堤.md "wikilink")「阿拉米達石牆」（Alameda
Rockwall），乃是舊金山灣的[釣魚聖地之一](../Page/釣魚.md "wikilink")。每年五月份開始，加州[比目魚](../Page/比目魚.md "wikilink")（[California
Halibut](../Page/California_Halibut.md "wikilink")）大批進入海灣產卵，阿拉米達石牆外的水域就是牠們聚集的地點之一。加州比目魚體型碩大，常見者約三英呎，最大可達五英呎。阿拉米達水域內常見的魚類還包括[銀花鱸魚](../Page/銀花鱸魚.md "wikilink")（[Striped
Bass](../Page/Striped_Bass.md "wikilink")），[豹紋鯊](../Page/豹紋鯊.md "wikilink")（[Leopard
Shark](../Page/Leopard_Shark.md "wikilink")），蝙蝠[魟魚](../Page/魟魚.md "wikilink")（[Bat
Ray](../Page/Bat_Ray.md "wikilink")）。阿拉米達有兩處公共碼頭，免費供私人船舶下水出水。

阿拉米達的活動與節慶包括:

農夫市場：在這裡可以購買到最新鮮的蔬果、花卉、糕點、以及海產。參與者多是本地的[有機栽種者](../Page/有機.md "wikilink")。每周二和每周六上午在委士打街（Webster）和泰勒街（Taylor）交叉口舉行，全年風雨無阻。

海灣[音樂會](../Page/音樂會.md "wikilink")（Concerts at the
Cove）：民眾可攜帶自己喜歡的食物，在草坪上席地而坐，一邊欣賞優美的音樂，一邊飽覽美麗的海灣風光。每年六、七、八月舉行。

[美國國慶日遊行](../Page/美國國慶日.md "wikilink")（Forth of July
Parade）：盛大的國慶遊行是阿拉米達的一項悠久傳統。整個社區一片旗海。

委士打街街會（Webster Street
Jam）：這是一整個週末的美食、音樂、藝術、以及歡樂，特別適合兒童；委士打街將暫時化為一個巨大派對現場。每年九月舉行。

委士打街[萬聖節](../Page/萬聖節.md "wikilink")[嘉年華會](../Page/嘉年華.md "wikilink")（Halloween
on Webster
Street）：把穿著萬聖節裝扮的小朋友們帶來委士打街，商家都會提供糖果、著色紙、[南瓜](../Page/南瓜.md "wikilink")，並且舉辦萬聖節裝扮競賽。

[聖誕老人降臨委士打街](../Page/聖誕老人.md "wikilink")（Santa Visits Webster
Street）：聖誕老人將會駕著他的[雪橇來到委士打街和泰勒街交叉口](../Page/雪橇.md "wikilink")，與遊人合照並贈送小禮物。

新年船隊遊行（New Year's Day Yacht
Parade）：每年一月一日阿拉米達的十多個船塢聯合舉辦船隊遊行，數百艘帆船和遊艇集結列隊環繞阿拉米達島一周，極為壯觀。

## 阿拉米達的各個社區

[Alameda_Neighborhoods.jpg](https://zh.wikipedia.org/wiki/File:Alameda_Neighborhoods.jpg "fig:Alameda_Neighborhoods.jpg")

阿拉米達市分為下列幾個社區:

1\.
瑪林納區（Marina）——本區因區內的Marina大街而得名，大街東面的房屋都坐落於河濱，背後都有[深水碼頭](../Page/深水碼頭.md "wikilink")，可停泊[帆船和](../Page/帆船.md "wikilink")[遊艇](../Page/遊艇.md "wikilink")。阿拉米達市只有三區擁有濱水住宅，此其一。瑪林納區非常僻靜，房屋多半是在[六零年代建成而且保養良好](../Page/六零年代.md "wikilink")，居民都注重維護。

2\.
芬賽區（Fernside）——本區因區內的芬賽大街而得名，房屋多建於1920年代、1930年代、以及1940年代，格局較大、做工考究且保養良好。芬賽大街東面的住宅也濱臨河道並附帶[深水碼頭](../Page/深水碼頭.md "wikilink")，為本市第二群[濱水住宅](../Page/濱水住宅.md "wikilink")。本區擁有非常獨特的社區風格，每棟住宅的設計各自不同，但是整體而言卻能夠產生一種[協調感](../Page/協調感.md "wikilink")。Gibbons大街斜貫其中，兩旁種滿巨大[楓樹](../Page/楓樹.md "wikilink")，居民顯然都以作為社區的一份子為榮。Thompson街的[聖誕節燈飾遠近馳名](../Page/聖誕節燈飾.md "wikilink")，居民相約共襄盛舉，各用巧思，還邀請[聖誕老公公與兒童同樂](../Page/聖誕老公公.md "wikilink")，年年吸引大量賞燈人潮，每晚遊人如織，車水馬龍。

3\. 東端區（East
End）——本區位於阿拉米達島東邊，區內建築物主要為[維多利亞式](../Page/維多利亞式.md "wikilink")（Victorian）、[現代式](../Page/現代式.md "wikilink")（Contemporary）、以及[加州式平房](../Page/加州式平房.md "wikilink")（California
Bungalow）。

4\. 東岸區（East
Shore）——本區面對[聖利安卓灣](../Page/聖利安卓灣.md "wikilink")，街道寬廣而人車稀少，是一個非常寧靜的純住宅社區。

5\.
中央區（Central）——阿拉米達最繁榮的公園街商業區以及市政府都位於本區內，因而得名。住宅最多為[安妮皇后式建築](../Page/安妮皇后式.md "wikilink")。中央區的沿河地段多作為商業用途。

6\. 青銅海岸區（Bronze
Coast）——本區的維多利亞式建築在規模上稍次於黃金海岸區，但數量則最多。大學[建築系的師生經常結隊在社區穿梭](../Page/建築系.md "wikilink")，研究本區的珍貴建築物。

7\. 黃金海岸區（Gold
Coast）——黃金海岸區的優美豪宅一度面對[舊金山灣](../Page/舊金山灣.md "wikilink")，雖然今天海岸線已經因填土造陸而往外推移，建築物壯觀依舊。林蔭深處的巍峨[維多利亞豪宅令本區成為阿拉米達的最高級住宅區](../Page/維多利亞豪宅.md "wikilink")。

8\. 南岸區（South
Shore）——南岸區因位於阿拉米達島南面而且濱臨金山灣而得名，大體上是在[六零年代開發](../Page/六零年代.md "wikilink")，多數住宅離海灘只有幾步之遙，部分直接面對[舊金山灣](../Page/舊金山灣.md "wikilink")，擁有一流的景觀。

9\. 碼頭村（Marina
Village）——本區是圍繞碼頭生活而精心規劃興建的，與奧克蘭的[傑克．倫敦廣場](../Page/傑克．倫敦廣場.md "wikilink")（[Jack
London
Square](../Page/Jack_London_Square.md "wikilink")）隔江相望，區內建築物主要為[共度公寓](../Page/共度公寓.md "wikilink")。

10\. 西端區（West
End）——西端區一直以來都是阿拉米達島上最西邊的居民社區，稱為西端可謂貨真價實，但是自從海軍軍港關閉、港區土地劃歸民用之後，西端區或許需要一個新的名稱。阿拉米達的第二大商業區位於本區的Webster大街上。西端區也是[阿拉米達學院](../Page/阿拉米達學院.md "wikilink")（[College
of Alameda](../Page/College_of_Alameda.md "wikilink")）的所在地。

11\. 寶林納灣（Ballena
Bay）——本區因區內的小海灣而得名，為上世紀七零年代開發，多數為[鎮屋](../Page/鎮屋.md "wikilink")（Town
House）。每單位都附帶[深水碼頭船位可停泊](../Page/深水碼頭.md "wikilink")[私人遊艇或](../Page/私人遊艇.md "wikilink")[帆船](../Page/帆船.md "wikilink")，為本市第三群濱水住宅，一出港口便入[舊金山灣](../Page/舊金山灣.md "wikilink")。

12\. 阿拉米達岬（Alameda
Point）——本區是原海軍基地，如今已經交還給阿拉米達市政府使用。舊的軍用設施如今都改為商業和工業用途。規劃中的建設包括住宅、野生動物保護區、私人小飛機機場、[高爾夫球場](../Page/高爾夫.md "wikilink")。

13\. 港灣島（Harbour Bay
Isle）——本區是高級住宅區，[平均家庭收入超過十萬美元](../Page/平均家庭收入.md "wikilink")。許多住宅建造在人造[礁湖周圍](../Page/礁湖.md "wikilink")，許多面向[舊金山灣](../Page/舊金山灣.md "wikilink")。整體予人一種處於公園內的情調和寧靜。

14\. 灣田島（Bay Farm Island）——本區因過去都是農田而得名，如今已經開發為住宅區。區內有一個渡輪碼頭，連接舊金山和奧克蘭。

15\. 高爾夫球場（Golf Course）

## 阿拉米達名人

[贾森·基德](../Page/贾森·基德.md "wikilink") －
[美國國家籃球協會](../Page/美國國家籃球協會.md "wikilink")（NBA）球星，曾效力[達拉斯小牛隊](../Page/達拉斯小牛隊.md "wikilink")，[鳳凰城太陽隊](../Page/鳳凰城太陽隊.md "wikilink")，以及[新澤西網隊](../Page/新澤西網隊.md "wikilink")，是NBA史上最佳球員之一。

[詹姆斯·哈羅德·杜立德](../Page/詹姆斯·哈羅德·杜立德.md "wikilink") －
[美國空軍中將](../Page/美國空軍.md "wikilink")，1942年率領一隊B-25轟炸機從[航空母艦起飛](../Page/航空母艦.md "wikilink")，成功轟炸東京，是為歷史著名的[空襲東京](../Page/空襲東京.md "wikilink")。這是美國首次攻擊[日本本土](../Page/日本.md "wikilink")，鼓舞了當時節節敗退的美軍的士氣。阿拉米達灣田島上的杜立德路就是紀念此人。

## 參考文獻

## 外部链接

  - <http://www.alamedainfo.com/> (英)
  - [Alameda Community Profile
    (英)](http://www.epodunk.com/cgi-bin/genInfo.php?locIndex=9767)

[Alameda](../Category/舊金山灣區城市.md "wikilink")
[Category:阿拉梅达县城市](../Category/阿拉梅达县城市.md "wikilink")