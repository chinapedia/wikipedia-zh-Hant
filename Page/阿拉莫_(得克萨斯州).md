[Sanantonio_alamo.jpg](https://zh.wikipedia.org/wiki/File:Sanantonio_alamo.jpg "fig:Sanantonio_alamo.jpg")

**阿拉莫**（，）是位于[美国](../Page/美国.md "wikilink")[德克萨斯州](../Page/德克萨斯州.md "wikilink")[圣安东尼奥市区的一座由传教站扩建成的](../Page/圣安东尼奥_\(得克萨斯州\).md "wikilink")[要塞](../Page/要塞.md "wikilink")，在[德克萨斯革命中曾有重要地位](../Page/德克萨斯革命.md "wikilink")。

## 历史

1724年，传教士在今日阿拉莫博物馆所在地建立了圣安东尼奥·德·瓦莱罗传教站，但[西班牙的世俗化运动使得传教站在](../Page/西班牙.md "wikilink")1793年被废弃。19世纪初，一支西班牙骑兵部队进驻此地，将此作为营地，因為这支部队的士兵都来自於[墨西哥城市阿拉莫](../Page/墨西哥.md "wikilink")·德·帕拉斯（），故阿拉莫之名便来源于此。

在[墨西哥独立战争](../Page/墨西哥独立战争.md "wikilink")（1815年－1821年）中，起义的[墨西哥人占领了这座要塞](../Page/墨西哥人.md "wikilink")。1835年10月2日的德克萨斯动乱之后，一群美国拓荒者在12月占领了阿拉莫并开始加强防御。

1836年3月2日，德克萨斯由于蓄奴问题而宣布从墨西哥独立，成立[德克萨斯共和国](../Page/德克萨斯共和国.md "wikilink")。由前[田纳西州](../Page/田纳西州.md "wikilink")[州长](../Page/州长.md "wikilink")[山姆·休斯敦担任](../Page/山姆·休斯敦.md "wikilink")[总统并亲自担任军事总指挥](../Page/总统.md "wikilink")。墨西哥将军暨[独裁者](../Page/独裁者.md "wikilink")[安東尼奧·洛佩斯·德·桑塔·安納率军](../Page/安東尼奧·洛佩斯·德·桑塔·安納.md "wikilink")7000人前来镇压独立运动，休斯敦则命令他手下200多名組成复杂的部隊暂时先撤退到阿拉莫固守，不久後被人数大占优的墨西哥军队包围。

阿拉莫城中当时的指挥是27岁的[威廉·塔拉维斯](../Page/威廉·塔拉维斯.md "wikilink")[中校](../Page/中校.md "wikilink")、40岁的冒险家[詹姆斯·鲍文和](../Page/詹姆斯·鲍文.md "wikilink")50岁的战争英雄及政治家[大衛·克拉克](../Page/大卫·克洛科特.md "wikilink")，经过13天伤亡惨重的[阿拉莫戰役](../Page/阿拉莫戰役.md "wikilink")，墨西哥军队终于占领了阿拉莫，所有男性抵抗者均被处死，妇女和儿童得到赦免。

三週后，以「记住阿拉莫」（Remember the
Alamo\!）为战斗口号的德克萨斯军队在山姆·休斯敦的指挥下在[圣哈辛托战役取得了决定性的胜利](../Page/圣哈辛托战役.md "wikilink")，桑塔·安那也被俘。德克萨斯也因此在之后的几年中保持了独立并在1845年加入[美利坚合众国](../Page/美利坚合众国.md "wikilink")。直到今天，阿拉莫之战依然被视作[美国陆军历史上的神话](../Page/美国陆军.md "wikilink")，被美国人认为是自由意志下勇气和牺牲精神的象征，而阿拉莫博物馆每年有多达250万名的参观者。

关于这场战争的故事曾多次被拍成[电影](../Page/美國電影.md "wikilink")，其中最著名的是1960年[約翰·韋恩自導自演的](../Page/約翰·韋恩_\(美國電影演員\).md "wikilink")《[邊城英烈傳](../Page/邊城英烈傳.md "wikilink")》，以及2004年[丹尼斯·奎德主演的](../Page/丹尼斯·奎德.md "wikilink")《[圍城13天：阿拉莫戰役](../Page/圍城13天：阿拉莫戰役.md "wikilink")》。

## 参考文献

  - Robert Wlodarski, Anne Powell Wlodarski: *Spirits of the Alamo. A
    History of the Mission and its Hauntings*, Plano/Texas: Republic of
    Texas Press 1999 ISBN 1-556-22681-0

## 外部链接

  - [阿拉莫博物馆](http://www.thealamo.org/)

[nl:Alamo (gebouw)](../Page/nl:Alamo_\(gebouw\).md "wikilink")

[Category:德克薩斯州建築物](../Category/德克薩斯州建築物.md "wikilink")
[Category:德克萨斯州军事](../Category/德克萨斯州军事.md "wikilink")
[Category:美国军事设施](../Category/美国军事设施.md "wikilink")
[Category:美国军事史](../Category/美国军事史.md "wikilink")