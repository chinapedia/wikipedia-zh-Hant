**阿拉贡王国**（）是1035年－1707年时[伊比利半岛东北部](../Page/伊比利半岛.md "wikilink")[阿拉贡地区的封建](../Page/阿拉贡自治区.md "wikilink")[王国](../Page/王国.md "wikilink")。阿拉贡位於西班牙东北部，因[阿拉贡河](../Page/阿拉贡河.md "wikilink")(Río
Aragón)而得名。9世纪，[法蘭克人驱逐此地的阿拉伯勢力](../Page/法蘭克人.md "wikilink")，建立阿拉贡伯爵領。925年并入[纳瓦拉王国](../Page/纳瓦拉王国.md "wikilink")。[中世纪时是](../Page/中世纪.md "wikilink")[伊比利亚半岛上的主要](../Page/伊比利亚半岛.md "wikilink")[基督教国家之一](../Page/基督教.md "wikilink")。1035年纳瓦拉王国国王[桑乔三世死后](../Page/桑乔三世_\(纳瓦拉\).md "wikilink")，拉米罗获得阿拉贡，成为独立王国，称[拉米罗一世](../Page/拉米罗一世_\(阿拉贡\).md "wikilink")。1104年[阿方索一世当政](../Page/阿方索一世_\(阿拉贡\).md "wikilink")，王国疆界向南扩展，直达[西班牙南部的](../Page/西班牙.md "wikilink")[格拉纳达](../Page/格拉纳达.md "wikilink")。1137年[拉米罗二世将其女儿与](../Page/拉米罗二世_\(阿拉贡\).md "wikilink")[巴塞罗那伯爵拉蒙](../Page/巴塞罗那伯爵.md "wikilink")·贝伦格尔四世联姻，导致阿拉贡同[加泰罗尼亚合并](../Page/加泰罗尼亚.md "wikilink")。1229年[海梅一世进占](../Page/海梅一世.md "wikilink")[马略卡岛](../Page/马略卡岛.md "wikilink")。不久又征服[巴伦西亚](../Page/巴伦西亚.md "wikilink")。1276年[佩德罗三世继位后征服了](../Page/佩德罗三世_\(阿拉贡\).md "wikilink")[西西里与](../Page/西西里.md "wikilink")[撒丁岛](../Page/撒丁岛.md "wikilink")，使阿拉贡王国成为[地中海的强国](../Page/地中海.md "wikilink")。1416—1458年[阿方索五世在位时](../Page/阿方索五世_\(阿拉贡\).md "wikilink")，阿拉贡王国征服了[那不勒斯](../Page/那不勒斯.md "wikilink")，王国的版图扩大到[意大利半岛](../Page/意大利.md "wikilink")。后来，阿拉贡的[斐迪南二世与](../Page/斐迪南二世_\(阿拉贡\).md "wikilink")[卡斯蒂利亚的继承人](../Page/卡斯蒂利亚.md "wikilink")[伊莎贝拉结婚](../Page/伊莎贝拉一世.md "wikilink")，使两国合并，从而形成了今日西班牙的主体部分，但仍然保有部分政治權力。伊莎贝拉去世后，斐典南成为[西班牙帝国的唯一统治者](../Page/西班牙帝国.md "wikilink")。1707年《[新基本法令](../Page/新基本法令.md "wikilink")》實施後，阿拉貢王國的殘餘機構解散，阿拉貢王國正式走入歷史。

## 相关条目

  - [阿拉贡君主列表](../Page/阿拉贡君主列表.md "wikilink")
  - [亞拉岡聯合王國](../Page/亞拉岡聯合王國.md "wikilink")（）

[AK](../Category/西班牙历史.md "wikilink")
[AK](../Category/阿拉贡联合王国.md "wikilink")
[AK](../Category/中世紀各國.md "wikilink")