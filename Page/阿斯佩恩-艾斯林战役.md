**阿斯佩恩-艾斯林战役**中，[拿破仑·波拿巴试图凭借武力在](../Page/拿破仑·波拿巴.md "wikilink")[维也纳附近横渡](../Page/维也纳.md "wikilink")[多瑙河](../Page/多瑙河.md "wikilink")。但[卡爾大公率领的奥地利军队在前者渡河時](../Page/卡爾大公.md "wikilink")，發動圍剿進攻，發生激战，拿破崙只好被迫撤退。此战役是在数十年中拿破仑亲自率领的军队第一次遭到失败。
戰役結果，法军损失大约23,000名士兵，包括7,000名战死，16,000名重伤，其中包含[法蘭西第一帝國第一位陣亡的元帥](../Page/法蘭西第一帝國.md "wikilink")[讓·拉納](../Page/讓·拉納.md "wikilink")。

## 註解

<references/>

## 參見

  - [拿破崙戰爭](../Page/拿破崙戰爭.md "wikilink")
  - [華格姆戰役](../Page/華格姆戰役.md "wikilink")

[Category:拿破仑战争战役](../Category/拿破仑战争战役.md "wikilink")
[Category:奥地利帝国战役](../Category/奥地利帝国战役.md "wikilink")
[Category:1809年](../Category/1809年.md "wikilink")