**阿斯图里亚斯语**（**asturianu**）又称作**llïonés**、**mirandés**或**Bable**，是一种[罗曼语族语言](../Page/罗曼语族.md "wikilink")，在[西班牙的](../Page/西班牙.md "wikilink")[阿斯图里亚斯](../Page/阿斯图里亚斯.md "wikilink")、[莱昂](../Page/莱昂.md "wikilink")、[萨莫拉和](../Page/萨莫拉.md "wikilink")[萨拉曼卡](../Page/萨拉曼卡.md "wikilink")，和[葡萄牙的](../Page/葡萄牙.md "wikilink")[Miranda
de
Douro使用](../Page/Miranda_de_Douro.md "wikilink")（在葡萄牙，正式名称是[Mirandese](../Page/Mirandese.md "wikilink")）。在阿斯图里亚斯，这语言受到自治地位立法保护，并是学校内的可选用语言。有些学者曾把阿斯图里亚斯语看成是[西班牙语的一种](../Page/西班牙语.md "wikilink")[方言](../Page/方言.md "wikilink")，但现在，阿斯图里亚斯语被看成为一种独自的语言。

## 字母

<table>
<thead>
<tr class="header">
<th><p>字母</p></th>
<th><p>名称</p></th>
<th><p>音素</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>A, a</p></td>
<td><p><strong>a</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>B, b</p></td>
<td><p><strong>be</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>C, c</p></td>
<td><p><strong>ce</strong></p></td>
<td><p>, </p></td>
</tr>
<tr class="even">
<td><p>D, d</p></td>
<td><p><strong>de</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>E, e</p></td>
<td><p><strong>e</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>F, f</p></td>
<td><p><strong>efe</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>G, g</p></td>
<td><p><strong>gue</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>H, h</p></td>
<td><p><strong>hache</strong></p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p>I, i</p></td>
<td><p><strong>i</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>L, l</p></td>
<td><p><strong>ele</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>M, m</p></td>
<td><p><strong>eme</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>N, n</p></td>
<td><p><strong>ene</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ñ, ñ</p></td>
<td><p><strong>eñe</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>O, o</p></td>
<td><p><strong>o</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>P, p</p></td>
<td><p><strong>pe</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>R, r</p></td>
<td><p><strong>erre</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>S, s</p></td>
<td><p><strong>ese</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>T, t</p></td>
<td><p><strong>te</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>U, u</p></td>
<td><p><strong>u</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>V</p></td>
<td><p><strong>uve</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>X</p></td>
<td><p><strong>xe</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Y</p></td>
<td><p><strong>ye</strong>, <strong>y griega</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Z</p></td>
<td><p><strong>zeta</strong>, <strong>zeda</strong>, <strong>ceda</strong></p></td>
<td></td>
</tr>
</tbody>
</table>

阿斯图里亚斯语也有几个合体字母，其中一些有各自的名称：

<table>
<thead>
<tr class="header">
<th><p>變音字母</p></th>
<th><p>名称</p></th>
<th><p>音素</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ch</p></td>
<td><p><strong>che</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>gu + e, i</p></td>
<td><p>--</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ll</p></td>
<td><p><strong>elle</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>qu + e, i</p></td>
<td><p><strong>cu</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>rr</p></td>
<td><p><strong>erre doble</strong></p></td>
<td></td>
</tr>
</tbody>
</table>

此外，字母“h”与并合字母“ll”可以通过加下标点而改变它们的发音，用来代表方言发音：

<table>
<thead>
<tr class="header">
<th><p>正常</p></th>
<th><p>发音</p></th>
<th><p>虚线</p></th>
<th><p>发音</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ll</p></td>
<td></td>
<td><p>ḷḷ</p></td>
<td><p>、，还有其他发音</p></td>
</tr>
<tr class="even">
<td><p>h</p></td>
<td><p>--</p></td>
<td><p>ḥ</p></td>
<td><p>、</p></td>
</tr>
</tbody>
</table>

## 參考文獻

  - Llera Ramo, F. (1994) *Los Asturianos y la Lengua Asturiana: Estudio
    Sociolingüístico para Asturias-1991*. Oviedo: Consejería de
    Educación y Cultura del Principado de Asturias ISBN 84-7847-297-5.

  - Wurm, Stephen A. (ed) (2001) *Atlas of the World's Languages in
    Danger of Disappearing*. Unesco ISBN 92-3-103798-6.

## 外部链接

  - [Ethnologue report for
    Asturian](http://www.ethnologue.com/show_language.asp?code=ast)
  - [L'Academia de la Llingua
    Asturiana](http://www.academiadelallingua.com/) — the official
    Asturian language committee
  - [Asturian–English
    dictionary](https://web.archive.org/web/20040404083624/http://www.websters-online-dictionary.org/definition/Asturian-english/)

[Category:罗曼语族](../Category/罗曼语族.md "wikilink")
[Category:西班牙語言](../Category/西班牙語言.md "wikilink")