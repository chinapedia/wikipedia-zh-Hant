**阿斯科利足球會**（**Ascoli Calcio
1898**）是一間位於[意大利中部](../Page/意大利.md "wikilink")[馬爾凱區](../Page/馬爾凱.md "wikilink")[阿斯科利皮切诺市的足球會](../Page/阿斯科利皮切诺.md "wikilink")，於1898年成立，現時在[意大利乙組聯賽作賽](../Page/意乙.md "wikilink")。

阿斯科利創會以來一直處於低組別聯賽，2001/2002年球季奪得[丙組聯賽冠軍](../Page/意丙.md "wikilink")，3年後他們以聯賽第五名完成球季，但其後得知聯賽亞軍及季軍-[熱那亞及](../Page/熱那亞足球俱樂部.md "wikilink")[-{zh-hans:都灵;zh-hant:拖連奴}-](../Page/拖連奴足球俱樂部.md "wikilink")，前者因為被裁定製造賽果而被降到丙組作賽，後者因為財政問題無法升班，結果阿斯科利連同第四名[泰列維素創會後首次晉身頂級聯賽之列](../Page/泰列維素足球俱樂部.md "wikilink")。

## 球會榮譽

  - [米杜柏盃](../Page/米杜柏盃.md "wikilink")：1987年

## 著名球員

  - [-{zh-hans:奥利弗·比尔霍夫;zh-hant:比亞荷夫}-](../Page/奥利弗·比尔霍夫.md "wikilink")（Oliver
    Bierhoff）

  - [林·柏拉迪](../Page/林·柏拉迪.md "wikilink")（Liam Brady）

  - [-{zh-hans:安德雷·巴尔扎利;
    zh-hk:巴沙利;}-](../Page/安德雷·巴尔扎利.md "wikilink")（Andrea
    Barzagli）

  - [-{zh-hans:法比奥·夸利亚雷拉;
    zh-hk:哥利亞列拿;}-](../Page/法比奧·哥利亞列拿.md "wikilink")（Fabio
    Quagliarella）

## 参考文献

## 外部連結

  - [官方網站](http://www.ascolicalcio.it)

[A](../Page/分類:1898年建立的足球俱樂部.md "wikilink")

[A](../Category/意大利足球俱樂部.md "wikilink")
[Category:馬爾凱大區](../Category/馬爾凱大區.md "wikilink")