**阿斯顿·马丁V12 Vanquish**（Aston Martin V12
Vanquish）是伊恩・卡勒姆所設計的當家豪華旗艦GT高性能[跑車](../Page/跑車.md "wikilink")，2001年發售，2007年停產。停產後由[阿斯顿·马丁DBS
V12暫時取代它的地位](../Page/阿斯顿·马丁DBS_V12.md "wikilink")，正式的版本將在2010年登場。

2012年再度登場,取代Aston Martin DBS旗艦車.並且用上新一代V12引擎,代號AM11.馬力有565匹

## 歷史

1998年一月，搭載V12引擎的概念車在北美洲國際車展發表。2000年正式的量產車亮相。2001年投入量產。

<File:Aston> Martin Vanquish S - Flickr - Alexandre Prévot (1)
(cropped).jpg|Aston Martin Vanquish S <File:Aston> Martin Vanquish
(8186456260).jpg|2012年Aston Martin Vanquish

新一代Vanquish配置6公升V12引擎,馬力有565匹,0-100公里加速只需4.1秒,極速330公里.新Vanquish係繼ONE-77之後,第二款用碳纖維車架的Aston
Martin

[Category:雅士頓馬田車輛](../Category/雅士頓馬田車輛.md "wikilink")
[Category:前置引擎](../Category/前置引擎.md "wikilink")
[Category:後輪驅動](../Category/後輪驅動.md "wikilink")