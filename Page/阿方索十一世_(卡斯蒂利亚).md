[Alfonsoxialgeciras.jpg](https://zh.wikipedia.org/wiki/File:Alfonsoxialgeciras.jpg "fig:Alfonsoxialgeciras.jpg")
**（正义的）阿方索十一世 Alfonso XI el
Justiciero**（约1311年8月13日－1350年3月26日）[莱昂和](../Page/莱昂.md "wikilink")[卡斯蒂利亚国王](../Page/卡斯蒂利亚.md "wikilink")（1312年－1350年在位）。

阿方索十一世是卡斯蒂利亚国王[斐迪南四世之子](../Page/斐迪南四世_\(卡斯蒂利亚\).md "wikilink")，母为[葡萄牙的康斯坦丝](../Page/葡萄牙的康斯坦丝.md "wikilink")，冲龄即位。在阿方索十一世成年前，由其祖母[玛丽亚·德·莫里纳和众公主摄政](../Page/玛丽亚·德·莫里纳.md "wikilink")。1325年开始亲政。他依靠城市居民的支持实行一些限制贵族权力的措施。阿方索十一世推行某些经济政策来获取城市工商业者的好感（如提高货币价值等）。

阿方索十一世与[阿拉贡王国和](../Page/阿拉贡王国.md "wikilink")[葡萄牙联合](../Page/葡萄牙.md "wikilink")，共同反对[摩尔人](../Page/摩尔人.md "wikilink")。1340年10月30日，他与葡萄牙国王[阿方索四世在](../Page/阿方索四世_\(葡萄牙\).md "wikilink")[萨拉多河战役中取得一次重要的胜利](../Page/萨拉多河战役.md "wikilink")，打败了来自[北非的](../Page/北非.md "wikilink")[穆斯林军队](../Page/穆斯林.md "wikilink")。此役之后，摩尔人再未能恢复他们的军事力量。

1350年，阿方索十一世在围攻[直布罗陀时死于](../Page/直布罗陀.md "wikilink")[鼠疫](../Page/鼠疫.md "wikilink")。

阿方索十一世对他的情妇[莱昂诺拉·德·古斯曼特别钟情](../Page/莱昂诺拉·德·古斯曼.md "wikilink")。在阿方索十一世去世后，莱昂诺拉·德·古斯曼的儿子[恩里克二世与阿方索十一世的合法继承人](../Page/恩里克二世_\(卡斯蒂利亚\).md "wikilink")[佩德罗一世之间爆发了一场内战](../Page/佩德罗一世_\(卡斯蒂利亚\).md "wikilink")。

[Category:卡斯蒂利亚君主](../Category/卡斯蒂利亚君主.md "wikilink")
[Category:莱昂君主](../Category/莱昂君主.md "wikilink")
[A](../Category/1350年逝世.md "wikilink")
[Category:1311年出生](../Category/1311年出生.md "wikilink")