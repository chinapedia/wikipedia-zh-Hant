**阿方索十三世**（，），全名**阿方索·萊昂·費爾南多·马里亞·海梅·伊西德羅·帕斯夸爾·安東尼奧·德·波旁—哈布斯堡-洛林**（Alfonso
León Fernando Maria Jaime Isidro Pascual Antonio de Borbón y
Habsburgo-Lorena），[波旁王朝的](../Page/波旁王朝.md "wikilink")[西班牙国王](../Page/西班牙国王.md "wikilink")（1886年-1931年），[阿方索十二世的遗腹子](../Page/阿方索十二世.md "wikilink")。1902年成年前，由他的母亲[玛丽亚·克里斯蒂娜摄政](../Page/玛丽亚·克里斯蒂娜.md "wikilink")。

阿方索十三世统治时期发生的最重要事件是[美西战争](../Page/美西战争.md "wikilink")。在这场灾难性战争中，西班牙被新兴强权国家[美国彻底击溃](../Page/美国.md "wikilink")，丧失[菲律宾和所有](../Page/菲律宾.md "wikilink")[美洲领地](../Page/西班牙的美洲殖民地.md "wikilink")。

1909至1911年，阿方索十三世的统治为一系列革命运动所困扰，其中尤以在[马德里和](../Page/马德里.md "wikilink")[巴塞罗那发生的革命活动最为剧烈](../Page/巴塞罗那.md "wikilink")。民众打出推翻[君主制的旗号](../Page/君主制.md "wikilink")。阿方索十三世在舆论压力下，在立法、教育和宗教等多方面进行了一些改革。在[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，阿方索十三世保持[中立政策](../Page/中立国.md "wikilink")，从而使衰弱的西班牙免于战火。

在阿方索十三世支持下，[米格尔·普里莫·德·里韦拉将军于](../Page/米格尔·普里莫·德·里韦拉.md "wikilink")1923年9月13日建立了[独裁政权](../Page/独裁.md "wikilink")。从那时起到1930年，阿方索十三世实际上是依靠这个为人民所厌恶的独裁者维持国王的地位。1931年西班牙爆发了革命，里韦拉政权被推翻。革命的结果是[西班牙第二共和国的建立](../Page/西班牙第二共和国.md "wikilink")。阿方索十三世被迫退位并逃亡。他在流亡生活中于罗马逝世。在西班牙，佛朗哥下令全国哀悼三天。

## 被擁立為法國王位繼承人

1936年9月29日，阿方索十三世的堂伯父[聖海梅公爵在](../Page/阿方索·卡洛斯_\(聖海梅公爵\).md "wikilink")[維也納過世](../Page/維也納.md "wikilink")，無子嗣。公爵部分的支持者遂以[薩利克繼承法](../Page/薩利克繼承法.md "wikilink")，推舉作為堂姪的阿方索十三世為領袖，稱**法國國王阿方斯一世**，直至其過世。名義上他同時成為法國和西班牙的國王，然而，此時的阿方索已經流亡，且在國際上也沒有國家真正認可阿方索的這種地位。而且，此舉亦違反了[烏特勒支條約的規定](../Page/烏特勒支條約.md "wikilink")：法國和西班牙的王位不得由同一人繼承。不過，法國已成為共和國長達幾乎七十年，此時支持君主政體者在法國已經完全毫無政治影響力了。

## 家庭

阿方索十三世在1906年5月31日娶英国[维多利亚女王的外孙女](../Page/维多利亚女王.md "wikilink")[巴腾堡郡主维多利亚·尤金妮亚](../Page/巴腾堡郡主维多利亚·尤金妮亚.md "wikilink")（1887－1969），育有4子2女。

  - 長子：[阿方索](../Page/阿方索，阿斯图里亚斯亲王_\(1907–1938\).md "wikilink")
    （1907年5月10日－1938年9月6日）阿斯图里亚亲王。血友病患者，1933年6月21日放弃本身和后代的王位继承权，先后有过两次贵庶通婚，无子女。在美国迈阿密死于车祸，终年31岁。

<!-- end list -->

  - 次子：[海梅](../Page/海梅_\(塞戈維亞公爵\).md "wikilink")（1908年6月23日－1975年3月20日）安茹和塞戈维亚公爵，1933年6月21日放弃本身和后代的王位继承权，先后有过两次贵庶通婚，有两子；法国波旁王朝正統派现任首领[路易二十是他的第二个孙子](../Page/路易·阿方索_\(安茹公爵\).md "wikilink")，享年66岁。
      - 孫：[阿方索](../Page/阿方索_\(安茹及加的斯公爵\).md "wikilink")
        （1936年～1989年）安茹及加的斯公爵
          - 曾孫：弗朗索瓦·德·波旁 （1972年～1984年）
          - 曾孫：[路易·阿方索](../Page/路易·阿方索_\(安茹公爵\).md "wikilink")
            （1974年～）安茹公爵
              - 玄孫女：歐亨尼婭公主 （2007年～）
              - 玄孫：勃艮第公爵路易 （2010年～）
              - 玄孫：貝里公爵阿方索 （2010年～）
      - 孫：[岡薩洛](../Page/岡薩洛_\(阿基坦公爵\).md "wikilink") （1937年～2000年）
          - 曾孫女：史蒂芬妮·米歇爾·德·波旁
  - 長女：比阿特丽丝（1909年6月22日－2002年11月22日）1935年1月14日嫁意大利的托洛尼亞·迪·希维特利·瑟西家族的亚历山德罗王子（1911年12月7日－1986年5月12日），享年93岁。
      - 外孫女：桑德拉·托洛尼亞 （1936年～）
      - 外孫：馬爾科·托洛尼亞 （1937年～2014年）
      - 外孫：馬里諾托洛尼亞 （1939年～1995年）
      - 外孫女：奧林匹亞托洛尼亞 （1943年～）
  - 次女：玛丽亚·克理丝蒂娜（1911年12月12日－1996年12月23日）1940年6月10日嫁意大利的恩里科·马隆伯爵（1895年3月15日－1968年10月23日），享年85岁。
      - 外孫女：維多利亞·马隆 （1941年～）
      - 外孫女：焦萬娜·马隆 （1943年～）
      - 外孫女：瑪麗亞·特里薩·马隆 （1945年～）
      - 外孫女：安娜·桑德拉·马隆 （1948年～）
  - 三子：[胡安·德·波旁亲王](../Page/胡安·德·波旁.md "wikilink")（1913年6月20日－1993年4月1日）稱**胡安三世**，巴塞罗那伯爵，1935年10月12日娶两西西里王国的玛丽亚·德·拉·玛塞迪丝公主（1910年12月23日－2000年1月2日），享年79岁。西班牙獨裁者[佛朗哥時代之後的第一位國王](../Page/佛朗哥.md "wikilink")[胡安·卡洛斯一世是其長子](../Page/胡安·卡洛斯一世.md "wikilink")。
      - 孫女：巴達霍斯女公爵皮拉 （1936年～）
      - 孫：[胡安·卡洛斯一世](../Page/胡安·卡洛斯一世.md "wikilink") （1938年～）
          - 曾孫：[費利佩六世](../Page/費利佩六世.md "wikilink") （1968年～）
      - 孫女：索里亞女公爵瑪格麗塔 （1939年～）
      - 孫：[阿方索王子](../Page/阿方索王子_\(西班牙\).md "wikilink") （1941年～1956年）
  - 四子：[冈萨罗](../Page/岡薩羅王子_\(西班牙\).md "wikilink")（1914年10月24日－1934年8月13日）血友病患者，在奥地利车祸身亡。未婚，终年19岁。

## 參閱

  - [波旁王朝](../Page/波旁王朝.md "wikilink")

[Category:西班牙君主](../Category/西班牙君主.md "wikilink")
[Category:西班牙波旁王朝](../Category/西班牙波旁王朝.md "wikilink")
[Category:西班牙天主教徒](../Category/西班牙天主教徒.md "wikilink")
[Category:死在意大利的西班牙人](../Category/死在意大利的西班牙人.md "wikilink")
[Category:嘉德騎士](../Category/嘉德騎士.md "wikilink")
[Category:馬德里人](../Category/馬德里人.md "wikilink")