**阿普林縣**（[英語](../Page/英語.md "wikilink")：**Appling
County**）是[美國](../Page/美國.md "wikilink")[喬治亞州的一個縣](../Page/喬治亞州.md "wikilink")。2000年，[人口有](../Page/人口.md "wikilink")17,419人。根據2005年[人口普查](../Page/人口普查.md "wikilink")，人口估計有17,954人。\[1\]
縣政府所在地位於[巴克斯利](../Page/巴克斯利_\(喬治亞州\).md "wikilink")。

## 歷史

阿普林縣是以[1812年戰爭中的](../Page/1812年戰爭.md "wikilink")[丹尼爾·阿普林](../Page/丹尼爾·阿普林.md "wikilink")\[2\][中校的名字命名](../Page/中校.md "wikilink")。

1818年12月15日，阿普林縣由[喬治亞州大會](../Page/喬治亞州大會.md "wikilink")\[3\]一項法案頒佈成立，成為[喬治亞州第](../Page/喬治亞州.md "wikilink")42個縣。原先的縣由-{[克里克族](../Page/克里克族.md "wikilink")}-\[4\]在1814年的《[傑克森堡條約](../Page/傑克森堡條約.md "wikilink")》\[5\]及1818年的《[克-{里}-克條約](../Page/克里克條約.md "wikilink")》\[6\]先後割讓的土地組成。

## 地理

根據[美國人口調查局](../Page/美國人口調查局.md "wikilink")\[7\]，阿普林縣面積有1,326[平方公里](../Page/平方公里.md "wikilink")（512[平方英里](../Page/平方英里.md "wikilink")），陸地佔1,317平方公里（509平方英里），水域佔9平方公里（4平方英里），亦即0.70%。

阿普林縣有不少相鄰縣份，該縣分別與喬治亞州的[塔特諾爾縣](../Page/塔特諾爾縣_\(喬治亞州\).md "wikilink")（東北部）、[韋恩縣](../Page/韋恩縣_\(喬治亞州\).md "wikilink")（東南部）、[皮爾斯縣](../Page/皮爾斯縣_\(喬治亞州\).md "wikilink")（南部）、[傑夫·戴維斯縣](../Page/傑夫·戴維斯縣_\(喬治亞州\).md "wikilink")（西部）、[培根縣](../Page/培根縣_\(喬治亞州\).md "wikilink")（西部）、[图姆斯县](../Page/图姆斯县_\(佐治亚州\).md "wikilink")（北部）接壤。

## 人口統計

2000年[人口普查顯示](../Page/人口普查.md "wikilink")，阿普林縣有17,419人、6,606戶及4,855個家庭。[人口密度為](../Page/人口密度.md "wikilink")13人/[平方公里](../Page/平方公里.md "wikilink")（34人/[平方英里](../Page/平方英里.md "wikilink")）。該縣有7,854個居住單位，平均密度6個居住單位/[平方公里](../Page/平方公里.md "wikilink")（15個居住單位/平方公里）。種族構成當中，[白人佔](../Page/白人.md "wikilink")76.79%，[黑人和](../Page/黑人.md "wikilink")[非裔美國人佔](../Page/非裔美國人.md "wikilink")19.59%，[美國原住民佔](../Page/美國原住民.md "wikilink")0.21%，[亞裔佔](../Page/亞裔美國人.md "wikilink")0.30%，[太平洋島國人佔](../Page/太平洋島國人.md "wikilink")0.01%，其他種族佔2.49%，而[混血兒佔](../Page/混血兒.md "wikilink")0.61%。各種族中的[西班牙裔或](../Page/西班牙裔.md "wikilink")[拉美裔佔人口的](../Page/拉美裔.md "wikilink")4.55%。

該縣6,606戶中，34.50%有未滿18歲的孩子同住，56.60%為同住的[夫婦](../Page/婚姻.md "wikilink")，12.50%為未婚的獨居女性，而26.50%是非家庭。同時，23.20%為個體戶，9.80%為65歲或以上的獨居長者。平均戶規模為2.60人，平均家庭規模為3.04人。年齡分佈方面，未滿18歲人士佔27.10%，18-24歲佔9.00%，25-44歲佔28.50%，45-64歲佔23.50%，65歲或以上佔11.8%。年齡中位數為35歲。男女比例為每100名女性相對有97.10男性，每100名18歲或以上女性相對有93.30名男性。戶收入中位數為30,266[美元](../Page/美元.md "wikilink")，家庭收入中位數為34,890美元。男性收入中位數為27,753美元，女性收入中位數為18,148美元。[人均收入為](../Page/人均收入.md "wikilink")15,044美元，約有14.90%家庭和18.60%人口在[貧窮線下](../Page/貧窮線.md "wikilink")（23.90%未滿18歲，24.40%為65歲或以上）。

## 市鎮

  - [巴克斯利](../Page/巴克斯利_\(喬治亞州\).md "wikilink")
  - [格拉姆](../Page/格拉姆_\(喬治亞州\).md "wikilink")
  - [Surrency](../Page/:en:Surrency,_Georgia.md "wikilink")

## 交通

  - [US_1.svg](https://zh.wikipedia.org/wiki/File:US_1.svg "fig:US_1.svg")
    [美國1號公路](../Page/美國1號公路.md "wikilink")
  - [US_341.svg](https://zh.wikipedia.org/wiki/File:US_341.svg "fig:US_341.svg")
    [美國341號公路](../Page/美國341號公路.md "wikilink")
  - [Georgia_4.svg](https://zh.wikipedia.org/wiki/File:Georgia_4.svg "fig:Georgia_4.svg")
    [喬治亞州4號州幹線](../Page/喬治亞州4號州幹線.md "wikilink")
  - [Georgia_15.svg](https://zh.wikipedia.org/wiki/File:Georgia_15.svg "fig:Georgia_15.svg")
    [喬治亞州15號州幹線](../Page/喬治亞州15號州幹線.md "wikilink")
  - [Georgia_27.svg](https://zh.wikipedia.org/wiki/File:Georgia_27.svg "fig:Georgia_27.svg")
    [喬治亞州27號州幹線](../Page/喬治亞州27號州幹線.md "wikilink")

## 參考

  - [阿普林縣縣府大樓資料](https://web.archive.org/web/20060221234938/http://www.cviog.uga.edu/Projects/gainfo/courthouses/applingCH.htm)（[喬治亞州大學](../Page/喬治亞州大學.md "wikilink")[卡爾文森政府學院官方網站](../Page/卡爾文森政府學院.md "wikilink")）

  - [喬治亞州](http://www.georgiaInfo.com)（georgiaInfo.com）

## 注

<div class="references-small">

<references/>

</div>

## 參見

  - [喬治亞州](../Page/喬治亞州.md "wikilink")

[A](../Category/佐治亚州行政区划.md "wikilink")
[Category:1818年建立](../Category/1818年建立.md "wikilink")

1.  \[<http://www.census.gov/popest/counties/tables/CO-EST2005-01-13.xls>　表格1：喬治亞洲縣份全年人口估值：2000年4月1日-2005年7月1日\]（[英國人口調查局官方網站](../Page/英國人口調查局.md "wikilink")）

2.  [丹尼爾·阿普林英語原名Daniel](../Page/丹尼爾·阿普林.md "wikilink") Appling。
3.  [喬治亞州大會英語原名](../Page/喬治亞州大會.md "wikilink")「Georgia General
    Assembly」。
4.  \-{[克里克族](../Page/克里克族.md "wikilink")}-英語原名Creek。
5.  《[傑克森堡條約](../Page/傑克森堡條約.md "wikilink")》英語原稱《Treaty of Fort
    Jackson》。
6.  《[克-{里}-克條約](../Page/克里克條約.md "wikilink")》英語原稱《Treaty of the Creek
    Agency》。
7.  [美國人口調查局英語原名U](../Page/美國人口調查局.md "wikilink").S. Census Bureau。