**阿曼達·斯特朗**（，），華文傳媒通稱**Amanda
S.**，2019年1月起中文姓名為**龍玥心**，[法國](../Page/法國.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")，父為法國商人，母為台灣人，在[香港出道](../Page/香港.md "wikilink")。曾在[台灣](../Page/台灣.md "wikilink")、[印度](../Page/印度.md "wikilink")、[法國居住](../Page/法國.md "wikilink")，留學[美國](../Page/美國.md "wikilink")[波士頓主修](../Page/波士頓.md "wikilink")[商業及](../Page/商業.md "wikilink")[經濟](../Page/經濟.md "wikilink")。後來在香港被發掘，獲邀在[化妝品](../Page/化妝品.md "wikilink")[SK-II的](../Page/SK-II.md "wikilink")[廣告擔任主角](../Page/廣告.md "wikilink")，令她轉移到[香港發展其模特兒事業](../Page/香港.md "wikilink")。曾在[郭富城](../Page/郭富城.md "wikilink")[MV中任女主角](../Page/MV.md "wikilink")，並與郭富城傳出緋聞。隨後與經理人公司[Starz
People簽約](../Page/Starz_People.md "wikilink")，並暫停其大學學業。阿曼達·斯特朗曾參與[TVB電視烹飪節目](../Page/TVB.md "wikilink")《[美女廚房](../Page/美女廚房.md "wikilink")》，並曾獻吻予[陳小春](../Page/陳小春.md "wikilink")、[方力申](../Page/方力申.md "wikilink")、[梁漢文及評判之一的](../Page/梁漢文.md "wikilink")[周中師傅](../Page/周中.md "wikilink")。

前夫Christian是[意大利人](../Page/意大利.md "wikilink")，兩人交往9年，於2011年9月11日在[羅馬舉行](../Page/羅馬.md "wikilink")[婚禮](../Page/婚禮.md "wikilink")。2014年7月，阿曼達·斯特朗宣佈已跟丈夫協議離婚。

身高170[公分](../Page/公分.md "wikilink")，體重48[公斤](../Page/公斤.md "wikilink")。

## 電影

  - [願望樹](../Page/願望樹.md "wikilink") (2001)
  - [絕色神偷](../Page/絕色神偷.md "wikilink") (2001)
  - [豐胸秘Cup](../Page/豐胸秘Cup.md "wikilink") (2002)
  - [心想事成](../Page/心想事成.md "wikilink") (2007)
  - [十分鍾情](../Page/十分鍾情.md "wikilink") (2008)

## 參考連結

  - [Amanda Strang-個人檔案](http://www.it.com.cn/f/hotweb/058/4/153594.htm)
  - [小春做觀音兵
    勇奪Amanda香吻](https://web.archive.org/web/20061208174109/http://www.singpao.com/20060726/gossip/860187.html)
  - [Amanda
    S.男友與人逛夜蒲名模身材豐滿](http://ent.sina.com.cn/s/h/2006-10-20/08121291927.html)

[Category:法國電影女演員](../Category/法國電影女演員.md "wikilink")
[Category:法國女性模特兒](../Category/法國女性模特兒.md "wikilink")
[Category:在香港的法國人](../Category/在香港的法國人.md "wikilink")
[Category:法籍華人](../Category/法籍華人.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港飲食界人士](../Category/香港飲食界人士.md "wikilink")
[Category:台灣裔香港人](../Category/台灣裔香港人.md "wikilink")
[Category:台灣裔法國人](../Category/台灣裔法國人.md "wikilink")