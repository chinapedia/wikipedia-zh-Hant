**阿格里真托**（
，[西西里語](../Page/西西里語.md "wikilink")：Girgenti）位于[意大利](../Page/意大利.md "wikilink")[西西里岛南海岸的中央点](../Page/西西里岛.md "wikilink")，是[阿格里真托省的首府](../Page/阿格里真托省.md "wikilink")，自古以来都是扼守[地中海的军事重镇](../Page/地中海.md "wikilink")，面积244平方公里，人口59,791人（3/2016）。阿格里真托市盛产柑橘、橄榄油和硫磺矿，经附近的港口[恩贝多利销往世界各地](../Page/恩贝多利.md "wikilink")。阿格里真托市还拥有大批的古建筑遗址和大量的出土文物，世界文化遗产[神殿之谷位于此地](../Page/神殿之谷.md "wikilink")。

## 友好城市

  - [美国](../Page/美国.md "wikilink")[坦帕](../Page/坦帕_\(佛罗里达州\).md "wikilink")

  - [法国](../Page/法国.md "wikilink")[瓦朗谢讷](../Page/瓦朗谢讷.md "wikilink")

  - [俄罗斯](../Page/俄罗斯.md "wikilink")[彼尔姆](../Page/彼尔姆.md "wikilink")

## 外部链接

  - [The Valley of the Temples. A visitor's guide to the Valley of the
    Temples and Agrigento](http://www.valleyofthetemples.com/)
  - [Paradoxplace Agrigento Photo & History
    Page](https://web.archive.org/web/20160603092703/http://paradoxplace.com/Perspectives/Sicily%20%26%20S%20Italy/Montages/Sicily/Other%20Sicily/Agrigento.htm)
  - [InfoAgrigento.it Tourist Point - the tourist portal of Agrigento
    and the Valley of the
    Temples](https://web.archive.org/web/20090305035051/http://accomodation.infoagrigento.it/)

[A](../Category/阿格里真托省市镇.md "wikilink")
[Category:希腊城邦](../Category/希腊城邦.md "wikilink")