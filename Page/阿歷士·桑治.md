**亞歷山大·迪米特里·桑治·比朗**（**Alexandre Dimitri Song
Billong**，），通稱**亞歷士·宋**（**Alex
Song**），出生於[喀麥隆](../Page/喀麥隆.md "wikilink")[杜阿拉](../Page/杜阿拉.md "wikilink")，是一名喀麥隆足球運動員，目前效力於[錫永](../Page/錫永足球俱樂部.md "wikilink")。他是喀麥隆名將（Rigobert
Song）的侄子，司職[防守中場](../Page/防守中場.md "wikilink")，也能客串[中堅位置](../Page/中堅.md "wikilink")。

## 球會生涯

  - 巴斯蒂亞

桑治於2003-2004年加入[巴斯蒂亞青年隊](../Page/巴斯蒂亚运动俱乐部.md "wikilink")。於翌季晉升上一隊並上陣34次。他在巴斯蒂亞時，受到幾家大球會（包括[國際米蘭](../Page/國際米蘭.md "wikilink")、[曼聯](../Page/曼聯.md "wikilink")、[里昂和](../Page/里昂足球會.md "wikilink")[米杜士堡](../Page/米杜士堡.md "wikilink")）的注意。\[1\]可是，2005年至2006年球季，他在阿仙奴往[奧地利的季前集訓時被領隊](../Page/奧地利.md "wikilink")[雲加看中](../Page/雲加.md "wikilink")，並以借用形式加盟。於2006年6月，阿仙奴同意以100萬英鎊買斷他，並簽下四年合約。\[2\]

  - 阿仙奴

桑治於2005年9月19日對[愛華頓的](../Page/愛華頓.md "wikilink")[英超聯賽以後備身份首次為阿仙奴上陣](../Page/英超聯賽.md "wikilink")，最終球會以2:0取勝。其後，他亦有在歐聯上陣數次，並且在聯賽以舉行至最尾數場時，多次以正選姿態上陣，以取代需要養傷或休息的[法比加斯和](../Page/法比加斯.md "wikilink")[基拔圖施華](../Page/基拔圖施華.md "wikilink")。

於2007年1月9日，阿仙奴於[聯賽盃](../Page/英格蘭聯賽盃.md "wikilink")8強作客[晏菲路球場對](../Page/晏菲路球場.md "wikilink")[利物浦](../Page/利物浦.md "wikilink")，桑治為球會射入他自加盟以來首個入球，助球隊終以6:3大勝。

於2007年1月30日，桑治以借用形成加盟[查爾頓](../Page/查爾頓.md "wikilink")，直到[2006-07球季完結](../Page/2006年至2007年英格蘭超級聯賽.md "wikilink")。\[3\]
雖然他表現不錯，但查爾頓最終降班，而桑治則返回阿仙奴。

在[2007-08球季](../Page/2007年至2008年英格蘭超級聯賽.md "wikilink")，桑治在聯賽盃的賽事正選上陣，並擔任中堅，但他因被[喀麥隆徵召參加](../Page/喀麥隆國家足球隊.md "wikilink")[2008年非洲國家盃](../Page/2008年非洲國家盃.md "wikilink")，所以錯過了四強對[熱刺的賽事](../Page/熱刺.md "wikilink")，球會最後被淘汰。球隊於聯賽最後數場，亦以中堅身份正選上陣。令人最深刻的，是作客[奧脫福球場對](../Page/奧脫福球場.md "wikilink")[曼聯](../Page/曼聯.md "wikilink")，由於正選中堅[高路托尼需要頂替](../Page/高路托尼.md "wikilink")[沙格拿擔任右後衛](../Page/沙格拿.md "wikilink")，而另一中堅[辛德路斯因在](../Page/辛德路斯.md "wikilink")[歐聯八強的失誤以致信心動搖](../Page/2007年至2008年歐洲聯賽冠軍盃.md "wikilink")，所以改由他正選擔任中堅。然而其表現未如理想，多次出現防守失誤。

2009/10賽季，這是桑治成功的一季，在阿仙奴開季的前12場聯賽都以正選身入上陣。2009年11月25日，桑治與阿仙奴續約至2014年。\[4\]
12月30日，桑治取得在阿仙奴的第二個入球，協助球隊以4-1擊敗[樸茨茅夫](../Page/朴茨茅斯足球俱乐部.md "wikilink")。桑治在這賽季進步非常明顯，至現在已經成為球隊的絕對主力。

2010/11賽季，桑治於聯賽第四輪主場對[保頓的賽事中攻入一個難度極高的精彩入球](../Page/保頓.md "wikilink")，協助球隊以4-1大勝。

2011/12賽季，前隊長[法比加斯的離隊及](../Page/法比加斯.md "wikilink")[韋舒亞的傷患](../Page/韋舒亞.md "wikilink")，令桑治在陣中成為不可或缺的球員，作為防守中場的他整個賽季交出11次助攻，可謂能攻善守。

2012/13賽季，阿仙奴8月19日在官方網站宣佈，與巴塞隆拿對桑治的轉會達成協議。據披露，桑治這次的轉會費為1500萬英鎊，他和巴塞談妥了一份5年合約，毀約金高達8000萬歐元

## 國際隊生涯

雖然以前未為國家隊上陣，但桑治仍然被徵召參加2008年的[非洲國家盃](../Page/非洲國家盃.md "wikilink")。他首次為國上陣是於第一場的分組賽，以4:2擊敗[埃及](../Page/埃及國家足球隊.md "wikilink")，他半場入替史提芬尼·麥比亞（Stephane
Mbia），和他叔父尼高拔並肩作戰。在這比賽期間，桑治在喀麥隆的防守擔當重要的角色，更於四強賽事獲得喀麥隆最佳球員榮譽。可惜於決賽對埃及受傷，間接令球隊落敗。但他依然和國家隊隊友，現效力[紐卡素的](../Page/紐卡素.md "wikilink")[中場](../Page/中場.md "wikilink")[謝利美一起當選賽事最佳](../Page/謝利美.md "wikilink")11人。（註：但他在最佳11人名單中，和原為中場的謝利美掉換位置，謝利美成為[後衛](../Page/後衛_\(足球\).md "wikilink")，而桑治則成為[中場](../Page/中場.md "wikilink")。）

桑治憑藉在[阿仙奴出色的表現而入選了](../Page/阿仙奴.md "wikilink")[喀麥隆出戰](../Page/喀麥隆.md "wikilink")[2010年世界盃的名單](../Page/2010年世界盃足球賽.md "wikilink")。

## 榮譽

  - 個人

<!-- end list -->

  - [非洲國家盃最佳陣容](../Page/非洲國家盃.md "wikilink")：2008年、2010年

## 參考資料

## 外部連結

  -
  -
  - [阿仙奴官方
    桑治檔案](https://web.archive.org/web/20100826033343/http://www.arsenal.com/first-team/players/24021/alexandre-song/)

[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")
[Category:2010年非洲國家盃球員](../Category/2010年非洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2008年夏季奧林匹克運動會足球運動員](../Category/2008年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:2008年非洲國家盃球員](../Category/2008年非洲國家盃球員.md "wikilink")
[Category:喀麥隆國家足球隊球員](../Category/喀麥隆國家足球隊球員.md "wikilink")
[Category:喀麦隆足球运动员](../Category/喀麦隆足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:巴斯蒂亞球員](../Category/巴斯蒂亞球員.md "wikilink")
[Category:阿仙奴球員](../Category/阿仙奴球員.md "wikilink")
[Category:查爾頓球員](../Category/查爾頓球員.md "wikilink")
[Category:巴塞隆拿球員](../Category/巴塞隆拿球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:法國外籍足球運動員](../Category/法國外籍足球運動員.md "wikilink")
[Category:喀麥隆旅外足球運動員](../Category/喀麥隆旅外足球運動員.md "wikilink")
[Category:杜阿拉人](../Category/杜阿拉人.md "wikilink")

1.
2.
3.
4.