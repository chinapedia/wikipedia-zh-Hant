[Apollo_program.svg](https://zh.wikipedia.org/wiki/File:Apollo_program.svg "fig:Apollo_program.svg")
[Apollo_11_first_step.jpg](https://zh.wikipedia.org/wiki/File:Apollo_11_first_step.jpg "fig:Apollo_11_first_step.jpg")

**阿波羅計划**（）或作**阿波罗工程**，[港澳地區及臺灣有時稱其為](../Page/港澳地區.md "wikilink")**太陽神計划**，是[美國太空總署从](../Page/美國太空總署.md "wikilink")1961年至1972年从事的一系列[載人航天任务](../Page/載人航天.md "wikilink")，於1960年代的10年中，主要致力于完成载人[登陸月球和安全返回地球的目标](../Page/登陸月球.md "wikilink")。1969年，[阿波罗11号](../Page/阿波罗11号.md "wikilink")[宇宙飞船达成了上述目标](../Page/宇宙飞船.md "wikilink")，[尼尔·阿姆斯特朗成为第一个踏足月球表面的人类](../Page/尼尔·阿姆斯特朗.md "wikilink")。为了进一步执行在月球的科学探测，阿波羅計划一直延续到1970年代早期。总共耗资约240亿美元（相当于2016年時的1070亿美元），因此有人认为，资金是美国能夠领先一步登陸月球的最大因素。

阿波羅計划是美國太空總署执行的迄今为止最庞大的月球探测計划，“阿波羅”飞船的任务包括为载人登月飞行作准备和实现载人登月飞行，已于1972年底结束。迄今（[年](../Page/{{CURRENTYEAR}}年.md "wikilink")）40多年來还没有过其他的载人航天器离开过地球轨道。阿波羅計划详细地揭示了月球表面特性、物质化学成份、光学特性并探测了月球[重力](../Page/重力.md "wikilink")、[磁场](../Page/磁场.md "wikilink")、月震等。后来的[天空实验室計划和美国](../Page/天空实验室計划.md "wikilink")、苏联联合的[阿波羅-联盟测试計划也使用了原来为阿波羅建造的设备](../Page/阿波羅-联盟测试計划.md "wikilink")，也就经常被认为是阿波羅計划的一部分。

阿波羅計划取得了巨大的成功，惟計划中也有过几次严重的危机，包括[阿波羅1號测试时的大火造成](../Page/阿波羅1號.md "wikilink")[维吉尔·格里森](../Page/维吉尔·格里森.md "wikilink")、[爱德华·怀特和](../Page/爱德华·怀特.md "wikilink")[罗杰·查菲的死亡](../Page/罗杰·查菲.md "wikilink")；[阿波羅13號的氧气罐爆炸以及](../Page/阿波羅13號.md "wikilink")[阿波羅-联盟测试計划返回大气层时排放的有毒气体都几乎使执行任务的宇航员丧命](../Page/阿波羅-联盟测试計划.md "wikilink")。

## 背景

阿波羅計划于1960年早期在[艾森豪威尔执政时被提出](../Page/德怀特·艾森豪威尔.md "wikilink")，作为[水星计划的后续计划](../Page/水星计划.md "wikilink")。水星计划使用的航天器只能进入地球轨道，只能搭载一名宇航员，而预想中的阿波罗航天器不仅能搭载三名宇航员，也许还可以登月。[美國太空總署经理阿伯](../Page/美國太空總署.md "wikilink")·西尔弗斯坦（Abe
Silverstein）当时选择以[希腊神话中的](../Page/希腊神话.md "wikilink")[太阳神命名此计划](../Page/阿波罗.md "wikilink")，事后提到这是他给自己的儿子预留的名字\[1\]。虽然航空航天局已经开始进行计划，但艾森豪威尔对航天计划似乎并不热衷，因此他们并没有得到足够的经费进行阿波罗计划\[2\]。
[John_F._Kennedy_speaks_at_Rice_University.jpg](https://zh.wikipedia.org/wiki/File:John_F._Kennedy_speaks_at_Rice_University.jpg "fig:John_F._Kennedy_speaks_at_Rice_University.jpg")[约翰·甘迺迪在](../Page/约翰·肯尼迪.md "wikilink")[莱斯大学发表关于太空计划的演讲](../Page/莱斯大学.md "wikilink")。\]\]

1960年11月，竞选时承诺要使美国在太空探索和导弹防御上全面超过[苏联的](../Page/苏联.md "wikilink")[约翰·肯尼迪当选总统](../Page/约翰·肯尼迪.md "wikilink")。虽然对太空计划较为热衷，他在当选总统后并没有立刻决定开始登月计划，甚至還打算砍掉一些花費大回報低的計畫。肯尼迪对航天事业并不十分了解，太空探索需要的大量资金也使他不敢轻易做出决定。\[3\]当航空航天局局长[詹姆斯·韦伯要求年度财政预算增加百分之三十时](../Page/詹姆斯·韦伯.md "wikilink")，肯尼迪支持加快发展大规模推进器的研发，却没有支持其他更大的项目\[4\]。

1961年4月12日，苏联宇航员[尤里·加加林成为了首次进入太空的人类](../Page/尤里·加加林.md "wikilink")，加深了美国对在太空竞赛中落后的恐惧。次日，在与[白宫科学委员会的会谈中](../Page/白宫科学委员会.md "wikilink")，许多议员希望能够立刻开始一项太空计划以保证在于苏联的竞赛中不至于落后太多\[5\]。肯尼迪对此事却较为谨慎，不愿意立刻进行任何重大举措。\[6\]4月20日，肯尼迪给副总统[林登·约翰逊发去备忘录](../Page/林登·约翰逊.md "wikilink")，询问他对于美国太空计划的意见，以及美国追赶苏联的可能性\[7\]。在翌日的回复中，约翰逊认为“我们既没有尽最大努力，也没有达到让美国保持领先的程度\[8\]\[9\]。”约翰逊还提到未来登月的计划不仅可行，也绝对可以使美国在太空竞赛中获得领先地位\[10\]。
[Kennedy_Giving_Historic_Speech_to_Congress_-_GPN-2000-001658.jpg](https://zh.wikipedia.org/wiki/File:Kennedy_Giving_Historic_Speech_to_Congress_-_GPN-2000-001658.jpg "fig:Kennedy_Giving_Historic_Speech_to_Congress_-_GPN-2000-001658.jpg")
1961年5月25日，肯尼迪在参、众两院特别会议中宣布支持阿波罗计划：

肯尼迪发表这段演讲前一个月，美国刚刚将第一个宇航员送入太空，还没有进入地球轨道。这种不利局面使一些航空航天局的工作人员对登月计划并不十分乐观\[11\]。

## 选择任务模式

[Apollo_Direct_Ascent_Concept.jpg](https://zh.wikipedia.org/wiki/File:Apollo_Direct_Ascent_Concept.jpg "fig:Apollo_Direct_Ascent_Concept.jpg")
在登月成为主要目标后，阿波罗计划的决策者们开始面临如何才能尽可能安全、经济、简单地将宇航员送上月球。曾有四个方案被考虑：

  - **[直接起飞](../Page/直接起飞.md "wikilink")：**此计划提出由一个巨大的[新星火箭携带一艘航天器](../Page/新星火箭.md "wikilink")，直接飞往月球；火箭在月球降落，任务完成后再次起飞，飞回地球。
  - **[地球轨道集合](../Page/地球轨道集合.md "wikilink")：**此计划需要两艘只有[土星5号一半大小的小型](../Page/土星5号.md "wikilink")[火箭将登月](../Page/火箭.md "wikilink")[航天器的不同部分送入地球轨道](../Page/航天器.md "wikilink")，集合-{}-并对接。整个航天器降落在月球表面。由于当时在轨道中集合多艘航天器的经验较少，且地球轨道拼装航天器是否可行也是未知数，所以此计划未被采纳。
  - **月球表面集合：**此计划需要两艘航天器被发射：一艘自动航天器携带推进系统，先期登月；载人航天器晚些发射。推进系统在月球表面被移至载人航天器上，然后返回地球。
  - **[月球轨道集合](../Page/月球轨道集合.md "wikilink")：**
    这个方案由约翰·C·霍博尔特的团队提出。这种方案是一艘较大的航天器，称为[指揮/服务舱](../Page/阿波罗指揮/服务舱.md "wikilink")，携带一艘装载宇航员的登月航天器，称为[登月舱](../Page/阿波罗登月舱.md "wikilink")。指揮/服务携带从地球到月球并返回的燃料和生活必需品，以及进入地球大气层所需要的隔热板。进入月球轨道之后，登月舱与指揮/服务分离，并降落在月球表面；指揮/服务留在月球轨道。3名宇航员中的1名留在指揮/服务中。登月完成之后，登月舱重新起飞，与指揮/服务在月球轨道集合，并返回地球。

与其他几个方案不同，月球轨道集合只需要一艘很小的航天器降落在月球表面，使返回时在月球上起飞航天器的质量大大减小。通过将登月舱的一部分留在月球上，月球起飞质量得以再次减小。

[登月舱本身分为两部分](../Page/阿波罗登月舱.md "wikilink")，包括降落部分和起飞部分，前者用于在登月时降落，后者在任务完成后起飞与指揮/服务舱会合并返回地球。由于航天器质量减轻，一次任务只需要一次单独的火箭发射。当时的顾虑是次数较多的对接和分离所提出的技术难度。

## 航天器

最终选择了月球轨道集合意味着航天器将包括三个主要部分：指揮舱、服务舱以及[登月舱](../Page/阿波罗登月舱.md "wikilink")。

### 指揮/服务舱

[Apollo_CSM_lunar_orbit.jpg](https://zh.wikipedia.org/wiki/File:Apollo_CSM_lunar_orbit.jpg "fig:Apollo_CSM_lunar_orbit.jpg")
[Splashdown_2.jpg](https://zh.wikipedia.org/wiki/File:Splashdown_2.jpg "fig:Splashdown_2.jpg")
指揮舱（Command
Module，CM）呈锥形，用于搭载宇航员从地面一直到月球轨道，是唯一在任务完成时返回地球的部分。指揮舱的设备包括反应控制推进器、对接口、导航系统以及阿波罗导航计算机。服务舱（Service
Module，SM）中储存了宇航员需要的各种设备，例如服务推动系统、燃料、氧气罐、机动喷口和通讯天线。在阿波罗15号任务中，航天器进入月球轨道时的推进器也位于服务舱的末尾。指揮舱与服务舱被称为指揮/服务舱（CSM）。服务舱还携带了科学仪器模块。返回大气层前，服务舱被丢弃。只有指揮舱外层有隔热板，能够抵挡进入大气层时的高温。进入大气层后，指揮舱打开[降落伞](../Page/降落伞.md "wikilink")，逐渐减速并降落在海面等待救援。

[北美航空竞标成功](../Page/北美航空.md "wikilink")，获得了指揮/服务舱的合同，由哈里森·斯托姆斯（Harrison
Storms）负责。北美航空和航空航天局的关系一度紧张，特别是阿波罗1号的大火使三名宇航员丧生后。大火是指揮舱内的电线短路引起的，原因非常复杂，调查小组的结论是“指揮舱的设计、制作工艺和质量控制都有问题\[12\]\[13\]。”

### 登月舱

[Apollo_16_LM.jpg](https://zh.wikipedia.org/wiki/File:Apollo_16_LM.jpg "fig:Apollo_16_LM.jpg")

[登月舱是真正登月时使用的部分](../Page/阿波罗登月舱.md "wikilink")。为了尽可能减轻重量，登月舱没有隔热板，动力很小，只能在月球表面飞行。登月舱能够搭载两名宇航员，包括一个降落部分和一个起飞部分。后者在登月任务完成时使用前者作为发射平台，进入月球轨道后与指揮/服务舱对接，准备返回地球。降落部分里还装载了阿波罗科学实验包，以及最后三次任务中的月球车。

[格鲁门航空航天公司进行登月舱的设计和建造](../Page/格鲁门公司.md "wikilink")，负责人是汤姆·凯利（Tom
Kelly）。登月舱也出现过许多问题；由于其测试的延迟，整个阿波罗计划都险些没能成功\[14\]。由于登月舱进度缓慢，首次登月舱的载人飞行也不得不推迟，使得阿波罗8号9号互换。阿波罗9号在地球轨道中首次使用了登月舱，阿波罗10号将登月舱带到了月球轨道（但没有登月），对整个航天器的各个部分进行了详细的测试。

## 各次任务

[Moon_map_showing_apollo_missions.PNG](https://zh.wikipedia.org/wiki/File:Moon_map_showing_apollo_missions.PNG "fig:Moon_map_showing_apollo_missions.PNG")
阿波罗计划中包括11次载人任务，从[阿波罗7号一直到](../Page/阿波罗7号.md "wikilink")[阿波罗17号](../Page/阿波罗17号.md "wikilink")，全部从[佛罗里达州的](../Page/佛罗里达州.md "wikilink")[肯尼迪航天中心发射](../Page/肯尼迪航天中心.md "wikilink")。[阿波罗4号到](../Page/阿波罗4号.md "wikilink")[阿波罗6号都是无人测试飞行](../Page/阿波罗6号.md "wikilink")（正式地讲没有阿波罗2号和阿波罗3号）。

1967年9月，位于[休斯敦的](../Page/休斯敦.md "wikilink")[载人航天中心提出了一系列任务以完成登月任务](../Page/林顿·约翰逊太空中心.md "wikilink")。七个任务类型被提出，每个类型都对特定的航天器和任务进行测试；每一个任务类型的执行都需要前一类型的成功完成。这些任务类型分别是：

  - \- 无人指揮/服务舱测试

  - \- 无人[登月舱测试](../Page/阿波罗登月舱.md "wikilink")

  - \- 载人[近地轨道指揮](../Page/近地轨道.md "wikilink")/服务舱飞行

  - \- 载人近地轨道指揮/服务舱和登月舱飞行

  - \- 载人指揮/服务舱和登月舱绕地球进行椭圆轨道飞行，远地点7400千米

  - \- 载人月球轨道指揮/服务舱和登月舱飞行

  - \- 载人登月

之后又增加了任务，-{月表}-停留时间被加长；最后的任务中，登月舱在月球表面停留三天。被取消的阿波罗18号至20号都是任务。

最遥远的设想甚至还包括了任务，有相当大的科学研究比例。当预算缩减成为现实时，这些科学研究项目被放到了任务里。

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/阿波罗1号.md" title="wikilink">阿波罗1号</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo_1_patch.png" title="fig:Apollo_1_patch.png">Apollo_1_patch.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗7号.md" title="wikilink">阿波罗7号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:AP7lucky7.png" title="fig:AP7lucky7.png">AP7lucky7.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗8号.md" title="wikilink">阿波罗8号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo-8-patch.png" title="fig:Apollo-8-patch.png">Apollo-8-patch.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗9号.md" title="wikilink">阿波罗9号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo-9-patch.png" title="fig:Apollo-9-patch.png">Apollo-9-patch.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗10号.md" title="wikilink">阿波罗10号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo-10-LOGO.png" title="fig:Apollo-10-LOGO.png">Apollo-10-LOGO.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗11号.md" title="wikilink">阿波罗11号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo_11_insignia.png" title="fig:Apollo_11_insignia.png">Apollo_11_insignia.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗12号.md" title="wikilink">阿波罗12号</a></p></td>
</tr>
<tr class="odd">
<td><p>|<span style="color:blue"><strong>任務成功 - 登陸月球</strong></span><br />
阿波罗11号后不到四个月，阿波罗12号再次踏上了登月的旅程。按照宇航员的轮换制度，如果没有添加阿波罗8号，<a href="../Page/皮特·康拉德.md" title="wikilink">皮特·康拉德的团队将会执行首次登月任务</a>。发射后，阿波罗12号被两次闪电击中，航天器一度很不稳定。由于地面指挥中心和三位宇航员（尤其是<a href="../Page/艾伦·宾.md" title="wikilink">艾伦·宾</a>）的冷静处理一切恢复正常，但指挥人员仍一度担心指令舱顶部降落伞遭到损坏；如果主降落伞真的无法使用，返回时航天器将像陨石一般坠入海中。</p>
<p>1969年11月19日阿波罗12号成功登月。前一次任务对降落点的要求不高，但阿波罗12号有一准确降落点，康拉德也轻松完成了任务。踏上月球后，身材矮小的康拉德[15]叫道：</p>
<p>计划中阿波罗12号将首次使用彩色摄像机向地球进行实况转播，但由于宾的失误，摄像机没能使用。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗13号.md" title="wikilink">阿波罗13号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo_13-insignia.png" title="fig:Apollo_13-insignia.png">Apollo_13-insignia.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗14号.md" title="wikilink">阿波罗14号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo_14-insignia.png" title="fig:Apollo_14-insignia.png">Apollo_14-insignia.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗15号.md" title="wikilink">阿波罗15号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo_15-insignia.png" title="fig:Apollo_15-insignia.png">Apollo_15-insignia.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗16号.md" title="wikilink">阿波罗16号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo-16-LOGO.png" title="fig:Apollo-16-LOGO.png">Apollo-16-LOGO.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿波罗17号.md" title="wikilink">阿波罗17号</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Apollo_17-insignia.png" title="fig:Apollo_17-insignia.png">Apollo_17-insignia.png</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

[Apollo_11_Saturn_V_lifting_off_on_July_16,_1969.jpg](https://zh.wikipedia.org/wiki/File:Apollo_11_Saturn_V_lifting_off_on_July_16,_1969.jpg "fig:Apollo_11_Saturn_V_lifting_off_on_July_16,_1969.jpg")于1969年7月16日UTC时间13点32分(美国东区时间是上午9点32分)发射\]\]

## 宇航员

以下的[宇航员曾在阿波罗计划中执行过飞行任务](../Page/宇航员.md "wikilink")：

来自**[水星计划7人](../Page/水星计划7人.md "wikilink")**

  - [瓦尔特·施艾拉](../Page/瓦尔特·施艾拉.md "wikilink") –
    [阿波罗7号](../Page/阿波罗7号.md "wikilink")
  - [艾伦·谢泼德](../Page/艾伦·谢泼德.md "wikilink") –
    [阿波罗14号](../Page/阿波罗14号.md "wikilink")

来自**[第二组宇航员](../Page/第二组宇航员.md "wikilink")**

  - [尼尔·阿姆斯特朗](../Page/尼尔·阿姆斯特朗.md "wikilink") –
    [阿波罗11号](../Page/阿波罗11号.md "wikilink")
  - [弗兰克·博尔曼](../Page/弗兰克·博尔曼.md "wikilink") –
    [阿波罗8号](../Page/阿波罗8号.md "wikilink")
  - [皮特·康拉德](../Page/皮特·康拉德.md "wikilink") –
    [阿波罗12号](../Page/阿波罗12号.md "wikilink")
  - [吉姆·洛威尔](../Page/吉姆·洛威尔.md "wikilink") –
    [阿波罗8号](../Page/阿波罗8号.md "wikilink")、[阿波罗13号](../Page/阿波罗13号.md "wikilink")
  - [詹姆斯·麦克迪维特](../Page/詹姆斯·麦克迪维特.md "wikilink") –
    [阿波罗9号](../Page/阿波罗9号.md "wikilink")
  - [托马斯·斯塔福德](../Page/托马斯·斯塔福德.md "wikilink") –
    [阿波罗10号](../Page/阿波罗10号.md "wikilink")、[阿波罗-联盟测试计划](../Page/阿波罗-联盟测试计划.md "wikilink")
  - [约翰·杨](../Page/约翰·杨.md "wikilink") –
    [阿波罗10号](../Page/阿波罗10号.md "wikilink")、[阿波罗16号](../Page/阿波罗16号.md "wikilink")

来自**[第三组宇航员](../Page/第三组宇航员.md "wikilink")**

  - [巴兹·奥尔德林](../Page/巴兹·奥尔德林.md "wikilink") –
    [阿波罗11号](../Page/阿波罗11号.md "wikilink")
  - [威廉·安德斯](../Page/威廉·安德斯.md "wikilink") –
    [阿波罗8号](../Page/阿波罗8号.md "wikilink")
  - [艾伦·宾](../Page/艾伦·宾.md "wikilink") –
    [阿波罗12号](../Page/阿波罗12号.md "wikilink")
  - [尤金·塞尔南](../Page/尤金·塞尔南.md "wikilink") –
    [阿波罗10号](../Page/阿波罗10号.md "wikilink")、[阿波罗17号](../Page/阿波罗17号.md "wikilink")
  - [迈克尔·科林斯](../Page/迈克尔·科林斯.md "wikilink") –
    [阿波罗11号](../Page/阿波罗11号.md "wikilink")
  - [瓦尔特·康尼翰](../Page/瓦尔特·康尼翰.md "wikilink") –
    [阿波罗7号](../Page/阿波罗7号.md "wikilink")
  - [唐·埃斯利](../Page/唐·埃斯利.md "wikilink") –
    [阿波罗7号](../Page/阿波罗7号.md "wikilink")
  - [理查德·戈尔登](../Page/理查德·戈尔登.md "wikilink") –
    [阿波罗12号](../Page/阿波罗12号.md "wikilink")
  - [拉塞尔·施威卡特](../Page/拉塞尔·施威卡特.md "wikilink") –
    [阿波罗9号](../Page/阿波罗9号.md "wikilink")
  - [大卫·斯科特](../Page/大卫·斯科特.md "wikilink") –
    [阿波罗9号](../Page/阿波罗9号.md "wikilink")、[阿波罗15号](../Page/阿波罗15号.md "wikilink")

来自**[第四组宇航员](../Page/第四组宇航员.md "wikilink")**

  - [哈里森·施密特](../Page/哈里森·施密特.md "wikilink") –
    [阿波罗17号](../Page/阿波罗17号.md "wikilink")

来自**[第五组宇航员](../Page/第五组宇航员.md "wikilink")**

  - [查尔斯·杜克](../Page/查尔斯·杜克.md "wikilink") –
    [阿波罗16号](../Page/阿波罗16号.md "wikilink")
  - [罗纳德·埃万斯](../Page/罗纳德·埃万斯.md "wikilink") –
    [阿波罗17号](../Page/阿波罗17号.md "wikilink")
  - [弗莱德·海斯](../Page/弗莱德·海斯.md "wikilink") –
    [阿波罗13号](../Page/阿波罗13号.md "wikilink")
  - [詹姆斯·艾尔文](../Page/詹姆斯·艾尔文.md "wikilink") –
    [阿波罗15号](../Page/阿波罗15号.md "wikilink")
  - [肯·马丁利](../Page/肯·马丁利.md "wikilink") –
    [阿波罗16号](../Page/阿波罗16号.md "wikilink")
  - [艾德加·米切尔](../Page/艾德加·米切尔.md "wikilink") –
    [阿波罗14号](../Page/阿波罗14号.md "wikilink")
  - [斯图尔特·罗萨](../Page/斯图尔特·罗萨.md "wikilink") –
    [阿波罗14号](../Page/阿波罗14号.md "wikilink")
  - [杰克·斯威格特](../Page/杰克·斯威格特.md "wikilink") –
    [阿波罗13号](../Page/阿波罗13号.md "wikilink")
  - [阿尔弗莱德·沃尔登](../Page/阿尔弗莱德·沃尔登.md "wikilink") –
    [阿波罗15号](../Page/阿波罗15号.md "wikilink")

## 返回标本

[Lunar_Ferroan_Anorthosite_60025.jpg](https://zh.wikipedia.org/wiki/File:Lunar_Ferroan_Anorthosite_60025.jpg "fig:Lunar_Ferroan_Anorthosite_60025.jpg")[铁方解](../Page/铁方解石.md "wikilink")[钙长石](../Page/钙长石.md "wikilink")，编号60025。由[阿波罗16号的宇航员在](../Page/阿波罗16号.md "wikilink")[月表高地](../Page/月球地质学#高地和环形山.md "wikilink")[笛卡尔环形山附近找到](../Page/笛卡尔_\(环形山\).md "wikilink")。\]\]

| 月球任务                                   | 返回标本  |
| -------------------------------------- | ----- |
| [阿波罗11号](../Page/阿波罗11号.md "wikilink") | 22千克  |
| [阿波罗12号](../Page/阿波罗12号.md "wikilink") | 34千克  |
| [阿波罗14号](../Page/阿波罗14号.md "wikilink") | 43千克  |
| [阿波罗15号](../Page/阿波罗15号.md "wikilink") | 77千克  |
| [阿波罗16号](../Page/阿波罗16号.md "wikilink") | 95千克  |
| [阿波罗17号](../Page/阿波罗17号.md "wikilink") | 111千克 |
|                                        |       |

阿波罗计划的各次登月任务共带回了381.7千克的[月球岩石标本](../Page/月岩.md "wikilink")，大部分目前储存在[休斯敦](../Page/休斯敦.md "wikilink")[林顿·约翰逊太空中心的](../Page/林顿·约翰逊太空中心.md "wikilink")[月球物质回收和回归宇航员检疫实验所中](../Page/月球物质回收和回归宇航员检疫实验所.md "wikilink")。

通过[放射测年后](../Page/放射测年.md "wikilink")，研究人员发现月表岩石标本与地球相比都很古老。最年轻的月表岩石都比地球上已知的最古老岩石要久远。[月海中的](../Page/月海.md "wikilink")[玄武岩标本的年龄多在](../Page/玄武岩.md "wikilink")32亿年左右，高地中的标本甚至达到46亿年的年龄。由此可见，月球是在[太阳系早期形成的](../Page/太阳系.md "wikilink")。

[大卫·斯科特和](../Page/大卫·斯科特.md "wikilink")[詹姆斯·艾尔文在执行](../Page/詹姆斯·艾尔文.md "wikilink")[阿波罗15号任务时找到的](../Page/阿波罗15号.md "wikilink")[起源石是月球岩石标本中最重要的岩石之一](../Page/起源石.md "wikilink")，被认为是在月球诞生时形成的。

许多月球岩石似乎集中在[微流星形成的撞击环形山中](../Page/微流星.md "wikilink")；在地球上由于有较厚的大气层而不可能出现类似情景。

## 争议

近来，由于对[美苏争霸时期的问题的一系列揭秘](../Page/冷战.md "wikilink")，对其真实性开始有争议。随着不断的媒体报道和相关者披露内幕，不少人（据称美国就有约2000万）认为这和“[星球大战计划](../Page/星球大战计划.md "wikilink")”一样，是美国自导自演的一场世纪大骗局，而且还对其本身的可行性提出种种质疑。质疑登月真实性的人所宣称发现的疑点都是基于[NASA公布的月球照片](../Page/NASA.md "wikilink")。而相信登月真实性的人则尽可能合理地对这些疑点进行科学解释。目前，绝大多数人还是认为阿波罗计划是真实的，认为那些宣称“披露内幕”或妄图否定阿波罗登月计划的人，要么[带有经济、政治目的](../Page/阴谋论.md "wikilink")，要么就是宣扬[伪科学的伪科学主义者](../Page/伪科学.md "wikilink")。

## 其它

阿波罗计划的总投资为194.08亿美元（至1973年），折合2005年的1350亿美元。
1960-1973年，NASA总预算566.61亿美元。1960年至1973，阿波罗计划的年度预算和美国航天局的年度预算的最高峰都出现在1966年，1966年美国GDP是8150亿美元。当年阿波罗计划年度预算29.67亿美元，占美国GDP的0.4%；当年NASA年度预算45.12亿美元，占美国GDP的0.6%。[1](http://history.nasa.gov/SP-4029/Apollo_18-16_Apollo_Program_Budget_Appropriations.htm)

总重为381.7公斤的[月球矿石在阿波罗计划中被带回地球](../Page/月球岩石标本.md "wikilink")。现在绝大部分矿石保存在[休斯顿的](../Page/休斯顿.md "wikilink")[月球物质回收和回归宇航员检疫实验所](../Page/月球物质回收和回归宇航员检疫实验所.md "wikilink")。少数被[美国政府分配到全国各个实验室进行分析](../Page/美国.md "wikilink")，或作为礼物送给其他各国政府。

2018年美國NVIDIA公司使用「光影追跡技術 (Real-Time Ray Tracing)」，驗證人類真有登陸月球\[16\]。

## 参见

  - [针对阿波罗登月计划真实性的指责](../Page/针对阿波罗登月计划真实性的指责.md "wikilink")
  - [阿波罗登月舱](../Page/阿波罗登月舱.md "wikilink")
  - [阿波罗指揮/服务舱](../Page/阿波罗指揮/服务舱.md "wikilink")
  - [月球](../Page/月球.md "wikilink")
  - [土星5号](../Page/土星5号.md "wikilink")
  - [天空实验室](../Page/天空实验室.md "wikilink")
  - [被取消的阿波罗任务](../Page/被取消的阿波罗任务.md "wikilink")
  - [太空竞赛](../Page/太空竞赛.md "wikilink")
  - [失效模式与影响分析](../Page/失效模式与影响分析.md "wikilink")
  - [藍色彈珠](../Page/藍色彈珠.md "wikilink")
  - [阿波羅18號](../Page/阿波羅18號.md "wikilink")
  - [阿波罗计划图库](https://www.flickr.com/photos/projectapolloarchive/)

## 注释

## 外部链接

  - 英文

<!-- end list -->

  - [阿波罗计划官方网站](http://spaceflight.nasa.gov/history/apollo/index.html)
  - [图片视频系列](http://spaceflight.nasa.gov/gallery/images/apollo/index.html)
  - [阿波罗太空项目的历史](https://web.archive.org/web/20110414071137/http://spaceflighthistory.com/apolloprogram.htm)
  - [图片集](http://www.lpi.usra.edu/resources/apollo/) 将近25,000张月球图片
  - [阿波罗项目对社会的影响](http://www.sti.nasa.gov/tto/apollo.htm)

<!-- end list -->

  - 中文

<!-- end list -->

  - [背景资料：美国阿波罗登月计划简单回顾](https://web.archive.org/web/20041214074907/http://gb.chinabroadcast.cn/321/2004/01/15/221@44999.htm)
  - [“阿波罗登月”是骗局吗](http://tech.sina.com.cn/review/media/2000-08-25/34803.shtml)

[Category:美国国家航空航天局](../Category/美国国家航空航天局.md "wikilink")
[Category:载人航天计划](../Category/载人航天计划.md "wikilink")
[阿波罗计划](../Category/阿波罗计划.md "wikilink")
[Category:工程计划](../Category/工程计划.md "wikilink")

1.  Murray and Cox, *Apollo*, p. 55.

2.  Murray and Cox, *Apollo*, p. 60.

3.  Sidey, *John F. Kennedy*, pp. 117-118.

4.  Beschloss, 'Kennedy and the Decision to Go to the Moon,' p. 55.

5.  "Discussion of Soviet Man-in-Space Shot," Hearing before the
    Committee on Science and Astronautics, U.S. House of
    Representatives, 87th Congress, First Session, April 13, 1961.

6.  Sidey, *John F. Kennedy*, p. 114

7.  Kennedy to Johnson, "Memorandum for Vice President," April 20, 1961.

8.  原文："We are neither making maximum effort nor achieving results
    necessary if this country is to reach a position of leadership."

9.  Johnson to Kennedy, "Evaluation of Space Program," April 21, 1961.

10.
11. Murray and Cox, *Apollo*, pp. 16-17.

12. 原文："deficiencies existed in Command Module design, workmanship and
    quality control."

13. \[Report of the Apollo 204 Review Board, Findings and
    Recommendations
    <http://www.hq.nasa.gov/office/pao/History/Apollo204/find.html>\]

14. \[Chariots for Apollo, Ch 7-4
    <http://www.hq.nasa.gov/office/pao/History/SP-4205/ch7-4.html>\]

15. 康拉德身高1.69米

16.