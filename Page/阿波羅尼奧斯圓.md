[Apollonian_circles.svg](https://zh.wikipedia.org/wiki/File:Apollonian_circles.svg "fig:Apollonian_circles.svg")

**阿波羅尼奧斯圓**是兩個相關的圓族。第一個圓族的每一個藍色圓與第二個圓族的每一個紅色圓相互[正交](../Page/正交.md "wikilink")。這些圓構成了[雙極坐標系的](../Page/雙極坐標系.md "wikilink")[基](../Page/基_\(線性代數\).md "wikilink")。阿波羅尼奧斯圓是[希臘](../Page/希臘.md "wikilink")[數學家](../Page/數學家.md "wikilink")[阿波羅尼奧斯](../Page/阿波羅尼奧斯.md "wikilink")
([古希腊语](../Page/古希腊语.md "wikilink")：**) 發現的。

## 定義

阿波羅尼奧斯圓是[線段定義的](../Page/線段.md "wikilink")，標記此線段為 \(\overline{CD}\,\!\) 。

### 第一族（藍色圓）

第一族中的每一個都由一個正[實數](../Page/實數.md "wikilink") *r* 確定，這些圓定義為滿足下列條件的點 *X*
的[軌跡](../Page/軌跡.md "wikilink")：

\[\left\{X\mid \frac{d(X,C)}{d(X,D)} = r\right\}.\] 即 *X* 到 *C* 的距離與 *X*
到 *D* 的距離之比值為 *r*.

當 *r* 很接近零時，相應的圓會靠近 *C* 的一側，而對接近 ∞ 的 *r*, 相應的圓則靠近 *D* 的一側。至於當*r* = 1
時，該圓會退化為線段 *CD* 之[中垂線](../Page/中垂線.md "wikilink")。

### 第二族（紅色圓）

第二族中的每個圓都由角 *θ* 確定， 這些圓定義為滿足下列條件的點 *X* 的軌跡：

\[\left\{X\mid \; \measuredangle CXD \; = \theta\right\}.\]
其中\(\measuredangle CXD\) 表示 *CXD* 的有向角。

當 *θ* 取遍 0 到 *π* 之所有值時，上式生成所有經過 *C* 和 *D* 的圓。

## 性質

不一樣圓圈的固定比例 \(k\,\!\)
必不一樣。圓與圓之間互不[同心](../Page/同心_\(幾何\).md "wikilink")，互不相交。

每一個藍圓與每一個紅圓以直角相交，可以簡易地解釋如下：關於一個圓心為點 C 的圓 Q
，一族的藍阿波羅尼奧斯圓的[反演像](../Page/反演.md "wikilink")，形成了一組同心圓，其圓心在點
D' 。點 D 關於圓 Q 的[反演是點](../Page/反演.md "wikilink") D' 。同樣的變換把一族的紅圓反演為一組從點
D'
放射出來的直線。這樣，反演將[雙極坐標變換為](../Page/雙極坐標系.md "wikilink")[極坐標](../Page/極坐標系.md "wikilink")。在極坐標裏，每一條徑向線與
圓心為原點的圓圈
以直角相交。由於反演是一個[共形變換](../Page/共形映射.md "wikilink")，所以，每一個藍圓圈與每一個紅圓圈以直角相交。

## 參閱

  - [反演](../Page/反演.md "wikilink")

## 參考文獻

  - C. Stanley Ogilvy (1990), *Excursions in Geometry*, Dover. ISBN
    0-486-26530-7。

[A](../Category/圓.md "wikilink") [A](../Category/初等幾何.md "wikilink")
[A](../Category/平面幾何.md "wikilink")