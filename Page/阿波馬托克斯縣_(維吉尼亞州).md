**阿波馬托克斯縣**（**Appomattox County,
Virginia**）是[美國](../Page/美國.md "wikilink")[維吉尼亞州中南部的一個縣](../Page/維吉尼亞州.md "wikilink")。面積867平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口13,705人。縣治[阿波馬托克斯](../Page/阿波馬托克斯.md "wikilink")（Appomattox）。

成立於1845年2月8日。縣名來自[阿波馬托克斯河](../Page/阿波馬托克斯河.md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[A](../Category/維吉尼亞州縣.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.