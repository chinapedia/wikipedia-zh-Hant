**阿灵顿国家公墓**（），位于[美國](../Page/美國.md "wikilink")[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")[阿靈頓](../Page/阿靈頓縣_\(維吉尼亞州\).md "wikilink")，美國國防部[五角大楼旁边](../Page/五角大楼.md "wikilink")，1864年6月15日开始作为军人[公墓使用](../Page/公墓.md "wikilink")。

佔地624 英亩（2.53
平方公里），在美國[南北戰爭期間建立](../Page/南北戰爭.md "wikilink")，由[阿靈頓宮周邊的土地劃出來](../Page/阿靈頓宮.md "wikilink")。在其中安葬了美國國家戰爭中犧牲的美國軍人，包括[南北戰爭](../Page/南北戰爭.md "wikilink")、[韓戰](../Page/韓戰.md "wikilink")、[越戰以至於最近的](../Page/越戰.md "wikilink")[伊拉克戰爭與](../Page/伊拉克戰爭.md "wikilink")[阿富汗戰爭](../Page/阿富汗戰爭.md "wikilink")。

它臨近[波多馬克河](../Page/波多馬克河.md "wikilink")，在河對岸，就是[林肯紀念堂](../Page/林肯紀念堂.md "wikilink")。搭乘[華盛頓地鐵](../Page/華盛頓地鐵.md "wikilink")，在[阿靈頓公墓站](../Page/阿靈頓公墓站.md "wikilink")（Arlington
Cemetery）下車就可以到達。它是[美國陸軍部直接管轄的兩座國家級公墓之一](../Page/美國陸軍部.md "wikilink")。[無名戰士墓](../Page/無名戰士墓.md "wikilink")（Tomb
of the Unknowns）位於阿靈頓國家公墓之中。

## 图集

[阿靈頓國家公墓](../Category/阿靈頓國家公墓.md "wikilink")
[Category:美國國家公墓](../Category/美國國家公墓.md "wikilink")
[Category:軍人墓園](../Category/軍人墓園.md "wikilink")