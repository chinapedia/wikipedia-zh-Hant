[AltaRockCarvingsMuseum.jpg](https://zh.wikipedia.org/wiki/File:AltaRockCarvingsMuseum.jpg "fig:AltaRockCarvingsMuseum.jpg")
**阿爾塔博物館**位於[挪威北部北極圈內](../Page/挪威.md "wikilink")[芬馬克郡](../Page/芬馬克郡.md "wikilink")[阿爾塔](../Page/阿尔塔_\(挪威\).md "wikilink")，是挪威最多訪客的夏季博物館。每日多達1000人次。\[1\]該館的展覽展示當地文化及歷史工業包括了鄰近地方的史前[阿爾塔岩畫](../Page/阿爾塔岩畫.md "wikilink")。阿爾塔岩畫是世界遺產遺跡之一，是[芬馬克郡的早期部落](../Page/芬馬克郡.md "wikilink")，距今大約11000年。\[2\]\[3\]阿爾塔博物館於1991年6月正式開放，並在1999年獲得[歐洲年度博物館](../Page/歐洲年度博物館.md "wikilink")。\[4\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [阿爾塔博物館網站](https://web.archive.org/web/20060424043823/http://www.alta.museum.no/)

[Category:歷史博物館](../Category/歷史博物館.md "wikilink")
[Category:挪威博物馆](../Category/挪威博物馆.md "wikilink")
[Category:芬马克郡](../Category/芬马克郡.md "wikilink")
[Category:欧洲年度博物馆奖获得者](../Category/欧洲年度博物馆奖获得者.md "wikilink")

1.

2.

3.

4.