**列克**（[阿爾巴尼亞語](../Page/阿爾巴尼亞語.md "wikilink")：）是[阿爾巴尼亞的流通貨幣](../Page/阿爾巴尼亞.md "wikilink")。輔幣單位昆塔，1列克=100昆塔。[5000_lek_obverse.jpg](https://zh.wikipedia.org/wiki/File:5000_lek_obverse.jpg "fig:5000_lek_obverse.jpg")

## 流通中的貨幣

### 硬幣

[缩略图](https://zh.wikipedia.org/wiki/File:Albania-lek-coins.jpg "fig:缩略图")

  - 1 列克
      - 正面：[鵜鶘](../Page/鵜鶘.md "wikilink")、年份
      - 背面：面值、稻草
  - 5 列克
      - 正面：[阿爾巴尼亞國旗](../Page/阿爾巴尼亞國旗.md "wikilink")、年份
      - 背面：面值、稻草
  - 10 列克
      - 正面：[培拉特城堡](../Page/培拉特.md "wikilink")、年份
      - 背面：面值、稻草
  - 20 列克
      - 正面：一個古老的[部落的](../Page/部落.md "wikilink")[船隻](../Page/船.md "wikilink")、年份
      - 背面：面值、稻草
  - 50 列克
      - 正面：[伊利里亞國王的肖像](../Page/伊利里亞.md "wikilink")、年份
      - 背面：面值、稻草
  - 100 列克
      - 正面：[伊利里亞攝政王后的肖像](../Page/伊利里亞.md "wikilink")、年份
      - 背面：面值、稻草

### 紙幣

#### 1996年

[缩略图](https://zh.wikipedia.org/wiki/File:Albanian_lek_2007.jpg "fig:缩略图")

  - 100 列克（只在 1996 年印製）
      - 正面：第 14 任阿爾巴尼亞總理 範斯·諾利
      - 背面：首楝阿爾巴尼亞議會大樓
  - 200 列克
      - 正面：阿爾巴尼亞[詩人和](../Page/詩人.md "wikilink")[作家納伊姆](../Page/作家.md "wikilink")·法西尼
      - 背面：納伊姆·法西尼的出生地
  - 500 列克
      - 正面：阿爾巴尼亞第1任總理[伊斯梅爾·捷馬利](../Page/伊斯梅爾·捷馬利.md "wikilink")
      - 背面：位於[夫羅勒的獨立建築物](../Page/夫羅勒.md "wikilink")
  - 1,000 列克
      - 正面：阿爾巴尼亞作家彼得·博格達尼
      - 背面：位於德雅勒沃的[教堂](../Page/教堂.md "wikilink")
  - 2,000 列克
      - 正面：名為 Gentius 的古[錢幣圖樣](../Page/錢幣.md "wikilink")
      - 背面：位於[布特林特的露天劇場](../Page/布特林特.md "wikilink")
  - 5,000 列克
      - 正面：阿爾巴尼亞領主[斯坎德培](../Page/斯坎德培.md "wikilink")
      - 背面：[克魯亞](../Page/克魯亞.md "wikilink")[城堡](../Page/城堡.md "wikilink")

#### 1992年

[缩略图](https://zh.wikipedia.org/wiki/File:Albanian_lek_banknote_set_1992.jpg "fig:缩略图")

  - 100 列克
      - 正面：國家戰士
      - 背面：[老鷹](../Page/老鷹.md "wikilink")、[高山](../Page/高山.md "wikilink")
  - 200 列克
      - 正面：[伊斯梅尔·捷马利](../Page/伊斯梅尔·捷马利.md "wikilink")
      - 背面：[阿尔巴尼亚国徽](../Page/阿尔巴尼亚国徽.md "wikilink")、宣布阿爾巴尼亞獨立
  - 500 列克
      - 正面：納伊姆·法西利
      - 背面：納伊姆·法西利的[詩](../Page/詩.md "wikilink")
  - 1,000 列克
      - 正面：[斯坎德培](../Page/斯坎德培.md "wikilink")
      - 背面：[克魯亞城堡](../Page/克魯亞.md "wikilink")

#### 1991年

[缩略图](https://zh.wikipedia.org/wiki/File:Albanian_lek_banknote_set_1991.jpg "fig:缩略图")

  - 100 列克
      - 正面：[煉油廠](../Page/煉油廠.md "wikilink")
      - 背面：[油井](../Page/油井.md "wikilink")，[工人](../Page/工人.md "wikilink")，[鋼鐵廠](../Page/鋼鐵.md "wikilink")
  - 500 列克
      - 正面：女孩、[向日葵](../Page/向日葵.md "wikilink")
      - 背面：高[山](../Page/山.md "wikilink")

## 匯率

## 外部連結

  - [阿爾巴尼亞列克介紹頁面](https://papermoneyworld.wordpress.com/2015/06/21/albania-%E9%98%BF%E7%88%BE%E5%B7%B4%E5%B0%BC%E4%BA%9E%E5%88%97%E5%85%8B%E7%B4%99%E5%B9%A3/)
  - [阿尔巴尼亚的历史及当前钞](http://www.bis-ans-ende-der-welt.net/Albanien-B-En.htm)


[Category:阿爾巴尼亞](../Category/阿爾巴尼亞.md "wikilink")