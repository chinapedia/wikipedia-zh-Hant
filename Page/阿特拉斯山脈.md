**阿特拉斯山脈**（；），或譯**亞特拉斯山脈**，是[地中海與](../Page/地中海.md "wikilink")[撒哈拉沙漠之間的](../Page/撒哈拉沙漠.md "wikilink")[山脈](../Page/山脈.md "wikilink")，位於[非洲西北部](../Page/非洲.md "wikilink")，長2,400公里，橫跨[摩洛哥](../Page/摩洛哥.md "wikilink")、[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")、[突尼西亞三國](../Page/突尼西亞.md "wikilink")，把地中海西南岸與撒哈拉沙漠分開。

最高峰為[圖卜卡勒峰](../Page/圖卜卡勒峰.md "wikilink")（Jbel
Toubkal，海拔4,167公尺），位於摩洛哥西南部（31°03′43″N,
7°54′58″W）。

當地居民主要為摩洛哥的[柏柏爾人和阿爾及利亞的](../Page/柏柏爾人.md "wikilink")[阿拉伯人](../Page/阿拉伯人.md "wikilink")。

## 地質歷史

在遠古時代，由於[歐洲](../Page/歐洲.md "wikilink")、[非洲和](../Page/非洲.md "wikilink")[北美洲相連](../Page/北美洲.md "wikilink")，阿特拉斯山脈在地質上是[阿帕拉契造山運動的一部分](../Page/阿利根尼造山幕.md "wikilink")。山脈在非洲和北美洲相撞時形成，當時遠比今日的[喜马拉雅山脈要高](../Page/喜马拉雅山脈.md "wikilink")。今日，這山脈的痕跡仍然可以在[美國](../Page/美國.md "wikilink")[東部的](../Page/美國東部.md "wikilink")[陡降線上或者在](../Page/陡降線.md "wikilink")[山脈看到](../Page/山脈.md "wikilink")。[西班牙南部的](../Page/西班牙.md "wikilink")[內華達山脈也是在同一次運動中形成的](../Page/内华达山脉_\(西班牙\).md "wikilink")。

阿特拉斯山脈許多高峰上留存著[冰期的遺跡](../Page/冰期.md "wikilink")，有明顯的[冰斗](../Page/冰斗.md "wikilink")，與冰河邊緣的地形特徵。\[1\]

[Atlasgebirge_topografisch_politsch_mittel_mit_Hochland.jpg](https://zh.wikipedia.org/wiki/File:Atlasgebirge_topografisch_politsch_mittel_mit_Hochland.jpg "fig:Atlasgebirge_topografisch_politsch_mittel_mit_Hochland.jpg")

## 地理構成

阿特拉斯山並未形成一道連續的山脈鏈，而是一連串散佈在高原、盆地與峽谷之間的山脈最突出的是[高阿特拉斯山](../Page/高阿特拉斯山脈.md "wikilink")，從摩洛哥西岸的[亞加的爾綿延](../Page/亞加的爾.md "wikilink")650公里長，包含了許多終年積雪的高峰，其中最高峰為[托布卡爾山](../Page/托布卡爾山.md "wikilink")。阿特拉斯山脈的東面是高度較緩的高原。北面的山坡雨水充沛，森林以[雪松](../Page/雪松.md "wikilink")、[柏](../Page/柏.md "wikilink")、[櫟樹為主](../Page/櫟樹.md "wikilink")。

分為摩洛哥的[大阿特拉斯山](../Page/大阿特拉斯山脈.md "wikilink")（[中阿特拉斯山](../Page/中阿特拉斯山脈.md "wikilink")、[高阿特拉斯山和](../Page/高阿特拉斯山脈.md "wikilink")[小阿特拉斯山](../Page/小阿特拉斯山.md "wikilink")（Anti-Atlas）），沿海較矮的[得爾阿特拉斯山](../Page/得爾阿特拉斯山.md "wikilink")（Tell
Atlas）和深入南方、較高的[撒哈拉阿特拉斯山](../Page/撒哈拉阿特拉斯山.md "wikilink")（Saharan
Atlas）。後兩者在阿爾及利亞。

## 參考文獻

[Category:非洲跨国山脉](../Category/非洲跨国山脉.md "wikilink")
[Category:突尼斯山脈](../Category/突尼斯山脈.md "wikilink")
[Category:阿爾及利亞山脈](../Category/阿爾及利亞山脈.md "wikilink")
[Category:摩洛哥山脈](../Category/摩洛哥山脈.md "wikilink")

1.