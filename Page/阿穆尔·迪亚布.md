**阿穆尔·迪亚布(Amr Abdel Basset Abdel Azeez Diab)**
([阿拉伯语](../Page/阿拉伯语.md "wikilink"):عمرو دياب) (出生于
1961年10月11日)，是埃及和阿拉伯世界的著名流行歌手。他的著名作品包括 "Nour El Ein," "Leily Nahari,"
"Wala Ala Balo,"等。

## 早年经历

母亲是法语教师，父亲是高级财务经理。迪亚布6岁便开始登台演出。

## 唱片列表

  - [El Leila De](../Page/El_Leila_De.md "wikilink") (今夜) (2007)
  - [Kammel Kalamak](../Page/Kammel_Kalamak.md "wikilink") (把话说完) (2005)
  - [Leily Nahary](../Page/Leily_Nahary.md "wikilink") (我的日日夜夜) (2004)
  - [Allem Alby](../Page/Allem_Alby.md "wikilink") (教导我的心) (2003)
  - [Aktar Wahed](../Page/Aktar_Wahed.md "wikilink") (最重要的人) (2001)
  - [Tamally Maak](../Page/Tamally_Maak.md "wikilink") (永远和你在一起) (2000)
  - [Amarein](../Page/Amarein.md "wikilink") (两个月亮) (1999)
  - [Awedoony](../Page/Awedoony.md "wikilink") (渐成习惯) (1998)
  - [Nour El-Ein](../Page/Nour_El-Ein.md "wikilink") (目光) (1996)
  - [Ragaeen](../Page/Ragaeen.md "wikilink") (去去就回) (1995)
  - [Zekrayat](../Page/Zekrayat.md "wikilink") (回忆) (1994)
  - [W Ylomoony](../Page/W_Ylomoony.md "wikilink") (都怪我) (1994)
  - [Ya Omrena](../Page/Ya_Omrena.md "wikilink") (我们的生活) (1993)
  - [Ice Cream Fi Gleam](../Page/Ice_Cream_Fi_Gleam.md "wikilink")
    (冰淇淋In Gleam) (1992)
  - [Ayamna](../Page/Ayamna.md "wikilink") (我们的日子) (1992)
  - [Habiby](../Page/Habiby_\(Album\).md "wikilink") (亲爱的) (1991)
  - [Matkhafeesh](../Page/Matkhafeesh.md "wikilink") (请别担心) (1990)
  - [Shawaana](../Page/Shawaana.md "wikilink") (好想你) (1989)
  - [Mayyal](../Page/Mayyal.md "wikilink") (坠入爱河) (1988)
  - [Ya Helwa](../Page/Ya_Helwa.md "wikilink") (嗨，宝贝) (1988)
  - [Khalseen](../Page/Khalseen.md "wikilink") (我们分手吧) (1987)
  - [Hala Hala](../Page/Hala_Hala.md "wikilink")\] (欢迎欢迎) (1986)
  - [Ghanny Men Albak](../Page/Ghanny_Men_Albak.md "wikilink") (心底的歌)
    (1984)
  - [Ya Tarek](../Page/Ya_Tarek.md "wikilink") (一路上) (1983)

## 单曲

  - "Allah Aleeha"
  - "Ana Mahma Kbert"
  - "Steen Million Fedai'" ( 写给埃及总统[穆巴拉克](../Page/穆巴拉克.md "wikilink"))
  - "El Qods Di Ardena"

## 外部链接

  - [Amr Diab Non-Stop](http://www.amrdiab.net)
  - [Amr Diab World](http://www.amrdiabworld.com)
  - [Amrdiab.info - The First Fan Site](http://www.amrdiab.info)
    (usually has exclusive news)
  - [All For Amr Diab - Over 25 Years of Legendary
    Success](https://web.archive.org/web/20090122070739/http://www.allforamrdiab.com/)
    [Amr Diab in
    *musique.arabe*](http://musique.arabe.over-blog.com/article-14660487.html)

[Category:1961年出生](../Category/1961年出生.md "wikilink")
[Category:埃及歌手](../Category/埃及歌手.md "wikilink")
[Category:流行音樂歌手](../Category/流行音樂歌手.md "wikilink")