**阿納拜斯龍屬**（[學名](../Page/學名.md "wikilink")：*Anabisetia*）是[禽龍類](../Page/禽龍類.md "wikilink")[恐龍的一個](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生活於[白堊紀的](../Page/白堊紀.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")[巴塔哥尼亞](../Page/巴塔哥尼亞.md "wikilink")。牠是雙足的[草食性恐龍](../Page/草食性.md "wikilink")，約有2公尺長。

## 命名

[阿根廷](../Page/阿根廷.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[羅多爾夫·科里亞](../Page/羅多爾夫·科里亞.md "wikilink")（Rodolfo
Coria）及Jorge
Calvo於2002年為阿納拜斯龍命名。屬名是為紀念一位已逝的[考古學家Ana](../Page/考古學家.md "wikilink")
Biset而設的，她是於阿根廷的[內烏肯省出生](../Page/內烏肯省.md "wikilink")，也是此恐龍被發現的地方。唯一被命名[種稱為](../Page/種.md "wikilink")**薩氏阿那拜斯龍**（*A.
saldiviai*），種名是為紀念在1985年發現[化石的當地](../Page/化石.md "wikilink")[農夫Roberto](../Page/農夫.md "wikilink")
Saldivia，他在1993年將化石交給古生物學家\[1\]。在1996年，首次有科學文獻提到這個重要化石\[2\]\[3\]。

## 化石

[Anabisetia.jpg](https://zh.wikipedia.org/wiki/File:Anabisetia.jpg "fig:Anabisetia.jpg")
阿納拜斯龍現存有四個標本，所有都在2002年的研究中被提及。[正模標本](../Page/正模標本.md "wikilink")（編號MCF-PVPH
74）是四個當中最為完整的，它是一些[頭顱骨碎片](../Page/頭顱骨.md "wikilink")，包括部份腦殼及兩塊[齒骨](../Page/齒骨.md "wikilink")、一個由肩膀至手掌的完整前肢、一個完整的後肢連腳掌、及整段[脊椎](../Page/脊椎.md "wikilink")。其餘三個標本都是較不完整，但包括了正模標本沒有的部份，包括更多的脊椎、一個完整的[骨盆及一個接近完整的尾巴](../Page/骨盆.md "wikilink")。編號MCF-PVPH-75、76這兩個標本被選為[副模標本](../Page/副模標本.md "wikilink")，第4個標本（編號MCF-PVPH-77）也被歸類於阿納拜斯龍。若將四個標本整合來看，除了頭顱骨有缺少，整個骨骼都算為完整。這些標本都是存放在[阿根廷的](../Page/阿根廷.md "wikilink")[卡門菲耐斯市立博物館中](../Page/卡門菲耐斯市立博物館.md "wikilink")\[4\]。

所有四個標本都是從[內烏肯省Plaza](../Page/內烏肯省.md "wikilink") Huincul南部的Cerro Bayo
Mesa中被發現。這個地方是屬於Cerro
Lisandro地層，屬於[內烏肯組的利邁河亞組](../Page/內烏肯組.md "wikilink")。這個地層的沉積物中保存了一個[沼澤](../Page/沼澤.md "wikilink")，被認為是在[上白堊紀的](../Page/白堊紀.md "wikilink")[森諾曼階末期至](../Page/森諾曼階.md "wikilink")[土侖階早期](../Page/土侖階.md "wikilink")，約為9500萬到9200萬年前\[5\]。

## 敘述

阿納拜斯龍是種小型二足[草食性恐龍](../Page/草食性.md "wikilink")。在2010年，[葛瑞格利·保羅](../Page/葛瑞格利·保羅.md "wikilink")（Gregory
S. Paul）估計其身長約2公尺，體重約20公斤\[6\]。科里亞等人列出阿納拜斯龍的數項獨有特徵。頭顱骨後方的枕骨髁（Occipital
condyle）比較朝下。[肩胛骨的肩鋒部分有個突出部分](../Page/肩胛骨.md "wikilink")，突出部分跟肩胛骨的比例是[真鳥腳類之中最大的](../Page/真鳥腳類.md "wikilink")。第五[掌骨平坦](../Page/掌骨.md "wikilink")，邊緣筆直，橫剖面不呈圓形。[腸骨的前端與其他](../Page/腸骨.md "wikilink")[骨盆連接部分](../Page/骨盆.md "wikilink")，佔了腸骨長度的一半以上。[坐骨前端支幹的橫剖面呈三角形](../Page/坐骨.md "wikilink")，後端支幹呈四角形。[腓骨末端接觸到](../Page/腓骨.md "wikilink")[距骨](../Page/距骨.md "wikilink")\[7\]。

## 分類

阿納拜斯龍被認為是[加斯帕里尼龍的近親](../Page/加斯帕里尼龍.md "wikilink")，加斯帕里尼龍是另一種[巴塔哥尼亞的](../Page/巴塔哥尼亞.md "wikilink")[鳥腳下目](../Page/鳥腳下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，但是由於缺乏[頭顱骨而很難準確的分類](../Page/頭顱骨.md "wikilink")。當最初被描述時，加斯帕里尼龍及阿納拜斯龍都被認為是基礎[禽龍類](../Page/禽龍類.md "wikilink")，比[腱龍更為進化](../Page/腱龍.md "wikilink")。但是[分支系統學分析卻指出加斯帕里尼龍其實不屬於禽龍類](../Page/分支系統學.md "wikilink")，是[北美洲鳥腳下目恐龍的近親](../Page/北美洲.md "wikilink")，如[奇異龍及](../Page/奇異龍.md "wikilink")[帕克氏龍](../Page/帕克氏龍.md "wikilink")\[8\]。阿納拜斯龍可能會是類似的分類位置。

## 參考資料

<references />

## 外部連結

  - [阿納拜斯龍的圖畫](http://dino.lm.com/images/display.php?id=871)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:禽龍類](../Category/禽龍類.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")

1.  Coria, R.A. & Calvo, J.O. 2002. A new iguanodontian ornithopod from
    Neuquen Basin, Patagonia, Argentina. *Journal of Vertebrate
    Paleontology*. 22(3): 503–509.

2.  R. A. Coria, G. Cladero, & L. Salgado, 1996, "Una neuva localidad
    fosilífera en la Formación Río Limay?, Cretácico Superior, Cerro
    Bayo Mesa, Provincia de Neuquén", *Ameghiniana* **33**(4): 463

3.  Coria, R.A. & J. O. Calvo, 1996, "Análisis filogenético preliminar
    del primer dinosaurio Iguanodontia registrado en la Formación Río
    Limay", *Resúmenes XII Jornadas Argentinas de Paleontología de
    Vertebrados. Ameghiniana* **33**: 462

4.
5.  Leanza, H.A., Apesteguia, S., Novas, F.E., & de la Fuente, M.S.
    2004. Cretaceous terrestrial beds from the Neuquén Basin (Argentina)
    and their tetrapod assemblages. *Cretaceous Research* 25(1): 61-87.

6.  Paul, G.S., 2010, *The Princeton Field Guide to Dinosaurs*,
    Princeton University Press p. 277

7.
8.  Norman, D.B., Sues, H-D., Witmer, L.M., & Coria, R.A. 2004. Basal
    Ornithopoda. In: Weishampel, D.A., Dodson, P., & Osmolska, H.
    (Eds.). *The Dinosauria* (2nd Edition). Berkeley: University of
    California Press. Pp. 393-412.