[India_Awadh_locator_map.svg](https://zh.wikipedia.org/wiki/File:India_Awadh_locator_map.svg "fig:India_Awadh_locator_map.svg")
**阿约提亚**（）
为[印度古城](../Page/印度.md "wikilink")，位于[北方邦](../Page/北方邦.md "wikilink")[法扎巴德县境内](../Page/法扎巴德县.md "wikilink")。阿约提亚城地处[萨拉育河右岸](../Page/萨拉育河.md "wikilink")、[新德里以东](../Page/新德里.md "wikilink")555公里。被认为是[印度教神祇](../Page/印度教.md "wikilink")[罗摩的出生地](../Page/罗摩.md "wikilink")。阿约提亚的字面意译是“不可夺取、不可战胜”的意思。

## 历史

据史料记载，印度的始祖与[摩奴法典作者](../Page/摩奴法典.md "wikilink")[摩奴是此城的创始人](../Page/摩奴.md "wikilink")。阿约提亚曾为[拘萨罗王国首都](../Page/拘萨罗.md "wikilink")。此城被印度教徒视为圣城，因为他们认为此城曾是《[罗摩衍那](../Page/罗摩衍那.md "wikilink")》中的英雄罗摩王国的都城。

在[佛教史料记载](../Page/佛教.md "wikilink")“阿踰陀”（）\[1\]城北四五里外是“沙祇大”（)\[2\]，公元七世纪时，[玄奘](../Page/玄奘.md "wikilink")[三藏曾到此城参访](../Page/三藏.md "wikilink")，当时这里有百餘所佛教寺院，三千名僧人，有十所[印度教寺廟信眾不多](../Page/印度教.md "wikilink")。並記載[无著菩萨就是在阿约提亚听](../Page/无著菩萨.md "wikilink")[弥勒菩萨说法](../Page/弥勒菩萨.md "wikilink")，并将此内容写下来，这就是著名的[弥勒五论](../Page/弥勒五论.md "wikilink")，玄奘提及了其中三論《[瑜伽師地論](../Page/瑜伽師地論.md "wikilink")》、《[大乘莊嚴經論頌](../Page/大乘莊嚴經論.md "wikilink")》和《[中邊分別論頌](../Page/辨中邊論.md "wikilink")》\[3\]。有人依據漢語古籍記載將“沙奇”\[4\]、“沙祇”或“沙祇大”也認定為阿約提亞。

在19世纪末，此城有96座印度教庙宇、36座清真寺。也是在此圣城，诗人Tulsi
Das写作了著名的罗摩史诗。在1990年代，阿约提亚成为印度教徒与穆斯林部落冲突的中心，1992年12月6日，印度教徒摧毁了建于1528年的[巴布爾清真寺](../Page/巴布爾清真寺.md "wikilink")（Babri
Masjid）
。因为据说它正好建在了罗摩神出生的地方，这里曾有一座被穆斯林破坏了的印度神庙。为了避免宗教冲突的升级，印度政府收购了这块地盘。而后在此地进行的考古发掘证实了罗摩崇拜者的说法，印度教徒们试图在此地重建新庙，准备工作正在陆续进行中。2003年8月印度考古探测（Archaeological
Survey of
India）报告称，在有争议的建筑（清真寺）下面，确实有大型的建筑遗迹，一对印度天神的残缺塑像在此出土，此外还发现了一些印度教古典象征---荷花型的装饰物等。

## 其他

[泰国城市](../Page/泰国.md "wikilink")[阿瑜陀耶及](../Page/阿瑜陀耶.md "wikilink")[印尼城市](../Page/印尼.md "wikilink")[日惹的名字都来源于阿约提亚](../Page/日惹.md "wikilink")。這地方也是倳説中[金首露妻子](../Page/金首露.md "wikilink")[許黄玉出生地](../Page/許黄玉.md "wikilink")。

## 註釋與引用

## 外部链接

  -
  - [Ayodhya the spiritual abode. Complete details of Ayodhya, proofs
    and documents.](http://www.ayodhya.com)

  - [Sri Ramar Temple,
    Ayodhya](http://www.divyadesam.com/hindu/temples/delhi/ayodhya-temple.shtml)

  - [Ayodhya](https://web.archive.org/web/20101017213315/http://www.jainheritagecentres.com/uttarpradesh/ayodhya.htm)
    at Jainheritagecentres.com.

  - [Ayojjhaa](http://www.palikanon.com/english/pali_names/ay/ayojjhaa.htm)
    in the Buddhist Dictionary of Pali Proper Names.

  - [Pilgrim Centers Of
    India](http://www.bharatadesam.com/places/ayodhya.php)

  - [Ayodhya map](http://www.shaktipeethas.org/ayodhya-map-t96.html)

  - [CNN Article on Ayodhya Conflict,
    Dec 6 2002](http://edition.cnn.com/2002/WORLD/asiapcf/south/12/06/ayodhya.background/)

[Category:北方邦城镇](../Category/北方邦城镇.md "wikilink")

1.  [玄奘](../Page/玄奘.md "wikilink")《[大唐西域記](../Page/大唐西域記.md "wikilink")》：「阿踰陀國。周五千餘里。國大都城周二十餘里。穀稼豐盛華菓繁茂。氣序和暢風俗善順。好營福勤學藝。伽藍百有餘所。僧徒三千餘人。大乘小乘兼功習學。天祠十所。異道寡少。大城中有故伽藍。是伐蘇畔度菩薩(唐言世親。舊曰婆藪盤豆譯曰天親訛謬也)數十年中於此製作大小乘諸異論。其側故基。是世親菩薩。為諸國王四方俊彥沙門婆羅門等。講義說法堂也。**城北四五里**。臨殑伽河岸大伽藍中有窣堵波。高二百餘尺。無憂王之所建也。是如來為天人眾於此三月說諸妙法。其側窣堵波。**過去四佛坐及經行遺迹之所**。伽藍西四五里有如來髮爪窣堵波。髮爪窣堵波北伽藍餘趾。昔經部室利邏多(唐言勝受)論師。於此製造。經部毘婆沙論。城西南五六里。大菴沒羅林中有故伽藍。是阿僧伽(唐言無著)菩薩請益導凡之處。……無著講堂故基西北四十餘里至故伽藍。北臨殑伽河。中有塼窣堵波。高百餘尺。世親菩薩初發大乘心處。」
2.  [法顯](../Page/法顯.md "wikilink")《[高僧法顯傳](../Page/高僧法顯傳.md "wikilink")》：「到沙祇大國。出沙祇城。**南門道東**。佛本在此嚼楊枝已刺土中。即生長七尺。不增不減。諸外道婆羅門嫉妬或斫或拔遠棄之。其處續生如故。此中亦有**四佛經行坐處**。起塔故在。」
3.  [玄奘](../Page/玄奘.md "wikilink")《[大唐西域記](../Page/大唐西域記.md "wikilink")》：「無著菩薩夜昇天宮。於慈氏菩薩所受。瑜伽師地論、莊嚴大乘經論、中邊分別論等。晝為大眾講宣妙理。」
4.  《[後漢書](../Page/後漢書.md "wikilink")·西域傳》：「東離國居沙奇城，在天竺東南三千餘里，大國也。其土氣、物類與天竺同。列城數十，皆稱王。大月氏伐之，遂臣服焉。男女皆長八尺，而怯弱。乘象、駱駝，往來鄰國。有寇，乘象以戰。」