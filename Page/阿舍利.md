[Biface_de_St_Acheul_MHNT.jpg](https://zh.wikipedia.org/wiki/File:Biface_de_St_Acheul_MHNT.jpg "fig:Biface_de_St_Acheul_MHNT.jpg")
**阿舍利**（英語：**Acheulean**或**Acheulian**）文化是[考古學上對於一類史前](../Page/考古學.md "wikilink")[人族](../Page/人族.md "wikilink")（Hominini）[石器](../Page/石器.md "wikilink")[工藝技術的稱呼](../Page/工藝.md "wikilink")。此文化橫跨於[舊石器時代早期的](../Page/舊石器時代.md "wikilink")[非洲](../Page/非洲.md "wikilink")、[亞洲及](../Page/亞洲.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")。

阿舍利文化是旧石器文化中的一个阶段，距今170万年至20万年间，因最早发现于[法国](../Page/法国.md "wikilink")[亚眠市郊的](../Page/亚眠.md "wikilink")[圣阿舍尔而得名](../Page/圣阿舍尔.md "wikilink")。它是左右对称的石器，多类型组合，例如：手斧、手镐、薄刃斧、砍砸器、大型石刀等。它的出现表明当时的人们已经具备了生产标准化器物的意识和能力。

## 外部連結

  - [Acheulian Tools of North Africa — World Museum of
    Man](https://web.archive.org/web/20071118105353/http://www.worldmuseumofman.org/acheuliannafricaartifacts1.htm)
  - [Acheulian Tools of Europe — World Museum of
    Man](https://web.archive.org/web/20071118105348/http://www.worldmuseumofman.org/acheulianeuropeartifacts1.htm)
  - [Acheulean
    Gallery](https://web.archive.org/web/20110513233924/http://home.wanadoo.nl/marco.langbroek/acheul.html)
  - [Acheulean tools from
    Britain](http://www.beloit.edu/~museum/logan/paleoexhibit/paleobritain.htm)
  - [Acheulian Iran](http://antiquity.ac.uk/projgall/biglari/index.html)
  - [Acheulian
    Armenia](http://antiquity.ac.uk/ProjGall/aslanian%20et%20al/index.html/)
  - [Acheulian
    Project](https://web.archive.org/web/20061028040447/http://www.arch.soton.ac.uk/Prospectus/CAHO/europeresearch.html#acheulian%20project)
  - [Acheulean
    France](http://www.beloit.edu/~museum/logan/paleoexhibit/acheulian.htm)
  - [Early human fire skills
    revealed](http://news.bbc.co.uk/1/hi/sci/tech/3670017.stm)
  - [The Acheulian biface project: a digital archive for teaching and
    research](http://antiquity.ac.uk/ProjGall/marshall/marshall.html)
  - [Acheulean replacing Cheulean
    crafts](http://www.selimhassan.com/010104.php#cheulian-acheulean-crafts)

[Category:舊石器時代](../Category/舊石器時代.md "wikilink")
[Category:欧洲考古学文化](../Category/欧洲考古学文化.md "wikilink")