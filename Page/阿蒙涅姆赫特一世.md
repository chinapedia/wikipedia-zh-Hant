**阿蒙涅姆赫特一世**或**阿曼尼赫特一世**()（约公元前1991年—约公元前1962年在位）[埃及](../Page/埃及.md "wikilink")[法老](../Page/法老.md "wikilink")。他是[第十二王朝的建立者](../Page/埃及第十二王朝.md "wikilink")。

阿蒙涅姆赫特一世不是王室成员出身，因而十分注意巩固自己的权力。在他统治时代限制了无法无天的贵族和各省统治者的权力，并把首都从[底比斯迁至](../Page/底比斯.md "wikilink")[法尤姆](../Page/法尤姆.md "wikilink")。通过他的一系列积极政策，埃及的混乱局面得到改善。

阿蒙涅姆赫特一世在其统治的最后十年里让儿子[辛努塞尔特一世成为共同在位者](../Page/辛努塞尔特一世.md "wikilink")。他最后死于谋杀。

## 注释

<references/>

  - Wolfgang Kosack: *Berliner Hefte zur ägyptischen Literatur 1 - 12*:
    Teil I. 1 - 6/ Teil II. 7 - 12 (2 Bände). Paralleltexte in
    Hieroglyphen mit Einführungen und Übersetzung. Heft 9: Die Lehre des
    Königs Amenemhet I. an seinen Sohn. Verlag Christoph Brunner, Basel
    2015. ISBN 978-3-906206-11-0.

[Category:第十二王朝法老](../Category/第十二王朝法老.md "wikilink")
[Category:開國君主](../Category/開國君主.md "wikilink")
[Category:古埃及被杀害人物](../Category/古埃及被杀害人物.md "wikilink")
[Category:遇刺身亡的君主](../Category/遇刺身亡的君主.md "wikilink")