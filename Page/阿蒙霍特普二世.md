[缩略图](https://zh.wikipedia.org/wiki/File:Merytre-tomb-of-Ra.jpg "fig:缩略图")\]\]
**阿蒙霍特普二世**（**Amenhotep
II**）是[古埃及](../Page/古埃及.md "wikilink")[第十八王朝第七位](../Page/第十八王朝.md "wikilink")[法老](../Page/法老.md "wikilink")，在位約26年（約公元前1427年—约公元前1401年在位），是[图特摩斯三世之子](../Page/图特摩斯三世.md "wikilink")。

阿蒙霍特普二世在即位初期與父親共同執政，在位第3年時父親去世，[叙利亚發生叛亂](../Page/叙利亚.md "wikilink")，帶兵-{征}-討，取得勝利。在位第7及9年時再出兵敘利亞，此後與[米坦尼国王達成非正式的和平](../Page/米坦尼.md "wikilink")。在南方，[努比亚在此時期沒有發生被記載下來的叛亂事件](../Page/努比亚.md "wikilink")。阿蒙霍特普二世又与[巴比伦](../Page/巴比伦.md "wikilink")、[赫梯等同时代强国建立外交联系](../Page/赫梯.md "wikilink")。

阿蒙霍特普二世死後，其子[图特摩斯四世繼位](../Page/图特摩斯四世.md "wikilink")。

## 家庭

阿蒙霍特普二世是[图特摩斯三世与侧妃](../Page/图特摩斯三世.md "wikilink")[美丽特拉-哈特谢普苏特的儿子](../Page/美丽特拉-哈特谢普苏特.md "wikilink")，一开始，他并非图特摩斯三世指定的继承人，而是图特摩斯三世与不明妃嫔（或许是王后[萨提雅或](../Page/萨提雅.md "wikilink")[涅弗鲁利](../Page/涅弗鲁利.md "wikilink")）的儿子，阿蒙霍特普二世的兄弟，[阿蒙奈哈特](../Page/阿蒙奈哈特\(圖特摩斯三世之子\).md "wikilink")，但他在图特摩斯三世統治第35年時死去，阿蒙霍特普才得以继承王位。阿蒙霍特普二世有一名妻子，[提娅](../Page/提娅.md "wikilink")，她的家世不详，也不曾于任何阿蒙霍特普二世的文物中出现，仅出现于儿子图特摩斯四世的建筑与文物中，对她的记载也有限（疑是法老们担心他们的皇后妃嫔会像女皇[哈特谢普苏特一样掌权或有过大的势力或者擔心皇后妃嬪會以](../Page/哈特谢普苏特.md "wikilink")[阿蒙之神妻](../Page/阿蒙之神妻.md "wikilink")(God
Wife of Amun)的封號與地位奪權或收集勢力而把许多妃嫔记载删去），他是否有任何妾室至今有待考察。
阿蒙霍特普二世也有许多孩子，大多数孩子母亲身份不明:

  - [图特摩斯四世](../Page/图特摩斯四世.md "wikilink")(Thutmose lV)，母提娅，后继承王位
  - [阿蒙霍特普](../Page/阿蒙霍特普\(王子\).md "wikilink")(Amenhotep)，王子，母不详
  - [维本仙努](../Page/维本仙努.md "wikilink")，王子，母不详，与兄弟[尼德杰姆于建筑工人Minmose的雕像上被提起](../Page/尼德杰姆.md "wikilink")，其木乃伊于[帝王谷其父亲的墓室](../Page/帝王谷.md "wikilink")，[KV35发现](../Page/KV35.md "wikilink")，现在仍然留在那
  - [阿蒙尼莫普](../Page/阿蒙尼莫普\(王子\).md "wikilink")(amenemopet)，王子，母不详，于一个称为stela
    c的石碑与护士Senetruiu的石碑上被提起
  - [尼德杰姆](../Page/尼德杰姆.md "wikilink")(nedjem)，王子，母不详，与兄弟[维本仙努于建筑工人Minmose的雕像上被提起](../Page/维本仙努.md "wikilink")
  - [卡伊瓦斯特](../Page/卡伊瓦斯特.md "wikilink")(Khaewaset)，王子，于两个石柱上被提起，并得到“overseer
    of the cattle”的封号，王子得到这个封号是十分罕见的。
  - Aeheperkare?
  - Aakheperure?
  - [亚雷特](../Page/亚雷特.md "wikilink")(Laret)，公主，母不详，后为[图特摩斯四世的皇后](../Page/图特摩斯四世.md "wikilink")
  - [雅赫摩斯](../Page/雅赫摩斯\(王子\).md "wikilink")(Ahmose)，王子，母亲可能是提娅，出现于一个石碑（现藏[柏林](../Page/柏林.md "wikilink")），和他一个损坏的雕像（现藏[开罗](../Page/开罗.md "wikilink")）

## 陵墓与木乃伊

阿蒙霍特普二世的木乃伊在其墓([KV35](../Page/KV35.md "wikilink"))
发现，有许多的[皇家木乃伊都被移到了他的陵墓](../Page/皇家木乃伊.md "wikilink")，所以他的陵墓变成了"皇家木乃伊坑"。在陵墓里发现的雕像，皇家木乃伊，培葬品等都被运到[开罗博物馆](../Page/开罗博物馆.md "wikilink")。

## 注释

<references/>

[Category:第十八王朝法老](../Category/第十八王朝法老.md "wikilink")
[Category:古埃及木乃伊](../Category/古埃及木乃伊.md "wikilink")