在[數學中](../Page/數學.md "wikilink")，**阿貝爾範疇**（或稱**交換範疇**）是一個能對[態射與](../Page/態射.md "wikilink")[對象取和](../Page/對象.md "wikilink")，而且[核與](../Page/核_\(代数\).md "wikilink")[上核存在且滿足一定性質的](../Page/上核.md "wikilink")[範疇](../Page/範疇.md "wikilink")；最基本的例子是[阿貝爾群構成的範疇](../Page/阿貝爾群.md "wikilink")**Ab**。阿貝爾範疇是[同調代數的基本框架](../Page/同調代數.md "wikilink")。

## 定義

阿貝爾範疇的公理版本繁多，在此僅取其一（見外部連結）。

一個範疇\(\mathcal{A}\)若滿足下述條件，則稱**阿貝爾範疇**：

1.  \(\mathcal{A}\)是[加法範疇](../Page/加法範疇.md "wikilink")。
2.  所有態射皆有[核與](../Page/核_\(代数\).md "wikilink")[上核](../Page/上核.md "wikilink")。
3.  所有態射皆為[嚴格態射](../Page/嚴格態射.md "wikilink")。

只滿足前兩個條件者稱作**預阿貝爾範疇**。

若取\(k\)為一[交換環](../Page/交換環.md "wikilink")，則在上述定義中以*k-加法範疇*代換*加法範疇*，便得到*k-阿貝爾範疇*之定義。

## 例子

  - 如上所述，全體[阿貝爾群構成一個阿貝爾範疇](../Page/阿貝爾群.md "wikilink")**Ab**，而有限生成阿貝爾群構成的滿子範疇也是阿貝爾範疇，有限阿貝爾群亦同。
  - 設\(R\)為環，則左（或右）\(R\)-模範疇構成一個阿貝爾範疇；根據[Mitchell嵌入定理](../Page/Mitchell嵌入定理.md "wikilink")，任何[小的阿貝爾範疇皆價於某個](../Page/小範疇.md "wikilink")\(R\)-模範疇的一個滿子範疇。
  - 如果\(R\)是左[諾特環](../Page/諾特環.md "wikilink")，則有全體有限生成左\(R\)-模構成阿貝爾範疇；這是阿貝爾範疇在[交換代數中的主要面貌](../Page/交換代數.md "wikilink")。
  - 由前兩個例子可知：固定一個[域或](../Page/域.md "wikilink")[除環](../Page/除環.md "wikilink")，其上的[向量空間成一阿貝爾範疇](../Page/向量空間.md "wikilink")，有限維向量空間亦同。
  - 設\(X\)為[拓撲空間](../Page/拓撲空間.md "wikilink")，則所有\(X\)上的（實或複）[向量叢構成阿貝爾範疇](../Page/向量叢.md "wikilink")。
  - 承上，固定一個阿貝爾範疇\(\mathcal{A}\)，則取值於\(\mathcal{A}\)的[層與預層都構成阿貝爾範疇](../Page/層論.md "wikilink")。這是阿貝爾範疇在[代數幾何中的主要面貌](../Page/代數幾何.md "wikilink")。
  - 若\(\mathcal{C}\)為[小範疇而](../Page/小範疇.md "wikilink")\(\mathcal{A}\)為阿貝爾範疇，則所有函子\(\mathcal{C} \rightarrow \mathcal{A}\)構成一個阿貝爾範疇（其態射為[自然變換](../Page/自然變換.md "wikilink")），若更設\(\mathcal{C}\)為[預加法範疇](../Page/預加法範疇.md "wikilink")，則所有加法函子\(\mathcal{C} \rightarrow \mathcal{A}\)也構成阿貝爾範疇。這在一方面推廣了空間上預層的例子，一方面也函攝了\(R\)-模的例子，因為[環可視為僅有單個對象的預加法範疇](../Page/環.md "wikilink")。
  - [拓撲向量空間是預阿貝爾範疇](../Page/拓撲向量空間.md "wikilink")，而非阿貝爾範疇。

## 基本性質

  - 在阿貝爾範疇中，任何態射\(f\)皆可分解為[單射](../Page/單射.md "wikilink")。[滿射](../Page/滿射.md "wikilink")，其中的滿射稱為\(f\)的*上像*，而單射則稱為\(f\)的*像*。此性質源自公理中對態射嚴格性的要求。
  - 任一態射\(f\)是[單射若且唯若](../Page/單射.md "wikilink")\(\mathrm{Ker}(f)=0\)，是[滿射若且唯若](../Page/滿射.md "wikilink")\(\mathrm{Coker}(f)=0\)，是[同構若且唯若](../Page/同構.md "wikilink")\(\mathrm{Ker}(f)=0, \mathrm{Coker}(f)=0\)。
  - [子對象與](../Page/子對象.md "wikilink")[商對象具良好性質](../Page/商對象.md "wikilink")。例如：任一對象的子對象構成的[偏序集合是](../Page/偏序關係.md "wikilink")[有界格](../Page/格_\(數學\).md "wikilink")。
  - 任一阿貝爾範疇\(\mathcal{A}\)可設想為有限生成[阿貝爾群的](../Page/阿貝爾群.md "wikilink")-{么}-半範疇上的[模](../Page/模.md "wikilink")；這意謂著我們能構造一個有限生成阿貝爾群\(G\)與對象\(A\)的[張量積](../Page/張量積.md "wikilink")。
  - 承上，阿貝爾範疇也是[上模](../Page/上模.md "wikilink")；\(\mathrm{Hom}(G,A)\)可以詮釋為\(\mathcal{A}\)的對象。若\(\mathcal{A}\)
    [完備](../Page/完備範疇.md "wikilink")，\(G\)的有限生成假設可以移除。

## 相關概念

阿貝爾範疇是同調代數的基本框架，它容許討論同調代數中的基本構造，如[正合序列](../Page/正合序列.md "wikilink")、短正合序列與導函子。

對所有阿貝爾範疇均成立的重要結果包括[五引理](../Page/五引理.md "wikilink")（含特例短五引理）與[蛇引理](../Page/蛇引理.md "wikilink")（含特例[九引理](../Page/九引理.md "wikilink")）等等。

## 源流

阿貝爾範疇源於[亞歷山大·格羅滕迪克知名的](../Page/亞歷山大·格羅滕迪克.md "wikilink")*東北論文*，該論文發表於1950年代，當時存在兩套不同的[上同調理論](../Page/上同調.md "wikilink")：群上同調與層上同調，兩者性質相近而定義迥異。格羅滕迪克將兩套理論以阿貝爾範疇上的[導函子統合](../Page/導函子.md "wikilink")：一者是[拓撲空間](../Page/拓撲空間.md "wikilink")\(X\)上的阿貝爾層範疇，一者則是[群](../Page/群.md "wikilink")\(G\)的\(G\)-[模範疇](../Page/模.md "wikilink")，導出上同調的函子分別是\(\mathcal{F} \mapsto \Gamma(X, \mathcal{F})\)與\(M \mapsto M^G\)。

## 文獻

  - P. Freyd. *Abelian Categories,* Harper and Row, New York, 1964.
    [可在線閱讀.](http://www.tac.mta.ca/tac/reprints/articles/3/tr3abs.html)
  - Alexandre Grothendieck, *Sur quelques points d'algèbre homologique*,
    Tôhoku Mathematics Journal, 1957
  - Barry Mitchell: *Theory of Categories*, New York, Academic Press,
    1965.
  - N. Popescu: *Abelian categories with applications to rings and
    modules*, Academic Press, London, 1973.
  - Masaki Kashiwara and Pierre Schapira, *Categories and Sheaves*,
    Springer. ISBN 3540279490

## 外部連結

  - [Categories, sites, sheaves and stacks /Pierre
    Schapira](https://web.archive.org/web/20070402075309/http://www.institut.math.jussieu.fr/~schapira/polycopies/Sta.pdf)

[Category:加法范畴](../Category/加法范畴.md "wikilink")
[A](../Category/同調代數.md "wikilink")
[Category:尼尔斯·阿贝尔](../Category/尼尔斯·阿贝尔.md "wikilink")