**阿迪尔·扎米尔·阿卜杜·阿尔·摩辛·扎米尔**（），出生在[科威特城](../Page/科威特城.md "wikilink")。被逮捕并拘留在[美国位于](../Page/美国.md "wikilink")[古巴的](../Page/古巴.md "wikilink")[关塔那摩海军基地](../Page/关塔那摩湾.md "wikilink")。
\[1\] \[2\]

根据[美国国防部的信息](../Page/美国国防部.md "wikilink").\[3\]阿尔·扎米尔的关塔那摩身份号码为568。美国国防部称阿尔·扎米尔于1963年8月23日出生在科威特的[科威特城](../Page/科威特城.md "wikilink")。

## 战斗人员身份审查法庭

最开始[布什政府宣称他们可以保证所有](../Page/乔治·沃克·布什.md "wikilink")[反恐战争中的俘虏们根据](../Page/反恐战争.md "wikilink")[日内瓦公约而获得的保护](../Page/日内瓦公约.md "wikilink")。该政策在司法部遭到质疑。有批评认为对于俘虏是否可享受[战犯的待遇](../Page/战犯.md "wikilink")，美国不能逃避其作为一个[有管辖权的仲裁机关来决定这个问题的责任](../Page/有管辖权的仲裁机关.md "wikilink")。

接下来[国防部创立了](../Page/美国国防部.md "wikilink")[战斗人员身份审查法庭](../Page/战斗人员身份审查法庭.md "wikilink")。然而，这些审查法庭没有权利决定战犯是否为“合法”的战斗人员
---
他们只能够对于这些战犯是否之前被界定为布什政府所定义的[敌方战斗人员作出建议](../Page/敌方战斗人员.md "wikilink")。

阿尔·扎米尔选择参与他的[战斗人员身份审查法庭](../Page/战斗人员身份审查法庭.md "wikilink")。\[4\]

### 指控

针对阿尔·扎米尔的指控包括：\[5\]

  -
    一个与[基地组织有关的被扣押者](../Page/基地组织.md "wikilink")。
    1.  该被扣押者承认与被扣押者[阿卜·阿卜杜勒·阿齐兹](../Page/阿卜·阿卜杜勒·阿齐兹.md "wikilink")(又名[阿卜杜勒·阿齐兹·阿尔-马特拉夫](../Page/阿卜杜勒·阿齐兹·阿尔-马特拉夫.md "wikilink"))以及[萨马·汉德在](../Page/萨马·汉德.md "wikilink")[喀布尔](../Page/喀布尔.md "wikilink"),
        [赫拉特和](../Page/赫拉特.md "wikilink")[坎大哈建立](../Page/坎大哈.md "wikilink")[瓦法组织](../Page/瓦法.md "wikilink")。被扣押者在其坎大哈办公室工作。
    2.  瓦发组织位列[13324号行政命令之上](../Page/13324号行政命令.md "wikilink")，是一个进行或者将会进行恐怖活动而造成极大危险的实体。
    3.  被扣押者与[苏里曼·阿卜·盖思有紧密联系](../Page/苏里曼·阿卜·盖思.md "wikilink")，并承认盖思为基地组织和[奥萨玛·本·拉登的发言人](../Page/奥萨玛·本·拉登.md "wikilink")。
    4.  被扣押者在[巴基斯坦一间住所内居住了几个星期](../Page/巴基斯坦.md "wikilink")，并策划逃离该国。他和其他16人后来在该住所内被巴基斯坦当局逮捕。
    5.  被扣押者事先知道[九一一袭击事件的计划](../Page/九一一袭击事件.md "wikilink")。

## 管理检讨委员会聆讯

被界定为“敌方战斗人员”的被扣押者按计划要在每年[管理检讨委员会的聆讯中就他们的案底进行检讨](../Page/管理检讨委员会.md "wikilink")。管理检讨委员会没有权力就一个被扣押者是否为战犯来进行检查，也没有权力检查一个被扣押者是否能定性为“敌方战斗人员”。

他们有权力做的事就是考虑一名被扣押者时候应该继续被美国扣押，由于他们还在造成威胁；或是应该被安全的遣返到他们的所属国进行监禁；或是应该被释放。

支持和反对继续扣押阿尔·扎米尔的因素都在国防部于2006年3月3日发表的121中。\[6\]

### 下列的支持继续扣押的主要因素

  -
    a. 评论
    1.  被扣押者事先知道[九一一袭击事件的计划](../Page/九一一袭击事件.md "wikilink")。
    2.  被扣押者被认为是[塔克菲理组织的一员](../Page/塔克菲理.md "wikilink"),
        但不是[伊斯兰圣战主义组织的成员](../Page/伊斯兰圣战主义.md "wikilink")，因为他没有为此而甘愿献身的毅力。
    3.  [达克非](../Page/达克非.md "wikilink")，或者叫“指责名不副实的[穆斯林的组织](../Page/穆斯林.md "wikilink")”，是1970年代在埃及建立的。这个组织的名称在[埃及以外也被采用](../Page/埃及.md "wikilink")，因为其成员通常都是逃犯。这一部分是因为他们激进的观念，包括制定推翻不完全基于[伊斯兰教教法的政府的目标](../Page/伊斯兰教教法.md "wikilink")。

<!-- end list -->

  -
    b. 联系/组织
    1.  该被扣押者承认与被扣押者[阿卜·阿卜杜勒·阿齐兹](../Page/阿卜·阿卜杜勒·阿齐兹.md "wikilink")(又名[阿卜杜勒·阿齐兹·阿尔-马特拉夫](../Page/阿卜杜勒·阿齐兹·阿尔-马特拉夫.md "wikilink"))以及[萨马·汉德在](../Page/萨马·汉德.md "wikilink")[喀布尔](../Page/喀布尔.md "wikilink"),
        [赫拉特和](../Page/赫拉特.md "wikilink")[坎大哈建立](../Page/坎大哈.md "wikilink")[瓦法组织](../Page/瓦法.md "wikilink")。被扣押者在其坎大哈办公室工作。
    2.  根据一项外国政府调查，非政府组织“瓦法”全名叫作“[al Wafa al Igatha al
        Islamia](../Page/al_Wafa_al_Igatha_al_Islamia.md "wikilink")”(瓦法人道主义组织)。其大本营在[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")，并相信与[奥萨玛·本·拉登和](../Page/奥萨玛·本·拉登.md "wikilink")[阿富汗](../Page/阿富汗.md "wikilink")[圣战战士有联系](../Page/圣战战士.md "wikilink")。
    3.  被扣押者与[苏里曼·阿卜·盖思有紧密联系](../Page/苏里曼·阿卜·盖思.md "wikilink")，并承认盖思为基地组织和[奥萨玛·本·拉登的发言人](../Page/奥萨玛·本·拉登.md "wikilink")。
    4.  被扣押者与基地组织的发言人苏里曼·阿卜·盖思及其家属一起到卡拉奇以帮助苏里曼·阿卜·盖思安全的从巴基斯坦撤离。
    5.  被扣押者声称[法埃兹·艾坎大里在他的住所内待过一个星期](../Page/法埃兹·艾坎大里.md "wikilink")。
    6.  法埃兹·艾坎大里是奥萨玛·本·拉登的法律顾问和亲密朋友。
    7.  被扣押者曾被邀请到一个牵涉到2002年10月[美国海军陆战队在](../Page/美国海军陆战队.md "wikilink")[科威特](../Page/科威特.md "wikilink")[费拉卡被袭击事件的人的居所](../Page/费拉卡.md "wikilink")。
    8.  被扣押者拥有一名在2002年10月杀死一名美国海军陆战队员之后又被杀的人的电话号码。
    9.  被扣押者曾因涉嫌牵涉达克非运动而被科威特政府调查。

<!-- end list -->

  -
    c. 其他相关数据
    1.  被扣押者在[巴基斯坦一间住所内居住了几个星期](../Page/巴基斯坦.md "wikilink")，并策划逃离该国。他和其他16人后来在该住所内被巴基斯坦当局逮捕。
    2.  被扣押者曾有参加偏激分子警戒会的活动。
    3.  被扣押者被认为是顽固不化的偏激分子。

### 下列的支持释放的主要因素

:\*被扣押者否认知道任何关于[瓦法组织与](../Page/瓦法.md "wikilink")[基地组织或者](../Page/基地组织.md "wikilink")[塔利班有关联的消息](../Page/塔利班.md "wikilink")。

:\*被扣押者否认与科威特反对党有任何关系。

:\*被扣押者宣称从来没有加入过达克非组织。

## 遣返与释放

阿尔·扎米尔是2005年11月被遣返回科威特的五名科威特人之一。\[7\]

该五人在科威特被审判，其后被释放。\[8\]

[华盛顿邮报报道指控他们的主要理由有两条](../Page/华盛顿邮报.md "wikilink")：协助成立了瓦法组织，一个与基地组织有关的阿富汗慈善团体；以及与塔利班共同作战。\[9\]
另外，指控还声称被扣押者的行为危及到科威特的政权以及其与友邦的关系。

被扣押者的辩护声称在关塔那摩获取的证言不能用于科威特法庭，因为被扣押者和询问者都没有签署。\[10\]
另外，他们还声称美国的指控不适用于科威特法律。

阿尔·扎米尔的审判开始与2006年3月，在2006年7月22日被释放\[11\]

## 参见

  - [Mohammed Fnaitil
    al-Dehani](../Page/Mohammed_Fnaitil_al-Dehani.md "wikilink")

## 参考资料

<references/>

[Al Zamil, Adil Zamil Abdull
Mohssin](../Category/美国法外的科威特囚犯.md "wikilink") [Al
Zamil, Adil Zamil Abdull Mohssin](../Category/关塔那摩湾被扣押人员.md "wikilink")
[Al Zamil, Adil Zamil Abdull Mohssin](../Category/科威特人.md "wikilink")
[Category:瓦法组织](../Category/瓦法组织.md "wikilink")
[Category:关塔那摩被扣押人员](../Category/关塔那摩被扣押人员.md "wikilink")

1.  [犯人名单(.pdf)](http://www.defenselink.mil/pubs/foi/detainees/detainee_list.pdf),
    *[美国国防部](../Page/美国国防部.md "wikilink")*, 4月20日 2006年
2.  [文件(.pdf)](http://wid.ap.org/documents/detainees/adilalzamil.pdf)
    from
    阿迪尔·扎米尔·阿卜杜·阿尔·摩辛·扎米尔'的*[战斗人员身份审查法庭](../Page/战斗人员身份审查法庭.md "wikilink")*
3.  [犯人名单
    (.pdf)](http://www.defenselink.mil/news/May2006/d20060515%20List.pdf),
    *[美国国防部](../Page/美国国防部.md "wikilink")*, 5月15日 2006年
4.  [documents
    (.pdf)](http://wid.ap.org/documents/detainees/adilalzamil.pdf) from
    阿迪尔·扎米尔·阿卜杜·阿尔·摩辛·扎米尔'的*[战斗人员身份审查法庭](../Page/战斗人员身份审查法庭.md "wikilink")*
5.  [文件(.pdf)](http://wid.ap.org/documents/detainees/adilalzamil.pdf)
    from
    阿迪尔·扎米尔·阿卜杜·阿尔·摩辛·扎米尔的[战斗人员身份审查法庭](../Page/战斗人员身份审查法庭.md "wikilink")
6.  [支持和反对继续扣押的因素(.pdf)](http://www.defenselink.mil/pubs/foi/detainees/csrt/ARB_Factors_Set_1_1161-1234.pdf)
    of 阿迪尔·扎米尔·阿卜杜·阿尔·摩辛·扎米尔 *[管理检讨委员会](../Page/管理检讨委员会.md "wikilink")*
    - page 41
7.  [科威特人从关塔那摩被释放](http://news.bbc.co.uk/2/hi/americas/4403498.stm)，*[BBC](../Page/BBC.md "wikilink")*，2005年11月4日
8.  [科威特法庭释放前关塔那摩人员](http://www.iol.co.za/index.php?set_id=1&click_id=22&art_id=vn20060522043620128C944859).
    *[独立在线(南非)](../Page/独立在线\(南非\).md "wikilink")*，2006年5月22日
9.  [5
    前关塔那摩人员在科威特被释放](http://www.washingtonpost.com/wp-dyn/content/article/2006/05/21/AR2006052101476.html),
    *[华盛顿邮报](../Page/华盛顿邮报.md "wikilink")*，2006年5月21日
10. [5
    前关塔那摩人员在科威特被释放](http://www.washingtonpost.com/wp-dyn/content/article/2006/05/21/AR2006052101476.html),
    *[华盛顿邮报](../Page/华盛顿邮报.md "wikilink")*，2006年5月21日
11. [科威特的极特魔人员再次被释放](http://www.kuwaittimes.net/Navariednews.asp?dismode=article&artid=326646425)
    ，*[科威特时报](../Page/科威特时报.md "wikilink")*，2006年7月23日