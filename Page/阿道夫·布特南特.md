**阿道夫·弗里德里希·约翰·布特南特**（[德语](../Page/德语.md "wikilink")：****，）是一位[德国化学家](../Page/德国.md "wikilink")。

## 生平

1903年生於[不來梅逝於](../Page/不來梅.md "wikilink")[慕尼黑](../Page/慕尼黑.md "wikilink")，1939年获[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。

[Category:德国化学家](../Category/德国化学家.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:慕尼黑大學教師](../Category/慕尼黑大學教師.md "wikilink")
[Category:蒂賓根大學教師](../Category/蒂賓根大學教師.md "wikilink")
[Category:哥廷根大學教師](../Category/哥廷根大學教師.md "wikilink")
[Category:馬爾堡大學校友](../Category/馬爾堡大學校友.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:战功十字勋章获得者](../Category/战功十字勋章获得者.md "wikilink")
[Category:马克斯·普朗克学会人物](../Category/马克斯·普朗克学会人物.md "wikilink")
[Category:不來梅人](../Category/不來梅人.md "wikilink")