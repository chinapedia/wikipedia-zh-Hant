**阿道夫·艾希曼**（，），[納粹德國前高官](../Page/納粹德國.md "wikilink")，也是在[清洗猶太人中執行](../Page/清洗猶太人.md "wikilink")「[最终解决方案](../Page/最终解决方案.md "wikilink")」的主要負責者，被[猶太人稱為](../Page/猶太人.md "wikilink")「**納粹劊子手**」，[二次大戰後定居至](../Page/二次大戰.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")，遭[以色列](../Page/以色列.md "wikilink")[情報特務局](../Page/情報特務局.md "wikilink")（俗稱[摩薩德](../Page/摩薩德.md "wikilink")）幹員綁架，公開審判後[絞死](../Page/絞死.md "wikilink")。

## 生平

阿道夫·艾希曼于1906年3月19日出生於[德意志帝國的](../Page/德意志帝國.md "wikilink")[索林根](../Page/索林根.md "wikilink")\[1\]，童年時代移居[奧地利](../Page/奧地利.md "wikilink")\[2\]。1932年，艾希曼加入奧地利[納粹黨](../Page/納粹黨.md "wikilink")，次年該黨被奧地利政府列為非法，艾希曼離開奧地利回到德國。\[3\]1934年因負責[達豪集中營而受到](../Page/達豪集中營.md "wikilink")[海德里希的賞識](../Page/海德里希.md "wikilink")；1937年艾希曼曾經前往[海法與](../Page/海法.md "wikilink")[開羅](../Page/開羅.md "wikilink")，研究將猶太人移住[巴勒斯坦的可能性](../Page/巴勒斯坦.md "wikilink")，後來艾希曼向納粹方面以經濟理由反對將猶太人移往巴勒斯坦的計劃。

1942年艾希曼出席[萬湖會議](../Page/萬湖會議.md "wikilink")，之後晉昇為黨衛軍[中校](../Page/中校.md "wikilink")(SS-Obersturmbannführer)；在大屠殺中主要負責將猶太人移送[集中營的運輸](../Page/集中營.md "wikilink")，還有和當地猶太領袖的磋商、當地的建立。尤其是與當地猶太領袖的合作，被納粹視為進行大屠殺的基石。

[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")，艾希曼被[美軍俘虜](../Page/美軍.md "wikilink")，但之後逃脫，从[意大利的](../Page/意大利.md "wikilink")[热那亚出发](../Page/热那亚.md "wikilink")，在經過漫長的逃亡旅行後，艾希曼流亡到[阿根廷](../Page/阿根廷.md "wikilink")[布宜诺斯艾利斯](../Page/布宜诺斯艾利斯.md "wikilink")，在当地的[奔驰公司工作](../Page/奔驰.md "wikilink")。但是1960年[以色列](../Page/以色列.md "wikilink")[情報特務局](../Page/情報特務局.md "wikilink")（俗稱[摩薩德](../Page/摩薩德.md "wikilink")）卻查出艾希曼的下落，並且於1960年5月11日將其綁架，並秘密運至以色列。由於艾希曼的逮捕方式相當於[綁架](../Page/綁架.md "wikilink")，引發了阿根廷與以色列之間的[外交糾紛](../Page/外交.md "wikilink")。

1960年5月23日下午，耶路撒冷时间4点整，[以色列总理](../Page/以色列总理.md "wikilink")[本·古里安向国人宣布](../Page/大卫·本-古理安.md "wikilink")：

1961年4月11日，艾希曼於[耶路撒冷受審](../Page/耶路撒冷.md "wikilink")，被以[反人道罪名等十五條罪名起訴](../Page/反人道罪.md "wikilink")。雖然有不少大屠殺受害者出面作證，但大部分證人的口供和艾希曼的罪責並無直接關聯，而檢察官對於納粹官僚體系的理解有許多錯誤，並在起訴書和詰問中大大的誇大了艾希曼在大屠殺的角色和權力。但由於一審的三位德裔法官對正當程序的堅持，還有對納粹足夠的理解，所以艾希曼罪責在一審判決書上大致上被正確的呈現。以色列政府將艾希曼安排在[防彈玻璃後方受審](../Page/防彈玻璃.md "wikilink")。艾希曼面對犯罪的控訴，都以「一切都是奉命行事」回答。同年12月11日艾希曼被判處有罪，12月15日被判[死刑](../Page/死刑.md "wikilink")。艾希曼提出上訴，最高法院維持原判，1962年5月31日夜間，艾希曼被處以[絞刑](../Page/絞刑.md "wikilink")。

## 评价

作家[-{zh-hk:漢娜·鄂蘭;zh-tw:漢娜·鄂蘭;zh-cn:汉娜·阿伦特;}-在](../Page/汉娜·阿伦特.md "wikilink")《》（*Eichmann
in
Jerusalem*）一书中认为艾希曼是一个遵从命令的[官僚](../Page/官僚制.md "wikilink")，他所体现的不是“极端的邪恶”，而是“平庸的邪恶”（The
banality of evil, 意指當任何人甘於放棄對善惡是非的判斷力去贗服權威，那麼最平凡的人也可能導致最極致的邪惡）。

## 注释

## 外部链接

  - [以色列首次披露追捕纳粹阿道夫·艾希曼过程](http://news.163.com/12/0209/09/7PQEU1FG00014JB5.html)

[Category:被處決的德國人](../Category/被處決的德國人.md "wikilink")
[Category:第二次世界大戰德國戰犯](../Category/第二次世界大戰德國戰犯.md "wikilink")
[Category:德國第二次世界大戰人物](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:納粹德國人物](../Category/納粹德國人物.md "wikilink")
[Category:索林根人](../Category/索林根人.md "wikilink")
[Category:战功十字勋章获得者](../Category/战功十字勋章获得者.md "wikilink")
[Category:被判反人类罪的德国人](../Category/被判反人类罪的德国人.md "wikilink")
[Category:被绑架的德国人](../Category/被绑架的德国人.md "wikilink")

1.  [媒体称二战后联邦德国政府帮助纳粹战犯逃亡南美](http://news.ifeng.com/mil/history/detail_2010_03/21/494068_0.shtml)

2.

3.