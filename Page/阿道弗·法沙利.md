[Three_yuujo_on_veranda_by_Adolfo_Farsari_1885.jpg](https://zh.wikipedia.org/wiki/File:Three_yuujo_on_veranda_by_Adolfo_Farsari_1885.jpg "fig:Three_yuujo_on_veranda_by_Adolfo_Farsari_1885.jpg")在[檐廊擺姿勢](../Page/檐廊.md "wikilink")，約攝於1885年，以[蛋白版制版法印像沖印](../Page/蛋白版制版法印像沖印.md "wikilink")，再徒手上色。\]\]

**阿道弗·法沙利**（）是一名[義大利](../Page/義大利.md "wikilink")[攝影師](../Page/攝影.md "wikilink")，工作於[日本](../Page/日本.md "wikilink")[橫濱市](../Page/橫濱市.md "wikilink")。曾經參與軍事上的工作，包括在[美國內戰中服兵役](../Page/美國內戰.md "wikilink")，最終成為一位成功的[企業家及商業攝影師](../Page/企業家.md "wikilink")。

## 作品

Photographs are indicated by Farsari's titles, followed by the date of
exposure, the photographic process, and a descriptive title.

Image:Farsari rickshaw.jpg|*Jinriki*, 1886. Hand-coloured albumen print
on a decorated album page.

A [rickshaw](../Page/rickshaw.md "wikilink") driver, two passengers and
a bearer. Image:Farsari sumo.jpg|*Wrestlers*, c. 1886. Hand-coloured
albumen print.

View of a sumo match showing *[rikishi](../Page/rikishi.md "wikilink")*
\[wrestlers\], a *[gyōji](../Page/gyoji.md "wikilink")* \[referee\] and
an audience. Image:Farsari house interior.jpg|*Rooms*, 1886.
Hand-coloured albumen print on a decorated album page.

Interior of a house, Japan. Image:Farsari Shitennō-ji.jpg|*Tennonji,
Osaka*, between 1885 and 1890. Hand-coloured albumen print on a
decorated album page.

View of [Shitennō-ji](../Page/Shitennō-ji.md "wikilink"),
[Osaka](../Page/Osaka.md "wikilink"). Image:Farsari Shiba.jpg| *Shiba
Chokugaku Mon (back)*, between 1885 and 1890. Hand-coloured albumen
print.

View of the Yūshō-in Mausoleum complex showing the bell tower and
Chokugaku gate, [Zōjō-ji](../Page/Zōjō-ji.md "wikilink"), Tokyo.
Image:Farsari photomontage.jpg|*Japan*, between 1885 and 1890. Albumen
print.

[Photomontage](../Page/Photomontage.md "wikilink") incorporating various
images by *A. Farsari & Co.*. Image:Farsari title page.jpg|*A. Farsari &
Co.*, c. 1890.

Title page from a photograph album by *A. Farsari & Co.*. Image:Farsari
ad Keeling's.jpg|Advertisement for *A. Farsari & Co.*, 1887. In
[*Keeling's Guide to
Japan*](http://www.baxleystamps.com/litho/meiji/keelings_1890_4th_2d.shtml),
4th Edition, 2nd Issue, 1890.

[Category:在日本的意大利人](../Category/在日本的意大利人.md "wikilink")
[Category:義大利攝影師](../Category/義大利攝影師.md "wikilink")
[Category:維琴察人](../Category/維琴察人.md "wikilink")
[Category:肖像攝影師](../Category/肖像攝影師.md "wikilink")