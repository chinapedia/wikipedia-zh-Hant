[Silla-monarch(1-12).png](https://zh.wikipedia.org/wiki/File:Silla-monarch\(1-12\).png "fig:Silla-monarch(1-12).png")中的新罗国世系\]\]
**阿達羅尼師今**（），新羅第8代君主，[逸聖尼師今與只珍內禮夫人之長子](../Page/逸聖尼師今.md "wikilink")，妃為[祗摩尼師今與愛禮夫人之女內禮夫人](../Page/祗摩尼師今.md "wikilink")，三國時期[新羅朴氏君主最後一位](../Page/新羅.md "wikilink")。
157年開雞立嶺路，159年再開竹嶺路，又在165年及167年兩度征[百濟](../Page/百濟.md "wikilink")，將[新羅版圖漸漸擴大](../Page/新羅.md "wikilink")。
阿達羅尼師今薨，無子（實有一子為朴景輝之先祖朴碧芳），國人立[昔脫解與阿孝夫人之孫](../Page/脫解尼師今.md "wikilink")，昔仇鄒與只珍內禮夫人之子[伐休尼師今](../Page/伐休尼師今.md "wikilink")（阿達羅王同母異父之弟）為君主。

## 治世

  - 154年，任[继元为伊飡](../Page/继元.md "wikilink")。155年，任[兴宣为一吉飡](../Page/兴宣.md "wikilink")。
  - 156年，设立[鸡立岭路](../Page/鸡立岭路.md "wikilink")。157年，设立[甘勿县](../Page/甘勿县.md "wikilink")、[马山县](../Page/马山县_\(新罗\).md "wikilink")。158年，设立[竹岭路](../Page/竹岭路.md "wikilink")。
  - 165年，阿飡[吉宣谋划反叛](../Page/吉宣.md "wikilink")，事迹败露后害怕被杀，便逃到[百济](../Page/百济.md "wikilink")。阿达罗向[百济王要人不得](../Page/盖娄王.md "wikilink")，便攻百济。百济守城不出，新罗军粮尽而归。
  - 167年，百济攻破新罗西部两座城池，俘获千余人民。后阿达罗命一吉飡兴宣领兵两万击之，又亲自率八千骑兵，到达[汉江](../Page/汉江_\(韩国\).md "wikilink")。百济很害怕，便于新罗和平。
  - 168年，伊飡继元去世，兴宣接任。170年，百济侵略边境。172年，任[仇道为波珍飡](../Page/仇道.md "wikilink")、[仇须兮为一吉飡](../Page/仇须兮.md "wikilink")。
  - 173年，[倭国女王](../Page/倭国.md "wikilink")[卑弥呼遣使来聘问](../Page/卑弥呼.md "wikilink")。
  - 184年，阿达罗尼师今去世，国人立[昔脱解子仇邹之子](../Page/脱解尼师今.md "wikilink")[伐休尼师今为君主](../Page/伐休尼师今.md "wikilink")。

## 家庭

  - 祖母：伊利生夫人
  - 父：[逸圣尼师今](../Page/逸圣尼师今.md "wikilink")
  - 母：只珍內禮夫人朴氏（支所禮王之女）
  - 妻：[内礼夫人朴氏](../Page/内礼夫人.md "wikilink")（[祗摩尼师今之女](../Page/祗摩尼师今.md "wikilink")）
  - 子：朴碧芳

## 外部連結

[Category:三國時代新羅君主](../Category/三國時代新羅君主.md "wikilink")