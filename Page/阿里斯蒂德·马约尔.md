**阿里斯蒂德·马约尔** （Aristide Joseph Bonaventure
Maillol，）是[法国](../Page/法国.md "wikilink")[雕塑家和](../Page/雕塑家.md "wikilink")[画家](../Page/画家.md "wikilink")。

## 生平

马约尔出生于法国西南部
[东比利牛斯省边境沿海的班雨勒镇](../Page/东比利牛斯省.md "wikilink")，1881年前往[巴黎学习](../Page/巴黎.md "wikilink")[艺术](../Page/艺术.md "wikilink")，1885年进入美术学校，师从于[卡巴内尔和](../Page/卡巴内尔.md "wikilink")[杰洛姆学习](../Page/杰洛姆.md "wikilink")，他早期的[绘画明显受到](../Page/绘画.md "wikilink")[夏凡纳和](../Page/夏凡纳.md "wikilink")[高更的影响](../Page/高更.md "wikilink")。

高更鼓励他从事装饰艺术，他参与了织花挂毯的设计，1893年，他在班雨勒镇开办了一个织花挂毯厂，在法国造成很大的影响。1895年开始，他创造了一些小泥塑，开始从事[雕塑并放弃了设计织花挂毯工作](../Page/雕塑.md "wikilink")。

他在83岁高龄在家乡班雨勒镇，因在雷雨天发生车祸而逝世。在巴黎有收藏他作品的一个博物馆，他的故居也已经开辟成一个博物馆。

## 作品

他重要的作品包括1912年为[塞尚设计的纪念碑](../Page/塞尚.md "wikilink")，以及一系列纪念[第一次世界大战的纪念碑](../Page/第一次世界大战.md "wikilink")。他的雕塑作品大部分是以女人人体作为主题的，稳重、成熟并有[古典主义艺术的痕迹](../Page/古典主义.md "wikilink")，是古典主义和现代[摩尔的抽象雕塑之间乘前启后的过渡时期最重要的雕塑家](../Page/摩尔.md "wikilink")。
[Aristide_Maillol_la_nuit_1902-2.jpg](https://zh.wikipedia.org/wiki/File:Aristide_Maillol_la_nuit_1902-2.jpg "fig:Aristide_Maillol_la_nuit_1902-2.jpg")\]\]

## 参考文献

  - Solomon R. Guggenheim Museum, “Aristide Maillol, 1861-1944”, New
    York, Solomon R. Guggenheim Foundation, 1975.

## 外部链接

  - [20世纪雕塑大师](http://www.ilovefiguresculpture.com/masters/maillol/maillol.html)
  - [马约尔博物馆网站](https://web.archive.org/web/20080222004819/http://www.museemaillol.com/index2.html)

[Maillol, Aristide](../Category/法国雕塑家.md "wikilink")