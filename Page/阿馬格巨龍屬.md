**阿馬格巨龍屬**（[屬名](../Page/屬.md "wikilink")：，意為「阿馬格巨人」）屬於[蜥腳下目的](../Page/蜥腳下目.md "wikilink")[泰坦巨龍類](../Page/泰坦巨龍類.md "wikilink")，是種大型的四足[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。

阿馬格巨龍的化石是在1983年發現於[阿根廷](../Page/阿根廷.md "wikilink")[內烏肯省的](../Page/內烏肯省.md "wikilink")[阿馬格組](../Page/阿馬格組.md "wikilink")（La
Amarga
Formation），地質年代為下[白堊紀的](../Page/白堊紀.md "wikilink")[巴列姆階](../Page/巴列姆階.md "wikilink")。[正模標本](../Page/正模標本.md "wikilink")（編號MACN
PV
N51、53、34）包含六節[尾椎](../Page/尾椎.md "wikilink")、一個[肩胛骨](../Page/肩胛骨.md "wikilink")、一個[股骨](../Page/股骨.md "wikilink")，以及一個[距骨](../Page/距骨.md "wikilink")。阿馬格巨龍的肩帶平坦、粗壯、且寬廣\[1\]。

## 參考資料

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:叉龍科](../Category/叉龍科.md "wikilink")

1.