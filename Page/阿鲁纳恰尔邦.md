**阿鲁纳恰尔邦**（，拉丁转写：**Arunachal
Pradesh**）又譯**“阿鲁纳查尔邦”**、**“阿倫納曹”**，是[印度的一個邦](../Page/印度.md "wikilink")，與南部的[阿薩姆邦和](../Page/阿薩姆邦.md "wikilink")[那加蘭邦接壤](../Page/那加蘭邦.md "wikilink")，與西方的[不丹](../Page/不丹.md "wikilink")、東部的[緬甸和北方的](../Page/緬甸.md "wikilink")[中國為鄰](../Page/中華人民共和國.md "wikilink")，首府為[伊塔納加爾](../Page/伊塔納加爾.md "wikilink")。

該邦大部份地區位於[中印邊界糾紛地帶](../Page/中印邊界問題.md "wikilink")，其合法性不被中國政府承認，中國宣称擁有对该邦的绝大部分地区之[主权](../Page/主权.md "wikilink")，称之为**[藏南地区](../Page/藏南地区.md "wikilink")**，并将该地区划入[西藏自治区的](../Page/西藏自治区.md "wikilink")[错那](../Page/错那.md "wikilink")、[隆子](../Page/隆子.md "wikilink")、[朗县](../Page/朗县.md "wikilink")、[米林](../Page/米林.md "wikilink")、[墨脱](../Page/墨脱.md "wikilink")、[察隅六县的管辖范围之内](../Page/察隅.md "wikilink")，不承认[印度拥有该地区的主权](../Page/印度.md "wikilink")。

## 历史

### 早期历史

阿鲁纳恰尔邦的历史至今仍然未有確實文獻記載，目前可以了解其早期歷史的就只有其南部的[阿豪姆王国對此地區的少量记载](../Page/阿豪姆王国.md "wikilink")。一般认为大多数土著部落群体的祖先从西藏迁移到此。这一地区曾经落入门巴族的门隅王国、不丹、吐蕃等多个国家的势力范围，然而直至印度独立前，大部分地区实际仍为各部落自治。据文献记载，中国清朝时期曾短期允许西藏地区自治，但名义上派遣中央驻藏大臣实行管理。清政府允许藏族贵族由其民族内，以宗教形式自行选择转世领导人授予王爵，但形式上需要受到清朝皇帝的承认。

### 麦克马洪线绘制

[china_india_border.png](https://zh.wikipedia.org/wiki/File:china_india_border.png "fig:china_india_border.png")

[India_disputed_areas_map.svg](https://zh.wikipedia.org/wiki/File:India_disputed_areas_map.svg "fig:India_disputed_areas_map.svg")

1912年，[清政府瓦解](../Page/辛亥革命.md "wikilink")，英国为了确保商业利益，出兵占领了达旺及其以东的今阿鲁纳恰尔邦北部地区，并建立了东北边境特区。1913年，西藏政府[驱逐所有汉族满族](../Page/第一次驅漢事件.md "wikilink")，宣布独立。

1913年至1914年，中英藏三方举行了[西姆拉会谈](../Page/西姆拉會議.md "wikilink")。该会议旨在确立中藏与印藏边界，及中国与西藏的关系。在会上[英国全权特使](../Page/英国.md "wikilink")[威廉·亨利·麦克马洪提出](../Page/亨利·麦克马洪.md "wikilink")[麦克马洪线为西藏](../Page/麦克马洪线.md "wikilink")、西康省及雲南省和[英属印度](../Page/英属印度.md "wikilink")(含今日的緬北克欽邦)之间的边界，该线将[藏南](../Page/藏南.md "wikilink")、[达旺及東段的坎底盆地](../Page/达旺.md "wikilink")(即今日之克欽邦最北的[葡萄縣](../Page/葡萄縣.md "wikilink"))、[江心坡](../Page/江心坡.md "wikilink")(即[恩梅開江和](../Page/恩梅開江.md "wikilink")[邁立開江之間](../Page/邁立開江.md "wikilink"))地區等化为英国控制区。后来當時的[中國代表则因中國政府反对而没有签字](../Page/中國.md "wikilink")，退出了谈判。当日，英藏代表皆签字批准包括麦克马洪线在内的协议。條約共獲英藏兩方認定，中方則拒絕承認。中方的观点是：阿鲁纳恰尔及其正東面的[坎底](../Page/坎底.md "wikilink")、[江心坡地區到雲南一帶是中国和西藏的一部分](../Page/江心坡.md "wikilink")，而[西藏政府不具有外交权](../Page/噶廈.md "wikilink")，因此西姆拉会谈条约只有单方签字（即英国），应视为无效。中国所坚持的是[中印传统线及](../Page/中印關係.md "wikilink")[中緬傳統線](../Page/中緬關係.md "wikilink")。

1954年，印度在该地成立了[东北边境特区](../Page/东北边境特区.md "wikilink")。同年出版的印度官方地图首次把麦克马洪线从1936年以来注明为“未标定界”改为“已定界”。1960年中緬(克欽邦、撣邦、克倫邦和緬甸本部於1948年分別宣布自英國獨立)重新議定邊界，簽訂[中華人民共和國和緬甸聯邦邊界條約](../Page/中華人民共和國和緬甸聯邦邊界條約.md "wikilink")，中國正式明文承認[麦克马洪线缅甸段為中国和缅甸的國界](../Page/麦克马洪线.md "wikilink")。1962年[中國和印度在边境上发生](../Page/中國.md "wikilink")[中印边境战争](../Page/中印边境战争.md "wikilink")。[中國軍隊在战争中大获全胜](../Page/中国人民解放军.md "wikilink")，但又撤退了20公里。戰爭過程中，中國控制了達旺地區。戰後中國軍隊主動退出，印度再次完全獲得該地控制權。

1972年印度将该特区改为[阿鲁纳恰尔中央直辖区](../Page/阿鲁纳恰尔中央直辖区.md "wikilink")，並廣設甘地銅像、醫療診所，普及國民中、小學，並設民族學院及大學。1986年底印度议会两院通过立法将阿鲁纳恰尔中央直辖区升格为邦。翌年，印度正式宣布成立阿鲁纳恰尔邦，意思即為“旭日之邦”。

目前中國政府不承认该邦的合法性，该问题尚在搁置中。現時印度在本邦與鄰國緬甸的邊境佈置了嚴密的軍事防守，加強機場、道路、橋梁、土木工程及自來水、電力、通信基礎建設，以國際節慶規模廣辦當地各民族的傳統節日文化活動。一方面，由於[青藏鐵路的完成](../Page/青藏鐵路.md "wikilink")，中國軍隊進入本區的速度可以加快許多，这使印軍也加強了戒備，嚴陣以待；另一方面，本邦南面[那加兰邦](../Page/那加兰邦.md "wikilink")、[曼尼普爾邦](../Page/曼尼普爾邦.md "wikilink")、[米佐拉姆邦和東面緬甸](../Page/米佐拉姆邦.md "wikilink")[克欽邦北部及野人山以南的](../Page/克欽邦.md "wikilink")[胡康河谷](../Page/胡康河谷.md "wikilink")，[景頗族](../Page/景頗族.md "wikilink")、[那加族](../Page/那加族.md "wikilink")、[欽族武裝獨立集團活躍](../Page/欽族.md "wikilink")，所以外人都要取得特別許可證，才可以進入本區。

多年来，中国政府一直堅持不承认“阿鲁纳恰尔邦”的法律地位，也正因为中印双方都认为“阿鲁纳恰尔”属于本国领土，该地区长期以来成为两国之间一个潜在的冲突点。20世纪80年代中期，印度曾试图在达旺附近建立一个永久性哨所，中国解放軍获悉后也隨即开始在该地区建立自己的哨所，其后印度决定派出第五山地师到达旺进行增援。那次事件差点引发中印之间的一场战争。\[1\]

2006年11月13日，[中國駐印度大使](../Page/中國駐印度大使.md "wikilink")[孫玉璽向印度傳媒表示中方擁有藏南地區主權](../Page/孫玉璽.md "wikilink")，引發[外交風波](../Page/外交.md "wikilink")，阿魯納恰爾邦行政長官要求中國政府召回孫玉璽。

1959年之前，第十四世达赖喇嘛不承认印度对藏南的主权，包括达旺。
然而，2008年6月，[西藏流亡政府的精神領袖](../Page/西藏流亡政府.md "wikilink")[十四世達賴喇嘛在接受](../Page/十四世達賴喇嘛.md "wikilink")《[印度時報](../Page/印度時報.md "wikilink")》採訪時承認印度對阿魯納恰爾邦的主權，將其排除在「[大藏區](../Page/大藏區.md "wikilink")」之外。\[2\]

## 人物

  - [第六世达赖喇嘛](../Page/第六世达赖喇嘛.md "wikilink")[仓央嘉措出生在该地区的](../Page/仓央嘉措.md "wikilink")[达旺](../Page/达旺县.md "wikilink")。

## 行政区划

2012年，阿鲁纳恰尔邦政府把本邦分成17个县进行管辖，並於每县設置區總，以照顧各區居民的需要。2014年7月15日，又正式成立第18个县--[南赛县](../Page/南赛县.md "wikilink")（Namsai）。
<font size=2>                    </font>

| 區名                                                          | 面积（[平方千米](../Page/平方千米.md "wikilink")） | 行政中心                                                 |
| ----------------------------------------------------------- | -------------------------------------- | ---------------------------------------------------- |
| [达旺县](../Page/达旺县.md "wikilink")（Tawang）                    | 2 172                                  | 达旺（Tawang）                                           |
| [西卡门县](../Page/西卡门县.md "wikilink")（West Kameng）             | 7 422                                  | [邦迪拉](../Page/邦迪拉.md "wikilink")（Bomdila）            |
| [东卡门县](../Page/东卡门县.md "wikilink")（East Kameng）             | 4 134                                  | [色帕](../Page/色帕.md "wikilink")（Seppa）                |
| [帕普派尔县](../Page/帕普派尔县.md "wikilink")（Papumpare）             | 3 462                                  | [伊塔那噶](../Page/伊塔那噶.md "wikilink")（Itanagar，也是本邦的首府） |
| [下苏班西里县](../Page/下苏班西里县.md "wikilink")（Lower Subansiri）     | 9 548                                  | [泽洛](../Page/泽洛.md "wikilink")（Ziro）                 |
| [上苏班西里县](../Page/上苏班西里县.md "wikilink")（Upper Subansiri）     | 7 032                                  | [达波日觉](../Page/达波日觉.md "wikilink")（Daporijo）         |
| [西桑朗县](../Page/西桑朗县.md "wikilink")（West Siang）              | 8 033                                  | [阿隆](../Page/阿隆_\(地名\).md "wikilink")（Along）         |
| [东桑朗县](../Page/东桑朗县.md "wikilink")（East Siang）              | 3 895                                  | [巴昔卡](../Page/巴昔卡.md "wikilink")（Pasighat）           |
| [上桑朗县](../Page/上桑朗县.md "wikilink")（Upper Siang）             | 6 590                                  | [营琼](../Page/营琼.md "wikilink")（Yingkiong）            |
| [上迪邦山谷县](../Page/上迪邦山谷县.md "wikilink")（Upper Dibang Valley） | 13 029                                 | [阿尼尼](../Page/阿尼尼.md "wikilink")（Anini）              |
| [下迪邦山谷县](../Page/下迪邦山谷县.md "wikilink")（Lower Dibang Valley） | ？                                      | [罗营](../Page/罗营.md "wikilink")（Roing）                |
| [洛西特县](../Page/洛西特县.md "wikilink")（Lohit）                   | 11 402                                 | [德泽](../Page/德泽.md "wikilink")（Tezu）                 |
| [长朗县](../Page/长朗县.md "wikilink")（Changlang）                 | 4 662                                  | [昌朗](../Page/昌朗.md "wikilink")（Changlang）            |
| [特拉普县](../Page/特拉普县.md "wikilink")（Tirap）                   | 2 362                                  | [孔萨](../Page/孔萨.md "wikilink")（Khonsa）               |
| [库朗库美县](../Page/库朗库美县.md "wikilink")（Kurung Kumey）          | ？                                      | [达泊瑞娇](../Page/达泊瑞娇.md "wikilink")（Daporijo）         |
| [安娇县](../Page/安娇县.md "wikilink")（Anjaw）                     | ？                                      | [哈威](../Page/哈威.md "wikilink")（Hawai）                |
| [南赛县](../Page/南赛县.md "wikilink")（Namsai）                    | ？                                      | [南赛](../Page/南赛.md "wikilink")（Namsai）               |

## 经济

下图显示了统计和计划执行部的阿鲁纳恰尔邦国内生产总值按市价计算的趋势，单位为十亿印度卢比。

| 年    | 国内生产总值（十亿INR） |
| ---- | ------------- |
| 1980 | 1.070         |
| 1985 | 2.690         |
| 1990 | 5.080         |
| 1995 | 11.840        |
| 2000 | 17.830        |
| 2005 | 31.880        |
| 2010 | 65.210        |
| 2014 | 155.880       |

阿鲁纳恰尔邦的国内生产总值估计在2004年为7.06亿美元，2014年为24.6亿美元，农业是当地经济的主要支柱。这里种植的作物有水稻，玉米，小米，小麦，豆类，甘蔗，姜和油籽等等。Jhum，当地用于种植的术语，在部落群体中广泛实行，近年来其他产业的逐渐增长，使得农业的相对比重下降。阿鲁纳恰尔邦有近61,000平方公里的森林，森林产品是经济下一个最重要的部分。阿鲁纳恰尔邦也是园艺和果园的理想选择。其主要行业有水稻，水果保鲜加工，手工工艺品。锯木厂和胶合板行业受法律禁止，但阿鲁纳恰尔邦有很多锯木厂。

阿鲁纳恰尔邦占印度未开发水电潜力的很大一部分。
2008年，阿鲁纳恰尔邦政府与各公司签署了许多谅解备忘录，规划了大约42个水力发电计划，将发电量超过27,000兆瓦。预计将在2009年4月开始建设1万至12,000兆瓦的西洋水电工程。

## 地理

[Mountains_of_Arunachal_Pradesh.jpg](https://zh.wikipedia.org/wiki/File:Mountains_of_Arunachal_Pradesh.jpg "fig:Mountains_of_Arunachal_Pradesh.jpg")

阿鲁纳恰尔邦的面积为83,743[平方千米](../Page/平方千米.md "wikilink")，是现时印度东北部面积最大的行政区。现有人口100多万。首府在[伊塔那噶](../Page/伊塔那噶.md "wikilink")（Itanagar）。

由於阿鲁纳恰尔邦位於[喜馬拉雅山脈旁](../Page/喜馬拉雅山脈.md "wikilink")，所以當地多山。2005年夏季，由於[印度持續發生暴雨](../Page/印度.md "wikilink")，使安嬌和勞哈特區都出現大規模[山泥傾瀉](../Page/山泥傾瀉.md "wikilink")，造成嚴重的傷亡。

## 民族

阿鲁纳恰尔邦的人口有超越100万人。根据当地政府统计，本地有82种的不同部族，絕大部分是蒙古人種漢藏語系。主要的部族包括了信仰[泛灵论的](../Page/泛灵论.md "wikilink")[阿迪族](../Page/阿迪族.md "wikilink")、[阿加族](../Page/阿加族.md "wikilink")、[米济族](../Page/米济族.md "wikilink")、[蔷薇族等等](../Page/薔薇族_\(民族\).md "wikilink")，佔了大多數民族和人口。其他部份為信仰[基督新教的](../Page/基督新教.md "wikilink")[浸信会以及](../Page/浸信会.md "wikilink")[羅馬天主教的](../Page/羅馬天主教.md "wikilink")[傈僳族](../Page/傈僳族.md "wikilink")、[日旺族](../Page/日旺族.md "wikilink")
、[景頗族和](../Page/景頗族.md "wikilink")[那加人等蒙古人種漢藏語系](../Page/那加人.md "wikilink")。部份為藏族支系的信仰[藏传佛教的](../Page/藏传佛教.md "wikilink")[门巴族](../Page/门巴族.md "wikilink")、[舍度苯族](../Page/舍度苯族.md "wikilink")、[珞巴族以及](../Page/珞巴族.md "wikilink")[康巴人](../Page/康巴人.md "wikilink")；[南传佛教的](../Page/南传佛教.md "wikilink")[坎底傣](../Page/坎底傣.md "wikilink")、[帕基人](../Page/帕基人.md "wikilink")。信仰[印度教的部族是](../Page/印度教.md "wikilink")[诺特族](../Page/诺特族.md "wikilink")，诺特族是那加人的一个部族。

### 宗教

## 语言

[印地语和](../Page/印地语.md "wikilink")[英语以及部族语言为](../Page/英语.md "wikilink")[官方语言](../Page/官方语言.md "wikilink")，但是當地原住民的语言大多數為[汉藏语系中的](../Page/汉藏语系.md "wikilink")[藏缅语族](../Page/藏缅语族.md "wikilink")，當中包括在2007年發現、2010年10月確定存在的[克羅語](../Page/克羅語.md "wikilink")。根据当地政府统计，本地的方言计有超过40种。[英語及](../Page/英語.md "wikilink")[藏語为各原住民部落溝通的](../Page/藏語.md "wikilink")[通用语](../Page/通用语.md "wikilink")，且相鄰南部的[那加蘭邦也是](../Page/那加蘭邦.md "wikilink")[蒙古人種](../Page/蒙古人種.md "wikilink")、[漢藏語系](../Page/漢藏語系.md "wikilink")，官方語言是[英語](../Page/英語.md "wikilink")。

## 参考文献

## 外部链接

  -
## 参见

  - [中印边界问题](../Page/中印边界问题.md "wikilink")
      - [藏南地區](../Page/藏南地區.md "wikilink")
      - [麦克马洪线](../Page/麦克马洪线.md "wikilink")
      - [中印边境战争](../Page/中印边境战争.md "wikilink")

{{-}}

[邦](../Category/印度的邦和中央直辖区.md "wikilink")
[阿鲁纳恰尔邦](../Category/阿鲁纳恰尔邦.md "wikilink")
[Category:中印邊界爭議](../Category/中印邊界爭議.md "wikilink")

1.  [资料：何谓“阿鲁纳恰尔邦”](http://news.qq.com/a/20090405/000269.htm)
2.