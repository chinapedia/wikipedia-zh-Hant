《**陀槍師姐III**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司製作的時裝警匪](../Page/電視廣播有限公司.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，全劇共32集，監製[鄺業生](../Page/鄺業生.md "wikilink")。此劇為《[陀槍師姐](../Page/陀槍師姐_\(電視劇\).md "wikilink")》系列第3輯。

## 演員表

### 陳家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p>陳大生</p></td>
<td><p>王二妹之夫<br />
陳小生之兄<br />
陳三元、陳四喜、陳五福之父<br />
程守忠、何金梅之亲家<br />
程峰、童家辉之岳父<br />
程莎莎、程嘉嘉之外公<br />
（已過世）</p></td>
</tr>
<tr class="odd">
<td><p><strong>陳小生</strong></p></td>
<td><p>特遣隊高級督察、被喻為「警界神槍手」<br />
王二妹之叔仔<br />
陳三元、陳四喜、陳五福之二叔<br />
陳大生之弟<br />
衛英姿男友<br />
程莎莎、程嘉嘉之二叔公<br />
朱少芬、吳英明、鄭吉祥上司</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朱咪咪.md" title="wikilink">朱咪咪</a></p></td>
<td><p>|<strong>王二妹</strong></p></td>
<td><p><strong>二妹姐</strong><br />
<a href="../Page/#生記茶餐廳.md" title="wikilink">生記茶餐廳老闆娘</a><br />
陈大生之妻<br />
陳小生之大嫂<br />
陳三元、陳四喜、陳五福之母<br />
程守忠、何金梅之亲家<br />
程峰、童家輝之岳母<br />
程莎莎、程嘉嘉之外婆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/滕麗名.md" title="wikilink">滕麗名</a></p></td>
<td><p><strong>陳三元</strong></p></td>
<td><p>陳大生、王二妹之大女<br />
參見<a href="../Page/#程家.md" title="wikilink">程家</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁雪湄.md" title="wikilink">梁雪湄</a></p></td>
<td><p>陈四喜</p></td>
<td><p>陈大生、王二妹之二女<br />
陈小生之侄女<br />
陈三元之妹<br />
陈五福之姐<br />
童家辉之妻<br />
程峰之姨仔<br />
程莎莎、程嘉嘉之姨</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邓一君.md" title="wikilink">邓一君</a></p></td>
<td><p>童家辉</p></td>
<td><p><strong>神童</strong><br />
常驻上海做生意<br />
陈四喜之夫<br />
陈三元之妹夫<br />
陈五福之姐夫<br />
陈大生、王二妹之二女婿<br />
陈小生之侄女婿<br />
程峰之连襟<br />
程莎莎、程嘉嘉之姨丈</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/姚樂怡.md" title="wikilink">姚樂怡</a></p></td>
<td><p>陳五福</p></td>
<td><p>電視台女明星<br />
陳大生、王二妹之三女<br />
陳小生之三侄女<br />
陳三元、陳四喜之妹<br />
歐志強女友<br />
程莎莎、程嘉嘉之二阿姨</p></td>
</tr>
</tbody>
</table>

### 程家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楚原.md" title="wikilink">楚　原</a></p></td>
<td><p>程守忠</p></td>
<td><p>何金梅之夫<br />
程峰之父<br />
陳三元之老爺<br />
程莎莎、程嘉嘉之爺爺</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬海倫.md" title="wikilink">馬海倫</a></p></td>
<td><p>何金梅</p></td>
<td><p>程守忠之妻<br />
程峰之母<br />
陳三元之奶奶<br />
程莎莎、程嘉嘉之嬤嬤</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a></strong></p></td>
<td><p><strong>程　峰</strong></p></td>
<td><p><a href="../Page/西九龍總區.md" title="wikilink">西九龍總區</a><a href="../Page/重案組.md" title="wikilink">重案組</a><a href="../Page/高級督察.md" title="wikilink">高級督察</a><br />
程守忠、何金梅之子<br />
陳三元之夫<br />
程莎莎、程嘉嘉之父<br />
王二妹之大女婿<br />
歐志強、鮑頂天、永康、大兵上司</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/滕麗名.md" title="wikilink">滕麗名</a></strong></p></td>
<td><p><strong>陳三元</strong></p></td>
<td><p>西九龍總部交通總部交通部<a href="../Page/見習督察.md" title="wikilink">見習督察</a>，于第30集升级为<a href="../Page/督察_(香港警察).md" title="wikilink">督察</a><br />
程峰之妻<br />
程守忠、何金梅之媳<br />
程莎莎、程嘉嘉之母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/成珈瑩.md" title="wikilink">成珈瑩</a></p></td>
<td><p>程莎莎</p></td>
<td><p>程峰、陳三元之女<br />
程守忠、何金梅之孫女<br />
陳大生、王二妹之外孫女<br />
陈小生之侄孙女<br />
童家輝、陳四喜、陳五福之姨甥女<br />
程嘉嘉之孖生家姐</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬俊榮.md" title="wikilink">馬俊榮</a></p></td>
<td><p>程嘉嘉</p></td>
<td><p>程峰、陳三元之子<br />
程守忠、何金梅之孫子<br />
陳大生、王二妹之外孫子<br />
陈小生之侄孙<br />
童家輝、陳四喜、陳五福之姨甥子<br />
程莎莎之孖生細佬<br />
於第24集被郭錦安殺死</p></td>
</tr>
</tbody>
</table>

### 衛家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏萍.md" title="wikilink">夏　萍</a></p></td>
<td><p>蔡玉蘭</p></td>
<td><p>衛英雄前岳母<br />
衛英姿之婆婆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏雨_(香港).md" title="wikilink">夏　雨</a></p></td>
<td><p><strong>衛英雄</strong></p></td>
<td><p>雄記車房老闆<br />
衛英姿之父<br />
王素心之夫<br />
蔡玉蘭前女婿</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/康華.md" title="wikilink">康　華</a></p></td>
<td><p><strong>王素心</strong></p></td>
<td><p><strong>心姐</strong><br />
心意吧老闆娘<br />
衛英雄之妻</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蔡少芬.md" title="wikilink">蔡少芬</a></strong></p></td>
<td><p><strong>衛英姿</strong></p></td>
<td><p><strong>竹篙精、阿姿、囡囡</strong><br />
WPC53289西九龍交通部警員→特遣隊<br />
衛英雄之女<br />
蔡玉蘭之外孫女<br />
榮兆佳前女友<br />
陳小生女友<br />
陳三元下屬兼好友</p></td>
</tr>
</tbody>
</table>

### 榮福堂

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/江漢.md" title="wikilink">江　漢</a></p></td>
<td><p>榮廣蔭</p></td>
<td><p>榮福堂創辦人<br />
榮兆祖、榮兆佳之父</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李麗麗.md" title="wikilink">李麗麗</a></p></td>
<td><p>黎淑娴</p></td>
<td><p>榮廣蔭第二任妻子<br />
榮兆佳之母<br />
榮兆祖之繼母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張鴻昌.md" title="wikilink">張鴻昌</a></p></td>
<td><p>榮兆祖</p></td>
<td><p><strong>Joe</strong><br />
榮福堂大公子<br />
榮廣蔭之長子<br />
黎淑娴之繼子<br />
榮兆佳同父異母之兄<br />
針對榮兆佳</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林其欣.md" title="wikilink">林其欣</a></p></td>
<td><p>李芷菁</p></td>
<td><p>榮兆祖之妻<br />
榮兆佳之大嫂<br />
針對榮兆佳</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔡子健.md" title="wikilink">蔡子健</a></p></td>
<td><p><strong>榮兆佳</strong></p></td>
<td><p><strong>Winson</strong><br />
榮福堂二公子<br />
榮廣蔭次子<br />
黎淑娴之子<br />
榮兆祖同父異母之弟<br />
被榮兆祖、李芷菁常實針對<br />
衛英姿、張曼琪之前男友<br />
被張曼琪得知做假帐后遭要挟<br />
殺死張曼琪之凶手<br />
於第30集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/趙靜儀.md" title="wikilink">趙靜儀</a></p></td>
<td><p>張曼琪</p></td>
<td><p><strong>Maggie</strong><br />
榮兆佳之前女友兼助手<br />
要挟榮兆佳与其结婚<br />
於第28集被榮兆佳错手推至玻璃台身亡并埋尸</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何啟南.md" title="wikilink">何啟南</a></p></td>
<td><p>Henry</p></td>
<td><p>榮兆佳助手</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林影红.md" title="wikilink">林影红</a></p></td>
<td><p>Amy</p></td>
<td><p>榮福堂接待员</p></td>
</tr>
</tbody>
</table>

### 西九龍交通部

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/滕麗名.md" title="wikilink">滕麗名</a></strong></p></td>
<td><p><strong>陳三元</strong></p></td>
<td><p>參見<a href="../Page/#程家.md" title="wikilink">程家</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蔡少芬.md" title="wikilink">蔡少芬</a></strong></p></td>
<td><p><strong>衛英姿</strong></p></td>
<td><p>參見<a href="../Page/#衛家.md" title="wikilink">衛家</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅莽.md" title="wikilink">羅　莽</a></p></td>
<td><p>潘文彬</p></td>
<td><p><strong>牛佬</strong><br />
交通部<a href="../Page/警長.md" title="wikilink">警長</a><br />
李Sir、陳三元下屬<br />
潘志明之父<br />
曾美美前夫</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麥嘉倫.md" title="wikilink">麥嘉倫</a></p></td>
<td><p>Billy</p></td>
<td><p>交通部警員<br />
李Sir、陳三元下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關菁.md" title="wikilink">關　菁</a></p></td>
<td><p>華　叔</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葉振聲.md" title="wikilink">葉振聲</a></p></td>
<td><p>黑　仔</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/華忠男.md" title="wikilink">華忠男</a></p></td>
<td><p>李Sir</p></td>
<td><p>交通部<a href="../Page/總督察.md" title="wikilink">總督察</a><br />
陳三元、衛英姿、潘文彬、華叔、黑仔上司</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/虞天偉.md" title="wikilink">虞天偉</a></p></td>
<td><p>大偈文</p></td>
<td><p>維修保養摩托車</p></td>
</tr>
</tbody>
</table>

### 西九龍重案組

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a></strong></p></td>
<td><p><strong>程　峰</strong></p></td>
<td><p>參見<a href="../Page/#程家.md" title="wikilink">程家</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李子奇.md" title="wikilink">李子奇</a></p></td>
<td><p>何一正</p></td>
<td><p>重案組<a href="../Page/警司.md" title="wikilink">警司</a><br />
程峰、鮑頂天、歐志強、大兵、永康上司</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李成昌.md" title="wikilink">李成昌</a></p></td>
<td><p>鮑頂天</p></td>
<td><p><strong>頂爺</strong><br />
西九龍重案組警長<br />
何一正、程峰下屬<br />
鮑國平之兄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邵傳勇.md" title="wikilink">邵傳勇</a></p></td>
<td><p>歐志強</p></td>
<td><p><strong>花灑</strong><br />
<a href="../Page/警員_(香港).md" title="wikilink">警員</a><br />
何一正、程峰下屬<br />
陳五福男友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐瑞偉.md" title="wikilink">歐瑞偉</a></p></td>
<td><p>黎Sir</p></td>
<td><p><strong>大兵</strong><br />
警員<br />
何一正、程峰下屬<br />
前華籍英兵</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/周暘.md" title="wikilink">周　暘</a></p></td>
<td><p>永　康</p></td>
<td><p>警員<br />
康仔<br />
何一正、程峰下屬</p></td>
</tr>
</tbody>
</table>

### 特遣隊

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p><strong>陳小生</strong></p></td>
<td><p>參見<a href="../Page/#陳家.md" title="wikilink">陳家</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蔡少芬.md" title="wikilink">蔡少芬</a></strong></p></td>
<td><p><strong>衛英姿</strong></p></td>
<td><p>參見<a href="../Page/#衛家.md" title="wikilink">衛家</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魯振順.md" title="wikilink">魯振順</a></p></td>
<td><p>梁Sir</p></td>
<td><p>警司<br />
陳小生、衛英姿、鄭吉祥、朱少芬、吳英明上司</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李岡龍.md" title="wikilink">李岡龍</a></p></td>
<td><p>鄭吉祥</p></td>
<td><p><strong>吉仔</strong><br />
警長<br />
梁Sir、陳小生下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬蹄露.md" title="wikilink">馬蹄露</a></p></td>
<td><p>朱少芬</p></td>
<td><p><strong>Rita</strong><br />
警員<br />
梁Sir、陳小生下屬<br />
警局四大美人之一</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蒲茗藍.md" title="wikilink">蒲茗藍</a></p></td>
<td><p>吳英明</p></td>
<td><p><strong>波子</strong><br />
警員<br />
梁Sir、陳小生下屬</p></td>
</tr>
</tbody>
</table>

### 其他警察

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **關係／暱稱** |
| [黃鳳瓊](../Page/黃鳳瓊.md "wikilink") | Cindy  | 四大美人之一    |
| [黃梓暐](../Page/黃梓暐.md "wikilink") | Rosa   |           |
| [陳楚翹](../Page/陳楚翹.md "wikilink") | Mimi   |           |

### 雄記車房

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏雨.md" title="wikilink">夏　雨</a></p></td>
<td><p>衛英雄</p></td>
<td><p>參見<a href="../Page/#衛家.md" title="wikilink">衛家</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/艾威.md" title="wikilink">艾　威</a></p></td>
<td><p>鄧劍豪</p></td>
<td><p><strong>豪Dee</strong><br />
衛英雄之徒兼好兄弟<br />
雄記車房技工<br />
冼翠兒男友<br />
於第30集被李大軍槍殺身亡</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杜大偉.md" title="wikilink">杜大偉</a></p></td>
<td><p>宋子傑</p></td>
<td><p>師傅仔<br />
鄧劍豪之徒<br />
雄記車房技工</p></td>
</tr>
</tbody>
</table>

### 心意吧

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/康華.md" title="wikilink">康　華</a></p></td>
<td><p>王素心</p></td>
<td><p>參見<a href="../Page/#衛家.md" title="wikilink">衛家</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Joe_Junior.md" title="wikilink">Joe Junior</a></p></td>
<td><p>張　標</p></td>
<td><p><strong>Auntie</strong><br />
心意吧調酒師</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊證樺.md" title="wikilink">楊證樺</a></p></td>
<td><p>周耀祥</p></td>
<td><p><strong>斧頭</strong><br />
心意吧員工</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伍燕妮.md" title="wikilink">伍燕妮</a></p></td>
<td><p>Honey</p></td>
<td><p>心意吧啤酒推銷員</p></td>
</tr>
</tbody>
</table>

### 生記茶餐廳

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭卓樺.md" title="wikilink">郭卓樺</a></p></td>
<td><p>歐健全</p></td>
<td><p><strong>口水全</strong><br />
前生記茶餐廳侍應<br />
郭錦安表弟</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳勉良.md" title="wikilink">陳勉良</a></p></td>
<td><p>師傅昌</p></td>
<td><p>生記茶餐廳侍應</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃德斌.md" title="wikilink">黃德斌</a></p></td>
<td><p><strong>郭錦安</strong></p></td>
<td><p>生記茶餐廳廚師<br />
口水全表哥<br />
參見<a href="../Page/#賣淫（第13－25集）.md" title="wikilink">賣淫（第13－25集）</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 東興社（第1－6集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳豪.md" title="wikilink">陳　豪</a></p></td>
<td><p><strong>陳耀揚</strong></p></td>
<td><p>旺角東興社話事人之一<br />
與麥志新不合<br />
Rambo、了哥、黃玉基之老大<br />
殺死何世龍、Rambo、麥志新、蔣天林之凶手<br />
於第6集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李煒棋.md" title="wikilink">李煒棋</a></p></td>
<td><p>麥志新</p></td>
<td><p><strong>Mark</strong><br />
旺角東興社話事人之一<br />
與陳耀揚不合<br />
於第5集被陳耀揚所害，煞車失靈車禍身亡</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李家鼎.md" title="wikilink">李家鼎</a></p></td>
<td><p>蔣天林</p></td>
<td><p>旺角東興社龍頭<br />
於第6集被陳耀揚槍殺身亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麥子雲.md" title="wikilink">麥子雲</a></p></td>
<td><p>黃玉基</p></td>
<td><p><strong>泊車基</strong><br />
陳耀掦手下，利用泊車之便，複印車匙模偷車<br />
於第3集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朱永棠.md" title="wikilink">朱永棠</a></p></td>
<td><p>了　哥</p></td>
<td><p>旺角東興社成員<br />
陳耀揚手下<br />
於第6集被捕</p></td>
</tr>
</tbody>
</table>

### 偽鈔（第7－12集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王維德.md" title="wikilink">王維德</a></p></td>
<td><p>林占美</p></td>
<td><p><strong>Jimmy</strong><br />
大發財務老闆<br />
偽鈔集團首腦<br />
於第12集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃文標.md" title="wikilink">黃文標</a></p></td>
<td><p>張永泰</p></td>
<td><p><strong>泰臣</strong><br />
大發財務職員<br />
偽鈔集團成員<br />
林占美手下<br />
於第12集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/欧阳国璋.md" title="wikilink">欧阳国璋</a></p></td>
<td><p>何安国</p></td>
<td><p><strong>盲國</strong><br />
林占美手下</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧汝超.md" title="wikilink">鄧汝超</a></p></td>
<td><p>林繼榮</p></td>
<td><p><strong>司機榮</strong><br />
好彩運輸公司貨車司機<br />
偽鈔集團成員<br />
於第12集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊明.md" title="wikilink">楊　明</a></p></td>
<td><p>潘志明</p></td>
<td><p><strong>Jason</strong><br />
潘文彬、曾美美之子<br />
前雄記車行員工<br />
曾暗恋衛英姿<br />
行使假鈔<br />
於第12集自首</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/周家怡.md" title="wikilink">周家怡</a></p></td>
<td><p>Co Co</p></td>
<td><p>潘志明女友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳狄克.md" title="wikilink">陳狄克</a></p></td>
<td><p>貴利堅</p></td>
<td><p>Co Co債主<br />
貴利成之兄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳榮峻.md" title="wikilink">陳榮峻</a></p></td>
<td><p>黃裕德</p></td>
<td><p>潘志明繼父<br />
曾美美之夫</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇恩磁.md" title="wikilink">蘇恩磁</a></p></td>
<td><p>曾美美</p></td>
<td><p>潘志明之母<br />
黃裕德之妻<br />
潘文彬之前妻</p></td>
</tr>
</tbody>
</table>

### 賣淫（第13－25集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃德斌.md" title="wikilink">黃德斌</a></p></td>
<td><p><strong>郭錦安</strong></p></td>
<td><p><a href="../Page/#生記茶餐廳.md" title="wikilink">生記茶餐廳廚師</a><br />
張紅麗男友<br />
鳳姐殺手<br />
袭击吴淑珍、郑少娟、林玉莲<br />
殺害張紅麗、梁永勝、程嘉嘉之凶手<br />
代替鮑國平（翁文成）殺害工具<br />
於第24集被拘捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/程小龍.md" title="wikilink">程小龍</a></p></td>
<td><p>貴利成</p></td>
<td><p>高利貸及賣淫集團首腦<br />
張紅麗債主<br />
貴利堅之弟<br />
於第16集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/游飈.md" title="wikilink">游　飈</a></p></td>
<td><p>吹　水</p></td>
<td><p>貴利成手下<br />
於第16集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邵卓堯.md" title="wikilink">邵卓堯</a></p></td>
<td><p>火　爆</p></td>
<td><p>貴利成手下<br />
於第16集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周凱珊.md" title="wikilink">周凱珊</a></p></td>
<td><p>張紅麗</p></td>
<td><p>大陸美專畢業生<br />
周光明之母，丈夫於地盤意外中去世，生前借了高利貸，故被貴利成強逼賣淫還債<br />
曾為北姑（大陸來香港賣淫者）<br />
<a href="../Page/#生記茶餐廳.md" title="wikilink">生記茶餐廳洗碗碟工人</a><br />
郭錦安女友<br />
於第19集被梁永勝強姦后，被郭錦安殺害</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/姚紹忠.md" title="wikilink">姚紹忠</a></p></td>
<td><p>周光明</p></td>
<td><p><strong>明仔</strong><br />
張紅麗之子</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古明華.md" title="wikilink">古明華</a></p></td>
<td><p>梁永勝</p></td>
<td><p><strong>賣魚勝</strong><br />
內地專賣團客，張紅麗舊客<br />
強姦張紅麗<br />
於第21集被郭錦安殺害</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/駱達華.md" title="wikilink">駱達華</a></p></td>
<td><p>鮑國平</p></td>
<td><p>鮑頂天之弟<br />
患有<a href="../Page/解離性人格疾患.md" title="wikilink">人格分裂</a><br />
面具色魔<br />
獄中教徒<br />
郭錦安筆友<br />
利用福尔摩斯密码教唆郭錦安杀害程嘉嘉<br />
於第25集跳樓身亡<br />
参见<a href="../Page/陀枪师姐II.md" title="wikilink">陀枪师姐II</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黄月明.md" title="wikilink">黄月明</a></p></td>
<td><p>吴淑珍</p></td>
<td><p>北姑（大陸來香港賣淫者）<br />
郑少娟、林玉莲好友<br />
於第15集被郭錦安袭击</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宋芝龄.md" title="wikilink">宋芝龄</a></p></td>
<td><p>郑少娟</p></td>
<td><p>北姑（大陸來香港賣淫者）<br />
吴淑珍、林玉莲好友<br />
於第17集被郭錦安袭击</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/钟晓莹.md" title="wikilink">钟晓莹</a></p></td>
<td><p>林玉莲</p></td>
<td><p>北姑（大陸來香港賣淫者）<br />
吴淑珍、林玉莲好友<br />
於第18集被郭錦安袭击</p></td>
</tr>
</tbody>
</table>

### 械劫（第26－32集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王俊棠.md" title="wikilink">王俊棠</a></p></td>
<td><p>李大軍</p></td>
<td><p>械劫集團首領<br />
冼翠兒舊相好<br />
殺死冼翠兒、鄧劍豪之凶手<br />
於第32集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邱萬城.md" title="wikilink">邱萬城</a></p></td>
<td><p>解向東</p></td>
<td><p>解向南之兄<br />
械劫集團同黨<br />
於第32集被程峰槍殺身亡</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林敬剛.md" title="wikilink">林敬剛</a></p></td>
<td><p>解向南</p></td>
<td><p>解向東之弟<br />
械劫集團同黨<br />
於第32集被鮑頂天槍殺身亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾守明.md" title="wikilink">曾守明</a></p></td>
<td><p>阿　賢</p></td>
<td><p>械劫集團同黨<br />
於第32集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭家生.md" title="wikilink">鄭家生</a></p></td>
<td><p>阿　武</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁健平.md" title="wikilink">梁健平</a></p></td>
<td><p>阿　傑</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馮曉文.md" title="wikilink">馮曉文</a></p></td>
<td><p>冼翠兒</p></td>
<td><p>夜總會媽媽桑<br />
冼家亮之母<br />
李大軍舊相好<br />
鄧劍豪女友<br />
第30集被李大軍槍殺身亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李厚賢.md" title="wikilink">李厚賢</a></p></td>
<td><p>冼家亮</p></td>
<td><p>冼翠兒之子</p></td>
</tr>
</tbody>
</table>

### 其他演員

|                                  |           |           |
| -------------------------------- | --------- | --------- |
| **演員**                           | **角色**    | **關係／暱稱** |
| [馬國明](../Page/馬國明.md "wikilink") | 銀行職員+拆彈專家 |           |

## 外部連結

  - [無綫電視官方網頁 -
    陀槍師姐III](https://web.archive.org/web/20081231201348/http://tvcity.tvb.com/drama/armed3/story/index.html)
  - [《陀槍師姐III》 GOTV
    第1集重溫](https://web.archive.org/web/20140222162347/http://gotv.tvb.com/programme/102367/149262/)

## 電視節目的變遷

[分類:香港交通部題材作品](../Page/分類:香港交通部題材作品.md "wikilink")

[Category:2001年無綫電視劇集](../Category/2001年無綫電視劇集.md "wikilink")
[3](../Category/陀槍師姐系列.md "wikilink")
[Category:無綫電視2000年代背景劇集](../Category/無綫電視2000年代背景劇集.md "wikilink")
[Category:電視劇續集](../Category/電視劇續集.md "wikilink")
[Category:多重人格題材電視劇](../Category/多重人格題材電視劇.md "wikilink")