**陆婉珍**（），祖籍[上海](../Page/上海.md "wikilink")，出生于[天津](../Page/天津.md "wikilink")。[中国](../Page/中国.md "wikilink")[分析化学和](../Page/分析化学.md "wikilink")[石油化学专家](../Page/石油化学.md "wikilink")，[中国科学院院士](../Page/中国科学院院士.md "wikilink")，在分析化学对中国的[石油开采](../Page/石油.md "wikilink")、加工中的应用有很重要的贡献。建立了一大批石油产品的分析方法，尤其是在[色谱分析](../Page/色谱.md "wikilink")、[光谱分析方面有一定的突破](../Page/光谱.md "wikilink")。此外，在[近红外光谱仪的研制以及应用方面也有很大的成绩](../Page/近红外光谱仪.md "wikilink")。

## 生平

陆婉珍1924年出生于天津，其她的父亲当时在天津[塘沽的一家纺织厂担任总工程师](../Page/塘沽.md "wikilink")。后来举家迁往[常州](../Page/常州.md "wikilink")，小学就是在这里上的（常州女子师范附小）。1936年到[苏州女子师范学校读初中](../Page/苏州.md "wikilink")。1937年爆发[抗日战争](../Page/抗日战争.md "wikilink")，由于家乡被日军占领，全家来到[重庆](../Page/重庆.md "wikilink")，她在这里读完了中学（重庆南开中学）。1946年从重庆[國立中央大學](../Page/國立中央大學_\(南京\).md "wikilink")[化学工程系毕业](../Page/南京工业大学.md "wikilink")，之后随家人到上海，在一家印染厂工作。1947年前往[美国留学](../Page/美国.md "wikilink")，1949年获得美国[伊利诺伊州立大学化学硕士学位](../Page/伊利诺伊州立大学.md "wikilink")；随后在[俄亥俄州立大学主修](../Page/俄亥俄州立大学.md "wikilink")[无机化学](../Page/无机化学.md "wikilink")，1951年获哲学博士学位，1952年-1953年在美国[西北大学作](../Page/西北大学.md "wikilink")[博士后](../Page/博士后.md "wikilink")。1953年在美国一家生产[玉米产品的公司从事分析工作](../Page/玉米.md "wikilink")。

1955年，她与丈夫[闵恩泽院士得到了一个去](../Page/闵恩泽.md "wikilink")[香港工作的机会](../Page/香港.md "wikilink")，由于当时美国政府还不允许中国留学生离境，因此借此离开美国，回到了中国。到中国后，她就为当时的石油工业部筹划建立石油炼制研究所（现在的[石油化工科学研究院](../Page/石油化工科学研究院.md "wikilink")），从此一直从事分析工作。并在1991年选为[中国科学院院士](../Page/中国科学院.md "wikilink")。

因病于2015年11月17日2时在北京逝世\[1\]。

陆婉珍的丈夫[闵恩泽是中国著名的石油化工](../Page/闵恩泽.md "wikilink")[催化剂化学家](../Page/催化剂.md "wikilink")，被誉为“中国催化剂之父”，获2007年[国家最高科学技术奖](../Page/国家最高科学技术奖.md "wikilink")，是中国科学院院士、中国工程院院士和第三世界科学院院士。

## 参考文献

### 引用

### 来源

  - 《陆婉珍论文集》编写组：《陆婉珍论文集》，化学工业出版社，2004年. ISBN 7-5025-5949-3.

{{-}}

[Category:中华人民共和国石油化学家](../Category/中华人民共和国石油化学家.md "wikilink")
[Category:中华人民共和国分析化学家](../Category/中华人民共和国分析化学家.md "wikilink")
[Category:中国科学院化学部院士](../Category/中国科学院化学部院士.md "wikilink")
[工L](../Category/中央大学校友.md "wikilink")
[Category:南京工业大学校友](../Category/南京工业大学校友.md "wikilink")
[0L](../Category/东南大学校友.md "wikilink")
[Category:南京大学校友](../Category/南京大学校友.md "wikilink")
[Category:重庆市南开中学校友](../Category/重庆市南开中学校友.md "wikilink")
[Category:上海科学家](../Category/上海科学家.md "wikilink")
[Category:天津科学家](../Category/天津科学家.md "wikilink")
[W](../Category/陆姓.md "wikilink")

1.