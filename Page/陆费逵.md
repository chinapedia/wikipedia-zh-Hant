**陆费逵**（），[复姓](../Page/复姓.md "wikilink")**陆费**，名**逵**，字**伯鸿**，号**少沧**，幼名**沧生**，笔名有**飞**、**冥飞**、**白**等。原籍[浙江](../Page/浙江省.md "wikilink")[桐乡](../Page/桐乡县_\(明朝\).md "wikilink")，生于[陕西](../Page/陕西.md "wikilink")[汉中](../Page/汉中.md "wikilink")。中国近代著名[教育家](../Page/教育家.md "wikilink")、[出版家](../Page/出版家.md "wikilink")，[中華書局創辦人](../Page/中華書局.md "wikilink")。弟[陸費堭](../Page/陸費堭.md "wikilink")(1887-?)，字仲炘。

## 生平

四世祖[陸費墀](../Page/陸費墀.md "wikilink")，曾任[四庫總校官](../Page/四庫.md "wikilink")，因對書中“違礙”之處失察，被斥責免官，旋即憂憤死。父[陸費芷滄為各地大員](../Page/陸費芷滄.md "wikilink")[幕僚](../Page/幕僚.md "wikilink")，母为[李鸿章侄女](../Page/李鸿章.md "wikilink")，颇识诗书。陆费逵生于[清](../Page/清朝.md "wikilink")[光绪十二年八月二十日](../Page/光绪.md "wikilink")。1908年開始，在同為浙人的舊交[張元濟創辦的](../Page/張元濟.md "wikilink")[商務印書館中任國文部編輯](../Page/商務印書館.md "wikilink")。

1909年陆费逵建议整理汉字，主张简化，提倡[白话文](../Page/白话文.md "wikilink")。1912年1月創立[中華書局於](../Page/中華書局.md "wikilink")[上海](../Page/上海.md "wikilink")，與商務分庭抗禮，展開新制教科書大戰。於戰火烽煙中先後出版《中華大字典》、《辭海》等書。

1941年7月9日，陆费逵病逝于[香港](../Page/香港.md "wikilink")。

## 名言

  - 不正直、則時時畏人知，精神之苦痛無窮。

<!-- end list -->

  - 人有以失機自憾者，不知人苟立志於正直，則決不為世間所閒卻；虛心平氣以觀世，凡用人者，孰不願得正直者而任之乎？常見有樸訥之人，毫無特長，僅以正直之故，得人信用，致成大業。而才華卓越之人，身敗名裂，甚或陷身囹圄，皆不正直之故也。

[lb陸費](../Category/中華民國教育家.md "wikilink")
[lb陸費](../Category/汉中人.md "wikilink")
[lb陸費](../Category/桐鄉人.md "wikilink")
[K](../Category/陆费姓.md "wikilink")