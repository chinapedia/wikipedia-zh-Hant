朝鲜半岛的**陇西李氏**（농서이씨）的后裔是出自[中国](../Page/中国.md "wikilink")[辽宁的](../Page/辽宁.md "wikilink")[铁岭李氏](../Page/铁岭李氏.md "wikilink")，为明末辽东总兵[李成梁和](../Page/李成梁.md "wikilink")[李如松](../Page/李如松.md "wikilink")、[李如梅父子的后代](../Page/李如梅.md "wikilink")。据《[明史](../Page/明史.md "wikilink")》记载，李成梁的高祖李英自朝鲜内附[明朝](../Page/明朝.md "wikilink"),授世铁岭卫指挥佥事,遂家焉。明史记载李英从朝鲜半岛迁居到[铁岭](../Page/铁岭.md "wikilink"),但李英的民族有朝鲜人说，蒙古人说，女真人说，汉人说等争议。但实际是祖先来自新罗人。
李成梁一支自称[陇西李氏之后](../Page/陇西李氏.md "wikilink")，祖上于唐朝末年为避乱迁入朝鲜\[1\]，经数代后于明朝洪武年间内附[明朝](../Page/明朝.md "wikilink")。\[2\]

## 在中国的先世

，
铁岭李氏是明朝初年渡鸭绿江而来的朝鲜人，更有朝鲜人认为这一支家族是出自[星州李氏](../Page/星州李氏.md "wikilink")，但日本学者[园田一龟在上个世纪](../Page/园田一龟.md "wikilink")30年代即有考证，认为该族原是居住鸭绿江南的女真人，14世纪末在始祖[李膺尼带领下由朝鲜迁至铁岭](../Page/李膺尼.md "wikilink")，后编入明朝卫所并出仕做官。

族谱以出任[铁岭卫指挥佥事的](../Page/铁岭卫指挥佥事.md "wikilink")[李英为一世祖](../Page/李英.md "wikilink")，之上还有[李哲根穗](../Page/李哲根穗.md "wikilink")、[李和山](../Page/李和山.md "wikilink")、[李厦霸努](../Page/李厦霸努.md "wikilink")、[李把图理](../Page/李把图理.md "wikilink")、[李膺尼五位祖先](../Page/李膺尼.md "wikilink")，但“因鼎革，碑记残毁，谱系散失，是以五位之世次无考未敢妄注也”。

第二世为[李文彬](../Page/李文彬.md "wikilink")，第三世为[李春英](../Page/李春英.md "wikilink")（或作[李春美](../Page/李春美.md "wikilink")），第四世[李泾](../Page/李泾.md "wikilink")，均世袭铁岭卫指挥佥事。李泾有子四人，居长者即明末辽东名将李成梁。李氏从第三世春字辈起，五个兄弟析为五房。李成梁一系属老长房，以下依次为老二房、老三房、老四房、老五房。

李氏家族成为明末最显赫的军功贵族，主要由于[李成梁的功绩](../Page/李成梁.md "wikilink")。李成梁有兄弟四人：[李成梁](../Page/李成梁.md "wikilink")、[李成材](../Page/李成材.md "wikilink")、[李成槟](../Page/李成槟.md "wikilink")、[李成用](../Page/李成用.md "wikilink")。李成梁有子五人[李如松](../Page/李如松.md "wikilink")、[李如柏](../Page/李如柏.md "wikilink")、[李如桢](../Page/李如桢.md "wikilink")、[李如樟](../Page/李如樟.md "wikilink")、[李如梅](../Page/李如梅.md "wikilink")，其中李如松曾经在[万历年间率军入朝鲜抗倭](../Page/万历.md "wikilink")，与朝鲜女子生了个儿子[李天根](../Page/李天根.md "wikilink")（起初名为[李天忠](../Page/李天忠.md "wikilink")）。

## 入朝鲜后的情形

后来在[甲申國難](../Page/甲申國難.md "wikilink")，李如松次子[李性中死于变乱](../Page/李性中.md "wikilink")，遗命其子李应仁（初名李应祖）东往朝鲜，逃亡到朝鲜[巨济岛](../Page/巨济岛.md "wikilink")（거제도）的鵝洲（아주）貫松村（관송촌）居住，李如梅之孙[李成龙也逃亡到了朝鲜](../Page/李成龙.md "wikilink")，再加上李如松与朝鲜女子生的私生子，于是朝鲜出现了[本贯为](../Page/本贯.md "wikilink")[陇西李氏的一支宗族](../Page/陇西李氏.md "wikilink")。

2000年南韩做的人口普查时，**陇西李氏**约有1045人。
另外，李如松与朝鲜女子[奉化琴氏](../Page/奉化琴氏.md "wikilink")（一作[通津琴氏](../Page/通津琴氏.md "wikilink")）的私生子李天忠后来与星州李氏合谱，多认为[李英是](../Page/李英.md "wikilink")[李长庚曾孙](../Page/李长庚.md "wikilink")，但是与铁岭李氏的记载不同，铁岭李氏族谱中的祖辈人名来自蒙古语，与东北地区的女真人取名方式相似。

  - 李成梁--李如松--李性忠--李应仁（李应祖）--李某--李某--李萱
  - 李成梁--李如松--李天根--李哲南--李承福
  - 李成梁--李如梅--李明祖--李得龙（李成龙）--李翻——李东栽--李勉

`                                                 |-李东培--李著`
`                                                 |-李东发`
`                                                 |-李东郁`

## 朝鲜的谱牒

朝鲜的[星州李氏和陇西李氏](../Page/星州李氏.md "wikilink")[李天忠支以曾经被元朝赐封陇西郡公的](../Page/李天忠.md "wikilink")[李长庚为中始祖](../Page/李长庚.md "wikilink")，李长羹生子五人：[李百年](../Page/李百年.md "wikilink")（密直公派）、[李千年](../Page/李千年.md "wikilink")（参知公派）、[李万年](../Page/李万年.md "wikilink")（侍中公派）、[李亿年](../Page/李亿年.md "wikilink")（留守公派）、[李兆年](../Page/李兆年.md "wikilink")（文烈公派）。李千年生二子：[李餘慶](../Page/李餘慶.md "wikilink")、[李承慶](../Page/李承慶.md "wikilink")（任辽阳行省参知政事）。李承慶之子即为[李英](../Page/李英.md "wikilink")。

星州李氏先世：1.李纯由——2.——3.——4.——5.李凡——6.李延居——7.李沖京——8.李營——9.李孝參——10.李敦文——11.李得禧——12.李长庚

## 参见

  - [李氏](../Page/李氏.md "wikilink")
  - [陇西李氏](../Page/陇西李氏.md "wikilink")

## 注釋

[Category:李姓本贯](../Category/李姓本贯.md "wikilink")

1.  [李成梁祖上则以陇西为籍](http://wiki.zupulu.com/topic.php?action=resumesview&topicid=470)
2.  [自朝鮮內附](http://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7238)