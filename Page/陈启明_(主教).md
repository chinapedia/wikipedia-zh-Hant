[Coat_of_arms_of_Giobbe_Chen_Chi_Ming.svg](https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Giobbe_Chen_Chi_Ming.svg "fig:Coat_of_arms_of_Giobbe_Chen_Chi_Ming.svg")

**陳啟明**主教（，），[遣使会士](../Page/遣使会.md "wikilink")，中国河北省[正定教区主教](../Page/正定教区.md "wikilink")。

1916年1月6日，陈启明成为神父。1937年10月9日，正定教区外籍主教[文致和被日军杀害](../Page/文致和.md "wikilink")，1939年1月5日，罗马教廷任命陈启明为河北省[正定代牧区第](../Page/正定教区.md "wikilink")8任主教，也是该教区首任华籍主教。5月25日，陈启明接受主教祝圣。
\[1\]1946年4月11日，罗马教廷宣布在中国正式建立圣统制，正定代牧区升为正式的正定教区。

1947年，由于政局变化，陈启明主教率11名神父南迁到四川[万县教区的忠县](../Page/万县教区.md "wikilink")、丰都、石柱3县继续传教\[2\]。1949年底，他们离开中国大陆。

1955年，陈启明主教出席在巴西[里约热内卢召开的第](../Page/里约热内卢.md "wikilink")36届国际圣体节大会。会后，罗马教廷委任他负责向巴西华侨传教。1956年，陈启明主教到任，开始带领一批华籍神父在巴西传教。
\[3\]

1959年6月10日，陈启明主教去世，年67岁。

## 参考文献

[Category:中華民國大陸時期天主教主教](../Category/中華民國大陸時期天主教主教.md "wikilink")
[Category:陈姓](../Category/陈姓.md "wikilink")
[Category:台灣外省人](../Category/台灣外省人.md "wikilink")
[Category:巴西天主教主教](../Category/巴西天主教主教.md "wikilink")
[Category:巴西天主教](../Category/巴西天主教.md "wikilink")
[Category:巴西華人](../Category/巴西華人.md "wikilink")

1.  [正定教区](http://www.gcatholic.com/dioceses/diocese/chen1.htm)
2.  [遣使会纪念周至毅神父百年华诞](http://www.cathlife.org.tw/index.php?option=com_content&task=view&id=773&Itemid=36)

3.  [1](http://adb.sh.gov.cn/shqb/node113/sqxx/node146/userobject1ai11832.html)巴西纪念中国神父抵巴50周年，2006-7-3