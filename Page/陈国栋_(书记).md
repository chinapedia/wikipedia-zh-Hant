**陈国栋**（），[江西](../Page/江西省.md "wikilink")[南昌人](../Page/南昌市.md "wikilink")。中共第十二、十三届[中央顾问委员会委员](../Page/中国共产党中央顾问委员会.md "wikilink")，第四届[全国政协常委](../Page/中国人民政治协商会议全国委员会.md "wikilink")，原[中共上海市委书记](../Page/中国共产党上海市委员会.md "wikilink")。

1931年参加革命工作，1932年3月加入[中国共产党](../Page/中国共产党.md "wikilink")。早年曾长期在[共青团工作](../Page/中国共产主义青年团.md "wikilink")，历任共青团上海国际电讯局特别支部书记，法南区委宣传部部长，沪东区委组织部部长、区委书记，共青团江苏省委组织部部长等职。[抗日战争时期](../Page/中国抗日战争.md "wikilink")，历任皖东北区泗县县长，苏中区四分区税务局局长、贸易局局长、两淮盐务局局长等职。[第二次国共内战时期](../Page/第二次国共内战.md "wikilink")，历任苏中区党委财委副书记，华中支前司令部副司令员，苏南行署副主任，苏南财委副书记。华东军政委员会财政部部长，华东财委副主任。

[中华人民共和国成立以后](../Page/中华人民共和国.md "wikilink")，先后任中央人民政府[财政部副部长](../Page/中华人民共和国财政部.md "wikilink")、党组副书记兼[交通銀行董事长](../Page/交通銀行.md "wikilink")。中央人民政府粮食部副部长、代部长、党组第一副书记、书记。“[文化大革命](../Page/文化大革命.md "wikilink")”中受迫害。1975年2月恢复工作后，历任[中华全国供销合作总社主任](../Page/中华全国供销合作总社.md "wikilink")、党组书记，国务院财贸小组组长，国家农业委员会副主任，[国务院财政经济委员会成员](../Page/中华人民共和国国务院.md "wikilink")，粮食部部长等职。1979年12月起，任中共[上海市委第二书记](../Page/上海市.md "wikilink")、市委第一书记兼[上海警备区第一政委](../Page/中国人民解放军上海警备区.md "wikilink")。1985年6月至1992年12月，任中共上海市顾问委员会主任。

2005年6月7日在上海逝世，享年94岁。\[1\]

## 参考文献

[C陈](../Category/中央人民政府粮食部部长.md "wikilink")
[C](../Category/南昌人.md "wikilink")
[G](../Category/陈姓.md "wikilink")
[Category:中华人民共和国财政部副部长](../Category/中华人民共和国财政部副部长.md "wikilink")
[Category:交通银行董事长](../Category/交通银行董事长.md "wikilink")
[Category:中共上海市委书记](../Category/中共上海市委书记.md "wikilink")
[Category:中共中央顾问委员会委员](../Category/中共中央顾问委员会委员.md "wikilink")
[Category:第四届全国政协常务委员](../Category/第四届全国政协常务委员.md "wikilink")

1.