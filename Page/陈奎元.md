**陈奎元**（），[汉族](../Page/汉族.md "wikilink")，[辽宁](../Page/辽宁.md "wikilink")[康平人](../Page/康平.md "wikilink")；[内蒙古师范学院政治教育专业毕业](../Page/内蒙古师范学院.md "wikilink")。1965年5月加入[中国共产党](../Page/中国共产党.md "wikilink")；是中共第十四、十五、十六、十七届中央委员。曾任[全国政协副主席](../Page/全国政协.md "wikilink")、[中国社会科学院院长](../Page/中国社会科学院.md "wikilink")、党组书记\[1\]。

1964年，陈奎元在大学毕业后，被分配到[中共](../Page/中共.md "wikilink")[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[呼伦贝尔盟委党校工作](../Page/呼伦贝尔盟.md "wikilink")。此后，他历任呼伦贝尔盟革委会办公室干事，宣传部科长，西新巴旗阿镇镇委书记，呼伦贝尔盟委党校副校长，呼伦贝尔盟委副秘书长、代秘书长，秘书长、盟委常委，盟委副书记、书记等职。1989年，陈奎元进入[中共](../Page/中共.md "wikilink")[内蒙古自治区党委常委](../Page/内蒙古自治区.md "wikilink")，并且兼任自治区高校工委书记；1991年又出任内蒙古自治区人民政府副主席。1992年1月，调任[西藏自治区党委副书记](../Page/西藏自治区.md "wikilink")；同年11月，接替[胡锦涛出任](../Page/胡锦涛.md "wikilink")[西藏自治区党委书记](../Page/西藏自治区.md "wikilink")。

陈奎元在[西藏待了八年](../Page/西藏.md "wikilink")。2000年，他被调任[河南省委书记](../Page/河南省.md "wikilink")。2003年1月，上调中央，出任[中国社会科学院院长](../Page/中国社会科学院.md "wikilink")、党组书记。同年3月，在[全国政协十届一次会议上](../Page/全国政协.md "wikilink")，陈奎元又当选为第十届[全国政协副主席](../Page/全国政协副主席.md "wikilink")；并于2008年3月获得连任。2013年4月，卸任中国社会科学院院长、党组书记，由[王伟光接任](../Page/王伟光.md "wikilink")\[2\]。

於2013年11月19日對陈奎元以及其他4個前中國政要發出逮捕令，並指控5人涉嫌在中國對[西藏進行](../Page/西藏.md "wikilink")[種族滅絕罪行](../Page/種族滅絕.md "wikilink")\[3\]。

## 参考文献

{{-}}

[C陈](../Category/中华人民共和国领导人.md "wikilink")
[Category:中共河南省委书记](../Category/中共河南省委书记.md "wikilink")
[Category:中共西藏自治区党委书记](../Category/中共西藏自治区党委书记.md "wikilink")
[Category:内蒙古自治区副主席](../Category/内蒙古自治区副主席.md "wikilink")
[Category:内蒙古师范大学校友](../Category/内蒙古师范大学校友.md "wikilink")
[C陈](../Category/沈阳人.md "wikilink") [K](../Category/陈姓.md "wikilink")

1.
2.
3.