**陳寶蓮**（英文名：Pauline
Chan，），已故[上海](../Page/上海.md "wikilink")、[香港](../Page/香港.md "wikilink")[女演員](../Page/女演員.md "wikilink")。原名**趙靜**\[1\]。

## 生平

陳寶蓮是家中獨女；4岁時父母离婚，之後被外婆带大。12歲時跟隨母親移居[香港](../Page/香港.md "wikilink")，15歲起兼職[模特兒](../Page/模特兒.md "wikilink")。17歲時參選1990年度[亞洲小姐競選落選後被經紀公司簽為](../Page/亞洲小姐競選.md "wikilink")[藝人](../Page/藝人.md "wikilink")，第一個主持的節目為《[冧莊](../Page/冧莊.md "wikilink")》。\[2\]

入行之初正值香港[三級電影盛行](../Page/三級片.md "wikilink")，她亦隨波逐流接拍多部三級影片《灯草和尚》《我来自北京》《五月樱唇》等，希望成名之後再洗脫性感艷星形象，可是卻未成功，且是非不斷。後來疑似因爲受藥物影響而經常神智不清，被香港媒體把她和[藍潔瑛](../Page/藍潔瑛.md "wikilink")、[蔡楓華](../Page/蔡楓華.md "wikilink")、[洪朝豐封為](../Page/洪朝豐.md "wikilink")「四大癲王」。\[3\]

2002年7月31日傍晚5時半，陳寶蓮於上海靜安區南陽路24樓的寓所跳樓[自殺身亡](../Page/自殺.md "wikilink")，遺下剛滿月的兒子。《[再見螢火蟲](../Page/再見螢火蟲_\(台灣電視劇\).md "wikilink")》成了她生前最後一部作品\[4\]。

## 感情生活

陳寶蓮不僅星路不如預期，感情生活也是波折重重。1993年，20岁的陈宝莲和[莫少聪在](../Page/莫少聪.md "wikilink")《[剑奴之血契约](../Page/剑奴之血契约.md "wikilink")》认识相恋，不过很快就分手了。1995年，和[黄子扬的戀情也无果而终](../Page/黄子扬.md "wikilink")。\[5\]

與乾爹黄任中之間的曖昧關係也最常為媒體的話題。黄任中曾向媒体公开和干女儿兩人同床有五十至六十次，從來沒發生過關係；他們之間是仰慕、父女、好友的那种感情。\[6\]

1997年，陈宝莲為進入[台湾市场](../Page/台湾.md "wikilink")，主动追求台湾男主持[张菲](../Page/张菲.md "wikilink")，后因[黄任中插手介入而失败](../Page/黄任中.md "wikilink")。后来与歌手[蔡枫华及曾参加](../Page/蔡枫华.md "wikilink")[亚视](../Page/亚视.md "wikilink")《[港男选举](../Page/港男选举.md "wikilink")》的吴学明也陸續传出过绯闻。\[7\]

2002年6月，陈宝莲生下一个男婴。其向好友透露，嬰兒的父親是在台北[迪斯科邂逅的](../Page/迪斯科.md "wikilink")[DJ](../Page/DJ.md "wikilink")，一位25岁的[美籍华人](../Page/美籍华人.md "wikilink")。\[8\]

據2016年5月8日出版的[明報週刊報道](../Page/明報週刊.md "wikilink")，此子現正由[王菲好拍檔](../Page/王菲.md "wikilink")[邱瓈寬](../Page/邱瓈寬.md "wikilink")\[9\]收養。

## 影視作品

### 電影

| 年份   | 片名                                                           | 飾演        |
| ---- | ------------------------------------------------------------ | --------- |
| 1991 | 《[夜生活女王霞姐傳奇](../Page/夜生活女王霞姐傳奇.md "wikilink")》               | 應召女郎玲達    |
| 1992 | 《[花街狂奔](../Page/花街狂奔.md "wikilink")》                         |           |
| 1992 | 《[聊齋三之燈草和尚](../Page/燈草和尚_\(電影\).md "wikilink")》              | 綺夢        |
| 1992 | 《[應召女郎1988之二現代應召女郎](../Page/應召女郎1988之二現代應召女郎.md "wikilink")》 | 阿蓮（伊娃）    |
| 1992 | 《[五月櫻唇](../Page/五月櫻唇.md "wikilink")》                         | 方惠娜       |
| 1992 | 《[飛女正傳](../Page/飛女正傳.md "wikilink")》                         | 白箭牌       |
| 1992 | 《[我來自北京](../Page/我來自北京.md "wikilink")》                       | 亞鳳        |
| 1992 | 《[姦魔](../Page/姦魔.md "wikilink")》                             |           |
| 1992 | 《[北妹傳奇](../Page/北妹傳奇.md "wikilink")》                         | 洪蓮        |
| 1993 | 《大嘢》                                                         |           |
| 1993 | 《[縱橫天下](../Page/縱橫天下.md "wikilink")》                         |           |
| 1993 | 《[省港流鶯](../Page/省港流鶯.md "wikilink")》                         | 陳寶蓮       |
| 1993 | 《[現代情慾篇之換妻檔案](../Page/現代情慾篇之換妻檔案.md "wikilink")》             |           |
| 1993 | 《[劍奴](../Page/劍奴.md "wikilink")》                             | 舞娘        |
| 1993 | 《[摧花神龍教](../Page/摧花神龍教.md "wikilink")》                       |           |
| 1993 | 《[神經刀與飛天貓](../Page/神經刀與飛天貓.md "wikilink")》                   | 伊賀派魔女     |
| 1993 | 《[三劍俠與飛機妹](../Page/三劍俠與飛機妹.md "wikilink")》                   |           |
| 1993 | 《[風起雲湧之情迷香江](../Page/風起雲湧之情迷香江.md "wikilink")》               |           |
| 1993 | 《[火舞風雲之北妹](../Page/火舞風雲之北妹.md "wikilink")》                   |           |
| 1994 | 《[國產凌凌漆](../Page/國產凌凌漆.md "wikilink")》                       | 瀋陽特務（愛美神） |
| 1994 | 《[一人有一個綺夢...青春夢裡人](../Page/一人有一個綺夢...青春夢裡人.md "wikilink")》   |           |
| 1995 | 《[偶遇](../Page/偶遇.md "wikilink")》（兼任出品人）                      | Pauline   |
| 1996 | 《[第八宗罪](../Page/第八宗罪.md "wikilink")》                         |           |
| 1996 | 《[旺角揸fit人](../Page/旺角揸fit人.md "wikilink")》                   | 護士        |
| 1996 | 《[鬼劇院之驚青艶女郎](../Page/鬼劇院之驚青艶女郎.md "wikilink")》               | 慕蓮        |
| 1996 | 《[假男假女](../Page/假男假女.md "wikilink")》                         | Pauline   |
| 1997 | 《[基佬40](../Page/基佬40.md "wikilink")》                         | 劍笙輝劇團團員   |
| 1997 | 《[我有我瘋狂](../Page/我有我瘋狂.md "wikilink")》                       | 廖小璇       |
| 1997 | 《[夜半2點鐘](../Page/夜半2點鐘.md "wikilink")》                       | 伊娃        |
| 1999 | 《[艷降勾魂](../Page/艷降勾魂.md "wikilink")》                         | Pauline   |
| 2000 | 《[情陷百樂門](../Page/情陷百樂門.md "wikilink")》                       | Wyman     |
|      |                                                              |           |

#### 台灣

| 年份   | 片名                                                                                                 | 飾演 |
| ---- | -------------------------------------------------------------------------------------------------- | -- |
| 1993 | 《[十大槍擊要犯之殺生狀元](../Page/十大槍擊要犯之殺生狀元.md "wikilink")》                                                 | 珍妮 |
| 1999 | 《[海上花](../Page/海上花.md "wikilink")》（[上海話](../Page/上海話.md "wikilink")[配音](../Page/配音.md "wikilink")） |    |
| 2002 | 《[千禧曼波](../Page/千禧曼波.md "wikilink")》                                                               |    |
|      |                                                                                                    |    |

### 電視節目

#### 香港

| 年份   | 節目                                                                   |
| ---- | -------------------------------------------------------------------- |
| 1990 | 《[冧莊](../Page/冧莊.md "wikilink")》第一輯 第1至15集                           |
| 1996 | 《[超級無敵獎門人](../Page/獎門人電視遊戲節目系列#第一輯_-_超級無敵獎門人.md "wikilink")》第一輯 第25集 |
|      |                                                                      |

#### 台灣

| 年份   | 劇名                                                     | 飾演         |
| ---- | ------------------------------------------------------ | ---------- |
| 2002 | 《[再見螢火蟲](../Page/再見螢火蟲_\(台灣電視劇\).md "wikilink")》\[10\] | PUB老闆娘（客串） |
|      |                                                        |            |

### 唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>唱片公司</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>较春</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><p>1998年8月1日</p></td>
<td style="text-align: left;"><p>鸿谷唱片</p></td>
<td style="text-align: left;"><ol>
<li>OAOA</li>
<li>结婚？</li>
<li>蝴蝶</li>
<li>算了</li>
<li>乱</li>
<li>第六首</li>
<li>随便</li>
<li>飘飘</li>
</ol></td>
</tr>
</tbody>
</table>

## 參考資料及註釋

## 外部連結

  -
  -
  -
  -
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:亞洲小姐](../Category/亞洲小姐.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:香港自殺藝人](../Category/香港自殺藝人.md "wikilink")
[Category:女性自殺者](../Category/女性自殺者.md "wikilink")
[Category:從高空跳下自殺身亡者](../Category/從高空跳下自殺身亡者.md "wikilink")
[B寶](../Category/趙姓.md "wikilink")

1.

2.

3.

4.

5.
6.  [黃任中痛悼陳寶蓮﹕不是最寵但感情深厚](http://www.epochtimes.com/b5/2/8/4/n206188.htm)

7.
8.
9.  [陳寶蓮兒子恨入行
    養母邱瓈寬不准](http://news.mingpao.com/pns/dailynews/web_tc/article/20160508/s00016/1462643938204)

10.