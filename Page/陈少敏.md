[Chen_Shaoin_1977.JPG](https://zh.wikipedia.org/wiki/File:Chen_Shaoin_1977.JPG "fig:Chen_Shaoin_1977.JPG")

**陈少敏**（），原名**孙肇修**，出生于[中国](../Page/中国.md "wikilink")[山东省](../Page/山东省.md "wikilink")[寿光县范于村](../Page/寿光县.md "wikilink")。1927年投身革命；1928年加入[中国共产党](../Page/中国共产党.md "wikilink")；是中共第七届[中央候补委员](../Page/中央候补委员.md "wikilink")，[第八届中央委员](../Page/中国共产党第八届中央委员会委员.md "wikilink")。生前曾担任[中国工会组织领导人](../Page/中国工会.md "wikilink")，历任[中华全国总工会副主席](../Page/中华全国总工会.md "wikilink")、[中国纺织工会第一任主席等职](../Page/中国纺织工会.md "wikilink")。

## 早年经历

早年，孙肇修曾在[青岛日本商人的纺纱厂做工](../Page/青岛.md "wikilink")。1927年2月，考入了[美国人创办的教会女子学校](../Page/美国.md "wikilink")－[潍县文美中学](../Page/潍县文美中学.md "wikilink")。在学校，她积极参加中共领导的学生运动，初显了自己的组织领导才能。

1930年3月，**孙肇修**改名**陈少敏**，是中共青岛市委职工运动委员会的委员。她以女工身份，掩护和配合时任[中共山东省委书记](../Page/中共山东省委.md "wikilink")[任国桢的革命活动](../Page/任国桢_\(烈士\).md "wikilink")；俩人于当年6月结婚。婚后一年多，因叛徒出卖，任国桢在山西被捕，后被处决。一年后，他们的女儿又因患[麻疹在老家夭折](../Page/麻疹.md "wikilink")。\[1\]

## 军中女将

在[抗日战争和](../Page/中國抗日戰爭.md "wikilink")[第二次国共内战期间](../Page/第二次国共内战.md "wikilink")，陈少敏一直冲锋陷阵，成为中共军队当中少有的女将领，也是曾长期负责一个地区全面工作的中共女性领导干部之一。曾历任[中共天津市委秘书长](../Page/中共天津市委.md "wikilink")、妇女部长，中共[唐山市委宣传部长](../Page/唐山.md "wikilink")，[冀](../Page/冀.md "wikilink")[鲁](../Page/鲁.md "wikilink")[豫特委副书记](../Page/豫.md "wikilink")，[中共江西省委妇女部长](../Page/中共江西省委.md "wikilink")，[洛阳特委书记](../Page/洛阳.md "wikilink")、[中共河南省委组织部长等职](../Page/中共河南省委.md "wikilink")。

1939年6月，陈少敏担任新成立的中共鄂中区党委书记。之后，她又和[李先念一起主持了鄂中和豫南地区抗日武装的整编](../Page/李先念.md "wikilink")，建立了[新四军豫鄂独立游击支队](../Page/新四军.md "wikilink")；李先念任司令员，陈少敏出任政治委员。而后，她还曾担任鄂豫边区党委书记，[新四军五师副](../Page/新四军.md "wikilink")[政委](../Page/政委.md "wikilink")，[中共中央中原局组织部部长等职](../Page/中共中央中原局.md "wikilink")。1945年，陈少敏当选[中共第七届中央候补委员](../Page/中共第七届中央候补委员.md "wikilink")；是当时，整个中共中央委员会里，仅有的三位女委员之一；另外两位分别是[邓颖超和](../Page/邓颖超.md "wikilink")[蔡畅](../Page/蔡畅.md "wikilink")。1948年初，陈少敏因[心脏病突发](../Page/心脏病.md "wikilink")，被迫到[华北后方医院治疗](../Page/华北.md "wikilink")。

## 工会领袖

1948年秋，陈少敏奉令调[华东局](../Page/华东局.md "wikilink")，中央决定准备由她担任[中共山东省委的领导工作](../Page/中共山东省委.md "wikilink")。但陈少敏自知个人身体状况以及性格特点等原因，向中央写信，请求从事工会的工作。在信中，她是这样写的：

『*我的工作在哪个地方，和谁在一起，都无意见。我本着不讲个人得失的原则，和谁一起工作都可以。……我没有耐性，作不了专门的妇女工作……，我粗心，对干部的方法太简单、太直爽，对落后的一点也不能让步，对人的歪风爱批评，所以，我作不了组织工作……我的毛病你们是知道的，最好分配我去作工人工作或农民工作，从一个工厂或一个生产合作社做起，一气儿做上三年、五年，搞出一套群众工作经验出来……*』\[2\]

[周恩来最终亲笔回信](../Page/周恩来.md "wikilink")，同意了陈少敏的请求。建国后，她出任中华全国总工会书记处书记、并兼任中国纺织工会第一任主席。此后，历任中华全国总工会副主席、党组副书记，[全国人大常委](../Page/全国人大.md "wikilink")，[全国政协常委等职](../Page/全国政协.md "wikilink")。在任工会领导期间，她深入调查研究，总结推广了在纺织工业上的“[郝建秀工作法](../Page/郝建秀工作法.md "wikilink")”，提高了纺织工业水平。也正是有了陈少敏的培养与帮助，[郝建秀日后成为了中国的](../Page/郝建秀.md "wikilink")[纺织工业部部长](../Page/纺织工业部.md "wikilink")\[3\]。

## 正直良心

1968年10月，在[中共八届十二中全会上](../Page/中共八届十二中全会.md "wikilink")，已在「[文化大革命](../Page/文化大革命.md "wikilink")」中受到冲击的陈少敏，以66岁的高龄抱病参加会议。会上在表决“永远开除”[刘少奇出党的决议时](../Page/刘少奇.md "wikilink")，陈少敏伏在桌上拒绝举手。散会后，[康生曾质问陈少敏](../Page/康生.md "wikilink")，但陈少敏的回答是：“这是我的权利！”。尽管当时发表的会议公报称，大会“一致通过”开除刘少奇出党的决定；但随着「文革」的历史渐渐恢复真相，陈少敏也就成为刘少奇冤假错案中唯一的良心\[4\]。

而正因为如此，康生、[江青等人加紧了对陈少敏的迫害](../Page/江青.md "wikilink")；给她扣上了许多“罪名”，例如“刘少奇在全国总工会的代理人”，“[中原突围时的叛徒头子](../Page/中原突围.md "wikilink")”等等；在全国总工会机关挨批斗，还被拉到[中山公园音乐堂去坐](../Page/北京中山公园.md "wikilink")“[喷气式](../Page/喷气式.md "wikilink")”。1969年10月，[林彪借口](../Page/林彪.md "wikilink")“战备疏散”，将陈少敏遣送到河南省[罗山农场劳动改造](../Page/罗山农场.md "wikilink")；后旧病复发，导致[半身不遂](../Page/半身不遂.md "wikilink")。直到[九一三事件发生后](../Page/九一三事件.md "wikilink")，才得以回[北京治疗](../Page/北京.md "wikilink")\[5\]。

1977年12月14日，陈少敏病逝于北京。在谈论起刘少奇时，[胡耀邦曾激动地在中共党内会议上说](../Page/胡耀邦.md "wikilink")：“在这个问题上，我们大家都犯过错误，都举了手。就是陈大姐没有举手，没有犯错误……”。\[6\]

## 参考文献

[Category:中国共产党党员
(1928年入党)](../Category/中国共产党党员_\(1928年入党\).md "wikilink")
[Category:新四军将领](../Category/新四军将领.md "wikilink")
[Category:中国人民解放军女性军官](../Category/中国人民解放军女性军官.md "wikilink")
[Category:中华人民共和国女性政府官员](../Category/中华人民共和国女性政府官员.md "wikilink")
[Category:中华全国总工会副主席](../Category/中华全国总工会副主席.md "wikilink")
[Category:中国共产党第八届中央委员会委员](../Category/中国共产党第八届中央委员会委员.md "wikilink")
[Category:中国共产党第七届中央委员会候补委员](../Category/中国共产党第七届中央委员会候补委员.md "wikilink")
[Category:文革受難者](../Category/文革受難者.md "wikilink")
[Category:寿光人](../Category/寿光人.md "wikilink")
[S少敏](../Category/陳姓.md "wikilink")
[\~](../Category/孙姓.md "wikilink")

1.  [巾帼英雄陈少敏](http://wfds.wfnet.cn/webmanage/dispdetail.asp?id=556)
2.  [周恩来写给陈少敏的一封信](http://www.gmw.cn/CONTENT/2005-01/25/content_170988.htm)
    杨金宝 苗俊千（口述） 桑乐泉（整理）光明网
3.  [陈少敏](http://www.yeqm.com/prc2004/zhxs/items/xing/chen/mr/jd/c-shaomin.html)
4.  [陈少敏，刘少奇事件中的正直者良心！](http://www.chinavalue.net/showarticle.aspx?id=49126&categoryID=30)价值中国网
    作者:金久皓
5.  [陈少敏简历](http://bjds.bjdj.gov.cn/ShowArticle.asp?ArticleID=12272)
    中共北京市委党史研究室
6.  [刘少奇被开除出党谁没举手赞成?](http://news.xinhuanet.com/book/2007-12/30/content_7321894.htm)
    北京青年报