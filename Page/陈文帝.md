**陳文帝陈蒨**（），一作**茜**，又名**昙蒨**\[1\]、**荃菺**，字**子華**。[中国](../Page/中国.md "wikilink")[南北朝时期](../Page/南北朝.md "wikilink")[陈朝第二位](../Page/陈朝.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")（560年—566年在位），在位7年，年号[天嘉](../Page/天嘉.md "wikilink")。

## 生平

陈蒨是陈朝开国皇帝[陈霸先長兄](../Page/陈霸先.md "wikilink")[陳道譚的長子](../Page/陳道譚.md "wikilink")，深受陈霸先的賞識與栽培，更令其總理軍政。後來武帝駕崩，皇后稱武帝遺詔命陈蒨入纂皇統，遂即帝位。陈蒨在位期間，励精图治，整顿吏治，注重农桑，兴修水利，恢复江南经济。此时陈朝政治清明，百姓富裕，国势强盛，史稱「天嘉小康」。陳蒨亦因而是[南朝历代皇帝中难得一见的明君](../Page/南朝.md "wikilink")。566年崩，享年44岁，[谥号为](../Page/谥号.md "wikilink")**文帝**，[庙号](../Page/庙号.md "wikilink")**世祖**。葬于永宁陵（在今[南京](../Page/南京.md "wikilink")[棲霞區](../Page/棲霞區.md "wikilink")[棲霞街道新合村獅子冲](../Page/棲霞街道.md "wikilink")）。

## 宠臣

陈蒨有一名貌美如婦的寵臣[韓子高](../Page/韓子高.md "wikilink")，《[南史](../Page/南史.md "wikilink")》記曰：“子高年十六，為總角，容貌美麗，狀似婦人。”陈蒨在还是临川王时邂逅了這位美少年，從此讓韓子高隨侍左右，寵愛備至。基於這種曖昧事實，所以後世有些[小說](../Page/小說.md "wikilink")、[戲曲藉題發揮](../Page/戲曲.md "wikilink")，露骨地將二人描繪成[同性愛關係](../Page/同性愛.md "wikilink")，例如[唐朝](../Page/唐朝.md "wikilink")[李翊的](../Page/李翊.md "wikilink")《[陳子高傳](../Page/陳子高傳.md "wikilink")》（[明朝](../Page/明朝.md "wikilink")[馮夢龍的](../Page/馮夢龍.md "wikilink")《[情史](../Page/情史.md "wikilink")》有節錄）、明朝[王驥德的](../Page/王驥德.md "wikilink")《[男王后](../Page/男王后.md "wikilink")》等皆是著名創作。

## 妻妾

  - 文皇后[沈妙容](../Page/沈妙容.md "wikilink")
  - [严淑媛](../Page/严淑媛.md "wikilink")
  - [潘容华](../Page/潘容华.md "wikilink")
  - [刘昭华](../Page/刘昭华.md "wikilink")
  - [王充华](../Page/王充华.md "wikilink")
  - [张修容](../Page/张修容.md "wikilink")
  - [韩修华](../Page/韩修华.md "wikilink")
  - [江贵妃](../Page/江贵妃.md "wikilink")
  - [孔贵妃](../Page/孔贵妃.md "wikilink")

## 儿子

1.  长子：陈废帝[陈伯宗](../Page/陈伯宗.md "wikilink")，字奉业，小字药王，生母皇后沈妙容
2.  次子：始兴王[陈伯茂](../Page/陈伯茂.md "wikilink")，字郁之，生母皇后沈妙容，568年被[陈宣帝贬为温麻侯并杀害](../Page/陈宣帝.md "wikilink")
3.  三子：鄱阳王[陈伯山](../Page/陈伯山.md "wikilink")，字静之，生母严淑媛
4.  四子早卒
5.  五子：庶人（昔为新安王）[陈伯固](../Page/陈伯固.md "wikilink")，字牢之，生母潘容华，582年作乱被杀
6.  六子：晋安王[陈伯恭](../Page/陈伯恭.md "wikilink")，字肃之，生母严淑媛
7.  七子：衡阳王[陈伯信](../Page/陈伯信.md "wikilink")，字孚之，生母刘昭华，589年被东衡州刺史[王勇所害](../Page/王勇.md "wikilink")
8.  八子：庐陵王[陈伯仁](../Page/陈伯仁.md "wikilink")，字寿之，生母王充华
9.  九子：江夏王[陈伯义](../Page/陈伯义.md "wikilink")，字坚之，生母张修容
10. 十子：武陵王[陈伯礼](../Page/陈伯礼.md "wikilink")，字用之，生母韩修华
11. 十一子早卒
12. 十二子：永阳王[陈伯智](../Page/陈伯智.md "wikilink")，字策之，生母江贵妃
13. 十三子：桂阳王[陈伯谋](../Page/陈伯谋.md "wikilink")，字深之，生母孔贵妃

## 后裔

  - 曾孙陈昭列，唐朝[文州刺史](../Page/文州.md "wikilink")。其女嫁朝散大夫、检校尚书、比部员外郎博陵崔玄隐
  - 陈义方，唐朝[开府仪同三司](../Page/开府仪同三司.md "wikilink")。其女嫁[荆州大都督府](../Page/荆州.md "wikilink")[长林县令骑都尉](../Page/长林.md "wikilink")[昌黎人韩仁楷](../Page/昌黎.md "wikilink")

|-   |-

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[Category:南朝陳皇帝](../Category/南朝陳皇帝.md "wikilink")
[C](../Category/中國雙性戀者.md "wikilink")
[陈](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")
[Category:陳姓](../Category/陳姓.md "wikilink")

1.  《[新唐书](../Page/新唐书.md "wikilink")·宰相世系表》