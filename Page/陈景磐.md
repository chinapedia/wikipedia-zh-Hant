**陈景磐**（），字瞻岩，[中国教育家](../Page/中国教育.md "wikilink")、教育史学者。

## 生平

陈景磐祖籍[福建省](../Page/福建省.md "wikilink")[古田县](../Page/古田县.md "wikilink")，1904年2月23日出生在[福州一个](../Page/福州.md "wikilink")[神职人员的家庭](../Page/神职人员.md "wikilink")，就读于[英国圣公会在福州市](../Page/英国圣公会.md "wikilink")[仓山区设立的三一中学](../Page/仓山区.md "wikilink")，是香港[地方教会的长老](../Page/地方教会.md "wikilink")[魏光禧的同學](../Page/魏光禧.md "wikilink")。1927年获[上海圣约翰大学学士学位](../Page/圣约翰大学_\(上海\).md "wikilink")，1929年又获该校哲学硕士学位。毕业后回到福州三一中学，教英文、哲学并兼任初中部教务长\[1\]。1931年赴北平进入[燕京大学](../Page/燕京大学.md "wikilink")，1934年获教育硕士学位。随后前往河南[开封](../Page/开封.md "wikilink")，在[加拿大圣公会开办的豫中中学](../Page/加拿大圣公会.md "wikilink")（St.
Andrew’s School）担任校长。

1936年进入加拿大[多伦多大学](../Page/多伦多大学.md "wikilink")，1940年获哲学博士，归国后受聘于[福建协和大学](../Page/福建协和大学.md "wikilink")（当时在福建北部的[邵武](../Page/邵武.md "wikilink")），任教育系教授兼附中校长。1942年前往福建西部[长汀](../Page/长汀.md "wikilink")，任教于国立[厦门大学](../Page/厦门大学.md "wikilink")。1945年抗战结束，陈景磐随厦大迁回[厦门](../Page/厦门.md "wikilink")。1950年，他前往[北京](../Page/北京.md "wikilink")，受聘于[燕京大学](../Page/燕京大学.md "wikilink")，任教育系教授。1952年全国大学院系调整，陈景磐被分配到[北京师范大学任教](../Page/北京师范大学.md "wikilink")，直到1989年去世。陈景磐从1961年开始指导教育史硕士研究生，1980年代后开始擔任教育史博士生导师，指導的博士生有[史静寰](../Page/史静寰.md "wikilink")、[吕达等](../Page/吕达.md "wikilink")。

## 著作

  - 《先师孔子》（Confuciusas a Teacher），1940年
  - 《孔子的教育思想》，湖北人民出版社，1957年
  - 《太平天国的教育》，1958年
  - 《中国近代教育史讲义》，1961年
  - 《中国近代教育史》（高等学校文科教材），人民教育出版社，1979年
  - 《杜威赫尔巴特教育思想研究》，山东教育出版社，1985年
  - 《中国近现代教育家传》，1986年
  - 《西方学者孟录、顾立雅等论孔子的教育思想》，《近四十年来孔子研究论文选编》，齐鲁书社，1987年
  - 《清代后期教育论著选》，人民教育出版社，1997年
  - 《陈景磐教育论文选》，北京师范大学出版社，2004

## 参考文献

<div class="references-small">

<references />

</div>

[Category:中国教育家](../Category/中国教育家.md "wikilink")
[Category:中国历史学家](../Category/中国历史学家.md "wikilink")
[Category:多倫多大學校友](../Category/多倫多大學校友.md "wikilink")
[Category:上海聖約翰大學校友](../Category/上海聖約翰大學校友.md "wikilink")
[Category:厦门大学校友](../Category/厦门大学校友.md "wikilink")
[Category:古田人](../Category/古田人.md "wikilink")
[J景](../Category/陈姓.md "wikilink")

1.  [《仓山区志》人物简介](http://www.fjsq.gov.cn/showtext.asp?ToBook=3005&index=507)