**陈有年**（），[明朝大臣](../Page/明朝.md "wikilink")，[字](../Page/字.md "wikilink")**登之**，[号](../Page/号.md "wikilink")**克庵**，[浙江](../Page/浙江.md "wikilink")[余姚县](../Page/余姚县.md "wikilink")（今[余姚市](../Page/余姚市.md "wikilink")）人。与余姚[孙鑨](../Page/孙鑨.md "wikilink")，[平湖](../Page/平湖.md "wikilink")[陆光祖并称为](../Page/陆光祖.md "wikilink")“浙中三贤太宰”，闻名天下。

## 经历

父亲[陈克宅](../Page/陈克宅.md "wikilink")，字即卿，[正德九年](../Page/正德_\(明朝\).md "wikilink")[进士](../Page/进士.md "wikilink")，[嘉靖中官御史](../Page/嘉靖.md "wikilink")，是一代名吏。陈有年于嘉靖四十一年（1562年）考中壬戌科二甲[进士](../Page/进士.md "wikilink")，授[刑部](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")。后改任[吏部](../Page/吏部.md "wikilink")，历验封郎中。[万历元年](../Page/万历.md "wikilink")（1573年），参与廷争，奏不得[张居正](../Page/张居正.md "wikilink")，告病退职。万历十二年（1584年），起用任稽勋郎中，历任考功、文选。“除目下，中外皆服”。迁[太常寺](../Page/太常寺.md "wikilink")[少卿](../Page/少卿.md "wikilink")，以右佥都御史任[江西](../Page/江西.md "wikilink")[巡抚](../Page/巡抚.md "wikilink")。[張居正死後后](../Page/張居正.md "wikilink")，經[孫繼先舉薦](../Page/孫繼先_\(隆慶進士\).md "wikilink")，恢復官職\[1\]，累迁[吏部右侍郎](../Page/吏部右侍郎.md "wikilink")，后改任[兵部](../Page/兵部.md "wikilink")，又改回[吏部](../Page/吏部.md "wikilink")。万历二十一年（1593年），诏拜[吏部尚书](../Page/吏部尚书.md "wikilink")。次年致仕。万历二十六年（1598年）正月卒，享年六十八岁。四月，诏任其为[南京右都御史](../Page/南京.md "wikilink")，但陈有年已经逝世。朝廷追封[太子太保](../Page/太子太保.md "wikilink")，[谥](../Page/谥.md "wikilink")**恭介**。

## 评价

“风节高天下”——摘自《明史·陈有年传》

## 著作

  - 《陈恭介公集》

## 参考

[Category:明朝吏部主事](../Category/明朝吏部主事.md "wikilink")
[Category:明朝吏部郎中](../Category/明朝吏部郎中.md "wikilink")
[Category:明朝太常寺少卿](../Category/明朝太常寺少卿.md "wikilink")
[Category:明朝江西巡抚](../Category/明朝江西巡抚.md "wikilink")
[Category:明朝吏部侍郎](../Category/明朝吏部侍郎.md "wikilink")
[Category:明朝兵部侍郎](../Category/明朝兵部侍郎.md "wikilink")
[Category:明朝吏部尚書](../Category/明朝吏部尚書.md "wikilink")
[Category:余姚人](../Category/余姚人.md "wikilink")
[Y](../Category/陳姓.md "wikilink")
[Category:諡恭介](../Category/諡恭介.md "wikilink")

1.  《明史》（卷229）：孫繼先
    ，字胤甫，盂人。隆慶五年進士。居正既敗，繼先請召吳中行、趙用賢、艾穆、沈思孝、鄒元標幷及余懋學、趙應元、傅應禎、朱鴻謨、孟一脈、王用汲。又薦魏學曾、宋纁、張岳、毛綱、胡執禮、王錫爵、賈三近、溫純、曹科、陳有年、朱光宇、趙參魯等諸人。既坐謫，終南京吏部主事。