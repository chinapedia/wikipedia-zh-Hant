**陈梓**（[1683年](../Page/1683年.md "wikilink")－[1759年](../Page/1759年.md "wikilink")），[清朝诗人](../Page/清朝.md "wikilink")、[书法家](../Page/书法家.md "wikilink")。[字](../Page/字.md "wikilink")**俯恭**、**敷公**、**一斋**，[号](../Page/号.md "wikilink")**古铭**，又号**客星山人**。[浙江](../Page/浙江.md "wikilink")[餘姚](../Page/餘姚.md "wikilink")（今屬[寧波市](../Page/寧波市.md "wikilink")）人。清朝[康乾盛世时期的著名诗人](../Page/康乾盛世.md "wikilink")。

## 生平

陈梓精于诗词古文，当时与北方[李锴齐名](../Page/李锴.md "wikilink")，人称“南陈北李”。

[雍正十一年](../Page/雍正.md "wikilink")（1734年），被荐举博学鸿词，不就。

[乾隆元年](../Page/乾隆.md "wikilink")（1736年），举孝廉方正，又不就。

## 著作

  - 《杨园先生年谱》四卷，（[清](../Page/清.md "wikilink")）[姚夏](../Page/姚夏_\(清朝\).md "wikilink")
    撰，陈梓 补订
  - 《林和靖诗集》三卷，（[宋](../Page/宋.md "wikilink")）[林逋](../Page/林逋.md "wikilink")
    撰，（[清](../Page/清.md "wikilink")）陈梓辑，（[清](../Page/清.md "wikilink")
    [乾隆十年](../Page/乾隆.md "wikilink")）深柳读书堂
    刻本，（[清](../Page/清.md "wikilink")）[管庭芬](../Page/管庭芬.md "wikilink")
    批校
  - 《四书质疑》
  - 《经义质疑》
  - 《删后诗文存》
  - 《客星零草》
  - 《寓峡草》
  - 《九九乐府》
  - 《一知杂著》
  - 《定泉诗话》

## 评价

  - “鸿博征车未足劳，定泉桥上客星高。凤栖坊冷遗诗在，一鹤青天见羽毛。”——清末诗人[沈涛](../Page/沈涛.md "wikilink")
    《幽湖百咏》
  - “陈先生书法卓绝人间，而世人知之者绝少，方知传世者必麻笺十万，始有人知也。”——清
    [康有为](../Page/康有为.md "wikilink")
  - “却将颜柳千钧力，散入张王草法中。满纸龙蛇惊起伏，平生拜倒潁川公。”
    “重如磐石细如针，大小相参信有神。融合全篇成一字，后来难继古无人。”
    “晚来病臂十余年，左腕匆匆草几篇。若把公书较南阜，力沉魄厚过前贤。”
    ——原[西泠印社社长](../Page/西泠印社.md "wikilink")
    [张宗祥](../Page/张宗祥.md "wikilink")

## 参考

  - [清代大书家陈梓](https://web.archive.org/web/20070927175250/http://www.jxcnt.com/news/files/0501/050105_044131.php)
  - [雅客艺术 艺术家简介 清代
    陈梓](https://web.archive.org/web/20070930060259/http://www.yahqq.com/profile/view.asp?id=828)

[C陈](../Category/清朝作家.md "wikilink")
[C陈](../Category/清朝詩人.md "wikilink")
[C陈](../Category/清朝書法家.md "wikilink")
[C陈](../Category/余姚人.md "wikilink")
[C陈](../Category/1683年出生.md "wikilink")
[C陈](../Category/1759年逝世.md "wikilink")