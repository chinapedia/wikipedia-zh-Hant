[Lsh-Cy.jpg](https://zh.wikipedia.org/wiki/File:Lsh-Cy.jpg "fig:Lsh-Cy.jpg")\]\]
**陈源**，[字](../Page/表字.md "wikilink")**通伯**，[笔名](../Page/笔名.md "wikilink")**西滢**，[中国文学家](../Page/中国.md "wikilink")，[江苏](../Page/江苏.md "wikilink")[无锡人](../Page/无锡.md "wikilink")。

## 生平

陈源1910年毕业于[南洋公学附属小学](../Page/南洋公学附属小学.md "wikilink")（今[南洋模范中学](../Page/南洋模范中学.md "wikilink")）。1912年在[表舅](../Page/表舅.md "wikilink")[吴稚晖的资助下](../Page/吴稚晖.md "wikilink")[留学](../Page/留学.md "wikilink")[英国](../Page/英国.md "wikilink")，在[爱丁堡大学和](../Page/爱丁堡大学.md "wikilink")[伦敦大学](../Page/伦敦大学.md "wikilink")[政治经济学专业学习](../Page/政治经济学.md "wikilink")。1922年回国，任[北京大学外文系教授](../Page/北京大学.md "wikilink")。1924年，陈源在[胡适的支持下与](../Page/胡适.md "wikilink")[徐志摩](../Page/徐志摩.md "wikilink")、[王世杰等共创](../Page/王世杰_\(中华民国政治家\).md "wikilink")《[现代评论](../Page/现代评论.md "wikilink")》杂志，主编其中的《闲话》专栏。在此期间，陈源与[鲁迅结怨](../Page/鲁迅.md "wikilink")，二人爆发多次[笔战](../Page/笔战.md "wikilink")。

1929年，陈任[武汉大学文学院院长](../Page/武汉大学文学院.md "wikilink")。1943年，陈赴[伦敦](../Page/伦敦.md "wikilink")，在[中英文化协会工作](../Page/中英文化协会.md "wikilink")，其间曾帮助[李四光摆脱](../Page/李四光.md "wikilink")[英国政府阻挠回国](../Page/英国政府.md "wikilink")。1946年，陈被[中华民国](../Page/中华民国.md "wikilink")[国民政府任命为驻](../Page/国民政府.md "wikilink")[联合国教科文组织首任代表](../Page/联合国教科文组织.md "wikilink")，常驻[法国](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。

1964年，法国与[中华人民共和国建交](../Page/中华人民共和国.md "wikilink")，中华民国外交代表团被迫回国。陈源遂以驻联合国代表的名义奉命留守巴黎，后被[法国警察强行架出](../Page/法国警察.md "wikilink")，导致其当场[心脏病发作而昏厥](../Page/心脏病.md "wikilink")。1966年，陳源因病[退休](../Page/退休.md "wikilink")，后居住于伦敦，并于1970年因[中风病逝于当地](../Page/中风.md "wikilink")。

陈源之妻是女作家[凌叔华](../Page/珞珈三女杰.md "wikilink")。

[category:无锡人](../Page/category:无锡人.md "wikilink")

[Category:中華民國外交官](../Category/中華民國外交官.md "wikilink")
[Category:中國駐聯合國教科文組織代表](../Category/中國駐聯合國教科文組織代表.md "wikilink")
[Category:中華民國作家](../Category/中華民國作家.md "wikilink")
[Category:武漢大學教授](../Category/武漢大學教授.md "wikilink")
[Category:北京大學教授](../Category/北京大學教授.md "wikilink")
[Category:第二届国民参政会参政员](../Category/第二届国民参政会参政员.md "wikilink")
[Category:第三届国民参政会参政员](../Category/第三届国民参政会参政员.md "wikilink")
[Category:第四届国民参政会参政员](../Category/第四届国民参政会参政员.md "wikilink")
[Category:愛丁堡大學校友](../Category/愛丁堡大學校友.md "wikilink")
[Category:倫敦大學校友](../Category/倫敦大學校友.md "wikilink")
[Category:上海市南洋模范中学校友](../Category/上海市南洋模范中学校友.md "wikilink")
[Category:移民英国的中华民国人](../Category/移民英国的中华民国人.md "wikilink")
[Category:在法国的中华民国人](../Category/在法国的中华民国人.md "wikilink")
[Y源](../Category/陳姓.md "wikilink")