**陈舜礼**（），著名[学者](../Page/学者.md "wikilink")，前[山西大学](../Page/山西大学.md "wikilink")[校长](../Page/校长.md "wikilink")。[浙江省](../Page/浙江省.md "wikilink")[奉化县](../Page/奉化县.md "wikilink")（[宁波](../Page/宁波.md "wikilink")）人。

## 简历

1939年，毕业于[清华大学](../Page/清华大学.md "wikilink")[经济学系](../Page/经济学.md "wikilink")。陈舜礼曾担任[国民政府资源委员会](../Page/国民政府.md "wikilink")[昆明办事处经济研究室的助理](../Page/昆明.md "wikilink")，并担任[中国农业银行在](../Page/中国农业银行.md "wikilink")[成都](../Page/成都.md "wikilink")、[兰州](../Page/兰州.md "wikilink")、[昆明等城市的办事员和主任](../Page/昆明.md "wikilink")。1949年，毕业于[英国](../Page/英国.md "wikilink")[牛津大学](../Page/牛津大学.md "wikilink")。并曾在[法国](../Page/法国.md "wikilink")[巴黎大学学习](../Page/巴黎大学.md "wikilink")。

1949年后，先后担任[天津](../Page/天津.md "wikilink")[南开大学](../Page/南开大学.md "wikilink")[教授](../Page/教授.md "wikilink")，[南开大学副教务长](../Page/南开大学.md "wikilink")。

1959年后，先后担任[山西大学](../Page/山西大学.md "wikilink")[教授](../Page/教授.md "wikilink")，[山西大学教务长](../Page/山西大学.md "wikilink")，[山西大学](../Page/山西大学.md "wikilink")[图书馆馆长](../Page/图书馆.md "wikilink")。1982年4月-1983年9月，担任[山西大学](../Page/山西大学.md "wikilink")[校长](../Page/校长.md "wikilink")。

## 任职

陈舜礼并是[山西省第](../Page/山西省.md "wikilink")4和第5届[政协副](../Page/政协.md "wikilink")[主席](../Page/主席.md "wikilink")，[中国民主促进会](../Page/中国民主促进会.md "wikilink")[天津市委员会副主任委员](../Page/天津市.md "wikilink")，[中国民主促进会第](../Page/中国民主促进会.md "wikilink")3届中央候补委员，[中国民主促进会第](../Page/中国民主促进会.md "wikilink")4届中央委员，[中国民主促进会第](../Page/中国民主促进会.md "wikilink")7届中央副[主席并兼任执行局主任](../Page/主席.md "wikilink")。

1988年，当选为[中国民主促进会第](../Page/中国民主促进会.md "wikilink")8届中央副[主席](../Page/主席.md "wikilink")。1997年12月，陈舜礼被推举为[中国民主促进会中央名誉副](../Page/中国民主促进会.md "wikilink")[主席](../Page/主席.md "wikilink")。

陈并是第7届[全国人民代表大会常务委员会委员](../Page/全国人民代表大会常务委员会.md "wikilink")，第4、5届[全国政协委员](../Page/全国政协.md "wikilink")，第6届[全国政协常委](../Page/全国政协.md "wikilink")。

## 参考

  - [山西大学 \>\> 陈舜礼校长
    简介](https://web.archive.org/web/20070926231306/http://www.sxu.edu.cn/xxgk/lrxz/csl.htm)
  - [中国民主促进会 \>\> 陈舜礼副主席
    简介](https://web.archive.org/web/20050216125023/http://zibominjian.org/minzhu/297.htm)
  - [（新闻）中国民主促进会中央委员会名誉副主席陈舜礼逝世](http://society.eastday.com/epublish/gb/paper148/20031219/class014800003/hwz1060679.htm)

[Category:华人经济学家](../Category/华人经济学家.md "wikilink")
[Category:中華人民共和國經濟學家](../Category/中華人民共和國經濟學家.md "wikilink")
[Category:中國民主促進會中央委員會副主席‎](../Category/中國民主促進會中央委員會副主席‎.md "wikilink")
[Category:山西大学校长](../Category/山西大学校长.md "wikilink")
[Category:山西大学教授](../Category/山西大学教授.md "wikilink")
[Category:南开大学教授](../Category/南开大学教授.md "wikilink")
[Category:第七届全国人大常委会委员](../Category/第七届全国人大常委会委员.md "wikilink")
[Category:第四屆全國政協委員](../Category/第四屆全國政協委員.md "wikilink")
[Category:第五屆全國政協委員](../Category/第五屆全國政協委員.md "wikilink")
[Category:第六届全国政协常务委员](../Category/第六届全国政协常务委员.md "wikilink")
[Category:牛津大学校友](../Category/牛津大学校友.md "wikilink")
[Category:巴黎大学校友](../Category/巴黎大学校友.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:南京市金陵中学校友](../Category/南京市金陵中学校友.md "wikilink")
[Category:奉化人](../Category/奉化人.md "wikilink")
[S](../Category/陳姓.md "wikilink")