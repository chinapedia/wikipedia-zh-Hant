[Apollo_15_descends_to_splashdown.jpg](https://zh.wikipedia.org/wiki/File:Apollo_15_descends_to_splashdown.jpg "fig:Apollo_15_descends_to_splashdown.jpg")仍然安全降落在海上。\]\]

**降落伞**（又称**保險傘**），是指在[航空科学中](../Page/航空科学.md "wikilink")，主要由透气的[丝绸](../Page/丝绸.md "wikilink")[织物制成](../Page/织物.md "wikilink")，并可折叠包装在伞包或伞箱内的物品。使用时將降落伞充气展开，能使人或[物体减速](../Page/物体.md "wikilink")、稳定地降落。降落伞通常有一个面积很大的伞盖，可以产生很大的空气阻力。降落伞是[空降兵的重要装备](../Page/空降兵.md "wikilink")。下落的人或物体通过绳索与伞盖相连，以此保证在空中下落的人或物体的安全\[1\]。

## 历史

[Le_Gascon_envolé.jpg](https://zh.wikipedia.org/wiki/File:Le_Gascon_envolé.jpg "fig:Le_Gascon_envolé.jpg")
学者认为，降落伞是[阿拉伯人在公元](../Page/阿拉伯人.md "wikilink")9世纪左右发明的。在欧洲[中世紀時期亦有类似降落伞的工具的記載](../Page/中世紀.md "wikilink")。

在1178年亦有[伊斯蘭教徒从](../Page/伊斯蘭教.md "wikilink")[高楼跳下來](../Page/高楼.md "wikilink")，但重傷而亡。1485年，[達文西在](../Page/達文西.md "wikilink")[米蘭畫下了降落傘的草圖](../Page/米蘭.md "wikilink")，设计了降落傘的初步形象。1617年[克羅地亞的發明家](../Page/克羅地亞.md "wikilink")根據[達文西的降落傘草圖製作了成品](../Page/達文西.md "wikilink")，并进行了降落實驗。

1783年，[法國人](../Page/法國.md "wikilink")[路易-塞巴斯蒂安·雷诺曼研製了新型的降落傘](../Page/路易-塞巴斯蒂安·雷诺曼.md "wikilink")\[2\]\[3\]。1785年，[讓-皮埃爾·布蘭查德使用降落傘從](../Page/讓-皮埃爾·布蘭查德.md "wikilink")[熱氣球上安全跃下](../Page/熱氣球.md "wikilink")。

隨后，人们在降落傘的框架上面铺設了用[麻布製作的東西](../Page/麻.md "wikilink")，增大[空气阻力](../Page/空气阻力.md "wikilink")，从而減低了降落伞的危險性。1790年代，有人用更輕的[絲製品試製了降落傘](../Page/丝绸.md "wikilink")。1797年，[安德烈-雅克·加纳林用新的絲製降落傘進行了降落试验](../Page/安德烈-雅克·加纳林.md "wikilink")。随后，加纳林又設計了為降落傘排氣口增加[穩定性的降落伞](../Page/穩定性.md "wikilink")\[4\]\[5\]。

1912年3月1日，[美國](../Page/美國.md "wikilink")[陸軍上尉](../Page/陸軍.md "wikilink")在[密蘇里州進行了來自飛機降落傘的測試](../Page/密蘇里州.md "wikilink")。

1913年，第一次取得現代降落傘的[專利權](../Page/專利權.md "wikilink")。

## 組成部分

  - 傘衣
  - 引導傘
  - 傘繩
  - 背帶系統
  - 開傘配件
  - 傘包[Type60_parachute.JPG](https://zh.wikipedia.org/wiki/File:Type60_parachute.JPG "fig:Type60_parachute.JPG")陳列所屬空降單位的（主傘背負）以及備用傘（前腹掛載）\]\]
  - 尼龍

## 用途

  - 於[空难时拯救](../Page/空难.md "wikilink")[飞行员的性命](../Page/飞行员.md "wikilink")
  - 保持飞机的稳定性（[控制構型載具](../Page/控制構型載具.md "wikilink")）
  - 降低飞机着陆时的速度
  - 在空中回收[飞行器](../Page/飞行器.md "wikilink")
  - [傘兵空降以進行軍事任務](../Page/傘兵.md "wikilink")（如敵後突襲、偷襲等）
  - 災難發生時空投物資
  - 一種現代娱乐活动

[F-117_lands.jpg](https://zh.wikipedia.org/wiki/File:F-117_lands.jpg "fig:F-117_lands.jpg")

## 分類

[USMC_Paratrooper.jpg](https://zh.wikipedia.org/wiki/File:USMC_Paratrooper.jpg "fig:USMC_Paratrooper.jpg")

### 人用伞

人用伞主要分為救生伞、伞兵傘、教练傘、运动伞、备份伞、[滑翔伞](../Page/滑翔伞.md "wikilink")\[6\]。

  - 美軍\[7\]
      - T-7降落傘

      -
      -
      - MC1-1C降落傘

<!-- end list -->

  - 日本陸上自衛隊(JGSDF)
      -
      -
### 阻力伞

阻力伞主要分為胸式伞、背式伞、坐式伞、阻力伞（减速伞）。

### 物用伞

物用伞主要分為投物伞、航弹伞、回收伞\[8\]。

### 特种用途伞

特种用途伞主要分為反尾旋伞、水下用伞、稳定伞、布雷伞。

## 參見

  - [跳傘](../Page/跳傘.md "wikilink")

  -
  - （已鮮少運用）

  -
  -
  - 下滑比（glide ratio） - 航空機械工程

  - [流体力学](../Page/流体力学.md "wikilink")

  - [終端速度](../Page/終端速度.md "wikilink")

## 參考資料

<div class="references-small">

  - [降落伞資料](https://web.archive.org/web/20070629061419/http://www.bjkp.gov.cn/texworld/tex/wande/m11-1.htm)

<references />

</div>

[Category:工具](../Category/工具.md "wikilink")
[Category:跳伞](../Category/跳伞.md "wikilink")
[Category:意大利发明](../Category/意大利发明.md "wikilink")
[Category:體育器材](../Category/體育器材.md "wikilink")

1.  《中國大百科全書·軍事Ⅰ》，[北京](../Page/北京.md "wikilink")，[中國大百科全書出版社](../Page/中國大百科全書出版社.md "wikilink")，1992年，第473頁。

2.
3.

4.

5.

6.
7.  [Early History of
    Parachuting](http://www.apf.asn.au/APF-Zone/APF-Information/History-of-the-APF/Early-History-of-Parachuting/default.aspx)
    - Australian Parachute Federation

8.