**陕西大秦足球俱乐部**，是在原[四川足球俱乐部的投资方美联蜀宣布放弃投资球队后](../Page/四川足球俱乐部.md "wikilink")，原球队人员在2011年成立的新俱乐部。前身为**四川都江堰欣宝足球俱乐部**，2012年1月在[陕西浐灞宣布迁入](../Page/陕西浐灞.md "wikilink")[贵州后](../Page/贵州.md "wikilink")，俱乐部随即宣布迁入[陕西](../Page/陕西.md "wikilink")。陕西大秦参加[2012年中国足球乙级联赛南区比赛](../Page/2012年中国足球乙级联赛.md "wikilink")，其后未参加[2013年中乙比赛](../Page/2013年中国足球乙级联赛.md "wikilink")。

## 简介

[Dujiangyan_Xinbao_FC_logo.jpg](https://zh.wikipedia.org/wiki/File:Dujiangyan_Xinbao_FC_logo.jpg "fig:Dujiangyan_Xinbao_FC_logo.jpg")
[2010年乙级联赛结束后](../Page/2010年中国足球乙级联赛.md "wikilink")，四川足球俱乐部因缺钱缺人一度面临解散。2011年4月，俱乐部一分为二，原投资方美联蜀宣布放弃投资成年队，与四川省足协另外合建四川全运队，以“四川足球俱乐部川大队”名义参加新赛季[乙级联赛](../Page/2011年中国足球乙级联赛.md "wikilink")\[1\]；而原球队主要人员得到西安一家企业的投资，新成立了**四川都江堰欣宝足球俱乐部**，也参加乙级联赛。

2012年1月在[陕西浐灞宣布迁入](../Page/陕西浐灞.md "wikilink")[贵州后](../Page/贵州.md "wikilink")，俱乐部随即宣布迁入[陕西](../Page/陕西.md "wikilink")，其后在网上公开征集俱乐部的新名称，经过投票后決定使用**陕西大秦足球俱乐部**这个名称\[2\]。

| 年份    | 赛事                                              | 赛区 | 名称      |
| ----- | ----------------------------------------------- | -- | ------- |
| 2011年 | [中国足球乙级联赛](../Page/2011年中国足球乙级联赛.md "wikilink") | 南区 | 四川都江堰欣宝 |
| 2012年 | [中国足球乙级联赛](../Page/2012年中国足球乙级联赛.md "wikilink") | 北区 | 陕西大秦    |
|       |                                                 |    |         |

## 球员荣誉

  - [石俊](../Page/石俊.md "wikilink")：[2011年中国足球乙级联赛最佳射手](../Page/2011年中国足球乙级联赛.md "wikilink")

## 参见

  - [四川全兴](../Page/四川全兴.md "wikilink")
  - [四川足球俱乐部](../Page/四川足球俱乐部.md "wikilink")

## 参考资料

<div class="references-small">

<references />

</div>

[Category:中国足球俱乐部](../Category/中国足球俱乐部.md "wikilink")
[Category:陕西足球](../Category/陕西足球.md "wikilink")
[Category:四川足球史](../Category/四川足球史.md "wikilink")
[Category:2011年建立的足球俱樂部](../Category/2011年建立的足球俱樂部.md "wikilink")

1.  [四川资方正式弃所属成年队
    长线投资美联蜀赞助](http://sports.sohu.com/20110406/n280152053.shtml)
2.  [原四川都江堰欣宝更名“陕西大秦队”](http://sichuan.scol.com.cn/fffy/content/2012-02/11/content_3394954.htm?node=894)