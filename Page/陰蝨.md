**陰蝨**（*Pthirus
pubis*）是一種[寄生於](../Page/寄生.md "wikilink")[人體](../Page/人體.md "wikilink")[毛髮的](../Page/毛髮.md "wikilink")[寄生虫](../Page/寄生虫.md "wikilink")，長約1至3毫米，無[翼](../Page/翼.md "wikilink")。因常見於[阴部](../Page/阴部.md "wikilink")，故稱陰蝨。另外，由於陰蝨身體扁平，远看如同皮屑，细看则如同小[螃蟹](../Page/螃蟹.md "wikilink")，故在英語中又稱蟹蝨
（Crab louse）。

## 簡介

雖然最常見於陰部，但陰蝨在絕大部份人類毛髮上均可生存。它的身體較[頭蝨寬闊](../Page/頭蝨.md "wikilink")，腳部較寬廣，使它可於疏落的毛髮間攀爬。除了[陰毛外](../Page/陰毛.md "wikilink")，阴虱也可在[头发](../Page/头发.md "wikilink")、[睫毛](../Page/睫毛.md "wikilink")、[鬍鬚和](../Page/鬍鬚.md "wikilink")[腋毛处寄生](../Page/腋毛.md "wikilink")。

和其他[蝨子一樣](../Page/蝨.md "wikilink")，陰蝨以人類的[血液為](../Page/血液.md "wikilink")[食物](../Page/食物.md "wikilink")，因此陰蝨棲息的患處會不時感到痕癢。另外，陰蝨亦可能帶有其他[病菌](../Page/病菌.md "wikilink")，使患者產生[感染](../Page/感染.md "wikilink")。

陰蝨的傳播途徑主要是[性交](../Page/性交.md "wikilink")，因此陰蝨感染被認為是[性传播疾病的一種](../Page/性传播疾病.md "wikilink")。[衣物和](../Page/衣物.md "wikilink")[床鋪也是陰蝨的重要傳播途徑](../Page/床.md "wikilink")。[廁所則](../Page/廁所.md "wikilink")**不會**傳染陰蝨，因為廁所的平面光滑，缺乏可讓蝨子停留的條件。如廁前不久如患有陰蝨患者剛使用過,亦有被傳染之可能。

## 生命週期

阴虱为[不完全变态发育](../Page/不完全变态.md "wikilink")，其生活史分为：卵、若虫和成虫三个阶段。

1.  **[卵](../Page/卵.md "wikilink")**：陰蝨的蟲卵一般都緊附在阴毛的毛幹根部上，呈[淡红色或](../Page/淡红色.md "wikilink")[铁锈色的](../Page/铁锈.md "wikilink")[橢圓形](../Page/橢圓形.md "wikilink")，类似于点状血痂。孵化期約為一星期。
2.  **[若虫](../Page/若虫.md "wikilink")**：若虫是阴虱自卵孵化后，成长为成虫之前的形态。若虫的样子与成虫相似，但是要小很多。若虫的成長期亦為一星期左右。
3.  **[成虫](../Page/成虫.md "wikilink")**：阴虱的成蟲呈[灰白色或](../Page/灰白色.md "wikilink")[褐色](../Page/褐色.md "wikilink")，[雌蟲一般比](../Page/雌性.md "wikilink")[雄蟲為大](../Page/雄性.md "wikilink")。

一般陰蝨的[壽命是](../Page/壽命.md "wikilink")30天，但所有脫離人體的陰蝨均會在2天內[死亡](../Page/死亡.md "wikilink")。

## 症状

被阴虱寄生后，通常会出现如下症状：

1.  阴部搔痒，特别是夜间搔痒更甚；
2.  阴部出现红疹、丘疹、血痂或青斑；
3.  内裤上有铁锈色粉末状或颗粒状虱粪；
4.  阴毛根部可发现铁锈色或红褐色椭圆形虫卵；
5.  阴毛处发现阴虱活动。

## 治療

1.  剃净阴毛及肛周毛发并将其焚烧；
2.  将被污染的衣物、床单、被罩蒸煮或开水浇烫消毒，杀灭虫卵及成虫；
3.  使用热水及肥皂（[硫磺皂](../Page/硫磺皂.md "wikilink")）清洗阴部；

## 外部連結

  - [MedlinePlus Medical Encyclopedia: Pubic
    lice](http://www.nlm.nih.gov/medlineplus/ency/article/000841.htm)

  - [University of chicago: Crab
    lice](https://web.archive.org/web/20060908151229/http://scc.uchicago.edu/crabs.htm)

  - [EmbarrassingProblems.com](http://www.embarrassingproblems.com):
    Crabs

  - [Genetic Analysis of Lice Supports Direct Contact between Modern and
    Archaic
    Humans](https://web.archive.org/web/20040405043242/http://www.plosbiology.org/plosonline/?request=get-document)

  -
[Category:寄生蟲](../Category/寄生蟲.md "wikilink")
[Category:蝨亞目](../Category/蝨亞目.md "wikilink")
[Category:性病](../Category/性病.md "wikilink")
[Category:害蟲](../Category/害蟲.md "wikilink")