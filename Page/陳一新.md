**陳一新**（），[籍貫](../Page/籍貫.md "wikilink")[中國](../Page/中國.md "wikilink")[湖南](../Page/湖南.md "wikilink")[長沙](../Page/長沙.md "wikilink")，生於英屬[香港](../Page/香港.md "wikilink")，最後逃難到[台灣](../Page/台灣.md "wikilink")，美國哥倫比亞大學政治學博士，[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）[政治學學者與](../Page/政治學.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，曾代表[新黨出任](../Page/新黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，現在是[淡江大學美洲研究所美國研究組教授兼中國國民黨智庫學者](../Page/淡江大學.md "wikilink")。

## 生平

## 新聞事件

2010年駕車撞傷女教師遭台北地方法院判刑\[1\]。

2015年，[國民黨智庫](../Page/國民黨.md "wikilink")「[國家政策基金會](../Page/國家政策基金會.md "wikilink")」學者陳一新以「美方看破蔡英文手腳？」為名投書[媒體](../Page/媒體.md "wikilink")，指出三月份時，[美國眾議院外委員成員來訪時談話](../Page/美國眾議院.md "wikilink")，包括指稱眾院外委會主席[羅伊斯](../Page/羅伊斯.md "wikilink")(Ed
Royce)在會中，有所謂「台灣既要加入[RCEP](../Page/RCEP.md "wikilink")，為何支持[太陽花學運](../Page/太陽花學運.md "wikilink")？」等等內容。[民進黨](../Page/民進黨.md "wikilink")[發言人](../Page/發言人.md "wikilink")[鄭運鵬否認此事](../Page/鄭運鵬.md "wikilink")，並表示[太陽花學運期間](../Page/太陽花學運.md "wikilink")，美國國會包括[羅伊斯在內的許多參](../Page/羅伊斯.md "wikilink")、眾兩院國會議員，都表示支持，羅伊斯議員更在期間多次支持台灣應加入[TPP以分散](../Page/TPP.md "wikilink")[國際市場](../Page/國際.md "wikilink")\[2\]。

## 外部連結

  - [陳一新個人網頁](http://mail.tku.edu.tw/066368/)
  - [淡江大學介紹](http://www.tifx.tku.edu.tw/info/22)

## 參考資料

[C陳](../Page/category:第3屆中華民國立法委員.md "wikilink")

[陈](../Category/陈姓.md "wikilink") [陈](../Category/台灣外省人.md "wikilink")
[陈](../Category/台灣戰後湖南移民.md "wikilink")
[C陳](../Category/新黨黨員.md "wikilink")
[C陳](../Category/淡江大學校友.md "wikilink")
[C陳](../Category/淡江大學教授.md "wikilink")
[C陳](../Category/哥伦比亚大学校友.md "wikilink")

1.  [撞傷女老師　前新黨立委陳一新被判刑3個月](http://news.ltn.com.tw/news/society/breakingnews/541866)
2.  [藍智庫學者造假？鄭運鵬：陳一新應向民進黨及社會公開道歉](http://www.peoplenews.tw/news/22bedae3-cc2b-40f5-ad2d-4261a92b9fea)