|          |
| -------- |
|          |
| **原名**   |
| **別號**   |
| **出生地**  |
| **出生日期** |
| **籍貫**   |

**陳乙東**（英文名：，），祖籍[中國](../Page/中國.md "wikilink")[廣東](../Page/廣東.md "wikilink")[潮州](../Page/潮州.md "wikilink")，生於[香港](../Page/香港.md "wikilink")，曾四度宣布參與[香港特首選舉及補選以及](../Page/香港特別行政區行政長官.md "wikilink")[2007年香港立法會港島選區補選](../Page/2007年香港立法會港島選區補選.md "wikilink")，然而從未成為正式候選人。參選期間以出位言論、大膽構思及連串起訴對手的法律行動，曾在報章上曝光，並熱心於各項政治運動。

其中「**巴士阿叔**」的身份最為人所熟識：自2006年4月底，網上出現一段《[巴士阿叔](../Page/巴士阿叔.md "wikilink")》影片，後陳乙東自稱為片中人。

他為香港網站[Uwants形象代言人](../Page/Uwants.md "wikilink")，為Uwants網站建立「巴士阿叔」的形象：粗口、不計較禮貌、高聲鬧人，自我中心，不理別人。

受到各界熱議，站在鎂光燈之下，也被連鎖網吧相中，為香港一網吧集團作為宣傳代言人，名利雙收\[1\]\[2\]\[3\]。

## 生平

陳乙東接受傳媒訪問時，曾提及自己在各國的生活及學歷，但有傳媒要求他給予學歷證明等資料時卻未能提供。**其生平多由他向傳媒自述而得，部份內容真確性暫無可考**，不少報章在訪問他後同時引述不同的精神病科[醫生](../Page/醫生.md "wikilink")、[社會學家](../Page/社會學家.md "wikilink")、[藝術家和圈中人的意見](../Page/藝術家.md "wikilink")，試圖從不同角度探討他的行為。

### 家族與童年

陳乙東有一兄六妹，父親從前在[九龍城開中藥行](../Page/九龍城.md "wikilink")，出生時便患上[腦膜炎](../Page/腦膜炎.md "wikilink")，至六歲康復（其中在一歲時赴[台定居](../Page/台.md "wikilink")），讀書時又經常和同學打架，不停記大過，換了三間小學。中學時曾於[新法書院就讀](../Page/新法書院.md "wikilink")。

### 留學外地

陳乙東自稱曾於台灣[中國文化大學附屬藝術專科學校舞蹈系就讀](../Page/中國文化大學.md "wikilink")。

陳乙東在[澳洲](../Page/澳洲.md "wikilink")[悉尼留學](../Page/悉尼.md "wikilink")，聲稱曾中彩票2000多萬元。16歲時已擁有房車與別墅，更自資一間外賣店，但流連賭場、夜總會與的士高，把獎金輸光變得一無所有。

### 流浪列國惹官非

他聲稱之後到處流浪，在[德國](../Page/德國.md "wikilink")[慕尼黑讀書時](../Page/慕尼黑.md "wikilink")，更試過在駕車途中撞死人不顧而去，被警方拘捕並判入獄8年，後來他上訴成功，監禁9個月後獲釋。在[英國時](../Page/英國.md "wikilink")，又因與情敵互相推撞時對方從樓梯失足跌死而被控誤殺，但閉路電視拍下情況故罪名不成立，但立即被遣返香港。

回港後不獲父母收留，曾露宿[中環](../Page/中環.md "wikilink")[皇后像廣場](../Page/皇后像廣場.md "wikilink")，後來他在[土瓜灣一間珠寶行做營業員](../Page/土瓜灣.md "wikilink")，但翌年（1990年）染上賭癮成為病態賭徒，每晚上賭船賭錢，曾於半個月輸掉百多萬，最後更欠下數百萬賭債，被迫運[白粉到](../Page/白粉.md "wikilink")[比利時抵償](../Page/比利時.md "wikilink")，但於海關被關員逮捕並判監八年，在比利時[布魯日監獄服刑](../Page/布魯日.md "wikilink")。獄中他鑽研[法學](../Page/法學.md "wikilink")、[營養學及](../Page/營養學.md "wikilink")[哲學](../Page/哲學.md "wikilink")，4年內修讀兩個[院士和一個](../Page/院士.md "wikilink")[碩士學位](../Page/碩士.md "wikilink")。獄中自修水果雕盤，曾憑此手藝獲當時比利時[国王](../Page/国王.md "wikilink")[博杜安一世賞識](../Page/博杜安一世.md "wikilink")，每星期由專車接載他到皇宮雕果盤獻技。

他聲稱自己1992年在獄中為香港撰寫政改方案並獲前港督[彭定康嘉許](../Page/彭定康.md "wikilink")。後來更借自己懂[中文](../Page/中文.md "wikilink")、[英文](../Page/英文.md "wikilink")、[德文](../Page/德文.md "wikilink")、[法文協助比利時當局翻譯中文版](../Page/法文.md "wikilink")[旅遊指南](../Page/旅遊指南.md "wikilink")，獲當時的比利時皇帝頒發嘉許狀。至1993年博杜安一世駕崩，陳乙東獲新任國王[阿尔贝二世特赦回港](../Page/阿尔贝二世_\(比利时\).md "wikilink")，其後一直失業，居於[元朗](../Page/元朗.md "wikilink")，以綜援維生。

他在接受《[壹周刊](../Page/壹周刊.md "wikilink")》專訪時表示他曾有兩段結婚，並在1981年為一女之父，但已離異，至女兒五歲後已沒有再見面。

## 巴士阿叔事件

******

陳乙東最廣為人所熟悉的是他的巴士阿叔事件。2006年4月27日晚上，一名中年男子在往[元朗的](../Page/元朗.md "wikilink")[巴士](../Page/巴士.md "wikilink")68X途中，因談電話向社工傾訴時被身後的青年拍膊頭，情緒激動怒罵對方，整個過程被另一名少年用手機拍下，後來短片中的名句「未解決」、「我有壓力」等在五月中旬開始被其他傳媒於節目上多次引用後，短片知名度浮上香港傳媒，五月下旬開始被英國《[衛報](../Page/衛報.md "wikilink")》及美國《[紐約時報](../Page/紐約時報.md "wikilink")》、《[CNN](../Page/CNN.md "wikilink")》等國際傳媒報導。陳乙東事後自稱為該男子因此一躍成為香港城中茶餘飯後之人物。但陳乙東在2006年5月底露面時留有長髮，與片中男人的短髮尾明顯有異，故有人質疑陳是否冒名頂替，藉以牟私。

### 有償新聞

2006年5月底，《壹週刊》聲稱巴士阿叔為陳乙東，但見刊翌日香港多份報紙指陳乙東向傳媒索取8000至10萬元不等的「採訪費」，但未能成功，隨後有報章質疑《壹週刊》是否曾向陳乙東提供利益，換取訪問。2006年6月1日出版之《壹週刊》曾為陳乙東進行個人專訪，透過不同專訪短片流露出陳乙東鮮為人知的另一面，包括有「冷靜自白（我想自殺）」、「唱卡啦OK抒發感情（英語國語金曲）
」、「口技表演（模仿嬰兒、貓及小號）」、「[粗口論](../Page/粗口.md "wikilink")」、「對隻揪與壓力之詮釋」、「四眼仔何銳熙發脾氣
陳乙東以和為貴」等。

6月4日，[新傳媒旗下刊物](../Page/新傳媒.md "wikilink")《[東方新地](../Page/東方新地.md "wikilink")》亦發表文章，聲稱直擊陳乙東召妓實況\[4\]，並於網頁上提供當日現場影片供讀者下載。4日後，該雜誌指，該訪問原在該刊會客室進行，但陳要求提供消遣，記者「本著披露其真性情的精神」要求隨行採訪並不作意見。到達深圳沙嘴之妓行程以至在場片段之猥褻行為均為陳自發。記者出發前無向公司預支亦無提過會為陳付帳，但陳無意亦無錢結帳，鑑於場所品流複雜，故此記者「迫於無奈」「暫代墊支」。

### 成為連鎖網吧代言人

受到各界熱議，站在鎂光燈之下，也被連鎖網吧相中，為香港一網吧集團作為宣傳代言人，而據報導廣告合約有六位數字，成為現時巴士阿叔的主要收入來源。巴士阿叔的廣告合約包含了擔任該集團所舉辦的比賽left
4 dead x Cyols
巴士阿叔對抗賽。但參與的機迷狂轟巴士阿叔完全不懂玩該遊戲，並則疑主委譁眾取寵，巴士阿叔根本不是機迷，只是為作宣傳之用。有很多電子遊戲機迷搶著和巴士阿叔「揸揸手」（握握手），有些機迷更只有幾歲，可見巴士阿叔的影響已延伸到新一代。\[5\]\[6\]\[7\]

### 受聘及遇襲

憑籍巴士阿叔的知名度，陳乙東獲「[扒王之王](../Page/扒王之王.md "wikilink")」連鎖餐店創辦人李德麟賞識聘為公關，預計在6月9日簽署合約。6月6日接受美國[CNN攝製隊作專題訪問](../Page/CNN.md "wikilink")，在餐廳與[甄文達表演雕果盤手藝](../Page/甄文達.md "wikilink")\[8\]。

6月7日晚上9時許，他在該集團位於旺角的「山口料理」上班期間，被三人戴口罩的男子毆打，臉部和後腦受傷並留院觀察。而李德麟亦表示暫不安排陳在出院後在餐廳中的工作。遇襲同時並有《[東方日報](../Page/東方日報.md "wikilink")》、《[太陽報](../Page/太陽報.md "wikilink")》、《[蘋果日報](../Page/蘋果日報.md "wikilink")》記者在場並拍下受襲照片，重案組已接手調查。

### 「扒王之王」老闆娘企圖自殺

自6月3日陳乙東受聘於「扒王之王」工作後，李德麟之妻[林珍奇便對陳乙東感到不滿](../Page/林珍奇.md "wikilink")，陳乙東曾公開稱很喜歡老闆之女兒[李依琳](../Page/李依琳.md "wikilink")，此外陳乙東負面報道不絕，[林珍奇擔心這會連累公司](../Page/林珍奇.md "wikilink")，為此曾多次與丈夫爭執；6月11日早上8時多，林被發現在花園道愛都大廈寓所服食過量安眠藥企圖自殺，送院無大恙。

其後[林珍奇解釋事件全因與丈夫李德麟](../Page/林珍奇.md "wikilink")（「扒王之王」的老闆）爭論「炒阿叔」一事引起，加上自己失眠才會發生仰藥事件。事發當天她早上七時許仍不能入睡，服下一粒安眠藥亦未有睡意，這時她再因「炒阿叔」一事與夫爭吵，得悉李生幫阿叔交電費後感到十分憤怒，於是打電話與視作親人的老員工傾訴，怎料愈講愈激動，一時失理智便吞下數十粒安眠藥。她強調這時並非有意輕生，只是擔心丈夫及公司的形象受到影響，一時衝動所致。她表示，阿叔的負面新聞一直令她承受沉重的壓力和失眠，現時她只希望丈夫能與阿叔劃清界線，不要再有任何瓜葛，這樣她才可放下心頭大石\[9\]。

陳在晚上得悉後已致電向李交代事件，聲稱「北上召妓」一事，是因為無錢繳交家中水電費，在上工前一天才到深圳洗澡，並提出辭職，老闆接納了他的請辭\[10\]。

「扒王之王」李德麟其後轉介陳乙東到一間環保節水系統公司任環境拓展主任，負責到[洛杉磯推銷產品](../Page/洛杉磯.md "wikilink")，另當地一間華人電台已邀請他客串主持節目，他安頓後或會再到[馬來西亞等地工作](../Page/馬來西亞.md "wikilink")，聲稱未來一年都不會長時間在港逗留。

## 參與政治運動

### 參選香港行政長官選舉

陳乙東曾4次表示有意角逐[香港特首之位](../Page/香港特別行政區行政長官.md "wikilink")，現為有史以來宣布參選特首次數最多之人，但從未能成為正式候選人。

#### 概況

1996年10月首屆特首選舉索取表格時，因沒有帶備意願書而被拒。2002年2月第二屆特首選舉他捲土重來，到[選舉事務處遞交提名表](../Page/選舉事務處.md "wikilink")，但因未能得到一百位[選舉委員會成員支持再次遭拒](../Page/選舉委員會.md "wikilink")。2005年6月的補選，陳乙東再度參選亦難逃敗陣。他曾嘗試循法律途徑挑戰[曾蔭權入稟法院阻止他自動當選](../Page/曾蔭權.md "wikilink")，但最終不了了之，結果陳乙東主動向曾蔭權遞上「投降書」請降。2007年第三屆特首選舉，他於2月26日遞交提名表格，但未獲足夠（100位）選舉委員支持而被選舉主任[馮驊法官裁定無效](../Page/馮驊.md "wikilink")。

#### 參選政綱

他參選時提出重新組織香港義勇軍，解決失業問題；領養老人計劃；並以退役船隻作海上監獄，陸上監獄原址即可拍賣，解決財赤；在全港18區提供免費飯堂，讓市民食大鑊飯；他又建議學習[荷蘭](../Page/荷蘭.md "wikilink")，設立[紅燈區](../Page/紅燈區.md "wikilink")，防止性病蔓延；[黑社會企業化](../Page/黑社會.md "wikilink")，所有社團需要登記註冊領取牌照，以維持社會秩序及增加政府稅金。在[大嶼山建立](../Page/大嶼山.md "wikilink")[拉斯維加斯式賭場](../Page/拉斯維加斯.md "wikilink")。

#### 枝節事件

2007年1月27日表示在「競選」期間（1月10日）把選舉經費存於[-{恒生}-銀行旺角](../Page/恒生銀行.md "wikilink")[彌敦道分行](../Page/彌敦道.md "wikilink")，及後才知被銀行經紀「騙」取選舉經費令其精神受損，向銀行索償250萬3千圓。

### 參選香港立法會補選

2007年10月15日，宣布參加[2007年香港立法會港島選區補選](../Page/2007年香港立法會港島選區補選.md "wikilink")。他聲稱參選是為了延續逝世的民建聯主席馬力的愛國愛港精神。如果他成功當選，會推動醫療及教育改革。在籌募經費方面，他會逐家逐戶去募捐。\[11\]
由於至報名參選截止日仍未籌募到50,000元，未能成為候選人。

### 參與台灣政治運動

陳乙東於2006年[9月5日晚上](http://www.ettoday.com/2006/09/05/162-1986984.htm)
11時飛抵[台北](../Page/台北.md "wikilink")，擬邀[施明德擔當孫文黨主席](../Page/施明德.md "wikilink")，並表示他有[中華民國護照並將參與](../Page/中華民國護照.md "wikilink")[倒扁行動](../Page/百萬人民倒扁運動.md "wikilink")\[12\]。但與施明德助理見面後態度大變為挺扁，指施本人處理運動方針只是與陳水扁「五十步笑百步」、「搞運動為賺錢」\[13\]，隨即於翌日上午8時返港\[14\]，並表示9月9日抵台挺扁。

9月9日抵台後早上到台大醫院，欲以芭比娃娃、一首詩和[劉德華](../Page/劉德華.md "wikilink")「你到底愛誰」改編歌曲
[送給](http://www.ettoday.com/2006/09/09/301-1988912.htm)[吳淑珍但被勸離](../Page/吳淑珍.md "wikilink")，9月18日下午在[台北車站南區倒扁靜坐現場](../Page/台北車站.md "wikilink")，高喊「台灣萬歲」，但被倒扁參與者潑水與拳腳相向。\[15\]

到2007年，陳乙東自稱成立「中華人民共和國（新台灣特別行政區）」，自命為「總統特首」，但沒有得到大眾注意。

### 支持陳馮富珍競選世衛總幹事

戴著[鑽石金錶的陳乙東於](../Page/鑽石.md "wikilink")2006年11月8日突現身於[瑞士城市](../Page/瑞士.md "wikilink")[日內瓦](../Page/日內瓦.md "wikilink")，並自言是專程為代表[中國競選](../Page/中國.md "wikilink")[世界衛生組織總幹事的](../Page/世界衛生組織.md "wikilink")[陳馮富珍打氣](../Page/陳馮富珍.md "wikilink")，讚揚陳太於[沙士期間表現良好並為陳太撰寫支持信](../Page/嚴重急性呼吸道症候群.md "wikilink")。陳乙東一度欲進入會場不果，最後只好坐在附近餐廳邊飲橙汁和吃沙律，邊向傳媒發表其「競選[台灣總統政綱](../Page/台灣.md "wikilink")」。

他並揚言決定參選[2008年中華民國總統選舉](../Page/2008年中華民國總統選舉.md "wikilink")，他說此行的最終目的是到[法國](../Page/法國.md "wikilink")，入稟[歐盟人權法庭控告](../Page/歐盟人權法庭.md "wikilink")[聯合國到現在仍未批准台灣加入聯合國](../Page/聯合國.md "wikilink")。

失業多年的陳乙東，是靠領取[綜援生活](../Page/綜援.md "wikilink")，[瑞士的消費甚高](../Page/瑞士.md "wikilink")，加上機票的支出，當為陳馮富珍採訪的記者詢問陳乙東的外訪經費從何得來時，陳乙東則回應指是由一班「小粉絲」全力支持他。
\[16\]

### 參選2010年立法會補選

2010年4月8日，陳乙東向[選舉事務處提交提名表格](../Page/選舉事務處.md "wikilink")，參選[2010年立法會的港島區補選](../Page/2010年香港立法會地方選區補選.md "wikilink")。但卻得不到足夠提名，因此參選無效。\[17\]

## 其他行徑

2006年8月22日，陳乙東到醫院探望遇襲受傷[何俊仁議員遭拒時](../Page/何俊仁.md "wikilink")，他其後表示他已成立「中國孫文黨」並稱有參選特首及立法會之想法\[18\]。

8月26日因藝人[鍾欣桐被](../Page/鍾欣桐.md "wikilink")《壹本便利》偷拍並刊登其更衣照片，陳向之前在巴士上偷拍他的短片拍攝者方穎恆提民事起訴，並向警方投訴遭偷拍，但未獲警方受理\[19\]。

8月28日到高等法院入稟時表示當時並透露他再度失業\[20\]，但他的行徑與之前轉職後表示低調的口徑大相逕庭，加上其短片營造的人氣已大減與過去數月的一些行為帶來負面評價，這種摶取見報率之行為已未見受歡迎。

2007年12月1日，患有愛滋病的落選區議會候選人[張錦雄](../Page/張錦雄.md "wikilink")，晚上手持「真情擁抱愛滋」的牌子，在銅鑼灣崇光百貨門外，要求路人擁抱他，希望市民在「[世界愛滋日](../Page/世界愛滋日.md "wikilink")」當日能以行動消除對愛滋病者的歧視，接受同性戀者和愛滋病患者。不少路人與他擁抱，以示對他的支持。擔任愛滋病義工多年的候任區議員[麥國風到場與張錦雄擁抱](../Page/麥國風.md "wikilink")，全民健康動力主席[勞永樂和](../Page/勞永樂.md "wikilink")「巴士阿叔」陳乙東也到場支持。\[21\]

2015年4月21日，陳乙東因涉嫌於2013年[清洗黑錢約](../Page/清洗黑錢.md "wikilink")20萬港元，在[屯門裁判法院受審](../Page/屯門裁判法院.md "wikilink")。陳在自辯時指稱有關款項是一名歐洲朋友借給他醫治哮喘之用。\[22\]2015年5月19日，有關案件在屯門裁判法院作出裁決。裁判官指由於陳的自辯沒有出現任何不可信之處，加上有關洗黑錢指控只是建基於一些推論之上，故裁定陳脫罪。\[23\]

## 十年後採訪

[毛記電視於](../Page/毛記電視.md "wikilink")2016年，即[巴士阿叔事件十年後再度採訪其近況](../Page/巴士阿叔事件.md "wikilink")。\[24\]

## 參考

  - [巴士阿叔事件](../Page/巴士阿叔事件.md "wikilink")

## 参考文献

<div class="references-small">

  - 本條目部份個人資料與經歷摘自2006年6月1日出版之《壹周刊》

<references/>

</div>

## 外部連結

  - [頭條日報：巴士阿叔何許人也？](https://web.archive.org/web/20060613123259/http://www.hkheadline.com.hk/html/unclebus/index5.html)

  - [香港青年政策研究所：陳乙東](http://www2.uzone21.com/uplus/index.epl?id=430)

  -
  - [巴士阿叔網誌（一）](http://busuncle.hkblog.com.hk)

  - [巴士阿叔網誌（二）](http://www.hkheadline.com/blog/reply_blog_edit.asp?f=CHUQWXHDNQ91289&id=76818)

  - [巴士阿叔網誌（三）](http://space.uwants.com/index.php/3153983)

[Category:香港網絡紅人](../Category/香港網絡紅人.md "wikilink")
[C](../Category/香港政治人物.md "wikilink")
[Chan](../Category/香港潮汕人.md "wikilink")
[Y](../Category/陈姓.md "wikilink")
[C](../Category/新法書院校友.md "wikilink")

1.  <http://www.chinanews.com/ga/ga-kjww/news/2009/08-26/1835126.shtml>
2.  <http://finance.591hx.com/article/2010-05-12/0000052623s.shtml>
3.  <http://dailynews.sina.com/bg/chn/chnpolitics/chinanews/20090825/1904609004.html>
4.  [淫亂實況　直擊巴士阿叔召妓](http://www.inmediahk.net/public/article?item_id=119318&group_id=31)
     2006年6月4日　東方新地
5.  <http://www.chinanews.com/ga/ga-kjww/news/2009/08-26/1835126.shtml>
6.  <http://finance.591hx.com/article/2010-05-12/0000052623s.shtml>
7.  <http://dailynews.sina.com/bg/chn/chnpolitics/chinanews/20090825/1904609004.html>
8.
9.  [醜聞影響生意
    決意「炒阿叔」扒王妻仰藥保夫形象](http://the-sun.on.cc/channels/news/20060613/20060613024941_0000.html)
    - 2006年6月13日 - 太陽報
10. [「扒-{后}-」林珍奇仰藥逼炒巴士阿叔](http://paper.wenweipo.com/2006/06/12/HK0606120009.htm)
    - 2006年6月12日 - 香港文匯報
11. [明報即時新聞](http://www.mpinews.com/htm/INews/20071015/gb11027t.htm)
12. 原文 [1](http://news.yam.com/bcc/life/200609/20060906281728.html)
13. 原文 [2](http://www.ettoday.com/2006/09/06/162-1987273.htm)
14. 原文
    [3](http://news.sina.com.tw/articles/14/13/35/14133583.html?/politics/20060906.html)
15. 東森新聞報，[倒扁／台灣加油！　巴士大叔鬧場挨打又被潑水](http://www.ettoday.com/2006/09/18/91-1992641.htm)。
16. [明報](../Page/明報.md "wikilink")：[巴士阿叔日內瓦支持陳太](http://hk.news.yahoo.com/061107/12/1vy2r.html)。
17. 香港電台
18. [4](http://hk.news.yahoo.com/060822/12/1rs5x.html)
19. [5](http://the-sun.orisun.com/channels/news/20060827/20060827031756_0000_3.html)
20.
21. [6](http://udn.com/NEWS/WORLD/WOR1/4121443.shtml)
22. [巴士阿叔疑洗20萬黑錢](http://hk.apple.nextmedia.com/news/art/20150422/19121431)
23. [控方指控基於推論
    巴士阿叔脫洗黑錢罪](http://hk.apple.nextmedia.com/realtime/news/20150519/53754288)
24. [《愛．回帶》 22/12
    我有壓力未解決！](http://www.tvmost.com.hk/201612221106_video_loverwind_busuncle)