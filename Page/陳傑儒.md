**陳傑儒**（），[中華民國政治人物](../Page/中華民國.md "wikilink")，現為[無黨團結聯盟](../Page/無黨團結聯盟.md "wikilink")[秘書長](../Page/秘書長.md "wikilink")，曾任[中國國民黨籍立法委員](../Page/中國國民黨.md "wikilink")。

## 外部連結

[立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00139&stage=4)

[Category:第2屆中華民國立法委員](../Category/第2屆中華民國立法委員.md "wikilink")
[Category:第3屆中華民國立法委員](../Category/第3屆中華民國立法委員.md "wikilink")
[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:第8屆臺中縣議員](../Category/第8屆臺中縣議員.md "wikilink")
[Category:第7屆臺中縣議員](../Category/第7屆臺中縣議員.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:無黨團結聯盟黨員](../Category/無黨團結聯盟黨員.md "wikilink")
[Category:國立成功大學校友](../Category/國立成功大學校友.md "wikilink")
[Category:台中市人](../Category/台中市人.md "wikilink")
[J傑](../Category/陳姓.md "wikilink")