**陈冠霖**（），出生於[台灣](../Page/台灣.md "wikilink")[台北](../Page/台北.md "wikilink")，[台湾知名](../Page/台湾.md "wikilink")[男演員](../Page/男演員.md "wikilink")、[男歌手](../Page/男歌手.md "wikilink")。2007年在大型历史传奇电视剧《[梁山伯与祝英台](../Page/梁山伯与祝英台.md "wikilink")》饰演马文才，深受中國观众喜爱，有着“史上最帅马文才”的美誉。2014年以中國諜報劇《代號九耳犬》中傅約翰一角獲得上海東方電影頻道年度最具人氣男演員獎。

## 影視作品

### 電視劇（台灣）

<table>
<thead>
<tr class="header">
<th><p>年代</p></th>
<th><p>電視台</p></th>
<th><p>劇名</p></th>
<th><p>飾演</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/公視主頻.md" title="wikilink">公視</a></p></td>
<td><p>《<a href="../Page/我們這一班.md" title="wikilink">我們這一班</a>》</p></td>
<td><p>劉耀明</p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/在室男.md" title="wikilink">在室男</a>》(台灣作家劇場)</p></td>
<td><p>阿成（眼鏡仔）</p></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/公視主頻.md" title="wikilink">公視</a></p></td>
<td><p>《<a href="../Page/野孩子_(電視劇).md" title="wikilink">野孩子</a>》（人生劇展）</p></td>
<td><p>小馬</p></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/中視數位台.md" title="wikilink">中視</a></p></td>
<td><p>《<a href="../Page/君子蘭花.md" title="wikilink">君子蘭花</a>》</p></td>
<td><p>林弘偉</p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/狀元親家.md" title="wikilink">狀元親家</a>》</p></td>
<td><p>徐德義</p></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/富貴在天_(台灣電視劇).md" title="wikilink">富貴在天</a>》</p></td>
<td><p>黃志清</p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/一枝草一點露.md" title="wikilink">一枝草一點露</a>》</p></td>
<td><p>江正 (許明豐)</p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/台視主頻.md" title="wikilink">台視</a></p></td>
<td><p>《<a href="../Page/上錯樓梯睡錯床.md" title="wikilink">上錯樓梯睡錯床</a>》</p></td>
<td><p>許民安</p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/大腳阿媽.md" title="wikilink">大腳阿媽</a>》</p></td>
<td><p>林永順</p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/七世夫妻之梁山伯與祝英台.md" title="wikilink">七世夫妻之梁山伯與祝英台</a>》</p></td>
<td><p>英台八個哥哥之一</p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/超級電視台.md" title="wikilink">超視</a></p></td>
<td><p>《<a href="../Page/星願_(電視劇).md" title="wikilink">星願</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/伴阮過一生.md" title="wikilink">伴阮過一生</a>》</p></td>
<td><p>林正信</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/中視數位台.md" title="wikilink">中視</a></p></td>
<td><p>《<a href="../Page/旅人的故事.md" title="wikilink">旅人的故事</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/中視數位台.md" title="wikilink">中視</a></p></td>
<td><p>《<a href="../Page/多桑與紅玫瑰.md" title="wikilink">多桑與紅玫瑰</a>》</p></td>
<td><p>李逢春</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/台灣阿誠.md" title="wikilink">台灣阿誠</a>》</p></td>
<td><p>白錦明</p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/台灣霹靂火.md" title="wikilink">台灣霹靂火</a>》</p></td>
<td><p>王永清<br />
王阿土</p></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/大愛電視台.md" title="wikilink">大愛電視台</a></p></td>
<td><p>《<a href="../Page/單翼輕航機.md" title="wikilink">單翼輕航機</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/台視主頻.md" title="wikilink">台視</a></p></td>
<td><p>《<a href="../Page/少年史豔文.md" title="wikilink">少年史豔文</a>》(大陸劇名三劍奇緣)</p></td>
<td><p>翻云魔君</p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/天地有情.md" title="wikilink">天地有情</a>》</p></td>
<td><p>楊志峰</p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/中視數位台.md" title="wikilink">中視</a></p></td>
<td><p>《<a href="../Page/風雲II_-_七武器.md" title="wikilink">風雲II - 七武器</a>》</p></td>
<td><p>斷浪</p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/客家電視台.md" title="wikilink">客家電視台</a></p></td>
<td><p>《<a href="../Page/水色嘉南.md" title="wikilink">水色嘉南</a>》（華視拍攝）</p></td>
<td><p><a href="../Page/八田與一.md" title="wikilink">八田與一</a></p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/八大第一台.md" title="wikilink">八大第一台</a></p></td>
<td><p>《<a href="../Page/真愛無悔.md" title="wikilink">真愛無悔</a>》</p></td>
<td><p>高英傑</p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/華視主頻.md" title="wikilink">華視</a></p></td>
<td><p>《<a href="../Page/生命的太陽.md" title="wikilink">生命的太陽</a>》</p></td>
<td><p>阿虎</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/華視主頻.md" title="wikilink">華視</a></p></td>
<td><p>《<a href="../Page/舊情綿綿_(電視劇).md" title="wikilink">舊情綿綿</a>》</p></td>
<td><p>汪文廣(陳信宏)</p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/美麗人生_(民視電視劇).md" title="wikilink">美麗人生</a>》</p></td>
<td><p>李振華</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/華視主頻.md" title="wikilink">華視</a></p></td>
<td><p>《<a href="../Page/天長地久_(2006年電視劇).md" title="wikilink">天長地久</a>》</p></td>
<td><p>張茂松</p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/黑貓大旅社_(電視劇).md" title="wikilink">黑貓大旅社</a>》</p></td>
<td><p>張志隆</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td><p>《<a href="../Page/驚異傳奇.md" title="wikilink">驚異傳奇</a>》之作家的筆</p></td>
<td><p>作家鄭文思</p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/我一定要成功.md" title="wikilink">我一定要成功</a>》</p></td>
<td><p>何亞倫</p></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/又見阿郎.md" title="wikilink">又見阿郎</a>》</p></td>
<td><p>白建雄</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/天下父母心.md" title="wikilink">天下父母心</a>》</p></td>
<td><p>李文龍<br />
鍾世慶</p></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/家和萬事興_(2010年電視劇).md" title="wikilink">家和萬事興</a>》</p></td>
<td><p>陳家寶（阿海）<br />
方小祖</p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/牽手_(2011年電視劇).md" title="wikilink">牵手</a>》</p></td>
<td><p>馬立昇</p></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/天下女人心.md" title="wikilink">天下女人心</a>》</p></td>
<td><p>杜英騏(陳英騏)<br />
許少強</p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/世間情.md" title="wikilink">世間情</a>》</p></td>
<td><p>柯展弘</p></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/珍珠人生.md" title="wikilink">珍珠人生</a>》(台灣好戲)</p></td>
<td><p>林嘉和</p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/甘味人生.md" title="wikilink">甘味人生</a>》</p></td>
<td><p>李嘉亮<br />
諸葛譽（老K）</p></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/一家人.md" title="wikilink">一家人</a>》</p></td>
<td><p>洪志勇</p></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/炮仔聲_(台灣電視劇).md" title="wikilink">炮仔聲</a>》</p></td>
<td><p>林至明</p></td>
</tr>
<tr class="even">
<td><p>|<a href="../Page/華視主頻.md" title="wikilink">華視</a></p></td>
<td><p>《<a href="../Page/那一天我們去放風箏.md" title="wikilink">那一天我們去放風箏</a>》(華視劇展)</p></td>
<td><p>小智</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>|<a href="../Page/大愛電視台.md" title="wikilink">大愛電視台</a></p></td>
<td><p>《<a href="../Page/捨得_(電視劇).md" title="wikilink">捨得</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇（中國大陸）

| 年份    | 劇名                                                                               | 飾演        | 大陸首播         | 大陸上星          | 台灣             | 備註                         |
| ----- | -------------------------------------------------------------------------------- | --------- | ------------ | ------------- | -------------- | -------------------------- |
| 2003年 | |《[護國良相狄仁傑](../Page/護國良相狄仁傑.md "wikilink")》之《[风摧边关](../Page/风摧边关.md "wikilink")》 | 匡連海       |              | 2003央視CCTV-8  |                |                            |
| 2005年 | 《[中華英雄](../Page/中華英雄.md "wikilink")》                                             | 金太保       |              |               |                |                            |
| 2005年 | 《風雲2七武器》                                                                         | 斷浪        |              |               | 中視、衛視中文台       |                            |
| 2007年 | 《[情鎖](../Page/情鎖.md "wikilink")》                                                 | 王柏東       | 2007上海電視劇頻道  | 2008安徽衛視      | 2018八大第一台      |                            |
| 2007年 | 《[梁山伯與祝英台](../Page/梁山伯與祝英台_\(2007年電視劇\).md "wikilink")》                          | 馬文才       | 2007广州广播电视台  |               | 高點電視台          |                            |
| 2008年 | 《[一定愛幸福](../Page/一定愛幸福.md "wikilink")》                                           | 阿清        |              | 2008廈門衛視      | 東森電視台          | 特別客串第7～19集                 |
| 2008年 | 《[寧為女人](../Page/寧為女人.md "wikilink")》                                             | 張主順       | 2008上海電視劇頻道  |               | 民視             |                            |
| 2008年 | 《[媽媽為我嫁](../Page/媽媽為我嫁.md "wikilink")》                                           | 洪振剛       | 2008廣東珠江頻道   | 2009安徽衛視      | 民視             |                            |
| 2009年 | 《[又見阿郎](../Page/又見阿郎.md "wikilink")》                                             | 白建雄       | 2009央視       | 2009央視        | 民視             | 中國央視與台灣民視合資拍攝              |
| 2012年 | 《望海的女人》                                                                          | 邱詠賦 & 宋長文 | 2012湖北綜合頻道   | 2014安徽衛視      |                |                            |
| 2012年 | 《我的媽媽是天使》                                                                        | 姜勁宇       | 2012湖北經視     | 2013江西衛視      |                | 特別客串第1集                    |
| 2014年 | 《代號九耳犬》                                                                          | 傅約翰       | 2014上海東方電影頻道 | 2014廣東衛視、廣西衛視 |                |                            |
| 2015年 | 《獵刃》                                                                             | 吳永順       | 2015湖南經視     | 2016湖北衛視、河南衛視 |                |                            |
| 2016年 | 《護寶風雲》                                                                           | 張蘭庭       | 2016廣東經視     |               |                |                            |
| 2018年 | 《幸福的腳步》                                                                          | 陳冠霖       | 2018廈門衛視     | 2018廈門衛視      | 2018東風衛視、鴻基台灣台 | 特別客串第1、2、4、14、35-36、39-40集 |

### 电影作品

| 年份    | 片名                                   | 飾演                             | 合作演員                                                                                                                                        |
| ----- | ------------------------------------ | ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------- |
| 2001年 | 《[江湖龙女](../Page/江湖龙女.md "wikilink")》 | 馬場老闆                           | [林心如](../Page/林心如.md "wikilink")、[tae](../Page/tae.md "wikilink")、[吴毅将](../Page/吴毅将.md "wikilink")                                          |
| 2017年 | 《[鳳皇傳](../Page/鳳皇傳.md "wikilink")》   | [苻堅](../Page/苻堅.md "wikilink") | [方逸倫](../Page/方逸倫.md "wikilink")、[陳穩](../Page/陳穩.md "wikilink")、[高雄 (演員)](../Page/高雄_\(演員\).md "wikilink")、[梁家仁](../Page/梁家仁.md "wikilink") |

### 广告CF作品

  - 古道綠茶系列-茉莉香
  - 三角矸國安感冒液

## 音樂作品

### 專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>唱片公司</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><strong>1st</strong></p></td>
<td style="text-align: left;"><p>《<strong>木頭人</strong>》</p></td>
<td style="text-align: left;"><p>2011年7月1日</p></td>
<td style="text-align: left;"><p><a href="../Page/華特音樂.md" title="wikilink">華特音樂</a></p></td>
<td style="text-align: left;"><p>國語、台語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2nd</strong></p></td>
<td style="text-align: left;"><p>《<strong>瀟灑男兒</strong>》</p></td>
<td style="text-align: left;"><p>2017年2月27日</p></td>
<td style="text-align: left;"><p><a href="../Page/威林音樂.md" title="wikilink">威林音樂</a></p></td>
<td style="text-align: left;"><p>國語、台語</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 其他合作作品及音樂錄影帶

| 年份    | 曲目     | 合作歌手 | 收錄專輯                                         | 備註       |
| ----- | ------ | ---- | -------------------------------------------- | -------- |
| 1999年 | 雨季的故事  | 趙薇   | [趙薇](../Page/趙薇.md "wikilink")"小燕子Swallow"專輯 | MV演出     |
| 2009年 | 前世緣    | 秀蘭瑪雅 | [秀蘭瑪雅](../Page/秀蘭瑪雅.md "wikilink")"故鄉月"專輯    | 合唱\&MV演出 |
| 2010年 | 黃昏之戀   | 秀蘭瑪雅 | [秀蘭瑪雅](../Page/秀蘭瑪雅.md "wikilink")"寄付"專輯     | 合唱\&MV演出 |
| 2011年 | 不能沒有你  | 秀蘭瑪雅 | [秀蘭瑪雅](../Page/秀蘭瑪雅.md "wikilink")"不能沒有你"專輯  | 合唱\&MV演出 |
| 2015年 | 我愛你你愛我 | 林葳   | [林葳](../Page/林葳.md "wikilink")"欠阮一世人"專輯      | 合唱\&MV演出 |
| 2015年 | 明明這甲意  | 林葳   | [林葳](../Page/林葳.md "wikilink")"欠阮一世人"專輯      | 合唱\&MV演出 |
|       |        |      |                                              |          |

## 得獎紀錄

| 年份    | 作品                                     | 獎項                      |
| ----- | -------------------------------------- | ----------------------- |
| 2014年 | 《[代號九耳犬](../Page/代號九耳犬.md "wikilink")》 | 上海東方電影頻道—年度最具人氣男演員獎     |
| 2014年 | 《[代號九耳犬](../Page/代號九耳犬.md "wikilink")》 | 廣西綜藝頻道—年度廣西地區最具人氣男演員獎   |
| 2015年 | 《[獵刃](../Page/獵刃.md "wikilink")》       | 廣西綜藝頻道—年度廣西地區最受觀眾喜愛男演員獎 |
|       |                                        |                         |

## 外部链接

  -
  -
  -
  -
  -
  -
## 參考資料

[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Y](../Category/台北市人.md "wikilink")
[Y](../Category/陳姓.md "wikilink")