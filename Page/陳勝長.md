**陳勝長**（），生於[英屬香港](../Page/英屬香港.md "wikilink")，籍貫中國[浙江](../Page/浙江.md "wikilink")[上虞](../Page/上虞.md "wikilink")，[香港教育工作者](../Page/香港.md "wikilink")，[香港中文大學中文系退休教授](../Page/香港中文大學.md "wikilink")，文史學者。

## 生平

陳勝長2006年自香港中文大學中國語言及文學系退休，退休前為中大中文系教授。陳勝長中學時就讀於聖保羅男女中學，後升讀香港中文大學[新亞書院中文系](../Page/新亞書院.md "wikilink")，獲學士學位後，再獲授文學碩士學位，師隨[潘重規先生](../Page/潘重規.md "wikilink")，碩士論文為〈說文段注牴牾考〉。

據1967年出版之《新亞生活》所述，陳勝長獲中文系保送至美國[哈佛大學讀研究院](../Page/哈佛大學.md "wikilink")，獲授文學碩士學位。回港後任香港中文大學中文系講師、高級講師、教授，直至退休。任教文字學導論、孟子、陶潛詩、現代作家研究等科目。

陳勝長教授治學，擅長以諧隱角度探究古代文獻。〈《說文》所說字義或非本義考辨並論所謂「微辭」問題〉一文便指出，《說文》某些字義非本義，並非許慎的無心之失，而是諷刺領導階層的微言。

## 近期著作

  - 1990 托洛茨基的文藝理論對魯迅的影響
  - 1991 讀戴震《屈原賦注》──兼論湖田草堂藏「初稿」殘本與 《經附錄》之真偽問題
  - 1995 考證與反思──從《周官》到魯迅
  - 1998 從周氏兄弟作品研究看文學的認知與評價問題
  - 2000 「五十自壽」詩與《兩地書》──周氏兄弟詩文互證發覆
  - 2001 絹本《孝女曹娥碑》墨跡考辨(節錄)
  - 2002 《說文》所說字義或非本義考辨並論所謂「微辭」問題
  - 2003 孟子在辯論中運用譬喻的方法探論（譯作）

資料來源：https://web.archive.org/web/20100205195424/http://dspace.lib.cuhk.edu.hk/

## 資料來源 / 參考文獻

陳勝長（1986）《考證與反思》，台北：東大圖書公司。

Items for Author "CHAN Shing Cheong" FROM
"<https://web.archive.org/web/20100205195424/http://dspace.lib.cuhk.edu.hk/>"

## 外部連結

[category:近現代儒學學者](../Page/category:近現代儒學學者.md "wikilink")

[Category:1946年出生](../Category/1946年出生.md "wikilink")
[S](../Category/陳姓.md "wikilink") [C](../Category/上虞人.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Category:香港中文大學教授](../Category/香港中文大學教授.md "wikilink")
[Category:文史工作者](../Category/文史工作者.md "wikilink")