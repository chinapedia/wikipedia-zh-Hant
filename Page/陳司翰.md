**陳司翰**（****，），[香港藝人](../Page/香港.md "wikilink")[陳慧琳的胞弟](../Page/陳慧琳.md "wikilink")，也是一位男[歌手](../Page/歌手.md "wikilink")，星路不順未能出名。最初簽約[EMI](../Page/EMI.md "wikilink")[百代唱片](../Page/百代唱片.md "wikilink")，合約于2002年终止。2001年，陳司翰推出了首張同名專輯，專輯主打歌為《[愛情最強](../Page/愛情最強.md "wikilink")》，其後亦於香港推出了《[Listen·陳司翰](../Page/Listen·陳司翰.md "wikilink")》，市場反應冷淡，2002年後絕跡香港，轉戰[台灣發展](../Page/台灣.md "wikilink")，推出了《[愛情主義](../Page/愛情主義.md "wikilink")》及《[玉環](../Page/玉環.md "wikilink")》反響也不大\[1\]，最後轉往中國大陆發展，以戲劇為主\[2\]。

## 電視劇

  - 2003年：[又見橘花香](../Page/又見橘花香.md "wikilink")
  - 2005年：[我的野蠻千金](../Page/我的野蠻千金.md "wikilink")
  - 2005年：[粉領一族](../Page/粉領一族.md "wikilink")
  - 2010年：[聊齋之](../Page/聊斋_\(2010年电视剧\).md "wikilink")「江城」 飾 王子雅
  - 2010年：[西遊記](../Page/西游记_\(2010年电视剧\).md "wikilink") 飾
    [唐僧](../Page/唐僧.md "wikilink")
  - 2011年：[活佛濟公2之](../Page/活佛濟公2.md "wikilink")「雪女傳說」飾 高楓
  - 2011年：[十二生肖傳奇](../Page/十二生肖傳奇.md "wikilink") 饰 元野/馬
  - 2012年：[微博达人](../Page/微博达人_\(电视剧\).md "wikilink") 飾 欧阳过
  - 2015年：[孤独的美食家飾](../Page/孤独的美食家_\(中国电视剧\).md "wikilink") 方方
  - 2015年：[金釵諜影](../Page/金釵諜影.md "wikilink") 飾 铁威堂
  - 2016年：[每个人都有秘密](../Page/每个人都有秘密.md "wikilink") 飾 叶航轩

## 電影

  - 2001年：[初戀嗱喳麵](../Page/初戀嗱喳麵.md "wikilink") 飾 警察
  - 2011年：[遍地狼煙](../Page/遍地狼煙.md "wikilink")
  - 2013年：[冬戀望日](../Page/冬戀望日.md "wikilink")

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/陳司翰_(專輯).md" title="wikilink">陳司翰</a></p></td>
<td style="text-align: left;"><p>粵語大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/百代唱片_(香港).md" title="wikilink">百代唱片</a></p></td>
<td style="text-align: left;"><p>2001年8月8日</p></td>
<td style="text-align: left;"><ol>
<li>18/22</li>
<li>愛情最強</li>
<li>一拍即合（雀巢檸檬茶廣告歌）</li>
<li>背心</li>
<li>愛與和平</li>
<li>名廠設計</li>
<li>隱形人</li>
<li>擺花街一號</li>
<li>天堂</li>
<li>男孩像我</li>
<li>喝采(國語)</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/Listen_(陳司翰EP).md" title="wikilink">Listen</a></p></td>
<td style="text-align: left;"><p>粵語EP</p></td>
<td style="text-align: left;"><p><a href="../Page/百代唱片_(香港).md" title="wikilink">百代唱片</a></p></td>
<td style="text-align: left;"><p>2002年3月28日</p></td>
<td style="text-align: left;"><ol>
<li>脫胎換骨</li>
<li>旋轉門</li>
<li>Windows</li>
<li>破相</li>
<li>吵架</li>
<li>硬漢</li>
<li>破相（Eric's Version）</li>
<li>愛在他們的天空</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p><a href="../Page/愛情主義.md" title="wikilink">愛情主義</a></p></td>
<td style="text-align: left;"><p>國語大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/百代唱片_(台灣).md" title="wikilink">科藝百代</a></p></td>
<td style="text-align: left;"><p>2002年8月16日</p></td>
<td style="text-align: left;"><ol>
<li>愛情主義</li>
<li>兩顆星的距離</li>
<li>駭客</li>
<li>雙城（<a href="../Page/劉虹嬅.md" title="wikilink">劉虹嬅合唱</a>）</li>
<li>教堂</li>
<li>震央</li>
<li>一個人在台北</li>
<li>最後約會</li>
<li>世界第八奇觀</li>
<li>夢的藍圖</li>
<li>飛行坐標</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p><a href="../Page/玉環_(陳司翰專輯).md" title="wikilink">玉環</a></p></td>
<td style="text-align: left;"><p>國語大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/福茂唱片.md" title="wikilink">福茂唱片</a></p></td>
<td style="text-align: left;"><p>2004年7月16日</p></td>
<td style="text-align: left;"><ol>
<li>玉環</li>
<li>嫉妒只是一粒沙</li>
<li>看見愛</li>
<li>停戰</li>
<li>揮霍浪漫</li>
<li>Rosa</li>
<li>是或一點也不</li>
<li>綜藝節目</li>
<li>夜上海</li>
<li>最後</li>
</ol></td>
</tr>
</tbody>
</table>

### 填詞作品

  - 2001年：《背心》、《名廠設計》、《天堂》（收錄於《陳司翰》）
  - 2002年：[陳慧琳](../Page/陳慧琳.md "wikilink")《情熱之間》（收錄於《ASK》），[許茹芸](../Page/許茹芸.md "wikilink")《和平日》，《破相》、《硬漢》（收錄於Listen.陳司翰），《駭客》、《震央》、《最後的約會》（收錄於《愛情主義》）
  - 2004年：《玉環》、《綜藝節目》（收錄於《玉環》）

## 派台歌曲成績

| **派台歌曲成績**                                       |
| ------------------------------------------------ |
| 唱片                                               |
| **2001年**                                        |
| [陳司翰](../Page/陳司翰_\(專輯\).md "wikilink")          |
| 陳司翰                                              |
| 陳司翰                                              |
| **2002年**                                        |
| [Listen](../Page/Listen_\(陳司翰EP\).md "wikilink") |
| Listen                                           |
| Listen                                           |
| Listen（特別版）                                      |
| [愛情主義](../Page/愛情主義.md "wikilink")               |
| **2003年**                                        |
| [又見橘花香電視原聲帶](../Page/又見橘花香.md "wikilink")        |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 廣告

  - 2001-2002年：[雀巢檸檬茶](../Page/雀巢.md "wikilink")
  - 2002-2003年：[bossini](../Page/bossini.md "wikilink")

## 獎項

**2001年**

  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2001 ---- 新城勁爆新人王
  - 2001年度[十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink") ---- 最受歡迎新人獎
    男歌手 銅獎
  - 第二十四屆[十大中文金曲頒獎典禮](../Page/十大中文金曲頒獎典禮.md "wikilink") ---- 最有前途新人獎男歌手
    銀獎
  - [IFPI香港唱片商會頒獎典禮](../Page/IFPI.md "wikilink") ---- 全年最暢銷新人

**2002年**

  - [無綫電視](../Page/無綫電視.md "wikilink")2002年度勁歌金曲第二季季選 ---- 得獎歌曲《旋轉門》
  - [馬來西亞](../Page/馬來西亞.md "wikilink") 第二屆 金曲紅人頒獎獎 ---- 最受歡迎紅男紅人新人 金獎
  - [TVB8金曲榜頒獎典禮](../Page/TVB8金曲榜頒獎典禮.md "wikilink")---- 金曲金榜 最佳男新人 銀獎

## 參考來源

## 外部鏈接

  -
  -
  -
  -
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港电影演员](../Category/香港电影演员.md "wikilink")
[C](../Category/聖保羅男女中學校友.md "wikilink")
[S司](../Category/陳姓.md "wikilink")

1.
2.