**陳嘉容**（原名**陳寶芬**，，，），祖籍[廣東](../Page/廣東.md "wikilink")[中山](../Page/中山市.md "wikilink")，[香港出生](../Page/香港.md "wikilink")，是一名[香港著名](../Page/香港.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")。她於[聖瑪加利女書院畢業後本來打算投考警察](../Page/聖瑪加利女書院.md "wikilink")。

她在1994年入行，由其姊姊的朋友介紹而入行，曾替多個國際時裝品牌時裝表演擔任模特兒。模特兒代理為[Icon
Models](../Page/Icon_Models.md "wikilink")（原稱Look
Models），其前[男友是香港著名](../Page/男朋友.md "wikilink")[髮型師](../Page/髮型師.md "wikilink")[郭子輝](../Page/郭子輝.md "wikilink")。

2004年，陳嘉容發現[卵巢患有良性](../Page/卵巢.md "wikilink")[腫瘤](../Page/腫瘤.md "wikilink")，其後施手術割除。2006年10月為[無綫電視主持旅遊飲食節目](../Page/無綫電視.md "wikilink")《[食盡東西](../Page/食盡東西.md "wikilink")》。

2007年，陳嘉容正式與[郭子輝分居](../Page/郭子輝.md "wikilink")。

2014年，[歐陽妙芝在向](../Page/歐陽妙芝.md "wikilink")[NowTV的訪問時透露](../Page/NowTV.md "wikilink")，陳嘉容疑與[黃宗澤相戀](../Page/黃宗澤.md "wikilink")\[1\]。

## 廣告

  - [和記電訊](../Page/和記電訊.md "wikilink")
  - Glycel
  - 香港[渣打銀行](../Page/渣打銀行.md "wikilink")
  - [周生生珠寶](../Page/周生生.md "wikilink")
  - [Fancl](../Page/Fancl.md "wikilink") Mco卸粧液
  - [Dermes](http://www.dermes.com.hk)
  - Nail Nail
  - Clarins

## 電影

  - 2006年 《超班寶寶》

## 電視劇

  - 2007年 《森之愛情》第七集《十五年的約會》飾 陳家嘉
  - 2008年 《[野蠻奶奶大戰戈師奶](../Page/野蠻奶奶大戰戈師奶.md "wikilink")》飾 Eunis

## 電視主持

  - 2006年 [食盡東西](../Page/食盡東西.md "wikilink")
    ([TVB](../Page/TVB.md "wikilink"))
  - 2016年  ([ViuTV](../Page/ViuTV.md "wikilink"))
  - 2017年 [匯智營商](../Page/匯智營商.md "wikilink")
    ([ViuTV](../Page/ViuTV.md "wikilink"))

## 出版書籍

  - 2003年 《Eunis美的經驗》
  - 2005年 《Eye Of Eunis》
  - 2007年 《陳嘉容星級靚湯》

## 曾參與之品牌時裝展

  - [YSL](../Page/:en:Yves_Saint-Laurent.md "wikilink")
  - [Chanel](../Page/香奈兒.md "wikilink")
  - [Anteprima](../Page/:en:Anteprima.md "wikilink")
  - [Moschino](../Page/:en:Moschino.md "wikilink")
  - [Feragamo](../Page/:en:Feragamo.md "wikilink")
  - [Giorgio Armani](../Page/乔治·阿玛尼.md "wikilink")
  - [Prada](../Page/:en:Prada.md "wikilink")
  - [Gucci](../Page/Gucci.md "wikilink")
  - [Cerruti](../Page/:en:Cerruti.md "wikilink")
  - [Gianni Versace](../Page/范思哲.md "wikilink")
  - [Christian Dior](../Page/克里斯汀·迪奧.md "wikilink")
  - [Fendi](../Page/:en:fendi.md "wikilink")
  - [Max Mara](../Page/:en:Max_Mara.md "wikilink")
  - [DKNY](../Page/:en:DKNY.md "wikilink")
  - [MARC JACOBS](../Page/MARC_JACOBS.md "wikilink")

## 外部連結

[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:香港女性作家](../Category/香港女性作家.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港中山人](../Category/香港中山人.md "wikilink")
[Category:陳姓](../Category/陳姓.md "wikilink")

1.  文國駿、馬建華,[妙芝數臭楊崢　搞散人頭家"](https://hk.entertainment.appledaily.com/entertainment/daily/article/20140220/18631268%22同情黃佩霞遭遇),*蘋果日報*,2014年2月20日