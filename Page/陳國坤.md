**陈国坤**（**Danny Chan Kwok
Kwan**，1975年8月1日－），人称**小龙**，是[香港男演员](../Page/香港.md "wikilink")、导演及摄影师。原为特约演员，獲[周星驰賞識](../Page/周星驰.md "wikilink")，于2000年担任电影《少林足球》的排舞师，及后因外貌神似[李小龙而成为](../Page/李小龙.md "wikilink")《少林足球》里一主要演员。陳國坤曾憑《[功夫](../Page/功夫_\(电影\).md "wikilink")》中的“斧頭幫大佬琛哥”一角，獲[第二十四屆香港電影金像獎](../Page/2005年香港電影金像獎.md "wikilink")“最佳男配角”提名。

2014年10月25日，陈国坤与香港女歌手[黄伊汶在](../Page/黄伊汶.md "wikilink")[峇里島完婚](../Page/峇里島.md "wikilink")。\[1\]

2017年10月，陳國坤接受訪問時表示最初在做電影配樂的音樂人[劉以達](../Page/劉以達.md "wikilink")、[韋啟良的錄音室中做助手](../Page/韋啟良.md "wikilink")，又認識了一些副導演，在不少電影做過臨時演員。而[韋啟良負責](../Page/韋啟良.md "wikilink")《少林足球》預告片的音樂，陳國坤將碟送到[周星馳的公司](../Page/周星馳.md "wikilink")。剛巧[周星馳看到](../Page/周星馳.md "wikilink")，之後[田啟文走出來](../Page/田啟文.md "wikilink")，拿張招募演員表格給他填，最後[周星馳問陳國坤想不想做演員](../Page/周星馳.md "wikilink")，他說想，就做了戲中的四師兄\[2\]。

## 背景

陳國坤於[中國](../Page/中華人民共和國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[寶安縣](../Page/寶安縣.md "wikilink")（今[深圳市](../Page/深圳市.md "wikilink")）出生，五歲時來港生活，住在[黃大仙區](../Page/黃大仙區.md "wikilink")\[3\]。他家有兩位胞兄，父母皆為漁民\[4\]。其外祖父則為[陳錫桃](../Page/陳錫桃.md "wikilink")（曾任[深圳市人大副主任](../Page/深圳市.md "wikilink")，已逝）。陳國坤在1993年畢業於[五旬節聖潔會永光書院](../Page/五旬節聖潔會永光書院.md "wikilink")。中學畢業後在眼鏡88當銷售員，後來晉升為驗光師監督。他之後當過侍應、廚房學徒、酒保、調酒師、股票經紀、地盤工人、裝修工人、三行工人以及旺角波鞋街鞋類售貨員。陳國坤在[劉以達與](../Page/劉以達.md "wikilink")[韋啟良的錄音室中做助手時將載有](../Page/韋啟良.md "wikilink")《少林足球》預告片音樂的光碟送到[周星馳的公司](../Page/周星馳.md "wikilink")。結果[周星馳著](../Page/周星馳.md "wikilink")[田啟文給陳國坤一份演員招募的表格](../Page/田啟文.md "wikilink")。陳國坤填完便離去。其後，他在[林子善的聯絡下到訓練班旁聽](../Page/林子善.md "wikilink")。周星馳詢問其當演員的意願後安排他在《[少林足球](../Page/少林足球.md "wikilink")》中演出，並簽約成為周氏公司的旗下藝人，為期八年。直至2004年，陳國坤才成立自己的製作公司。他現時主力拍攝電影及網絡劇。監製[宋本中為其事業上的合作伙伴](../Page/宋本中.md "wikilink")。\[5\]

陳國坤於2014年迎娶歌手[黃伊汶](../Page/黃伊汶.md "wikilink")\[6\]，現育有一子。兒子陳真於2016年3月出生。\[7\]

## 作品

|                                                          |                                              |                                          |                                                                   |
| -------------------------------------------------------- | -------------------------------------------- | ---------------------------------------- | ----------------------------------------------------------------- |
| 1996年                                                    | [古惑仔3之隻手遮天](../Page/古惑仔3之隻手遮天.md "wikilink") |                                          | [劉偉強](../Page/劉偉強.md "wikilink")                                  |
| 2000年                                                    | [江湖告急](../Page/江湖告急.md "wikilink")           |                                          | [林超賢](../Page/林超賢.md "wikilink")                                  |
| 2001年                                                    | [少林足球](../Page/少林足球.md "wikilink")           | 四師兄、鬼影擒拿手                                | [周星馳](../Page/周星馳.md "wikilink")                                  |
| [尖峰時刻2](../Page/尖峰時刻2.md "wikilink")                     | 桑拿房打手成員                                      | [布萊特·拉特納](../Page/布萊特·拉特納.md "wikilink") |                                                                   |
| 2002年                                                    | [一蚊雞保鏢](../Page/一蚊雞保鏢.md "wikilink")         | 收數佬                                      | [黃子華](../Page/黃子華.md "wikilink")、[鄺文偉](../Page/鄺文偉.md "wikilink") |
| 2004年                                                    | [功夫](../Page/功夫_\(电影\).md "wikilink")        | 斧頭幫大佬琛哥                                  | [周星馳](../Page/周星馳.md "wikilink")                                  |
| 2005年                                                    | [功夫無敵](../Page/功夫無敵.md "wikilink")           | 董青洋                                      |                                                                   |
| 2006年                                                    | [得閒飲茶](../Page/得閒飲茶.md "wikilink")           | 康仔                                       | [林子聰](../Page/林子聰.md "wikilink")                                  |
| [愛得太遲](../Page/愛得太遲.md "wikilink")                       | Otto                                         |                                          |                                                                   |
| 2007年                                                    | [功夫无敌](../Page/功夫无敌.md "wikilink")           |                                          |                                                                   |
| [心想事成](../Page/心想事成.md "wikilink")                       | 孫悟空                                          | [鄭中基](../Page/鄭中基.md "wikilink")         |                                                                   |
| [這是我的名字，你有什麼意見嗎?](../Page/這是我的名字，你有什麼意見嗎?.md "wikilink") | 帥哥坤、醜哥坤                                      |                                          |                                                                   |
| 2008年                                                    | [長江7號](../Page/長江7號.md "wikilink")           |                                          | [周星馳](../Page/周星馳.md "wikilink")                                  |
| [我老婆係賭聖](../Page/我老婆係賭聖.md "wikilink")                   | 仇曼聯                                          | [王晶](../Page/王晶.md "wikilink")           |                                                                   |
| [大四喜](../Page/大四喜.md "wikilink")                         | 何堅                                           |                                          |                                                                   |
| 2009年                                                    | [家有囍事2009](../Page/家有囍事2009.md "wikilink")   |                                          | [谷德昭](../Page/谷德昭.md "wikilink")                                  |
| 2010年                                                    | [越光寶盒](../Page/越光寶盒.md "wikilink")           | 士兵                                       | [劉鎮偉](../Page/劉鎮偉.md "wikilink")                                  |
| [初恋红豆冰](../Page/初恋红豆冰.md "wikilink")                     | 友情客串                                         | [阿牛](../Page/阿牛.md "wikilink")           |                                                                   |
| 2011年                                                    | [截拳之魂](../Page/截拳之魂.md "wikilink")\[8\]      | Jin                                      |                                                                   |
| 2015年                                                    | [道士下山](../Page/道士下山.md "wikilink")           | 趙心川                                      | [陳凱歌](../Page/陳凱歌.md "wikilink")                                  |
| [探靈檔案](../Page/探靈檔案.md "wikilink")                       |                                              |                                          |                                                                   |
| [葉問3](../Page/葉問3.md "wikilink")                         | [李小龍](../Page/李小龍.md "wikilink")             | [葉偉信](../Page/葉偉信.md "wikilink")         |                                                                   |
| 2017年                                                    | [浪漫搭檔](../Page/浪漫搭檔.md "wikilink")           | 莫小河                                      | 劉夏同                                                               |
| [你往哪里跑](../Page/你往哪里跑.md "wikilink")                     |                                              |                                          |                                                                   |
| 歡樂喜劇人                                                    |                                              |                                          |                                                                   |
| 2018年                                                    | [臥底巨星](../Page/臥底巨星.md "wikilink")           | 泰哥                                       | [谷德昭](../Page/谷德昭.md "wikilink")                                  |
| [功夫联盟](../Page/功夫联盟.md "wikilink")                       | 陈阿真                                          | [刘镇伟](../Page/刘镇伟.md "wikilink")         |                                                                   |
| [濟公之英雄歸位](../Page/濟公之英雄歸位.md "wikilink")                 |                                              |                                          |                                                                   |
| [冒牌搭檔](../Page/冒牌搭檔.md "wikilink")                       | 小龍                                           |                                          |                                                                   |
| 2019年                                                    | [最佳男友進化論](../Page/最佳男友進化論.md "wikilink")     |                                          |                                                                   |
|                                                          |                                              |                                          |                                                                   |

### 電視劇

  - 2005年《[哈拉教父](../Page/哈拉教父.md "wikilink")》客串
  - 2006年《[功夫狀元](../Page/功夫狀元.md "wikilink")》
  - 2007年《[食神](../Page/食神_\(2006年電視劇\).md "wikilink")》飾 鵝頭
  - 2008年《[李小龍傳奇](../Page/李小龍傳奇.md "wikilink")》飾
    [李小龍](../Page/李小龍.md "wikilink")
  - 2011年《[大唐双龙传之长生诀](../Page/大唐双龙传之长生诀.md "wikilink")》飾
    [寇仲](../Page/寇仲.md "wikilink")
  - 2012年《[大营救](../Page/大营救.md "wikilink")》飾 秦国忠
  - 2012年《[馬永貞](../Page/馬永貞_\(2012年電視劇\).md "wikilink")》飾
    [馬永貞](../Page/馬永貞.md "wikilink")
  - 2014年《[坐88路车回家](../Page/坐88路车回家.md "wikilink")》飾 赵乔治
  - 2015年《[医馆笑传](../Page/医馆笑传.md "wikilink")》
  - 2015年《[新猛龙过江](../Page/新猛龙过江.md "wikilink")》飾 聂云

### 網絡劇

  - 2015年《[天才在左，疯子在右](../Page/天才在左，疯子在右_\(网剧\).md "wikilink")》
  - 2017年《[反黑](../Page/反黑.md "wikilink")》飾
    [張少鈞](../Page/張少鈞.md "wikilink")

### 綜藝節目

| 首播年份  | 播出频道                               | 節 目 名 稱                                             |
| ----- | ---------------------------------- | --------------------------------------------------- |
| 2016年 | [浙江衛視](../Page/浙江衛視.md "wikilink") | 《[來吧冠軍](../Page/來吧冠軍.md "wikilink")》第一季 拳擊（05/22播出） |

## 廣告

  - 2001 金象米
  - 2002 生力啤-上網篇
  - 2002 生力啤-新鮮到飛起篇
  - 2002 麥當勞球迷套餐(中國大陸)
  - 2003 諾基亞6100（台灣）
  - 2013 JOHNNIE WALKER 傳奇巨星李小龍篇 ，飾演電腦動畫(CGI)合成的李小龍

## 參考資料

<div style="font-size: 85%">

<references />

</div>

## 外部連結

  -
  -
  - [陳國坤之部落格](http://www.alivenotdead.com/dannychan)

  - [截拳之魂預告片](http://www.youtube.com/watch?v=kKnsl3LjBuc)

[Category:1975年出生](../Category/1975年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[G](../Category/香港男演員.md "wikilink")
[G](../Category/動作片演員.md "wikilink")
[G](../Category/香港武打演員.md "wikilink")
[G](../Category/香港電影男演員.md "wikilink")
[G](../Category/香港導演.md "wikilink")
[G](../Category/攝影師.md "wikilink")
[Category:宝安人](../Category/宝安人.md "wikilink")
[G](../Category/陳姓.md "wikilink")
[Category:五旬節聖潔會永光書院校友](../Category/五旬節聖潔會永光書院校友.md "wikilink")

1.  [陈国坤、黄伊汶巴厘岛大婚
    陈小春现身送祝福](http://ent.southcn.com/8/2014-10/27/content_110901047.htm)

2.
3.

4.

5.
6.

7.

8.