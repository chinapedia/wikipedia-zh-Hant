**陳大慶**（），[中华民国陸軍](../Page/中华民国陸軍.md "wikilink")[一級上將](../Page/一級上將.md "wikilink")，曾任国防部长。1904年生于[江西崇義](../Page/江西.md "wikilink")。[黃埔軍官學校第一期畢業](../Page/黃埔軍官學校.md "wikilink")。
自[國民黨東征](../Page/國民黨東征.md "wikilink")、[國民黨北伐](../Page/國民黨北伐.md "wikilink")、[抗日戰爭](../Page/抗日戰爭.md "wikilink")、[國共內戰等](../Page/國共內戰.md "wikilink")，由排、連、營長，積功升團、旅長。

## 生平

### 抗戰

[中国抗日战争期间](../Page/中国抗日战争.md "wikilink")，任第四師師長，調五十八軍副軍長，升新編第二軍軍長，旋改編為第二十九軍軍長，調十九集團軍副總司令，至1944年，升十九集團中將總司令。抗战勝利後，任第一綏靖區副司令官，
翌年，調[南京衛戌副司令官](../Page/南京.md "wikilink")，1948年調[衢州綏署副主任](../Page/衢州.md "wikilink")，后調京滬杭警備副總司令、兼淞滬警備司令。陈大庆先后参加了東征棉湖，惠州之役。北伐南昌、冀東戰役。五次圍剿黃岡、廣昌之役
。抗日南口、[台兒莊戰役](../Page/台兒莊戰役.md "wikilink")。武漢、鄂北、豫南、及[中原會戰](../Page/中原會戰.md "wikilink")、[上海保衛戰等](../Page/上海保衛戰.md "wikilink")。

### 遷台

中華民國政府退守[台湾后](../Page/台湾.md "wikilink")，陈調至中樞服務。1954年出任[國家安全局副局長](../Page/國家安全局.md "wikilink")，繼掌國家安全局
，1960年晉任陸軍二級上將。1962年11月，調任[台灣省](../Page/台灣省.md "wikilink")[警備總部總司令](../Page/警備總部.md "wikilink")、兼軍管區司令。於1967年任[中華民國陸軍總司令](../Page/中華民國陸軍.md "wikilink")，嗣膺任[台灣省政府主席](../Page/台灣省政府主席.md "wikilink")、[中華民國國防部長](../Page/中華民國國防部.md "wikilink")。

### 墓所

1973年8月22日逝世，追贈為陸軍一級上將。墓地位於現[新北市](../Page/新北市.md "wikilink")[八里區](../Page/八里區.md "wikilink")。

## 外部链接

  - [陳大慶部長](http://nrch.cca.gov.tw/ccahome/website/site20/contents/011/cca220003-li-wpkbhisdict002781-0828-u.xml)

|- |colspan="3"
style="text-align:center;"|**[中華民國行政院](../Page/中華民國行政院.md "wikilink")**
|-    |- |colspan="3"
style="text-align:center;"|**[臺灣省政府](../Page/臺灣省政府.md "wikilink")**
|-    |- |colspan="3"
style="text-align:center;"|**[中華民國國防會議](../Page/中華民國國家安全會議.md "wikilink")**
|-    |-  |- |colspan="3"
style="text-align:center;"|**[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")**
|-    |- |colspan="3"
style="text-align:center;"|**[台灣警備總司令部](../Page/台灣警備總司令部.md "wikilink")**
|-    |-

[Category:中華民國陸軍一級上將](../Category/中華民國陸軍一級上將.md "wikilink")
[Category:中華民國國防部部長](../Category/中華民國國防部部長.md "wikilink")
[Category:台灣省主席](../Category/台灣省主席.md "wikilink")
[Category:中華民國陸軍總司令](../Category/中華民國陸軍總司令.md "wikilink")
[Category:中華民國國家安全局局長](../Category/中華民國國家安全局局長.md "wikilink")
[Category:國共戰爭人物](../Category/國共戰爭人物.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:黄埔军校第一期](../Category/黄埔军校第一期.md "wikilink")
[Category:台灣戰後江西移民](../Category/台灣戰後江西移民.md "wikilink")
[Category:崇义人](../Category/崇义人.md "wikilink")
[C](../Category/客家裔臺灣人.md "wikilink")
[D大](../Category/陳姓.md "wikilink")
[Category:台灣外省人支持統一者](../Category/台灣外省人支持統一者.md "wikilink")