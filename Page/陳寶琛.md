**陈宝琛**（），原字**長庵**，改字**伯潛**，號**弢庵**、**陶庵**。[福建](../Page/福建.md "wikilink")[闽县](../Page/闽县.md "wikilink")[螺洲人](../Page/螺洲镇.md "wikilink")。

## 生平

曾祖父[陳若霖官至](../Page/陳若霖.md "wikilink")[刑部尚書](../Page/刑部尚書.md "wikilink")。[同治七年](../Page/同治.md "wikilink")（1868年）進士，選[翰林院](../Page/翰林院.md "wikilink")[庶吉士](../Page/庶吉士.md "wikilink")，授[編修](../Page/編修.md "wikilink")。陳寶琛兄弟6人，3人[進士](../Page/進士.md "wikilink")，3人[舉人](../Page/舉人.md "wikilink")，人稱“兄弟六科甲”。[光緒元年](../Page/光緒.md "wikilink")（1875年）擢翰林侍讀，與學士[張佩綸](../Page/張佩綸.md "wikilink")、[通政使](../Page/通政使.md "wikilink")[黃體芳](../Page/黃體芳.md "wikilink")、侍郎[宝廷等好論時政](../Page/宝廷.md "wikilink")，合稱“[清流四諫](../Page/清流黨.md "wikilink")”。光绪五年（1879年）[沙俄侵占](../Page/沙俄.md "wikilink")[伊犁九城](../Page/伊犁.md "wikilink")，力主收复。

光緒六年（1880年），充[武英殿](../Page/武英殿.md "wikilink")[提調官](../Page/提調.md "wikilink")。翌年，授翰林院[侍講學士](../Page/侍講學士.md "wikilink")，纂修《穆宗本紀》。光緒八年（1882年）任[江西](../Page/江西.md "wikilink")[學政](../Page/學政.md "wikilink")，重修
[白鹿洞書院](../Page/白鹿洞書院.md "wikilink")。光緒十年(1884年)，上疏《請募勇參用西法教練》，主張“變化以盡利，任人以責實，籌餉以持久”，[中法戰爭失利](../Page/中法戰爭.md "wikilink")，受牽連，被降五級，回鄉閉門讀書，修葺先祖“賜書樓”，構築“滄趣樓”。光緒二十五年（1899年），任[鳌峰书院](../Page/鳌峰书院.md "wikilink")[山長](../Page/山長.md "wikilink")。光緒三十一年（1905年），任[福建鐵路總辦](../Page/福建.md "wikilink")，修[漳厦铁路](../Page/漳厦铁路.md "wikilink")，并任福建高等學堂（即今[福州第一中學](../Page/福州第一中學.md "wikilink")）[監督](../Page/監督.md "wikilink")。光緒三十三年（1907年）創立全閩師範學堂（今[福建師大](../Page/福建師大.md "wikilink")）。

[宣統元年](../Page/宣統.md "wikilink")（1909年）奉召入京，擔任[禮學館總纂大臣](../Page/禮學館.md "wikilink")。宣統三年（1911年），陳寶琛在[毓庆宫行走](../Page/毓庆宫.md "wikilink")，任[溥儀老師](../Page/溥儀.md "wikilink")，賜[紫禁城騎馬](../Page/紫禁城騎馬.md "wikilink")。继任汉军副都统、[弼德院顾问大臣](../Page/弼德院.md "wikilink")。

民國元年（1912年）2月12日，清帝遜位，后仍追隨溥儀。命修《德宗實錄》。民國十年（1921年），修成《[德宗本紀](../Page/德宗本紀.md "wikilink")》，授[太傅](../Page/太傅.md "wikilink")。民國十二年（1923年），引薦[鄭孝胥入宮](../Page/鄭孝胥.md "wikilink")。

1917年[张勋复辟時](../Page/张勋复辟.md "wikilink")，推舉為内阁议政大臣。民國十四年（1925年），隨溥儀移居[天津](../Page/天津.md "wikilink")。民國二十一年（1932年），[滿洲國成立](../Page/滿洲國.md "wikilink")，寶琛專程赴[長春探望溥儀](../Page/長春.md "wikilink")，拒受伪职。民國二十四年（1935年）乙亥二月初一日（3月5日）卯時病逝於[北平住处](../Page/北平.md "wikilink")，春秋八十有八。喪聞，溥儀震悼，賜奠醊，賜祭一壇，特諡**文忠**，晉贈[太師](../Page/太師.md "wikilink")，賞給
[陀羅經被](../Page/陀羅經被.md "wikilink")，賞銀九千元治喪葬。

有藏書10萬冊，“清末陳氏私家藏書之多，冠于全閩”。

## 著作

著有《陳文忠公奏議》、《滄趣樓文存》、《滄趣樓詩集》、《滄趣樓律賦》、《南游草》等。

## 參考文献

  - [陳三立](../Page/陳三立.md "wikilink")，《贈太師陳文忠公墓志銘》，《清朝碑傳全集》，5冊，4004-4005

## 外部連結

  - [陳寶琛](http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%B3%AF%C4_%B5%60)
    中研院史語所
  - [陳寶琛字聯](http://collections.lib.uwm.edu/cdm/compoundobject/collection/scroll/id/7/rec/1)
    周策縱書畫卷軸與扇面收藏集

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝翰林院侍讀學士](../Category/清朝翰林院侍讀學士.md "wikilink")
[Category:清朝翰林院侍講學士](../Category/清朝翰林院侍講學士.md "wikilink")
[Category:清朝江西學政](../Category/清朝江西學政.md "wikilink")
[Category:清朝藏書家](../Category/清朝藏書家.md "wikilink")
[Category:清朝诗人](../Category/清朝诗人.md "wikilink")
[Category:帝師](../Category/帝師.md "wikilink")
[Category:毓慶宮行走](../Category/毓慶宮行走.md "wikilink")
[Category:仓山人](../Category/仓山人.md "wikilink")
[B](../Category/陳姓.md "wikilink")
[Category:諡文忠](../Category/諡文忠.md "wikilink")
[Category:資政院議員](../Category/資政院議員.md "wikilink")
[Category:弼德院顧問大臣](../Category/弼德院顧問大臣.md "wikilink")
[Category:賜紫禁城騎馬](../Category/賜紫禁城騎馬.md "wikilink")
[Category:葬于福州](../Category/葬于福州.md "wikilink")