**陳幼堅**（英文名：，），[香港出生](../Page/香港.md "wikilink")，香港平面及[室內設計師](../Page/室內設計.md "wikilink")、藝術家，主要從事[廣告及](../Page/廣告.md "wikilink")[品牌設計工作](../Page/品牌.md "wikilink")。

## 生平

1950年生於[香港](../Page/香港.md "wikilink")，曾就讀[佛教黃鳳翎中學](../Page/佛教黃鳳翎中學.md "wikilink")。1980年成立陳幼堅設計公司，是少數獲得國際地位的華人設計師之一。在過去42年設計生涯中，他帶領公司獲得本地及國際設計獎項600多個，包括[美國Communication](../Page/美國.md "wikilink")
Arts優異獎、New York Art Directors Club
Award、[英國D](../Page/英國.md "wikilink")\&AD、[日本Tokyo](../Page/日本.md "wikilink")
Type Directors Club Award、香港設計師協會環球設計大奬等等。
1996年其司被[美國](../Page/美國.md "wikilink")[紐約著名雜誌GRAPHIS選為該年度世界十大設計公司之一](../Page/紐約.md "wikilink")。
2002年他更獲[香港特別行政區政府頒授榮譽勳章](../Page/香港特別行政區政府.md "wikilink")，以表揚其對香港設計界的貢獻，2014年加入
AGI（[國際平面設計聯盟](../Page/國際平面設計聯盟.md "wikilink")）。

自2000年起，陳幼堅不斷在創作領域上作出新的嘗試，從純粹的商業平面及室內設計走向藝術領域。其實早於1991年他已在[日本](../Page/日本.md "wikilink")[東京Creation](../Page/東京.md "wikilink")
Gallery G8舉行首次個展「East Meets
West」。2002年，他再應[日本最負盛名的平面設計畫廊Ginza](../Page/日本.md "wikilink")
Graphic
Gallery之邀，在[東京](../Page/東京.md "wikilink")[銀座舉行個展](../Page/銀座.md "wikilink")。2003年在[香港文化博物館舉行名為](../Page/香港文化博物館.md "wikilink")「生活的藝術」的回顧展。除了兩次入選[上海雙年展](../Page/上海雙年展.md "wikilink")（2002及2006年）及「香港當代藝術雙年獎」（2009年）外，陳氏亦是首位香港設計師，獲邀於[上海美術館舉行其商業及個人純藝術作品展](../Page/上海美術館.md "wikilink")。2010年他於[香港及](../Page/香港.md "wikilink")[上海及](../Page/上海.md "wikilink")[米蘭發表](../Page/米蘭.md "wikilink")[iPhone攝影作品系列](../Page/iPhone.md "wikilink")「iEye愛」後，再獲北京新時代畫廊邀請，在北京798藝術區展出。此次為陳氏首次於北京舉行之個人展覽，亦是「iEye愛」發表以來最大型的展示，展出超過40幅作品，大部份為從未公開發表、以北京為主題之全新作品。2014年更獲邀請，在新加坡藝術區[Gillman
Barracks舉行了陳氏在東南亞的首個個人展覽](../Page/Gillman_Barracks.md "wikilink")，亦是他第一個藝術作品回顧展。展覽將展出陳氏自2000
年起創作、來自 6 個系列超過 30
件個人作品，包括平面、攝影、裝置等媒介，如《京都，我的愛人！》、《一樣米養百樣人》、《英雄所見略同》、《iEye愛》、《為人民服務》及《一夢》。在這十多年的創作歷程中，讓陳氏超越一向以平面設計的框框，通過其視覺語言，加上對消費文化與生活的體會，在融合視覺與空間的表達過程中，不斷挑戰自己。

陳幼堅曾參與多個群展，包括[香港藝術館的](../Page/香港藝術館.md "wikilink")「當代香港藝術2000」、[瑞士巴塞爾的](../Page/瑞士.md "wikilink")「2002巴塞爾國際藝術博覽會」、[英國](../Page/英國.md "wikilink")[倫敦the](../Page/倫敦.md "wikilink").gallery@oxo
and
Bargehouse紅樓軒的「夢02展覽」、2004年中國寧波美術館的「第3屆寧波國際海報雙年展、2008年的「香港國際藝術展」、2009年[上海八佰秀創意園區的](../Page/上海.md "wikilink")「重生八佰秀」以及2010年[上海的](../Page/上海.md "wikilink")「Moleskine
Detour」等。2011年他以大型[裝置藝術作品](../Page/裝置藝術.md "wikilink")《搭橋》參與由又一山人（黃炳培）策展的「甚麼是明天
-
三十乘三十創意展」，並於[深圳華](../Page/深圳.md "wikilink")·美術館及[香港ArtisTree展出](../Page/香港.md "wikilink")；同年參與[香港](../Page/香港.md "wikilink")[路易威登藝文空間](../Page/路易威登.md "wikilink")「See
the
Light」。2012年再接受又一山人（黃炳培）之邀參與於[香港藝術中心包氏畫廊舉行的](../Page/香港藝術中心.md "wikilink")「你眼:望我眼
- 攝影師眼中的攝影師」作品展。

經常接受[日本雜誌](../Page/日本.md "wikilink")、電視臺及各大傳媒訪問外，亦屢獲邀請擔任國際設計及藝術比賽的評判，包括國際陶瓷大賞賽和森澤國際造字大賞的評委。

陳氏亦為古玩、工藝品及藝術品愛好者及收藏家，並開設「27畫廊」，旨在推廣由平面設計啟發的創意與美學，同時促進藝術融入大眾的日常生活。

## 展覽

### 個展

  - 1991 〈East Meets West〉 Creation Gallery G8
    [日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")
  - 2002 〈東情西韻〉 Ginza Graphic Gallery
    [日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")
  - 2003 〈生活的藝術〉
    [香港文化博物館](../Page/香港文化博物館.md "wikilink")[中國](../Page/中國.md "wikilink")[香港](../Page/香港.md "wikilink")
  - 2007 〈因為生活…陳幼堅x Alan Chan Design Co作品展〉
    [上海美術館](../Page/上海美術館.md "wikilink")[中國](../Page/中國.md "wikilink")[上海](../Page/上海.md "wikilink")
  - 2010 〈iEye愛– 陳幼堅iPhone攝影展〉
    27畫廊[中國](../Page/中國.md "wikilink")[香港及](../Page/香港.md "wikilink")[上海](../Page/上海.md "wikilink")
  - 2012 〈iEye愛– 陳幼堅iPhone攝影展〉
    [義大利](../Page/義大利.md "wikilink")[米蘭Anteprima藝廊](../Page/米蘭.md "wikilink")
  - 2012 〈iEye愛– 陳幼堅iPhone攝影展〉
    [北京](../Page/北京.md "wikilink")[798藝術區](../Page/798藝術區.md "wikilink")
    新時代畫廊
  - 2014 〈開始…開始了 - 陳幼堅視覺藝術回顧展〉 新加坡藝術區Gillman Barracks, Mizuma Gallery

### 群展 (部份)

  - 2000 〈當代香港藝術2000〉
    [香港藝術館](../Page/香港藝術館.md "wikilink")[中國](../Page/中國.md "wikilink")[香港](../Page/香港.md "wikilink")
  - 2002 〈2002巴塞爾國際藝術博覽會〉 巴塞爾展覽場[瑞士巴塞爾](../Page/瑞士.md "wikilink")
  - 2002 〈夢02展覽〉 the.gallery@oxo and
    Bargehouse紅樓軒[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")
  - 2002 〈第4屆上海雙年展〉
    [上海美術館](../Page/上海美術館.md "wikilink")[中國](../Page/中國.md "wikilink")[上海](../Page/上海.md "wikilink")
  - 2004 〈第3屆[寧波國際海報雙年展](../Page/寧波.md "wikilink")〉
    [寧波美術館](../Page/寧波美術館.md "wikilink")[中國](../Page/中國.md "wikilink")[寧波](../Page/寧波.md "wikilink")
  - 2006 〈第6屆[上海雙年展](../Page/上海雙年展.md "wikilink")〉
    [上海美術館](../Page/上海美術館.md "wikilink")[中國](../Page/中國.md "wikilink")[上海](../Page/上海.md "wikilink")
  - 2008 〈香港國際藝術展〉
    [香港會議展覽中心中國](../Page/香港會議展覽中心.md "wikilink")[香港](../Page/香港.md "wikilink")
  - 2009 〈重生八佰秀〉
    八佰秀創意園區[中國](../Page/中國.md "wikilink")[上海](../Page/上海.md "wikilink")
  - 2010 〈香港當代藝術雙年獎展覽2009〉
    [香港藝術館](../Page/香港藝術館.md "wikilink")[中國](../Page/中國.md "wikilink")[香港](../Page/香港.md "wikilink")
  - 2010 〈Moleskine Detour〉
    [中國](../Page/中國.md "wikilink")[上海](../Page/上海.md "wikilink")
  - 2010 〈Butchers Deluxe〉 Contemporary by Angela Li
    [中國](../Page/中國.md "wikilink")[香港](../Page/香港.md "wikilink")
  - 2011 〈What’s next 30x30創意展〉
    華．美術館[中國](../Page/中國.md "wikilink")[深圳](../Page/深圳.md "wikilink")；Artistree
    [中國](../Page/中國.md "wikilink")[香港](../Page/香港.md "wikilink")
  - 2011 〈See the Light〉
    [路易威登藝文空間](../Page/路易威登.md "wikilink")[中國](../Page/中國.md "wikilink")[香港](../Page/香港.md "wikilink")
  - 2012 〈你眼:望我眼 攝影師眼中的攝影師〉 [香港藝術中心](../Page/香港藝術中心.md "wikilink") 包氏畫展
  - 2012 〈圖與詞：馬格利特以來〉
    [中國美術館](../Page/中國美術館.md "wikilink")[中國](../Page/中國.md "wikilink")[北京](../Page/北京.md "wikilink")

## 獎項

陳幼堅曾獲超過600個設計獎項，包括：

  - [美國](../Page/美國.md "wikilink")[紐約](../Page/紐約.md "wikilink") Art
    Directors Club Award
  - [日本](../Page/日本.md "wikilink") Typodirectors Club Award
  - [美國CA設計雜誌優異獎](../Page/美國.md "wikilink")
  - [香港包裝](../Page/香港.md "wikilink")、書刊、海報獎。(1972、1985及1988年)
  - [香港藝術家聯盟](../Page/香港藝術家聯盟.md "wikilink") 設計家年獎 (1989年)
  - 榮譽勳章，[香港特別行政區政府](../Page/香港特別行政區政府.md "wikilink") (2002年)

## 資料來源

  - [香港設計
    貿發網](https://web.archive.org/web/20061021182232/http://hkdesign.tdctrade.com/chi/designer.asp?Page=2)
  - [文化博物館展覽透視陳幼堅的生活藝術](http://www.info.gov.hk/gia/general/200307/05/0705073.htm)
  - [敘述設計訊息中心](https://web.archive.org/web/20061021224620/http://home.educities.edu.tw/lingyf/na/pep107.html)
  - [行政長官主持勳銜頒授典禮](http://www.info.gov.hk/gia/general/200210/12/1012161.htm)
    2002年10月

## 外部連結

  - [陳幼堅設計公司官網](http://www.alanchandesign.com/)
  - [Facebook](https://www.facebook.com/alanchanpage)
  - [微博](http://www.weibo.com/1782537101/profile?topnav=1&wvr=6)

[Category:陳姓](../Category/陳姓.md "wikilink")
[Category:香港設計師](../Category/香港設計師.md "wikilink")
[Category:佛教黃鳳翎中學校友](../Category/佛教黃鳳翎中學校友.md "wikilink")