**陳彥成**（），[台灣](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[味全龍](../Page/味全龍.md "wikilink")、[兄弟象](../Page/兄弟象.md "wikilink")、[興農牛及](../Page/興農牛.md "wikilink")[時報鷹隊](../Page/時報鷹.md "wikilink")，守備位置為[捕手](../Page/捕手.md "wikilink")、[三壘手](../Page/三壘手.md "wikilink")。綽號「小鋼砲」。

## 經歷

  - [新竹市西門國小少棒隊](../Page/新竹市.md "wikilink")
  - [臺北縣板橋國中青少棒隊](../Page/新北市.md "wikilink")(榮工)
  - [臺北縣中華中學青棒隊](../Page/新北市.md "wikilink")(榮工)
  - [文化大學棒球隊](../Page/文化大學.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[味全龍隊](../Page/味全龍.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[兄弟象隊](../Page/兄弟象.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛隊](../Page/興農牛.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[時報鷹隊](../Page/時報鷹.md "wikilink")
  - 高美醫護管理專科學校青棒隊教練
  - [台中縣清海國中青少棒隊總教練](../Page/台中縣.md "wikilink")
  - [新竹市](../Page/新竹市.md "wikilink")[中華大學棒球隊總教練](../Page/中華大學.md "wikilink")（2013年～）

## 職棒生涯成績

| 年度    | 球隊                               | 出賽  | 打數   | 安打  | 全壘打 | 打點  | 盜壘 | 四死  | 三振  | 壘打數 | 雙殺打 | 打擊率   |
| ----- | -------------------------------- | --- | ---- | --- | --- | --- | -- | --- | --- | --- | --- | ----- |
| 1990年 | [味全龍](../Page/味全龍.md "wikilink") | 67  | 223  | 70  | 5   | 30  | 2  | 13  | 27  | 99  | 9   | 0.314 |
| 1991年 | [味全龍](../Page/味全龍.md "wikilink") | 50  | 124  | 30  | 4   | 22  | 1  | 11  | 16  | 48  | 3   | 0.242 |
| 1992年 | [兄弟象](../Page/兄弟象.md "wikilink") | 83  | 273  | 73  | 10  | 44  | 3  | 27  | 43  | 125 | 10  | 0.267 |
| 1993年 | [兄弟象](../Page/兄弟象.md "wikilink") | 76  | 258  | 67  | 6   | 39  | 1  | 32  | 41  | 100 | 4   | 0.260 |
| 1994年 | [兄弟象](../Page/兄弟象.md "wikilink") | 83  | 250  | 65  | 6   | 30  | 5  | 21  | 50  | 100 | 5   | 0.260 |
| 1995年 | [兄弟象](../Page/兄弟象.md "wikilink") | 79  | 259  | 57  | 3   | 14  | 0  | 9   | 42  | 72  | 11  | 0.220 |
| 1996年 | [興農牛](../Page/興農牛.md "wikilink") | 50  | 119  | 26  | 0   | 10  | 1  | 11  | 24  | 32  | 2   | 0.218 |
| 1997年 | [時報鷹](../Page/時報鷹.md "wikilink") | 33  | 89   | 16  | 0   | 8   | 1  | 9   | 18  | 18  | 7   | 0.180 |
| 合計    | 8年                               | 522 | 1596 | 404 | 34  | 197 | 14 | 133 | 261 | 594 | 52  | 0.253 |

## 特殊事蹟

  - 1993年7月18日，於[台北市立棒球場對](../Page/台北市立棒球場.md "wikilink")[時報鷹隊](../Page/時報鷹隊.md "wikilink")，在1局下從[郭建成手中擊出](../Page/郭建成.md "wikilink")[中華職棒第](../Page/中華職棒.md "wikilink")900號[全壘打](../Page/全壘打.md "wikilink")。
  - 1993年8月14日，於[立德棒球場出戰](../Page/立德棒球場.md "wikilink")[俊國熊隊](../Page/俊國熊隊.md "wikilink")，在1局上從[野中尊制手中擊出](../Page/野中尊制.md "wikilink")[全壘打](../Page/全壘打.md "wikilink")，達成對當時的六隊皆擊出[全壘打的紀錄](../Page/全壘打.md "wikilink")。
  - 1993年8月18日，於[立德棒球場出戰](../Page/立德棒球場.md "wikilink")[俊國熊隊](../Page/俊國熊隊.md "wikilink")，擊出個人第一支滿貫[全壘打](../Page/全壘打.md "wikilink")，投手為[野中尊制](../Page/野中尊制.md "wikilink")。

## 外部連結

[C](../Category/台灣棒球選手.md "wikilink")
[C](../Category/味全龍隊球員.md "wikilink")
[C](../Category/兄弟象隊球員.md "wikilink")
[C](../Category/興農牛隊球員.md "wikilink")
[C](../Category/時報鷹隊球員.md "wikilink")
[C](../Category/中華青棒隊球員.md "wikilink")
[C](../Category/中華成棒隊球員.md "wikilink")
[C](../Category/中國文化大學校友.md "wikilink")
[Y彥](../Category/陳姓.md "wikilink")