**陳意涵**（英文名：****，），[台灣知名](../Page/台灣.md "wikilink")[女演員](../Page/女演員.md "wikilink")。出生於[台湾](../Page/台湾.md "wikilink")[新北市](../Page/新北市.md "wikilink")[土城區](../Page/土城區.md "wikilink")。陳意涵在2002年曾參與[中視綜藝節目](../Page/中視.md "wikilink")《[我猜我猜我猜猜猜](../Page/我猜我猜我猜猜猜.md "wikilink")》的「人不可貌相」單元\[1\]，之後2006年大學畢業後在餐廳打工時，被經紀人小男哥發掘而正式踏入演藝圈。
於2018年8月17日與導演男友許富翔登記結婚。

## 演藝生涯

進入演藝圈後，在2006年11月10日，出演了自己的第一部電影《[單車上路](../Page/單車上路.md "wikilink")》，並擔任女主角，並獲得了第七屆華語電影傳媒大賞最佳新演員提名\[2\]。同年簽約經紀公司\[3\]。其後，參與多部電影、[電視劇](../Page/電視劇.md "wikilink")、陈意涵[廣告及](../Page/電視廣告.md "wikilink")[MV的拍攝](../Page/MV.md "wikilink")。在2006年，參與[趙寶剛執導的電視劇](../Page/趙寶剛.md "wikilink")《[奮鬥](../Page/奮鬥.md "wikilink")》中飾演方靈珊，深得中國觀眾喜愛\[4\]，更因此角色獲得了“小靈仙”的稱號\[5\]，使陳意涵在中國大陆有一定的知名度，為日後於在中國大陆發展打下基礎。在電影《[星月無盡](../Page/星月無盡.md "wikilink")》、《[聽說](../Page/聽說.md "wikilink")》和電視劇《[我要變成硬柿子](../Page/我要變成硬柿子.md "wikilink")》、《[痞子英雄](../Page/痞子英雄.md "wikilink")》皆擔任女主角，令知名度大增，目前是演藝圈中表現亮眼的新生代女演員之一\[6\]。

在2010年第12屆[台北電影節](../Page/台北電影節.md "wikilink")，陳意涵憑電影《[聽說](../Page/聽說.md "wikilink")》獲得「最佳女演員」獎項，演技獲得認可。她所主演的多部電影，例如《[愛LOVE](../Page/LOVE_\(2012年電影\).md "wikilink")》和《[閨蜜](../Page/閨密_\(電影\).md "wikilink")》，皆是口碑票房均好。除了在臺灣，陳意涵也到[中國大陆拍攝戲劇](../Page/中國大陆.md "wikilink")，所以在中國大陆也有知名度。

2014年，陳意涵更憑電影《[軍中樂園](../Page/軍中樂園_\(電影\).md "wikilink")》中的軍妓角色，獲得第51屆金馬獎最佳女配角提名\[7\]。

## 戲劇作品

### 台灣電視劇

|       |        |                                                                               |                                                |       |          |
| ----- | ------ | ----------------------------------------------------------------------------- | ---------------------------------------------- | ----- | -------- |
| 播出年份  | 播出日期   | 電視台                                                                           | 劇名                                             | 飾演    | 備註       |
| 2006年 | 07月16日 | [台視](../Page/台視.md "wikilink")、[三立都會台](../Page/三立都會台.md "wikilink")           | 《[微笑pasta](../Page/微笑pasta.md "wikilink")》     | 田欣    | 客串（第15集） |
| 2007年 | 07月11日 | [華視](../Page/華視.md "wikilink")                                                | 《[我要變成硬柿子](../Page/我要變成硬柿子.md "wikilink")》     | 孫詩詩   | 第一女主角    |
| 2009年 | 04月11日 | [公視主頻道](../Page/公視主頻道.md "wikilink")、[TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink") | 《[痞子英雄](../Page/痞子英雄.md "wikilink")》           | 陳琳    |          |
| 2011年 | 12月18日 | [民視無線台](../Page/民視無線台.md "wikilink")、[八大綜合台](../Page/八大綜合台.md "wikilink")     | 《[華麗的挑戰](../Page/華麗的挑戰_\(電視劇\).md "wikilink")》 | 宮囍    |          |
| 2014年 | 06月1日  | [民視](../Page/民視.md "wikilink")、[八大綜合台](../Page/八大綜合台.md "wikilink")           | 《[你照亮我星球](../Page/你照亮我星球.md "wikilink")》       | 城曼介紹人 | 客串（第4集）  |

### 中國大陸電視劇

|       |        |                                                                   |                                                      |           |           |
| ----- | ------ | ----------------------------------------------------------------- | ---------------------------------------------------- | --------- | --------- |
| 播出年份  | 播出日期   | 首播頻道                                                              | 劇名                                                   | 飾演        | 備註        |
| 2007年 | 05月17日 | [上海電視劇頻道](../Page/上海電視劇頻道.md "wikilink")                          | 《[奮鬥](../Page/奋斗_\(2007年电视剧\).md "wikilink")》        | 方靈珊       | 客串        |
| 2008年 | 10月11日 | [四川衛視](../Page/四川衛視.md "wikilink")                                | 《長江一號》                                               | 小杜鵑（大島貞子） | 配角        |
| 2010年 | 07月12日 | [央視一套](../Page/央視一套.md "wikilink")                                | 《[新流星蝴蝶剑](../Page/流星蝴蝶劍_\(2010年電視劇\).md "wikilink")》 | 孫蝶        | 第一女主角     |
| 2011年 | 07月5日  | [南方衛視](../Page/南方衛視.md "wikilink")                                | 《[摩登新人類](../Page/摩登新人類.md "wikilink")》               | 夏紅果       |           |
| 2013年 | 05月12日 | 福建电视剧频道、江西公共频道                                                    | 《[親情保衛戰](../Page/親情保衛戰.md "wikilink")》               | 瑞真真       |           |
| 2014年 | 07月10日 | [優酷網](../Page/優酷網.md "wikilink")、[土豆網](../Page/土豆網.md "wikilink") | 《[小時代之摺紙時代](../Page/小時代之摺紙時代.md "wikilink")》         | 林蕭        |           |
| 2017年 | 6月11日  | 浙江衛視、北京衛視                                                         | 《[深夜食堂](../Page/深夜食堂_\(亞洲華語版\).md "wikilink")》       | 小梅        | 客串        |
| 2018年 | 1月18日  | [騰訊網](../Page/騰訊網.md "wikilink")                                  | 《[幸福，近在咫尺](../Page/幸福，近在咫尺.md "wikilink")》           | 蔣一依       | |導演暨第一女主角 |
| 待播    |        |                                                                   | 《[愛情築夢師](../Page/愛情築夢師.md "wikilink")》               | 蒙依萌       | 第一女主角     |
| 待播    |        |                                                                   | 《[火柴小姐和美味先生](../Page/火柴小姐和美味先生.md "wikilink")》       | 阮糖        |           |
|       |        | |《[愛在星空下](../Page/愛在星空下.md "wikilink")》                           | 孫小艾                                                  |           |           |
|       |        |                                                                   |                                                      |           |           |

### 電影

<table>
<tbody>
<tr class="odd">
<td><p>上映年份</p></td>
<td><p>上映日期</p></td>
<td><p>片名</p></td>
<td><p>飾演</p></td>
<td><p>備註</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>10月11日</p></td>
<td><p>《<a href="../Page/單車上路.md" title="wikilink">單車上路</a>》</p></td>
<td><p>阿妹</p></td>
<td><p>第一女主角</p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>03月30日</p></td>
<td><p>《<a href="../Page/刺青_(電影).md" title="wikilink">刺青</a>》</p></td>
<td><p>真真</p></td>
<td><p>配角</p></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>04月24日</p></td>
<td><p>《<a href="../Page/星月無盡.md" title="wikilink">星月無盡</a>》</p></td>
<td><p>黃星君</p></td>
<td><p>第一女主角</p></td>
</tr>
<tr class="odd">
<td><p>08月28日</p></td>
<td><p>《<a href="../Page/聽說.md" title="wikilink">聽說</a>》</p></td>
<td><p>秧秧</p></td>
<td><p>第一女主角<br />
獲得第12屆台北電影節「最佳女演員」</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>09月12日</p></td>
<td><p>《<a href="../Page/馬卓.md" title="wikilink">馬卓</a>》</p></td>
<td><p>馬卓</p></td>
<td><p>第一女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p>02月10日</p></td>
<td><p>《<a href="../Page/LOVE_(2012年電影).md" title="wikilink">愛 LOVE</a>》</p></td>
<td><p>|李宜珈</p></td>
<td><p>四大女主角之一</p></td>
</tr>
<tr class="even">
<td><p>08月17日</p></td>
<td><p>《<a href="../Page/BBS鄉民的正義.md" title="wikilink">BBS鄉民的正義</a>》</p></td>
<td><p>李澄湘</p></td>
<td><p>第一女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>12月28日</p></td>
<td><p>《<a href="../Page/花漾.md" title="wikilink">花漾</a>》</p></td>
<td><p>|白小霜</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>12月6日</p></td>
<td><p>《<a href="../Page/愛情無全順.md" title="wikilink">愛情無全順</a>》</p></td>
<td><p>梁小琪</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>07月30日</p></td>
<td><p>《<a href="../Page/閨密_(電影).md" title="wikilink">閨蜜</a>》</p></td>
<td><p>希汶</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>09月5日</p></td>
<td><p>《<a href="../Page/軍中樂園_(電影).md" title="wikilink">軍中樂園</a>》</p></td>
<td><p>阿嬌</p></td>
<td><p>四大女主角之一</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11月28日</p></td>
<td><p>《<a href="../Page/壞姐姐之拆婚聯盟.md" title="wikilink">壞姐姐之拆婚聯盟</a>》</p></td>
<td><p>黃二珊</p></td>
<td><p>第一女主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>04月2日</p></td>
<td><p>《<a href="../Page/咱们结婚吧_(电影).md" title="wikilink">咱们结婚吧</a>》</p></td>
<td><p>顧小蕾</p></td>
<td><p>四大女主角之一</p></td>
</tr>
<tr class="odd">
<td><p>08月7日</p></td>
<td><p>《<a href="../Page/新步步驚心.md" title="wikilink">新步步驚心</a>》</p></td>
<td><p>張小文/馬爾泰•若曦</p></td>
<td><p>第一女主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p>08月19日</p></td>
<td><p>《仙班校園2》(網絡電影)</p></td>
<td><p>火炬手</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p>2月9日</p></td>
<td><p>《<a href="../Page/花甲大人轉男孩.md" title="wikilink">花甲大人轉男孩</a>》</p></td>
<td><p>小美</p></td>
<td><p>友情客串</p></td>
</tr>
<tr class="even">
<td><p>3月2日</p></td>
<td><p>《<a href="../Page/閨蜜2：無二不作.md" title="wikilink">閨蜜2：無二不作</a>》</p></td>
<td><p>希汶</p></td>
<td><p>第一女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3月30日</p></td>
<td><p>《<a href="../Page/我說的都是真的.md" title="wikilink">我說的都是真的</a>》</p></td>
<td><p>印小雪</p></td>
<td><p>特別演出</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>11月30日</p></td>
<td><p>《<a href="../Page/比悲傷更悲傷的故事_(台灣電影).md" title="wikilink">比悲傷更悲傷的故事</a>》</p></td>
<td><p>宋媛媛</p></td>
<td><p>第一女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>待上映</p></td>
<td></td>
<td><p>《<a href="../Page/槍砲腰花.md" title="wikilink">槍砲腰花</a>》</p></td>
<td><p>小楚</p></td>
<td><p>第一女主角</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 網絡劇/電影短片/微電影

|          |                                                                         |       |       |
| -------- | ----------------------------------------------------------------------- | ----- | ----- |
| 播出日期     | 片名                                                                      | 飾演    | 性質    |
| 2006年    | [瑞芳觀光短片](../Page/瑞芳區.md "wikilink")《[黃金密碼](../Page/黃金密碼.md "wikilink")》 | 晶晶    | 第一女主角 |
| 2009年7月  | 新媒體劇《心情spa第一季：女人這點事》                                                    | 陳大發   |       |
| 2010年    | [我猜我猜我猜猜猜之](../Page/我猜我猜我猜猜猜.md "wikilink")《賽琳娜小短劇》宅男任小弟                | 陳意涵女神 |       |
| 2011年3月  | 網路劇《[戀愛SOS](../Page/戀愛SOS.md "wikilink")》                               | 蘇小男   |       |
| 2012年5月  | 果微醺微電影《愛情魔鏡》                                                            | 陳意涵   |       |
| 2012年12月 | 台灣觀光局宣傳微電影《噗通噗通24小時台灣》(푸통푸통 24시 타이완 )                                   | 陳意涵   |       |

### 配音

|      |                                                  |      |       |
| ---- | ------------------------------------------------ | ---- | ----- |
| 年份   | 片名                                               | 配音角色 | 性質    |
| 2015 | 《[腦筋急轉彎](../Page/腦筋急轉彎_\(電影\).md "wikilink")》臺灣版 | 樂樂   | 第一女主角 |

## 音樂錄影帶作品

| 播出日期     | 歌曲             | 歌手                                                                         | 備註                                           |
| -------- | -------------- | -------------------------------------------------------------------------- | -------------------------------------------- |
| 2005年4月  | 紙飛機            | [張峰奇](../Page/張峰奇.md "wikilink")                                           |                                              |
| 2006年6月  | 清晨女子           | [周傳雄](../Page/周傳雄.md "wikilink")                                           |                                              |
| 2006年6月  | 傷痛無聲           | 周傳雄                                                                        | 與[張天霖合作](../Page/張天霖.md "wikilink")          |
| 2006年12月 | 分手不分開          | [王浩信](../Page/王浩信.md "wikilink")                                           |                                              |
| 2009年4月  | 我的依賴           | [蔡依林](../Page/蔡依林.md "wikilink")                                           |                                              |
| 2009年7月  | 禮物             | [羅美玲](../Page/羅美玲.md "wikilink")                                           | 《[痞子英雄](../Page/痞子英雄.md "wikilink")》片段客串     |
| 2009年8月  | 無賴正義           | [趙又廷與](../Page/趙又廷.md "wikilink")[COLOR](../Page/COLOR_BAND.md "wikilink") | 《痞子英雄》片段客串                                   |
| 2009年8月  | 關於我們           | [痞克四](../Page/痞克四.md "wikilink")                                           | 《痞子英雄》片段客串                                   |
| 2009年8月  | 放逐愛情           | [解偉苓](../Page/解偉苓.md "wikilink")                                           | 《痞子英雄》片段客串                                   |
| 2011年2月  | Touch My Heart | [羅志祥](../Page/羅志祥.md "wikilink")                                           |                                              |
| 2012年1月  | 天天月月           | [衛斯里](../Page/衛斯里.md "wikilink")                                           |                                              |
| 2012年2月  | 傻子             | [林宥嘉](../Page/林宥嘉.md "wikilink")                                           | 《[愛 LOVE](../Page/愛_LOVE.md "wikilink")》片段客串 |

## 綜藝節目錄影

  - 中視 《[我猜我猜我猜猜猜](../Page/我猜我猜我猜猜猜.md "wikilink")》人不可貌相 ─
    超迷人\!\!漫畫美少女（2002年8月10日）

<!-- end list -->

  - 東風衛視 《[我愛金城武](../Page/我愛金城武.md "wikilink")》（2007年7月23日-24日）

<!-- end list -->

  - 中天電視 《[小氣大財神](../Page/小氣大財神.md "wikilink")》（2007年7月25日）

<!-- end list -->

  - 華視 《[綜藝大乃霸](../Page/綜藝大乃霸.md "wikilink")》（2007年7月28日）

<!-- end list -->

  - 華視 《[快樂有go正](../Page/快樂有go正.md "wikilink")》（2007年7月28日）

<!-- end list -->

  - 東風衛視 《[王牌大明星](../Page/王牌大明星.md "wikilink")》（2009年3月30日）

<!-- end list -->

  - 中天綜合台 《[康熙來了](../Page/康熙來了.md "wikilink")》（2009年3月31日）

<!-- end list -->

  - 中天綜合台 《[大學生了沒](../Page/大學生了沒.md "wikilink")》（2009年5月4日）

<!-- end list -->

  - 三立都會台 《[型男大主廚](../Page/型男大主廚.md "wikilink")》（2009年5月13日）

<!-- end list -->

  - 中天綜合台 《[大學生了沒](../Page/大學生了沒.md "wikilink")》（2009年8月6日）

<!-- end list -->

  - 《[銀河網路電台](../Page/銀河網路電台.md "wikilink")》電影《聽說》宣傳（2009年8月27日）

<!-- end list -->

  - TVBS歡樂台 《[得獎的事](../Page/得獎的事.md "wikilink")》電影《聽說》宣傳（2009年9月1日）

<!-- end list -->

  - TVBS歡樂台 《[新聞蒐得妙](../Page/新聞蒐得妙.md "wikilink")》電影《聽說》宣傳（2009年9月6日）

<!-- end list -->

  - 三立都會台 《[型男大主廚](../Page/型男大主廚.md "wikilink")》電影《聽說》宣傳（2009年10月6日）

<!-- end list -->

  - 台視《[百万小学堂](../Page/百万小学堂.md "wikilink")》（2010年3月12日）

<!-- end list -->

  - 《[新浪聊天室](../Page/新浪聊天室.md "wikilink")》電視劇《新流星蝴蝶剑》宣传 （2010年7月13日）

<!-- end list -->

  - 《[影视俱乐部](../Page/影视俱乐部.md "wikilink")》電視劇《新流星蝴蝶剑》宣传 （2010年7月12日）

<!-- end list -->

  - 《[最佳现场](../Page/最佳现场.md "wikilink")》 （2010年7月15日）

<!-- end list -->

  - 《[搜狐明星直播间](../Page/搜狐明星直播间.md "wikilink")》（2010年7月15日）

<!-- end list -->

  - 《[网易聊天室](../Page/网易聊天室.md "wikilink")》 （2010年7月15日）

<!-- end list -->

  - 《[影视风云榜](../Page/影视风云榜.md "wikilink")》、《[明星厨房](../Page/明星厨房.md "wikilink")》（2010年7月）

<!-- end list -->

  - 中天綜合台 《[康熙來了](../Page/康熙來了.md "wikilink")》（2010年8月26日）

<!-- end list -->

  - 中視《[綜藝大哥大](../Page/綜藝大哥大.md "wikilink")》( 2010年8月28日)

<!-- end list -->

  - TVBS歡樂台 《海爸王見王》（2010年8月29日）

<!-- end list -->

  - GTV綜合台 《[娛樂百分百](../Page/娛樂百分百.md "wikilink") - 我家也有大明星》（2010年9月3日）

<!-- end list -->

  - GTV綜合台
    《[娛樂百分百](../Page/娛樂百分百.md "wikilink")》電視劇《華麗的挑戰》宣傳（2011年12月14日）

<!-- end list -->

  - GTV綜合台 《[娛樂百分百](../Page/娛樂百分百.md "wikilink") - 華麗新春大對抗》（2012年1月22日）

<!-- end list -->

  - 東風衛視 《[佼個朋友吧](../Page/佼個朋友吧.md "wikilink")》電視劇《華麗的挑戰》宣傳（2012年2月1日）

<!-- end list -->

  - 緯來綜合台《[姊妹淘心話](../Page/姊妹淘心話.md "wikilink")》（2012年2月16日）

<!-- end list -->

  - 中天綜合台 《[SS小燕之夜](../Page/SS小燕之夜.md "wikilink")》（2012年12月20日）

<!-- end list -->

  - 三立都會台 《[型男大主廚](../Page/型男大主廚.md "wikilink")》（2012年12月26日）

<!-- end list -->

  - 中天綜合台 《[康熙來了](../Page/康熙來了.md "wikilink")》（2013年1月3日）

<!-- end list -->

  - 台視《[百万小学堂](../Page/百万小学堂.md "wikilink")》 （2013年1月4日）

<!-- end list -->

  - GTV綜合台 《[娛樂百分百](../Page/娛樂百分百.md "wikilink") - 戀愛百分百》（2013年1月10日）

<!-- end list -->

  - 中天綜合台 《[大學生了沒](../Page/大學生了沒.md "wikilink")》代班主持（2013年1月21日）

<!-- end list -->

  - 中天綜合台 《[康熙來了](../Page/康熙來了.md "wikilink")》（2014年8月25日）

<!-- end list -->

  - 中天綜合台 《[SS小燕之夜](../Page/SS小燕之夜.md "wikilink")》電影《軍中樂園》宣傳（2014年8月29日）

<!-- end list -->

  - 湖南衛視《[快乐大本营](../Page/快乐大本营.md "wikilink")》宣傳電影《[壞姐姐之拆婚連盟](../Page/壞姐姐之拆婚連盟.md "wikilink")》（2014年11月1日）

<!-- end list -->

  - 湖南衛視\]《[花兒與少年第二季](../Page/花兒與少年.md "wikilink")》固定嘉賓 (2015年)

<!-- end list -->

  - 湖南衛視《[快乐大本营](../Page/快乐大本营.md "wikilink")》宣傳電影《[咱們結婚吧
    (電影)](../Page/咱們結婚吧_\(電影\).md "wikilink")》（2015年3月28日）

<!-- end list -->

  - 《一票难求》宣傳电影《咱们结婚吧》（2015年4月3日）

<!-- end list -->

  - 《星映话》宣傳电影《咱们结婚吧》（2015年4月3日）

<!-- end list -->

  - 《腾讯首映礼》宣傳电影《咱们结婚吧》（2015年4月9日）

<!-- end list -->

  - 湖南衛視《[快乐大本营](../Page/快乐大本营.md "wikilink")》宣傳[真人秀](../Page/真人秀.md "wikilink")《[花兒與少年第二季](../Page/花兒與少年.md "wikilink")》（2015年5月23日）

<!-- end list -->

  - 湖南衛視《[快乐大本营](../Page/快乐大本营.md "wikilink")》宣傳電影《[新步步驚心](../Page/新步步驚心.md "wikilink")》（2015年8月1日）

<!-- end list -->

  - 浙江卫视《[奔跑吧兄弟第四季](../Page/奔跑吧兄弟.md "wikilink")》节目嘉宾（2016年）

<!-- end list -->

  - 腾讯视频《[約吧！大明星](../Page/約吧！大明星.md "wikilink")》(2016年6月9日)

<!-- end list -->

  - 《明星大侦探第二季 - 芒果TV明星推理综艺秀》（2017年1月13日）

<!-- end list -->

  - 腾讯视频《[触不到的TA](../Page/触不到的TA.md "wikilink")》跨次元偶像剧真人秀（2017年1月16日）

<!-- end list -->

  - 东方卫视《[越野千里](../Page/越野千里.md "wikilink")》自然探索真人秀 （2017年2月）
  - 浙江卫视《[食在囧途](../Page/食在囧途.md "wikilink")》中国首档原创轻喜剧美食真人秀 （2017年2月25日）
  - 江苏卫视《[来吧，兄弟](../Page/来吧，兄弟.md "wikilink")》美食公主 (2017年8月18日)

## 音樂作品

### 作詞

<table>
<thead>
<tr class="header">
<th><p>首播日期</p></th>
<th><p>歌手</p></th>
<th><p>名稱</p></th>
<th><p>類型</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/王奇.md" title="wikilink">王奇</a></p></td>
<td><p>天晴</p></td>
<td><p>電視劇歌曲</p></td>
<td><p>1.《<a href="../Page/摩登新人類.md" title="wikilink">摩登新人類</a>》電視劇片尾曲<br />
2.陳意涵初嘗音樂創作，為《摩登新人類》主題曲親自作詞</p></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/郭緯.md" title="wikilink">郭緯</a></p></td>
<td><p>薔薇</p></td>
<td><p>電視劇歌曲</p></td>
<td><p>1.《<a href="../Page/摩登新人類.md" title="wikilink">摩登新人類</a>》電視劇插曲<br />
2.陳意涵初嘗音樂創作，為《摩登新人類》插曲親自作詞</p></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><a href="../Page/孫盛希.md" title="wikilink">孫盛希</a></p></td>
<td><p>另一個結局</p></td>
<td><p>電視劇歌曲</p></td>
<td><p>1.《<a href="../Page/幸福，近在咫尺.md" title="wikilink">幸福，近在咫尺</a>》電視劇片尾曲<br />
2.陳意涵首度擔綱導演，並親自為片尾曲作詞</p></td>
</tr>
</tbody>
</table>

### 單曲

<table>
<thead>
<tr class="header">
<th><p>首播日期</p></th>
<th><p>名稱</p></th>
<th><p>類型</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>一起老去</p></td>
<td><p>電影單曲</p></td>
<td><p>1.《<a href="../Page/閨密_(電影).md" title="wikilink">閨蜜</a>》 電影主題曲<br />
2.與<a href="../Page/薛凱琪.md" title="wikilink">薛凱琪</a>、<a href="../Page/楊子姗.md" title="wikilink">楊子姗合唱</a></p></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>最美的情書</p></td>
<td><p>電影單曲</p></td>
<td><p>《新步步驚心》 電影主題曲</p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p>花兒與少年</p></td>
<td><p>電視主題曲</p></td>
<td><p>1.《<a href="../Page/花兒與少年.md" title="wikilink">花兒與少年</a>》 第二季主題曲<br />
2.與<a href="../Page/井柏然.md" title="wikilink">井柏然</a>、<a href="../Page/寧靜.md" title="wikilink">寧靜</a>、<a href="../Page/毛阿敏.md" title="wikilink">毛阿敏</a>、<a href="../Page/郑爽.md" title="wikilink">鄭爽</a>、<a href="../Page/楊洋.md" title="wikilink">楊洋</a>、<a href="../Page/許晴.md" title="wikilink">許晴合唱</a></p></td>
</tr>
</tbody>
</table>

## 文字作品

  - 2012年12月29日 ／北島相遇 Wish you were here.／凱特文化 出版\[8\]／ISBN
    9789865882006

## 廣告

  - 2009[光泉](../Page/光泉.md "wikilink") 茉莉綠茶\[9\]
  - 2009[三菱鉛筆](../Page/三菱鉛筆.md "wikilink") uniball原子筆\[10\]
  - 2009[蒙牛隨變歐羅旋冰品](../Page/蒙牛.md "wikilink")（[比利時篇](../Page/比利時.md "wikilink")\[11\][土耳其篇](../Page/土耳其.md "wikilink")\[12\]）
  - 2010[嬌生美體主張](../Page/嬌生.md "wikilink")\[13\]
  - 2010-2012
    [MAYBELLINE純淨礦物BB霜](../Page/MAYBELLINE.md "wikilink")\[14\]\[15\]\[16\]
  - 2011[寶島眼鏡](../Page/寶島.md "wikilink")\[17\]
  - 2011[台灣啤酒果微醺](../Page/台灣啤酒.md "wikilink")\[18\]
  - 2012[花王洗髮精](../Page/花王.md "wikilink")\[19\]
  - 2013每朝健康茶（學生篇\[20\]、 OL篇\[21\]）
  - 2014Whisper Pure Skin\[22\]
  - Friday's餐廳 豬肋排廣告
  - [7-Eleven](../Page/7-Eleven.md "wikilink") 波卡Lay's人形立牌
  - 含鈺國際形象篇
  - 古道綠茶
  - [統一食品](../Page/統一企業.md "wikilink") 來一客泡麵

## 商品代言

|                                                                      |                                                        |                                       |                                       |
| -------------------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------- | ------------------------------------- |
| 年份                                                                   | 代言品牌                                                   | 品項                                    | 備註                                    |
| 2009年                                                                | 《好自在》                                                  | 全系列產品                                 |                                       |
| 《[嬌生](../Page/嬌生.md "wikilink")》                                     | 美體主張乳液產品                                               |                                       |                                       |
| 《[騰訊](../Page/騰訊.md "wikilink")》                                     | 3D卡通網游QQ西游線上遊戲                                         |                                       |                                       |
| 2010年2月                                                              | 《[Maybelline媚比琳](../Page/Maybelline.md "wikilink")》    | 純淨礦物BB霜、BB慕斯                          |                                       |
| 2011年                                                                | 《[Maybelline媚比琳](../Page/Maybelline.md "wikilink")》    | 純淨礦物BB霜、BB慕斯                          |                                       |
| 《[全家便利商店](../Page/全家便利商店.md "wikilink")》                             | 夏日輕食系列                                                 | 與[姚元浩共同代言](../Page/姚元浩.md "wikilink") |                                       |
| 《[Skechers](../Page/Skechers.md "wikilink")》                         | 時尚拼貼系列                                                 |                                       |                                       |
| 2012年                                                                | 《[全家便利商店](../Page/全家便利商店.md "wikilink")》               | 拼貼時尚系列                                | 與[丁春誠共同代言](../Page/丁春誠.md "wikilink") |
| 《[台灣啤酒](../Page/台灣啤酒.md "wikilink")》                                 | 果微醺系列                                                  |                                       |                                       |
| 《[UNIQLO](../Page/UNIQLO.md "wikilink")》                             | 台灣區代言人                                                 | 與[陳柏霖共同代言](../Page/陳柏霖.md "wikilink") |                                       |
| 《[花王](../Page/花王.md "wikilink")》                                     | 洗髮精                                                    |                                       |                                       |
| 《[Skechers](../Page/Skechers.md "wikilink")》                         | 時尚拼貼系列                                                 |                                       |                                       |
| 2013年                                                                | 《[台灣啤酒](../Page/台灣啤酒.md "wikilink")》                   | 果微醺系列                                 |                                       |
| 《[UNIQLO](../Page/UNIQLO.md "wikilink")》                             | 台灣區代言人                                                 | 與[陳柏霖共同代言](../Page/陳柏霖.md "wikilink") |                                       |
| 《[御茶園](../Page/御茶園.md "wikilink")》                                   | 台灣區代言人                                                 |                                       |                                       |
| 《[Skechers](../Page/Skechers.md "wikilink")》                         | 時尚拼貼系列                                                 |                                       |                                       |
| 《[Biore](../Page/Biore.md "wikilink")[蜜妮](../Page/蜜妮.md "wikilink")》 | 洗顏慕絲系列                                                 |                                       |                                       |
| 《[愛能視iLens](../Page/愛能視iLens.md "wikilink")》                         | 全系列隱形眼鏡產品代言人                                           |                                       |                                       |
| 2014年                                                                | 《好自在》                                                  | 純肌系列產品代言人                             |                                       |
| 《[Citizen Wicca](../Page/Citizen_Wicca.md "wikilink")》               | Wicca系列手錶代言人                                           |                                       |                                       |
| 《[愛能視iLens](../Page/愛能視iLens.md "wikilink")》                         | 全系列隱形眼鏡產品代言人                                           |                                       |                                       |
| 2015年                                                                | 《[Citizen Wicca](../Page/Citizen_Wicca.md "wikilink")》 | Wicca系列手錶代言人                          |                                       |
| 《[愛能視iLens](../Page/愛能視iLens.md "wikilink")》                         | 全系列隱形眼鏡產品代言人                                           |                                       |                                       |
| 2018年                                                                | 《[Samsung](../Page/Samsung.md "wikilink")》             | A8、A8+ 2018版台灣區代言人                    | 與[劉以豪共同代言](../Page/劉以豪.md "wikilink") |

## 公益活動

2009年11月

  - 電影《聽說》在香港上映期間，曙光影視發行公司與慈善機構[龍耳合作舉辦了慈善首映禮](../Page/龍耳.md "wikilink")\[23\]\[24\]。在此次籌辦首映禮，彭于晏聯同聾人隊員與[香港明星足球隊進行足球友誼賽](../Page/香港明星足球隊.md "wikilink")\[25\]，陳意涵擔任啦啦隊為聾人隊員打氣\[26\]，並得到[勞工及福利局局長](../Page/勞工及福利局.md "wikilink")[張建宗](../Page/張建宗.md "wikilink")、[影視明星](../Page/電影演員.md "wikilink")[曾志偉](../Page/曾志偉.md "wikilink")、[羅慧娟及](../Page/羅慧娟.md "wikilink")[立法會議員梁耀宗及正言匯社](../Page/立法會議員.md "wikilink")[張超雄等人的致函支持](../Page/張超雄.md "wikilink")，亦得到多家媒體報道\[27\]\[28\]，向社會大眾推廣「聾健共融」的理念。
  - 陳意涵在香港宣傳電影《聽說》期間，慈善機構[龍耳派出手語導師協助下](../Page/龍耳.md "wikilink")，拍攝一系列十集《手語教室》片段\[29\]，於2009年11月每天早、午、晚時段，於[亞洲電視平台上播放](../Page/亞洲電視.md "wikilink")，藉此加強公眾對手語的認識。

## 獎項

### 大型頒獎禮獎項

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>年份</p></th>
<th style="text-align: left;"><p>獎項</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><strong>2009</strong></p></td>
<td style="text-align: left;"><ul>
<li>腾讯网星光大典－港台年度电视女演员獎</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2010</strong></p></td>
<td style="text-align: left;"><ul>
<li>第12屆台北電影節－最佳女演員獎《<a href="../Page/聽說.md" title="wikilink">聽說</a>》</li>
</ul></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><strong>2015</strong></p></td>
<td style="text-align: left;"><ul>
<li>時尚COSMO美麗盛典－年度美麗偶像大獎</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2016</strong></p></td>
<td style="text-align: left;"><ul>
<li>網易有態度人物盛典－年度最有態度正能量女王</li>
</ul></td>
</tr>
</tbody>
</table>

### 其他

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>年份</p></th>
<th style="text-align: left;"><p>獎項</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><strong>2009</strong></p></td>
<td style="text-align: left;"><ul>
<li><a href="../Page/無名小站.md" title="wikilink">無名小站</a>2009年終排行榜 年度人氣最火正妹第三名[30]</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2012</strong></p></td>
<td style="text-align: left;"><ul>
<li>Y!名人娛樂 2012年度人氣大獎第三名</li>
</ul></td>
</tr>
</tbody>
</table>

## 參考文獻

## 外部連結

  -
  -
  -
  -
  -
  -
[Category:台灣電影女演員](../Category/台灣電影女演員.md "wikilink")
[Category:土城人](../Category/土城人.md "wikilink")
[Y意](../Category/陳姓.md "wikilink")
[Category:景文科技大學校友](../Category/景文科技大學校友.md "wikilink")
[Category:新北市立淡水高級商工職業學校校友](../Category/新北市立淡水高級商工職業學校校友.md "wikilink")
[Category:台北電影節得主](../Category/台北電影節得主.md "wikilink")
[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")

1.
2.  [第七屆華語電影傳媒大賞首輪評審內幕曝光](http://ent.sina.com.cn/m/c/2007-03-09/17461473263.html)，南方都市報,2007年03月09日17:46
3.  [策劃：《Love》台灣新生代明星養成記（3）](http://ent.sina.com.cn/m/2012-02-16/01093555930_3.shtml)，新浪娱乐微博,2012年02月16日01:09
4.  [《奮鬥》北京台收視佳陳意涵網​​絡支持度撥頭籌](http://yule.sohu.com/20071011/n252603736.shtml)，搜狐娛樂,2007年10月11日17:35
5.  [林宥嘉陳意涵揭台灣演藝圈的新生代偶像明星](http://gb.cri.cn/27224/2013/03/12/1042s4049078_2.htm)，金鷹網,2013-03-12
    14:10:57
6.  完全娛樂2010-04-15
7.  [金馬獎2014完整入圍名單出爐](http://www.cna.com.tw/news/firstnews/201410015006-1.aspx)，中央社，2014年10月1日
8.  [博客來-北島相遇Wish you were
    here.](http://www.books.com.tw/products/0010570825)
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30. [無名小站 2009年終排行榜一覽表(最火正妹/最嗆型男)](http://www.wretch.cc/blog/ycorpblog/12196534)