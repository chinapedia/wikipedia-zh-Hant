**陳慧珊**（****，），[美籍華人](../Page/美籍華人.md "wikilink")，[香港著名女演員及歌星](../Page/香港.md "wikilink")，1990年代末至2000年代初[無綫電視](../Page/電視廣播有限公司.md "wikilink")（TVB）迅速走紅的四大當家花旦之一。曾主演了多套廣受歡迎的電視劇，如《[鑑證實錄](../Page/鑑證實錄.md "wikilink")》、《[妙手仁心](../Page/妙手仁心.md "wikilink")》、《[先生貴性](../Page/先生貴性.md "wikilink")》、《[絕世好爸](../Page/絕世好爸.md "wikilink")》、《[衝上雲霄](../Page/衝上雲霄.md "wikilink")》等，擅長飾演專業人士\[1\]。從1999年到2001年度，陳慧珊連續3年當選[壹週刊舉辦全民投票](../Page/壹週刊.md "wikilink")《[壹電視大獎](../Page/壹電視大獎.md "wikilink")》「十大電視藝人
- 第一位」，為眾男女藝人之冠。

音樂事業方面，陳慧珊在2000年度加入樂壇，憑主唱歌曲《[就算我做錯](https://www.youtube.com/watch?v=APTmEE_uXk0)》成為唯一一位[無綫電視經理人合約藝員](../Page/電視廣播有限公司.md "wikilink")（俗稱親生仔女）於樂壇新人時期奪得「[三台冠軍歌](../Page/2000年度香港四台冠軍歌曲列表.md "wikilink")」及《[新城勁爆頒獎禮](../Page/2000年度新城勁爆頒獎禮得獎名單.md "wikilink")》「勁爆歌曲獎」。

演藝事業方面，陳慧珊憑《[絕世好爸](../Page/絕世好爸.md "wikilink")》榮獲《[萬千星輝賀台慶2002](../Page/2002年度萬千星輝賀台慶.md "wikilink")》的「[本年度我最喜愛的女主角](../Page/萬千星輝頒獎典禮最佳女主角.md "wikilink")」及「本年度我最喜愛的電視角色」，成為出道年資最短的「香港雙料視-{后}-」（5年）。2003年度再憑代表作《[衝上雲霄](../Page/衝上雲霄.md "wikilink")》中Belle空姐一角，令她榮獲《[Astro華麗台電視劇大獎2004](../Page/Astro華麗台電視劇大獎2004.md "wikilink")》的「[我的至愛女主角](../Page/TVB_馬來西亞星光薈萃頒獎典禮最喜愛TVB女主角.md "wikilink")」及「我的至愛電視角色」，成為首屆「大馬雙料視-{后}-」，連同其主唱的《衝上雲霄》片尾曲《[我不愛你](https://www.youtube.com/watch?v=bjW7XBuF7RY)》，為陳慧珊的演藝事業推上最高峯。

## 演藝經歷

### 入行經過

陳慧珊在5歲時，隨同家人移民[美國](../Page/美國.md "wikilink")[波士頓](../Page/波士頓.md "wikilink")；並於[波士頓學院修讀](../Page/波士頓學院.md "wikilink")[新聞學](../Page/新聞學.md "wikilink")。畢業後，在波士頓電視台工作，任職助理編導。

1994年，她和前夫[鍾偉明一起](../Page/鍾偉明_\(香港科技大學教授\).md "wikilink")[返回香港後](../Page/香港回流潮.md "wikilink")；經介紹，加入了[香港無綫電視](../Page/香港無綫電視.md "wikilink")[明珠台的新聞部](../Page/明珠台.md "wikilink")，擔任新聞節目及天氣節目主持、記者及編導等。

### 一躍成名

1996年，因為[無綫電視監製](../Page/無綫電視.md "wikilink")[鄧特希的幫助](../Page/鄧特希.md "wikilink")，陳慧珊從[明珠台轉往](../Page/明珠台.md "wikilink")[翡翠台工作](../Page/翡翠台.md "wikilink")，轉職成為藝人，也開始接拍電視劇。因為她的首部電視劇《[壹號皇庭V](../Page/壹號皇庭V.md "wikilink")》，而引起了觀眾的注意。1997年開始成為無綫主力力捧的當紅花旦，更憑《[鑑證實錄](../Page/鑑證實錄.md "wikilink")》以一年的年資首度提名「最佳女主角」（最後五強）。

她從1998年至2004年，一直獲選為《[壹週刊](../Page/壹週刊.md "wikilink")》舉辦的全民投票《[壹電視大獎](../Page/壹電視大獎.md "wikilink")》「十大電視藝人」；並且在1999年到2001年，連續3年當選「十大電視藝人
-
第一位」，為眾男女藝人之冠。她更是1990年代末至2000年代初，[無綫電視公認的當家花旦](../Page/無綫電視.md "wikilink")。

1999年，陳慧珊與[羅嘉良合演的](../Page/羅嘉良.md "wikilink")《[先生貴性](../Page/先生貴性.md "wikilink")》獲得了《[萬千星輝賀台慶1999](../Page/1999年度萬千星輝賀台慶.md "wikilink")》中的「最佳拍擋大獎」。而他們合唱的主題曲：「[對你
我永不放棄](https://www.youtube.com/watch?v=Eoh0EuHM4-E)」（改編自Leo Slayer-When
I Need You )，也大受歡迎。

2000年，陳慧珊亦開始進攻樂壇，推出其首張同名廣東專輯《[陳慧珊](../Page/陳慧珊_\(專輯\).md "wikilink")》。歌曲「[就算我做錯](https://www.youtube.com/watch?v=APTmEE_uXk0)」令陳慧珊以樂壇新人及首位無線藝員姿態奪得[三台冠軍歌](../Page/2000年度香港四台冠軍歌曲列表.md "wikilink")、年底榮獲《[十大勁歌金曲頒獎典禮](../Page/2000年度十大勁歌金曲得獎名單.md "wikilink")》「最受歡迎新人獎(金獎)」、《[新城勁爆頒獎禮](../Page/2000年度新城勁爆頒獎禮得獎名單.md "wikilink")》的「勁爆新登場歌手」、「勁爆歌曲獎－《就算我做錯》」以及《[十大中文金曲頒獎音樂會](../Page/第二十三屆十大中文金曲得獎名單.md "wikilink")》「最有前途新人獎(銀獎)」。與此同時，她在《[創世紀](../Page/創世紀_\(電視劇\).md "wikilink")》中飾演的Helen，也有不俗表現；可惜在拍攝期間，陳慧珊因身體狀況欠佳，而減少了在《[創世紀II天地有情](../Page/創世紀_\(電視劇\).md "wikilink")》中的戲份。2001年，陳慧珊與鍾偉明離婚，但事件未有對她的事業做成影響。

### 事業高峯

2002年，陳慧珊憑《[絕世好爸](../Page/絕世好爸.md "wikilink")》的高珮怡（Tracy）一角榮獲《[萬千星輝賀台慶2002](../Page/2002年度萬千星輝賀台慶.md "wikilink")》的「[本年度我最喜愛的女主角](../Page/萬千星輝頒獎典禮最佳女主角.md "wikilink")」及「本年度我最喜愛的電視角色」，成為出道年資最短的「香港雙料視-{后}-」（5年）。因此，她被捧為TVB的四大當家花旦之一，另外三位是[蔡少芬](../Page/蔡少芬.md "wikilink")、[宣萱及](../Page/宣萱.md "wikilink")[郭可盈](../Page/郭可盈.md "wikilink")。同年播出由[郭晉安及](../Page/郭晉安.md "wikilink")[佘詩曼主演的](../Page/佘詩曼.md "wikilink")《[情牽百子櫃](../Page/情牽百子櫃.md "wikilink")》，原本陳慧珊並無參演該劇，但因該劇頗有迴響，連帶該劇主題曲－由陳慧珊和[許志安合唱的](../Page/許志安.md "wikilink")《[苦口良藥](https://www.youtube.com/watch?v=HfPvF4Q0D2M)》大受歡迎，並成為卡拉OK流行熱點的男女合唱歌曲。

2003年，《[衝上雲霄](../Page/衝上雲霄.md "wikilink")》中Belle一角，絕對是陳慧珊的代表角色，亦為她的事業推上了最高峯。她在劇中無論是空中小姐的制服造型，抑或與[吳鎮宇](../Page/吳鎮宇.md "wikilink")、[馬德鐘錯中複雜的三角關係都令觀眾津津樂道](../Page/馬德鐘.md "wikilink")。她亦憑藉此劇獲得了《[萬千星輝賀台慶2003](../Page/2003年度萬千星輝賀台慶.md "wikilink")》的「我最喜愛的電視角色」，以及《[Astro華麗台電視劇大獎2004](../Page/Astro華麗台電視劇大獎2004.md "wikilink")》的「[我的至愛女主角](../Page/TVB_馬來西亞星光薈萃頒獎典禮最喜愛TVB女主角.md "wikilink")」及「我的至愛電視角色」，成為首屆「大馬雙料視-{后}-」。此外由陳慧珊主唱的《衝上雲霄》片尾曲《[我不愛你](https://www.youtube.com/watch?v=bjW7XBuF7RY)》在坊間亦大受歡迎。

### 外闖時期

2004年，陳慧珊離開[無線電視外出發展](../Page/無線電視.md "wikilink")。她曾表示為了突破從前的討好印象而選擇外闖，因而扮演了許多跟以往不一樣的角色；如：[亞洲電視拍攝的](../Page/亞洲電視.md "wikilink")《[大冒險家](../Page/大冒險家_\(電視劇\).md "wikilink")》中的陶莉娟、[中國大陸](../Page/中國大陸.md "wikilink")[央視合作的](../Page/央視.md "wikilink")《[黃飛鴻與十三姨](../Page/黃飛鴻.md "wikilink")》中的十三姨、[台灣劇](../Page/台劇.md "wikilink")《[維納斯之戀](../Page/維納斯之戀.md "wikilink")》中的王小鷗，以及電影《死心‧不息》中的蔣南；這些突破，亦是她後來接拍《[女人俱樂部](../Page/女人俱樂部.md "wikilink")》的主要原因。此外，她仍經常出席香港地區活動。

### 重返無綫電視

2006年，陳慧珊重返[無綫電視](../Page/無綫電視.md "wikilink")，為無綫主持節目《[細說名城](../Page/細說名城.md "wikilink")》，並與無綫簽署部頭合約。

2007年2月的一次報紙訪問中\[2\]，陳慧珊本人親口證實，已於2006年在[美國秘密結婚](../Page/美國.md "wikilink")，丈夫是多年好友兼任經理人鍾家鴻（Mike）。同年，陳慧珊承認懷孕，並準備做最少一年的全職母親，因此將[無綫電視的工作暫時延後](../Page/無綫電視.md "wikilink")。同年9月16日，在香港一間醫院產下一女鍾律然\[3\]。

2009年7月，陳慧珊出演時裝[電視劇](../Page/電視劇.md "wikilink")《[搜下留情](../Page/搜下留情.md "wikilink")》，與[馬德鐘共同演出](../Page/馬德鐘.md "wikilink")，並於2010年間播出。

2012年2月，其夫鍾家鴻的公司經營不善破產，陳慧珊宣佈復出；下半年將返回[無綫電視拍劇](../Page/無綫電視.md "wikilink")，並落實參演《[衝上雲霄II](../Page/衝上雲霄II.md "wikilink")》，可惜最後亦無緣參與當中。其後於2012年7月宣佈，9月會在香港大學就讀碩士學位，為將來可以在美國教書，並與丈夫、女兒返回美國生活；暫時息影。

2013年4月，陳慧珊演出[曾志偉監製的重頭劇](../Page/曾志偉.md "wikilink")《[女人俱樂部](../Page/女人俱樂部.md "wikilink")》。她為了重返無綫，而主動提出向香港大學申請將英文碩士課程延期。陳慧珊在《[女人俱樂部](../Page/女人俱樂部.md "wikilink")》飾演的容丹丹，是自《[隔世追兇](../Page/隔世追兇.md "wikilink")》後再次飾演反派。

2015年3月，陳慧珊再與[黃智賢合演](../Page/黃智賢.md "wikilink")《[波士早晨](../Page/波士早晨.md "wikilink")》，並會與黃智賢有多場吵嘴戲份。同年5月陳慧珊拍畢此劇後，因合約約滿而離巢無綫，結束與無綫9年之賓主關係。此劇最後安排於2018年2月中播出，惟她已經轉到[ViuTV擔任節目](../Page/ViuTV.md "wikilink")[監製](../Page/監製.md "wikilink")，有傳因此其戲份被大量刪剪，致劇集由原本20集刪減至15集，而在宣傳片中，其頭像亦被縮小並放在左下角。在2月6日的中環街頭宣傳活動中，其於劇集宣傳海報及紀念品的頭像更被刪走，震怒網民。\[4\]\[5\]

### 擔任監製

2017年10月，於[ViuTV](../Page/ViuTV.md "wikilink")「2018年節目發布會」中，陳慧珊將會為一個综藝節目擔任監製，在節目其中一個環節她將會到[尼泊爾探討婚姻及體驗當地生活](../Page/尼泊爾.md "wikilink")，罕有一家三口齊齊亮相節目，接受訪問時直言起初被邀請也擔心是否如真人騷，當知道自己的環節能親自任監製才樂意去做。並希望將來有機會自編自導自演。表示自己剛完成了一個關於人生選擇課題的劇本，現正和各方洽談能否製作成節目，為社會作出貢獻\[6\]。陳慧珊於[香港大學修讀完英文碩士後](../Page/香港大學.md "wikilink")，目前仍修讀[博士學位課程](../Page/博士.md "wikilink")。

## 演出作品

### 電視劇集（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                          |                                                |                |
| ---------------------------------------- | ---------------------------------------------- | -------------- |
| **首播**                                   | **劇名**                                         | **角色**         |
| 1997年                                    | [壹號皇庭V](../Page/壹號皇庭V.md "wikilink")           | 歐子強（Catherine） |
| [鑑證實錄](../Page/鑑證實錄.md "wikilink")       | 聶寶言（Pauline）                                   |                |
| 1998年                                    | [妙手仁心](../Page/妙手仁心.md "wikilink")             | 江新月（Annie）     |
| 1999年                                    | [鑑證實錄II](../Page/鑑證實錄.md "wikilink")           | 聶寶言（Pauline）   |
| [先生貴性](../Page/先生貴性.md "wikilink")       | 方思晴（Christine）                                 |                |
| [吾係差人](../Page/吾係差人.md "wikilink")       | 江碧晴（Sunnie）                                    |                |
| [創世紀](../Page/創世紀_\(電視劇\).md "wikilink") | 霍希賢（Helen）                                     |                |
| 2000年                                    | [創世紀II天地有情](../Page/創世紀_\(電視劇\).md "wikilink") |                |
| [妙手仁心II](../Page/妙手仁心.md "wikilink")     | 江新月（Annie）                                     |                |
| 2001年                                    | [美味情緣](../Page/美味情緣.md "wikilink")             | 喬花枝（Joyce）     |
| 2002年                                    | [騎呢大狀](../Page/騎呢大狀.md "wikilink")             | 戴　歡（Joey）      |
| [烈火雄心II](../Page/烈火雄心II.md "wikilink")   | 何寶琳（Michelle）                                  |                |
| [絕世好爸](../Page/絕世好爸.md "wikilink")       | 高佩怡（Tracy）                                     |                |
| 2003年                                    | [衝上雲霄](../Page/衝上雲霄.md "wikilink")             | 樂以珊（Isabelle）  |
| 2004年                                    | [翡翠戀曲](../Page/翡翠戀曲.md "wikilink")             | 莫希兒（Tiffany）   |
| [隔世追兇](../Page/隔世追兇.md "wikilink")       | 高珊／雷嬅（Nicole）／丁芷韻                              |                |
| 2010年                                    | [搜下留情](../Page/搜下留情.md "wikilink")             | 鄭笑欣            |
| 2014年                                    | [女人俱樂部](../Page/女人俱樂部.md "wikilink")           | 容丹丹（Diana）     |
| 2018年                                    | [波士早晨](../Page/波士早晨.md "wikilink")             | 路在林（Hazel）     |

### 電視劇集（其他）

|        |                                                                                                                |                                  |
| ------ | -------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| **首播** | **劇名**                                                                                                         | **角色**                           |
| 1998年  | [香港電台](../Page/香港電台.md "wikilink")《[非常平等任務：危險遊戲》](http://app4.rthk.hk/special/rthkmemory/details/tv-drama/485) | 馬小姐                              |
| 2004年  | 《[Like My Own](../Page/Like_My_Own_\(新加坡英語電視劇\).md "wikilink")》                                                | Mabel Lim                        |
| 2005年  | [亞洲電視](../Page/亞洲電視.md "wikilink")《[大冒險家](../Page/大冒險家_\(電視劇\).md "wikilink")》                                 | 陶莉娟                              |
| 2005年  | 《[黃飛鴻與十三姨](../Page/黃飛鴻.md "wikilink")》                                                                         | [十三姨](../Page/十三姨.md "wikilink") |
| 2005年  | 《[維納斯之戀](../Page/維納斯之戀.md "wikilink")》                                                                         | 王小鷗                              |
| 2007年  | 《[狼烟](../Page/狼烟.md "wikilink")》                                                                               | 韓葉                               |
| 2010年  | 《[杜拉拉升職記](../Page/杜拉拉升職記_\(電視劇\).md "wikilink")》                                                               | 陳玫瑰（Rose）                        |
| 2011年  | 《[丈母娘来了](../Page/丈母娘来了.md "wikilink")》                                                                         | 翁經理（Wendy）                       |
| 2015年  | 《[港媳嫁到](../Page/港媳嫁到.md "wikilink")》                                                                           | Fiona                            |
|        |                                                                                                                |                                  |

### 電視電影（[無綫電視](../Page/無綫電視.md "wikilink")）

|        |                                      |        |
| ------ | ------------------------------------ | ------ |
| **首播** | **片名**                               | **角色** |
| 2004年  | 《[伴我驕陽](../Page/伴我驕陽.md "wikilink")》 | 劉愷     |

### 電影

|        |                                                    |               |
| ------ | -------------------------------------------------- | ------------- |
| **首播** | **片名**                                             | **角色**        |
| 1994年  | 《[Fatal Maple](../Page/Fatal_Maple.md "wikilink")》 | Ellen         |
| 2001年  | 《[愛情觀自在](../Page/愛情觀自在.md "wikilink")》             | 小靜            |
| 2002年  | 《[乾柴烈火](../Page/乾柴烈火.md "wikilink")》               | Michelle      |
| 2006年  | 《[死心‧不息](../Page/死心‧不息.md "wikilink")》             | 蔣南            |
| 2007年  | 《[神探](../Page/神探.md "wikilink")》                   | 陳桂彬妻子         |
| 2008年  | 《[保持通話](../Page/保持通話.md "wikilink")》               | Jeanie （阿邦之姊） |

### 電視節目主持

  - 《Eye on Hong Kong》
  - 《City Focus》
  - 《Pearl Watch》
  - 《City Life》
  - 《[細說名城](../Page/細說名城.md "wikilink")》

### 電視節目製作人

  - 2019年：《放學後》（[香港開電視](../Page/香港開電視.md "wikilink")）

## 音樂作品（唱片）

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行商</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/陳慧珊_(專輯).md" title="wikilink">陳慧珊</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/新藝寶唱片.md" title="wikilink">新藝寶唱片</a></p></td>
<td style="text-align: left;"><p>2000年6月</p></td>
<td style="text-align: left;"><ol>
<li>就算我做錯</li>
<li>乾脆</li>
<li>固定伴侶（陳慧珊/蘇永康）</li>
<li>你是誰</li>
<li>壞男人</li>
<li>紅綠燈</li>
<li>就係她（他）</li>
<li>對你太在乎（Live Version）（陳慧珊/吳國敬）</li>
<li>就算我做錯（Remix）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/自在_(專輯).md" title="wikilink">自在</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/新藝寶唱片.md" title="wikilink">新藝寶唱片</a></p></td>
<td style="text-align: left;"><p>2001年6月7日</p></td>
<td style="text-align: left;"><ol>
<li>我還記得我是誰</li>
<li>迷信</li>
<li>撒旦與天使</li>
<li>相信直覺</li>
<li>模範生</li>
<li>悲哀不悲哀</li>
<li>水造了我</li>
<li>望住我</li>
<li>不是我的</li>
<li>自在</li>
<li>只知我難避開</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p><a href="../Page/愛得起_(陳慧珊專輯).md" title="wikilink">愛得起</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/新藝寶唱片.md" title="wikilink">新藝寶唱片</a></p></td>
<td style="text-align: left;"><p>2002年10月21日</p></td>
<td style="text-align: left;"><ol>
<li>怕你怕我怕（The Love We Make）</li>
<li>愛得起</li>
<li>蜘蛛俠</li>
<li>別人</li>
<li>自作自受</li>
<li>嫌你太好</li>
<li>無痛分手</li>
<li>暖流（陳慧珊/蘇永康）</li>
<li>有緣人</li>
<li>泥公仔</li>
<li>苦口良藥（陳慧珊/許志安）</li>
<li>抱緊眼前人</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p><a href="../Page/失而復得:_陳慧珊.md" title="wikilink">失而復得: 陳慧珊</a></p></td>
<td style="text-align: left;"><p>精選</p></td>
<td style="text-align: left;"><p><a href="../Page/新藝寶唱片.md" title="wikilink">新藝寶唱片</a></p></td>
<td style="text-align: left;"><p>2006年8月24日</p></td>
<td style="text-align: left;"><ol>
<li>抱緊眼前人</li>
<li>暖流（陳慧珊/蘇永康）</li>
<li>固定伴侶（陳慧珊/蘇永康）</li>
<li>我不愛你</li>
<li>苦口良藥（陳慧珊/許志安）</li>
<li>有緣人</li>
<li>我還記得我是誰</li>
<li>自在</li>
<li>模範生</li>
<li>迷信</li>
<li>撒旦與天使</li>
<li>愛得起</li>
<li>只知我難避開</li>
<li>就算我做錯</li>
<li>乾脆</li>
<li>對你太在乎（Live Version）（陳慧珊/吳國敬）</li>
</ol></td>
</tr>
</tbody>
</table>

### 其他電視劇歌曲

|          |          |                                         |        |                                     |
| -------- | -------- | --------------------------------------- | ------ | ----------------------------------- |
| **年份**   | **歌曲**   | **劇名**                                  | **性質** | **備註**                              |
| 1998年12月 | 對你，我永不放棄 | 電視劇《[先生貴性](../Page/先生貴性.md "wikilink")》 | 主題曲    | 與[羅嘉良合唱](../Page/羅嘉良.md "wikilink") |
| 2002年10月 | 有緣人      | 電視劇《[絕世好爸](../Page/絕世好爸.md "wikilink")》 | 插曲     |                                     |
| 2003年10月 | 我不愛你     | 電視劇《[衝上雲霄](../Page/衝上雲霄.md "wikilink")》 | 插曲     |                                     |
| 2004年6月  | 無用的我     | 電視劇《[隔世追兇](../Page/隔世追兇.md "wikilink")》 | 插曲     |                                     |
|          |          |                                         |        |                                     |

## 派台歌曲成績

| **派台歌曲上榜最高位置**                                                     |
| ------------------------------------------------------------------ |
| 唱片                                                                 |
| **1998年**                                                          |
| [對你，我永不放棄](../Page/對你，我永不放棄.md "wikilink")                         |
| **1999年**                                                          |
|                                                                    |
| [陳慧珊](../Page/陳慧珊_\(專輯\).md "wikilink")                            |
| **2000年**                                                          |
| 陳慧珊                                                                |
| 陳慧珊                                                                |
| [Rain 85013620](../Page/Rain_85013620.md "wikilink")（Bonus CD 特別版） |
| **2001年**                                                          |
| [自在](../Page/自在_\(專輯\).md "wikilink")                              |
| 自在                                                                 |
| **2002年**                                                          |
| [愛得起](../Page/愛得起_\(陳慧珊專輯\).md "wikilink")                         |
| 愛得起                                                                |
| **2003年**                                                          |
| [失而復得: 陳慧珊](../Page/失而復得:_陳慧珊.md "wikilink")                       |

| **各台冠軍歌總數**                           |
| ------------------------------------- |
| [903](../Page/903_專業推介.md "wikilink") |
| **0**                                 |

## 獎項

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>頒獎典禮</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p>'''劇名/歌名</p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/鑑證實錄.md" title="wikilink">鑑證實錄</a>》</p></td>
<td><p>聶寶言</p></td>
<td><p><br />
（最後五強）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>最佳拍擋大獎</p></td>
<td><p>《<a href="../Page/妙手仁心.md" title="wikilink">妙手仁心</a>》</p></td>
<td><p>江新月、黎國柱( <a href="../Page/林保怡.md" title="wikilink">林保怡</a> )</p></td>
<td><p><br />
（最後五強）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/壹電視大獎.md" title="wikilink">壹電視大獎</a></p></td>
<td><p>十大電視藝人第七位</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/壹電視大獎.md" title="wikilink">壹電視大獎</a></p></td>
<td><p>十大電視藝人第一位</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/壹電視大獎.md" title="wikilink">壹電視大獎</a></p></td>
<td><p>最上鏡藝人</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/鑑證實錄II.md" title="wikilink">鑑證實錄II</a>》</p></td>
<td><p>聶寶言</p></td>
<td><p><br />
（最後五強）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>最佳拍擋大獎</p></td>
<td><p>《<a href="../Page/先生貴性.md" title="wikilink">先生貴性</a>》</p></td>
<td><p>方思晴、鄧炳權( <a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a> )</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/壹電視大獎.md" title="wikilink">壹電視大獎</a></p></td>
<td><p>十大電視藝人第一位</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/十大勁歌金曲頒獎典禮.md" title="wikilink">十大勁歌金曲頒獎典禮</a></p></td>
<td><p>最受歡迎新人獎(金獎)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/十大勁歌金曲頒獎典禮.md" title="wikilink">十大勁歌金曲頒獎典禮</a></p></td>
<td><p>最受歡迎合唱歌曲獎(銅獎)</p></td>
<td><p>《<a href="https://www.youtube.com/watch?v=8kkaAJEp2LQ">將生活留給自己</a>》</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新城勁爆歌曲頒獎禮.md" title="wikilink">新城勁爆歌曲頒獎禮</a></p></td>
<td><p>勁爆新登場歌手</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新城勁爆歌曲頒獎禮.md" title="wikilink">新城勁爆歌曲頒獎禮</a></p></td>
<td><p>勁爆歌曲獎</p></td>
<td><p>《<a href="https://www.youtube.com/watch?v=APTmEE_uXk0">就算我做錯</a>》</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/十大中文金曲頒獎音樂會.md" title="wikilink">十大中文金曲頒獎音樂會</a></p></td>
<td><p>最有前途新人獎(銀獎)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/壹電視大獎.md" title="wikilink">壹電視大獎</a></p></td>
<td><p>十大電視藝人第一位</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>最佳拍擋大獎</p></td>
<td><p>《<a href="../Page/美味情緣.md" title="wikilink">美味情緣</a>》</p></td>
<td><p>喬花枝、馬　友( <a href="../Page/吳啟華.md" title="wikilink">吳啟華</a> )</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/絕世好爸.md" title="wikilink">絕世好爸</a>》</p></td>
<td><p>高珮怡</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>我最喜愛的電視角色</p></td>
<td><p>《<a href="../Page/絕世好爸.md" title="wikilink">絕世好爸</a>》</p></td>
<td><p>高珮怡</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/明報周刊.md" title="wikilink">明報周刊演藝動力大獎</a></p></td>
<td><p>最突出電視女藝員</p></td>
<td><p>《<a href="../Page/騎呢大狀.md" title="wikilink">騎呢大狀</a>》</p></td>
<td><p>戴　歡</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/壹電視大獎.md" title="wikilink">壹電視大獎</a></p></td>
<td><p>十大電視藝人第六位</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/衝上雲霄.md" title="wikilink">衝上雲霄</a>》</p></td>
<td><p>樂以珊</p></td>
<td><p><br />
（最後五強）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>我最喜愛的電視角色</p></td>
<td><p>《<a href="../Page/衝上雲霄.md" title="wikilink">衝上雲霄</a>》</p></td>
<td><p>樂以珊</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/壹電視大獎.md" title="wikilink">壹電視大獎</a></p></td>
<td><p>十大電視藝人第六位</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/Astro華麗台電視劇大獎.md" title="wikilink">Astro華麗台電視劇大獎</a></p></td>
<td><p>我的至愛女主角</p></td>
<td><p>《<a href="../Page/衝上雲霄.md" title="wikilink">衝上雲霄</a>》</p></td>
<td><p>樂以珊</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Astro華麗台電視劇大獎.md" title="wikilink">Astro華麗台電視劇大獎</a></p></td>
<td><p>我的至愛角色</p></td>
<td><p>《<a href="../Page/衝上雲霄.md" title="wikilink">衝上雲霄</a>》</p></td>
<td><p>樂以珊</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/壹電視大獎.md" title="wikilink">壹電視大獎</a></p></td>
<td><p>十大電視藝人第七位</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萬千星輝頒獎典禮.md" title="wikilink">萬千星輝頒獎典禮</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/隔世追兇.md" title="wikilink">隔世追兇</a>》</p></td>
<td><p>高珊／雷嬅（Nicole）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 家庭

母親是上海人，所以Flora很愛吃上海菜。

## 廣告代言

  - 2001年：[瑪花纖體](https://www.youtube.com/watch?v=vz0gsFO78Hw)（[彭羚](../Page/彭羚.md "wikilink")、陳慧珊、[鍾麗緹](../Page/鍾麗緹.md "wikilink")）
  - 2001年：[Neutrogena 24小時毛孔細緻組合](https://www.youtube.com/watch?v=vkj3wwQs-gQ)
  - 2008年：[詩琳美容（事業篇）](https://www.youtube.com/watch?v=-Yfp_5tPwdA)[（戀愛篇）](https://www.youtube.com/watch?v=o2UydrFD8aQ)[（家庭篇）](https://www.youtube.com/watch?v=TpDEHBbvsqs)
  - 2017-2018年：健麗國際醫學美容集團

## 參考

<references/>

## 外部連結

  -
[wai](../Category/陳姓.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:台山人](../Category/台山人.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")
[Category:香港電視女演員](../Category/香港電視女演員.md "wikilink")
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[陳](../Category/香港專欄作家.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:前無綫電視新聞報導員](../Category/前無綫電視新聞報導員.md "wikilink")
[Category:壹電視大獎十大電視藝人得主](../Category/壹電視大獎十大電視藝人得主.md "wikilink")
[Category:波士頓學院校友](../Category/波士頓學院校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:美国女歌手](../Category/美国女歌手.md "wikilink")
[Category:美國女性模特兒](../Category/美國女性模特兒.md "wikilink")

1.
2.  2007年2月19日(當年农历新年初二)前後某天的報導，载于香港[太阳报等各大报纸](../Page/太阳报.md "wikilink")。
3.  [紅男綠女-FnM 生活誌.htm](http://ladyzone.sinchew-i.com/node/642)
4.
5.
6.  [陳慧珊一家三口拍真人騷
    親自監製ViuTV節目](http://bka.mpweekly.com/focus/local/20171010-85557)