**陳永明**，[香港語言學者](../Page/香港.md "wikilink")，曾為[香港電台主持電視文教節目](../Page/香港電台.md "wikilink")。亦擅[哲學](../Page/哲學.md "wikilink")，著作曾為香港暢銷書之一。

## 生平

畢業於[香港中文大學](../Page/香港中文大學.md "wikilink")[新亞書院中文系](../Page/新亞書院.md "wikilink")，其後在美國[耶魯大學攻讀](../Page/耶魯大學.md "wikilink")[哲學碩士](../Page/哲學碩士.md "wikilink")，並在[威斯康辛大學東亞研究系攻讀](../Page/威斯康辛大學.md "wikilink")[博士](../Page/博士.md "wikilink")，曾任[香港浸會大學中文系](../Page/香港浸會大學.md "wikilink")-{}-系主任及[香港教育大學語文教育學院院長](../Page/香港教育大學.md "wikilink")。

於1996年開始在電視台及電台主持文化節目，曾在[亞洲電視主持](../Page/亞洲電視.md "wikilink")「優勝之道論孔子」，並在[香港電台主持](../Page/香港電台.md "wikilink")「思前後想」、「詩韻詞情」、「文化·com」及「中文一分鐘」節目。他曾為《[信報](../Page/信報.md "wikilink")》〈繁星哲語〉專欄、《[香港聯合報](../Page/香港聯合報.md "wikilink")》〈望遠集〉及〈摘藝〉專欄撰稿。陳教授的著作涉獵面十分廣，由中國文學至哲學，以致音樂，都有獨特的見解，著作包括有《中國文學散論》、《哲學子午線》、《音樂子午線》、《哲人哲語》、《原來孔子》及《五線譜上的躑躅》等。\[1\]

## 著作

  - [哲學子午線](../Page/哲學子午線.md "wikilink")
  - 音樂子午線
  - 哲人哲語
  - 原來孔子
  - 原來尼采

## 節目主持

  - 《[中文一分鐘](../Page/中文一分鐘.md "wikilink")》：[香港電台](../Page/香港電台.md "wikilink")
  - 《[優勝之道論孔子](../Page/優勝之道論孔子.md "wikilink")》：[亞洲電視](../Page/亞洲電視.md "wikilink")[本港台配音劇](../Page/本港台.md "wikilink")《[孔子](../Page/孔子_\(1990年電視劇\).md "wikilink")》解說

## 參考資料

<references/>

## 外部連結

[彩雲見證-- 陳永明](http://www.shanfook.org.hk/shanfook/html/cloud29.html)

[Category:陈姓](../Category/陈姓.md "wikilink")
[Category:香港浸會大學教授](../Category/香港浸會大學教授.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:語言學家](../Category/語言學家.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Category:威斯康辛大學校友](../Category/威斯康辛大學校友.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:香港教育大學教授](../Category/香港教育大學教授.md "wikilink")

1.  [陳永明教授簡介](http://www.skcgss.edu.hk/asp/lang_week/Info/Intro2.html)