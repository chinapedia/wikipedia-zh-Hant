**陳浩民**（，），香港演員及歌手，妻子是[湖南籍](../Page/湖南.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")[蔣麗莎](../Page/蔣麗莎.md "wikilink")，兩人育有一子三女。

## 演藝歷程

1994年，陳浩民為[維他奶拍攝](../Page/維他奶.md "wikilink")[廣告而加入娛樂圈](../Page/廣告.md "wikilink")，之後成為[香港無綫電視藝員](../Page/香港無綫電視.md "wikilink")，演出多部戲劇，最為人熟悉的是演出《[西遊記（貳）](../Page/西遊記（貳）.md "wikilink")》[孫悟空一角](../Page/孫悟空.md "wikilink")。其他主要作品有《[天龍八部](../Page/天龍八部_\(1997年電視劇\).md "wikilink")》、《[封神榜](../Page/封神榜_\(2001年電視劇\).md "wikilink")》、《[人龍傳說](../Page/人龍傳說.md "wikilink")》等；近年主力在[中國大陆和](../Page/中國大陆.md "wikilink")[台灣地区工作](../Page/台灣地区.md "wikilink")，拍了多部中國電視劇。

## 家庭狀況

於2011年7月與[中國](../Page/中國.md "wikilink")[湖南](../Page/湖南.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")[蔣麗莎奉子成婚](../Page/蔣麗莎.md "wikilink")，同年11月底誕下長女陳雅薷，2013年再添儿子陈璟逸，湊成個「好」字，2014年二女儿陳若嫣出生，2016年3月30日，再添一女Victoria，共育有一子三女。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                           |                                                 |                                      |
| ----------------------------------------- | ----------------------------------------------- | ------------------------------------ |
| **首播**                                    | **劇名**                                          | **角色**                               |
| 1997年                                     | [天龍八部](../Page/天龍八部_\(1997年電視劇\).md "wikilink") | [段譽](../Page/段譽.md "wikilink")       |
| 1998年                                     | [聊齋（貳）之鬼母痴兒](../Page/聊齋（貳）.md "wikilink")       | 石天生                                  |
| [聊齋（貳）之魅影靈狐](../Page/聊齋（貳）.md "wikilink") | 桑　曉                                             |                                      |
| [西遊記（貳）](../Page/西遊記（貳）.md "wikilink")    | [孫悟空](../Page/孫悟空.md "wikilink")                |                                      |
| 1999年                                     | [反黑先鋒](../Page/反黑先鋒.md "wikilink")              | 常健康（John）                            |
| [人龍傳說](../Page/人龍傳說.md "wikilink")        | 葉　希                                             |                                      |
| 2000年                                     | [封神榜](../Page/封神榜_\(2001年電視劇\).md "wikilink")   | 李[哪吒](../Page/哪吒.md "wikilink")、火德星君 |
| [美麗人生](../Page/美麗人生_\(港劇\).md "wikilink") | 劉二索（Stone）                                      |                                      |
| [陀槍師姐II](../Page/陀槍師姐II.md "wikilink")    | 常健康（John）（客串）                                   |                                      |
| 2001年                                     | [天子尋龍](../Page/天子尋龍.md "wikilink")              | [李隆基](../Page/李隆基.md "wikilink")     |
| 2003年                                     | [智勇新警界](../Page/智勇新警界.md "wikilink")            | 陶展雄                                  |
| 2006年                                     | [人生馬戲團](../Page/人生馬戲團.md "wikilink")            | 何峻峰（Hill）                            |
| [施公奇案](../Page/施公奇案.md "wikilink")        | [黃天霸](../Page/黃天霸.md "wikilink")                |                                      |
| 2007年                                     | [天機算](../Page/天機算.md "wikilink")                | 葉　楊                                  |
| 2008年                                     | [搜神傳](../Page/搜神傳.md "wikilink")                | [石敢當](../Page/石敢當.md "wikilink")     |

### 電視劇（中國大陆）

| 首播年份                                                  | 片名                                                                 | 角色                               | 備註                                 |
| ----------------------------------------------------- | ------------------------------------------------------------------ | -------------------------------- | ---------------------------------- |
| 1999年                                                 | [莊姬公主](../Page/莊姬公主.md "wikilink")                                 | [趙武](../Page/趙武.md "wikilink")   |                                    |
| 2002年                                                 | [銀鼠](../Page/銀鼠.md "wikilink")                                     | 仇隱                               |                                    |
| [一網情深](../Page/一網情深.md "wikilink")                    | 何嘉林                                                                |                                  |                                    |
| 2003年                                                 | [乾隆下江南](../Page/乾隆下江南_\(電視劇\).md "wikilink")                       | [乾隆帝](../Page/乾隆帝.md "wikilink") |                                    |
| [牽手向明天](../Page/牽手向明天.md "wikilink")                  | 張君華                                                                |                                  |                                    |
| [帶我飛帶我走](../Page/帶我飛帶我走.md "wikilink")                | 潘彬彬                                                                |                                  |                                    |
| 2004年                                                 | [六指琴魔](../Page/六指琴魔.md "wikilink")                                 | 呂麟                               |                                    |
| 2005年                                                 | [劍出江南](../Page/劍出江南.md "wikilink")                                 | 慕容真                              |                                    |
| [煙花三月](../Page/煙花三月.md "wikilink")                    | [納蘭容若](../Page/納蘭容若.md "wikilink")                                 |                                  |                                    |
| 2006年                                                 | [愛情機器](../Page/愛情機器.md "wikilink")                                 | 紀名遠                              |                                    |
| [大清后宫之还君明珠](../Page/大清后宫之还君明珠.md "wikilink")          | 榮廣海                                                                |                                  |                                    |
| 2007年                                                 | [食神](../Page/食神_\(2006年電視劇\).md "wikilink")                        | 徐教授                              |                                    |
| [聊齋2](../Page/聊斋_\(2007年电视剧\).md "wikilink")          | 郝天全                                                                | 单元男主角                            |                                    |
| [楚留香傳奇](../Page/楚留香傳奇_\(電視劇\).md "wikilink")          | [胡鐵花](../Page/胡鐵花_\(小說\).md "wikilink")                            |                                  |                                    |
| 2008年                                                 | [愛夢電影第四季](../Page/愛夢電影.md "wikilink")                              | 胡青洋                              | [電視電影](../Page/電視電影.md "wikilink") |
| [巧克力情人](../Page/巧克力情人.md "wikilink")                  | 項少揚                                                                |                                  |                                    |
| 2009年                                                 | [多情女人癡情男](../Page/多情女人癡情男.md "wikilink")                           | 馮　寬                              |                                    |
| [包青天之通判劫](../Page/包青天_\(2009年電視劇\).md "wikilink")     | 唐　真                                                                | 單元男主角                            |                                    |
| 2010年                                                 | [包三姑外傳](../Page/包三姑外傳.md "wikilink")                               | 書生縣令                             |                                    |
| [新包青天之七俠五義](../Page/新包青天之七俠五義.md "wikilink")          | [錦毛鼠](../Page/錦毛鼠.md "wikilink")([白玉堂](../Page/白玉堂.md "wikilink")) | 单元男主角                            |                                    |
| [活佛濟公](../Page/活佛濟公.md "wikilink")                    | [濟　公](../Page/濟公.md "wikilink")                                    |                                  |                                    |
| 2011年                                                 | [十二生肖傳奇](../Page/十二生肖傳奇.md "wikilink")                             | 米俊非                              |                                    |
| [秦香蓮](../Page/秦香蓮_\(電視劇\).md "wikilink")              | [陳世美](../Page/陳世美.md "wikilink")                                   |                                  |                                    |
| [天地姻緣七仙女](../Page/天地姻緣七仙女.md "wikilink")              | 梅若瑩                                                                |                                  |                                    |
| [大唐女巡按](../Page/大唐女巡按.md "wikilink")                  | 連庭飛                                                                |                                  |                                    |
| [活佛濟公2](../Page/活佛濟公2.md "wikilink")                  | [濟　公](../Page/濟公.md "wikilink")                                    |                                  |                                    |
| 2012年                                                 | [卜案](../Page/卜案.md "wikilink")                                     | [李淳風](../Page/李淳風.md "wikilink") |                                    |
| [西施秘史](../Page/西施秘史.md "wikilink")                    | [范　蠡](../Page/范蠡.md "wikilink")                                    |                                  |                                    |
| [薛平貴與王寶釧](../Page/薛平貴與王寶釧_\(2012年電視劇\).md "wikilink") | 薛平貴 / [唐宣宗](../Page/唐宣宗.md "wikilink")                             |                                  |                                    |
| [活佛濟公3](../Page/活佛濟公3.md "wikilink")                  | [濟　公](../Page/濟公.md "wikilink")/闫涛                                 |                                  |                                    |
| [聰明小空空](../Page/聰明小空空.md "wikilink")                  | 張　雁                                                                |                                  |                                    |
| [藍蝶之謎](../Page/藍蝶之謎.md "wikilink")                    | 席子鯤                                                                |                                  |                                    |
| 2013年                                                 | [天天有喜](../Page/天天有喜.md "wikilink")                                 | 劉　楓                              |                                    |
| 2014年                                                 | [長安三怪探](../Page/長安三怪探.md "wikilink")                               | 獨孤仲平                             |                                    |
| [新济公活佛](../Page/新济公活佛.md "wikilink")(上)               | [濟　公](../Page/濟公.md "wikilink")                                    |                                  |                                    |
| [少林寺传奇·藏经阁](../Page/少林寺传奇·藏经阁.md "wikilink")          | 皇　上                                                                | 男二號                              |                                    |
| [欢喜县令](../Page/欢喜县令.md "wikilink")                    | 杜云腾                                                                |                                  |                                    |
| 2015年                                                 | [新济公活佛](../Page/新济公活佛.md "wikilink")(下)                            | [濟　公](../Page/濟公.md "wikilink")  |                                    |
| 2016年                                                 | [天天有喜之人间有爱](../Page/天天有喜之人间有爱.md "wikilink")                       | 刘四喜                              |                                    |
| [刘海戏金蟾](../Page/刘海戏金蟾.md "wikilink")                  | [劉　海](../Page/劉海.md "wikilink")                                    |                                  |                                    |
| [乞丐皇帝与大脚皇后传奇](../Page/乞丐皇帝与大脚皇后传奇.md "wikilink")      | [朱元璋](../Page/朱元璋.md "wikilink")                                   |                                  |                                    |
| [皇子归来之欢喜知府](../Page/欢喜县令.md "wikilink")               | 杜云腾                                                                | 2014年拍攝                          |                                    |
| 2018年                                                 | 封神学院                                                               | 艾總                               | 網絡劇，特別出演                           |
| [吃素的小爸](../Page/吃素的小爸.md "wikilink")                  | 濟爺                                                                 | 網絡劇                              |                                    |
| [防水逆联盟](../Page/防水逆联盟.md "wikilink")                  | 張野                                                                 | 網絡劇，客串                           |                                    |
| [皇甫神医](../Page/皇甫神医.md "wikilink")                    | [皇甫谧](../Page/皇甫谧.md "wikilink")                                   | 2014年拍攝                          |                                    |
| [斗香](../Page/斗香.md "wikilink")                        | 汪大猷                                                                |                                  |                                    |
| 待播                                                    | [私立蜀山学园](../Page/私立蜀山学园.md "wikilink")                             | 鍾舜                               | 網絡劇                                |
| [亮仔的幸福奇缘](../Page/亮仔的幸福奇缘.md "wikilink")              | 赵亮                                                                 | 原名;当幸福来敲门 (2007年拍攝)              |                                    |
| 天龙八音                                                  |                                                                    |                                  |                                    |
|                                                       |                                                                    |                                  |                                    |

### 電視劇（[台灣](../Page/台灣.md "wikilink")）

| 首播年份  | 片名                                             | 角色          | 備註 |
| ----- | ---------------------------------------------- | ----------- | -- |
| 2002年 | [偷偷爱上你](../Page/偷偷爱上你.md "wikilink")           | 趙正磊         | 客串 |
| 2003年 | [再見螢火蟲](../Page/再見螢火蟲_\(台灣電視劇\).md "wikilink") |             | 客串 |
| 2004年 | [升空高飛](../Page/升空高飛.md "wikilink")             | 高振宇（Joseph） |    |
|       |                                                |             |    |

### 電影

|        |                                                       |                |
| ------ | ----------------------------------------------------- | -------------- |
| **年份** | **片名**                                                | **角色**         |
| 1995年  | 《[都市情緣](../Page/都市情緣.md "wikilink")》（叛逆小子）            | 威少(當時以別名陳浩文出演) |
| 1996年  | 《[古惑天堂](../Page/古惑天堂.md "wikilink")》                  |                |
| 2000年  | 《[鬼請你看戲](../Page/鬼請你看戲.md "wikilink")》                |                |
| 2001年  | 《[古或仔之出位](../Page/古或仔之出位.md "wikilink")》              | 綠豆             |
| 2002年  | 《[性Salon狂想曲](../Page/性Salon狂想曲.md "wikilink")》        |                |
| 2003年  | 《[飄忽男女](../Page/飄忽男女.md "wikilink")》（情緣氣水機）           |                |
| 2008年  | 《[小李廣花榮](../Page/小李廣花榮.md "wikilink")》                | 广花荣            |
| 2014年  | 《[冰封：重生之门](../Page/冰封：重生之门.md "wikilink")》            | 客串             |
| 2016年  | 《[龍拳小子](../Page/龍拳小子.md "wikilink")》                  | 林父(客串)         |
| 2016年  | 《[笑林足球](../Page/笑林足球.md "wikilink")》                  |                |
| 2016年  | 《古墓女友》(網絡電影)                                          | 皇帝             |
| 2017年  | 《鬥戰勝佛》(網絡電影)                                          | 孫悟空            |
| 2017年  | 《[借眼](../Page/借眼.md "wikilink")》                      | 高木             |
| 2017年  | 《青天降妖錄》(網絡電影)                                         | 包希仁            |
| 2018年  | 《濟公之英雄歸位》(網絡電影)                                       | 濟公             |
| 2018年  | 《青天降妖錄2》(網絡電影)                                        | 包希仁            |
| 2018年  | 《齐天大圣·万妖之城》(網絡電影)                                     | 孫悟空            |
| 2018年  | 《濟公之神龙再现》(網絡電影)                                       | 濟公             |
| 2018年  | 《冒牌搭檔》                                                | 钱浩南            |
| 2019年  | 《好漢三條半》(網絡電影)                                         |                |
| 2019年  | 《開封降魔記》(網絡電影)                                         | 包拯             |
| 2019年  | 《唐伯虎点秋香2019》(網絡電影)                                    | 唐伯虎            |
| 2019年  | 《濟公之降龍有悔》(網絡電影)                                       | 濟公             |
| 2019年  | 《齊天大聖之火焰山》(網絡電影)                                      | 孫悟空            |
| 待上映    | 《毒网》(原名:[風雨陽光之緝毒先鋒](../Page/風雨陽光之緝毒先鋒.md "wikilink")) | 林戈             |
|        |                                                       |                |

### 綜藝節目（中國大陸）

  - 2014年4月19日 [湖南衛視](../Page/湖南衛視.md "wikilink")
    《[快樂大本營](../Page/快樂大本營.md "wikilink")》
    (與大女兒、楊威父子、劉暢父女、李銳父女)
  - 2015年 《[爲她而戰](../Page/爲她而戰.md "wikilink")》
  - 2016年2月20日
    [湖南衛視](../Page/湖南衛視.md "wikilink")《[快樂大本營](../Page/快樂大本營.md "wikilink")》
    (與[陈学冬](../Page/陈学冬.md "wikilink")、[蒋劲夫](../Page/蒋劲夫.md "wikilink")、[穆婷婷](../Page/穆婷婷.md "wikilink")、程硯秋、[孫驍驍](../Page/孫驍驍.md "wikilink"))
  - 2016年[湖南衛視](../Page/湖南衛視.md "wikilink")《[全員加速中](../Page/全員加速中.md "wikilink")》

### 綜藝節目（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 1995年：《康泰大通咭 美妙新旅程之星馬》主持
  - 1996年：《為食到[關島](../Page/關島.md "wikilink")》 主持
  - 1996年：《為食到[維省](../Page/維省.md "wikilink")》 主持
  - 1996年：《永安旅遊話你知：點解[北京咁好玩](../Page/北京.md "wikilink")》主持
  - 1998年：《永安旅遊話你知：點解[德國咁好玩](../Page/德國.md "wikilink")》主持
  - 1998年：《[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")1998》司儀
  - 1999年：《[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")1999》司儀
  - 2000年：《康泰最緊要好玩之陽光[泰國新境界](../Page/泰國.md "wikilink")》主持
  - 2017年：《[流行經典50年](../Page/流行經典50年.md "wikilink")》 演唱嘉賓

## 音樂作品

### 電視劇歌曲

<table>
<tbody>
<tr class="odd">
<td><p><strong>歌曲</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p>取一念</p></td>
<td><p>劇集《<a href="../Page/西遊記（貳）.md" title="wikilink">西遊記（貳）</a>》主題曲</p></td>
</tr>
<tr class="odd">
<td><p>人龍傳說</p></td>
<td><p>劇集《<a href="../Page/人龍傳說.md" title="wikilink">人龍傳說</a>》主題曲</p></td>
</tr>
<tr class="even">
<td><p>封神</p></td>
<td><p>劇集《<a href="../Page/封神榜_(2001年電視劇).md" title="wikilink">封神榜</a>》主題曲<br />
與<a href="../Page/劉玉翠.md" title="wikilink">劉玉翠合唱</a></p></td>
</tr>
<tr class="odd">
<td><p>先知先覺</p></td>
<td><p>劇集《<a href="../Page/施公奇案_(香港).md" title="wikilink">施公奇案</a>》主題曲</p></td>
</tr>
<tr class="even">
<td><p>敢情線</p></td>
<td><p>劇集《<a href="../Page/智勇新警界.md" title="wikilink">智勇新警界</a>》片尾曲</p></td>
</tr>
</tbody>
</table>

### 唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>唱片公司</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p>《完全自我手冊》（粵語）</p></td>
<td style="text-align: left;"><p>1995年</p></td>
<td style="text-align: left;"><p>星光唱片</p></td>
<td style="text-align: left;"><ol>
<li>完全自我手冊</li>
<li>差不多</li>
<li>相識太遲</li>
<li>完全自我手冊（舞曲混音版）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p>《<a href="../Page/西遊記（貳）.md" title="wikilink">西遊記（貳）主題曲原聲大碟</a>》</p></td>
<td style="text-align: left;"><p>1998年</p></td>
<td style="text-align: left;"><p>環球唱片</p></td>
<td style="text-align: left;"><ol>
<li>取一念（《西遊記II》主題曲）</li>
<li>滅魔大將軍之西征（Mood Music）</li>
<li>有瞭答案（《西遊記II》插曲）（李蕙敏）</li>
<li>悲智雙全（Mood Music）</li>
<li>遇強越強（《西遊記II》插曲）</li>
<li>英雄多情自古空馀恨（Mood Music）</li>
<li>有瞭答案（TV版）（李蕙敏）</li>
<li>坐禪（Mood Music）</li>
<li>遇水化龍（二胡獨奏）</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3</p></td>
<td style="text-align: left;"><p>《<a href="../Page/寵物小精靈.md" title="wikilink">寵物小精靈EP</a>》</p></td>
<td style="text-align: left;"><p>1999年</p></td>
<td style="text-align: left;"><p>環球唱片</p></td>
<td style="text-align: left;"><ol>
<li>宠物小精灵</li>
<li>微笑沉睡比卡超</li>
<li>数数小精灵</li>
<li>宠物小精灵（Karaoke Version）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4</p></td>
<td style="text-align: left;"><p>《電視劇主題曲集》</p></td>
<td style="text-align: left;"><p>1999年8月</p></td>
<td style="text-align: left;"><p>環球唱片</p></td>
<td style="text-align: left;"><ol>
<li>我不放棄</li>
<li>人龍傳說</li>
<li>取一念</li>
<li>遇強越強</li>
<li>寵物小靈精</li>
<li>數數小靈精</li>
<li>敢情線</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>5</p></td>
<td style="text-align: left;"><p>《齊唱千禧喜洋洋》（與<a href="../Page/薛家燕.md" title="wikilink">薛家燕等合唱</a>）</p></td>
<td style="text-align: left;"><p>2000年</p></td>
<td style="text-align: left;"><p>環球唱片</p></td>
<td style="text-align: left;"><ol>
<li>好心有好報（陳浩民與好姨合唱）（國語）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>6</p></td>
<td style="text-align: left;"><p>《A Day With Benny》（粵語）</p></td>
<td style="text-align: left;"><p>2001年9月21日</p></td>
<td style="text-align: left;"><p>環球唱片</p></td>
<td style="text-align: left;"><ol>
<li>封神</li>
<li>三角共震</li>
<li>如果難忍</li>
<li>取一念</li>
<li>遇強越強</li>
<li>人龍傳說</li>
<li>我不放棄</li>
<li>寵物小靈精</li>
<li>數數小靈精</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>7</p></td>
<td style="text-align: left;"><p>《BENNY》</p></td>
<td style="text-align: left;"><p>2002年</p></td>
<td style="text-align: left;"><p>EQ Music（新加坡）</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>8</p></td>
<td style="text-align: left;"><p>《愛海滔滔》（國語）</p></td>
<td style="text-align: left;"><p>2004年12月1日</p></td>
<td style="text-align: left;"><p>新亞洲娛樂</p></td>
<td style="text-align: left;"><ol>
<li>默契</li>
<li>愛海滔滔</li>
<li>默默</li>
<li>相愛太難</li>
<li>愛情傻瓜</li>
<li>愛你真好</li>
<li>高攀不起</li>
<li>相欠</li>
<li>一觸即發</li>
<li>愛海滔滔 （Kala版）</li>
<li>默默 （Kala版）</li>
<li>相愛太難 （Kala版）</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>9</p></td>
<td style="text-align: left;"><p>《男人之痛》（國語）</p></td>
<td style="text-align: left;"><p>2008年3月14日</p></td>
<td style="text-align: left;"><p>新亞洲娛樂</p></td>
<td style="text-align: left;"><ol>
<li>男人之痛</li>
<li>我願為你赴湯蹈火</li>
<li>對不起，請原諒我</li>
<li>完美飛行</li>
<li>夢想的地平線</li>
<li>當愛不在我身邊（陳浩民、馬梓涵）</li>
<li>一萬次為什麼（陳浩民、陳瑀涵）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>10</p></td>
<td style="text-align: left;"><p>《I Am / You Are》（國語）</p></td>
<td style="text-align: left;"><p>2012年4月11日</p></td>
<td style="text-align: left;"><p>艾迪昇傳播事業有限公司</p></td>
<td style="text-align: left;"><ol>
<li>忘不了的人</li>
<li>繼續走</li>
<li>超越世界</li>
<li>第一次</li>
<li>擦心而過</li>
<li>世界小姐</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>11</p></td>
<td style="text-align: left;"><p>《我愛你》（國語）</p></td>
<td style="text-align: left;"><p>2013年6月17日</p></td>
<td style="text-align: left;"><p>艾迪昇傳播事業有限公司</p></td>
<td style="text-align: left;"><ol>
<li>我愛妳</li>
<li>感謝</li>
<li>電影情歌</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>12</p></td>
<td style="text-align: left;"><p>《還是你最好》（國語）</p></td>
<td style="text-align: left;"><p>2014年11月14日</p></td>
<td style="text-align: left;"><p>艾迪昇傳播事業有限公司</p></td>
<td style="text-align: left;"><ol>
<li>還是你最好</li>
<li>起點</li>
</ol></td>
</tr>
</tbody>
</table>

## 廣告

  - [維他奶](../Page/維他奶.md "wikilink")（鄉情篇），憑此廣告獲[亞洲電視](../Page/亞洲電視.md "wikilink")《十大電視廣告》最佳廣告男主角

## 外部連結

  -
  - 經紀公司[艾迪昇傳播事業有限公司](https://www.facebook.com/ai.d.sheng.10/)粉絲專頁

  -
## 爭議

### 陳嘉桓被輕薄事件

發生於2011年11月，當時[陳嘉桓於橫店拍攝電影期間](../Page/陳嘉桓.md "wikilink")，和同劇的男演員到食夜宵時，被飲醉的陳浩民及馬德鐘輕薄，被娛記拍到，引起一番熱議。
事發經過：

2011年11月21日，《東方新地》報導，陳嘉桓於橫店拍攝電影《笑功震武林》，和同劇的[洪天明](../Page/洪天明.md "wikilink")、[陳浩民與](../Page/陳浩民.md "wikilink")[馬德鐘到燒烤店吃串燒](../Page/馬德鐘.md "wikilink")。當時三名男士已喝得很醉，[洪天明更醉得在燒烤店睡着了](../Page/洪天明.md "wikilink")，[陳浩民與](../Page/陳浩民.md "wikilink")[馬德鐘則醉到要鬧着玩](../Page/馬德鐘.md "wikilink")，據知[馬德鐘先攬錫Rose](../Page/馬德鐘.md "wikilink")，[陳浩民則欲咀Rose面及嘴](../Page/陳浩民.md "wikilink")，嚇得Rose「魂飛魄散」。

### 陳浩民「狼吻門」風波

據悉，2011年陳浩民曾經酒後失態，對女星陳嘉桓又抱又親，更遭阿嬌指「性騷擾不是第一次」，形象跌至谷底。事後陳浩民在記者會落淚道歉兼捐錢平息事件。自此之後，陳浩民經常以好爸爸及好老公形象出現，洗去「賤狼」形象。然而日前陳浩民在橫店拍攝新戲，再度被拍到與女星親密互動，牽手餵食，遭網民炮轟「泡女本性難移」。

據台灣媒體報導，香港一線小生陳浩民2011年因涉入「狼吻門」事件，形象大傷，好不容易才靠著與愛女參加《爸爸去哪兒2》重拾愛家好爸爸形象之際，卻又被媒體爆出趁著拍攝電視劇《天天有喜之人間有愛》空檔，夜會長發美女，甚至上演親密餵食秀。
日前陳浩民在橫店拍新劇續集，被拍到與女同情異常親密。事緣深夜時分，他與劇組兩位女同事外出食飯，其間大獻殷勤，坐在浩民對面的女同事成為當晚的「目標人物」，陳浩民幫對方看掌，又餵食，走的時候更牽著對方，女方亦相當受落。

## 參考

[ho](../Category/陳姓.md "wikilink")
[Category:福州人](../Category/福州人.md "wikilink")
[Category:香港福建人](../Category/香港福建人.md "wikilink")
[Category:20世紀演員](../Category/20世紀演員.md "wikilink")
[Category:21世紀男演員](../Category/21世紀男演員.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:粵語流行音樂歌手](../Category/粵語流行音樂歌手.md "wikilink")
[Category:香港電視男演員](../Category/香港電視男演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:高主教書院校友](../Category/高主教書院校友.md "wikilink")
[Category:嶺南大學校友 (香港)](../Category/嶺南大學校友_\(香港\).md "wikilink")