**陈洁如**（），幼名**陈凤**，又名**陈璐**，[上海人](../Page/上海.md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[寧波](../Page/寧波.md "wikilink")。[美国](../Page/美国.md "wikilink")[哥伦比亚大学](../Page/哥伦比亚大学.md "wikilink")[教育](../Page/教育.md "wikilink")[硕士](../Page/硕士.md "wikilink")，是[国民党](../Page/国民党.md "wikilink")[总裁](../Page/总裁.md "wikilink")[蒋介石的](../Page/蒋介石.md "wikilink")[前妻之一](../Page/前妻.md "wikilink")，[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，曾任上海市[卢湾区](../Page/卢湾区.md "wikilink")[政协委员](../Page/政协委员.md "wikilink")，晚年定居[香港](../Page/香港.md "wikilink")。\[1\]

蒋原本表示陈为其[侧室](../Page/侧室.md "wikilink")，而后蒋与[宋美龄结婚](../Page/宋美龄.md "wikilink")，又改口表示与陈洁如本无婚约，两人正式脱离关係。\[2\]

## 簡介

陳潔如祖籍[宁波](../Page/宁波.md "wikilink")[镇海](../Page/镇海区_\(宁波市\).md "wikilink")，出生于[上海](../Page/上海.md "wikilink")，家境宽裕，她的父亲陈学方是经营纸业的商人，母亲是[苏州人](../Page/苏州.md "wikilink")。

在12岁的时候进入[蔡元培创办的](../Page/蔡元培.md "wikilink")[上海](../Page/上海.md "wikilink")[爱国女子中学就读](../Page/爱国女子中学.md "wikilink")，在那里与巨商[张静江的几个女儿成为好友](../Page/张静江.md "wikilink")，并经常去张家做客。1919年时，陈洁如在张家做客时，偶然见到了来访的[孙中山和蒋介石](../Page/孙中山.md "wikilink")，蒋对她一见锺情，并展开热烈追求，\[3\]陈洁如不知所措，但后来对蒋逐渐有了好感。陈洁如的母亲在请人调查后却发现蔣中正已经有一妻一妾，并且没有正当的职业，于是拒绝了蒋的求婚。[蔣中正则透过张静江和孙中山的关系](../Page/蔣中正.md "wikilink")，继续向陈母表示有意明媒正娶陈洁如，并最终得到陈母的首肯。蒋向陈洁如声称自己已经与原配及侍妾脱离关系，并许诺“你将是我独一无二的合法妻子”。

陈洁如声称1921年12月5日与蒋介石结婚，但蒋介石1927年9月27日接受《[纽约时报](../Page/纽约时报.md "wikilink")》采访时称陈只是[妾](../Page/妾.md "wikilink")。1921年，在上海，蔣經國受到蔣中正下屬[陳果夫照顧](../Page/陳果夫.md "wikilink")，蔣中正側室陳潔如也一同前往上海，並成為蔣經國繼母。陈曾提出结婚证上的字样，但没有给出影印本。陈洁如回忆录中的照片也没有和婚礼有关的。蒋介石1921-1922年的日记多次晚上提到去陈洁如处，但没有说起结婚。\[4\]1927年9月28、29、30日，在上海《申報》，連續三天刊登了題為「蔣中正啟事」的單身申明，稱：「毛氏髮妻，早經仳離；姚陳二妾，本無契約。」\[5\]

[國民革命軍北伐](../Page/國民革命軍北伐.md "wikilink")，蔣介石攻克[南昌之后](../Page/南昌.md "wikilink")，[鲍罗廷](../Page/鲍罗廷.md "wikilink")、[邓演达等人控制](../Page/邓演达.md "wikilink")[武漢國民政府](../Page/武漢國民政府.md "wikilink")，欲剥夺蔣中正的权力。蔣中正为继续北伐而急于得到上海银行团的支持，极有影响的[宋氏家族为此开出的条件是蔣中正需与宋美龄结婚](../Page/宋氏家族.md "wikilink")，并任命[宋子文为](../Page/宋子文.md "wikilink")[财政部长](../Page/财政部长.md "wikilink")。蔣中正遂请求陈洁如暂时离开[中国](../Page/中国.md "wikilink")5年，待[北伐成功](../Page/北伐.md "wikilink")，[中国統一之后即刻接她回来](../Page/中国統一.md "wikilink")。

1927年8月陈洁如离开上海前往[美国](../Page/美国.md "wikilink")，\[6\]1933年返回上海。\[7\]留美5年多，并从[哥伦比亚大学教育学院获得](../Page/哥伦比亚大学.md "wikilink")[硕士学位](../Page/硕士.md "wikilink")。
1933年，陈洁如回到上海。养女[陳瑤光](../Page/陳瑤光.md "wikilink")，在蒋陈离异后改从母姓，与陈洁如生活在一起。1949年[国民党败退](../Page/国民党.md "wikilink")[台湾](../Page/台湾.md "wikilink")，陈洁如在与[共产党有联系的女婿](../Page/共产党.md "wikilink")[陆久之的帮助下](../Page/陆久之.md "wikilink")，决意留在上海，被邀为上海市[卢湾区](../Page/卢湾区.md "wikilink")[政协委员](../Page/政协委员.md "wikilink")。

1961年陈洁如离开上海，定居[香港](../Page/香港.md "wikilink")，改名“陈璐”，住在[铜锣湾百德新街](../Page/铜锣湾.md "wikilink")。小时曾得其照料的[蒋经国出面为她在港购买了住房](../Page/蒋经国.md "wikilink")，并给予了经济上的帮助（蒋氏通过[戴季陶之子](../Page/戴季陶.md "wikilink")[戴安国](../Page/戴安国.md "wikilink")，每月接济她500[美元](../Page/美元.md "wikilink")）。1962年，蔣中正曾托人给她带信，信中说“往昔风雨同舟的日子裡，所受照扶，未曾须臾去怀……”。\[8\]陈洁如后来有一信给蒋，其中有：“三十多年来，我的委屈唯君知之”等语，\[9\]颇为哀婉。

陳潔如在香港期间写有回忆录，《[陈洁如回忆录](../Page/陈洁如回忆录.md "wikilink")》详述了她与蔣中正的爱情悲剧。[国民党为阻止其出版](../Page/国民党.md "wikilink")，使某出版商将书稿高价买下。这本书直至蒋氏父子去世后才得以出版。后来公开发表的回忆录，系用英文写成，是李荫生、李时敏两兄弟与陈洁如共同完成的。陈洁如提供口述、日记和中文的自传手稿，李荫生主要执笔，李时敏负责编制人名与地名的注释表。

1971年1月21日陳潔如在[香港去世](../Page/香港.md "wikilink")。[周恩来得悉陈在香港病逝的消息后](../Page/周恩来.md "wikilink")，亲自批准陈洁如的养女[陈瑶光赴港奔丧](../Page/陈瑶光.md "wikilink")，料理后事。

2002年秋，陈瑶光迁徙母亲的棂榇安葬在上海福寿园。

## 著述

  - 《[陳潔如回憶錄](../Page/陳潔如回憶錄.md "wikilink")》

## 相关人物

## 参考文献

## 外部链接

  - [人物：红颜命薄--做了七年蒋介石夫人的陈洁如](http://news.sina.com.cn/cul/2004-11-01/57.html)
  - [蔣介石與陳潔如:風雨情 同舟七年 見美玲
    一朝拋卻](https://web.archive.org/web/20080326214344/http://big5.ce.cn/culture/history/200605/05/t20060505_6882787.shtml)
  - [蔣介石情史解密　陳潔如、宋美齡較具功能性](http://www.ettoday.com/2005/04/18/91-1779304.htm)
  - [我與蔣介石--（陳潔如回憶錄節錄）](http://tw.myblog.yahoo.com/jw!0MHaLBiGER8APa4dyls-/article?mid=1877&prev=1886&next=1870&l=d&fid=32)

{{-}}

[Category:蒋家第一代](../Category/蒋家第一代.md "wikilink")
[Category:中华民国大陆时期女性人物](../Category/中华民国大陆时期女性人物.md "wikilink")
[Category:市辖区政协委员](../Category/市辖区政协委员.md "wikilink")
[Category:女性地方政协委员](../Category/女性地方政协委员.md "wikilink")
[Category:移民香港的大陆人](../Category/移民香港的大陆人.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:宁波裔上海人](../Category/宁波裔上海人.md "wikilink")
[Category:香港宁波人](../Category/香港宁波人.md "wikilink")
[Category:镇海县人](../Category/镇海县人.md "wikilink")
[J洁如](../Category/陳姓.md "wikilink")

1.  [《人物：红颜命薄--做了七年蒋介石夫人的陈洁如(图)》](http://news.sina.com.cn/cul/2004-11-01/57.html)，摘自赵宏著《蒋介石家族的女人们》，news.sina.com.cn转载。

2.

3.
4.  陶涵（Taylor）:《蒋介石与现代中国》，林添贵译，中信出版社2012年，228页。

5.  \[ <http://www.timetw.com/9984.html《揭秘:宋美齡為何要放棄初戀最終嫁給蔣介石>？》

6.  [《人物风流：做了七年蒋介石夫人的陈洁如(图)(2)》](http://news.sina.com.cn/cul/2004-11-01/58.html)。

7.  [《人物风流：做了七年蒋介石夫人的陈洁如(图)(3)》](http://news.sina.com.cn/cul/2004-11-01/59.html)。

8.
9.