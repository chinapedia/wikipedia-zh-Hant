**陳玉樹**教授（，），前任[嶺南大學校長](../Page/嶺南大學_\(香港\).md "wikilink")，[香港](../Page/香港.md "wikilink")[教育家](../Page/教育家.md "wikilink")，曾就讀於[何明華會督銀禧中學及](../Page/何明華會督銀禧中學.md "wikilink")[香港中文大學](../Page/香港中文大學.md "wikilink")[崇基學院](../Page/崇基學院.md "wikilink")（修讀工商管理），其後往[美國](../Page/美國.md "wikilink")[加州柏克萊大學深造](../Page/加州柏克萊大學.md "wikilink")，先後獲頒發工商管理碩士、經濟學文學碩士及經濟財務學哲學博士學位。他曾任教於[香港中文大學](../Page/香港中文大學.md "wikilink")、美國[西北大學及](../Page/西北大學.md "wikilink")[南加州大學](../Page/南加州大學.md "wikilink")。
在回港之前，陳教授於南加州大學擔任Justin Dart財務學講座教授一職。

他於1990年加入[香港科技大學](../Page/香港科技大學.md "wikilink")，出任當時的財務及經濟學系副主任及財務學教授，1993年任商學院創院院長，2001年獲委任為學術副校長，2007年出任[嶺南大學校長](../Page/嶺南大學_\(香港\).md "wikilink")，在2012年10月15日以健康為由（患上肺癌）辭職。

他於科技大學任職時創立了[香港科技大學](../Page/香港科技大學.md "wikilink")[工商管理學院](../Page/香港科技大學工商管理學院.md "wikilink")，促使科技大學成為海內外知名學府之一。

他曾任學術期刊「銀行與金融」（Journal of Banking and Finance）及「金融中介」（Journal of
Financial Intermediation）副編輯，並参與業內協會及學會。

他於2006年獲[香港公開大學頒授榮譽院士](../Page/香港公開大學.md "wikilink")，並於2012年獲[香港科技大學頒授榮譽法學博士](../Page/香港科技大學.md "wikilink")。2015年6月，獲政府委任為「離職公務員就業申請諮詢委員會」主席，負責處理離職公務員再就業的職務，但他於2016年6月辭任\[1\]。

## 工作經驗

  - 1977至1978年：香港中文大學財務學系助理講師
  - 1982年至1986年：美國西北大學Kellogg管理學院助理教授、副教授
  - 1986年至1990年：美國南加州大學財務學副教授
  - 1990至1993年：美國南加州大學Justin Dart財務學講座教授
  - 1991至1993年：香港科技大學商學院財務及經濟學系副主任及教授、學院教研主任
  - 1993至2000年：香港科技大學商學院創院院長\[2\]
  - 2001年至2007年：香港科技大學學術副校長暨財務學講座教授\[3\]
  - 2007年至2012年：嶺南大學校長、財務學系講座教授\[4\]

## 榮譽

  - 2012年11月16日：獲香港科技大學頒授榮譽法學博士學位\[5\]。

## 注释

<references />

[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:香港嶺南大學校長](../Category/香港嶺南大學校長.md "wikilink")
[Category:香港科技大學榮譽博士](../Category/香港科技大學榮譽博士.md "wikilink")
[Category:香港经济学家](../Category/香港经济学家.md "wikilink")
[Y](../Category/陳姓.md "wikilink")
[Category:香港嶺南大學教授](../Category/香港嶺南大學教授.md "wikilink")
[Category:何明華會督銀禧中學校友](../Category/何明華會督銀禧中學校友.md "wikilink")
[Category:罹患肺癌逝世者](../Category/罹患肺癌逝世者.md "wikilink")
[Category:香港科技大學教授](../Category/香港科技大學教授.md "wikilink")

1.  《也說亮話：悼陳玉樹教授》http://hk.apple.nextmedia.com/financeestate/art/20170605/20044353
2.  《智慧箴言 專訪前院長》http://www.bm.ust.hk/zh-hk/feature_stories/detail/74
3.  《謙謙玉樹
    臨風揮手－－－科大送別創校功臣陳玉樹教授》http://newsletter.ust.hk/07/summer/cn/24/index.html
4.  《悼陳玉樹教授》https://www.ln.edu.hk/cht/lingnan-touch/67/in-memory-of-prof-chan-yuk-shee
5.  《陳玉樹校長獲香港科技大學頒授榮譽博士學位》http://webapp.ln.edu.hk/sys/ocpa/corp-enews/news/陳玉樹校長獲香港科技大學頒授榮譽博士學位/441/chs