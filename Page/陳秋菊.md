**陳秋菊**（)，字**尚志**，[臺灣臺北文山堡烏月庄](../Page/臺灣.md "wikilink")（今[新北市](../Page/新北市.md "wikilink")[深坑區](../Page/深坑區.md "wikilink")）人，原籍福建，為[清治時期](../Page/清治時期.md "wikilink")[林朝棟所屬](../Page/林朝棟.md "wikilink")[棟軍的主要將領](../Page/棟軍.md "wikilink")，官階為[參將](../Page/參將.md "wikilink")(1892年胡傳檢閱時)，主持棟字隘勇左營\[1\]。陳秋菊是台灣抗日史上的一位傳奇人物，有[白馬將軍的稱號](../Page/白馬將軍.md "wikilink")\[2\]。

## 生平

陳秋菊，臺北深坑人，家庭是茶農出身，陳秋菊生於[清咸豐年間的](../Page/大清帝國.md "wikilink")[淡水廳](../Page/淡水廳.md "wikilink")[拳山堡深坑仔莊](../Page/拳山堡.md "wikilink")（日治時期為[臺北廳深坑支廳](../Page/臺北廳.md "wikilink")[文山堡烏月庄](../Page/拳山堡.md "wikilink")）。自幼頗知詩書，長大後，曾擔任深坑庄總理。

[清法戰爭](../Page/清法戰爭.md "wikilink")([西仔反](../Page/西仔反.md "wikilink"))時，曾奉命招募團練[義勇五百人防守](../Page/鄉勇.md "wikilink")[基隆](../Page/基隆.md "wikilink")，並於基隆附近邀擊法軍有功，翌年獲清廷以軍功任命為四品[武官](../Page/武官.md "wikilink")，頂戴雙花藍翎，加賜[都司](../Page/都司.md "wikilink")。

1894年，奉巡撫[唐景崧之令率義勇以備邊警](../Page/唐景崧.md "wikilink")。

[乙未戰爭之時](../Page/乙未戰爭.md "wikilink")，因[守備](../Page/守備.md "wikilink")[黃宗河內渡](../Page/黃宗河.md "wikilink")，臺民憤激，陳秋菊兼領其軍，率領棟字隘勇左營及前營並兼有十數營[鄉勇](../Page/鄉勇.md "wikilink")，共約六千多人兵力（參見[棟軍](../Page/棟軍.md "wikilink")），紛起義以武力抗禦日本殖民者。[日本佔領](../Page/日本.md "wikilink")[台南後仍在](../Page/臺南市.md "wikilink")[深坑一帶抗日](../Page/深坑區.md "wikilink")，並曾二次圍攻[臺北城](../Page/臺北城.md "wikilink")。

1896年（[明治](../Page/明治.md "wikilink")29年）元旦，當[日本人高興慶祝佔有全台時](../Page/日本.md "wikilink")，陳秋菊與[胡阿錦](../Page/胡阿錦.md "wikilink")、[簡大獅等圍攻](../Page/簡大獅.md "wikilink")[臺北城](../Page/臺北城.md "wikilink")，一度佔領[大稻埕](../Page/大稻埕.md "wikilink")。當時，陳秋菊騎著一匹白馬率眾領導攻城，因此又被稱為「白馬將軍」。

1897年8月，日本新任臺灣總督[兒玉源太郎實施](../Page/兒玉源太郎.md "wikilink")[保甲制度及](../Page/保甲制度.md "wikilink")「土匪招降對策」，[日本人以](../Page/日本人.md "wikilink")[招撫方式使其投降](../Page/招撫.md "wikilink")，應允給予陳秋菊經營[樟腦的開採與製作權](../Page/樟腦.md "wikilink")，陳秋菊在交換條件下，率一千三百餘名抗日義軍下山「歸順」，其中有部份義軍慘遭日軍殺戮。陳秋菊此後勤儉治產，因而致富。

1907年5月（明治四十年），陳秋菊獲得台灣總督府所頒授的佩紳章。1922年8月22日，抑鬱而終\[3\]。

## 身後

陳秋菊古厝(今[新北市](../Page/新北市.md "wikilink")[深坑區旺耽路](../Page/深坑區.md "wikilink")39巷12號)

## 參考資料

[C陳](../Category/台灣清治時期人物.md "wikilink")
[C陳](../Category/台灣日治時期人物.md "wikilink")
[Category:乙未戰爭抗日人物](../Category/乙未戰爭抗日人物.md "wikilink")
[Category:陳姓](../Category/陳姓.md "wikilink")
[Category:台灣紳章附與人物](../Category/台灣紳章附與人物.md "wikilink")

1.  [深坑石碇與陳秋菊 文史工作者
    王受寧](http://wenfeng-web.myweb.hinet.net/new_page_3-1.htm)
2.  [白馬將軍陳秋菊評傳(一)∼先世及其崛起](http://www.tnu.edu.tw/ge/taiwanmap/%E6%9C%9F%E5%88%8A%E8%AB%96%E6%96%87/%E7%99%BD%E9%A6%AC%E5%B0%87%E8%BB%8D%E9%99%B3%E7%A7%8B%E8%8F%8A%E8%A9%95%E5%82%B3_%E4%B8%80_~%E5%85%88%E4%B8%96%E5%8F%8A%E5%85%B6%E5%B4%9B%E8%B5%B7.pdf)
    ，[東南技術學院東南學報第二十七期](../Page/東南技術學院.md "wikilink")，中華民國九十三年十二月出版，第369-382頁
3.  [陳秋菊1854？─1922](http://memory.ncl.edu.tw/tm_cgi/hypage.cgi?HYPAGE=toolbox_figure_detail.hpg&xml_id=0000295446)