**陳華宗**（），[台灣](../Page/台灣.md "wikilink")[台南縣](../Page/台南縣.md "wikilink")[學甲鎮](../Page/學甲區.md "wikilink")（今[臺南市學甲區](../Page/臺南市.md "wikilink")）人，[日治時期擔任學甲庄長](../Page/台灣日治時期.md "wikilink")，戰後曾任台南縣議會議長，[台灣省議會議員等職](../Page/台灣省議會.md "wikilink")。

## 早年生平

陳華宗在[明治三十七年](../Page/明治.md "wikilink")（1903年）3月10日出生於[學甲](../Page/學甲.md "wikilink")[中洲](../Page/中洲.md "wikilink")\[1\]，為當地望族子弟。1917年時離台前往東京求學，先後畢業於[東京豐山中學](../Page/東京.md "wikilink")、[立正大學史學科](../Page/立正大學.md "wikilink")。

之後陳華宗在立正大學史學研究室擔任副手，並曾在神奈川縣的中原高等女學校任職，1932年辭職回到台灣。

## 政途

1934年1月時，陳華宗出任中洲信用組合理事，並在同年10月受任命為學甲庄協議會員\[2\]。

[昭和十年](../Page/昭和.md "wikilink")（1935年）5月，擔任[學甲庄長](../Page/學甲庄.md "wikilink")，六年任期之間，力排眾議，推行學甲[都市計劃與](../Page/都市計劃.md "wikilink")[土地區劃整理工作](../Page/土地區劃.md "wikilink")，為學甲市鎮發展奠立基礎。任滿後，轉任[台南市](../Page/台南市.md "wikilink")[長榮中學歷史教師](../Page/長榮中學.md "wikilink")。

1941年起，陳華宗出任[嘉南大圳](../Page/嘉南大圳.md "wikilink")[水利組合代表](../Page/水利組合.md "wikilink")，戰後又任[嘉南農田水利會接收委員](../Page/嘉南農田水利會.md "wikilink")、經理課長等職；1950年當選水利會副主任委員，1956年當選評議委員會常務委員，1959年當選會長。任內主張興建[白河水庫](../Page/白河水庫.md "wikilink")、[曾文水庫](../Page/曾文水庫.md "wikilink")，獲政府採納施行，廣建灌溉渠道，開挖[雲林地區地下水井](../Page/雲林.md "wikilink")，以利農業。更因負責[八七水災後重建工作](../Page/八七水災.md "wikilink")，獲頒七等[景星勳章](../Page/景星勳章.md "wikilink")。

1946年獲選為台南縣參議會參議員，並擔任議長。1947年[二二八事件爆發](../Page/二二八事件.md "wikilink")，任[二二八事件處理委員會臺南縣分會主任委員](../Page/二二八事件處理委員會列表.md "wikilink")，勸導人民協助國軍清勦奸暴，並勸令附和盲從者，迅卽自首自新\[3\]。陳華宗與參議員[吳新榮等人](../Page/吳新榮.md "wikilink")，被指為[內亂罪](../Page/內亂.md "wikilink")，遭受逮捕，拘禁於台北看守所，判處[死刑](../Page/死刑.md "wikilink")，而後又以無罪釋放\[4\]。

陳氏連任四屆議長，任期間與[吳三連](../Page/吳三連.md "wikilink")、[高文瑞](../Page/高文瑞.md "wikilink")、[劉博文](../Page/劉博文.md "wikilink")、[陳天賜合稱台南縣議會五虎將](../Page/陳天賜.md "wikilink")。在[1951年至](../Page/1951年.md "wikilink")[1957年間](../Page/1957年.md "wikilink")，吳三連當選省參議員，而後又出任臺北市長；高文瑞當選第一、第二屆縣長；陳華宗擔任議長，三人都出身北門地區，成為「北門派」領袖。而北門派也成為後來「海派」的開端\[5\]。

1964年當選台灣省議會議員，1968年獲得近百分之百的選票連任\[6\]，任內主張開發[北門鄉](../Page/北門區.md "wikilink")[蘆竹溝為綜合商港](../Page/蘆竹溝.md "wikilink")。

1968年11月10日凌晨六時10分，陳華宗在前往[台北省議會招待所途中](../Page/台北.md "wikilink")，在[博愛路與](../Page/博愛路.md "wikilink")[貴陽街口的與貨車相撞](../Page/貴陽街.md "wikilink")，送醫後不治死亡\[7\]\[8\]。

## 紀念

陳華宗由於作風清廉，深受學甲百姓感念，於當地被視為政治人物典範，稱為「華宗精神」。生前，1955年學甲鎮民代表會通過以「華宗橋」命名[將軍溪聯外橋樑](../Page/將軍溪.md "wikilink")，以資彰表陳氏對學甲貢獻，並開始籌建銅像與公園。陳華宗逝世後，1969年，銅像與紀念公園正式落成，而後又遷至「華宗紀念公園」現址。當地並以「華宗路」命名[台19線](../Page/台19線.md "wikilink")[省道學甲段](../Page/省道.md "wikilink")。1978年起，學甲鎮公所開始舉辦[華宗杯排球錦標賽](../Page/華宗杯排球錦標賽.md "wikilink")，為全國重要排球比賽之一。

## 參考文獻

## 外部連結

  - [陳華宗 -
    臺南市政府文化局-臺南文史-史蹟源流](http://culture.tainan.gov.tw/residence/index-1.php?m2=81&id=142)

[Category:第4屆臺灣省議員](../Category/第4屆臺灣省議員.md "wikilink")
[Category:第3屆臺灣省議員](../Category/第3屆臺灣省議員.md "wikilink")
[Category:臺南縣議長](../Category/臺南縣議長.md "wikilink")
[Category:第4屆臺南縣議員](../Category/第4屆臺南縣議員.md "wikilink")
[Category:第3屆臺南縣議員](../Category/第3屆臺南縣議員.md "wikilink")
[Category:第2屆臺南縣議員](../Category/第2屆臺南縣議員.md "wikilink")
[Category:第1屆臺南縣議員](../Category/第1屆臺南縣議員.md "wikilink")
[Category:臺南縣參議員](../Category/臺南縣參議員.md "wikilink")
[Category:台灣日治時期政治人物](../Category/台灣日治時期政治人物.md "wikilink")
[Category:臺灣高級中學教師](../Category/臺灣高級中學教師.md "wikilink")
[Category:立正大學校友](../Category/立正大學校友.md "wikilink")
[Category:學甲人](../Category/學甲人.md "wikilink")
[Hua華](../Category/陳姓.md "wikilink")
[Category:臺灣車禍身亡者](../Category/臺灣車禍身亡者.md "wikilink")

1.

2.
3.  二二八事件臺灣本地新聞史料彙編(第三冊)（《中華日報》1947.4.10:3，二欄題） p 1492

4.

5.
6.
7.

8.