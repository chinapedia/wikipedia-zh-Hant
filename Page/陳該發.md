**陳該發**（1968年11月9日－），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[三商虎](../Page/三商虎.md "wikilink")、[第一金剛及](../Page/第一金剛.md "wikilink")[La
New熊隊](../Page/La_New熊.md "wikilink")，守備位置為[右外野手](../Page/右外野手.md "wikilink")。

**陳該發**為[台灣本土第一位達成](../Page/台灣.md "wikilink")[完全打擊的](../Page/完全打擊.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手](../Page/選手.md "wikilink")，也是唯一在兩聯盟皆打出[完全打擊的球員](../Page/完全打擊.md "wikilink")。

## 經歷

  - [台南市立人國小少棒隊](../Page/台南市.md "wikilink")
  - [臺北縣板橋國中青少棒隊](../Page/新北市.md "wikilink")（榮工）
  - [台南市南英商工青棒隊](../Page/台南市.md "wikilink")
  - 陸光棒球隊
  - [合作金庫棒球隊](../Page/合作金庫.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[三商虎](../Page/三商虎.md "wikilink")（1996年－1999年）
  - [台灣大聯盟](../Page/台灣大聯盟.md "wikilink")[嘉南勇士](../Page/嘉南勇士.md "wikilink")（2000年－2002年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[第一金剛](../Page/第一金剛.md "wikilink")（2003年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[La
    New熊](../Page/La_New熊.md "wikilink")（2004年－2005年）

:\*[中華職棒](../Page/中華職棒.md "wikilink")[La
New熊](../Page/La_New熊.md "wikilink")[二軍打擊教練](../Page/La_New二軍.md "wikilink")（2006年～2009年2月）

:\*[中華職棒](../Page/中華職棒.md "wikilink")[La
New熊打擊教練](../Page/La_New熊.md "wikilink")（2009年3月～2011年1月5日）

:\*[中華職棒](../Page/中華職棒.md "wikilink")[Lamigo桃猿](../Page/Lamigo桃猿.md "wikilink")[二軍打擊教練](../Page/Lamigo二軍.md "wikilink")（2011年1月6日～2012年5月30日）

:\*[中華職棒](../Page/中華職棒.md "wikilink")[Lamigo桃猿球探](../Page/Lamigo桃猿.md "wikilink")（2011年1月6日～2013年）

:\*[中華職棒](../Page/中華職棒.md "wikilink")[Lamigo桃猿打擊教練](../Page/Lamigo桃猿.md "wikilink")（2012年5月31日～2015年6月3日）

:\*[中華職棒](../Page/中華職棒.md "wikilink")[Lamigo桃猿](../Page/Lamigo桃猿.md "wikilink")[二軍打擊教練](../Page/Lamigo二軍.md "wikilink")（2015年6月4日～12月10日）

:\*[中華職棒](../Page/中華職棒.md "wikilink")[義大犀牛打擊教練](../Page/義大犀牛.md "wikilink")（2015年12月14日-2016年10月31日）

:\*[中華職棒](../Page/中華職棒.md "wikilink")[富邦悍將打擊教練](../Page/富邦悍將.md "wikilink")（2016年11月1日-）

## 特別記錄

1997年，在中華職棒三商虎，成為中職史上首位完全打擊的本土打者。

2001年，在台灣大聯盟嘉南勇士隊，達成完全打擊，為台灣大聯盟首位達成完全打擊的選手。

## 職棒生涯成績

  - **粗體字**為[全聯盟當年最佳或最高成績](../Page/中華職棒.md "wikilink")

| 年度    | 球隊                                       | 出賽  | 打數   | 安打  | 全壘打 | 打點  | 盜壘 | 四死  | 三振      | 壘打數 | 雙殺打 | 打擊率   |
| ----- | ---------------------------------------- | --- | ---- | --- | --- | --- | -- | --- | ------- | --- | --- | ----- |
| 1996年 | [三商虎](../Page/三商虎.md "wikilink")         | 97  | 377  | 98  | 9   | 32  | 9  | 29  | 73      | 150 | 6   | 0.260 |
| 1997年 | [三商虎](../Page/三商虎.md "wikilink")         | 91  | 355  | 89  | 8   | 41  | 10 | 30  | 92      | 136 | 9   | 0.251 |
| 1998年 | [三商虎](../Page/三商虎.md "wikilink")         | 103 | 392  | 102 | 16  | 56  | 11 | 32  | **103** | 177 | 11  | 0.260 |
| 1999年 | [三商虎](../Page/三商虎.md "wikilink")         | 86  | 310  | 98  | 9   | 35  | 8  | 45  | 59      | 152 | 1   | 0.316 |
| 2003年 | [第一金剛](../Page/第一金剛.md "wikilink")       | 78  | 248  | 57  | 5   | 26  | 3  | 17  | 57      | 81  | 8   | 0.230 |
| 2004年 | [La New熊](../Page/La_New熊.md "wikilink") | 54  | 153  | 33  | 2   | 10  | 1  | 7   | 36      | 43  | 4   | 0.216 |
| 合計    | 6年                                       | 509 | 1835 | 477 | 49  | 200 | 42 | 160 | 420     | 739 | 39  | 0.260 |

## 外部連結

[C](../Category/1968年出生.md "wikilink")
[C](../Category/在世人物.md "wikilink")
[C](../Category/台灣棒球選手.md "wikilink")
[C](../Category/中華成棒隊球員.md "wikilink")
[C](../Category/三商虎隊球員.md "wikilink")
[C](../Category/第一金剛隊球員.md "wikilink")
[C](../Category/嘉南勇士隊球員.md "wikilink")
[C](../Category/Lamigo桃猿隊球員.md "wikilink")
[C](../Category/中華職棒單月最有價值球員獎得主.md "wikilink")
[C](../Category/南英高級商工職業學校校友.md "wikilink")
[C](../Category/陳姓.md "wikilink")