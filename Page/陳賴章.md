**陳賴章墾號**是指[閩南](../Page/閩南.md "wikilink")[泉州人](../Page/泉州.md "wikilink")[陳逢春](../Page/陳逢春.md "wikilink")、[賴永和](../Page/賴永和.md "wikilink")、[陳天章](../Page/陳天章.md "wikilink")、[陳憲伯](../Page/陳憲伯.md "wikilink")、[戴天樞等於](../Page/戴天樞.md "wikilink")1709年（[清](../Page/清.md "wikilink")[康熙](../Page/康熙.md "wikilink")48年）為了合股開墾[大佳臘](../Page/大加蚋.md "wikilink")（或作大加蚋，今[大台北地區](../Page/台北.md "wikilink")）而成立的團體，**陳賴章**為該綜合合股人姓名的該團體名稱，並非人名。而此類型以土地開發開墾為主的合股團體，一般來說都稱為「[墾號](../Page/墾號.md "wikilink")」。

「陳賴章墾號」不但是18世紀初台灣最大規模的[漢人](../Page/漢人.md "wikilink")[墾號](../Page/墾號.md "wikilink")，也促成了日後的台北北部地區的[平埔族遷移](../Page/平埔族.md "wikilink")、[同化與滅絕](../Page/同化.md "wikilink")。

## 墾號

18世紀初，[台灣清治時期的可供開墾的土地以權利分](../Page/台灣清治時期.md "wikilink")，約略可以分成「無主地」（荒地）和「番地」（大都為[平埔族](../Page/平埔族.md "wikilink")[原住民個人擁有的土地](../Page/原住民.md "wikilink")）。只要是非[平埔族個人擁有的土地](../Page/平埔族.md "wikilink")，一律稱「無主地」，地權皆屬統治台灣的清廷官方所有。台灣漢人或移民若想於台灣開墾土地，皆須按照既定程序向台灣清朝官府（縣衙）申請土地「開墾權」。

該申請首先必須記載開墾範圍，地界。申請後，為避免糾紛或侵犯平埔族土地，官府會約略審查並派人會勘，並經數個月公告後，如無人提出異議，才發給「墾照」。而獲得該墾照的原申請人，稱為墾主。（如陳賴章墾號的墾主即為陳天章、陳逢春、賴永和、陳憲伯、戴天樞等五人）。之後墾主即招佃農從事實際拓墾工作。假使能於[秋穫等規定期間內開墾成功](../Page/秋穫.md "wikilink")，向縣衙納糧完稅，則可取得該墾地所有權，成為業戶或業主。

## 陳賴章墾號

「陳賴章墾號」乍看之下為人名，但實際上並非如此，而是由陳逢春、賴永和、陳天章、戴天樞、陳憲伯等五人合股組成的開墾組織。其體系下的泉州人，大略包含了俗稱[泉州三邑與泉州](../Page/泉州三邑.md "wikilink")[同安人](../Page/同安.md "wikilink")。

本先客居[台灣](../Page/台灣.md "wikilink")[嘉義的這些](../Page/嘉義縣_\(清朝\).md "wikilink")[福建泉州移民](../Page/福建.md "wikilink")，於1709年以該墾號名義向[臺灣府](../Page/臺灣府.md "wikilink")[諸羅縣官府申請開墾大佳臘的土地開墾](../Page/諸羅縣.md "wikilink")。

而因18世紀初台灣南部地區人口漸多，可耕田地漸少，加上1705年至1708年連續三年的飢荒，於是官方批准了陳賴章墾號所提出的該開墾申請，希望藉由該墾號的拓墾，增加臺灣糧食的供應力，以支應[福建的缺糧情況](../Page/福建.md "wikilink")。

## 墾號範圍

陳賴章墾號所申請的地區為台灣北部的大佳臘。其開墾範圍面積相當廣。文獻上所記載為「東至[雷里](../Page/雷里.md "wikilink")（今[南萬華](../Page/加蚋子.md "wikilink")）、[秀朗](../Page/秀朗.md "wikilink")（今[中和](../Page/中和區.md "wikilink")）二社，西至[八里坌](../Page/八里坌.md "wikilink")（今[八里](../Page/八里區.md "wikilink")）、[干脰](../Page/干脰.md "wikilink")（今[關渡](../Page/關渡.md "wikilink")），南至[興直山腳](../Page/興直山腳.md "wikilink")（今[新莊](../Page/新莊區.md "wikilink")），北至[大龍峒溝](../Page/大龍峒溝.md "wikilink")（今[圓山](../Page/圓山.md "wikilink")）」

這些地方，除了包含台北[艋舺](../Page/艋舺.md "wikilink")，[大龍峒](../Page/大龍峒.md "wikilink")，[大稻埕](../Page/大稻埕.md "wikilink")，[錫口等現今](../Page/錫口.md "wikilink")[台北市市中心地區之外](../Page/台北市.md "wikilink")，更包括了今日的[新北市](../Page/新北市.md "wikilink")[汐止](../Page/汐止區.md "wikilink")、[中和](../Page/中和區.md "wikilink")、[永和](../Page/永和區.md "wikilink")、[八里](../Page/八里區.md "wikilink")、[三重](../Page/三重區.md "wikilink")、[蘆洲](../Page/蘆洲區.md "wikilink")、[泰山](../Page/泰山區_\(新北市\).md "wikilink")、[新莊地區一帶](../Page/新莊區.md "wikilink")，面積廣達百平方公里。

## 影響

陳賴章墾號前往[台北開墾後](../Page/台北.md "wikilink")，讓本以[南台灣為中心的](../Page/南台灣.md "wikilink")[閩南](../Page/閩南.md "wikilink")[泉州移民勢力深入](../Page/泉州.md "wikilink")[北台灣](../Page/北台灣.md "wikilink")，也正式宣佈清朝官方保護[蕃地與平埔族權益的政策的轉變](../Page/蕃地.md "wikilink")。

而該墾號的通過，除了象徵[清朝默許](../Page/清朝.md "wikilink")[移民合法性之外](../Page/移民.md "wikilink")，對於台灣民間更有多層面的影響。

以民間影響論，此墾號的通過，掀起了[福建](../Page/福建.md "wikilink")[泉州人前往台灣謀生的另一波移民潮](../Page/泉州.md "wikilink")。台北地區的此移民潮，不但造成該地區許多[平埔族的](../Page/平埔族.md "wikilink")[漢化與滅種](../Page/漢化.md "wikilink")（如[凱達格蘭族](../Page/凱達格蘭族.md "wikilink")、[雷朗族](../Page/雷朗族.md "wikilink")），也造成[大台北](../Page/大台北.md "wikilink")[大稻埕](../Page/大稻埕.md "wikilink")、[八芝蘭](../Page/士林區.md "wikilink")、[艋舺](../Page/艋舺.md "wikilink")、[新莊等地區](../Page/新莊區.md "wikilink")，長達一兩百年之久的[漳](../Page/漳州.md "wikilink")、[泉](../Page/泉州.md "wikilink")、[粵](../Page/粵.md "wikilink")[分類械鬥](../Page/分類械鬥.md "wikilink")。

## 參考資料

  - 尹章義，《臺灣開發史研究》，1989年

## 相關條目

  - [姜秀鑾](../Page/姜秀鑾.md "wikilink")
  - [藍張興](../Page/藍張興.md "wikilink")

[Category:台北市歷史](../Category/台北市歷史.md "wikilink")
[Category:台北市經濟](../Category/台北市經濟.md "wikilink")
[Category:墾號](../Category/墾號.md "wikilink")