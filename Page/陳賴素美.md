Chen Lai Su-mei | image name = | width = | order=
第9屆[立法委員](../Page/立法委員.md "wikilink") | term_start=
2016年2月1日 | term_end= | constituency=
[桃園市第二選舉區](../Page/桃園市第二選舉區.md "wikilink")
（[大園區](../Page/大園區.md "wikilink")、[觀音區](../Page/觀音區.md "wikilink")、[新屋區](../Page/新屋區.md "wikilink")、
[楊梅區](../Page/楊梅區.md "wikilink")） | majority= 89,792(50.17%) | office1 =
[桃園市黨部主委](../Page/桃園市.md "wikilink") | term_start1 = 2014年7月6日 |
term_end1 = | predecessor1 = [鄭文燦](../Page/鄭文燦.md "wikilink") |
successor1 = 現任 | office2 =
[Flag_of_Taoyuan_City.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Taoyuan_City.svg "fig:Flag_of_Taoyuan_City.svg")第1屆[桃園市議員](../Page/桃園市議會.md "wikilink")
| term_start2 = 2014年12月25日 | term_end2 = 2016年1月31日 | constituency2 =
第九選區（[楊梅區](../Page/楊梅區.md "wikilink")） | office3
=[Flag_of_Taoyuan_City.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Taoyuan_City.svg "fig:Flag_of_Taoyuan_City.svg")第15-17屆[桃園縣](../Page/桃園縣.md "wikilink")[議員](../Page/第17屆桃園縣議員列表.md "wikilink")
| term_start3 = 2002年3月1日 | term_end3 = 2014年12月24日 | constituency3 =
第九選舉區
（[楊梅鎮](../Page/楊梅鎮.md "wikilink")→[楊梅市](../Page/楊梅市.md "wikilink")） |
sex = 女 | birth_date =  | birth_place =
[新竹縣](../Page/新竹縣.md "wikilink") | death_date = |
death_place = | Native place = [新竹縣](../Page/新竹縣.md "wikilink") |
nationality =  | party =  | parents = | relatives = | spouse = |
children = | profession = | website = }}

<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>國家地政士特考及格</li>
<li><a href="../Page/國立中央大學.md" title="wikilink">國立中央大學</a><a href="../Page/EMBA.md" title="wikilink">EMBA企管碩士</a>（學分班）</li>
<li><a href="../Page/萬能科技大學.md" title="wikilink">萬能科技大學學士</a></li>
<li><a href="../Page/國立新竹高商.md" title="wikilink">國立新竹高商</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li><a href="../Page/立法院.md" title="wikilink">立法院第</a>9屆<a href="../Page/桃園市.md" title="wikilink">桃園市第二選區</a>(大園、觀音、新屋、楊梅)<a href="../Page/立法委員.md" title="wikilink">立法委員</a></li>
<li><a href="../Page/民進黨.md" title="wikilink">民進黨中央執行委員</a></li>
<li><a href="../Page/民進黨.md" title="wikilink">民進黨</a><a href="../Page/桃園市.md" title="wikilink">桃園市</a>（<a href="../Page/直轄市_(中華民國).md" title="wikilink">直轄市</a>）黨部主任委員</li>
<li>民進黨桃園縣黨部執行委員</li>
<li>民進黨桃園縣楊梅市黨部主任委員</li>
<li>民進黨全國黨代表</li>
<li>桃園縣議會第17屆縣議員</li>
<li>桃園縣議會第16屆縣議員</li>
<li>桃園縣議會第15屆縣議員</li>
<li>聯合代書事務所負責人</li>
<li>桃園縣陳姓宗親會副會長</li>
<li>桃園縣楊梅市陽光婦女會理事長</li>
<li>桃園縣活力康福養生協會理事長</li>
</ul></td>
</tr>
</tbody>
</table>

**陳賴素美**（****，），[臺灣女性](../Page/臺灣.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，[新竹縣人](../Page/新竹縣.md "wikilink")，為[民主進步黨籍](../Page/民主進步黨.md "wikilink")，[萬能科技大學學士](../Page/萬能科技大學.md "wikilink")，[國立中央大學](../Page/國立中央大學.md "wikilink")[EMBA企管碩士班](../Page/EMBA.md "wikilink")。現任[立法院第](../Page/立法院.md "wikilink")9屆桃園市第二選區(大園、觀音、新屋、楊梅)立法委員，[民主進步黨第](../Page/民主進步黨.md "wikilink")17屆中央執行委員。曾任[桃園縣議會第九選區](../Page/桃園縣議會.md "wikilink")（[楊梅市](../Page/楊梅市.md "wikilink")/鎮）第十六、十七屆縣議員，第一屆[桃園市議員](../Page/桃園市議會.md "wikilink")，民進黨桃園市（[直轄市](../Page/直轄市_\(中華民國\).md "wikilink")）黨部主任委員,民進黨全國黨代表，民進黨桃園縣楊梅市黨部主任委員，民進黨桃園縣黨部執行委員，聯合代書事務所負責人等。\[1\]。

## 政治參選

2015年3月26日陳賴素美在桃園市大園區、觀音區、新屋區、楊梅區立委選區民調初選「黑馬」勝出，以[市議員身分打敗民進黨桃園市](../Page/市議員.md "wikilink")（縣）前任三位[立法委員](../Page/立法委員.md "wikilink")[郭榮宗](../Page/郭榮宗.md "wikilink")、[彭添富](../Page/彭添富.md "wikilink")、[彭紹瑾等人](../Page/彭紹瑾.md "wikilink")\[2\]。同年4月15日，代表民進黨角逐[2016年中華民國立法委員選舉](../Page/2016年中華民國立法委員選舉.md "wikilink")[桃園市第二選舉區](../Page/桃園市第二選舉區.md "wikilink")（[大園區](../Page/大園區.md "wikilink")、[觀音區](../Page/觀音區.md "wikilink")、[新屋區](../Page/新屋區.md "wikilink")、[楊梅區](../Page/楊梅區.md "wikilink")）對上[國民黨的現任](../Page/國民黨.md "wikilink")[立委](../Page/立委.md "wikilink")[廖正井](../Page/廖正井.md "wikilink")
\[3\]。

## 政策

### 救消失的自行車道

陳賴素美會勘桃園市大園區竹圍到圳頭里海天一色自行車道遭風飛沙淹沒，要求重新研議規畫車道動線，其他沙崙、圳頭車道的積沙垃圾由經濟部第二河川局、市府單位清除。\[4\]。

## 爭議

### 遭指控罷占租屋不還

2004年陳賴素美第一次參選時，向彭、陳夫妻租下透天厝，做為競選總部和服務處。租約於2015年5月7日到期。房東指出，因為稅務衍生費用，負擔增加，欲提高房租，但陳賴素美對租金提高一事從不回應。而其夫經民進黨黨部調查發現，有多項前科紀錄包括竊盜、賭博、媒介色情、槍炮和殺人未遂等，並曾於1986至1993年入獄服刑。其夫出獄後因在地方頗有勢力，發動支持者力挺前民進黨主席[蘇貞昌](../Page/蘇貞昌.md "wikilink")，換得陳賴素美出任民進黨中評委，並競選議員，一路連任3屆。其夫則隱身幕後成為陳賴素美的主要操盤人。陳賴素美已於2015年6月將服務處清空，搬遷他處。\[5\]

## 2014年直轄市議員選舉投票結果

### 桃園市第九選舉區（楊梅區）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>性別</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/姜水浪.md" title="wikilink">姜水浪</a></p></td>
<td><p>男</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Taiwan_Stars.png" title="fig:Red_Taiwan_Stars.png">Red_Taiwan_Stars.png</a><a href="../Page/台灣主義黨.md" title="wikilink">台灣主義黨</a></p></td>
<td><p>597</p></td>
<td><p>0.83%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><strong><a href="../Page/周玉琴.md" title="wikilink">周玉琴</a></strong></p></td>
<td><p>女</p></td>
<td></td>
<td><p>14,468</p></td>
<td><p>20.22%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/林羅春.md" title="wikilink">林羅春</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>3,272</p></td>
<td><p>4.57%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><strong><a href="../Page/李家興.md" title="wikilink">李家興</a></strong></p></td>
<td><p>男</p></td>
<td></td>
<td><p>13,875</p></td>
<td><p>19.39%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><strong><a href="../Page/陳賴素美.md" title="wikilink">陳賴素美</a></strong></p></td>
<td><p>女</p></td>
<td></td>
<td><p>15,882</p></td>
<td><p>22.19%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/陳秀玲.md" title="wikilink">陳秀玲</a></p></td>
<td><p>女</p></td>
<td></td>
<td><p>5,916</p></td>
<td><p>8.27%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><strong><a href="../Page/張火爐_(桃園市議員).md" title="wikilink">張火爐</a></strong></p></td>
<td><p>男</p></td>
<td></td>
<td><p>14,664</p></td>
<td><p>20.49%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/彭基原.md" title="wikilink">彭基原</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>2,892</p></td>
<td><p>4.04%</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [雙姓](../Page/雙姓.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [桃園市議會影音網站](http://www.tycc.gov.tw/content/video/video_main.aspx?wtp=1&wnd=215&kw=%e9%99%b3%e8%b3%b4%e7%b4%a0%e7%be%8e)
  - [陳賴素美｜Facebook](https://www.facebook.com/pages/%E9%99%B3%E8%B3%B4%E7%B4%A0%E7%BE%8E-%E7%B2%89%E7%B5%B2%E5%9C%98/448974038585873)
  - [綠初選「黑馬」！　陳賴素美勝出踢走3立委｜三立新聞台 |
    Youtube](https://www.youtube.com/watch?v=gTpyeGm21fc)

|- |colspan="3" style="text-align:center;"|**** |-      |- |colspan="3"
style="text-align:center;"|**[Flag_of_Taoyuan_City.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Taoyuan_City.svg "fig:Flag_of_Taoyuan_City.svg")[桃園市議會](../Page/桃園市議會.md "wikilink")**
|-    |-   |- |colspan="3"
style="text-align:center;"|**[Flag_of_Taoyuan_City.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Taoyuan_City.svg "fig:Flag_of_Taoyuan_City.svg")[桃園縣議會](../Page/桃園縣議會.md "wikilink")**
|-    |-

[Category:桃園市議員](../Category/桃園市議員.md "wikilink")
[Category:桃園縣議員](../Category/桃園縣議員.md "wikilink")
[Category:第1屆桃園市議員](../Category/第1屆桃園市議員.md "wikilink")
[Category:第17屆桃園縣議員](../Category/第17屆桃園縣議員.md "wikilink")
[Category:第16屆桃園縣議員](../Category/第16屆桃園縣議員.md "wikilink")
[Category:民主進步黨女黨員](../Category/民主進步黨女黨員.md "wikilink")
[管](../Category/國立中央大學校友.md "wikilink")
[Category:萬能科技大學校友](../Category/萬能科技大學校友.md "wikilink")
[Category:桃園市政治人物](../Category/桃園市政治人物.md "wikilink")
[Category:臺灣女性立法委員](../Category/臺灣女性立法委員.md "wikilink")
[Category:楊梅人](../Category/楊梅人.md "wikilink")
[Category:新竹縣人](../Category/新竹縣人.md "wikilink")
[Category:第9屆中華民國立法委員](../Category/第9屆中華民國立法委員.md "wikilink")
[C](../Category/客家裔臺灣人.md "wikilink")

1.  [桃園市議會陳議員個人網站](http://www.tycc.gov.tw/content/intro/intro_main.aspx?wtp=1&wnd=204&id=1062)
2.  [綠桃園市第2選區初選　陳賴素美打敗3位前立委](http://www.appledaily.com.tw/realtimenews/article/new/20150325/580713/)
3.  [民進黨第九屆立法委員提名候選人公告](http://www.dpp.org.tw/news_content.php?&sn=7808)

4.  [1](http://udn.com/news/story/7324/1568552-救消失的自行車道-桃園立委會勘謀策)
5.  [影響提名？民進黨桃園主委遭控霸民宅不還](http://www.peoplenews.tw/news/150fec61-ea0a-4f9e-8ffa-d280d84fc33c),
    [民報](../Page/民報.md "wikilink"), 2015/6/10