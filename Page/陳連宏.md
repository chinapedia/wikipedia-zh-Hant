**陳連宏**（），原名**陳俊宏**，為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，台南市[大內區](../Page/大內區.md "wikilink")[西拉雅族原住民](../Page/西拉雅族.md "wikilink")。曾為[中華職棒](../Page/中華職棒.md "wikilink")[統一7-ELEVEn獅教練](../Page/統一7-ELEVEn獅.md "wikilink")。守備位置為[外野手](../Page/外野手.md "wikilink")，球員時期後期大都以[指定打擊身分上場](../Page/指定打擊.md "wikilink")。08月4日因[統一7-ELEVEn獅戰績壓力](../Page/統一7-ELEVEn獅.md "wikilink")，下半季卻敗多勝少，使得總教練[中島輝士於賽前由球團表示將至二軍擔任教練](../Page/中島輝士.md "wikilink")，由打擊教練**陳連宏**代理總教練職務。08月13日賽前[統一7-ELEVEn獅自](../Page/統一7-ELEVEn獅.md "wikilink")**陳連宏**代理總教練後拿下5勝1敗成績，領隊蘇泰安決定將其真除，為[統一7-ELEVEn獅隊史第](../Page/統一7-ELEVEn獅.md "wikilink")13任總教練、第9任正式總教練。其胞弟為於2016年球季退休的前[Lamigo桃猿](../Page/Lamigo桃猿.md "wikilink")[外野手和](../Page/外野手.md "wikilink")[指定打擊](../Page/指定打擊.md "wikilink")[陳金鋒](../Page/陳金鋒.md "wikilink")

## 經歷

  - [台南縣善化國小少棒隊](../Page/台南縣.md "wikilink")
  - [台南市復興國中青少棒隊](../Page/台南市復興國中.md "wikilink")
  - [台中市](../Page/台中市.md "wikilink")[新民商工青棒隊](../Page/台中市私立新民高級中學.md "wikilink")（俊國）
  - [俊國建設棒球隊](../Page/俊國建設.md "wikilink")（業餘）
  - [中國信託棒球隊](../Page/中國信託.md "wikilink")（業餘）
  - 陸光棒球隊
  - 中華職棒[和信鯨隊](../Page/和信鯨.md "wikilink")（1997年 - 2001年）
  - 中華職棒[中信鯨隊](../Page/中信鯨.md "wikilink")（2002年）
  - 中華職棒[統一獅隊](../Page/統一獅.md "wikilink")（2003年 - 2008年2月21日）
  - 中華職棒[統一7-ELEVEn獅隊](../Page/統一7-ELEVEn獅.md "wikilink")（2008年2月22日 -
    2011年）
  - 中華職棒[統一7-ELEVEn獅隊二軍打擊教練](../Page/統一7-ELEVEn獅.md "wikilink")（2012年 -
    2012年2月）
  - 中華職棒統一7-ELEVEn獅隊打擊教練（2012年2月 - 2013年8月3日）
  - 中華職棒統一7-ELEVEn獅隊代理總教練（2013年8月3日 - 2013年8月13日）
  - [統一7-ELEVEn獅隊總教練](../Page/統一7-ELEVEn獅.md "wikilink")（2013年8月14日 -
    2015年12月）
  - [統一7-ELEVEn獅隊首席教練](../Page/統一7-ELEVEn獅.md "wikilink")（2016年）
  - [富邦悍將隊二軍總教練](../Page/富邦悍將.md "wikilink")（2018年)
  - [富邦悍將隊總教練](../Page/富邦悍將.md "wikilink")（2018年7月30日-)

## 職棒生涯成績

| 年度    | 球隊                                               | 出賽   | 打數   | 安打   | 全壘打 | 打點  | 盜壘  | 四死  | 三振  | 壘打數  | 雙殺打    | 打擊率   |
| ----- | ------------------------------------------------ | ---- | ---- | ---- | --- | --- | --- | --- | --- | ---- | ------ | ----- |
| 1997年 | [中信鯨](../Page/中信鯨.md "wikilink")                 | 86   | 260  | 65   | 3   | 21  | 14  | 36  | 74  | 82   | 5      | 0.250 |
| 1998年 | [中信鯨](../Page/中信鯨.md "wikilink")                 | 101  | 328  | 103  | 16  | 65  | 26  | 65  | 70  | 181  | 0      | 0.314 |
| 1999年 | [中信鯨](../Page/中信鯨.md "wikilink")                 | 87   | 299  | 99   | 5   | 50  | 13  | 59  | 70  | 149  | 8      | 0.331 |
| 2000年 | [中信鯨](../Page/中信鯨.md "wikilink")                 | 84   | 286  | 87   | 13  | 46  | 8   | 60  | 65  | 149  | 6      | 0.304 |
| 2001年 | [中信鯨](../Page/中信鯨.md "wikilink")                 | 84   | 279  | 80   | 7   | 44  | 10  | 54  | 61  | 122  | 8      | 0.287 |
| 2002年 | [中信鯨](../Page/中信鯨.md "wikilink")                 | 57   | 178  | 52   | 3   | 27  | 8   | 36  | 30  | 70   | 3      | 0.292 |
| 2003年 | [統一獅](../Page/統一獅.md "wikilink")                 | 95   | 333  | 111  | 10  | 62  | 5   | 40  | 45  | 166  | **16** | 0.333 |
| 2004年 | [統一獅](../Page/統一獅.md "wikilink")                 | 80   | 205  | 62   | 5   | 29  | 3   | 21  | 55  | 86   | 8      | 0.302 |
| 2005年 | [統一獅](../Page/統一獅.md "wikilink")                 | 94   | 305  | 94   | 5   | 26  | 5   | 49  | 43  | 127  | 8      | 0.308 |
| 2006年 | [統一獅](../Page/統一獅.md "wikilink")                 | 82   | 246  | 75   | 4   | 33  | 2   | 36  | 36  | 98   | 11     | 0.305 |
| 2007年 | [統一獅](../Page/統一獅.md "wikilink")                 | 87   | 266  | 76   | 11  | 48  | 3   | 36  | 47  | 127  | 10     | 0.286 |
| 2008年 | [統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink") | 87   | 309  | 97   | 13  | 65  | 2   | 54  | 55  | 158  | 7      | 0.314 |
| 2009年 | [統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink") | 113  | 405  | 126  | 12  | 76  | 3   | 58  | 77  | 187  | 19     | 0.311 |
| 2010年 | [統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink") | 58   | 168  | 33   | 2   | 10  | 0   | 24  | 36  | 47   | 5      | 0.196 |
| 2011年 | [統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink") | 50   | 136  | 36   | 3   | 16  | 0   | 14  | 18  | 51   | 7      | 0.265 |
| 合計    | 15年                                              | 1245 | 4003 | 1196 | 112 | 611 | 102 | 642 | 786 | 1800 | 121    | 0.299 |

### 總教練時期

#### [中華職棒](../Page/中華職棒.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>球隊</p></th>
<th><p>名次</p></th>
<th><p>出賽</p></th>
<th><p>勝場</p></th>
<th><p>和局</p></th>
<th><p>敗場</p></th>
<th><p>勝率</p></th>
<th><p>勝差</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/統一7-ELEVEn獅.md" title="wikilink">統一7-ELEVEn獅</a></p></td>
<td><p><strong>1</strong></p></td>
<td><p>44</p></td>
<td><p>27</p></td>
<td><p>2</p></td>
<td><p>15</p></td>
<td><p>0.643</p></td>
<td><p>-</p></td>
<td><p>接替<a href="../Page/中島輝士.md" title="wikilink">中島輝士</a>、<a href="../Page/2013年中華職棒總冠軍賽.md" title="wikilink">總冠軍</a></p></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/統一7-ELEVEn獅.md" title="wikilink">統一7-ELEVEn獅</a></p></td>
<td><p>2</p></td>
<td><p>120</p></td>
<td><p>58</p></td>
<td><p>7</p></td>
<td><p>55</p></td>
<td><p>0.513</p></td>
<td><p>6</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/統一7-ELEVEn獅.md" title="wikilink">統一7-ELEVEn獅</a></p></td>
<td><p>4</p></td>
<td><p>119</p></td>
<td><p>49</p></td>
<td><p>2</p></td>
<td><p><strong>68</strong></p></td>
<td><p>0.422</p></td>
<td><p>17.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td><p><a href="../Page/富邦悍將.md" title="wikilink">富邦悍將</a></p></td>
<td><p>3</p></td>
<td><p>44</p></td>
<td><p>22</p></td>
<td><p>0</p></td>
<td><p>22</p></td>
<td><p>.500</p></td>
<td><p>3</p></td>
<td><p>接替<a href="../Page/葉君璋.md" title="wikilink">葉君璋</a>、因禁賽3場由<a href="../Page/劉榮華.md" title="wikilink">劉榮華代理</a><br />
<a href="../Page/2018年中華職棒季後挑戰賽.md" title="wikilink">季後賽</a>（1-3敗<a href="../Page/統一獅隊.md" title="wikilink">統一隊</a>）</p></td>
</tr>
<tr class="odd">
<td><p>合計</p></td>
<td><p>4年</p></td>
<td><p>－</p></td>
<td><p>326</p></td>
<td><p>156</p></td>
<td><p>11</p></td>
<td><p>159</p></td>
<td><p>0.495</p></td>
<td><p>-</p></td>
<td></td>
</tr>
</tbody>
</table>

## 職棒生涯特殊記錄

  - 2008年3月16日：刷新高國慶當日八局下半締造的開幕戰個人最高打點記錄，該比賽陳連宏藉由一支滿貫全壘打及八局下半一支三分打點全壘打，以7分打點締造中華職棒開幕戰個人最高打點記錄。
  - 2008年10月6日：於台南球場對戰[興農牛](../Page/興農牛.md "wikilink")，達成生涯1000支安打紀錄，中職第8人。
  - 2009年4月10日：於台南球場對戰[興農牛](../Page/興農牛.md "wikilink")，達成生涯第100次盜壘成功。
  - 2009年7月9日：於台南球場對戰[興農牛](../Page/興農牛.md "wikilink")，從[林英傑手中擊出生涯第](../Page/林英傑.md "wikilink")100支全壘打，聯盟第9人（成為聯盟第3位千安百轟百盜、聯盟第4位百轟百盗、聯盟第6位千安百轟）。
  - 2011年3月29日：於台南棒球場對戰[兄弟象](../Page/兄弟象.md "wikilink")，為個人生涯第1200場出賽，史上第7人。
  - 2011年7月16日：於新莊棒球場對戰[兄弟象於四局上半轟出](../Page/兄弟象.md "wikilink")3分打點全壘打，達成個人職棒生涯第600分打點，史上第4人。
  - 2012年4月7日：與隊友[莊景賀於台南棒球場](../Page/莊景賀.md "wikilink")，舉辦引退儀式。

## 外部連結

|- |colspan="3"
style="text-align:center;"|**[中華職業棒球大聯盟](../Page/中華職業棒球大聯盟.md "wikilink")**
|-

[Category:統一獅隊球員](../Category/統一獅隊球員.md "wikilink")
[Category:中信鯨隊球員](../Category/中信鯨隊球員.md "wikilink")
[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:台灣棒球教練](../Category/台灣棒球教練.md "wikilink")
[Category:台灣原住民運動員](../Category/台灣原住民運動員.md "wikilink")
[Category:台南市立復興國民中學校友](../Category/台南市立復興國民中學校友.md "wikilink")
[Category:中華青棒隊球員](../Category/中華青棒隊球員.md "wikilink")
[Category:中華成棒隊球員](../Category/中華成棒隊球員.md "wikilink")
[Category:中華職棒單月最有價值球員獎得主](../Category/中華職棒單月最有價值球員獎得主.md "wikilink")
[Category:中華職棒千安選手](../Category/中華職棒千安選手.md "wikilink")
[Category:中華職棒百盜選手](../Category/中華職棒百盜選手.md "wikilink")
[Category:中華職棒百轟選手](../Category/中華職棒百轟選手.md "wikilink")
[Category:統一獅隊總教練](../Category/統一獅隊總教練.md "wikilink")
[Category:統一獅隊教練](../Category/統一獅隊教練.md "wikilink")
[Category:西拉雅族人](../Category/西拉雅族人.md "wikilink")
[Category:陳姓](../Category/陳姓.md "wikilink")