**陳重信**（1943年－），[臺灣](../Page/臺灣.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，曾代表[民主進步黨任職僑選](../Page/民主進步黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，為海外僑民代表，2007年5月任[行政院環境保護署代理署長](../Page/行政院環境保護署.md "wikilink")。2007年6月，陳重信任行政院環境保護署署長，僑選立法委員缺額由前[世界台灣同鄉會祕書長](../Page/世界台灣同鄉會.md "wikilink")[陳東榮遞補](../Page/陳東榮.md "wikilink")。

2007年8月10日，環保署暴力驅逐《[自由時報](../Page/自由時報.md "wikilink")》記者[周富美採訪](../Page/周富美.md "wikilink")[環境影響評估大會](../Page/環境影響評估.md "wikilink")，是為[周富美事件](../Page/周富美事件.md "wikilink")：當時周富美因採訪環評大會而遭陳重信坐視環保署政風室主任范大維暴力驅離\[1\]，事後《自由時報》將周富美調到內勤，周富美憤而提出辭呈。2007年8月11日，[臺灣蠻野心足生態協會理事長](../Page/臺灣蠻野心足生態協會.md "wikilink")[文魯彬說](../Page/文魯彬.md "wikilink")，陳重信曾經在[臺灣戒嚴時期名列](../Page/臺灣戒嚴時期.md "wikilink")[黑名單而長期滯留](../Page/黑名單_\(臺灣\).md "wikilink")[美國](../Page/美國.md "wikilink")，現在卻在臺灣實施「環評戒嚴」對記者動粗，實在諷刺\[2\]。

2008年5月，[沈世宏取代陳重信接任行政院環境保護署署長](../Page/沈世宏.md "wikilink")。\[3\]

## 經歷

  - [美國國家環境保護局資深科學主管](../Page/美國國家環境保護局.md "wikilink")
  - [美國食品藥物管理局消費安全官員](../Page/美國食品藥物管理局.md "wikilink")
  - [華府](../Page/華府.md "wikilink")[台灣國際關係中心副主任](../Page/台灣國際關係中心.md "wikilink")
  - [世界台灣同鄉會秘書長](../Page/世界台灣同鄉會.md "wikilink")
  - [宜蘭盃國際名校划船邀請賽海外總聯絡](../Page/宜蘭盃國際名校划船邀請賽.md "wikilink")
  - 第六屆[立法委員](../Page/立法委員.md "wikilink")
  - [行政院環境保護署副署長](../Page/行政院環境保護署.md "wikilink")
  - [行政院環境保護署署長](../Page/行政院環境保護署.md "wikilink")

## 參考資料

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00134&stage=6)

|- |colspan="3"
style="text-align:center;"|**[ROC_Executive_Yuan_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Executive_Yuan_Logo.svg "fig:ROC_Executive_Yuan_Logo.svg")[行政院](../Page/行政院.md "wikilink")**
|-

[C](../Category/第6屆中華民國立法委員.md "wikilink")
[C](../Category/民主進步黨黨員.md "wikilink")
[C](../Category/陳姓.md "wikilink")
[Category:行政院環境保護署副署長](../Category/行政院環境保護署副署長.md "wikilink")
[Category:行政院環境保護署署長](../Category/行政院環境保護署署長.md "wikilink")

1.
2.
3.  [歷任署長一覽表](http://www.epa.gov.tw/ct.asp?xItem=6421&ctNode=30619&mp=epa)
    . 行政院環境保護署