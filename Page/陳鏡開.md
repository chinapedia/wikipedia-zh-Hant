[1964-09_1964年_陈镜开打破世界纪录.jpg](https://zh.wikipedia.org/wiki/File:1964-09_1964年_陈镜开打破世界纪录.jpg "fig:1964-09_1964年_陈镜开打破世界纪录.jpg")
**陈镜开**（），生于[广东省](../Page/广东省.md "wikilink")[东莞市](../Page/东莞市.md "wikilink")[石龙镇](../Page/石龙镇_\(东莞市\).md "wikilink")，\[1\]。[中國](../Page/中华人民共和国.md "wikilink")1950年代著名[舉重選手](../Page/舉重.md "wikilink")。陈镜开是中华人民共和国成立以来第一个打破世界纪录的运动员，曾经9次打破世界纪录，5次获得国家体育运动荣誉奖章，荣立特等功一次、一等功两次，并被推选为第二、三、四、五届[全国人大代表](../Page/全国人大.md "wikilink")。

陈镜开曾连续9次打破世界纪录，但却始终无缘奥运赛场。[文化大革命中](../Page/文化大革命.md "wikilink")，陈镜开被派去做锅炉工，据说连举重用的杠铃都扔进了炼钢炉\[2\]。1987年，他获得了[国际奥委会授予的](../Page/国际奥委会.md "wikilink")[奥林匹克银质勋章](../Page/奥林匹克银质勋章.md "wikilink")。

2010年12月6日，陈镜开在廣州逝世，終年75歲。\[3\]

## 相关人物

[1984年第23届奥运会](../Page/1984年夏季奧林匹克運動會.md "wikilink")，陈镜开的侄子[陈伟强夺得男子](../Page/陈伟强.md "wikilink")60公斤级举重金牌。

## 注释

[Category:東莞籍運動員](../Category/東莞籍運動員.md "wikilink")
[Category:华南师范大学附属中学校友](../Category/华南师范大学附属中学校友.md "wikilink")
[Category:中国举重运动员](../Category/中国举重运动员.md "wikilink")
[Category:第二届全国人大代表](../Category/第二届全国人大代表.md "wikilink")
[Category:第三届全国人大代表](../Category/第三届全国人大代表.md "wikilink")
[Category:第四届全国人大代表](../Category/第四届全国人大代表.md "wikilink")
[Category:第五届全国人大代表](../Category/第五届全国人大代表.md "wikilink")
[Category:奥林匹克勋章获得者](../Category/奥林匹克勋章获得者.md "wikilink")
[J](../Category/陈姓.md "wikilink")

1.
2.
3.  [“开国第一举”落下，从此与鲜花相伴](http://sports.southcn.com/s/2010-12/07/content_18213219.htm)