**陳鑑**（），[字](../Page/表字.md "wikilink")**緝熙**\[1\]，[號](../Page/號.md "wikilink")**方庵**、**芳庵**，室名**心遠樓**\[2\]，[直隸](../Page/南直隸.md "wikilink")[長洲](../Page/長洲.md "wikilink")（今[江蘇省](../Page/江蘇省.md "wikilink")[蘇州市](../Page/蘇州市.md "wikilink")）人。

## 生平

[正统十三年](../Page/正统_\(年号\).md "wikilink")（1448年）一甲第二名進士（[榜眼](../Page/榜眼.md "wikilink")）。[天順元年](../Page/天顺_\(明朝\).md "wikilink")（1457年）與[高閏出使](../Page/高閏.md "wikilink")[朝鮮](../Page/朝鮮.md "wikilink")，與朝鮮大臣有唱和，集成《丁丑皇華集》\[3\]\[4\]。官至[國子監祭酒](../Page/國子監祭酒.md "wikilink")\[5\]。

## 著作

著有《介庵集》，今不傳。

## 家族

曾祖父陳彥名。祖父陳子騏。父亲陳德潤\[6\]。

## 注釋

## 參考書目

  - 吳寬《前朝列大夫國子祭酒陳公墓誌銘》
  - 《明實錄·憲宗實錄》卷八十九「成化七年三月辛卯」

[Category:明朝榜眼](../Category/明朝榜眼.md "wikilink")
[Category:明朝翰林](../Category/明朝翰林.md "wikilink")
[Category:明朝禮部侍郎](../Category/明朝禮部侍郎.md "wikilink")
[Category:明朝國子監祭酒](../Category/明朝國子監祭酒.md "wikilink")
[Category:明朝赴朝鮮使臣](../Category/明朝赴朝鮮使臣.md "wikilink")
[Category:蘇州人](../Category/蘇州人.md "wikilink")
[J](../Category/陳姓.md "wikilink")

1.  翁方綱在〈跋蘭亭領字从山本二首〉一文談到蘭亭褚摹本之鑑定時有「若在明代陳緝熙、王損齋、董思白諸人所据，或未可為信」(《復初齋文集》卷27),此陳緝熙應即指長洲陳鑑也.
2.  《吳縣志》67卷
3.  《朝鮮李朝世祖實錄》記載：先是，明使陳鑑、高閏來頒正統皇帝復位詔。陳、高等凡所見雜興，一寓於詩，合若干首，幷本國人所和，印而贈之，名曰皇華集。其後中朝人因本國人赴燕京，求之者頗多，輒印送之。
4.  《皇華集》乃收錄有陳鑒、[倪謙](../Page/倪謙.md "wikilink")、[金湜](../Page/金湜.md "wikilink")、[祁順](../Page/祁順.md "wikilink")、[唐皋](../Page/唐皋.md "wikilink")、[龔用卿](../Page/龔用卿.md "wikilink")、[華察](../Page/華察.md "wikilink")、[張承憲](../Page/張承憲.md "wikilink")、王鶴、[歐希稷](../Page/歐希稷.md "wikilink")、[許國](../Page/許國.md "wikilink")、[韓世能等人出使朝鮮時的作品](../Page/韓世能.md "wikilink")。
5.  《盖平县志》清-骆云：陈鉴　盖州人正统闲进士赐及第官翰林学士使朝鲜有皇华集行于世历官祭酒礼部侍郎文章学行一时景仰。
6.  《天一阁藏明代科举录选刊·登科录》（《正統十三年戊辰科殿試金榜》）