**陶哲轩**（，），第二代[澳洲香港移民](../Page/華裔澳大利亞人.md "wikilink")，澳大利亚[数学家](../Page/数学家.md "wikilink")，童年时期即[天资过人](../Page/神童.md "wikilink")\[1\]
，24歲當[UCLA數學系終身教授](../Page/UCLA.md "wikilink")，31歲獲[菲爾茲獎](../Page/菲爾茲獎.md "wikilink")。

目前主要研究[调和分析](../Page/调和分析.md "wikilink")、[偏微分方程](../Page/偏微分方程.md "wikilink")、[组合数学](../Page/组合数学.md "wikilink")、[解析数论和](../Page/解析数论.md "wikilink")[表示论](../Page/表示论.md "wikilink")。目前他与[韩裔妻子劳拉](../Page/韩裔.md "wikilink")（Laura）和儿子威廉（William）住在[美国](../Page/美国.md "wikilink")[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[洛杉矶](../Page/洛杉矶.md "wikilink")。

## 生平

### 家庭

父親陶象國（Dr. Billy
Tao）是[兒科醫生](../Page/兒科.md "wikilink")，生於[上海](../Page/上海.md "wikilink")，1969年[香港大學](../Page/香港大學.md "wikilink")[內外全科醫學士畢業](../Page/內外全科醫學士.md "wikilink")。\[2\]母親梁蕙蘭（Grace
Tao）是[香港人](../Page/香港人.md "wikilink")，[香港大學物理及數學](../Page/香港大學.md "wikilink")[一級榮譽畢業](../Page/一級榮譽.md "wikilink")\[3\]，曾在香港當中學數學老師\[4\]。兩人在香港大學結識\[5\]，1972年舉家移民澳洲，是第一代[澳洲香港移民](../Page/華裔澳大利亞人.md "wikilink")。\[6\]
.

### 幼年

陶哲轩在幼年時期便展现出数学天分。陶哲轩在7岁进入[高中就讀](../Page/高中.md "wikilink")，9岁进入[福林德斯大学](../Page/福林德斯大学.md "wikilink")，10岁、11岁、12岁参加[国际数学奥林匹克竞赛](../Page/国际数学奥林匹克竞赛.md "wikilink")，分获铜牌、银牌、金牌，分別是金銀銅牌最年輕得主的記錄保持者。他在16岁获得学士学位，17岁获得硕士学位，21岁获得[普林斯顿大学博士学位](../Page/普林斯顿大学.md "wikilink")，其博士指导教授是[埃利亚斯·施泰因](../Page/埃利亚斯·施泰因.md "wikilink")（Elias
M.
Stein）。他從24岁起在[加利福尼亚大学洛杉矶分校擔任教授](../Page/加利福尼亚大学洛杉矶分校.md "wikilink")。陶哲軒除了使用英語，還會說[粵語](../Page/粵語.md "wikilink")。

## 研究和奖项

他在2000年获得[塞勒姆奖](../Page/塞勒姆奖.md "wikilink")，2002年获得[博谢纪念奖](../Page/博谢纪念奖.md "wikilink")，2003年获得[克雷研究奖](../Page/克雷研究奖.md "wikilink")，以表扬他对分析学的贡献，当中包括[掛谷猜想和](../Page/掛谷猜想.md "wikilink")[wave
map](../Page/wave_map.md "wikilink")。[本·格林和陶哲轩在](../Page/本·格林_\(数学家\).md "wikilink")2004年发表了一篇论文预印稿，宣称证明了[格林-陶定理](../Page/格林-陶定理.md "wikilink")，即存在任意长的[素数](../Page/素数.md "wikilink")[等差数列](../Page/等差数列.md "wikilink")。

2005年获得利瓦伊·L·科南特奖（获奖者还有艾伦·克努森）。2006年8月22日，他在[西班牙](../Page/西班牙.md "wikilink")[马德里的](../Page/马德里.md "wikilink")[国际数学家大会獲得](../Page/国际数学家大会.md "wikilink")[菲尔兹奖](../Page/菲尔兹奖.md "wikilink")。并于2006年8月23日在[国际数学家大会做了一小时报告](../Page/国际数学家大会.md "wikilink")。同年获[SASTRA拉马努金奖](../Page/SASTRA拉马努金奖.md "wikilink")。2012年获[克拉福德奖](../Page/克拉福德奖.md "wikilink")。2014年榮獲[數學突破獎](../Page/數學突破獎.md "wikilink")，得到獎金300萬美元。\[7\]
2015年9月17日，他宣布证明了[保罗·埃尔德什的](../Page/保罗·埃尔德什.md "wikilink")[埃尔德什差异问题存在](../Page/埃尔德什差异问题.md "wikilink")\[8\]，这个困扰学术界80多年的问题。\[9\]

## 參考文獻

## 外部链接

  - [陶哲轩的学校网页](http://www.math.ucla.edu/~tao)

  - [陶哲轩的数学研究博客](http://terrytao.wordpress.com)

  - [克雷研究奖公告](http://www.claymath.org/research)

  -
  -
  -
{{-}}

[Category:菲尔兹奖获得者](../Category/菲尔兹奖获得者.md "wikilink")
[Category:麦克阿瑟学者](../Category/麦克阿瑟学者.md "wikilink")
[Category:21世纪数学家](../Category/21世纪数学家.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:數論学家](../Category/數論学家.md "wikilink")
[Category:组合学家](../Category/组合学家.md "wikilink")
[Category:澳大利亚数学家](../Category/澳大利亚数学家.md "wikilink")
[Category:美国数学家](../Category/美国数学家.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:普林斯頓大學校友](../Category/普林斯頓大學校友.md "wikilink")
[Category:澳大利亚华人](../Category/澳大利亚华人.md "wikilink")
[Category:华人数学家](../Category/华人数学家.md "wikilink")
[Category:阿德萊德人](../Category/阿德萊德人.md "wikilink")
[Z哲](../Category/陶姓.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[T](../Category/洛杉磯加州大學教師.md "wikilink")
[Category:福林德斯大學校友](../Category/福林德斯大學校友.md "wikilink")
[Category:数学分析师](../Category/数学分析师.md "wikilink")
[Category:国际数学奥林匹克竞赛参赛者](../Category/国际数学奥林匹克竞赛参赛者.md "wikilink")
[Category:突破奖获得者](../Category/突破奖获得者.md "wikilink")
[Category:克拉福德奖获得者](../Category/克拉福德奖获得者.md "wikilink")
[Category:皇家奖章获得者](../Category/皇家奖章获得者.md "wikilink")
[Category:SASTRA拉马努金奖获得者](../Category/SASTRA拉马努金奖获得者.md "wikilink")
[Category:博谢纪念奖获得者](../Category/博谢纪念奖获得者.md "wikilink")
[Category:弗雷德里克·埃瑟·内默斯数学奖获得者](../Category/弗雷德里克·埃瑟·内默斯数学奖获得者.md "wikilink")

1.
2.  [Dr Billy
    Tao](https://www.healthshare.com.au/profile/professional/135326-dr-billy-tao/),
    Healthshare.
3.  [Terence Tao: the Mozart of
    maths](http://www.smh.com.au/good-weekend/terence-tao-the-mozart-of-maths-20150216-13fwcv.html),
    March 7, 2015, Stephanie Wood, [The Sydney Morning
    Herald](../Page/The_Sydney_Morning_Herald.md "wikilink").
4.  *[Oriental Daily](../Page/Oriental_Daily.md "wikilink")*, Page A29,
    24 August 2006.
5.  [Terence Chi-Shen
    Tao](http://www-history.mcs.st-andrews.ac.uk/Biographies/Tao.html),
    MacTutor History of Mathematics archive, School of Mathematics and
    Statistics, University of St Andrews, Scotland.
6.
7.  [2014年數學突破獎](https://breakthroughprize.org/?controller=Page&action=news&news_id=18)
8.  <http://arxiv.org/abs/1509.05363>
9.