**陶氏化学**（****，，）是一間跨國[化學公司](../Page/化學.md "wikilink")，總部設於[美國](../Page/美國.md "wikilink")[密歇根州](../Page/密歇根州.md "wikilink")，1897年由[赫伯特·亨利·道创建](../Page/赫伯特·亨利·道.md "wikilink")。以[資產值計](../Page/資產值.md "wikilink")，是美国第二大、世界第三大的化學公司。自從2015年宣布將確定通過與[杜邦化工合併](../Page/杜邦化工.md "wikilink")（目前還在進行內部整合、合併階段），完全合併後將會成為全世界最大的化工巨擎，之後將會劃分成三個產業領域。

## 产品

陶氏化工是世界上最大的[塑料生产厂商](../Page/塑料.md "wikilink")，产品包括[聚苯乙烯](../Page/聚苯乙烯.md "wikilink")、[聚亚氨酯](../Page/聚亚氨酯.md "wikilink")、[聚对苯二甲酸乙二醇酯](../Page/聚对苯二甲酸乙二醇酯.md "wikilink")、[聚丙烯和](../Page/聚丙烯.md "wikilink")[合成橡胶等](../Page/合成橡胶.md "wikilink")。同时，它也是世界上主要的[氯化钙](../Page/氯化钙.md "wikilink")、[环氧乙烷](../Page/环氧乙烷.md "wikilink")、[丙烯酸盐](../Page/丙烯酸盐.md "wikilink")、[表面活性剂和](../Page/表面活性剂.md "wikilink")[纤维树脂的生产商](../Page/纤维树脂.md "wikilink")。陶氏化工业生产各种不同的农用药剂，最著名的是[毒死蜱](../Page/毒死蜱.md "wikilink")。陶氏化工也有一些被消费者们所熟悉的产品：「莎蘭」牌[保鲜膜](../Page/保鲜膜.md "wikilink")、「密保諾」牌[夾鏈袋](../Page/夾鏈袋.md "wikilink")
bags（现已售予[美國莊臣](../Page/美國莊臣.md "wikilink")）及聚苯乙烯[泡沫塑料](../Page/泡沫塑料.md "wikilink")。

## 历史

2015年12月11日[陶氏化學和](../Page/陶氏化學.md "wikilink")[杜邦化工以全股票交易的方式](../Page/杜邦.md "wikilink")「對等合併」，新公司將成全球僅次於[巴斯夫](../Page/巴斯夫.md "wikilink")（BASF）的第2大化工企業，並超越[孟山都](../Page/孟山都.md "wikilink")（Monsanto）成為全球最大種子和農藥公司。

2017年6月15日陶氏化工與杜邦公司已獲美國[反托拉斯](../Page/反托拉斯.md "wikilink")（在中國稱為[反壟斷](../Page/反壟斷.md "wikilink")）監管官員核准合併，但需出售一些專利種子和其他資產。

2017年8月28日陶氏化工与[沙烏地阿拉伯國家石油公司签署了一项不具约束力的协议](../Page/沙烏地阿拉伯國家石油公司.md "wikilink")，将其在Sadara化学公司的权益提高到50%。之前陶氏化工持有该公司35%的股权

2017年9月1日陶氏杜邦（纽交所交易代码：DWDP）宣布，陶氏化学公司（DOW，陶氏）与杜邦公司（DuPont，杜邦）于8月31日成功完成对等合并。合并后的实体为一家控股公司，称为[陶氏杜邦](../Page/陶氏杜邦.md "wikilink")（DowDuPont）。

## 参见

  - [美國聯合碳化物](../Page/美國聯合碳化物.md "wikilink")

  - [上海陶氏中心](../Page/上海陶氏中心.md "wikilink")

  - [美國陶氏益農](../Page/美國陶氏益農.md "wikilink")

  - [羅門哈斯](../Page/羅門哈斯.md "wikilink")

  - [傑出僱主獎](../Page/傑出僱主獎.md "wikilink")

  - [奧運會全球合作夥伴](../Page/奧運會全球合作夥伴.md "wikilink")

  - [水處理](../Page/水處理.md "wikilink")

  - [1,2-二溴-3-氯丙烷](../Page/1,2-二溴-3-氯丙烷.md "wikilink")

  - [橙劑](../Page/橙劑.md "wikilink")

  - [極光行動](../Page/極光行動.md "wikilink")

  - [環氧樹脂](../Page/環氧樹脂.md "wikilink")

  - [博帕爾事件](../Page/博帕爾事件.md "wikilink")

  - [興農企業](../Page/興農企業.md "wikilink")：台灣主要的農牧業用藥劑、[肥料等為主打產品](../Page/肥料.md "wikilink")，是僅次於交由民營的[台灣肥料股份有限公司的私人企業](../Page/台灣肥料股份有限公司.md "wikilink")

  - ：由海水中製造[溴元素的方式](../Page/溴.md "wikilink")。

## 外部链接

  - [陶氏化学（全球）官方網站](http://www.dow.com)
  - [陶氏化学（大中華地區）官方網站](http://www.dow.com.cn)
  - [陶氏化学歷史](http://www.dow.com/about/aboutdow/history/1890s.htm)
  - [新陶氏坚定可持续发展之路](http://www.cheminfo.gov.cn/Dow/Detail.aspx?id=280)
  - [陶氏的可持续发展之道](https://web.archive.org/web/20121015032159/http://www.cheminfo.gov.cn/dow/Detail.aspx?id=394)
  - [陶氏创新天地宽](http://www.cheminfo.gov.cn/Dow/Detail.aspx?id=315)
  - [Innovation](https://web.archive.org/web/20111007013546/http://www.dow.com/innovation/)

[陶氏化学](../Category/陶氏化学.md "wikilink")
[Category:塑料公司](../Category/塑料公司.md "wikilink")
[Category:美國化學工業公司](../Category/美國化學工業公司.md "wikilink")
[Category:1897年成立的公司](../Category/1897年成立的公司.md "wikilink")