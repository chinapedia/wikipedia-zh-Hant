**陸建瀛**（），字**立夫**、**仲白**，\[1\]湖北[沔陽人](../Page/沔陽.md "wikilink")，[清朝政治人物](../Page/清朝.md "wikilink")，累官兩江總督，[太平軍破](../Page/太平軍.md "wikilink")[江寧](../Page/江寧.md "wikilink")（今[南京](../Page/南京.md "wikilink")）時身死。

## 生平

[道光二年](../Page/道光.md "wikilink")（1822年）[進士](../Page/進士.md "wikilink")。選[庶吉士](../Page/庶吉士.md "wikilink")，[散館授](../Page/散館.md "wikilink")[編修](../Page/編修.md "wikilink")，直[上書房](../Page/上書房.md "wikilink")，洊遷[中允](../Page/中允.md "wikilink")。大考，擢[侍講](../Page/侍講.md "wikilink")，轉[侍讀](../Page/侍讀.md "wikilink")。道光二十年（1840年），出為[直隸](../Page/直隶省.md "wikilink")[天津道](../Page/天津道.md "wikilink")，累擢[布政使](../Page/布政使.md "wikilink")。\[2\]道光二十六年（1846年）升雲南[巡撫](../Page/巡撫.md "wikilink")，同年調[江蘇巡撫](../Page/江蘇巡撫.md "wikilink")。[道光二十九年](../Page/道光.md "wikilink")（1849年）四月任[兩江總督](../Page/兩江總督.md "wikilink")，\[3\]

[咸豐三年](../Page/咸丰_\(年号\).md "wikilink")（1853年）二月，太平軍攻江寧。陆建瀛于[仪凤门督战](../Page/仪凤门_\(南京\).md "wikilink")，但夜间回宿总督府。太平军半夜以穴地攻城之法攻破仪凤门，陆建瀛即乘“四人绿呢小舆”企图逃入[江宁满城](../Page/江宁满城.md "wikilink")（即江宁驻防八旗所驻守的内城，原明朝迁都之前之皇城），遭江宁将军[祥厚拒绝入城](../Page/祥厚.md "wikilink")。陆建瀛不得已返回仪凤门督战，半路遭遇太平军一支数百人的突击队。“轿夫四散”，一个官员企图背着陆建瀛逃走（原文“将负而趋”），但未能成功，陆被太平军杀死於兩江總督府內小校場。而《清史稿》中则称“建瀛易服走，为寇所戕”。\[4\]\[5\]

## 後事

在江寧城破前，江寧將軍祥厚等人上疏彈劾陸建瀛指揮作戰無方，咸豐帝下令把陸建瀛革職治罪，可是公文未送到江寧而陸建瀛已戰死，皇帝知道後恢復了他的總督銜，依例議恤，還其家產，後來因為御史[方俊反對而撤回恤典](../Page/方俊.md "wikilink")。\[6\]

陸建瀛的兒子[陸鍾漢後來官至江苏](../Page/陸鍾漢.md "wikilink")[知府](../Page/知府.md "wikilink")。[咸丰十年](../Page/咸丰.md "wikilink")，在军押送粮饷，遇太平軍於[江阴](../Page/江阴.md "wikilink")，戰歿，赠[太仆寺卿](../Page/太仆寺.md "wikilink")。\[7\]

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [資料連結](http://archive.ihp.sinica.edu.tw/ttscgi/ttsquery?0:0:mctauac:TM%3D%B3%B0%AB%D8%C3s)，中研院史語所明清檔案工作室。

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林院編修](../Category/清朝翰林院編修.md "wikilink")
[Category:清朝翰林院侍讀](../Category/清朝翰林院侍讀.md "wikilink")
[Category:清朝天津道](../Category/清朝天津道.md "wikilink")
[Category:清朝直隸布政使](../Category/清朝直隸布政使.md "wikilink")
[Category:清朝雲南巡撫](../Category/清朝雲南巡撫.md "wikilink")
[Category:清朝江蘇巡撫](../Category/清朝江蘇巡撫.md "wikilink")
[Category:清朝兩江總督](../Category/清朝兩江總督.md "wikilink")
[Category:清朝戰爭身亡者](../Category/清朝戰爭身亡者.md "wikilink")
[Category:仙桃人](../Category/仙桃人.md "wikilink")
[J](../Category/陸姓.md "wikilink")

1.

2.  《[清史稿](../Page/清史稿.md "wikilink")》列傳一百八十四

3.  《清史稿》本紀十九

4.  《清史稿》列傳二百六十二

5.  据南京大学出版社《太平天国战争全史》转引自张汝南《金陵省难记略》

6.
7.