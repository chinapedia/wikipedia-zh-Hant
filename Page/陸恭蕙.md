**陸恭蕙**（，），前[香港特別行政區](../Page/香港特別行政區.md "wikilink")[環境局副局長](../Page/環境局.md "wikilink")、前[民權黨主席及創辦人](../Page/民權黨.md "wikilink")，[思匯政策研究所創辦人及](../Page/思匯政策研究所.md "wikilink")[保護海港協會主席](../Page/保護海港協會.md "wikilink")，於1992年至2000年期間擔任[立法局及](../Page/香港立法局.md "wikilink")[立法會議員](../Page/立法會議員.md "wikilink")。現任[香港科技大學環境及可持續發展學部首席發展顧問](../Page/香港科技大學.md "wikilink")\[1\]。

## 生平

1956年在香港出生，幼年於[香港英童中學就讀](../Page/香港英童中學.md "wikilink")，至14歲時被送到英國的Bedford中學升學。1979年，她在英國[赫爾大學取得法律系學士學位](../Page/赫爾大學.md "wikilink")（後期還在[香港城市大學取得中國法及比較法法學碩士學位](../Page/香港城市大學.md "wikilink")），但她並沒有加入法律界，反而進入了美資的期貨交易公司飛利浦兄弟香港有限公司（）當練習生，從事金屬期貨買賣；後更成為公司駐北京的董事總經理，至1991年因事離任\[2\]
。

1992年，陸恭蕙在[于品海邀請下出任](../Page/于品海.md "wikilink")[智才發展部的總監](../Page/智才發展部.md "wikilink")，負責引入「Loft」零售店和九號貨櫃碼頭的投標工作\[3\]。同年，她獲時任港督[彭定康委任](../Page/彭定康.md "wikilink")[立法局議員](../Page/立法局議員.md "wikilink")，展開其政治生涯\[4\]。任內，她推動通過《新界土地（豁免）條例》，成功令女性原居民也獲得[繼承權](../Page/繼承權.md "wikilink")；由於這破壞了[新界原居民的傳統](../Page/新界原居民.md "wikilink")，曾有男性原居民威脅把她強姦\[5\]。1995年，她參加[立法局地方直選](../Page/1995年香港立法局選舉.md "wikilink")，並當選取得港島中議席。1997年，她在立法局提出私人條例草案，通過《[保護海港條例](../Page/保護海港條例.md "wikilink")》；此外，她在年內建立了[民權黨](../Page/民權黨.md "wikilink")，兼任黨主席。

2000年4月，陸恭蕙宣佈不再參選立法會，退出政壇；同年12月，成立[思匯政策研究所](../Page/思匯政策研究所.md "wikilink")，並任行政總監。由於陸恭蕙與商界關係友好，思匯成立初期，已得到[新鴻基地產慈善基金](../Page/新鴻基地產.md "wikilink")、美國Rockefeller
Brothers Fund等十間機構捐款\[6\]。

2006年加入[香港交易所任董事](../Page/香港交易所.md "wikilink")。2009年獲委任為非官守太平紳士。

2012年9月12日，[香港特別行政區政府公佈委任陸恭蕙出任](../Page/香港特別行政區政府.md "wikilink")[環境局副局長](../Page/環境局.md "wikilink")；思匯同日公布由[葉溵溵接替陸出任行政總監](../Page/葉溵溵.md "wikilink")。陸表示自己從沒有申請副局長職位，是特首[梁振英親自邀請](../Page/梁振英.md "wikilink")，才答應出任；她加入政府的目的是做好環保工作，希望與[黃錦星合作可以做到事](../Page/黃錦星.md "wikilink")\[7\]。

## 家庭

陸恭蕙的母親[莫綺萍為](../Page/莫綺萍.md "wikilink")[莫仕揚家族成員](../Page/莫仕揚家族.md "wikilink")，是[太古洋行](../Page/太古洋行.md "wikilink")[買辦](../Page/買辦.md "wikilink")[莫幹生的孫女](../Page/莫幹生.md "wikilink")；父親[陸孝儀則是棉花商人](../Page/陸孝儀.md "wikilink")，堂大伯[陸孝佩為](../Page/陸孝佩.md "wikilink")[公和建築創辦人](../Page/公和建築.md "wikilink")，與新地主席[郭炳聯為姻親](../Page/郭炳聯.md "wikilink")。陸恭蕙的父母在她六歲那年離婚，陸母後來改嫁丹麥人Jens
Munk，而陸父則在美國再婚育有兩子，名叫陸恭元、[陸恭和](../Page/陸恭和.md "wikilink")\[8\]。

陸恭蕙曾與美國人[方堡文結婚](../Page/方堡文.md "wikilink")，但婚姻只維持了八個月\[9\]。後與[何力勤成為伴侶](../Page/何力勤.md "wikilink")，二人在2005年於美國借[代母誕下一名女嬰](../Page/代母.md "wikilink")\[10\]。梁振英政府完結，陸恭蕙離開政府，隨同家庭在香港和[洛杉磯兩邊走](../Page/洛杉磯.md "wikilink")\[11\]。

## 公職

  - [香港科技大學校董會成員](../Page/香港科技大學.md "wikilink")（1999-2006）
  - [思匯政策研究所行政總監](../Page/思匯政策研究所.md "wikilink")
  - [保護海港協會主席](../Page/保護海港協會.md "wikilink")
  - [健康空氣行動主席](../Page/健康空氣行動.md "wikilink")
  - [香港交易所董事](../Page/香港交易所.md "wikilink")，委員會小組成員（2005／2006年度）
  - [立法會議員](../Page/立法會議員.md "wikilink")（1992年10月－2000年9月）
  - 前立法會環境事務委員會主席

## 著作

  - 《地下陣線：中共在香港的歷史》[1](http://cht.civic-exchange.org/wp/underground_front_cn/#more-12369)

  - CCP in HK 《共產黨在香港》
    [2](http://books.google.com.hk/books/about/Underground_Front.html?id=ZZpCfvK3QhcC&redir_esc=y)
    isbn=9888028944

## 參考

[Category:前香港立法局議員](../Category/前香港立法局議員.md "wikilink")
[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:香港女性政治人物](../Category/香港女性政治人物.md "wikilink")
[Category:香港環保人士](../Category/香港環保人士.md "wikilink")
[Category:OBE勳銜](../Category/OBE勳銜.md "wikilink")
[Category:香港十大傑出青年](../Category/香港十大傑出青年.md "wikilink")
[Category:香港天主教徒](../Category/香港天主教徒.md "wikilink")
[Category:港島中學校友](../Category/港島中學校友.md "wikilink")
[Category:香港城市大學校友](../Category/香港城市大學校友.md "wikilink")
[Category:香港聖保祿學校校友](../Category/香港聖保祿學校校友.md "wikilink")
[Category:赫爾大學校友](../Category/赫爾大學校友.md "wikilink")
[Category:前香港立法會議員](../Category/前香港立法會議員.md "wikilink")
[Category:香港科技大學教授](../Category/香港科技大學教授.md "wikilink")
[K](../Category/陸姓.md "wikilink")

1.  [香港科技大學](http://www.envr.ust.hk/our-division/people/faculty-staff/cloh.html)

2.

3.

4.
5.
6.
7.

8.
9.
10.

11. [清心直說](../Page/清心直說#2018年.md "wikilink")，[明珠台](../Page/明珠台.md "wikilink")，2018-06-05，英語