**陸桂祥**，號竹天，本籍[江蘇松江人](../Page/江蘇.md "wikilink")，[交通大學畢業](../Page/交通大學.md "wikilink")。他於1945年10月，從[中國大陸跟隨](../Page/中國大陸.md "wikilink")[國民政府前往台灣](../Page/國民政府.md "wikilink")，擔任[台灣省行政長官公署官員](../Page/台灣省行政長官公署.md "wikilink")。1946年2月16日，他接任[連震東](../Page/連震東.md "wikilink")，擔任[臺北縣縣長](../Page/臺北縣縣長.md "wikilink")。陸桂祥擔任臺北縣縣長期間，被台灣[仕紳指控疑似發生私吞五億元台幣的重大貪污案件](../Page/仕紳.md "wikilink")，由《和平日報》臺灣版採訪課長[丁文治在新聞揭發](../Page/丁文治.md "wikilink")\[1\]，因此於翌年即被撤職查辦。部分文獻指出，該案件亦為[二二八事件的遠因之一](../Page/二二八事件.md "wikilink")。\[2\]

## 注釋

<div class="references-small">

<references />

</div>

[L](../Category/台北縣縣長.md "wikilink")
[Category:國立交通大學校友](../Category/國立交通大學校友.md "wikilink")
[Category:松江人](../Category/松江人.md "wikilink")
[Category:陸姓](../Category/陸姓.md "wikilink")
[Category:臺灣貪污犯](../Category/臺灣貪污犯.md "wikilink")
[Category:二二八事件相關人物](../Category/二二八事件相關人物.md "wikilink")

1.
2.  [二二八網站](http://www.228.org.tw/upload_file/seminar_90_04.pdf?PHPSESSID=be30f845edbcba1c6089a344052b8038#search=%22%E9%99%B8%E6%A1%82%E7%A5%A5%22)