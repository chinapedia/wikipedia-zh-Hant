**陸羽茶室槍殺案**是[香港少有的一宗因](../Page/香港.md "wikilink")[商業利益而引發的](../Page/商業.md "wikilink")[買兇殺人事件](../Page/買兇殺人.md "wikilink")。

案發於2002年11月30日\[1\][香港](../Page/香港島.md "wikilink")[中環](../Page/中環.md "wikilink")[陸羽茶室](../Page/陸羽茶室.md "wikilink")，品茗熟客及香港商人[林漢烈被](../Page/林漢烈.md "wikilink")[職業殺手近距離連開兩槍射殺](../Page/職業殺手.md "wikilink")。

事後買兇者及行兇者一幫共八人，在[深圳被捕](../Page/深圳.md "wikilink")，由[深圳市中級人民法院宣判](../Page/深圳市中級人民法院.md "wikilink")，殺手[楊文判處](../Page/楊文.md "wikilink")[死刑](../Page/死刑.md "wikilink")(退伍[軍人](../Page/解放軍.md "wikilink"))，5人徒刑，其餘2人無罪釋放。其中前[亞視](../Page/亞視.md "wikilink")[演員及](../Page/演員.md "wikilink")[中國大陸](../Page/中國大陸.md "wikilink")[房地產](../Page/房地產.md "wikilink")[企業家](../Page/企業家.md "wikilink")[楊家安原來是主謀之一](../Page/楊家安.md "wikilink")。\[2\]\[3\]

## 人物關係

  - [林漢烈](../Page/林漢烈.md "wikilink")：[高球](../Page/高爾夫球.md "wikilink")[會所會藉包銷人](../Page/會所.md "wikilink")，因2億元[債務](../Page/債務.md "wikilink")，曾經申請將對方的業務[清盤](../Page/清盤.md "wikilink")，可惜開庭前即被槍殺。\[4\]
  - [楊家安](../Page/楊家安.md "wikilink")：香港[元朗](../Page/元朗.md "wikilink")[家安花園地產發展商](../Page/家安花園.md "wikilink")，前[麗的電視藝員及](../Page/麗的電視藝員.md "wikilink")[龍虎武師](../Page/龍虎武師.md "wikilink")，喜愛打[高球識朋友](../Page/高球.md "wikilink")；他被指是槍殺案幕後主腦，被判[無期徒刑](../Page/無期徒刑.md "wikilink")。<ref>

[林漢烈命案轟動中港](http://the-sun.on.cc/cnt/news/20110813/00407_014.html)</ref>

  - [劉一賢](../Page/劉一賢.md "wikilink")、[謝冰](../Page/謝冰.md "wikilink")：香港人，兩人被指是楊家安的共犯；被判無期徒刑。
  - 楊文：[湖南益陽沅江市人](../Page/湖南.md "wikilink")，[廣東](../Page/廣東.md "wikilink")[下崗戶](../Page/下崗.md "wikilink")，退役[解放軍](../Page/解放軍.md "wikilink")[官兵](../Page/官兵.md "wikilink")；他涉嫌槍殺林漢烈，被判[死刑](../Page/死刑.md "wikilink")。
  - 張志新：幫兇及放哨人；被判[有期徒刑十三年](../Page/有期徒刑.md "wikilink")。
  - 鄔衛吾：楊文的[朋友](../Page/朋友.md "wikilink")；被判有期徒刑三年。
  - 何可夫：楊家安的姨甥；因證據不足，被判無罪釋放。
  - 崔銘揚：楊家安的[司機](../Page/司機.md "wikilink")；因證據不足，被判無罪釋放。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:香港命案](../Category/香港命案.md "wikilink")
[Category:2002年香港](../Category/2002年香港.md "wikilink")
[Category:中华人民共和国已执行死刑命案](../Category/中华人民共和国已执行死刑命案.md "wikilink")
[Category:2002年11月](../Category/2002年11月.md "wikilink")

1.  [1](http://appledaily.atnext.com/template/apple/sec_main.cfm?iss_id=20021201)
2.  [公司主席楊家安涉陸羽槍殺案主腦](https://web.archive.org/web/20061116224633/http://hk.news.yahoo.com/061025/12/1v88e.html)
3.  [2007年7月10日，陸羽槍殺案，楊家安囚終身](https://web.archive.org/web/20070713104707/http://hk.news.yahoo.com/070710/60/2b7o3.html)
4.  [林漢烈以椰菜娃娃生意起家](http://hk.apple.nextmedia.com/news/art/20061026/6445024)