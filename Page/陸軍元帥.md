**陸軍元帥**是一个军官等级，是在[英國](../Page/英國.md "wikilink")、[德國及一些國家軍隊中最高軍階的委任軍官](../Page/德國.md "wikilink")，相當於[美國陸軍的五星上将位階](../Page/美國陸軍.md "wikilink")。\[1\]

## 各国陸軍元帥

### 阿富汗

  - [纳斯鲁拉汗](../Page/纳斯鲁拉汗.md "wikilink") (1875–1920)
  - 2004 - [穆罕默德·法希姆](../Page/穆罕默德·法希姆.md "wikilink") (b. 1957)

### 阿尔巴尼亚

  - 1928年9月1日 - [索古一世](../Page/索古一世.md "wikilink") (1895–1961)

### 澳大利亚

  - 1927年 -
    [威廉·里德尔·伯德伍德，第一代伯德伍德男爵](../Page/威廉·里德尔·伯德伍德，第一代伯德伍德男爵.md "wikilink")
    (1865–1951)
  - 1950年6月8日 - [托马斯·布莱梅爵士](../Page/托马斯·布莱梅.md "wikilink") (1884–1951)
  - 1954年4月1日 - [菲利普亲王 (爱丁堡公爵)](../Page/菲利普亲王_\(爱丁堡公爵\).md "wikilink")
    ((b. 1921)

### 奥地利/奥匈帝国

### 白俄罗斯

  - [亚历山大·卢卡申科](../Page/亚历山大·卢卡申科.md "wikilink") (b. 1954)

### 巴西

參見：

### 柬埔寨

  - 1971 - [朗诺](../Page/朗诺.md "wikilink") (1913–1985)
  - [诺罗敦·拉那烈亲王](../Page/諾羅敦·拉那烈.md "wikilink") (b. 1944)
  - 2009 - [韩桑林](../Page/韩桑林.md "wikilink") (b. 1934)\[2\]
  - 2009 - [谢辛](../Page/谢辛.md "wikilink") (b. 1932)\[3\]
  - 2009 - [洪森](../Page/洪森.md "wikilink") (b. 1952)\[4\]

### 中非帝国

  - [让-贝德尔·博卡萨](../Page/让-贝德尔·博卡萨.md "wikilink") (1921–1986)

### 中华民国

  - 1911 - [黃興](../Page/黃興.md "wikilink")(1874–1916)
  - 1913 - [袁世凯](../Page/袁世凯.md "wikilink") (1859–1916)\[5\]
  - 1917 - [孙中山](../Page/孙中山.md "wikilink") (1866–1925)\[6\]
  - 1921 - [陆荣廷](../Page/陆荣廷.md "wikilink") (1859–1928)
  - 1921 - [唐继尧](../Page/唐继尧.md "wikilink") (1883–1927)
  - [吴佩孚](../Page/吴佩孚.md "wikilink") (1874–1939)
  - [孙传芳](../Page/孙传芳.md "wikilink") (1885–1935)
  - 1927 - [张作霖](../Page/张作霖.md "wikilink") (1875–1928)\[7\]
  - 1935 - [蒋中正](../Page/蒋中正.md "wikilink") (1887–1975)\[8\]

### 中华人民共和国

### 克罗地亚

  - 1995 - [弗拉尼奥·图季曼](../Page/弗拉尼奥·图季曼.md "wikilink") (1922–1999)

### 克羅埃西亞獨立國

  - 1941 - [斯拉夫科·克瓦特尼克](../Page/斯拉夫科·克瓦特尼克.md "wikilink") (1876–1947)

### 埃及王国

  - [阿拔斯一世·希尔米帕夏](../Page/阿拔斯一世_\(埃及\).md "wikilink") (1813–1854)
  - [易卜拉欣帕夏](../Page/易卜拉欣帕夏.md "wikilink") (1789–1848)
  - [叶海亚·曼苏尔·耶根](../Page/叶海亚·曼苏尔·耶根.md "wikilink") (1837–1913)
  - [霍雷肖·赫伯特·基钦纳](../Page/赫伯特·基奇纳，第一代基奇纳伯爵.md "wikilink") (1850–1916)
  - 1914年12月20日 - [侯赛因·卡迈勒苏丹](../Page/侯赛因·卡迈勒_\(埃及\).md "wikilink")
    (1853–1917)
  - [福阿德一世国王](../Page/福阿德一世.md "wikilink") (1868–1936)
  - [阿齐兹·阿里·米尔里](../Page/阿齐兹·阿里·米尔里.md "wikilink") (1879–1965)
  - [法鲁克一世国王](../Page/法鲁克一世.md "wikilink") (1920–1965)
  - 1949 - [约旦国王阿卜杜拉一世](../Page/阿卜杜拉一世_\(约旦\).md "wikilink") (1882–1951)
  - 1952年7月26日 - [福阿德二世国王](../Page/福阿德二世.md "wikilink") (b. 1952)
  - 1955年2月21日 - [约旦国王侯赛因](../Page/侯赛因·本·塔拉勒.md "wikilink") (1935–1999)

### 埃及共和国

  - [阿卜杜勒·哈基姆·阿梅尔](../Page/阿卜杜勒·哈基姆·阿梅尔.md "wikilink") (1919–1967)
  - 1973年11月 - [艾哈迈德·伊斯迈尔·阿里](../Page/艾哈迈德·伊斯迈尔·阿里.md "wikilink")
    (1917–1974)
  - [阿卜杜勒·加尼·贾马西](../Page/阿卜杜勒·加尼·贾马西.md "wikilink") (1921–2003)
  - [福阿德·齐克里](../Page/福阿德·齐克里.md "wikilink") (1923–1983)
  - [艾哈迈德·巴达维](../Page/艾哈迈德·巴达维.md "wikilink") (1927–1981)
  - [穆罕默德·阿利·法赫米](../Page/穆罕默德·阿利·法赫米.md "wikilink") (1920–1999)
  - [阿布德·哈利姆·阿布·加扎拉](../Page/阿布德·哈利姆·阿布·加扎拉.md "wikilink") (1930–2008)
  - 1991 - [穆罕默德·侯赛因·坦塔维](../Page/穆罕默德·侯赛因·坦塔维.md "wikilink") (b. 1935)
  - 2014 - [阿卜杜勒-法塔赫·塞西](../Page/阿卜杜勒-法塔赫·塞西.md "wikilink") (b. 1954)

### 衣索比亞

  - 1934 -  (1897–1960)
  - [海爾·塞拉西一世](../Page/海爾·塞拉西一世.md "wikilink") (1892–1975)

### 芬兰

  - 1933 - [卡尔·古斯塔夫·埃米尔·曼纳海姆](../Page/卡尔·古斯塔夫·埃米尔·曼纳海姆.md "wikilink")
    (1867–1951)

### 法国

### 印度

  - 1973年1月3日 - [萨姆·马内克肖](../Page/萨姆·马内克肖.md "wikilink") (1914–2008)
  - 1983年1月14日 - [科丹德拉·马达帕·卡里亚帕](../Page/科丹德拉·马达帕·卡里亚帕.md "wikilink")
    (1899–1993)

### 印度尼西亚

  - 1945年 -  (1916–1950)
  - 1985年 - [苏哈托](../Page/苏哈托.md "wikilink") (1921–2008)
  - 2000年 - [阿卜杜尔·哈里斯·纳苏蒂安](../Page/阿卜杜尔·哈里斯·纳苏蒂安.md "wikilink")
    (1918–2000)

### 日本

### 巴基斯坦

[巴基斯坦总统](../Page/巴基斯坦总统.md "wikilink")[穆罕默德·阿尤布·汗自任为陆军元帅](../Page/阿尤布·汗.md "wikilink")。

### 菲律宾

美国的[道格拉斯·麦克阿瑟少将](../Page/道格拉斯·麦克阿瑟.md "wikilink")1936年8月24日担任菲律宾政府军事顾问时被[菲律宾总统](../Page/菲律宾总统.md "wikilink")[曼努埃尔·奎松授予菲律宾陆军元帅军衔](../Page/曼努埃尔·奎松.md "wikilink")。

### 约旦

  - 1948年 - [阿卜杜拉一世](../Page/阿卜杜拉一世.md "wikilink") (1882–1951)

  - 1951年7月20日 - [塔拉勒·伊本·阿卜杜拉](../Page/塔拉勒·伊本·阿卜杜拉.md "wikilink")
    (1909–1972)

  - 1952年8月11日 - [胡笙國王](../Page/胡笙國王.md "wikilink") (1935–1999)

  - (1914–2001)

  -
  - 阿卜杜勒·哈菲兹·卡布纳

  - 1999年2月7日 - [阿卜杜拉二世](../Page/阿卜杜拉二世.md "wikilink") (b. 1962)

  - 2005年 -  (d.2009)

### 摩洛哥

  - 1970年11月17日 -  (1897–1975)

### 莫桑比克

  - [萨莫拉·马谢尔](../Page/萨莫拉·马谢尔.md "wikilink") (1933–1986)

### 新西兰

  - 1977年6月11日 - [菲利普亲王 (爱丁堡公爵)](../Page/菲利普亲王_\(爱丁堡公爵\).md "wikilink")
    (b. 1921)

### 斯里蘭卡

  - 2015年3月22日 -  (b. 1950)

### 泰國

參見：[泰國陸軍元帥](../Page/w:en:Field_marshal_\(Thailand\).md "wikilink")

### 土耳其

土耳其元帅（mareşal）起于[奥斯曼帝国时期](../Page/奥斯曼帝国.md "wikilink")。在[波斯称](../Page/波斯.md "wikilink")"مشير"
*(müşir)*\[9\]土耳其立国后，只有两人获得元帅称号：国父[凯末尔·阿塔蒂尔克和参谋长](../Page/穆斯塔法·凯末尔·阿塔蒂尔克.md "wikilink")[費夫齊·恰克馬克](../Page/費夫齊·恰克馬克.md "wikilink")。

### 英国

### 美国

美国只有[五星上将军衔](../Page/五星上将.md "wikilink")，没有使用元帅军衔。只有[道格拉斯·麦克阿瑟将军在](../Page/道格拉斯·麦克阿瑟.md "wikilink")1936年8月24日至1937年12月31日被菲律宾政府授予军衔\[10\]。1944年12月14日美国国会正式设立五星上将军衔（參見[美國國會法案](../Page/美國國會法案.md "wikilink")《[78–482](../Page/:en:s:Public_Law_78-482.md "wikilink")》），[乔治·马歇尔在两日](../Page/乔治·马歇尔.md "wikilink")（16日）后成为首位五星上将\[11\]，[道格拉斯·麦克阿瑟則於](../Page/道格拉斯·麦克阿瑟.md "wikilink")18日晉陞\[12\]\[13\]。

### 德国

陆军元帅(德语：Generalfeldmarschall)是德国陆军最高军衔。尽管此军衔自1631年以来一直以不同的名称存在，1870年普鲁士王子弗里德里希·卡尔和腓特烈三世皇帝重建陆军元帅军衔，目的是使他们的军衔比其他将军更高，此后一直是德国陆军最高军衔，直到1945年废除为止。\[14\]

### 苏联

### 馬來西亞

  - 1957年8月31日- [端姑阿都拉曼](../Page/端姑阿都拉曼.md "wikilink")（1885-1960）
  - 1960年4月14日- （1898至1960年）
  - 1960年9月21日- （1920-2000）
  - 1965年9月21日- （1907年至1979年）
  - 1970年9月21日-
    [蘇丹端姑·阿布都·哈林](../Page/蘇丹端姑·阿布都·哈林.md "wikilink")（1927年B-2017年D）
  - 1975年9月21日- 蘇丹（1917年至1979年）
  - 1979年3月29日- 蘇丹艾哈邁德·沙阿（B 1930年）。
  - 1984年4月26日-
    [蘇丹馬末·依斯干達](../Page/蘇丹馬末·依斯干達.md "wikilink")（1932年至2010年）
  - 1989年4月26日- 蘇丹（1928至2014年）
  - 1994年4月26日- [端姑·惹化](../Page/端姑·惹化.md "wikilink")（1922年至2008年）
  - 1999年4月26日- [蘇丹沙拉胡丁](../Page/蘇丹沙拉胡丁.md "wikilink")（1926-2001）
  - 2001年12月13日- [端姑·賽西拉如丁](../Page/端姑·賽西拉如丁.md "wikilink")（; B 1943）
  - 2006年12月13日- [端姑·米占再納阿比丁](../Page/端姑·米占再納阿比丁.md "wikilink")（; B
    1962）
  - 2011年12月13日-
    [蘇丹端姑·阿布都·哈林](../Page/蘇丹端姑·阿布都·哈林.md "wikilink")（1927年B-2017年D）
  - 2016年12月13日- [苏丹莫哈末五世](../Page/苏丹莫哈末五世.md "wikilink")（1969年B）

### 蒙古

  - [霍尔洛·乔巴山](../Page/霍尔洛·乔巴山.md "wikilink") (1895–1952)
  - [格勒格道尔吉·德米德](../Page/格勒格道尔吉·德米德.md "wikilink") (1900–1937)
  - [尤睦佳·澤登巴爾](../Page/尤睦佳·澤登巴爾.md "wikilink") (1916–1991)

### 朝鲜

  - 1950s - [金日成](../Page/金日成.md "wikilink") (1912–1994)\[15\]
  - 1992 - [吴振宇](../Page/吴振宇_\(朝鮮\).md "wikilink") (1917–1995)
  - 1992 - [金正日](../Page/金正日.md "wikilink") (1941–2011)\[16\]
  - 1995 - [李乙雪](../Page/李乙雪.md "wikilink") (1921 - 2015)
  - 1995 - [崔光](../Page/崔光_\(朝鲜\).md "wikilink") (1917–1997)
  - 2012年7月 - [金正恩](../Page/金正恩.md "wikilink") (b. 1983)\[17\]
  - 2016年4月 - [金永春](../Page/金永春.md "wikilink")： (b. 1936)
  - 2016年4月 - [玄哲海](../Page/玄哲海.md "wikilink")： (b. 1934)

### 韩国

  - [白善烨](../Page/白善烨.md "wikilink") (b.
    1920)，2009年[李明博政府宣称](../Page/李明博.md "wikilink")“白善烨是韩国军队的第一位上将，他在[韩战时期曾任师长](../Page/韩战.md "wikilink")、军长等要职，在反攻[金日成军的北进战役时](../Page/金日成.md "wikilink")，他带兵第一个杀入[平壤](../Page/平壤.md "wikilink")。”而决定推举白善烨为韩国第一位名誉元帅（5星将军）\[18\]。

### 沙烏地阿拉伯

  - 1991年 - [哈立德·本·蘇丹](../Page/哈立德·本·蘇丹.md "wikilink")（; B 1949）

  - （B，1939）

### 蘇丹

  - [加法爾·尼邁里](../Page/加法爾·尼邁里.md "wikilink") (1930–2009)

  - (b. 1934)

  - [奧馬爾·巴希爾](../Page/奧馬爾·巴希爾.md "wikilink") (b. 1944)

### 乌干达

  - [乌干达总统](../Page/乌干达总统.md "wikilink")[伊迪·阿明·达达在](../Page/伊迪·阿明·达达.md "wikilink")1971年政变上台后，自封为陆军元帅。

### 利比亚

  - [哈利法·贝加斯姆·哈夫塔尔](../Page/哈利法·贝加斯姆·哈夫塔尔.md "wikilink")，2016年9月晋升。

### 葡萄牙

  - 1809 -
    [威廉·卡尔·贝雷斯福德，第一代贝雷斯福德子爵](../Page/威廉·卡尔·贝雷斯福德，第一代贝雷斯福德子爵.md "wikilink")
    (1768–1854)
  - [阿瑟·韋爾斯利，第一代威靈頓公爵](../Page/阿瑟·韋爾斯利，第一代威靈頓公爵.md "wikilink")
    (1769–1852)
  - 1927 - [戈麦斯·达科斯塔](../Page/戈麦斯·达科斯塔.md "wikilink") (1863–1929)
  - 1948 - [安东尼奥·奥斯卡·卡尔莫纳](../Page/安东尼奥·奥斯卡·卡尔莫纳.md "wikilink")
    (1869–1951)
  - 1958 - [弗朗西斯科·克拉维罗·洛佩斯](../Page/弗朗西斯科·克拉维罗·洛佩斯.md "wikilink")
    (1894–1964)
  - 1981 - [安东尼奥·德·斯皮诺拉](../Page/安东尼奥·斯皮诺拉.md "wikilink") (1910–1996)

## 其他国家

  - [希腊元帅](../Page/希腊.md "wikilink")（Stratarches、Στρατάρχης）

  - [意大利陆军元帅](../Page/意大利陆军元帅.md "wikilink")

  - [以色列元帅](../Page/以色列.md "wikilink")（Rav-Aluf）

  -
  - [马来西亚陆军元帅](../Page/马来西亚陆军元帅.md "wikilink")（Yang di-Pertuan Agong）

  - [秘鲁元帅](../Page/秘鲁.md "wikilink")

  - [波兰元帅](../Page/波兰元帅.md "wikilink")

  - [罗马尼亚元帅](../Page/罗马尼亚.md "wikilink")（Mareşal）

  - [俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")[陆军元帅](../Page/陆军元帅.md "wikilink")（General
    field marshal）

  - [俄罗斯联邦元帅](../Page/俄罗斯联邦.md "wikilink")

  - [南非陆军元帅](../Page/南非.md "wikilink")[揚·史末資](../Page/揚·史末資.md "wikilink")

  - [塞尔维亚元帅](../Page/塞尔维亚.md "wikilink")（Vojvoda）

  - （Fältmarskalk）

  - [泰国元帅](../Page/泰国.md "wikilink")（Chom Phon）

## 各国陸軍元帥照片

Chiang Kai-shek（蔣中正）.jpg|[蒋中正](../Page/蒋中正.md "wikilink") Zhu
De.jpg|thumb|[朱德](../Page/朱德.md "wikilink") Liu
Bocheng.jpg|[刘伯承](../Page/刘伯承.md "wikilink") Talvisoda
marski-2.jpg|[曼纳海姆](../Page/卡尔·古斯塔夫·埃米尔·曼纳海姆.md "wikilink") Field
Marshal Sarit Dhanarajata.jpg|[沙立·他那叻](../Page/沙立·他那叻.md "wikilink")
Ataturk29.JPG|[凯末尔·阿塔蒂尔克](../Page/穆斯塔法·凯末尔·阿塔蒂尔克.md "wikilink") INF3-11
General Sir Harold Alexander Artist William Little
1939-1946.jpg|[哈羅德·亞歷山大](../Page/哈羅德·亞歷山大.md "wikilink")
General Sir Bernard Montgomery in England, 1943 TR1037
(cropped).jpg|[伯納德·勞·蒙哥馬利](../Page/伯納德·勞·蒙哥馬利.md "wikilink")
Douglas MacArthur smoking his corncob
pipe.jpg|[道格拉斯·麥克阿瑟](../Page/道格拉斯·麥克阿瑟.md "wikilink")
Dwight D. Eisenhower as General of the Army
crop.jpg|[德懷特·艾森豪](../Page/德懷特·艾森豪.md "wikilink")
Bundesarchiv Bild 146-1985-013-07, Erwin
Rommel-2.jpg|thumb|[埃爾溫·隆美爾](../Page/埃爾溫·隆美爾.md "wikilink")
Bundesarchiv Bild 183-H01757, Erich von
Manstein.jpg|thumb|[埃里希·馮·曼施坦因](../Page/埃里希·馮·曼施坦因.md "wikilink")
Zhukov-LIFE-1944-1945.jpg|thumb|[朱可夫](../Page/朱可夫.md "wikilink")
Aleksandr Vasilevsky 4.jpg|thumb|[華西列夫斯基](../Page/華西列夫斯基.md "wikilink")
Horloogiyn Choybalsan.jpg|thumb|[霍尔洛·乔巴山](../Page/霍尔洛·乔巴山.md "wikilink")
Kim Il Sung Portrait-2.jpg|thumb|[金日成](../Page/金日成.md "wikilink")

## 各国陸軍元帥军衔标志

Afgn-Army-Marshal(Field Marshal).svg|阿富汗元帅肩章 Australian Army
OF-10.svg|澳大利亚陆军元帅肩章 Kuk FieldMarsh 1918.svg|奥匈帝国陆军元帅领章
Marechal-V.gif|巴西陆军元帅肩章 Cambodian Army OF-10.svg|柬埔寨五星上将肩章 Marshal of
the PRC rank insignia (vertical).svg|中华人民共和国元帅肩章
Army-HRV-OF-10.svg|克罗地亚陆军元帅肩章 Vojskovođa.gif|克罗地亚独立国陆军元帅领章
KEgypt-Army-OF-10.svg|埃及王国陆军元帅肩章 Egypt Army - OF10.svg|埃及共和国陆军元帅肩章
Sotamarsalkka.svg|芬兰陆军元帅领章 Army-FRA-OF-10.svg|法国陆军元帅肩章 Field Marshal of
the Indian Army.svg|印度陆军元帅 23-TNI Army-GA.svg|印度尼西亚陆军元帅 元帥徽章.svg|日本元帅徽章
DR Generalfeldmarschall 1918.gif|德国陆军元帅 1918年的陆军元帅肩章
RA-SA_F10MarsSU_1955.png|[苏联元帅肩章](../Page/苏联元帅.md "wikilink")
1943-1991 Marshal 1944-1972.png|蒙古元帅肩章 1944-1972

## 参见

  - [元帥](../Page/元帥.md "wikilink")
      - [海军元帅](../Page/海军元帅.md "wikilink")
      - [空军元帅](../Page/空军元帅.md "wikilink")
  - [大元帅](../Page/大元帅.md "wikilink")

## 备注

## 參考

[Category:元帥](../Category/元帥.md "wikilink")
[Category:陸軍人物](../Category/陸軍人物.md "wikilink")

1.  [藝術與建築索引典—陸軍元帥](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300236754)
    於2011 年4 月8 日查閱

2.  [His Majesty Promotes Cambodian Leaders to Five-Star General
    DECEMBER 27, 2009](http://cppdailynews.blogspot.com/2009/12/his-majesty-promotes-cambodian-leaders.html)

3.
4.
5.  Held a senior rank to other marshals of the same country.

6.
7.
8.
9.

10.

11.

12.
13.

14. The equivalent of a *Generalfeldmarschall* in the German navy was
    *[Großadmiral](../Page/Großadmiral.md "wikilink")* (grand admiral).
    The rank of *Generalfeldmarschall* was abolished after the fall of
    [Nazi Germany](../Page/Nazi_Germany.md "wikilink") in 1945.

15.
16.
17. ["North Korea's Kim Jong-un named marshal", BBC News, 18
    July 2012](http://www.bbc.co.uk/news/world-asia-18881524)

18.