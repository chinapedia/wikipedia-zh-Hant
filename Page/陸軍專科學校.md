[陸軍專科學校正門.jpg](https://zh.wikipedia.org/wiki/File:陸軍專科學校正門.jpg "fig:陸軍專科學校正門.jpg")
[Army_Academy_R.O.C._Military_Band_Performing_in_No.13_Pier_of_Zhongzheng_Naval_Base_20130504k.jpg](https://zh.wikipedia.org/wiki/File:Army_Academy_R.O.C._Military_Band_Performing_in_No.13_Pier_of_Zhongzheng_Naval_Base_20130504k.jpg "fig:Army_Academy_R.O.C._Military_Band_Performing_in_No.13_Pier_of_Zhongzheng_Naval_Base_20130504k.jpg")
[Army_Academy_R.O.C._Marching_Band_Performing_in_Chengkungling_Ground_20131012a.jpg](https://zh.wikipedia.org/wiki/File:Army_Academy_R.O.C._Marching_Band_Performing_in_Chengkungling_Ground_20131012a.jpg "fig:Army_Academy_R.O.C._Marching_Band_Performing_in_Chengkungling_Ground_20131012a.jpg")
[Army_Academy_R.O.C._Military_Drums_Team_Performing_in_Chengkungling_Ground_20131012c.jpg](https://zh.wikipedia.org/wiki/File:Army_Academy_R.O.C._Military_Drums_Team_Performing_in_Chengkungling_Ground_20131012c.jpg "fig:Army_Academy_R.O.C._Military_Drums_Team_Performing_in_Chengkungling_Ground_20131012c.jpg")
[Army_Academy_R.O.C._Lion_Dance_Team_Performing_Program_in_Afternoon_20130504a.jpg](https://zh.wikipedia.org/wiki/File:Army_Academy_R.O.C._Lion_Dance_Team_Performing_Program_in_Afternoon_20130504a.jpg "fig:Army_Academy_R.O.C._Lion_Dance_Team_Performing_Program_in_Afternoon_20130504a.jpg")
**陸軍專科學校**，簡稱**陸軍專校**、**陸專**，是訓練[中華民國國軍](../Page/中華民國國軍.md "wikilink")[士官](../Page/士官.md "wikilink")[幹部的](../Page/幹部.md "wikilink")[軍事學校](../Page/軍事學校.md "wikilink")。位於[臺灣](../Page/臺灣.md "wikilink")[桃園市](../Page/桃園市.md "wikilink")[中壢區](../Page/中壢區.md "wikilink")[龍岡地區的](../Page/龍岡.md "wikilink")[軍事學校](../Page/軍事學校.md "wikilink")，早期招收對象在營的[國軍](../Page/國軍.md "wikilink")[士官](../Page/士官.md "wikilink")[幹部](../Page/幹部.md "wikilink")，近年開始對外招收社會青年、高中／高職生畢業生，並且持續招收在營[士官入學](../Page/士官.md "wikilink")，目標是提升[中華民國國軍](../Page/中華民國國軍.md "wikilink")[士官](../Page/士官.md "wikilink")[幹部的學習能力與](../Page/幹部.md "wikilink")[學歷](../Page/學歷.md "wikilink")。陸軍專科學校同時亦是各[軍種](../Page/軍種.md "wikilink")[單位委託受訓或專長訓練的](../Page/單位.md "wikilink")[軍事學校](../Page/軍事學校.md "wikilink")，該校不僅只協助[中華民國陸軍一個](../Page/中華民國陸軍.md "wikilink")[軍種受訓與](../Page/軍種.md "wikilink")[教學](../Page/教學.md "wikilink")，同時協助了[海軍陸戰隊](../Page/中華民國海軍陸戰隊.md "wikilink")、[聯勤](../Page/聯勤.md "wikilink")、[憲兵](../Page/中華民國憲兵.md "wikilink")、[後備](../Page/國防部後備司令部.md "wikilink")、[飛彈指揮部等](../Page/飛彈指揮部.md "wikilink")[軍種或](../Page/軍種.md "wikilink")[單位輔助在校學生修習學業](../Page/單位.md "wikilink")，[學生畢業後便回到各自所屬](../Page/學生.md "wikilink")[軍種服務](../Page/軍種.md "wikilink")。近年更增加[政戰局](../Page/國防部政治作戰局.md "wikilink")、[空軍](../Page/中華民國空軍.md "wikilink")、[電展室](../Page/國防部電訊發展室.md "wikilink")、[資通電軍等](../Page/國防部參謀本部資通電軍指揮部.md "wikilink")[軍種](../Page/軍種.md "wikilink")[單位協助輔導修業](../Page/單位.md "wikilink")。修業年限為二年。

## 校史

  - 1957年 成立「**陸軍第一士官學校**」。
  - 1986年
    納編**[福建省](../Page/福建省_\(中華民國\).md "wikilink")[金門縣](../Page/金門縣.md "wikilink")**「**陸軍第二士官學校**」，改名為「**陸軍士官學校**」。
  - 1998年 改制為綜合高中。
  - 2000年 改名為「**國立陸軍高級中學**」，為三年制綜合高中，並分設八個學程。
  - 2005年 升格為「**陸軍專科學校**」，為二年制專科學校。

## 校訓

  - **親愛精誠**：原本是[黃埔軍校](../Page/黃埔軍校.md "wikilink")[校長](../Page/校長.md "wikilink")[蔣中正所擬定的](../Page/蔣中正.md "wikilink")[校訓](../Page/校訓.md "wikilink")，至今已延用至[中華民國](../Page/中華民國.md "wikilink")[陸](../Page/陸軍.md "wikilink")、[海](../Page/海軍.md "wikilink")、[空軍以及各](../Page/空軍.md "wikilink")[軍事院校的](../Page/軍事院校.md "wikilink")[校訓或精神指標中](../Page/校訓.md "wikilink")，同時也是陸軍專科學校的共同[校訓之一](../Page/校訓.md "wikilink")。

[親愛精誠依據](../Page/親愛精誠.md "wikilink")[蔣中正的闡述](../Page/蔣中正.md "wikilink")，意義是造就「頂天立地」「繼往開來」，「堂堂正正」的[革命軍人](../Page/革命軍人.md "wikilink")。而[蔣中正於](../Page/蔣中正.md "wikilink")[民國十四年](../Page/民國.md "wikilink")[元旦對](../Page/元旦.md "wikilink")[黃埔軍校](../Page/黃埔軍校.md "wikilink")[學生訓話中更闡釋了](../Page/學生.md "wikilink")：「『親愛』是要所有的革命同志能『相親相愛』，官校的宗旨『精』是『精益求精』，『誠』是『誠心誠意』。

## 歷任校長

| 任次   | 姓名                                 | 任期時間                    | 備註    |
| ---- | ---------------------------------- | ----------------------- | ----- |
| 第1任  | [郭修甲上校](../Page/郭修甲.md "wikilink") | 1957年2月1日－1958年3月15日    |       |
| 第2任  | [陳應照少將](../Page/陳應照.md "wikilink") | 1958年3月16日－1959年2月28日   |       |
| 第3任  | [蕭宏毅少將](../Page/蕭宏毅.md "wikilink") | 1959年3月1日－1961年8月31日    |       |
| 第4任  | [江學海少將](../Page/江學海.md "wikilink") | 1961年9月1日－1963年1月14日    |       |
| 第5任  | [孟逑美少將](../Page/孟逑美.md "wikilink") | 1963年1月15日－1965年3月10日   |       |
| 第6任  | [葛先樸少將](../Page/葛先樸.md "wikilink") | 1965年3月11日－1966年8月31日   |       |
| 第7任  | [常持琇少將](../Page/常持琇.md "wikilink") | 1966年9月1日－1969年6月20日    |       |
| 第8任  | [杜文芳少將](../Page/杜文芳.md "wikilink") | 1969年6月21日－1970年3月31日   |       |
| 第9任  | [李　健少將](../Page/李健.md "wikilink")  | 1970年4月1日－1973年5月15日    |       |
| 第10任 | [項世英少將](../Page/項世英.md "wikilink") | 1973年5月16日－1975年5月15日   |       |
| 第11任 | [趙濟世少將](../Page/趙濟世.md "wikilink") | 1975年5月16日－1977年5月15日   |       |
| 第12任 | [吳招有少將](../Page/吳招有.md "wikilink") | 1977年5月16日－1979年7月15日   |       |
| 第13任 | [張德廷少將](../Page/張德廷.md "wikilink") | 1979年7月16日－1982年10月31日  |       |
| 第14任 | [姚光宇少將](../Page/姚光宇.md "wikilink") | 1982年11月1日－1984年10月15日  |       |
| 第15任 | [楊德輝少將](../Page/楊德輝.md "wikilink") | 1984年10月16日－1988年12月31日 |       |
| 第16任 | [劉寧善少將](../Page/劉寧善.md "wikilink") | 1989年1月1日－1990年2月15日    |       |
| 第17任 | [江中柱少將](../Page/江中柱.md "wikilink") | 1990年2月16日－1991年8月31日   |       |
| 第18任 | [張鑄勳少將](../Page/張鑄勳.md "wikilink") | 1991年9月1日－1993年9月30日    |       |
| 第19任 | [劉艾迪少將](../Page/劉艾迪.md "wikilink") | 1993年10月1日－1995年12月31日  |       |
| 第20任 | [劉北陵少將](../Page/劉北陵.md "wikilink") | 1996年1月1日－1997年8月31日    |       |
| 第21任 | [吳達澎少將](../Page/吳達澎.md "wikilink") | 1997年9月1日－2000年7月31日    |       |
| 第22任 | [黃奕炳少將](../Page/黃奕炳.md "wikilink") | 2000年8月1日－2002年2月28日    |       |
| 第23任 | [郭亨政少將](../Page/郭亨政.md "wikilink") | 2002年3月1日－2003年7月1日     |       |
| 第24任 | [陳敬忠少將](../Page/陳敬忠.md "wikilink") | 2003年7月1日－2004年12月31日   |       |
| 第25任 | [張志範少將](../Page/張志範.md "wikilink") | 2005年1月1日－2005年11月31日   |       |
| 第26任 | [戴宏聲少將](../Page/戴宏聲.md "wikilink") | 2005年12月1日－2006年4月30日   |       |
| 第27任 | [喬元雷少將](../Page/喬元雷.md "wikilink") | 2006年5月1日－2007年12月31日   |       |
| 第28任 | [潘家宇少將](../Page/潘家宇.md "wikilink") | 2008年1月1日－2009年9月30日    |       |
| 第29任 | [劉必棟少將](../Page/劉必棟.md "wikilink") | 2009年10月1日－2012年7月31日   |       |
| 第30任 | [童光復少將](../Page/童光復.md "wikilink") | 2012年8月1日－2013年10月15日   |       |
| 第31任 | [曹君範少將](../Page/曹君範.md "wikilink") | 2013年10月16日－2015年12月31日 |       |
| 第32任 | [李福華少將](../Page/李福華.md "wikilink") | 2016年1月1日－2017年9月30日    |       |
| 代理   | [李志萍上校](../Page/李志萍.md "wikilink") | 2017年10月1日－2017年11月9日   | 教育長代理 |
| 第33任 | [石文龍少將](../Page/石文龍.md "wikilink") | 2017年11月10日－現任          |       |

## 教學單位

| <span style="font-size:12pt;"> 教學單位 | 通識中心  | 電腦與通訊工程科 |
| ----------------------------------- | ----- | -------- |
| 電子工程科                               | 化學工程科 |          |
| 動力機械工程科                             | 機械工程科 |          |
| 車輛工程科                               | 飛機工程科 |          |
| 土木工程科                               | 體育科   |          |

## 參考資料

## 外部連結

  - [中華民國陸軍專科學校](http://www.aaroc.edu.tw/)

[Category:1957年創建的教育機構](../Category/1957年創建的教育機構.md "wikilink")
[Category:中華民國軍事院校](../Category/中華民國軍事院校.md "wikilink")
[Category:桃園市大專院校](../Category/桃園市大專院校.md "wikilink")
[Category:中壢區](../Category/中壢區.md "wikilink")
[H](../Category/中華民國陸軍.md "wikilink")