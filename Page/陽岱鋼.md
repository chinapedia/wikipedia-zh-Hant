**陽岱鋼**（、漢語拼音：Yáng
Dàigāng、；），原名**仲壽**，生於[台東市](../Page/台東市.md "wikilink")，[臺灣原住民](../Page/臺灣原住民.md "wikilink")[阿美族人](../Page/阿美族.md "wikilink")，為[旅日棒球選手](../Page/旅日棒球選手.md "wikilink")，目前效力於[日本職棒](../Page/日本職棒.md "wikilink")[中央聯盟](../Page/中央聯盟.md "wikilink")[讀賣巨人隊](../Page/讀賣巨人隊.md "wikilink")，守備位置是[外野手](../Page/外野手.md "wikilink")。2005年高校生選秀第一指名，成為[日本職棒史上台灣人最高指名](../Page/日本職棒.md "wikilink")。其兄[陽耀勳目前效力於](../Page/陽耀勳.md "wikilink")[中華職棒](../Page/中華職棒.md "wikilink")[Lamigo桃猿隊](../Page/Lamigo桃猿隊.md "wikilink")。
2012年11月8日，陽岱鋼獲得日本職棒[太平洋聯盟外野手金手套獎](../Page/太平洋聯盟.md "wikilink")，成為第一位獲得日本職棒金手套獎的台籍野手。2017年6月6日從巨人二軍復出

2013年11月28日，陽岱鋼入圍由[教育部體育署](../Page/教育部體育署.md "wikilink")、[中華奧會](../Page/中華奧會.md "wikilink")、[中華民國體育運動總會所主辦的](../Page/中華民國體育運動總會.md "wikilink")102年度[運動精英獎的](../Page/運動精英獎.md "wikilink")「傑出男運動員獎」\[1\]。而在同年的12月18日宣布「最佳男運動員獎」由陽岱鋼獲得，領獎者為其太太謝宛容。\[2\]

## 生平

### 業餘時期

陽岱鋼自[國立臺東大學附屬體育高級中學的國中部畢業後](../Page/國立臺東大學附屬體育高級中學.md "wikilink")，赴日本留學，於畢業。高中時代擔任游擊手，累計三年擊出39支全壘打，並擁有50公尺5秒9的腳程與遠投110公尺的肩力，優秀的運動能力受到了球探的矚目。

### 日本職棒

#### 2006\~2016:[北海道日本火腿鬥士](../Page/北海道日本火腿鬥士.md "wikilink")

2005年底參加[日本職棒高中生選秀](../Page/日本職棒.md "wikilink")，被[北海道日本火腿鬥士及](../Page/北海道日本火腿鬥士.md "wikilink")[福岡軟銀鷹兩隊同時第一指名](../Page/福岡軟銀鷹.md "wikilink")，結果由前者抽籤贏得與陽岱鋼的交涉權。\[3\]出身福岡第一高校，喪失與哥哥[陽耀勳同隊機會的陽岱鋼大感失望](../Page/陽耀勳.md "wikilink")，一度考慮等待大學畢業後再逆指名加盟軟銀鷹，最後在火腿鬥士的積極遊說下，才與火腿隊簽約。\[4\]

2006年9月，首次升上一軍，但沒有任何出場紀錄；11月，陽岱鋼返台代表台灣參加[洲際盃棒球賽](../Page/洲際盃棒球賽.md "wikilink")，以主力先發游擊手身分出賽，交出打擊率0.267、2支全壘打的成績，賽後榮獲得分王榮譽。

2007年4月20日，陽岱鋼首次獲得一軍先發機會，擔任第七棒三壘手，但兩個打數沒有擊出安打，吞下兩次三振。

[Dai-Kang_Yang_on_July_1,_2013.jpg](https://zh.wikipedia.org/wiki/File:Dai-Kang_Yang_on_July_1,_2013.jpg "fig:Dai-Kang_Yang_on_July_1,_2013.jpg")時期\]\]
2013年3月29日，日本職棒舉行新球季開幕賽，陽岱鋼擔任火腿隊先發第1棒、中外野手出戰[埼玉西武獅](../Page/埼玉西武獅.md "wikilink")，單場3安打，拿下猛打賞且助球隊贏球。此外，陽岱鋼也是新球季唯一登錄在一軍名單的台灣選手\[5\]。2013年球季結束，陽岱鋼以單季盜壘47次，成為太平洋聯盟盜壘王，也是火腿鬥士隊史第一位聯盟盜壘王。12月14日，與日本火腿鬥士隊簽下2年總金額4億日幣複數年合約。年薪由原來的8千8百萬日幣增加到1億8千萬日幣。\[6\]。

2014年，陽岱鋼揮出25轟，並外帶20盜。

#### 2017\~:[讀賣巨人](../Page/讀賣巨人.md "wikilink")

2016年11月7日，宣告成為自由球員，吸引[歐力士野牛](../Page/歐力士野牛.md "wikilink")、[東北樂天金鷲和讀賣巨人爭奪](../Page/東北樂天金鷲.md "wikilink")，12月14日與巨人達成協議，巨人隊提出5年10億日圓合約、球衣背號為2號為條件，12月19日在東京舉行加盟巨人隊的記者會。

2016年12月19日，陽岱鋼的合約是5年15億日圓（約[新台幣](../Page/新台幣.md "wikilink")4億1000萬元），追平[廣澤克己](../Page/廣澤克己.md "wikilink")、[清原和博等巨星](../Page/清原和博.md "wikilink")，追平日本職棒史上自由球員的最長年限合約，並在巨人隊總經理堤臣佳、總教練[高橋由伸陪同下](../Page/高橋由伸.md "wikilink")，出席在東京帝國飯店舉行的加盟記者會。

2018年9月17日，[讀賣巨人與](../Page/讀賣巨人.md "wikilink")[中日龍比賽中](../Page/中日龍.md "wikilink")，在第8局代打上場，從投手[祖父江大輔的手中敲出陽春砲](../Page/祖父江大輔.md "wikilink")，替球隊送回第2分，此也是日職生涯的第100轟。\[7\]

## 家庭

  - 妻：謝宛容，兩人育有二女
  - 叔叔：[陽介仁](../Page/陽介仁.md "wikilink")
  - 長兄：[陽耀勳](../Page/陽耀勳.md "wikilink")
  - 仲兄：[陽品華](../Page/陽品華.md "wikilink")
  - 姊：[陽詩怡](../Page/陽詩怡.md "wikilink")
  - 妹：[陽詩慧](../Page/陽詩慧.md "wikilink")
  - 妹：[陽沁彤](../Page/陽沁彤.md "wikilink")

## 經歷

  - [台東縣新生國小少棒隊](../Page/台東縣.md "wikilink")
  - [台東縣新生國中青少棒隊](../Page/台東縣.md "wikilink")
  - [台東縣](../Page/台東縣.md "wikilink")[台東體中國中部青少棒隊](../Page/台東體中.md "wikilink")
  - [日本](../Page/日本.md "wikilink")青棒隊
  - [日本職棒](../Page/日本職棒.md "wikilink")[北海道日本火腿鬥士隊](../Page/北海道日本火腿鬥士.md "wikilink")
    (2006年—2016年)
  - [日本職棒](../Page/日本職棒.md "wikilink")[讀賣巨人隊](../Page/讀賣巨人.md "wikilink")
    (2017年—)

## 詳細情報

### 年度別打撃成績

<table>
<thead>
<tr class="header">
<th><p>球團|</p></th>
<th><p>年度|</p></th>
<th><p>試合|</p></th>
<th><p>打<br />
席|</p></th>
<th><p>打<br />
數|</p></th>
<th><p>得分|</p></th>
<th><p>安打|</p></th>
<th><p>二<br />
壘<br />
打|</p></th>
<th><p>三<br />
壘<br />
打|</p></th>
<th><p>本<br />
壘<br />
打|</p></th>
<th><p>壘<br />
打<br />
數|</p></th>
<th><p>打點|</p></th>
<th><p>盜壘|</p></th>
<th><p>盜<br />
壘<br />
刺|</p></th>
<th><p>觸擊|</p></th>
<th><p>犧<br />
牲<br />
打|</p></th>
<th><p>保送|</p></th>
<th><p>觸身|</p></th>
<th><p>三振|</p></th>
<th><p>雙殺|</p></th>
<th><p>打<br />
擊<br />
率|</p></th>
<th><p>上<br />
壘<br />
率|</p></th>
<th><p>長<br />
打<br />
率|</p></th>
<th><p>OPS</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/北海道日本火腿鬥士.md" title="wikilink">日本火腿</a></p></td>
<td><p>2007</p></td>
<td><p>55</p></td>
<td><p>116</p></td>
<td><p>109</p></td>
<td><p>12</p></td>
<td><p>26</p></td>
<td><p>8</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>38</p></td>
<td><p>10</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>30</p></td>
<td><p>0</p></td>
<td><p>.239</p></td>
<td><p>.259</p></td>
<td><p>.349</p></td>
<td><p>.608</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>44</p></td>
<td><p>123</p></td>
<td><p>111</p></td>
<td><p>7</p></td>
<td><p>16</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>24</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>31</p></td>
<td><p>3</p></td>
<td><p>.144</p></td>
<td><p>.193</p></td>
<td><p>.216</p></td>
<td><p>.409</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>15</p></td>
<td><p>13</p></td>
<td><p>11</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>.182</p></td>
<td><p>.250</p></td>
<td><p>.182</p></td>
<td><p>.432</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p>109</p></td>
<td><p>281</p></td>
<td><p>253</p></td>
<td><p>35</p></td>
<td><p>62</p></td>
<td><p>12</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>86</p></td>
<td><p>31</p></td>
<td><p>8</p></td>
<td><p>1</p></td>
<td><p>10</p></td>
<td><p>2</p></td>
<td><p>12</p></td>
<td><p>4</p></td>
<td><p>70</p></td>
<td><p>4</p></td>
<td><p>.245</p></td>
<td><p>.288</p></td>
<td><p>.340</p></td>
<td><p>.628</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p>141</p></td>
<td><p>603</p></td>
<td><p>537</p></td>
<td><p>66</p></td>
<td><p>147</p></td>
<td><p>23</p></td>
<td><p>4</p></td>
<td><p>6</p></td>
<td><p>196</p></td>
<td><p>36</p></td>
<td><p>19</p></td>
<td><p>2</p></td>
<td><p>38</p></td>
<td><p>1</p></td>
<td><p>22</p></td>
<td><p>5</p></td>
<td><p>134</p></td>
<td><p>5</p></td>
<td><p>.274</p></td>
<td><p>.308</p></td>
<td><p>.365</p></td>
<td><p>.673</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><strong>144</strong></p></td>
<td><p>599</p></td>
<td><p>534</p></td>
<td><p>71</p></td>
<td><p>153</p></td>
<td><p>28</p></td>
<td><p>5</p></td>
<td><p>7</p></td>
<td><p>212</p></td>
<td><p>55</p></td>
<td><p>17</p></td>
<td><p>6</p></td>
<td><p>18</p></td>
<td><p>5</p></td>
<td><p>37</p></td>
<td><p>6</p></td>
<td><p>123</p></td>
<td><p>10</p></td>
<td><p>.287</p></td>
<td><p>.337</p></td>
<td><p>.398</p></td>
<td><p>.735</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><strong>144</strong></p></td>
<td><p>658</p></td>
<td><p>574</p></td>
<td><p>93</p></td>
<td><p>162</p></td>
<td><p>27</p></td>
<td><p>2</p></td>
<td><p>18</p></td>
<td><p>247</p></td>
<td><p>67</p></td>
<td><p><strong>47</strong></p></td>
<td><p>10</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>69</p></td>
<td><p>9</p></td>
<td><p>142</p></td>
<td><p>9</p></td>
<td><p>.282</p></td>
<td><p>.367</p></td>
<td><p>.430</p></td>
<td><p>.797</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p>125</p></td>
<td><p>540</p></td>
<td><p>471</p></td>
<td><p>77</p></td>
<td><p>138</p></td>
<td><p>18</p></td>
<td><p>1</p></td>
<td><p>25</p></td>
<td><p>233</p></td>
<td><p>85</p></td>
<td><p>20</p></td>
<td><p>6</p></td>
<td><p>9</p></td>
<td><p>3</p></td>
<td><p>45</p></td>
<td><p>12</p></td>
<td><p>108</p></td>
<td><p>10</p></td>
<td><p>.293</p></td>
<td><p>.367</p></td>
<td><p>.495</p></td>
<td><p>.853</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p>86</p></td>
<td><p>381</p></td>
<td><p>352</p></td>
<td><p>47</p></td>
<td><p>91</p></td>
<td><p>10</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
<td><p>126</p></td>
<td><p>36</p></td>
<td><p>14</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>21</p></td>
<td><p>4</p></td>
<td><p>93</p></td>
<td><p>4</p></td>
<td><p>.259</p></td>
<td><p>.306</p></td>
<td><p>.358</p></td>
<td><p>.664</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p>130</p></td>
<td><p>555</p></td>
<td><p>495</p></td>
<td><p>66</p></td>
<td><p>145</p></td>
<td><p>24</p></td>
<td><p>1</p></td>
<td><p>14</p></td>
<td><p>213</p></td>
<td><p>61</p></td>
<td><p>5</p></td>
<td><p>6</p></td>
<td><p>7</p></td>
<td><p>1</p></td>
<td><p>42</p></td>
<td><p>10</p></td>
<td><p>121</p></td>
<td><p>10</p></td>
<td><p>.293</p></td>
<td><p>.359</p></td>
<td><p>.430</p></td>
<td><p>.790</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/讀賣巨人.md" title="wikilink">巨人</a></p></td>
<td><p>2017</p></td>
<td><p>87</p></td>
<td><p>381</p></td>
<td><p>330</p></td>
<td><p>46</p></td>
<td><p>87</p></td>
<td><p>18</p></td>
<td><p>1</p></td>
<td><p>9</p></td>
<td><p>134</p></td>
<td><p>33</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>41</p></td>
<td><p>7</p></td>
<td><p>80</p></td>
<td><p>6</p></td>
<td><p>.264</p></td>
<td><p>.356</p></td>
<td><p>.406</p></td>
<td><p>.762</p></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p>87</p></td>
<td><p>276</p></td>
<td><p>253</p></td>
<td><p>24</p></td>
<td><p>62</p></td>
<td><p>12</p></td>
<td><p>2</p></td>
<td><p>10</p></td>
<td><p>108</p></td>
<td><p>37</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>17</p></td>
<td><p>2</p></td>
<td><p>72</p></td>
<td><p>7</p></td>
<td><p>.245</p></td>
<td><p>.297</p></td>
<td><p>.427</p></td>
<td><p>.724</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2019</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>通算：12年</p></td>
<td><p>1167</p></td>
<td><p>4526</p></td>
<td><p>4029</p></td>
<td><p>546</p></td>
<td><p>1091</p></td>
<td><p>180</p></td>
<td><p>24</p></td>
<td><p>100</p></td>
<td><p>1619</p></td>
<td><p>455</p></td>
<td><p>140</p></td>
<td><p>39</p></td>
<td><p>102</p></td>
<td><p>19</p></td>
<td><p>315</p></td>
<td><p>61</p></td>
<td><p>1007</p></td>
<td><p>68</p></td>
<td><p>.271</p></td>
<td><p>.332</p></td>
<td><p>.402</p></td>
<td><p>.734</p></td>
<td></td>
</tr>
</tbody>
</table>

  - 2018年度球季結束時
  - 各年度**粗體字**為該聯盟最多者

### 球衣背號

  - **24**（2006年－2012年）
  - **1**（2013年－2016年）\[8\]同時也是2013年[WBC代表隊背號](../Page/世界棒球經典賽.md "wikilink")
  - **2**（2017年－）

### 個人記錄

#### 日本職棒記錄

  - 首次記錄

<!-- end list -->

  - 首次出場・首次先發出場：2007年4月20日、對[福岡軟體銀行鷹](../Page/福岡軟體銀行鷹.md "wikilink")3回戰（[東京巨蛋](../Page/東京巨蛋.md "wikilink")）
    - 先發擔任第7棒・[三壘手出場](../Page/三壘手.md "wikilink")
  - 首次安打：2007年4月25日、對[千葉羅德海洋](../Page/千葉羅德海洋.md "wikilink")4回戰（[札幌巨蛋](../Page/札幌巨蛋.md "wikilink")）
    - 3局下、投手：[久保康友](../Page/久保康友.md "wikilink")
  - 首次打點：2007年4月29日、對[東北樂天金鷹](../Page/東北樂天金鷹.md "wikilink")8回戰（[宮城球場](../Page/宮城球場.md "wikilink")）
    - 2局上1出局、投手：[青山浩二](../Page/青山浩二.md "wikilink")
  - 首次盗壘：同上 - 2局上1出局、投手：同、捕手：嶋基宏
  - 首支全壘打：2008年4月29日、對千葉羅德海洋7回戰（[千葉海洋球場](../Page/千葉海洋球場.md "wikilink")）
    - 7局上1出局、投手：[成瀨善久](../Page/成瀨善久.md "wikilink")
  - 首支滿壘全壘打：2013年4月4日、對千葉羅德海洋2回戰
    ([千葉海洋球場](../Page/千葉海洋球場.md "wikilink")）6局上1出局、面對投手[荻野忠寬擊出左外野方向全壘打](../Page/荻野忠寬.md "wikilink")\[9\]

<!-- end list -->

  - 其他記錄

<!-- end list -->

  - [日本職棒全明星賽出場](../Page/日本職棒全明星賽.md "wikilink")：3回 （2012\~2014年）
  - 初打席首打者全壘打：2012年第1戰（7月20日、[大阪巨蛋](../Page/京瓷巨蛋大阪.md "wikilink")）、1局下面對[杉内俊哉所擊出](../Page/杉内俊哉.md "wikilink")
    ※史上2位達成
  - 2013年球季，單季47次盜壘，成為太平洋聯壘盜壘王。
  - 2013年札幌巨蛋年度MVP

### 國際賽記錄

  - [2013年世界棒球經典賽預賽B組MVP](../Page/2013年世界棒球經典賽_-_B組.md "wikilink")，打擊率3成33，4分打點，並登上MLB官網首頁\[10\]。

### 登錄名

  - 陽 仲壽（2006年－2009年）
  - 陽 岱鋼（2010年－） ※改名

### 登場曲

  - [Bon Jovi](../Page/Bon_Jovi.md "wikilink") 『[It's my
    life](../Page/It's_my_life.md "wikilink")』（2011年－）
  - [聯合公園](../Page/聯合公園.md "wikilink")
    『[Numb](../Page/ナム.md "wikilink")』 （2011年－）
  - [森雄二とサザンクロス](../Page/森雄二とサザンクロス.md "wikilink") 『好きですサッポロ』（2011年途中－）
  - [平井大](../Page/平井大.md "wikilink") 『[Slow &
    Easy](../Page/Slow_&_Easy.md "wikilink")』（2015年－）

## 當選國手紀錄

  - 1999年[小馬聯盟世界少棒錦標賽](../Page/小馬聯盟世界少棒錦標賽.md "wikilink")
  - 2006年[第一屆世界棒球經典賽](../Page/2006年世界棒球經典賽.md "wikilink")
  - 2006年[台灣洲際盃棒球賽](../Page/2006年洲際盃棒球賽.md "wikilink")
  - 2006年[杜哈亞運](../Page/2006年亞洲運動會.md "wikilink")
  - 2007年[亞錦賽](../Page/2007年亞洲棒球錦標賽.md "wikilink")
  - 2013年[第三屆世界棒球經典賽](../Page/2013年世界棒球經典賽.md "wikilink")
  - 2015年[2015年世界棒球12強賽](../Page/2015年世界棒球12強賽.md "wikilink")
  - 2017年[2017年亞洲職棒冠軍爭霸賽](../Page/2017年亞洲職棒冠軍爭霸賽.md "wikilink")

## 獲獎紀錄

  - [日本職棒太平洋聯盟盜壘王](../Page/日本職棒.md "wikilink")：1次（2013年）
  - [日本職棒太平洋聯盟](../Page/日本職棒.md "wikilink")[金手套](../Page/金手套.md "wikilink")：4次（外野手：2012年、2013年、2014年、2016年）
  - [日本職棒札幌巨蛋年度MVP](../Page/日本職棒.md "wikilink")：2次（2013年、2014年）
  - [日本職棒全明星賽MVP](../Page/日本職棒全明星賽.md "wikilink")：1次（2012年第3戰）
  - [日本職棒全明星賽敢闘選手獎](../Page/日本職棒全明星賽.md "wikilink")：3次（2012年第1戰、2014年第1戰、2014年第2戰）
  - [日本職棒全明星賽Be](../Page/日本職棒全明星賽.md "wikilink") a driver賞：1次（2014年）

## 評論

1.  貓熊[圓仔被臺北市政府與媒體炒作](../Page/圓仔.md "wikilink")，陽岱鋼在[臉書表示](../Page/臉書.md "wikilink")「貓熊很可愛，可是我們也有自己的[國寶叫](../Page/國寶.md "wikilink")[台灣黑熊](../Page/台灣黑熊.md "wikilink")」。\[11\]

## 參考來源

<div class="references-small">

<references />

</div>

## 外部連結

  - [NPB官方網站個人頁面](http://npb.jp/bis/players/21825112.html)

  -
[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:台灣原住民運動員](../Category/台灣原住民運動員.md "wikilink")
[Category:中華成棒隊球員](../Category/中華成棒隊球員.md "wikilink")
[Category:北海道日本火腿鬥士隊球員](../Category/北海道日本火腿鬥士隊球員.md "wikilink")
[Category:讀賣巨人隊球員](../Category/讀賣巨人隊球員.md "wikilink")
[Category:台灣旅日棒球選手](../Category/台灣旅日棒球選手.md "wikilink")
[Category:阿美族人](../Category/阿美族人.md "wikilink")
[Category:臺東人](../Category/臺東人.md "wikilink")
[Category:臺東市人](../Category/臺東市人.md "wikilink")
[Category:陽姓](../Category/陽姓.md "wikilink")

1.  [102年精英獎終生成就獎及傑出獎(入圍者)名單出爐！](http://www.cna.com.tw/postwrite/Detail/138007.aspx#.Upc78hDlMVg)，[中央通訊社](../Page/中央通訊社.md "wikilink")，2013年11月28日
2.  [精英獎/陽岱鋼最佳男運動員](http://tw.news.yahoo.com/%E7%B2%BE%E8%8B%B1%E7%8D%8E-%E9%99%BD%E5%B2%B1%E9%8B%BC%E6%9C%80%E4%BD%B3%E7%94%B7%E9%81%8B%E5%8B%95%E5%93%A1-082651502.html)，[中央通訊社](../Page/中央通訊社.md "wikilink")，2013年12月18日
3.  [陽仲壽返台
    思考去向](http://www.libertytimes.com.tw/2005/new/oct/13/today-sp2.htm)，《自由時報》，2005年10月13日。
4.  [陽仲壽
    約定火腿](http://www.libertytimes.com.tw/2005/new/nov/10/today-sp4.htm)，《自由時報》，2005年11月10日。
5.
6.  [日本ハムの陽岱鋼は2年総額4億円＝プロ野球・契約更改](https://archive.is/20131204120703/headlines.yahoo.co.jp/hl?a=20131204-00000095-jij-spo)，[時事通信](../Page/時事通信.md "wikilink")，2013年12月4日
7.  [自認非全壘打型選手
    陽岱鋼開心達成百轟](https://udn.com/news/story/7001/3372710)，[聯合新聞網](../Page/聯合新聞網.md "wikilink")，2018-09-17
8.  [日本ハム陽岱鋼が新庄氏の背番号「１」継承！大野は「２」に変更](http://www.sanspo.com/baseball/news/20121122/fig12112211410002-n1.html)

9.  [【日本ハム】陽岱鋼１号はプロ初の満弾！
    日刊スポーツ 4月4日(木)22時10分配信](http://headlines.yahoo.co.jp/hl?a=20130404-00000112-nksports-base)
10.
11. [台灣黑熊住三級貧民區？　網友批：動物也有M型社會](http://www.ettoday.net/news/20130902/264872.htm),
    [ETtoday](../Page/ETtoday.md "wikilink"), 2013年09月2日