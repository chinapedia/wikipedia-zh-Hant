**隆尧县**是[河北省](../Page/河北省.md "wikilink")[邢台市下辖的一个县](../Page/邢台.md "wikilink")。县人民政府驻隆尧镇。

## 行政区划

下辖6个[镇](../Page/行政建制镇.md "wikilink")、6个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [邢台市隆尧县商务之窗](http://longyao.mofcom.gov.cn/)
  - [隆尧县人民政府](https://web.archive.org/web/20120718065647/http://www.longyao.gov.cn/default.asp)

[隆尧县](../Page/category:隆尧县.md "wikilink")
[县](../Page/category:邢台区县市.md "wikilink")
[邢台](../Page/category:河北省县份.md "wikilink")