**隐山**，又名**龙山**、**龙王山**，[海拔](../Page/海拔.md "wikilink")437米，位于[湖南省](../Page/湖南省.md "wikilink")[湘潭县黄荆坪](../Page/湘潭县.md "wikilink")。山虽不高，但却是一座蕴含历史文化的名山。

## 名称来历

隐山名称的来由有如下一些说法：一说因[宋代文学家](../Page/宋代.md "wikilink")[周敦颐曾在此隐居讲学](../Page/周敦颐.md "wikilink")。一说则来自于一个[禅宗故事](../Page/禅宗.md "wikilink")：[曹洞宗创始人](../Page/曹洞宗.md "wikilink")[洞山良价与他的师兄](../Page/洞山良价.md "wikilink")[神山僧密在](../Page/神山僧密.md "wikilink")[潭州](../Page/潭州_\(隋朝\).md "wikilink")[云游](../Page/云游.md "wikilink")，发现了一条漂着菜叶的山涧，便随山涧深入到山中，最终寻到了一位隐僧，隐僧因为自己被发现而烧掉了原来的庵房，隐入山中更深处，而被称为龙山和尚或隐山和尚。另外[明朝](../Page/明朝.md "wikilink")[正德皇帝南游至此](../Page/正德皇帝.md "wikilink")，惊叹于隐匿山中的佛寺盛景，而题下了“天下隐山”的匾额。

## 历史名人

### 周敦颐

北宋[理学开创者](../Page/理学.md "wikilink")、文学家[周敦颐曾在隐山隐居讲学](../Page/周敦颐.md "wikilink")，传说周敦颐在此领悟了阴阳[八卦](../Page/八卦.md "wikilink")[太极之理](../Page/太极.md "wikilink")。隐山并有纪念《[爱莲说](../Page/爱莲说.md "wikilink")》的莲花池，“出淤泥而不染，濯清莲而不妖”被后人视为湖湘文化的重要品质。

### 胡安国胡宏父子

[胡安国从](../Page/胡安国.md "wikilink")[福建迁至隐山](../Page/福建.md "wikilink")，并开办[碧泉书院](../Page/碧泉书院.md "wikilink")，开创了[湖湘学派](../Page/湖湘学派.md "wikilink")。胡安国死后即葬于隐山，后[胡宏与其父合墓](../Page/胡宏_\(理学家\).md "wikilink")。

### 周小舟

曾任湖南省委第一书记的[周小舟出生在隐山东麓狮龙桥边](../Page/周小舟.md "wikilink")。

## 名胜古迹

隐山的古迹被概括为一寺、一墓、两祠、两树、四池、八桥。

一寺即[慈云禅寺](../Page/慈云禅寺.md "wikilink")，又叫[龙王寺](../Page/龙王寺.md "wikilink")。毁于[文革时期](../Page/文革.md "wikilink")。

一墓即北宋理学家、文学家[胡安国](../Page/胡安国.md "wikilink")（文定公）及其儿子[胡宏的合墓](../Page/胡宏_\(理学家\).md "wikilink")。墓始筑于[南宋](../Page/南宋.md "wikilink")[绍兴八年](../Page/紹興_\(南宋\).md "wikilink")（公元1138年）4月，后几经毁修。1981年，胡氏后裔重修，题墓名为“始祖胡公文定老大人、胡母刘氏老孺人之墓，二世祖五峰公附墓”。墓长7米，宽9米，现为湘潭县级文物保护单位。

二祠一为[濂溪祠](../Page/濂溪祠.md "wikilink")，为后人纪念号濂溪的[周敦颐所建](../Page/周敦颐.md "wikilink")。濂溪祠[门联为](../Page/门联.md "wikilink")“道承孔孟；学启程朱”，彰显了后人对周敦颐学术地位的评价。另一祠是三贤祠，为纪念胡文定公父子及常来此的[张南轩而立](../Page/张栻.md "wikilink")。

二树为濂溪祠前的垂丝[柏树及](../Page/柏树.md "wikilink")[银杏树](../Page/银杏树.md "wikilink")，据传为周敦颐亲手所植。

四池即[莲花池](../Page/莲花.md "wikilink")、洗笔池、化砚池、雷池。传说是周敦颐植莲、洗笔、化墨之处。

八桥为隐水桥、流叶桥、通箭桥、珂里桥、栗林桥、狮龙桥、神仙桥、龙王桥。

[category:湘潭地理](../Page/category:湘潭地理.md "wikilink")
[category:湖南山峰](../Page/category:湖南山峰.md "wikilink")

[Category:衡山山系](../Category/衡山山系.md "wikilink")