**隔离但平等**（）是源自於19世紀[美國黑人](../Page/美國黑人.md "wikilink")[种族隔离政策的一种表现形式](../Page/种族隔离.md "wikilink")，它试图通过为不同种族提供-{表}-面平等的设施或待遇，从而使实施空间隔离的做法[合法化](../Page/合法性.md "wikilink")，遲至20世紀才取得平等性質的公民權。20世紀末部分歐美國家開始重視[同性戀權益](../Page/同性戀.md "wikilink")，起初推行的同性[民事結合或同性婚姻專門法](../Page/民事結合.md "wikilink")（不適用一般婚姻法）亦被喻為另一種隔離但平等。\[1\]\[2\]

## 歷史

### 美國黑人種族隔離政策

[美国](../Page/美国.md "wikilink")[南北战争之后](../Page/南北战争.md "wikilink")，[美國憲法第十三修正案在](../Page/美國憲法第十三修正案.md "wikilink")1865年通過，[奴隶制被废除](../Page/奴隶制.md "wikilink")。“隔离但平等”于1877年美國聯邦軍隊撤出南方後，成为南部各州的一种普遍现象。各州以“[非裔美国人](../Page/非裔美国人.md "wikilink")”和“[欧裔美国人](../Page/欧裔美国人.md "wikilink")”之名将[黑人和](../Page/黑人.md "wikilink")[白人从空间上分割开来](../Page/白人.md "wikilink")，避免产生接触。

1896年，[美国最高法院在](../Page/美国最高法院.md "wikilink")[普莱西诉弗格森案中裁决这种做法符合](../Page/普莱西诉弗格森案.md "wikilink")[美国宪法](../Page/美国宪法.md "wikilink")，于是这种行为正式取得合法地位。最高法院裁決在今日看來被認為是最差劣的判決之一，但在當時來看被認為是實際上安撫南方各州，避免因作出有利黑人的判決，而導致已實行种族隔离法律的南方各州再次尋求脫離聯邦。

### 黑人隔離政策終結

随着[非裔美国人民权运动的不断推进](../Page/非裔美国人民权运动.md "wikilink")，“隔离但平等”原则受到了越来越多的挑战。

1954年，美国最高法院在[布朗诉托皮卡教育委员会案](../Page/布朗诉托皮卡教育委员会案.md "wikilink")（Brown v.
Board of
Education）中，以9：0一致裁决：由于“黑白隔离政策表示黑人低劣”，所以原告和提出诉讼而处境与此相似的其他人，由于受所述种族隔离之害，已被剥夺了[美國憲法第十四修正案與](../Page/美國憲法第十四修正案.md "wikilink")[平等保護條款所保障的权利](../Page/平等保護條款.md "wikilink")。此案的裁决突破了普莱西案对“隔离但平等”原则的认可，取消了在教育领域的种族隔离。此后最高法院又通过一系列判决，实质上否认了“隔离但平等”原则的合法性，被最終終結了美國實行逾一世紀的種族隔離政策。

### 後應用至同性婚姻政策

在現時部分[同性婚姻合法化的國家中](../Page/同性婚姻.md "wikilink")，之前都經過部分[民事結合的階段](../Page/民事結合.md "wikilink")。民事結合一般被指是容許同志伴侶經[註冊後享有與異性伴侶相近的法律地位](../Page/註冊.md "wikilink")，但有關的關係在法律上並不認為是[婚姻](../Page/婚姻.md "wikilink")。支持者（包括部分[同志運動支持者及反對同志運動及同志婚姻的人士](../Page/同志運動.md "wikilink")）認為民事結合可以保障同性伴侶的權益，部分包括容許同志伴侶[收養](../Page/收養.md "wikilink")，同時並不會影響而異性婚姻為主的傳統婚姻制度，可平衡各方的權益。部分[LGBT權利運動的支持者批評](../Page/LGBT權利運動.md "wikilink")，民事結合實際上是一種“隔离但平等”，同志伴侶在法律上始終並非婚姻，且部分民事結合的國家在生活細節及各方面始終有別於異性伴侶，同性伴侶並無法享有與異性伴侶相等的權利。

## 參見

  - [美國重建時期](../Page/美國重建時期.md "wikilink")
  - [民事結合](../Page/民事結合.md "wikilink")
  - [同性婚姻](../Page/同性婚姻.md "wikilink")

## 参考材料

  - [人民法院报-王亚琴-“隔离但平等”的终结](https://web.archive.org/web/20070929095623/http://www.jcrb.com/zyw/n2/ca123672.htm)

[Category:美国种族隔离](../Category/美国种族隔离.md "wikilink")

1.
2.