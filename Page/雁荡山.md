[Yandang_Mountain.jpg](https://zh.wikipedia.org/wiki/File:Yandang_Mountain.jpg "fig:Yandang_Mountain.jpg")绘《雁荡图》\]\]
**雁荡山**坐落于[浙江省](../Page/浙江.md "wikilink")[温州市](../Page/温州.md "wikilink")，分为北雁荡山、中雁荡山、南雁荡山。其中以北雁荡山最为有名，北雁荡山、中雁荡山均位于温州市北部[乐清境内](../Page/乐清市.md "wikilink")。北雁荡山总面积450平方千米，最高峰百岗尖海拔1150米，以奇峰、瀑布著称。它是[国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")，[中国十大名山之一](../Page/中国十大名山.md "wikilink")，国家首批五A级景区。它形成于1.2亿年前，是一座典型的[白垩纪流纹质古](../Page/白垩纪.md "wikilink")[火山](../Page/火山.md "wikilink")。它的灵峰、灵岩、[大龙湫被称为](../Page/大龙湫.md "wikilink")“雁荡三绝”。

2005年2月11日，雁荡山被[联合国教科文组织宣布为](../Page/联合国教科文组织.md "wikilink")[世界地质公园](../Page/世界地质公园.md "wikilink")。

## 地理

雁荡山地质结构形成于中生代、白垩纪的火山喷发时期，距今约有1.2亿年。由于雁荡山的岩石属[流纹岩](../Page/流纹岩.md "wikilink")，有流纹质天然博物馆之称。

## 主要景区

雁荡山有灵峰、灵岩、大龙湫、雁湖、中折瀑、显胜门、仙桥、羊角洞八大景区，其中以灵峰、灵岩、大龙湫最为有名。

### 灵峰

灵峰景区是雁荡山三大景区之一，总面积46平方千米，日景、夜景都十分耐看，主要景点有双笋峰、合掌峰、夫妻峰、雄鹰展翅、婆婆峰、公公峰等。值得一提的是，很多景点其实是一座山峰，从不同的角度可以看到不同的景观，如公公峰和婆婆峰。另外，白天、夜晚看同一座山峰也可看到不同的景观，如合掌峰是日景，夫妻峰是同一山峰的夜景。

### 灵岩

灵岩景区东起响岩门，西至马鞍岭，南到飞泉寺，北达百岗尖，面积9平方公里，主要景点有：屏霞嶂、灵岩寺、天柱峰、展旗峰、灵岩飞渡、小龙湫、龙鼻洞、卓笔峰、独秀峰、卧龙谷。

### 大龙湫

大龙湫景区，东起马鞍岭，西至东岭，南起筋竹涧口，北至凌云尖，被誉为“天下第一瀑”的大龙湫落差为197米。
[大龙湫_-_panoramio_-_Likaihua_(3).jpg](https://zh.wikipedia.org/wiki/File:大龙湫_-_panoramio_-_Likaihua_\(3\).jpg "fig:大龙湫_-_panoramio_-_Likaihua_(3).jpg")
[Yandang_Mountain_Attraction_Distribution.png](https://zh.wikipedia.org/wiki/File:Yandang_Mountain_Attraction_Distribution.png "fig:Yandang_Mountain_Attraction_Distribution.png")

## 文化

庆历年间进士[汪子卿居雁荡山](../Page/汪子卿.md "wikilink")，作《雁山叙别图》。[沈括作有](../Page/沈括.md "wikilink")《雁荡山记》一文，称：“温州雁荡山”“天下奇秀”。明代著名地理学家徐霞客曾三次游历雁荡山，前后写下了两篇《游雁荡山日记》，[徐霞客擲筆歎曰](../Page/徐霞客.md "wikilink")：「欲窮雁蕩之勝，非飛仙不能。」南宋詩人[徐照有五言律詩](../Page/徐照.md "wikilink")[遊雁蕩山八首](../Page/s:遊雁蕩山.md "wikilink")。

## 交通

[甬台温铁路](../Page/甬台温铁路.md "wikilink")（[杭福深客運專線](../Page/杭福深客運專線.md "wikilink")）:[雁荡山站](../Page/雁荡山站.md "wikilink")

## 官方网站

<http://www.wzyds.com/>

[Category:乐清市](../Category/乐清市.md "wikilink")
[Category:温州旅游](../Category/温州旅游.md "wikilink")
[Category:浙江山峰](../Category/浙江山峰.md "wikilink")
[Category:中国国家森林公园](../Category/中国国家森林公园.md "wikilink")
[Category:中国山峰](../Category/中国山峰.md "wikilink")