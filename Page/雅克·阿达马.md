[Hadamard2.jpg](https://zh.wikipedia.org/wiki/File:Hadamard2.jpg "fig:Hadamard2.jpg")
**雅克·所罗门·阿达马**（，）是[法国](../Page/法国.md "wikilink")[数学家](../Page/数学家.md "wikilink")。他最有名的是他的[素数定理证明](../Page/素数定理.md "wikilink")。

## 生平

他在[巴黎高等师范学院学习](../Page/巴黎高等师范学院.md "wikilink")。[德雷福斯事件他也牵涉在内](../Page/德雷福斯事件.md "wikilink")，此后他活跃于政治，坚定支持为犹太人的事業。

他为[偏微分方程创造了](../Page/偏微分方程.md "wikilink")[适定性问题概念](../Page/适定性问题.md "wikilink")。他也給了其名字予论体积的[阿达马不等式](../Page/阿达马不等式.md "wikilink")，还有[阿达马矩阵](../Page/阿达马矩阵.md "wikilink")，是[阿达马变换所以建基的](../Page/阿达马变换.md "wikilink")。[量子计算的](../Page/量子计算.md "wikilink")[阿达马门使用这个矩阵](../Page/阿达马门.md "wikilink")。

1936年曾曾受[清華大學邀請至](../Page/清華大學.md "wikilink")[中國講學讲学](../Page/中國.md "wikilink")3个多月。（同時期者尚有美國數學家[诺伯特·维纳](../Page/诺伯特·维纳.md "wikilink")），影響了中國近代數學發展。

在阿达马所著的《数学领域的发明[心理学](../Page/心理学.md "wikilink")》（*Psychology of
Invention in the Mathematical
Field*），他用[內省來描述数学思维过程](../Page/內省.md "wikilink")。与把[认知和](../Page/认知.md "wikilink")[语言等同的作者截然相反](../Page/语言.md "wikilink")，他描述他的数学思考大部分是无字的，往往有[心象伴随著](../Page/心象.md "wikilink")，浓缩了证明的整体思路。

他学生之一是[安德烈·韦伊](../Page/安德烈·韦伊.md "wikilink")。

## 著作

  - Hadamard, Jacques, "*Psychology of Invention in the Mathematical
    Field*". Dover Pubns; November 1990. ISBN 0-486-20107-4
  - 幾何學教程（）

## 深入阅读

  - *Life and Work of Jacques Hadamard*, Vladimir Maz'ya & T. O.
    Shaposhnikova, American Mathematical Society, February 1998,
    hardcover, 574 pages, ISBN 0-8218-0841-9

[Category:法国数学家](../Category/法国数学家.md "wikilink")
[Category:數論学家](../Category/數論学家.md "wikilink")
[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:巴黎高等師範學院校友](../Category/巴黎高等師範學院校友.md "wikilink")
[Category:路易大帝中学校友](../Category/路易大帝中学校友.md "wikilink")