《**雅典學院**》（），一译《**雅典学派**》，是[義大利文藝復興艺术家](../Page/義大利文藝復興.md "wikilink")[拉斐尔因受任装饰](../Page/拉斐尔·圣齐奥.md "wikilink")[梵蒂冈](../Page/梵蒂冈.md "wikilink")[使徒宫](../Page/使徒宫.md "wikilink")，而在1509年至1510年间创作的一幅[濕壁畫](../Page/濕壁畫.md "wikilink")，位于[拉斐尔房间的签字厅](../Page/拉斐尔房间.md "wikilink")，\[1\]
目前是[梵蒂冈博物馆的一部分](../Page/梵蒂冈博物馆.md "wikilink")。此作被廣泛認定為拉斐爾的代表作之一，象征著[文藝復興全盛期的精神](../Page/文藝復興全盛期.md "wikilink")。\[2\]

## 簡介

這是[教宗命拉斐爾畫的畫](../Page/教宗.md "wikilink")。在透視點的二人分別為[柏拉圖及](../Page/柏拉圖.md "wikilink")[亞里士多德](../Page/亞里士多德.md "wikilink")。人群後、左、右兩邊牆壁上的是[阿波羅及](../Page/阿波羅.md "wikilink")[雅典娜二](../Page/雅典娜.md "wikilink")[神的](../Page/神.md "wikilink")[雕像](../Page/雕像.md "wikilink")。眾人包括[哲學](../Page/哲學.md "wikilink")、[詩歌](../Page/詩歌.md "wikilink")、[音樂](../Page/音樂.md "wikilink")、[神學等](../Page/神學.md "wikilink")[學者](../Page/學者.md "wikilink")，都是教王喜歡的範籌。更加以哲學的殿堂來代表教宗家。

在這件作品中，讓每個哲學家都顯現「個人靈魂」的企圖，用以區別個體之間不同的關係，並將他們連接在形式上的韻律中，處理人與背景相互關係接近[列奥纳多·达·芬奇的做法](../Page/列奥纳多·达·芬奇.md "wikilink")；但整體構圖中出現古典樣式大廳－超高圓頂、酒桶穹窿(Barrel
Vault)、巨大的人像－乃是受到布拉曼特(Bramante)的影響，預言它未來是放置在[圣彼得大教堂中的模樣](../Page/圣彼得大教堂.md "wikilink")。乃透視學的高峰，承襲前人的精華而成。拉斐爾將西方文明不同時期的人集中在同個空間，[古希臘](../Page/古希臘.md "wikilink")、[古羅馬和](../Page/古羅馬.md "wikilink")[作者所在时代](../Page/文艺复兴.md "wikilink")[義大利](../Page/義大利.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")、[藝術家](../Page/藝術家.md "wikilink")、[科學家薈萃一堂](../Page/科學家.md "wikilink")，表現自身篤信人類智慧和諧，並讚美西方文明的智慧結晶。

拉斐爾比達文西、米開朗基羅都晚出生，卻比米開朗基羅早死四十多年，只比達文西晚死一年，是相當短命的藝術家。如果要用幾個字來形容拉斐爾，那就是和諧、圓融、愉快、優美、溫和。不僅畫風如此，待人也是如此。他跟達文西、米開朗基羅一樣，身處愛藝術文化的教皇[朱力阿斯二世的威嚴之下](../Page/儒略二世.md "wikilink")，但是拉斐爾卻跟朱力阿斯二世處的很好，隨後的[利奧十世](../Page/利奧十世.md "wikilink")，也最喜歡他，是個人見人愛的年輕人。25歲那年，朱力阿斯二世邀請拉斐爾為梵諦岡宮的簽字大廳畫壁畫。拉斐爾與教皇、學者們交換意見許久以後，決定依據詩人[德拉·欣雅杜爾的詩來配畫](../Page/德拉·欣雅杜爾.md "wikilink")，以歌頌神學、哲學、詩歌、法學為內容：神學的「聖禮的辯論」、哲學的「雅典學院」、詩歌的「帕拿巴斯山」、法學的「三德」(真理<女人看鏡子>、權力\<腳伏獅子，手拿代表法律的樹枝\>、節制<手拿繩索看天使>)。

「雅典學院」這幅畫中，拉斐爾把不同時期的人全都集中在一個空間，古希臘羅馬和當代義大利五十多位哲學家藝術家科學家薈萃一堂，表現自己篤信人類智慧的和諧、並對人類智慧的讚美。這麼多哲學家集中於一畫面，拉斐爾很聰慧的把不同人物，按其個別的思想特點，以最易讓人理解和感覺的方法繪畫出來。

《雅典學院》整個背景和構圖，如同舞台空間一樣，觀眾面對這幅畫就如同親臨劇場一般，採[透視法以二度空間呈現三度空間的縱深](../Page/透視法.md "wikilink")。拉斐爾將[柏拉圖和](../Page/柏拉圖.md "wikilink")[亞里士多德變成劇中人物](../Page/亞里士多德.md "wikilink")，（他把柏拉圖繪成達文西的臉，表達對達文西的敬重）以他二人為中心，激動人心的辯論場面向兩翼和前景展開。彷彿正在「表演」一齣古希臘思想史，唯心和唯物之爭。

## 画中的著名人物

[thumb|center|800px
|在图中标号的人物中，一些是以当时人物为原型所绘的（9，13，14，21，R），还有一些的人物身份还难以确定（3，4，7，8和10）](../Page/File:Raffaello_Scuola_di_Atene_numbered.svg.md "wikilink")

  - [季蒂昂的芝诺](../Page/季蒂昂的芝诺.md "wikilink")：在伊壁鸠鲁左侧的老者。见**1**
  - [伊壁鸠鲁](../Page/伊壁鸠鲁.md "wikilink")：頭戴葉冠者，主張「人的幸福是追求心靈中永遠的快樂」。见**2**。
  - [阿那克西曼德](../Page/阿那克西曼德.md "wikilink")：畢達哥拉斯左邊的老者是阿那克西曼德。见**4**。
  - [阿維罗伊](../Page/阿維罗伊.md "wikilink")：畢達哥拉斯後方頭纏白巾手放在胸口的老者。见**5**。
  - [數學家](../Page/數學家.md "wikilink")[畢達哥拉斯](../Page/畢達哥拉斯.md "wikilink")：前方蹲著看書的禿頂老人在厚書上寫字者，亦是《毕达哥拉斯定理》發明者。見**6**。
  - [亚历山大大帝](../Page/亚历山大大帝.md "wikilink")：亞歷山大大帝:雙手交叉於胸前，為希臘馬其頓王。金髮，藍色上衣，身穿白袍者。
  - [安提西尼](../Page/安提西尼.md "wikilink")：在色諾芬左邊的是蘇格拉底的門徒安提西尼，犬儒學派的創始人。见**8**。
  - [希帕提婭](../Page/希帕提婭.md "wikilink")：阿維洛伊身旁白衣長髮者是知名古希臘女性數學家希帕提婭，参照了烏爾比諾公爵或者是作者的夫人玛格丽塔所绘。见**9**。
  - [色諾芬](../Page/色諾芬.md "wikilink")：蘇格拉底身旁著衣水藍袍的是軍事與文史學家色諾芬。见**10**。
  - [巴门尼德](../Page/巴门尼德.md "wikilink")：打開書本看著畢氏的是持存在論的思想家巴门尼德。见**11**。
  - 哲人[蘇格拉底](../Page/蘇格拉底.md "wikilink"):穿綠袍轉身向左扳手指與人爭辯者。見**12**。
  - [赫拉克利特](../Page/赫拉克利特.md "wikilink")：最前方中央偏左握笔倚桌思考者，是持流變論的代表人物赫拉克里特，但**拉斐爾**用[米開朗基羅的臉來繪製](../Page/米開朗基羅.md "wikilink")。見**13**。
  - 哲人[柏拉图](../Page/柏拉图.md "wikilink")：以手指指天。拉斐尔以[達芬奇](../Page/達芬奇.md "wikilink")（達文西）为原型绘制的此人物。見**14**。
  - 哲人[亚里士多德](../Page/亚里士多德.md "wikilink")：有人認為他是以米開朗基羅為原型繪製。見**15**。
  - [第欧根尼](../Page/锡诺普的第欧根尼.md "wikilink")：斜躺在階梯上半裸著的老人，是古希臘犬儒學派學者。见**16**。
  - [欧几里得](../Page/欧几里得.md "wikilink")：右下躬著身子，手執圓規量著一個幾何圖形。见**18**。
  - [琐羅亞斯德](../Page/琐羅亞斯德.md "wikilink")：手持[天球儀者](../Page/天球儀.md "wikilink")。见**19**。
  - [天文學家](../Page/天文學家.md "wikilink")[托勒密](../Page/托勒密.md "wikilink")：手持[地球儀者](../Page/地球儀.md "wikilink")。见**20**。
  - [烏爾比諾公爵](../Page/烏爾比諾公爵.md "wikilink")：右下角拉斐爾身旁白衣少年是當時教皇的侄子、有名的藝術愛好者烏爾比諾公爵。见**21**。
  - [拉斐爾](../Page/拉斐爾.md "wikilink")：右下角黑衣戴帽男子為此壁畫作者本人（拉斐尔），象徵藝術乃登入智者的殿堂。見**R**(**R**aphael/**R**affaello)。

<File:Sanzio> 01 Plato
Aristotle.jpg|[柏拉圖與](../Page/柏拉圖.md "wikilink")[亞里士多德](../Page/亞里士多德.md "wikilink")
<File:Sanzio> 01 Heraclitus.jpg|[赫拉克利特](../Page/赫拉克利特.md "wikilink")
<File:Raffael_060.jpg>
<File:Raffael_070.jpg>|[季蒂昂的芝諾或](../Page/季蒂昂的芝諾.md "wikilink")[埃利亞的芝諾](../Page/埃利亞的芝諾.md "wikilink")
<File:Sanzio> 01 Epicurus.jpg|[伊壁鳩魯](../Page/伊壁鳩魯.md "wikilink")
<File:Averroes>
closeup.jpg|[阿維羅伊和](../Page/阿維羅伊.md "wikilink")[畢達哥拉斯](../Page/畢達哥拉斯.md "wikilink")
<File:Raffael> 068.jpg|[畢達哥拉斯](../Page/畢達哥拉斯.md "wikilink")
<File:Raffael>
059.jpg|[阿尔西比亚德斯或](../Page/阿尔西比亚德斯.md "wikilink")[亞歷山大大帝和](../Page/亞歷山大大帝.md "wikilink")[安提西尼或者](../Page/安提西尼.md "wikilink")[色諾芬](../Page/色諾芬.md "wikilink")
<File:Raffael> 065.jpg|以[Francesco Maria della
Rovere或拉斐尔的夫人玛格丽塔为原型的](../Page/:en:Francesco_Maria_I_della_Rovere.md "wikilink")[希帕提婭和](../Page/希帕提婭.md "wikilink")[巴門尼德](../Page/巴門尼德.md "wikilink")
<File:Sanzio_01_Parmenides.jpg>|[巴門尼德](../Page/巴門尼德.md "wikilink")
<File:Raffael>
069.jpg|[安提西尼或](../Page/安提西尼.md "wikilink")[色諾芬和](../Page/色諾芬.md "wikilink")[蘇格拉底](../Page/蘇格拉底.md "wikilink")
<File:Raffael>
066.jpg|以[米開朗基羅为原型的](../Page/米開朗基羅.md "wikilink")[赫拉克利特](../Page/赫拉克利特.md "wikilink")
<File:Raffael>
067.jpg|以[李奧納多·達文西为原型的](../Page/李奧納多·達文西.md "wikilink")[柏拉图](../Page/柏拉图.md "wikilink")
<File:Raffael> 061.jpg|[亞里斯多德](../Page/亞里斯多德.md "wikilink")
<File:Raffael> 062.jpg|[錫諾普的第歐根尼](../Page/錫諾普的第歐根尼.md "wikilink")
[File:Sanzio_01_Euclid.jpg|以](File:Sanzio_01_Euclid.jpg%7C以)[伯拉孟特为原型的](../Page/伯拉孟特.md "wikilink")[歐幾里德或](../Page/歐幾里德.md "wikilink")[阿基米德](../Page/阿基米德.md "wikilink")
<File:Raffael_071.jpg>|[托勒密](../Page/托勒密.md "wikilink")（手持[地球儀者](../Page/地球儀.md "wikilink")）及[琐羅亞斯德](../Page/琐羅亞斯德.md "wikilink")（手持[天球儀者](../Page/天球儀.md "wikilink")）
<File:Raphael> School of Athens GNR.jpg

## 參考文獻

## 參考書目

  - Roger Jones and [Nicholas
    Penny](../Page/Nicholas_Penny.md "wikilink"), *Raphael*, Yale, 1983,
    ISBN 0300030614
  - [Heinrich Wölfflin](../Page/Heinrich_Wölfflin.md "wikilink"),
    *Classic Art: An Introduction to the Italian Renaissance* (London:
    Phaidon, 2d edn. 1953)
  - Inspired [Guns n' Roses](../Page/Guns_n'_Roses.md "wikilink") *Use
    Your Illusion* album's cover
  - In the music video for the song
    [Tessellate](../Page/Tessellate_\(song\).md "wikilink") by the
    british band [Alt-J](../Page/Alt-J.md "wikilink") the director Ben
    Newbury shows an artistic reworking of the painting, using 21st
    century characters of lower socio-economic status in a room similar
    to the painting's background.

## 外部連結

  - [The School of
    Athens](http://www.wga.hu/frames-e.html?/html/r/raphael/4stanze/1segnatu/1/)
    at the *Web Gallery of Art*
  - [The School of
    Athens](https://web.archive.org/web/20070327091619/http://www.clio.unina.it/~alfredo/index.php?mod=05_Interessi%2FLa_Scuola_di_Atene)
    (interactive map)
  - [Cartoon of The School of
    Athens](http://www.learn.columbia.edu/raphael/htm/raphael_athens_cartoon.htm)
  - [The School of Athens reproduction at UNC
    Asheville](https://web.archive.org/web/20070929091018/http://www.unca.edu/news/releases/2007/athens.html)
  - [BBC Radio 4 discussion about the significance of this picture in
    the programme "In Our Time" with Melvyn
    Bragg.](http://www.bbc.co.uk/radio4/history/inourtime/inourtime_20090326.shtml)

[Category:壁画作品](../Category/壁画作品.md "wikilink")
[Category:拉斐尔的绘画作品](../Category/拉斐尔的绘画作品.md "wikilink")
[Category:梵蒂冈绘画作品](../Category/梵蒂冈绘画作品.md "wikilink")
[Category:1510年畫作](../Category/1510年畫作.md "wikilink")

1.  Jones and Penny, 74
2.  [*History of Art: The Western
    Tradition*](http://books.google.com/books?id=MMYHuvhWBH4C&pg=PT470&dq=TIMAEUS+raphael++%22school+of+athens%22++plato+Pythagoras&ei=gS47R9vpNY_g6wL7_6XWCg&sig=R4GMVcpACM4NY6zdu-acOEp_tQI#PPT469,M1)
    By Horst Woldemar Janson, Anthony F. Janson