**雅各布·L·德弗斯** (Jacob "Jake" Loucks
Devers，），美国陆军上將，擅長裝甲戰；他被人最讚回憶是在[第二次世界大戰歐洲戰場他帥領美軍第](../Page/第二次世界大戰.md "wikilink")6軍團自光復法國南部到攻佔德國南部，并在[D日後第一個到達萊茵河的軍官](../Page/D日.md "wikilink")。

## 生平

德弗斯出生在[賓州約克市](../Page/賓州.md "wikilink")，1909年自[西點軍校畢業](../Page/西點軍校.md "wikilink")，成績103之第39名，同班同學有：[巴頓排第](../Page/巴頓.md "wikilink")46名，[中將約翰](../Page/中將.md "wikilink")
C. H.李(John C. H.
Lee)第12名,[西點軍校前任校長](../Page/西點軍校.md "wikilink")[羅伯特·勞倫斯·艾克爾伯格](../Page/羅伯特·勞倫斯·艾克爾伯格.md "wikilink")(Robert
Eichelberger)第68名，跨國企業富商[史丹利·隆堡](../Page/史丹利·隆堡.md "wikilink")(Stanley
Rumbough) (70), [Edwin Forrest
Harding](../Page/Edwin_Forrest_Harding.md "wikilink") (74),
攻佔[柏林中將](../Page/柏林.md "wikilink")[亞歷山大帕奇](../Page/亞歷山大帕奇.md "wikilink")(Alexander
Patch)(不知道第幾名，後段班),北歐作戰中將[威廉胡得辛普森](../Page/威廉胡得辛普森.md "wikilink")(William
H.
Simpson)第101名.在兩次世界大戰中間時候，他全神貫注在[火炮作戰戰術及機械技術改革努力](../Page/火炮.md "wikilink")。

[第二次世界大戰爆發時](../Page/第二次世界大戰.md "wikilink")，他在[巴拿馬美軍部隊領軍](../Page/巴拿馬.md "wikilink")，後來改派至北[加州Fort](../Page/加州.md "wikilink")
Bragg擔任美軍第9步兵師[少將師長](../Page/少將.md "wikilink")，任職時間是自1940年11月15日至1941年；在1941年8月14日，這位美國陸軍最年輕的少將派至[肯塔基州Fort](../Page/肯塔基州.md "wikilink")
Knox軍事基地接掌[坦克部隊](../Page/坦克.md "wikilink")，在他汲汲經營下，Fort
Knox軍事基地發展成為美軍第16坦克師及美軍第63裝甲獨立旅可容納兩單位坦克部隊訓練駐紮基地；1943年5月，德弗斯正式成為[第二次世界大戰美軍在](../Page/第二次世界大戰.md "wikilink")[歐洲戰場](../Page/歐洲.md "wikilink")[中將總司令](../Page/中將.md "wikilink")。在他於英國[倫敦美軍司令部](../Page/倫敦.md "wikilink")，他組織並訓練出數個戰鬥力十足的美軍[步兵師](../Page/步兵.md "wikilink")，在1944年[諾曼地登陸戰勝戰](../Page/諾曼地登陸.md "wikilink")，貢獻居功厥偉。

終於在1944年7月，德弗斯盼望已久領軍野戰部隊可以讓他大顯身手：美軍第6集團軍群，內轄：12個美軍「師」及16單位法軍步兵「師」共約30萬[盟軍大軍](../Page/盟軍.md "wikilink")；他統領這些部隊參與戰役有：「Alsace追擊戰」及「[科爾馬包圍戰](../Page/科爾馬包圍戰.md "wikilink")」、橫渡萊茵河的[戰利品行動](../Page/戰利品行動.md "wikilink")，最後在1945年5月6日在[奧地利西部接受潰不成軍的德軍獻降](../Page/奧地利.md "wikilink")。

德弗斯在他超過35年軍旅生涯顯現他高度才華及軍事專業領袖群倫，可資模範學習；
1940年5月晉升[准將](../Page/准將.md "wikilink")；

1940年10月晉升少將；

1942年9月晉升中將；

1945年5月8日[納粹德國投降那天](../Page/納粹德國.md "wikilink")，晉升[上將](../Page/上將.md "wikilink")

1949年9月30日退役。

德弗斯逝於[華盛頓特區](../Page/華盛頓特區.md "wikilink")，最後葬在[阿靈頓國家公墓](../Page/阿靈頓國家公墓.md "wikilink")。

## 參考聯結

  - [Army.mil: Jacob L.
    Devers](https://web.archive.org/web/20070520225856/http://cgsc.leavenworth.army.mil/carl/resources/ftlvn/ww2.asp#devers)
  - [Lost Victory - Strasbourg,
    November 1944](https://web.archive.org/web/20060528203232/http://www.efour4ever.com/44thdivision/bridgehead.htm)

[D](../Category/1887年出生.md "wikilink")
[D](../Category/1979年逝世.md "wikilink")
[Category:西點軍校校友](../Category/西點軍校校友.md "wikilink")
[D](../Category/美國陸軍上將.md "wikilink")
[D](../Category/賓夕法尼亞州人.md "wikilink")