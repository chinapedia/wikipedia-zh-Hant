**雅库茨克**（；[雅庫特語](../Page/雅庫特語.md "wikilink")：）是[俄羅斯](../Page/俄羅斯.md "wikilink")[薩哈共和國首府](../Page/薩哈共和國.md "wikilink")，位於[勒拿河西岸](../Page/勒拿河.md "wikilink")，2010年人口269,601。

## 歷史

雅库茨克建立於1632年，\[1\]當時還只是個[哥薩克人構築的邊防](../Page/哥薩克人.md "wikilink")[堡壘](../Page/堡壘.md "wikilink")。不過隨著大量的金礦與其他礦產的[礦脈於](../Page/礦脈.md "wikilink")1880年代到1890年代之間被發現之後，雅庫茨克的人口就隨著開礦的人口大幅地增加了起來。但史達林執政後，雅庫次克的人口又再一次因為重工業發展與政策的結合而下跌，對於礦脈的密集開發因此對於人力的需求而蓬勃增加。

不過雅庫次克的這一波人口擴增又包括另外一個可笑又悽涼的背景，先有了雅庫次克，又有了礦脈的發現，加上政策對於礦產的需求，因此[西伯利亞週邊的強制勞動](../Page/西伯利亞.md "wikilink")[集中營](../Page/集中營.md "wikilink")（forced
labor camp）的數量又增加了，因此開發礦產的人力與擴展雅庫次克的腹地兩個條件就一次達成了。

## 氣候

雅库茨克属[亚寒带针叶林气候](../Page/亚寒带针叶林气候.md "wikilink")，是世界最寒冷的城市之一，冬季天气相當寒冷，1月平均气温接近-40°C，最低可降至-60°C以下，极端最低气温-64.4°C；夏季天氣普遍凉爽，最高气温一般不超過30°C，但有時氣溫也可以高至35-38°C，极端最高气温38.4°C。[气温绝对年较差达](../Page/气温绝对年较差.md "wikilink")102.8°C，是世界气温绝对年较差最大的城市，氣候相當極端。

<div style="width:80%;">

</div>

## 友好城市

  - [阿拉斯加州](../Page/阿拉斯加州.md "wikilink")[費爾班克斯](../Page/費爾班克斯_\(阿拉斯加州\).md "wikilink")

  - [黑森州](../Page/黑森州.md "wikilink")[达姆施塔特](../Page/达姆施塔特.md "wikilink")

  - [山形县](../Page/山形县.md "wikilink")[村山市](../Page/村山市.md "wikilink")

  - [庆尚南道](../Page/庆尚南道.md "wikilink")[昌原市](../Page/昌原市.md "wikilink")

  - [黑龙江省](../Page/黑龙江省.md "wikilink")[哈尔滨市](../Page/哈尔滨市.md "wikilink")

## 註解

## 外部連結

  - [Official website of
    Yakutsk](https://web.archive.org/web/20151101174015/http://yakutsk-city.ru/)

  - [Sakha Life Information Agency](http://www.sakhalife.ru/)
  - [Lena
    Pillars](https://web.archive.org/web/20080702081926/http://www.nhpfund.org/nominations/lena_pillars.html)
    at [Natural Heritage Protection Fund](http://www.nhpfund.org/)
  - [Flickr photos tagged
    Yakutsk](http://www.flickr.com/photos/tags/yakutsk)

[Я](../Category/萨哈共和国城市.md "wikilink")

1.