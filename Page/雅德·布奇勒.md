**贾德森·唐纳德·布奇勒**（，），[美国前职业](../Page/美国.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，场上位置[锋卫摇摆人](../Page/锋卫摇摆人.md "wikilink")，曾随[芝加哥公牛队夺得三次](../Page/芝加哥公牛队.md "wikilink")[NBA总冠军](../Page/NBA总冠军.md "wikilink")。

布奇勒出生于[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[圣迭戈](../Page/圣迭戈_\(加利福尼亚州\).md "wikilink")，大学就读于[亚利桑那州立大学](../Page/亚利桑那州立大学.md "wikilink")。1990年，布奇勒在[NBA选秀中于第](../Page/1990年NBA选秀.md "wikilink")2轮第38顺位被[西雅图超音速队选中](../Page/西雅图超音速队.md "wikilink")，并被立即交换到[新泽西网队](../Page/新泽西网队.md "wikilink")。在一个赛季后，布奇勒被网队放弃。

在[圣安东尼奥马刺队打了几场短工后](../Page/圣安东尼奥马刺队.md "wikilink")，布奇勒同[金州勇士队签约](../Page/金州勇士队.md "wikilink")，1995年，他同芝加哥公牛队签约。在三连冠后，他转投[底特律活塞队](../Page/底特律活塞队.md "wikilink")，后又先后效力于[菲尼克斯太阳队和](../Page/菲尼克斯太阳队.md "wikilink")[奥兰多魔术队](../Page/奥兰多魔术队.md "wikilink")，2002年退役。

## 参考资料

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:新泽西网队球员](../Category/新泽西网队球员.md "wikilink")
[Category:芝加哥公牛队球员](../Category/芝加哥公牛队球员.md "wikilink")
[Category:圣安东尼奥马刺队球员](../Category/圣安东尼奥马刺队球员.md "wikilink")
[Category:金州勇士队球员](../Category/金州勇士队球员.md "wikilink")
[Category:底特律活塞队球员](../Category/底特律活塞队球员.md "wikilink")
[Category:菲尼克斯太阳队球员](../Category/菲尼克斯太阳队球员.md "wikilink")
[Category:奥兰多魔术队球员](../Category/奥兰多魔术队球员.md "wikilink")