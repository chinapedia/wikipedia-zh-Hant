[IELTSlogo.gif](https://zh.wikipedia.org/wiki/File:IELTSlogo.gif "fig:IELTSlogo.gif")
**雅思**（，），全称为**國際英語測試系统**（），是著名的国际性[英语标准化水平测试之一](../Page/英语.md "wikilink")。IELTS于1989年设立，由[澳大利亚教育国际开发署(IDP)](https://www.ieltstaiwan.org)和[剑桥大学考试委员会和](../Page/剑桥大学考试委员会.md "wikilink")[英國文化協會共同举办](../Page/英國文化協會.md "wikilink")，其中剑桥大学负责有关学术水平及试题内容，而IDP及英国文化协会负责于世界各地定期举办考试。考生可以选择学术类测试（A类，）和培训类测试（G类，）。雅思成绩被[英国](../Page/英国.md "wikilink")、[爱尔兰以及](../Page/爱尔兰共和国.md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[南非等](../Page/南非.md "wikilink")[英联邦成員及越来越多的](../Page/英联邦.md "wikilink")[美国的教育机构和各种各样的专业组织接受](../Page/美国.md "wikilink")。雅思考试成绩的有效期限为两年。

## 雅思的特点

雅思集合下列几项特色：

  - 多种口音和写作风格在书面上出现，其目的是为了减少考试对语言学上的偏爱。考试注重的是国际英语，在听力测试中表现的最为显著，听力测试的口音主要包括了[英国口音](../Page/英國英語.md "wikilink")、[美国口音](../Page/美国英语.md "wikilink")、[澳洲口音及](../Page/澳大利亚英语.md "wikilink")[新西兰口音等类型](../Page/新西兰英语.md "wikilink")，相比之下[托福则更注重北美英语](../Page/托福.md "wikilink")（[美国英语](../Page/美国英语.md "wikilink")、[加拿大英语](../Page/加拿大英语.md "wikilink")）。
  - 雅思测试包括：听、说、读、写的能力。
  - 两种模式可供选择：学术类（A）和培训类（G）
      - 学术类测试是为那些想进入[大学或其它](../Page/大学.md "wikilink")[高等教育机构的人士而設](../Page/高等教育机构.md "wikilink")。
      - 培训类测试則是为那些计划开始非学术性的训练，获得工作经验，或者以[移民為目的的人士而設](../Page/移民.md "wikilink")。
  - 每种语言技巧都有相应的分数（听，说，读，写）。总分的范围从1（不懂英语）到9（母语专家程度），如果缺考、白卷或者因作弊被取消成績，則為0分。

## 考試形式

IELTS雅思考試共分聽、說、讀、寫四項科目。早上9AM起進行聽力、閱讀、寫作，約在中午完成；下午則依序進行口语考試。

不論IELTS雅思考試或IELTS for UKVI用於英國簽證及移民的雅思考試，均分Academic學術及General
Training一般訓練兩組，兩組的聽力與口语考題是相同的，閱讀與寫作的題目則因組別而異。

筆試開始時，考官會發給考生一張答題卡，正面和反面分別用來回答聽力和閱讀的題目。

### 聽力測驗

首先進行的聽力測驗，題目共分四個部分。第一個部分是日常生活中會發生的對話（通常為二人），第二個部分是生活相關的獨白（有时亦会有另一方的出现，通常是主持人，引导独白）；第三個部分是基于教育教学热点话题的學術性的對話（最多四人），第四個部分為學術論文演講，難度依次增加（亦為了再篩選精英，有時有些句子會帶重口音或道地詞語）。通常前三個部分都會分成兩段，分別回答不同的問題。考生在聽完每段錄音後會有一小段時間複查（但因為不會重複，所以要即時寫出答案）。全部錄音放完需時30分鐘，剩餘10分鐘供考生將答案從試卷填寫到答題卡上。時間到了之後考官會把試卷收上來，並要求考生將答題卡翻過來。

### 閱讀能力測驗

聽力測驗之後是閱讀測驗，時間60分鐘，總共40題。

學術組包含三篇文章，每篇約800\~1000字，文章取材於學術刊物；一般訓練組包含三到六篇短文，含公告、廣告、簡介等形式

答題方式亦包含配對、是非、填充、選擇、簡答、標籤題等

### 寫作能力測驗

閱讀測驗之後是寫作測驗，時間60分鐘。

學術組的Task 1為圖表練習，以描述及比較各類圖表為主，考生需於20分內寫完150字的描述。Task 2
則為申論題，範圍廣且有深度，考生需按題目分析及表達個人意見或做優缺點比較等。

一般訓練組的Task 1則與A組有別，通常為書信的撰寫如：邀請函，投訴信，道歉信等，至少150字。而Task 2就與A組類似，為申論題

### 口语能力測驗

口语測驗是一對一進行，主考官會首先就考生的一些個人問題發問，並選擇話題加以展開。到一定程度後，考官出示題目卡，要求考生就題目所涉及內容進行回答並適當展開論述，時間不少於一分鐘。最後，考官會就一些深入的話題與考生進行討論，以考察考生的應對能力。總長度時限為14分鐘。

考試分為3個部份：

**第一部份**－考生和考官先做自我介紹。接著，考生將回答有關其本身的一些個人問題，包括居住的地方、家人、工作/學業情況、嗜好以及任何其他相關的問題。這部份的會話時間是4－5分鐘。

**第二部份**－考生將拿到一個題目和一些提示，必須針對特定題目發言。考生有1分鐘的準備時間，并可以做筆記。發言時間是1-2分鐘。考生發言完畢后，考官將提出一兩個問題。

**第三部份**－考官和考生針對比較深奧的課題進行討論，而這些課題的主題將與第二部份的題目有關。討論時間是4－5分鐘。

注：在某些情况下，口试考场和笔试考场不在同一个场地。（根据自己选择的考场而定）

### 考前模拟

对于学生而言，考前通过用往届雅思真题模拟，可以了解自己在哪个单项上薄弱，通过针对性的训练，平均可以提高0.5-1.0。\[1\]
目前国内外有众多线上线下辅导机构可帮助学生做考前准备。其中知名的线上机构包括国内新东方\[2\]、考满分\[3\]，以及国外洋学伴\[4\]、普林斯顿评论\[5\]等。

### 考试时长

考试的全部时长约为**2小时45分钟**，包括听力、阅读和写作。

  - **听力：40分钟**，30分钟为录音播放，剩下的10分钟填写OMR答题卡。
  - **阅读：60分钟**
  - **写作：60分钟**
  - **口說：11–14分钟**

（注意：阅读与写作没有为填写答题卡设立专门时间段）

前三部分 — 听力、阅读、写作 — 在同一天内完成，期间没有间歇。口试可以在上述考试的前七天或后七天组织进行，具体由则由考试中心视情况决定。

### 評分標準

聽力考試和閱讀考試為標準化考試，根據答對題目的多少進行評分，允許有0.5分存在。寫作和口語則由考官根據標準評定，但過往則沒有0.5分；但由2007年7月1日起，寫作和口語允許有0.5分存在。\[6\]

將所有4個科目的成績求平均，並四捨五入到0.5分，就得到本次考試的總評分數。若達.25分則四捨五入至.5分，若達.75分則四捨五入至下一滿分（例：若平均分數為6.375，則總評分數會顯示6.5；但如果平均分數為6.125,則總評分數會顯示6.0）。

### 计分转换表

该计分转换表可用于听力测试中原始分值到成绩分数的转换。该表只用于参考，因为成绩可能会因考题的难易而有所不同。

| 成绩   | 9.0   | 8.5   | 8.0   | 7.5   | 7.0   | 6.5   | 6.0   | 5.5   | 5.0   | 4.5   | 4.0   | 3.5 | 3.0 | 2.5 |
| ---- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | --- | --- | --- |
| 原始分值 | 39－40 | 37－38 | 35－36 | 32－34 | 30－31 | 26－29 | 23－25 | 18－22 | 16－17 | 13－15 | 10－12 | 8－9 | 6－7 | 4－5 |

## 考试地点和日期

雅思在全球有300所考试中心。每年有多达48个考试日期，根据当地考生的需求，每个考点可以最多接受4次考试。
2006年雅思改變規則，取消了原来两次连续考试必须间隔90日的规定。\[7\]。

## 考生人数

2007年IELTS全球约有93.8万考生参加考试，是三年前的两倍。中国地區近年來在IELTS考试呈现增长，2007全年共逾21万人参加考试，较06年比增长近60%，为历史新高\[8\]。2017年全球已突破300萬人參加雅思考試。

## 考试费用

雅思考试具体价格依全球不同地区而异。英国参加每次雅思考试所需费用大约为130[英镑](../Page/英镑.md "wikilink")；在中国内地，从2014年4月1日起，雅思考试费提升至1700元人民币，2016年9月1日起提升至1,960元人民币\[9\]；在香港，從2013年4月1日起，考試費提升至1700元港幣，2016年9月1日起提升至1,930元港幣，2017年9月1日起提升至2,010元港幣。\[10\]；在台灣則為新台幣6500元，並從2018年1月起，費用調整為新台幣7000元。

## 全球成绩

### 高分国家

2011年，学术雅思平均分前五名的国家为：\[11\]

|    |                                    |     |
| -- | ---------------------------------- | --- |
| 排名 | 国家                                 | 成绩  |
| 1  | [马来西亚](../Page/马来西亚.md "wikilink") | 6.9 |
| 2  | [羅馬尼亞](../Page/羅馬尼亞.md "wikilink") | 6.9 |
| 3  | [法国](../Page/法国.md "wikilink")     | 6.8 |
| 4  | [德国](../Page/德国.md "wikilink")     | 6.8 |
| 5  | [巴西](../Page/巴西.md "wikilink")     | 6.7 |
| 6  | [肯尼亚](../Page/肯尼亚.md "wikilink")   | 6.7 |
| 7  | [菲律宾](../Page/菲律宾.md "wikilink")   | 6.7 |

### 高分母语考生

2011年，学术雅思的高分母语考生群体为：\[12\]

|    |                                      |     |
| -- | ------------------------------------ | --- |
| 排名 | 母语                                   | 成绩  |
| 1  | [德语](../Page/德语.md "wikilink")       | 7.2 |
| 2  | [英语](../Page/英语.md "wikilink")       | 7.1 |
| 3  | [罗马尼亚语](../Page/罗马尼亚语.md "wikilink") | 6.9 |
| 4  | [保加利亚语](../Page/保加利亚语.md "wikilink") | 6.8 |
| \- | [波兰语](../Page/波兰语.md "wikilink")     | 6.8 |
| 5  | [卡纳达语](../Page/卡纳达语.md "wikilink")   | 6.7 |
| \- | [葡萄牙语](../Page/葡萄牙语.md "wikilink")   | 6.7 |
| \- | [他加祿語](../Page/他加祿語.md "wikilink")   | 6.7 |

## 院校录取

超过半数（51%）的考生最终选择出国留学。雅思的最低成绩要求因院校而不同。

### 美国

对雅思成绩要求最高的是[哥伦比亚大学新闻学院](../Page/哥伦比亚大学.md "wikilink")，为8.5。\[13\]这是美国唯一要求如此高成绩的院校。[俄亥俄州立大学的莫里茨法学院在雅思官网上要求成绩为](../Page/俄亥俄州立大学.md "wikilink")8.5，但学校则要求8.0。\[14\]圣路易斯大学的最低分为6。

### 英國

对雅思分数要求最高的是[华威大学的市场营销硕士](../Page/华威大学.md "wikilink")，为8分。\[15\]绝大多数大学对雅思的要求在5.5到7.0之间，例如：

|                                            |                                           |
| ------------------------------------------ | ----------------------------------------- |
| 大学                                         | 最低分                                       |
| [牛津大学](../Page/牛津大学.md "wikilink")         | 7.0 / 7.5\[16\]                           |
| [倫敦政治經濟學院](../Page/倫敦政治經濟學院.md "wikilink") | 7.0 / 7.5（依学部的要求而不同）                      |
| [曼徹斯特大学](../Page/曼徹斯特大学.md "wikilink")     | 7.0 / 7.5（依学部的要求而不同）                      |
| [爱丁堡大学](../Page/爱丁堡大学.md "wikilink")       | 7.0（所有商科、管理、财经、法律、英语/凯尔特/苏格兰语言文化研究）\[17\] |
| [剑桥大学](../Page/剑桥大学.md "wikilink")         | 7.0 / 7.5\[18\]                           |
| [格拉斯哥大学](../Page/格拉斯哥大学.md "wikilink")     | 6.5（综合） / 7.0（人文）\[19\]                   |
| [伦敦大学学院](../Page/伦敦大学学院.md "wikilink")     | 6.5 / 7.0 / 7.5（依学部的要求而不同）                |
| [伦敦帝国学院](../Page/伦敦帝国学院.md "wikilink")     | 6.5（7.0－生命科学与商学院）                         |
| [艾希特大學](../Page/艾希特大學.md "wikilink")       | 7.0                                       |
| [利物浦大学](../Page/利物浦大学.md "wikilink")       | 6.0\[20\]                                 |
| [伯明翰大学](../Page/伯明翰大学.md "wikilink")       | 6.5                                       |
| [艾塞克斯大學](../Page/艾塞克斯大學.md "wikilink")     | 5.5                                       |
| 克兰菲尔德大学                                    | 6.5 / 7.0（依硕士项目的不同而异）                     |

### 阿根廷

[布宜诺斯艾利斯大学对英语作为第二外语的要求是雅思最低成绩](../Page/布宜诺斯艾利斯大学.md "wikilink")7.5。

### 德国

[班貝格大學要求雅思最低成绩为](../Page/班貝格大學.md "wikilink")7.0（欧洲双硕士：英美研究）。绝大多数德国大学硕士学位对雅思的最低成绩要求为6.0或6.5。

[曼海姆大學](../Page/曼海姆大學.md "wikilink") BWL專業本科要求雅思最低成绩为6.0

### 香港

[香港律師會的香港法學專業證書由](../Page/香港律師會.md "wikilink")[香港大学](../Page/香港大学.md "wikilink")、[香港中文大学](../Page/香港中文大学.md "wikilink")、[香港城市大学教授](../Page/香港城市大学.md "wikilink")，录取的最低要求是雅思成绩7.0。[香港中文大学对两门商科提前录取的成绩要求是](../Page/香港中文大学.md "wikilink")7.0，它们是“環球商業學”与“國際貿易與中國企業”。

### 意大利

[都灵理工大学对雅思成绩的最低要求是本科](../Page/都灵理工大学.md "wikilink")5.0，研究生5.5。意大利绝大多数大学对本科生的最低要求是4.5，硕士生则为6.5。页面文本。\[21\]

### 哈萨克斯坦

纳扎巴耶夫大学以雅思最低成绩为6.0作为升入医学院、工程院的要求。人文社科院则要求6.5。

### 荷兰

[代尔夫特理工大学对所有](../Page/代尔夫特理工大学.md "wikilink")[理学士雅思最低要求是](../Page/理学士.md "wikilink")5.5，[航空航天工程与纳米生物则要求](../Page/航空航天工程.md "wikilink")6.5。[阿姆斯特丹大學的社会科学院则要求所有国际硕士的最低成绩](../Page/阿姆斯特丹大學.md "wikilink")6.5，并且每个单项成绩也在6.0之上。\[22\]这项标准对于当地愿意参加国际硕士的学生来说同样适用。

### 比利时

[荷语天主教鲁汶大学绝大多数硕士项目对雅思成绩的最低要求是](../Page/荷语天主教鲁汶大学.md "wikilink")6.5-7.5。\[23\]
[根特大学绝大多数硕士项目对雅思的最低要求是](../Page/根特大学.md "wikilink")5.5-6.5。博士项目则可能要求7.0。\[24\]

## 移民

许多[英联邦成员国使用雅思作为移民英语合格的证明](../Page/英联邦.md "wikilink")。\[25\]

### 澳大利亚

澳大利亚移民局从1998年5月起使用雅思考察移民申请人的英语水平，考试替代了之前所使用的“***access:*** test”。\[26\]

2012年7月，澳大利亚独立移民签证（永久居民）申请人必须在雅思的各部分达到至少6分，或是在澳大利亚职业英语考试（Occupational
English Test）上成绩打A才可以通过。\[27\]

如果考试成绩没有达到合格标准，考生可以获得部分认可。移民申请人若是来自英语国家（英國、加拿大、新西兰等国）可以自动获得英语合格成绩。如果他们希望获得英语精通成绩，就必须通过雅思或OET考试。\[28\]

### 新西兰

新西兰自1995年开始使用雅思考试。起初，申请人需要在四部分上获得5分成绩，否则需要交纳NZ$20,000。如果在一定的期限内（3－12个月），申请人通过了考试，就会获得部分或全部的费用返还。几年后，这项政策被修改：费用减少了，并作为“先上车、后买票”的雅思学费。\[29\]

目前，新西兰投资移民申请人必须提供“合适的”英语水平证明。除非申请人在新西兰或其他英语国家学习或工作了一定长的时间，那么就得在雅思综合成绩上考取4分，高额投资移民只需要3分。\[30\]

### 加拿大

[加拿大公民及移民部使用雅思](../Page/加拿大公民及移民部.md "wikilink")/[TEF作为申请人英语](../Page/法語水準測試.md "wikilink")/法语合格的证明。\[31\]就技术移民记点考试而言，申请人在每个项目上（阅读、写作、口语、听力）单独计分，雅思每个项目的4分都可以在记点考试上兑换为满分，而听力部分需要雅思8分来兑换记点考试的满分。\[32\]

根据加拿大移民申请表上所述，思陪考试（加拿大英语水平指数考试：Canadian English Language Proficiency
Index Program，CELPIP）成绩可以代替雅思考试。\[33\]

在申请[加拿大国籍时](../Page/加拿大國籍法.md "wikilink")，递交雅思成绩是证明自己在官方语言上合格的一种方式。\[34\]

### 英國（不列颠）

不列颠“Points Based System Tier
1”（普通移民）项目中，申请人在考试中若获得10分，就可得到“等同于[欧洲共同语言框架中的C](../Page/歐洲共同語言參考標準.md "wikilink")1级别”的认证，或是等同于雅思的6.5，或GCSE的C级。在以英语为授课语言的大学获得学位也是另一种英语水平合格的证明。\[35\]

## 参见

  - [統一英語水平評核計劃](../Page/統一英語水平評核計劃.md "wikilink")——在[香港推廣IELTS的一個計劃](../Page/香港.md "wikilink")
  - [语言能力考试列表](../Page/语言能力考试列表.md "wikilink")

## 參考資料

## 外部链接

### 官方网站

  - [IELTS官方网站](http://www.ielts.org)

  - [雅思考试中國官方网站](http://www.chinaielts.org/)

  - [台灣IELTS雅思官方考試中心-IDP國際教育中心](http://www.ieltstaiwan.org/)

  - [香港英國文化協會特別為IELTS設立的網站](https://ielts.britishcouncil.org.hk/)

### 考試中心

  - [英国大使馆文化教育处 —
    中国](https://web.archive.org/web/20100526224426/http://www.britishcouncil.org/zh/china.htm)

  - [英國文化協會 — 香港](http://ielts.britishcouncil.org.hk)

  - [IDP教育—香港](http://hongkong.idp.com/hong_kong/ielts國際英國測試_雅思.aspx)

  - [台灣IELTS雅思官方考試中心-IDP國際教育中心](http://www.ieltstaiwan.org/)

  - [IELTS雅思授權考試中心](http://www.ielts.com.tw)

[Category:英國教育](../Category/英國教育.md "wikilink")
[Category:英語考試](../Category/英語考試.md "wikilink")
[Category:語言考試](../Category/語言考試.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10. [考試日期、費用及地點](https://www.britishcouncil.hk/exam/ielts/dates-fees-locations)，British
    Council。

11.

12.

13.

14. [OSU.edu](http://professional.osu.edu/law.asp#eng)

15.
16. [Oxford University, English language
    requirements](http://www.ox.ac.uk/admissions/undergraduate_courses/courses/courses_and_entrance_requirements/english_language.html)

17. [ED.ac.uk](http://www.ed.ac.uk/studying/postgraduate/international/language)

18. [Cambridge University, Undergraduate Admissions: Entrance
    requirements for international
    students](http://www.cam.ac.uk/admissions/undergraduate/international/requirements.html)

19. [Glasgow University, English as a foreign
    language](http://www.gla.ac.uk/international/inyourregion/englishasaforeignlanguage/)


20. [Liverpool University English language entry
    requirements](http://www.liv.ac.uk/international/countries/english-language.htm)


21. [链接文本](http://apply.polito.it/language_requirements.html) ，附加文本。

22. [ielts](https://www.language-center.com.tw/ielts-online.html)

23. [Proficiency tests in
    English](http://www.kuleuven.be/admissions/language/lang_test.html)
    , KU Leuven. Retrieved 2013-05-17.

24. [Specific Language
    Requirements 2012-2013](http://www.ugent.be/en/teaching/studying/languageofinstruction/proficiency/specificllanguage.pdf/view)
    , Ghent University. Retrieved 2013-05-17.

25. [Who accept IELTS? Government
    Agencies](http://bandscore.ielts.org/government.aspx)

26.

27. [Skilled – Independent (Migrant) Visa (Subclass 175): English
    language
    ability](http://www.immi.gov.au/skilled/general-skilled-migration/175/eligibility-english.htm)
    (Australia's Department of Immigration; checked 2010-07-08)

28.
29.

30. <http://www.immigration.govt.nz/migrant/stream/alreadyinnz/business/englishlanguagerequirements/>
    Applying for residence under the Entrepreneur Category: English
    language requirements\] ([Immigration New
    Zealand](../Page/Immigration_New_Zealand.md "wikilink"); checked
    2010-07-08)

31. [Language
    Testing](http://www.cic.gc.ca/english/immigrate/cec/language-testing.asp)
    Date Modified: 2010-07-07. (CIC, checked 2010-07-17)

32. [Application for permanent residence: Federal skilled worker class
    (IMM 7000)](http://www.cic.gc.ca/english/information/applications/guides/EG72.asp#results)
    , Date Modified: 2010-06-24. (CIC, checked 2010-07-17)

33.
34. [Determine your eligibility –
    Citizenship](http://www.cic.gc.ca/english/citizenship/become-eligibility.asp)
    (Date Modified: 2013-02-06)

35. [Guidance - Points Based System Tier 1, General Migrant (INF 21).
    Last updated 6
    April 2010](http://www.ukvisas.gov.uk/en/howtoapply/infs/inf21pbsgeneralmigrant#21980509)