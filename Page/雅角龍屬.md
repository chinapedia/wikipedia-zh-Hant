**雅角龍屬**（[學名](../Page/學名.md "wikilink")：*Graciliceratops*）是[原角龍科](../Page/原角龍科.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，目前只有發現一個部份骨骼，[化石發現於](../Page/化石.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")，地質年代估計是屬於[上白堊紀](../Page/上白堊紀.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")**蒙古雅角龍**（*G.
mongoliensis*），也是目前的唯一種，是由[古生物學家](../Page/古生物學家.md "wikilink")[保羅·塞里諾](../Page/保羅·塞里諾.md "wikilink")（Paul
Sereno）在2000年所描述、命名的。

## 分類

雅角龍屬於[角龍下目](../Page/角龍下目.md "wikilink")，角龍下目是一類[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，擁有類似[鸚鵡的喙狀嘴](../Page/鸚鵡.md "wikilink")，主要生活於[白堊紀的](../Page/白堊紀.md "wikilink")[北美洲及](../Page/北美洲.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")。所有角龍類恐龍都在白堊紀末期[滅絕](../Page/滅絕.md "wikilink")，約6500萬年前。

## 食性

如同其他[角龍下目](../Page/角龍下目.md "wikilink")，雅角龍是[草食性恐龍](../Page/草食性.md "wikilink")。在[白堊紀時期](../Page/白堊紀.md "wikilink")，[開花植物的地理範圍有限](../Page/開花植物.md "wikilink")，所以雅角龍可能以當時的優勢植物為食，例如：[蕨類](../Page/蕨類.md "wikilink")、[蘇鐵](../Page/蘇鐵.md "wikilink")、[松科](../Page/松科.md "wikilink")。牠們可能使用銳利的喙狀嘴咬下樹葉或針葉。

## 參考

  - <https://web.archive.org/web/20071005145847/http://www.dinosaurier-web.de/galery/pages_g/graciliceratops.html>
  - <https://web.archive.org/web/20091207085409/http://www.thescelosaurus.com/ceratopsia.htm>
  - <http://web.me.com/dinoruss/de_4/5cec564.htm>

## 外部連結

  - [雅角龍](https://web.archive.org/web/20071210140343/http://www.nhm.ac.uk/jdsml/nature-online/dino-directory/detail.dsml?Genus=Graciliceratops)
  - [恐龍博物館——雅角龍](https://web.archive.org/web/20071109033304/http://www.dinosaur.net.cn/museum/Graciliceratops.htm)

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:冠飾角龍類](../Category/冠飾角龍類.md "wikilink")