[缩略图](https://zh.wikipedia.org/wiki/File:Ahmose-mummy-head.png "fig:缩略图")\]\]
**雅赫摩斯一世** （[英语文献中一般写作](../Page/英语.md "wikilink")：*Ahmose
I*；？—约前1525年）[古埃及](../Page/古埃及.md "wikilink")[第十八王朝创立者](../Page/埃及第十八王朝.md "wikilink")（约前1550年—约前1525年在位）。

雅赫摩斯一世是[埃及第十七王朝最后一位法老](../Page/埃及第十七王朝.md "wikilink")[卡摩斯的弟弟](../Page/卡摩斯.md "wikilink")。他继承兄长的事业，将[喜克索斯人彻底逐出埃及](../Page/喜克索斯人.md "wikilink")，使埃及获得复兴。雅赫摩斯在位时对[努比亚进行了多次远征](../Page/努比亚.md "wikilink")，从而恢复了埃及对该地区的传统控制。他的王朝标志着埃及史上[新王国时期的开始](../Page/新王国.md "wikilink")。

关于雅赫摩斯一世的统治，最重要的文物是[埃班之子雅赫摩斯的墙上铭文](../Page/埃班之子雅赫摩斯.md "wikilink")。这是一位与法老同名的船长的英雄事迹，这位船长曾追随雅赫摩斯一世，并参加了他的大多数战役。

## 法老的名字

雅赫摩斯一世的名字包括两部分：其中一个是[王衔](../Page/王衔.md "wikilink")（praenomen，或者叫假名），另一个是真名。雅赫摩斯一世的真名是雅赫摩斯（Ahmose），意思是“月神之子”。他的王衔是内布普赫提拉
（Nebpehtire）， 这是一个与埃及太阳神[拉有关的名字](../Page/拉.md "wikilink")。

## 注释

<references/>

[Category:第十八王朝法老](../Category/第十八王朝法老.md "wikilink")
[Category:古埃及木乃伊](../Category/古埃及木乃伊.md "wikilink")
[Category:開國君主](../Category/開國君主.md "wikilink")