**Atari
Lynx**是生產商[雅達利於](../Page/雅達利.md "wikilink")1989年發佈的手提式[遊戲機](../Page/遊戲機.md "wikilink")，是世上首款設有[彩色](../Page/彩色.md "wikilink")[液晶顯示屏的手提電子遊戲機](../Page/液晶顯示屏.md "wikilink")，與[任天堂的](../Page/任天堂.md "wikilink")[GameBoy遊戲機同年推出](../Page/GameBoy.md "wikilink")。及後由於銷量並不理想，即使於1990年推出改良版Lynx
II，但仍未能吸引遊戲開發商生產足夠的遊戲，而最終隨Atari與[JT
Storage合併而消失於市場](../Page/JT_Storage.md "wikilink")。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：[MOS](../Page/MOS_Technology.md "wikilink")
    65SC02，最高運作頻率為4 MHz（平均以約3.6 MHz運作）
      - 8位元處理器，設有16位元的記憶體定址能力
      - 音效處理引擎
          - 4聲道音效
          - 8位元DAC／每聲道（4聲道 × 8位元DAC／每聲道 = 常列為32位元音效）
      - LCD的視訊DMA驅動程式
          - 4,096色、12位元色板
          - 單一掃瞄線能同時顯示最多16色的單一色板（4位元），經色板轉換後可顯示多於16色
      - 8個系統計時器，當中2個供LCD計時用，1個為UART用
      - 設有[中斷要求控制器](../Page/中斷要求.md "wikilink")
      - UART（為ComLynx而設）
      - 512位元組[唯讀記憶體供開機程序及遊戲卡暫存](../Page/唯讀記憶體.md "wikilink")
  - Suzy 16位元可編程[CMOS晶片](../Page/CMOS.md "wikilink")，以16 MHz運作
      - 顯示引擎
          - 硬體繪圖顯示加速
          - 無限數量的高速圖像單元（Sprite）支援，設有碰撞偵測功能
          - 硬件支援的圖像單元高速縮放、變形及反轉效果支援
          - 高速的硬件壓縮圖像單元解壓支援
          - 硬件支援截剪及多向滑動圖像單元顯示能力
          - 可變的圖像更新率支援，最高可達每秒75幅
          - 顯示[解像度達](../Page/解像度.md "wikilink")160 x 102，合共16,320像素
      - 數學運算處理器
          - 硬件支援 16位元 x 16位元
            合共32位元之[乘數運算並選擇性支援加數](../Page/乘.md "wikilink")，反向支援
            32位元／16位元 → 16位元之除數
          - 支援處理器運算以外，同時運算一項[乘或](../Page/乘.md "wikilink")[除數指令的運算](../Page/除.md "wikilink")
  - [記憶體](../Page/記憶體.md "wikilink")：64[KB](../Page/kilobyte.md "wikilink")
    120ns [DRAM](../Page/DRAM.md "wikilink")
  - 儲存裝置：卡帶—曾推出容量為128、256及512KB的卡帶，經bank-switching邏輯支援後容量可達2
    [MB](../Page/MB.md "wikilink")，部份非原廠卡帶設有EEPROM記錄最高分數之用
  - 連接介面：
      - 3.5mm[耳機接頭](../Page/耳機.md "wikilink")，支援多聲道耳機，Lunx只作單聲道輸出
      - ComLynx多機連線串聯埠
  - 顯示屏：3.5[英吋](../Page/英吋.md "wikilink")（[對角](../Page/對角線.md "wikilink")）[液晶顯示屏](../Page/液晶顯示屏.md "wikilink")
  - [電池](../Page/電池.md "wikilink")：使用6粒AA[乾電](../Page/乾電.md "wikilink")，提供約4至5小時使用時間

## 参考文献

[雅达利Lynx](../Category/雅达利Lynx.md "wikilink")
[Lynx](../Category/雅達利遊戲機.md "wikilink")
[Category:第四世代遊戲機](../Category/第四世代遊戲機.md "wikilink")
[Category:掌上遊戲機](../Category/掌上遊戲機.md "wikilink")