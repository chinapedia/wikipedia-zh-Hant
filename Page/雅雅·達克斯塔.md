**盈盈·達克斯塔**\[1\]（***Yaya DaCosta***；本名卡玛拉·達克斯塔·约翰逊 Camara DaCosta
Johnson
,)，[美國](../Page/美國.md "wikilink")[模特兒及演員](../Page/模特兒.md "wikilink")，她是《[全美超級模特兒新秀大賽](../Page/全美超級模特兒新秀大賽.md "wikilink")》[第三季的亞軍](../Page/全美超級模特兒新秀大賽第三季.md "wikilink")。

## 生平

盈盈是西非人。她從小習舞，練過芭蕾舞、爵士舞和現代舞等。她在[美國](../Page/美國.md "wikilink")[布朗大学就讀](../Page/布朗大学.md "wikilink")，她在大學就讀[國際關係和非洲文化](../Page/國際關係.md "wikilink")，於2004年5月畢業。盈盈除了能說[英語](../Page/英語.md "wikilink")，她也能說[葡萄牙語](../Page/葡萄牙語.md "wikilink")、[西班牙語以及](../Page/西班牙語.md "wikilink")[法語](../Page/法語.md "wikilink")。

## 全美超級模特兒新秀大賽

在[第三季當中](../Page/全美超級模特兒新秀大賽第三季.md "wikilink")，她連續五集勝出了挑戰，無論是跑樓梯、演戲以至演繹[日本文化等](../Page/日本.md "wikilink")，她也能一一成功挑戰，更破了比賽紀錄。盈盈的皮膚在比賽期間很差，所以泰雅在[第三季第三集帶她去美容](../Page/全美超級模特兒新秀大賽第三季.md "wikilink")。而她唯一一次倒數是在第九集，她向評判推銷日本酸梅時她把酸梅吐出，所以評判認為她不尊重商品，故和妮歌兩人同為倒數，最後妮歌被淘汰。

盈盈拍下不少精彩的相片，例如封面女郎照、和服照等。雖然她確實有超模實力，可是她在比賽時間自大高傲的性格令到評判不甚歡愛，所以她最後也敗給對手[伊娃·佩霍](../Page/伊娃·佩霍.md "wikilink")。

## 事業

雅雅在比賽後和福特模特兒公司\[2\]\[3\] 和Models
1模特兒公司簽約，她為[Sephora](../Page/:en:Sephora.md "wikilink")、Interview
Magazine（2006年4月號）、[Essence
Magazine](../Page/:en:Essence_Magazine.md "wikilink")、CharmaineLouise、Jewel
Magazine、DJA Clothing Company、[Issac
Mizrahi和](../Page/:en:Issac_Mizrahi.md "wikilink")[Target任模特兒](../Page/:en:Target.md "wikilink")。

雅雅也拍輯了[Radioshack](../Page/:en:Radioshack.md "wikilink")、[Garnier
Fructis和Lincoln](../Page/:en:Garnier_Fructis.md "wikilink") Townhouse
Commercial等的廣告。

她參與過的時裝秀有Marc Bouwer Fall 2005。

2006年，雅雅進軍演藝界。她在Hip Hop歌曲Pulling Me
Back的[音樂錄影帶中演出](../Page/音樂錄影帶.md "wikilink")，雅雅也拍攝了電影《[獨領風潮](../Page/獨領風潮.md "wikilink")》，雅雅她是第二女主角，扮演一名黑人女學生，該電影由[安東尼奥·班德拉斯演男主角](../Page/安東尼奥·班德拉斯.md "wikilink")，因為她自小有跳舞，她演這套戲演繹得異常出色。\[4\]
雅雅也在電視節目“Eve”中客串演出。\[5\]

## 演出作品

  - 《[獨領風潮](../Page/獨領風潮.md "wikilink")》(2006)
  - 《上海酒店》（*Shanghai Hotel*），飾Hill Harper (2007)
  - 《甜蜜樂音》(*honeydripper*)(2007)
  - 《Racing for Time》(2008)
  - 《天堂信差》(2009)
  - 《[性福拉警報](../Page/性福拉警報.md "wikilink")》（*The Kids Are All
    Right*）(2010)
  - 《[芝加哥醫情](../Page/芝加哥醫情.md "wikilink")》(2010-)
  - 《[創：光速戰記](../Page/創：光速戰記.md "wikilink")》(2010)
  - 《[鐘點戰](../Page/鐘點戰.md "wikilink")》(2011)
  - 《神鬼行動(搶魚市)》(2012)
  - 《[白宮第一管家](../Page/白宮第一管家.md "wikilink")》(2013)
  - 《如此這般》('' And So It Goes'')(2014)
  - 《惠妮》(2015)

## 注釋

## 外部連結

  - [雅雅的ANTM個人資料](http://www.upn.com/shows/top_model3/models/bio/yaya.shtml)

  - [雅雅的MySpace](http://www.myspace.com/camaradacosta)

  -
[Category:全美超級模特兒新秀大賽](../Category/全美超級模特兒新秀大賽.md "wikilink")
[Category:美國女性模特兒](../Category/美國女性模特兒.md "wikilink")
[Category:布朗大學校友](../Category/布朗大學校友.md "wikilink")
[Category:奈及利亞裔美國人](../Category/奈及利亞裔美國人.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:巴西裔美國人](../Category/巴西裔美國人.md "wikilink")

1.  或[無線電視](../Page/無線電視.md "wikilink")[明珠台譯為](../Page/明珠台.md "wikilink")**盈盈**
2.  [FORD models website](http://www.fordmodels.com/main.cfm)
3.  [Models1 Homepage](http://www.models1.co.uk/default.asp)
4.  ["Take the lead" on IMDb](http://www.imdb.com/title/tt0446046/)
5.  [1](http://www.imdb.com/title/tt0573866/)