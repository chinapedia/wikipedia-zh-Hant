**集集街**，為1920年\~1945年間存在之行政區，轄屬[臺中州](../Page/臺中州.md "wikilink")[新高郡](../Page/新高郡.md "wikilink")，1920年\~1940年間為集集-{庄}-，1940年升格為集集街。今[南投縣](../Page/南投縣.md "wikilink")[集集鎮及](../Page/集集鎮.md "wikilink")[水里鄉](../Page/水里鄉.md "wikilink")。

## 街-{庄}-原名

原屬

  - 集集堡之集集街、林尾-{庄}-、柴橋頭-{庄}-、社仔-{庄}-
  - 沙連下堡之隘藔-{庄}-
  - 五城堡之拔社埔-{庄}-\[1\]

## 行政區劃

集集街轄域原有**集集**、**林尾**、**柴橋頭**、**隘寮**、**社子**、**拔社埔**六個[大字](../Page/大字_\(行政區劃\).md "wikilink")，1940年10月1日後加入新高郡蕃地內一部分，設「郡坑」\[2\]。

1940年後集集街轄域內分為**集集**、**林尾**、**柴橋頭**、**隘寮**、**社子**、**[拔社埔](../Page/民和村.md "wikilink")**、**郡坑**七個[大字](../Page/大字_\(行政區劃\).md "wikilink")\[3\]。

  - **集集**大字下有「集集」、「鷄籠山」小字名
  - **柴橋頭**大字下有「吳厝」、「洞角」、「北勢坑」小字名\[4\]

戰後初設集集鎮，1950年**社子**、**拔社埔**、**郡坑**獨立組成水里鄉。

## 行政區與大字對照

集集鎮現有十一里，水里鄉現有十九村，各村與日治時期大字對照如下：

  - 集集街大字與現今村里名對照表：

<table>
<tbody>
<tr class="odd">
<td><dl>
<dt>集集鎮</dt>

</dl>
<table>
<thead>
<tr class="header">
<th><p>大字名</p></th>
<th><p>現今里名</p></th>
<th><p>大字名</p></th>
<th><p>現今里名</p></th>
<th><p>大字名</p></th>
<th><p>現今里名</p></th>
<th><p>大字名</p></th>
<th><p>現今里名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>集集</strong></p></td>
<td><p>集集、和平</p></td>
<td><p><strong>林尾</strong></p></td>
<td><p>林尾</p></td>
<td><p><strong>隘寮</strong></p></td>
<td><p>隘寮、田寮</p></td>
<td><p><strong>柴橋頭</strong></p></td>
<td><p>玉映、吳厝、八張、永昌、富山、廣明</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
<dl>
<dt>水里鄉</dt>

</dl>
<table>
<thead>
<tr class="header">
<th><p>大字名</p></th>
<th><p>現今村名</p></th>
<th><p>大字名</p></th>
<th><p>現今村名</p></th>
<th><p>大字名</p></th>
<th><p>現今村名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>社子</strong></p></td>
<td><p>中央、北埔、永豐、車埕、南光、城中、頂崁、新城、新興、農富、鉅工、水里</p></td>
<td><p><strong>拔社埔</strong></p></td>
<td><p>民和</p></td>
<td><p><strong>郡坑</strong></p></td>
<td><p>新山、郡坑、上安</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

  - 水里鄉之玉峰、永興、興隆三村原屬鹿谷庄

## 參考來源

[Category:臺中州](../Category/臺中州.md "wikilink")
[集集鎮](../Category/集集鎮.md "wikilink")

1.  《新舊對照管轄便覽》
2.  府令第123-124號
3.  [集集街淺談](http://nantoulife.pixnet.net/blog/post/23432351-%E9%9B%86%E9%9B%86%E8%A1%97%E6%B7%BA%E8%AB%87)
4.  《臺灣總督府行政區域便覽》