《**集韻**》和《[禮部韻略](../Page/禮部韻略.md "wikilink")》都是[宋仁宗景祐四年](../Page/宋仁宗.md "wikilink")（公元1037年）由[丁度等人奉命編寫的官方](../Page/丁度.md "wikilink")[韻书](../Page/韻书.md "wikilink")。据[李燾](../Page/李燾.md "wikilink")《[說文解字五音譜敘](../Page/說文解字五音譜敘.md "wikilink")》记载，宋仁宗景祐四年，即《廣韻》頒行後29年，[宋祁](../Page/宋祁.md "wikilink")、[鄭戩給仁宗上書批評](../Page/鄭戩.md "wikilink")《[廣韻](../Page/廣韻.md "wikilink")》多用舊文，“繁省失當，有誤科試”。另据[王應麟](../Page/王應麟.md "wikilink")《[玉海](../Page/玉海.md "wikilink")》记载，[賈昌朝也同时上書批評](../Page/賈昌朝.md "wikilink")[宋真宗景德年間編的](../Page/宋真宗.md "wikilink")《[韻略](../Page/韻略.md "wikilink")》是“多無訓釋，疑混聲、重疊字，舉人誤用”。於是仁宗皇帝下令由丁度等人重修這兩部韻書。结果於景祐四年當年就完成了《[禮部韻略](../Page/禮部韻略.md "wikilink")》，两年後於仁宗寶元二年（公元1039年）完成了《**集韻**》。

《**集韻**》為《廣韻》之改正，仍分206韻。只是韻目用字，部分韻目的次序和韻目下面所注的韻字同用、獨用的規定稍有不同。從[王仁昫的](../Page/王仁昫.md "wikilink")《[刊謬補缺切韻](../Page/刊謬補缺切韻.md "wikilink")》開始，及至[孫愐的](../Page/孫愐.md "wikilink")《[唐韻](../Page/唐韻.md "wikilink")》，韻字都加入注釋，並且引文都有出處，於是韻書便同时具有[辭書和](../Page/辭書.md "wikilink")[字典的功能](../Page/字典.md "wikilink")。《集韻》和《廣韻》主要的不同之處還在於《集韻》收字多，并且收的[異體字很多](../Page/異體字.md "wikilink")。一個字不管是正體、古體、或體、俗體，全部收進，一个字可多到八九個寫法。《集韻》共收53525字，比《廣韻》多收27331字。缺點是對字的來源不加說明，字訓以《[說文解字](../Page/說文解字.md "wikilink")》為根據，[反切多採自](../Page/反切.md "wikilink")《[經典釋文](../Page/經典釋文.md "wikilink")》。

## 參見

  - [韻書](../Page/韻書.md "wikilink")
  - [韵图](../Page/韵图.md "wikilink")

## 參考文獻

  -
  -

  - Teng, Ssu-yü and Biggerstaff, Knight. 1971. *An Annotated
    Bibliography of Selected Chinese Reference Works*, 3rd ed.
    Cambridge, Mass: Harvard University Press.

[category:語言學書籍](../Page/category:語言學書籍.md "wikilink")

[Category:韻書](../Category/韻書.md "wikilink")
[Category:經部小學類](../Category/經部小學類.md "wikilink")
[Category:宋朝典籍](../Category/宋朝典籍.md "wikilink")
[Category:11世紀書籍](../Category/11世紀書籍.md "wikilink")