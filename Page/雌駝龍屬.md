</ref> }}

**雌駝龍屬**（[屬名](../Page/屬.md "wikilink")：，意為「來自 Ingen Khoboor
的旅人」）又名**母駝龍**，是[偷蛋龍科](../Page/偷蛋龍科.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，身長約
1.4 至 2 公尺長，體重約 25
公斤重，生存於[上白堊紀](../Page/上白堊紀.md "wikilink")（約七千萬年前）的[蒙古南部](../Page/蒙古.md "wikilink")。

雌駝龍目前僅發現少數標本，發現於[耐梅蓋特組的](../Page/耐梅蓋特組.md "wikilink") Bugin Tsav
地層，當地還發現了[單爪龍的](../Page/單爪龍.md "wikilink")[正模標本](../Page/正模標本.md "wikilink")。其標本包含手臂、腿部、骨盆、肩帶、部分頭骨、少數脊椎，但還沒完全敘述。雌駝龍屬於[偷蛋龍科](../Page/偷蛋龍科.md "wikilink")，其[恥骨有向前的柄部](../Page/恥骨.md "wikilink")，下頜的邊緣為大幅S狀彎曲，口鼻部短，頭頂圓形，顱頂骨頭癒合。有些化石的[胸骨中線癒合](../Page/胸骨.md "wikilink")，形成短隆線。

## 詞源

[Ingenia_yanshini.JPG](https://zh.wikipedia.org/wiki/File:Ingenia_yanshini.JPG "fig:Ingenia_yanshini.JPG")
[Ingenia.jpg](https://zh.wikipedia.org/wiki/File:Ingenia.jpg "fig:Ingenia.jpg")
早期雌駝龍的學名為 *Ingenia*，意為「來自 Ingen Khoboor
的」，是按發現地而取的；種名則是以[蘇聯](../Page/蘇聯.md "wikilink")[聖彼得堡](../Page/聖彼得堡.md "wikilink")[俄羅斯科學院的](../Page/俄羅斯科學院.md "wikilink")
Aleksandr Leonidovich Yanshin
為名，他曾在這次挖掘活動中對[瑞欽·巴思缽](../Page/瑞欽·巴思缽.md "wikilink")（Rinchen
Barsbold）提出建議\[1\]，然而有些中文書籍誤認學名意為「機靈的」，而翻成**機靈龍**。但是，最近才發現這個學名已被[線蟲動物門的一屬所有](../Page/線蟲動物門.md "wikilink")，由於本屬較晚使用，使得這個學名無效，目前則以
*Ajancingenia* 代之，學名是由[蒙古文](../Page/蒙古文.md "wikilink") аянч
（意為「旅行者」，是指特大的指骨，像是豎起大拇指請求搭便車的手勢）與舊學名組成\[2\]。

## 参考文献

## 外部連結

  - [恐龍博物館──雌駝龍](https://web.archive.org/web/20070824134109/http://www.dinosaur.net.cn/museum/Ingenia.htm)

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:偷蛋龍科](../Category/偷蛋龍科.md "wikilink")

1.  Barsbold R. 1981. *Bezzubye khishchnye dinozavry Mongolii.*
    \[Toothless carnivorous dinosaurs of Mongolia.\]. *Trudy --
    Sovmestnaya Sovetsko-Mongol'skaya Paleontologicheskaya Ekspeditsiya*
    **15**: 28-39, 124. \[in Russian, w/ English summary\].
2.  Easter, J. 2013. "A new name for the oviraptorid dinosaur
    "*Ingenia*" *yanshini* (Barsbold, 1981; preoccupied by Gerlach,
    1957)". Zootaxa **3737** (2): 184–190. .