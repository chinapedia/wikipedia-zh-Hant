[YumbuLhakhang2.jpg](https://zh.wikipedia.org/wiki/File:YumbuLhakhang2.jpg "fig:YumbuLhakhang2.jpg")
**雍布拉康**（），传说是[西藏第一位王](../Page/西藏.md "wikilink")，雅隆部落的首领[聂赤赞普于公元前](../Page/聂赤赞普.md "wikilink")127年建造的第一座宫殿，位于[山南地区](../Page/山南地区.md "wikilink")[乃东县东南约](../Page/乃东县.md "wikilink")5公里、[昌珠镇境内的](../Page/昌珠镇.md "wikilink")[扎西次日山上](../Page/扎西次日山.md "wikilink")，
“雍布”在[藏语中的意思为母鹿](../Page/藏语.md "wikilink")，“拉”意为后腿，“康”意为宫殿，因为扎西次日山形像一只卧着的母鹿，雍布拉康正立于母鹿腿上。

据说由于聂赤赞普教导人们种地，现在雍布拉康附近还保留着西藏的第一块耕地。由于雅隆人学会农耕，部落逐渐强大起来，终于在第33代赞普[松赞干布时代统一了全吐蕃各部落](../Page/松赞干布.md "wikilink")，建立了吐蕃王国，并迁都到北面的拉萨平原，在[拉萨首先建立了](../Page/拉萨.md "wikilink")[大昭寺](../Page/大昭寺.md "wikilink")，将雍布拉康作为松赞干布和文成公主的夏宫，目前只在大昭寺和雍布拉康有松赞干布和文成公主共同的塑像。

雍布拉康内的壁画描绘了西藏的第一位赞普，第一座建筑和第一块耕地的故事。

第五世[达赖喇嘛将雍布拉康改为一座寺庙至今](../Page/达赖喇嘛.md "wikilink")，内供奉[释迦牟尼佛像](../Page/释迦牟尼.md "wikilink")。[文化大革命期间](../Page/文化大革命.md "wikilink")，雍布拉康被拆毁，全部文物均流失，仅留下断壁残垣，现存建筑是1982年后修复的。

雍布拉康还保存着[西藏第一部佛经](../Page/西藏.md "wikilink")《诸佛菩萨名称经》，据说在[文成公主将](../Page/文成公主.md "wikilink")[佛教带到西藏以前](../Page/佛教.md "wikilink")，在第28代赞普时代，从天上降下一本书到雍布拉康的屋顶上，当时信奉[苯教的藏人不知为何物](../Page/苯教.md "wikilink")，但既然是天降的，仍然保存下来。直到在[藏族皈依](../Page/藏族.md "wikilink")[佛教后](../Page/佛教.md "wikilink")，才知道这是一本佛教经典，现在仍然保存在雍布拉康。

雍布拉康东北400余米的山沟里，有一眼终年不涸的泉水，传说是被松赞干布时重臣[噶尔东赞发现的](../Page/禄东赞.md "wikilink")，被称为“嘎泉”。到雍布拉康礼拜的人都到嘎泉饮用泉水，据说可医百病。
[YumbuLhakhang.jpg](https://zh.wikipedia.org/wiki/File:YumbuLhakhang.jpg "fig:YumbuLhakhang.jpg")

## 外部链接

  - [雍布拉康介绍](https://web.archive.org/web/20070509065248/http://www.chinatibetnews.com/GB/channel8/55/200208/13/348.html)
  - [西藏相簿：1920-1950年雍布拉康照片。](http://web.prm.ox.ac.uk/tibet/thumbnails_region_Yumbu+lakhar.html)

[Category:西藏旅游](../Category/西藏旅游.md "wikilink")
[Category:藏传佛教寺院](../Category/藏传佛教寺院.md "wikilink")