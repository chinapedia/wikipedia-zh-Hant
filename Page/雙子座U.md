**雙子座U**，位於[雙子座](../Page/雙子座.md "wikilink")，是[矮新星的典範之一](../Page/矮新星.md "wikilink")。這類[聯星系統包含一顆](../Page/聯星.md "wikilink")[白矮星和一顆靠近的](../Page/白矮星.md "wikilink")[紅矮星](../Page/紅矮星.md "wikilink")，大約每100天就會爆發一次並造成光度的增加。在1855年的一次爆發中被[约翰·罗素·欣德發現了](../Page/约翰·罗素·欣德.md "wikilink")，從此就被持續觀測至今。

雙子座U聯星系統的軌道週期非常短，只有4小時又11分；僅僅是這樣的軌道就會在每次公轉時造成凌與食的變光現象。通常，這對聯星的[視星等在](../Page/視星等.md "wikilink")14.0和15.1等之間；然而當爆發時，亮度會增加百倍左右，達到9等。雖然平均的間格大約是100天，這個週期實際上並不穩定，紀錄上是從62天至257天不等。在[矮新星的案例中](../Page/矮新星.md "wikilink")，理論上爆發是由白矮星[吸積盤週期性的浪湧](../Page/吸積盤.md "wikilink")，造成盤本身不穩定的結果。

## 相關條目

  - [激變變星](../Page/激變變星.md "wikilink")
  - [天鵝座SS](../Page/天鵝座SS.md "wikilink")

## 參考資料

  - [AAVSO: "U Gem: February 1999 Variable Star of the Month."
    (Accessed 5/19/06)](http://www.aavso.org/vstar/vsots/0299.shtml)
  - Burnham, Robert. *Burnham's Celestial Handbook.* New York: Dover
    Publications, Inc., 1978. ISBN 0-486-23568-8 pp. 925-34.

## 外部連結

  - [animation](http://www.regulusastro.com/regulus/photos/text/u_gem.html)
  - [IR spectral data for redshift
    calculations](http://www.astro.ex.ac.uk/people/timn/UGem/)

[Category:紅矮星](../Category/紅矮星.md "wikilink")
[Category:白矮星](../Category/白矮星.md "wikilink")
[Category:聯星](../Category/聯星.md "wikilink")
[Category:矮新星](../Category/矮新星.md "wikilink")
[Category:雙子座](../Category/雙子座.md "wikilink")
[Category:激變變星](../Category/激變變星.md "wikilink")