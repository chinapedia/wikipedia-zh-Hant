**雙星**是[觀測天文學的名詞](../Page/觀測天文學.md "wikilink")，當兩顆[恆星由](../Page/恆星.md "wikilink")[地球上觀察時](../Page/地球.md "wikilink")，在視線的方向上非常接近，以致以肉眼看起來像是只有一顆恆星，但使用[望遠鏡時就能分辨出來是一對的恆星](../Page/望遠鏡.md "wikilink")。這種情形可以發生在一對[聯星](../Page/聯星.md "wikilink")，也就是有著互動的[軌道](../Page/軌道.md "wikilink")，並且被彼此的[引力束縛在一起](../Page/引力.md "wikilink")；也可以是[光學雙星](../Page/光學雙星.md "wikilink")，這是兩顆有著不同的距離，但恰巧在天空中相同的方向上被對準在一起\[1\]\[2\]。聯星對恆星天文學家是很重要的，當知道它們的運動，就可以直接計算它們的質量和其它地恆星參數。
從1780年代開始，研究雙星的專業和業餘天文學家就透過望遠鏡的觀測量雙星之間的距離和角度，以量度每一對雙星之間的相對運動\[3\]。如果測量出的相對運動是一段[軌道弧線](../Page/軌道.md "wikilink")，或者相對運動相較於這兩顆恆星本身的一般[自行是很小的值](../Page/自行.md "wikilink")，就可以得到這兩顆恆星沒有相互的軌道運動。換言之，這一對就只是光學雙星\[4\]。雖然多顆的[恆星系統的運動比聯星更為複雜](../Page/恆星系統.md "wikilink")，但對[聚星的研究也是用這種方法](../Page/恆星系統#聚星系統.md "wikilink")。

成對的恆星有下列三種：

  - 光學雙星：是無關聯的恆星 － 只是從地球看過去他們恰好對齊在一起；
  - [目視聯星](../Page/聯星#目視聯星.md "wikilink") － 被引力束縛在一起，使用光學望遠鏡就可以分辨的恆星；
  - 非目視聯星 －
    要使用更專業的工具，像是[掩星](../Page/掩星.md "wikilink")（[食雙星](../Page/聯星#食聯星.md "wikilink")）、[光譜](../Page/天體光譜學.md "wikilink")（[光譜聯星](../Page/聯星#光譜聯星.md "wikilink")）、或異常的[自行](../Page/自行.md "wikilink")（[天測聯星](../Page/聯星#天測聯星.md "wikilink")）才能分辨的聯星。

就觀念而言，後面這兩種之間其實沒有差別。望遠鏡的改進可以將非目視聯星重分類至目視聯星中，像[北極星在](../Page/北極星.md "wikilink")2006年就發生這種情形。因此，第三種只是我們在觀測方法上的不同造成的區別。

## 歷史

[大熊座的](../Page/大熊座.md "wikilink")[開陽在](../Page/開陽_\(恆星\).md "wikilink")1650年首度被[喬萬尼·巴特斯達·里奇奧利登錄為雙星](../Page/喬萬尼·巴特斯達·里奇奧利.md "wikilink")\[5\]\[6\]（但[貝內代托·卡士德里和](../Page/貝內代托·卡士德里.md "wikilink")[伽利略可能在更早](../Page/伽利略.md "wikilink")）\[7\]。很快就確認了其它的雙星系統：[羅伯特·虎克在](../Page/羅伯特·虎克.md "wikilink")1664年發現的第一個雙星系統，[白羊座γ](../Page/白羊座γ.md "wikilink")\[8\]；而[南十字座明亮的](../Page/南十字座.md "wikilink")[十字架二在](../Page/十字架二.md "wikilink")1685年被馮提尼發現是雙星\[9\]。從此之後，對雙星的搜尋在天空中被很徹底的執行，不只是明亮的雙星，還下探至[視星等](../Page/視星等.md "wikilink")9.0等的極限\[10\]。在北半球的天空中，比9等亮的恆星中，每18顆就至少有一顆是雙星，可以使用口徑
的望遠鏡分辨出來\[11\]。

類別不相關的光學雙星和聯星被混編在同一本星表中是有其歷史上的事實和原因。當開陽被發現是聯星時，仍很難確認一對雙星是聯星，還僅僅是光學雙星。
望遠鏡、光譜\[12\]、和攝影的改進，都是用來區分這兩者的基本工具。當它被確認是視聯星後，開陽本身的結構被發現是光譜聯星\[13\]。此外，開陽和[輔又組合成光學雙星](../Page/馬和騎士.md "wikilink")，兩者相距3[光年](../Page/光年.md "wikilink")。它們被懷疑，但又沒有絕對的證據，能證明兩者之間沒有引力的相互作用。因此，雙星這個名詞對尚不了解的一對恆星而言，仍然是一個有用的項目。

## 雙星的觀測

[Masquerading_as_a_double_star.jpg](https://zh.wikipedia.org/wiki/File:Masquerading_as_a_double_star.jpg "fig:Masquerading_as_a_double_star.jpg")和在雙子座一顆暗星的位置\[14\]。\]\]

*視雙星*的定義是使用光學望遠鏡可以看見與分辨的雙星，這在已知的雙星中佔了絕大多數\[15\]。如果視雙星顯現相似的屬性，像是相似的[自行穿越空間](../Page/自行.md "wikilink")、[三角視差或](../Page/視差.md "wikilink")[徑向速度](../Page/徑向速度.md "wikilink")，這些都是它們之間有引力關聯，形成[聯星的證據](../Page/聯星.md "wikilink")；在這種情況下，視雙星會被稱為*視聯星*。

以目視測量視雙星的分離度，或角距離，是測量這兩顆星在天空中的[位置角和彼此之間以角度度量的距離](../Page/位置角.md "wikilink")。以位置角來指示兩星分隔的距離與方向時，是從亮星朝向暗星來測量，並且正北方被定義為0°\[16\]。這些測量的結果被稱為*量測值（measures）*，視聯星量測值中的位置角會逐漸改變，兩顆星之間分開的距離也會在最高和最低值之間擺動。在平面上繪製量測值將產生一個橢圓。這是投影在天球上的''視軌道
''，從視軌道可以計算出這兩顆星真實的[軌道](../Page/軌道_\(力學\).md "wikilink")\[17\]。雖然預期在視雙星目錄上的絕大多數可能都是視聯星\[18\]，但是在超過100,000顆已知的視雙星中，僅有幾千顆的軌道已經被計算出來\[19\]\[20\]。

## 聯星和其他雙星之間的區別

通過觀察它們的相對運動可以區別視雙星和視聯星。如果運動是[軌道的一部分](../Page/軌道.md "wikilink")，或是恆星有著相似的[徑向速度](../Page/徑向速度.md "wikilink")，或是相較於一般的自行有著較小但不同的[自行](../Page/自行.md "wikilink")，這一部分可能都是自然的性質。當觀測的時間短於週期時，光學雙星和長週期視聯星可能都呈現直線的運動，這就會很難區分這兩種的可能性\[21\]。

## 名稱

一些明亮的雙星有[拜耳名稱](../Page/拜耳命名法.md "wikilink")，在這種情況下，可以用上標字元來表示。例如，[南十字座α](../Page/南十字座α.md "wikilink")（南十字二）和伴星分別是是南十字α<sup>1</sup>和南十字α<sup>2</sup>。由於南十字α<sup>1</sup>還是[光譜聯星](../Page/聯星#光譜聯星.md "wikilink")，因此實際上是[聚星](../Page/聚星.md "wikilink")。上標字元也可以用來區分更遙遠，無物理關係，在拜耳名稱下成雙的恆星，像是[摩羯座α<sup>1,2</sup>](../Page/摩羯座α.md "wikilink")（相距0.11°）、[半人馬座ξ<sup>1,2</sup>](../Page/半人馬座ξ.md "wikilink")（相距0.66°）、[人馬座ξ<sup>1,2</sup>](../Page/人馬座ξ.md "wikilink")（相距0.46°）。這些光學雙星都可以用裸眼直接觀看。

除了這些對雙星，雙星的成員通常用字母*A*（表示較亮的、主星）和*B*（表示較暗的、伴星）來區分，再增加的就依據字母來排序。例如，[大犬座α](../Page/大犬座α.md "wikilink")（天狼星）的成員是大犬座αA和大犬座αB（天狼星A和天狼星B）；[牧夫座44的成員是牧夫座](../Page/牧夫座44.md "wikilink")44A和牧夫座44B；[ADS
16402的成員是ADS](../Page/ADS_16402.md "wikilink") 16402A和ADS
16402B；依此類推。字母*AB*可能一起用來指出這是一對雙星，在[聚星](../Page/聚星.md "wikilink")，字母*C*、*D*，也是用來標示不同的伴星，它們的順序是依照與最亮A星距離的增加來排列\[22\]。

| 發現者                                                      | 傳統碼           | WDS碼         |
| -------------------------------------------------------- | ------------- | ------------ |
| [布里斯班天文台](../Page/布里斯班天文台.md "wikilink")                 | Brs0          | BSO          |
| [S. W. Burnham](../Page/S._W._Burnham.md "wikilink")     | β             | BU           |
| [詹姆士·丹露帕](../Page/詹姆士·丹露帕.md "wikilink")                 | Δ             | DUN          |
| [威廉·赫歇爾](../Page/威廉·赫歇爾.md "wikilink")                   | H I, II, etc. | H 1, 2, etc. |
| [尼可拉·路易·拉卡伊](../Page/尼可拉·路易·拉卡伊.md "wikilink")           | Lac           | LCL          |
| [瓦西里·雅可夫列維奇·斯特魯維](../Page/瓦西里·雅可夫列維奇·斯特魯維.md "wikilink") | Σ             | STF          |
| Struve Appendix Catalogue I                              | Σ I           | STFA         |
| Struve Appendix Catalogue II                             | Σ II          | STFB         |
| [奧托·斯特魯維](../Page/奧托·斯特魯維.md "wikilink")                 | OΣ            | STT          |
| Pulkova Appendix Catalogue                               | OΣΣ           | STTA         |

雙星發現者的名稱\[23\]

視雙星通過在它們發現者名字的縮寫後面加上數字編輯出獨特的目錄。例如，半人馬座αAB是在1689年被Richaud
神父發現的，因此就被稱為*RHD 1*
\[24\]\[25\]。其它的例子包括Δ65，是[詹姆士·丹露帕發現的第](../Page/詹姆士·丹露帕.md "wikilink")65顆雙星，和Σ2451是[F.
G. W. Struve發現的第](../Page/瓦西里·雅可夫列維奇·斯特魯維.md "wikilink")2451顆雙星。

[華盛頓雙星目錄](../Page/華盛頓雙星目錄.md "wikilink")，是包含超過100,000顆雙星和[聚星的龐大資料庫](../Page/聚星.md "wikilink")\[26\]，每一筆都列出了兩顆星之間距離的量測值。在目錄內的每一顆雙星構成一個條目，有*n*個成員的聚星會構成n-1對雙星，每個都會給出與聚星中其他成員的分離距離。像*AC*這樣的編碼是表示被測量的成員C相對於A的量測值；編碼也可能改變成*AB-D*以指示密接的A與B和D的量測值，本例中指的是D相對於AB對；而像*Aa*這樣的編碼也可以表示單一的成員，在這個例子可能指的是另一個成員相對於A的量測值\[27\]。發現者的編碼也還是會被列出，但是傳統的發現者名字縮寫，像是*Δ*和*Σ*等，都已經改編成一個羅馬大寫字母的字串，例如Δ65
已成為*DUN 65*和*Σ2451*已成為*STF2451*。更多的例子呈現在右邊的表中\[28\]\[29\]。

## 例子

### 視聯星

  - [半人馬座α](../Page/半人馬座α.md "wikilink")
  - [天狼星](../Page/天狼星.md "wikilink")
  - [南河三](../Page/南河三.md "wikilink")
  - [五車二](../Page/五車二.md "wikilink")
  - [波江座p](../Page/HD_10360.md "wikilink")
  - [北極星](../Page/北極星.md "wikilink")
  - [十字架二](../Page/十字架二.md "wikilink")

### 光學雙星

  - [摩羯座α<sup>1</sup>和](../Page/牛宿增六.md "wikilink")[摩羯座α<sup>2</sup>](../Page/牛宿二.md "wikilink")
  - [蒼蠅座θ和](../Page/蒼蠅座θ.md "wikilink")[蒼蠅座θB](../Page/蒼蠅座θB.md "wikilink")
  - [南冕座η<sup>1</sup>和](../Page/南冕座η1.md "wikilink")[南冕座η<sup>2</sup>](../Page/南冕座η2.md "wikilink")
  - [南冕座κ<sup>1</sup>和](../Page/鱉十.md "wikilink")[南冕座κ<sup>2</sup>](../Page/鱉十.md "wikilink")
  - [Winnecke 4](../Page/M40_\(雙星\).md "wikilink")

### 不確定

  - [半人馬座α系統](../Page/南門二.md "wikilink")（AB）和[比鄰星](../Page/比鄰星.md "wikilink")（半人馬座αＣ）：一般認為是有物理關聯性的系統。
  - [北河二系統](../Page/北河二.md "wikilink")（Aa/Ab/Ba/Bb）和雙子座YY（也就是北河二Ca/Cb）一般認為是一個物理系統。
  - [開陽系統](../Page/開陽_\(恆星\).md "wikilink")（Aa/Ab/Ba/Bb）和輔（本身是聯星，因此是開陽Ca/Cb，在2009年之前，一般都不認為有物理上的關聯）。

## 相關條目

  - [聯星](../Page/聯星.md "wikilink")
  - [科幻中的聯星](../Page/科幻中的聯星.md "wikilink")
  - [聚星](../Page/聚星.md "wikilink")

## 參考資料

[Category:恆星類型](../Category/恆星類型.md "wikilink")
[Category:恒星](../Category/恒星.md "wikilink")
[雙星](../Category/雙星.md "wikilink")
[聚星](../Category/聚星.md "wikilink")

1.  *The Binary Stars*, [Robert Grant
    Aitken](../Page/Robert_Grant_Aitken.md "wikilink"), New York: Dover,
    1964, p. 1.

2.

3.

4.
5.
6.  [Vol. 1, part 1, p. 422, *Almagestum
    Novum*](http://leo.astronomy.cz/mizar/riccioli.htm), Giovanni
    Battista Riccioli, Bononiae: Ex typographia haeredis Victorij
    Benatij, 1651.

7.  [A New View of Mizar](http://leo.astronomy.cz/mizar/article.htm),
    Leos Ondra, accessed on line May 26, 2007.

8.

9.
10. See *The Binary Stars*, [Robert Grant
    Aitken](../Page/Robert_Grant_Aitken.md "wikilink"), New York: Dover,
    1964, pp. 24–25, 38, and p. 61, The present status of double star
    astronomy, K. Aa. Strand, *Astronomical Journal* **59** (March
    1954), pp. 61–66, .

11. *The Binary Stars*, [Robert Grant
    Aitken](../Page/Robert_Grant_Aitken.md "wikilink"), New York: Dover,
    1964, p. 260.

12. [Fraunhofer](../Page/Joseph_von_Fraunhofer.md "wikilink"), 1814

13. [Pickering](../Page/Edward_Charles_Pickering.md "wikilink"), 1889

14.

15.

16. p. 2, *Observing and Measuring Double Stars*, Bob Argyle, ed.,
    London: Springer-Verlag, 2004, ISBN 978-1-85233-558-8.

17. p. 53–67, *Observing and Measuring Double Stars*, Bob Argyle, ed.,
    London: Springer-Verlag, 2004, ISBN 978-1-85233-558-8.

18.
19. "Introduction and Growth of the WDS", [The Washington Double Star
    Catalog](http://ad.usno.navy.mil/wds/wdstext.html#intro), Brian D.
    Mason, Gary L. Wycoff, and William I. Hartkopf, Astrometry
    Department, [United States Naval
    Observatory](../Page/United_States_Naval_Observatory.md "wikilink"),
    accessed on line August 20, 2008.

20. [Sixth Catalog of Orbits of Visual Binary
    Stars](http://ad.usno.navy.mil/wds/orb6.html), William I. Hartkopf
    and Brian D. Mason, [United States Naval
    Observatory](../Page/United_States_Naval_Observatory.md "wikilink"),
    accessed on line August 20, 2008.

21.

22.

23.
24.
25. Entry 14396-6050, discoverer code RHD 1AB,[The Washington Double
    Star
    Catalog](http://ad.usno.navy.mil/wds/Webtextfiles/wdsnewframe3.html),
    [United States Naval
    Observatory](../Page/United_States_Naval_Observatory.md "wikilink").
    Accessed on line August 20, 2008.

26.
27. [Format of the current
    WDS](http://ad.usno.navy.mil/wds/wdsweb_format.txt) , Washington
    Double Star Catalog, [United States Naval
    Observatory](../Page/United_States_Naval_Observatory.md "wikilink").
    Accessed on line August 26, 2008.

28. p. 307–308, *Observing and Measuring Double Stars*, Bob Argyle, ed.,
    London: Springer-Verlag, 2004, ISBN 978-1-85233-558-8.

29. [References and discoverer codes, The Washington Double Star
    Catalog](http://ad.usno.navy.mil/wds/wdsnewref.txt), [United States
    Naval
    Observatory](../Page/United_States_Naval_Observatory.md "wikilink").
    Accessed on line August 20, 2008.