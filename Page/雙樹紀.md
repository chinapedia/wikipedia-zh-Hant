**雙樹紀**（*Years of the
Trees*），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·瑞爾·托爾金的史詩式奇幻小說系列中的](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")[阿爾達三大時代之一](../Page/阿爾達.md "wikilink")。位於[巨燈紀之後](../Page/巨燈紀.md "wikilink")，[太陽紀之前](../Page/太陽紀.md "wikilink")，是開始為人所知的年代。

[兩盞巨燈倒下後](../Page/兩盞巨燈.md "wikilink")，[維拉植物女神](../Page/維拉.md "wikilink")[雅凡娜造了](../Page/雅凡娜.md "wikilink")[雙聖樹代替兩盞巨燈成為阿爾達的光源](../Page/雙聖樹.md "wikilink")。雙聖樹位於[阿門洲維拉統治的](../Page/阿門洲.md "wikilink")[維林諾上](../Page/維林諾.md "wikilink")，被命名為[泰爾佩瑞安](../Page/泰爾佩瑞安.md "wikilink")（銀色）和[羅瑞林](../Page/羅瑞林.md "wikilink")（金色）。但[中土大陸依然處於黑暗](../Page/中土大陸.md "wikilink")，唯有星光照耀。

在[西爾卡内海的](../Page/西爾卡内海.md "wikilink")[庫維因恩海灣沉睡之](../Page/庫維因恩.md "wikilink")[精靈甦醒](../Page/精靈甦醒.md "wikilink")，維拉狩獵之神[歐羅米目擊](../Page/歐羅米.md "wikilink")[精靈出現](../Page/精靈.md "wikilink")，但有很多精靈被[米爾寇擄去](../Page/米爾寇.md "wikilink")。

大多數，雖則不是所有。精靈被歐羅米說服去參加偉大旅程，向西前往阿門洲。沿途有些精靈們脫離了隊伍，留在中土大陸，形成[辛達族](../Page/辛達族.md "wikilink")
*Sindar*和[南多精靈](../Page/南多精靈.md "wikilink")
*Nandor*。而到達阿門洲的三個種族是[凡雅族](../Page/凡雅族.md "wikilink")、[諾多族和](../Page/諾多族.md "wikilink")[帖勒瑞族](../Page/帖勒瑞族.md "wikilink")。

維拉活捉了米爾寇，將他囚禁在阿門洲。接著米爾寇服完了幾千年的刑期，再度被帶到維拉寶座前，米爾寇卑躬屈膝懇求原諒，維拉悲哀女神[妮娜也開口說情](../Page/妮娜.md "wikilink")，維拉之首[曼威接受他的道歉](../Page/曼威.md "wikilink")，於是米爾寇被釋放。米爾寇之後在言語和行為上都表現良好，不論是維拉和[艾爾達都從他的意見與幫助中大得益處](../Page/艾爾達.md "wikilink")，只要他們開口，他從不拒絕，一段時日之後，他便獲得了往來全地的自由。

他播下了紛爭的種子在精靈之中，挑動諾多至高王[芬威兩個兒子](../Page/芬威.md "wikilink")[費諾和](../Page/費諾.md "wikilink")[芬國昐从而引起纷争](../Page/芬國昐.md "wikilink")。在大蜘蛛[昂哥立安幫助下](../Page/昂哥立安.md "wikilink")，他殺死芬威盜取[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")，三顆包含雙聖樹之光的宝石，但雙聖樹被米爾寇和昂哥立安破壞。

一名信使自[佛密諾斯趕來](../Page/佛密諾斯.md "wikilink")，說米爾寇殺死了芬威，竊取了精靈寶鑽。費諾聞訊大聲咒詛米爾寇，將他易名[魔苟斯](../Page/魔苟斯.md "wikilink")，他鼓動族人返回中土大陸，向魔苟斯发起挑战。但是諾多族不懂造船，於是費諾帶軍前往[伊瑞西亞島](../Page/伊瑞西亞島.md "wikilink")[澳闊隆迪港](../Page/澳闊隆迪.md "wikilink")，向關係友好的[泰勒瑞族借船](../Page/泰勒瑞族.md "wikilink")，費諾希望泰勒瑞族也加入他們的行列，但泰勒瑞族拒絕，反而勸阻費諾，費諾聞言轉身離去，下令軍隊強行奪船，泰勒瑞族當然反抗不從，紛紛將諾多族精靈推下海去。於是，發生了血戰，因泰勒瑞族的武器不夠好，于是费诺大胜，夺得泰勒瑞族的船只。第一次[親族残杀事件發生了](../Page/親族残杀.md "wikilink")，这是阿尔达既[芬威被谋杀后的第二起血案](../Page/芬威.md "wikilink")，于是维拉[曼督斯诅咒诺多族并向他们关闭了返回](../Page/曼督斯.md "wikilink")[阿门洲的路](../Page/阿门洲.md "wikilink")。

費諾的船隊到達[多爾露明的](../Page/多爾露明.md "wikilink")[專吉斯特狹灣登陸](../Page/專吉斯特狹灣.md "wikilink")。他下令燒掉白船，遺棄他的兄弟芬國昐大隊於對岸，这是曼督斯的诅咒显现的开端。

### 雙樹紀

為使年表一貫性，依然使用維拉時代的紀年，維拉時代4580年之後，多是已經確定了的紀年。

  - 3501年，被視為雙樹紀元年。

<!-- end list -->

  - 維拉大地之神奧力等不及[伊露維塔的孩子出生](../Page/伊露維塔的孩子.md "wikilink")，自行創作[矮人](../Page/矮人.md "wikilink")，當矮人的七個祖先製作完成後，被伊露維塔發現，伊露維塔指明奧力創作超越權限，奧力悔過和懇求，結果伊露維塔收矮人為養子，並在[首生的孩子甦醒之後才會醒來](../Page/精靈.md "wikilink")。奧力妻子維拉植物女神雅凡娜知道事情後焦慮不安，害怕矮人砍伐樹木，要求伊露維塔保護樹木，結果伊露維塔創作樹之牧者，[樹人](../Page/樹人.md "wikilink")。

<!-- end list -->

  - 4500年，維拉們討論雅凡娜和維拉狩獵之神[歐羅米從中土大陸帶回的消息](../Page/歐羅米.md "wikilink")，米爾寇建造[安格班](../Page/安格班.md "wikilink")，托卡斯要求發動戰爭，但是維拉冥神[曼督斯表示時期未到](../Page/曼督斯.md "wikilink")，因為伊露維塔的孩子尚未出生，否決托卡斯的提議。

<!-- end list -->

  - 瓦爾妲收集雙聖樹之[露水](../Page/露水.md "wikilink")，創作[星座](../Page/星座.md "wikilink")。

<!-- end list -->

  - 瓦爾妲設置[米涅爾瑪卡](../Page/米涅爾瑪卡.md "wikilink")（即現實的[獵戶座](../Page/獵戶座.md "wikilink")）和其它星座於天空上。

<!-- end list -->

  - 4550年，瓦爾妲完成星座創作，創作星座[維拉科卡](../Page/維拉科卡.md "wikilink")（意思為「維拉的鐮刀」，即現實的[大熊座](../Page/大熊座.md "wikilink")，[北斗七星](../Page/北斗七星.md "wikilink")），對米爾寇挑戰，維拉科卡成為米爾寇厄運難逃的記號。

## 參見

  - [魔戒](../Page/魔戒.md "wikilink")
  - [精靈寶鑽](../Page/精靈寶鑽.md "wikilink")

[en:History of Arda\#Years of the
Trees](../Page/en:History_of_Arda#Years_of_the_Trees.md "wikilink")
[fr:Chronologie de la Terre du Milieu\#Les Années des
Arbres](../Page/fr:Chronologie_de_la_Terre_du_Milieu#Les_Années_des_Arbres.md "wikilink")
[ja:アルダの歴史\#二本の木の時代](../Page/ja:アルダの歴史#二本の木の時代.md "wikilink")
[pl:Historia Ardy\#Lata
Drzew](../Page/pl:Historia_Ardy#Lata_Drzew.md "wikilink")

[YT](../Category/中土大陸.md "wikilink")