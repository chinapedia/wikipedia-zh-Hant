《**雙生兒**》是[香港](../Page/香港.md "wikilink")[組合](../Page/組合.md "wikilink")[Twins的粵語音樂專輯](../Page/Twins.md "wikilink")，於2002年1月29日在[香港發行](../Page/香港.md "wikilink")，專輯《双生儿》CD+VCD收录了‘着睡衣跳舞’及贺年歌‘孖宝668’等5首新歌。而VCD上就有‘孖宝668’及‘爱情当入樽’的MTV。在‘孖宝668’的MTV中，Twins以全新搞笑形象示人，阿Asa
更反串男角，和风骚入骨的阿嬌向大家拜年。憑《大浪漫主義》及《孖寶668》兩曲廣受[香港欢迎](../Page/香港.md "wikilink")。

## 曲目

## 曲目資料

  - 《孖寶668》為[Twins第一首賀年歌曲](../Page/Twins.md "wikilink")，其後她們每年均推出一首賀年歌曲。此曲為把過去賀年歌曲串燒成一曲。

## 相關專輯

### Twins MV Karaoke VCD/DVD

為收錄[愛情當入樽和](../Page/愛情當入樽.md "wikilink")[Twins碟內的MV](../Page/Twins_\(EP\).md "wikilink")，於2002年4月20日在[香港發行](../Page/香港.md "wikilink")，並收錄**雙生兒**曲目《孖寶668》。

#### 曲目

1.  明愛暗戀補習社
2.  女校男生
3.  換季
4.  愛情當入樽
5.  戀愛大過天
6.  有所不知
7.  快熟時代
8.  學生手冊
9.  和平日
10. 孖寶668

[Category:Twins音樂專輯](../Category/Twins音樂專輯.md "wikilink")
[Category:2002年音樂專輯](../Category/2002年音樂專輯.md "wikilink")
[Category:2002年迷你專輯](../Category/2002年迷你專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")