是1988年上映的[美國](../Page/美國.md "wikilink")[電影](../Page/美國電影.md "wikilink")，以兄弟之情為主題。是由[巴瑞·萊文森執導](../Page/巴瑞·萊文森.md "wikilink")，劇本是由[罗纳德·貝斯](../Page/罗纳德·貝斯.md "wikilink")（Ronald
Bass）及[巴利·摩亞](../Page/巴利·摩亞.md "wikilink")（Barry
Morrow）撰寫。這個故事根据真人真事改編，主要製作的靈感是來自[金·匹克](../Page/金·匹克.md "wikilink")。[汤姆·克鲁斯憑此影片開始成名](../Page/汤姆·克鲁斯.md "wikilink")，促使他在演界平步青雲。

电影講述自私的查理（，由[湯姆·克魯斯飾](../Page/湯姆·克魯斯.md "wikilink")），發現父親將幾乎所有遺產都留給了雷蒙（，由[德斯汀·荷夫曼飾](../Page/德斯汀·荷夫曼.md "wikilink")），一個他一直以來從不知道的兄長，只留下一輛舊款[轿车及玫瑰花園給自己](../Page/轿车.md "wikilink")，所以查理希望奪回資產。

## 情节介绍

查理在兩歲時失去母愛，他因在年少時偷開父親的心愛古董[別克轎車出去兜風而與父親關係決裂](../Page/別克.md "wikilink")。查理一直以為自己是獨生子，但記得「-{雨人}-」是他小時候的玩伴，「-{雨人}-」經常唱歌給查理聽。長大後，查理認為「-{雨人}-」只是自己幼時幻想出來的虛構人物。

查理在[洛杉磯經營著一家小型汽車公司](../Page/洛杉磯.md "wikilink")，但環保部門對他進口高級汽車的漫長審批令他交貨不順而財務困窘。一天，他接到父親離世的消息，決定回[辛辛那提老家辦理父親後事及繼承遺產](../Page/辛辛那提.md "wikilink")。然而查理發現父親把房屋及約三百萬美元的資產，放進信託基金給予一個不認識的受益人，只留下那輛令兩人關係決裂的古董別克轎車及得獎玫瑰花叢給他。

不甘心的查理一路追尋線索，發現該受益人竟是他的親生哥哥雷蒙。兩人的父親害怕患有[自閉症的雷蒙因失去母親](../Page/自閉症.md "wikilink")，令他情緒受創而傷害到幼小的查理，於是送他至[精神病療養院隔離](../Page/精神病.md "wikilink")，一住就是三十年。查理對這位自兩歲便分別，三十年不見的兄長毫無記憶，也發現周遭的人刻意不對他提起雷蒙的事。

查理趁院方不注意時，偷帶雷蒙回洛杉磯，一路上想方設法從基金管理人也就是療養院院長手中取得父親留給雷蒙的遺產之一半，他認為這是他應得的。旅程中，查理驚訝的發現雷蒙就是他記憶中的「-{雨人}-」，雷蒙害怕下雨把自己的名字唸為「雷曼」（Rainman），還看到了雷蒙保管的兩人合照。

途經一間餐館用餐時，雷蒙不小心打翻了牙籤盒，卻能即時算出盒內牙籤的數目。查理因此發現雷蒙的記憶及數字上有莫大的驚人能力，就帶他至[拉斯维加斯賭場玩](../Page/拉斯维加斯.md "wikilink")[二十一點](../Page/二十一點.md "wikilink")，利用算牌技巧贏得了六萬多美元，剛好能填補上查理汽車公司的資金缺口。而更通過教導雷蒙跳舞和與雷蒙相處的時間，查理對雷蒙的兄弟情感漸漸甦醒。及後返回洛杉磯的住處，查理才驚覺自己對雷蒙的情感已經無法割捨。

這時，上百萬的遺產於查理而言，已經比不上對雷蒙的情感。查理一直想爭回對雷蒙的監護權，但訴訟失敗，雷蒙的監護權裁定給療養院方，而雷蒙也因此必須被送回療養院。查理在送別時對雷蒙的依依不捨，讓查理最終明白到自己內心一直渴望的是家人的情感。

## 主要角色

| 演員                                                                         | 角色     |
| -------------------------------------------------------------------------- | ------ |
| [达斯汀·霍夫曼](../Page/达斯汀·霍夫曼.md "wikilink")                                   | 雷蒙·巴比特 |
| [汤姆·克鲁斯](../Page/汤姆·克鲁斯.md "wikilink")                                     | 查理·巴比特 |
| [薇拉莉·葛琳諾](../Page/薇拉莉·葛琳諾.md "wikilink")                                   | 蘇珊     |
| 杰拉爾德·莫倫（[Gerald R. Molen](../Page/:en:Gerald_R._Molen.md "wikilink")）      | 巴頓醫生   |
| 傑克·默多克（[Jack Murdock](../Page/:en:Jack_Murdock.md "wikilink")）             | 約翰·莫尼  |
| 米高·羅拔士（[Michael D. Roberts](../Page/:en:Michael_D._Roberts.md "wikilink")） | 偉倫     |
| 拉夫·西摩爾（[Ralph Seymour](../Page/:en:Ralph_Seymour.md "wikilink")）           | 藍尼     |
| 露辛達·傑尼（[Lucinda Jenney](../Page/:en:Lucinda_Jenney.md "wikilink")）         | 艾華     |
| 邦妮·亨特（[Bonnie Hunt](../Page/:en:Bonnie_Hunt.md "wikilink")）                | 莎利     |
| 金·羅拔蘭（[Kim Robillard](../Page/:en:Kim_Robillard.md "wikilink")）            | 小鎮心理醫生 |
| 贝丝·格兰特（[Beth Grant](../Page/:en:Beth_Grant.md "wikilink")）                 | 農莊中的母親 |

## 獎項

### 奧斯卡金像獎

《雨人》在第六十一屆[奧斯卡金像獎獲得八項提名及四項大獎](../Page/奧斯卡金像獎.md "wikilink")，包括：最佳影片、最佳導演、最佳男主角（达斯汀·霍夫曼）及最佳原創劇本。另外未獲獎的四項提名包括：最佳美術設計、最佳攝影獎、最佳電影剪輯及最佳電影配樂。

### 柏林電影節

《雨人》奪得了1989年第三十九屆[柏林电影节的金熊獎](../Page/柏林电影节.md "wikilink")。雨人是惟一一部既奪得金熊獎又获得奧斯卡最佳影片的电影。

### 意大利大衛獎

《雨人》在意大利大衛獎中奪得了最佳外語片、外國男演員（达斯汀·霍夫曼）\[1\]。

## 瑣事

  - 原來的劇本設定雷蒙的性格是一個開朗友善，十分熱情的人。後來在达斯汀·霍夫曼的極力游說導演及編劇，把雷蒙改變成為一個孤僻內向，不愛說話的自閉症患者。
  - 《雨人》原先是由[斯蒂芬·斯皮尔伯格籌備執導](../Page/斯蒂芬·斯皮尔伯格.md "wikilink")，後來他選擇了執導《[聖戰奇兵](../Page/聖戰奇兵.md "wikilink")》\[2\]。
  - 《辛普森一家》部份集數內容與雨人有不謀而合的部份。

## 参考文献

## 外部链接

  -
  -
  -
  -
  -
  - [HMV香港](http://www.hmv.com.hk/ch/product/dvd.asp?sku=593239)

  - 第六十一屆金像獎－《手足情未了》
    [中國網](http://www.china.com.cn/info/zhuanti/79thosk/txt/2007-02/14/content_7830569.htm)

  - NASA欲破《手足情未了》神腦之謎
    [新華網](http://tech.sina.com.cn/d/2005-03-21/1654556925.shtml)

## 参见

  - [自闭症](../Page/自闭症.md "wikilink")
  - [學者症候群](../Page/學者症候群.md "wikilink")
  - [潜水钟与蝴蝶](../Page/潜水钟与蝴蝶.md "wikilink")
  - [電影索引](../Page/電影索引.md "wikilink")

{{-}}

[Category:1988年電影](../Category/1988年電影.md "wikilink")
[Category:美國劇情片](../Category/美國劇情片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:家庭片](../Category/家庭片.md "wikilink")
[Category:米高梅電影](../Category/米高梅電影.md "wikilink")
[Category:奧斯卡最佳影片](../Category/奧斯卡最佳影片.md "wikilink")
[Category:奧斯卡最佳導演獲獎電影](../Category/奧斯卡最佳導演獲獎電影.md "wikilink")
[Category:奧斯卡最佳原創劇本獲獎電影](../Category/奧斯卡最佳原創劇本獲獎電影.md "wikilink")
[Category:奧斯卡最佳男主角獲獎電影](../Category/奧斯卡最佳男主角獲獎電影.md "wikilink")
[Category:金熊獎](../Category/金熊獎.md "wikilink")
[Category:金球獎最佳劇情片](../Category/金球獎最佳劇情片.md "wikilink")
[Category:公路電影](../Category/公路電影.md "wikilink")
[Category:自閉症題材電影](../Category/自閉症題材電影.md "wikilink")
[Category:學者症候群題材電影](../Category/學者症候群題材電影.md "wikilink")
[Category:奧克拉荷馬州取景電影](../Category/奧克拉荷馬州取景電影.md "wikilink")
[Category:內華達州取景電影](../Category/內華達州取景電影.md "wikilink")
[Category:肯塔基州取景電影](../Category/肯塔基州取景電影.md "wikilink")
[Category:俄亥俄州取景電影](../Category/俄亥俄州取景電影.md "wikilink")
[Category:金球獎最佳戲劇類男主角獲獎電影](../Category/金球獎最佳戲劇類男主角獲獎電影.md "wikilink")

1.  [國立交通大學浩然圖書館](http://www.lib.nctu.edu.tw/Multimedia/new/index/LDF/LDF000020.html)
2.  斯蒂芬·斯皮尔伯格专访 [閃樂](http://blog.9mv.com/article_16678.html)