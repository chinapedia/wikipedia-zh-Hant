《**雪山飞狐**》，[金庸第四部](../Page/金庸.md "wikilink")[武俠小說](../Page/武俠小說.md "wikilink")，是《[飞狐外传](../Page/飞狐外传.md "wikilink")》「姊妹篇」，與其情節互相關聯，但又各自獨立。1955年成稿，1959年开始连载于[香港](../Page/香港.md "wikilink")《[新晚报](../Page/新晚报.md "wikilink")》。

## 故事大綱

發生於[清](../Page/清.md "wikilink")[乾隆四十五年三月十五日](../Page/乾隆.md "wikilink")。以插叙倒叙为主要叙述方法，通过各次要人物的對話中“[羅生門](../Page/羅生門_\(電影\).md "wikilink")”式的回忆敍述，帶出从[李自成起义到故事发生时](../Page/李自成.md "wikilink")，主角[胡斐之父](../Page/胡斐.md "wikilink")[胡一刀與武林人大漢奸士之间恩怨情仇的故事](../Page/胡一刀.md "wikilink")。

故事的開頭是因為要搶奪一個鐵盒，天龙门众人誤以為[陶百歲](../Page/陶百歲.md "wikilink")、[陶子安父子殺害天龍門掌門](../Page/陶子安.md "wikilink")[田歸農](../Page/田歸農.md "wikilink")，於是便跟蹤二人要搶回鐵盒，在爭鬥的過程中，[熊元獻](../Page/熊元獻.md "wikilink")、[劉元鶴](../Page/劉元鶴.md "wikilink")、[鄭三娘也加入搶奪之列](../Page/鄭三娘.md "wikilink")，後來，[寶樹和尚出面制服了所有的人](../Page/寶樹和尚.md "wikilink")，並且將所有的人帶到一座山峰上的山莊，山莊的主人叫[杜希孟](../Page/杜希孟.md "wikilink")，是「打遍天下無敵手」金面佛[苗人鳳的朋友](../Page/苗人鳳.md "wikilink")，[杜希孟之所以要邀英雄豪傑上山助陣](../Page/杜希孟.md "wikilink")，乃是因為雪山飛狐要向他挑戰，[寶樹帶著大家來到這裡](../Page/寶樹.md "wikilink")，也是因為這次挑戰跟上一代以及鐵盒有關係。

在[杜希孟還沒回山莊之前](../Page/杜希孟.md "wikilink")，[寶樹](../Page/寶樹.md "wikilink")、和[苗人鳳的女兒](../Page/苗人鳳.md "wikilink")[苗若蘭說出了鐵盒的秘密](../Page/苗若蘭.md "wikilink")，原來鐵盒中是闯王[李自成的寶刀](../Page/李自成.md "wikilink")。當年闯王身邊有分別姓[胡](../Page/胡姓.md "wikilink")、姓[苗](../Page/苗姓.md "wikilink")、姓[范及姓](../Page/范.md "wikilink")[田四大近身衛士](../Page/田姓.md "wikilink")，四人皆武功高強。闖王敗亡後，[苗](../Page/苗姓.md "wikilink")、[范](../Page/范.md "wikilink")、[田三名衛士誤會姓](../Page/田姓.md "wikilink")[胡的衛士殺了](../Page/胡姓.md "wikilink")[李自成而投靠大汉奸](../Page/李自成.md "wikilink")[吳三桂](../Page/吳三桂.md "wikilink")，於是追殺姓胡的衛士，姓胡的衛士最後死了，但是他兒子長大後告訴苗、[范](../Page/范.md "wikilink")、田這三位衛士當初他父親並沒有殺[李自成](../Page/李自成.md "wikilink")，反而他為了保護李自成，以一將士屍體假扮成李自成屍首，向吳三桂投降，是故意接近[吳三桂](../Page/吳三桂.md "wikilink")，想要把握時機復仇，而[李自成亦因此避過清廷搜捕](../Page/李自成.md "wikilink")，當時仍在生。這三位衛士聽完後很慚愧，就都自殺死了。但三人死前沒有向後人交待此事，故苗、[范](../Page/范.md "wikilink")、田三人的後代並不知情，以為是他們的父親是被姓胡的害死，立志要向姓胡的報仇。於是世世代代的恩怨就這樣一直累積下去超過一百年。

後來[寶樹](../Page/寶樹.md "wikilink")、[陶百歲](../Page/陶百歲.md "wikilink")、[劉元鶴又陸續說出了鐵盒跟地圖的秘密](../Page/劉元鶴.md "wikilink")，原來雪山飛狐[胡斐是胡姓衛士的後人](../Page/胡斐.md "wikilink")，金面佛[苗人鳳及女兒苗若蘭是苗姓衛士的後人](../Page/苗人鳳.md "wikilink")，[田歸農與丐幫范幫主是田姓及范姓衛士的後人](../Page/田歸農.md "wikilink")。而這世代的恩怨一直累積到雪山飛狐[胡斐的父親](../Page/胡斐.md "wikilink")[胡一刀與](../Page/胡一刀.md "wikilink")[苗人鳳身上](../Page/苗人鳳.md "wikilink")。廿七年前，[苗人鳳](../Page/苗人鳳.md "wikilink")、[田歸農與](../Page/田歸農.md "wikilink")[范幫主要與](../Page/范幫主.md "wikilink")[胡一刀比試](../Page/胡一刀.md "wikilink")，[田歸農](../Page/田歸農.md "wikilink")、[范幫主無法打贏](../Page/范幫主.md "wikilink")，最後由[苗人鳳與](../Page/苗人鳳.md "wikilink")[胡一刀比試](../Page/胡一刀.md "wikilink")，他們總共戰了五天五夜，在這五天中，他們早已互相崇拜對方，彼此更是英雄惜英雄，但是比試總要有個勝負，最後[胡一刀與](../Page/胡一刀.md "wikilink")[苗人鳳交手時手部被刀所刺受輕傷](../Page/苗人鳳.md "wikilink")，但因兵器不知何故被塗上劇毒，胡一刀中毒身亡，其妻亦自殺殉夫。[苗人鳳原本要遵守諾言的收養](../Page/苗人鳳.md "wikilink")[胡一刀剛出生的兒子](../Page/胡一刀.md "wikilink")[胡斐](../Page/胡斐.md "wikilink")，但胡斐卻不知所蹤。而山莊下人[平阿四此時說出實情](../Page/平阿四.md "wikilink")，表示當時他是客店雜工，胡一刀為他還債，故有恩於他，他目睹跌打醫師[閻基受](../Page/閻基.md "wikilink")[田歸農指使](../Page/田歸農.md "wikilink")，在[胡一刀及](../Page/胡一刀.md "wikilink")[苗人鳳的兵器上下毒](../Page/苗人鳳.md "wikilink")。胡一刀夫婦被害後，他怕奸人會對胡斐不利，將他抱走，之後將他撫養成人。[平阿四亦指當年下毒的](../Page/平阿四.md "wikilink")[閻基](../Page/閻基.md "wikilink")，就是現在的和尚[寶樹](../Page/寶樹.md "wikilink")。

之後[劉元鶴又說出了鐵盒跟地圖的大秘密](../Page/劉元鶴.md "wikilink")，原本大家都以為鐵盒中的寶刀只是很珍貴而已，沒想到如果將寶刀配合一張地圖，便可以尋找到當時[李自成的大寶藏](../Page/李自成.md "wikilink")，原本地圖一直在苗家手中，寶刀則一直歸田家收藏，如今寶刀在[寶樹手中](../Page/寶樹.md "wikilink")，地圖在[苗若蘭頭上的珠釵](../Page/苗若蘭.md "wikilink")，[劉元鶴強行取下了](../Page/劉元鶴.md "wikilink")[苗若蘭珠釵上的地圖](../Page/苗若蘭.md "wikilink")，並將她關在房間內。此時原本打鬧的一群人，為了找尋寶藏竟合力起來，由[寶樹](../Page/寶樹.md "wikilink")、[劉元鶴帶領著眾人去尋寶藏](../Page/劉元鶴.md "wikilink")。

[杜希孟回來後](../Page/杜希孟.md "wikilink")，原來他已與[范幫主](../Page/范幫主.md "wikilink")、[賽總管合作要殺](../Page/賽總管.md "wikilink")[苗人鳳](../Page/苗人鳳.md "wikilink")，在打鬥之中，[胡斐救了](../Page/胡斐.md "wikilink")[苗人鳳](../Page/苗人鳳.md "wikilink")，但[苗人鳳卻因誤會](../Page/苗人鳳.md "wikilink")[胡斐對](../Page/胡斐.md "wikilink")[苗若蘭無禮而想殺](../Page/苗若蘭.md "wikilink")[胡斐](../Page/胡斐.md "wikilink")，[胡斐帶著](../Page/胡斐.md "wikilink")[苗若蘭離開](../Page/苗若蘭.md "wikilink")，兩人到[寶樹他們找到寶藏的地方](../Page/寶樹.md "wikilink")，將這些貪婪的人全部跟寶藏關在一起，[胡斐帶著](../Page/胡斐.md "wikilink")[苗若蘭回到山莊](../Page/苗若蘭.md "wikilink")，與[苗人鳳一會](../Page/苗人鳳.md "wikilink")，最後的結局則是[胡斐不知是否要殺](../Page/胡斐.md "wikilink")[苗人鳳](../Page/苗人鳳.md "wikilink")，[苗人鳳殺了他的父親](../Page/苗人鳳.md "wikilink")，而他卻愛著[苗若蘭](../Page/苗若蘭.md "wikilink")。

## 登場人物

### 主要人物

  - [胡斐](../Page/胡斐.md "wikilink")——
    本作品第二男主角，「遼東大俠」胡一刀之子，江湖人稱「雪山飛狐」。喜歡苗若蘭。
  - [苗若蘭](../Page/苗若蘭.md "wikilink")——本作品女主角，金面佛苗人鳳和南蘭的女儿。容貌秀丽，娇柔无比，不會武功，处事不惊、遇事不慌，又不少天真善良。喜欢胡斐。
  - [苗人鳳](../Page/苗人鳳.md "wikilink")——外號「打遍天下無敵手」，人稱「金面佛」，闖王四大護衛苗姓護衛後裔。
  - [胡一刀](../Page/胡一刀.md "wikilink")——本作品第一男主角，「遼東大俠」，人稱「黑面神」，[闖王四大護衛胡姓護衛](../Page/李自成.md "wikilink")「飛天狐狸」後裔。
  - [胡夫人](../Page/胡夫人.md "wikilink")——胡一刀夫人，胡斐之母，江湖好手[杜希孟之表妹](../Page/杜希孟.md "wikilink")。
  - [寶樹和尚](../Page/寶樹和尚.md "wikilink")——原名阎基，后得到飞天狐狸的武功秘籍两页，学成了一身武功。曾在胡一刀與苗人鳳決鬥中下毒，導致胡一刀中毒而死，之后出家成为和尚。
  - [范帮主](../Page/范帮主.md "wikilink")——興漢[丐帮帮主](../Page/丐帮.md "wikilink")，闖四大護衛[范姓護衛後裔](../Page/范.md "wikilink")。
  - [田歸農](../Page/田歸農.md "wikilink")——遼東天龍門北宗的掌門，闖四大護衛[田姓護衛後裔](../Page/田.md "wikilink")。
  - 闖王四大護衛（胡、苗、田、范四大護衛）——闖王假死後，苗、田、范因誤會而與胡姓護衛反目成仇，分別喬裝為[腳夫](../Page/腳夫.md "wikilink")、[郎中及](../Page/郎中_\(医生\).md "wikilink")[乞丐前往刺殺對方](../Page/乞丐.md "wikilink")。
  - 闖王[李自成](../Page/李自成.md "wikilink")——為逃避[清兵追殺](../Page/清.md "wikilink")，在[九宮山假死](../Page/九宮山.md "wikilink")，後於[石門县](../Page/石門县.md "wikilink")[夾山](../Page/夾山.md "wikilink")[普慈寺出家為僧](../Page/普慈寺.md "wikilink")，法號**奉天玉**。

### 次要人物

  - [曹雲奇](../Page/曹雲奇.md "wikilink")（遼東天龍門北宗新接任的掌門人『騰龍劍』）
  - [周雲陽](../Page/周雲陽.md "wikilink")（遼東天龍門北宗『迴龍劍』）
  - [阮士中](../Page/阮士中.md "wikilink")（遼東天龍門北宗『七星手』）
  - [殷吉](../Page/殷吉.md "wikilink")（天龍門南宗的掌門人『威震天南』）
  - [陶子安](../Page/陶子安.md "wikilink")（飲馬川山寨、[陶百歲之子](../Page/陶百歲.md "wikilink")）
  - [田青文](../Page/田青文.md "wikilink")（『錦毛貂』[田歸農之女](../Page/田歸農.md "wikilink")）
  - [陶百歲](../Page/陶百歲.md "wikilink")（『鎮關東』飲馬川山寨）
  - [馬寨主](../Page/馬寨主.md "wikilink")（飲馬川山寨）(後被[鄭三娘砍死](../Page/鄭三娘.md "wikilink"))
  - [熊元獻](../Page/熊元獻.md "wikilink")（北京平通鏢局的總鏢頭）
  - [鄭三娘](../Page/鄭三娘.md "wikilink")（『雙刀』[鄭三娘](../Page/鄭三娘.md "wikilink")）
  - [靜智大師](../Page/靜智大師.md "wikilink")（山東百會寺）([陶子安用暗器想殺](../Page/陶子安.md "wikilink")[劉元鶴](../Page/劉元鶴.md "wikilink")，卻被[劉元鶴順手拉過來擋箭而死](../Page/劉元鶴.md "wikilink"))
  - [劉元鶴](../Page/劉元鶴.md "wikilink")（京中一等侍衛『大內十八高手』）
  - [玄冥子道長](../Page/玄冥子.md "wikilink")（青藏派）
  - [靈清居士](../Page/靈清居士.md "wikilink")（崑崙山）
  - [蔣老拳師](../Page/蔣.md "wikilink")（河南太極門）
  - 雙生僮兒（[胡斐之家僮](../Page/胡斐.md "wikilink")，根據書中「二人一模一樣」及年齡的描述很有可能就是[飛狐外傳中馬春花的那對雙生兒子](../Page/飛狐外傳.md "wikilink")，但故事中未有確實證明）
  - [于管家](../Page/于.md "wikilink")（[杜希孟之家僕](../Page/杜希孟.md "wikilink")）
  - [琴兒](../Page/琴兒.md "wikilink")（[苗人鳳之家僕](../Page/苗人鳳.md "wikilink")）
  - 周奶媽（[苗人鳳之家僕](../Page/苗人鳳.md "wikilink")）
  - 韓嬸子（[苗人鳳之家僕](../Page/苗人鳳.md "wikilink")）
  - [賽總管](../Page/賽.md "wikilink")（滿洲武士的第一高手）
  - [田安豹](../Page/田安豹.md "wikilink")（[田歸農之父](../Page/田歸農.md "wikilink")）【與[苗人鳳之父互鬥而死](../Page/苗人鳳.md "wikilink")】
  - [商劍鳴](../Page/商劍鳴.md "wikilink")（『八卦刀』）

## 改編作品

### 電影

  - [1964年](../Page/雪山飛狐_\(1964年電影\).md "wikilink")，香港粵語長片，峨嵋電影公司，共2集，導演[李化](../Page/李化.md "wikilink")，[江漢飾](../Page/江漢_\(香港演員\).md "wikilink")[胡斐](../Page/胡斐.md "wikilink")，[歐嘉慧](../Page/歐嘉慧.md "wikilink")，[李月清](../Page/李月清.md "wikilink")，[石堅主演](../Page/石堅.md "wikilink")。

### 電視劇

以《雪山飛狐》为名称的電視劇系列，皆為[香港](../Page/香港.md "wikilink")[武侠小说作家](../Page/武侠小说.md "wikilink")[金庸的两部书](../Page/金庸.md "wikilink")《[雪山飛狐](../Page/雪山飛狐.md "wikilink")》及《[飛狐外傳](../Page/飛狐外傳.md "wikilink")》之合成劇集，劇名仍作《雪山飛狐》。

<table style="width:460%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 90%" />
<col style="width: 90%" />
<col style="width: 90%" />
<col style="width: 90%" />
<col style="width: 90%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>1978</strong></p></td>
<td><p><strong>1985</strong></p></td>
<td><p><strong>1991</strong></p></td>
<td><p><strong>1999</strong></p></td>
<td><p><strong>2007</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>名稱</strong></p></td>
<td><p><a href="../Page/雪山飛狐_(1978年電視劇).md" title="wikilink">雪山飛狐</a></p></td>
<td><p><a href="../Page/雪山飛狐_(1985年電視劇).md" title="wikilink">雪山飛狐</a></p></td>
<td><p><a href="../Page/雪山飛狐_(1991年電視劇).md" title="wikilink">雪山飛狐</a></p></td>
<td><p><a href="../Page/雪山飛狐_(1999年電視劇).md" title="wikilink">雪山飛狐</a></p></td>
<td><p><a href="../Page/雪山飛狐_(2007年電視劇).md" title="wikilink">雪山飛狐</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>電視台</strong></p></td>
<td><p><a href="../Page/佳藝電視.md" title="wikilink">佳藝電視</a></p></td>
<td><p><a href="../Page/電視廣播有限公司.md" title="wikilink">無綫電視</a></p></td>
<td><p><a href="../Page/台灣電視公司.md" title="wikilink">台灣電視</a></p></td>
<td><p>無綫電視</p></td>
<td><p><a href="../Page/亞洲電視.md" title="wikilink">亞洲電視</a><br />
<a href="../Page/慈文影視.md" title="wikilink">慈文影視</a></p></td>
</tr>
<tr class="even">
<td><p><strong>地區</strong></p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
<td><p>香港</p></td>
<td><p><a href="../Page/中國大陸.md" title="wikilink">中</a><a href="../Page/香港.md" title="wikilink">港</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>又名</strong></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><strong>集數</strong></p></td>
<td><p>53</p></td>
<td><p>40</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>監製</strong></p></td>
<td><p><a href="../Page/蕭笙.md" title="wikilink">蕭笙</a></p></td>
<td><p><a href="../Page/王天林.md" title="wikilink">王天林</a></p></td>
<td><p><a href="../Page/盛竹如.md" title="wikilink">盛竹如</a></p></td>
<td><p><a href="../Page/李添勝.md" title="wikilink">李添勝</a></p></td>
<td><p><a href="../Page/王晶.md" title="wikilink">王晶</a><br />
總導演<a href="../Page/劉偉強.md" title="wikilink">劉偉強</a></p></td>
</tr>
<tr class="even">
<td><p><strong>角色</strong></p></td>
<td><p><strong>演員</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/胡斐.md" title="wikilink">胡斐</a></strong></p></td>
<td><p><a href="../Page/衛子雲.md" title="wikilink">衛子雲</a></p></td>
<td><p><a href="../Page/呂良偉.md" title="wikilink">呂良偉</a></p></td>
<td><p><a href="../Page/孟飛.md" title="wikilink">孟飛</a></p></td>
<td><p><a href="../Page/陳錦鴻.md" title="wikilink">陳錦鴻</a></p></td>
<td><p><a href="../Page/聶遠.md" title="wikilink">聶遠</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/苗若蘭.md" title="wikilink">苗若蘭</a></strong></p></td>
<td><p><a href="../Page/李通明.md" title="wikilink">李通明</a></p></td>
<td><p><a href="../Page/曾華倩.md" title="wikilink">曾華倩</a></p></td>
<td><p><a href="../Page/王璐瑶.md" title="wikilink">王璐瑶</a></p></td>
<td><p><a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a></p></td>
<td><p><a href="../Page/安以軒.md" title="wikilink">安以軒</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/胡一刀.md" title="wikilink">胡一刀</a></strong></p></td>
<td><p>衛子雲</p></td>
<td><p>呂良偉</p></td>
<td><p>孟飛</p></td>
<td><p><a href="../Page/黃日華.md" title="wikilink">黃日華</a></p></td>
<td><p><a href="../Page/黃秋生.md" title="wikilink">黃秋生</a></p></td>
</tr>
<tr class="even">
<td><p><strong>胡一刀夫人</strong></p></td>
<td><p><a href="../Page/馬海倫.md" title="wikilink">馬海倫</a></p></td>
<td><p><a href="../Page/戚美珍.md" title="wikilink">戚美珍</a><br />
（魏映雪）</p></td>
<td><p><a href="../Page/龔慈恩.md" title="wikilink">龔慈恩</a><br />
（冰雪兒）</p></td>
<td><p><a href="../Page/邵美琪.md" title="wikilink">邵美琪</a><br />
（郎劍秋）</p></td>
<td><p><a href="../Page/張彤_(演員).md" title="wikilink">張彤</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/苗人鳳.md" title="wikilink">苗人鳳</a></strong></p></td>
<td><p><a href="../Page/白彪.md" title="wikilink">白彪</a></p></td>
<td><p><a href="../Page/謝賢.md" title="wikilink">謝賢</a></p></td>
<td><p><a href="../Page/慕思成.md" title="wikilink">慕思成</a></p></td>
<td><p><a href="../Page/尹揚明.md" title="wikilink">尹揚明</a></p></td>
<td><p><a href="../Page/方中信.md" title="wikilink">方中信</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/程靈素.md" title="wikilink">程靈素</a></strong></p></td>
<td><p><a href="../Page/文雪兒.md" title="wikilink">文雪兒</a></p></td>
<td><p><a href="../Page/景黛音.md" title="wikilink">景黛音</a></p></td>
<td><p>龔慈恩</p></td>
<td><p><a href="../Page/劉曉彤.md" title="wikilink">劉曉彤</a><br />
（么一一）</p></td>
<td><p><a href="../Page/鍾欣桐.md" title="wikilink">鍾欣桐</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/袁紫衣.md" title="wikilink">袁紫衣</a></strong></p></td>
<td><p><a href="../Page/米雪.md" title="wikilink">米雪</a></p></td>
<td><p><a href="../Page/周秀蘭.md" title="wikilink">周秀蘭</a></p></td>
<td><p><a href="../Page/伍宇娟.md" title="wikilink">伍宇娟</a><br />
（袁銀姑）</p></td>
<td><p><a href="../Page/滕麗明.md" title="wikilink">滕麗明</a><br />
<small>（袁紫衣+馬春花+田青文<br />
合成新角聶桑青）</small></p></td>
<td><p><a href="../Page/朱茵.md" title="wikilink">朱茵</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/田歸農.md" title="wikilink">田歸農</a></strong></p></td>
<td><p><a href="../Page/羅樂林.md" title="wikilink">羅樂林</a></p></td>
<td><p><a href="../Page/曾江.md" title="wikilink">曾江</a></p></td>
<td><p><a href="../Page/湯鎮宗.md" title="wikilink">湯鎮宗</a></p></td>
<td><p><a href="../Page/張兆輝.md" title="wikilink">張兆輝</a></p></td>
<td><p><a href="../Page/譚耀文.md" title="wikilink">譚耀文</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/南蘭.md" title="wikilink">南蘭</a></strong></p></td>
<td><p>陳琬筠</p></td>
<td><p><a href="../Page/陳秀珠.md" title="wikilink">陳秀珠</a></p></td>
<td><p><a href="../Page/袁嘉佩.md" title="wikilink">袁嘉佩</a></p></td>
<td><p><a href="../Page/曹眾.md" title="wikilink">曹眾</a></p></td>
<td><p><a href="../Page/呂一.md" title="wikilink">呂一</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/平阿四.md" title="wikilink">平阿四</a></strong></p></td>
<td><p>招浩東</p></td>
<td><p><a href="../Page/龍天生.md" title="wikilink">龍天生</a></p></td>
<td><p><a href="../Page/關勇.md" title="wikilink">關勇</a></p></td>
<td><p><a href="../Page/廖啟智.md" title="wikilink">廖啟智</a></p></td>
<td><p><a href="../Page/高虎.md" title="wikilink">高虎</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/閻基.md" title="wikilink">閻基</a></strong></p></td>
<td><p><a href="../Page/鄭雷.md" title="wikilink">鄭雷</a></p></td>
<td><p><a href="../Page/許紹雄.md" title="wikilink">許紹雄</a></p></td>
<td><p>-</p></td>
<td><p>劉江</p></td>
<td><p>马维福</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/馬春花.md" title="wikilink">馬春花</a></strong></p></td>
<td><p><a href="../Page/甘婉霞.md" title="wikilink">甘婉霞</a></p></td>
<td><p><a href="../Page/趙雅芝.md" title="wikilink">趙雅芝</a></p></td>
<td><p><a href="../Page/呂瑩盈.md" title="wikilink">呂瑩盈</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/任袁媛.md" title="wikilink">任袁媛</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/福康安.md" title="wikilink">福康安</a></strong></p></td>
<td><p><a href="../Page/伍衛國.md" title="wikilink">伍衛國</a></p></td>
<td><p><a href="../Page/黃允材.md" title="wikilink">黃允材</a></p></td>
<td><p><a href="../Page/林煒.md" title="wikilink">林煒</a></p></td>
<td><p><a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a></p></td>
<td><p><a href="../Page/吳慶哲.md" title="wikilink">吳慶哲</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳家洛_(書劍恩仇錄).md" title="wikilink">陳家洛</a></strong></p></td>
<td><p>-</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/田青文.md" title="wikilink">田青文</a></strong></p></td>
<td><p><a href="../Page/陳寶儀.md" title="wikilink">陳寶儀</a></p></td>
<td><p><a href="../Page/朱小寶.md" title="wikilink">朱小寶</a></p></td>
<td><p><a href="../Page/梁佩玉.md" title="wikilink">梁佩玉</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/李自成.md" title="wikilink">李自成</a></strong></p></td>
<td><p><a href="../Page/喬宏.md" title="wikilink">喬宏</a></p></td>
<td><p><a href="../Page/劉江_(香港).md" title="wikilink">劉江</a></p></td>
<td><p>李杰</p></td>
<td><p><a href="../Page/駱應鈞.md" title="wikilink">駱應鈞</a></p></td>
<td><p>李振起</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/文泰來.md" title="wikilink">文泰來</a></strong></p></td>
<td></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p><a href="../Page/鄺佐輝.md" title="wikilink">鄺佐輝</a></p></td>
<td><p><a href="../Page/桑衛淋.md" title="wikilink">桑衛淋</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>胡卫士</strong></p></td>
<td></td>
<td><p><a href="../Page/甘國衛.md" title="wikilink">甘國衛</a></p></td>
<td><p><a href="../Page/杨玄.md" title="wikilink">杨玄</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>苗卫士</strong></p></td>
<td></td>
<td><p><a href="../Page/吳孟達.md" title="wikilink">吳孟達</a></p></td>
<td><p><a href="../Page/慕思成.md" title="wikilink">慕思成</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>范卫士</strong></p></td>
<td></td>
<td><p><a href="../Page/陳榮峻.md" title="wikilink">陳榮峻</a></p></td>
<td><p><a href="../Page/付克礼.md" title="wikilink">付克礼</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>范帮主</strong></p></td>
<td><p><a href="../Page/曹达华.md" title="wikilink">曹达华</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>田卫士</strong></p></td>
<td></td>
<td><p><a href="../Page/蘇漢生.md" title="wikilink">蘇漢生</a></p></td>
<td><p><a href="../Page/汤镇宗.md" title="wikilink">汤镇宗</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/吴三桂.md" title="wikilink">吴三桂</a></strong></p></td>
<td></td>
<td><p><a href="../Page/劉慶基.md" title="wikilink">劉丹</a></p></td>
<td><p><a href="../Page/穆怀伟.md" title="wikilink">穆怀伟</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/无尘道长.md" title="wikilink">无尘道长</a></strong></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/赵贵.md" title="wikilink">赵贵</a></p></td>
<td></td>
<td><p><a href="../Page/王志强.md" title="wikilink">王志强</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/赵半山.md" title="wikilink">赵半山</a></strong></p></td>
<td><p><a href="../Page/秦煌.md" title="wikilink">秦煌</a></p></td>
<td><p><a href="../Page/张绍滨.md" title="wikilink">张绍滨</a></p></td>
<td><p>秦煌</p></td>
<td><p><a href="../Page/董志华.md" title="wikilink">董志华</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/骆冰.md" title="wikilink">骆冰</a></strong></p></td>
<td></td>
<td><p><a href="../Page/胡美仪.md" title="wikilink">胡美仪</a></p></td>
<td><p><a href="../Page/韩烽.md" title="wikilink">韩烽</a></p></td>
<td><p><a href="../Page/冯晓文.md" title="wikilink">冯晓文</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>常赫志</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/孟繁龙.md" title="wikilink">孟繁龙</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>常伯志</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/王运生.md" title="wikilink">王运生</a></p></td>
</tr>
<tr class="even">
<td><p><strong>無青子(<a href="../Page/陸菲青.md" title="wikilink">陸菲青</a>)</strong></p></td>
<td></td>
<td><p><a href="../Page/羅國維.md" title="wikilink">羅國維</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>徐天宏</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/金鹏.md" title="wikilink">金鹏</a></p></td>
</tr>
<tr class="even">
<td><p><strong>余鱼同</strong></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/李冈.md" title="wikilink">李冈</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>心砚</strong></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/梁思浩.md" title="wikilink">梁思浩</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>相国夫人</p></td>
<td><p>黃蕙芬</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/罗兰.md" title="wikilink">罗兰</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>马行空</strong></p></td>
<td><p><a href="../Page/吳桐.md" title="wikilink">吳桐</a></p></td>
<td><p><a href="../Page/藍天_(演員).md" title="wikilink">蓝天</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>徐铮</strong></p></td>
<td><p><a href="../Page/李銓勝.md" title="wikilink">李銓勝</a></p></td>
<td><p><a href="../Page/李國麟_(演員).md" title="wikilink">李國麟</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>凤天南</strong></p></td>
<td><p><a href="../Page/秦沛.md" title="wikilink">秦沛</a></p></td>
<td><p><a href="../Page/杨泽霖.md" title="wikilink">杨泽霖</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>凤一鸣</strong></p></td>
<td></td>
<td><p><a href="../Page/何贵林.md" title="wikilink">何贵林</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/王伟男.md" title="wikilink">王伟男</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>袁银姑</strong></p></td>
<td></td>
<td><p><a href="../Page/高妙思.md" title="wikilink">高妙思</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>钟阿四</strong></p></td>
<td></td>
<td><p><a href="../Page/孫季卿.md" title="wikilink">孫季卿</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>商剑鸣</strong></p></td>
<td></td>
<td><p><a href="../Page/石坚.md" title="wikilink">石坚</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>商宝震</strong></p></td>
<td><p><a href="../Page/程思俊.md" title="wikilink">程思俊</a></p></td>
<td><p><a href="../Page/关礼杰.md" title="wikilink">关礼杰</a></p></td>
<td><p><a href="../Page/玉尚.md" title="wikilink">玉尚</a></p></td>
<td><p><a href="../Page/容锦昌.md" title="wikilink">容锦昌</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>商老太</strong></p></td>
<td><p>丁茵</p></td>
<td><p><a href="../Page/罗兰.md" title="wikilink">罗兰</a></p></td>
<td></td>
<td><p><a href="../Page/陈安莹.md" title="wikilink">陈安莹</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>王剑杰</strong></p></td>
<td></td>
<td><p><a href="../Page/麦皓为.md" title="wikilink">麦皓为</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>王剑英</strong></p></td>
<td><p><a href="../Page/張鴻昌.md" title="wikilink">張鴻昌</a></p></td>
<td><p><a href="../Page/刘国诚.md" title="wikilink">刘国诚</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>杜希孟</strong></p></td>
<td><p><a href="../Page/馬宗德.md" title="wikilink">馬宗德</a></p></td>
<td><p><a href="../Page/鲍方.md" title="wikilink">鲍方</a></p></td>
<td></td>
<td><p><a href="../Page/鲁振顺.md" title="wikilink">鲁振顺</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>无嗔</strong></p></td>
<td></td>
<td><p><a href="../Page/张英才.md" title="wikilink">张英才</a></p></td>
<td><p><a href="../Page/高振鹏.md" title="wikilink">高振鹏</a></p></td>
<td><p><a href="../Page/鲍方.md" title="wikilink">鲍方</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>石万嗔</strong></p></td>
<td></td>
<td><p><a href="../Page/骆应钧.md" title="wikilink">骆应钧</a></p></td>
<td><p><a href="../Page/马静圆.md" title="wikilink">马静圆</a></p></td>
<td><p><a href="../Page/刘家辉.md" title="wikilink">刘家辉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>慕容景岳</strong></p></td>
<td><p>金貴</p></td>
<td></td>
<td><p><a href="../Page/杨玄.md" title="wikilink">杨玄</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>薛鹊</strong></p></td>
<td><p>江可欣</p></td>
<td></td>
<td><p><a href="../Page/秦迎春.md" title="wikilink">秦迎春</a></p></td>
<td></td>
<td><p><a href="../Page/陈佳佳.md" title="wikilink">陈佳佳</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>汤沛</strong></p></td>
<td><p>溫泉</p></td>
<td><p><a href="../Page/李海生.md" title="wikilink">李海生</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>汪铁鹗</strong></p></td>
<td><p><a href="../Page/麥子雲.md" title="wikilink">麥子雲</a></p></td>
<td><p><a href="../Page/朱铁和.md" title="wikilink">朱铁和</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>曾铁鸥</strong></p></td>
<td></td>
<td><p><a href="../Page/谈泉庆.md" title="wikilink">谈泉庆</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>陶子安</strong></p></td>
<td><p>王錫光</p></td>
<td><p><a href="../Page/关灼元.md" title="wikilink">关灼元</a></p></td>
<td><p><a href="../Page/吴江波.md" title="wikilink">吴江波</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>陶百岁</strong></p></td>
<td><p><a href="../Page/楊澤霖.md" title="wikilink">楊澤霖</a></p></td>
<td><p><a href="../Page/朱刚.md" title="wikilink">朱刚</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>曹云奇</strong></p></td>
<td><p>古耀文</p></td>
<td><p><a href="../Page/李建川.md" title="wikilink">李建川</a></p></td>
<td><p><a href="../Page/于云鹤.md" title="wikilink">于云鹤</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>刘元鹤</strong></p></td>
<td></td>
<td><p><a href="../Page/叶天行.md" title="wikilink">叶天行</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>阮士中</strong></p></td>
<td></td>
<td><p><a href="../Page/陈狄克.md" title="wikilink">陈狄克</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 廣播劇

  - 1981年，[香港電台改編製作](../Page/香港電台.md "wikilink")《雪山飛狐》廣播劇，合共15集，編導[冼杞然](../Page/冼杞然.md "wikilink")，[陳偉匡飾胡斐](../Page/陳偉匡.md "wikilink")，[朱曼子飾苗若蘭](../Page/朱曼子.md "wikilink")，[傅菁葦飾苗人鳳](../Page/傅菁葦.md "wikilink")，[姚秀鈴飾田青文](../Page/姚秀鈴.md "wikilink")，劉安東飾曹雲奇，於1981年6月8日至1981年6月26日，星期一至五上午《金庸武俠天地》時段內播出。

### 漫畫

  - 《雪山飛狐》：[香港漫畫](../Page/香港漫畫.md "wikilink")，監製[馬榮成](../Page/馬榮成.md "wikilink")，[梁偉家](../Page/梁偉家.md "wikilink")、[黃水斌主筆](../Page/黃水斌.md "wikilink")，[天下出版社出版](../Page/天下出版.md "wikilink")。

## 外部連結

  - [經典重溫頻道
    金庸武俠天地—雪山飛狐](http://www.rthk.org.hk/classicschannel/pop_radio80s_0032.htm)（廣播劇）
  - [雪山飛狐
    中華武俠文化網](http://edu.ocac.gov.tw/culture/chinese/cul_kungfu/c/3-2-7.htm)
  - [雪山飛狐
    簡介](https://web.archive.org/web/20150527043549/http://www.reocities.com/richwong_98/jinyong/xsfh/cover.html)

[Category:金庸小說](../Category/金庸小說.md "wikilink")
[Q](../Category/乾隆時期背景小說.md "wikilink")
[\*](../Category/雪山飛狐.md "wikilink")
[Category:平行世界題材小說](../Category/平行世界題材小說.md "wikilink")
[Category:1959年長篇小說](../Category/1959年長篇小說.md "wikilink")