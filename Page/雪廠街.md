[HK_Central_York_House_Ice_House_Street_Gucci.jpg](https://zh.wikipedia.org/wiki/File:HK_Central_York_House_Ice_House_Street_Gucci.jpg "fig:HK_Central_York_House_Ice_House_Street_Gucci.jpg")\]\]
**雪廠街**（）是[香港的一條道路](../Page/香港.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[中環](../Page/中環.md "wikilink")。是單程下山車道。它的南段連接近[藝穗會的](../Page/藝穗會.md "wikilink")[雲咸街](../Page/雲咸街.md "wikilink")，而北段則與[干諾道中交界](../Page/干諾道.md "wikilink")。

## 歷史

雪廠街的得名，是因以前在这里有一座儲存[冰塊的冷藏庫](../Page/冰.md "wikilink")（雪厂其实是雪仓，港人称冰为雪，雪厂就是贮冰的仓库）。1845年4月17日，該冷藏庫在雪廠街建立，供應來自[北美洲的冰塊予附近的醫院及居民](../Page/北美洲.md "wikilink")，直到1880年代為止。

当年香港用的冰都是[美国船运而来的天然冰](../Page/美国.md "wikilink")。美国的大湖和河流冬季结冰所产生的冰块，运到香港后，涂上[锯屑和](../Page/锯屑.md "wikilink")[糠皮防止溶化](../Page/糠皮.md "wikilink")，贮入雪厂待用。供应冰块的公司（）成立于1845年，它的[贮冰仓设于雪厂街](../Page/贮冰仓.md "wikilink")。当时[中环尚未填海](../Page/中环.md "wikilink")，雪厂就在海边，运冰船卸货入仓非常方便。

雪厂是一座两层的建筑物，地皮由政府免费批给，期限为75年，附带条件是廉价供应冰块给公立医院。该厂每天出冰两次，一次为上午五时至七时，一次为下午二时至四时，每天消耗量约七百磅。美国冰一度独占香港的冷藏市场，直到1866年左右，[凯尔才在](../Page/凯尔.md "wikilink")[湾仔](../Page/湾仔.md "wikilink")[春园街一带设厂自制冰块](../Page/春园街.md "wikilink")，与之竞争。1880年以后，本港自制的冰块售价越来越低，远道而来的美国冰无利可图，便停止输入。

输入美国冰的[丢杜公司后来和](../Page/丢杜公司.md "wikilink")[凯尔改组的](../Page/白文信.md "wikilink")[香港制冰公司合并](../Page/香港制冰公司.md "wikilink")，且交由[渣甸洋行经营](../Page/怡和洋行.md "wikilink")。1883年，港府向[渣甸洋行提议](../Page/怡和洋行.md "wikilink")，将雪厂街的地皮长期批与该洋行，租期999年，批价12,500元[渣甸洋行仅肯出价八千元](../Page/怡和洋行.md "wikilink")，并且要分五年缴清，理由是雪厂的地基低于雪厂街的路面，卑湿异常，除了贮冰外难以作其他用途。再后来，雪厂和制冰公司又都卖给[牛奶公司](../Page/牛奶公司.md "wikilink")。雪厂街的古老雪厂，直至1918年[牛奶公司向](../Page/牛奶國際.md "wikilink")[渣甸洋行收买时仍然存在](../Page/怡和洋行.md "wikilink")。

值得留意的是，現存的[舊牛奶公司倉庫](../Page/舊牛奶公司倉庫.md "wikilink")（曾用作冰庫），與使雪廠街得名的雪廠建築物並無任何關係。

## 沿途著名景點

  - [藝穗會](../Page/藝穗會.md "wikilink")
  - [都爹利街煤氣燈](../Page/都爹利街.md "wikilink")
  - [皇后大道中](../Page/皇后大道.md "wikilink")
  - [德輔道中](../Page/德輔道.md "wikilink")
  - [太子大廈](../Page/太子大廈.md "wikilink")
  - [干諾道中的](../Page/干諾道.md "wikilink")[香港文華東方酒店](../Page/香港文華東方酒店.md "wikilink")
  - [聖佐治大廈](../Page/聖佐治大廈.md "wikilink")

## 外部連結

  - [【街道搜查】雪廠街冰庫 北美鮮冰坐船來港 即時新聞
    果籽](http://hk.apple.nextmedia.com/realtime/supplement/20150920/54220694)
    蘋果日報，2015年9月20日

[Category:中環街道](../Category/中環街道.md "wikilink")