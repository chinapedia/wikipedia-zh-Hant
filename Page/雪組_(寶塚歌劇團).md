**雪組**（ゆきぐみ）是[寶塚歌劇團的第三個組](../Page/寶塚歌劇團.md "wikilink")，組的代表色早期為白色，現為綠色。

## 雪組特色

創立於1924年，宝塚大劇場開場之際。戰後，以春日野八千代為中心，以擅長日本舞、日本傳統戲劇的人材輩出著稱，舞台藝風端正，擁有「日本物之雪」的美名。雪組的日舞名取人數比率一直名列五組之首，僅次於專科。歷代主演男役與組員中不少是以歌唱或演技見長。

21世紀後也有幾位以舞蹈為賣點的主演男役，為傳統的日本物之雪添加了新的魅力。近年來受到好評的群舞也逐漸成為雪組新的特色之一。大多演出傳統和風的劇目，例如「忠臣藏」、「仁者俠醫(JIN-仁-)」、「一夢庵風流記
前田慶次」、「るろうに剣心 ([浪客劍心](../Page/浪客劍心.md "wikilink"))」皆為其成功的代表\[1\]。

## 雪組目前編制

主演男役為[望海風斗](../Page/望海風斗.md "wikilink")、主演娘役為[真彩希帆](../Page/真彩希帆.md "wikilink")。

二番手男役為[彩風咲奈](../Page/彩風咲奈.md "wikilink")

組長為奏乃はると、副組長為千風カレン。

其他重要演員有[彩凪翔](../Page/彩凪翔.md "wikilink")、[朝美絢](../Page/朝美絢.md "wikilink")、[永久輝せあ](../Page/永久輝せあ.md "wikilink")、[綾凰華](../Page/綾凰華.md "wikilink")、[星南のぞみ](../Page/星南のぞみ.md "wikilink")、[彩みちる](../Page/彩みちる.md "wikilink")、[縣千](../Page/縣千.md "wikilink")、[潤花](../Page/潤花.md "wikilink")
等(2018年4月現狀)

## 歴代主演男役

  - 郷ちぐさ（1971年3月30日～1972年10月31日）
  - 汀夏子（1972年11月1日～1980年8月1日）
  - 麻実れい（1980年8月2日～1985年4月30日）
  - 平みち（1985年5月1日～1988年11月29日）
  - 杜けあき（1988年11月30日～1993年3月31日）
  - [一路真輝](../Page/一路真輝.md "wikilink")（1993年4月1日～1996年6月30日）
  - [高嶺ふぶき](../Page/高嶺ふぶき.md "wikilink")（1996年7月1日～1997年7月30日）
  - [轟悠](../Page/轟悠.md "wikilink")（1997年7月31日～2002年2月11日）之後異動至専科
  - [絵麻緒ゆう](../Page/絵麻緒ゆう.md "wikilink")（2002年2月12日～2002年9月23日）
  - [朝海光](../Page/朝海光.md "wikilink")（2002年9月24日～2006年12月24日）
  - [水夏希](../Page/水夏希.md "wikilink")（2006年12月25日～2010年9月12日）
  - [音月桂](../Page/音月桂.md "wikilink")（2010年9月13日～2012年12月24日）
  - [壯一帆](../Page/壮一帆.md "wikilink")（2012年12月25日～2014年8月31日）
  - [早霧せいな](../Page/早霧せいな.md "wikilink")（2014年9月1日～2017年7月23日）
  - [望海風斗](../Page/望海風斗.md "wikilink")（2017年7月24日～ ）

## 歴代主演娘役

  - 東千晃（1976年10月1日～1979年12月27日。1979年12月28日異動至星組）
  - 遥くらら（1979年12月28日～1984年7月29日）
  - 神奈美帆（1985年5月1日～1988年11月29日）
  - 鮎ゆうき（1988年11月30日～1991年12月26日）
  - 紫とも（1991年12月27日～1994年3月28日）
  - [花總まり](../Page/花總真理.md "wikilink")（1994年3月29日～1997年12月31日轉任宙組主演娘役）
  - [月影瞳](../Page/月影瞳.md "wikilink")（1998年1月1日～2002年2月11日）
  - [紺野まひる](../Page/紺野まひる.md "wikilink")（2002年2月12日～2002年9月23日）
  - [舞風りら](../Page/舞風りら.md "wikilink")（2002年9月24日～2006年12月24日）
  - [白羽ゆり](../Page/白羽優理.md "wikilink")（2006年12月25日～2009年5月31日）
  - [愛原実花](../Page/愛原實花.md "wikilink")（2009年6月1日～2010年9月12日）
  - [舞羽美海](../Page/舞羽美海.md "wikilink")（2011年3月24日～2012年12月24日）
  - [愛加あゆ](../Page/愛加あゆ.md "wikilink")（2012年12月25日～2014年8月31日）
  - [咲妃みゆ](../Page/咲妃みゆ.md "wikilink")（2014年9月1日～2017年7月23日）
  - [真彩希帆](../Page/真彩希帆.md "wikilink") （2017年7月24日～ ）

## 其他

### 雪組出身在他組擔任主演者

依組順排列

#### 男役

  - 大浦みずき（花）、大滝子（月）、安蘭けい（星）、[和央ようか](../Page/和央ようか.md "wikilink")（宙）、貴城けい（宙）、[凰稀かなめ](../Page/凰稀かなめ.md "wikilink")（宙）

#### 娘役

  - 純名里沙（花）、五條愛川（月）、渚あき（星）

### 其他雪組出身OG

  - [葛城七穂](../Page/葛城七穂.md "wikilink")（1997年退團）-
    現為[聲優](../Page/聲優.md "wikilink")。
  - [神月茜](../Page/神月茜.md "wikilink")（2004年退團） -
    現為歌手。[岡本茜](../Page/岡本茜.md "wikilink")、另以Akane
    Liv為名於樂團[LIV MOON擔任主唱一職](../Page/LIV_MOON.md "wikilink")。
  - [湖条千秋](../Page/湖条千秋.md "wikilink")（1979年退團） -
    現為女演員、妹妹為原星組娘役TOP[湖条れいか](../Page/湖条れいか.md "wikilink")。
  - [毬谷友子](../Page/毬谷友子.md "wikilink")（1985年退團）-
    寶塚音樂學校史上最高畢業成績紀錄保持者，現為女演員。
  - [北原遥子](../Page/北原遥子.md "wikilink")（1984年退團）-
    退團後即以女演員的身分活躍於螢光幕前，但不幸於1985年[日本航空123號班機空難中罹難死亡](../Page/日本航空123號班機空難.md "wikilink")。
  - [朝霧舞](../Page/朝霧舞.md "wikilink")（1994年退團） -
    退團後與[歐力士野牛隊打擊教練](../Page/歐力士野牛.md "wikilink")(當時為內野手)[小川博文結婚](../Page/小川博文.md "wikilink")。
  - [宝樹彩](../Page/AYAKO.md "wikilink")（1997年退團）-
    現為[編舞家](../Page/編舞.md "wikilink")。
  - [楓沙樹](../Page/楓沙樹.md "wikilink")（2002年退團）-　現為瑜珈指導者。
  - [汐美真帆](../Page/汐美真帆.md "wikilink")（2004年退團）- 現為瑜珈指導者。
  - [珠希かほ](../Page/珠希かほ.md "wikilink")（2000年退團） - 退團後隨即與[尾上松緑
    (4代目)結婚](../Page/尾上松緑_\(4代目\).md "wikilink")。
  - [舞坂ゆき子](../Page/舞坂ゆき子.md "wikilink")（2002年退團）-
    歌手[坂本九之女](../Page/坂本九.md "wikilink")。現為歌手。
  - [大月さゆ](../Page/大月さゆ.md "wikilink")（2010年退團）- 現為女演員

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [寶塚歌劇團雪組 Star File](http://kageki.hankyu.co.jp/star/snow.html)（日語）
  - [寶塚歌劇團中文公式網頁](http://takarazukarevue.tw/index.html)
  - [寶塚歌劇團公式網頁](http://kageki.hankyu.co.jp/)（日語）

[Category:寶塚歌劇團](../Category/寶塚歌劇團.md "wikilink")

1.  [宝塚歌劇とは？100年続く華やかな舞台の秘密](https://kageki.hankyu.co.jp/fun/about_takarazuka.html)宝塚歌劇団公式ホームページ