**Snowdome**（雪花球），為[木村KAELA的個人第](../Page/木村KAELA.md "wikilink")8張單曲，
於2007年1月17日由哥倫比亞音樂發行。

## 曲目

1.  **Snowdome**
      - 作詞：木村KAELA 作曲：BEAT CRUSADERS
      - 為本人演出的廣告JR東日本「[JR
        SKISKI](../Page/JR東日本_SKI_SKI.md "wikilink")」電視廣告曲。
2.  **Ground Control**
      - 作詞：Jez Ashurst 作曲：Jez Ashurst
      - 為本人演出的廣告日本[東芝](../Page/東芝.md "wikilink")「[gigabeat](../Page/gigabeat.md "wikilink")」電視廣告曲。
3.  **Snowdome （insturumental）**
4.  **Ground Control (instureumental)**

## 解説

  - 初回限定盤附DVD，收錄2006年第二次巡迴演唱會中的2曲「[Tree
    Climbers](../Page/Tree_Climbers.md "wikilink")」「ROCK
    ON(此曲收錄於[BEAT單曲](../Page/BEAT.md "wikilink"))」演唱影像。
  - 台灣於2007年3月13日由風雲唱片代理發行台壓(CD only版本)，其中文名稱命為「雪花球」。

[Category:木村KAELA歌曲](../Category/木村KAELA歌曲.md "wikilink")
[Category:2007年單曲](../Category/2007年單曲.md "wikilink")
[Category:廣告歌曲](../Category/廣告歌曲.md "wikilink")
[Category:雪題材樂曲](../Category/雪題材樂曲.md "wikilink")