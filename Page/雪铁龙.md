**雪铁龙**（[法文](../Page/法文.md "wikilink")：）是[法国汽车品牌](../Page/法国.md "wikilink")，由[安德烈·雪铁龙于](../Page/安德烈·雪铁龙.md "wikilink")1919年创建。

雪铁龙一直以其超前技术扬名于世。特别是它于1934年推出的[前轮驱动](../Page/前轮驱动.md "wikilink")[Traction
Avant](../Page/Traction_Avant.md "wikilink")，还有赫赫大名的车型[2CV](../Page/2CV.md "wikilink")、[DS](../Page/雪铁龙DS.md "wikilink")、[SM](../Page/雪铁龙SM.md "wikilink")、[CX](../Page/雪铁龙CX.md "wikilink")。雪铁龙归属于[标致雪铁龙集团](../Page/标致雪铁龙集团.md "wikilink")。

雪铁龙的销量在2006年再创新高，达1,406,000辆（+0.8%），其中1,071,000辆（+2.9%）在西欧销售，其他335,000辆（+14.9%），在世界其他地区。

## 香港的中文譯名

[香港](../Page/香港.md "wikilink")（以及[澳門](../Page/澳門.md "wikilink")）最初都是使用「-{zh-hans:雪铁龙;zh-hant:雪鐵龍}-」這個中文譯名，後來聞説「鐵龍」和「鐵籠」同音，意義不太好，所以曾改作「-{zh-hans:先进;zh-hant:先進}-」，但後來又改回全球統一的中文譯名「-{zh-hans:雪铁龙;zh-hant:雪鐵龍}-」。

## 历史

### 开始

[1925](https://zh.wikipedia.org/wiki/File:Tour_Eiffel_Citroen.jpg "fig:1925")
[[雪铁龙C3-XR](../Page/雪铁龙C3-XR.md "wikilink")\[1\]|thumb](https://zh.wikipedia.org/wiki/File:Citroën_C3-XR_01_2014-10-14.jpg "fig:雪铁龙C3-XR|thumb")
[[雪铁龙凯旋](../Page/雪铁龙凯旋.md "wikilink")
(2012)|thumb](https://zh.wikipedia.org/wiki/File:Citroën_C-Quatre_sedan_facelift_China_2012-06-02.JPG "fig:雪铁龙凯旋 (2012)|thumb")
[1934雪铁龙Traction
avant](https://zh.wikipedia.org/wiki/File:Traction_avant.jpg "fig:1934雪铁龙Traction avant")
[[雪铁龙DS](../Page/雪铁龙DS.md "wikilink")|thumb|220px](https://zh.wikipedia.org/wiki/File:De_l'avant_-_Flickr_-_besopha.jpg "fig:雪铁龙DS|thumb|220px")
[Citroen_GT.JPG](https://zh.wikipedia.org/wiki/File:Citroen_GT.JPG "fig:Citroen_GT.JPG")
(2009)\]\]
[2011-03-04_Autosalon_Genf_1307.JPG](https://zh.wikipedia.org/wiki/File:2011-03-04_Autosalon_Genf_1307.JPG "fig:2011-03-04_Autosalon_Genf_1307.JPG")
\]\]

雪鐵龍是在[第一次世界大战结束后不久的](../Page/第一次世界大战.md "wikilink")1919年創立。其創辦人[安德烈·雪铁龙](../Page/安德烈·雪铁龙.md "wikilink")（André
Citroën）在战时曾辦工厂生产炮弹。\[2\]
为了能够生产其第一辆汽车──A型车，他把位于[巴黎JAVEL河岸的工厂改造](../Page/巴黎.md "wikilink")。这款车是欧洲的第一款系列型号车。

1924年，雪铁龙與[美国工程师Edward](../Page/美国.md "wikilink") Gowan
Budd合作。自1899年起，Budd一直從事钢制火车车厢之研究。Budd其後把这一技术运用到汽车生产（尤其是[道奇汽車](../Page/道奇汽車.md "wikilink")）。1925年雪铁龙第一次在欧洲引入钢制车身。

刚开始时雪铁龙取得成功，然而不久竞争者（采用全木车身）在汽车外形上引入了更多的[空气动力学原理](../Page/空气动力学.md "wikilink")。雪铁龙也能够应对，他们把发展得更加现代。

雪铁龙以其低价销售著称，但这政策使雪鐵龍產生利潤上大损失。由于欠银行大量债务，安德烈·雪铁龙决定在30年代初推出一款革命性的车型-[Traction
Avant](../Page/Traction_Avant.md "wikilink")。\[3\]
这款车远远领先于其他竞争对手，在那15年间几乎成了商业成功的代名词。但是在开始时它的使用和维修成本比较高。在1935年安德烈·雪铁龙被Pierre-Jules
Boulanger所取代，走下了领导岗位。

1974年[标致獲得了雪鐵龍](../Page/标致.md "wikilink")38.2%股權。1976年[标致還把雪鐵龍股權擴大到](../Page/标致.md "wikilink")89.95%，變成現今的[标致雪铁龙集团](../Page/标致雪铁龙集团.md "wikilink")。

雪鐵龍汽車的外型設計大都保有獨特風格，不追隨市場流行，如雪鐵龍2CV
車款從1948年生產到1990年，與英國第一代Mini汽車一樣成為長期生產和古典懷舊汽車的代表，雪鐵龍
DS 車款則是汽車界前衛設計的代表作。\[4\]
雪鐵龍專利的電腦控制液壓氣動式底盤懸吊系統在汽車界極為創新與出名，過彎時[電腦會控制懸吊系統軟硬度讓其自動變硬](../Page/電腦.md "wikilink")，減少車身傾斜，即使一輪爆胎剩三個車輪也能自動平衡行駛，行駛舒適性也媲美大型高級[巴士](../Page/巴士.md "wikilink")，英國[劳斯莱斯汽车汽車也曾購買此套系統來使用](../Page/劳斯莱斯汽车.md "wikilink")。

英國[BBC電視汽車節目](../Page/BBC.md "wikilink")"Top gear"曾以配備此套電子控制液壓氣動懸吊的雪鐵龍C6
與配備輕量化全鋁合金懸吊的BMW 5系列(E60)轎車做行駛舒適性比較，結果雪鐵龍C6車上的攝影機畫面晃動明顯較小，證明此種懸吊行駛舒適性較佳。

在[WRC](../Page/WRC.md "wikilink") 越野拉力賽車中雪鐵龍車隊從2003到2012年拿下八次車廠世界冠軍。

1984年，雪铁龙开始在中国销售汽车。\[5\] \[6\]
并在当地设立合资公司--[神龙汽车](../Page/神龙汽车.md "wikilink")，并开始在当地生产[雪铁龙ZX的本地型号](../Page/雪铁龙ZX.md "wikilink")[富康以及相关的变型车](../Page/富康.md "wikilink")，如[东风雪铁龙988](../Page/东风雪铁龙988.md "wikilink")，[东风雪铁龙爱丽舍](../Page/东风雪铁龙爱丽舍.md "wikilink")。之后引进生产了[雪铁龙赛纳和](../Page/雪铁龙赛纳.md "wikilink")[雪铁龙C4的相关车型](../Page/雪铁龙C4.md "wikilink")。

### 历代型号

  - Type A : 1919-1921，第一款多系列车型，驾驶容易。存在型号：运动型、卡车型、履带型等等。
  - 5 CV type C :
    1921-1926，2座或3座，木制车身，没有前刹车，最高时速60km/h，电子点火，发动机为4汽缸、856cm³，热虹吸管冷却，变速箱为3速，弹簧悬挂。由于尾部翘起，外号「鸡屁股」。3座的型号外号「三叶草」，另外这车没有左门。
  - B2 : 1921-1925，取代Type A，有履带型版本。
  - B10 : 1924-1925，全钢车身，变速箱3速。
  - B12 : 1926-1927，发动机为4气缸、453cm³。
  - B14 : 1926-1928，前后均为鼓式刹车，5座。取得了巨大的成功：生产了100,000辆。
  - B15 : 1928，封闭式卡车。
  - C4 : 1928-1932，取代B14，在冷却系统中起用了水泵，分电盘点火，发动机采用减震设计。
  - C6 : 1928-1932，第一款6汽缸雪铁龙，安装了强化玻璃。

## 現今型號

  - [ZERO](../Page/雪铁龙ZERO.md "wikilink") (2010–)
  - [C1](../Page/雪铁龙C1.md "wikilink") (2005–)
  - [DS3](../Page/雪铁龙DS3.md "wikilink") (2009–)
  - [C3](../Page/雪铁龙C3.md "wikilink") (from October 2009)
  - [C3 Picasso](../Page/雪铁龙C3毕加索.md "wikilink") (2009–)
  - [C4](../Page/雪铁龙C4.md "wikilink") (2004–)
  - [C4 Picasso](../Page/雪铁龙C4毕加索.md "wikilink") (2007–)
  - [DS4](../Page/雪铁龙DS4.md "wikilink") (2011–)
  - [C5](../Page/雪铁龙C5.md "wikilink") (2008–)
  - [DS5](../Page/雪铁龙DS5.md "wikilink") (2011–)
  - [Nemo](../Page/雪铁龙Nemo.md "wikilink") (2008–)
  - [Berlingo](../Page/雪铁龙Berlingo.md "wikilink") (1996–)
  - [C4 Aircross](../Page/雪铁龙C4_Aircross.md "wikilink") (2012–)
  - [C8](../Page/雪铁龙C8.md "wikilink") (2002–)
  - [C4 L](../Page/雪铁龙C4_L.md "wikilink") (2012–): 為海外市場而設
  - [爱丽舍](../Page/雪铁龙爱丽舍.md "wikilink")（2002-）: 為海外市場而設
  - [Jumpy](../Page/雪铁龙Jumpy.md "wikilink") (1995–)
  - [Jumper](../Page/雪铁龙Jumper.md "wikilink") (1994–)

## 參考文獻

## 外部連結

  - [Le site sur la 2cv de Citroen](http://www.2cv-legende.com/)
  - [Le portail de Citroën](http://www.citroen.fr/)
  - [Le site institutionnel de Citroën](http://www.citroen.com/)
  - [雪铁龙DSinAsia](http://www.dsinasia.com/China/China.html)

[category:1919年成立的公司](../Page/category:1919年成立的公司.md "wikilink")

[Category:雪铁龙汽车](../Category/雪铁龙汽车.md "wikilink")
[Category:法國汽車公司](../Category/法國汽車公司.md "wikilink")
[C](../Category/汽车品牌.md "wikilink") [C](../Category/法國品牌.md "wikilink")
[Category:1919年法國建立](../Category/1919年法國建立.md "wikilink")
[Category:標緻汽車](../Category/標緻汽車.md "wikilink")

1.  <http://www.dpca.com.cn/dpca/publish/news/W149C578BC97CA3FB840E068content.html>
    　　洞悉消费者 C3-XR进军SUV市场占先机
2.  <http://www.svvs.org/citroen3.shtml>
3.  <http://www.curbsideclassic.com/curbside-classics-european/curbside-classic-citroen-traction-avant-en-indochine/>
4.
5.  <http://karakullake.blogspot.com/2009/04/citroen-cx-taxi-cabs.html>
6.