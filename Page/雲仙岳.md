**雲仙岳**（）是位於[日本](../Page/日本.md "wikilink")[長崎縣](../Page/長崎縣.md "wikilink")[島原半島中央部的](../Page/島原半島.md "wikilink")[火山](../Page/火山.md "wikilink")。廣義上包括了普賢岳、國見岳、妙見岳三峰、野岳、九千部岳、矢岳、高岩山、絹笠山五岳的總稱，被稱為「三峰五岳雲仙岳」。在行政區劃範圍上橫跨了[島原市](../Page/島原市.md "wikilink")、[南島原市](../Page/南島原市.md "wikilink")、[雲仙市](../Page/雲仙市.md "wikilink")。狭義則指「三峰五岳」中的「三峰」。主峰為**普賢岳**、最高峰為**平成新山**。

## 歷史

1791年11月開始發生地震。1792年2月10日普賢岳鳴動，從山頂附近的火山口噴氣，噴出砂土。3月1日熔岩開始流出，持續約兩個月左右。5月21日夜發生火山性地震，整座眉山（當時前山）山崩滑入[有明海](../Page/有明海.md "wikilink")，造成100米高的[超大海嘯](../Page/大海嘯.md "wikilink")，大海嘯在抵達對岸後又反覆在有明海兩岸折返多次，在[島原](../Page/島原.md "wikilink")、[肥後](../Page/肥後.md "wikilink")（今[熊本](../Page/熊本.md "wikilink")）、[天草地區造成約](../Page/天草群島.md "wikilink")15,000人死亡，至今仍是日本歷史上死傷最慘重的火山相關災害。

1990年7月開始發生地震，11月17日噴火發生。1991年6月3日下午4時8分發生[火山碎屑流](../Page/火山碎屑流.md "wikilink")，包括學生、火山學者（包括
Krafft 夫婦與 [哈利·格里肯](../Page/哈利·格里肯.md "wikilink")（Harry
Glicken））、警察、消防人員、司機等在內的43人死亡失蹤、9人輕重傷、179棟建築物被害。1995年2月11日發生最後一次火碎流，2月火山地震急遽減少，從1991年起開始爆發的熔岩停止。1991年至1995年的熔岩噴出量約2億m³。

<center>

<File:Fugendake> 02 Pyroplastic flow area.JPG|主峰普賢岳
[File:UnzenDevastation.jpg|1991年6月3日噴發造成的損害](File:UnzenDevastation.jpg%7C1991年6月3日噴發造成的損害)

</center>

## 外部連結

  - [雲仙温泉観光協会官方網站](http://www.unzen.org/c_ver/)

  - [雲仙岳](http://www.data.jma.go.jp/svd/vois/data/fukuoka/504_Unzendake/504_index.html)
    - 気象庁官方網站

[Category:特别名胜](../Category/特别名胜.md "wikilink")
[Category:九州地方火山](../Category/九州地方火山.md "wikilink")
[Category:長崎縣山峰](../Category/長崎縣山峰.md "wikilink")
[Category:十年火山](../Category/十年火山.md "wikilink")
[Category:20世纪火山事件](../Category/20世纪火山事件.md "wikilink")
[Category:活火山](../Category/活火山.md "wikilink")
[Category:火山穹丘](../Category/火山穹丘.md "wikilink")
[Category:島原市](../Category/島原市.md "wikilink")
[Category:南島原市](../Category/南島原市.md "wikilink")
[Category:雲仙市](../Category/雲仙市.md "wikilink")
[Category:平成百景](../Category/平成百景.md "wikilink")
[Category:警察殉職人員及事件](../Category/警察殉職人員及事件.md "wikilink")