[Qing_Dynasty_Yun-Gui_map_1911.svg](https://zh.wikipedia.org/wiki/File:Qing_Dynasty_Yun-Gui_map_1911.svg "fig:Qing_Dynasty_Yun-Gui_map_1911.svg")
**雲貴總督**（），正式官銜為**總督雲貴等處地方提督軍務、糧饟兼巡撫事**，是[清朝九位最高級的](../Page/清朝.md "wikilink")[封疆大臣之一](../Page/封疆大臣.md "wikilink")，總管[雲南和](../Page/雲南.md "wikilink")[貴州的軍民政務](../Page/貴州.md "wikilink")。

## 沿革

  - 順治十六年，設立經略，然後改設總督。
  - 康熙元年，分別設立雲南總督，總督府駐曲靖；貴州總督，總督府駐安順。
  - 康熙三年，取消雲南總督，由貴州總督兼任，總督府遷貴陽。
  - 康熙十二年，又恢復雲南總督，總督府仍駐曲靖。
  - 康熙二十六年，雲南總督府遷雲南府。
  - 雍正十年，雲貴總督兼轄廣西。
  - 雍正十二年，雲貴總督恢復掌管兩省。
  - 光緒三十一年，雲貴總督兼任雲南巡撫。
  - 清宣統三年，[辛亥革命爆發](../Page/辛亥革命.md "wikilink")，末任總督被革命軍禮送出境。

## 历任总督

  - [洪承畴](../Page/洪承畴.md "wikilink")（1653年－1658年）湖广两广云贵总督
  - [赵廷臣](../Page/赵廷臣.md "wikilink")（1659年－1661年）
  - [佟延年](../Page/佟延年.md "wikilink")（1661年）贵州总督
  - [杨茂勋](../Page/杨茂勋.md "wikilink")（1661年－1665年）贵州总督
  - [卞三元](../Page/卞三元.md "wikilink")（1661年－1668年）
  - [甘文焜](../Page/甘文焜.md "wikilink")（1668年－1673年）
  - [鄂善](../Page/鄂善.md "wikilink")（1673年－1677年）
  - [周有德](../Page/周有德.md "wikilink")（1679年－1680年）
  - [赵良栋](../Page/赵良栋.md "wikilink")（1680年－1682年）
  - [蔡毓榮](../Page/蔡毓榮.md "wikilink")（1682年－1686年）
  - [范承勋](../Page/范承勋.md "wikilink")（1686年－1694年）
  - [丁思孔](../Page/丁思孔.md "wikilink")（1694年）
  - [王继文](../Page/王继文.md "wikilink")（1694年－1698年）
  - [巴锡](../Page/巴锡.md "wikilink")（1698年－1705年）
  - [贝和诺](../Page/贝和诺.md "wikilink")（1705年－1710年）
  - [郭瑮](../Page/郭瑮.md "wikilink")（1710年－1716年）
  - [蒋陈锡](../Page/蒋陈锡.md "wikilink")（1716年－1722年）
  - [张文焕](../Page/张文焕.md "wikilink")（1720年－1722年）
  - [高其倬](../Page/高其倬.md "wikilink")（1722年－1725年）
  - [伊都立](../Page/伊都立.md "wikilink")（1725年）
  - [杨名时](../Page/杨名时.md "wikilink")（1725年－1726年）
  - [鄂爾泰](../Page/鄂爾泰.md "wikilink")（1726年－1731年）1727年管理广西，为云广总督
  - [高其倬](../Page/高其倬.md "wikilink")（1731年－1733年）
  - [尹继善](../Page/尹继善.md "wikilink")（1733年－1737年）1735年复为云贵总督
  - [庆复](../Page/庆复.md "wikilink")（1737年－1741年）
  - [张广泗](../Page/张广泗.md "wikilink")（1736年－1747年）为贵州总督
  - [张允随](../Page/张允随.md "wikilink")（1741年－1750年）
  - [硕色](../Page/硕色.md "wikilink")（1750年－1755年）
  - [爱必达](../Page/爱必达.md "wikilink")（1755年－1756年）
  - [恒文](../Page/恒文.md "wikilink")（1756年－1757年）
  - [爱必达](../Page/爱必达.md "wikilink")（1757年－1761年）
  - [吴达善](../Page/吴达善.md "wikilink")（1761年－1764年）
  - [刘藻](../Page/刘藻.md "wikilink")（1764年－1766年）
  - [杨应琚](../Page/杨应琚.md "wikilink")（1766年－1767年）
  - [明瑞](../Page/明瑞.md "wikilink")（1767年－1768年）
  - [鄂寧](../Page/鄂寧.md "wikilink")（1768年）
  - [阿桂](../Page/阿桂.md "wikilink")（1768年－1769年）
  - [明德](../Page/明德.md "wikilink")（1769年）
  - [阿思哈](../Page/阿思哈.md "wikilink")（1769年）
  - [彰宝](../Page/彰宝.md "wikilink")（1769年－1771年）
  - [德福](../Page/德福.md "wikilink")（1771年）
  - [彰宝](../Page/彰宝.md "wikilink")（1771年－1774年）
  - [图思德](../Page/图思德.md "wikilink")（1774年－1777年）
  - [李侍尧](../Page/李侍尧.md "wikilink")（1777年－1780年）
  - [舒常](../Page/舒常.md "wikilink")（1780年）
  - [福康安](../Page/福康安.md "wikilink")（1780年－1781年）
  - [富纲](../Page/富纲.md "wikilink")（1781年－1786年）
  - [特成额](../Page/特成额.md "wikilink")（1786年）
  - [富纲](../Page/富纲.md "wikilink")（1786年－1794年）
  - [福康安](../Page/福康安.md "wikilink")（1794年－1795年）
  - [勒保](../Page/勒保.md "wikilink")（1795年－1797年）
  - [鄂辉](../Page/鄂辉.md "wikilink")（1797年－1798年）
  - [富纲](../Page/富纲.md "wikilink")（1798年－1799年）
  - [覺羅長麟](../Page/覺羅長麟.md "wikilink")（嘉慶四年）
  - [书麟](../Page/书麟.md "wikilink")（1799年－1800年）
  - [觉罗琅玕](../Page/觉罗琅玕.md "wikilink")（1800年－1804年）
  - [伯麟](../Page/伯麟.md "wikilink")（1804年－1820年）
  - [庆保](../Page/庆保.md "wikilink")（1820年）
  - [史致光](../Page/史致光.md "wikilink")（1820年－1822年）
  - [明山](../Page/明山.md "wikilink")（1822年－1824年）
  - [长龄](../Page/长龄.md "wikilink")（1824年－1825年）
  - [赵慎畛](../Page/赵慎畛.md "wikilink")（1825年－1826年）
  - [阮元](../Page/阮元.md "wikilink")（1826年－1835年）
  - [伊里布](../Page/伊里布.md "wikilink")（1835年－1839年）
  - [邓廷桢](../Page/邓廷桢.md "wikilink")（1839年）
  - [桂良](../Page/桂良.md "wikilink")（1839年－1845年）
  - [賀長齡](../Page/賀長齡.md "wikilink")（1845年－1847年）
  - [李星沅](../Page/李星沅.md "wikilink")（1847年－1848年）
  - [林則徐](../Page/林則徐.md "wikilink")（1848年－1849年）
  - [程矞采](../Page/程矞采.md "wikilink")（1849年－1850年）
  - [吳文鎔](../Page/吳文鎔.md "wikilink")（1850年－1852年）
  - [罗绕典](../Page/罗绕典.md "wikilink")（1852年－1854年）
  - [恒春](../Page/恒春_\(清朝皇族\).md "wikilink")（1854年－1857年）
  - [吴振棫](../Page/吴振棫.md "wikilink")（1857年－1858年）
  - [张亮基](../Page/张亮基.md "wikilink")（1858年－1860年）
  - [刘源灏](../Page/刘源灏.md "wikilink")（1860年－1861年）
  - [福清](../Page/福清.md "wikilink")（1861年）
  - [潘鐸](../Page/潘鐸.md "wikilink")（1861年－1863年）
  - [勞崇光](../Page/勞崇光.md "wikilink")（1863年－1867年）
  - [张凯嵩](../Page/张凯嵩.md "wikilink")（1867年－1868年）
  - [刘岳昭](../Page/刘岳昭.md "wikilink")（1868年－1875年）
  - [刘长佑](../Page/刘长佑.md "wikilink")（1875年－1883年）
  - [岑毓英](../Page/岑毓英.md "wikilink")（1882年－1889年）
  - [谭钧培](../Page/谭钧培.md "wikilink")（1889年）兼署
  - [王文韶](../Page/王文韶.md "wikilink")（1889年－1894年）
  - [崧蕃](../Page/崧蕃.md "wikilink")（1895年－1900年）
  - [魏光焘](../Page/魏光焘.md "wikilink")（1900年－1902年）
  - [丁振铎](../Page/丁振铎.md "wikilink")（1902年－1906年）
  - [岑春煊](../Page/岑春煊.md "wikilink")（1906年－1907年）
  - [锡良](../Page/锡良.md "wikilink")（1907年－1910年）
  - [李经羲](../Page/李经羲.md "wikilink")（1909年－1911年），末代总督。

## 参考文献

  - 《[清史稿](../Page/清史稿.md "wikilink")》

{{-}}

[清朝雲貴總督](../Category/清朝雲貴總督.md "wikilink")
[清朝雲南總督](../Category/清朝雲南總督.md "wikilink")
[清朝貴州總督](../Category/清朝貴州總督.md "wikilink")