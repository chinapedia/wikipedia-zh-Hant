在[抽象代数中](../Page/抽象代数.md "wikilink")，一个[环的一个非零元素](../Page/环_\(代数\).md "wikilink")*a*是一个**左零因子**，当且仅当存在一个非零元素*b*，使得*ab=0*。类似的，一个非零元素*a*是一个**右零因子**，当且仅当存在一个非零元素*b*，使得*ba=0*。左零因子和右零因子通稱為**零因子**（zero
divisor）。\[1\]\[2\]。在[交换环中](../Page/交换环.md "wikilink")，左零因子与右零因子是等价的。一个既不是左零因子也不是右零因子的非零元素称为**[正则的](../Page/正则.md "wikilink")**。

## 例子

  - [整数环](../Page/整数.md "wikilink")**Z**没有零因子，但是在环**Z** × **Z** 中，有(0,ｎ)
    × (ｍ,0) = (0,0)，于是(0,ｎ)和(ｍ,0)都是零因子。

<!-- end list -->

  - 在[商环](../Page/商环.md "wikilink")
    **Z**/6**Z**中，[同余类](../Page/同余类.md "wikilink")4，就是4
    + 6**Z**，是一个零因子，因为3 × 4便是同余类0。

<!-- end list -->

  - 在方[矩阵组成的环中](../Page/矩阵.md "wikilink")，[不可逆矩阵都是零因子](../Page/逆矩陣.md "wikilink")。例如：

\[\begin{pmatrix}1&1\\2&2\end{pmatrix}\]

  -
    因为
    \(\begin{pmatrix}1&1\\2&2\end{pmatrix}\)　\(\cdot\)<math>\\begin{pmatrix}1&1\\\\

\-1&-1\\end{pmatrix}</math>\(=\)\(\begin{pmatrix}-2&1\\
-2&1\end{pmatrix}\)\(\cdot\)　\(\begin{pmatrix}1&1\\
2&2\end{pmatrix}\)　\(=\)　\(\begin{pmatrix}0&0\\0&0\end{pmatrix}\)

  - 　更一般地说，在某些域上的ｎ×ｎ的[矩阵组成的环中](../Page/矩阵.md "wikilink")，左零因子也就是右零因子（实际上就是所有的非零的[奇异矩阵](../Page/可逆矩阵.md "wikilink")）。在某些[整环上的ｎ](../Page/整环.md "wikilink")×ｎ的[矩阵组成的环中](../Page/矩阵.md "wikilink")，零因子就是所有[行列式为](../Page/行列式.md "wikilink")0的非零矩阵。

<!-- end list -->

  - 下面给出一个环中的左零因子和右零因子的例子，它们都不是零因子。
      - 令*S*为所有整数数列的集合，则*S*到*S*的映射，对于数列的加法和映射的复合，成为一个环End(*S*),。
      - 考虑以下三个映射：右移映射：*R*(*a*<sub>1</sub>,
        *a*<sub>2</sub>,*a*<sub>3</sub>,...) = (0, *a*<sub>1</sub>,
        *a*<sub>2</sub>,...)， 左移映射：L(*a*<sub>1</sub>,
        *a*<sub>2</sub>,*a*<sub>3</sub>,... ) = (*a*<sub>2</sub>,
        *a*<sub>3</sub>,...)，以及只保留首项的映射： *T*(*a*<sub>1</sub>,
        *a*<sub>2</sub>,*a*<sub>3</sub>,... ) = (*a*<sub>1</sub>, 0, 0,
        ... )
      - 　*LT*　＝　*TR*　＝　0，所以*L*是一个左零因子，*R*是一个右零因子。但是*L*不是右零因子，*R*也不是左零因子。因为*LR*便是恒等映射。也就是说，如果有一个映射*f*使得*fL*=
        0，那么0＝(*fL*)*R* = *f*(*LR*)= *f*1 =
        *f*，*f*必然是0，于是*L*不可能是右零因子。同理，*R*也不可能是左零因子。
      - 实际上，我们可以将*S*到*S*的映射看作[可数阶数的矩阵](../Page/可数.md "wikilink")，于是左移映射L就可以表示为：

\[A = \begin{pmatrix}
0      & 1 & 0      &0&0&\\
0 & 0 & 1 &0&0&\cdots\\
0 & 0 & 0 &1&0&\\
0&0&0&0&1&\\
&&\vdots&&&\ddots
\end{pmatrix}\]

:\*同理*R*则是*L*的转置矩阵（同时也是*L*的逆矩阵）。可以看出这个例子在有限阶矩阵中是无法构造的。

## 性质

  - 左零因子或右零因子不可能是[可逆元](../Page/可逆元.md "wikilink")。

<!-- end list -->

  - 任意的非零的[等幂元](../Page/等幂.md "wikilink")*a* ≠ 1都是零因子，因为由*a*<sup>2</sup>
    = *a*可推出*a*(*a* − 1) = (*a* − 1)*a* =
    0。此外，[幂零元是当然的零因子](../Page/幂零元.md "wikilink")。

<!-- end list -->

  - 一个非退化的[交换环](../Page/交换环.md "wikilink")（0 ≠
    1）若没有零因子，则是一个[整环](../Page/整环.md "wikilink")。

<!-- end list -->

  - [商环](../Page/商环.md "wikilink")**Z**/''n***Z**包含零因子，当且仅当*n*是[合数](../Page/合数.md "wikilink")。如果*n*是[素数](../Page/素数.md "wikilink")，**Z**/*n'**'Z**是一个域，因而没有零因子，因为每个元素都是[可逆的](../Page/可逆元.md "wikilink")。

<!-- end list -->

  - 在[Cayley-Dickson构造下的](../Page/Cayley-Dickson构造.md "wikilink")[十六元数中](../Page/十六元数.md "wikilink")，也包含了零因子。

## 参见

  - [环](../Page/环_\(代数\).md "wikilink")
  - [整环](../Page/整环.md "wikilink")

## 註釋

## 參考資料

[L](../Category/交換代数.md "wikilink") [L](../Category/环论.md "wikilink")
[Category:零](../Category/零.md "wikilink")

1.
2.