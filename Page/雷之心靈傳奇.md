\-{zh:《**雷之心靈傳奇**》（）;zh-hans:《**灵魂歌王**》（）;zh-hant:《**雷之心靈傳奇**》（）;zh-hk:《》;zh-mo:《》;zh-tw:《**雷之心靈傳奇**》（）;}-，[2004年的](../Page/2004年电影.md "wikilink")[傳記電影](../Page/傳記電影.md "wikilink")，記述傳奇性的[节奏布鲁斯](../Page/节奏布鲁斯.md "wikilink")（R\&B）音樂家[-{雷}-·查爾斯一生中的三十年](../Page/雷·查爾斯.md "wikilink")\[1\]。

這部[獨立製片電影由](../Page/獨立電影.md "wikilink")[泰勒·哈克佛執導](../Page/泰勒·哈克佛.md "wikilink")，[傑米·福克斯演出主角查爾斯](../Page/傑米·福克斯.md "wikilink")，福克斯以這個角色獲得了[2004年](../Page/2004年電影.md "wikilink")[奧斯卡](../Page/第77届奥斯卡金像奖.md "wikilink")[最佳男主角獎](../Page/奧斯卡最佳男主角獎#2000.E5.B9.B4.E4.BB.A3.md "wikilink")。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [《雷之心靈傳奇》官方網站](http://www.raymovie.com/)

      - [中文官方網站](https://web.archive.org/web/20070501214123/http://www.uip.com.tw/ray/)

  -
  - [導演泰勒·海克福沒有侷限的藝術](http://www.washingtonpost.com/wp-dyn/articles/A7913-2004Oct28.html)
    -《[華盛頓郵報](../Page/華盛頓郵報.md "wikilink")》2004年10月的文章

  - [克-{雷}-格·阿姆斯壯（《雷之心靈傳奇》的配樂者）](http://www.craigarmstrongonline.com/film_score/ray.htm)

  - [*It's a shame about
    Ray*'](http://www.slate.com/Default.aspx?id=2108507)
    《Slate》雜誌的評論，列出電影中錯誤之處

  - [電影中歌曲 *Hit The Road Jack*
    的背景](http://www.rollingstone.com/news/story/6596222/hit_the_road_jack)

[Category:2004年电影](../Category/2004年电影.md "wikilink")
[Category:美國劇情片](../Category/美國劇情片.md "wikilink")
[Category:2000年代劇情片](../Category/2000年代劇情片.md "wikilink")
[Category:美國獨立電影](../Category/美國獨立電影.md "wikilink")
[Category:美国传记片](../Category/美国传记片.md "wikilink")
[Category:金球獎最佳音樂或喜劇片](../Category/金球獎最佳音樂或喜劇片.md "wikilink")
[Category:1930年代背景電影](../Category/1930年代背景電影.md "wikilink")
[Category:1940年代背景電影](../Category/1940年代背景電影.md "wikilink")
[Category:1950年代背景電影](../Category/1950年代背景電影.md "wikilink")
[Category:1960年代背景電影](../Category/1960年代背景電影.md "wikilink")
[Category:1970年代背景電影](../Category/1970年代背景電影.md "wikilink")
[Category:1980年代背景電影](../Category/1980年代背景電影.md "wikilink")
[Category:新奥尔良取景电影](../Category/新奥尔良取景电影.md "wikilink")
[Category:環球影業電影](../Category/環球影業電影.md "wikilink")
[Category:2000年代传记片](../Category/2000年代传记片.md "wikilink")
[Category:鋼琴與鋼琴家題材電影](../Category/鋼琴與鋼琴家題材電影.md "wikilink")
[Category:爵士樂題材電影](../Category/爵士樂題材電影.md "wikilink")
[Category:佛羅里達州背景電影](../Category/佛羅里達州背景電影.md "wikilink")
[Category:西雅圖背景電影](../Category/西雅圖背景電影.md "wikilink")
[Category:美國傳記片](../Category/美國傳記片.md "wikilink")
[Category:泰勒·哈克佛電影](../Category/泰勒·哈克佛電影.md "wikilink")
[Category:奧斯卡最佳男主角獲獎電影](../Category/奧斯卡最佳男主角獲獎電影.md "wikilink")
[Category:美國演員工會獎電影類最佳男主角獲獎作品](../Category/美國演員工會獎電影類最佳男主角獲獎作品.md "wikilink")
[Category:英国电影学院奖最佳男主角获奖电影](../Category/英国电影学院奖最佳男主角获奖电影.md "wikilink")
[Category:奥斯卡最佳音响效果获奖电影](../Category/奥斯卡最佳音响效果获奖电影.md "wikilink")

1.  導演泰勒·海克福在DVD特別收錄的評論旁白表示，本片是描述查爾斯在1935年至1968年之間的生活；但電影中最後[朱莉安·龐德](../Page/朱莉安·龐德.md "wikilink")（[Julian
    Bond](../Page/:en:Julian_Bond.md "wikilink")）出現的一幕，是發生在1979年的[喬治亞州議會堂](../Page/喬治亞州議會堂.md "wikilink")（[Georgia
    State
    Capitol](../Page/:en:Georgia_State_Capitol.md "wikilink")），這一幕查爾斯特別要求加入電影中的。