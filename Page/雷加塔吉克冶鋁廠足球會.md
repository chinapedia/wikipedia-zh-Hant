**雷加塔吉克冶鋁廠足球會**（**Regar-TadAZ Tursunzoda**）
是一支位于[塔吉克斯坦城市](../Page/塔吉克斯坦.md "wikilink")[圖爾孫扎德的足球俱乐部](../Page/圖爾孫扎德.md "wikilink")，球队属于塔吉克斯坦顶级联赛球队，是联赛中的绝对强队，球队取得了2005年[亚洲足协会长杯的冠军头衔](../Page/亚洲足协会长杯.md "wikilink")。

俱乐部由雷戈尔塔吉克铝冶炼厂（Regar-TadAZ ）命名。

## 球队荣誉

  - **[塔吉克斯坦足球聯賽](../Page/塔吉克斯坦足球聯賽.md "wikilink"): 5次**

<!-- end list -->

  -

      -
        2001, 2002, 2003, 2004, 2006

<!-- end list -->

  - **[塔吉克斯坦盃](../Page/塔吉克斯坦盃.md "wikilink"): 3次**

<!-- end list -->

  -

      -
        2001, 2005, 2006

<!-- end list -->

  - **[亞足聯主席盃](../Page/亞足聯主席盃.md "wikilink"): 1**

<!-- end list -->

  -

      -
        2005

## 外部链接

  - [Football - Fiche
    club](http://www.footballdatabase.eu/html/football_clubs_regar-tadaz_tursunzoda_0.html)

  - [Tajikistan Cup 2007](http://www.rsssf.com/tablest/tajicup07.html)

  - [Regar-Tadaz
    Tursunzoda](https://web.archive.org/web/20070929152342/http://www.weltfussball.de/teams.php?m_id=7423)

[Category:塔吉克斯坦足球俱乐部](../Category/塔吉克斯坦足球俱乐部.md "wikilink")