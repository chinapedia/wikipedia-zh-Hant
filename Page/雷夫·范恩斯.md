**雷夫·范恩斯**（，，），本名**雷夫·納旦尼爾·土伊斯勒頓-維克漢-范恩斯**（Ralph Nathaniel
Twisleton-Wykeham-Fiennes）\[1\]，[英國](../Page/英國.md "wikilink")[男演員](../Page/男演員.md "wikilink")，曾於《[辛德勒的名單](../Page/辛德勒的名單.md "wikilink")》、《[英倫情人](../Page/英倫情人.md "wikilink")》、《[疑雲殺機](../Page/疑雲殺機.md "wikilink")》、《[哈利波特](../Category/哈利波特系列電影.md "wikilink")》系列電影等片中演出，近期則於電影《[為愛朗讀](../Page/為愛朗讀.md "wikilink")》《[布達佩斯大飯店](../Page/布達佩斯大飯店.md "wikilink")》中擔綱演出。在2012年第23部[007
空降危機中擔綱要角](../Page/007_空降危機.md "wikilink")。

范恩斯曾獲[奧斯卡金像獎提名兩次](../Page/奧斯卡金像獎.md "wikilink")，也是唯一在[百老匯劇院以](../Page/百老匯劇院.md "wikilink")[哈姆雷特王子一角獲](../Page/哈姆雷特.md "wikilink")[東尼獎的男演員](../Page/東尼獎.md "wikilink")。2001年，范恩斯從[華盛頓的莎士比亞劇院獲得了](../Page/華盛頓.md "wikilink")[威廉·莎士比亞獎](../Page/威廉·莎士比亞.md "wikilink")。

范恩斯現任[聯合國兒童基金會英國大使](../Page/聯合國兒童基金會.md "wikilink")（UNICEF UK
ambassador）。\[2\]

## 早年

范恩斯生於[英格蘭](../Page/英格蘭.md "wikilink")[薩福克郡](../Page/薩福克郡.md "wikilink")[伊普斯維奇的一個貴族世家](../Page/伊普斯維奇.md "wikilink")，是農夫兼攝影師[馬克·范恩斯](../Page/馬克·范恩斯.md "wikilink")（1933-2004，其父是企業家[摩理斯·范恩斯爵士](../Page/摩理斯·范恩斯爵士.md "wikilink")）與作家[珍妮佛·賴許](../Page/珍妮佛·賴許.md "wikilink")（1938-1993）之子\[3\]。范恩斯的姓氏起源於[諾曼人](../Page/諾曼人.md "wikilink")\[4\]。他是[查尔斯王子隔了八代的表親](../Page/查尔斯王子.md "wikilink")，也是冒險家[雷諾夫·范恩斯爵士](../Page/雷諾夫·范恩斯.md "wikilink")（Ranulph
Fiennes）的隔了三代的表親。雷夫是七個孩子中最年長的，他的兄弟姐妹有演員[約瑟夫·范恩斯](../Page/約瑟夫·范恩斯.md "wikilink")（《[莎翁情史](../Page/莎翁情史.md "wikilink")》、《[馬丁·路德](../Page/馬丁·路德.md "wikilink")》）、導演[瑪莎·范恩斯](../Page/瑪莎·范恩斯.md "wikilink")（雷夫於她所執導的電影《[遲來的情書](../Page/遲來的情書.md "wikilink")》飾演劇名角色）、作曲家[馬格納斯·范恩斯](../Page/馬格納斯·范恩斯.md "wikilink")、製片人[蘇菲亞·范恩斯](../Page/蘇菲亞·范恩斯.md "wikilink")、環保人士雅各·范恩斯以及義兄、[考古學家麥可](../Page/考古學家.md "wikilink")·埃莫里。

## 私人生活

[2003-10-06_ralph_fiennes_in_Bishkek.jpg](https://zh.wikipedia.org/wiki/File:2003-10-06_ralph_fiennes_in_Bishkek.jpg "fig:2003-10-06_ralph_fiennes_in_Bishkek.jpg")
范恩斯在就讀於[英國皇家戲劇學校時](../Page/英國皇家戲劇學校.md "wikilink")，認識了於同校就讀的[女演員](../Page/女演員.md "wikilink")[愛麗克絲·金斯頓](../Page/愛麗克絲·金斯頓.md "wikilink")（Alex
Kingston）。在交往十年後，兩人於1993結婚，但於1997年離婚。1995年，范恩斯開始與年長他十八歲的女演員[法蘭西絲卡·安妮絲](../Page/法蘭西絲卡·安妮絲.md "wikilink")（Francesca
Annis）交往，其於[哈姆雷特中飾演范恩斯的母親](../Page/哈姆雷特.md "wikilink")。2006年2月，兩人在[小型報媒體揭露了范恩斯與](../Page/小型報.md "wikilink")[羅馬尼亞歌手柯尼黎亞](../Page/羅馬尼亞.md "wikilink")·克里森（Cornelia
Crisan）的緋聞後分手\[5\]。2006年末，有消息指出范恩斯正與[美國女演員](../Page/美國.md "wikilink")[艾倫·巴金](../Page/艾倫·巴金.md "wikilink")（Ellen
Barkin）交往。\[6\]

## 作品

### 電影

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>電影</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1990年電影.md" title="wikilink">1990</a></p></td>
<td><p><em><a href="../Page/A_Dangerous_Man:_Lawrence_After_Arabia.md" title="wikilink">A Dangerous Man: Lawrence After Arabia</a></em></p></td>
<td><p>T. E. Lawrence</p></td>
<td><p>TV</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992年電影.md" title="wikilink">1992</a></p></td>
<td><p><a href="../Page/呼啸山莊.md" title="wikilink">呼啸山莊</a></p></td>
<td><p>Heathcliff</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1993年電影.md" title="wikilink">1993</a></p></td>
<td><p><a href="../Page/魔法聖嬰.md" title="wikilink">魔法聖嬰</a></p></td>
<td><p>The Bishop's son</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/辛德勒的名單.md" title="wikilink">辛德勒的名單</a></p></td>
<td><p><a href="../Page/亞蒙·哥德.md" title="wikilink">亞蒙·哥德</a></p></td>
<td><p><a href="../Page/英國電影學院獎最佳男配角.md" title="wikilink">英國電影學院獎最佳男配角</a><br />
提名 — <a href="../Page/奧斯卡最佳男配角獎.md" title="wikilink">奧斯卡最佳男配角獎</a><br />
提名 — <a href="../Page/金球獎最佳電影男配角.md" title="wikilink">金球獎最佳電影男配角</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1994年電影.md" title="wikilink">1994</a></p></td>
<td><p><a href="../Page/益智遊戲_(1994年電影).md" title="wikilink">益智遊戲</a> (<a href="../Page/幕後謊言.md" title="wikilink">幕後謊言</a>)</p></td>
<td><p>Charles Van Doren</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1995年電影.md" title="wikilink">1995</a></p></td>
<td><p><a href="../Page/廿一世紀的前一天.md" title="wikilink">廿一世紀的前一天</a></p></td>
<td><p>Lenny Nero</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1996年電影.md" title="wikilink">1996</a></p></td>
<td><p><a href="../Page/英倫情人.md" title="wikilink">英倫情人</a></p></td>
<td><p>Count László de Almássy</p></td>
<td><p>提名 — <a href="../Page/奧斯卡最佳男主角獎.md" title="wikilink">奧斯卡最佳男主角獎</a><br />
提名 — <a href="../Page/英國電影學院獎最佳男主角.md" title="wikilink">英國電影學院獎最佳男主角</a><br />
提名 — <a href="../Page/金球獎最佳戲劇類電影男主角.md" title="wikilink">金球獎最佳戲劇類電影男主角</a><br />
提名 — <a href="../Page/影視演員協會獎.md" title="wikilink">影視演員協會獎最佳男主角獎</a><br />
提名 — <a href="../Page/影視演員協會獎.md" title="wikilink">影視演員協會獎最佳電影演員獎</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1997年電影.md" title="wikilink">1997</a></p></td>
<td><p><a href="../Page/奧斯卡與露絲達.md" title="wikilink">奧斯卡與露絲達</a></p></td>
<td><p>Oscar Hopkins</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年電影.md" title="wikilink">1998</a></p></td>
<td><p><a href="../Page/復仇者.md" title="wikilink">復仇者</a></p></td>
<td><p>John Steed</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/埃及王子.md" title="wikilink">埃及王子</a></p></td>
<td><p><a href="../Page/拉美西斯二世.md" title="wikilink">拉美西斯二世</a></p></td>
<td><p>（配音）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1999年電影.md" title="wikilink">1999</a></p></td>
<td><p><a href="../Page/陽光情人.md" title="wikilink">陽光情人</a></p></td>
<td><p>Ignatz Sonnenschein/Adam Sors/Ivan Sors</p></td>
<td><p><a href="../Page/歐洲電影獎.md" title="wikilink">歐洲電影獎最佳歐洲演員</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/遲來的情書.md" title="wikilink">遲來的情書</a></p></td>
<td><p>Evgeny Onegin</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛情的盡頭.md" title="wikilink">愛情的盡頭</a></p></td>
<td><p>Maurice Bendrix</p></td>
<td><p>提名 — <a href="../Page/英國電影學院獎最佳男主角.md" title="wikilink">英國電影學院獎最佳男主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年電影.md" title="wikilink">2000</a></p></td>
<td><p><a href="../Page/奇蹟製造者.md" title="wikilink">奇蹟製造者</a></p></td>
<td><p><a href="../Page/耶穌.md" title="wikilink">耶穌基督</a></p></td>
<td><p>（配音）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年電影.md" title="wikilink">2002</a></p></td>
<td><p><a href="../Page/童魘.md" title="wikilink">童魘</a></p></td>
<td><p>Spider</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/寶刀未老.md" title="wikilink">寶刀未老</a></p></td>
<td><p>Tony Angel</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/紅龍.md" title="wikilink">紅龍</a></p></td>
<td><p>Francis Dolarhyde</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/女傭變鳳凰.md" title="wikilink">女傭變鳳凰</a></p></td>
<td><p>Christopher Marshall</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2005年電影.md" title="wikilink">2005</a></p></td>
<td><p><a href="../Page/我行我素.md" title="wikilink">我行我素</a></p></td>
<td><p>Mayor Michael Ebbs</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/恐色症.md" title="wikilink">恐色症</a></p></td>
<td><p>Stephen Tulloch</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/疑雲殺機.md" title="wikilink">疑雲殺機</a></p></td>
<td><p>Justin Quayle</p></td>
<td><p>提名 — <a href="../Page/英國電影學院獎最佳男主角.md" title="wikilink">英國電影學院獎最佳男主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/酷狗寶貝之魔兔詛咒.md" title="wikilink">酷狗寶貝之魔兔詛咒</a></p></td>
<td><p>Victor Quartermaine</p></td>
<td><p>（配音）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/異國情緣.md" title="wikilink">異國情緣</a></p></td>
<td><p>Todd Jackson</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈利波特－火盃的考驗_(電影).md" title="wikilink">哈利波特－火盃的考驗</a></p></td>
<td><p><a href="../Page/佛地魔.md" title="wikilink">佛地魔</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年電影.md" title="wikilink">2006</a></p></td>
<td><p><em><a href="../Page/Land_of_the_Blind_(film).md" title="wikilink">Land of the Blind</a></em></p></td>
<td><p>Joe</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年電影.md" title="wikilink">2007</a></p></td>
<td><p><a href="../Page/哈利波特－鳳凰會的密令_(電影).md" title="wikilink">哈利波特－鳳凰會的密令</a></p></td>
<td><p><a href="../Page/佛地魔.md" title="wikilink">佛地魔</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/情同姊妹.md" title="wikilink">情同姊妹</a></p></td>
<td><p>Bernard Lafferty</p></td>
<td><p>提名 — <a href="../Page/艾美獎.md" title="wikilink">艾美獎連續短劇及電視電影最佳男主角</a><br />
提名 — <a href="../Page/金球獎_(影視獎項).md" title="wikilink">金球獎最佳連續短劇與電視電影男主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年電影.md" title="wikilink">2008</a></p></td>
<td><p><a href="../Page/殺手沒有假期.md" title="wikilink">殺手沒有假期</a></p></td>
<td><p>Harry Waters</p></td>
<td><p>提名 — <a href="../Page/英國獨立電影獎.md" title="wikilink">英國獨立電影獎最佳男主角</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/浮華一世情.md" title="wikilink">浮華一世情</a></p></td>
<td><p>William Cavendish, 5th Duke of Devonshire</p></td>
<td><p>提名 — <a href="../Page/金球獎最佳電影男配角.md" title="wikilink">金球獎最佳電影男配角</a><br />
提名 — <a href="../Page/英國獨立電影獎.md" title="wikilink">英國獨立電影獎最佳男配角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/為愛朗讀.md" title="wikilink">為愛朗讀</a></p></td>
<td><p>Older Michael Berg</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2009年電影.md" title="wikilink">2009</a></p></td>
<td><p><a href="../Page/危機倒數.md" title="wikilink">危機倒數</a></p></td>
<td><p>傭兵小隊長</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈利波特－混血王子的背叛_(電影).md" title="wikilink">哈利波特－混血王子的背叛</a></p></td>
<td><p><a href="../Page/佛地魔.md" title="wikilink">佛地魔</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年電影.md" title="wikilink">2010</a></p></td>
<td><p><a href="../Page/超世紀封神榜.md" title="wikilink">超世紀封神榜</a></p></td>
<td><p><a href="../Page/黑帝斯.md" title="wikilink">黑帝斯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em>Cemetery Junction</em></p></td>
<td><p>Mr Kendrick</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em>Nanny McPhee and the Big Bang</em></p></td>
<td><p>Lord Gray</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈利波特－死神的聖物1.md" title="wikilink">哈利波特－死神的聖物1</a></p></td>
<td><p><a href="../Page/佛地魔.md" title="wikilink">佛地魔</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011年電影.md" title="wikilink">2011</a></p></td>
<td><p><a href="../Page/哈利波特－死神的聖物2.md" title="wikilink">哈利波特－死神的聖物2</a>[7]</p></td>
<td><p><a href="../Page/佛地魔.md" title="wikilink">佛地魔</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英雄叛國記.md" title="wikilink">英雄叛國記</a></p></td>
<td><p>Caius Martius Coriolanus</p></td>
<td><p>首次擔任導演</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第八頁.md" title="wikilink">第八頁</a><br />
<em>Page Eight</em></p></td>
<td><p>Alec Beasley（<a href="../Page/英國.md" title="wikilink">英國</a><a href="../Page/首相.md" title="wikilink">首相</a>）</p></td>
<td><p><a href="../Page/BBC.md" title="wikilink">BBC電視電影</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012年电影.md" title="wikilink">2012</a></p></td>
<td><p><a href="../Page/怒戰天神.md" title="wikilink">怒戰天神</a></p></td>
<td><p>冥王黑帝斯</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/007_空降危機.md" title="wikilink">007 空降危機</a></p></td>
<td><p>M先生</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/遠大前程.md" title="wikilink">遠大前程</a><br />
<em><a href="https://en.wikipedia.org/wiki/Great_Expectations_(2012_film)">Great Expectations</a></em></p></td>
<td><p>Magwitch</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年电影.md" title="wikilink">2013</a></p></td>
<td><p><em>狄更斯的秘密情史</em></p></td>
<td><p><a href="../Page/查尔斯·狄更斯.md" title="wikilink">查尔斯·狄更斯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年电影.md" title="wikilink">2014</a></p></td>
<td><p><a href="../Page/布達佩斯大飯店.md" title="wikilink">布達佩斯大飯店</a></p></td>
<td><p>M. Gustave H.</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/強尼之危機四伏.md" title="wikilink">-{zh-tw:強尼之危機四伏;zh-cn:钱宁之危机四伏;zh-hk:諜戰風雲;}-</a><br />
<em>Turks &amp; Caicos</em></p></td>
<td><p>Alec Beasley</p></td>
<td><p><a href="../Page/BBC.md" title="wikilink">BBC電視電影</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/永不屈服_(電視電影).md" title="wikilink">-{zh-tw:永不屈服;zh-cn:永不屈服;zh-hk:諜戰風雲續集;}-</a><br />
<em>Salting the Battlefield</em></p></td>
<td><p>Alec Beasley</p></td>
<td><p>BBC電視電影</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015年電影.md" title="wikilink">2015</a></p></td>
<td><p><a href="../Page/007：惡魔四伏.md" title="wikilink">007：惡魔四伏</a><br />
<em>Spectre</em></p></td>
<td><p>M先生</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/烽火母女情.md" title="wikilink">烽火母女情</a><br />
<em>Two Women</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2016年電影.md" title="wikilink">2016</a></p></td>
<td><p><a href="../Page/凱薩萬歲！.md" title="wikilink">凱薩萬歲！</a></p></td>
<td><p>Laurence Lorenz</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/酷寶：魔弦傳說.md" title="wikilink">酷寶：魔弦傳說</a></p></td>
<td><p>The Moon King</p></td>
<td><p>（配音）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2017年电影.md" title="wikilink">2017</a></p></td>
<td><p><a href="../Page/樂高蝙蝠俠電影.md" title="wikilink">樂高蝙蝠俠電影</a></p></td>
<td><p>阿福</p></td>
<td><p>（配音）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2019年电影.md" title="wikilink">2019</a></p></td>
<td><p><a href="../Page/樂高大電影2.md" title="wikilink">樂高大電影2</a></p></td>
<td><p>阿福</p></td>
<td><p>（配音）</p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  -
  - [BroadwayWorld Exclusive World Premiere: In Bruges
    Trailer](http://www.broadwayworld.com/viewcolumn.cfm?colid=23362)

  - [Ralph Fiennes
    interview](http://www.moviehole.net/interviews/20051026_exclusive_interview_ralph_fien.html)

  - [Fiennes' Hostess was an escort - News article about Ralph's mile
    high
    encounter](http://www.news.com.au/dailytelegraph/story/0,22049,21400386-5001021,00.html)

  - [Photos](http://www.thehist.com/index.php?option=content&task=view&id=372&Itemid=317)
    of Ralph Fiennes' visit to the [College Historical
    Society](../Page/College_Historical_Society_\(Trinity_College,_Dublin\).md "wikilink")

[Category:英國演員](../Category/英國演員.md "wikilink")
[Category:哈利·波特演员](../Category/哈利·波特演员.md "wikilink")
[Category:英国电影学院奖最佳男配角获得者](../Category/英国电影学院奖最佳男配角获得者.md "wikilink")
[Category:欧洲电影奖最佳男主角获得者](../Category/欧洲电影奖最佳男主角获得者.md "wikilink")

1.
2.
3.
4.
5.
6.
7.