**雷姆斯-穆尔县**（Rems-Murr-Kreis）是[德国](../Page/德国.md "wikilink")[巴登-符腾堡州的一个县](../Page/巴登-符腾堡州.md "wikilink")，隶属于[斯图加特行政区](../Page/斯图加特行政区.md "wikilink")，首府[魏布林根](../Page/魏布林根.md "wikilink")。

## 地理

雷姆斯-穆尔县北面与[海尔布隆县和](../Page/海尔布隆县.md "wikilink")[施韦比施哈尔县](../Page/施韦比施哈尔县.md "wikilink")，东面与[奥斯特阿尔布县](../Page/奥斯塔尔伯县.md "wikilink")，南面与[格平根县和](../Page/格平根县.md "wikilink")[埃斯林根县](../Page/埃斯林根县.md "wikilink")，西面与[斯图加特市和](../Page/斯图加特.md "wikilink")[路德维希堡县相邻](../Page/路德维希堡县.md "wikilink")。

## 县徽

雷姆斯-穆尔县的县徽是金色的底色上，两条蓝色的浪线和一根黑色的[鹿角](../Page/鹿角.md "wikilink")，鹿角代表14世纪起统治雷姆斯-穆尔县大部分地区的[符腾堡家族](../Page/符腾堡.md "wikilink")，两条浪线分别代表[雷姆斯河](../Page/雷姆斯河.md "wikilink")（Rems）和[穆尔河](../Page/穆尔河.md "wikilink")（Murr），雷姆斯-穆尔县的名字即来自于这两条河流。雷姆斯-穆尔的县徽启用于1974年。

## 参考文献

<div class="references-small">

  - *Das Land Baden-Württemberg* – Amtliche Beschreibung nach Kreisen
    und Gemeinden (in acht Bänden); Hrsg. von der Landesarchivdirektion
    Baden-Württemberg; Band III: Regierungsbezirk Stuttgart –
    Regionalverband Mittlerer Neckar, Stuttgart, 1978, ISBN
    3-17-004758-2

</div>

[R](../Category/巴登-符腾堡州的县.md "wikilink")