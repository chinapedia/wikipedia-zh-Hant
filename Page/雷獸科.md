**雷獸**（[學名](../Page/學名.md "wikilink")：，或稱）是[奇蹄目下一科已](../Page/奇蹄目.md "wikilink")[滅絕的](../Page/滅絕.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")。雖然雷獸有可能是[馬的近親](../Page/馬.md "wikilink")，但外表卻很像[犀牛](../Page/犀牛.md "wikilink")。牠們生存於5千6百萬至3千4百萬年前的[始新世早期至晚期](../Page/始新世.md "wikilink")。

## 特徵及演化

雷獸前肢有四趾，後肢則有三趾。牠們的[牙齒適合撕開](../Page/牙齒.md "wikilink")[植物](../Page/植物.md "wikilink")。牠們的[臼齒有呈W狀的外冠](../Page/臼齒.md "wikilink")。

雷獸在[北美洲有豐富的](../Page/北美洲.md "wikilink")[化石紀錄](../Page/化石.md "wikilink")，故可以推斷出清楚的[演化歷史](../Page/演化.md "wikilink")。\[1\]最早的雷獸，如[始雷獸](../Page/始雷獸.md "wikilink")，較為細小，高度不多於1米，且沒有角。後期的雷獸，縱然仍有一些細少的[物種](../Page/物種.md "wikilink")（如*Nanotitanops*），都演化成巨大的身體，高達2.5米，且有像角的奇異突出物。例如[巨角犀就在鼻上演化了一對很大及](../Page/巨角犀.md "wikilink")[兩性異形的角](../Page/兩性異形.md "wikilink")，這對兩性異形的角顯示雷獸是高度群居的，雄性可能會以角相撞以吸引異性。但是，不像[犀牛](../Page/犀牛.md "wikilink")，雷獸的角是由[額骨及](../Page/額骨.md "wikilink")[鼻骨組成的](../Page/鼻骨.md "wikilink")，且並排排列而非前後排列。一些如*Dolichorhinus*的屬演化了很長的[頭顱骨](../Page/頭顱骨.md "wikilink")。

雷獸可能因不能適應較乾旱的環境及[漸新世普遍的較粗糙植物而](../Page/漸新世.md "wikilink")[滅絕](../Page/滅絕.md "wikilink")。\[2\]

## 分類

雷獸現時有兩個分類系統。第一個包含了43個[屬及](../Page/屬.md "wikilink")8個[亞科](../Page/亞科.md "wikilink")，總括了1920年前的研究。\[3\]第二個是較近期的研究，它指很多過往的亞科都是無效的，並包括了最新的發現。\[4\]\[5\]\[6\][蘭布達獸及](../Page/蘭布達獸.md "wikilink")[異馬獸已不被包含在雷獸科中](../Page/異馬獸.md "wikilink")，但蘭布達獸仍可能是雷獸已知的近親，而異馬獸則被認為是[馬科的早期成員](../Page/馬科.md "wikilink")。

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>1997年分類法</p></th>
<th><p>2004年新分類法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><strong>雷獸科</strong>
<ul>
<li><a href="../Page/巴基雷獸.md" title="wikilink">巴基雷獸</a>（<em>Pakotitanops</em>）：分類不明，來自<a href="../Page/巴基斯坦.md" title="wikilink">巴基斯坦</a>。</li>
<li><em>Nanotitanops</em>：分類不明，來自<a href="../Page/亞洲.md" title="wikilink">亞洲</a>。
<ul>
<li>蘭布達獸亞科（Lambdotheriinae）
<ul>
<li><a href="../Page/蘭布達獸.md" title="wikilink">蘭布達獸</a>（<em>Lambdotherium</em>）：來自<a href="../Page/北美洲.md" title="wikilink">北美洲</a>。</li>
<li><a href="../Page/異馬獸.md" title="wikilink">異馬獸</a>（<em>Xenicohippus</em>）：來自北美洲。</li>
</ul></li>
<li>古雷獸亞科（Palaeosyopinae）
<ul>
<li><a href="../Page/古雷獸.md" title="wikilink">古雷獸</a>（<em>Palaeosyops</em>）：包括<a href="../Page/始雷獸.md" title="wikilink">始雷獸</a>，來自北美洲，高0.5米。</li>
<li><em>Mulkrajanops</em>：來自巴基斯坦，高1.25米。</li>
</ul></li>
<li><a href="../Page/長鼻雷獸亞科.md" title="wikilink">長鼻雷獸亞科</a>（Dolichorhininae）
<ul>
<li><a href="../Page/後鼻雷獸.md" title="wikilink">後鼻雷獸</a>（<em>Metarhinus</em>）：來自北美洲，高1米。</li>
<li><a href="../Page/楔形泰坦兽属.md" title="wikilink">楔形泰坦兽属</a>（<em>Sphenocoelus</em>）：來自北美洲，高1.25米。</li>
<li><a href="../Page/中鼻雷兽属.md" title="wikilink">中鼻雷兽属</a>（<em>Mesatirhinus</em>）：來自北美洲，高1米。</li>
</ul></li>
<li>雷獸亞科（Brontotheriinae）
<ul>
<li><a href="../Page/爵婦新獸屬.md" title="wikilink">爵婦新獸屬</a>（<em>Duchesneodus</em>）：來自北美洲。</li>
<li><a href="../Page/王雷獸.md" title="wikilink">王雷獸</a>（<em>Brontotherium</em>）：來自北美洲。</li>
<li><a href="../Page/巨角犀.md" title="wikilink">巨角犀</a>（<em>Megacerops</em>）：來自北美洲，高2.5米。</li>
</ul></li>
<li><a href="../Page/大角雷獸亞科.md" title="wikilink">大角雷獸亞科</a>（Embolotheriinae）
<ul>
<li><a href="../Page/泰坦齿雷兽.md" title="wikilink">泰坦齿雷兽</a>（<em>Titanodectes</em>）：來自亞洲。</li>
<li><a href="../Page/大角雷獸.md" title="wikilink">大角雷獸</a>（<em>Embolotherium</em>）：來自<a href="../Page/蒙古.md" title="wikilink">蒙古</a>，高2.5米。</li>
<li><a href="../Page/原大角雷獸.md" title="wikilink">原大角雷獸</a>（<em>Protembolotherium</em>）：來自<a href="../Page/外蒙古.md" title="wikilink">外蒙古</a>，高2米。</li>
</ul></li>
<li>漸雷獸亞科（Brontopinae）
<ul>
<li><a href="../Page/短齿雷兽.md" title="wikilink">短齿雷兽</a>（<em>Brachydiastematherium</em>）：來自<a href="../Page/東歐.md" title="wikilink">東歐</a>，高2米。</li>
<li><a href="../Page/腫雷獸屬.md" title="wikilink">腫雷獸屬</a>（<em>Pachytitan</em>）：來自蒙古，高2米。</li>
<li><a href="../Page/滇雷獸.md" title="wikilink">滇雷獸</a>（<em>Dianotitan</em>）：來自<a href="../Page/中國.md" title="wikilink">中國</a>，高2米。</li>
<li><a href="../Page/巨頜雷獸屬.md" title="wikilink">巨頜雷獸屬</a>（<em>Gnathotitan</em>）：來自內蒙古，高2.5米。</li>
<li><a href="../Page/小雷獸屬.md" title="wikilink">小雷獸屬</a>（<em>Microtitan</em>）：來自內蒙古，高0.75米。</li>
<li><a href="../Page/晚叉額雷獸.md" title="wikilink">晚叉額雷獸</a>（<em>Epimanteoceras</em>）：來自內蒙古，高2米。</li>
<li><a href="../Page/原雷獸.md" title="wikilink">原雷獸</a>（<em>Protitan</em>）：來自內蒙古，高2米。</li>
<li><a href="../Page/鼻雷獸.md" title="wikilink">鼻雷獸</a>（<em>Rhinotitan</em>）：來自內蒙古，高2.5米。</li>
<li><a href="../Page/晚雷獸屬.md" title="wikilink">晚雷獸屬</a>（<em>Metatitan</em>）：來自蒙古，高1.5米。</li>
<li><a href="../Page/原巨雷獸.md" title="wikilink">原巨雷獸</a>（<em>Protitanotherium</em>）：來自北美洲，高2米。</li>
<li><a href="../Page/副雷獸屬.md" title="wikilink">副雷獸屬</a>（<em>Parabrontops</em>）：來自蒙古，高2米。</li>
<li><em>Oreinotherium</em>：來自北美洲。</li>
<li><a href="../Page/漸雷獸.md" title="wikilink">漸雷獸</a>（<em>Brontops</em>）：來自北美洲。</li>
<li><a href="../Page/原雷兽.md" title="wikilink">原雷兽</a>（<em>Protitanops</em>）：來自北美洲，高2米。</li>
<li><a href="../Page/侏儒雷獸.md" title="wikilink">侏儒雷獸</a>（<em>Pygmaetitan</em>）：來自中國，高0.5米。</li>
</ul></li>
<li>沼雷獸亞科（Telmatheriinae）
<ul>
<li><a href="../Page/尖雷獸.md" title="wikilink">尖雷獸</a>（<em>Acrotitan</em>）：來自內蒙古，高0.3米。</li>
<li><a href="../Page/索齿雷兽.md" title="wikilink">索齿雷兽</a>（<em>Desmatotitan</em>）：來自內蒙古，1.25米高。</li>
<li><a href="../Page/熊雷獸.md" title="wikilink">熊雷獸</a>（<em>Arctotitan</em>）：來自中國。</li>
<li><a href="../Page/豬形雷獸屬.md" title="wikilink">豬形雷獸屬</a>（<em>Hyotitan</em>）：來自內蒙古，高2.2米。</li>
<li><em>Sthenodectes</em>：來自北美洲，高1.25米。</li>
<li><a href="../Page/沼雷獸.md" title="wikilink">沼雷獸</a>（<em>Telmatherium</em>）：包括<a href="../Page/後沼雷獸.md" title="wikilink">後沼雷獸</a>（<em>Metatelmatherium</em>），來自北美洲及內蒙古，高1.5米。</li>
<li><a href="../Page/西瓦雷兽.md" title="wikilink">西瓦雷兽</a>（<em>Sivatitanops</em>）：來自亞洲及<a href="../Page/歐洲.md" title="wikilink">歐洲</a>。</li>
</ul></li>
<li>Menodontinae亞科
<ul>
<li><em>Diplacodon</em>：來自北美洲，高2米。</li>
<li><a href="../Page/始泰坦雷兽.md" title="wikilink">始泰坦雷兽</a>（<em>Eotitanotherium</em>）：來自北美洲。</li>
<li><a href="../Page/南方雷兽.md" title="wikilink">南方雷兽</a>（<em>Notiotitanops</em>）：來自北美洲，高2米。</li>
<li><a href="../Page/月雷獸屬.md" title="wikilink">月雷獸屬</a>（<em>Menodus</em>）：來自歐洲及北美洲。</li>
<li><em>Ateleodon</em>：來自北美洲。</li>
</ul></li>
</ul></li>
</ul></li>
</ul></td>
<td><ul>
<li><strong>雷獸科</strong>
<ul>
<li><a href="../Page/巴基雷獸.md" title="wikilink">巴基雷獸</a>（<em>Pakotitanops</em>）：分類不明。</li>
<li><em>Mulkrajanops</em>：分類不明。</li>
<li><a href="../Page/始雷獸.md" title="wikilink">始雷獸</a>（<em>Eotitanops</em>）：來自<a href="../Page/北美洲.md" title="wikilink">北美洲</a>，高0.5米。</li>
<li><a href="../Page/古雷獸.md" title="wikilink">古雷獸</a>（<em>Palaeosyops</em>）：來自北美洲，高1米。</li>
<li>雷獸亞科（Brontotheriinae）
<ul>
<li><a href="../Page/丘齿雷兽.md" title="wikilink">丘齿雷兽</a>（<em>Bunobrontops</em>）：來自<a href="../Page/亞洲.md" title="wikilink">亞洲</a>。</li>
<li><a href="../Page/中鼻雷兽属.md" title="wikilink">中鼻雷兽属</a>（<em>Mesatirhinus</em>）：來自北美洲，高1米。</li>
<li><a href="../Page/長鼻雷獸屬.md" title="wikilink">長鼻雷獸屬</a>（<em>Dolichorhinus</em>）：來自北美洲，高1.25米。</li>
<li><a href="../Page/楔形泰坦兽属.md" title="wikilink">楔形泰坦兽属</a>（<em>Sphenocoelus</em>）：來自北美洲，高1.25米。</li>
<li><a href="../Page/索齿雷兽.md" title="wikilink">索齿雷兽</a>（<em>Desmatotitan</em>）：來自內蒙古，高1.25米。</li>
<li><em>Fossendorhinus</em>：來自北美洲。</li>
<li><a href="../Page/後鼻雷獸.md" title="wikilink">後鼻雷獸</a>（<em>Metarhinus</em>）：來自北美洲，高1米。</li>
<li><a href="../Page/小雷獸屬.md" title="wikilink">小雷獸屬</a>（<em>Microtitan</em>）：來自內蒙古，高0.75米。</li>
<li><em>Sthenodectes</em>：來自北美洲，高1.25米。</li>
<li><a href="../Page/沼雷獸.md" title="wikilink">沼雷獸</a>（<em>Telmatherium</em>）：來自北美洲，高1.25米。</li>
<li><a href="../Page/後沼雷獸.md" title="wikilink">後沼雷獸</a>（<em>Metatelmatherium</em>）：來自北美洲及內蒙古，高1.25米。</li>
<li><a href="../Page/晚叉額雷獸.md" title="wikilink">晚叉額雷獸</a>（<em>Epimanteoceras</em>）：來自內蒙古，高2米。</li>
<li><a href="../Page/豬形雷獸屬.md" title="wikilink">豬形雷獸屬</a>（<em>Hyotitan</em>）：分類不明，來自內蒙古，高2.2米。</li>
<li><em>Nanotitanops</em>：分類不明：來自亞洲。</li>
<li><a href="../Page/侏儒雷獸.md" title="wikilink">侏儒雷獸</a>（<em>Pygmaetitan</em>）：分類不明，來自中國，高0.5米。</li>
<li><a href="../Page/尖雷獸.md" title="wikilink">尖雷獸</a>（<em>Acrotitan</em>）：分類不明，來自內蒙古，高0.3米。</li>
<li><a href="../Page/熊雷獸.md" title="wikilink">熊雷獸</a>（<em>Arctotitan</em>）：分類不明，來自中國。</li>
<li><a href="../Page/曲阜雷獸.md" title="wikilink">曲阜雷獸</a>（<em>Qufutitan</em>）：分類不明，來自中國。</li>
<li>雷獸族（Brontotheriini）
<ul>
<li><a href="../Page/原雷獸.md" title="wikilink">原雷獸</a>（<em>Protitan</em>）：來自內蒙古，高2米。</li>
<li><a href="../Page/原巨雷獸.md" title="wikilink">原巨雷獸</a>（<em>Protitanotherium</em>）：來自北美洲，高2米。</li>
<li><a href="../Page/鼻雷獸.md" title="wikilink">鼻雷獸</a>（<em>Rhinotitan</em>）：來自內蒙古，高2.5米。</li>
<li><em>Diplacodon</em>：包括<em>Eotitanotherium</em>，來自北美洲，高2米。</li>
<li><a href="../Page/腫雷獸屬.md" title="wikilink">腫雷獸屬</a>（<em>Pachytitan</em>）：來自內蒙古，高2米。</li>
<li><a href="../Page/短齿雷兽.md" title="wikilink">短齿雷兽</a>（<em>Brachydiastematherium</em>）：來自<a href="../Page/東歐.md" title="wikilink">東歐</a>，高2米。</li>
<li><a href="../Page/西瓦雷兽.md" title="wikilink">西瓦雷兽</a>（<em>Sivatitanops</em>）；來自亞洲及歐洲。</li>
<li>大角雷獸亞族（Embolotheriina）
<ul>
<li><a href="../Page/巨頜雷獸.md" title="wikilink">巨頜雷獸</a>（<em>Gnathotitan</em>）：來自內蒙古，高2.5米。</li>
<li><em>Aktautitan</em>：來自<a href="../Page/哈薩克斯坦.md" title="wikilink">哈薩克斯坦</a>，高2.5米。</li>
<li><a href="../Page/晚雷獸屬.md" title="wikilink">晚雷獸屬</a>（<em>Metatitan</em>）：來自蒙古，高1.5米。</li>
<li><em>Nasamplus</em>：來自內蒙古。</li>
<li><a href="../Page/原大角雷獸.md" title="wikilink">原大角雷獸</a>（<em>Protembolotherium</em>）：來自外蒙古，高2米。</li>
<li><a href="../Page/大角雷獸.md" title="wikilink">大角雷獸</a>（<em>Embolotherium</em>）：包括<em>Titanodectes</em>，來自蒙古，高2.5米。</li>
</ul></li>
<li>雷獸亞族（Brontotheriina）
<ul>
<li><a href="../Page/副雷獸屬.md" title="wikilink">副雷獸屬</a>（<em>Parabrontops</em>）：來自蒙古，高2米。</li>
<li><a href="../Page/原雷兽.md" title="wikilink">原雷兽</a>（<em>Protitanops</em>）：來自北美洲，高2米。</li>
<li><a href="../Page/南方雷兽.md" title="wikilink">南方雷兽</a>（<em>Notiotitanops</em>）：來自北美洲，高2米。</li>
<li><a href="../Page/滇雷獸.md" title="wikilink">滇雷獸</a>（<em>Dianotitan</em>）：來自中國，高2米。</li>
<li><a href="../Page/爵婦新獸屬.md" title="wikilink">爵婦新獸屬</a>（<em>Duchesneodus</em>）：來自北美洲。</li>
<li><a href="../Page/巨角犀.md" title="wikilink">巨角犀</a>（<em>Megacerops</em>）：包括<a href="../Page/月雷獸屬.md" title="wikilink">月雷獸屬</a>、<a href="../Page/王雷獸.md" title="wikilink">王雷獸</a>、<a href="../Page/漸雷獸.md" title="wikilink">漸雷獸</a>、<em>Menops</em>、<em>Ateleodon</em>及<em>Oreinotherium</em>，來自北美洲，高2.5米。</li>
</ul></li>
</ul></li>
</ul></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [雷獸超科](https://web.archive.org/web/20071209110538/http://www.fmnh.helsinki.fi/users/haaramo/metazoa/Deuterostoma/chordata/Synapsida/Eutheria/Perissodactyla/Brontotheroidea.htm)

## 參考

[Category:奇蹄目](../Category/奇蹄目.md "wikilink")
[Category:雷獸](../Category/雷獸.md "wikilink")

1.

2.
3.

4.

5.

6.