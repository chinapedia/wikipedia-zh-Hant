**雷蒙德·-{zh-tw:伯納;zh-cn:伯纳德;zh-hk:巴納特;}-·卡特尔** （**Raymond Bernard
Cattell**，），是一名[英国和](../Page/英国.md "wikilink")[美国](../Page/美国.md "wikilink")[心理学家](../Page/心理学家.md "wikilink")，创立[晶体智力和](../Page/晶体智力.md "wikilink")[流体智力理论解释人类认知能力](../Page/流体智力.md "wikilink")。在卡特尔92年的人生中，以多产著称，最终发表了55本著作，约500篇论文，以及30项标准测验。

卡特尔致力于使用严格的科学方法，他很早就建议将要素分析方法应用于心理学，以取代他所称的“口头理论”。卡特尔在应用要素分析方面最重要的成就之一是分析出人类[个性的](../Page/个性.md "wikilink")16种根源特质。他相信，通常人们所理解的个性，只是可以观察到的外显[行为](../Page/行为.md "wikilink")，属于表面特质；它们由16种根源特质所决定。
而深源特質分為三類，分別為能力特質、氣質特質、動力特質。能力特質與使個人運作有效率的技巧與能力有關；氣質特質則與個人的情緒生活及其行為特性有關；動力特質個人的奮鬥力量、動機、自認重要的追求目標有關。

## 创新与成就

  - 流体智力与晶体智力

1997年，美国心理学会(APA)为92岁的卡特尔颁发“心理科学终生成就金质奖章”。不过，在奖章颁发之前，一名伊利诺斯大学的校友[巴里·梅赫勒](../Page/巴里·梅赫勒.md "wikilink")，通过他的非营利基金会[ISAR公开发起挑战](../Page/ISAR.md "wikilink")，反对卡特尔
[1](http://www.ferris.edu/isar/bios/Cattell/gold.htm)，控告卡特尔同情种族主义和法西斯主义
[2](http://www.ferris.edu/isar/bios/Cattell/homepage.htm)

## 参考文献

1.  Mehler reports that he was mentored by Jerry Hirsch, a colleague and
    strong critic of Cattell at the University of Illinois, where
    Cattell and Hirsch spent the majority of their careers. Cattell was
    also criticized by Rutgers professor [William H. "Bill"
    Tucker](../Page/William_H._Tucker.md "wikilink"), a friend and
    associate of Mehler's to whom Mehler "generously opened both his
    files and his home". In Tucker's book published with University of
    Illinois Press
    [3](https://web.archive.org/web/20061210102807/http://www.press.uillinois.edu/epub/books/tucker/acknow.html),
    Tucker claims that Cattell (in 1937) praised the eugenics laws of
    the pre-war Third Reich for promoting racial improvement.

</div>

## 著作

## 论文

## 外部链接

  - [A Memorial to Raymond Bernard
    Cattell](http://www.cattell.net/devon/rbcmain.htm)
  - [Human Intelligence: Raymond B.
    Cattell](http://www.indiana.edu/~intell/rcattell.shtml)
  - [The Psi Cafe: Raymond
    Cattell](https://web.archive.org/web/20070210024118/http://www.psy.pdx.edu/PsiCafe/KeyTheorists/Cattell.htm)
  - [John Gillis's official biography of Raymond B.
    Cattell](https://web.archive.org/web/20050328192418/http://www.stu.ca/~jgillis/cattell.html)
  - [A Concise Beyondist
    Catechism](https://web.archive.org/web/20070301044037/http://www.lrainc.com/swtaboo/taboos/beyond01.html)
  - [Interview With Raymond B.
    Cattell](http://www.eugenics.net/papers/eb7.html) from *The Eugenics
    Bulletin*, Spring-Summer 1984.
  - [Raymond B. Cattell and The Fourth
    Inquisition](http://www.eugenics.net/papers/Inq4.html)

[C](../Category/1905年出生.md "wikilink")
[C](../Category/1998年逝世.md "wikilink")
[C](../Category/美国心理学家.md "wikilink")
[C](../Category/英国心理学家.md "wikilink")
[C](../Category/倫敦國王學院校友.md "wikilink")
[Category:智力](../Category/智力.md "wikilink")
[Category:心理测验学](../Category/心理测验学.md "wikilink")