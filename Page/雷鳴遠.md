**雷鳴遠**（），原名**文森特·勒貝**（），字**振聲**，洗名文森，本籍[比利時](../Page/比利時.md "wikilink")，[天主教](../Page/天主教.md "wikilink")[遣使会](../Page/遣使会.md "wikilink")[神父](../Page/神父.md "wikilink")（1933年脱离），也是两个中国修会[耀汉小兄弟会和](../Page/耀汉小兄弟会.md "wikilink")[德来小姊妹会的创始人](../Page/德来小姊妹会.md "wikilink")。他於1927年加入[中国国籍](../Page/中華民國大陸時期.md "wikilink")，於[抗日战争初期](../Page/抗日战争.md "wikilink")，組織救濟團隊，救治中國各地平民。

## 生平

1877年8月19日，雷鸣远生于[比利时](../Page/比利时.md "wikilink")[根特城一个虔诚的天主教家庭](../Page/根特.md "wikilink")，父亲是[佛蘭芒人](../Page/佛蘭芒人.md "wikilink")，担任[公证人](../Page/公证人.md "wikilink")，母亲是有一半法国血统的英国人。他是家中长子，洗名文森（Vincent）。11岁时阅读1840年在中國武昌殉教的[董文学神父传记后立志前往中国传教](../Page/董文学.md "wikilink")。1895年在[巴黎加入](../Page/巴黎.md "wikilink")[董文学神父所属的](../Page/董文学.md "wikilink")[遣使會](../Page/遣使會.md "wikilink")。於[神學院就讀期间](../Page/神學院.md "wikilink")，1900年中国发生[义和团运动](../Page/义和团运动.md "wikilink")，比利时传教士[韓默理主教在内蒙古殉教](../Page/韓默理.md "wikilink")，更促成雷鸣远決定前往中國傳教。

1901年，雷鸣远随北京教区的[樊国梁主教乘船来华](../Page/樊国梁.md "wikilink")。1902年在北京成为[神父](../Page/神父.md "wikilink")，随后被派往[武清县小韩村等地传教](../Page/武清.md "wikilink")。1903年，雷鸣远主持重建在义和团运动中被损毁的[小韩村教堂](../Page/小韩村教堂.md "wikilink")。

雷鸣远到達中國傳教後，他努力學習中國文化，讀中國書，能说一口流利标准的[国语](../Page/国语.md "wikilink")，用毛筆写漂亮的[行书](../Page/行书.md "wikilink")。1912年，从[北京教区分出](../Page/北京教区.md "wikilink")[天津教区](../Page/天津教区.md "wikilink")，传教成绩奇佳的雷鸣远便升任这个新教区的[副主教](../Page/副主教.md "wikilink")。

1915年10月10日，雷鸣远和中国天主教徒在天津租界以外的南市荣业大街创办《[益世報](../Page/益世報.md "wikilink")》\[1\]。

雷鸣远批评由外国各个修会代表本国利益控制中国天主教的作法，提出“中国归中国人，中国人归基督”的口号，积极推动教廷任命中国籍主教。为实现这个梦想，他受到[遣使會的排挤](../Page/遣使會.md "wikilink")。

1916年天津发生[老西開事件](../Page/老西開事件.md "wikilink")，雷鸣远大力支持天津市民反对法国人扩展[天津法租界](../Page/天津法租界.md "wikilink")，将[圣若瑟主教座堂及附近地区并入该租界的行动](../Page/圣若瑟主教座堂_\(天津\).md "wikilink")，在《益世报》发表大量反对法租界扩张的文章，与支持法租界扩张的法國籍天津教区主教[杜保禄发生冲突](../Page/杜保禄.md "wikilink")，于1917年被遣使会会长罗得芳降职调往浙江省[宁波教区](../Page/宁波教区.md "wikilink")，1920年4月由传信部特使光主教安排，送回欧洲。

在欧洲期间，雷鸣远从事中国留学生的传教工作。1923年，他在巴黎为他们建立"Associatio Catholica iuventutis
Sinensis"。

雷鸣远继续努力，向教廷上万言书，后来得以面见教宗陈情\[2\]，举荐6位中国主教的人选\[（[海门教区](../Page/海门教区.md "wikilink")[朱开敏](../Page/朱开敏.md "wikilink")、[蒲圻教区](../Page/蒲圻教区.md "wikilink")[成和德](../Page/成和德.md "wikilink")、[汾阳教区](../Page/汾阳教区.md "wikilink")[陈国砥](../Page/陈国砥.md "wikilink")、[宣化教区](../Page/宣化教区.md "wikilink")[赵怀义](../Page/赵怀义.md "wikilink")、[台州教区](../Page/台州教区.md "wikilink")[胡若山](../Page/胡若山.md "wikilink")、[安国教区](../Page/安国教区.md "wikilink")[孙德桢](../Page/孙德桢.md "wikilink")）\[3\]。终于在1926年10月28日，首批六位中国主教在[罗马](../Page/罗马.md "wikilink")[圣伯多禄大殿由教宗](../Page/圣伯多禄大殿.md "wikilink")[庇护十一世亲自](../Page/庇护十一世.md "wikilink")[祝圣](../Page/祝圣.md "wikilink")。在参加祝圣典礼时，雷鸣远喜极而泣达两小时之久，说：“主啊！现在可以放你的仆人平安而去。”

1927年，雷鸣远申請中國國籍獲准，始被法國放行來華。在[河北省中国籍主教](../Page/河北省.md "wikilink")[孙德桢领导的](../Page/孙德桢.md "wikilink")[安国教区更积极地传教](../Page/安国教区.md "wikilink")，并创立[耀汉小兄弟会](../Page/耀汉小兄弟会.md "wikilink")（1928年）和[德来小姊妹会](../Page/德来小姊妹会.md "wikilink")。雷神父于1933年7月脱离[遣使会](../Page/遣使会.md "wikilink")，翌年加入自己创立的[耀汉小兄弟会](../Page/耀汉小兄弟会.md "wikilink")，并发圣愿。

雷鸣远站在中国人的立场，极力主张抗日。1933年热河[长城抗战](../Page/长城抗战.md "wikilink")，他带领教徒前去抢救伤兵；1937年[抗日战争全面爆发以后](../Page/抗日战争.md "wikilink")，雷鸣远神父率领教友共六百余人，组织战地服务团、救护队，在[太行山和](../Page/太行山.md "wikilink")[中条山一带抢救伤兵](../Page/中条山.md "wikilink")，救济难民，教育失学儿童。

1940年，雷鸣远服务的政府军[鹿钟麟部与](../Page/鹿钟麟.md "wikilink")[八路军发生冲突](../Page/八路军.md "wikilink")，3月9日，雷鸣远被八路军俘虏，关押在[太行山山区的山西省辽县](../Page/太行山.md "wikilink")（今[左权县](../Page/左权县.md "wikilink")），被当作国民党间谍受到6个星期的洗脑和酷刑，40多天后，经[国民政府交涉](../Page/国民政府.md "wikilink")，雷鸣远被释放，但已罹患重病\[4\]。

在到达[重庆后不久](../Page/重庆.md "wikilink")，1940年6月24日，雷鸣远因[黄疸病逝世于](../Page/黄疸病.md "wikilink")[歌樂山](../Page/歌樂山.md "wikilink")\[5\]。同年7月18日，[中華民國](../Page/中華民國.md "wikilink")[國民政府以](../Page/國民政府.md "wikilink")1287號[褒揚令公開褒揚](../Page/褒揚令.md "wikilink")。11月29日，重慶市召开追悼雷鳴遠神父大會。[台北的](../Page/臺北市.md "wikilink")[國民革命忠烈祠亦有設他的靈位](../Page/國民革命忠烈祠.md "wikilink")。\[6\]

[雷震遠](../Page/雷震遠.md "wikilink")、雷鳴遠的著作《內在的敵人》，該書揭露[八路軍於](../Page/八路軍.md "wikilink")1940年[皖南事變前突襲國軍三個軍共](../Page/皖南事變.md "wikilink")6萬多人，卻沒有動不足50英里外的日軍\[7\]。

## 格言

  - “全牺牲、真爱人、常喜乐。”
  - “中国归中国人，中国人归基督。”
  - “我为爱中国而生，我为爱中国而死。”
  - “有一百分力量不出九十九分。”

## 参考文献

## 外部链接

  - [vincentlebbe.net](http://www.vincentlebbe.net)
  - [耀汉小兄弟会简史](https://web.archive.org/web/20120107081152/http://www.radiovaticana.org/cinesegb/notiziari/notiz2004/notiz04-10/not10-04.htm)
  - [雷鳴遠神父紀錄片1990](http://www.tudou.com/programs/view/KrxSf0ZaWJ4)
  - [天主教鳴遠中學](http://www.mingyuen.edu.hk)
  - [鳴遠校友會](http://www.hkmyms.net)

## 参见

  - [张刚毅](../Page/张刚毅.md "wikilink")
  - [衛道中學](../Page/衛道中學.md "wikilink")

{{-}}

[Category:在中华民国的天主教传教士
(1949年前)](../Category/在中华民国的天主教传教士_\(1949年前\).md "wikilink")
[Category:比利时天主教传教士](../Category/比利时天主教传教士.md "wikilink")
[Category:歸化中華民國公民](../Category/歸化中華民國公民.md "wikilink")
[Category:根特人](../Category/根特人.md "wikilink")
[雷](../Category/臺中市私立衛道高級中學人物.md "wikilink")

1.  [旧南市的火炬](http://www.catholic.tj.cn/www/20/2007-10/1984.html)
2.
3.
4.  曹立珊：雷鳴遠神父生平簡介
5.  {{ cite journal | last = Bays | first = Daniel | title = From
    Foreign Mission to Chinese Church | journal = Christian History &
    Biography | issue = 98 | pages = 9–10 |date=Spring 2008}}
6.  中華殉道先烈傳，劉宇聲主編
7.