**電擊G's magazine**是由[ASCII Media
Works](../Page/ASCII_Media_Works.md "wikilink")（前[MediaWorks](../Page/MediaWorks.md "wikilink")）從1992年起發行的月刊[遊戲雜誌](../Page/遊戲雜誌.md "wikilink")。毎月30日發售。

該雜誌主要針對[家用遊戲機的](../Page/家用遊戲機.md "wikilink")[美少女遊戲軟件](../Page/美少女遊戲.md "wikilink")。另外它還推行近年常常所說的讀者參加雜誌上連載獨創規劃，並將其進行[遊戲軟體化](../Page/遊戲軟體.md "wikilink")、[動畫化的](../Page/動畫.md "wikilink")[跨媒體製作](../Page/跨媒體製作.md "wikilink")。

## 沿革

  - 1992年12月26日，MediaWorks發行《電擊PC Engine》（）創刊號（1993年2月號）。
  - 1996年4月30日，《電擊PC Engine》改名為《電擊G's Engine》（）並發行1996年6月號。
  - 1997年6月30日，《電擊G's Engine》改名為《電擊G's雜誌》（）並發行1997年8月號。
  - 2002年3月30日，《電擊G's雜誌》改名為《電擊G's magazine》（）並發行2002年5月號。
  - 2008年4月30日，《電擊G's magazine》發行2008年6月號，發行公司名稱改為ASCII Media Works。
  - 2009年3月2日，《電擊G's
    magazine》的[行動上網專用網站](../Page/行動上網.md "wikilink")《電擊G's
    mobile》（）開站。

## 本誌連載的主要讀者參加計劃

此處不包括由其他公司開發的作品的Fan Page等。

  - [女神天國](../Page/女神天國.md "wikilink")（畫：[好實昭博](../Page/好實昭博.md "wikilink")・[宮須彌](../Page/宮須彌.md "wikilink")、文：きくたけ（菊池たけし）、1993年2月號～1996年6月號）遊戲・OVA
      - 1999年9月號續編「聖女神天國」の予告が掲載されたが、連載に至らず2000年1月号にお詫びが掲載された。
  - [ヴァルツァーの紋章](../Page/ヴァルツァーの紋章.md "wikilink")（原案：安藤真、文：[新木伸](../Page/新木伸.md "wikilink")、畫：[小林瑞代](../Page/小林瑞代.md "wikilink")・[佐島真実](../Page/佐島真実.md "wikilink")、1993年2月號～1995年1月號）小說
  - ハイパーウォーズ（原作：阿須貞明、畫：長谷川努、1993年12月號～1994年10月號）遊戲
  - ルートランサー（原案：林田ゆり、画：十羽織羽磨、1994年1月号～11月号）
  - [六翼天使之聲](../Page/六翼天使之聲.md "wikilink")（畫：[七瀬葵](../Page/七瀬葵.md "wikilink")、1996年4月號～1998年8月號）TV動畫

<!-- end list -->

  -
    連載完結後、因為決定動畫化而在1999年3月號～2000年2月號的封面登場。

<!-- end list -->

  - [お嬢様特急（エクスプレス）](../Page/お嬢様特急.md "wikilink")（企画：[赤堀悟](../Page/赤堀悟.md "wikilink")・画：[柳沢まさひで](../Page/柳沢まさひで.md "wikilink")、1998年5月號～11月號）遊戲
  - [妹妹公主](../Page/妹妹公主.md "wikilink")（画：[天廣直人](../Page/天廣直人.md "wikilink")、文：[公野櫻子](../Page/公野櫻子.md "wikilink")、1999年3月號～2003年9月號）遊戲・TV動畫・等多種模式的作品

<!-- end list -->

  -
    2000年3月～2003年9月號封面。

<!-- end list -->

  - [歡樂課程](../Page/歡樂課程.md "wikilink")/HAPPY
    LESSON（原作・畫：[佐佐木睦美](../Page/佐佐木睦美.md "wikilink")（但是事實上擔任寫文的是鷹野よしき（高野希義現編集長））、1999年4月号～2002年9月号）遊戲・OVA・TV動畫・漫畫・小說
  - [Milky
    Season](../Page/Milky_Season.md "wikilink")（畫：森藤卓弥、2000年3月號～2001年10月號）遊戲
  - Merry Little Park（畫：ひさかた悠、2001年4月號～12月號）
  - [雙戀](../Page/雙戀.md "wikilink")（原作：雙葉雛、畫：佐佐木睦美、2002年10月號～2005年10月號）TV動畫・遊戲・漫畫

<!-- end list -->

  -
    [歡樂課程連載完結後立即開始的企劃](../Page/歡樂課程.md "wikilink")。2003年10月號～2005年10月號封面。

<!-- end list -->

  - [草莓危機](../Page/草莓危機.md "wikilink")（原作：公野櫻子、画：真木ちとせ（2003年11月號～2005年9月號）/たくみなむち（2005年10月號～）、2003年11月號～）網上電台・漫畫・小說・動畫
  - [ウルトラC（チャーミング）\!](../Page/ウルトラC（チャーミング）!.md "wikilink")（畫：大崎はるい、企画・文：かのまるひ、2003年12月號～2004年8月號）
  - マリッジロワイヤル（原作・畫：[西又葵](../Page/西又葵.md "wikilink")・[鈴平ひろ](../Page/鈴平ひろ.md "wikilink")、2006年1月號～2011年5月號）

<!-- end list -->

  -
    此作是與[Navel合作的合同企画](../Page/Navel.md "wikilink")。

<!-- end list -->

  - [2/3
    アイノキョウカイセン](../Page/2/3_アイノキョウカイセン.md "wikilink")（畫：[京極しん](../Page/京極しん.md "wikilink")・[小川静香](../Page/小川静香.md "wikilink")、2006年1月號～
    2007年12月號）

<!-- end list -->

  -
    連載開始前の仮題は「Real/Digital Project」。

<!-- end list -->

  - [A.I. Love
    You\!](../Page/A.I._Love_You!.md "wikilink")（畫：高階@聖人、2006年2月號～2009年11月號）

<!-- end list -->

  -
    読者コーナー「MOESTA」内の1コーナーであるため、現時点では目次に掲載されていない。

<!-- end list -->

  - [公主導航](../Page/公主導航.md "wikilink")（畫：[七尾奈留](../Page/七尾奈留.md "wikilink")、文：[SATZ](../Page/SATZ.md "wikilink")）：2007年10月號
    - 2011年5月號
  - [寶貝公主](../Page/寶貝公主.md "wikilink")（原作：公野櫻子、畫：[みぶなつき](../Page/みぶなつき.md "wikilink")）：2007年12月號
    - 2012年6月號
  - [Love
    Live\!](../Page/Love_Live!.md "wikilink")（原作：[矢立肇](../Page/矢立肇.md "wikilink")、原案：公野櫻子）：2010年7月號
    -

## 增刊

### 電擊G's Festival\!

2004年12月開始的不定期增刊。

  - Vol.1 2004年12月16日發售

<!-- end list -->

  - [To Heart 2](../Page/To_Heart_2.md "wikilink")（封面）68頁大特集。
  - 附錄：「To Heart 2」A4判磁鐵海報・樸克牌(連盒子)．2005年日程簿

<!-- end list -->

  - Vol.2 2005年9月15日發售

<!-- end list -->

  - [夜明前的琉璃色](../Page/夜明前的琉璃色.md "wikilink")（封面）・[Fate/hollow
    ataraxia特集](../Page/Fate/hollow_ataraxia.md "wikilink")。
  - 附錄：「Fate/hollow ataraxia」雙面枕頭套・「夜明前的琉璃色」拼圖・手提電話吊帶（圖案: 間桐櫻・鷹見澤菜月）

<!-- end list -->

  - Vol.3 2005年12月2日發售<font color="red">（未滿18歳人士禁止購買）</font>

<!-- end list -->

  - [To Heart 2 XRATED](../Page/To_Heart_2.md "wikilink")（封面）1冊大特集。
  - 附錄：

<!-- end list -->

  - Vol.4 2006年5月11日發售

<!-- end list -->

  - [初音岛II](../Page/初音岛II.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.5 2006年5月25日發售

<!-- end list -->

  - [娇蛮之吻](../Page/娇蛮之吻.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.6 2006年11月30日發售

<!-- end list -->

  - [夜明前的琉璃色](../Page/夜明前的琉璃色.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.7 2007年2月16日發售

<!-- end list -->

  - [暮蟬悲鳴時](../Page/暮蟬悲鳴時.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.8 2007年3月29日發售

<!-- end list -->

  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.9 2007年6月30日發售

<!-- end list -->

  - [Little Busters\!](../Page/Little_Busters!.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.10 2007年8月30日發售

<!-- end list -->

  - [七色★星露 Pure\!\!](../Page/七色★星露.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.11 2008年1月19日發售

<!-- end list -->

  - [FORTUNE ARTERIAL](../Page/FORTUNE_ARTERIAL.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.12 2008年5月20日發售

<!-- end list -->

  - [初音島II](../Page/初音島II.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.13 2008年12月19日發售

<!-- end list -->

  - [恋姬†无双](../Page/恋姬†无双.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.14 2009年1月23日發售

<!-- end list -->

  - [我们没有翅膀](../Page/我们没有翅膀.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.15 2009年2月20日發售

<!-- end list -->

  - [夜明前的琉璃色](../Page/夜明前的琉璃色.md "wikilink")（封面）大特集。
  - 附錄：

<!-- end list -->

  - Vol.16 2010年4月14日發售

<!-- end list -->

  - （封面）大特集。

<!-- end list -->

  - Vol.17 2010年7月28日發售

<!-- end list -->

  - [Angel Beats\!](../Page/Angel_Beats!.md "wikilink")（封面）大特集。
  - 附錄：

## 關連項目

  - [電擊文庫MAGAZINE](../Page/電擊文庫MAGAZINE.md "wikilink")
  - [コンプティーク](../Page/コンプティーク.md "wikilink")
  - [Virtual IDOL](../Page/Virtual_IDOL.md "wikilink")
  - [DearMy...](../Page/DearMy....md "wikilink")
  - [RASPBERRY](../Page/RASPBERRY.md "wikilink")

## 外部連結

  - [電撃G's 雜誌](http://gs.dengeki.com/)（官方網頁）

[電擊G's雜誌](../Page/category:電擊G's雜誌.md "wikilink")

[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[Category:動畫雜誌](../Category/動畫雜誌.md "wikilink")
[Category:遊戲雜誌](../Category/遊戲雜誌.md "wikilink")
[Category:1992年日本建立](../Category/1992年日本建立.md "wikilink")