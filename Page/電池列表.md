**電池列表**將儘量列出所有类型的電池，並列出別名以方便查考，易混淆的項目附加簡要說明。

## 依產生電力的方式區分

  - [濕電池](../Page/濕電池.md "wikilink")
      - [原電池](../Page/原電池.md "wikilink")（一次電池、拋棄式電池）
          - [伏打電池](../Page/伏打電堆.md "wikilink") （Voltaic Pile）
          - [丹尼爾電池](../Page/丹尼爾電池.md "wikilink") （Daniell cell）
          - [勒克朗舍電池](../Page/勒克朗舍電池.md "wikilink") （Leclanché cell）
      - [蓄電池](../Page/蓄電池.md "wikilink")（可充電電池、二次電池）
          - [鉛酸蓄電池](../Page/鉛酸蓄電池.md "wikilink")，又名鉛蓄電池
              - 非封閉式 (傳統型，需保持液面高度，不足時需加蒸餾水)
              - 密封式 (Sealed，外殼密封，不需也不能加水)
  - [乾電池](../Page/乾電池.md "wikilink")
      - [拋棄式電池](../Page/原電池.md "wikilink") （一次電池、不可充電電池、原電池）
          - [碳鋅電池](../Page/碳鋅電池.md "wikilink")，又名錳鋅電池、鋅-二氧化錳電池、鋅錳電池，有時也被簡稱錳乾電池
          - [鹼錳電池](../Page/鹼錳電池.md "wikilink")，鹼性錳鋅電池的簡稱，通常簡稱鹼性電池
          - [鋰電池](../Page/鋰電池.md "wikilink")，使用鋰的電池，依化學成分又可分為很多種，詳見條目
            編號開頭 CR 的常見一次性鋰電池是鋰錳電池，即鋰-二氧化錳電池。不同於鋰充電電池。
          - [鋅空氣電池](../Page/鋅空電池.md "wikilink")，又名鋅氧電池，有時簡稱鋅空電池，極為少數的情況被稱鋅電池
          - [鋅汞電池](../Page/鋅汞電池.md "wikilink")，通常稱水銀電池
          - [氫氧電池](../Page/氫氧電池.md "wikilink")，這裡指的是氫氧化鎳鹼性電池的商品名稱，不是氫氧燃料電池
          - [鎂錳電池](../Page/鎂錳電池.md "wikilink")
          - [氧化銀電池](../Page/氧化銀電池.md "wikilink")
      - [可充電電池](../Page/蓄電池.md "wikilink") （二次電池）
          - [鎳鎘電池](../Page/鎳鎘電池.md "wikilink") (Ni-Cd)
          - [鎳鐵電池](../Page/鎳鐵電池.md "wikilink")
          - [鎳氫電池](../Page/鎳氫電池.md "wikilink") (Ni-MH)
          - [鋰離子電池](../Page/鋰離子電池.md "wikilink") (Li-Ion)
          - [銀鋅電池](../Page/銀鋅電池.md "wikilink")，化學反應與氧化銀電池相同，但電池製作不同
  - [燃料電池](../Page/燃料電池.md "wikilink")
      - [鹼性燃料電池](../Page/鹼性燃料電池.md "wikilink") （氫氧燃料電池）
      - [磷酸燃料電池](../Page/磷酸燃料電池.md "wikilink")
      - [質子交換膜燃料電池](../Page/質子交換膜燃料電池.md "wikilink")
      - [熔融碳酸鹽燃料電池](../Page/熔融碳酸鹽燃料電池.md "wikilink")
      - [固態氧化物燃料電池](../Page/固態氧化物燃料電池.md "wikilink")
      - [直接甲醇燃料電池](../Page/直接甲醇燃料電池.md "wikilink")
  - 放射物電池
      - [核電池](../Page/核電池.md "wikilink")
      - [射線電池](../Page/射線電池.md "wikilink")

## 依電池的尺寸區分

[6_most_common_battery_types-1.jpg](https://zh.wikipedia.org/wiki/File:6_most_common_battery_types-1.jpg "fig:6_most_common_battery_types-1.jpg")

  - 圓柱形（皆為1.5伏特）

<!-- end list -->

  - D，通稱「1號電池」，常用於大型的手電筒、瓦斯熱水器的電源
  - C，通稱「2號電池」，常用於振鈴鬧鐘
  - AA，台灣通稱「3號電池」（中国大陆通称5号电池），使用普遍，用於多種攜帶式電器或電子產品
  - AAA，台灣通稱「4號電池」（中国大陆通称7号电池），常用於各式家電的遙控器
  - N，台灣通稱「5號電池」（中国大陆通称8号电池），較不常見
  - AAAA，台灣通稱「6號電池」（中国大陆通称9号电池），不常見，用於手機、平板或手寫板的感應式數位筆、藍牙耳機等。這是一種適應產品小型化的較新型電池，2008年開始推廣用於一般消費產品。\[1\]

<!-- end list -->

  - 方形

<!-- end list -->

  - 9伏特電池

## 參考資料與附註

[电池](../Category/电池.md "wikilink")

1.  [Digitime 360° 產品:
    勁量電池新聞稿, 2008年7月](http://www.digitimes.com.tw/tw/dt/n/shwnws.asp?CnlID=10&cat=25&id=0000098475_A8J96J3AHR8ULBD419SQN&ct=2)