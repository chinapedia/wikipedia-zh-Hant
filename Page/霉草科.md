**霉草科**共包括8[属约](../Page/属.md "wikilink")48[种](../Page/种.md "wikilink")，广泛分布在全球[热带和](../Page/热带.md "wikilink")[亚热带区域](../Page/亚热带.md "wikilink")，包括[东南亚](../Page/东南亚.md "wikilink")、中[南美洲](../Page/南美洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[马达加斯加岛北部和](../Page/马达加斯加岛.md "wikilink")[澳大利亚东北](../Page/澳大利亚.md "wikilink")，[中国只有](../Page/中国.md "wikilink")[喜荫草属](../Page/喜荫草属.md "wikilink")（*Sciaphila*）1属3种，分布在[海南和南方沿海岛屿](../Page/海南.md "wikilink")。喜荫草属的种类最多。

本科[植物为腐生](../Page/植物.md "wikilink")[草本](../Page/草本.md "wikilink")，[茎不分枝](../Page/茎.md "wikilink")；[叶退化为鳞片状](../Page/叶.md "wikilink")；小[花单性](../Page/花.md "wikilink")，辐射对称，雌雄同株或异株，[花被](../Page/花被.md "wikilink")3－8；[蒴果](../Page/蒴果.md "wikilink")。

1981年的[克朗奎斯特分类法单独设立一个](../Page/克朗奎斯特分类法.md "wikilink")[霉草目](../Page/霉草目.md "wikilink")，将本[科和](../Page/科.md "wikilink")[无叶莲科一起放入该](../Page/无叶莲科.md "wikilink")[目](../Page/目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为无法将本科放入任何一目](../Page/APG_分类法.md "wikilink")，直接列在[单子叶植物分支之下](../Page/单子叶植物分支.md "wikilink")，
2003年经过修订的[APG II
分类法将本科列入](../Page/APG_II_分类法.md "wikilink")[露兜树目](../Page/露兜树目.md "wikilink")。

## 属

  - *[Andruris](../Page/Andruris.md "wikilink")* Schltr.
  - *Hexuris* Miers(SUS) = *Peltophyllum* Gardner
  - *Hyalisma* Champ. = *Sciaphila* Blume
  - *Lacandonia* E.Martinez & Ramos = *Triuris* Miers
  - *[Peltophyllum](../Page/Peltophyllum.md "wikilink")* Gardner
  - [喜荫草属](../Page/喜荫草属.md "wikilink") *Sciaphila* Blume
  - *[Seychellaria](../Page/Seychellaria.md "wikilink")* Hemsl.
  - *[Soridium](../Page/Soridium.md "wikilink")* Miers
  - [死寄生草属](../Page/死寄生草属.md "wikilink") *Triuris* Miers

## 参考文献

  - Maas-van de Kamer, H. & T. Weustenfeld (1998) in Kubitzki, K.
    (Editor): *The Families and Genera of Vascular Plants*, Vol. 3.
    Springer-Verlag. Berlin, Germany. ISBN 3-540-64060-6

## 外部链接

:\* 在[L. Watson和M.J. Dallwitz
(1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[霉草科](http://delta-intkey.com/angio/www/triurida.htm)

:\*
[e-flora中的霉草科''](http://www.efloras.org/florataxon.aspx?flora_id=1200&taxon_id=10917)

:\* [The specialists at
work](https://web.archive.org/web/20080920061601/http://www.staff.uni-marburg.de/~b_morpho/maas.html)

:\*
[*Sciaphila*的照片](http://www.mobot.org/MOBOT/Research/APweb/images/sciaphila.html)

:\* [*Sciaphila
ramosa*的照片](http://www.hkherbarium.net/Herbarium/html%20text/96Sciaphila%20ramosa.htm)

:\* [*Sciaphila
megastyla*的照片](http://www.hkherbarium.net/Herbarium/html%20text/95Sciaphila%20megastyla.htm)

:\* [*Sciaphila
tosaensis*的种类](https://web.archive.org/web/20070310231318/http://taxa.soken.ac.jp/MakinoDB/makino/DCP0004/html_j/125.html)

:\*
[NCBI中的霉草科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=232376&lvl=3&lin=f&keep=1&srchmode=1&unlock)

:\*
[CSDL中的霉草科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Triuridaceae)

[\*](../Category/霉草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")