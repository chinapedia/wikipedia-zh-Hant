**霍普金斯縣**（**Hockley County,
Texas**）位[美國](../Page/美國.md "wikilink")[德克薩斯州西北部的一個縣](../Page/德克薩斯州.md "wikilink")。面積2,353平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口22,716人。縣治[萊弗蘭](../Page/萊弗蘭.md "wikilink")（Lavelland）。

成立於1876年8月21日，縣政府成立於1921年2月。縣名紀念[德克薩斯共和國戰爭部長](../Page/德克薩斯共和國.md "wikilink")[喬治·華盛頓·霍克利](../Page/喬治·華盛頓·霍克利.md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[H](../Category/得克萨斯州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.