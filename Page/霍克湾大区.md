  -
    *本條目講述的是[紐西蘭的](../Page/紐西蘭.md "wikilink")**豪克斯灣地區**，其附近的[海灣可參見](../Page/海灣.md "wikilink")[豪克灣](../Page/豪克灣.md "wikilink")。*

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><big><strong>豪克斯灣地區議會</strong><br />
<strong>Hawke's Bay Regional Council</strong></big></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/國家列表.md" title="wikilink">國家</a>：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Position_of_Hawkes_Bay.png" title="fig:Position_of_Hawkes_Bay.png">Position_of_Hawkes_Bay.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/紐西蘭地區列表.md" title="wikilink">地區議會</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>名稱：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>主席：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/人口.md" title="wikilink">人口</a>：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/陸地.md" title="wikilink">陸地</a><a href="../Page/面積.md" title="wikilink">面積</a>：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/網站.md" title="wikilink">網站</a>：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>市鎮</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/市.md" title="wikilink">市</a>：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/鎮.md" title="wikilink">鎮</a>：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>法定地方政府</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>名稱：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>網站：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

**霍克湾**（又譯**霍克斯灣**，**豪克斯湾**；[英語](../Page/英語.md "wikilink")：**Hawke's
Bay**；[毛利語](../Page/毛利語.md "wikilink")：****）是位於[紐西蘭](../Page/紐西蘭.md "wikilink")[北島東岸的一個](../Page/北島_\(紐西蘭\).md "wikilink")[地區](../Page/紐西蘭地區列表.md "wikilink")，地區[議會設於](../Page/議會.md "wikilink")[內皮爾及](../Page/內皮爾_\(紐西蘭\).md "wikilink")[哈斯丁](../Page/哈斯丁_\(紐西蘭\).md "wikilink")（Hastings）兩市。

## 地理

豪克斯灣位於[紐西蘭](../Page/紐西蘭.md "wikilink")[北島東岸](../Page/北島_\(紐西蘭\).md "wikilink")，其名稱沿用了[豪克灣](../Page/豪克灣.md "wikilink")（Hawke
Bay）的舊稱。\[1\]
豪克灣是一個半環形的的大[海灣](../Page/海灣.md "wikilink")，由[瑪希亞半島](../Page/瑪希亞半島.md "wikilink")（Mahia
Peninsula）以東北至[拐子角](../Page/拐子角.md "wikilink")（Cape Kidnappers）\[2\]
以西南延伸100[公里](../Page/公里.md "wikilink")。

豪克斯灣的範圍包括中、北豪克灣附近的沿海山丘地區、北邊的[懷羅瓦河](../Page/懷羅瓦河.md "wikilink")（Wairoa
River）的沖積平原、西邊的[哈斯丁](../Page/哈斯丁_\(紐西蘭\).md "wikilink")（Hastings）附近的[何瑞通加平原](../Page/何瑞通加平原.md "wikilink")（Heretaunga
Plains），以及位處[Kaweka山脈與](../Page/Kaweka山脈.md "wikilink")[瓦希尼山脈](../Page/瓦希尼山脈.md "wikilink")（Ruahine
Ranges）之間的山丘內陸。

豪克斯灣的圍範與前[豪克斯灣省](../Page/豪克斯灣省.md "wikilink")（Hawke's Bay
Province）的有別，一些位於西南邊的[馬努瓦圖-旺格紐伊地區](../Page/馬努瓦圖-旺格紐伊.md "wikilink")（Manawatu-Wanganui
Region）城鎮，譬如[Dannevirke及](../Page/:en:Dannevirke.md "wikilink")[伍德威爾](../Page/伍德威爾_\(紐西蘭\).md "wikilink")（Woodville），自稱是豪克斯灣的一部分。

豪克斯灣由[懷羅瓦地區](../Page/懷羅瓦.md "wikilink")（Wairoa
District）、[哈斯丁](../Page/哈斯丁_\(紐西蘭地區\).md "wikilink")（Hastings
District）、[內皮爾市](../Page/內皮爾_\(紐西蘭\).md "wikilink")、
[中豪克斯灣](../Page/中豪克斯灣_\(紐西蘭地區\).md "wikilink")（Central
Hawke's Bay
District）、[陶波地區小部分地區](../Page/陶波.md "wikilink")，以及朗伊蒂基地區（Rangitikei
District）組成。

## 歷史

1858-1876年間，[豪克斯灣省](../Page/豪克斯灣省.md "wikilink")（Hawke's Bay
Province）曾經是紐西蘭的一個[省份](../Page/紐西蘭省份.md "wikilink")。1858年2月，在[內皮爾召開的一個會議頒佈](../Page/內皮爾.md "wikilink")，豪克斯脫離[威靈頓省](../Page/威靈頓省.md "wikilink")（Wellington
Province）置省。

1931年2月3日，內皮爾及[哈斯丁](../Page/哈斯丁_\(紐西蘭\).md "wikilink")（Hastings）遭受[紐西蘭最慘重的](../Page/紐西蘭.md "wikilink")[自然災害破壞](../Page/自然災害.md "wikilink")。[黎克特制](../Page/黎克特制.md "wikilink")7.9級的[地震導致](../Page/地震.md "wikilink")256人喪生。\[3\]
其後，內皮爾展開重建，至今以[裝飾藝術建築聞名於世](../Page/裝飾藝術.md "wikilink")。\[4\]\[5\]
每年[二月](../Page/二月.md "wikilink")，內皮爾會秉承傳統，舉辦[裝飾藝術週末](../Page/裝飾藝術.md "wikilink")（Art
Deco Weekend）\[6\]\[7\]。

## 人口

根據2006年[人口普查](../Page/人口普查.md "wikilink")，豪克斯灣的人口有147,783人，當中55,359人在[內皮爾市居住](../Page/內皮爾_\(紐西蘭\).md "wikilink")。內皮爾及[哈斯丁](../Page/哈斯丁_\(紐西蘭\).md "wikilink")（Hastings）是主要的市區。小社區包括[懷羅瓦](../Page/懷羅瓦.md "wikilink")（Wairoa）、[哈夫洛克北](../Page/哈夫洛克北.md "wikilink")（Havelock
North）、[Tikokino](../Page/:en:Tikokino.md "wikilink")、[威帕](../Page/威帕.md "wikilink")（Waipawa）、[普庫勞](../Page/普庫勞.md "wikilink")（Waipukurau）及[Takapau](../Page/:en:Takapau.md "wikilink")。豪克斯灣有相當大的[毛利人](../Page/毛利人.md "wikilink")[人口](../Page/人口.md "wikilink")，根據2006年人口普查，佔總人口的24%）。[卡杭谷努](../Page/:en:Ngāti_Kahungunu.md "wikilink")（Ngāti
Kahungunu）是當地最大的毛利部族。

## 氣候與農業

[HB_Vineyard_autumn.JPG](https://zh.wikipedia.org/wiki/File:HB_Vineyard_autumn.JPG "fig:HB_Vineyard_autumn.JPG")時豪克斯灣一[葡萄園](../Page/葡萄.md "wikilink")\]\]
豪克斯灣的[夏季長而酷熱](../Page/夏季.md "wikilink")，[冬季涼快](../Page/冬季.md "wikilink")，這種乾燥、溫和的[氣候特別適宜種植](../Page/氣候.md "wikilink")[葡萄](../Page/葡萄.md "wikilink")。豪克斯灣的大果園和葡萄園眾多，以出产[世界顶级](../Page/世界.md "wikilink")[葡萄酒闻名](../Page/葡萄酒.md "wikilink")，亦藉此得過不少獎項。\[8\]
豪克斯灣的山丘地區適宜飼養[牛](../Page/牛.md "wikilink")、[羊](../Page/羊.md "wikilink")，而地勢不平的地區受[森林地帶阻隔](../Page/森林.md "wikilink")。

## 趣聞

  - [塔烏瑪塔法卡塔尼哈娜可阿烏阿烏歐塔瑪提亞坡凱費努啊奇塔娜塔胡](../Page/塔烏瑪塔法卡塔尼哈娜可阿烏阿烏歐塔瑪提亞坡凱費努啊奇塔娜塔胡.md "wikilink")（Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu）是一座高305[米](../Page/米.md "wikilink")，座落於豪克斯灣南部的小[山](../Page/山.md "wikilink")，其[英文名](../Page/英文.md "wikilink")（實際為[毛利文](../Page/毛利文.md "wikilink")）全長85個[字母](../Page/字母.md "wikilink")，是全[世界的長](../Page/世界.md "wikilink")[地名之一](../Page/地名.md "wikilink")。\[9\]\[10\]\[11\]

## 參考

## 参见

  - [內皮爾](../Page/內皮爾_\(紐西蘭\).md "wikilink")
  - [紐西蘭地理](../Page/紐西蘭地理.md "wikilink")
  - [塔乌玛塔法卡塔尼哈娜可阿乌阿乌欧塔玛提亚坡凯费努啊奇塔娜塔胡](../Page/塔乌玛塔法卡塔尼哈娜可阿乌阿乌欧塔玛提亚坡凯费努啊奇塔娜塔胡.md "wikilink")

## 外部連結

  - [豪克斯灣官方資訊](http://www.hawkesbay.com)
  - [認識豪克斯灣：豪克斯灣指南](https://web.archive.org/web/20160312162224/http://www.new2hb.com/)

  - [內皮爾市議會網站](http://www.napier.govt.nz/index.php?cid=napier/history/hi_gen)

  - [豪克斯灣省及省地區](https://web.archive.org/web/20070103043707/http://www.teara.govt.nz/1966/H/HawkesBayProvinceAndProvincialDistrict/HawkesBayProvinceAndProvincialDistrict/en)（《紐西蘭百科全書》）


[Category:紐西蘭大區](../Category/紐西蘭大區.md "wikilink")

1.  豪克斯灣（Hawke's Bay）和[豪克灣](../Page/豪克灣.md "wikilink")（Hawke
    Bay）都位於[紐西蘭](../Page/紐西蘭.md "wikilink")[北島](../Page/北島_\(紐西蘭\).md "wikilink")，前者是地區，後者是[海灣](../Page/海灣.md "wikilink")。
2.  [拐子角](../Page/拐子角.md "wikilink")，又譯[擄人岬](../Page/擄人岬.md "wikilink")，英語原名Cape
    Kidnappers。
3.  [地震：生活在邊緣](http://www.hawkesbaynz.com/trade_media/media_kits/story_angles/architecture/earthquake_living_on_the_edge.htm?xhighlightwords=earthquake)

4.  [歷史建築](http://www.hawkesbaynz.com/sights_and_activities/art_culture_and_heritage/historic_buildings/index.htm?xhighlightwords=earthquake)

5.  [葡萄酒國家心臟的古典遺產珍寶](http://www.hawkesbaynz.com/trade_media/media_kits/story_angles/architecture/a_classical_heritage_gem.htm?xhighlightwords=earthquake)

6.  [紐西蘭豪克斯灣內皮爾裝飾藝術信託](http://www.hawkesbaynz.com/pages/artdecotrust)
7.  [未來活動日子](http://www.artdeconapier.com/Future_Events_Dates_64.aspx)
    （[裝飾藝術信託網站](../Page/裝飾藝術.md "wikilink")）
8.  [紐西蘭豪克斯灣旅遊：食物與葡萄酒](http://www.hawkesbaynz.com/food_wine/)
9.  [名稱之爭](http://www.kiwiherald.com/LongestPlaceName.html) （Kiwi
    Herald）
10. [語言條目：世界最長地名](http://www.yourdictionary.com/library/article009.html)（YourDictionary）

11. [世界最長地名](http://www.hawkesbaynz.com/trade_media/media_kits/story_angles/art_and_culture/longest_place_name.htm)（豪克斯灣紐西蘭官方資訊）