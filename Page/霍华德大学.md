**霍华德大学**（**Howard
University**）是位于[美国](../Page/美国.md "wikilink")[华盛顿哥伦比亚特区的一所私立大学](../Page/华盛顿哥伦比亚特区.md "wikilink")，在[美国国会的批准下于](../Page/美国国会.md "wikilink")1867年3月2日由[南北战争中的联邦将军](../Page/南北战争.md "wikilink")建立，霍华德其後亦當大學的第三任校長。

霍华德大学是一所历史悠久的[黑人大学](../Page/傳統黑人大學.md "wikilink")，其授予[非裔美国人](../Page/非裔美国人.md "wikilink")[哲学博士学位的数量为全美第一](../Page/哲学博士.md "wikilink")。2015年《[美国新闻与世界报道](../Page/美国新闻与世界报道.md "wikilink")》將其列在「全國大學」中的第145位。\[1\]

## 參考文獻

## 外部連結

  - [Official website](http://www.howard.edu/)

  - [Official athletics website](http://www.howard-bison.com/)

  -
  -
[Category:華盛頓哥倫比亞特區大學](../Category/華盛頓哥倫比亞特區大學.md "wikilink")
[Category:美國傳統黑人大學](../Category/美國傳統黑人大學.md "wikilink")
[Category:1867年創建的教育機構](../Category/1867年創建的教育機構.md "wikilink")
[Category:冠以人名的教育機構](../Category/冠以人名的教育機構.md "wikilink")
[Category:霍華德大學](../Category/霍華德大學.md "wikilink")

1.