**霍汶希**（，），生于英属香港，童星出身的香港著名演艺圈经纪人，[英皇娛樂藝人管理部總監及唱片部總監](../Page/英皇娛樂.md "wikilink")、北京辦事處營運總裁。

## 早年

霍汶希生于英属香港，原名**霍寶珠**，而且她是雙胞胎，与孖生妹霍寶玲（Noven Fok）一模一样，另有六名亲生手足，最大的姊姊相差20岁。

霍汶希曾任職连卡佛售貨員、广告[模特兒等](../Page/模特兒.md "wikilink")，早年曾拍攝[必勝客](../Page/必勝客.md "wikilink")[廣告](../Page/廣告.md "wikilink")。

## 香港金牌经纪人

模特生涯结束后，霍汶希在1993年转为幕后，加入[飞图唱片](../Page/飞图唱片.md "wikilink")；后英皇集团收购，于是成为为英皇娱乐幕后经纪人。

2003年[廉政公署的](../Page/廉政公署.md "wikilink")[舞影行動](../Page/舞影行動.md "wikilink")，霍汶希亦牽涉其中。

霍汶希曾寫過[博益出版的](../Page/博益.md "wikilink")《我的驕傲-偶像必紅方程式》及《我的驕傲2》，講述作為經理人的經驗，及爆旗下藝人一些不為人知的秘密。其後第3集《偶像回到校園時》講及她2006年往[美國](../Page/美國.md "wikilink")[留學的經歷](../Page/留學.md "wikilink")。

## 个人生活

霍汶希至今未婚，早年曾与叶崇仁恋爱过。

2013年母亲节当天，霍汶希微博里自爆2年前生了一个女儿，取名Honey；据称Honey生父为广州富商贺丹青。\[1\]\[2\]。

## 旗下藝人

## 著作

  - [我的驕傲-偶像必紅方程式](../Page/我的驕傲-偶像必紅方程式.md "wikilink")
  - [我的驕傲2](../Page/我的驕傲2.md "wikilink")
  - 美國日誌 mani 偶像回到校園時
  - Mani VS 偶像終極大反擊

## 電影

  - 2012年 《[超級經理人](../Page/超級經理人.md "wikilink")》

## 獎項

**2007:**

  - 2006年度[點正娛樂新聞王](../Page/點正娛樂新聞王.md "wikilink") ——樂壇幕後新聞王

## 參考資料

## 外部連結

  - [英皇娛樂集團](http://www.eegmusic.com)
  - [英皇電影集團](http://www.emp.hk)

## 相關條目

  - [英皇集團](../Page/英皇集團.md "wikilink")
  - [英皇娛樂集團有限公司](../Page/英皇娛樂集團有限公司.md "wikilink")
  - [吴雨](../Page/吴雨.md "wikilink")

[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:1972年出生](../Category/1972年出生.md "wikilink")
[Category:香港經理人](../Category/香港經理人.md "wikilink")
[Category:香港前儿童演员](../Category/香港前儿童演员.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:英皇集團](../Category/英皇集團.md "wikilink")
[Category:九龙真光中学校友](../Category/九龙真光中学校友.md "wikilink")
[Category:纽约大学校友](../Category/纽约大学校友.md "wikilink")
[Wen汶希](../Category/霍姓.md "wikilink")
[Category:雙胞胎人物](../Category/雙胞胎人物.md "wikilink")

1.  [Mani 自 爆 未 婚 生 女 愛 將 紛 祝
    福](http://www.singtao.com/yesterday/ent/0512fo01.html)2013年5月12日
    [星島日報](../Page/星島日報.md "wikilink")
2.  [Mani
    自爆未婚生女](http://hk.dv.nextmedia.com/actionnews/hit/20130512/18256830/20014857)