[Mikloshorthy.jpg](https://zh.wikipedia.org/wiki/File:Mikloshorthy.jpg "fig:Mikloshorthy.jpg")

**霍爾蒂·米克洛什**（，，），[匈牙利的軍人與政治人物](../Page/匈牙利.md "wikilink")。1920－1944年為[攝政](../Page/攝政.md "wikilink")，掌握軍政實權。

## 生平

霍尔蒂出身贵族家庭，并且参与[奥匈帝国的](../Page/奧匈帝國.md "wikilink")[海军](../Page/海軍.md "wikilink")，在[第一次世界大战期间由于在](../Page/第一次世界大戰.md "wikilink")[亚得里亚海击溃](../Page/亞得里亞海.md "wikilink")[意大利的海军而驰名](../Page/意大利.md "wikilink")。1918年霍尔蒂以海军少将名义转任海军舰队司令官，直到第一次世界大战的结束为止。

### 建立獨裁政權

[Horthy_the_regent.jpg](https://zh.wikipedia.org/wiki/File:Horthy_the_regent.jpg "fig:Horthy_the_regent.jpg")
1919年匈牙利成立以[库恩·贝洛為首的](../Page/库恩·贝洛.md "wikilink")[匈牙利蘇維埃共和國](../Page/匈牙利蘇維埃共和國.md "wikilink")，但遭到当时[协约国的大举围攻](../Page/協約國.md "wikilink")，导致匈牙利红军损失惨重，而此时国内其他政治力量转而投向协约国并支持霍爾蒂與其軍隊，而霍爾蒂在[匈牙利蘇維埃共和國政府因无力反抗而辞职之後便掌握政權](../Page/匈牙利蘇維埃共和國.md "wikilink")，成立[匈牙利王國並出任王國攝政](../Page/匈牙利王國_\(1920年—1946年\).md "wikilink")（國王之位空缺），建立軍事獨裁體制。

### 失勢

[Miklós_Horthy_and_Adolf_Hitler_1938.jpg](https://zh.wikipedia.org/wiki/File:Miklós_Horthy_and_Adolf_Hitler_1938.jpg "fig:Miklós_Horthy_and_Adolf_Hitler_1938.jpg")於[柏林](../Page/柏林.md "wikilink")\]\]
[第二次世界大戰爆發後](../Page/第二次世界大戰.md "wikilink")，霍爾蒂為了占領[捷克斯洛伐克以及](../Page/捷克斯洛伐克.md "wikilink")[羅馬尼亞兩國匈牙利人的居住地區而與](../Page/羅馬尼亞.md "wikilink")[納粹德國](../Page/納粹德國.md "wikilink")[阿道夫·希特勒結盟](../Page/阿道夫·希特勒.md "wikilink")，而匈牙利也擴大了領土；但是1944年霍爾蒂有意退出戰爭並與[德國斷交之時](../Page/德國.md "wikilink")，便被德軍挟持至德國，德國方面並扶植了以[箭十字黨為首的](../Page/箭十字黨.md "wikilink")[傀儡政權](../Page/傀儡政權.md "wikilink")。戰後霍爾蒂被[南斯拉夫以](../Page/南斯拉夫.md "wikilink")[戰爭罪要求引渡](../Page/戰爭罪.md "wikilink")，但是被[聯合國及](../Page/聯合國.md "wikilink")[美國阻止](../Page/美國.md "wikilink")。後在美國保護下，霍爾蒂得以不用像其他納粹戰犯一樣接受戰爭犯罪和屠殺等罪行審判。

### 流亡中去世

[Kenderes_Horthy_családi_sírbolt.jpg](https://zh.wikipedia.org/wiki/File:Kenderes_Horthy_családi_sírbolt.jpg "fig:Kenderes_Horthy_családi_sírbolt.jpg")
1945年[蘇聯](../Page/蘇聯.md "wikilink")[红军占領匈牙利後](../Page/苏联红军.md "wikilink")，霍爾蒂便流亡[葡萄牙](../Page/葡萄牙.md "wikilink")，到1957年過世時都未能再踏上匈牙利國土。直到1990年[匈牙利社会主义工人党下台之后](../Page/匈牙利社会主义工人党.md "wikilink")，1993年霍爾蒂的遺骸才歸葬匈牙利故鄉。

[Category:最神聖報喜勳章爵士](../Category/最神聖報喜勳章爵士.md "wikilink")
[Category:匈牙利政治人物](../Category/匈牙利政治人物.md "wikilink")
[Category:被政變推翻的領導人](../Category/被政變推翻的領導人.md "wikilink")
[Category:第二次世界大戰領袖](../Category/第二次世界大戰領袖.md "wikilink")
[Category:匈牙利第二次世界大戰人物](../Category/匈牙利第二次世界大戰人物.md "wikilink")
[Category:匈牙利國防部長](../Category/匈牙利國防部長.md "wikilink")
[Category:匈牙利反共主義者](../Category/匈牙利反共主義者.md "wikilink")
[Category:奧匈帝國第一次世界大戰人物](../Category/奧匈帝國第一次世界大戰人物.md "wikilink")
[Category:奧匈帝國海軍軍官](../Category/奧匈帝國海軍軍官.md "wikilink")
[Category:匈牙利基督徒](../Category/匈牙利基督徒.md "wikilink")
[Category:匈牙利貴族](../Category/匈牙利貴族.md "wikilink")