**霍邱县**是[中國](../Page/中國.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[六安市下辖的一个县](../Page/六安市.md "wikilink")。面积3493平方千米，人口162.7万。邮政编码237400。县人民政府驻城关镇。

## 歷史沿革

[周代時為](../Page/周代.md "wikilink")[蓼國封地](../Page/蓼國.md "wikilink")，故稱蓼城。[南朝梁置霍](../Page/南朝梁.md "wikilink")-{丘}-戌。[隋朝](../Page/隋朝.md "wikilink")[開皇十九年](../Page/開皇.md "wikilink")（599年）置霍丘縣，縣名因周代霍叔曾遷於此而得名（《讀史方輿紀要》：「志云：周成王時，霍叔遷於此，縣因以名。」）

[清朝時](../Page/清朝.md "wikilink")，清代統治者為避[孔子名諱](../Page/孔子.md "wikilink")（孔子名丘），改為霍邱縣。[民國時期沿用](../Page/民國.md "wikilink")。

## 行政区划

下辖19个[镇](../Page/镇_\(中华人民共和国\).md "wikilink")，11个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")。

  - 镇：[城关镇](../Page/城关镇_\(霍邱县\).md "wikilink")、[曹庙镇](../Page/曹庙镇.md "wikilink")、[众兴集镇](../Page/众兴集镇.md "wikilink")、[夏店镇](../Page/夏店镇.md "wikilink")、[乌龙镇](../Page/乌龙镇.md "wikilink")、[户胡镇](../Page/户胡镇.md "wikilink")、[龙潭镇](../Page/龙潭镇_\(霍邱县\).md "wikilink")、[石店镇](../Page/石店镇.md "wikilink")、[冯井镇](../Page/冯井镇.md "wikilink")、[临水镇](../Page/临水镇.md "wikilink")、[高塘镇](../Page/高塘镇.md "wikilink")、[岔路镇](../Page/岔路镇.md "wikilink")、[新店镇](../Page/新店镇.md "wikilink")、[孟集镇](../Page/孟集镇.md "wikilink")、[花园镇](../Page/花园镇.md "wikilink")、[周集镇](../Page/周集镇.md "wikilink")、[马店镇](../Page/马店镇_\(霍邱县\).md "wikilink")、[长集镇](../Page/长集镇.md "wikilink")、[河口镇](../Page/河口镇.md "wikilink")。
  - 乡：[宋店乡](../Page/宋店乡.md "wikilink")、[三流乡](../Page/三流乡.md "wikilink")、[城西湖乡](../Page/城西湖乡.md "wikilink")、[临淮岗乡](../Page/临淮岗乡.md "wikilink")、[邵岗乡](../Page/邵岗乡.md "wikilink")、[白莲乡](../Page/白莲乡.md "wikilink")、[范桥乡](../Page/范桥乡.md "wikilink")、[王截流乡](../Page/王截流乡.md "wikilink")、[潘集乡](../Page/潘集乡.md "wikilink")、[彭塔乡](../Page/彭塔乡.md "wikilink")、[冯瓴乡](../Page/冯瓴乡.md "wikilink")。

[霍邱县](../Category/霍邱县.md "wikilink") [县](../Category/六安区县.md "wikilink")
[六安](../Category/安徽县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")