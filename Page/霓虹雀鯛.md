**霓虹雀鯛**，又稱**天藍雀鯛**，俗名為厚殼仔，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[雀鯛科的其中一](../Page/雀鯛科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[安達曼海](../Page/安達曼海.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國](../Page/中國.md "wikilink")[南海](../Page/南海.md "wikilink")、[越南](../Page/越南.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[索羅門群島](../Page/索羅門群島.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[法屬波里尼西亞](../Page/法屬波里尼西亞.md "wikilink")、[諾魯](../Page/諾魯.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[夏威夷群島](../Page/夏威夷群島.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[萊恩群島](../Page/萊恩群島.md "wikilink")、[羅雷淺灘](../Page/羅雷淺灘.md "wikilink")、[羅得豪島等海域](../Page/羅得豪島.md "wikilink")。

## 深度

水深1至12公尺。

## 特徵

本魚體呈長橢圓形而側扁，體色天藍，在腹部、尾鰭及背鰭、臀鰭末端略帶淺黃色。吻短而鈍圓。口中型；頜齒兩列，小而呈圓錐狀。眶下骨裸出，下緣平滑。眶前骨與眶下骨間無缺刻；前鰓蓋骨後緣具鋸齒。當其受驚下、環境或生理有變化時，體色會變成藍黑色，所以又稱為變色雀鯛。背鰭硬棘13枚、背鰭軟條13至15枚、臀鰭硬棘2枚、臀鰭軟條14至15。體長可達9公分。

## 生態

本魚喜歡大群群聚在一起，盤旋在礁盤上層水域，有時亦會三三兩兩居住在礁隙中。屬雜食性，以動物性浮游生物及[藻類為食](../Page/藻類.md "wikilink")。

## 經濟利用

體色鮮麗，極適合養在水族箱觀賞，很少人食用。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[coelestis](../Category/雀鯛屬.md "wikilink")