**露絲拉娜·寇舒諾娃**（俄罗斯语：Руслана Ќоршунова，英语：Ruslana
Korshunova，），生于苏联时期阿拉木图市，已故[俄罗斯裔](../Page/俄罗斯.md "wikilink")[超級名模](../Page/超級名模.md "wikilink")。

天然栗色柔顺長髮是她的魅力標記，人们亲切暱稱她為「俄羅斯辮子靓丽姑娘」（Russian Rapunzel），也被譽為「當代時尚界的靈感女神」。

2008年6月28日，正红极一时的她突然从纽约市中心的自家住處9楼阳台离奇坠落当场身亡，终年歲。她的离奇早逝令整个模特行业为之震惊哀恸，而且导致模特行业很多行内血色黑暗真相内幕层层曝光得满城风雨，剧烈引发了许多人关于当今模特行业的争议和批判。

## 生平

露絲拉娜出生於[蘇聯時代](../Page/蘇聯.md "wikilink")，[哈薩克蘇維埃社會主義共和國](../Page/哈薩克蘇維埃社會主義共和國.md "wikilink")（即今[哈薩克共和國](../Page/哈薩克.md "wikilink")），[阿拉木圖市](../Page/阿拉木圖.md "wikilink")\[1\]，能說流利的[俄語](../Page/俄語.md "wikilink")、[英語與](../Page/英語.md "wikilink")[德語](../Page/德語.md "wikilink")\[2\]。

2003年，*All Asia*雜誌報道一個阿拉木圖的德語社團時，露絲拉娜的照片被刊登出來，*Models 1*
模特兒經紀公司的黛比·瓊斯（Debbie
Jones）在飛機上隨手翻閱到該雜誌並-{注意}-到她，幾經努力找到這位她所說「美如神話故事人物」的女孩，簽下當時年僅
15 歲的露絲拉娜成為模特兒\[3\]\[4\]。

露絲拉娜曾在 *IMG Models*\[5\]、*Beatrice Model*、*Traffic Models*、*Marilyn
Models*、與 *iCasting Moscow* 等模特兒經紀公司旗下。

2005年，露絲拉娜漸露頭角，曾躍上《[ELLE](../Page/ELLE.md "wikilink")（[法國](../Page/法國.md "wikilink")）》雜誌、《[VOGUE](../Page/VOGUE.md "wikilink")（[波蘭](../Page/波蘭.md "wikilink")）》雜誌與《VOGUE（[俄羅斯](../Page/俄羅斯.md "wikilink")）》等雜誌的封面，也陸續為
[Blugirl](../Page/:en:Blugirl.md "wikilink")、[Clarins](../Page/:en:Clarins.md "wikilink")、Ghost、[Girbaud](../Page/:en:Marithé_François_Girbaud.md "wikilink")、[Kenzo
Accessories](../Page/:en:Kenzo_Takada.md "wikilink")、[Marithé &
François](../Page/:en:Marithé_François_Girbaud.md "wikilink")、[Max
Studio](../Page/:en:Leon_Max.md "wikilink")、[Moschino](../Page/:en:Moschino.md "wikilink")、Old
England、Pantene Always Smooth、[Paul
Smith與](../Page/:en:Paul_Smith_\(fashion_designer\).md "wikilink")[Vera
Wang
lingerie的平面模特兒](../Page/:en:Vera_Wang.md "wikilink")\[6\]，以及為[DKNY](../Page/:en:DKNY.md "wikilink")、[Vera
Wang](../Page/:en:Vera_Wang.md "wikilink")（王薇薇）、[Marc
Jacobs](../Page/:en:Marc_Jacobs.md "wikilink")、[Nina
Ricci等知名](../Page/:en:Nina_Ricci_\(designer\).md "wikilink")[時尚](../Page/時尚.md "wikilink")[品牌](../Page/品牌.md "wikilink")[時裝表演走台步或拍廣告](../Page/時裝表演.md "wikilink")\[7\]。

2008年6月28日，[美東時間下午](../Page/美東時間.md "wikilink")02:30分左右，露絲拉娜被發現從[美國](../Page/美國.md "wikilink")[紐約](../Page/紐約.md "wikilink")[曼哈頓金融區水街](../Page/曼哈頓.md "wikilink")（）住所大樓墜樓身亡，死亡现场血溅满地脑浆满散，惨不忍睹，得年歲，警方判斷她是從9樓住處陽台墜下，並表示因其住處室內沒有任何掙扎或打鬥的跡象，警方判斷為自殺\[8\]\[9\]。但她的至亲好友们均一致坚称她不可能自杀。

2008年7月7日，露絲拉娜遺體葬於[莫斯科](../Page/莫斯科.md "wikilink")\[10\]，露絲拉娜的母親向[俄羅斯當地報紙](../Page/俄羅斯.md "wikilink")《[共青團真理報](../Page/共青團真理報.md "wikilink")》表示，俄國首都是她女兒喜愛的城市之一，相信露絲拉娜會希望莫斯科是她最後安身之所\[11\]\[12\]。

她的死被普遍公认为是“模特行业黑幕的冰山一角”，震惊了整个模特行业，生前经纪公司在官网上发布巨大篇幅悼念这位当红超模的猝死。随后许多内行人士逐渐揭露模特圈的许多黑幕，尤其是黑帮控制、官商勾结、权色交易泛滥、模特吸毒普遍、抑郁症常见、模特精神压力不堪等，说法诸多，争议甚广。

## 參考資料

## 外部連結

  -
[Category:俄羅斯女性模特兒](../Category/俄羅斯女性模特兒.md "wikilink")
[Category:死在美国的俄罗斯人](../Category/死在美国的俄罗斯人.md "wikilink")
[Category:俄羅斯自殺者](../Category/俄羅斯自殺者.md "wikilink")
[Category:阿拉木圖人](../Category/阿拉木圖人.md "wikilink")
[Category:自殺女性模特兒](../Category/自殺女性模特兒.md "wikilink")

1.

2.

3.

4.

5.

6.  [Ruslana
    Korshunova](http://www.fashionmodeldirectory.com/models/Ruslana_Korshunova)
    Fashion Model Directory.com. Retrieved on June 29, 2008.

7.
8.
9.
10.

11.

12.