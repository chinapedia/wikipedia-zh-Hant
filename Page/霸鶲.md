**霸鶲**是[雀形目](../Page/雀形目.md "wikilink")[霸鶲科](../Page/霸鶲科.md "wikilink")（Tyrannidae）下的[鳥類](../Page/鳥類.md "wikilink")，主要生活在[北美洲及](../Page/北美洲.md "wikilink")[南美洲的](../Page/南美洲.md "wikilink")[熱帶地區](../Page/熱帶.md "wikilink")。牠們是[地球上最大的](../Page/地球.md "wikilink")[科](../Page/科.md "wikilink")，約有400個[物種](../Page/物種.md "wikilink")。其下的物種在外形上、款式及顏色上有很大的不同。一些霸鶲外表上很像[鶲科](../Page/鶲科.md "wikilink")。霸鶲屬於[霸鶲亞目](../Page/霸鶲亞目.md "wikilink")，沒有[鳴禽的複雜的發聲能力](../Page/鳴禽.md "wikilink")。

大部份的霸鶲較為素色，很多都有直立的冠。牠們大部份是吃[昆蟲的](../Page/昆蟲.md "wikilink")，有些吃[生果或細小的](../Page/生果.md "wikilink")[脊椎動物](../Page/脊椎動物.md "wikilink")。最細小的霸鶲是[短尾侏霸鶲及](../Page/短尾侏霸鶲.md "wikilink")[黑冠侏霸鶲](../Page/黑冠侏霸鶲.md "wikilink")。牠們只長約6.5-6.8厘米，重4-5克，是世界上最細小的雀形目。最大的霸鶲是[大鵙霸鶲](../Page/大鵙霸鶲.md "wikilink")，長29厘米及重88克。其他物種如[燕尾霸鶲及](../Page/燕尾霸鶲.md "wikilink")[叉尾王霸鶲的總長度較長](../Page/叉尾王霸鶲.md "wikilink")，但主要是因牠們有很長的尾巴。

很多原先分類在此科中的物種，現已重新分類在Tityridae中。

## 棲息地及分佈

霸鶲的[物種多樣性相比其棲息地變化更大](../Page/物種多樣性.md "wikilink")。牠們在[熱帶低地及山區的常綠森林有最高的單一位點物種多樣性](../Page/熱帶.md "wikilink")，而在[河流](../Page/河流.md "wikilink")、[棕櫚森林](../Page/棕櫚.md "wikilink")、白沙林、熱帶每年生樹林邊緣、南部熱帶森林及其邊緣、半潮濕／潮濕的山區灌木叢、及北部[溫帶](../Page/溫帶.md "wikilink")[草原的單一位點物種多樣性則最低](../Page/草原.md "wikilink")。最高與最低的物種多樣性是很極端的：在熱帶低地常綠森林中就有90個不同物種，但在其他的地方則只有1種。這是由於在某些地區的[生態位較小](../Page/生態位.md "wikilink")，故可以居住的地方亦較小。

霸鶲的特有性在熱帶低地及山區的常綠森林非常高，在當中的霸鶲亦有最大的特有性。在熱帶低地常綠森林就有49個霸鶲[特有種](../Page/特有種.md "wikilink")，而在山區常綠森林則有46個特有種。牠們的特有性程度相信是相似的。

## 保育狀況

*Camptostoma imberbe*及*Pachyramphus
aglaiae*現正受1918年的《候鳥協定法案》所保護。\[1\]這砍兩種霸鶲在[美國南部都很普遍](../Page/美國.md "wikilink")。但其他在[中美洲及](../Page/中美洲.md "wikilink")[南美洲的物種則更受威脅](../Page/南美洲.md "wikilink")。於2007年，[國際鳥盟認為](../Page/國際鳥盟.md "wikilink")[凯氏哑霸鹟](../Page/凯氏哑霸鹟.md "wikilink")
*Hemitriccus
kaempferi*是[極危物種](../Page/極危.md "wikilink")。都是[巴西的](../Page/巴西.md "wikilink")[特有種](../Page/特有種.md "wikilink")。另外8個物種是[瀕危物種及](../Page/瀕危物種.md "wikilink")18個物種是易危物種。\[2\]

## 分類

霸鶲科下有超過100個[屬及約](../Page/屬.md "wikilink")400個[物種](../Page/物種.md "wikilink")。[蒂拉屬](../Page/蒂拉屬.md "wikilink")、*Pachyramphus*、*Laniocera*及*Xenopsaris*雖然曾被分類此科中，但證據顯示牠們是屬於Tityridae的。\[3\]

  - （*Ornithion*）
  - （*Camptostoma*）
  - （*Phaeomyias*）
  - （*Nesotriccus*）
  - （*Capsiempis*）
  - [小霸鶲屬](../Page/小霸鶲屬.md "wikilink")（*Tyrannulus*）
  - （*Myiopagis*）
  - （*Pseudelaenia*）
  - [橄欖色霸鶲屬](../Page/橄欖色霸鶲屬.md "wikilink")（*Elaenia*）
  - （*Serpophaga*）
  - （*Mionectes*）
  - （*Leptopogon*）
  - （*Pseudotriccus*）
  - （*Phylloscartes*）
  - （*Phyllomyias*）
  - （*Zimmerius*）
  - （*Sublegatus*）
  - （*Suiriri*）
  - （*Mecocerculus*）
  - （*Inezia*）
  - （*Stigmatura*）
  - （*Uromyias*）
  - （*Anairetes*）
  - （*Tachuris*）
  - （*Culicivora*）
  - （*Polystictus*）
  - （*Pseudocolopteryx*）
  - （*Euscarthmus*）
  - （*Myiornis*）
  - （*Lophotriccus*）
  - （*Oncostoma*）
  - （*Poecilotriccus*）
  - （*Taeniotriccus*）
  - （*Hemitriccus*）

<!-- end list -->

  - （*Todirostrum*）
  - [蟻鷚屬](../Page/蟻鷚屬.md "wikilink")（*Corythopis*）
  - （*Cnipodectes*）
  - [扁嘴雀屬](../Page/扁嘴雀屬.md "wikilink")（*Ramphotrigon*）
  - [扁嘴霸鶲屬](../Page/扁嘴霸鶲屬.md "wikilink")（*Rhynchocyclus*）
  - （*Tolmomyias*）
  - （*Platyrinchus*）
  - （*Onychorhynchus*）
  - （*Myiotriccus*）
  - （*Myiophobus*）
  - （*Terenotriccus*）
  - （*Myiobius*）
  - （*Neopipo*）
  - （*Pyrrhomyias*）
  - （*Hirundinea*）
  - （*Cnemotriccus*）
  - （*Lathrotriccus*）
  - （*Aphanotriccus*）
  - （*Xenotriccus*）
  - （*Mitrephanes*）
  - [綠霸鶲屬](../Page/綠霸鶲屬.md "wikilink")（*Contopus*）
  - （*Empidonax*）
  - （*Sayornis*）
  - （*Pyrocephalus*）
  - （*Silvicultrix*）
  - （*Ochthoeca*）
  - （*Tumbezia*）
  - （*Colorhamphus*）
  - （*Ochthornis*）
  - （*Cnemarchus*）
  - （*Myiotheretes*）
  - （*Xolmis*）
  - （*Heteroxolmis*）

<!-- end list -->

  - （*Neoxolmis*）
  - （*Agriornis*）
  - （*Polioxolmis*）
  - （*Muscisaxicola*）
  - （*Muscigralla*）
  - （*Lessonia*）
  - （*Knipolegus*）
  - （*Hymenops*）
  - （*Fluvicola*）
  - （*Arundinicola*）
  - （*Alectrurus*）
  - （*Gubernetes*）
  - （*Satrapa*）
  - （*Colonia*）
  - （*Machetornis*）
  - （*Muscipipra*）
  - （*Attila*）
  - （*Casiornis*）
  - （*Sirystes*）
  - （*Rhytipterna*）
  - （*Myiarchus*）
  - （*Deltarhynchus*）
  - [蠅霸鶲屬](../Page/蠅霸鶲屬.md "wikilink")（*Pitangus*）
  - （*Megarynchus*）
  - （*Myiozetetes*）
  - （*Conopias*）
  - （*Myiodynastes*）
  - （*Legatus*）
  - （*Phelpsia*）
  - （*Empidonomus*）
  - （*Griseotyrannus*）
  - （*Tyrannopsis*）
  - [霸鶲屬](../Page/霸鶲屬.md "wikilink")（*Tyrannus*）

## 參考

## 外部連結

  - [霸鶲影片](http://ibc.hbw.com/ibc/phtml/familia.phtml?idFamilia=115)

[Category:霸鶲科](../Category/霸鶲科.md "wikilink")
[Category:霸鶲下目](../Category/霸鶲下目.md "wikilink")

1.
2.
3.