**靈寶黃金股份有限公司**（），簡稱**靈寶黃金**、**靈金**，成立於2002年，是[河南省](../Page/河南省.md "wikilink")[靈寶市的綜合型](../Page/靈寶市.md "wikilink")[黃金礦業企業](../Page/黃金.md "wikilink")，大股东為**靈寶市國有資產經營公司**。公司主要業務为黄金冶煉，旗下矿场位于[河南](../Page/河南.md "wikilink")、[新疆](../Page/新疆.md "wikilink")、[江西](../Page/江西.md "wikilink")。它在2006年於[香港交易所上市](../Page/香港交易所.md "wikilink")，招股價為每股$3.3港元。\[1\]\[2\]

## 外部連結

  - [靈寶黃金股份有限公司](http://www.lbgold.com/)

## 參考

[Category:香港上市工業公司](../Category/香港上市工業公司.md "wikilink")
[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")
[Category:中國H股](../Category/中國H股.md "wikilink")
[Category:金屬公司](../Category/金屬公司.md "wikilink")
[Category:河南公司](../Category/河南公司.md "wikilink")
[Category:金](../Category/金.md "wikilink")

1.  [灵宝黄金
    股市掘金](http://www.jinbw.com.cn/jinbw/xwzx/dfcjzk/ziben/20060119146.htm)

2.  [灵宝黄金上市首日飙升32.8%](http://finance.sina.com.cn/stock/hkstock/hkstocknews/20060113/07112272749.shtml)