[靈恩運動](../Page/靈恩運動.md "wikilink")，自20世紀初開始了穩步發展，因起源於[基督新教](../Page/基督新教.md "wikilink")，所以靈恩派教會的信仰宣言裏，不會出現「靈恩派」的名稱，他們不願意被貼上標籤，但傳統基督教派不同的是，更重視個人化的屬靈經驗，甚至許多基督傳統教派較少見的現象，例如：[說方言](../Page/說方言.md "wikilink")、醫病神蹟、[先知預言](../Page/先知.md "wikilink")。這些現象是類似於使徒傳道時期出現的現象。

靈恩派和[五旬宗有許多相似之處](../Page/五旬宗.md "wikilink")，部分強調信徒說方言，醫病神蹟，這與傳統基督信仰的務實觀點，有相當的距離。

不過與傳統基督教是有一些很明顯的分別，所有靈恩派最大特徵是在聚會時也會說一些自己及其他人也不能[翻譯的聲音](../Page/翻譯.md "wikilink")，他們稱其為「[說方言](../Page/說方言.md "wikilink")」，靈恩派對此有幾個解釋，例如是天使的語言、天上的語言、聖靈的語言，方言是根據聖經在[使徒時代所提及過的能力之一](../Page/使徒時代.md "wikilink")，不過聖經上所出現的方言能力，是能夠被[翻譯的](../Page/翻譯.md "wikilink")，是傳教時能講出當地人所用的實在語言，靈恩派的「聲響」則完全無法理解。

靈恩派與傳統基督教被[聖靈充滿的情況也有很大差異](../Page/聖靈.md "wikilink")，靈恩派被聖靈充滿時，不時會引起以下獨有的反應，雖然以上現象在[聖經從沒有出現過](../Page/聖經.md "wikilink")，但靈恩派相信這是由聖靈而來的反應，是被聖靈所充滿後的表現之一，據稱聖靈的充滿有助於與神有緊密連接著的關係，也沒有證據顯示這些表現最後會產生不良後果，而傳統基督教徒認為，被聖靈充滿時，是不一定有特殊反應的，反之是心靈上與聖靈達至異常親密的關係，或是唱詩、或是講道、或是方言之類，絕不會是固定化地去說方言，因為哥林多前書所述的恩賜是有很多種，傳統基督徒不認同被聖靈充滿就必須說方言。

靈恩派據稱有治病能力，但與聖經[使徒時代所出現的治病能力不完全相同](../Page/使徒時代.md "wikilink")，現代靈恩派的較多是治療非永久性的病，例如[傷風](../Page/傷風.md "wikilink")、[骨痛](../Page/骨痛.md "wikilink")、[瘀青等一些身體也有自我復原能力的病痛](../Page/瘀青.md "wikilink")、儘管當中也不乏諸如可自行復原的病痛如末期[癌症](../Page/癌症.md "wikilink")，而聖經以往述說的治病，通常是治好[盲](../Page/盲.md "wikilink")、[癱瘓](../Page/癱瘓.md "wikilink")、[痲瘋](../Page/痲瘋.md "wikilink")..等不能復原的病痛，而比較少記載治過一些身體可自行復原的病痛(例如[馬可福音](../Page/馬可福音.md "wikilink")1:30-31，[耶穌幫助西門的岳母治療](../Page/耶穌.md "wikilink")[熱病](../Page/熱病.md "wikilink"))。

靈恩派人數增長很快，根據David Barrett編World Christian Encyclopedia,
2001的統計：1987年靈恩派有二億四千七百萬人，而2000年是七億七千萬人(David
Barrett，Todd M Johnson編World Christian Trends AD30-AD2000, William
Carey,
2001)。靈恩派活躍於[南非](../Page/南非.md "wikilink")、[美國](../Page/美國.md "wikilink")、[拉丁美洲](../Page/拉丁美洲.md "wikilink")、[非洲及](../Page/非洲.md "wikilink")[歐洲等地](../Page/歐洲.md "wikilink")，最顯著的勢力範圍可說是[拉丁美洲與](../Page/拉丁美洲.md "wikilink")[非洲](../Page/非洲.md "wikilink")。

## 核心信條

他們一些核心信條和行為包括：

  - 相信[聖靈並不偏待人](../Page/聖靈.md "wikilink")，每個人都可以且應當被聖靈充滿、被聖靈掌管。
  - 認為被聖靈充滿，與[說方言有相關](../Page/說方言.md "wikilink")，若人被聖靈充滿，相信不久就會有說方言相關的恩賜。
  - 相信神的[神蹟並未隨著使徒時代的結束而消失](../Page/神蹟.md "wikilink")，現今的世代仍有醫病、[先知預言存在](../Page/先知.md "wikilink")。
  - 每位信徒都有聖靈所賜的恩賜，也就是靠著聖靈行神蹟。

## 現今的靈恩派

由於靈恩派的發展，如[靈恩運動](../Page/靈恩運動.md "wikilink")，今天的靈恩派與二十世紀初的靈恩派已有所不同。
靈恩派與[福音派雖在](../Page/福音派.md "wikilink")[神學上各持立場](../Page/神學.md "wikilink")，彼此卻有接納的現象。至於很多[基要派在救恩論上迴避](../Page/基要派.md "wikilink")[加爾文五點](../Page/加爾文主義#加爾文五要點.md "wikilink")，根據[香港聖公會在](../Page/香港聖公會.md "wikilink")2005年2月6日發行的《[教聲](../Page/教聲.md "wikilink")》，這些信徒有不少自稱是靈恩派的信徒。

2013年，[基督教純福音](../Page/基督教.md "wikilink")（五旬宗派）牧師[郭美江發表的偏激言論而遭上傳網路及批評事件](../Page/郭美江.md "wikilink")，其中誇張解說包含歧視[同性戀](../Page/同性戀.md "wikilink")、基督教神蹟等相關言論，史稱「[郭美江傳道爭議事件](../Page/郭美江傳道爭議事件.md "wikilink")」。

灵恩派在中国与其他教派一样受到打击。如2017年7月12日，内蒙古一灵恩派聚会点遭警察闯入，信徒遭殴打和抓捕拘留\[1\]。2018年12月18日，福建厦门当局以虚假理由取缔了靈恩派家庭教會「神愛之家基督教會」\[2\]。

## 参考文献

<div class="references-small">

<references/>

</div>

## 参见

  - [五旬宗](../Page/五旬宗.md "wikilink")
  - [灵恩运动](../Page/灵恩运动.md "wikilink")
  - [新靈恩運動](../Page/新靈恩運動.md "wikilink")
  - [靈糧堂](../Page/靈糧堂.md "wikilink")

{{-}}

[Category:基督教新教教派](../Category/基督教新教教派.md "wikilink")
[靈恩派](../Category/靈恩派.md "wikilink")

1.  [內蒙古一家庭教會百餘名信徒因聚會遭警察暴力鎮壓](https://zh.bitterwinter.org/more-than-100-family-church-believers-in-mongolia-violently-suppressed/)
2.  [福建當局以虛假理由取締家庭教會](https://zh.bitterwinter.org/fujian-authorities-ban-house-churches-for-fake-reasons/)