**青云谱区**是[中国](../Page/中国.md "wikilink")[江西省](../Page/江西省.md "wikilink")[南昌市的一个市辖区](../Page/南昌市.md "wikilink")，因[青云谱道观而得名](../Page/青云谱道观.md "wikilink")。总面积为40.4平方公里，2004年人口为24.1万。

## 行政区划

2013年，青云谱区辖5个街道、1个镇：

  - 街道：[洪都街道](../Page/洪都街道.md "wikilink")、[京山街道](../Page/京山街道.md "wikilink")、[三家店街道](../Page/三家店街道.md "wikilink")、[岱山街道](../Page/岱山街道.md "wikilink")、[徐家坊街道](../Page/徐家坊街道.md "wikilink")
  - 镇：[青云谱镇](../Page/青云谱镇.md "wikilink")

## 参考文献

## 外部链接

  - [南昌市青云谱区人民政府](http://www.qyp.gov.cn/)

[青云谱区](../Category/青云谱区.md "wikilink")
[区](../Category/南昌区县.md "wikilink")
[南昌](../Category/江西市辖区.md "wikilink")