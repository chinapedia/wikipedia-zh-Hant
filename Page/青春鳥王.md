《**青春鳥王**》（**The Bird
King**）是音樂團體[糯米糰發行的第二張](../Page/糯米糰_\(樂團\).md "wikilink")[專輯](../Page/專輯.md "wikilink")，該專輯推出於其成員紛紛[退伍後加入](../Page/退伍.md "wikilink")[魔岩唱片時](../Page/魔岩唱片.md "wikilink")，其中著名曲目有〈巴黎草莓〉、〈跆拳道〉。〈跆拳道〉一曲取材自〈[龍的傳人](../Page/龍的傳人.md "wikilink")〉，並蓄意[戲仿當時翻唱該曲的](../Page/戲仿.md "wikilink")[王力宏](../Page/王力宏.md "wikilink")。\[1\]\[2\]

## 曲目

1.  阿魯巴痢疾
2.  拜拜拜
3.  Funky People
4.  巴黎草莓
5.  青春小鳥
6.  沖脫炮蓋爽
7.  跆拳道
8.  Disco Night
9.  星期六晚上你要帶我去哪邊
10. 來不及
11. \-{蚋}-仔肉
12. Outro

## 參考資料

[Category:糯米團音樂專輯](../Category/糯米團音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:2000年音樂專輯](../Category/2000年音樂專輯.md "wikilink")

1.
2.