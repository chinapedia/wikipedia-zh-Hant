[qingtuan.jpg](https://zh.wikipedia.org/wiki/File:qingtuan.jpg "fig:qingtuan.jpg")
[芝麻馅料兰溪清明粿.jpg](https://zh.wikipedia.org/wiki/File:芝麻馅料兰溪清明粿.jpg "fig:芝麻馅料兰溪清明粿.jpg")
**青-{糰}-**，又稱**青團**、**青草糰**、**清明粿**、**艾粿**、**艾粑粑**等，[閩南稱為](../Page/閩南.md "wikilink")**青草粿**、**草仔粿**、**草粿**等，[客家人稱為](../Page/客家人.md "wikilink")**艾糍粑**、**艾糍**、**艾粄**等。是[中國南方部分地区](../Page/中國南方.md "wikilink")[清明節](../Page/清明節.md "wikilink")、[寒食節](../Page/寒食節.md "wikilink")、[中元節的名點之一](../Page/中元節.md "wikilink")，因為其使用青草汁液，製作外皮，故色澤為青綠色，所以叫做青糰。早期只有甜鹹兩種口味，一種包棗泥、豆沙等，另一種包豬肉、竹筍等，現代出現印上卡通圖案或者添加一些夾心軟糖。口味有巧克力味、各種水果味，甚至燕窩味等等，餡料出現猪油筍子、金针鲜肉、[金華火腿](../Page/金華火腿.md "wikilink")、雞湯鮑魚等口味，可說是青草口味的包子。有些甚至包著[冰淇淋](../Page/冰淇淋.md "wikilink")，或是變成西餐後的甜點\[1\]。

## 歷史

食用的習俗最早可在周朝找到線索\[2\]，於是百姓熄炊而“寒食三日”。寒食三日充饥傳統食品中有一種“青精飯”\[3\]\[4\]\[5\]。

因為清明節與[寒食節合二為一](../Page/寒食節.md "wikilink")，所以有些地方有吃冷食的習俗，寒食節有上墳和墓祭之俗，後來和三月上巳招魂續魄的習俗逐漸定在寒食上祭\[6\]。由於清明節氣在寒食第三日隨著時間轉移，寒食的習俗移到清明之中。宋朝以後寒食掃墓的習慣也遷到清明中。

[宋朝時叫做](../Page/宋朝.md "wikilink")“粉糰”，掃墓祭拜或踏青時食用，到了明清開始流行於[江浙](../Page/江浙.md "wikilink")\[7\]，清明節從普通的的農業節氣晉升為重大節日，寒食的影響消失無蹤，但飲食民俗卻以變形的方式傳承下去，並保存在清明節裡\[8\]。如今清明節也不如以往，現代人以應令嘗鮮為主，青糰的祭祖功能逐漸淡薄。

## 江浙地區

早期為掃墓時攜帶的點心，現在主要被人們用來當春天的時令點心來食用，也可以饋贈或款待親友\[9\]。青糰內餡濃密，外皮鬆軟，不甜不膩，味道清香，有[青草香氣](../Page/青草.md "wikilink")，有點黏但不會黏上牙齒，青糰的夾心有兩種，甜的多為[豆沙](../Page/豆沙.md "wikilink")\[10\]。

### 原料

以新鮮的[艾草汁和](../Page/艾草.md "wikilink")[糯米一起舂合讓米粉與汁融合所以為青色](../Page/糯米.md "wikilink")，包上[豆沙](../Page/豆沙.md "wikilink")、[棗泥等內餡](../Page/棗泥.md "wikilink")，[蘆葉墊在青團下面放到蒸籠內](../Page/蘆葉.md "wikilink")，熟了後抹上[麻油](../Page/麻油.md "wikilink")\[11\]\[12\]\[13\]。

青糰上色用的材料叫“青”，[浙江省](../Page/浙江省.md "wikilink")[临海市的青為](../Page/临海市.md "wikilink")[鼠鼬草](../Page/鼠鼬草.md "wikilink")，[蘇州](../Page/蘇州.md "wikilink")、[杭州一般用用青菜汁](../Page/杭州.md "wikilink")、嫩丝瓜叶汁增色，將“青”煮熟搗成汁與糯米粉和在一起。临海青糰的馅料有鹹、甜兩個樣式，鹹的是豆乾、笋丁、肉丁、咸菜等，而甜的多為豆沙，為了能兩種樣式分清，一般將而鹹的包成餃子狀，甜的包成圓的。用楮树叶垫底下，用蒸籠蒸15分钟完成\[14\]。

## 閩南地區

**草仔粿**（[閩南話](../Page/閩南話.md "wikilink")：），又稱**青草粿**、**草粿**，流行於[閩南等地的傳統食物](../Page/閩南.md "wikilink")，因攙入可食用草類而得名。早年是[中元普渡和](../Page/中元普渡.md "wikilink")[掃墓祭拜之食品](../Page/掃墓.md "wikilink")，現多為無聊時的小點心。

### 原料

外型扁平約巴掌大小，內以蘿蔔切絲剁碎為包餡，味道鹹，綠色外表，以[香蕉葉](../Page/香蕉.md "wikilink")、[月桃葉等為墊](../Page/月桃.md "wikilink")。閩南地區主要只有鹹的餡料，臺灣受到客家庄的影響，也流行甜餡，食用芝麻、花生餡的吃法。

因使用材料的差異而有不同稱呼，較常見的為使用[鼠麴草](../Page/鼠麴草.md "wikilink")製作的**鼠麴粿**（[閩南語](../Page/閩南語.md "wikilink")：；[客家語](../Page/客家語.md "wikilink")：田艾粄）、使用[艾草製作的](../Page/艾草.md "wikilink")**艾草粿**（[客家語](../Page/客家語.md "wikilink")：艾粄，ngie55ban31）。除此之外，常用來製作草仔粿的材料尚有青[苧麻葉](../Page/苧麻.md "wikilink")、[大葉田香草](../Page/大葉田香草.md "wikilink")、[桑葉](../Page/桑葉.md "wikilink")、[香蘭葉](../Page/香蘭葉.md "wikilink")、[雞屎藤等](../Page/雞屎藤.md "wikilink")\[15\]\[16\]。

## 客家地區

**客家青糰**，又稱**艾糍粑**、**艾糍**、**艾粄**。是以[艾草和](../Page/艾草.md "wikilink")[糯米粉为为主要原料制作成的一种](../Page/糯米.md "wikilink")[粑粑](../Page/粑粑.md "wikilink")，它是客家人传统的小吃。早年在[贛南](../Page/贛南.md "wikilink")[客家人](../Page/客家人.md "wikilink")、[广东](../Page/广东.md "wikilink")[客家人地区](../Page/客家人.md "wikilink")，艾糍是[中元節祭祀](../Page/中元節.md "wikilink")，清明节[掃墓時攜帶的必备食物](../Page/掃墓.md "wikilink")。随着商业化的发展，艾糍已经走出中元節與清明節，隨時在城乡的糕点摊档、酒楼食肆都可以见到其身影，以[綠豆沙为馅者最受歡迎](../Page/綠豆沙.md "wikilink")。

### 原料

  - 艾草
  - 糯米粉
  - 馅：分鹹、甜两种。鹹馅一般用鲜竹笋、豆角、肉丝等作馅料。甜馅一般用花生、糖、芝麻、綠豆作为馅料。
  - 灰水（碱水）：可以不加，但加了灰水使糍粑不致于相互粘在一起，而且口感更佳。

将艾草放入开水中煮烂，用漏勺捞起，滤去多余的水份（有些地方是将艾草晒干，磨成粉）；然后将煮好的艾草与[糯米粉加灰水和好](../Page/糯米.md "wikilink")（做甜艾糍最好适当加点糖）；再像包[饺子](../Page/饺子.md "wikilink")、[汤圆一样放入馅料包好](../Page/汤圆.md "wikilink")；最后放入蒸笼蒸熟，也可以放入油锅里面炸。

## 文字註釋

## 参考文献

## 參見

  - [清明節](../Page/清明節.md "wikilink")
  - [寒食節](../Page/寒食節.md "wikilink")

## 外部連結

  - [平埔原住民的祭祖文化--按拉粿(鼠麴粿)](https://tw.appledaily.com/new/realtime/20180401/1326398/?utm_source=facebook&utm_medium=social&utm_campaign=twad_article_share&utm_content=share_link&fbclid=IwAR33Triflni_QviFM-QvHOivu1MGel5ZdsXCkpQ295oVAdlwX2PvGqdoO04)

[Category:節日食品](../Category/節日食品.md "wikilink")
[Category:节日习俗小作品](../Category/节日习俗小作品.md "wikilink")
[Category:江南文化](../Category/江南文化.md "wikilink")
[Q青](../Category/客家食品.md "wikilink")
[Q青](../Category/上海小吃.md "wikilink")
[Q青](../Category/江苏小吃.md "wikilink")
[Q青](../Category/浙江小吃.md "wikilink")
[Q青](../Category/台灣小吃.md "wikilink")
[Category:粿](../Category/粿.md "wikilink")
[Category:糯米食品](../Category/糯米食品.md "wikilink")
[Category:中元節與盂蘭盆節
(華人)](../Category/中元節與盂蘭盆節_\(華人\).md "wikilink")

1.
2.  《[周禮](../Page/周禮.md "wikilink")》：仲春以木鐸循火禁於國中

3.  《[瑣碎錄](../Page/瑣碎錄.md "wikilink")》：蜀入遇寒食日，採陽桐葉，細冬青染飯，色青而有光。

4.  明《[七修類槁](../Page/七修類槁.md "wikilink")》：古人寒食採楊桐葉，染飯青色以祭，資陽氣也，今變而為青白團子，乃此義也

5.  清《[清嘉錄](../Page/清嘉錄.md "wikilink")》：市上賣青團熟藕，為祀先之品，皆可冷食

6.  《[唐書](../Page/唐書.md "wikilink")》：開元二十年敕，寒食上墓，《[禮經](../Page/禮經.md "wikilink")》無文。近代相傳，浸以成俗，宜許上墓同拜掃禮。

7.

8.
9.

10.
11.

12.

13.
14.

15. [《江奶奶的青草粿》，八煙聚落官網](http://bayien.eef.org.tw/bayie_08_4a1.html)

16. [《樹嬸辦桌之二--『粿』在自然》，貢寮·水·梯田，人禾環境倫理發展基金會](https://archive.is/20130706142425/kongaliao-water-terrace.blogspot.tw/2012/04/blog-post_30.html)