**青色**又稱**綠藍色**，在是指在介於[綠色和](../Page/綠色.md "wikilink")[藍色之間的](../Page/藍色.md "wikilink")[颜色](../Page/颜色.md "wikilink")，[波长大约为](../Page/波长.md "wikilink")500-485[奈米](../Page/奈米.md "wikilink")。

青色可以是指黑色、綠色或藍色，需根據前後文推斷。在中國的[五行學說中](../Page/五行.md "wikilink")，青色是[木的一種象徵](../Page/木.md "wikilink")；青色在中國文化中有生命的含義，也是[春季的象徵](../Page/春季.md "wikilink")。現代日語用詞上分指藍色（青/あお，ao）和綠色（緑/みどり，midori）。

在[加色法中](../Page/加色法.md "wikilink")，青色被定义为等量的[绿色光和](../Page/绿色.md "wikilink")[蓝色光混合而成的颜色](../Page/蓝色.md "wikilink")（<sub>如右方色标所示</sub>）。
[AdditiveColor.svg](https://zh.wikipedia.org/wiki/File:AdditiveColor.svg "fig:AdditiveColor.svg")中，绿色（G）和蓝色（B）混合得到青色。\]\]

## 漢語中的青

[漢字](../Page/漢字.md "wikilink")**青**，[舊字形爲](../Page/舊字形.md "wikilink")**-{靑}-**，[古漢語中指植物生長時之色](../Page/古漢語.md "wikilink")\[1\]\[2\]，也是從[藍草中提取出的色彩](../Page/藍草.md "wikilink")。\[3\]在中國[五行學說中對應](../Page/五行.md "wikilink")[木](../Page/木.md "wikilink")，是[東方](../Page/東方.md "wikilink")[正色](../Page/正色.md "wikilink")。\[4\]\[5\]在中國傳統的觀念中，青[黃為](../Page/黃.md "wikilink")**[綠](../Page/綠.md "wikilink")**色，[白青為](../Page/白.md "wikilink")**[碧](../Page/碧.md "wikilink")**色\[6\]，深青為**[蒼](../Page/蒼.md "wikilink")**\[7\]（一說淺青色爲蒼\[8\]），青又叫**[蔥](../Page/蔥.md "wikilink")**。\[9\]\[10\]

「青」在現代漢語中有時指[綠色](../Page/綠色.md "wikilink")，如「青菜」；有時指[藍色](../Page/藍色.md "wikilink")，如「青天」；有時指黑色\[11\]，如「青紅皂白」。現代日語則大多意指[藍色](../Page/藍色.md "wikilink")。\[12\]

[青-seal.svg](https://zh.wikipedia.org/wiki/File:青-seal.svg "fig:青-seal.svg")\]\]

小篆“青”字的上部爲生、下部爲丹。《說文》指出，“生”、“丹”提示其字義，因爲在[五行理論中](../Page/五行.md "wikilink")，青代表木，而木又“生”火；赤（“丹”是赤色石\[13\]）對應火。[章太炎認爲](../Page/章太炎.md "wikilink")“青”由“生”派生而來，生的甲骨文是[草破土而出的](../Page/草.md "wikilink")[象形字](../Page/象形.md "wikilink")\[14\]，所以“青”字反映了草木生長的形貌。\[15\]《[釋名](../Page/釋名.md "wikilink")》認爲“青，生也”，指出青代表生命。青的字形也反映了它的字義。也因此，漢語多以“青”字描述植物，可以表現其生命力\[16\]；將人的幼年稱爲“青年”\[17\]；黑髮叫青絲。\[18\]現代有學者指出，青是“生命的顏色”。\[19\]

[五行中](../Page/五行.md "wikilink")，[東方之行爲木](../Page/東方.md "wikilink")，色青，所以青是東方的[正色](../Page/正色.md "wikilink")。[木尅](../Page/木.md "wikilink")[土](../Page/土.md "wikilink")，土是[黃色](../Page/黃色.md "wikilink")，青黃相間則爲[綠色](../Page/綠色.md "wikilink")，所以綠是東方的[間色](../Page/間色.md "wikilink")。\[20\]東方對應春季\[21\]，所以有“青春”之稱，後人代指人的青年時期。\[22\]

## 參見

  - [印刷四分色模式](../Page/印刷四分色模式.md "wikilink")

## 參考文獻

[Category:颜色](../Category/颜色.md "wikilink")
[Category:光谱](../Category/光谱.md "wikilink")

1.  [《釋名》](http://www.zdic.net/z/27/kx/9752.htm)：“青，生也，象物之生時色也。”

2.  符渝:青——生命的顏色，收錄於《文史知識》2014/6月刊

3.  《澄衷蒙學堂字課圖說》卷二，新星出版社

4.  [《說文解字》](http://www.zdic.net/z/27/sw/9752.htm)：“東方色也。木生火，从生丹。丹青之信言象然。凡青之屬皆从青。”

5.  [《孟子註疏/孟子註疏題辭解》](https://zh.wikisource.org/wiki/%E5%AD%9F%E5%AD%90%E8%A8%BB%E7%96%8F/%E5%AD%9F%E5%AD%90%E6%B3%A8%E7%96%8F%E9%A1%8C%E8%BE%AD%E8%A7%A3)：“孔注云︰朱，正色；紫，間色。案皇氏云︰青、赤、黃、白、黑，五方正色也。不正謂五方間色，綠、紅、碧、紫、駵黃是也。青是東方正，綠是東方間，東為木本，色青。木尅土，土色黃，並以所尅為間。故綠色，青、黃也。朱是南方正，紅是南方間，南為火，火色赤，火尅金，金色白，故紅色，赤、白也。白是西方正，碧是西方間，西為金，金色白，金尅木，故碧色，青、白也。黑是北方正，紫是北方間，北方水，水色黑，水尅火，火色赤，故紫色，赤、黑也。黃是中央正，亞黃是中央間，中央土，土色黃，土尅水，水色黑，故亞黃色，黃、黑也。是正間然。”

6.  [《說文解字注》](http://www.zdic.net/z/21/sw/7DA0.htm)綠色青黃也。...碧色白青也。

7.  [《臨川吳氏註》](http://www.zdic.net/z/22/kx/84BC.htm)蒼，深青色。

8.  [《禮記正義》卷十七](https://zh.m.wikisource.org/wiki/%E7%A6%AE%E8%A8%98%E6%AD%A3%E7%BE%A9/17)
    正義曰：春雲「載青旂，衣青衣，服蒼玉」，青深而蒼淺。

9.  [《注》](http://www.zdic.net/z/22/kx/8471.htm)《詩·小雅》有瑲蔥珩。《註》蔥，蒼也

10. [《爾雅·釋器》](http://www.zdic.net/z/22/kx/8471.htm)

11. [青 - 萌典](https://www.moedict.tw/%E9%9D%92)

12. [goo辞書](http://dictionary.goo.ne.jp/srch/all/%E9%9D%92/m0u/)

13. [《說文解字注》](http://www.zdic.net/z/27/sw/9752.htm)：“東方色也。考工記曰。東方謂之青。木生火。从生丹。丹、赤石也。赤、南方之色也。倉經切。十一部。丹青之信言必然。俗言信若丹青。謂其相生之理有必然也。援此以說从生丹之意。”

14. [《說文解字》](http://www.zdic.net/z/1e/sw/751F.htm)：“進也。象艸木生出土上。”

15. 《[章太炎全集](../Page/章太炎全集.md "wikilink")》：“生孳乳爲青，謂青如艸茲也。”，轉引自符渝:青——生命的顏色，收錄於《文史知識》2014/6月刊

16.
17.
18.
19.
20.
21. [《說文解字注》](http://www.zdic.net/z/1b/sw/6625.htm)鄉飮酒義曰。東方者春。

22.