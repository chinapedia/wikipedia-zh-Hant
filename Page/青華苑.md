[Chingwah.jpg](https://zh.wikipedia.org/wiki/File:Chingwah.jpg "fig:Chingwah.jpg")
[Unique_shop_in_Ching_Wah_Court.jpg](https://zh.wikipedia.org/wiki/File:Unique_shop_in_Ching_Wah_Court.jpg "fig:Unique_shop_in_Ching_Wah_Court.jpg")\]\]
[Ching_Wah_Court_Pond.jpg](https://zh.wikipedia.org/wiki/File:Ching_Wah_Court_Pond.jpg "fig:Ching_Wah_Court_Pond.jpg")
[Ching_Wah_Court_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Ching_Wah_Court_Basketball_Court.jpg "fig:Ching_Wah_Court_Basketball_Court.jpg")
[Ching_Wah_Court_Playground.jpg](https://zh.wikipedia.org/wiki/File:Ching_Wah_Court_Playground.jpg "fig:Ching_Wah_Court_Playground.jpg")
**青華苑**為[香港房屋委員會建成的](../Page/香港房屋委員會.md "wikilink")[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[葵青區的](../Page/葵青區.md "wikilink")[青衣島青芊街](../Page/青衣島.md "wikilink")22號，[長康邨第二期的對面](../Page/長康邨.md "wikilink")。屋苑分為兩期發展。第一期於1986年入伙，而第二期於1987年入伙。並由[富城物業管理有限公司負責管理](../Page/富城物業管理有限公司.md "wikilink")。

## 大廈資料

| 樓宇名稱\[1\] | 樓宇類型                               | 入伙日期\[2\] |
| --------- | ---------------------------------- | --------- |
| 華奐閣       | [風車型](../Page/風車型.md "wikilink")   | 1986年3月   |
| 華璇閣       |                                    |           |
| 華碧閣       | [新十字型](../Page/新十字型.md "wikilink") | 1987年5月   |
| 華豐閣       |                                    |           |
| 華欣閣       |                                    |           |
| 華翔閣       |                                    |           |
|           |                                    |           |

## 設施

青華苑設有以下設施：

  - 屋苑辦事處
  - 屋苑保安室
  - 停車場
  - OK便利店（營業時間：06:00-00:00）
  - 籃球場
  - 遊樂場
  - [青衣商會幼稚園](http://www.tytakg.edu.hk)（1986年創辦）（位於青華苑停車場大厦地下）

Ching Wah Court Jogging Path.jpg|緩跑徑 Ching Wah Court Covered
Walkway.jpg|有蓋行人通道 Ching Wah Court Sculpture.jpg|塑像 Ching Wah Court
Pavilion.jpg|涼亭 Ching Wah Court Playground (2).jpg|兒童遊樂場（2） Ching Wah
Court Playground (3).jpg|兒童遊樂場（3）

Ching Wah Court Playground (4).jpg|兒童遊樂場（4） Ching Wah Court Table Tennis
Zone.jpg|乒乓球檯 Ching Wah Court Chess Zone.jpg|棋藝區 Ching Wah Court Pebble
Walking Trail.jpg|卵石路步行徑 Ching Wah Court Car Park Roof.jpg|停車場天台休憩區
Ching Wah Court Car Park.jpg|停車場

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [青康路](../Page/青康路.md "wikilink")

<!-- end list -->

  - [青衣西路](../Page/青衣西路.md "wikilink")

</div>

</div>

## 相關條目

  - [香港居者有其屋列表](../Page/香港居者有其屋列表.md "wikilink")

## 資料來源

## 外部連結

  - [香港房屋委員會](http://www.housingauthority.gov.hk/)
  - [青華苑網頁](http://www.chingwahcourt.com.hk/)

[en:Public housing estates on Tsing Yi Island\#Ching Wah
Court](../Page/en:Public_housing_estates_on_Tsing_Yi_Island#Ching_Wah_Court.md "wikilink")

[Category:青衣島](../Category/青衣島.md "wikilink")

1.  [青華苑](http://www.housingauthority.gov.hk/b5/interactivemap/court/0,,3-0-6_5753,00.html)

2.  <http://www.rvd.gov.hk/doc/tc/nt_201504.pdf>