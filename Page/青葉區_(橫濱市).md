{{ 日本區

`| 自治體名=青葉區`

|圖像=[Motoishikawacho_(Yokohama)_01.jpg](https://zh.wikipedia.org/wiki/File:Motoishikawacho_\(Yokohama\)_01.jpg "fig:Motoishikawacho_(Yokohama)_01.jpg")
|圖像說明=元石川町北部[都市農業專用地中的](../Page/都市農業.md "wikilink")[桃園](../Page/桃.md "wikilink")

`| 區章=`
`| 日文原名=青葉区`
`| 平假名=あおばく`
`| 羅馬字拼音=Aoba-ku`
`| 都道府縣=神奈川縣`
`| 支廳=`
`| 市=橫濱市`
`| 編號=14117-8`
`| 面積=35.06`
`| 邊界未定=`
`| 人口=307,656 `
`| 統計時間=2013年8月1日`
`| 自治體=`[`橫濱市`](../Page/橫濱市.md "wikilink")`（`[`綠區`](../Page/綠區_\(橫濱市\).md "wikilink")`、`[`都築區`](../Page/都築區.md "wikilink")`）、`[`川崎市`](../Page/川崎市.md "wikilink")`（`[`宮前區`](../Page/宮前區.md "wikilink")`、`[`麻生區`](../Page/麻生區.md "wikilink")`）`
[`東京都`](../Page/東京都.md "wikilink")`：`[`町田市`](../Page/町田市.md "wikilink")
`| 樹=`
`| 花=`
`| 其他象徵物=`
`| 其他象徵=`
`| 郵遞區號=225-0024`
`| 所在地=青葉區市尾町31番地4`
`| 電話號碼=45-978-2323`
`| 外部連結=`<http://www.city.yokohama.lg.jp/aoba/>` `
`| 經度=`
`| 緯度=`
`| 地圖=Location of Aoba ward Yokohama city Kanagawa prefecture Japan.svg`

}}

**青葉區**（）是[橫濱市的](../Page/橫濱市.md "wikilink")18區之一。範圍為舊綠區（自[港北區分出](../Page/港北區_\(日本\).md "wikilink")）的北部。

## 交通

### 鐵路

  - [東京急行電鐵](../Page/東京急行電鐵.md "wikilink")

<!-- end list -->

  - [Tokyu_DT_line_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyu_DT_line_symbol.svg "fig:Tokyu_DT_line_symbol.svg")
    [田園都市線](../Page/田園都市線.md "wikilink")：[多摩廣場站](../Page/多摩廣場站.md "wikilink")
    - [薊野站](../Page/薊野站_\(神奈川縣\).md "wikilink") -
    [江田站](../Page/江田站_\(神奈川縣\).md "wikilink") -
    [市尾站](../Page/市尾站_\(神奈川縣\).md "wikilink") -
    [藤丘站](../Page/藤丘站_\(神奈川縣\).md "wikilink") -
    [青葉台站](../Page/青葉台站.md "wikilink") -
    [田奈站](../Page/田奈站.md "wikilink")
  - [Tokyu_KD_line_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyu_KD_line_symbol.svg "fig:Tokyu_KD_line_symbol.svg")
    [子供之國線](../Page/子供之國線.md "wikilink")：[恩田站](../Page/恩田站.md "wikilink")
    - [子供之國站](../Page/子供之國站_\(神奈川縣\).md "wikilink")

<!-- end list -->

  - [橫濱市交通局](../Page/橫濱市交通局.md "wikilink")（[橫濱市營地下鐵](../Page/橫濱市營地下鐵.md "wikilink")）

<!-- end list -->

  - [Yokohama_Municipal_Subway_Blue_Line_symbol.svg](https://zh.wikipedia.org/wiki/File:Yokohama_Municipal_Subway_Blue_Line_symbol.svg "fig:Yokohama_Municipal_Subway_Blue_Line_symbol.svg")
    [藍線](../Page/橫濱市營地下鐵藍線.md "wikilink")：薊野站

## 外部連結

[Category:橫濱市的區](../Category/橫濱市的區.md "wikilink")