[HK_Tsing_Yi_Estate.jpg](https://zh.wikipedia.org/wiki/File:HK_Tsing_Yi_Estate.jpg "fig:HK_Tsing_Yi_Estate.jpg")
[Tsing_Yi_Shopping_Centre.jpg](https://zh.wikipedia.org/wiki/File:Tsing_Yi_Shopping_Centre.jpg "fig:Tsing_Yi_Shopping_Centre.jpg")
[Tsing_Yi_Shopping_Centre_Level_1.jpg](https://zh.wikipedia.org/wiki/File:Tsing_Yi_Shopping_Centre_Level_1.jpg "fig:Tsing_Yi_Shopping_Centre_Level_1.jpg")
[Tsing_Yi_Estate_Community_Hall.jpg](https://zh.wikipedia.org/wiki/File:Tsing_Yi_Estate_Community_Hall.jpg "fig:Tsing_Yi_Estate_Community_Hall.jpg")
[Tsing_Yi_Estate_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Tsing_Yi_Estate_Basketball_Court.jpg "fig:Tsing_Yi_Estate_Basketball_Court.jpg")
是[香港公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[青衣島中部](../Page/青衣島.md "wikilink")，是青衣島內第三條落成的公共屋邨，前身是[青衣舊墟海邊](../Page/青衣舊墟.md "wikilink")，其中部分土地是由填海得來的\[1\]，也是少數有齊[Y2、Y3及Y4型設計大廈的公共屋邨](../Page/Y型大廈.md "wikilink")。青衣邨於1986年及1989年落成入伙，由[香港房屋委員會管理](../Page/香港房屋委員會.md "wikilink")，其後於2001年2月在[租者置其屋計劃](../Page/租者置其屋計劃.md "wikilink")（第四期）把單位出售給所屬租戶，現已成立[業主立案法團](../Page/業主立案法團.md "wikilink")。

## 屋邨資料

### 樓宇

| 樓宇名稱（座號）                | 樓宇類型                              | 入伙日期         |
| ----------------------- | --------------------------------- | ------------ |
| 宜居樓 （第1座）               | [Y2型](../Page/Y型大廈.md "wikilink") | 1986年6月（一期）  |
| 宜業樓 （第2座）               |                                   |              |
| 宜偉樓 （第3座）               | Y4型                               | 1989年10月（二期） |
| 宜逸樓 （第4座，一層有30個單位，非24個） | Y3型                               | 1989年7月（二期）  |
|                         |                                   |              |

### 學校

青衣邨內有一所幼兒園、兩所小學及一所中學。

  - [基督教香港信義會靈工幼兒學校](http://lkns.elchk.org.hk/)（1988年創辦）（宜居樓地下）
  - [荃灣商會學校](http://www.twtaps.edu.hk)（1986年創辦）
  - [仁濟醫院趙曾學韞小學](http://www.ychcthwps.edu.hk)（1989年創辦）
  - [明愛聖若瑟中學](../Page/明愛聖若瑟中學.md "wikilink")

<!-- end list -->

  - *已結束*

<!-- end list -->

  - 新界婦孺福利會梁省德幼稚園（青衣村）（1986年創辦）（位於青衣邨宜業樓地下）

### 設施

青衣邨內設有多項設施，包括多層停車場、[社區會堂](../Page/社區會堂.md "wikilink")、體育館、商場、籃球場及兒童遊樂場。屋苑地下設有[巴士總站](../Page/青衣邨巴士總站.md "wikilink")，並且有樓梯連接上蓋屋邨及行人天橋網絡（由青衣邨二期開始至[青衣碼頭](../Page/青衣碼頭.md "wikilink")）。

在公共設施方面，青衣商場佔地約7,300平方米\[2\]。商場地下曾經設有街市，主要以售賣雜貨及凍肉為主。2014年商場大部份商店關閉，只餘下1間超級市場及3間[冬菇亭熟食檔經營](../Page/冬菇亭.md "wikilink")。2014年至2015年商場進行大翻新，並將地下街市及1樓熟食檔改作商舖。

HK Tsing Yi Estate Platform.jpg|屋邨平台 Tsing Yi Estate Children's
Playground.jpg|兒童遊樂場 Tsing Yi Estate Children's Playground
(2).jpg|兒童遊樂場（2） HK Tsing Yi Estate Garden.jpg|兒童遊樂場（3）
Tsing Yi Estate Children's Playground (4) and Gym Zone.jpg|兒童遊樂場（4）及健體區
HK Tsing Yi Estate Bus Terminal.jpg|青衣邨巴士總站

Tsing Yi Carpark.jpg|青衣停車場 Fung Shue Wo Sports Centre.jpg|楓樹窩體育館 HK CSJ
Secondary School.jpg|明愛聖若瑟中學 HK Tsing Yi Estate Shopping
Centre.jpg|青衣邨商場（2008年） HK Tsing Yi Estate Vacant
Mushroom.jpg|部份空置熟食亭（2008年）

## 著名居民

  - [林莉](../Page/林莉_\(藝人\).md "wikilink")：香港藝人

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{東涌綫色彩}}">█</font><a href="../Page/東涌綫.md" title="wikilink">東涌綫</a>、<font color={{機場快綫色彩}}>█</font><a href="../Page/機場快綫.md" title="wikilink">機場快綫</a>：<a href="../Page/青衣站.md" title="wikilink">青衣站</a>（步行10分鐘）</li>
</ul>
<dl>
<dt><a href="../Page/青綠街.md" title="wikilink">青綠街</a></dt>

</dl>
<dl>
<dt><a href="../Page/楓樹窩路.md" title="wikilink">楓樹窩路</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參考資料及注釋

## 外部連結

  - [房屋署青衣邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2822)
  - [青衣商場簡介](http://www.thelinkreit.com/TC/leasing/Pages/Property-Locator-Details.aspx?pid=63)

[en:Public housing estates on Tsing Yi Island\#Tsing Yi
Estate](../Page/en:Public_housing_estates_on_Tsing_Yi_Island#Tsing_Yi_Estate.md "wikilink")

[Category:青衣島](../Category/青衣島.md "wikilink")
[Category:租者置其屋計劃屋邨](../Category/租者置其屋計劃屋邨.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.  [1](http://www.discuss.com.hk/viewthread.php?tid=9732145)
2.  [領展管理青衣商場簡介](http://www.thelinkreit.com/TC/leasing/Pages/Property-Locator-Details.aspx?pid=63)