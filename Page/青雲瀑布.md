**青雲瀑布**位於[嘉義縣](../Page/嘉義縣.md "wikilink")[大埔鄉和平村](../Page/大埔鄉.md "wikilink")，[曾文溪水系](../Page/曾文溪.md "wikilink")，當地人又稱為「榮華山瀑布」，為[西拉雅國家風景區內著名的瀑布景觀](../Page/西拉雅國家風景區.md "wikilink")。早前鄒族少年要從瀑布上往下跳才算是成年，現在成了情侶約會之處，加上水量充沛時從頂傾洩而下的水量伴隨隆隆的水聲，當流水匯合為一時，就像似情侶們牽手瀑布底下見證一樣，故又稱為「情人瀑布」。青雲瀑布地質上為砂岩、頁岩，由於兩者的岩性差異，使瀑布受蝕程度不同，故瀑布上方岩層較下方突出。由[台三線行駛至](../Page/台三線.md "wikilink")[大埔橋附近](../Page/大埔橋.md "wikilink")，轉接青山產業道路進入，於路旁即可眺望瀑布。青雲瀑布水量豐沛、水潭廣闊，適合夏日遊玩戲水。

## 交通資訊

  - 自行開車：由[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[國道3號下中埔交流道轉](../Page/福爾摩沙高速公路.md "wikilink")[TW_PHW18.svg](https://zh.wikipedia.org/wiki/File:TW_PHW18.svg "fig:TW_PHW18.svg")[台18線](../Page/台18線.md "wikilink")(阿里山公路)往中埔方向至十字路，右轉[TW_PHW3.svg](https://zh.wikipedia.org/wiki/File:TW_PHW3.svg "fig:TW_PHW3.svg")[台3線至大埔拱橋旁](../Page/台3線.md "wikilink")，轉[嘉129-1線青山產業道路依循茶山指標前行](../Page/嘉129-1線.md "wikilink")500公尺，即可抵達。
  - 大眾運輸：搭乘台鐵至嘉義火車站轉乘嘉義縣公車處7301線【嘉義-大埔-嘉義農場】至大埔橋站下車。

## 參考資料

  -
## 外部連結

  - [在Youtube的青雲瀑布影像](https://www.youtube.com/watch?v=NP0jrVqHQpw)
  - [在Panoramio的青雲瀑布影像](http://www.panoramio.com/photo/49346233)

[category:台灣瀑布](../Page/category:台灣瀑布.md "wikilink")

[Category:嘉義縣地理](../Category/嘉義縣地理.md "wikilink")
[Category:嘉義縣旅遊景點](../Category/嘉義縣旅遊景點.md "wikilink")
[Category:大埔鄉](../Category/大埔鄉.md "wikilink")