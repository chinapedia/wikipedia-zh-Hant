**青鳥**、**藍鳥**、**BLUE BIRD**可以指：

## 樂曲

  - 《[青鳥](../Page/青鳥_\(生物股長單曲\).md "wikilink")》日本樂隊[生物股長的第](../Page/生物股長.md "wikilink")10張單曲
  - [BLUE
    BIRD](../Page/BLUE_BIRD.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")[濱崎步第](../Page/濱崎步.md "wikilink")40張單曲。
  - [Blue Bird
    (可苦可樂單曲)](../Page/Blue_Bird_\(可苦可樂單曲\).md "wikilink")，[可苦可樂的單曲](../Page/可苦可樂.md "wikilink")。
  - blue bird，[松浦亞彌的單曲](../Page/松浦亞彌.md "wikilink")。
  - blue
    bird，[Cocco的歌曲](../Page/Cocco.md "wikilink")。電影《[VITAL](../Page/VITAL.md "wikilink")》片尾主題曲。
  - Bluebird，[今井美樹的單曲](../Page/今井美樹.md "wikilink")（1993年發售）。
  - Blue Bird，[EMI
    MARIA的單曲](../Page/EMI_MARIA.md "wikilink")。收錄於音樂專輯《BLUE
    BIRD》中。
  - Blue Bird，電視動畫《[東京喵喵](../Page/東京喵喵.md "wikilink")》的角色歌曲。

## 專輯

  - Blue Bird，[坂本麗衣的音樂專輯](../Page/坂本麗衣.md "wikilink")。
  - BLUE BIRD，[EMI MARIA的](../Page/EMI_MARIA.md "wikilink")2011年音樂專輯。

## 戲劇、電視劇、電影

  - [青鸟
    (戏剧)](../Page/青鸟_\(戏剧\).md "wikilink")，[莫里斯·梅特林克的代表作](../Page/莫里斯·梅特林克.md "wikilink")

  -
  -
  -
  -
  -
  -
  - [青鳥
    (電視劇)](../Page/青鳥_\(電視劇\).md "wikilink")，日本[TBS電視台自](../Page/TBS.md "wikilink")1997年10月10日至1997年12月19日播放的[TBS週五連續劇](../Page/TBS週五連續劇.md "wikilink")，共11集

## 組織

  - [青鳥
    (香港教育學院)](../Page/青鳥_\(香港教育學院\).md "wikilink")，[香港教育學院的](../Page/香港教育學院.md "wikilink")[環保學會及](../Page/環保.md "wikilink")[綠色大專聯盟的成員](../Page/綠色大專聯盟.md "wikilink")
  - [青鳥
    (團體)](../Page/青鳥_\(團體\).md "wikilink")，[香港一個關注](../Page/香港.md "wikilink")[性工作者權益的團體](../Page/性工作者.md "wikilink")
  - [北大青鳥](../Page/北大青鳥.md "wikilink")，[北京大學旗下的子公司](../Page/北京大學.md "wikilink")

## 動植物

  - [三寶鳥](../Page/三寶鳥.md "wikilink")
  - [三足烏](../Page/三足烏.md "wikilink")
  - [日本蓝鸟](../Page/日本蓝鸟.md "wikilink")，多浆植物，景天科，拟石莲花属
  - [东蓝鸲](../Page/东蓝鸲.md "wikilink")、[山蓝鸲](../Page/山蓝鸲.md "wikilink")、[西蓝鸲等](../Page/西蓝鸲.md "wikilink")，美国的一类鸟种

## 其他

  - [日产蓝鸟](../Page/日产蓝鸟.md "wikilink")，[日產汽車所生產的一個車型](../Page/日產汽車.md "wikilink")
  - [风水的一种別稱](../Page/风水.md "wikilink")