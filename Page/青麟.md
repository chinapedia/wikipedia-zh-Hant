**青麟**（），[字](../Page/表字.md "wikilink")**墨卿**，图们氏，[满洲](../Page/满洲.md "wikilink")[正白旗人](../Page/正白旗.md "wikilink")。[道光二十一年](../Page/道光.md "wikilink")（1841年）进士，选[庶吉士](../Page/庶吉士.md "wikilink")，授[编修](../Page/编修.md "wikilink")，迁[中允擢](../Page/中允.md "wikilink")[侍讲](../Page/侍讲.md "wikilink")。五迁至[内阁学士](../Page/内阁学士.md "wikilink")。督[江苏](../Page/江苏.md "wikilink")[學政有官譽](../Page/學政.md "wikilink")。[咸丰二年](../Page/咸丰.md "wikilink")，擢[户部侍郎](../Page/户部侍郎.md "wikilink")。督催丰北塞决工程。三年出督[湖北](../Page/湖北.md "wikilink")[学政](../Page/学政.md "wikilink")（相當于省教育廳長），调[礼部侍郎](../Page/礼部侍郎.md "wikilink")。

## [德安守功](../Page/德安.md "wikilink")

  - 时[太平軍由](../Page/太平軍.md "wikilink")[江西回窜](../Page/江西.md "wikilink")[湖北](../Page/湖北.md "wikilink")，青麟按试[德安](../Page/德安.md "wikilink")，闻戰報停试，督率[知府易容之募](../Page/知府.md "wikilink")[乡勇筹防守](../Page/乡勇.md "wikilink")，府城获全。疏陈军事，请[湖北](../Page/湖北.md "wikilink")、[江西](../Page/江西.md "wikilink")、[安徽三省合剿](../Page/安徽.md "wikilink")，以期得力。
  - 四年，授[湖北巡抚](../Page/湖北巡抚.md "wikilink")。城中兵仅千人，[荆州将军台涌署](../Page/荆州.md "wikilink")[湖廣总督](../Page/湖廣总督.md "wikilink")，未到任；而[太平軍由](../Page/太平軍.md "wikilink")[黄州进至](../Page/黄州.md "wikilink")[汉阳](../Page/汉阳.md "wikilink")、[汉口](../Page/汉口.md "wikilink")，渡江欲第二次扑攻[武昌](../Page/武昌.md "wikilink")。青麟督[总兵杨昌泗](../Page/总兵.md "wikilink")、[游击](../Page/游击.md "wikilink")[侯凤岐与](../Page/侯凤岐.md "wikilink")[副都统魁玉水陆合击](../Page/副都统.md "wikilink")，擊退[太平軍](../Page/太平軍.md "wikilink")；复败之豹子海、鲁家港，毁贼垒五。

## [武昌省城失守](../Page/武昌.md "wikilink")

  - 已而[太平軍扑塘角](../Page/太平軍.md "wikilink")、鲇鱼套，逼攻省城，青麟在[武昌武胜门督战](../Page/武昌.md "wikilink")，城中忽火起，土匪内应，兵尽溃，[武昌第二次失守](../Page/武昌.md "wikilink")。青麟将自殺，众阻止自殺擁之逃到[长沙](../Page/长沙.md "wikilink")，折赴[荆州](../Page/荆州.md "wikilink")。

## [咸豐怒斬](../Page/咸豐.md "wikilink")

  - 初，[清文宗闻青麟出家財犒军](../Page/清文宗.md "wikilink")，甚嘉之，至是愤武昌屡失，弃城越境，罪尤重，诏曰：“青麟简任封圻，正当贼匪充斥，武昌兵单饷匮。朕以其任学政时保守德安，念其勤劳，畀以重任。省垣布置，屡次击贼获胜。八十馀日之中，困苦艰难，所奏原无虚假，朕方严催援兵接应。六月初间，魁玉、杨昌泗等连破贼营，但能激厉力战，何致遽陷？**婴城固守，解围有日，犹将宥过论功。纵力尽捐躯，褒忠有典，岂不心迹光明？**乃仓皇远避，径赴长沙，直是弃城而逃。**长沙非所辖之地，越境偷生，何词以解？若再加宽典，是疆臣守土之责，几成具文，何以对死事诸臣耶！**朕赏罚一秉大公，岂能以前此微劳，稍从末减？俟到荆州时，交[官文传旨正法](../Page/官文.md "wikilink")。”遂令[官文斬](../Page/官文.md "wikilink")**青麟**。
  - 逝後，[曾國藩為青麟抱不平](../Page/曾國藩.md "wikilink")。

## 參考

  - 《清史稿》

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:內閣學士](../Category/內閣學士.md "wikilink")
[Category:清朝江蘇學政](../Category/清朝江蘇學政.md "wikilink")
[Category:清朝湖北學政](../Category/清朝湖北學政.md "wikilink")
[Category:清朝湖北巡撫](../Category/清朝湖北巡撫.md "wikilink")
[Category:清朝被處決者](../Category/清朝被處決者.md "wikilink")
[Category:滿洲正白旗人](../Category/滿洲正白旗人.md "wikilink")