**青龙**（350年正月-闰二月）是[十六国时期](../Page/十六国.md "wikilink")[后赵政权第六任君主](../Page/后赵.md "wikilink")[石鑒的](../Page/石鑒.md "wikilink")[年号](../Page/年号.md "wikilink")，共计三個月。

后赵將領[石閔發動](../Page/石閔.md "wikilink")[政變推翻石鑒](../Page/政變.md "wikilink")，建立[冉魏](../Page/冉魏.md "wikilink")，改元[永兴元年](../Page/永兴.md "wikilink")。后赵新兴王[石祗听说石鉴被冉闵杀死](../Page/石祗.md "wikilink")，于是自立，350年三月改元[永宁](../Page/永宁.md "wikilink")。

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|青龙||元年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||350年 |-
style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")|||[庚戌](../Page/庚戌.md "wikilink")
|}

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[青龙年号](../Page/青龙.md "wikilink")
  - 同期存在的其他政权年号
      - [永和](../Page/永和_\(晋穆帝\).md "wikilink")（345年-356年）：东晋皇帝[晉穆帝司馬聃的年号](../Page/晉穆帝.md "wikilink")
      - [建兴](../Page/建兴.md "wikilink")：[前凉政权年号](../Page/前凉.md "wikilink")
      - [建国](../Page/建国.md "wikilink")（338年十一月-376年）：[代政权](../Page/代国.md "wikilink")[拓跋什翼犍年号](../Page/拓跋什翼犍.md "wikilink")
      - [永兴](../Page/永兴_\(冉闵\).md "wikilink")（350年閏二月—352年四月）：[冉魏政权](../Page/冉魏.md "wikilink")[冉闵年号](../Page/冉闵.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:后赵年号](../Category/后赵年号.md "wikilink")
[Category:350年代中国政治](../Category/350年代中国政治.md "wikilink")
[Category:350年](../Category/350年.md "wikilink")