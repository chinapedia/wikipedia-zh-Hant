**青龍电影奖**（；）是由[朝鮮日報自](../Page/朝鮮日報.md "wikilink")1963年起舉辦的[韓國年度](../Page/韓國.md "wikilink")[電影頒獎典禮](../Page/電影.md "wikilink")。它是韩国最具影响力和权威的电影颁奖礼之一，与韩国电影[大钟奖和韩国](../Page/大钟奖.md "wikilink")[百想艺术大赏并称韩国三大影视奖](../Page/百想艺术大赏.md "wikilink")。

曾經在1974-1989年停止頒獎典禮。直到1990年開始復辦。

青龍电影奖只會評審過去一年的大片或有高度藝術價值的受歡迎影片。在選拔過程中，約有40部的影片會被選出作為提名作品，並免費供公眾欣賞。在篩選完成後，頒獎典禮便隨之開幕。

## 獎項

  - 最優秀作品奖
  - 監督奖
  - 新人監督奖
  - 男主角奖
  - 女主角奖
  - 男助演（配角）奖
  - 女助演（配角）奖
  - 新人男演員奖
  - 新人女演員奖

<!-- end list -->

  - 攝影奖
  - 音樂奖
  - 美術奖
  - 技術奖
  - 腳本奖
  - 照明奖
  - 最高觀影數影片奖
  - 人氣明星奖（由網民投票選出）

此外，每年頒獎典禮舉行前一星期，上一屆獲獎者會聚集一起做打手印儀式，寓意其得獎榮譽直到永遠。

## 歷屆獲獎者

| 历届獲獎名單      |
| ----------- |
| 年度（届数）      |
| 1963年（第1届）  |
| 1964年（第2届）  |
| 1965年（第3届）  |
| 1966年（第4届）  |
| 1967年（第5届）  |
| 1969年（第6届）  |
| 1970年（第7届）  |
| 1971年（第8届）  |
| 1972年（第9届）  |
| 1973年（第10届） |
| 1990年（第11届） |
| 1991年（第12届） |
| 1992年（第13届） |
| 1993年（第14届） |
| 1994年（第15届） |
| 1995年（第16届） |
| 1996年（第17届） |
| 1997年（第18届） |
| 1998年（第19届） |
| 1999年（第20届） |
| 2000年（第21届） |
| 2001年（第22届） |
| 2002年（第23届） |
| 2003年（第24届） |
| 2004年（第25届） |
| 2005年（第26届） |
| 2006年（第27届） |
| 2007年（第28届） |
| 2008年（第29届） |
| 2009年（第30届） |
| 2010年（第31届） |
| 2011年（第32届） |
| 2012年（第33届） |
| 2013年（第34届） |
| 2014年（第35届） |
| 2015年（第36届） |
| 2016年（第37届） |
| 2017年（第38届） |
| 2018年（第39届） |

## 歷屆獲獎名單

### 2012年

2012年11月30日，第33届韩国电影青龙奖在韩国首尔举行颁奖礼，金惠秀、刘俊相主持。《[圣殇](../Page/圣殇.md "wikilink")》获得最佳影片奖，[崔岷植](../Page/崔岷植.md "wikilink")、[林秀晶分封帝后](../Page/林秀晶.md "wikilink")，[曹政奭](../Page/曹政奭.md "wikilink")、[金高恩当选最佳新人](../Page/金高恩.md "wikilink")，而[郑智泳导演则凭借](../Page/郑智泳.md "wikilink")《[断箭](../Page/断箭.md "wikilink")》获封最佳导演，[河正宇](../Page/河正宇.md "wikilink")、[金秀賢](../Page/金秀贤_\(男演員\).md "wikilink")、[裴秀智](../Page/裴秀智.md "wikilink")、[孔曉振获人气明星奖](../Page/孔曉振.md "wikilink")。《[双面君王](../Page/双面君王.md "wikilink")》获得最佳美术奖。\[1\]

  - 最佳影片：[圣殇](../Page/圣殇.md "wikilink")
  - 最佳導演：[鄭智泳](../Page/鄭智泳.md "wikilink")《[斷箭](../Page/斷箭.md "wikilink")》
  - 最佳男演員：[崔岷植](../Page/崔岷植.md "wikilink")《[與犯罪的戰爭](../Page/與犯罪的戰爭.md "wikilink")》
  - 最佳女演員：[林秀晶](../Page/林秀晶.md "wikilink")《[我妻子的一切](../Page/我妻子的一切.md "wikilink")》
  - 最佳男配角：[柳承龍](../Page/柳承龍.md "wikilink")《我妻子的一切》
  - 最佳女配角：[文晶熙](../Page/文晶熙.md "wikilink")《[鐵線蟲](../Page/鐵線蟲_\(電影\).md "wikilink")》
  - 最佳新人男演員：[曹政奭](../Page/曹政奭.md "wikilink")《[建築學概論](../Page/建築學概論.md "wikilink")》
  - 最佳新人女演員：[金高恩](../Page/金高恩.md "wikilink")《[銀嬌](../Page/銀嬌.md "wikilink")》
  - 最佳新人導演：金洪宣《[共謀者](../Page/共謀者.md "wikilink")》
  - 最佳攝影獎：金泰景《銀嬌》
  - 最佳照明獎：洪勝哲《銀嬌》
  - 最佳配樂獎：趙英鬱《與犯罪的戰爭》
  - 最佳美術設計獎：吳興錫《[光海，成為王的男人](../Page/光海，成為王的男人.md "wikilink")》
  - 最佳劇本獎：尹鐘彬《與犯罪的戰爭》
  - 最佳技術獎：劉尚燮、鄭允憲《[盜賊門](../Page/盜賊門.md "wikilink")》
  - 人氣電影：《盜賊門》
  - 人氣明星獎：[河正宇](../Page/河正宇.md "wikilink")《與犯罪的戰爭》、[裴秀智](../Page/裴秀智.md "wikilink")《建築學概論》、[金秀賢](../Page/金秀賢_\(男演員\).md "wikilink")《盜賊門》、[孔曉振](../Page/孔曉振.md "wikilink")《愛情小說》

### 2013年

  - 最佳影片：[素媛](../Page/素媛.md "wikilink")
  - 最佳導演：[奉俊昊](../Page/奉俊昊.md "wikilink")《[雪國列車](../Page/雪國列車.md "wikilink")》
  - 最佳男主角：[黃政民](../Page/黃政民.md "wikilink")《[新世界](../Page/新世界_\(2013年電影\).md "wikilink")》
  - 最佳女主角：[韓孝周](../Page/韓孝周.md "wikilink")《[監視者們](../Page/監視者們.md "wikilink")》
  - 最佳男配角：[李政宰](../Page/李政宰.md "wikilink")《[觀相](../Page/觀相.md "wikilink")》
  - 最佳女配角：[羅美蘭](../Page/羅美蘭.md "wikilink")《許願》
  - 最佳新人男演員：[呂珍九](../Page/呂珍九.md "wikilink")《[華頤：吞噬怪物的孩子](../Page/華頤：吞噬怪物的孩子.md "wikilink")》
  - 最佳新人女演員：[朴智秀](../Page/朴智秀.md "wikilink")《[虎尾蘭之夢](../Page/虎尾蘭之夢.md "wikilink")》
  - 最佳新人導演：金秉宇《[恐怖直播](../Page/恐怖直播.md "wikilink")》
  - 最佳攝影：《[柏林](../Page/柏林_\(電影\).md "wikilink")》
  - 最佳照明：《柏林》
  - 最佳音樂：《華頤：吞噬怪物的孩子》
  - 最佳美術：《雪國列車》
  - 最佳技術獎：《[王牌巨猩](../Page/王牌巨猩.md "wikilink")》
  - 最佳劇本：金智慧、[曹重勳](../Page/曹重勳.md "wikilink")《許願》
  - 年度最賣座電影獎：《[七號房的禮物](../Page/七號房的禮物.md "wikilink")》
  - 年度人氣獎：[李秉憲](../Page/李秉憲.md "wikilink")、[薛景求](../Page/薛景求.md "wikilink")、[孔曉振](../Page/孔曉振.md "wikilink")、[金敏喜](../Page/金敏喜.md "wikilink")

### 2014年

  - 最佳影片：[辩护人](../Page/辩护人_\(电影\).md "wikilink")
  - 最佳導演：《[鳴梁](../Page/鳴梁_\(電影\).md "wikilink")》
  - 最佳男主角：[宋康昊](../Page/宋康昊.md "wikilink")《[辩护人](../Page/辩护人_\(电影\).md "wikilink")》
  - 最佳女主角：[千玗嬉](../Page/千玗嬉.md "wikilink")《[韩公主](../Page/韩公主.md "wikilink")》
  - 最佳男配角：[赵镇雄](../Page/赵镇雄.md "wikilink")《走到尽头》
  - 最佳女配角：[金姈爱](../Page/金姈爱.md "wikilink")《辩护人》
  - 最佳新人男演員：[朴有天](../Page/朴有天.md "wikilink")《海雾》
  - 最佳新人女演員：[金赛纶](../Page/金赛纶.md "wikilink")《道熙呀》

### 2015年

  - 最佳影片：[暗殺](../Page/暗殺_\(2015年電影\).md "wikilink")
  - 最佳導演：[柳承元](../Page/柳承元.md "wikilink")《[老手](../Page/老手.md "wikilink")》
  - 最佳男主角：[劉亞仁](../Page/劉亞仁.md "wikilink")《[思悼](../Page/思悼.md "wikilink")》
  - 最佳女主角：[李贞贤](../Page/李贞贤.md "wikilink")《[诚实国度的爱丽丝](../Page/诚实国度的爱丽丝.md "wikilink")》
  - 最佳男配角：[吳達庶](../Page/吳達庶.md "wikilink")《[國際市場](../Page/國際市場.md "wikilink")》
  - 最佳女配角：[全慧珍](../Page/全慧珍.md "wikilink")《[思悼](../Page/思悼.md "wikilink")》
  - 最佳新人導演：[金泰勇](../Page/金泰勇.md "wikilink")《[巨人](../Page/巨人.md "wikilink")》
  - 最佳新人男演員：[崔宇植](../Page/崔宇植.md "wikilink")《[巨人](../Page/巨人.md "wikilink")》
  - 最佳新人女演員：[李俞英](../Page/李俞英.md "wikilink")《[姦臣](../Page/姦臣_\(韓國電影\).md "wikilink")》
  - 最佳攝像照明：《[思悼](../Page/思悼.md "wikilink")》
  - 最佳配樂：《[思悼](../Page/思悼.md "wikilink")》
  - 最佳剪輯：《[Beauty Inside](../Page/Beauty_Inside.md "wikilink")》
  - 最佳美術：《[國際市場](../Page/國際市場.md "wikilink")》
  - 最佳技術：《[暗殺](../Page/暗殺_\(2015年電影\).md "wikilink")》
  - 最佳劇本：《[少數意見](../Page/少數意見.md "wikilink")》
  - 最高觀影數影片：《[國際市場](../Page/國際市場.md "wikilink")》
  - 人氣明星獎：[李敏鎬](../Page/李敏鎬.md "wikilink")、[朴敘俊](../Page/朴敘俊.md "wikilink")、[朴寶英](../Page/朴寶英.md "wikilink")、[金雪炫](../Page/金雪炫.md "wikilink")
  - 最佳短片：《The Photographers》

### 2016年

  - 最佳影片：[局内者们](../Page/局内者们.md "wikilink")
  - 最佳導演：[羅宏镇](../Page/羅宏镇.md "wikilink")《[哭聲](../Page/哭聲_\(電影\).md "wikilink")》
  - 最佳男主角：[李秉憲](../Page/李秉憲.md "wikilink")《[局内者们](../Page/局内者们.md "wikilink")》
  - 最佳女主角：[金敏喜](../Page/金敏喜.md "wikilink")《[小姐](../Page/下女的诱惑.md "wikilink")》
  - 最佳男配角：《[哭聲](../Page/哭聲_\(電影\).md "wikilink")》
  - 最佳女配角：[朴素淡](../Page/朴素淡.md "wikilink")《[黑祭司們](../Page/黑祭司們.md "wikilink")》
  - 最佳新人導演：《》
  - 最佳新人男演員：[朴正民](../Page/朴正民.md "wikilink")《》
  - 最佳新人女演員：[金泰梨](../Page/金泰梨.md "wikilink")《[小姐](../Page/下女的诱惑.md "wikilink")》
  - 最佳攝影：《[阿修罗](../Page/阿修罗_\(電影\).md "wikilink")》
  - 最佳剪輯：《[哭聲](../Page/哭聲_\(電影\).md "wikilink")》
  - 最佳劇本：《》
  - 最佳美術：《[小姐](../Page/下女的诱惑.md "wikilink")》
  - 最佳音樂：《[哭聲](../Page/哭聲_\(電影\).md "wikilink")》
  - 最佳技術：《[釜山行](../Page/釜山行.md "wikilink")》（特殊化妆）
  - 最高觀影數影片：《[釜山行](../Page/釜山行.md "wikilink")》
  - 人氣明星獎：[鄭雨盛](../Page/鄭雨盛.md "wikilink")、、[孫藝珍](../Page/孫藝珍.md "wikilink")、[裴斗娜](../Page/裴斗娜.md "wikilink")
  - 最佳短片：《夏夜》

### 2017年

  - 最佳電影：《[我只是個計程車司機](../Page/我只是個計程車司機.md "wikilink")》
  - 最佳導演：金賢錫《[花漾奶奶秀英文](../Page/I_Can_Speak.md "wikilink")》
  - 最佳男主角：[宋康昊](../Page/宋康昊.md "wikilink")《[我只是個計程車司機](../Page/我只是個計程車司機.md "wikilink")》
  - 最佳女主角：[羅文姬](../Page/羅文姬.md "wikilink")《[花漾奶奶秀英文](../Page/I_Can_Speak.md "wikilink")》
  - 最佳男配角：陳善奎《犯罪都市》
  - 最佳女配角：金素貞《[THE KING](../Page/The_King_\(2017年電影\).md "wikilink")》
  - 最佳男子新人獎：[都敬秀](../Page/都敬秀.md "wikilink")《[哥哥](../Page/哥哥_\(電影\).md "wikilink")》
  - 最佳女子新人獎：[崔嬉序](../Page/崔嬉序.md "wikilink")《[朴烈](../Page/朴烈.md "wikilink")》
  - 最佳新人導演獎：李賢珠《戀愛談》
  - 攝影獎：《不汗黨：地下秩序》
  - 剪輯獎：《[THE KING](../Page/The_King_\(2017年電影\).md "wikilink")》
  - 劇本獎：《南漢山城》
  - 美術獎：《[軍艦島](../Page/軍艦島_\(電影\).md "wikilink")》
  - 音樂獎：《[我只是個計程車司機](../Page/我只是個計程車司機.md "wikilink")》
  - 技術獎：《惡女》（特技動作）
  - 最高觀影次數獎：《[我只是個計程車司機](../Page/我只是個計程車司機.md "wikilink")》
  - 人氣明星獎：[金秀安](../Page/金秀安.md "wikilink")、[薛景求](../Page/薛景求.md "wikilink")、[羅文姬](../Page/羅文姬.md "wikilink")、[趙寅成](../Page/趙寅成.md "wikilink")

### 2018年

  - 最佳電影：《[1987：黎明到來的那一天](../Page/1987：黎明到來的那一天.md "wikilink")》
  - 最佳導演：《[北風](../Page/北風_\(2018年電影\).md "wikilink")》
  - 最佳男主角：[金允錫](../Page/金允錫.md "wikilink")《[1987：黎明到來的那一天](../Page/1987：黎明到來的那一天.md "wikilink")》
  - 最佳女主角：[韓志旼](../Page/韓志旼.md "wikilink")《[白小姐](../Page/白小姐.md "wikilink")》
  - 最佳男配角：[金柱赫](../Page/金柱赫.md "wikilink")《[信徒](../Page/信徒_\(電影\).md "wikilink")》
  - 最佳女配角：[金香起](../Page/金香起.md "wikilink")《[與神同行：最終審判](../Page/與神同行：最終審判.md "wikilink")》
  - 最佳男子新人獎：[南柱赫](../Page/南柱赫.md "wikilink")《[安市城](../Page/安市城_\(電影\).md "wikilink")》
  - 最佳女子新人獎：《》
  - 最佳新人導演獎：全高雲《》
  - 攝影獎：金宇亨、金勝奎《[1987：黎明到來的那一天](../Page/1987：黎明到來的那一天.md "wikilink")》
  - 剪輯獎：金亨朱、鄭凡植、楊東燁《[鬼病院：靈異直播](../Page/鬼病院：靈異直播.md "wikilink")》
  - 劇本獎：、金泰均《》
  - 美術獎：朴一賢《[北風](../Page/北風_\(2018年電影\).md "wikilink")》
  - 音樂獎：《[信徒](../Page/信徒_\(電影\).md "wikilink")》
  - 技術獎：陳宗賢《[與神同行：最終審判](../Page/與神同行：最終審判.md "wikilink")》（視覺效果）
  - 最佳短篇電影獎：許智恩、李慶浩《新紀錄》
  - 最高觀影次數獎：《[與神同行](../Page/與神同行_\(電影\).md "wikilink")》
  - 人氣明星獎：[金英光](../Page/金英光.md "wikilink")、、[朱智勛](../Page/朱智勛.md "wikilink")、[金香起](../Page/金香起.md "wikilink")

## 參考資料

  - [The 27th Blue Dragon Awards at "Tracking the Blue Dragon
    Dumplings"](https://web.archive.org/web/20101130075033/http://www.koreasociety.org/content/view/287/86)

  - [韓國电影－青龍电影奖](http://www.krmdb.com/26BlueDragon-Result.b5.shtml)

## 外部链接

  -
[C](../Category/韩国电影奖项.md "wikilink")
[青龙电影奖](../Category/青龙电影奖.md "wikilink")

1.  [青龙奖《圣殇》获最佳 林秀晶、崔岷植分封帝后](http://ent.qq.com/a/20121201/000057.htm)