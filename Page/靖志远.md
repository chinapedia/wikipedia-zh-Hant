**靖志远**（），生于[山东](../Page/山东.md "wikilink")[临沭](../Page/临沭.md "wikilink")，少年迁入[湖北](../Page/湖北.md "wikilink")[襄樊](../Page/襄樊.md "wikilink")（今[襄阳市](../Page/襄阳市.md "wikilink")）。[中国人民解放军上将](../Page/中国人民解放军上将.md "wikilink")。是中共第十六、十七届中央委员。

## 简历

  - 1963年8月，参军。
  - 1985年11月至1993年2月，任[第二炮兵基地參謀長](../Page/第二炮兵.md "wikilink")、副司令員。
  - 1990年7月，晉升少將。
  - 1993年2月至1999年2月，任第二炮兵基地司令員。
  - 1999年2月至2003年1月，任第二炮兵參謀長、第二炮兵黨委常委。
  - 2000年7月，晉升中將。
  - 2003年1月，复任第二炮兵司令員。
  - 2004年9月，当选[中央军委委員](../Page/中央军委.md "wikilink")。
  - 2004年9月25日，晉升为[上將](../Page/上將.md "wikilink")。
  - 2012年10月，不再担任第二炮兵部队司令员，由原[中国人民解放军副总参谋长](../Page/中国人民解放军副总参谋长.md "wikilink")[魏凤和继任](../Page/魏凤和.md "wikilink")。

## 参考文献

{{-}}

[Category:第二炮兵基地司令員](../Category/第二炮兵基地司令員.md "wikilink")
[Category:中国人民解放军第二炮兵部队上将](../Category/中国人民解放军第二炮兵部队上将.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:临沭人](../Category/临沭人.md "wikilink")
[Z](../Category/靖姓.md "wikilink")