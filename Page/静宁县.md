**静宁县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省](../Page/甘肃省.md "wikilink")[平凉市下属的一个](../Page/平凉市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。面积2193平方公里，2004年人口47万。[邮政编码](../Page/邮政编码.md "wikilink")
743400，县政府驻城关镇。

## 历史

[宋真宗](../Page/宋真宗.md "wikilink")[大中祥符四年](../Page/大中祥符.md "wikilink")（1011年），[渭州知州](../Page/渭州_\(唐朝\).md "wikilink")[曹玮在陇山外之笼干川修筑陇干城](../Page/曹瑋.md "wikilink")（今[隆德县城](../Page/隆德县.md "wikilink")），作为抵御[西夏的军事要塞](../Page/西夏.md "wikilink")。[宋哲宗](../Page/宋哲宗.md "wikilink")[元祐八年](../Page/元祐_\(年號\).md "wikilink")（1093年），置陇干县于外底堡（今静宁县城），并将[德顺军治移陇干县](../Page/德順軍.md "wikilink")，军、县同治一城。

[元成宗](../Page/元成宗.md "wikilink")[大德八年](../Page/大德_\(元朝\).md "wikilink")（1304年）改[德顺州为](../Page/德顺州.md "wikilink")[静宁州](../Page/静宁州.md "wikilink")，领隆德县。

民国二年（1913年）改静宁州为县，隶属[泾原道](../Page/泾原道.md "wikilink")。

## 地理

静宁县位于甘肃省东部，[六盘山西麓](../Page/六盘山.md "wikilink")、[渭河支流葫芦河中上游](../Page/渭河.md "wikilink")。与之接壤的县市有:东北边[宁夏回族自治区的隆德县](../Page/宁夏回族自治区.md "wikilink")、[西吉县](../Page/西吉县.md "wikilink")，西边[通渭县](../Page/通渭县.md "wikilink")、南边[秦安县](../Page/秦安县.md "wikilink")，西北边[会宁县](../Page/会宁县.md "wikilink")，东南边[庄浪县](../Page/庄浪县.md "wikilink")。

## 行政区划

下辖13个[镇](../Page/镇.md "wikilink")、11个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 外部链接

  - [静宁县政府网页](https://archive.is/20120804083611/http://www.jingning.gansu.gov.cn/)

## 参考资料

[甘/甘肃](../Page/category:国家级贫困县.md "wikilink")

[静宁县](../Category/静宁县.md "wikilink") [县](../Category/平凉区县.md "wikilink")
[平凉](../Category/甘肃省县份.md "wikilink")