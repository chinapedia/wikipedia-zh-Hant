**非主流電影**是指一些成本低廉、品質粗糙、實驗性或非商業的小眾影片，與之相對的是「[主流電影](../Page/主流電影.md "wikilink")」。它可能是指：

## 業餘製作的電影

  - **[家庭錄像](../Page/家庭錄像.md "wikilink")**：業餘製作的影片，供家庭成員和朋友觀賞。→*[:en:Home
    movies](../Page/:en:Home_movies.md "wikilink")*
      - **[家庭劇場](../Page/家庭劇場.md "wikilink")**：模擬劇院的家庭式視聽放映場所。→*[:en:Home
        theater](../Page/:en:Home_theater.md "wikilink")*
  - **[業餘電影](../Page/業餘電影.md "wikilink")**：業餘、非専業製作的影片。→*[:en:Amateur
    film](../Page/:en:Amateur_film.md "wikilink")*
  - **[愛好者電影](../Page/愛好者電影.md "wikilink")**：電影、電視、漫畫或遊戲同好者們自編自導演繹的影片。→*[:en:Fan
    film](../Page/:en:Fan_film.md "wikilink")*
  - **[機械電影](../Page/Machinima.md "wikilink")**：以3D電腦圖形製作的影片。→*[:en:Machinima](../Page/:en:Machinima.md "wikilink")*

## 专業製作的電影

  - **[造反影像](../Page/造反影像.md "wikilink")**：基於「地下電影」運動的影片，經由不特定的組合互相交流，產生具有震憾性的個人價值觀和幽默感。見「[造反藝術](../Page/造反藝術.md "wikilink")」。→*[:en:Cinema
    of
    Transgression](../Page/:en:Cinema_of_Transgression.md "wikilink")*
  - **[獨立電影](../Page/獨立電影.md "wikilink")**：沒有大型电影公司投資製作的低成本商業电影，時間在40分鐘左右或以上，通常在午夜劇場或有線電視放映。→*[en:Independent/Indie
    film](../Page/:en:Independent_film.md "wikilink")*
  - **[剝削電影](../Page/剝削電影.md "wikilink")**：大量描繪色情、血腥及暴力的獨立電影。→*[:en:Exploitation
    film](../Page/:en:Exploitation_film.md "wikilink")*
      - **[磨坊劇場](../Page/磨坊劇場.md "wikilink")**：放映「剝削電影」的劇場。→*[:en:Grindhouse](../Page/:en:Grindhouse.md "wikilink")*
  - **[B級電影](../Page/B級電影.md "wikilink")**：低成本製作的商業影片，通常都是品質粗糙的電影作品。→*[:en:B
    movie](../Page/:en:B_movie.md "wikilink")*
      - **[C級電影](../Page/C級電影.md "wikilink")**：比「B級電影」更低成本製作的商業影片，一般在有線電視放映。→*[en:C
        movie](../Page/:en:B_movie#C_movie.md "wikilink")*
      - **[Z級電影](../Page/Z級電影.md "wikilink")**：1960年代的低成本製作商業影片。→*[:en:Z
        movie](../Page/:en:Z_movie.md "wikilink")*
  - **[午夜電影](../Page/午夜電影.md "wikilink")**：在午夜時間放映的影片，通常都是一些B級電影或獨立電影。→*[:en:Midnight
    movie](../Page/:en:Midnight_movie.md "wikilink")*
      - **[汽車劇場](../Page/汽車劇場.md "wikilink")**：露天的電影劇場，通常放映的都是一些B級電影或獨立電影。→*[:en:Drive-in
        theater](../Page/:en:Drive-in_theater.md "wikilink")*
  - **[無浪潮電影](../Page/無浪潮電影.md "wikilink")**：1970-80年代的電影藝術運動，强調焦躁不安的情緒。見「[無浪潮](../Page/無浪潮.md "wikilink")」。→*[:en:No
    Wave Cinema](../Page/:en:No_Wave_Cinema.md "wikilink")*
  - **[雙片連映](../Page/雙片連映.md "wikilink")**：將兩部40分鐘左右的影片拼湊在一起，同時上映。→*[:en:Double
    feature](../Page/:en:Double_feature.md "wikilink")*

## 其它

  - **[藝術電影](../Page/藝術電影.md "wikilink")**：著重於一些有嚴肅需求，但尚未完善或尚未被滿足的觀眾，而非普羅大眾的電影。→*[en:Arthouse](../Page/:en:Art_film.md "wikilink")*
  - **[低成本電影](../Page/低成本電影.md "wikilink")**：低成本製作的影片，通常「獨立電影」都是低成本電影。→*[:en:Low
    budget film](../Page/:en:Low_budget_film.md "wikilink")*
  - **[無成本電影](../Page/無成本電影.md "wikilink")**：製作成本近乎於零的影片。→*[:en:No
    budget film](../Page/:en:No_budget_film.md "wikilink")*
  - **[邪典電影](../Page/邪典電影.md "wikilink")**：通常是指一些帶有次文化教義而被不特定族群追捧的影片。→*[:en:Cult
    film](../Page/:en:Cult_film.md "wikilink")*
  - **[實驗電影](../Page/實驗電影.md "wikilink")**：呈現某些手法、視覺藝術或效果的電影。→*[:en:Experimental
    film](../Page/:en:Experimental_film.md "wikilink")*
      - **[再現代電影](../Page/再現代電影.md "wikilink")**：21世紀初興起的電影藝術運動，重點於回歸「現代主義的本來原則」。見「[再現代主義](../Page/再現代主義.md "wikilink")」。→*[:en:Remodernist
        film](../Page/:en:Remodernist_film.md "wikilink")*
  - **[顯微電影](../Page/顯微電影.md "wikilink")**：以DV拍攝，由電腦軟體剪輯後，通過影帶、光碟或互聯網絡發行的「低成本業餘電影」或「實驗電影」。→*[:en:Microcinema](../Page/:en:Microcinema.md "wikilink")*
  - **[另類電影](../Page/另類電影.md "wikilink")**：學術上對不同類型的非「正統」影像的分類。→*[:en:Paracinema](../Page/:en:Paracinema.md "wikilink")*
  - **[地下電影](../Page/地下電影.md "wikilink")**：可能抵觸法律的另類題材、類型或資金的影片，一般要經由特殊管道才有辦法觀賞。→*[:en:Underground
    film](../Page/:en:Underground_film.md "wikilink")*
  - **[短片](../Page/短片.md "wikilink")**：時間在40分鐘以下的影片。→*[:en:Short
    film](../Page/:en:Short_film.md "wikilink")*
  - **[成人电影](../Page/成人电影.md "wikilink")**：含有不適合未成年者觀賞內容的影帶或光碟，特别是指「色情電影」。→*[:en:Pornographic
    film](../Page/:en:Pornographic_film.md "wikilink")*
  - **[恐怖影片](../Page/恐怖影片.md "wikilink")**：英國口語，指含有噁心內容的影帶或光碟，特别是指「低成本恐怖電影」。→*[:en:Video
    nasty](../Page/:en:Video_nasty.md "wikilink")*

## 參見

  - [录影带首映](../Page/录影带首映.md "wikilink")

[Category:五字作品消歧义](../Category/五字作品消歧义.md "wikilink")