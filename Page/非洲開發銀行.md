**非洲開發銀行**（[英語](../Page/英語.md "wikilink")：**A**frican **D**evelopment
**B**ank，**ADB**）是於1964年成立的地區性[國際開發銀行](../Page/世界開發銀行.md "wikilink")，总部位于[科特迪瓦首都](../Page/科特迪瓦.md "wikilink")[阿比让](../Page/阿比让.md "wikilink")，成立宗旨在促進[非洲的社會及經濟發展](../Page/非洲.md "wikilink")，共有53個非洲國家及24個非非洲區國家為其會員。

## 具有會籍的正式成員國

[
](https://zh.wikipedia.org/wiki/File:Map_of_African_Development_Bank_Members.svg "fig:   ")
**首批非洲开发银行受惠国:**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

**非洲开发银行受惠国:**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

**非洲开发银行和非洲金融共同體受惠国:**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

備註:包括[毛里塔尼亞伊斯蘭共和國在內的](../Page/毛里塔尼亞伊斯蘭共和國.md "wikilink")[非洲聯盟成員國](../Page/非洲聯盟成員國列表.md "wikilink")，但不包括[阿拉伯撒哈拉民主共和國](../Page/阿拉伯撒哈拉民主共和國.md "wikilink")；[摩洛哥雖然不是非洲聯盟成員](../Page/摩洛哥.md "wikilink")，但也享受本組織的惠顧

**非非洲聯盟特邀成员国:**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

[Category:1964年成立的银行](../Category/1964年成立的银行.md "wikilink")
[Category:非洲国际组织](../Category/非洲国际组织.md "wikilink")
[Category:国际经济组织](../Category/国际经济组织.md "wikilink")
[Category:多边开发银行](../Category/多边开发银行.md "wikilink")