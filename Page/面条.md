[Bowl_full_of_noodles_with_marinated_beef,_bok_choy,_celery_and_green_pea_shoots_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:Bowl_full_of_noodles_with_marinated_beef,_bok_choy,_celery_and_green_pea_shoots_-_panoramio.jpg "fig:Bowl_full_of_noodles_with_marinated_beef,_bok_choy,_celery_and_green_pea_shoots_-_panoramio.jpg")
**麵**或**麵条**，一种用[穀物或](../Page/穀物.md "wikilink")[豆类的](../Page/豆类.md "wikilink")[麵粉加](../Page/麵粉.md "wikilink")[水和成面团](../Page/水.md "wikilink")，之后或者压或擀制成片再切或压，或者使用搓、拉、捏等手段，制成条状（或窄或宽，或扁或圆）或小片状的食品。可再经煮、炒、烩、炸等烹調方式食用。

关于麵条的最早文字记录在中国[东汉时期](../Page/东汉.md "wikilink")，2005年時，[中国社会科学院考古研究所研究员叶茂林在](../Page/中国社会科学院.md "wikilink")[青海省的](../Page/青海省.md "wikilink")[喇家遗址中发现了距今](../Page/喇家遗址.md "wikilink")4,000年以上历史的麵条，长50厘米，宽0.3厘米，由[粟制成](../Page/粟.md "wikilink")，根據最早的史料及实物佐证，因此通常認為面起源于[中国](../Page/中国.md "wikilink")[新石器時代](../Page/中國新石器時代.md "wikilink")。\[1\]\[2\]

## 別名

一些地區稱呼同類食品有別的名字，如魯南部份地區稱麵條為“麵湯”（湯讀輕聲，指麵條湯時為“面湯湯”）。

## 東亞麵條

### 中式面条

[ChineseNoodles.jpg](https://zh.wikipedia.org/wiki/File:ChineseNoodles.jpg "fig:ChineseNoodles.jpg")\]\]
在中国，最初所有面食统称为饼，其中在汤中煮熟的叫「**汤饼**」，即最早的麵条。[汉](../Page/汉.md "wikilink")[刘熙](../Page/刘熙.md "wikilink")《[释名·释饮食](../Page/释名·释饮食.md "wikilink")》中有**索饼**；[北魏](../Page/北魏.md "wikilink")[贾思勰](../Page/贾思勰.md "wikilink")《[齐民要术](../Page/齐民要术.md "wikilink")》中记有「**水引饼**」，是一种一尺一断，薄如“韭叶”的水煮食品；[唐朝又有称为](../Page/唐朝.md "wikilink")**冷淘**的过水凉面；[宋朝饮食市场上的面条品种达](../Page/宋朝.md "wikilink")10种之多，丰富多彩，有插肉麵、浇头麵等；[元朝出现了可以久存的](../Page/元朝.md "wikilink")[挂麵](../Page/挂麵.md "wikilink")\[3\]\[4\]；[明朝有制作技术高超的](../Page/明朝.md "wikilink")[拉麵](../Page/拉麵.md "wikilink")，还有[山西等地制作特殊的](../Page/山西.md "wikilink")[刀削麵](../Page/刀削麵.md "wikilink")；[清朝](../Page/清朝.md "wikilink")[乾隆年间又有经过煮](../Page/乾隆.md "wikilink")、炸后，再加入菜肴烧焖而熟的[伊府麵](../Page/伊府麵.md "wikilink")，这些都是中国历史上著名的麵条制品。

面条的口感好，制作简单，所以在中国非常流行。而由于製条、调味的不同，使中国各地及華人世界出现了数以千计的麵条品种，让人目不暇接。通常把稻米以外原料的麵條才稱為「麵」，以稻米為原料的稱為「粉」。麵條類型有[白麵條](../Page/白麵條.md "wikilink")、[拉麵](../Page/中式拉麵.md "wikilink")、[青海的](../Page/青海.md "wikilink")[麵片](../Page/麵片.md "wikilink")、[麵線](../Page/麵線.md "wikilink")、[油麵](../Page/油麵.md "wikilink")、[生麵](../Page/生麵.md "wikilink")、[米粉](../Page/米粉.md "wikilink")、[米線](../Page/米線.md "wikilink")、[刀削麵等](../Page/刀削麵.md "wikilink")。

### 日本面条

[Agodashi_Ramen_Ikitsuki_Nagasaki_2008.jpg](https://zh.wikipedia.org/wiki/File:Agodashi_Ramen_Ikitsuki_Nagasaki_2008.jpg "fig:Agodashi_Ramen_Ikitsuki_Nagasaki_2008.jpg")\]\]
根据[日本学者田中静一考证](../Page/日本.md "wikilink")，面条可能是在[唐朝传入日本](../Page/唐朝.md "wikilink")\[5\]，《和名类聚抄》称为**索饼**\[6\]。用切割馄饨薄面皮制造面条的另一种方法，可能在15世纪时由中国传入日本奈良，当时写为“温饨”读作udon(乌东)；至于[馒头则是在元代由](../Page/馒头.md "wikilink")[林净因传入日本](../Page/林净因.md "wikilink")，林净因在日本被称为馒头之父，在奈良有一座林净因记念神殿\[7\]。

著名的麵条有主要有拉面、乌冬面、[荞麦面与](../Page/荞麦面条.md "wikilink")[素麵](../Page/素麵.md "wikilink")。其中拉面种类最多。按照面汤的种类主要分酱油面、味增面和咸味面。\[8\]

二十世纪中，日籍臺灣人[吴百福發明了](../Page/吴百福.md "wikilink")[方便面](../Page/方便面.md "wikilink")，迅速在亚洲流行开来。\[9\]。

### 朝鲜面条

朝鲜麵條有素麵、[玉米麵](../Page/玉米麵.md "wikilink")。按照吃有著名面条有[冷麵](../Page/朝鲜冷面.md "wikilink")、拌麵、炒麵、湯麵等。配料有泡菜、海鲜等。\[10\]

## 意大利-{}-面条

[Spaghetti_Vongole.jpg](https://zh.wikipedia.org/wiki/File:Spaghetti_Vongole.jpg "fig:Spaghetti_Vongole.jpg")

[考古學家考證](../Page/考古學家.md "wikilink")，在[意大利中部一個公元前](../Page/意大利.md "wikilink")4世紀的[伊特魯裏亞人的墓穴裡發現有意大利粉](../Page/伊特魯裏亞.md "wikilink")。\[11\]

传说旅行家[马可波罗将面条由中国介绍到](../Page/马可波罗.md "wikilink")[意大利](../Page/意大利.md "wikilink")，但没有确实的证据\[12\]。事实上在马可波罗回到意大利之前，意大利已经流行[通心面Maccheroni和](../Page/通心面.md "wikilink")[细面条Vermicelli](../Page/细面条.md "wikilink")。保存在意大利[热那亚档案馆有一份士兵巴士托内在](../Page/热那亚.md "wikilink")1279年写的遗书，嘱咐将一篮子通心面交给亲属\[13\]。Rosenberg
和石毛直道指出，面条可能通过丝绸之路，由中国传入阿拉伯，由阿拉伯人传入[西西里岛](../Page/西西里岛.md "wikilink")，再传入意大利半岛\[14\]。

18世纪意大利发明了番茄酱面条，此后成意大利食品风行世界\[15\]。

## 中东面条

1226年的[巴格达](../Page/巴格达.md "wikilink")，流行Rishta--一种线形面条，和Ytriyya--[通心粉](../Page/通心粉.md "wikilink")\[16\]。

## 东南亚面条

面条在明代传入东南亚。[柬埔寨称为Numbanchock](../Page/柬埔寨.md "wikilink")，[泰国叫Sen](../Page/泰国.md "wikilink")
mee，印尼和马来西亚称为mie或mi。\[17\]

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部链接

  - [BBC有关喇家遗址面条的新闻报道](http://news.bbc.co.uk/2/hi/science/nature/4335160.stm)
  - [Sciscape的報導](https://web.archive.org/web/20051225113742/http://www.sciscape.org/news_detail.php?news_id=1896)

## 参见

  - [方便面](../Page/方便面.md "wikilink")
  - [杯麵](../Page/杯麵.md "wikilink")

[面条](../Category/面条.md "wikilink")

1.  2005年10月的英国《自然》杂志刊登相关论文。(Lu, H. et al. Culinary archaeology: Millet
    noodles in Late Neolithic China. Nature 437, 967-968 (13 October
    2005) | doi: 10.1038/437967a)

2.

3.  元《饮膳正要》卷一

4.  李约瑟《[中国科学技术史](../Page/中国科学技术史.md "wikilink")》第六卷第五分册，416页

5.  李约瑟《中国科学技术史》第六卷第五分册第四十章生物化学技术，422页，引用日本田中静一论文。

6.  李约瑟《中国科学技术史》第六卷第五分册，422页

7.  李约瑟《中国科学技术史》第六卷第五分册，422页

8.
9.  李约瑟《中国科学技术史》第六卷第五分册，423页

10.
11.

12. 李约瑟《中国科学技术史》第六卷第五分册，423页

13. 李约瑟《中国科学技术史》第六卷第五分册，426页图 120

14. 李约瑟《中国科学技术史》第六卷第五分册，426页

15. 李约瑟《中国科学技术史》第六卷第五分册，428页

16. 李约瑟《中国科学技术史》第六卷第五分册，425页

17. 李约瑟中《国科学技术》史第六卷第五分册，423页