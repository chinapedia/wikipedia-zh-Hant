[HK_ShatinYuLanFestival_KingOfGhost.JPG](https://zh.wikipedia.org/wiki/File:HK_ShatinYuLanFestival_KingOfGhost.JPG "fig:HK_ShatinYuLanFestival_KingOfGhost.JPG")沙田舉行的潮人盂蘭盆會\]\]
[Mianran-dashi-20100824-YauTong-IMAG0043.jpg](https://zh.wikipedia.org/wiki/File:Mianran-dashi-20100824-YauTong-IMAG0043.jpg "fig:Mianran-dashi-20100824-YauTong-IMAG0043.jpg")
[HK_Sheung_Wan_U_Lan_Ghost_Festival_night_paper_goddess_stage_Aug-2012.JPG](https://zh.wikipedia.org/wiki/File:HK_Sheung_Wan_U_Lan_Ghost_Festival_night_paper_goddess_stage_Aug-2012.JPG "fig:HK_Sheung_Wan_U_Lan_Ghost_Festival_night_paper_goddess_stage_Aug-2012.JPG")

**面燃大士**或**面然大士**（然是燃的古字），又称**焰口**，為[道教](../Page/道教.md "wikilink")、[佛教的著名](../Page/佛教.md "wikilink")[神祇](../Page/神祇.md "wikilink")，俗謂「**大士爺**」、「**大士王**」、「**普渡公**」或「**普渡爺**」。原在佛教中是形容恐怖的鬼王，传说是[观世音菩萨的化身](../Page/观世音菩萨.md "wikilink")，或为观世音菩萨的皈依[鬼道弟子](../Page/鬼道.md "wikilink")，他指引释迦佛的弟子[阿难陀尊者去求教佛陀](../Page/阿难陀.md "wikilink")，让佛陀说出**[放焰口](../Page/施饿鬼.md "wikilink")**的仪轨，後来这套仪轨在[盂兰盆节中也会用到](../Page/盂兰盆节.md "wikilink")，再後来焰口被道教吸收爲神祇。道教认为，在[農曆七月](../Page/農曆.md "wikilink")，所有來陽世享[香火的](../Page/香火.md "wikilink")[亡靈](../Page/亡靈.md "wikilink")，都歸此神管理；不少地方[中元法會祭拜](../Page/中元法會.md "wikilink")[亡靈之前](../Page/亡靈.md "wikilink")，會先祭拜**面燃大士**。\[1\]在[臺灣](../Page/臺灣.md "wikilink")，[嘉義縣](../Page/嘉義縣.md "wikilink")[民雄鄉有專為祭祀此神的](../Page/民雄鄉.md "wikilink")「[大士爺廟](../Page/大士爺廟.md "wikilink")」。

## 名號

在佛教尊稱為「**鐵圍山内面燃大士菩薩**」，又稱「**焦面鬼王**」、「**焰口鬼王**」、[道教神銜為](../Page/道教.md "wikilink")「**幽冥面燃鬼王監齋使者羽林大神普渡真君**」，通稱「**羽林監齋普渡真君**」\[2\]。簡稱「**羽林大神**」、「**普渡真君**」。

## 典故來由

### 道教說法

[道教的說法則是](../Page/道教.md "wikilink")[太乙救苦天尊](../Page/太乙救苦天尊.md "wikilink")[化身](../Page/化身.md "wikilink")
「**幽冥教主冥司面燃鬼王硏孑大帝**」或稱「**面燃羽林監齋普渡真君**」，主宰諸鬼，護佑冥、陽兩界。平時居沃焦之山下，為[陰間諸鬼之](../Page/陰間.md "wikilink")[統帥](../Page/統帥.md "wikilink")，在[地官大帝赦罪之月](../Page/地官大帝.md "wikilink")，負責中元節監督[亡魂享領](../Page/亡魂.md "wikilink")[人間](../Page/人間.md "wikilink")[香火事宜](../Page/香火.md "wikilink")。另，[靈寶派等一些派別不是請](../Page/靈寶派.md "wikilink")「面燃鬼王」監齋，而是有「[何昌](../Page/何昌.md "wikilink")」、「[喬荀](../Page/喬荀.md "wikilink")」兩神分任此職。如[宋末](../Page/宋.md "wikilink")[元初](../Page/元.md "wikilink")[林靈真所撰](../Page/林靈真.md "wikilink")《靈寶領教濟度金書》寫為「北魁玄範府何、喬二[大聖](../Page/大聖.md "wikilink")」，有的版本寫為「陽神大聖何昌」，「陰神大聖喬荀」。

據《濟煉施食科》及《召攝科》的典籍，何、喬二[大聖](../Page/大聖.md "wikilink")，屬於北魁玄範府神虎攝召司的鐵牛大神「魯天君」（據說名為魯玄水）部下，「魯天君」信仰在各地減退之後，也有教派會把何、喬二[大聖視為普渡真君的](../Page/大聖.md "wikilink")[脅侍](../Page/脅侍.md "wikilink")。

其實，原先道教中負責召喚陰魂的大神，還有「神虎大神」（形象虎頭人身，或者穿著虎皮）、「攝召烏[大王](../Page/王爺神.md "wikilink")」、「攝召-{涂}-大王」、「[五道將軍](../Page/五道將軍.md "wikilink")」等，這些神靈現在幾乎都被認為是普渡真君的[從神](../Page/從神.md "wikilink")。

### 佛教說法

面燃鬼王（焰口鬼王）的典故出現在「阿難遇面燃鬼王」的佛經故事。《佛說救拔焰口餓鬼陀羅尼經》記載，[釋迦牟尼佛的弟子](../Page/釋迦牟尼佛.md "wikilink")[阿難](../Page/阿難.md "wikilink")[尊者在林間修習](../Page/尊者.md "wikilink")[禪定時](../Page/禪定.md "wikilink")，忽然看見一位滿臉火焰熊熊燃燒，骨瘦如柴，痛苦異常的鬼王來到面前。鬼王自稱「面燃」，說阿難三天之後將墮落[餓鬼道](../Page/餓鬼.md "wikilink")，想避免，就要[布施百千個](../Page/布施.md "wikilink")[餓鬼及百千個](../Page/餓鬼.md "wikilink")[婆羅門](../Page/婆羅門.md "wikilink")[仙人各一斛飲食](../Page/仙人.md "wikilink")，並供養[三寶](../Page/三寶.md "wikilink")。阿難向佛陀稟報，[佛陀教阿難](../Page/佛陀.md "wikilink")《[陀羅尼施食法](../Page/陀羅尼.md "wikilink")》，[陀羅尼](../Page/陀羅尼.md "wikilink")[加持過的食物成為法供](../Page/加持.md "wikilink")，上奉佛法僧[三寶](../Page/三寶.md "wikilink")，平等下施餓鬼等眾生，如此則能消除眾鬼的痛苦，令他們捨去鬼身，生於[天道](../Page/天道.md "wikilink")。阿難便遵照佛陀的教化與指示，設齋供僧，並且[施餓鬼來祈福](../Page/施餓鬼.md "wikilink")，因此獲得解脫。從此佛家就有了設「面燃鬼王」[牌位](../Page/牌位.md "wikilink")，[放燄口法會等習俗](../Page/放燄口.md "wikilink")。

「面燃鬼王」的來由與職能，[佛教說法有數個](../Page/佛教.md "wikilink")：一是祂是[觀音大士的](../Page/觀音大士.md "wikilink")[化身](../Page/化身.md "wikilink")，故稱「大士爺」。《佛說觀自在化身焰口鬼王大威光陀羅尼經》：「我實從[觀自在菩薩心中所化](../Page/觀自在菩薩.md "wikilink")，能攝諸祕密要門。我亦號為**焰口**，常現餓鬼形而居苦趣，示教利喜，啖諸穢濁，令發[阿耨多羅三藐三菩提心](../Page/阿耨多羅三藐三菩提.md "wikilink")。世尊，往昔諸佛為顯我之大威猛故，集諸微玅持具，嚴飾我身。皆各舒右手摩我頂門。從是諸手印內流無量甘露，令我力同諸佛，智同諸佛，用同諸佛。我得諸佛加持，故能祕化九形於九界，以根本身而住佛法界中。無量[藥叉隨相衛護](../Page/藥叉.md "wikilink")，一切諸魔聞之遠懼。」但也有質疑《佛說觀自在化身焰口鬼王大威光陀羅尼經》不知出處，此經的來源還需要考究。

也有人認為「面燃大士」以鬼王相貌展現，源自《[法華經普門品](../Page/觀世音菩薩普門品.md "wikilink")》中，觀音大士“應以[天](../Page/提婆.md "wikilink")、[龍](../Page/龍神.md "wikilink")、[夜叉](../Page/夜叉.md "wikilink")、[乾闥婆](../Page/乾闥婆.md "wikilink")、[阿修羅](../Page/阿修羅.md "wikilink")、[迦樓羅](../Page/迦樓羅.md "wikilink")、[緊那羅](../Page/緊那羅.md "wikilink")、[摩睺羅伽](../Page/摩睺羅伽.md "wikilink")、人、非人等身得度者，即皆現之而為說法。」「以種種形遊諸國土度脫眾生」，是觀音大士教化餓鬼界眾生的需要而產生，另一方面是警惕人們應該積極[布施](../Page/布施.md "wikilink")[僧侶](../Page/僧侶.md "wikilink")、[貧民](../Page/貧民.md "wikilink")，不宜動貪愛及慳吝想，以免墮入餓鬼心識，成為“面燃”的眷屬。

二是祂原為諸鬼的[首領](../Page/首領.md "wikilink")，因受觀音大士教化而[皈依其](../Page/皈依.md "wikilink")[門下](../Page/門下.md "wikilink")，從此被稱作「大士爺」，成為護持[盂蘭節普渡事項的](../Page/盂蘭節.md "wikilink")[護法神](../Page/護法神.md "wikilink")。

三是「面燃大士」亦擁護[地藏菩薩](../Page/地藏菩薩.md "wikilink")。據載，[福建省](../Page/福建省.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[安溪縣永安里](../Page/安溪縣.md "wikilink")[謝氏](../Page/謝姓.md "wikilink")，夜夢其[祖先泣訴](../Page/祖先.md "wikilink")，「家有鬼，不能進。」於是謝氏率其一家，虔誠誦念地藏菩薩名號經典，焚香叩請[地藏菩薩](../Page/地藏菩薩.md "wikilink")。夜中，夢見「面燃大士」前來說法，曰此鬼為前世冤親，[誦念佛號](../Page/念佛.md "wikilink")[迴向之](../Page/迴向.md "wikilink")，即可化解。其家主夢中語大士曰：「我乃祈請地藏菩薩，尊神乃[觀音大士化身](../Page/觀音大士.md "wikilink")，何幸駕臨？」面燃大士笑曰：「諸佛同體，何起分別？地藏即不可擁護乎？」\[3\]《[華嚴經](../Page/華嚴經.md "wikilink")》有言：「一切諸如來，同共一[法身](../Page/法身.md "wikilink")。」又曰：「一切如來一法身，[真如平等無分別](../Page/真如.md "wikilink")。」\[4\]

## 民間的奉祀

[華人民間則多採](../Page/華人.md "wikilink")[佛教說法](../Page/佛教.md "wikilink")，而使用道教科儀。無論是中元盛會，或是各種類型的超度法會與齋醮，多以佛教形式塑造大士爺神像：「頂生二角、青面獠牙，高大威武，頭上還有一尊[觀世音菩薩佛像](../Page/觀世音菩薩.md "wikilink")，象徵其代表慈悲的觀音大士」，而在各種類型的超度法會與齋醮前請道士進行開光。[1](http://lingyingtan.blogspot.com/2012/01/blog-post_31.html)

中元節祭[亡魂前](../Page/亡魂.md "wikilink")，一般都先拜大士爺，有些慎重的信徒會以紙紮出大士爺塑像，也有些用[牌位](../Page/牌位.md "wikilink")、畫像、木石雕像祭祀，並將大士爺神位或神像放置供桌之前，以求祭祀順利。待到七月一過，一般會[火化大士爺像](../Page/火化.md "wikilink")，送其登天。

[南洋](../Page/南洋.md "wikilink")[華僑風俗](../Page/華僑.md "wikilink")，[中元普渡時](../Page/中元普渡.md "wikilink")，會懸掛寫有「南無渡幽拔苦無量壽佛」或「南無分衣施食阿彌陀佛」之類的幢幡於大士爺神像之側，甚至是神像手上；如書「渡幽拔苦」之類字句是希望[佛祖引領亡靈往生](../Page/佛祖.md "wikilink")[佛國](../Page/佛國.md "wikilink")，若寫「分衣施食」等[詞彙](../Page/詞彙.md "wikilink")，則指祈請[阿彌陀佛](../Page/阿彌陀佛.md "wikilink")（[無量壽佛](../Page/無量壽佛.md "wikilink")）助大士爺，平均分配[紙錢](../Page/紙錢.md "wikilink")、衣物、食品予眾家亡魂。\[5\]

## 相關條目

  - [中元節](../Page/中元節.md "wikilink")
  - [盂兰盆节](../Page/盂兰盆节.md "wikilink")
  - [觀音大士](../Page/觀音大士.md "wikilink")

## 注釋

## 參考資料

  - [不空三藏](../Page/不空三藏.md "wikilink") 《佛說觀自在化身焰口鬼王大威光陀羅尼經》
  - [大士爺 - 臺灣大百科全書](http://nrch.culture.tw/twpedia.aspx?id=7439)

## 外部連結

  -
[Category:觀世音](../Category/觀世音.md "wikilink")
[Category:護法神](../Category/護法神.md "wikilink")
[Category:道教神祇](../Category/道教神祇.md "wikilink")
[Category:中國民間信仰](../Category/中國民間信仰.md "wikilink")
[Category:中元節與盂蘭盆節 (華人)](../Category/中元節與盂蘭盆節_\(華人\).md "wikilink")
[Category:太平清醮](../Category/太平清醮.md "wikilink")
[Category:盂蘭盆會](../Category/盂蘭盆會.md "wikilink")
[Category:紙紮](../Category/紙紮.md "wikilink")
[Category:佛教神祇](../Category/佛教神祇.md "wikilink")

1.
2.
3.
4.
5.