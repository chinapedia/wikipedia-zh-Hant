[Gram_Stain_Anthrax.jpg](https://zh.wikipedia.org/wiki/File:Gram_Stain_Anthrax.jpg "fig:Gram_Stain_Anthrax.jpg")（圖中藍紫色棒狀，革兰氏阳性）\]\]
**革蘭氏染色**（）是用來鑒别[細菌的一種方法](../Page/細菌.md "wikilink")：這種染色法利用細菌細胞壁上的[生物化学性质不同](../Page/生物化学.md "wikilink")，可將細菌分成兩類，即[革蘭氏陽性](../Page/革蘭氏陽性菌.md "wikilink")（）与[革蘭氏陰性](../Page/革蘭氏陰性菌.md "wikilink")（）。这一染色方法由丹麥醫生[漢斯·克-{里}-斯蒂安·革蘭於](../Page/汉斯·克里斯蒂安·革兰.md "wikilink")1884年所發明，最初是用來鑑別肺炎球菌與克雷白氏肺炎菌之間的關係\[1\]，后推广为鉴别细菌种类的重要特性之一，对由细菌感染引起的疾病的临床诊断及治疗有着广泛用途。

[革蘭氏阳性菌和](../Page/革蘭氏陽性菌.md "wikilink")[革蘭氏阴性菌对](../Page/革兰氏阴性菌.md "wikilink")[抗生素的反应并不一样](../Page/抗生素.md "wikilink")，因而通过抽取样品医生可以在很短的时间内（5分钟左右）确定细菌的革蘭氏性质，医生从而可依据结果初步判断，迅速给病人注射抗生素，而不需要等待几天细菌培植得到的化验结果。然而并非所有的细菌都可以通过革兰氏染色归类，如[分歧杆菌等则需进行](../Page/分枝杆菌属.md "wikilink")[抗酸染色鉴别](../Page/抗酸染色.md "wikilink")\[2\]。

## 染色结果

革蘭氏染色的对象是[细菌的](../Page/细菌.md "wikilink")[细胞壁](../Page/细胞壁.md "wikilink")。染色后的细菌可在[显微镜下更好的观察](../Page/显微镜.md "wikilink")，以便于区分。不同的细菌在该染色法的作用底下反应不同，藉以区分成为两类：

  - [革蘭氏阳性菌](../Page/革蘭氏陽性菌.md "wikilink")，胞壁染色后呈**蓝紫色**
  - [革蘭氏阴性菌](../Page/革兰氏阴性菌.md "wikilink")，染色后呈**红色**。

### 革蘭氏阳性菌

### 革蘭氏阴性菌

### 不确定或者多变

## 染色流程及原理

流程可概括为：染色-脱色-複染。

  - 第一步初染剂（Primary Stain）

使用[结晶紫](../Page/結晶紫.md "wikilink")染色，染色部位主要為染細胞壁上的肽聚醣。

  - 第二步媒染劑（Mordant）

加入[複方碘溶液或稱革蘭氏碘液](../Page/複方碘溶液.md "wikilink")（Gram's
Iodine）後，兩者會形成不溶性的結晶紫-碘（CV-I）複合体。細菌呈紫黑色。第二步是關键，它的目的是分辨细菌的革蘭氏染色特性。若為革蘭氏陽性菌，此複合體係結合於細胞壁之鎂、核糖核酸（Magnesium
Ribonucletic Acid），形成鎂-核糖核酸-結晶紫-碘（Mg-RNA-CV-I）複合體，不易脫去，染劑也就固定下來了。

  - 第三步脫色劑（Decoloring Agent）

使用酒精（95%）脱色，試劑具脂溶劑（Lipid Solvent）和蛋白脫水劑（Protein Dehydrating
Agent）之雙重功能。此步驟對革蘭氏陽性細菌無效，因為細胞壁上厚厚的[細胞壁](../Page/細胞壁.md "wikilink")(成分為肽聚醣)阻隔了結晶紫-碘複合體的外渗，只能留在细菌体内。革蘭氏陰性菌細胞壁層薄，會被脱色而成為無色。

  - 第四步複染劑（Counterstain）

為了對比，在此之後会加入[番红](../Page/番红.md "wikilink")（Safranin）進行複染，革蘭氏阴性菌成红色，而革蘭氏阳性菌仍保有原來的紫色。

## 定性

### KOH-试验

另外的一种方法是[氢氧化鉀](../Page/氢氧化鉀.md "wikilink")-试验。将培养好的细菌取出一小部分，使其悬浮于一滴KOH溶液中。革蘭氏阳性菌与之不发生反应，用细针穿过该溶液，溶液呈正常液体状。革蘭氏陰性菌因細胞壁中主要成分為脂多醣，脂質含量較高，接觸強鹼KOH後與KOH發生皂化反應，以細針穿過會有明顯黏稠狀

### 氨肽酶试验

除了个别例外，只有革蘭氏阴性菌才能证明氨肽酶的存在。这是该试验的基础。
将要进行试验的细菌放入试管加水。放入一张显示肽酶活性的试纸，在37°C下等待10到35分钟，试纸变成深黄色。

## 参见

  - [革蘭氏阳性菌](../Page/革蘭氏陽性菌.md "wikilink")
  - [革蘭氏阴性菌](../Page/革兰氏阴性菌.md "wikilink")
  - [分歧杆菌与](../Page/分枝杆菌属.md "wikilink")[抗酸染色](../Page/抗酸染色.md "wikilink")

## 外部链接

  - [丁香园上的](../Page/丁香园.md "wikilink")“[革兰氏染色法(原理、方法和意义)](https://www.biomart.cn/experiment/430/488/489/25725.htm)”
  - [实验二十二
    简单染色法和革兰氏染色法](http://zy.swust.net.cn/07/1/spzljcjs/s22.htm)****
  - [微生物學實驗](http://www2.nsysu.edu.tw/Bio/images/commen/microbio-ex1070825.pdf)之“實驗二、革蘭氏染色法”
  - [高瞻生物實驗手冊](http://highscope.ch.ntu.edu.tw/wordpress/wp-content/uploads/2009/12/33.pdf)之“實作二：細菌染色觀察（簡單染色法、革蘭氏染色法）”
  - [Gram staining technique video](http://www.tgw1916.net/movies.html)

## 参考文献

### 注释

### 引用

[Category:微生物学](../Category/微生物学.md "wikilink")
[Category:實驗室技術](../Category/實驗室技術.md "wikilink")
[Category:染色法](../Category/染色法.md "wikilink")
[Category:醫學檢驗](../Category/醫學檢驗.md "wikilink")

1.
2.