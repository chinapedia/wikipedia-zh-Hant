**靴雕**（[學名](../Page/學名.md "wikilink")：**）是一種中型的[猛禽](../Page/猛禽.md "wikilink")，一般身長47[公分](../Page/公分.md "wikilink")（即18[英吋](../Page/英吋.md "wikilink")），翼展長120公分。

牠們一般分佈在南[歐洲](../Page/歐洲.md "wikilink")、北[非洲和](../Page/非洲.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")，一般會選擇在[樹上或在](../Page/樹.md "wikilink")[危岩的](../Page/危岩.md "wikilink")[巢生](../Page/巢.md "wikilink")[蛋而每次會生下](../Page/蛋.md "wikilink")1至2顆蛋。牠們通常會捕獵[哺乳類動物](../Page/哺乳類動物.md "wikilink")、[爬行動物和](../Page/爬行動物.md "wikilink")[鳥作食物](../Page/鳥.md "wikilink")。

[雄性靴雕的重量可逾](../Page/雄性.md "wikilink")0.7[公斤](../Page/公斤.md "wikilink")（即1.5磅），[雌性靴雕的重量則接近](../Page/雌性.md "wikilink")1公斤（即超過2磅）。牠們會發出[尖銳的](../Page/尖銳.md "wikilink")*kli-kli-kli*的[叫聲](../Page/叫聲.md "wikilink")。

## 參考資料

  - Database entry includes justification for why this species is of
    least concern

  - *[Splitting](../Page/Lumpers_and_splitters.md "wikilink") headaches?
    Recent [taxonomic](../Page/taxonomy.md "wikilink") changes affecting
    the [British](../Page/Great_Britain.md "wikilink") and Western
    [Palaearctic](../Page/Palaearctic.md "wikilink") lists* - Martin
    Collinson, [British
    Birds](../Page/British_Birds_\(magazine\).md "wikilink") vol 99
    (June 2006), 306-323

  - Bunce, M., et al. (2005) Ancient [DNA](../Page/DNA.md "wikilink")
    Provides New Insights into the
    [Evolutionary](../Page/Evolution.md "wikilink") History of New
    Zealand's Extinct Giant Eagle. PLoS Biol 3(1): e9 DOI:
    10.1371/journal.pbio.0030009 [HTML open-source
    article](http://biology.plosjournals.org/perlserv/?request=get-document&doi=10.1371%2Fjournal.pbio.0030009)

  - Lerner, H. R. L. and D. P. Mindell (2005) Phylogeny of eagles, [Old
    World vultures](../Page/Old_World_vulture.md "wikilink"), and other
    Accipitridae based on [nuclear](../Page/Nuclear_DNA.md "wikilink")
    and [mitochondrial DNA](../Page/mitochondrial_DNA.md "wikilink").
    *Molecular Phylogenetics and Evolution* (37): 327-346. [PDF
    document](http://www-personal.umich.edu/~hlerner/LernerMindell2005Proofs.pdf)

[Category:真鵰屬](../Category/真鵰屬.md "wikilink")