**鞍山鋼鐵集團公司**，是[中華人民共和國國務院監管的](../Page/國務院國有資產監督管理委員會.md "wikilink")[中國中央企業](../Page/中國中央企業.md "wikilink")，是[中國大陸第三大](../Page/中國大陸.md "wikilink")[鋼鐵](../Page/鋼鐵.md "wikilink")[國有企業](../Page/國有企業.md "wikilink"),(2015年)計及民企中國第4大、產量世界第7大。\[1\]集團從事採礦、選礦、煉[鐵](../Page/鐵.md "wikilink")、煉[鋼到軋鋼綜合配套](../Page/鋼.md "wikilink")，以及焦化、耐火、動力、[運輸](../Page/運輸.md "wikilink")、冶金[機械](../Page/機械.md "wikilink")、[建設](../Page/建設.md "wikilink")、[技術研發](../Page/技術.md "wikilink")、[設計](../Page/設計.md "wikilink")、[自動化](../Page/自動化.md "wikilink")、綜合利用。公司總部設在[遼寧](../Page/遼寧.md "wikilink")[鞍山](../Page/鞍山.md "wikilink")。\[2\]

## 历史

集團的前身是[日本統治](../Page/日本.md "wikilink")[中國東北時期](../Page/中國東北.md "wikilink")[南滿洲鐵道株式會社在](../Page/南滿洲鐵道株式會社.md "wikilink")1916年成立的「[鞍山製鐵所](../Page/鞍山製鐵所.md "wikilink")」和「[昭和製鋼所](../Page/昭和製鋼所.md "wikilink")」。[二戰結束前遭到](../Page/二戰.md "wikilink")[蘇聯入侵拆遷設備損失慘重](../Page/蘇聯.md "wikilink")。

[國民政府接收東北後](../Page/國民政府.md "wikilink")，負責掌管全國重工業的[資源委員會蒐集東北剩餘練鋼設備改組成鞍山鋼鐵公司](../Page/資源委員會.md "wikilink")。[中國共產黨接收東北後鞍山鋼鐵公司仍維持既往編制](../Page/中國共產黨.md "wikilink")，並藉由蘇援重建成為中華人民共和國規模最大且最先進的鋼鐵生產公司，1997年，鞍山鋼鐵集團將冷軋廠、線材廠及厚板廠[資產注入](../Page/資產.md "wikilink")[子公司](../Page/子公司.md "wikilink")[鞍鋼股份](../Page/鞍鋼股份.md "wikilink")，同年並在[香港交易所及](../Page/香港交易所.md "wikilink")[深圳證券交易所](../Page/深圳證券交易所.md "wikilink")[上市](../Page/上市公司.md "wikilink")。\[3\]

2010年5月，鞍山鋼鐵集團公司與[攀鋼集團的合併計劃獲得](../Page/攀鋼集團.md "wikilink")[國資委的批准](../Page/國務院國有資產監督管理委員會.md "wikilink")，兩家公司聯合重組後，將合併為鞍山鋼鐵集團公司。\[4\]\[5\]

## 画廊

[File:1950-07-Anshan.png|1950年的鞍山钢铁](File:1950-07-Anshan.png%7C1950年的鞍山钢铁)
[File:1950-07-Anshan2.png|1950年的鞍山钢铁](File:1950-07-Anshan2.png%7C1950年的鞍山钢铁)
[File:1950-07-Anshan3.png|1950年的鞍山钢铁](File:1950-07-Anshan3.png%7C1950年的鞍山钢铁)
<File:1953-01> 1952年鞍山钢铁.png|1952年鞍山钢铁 <File:1953-01>
1952年鞍山钢铁25吨重调车.png|1952年鞍山钢铁25吨重调车 <File:1953-01>
1952年鞍山钢铁工程师关子祥与罗耀星检查铁矿石.png|1952年鞍山钢铁工程师关子祥与罗耀星检查铁矿石 <File:1953-01>
1952年鞍山钢铁结构金属加工厂.png|1952年鞍山钢铁结构金属加工厂 <File:1953-01>
1952年鞍山钢铁修建高炉.png|1952年鞍山钢铁修建高炉 <File:1962-01> 1962年
鞍山钢铁公司.jpg|1962年 鞍山钢铁公司 1962-04 1962年 鞍山钢铁 大钢锭.jpg|1962年 鞍山钢铁 大钢锭
1962-04 1962年 鞍山钢铁 钢桩设计 工程师龙春满 林健桩 赵季堃.jpg|1962年 鞍山钢铁 钢桩设计 工程师龙春满 林健桩
赵季堃 1962-04 1962年 鞍山钢铁 化工总厂炼焦炉.jpg|1962年 鞍山钢铁 化工总厂炼焦炉 1962-04
1962年 鞍山钢铁 无缝钢管厂.jpg|1962年 鞍山钢铁 无缝钢管厂 1962-04 1962年 鞍山钢铁
新产品六孔鱼尾板.jpg|1962年 鞍山钢铁 新产品六孔鱼尾板 1962-04 1962年 鞍山钢铁
选矿车间工人们正为高炉准备精矿粉.jpg|1962年 鞍山钢铁 选矿车间工人们正为高炉准备精矿粉
<File:1964-01> 1964年 鞍山钢铁公司.jpg|1964年 鞍山钢铁公司 1964-03 1964年
鞍钢夜大学.jpg|1964年 鞍钢夜大学 1964-03 1964年 鞍钢夜大学学生学习.jpg|1964年
鞍钢夜大学学生学习 1964-03 1964年 鞍钢夜大学学习.jpg|1964年 鞍钢夜大学学生学习
<File:1964-04> 1964年 鞍山钢铁公司生产的槽型钢板桩.jpg|1964年 鞍山钢铁公司生产的槽型钢板桩
<File:1965-5> 1965年 鞍山钢铁修建回收塔.jpg|1965年 鞍山钢铁修建回收塔 <File:1965-9> 1965
鞍山钢铁大型轧钢厂.jpg|1965 鞍山钢铁大型轧钢厂

## 参考来源

## 外部链接

  - [鞍山鋼鐵集團公司](http://www.ansteelgroup.com/)

[Category:总部在辽宁的中华人民共和国中央企业](../Category/总部在辽宁的中华人民共和国中央企业.md "wikilink")
[Category:中国大陆钢铁公司](../Category/中国大陆钢铁公司.md "wikilink")
[Category:鞍山公司](../Category/鞍山公司.md "wikilink")
[Category:1948年中國建立](../Category/1948年中國建立.md "wikilink")
[Category:1948年成立的公司](../Category/1948年成立的公司.md "wikilink")

1.
2.  [鞍山鋼鐵集團公司簡介](http://www.ansteelgroup.com/agjs1.htm)
3.  [鞍钢股份公司简介](http://money.finance.sina.com.cn/corp/go.php/vCI_CorpInfo/stockid/000898.phtml)
4.  [鞍鋼母准併攀鋼成一哥 行業重組揭幕
    注入上市公司需時](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20100525&sec_id=15307&subsec=15320&art_id=14064668)
    蘋果日報，2010年5月25日
5.  [母公司攀鋼獲准合併
    鞍鋼今復牌](http://hk.apple.nextmedia.com/realtime/art_main.php?iss_id=20100526&sec_id=7015342&subsec=6996645&art_id=14069604)
    蘋果日報即時財經，2010年5月26日