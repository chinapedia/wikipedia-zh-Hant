[Tatar-1.jpg](https://zh.wikipedia.org/wiki/File:Tatar-1.jpg "fig:Tatar-1.jpg")
[Steak_tartare098.jpg](https://zh.wikipedia.org/wiki/File:Steak_tartare098.jpg "fig:Steak_tartare098.jpg")
**韃靼牛肉**（Steak
tartare），又稱**他他牛肉**、**野人牛肉**，是新鮮的[牛肉](../Page/牛肉.md "wikilink")、[馬肉用刀剁碎而成的一道菜式](../Page/馬肉.md "wikilink")。\[1\]\[2\]傳統吃法通常加上[鹽](../Page/鹽.md "wikilink")、鮮磨的[胡椒粉](../Page/胡椒粉.md "wikilink")、[塔巴斯科辣椒醬](../Page/塔巴斯科辣椒醬.md "wikilink")（Tabasco）、[喼汁](../Page/喼汁.md "wikilink")（Sauce
Worcestershire）；意大利式吃法又加上[洋蔥末](../Page/洋蔥.md "wikilink")、[續隨子](../Page/續隨子.md "wikilink")（Câpre）、酸[黃瓜](../Page/黃瓜.md "wikilink")（cornichons）、西洋[香菜末](../Page/香菜.md "wikilink")（persil）、[大蒜末](../Page/大蒜.md "wikilink")、[橄欖油](../Page/橄欖.md "wikilink")，最後打上一顆鮮生蛋黃。

此菜仍然流行於[法國](../Page/法國.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[瑞士等地](../Page/瑞士.md "wikilink")，習慣上佐以一盤炸薯條（在[比利时稱為](../Page/比利时.md "wikilink")**美國牛柳**並佐以[吐司](../Page/吐司.md "wikilink")）。

## 历史

鞑靼牛肉的名称来自“*à la
tartare*”的缩写，一说为“佐[塔塔醬食用](../Page/塔塔醬.md "wikilink")”。\[3\]\[4\]

现代与生鸡蛋同食的鞑靼牛肉于20世纪初期在法国出现，而现时的鞑靼牛肉当时被称为“*steac k à
l'Americaine*（美式牛排），鞑靼牛肉则是“美式牛排”的变种。1921年[奥古斯特·埃斯科菲耶所著的](../Page/奥古斯特·埃斯科菲耶.md "wikilink")《》指出“美式牛排”一侧配以[塔塔醬](../Page/塔塔醬.md "wikilink")，且不添加生鸡蛋。

后来，“美式牛排”及其变种的区别逐渐消失，1938年编订的《Larousse
Gastronomique》已将鞑靼牛肉描述为配生鸡蛋黄的生牛肉末，且未提及塔塔酱。

鞑靼牛肉一名可能与中亚[鞑靼人有关](../Page/鞑靼人.md "wikilink")，但实际上与现时的鞑靼族饮食并无关系。\[5\]

“À la tartare”或是“tartare”这一表述现时对一些菜肴的意义仍为“搭配塔塔酱食用”，主要针对炸鱼。\[6\]

## 健康问题

由于可能造成的[细菌和寄生虫](../Page/细菌.md "wikilink")\[7\]（如[弓形虫和](../Page/弓形虫.md "wikilink")）污染，鞑靼牛肉在部分地区的盛行程度有所下降。然而，在符合卫生规范且使用鲜肉的条件下，鞑靼牛肉受细菌污染的可能性会降低。\[8\]即便如此，鞑靼牛肉仍然不适合孕妇（因鞑靼牛肉可能造成胎儿的先天性弓形体病）\[9\]、[免疫系统较弱或患有慢性病的人食用](../Page/免疫系统.md "wikilink")，因这些人更容易感染[大腸桿菌和](../Page/大腸桿菌.md "wikilink")[沙門氏菌屬](../Page/沙門氏菌屬.md "wikilink")。

[弓形虫可能会在生肉或欠熟的肉中寄生](../Page/弓形虫.md "wikilink")。\[10\]在食用生肉上的文化差异亦可能是不同地区间弓形虫病流行程度不同的原因（法国55%，英国仅10%）。\[11\]

2011年，[日本](../Page/日本.md "wikilink")[富山縣](../Page/富山縣.md "wikilink")、[福井县的](../Page/福井县.md "wikilink")[燒肉酒家惠比壽食物中毒事件](../Page/燒肉酒家惠比壽食物中毒事件.md "wikilink")，其中因0111型大肠杆菌等引起的溶血性尿毒症症候群(HUS)症状、或疑似此症的重症病患，共有23人。2名男童死亡。\[12\]

2008年[諾貝爾生理醫學獎得主](../Page/諾貝爾獎.md "wikilink")、病毒學專家[楚爾郝森](../Page/楚爾郝森.md "wikilink")（Harald
zur
Hausen）在[台灣地區的](../Page/台灣.md "wikilink")[高雄醫學大學](../Page/高雄醫學大學.md "wikilink")「2014傳染病與癌症國際研討會」上演講時，提出認為吃未熟牛肉易導致腸癌，常喝未高溫消毒牛奶者也易得乳癌，建議民眾注意。\[13\]

## 变种

[00_Steak_Tartare.jpg](https://zh.wikipedia.org/wiki/File:00_Steak_Tartare.jpg "fig:00_Steak_Tartare.jpg")[法国区的鞑靼牛肉](../Page/貝爾登巷.md "wikilink")\]\]

### 非洲

[埃塞俄比亚有一种与鞑靼牛肉相似的生牛肉食品](../Page/埃塞俄比亚.md "wikilink")，称（Kitfo，）。

### 亚洲

[韓國料理中的](../Page/韓國料理.md "wikilink")[肉膾与鞑靼牛肉相似](../Page/肉膾.md "wikilink")。

[泰國北部地區的](../Page/泰國北部地區.md "wikilink")[生肉沙拉由剁碎的生牛肉](../Page/生肉沙拉.md "wikilink")、生猪肉或是生[野豬肉制成](../Page/野豬.md "wikilink")，而其名称larb源于泰国北部方言中的“剁碎”一词。\[14\]

### 欧洲

[Filet_americain_on_bread.jpg](https://zh.wikipedia.org/wiki/File:Filet_americain_on_bread.jpg "fig:Filet_americain_on_bread.jpg")
鞑靼牛肉在[奥地利被称为Beef](../Page/奥地利.md "wikilink") tartare（不同于“鞑靼牛排”）。

在[瑞典](../Page/瑞典.md "wikilink")，鞑靼牛肉被称为Råbiff，常与生蛋黄、生洋葱、腌甜菜和[酸豆搭配食用](../Page/續隨子_\(山柑屬\).md "wikilink")。

[德国有一种名为Mett或Hackepeter的生猪肉食品](../Page/德国.md "wikilink")，一般覆盖在黑麦面包上食用。

鞑靼牛肉在[比利时被称为](../Page/比利时.md "wikilink")“*filet
américain*”（美国牛柳），搭配[馬鈴薯條食用](../Page/馬鈴薯條.md "wikilink")。

### 北美洲

[墨西哥式的鞑靼牛肉一般在](../Page/墨西哥.md "wikilink")[青檸汁中腌漬](../Page/青檸.md "wikilink")。

[加拿大亦有类似于鞑靼牛肉的生](../Page/加拿大.md "wikilink")[美洲野牛肉食品](../Page/美洲野牛属.md "wikilink")。\[15\]

### 南美

[智利有一种称为Crudos的生牛肉食品](../Page/智利.md "wikilink")。

## 參見

  - [肉膾](../Page/肉膾.md "wikilink")
  - [漢堡 (食物)](../Page/漢堡_\(食物\).md "wikilink")
  - [組合肉](../Page/組合肉.md "wikilink")

## 參考文獻

## 延伸阅读

  - Linda Stradley, *I'll Have What They're Having: Legendary Local
    Cuisine*, Falcon, 2002
  - Craig J. Smith, *The Raw Truth: Don't Blame the Mongols (or Their
    Horses)*, NY Times, 6 Apr 2005 \[16\]
  - Raymond Sokolov, *How to Cook*, revised edition 2004,
    ISBN 978-0-06-008391-5, p. 41 [at Google
    Books](http://books.google.com/books?id=iso1q7-jCn8C&pg=PT55)
  - Albert Jack, *What Caesar Did for My Salad: Not to Mention the
    Earl's Sandwich, Pavlova's Meringue and Other Curious Stories Behind
    Our Favourite Food*, 2010, ISBN 978-1-84614-254-3, p. 141 [at Google
    Books](http://books.google.com/books?id=uCzTNVCS45AC&pg=PT141)
  - 你不可不知的世界飲食史(作者：宮崎正勝)ISBN 978-986-5967-74-1

[Category:牛肉料理](../Category/牛肉料理.md "wikilink")
[Category:生肉類食品](../Category/生肉類食品.md "wikilink")

1.
2.  Raymond Sokolov, *The Cook's Canon*, 2003, ISBN 978-0-06-008390-8,
    p. 183 [at Google
    Books](http://books.google.com/books?id=KUml-NrkyegC&pg=PA183)
3.
4.  Albert Jack, *What Caesar Did for My Salad: Not to Mention the
    Earl's Sandwich, Pavlova's Meringue and Other Curious Stories Behind
    Our Favourite Food*, 2010, ISBN 978-1-84614-254-3, p. 141 [at Google
    Books](http://books.google.com/books?id=uCzTNVCS45AC&pg=PT141)
5.
6.  Prosper Montagné, Charlotte Snyder Turgeon, *The new Larousse
    gastronomique: the encyclopedia of food, wine & cookery*, 1977, p.
    334
7.
8.  <http://whqlibdoc.who.int/publications/2009/9789241547895_eng.pdf>
9.  <http://sogc.org/wp-content/uploads/2013/02/gui285CPG1301E-Toxoplasmosis.pdf?b2581b>
10.
11.
12.
13.
14. <http://shesimmers.com/2009/06/how-to-make-larb-gai-lahb-gai-laab-gai-larp-gai-laap-gai-lahb-gai-%E0%B8%A5%E0%B8%B2%E0%B8%9A%E0%B9%84%E0%B8%81%E0%B9%88.html>
    source by author in the comments
15.
16.