**韋家雄**（，），[香港男演員](../Page/香港.md "wikilink")，現為[無綫電視基本藝人合約藝員](../Page/無綫電視藝員列表.md "wikilink")。

## 背景

### 早年生活

韋家雄是香港著名[電視](../Page/電視.md "wikilink")[電影製作人](../Page/電影.md "wikilink")[韋家輝胞弟](../Page/韋家輝.md "wikilink")\[1\]。他小時候曾居住在[香港島](../Page/香港島.md "wikilink")[華富邨](../Page/華富邨.md "wikilink")\[2\]，並就讀邨內[廣悅堂基悅小學的下午校](../Page/廣悅堂基悅小學.md "wikilink")；當時未登上新聞主播的[蘇凌峰為其訓導主任](../Page/蘇凌峰.md "wikilink")。他父親為商人，在華富邨內開設小型塑膠廠經營生意，後因婚外情而離棄兒女及妻子，四人從此基於家貧關係，曾一度寄居在[九龍](../Page/九龍.md "wikilink")[土瓜灣的舅父家中](../Page/土瓜灣.md "wikilink")，但住了一段時間就獲政府分配單位，安排移居[山谷道邨](../Page/山谷道邨.md "wikilink")。

韋家雄畢業於[基督教香港信義會紅磡信義學校](../Page/基督教香港信義會紅磡信義學校.md "wikilink")、[孔仲岐紀念中學及](../Page/孔仲岐紀念中學.md "wikilink")[威靈頓英文中學尖沙咀分校](../Page/威靈頓英文中學.md "wikilink")，惟讀至中六便離校\[3\]。他為幫補家計，13歲已於每天凌晨在一間小型酒樓（地痞館）裡賣[點心到下午才上學](../Page/點心.md "wikilink")，同時亦學習製作[麵包以增加收入](../Page/麵包.md "wikilink")。

### 入行經過與發展

韋家雄入行前曾任[廣告](../Page/廣告.md "wikilink")[攝影師](../Page/攝影師.md "wikilink")，1989年報讀[亞洲電視藝員訓練班而入行](../Page/亞洲電視藝員訓練班.md "wikilink")，成為亞洲電視[演員](../Page/演員.md "wikilink")，1995年則轉投[無綫電視](../Page/無綫電視.md "wikilink")。此後他一度暫別電視圈，專注發展[LED燈生意](../Page/LED燈.md "wikilink")，其公司「安盟科技有限公司」設在[日本](../Page/日本.md "wikilink")[福岡](../Page/福岡.md "wikilink")，另有一間公司「傲高科技有限公司」是跟拍檔呂柏納共同經營\[4\]，曾接辦不少大型活動項目如[大除夕倒數](../Page/大除夕.md "wikilink")、[2008年北京奧運及](../Page/2008年北京奧運.md "wikilink")[上海世界博覽會法國館](../Page/上海世界博覽會.md "wikilink")[路易威登展覽等](../Page/路易威登.md "wikilink")，可因2011年[東日本大震災而欠下巨債](../Page/東日本大震災.md "wikilink")\[5\]。2013年還清債務後，他仍繼續發展其他生意，與[楊明合作開設後期製作公司](../Page/楊明.md "wikilink")\[6\]。

2015年12月13日，他於《[萬千星輝頒獎典禮2015](../Page/萬千星輝頒獎典禮2015.md "wikilink")》中憑「麥瀚林」（《[枭雄](../Page/枭雄.md "wikilink")》）一角奪得「最佳男配角」，獲獎時也再三感謝[黃秋生當頭棒喝的一番話](../Page/黃秋生.md "wikilink")，使其命運改變了\[7\]。2016年，韋家雄以「羅蟹」一角（《[公公出宮](../Page/公公出宮.md "wikilink")》）三度入圍《[萬千星輝頒獎典禮2016](../Page/萬千星輝頒獎典禮2016.md "wikilink")》「最佳男配角」。同年11月，有女性因在[港鐵車廂內被一中年男子偷拍重要部位而與其爭執](../Page/港鐵.md "wikilink")，韋家雄見狀即上前阻止，更力指對方不是。事後不少網民還讚賞他見義勇為\[8\]。另外，他是無綫電視監製[方駿釗和](../Page/方駿釗.md "wikilink")[陳耀全的常用演員](../Page/陳耀全.md "wikilink")。

## 感情生活

韋家雄和前妻育有一子；曾跟無綫電視藝員拍拖3年。2018年，他再與任職銀行的鄧楚雅（Keynes）拍拖\[9\]，同年10月6日於[旺角](../Page/旺角.md "wikilink")[帝景酒店結婚](../Page/帝景酒店.md "wikilink")\[10\]\[11\]\[12\]，婚禮以「先中式後西式」進行\[13\]\[14\]\[15\]。

## 演出作品

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1992年</p>
<center></td>
<td><p><a href="../Page/伯虎為卿狂.md" title="wikilink">伯虎為卿狂</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/今裝戲寶.md" title="wikilink">今裝戲寶</a></p></td>
<td><p>同　事（單元：玉女添丁）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>徒　弟（單元：難兄難弟）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>1993年</p>
<center></td>
<td><p><a href="../Page/賭神秘笈&#39;93.md" title="wikilink">賭神秘笈'93</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/銀狐_(電視劇).md" title="wikilink">銀狐</a></p></td>
<td><p>姚菊人手下</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中國教父.md" title="wikilink">中國教父</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬場風雲(電視劇).md" title="wikilink">馬場風雲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/點解阿sir係隻鬼II.md" title="wikilink">點解阿sir係隻鬼II</a></p></td>
<td><p>George 趙</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香港奇案.md" title="wikilink">香港奇案</a></p></td>
<td><p>（單元：血水箱）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>1994年</p>
<center></td>
<td><p><a href="../Page/皇家警察實錄II.md" title="wikilink">皇家警察實錄II</a></p></td>
<td><p>（單元：毒梟煞星）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戲王之王_(電視劇).md" title="wikilink">戲王之王</a></p></td>
<td><p>徐仲暉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郎心如鐵(1994年電視劇).md" title="wikilink">郎心如鐵</a></p></td>
<td><p>方曉欣之弟</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美夢成真.md" title="wikilink">美夢成真</a></p></td>
<td><p>明　哥</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>1995年</p>
<center></td>
<td><p><a href="../Page/法外英雄.md" title="wikilink">法外英雄</a></p></td>
<td><p>（單元：奪命哥羅芳）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/一屋兩鬼三人行.md" title="wikilink">一屋兩鬼三人行</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛者有其屋.md" title="wikilink">愛者有其屋</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1995 - 1999年</p>
<center></td>
<td><p><a href="../Page/真情.md" title="wikilink">真情</a></p></td>
<td><p>李子力</p></td>
</tr>
<tr class="odd">
<td><center>
<p>1996年</p>
<center></td>
<td><p><a href="../Page/O記實錄II.md" title="wikilink">O記實錄II</a></p></td>
<td><p>伍立光</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/900重案追兇.md" title="wikilink">900重案追兇</a></p></td>
<td><p>古　根</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>1997年</p>
<center></td>
<td><p><a href="../Page/迷離檔案.md" title="wikilink">迷離檔案</a></p></td>
<td><p>阿　貴</p></td>
</tr>
<tr class="even">
<td><center>
<p>1998年</p>
<center></td>
<td><p><a href="../Page/陀槍師姐.md" title="wikilink">陀槍師姐</a></p></td>
<td><p>鄭忠信</p></td>
</tr>
<tr class="odd">
<td><center>
<p>1999年</p>
<center></td>
<td><p><a href="../Page/全院滿座.md" title="wikilink">全院滿座</a></p></td>
<td><p>周偉堅</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刑事偵緝檔案IV.md" title="wikilink">刑事偵緝檔案IV</a></p></td>
<td><p>溫志森</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2000年</p>
<center></td>
<td><p><a href="../Page/撻出愛火花.md" title="wikilink">撻出愛火花</a></p></td>
<td><p>趙慶齊</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/醫神華佗.md" title="wikilink">醫神華佗</a></p></td>
<td><p>樊　阿</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2001年</p>
<center></td>
<td><p><a href="../Page/酒是故鄉醇.md" title="wikilink">酒是故鄉醇</a></p></td>
<td><p>何水清</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美麗人生_(香港電視劇).md" title="wikilink">美麗人生</a></p></td>
<td><p>前　進</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/封神榜_(2001年電視劇).md" title="wikilink">封神榜</a></p></td>
<td><p><a href="../Page/土行孫.md" title="wikilink">土行孫</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/倚天屠龍記.md" title="wikilink">倚天屠龍記</a></p></td>
<td><p>壽南山</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2002年</p>
<center></td>
<td><p><a href="../Page/皆大歡喜_(古裝電視劇).md" title="wikilink">皆大歡喜</a></p></td>
<td><p>馬　勤</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/再生緣_(2002年電視劇).md" title="wikilink">再生緣</a></p></td>
<td><p>阿桑哥</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/談判專家_(電視劇).md" title="wikilink">談判專家</a></p></td>
<td><p>齊家全</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/無考不成冤家.md" title="wikilink">無考不成冤家</a></p></td>
<td><p>曾英偉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2003年</p>
<center></td>
<td><p><a href="../Page/九五至尊.md" title="wikilink">九五至尊</a></p></td>
<td><p>癩皮華</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西關大少.md" title="wikilink">西關大少</a></p></td>
<td><p>梁　和</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皆大歡喜_(時裝電視劇).md" title="wikilink">皆大歡喜</a></p></td>
<td><p>Sam</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2004 - 2005年</p>
<center></td>
<td><p><a href="../Page/皆大歡喜_(時裝電視劇).md" title="wikilink">皆大歡喜</a></p></td>
<td><p>米　貴</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2004年</p>
<center></td>
<td><p><a href="../Page/烽火奇遇結良緣.md" title="wikilink">烽火奇遇結良緣</a></p></td>
<td><p>樊　武</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金枝慾孽.md" title="wikilink">金枝慾孽</a></p></td>
<td><p><strong>陳　爽</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水滸無間道.md" title="wikilink">水滸無間道</a></p></td>
<td><p>周國賢</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2005年</p>
<center></td>
<td><p><a href="../Page/牛郎織女_(無綫電視劇).md" title="wikilink">牛郎織女</a></p></td>
<td><p>土地公</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/胭脂水粉.md" title="wikilink">胭脂水粉</a></p></td>
<td><p>戴　廣</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2006年</p>
<center></td>
<td><p><a href="../Page/謎情家族.md" title="wikilink">謎情家族</a></p></td>
<td><p>高卓文（青年）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/法證先鋒.md" title="wikilink">法證先鋒</a></p></td>
<td><p>朱德安</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刑事情報科_(電視劇).md" title="wikilink">刑事情報科</a></p></td>
<td><p>郭明華</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2007年</p>
<center></td>
<td><p><a href="../Page/學警出更.md" title="wikilink">學警出更</a></p></td>
<td><p>鄭子強</p></td>
</tr>
<tr class="even">
<td><center>
<p>2009年</p>
<center></td>
<td><p><a href="../Page/老婆大人II.md" title="wikilink">老婆大人II</a></p></td>
<td><p>羅浩祈</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2010年</p>
<center></td>
<td><p><a href="../Page/飛女正傳.md" title="wikilink">飛女正傳</a></p></td>
<td><p>張偉信</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/施公奇案II.md" title="wikilink">施公奇案II</a></p></td>
<td><p>金毛鼠</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2012年</p>
<center></td>
<td><p><a href="../Page/4_In_Love_(電視劇).md" title="wikilink">4 In Love</a></p></td>
<td><p><strong>鍾炳良</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>2013年</p>
<center></td>
<td><p><a href="../Page/金枝慾孽貳.md" title="wikilink">金枝慾孽貳</a></p></td>
<td><p><strong>馬新滿</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/神探高倫布.md" title="wikilink">神探高倫布</a></p></td>
<td><p>中東男</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/師父·明白了.md" title="wikilink">師父·明白了</a></p></td>
<td><p><strong>白一一</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛·回家_(第一輯).md" title="wikilink">愛·回家</a></p></td>
<td><p>苗　生</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2014年</p>
<center></td>
<td><p><a href="../Page/單戀雙城.md" title="wikilink">單戀雙城</a></p></td>
<td><p><strong>專　榮</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新抱喜相逢.md" title="wikilink">新抱喜相逢</a></p></td>
<td><p>偉　哥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/守業者.md" title="wikilink">守業者</a></p></td>
<td><p>唐千斤</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛我請留言.md" title="wikilink">愛我請留言</a></p></td>
<td><p>父　親</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廉政行動2014.md" title="wikilink">廉政行動2014</a></p></td>
<td><p>趙富順（單元五）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大藥坊.md" title="wikilink">大藥坊</a></p></td>
<td><p>皇甫壽</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/再戰明天.md" title="wikilink">再戰明天</a></p></td>
<td><p><strong>杜子書（讀書仔）</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飛虎II.md" title="wikilink">飛虎II</a></p></td>
<td><p>田新喜</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/名門暗戰.md" title="wikilink">名門暗戰</a></p></td>
<td><p>簡仁信</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2015年</p>
<center></td>
<td><p><a href="../Page/四個女仔三個BAR.md" title="wikilink">四個女仔三個BAR</a></p></td>
<td><p>秦建安</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天眼_(香港電視劇).md" title="wikilink">天眼</a></p></td>
<td><p>江明威</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/收規華.md" title="wikilink">收規華</a></p></td>
<td><p><strong>石　九</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陪著你走_(電視劇).md" title="wikilink">陪著你走</a></p></td>
<td><p>吳德波</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無雙譜_(2015年電視劇).md" title="wikilink">無雙譜</a></p></td>
<td><p><strong>陸　判</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/枭雄.md" title="wikilink">枭雄</a></p></td>
<td><p><strong>麥瀚林</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2016年</p>
<center></td>
<td><p><a href="../Page/鐵馬戰車.md" title="wikilink">鐵馬戰車</a></p></td>
<td><p>戴馬成（大麻成）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公公出宮.md" title="wikilink">公公出宮</a></p></td>
<td><p>羅　蟹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/火線下的江湖大佬.md" title="wikilink">火線下的江湖大佬</a></p></td>
<td><p>何其爽（青年）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/一屋老友記.md" title="wikilink">一屋老友記</a></p></td>
<td><p>黃百川</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巨輪II.md" title="wikilink">巨輪II</a></p></td>
<td><p>司徒標</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2017年</p>
<center></td>
<td><p><a href="../Page/親親我好媽.md" title="wikilink">親親我好媽</a></p></td>
<td><p><strong>成吉思</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/心理追兇_Mind_Hunter.md" title="wikilink">心理追兇 Mind Hunter</a></p></td>
<td><p>盧健強</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/踩過界.md" title="wikilink">踩過界</a></p></td>
<td><p>趙翔鳳（青年）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/超時空男臣.md" title="wikilink">超時空男臣</a></p></td>
<td><p><strong>溫偉泰</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/降魔的.md" title="wikilink">降魔的</a></p></td>
<td><p>陳永廉（嘟嘟 Sir）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2018年</p>
<center></td>
<td><p><a href="../Page/平安谷之詭谷傳說.md" title="wikilink">平安谷之詭谷傳說</a></p></td>
<td><p>陸水雄</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮心計2深宮計.md" title="wikilink">宮心計2深宮計</a></p></td>
<td><p>成　恭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/跳躍生命線.md" title="wikilink">跳躍生命線</a></p></td>
<td><p>高行仁</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>未播映</p>
<center></td>
<td><p><a href="../Page/白色強人.md" title="wikilink">白色強人</a></p></td>
<td><p>鄭志堅</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/街坊財爺.md" title="wikilink">街坊財爺</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/牛下女高音.md" title="wikilink">牛下女高音</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>拍攝中</p>
<center></td>
<td><p><a href="../Page/大醬園.md" title="wikilink">大醬園</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/她她她的少女時代.md" title="wikilink">她她她的少女時代</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/降魔的2.0.md" title="wikilink">降魔的2.0</a></p></td>
<td><p>陳永廉（嘟嘟 Sir）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇（[香港電台](../Page/香港電台電視部.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>2009年</p>
<center></td>
<td><p><a href="../Page/有房出租.md" title="wikilink">有房出租</a></p></td>
<td><p>袁　總</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2010年</p>
<center></td>
<td><p><a href="../Page/有房出租_(第二輯).md" title="wikilink">有房出租2010</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/證義搜查線.md" title="wikilink">證義搜查線</a></p></td>
<td><p>汪大發</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2011年</p>
<center></td>
<td><p><a href="../Page/一屋住家人.md" title="wikilink">一屋住家人</a></p></td>
<td><p>勝</p></td>
</tr>
<tr class="even">
<td><center>
<p>2012年</p>
<center></td>
<td><p><a href="../Page/火速救兵II.md" title="wikilink">火速救兵II</a></p></td>
<td><p>馮老闆（單元：勇者無懼）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/手語隨想曲.md" title="wikilink">手語隨想曲</a></p></td>
<td><p>（單元：說不出的面試）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/私隱何價.md" title="wikilink">私隱何價</a></p></td>
<td><p>陳保安員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2013年</p>
<center></td>
<td><p><a href="../Page/一念之間2.md" title="wikilink">一念之間2</a></p></td>
<td><p>阿　南（單元：十分鐘）</p></td>
</tr>
<tr class="even">
<td><center>
<p>2014年</p>
<center></td>
<td><p><a href="../Page/總有出頭天.md" title="wikilink">總有出頭天</a></p></td>
<td><p>Q太郎</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我的家庭醫生.md" title="wikilink">我的家庭醫生</a></p></td>
<td><p>阿珊丈夫（單元：我睇夠醫生未？）</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2015年</p>
<center></td>
<td><p><a href="../Page/獅子山下2015.md" title="wikilink">獅子山下2015</a></p></td>
<td><p>梁惠玲前夫</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/監警有道.md" title="wikilink">監警有道</a></p></td>
<td><p>阿　雄</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2016年</p>
<center></td>
<td><p><a href="../Page/我的家庭醫生_II.md" title="wikilink">我的家庭醫生 II</a></p></td>
<td><p>雄　哥</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/有倉出租.md" title="wikilink">有倉出租</a></p></td>
<td><p>阿　雄（第8集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2017年</p>
<center></td>
<td><p><a href="../Page/反斗英語_2017.md" title="wikilink">反斗英語 2017</a></p></td>
<td><p>德　哥（第9集）</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 綜藝節目（無綫電視）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>節目名</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1997年</p>
<center></td>
<td><p><a href="../Page/超級無敵獎門人之再戰江湖.md" title="wikilink">超級無敵獎門人之再戰江湖</a></p></td>
<td><p>第13集嘉賓</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2010年</p>
<center></td>
<td><p><a href="../Page/超級遊戲獎門人.md" title="wikilink">超級遊戲獎門人</a></p></td>
<td><p>第4集嘉賓</p></td>
</tr>
<tr class="even">
<td><center>
<p>2012年</p>
<center></td>
<td><p><a href="../Page/登登登對.md" title="wikilink">登登登對</a></p></td>
<td><p>第17集嘉賓</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瘋狂歷史補習社.md" title="wikilink">瘋狂歷史補習社</a></p></td>
<td><p>演出第7集</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2015年</p>
<center></td>
<td><p><a href="../Page/一綫娛樂.md" title="wikilink">一綫娛樂</a></p></td>
<td><p>第9集嘉賓</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2016年</p>
<center></td>
<td><p><a href="../Page/新春開運王.md" title="wikilink">新春開運王</a></p></td>
<td><p>第3集嘉賓</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Sunday好戲王.md" title="wikilink">Sunday好戲王</a></p></td>
<td><p>第6集嘉賓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/男人食堂.md" title="wikilink">男人食堂</a></p></td>
<td><p>第5、16集嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2017年</p>
<center></td>
<td><p><a href="../Page/最緊要好玩.md" title="wikilink">最緊要好玩</a></p></td>
<td><p>演出</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>第2集嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/最緊要好玩.md" title="wikilink">最緊要好好玩 消暑版</a></p></td>
<td><p>演出</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/再親我好媽.md" title="wikilink">再親我好媽</a></p></td>
<td><p>演出第4集</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2018年</p>
<center></td>
<td><p><a href="../Page/一周八爪娛.md" title="wikilink">一周八爪娛</a></p></td>
<td><p>2月11日嘉賓</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/兄弟大茶飯.md" title="wikilink">兄弟大茶飯</a></p></td>
<td><p>第9集嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/TVB_50+1再出發節目巡禮2019.md" title="wikilink">TVB 50+1再出發節目巡禮2019</a></p></td>
<td><p>嘉賓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2019年</p>
<center></td>
<td><p><a href="../Page/無綫電視賀年節目列表.md" title="wikilink">金豬耀保良</a></p></td>
<td><p>演出嘉賓</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

<table>
<tbody>
<tr class="odd">
<td><p><strong>首映</strong></p></td>
<td><p><strong>電影名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1992年</p>
<center></td>
<td><p><a href="../Page/一千靈異夜之鬼拳師.md" title="wikilink">一千靈異夜之鬼拳師</a></p></td>
<td><p>趙汝南徒弟</p></td>
</tr>
<tr class="odd">
<td><center>
<p>1995年</p>
<center></td>
<td><p><a href="../Page/猛鬼屠房.md" title="wikilink">猛鬼屠房</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/霹靂火_(電影).md" title="wikilink">霹靂火</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/玻璃鎗的愛.md" title="wikilink">玻璃鎗的愛</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1996年</p>
<center></td>
<td><p><a href="../Page/孽慾追擊檔案之邪殺.md" title="wikilink">孽慾追擊檔案之邪殺</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/摩登菩呢提.md" title="wikilink">摩登菩呢提</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1997年</p>
<center></td>
<td><p><a href="../Page/旺角大家姐.md" title="wikilink">旺角大家姐</a>（兼任編劇）</p></td>
<td><p>阿　寶</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2001年</p>
<center></td>
<td><p><a href="../Page/敵對.md" title="wikilink">敵對</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/驚聲尖叫_(香港電影).md" title="wikilink">驚聲尖叫</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2002年</p>
<center></td>
<td><p><a href="../Page/野狼.md" title="wikilink">野狼</a></p></td>
<td><p>B　仔</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賤精先生.md" title="wikilink">賤精先生</a></p></td>
<td><p>阿　雄</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2009年</p>
<center></td>
<td><p><a href="../Page/旺角監獄.md" title="wikilink">旺角監獄</a></p></td>
<td><p>箭豬哥</p></td>
</tr>
<tr class="even">
<td><center>
<p>2010年</p>
<center></td>
<td><p><a href="../Page/最危險人物.md" title="wikilink">最危險人物</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2012年</p>
<center></td>
<td><p><a href="../Page/寒戰.md" title="wikilink">寒戰</a></p></td>
<td><p>黃　強</p></td>
</tr>
<tr class="even">
<td><center>
<p>2018年</p>
<center></td>
<td><p><a href="../Page/笑澳江湖.md" title="wikilink">笑澳江湖</a></p></td>
<td><p>段老闆</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 舞台劇

  - 2010年：《[無動機殺人犯](../Page/無動機殺人犯.md "wikilink")》

### 音樂錄像

  - 1997年：[丁子峻](../Page/丁子峻.md "wikilink")《一個露台》

## 曾獲獎項與提名

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>2013年</p>
<center></td>
<td><p><a href="../Page/萬千星輝頒獎典禮2013.md" title="wikilink">萬千星輝頒獎典禮2013</a>：最佳男配角「馬新滿」（最後五強） -<br />
《<a href="../Page/金枝慾孽貳.md" title="wikilink">金枝慾孽貳</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2015年</p>
<center></td>
<td><p><a href="../Page/萬千星輝頒獎典禮2015.md" title="wikilink">萬千星輝頒獎典禮2015</a>：最佳男配角「麥瀚林」 - 《<a href="../Page/梟雄.md" title="wikilink">梟雄</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2016年</p>
<center></td>
<td><p><a href="../Page/萬千星輝頒獎典禮2016.md" title="wikilink">萬千星輝頒獎典禮2016</a>：最佳男配角「羅蟹」 - 《<a href="../Page/公公出宮.md" title="wikilink">公公出宮</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2018年</p>
<center></td>
<td><p><a href="../Page/萬千星輝頒獎典禮2018.md" title="wikilink">萬千星輝頒獎典禮2018</a>：最佳男配角「成恭」 - 《<a href="../Page/宮心計2深宮計.md" title="wikilink">宮心計2深宮計</a>》</p></td>
<td><p>（最後五強）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香港電視大獎.md" title="wikilink">2018香港電視大獎</a>：男配角獎（劇集）「成恭」 - 《宮心計2深宮計》/「陸水雄」 - 《<a href="../Page/平安谷之詭谷傳說.md" title="wikilink">平安谷之詭谷傳說</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注釋

## 參考來源

## 外部連結

  -
  -
  -
[kar](../Category/韋姓.md "wikilink")
[Category:香港電視男演員](../Category/香港電視男演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:廣悅堂基悅小學校友](../Category/廣悅堂基悅小學校友.md "wikilink")
[Category:孔仲歧紀念中學校友](../Category/孔仲歧紀念中學校友.md "wikilink")
[Category:威靈頓英文中學校友](../Category/威靈頓英文中學校友.md "wikilink")

1.

2.

3.

4.  [捱足20年《 4 in
    Love》包皮男韋家雄唔靠大佬](http://www.ihktv.com/face-246-wikarhung.html)，《[HKChannel](../Page/HKChannel.md "wikilink")》，2012年2月8日

5.  [今日VIP 2015.12.31 -
    韋家雄](http://programme.tvb.com/variety/greenroom/episode/20151231/)，今日VIP
    - 每集內容，2015年12月31日

6.  [姚子羚
    韋家雄等到了](http://hd.stheadline.com/ent/ent_content.asp?track=news&contid=356581&srctype=n)，《[頭條網](../Page/頭條日報.md "wikilink")》，2015年12月31日

7.  [最佳男配角韋家雄：世界沒有不勞而獲](http://topick.hket.com/article/1087379/%e6%9c%80%e4%bd%b3%e7%94%b7%e9%85%8d%e8%a7%92%e9%9f%8b%e5%ae%b6%e9%9b%84%ef%bc%9a%e4%b8%96%e7%95%8c%e6%b2%92%e6%9c%89%e4%b8%8d%e5%8b%9e%e8%80%8c%e7%8d%b2)，《[香港經濟日報](../Page/香港經濟日報.md "wikilink")》，2015年12月23日

8.  [【有片】港鐵阿叔疑偷拍女乘客
    藝人韋家雄阻止　網民大讚真漢子](https://www.hk01.com/%E6%B8%AF%E8%81%9E/67295/-%E6%9C%89%E7%89%87-%E6%B8%AF%E9%90%B5%E9%98%BF%E5%8F%94%E7%96%91%E5%81%B7%E6%8B%8D%E5%A5%B3%E4%B9%98%E5%AE%A2-%E8%97%9D%E4%BA%BA%E9%9F%8B%E5%AE%B6%E9%9B%84%E9%98%BB%E6%AD%A2-%E7%B6%B2%E6%B0%91%E5%A4%A7%E8%AE%9A%E7%9C%9F%E6%BC%A2%E5%AD%90)，《香港01》，2017年1月22日

9.  [【拍拖兩個月】52歲韋家雄閃娶銀行之花：個仔戥我開心](http://nextplus.nextmedia.com/news/recommend/20180213/571579)，《[壹週Plus](../Page/壹週刊.md "wikilink")》，2018年2月13日

10.
11.

12. [娶細廿年女友　韋家雄擬結婚通知書曝光](http://hk.on.cc/hk/bkn/cnt/entertainment/20180720/bkn-20180720170158189-0720_00862_001.html)，《東網》，2018年7月20日

13. [韋家雄娶老婆
    跟足傳統上頭食湯丸](http://hd.stheadline.com/life/ent/realtime/1334181/)，《頭條網》，2018年10月6日

14. [【率先睇】53歲韋家雄接新娘 愛的宣言得三個字：I Love
    Her](https://hk.entertainment.appledaily.com/enews/realtime/article/20181006/58762693)，《[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")》，2018年10月6日

15. [不想愛妻太快「脹卜卜」
    韋家雄想先享受二人世界](http://paper.wenweipo.com/2018/10/07/EN1810070002.htm)，《[香港文匯報](../Page/香港文匯報.md "wikilink")》，2018年10月7日