[AsianaHQsign0.jpg](https://zh.wikipedia.org/wiki/File:AsianaHQsign0.jpg "fig:AsianaHQsign0.jpg")
[HL8284@HKG_(20181006112351).jpg](https://zh.wikipedia.org/wiki/File:HL8284@HKG_\(20181006112351\).jpg "fig:HL8284@HKG_(20181006112351).jpg")-200ER，即將降落於[香港國際機場](../Page/香港國際機場.md "wikilink")\]\]
[Asiana_Airlines_A320-200_HL7722.JPG](https://zh.wikipedia.org/wiki/File:Asiana_Airlines_A320-200_HL7722.JPG "fig:Asiana_Airlines_A320-200_HL7722.JPG")

**韓亞航空**（，，，前称**首爾（汉城）航空**）是一間以[首爾為基地的大韓民國第二大](../Page/首爾.md "wikilink")[航空公司](../Page/航空公司.md "wikilink")（僅次於[大韓航空](../Page/大韓航空.md "wikilink")），與[大韓航空同為韓國的兩間主流航空公司](../Page/大韓航空.md "wikilink")。韓亞航空是九間在[Skytrax評估中獲得五星級的航空公司之一](../Page/Skytrax.md "wikilink")。\[1\]

韓亞航空是[星空聯盟成員](../Page/星空聯盟.md "wikilink")，共有106個航點，公司總部及國際線[樞紐機場位於鄰近首爾的](../Page/樞紐機場.md "wikilink")[仁川國際機場](../Page/仁川國際機場.md "wikilink")，而國內樞紐則是首爾[金浦國際機場](../Page/金浦國際機場.md "wikilink")。

## 歷史

韓亞航空於1988年2月17日成立，並在同年12月23日開辦往[釜山的航班](../Page/釜山.md "wikilink")，而1988至1990年採用之徽號是人類雙手張開圖案，並在[濟州與](../Page/濟州.md "wikilink")[首爾作為國內航線的中樞點](../Page/首爾.md "wikilink")。韓亞航空是由母公司[錦湖韓亞集團根據](../Page/錦湖韓亞集團.md "wikilink")[韓國政府政策成立韓國第二家](../Page/韓國政府.md "wikilink")[國家航空公司](../Page/國家航空公司.md "wikilink")，原稱作**首爾航空**。韓亞航空自1988年成立以來迅速發展，韓亞航成立並不像26年前[大韓航空那樣艱辛以兩架二手飛機起家](../Page/大韓航空.md "wikilink")，反之，韓亞航在正式對外營運的半年內，即1989年4月11日引進愛爾蘭GPA集團租借的6架[波音737-300客機](../Page/波音737-300.md "wikilink")，創業之初，公司便以高品質的服務策略取代廉價的高密度載客量，成功的行銷策略為韓亞航帶來龐大的收益。

在1990年4月，僅成立16個月的韓亞航空就達成了3百萬人次的搭載紀錄，並在日本[東京](../Page/東京.md "wikilink")、[名古屋](../Page/名古屋.md "wikilink")、[仙台與](../Page/仙台.md "wikilink")[福岡設立國際航線](../Page/福岡.md "wikilink")。在成立不到2年且並未獲利的情況下，韓亞航空在1990年9月1日與[波音簽署了](../Page/波音.md "wikilink")60億美金的購機計劃，韓亞航空一共簽署了27架波音飛機訂單，內容包括9架[波音747-400](../Page/波音747-400.md "wikilink")、10架[波音767-300與](../Page/波音767-300.md "wikilink")8架[波音737-400以及另外](../Page/波音737-400.md "wikilink")24架選擇權訂單。

在1991年初，韓亞航空龐大的機隊發展計劃已經將航線從原本的韓國與日本拓展至[台北](../Page/台北.md "wikilink")、[香港](../Page/香港.md "wikilink")、[曼谷](../Page/曼谷.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")，11月底韓亞航空啟用了一架可搭载279名乘客與34噸貨物的波音747-400
combi機型，營運往返[首爾與](../Page/首爾.md "wikilink")[洛杉磯的跨洋直航航線](../Page/洛杉磯.md "wikilink")。由於洛杉磯航線的成功，韓亞航空又增加了[舊金山與](../Page/舊金山.md "wikilink")[紐約的航點路線](../Page/紐約.md "wikilink")，在美國4個主要門戶機場，韓亞航空在開航3年內就已經佔了3個航點。拜亞洲經濟起飛與美國開放天空之賜，讓韓亞航空得以快速成長。在美國航線成功後，韓亞開始放慢擴展步伐，但在1993年，在大韓航空成功開拓[越南](../Page/越南.md "wikilink")[胡志明市的航線後的一個月](../Page/胡志明市.md "wikilink")，韓亞航空亦加入越南市場。後來韓亞航空於2003年加入了[星空聯盟](../Page/星空聯盟.md "wikilink")，星空聯盟內龐大的[中國航點需求](../Page/中國.md "wikilink")，讓韓亞航空在中國已經擁有18個航點，提供了將近600個航班與16萬個座位，成為一家中型的國際航空公司，機隊共有69架飛機，營運往21個國家66個城市82條航線，及國內12個城市15條航線。

2007年4月18日，韓亞航空聯同[國泰航空](../Page/國泰航空.md "wikilink")、[馬來西亞航空](../Page/馬來西亞航空.md "wikilink")、[卡塔尔航空](../Page/卡塔尔航空.md "wikilink")、[新加坡航空及](../Page/新加坡航空.md "wikilink")[翠鳥航空獲](../Page/翠鳥航空.md "wikilink")[Skytrax評為五星航空公司](../Page/Skytrax.md "wikilink")。2009年2月17日更被[Air
Transport
World](http://www.atwonline.com/)選為「年度航空公司」，到2010年更被[Skytrax評為年度全球最佳航空公司第一](../Page/Skytrax.md "wikilink")。

## 航點

## 代碼共享

韓亞航空與以下航空公司實行[代碼共享](../Page/代碼共享.md "wikilink")：

  - [阿斯塔納航空](../Page/阿斯塔納航空.md "wikilink")
  - [釜山航空](../Page/釜山航空.md "wikilink")（子公司）
  - [加拿大航空](../Page/加拿大航空.md "wikilink")\*
  - [中國國際航空](../Page/中國國際航空.md "wikilink")\*
  - [印度航空](../Page/印度航空.md "wikilink")\*
  - [澳門航空](../Page/澳門航空.md "wikilink")
  - [紐西蘭航空](../Page/紐西蘭航空.md "wikilink")\*
  - [首爾航空](../Page/首爾航空.md "wikilink")（子公司）
  - [全日空](../Page/全日空.md "wikilink")\*
  - [奧地利航空](../Page/奧地利航空.md "wikilink")\*
  - [中國南方航空](../Page/中國南方航空.md "wikilink")
  - [巴拿馬航空](../Page/巴拿馬航空.md "wikilink")\*
  - [克羅埃西亞航空](../Page/克羅埃西亞航空.md "wikilink")\*
  - [埃塞俄比亞航空](../Page/埃塞俄比亞航空.md "wikilink")\*
  - [阿提哈德航空](../Page/阿提哈德航空.md "wikilink")
  - [長榮航空](../Page/長榮航空.md "wikilink")\*
  - [香港航空](../Page/香港航空.md "wikilink")
  - [LOT波蘭航空](../Page/LOT波蘭航空.md "wikilink")\*
  - [澳洲航空](../Page/澳洲航空.md "wikilink")^
  - [卡達航空](../Page/卡達航空.md "wikilink")^
  - [西伯利亚航空](../Page/西伯利亚航空.md "wikilink")^
  - [山東航空](../Page/山東航空.md "wikilink")
  - [深圳航空](../Page/深圳航空.md "wikilink")\*
  - [新加坡航空](../Page/新加坡航空.md "wikilink")\*
  - [南非航空](../Page/南非航空.md "wikilink")\*
  - [斯里蘭卡航空](../Page/斯里蘭卡航空.md "wikilink")^
  - [泰國國際航空](../Page/泰國國際航空.md "wikilink")\*
  - [土耳其航空](../Page/土耳其航空.md "wikilink")\*
  - [聯合航空](../Page/聯合航空.md "wikilink")\*
  - [德國漢莎航空](../Page/德國漢莎航空.md "wikilink")\*

<small>\*[星空聯盟成員](../Page/星空聯盟.md "wikilink")
^[寰宇一家成員](../Page/寰宇一家.md "wikilink")</small>

## 現役機隊

[AAR_B767_HL7516.jpg](https://zh.wikipedia.org/wiki/File:AAR_B767_HL7516.jpg "fig:AAR_B767_HL7516.jpg")型客機\]\]
[HL8259@PEK_(20180820144944).jpg](https://zh.wikipedia.org/wiki/File:HL8259@PEK_\(20180820144944\).jpg "fig:HL8259@PEK_(20180820144944).jpg")型客機正降落[北京首都國際機場](../Page/北京首都國際機場.md "wikilink")\]\]
[HL8308@HKG_(20181026135015).jpg](https://zh.wikipedia.org/wiki/File:HL8308@HKG_\(20181026135015\).jpg "fig:HL8308@HKG_(20181026135015).jpg")型客機於[香港國際機場起飛](../Page/香港國際機場.md "wikilink")\]\]
[HL7625@HKG_(20181205135851).jpg](https://zh.wikipedia.org/wiki/File:HL7625@HKG_\(20181205135851\).jpg "fig:HL7625@HKG_(20181205135851).jpg")型客機於[香港國際機場](../Page/香港國際機場.md "wikilink")\]\]
[HL7420@HKG_(20190321114517).jpg](https://zh.wikipedia.org/wiki/File:HL7420@HKG_\(20190321114517\).jpg "fig:HL7420@HKG_(20190321114517).jpg")型貨機\]\]

截至2018年4月，韓亞航空擁有下列飛機\[2\]\[3\]：

<center>

<table>
<caption><strong>韓亞航空客機機隊</strong></caption>
<thead>
<tr class="header">
<th><p><font color=white>機型</p></th>
<th><p><font color=white>服役中</p></th>
<th><p><font color=white>已訂購</p></th>
<th><p><font color=white>載客量</p></th>
<th><p><font color=white>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><abbr title="頭等艙"><font color=white>P</abbr></p></td>
<td><p><abbr title="商務艙"><font color=white>J</abbr></p></td>
<td><p><abbr title="特選經濟艙"><font color=white>S</abbr></p></td>
<td><p><abbr title="經濟艙"><font color=white>Y</abbr></p></td>
<td><p><font color=white>總數</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A320.md" title="wikilink">空中巴士A320-231</a></p></td>
<td><p>7</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A320.md" title="wikilink">空中巴士A321-131</a></p></td>
<td><p>2</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A321.md" title="wikilink">空中巴士A321-231</a></p></td>
<td><p>17</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>12<br />
—</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A321.md" title="wikilink">空中巴士A321-251neo</a></p></td>
<td><p>—</p></td>
<td><p>25</p></td>
<td><p>即將公布</p></td>
<td><p>預計於2019年起交付。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A330.md" title="wikilink">空中巴士A330-323</a></p></td>
<td><p>15</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>30</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A350.md" title="wikilink">空中巴士A350-941</a></p></td>
<td><p>6</p></td>
<td><p>15</p></td>
<td><p>—</p></td>
<td><p>28</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A350.md" title="wikilink">空中巴士A350-1041</a></p></td>
<td><p>—</p></td>
<td><p>9</p></td>
<td><p>即將公布</p></td>
<td><p>預計於2020年起交付。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A380.md" title="wikilink">空中巴士A380-841</a></p></td>
<td><p>6</p></td>
<td><p>—</p></td>
<td><p>12</p></td>
<td><p>66</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-48E</a></p></td>
<td><p>2</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音767.md" title="wikilink">波音767-38E</a></p></td>
<td><p>7</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>15<br />
—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音777.md" title="wikilink">波音777-200ER</a></p></td>
<td><p>9</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>22<br />
24</p></td>
</tr>
<tr class="odd">
<td><p><font color=white>貨機機隊</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-48EF</a></p></td>
<td><p>4</p></td>
<td><p>—</p></td>
<td><p>120,000公斤<br />
（265,000磅）</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-48EBDSF</a></p></td>
<td><p>7</p></td>
<td><p>—</p></td>
<td><p>120,000公斤<br />
（265,000磅）</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音767.md" title="wikilink">波音767-38EERF</a></p></td>
<td><p>1</p></td>
<td><p>—</p></td>
<td><p>54,000公斤<br />
（119,000磅）</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>總數</p></td>
<td><p>71</p></td>
<td><p>47</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</center>

## 退役機隊

  - [波音737-400](../Page/波音737.md "wikilink")
  - [波音737-500](../Page/波音737.md "wikilink")
  - [波音747-48M](../Page/波音747.md "wikilink")
  - [波音767-38EER](../Page/波音767.md "wikilink")

## 意外及爭議事件

  - 1993年7月26日，編號HL7229的[波音737-5L9客機執飛](../Page/波音737#737-500.md "wikilink")[韓亞航空733號班機](../Page/韓亞航空733號班機空難.md "wikilink")，在[木浦市因天氣惡劣撞山墜毀](../Page/木浦市.md "wikilink")，68人死亡。
  - 2011年6月17日，一架载有119人的[空中巴士A321執飛](../Page/空中客车A321.md "wikilink")[韩亚航空324号班机](../Page/韓國士兵誤向客機開火事件.md "wikilink")，从[成都双流国际机场起飞前往](../Page/成都双流国际机场.md "wikilink")[首爾](../Page/首爾.md "wikilink")[仁川国际机场的在即将降落时被韩国驻守仁川外海乔桐岛军队误认为是朝鲜战机并用步槍进行了](../Page/仁川国际机场.md "wikilink")10分钟的射击，不过由於客機的距離有500－600米，子彈沒有擊中客機，機上乘客亦無感覺飞机被射擊。客機最终安全降落，没有造成人员伤亡。军方称这架客机偏离了正常航道，但韩亚航空对此予以否认\[4\]。
  - 2011年7月28日：一架波音747-48EF貨機（註冊編號：HL7604）執飛[韓亞航空991號班機](../Page/韓亞航空991號班機空難.md "wikilink")，從首爾仁川國際機場飛往[上海](../Page/上海.md "wikilink")[浦東國際機場期間](../Page/浦東國際機場.md "wikilink")，報稱因「機件故障」需要折返，在濟州外海附近墜毀，2名機員死亡。\[5\]

[NTSBAsiana214Fuselage2.jpg](https://zh.wikipedia.org/wiki/File:NTSBAsiana214Fuselage2.jpg "fig:NTSBAsiana214Fuselage2.jpg")降落時失事起火。\]\]

  - 2013年7月7日：一架[波音777-28E
    ER](../Page/波音777#777-200ER.md "wikilink")（註冊編號：HL7742）執飛[韓亞航空214航班从](../Page/韓亞航空214號班機空難.md "wikilink")[仁川国际机场起飞前往](../Page/仁川国际机场.md "wikilink")[舊金山國際機場](../Page/舊金山國際機場.md "wikilink")，於[舊金山時間](../Page/舊金山.md "wikilink")2013年7月6日上午11時36分在舊金山國際機場降落時失事起火，载291名乘客和16名机组人员，其中有141名中国游客。目前已造成3名中國人死亡\[6\]，百多人受傷，多數傷者皆因飛機撞擊力道過大，造成脊椎受傷或斷裂。整件事故正在調查階段，但目前指出此事故應為人為疏失。韓亞航空公司證實，訓練教官李貞珉（Lee
    Jeong-min，譯音）1個月前剛領到波音777型客機教官執照。李貞珉被指派指導李康國（Lee
    Kang-kook，譯音）降落事宜。李貞珉擁有數千小時的飛行經驗，但卻是第一天當教官，機師李康國也是第一次駕駛該型號飛機降落\[7\]\[8\]。
  - 2014年3月17日：一名[張姓女](../Page/張姓.md "wikilink")[模特兒乘坐韓亞航空從首爾仁川国际机场飛往](../Page/模特兒.md "wikilink")[法國](../Page/法國.md "wikilink")[巴黎的航班商務客位](../Page/巴黎.md "wikilink")，當空服員向她奉上[杯麵時將滾燙湯汁灑到她身上](../Page/杯麵.md "wikilink")，致其私處、腹部和大腿二至三度燙傷，她對韓亞提出訴訟，索償200萬韩圆。張女指航空公司未有妥善處理傷口，機上也沒有急救用品，私處受傷也嚴重影響她和丈夫的性生活。法院最終於2018年1月17日判決韓亞須賠償張女1.1億韩圆\[9\]。
  - 2015年4月14日：一架[空中巴士A320客机執飛](../Page/空中客车A320.md "wikilink")[韓亞航空162航班從首爾仁川國際機場起飞](../Page/韓亞航空162號班機空難.md "wikilink")，於晚上8時許在位于[日本](../Page/日本.md "wikilink")[广岛县](../Page/广岛县.md "wikilink")[三原市的](../Page/三原市.md "wikilink")[广岛机场着陆后不久滑出跑道](../Page/广岛机场.md "wikilink")，之后停止滑行。机上載有乘客74人，此前有媒体报道称机上23名乘客受轻伤，据三原市消防总部介绍，机上22名乘客受轻伤。报道称，客机从机场西面跑道降落后，失控冲出跑道，滑向跑道南面草坪后停下，机组人员之后放出逃生滑梯，疏散机上所有人，并步行至客运大楼。日本[国土交通省调查后称](../Page/国土交通省.md "wikilink")，客机起落架在降落时，撞到距离跑道尽头约325米的6.4米高天线，怀疑因此导致失控。据悉，肇事客机于当地时间14日晚上6时50分从仁川国际机场出发，于8时5分降落广岛机场，较预定迟5分钟。\[10\]
  - 2018年5月14日：当地时间17时46分，韩亚航空一架[空中巴士A330客机在土耳其](../Page/空中巴士A330.md "wikilink")[伊斯坦布尔阿塔图尔克机场与](../Page/伊斯坦布尔阿塔图尔克机场.md "wikilink")[土耳其航空一架](../Page/土耳其航空.md "wikilink")[空中巴士A321客机相撞](../Page/空中客车A321.md "wikilink")，土耳其航空的飛機尾舵被扯下，幸未造成人员伤亡。\[11\]

## 電視劇

韓亞航空为韓國[SBS電視劇](../Page/SBS_\(韓國\).md "wikilink")：《[拜託了，機長](../Page/拜託了，機長.md "wikilink")》（）的赞助航空公司。

## 參見

  - [釜山航空](../Page/釜山航空.md "wikilink")（子公司）
  - [首爾航空](../Page/首爾航空.md "wikilink")（子公司）

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [韓亞航空](http://www.flyasiana.com)

  -
  -
  -
[韩亚航空](../Category/韩亚航空.md "wikilink")
[Category:韓國航空公司](../Category/韓國航空公司.md "wikilink")
[Category:1988年成立的航空公司](../Category/1988年成立的航空公司.md "wikilink")
[Category:1988年韓國建立](../Category/1988年韓國建立.md "wikilink")

1.  [Skytrax: 5-Star
    Airlines](http://www.airlinequality.com/StarRanking/5star.htm)
2.  [asms.casa.go.kr](http://atis.casa.go.kr/NewAsms/asms_safe/menu01.asp?menu=16)
    . Atis.casa.go.kr. Retrieved 15 December 2010.
3.  [\[한국의 일등
    상품](http://news.chosun.com/site/data/html_dir/2009/10/29/2009102901574.html)
    아시아나항공\]
4.
5.
6.  <http://m.rthk.hk/news/20130713/935186.htm>
7.  <http://rthk.hk/mobile/news/20130707/933743.htm>
8.  [降落777
    韓亞航教官和機師都是菜鳥](http://tw.news.yahoo.com/降落777-韓亞航教官和機師都是菜鳥-055003858.html)
9.
10. [1](http://www.chinanews.com/gj/2015/04-15/7207744.shtml)
11. [2](https://www.thepaper.cn/newsDetail_forward_2128289)