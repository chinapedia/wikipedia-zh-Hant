**韓國童軍總會**（），為[韓國的國家童軍組織](../Page/韓國.md "wikilink")。

韓國童軍運動是在1922年10月於[日本統治下開始發展](../Page/日本.md "wikilink")，並於1924年派代表，參加在北京舉辦的第1次遠東童軍競賽。然而，從1937年到1945年8月15日，日本當局下令禁止童軍運動\[1\]。1950年[韓戰爆發前](../Page/韓戰.md "wikilink")，朝鮮半島各地皆存在著童軍組織。1953年，[世界童軍運動組織承認韓國童軍總會](../Page/世界童軍運動組織.md "wikilink")。截至2011年，共有201,455位童軍成員\[2\]。

## 制度和象徵

[Korean_Scout_Uniforms_2007.jpg](https://zh.wikipedia.org/wiki/File:Korean_Scout_Uniforms_2007.jpg "fig:Korean_Scout_Uniforms_2007.jpg")
[老虎童軍為](../Page/老虎童軍.md "wikilink")[童軍和](../Page/童軍.md "wikilink")[冒險童軍的最高等級並頒給通過此等級考驗的童軍](../Page/冒險童軍.md "wikilink")。

韓國童軍的銘言為**준비**，韓文中的**準備**。

## 朝鮮的童軍活動

[朝鮮在](../Page/朝鮮.md "wikilink")1950年以前與韓國童軍總會一起活動，但現今朝鮮為尚未有童軍活動的6個獨立國家之一。

## 參考文獻

## 外部連結

  - [韓國童軍總會](http://www.scout.or.kr/index.asp)

[Category:各國童軍活動](../Category/各國童軍活動.md "wikilink")

1.
2.