**組織暴力團**（），簡稱**組暴**（），是[韓國對](../Page/大韓民國.md "wikilink")[黑社會暴力組織的稱呼](../Page/黑社會.md "wikilink")。

依據韓國警方於2007年10月公佈，目前在[韓國活動的組織暴力團多達](../Page/大韓民國.md "wikilink")471個**派**，暴力輩（幫派成員）有1萬1476人。

其中有167個派被列為特別管理對象。

此外，根據資料顯示暴力輩人數在5年間增加了17.8%、組織增加6.7%。釜山的暴力輩人數最多有1833人。

全國規模較大型的組織暴力團幾乎都在[京畿道活動](../Page/京畿道.md "wikilink")，尤其以平澤、安城一帶為地盤，

在2001年創立的“清河蔚生派”（청하위생파；日文「庁下衛生派」）為當前全[韓國境內最大的組織暴力團](../Page/大韓民國.md "wikilink")，

該組織暴力輩有76名；另外，分別在京畿[水原活動的南門派](../Page/水原.md "wikilink")（남문파）及[京畿活動的逆轉派](../Page/京畿.md "wikilink")（역전파），兩個組織的暴力輩人數都達70人以上。

## 簡介

[韓國的組織暴力團發展大致分為](../Page/大韓民國.md "wikilink")5個時期；分別為最早的「**萌芽時期**」，組織暴力團隨著工商的興起而開始發展，此時，大多靠拳頭慢慢建立起權勢。

1910年代，[日本统治时期至](../Page/朝鲜日治时期.md "wikilink")[韓國光復後的](../Page/大韓民國.md "wikilink")1950年代這段時期為「**浪漫派暴力時期**」，這個時期的農村[浪人逐漸移往城市發展而聚集起來](../Page/浪人.md "wikilink")。當時，[京城](../Page/漢城.md "wikilink")（今首爾）被分為以韓國人為主的[鍾路商圈及以日本人為主的](../Page/鍾路區.md "wikilink")[明洞商圈](../Page/明洞.md "wikilink")；在明洞商圈勢力最大的知名親分老大為“林組長”（在日韓國人、本名音譯「鮮宇英賓」）他追隨著日本名人[頭山滿來到韓國發展](../Page/頭山滿.md "wikilink")，並掌控韓國[明洞當地的高級居酒屋市場](../Page/明洞.md "wikilink")；另一方面，[鍾路商圈的](../Page/鍾路區.md "wikilink")[高熙慶](../Page/高熙慶.md "wikilink")（音譯），他掌握了[鍾路商圈遊興街](../Page/鍾路區.md "wikilink")（同日本繁華街）的地盤，在當代被視為[朝鮮最高的頭目](../Page/朝鮮.md "wikilink")，與同在鍾路商圈發展的[嚴東旭](../Page/嚴東旭.md "wikilink")（音譯）、綽號"番人"的[金斗漢](../Page/金斗漢.md "wikilink")（其父親為韓國抗日名將[金佐鎮](../Page/金佐鎮.md "wikilink")），以上3者，構成當時[朝鮮的](../Page/朝鮮.md "wikilink")3大勢力版圖。直到1934年，18歲的金斗漢，先後打敗了嚴東旭及高熙慶一統韓國人的勢力，與“林組長”形成日、韓兩股代表勢力相對抗的版圖。在一次金斗漢襲擊[日本黑幫林組的](../Page/日本.md "wikilink")"東洋貿易"，讓他成為家戶逾曉的人物，日後他被視為是韓國俠道的抗日英雄。

1950年代的[自由黨政權時代](../Page/自由黨.md "wikilink")（[李承晚創立](../Page/李承晚.md "wikilink")），組暴份子與政治勾結共生，為「**政治黑道人物共生時期**」；這個時期的政治人物結合組織暴力團的力量以鞏固政權。1950年代朝鮮獨立後，金斗漢接收日本黑幫“林組”撤出的地盤成為當時勢力最大的大頭目。由於韓國獨立不久，警備勢力薄弱，為了維護治安因此假借了金斗漢的力量，也因為這樣的理由，奠定了金斗漢日後參政的根基。在1953年金斗漢決議退出江湖後，便以[東大門商人工會理事長的身分參與政治活動](../Page/東大門市場.md "wikilink")，身為右翼政治家的他，組織了[反共青年團](../Page/反共青年團.md "wikilink")，並分別當選兩屆[國會議員](../Page/國會議員.md "wikilink")；但是，該政團在[516軍事政變事件後消失無蹤](../Page/516軍事政變.md "wikilink")。這時期，除了金斗漢這個時代人物之外；[李聖淳他被視為是繼承金斗漢勢力的兩大頭目其中一人](../Page/李聖淳.md "wikilink")，綽號「山貓」；另一名則是韓國相撲界名人[李丁載](../Page/李丁載.md "wikilink")，他的另一個身分是勢力範圍在東大門一帶的大頭目，他在當時的[李承晚總統的庇護下](../Page/李承晚.md "wikilink")，作為政治黑道人物暗中活動，他被政客利用來維持政權，同時也操控他以暴力手段壓制反對勢力（1960年4月18日的[高麗大學生襲擊事件等](../Page/高麗大學生襲擊事件.md "wikilink")）；李丁載也藉由這一股政治力，消滅了繼承金斗漢勢力的另名大頭目，這就是此時代典型的黑白共生現象。

1961年的[516軍事政變事件過後的](../Page/516軍事政變.md "wikilink")1970年代，組織暴力團彼此間的鬥爭激化，為「**全國區暴力時期**」。這時期的重要人物為[金泰村](../Page/金泰村.md "wikilink")，他在1977年的“[下剋上事件](../Page/下剋上事件.md "wikilink")”讓他一舉成名。另外、在[仁川](../Page/仁川.md "wikilink")[新松都賓館事件後](../Page/新松都賓館事件.md "wikilink")，韓國主要組織暴力團發展出3股大型勢力版圖，分別為金泰村領導的[西方派](../Page/西方派.md "wikilink")（서방파）、[李東載為首的](../Page/李東載.md "wikilink")[光州](../Page/光州.md "wikilink")[OB派](../Page/OB派.md "wikilink")（OB파）以及[楊恩邑派](../Page/楊恩邑.md "wikilink")（양은이파）。其他區域性知名組織則有[釜山的](../Page/釜山.md "wikilink")[七星派](../Page/七星派.md "wikilink")（칠성파；頭目：[李康桓](../Page/李康桓.md "wikilink")）、[大田的](../Page/大田.md "wikilink")[裕泰派](../Page/裕泰派.md "wikilink")
(옥태파)、[大邱的](../Page/大邱.md "wikilink")[東城路派](../Page/東城路派.md "wikilink")（동성로파）、[水原派](../Page/水原派.md "wikilink")（수원파）、[配車場派](../Page/配車場派.md "wikilink")（배차장파）等。此時期也是"刀械"正式用於幫派在街頭鬥爭中的工具,使得組織的鬥爭開始變得較以往殘忍。

1990年代後、現代的「**企業型暴力時期**」。現代的組織暴力團自身也慢慢產生許多的變化，從過去“簡單[勒索型](../Page/勒索.md "wikilink")”組織逐漸轉化為“權力介入型”組織，一些“親分大哥”搖身一變成為企業家。他們把從組織暴力團賺來的錢大量投入正當行業的[房地產](../Page/房地產.md "wikilink")、建築服務業或[教育](../Page/教育.md "wikilink")[文化等行業](../Page/文化.md "wikilink")。

## 組織

## 組織暴力團的現況

韓國警察廳提交的《2007年管理對象暴力組織現狀》報告。組暴分佈圖

[日文2007/5版](http://file.chosunonline.com//article/2007/05/17/335448315748150718.jpg)（「派」日文譯為「組」）

| 行政區                               | 組織數   | 成員數   | 組織名稱（正式成員數）                                                                                                  |
| --------------------------------- | ----- | ----- | ------------------------------------------------------------------------------------------------------------ |
| [首爾](../Page/首爾.md "wikilink")    | 105個派 | 1400名 | [南部洞派](../Page/南部洞派.md "wikilink")（62人）、中央洞派（38人）、連合新村派（60人）等                                                |
| [仁川](../Page/仁川.md "wikilink")    | 28個派  | 517名  | 富平新村派（48人）、主眼派（28人）等                                                                                         |
| [蔚山](../Page/蔚山.md "wikilink")    | 8個派   | 282名  | (新)新站前派（63人）、新木工派（33人）、新站前派（28人）等                                                                            |
| [釜山](../Page/釜山.md "wikilink")    | 101個派 | 1833名 | ・[七星派](../Page/七星派.md "wikilink")（58人）、20世紀派（26人）、零度派（33人）、猶太派（40人）、[臺灣三光派](../Page/三光幫.md "wikilink")（68人）等 |
| [京畿道](../Page/京畿道.md "wikilink")  | 59個派  | 1820名 | 清河蔚笙派（76人、日文譯）、[水原南門派](../Page/水原南門派.md "wikilink")（75人）、逆轉派（70人）、北門派（67人）、國際黑手黨派（60人）、原住民派（58人） 等           |
| [江原道](../Page/江原道.md "wikilink")  | 15個派  | 293名  | 連邦派（44人）、新鐘路企画派（36人）等                                                                                        |
| [忠清北](../Page/忠清北道.md "wikilink") | 10個派  | 636名  | 火星派（69人）、樂園溫泉派（37人）、 施羅蘇禰派（34人、頭目：李星順） 等                                                                     |
| [忠清南](../Page/忠清南道.md "wikilink") | 26個派  | 522名  | 新運動場派（56人）、松岳派（54人）、太陽會派（50人） 等                                                                              |
| [慶尚北](../Page/慶尚北道.md "wikilink") | 43個派  | 1064名 | 大明派 （59人）、合併派（52人）、三叉路派（39人） 等。[大邱地區](../Page/大邱.md "wikilink")・東城路派（68人）、鄉村洞派（67人）、東歐連合派（29人） 等             |
| [慶尚南](../Page/慶尚南道.md "wikilink") | 24個派  | 430名  | 皇帝派（38人）、迎春派（36人）、神通派（34人） 等                                                                                 |
| [全羅北](../Page/全羅北.md "wikilink")  | 16個派  | 953名  | 白鶴官派（54人）、世界盃派-運動場派（45人）、配車場派-夜間派（35人）等                                                                      |
| [全羅南](../Page/全羅南.md "wikilink")  | 33個派  | 1542名 | [國際PJ派](../Page/國際PJ派.md "wikilink")（58人）、[OB派](../Page/OB派.md "wikilink")（50人）、無等山派（53人）等                   |
| [濟州](../Page/濟州.md "wikilink")    | 3個派   | 184名  | 遺託派（53人）、山地派（46人）、地閥派（땅벌파、39人） 等                                                                             |

  - 在過去70年-90年代被列為韓國10大組織暴力團（10대 폭력조직）的組織

<!-- end list -->

  -   - 西方派（서방파）
      - 閃電派（번개파）
      - 楊恩邑派（양은이파）
      - OB派（OB파）
      - 零度派（영도파）
      - 七星派（칠성파）
      - 木浦派（목포파）
      - 群山派（군산파）
      - 配車場派（배차장파）
      - 全周派（전주파）

<!-- end list -->

  - 釜山組暴、釜山6大組織暴力團變遷史

<!-- end list -->

  -   - 1957年在[釜山創立的七星派](../Page/釜山.md "wikilink")（칠성파；頭目：李康桓）
      - 1988年由七星派分裂出零度派（영도파）及新七星派（신칠성파）；另一方面，也發展出"反七星派"的猶太派（유태파）以及在[中區](../Page/釜山.md "wikilink")[富平洞成立的](../Page/富平洞.md "wikilink")20世紀派（20세기파；頭目：鄭宗植）
      - 1999年20世紀派分裂出再建20世紀派（재건20세기파）
      - 2010年，臺灣新竹三光幫於釜山駐地且當地稱為臺灣三光派（대만세빛파），主要從事遊藝場居多，並招收韓國人來管理，且常在當地大動干戈

## 外部連結

  - [Man’s gambling ties go far
    back](http://archives.starbulletin.com/2004/02/08/news/story3.html)
  - [citypaper.net](https://web.archive.org/web/20060304014338/http://citypaper.net/articles/2002-08-22/cb2.shtml)
  - <http://kellogg.nd.edu/events/calendar/feb09/Lee.pdf>
  - <https://web.archive.org/web/20071028161413/http://news.ncmonline.com/news/view_article.html?article_id=910aafffcf7ec967ae92c5411382b9f8>
  - [Extortion Case Explores Rifts in Korean Enclave in
    Queens](http://select.nytimes.com/gst/abstract.html?res=FA0814FF3C5C0C768CDDAE0894D9404482)
  - [Organized Crime In California- Annual Report To The California
    Legislature 2004](http://www.ag.ca.gov/publications/org_crime2004.pdf)
  - [Organized Crime In California- Annual Report To The California
    Legislature 1996](http://www.fas.org/irp/world/para/docs/orgcrm96.pdf)
  - [Anxiety builds as crime increases in
    Koreatown](http://www.latimes.com/news/local/la-me-koreatown31oct31,1,4413066,full.story?ctrack=1&cset=true)
  - [Asian Organized Crime Groups – State of New Jersey Commission of
    Investigation 1989
    Report](http://www.mafianj.com/asian/koreans.shtml)
  - [Korea 'fist' genealogy and
    history](http://www.munhwa.com/news/view.html?no=20050207010110270370010)
  - [Five Indicted In a Robbery At a
    Church](http://www.nytimes.com/1992/12/31/nyregion/five-indicted-in-a-robbery-at-a-church.html)
  - [Korean Gangsters Held in
    Extortion](http://www.nytimes.com/1993/05/09/nyregion/korean-gangsters-held-in-extortion.html)
  - [5 Men Said to Be in Korean Mob Are Charged in Waiters'
    Assault](http://www.nytimes.com/2001/05/24/nyregion/5-men-said-to-be-in-korean-mob-are-charged-in-waiters-assault.html)
  - [MANHATTAN’S INVISIBLE KOREAN
    POWER](http://www.scribd.com/doc/7737612/New-York-Press-ADARIO-STRANGE-Manhattans-Invisible-Korean-Power)
  - [Korean Pride:Gangs And The Korean
    Community](http://www.stanford.edu/group/reflections/Winter1998/Nonfiction/KoreanPride.html)
  - [The Way of the
    Fists](http://www.time.com/time/magazine/article/0,9171,501020114-192602,00.html)
  - [Scam, Like A Nesting Doll, Hid Even
    More](http://www.washingtonpost.com/wp-dyn/content/article/2009/01/10/AR2009011001848.html)

[Category:犯罪組織](../Category/犯罪組織.md "wikilink")
[Category:韓國組暴](../Category/韓國組暴.md "wikilink")