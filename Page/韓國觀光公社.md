[HK_Causeway_Bay_World_Trade_Centre_22F_Korea_Tourism_Organization_a.jpg](https://zh.wikipedia.org/wiki/File:HK_Causeway_Bay_World_Trade_Centre_22F_Korea_Tourism_Organization_a.jpg "fig:HK_Causeway_Bay_World_Trade_Centre_22F_Korea_Tourism_Organization_a.jpg")支社\]\]
**韓國觀光公社**（），別称**-{zh-hant:韓國旅遊發展局;
zh-hans:韩国观光公社;}-**，是[大韓民國政府為了向其他國家推廣其觀光](../Page/韓國.md "wikilink")[旅遊業而設立的官方機構](../Page/韩国旅游业.md "wikilink")，其分社一般內設旅客諮詢中心，並免費提供韓國各地的觀光情報，例如[旅遊指南](../Page/旅遊指南.md "wikilink")、[地圖以及關於當地](../Page/地圖.md "wikilink")[購物與住宿的資料](../Page/購物.md "wikilink")。現任社長為[鄭昌洙](../Page/鄭昌洙.md "wikilink")。

## 組織

  - 理事會
  - 秘書室
  - 宣傳室
  - 監察室
  - 經營本部
  - 國際觀光本部
  - 國民觀光本部
  - 觀光產業本部

## 國內外分社

**總部：**[首爾特別市](../Page/首爾特別市.md "wikilink")[中區清溪川路](../Page/中區_\(首爾\).md "wikilink")40號2,6樓
**國內支社**

  - 江原支社：[江原道](../Page/江原道.md "wikilink")[春川市中央路](../Page/春川市.md "wikilink")16號武林大樓5樓
  - 世宗忠北支社：[忠清北道](../Page/忠清北道.md "wikilink")[清州市興德區佳景路第](../Page/清州市.md "wikilink")161街20號KT佳景大樓5樓
  - 大田忠南支社：[大田廣域市](../Page/大田廣域市.md "wikilink")[西區屯山路第](../Page/西區_\(大田\).md "wikilink")123街KD大樓7樓
  - 全北支社：[全羅北道](../Page/全羅北道.md "wikilink")[全州市完山區麻田들路](../Page/全州市.md "wikilink")66號KT\&G大樓3樓
  - 光州全南支社：[光州廣域市](../Page/光州廣域市.md "wikilink")[西區常務中央路](../Page/西區_\(光州\).md "wikilink")110號郵政局保險大樓光州會館7樓
  - 釜山蔚山支社：[釜山廣域市](../Page/釜山廣域市.md "wikilink")[釜山鎮區西面路](../Page/釜山鎮區.md "wikilink")74號永恆城市大樓15樓1502室
  - 大邱慶北支社：[大邱廣域市](../Page/大邱廣域市.md "wikilink")[中區東德路](../Page/中區_\(大邱\).md "wikilink")167號KT智慧大廈4樓
  - 慶南支社：[慶尚南道](../Page/慶尚南道.md "wikilink")[慶州市慶州大路第](../Page/慶州市.md "wikilink")1036街KDB大宇證券大樓4樓
  - 濟州支社：[濟州特別自治道](../Page/濟州特別自治道.md "wikilink")[西歸浦市中文觀光路第](../Page/西歸浦市.md "wikilink")110街15號

**國外支社**
\***東北亞：**

  -   - ：東京、大阪、福岡

      - ：北京、上海、廣州、瀋陽、成都、西安、武漢

      -
      - ：臺北

      - ：烏蘭巴托

  - **東南亞/南亞/中亞/西亞：**

      -
      - ：曼谷

      - ：吉隆坡

      - ：杜拜

      - ：新德里

      - ：河內

      - ：雅加達

      - ：馬尼拉

      - ：伊斯坦堡

      - ：阿拉木圖

  - **美洲/大洋洲**

      - ：洛杉磯、紐約

      - ：多倫多

      - ：雪梨

  - **歐洲**

      - ：法蘭克福

      - ：巴黎

      - ：倫敦

      - ：莫斯科、海參崴

## 参见

  - [韩国旅游业](../Page/韩国旅游业.md "wikilink")

## 外部連結

  - [韓國觀光公社](http://www.visitkorea.or.kr/intro.html)官方網站(旅行情報)

  - [韓國觀光公社](http://kto.visitkorea.or.kr/kor.kto)官方網站(公社情報)

  -
  -
  -
  - [韓國觀光公社](http://blog.naver.com/korea_diary)的[Naver官方部落格](../Page/Naver.md "wikilink")

  - [韓國觀光公社](http://post.naver.com/korea_diary)的[NaverPost官方帳戶](../Page/Naver.md "wikilink")

  - [韓國觀光公社](http://story.kakao.com/ch/visitkorea/feed)的[Kakao官方帳戶](../Page/Kakao.md "wikilink")

  - [韓國觀光公社](http://brunch.co.kr/@koreadiary)的[Brunch官方帳戶](../Page/Brunch.md "wikilink")

<File:HK> Causeway Bay World Trade Centre 22F Korea Tourism Organization
Info a.jpg|[香港世貿中心支社](../Page/香港世貿中心.md "wikilink") <File:HK> Causeway
Bay World Trade Centre 22F Korea Tourism Organization
booklets.JPG|[旅遊指南](../Page/旅遊指南.md "wikilink")、[地圖等](../Page/地圖.md "wikilink")
<File:HK> Causeway Bay World Trade Centre 22F Korea Tourism Organization
Classroom.jpg|韓國文化[講座的](../Page/講座.md "wikilink")[課室](../Page/課室.md "wikilink")

[Category:韓國旅遊](../Category/韓國旅遊.md "wikilink")
[Category:韩国政府机构](../Category/韩国政府机构.md "wikilink")
[Category:旅游部门](../Category/旅游部门.md "wikilink")