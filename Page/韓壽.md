**韓壽**（），字**德真**，[南陽](../Page/南陽.md "wikilink")[堵陽人](../Page/堵陽.md "wikilink")。[三國時](../Page/三國.md "wikilink")[魏司徒](../Page/曹魏.md "wikilink")[韓暨的曾孫](../Page/韓暨.md "wikilink")，[賈謐的父亲](../Page/賈謐.md "wikilink")，[西晉初權臣](../Page/西晉.md "wikilink")[賈充的女婿](../Page/賈充.md "wikilink")，[賈南風的妹婿](../Page/賈南風.md "wikilink")。

## 生平

三國時魏司徒[韓暨的曾孫](../Page/韓暨.md "wikilink")，[侍御史](../Page/侍御史.md "wikilink")[韩洪之子](../Page/韩洪.md "wikilink")。年少風流，風度翩翩，《[晉書](../Page/晉書.md "wikilink")》説他“美姿貌，善容止。”曾投权臣[贾充門下](../Page/贾充.md "wikilink")，任司空掾（司空秘書），為其小女[賈午所愛慕](../Page/賈午.md "wikilink")，在賈午的追求之下，韓壽與之私通。後來賈午以[晉武帝賜賈充的外國奇香偷贈韓壽](../Page/晉武帝.md "wikilink")（成語典故「韓壽偷香」，成为男女私会的代名词，其实偷香的是贾午），被賈充發現，得知兩人已私會一段時間，只好“以女妻壽”（《郭子》作韩寿与陈骞女事，但陈氏未嫁先死）。古代稱“[相如竊玉](../Page/司馬相如.md "wikilink")、韓壽偷香、[張敞畫眉](../Page/張敞.md "wikilink")、[沈約瘦腰](../Page/沈約.md "wikilink")”為風流四事。後蜀[歐陽炯](../Page/歐陽炯.md "wikilink")《春光好》詞：“雖似安仁擲果，未聞韓壽分香。”

韓壽後來官至散騎常侍、河南尹，於[晉惠帝元康初過世](../Page/晉惠帝.md "wikilink")，並獲追贈驃騎將軍。《晉書·后妃傳》記載[賈后被殺後](../Page/賈南風.md "wikilink")，韓壽、賈午等人皆伏誅，其實並不正確，由於當時確實有誅殺韓壽的其他兄弟，后妃傳可能是誤記。

## 子女

韓壽與賈午之間，至少育有二子：

  - 長子[賈謐](../Page/賈謐.md "wikilink")。因[賈充沒有男嗣](../Page/賈充.md "wikilink")，其妻[郭槐在他過世後作主讓外孫韓謐過繼給早夭長子](../Page/郭槐.md "wikilink")[賈黎民](../Page/賈黎民.md "wikilink")，成為賈充的繼承人。由日後賈謐與晉惠帝太子[司馬遹相繼與](../Page/司馬遹.md "wikilink")[王衍的兩個女兒結婚看來](../Page/王衍_\(西晋\).md "wikilink")，賈謐的年紀可能與司馬遹相仿。
  - 次子[韓慰祖](../Page/韓慰祖.md "wikilink")。由於[賈南風僅生四女](../Page/賈南風.md "wikilink")，沒有兒子，於是曾經假裝懷孕生子，卻又取來妹妹賈午之子慰祖，宣稱是晉武帝喪期內所生，所以沒有公開。以此推算，韓慰祖可能生於武帝過世前後，不久韓壽便也過世了。

此外，《晉書》中也有關於韓壽之女的記載。據《晉書·華廙傳》記載，韓壽生前任河南尹時，曾託賈后代為向[華廙求親](../Page/華廙.md "wikilink")，希望能將女兒嫁給華廙的孫子[華陶](../Page/華陶.md "wikilink")，結果遭到拒絕。《晉書·愍懷太子傳》又載，賈后母郭槐曾希望將韓壽之女嫁給太子司馬遹為妃，司馬遹也想娶韓氏以自保，卻遭賈午及賈后反對。這兩段記載中的韓壽之女究竟是同一人或不同二人，已不可考。

## 參考書目

  - 《晉書·后妃傳》
  - 《晉書·賈謐傳》
  - 《晉書·華廙傳》
  - 《晉書·愍懷太子傳》
  - 《晋書》卷40「列传第10・賈充」
  - [劉義慶](../Page/劉義慶.md "wikilink")：《[世說新語](../Page/世說新語.md "wikilink")·惑溺》

[category:晉朝政治人物](../Page/category:晉朝政治人物.md "wikilink")

[Shou壽](../Category/韓姓.md "wikilink")