**韓恢**（），字**复炎**，今[中國](../Page/中國.md "wikilink")[江蘇省](../Page/江蘇省.md "wikilink")[泗陽縣](../Page/泗陽縣.md "wikilink")[史集鄉](../Page/史集鄉.md "wikilink")[韓圩村人](../Page/韓圩村.md "wikilink")。[中華民國軍事人物](../Page/中華民國.md "wikilink")，曾擔任討袁軍江北總司令。

## 生平

他小時候讀過[私塾](../Page/私塾.md "wikilink")，22歲時（1909年）從軍，後來加入[同盟會](../Page/同盟會.md "wikilink")，並參加[黃花崗起義](../Page/黃花崗起義.md "wikilink")，失敗後回到[南京](../Page/南京.md "wikilink")。後來參加[辛亥革命](../Page/辛亥革命.md "wikilink")，協助革命軍攻下南京。[二次革命時](../Page/二次革命.md "wikilink")，他擔任南京都督，但最後因南京失守而撤退。二次革命失敗後，他滯留於[上海](../Page/上海.md "wikilink")，企圖重整軍隊來對抗[袁世凱](../Page/袁世凱.md "wikilink")，並任「討袁軍江北總司令」。1913年8月18日發動[南通起義戰役](../Page/南通起義.md "wikilink")，失敗後逃至[日本](../Page/日本.md "wikilink")。1914年加入[孫文在日本成立的](../Page/孫文.md "wikilink")[中華革命黨](../Page/中華革命黨.md "wikilink")。1915年底受命回到上海，企圖再度起兵，擔任「討袁軍第三軍軍長」，軍事計畫因遭[馮國璋發現而中止](../Page/馮國璋.md "wikilink")。1921年，孫文任非常大總統，12月任命韓恢為「江蘇招討使」。1922年的[六一六事變](../Page/六一六事變.md "wikilink")，他又趕往[廣東救援孫文](../Page/廣東.md "wikilink")，並任「討賊軍總司令」，成功將孫文救至上海。但是韓恢行蹤遭到江蘇督軍[齊燮元察覺](../Page/齊燮元.md "wikilink")，齊燮元於1922年10月28日命令上海淞滬警察廳廳長[徐國梁逮捕韓恢並移送至南京](../Page/徐國梁.md "wikilink")，11月1日在[小营遭齊燮元殺害](../Page/小营.md "wikilink")。

孫文後來追任韓恢為陸軍上將。1928年，韩恢遗骸由小营迁葬至南京[紫金山南麓](../Page/紫金山.md "wikilink")[卫岗](../Page/卫岗.md "wikilink")。[文化大革命期间](../Page/文化大革命.md "wikilink")，[韩恢墓被毁](../Page/韩恢墓.md "wikilink")。1987年，获得南京市栖霞区政府修复\[1\]。1992年，韩恢墓成为第二批[南京市文物保护单位](../Page/南京市文物保护单位.md "wikilink")。

## 注释

<references/>

[Category:中華民國大陸時期軍事人物](../Category/中華民國大陸時期軍事人物.md "wikilink")
[Category:中華民國被殺害人物](../Category/中華民國被殺害人物.md "wikilink")
[Category:泗陽人](../Category/泗陽人.md "wikilink")
[Hui恢](../Category/韓姓.md "wikilink")
[Category:葬于南京](../Category/葬于南京.md "wikilink")

1.