[玛格丽特·赵](../Page/玛格丽特·赵.md "wikilink")
[郑秀妍](../Page/郑秀妍.md "wikilink"){{·}}[姜成鎬](../Page/姜成鎬.md "wikilink"){{·}}[高洪柱](../Page/高洪柱.md "wikilink")
[朴宰范](../Page/朴宰范.md "wikilink"){{·}}[张永宙](../Page/张永宙.md "wikilink"){{·}}[杰米·钟](../Page/杰米·钟.md "wikilink")
|poptime =
249万2252人(2018)(韓國外交部統計)，170萬人(2010)<small>（占美国总人口的約1%）</small>\[1\]
|popplace =
[洛杉矶市](../Page/洛杉矶市.md "wikilink")、[纽约市](../Page/纽约市.md "wikilink")、[华盛顿哥伦比亚特区等](../Page/华盛顿哥伦比亚特区.md "wikilink")
|langs = [英语](../Page/英语.md "wikilink")、[韓语](../Page/韓语.md "wikilink")
|rels =
[基督新教](../Page/基督新教.md "wikilink")61%、[天主教](../Page/天主教.md "wikilink")10%、[佛教](../Page/佛教.md "wikilink")6%及[無宗教](../Page/無宗教.md "wikilink")23%
}}

**-{zh-hans:韩;zh-hant:韓}-裔美国人**（；），或称为**-{zh-hans:朝鲜;zh-hant:朝鮮}-裔美國人**（）指擁有[韓民族血統的](../Page/韓民族.md "wikilink")[美國](../Page/美國.md "wikilink")[国民](../Page/国民.md "wikilink")。因為從朝鮮和中國大陸來的朝鮮裔特別少，所以沒有統計。根據韓國外交部2017年官方統計，2492,252名韓人在美國(包括[美籍韓裔](../Page/美籍韓裔.md "wikilink"))。\[2\]

## 数目与分布

历史上朝鲜裔美国人的数目如下：\[3\]\[4\]：

| 年份    | 人数      |
| ----- | ------- |
| 1910年 | 462     |
| 1920年 | 1224    |
| 1930年 | 1860    |
| 1940年 | 1711    |
| 1970年 | 69130   |
| 1980年 | 354593  |
| 1990年 | 798849  |
| 2000年 | 1076872 |
| 2010年 | 1423784 |

根据2012年[美国人口调查局的调查](../Page/美国人口调查局.md "wikilink")，朝鲜裔美国人有170万6822人\[5\]。

根据[2010年美国人口普查结果](../Page/2010年美国人口普查.md "wikilink")\[6\]，朝鲜裔美国人分布最广的地区依次为[加利福利亚州](../Page/加利福利亚州.md "wikilink")（45.2万，占该州人口的1.2％）、[夏威夷州](../Page/夏威夷州.md "wikilink")（23.2万，占全州人口的1.8％）、[纽约州](../Page/纽约州.md "wikilink")（14.1万，占该州人口的0.7％）、[新泽西州](../Page/新泽西州.md "wikilink")（9.4万，占该州人口的1.1％）、[-{zh-cn:弗吉尼亚州;zh-hk:維珍尼亞州;zh-tw:維吉尼亞州}-](../Page/弗吉尼亚州.md "wikilink")（7.1万，占该州人口的0.9％）、[德克萨斯州](../Page/德克萨斯州.md "wikilink")（6.8万，占该州人口的0.3％）、[华盛顿州](../Page/华盛顿州.md "wikilink")（6.24万，占该州人口的0.9％）、[伊利诺伊州](../Page/伊利诺伊州.md "wikilink")（6.15万，占该州人口的0.5％）、[乔治亚州](../Page/乔治亚州.md "wikilink")（5.25万，占该州人口的0.5％）、[马里兰州](../Page/马里兰州.md "wikilink")（4.9万，占该州人口的0.8％）、[宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink")（4.1万，占该州人口的0.3％）。

## 历史

[Korean_Americans.jpg](https://zh.wikipedia.org/wiki/File:Korean_Americans.jpg "fig:Korean_Americans.jpg")

[徐载弼被认为是第一个朝鲜裔美国人](../Page/徐载弼.md "wikilink")，1884年的[甲申政变失败之后他于](../Page/甲申政变.md "wikilink")1885年5月26日抵达美国并在1890年加入了美国国籍，朝鲜半岛向美国的早期移民史上另外两个重要人物是[韓國獨立運動人士](../Page/韓國獨立運動.md "wikilink")[安昌浩与](../Page/安昌浩.md "wikilink")[李承晚](../Page/李承晚.md "wikilink")。

1903年1月13日，一批[大韓帝國勞工抵达](../Page/大韓帝國.md "wikilink")[美国](../Page/美国.md "wikilink")[夏威夷](../Page/夏威夷.md "wikilink")[甘蔗園](../Page/甘蔗.md "wikilink")。

1968年1月30日[美国的](../Page/美国.md "wikilink")《[移民与国籍法](../Page/1965年移民与国籍法案.md "wikilink")》生效之後，有更多的[韓民族或者朝鮮民族](../Page/韓民族.md "wikilink")（主要是[-{zh-hans:韩国;zh-hant:南韓}-人](../Page/大韓民國.md "wikilink")）移民到[美國](../Page/美國.md "wikilink")。

## 著名人物

  - [魏聖美](../Page/魏聖美.md "wikilink")：美國職業[高爾夫球球員](../Page/高爾夫球.md "wikilink")
  - [張永宙](../Page/莎拉·張.md "wikilink")：小提琴演奏家
  - [約翰·趙](../Page/約翰·趙.md "wikilink")：[美國](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [丹尼斯·吳](../Page/丹尼斯·吳.md "wikilink")：演員、模特兒
  - [喬瑟夫·漢恩](../Page/喬瑟夫·漢恩.md "wikilink")([Joseph
    Hahn](../Page/Joseph_Hahn.md "wikilink"))：，[聯合公園DJ](../Page/聯合公園.md "wikilink")
  - [朴宰範](../Page/朴宰範.md "wikilink") ([Jay
    Park](../Page/Jay_Park.md "wikilink"))：歌手，前[2PM團員](../Page/2PM.md "wikilink")
  - [黃美英](../Page/黃美英.md "wikilink") (Tiffany
    Hwang)：歌手，[少女時代團員](../Page/少女時代.md "wikilink")
  - [鄭秀妍](../Page/鄭秀妍.md "wikilink") (Jessica
    Jung)：歌手，前少女时代团员,品牌創辦人及創意總監
  - [鄭秀晶](../Page/鄭秀晶.md "wikilink") (Chrystal
    Jung)：歌手，[f(x)團員](../Page/f\(x\).md "wikilink")
  - [鄭龍珠](../Page/鄭龍珠.md "wikilink") (Nicole
    Jung)：歌手，前[Kara團員](../Page/Kara.md "wikilink")
  - [Ailee](../Page/李藝真.md "wikilink")：歌手
  - [禹成賢](../Page/禹成賢.md "wikilink")：歌手，[U-KISS團員](../Page/U-KISS.md "wikilink")
  - [金炅才](../Page/金炅才.md "wikilink")：歌手，[U-KISS團員](../Page/U-KISS.md "wikilink")
  - [梁振錫](../Page/梁振錫.md "wikilink")：[世界跆拳道聯盟秘書長](../Page/世界跆拳道聯盟.md "wikilink")\[7\]。
  - [金永玉](../Page/金永玉.md "wikilink")：[美國陸軍退役](../Page/美國陸軍.md "wikilink")[上校](../Page/上校.md "wikilink")，在[二戰及](../Page/二戰.md "wikilink")[韓戰中擔任指揮軍官](../Page/韓戰.md "wikilink")，獲得19項戰功勳章。
  - [蘇珊·安·古迪](../Page/蘇珊·安·古迪.md "wikilink")：[安昌浩的長女](../Page/安昌浩.md "wikilink")，[美國海軍退役](../Page/美國海軍.md "wikilink")[上尉](../Page/海軍上尉.md "wikilink")（1942年－1946年）、美軍最早的亞裔女性[軍官之一](../Page/軍官.md "wikilink")
  - [金允珍](../Page/金允珍.md "wikilink")：演員
  - [Eric Nam](../Page/Eric_Nam.md "wikilink")(에릭남, 本名:南允道(남윤도))：歌手
  - [Jae](../Page/Jae.md "wikilink")（[Jae
    Park](../Page/Jae.md "wikilink")）：歌手，樂團[DAY6的](../Page/DAY6.md "wikilink")[吉他手](../Page/吉他手.md "wikilink")。
  - [Joshua](../Page/Joshua.md "wikilink")([Jisoo Joshua
    Hong](../Page/Jisoo_Joshua_Hong.md "wikilink"))：歌手，[SEVENTEEN團員](../Page/SEVENTEEN.md "wikilink")
  - [Aron](../Page/Aron.md "wikilink") ([Aaron
    Kwak](../Page/Aaron_Kwak.md "wikilink")):歌手，本名:郭英敏，[NU'EST成員](../Page/NU'EST.md "wikilink")。

## 注释

## 参考文献

## 外部链接

  - [揭秘朝鲜裔美国人南宫:曾撮合金日成卡特会谈](https://web.archive.org/web/20150704153245/http://www.bluehn.com/2013/0205/205218.html)

## 参见

  - [美籍韓裔](../Page/美籍韓裔.md "wikilink")
  - [亚裔美国人](../Page/亚裔美国人.md "wikilink")
  - [美韩关系](../Page/美韩关系.md "wikilink")、[美朝关系](../Page/美朝关系.md "wikilink")
  - [韓民族](../Page/韓民族.md "wikilink")
      - [朝侨/韩侨](../Page/韩侨.md "wikilink")

{{-}}

[韓裔美國人](../Category/韓裔美國人.md "wikilink")
[Korean](../Category/亞洲裔美國人.md "wikilink")

1.

2.  （發布，2018年01月25日）

3.

4.

5.
6.
7.  [WTF World Taekwondo Federation
    Council](http://www.wtf.org/wtf_eng/site/about_wtf/executive.html#3)