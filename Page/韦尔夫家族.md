**韦尔夫家族**或**韦尔夫王朝**（英文名：Welfen），[德国的传统贵族世家](../Page/德国.md "wikilink")，在[意大利被称为](../Page/意大利.md "wikilink")**[圭尔夫家族](../Page/歸爾甫派和吉伯林派.md "wikilink")**（guelfo）。在历史上的不同时期，该家族的成员曾先后是[士瓦本](../Page/士瓦本.md "wikilink")、[勃艮第](../Page/勃艮第.md "wikilink")、意大利、[巴伐利亚](../Page/巴伐利亚.md "wikilink")（**拜恩**）、[萨克森](../Page/萨克森.md "wikilink")、[布倫瑞克-吕讷堡公国](../Page/布倫瑞克-吕讷堡公国.md "wikilink")（**汉诺威**）的统治王朝；家族成员[布倫瑞克的奥托曾为](../Page/奥托四世.md "wikilink")[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")（1209年加冕）。来自此家族的[乔治一世在](../Page/乔治一世_\(大不列颠\).md "wikilink")1714年[加冕成为](../Page/加冕.md "wikilink")[英国国王](../Page/英国国王.md "wikilink")，开创[汉诺威王朝在英国的统治](../Page/汉诺威王朝.md "wikilink")，今[英国王室仍是乔治一世的后裔](../Page/英国王室.md "wikilink")。

## 家族历史

韦尔夫家族起源于[德意志](../Page/德意志.md "wikilink")[士瓦本的阿尔特多夫伯爵韦尔夫](../Page/士瓦本.md "wikilink")（824/5年卒），他的两个女儿先后成为[法兰克帝国的皇后](../Page/法兰克帝国.md "wikilink")，他在[士瓦本和](../Page/士瓦本.md "wikilink")[勃艮第拥有辽阔的领地](../Page/勃艮第.md "wikilink")。后家族分为[士瓦本和](../Page/士瓦本.md "wikilink")[勃艮第两支](../Page/勃艮第.md "wikilink")，[士瓦本支是](../Page/士瓦本.md "wikilink")[士瓦本贵族](../Page/士瓦本.md "wikilink")[韦尔夫一世的后代](../Page/韦尔夫一世.md "wikilink")，[韦尔夫一世是阿尔特多夫伯爵韦尔夫的孙子](../Page/韦尔夫一世.md "wikilink")。他的后代[克恩滕公爵](../Page/克恩滕.md "wikilink")[韦尔夫三世之妹康尼根达与意大利豪门](../Page/韦尔夫三世.md "wikilink")**[埃斯特家族](../Page/埃斯特家族.md "wikilink")**的始祖[阿尔贝托·阿佐二世结婚](../Page/阿尔贝托·阿佐二世.md "wikilink")。由于韦尔夫家族老系，於1055年绝嗣，[阿佐二世与康尼根达的后代成为韦尔夫家族新系](../Page/阿佐二世.md "wikilink")，[阿佐二世再婚的后代即](../Page/阿佐二世.md "wikilink")[埃斯特家族的成员](../Page/埃斯特家族.md "wikilink")。

[勃艮第支源于韦尔夫的曾孙](../Page/勃艮第.md "wikilink")[鲁道夫一世](../Page/鲁道夫一世_\(勃艮第\).md "wikilink")（912年卒）888年在[上勃艮第称孤道寡](../Page/上勃艮第.md "wikilink")；其子[鲁道夫二世](../Page/鲁道夫二世_\(勃艮第\).md "wikilink")（937年卒）在923年—926年间短暂统治过[意大利](../Page/意大利.md "wikilink")。随着[鲁道夫三世在](../Page/鲁道夫三世_\(勃艮第\).md "wikilink")1032年去世，[勃艮第支绝嗣](../Page/勃艮第.md "wikilink")。

[阿佐二世的长子](../Page/阿佐二世.md "wikilink")[韦尔夫四世于](../Page/韦尔夫四世.md "wikilink")1070年被神圣罗马帝国皇帝[亨利四世封为巴伐利亚公爵](../Page/亨利四世_\(神圣罗马帝国\).md "wikilink")；其子[黑亨利](../Page/黑亨利.md "wikilink")（1126年卒）将萨克森的部分土地——其中包括日后家族最重要的领地[吕讷堡](../Page/吕讷堡.md "wikilink")——并入巴伐利亚公国。黑亨利之子[骄傲的亨利](../Page/骄傲的亨利.md "wikilink")（1139年卒）娶神圣罗马帝国皇帝[洛泰尔二世之女为妻](../Page/洛泰尔二世_\(神圣罗马帝国\).md "wikilink")，并在洛泰尔二世死后继承了萨克森公国。骄傲的亨利以前任皇帝之女婿的身份竞选神圣罗马帝国皇位，但是对他的强大疑虑重重的诸侯们选择了[霍亨斯陶芬家族的](../Page/霍亨斯陶芬家族.md "wikilink")[康拉德三世为皇帝](../Page/康拉德三世_\(德意志\).md "wikilink")（1138年）。此后两家结下深仇。骄傲的亨利之子[狮子亨利在与霍亨斯陶芬家族的皇帝](../Page/狮子亨利.md "wikilink")[腓特烈一世的斗争中失去了巴伐利亚和萨克森](../Page/腓特烈一世_\(神圣罗马帝国\).md "wikilink")。狮子亨利之子[奥托四世是唯一获得神圣罗马帝国皇位的韦尔夫家族成员](../Page/奥托四世.md "wikilink")。他的侄子“孩子”奥托（1152年卒）于1235年得到[不伦斯克-吕讷堡公国](../Page/不伦斯克-吕讷堡.md "wikilink")（又称吕讷堡-卡伦堡公国；更有名的叫法是汉诺威公国）作为世袭封地，从此开始了不伦瑞克王朝的统治（至1918年）。

## 外部链接

## 参见

  - [韦尔夫家族成员列表](../Page/韦尔夫家族成员列表.md "wikilink")
  - [埃斯特家族](../Page/埃斯特家族.md "wikilink")
  - [勃艮第统治者列表](../Page/勃艮第统治者列表.md "wikilink")
  - [巴伐利亚统治者列表](../Page/巴伐利亚统治者列表.md "wikilink")
  - [汉诺威统治者列表](../Page/汉诺威统治者列表.md "wikilink")
  - [汉诺威王朝](../Page/汉诺威王朝.md "wikilink")

{{-}}

[韦尔夫王朝](../Category/韦尔夫王朝.md "wikilink")
[Category:欧洲家族](../Category/欧洲家族.md "wikilink")