**韦恩·道格拉斯·格雷茨基**（****，），[加拿大著名](../Page/加拿大.md "wikilink")[冰球运动员](../Page/冰球.md "wikilink")，现任[NHL](../Page/NHL.md "wikilink")[菲尼克斯野狼队主教练](../Page/菲尼克斯野狼队.md "wikilink")。

格雷茨基被许多人认为是冰球史上最伟大的运动员，他曾经率领[愛民頓油人隊](../Page/愛民頓油人隊.md "wikilink")4次夺得[斯坦利杯](../Page/斯坦利杯.md "wikilink")，9次当选[MVP](../Page/MVP.md "wikilink")，10次获得[得分王头衔](../Page/得分王.md "wikilink")，他保持着40项常规赛纪录、15项季后赛纪录和6项[全明星赛纪录](../Page/NHL全明星赛.md "wikilink")。格雷茨基是唯一一位在一个赛季常规赛中得分超过200的运动员（他曾4次做到这一点），得分100以上的赛季有15个，其中有13个是连续的。他的号码99号，已经被NHL正式宣布从联盟中退休。

## 外部連結

  - [NHL.com Wayne Gretzky
    section](https://web.archive.org/web/20090823072038/http://www.nhl.com/history/gretzky.html)

  -
  -
  -
  -
  -
  -
  - [/ Wayne Gretzky, winner of the Lionel Conacher Award and the Bobbie
    Rosenfeld Award: Virtual Museum of Canada
    Exhibit](http://www.conacher-rosenfeld.ca/les_gagnants-winners/conacher/wayne_gretzky-eng.html)

[Category:加拿大冰球教練](../Category/加拿大冰球教練.md "wikilink")
[Category:英格蘭裔加拿大人](../Category/英格蘭裔加拿大人.md "wikilink")
[Category:波蘭裔加拿大人](../Category/波蘭裔加拿大人.md "wikilink")
[Category:烏克蘭裔加拿大人](../Category/烏克蘭裔加拿大人.md "wikilink")
[Category:冰球名人堂成員](../Category/冰球名人堂成員.md "wikilink")
[Category:加拿大奧運冰球運動員](../Category/加拿大奧運冰球運動員.md "wikilink")
[Category:洛杉磯國王隊球員](../Category/洛杉磯國王隊球員.md "wikilink")
[Category:愛德蒙頓油人隊球員](../Category/愛德蒙頓油人隊球員.md "wikilink")
[Category:紐約遊騎兵隊球員](../Category/紐約遊騎兵隊球員.md "wikilink")
[Category:聖路易斯藍調隊球員](../Category/聖路易斯藍調隊球員.md "wikilink")
[Category:奥林匹克勋章获得者](../Category/奥林匹克勋章获得者.md "wikilink")