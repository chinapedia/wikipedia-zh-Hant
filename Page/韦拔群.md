**韦拔群**，[中国](../Page/中国.md "wikilink")[广西](../Page/广西.md "wikilink")[东兰人](../Page/东兰.md "wikilink")，[壮族](../Page/壮族.md "wikilink")，[中国工农红军高级将领](../Page/中国工农红军.md "wikilink")。

## 生平

韦早年就读于广西政法学堂，1916年参加[护国战争](../Page/护国战争.md "wikilink")，同年入[讲武堂学习](../Page/讲武堂.md "wikilink")，毕业后在[贵州军队任职](../Page/贵州.md "wikilink")。1919年，韦拔群受到[五四运动影响](../Page/五四运动.md "wikilink")，离开军队。1921年，韦回到家乡领导农民运动，曾三次攻打东兰县城。1924年到[广州农民运动讲习所学习](../Page/广州农民运动讲习所.md "wikilink")，次年在东兰开办农民运动讲习所。1928年加入[中国共产党](../Page/中国共产党.md "wikilink")，并担任田南道农运办事处主任，组织农民协会和武装，此后一直在广西坚持[游击战](../Page/游击战.md "wikilink")。

1929年，韦率部迎接了[南宁兵变后撤出](../Page/南宁兵变.md "wikilink")[南宁的中共军队](../Page/南宁.md "wikilink")，并攻占了东兰县。12月11日，韦率部参加[百色起义](../Page/百色起义.md "wikilink")，任[右江工农民主政府委员](../Page/右江.md "wikilink")、[红七军第](../Page/红七军.md "wikilink")3纵队司令。

1930年10月，红七军奉命北上[中央苏区](../Page/中央苏区.md "wikilink")，韦出任红二十一师师长，率部留在广西坚持游击战。在此后一年多时间里，韦部抗击了[白崇禧等人的多次进攻](../Page/白崇禧.md "wikilink")。1932年4月，韦部被围困在西山地区。10月18日夜，被其警卫刺杀。

[Category:中国工农红军将领](../Category/中国工农红军将领.md "wikilink")
[Category:中华民国大陆时期遇刺身亡者](../Category/中华民国大陆时期遇刺身亡者.md "wikilink")
[Category:中国共产党党员
(1928年入党)](../Category/中国共产党党员_\(1928年入党\).md "wikilink")
[Category:东兰人](../Category/东兰人.md "wikilink")
[Category:壮族人](../Category/壮族人.md "wikilink")
[B](../Category/韋姓.md "wikilink")
[Category:中华苏维埃共和国中央执行委员会委员](../Category/中华苏维埃共和国中央执行委员会委员.md "wikilink")