\[1\]

**韦晴光**（），[广西](../Page/广西.md "wikilink")[南宁人](../Page/南宁.md "wikilink")，原[中国](../Page/中国.md "wikilink")[乒乓球运动员](../Page/乒乓球.md "wikilink")，后来曾代表[日本出战](../Page/日本.md "wikilink")。

韦晴光于1972年开始接受专业训练，并进入广西队，1984年代表广西获得全国团体、男单、混双三项冠军，次年又获得全国男双冠军，进入国家队。

1987年和1988年，韦晴光与[陈龙灿合作](../Page/陈龙灿.md "wikilink")，先后获得第39届[世乒赛和](../Page/世乒赛.md "wikilink")[汉城奥运会男双冠军](../Page/1988年夏季奥林匹克运动会.md "wikilink")。\[2\]1990年[北京亚运会后退役](../Page/1990年亚洲运动会.md "wikilink")。

1991年，韦前往日本打球，1997年加入日本国籍，转而代表日本打球，并改名**伟关晴光**，曾出战[悉尼奥运会](../Page/2000年夏季奥林匹克运动会.md "wikilink")。后曾兼任日本乒乓球队教练。退役之后，韦晴光放弃继续担任日本乒乓球国家队教练，在[日本青森山田学院担任教练](../Page/日本青森山田学院.md "wikilink")。\[3\]

## 日本姓氏由来

“**伟关**”这一姓氏是韦晴光将他本人和妻子的姓氏放在一起，再经日语化后而成的，日本通用的汉字中没有“韦”字，但有“伟”字，于是改“韦”为“伟”。韦晴光的妻子姓“石”叫[石小娟](../Page/石小娟.md "wikilink")，而“石”的日文[音讀发音与](../Page/音讀.md "wikilink")“关([日文漢字寫作](../Page/日文漢字.md "wikilink")『関』)”的日语訓讀发音完全相同，皆唸為せき(SEKI)。“伟关”这一姓氏由韦晴光原创，在日本绝无仅有。

## 参考资料

[Category:前中國乒乓球運動員](../Category/前中國乒乓球運動員.md "wikilink")
[Category:中国奥运乒乓球运动员](../Category/中国奥运乒乓球运动员.md "wikilink")
[Eiseki Seikou](../Category/日本奧運桌球運動員.md "wikilink") [Eiseki
Seikou](../Category/日本退役乒乓球運動員.md "wikilink")
[Category:南宁籍运动员](../Category/南宁籍运动员.md "wikilink")
[Category:归化日本公民的中华人民共和国人](../Category/归化日本公民的中华人民共和国人.md "wikilink")
[Jingguang](../Category/韦姓.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:1988年夏季奧林匹克運動會獎牌得主](../Category/1988年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1988年夏季奥林匹克运动会乒乓球运动员](../Category/1988年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:奥林匹克运动会乒乓球奖牌得主](../Category/奥林匹克运动会乒乓球奖牌得主.md "wikilink")
[Category:亞洲運動會桌球獎牌得主](../Category/亞洲運動會桌球獎牌得主.md "wikilink")
[Category:1990年亞洲運動會金牌得主](../Category/1990年亞洲運動會金牌得主.md "wikilink")
[Category:1990年亞洲運動會銀牌得主](../Category/1990年亞洲運動會銀牌得主.md "wikilink")
[Category:1998年亞洲運動會銅牌得主](../Category/1998年亞洲運動會銅牌得主.md "wikilink")
[Category:2000年夏季奥林匹克运动会乒乓球运动员](../Category/2000年夏季奥林匹克运动会乒乓球运动员.md "wikilink")

1.  [【奥运经典】1988年男子双打决赛录像](http://www.56.com/u68/v_NTkwMzEwMjc.html)
2.  [我的奥林匹克—陈龙灿、韦晴光](http://space.tv.cctv.com/video/VIDE1208274595850719)
3.  [韦晴光在日本](http://news.hexun.com/2009-05-04/117308237.html)