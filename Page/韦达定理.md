**韦达定理**給出[多項式方程的](../Page/多項式方程.md "wikilink")[根與](../Page/根_\(数学\).md "wikilink")[係數的关系](../Page/係數.md "wikilink")，又稱為**根與係數**。[定理陳述如下](../Page/定理.md "wikilink")：

设\(F(x)=\sum_{m=1}^{n}a_mx^m + a_0 = 0\)是一个\(n\ge 1\)次[方程](../Page/方程.md "wikilink")，则有：

\[\begin{cases}
\sum_{k=1}^{n}x_k = -\frac{a_{n-1}}{a_n} \\
\sum_{m=1}^{n-1}x_m\sum_{k=m+1}^{n}x_k = \frac{a_{n-2}}{a_n} \\
\qquad \qquad \vdots \\
\Pi_{k=1}^{n}x_k = (-1)^n \frac{a_0}{a_n}
\end{cases}\]

在[一元二次方程的特例中](../Page/一元二次方程.md "wikilink")，两個根的和等于方程的一次项系数除以二次项系数的[相反数](../Page/相反数.md "wikilink")；两個根的乘积等于方程的[常数项除以二次项系数](../Page/常数.md "wikilink")。

设\(x_1, x_2\)是一元二次方程\(ax^2+bx+c=0\)的两根，那么：

\[x_1+x_2=-\frac{b}{a}, x_1x_2=\frac{c}{a}\]

韦达定理的[逆定理同样成立](../Page/定理#逆定理.md "wikilink")。仍然以一元二次方程為例：給定一個一元二次方程。如果有两个数，它們的和等于該方程的一次项系数除以二次项系数的相反数，它們的积又等于該方程的常数项除以二次项系数，那么它們就是該方程的兩根。

设关于\(x\)的一元二次方程为\(ax^2+bx+c=0\)，且：

\[\alpha + \beta =-\frac{b}{a}, \alpha \beta =\frac{c}{a}\]

则\(\alpha, \beta\)必定是一元二次方程\(ax^2+bx+c=0\)的两个根。

## 定理特例的证明

设\(x_1, x_2\)是一元二次方程\(ax^2+bx+c=0\quad(a \ne 0)\)的两个解，且不妨令\(x_1 \ge x_2\)。

证明一：根据求根公式，有：

\[x_1=\frac{-b + \sqrt {b^2-4ac}}{2a}, x_2=\frac{-b - \sqrt {b^2-4ac}}{2a}\]

所以：

\[x_1+x_2=\frac{-b + \sqrt {b^2-4ac} + \left (-b \right) - \sqrt {b^2-4ac}}{2a} =-\frac{b}{a}\]

\[x_1x_2=\frac{ \left (-b + \sqrt {b^2-4ac} \right) \left (-b - \sqrt {b^2-4ac} \right)}{\left (2a \right)^2} =\frac{c}{a}\]

证明二：方程两边同时除以\(a\)，有：

\[x^2 + \frac{b}{a}x + \frac{c}{a} = 0 \dots \text{(1)}\]

\[(x-x_1)(x-x_2) = 0\]

展开得：

\[x^2-(x_1+x_2)x+x_1x_2 = 0 \dots \text{(2)}\]

对照式子(1)与(2)，得：

\[x_1+x_2=-\frac{b}{a}, x_1x_2=\frac{c}{a}\]

## 定理的证明

设\(x_1, x_2, \dots, x_n\)是一元n次方程\(\sum_{m=1}^{n}a_mx^m + a_0 = 0,(n\ge 1, a_n\ne0)\)的n个解。

則有\(\sum_{m=1}^{n}a_mx^m + a_0 = a_n \Pi_{m=1}^{n}(x-x_m)\)

由[乘法原理](../Page/乘法原理.md "wikilink")：

\[\begin{cases}
a_{n-1}=a_n\sum_{k=1}^{n}(-x_k) \\
a_{n-2}=a_n\sum_{m=1}^{n-1}(-x_m)\sum_{i>m}(-x_i) \\
a_{n-3}=a_n\sum_{m=1}^{n-2}(-x_m)\sum_{i>j>m}(x_ix_j) \\
\qquad \qquad \vdots \\
a_0=a_n\Pi_{k=1}^{n}(-x_k)
\end{cases}\]

移項化簡后得：

\[\begin{cases}
\sum_{k=1}^{n}x_k = (-1)^1 \frac{a_{n-1}}{a_n} \\
\sum_{m=1}^{n-1}x_m\sum_{i>m}x_i = (-1)^2 \frac{a_{n-2}}{a_n} \\
\sum_{m=1}^{n-2}x_m\sum_{i>j>m}x_ix_j = (-1)^3 \frac{a_{n-3}}{a_n} \\
\qquad \qquad \vdots \\
\Pi_{k=1}^{n}x_k = (-1)^n \frac{a_0}{a_n}
\end{cases}\]

## 参见

  - [法兰西斯·韦达](../Page/法兰西斯·韦达.md "wikilink")
  - [对称多项式](../Page/对称多项式.md "wikilink")
  - [韋達跳躍](../Page/韋達跳躍.md "wikilink")

[W](../Category/数学定理.md "wikilink") [W](../Category/多项式.md "wikilink")
[W](../Category/初等代数.md "wikilink")