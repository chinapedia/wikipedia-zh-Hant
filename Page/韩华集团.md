**韓華集團**（，[朝鮮汉字](../Page/朝鮮汉字.md "wikilink")：），原名**韓国火药**，是[韩国的大型财团之一](../Page/韩国.md "wikilink")，[财富世界500强企业](../Page/财富世界500强.md "wikilink")\[1\]，总部位于[韩国](../Page/韩国.md "wikilink")[首尔](../Page/首尔.md "wikilink")。韩华集团业务领域涉及[化工](../Page/化工.md "wikilink")、[能源](../Page/能源.md "wikilink")、[建筑](../Page/建筑.md "wikilink")、[金融](../Page/金融.md "wikilink")[保险和](../Page/保险.md "wikilink")[零售等领域](../Page/零售.md "wikilink")，旗下拥有[韩华化学](../Page/韩华化学.md "wikilink")、[韩华建设](../Page/韩华建设.md "wikilink")、[韩华生命保险等](../Page/韩华生命保险.md "wikilink")56家关联公司和69个海外网点\[2\]，并拥有[韩国职业棒球](../Page/韓國職棒聯賽.md "wikilink")[韩华鹰球隊](../Page/韩华鹰.md "wikilink")。

## 历史

韩华集团的前身为成立于1952年的“韩国火药株式会社”（），最初以生产[火药為主业](../Page/火药.md "wikilink")。1964年，韩国火药收购新韩轴承工业，开始了集團多元化的进程。1992年10月，韩国火药更名为原简称“韩火”。官方中文名称则未采用原文而称“韩华”。韩华通过不断的扩张收购，已经发展成为多元化的集体公司。

2007年以来，韩华进一步立足全球，加快了集团的国际化。2007年11月，韩华收购[美国Azdel股份](../Page/美国.md "wikilink")。在随后的几年中，韩华集团先后通过收购的方式在[亚洲](../Page/亚洲.md "wikilink")、[欧洲和](../Page/欧洲.md "wikilink")[北美开設分公司](../Page/北美.md "wikilink")。2011年6月，成立韩华中国总部。

2014年11月26日韓華集團以1.9兆韓圜（17億美元）收購三星旗下的化學和國防公司，包括Samsung
Techwin約32.4%股權、三星綜合化學（Samsung General Chemicals）57.6%
、南韓國防航太設備製造商Samsung Thales、三星道達爾股份有限公司（Samsung Total
Petrochemicals）。

## 参考资料

## 外部链接

  - [韩华集团中文网页](http://www.hanwha.co.kr/china/company/info/overview.jsp)

[韩华集团](../Category/韩华集团.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:韩国建筑公司](../Category/韩国建筑公司.md "wikilink")

1.  [Global 500](http://money.cnn.com/magazines/fortune/global500/2011/snapshots/11164.html),
    CNN Money
2.  [韩华简介](http://www.hanwha.co.kr/kor/images/download/brochure2011_cha.pdf)，韩华集团