**韩鹏**（）岀生于[山东省](../Page/山东省.md "wikilink")[济南市](../Page/济南市.md "wikilink")，是一名退役[中国足球运动员](../Page/中国.md "wikilink")，司职[前鋒](../Page/前锋_\(足球\).md "wikilink")。

## 成长经历

韩鹏少年时的踢球生涯并不是非常规范，他先是在[历城区祝甸小学念完了小学](../Page/历城区.md "wikilink")，1995年考上历城五中，他一开始是一名后卫。1997年韩鹏想进入正规的学校训练，他先是考上了北京龙利足球学校，但3万元的学费让那时并不富裕的家庭负担不起，最终韩鹏进入了济南新世纪足球学校。

## 职业生涯

### 俱乐部

1998年6月，韩鹏以前锋的身份入选了山东鲁能三队，此时韩鹏进行正规足球训练还不到一年的时间。1999年韩鹏直接从鲁能三队“跳级”进入一队，不过随后他在2001年回到二队。

在2002年[宿茂臻退役后](../Page/宿茂臻.md "wikilink")，[山东鲁能泰山足球俱乐部需要找到一名高中锋代替他](../Page/山东鲁能泰山足球俱乐部.md "wikilink")，韩鹏这时从二队上调到一队，最初只是山东队的替补前锋而已，但后来逐渐赢得主教练[留比萨·图巴科维奇和队友们的信任](../Page/留比萨·图巴科维奇.md "wikilink")，成为了主力球员。在场上韩鹏善于在空中争抢头球，强点意识出众。韩鹏曾在2007和2008赛季两度重伤，一定程度上影响了他在总进球和总出场的数据。

2010中超联赛第14轮，山东鲁能4-1大胜[长沙金德的比赛中韩鹏](../Page/长沙金德.md "wikilink")5分钟内上演了帽子戏法。值得注意的是，这是韩鹏的第一个帽子戏法，也创造了中国顶级联赛完成帽子戏法时隔时间最短的纪录。

2011年4月20日山东鲁能主场5-0横扫[阿雷马](../Page/阿雷马足球俱乐部.md "wikilink")，迎来了一场酣畅淋漓的胜利。比赛中，替补出场的韩鹏攻入一球，将比分扩大为3-0。这个进球是韩鹏在亚冠赛场攻入的第10球，追平了[郝海东的亚冠进球纪录](../Page/郝海东.md "wikilink")。此前，韩鹏已经创造了亚冠出场次数最多的纪录。

2011中超联赛第17轮，山东鲁能在客场1-1[长春亚泰的比赛中](../Page/长春亚泰.md "wikilink")，韩鹏为球队打进扳平的一球，这是韩鹏个人的第74个中超联赛进球，超过[李金羽的](../Page/李金羽.md "wikilink")73个进球成为中超联赛进球最多的球员。

2015年，韩鹏将球衣号码9号让给新外援[塔尔德利](../Page/迭戈·塔尔德利.md "wikilink")，身披18号继续为鲁能效力。\[1\]

### 国家队

韩鹏2006年開始入選[中國國家足球隊](../Page/中國國家足球隊.md "wikilink")。

在[2007年亚洲杯赛上](../Page/2007年亞洲盃足球賽.md "wikilink")，韩鹏成为国足的主力首发前锋。

## 聯賽上陣紀錄

  - <span style="font-size:smaller;">最後更新:2015年10月31日</span>

<table>
<thead>
<tr class="header">
<th><p>球季</p></th>
<th><p>俱乐部</p></th>
<th><p>國家</p></th>
<th><p>上陣</p></th>
<th><p>入球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>18</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>8</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>10</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>24</p></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>26</p></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>22</p></td>
<td><p>13</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>10</p></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>28</p></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>28</p></td>
<td><p>17</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>26</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>15</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>15</p></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>21</p></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/山东鲁能泰山.md" title="wikilink">山东鲁能泰山</a></p></td>
<td></td>
<td><p>17</p></td>
<td><p>2</p></td>
</tr>
</tbody>
</table>

## 国家队进球

  - 仅列出国际A级比赛进球

<table>
<thead>
<tr class="header">
<th><p>时间</p></th>
<th><p>地点</p></th>
<th><p>对手</p></th>
<th><p>比分</p></th>
<th><p>赛事</p></th>
<th><p>进球(累计)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2006年11月15日</p></td>
<td><p><a href="../Page/长沙市.md" title="wikilink">长　沙</a></p></td>
<td></td>
<td><p>1-1</p></td>
<td><p><a href="../Page/2007年亞洲盃足球賽.md" title="wikilink">2007年亚洲杯预选赛</a></p></td>
<td><p>1( 1)</p></td>
</tr>
<tr class="even">
<td><p>2007年2月7日</p></td>
<td><p><a href="../Page/苏州市.md" title="wikilink">苏　州</a></p></td>
<td></td>
<td><p>2-1</p></td>
<td><p>友谊赛</p></td>
<td><p>1( 2)</p></td>
</tr>
<tr class="odd">
<td><p>2007年3月27日</p></td>
<td><p><a href="../Page/澳门.md" title="wikilink">澳　门</a></p></td>
<td></td>
<td><p>3-1</p></td>
<td><p>友谊赛</p></td>
<td><p>2( 4)</p></td>
</tr>
<tr class="even">
<td><p>2007年7月10日</p></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a></p></td>
<td></td>
<td><p>5-1</p></td>
<td><p><a href="../Page/2007年亞洲盃足球賽.md" title="wikilink">2007年亚洲杯</a></p></td>
<td><p>2( 6)</p></td>
</tr>
<tr class="odd">
<td><p>2008年3月15日</p></td>
<td><p><a href="../Page/昆明市.md" title="wikilink">昆　明</a></p></td>
<td></td>
<td><p>3-3</p></td>
<td><p>友谊赛</p></td>
<td><p>1( 7)</p></td>
</tr>
<tr class="even">
<td><p>2009年7月25日</p></td>
<td><p><a href="../Page/天津市.md" title="wikilink">天　津</a></p></td>
<td></td>
<td><p>3-0</p></td>
<td><p>友谊赛</p></td>
<td><p>1( 8)</p></td>
</tr>
<tr class="odd">
<td><p>2009年11月8日</p></td>
<td><p><a href="../Page/科威特城.md" title="wikilink">科威特城</a></p></td>
<td></td>
<td><p>2-2</p></td>
<td><p>友谊赛</p></td>
<td><p>1( 9)</p></td>
</tr>
<tr class="even">
<td><p>2009年12月30日</p></td>
<td><p><a href="../Page/义乌市.md" title="wikilink">义　乌</a></p></td>
<td></td>
<td><p>2-2</p></td>
<td><p>友谊赛</p></td>
<td><p>1(10)</p></td>
</tr>
</tbody>
</table>

## 荣誉

### 俱乐部

#### 山东鲁能泰山

  - [中国足球超级联赛冠军](../Page/中国足球超级联赛.md "wikilink")：[2006](../Page/2006年中国足球超级联赛.md "wikilink")、[2008](../Page/2008年中国足球超级联赛.md "wikilink")、[2010](../Page/2010年中国足球超级联赛.md "wikilink")
  - [中国足协杯冠军](../Page/中国足协杯.md "wikilink")：[2004](../Page/2004年中国足协杯.md "wikilink")，[2006](../Page/2006年中国足协杯.md "wikilink")、[2014](../Page/2014年中国足协杯.md "wikilink")
  - [中国超級联賽杯冠军](../Page/中国超級联賽杯.md "wikilink")：[2004](../Page/2004年中国超级联赛杯.md "wikilink")
  - [中国足球协会超级杯冠军](../Page/中国足球协会超级杯.md "wikilink")：[2015](../Page/2015年中国足球协会超级杯.md "wikilink")

### 个人

  - [中国足协杯最佳射手](../Page/中国足协杯.md "wikilink")：[2006](../Page/2006年中国足协杯.md "wikilink")

## 参考资料

[Category:中国奥运足球运动员](../Category/中国奥运足球运动员.md "wikilink")
[Category:济南籍足球运动员](../Category/济南籍足球运动员.md "wikilink")
[Category:中国国家足球队运动员](../Category/中国国家足球队运动员.md "wikilink")
[Category:山东鲁能泰山球员](../Category/山东鲁能泰山球员.md "wikilink")
[Category:2008年夏季奥林匹克运动会足球运动员](../Category/2008年夏季奥林匹克运动会足球运动员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:中超球员](../Category/中超球员.md "wikilink")
[Peng鹏](../Category/韩姓.md "wikilink")

1.  [网易：鲁能备战超级杯球员换号码
    塔神9号韩鹏披18号](http://sports.163.com/15/0213/13/AIBBJS4100051C89.html)