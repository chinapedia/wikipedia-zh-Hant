**音色**是音的特色。不同音色的声音，即使在同一[音高和同一](../Page/音高.md "wikilink")[响度的情况下](../Page/响度.md "wikilink")，也能让人区分开来。同样的[音高和](../Page/音高.md "wikilink")[响度配上不同的音色就好比同样](../Page/响度.md "wikilink")[色相和](../Page/色相.md "wikilink")[明度配上不同的](../Page/明度.md "wikilink")[色度一样](../Page/色度.md "wikilink")。

## 定義

[美国国家标准协会将音色定义为](../Page/美国国家标准协会.md "wikilink")“……一种感官属性，使听者可以根据它判断出两个具有相同的[响度和](../Page/响度.md "wikilink")[音高的音是不相似的](../Page/音高.md "wikilink")。（that
attribute of sensation in terms of which a listener can judge that two
sounds having the same loudness and pitch are
dissimilar）”对1960年的定义的注释中（第45页）增加了“音色主要决定于声音频谱对人的刺激，但也决定于[波形](../Page/波形.md "wikilink")、[声压](../Page/声压.md "wikilink")、[频谱的频率位置和频谱对人的时间性刺激](../Page/频谱.md "wikilink")。

## 原理

[声音是由发声的物体震动产生的](../Page/声音.md "wikilink")。当发声物体的主体震动时会发出一个基音，同时其余各部分也有复合的震动，这些震动组合产生[泛音](../Page/泛音.md "wikilink")。正是这些泛音决定了发生物体的音色，使人能辨别出不同的乐器甚至不同的人发出的声音。所以根据音色的不同可以划分出男音和女音；高音、中音和低音；弦乐和管乐等。

所有泛音都比基音的频率高，但强度都相当弱，否则就无法调准乐器的音高了。

## 屬性

J.F. Schouten（1968,
p.42）将“音色难以捕捉的属性”描述为由[罗伯特·埃里克松](../Page/罗伯特·埃里克松.md "wikilink")（1975）“根据很多现代音乐来衡量”而发现，“至少由以下五个声学参数决定”：

1.  介于音调与类噪音之类（The range between tonal and noiselike character.）
2.  波谱包络面（The spectral envelope.）

## 頻譜

## 波形

**音色**同樣也受到以下參數的影響，Attack（聲音從無到峰值的時間）、Interonset
interval、Decay（經過Attack，由峰值降回平穩狀態的時間）、Sustain（平穩狀態的時間）、Release（由平穩開始衰減到無的時間）和[暫態](../Page/暫態.md "wikilink")，因此這些都是[取樣器可控制的參數](../Page/取樣器.md "wikilink")。舉個例子，若取走[鋼琴或是](../Page/鋼琴.md "wikilink")[小號所產生訊號中的Attack成分](../Page/小號.md "wikilink")，將導致聽者難以辨認樂器的音色，這是由於鋼琴內的音鎚敲擊琴弦或嘴唇與吹嘴接觸發出的氣聲，皆為辨認樂器音色的重要特徵。

## 音樂領域

## 相关概念

  - 音质：声音的品质，包括音高、音调和音色三个方面。
  - 力度：声音的强度，即震动的幅度。
  - 音调：即音频、音高，震动的频率。
  - 音品：即音色，即泛音与谐波。

[Category:音樂理論](../Category/音樂理論.md "wikilink")