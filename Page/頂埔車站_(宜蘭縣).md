[TRA_EMC412_in_Dingpu_Station_20070705.jpg](https://zh.wikipedia.org/wiki/File:TRA_EMC412_in_Dingpu_Station_20070705.jpg "fig:TRA_EMC412_in_Dingpu_Station_20070705.jpg")
**頂埔車站**位於[台灣](../Page/台灣.md "wikilink")[宜蘭縣](../Page/宜蘭縣.md "wikilink")[頭城鎮](../Page/頭城鎮.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[宜蘭線的](../Page/宜蘭線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。

## 車站構造

  - [岸式月台兩座](../Page/岸式月台.md "wikilink")。
  - 設於[平交道旁](../Page/平交道.md "wikilink")。
  - 站內設有[天橋連絡兩月台](../Page/天橋.md "wikilink")。（兩月台有設置出入口）
  - 車站站房已停用。

## 利用狀況

  - 目前為[招呼站](../Page/臺灣鐵路管理局車站等級#招呼站.md "wikilink")。
  - 僅停靠區間車。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通及](../Page/一卡通_\(台灣\).md "wikilink")[icash
    2.0付費](../Page/icash.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 2000 | 62        |
| 2001 | 93        |
| 2002 | 78        |
| 2003 | 96        |
| 2004 | 86        |
| 2005 | 84        |
| 2006 | 94        |
| 2007 | 87        |
| 2008 | 95        |
| 2009 | 95        |
| 2010 | 121       |
| 2011 | 117       |
| 2012 | 125       |
| 2013 | 133       |
| 2014 | 126       |
| 2015 | 132       |
| 2016 | 120       |

歷年旅客人次紀錄

## 車站週邊

  - 金面溪

## 公車資訊

### 國道客運

  - [國光客運](../Page/國光客運.md "wikilink")：
      - **<font color="red">1811</font>**
        [台北](../Page/台北車站.md "wikilink")－[20px中山高](../Page/檔案:TWHW1.svg.md "wikilink")－－[羅東](../Page/羅東轉運站.md "wikilink")
      - **<font color="red">1877</font>**
        [圓山轉運站](../Page/圓山轉運站.md "wikilink")－[20px中山高](../Page/檔案:TWHW1.svg.md "wikilink")－南港－環東大道－[20px北二高](../Page/檔案:TWHW3.svg.md "wikilink")－[20px北宜高](../Page/檔案:TWHW5.svg.md "wikilink")--二城-頂埔-頭城－烏石港

### 公路客運

  - [國光客運](../Page/國光客運.md "wikilink")：
      - **<font color="red">1740</font>** 宜蘭-雙溪

### [宜蘭縣市區公車](../Page/宜蘭縣市區公車.md "wikilink")

  - [葛瑪蘭客運](../Page/葛瑪蘭客運.md "wikilink")：
      - **<font color="red">131</font>** 礁溪車站－東北角風景區外澳站
  - [國光客運](../Page/國光客運.md "wikilink")：
      - **<font color="red">紅1</font>** 外澳－羅東轉運站(假日行駛)
      - **<font color="red">1766</font>** 南方澳－烏石港(平日行駛)
      - **<font color="red">1767</font>** 南方澳－純精路－頭城(平日行駛)

### 鄉鎮市區免費巴士

  - 頭城鎮公所免費巴士：
      - **<font color="red">331海線</font>** 頂埔－石城
      - **<font color="red">332山線</font>** 頂埔－烏石港

## 圖片集

<File:Dingpu> Station Front.jpg|頂埔車站已經停用的車站本體 <File:Dingpu> Station
Platform.jpg|頂埔車站的月台與人行天橋 <File:Dingpu> Station.jpg|設於平交道旁的頂埔站
<File:Dingpu> Station south view.jpg|頂埔站南觀

## 歷史

  - 1937年5月1日：設站。
  - 1960年4月10日：復設招呼站，155、158、163、166次普通客車增停本站。
  - 2004年9月6日：管理站由[頭城站改為](../Page/頭城車站.md "wikilink")[礁溪站](../Page/礁溪車站.md "wikilink")。
  - 2005年1月16日：由簡易站降為招呼站。
  - 2015年6月30日：啟用多卡通刷卡機。

## 鄰近車站

  - **現行營運模式**

<!-- end list -->

  - **未來營運模式**

（計畫中）  （計畫中） |-

## 相關條目

  - [頂埔車站](../Page/頂埔車站.md "wikilink")：關於其他同名車站。

## 參考文獻

## 外部連結

[Category:宜蘭線車站](../Category/宜蘭線車站.md "wikilink")
[Category:宜蘭縣鐵路車站](../Category/宜蘭縣鐵路車站.md "wikilink")
[Category:1937年启用的铁路车站](../Category/1937年启用的铁路车站.md "wikilink")
[Category:頭城鎮](../Category/頭城鎮.md "wikilink")