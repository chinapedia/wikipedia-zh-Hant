**頂棘龍屬**（[屬名](../Page/屬.md "wikilink")：**）是[獸腳亞目下的一](../Page/獸腳亞目.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生活於[下白堊紀的](../Page/白堊紀.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，化石發現於[德國](../Page/德國.md "wikilink")[下薩克森州的](../Page/下薩克森州.md "wikilink")[奥伯基希砂石層](../Page/奥伯基希砂石層.md "wikilink")（Obernkirchen
Sandstein），地質年代相當於[巴列姆階](../Page/巴列姆階.md "wikilink")。過去曾有其他化石被歸類於頂棘龍，來自於[英格蘭](../Page/英格蘭.md "wikilink")、[比利時](../Page/比利時.md "wikilink")，地質年代從白堊紀早期的[凡藍今階到](../Page/凡藍今階.md "wikilink")[阿普第階](../Page/阿普第階.md "wikilink")，但無法確定屬於頂棘龍\[1\]\[2\]。

牠的[模式種學名為](../Page/模式種.md "wikilink")**丹氏頂棘龍**（*A.
dunkeri*），是在1884年被歸類於[斑龍的一個種](../Page/斑龍.md "wikilink")，只根據[牙齒而被描述](../Page/牙齒.md "wikilink")、命名。在1923年，Friedrich
von
Huene將這個種，命名為獨立的屬。牠的屬名的意思是「高棘」\[3\]。過去曾有其他已敘述[種](../Page/種.md "wikilink")，現已被分類在其他屬中，當中包括了[重爪龍](../Page/重爪龍.md "wikilink")。原先被認為跟牙齒有關係的[脊椎](../Page/脊椎.md "wikilink")，曾一度改屬於[肉食龍下目的](../Page/肉食龍下目.md "wikilink")[比克爾斯棘龍](../Page/比克爾斯棘龍.md "wikilink")，但如今比克爾斯棘龍普遍被認為是頂棘龍的[異名](../Page/異名.md "wikilink")。

## 參考資料

  - Lydekker, 1888. Catalogue of the Fossil Reptilia and Amphibia in the
    British Museum (Natural History), Cromwell Road, S.W., Part 1.
    Containing the Orders Ornithosauria, Crocodilia, Dinosauria,
    Squamta, Rhynchocephalia, and Proterosauria: British Museum of
    Natural History, London, 309pp.
  - Huene, 1926. The carnivorous Saurischia in the Jura and Cretaceous
    formations, principally in Europe. Revista del Museo de La Plata.
    29, 1-167.

## 外部链接

  - [Dinoruss](http://www.dinoruss.com/de_4/5a7e946.htm)
  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:棘龍科](../Category/棘龍科.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.  <https://archive.is/20120630212336/home.comcast.net/~eoraptor/Neotheropoda.htm%23Altispinaxdunkeri>
2.  <http://www.dinodata.org/index.php?option=com_content&task=view&id=6014&Itemid=67>
3.  Huene, 1923. Carnivorous Saurischia in Europe since the Triassic.
    Bulletin of the Geological Society of America. 34: 449-458.