**順德聯誼總會何日東小學**（），是位於[香港](../Page/香港.md "wikilink")[屯門區](../Page/屯門區.md "wikilink")[友愛邨的第五座小學校舍](../Page/友愛邨.md "wikilink")，由香港[慈善團體](../Page/慈善團體.md "wikilink")[順德聯誼總會在](../Page/順德聯誼總會.md "wikilink")1983年成立。在屯門區中有同屬順德聯誼總會的[順德聯誼總會譚伯羽中學](../Page/順德聯誼總會譚伯羽中學.md "wikilink")、[順德聯誼總會胡少渠紀念小學](../Page/順德聯誼總會胡少渠紀念小學.md "wikilink")、[順德聯誼總會梁銶琚中學及順德聯誼總會梁李秀娛幼稚園](../Page/順德聯誼總會梁銶琚中學.md "wikilink")，位於屯門友愛邨及安定邨。而順德聯誼總會何日東小學的下午校將於2011年與[順德聯誼總會胡少渠紀念小學的下午校合併遷至位於掃管笏的新校舍](../Page/順德聯誼總會胡少渠紀念小學.md "wikilink")，並改名為[順德聯誼總會李金小學](../Page/順德聯誼總會李金小學.md "wikilink")，轉為以全日制授課。

## 知名校友

  - [鄭詩亭](../Page/鄭詩亭.md "wikilink")：有線新聞記者

## 重大事件

  - [2008年香港爆發流行性感冒事件](../Page/2008年香港爆發流行性感冒事件.md "wikilink")\[1\]

## 参見

  - [順德聯誼總會梁銶琚中學](../Page/順德聯誼總會梁銶琚中學.md "wikilink")

## 參考資料

[Category:順德聯誼總會屬會學校](../Category/順德聯誼總會屬會學校.md "wikilink")
[Category:屯門市中心](../Category/屯門市中心.md "wikilink")
[Category:屯門區小學](../Category/屯門區小學.md "wikilink")
[Category:順德聯誼總會在香港創辦的小學](../Category/順德聯誼總會在香港創辦的小學.md "wikilink")

1.  [屯門順德聯誼總會何日東小學，下午校二年級七歲男童羅浩明，甲型及乙型流感病毒](http://hk.news.yahoo.com/080310/60/2qbmv.html)