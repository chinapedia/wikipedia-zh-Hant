[FC_Shakhtar_Donetsk.jpg](https://zh.wikipedia.org/wiki/File:FC_Shakhtar_Donetsk.jpg "fig:FC_Shakhtar_Donetsk.jpg")
**薩克達足球會**（），是[烏克蘭足球超級聯賽球隊](../Page/烏克蘭足球超級聯賽.md "wikilink")，该队汇集了东欧国家的许多国脚，并在2000年后的乌克兰联赛中占据统治地位。

## 球會榮譽

  - **[烏克蘭足球超級聯賽](../Page/烏克蘭足球超級聯賽.md "wikilink")：11**

<!-- end list -->

  -

      -
        2002, 2005, 2006, 2008, 2010, 2011, 2012, 2013, 2014, 2017, 2018

<!-- end list -->

  - **[烏克蘭盃](../Page/烏克蘭盃.md "wikilink")：12**

<!-- end list -->

  -

      -
        1995, 1997, 2001, 2002, 2004, 2008, 2011, 2012, 2013, 2016,
        2017, 2018

<!-- end list -->

  - **[烏克蘭超級盃](../Page/烏克蘭超級盃.md "wikilink")：4**

<!-- end list -->

  -

      -
        2005, 2008, 2010, 2012

<!-- end list -->

  - **[歐洲足協盃](../Page/歐洲足協盃.md "wikilink")：1**

<!-- end list -->

  -

      -
        [2009](../Page/2008–09年歐洲足協盃.md "wikilink")

<!-- end list -->

  - **[蘇聯盃](../Page/蘇聯盃.md "wikilink")：4**

<!-- end list -->

  -

      -
        1961, 1962, 1980, 1983

<!-- end list -->

  - **[蘇聯超級盃](../Page/:en:Soviet_Super_Cup.md "wikilink")：1**

<!-- end list -->

  -

      -
        1983

## 現役球員

## 參考資料

## 外部連結

  - [官方網站](http://shakhtar.com/)
  - [烏克蘭足球網](https://web.archive.org/web/20090302015708/http://www.ukrainiansoccer.net/)

[Category:頓涅茨克](../Category/頓涅茨克.md "wikilink")
[Category:乌克兰足球俱乐部](../Category/乌克兰足球俱乐部.md "wikilink")