**領鞭毛蟲綱**（學名：）是一種[原生生物](../Page/原生生物.md "wikilink")，是[单细胞或群体](../Page/单细胞.md "wikilink")。擁有一根[鞭毛](../Page/鞭毛.md "wikilink")，形态类似于[淡水海绵的群体](../Page/淡水海绵.md "wikilink")，表面的[酪氨酸激酶受体也类似于海绵](../Page/酪氨酸激酶受体.md "wikilink")，被认为是[动物的](../Page/动物.md "wikilink")[姐妹群](../Page/姐妹群.md "wikilink")\[1\]。

## 語源

領鞭毛蟲的學名*Choanoflagellate*源於[希臘語的](../Page/希臘語.md "wikilink")（可解作「漏斗」或「衣領」）和[拉丁語的](../Page/拉丁語.md "wikilink")（意指[鞭毛](../Page/鞭毛.md "wikilink")）。

## 外貌及成長

[Salpingoeca_sp..jpg](https://zh.wikipedia.org/wiki/File:Salpingoeca_sp..jpg "fig:Salpingoeca_sp..jpg")).\]\]
每條領鞭毛蟲都有一條鞭毛，由一圈充滿[肌動蛋白](../Page/肌動蛋白.md "wikilink")、叫作[微绒毛的突出物環繞](../Page/微绒毛.md "wikilink")，在鞭毛外形成了一圈圓柱狀或椎狀的「衣領」，因以為名。
鞭毛的移動可透過「衣領」抽水，[細菌及碎屑會被微絨毛捕獲](../Page/細菌.md "wikilink")，然後被領鞭毛蟲進食。由鞭毛產生的水流亦會推動細胞，使之可以自由游動，就好像[動物的](../Page/動物.md "wikilink")[精蟲一樣](../Page/精蟲.md "wikilink")。相對來說，其他鞭毛蟲往往是透過其鞭毛的活動來「拉扯」鞭毛蟲移動。

## 結構及演化

目前有兩個物種的整個基因及兩個物種的[轉錄組的基因已被完整排序並出版](../Page/轉錄組.md "wikilink")，方便了對領鞭毛蟲及生物演化的研究。

### *Monosiga brevicollis*

**领鞭毛虫**（）是一种单细胞水生动物，具有特殊的结构：其触手呈领状环绕鞭毛，与形成海绵（最原始的多细胞生物）的领细胞基本结构相同。其基因有41.6[Mbp](../Page/鹼基對.md "wikilink")，與[絲狀真菌及其他自由生活的單細胞生物的基因數量相若](../Page/絲狀真菌.md "wikilink")，但遠少於一般的動物。

[萨克生物研究院研究发现](../Page/萨克生物研究院.md "wikilink")，领鞭毛虫的[酪氨酸激酶](../Page/酪氨酸激酶.md "wikilink")（Tyrosine
Kinases）基因竟高达128种之多，比人类中所发现的多38种，而[酪氨酸激酶调控网络对后生动物进化有重大作用](../Page/酪氨酸激酶.md "wikilink")。一般认为，酪氨酸激酶调控只在多细胞动物中进行，酪氨酸激酶调控网络越来越复杂的演化造成了多细胞动物本身复杂性演化。但作为单细胞动物的领鞭毛虫是目前发现的唯一例外。

一項[演化基因組學研究發現](../Page/演化基因組學.md "wikilink")：在領鞭毛蟲的基因組內發現多條來自[藍綠藻的基因](../Page/藍綠藻.md "wikilink")。負責研究的學者認為：可能在早基的演化歷史中，領鞭毛蟲透過[吞噬作用捕食藍綠藻作食物時](../Page/吞噬作用.md "wikilink")，藍綠藻的基因殘留了在領鞭毛蟲內融合了。

另一項研究發現：領鞭毛蟲的基因內有一條叫[GKPID](../Page/GKPID.md "wikilink")（）的基因，負責控制其鞭毛在細胞分裂時的方向。這條基因被認為標誌着單細胞生物過渡成為多細胞生物的演化歷史。

### *Salpingoeca rosetta*

*[Salpingoeca rosetta](../Page/Salpingoeca_rosetta.md "wikilink")*的
基因大小有55
Mbp。[細胞黏附](../Page/細胞黏附.md "wikilink")、[神經肽和](../Page/神經肽.md "wikilink")[鞘醣脂代謝的基因的同源物存在於基因組中](../Page/鞘醣脂代謝.md "wikilink")。

### *Monosiga ovata* 的轉錄組

An EST dataset from *Monosiga ovata* was published in 2006. The major
finding of this transcriptome was the choanoflagellate Hoglet domain and
shed light on the role of domain shuffling in the evolution of the
[Hedgehog signaling
pathway](../Page/Hedgehog_signaling_pathway.md "wikilink").

### *Stephanoeca diplocostata* 的轉錄組

本物種的轉錄組是有[砂殼的領鞭毛蟲屬物種的首個導致領鞭毛蟲的](../Page/砂殼.md "wikilink")[矽傳輸基因的發現](../Page/矽傳輸.md "wikilink")。
隨後，類似的基因亦有在另一個有砂殼的領鞭毛蟲物種*[Diaphanoeca
grandis](../Page/Diaphanoeca_grandis.md "wikilink")*發現。透過分析這些基因， the
choanoflagellate SITs show homology to the SIT-type silicon transporters
of [diatoms](../Page/diatoms.md "wikilink") and have evolved through
[horizontal gene
transfer](../Page/horizontal_gene_transfer.md "wikilink").

## 參考文獻

## 外部連結

  -
[Category:原生生物](../Category/原生生物.md "wikilink")

1.