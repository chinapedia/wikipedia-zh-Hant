**頜翼龍屬**（屬名：*Gnathosaurus*）意為「頜部[蜥蜴](../Page/蜥蜴.md "wikilink")」，是[翼龍目](../Page/翼龍目.md "wikilink")[梳頜翼龍科的一屬](../Page/梳頜翼龍科.md "wikilink")，目前只有一個種*G.
subulatus*，是由1833年所敘述、命名。頜翼龍的翼展估計約1.7公尺寬，頭顱骨長度為28公分。頜翼龍擁有將近130顆針狀牙齒，排列於匙狀嘴巴的後部。頜翼龍的下頜碎片是在1832年於[德國南部的](../Page/德國.md "wikilink")[索倫霍芬石灰岩層發現](../Page/索倫霍芬.md "wikilink")，並曾誤認為是[鱷魚下頜的碎片](../Page/鱷魚.md "wikilink")。直到一個世紀後，1951年發現了一個頜翼龍的頭顱骨，才發現牠們屬於翼龍類。

關於頜翼龍的食性是完全根據假設。牠們的牙齒排列成匙狀，可能用來過濾水中的小型動物。

數個古生物學家，例如[克里斯多福·班尼特](../Page/克里斯多福·班尼特.md "wikilink")（Christopher
Bennett），提出[翼手龍的一個體型嬌小種](../Page/翼手龍屬.md "wikilink")（*Pterodactylus
micronyx*），其實是頜翼龍的幼年個體\[1\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - <http://www.pterosaur.co.uk>

[Category:梳頜翼龍超科](../Category/梳頜翼龍超科.md "wikilink")
[Category:侏羅紀翼龍類](../Category/侏羅紀翼龍類.md "wikilink")

1.  Bennett, S.C. (2002). "Soft tissue preservation of the cranial crest
    of the pterosaur *Germanodactylus* from Solnhofen." *Journal of
    Vertebrate Paleontology*, **22**(1): 43-48.