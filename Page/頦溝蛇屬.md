**頦溝蛇屬**（[學名](../Page/學名.md "wikilink")：*Melanophidium*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[盾尾蛇科下的一個](../Page/盾尾蛇科.md "wikilink")[屬](../Page/屬.md "wikilink")，主要包括分布於南[印度一帶的一種無毒蛇種](../Page/印度.md "wikilink")。目前共有3個品種已被確認。\[1\]

## 地理分布

頦溝蛇主要分布於南[印度的](../Page/印度.md "wikilink")[西高止山脈一帶](../Page/西高止山脈.md "wikilink")。\[2\]

## 品種

| 品種\[3\]                                                                 | 學名及命名者\[4\]                                          | 異稱     | 地理分布\[5\]                                                           |
| ----------------------------------------------------------------------- | ---------------------------------------------------- | ------ | ------------------------------------------------------------------- |
| **[雙線頦溝蛇](../Page/雙線頦溝蛇.md "wikilink")**                                | Melanophidium bilineatum，<small>Beddome，1870</small> | 雙線黑盾尾蛇 | 南[印度](../Page/印度.md "wikilink")[西高止山脈](../Page/西高止山脈.md "wikilink") |
| **[點斑頦溝蛇](../Page/點斑頦溝蛇.md "wikilink")**                                | Melanophidium punctatum，<small>Beddome，1871</small>  | 貝氏黑盾尾蛇 | 南[印度](../Page/印度.md "wikilink")[西高止山脈](../Page/西高止山脈.md "wikilink") |
| **[頦溝蛇](../Page/頦溝蛇.md "wikilink")**<font size="-1"><sup>T</sup></font> | Melanophidium wynaudense，<small>Beddome，1863</small> | 印度黑土蛇  | 南印度西高止山脈                                                            |
|                                                                         |                                                      |        |                                                                     |

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：頦溝蛇屬](http://reptile-database.reptarium.cz/search.php?&genus=Melanophidium&submit=Search)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:盾尾蛇科](../Category/盾尾蛇科.md "wikilink")

1.

2.
3.
4.
5.