**頭份交流道**為[台灣](../Page/台灣.md "wikilink")[國道一號中山高速公路的交流道](../Page/中山高速公路.md "wikilink")\[1\]，位於台灣[苗栗縣](../Page/苗栗縣.md "wikilink")[頭份市](../Page/頭份市.md "wikilink")，指標為110k，是[苗栗縣北部的主要聯外交流道](../Page/苗栗縣.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW1.svg" title="fig:TWHW1.svg">TWHW1.svg</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_PHW1.svg" title="fig:TW_PHW1.svg">TW_PHW1.svg</a></p></td>
<td><p><strong>頭份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_CHW124.svg" title="fig:TW_CHW124.svg">TW_CHW124.svg</a></p></td>
<td><p><strong>三灣</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 鄰近公路系統（國道除外）

  -
  -
## 鄰近交流道

## 参考

[Category:苗栗縣交流道](../Category/苗栗縣交流道.md "wikilink")
[Category:頭份市](../Category/頭份市.md "wikilink")

1.