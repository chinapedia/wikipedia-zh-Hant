**頭孢曲松**（Ceftriaxone），商品名為羅氏芬（Rocephin），是一種可用於治療如[革蘭氏陽性菌及](../Page/革蘭氏陽性菌.md "wikilink")[革蘭氏陰性菌等多種細菌感染的抗生素](../Page/革蘭氏陰性菌.md "wikilink")，包括[中耳炎](../Page/中耳炎.md "wikilink")、[心內膜炎](../Page/心內膜炎.md "wikilink")、[腦膜炎](../Page/腦膜炎.md "wikilink")、[肺炎](../Page/肺炎.md "wikilink")、骨關節炎、腹腔內感染、皮膚炎、[泌尿道感染](../Page/泌尿道感染.md "wikilink")、[淋病和](../Page/淋病.md "wikilink")[骨盆腔發炎](../Page/骨盆腔發炎.md "wikilink")\[1\]。有時也會在手術前和咬傷後使用，以防止感染\[2\]。
頭孢曲松可以通過靜脈或肌肉注射\[3\] 。

此藥常見的副作用包括注射部位疼痛和過敏反應\[4\]
。其他可能的副作用包括[偽膜性結腸炎](../Page/偽膜性結腸炎.md "wikilink")、、[膽石症和](../Page/膽石症.md "wikilink")[癲癇](../Page/癲癇.md "wikilink")\[5\]
。此藥不建議用於對[青黴素過敏的患者](../Page/青黴素.md "wikilink")，但副作用較輕的患者除外\[6\]
。如採用靜脈注射方式時，不應同時從靜脈輸注[鈣](../Page/鈣.md "wikilink")\[7\]
。初步證據顯示此藥用於懷孕和哺乳期間是相對安全的\[8\]。此藥是第三代的[頭孢菌素](../Page/頭孢菌素.md "wikilink")，藥理為抑制細菌[細胞壁的製造](../Page/細胞壁.md "wikilink")\[9\]。

此藥是由[羅氏大藥廠在](../Page/羅氏.md "wikilink")20世紀80年代早期所發明出來\[10\]，並已列名[世界衛生組織基本藥物標準清單](../Page/世界衛生組織基本藥物標準清單.md "wikilink")，為所需最有效和最安全的藥物之一\[11\]，目前也有其他的[學名藥](../Page/學名藥.md "wikilink")\[12\]
。在已開發國家，此藥每劑的批發成本在2014年時約為0.20至2.32美元\[13\]；而在美國以此藥治療的療程通常費用不會超過25美元\[14\]。

## 臨床使用

  -

頭孢曲松經常（與[大環內酯及](../Page/大環內酯.md "wikilink")／或[氨基糖苷類抗生素一起](../Page/氨基糖苷類抗生素.md "wikilink")）用於治療社區感染的[肺炎](../Page/肺炎.md "wikilink")。它是一種用於治療[細菌性腦膜炎的藥物之一](../Page/細菌性腦膜炎.md "wikilink")。在小兒科，它普遍使用在治療4－8個星期大幼兒的[敗血病](../Page/敗血病.md "wikilink")。它亦可治療[萊姆病及](../Page/萊姆病.md "wikilink")[淋病](../Page/淋病.md "wikilink")。

一般開始時的劑量是每天靜脈注射1g。其後每12－24小時則為1－2g（靜脈注射或肌注），按照感染的種類及嚴重性，可調校至每日4g。對於患有淋病的成年人，劑量是一次肌注250mg。治療淋病病人一般需要與[阿奇黴素一併使用](../Page/阿奇黴素.md "wikilink")，並處理[衣原體屬](../Page/衣原體屬.md "wikilink")。

頭孢曲松禁止使用於對[頭孢菌素](../Page/頭孢菌素.md "wikilink")、[青黴素及](../Page/青黴素.md "wikilink")／或[碳青黴烯敏感的病人](../Page/碳青黴烯.md "wikilink")。

## 化學結構

頭孢曲松是一種橙黃色的結晶粉，可溶於[水](../Page/水.md "wikilink")，僅溶於[甲醇及甚少溶於](../Page/甲醇.md "wikilink")[乙醇](../Page/乙醇.md "wikilink")。1%的頭孢曲松溶液的[pH值約為](../Page/pH值.md "wikilink")6.7。

[甲氧基](../Page/甲氧基.md "wikilink")[亞胺的結構能提高在於對](../Page/亞胺.md "wikilink")[革蘭氏陰性菌所分泌的](../Page/革蘭氏陰性菌.md "wikilink")[β內醯胺酶的穩定性](../Page/β內醯胺酶.md "wikilink")。這種特別的穩定性能增加頭孢曲松對抗革蘭氏陰性菌的阻力。取代[頭孢噻肟易於](../Page/頭孢噻肟.md "wikilink")[水解的](../Page/水解.md "wikilink")[乙醯](../Page/乙醯.md "wikilink")，頭孢曲松的一端有著代謝穩定的偶氮離子酮。

## 外部連結

  - [MedlinePlus Drug Information: Cephalosporins
    (systemic)](http://www.nlm.nih.gov/medlineplus/druginfo/uspdi/202119.html)
  - [羅氏芬®的相關資料](https://web.archive.org/web/20061017081419/https://www.rocheusa.com/products/rocephin/)

{{-}}

[Category:頭孢菌素](../Category/頭孢菌素.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")

1.
2.
3.

4.
5.
6.
7.
8.

9.
10.

11.

12.
13.

14.