**頭屋交流道**為[台灣](../Page/台灣.md "wikilink")[國道一號](../Page/國道一號.md "wikilink")[中山高速公路的交流道](../Page/中山高速公路.md "wikilink")\[1\]，位於台灣[苗栗縣](../Page/苗栗縣.md "wikilink")[頭屋鄉](../Page/頭屋鄉.md "wikilink")，指標為125k，於2013年8月2日下午2時開放通車，與[苗栗交流道同為距離苗栗市區最近的交流道](../Page/苗栗交流道.md "wikilink")。本交流道為連接[台72線快速公路的最佳方案](../Page/台72線.md "wikilink")\[2\]，需經由[台13線後銜接](../Page/台13線.md "wikilink")[台72線](../Page/台72線.md "wikilink")[頭屋一交流道](../Page/頭屋一交流道.md "wikilink")。公路總局評估整合[台72線](../Page/台72線.md "wikilink")[銅鑼交流道銜接](../Page/台72線.md "wikilink")[國道一號成系統交流道不具經濟效益](../Page/國道一號.md "wikilink")，故未設置。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW1.svg" title="fig:TWHW1.svg">TWHW1.svg</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_PHW72.svg" title="fig:TW_PHW72.svg">TW_PHW72.svg</a></p></td>
<td><p><strong>頭屋</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_PHW13.svg" title="fig:TW_PHW13.svg">TW_PHW13.svg</a></p></td>
<td><p><strong>造橋</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

  - [國道1號增設頭屋交流道工程（高速公路局）](http://www.freeway.gov.tw/Publish.aspx?cnid=95&p=1580)

## 鄰近交流道

|}

## 参考

[Category:苗栗縣交流道](../Category/苗栗縣交流道.md "wikilink")
[Category:頭屋鄉](../Category/頭屋鄉.md "wikilink")

1.
2.