**頭髮醬油**是在[中国](../Page/中国.md "wikilink")[中央电视台](../Page/中央电视台.md "wikilink")2004年被曝光的著名的[黑心食品之一](../Page/黑心食品.md "wikilink")，指一些使用[頭髮作為原材料製成的](../Page/頭髮.md "wikilink")[醬油](../Page/醬油.md "wikilink")。

## 媒体曝光

2004年1月5日，[湖北省](../Page/湖北省.md "wikilink")[荆州市一家调味品工厂用](../Page/荆州市.md "wikilink")“[氨基酸液](../Page/氨基酸.md "wikilink")”加[氢氧化钠](../Page/氢氧化钠.md "wikilink")、[盐酸](../Page/盐酸.md "wikilink")、[红糖](../Page/红糖.md "wikilink")、[色素等原料勾兑酱油](../Page/色素.md "wikilink")。而湖北省另一家专门生产“氨基酸液”的公司所采用的原料是各种富含[蛋白质的废物](../Page/蛋白质.md "wikilink")，主要有人的毛发，医用棉等。原料不经任何处理直接进行酸解，还原成氨基酸\[1\]。而湖北省的不肖业者生产的这些黑心头发酱油不仅在大陆销售，并且还流入[香港市场](../Page/香港.md "wikilink")。\[2\]

## 製造過程

奸商首先利用從[東北](../Page/中國東北.md "wikilink")、[華北等地](../Page/華北.md "wikilink")，以每[公斤](../Page/公斤.md "wikilink")1元[人民幣的價錢](../Page/人民幣.md "wikilink")，從[理髮店收集從客人身上剪下來的頭髮](../Page/理髮店.md "wikilink")，經簡單的挑揀後，再以每公斤1.8人民币的價錢賣給[湖北的](../Page/湖北.md "wikilink")[化工廠](../Page/化工廠.md "wikilink")。在化工廠內用[鹽酸和其他化學劑提煉](../Page/鹽酸.md "wikilink")，得到[氨基酸](../Page/氨基酸.md "wikilink")[溶液](../Page/溶液.md "wikilink")。這些溶液被賣至全國各地的小廠再製成醬油。由於政府對醬油內的氨基酸含量有嚴格限制，不法商人就用從頭髮提煉的氨基酸溶液去補充，以減少[大豆等一般醬油原料的使用](../Page/大豆.md "wikilink")，從而節省金錢。

## 危害

收集回來的頭髮經過含有大量[染髮料](../Page/染髮.md "wikilink")、整髮料等化學物，夾雜著大量[垃圾](../Page/垃圾.md "wikilink")，衛生惡劣。

提練出來的溶量含有、[鉛](../Page/鉛.md "wikilink")、[氯丙醇等有害物質](../Page/氯丙醇.md "wikilink")，製成的醬油含有[4-甲基咪唑](../Page/甲基咪唑.md "wikilink")，可誘發[癲癇](../Page/癲癇.md "wikilink")。食用這種醬油有[致癌的可能](../Page/致癌.md "wikilink")。

## 参考文献

## 参见

  - [黑心食品](../Page/黑心食品.md "wikilink")

[Category:醬油](../Category/醬油.md "wikilink")
[Category:黑心食品](../Category/黑心食品.md "wikilink")
[Category:中国食品安全](../Category/中国食品安全.md "wikilink")
[Category:2004年中國](../Category/2004年中國.md "wikilink")

1.
2.