[顏元.jpg](https://zh.wikipedia.org/wiki/File:顏元.jpg "fig:顏元.jpg")》第二集之颜元像\]\]
**顏元**（），[字](../Page/表字.md "wikilink")**易直**，又字**渾然**，[號](../Page/號.md "wikilink")**習齋**，[直隶](../Page/直隶.md "wikilink")[博野](../Page/博野.md "wikilink")（[河北](../Page/河北.md "wikilink")[安國縣東北](../Page/安國縣.md "wikilink")）人，[明末](../Page/明.md "wikilink")[清初](../Page/清.md "wikilink")[思想家](../Page/思想家.md "wikilink")、[教育家](../Page/教育家.md "wikilink")，[顏李學派](../Page/顏李學派.md "wikilink")（“李”指颜元的学生[李塨](../Page/李塨.md "wikilink")）的創始者。

## 生平

顏元的父親颜昶曾被[蠡縣一位小官吏朱九祚收為](../Page/蠡縣.md "wikilink")[養子](../Page/養子.md "wikilink")。顏元生在朱家，原名**朱邦良**，為蠡縣人，後其父颜昶因與朱家失和，於[明朝末年顏元四歲時](../Page/明朝.md "wikilink")，隨清兵逃往關外。其母王氏因夫去杳無音訊，於顏元12歲時改嫁。顏元便同其養祖父母一起生活。養祖母去世，顏元代父居喪，行朱子「三日不含，朝夕哭」的家禮，飢餓哀毀幾至於死。這使得顏元逐漸產生反[理學的思想](../Page/理學.md "wikilink")。

顏元青年時曾從事「耕田灌園」，晚年在[肥鄉县](../Page/肥鄉县.md "wikilink")[漳南書院任教](../Page/漳南書院.md "wikilink")。在學術上和學生[李塨創立](../Page/李塨.md "wikilink")[顏李學派](../Page/顏李學派.md "wikilink")，提倡並實用主義。顏元創立的學派和官方提倡的[程朱理學不同](../Page/程朱理學.md "wikilink")，主張讀書的目的應該是「經世致用」，而非一味「格物致知」，自尋自證「天理」。五十七歲時候，南遊[河南](../Page/河南.md "wikilink")，發現「見人人禪子，家家虛文，直與孔門敵對。必破一分程朱，始入一分[孔孟](../Page/孔孟.md "wikilink")，乃定以為孔孟、程朱兩途」\[1\]。他說：「[八股之害](../Page/八股.md "wikilink")，甚於焚坑」“其闢佛老，皆所自犯不覺”，[朱子的學術不過是](../Page/朱子.md "wikilink")“禪宗、訓詁、文字、鄉愿四者集成一種人”\[2\]，「千百年來，率天下入故紙中，耗盡身心氣力，作弱人，病人，無用人者，皆晦庵為也！」\[3\]。

## 著作

着有《存学编》四卷，《存性编》二卷，《存治编》一卷，《存人编》四卷，《朱子语类评》一卷，《礼文手钞》五卷，《四书正误》六卷，《习斋记余》十卷等。

## 影响

### 民国时期的四存学会

1920年，在[总统](../Page/总统.md "wikilink")[徐世昌的支持下](../Page/徐世昌.md "wikilink")，[北京成立](../Page/北京.md "wikilink")“四存學會”，研究与弘揚顏習齋的學術思想。所謂“四存”乃是以顏習齋四部書，即存性、存人、存學、存治而得名。1921年在北京府右街建立了一所“四存中學”，並在顏習齋的故里[河北省](../Page/河北省.md "wikilink")[博野縣楊村建立一所](../Page/博野縣.md "wikilink")“四存小學”，1929年由失意政客[張蔭梧任校长](../Page/張蔭梧.md "wikilink")，后改造为[四存中學](../Page/四存中學.md "wikilink")。1937年[抗日战争爆发](../Page/抗日战争.md "wikilink")，張蔭梧以该中学为核心组织河北民间抗日武装，自称河北民军，后被[八路军兼并](../Page/八路军.md "wikilink")。抗战胜利后，張蔭梧在北平经营“習齋學會”。1949年1月[共軍進入北平](../Page/共軍.md "wikilink")，认为張蔭梧有组织暴动的嫌疑，于2月15日将其秘密逮捕，5月27日病死于[清河囚犯集训队](../Page/清河.md "wikilink")。\[4\]\[5\]

## 参考文献

<div class="references-small">

<references />

</div>

  - [李恕谷](../Page/李恕谷.md "wikilink")、王源辑《颜习斋先生年谱》
  - [根據四存會本校對過的《存性編》](http://www.wfu.edu/~moran/zhexuejialu/CXBbaiwen.html)
  - 颜炳罡：[返本开新——从颜元的习行哲学看明末清初的思想特点](http://www.hongdao.net/hd_index.php?message=101000&item=hd_zxwz&key=art_id&id=1954)

[Y顏](../Category/清朝教育家.md "wikilink")
[Y顏](../Category/中国思想家.md "wikilink")
[Y顏](../Category/颜姓.md "wikilink")
[Category:東廡先賢先儒](../Category/東廡先賢先儒.md "wikilink")

1.  《顏習齋先生年譜》
2.  “習齋記餘”卷三“寄桐鄉錢生曉城書”
3.  《朱子語類評》
4.  星岛日报：《智擒張蔭梧 中共入城後京都第一案》
5.  常真：《[回忆智擒张荫梧](http://www.mps.gov.cn/n16/n1327/n4834/n1452660/1471151.html)
    》，公安部网