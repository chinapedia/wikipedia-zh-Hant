[颜杲卿_2.jpg](https://zh.wikipedia.org/wiki/File:颜杲卿_2.jpg "fig:颜杲卿_2.jpg")
[顔杲卿.jpg](https://zh.wikipedia.org/wiki/File:顔杲卿.jpg "fig:顔杲卿.jpg")
**顏杲卿**（），字**昕**，[長安](../Page/長安.md "wikilink")[萬年县人](../Page/万年县_\(北周\).md "wikilink")，祖籍[琅琊](../Page/琅琊.md "wikilink")[临沂](../Page/临沂.md "wikilink")，[唐朝官員](../Page/唐朝.md "wikilink")。天寶十五載（756年），安祿山叛軍圍攻常山，安祿山俘虜其子[顏季明](../Page/顏季明.md "wikilink")，藉此逼迫顏杲卿投降，但顏杲卿不肯屈服，還大罵安祿山，季明被斬。不久城為[史思明所破](../Page/史思明.md "wikilink")，顏杲卿被押到[洛陽](../Page/洛陽.md "wikilink")，見到安祿山，顏杲卿瞋目怒罵，也被處死。

顏杲卿死后，因[杨国忠听信](../Page/杨国忠.md "wikilink")[张通幽的谗言](../Page/张通幽.md "wikilink")，没有褒赠。[颜真卿向](../Page/颜真卿.md "wikilink")[唐肃宗哭诉此事](../Page/唐肃宗.md "wikilink")，唐肃宗转诉[唐玄宗](../Page/唐玄宗.md "wikilink")，唐玄宗杖杀张通幽。颜杲卿的儿子[颜泉明免于一死](../Page/颜泉明.md "wikilink")，在洛阳将父亲和他下属[袁履谦的尸体收敛](../Page/袁履谦.md "wikilink")\[1\]。[乾元元年](../Page/乾元_\(唐朝\).md "wikilink")（758年），[追赠](../Page/追赠.md "wikilink")[太子太保](../Page/太子太保.md "wikilink")，[谥曰忠节](../Page/谥.md "wikilink")，以其子[颜威明为](../Page/颜威明.md "wikilink")[太仆丞](../Page/太仆丞.md "wikilink")。

## 生平

五代祖為[顏之推](../Page/顏之推.md "wikilink")，[北齊](../Page/北齊.md "wikilink")[黃門侍郎](../Page/黃門侍郎.md "wikilink")、[修文館學士](../Page/修文館.md "wikilink")。齊亡入周，始家關內，遂為長安人焉。曾伯祖為[顏師古](../Page/顏師古.md "wikilink")，唐貞觀年間[秘書監](../Page/秘書監.md "wikilink")，自有傳。曾祖[顏勤禮](../Page/顏勤禮.md "wikilink")，[崇文館學士](../Page/崇文館.md "wikilink")。祖父[顏昭甫](../Page/顏昭甫.md "wikilink")，曹王侍讀。父親[顏元孫](../Page/顏元孫.md "wikilink")，[垂拱初年登進士第](../Page/垂拱.md "wikilink")，考功員外郎劉奇榜其詞策，文瑰俊拔，多士聳觀。歷任長安尉、太子舍人、濠州刺史卒。

与堂弟[顏真卿同為](../Page/顏真卿.md "wikilink")[顏師古五代孫](../Page/顏師古.md "wikilink")，以文儒世家。

顏杲卿，以[廕生受官](../Page/廕生.md "wikilink")，個性剛直。在[開元年間](../Page/開元.md "wikilink")，出為魏州錄事參軍、遂州司法參軍，振舉綱目，政績第一。[天寶](../Page/天寶.md "wikilink")14年，遷[范陽](../Page/范陽.md "wikilink")[戶曹參軍](../Page/戶曹參軍.md "wikilink")，安祿山聞其名，上表為營田判官，出為[常山太守](../Page/常山.md "wikilink")\[2\]。時安祿山為河北、河東采訪使，常山在其部內。同年十一月，安祿山舉范陽之兵詣闕。十二月十二日，攻陷東都。顏杲卿憂慮逆賊打到[潼關](../Page/潼關.md "wikilink")，恐危及宗社。此時從弟[顏真卿為平原太守](../Page/顏真卿.md "wikilink")，初聞安祿山叛變。[顏真卿乃遣使告訴顏杲卿](../Page/顏真卿.md "wikilink")，相與起義兵，掎角斷賊歸路，以紓西寇之勢。

[天寶十四載](../Page/天宝_\(唐朝\).md "wikilink")（755年），[安史之亂時](../Page/安史之亂.md "wikilink")，杲卿和兒子季明守[常山郡](../Page/常山郡.md "wikilink")（[河北省](../Page/河北省.md "wikilink")[正定縣西南](../Page/正定縣.md "wikilink")），任[太守](../Page/太守.md "wikilink")，顏真卿守[平原郡](../Page/平原郡.md "wikilink")，設計殺[安祿山部將李欽湊](../Page/安祿山.md "wikilink")，擒高邈、何千年。河北有十七郡响应。这迫使已经推进到陕虢之间（今[三门峡和](../Page/三门峡.md "wikilink")[灵宝之间](../Page/灵宝.md "wikilink")）的安禄山军队回撤，命令史思明率军渡河进攻。

天寶十五載（756年），安祿山叛軍圍攻常山，安祿山俘虜[顏季明](../Page/顏季明.md "wikilink")，藉此逼迫顏杲卿投降，但顏杲卿不肯屈服，還大罵安祿山，季明被斬。不久城為[史思明所破](../Page/史思明.md "wikilink")，顏杲卿被押到[洛陽](../Page/洛陽.md "wikilink")，見到安祿山，安祿山怒曰：「吾拔擢你太守，為何負我而造反？」顏杲卿張大眼罵曰：「汝營州牧羊羯奴耳，竊荷恩寵，天子負汝何事，而乃反乎？我世唐臣，守忠義，恨不斬汝以謝上，從從爾反耶？」安祿山不勝忿，縛之往天津橋柱，節解以肉啖之，罵聲不絕，賊鉤斷其舌，曰：「復能罵否？」杲卿含胡而絕，年六十五。

## 身後

顏杲卿死后，因[杨国忠听信](../Page/杨国忠.md "wikilink")[张通幽的谗言](../Page/张通幽.md "wikilink")，没有褒赠。颜真卿向[唐肃宗哭诉此事](../Page/唐肃宗.md "wikilink")，唐肃宗转诉[唐玄宗](../Page/唐玄宗.md "wikilink")，唐玄宗杖杀张通幽。

[李光弼](../Page/李光弼.md "wikilink")、[郭子儀收復常山](../Page/郭子儀.md "wikilink")，救出杲卿、履謙二家親屬數百人於獄，厚給遺，令行喪。

[乾元元年](../Page/乾元_\(唐朝\).md "wikilink")（758年），[追赠](../Page/追赠.md "wikilink")[太子太保](../Page/太子太保.md "wikilink")，[谥曰忠节](../Page/谥.md "wikilink")，封其妻崔清河郡夫人，以其子[颜威明为](../Page/颜威明.md "wikilink")[太仆丞](../Page/太仆丞.md "wikilink")。

颜杲卿的儿子[颜泉明免于一死](../Page/颜泉明.md "wikilink")，在洛阳将父亲和他下属[袁履谦的尸体收敛](../Page/袁履谦.md "wikilink")\[3\]。

[乾元元年五月](../Page/乾元.md "wikilink")，帝詔曰：「故衛尉卿、兼御史中丞、恆州刺史顏杲卿，任彼專城，誌梟狂虜，艱難之際，忠義在心。憤群兇而慷慨，臨大節而奮發，遂擒元惡，成此茂勛。屬胡虜憑陵，流毒方熾，孤城力屈，見陷寇仇，身歿名存，實彰忠烈。夫仁者有勇，驗之於臨難；臣之報國，義存於捐軀。嘉其死節之誠，未備飾終之禮，可贈太子太保。」\[4\]

## 其他

1\.[文天祥的](../Page/文天祥.md "wikilink")《[正氣歌](../Page/正氣歌.md "wikilink")》中的「為顏常山舌」指的就是顏杲卿被俘虜後怒罵[安祿山之事](../Page/安祿山.md "wikilink")。

2\.[唐肅宗](../Page/唐肅宗.md "wikilink")[乾元元年](../Page/乾元.md "wikilink")（758年），[顏真卿](../Page/顏真卿.md "wikilink")（顏杲卿弟）命人至河北尋得顏杲卿子[顏季明頭顱後](../Page/顏季明.md "wikilink")，揮淚寫下《[祭姪文稿](../Page/祭姪文稿.md "wikilink")》。真跡現藏於[臺北國立故宮博物院](../Page/臺北國立故宮博物院.md "wikilink")，被譽為「天下三大行書字帖」之一。

## 注释

## 参考文献

  -
[Category:唐朝户曹参军](../Category/唐朝户曹参军.md "wikilink")
[Category:唐朝太守](../Category/唐朝太守.md "wikilink")
[Category:唐朝軍事人物](../Category/唐朝軍事人物.md "wikilink")
[Category:唐朝被殺害人物](../Category/唐朝被殺害人物.md "wikilink")
[Category:唐朝追赠太子太保](../Category/唐朝追赠太子太保.md "wikilink")
[G](../Category/琅邪颜氏.md "wikilink") [G](../Category/颜姓.md "wikilink")
[Category:諡忠節](../Category/諡忠節.md "wikilink")

1.  《[资治通鉴](../Page/资治通鉴.md "wikilink")·卷第二百二十·唐纪三十六》乾元元年......五月......乙未......赠故常山太守颜杲卿太子太保，谥曰忠节，以其子威明为太仆丞。杲卿之死也......与杲卿无异，乃始惭服......
2.  《[新唐書](../Page/新唐書.md "wikilink")》卷第一百九十二，忠義中「顔杲卿」
3.  《[资治通鉴](../Page/资治通鉴.md "wikilink")·卷第二百二十·唐纪三十六》乾元元年......五月......乙未......赠故常山太守颜杲卿太子太保，谥曰忠节，以其子威明为太仆丞。杲卿之死也......与杲卿无异，乃始惭服......
4.  《[旧唐書](../Page/旧唐書.md "wikilink")》卷第一百八十七下