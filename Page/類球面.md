<table>
<tbody>
<tr class="odd">
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:OblateSpheroid.PNG" title="fig:OblateSpheroid.PNG">OblateSpheroid.PNG</a></p>
</center></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:ProlateSpheroid.png" title="fig:ProlateSpheroid.png">ProlateSpheroid.png</a></p>
</center></td>
</tr>
<tr class="even">
<td><p><em>扁球面</em></p></td>
<td><p><em>長球面</em></p></td>
</tr>
</tbody>
</table>

**類球面**是一種[二次曲面](../Page/二次曲面.md "wikilink")。二維的[橢圓有兩個主軸](../Page/橢圓.md "wikilink")，稱為**長軸**與**短軸**。在三維空間裏，將一個橢圓繞著其任何一主軸旋轉，則可得到一個類球面。

  - 假若，這旋轉主軸是長軸，則這個類球面為長球面。例如，[英式足球裏所用的](../Page/英式足球.md "wikilink")[橄欖球是長球形狀](../Page/橄欖球.md "wikilink")。
  - 假若，這旋轉主軸是短軸，則這個類球面為扁球面。例如，[地球在北極與南極稍微有點扁平](../Page/地球.md "wikilink")，在赤道又有點凸漲。所以，地球是扁球形狀。
  - 假若，生成的橢圓是[圓圈](../Page/圓圈.md "wikilink")，則這個類球面為完全對稱的[圓球面](../Page/球面.md "wikilink")。

用另外一種方法來描述，類球面是一種[橢球面](../Page/橢球面.md "wikilink")。採用直角坐標\((x,\ y,\ z)\,\!\)，橢球面可以表達為

\[{x^2 \over a^2}+{y^2 \over b^2}+{z^2 \over c^2}=1\]；

其中，\(a\,\!\)與\(b\,\!\)分別是橢球面在x-軸與y-軸的**赤道半徑**，\(c\,\!\)是橢球面在z-軸的**極半徑**．這三個正值實數的半徑決定了橢球面的形狀。

  - 假若，三個半徑都相等，則這橢球面是[圓球面](../Page/球面.md "wikilink")：

\[a=b=c\,\!\]。

  - 假若，兩個赤道半徑相等，則這橢球面是類球面：

\[a=b\,\!\]。

  - 假若，類球面的赤道半徑小於極半徑，則這是類球面是長球面：

\[a=b<c\,\!\]。

  - 假若，類球面的赤道半徑大於極半徑，則這是類球面是扁球面：

\[a=b>c\,\!\]。

## 面積

一個長球面的面積是

\[2\pi\left(a^2+\frac{ac\ o\!\varepsilon}{\sin(o\!\varepsilon)}\right)\,\!\]；

其中，\(o\!\varepsilon=\arccos\left(\frac{a}{c}\right)\,\!\)，\(o\!\varepsilon\,\!\)（
唸為ethyl）是橢圓的[角離心率](../Page/角離心率.md "wikilink")（）。橢圓的[離心率](../Page/離心率.md "wikilink")\(e\,\!\)等於\(\sin(o\!\varepsilon)\,\!\)。

一個扁球面的面積是

\[2\pi\left[a^2+\frac{c^2}{\sin(o\!\varepsilon)} \ln\left(\frac{1+ \sin(o\!\varepsilon)}{\cos(o\!\varepsilon)}\right)\right]\,\!\]；

其中，\(o\!\varepsilon=\arccos\left(\frac{c}{a}\right)\,\!\)。

## 體積

類球的體積是\(\frac{4}{3}\pi a^2 c\,\!\)。

## 曲率

假若，一個類球面被參數化為

\[\boldsymbol{\sigma}(\beta,\ \lambda) = (a \cos \beta \cos \lambda,\ a \cos \beta \sin \lambda,\ b \sin \beta)\,\!\]
;

其中，\(\beta\,\!\)是[參數緯度](../Page/參數緯度.md "wikilink")（），\(- \frac{\pi}{2}<\beta<\frac{\pi}{2}\,\!\)，\(\lambda\,\!\)是[經度](../Page/經度.md "wikilink")，\(- \pi<\lambda<+\pi\,\!\)。

那麼，類球面的[高斯曲率](../Page/高斯曲率.md "wikilink")（）是

\[K(\beta,\lambda) = {b^2 \over (a^2 + (b^2 - a^2) \cos^2 \beta)^2}\,\!\]。

類球面的[平均曲率](../Page/平均曲率.md "wikilink")（）是

\[H(\beta,\lambda) = {b (2 a^2 + (b^2 - a^2) \cos^2 \beta) \over 2 a (a^2 + (b^2 - a^2) \cos^2 \beta)^{3/2}}\,\!\]。

對於類球面，這兩種曲率永遠是正值的。所以，類球面的每一點都是橢圓的。

## 參閱

  - [皮埃爾·莫佩爾蒂](../Page/皮埃爾·莫佩爾蒂.md "wikilink")
  - [橢球體](../Page/橢球體.md "wikilink")
  - [卵形體](../Page/卵形體.md "wikilink")（）
  - [長球面坐標系](../Page/長球面坐標系.md "wikilink")
  - [扁球面坐標系](../Page/扁球面坐標系.md "wikilink")

[L](../Category/曲面.md "wikilink") [L](../Category/二次曲面.md "wikilink")