**顧璘**（），字華玉，號東橋，[直隸](../Page/南直隶.md "wikilink")[应天府](../Page/应天府_\(明朝\).md "wikilink")[上元县](../Page/上元县.md "wikilink")（在今[江苏省](../Page/江苏省.md "wikilink")[南京市](../Page/南京市.md "wikilink")）人。明朝政治人物，官至工部尚書。南京刑部尚書。

## 生平

生於[明憲宗](../Page/明憲宗.md "wikilink")[成化十二年](../Page/成化.md "wikilink")（1476年），祖籍[蘇州](../Page/蘇州.md "wikilink")[吳縣](../Page/吳縣.md "wikilink")，高祖[顧通在](../Page/顧通.md "wikilink")[洪武年間由](../Page/洪武.md "wikilink")[蘇州遷上元](../Page/蘇州.md "wikilink")。少有才名，與同-{里}-[陳沂](../Page/陳沂.md "wikilink")、[王韋並稱](../Page/王韋.md "wikilink")“金陵三俊”，又與寶應的[朱應登稱為四大家](../Page/朱應登.md "wikilink")\[1\]，亦是[十才子之一](../Page/十才子.md "wikilink")。弘治九年（1496年）進士，授[廣平縣知縣](../Page/廣平縣.md "wikilink")，正德四年（1509年）知[開封府](../Page/開封府.md "wikilink")，與鎮守太監廖堂、王宏忤不和，被逮下[錦衣獄](../Page/錦衣獄.md "wikilink")，謫[全州知州](../Page/全州.md "wikilink")。秩滿，遷[台州府知府](../Page/台州府.md "wikilink")。遷吏部右侍郎，改工部，[顯陵竣工](../Page/顯陵.md "wikilink")，進[工部尚書](../Page/工部尚書.md "wikilink")，改南京[刑部尚書](../Page/刑部尚書.md "wikilink")。晚年致仕歸-{里}-，築息园，大治亭舍，好賓客，座無虛席\[2\]。世稱東橋先生。[钱谦益称之](../Page/钱谦益.md "wikilink")：“处承平全盛之世，享园林钟鼓之乐，江左风流，迄今犹称为领袖也。”卒於嘉靖二十四年（1545年）。\[3\]

## 著作

著有《顧華玉集》、《浮湘集》、《息園詩文稿》、《國寶新編》、《憑幾集》、《緩慟集》、《近言》等书。

## 註釋

## 參考文獻

  - （清）[張廷玉等](../Page/張廷玉.md "wikilink")，《明史》
  - [钱谦益](../Page/钱谦益.md "wikilink")：《列朝诗集小传》丙集《顾尚书璘》
  - [文徵明](../Page/文徵明.md "wikilink")：《故资善大夫南京刑部尚书顾公墓志铭》，《甫田集》卷三十二

## 外部連結

  - [南京更正明代顾璘墓
    专家建议普查论证后重立碑](http://news.sohu.com/20110404/n280128741.shtml)

{{-}}

[Category:弘治八年乙卯科舉人](../Category/弘治八年乙卯科舉人.md "wikilink")
[Category:明朝廣平縣知縣](../Category/明朝廣平縣知縣.md "wikilink")
[Category:南京吏部主事](../Category/南京吏部主事.md "wikilink")
[Category:明朝開封府知府](../Category/明朝開封府知府.md "wikilink")
[Category:明朝台州府知府](../Category/明朝台州府知府.md "wikilink")
[Category:明朝浙江布政使司參政](../Category/明朝浙江布政使司參政.md "wikilink")
[Category:明朝山西按察使](../Category/明朝山西按察使.md "wikilink")
[Category:明朝江西按察使](../Category/明朝江西按察使.md "wikilink")
[Category:明朝浙江布政使](../Category/明朝浙江布政使.md "wikilink")
[Category:明朝山西巡撫](../Category/明朝山西巡撫.md "wikilink")
[Category:明朝湖廣巡撫](../Category/明朝湖廣巡撫.md "wikilink")
[Category:明朝吏部侍郎](../Category/明朝吏部侍郎.md "wikilink")
[Category:明朝工部尚書](../Category/明朝工部尚書.md "wikilink")
[Category:南京刑部尚書](../Category/南京刑部尚書.md "wikilink")
[Category:明朝詩人](../Category/明朝詩人.md "wikilink")
[Category:南京人](../Category/南京人.md "wikilink")
[L](../Category/顧姓.md "wikilink")

1.  《明史·卷二百八十六》：初，（顧）璘與同里陳沂、王韋，號「金陵三俊」。其後寶應朱應登繼起，稱四大家。璘詩，矩矱唐人，以風調勝。韋婉麗多致，頗失纖弱。沂與韋同調。應登才思泉涌，落筆千言。然璘、應登羽翼李夢陽，而韋、沂則頗持異論。三人者，仕宦皆不及璘。
2.  《四友齋叢說·卷十五·史十一》：顧東橋文譽籍甚，又處都會之地，都下後進皆來請業，與四方之慕從而至者，戶外之屨常滿。先生喜設客，每四五日即一張燕，余時時在其座。先生每燕必用樂，乃教坊樂工也。以箏琶佐觴，有小樂工名楊彬者，頗俊雅，先生甚喜之，常詫客曰：「蔣南泠詩所謂消得楊郎一曲歌者，正此子也。」先生每發一談，則樂聲中闋。談竟，樂復作。議論英發，音吐如鐘。每一發端，聽者傾座，真可謂一代之偉人。
3.  《明史·[卷二百八十六](../Page/s:明史/卷286.md "wikilink")》：顧璘，字華玉，上元人。弘治九年進士。授廣平知縣，擢南京吏部主事，晉郎中。正德四年出為開封知府，數與鎮守太監廖堂、王宏忤，逮下錦衣獄，謫全州知州。秩滿，遷台州知府。歷浙江左布政使，山西、湖廣巡撫，右副都御史，所至有聲。遷吏部右侍郎，改工部。董顯陵工畢，遷南京刑部尚書。罷歸，年七十余卒。