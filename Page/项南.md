**项南**（），原名项道成，[福建省](../Page/福建省.md "wikilink")[龙岩市](../Page/龙岩市.md "wikilink")[连城县朋口乡](../Page/连城县.md "wikilink")（现为[朋口镇](../Page/朋口镇.md "wikilink")）文地村人\[1\]。项南父亲[项与年是闽西地区的一个共产党员](../Page/项与年.md "wikilink")。

## 简历

1929年，项南参加[少年先锋队](../Page/少年先锋队.md "wikilink")，任[连城文坊](../Page/连城.md "wikilink")[苏区](../Page/苏区.md "wikilink")[少年先锋队队长](../Page/少年先锋队.md "wikilink")。1930年，随[地下工作的父母在](../Page/地下工作.md "wikilink")[南京和](../Page/南京.md "wikilink")[上海半工半读](../Page/上海.md "wikilink")。

1937年，[抗战爆发后](../Page/抗日战争.md "wikilink")，在[福建](../Page/福建.md "wikilink")[长乐县组织宣传抗日的](../Page/长乐县.md "wikilink")“明天剧咏团”。1938年，项南加入[中国共产党](../Page/中国共产党.md "wikilink")，介绍人为福建省委宣传部部长[王助](../Page/王助.md "wikilink")。1938年10月，他在[顺昌县发起组织](../Page/顺昌县.md "wikilink")“顺昌抗敌剧团”并任团长。1939年11月，任[闽清县政府战时民教工作队队长](../Page/闽清县.md "wikilink")。1939年10月，先后任[广西](../Page/广西.md "wikilink")[桂林苗圃主任](../Page/桂林.md "wikilink")、青年科学技术人员协进会理事、成达师范教员。1941年6月，到达苏北[盐城](../Page/盐城.md "wikilink")[新四军军部](../Page/新四军.md "wikilink")，由中共华中局分配工作。1941年，在[新四军重新加入](../Page/新四军.md "wikilink")[中国共产党](../Page/中国共产党.md "wikilink")。1942-1945年，先后任[阜东县政府秘书](../Page/阜东县.md "wikilink")、阜东九区区委书记、阜东县委宣传部部长、苏北第五分区专员公署建设处处长等职。

1951年，项南任青年团安徽省委书记、[安徽大学](../Page/安徽师范大学.md "wikilink")（现[安徽师范大学](../Page/安徽师范大学.md "wikilink")）党委书记。1952年，任青年团华东工委第二副书记。1954年，当选为第一届[全国人大代表](../Page/全国人大.md "wikilink")。1957年，任[团中央书记处书记](../Page/团中央.md "wikilink")。1961年，调任八机部办公厅副主任，后任农机局局长。1970年5月，恢复工作后，历任一机部农机局局长，一机部党的核心小组成员、副部长。1979年2月，任农机部副部长、党组副书记，当选为第五届全国人大代表。

1980年，调[福建省工作](../Page/福建省.md "wikilink")，历任[中共福建省委常务书记](../Page/中共福建省委.md "wikilink")、福建省五届人大常委会主任、中共福建省委第一书记兼省军区第一政委、中共福建省委书记，并当选为第六届全国人大代表和第十二届中央委员。

1985年，因“晋江假药案”承受巨大压力，从福建省委书记位置上下台，政论作家高新认为这是[陈云](../Page/陈云.md "wikilink")、[胡乔木等保守派对改革派代表人物的一次借口打击](../Page/胡乔木.md "wikilink")\[2\]\[3\]。

1989年后，担任[中国扶贫基金会会长](../Page/中国扶贫基金会.md "wikilink")、[中华职业教育社副理事长](../Page/中华职业教育社.md "wikilink")、[中国老区建设促进会和](../Page/中国老区建设促进会.md "wikilink")[中华炎黄文化研究会顾问](../Page/中华炎黄文化研究会.md "wikilink")、[中国扶贫基金会首席顾问等职](../Page/中国扶贫基金会.md "wikilink")。1992年，被选为[中共十四大代表](../Page/中共十四大.md "wikilink")。1997年列席[中共十五大](../Page/中共十五大.md "wikilink")。

1997年11月10日因[心脏病突发](../Page/心脏病.md "wikilink")，在[北京](../Page/北京.md "wikilink")[逝世](../Page/逝世.md "wikilink")。

项南是中国共产党的第九届、十届、十一届、十二届中央委员会委员，中共中央顾问委员会委员。

其妻[汪志馨](../Page/汪志馨.md "wikilink")（1916年-2016年12月3日）曾任福建省人大常委会教科文卫委主任\[4\]。

## 注释

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[X](../Category/连城人.md "wikilink")
[X](../Category/中華人民共和國政府官員.md "wikilink")
[Category:中共福建省委书记](../Category/中共福建省委书记.md "wikilink")
[Category:中国共产党第十二届中央委员会委员](../Category/中国共产党第十二届中央委员会委员.md "wikilink")
[Category:第六届全国人大代表](../Category/第六届全国人大代表.md "wikilink")
[N](../Category/項姓.md "wikilink")
[Category:福建省人大常委会主任](../Category/福建省人大常委会主任.md "wikilink")
[Category:中共十四大代表](../Category/中共十四大代表.md "wikilink")
[Category:中共中央顾问委员会委员](../Category/中共中央顾问委员会委员.md "wikilink")

1.
2.
3.
4.