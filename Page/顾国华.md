**顾国华**，1927年-2005年，[香港航运业企业家](../Page/香港.md "wikilink")，曾任泰昌祥轮船（[香港](../Page/香港.md "wikilink")）集团[主席](../Page/主席.md "wikilink")。

## 简介

顾国华是航运业实业家[顾宗瑞的次子](../Page/顾宗瑞.md "wikilink")。[浙江省](../Page/浙江省.md "wikilink")[宁波](../Page/宁波.md "wikilink")[镇海县](../Page/镇海.md "wikilink")（今[北仑区](../Page/北仑区.md "wikilink")）人。顾国华早年在[上海求学](../Page/上海.md "wikilink")，后到[美国留学](../Page/美国.md "wikilink")，毕业于[美国](../Page/美国.md "wikilink")[宾夕法尼亚大学航海系](../Page/宾夕法尼亚大学.md "wikilink")。20世纪40年代末因战乱，[顾宗瑞将家庭和企业迁往](../Page/顾宗瑞.md "wikilink")[香港](../Page/香港.md "wikilink")，顾国华遂随家人来到[香港发展](../Page/香港.md "wikilink")。

在[香港早期](../Page/香港.md "wikilink")，顾国华主要协助[父亲](../Page/父亲.md "wikilink")[顾宗瑞拓展事业](../Page/顾宗瑞.md "wikilink")。顾宗瑞先生逝世后，顾国华接替父亲出任泰昌祥轮船（[香港](../Page/香港.md "wikilink")）集团[主席](../Page/主席.md "wikilink")。顾国华还担任有香港浙江同乡会联合会副会长，宁波旅港同乡会会长，香港甬港联谊会名誉会长和[中华人民共和国](../Page/中华人民共和国.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[政协委员](../Page/政协.md "wikilink")。

顾国华致力慈善事业，多次出资大力支持大陆及家乡的[教育](../Page/教育.md "wikilink")、医疗事业。2003年1月，顾国华当选香港浙江同乡会联合会的会长。2000年，当选上海宁波同乡联谊会的名誉理事。1993年7月，[宁波市](../Page/宁波市.md "wikilink")[人大常委会授予顾国华宁波市荣誉市民称号](../Page/人大.md "wikilink")。1994年10月，[浙江省人民政府授予顾国华](../Page/浙江省.md "wikilink")“爱乡楷模”的荣衔。

2005年1月，顾国华在[香港因病去世](../Page/香港.md "wikilink")。由兒子[顧建綱接任泰昌祥轮船](../Page/顧建綱.md "wikilink")（[香港](../Page/香港.md "wikilink")）集团[主席](../Page/主席.md "wikilink")

## 参考

  - [泰昌祥轮船（香港）集团主席顾国华](http://www.tt91.com/nb/news_detail.asp?id=14017)
  - [大碶头顾氏：宁波走出来的航海世家--浙江在线-人文频道](http://culture.zjol.com.cn/05culture/system/2005/12/27/006419743.shtml)
  - [顾国华（1927\~2005）](http://www.dq.gov.cn/show.asp?nid=1635)

[G](../Category/1927年出生.md "wikilink")
[G](../Category/2005年逝世.md "wikilink")
[G](../Category/宁波商帮.md "wikilink")
[G](../Category/賓夕法尼亞大學校友.md "wikilink")
[G](../Category/香港商人.md "wikilink") [G](../Category/宁波人.md "wikilink")
[G](../Category/北仑区人.md "wikilink") [G](../Category/宁波船帮.md "wikilink")
[G](../Category/香港寧波人.md "wikilink") [Z](../Category/顾姓.md "wikilink")
[国华](../Category/大碶头顾氏.md "wikilink")