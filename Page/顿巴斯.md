[Map_of_Ukraine_political_simple_Donbass.png](https://zh.wikipedia.org/wiki/File:Map_of_Ukraine_political_simple_Donbass.png "fig:Map_of_Ukraine_political_simple_Donbass.png")
[Map_of_the_Donbass.png](https://zh.wikipedia.org/wiki/File:Map_of_the_Donbass.png "fig:Map_of_the_Donbass.png")
[Donbass_economic.jpg](https://zh.wikipedia.org/wiki/File:Donbass_economic.jpg "fig:Donbass_economic.jpg")
**顿巴斯**（[乌克兰文](../Page/乌克兰文.md "wikilink")：、[俄语](../Page/俄语.md "wikilink")：）是**[顿涅茨克盆地](../Page/顿涅茨克.md "wikilink")**（[乌克兰文](../Page/乌克兰文.md "wikilink")、[俄语](../Page/俄语.md "wikilink")）的简称。这里是今天[乌克兰东部的一个有其自己的历史](../Page/乌克兰.md "wikilink")、经济、文化特点的地区。

顿巴斯地区跨越乌克兰的两个州：[顿涅茨克州的中部](../Page/顿涅茨克州.md "wikilink")、北部和[盧甘斯克州南部](../Page/盧甘斯克州.md "wikilink")。最大城市[顿涅茨克通常被认为是顿巴斯的非正式首府](../Page/顿涅茨克.md "wikilink")。这个地区的名称来自于19世纪在此处[顿涅茨河流域发现的一个大](../Page/顿涅茨河.md "wikilink")[煤矿](../Page/煤矿.md "wikilink")。

1676年这里有了最早的城镇，1721年这里发现大量煤矿，成就了它直到20世纪中期的繁荣。

顿巴斯地区有时也包括邻近的位于[俄罗斯的](../Page/俄罗斯.md "wikilink")[罗斯托夫州](../Page/罗斯托夫州.md "wikilink")。那里有时也被称为“俄罗斯的顿巴斯”，但其经济、社会影响力要比乌克兰部分的小得多。

[苏联国内战争期间](../Page/苏联国内战争.md "wikilink")，这里是[红军和](../Page/红军.md "wikilink")[白军争夺的主要战场之一](../Page/白军.md "wikilink")。[苏德战争期间](../Page/苏德战争.md "wikilink")，这里发生了著名的。

[苏联解体后](../Page/苏联解体.md "wikilink")，这里是乌克兰的领土。2014年2月21日，乌克兰亲俄总统[維克多·費奧多羅維奇·亞努科維奇被](../Page/維克多·費奧多羅維奇·亞努科維奇.md "wikilink")[亲欧盟示威者驱逐下台](../Page/烏克蘭親歐盟示威運動.md "wikilink")，离开首都[基辅之后](../Page/基辅.md "wikilink")，位于顿巴斯的两个州的分离主义者在未经乌克兰政府批准的情况下，自行组织[全民公投](../Page/全民公投.md "wikilink")，分别成立[頓涅茨克人民共和國和](../Page/頓涅茨克人民共和國.md "wikilink")[盧甘斯克人民共和國](../Page/盧甘斯克人民共和國.md "wikilink")，并于2014年5月24日聯盟而成[新俄羅斯聯邦](../Page/新俄羅斯聯邦.md "wikilink")，目前仍在继续的[顿巴斯战争开始](../Page/顿巴斯战争.md "wikilink")。

## 参见

  - [庫茲巴斯](../Page/庫茲巴斯.md "wikilink")

[D顿](../Category/烏克蘭地理.md "wikilink") [D顿](../Category/盆地.md "wikilink")