**预序关系**（简称**预序**，又称**先序**，**preorder**）、在[数学中](../Page/数学.md "wikilink")，是一类接近于[偏序关系的二元关系](../Page/偏序关系.md "wikilink")，但仅满足[自反性和](../Page/自反性.md "wikilink")[传递性而不满足](../Page/传递性.md "wikilink")[反对称性](../Page/反对称性.md "wikilink")。偏序的大多数理论均可扩展到预序。

## 定义

考虑[集合](../Page/集合.md "wikilink") *P*
及其上的[二元关系](../Page/二元关系.md "wikilink")
\(\lesssim\)。若 \(\lesssim\)
具有[自反性和](../Page/自反性.md "wikilink")[传递性](../Page/传递性.md "wikilink")，则称
\(\lesssim\) 为**预序**。具体来说，对任意 *P* 的元素 *a*，*b* 和 *c*，下列性质成立：

  -
    *a* \(\lesssim\) *a* (自反性)
    若 *a* \(\lesssim\) *b* 且 *b* \(\lesssim\) *c*，则 *a* \(\lesssim\) *c*
    (传递性)

带预序的集合称为**预序集合**。同时满足[反对称性](../Page/反对称性.md "wikilink")（若 *a*
\(\lesssim\) *b* 且 *b* \(\lesssim\) *a*，则 *a* =
*b*）的预序为[偏序](../Page/偏序.md "wikilink")。

## 说明

作为特例，[空集上的](../Page/空集.md "wikilink")[空关系为一预序](../Page/空关系.md "wikilink")。[空集加上空关系构成一预序集](../Page/空集.md "wikilink")。

## 导出偏序

将预序集的等价元素等同起来，可得到由该预序集所导出的偏序集。具体过程如下：定义预序集 *X* 上的等价关系 \(\sim \,\)，使得 *a*
\(\sim \,\) *b* [当且仅当](../Page/当且仅当.md "wikilink") *a* \(\lesssim\) *b*
且 *b* \(\lesssim\) *a*。定义所得[商集](../Page/商集.md "wikilink")
\(X / \mathrm{\sim}\)（所有 \(\sim \,\)
的[等价类构成的集合](../Page/等价类.md "wikilink")）上的序关系
\(\le\) ，使得\[*x*\] \(\le\) \[*y*\] 当且仅当 *x* \(\lesssim\) *y*。由
\(\sim \,\) 的构造可知，\(\le\)
的定义与所选等价类的代表元素无关，故上述定义[明确](../Page/明确定义.md "wikilink")。易证该关系为一偏序。

## 举例

  - [拓扑中](../Page/拓扑.md "wikilink")[网络收敛的定义使用预序比使用偏序可避免重要特征的丢失](../Page/网络.md "wikilink")。
  - The embedding relation for countable total orderings.
  - [图论中的graph](../Page/图论.md "wikilink")-minor关系。
  - Preference, according to common models.

## 参见

  - [二元关系](../Page/二元关系.md "wikilink")
  - [偏序关系](../Page/偏序关系.md "wikilink")
  - [全序关系](../Page/全序关系.md "wikilink")
  - [等价关系](../Page/等价关系.md "wikilink")
  - [有向集合](../Page/有向集合.md "wikilink")
  - [预序范畴](../Page/预序范畴.md "wikilink")
  - [预良序](../Page/预良序.md "wikilink")

## 參考文獻

  -
[Y](../Category/序理论.md "wikilink")
[Category:数学关系](../Category/数学关系.md "wikilink")