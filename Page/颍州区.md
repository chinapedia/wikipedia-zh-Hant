**颍州区**是[中國](../Page/中國.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[阜阳市下辖的一个区](../Page/阜阳市.md "wikilink")，于1996年成立。区人民政府驻颍南大道。

## 行政区划

辖4街道，8镇，1乡。

  - 街道办事处：[鼓楼街道](../Page/鼓楼街道_\(阜阳市\).md "wikilink")、[颍西街道](../Page/颍西街道.md "wikilink")、[清河街道](../Page/清河街道_\(阜阳市\).md "wikilink")、[文峰街道](../Page/文峰街道_\(阜阳市\).md "wikilink")
  - 镇：[程集镇](../Page/程集镇_\(阜阳市\).md "wikilink")、[王店镇](../Page/王店镇_\(阜阳市\).md "wikilink")、[西湖镇](../Page/西湖镇_\(阜阳市\).md "wikilink")、[九龙镇](../Page/九龙镇_\(阜阳市\).md "wikilink")、[三合镇](../Page/三合镇_\(阜阳市\).md "wikilink")、[三十里铺镇](../Page/三十里铺镇_\(阜阳市\).md "wikilink")、[袁集镇](../Page/袁集镇.md "wikilink")、[三塔集镇](../Page/三塔集镇.md "wikilink")。
  - 乡：[马寨乡](../Page/马寨乡.md "wikilink")。

[阜阳](../Page/category:安徽市辖区.md "wikilink")

[颍州区](../Category/颍州区.md "wikilink")
[区](../Category/阜阳区县市.md "wikilink")