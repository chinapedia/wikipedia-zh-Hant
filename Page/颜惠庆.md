[Dr.W.W.Yan.jpg](https://zh.wikipedia.org/wiki/File:Dr.W.W.Yan.jpg "fig:Dr.W.W.Yan.jpg")

**颜惠庆**（英文名：****或****，），[字](../Page/表字.md "wikilink")**骏人**，[上海](../Page/上海.md "wikilink")[虹口人](../Page/虹口.md "wikilink")，[中華民国大陆时期](../Page/中華民国.md "wikilink")[政治家](../Page/政治家.md "wikilink")、[外交家](../Page/外交家.md "wikilink")、[作家政治家](../Page/作家.md "wikilink")。

## 生平

### 清末

[Yan_Huiqing_Youth.jpg](https://zh.wikipedia.org/wiki/File:Yan_Huiqing_Youth.jpg "fig:Yan_Huiqing_Youth.jpg")

颜惠庆祖上来自[山东](../Page/山东.md "wikilink")，后南渡[福建](../Page/福建.md "wikilink")。他出身于一个[基督教家庭](../Page/基督教.md "wikilink")，其父[颜永京早年从福建来到上海并且担任](../Page/颜永京.md "wikilink")[虹口](../Page/虹口.md "wikilink")[圣公会教区](../Page/圣公会.md "wikilink")[牧师](../Page/牧师.md "wikilink")，还是[圣约翰大学创办者之一](../Page/圣约翰大学_\(上海\).md "wikilink")。颜早年就读于[聖约翰書院](../Page/聖约翰書院.md "wikilink")、[英華書塾](../Page/英華書塾.md "wikilink")、[同文書院](../Page/同文書院.md "wikilink")，1895年赴[美国读高中](../Page/美国.md "wikilink")，1897年进入[弗吉尼亚大学学习](../Page/弗吉尼亚大学.md "wikilink")，在校期間曾任教會學校教師，曾參與中國駐美國公使館舘務。

1900年6月，颜惠庆毕业，8月回国，此后任圣约翰大学[英文教授](../Page/英文.md "wikilink")。1905年（光緒31年）任上海《南方報》英文版編輯、[商务印书馆编辑](../Page/商务印书馆.md "wikilink")，在此期间编撰了《英华大辞典》。1906年（光緒32年）9月，参加[清朝政府举行的留学生考试](../Page/清朝.md "wikilink")，获得全国第二名和[进士出身](../Page/进士.md "wikilink")。

1907年（光緒33年）冬，颜惠庆任中国駐美國公使館二等参贊，隨[公使](../Page/特命全権公使.md "wikilink")[伍廷芳赴任](../Page/伍廷芳.md "wikilink")。驻美期间，颜惠庆在[乔治·华盛顿大学系统进修了外交理论和](../Page/乔治·华盛顿大学.md "wikilink")[国际法知识](../Page/国际法.md "wikilink")，並加入美國国際法学会。

1910年（[宣統](../Page/宣統.md "wikilink")2年），颜惠庆歸国，任外務部主事、外務部参議。[辛亥革命爆發後](../Page/辛亥革命.md "wikilink")，代表[袁世凱訪問各国駐華公使](../Page/袁世凱.md "wikilink")，獲得各國支持，以此取得了袁的信任，被提拔為外務部左丞。
[顏惠慶公使夫人即孫寶琦總長之女弟.jpg](https://zh.wikipedia.org/wiki/File:顏惠慶公使夫人即孫寶琦總長之女弟.jpg "fig:顏惠慶公使夫人即孫寶琦總長之女弟.jpg")之女弟\]\]

### 民国初期

[民国元年](../Page/民国.md "wikilink")（1912年），颜惠庆出任[唐绍仪内阁的外交部次长](../Page/唐绍仪.md "wikilink")，1913年春，颜惠庆任驻[德国](../Page/中国驻德国大使列表.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[丹麦三国](../Page/丹麦.md "wikilink")[公使](../Page/公使.md "wikilink")。1919年，以顾问身份参加中国代表团，出席[巴黎和会](../Page/巴黎和会.md "wikilink")。1920年回国。1920年（民国9年）8月，颜惠庆奉大總統[徐世昌之命出任外交总长直到](../Page/徐世昌.md "wikilink")1922年（民国11年）8月，任内基本採取親英美路綫，重視對日本的交涉，尤其是有關山東問題的交涉。1921年（民国10年）5月，恢復和德國的邦交。11月[華盛頓会議](../Page/華盛頓会議_\(1922年\).md "wikilink")，颜惠庆派出施肇基、顾维钧、王宠惠三人为全权代表。1921年12月18日代理[国务总理](../Page/国务总理.md "wikilink")，1922年6月12日正式任国务总理。

1922年1月至1926年（民国15年）6月，[北京政府内各派紛争](../Page/北京政府.md "wikilink")，顔惠慶曾任農商總長、外交總長、内務總長。他除1922年曾正式組閣外，1924年再次组阁。1926年5月臨時執政段祺瑞失勢，颜惠庆曾代行臨時執政職，1個月後卸任。此後，颜惠庆隐居[天津英租界](../Page/天津英租界.md "wikilink")，專心從事金融和慈善活動。1927年，[私立南开大学董事长范源濂去世后](../Page/南开大学.md "wikilink")，颜惠庆作为南开大学董事会董事继任董事长一职\[1\]。
[缩略图](https://zh.wikipedia.org/wiki/File:中國紅十字會歷史照片040.png "fig:缩略图")

### 国民政府时期

1931年[九一八事变后](../Page/九一八事变.md "wikilink")，颜惠庆以外交元老身份应[南京国民政府外交部長](../Page/南京国民政府.md "wikilink")[王正廷征召](../Page/王正廷.md "wikilink")，任對日特種委員会委員。11月，任駐美國[公使](../Page/公使.md "wikilink")，到華盛頓赴任。1932年1月，被任命为中国驻[國際聯盟代表团首席代表](../Page/國際聯盟.md "wikilink")。1932年，颜惠庆代表中国在国联大会上提交了日本侵略中国案，促请国联大会和行政院制裁日本。同年，作为中国政府首席代表出席[日内瓦](../Page/日内瓦.md "wikilink")[国际裁军会议](../Page/国际裁军会议.md "wikilink")，其间同[苏联展开秘密外交](../Page/苏联.md "wikilink")，于当年12月成功地与苏联签订建交协议。同年，颜惠庆任南开大学校董会董事。1933年（民国22年）2月日本[松岡洋右在國聯發表退出国聯宣言](../Page/松岡洋右.md "wikilink")，顔恵慶針鋒相對地發表了反對演説。1933年1月31日，颜惠庆被任命为[中国驻苏联大使](../Page/中国驻苏联大使.md "wikilink")，1936年因健康原因辞职回国。

1937年[抗日战争爆发后](../Page/抗日战争_\(中国\).md "wikilink")，颜惠庆任[中国紅十字会国際委員会主席](../Page/中國紅十字會_\(中華民國\).md "wikilink")，在上海主持难民和伤兵的救治工作。1939年，以国民政府特使身份赴美国两次拜见美国总统[富兰克林·罗斯福](../Page/富兰克林·罗斯福.md "wikilink")，为中国争取到了大量美国援助。1940年12月，自[旧金山返](../Page/旧金山.md "wikilink")[香港](../Page/香港.md "wikilink")，滞留于此。[太平洋战争爆发后](../Page/太平洋战争.md "wikilink")，[日本占领香港](../Page/日本.md "wikilink")，颜惠庆被软禁，其間拒絕了日方要其出任親日政府職務的要求，后被逮捕押回上海关押。

1945年抗战胜利后，颜惠庆在上海任上海市参議員、[聯合国遠東救濟與復興委員會主席](../Page/聯合国遠東救濟與復興委員會.md "wikilink")，還當选为[第1屆中華民國立法委员](../Page/第1屆中華民國立法委员.md "wikilink")。1949年2月，受代总统[李宗仁派遣](../Page/李宗仁.md "wikilink")，前往[西柏坡同](../Page/西柏坡.md "wikilink")[中国共产党和谈](../Page/中国共产党.md "wikilink")。2月23日，顏惠慶等飛[石家莊晤毛周](../Page/石家莊.md "wikilink")，對「和平」及通航問題，「廣泛交換意見」\[2\]。和谈失败后，顔返回上海。5月6日，[蔣經國奉](../Page/蔣經國.md "wikilink")[蔣介石命訪顏惠慶](../Page/蔣介石.md "wikilink")「於[中山醫院](../Page/中山醫院.md "wikilink")」\[3\]。[中国人民解放軍攻佔上海前](../Page/中国人民解放軍.md "wikilink")，他留在上海，拒絕隨中國國民黨撤離。

### 中华人民共和国时期

[中華人民共和国建国後](../Page/中華人民共和国.md "wikilink")，颜惠庆任[中国人民政治協商会議全国委員会委員](../Page/中国人民政治協商会議全国委員会.md "wikilink")、[政务院政法委员会委員](../Page/政务院政法委员会.md "wikilink")、[華東軍政委員会副主席](../Page/華東軍政委員会.md "wikilink")。

1950年5月24日，颜惠庆因[心臟病病逝于上海](../Page/心臟病.md "wikilink")，享年74歳（滿73歳）。[毛泽东和](../Page/毛泽东.md "wikilink")[周恩来均致唁电](../Page/周恩来.md "wikilink")。

## 其他

  - 颜在教育事业方面也多有建树。他曾于1907年主持编辑了中国首本《[英华大辞典](../Page/英华大辞典.md "wikilink")》。后又参与筹办[清华学堂](../Page/清华大学.md "wikilink")，并主持成立了清华学校校董会。曾担任私立南开大学董事会董事、董事长。他还参与了[国立北平图书馆的建立和](../Page/国立北平图书馆.md "wikilink")[欧美同学会的组织](../Page/欧美同学会.md "wikilink")。
  - 在商界，颜于1929年出任[天津大陆商业公司和](../Page/天津大陆商业公司.md "wikilink")[天津大陆银行的董事长](../Page/天津大陆银行.md "wikilink")。
  - 颜著有英文自传《东西方万花筒》(*East-West
    Kaleidoscope*)，其在中国大陆出版的中译本名为《颜惠庆自传——一位民国元老的历史记忆》，吴建雍、李宝臣、叶凤美译，于2005年1月商务印书馆出版。

## 颜惠庆内阁

### 1922年颜惠庆内阁

1922年6月12日成立。

### 1924年颜惠庆内阁

1924年9月14日成立。

### 1926年颜惠庆内阁

1926年5月13日恢复。宣佈曹錕於5月1日辭職，依法由國務院攝行大總統職權。

## 家庭

[颜永京有四子一女](../Page/颜永京.md "wikilink")，长子[颜志庆](../Page/颜志庆.md "wikilink")，次子[颜惠庆](../Page/颜惠庆.md "wikilink")（1877年－1950年）是中华民国政府总理、外交家，幼子[颜德庆](../Page/颜德庆.md "wikilink")（1878年－1942年）为铁路工程师，另有一子早夭，女儿颜庆莲。侄子[颜福庆](../Page/颜福庆.md "wikilink")（1882年－1970年）幼年丧父，亦为颜永京资助留学，日后为著名医学教育家。颜惠庆、颜德庆、颜福庆并称“颜氏三杰”。

## 注释

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - </span>

  - </span>

  -
## 外部链接

  - [《东西方万花筒》中文版前言](https://web.archive.org/web/20051217190126/http://www.zgjds.org/zgjds/07science/literature155.htm)

## 参见

  - [颜永京](../Page/颜永京.md "wikilink")、[颜德庆](../Page/颜德庆.md "wikilink")、[颜福庆](../Page/颜福庆.md "wikilink")

{{-}}

|                                      |
| ------------------------------------ |
| （[北京政府](../Page/北京政府.md "wikilink")） |

[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:中華民國企業家](../Category/中華民國企業家.md "wikilink") [Category:清華大學校長
(中華民國大陸時期)](../Category/清華大學校長_\(中華民國大陸時期\).md "wikilink")
[Category:中華民國外交總長](../Category/中華民國外交總長.md "wikilink")
[Category:中華民國農商總長](../Category/中華民國農商總長.md "wikilink")
[Category:中華民國内務總長](../Category/中華民國内務總長.md "wikilink")
[Category:中華民國國務總理](../Category/中華民國國務總理.md "wikilink")
[Category:中華民國駐德國大使](../Category/中華民國駐德國大使.md "wikilink")
[Category:中華民國駐瑞典大使](../Category/中華民國駐瑞典大使.md "wikilink")
[Category:中華民國駐丹麥大使](../Category/中華民國駐丹麥大使.md "wikilink")
[Category:中華民國駐蘇聯大使](../Category/中華民國駐蘇聯大使.md "wikilink")
[Category:中華民國駐美國大使](../Category/中華民國駐美國大使.md "wikilink")
[Category:中华民国北平市市长](../Category/中华民国北平市市长.md "wikilink")
[Category:第一届国民参政会参政员](../Category/第一届国民参政会参政员.md "wikilink")
[Category:第二届国民参政会参政员](../Category/第二届国民参政会参政员.md "wikilink")
[Category:第1屆正選立法委員](../Category/第1屆正選立法委員.md "wikilink")
[Category:第一屆全國政協委員](../Category/第一屆全國政協委員.md "wikilink")
[Category:維吉尼亞大學校友](../Category/維吉尼亞大學校友.md "wikilink")
[Category:中華民國紅十字會會長](../Category/中華民國紅十字會會長.md "wikilink")
[Category:葬于宋庆龄陵园](../Category/葬于宋庆龄陵园.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[H惠](../Category/顏姓.md "wikilink")

1.
2.  蔣經國<危急存亡之秋>，刊《風雨中的寧靜》，[台北](../Page/台北.md "wikilink")，[正中書局](../Page/正中書局.md "wikilink")，1988年，第153頁。
3.  蔣經國<危急存亡之秋>，刊《風雨中的寧靜》，台北，正中書局，1988年，第197頁。