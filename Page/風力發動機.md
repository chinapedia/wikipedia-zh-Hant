[Windmills_D1-D4_(Thornton_Bank).jpg](https://zh.wikipedia.org/wiki/File:Windmills_D1-D4_\(Thornton_Bank\).jpg "fig:Windmills_D1-D4_(Thornton_Bank).jpg")

**風力發動機**（），簡稱**風機**或**風力機**，將氣流的[動能轉為](../Page/動能.md "wikilink")[機械能](../Page/機械能.md "wikilink")。此裝置通常會接上並且帶動[發電機用來發電](../Page/發電機.md "wikilink")，是構成[風力發電廠的必要條件之一](../Page/風力發電廠.md "wikilink")。

## 歷史

[古老的垂直轴风力发电机.jpg](https://zh.wikipedia.org/wiki/File:古老的垂直轴风力发电机.jpg "fig:古老的垂直轴风力发电机.jpg")
风力发电机是现代科学技术的产物，是人类利用自然风能将气流的动能转为[机械能](../Page/机械能.md "wikilink")，並连接和带动发电机运转用來发电的一种发电设备。人类利用风力发电的尝试，最早在19世纪末的欧洲就已经开始。20世纪三十年代，丹麦、瑞典、苏联和美国应用航空工业的旋翼技术，成功地研制了一些小型风力发电装置。这种小型风力发电机，广泛在多风的海岛和偏僻的乡村使用，它所获得的电力成本比小型内燃机的发电成本低得多。人类最早利用风力来发电的尝试起源于[丹麦设计的垂直轴风力](../Page/丹麦.md "wikilink")[发电机](../Page/发电机.md "wikilink")，水平轴风力发电机最早也出现在欧洲。

## 材料

风力发电机构件，特别是风机转子叶片必须在保证结构强度与性能长期稳定的条件下实现轻量化设计。此外，为满足经济要求，风机构件还需具备成本效益优势。不仅如此，较长的生产周期亦要求材料兼备高温与易加工的特性。风机转子叶片的材料经历了从木材，金属，工程塑料到玻璃钢复合材料的发展历程。所谓玻璃钢就是环氧树脂、不饱和树脂等塑料渗入长度不同的玻璃纤维或碳纤维而做成的增强塑料，增强塑料表面可再缠玻璃纤维及涂环氧树脂，其他部分填充泡沫塑料。泡沫在叶片中主要是提高叶片刚度、增加稳定性并且减轻叶片质量的作用。泡沫的本体力学性能会影响到夹层机构的力学性能，同时泡沫密度不同对叶片的质量分布也会产生影响，进而影响到叶片的载荷分布。聚氯乙烯（PVC）泡沫使用最为广泛，属于热固性泡沫，也是第一种用在承载构件夹层结构中的结构泡沫芯材。但以PVC为代表的热固性泡沫，由于无法降解和回收，对环境和资源都存在影响，随着未来低碳经济的发展，热塑性泡沫如PET（Airex）等将会得到越来越多的重视和发展。

## 種類

### 水平軸風力發電機

水平轴风力发电机可分为升力型和阻力型两类。升力型风力发电机旋转速度快，阻力型旋转速度慢。对于风力发电，多采用升力型水平轴风力发电机。大多数水平轴风力发电机具有对风装置，能随风向改变而转动。对于小型风力发电机，这种对风装置采用尾舵，而对于大型的风力发电机，则利用风向传感元件以及伺服电机组成的传动机构。风力机的风轮安装在轮毂上，风力机运行时轮毂正对风向称为上风向风力机，背对风向则称为下风向风机。水平轴风力发电机的式样很多，有的具有反转叶片的风轮，有的在一个塔架上安装多个风轮，以便在输出功率一定的条件下减少塔架的成本，还有的水平轴风力发电机在风轮周围产生漩涡，集中气流，增加气流速度。

<center>

<table>
<tbody>
<tr class="odd">
<td><center>
<p><small>各种类型的</small><strong>水平軸</strong><small>風力發電機</small></p>
</center></td>
</tr>
<tr class="even">
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:EolienneCheminDeHalageNanterre.jpg" title="fig:EolienneCheminDeHalageNanterre.jpg">EolienneCheminDeHalageNanterre.jpg</a></p>
</center></td>
</tr>
</tbody>
</table>

</center>

### 垂直軸風力發電機

T垂直轴风力发电机在风向改变的时候无需对风，在这点上相对于水平轴风力发电机是一大优势，它不仅使结构设计简化，而且也减少了风轮对风时的陀螺力。利用阻力旋转的垂直轴风力发电机有几种类型，其中有利用平板和被子做成的风轮，这是一种纯阻力装置；S型风车，具有部分升力，但主要还是阻力装置。这些装置有较大的启动力矩，但尖速比低，在风轮尺寸、重量和成本一定的情况下，提供的功率输出低。

<center>

<table>
<tbody>
<tr class="odd">
<td><center>
<p><small>各种类型的</small><strong>垂直軸</strong><small>風力發電機</small></p>
</center></td>
</tr>
<tr class="even">
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Noveolienne_en_gros_plan.JPG" title="fig:Noveolienne_en_gros_plan.JPG">Noveolienne_en_gros_plan.JPG</a></p>
</center></td>
</tr>
</tbody>
</table>

</center>

### 水車式風力機

**水車式風力機**就是採用類似水車樣式的風力機,是擷取風力的最佳設計。

  - 水車式風力機的特點是可以依據風向的變化，讓葉片保持著正面迎風呈180度，順風向而轉的特點；因故擷取風力係數為1。

目前常用**風力發動機**大都為**水平軸式風力機**，這種形式的風力機其葉片須與風向成45度（最佳角度），來才能推動葉片，方能進行風力發電，如此擷取風力係數只為0.5；發電的效益較低，
故而改用水車式風力機，可使用全葉面來擷取風力，讓風力擷取係數達到1:1的效果；提升風力發電的效益。\[1\]

## 主要製造商

  - 德國

<!-- end list -->

  - [愛納康](../Page/愛納康.md "wikilink")（Enercon）
  - [Repower](../Page/Repower.md "wikilink")
  - [Nordex](../Page/Nordex.md "wikilink")
  - [Multibrid](../Page/Multibrid.md "wikilink")
  - [Power Wind](../Page/Power_Wind.md "wikilink")
  - [英華威](../Page/英華威.md "wikilink")(InfraVest)
  - [艾萬迪斯](../Page/艾萬迪斯.md "wikilink")(Avantis)

<!-- end list -->

  - 丹麥

<!-- end list -->

  - [Bonus](../Page/Bonus.md "wikilink")（被[西門子收购](../Page/西門子.md "wikilink")）
  - [NEG Micon](../Page/NEG_Micon.md "wikilink")（并入维斯塔斯）
  - [維斯塔斯](../Page/維斯塔斯.md "wikilink")（Vestas）

<!-- end list -->

  - 西班牙

<!-- end list -->

  - [Gamesa](../Page/Gamesa.md "wikilink")
  - [MADE](../Page/MADE.md "wikilink")（并入Gamesa）
  - [西門子歌美颯再生能源](../Page/西門子歌美颯再生能源.md "wikilink")（Siemens Gamesa
    Renewable Energy）

<!-- end list -->

  - 美國

<!-- end list -->

  - [恩龍](../Page/安然公司.md "wikilink")（Enron，被[GE
    wind收购](../Page/GE_wind.md "wikilink")）
  - [GE风能](../Page/GE风能.md "wikilink")
  - [奇異公司](../Page/奇異公司.md "wikilink")（General Electric）

<!-- end list -->

  - 日本

<!-- end list -->

  - [三菱重工](../Page/三菱重工.md "wikilink")

<!-- end list -->

  - 中華人民共和國（大陸）

<!-- end list -->

  - [广州绿欣发电机](../Page/广州绿欣发电机.md "wikilink")
  - [青岛格林风新能源设备](../Page/青岛格林风新能源设备.md "wikilink")
  - [上海奥博莱](../Page/上海奥博莱.md "wikilink")
  - [重庆海装风电](../Page/重庆海装风电.md "wikilink")
  - [上海风电](../Page/上海风电.md "wikilink")
  - [红叶风电](../Page/红叶风电.md "wikilink")
  - [运达风电](../Page/运达风电.md "wikilink")
  - [金风科技](../Page/金风科技.md "wikilink")
  - [华锐风电](../Page/华锐风电.md "wikilink")
  - [东方电气](../Page/东方电气.md "wikilink")
  - [联合动力](../Page/联合动力.md "wikilink")
  - [上海麟风风能](../Page/上海麟风风能.md "wikilink")
  - [武汉国测诺德](../Page/武汉国测诺德.md "wikilink")

<!-- end list -->

  - 印度

<!-- end list -->

  - [蘇司蘭](../Page/蘇司蘭.md "wikilink")(Suzlon)

<!-- end list -->

  - 中華民國（臺灣）

<!-- end list -->

  - [東元電機](../Page/東元電機.md "wikilink")
  - [豐能動力科技](../Page/豐能動力科技.md "wikilink")

## 參見

  - [風能](../Page/風能.md "wikilink")
  - [風力發電](../Page/風力發電.md "wikilink")
  - [風力發電廠](../Page/風力發電廠.md "wikilink")
  - [風車 (動力機)](../Page/風車_\(動力機\).md "wikilink")

## 外部連結

  - [青岛格林风新能源设备](http://www.greefenergy.com)
  - [綠色和平：風力發電](http://www.greenpeace.org/china/ch/campaigns/stop-climate-change/solutions/wind-energy)
  - [风能中国网](https://web.archive.org/web/20120605013223/http://www.windpower-china.cn/)
  - [台灣風能協會](http://www.twnwea.org.tw)

## 參考資料

[Category:空气动力学](../Category/空气动力学.md "wikilink")
[Category:電力](../Category/電力.md "wikilink")
[Category:發電機](../Category/發電機.md "wikilink")
[Category:电机械工程](../Category/电机械工程.md "wikilink")
[Category:能源转换](../Category/能源转换.md "wikilink")
[Category:風力發電](../Category/風力發電.md "wikilink")

1.  \[<https://twpat-simple.tipo.gov.tw/tipotwoc/tipotwkm>?\!\!FR_M540187
    中華民國專利證M540187\]