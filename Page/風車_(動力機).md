[DK_Fanoe_Windmill01.JPG](https://zh.wikipedia.org/wiki/File:DK_Fanoe_Windmill01.JPG "fig:DK_Fanoe_Windmill01.JPG")西海岸法诺岛上的荷蘭型風車\]\]

**风车**是一种把[风能转变为](../Page/风能.md "wikilink")[机械能的动力机](../Page/机械能.md "wikilink")；用可调节叶片或梯级横木轮子收集风力。

简单的风车由带有风蓬的风轮、支架及传动装置等构成；风轮的转速和功率，可以根据风力的大小，适当改变风蓬的数目或受风面积来调整；在风向改变时，必须搬动前支架使风轮面向风；完备的风车带有自动调速和迎风装置等。

具備發電用途的風車又稱為[風力發電機](../Page/風力發電機.md "wikilink")。

## 历史

[De_Liefde_Windmill,_Sakura,_Chiba,_Japan_-_20060417.jpg](https://zh.wikipedia.org/wiki/File:De_Liefde_Windmill,_Sakura,_Chiba,_Japan_-_20060417.jpg "fig:De_Liefde_Windmill,_Sakura,_Chiba,_Japan_-_20060417.jpg")
第一架风车大约是出现古希臘\[1\]\[2\]。

在[中国](../Page/中国.md "wikilink")，使用风车的历史很早。在[辽阳](../Page/辽阳.md "wikilink")[三道壕](../Page/三道壕.md "wikilink")[东汉晚期的](../Page/东汉.md "wikilink")[汉墓](../Page/汉墓.md "wikilink")[壁画上](../Page/壁画.md "wikilink")，就画有风车的图样。距今已有1700多年的历史。[明代开始应用风力來驅動](../Page/明代.md "wikilink")[水车以灌溉农田](../Page/水车.md "wikilink")。

西元1180年[诺曼底的一个风车](../Page/诺曼底.md "wikilink")，这种风车有一个卧式的主动轴和垂直的帆翼，所以许多人认为这很可能和[十世纪有垂直主动轴的东方风车无关](../Page/十世纪.md "wikilink")，而是[欧洲人自己发明的](../Page/欧洲人.md "wikilink")。到[十九世纪](../Page/十九世纪.md "wikilink")，风车的使用达到全盛时期。据记载，当时仅[荷兰就有一万多架风车](../Page/荷兰.md "wikilink")，[美国农村更有一百多万架风车](../Page/美国.md "wikilink")。

到[二十世纪初](../Page/二十世纪.md "wikilink")，[丹麦还保留有十多万架风车](../Page/丹麦.md "wikilink")。在[英国](../Page/英国.md "wikilink")、[希腊等岛屿国家的乡村中](../Page/希腊.md "wikilink")，都广泛地使用风车。在一些动力资源缺乏和交通不便的[草原牧区](../Page/草原.md "wikilink")、沿海[岛屿](../Page/岛屿.md "wikilink")，仍然用它来进行一些工作。

## 分類

[Brill_windmill_April_2017.jpg](https://zh.wikipedia.org/wiki/File:Brill_windmill_April_2017.jpg "fig:Brill_windmill_April_2017.jpg")

### 水平軸風車

  - 螺旋槳型
  - 多翼型
  - 荷蘭型
  - 帆翼型（克里特式）

[Verticle_Windmill.JPG](https://zh.wikipedia.org/wiki/File:Verticle_Windmill.JPG "fig:Verticle_Windmill.JPG")

### 垂直軸風車

  - 旋轉輪型
  - Q錐型
  - 雙翼型
  - 划槳翼型

## 圖片

<File:Windmill> in Zaanse Schans.jpg|荷蘭型風車 檔案:Windpump near
Winburg.jpg|多翼型風車 檔案:Darrieus-windmill.jpg|Q錐型風車 <File:Windmill>
01.JPG|風力發電用風車

## 參考資料

[category:风车](../Page/category:风车.md "wikilink")

[es:Molino\#Molinos de
viento](../Page/es:Molino#Molinos_de_viento.md "wikilink")

[Category:建築物](../Category/建築物.md "wikilink")
[Category:風力發電](../Category/風力發電.md "wikilink")

1.  [Mill definition](http://www.thefreedictionary.com/Mill)
2.  [Windmill definition stating that a windmill is a mill or machine
    operated by the
    wind](http://www.merriam-webster.com/dictionary/windmill)