**風雷影音**是一款獨創雙播放內核的[媒体播放器](../Page/媒体播放器.md "wikilink")，最後版本為2.1.0.5。風雷影音已停止開發、更新。

風雷影音聚合了[MPlayer以及](../Page/MPlayer.md "wikilink")[MPC的功能](../Page/MPC.md "wikilink")，全面支持高清影片播放。採用級聯式多層過濾解碼，使影片兼容性提升到。可以支援播放AVI、MPEG（MPG）、WMV、WMA、RMVB（RM）、MOV、3GP、QT、ASF、DVD、VCD、CD、MKV、MTS、M2TS、EAC3、EVO、TS、TP、FLV、、CSF、MP4、MP5、VP6、PMP、PMF、OGG、DIVx、M1V、FLA、FLAC、MP3、APE、AC3、MOD、AIFC、AIFF、MID、MIDI、SMK、SWF、AAC、AC3等206種格式的影音文件。

## 外部链接

  - [風雷影音](http://web.archive.org/web/*/flmpc.com)

[Category:媒體播放器](../Category/媒體播放器.md "wikilink")