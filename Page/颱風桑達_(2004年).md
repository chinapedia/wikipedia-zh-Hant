**颱風桑達**（，國際編號：**0418**，[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")：**22W**，[菲律賓大氣地球物理和天文管理局](../Page/菲律賓大氣地球物理和天文管理局.md "wikilink")：**Nina**）為[2004年太平洋颱風季第十八個被命名的風暴](../Page/2004年太平洋颱風季.md "wikilink")。「桑達」一名由[越南提供](../Page/越南.md "wikilink")，是指[紅河一條名為](../Page/紅河.md "wikilink")「」（Sông
Đà）的[支流](../Page/支流.md "wikilink")。

桑達是一個星期內第二個吹襲日本的颱風，之前日本受到[颱風暹芭的影響](../Page/颱風暹芭_\(2004年\).md "wikilink")。

## 發展過程

桑達是於2004年8月28日，在[馬紹爾群島附近形成](../Page/馬紹爾群島.md "wikilink")，隨后向西前進。31日時中心附近最大風速達每秒45米（大約每小時160公里）。原本預測桑達將朝[中國前進](../Page/中國.md "wikilink")，但由於[副熱帶高壓的減弱](../Page/副熱帶高壓.md "wikilink")，桑達改朝西北前進。

9月5日下午4時半，桑達於[日本](../Page/日本.md "wikilink")[沖繩縣](../Page/沖繩縣.md "wikilink")[名護市附近通過](../Page/名護市.md "wikilink")，在[名護市測得](../Page/名護市.md "wikilink")924.4hPa的最低海面氣壓，之后其強度有所減弱。桑達進入[東中國海后轉向東北](../Page/東中國海.md "wikilink")，在7日8時半左右於[日本](../Page/日本.md "wikilink")[長崎縣](../Page/長崎縣.md "wikilink")[長崎市附近登陸](../Page/長崎市.md "wikilink")，當時中心氣壓有945hPa，中心附近最大風速40m／s（大約145km／h）。隨后陸續經過[佐賀縣](../Page/佐賀縣.md "wikilink")、[福岡縣](../Page/福岡縣.md "wikilink")、[山口縣](../Page/山口縣.md "wikilink")，12時進入[日本海](../Page/日本海.md "wikilink")。

其后桑達快速北上，8日時接近[東北地方及](../Page/東北地方.md "wikilink")[北海道地方](../Page/北海道地方.md "wikilink")，于北海道西南沿岸變為[溫帶氣旋](../Page/溫帶氣旋.md "wikilink")。

## 影響

###

9月6日至7日，在[南韓的](../Page/南韓.md "wikilink")[鬱陵島錄得](../Page/鬱陵島.md "wikilink")112毫米的雨量。

###

4日至9日，在[諸塚錄得](../Page/諸塚村.md "wikilink")905毫米雨量，其中358毫米集中在6日及7日早上。在廣島錄得最高風速為135英哩／60.2米／秒。在佐賀錄得944.3的低氣壓。新聞報導指出颱風在日本造成20人死亡及700多人受傷。此外，15名輪船人員失蹤。桑達造成了三宗地震。\[1\]

## 圖片庫

Songda 2004 09 01 0040Z.jpg|9月1日 Super Typhoon Songda 2004.jpg|9月1日
Typhoon Songda 02 sept 2004 0125Z.jpg|9月2日 Typhoon 200418
SONGDA.JPG|9月4日[國際太空站下的桑達](../Page/國際太空站.md "wikilink") Songda 2004
09 06 0235Z.jpg|9月6日 Songda 2004 09 06 0235Z.jpg|9月6日

## 參考資料

## 參見

  - [2004年太平洋颱風季](../Page/2004年太平洋颱風季.md "wikilink")

## 外部連結

  - <http://www.jma.go.jp/> [日本氣象廳首頁](../Page/日本氣象廳.md "wikilink")

  - <http://www.usno.navy.mil/JTWC/>
    [聯合颱風警報中心首頁](../Page/聯合颱風警報中心.md "wikilink")

  - <http://www.nmc.gov.cn/> [中央氣象台首頁](../Page/中央氣象台.md "wikilink")

  - <http://www.cwb.gov.tw/> [中央氣象局首頁](../Page/中央氣象局.md "wikilink")

  - <http://www.hko.gov.hk/> [香港天文台首頁](../Page/香港天文台.md "wikilink")

  - <http://www.smg.gov.mo/>
    [澳門地球物理暨氣象局首頁](../Page/澳門地球物理暨氣象局.md "wikilink")

[category:2004年太平洋颱風季](../Page/category:2004年太平洋颱風季.md "wikilink")
[category:四级热带气旋](../Page/category:四级热带气旋.md "wikilink")

[Category:影響日本的熱帶氣旋](../Category/影響日本的熱帶氣旋.md "wikilink")
[Category:2004年日本](../Category/2004年日本.md "wikilink")

1.