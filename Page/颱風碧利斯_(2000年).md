**颱風碧利斯**（，國際編號：**0010**，[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")：**18W**，[菲律賓大氣地球物理和天文管理局](../Page/菲律賓大氣地球物理和天文管理局.md "wikilink")：**Isang**）為[2000年太平洋颱風季第十個被命名的風暴](../Page/2000年太平洋颱風季.md "wikilink")。「碧利斯」一名由[菲律賓提供](../Page/菲律賓.md "wikilink")，是「快速」的意思。

碧利斯以[五級颱風的強度登陸](../Page/薩菲爾-辛普森颶風等級.md "wikilink")[台灣](../Page/台灣.md "wikilink")[台東縣](../Page/台東縣.md "wikilink")[成功鎮](../Page/成功鎮.md "wikilink")，在台灣島上帶來17級風力和暴雨，並於[福建](../Page/福建.md "wikilink")、[浙江一帶造成嚴重災情](../Page/浙江.md "wikilink")。雖然如此，由於[台灣並非](../Page/台灣.md "wikilink")[世界氣象組織](../Page/世界氣象組織.md "wikilink")[颱風委員會成員](../Page/颱風委員會.md "wikilink")，而且當時並無委員國提議除名，「碧利斯」這個名字並沒有因此而退役。直到[2006年同名颱風在中國大陸和台灣造成超過](../Page/強烈熱帶風暴碧利斯_\(2006年\).md "wikilink")800人死亡或失蹤後，「碧利斯」這個名字才被永久退役。

## 發展過程

[JTWC_wp1800.gif](https://zh.wikipedia.org/wiki/File:JTWC_wp1800.gif "fig:JTWC_wp1800.gif")預報圖\]\]
碧利斯是本颱風季最強的熱帶氣旋。8月18日UTC12時，一熱帶低氣壓在[雅蒲島西北海面上生成](../Page/雅蒲島.md "wikilink")。它在[副熱帶高壓脊的引導下](../Page/副熱帶高壓脊.md "wikilink")，穩定地向西北移動。8月19日UTC6時，該熱帶低氣壓增強為熱帶風暴，並命名為碧利斯。UTC18時，碧利斯增強為強烈熱帶風暴。8月20日UTC12時，碧利斯在[菲律賓](../Page/菲律賓.md "wikilink")[呂宋島東北面增強為颱風](../Page/呂宋島.md "wikilink")，最高持續風速為110kt，一分鐘平均風速140kts。其後碧利斯轉向西北偏西移動，直趨[台灣](../Page/台灣.md "wikilink")，同時在良好的大氣環境下穩定地增強。碧利斯於8月22日UTC14時30分（當地時間22時30分）於[台灣](../Page/台灣.md "wikilink")[台東縣](../Page/台東縣.md "wikilink")[成功鎮登陸](../Page/成功鎮.md "wikilink")，大約4小時半後在[雲林縣](../Page/雲林縣.md "wikilink")[口湖鄉附近進入台灣海峽](../Page/口湖鄉.md "wikilink")。碧利斯並沒有受台灣[中央山脈的嚴重破壞](../Page/中央山脈.md "wikilink")，出海時仍達颱風強度。8月23日UTC4時左右（當地時間中午時分），碧利斯於[福建省](../Page/福建省.md "wikilink")[泉州市](../Page/泉州市.md "wikilink")[晉江市](../Page/晉江市.md "wikilink")[東石鎮附近作第二次登陸](../Page/東石鎮_\(晉江市\).md "wikilink")。隨後，碧利斯繼續向內陸推進並迅速減弱。UTC6時，碧利斯減弱為熱帶風暴。UTC12時，碧利斯減弱為熱帶低氣壓。隨後它受[西風槽影響](../Page/西風槽.md "wikilink")，逐漸轉向東北偏北移動。8月25日UTC18時，碧利斯在[黃海轉化為溫帶氣旋](../Page/黃海.md "wikilink")。8月27日UTC6時，碧利斯登陸[北韓](../Page/北韓.md "wikilink")[黃海南道](../Page/黃海南道.md "wikilink")[延安郡一帶](../Page/延安郡.md "wikilink")。UTC12時，完全消散。\[1\]

## 影響

###

  -
    當地發佈最高熱帶氣旋警告信號：[海上](../Page/海上颱風警報.md "wikilink")[陸上颱風警報](../Page/陸上颱風警報.md "wikilink")

8月21日上午8時25分，[中央氣象局發佈](../Page/中央氣象局.md "wikilink")[海上颱風警報](../Page/海上颱風警報.md "wikilink")。下午2時45分，中央氣象局發佈[陸上颱風警報](../Page/陸上颱風警報.md "wikilink")。

8月23日下午8時5分，中央氣象局解除所有颱風警報。

碧利斯在[台灣造成最少](../Page/台灣.md "wikilink")11人死亡，80人受傷，434棟房屋倒塌，超過1800間房屋損毀，100萬戶電力供應中斷，數十萬戶食水供應中斷。碧利斯在[台灣造成約](../Page/台灣.md "wikilink")1.3億美元的經濟損失，以[花蓮縣](../Page/花蓮縣.md "wikilink")、[台中縣受災最為嚴重](../Page/台中縣.md "wikilink")。

在碧利斯吹襲期間，[花蓮縣](../Page/花蓮縣.md "wikilink")[玉里鎮測得](../Page/玉里鎮.md "wikilink")912毫米的雨量，[台東縣](../Page/台東縣.md "wikilink")[成功鎮更測出了每秒](../Page/成功鎮.md "wikilink")78.4公尺（相當於17級以上）的最大陣風，打破台灣本島氣象站有史以來的紀錄。\[2\]
\[3\] \[4\]

###

碧利斯在[中國大陸造成最少](../Page/中國.md "wikilink")57人死亡，1077人受傷或失蹤。與風暴相關的[龍捲風吹襲了](../Page/龍捲風.md "wikilink")[浙江](../Page/浙江.md "wikilink")[溫州](../Page/溫州.md "wikilink")[樂清市附近四條村](../Page/樂清市.md "wikilink")，摧毀了20幢建築物，另有130幢損壞。碧利斯在[中國大陸造成約](../Page/中國.md "wikilink")5.34億美元的經濟損失。\[5\]
\[6\]

###

  -
    當地發佈最高熱帶氣旋警告信號：

一號戒備信號在八月二十三日上午6時正懸掛，當時碧利斯位於香港東北偏東約550公里。受到碧利斯的環流的影響，本地風力有所增強，天氣也漸轉不穩定，有幾陣狂風驟雨及雷暴。香港天文台總部在八月二十三日下午4時49分錄得最低瞬時海平面氣壓為996.7百帕斯卡。碧利斯在下午6時左右最接近本港，當時它位於香港東北面約430公里。由於碧利斯迅速減弱並進一步移入內陸，所有熱帶氣旋警告信號在下午7時15分除下。\[7\]

碧利斯過後，與其相聯的西南氣流在八月二十四日的清晨為香港帶來暴雨。黑色暴雨警告信號在早上3時05分發出，到早上5時55分才取消。港島南區雨量最多，淺水灣及鶴咀都在一小時內錄得超過100毫米的雨量，由午夜至早上5時的五個小時內則錄得超過300毫米的雨量。\[8\]

在暴雨下，本港有逾92宗水浸及23宗山泥傾瀉。港島區情況最為嚴重。在皇后大道東，由金鐘至摩理臣山道的一段，水深一度達一米。區內不少銀行、店鋪及大廈皆受影響。跑馬地馬場也有水浸，部分投注終端機受損。薄扶林村則水深一度達二米，多名村民由消防員救離。在八月二十四日下午，元朗一個家庭共12人被雨水圍困，由消防員協助脫險。香港公園附近發生山泥傾瀉，泥沙從香港公園沖至法院道，公園內部分設施遭山泥破壞，法院道亦需暫時封閉。\[9\]

暴雨亦造成多處道路下陷。在石澳道，路陷嚴重，要封閉多時。\[10\]

[強烈季候風信號也一度懸掛](../Page/香港天文台#強烈季候風信號.md "wikilink")。 \[11\]

## 熱帶氣旋警告使用記錄

\- {{\#time:Y.m.d H:i|2000-8-23 19:15}}
|上一熱帶氣旋=[熱帶低氣壓08W](../Page/2000年太平洋颱風季#熱帶低氣壓08W.md "wikilink")
|下一熱帶氣旋=[強烈熱帶風暴瑪莉亞](../Page/熱帶風暴瑪莉亞_\(2000年\).md "wikilink") }}

## 圖片庫

Super Typhoon Bills at peak intensity Aug 22 2000.jpg|8月22日 Typhoon
Bilis 22 Aug 2000 VIS Image.jpg|8月22日 Typhoon Bilis 22 aug 2000
0832Z.jpg|8月22日 TRCbilis235A GM.jpg|8月22日 Bilis 22 Aug 2000
2331Z.jpg|8月22日 Bilis 23 Aug 2000 0228Z.jpg|8月23日

## 參考資料

## 參見

同期出現的熱帶氣旋：

  - [熱帶風暴格美](../Page/熱帶風暴格美_\(2000年\).md "wikilink")
  - [颱風派比安](../Page/颱風派比安_\(2000年\).md "wikilink")

## 外部連結

  - <http://www.jma.go.jp/> [日本氣象廳首頁](../Page/日本氣象廳.md "wikilink")

  - <http://www.usno.navy.mil/JTWC/>
    [聯合颱風警報中心首頁](../Page/聯合颱風警報中心.md "wikilink")

  - <http://www.nmc.gov.cn/> [中央氣象台首頁](../Page/中央氣象台.md "wikilink")

  - <http://www.cwb.gov.tw/> [中央氣象局首頁](../Page/中央氣象局.md "wikilink")

  - <http://www.hko.gov.hk/> [香港天文台首頁](../Page/香港天文台.md "wikilink")

  - <http://www.smg.gov.mo/>
    [澳門地球物理暨氣象局首頁](../Page/澳門地球物理暨氣象局.md "wikilink")

[Category:2000年太平洋颱風季](../Category/2000年太平洋颱風季.md "wikilink")
[Category:影響臺灣的熱帶氣旋](../Category/影響臺灣的熱帶氣旋.md "wikilink")
[颱風](../Category/2000年中國.md "wikilink")
[颱風](../Category/2000年台灣.md "wikilink")
[Category:五級熱帶氣旋](../Category/五級熱帶氣旋.md "wikilink")
[颱風](../Category/2000年韩国.md "wikilink")
[Category:影响中国大陆的热带气旋](../Category/影响中国大陆的热带气旋.md "wikilink")
[Category:台风](../Category/台风.md "wikilink")

1.  [2000年日本氣象廳年鑒](http://www.jma.go.jp/jma/jma-eng/jma-center/rsmc-hp-pub-eg/AnnualReport/2000/Text/Text2000.pdf)

2.  [聯合颱風警報中心對碧利斯的報告](http://metocph.nmci.navy.mil/jtwc/atcr/2000atcr/ch1/chap1_page23.html)


3.  [[香港天文台二零零零年熱帶氣旋年報](../Page/香港.md "wikilink")](http://www.hko.gov.hk/publica/tc/tc2000.pdf)

4.  [TDB颱風資料庫](http://rdc28.cwb.gov.tw/data.php?num=2000100819&year=2000&c_name=%BA%D1%A7Q%B4%B5&e_name=BILIS)

5.  [聯合颱風警報中心對碧利斯的報告](http://metocph.nmci.navy.mil/jtwc/atcr/2000atcr/ch1/chap1_page23.html)


6.  [[香港天文台二零零零年熱帶氣旋年報](../Page/香港.md "wikilink")](http://www.hko.gov.hk/publica/tc/tc2000.pdf)

7.  [2000年香港天文台熱帶氣旋年刊](http://www.hko.gov.hk/publica/tc/tc2000.pdf)

8.
9.
10.
11. [天氣稿第081號 -
    強烈季候風信號](http://www.info.gov.hk/gia/wr/200008/24/0824099.htm),
    2000年8月24日