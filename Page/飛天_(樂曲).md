[缩略图](https://zh.wikipedia.org/wiki/File:File-Apsara_playing_a_Chinese_flute_-_Yulin_Cave_15.jpg "fig:缩略图")[榆林窟中的](../Page/榆林窟.md "wikilink")[飛天神壁畫](../Page/飛天_\(佛教\).md "wikilink")。\]\]
《**飛天**》是一首[國樂合奏曲](../Page/國樂.md "wikilink")，為1982年時[徐景新](../Page/徐景新.md "wikilink")、[陳大偉所作](../Page/陳大偉_\(中國音樂家\).md "wikilink")。樂曲根據[敦煌壁畫中](../Page/敦煌壁畫.md "wikilink")[印度神話神祇](../Page/印度教神话.md "wikilink")[飛天女神](../Page/飛天女神.md "wikilink")（）的形象為題材所作，具有特色的曲調、風格也善用國樂器的音色，是國樂界常演出的曲目之一。本曲在中國第三屆音樂作品評獎的民族器樂組中，獲得二等獎\[1\]。

作曲者以音樂描繪飛天形象，不受空間限制，可以盡情抒發。旋律性格既統一復有變化，結構龐大，緊凑而不拖延。\[2\]

## 樂曲沿革

徐景新在一次訪問敦煌之後，與陳大偉二人共同創作此曲。於第十屆「上海之春」音樂會首演\[3\]。

## 樂曲結構

樂曲一開始的引子以[雲鑼](../Page/云锣.md "wikilink")、[碰鈴的響聲](../Page/碰鈴.md "wikilink")，高音、中音[笙的](../Page/笙.md "wikilink")[七和弦極弱](../Page/七和弦.md "wikilink")（pp）呼舌，[揚琴雙聲部不同節奏交錯演奏](../Page/揚琴.md "wikilink")，[拉弦樂器四](../Page/拉弦樂器.md "wikilink")、五度音程滑奏，[古箏碼後刮奏](../Page/古筝.md "wikilink")，低音樂器空五度持續低音伴奏，[定音鼓五度上滑音等音效描繪](../Page/定音鼓.md "wikilink")「清風微拂、祥雲縈繞，淡霧回移、金光閃爍」的天庭。遼闊、明亮的[口笛聲猶如自雲層中透出一般](../Page/口笛.md "wikilink")，飄逸而出。\[4\]

中間的[複三段體樂曲第一段由](../Page/三段体.md "wikilink")[磬和](../Page/磬_\(法器\).md "wikilink")[卜魚的輕擊聲引出](../Page/木鱼.md "wikilink")，在[胡琴帶有流動感的](../Page/胡琴.md "wikilink")[切分顫音伴奏中](../Page/切分音.md "wikilink")，高音笙以其明亮、清脆的音色奏出舒暢舒展、優美動聽的古典曲調。其後主題移至B調羽調式重複，演奏聲部亦隨之增加，氣氛更為活躍。第二段共有兩個主題，在鈴、雲鑼、卜魚襯托下，[彈撥樂奏出活潑](../Page/彈撥樂器.md "wikilink")、歡樂的第一主題，經過中速間奏後，第一主題以彈撥、胡琴的競奏形式再現，接著又突然停止，由古箏以渾厚的中、低音漸快奏出第二主題，多次變化、反覆後進入樂曲第一高潮。中間插入由[管鐘和笙帶入的誦經嚴肅場面](../Page/管鐘.md "wikilink")。第三段為第一段的主題再現，轉到E調羽調式，以吹管樂器高奏主題，彈撥樂器急速掃輪，拉弦樂器切分節奏顫音強奏和打擊樂器的交叉碰擊，展現出氣勢磅礴的天界。\[5\]

尾聲為引子的再現，在弦樂透明的[泛音聲中結束全曲](../Page/泛音.md "wikilink")，猶如眾神與仙女狂舞後逐漸離去。\[6\]

<File:Yunluo> 雲鑼.jpg|雲鑼 <File:Alto> Sheng 中音笙.jpg|中音笙
<File:Pojocaskleda> (2).JPG|磬 [File:Koudi.jpg|口笛](File:Koudi.jpg%7C口笛)
[File:Tubular-bells.JPG|管鐘](File:Tubular-bells.JPG%7C管鐘) <File:Wood>
Block 卜魚.jpg|卜魚 <File:Pengling> player in a Chinese Orchestra.jpg|碰鈴演奏者

## 參考來源

<references />

## 外部連結

  -
[Category:中樂作品](../Category/中樂作品.md "wikilink")

1.

2.

3.
4.
5.
6.