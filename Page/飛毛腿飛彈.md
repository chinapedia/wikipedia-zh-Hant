[Rakieta_wz8K-14_SCUD_RB.jpg](https://zh.wikipedia.org/wiki/File:Rakieta_wz8K-14_SCUD_RB.jpg "fig:Rakieta_wz8K-14_SCUD_RB.jpg")
导弹 wz. 8/K-14 (Scud-B)\]\]
[2P19_9K72.jpg](https://zh.wikipedia.org/wiki/File:2P19_9K72.jpg "fig:2P19_9K72.jpg")

**飞毛腿导弹**（Scud）是一个已经被大众接受了的词汇，指[苏联在](../Page/苏联.md "wikilink")[冷战时期开发并被广泛出口的一系列的](../Page/冷战.md "wikilink")[战术](../Page/战术.md "wikilink")[弹道导弹](../Page/弹道导弹.md "wikilink")。这个名称是从[北約代號为](../Page/北約代號.md "wikilink")**SS-1飞毛腿**得来的，它包括了苏联的从**R-11**、**R-17**、
**R-300厄尔布鲁士**的一系列导弹。飞毛腿这个名字还被媒体指别的国家根据苏联原型广泛发展的许多种导弹。如在美国有时指飞毛腿SCUD被泛指为任何国家的不是从西方原型发展出来的[弹道导弹](../Page/弹道导弹.md "wikilink")。

## 研制

[9P117_9K72.jpg](https://zh.wikipedia.org/wiki/File:9P117_9K72.jpg "fig:9P117_9K72.jpg")
(9P117)发射车与8K14火箭\]\]

第一次「飞毛腿（Scud）」这个词是[北约将R](../Page/北约.md "wikilink")-11[弹道导弹称为](../Page/弹道导弹.md "wikilink")**飞毛腿-A**型（SS-1b）。更早的被[北约称为](../Page/北约.md "wikilink")**SS-1
Scunner**是一个非常不一样的设计，几乎是[德国](../Page/德国.md "wikilink")[V2火箭的直接翻版](../Page/V2火箭.md "wikilink")。[R-11同样采用了V](../Page/R-11.md "wikilink")-2的技术，但它是一个全新的设计，更小，与V-2和R-1的形状不一样。R-11是由[Korolyev
OKB](../Page/S.P._Rocket_and_Space_Corporation_Energia.md "wikilink")\[1\][Makeyev
OKB](../Page/:en:Makeyev_OKB.md "wikilink")
设计并在1957年服役的。R-11最有革命性的创新是[A.M.
Isaev设计的引擎](../Page/Aleksei_Mihailovich_Isaev.md "wikilink")，比V-2的多室设计（multi-chamber
design）远为简单，并且用防震荡折流板（anti-oscillation
baffle）防止间歇燃烧。这是苏联太空火箭所使用的更大的引擎的设计先驱。

后来发展的变种有1961年的**R-300厄尔布鲁士/飞毛腿-B（ SS-1c）**和1965年的**飞毛腿-C（SS-1d
）**，都可以携带5-80千吨的常规高爆的[核武器或](../Page/核武器.md "wikilink")[化学武器](../Page/化学武器.md "wikilink")（加料的[VX](../Page/VX_\(神經毒劑\).md "wikilink")）弹头。1980年代发展的**飞毛腿-D（SS-1e）**可以携带一个常规高爆弹头，一个[燃料空气弹头](../Page/燃料空气炸弹.md "wikilink")，40个反[跑道炸弹或者](../Page/跑道.md "wikilink")100个5千克的人员杀伤性小炸弹。

所有型号都是11.25米长（除了飞毛腿Scud-A，短了一米）直径0.88米。推动引擎是用煤油和硝酸的单引擎（飞毛腿-A）或者用[UDMH和](../Page/UDMH.md "wikilink")[RFNA](../Page/RFNA.md "wikilink")（Russian
SG-02 Tonka 250）的引擎（别的型号）。

最大速度5马赫。

## 型号

### 苏联

#### R-11

1951年，由[Sergey
Korolev领导的](../Page/Sergey_Korolyov.md "wikilink")[OKB-1的工程师](../Page/OKB-1.md "wikilink")[Victor
Makeev开始设计R](../Page/Victor_Makeev.md "wikilink")-11导弹系统。1953年4月18日首飞，使用伊萨耶夫设计的煤油+硝酸作为燃料的引擎。1953年12月13日，位于[Zlatoust的](../Page/Zlatoust.md "wikilink")[SKB-385工厂开始生产该型号](../Page/SKB-385.md "wikilink")。1955年6月，Makeev成为SKB-385的总设计师。1955年7月，R-11正式入役。\[2\]1958年4月1日，携带核弹头的R-11M入役。发射系统型号为8K11。\[3\]

R-11M最大射程270 km，如果携带50kt核弹头则最大射程为150 km.\[4\] \[5\]

潜射型号R-11FM (SS-N-1 Scud-A)于1955年2月在试飞,1955年9月在[Project
611](../Page/Project_611.md "wikilink")
(祖鲁级)潜艇上试射。型号研制于1955年8月转移给Makeev的SKB-385。\[6\]1959年实战部署在611级及[Project
629](../Page/Project_629.md "wikilink") (高尔夫级)弹道导弹常规潜艇.
服役期间，发射77次，其中59次成功。\[7\]

#### R-17

[Wz8K14_RB3.jpg](https://zh.wikipedia.org/wiki/File:Wz8K14_RB3.jpg "fig:Wz8K14_RB3.jpg")
R-11的后继型号是R-17 (SS-1C 飞毛腿-B),
并在1970年代改称R-300，产量估计7000套。服役于32个国家，还有4个国家仿制。\[8\]1961年首飞，1964年入役。\[9\]

R-17可携带常规、核、化学或集束弹头。\[10\]最初使用履带式底盘作为2P19[transporter erector
launcher](../Page/transporter_erector_launcher.md "wikilink") (TEL),
但不久改为Titan Central Design Bureau的轮式底盘，1967年入役。\[11\]
[MAZ-543车辆作为](../Page/MAZ-543.md "wikilink")9P117
*Uragan*.使用液压动力把导弹起竖，这需要4分钟才能发射。\[12\]

#### 飞毛腿-C

Makeyev OKB研制的R-17增程型，西方称为飞毛腿-C，1965年在Kapustin Yar卡普斯京亚尔靶场首飞。,
射程500–600 km，但战斗荷载降低了。当时苏联有更好的替代装备，如[TR-1
Temp](../Page/TR-1_Temp.md "wikilink") (SS-12
Scaleboard),因此飞毛腿-C并没有被苏军列装。\[13\]

#### 飞毛腿-D

R-17 VTO增强了R-17的精度。The Central Scientific Research Institute for
Automation and Hydraulics
(TsNIAAG)1968年开始这一项目。1979年9月首飞。1989年入役，型号为9K720
Aerofon。\[14\] 此时，苏军有了更为先进的[OTR-21
Tochka](../Page/OTR-21_Tochka.md "wikilink") (SS-21) 与[R-400
Oka](../Page/R-400_Oka.md "wikilink") (SS-23), 飞毛腿-D针对出口市场。\[15\]

#### 技术性能

| NATO codename                                             | 飞毛腿-A    | 飞毛腿-B      | 飞毛腿-C    | 飞毛腿-D    |
| --------------------------------------------------------- | -------- | ---------- | -------- | -------- |
| [美国国防情报局](../Page/美国国防情报局.md "wikilink")                  | SS-1b    | SS-1c      | SS-1d    | SS-1e    |
| 研制型号                                                      | R-11     | R-17/R-300 |          | R-17 VTO |
| 入役时间                                                      | 1957     | 1964       | 1965     | 1989     |
| 弹长                                                        | 10.7 m   | 11.25 m    | 11.25 m  | 12.29 m  |
| 弹径                                                        | 0.88 m   | 0.88 m     | 0.88 m   | 0.88 m   |
| 发射重量                                                      | 4,400 kg | 5,900 kg   | 6,400 kg | 6,500 kg |
| 射程                                                        | 180 km   | 300 km     | 550 km   | 1000 km  |
| 载重                                                        | 950 kg   | 985 kg     | 600 kg   | 985 kg   |
| 精度 ([CEP](../Page/Circular_error_probable.md "wikilink")) | 3000 m   | 450 m      | 700 m    | 50 m     |

### 北韓

#### 劳动-1

### 伊拉克

"飞毛腿"这个名字也被用来指同一种导弹经[伊拉克修改的版本](../Page/伊拉克.md "wikilink")。修改后射程增加，当许多导弹向[以色列](../Page/以色列.md "wikilink")(40)和[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")(46)发射时，这一点尤为突出。美国制造的[爱国者导弹系统据说成功地拦截了导弹](../Page/爱国者导弹系统.md "wikilink")，但是许多评论家声称爱国者导弹的作用被严重夸张了，实际上有85%的失败率。这些导弹是伊拉克最有危险的进攻性武器之一，尤其是对于以色列而言。携带生化弹头的可能性也被广泛关注。

最终，飞毛腿导弹直接造成了一名以色列人，以及28名美国士兵（导弹击中了位于沙特阿拉伯首都[利雅得的一座](../Page/利雅得.md "wikilink")[宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink")[国民警卫队兵营](../Page/国民警卫队.md "wikilink")）的死亡。针对飞毛腿导弹的搜索耗用了自愿联盟空军大约三分之一的力量。借助于“运输-起竖-发射”（TEL）三用车，这些导弹可以被非常机动地运输，并且难以被捕捉；美国和英国的特种部队成员经常承担在敌军的防线后方搜索并破坏这些导弹的任务。对于上述集团而言，消除来自飞毛腿导弹的威胁是有着特殊重要性的。正如以色列曾经扬言，如果侵袭继续，便将加入对伊拉克的战争，但这会分裂反以阿拉伯国家组成的攻打伊拉克的联盟，也会大大加重美国和英国的战争代价。

伊拉克发展了四种改型：飞毛腿导弹（Scud）、远程飞毛腿导弹（longer-range Scud或Scud
LR）、[侯赛因](../Page/侯赛因.md "wikilink")（Al
Hussein）和[阿巴斯](../Page/阿巴斯.md "wikilink")（Al
Abbas）。除了那些几乎未经改造的武器之外，这些导弹并不成功，因为它们往往在飞行过程中崩解，并且只能搭载小弹头。

### 伊朗

### 巴基斯坦

## 作战历史

[Scud-launcher-scotland1.jpg](https://zh.wikipedia.org/wiki/File:Scud-launcher-scotland1.jpg "fig:Scud-launcher-scotland1.jpg")
飞毛腿导弹（包括衍生物）是少数在实战中被使用的弹道导弹之一，发射总数仅次于V2火箭（[地堡-U是唯一其他](../Page/SS-21.md "wikilink")“盛怒之下”发射的弹道导弹）。

### 两伊战争

### 阿富汗内战

### 海湾战争

### 1994年也门内战

### 车臣战争

### 利比亚内战

### 叙利亚内战

### 2017也门沙特冲突

## 当前使用国

俄羅斯🇷🇺

阿富汗

亞美利亞

伊朗

伊拉克

利比利亞

朝鮮🇰🇵

埃及

## 以前使用国

蘇聯

阿富汗

亞美利亞

伊朗

伊拉克

利比利亞

朝鮮🇰🇵

埃及

## 参考文献

## 外部链接

  - [Global Security: R-11 / SS-1b
    SCUD](http://www.globalsecurity.org/wmd/world/russia/r-11.htm)
  - [The Scud Missile
    Syndrome](http://www.defencejournal.com/may99/scud-missile.htm)
  - [Капустин Яр](http://kapyar.km.ru/) (俄语)

## 参见

  - [NMD](../Page/NMD.md "wikilink")
  - [沙哈布一型](../Page/沙哈布一型.md "wikilink")（Shahab-1）：伊朗版本的飞毛腿-B
  - [大浦洞-1飛彈](../Page/大浦洞-1飛彈.md "wikilink")、[大浦洞-2飛彈](../Page/大浦洞-2飛彈.md "wikilink")、[火星6型彈道飛彈](../Page/火星6型彈道飛彈.md "wikilink")：北韓以飛毛腿飛彈的技術研發的中長程飛彈

<noinclude>  </noinclude>

[Category:苏联弹道导弹](../Category/苏联弹道导弹.md "wikilink")
[Category:战术弹道导弹](../Category/战术弹道导弹.md "wikilink")
[Category:面对面导弹](../Category/面对面导弹.md "wikilink")

1.

2.

3.  Zaloga, p.7

4.  Zaloga, p.4

5.

6.
7.

8.
9.
10.
11. Zaloga, pp.14-15

12.
13. Zaloga, p.17

14. Zaloga, p.19

15.