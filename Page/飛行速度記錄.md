[Lockheed_SR-71_Blackbird.jpg](https://zh.wikipedia.org/wiki/File:Lockheed_SR-71_Blackbird.jpg "fig:Lockheed_SR-71_Blackbird.jpg")是現時的紀錄保持者。\]\]
**飛行速度記錄**是特定種類飛行器所能達到的最高速度。

所有官方航空記錄都由[國際航空聯合會](../Page/國際航空聯合會.md "wikilink")（FAI）定義並正式通過。飛行記錄被分爲若干個級別和副分類。飛行器被分爲三個種類：陆基飛機，[水上飛機和](../Page/水上飛機.md "wikilink")[水陸兩用飛機](../Page/水陸兩用飛機.md "wikilink")；而在這些級別中，不同重量級有不同的記錄。當中還有副分類：分別由[渦輪噴射引擎](../Page/渦輪噴射引擎.md "wikilink")、[渦輪螺旋槳引擎和](../Page/渦輪螺旋槳引擎.md "wikilink")[火箭發動機推動的飛行器](../Page/火箭發動機.md "wikilink")。這些分類中，不同海拔的飛行速度、飛機的大小和裝載物的重量又分別有不同的記錄。也有特定城市之間的速度記錄，例如[倫敦和](../Page/倫敦.md "wikilink")[紐約](../Page/紐約.md "wikilink")。

## FAI級別

| 級別 | 種類                                                                                                    |
| -- | ----------------------------------------------------------------------------------------------------- |
| A  | [熱氣球](../Page/熱氣球.md "wikilink")                                                                      |
| B  | [飛船](../Page/飛船.md "wikilink")                                                                        |
| C  | [飛機](../Page/飛機.md "wikilink")                                                                        |
| D  | [滑翔機和](../Page/滑翔機.md "wikilink")[動力滑翔飛機](../Page/動力滑翔飛機.md "wikilink")                               |
| E  | [旋翼機](../Page/旋翼機.md "wikilink")                                                                      |
| F  | [模型飛機](../Page/模型飛機.md "wikilink")                                                                    |
| G  | [跳降落傘](../Page/跳降落傘.md "wikilink")                                                                    |
| H  | [垂直升降飛機](../Page/VTOL.md "wikilink")                                                                  |
| I  | [人力飛機](../Page/人力飛機.md "wikilink")                                                                    |
| K  | [太空飛機](../Page/太空飛機.md "wikilink")                                                                    |
| M  | [Tilt-wing / Tilt-engine aircraft](../Page/Tiltrotor.md "wikilink")                                   |
| N  | [短途飛機](../Page/STOL.md "wikilink")                                                                    |
| O  | [Hang gliding](../Page/Hang_gliding.md "wikilink") & [Paragliding](../Page/Paragliding.md "wikilink") |
| P  | [Aerospacecraft](../Page/Aerospacecraft.md "wikilink")                                                |
| R  | [Microlight](../Page/Microlight.md "wikilink")                                                        |
| S  | Space Models                                                                                          |
| U  | [無人航空載具](../Page/無人航空載具.md "wikilink")                                                                |

## 時間綫

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>機師</p></th>
<th><p>時速</p></th>
<th><p>機種</p></th>
<th><p>地點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>英哩</p></td>
<td><p>公里</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1903年</p></td>
<td><p><a href="../Page/萊特兄弟.md" title="wikilink">萊特兄弟</a></p></td>
<td><p>9.80</p></td>
<td><p>15.77</p></td>
<td><p><a href="../Page/萊特飛行器.md" title="wikilink">萊特飛行器</a></p></td>
</tr>
<tr class="odd">
<td><p>1905年</p></td>
<td><p><a href="../Page/萊特兄弟.md" title="wikilink">萊特兄弟</a></p></td>
<td><p>37.85</p></td>
<td><p>60.91</p></td>
<td><p><a href="../Page/萊特飛行器三.md" title="wikilink">萊特飛行器三</a></p></td>
</tr>
<tr class="even">
<td><p>1908年</p></td>
<td><p><a href="../Page/Henry_Farman.md" title="wikilink">Henry Farman</a></p></td>
<td><p>40.26</p></td>
<td><p>64.79</p></td>
<td><p><a href="../Page/Voisin_biplane.md" title="wikilink">Voisin biplane</a></p></td>
</tr>
<tr class="odd">
<td><p>1909年</p></td>
<td><p><a href="../Page/路易·布莱里奥.md" title="wikilink">路易·布莱里奥</a></p></td>
<td><p>47.82</p></td>
<td><p>76.96</p></td>
<td><p><a href="../Page/Blériot_XII.md" title="wikilink">Blériot XII</a></p></td>
</tr>
<tr class="even">
<td><p>1910年</p></td>
<td><p><a href="../Page/Alfred_Leblanc.md" title="wikilink">Alfred Leblanc</a></p></td>
<td><p>68.20</p></td>
<td><p>109.8</p></td>
<td><p><a href="../Page/Blériot_XI.md" title="wikilink">Blériot XI</a></p></td>
</tr>
<tr class="odd">
<td><p>1911年</p></td>
<td><p><a href="../Page/Edouard_Nieuport.md" title="wikilink">Edouard Nieuport</a></p></td>
<td><p>82.73</p></td>
<td><p>133.1</p></td>
<td><p><a href="../Page/Nieuport_Nie-2_N.md" title="wikilink">Nieuport Nie-2 N</a></p></td>
</tr>
<tr class="even">
<td><p>1912年</p></td>
<td><p><a href="../Page/Jules_Vedrines.md" title="wikilink">Jules Vedrines</a></p></td>
<td><p>108.2</p></td>
<td><p>174.1</p></td>
<td><p>Monocoque <a href="../Page/Deperdussin.md" title="wikilink">Deperdussin</a></p></td>
</tr>
<tr class="odd">
<td><p>1913年</p></td>
<td><p><a href="../Page/Maurice_Prevost.md" title="wikilink">Maurice Prevost</a></p></td>
<td><p>126.7</p></td>
<td><p>203.8</p></td>
<td><p>Monocoque Deperdussin</p></td>
</tr>
<tr class="even">
<td><p>1914年</p></td>
<td><p><a href="../Page/Norman_Spratt.md" title="wikilink">Norman Spratt</a></p></td>
<td><p>134.5</p></td>
<td><p>216.5</p></td>
<td><p><a href="../Page/RAF_SE.4.md" title="wikilink">RAF SE.4</a></p></td>
</tr>
<tr class="odd">
<td><p>1918年</p></td>
<td><p><a href="../Page/Roland_Rohlfs.md" title="wikilink">Roland Rohlfs</a></p></td>
<td><p>163.1</p></td>
<td><p>262.4</p></td>
<td><p><a href="../Page/Curtiss_Wasp.md" title="wikilink">Curtiss Wasp</a></p></td>
</tr>
<tr class="even">
<td><p>1919年</p></td>
<td><p><a href="../Page/Joseph_Sadi-Lecointe.md" title="wikilink">Joseph Sadi-Lecointe</a></p></td>
<td><p>191.1</p></td>
<td><p>307.5</p></td>
<td><p><a href="../Page/Nieuport-Delage_29v.md" title="wikilink">Nieuport-Delage 29v</a></p></td>
</tr>
<tr class="odd">
<td><p>1920年</p></td>
<td><p>Joseph Sadi-Lecointe</p></td>
<td><p>194.5</p></td>
<td><p>313.0</p></td>
<td><p>Nieuport-Delage 29v</p></td>
</tr>
<tr class="even">
<td><p>1921年</p></td>
<td><p>Joseph Sadi-Lecointe</p></td>
<td><p>205.2</p></td>
<td><p>330.3</p></td>
<td><p>Nieuport-Delage</p></td>
</tr>
<tr class="odd">
<td><p>1922年</p></td>
<td><p><a href="../Page/威廉·米切尔.md" title="wikilink">威廉·米切尔</a></p></td>
<td><p>224.3</p></td>
<td><p>360.9</p></td>
<td><p><a href="../Page/Curtiss_R-6.md" title="wikilink">Curtiss R-6</a></p></td>
</tr>
<tr class="even">
<td><p>1923年</p></td>
<td><p><a href="../Page/Alford_J._Williams.md" title="wikilink">Alford J. Williams</a></p></td>
<td><p>267.2</p></td>
<td><p>430.0</p></td>
<td><p><a href="../Page/Curtiss_R2C-1.md" title="wikilink">Curtiss R2C-1</a></p></td>
</tr>
<tr class="odd">
<td><p>1924年</p></td>
<td><p><a href="../Page/Florentin_Bonnet.md" title="wikilink">Florentin Bonnet</a></p></td>
<td><p>278.5</p></td>
<td><p>448.2</p></td>
<td><p><a href="../Page/Bernard_Ferbois_V2.md" title="wikilink">Bernard Ferbois V2</a></p></td>
</tr>
<tr class="even">
<td><p>1927年</p></td>
<td><p><a href="../Page/Mario_de_Bernardi.md" title="wikilink">Mario de Bernardi</a></p></td>
<td><p>297.8</p></td>
<td><p>479.3</p></td>
<td><p><a href="../Page/Macchi_M.52.md" title="wikilink">Macchi M.52</a></p></td>
</tr>
<tr class="odd">
<td><p>1928年</p></td>
<td><p>Mario de Bernardi</p></td>
<td><p>318.6</p></td>
<td><p>512.7</p></td>
<td><p><a href="../Page/Macchi_M.52bis.md" title="wikilink">Macchi M.52bis</a></p></td>
</tr>
<tr class="even">
<td><p>1929年</p></td>
<td><p><a href="../Page/Giuseppe_Motta.md" title="wikilink">Giuseppe Motta</a></p></td>
<td><p>362.0</p></td>
<td><p>582.6</p></td>
<td><p><a href="../Page/Macchi_M.67.md" title="wikilink">Macchi M.67</a></p></td>
</tr>
<tr class="odd">
<td><p>1931年</p></td>
<td><p><a href="../Page/George_Stainforth.md" title="wikilink">George H. Stainforth</a></p></td>
<td><p>407.5</p></td>
<td><p>655.8</p></td>
<td><p><a href="../Page/Supermarine_S.6B.md" title="wikilink">Supermarine S.6B</a> seaplane</p></td>
</tr>
<tr class="even">
<td><p>1933年</p></td>
<td><p><a href="../Page/Francesco_Agello.md" title="wikilink">Francesco Agello</a></p></td>
<td><p>424</p></td>
<td><p>682</p></td>
<td><p><a href="../Page/Macchi_M.C.72.md" title="wikilink">Macchi M.C.72</a></p></td>
</tr>
<tr class="odd">
<td><p>1934年</p></td>
<td><p><a href="../Page/Francesco_Agello.md" title="wikilink">Francesco Agello</a></p></td>
<td><p>440.6</p></td>
<td><p>709.0</p></td>
<td><p><a href="../Page/Macchi_M.C.72.md" title="wikilink">Macchi M.C.72</a></p></td>
</tr>
<tr class="even">
<td><p>1939年</p></td>
<td><p><a href="../Page/Fritz_Wendel.md" title="wikilink">Fritz Wendel</a></p></td>
<td><p>469.22</p></td>
<td><p>755.13</p></td>
<td><p><a href="../Page/Me_209戰鬥機.md" title="wikilink">Me 209戰鬥機</a></p></td>
</tr>
<tr class="odd">
<td><p>1941年</p></td>
<td><p><a href="../Page/Heini_Dittmar.md" title="wikilink">Heini Dittmar</a></p></td>
<td><p>623.65</p></td>
<td><p>1003.67</p></td>
<td><p><a href="../Page/Me_163戰鬥機.md" title="wikilink">Me 163戰鬥機AV</a>4</p></td>
</tr>
<tr class="even">
<td><p>1944年</p></td>
<td><p><a href="../Page/Heinz_Herlitzius.md" title="wikilink">Heinz Herlitzius</a></p></td>
<td><p>624</p></td>
<td><p>1004</p></td>
<td><p><a href="../Page/Me_262戰鬥機.md" title="wikilink">Me 262戰鬥機S</a>2</p></td>
</tr>
<tr class="odd">
<td><p>1944年</p></td>
<td><p><a href="../Page/Heini_Dittmar.md" title="wikilink">Heini Dittmar</a></p></td>
<td><p>702</p></td>
<td><p>1130</p></td>
<td><p><a href="../Page/Me_163戰鬥機.md" title="wikilink">Me 163戰鬥機BV</a>18</p></td>
</tr>
<tr class="even">
<td><p>1945年</p></td>
<td><p><a href="../Page/H._J._Wilson.md" title="wikilink">H. J. Wilson</a></p></td>
<td><p>606.4</p></td>
<td><p>975.9</p></td>
<td><p><a href="../Page/Gloster_Meteor.md" title="wikilink">Gloster Meteor F Mk4</a></p></td>
</tr>
<tr class="odd">
<td><p>1946年</p></td>
<td><p><a href="../Page/Edward_Donaldson.md" title="wikilink">Edward Donaldson</a></p></td>
<td><p>615.78</p></td>
<td><p>990.79</p></td>
<td><p><a href="../Page/Gloster_Meteor.md" title="wikilink">Gloster Meteor F Mk4</a></p></td>
</tr>
<tr class="even">
<td><p>1947年</p></td>
<td><p><a href="../Page/Col._Andrew_Boyd.md" title="wikilink">Col. Andrew Boyd</a></p></td>
<td><p>623.74</p></td>
<td><p>1,003.60</p></td>
<td><p><a href="../Page/F-80戰鬥機.md" title="wikilink">F-80戰鬥機</a></p></td>
</tr>
<tr class="odd">
<td><p>1947年</p></td>
<td><p><a href="../Page/查克·葉格.md" title="wikilink">查克·葉格</a></p></td>
<td><p>670.0</p></td>
<td><p>1078</p></td>
<td><p><a href="../Page/X-1試驗機.md" title="wikilink">X-1試驗機</a></p></td>
</tr>
<tr class="even">
<td><p>1948年</p></td>
<td><p><a href="../Page/Maj._Richard_L._Johnson,_USAF.md" title="wikilink">Maj. Richard L. Johnson, USAF</a></p></td>
<td><p>670.84</p></td>
<td><p>1079.6</p></td>
<td><p><a href="../Page/F-86軍刀戰鬥機.md" title="wikilink">F-86軍刀戰鬥機</a></p></td>
</tr>
<tr class="odd">
<td><p>1953年</p></td>
<td><p><a href="../Page/Neville_Duke.md" title="wikilink">Neville Duke</a></p></td>
<td><p>727.6</p></td>
<td><p>1,171</p></td>
<td><p><a href="../Page/Hawker_Hunter.md" title="wikilink">Hawker Hunter F Mk3</a></p></td>
</tr>
<tr class="even">
<td><p>1953年</p></td>
<td><p><a href="../Page/Mike_Lithgow.md" title="wikilink">Mike Lithgow</a></p></td>
<td><p>735.7</p></td>
<td><p>1,184</p></td>
<td><p><a href="../Page/Supermarine_Swift.md" title="wikilink">Supermarine Swift</a> F4</p></td>
</tr>
<tr class="odd">
<td><p><em>從這裡開始，記錄是設定在高空，而不是在海平面</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1955年</p></td>
<td><p><a href="../Page/Horace_A._Hanes.md" title="wikilink">Horace A. Hanes</a></p></td>
<td><p>822.1</p></td>
<td><p>1,323</p></td>
<td><p>F-100C <a href="../Page/F-100_Super_Sabre.md" title="wikilink">Super Sabre</a></p></td>
</tr>
<tr class="odd">
<td><p>1956年</p></td>
<td><p><a href="../Page/Peter_Twiss.md" title="wikilink">Peter Twiss</a></p></td>
<td><p>1,132</p></td>
<td><p>1,822</p></td>
<td><p><a href="../Page/Fairey_Delta_2.md" title="wikilink">Fairey Delta 2</a></p></td>
</tr>
<tr class="even">
<td><p>1959年</p></td>
<td><p>Col. <a href="../Page/Georgii_Mosolov.md" title="wikilink">Georgii Mosolov</a></p></td>
<td><p>1,484</p></td>
<td><p>2,388</p></td>
<td><p>Ye-66（proto <a href="../Page/MiG-21.md" title="wikilink">MiG-21</a>）</p></td>
</tr>
<tr class="odd">
<td><p>1965年</p></td>
<td><p><a href="../Page/Robert_L._Stephens.md" title="wikilink">Robert L. Stephens</a><br />
and <a href="../Page/Daniel_Andre.md" title="wikilink">Daniel Andre</a></p></td>
<td><p>2,070</p></td>
<td><p>3,332</p></td>
<td><p><a href="../Page/YF-12戰鬥機.md" title="wikilink">YF-12戰鬥機</a></p></td>
</tr>
<tr class="even">
<td><p>1976年</p></td>
<td><p><a href="../Page/Eldon_W._Joersz.md" title="wikilink">Eldon W. Joersz</a></p></td>
<td><p>2,194</p></td>
<td><p>3,530</p></td>
<td><p><a href="../Page/SR-71黑鳥式偵察機.md" title="wikilink">SR-71黑鳥式偵察機</a></p></td>
</tr>
</tbody>
</table>

## 官方與非官方記錄

[洛歇SR-71黑鳥式偵察機戰鬥機以每小時](../Page/SR-71.md "wikilink")3,529.56公里（每小時2,194英里）保持了最快的有人駕駛的進氣式（airbreathing）噴射飛機官方飛行速度紀錄。它能夠在常規跑道上無輔助地起飛和降落。紀錄是Eldon
W.
Joersz於1976年7月18日在[美國](../Page/美國.md "wikilink")[加州Beale空軍基地附近區域創下的](../Page/加州.md "wikilink")。\[1\]

經過一段很長的時間，在緊接著的第二次世界大戰期間，由[Messerschmitt Me
163](../Page/Me_163戰鬥機.md "wikilink")
AV4（第三型）火箭飛機於1941年10月2日創下的1004.5
km/h（623.8
mph）紀錄雖然是未公開非官方的，卻其實是當時世上任何飛行器中所測量到的最快速度。這個在戰時沒有確認記錄的條件下創造的數據，是Me
163A V4由[Bf
110戰鬥機拖動後在接近海平面的高度創下的](../Page/Bf_110戰鬥機.md "wikilink")。二戰後，出現了很多新記錄的聲明，如[Gloster
Meteor超越了戰前記錄保持者](../Page/Gloster_Meteor.md "wikilink")（[Me 209
V1活塞式戰鬥機](../Page/Me_209戰鬥機.md "wikilink")）的755 km/h (469
mph)的速度記錄，但是真正超越 Me 163 A V4的是[Douglas
Skystreak](../Page/Douglas_Skystreak.md "wikilink")，創立於1947年8月20日。

當今世上最快的載人大氣層飛行器是[阿波羅指令艙](../Page/阿波羅指令艙.md "wikilink")。當時它從月球返回地球，速度達到30馬赫左右。雖然主要靠空氣制動，它還是達到了0.368左右的[升阻比](../Page/升阻比.md "wikilink")\[2\]可以控制飛行軌道。然而它似乎有別於正常人們心目中對「飛行器」的概念，而且大於或等於1的升阻比按理說是可以認定為飛行的底線。

## 其他飛行速度記錄

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>機師</p></th>
<th><p>速度</p></th>
<th><p>機種</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>mph</p></td>
<td><p>km/h</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1960年</p></td>
<td><p>Ivan Soukhomline（蘇聯）</p></td>
<td><p>599</p></td>
<td><p>920</p></td>
<td><p><a href="../Page/圖-95.md" title="wikilink">圖-95</a></p></td>
</tr>
<tr class="odd">
<td><p>1967年</p></td>
<td><p>William Joseph Knight</p></td>
<td><p>4,510</p></td>
<td><p>7,258</p></td>
<td><p><a href="../Page/X-15試驗機.md" title="wikilink">X-15試驗機</a></p></td>
</tr>
<tr class="even">
<td><p>1981年-今</p></td>
<td><p>多人</p></td>
<td><p>17,500</p></td>
<td><p>28,000</p></td>
<td><p><a href="../Page/太空梭.md" title="wikilink">太空梭</a></p></td>
</tr>
<tr class="odd">
<td><p>1986年8月11日</p></td>
<td><p>John Egginton</p></td>
<td><p>249.1</p></td>
<td><p>401.0</p></td>
<td><p><a href="../Page/韦斯特兰山猫直升机.md" title="wikilink">山猫直升机 <em>G-LYNX</em></a></p></td>
</tr>
<tr class="even">
<td><p>1988年12月31日</p></td>
<td><p>L.P. Krantov</p></td>
<td><p>258.8</p></td>
<td><p>415</p></td>
<td><p><a href="../Page/圖-134.md" title="wikilink">圖-134</a></p></td>
</tr>
<tr class="odd">
<td><p>1989年8月21日</p></td>
<td><p>Lyle Shelton</p></td>
<td><p>528</p></td>
<td><p>850</p></td>
<td><p><a href="../Page/Rare_Bear.md" title="wikilink">F8F Bearcat <em>Rare Bear</em></a></p></td>
</tr>
<tr class="even">
<td><p>2006年12月22日</p></td>
<td><p>Klaus Ohlmann<br />
Matias Garcia Mazzaro</p></td>
<td><p>190.6</p></td>
<td><p>306.8</p></td>
<td><p><a href="../Page/Schempp-Hirth_Nimbus-4.md" title="wikilink">Schempp-Hirth Nimbus-4DM</a></p></td>
</tr>
<tr class="odd">
<td><p>2004年12月16日</p></td>
<td><p>無人</p></td>
<td><p>7,546</p></td>
<td><p>12,144</p></td>
<td><p><a href="../Page/X-43A試驗機.md" title="wikilink">X-43A試驗機</a></p></td>
</tr>
</tbody>
</table>

## 參見

  - [世界之最列表](../Page/世界之最列表.md "wikilink")
  - [Lockheed_X-7](../Page/Lockheed_X-7.md "wikilink")
    4.31馬赫（2,881mph）於1950年代

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [web
    site](https://web.archive.org/web/20061206205339/http://records.fai.org/general_aviation/)
    of the [Fédération Aéronautique
    Internationale](../Page/Fédération_Aéronautique_Internationale.md "wikilink")（FAI）
  - [Speed records time
    line](https://web.archive.org/web/20071013142357/http://speedrecordclub.com/records/outair.htm)

[Category:交通之最](../Category/交通之最.md "wikilink")
[Category:航空](../Category/航空.md "wikilink")

1.
2.  [Hillje, Ernest R., "Entry Aerodynamics at Lunar Return Conditions
    Obtained from the Flight of Apollo 4 (AS-501)," NASA TN
    D-5399,（1969）.](http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19690029435_1969029435.pdf)