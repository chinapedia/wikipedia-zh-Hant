《**飞越疯人院**》（[英语](../Page/英语.md "wikilink")：），1975年上映的一部[美国电影](../Page/美国电影.md "wikilink")，第2部囊括五项奥斯卡至尊奖项的电影（至今共3部），世界电影艺术历史上最经典的美国巨著之一，被称为“影视表演的必修课”。

剧本改编自一部，曾获1975年第48届[奥斯卡最佳影片](../Page/奥斯卡.md "wikilink")，最佳男、女主角，最佳导演和最佳改编剧本。據導演[米洛斯·福曼所說](../Page/米洛斯·福曼.md "wikilink")，這部電影其實是從他由[布拉格之春之後](../Page/布拉格之春.md "wikilink")[鐵幕](../Page/鐵幕.md "wikilink")[捷克逃到美國的個人經歷而改編的](../Page/捷克.md "wikilink")。這次經歷，使他明白到任何的夢想的實現機會可能不高，但若連這個機會都放棄的話，這個夢想可實現的機會就只有零。而這種精神，也非常切合美國人一向提倡的美國精神。

## 外部链接

  -
{{-}}

[Category:1975年電影](../Category/1975年電影.md "wikilink")
[Category:英语电影](../Category/英语电影.md "wikilink")
[Category:美國劇情片](../Category/美國劇情片.md "wikilink")
[Category:1970年代劇情片](../Category/1970年代劇情片.md "wikilink")
[Category:美国小说改编电影](../Category/美国小说改编电影.md "wikilink")
[Category:健康照護題材電影](../Category/健康照護題材電影.md "wikilink")
[Category:精神病題材電影](../Category/精神病題材電影.md "wikilink")
[Category:俄勒岡州取景電影](../Category/俄勒岡州取景電影.md "wikilink")
[Category:米洛斯·福曼电影](../Category/米洛斯·福曼电影.md "wikilink")
[Category:独立电影](../Category/独立电影.md "wikilink")
[Category:奥斯卡最佳影片](../Category/奥斯卡最佳影片.md "wikilink")
[Category:英国电影学院奖最佳影片](../Category/英国电影学院奖最佳影片.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")
[Category:奧斯卡最佳导演獲獎電影](../Category/奧斯卡最佳导演獲獎電影.md "wikilink")
[Category:奧斯卡最佳改编剧本獲獎電影](../Category/奧斯卡最佳改编剧本獲獎電影.md "wikilink")
[Category:奧斯卡最佳男主角獲獎電影](../Category/奧斯卡最佳男主角獲獎電影.md "wikilink")
[Category:奧斯卡最佳女主角獲獎電影](../Category/奧斯卡最佳女主角獲獎電影.md "wikilink")
[Category:金球獎最佳戲劇類女主角獲獎電影](../Category/金球獎最佳戲劇類女主角獲獎電影.md "wikilink")
[Category:金球獎最佳戲劇類男主角獲獎電影](../Category/金球獎最佳戲劇類男主角獲獎電影.md "wikilink")
[Category:英国电影学院奖最佳女主角获奖电影](../Category/英国电影学院奖最佳女主角获奖电影.md "wikilink")
[Category:英国电影学院奖最佳男主角获奖电影](../Category/英国电影学院奖最佳男主角获奖电影.md "wikilink")