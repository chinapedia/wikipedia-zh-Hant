**飞马座**（別名**天馬座**）的大四边形是[秋季星空中北天区中最耀眼的星象](../Page/秋季.md "wikilink")，整个这片天区远离[银河系的](../Page/银河系.md "wikilink")[银盘](../Page/银盘.md "wikilink")。所以布满了明暗各异的[星系](../Page/星系.md "wikilink")。这里有一个[梅西耶天体](../Page/梅西耶天体.md "wikilink")，即[球状星团](../Page/球状星团.md "wikilink")[M15](../Page/M15.md "wikilink")。

## 神话和历史

在[希腊神话中](../Page/希腊神话.md "wikilink")，[珀尔修斯杀死了](../Page/珀尔修斯.md "wikilink")[戈耳工](../Page/戈耳工.md "wikilink")[美杜莎後](../Page/美杜莎.md "wikilink")，从美杜莎颈腔喷出的血中跳出了一匹飞马[珀伽索斯](../Page/珀伽索斯.md "wikilink")。这匹长著翅膀的神马立刻飞到了天上，后来它降落到[赫利孔山上](../Page/赫利孔山.md "wikilink")，在那里创造了[灵泉](../Page/灵泉.md "wikilink")，这[泉後来成了](../Page/泉.md "wikilink")[诗人的灵感之源](../Page/诗人.md "wikilink")。[雅典娜後来驯服了这匹马](../Page/雅典娜.md "wikilink")，把它赠给[柏勒洛丰](../Page/柏勒洛丰.md "wikilink")，让他骑着这匹马旅行。在一次旅行中，飞马将柏勒洛丰甩下摔死，独自继续旅行，最後到达了[天界](../Page/天界.md "wikilink")，成为了一个[星座](../Page/星座.md "wikilink")。

## 有趣的星

  - [室宿二](../Page/室宿二.md "wikilink")（飞马座β）：这是一颗不规则[变星](../Page/变星.md "wikilink")，[星等变化在](../Page/星等.md "wikilink")+2.2到+2.75间

## 深空天体

  - [M15](../Page/M15.md "wikilink")：这是[球状星团家族中一颗细致](../Page/球状星团.md "wikilink")，明亮的代表，值得注意的是它那非常紧密的核。即使使用最小的[望远镜也可以看见它](../Page/望远镜.md "wikilink")，用8英寸（20厘米）的望远镜可以看清它的外围区域内的数百颗明亮的[恒星](../Page/恒星.md "wikilink")。它的星等是+6.4，[直径是](../Page/直径.md "wikilink")6.8[弧分](../Page/弧分.md "wikilink")。
  - [NGC
    7331](../Page/NGC_7331.md "wikilink")：这个[漩涡星系看起来好像是](../Page/漩涡星系.md "wikilink")[仙女座](../Page/仙女座.md "wikilink")[M31的一个遥远的兄弟](../Page/M31.md "wikilink")，它的星等是+9.5，使用8厘米的望远镜可以隐约看见，而使用8英寸的望远镜可以看得更为有趣。
  - [NGC
    7479](../Page/NGC_7479.md "wikilink")：这是[哈勃棒旋星系中SBb型的代表](../Page/哈勃棒旋星系.md "wikilink")，尽管使用小型仪器不太容易看见它。用20厘米的望远镜才能看清，它是一个直棒，中间有一个明亮的核。
  - [NGC
    7814](../Page/NGC_7814.md "wikilink")：这是一个边缘对着[地球的相当明亮的Sa型或Sb型旋臂](../Page/地球.md "wikilink")[星系](../Page/星系.md "wikilink")，20厘米的望远镜可以看的很清楚，它的星等是+11.4。
  - [斯蒂芬五重奏星系](../Page/斯蒂芬五重奏.md "wikilink")：这是[星群以它](../Page/星群.md "wikilink")19世纪的发现者[法国](../Page/法国.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")[埃杜瓦·斯蒂芬的名字命名的](../Page/埃杜瓦·斯蒂芬.md "wikilink")，斯蒂芬五重奏星系是一个紧密的[星系群中的五个显著的成员](../Page/星系群.md "wikilink")，这个星系群还有六个较暗的成员（[NGC
    7320C](../Page/NGC_7320C.md "wikilink")）。这些星系都是+14等或更暗的星系，使用20厘米望远镜可以看见一两戈较明亮的成员。
  - [飛馬座IK](../Page/飛馬座IK.md "wikilink")

## 另見

  - [飞马座恒星列表](../Page/飞马座恒星列表.md "wikilink")

## 備註

## 參考來源

## 外部連結

[飞马座](../Category/飞马座.md "wikilink")
[Fei1](../Category/星座.md "wikilink")
[Category:托勒密星座](../Category/托勒密星座.md "wikilink")
[Category:北天星座](../Category/北天星座.md "wikilink")