[Lecithin-Formulierungen.jpg](https://zh.wikipedia.org/wiki/File:Lecithin-Formulierungen.jpg "fig:Lecithin-Formulierungen.jpg")

**食品添加剂**是为了保持味道或增强口感、改善外观添加到食物中的物质。
一些添加剂已经使用了几个世纪；例如，（用[醋](../Page/醋.md "wikilink")）[腌制](../Page/醃.md "wikilink")、[盐腌来保存食物](../Page/食盐.md "wikilink")（如[醃肉](../Page/醃肉.md "wikilink")），[糖果的保存以及用](../Page/糖果.md "wikilink")[二氧化硫来保存](../Page/二氧化硫.md "wikilink")[葡萄酒](../Page/葡萄酒.md "wikilink")。随着二十世纪下半叶加工食品的出现，引入了越来越多的天然和人工合成的添加剂。

## 产业状况

  - 据统计，全球食品业每年花在食品添加剂商的[投资高达](../Page/投资.md "wikilink")200亿美元。
  - 平均每人每年要吃进6至7公斤的食品添加剂。

## 管理

在20世纪50年代以前，食品添加剂採用負面表列的方式，凡是不在明令禁止的名单上，就可以添加到食品中。现在大多数国家都规定，只有获得许可的添加剂才能在食品中使用。根據食品藥物管理局發布之[食品添加物使用範圍及限量](http://consumer.fda.gov.tw/Files/food/law/%E9%99%84%E8%A1%A8%E4%B8%80%20%20%E9%A3%9F%E5%93%81%E6%B7%BB%E5%8A%A0%E7%89%A9%E4%BD%BF%E7%94%A8%E7%AF%84%E5%9C%8D%E5%8F%8A%E9%99%90%E9%87%8F_1020531.pdf)，其中規定了各類食品添加物在食品中所佔比例，例如[己二烯酸](../Page/己二烯酸.md "wikilink")（Sorbic
Acid），則逐一列出可使用於魚肉煉製品、肉製品、海膽、魚子醬、花生醬、醬菜類、水分含量25％以上（含25％）之蘿蔔乾、醃漬蔬菜、豆皮豆乾類及乾酪；並且也將用量數據化，例如"以Sorbic
Acid計為2.0g/kg以下"。

### 各地相關法規

  - ：

  - ：食品添加物使用範圍及限量暨規格標準\[1\]

  - ：《食品添加劑使用標準》（GB2760-2011）\[2\]

### 國際標準

  - [國際標準組織曾針對食品添加剂制定了相關標準於ICS](../Page/國際標準組織.md "wikilink")
    67.220。\[3\]
  - [国际食品法典委员会制定食品添加物的國際編碼系統](../Page/国际食品法典委员会.md "wikilink")
  - 1983年，[欧盟采用](../Page/欧盟.md "wikilink")[E编码](../Page/E编码.md "wikilink")（E-number）系统，将几百种获得使用的添加剂列入许可名单。只有在其对人体[健康的影响经过测试后](../Page/健康.md "wikilink")，才能被列入E编码系统。

## 争议

### 作用

  - 贊成方認為：使用食品添加剂是为了防止[食物變質](../Page/食物.md "wikilink")，保護[消费者免遭](../Page/消费者.md "wikilink")[食物中毒](../Page/食物中毒.md "wikilink")。
  - 反对方認為：用于阻止食物变质的添加剂只占食品添加剂总量的1%。大约90%的食品添加剂是装饰性的，这其中有染色的，[甜味剂](../Page/甜味剂.md "wikilink")，及[食用香料](../Page/食用香料.md "wikilink")。此外还有[乳化剂](../Page/乳化剂.md "wikilink")，稳定剂之类的食品加工助剂。

### 安全

  - 在允许使用的添加剂中，有很多人有异议。如在[欧盟](../Page/欧盟.md "wikilink")，有人认为欧盟对食品[香料的管理不如对其它食品添加剂那样严](../Page/香料.md "wikilink")。目前多数国家因在食品中香料加入非常少，所以对食用香料不采取逐个检测安全性的做法。这引起了一些人的不满。
  - 对于国家卫生部门的检测方法也有人批评，他们认为几乎所有的检测都是在[老鼠身上进行实验](../Page/老鼠.md "wikilink")，但实验结果是否同样适用于人还是个未知数。
  - 另外还有人对大多数相關研究是由生产食品添加剂的公司进行的，这一状况不满，怀疑研究结果的公正性。

### 趨勢

2010年間自英國通路商發起自主推廣clean
label，少添加甚至無添加的產品標誌蔚為風潮；然而多數消費者並不具備判斷標示屬實與否的能力，因此而起的食品詐欺儼然是食品衛生安全管理的重要議題，國際間也陸續成立國際或是區域層級的潔淨飲食推廣組織，其中以亞太無添加餐飲食品發展協會(Anti
Additive Association)最具規模。

## 食品添加剂的種類

食品添加劑的目的主要為：延長保存期限用、視覺調整、味覺調整、改善食品品質、提高營養價值及方便製造等。其按照功能性，可大致分為十七種：\[4\]\[5\]

  - [防腐劑](../Page/防腐劑.md "wikilink")（在部分情況會稱為**品質改良劑**，常被添加在即食類食品，例如[便當](../Page/便當.md "wikilink")、[麵包](../Page/麵包.md "wikilink")、[麵條](../Page/麵條.md "wikilink")）

<!-- end list -->

  -
    例如：[亚硝酸钠](../Page/亚硝酸钠.md "wikilink")、[亞硝酸](../Page/亞硝酸.md "wikilink")、[己二烯酸鉀](../Page/山梨酸鉀.md "wikilink")、[己二烯酸](../Page/己二烯酸.md "wikilink")、[丙酸钠](../Page/丙酸钠.md "wikilink")、[对羟基苯甲酸酯](../Page/对羟基苯甲酸酯.md "wikilink")（尼泊金酯）

<!-- end list -->

  - 殺菌劑
  - [抗氧化劑](../Page/抗氧化劑.md "wikilink")
  - [漂白劑](../Page/漂白劑.md "wikilink")
  - 保色劑（限用於魚類或肉類的加工品）

<!-- end list -->

  -
    例如：[亚硝酸钠](../Page/亚硝酸钠.md "wikilink")、[亞硝酸鉀](../Page/亞硝酸鉀.md "wikilink")、[硝酸钠](../Page/硝酸钠.md "wikilink")、[硝酸鉀](../Page/硝酸鉀.md "wikilink")

<!-- end list -->

  - [膨鬆劑](../Page/膨鬆劑.md "wikilink")（膨脹劑）

<!-- end list -->

  -
    例如：[銨粉](../Page/銨粉.md "wikilink")、[小蘇打](../Page/小蘇打.md "wikilink")、[酵母粉](../Page/酵母.md "wikilink")

<!-- end list -->

  - 品質改良用、釀造用及食品製造用劑
  - [營養添加劑](../Page/營養.md "wikilink")

<!-- end list -->

  -
    例如：各式[維生素](../Page/維生素.md "wikilink")

<!-- end list -->

  - 著色劑

<!-- end list -->

  -
    例如：[食用色素](../Page/食用色素.md "wikilink")、[起雲劑](../Page/起雲劑.md "wikilink")

<!-- end list -->

  - [香料](../Page/香料.md "wikilink")
  - [調味劑](../Page/調味劑.md "wikilink")

:\* 酸味劑、[酸度调节剂](../Page/酸度调节剂.md "wikilink")（PH值調整劑）

::例如：[檸檬酸](../Page/檸檬酸.md "wikilink")、[蘋果酸](../Page/蘋果酸.md "wikilink")、[乳酸](../Page/乳酸.md "wikilink")、[乙酸](../Page/乙酸.md "wikilink")（醋酸）

:\* 苦味劑

::常用於[可樂和](../Page/可樂.md "wikilink")[瓜拿納飲料](../Page/瓜拿納.md "wikilink")，例如：[咖啡鹼](../Page/咖啡鹼.md "wikilink")、[柚苷](../Page/柚皮苷.md "wikilink")

:\* 胺基酸系統（[鮮味劑](../Page/鮮味劑.md "wikilink")）

  -

      -
        例如：[味精](../Page/味精.md "wikilink")、[L-麩胺酸鈉](../Page/L-麩胺酸鈉.md "wikilink")

<!-- end list -->

  - [甜味剂](../Page/甜味剂.md "wikilink")(102年1月修正發布[甜味劑由原](../Page/甜味劑.md "wikilink")[調味劑延伸為第十一類之一](../Page/調味劑.md "wikilink"))

<!-- end list -->

  -
    例如：[葡萄糖](../Page/葡萄糖.md "wikilink")、[寡糖](../Page/寡糖.md "wikilink")、[果糖](../Page/果糖.md "wikilink")、[麥芽糖](../Page/麥芽糖.md "wikilink")
      - 人工甜味剂（或稱人工甘味剂）
    <!-- end list -->
      -
        例如：[阿斯巴甜](../Page/阿斯巴甜.md "wikilink")、[糖精](../Page/糖精.md "wikilink")、[蔗糖素](../Page/三氯蔗糖.md "wikilink")、[醋磺內脂鉀](../Page/乙醯磺胺酸鉀.md "wikilink")、[甘草醇](../Page/甘草醇.md "wikilink")、[安赛蜜](../Page/安赛蜜.md "wikilink")
    <!-- end list -->
      - 天然甜味剂(取自草植)
    <!-- end list -->
      -
        例如:[甜菊苷](../Page/甜菊苷.md "wikilink")、[甘草素等](../Page/甘草素.md "wikilink")
    <!-- end list -->
      - 转化甜味剂(半天然)
    <!-- end list -->
      -
        例如:[山梨糖醇](../Page/山梨糖醇.md "wikilink")、[麦芽糖醇](../Page/麦芽糖醇.md "wikilink")、[木糖醇等](../Page/木糖醇.md "wikilink")

<!-- end list -->

  - 黏稠劑

<!-- end list -->

  -
    例如：[澄粉](../Page/澄粉.md "wikilink")、[蛋白](../Page/蛋白.md "wikilink")、[卡德蘭膠](../Page/卡德蘭膠.md "wikilink")、[羧甲基纖維素](../Page/羧甲基纖維素.md "wikilink")（CMC）、[海藻酸鈉](../Page/海藻酸鈉.md "wikilink")、[鹿角菜膠](../Page/鹿角菜膠.md "wikilink")、羧甲基纖維素鈉

<!-- end list -->

  - 結著劑
  - 食品工業用化學藥品

:\*[蛋白質分解劑](../Page/蛋白質分解劑.md "wikilink")

  -

      -
        例如：[鹽酸](../Page/鹽酸.md "wikilink")

<!-- end list -->

  - 溶劑

<!-- end list -->

  -
    例如：[丙二醇](../Page/丙二醇.md "wikilink")、[甘油](../Page/甘油.md "wikilink")

<!-- end list -->

  - [乳化劑](../Page/乳化劑.md "wikilink")
      -
        [脂肪酸甘油酯](../Page/脂肪酸甘油酯.md "wikilink")
  - 其他

:\* [化製澱粉](../Page/化製澱粉.md "wikilink")（常被歸類為粘稠劑）

::例如：酸化製澱粉、糊化澱粉、漂白澱粉、氧化澱粉、醋酸澱粉、磷酸澱粉

:\* 水分控制劑

::例如：[山梨醇](../Page/山梨醇.md "wikilink")、[乳酸](../Page/乳酸.md "wikilink")、[甘油](../Page/甘油.md "wikilink")、[己六醇](../Page/己六醇.md "wikilink")、[丙二醇](../Page/丙二醇.md "wikilink")

:\* 消泡劑

  -

      -
        例如：[矽樹脂](../Page/矽樹脂.md "wikilink")（Silicon Resin）

## 比较

下表列出食品添加剂，并介绍它们的功能分类、中文名称（英文名）、化学名/分子式、天然与否、典型食品、历史/价位/备注等。

### 方便食品

[方便食品由于保质期和商家促销的需要](../Page/方便食品.md "wikilink")，普遍会使用食品添加剂，比如[方便面和](../Page/方便面.md "wikilink")[火腿肠中广泛使用防腐剂](../Page/火腿肠.md "wikilink")、色素等，是人民群众最容易接触到的食品添加剂。

### 防腐剂

  - [亚硝酸钠](../Page/亚硝酸钠.md "wikilink")（Sodium_nitrite），别名250号添加剂，人工合成，应用广泛，历史悠久，
    NaNO<sub>2</sub>。
  - [山梨酸钾](../Page/山梨酸钾.md "wikilink")（Potassium_sorbate），又名2,4-己二烯酸钾，人工合成，应用广泛，价位较高，
    C<sub>6</sub>H<sub>7</sub>O<sub>2</sub>K。

### 着色剂（色素）

  - [红曲米](../Page/红曲米.md "wikilink")，又名红粬，天然，《本草纲目》记载，常用于[火腿肠](../Page/火腿肠.md "wikilink")、肉类酒类。
  - [辣椒红素](../Page/辣椒红素.md "wikilink")，天然色素。
  - [核黄素](../Page/核黄素.md "wikilink")，又称维生素B2，天然色素，常用于[方便面](../Page/方便面.md "wikilink")。
  - [柠檬黄](../Page/柠檬黄.md "wikilink")，人工合成的偶氮类酸性染料，诞生于1884年。

### 甜味剂

  - [安赛蜜](../Page/安赛蜜.md "wikilink")（Acesulfame
    potassium），人工合成，化学名：乙酰磺胺酸钾，C4H4KNO4S。常见于袋装[榨菜](../Page/榨菜.md "wikilink")。
  - [甜蜜素](../Page/甜蜜素.md "wikilink")（Sodium
    cyclamate），又称甜精，人工合成，化学名：环己基氨基磺酸钠。

### 增味剂

  - [谷氨酸钠也称为MSG或味精](../Page/谷氨酸钠.md "wikilink")，半天然增味剂，1909年人工制备。应用广泛。

### 增稠剂

  - [卡拉胶](../Page/卡拉胶.md "wikilink")（Carrageenan），又称鹿角菜胶，是从海洋红藻提取的多糖的统称，是多种物质的混合物。应用广泛，常见于果酱/肉酱。
  - [黄原胶](../Page/黄原胶.md "wikilink")（Xanthan
    gum），俗称玉米糖胶，是一种经由野油菜黄单孢菌发酵产生的复合多糖体。在美国，它通常是经由玉米淀粉所制造。常见于[方便面](../Page/方便面.md "wikilink")。
  - [瓜尔胶](../Page/瓜尔胶.md "wikilink")（guargum），是从植物瓜尔豆中提取的一种高纯化天然多糖。常见于[方便面](../Page/方便面.md "wikilink")。

## 檢測

许多食品添加剂吸收光谱紫外区或可见光区域的辐射。使用外部校准可以将此吸光度用于测定样品中添加剂的浓度。但是，添加剂可能会一起出现，而一种添加剂的吸光度会干扰另一种添加剂的吸光度。预先的分离步骤是有必要的，添加剂们会首先用高压液相色谱法（HPLC）分开，然后由紫外线或可见光检测器在线监测。

## 參考資料

## 參見

  - [食品添加剂比较](../Page/食品添加剂比较.md "wikilink")

  -
  -
  - [營養補充品](../Page/營養補充品.md "wikilink")

  - [E编码](../Page/E编码.md "wikilink")

  -
  -
  - [食品加工](../Page/食品加工.md "wikilink")

  - [營養補充品](../Page/營養補充品.md "wikilink")

  -
  -
  -
  -
  - [有机肥料](../Page/有机肥料.md "wikilink")

  - [粉紅肉渣](../Page/粉紅肉渣.md "wikilink")

  - [熏](../Page/熏.md "wikilink")

  - [甜味剂](../Page/甜味剂.md "wikilink")

## 外部連結

  - [食品添加物使用範圍及限量暨規格標準](http://consumer.fda.gov.tw/Law/FoodAdditivesList.aspx?nodeID=521)，[行政院衛生署](../Page/行政院衛生署.md "wikilink")[食品藥物管理局](../Page/食品藥物管理局.md "wikilink")[食品藥物消費者知識服務網](../Page/食品藥物消費者知識服務網.md "wikilink")。（[全國法規資料庫](http://law.moj.gov.tw/LawClass/LawContent.aspx?pcode=L0040084)入口）
  - [食品添加劑使用標準](http://www.nhfpc.gov.cn/zwgkzt/cybz/201106/6f3bebb264e24f588b453655bf463d8a/files/b2413b87e525441ebb2882e61137242c.pdf)，[中華人民共和國衛生部](../Page/中華人民共和國衛生部.md "wikilink")。

[\*](../Category/食品添加剂.md "wikilink")

1.
2.
3.
4.
5.