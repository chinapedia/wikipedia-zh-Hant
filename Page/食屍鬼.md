[Amine_Discovered_with_the_Goule.jpg](https://zh.wikipedia.org/wiki/File:Amine_Discovered_with_the_Goule.jpg "fig:Amine_Discovered_with_the_Goule.jpg")》中的食屍鬼。\]\]
**食屍鬼**（[阿拉伯語](../Page/阿拉伯語.md "wikilink")：**الغول**;
al-ghūl）是[阿拉伯神話的](../Page/阿拉伯神話.md "wikilink")[怪物](../Page/怪物.md "wikilink")。食屍鬼是一種住在[沙漠中能變化成動物的變身](../Page/沙漠.md "wikilink")[惡魔](../Page/惡魔.md "wikilink")，尤其是變成食腐的[鬣狗](../Page/鬣狗.md "wikilink")（hyæna）。他們會劫掠墳墓，以死者[屍體的血肉或者是幼兒為食](../Page/屍體.md "wikilink")，亦會將旅人誘至沙漠荒地中殺害並吞噬。[英仙座β星](../Page/英仙座.md "wikilink")[大陵五的英文名稱Algol是來自此阿拉伯傳說生物](../Page/大陵五.md "wikilink")。
据说食尸鬼会掘開坟墓食用尸体，有时也会袭击人类。\[1\]
[印度教中的](../Page/印度教.md "wikilink")[魔怪](../Page/魔怪.md "wikilink")[畢舍遮亦以死人屍體為食物](../Page/畢舍遮.md "wikilink")。

## 其他文化創作的食屍鬼

### 《活死人之夜》或《恶灵古堡》

阿拉伯食尸鬼的信息比较接近《活死人之夜》或《恶灵古堡》的丧尸，一说身份属于魔神的一种。食尸鬼被视为栖息在沙漠的恶灵。\[2\]

### 一般人對食屍鬼的認知

一般人通常會把食屍鬼比喻成[盜墓者](../Page/盜墓者.md "wikilink")，其原因是因為它們只貪圖屍體，把屍體啃到只剩骨頭，對一般人無害。

### 克蘇魯神話

在[霍華·菲力普·洛夫克萊夫特所創造的](../Page/霍華·菲力普·洛夫克萊夫特.md "wikilink")[克蘇魯神話中](../Page/克蘇魯神話.md "wikilink")，食屍鬼是[夜行性的地底](../Page/夜行性.md "wikilink")[種族](../Page/種族.md "wikilink")。曾是人類，他們持續的只食死人肉的習性使他們轉化為駭人的、野獸般的人形生物。雖然他們有駭人的外表，但是他們也有足夠的智能與人對話。

### 龍與地下城

在[角色扮演遊戲](../Page/角色扮演遊戲.md "wikilink")[龍與地下城中](../Page/龍與地下城.md "wikilink")，食屍鬼是怪物般的充滿腐屍味的[不死人類](../Page/不死生物.md "wikilink")。除了腐肉，他們亦捕捉吞食不夠警覺的活物。他們能藉著接觸來麻痺獵物，只有[精靈可以免疫](../Page/精靈.md "wikilink")，他們的近親ghast是更恐怖的存在，精靈亦不能倖免他們的麻痺攻擊。

### The Witcher系列

在波蘭奇幻小說家Andrzej Sapkowski所著的小說The
Witcher與其衍生電子遊戲中，食屍鬼是一種出現在墳地、擅長在墓穴中挖掘隧道、以屍體為食的怪物。通常智能低下、無法與人類溝通；但依舊有少部分例外。在電子遊戲一代中，有一隻食屍鬼跟主角說過比起精靈，他們比較喜歡人類的血肉。

### 巴哈姆特

在[角色扮演遊戲](../Page/角色扮演遊戲.md "wikilink")[巴哈姆特中](../Page/巴哈姆特.md "wikilink")，食屍鬼原本是人，被紅[斗篷男子改造失敗之後變成只能以人屍為食物的食屍鬼](../Page/斗篷.md "wikilink")，為老麥任務的boss，一共有六隻。

### 白狼的黑暗世界

在[白狼遊戲的](../Page/白狼遊戲.md "wikilink")[黑暗世界系列裡](../Page/黑暗世界.md "wikilink")，食屍鬼是由[吸血鬼賜予其血液而得到其部分超自然力量的生物](../Page/吸血鬼.md "wikilink")，因為血液中的魔力喝下吸血鬼血液會受其控制作為其僕人，受吸血鬼的差使。

除了人類，部分吸血鬼亦會將老鼠、蜘蛛，甚至鱷魚轉化成其忠心的僕人。

東歐的Tzimisce族有培育天生的食屍鬼家族。

### 魔獸爭霸

在[魔兽争霸III](../Page/魔兽争霸III.md "wikilink")，食屍鬼是不死族的基本戰鬥單位和木头的採集單位。在[dota中](../Page/dota.md "wikilink")，食尸鬼指天灾军团的基本近战单位，也可以指噬魂鬼这个英雄。

### 天堂網路遊戲

在[Lineage中](../Page/Lineage.md "wikilink")，食屍鬼是一種有著[喪屍外觀的怪物](../Page/喪屍.md "wikilink")，被他攻擊到玩家有可能會[麻痺](../Page/麻痺.md "wikilink")（全身僵直不能動彈）。

### 遊戲王

在卡片遊戲[遊戲王中](../Page/遊戲王.md "wikilink")，Ghoul被翻譯為[盜墓者](../Page/盜墓者.md "wikilink")，以Ghoul為名稱的卡名多半具有破壞對方墓地的效果，而動畫中更有以Ghouls（古魯斯）為名稱的稀有卡獵人集團（Rare
Cards Hunters）。

### 東京喰種

在漫畫[東京喰種中](../Page/東京喰種.md "wikilink")，食屍鬼是一種名為「喰種」的人形食屍鬼，除了人肉、人血、水和咖啡之外無法吃下其他的食物。

### [十字架与吸血鬼](../Page/十字架与吸血鬼.md "wikilink")

邪恶的精灵侵占人类的尸体而形成的妖怪。嗜人肉，时常袭击幼童，或盗食坟里的尸体。畏惧神、信仰及十字架。当人类接受了吸血鬼血后会出现副作用，当中就包括有机会像月音般尸鬼化。

## 參考

[G](../Category/阿拉伯傳說生物.md "wikilink")
[G](../Category/波斯傳說生物.md "wikilink")
[G](../Category/克苏鲁神话.md "wikilink")
[G](../Category/不死生物.md "wikilink")

1.  奇幻世界大百科 p201 <isbn:978-986-5688-48-6>
2.  奇幻世界大百科 p201 <isbn:978-986-5688-48-6>