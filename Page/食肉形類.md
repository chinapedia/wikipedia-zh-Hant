**食肉形類**（[学名](../Page/学名.md "wikilink")：）是[哺乳類的一個分支](../Page/哺乳類.md "wikilink")，包括了現今的[食肉目及已](../Page/食肉目.md "wikilink")[滅絕的](../Page/滅絕.md "wikilink")[細齒獸超科](../Page/細齒獸超科.md "wikilink")，但不包括[肉齒目](../Page/肉齒目.md "wikilink")。肉齒目是食肉形類5870萬年前的姊妹分類。最古老的食肉形類是[古靈貓科](../Page/古靈貓科.md "wikilink")，其下已確定最古老的要算是6300萬年前的*Protictis*。[加拿大發現的](../Page/加拿大.md "wikilink")*Ravenictis*，是最少屬於6500萬年前，亦有可能是屬於食肉形類。

## 定義

食肉形類的[共有衍徵是上第四](../Page/共有衍徵.md "wikilink")[前臼齒](../Page/前臼齒.md "wikilink")（P4）及下第一[臼齒](../Page/臼齒.md "wikilink")（m1）修改為[裂牙](../Page/裂牙.md "wikilink")，而廣為人知的[食肉目則改為現存物種的細小](../Page/食肉目.md "wikilink")[頂部群](../Page/頂部群.md "wikilink")。故此，食肉形類是一個細小的分支，包含了最早期擁有P4/m1裂牙共有衍徵的[生物](../Page/生物.md "wikilink")。\[1\]

## 演化

當食肉形類首先在[古新世出現時](../Page/古新世.md "wikilink")，牠們並非唯一陸地上的食肉[哺乳動物](../Page/哺乳動物.md "wikilink")。[肉齒目包括了](../Page/肉齒目.md "wikilink")[牛鬣獸科及](../Page/牛鬣獸科.md "wikilink")[鬣齒獸科](../Page/鬣齒獸科.md "wikilink")，當時亦都存在，並成功在[始新世衍生出最多物種](../Page/始新世.md "wikilink")。\[2\]從牠們的[齒列可知](../Page/齒列.md "wikilink")，大部份都是熟習吃肉的。在[北美洲](../Page/北美洲.md "wikilink")，一些肉齒目在始新世後仍然生存，並於[漸新世晚期全部](../Page/漸新世.md "wikilink")[滅絕](../Page/滅絕.md "wikilink")。\[3\]肉齒目的衰落正好是食肉形類的爆發及[食肉目的演化輻射](../Page/食肉目.md "wikilink")，並很多現今[肉食性動物的起源](../Page/肉食性.md "wikilink")。\[4\]姑勿論肉齒目是否被取代，但食肉形類最初時的發展曾被肉齒目所影響及阻礙。

最初的食肉形類似乎是由一種細小及吃[昆蟲的哺乳動物在始新世中期演化而來的](../Page/昆蟲.md "wikilink")，當時正值哺乳動物的急速分化。已知最早的食肉形類成員很難發現的[骨骼](../Page/骨骼.md "wikilink")[化石](../Page/化石.md "wikilink")，一般都只有[牙齒標本及部份](../Page/牙齒.md "wikilink")[顎骨碎片](../Page/顎骨.md "wikilink")。這類創新的[物種被分類在](../Page/物種.md "wikilink")[細齒獸科分支中](../Page/細齒獸科.md "wikilink")，在形態上可以分為兩個分支，即[古靈貓亞科及](../Page/古靈貓亞科.md "wikilink")[細齒獸亞科](../Page/細齒獸亞科.md "wikilink")。兩個分支的成員都有食肉形類的牙齒特徵，即沿顎骨關節有[裂牙](../Page/裂牙.md "wikilink")。\[5\]早期細齒獸科成員的分別在於其[齒式](../Page/齒式.md "wikilink")，細齒獸亞科的會像[犬型亞目](../Page/犬型亞目.md "wikilink")，而古靈貓亞科的則像[貓型亞目](../Page/貓型亞目.md "wikilink")。

## 分類及種系發生學

以下是食肉形類的科學分類：

  - **食肉形類**（Carnivoramorpha）
      - †[細齒獸超科](../Page/細齒獸超科.md "wikilink")（Miacoidea）
          - †[細齒獸科](../Page/細齒獸科.md "wikilink")（Miacidae）
              - †[寨里犬屬](../Page/寨里犬屬.md "wikilink")（*Chailicyon*）、*Eostictis*、*Ictognathus*、†[小古貓屬](../Page/小古貓屬.md "wikilink")（*Miacis*）、*Miocyon*、*Oodectes*、*Palaearctonyx*、*Paramiacis*、*Paroodectes*、*Prodaphaemus*、*Quercgyale*、*Tapocyon*、*Uintacyon*、*Vassacyon*、[擬狐獸屬](../Page/擬狐獸屬.md "wikilink")（*Vulpavus*）、*Xinyuictis*及*Ziphacodon*。
          - †[古靈貓科](../Page/古靈貓科.md "wikilink")（Viverravidae）
              - *Bryanictis*、*Didymictis*、*Ictidopappus*、*Mustelodon*、*Pristinictis*、*Protictis*、*Raphictis*、*Simpsonictis*及*Viverravus*。
      - [食肉目](../Page/食肉目.md "wikilink")（Carnivora）
          - [犬型亞目](../Page/犬型亞目.md "wikilink")（Caniformia）或[犬型總科](../Page/犬型總科.md "wikilink")（Canoidea）
          - [貓型亞目](../Page/貓型亞目.md "wikilink")（Feliformia）或[貓型總科](../Page/貓型總科.md "wikilink")（Feloidea）

以下是食肉形類的種系發生：\[6\]

## 參考

## 外部連結

  -
  -
  -
[Category:哺乳類](../Category/哺乳類.md "wikilink")
[Category:食肉形類](../Category/食肉形類.md "wikilink")

1.
2.
3.
4.
5.
6.