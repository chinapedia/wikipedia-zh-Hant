**食肉目**（[学名](../Page/学名.md "wikilink")：）在[动物分类学上是](../Page/动物分类学.md "wikilink")[哺乳纲中的一个](../Page/哺乳动物.md "wikilink")[目](../Page/目_\(生物\).md "wikilink")。食肉目包括260多种胎生的动物。除杂食的[熊科](../Page/熊科.md "wikilink")（比如主要吃[竹叶的](../Page/竹.md "wikilink")[大熊猫](../Page/大熊猫.md "wikilink")），绝大部分均在不同程度上以其他鸟兽、两栖类、爬行类和鱼类为食。食肉目动物分布几乎全世界各地。具有發達的[門齒與](../Page/門齒.md "wikilink")[犬齒](../Page/犬齒.md "wikilink")。

## 特征

食肉目动物犬齿大而尖锐，上颌最后一个前臼齿和下颌第一个臼齿特别发达，称为“[食肉齿](../Page/食肉齿.md "wikilink")”、“裂齿”或“裂肉齒”；食肉目动物[爪子锐利](../Page/爪子.md "wikilink")，有些种类能收缩；大脑发达，具有[脑回](../Page/脑回.md "wikilink")。

食肉目动物是哺乳动物各目中体型差异最大的一个目，体型最小的[伶鼬](../Page/伶鼬.md "wikilink")（*Mustela
nivalis*）只有25克，11厘米，[北极熊](../Page/北极熊.md "wikilink")（*Ursus
maritimus*）则可以重达1000[公斤](../Page/公斤.md "wikilink")，体型更大的[南象海豹雄性可以达到](../Page/南象海豹.md "wikilink")2,270公斤以上，体长可达4.2米。

## 分类

  - **食肉目 Carnivora**
      - [犬型亚目](../Page/犬型亚目.md "wikilink") Caniformia
          - [犬熊科](../Page/犬熊科.md "wikilink") Amphicyonidae

          - [犬科](../Page/犬科.md "wikilink") Canidae

          - [熊型下目](../Page/熊型下目.md "wikilink") Arctoidea

              - [鼬总科](../Page/鼬总科.md "wikilink") Musteloidea
                  - [小熊猫科](../Page/小熊猫科.md "wikilink") Ailuridae
                  - [臭鼬科](../Page/臭鼬科.md "wikilink") Mephitidae
                  - [浣熊科](../Page/浣熊科.md "wikilink") Procyonidae
                  - [鼬科](../Page/鼬科.md "wikilink") Mustelidae
              - [鳍足形类](../Page/鳍足形类.md "wikilink") Pinnipedimorpha
                  - [鳍足类](../Page/鳍足类.md "wikilink") Pinenipedia
                      - [海熊兽科](../Page/海熊兽.md "wikilink") Enaliarctidae

                      - [海象科](../Page/海象科.md "wikilink") Odobenidae

                      - [海狮科](../Page/海狮科.md "wikilink") Otariidae

                      - [海豹科](../Page/海豹科.md "wikilink") Phocidae
              - [熊总科](../Page/熊总科.md "wikilink") Ursoidea
                  - [半狗科](../Page/半狗科.md "wikilink") Hemicyonidae

                  - [熊科](../Page/熊科.md "wikilink") Ursidae
      - [猫型亚目](../Page/猫型亚目.md "wikilink") Feliformia
          - [猎猫科](../Page/猎猫科.md "wikilink") Nimravidae

          - [中鬣狗科](../Page/中鬣狗科.md "wikilink") Precrocutidae

          - [双斑狸科](../Page/双斑狸科.md "wikilink") Nadiniidae

          - Aeluroidea超科

              - Stenoplesictidae科

          - [猫总科](../Page/猫总科.md "wikilink") Feloidea

              - [巴博剑齿虎科](../Page/巴博剑齿虎科.md "wikilink") Barbourofelidae

              - [林狸科](../Page/林狸属.md "wikilink") Prionodontidae

              - [猫科](../Page/猫科.md "wikilink") Felidae

          - [灵猫下目](../Page/灵猫下目.md "wikilink") Viverroidea

              - [灵猫科](../Page/灵猫科.md "wikilink") Viverridae
              - [獴总科](../Page/獴总科.md "wikilink") Herpestoidea
                  - [食蚁狸科](../Page/食蚁狸科.md "wikilink") Eupleridae
                  - [鬣狗科](../Page/鬣狗科.md "wikilink") Hyaenidae
                  - [獴科](../Page/獴科.md "wikilink") Herpestidae

### 种系发生学

食肉目各科演化关系如下：

## 注释

## 参考文献

[Category:食肉目](../Category/食肉目.md "wikilink")