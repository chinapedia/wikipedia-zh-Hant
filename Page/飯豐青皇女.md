**飯豐青皇女**，又稱**飯豐女王**、**青海皇女**、**忍海部皇女**（約，即約生於[允恭天皇](../Page/允恭天皇.md "wikilink")29年，卒於[清寧天皇](../Page/清寧天皇.md "wikilink")5年），公元5世紀後期的[日本皇族](../Page/日本.md "wikilink")（據《[古事記](../Page/古事記.md "wikilink")》和《[日本書紀](../Page/日本書紀.md "wikilink")》的記載）。在清寧天皇崩御後，她曾短暫執政，但她並沒有被列為正式的[天皇](../Page/天皇.md "wikilink")。

其身世有兩種說法：《古事記》、《日本書紀》〈履中紀〉記載她是[履中天皇之女](../Page/履中天皇.md "wikilink")，[母親是](../Page/母親.md "wikilink")[葦田宿禰](../Page/葦田宿禰.md "wikilink")（[葛城襲津彥之子](../Page/葛城襲津彥.md "wikilink")）的女兒[黑媛](../Page/黑媛.md "wikilink")。但是《日本書紀》另一篇〈顯宗紀〉的世系圖卻記載她是[市邊押磐皇子之女](../Page/市邊押磐皇子.md "wikilink")，母親是葦田宿禰孫女、[葛城蟻臣之女](../Page/葛城蟻臣.md "wikilink")[荑媛](../Page/荑媛.md "wikilink")。

據《日本書紀》記載，在清寧天皇死後，弘計王（後來的[顯宗天皇](../Page/顯宗天皇.md "wikilink")）與億計王（後來的[仁賢天皇](../Page/仁賢天皇.md "wikilink")）兄弟互讓皇位，於是他們的同母姊飯豐青皇女在忍海角刺宮（傳說是位於後來的[奈良縣](../Page/奈良縣.md "wikilink")[葛城市忍海之角刺神社](../Page/葛城市.md "wikilink")）執政，被稱為**忍海飯豐青尊**。《日本書紀》將她描述為一位巾幗英雄，並收錄了一首當時的詞人讚頌她的詩歌。然而《古事記》有記載少許不同：清寧天皇無子，死後飯豐青皇女代治天下，由身為姨母的她在[播磨國迎](../Page/播磨國.md "wikilink")-{回}-流落民間的弘計、億計兄弟。

但是上述的記載都沒有承認飯豐青皇女的天皇身份。後世記載則有承認飯豐青皇女為天皇者，包括《[扶桑略記](../Page/扶桑略記.md "wikilink")》稱她為「飯豐女皇廿四代女帝」，《[本朝皇胤紹運錄](../Page/本朝皇胤紹運錄.md "wikilink")》記載「飯豐天皇
忍海部女王是也」。

飯豐青皇女執政的時期很短，在位約10個月左右便去世了。《[水鏡](../Page/水鏡.md "wikilink")》記載她終年45歲。她葬於葛城埴口丘陵，規格與奈良縣葛城市北花內的[北花內大塚古墳](../Page/北花內大塚古墳.md "wikilink")（，全長90米）相同。

## 參考資料

<div class="references-small">

1.  [《日本書紀》卷十二　〈履中天皇　反正天皇〉](http://miko.org/~uraki/kuon/furu/text/syoki/syoki12.htm)，2006年12月12日驗證。
2.  [《日本書紀》卷十五　〈清寧天皇　顯宗天皇　仁賢天皇〉](http://miko.org/~uraki/kuon/furu/text/syoki/syoki15.htm)，2006年12月12日驗證。
3.  [《古事記》第十二章
    〈天皇的御世〉](http://miko.org/~uraki/kuon/furu/text/kojiki/12_3.htm#yuzuri01)，2006年12月12日驗證。

</div>

[Category:古墳時代人物](../Category/古墳時代人物.md "wikilink")
[Category:女天皇](../Category/女天皇.md "wikilink")