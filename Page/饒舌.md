**饶舌**，在現代也譯**說唱**
（）是一種帶有[節奏與](../Page/節奏.md "wikilink")[押韻的歌唱技巧](../Page/押韻.md "wikilink")。在美國[1970年代興起的](../Page/1970年代.md "wikilink")[嘻哈文化中](../Page/嘻哈.md "wikilink")，说唱是主要元素之一，也叫作（司衆），最早是[美國黑人在社區街頭表達政治和社會思想的途徑之一](../Page/非裔美國人.md "wikilink")，其起源與[口語詩有關](../Page/口語詩.md "wikilink")，並且大量運用俗語、俚語等民間口語。作為表演藝術，它注重詞（）、腔（）、吐（）。作為音樂風格，以吐词为词、[R\&B及其他音樂](../Page/R&B.md "wikilink")[取樣為曲的](../Page/音樂取樣.md "wikilink")[嘻哈樂在幾十年裏發展成了美國流行樂的一種主要風格](../Page/嘻哈樂.md "wikilink")，也影響到了包括亞洲在內的世界流行樂，許多流行歌曲都會加入说唱元素。

## 詞源

的原義是快速讀或是快速講的意思。它可能是由這個字縮短而造出的新字。

## 參見

  - [嘻哈](../Page/嘻哈.md "wikilink")（）
  - [催眠麥克風](../Page/催眠麥克風.md "wikilink")，由日本[國王唱片所製作的RAP對決企劃](../Page/國王唱片.md "wikilink")。

## 延伸閱讀

<div class="references-small">

  -
  -
  -
[Category:饒舌](../Category/饒舌.md "wikilink")
[Category:嘻哈](../Category/嘻哈.md "wikilink")
[R](../Category/声乐.md "wikilink") [R](../Category/歌詞.md "wikilink")