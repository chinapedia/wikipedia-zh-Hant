**饮水疗法**是1970年代、[日本](../Page/日本.md "wikilink")[首相](../Page/首相.md "wikilink")[田中角荣访华以后流行的一种](../Page/田中角荣.md "wikilink")[辅助性](../Page/辅助性.md "wikilink")[治疗方法](../Page/治疗方法.md "wikilink")。民间[传说这种疗法源自日本](../Page/传说.md "wikilink")，难以确定是时间上的巧合还是真有事实根据。

办法为每天早上起床之后，空腹喝三杯凉开[水](../Page/水.md "wikilink")。认为可以清洗[肠胃中的残留](../Page/肠胃.md "wikilink")[废物](../Page/废物.md "wikilink")，保持人体所需水（水分在人体体重比例中占70%多）的正常供应，对慢性肥厚性胃炎、慢性萎缩性胃炎、浅表性[胃炎](../Page/胃炎.md "wikilink")、[消化道溃疡](../Page/消化道溃疡.md "wikilink")、慢性[肠炎](../Page/肠炎.md "wikilink")、慢性[消化系统疾病有一定疗效](../Page/消化系统.md "wikilink")。

也有支持者认为，它有降低[血黏度](../Page/血黏度.md "wikilink")，流畅[血液](../Page/血液.md "wikilink")、预防[中风的效果](../Page/中风.md "wikilink")；对[便秘](../Page/便秘.md "wikilink")、[胃酸过多](../Page/胃酸.md "wikilink")、下痢、[膀胱炎](../Page/膀胱.md "wikilink")、[尿道结石](../Page/尿道结石.md "wikilink")……等，均有缓解之效；甚至有的支持者认为，说它有冬天预防[感冒](../Page/感冒.md "wikilink")、[减肥](../Page/减肥.md "wikilink")、延年益寿、纠正[羊水过少和矫治异常胎位等作用](../Page/羊水.md "wikilink")。

由于这种疗法简易可行，不需复杂的技巧、操作程序，额外耗费金钱，迎合了当时人们讲究养生的心理要求。喝过的人有说神清气爽，胃口好，消化功能增强，于强身健体有明显疗效。总之通过口口相传，互相模仿，一时间在全国范围广为流行。虽然那个时代人们行为总体上带有反智性盲从特点，但它对[经济落后](../Page/经济.md "wikilink")、医疗条件欠发达状况下的健身防病，人们保健的一个补充。其盛况比较前些年的[鸡血疗法更有过之](../Page/鸡血疗法.md "wikilink")。

认为这样会造成有[益菌群失调](../Page/益菌.md "wikilink")，反而不利于保健。尤其是出汗后喝水，血液中的盐分流失过多，吸水能力就降低，水分通过细胞膜进入细胞内，使细胞水肿，严重者会出现头晕、眼花等“[水中毒](../Page/水中毒.md "wikilink")”的症状。一次饮水量过多，大量的水积聚在胃肠中，使人胸腹感到胀满，不利健康。饮水过多，还会冲淡胃液，导致胃肠的吸收能力减弱。因此提倡“少量多饮”。<sup>\[注\]</sup>

一些文革回忆文章常把它和“[鸡血疗法](../Page/鸡血疗法.md "wikilink")”、“[甩手疗法](../Page/甩手疗法.md "wikilink")”、“[红茶菌](../Page/红茶菌.md "wikilink")（[海宝](../Page/海宝.md "wikilink")）”、“[针灸治聋哑](../Page/针灸.md "wikilink")”并列；其实饮水疗法和甩手疗法这两种方法，实际流传时间更长一些。

## 参考资料

  - [《老年人多饮水可预防冠心病》，载“冠心病”网](https://web.archive.org/web/20041224194914/http://sdep.cei.gov.cn/shcun/files/2002090503j.htm)
  - 《“健康人生”：喝水也中毒？》，《解放日报》 2002年9月5日，“健康与发展”

[category:健康](../Page/category:健康.md "wikilink")

[Category:民間療法](../Category/民間療法.md "wikilink")