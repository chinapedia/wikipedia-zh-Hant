[New_facade_of_CNU_Main_Bldg_(20160615070040).jpg](https://zh.wikipedia.org/wiki/File:New_facade_of_CNU_Main_Bldg_\(20160615070040\).jpg "fig:New_facade_of_CNU_Main_Bldg_(20160615070040).jpg")
[Capital_Normal_University_North_Campus_International_Center.JPG](https://zh.wikipedia.org/wiki/File:Capital_Normal_University_North_Campus_International_Center.JPG "fig:Capital_Normal_University_North_Campus_International_Center.JPG")
**首都师范大学**，简称**首师大**、**首都师大**，位于[北京市西三环北路](../Page/北京市.md "wikilink")105号，多数校区集中在[海淀区八里庄一带](../Page/八里庄街道_\(北京市海淀区\).md "wikilink")。创建于1954年，原名**北京师范学院**，1992年北京师范学院分院并入北京师范学院，学校更名为首都师范大学。首师大是一所包括文、理、工、管、法、教育、外语、艺术等专业的综合性师范大学。

[CNU_Lab_Bldg_(20131212143800).JPG](https://zh.wikipedia.org/wiki/File:CNU_Lab_Bldg_\(20131212143800\).JPG "fig:CNU_Lab_Bldg_(20131212143800).JPG")
[Student_Activity_Center@CNU_(20140918072421).JPG](https://zh.wikipedia.org/wiki/File:Student_Activity_Center@CNU_\(20140918072421\).JPG "fig:Student_Activity_Center@CNU_(20140918072421).JPG")
[CNU_Liangxiang_Campus_131126.JPG](https://zh.wikipedia.org/wiki/File:CNU_Liangxiang_Campus_131126.JPG "fig:CNU_Liangxiang_Campus_131126.JPG")

## 历史

1954年，成立**北京师范学院**。1960年，[华北人民大学哲学系](../Page/华北人民大学.md "wikilink")、政教系并入北京师范学院。1962年，[北京工农师范学院](../Page/北京工农师范学院.md "wikilink")、[北京体育师范学院并入北京师范学院](../Page/北京体育师范学院.md "wikilink")。1964年，[北京艺术师范学院](../Page/北京艺术师范学院.md "wikilink")、[北京师范专科学校并入北京师范学院](../Page/北京师范专科学校.md "wikilink")。

1978年，[北京外国语学院分院](../Page/北京外国语学院分院.md "wikilink")、[北京语言学院分院](../Page/北京语言学院分院.md "wikilink")、[北京师范学院分院各自建院](../Page/北京师范学院分院.md "wikilink")。

1979年，北京师范学院分出体育系，恢复北京体育师范学院。

1980年，[北京外国语学院分院](../Page/北京外国语学院分院.md "wikilink")、[北京语言学院分院](../Page/北京语言学院分院.md "wikilink")、[北京外国语学校](../Page/北京外国语学校.md "wikilink")，三校合并，定名为**[北京外国语学院分院](../Page/北京外国语学院分院.md "wikilink")**。1985年，北京外国语学院分院划入[北京联合大学](../Page/北京联合大学.md "wikilink")，定名为**北京联合大学外国语师范学院**。

1992年，北京师范学院、北京师范学院分院、[北京联合大学职业师范学院](../Page/北京联合大学职业师范学院.md "wikilink")，三校合并，定名为**首都师范大学**。1993年，[北京联合大学外国语师范学院并入首都师范大学](../Page/北京联合大学外国语师范学院.md "wikilink")。1999年，[北京第三师范学校](../Page/北京第三师范学校.md "wikilink")、[通州师范学校并入首都师范大学](../Page/通州师范学校.md "wikilink")。2011年，[北京幼儿师范学院并入首都师范大学](../Page/北京幼儿师范学院.md "wikilink")\[1\]。

## 院系

[CNU_Motto_Stone.JPG](https://zh.wikipedia.org/wiki/File:CNU_Motto_Stone.JPG "fig:CNU_Motto_Stone.JPG")
17个院系

  - 文学院
      - [汉语言文学专业](../Page/汉语言文学.md "wikilink")（师范）
      - 汉语言文学专业
      - 汉语言文学专业（高级涉外文秘方向）
      - [戏剧影视文学专业](../Page/戏剧影视文学.md "wikilink")
      - [对外汉语专业](../Page/对外汉语.md "wikilink")
      - [文化产业管理专业](../Page/文化产业管理.md "wikilink")
  - 历史学院
      - [历史学专业](../Page/历史学.md "wikilink")（师范）
      - 历史学专业（基地班方向）
      - 历史学专业（城市传统与城市文化管理方向）
      - 历史学专业（文物鉴定与保护方向）
      - [世界历史专业](../Page/世界历史.md "wikilink")
  - 政法学院
      - [思想政治教育专业](../Page/思想政治教育.md "wikilink")（师范）
      - [哲学专业](../Page/哲学.md "wikilink")
      - [法学专业](../Page/法学.md "wikilink")
      - [社会工作专业](../Page/社会工作.md "wikilink")
      - [政治学与](../Page/政治学.md "wikilink")[行政学专业](../Page/行政学.md "wikilink")
  - 管理学院
      - [公共事业管理专业](../Page/公共事业管理.md "wikilink")
      - [国际经济与贸易专业](../Page/国际经济与贸易.md "wikilink")
      - [劳动与社会保障专业](../Page/劳动与社会保障.md "wikilink")
      - [信息管理与信息系统专业](../Page/信息管理与信息系统.md "wikilink")
      - [信息管理与信息系统专业](../Page/信息管理与信息系统.md "wikilink")（电子商务方向）
  - 教育学院\[2\]
      - [教育学专业](../Page/教育学.md "wikilink")
      - [心理学专业](../Page/心理学.md "wikilink")
  - [学前教育学院](../Page/学前教育.md "wikilink")（原北京幼儿师范学校，2011年并入首都师范大学）
  - 外国语学院
      - [英语专业](../Page/英语.md "wikilink")（师范）
      - [英语专业](../Page/英语.md "wikilink")
      - [俄语专业](../Page/俄语.md "wikilink")
      - [德语专业](../Page/德语.md "wikilink")
      - [法语专业](../Page/法语.md "wikilink")
      - [西语专业](../Page/西语.md "wikilink")
      - [日语专业](../Page/日语.md "wikilink")
  - 音乐学院
      - [音乐学专业](../Page/音乐学.md "wikilink")（师范）
      - [音乐学专业](../Page/音乐学.md "wikilink")
      - [舞蹈学专业](../Page/舞蹈学.md "wikilink")
      - [录音艺术专业](../Page/录音艺术.md "wikilink")
  - 美术学院
      - [美术学专业](../Page/美术学.md "wikilink")（师范）
      - [美术学专业](../Page/美术学.md "wikilink")（艺术市场方向）
      - [绘画专业](../Page/绘画.md "wikilink")（国画方向）
      - [绘画专业](../Page/绘画.md "wikilink")（油画方向）
      - [艺术设计专业](../Page/艺术设计.md "wikilink")（环境艺术设计方向）
      - [艺术设计专业](../Page/艺术设计.md "wikilink")（平面艺术设计方向）
      - [艺术设计专业](../Page/艺术设计.md "wikilink")（新媒体艺术设计方向）
  - 数学科学学院
      - [数学与](../Page/数学.md "wikilink")[应用数学专业](../Page/应用数学.md "wikilink")（师范）
      - [数学与](../Page/数学.md "wikilink")[应用数学专业](../Page/应用数学.md "wikilink")
      - [信息与计算科学专业](../Page/信息与计算科学.md "wikilink")
      - [数学与](../Page/数学.md "wikilink")[应用数学专业](../Page/应用数学.md "wikilink")（实验班）
  - 物理系
      - [物理学专业](../Page/物理学.md "wikilink")（师范）
      - [信息工程专业](../Page/信息工程.md "wikilink")（光电信息方向）
  - 化学系
      - [化学专业](../Page/化学.md "wikilink")（师范）
      - [应用化学专业](../Page/应用化学.md "wikilink")
  - 生命科学学院
      - [生物科学专业](../Page/生物科学.md "wikilink")（师范）
      - [生物科学专业](../Page/生物科学.md "wikilink")
      - [生物技术专业](../Page/生物技术.md "wikilink")
  - 资源环境与旅游学院
      - [地理科学专业](../Page/地理学.md "wikilink")（师范）
      - [地理信息系统专业](../Page/地理信息系统.md "wikilink")
      - [旅游管理专业](../Page/旅游管理.md "wikilink")
      - [遥感科学与技术专业](../Page/遥感.md "wikilink")
      - [环境科学专业](../Page/环境科学.md "wikilink")
      - [环境工程（学硕）专业](../Page/环境工程（学硕）.md "wikilink")
      - [环境工程（专硕）专业](../Page/环境工程（专硕）.md "wikilink")
  - 教育技术系
      - [教育技术学专业](../Page/教育技术学.md "wikilink")
  - 信息工程学院
      - [信息工程专业](../Page/信息工程.md "wikilink")
      - [智能科学与技术专业](../Page/智能科学与技术.md "wikilink")
      - [计算机科学与技术专业](../Page/计算机科学与技术.md "wikilink")（师范）
      - [计算机科学与技术专业](../Page/计算机科学与技术.md "wikilink")
      - [电子信息工程专业](../Page/电子信息工程.md "wikilink")
      - [软件工程专业](../Page/软件工程.md "wikilink")
  - 初等教育学院
      - [小学教育专业](../Page/小学教育.md "wikilink")（中文方向）
      - [小学教育专业](../Page/小学教育.md "wikilink")（英语方向）
      - [小学教育专业](../Page/小学教育.md "wikilink")（科学方向）
      - [小学教育专业](../Page/小学教育.md "wikilink")（数学方向）
      - [小学教育专业](../Page/小学教育.md "wikilink")（信息技术方向）
      - [美术学专业](../Page/美术学.md "wikilink")（小学教育方向）
      - [音乐学专业](../Page/音乐学.md "wikilink")（小学教育方向）
  - 成人教育学院
  - 国际文化学院

以及

  - 马克思主义教育学院
  - 大学英语教研部
  - 体育教研部

共有本科专业47个。

此外首都師範大學自2007年起，與境外大學聯合辦[孔子學院](../Page/孔子學院.md "wikilink")，[圣彼得堡国立大学孔子学院](../Page/圣彼得堡国立大学.md "wikilink")、[明尼苏达大学孔子学院](../Page/明尼苏达大学.md "wikilink")、[威尼斯大学孔子学院](../Page/威尼斯大学.md "wikilink")、[皮乌拉大学孔子学院](../Page/皮乌拉大学.md "wikilink")、[纽约州立大学布法罗分校孔子学院与](../Page/纽约州立大学布法罗分校.md "wikilink")[不来梅孔子学院](../Page/不来梅孔子学院.md "wikilink")\[3\]。

## 中国书法文化研究院

首都师范大学书法专业创办于1985年，1999年成立中国书法文化研究所，是中国大学中首个系级建制的书法学科教学科研单位，由著名学者、书法家**[欧阳中石](../Page/欧阳中石.md "wikilink")**教授担任所长。2005年成立中国书法文化研究院，[国学大师](../Page/国学.md "wikilink")[季羡林先生为其题词](../Page/季羡林.md "wikilink")。该学科点是第一个以书法为研究方向而设立的博士学位授权点（1993年国务院学位委员会批准）,第一个以书法为研究方向而设立的博士后科研流动站（1998年国家人事部批准），教育部第一个书法类艺术师资人才培养培训基地，中国书法学科中第一个省部级（北京市）重点学科。2005年成为国家重点建设学科。[链接：中国书法文化研究院主页](https://web.archive.org/web/20140405100822/http://www.cnusf.com/)

## 校友

  - [石国鹏](../Page/石国鹏.md "wikilink")，中学历史教师，以改革中国大陆高中近现代史课程为著称。

  - [袁腾飞](../Page/袁腾飞.md "wikilink")，中学高级教师。[中国中央电视台科教频道](../Page/中国中央电视台.md "wikilink")《[百家讲坛](../Page/百家讲坛.md "wikilink")》系列节目，先后推出由他主讲的《[两宋风云](../Page/两宋风云.md "wikilink")》，《[塞北三朝](../Page/塞北三朝.md "wikilink")》。

  - [纪连海](../Page/纪连海.md "wikilink")，中学历史教师，2005年后参与[百家讲坛的节目录制](../Page/百家讲坛.md "wikilink")，在电视中讲演[和珅](../Page/和珅.md "wikilink")、[纪晓岚等历史人物故事](../Page/纪晓岚.md "wikilink")。

  - [陈迈平](../Page/陈迈平.md "wikilink")，翻译家，[瑞典](../Page/瑞典.md "wikilink")[斯德哥尔摩大学执教](../Page/斯德哥尔摩大学.md "wikilink")，任瑞典笔会理事兼任国际秘书。

  - [董浩](../Page/董浩.md "wikilink")，著名[兒童節目主持人](../Page/兒童節目.md "wikilink")，[中国中央电视台](../Page/中国中央电视台.md "wikilink")[配音演員](../Page/配音演員.md "wikilink")。

  - [金兆鈞](../Page/金兆鈞.md "wikilink")，中國樂評家。

  - [郭長江](../Page/郭長江.md "wikilink")，政府官員。第十一屆全國政協委員，代表社會福利和社會保障界。

  - [汪丁丁](../Page/汪丁丁.md "wikilink")，中國經濟學家。

  - [江廣平](../Page/江廣平.md "wikilink")，政府官員，現任全國總工會書記處書記、黨組成員，國際聯絡部部長。

  - [張越](../Page/張越_\(主持人\).md "wikilink")，著名電視[節目主持人](../Page/節目主持人.md "wikilink")。

  - [周孝正](../Page/周孝正.md "wikilink")，現為[中國人民大學法律社會學教授](../Page/中國人民大學.md "wikilink")、研究所所長和國際關係學院外交系主任。

  - [盧中南](../Page/盧中南.md "wikilink")，中國著名書法家。

  - [王海容](../Page/王海容.md "wikilink")，[毛澤東的表侄孫女](../Page/毛澤東.md "wikilink")，參與[中美建交](../Page/中美建交.md "wikilink")，曾擔任毛澤東和[尼克森談話翻譯](../Page/尼克森.md "wikilink")，後來任[中華人民共和國外交部副部長](../Page/中華人民共和國外交部.md "wikilink")。

  - [彭三源](../Page/彭三源.md "wikilink")，中國女性作家、影視作品編劇，[中國作家協會會員](../Page/中國作家協會.md "wikilink")。

  - [馬書田](../Page/馬書田.md "wikilink")，是中國的[民俗學者](../Page/民俗.md "wikilink")、作家。

  - [劉源](../Page/劉源.md "wikilink")，[劉少奇之子](../Page/劉少奇.md "wikilink")，中國共產黨第十七屆、十八屆中央委員，上將軍銜。

  - [李其炎](../Page/李其炎.md "wikilink")，政府官員，中國人民政治協商會議第十屆全國委員會常委、社會和法制委員會主任委員。

  - [劉念春](../Page/劉念春.md "wikilink")，中國異議人士，現時旅居[美國](../Page/美國.md "wikilink")[紐約](../Page/紐約.md "wikilink")。

  - [房寧](../Page/房寧.md "wikilink")，現為中國社科院政治學研究所所長，中國政治學會副會長。

  - [尹秀珍](../Page/尹秀珍.md "wikilink")，油畫家，中國第一個女性藝術家受邀在[紐約現代藝術博物館舉辦畫展](../Page/紐約現代藝術博物館.md "wikilink")。

  - ，
    現在[天安門城樓毛澤東畫像繪製者](../Page/天安門城樓.md "wikilink")。包括中國大陸版[孫中山](../Page/孫中山.md "wikilink")、[馬克思](../Page/馬克思.md "wikilink")、[列寧](../Page/列寧.md "wikilink")、[斯大林畫像均出自他手](../Page/斯大林.md "wikilink")。

  - [白敬亭](../Page/白敬亭.md "wikilink")，中國新生代男演員，代表作《[匆匆那年](../Page/匆匆那年_\(網路劇\).md "wikilink")》。

## 附属办学

[首都师范大学附属中学](../Page/首都师范大学附属中学.md "wikilink")，是受北京市教委和首都师范大学双重领导的市属重点中学，2001年12月，成为首批国家级示范性高中校。

## 注釋

<div class="references-small">

<references group="注"/>

</div>

## 参看

  - [首都师范大学科德学院](../Page/首都师范大学科德学院.md "wikilink")

## 外部链接

  - [首都师范大学主页](http://www.cnu.edu.cn/)

## 参考文献

[Category:北京高等院校](../Category/北京高等院校.md "wikilink")
[Category:1954年創建的教育機構](../Category/1954年創建的教育機構.md "wikilink")
[Category:首都師範大學](../Category/首都師範大學.md "wikilink")
[Category:海淀区](../Category/海淀区.md "wikilink")
[Category:一流学科建设高校](../Category/一流学科建设高校.md "wikilink")

1.
2.  2010年在教育科学学院、首都基础教育发展研究院的基础上组建教育学院
3.  [首都師範大學召开2013年度孔子学院工作交流会](http://www.cnu.edu.cn/pages/info_details_xwbd.jsp?boardid=70201&classcode=70201&seq=20501)
    ，2013-12-17 国际文化学院