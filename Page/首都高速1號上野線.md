**首都高速道路1號上野線**是[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[中央區](../Page/中央區_\(東京\).md "wikilink")[江戶橋系統交流道至同都](../Page/江戶橋JCT.md "wikilink")[台東區](../Page/台東區.md "wikilink")[入谷出入口的](../Page/入谷出入口.md "wikilink")[首都高速道路路線](../Page/首都高速道路.md "wikilink")。

前往上野線只能經由[都心環狀線的內環進入](../Page/都心環狀線.md "wikilink")，無法從[都心環狀線的外環和](../Page/都心環狀線.md "wikilink")[首都高速6號向島線前往](../Page/首都高速6號向島線.md "wikilink")。因此前往上野線的路線是有限制的，所以本道路的交通量較小，塞車也較不嚴重。

每周六晚上10點到周日早上7點禁行大貨車。

## 路線編號

  - 1

## 出入口

<table>
<thead>
<tr class="header">
<th><p>出入口編號</p></th>
<th><p>設施名</p></th>
<th><p>連結路線名</p></th>
<th><p>起點<small>至</small><br />
<small>(<a href="../Page/km.md" title="wikilink">km</a>)</small></p></th>
<th><p>備註</p></th>
<th><p>所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>-</p></td>
<td><p><strong><a href="../Page/江戶橋JCT.md" title="wikilink">江戶橋系統交流道</a></strong></p></td>
<td><p><strong><a href="../Page/首都高速都心環狀線.md" title="wikilink">(C1)環狀線</a> <a href="../Page/銀座出入口.md" title="wikilink">銀座</a>、<a href="../Page/羽田出入口.md" title="wikilink">羽田方向</a></strong></p></td>
<td><p>0.0</p></td>
<td></td>
<td><p><a href="../Page/中央區_(東京都).md" title="wikilink">中央區</a></p></td>
</tr>
<tr class="even">
<td><p>181</p></td>
<td><p><a href="../Page/本町出入口.md" title="wikilink">本町出入口</a></p></td>
<td><p><a href="../Page/昭和通_(東京都).md" title="wikilink">昭和通</a></p></td>
<td><p>0.2</p></td>
<td><p>銀座、羽田方向出入口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>182</p></td>
<td><p><a href="../Page/國道4號.md" title="wikilink">國道4號</a>（昭和通）</p></td>
<td><p>1.0</p></td>
<td><p>上野、入谷方向出入口</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>183</p></td>
<td><p><a href="../Page/上野出入口.md" title="wikilink">上野出入口</a></p></td>
<td><p>2.7</p></td>
<td><p>銀座、羽田方向出入口</p></td>
<td><p><a href="../Page/台東區.md" title="wikilink">台東區</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><a href="../Page/北上野收費站.md" title="wikilink">北上野收費站</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>銀座、羽田方向</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>184</p></td>
<td><p><a href="../Page/入谷出入口.md" title="wikilink">入谷出入口</a></p></td>
<td><p>國道4號（昭和通）</p></td>
<td><p>4.0</p></td>
<td><p>銀座、羽田方向出入口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/#高速1號線（2期）.md" title="wikilink">高速1號線（2期）</a></strong>（廣域道路）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 特定收費區間

  - 本町出入口→上野出入口、入谷出入口（全部車輛）
  - 上野出入口、入谷出入口→本町出入口（[ETC搭載車](../Page/ETC.md "wikilink")）

## 歷史

  - 1963年12月21日：江戶橋系統交流道～本町出入口通車
  - 1969年5月31日：本町出入口～入谷出入口開通，**全線通車**
  - 1992年12月23日：公佈延伸計畫

## 延伸計劃

1999年12月，[首都高速道路公團曾公布將此路線自入谷出入口往北延伸至與](../Page/首都高速道路公團.md "wikilink")[中央環狀線連接的計劃](../Page/首都高速道路中央環狀線.md "wikilink")\[1\]，當時被稱為「高速1號線（2期）計畫」。

現在要來往[川口線](../Page/首都高速道路川口線.md "wikilink") -
[都心環狀線間必須通過](../Page/首都高速道路都心環狀線.md "wikilink")[6號向島線](../Page/首都高速道路6號向島線.md "wikilink")、[5號池袋線與中央環狀線](../Page/首都高速道路5號池袋線.md "wikilink")、如果1號上野線延伸至中央環狀線，川口線・[東北自動車道方向與都心環狀線間就有直通路線可以使用](../Page/東北自動車道.md "wikilink")。

現時還沒得出具體的計劃，沿線皆是繁忙街道，工程難於進行。

## 交通量

平日24小時交通量（平成17年度道路交通調查）

  - 台東區東上野4丁目26：22,809

## 參考資料

## 相關條目

  - [日本高速道路列表](../Page/日本高速道路列表.md "wikilink")
  - [關東地方道路列表](../Page/關東地方道路列表.md "wikilink")
  - [東京高速道路](../Page/東京高速道路.md "wikilink")

## 外部連結

  - [首都高速道路株式会社](http://www.shutoko.jp/)

[01 Ueno-sen](../Category/首都高速道路.md "wikilink")
[Category:東京都道路](../Category/東京都道路.md "wikilink")
[3701](../Category/東京都道.md "wikilink")

1.