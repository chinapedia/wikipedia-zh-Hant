**香农河**（爱尔兰语：Sionainn，英语：River
Shannon)是[爱尔兰最长的河流](../Page/爱尔兰.md "wikilink")，全长259千米，它几乎贯穿了整个爱尔兰岛，自古以来就是给常重要的水道。希腊学者[托勒密就绘制过香农河的地图](../Page/托勒密.md "wikilink")。该河自北向西南流动，在爱尔兰西南部港口城市[利默里克入海](../Page/利默里克.md "wikilink")。\[1\]\[2\]

## 參考文獻

[Category:愛爾蘭河流](../Category/愛爾蘭河流.md "wikilink")

1.
2.