[Fragrant_Hill_Hotel.jpg](https://zh.wikipedia.org/wiki/File:Fragrant_Hill_Hotel.jpg "fig:Fragrant_Hill_Hotel.jpg")
[Fragrant_Hill_Hotel_lobby.JPG](https://zh.wikipedia.org/wiki/File:Fragrant_Hill_Hotel_lobby.JPG "fig:Fragrant_Hill_Hotel_lobby.JPG")
[Fragrant_Hill_Hotel_garden.jpg](https://zh.wikipedia.org/wiki/File:Fragrant_Hill_Hotel_garden.jpg "fig:Fragrant_Hill_Hotel_garden.jpg")
**香山飯店**是一座位于[中国](../Page/中国.md "wikilink")[北京](../Page/北京.md "wikilink")[西山风景区](../Page/西山.md "wikilink")[香山公园内的](../Page/香山公园.md "wikilink")[四星级](../Page/四星级.md "wikilink")[酒店](../Page/酒店.md "wikilink")，原址曾为[香山慈幼院](../Page/香山慈幼院.md "wikilink")\[1\]，饭店的建筑和园林由美籍华裔建筑设计师[贝聿铭主持设计](../Page/贝聿铭.md "wikilink")，1982年建成开业，1984年曾获“美国建筑学会荣誉奖”。

香山饭店主体建筑是一栋白色的现代主义楼房，饭店宽阔的常春厅大堂，采用玻璃屋顶，自然采光，使内庭成为光庭，乃是贝氏建筑设计的特点之一。主体建筑后的流华池，是典型的中国式园林，弯曲的小径，铺鹅卵石，周围布置假山，白色建筑倒影在水池中，和中式园林有机的融为一体。

## 相關

  - [楊元龍](../Page/楊元龍.md "wikilink")
  - [僑美旅遊事業有限公司](../Page/僑美旅遊.md "wikilink")

## 外部链接

  - [香山飯店](http://www.xsfd.com/)

[Category:北京市酒店](../Category/北京市酒店.md "wikilink")
[Category:香山公园](../Category/香山公园.md "wikilink")
[Category:1982年完工建筑物](../Category/1982年完工建筑物.md "wikilink")
[Category:贝聿铭的建筑设计](../Category/贝聿铭的建筑设计.md "wikilink")
[Category:北京首旅集团](../Category/北京首旅集团.md "wikilink")

1.