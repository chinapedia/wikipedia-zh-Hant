__NOTOC__

|                                                                          |                                         |                                         |
| ------------------------------------------------------------------------ | --------------------------------------- | --------------------------------------- |
| [←](../Page/香港2007年10月.md "wikilink")                                    | **2007年11月**                            | [→](../Page/香港2007年12月.md "wikilink")   |
| <span style="color: red;">日</span>                                       | 一                                       | 二                                       |
|                                                                          |                                         |                                         |
| **[<span style="color: red;">4</span>](../Page/#11月4日.md "wikilink")**   | **[5](../Page/#11月5日.md "wikilink")**   | **[6](../Page/#11月6日.md "wikilink")**   |
| **[<span style="color: red;">11</span>](../Page/#11月11日.md "wikilink")** | **[12](../Page/#11月12日.md "wikilink")** | **[13](../Page/#11月13日.md "wikilink")** |
| **[<span style="color: red;">18</span>](../Page/#11月18日.md "wikilink")** | **[19](../Page/#11月19日.md "wikilink")** | **[20](../Page/#11月20日.md "wikilink")** |
| **[<span style="color: red;">25</span>](../Page/#11月25日.md "wikilink")** | **[26](../Page/#11月26日.md "wikilink")** | **[27](../Page/#11月27日.md "wikilink")** |

## [11月1日](../Page/11月1日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月1日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - 警方搜查連鎖家品店「**[住好啲](../Page/住好啲.md "wikilink")**」，檢獲印有涉嫌跟[三合會有關的襯衣及明信片](../Page/三合會.md "wikilink")，行動中共拘18人。
    [明報](https://web.archive.org/web/20071104165601/http://hk.news.yahoo.com/071101/12/2iot7.html)

<!-- end list -->

  - 警方下午採取行動，到住好啲旗下五間分店搜集證據，合共帶走88件襯衣及逾500張明信片。警方發言人指大部分市民都知道有關字句是三合會組織的名稱，故穿有關襯衣或會違法，呼籲市民不要穿著，並冀商店維護創作自由時，亦須遵守法律。
  - 「住好啲」發言人回應時稱，產品設計只反映香港文化，對可能違法感意外，已購涉案產品的市民可獲安排退款。立法會議員涂謹申則表示，產品是否違例須視乎公司是否有意帶出三合會相關訊息，但有關設計或已墮入灰色地帶。

<!-- end list -->

  - [美國](../Page/美國.md "wikilink")[聯儲局減](../Page/聯儲局.md "wikilink")[息四分一厘後](../Page/息.md "wikilink")，香港多家銀行亦跟隨。其中[匯豐](../Page/匯豐.md "wikilink")、[恆生及](../Page/恆生.md "wikilink")[中銀香港將](../Page/中銀香港.md "wikilink")**[最優惠利率](../Page/最優惠利率.md "wikilink")**減至7.25%，而[東亞及](../Page/東亞.md "wikilink")[渣打等則減至](../Page/渣打.md "wikilink")7.5%。有財資界人士稱，由於認購新股資金即將解凍，不排除短期內再有減息空間。
    [明報](https://web.archive.org/web/20071104084216/http://hk.news.yahoo.com/071101/12/2iosv.html)

<!-- end list -->

  - [金管局總裁](../Page/金管局.md "wikilink")[任志剛指出](../Page/任志剛.md "wikilink")，過去數天**港元**出現投機炒賣升值的活動是不正常的現象，重申特區政府堅決捍衛[聯匯制度](../Page/聯匯制度.md "wikilink")。而[中大的](../Page/中大.md "wikilink")[蘇偉文教授則指國際炒家將熱錢流入香港或會激化](../Page/蘇偉文.md "wikilink")[資產泡沫並使](../Page/資產泡沫.md "wikilink")[通脹加劇](../Page/通脹.md "wikilink")，認為加薪是紓緩通脹的不二法門。
    [明報](http://hk.news.yahoo.com/071101/12/2iory.html)

<!-- end list -->

  - **[人力資源管理學會](../Page/香港人力資源管理學會.md "wikilink")**估計，香港僱員在明年的平均薪酬加幅為4%。該會訪問了15個行業共101家機構，結果顯示是年有97%的公司加薪，平均加幅2.8%，其中以銀行業和保險業等加幅較高。該會指現時股票市場暢旺，令企業面對人才流失的問題，建議企業應採取措施挽留人才。
    [明報](http://hk.news.yahoo.com/071101/12/2ioux.html)

<!-- end list -->

  - 一名男子涉嫌恐嚇[立法會議員](../Page/香港立法會.md "wikilink")**[梁家傑](../Page/梁家傑.md "wikilink")**，被裁定表證成立。案件於[東區裁判法院開審](../Page/東區裁判法院.md "wikilink")，被告劉樹榮否認兩項[刑事恐嚇控罪](../Page/刑事恐嚇.md "wikilink")，梁家傑作供時稱，他在六月收到三封附餐刀、不銹鋼叉及膠匙的信，於是報警，案件翌日再審。
    [明報](http://hk.news.yahoo.com/071101/12/2iou3.html)

## [11月2日](../Page/11月2日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月2日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [香港立法會](../Page/香港立法會.md "wikilink")**[香港島地方選區補選](../Page/2007年香港立法會港島選區補選.md "wikilink")**的選舉主任裁定除[李慨俠外](../Page/李慨俠.md "wikilink")，其餘八名人士包括[葉劉淑儀](../Page/葉劉淑儀.md "wikilink")、[陳方安生](../Page/陳方安生.md "wikilink")、[蔣志偉](../Page/蔣志偉.md "wikilink")、[柳玉成](../Page/柳玉成.md "wikilink")、[蕭思江](../Page/蕭思江.md "wikilink")、[凌尉雲](../Page/凌尉雲.md "wikilink")、[李永健及](../Page/李永健.md "wikilink")[何來獲有效提名](../Page/何來.md "wikilink")，成為補選的候選人。[明報](https://web.archive.org/web/20071104070733/http://hk.news.yahoo.com/071102/12/2iqp9.html)
  - 政府發表立法強制**[停車熄匙](../Page/停車熄匙.md "wikilink")**諮詢文件，建議停車熄匙的違例罰款為定額320港元。[明報](https://web.archive.org/web/20071104021146/http://hk.news.yahoo.com/071102/12/2iqfm.html)

## [11月3日](../Page/11月3日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月3日香港報紙頭條.md "wikilink")）

## [11月4日](../Page/11月4日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月4日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[國泰航空](../Page/國泰航空.md "wikilink")**招聘1,200名機艙服務員，吸引近千人應徵。在早上九時招聘會開始前，不少人已在尖沙咀的會場外等候。國泰表示增聘員工是為了應付明年增加國際航線的班次，為配合來自內地乘客增加，新聘的機艙服務員必須能操流利的普通話。
    [明報](http://hk.news.yahoo.com/071104/12/2ist4.html)

<!-- end list -->

  - **[基督教協基會](../Page/基督教協基會.md "wikilink")**在是年6月訪問了747名中學生，其中30%認為香港青少年的國際視野低，亦有逾半對國際資訊無興趣。機構認為這顯示了青少年的國際視野流於片面，對外國時事關心不足，建議學校通識教育課應增加外國文化及時事內容。
    [香港都市日報](http://www.metrohk.com.hk/news.php?startDate=05112007&newscat=1&newsid=58184)

<!-- end list -->

  - [立會港島區補選候選人](../Page/2007年香港立法會港島選區補選.md "wikilink")**[蔣志偉](../Page/蔣志偉.md "wikilink")**舉行誓師儀式，他表示參選是要為小市民發聲。
    [星島日報](http://hk.news.yahoo.com/071104/60/2isx5.html)

<!-- end list -->

  - 儀式在添馬艦舉行，有過百名支持者出席。他表示現行立法會的功能界別未能真正反映從業員的聲音，又形容自己乃一黑馬，若能凝聚運輸業界力量，他絕對有能力勝出。除蔣志偉外，[葉劉淑儀](../Page/葉劉淑儀.md "wikilink")、[陳方安生](../Page/陳方安生.md "wikilink")、[柳玉成](../Page/柳玉成.md "wikilink")、[李永健](../Page/李永健.md "wikilink")、[蕭思江](../Page/蕭思江.md "wikilink")、[凌尉雲及](../Page/凌尉雲.md "wikilink")[何來亦有參與選舉](../Page/何來.md "wikilink")。

## [11月5日](../Page/11月5日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月5日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [恆指在](../Page/恆指.md "wikilink")[溫家寶發表](../Page/溫家寶.md "wikilink")「**[港股直通車](../Page/港股直通車.md "wikilink")**」的言論後急瀉，恆指在低開302點後跌勢加劇，其後再傳出[中國證監會限制](../Page/中國證監會.md "wikilink")[QDII中的港股比重](../Page/QDII.md "wikilink")，令跌勢一發不可收拾；恆指收報28942.32，跌1526.02，創歷來單日最大點數跌幅。[星島新聞](https://web.archive.org/web/20071107083541/http://hk.news.yahoo.com/071105/60/2iv7z.html)

<!-- end list -->

  - 中國大陸商貿網站**[阿里巴巴](../Page/阿里巴巴.md "wikilink")**將於星期二上市，市場暗盤價最高較招股價高出87%；收報25.35港元，還沒計手續費，散戶每手帳面可獲5900港元盈利，有證券界人士稱，港股急挫或影響部分新股表現，但阿里巴巴由於街貨量低，故相對上會有支持。
    [明報](http://hk.news.yahoo.com/071105/12/2ivzh.html)

<!-- end list -->

  - [政務司司長](../Page/政務司司長.md "wikilink")**[唐英年](../Page/唐英年.md "wikilink")**在[北京與](../Page/北京.md "wikilink")[奧組委主席](../Page/奧組委.md "wikilink")[劉淇會面](../Page/劉淇.md "wikilink")，劉淇表示，[國際奧林匹克委員會對香港](../Page/國際奧林匹克委員會.md "wikilink")[馬術測試賽作高水準評價感到高興](../Page/馬術.md "wikilink")，對香港舉辦馬術賽事充滿信心。而唐英年亦指香港作為奧運協辦城市之一，會全力把馬術比賽辦好。
    [明報](http://hk.news.yahoo.com/071105/12/2iw4h.html)、[大公報](https://web.archive.org/web/20071109200720/http://www.takungpao.com/news/07/11/06/MW-819481.htm)

<!-- end list -->

  - **[屯門](../Page/屯門.md "wikilink")**有男童被[超級市場](../Page/超級市場.md "wikilink")[旋轉閘夾倒](../Page/旋轉閘.md "wikilink")。事發於下午3時許，一名24歲印度裔女子攜同兩歲大的男童到[康麗花園](../Page/康麗花園.md "wikilink")[惠康超級市場購物](../Page/惠康超級市場.md "wikilink")，其間男童疑因貪玩，反方向直衝閘門而被卡住頭部，男童其後被救出並被送院檢查。
    [明報](http://hk.news.yahoo.com/071105/12/2iw2v.html)

<!-- end list -->

  - [嘉禾娛樂創辦人](../Page/嘉禾娛樂.md "wikilink")**[鄒文懷](../Page/鄒文懷.md "wikilink")**宣布退休。他表示自己已年屆80，是退位讓賢之時，加上其女兒陳鄒重珩早已表明無意接任，故較早時已將其持有股份售予[橙天娱乐](../Page/橙天娱乐.md "wikilink")。鄒文懷認為，未來中港的電影業將會融合，並與-{荷李活}-電影並駕齊驅。
    [明報](http://hk.news.yahoo.com/071105/12/2iw34.html)

## [11月6日](../Page/11月6日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月6日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [香港股市是日市況反覆](../Page/香港股市.md "wikilink")，**[恆生指數](../Page/恆生指數.md "wikilink")**在高開277點後掉頭回落，其後跌至28479點的全日低位，但大市低位有承接，並以接近全日的最高位收市，收市報29438點，升495點，成交額1696億。地產及銀行股表現突出，但[中移動及](../Page/中移動.md "wikilink")[匯控則跑輸大市](../Page/匯控.md "wikilink")。
    [明報](http://hk.news.yahoo.com/071106/12/2iz1n.html)

<!-- end list -->

  - 香港是日共發生三宗**[火警](../Page/火警.md "wikilink")**，其中以[屯門的火警最嚴重](../Page/屯門.md "wikilink")，[消防處籲請市民注意家居防火安全](../Page/消防處.md "wikilink")。
    [明報1](http://hk.news.yahoo.com/071106/12/2iz2l.html)、[2](https://web.archive.org/web/20080102015143/http://hk.news.yahoo.com/071106/12/2iz2p.html)、[3](http://hk.news.yahoo.com/071106/12/2iz4c.html)

<!-- end list -->

  - 凌晨一時許，山景邨景麗樓34樓一單位起火，消防員到場破門入屋，男戶主被發現時已被燒死，女戶主則有六成皮膚燒傷，情況危殆。警方在初步調查後發現，起火單位存放了大量助燃品，認為起火原因有可疑，暫列縱火案處理。
  - 消防在起火單位發現一個火頭，相信是睡床首先起火，但未有證據顯示有人放火，故需進一步調查。的男女戶主有拾荒習慣，據了解，鄰居亦稱單位內常堆滿雜物，並曾勸告戶主要清理，但戶主沒有理會，而房署則表示起火單位並無被扣分的記錄。
  - 另凌晨4時許，荃灣眾安街周合成大廈一單位，疑因有油燈燒着雜物而起火，消防員到場後將火警升為三級，並疏散34名住客，三人因吸入濃煙不適送院治理。而美孚新邨則疑有人遺下煙頭，於早上11時11分起火，一頭狗隻被困後幸被救出，火警中亦無人傷。

<!-- end list -->

  - **[昂坪360](../Page/昂坪360.md "wikilink")**匯報重開準備的工作，目前正進行載重測試，希望可以在年底重開。[明報](http://hk.news.yahoo.com/071106/12/2ix39.html)

## [11月7日](../Page/11月7日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月7日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [香港電台唱片騎師](../Page/香港電台.md "wikilink")**[梁奕倫](../Page/梁奕倫.md "wikilink")**被控詐騙稿費，獲輕判150小時[社會服務令](../Page/社會服務令.md "wikilink")。裁判官指考慮到梁氏姊弟在案中並無實際得益，電台亦沒有金錢損失。加上梁奕倫熱心公益，事後亦感到內疚及承諾不會再犯，故處輕判。
    [明報即時新聞](https://web.archive.org/web/20071215202522/http://hk.news.yahoo.com/071107/12/2j0br.html)

## [11月8日](../Page/11月8日.md "wikilink")

  - [香港大學舉辦](../Page/香港大學.md "wikilink")**[立法會港島補選](../Page/2007年香港立法會港島選區補選.md "wikilink")**首場論壇，8名候選人均有出席。
    [維基新聞](../Page/n:香港大學舉辦立法會港島補選論壇.md "wikilink")

## [11月9日](../Page/11月9日.md "wikilink")

  - [國務院](../Page/國務院.md "wikilink")[港澳辦副主任](../Page/港澳辦.md "wikilink")[陳佐洱證實](../Page/陳佐洱.md "wikilink")，[中央政治局常委](../Page/中央政治局.md "wikilink")**[習近平](../Page/習近平.md "wikilink")**已接替國家副主席[曾慶紅](../Page/曾慶紅.md "wikilink")，兼任港澳工作協調小組組長，主管港澳事務。陳佐洱又指[十七大的報告中](../Page/十七大.md "wikilink")，已表明要保持香港長期繁榮穩定，故中央對香港的政策是堅定不移，不會改變。
    [明報](http://hk.news.yahoo.com/071109/12/2j6ta.html)

<!-- end list -->

  - [荷里活電影](../Page/荷里活.md "wikilink")**[蝙蝠俠](../Page/蝙蝠俠.md "wikilink")**下午在[中環](../Page/中環.md "wikilink")[擺花街取景](../Page/擺花街.md "wikilink")，吸引大量市民圍觀。有商販指拍攝影響生意不大，但亦有市民指封路影響其日常活動。[香港電影發展局主席](../Page/香港電影發展局.md "wikilink")[蘇澤光認為](../Page/蘇澤光.md "wikilink")，著名電影到港取景可提升香港形象，冀市民能體諒拍攝工作。
    [明報](https://web.archive.org/web/20071215031548/http://hk.news.yahoo.com/071109/12/2j6sw.html)

<!-- end list -->

  - [華特迪士尼發表業績報告](../Page/華特迪士尼.md "wikilink")，指**[香港迪士尼](../Page/香港迪士尼.md "wikilink")**連續兩年入場人數未達標，拖累公司整體業績。迪士尼將在2008年償還近三億美元貸款以減少利息支出，並考慮暫緩向港府收取未來數年的專營權費及商討擴建樂園，冀改善財政狀況及增加吸引力。
    [明報](https://web.archive.org/web/20080512074108/http://hk.news.yahoo.com/071109/12/2j6t7.html)

<!-- end list -->

  - [香港科技大學舉行學位頒授典禮](../Page/香港科技大學.md "wikilink")，著名作家**[查良鏞](../Page/金庸.md "wikilink")**獲頒授文學榮譽博士。他深信活到老、學到老，認為到老還喜愛讀書是人生的一大快事。另外兩位獲頒授榮譽學位的是商人[蒙民偉](../Page/蒙民偉.md "wikilink")、[諾貝爾化學獎得主](../Page/諾貝爾化學獎.md "wikilink")[卡爾夏通斯教授](../Page/巴里·夏普莱斯.md "wikilink")。
    [大公報](https://web.archive.org/web/20071113232737/http://www.takungpao.com/news/07/11/10/JX-821420.htm)

## [11月10日](../Page/11月10日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月10日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - 警方消息人士透露，前[市政局](../Page/市政局.md "wikilink")[主席](../Page/主席.md "wikilink")**[梁定邦](../Page/梁定邦_\(醫生\).md "wikilink")**於[11月6日上午在](../Page/11月6日.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[怡和街險遭匪徒](../Page/怡和街.md "wikilink")[綁架](../Page/綁架.md "wikilink")，梁在混亂間咬斷賊人[手指脫險](../Page/手指.md "wikilink")。梁定邦並無向外證實事件，而警方則對此高度重視，料日內採取拘捕行動。
    [星島日報](https://web.archive.org/web/20071213025403/http://hk.news.yahoo.com/071111/60/2j7wc.html)
  - 香港政府舉行**[中九龍幹線](../Page/中九龍幹線.md "wikilink")**論壇，多名市民提出對[油麻地的願景](../Page/油麻地.md "wikilink")。
    [維基新聞](../Page/n:香港政府舉行中九龍幹線論壇_多名市民提出對油麻地的願景.md "wikilink")

## [11月11日](../Page/11月11日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月11日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - 一名從[中國大陸到港留學的](../Page/中國大陸.md "wikilink")**[香港科技大學](../Page/香港科技大學.md "wikilink")**博士研究生，疑因難以抵受[學業壓力在](../Page/學業壓力.md "wikilink")[11月10日晚上](../Page/11月10日.md "wikilink")[自殺身亡](../Page/自殺.md "wikilink")。
    [星島日報](https://web.archive.org/web/20071213013041/http://hk.news.yahoo.com/071111/60/2j7wk.html)

## [11月12日](../Page/11月12日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月12日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [證監會向](../Page/證監會.md "wikilink")**[浩誠證券](../Page/浩誠證券.md "wikilink")**發出限制通知書，指涉及未經授權交易及其他不當行為。[明報](http://hk.news.yahoo.com/071112/12/2j8yy.html)

<!-- end list -->

  - 受[美國次按問題及](../Page/美國次按問題.md "wikilink")[人行調高](../Page/人行.md "wikilink")[存款準備金率影響](../Page/存款準備金率.md "wikilink")，**[恆指](../Page/恆指.md "wikilink")**是日裂口低開逾720點至28,000點水平，其後更低見27,468點，收報27,665點，下跌1,117點，成交1,399億港元。[匯控及中資金融股及資源股均出現強大沽壓](../Page/匯控.md "wikilink")，而地產股則因銀行上周減息而靠穩。
    [明報](https://web.archive.org/web/20071213043830/http://hk.news.yahoo.com/071112/12/2jb2b.html)

<!-- end list -->

  - 六個貨運業團體要求增收**[燃油附加費](../Page/燃油附加費.md "wikilink")**以彌補[油價升幅](../Page/柴油.md "wikilink")。他們建議香港境內貨運每公里收費大約八毫，跨境運輸則為每公里六毫半，至於附加費的收取方法，則由櫃車車主和付貨人自行商討。付貨人委員會反對建議，認為難以計算貨運成本。
    [亞視新聞](http://app.hkatvnews.com/v3/oldcontent.php?date=2007-11-12&news_id=111069)

<!-- end list -->

  - 多個**[的士業](../Page/香港的士.md "wikilink")**團體向運輸署申請臨時起錶加價一元，團體下午約見[運輸署官員](../Page/運輸署.md "wikilink")，他們指是次臨時加價措施並無時限性，旨在減輕[石油氣價格上升對司機的壓力](../Page/石油氣.md "wikilink")。將來會視乎[交諮會就的士收費的諮詢結果](../Page/交諮會.md "wikilink")，再決定是否繼續加價措施。
    [明報](http://hk.news.yahoo.com/071112/12/2jb4v.html)

<!-- end list -->

  - 從事乾散貨船運的**[中外運航運](../Page/中外運航運.md "wikilink")**，是日起一連四日公開招股。消息指各金融機構已派發45萬份白表及20萬份黃表，而[孖展認購反應理想](../Page/孖展.md "wikilink")，認購金額已逾200億港元；由於銀行資金充裕，孖展息率亦低見3.8%的近一年低位。
    [經濟通](http://hk.news.yahoo.com/071113/74/2jbdv.html)

## [11月13日](../Page/11月13日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月13日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[麥當勞](../Page/麥當勞.md "wikilink")**快餐店董事總經理[劉士盛](../Page/劉士盛.md "wikilink")，涉嫌受賄被[廉署起訴](../Page/廉政公署_\(香港\).md "wikilink")。47歲的劉士盛涉嫌向十位供應商收取非法回[佣](../Page/佣.md "wikilink")，被控串謀代理人收受利益及妨礙司法公正共三項罪名。他獲准以50萬港元保釋，候至2008年[1月8日再應訊](../Page/1月8日.md "wikilink")。
    [太陽報](http://the-sun.on.cc/cgi-bin/hotnews2.cgi?a=news&b=20071114&c=20071114024254_0000.html)

## [11月14日](../Page/11月14日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月14日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - 受[美股及](../Page/美股.md "wikilink")[中國內地股市利好刺激](../Page/中國內地股市.md "wikilink")，加上[油價顯著回落](../Page/原油.md "wikilink")，**[恆生指數](../Page/恆生指數.md "wikilink")**是日裂口高開984點，其後升幅不斷擴大，更以接近全日高位收市，報29166.01，升1362.66，成交1491.21億。而[國企指數也一洗頹勢](../Page/國企指數.md "wikilink")，急升6.8%，收報13837.21，升1142.05。
    [明報](http://hk.news.yahoo.com/071114/12/2jhm7.html)

<!-- end list -->

  - 警方根據線報，晚上在**[沙頭角公路](../Page/沙頭角公路.md "wikilink")**[流水響迴旋處截停一部的房車](../Page/流水響.md "wikilink")，在車上檢獲兩公斤懷疑是「K仔」的[氯胺酮](../Page/氯胺酮.md "wikilink")，並拘捕兩名分別24歲和28歲的男子。警方指兩人是[沙頭角](../Page/沙頭角.md "wikilink")[邊境禁區內的居民](../Page/香港邊境禁區.md "wikilink")，相信[毒品會供應](../Page/毒品.md "wikilink")[九龍區一帶娛樂場所](../Page/九龍.md "wikilink")。
    [明報即時新聞](https://web.archive.org/web/20071214221510/http://hk.news.yahoo.com/071114/12/2jhkz.html)

<!-- end list -->

  - **[協恩中學](../Page/協恩中學.md "wikilink")**於[灣仔運動場舉行](../Page/灣仔運動場.md "wikilink")[陸運會](../Page/陸運會.md "wikilink")，[起步槍走火](../Page/起步槍.md "wikilink")，[司令員男](../Page/司令員.md "wikilink")[教師中彈](../Page/教師.md "wikilink")。[明報](http://hk.news.yahoo.com/071114/12/2jhnb.html)

## [11月15日](../Page/11月15日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月15日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [太古地產較早前向](../Page/太古地產.md "wikilink")[城規會申請在](../Page/城市規劃委員會.md "wikilink")**[西摩道](../Page/西摩道.md "wikilink")**興建57層高住宅，被指影響景觀而被拒，太古提司法覆核獲[高院裁定勝訴](../Page/香港高等法院.md "wikilink")。法官指城規會以交通阻塞及消防安全為由限制樓宇高度，但現時附近地皮亦已被一併發展，故認為城規會理據不足。
    [明報](http://hk.news.yahoo.com/071115/12/2jknb.html)
  - 有[測量界人士擔心在有關地盤興建](../Page/測量.md "wikilink")[高樓大廈會影響通風及景觀](../Page/高樓大廈.md "wikilink")，而事件亦會影響其公信力。有附近居民則擔心樓宇落成後會加重交通負荷，**[環保觸覺](../Page/環保觸覺.md "wikilink")**亦對此感失望，要求政府繼續上訴，而政府發言人則指要研究判詞後才作下一步行動。
    [明報](http://hk.news.yahoo.com/071115/12/2jkne.html)

<!-- end list -->

  - **[中電](../Page/中電集團.md "wikilink")**行政總裁[包立賢稱](../Page/包立賢.md "wikilink")，燃料價格上升對經營構成壓力，但公司仍未決定會否於明年增加電費，他對與港府達成新[利潤管制協議感樂觀](../Page/利潤管制協議.md "wikilink")，並指談判過程有建設性。另中電有意參與[新加坡國營電廠私有化計劃](../Page/新加坡國營電廠.md "wikilink")，但現時論入標則言之過早。
    [明報](http://hk.news.yahoo.com/071115/12/2jkmt.html)

## [11月16日](../Page/11月16日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月16日香港報紙頭條.md "wikilink")）

## [11月17日](../Page/11月17日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月17日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[區議會選舉](../Page/2007年香港區議會選舉.md "wikilink")**將於翌日舉行，有學者指由於政府沒有作大型宣傳計劃，加上現時社會怨氣較[2003年時少](../Page/2003年.md "wikilink")，故投票率會較2003年時低。而[政制及內地事務局表示](../Page/政制及內地事務局.md "wikilink")，因選民人數增加，用於選民登記宣傳的費用因而調整。
    [明報](https://web.archive.org/web/20071220051523/http://hk.news.yahoo.com/071117/12/2jo3g.html)
  - 另外，[香港明愛在](../Page/香港明愛.md "wikilink")**[區議會選舉](../Page/2007年香港區議會選舉.md "wikilink")**前訪問了逾千名青少年，有一半表示不清楚21歲便可參選區議員，不足一半青少年表示會登記成為選民及投票，他們建議政府應增撥資源給各區學校及社會服務機構，帶領青少年參與更多社會事務。
    [明報](http://hk.news.yahoo.com/071117/12/2jo3j.html)

## [11月18日](../Page/11月18日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月18日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[區議會選舉](../Page/2007年香港區議會選舉.md "wikilink")**是日進行投票，全港共有1,148,815人投票，投票率38.83%，較上屆下跌5.27%。其中以離島區投票率最高，為44.73%，而灣仔區則最少，為30.65%，投票於晚上十時半結束。
    [香港政府新聞公報](http://www.info.gov.hk/gia/general/200711/19/P200711190073.htm)
  - 行政長官**[曾蔭權](../Page/曾蔭權.md "wikilink")**伉儷和多名官員分別到票站投票，曾蔭權表示新一屆區議會職能將會提升，是政府的重要夥伴，與市民的利益有切身關係。而[政制及內地事務局局長](../Page/政制及內地事務局.md "wikilink")[林瑞麟則未有估計投票率](../Page/林瑞麟.md "wikilink")，但鼓勵選民踴躍投票。
    [香港政府新聞公報1](http://www.info.gov.hk/gia/general/200711/18/P200711180100.htm)、[2](http://www.info.gov.hk/gia/general/200711/18/P200711180109.htm)
  - 鑑於[元朗](../Page/元朗.md "wikilink")**[十八鄉](../Page/十八鄉.md "wikilink")**南選區早前有候選人的競選經理遇襲受傷，警方是日加派440名警員在元朗區各票站維持治安，有村民稱事件沒有影響投票心情。而[選管會主席](../Page/選管會.md "wikilink")[彭鍵基亦表明](../Page/彭鍵基.md "wikilink")，政府絕不容忍任何人以暴力或非法手段影響投票結果。
    [明報](http://hk.news.yahoo.com/071118/12/2jp3k.html)
  - 兩年一度的**[海港日](../Page/海港日.md "wikilink")**於[維多利亞港兩岸盛大舉行](../Page/維多利亞港.md "wikilink")。
    [維基新聞](../Page/n:香港海港日海陸空盡出_千帆競渡兩岸同歡.md "wikilink")

## [11月19日](../Page/11月19日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月19日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[區議會選舉](../Page/2007年香港區議會選舉.md "wikilink")**順利完成。
    [官方網站投票結果](https://web.archive.org/web/20071119051344/http://www.elections.gov.hk/dc2007/chi/results.html)、[維基新聞](../Page/n:香港區議會選舉_建制派大勝.md "wikilink")
      - **[民建聯](../Page/民建聯.md "wikilink")**在[區選中大捷](../Page/2007年香港區議會選舉.md "wikilink")，該黨派出177人參選，共得115個議席，當選率達65%，是歷屆最高。主席譚耀宗認為市民現較著重經濟及民生問題，而民建聯的工作符合市民期望，故贏得支持。他會要求當選黨員，努力做到市民的期望和要求。
        [大公報](https://web.archive.org/web/20071122221445/http://www.takungpao.com/news/07/11/20/GW-825850.htm)
      - 而**[泛民](../Page/泛民.md "wikilink")**在區選的成績強差人意，294名代表泛民的候選人中只有109人當選，當選率只有36%。民主黨主席何俊仁承認對手有強勁能力及精密的策略，會全面檢討選舉結果。至於[自由黨則取得](../Page/自由黨.md "wikilink")14個議席，主席田北俊滿意成績，但承認失言風波對山頂選情有影響。[明報](http://hk.news.yahoo.com/071119/12/2jrwk.html)

## [11月20日](../Page/11月20日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月20日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[香港股市](../Page/香港股市.md "wikilink")**再現單日轉向，大市早上受美股急跌影響而跌逾千點，但午後市場傳出[聯儲局可能會召開特別會議減息](../Page/聯儲局.md "wikilink")，藍籌股被趁低吸納，[恆指在四分鐘內收復所有失地](../Page/恆指.md "wikilink")，一度倒升391點，收報27,771點，升311點，成交1,464億。[明報](https://web.archive.org/web/20071214041122/http://hk.news.yahoo.com/071120/12/2jv75.html)
  - [民協主席](../Page/香港民主民生協進會.md "wikilink")**[馮檢基](../Page/馮檢基.md "wikilink")**辭去已擔任18年的主席職務，[廖成利將署任主席一職](../Page/廖成利.md "wikilink")。馮稱區選結果是從政以來最沉重的打擊，辭職是為失去八個議席負責，冀辭職可為民協帶來新氣象。民協將設小組了解選戰失利的原因，翌年2月提交報告。
    [明報](https://web.archive.org/web/20071214004322/http://hk.news.yahoo.com/071120/12/2jv85.html)
  - **[百佳](../Page/百佳.md "wikilink")**超市宣佈於翌日起不再主動派發[膠袋予顧客](../Page/膠袋.md "wikilink")，顧客如需膠袋，會被勸喻港幣兩毫以用作推廣環保。發言人稱措施旨在培育市民改變消費習慣，並會節省成本。但有環保團體擔心以勸喻形式收費會使成效不彰。
    [明報](https://web.archive.org/web/20071213233148/http://hk.news.yahoo.com/071120/12/2jv7c.html)

## [11月21日](../Page/11月21日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月21日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[恆指](../Page/恆生指數.md "wikilink")**受
    多項利淡因素拖累急跌。大市低開近500點後急轉直下，午後沽壓進一步增加，恆指收報26,618點，跌1,153點，成交1,288億。藍籌股幾乎全線向下，但富士康逆市造好。有分析指大市短期會繼續調整，散戶不宜吸納股票。
    [明報](http://hk.news.yahoo.com/071121/12/2jy2h.html)

<!-- end list -->

  - 警方在[八鄉拘捕一對男女](../Page/八鄉.md "wikilink")，被捕男子疑與過去一周的多宗**[持槍劫案](../Page/2007年11月香港連環持槍劫案.md "wikilink")**有關。警方下午到疑犯住所外埋伏，至疑犯外出用車時將之拘捕，並起出一支氣槍連子彈及一批懷疑毒品。警方指疑犯每次犯案後，均駕駛一私家車離開，警方憑此線索而破案。
    [明報](https://web.archive.org/web/20071213022943/http://hk.news.yahoo.com/071121/12/2jy55.html)

<!-- end list -->

  - **[百佳](../Page/百佳.md "wikilink")**是日起不再主動派發[膠袋](../Page/膠袋.md "wikilink")，並轉用可被降解的膠袋盛載貨品。有使用新款膠袋的顧客認為膠袋太薄，擔心容易破損。亦有顧客得悉新措施後不欲捐款，職員便使用舊款膠袋盛載貨品。多家連鎖店舖均表示，會維持現行無膠袋日措施。
    [明報](https://web.archive.org/web/20071213031633/http://hk.news.yahoo.com/071121/12/2jy77.html)

<!-- end list -->

  - 政府建議修訂建築物條例，簡化小型工程監管制度。新例將小型工程分為三級，業主只需聘用不同資歷的承建商及從業員，完成工程後向屋宇署提交證明文件便可，並無須在工程開始前入則。法例將於下月提交立法會審議，預料2009年生效。
    [香港政府新聞公報](http://www.info.gov.hk/gia/general/200711/21/P200711210248.htm)

<!-- end list -->

  - 漁護署發現入冬後首宗野鳥疑帶H5禽流感病毒的個案，署方於[11月18日在](../Page/11月18日.md "wikilink")[屯門公園檢獲一隻染病小白鷺](../Page/屯門公園.md "wikilink")，該野鳥於翌日死亡。香港觀鳥會會長張浩輝認為，小白鷺是香港的留鳥，疑病毒或經走私雀鳥傳到香港。
    [香港政府新聞公報](http://www.info.gov.hk/gia/general/200711/21/P200711210277.htm)、[香港電台](http://www.rthk.org.hk/rthk/news/expressnews/20071121/news_20071121_55_448409.htm)

## [11月22日](../Page/11月22日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月22日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [香港股市大幅波動](../Page/香港股市.md "wikilink")，受[美股三大指數下挫](../Page/美股.md "wikilink")，**[恆生指數](../Page/恆生指數.md "wikilink")**低開約300點，但其後一度倒升385點。然而中國內地股市下午急跌4%，使恆指在半小時內跌757點，低見25,861點，收報26,004點，下跌613點，成交1,239億。
    [明報](http://hk.news.yahoo.com/071122/12/2k122.html)

## [11月23日](../Page/11月23日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月23日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[恆指](../Page/恆指.md "wikilink")**在在連跌兩日之後回升。大市於高開480點後升勢持續，最多升719點，但高位缺乏買盤承接，在三百多點的波幅內上落。恆指收報26541點，升536點，成交減少至1048億元。有分析指港股只屬技術反彈，後市不容過分樂觀。
    [明報](http://hk.news.yahoo.com/071123/12/2k3o8.html)

## [11月24日](../Page/11月24日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月24日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - 行政長官**[曾蔭權](../Page/曾蔭權.md "wikilink")**到京述職後轉到[廣東省訪問](../Page/廣東省.md "wikilink")，與省委書記[張德江及省長](../Page/張德江.md "wikilink")[黃華華會面](../Page/黃華華.md "wikilink")。曾蔭權稱粵省經濟發展迅速，冀將來每年述職後可到粵省與領導人會面，講述香港最新的情況。而張德江則表示，粵港關係日漸密切，冀兩地能有進一步的合作。
    [大公報](http://www.takungpao.com/news/07/11/25/MW-828101.htm)

## [11月25日](../Page/11月25日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月25日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - 由八大電子傳媒合辦的**[立法會補選](../Page/2007年香港立法會港島選區補選.md "wikilink")**論壇晚上在[遮打花園舉行](../Page/遮打花園.md "wikilink")，八位候選人展開激烈辯論。
    [明報](https://web.archive.org/web/20071214052159/http://hk.news.yahoo.com/071125/12/2k4wx.html)、[維基新聞](../Page/n:香港八大傳媒合辦立法會補選論壇_八名候選人同場激辯.md "wikilink")
      - 其中，[陳方安生問及](../Page/陳方安生.md "wikilink")[葉劉淑儀為何在普選方案加入篩選機制](../Page/葉劉淑儀.md "wikilink")。葉劉淑儀指建議是為確保將來的特首能兼顧各方利益，而當被凌尉雲問及23條立法時，葉劉淑儀表示，社會必須有充分諮詢及共識，才能開展立法工作。
      - 其他候選人方面，[凌尉雲冀政府能增投資機會](../Page/凌尉雲.md "wikilink")；[蔣志偉支持制訂公平競爭法](../Page/蔣志偉.md "wikilink")；[蕭思江則強調保持穩定才有繁榮](../Page/蕭思江.md "wikilink")；[何來則對香港教育制度表示失望](../Page/何來.md "wikilink")；[柳玉成表示參選是要增加選民的選擇](../Page/柳玉成.md "wikilink")，另[李永健則表示需要](../Page/李永健.md "wikilink")15萬游離選民的支持。
  - 數百名**[天水圍新市鎮](../Page/天水圍新市鎮.md "wikilink")**居民發起單車遊行，爭取區內福利設施。他們分別步行及騎單車到[天水圍站](../Page/天水圍站_\(西鐵綫\).md "wikilink")，其後乘巴士到[中環](../Page/中環.md "wikilink")，再遊行到[政府總部](../Page/香港政府總部.md "wikilink")。他們把訴求用[粉筆寫在地上](../Page/粉筆.md "wikilink")，又將[鹹水草綁在鐵欄上](../Page/鹹水草.md "wikilink")，冀望政府正視他們的訴求。
    [明報](https://web.archive.org/web/20071223004816/http://hk.news.yahoo.com/071125/12/2k4z1.html)
  - **[百佳](../Page/百佳.md "wikilink")**決定取消不主動派[膠袋計劃](../Page/膠袋.md "wikilink")。公司指計劃雖受顧客和環保團體支持，但同時被指所收捐款欠透明度，故決定取消計劃。已收之約六萬港元善款將捐[公益金作推廣環保用途](../Page/公益金.md "wikilink")。多個環保團體不滿決定，指百佳無意推動環保。
    [明報](https://web.archive.org/web/20071213181904/http://hk.news.yahoo.com/071125/12/2k4ww.html)
  - [民建聯成員](../Page/民建聯.md "wikilink")**[姚銘](../Page/姚銘.md "wikilink")**，涉嫌襲擊[公民黨成員林雨陽被捕](../Page/公民黨.md "wikilink")。[明報](https://web.archive.org/web/20071214040102/http://hk.news.yahoo.com/071126/12/2k7uy.html)

## [11月26日](../Page/11月26日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月26日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [無線及](../Page/無線電視.md "wikilink")[亞視將於](../Page/亞洲電視.md "wikilink")12月31日起推出**[數-{碼}-電視廣播](../Page/香港數碼地面電視廣播.md "wikilink")**，除現有的四條免費頻道外，[香港島北岸](../Page/香港島.md "wikilink")、[九龍及](../Page/九龍.md "wikilink")[沙田等地居民將可率先收看新增的八條數碼頻道](../Page/沙田.md "wikilink")。為讓更多市民以數碼廣播欣賞[北京奧運賽事](../Page/2008年夏季奥林匹克运动会.md "wikilink")，兩台將加快興建其餘六個發射站，於明年八月初完成。
    [香港政府新聞公報](http://www.info.gov.hk/gia/general/200711/26/P200711260176.htm)、[明報](https://web.archive.org/web/20071213194355/http://hk.news.yahoo.com/071126/12/2k7xf.html)
  - 受[美股造好刺激](../Page/美股.md "wikilink")，**[恆指](../Page/恆指.md "wikilink")**裂口高開逾850點，升幅進一步擴大，最多升逾1,100點，收報27,626點，升1,085點，成交增加至1,101億港元。地產股受憧憬美國再減息刺激，[油價高位徘徊](../Page/原油.md "wikilink")，使礦產資源股急升，而公路股亦有追捧。
    [明報](http://hk.news.yahoo.com/071126/12/2k7u1.html)
  - [香港天文台表示](../Page/香港天文台.md "wikilink")，由於[華南受一股強烈東北](../Page/華南.md "wikilink")[季候風影響](../Page/季候風.md "wikilink")，預料香港本周天氣將明顯轉涼。周二氣溫只有[攝氏](../Page/攝氏.md "wikilink")16-20度左右，風勢亦會頗大。周中期間早上氣溫將進一步降至13-14度，是入冬後最低溫。天文台提醒市民採取禦寒措施。
    [明報](https://web.archive.org/web/20071213163446/http://hk.news.yahoo.com/071126/12/2k7y0.html)

## [11月27日](../Page/11月27日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月27日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - **[九巴](../Page/九龍巴士.md "wikilink")**將於12月2日起推出七組新轉乘優惠計劃，包括[荃灣及](../Page/荃灣.md "wikilink")[葵涌往來](../Page/葵涌.md "wikilink")[西九龍](../Page/西九龍.md "wikilink")、及[新界西往來](../Page/新界西.md "wikilink")[沙田及](../Page/沙田.md "wikilink")[新界北的](../Page/新界北.md "wikilink")21條路線，九巴否認提供優惠與[兩鐵合併有關](../Page/兩鐵合併.md "wikilink")，另為配合兩鐵合併，同日起所有以鐵路車站命名的巴士站將統稱為「鐵路站」。
    [九巴新聞稿](http://www.kmb.hk/chinese.php?page=next&file=news/service/news3q07/news2007112701.html)、[明報](https://web.archive.org/web/20071213213034/http://hk.news.yahoo.com/071127/12/2kaic.html)
  - 受[美股急挫拖累](../Page/美股.md "wikilink")，**[恆生指數](../Page/恆生指數.md "wikilink")**裂口低開922點後，更一度跌989點，低見26,637.61，但其後[花旗集團出售半成股權給](../Page/花旗集團.md "wikilink")[中東的](../Page/中東.md "wikilink")[阿布札比投資局使跌幅收窄](../Page/阿布札比投資局.md "wikilink")，恆指午後在27,000點爭持，收報27,210.21，跌416.41，成交1,200.68億港元。
    [明報](http://hk.news.yahoo.com/071127/12/2kahc.html)

## [11月28日](../Page/11月28日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月28日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - 逾千名**[社工](../Page/社工.md "wikilink")**發起[罷工](../Page/罷工.md "wikilink")，要求檢討[整筆過撥款制度](../Page/整筆過撥款.md "wikilink")，[遊行至](../Page/遊行.md "wikilink")[胡忠大廈](../Page/胡忠大廈.md "wikilink")。[明報](https://web.archive.org/web/20071215021733/http://hk.news.yahoo.com/071128/12/2kd64.html)
  - [香港審計署發表調查報告](../Page/香港審計署.md "wikilink")，批評**[旅發局](../Page/旅發局.md "wikilink")**等多個政府部門浪費公帑。
    [審計署報告](http://www.aud.gov.hk/chi/pubpr_arpt/rpt_49.htm)、[明報](https://web.archive.org/web/20071220025942/http://hk.news.yahoo.com/071128/12/2kd5x.html)

<!-- end list -->

  - 審計署批評旅發局其中七名員工薪酬高於頂薪點，前總幹事[臧明華未獲財政司長批准下自行申請近](../Page/臧明華.md "wikilink")18萬元醫療保險，而職員公幹及酬酢費用經常超支，而旅發局所舉辦的推廣活動成效未及預期，要求旅發局改善。
  - 對於審計署的批評，旅發局主席田北俊承認，部份源於前旅協的指引及制度於改組時未有檢討，承認將來會作改善。有立會議員認為報告反映政府對旅發局監管不力，使旅發局管治出現問題，要求周梁淑怡及臧明華到立法會解釋。
  - 審計署亦批評香港郵政在2000年推出電子證書服務，但現時所簽發電子證書只及最初估計的17%，只有154萬多份，虧損近二億港元。審計署認為香港郵政在項目較早階段已知申請率偏低，應儘早重新審視撥款及作合理推算。
  - 審計報告指運輸署有考牌主任未有主持路試，而觀塘及沙田的牌照事務處使用率亦偏低，同時在建行人天橋及隧道時未考慮其他設施對使用率的影響，建議運輸署應檢討人手編配及加強監察設施的使用率，並考慮關閉上述的牌照事務處。
  - 審計報告又批評房署、食環署及康文署監管外判工作不力，包括縱容外判公司工人剋扣工資，同一承辦商被發出失責通知後但乃被評為良好，承辦商表現欠佳時未有採取規管行動或手法寬鬆，建議部門應提供清晰的外判指引及加強監管。

<!-- end list -->

  - **[恆指](../Page/恆指.md "wikilink")**在期指結算前夕反覆造好，大市受美股反彈高開逾百點，但其後指數一度倒跌，其後在27,000點附近爭持，恆指收報27371點，升161點，成交1,041億。次按陰影仍困擾[匯控股價](../Page/匯控.md "wikilink")，但中資電訊股及個別地產股有追捧。
    [明報](http://hk.news.yahoo.com/071128/12/2kd3s.html)
  - **[長實](../Page/長江實業.md "wikilink")**再奪[日出康城第三期項目的發展權](../Page/日出康城.md "wikilink")，執行董事[吳佳慶相信會帶來協同效應](../Page/吳佳慶.md "wikilink")，集團傾向獨資發展該項目，投資額約60億港元，建成後意向呎價約6000港元左右。另集團正等待鄰近樓盤首都的預售同意書，冀短期內可售。
    [明報](http://hk.news.yahoo.com/071128/12/2kd9k.html)

## [11月29日](../Page/11月29日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月29日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [天后](../Page/天后_\(香港\).md "wikilink")[歌頓道與](../Page/歌頓道.md "wikilink")[永興街交界後巷](../Page/永興街.md "wikilink")，有8戶**[僭建物](../Page/僭建物.md "wikilink")**清拆。[明報](http://hk.news.yahoo.com/071129/12/2kev8.html)
  - **[仁愛堂田家炳小學](../Page/仁愛堂田家炳小學.md "wikilink")**前[校長蘇有恆](../Page/校長.md "wikilink")，[非禮校內女](../Page/非禮.md "wikilink")[教師](../Page/教師.md "wikilink")7年罪成囚12個星期。[星島日報](https://web.archive.org/web/20071213013903/http://hk.news.yahoo.com/071129/60/2kgdr.html)

## [11月30日](../Page/11月30日.md "wikilink")

  - （[請按此參閱當天之報章頭條](../Page/n:2007年11月30日香港報紙頭條.md "wikilink")）

<!-- end list -->

  - [馬會及政府將耗資](../Page/馬會.md "wikilink")6,000萬港元，合作把[天恆邨停車場改裝為電話投注中心](../Page/天恆邨.md "wikilink")，中心佔地50,000平方呎，預期於[2009年初啟用](../Page/2009年.md "wikilink")，估計可提供2,500個職位。[財政司長](../Page/財政司.md "wikilink")[曾俊華稱](../Page/曾俊華.md "wikilink")，正考慮在財政預算案中協助天水圍的低收入人士。
    [香港政府新聞公報](http://www.info.gov.hk/gia/general/200711/30/P200711300278.htm)、[明報](https://web.archive.org/web/20080124120653/http://hk.news.yahoo.com/071130/12/2kj6x.html)

<!-- end list -->

  - 市場繼續憧憬[美國減息](../Page/美國.md "wikilink")，**[恆指](../Page/恆指.md "wikilink")**高開122點，其後雖一度倒跌，但有買盤推動指數上升，恆指收報28,643點，上升161點，成交接近1,165億港元。恆指在11月份從高位31,897點跌至最低位25,861點，波幅逾6,000點，月內跌2,709點，跌幅逾8%。
    [明報](http://hk.news.yahoo.com/071130/12/2kj47.html)

<!-- end list -->

  - 兩鐵在**[合併](../Page/兩鐵合併.md "wikilink")**前舉行服飾日，員工均穿上新制服以示團結。但[九鐵不少員工對於由](../Page/九鐵.md "wikilink")[地鐵接管九鐵均抱挫敗感](../Page/地鐵.md "wikilink")，逾100名九鐵經理級員工擔心合併後未能通過評核會被裁減而申請自願離職。亦有基層員工對九鐵依依不捨，但為生活仍會留守崗位。
    [明報](https://web.archive.org/web/20071215070850/http://hk.news.yahoo.com/071130/12/2kj73.html)

<!-- end list -->

  - 政府委任**[兩鐵合併](../Page/兩鐵合併.md "wikilink")**後的[九鐵管理局成員](../Page/九鐵管理局.md "wikilink")，其中[財經事務及庫務局局長](../Page/財經事務及庫務局.md "wikilink")[陳家強將出任管理局主席](../Page/陳家強.md "wikilink")，另[運輸及房屋局局長](../Page/運輸及房屋局.md "wikilink")[鄭汝樺等](../Page/鄭汝樺.md "wikilink")5名官員出任管理局成員。政府指兩鐵合併後，九鐵公司仍然存在，故須設管理局維持運作。
    [香港政府新聞公報](http://www.info.gov.hk/gia/general/200711/30/P200711300109.htm)

[11月](../Category/2007年香港.md "wikilink")