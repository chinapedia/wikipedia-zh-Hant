[Tsing_Long_Highway_Route_3_2016.jpg](https://zh.wikipedia.org/wiki/File:Tsing_Long_Highway_Route_3_2016.jpg "fig:Tsing_Long_Highway_Route_3_2016.jpg")\]\]

**3號幹線**（）是[香港西部的一條](../Page/香港.md "wikilink")[主要幹線](../Page/香港幹線編號系統.md "wikilink")，亦是香港其中一條高速幹線，幹線長度為香港第3（過海幹線則是最長）。它連接[香港島的](../Page/香港島.md "wikilink")[西營盤](../Page/西營盤.md "wikilink")[干諾道西和](../Page/干諾道.md "wikilink")[新界的](../Page/新界.md "wikilink")[元朗](../Page/元朗區.md "wikilink")。當中包括兩條收費及一條免費隧道：[西區海底隧道](../Page/西區海底隧道.md "wikilink")（收費）、[長青隧道](../Page/長青隧道.md "wikilink")（免費）及[大欖隧道](../Page/大欖隧道.md "wikilink")（收費）。它是連接[香港島](../Page/香港島.md "wikilink")、[九龍西和](../Page/九龍西.md "wikilink")[新界西的主要幹道](../Page/新界西.md "wikilink")。

這條幹線的主要組成部分都是因為興建[新機場而興建的連接道路](../Page/香港國際機場.md "wikilink")（參見[機場核心計劃](../Page/機場核心計劃.md "wikilink")），另外則是為解決[新界西的道路及](../Page/新界西.md "wikilink")[大老山隧道之](../Page/大老山隧道.md "wikilink")1994年至1996年擠塞情況。這些包括：（元朗方向顯示）

  - [西區海底隧道](../Page/西區海底隧道.md "wikilink")（西隧），連接港島西近[西營盤及](../Page/西營盤.md "wikilink")[西九龍](../Page/西九龍.md "wikilink")
  - [西九龍公路](../Page/西九龍公路.md "wikilink")，連接西區海底隧道出口至[荔枝角以南](../Page/荔枝角.md "wikilink")，由南至北經過[西九龍填海區](../Page/西九龍填海區.md "wikilink")
  - [青葵公路](../Page/青葵公路.md "wikilink")，屬[青馬管制區](../Page/青馬管制區.md "wikilink")，繞過[葵青貨櫃碼頭並包括](../Page/葵青貨櫃碼頭.md "wikilink")[長青橋](../Page/長青橋.md "wikilink")，連接[荔枝角以南至](../Page/荔枝角.md "wikilink")[青衣](../Page/青衣島.md "wikilink")
  - [長青隧道](../Page/長青隧道.md "wikilink")，屬[青馬管制區](../Page/青馬管制區.md "wikilink")，穿過青衣中部山頭連接青衣東西兩邊
  - [長青公路](../Page/長青公路.md "wikilink")，屬青馬管制區，位於青衣西部
  - [青衣西北交匯處](../Page/青衣西北交匯處.md "wikilink")，屬青馬管制區，位於青衣西北角，讓車輛來往3號幹線及8號幹線之間
  - [青朗公路](../Page/青朗公路.md "wikilink")，包括[汀九橋](../Page/汀九橋.md "wikilink")（屬青馬管制區）及[大欖隧道](../Page/大欖隧道.md "wikilink")，連接[青衣至](../Page/青衣.md "wikilink")[元朗](../Page/元朗.md "wikilink")，並接駁至9號幹線聯繫[屯門](../Page/屯門.md "wikilink")、[元朗及](../Page/元朗.md "wikilink")[落馬洲](../Page/落馬洲.md "wikilink")[邊境通道](../Page/落馬洲管制站.md "wikilink")

同時分別與[4號幹線](../Page/4號幹線.md "wikilink")、[5號幹線](../Page/5號幹線.md "wikilink")、[7號幹線](../Page/7號幹線.md "wikilink")、[8號幹線和](../Page/8號幹線.md "wikilink")[9號幹線連接](../Page/9號幹線.md "wikilink")。

## 歷史

  - [西九龍公路和](../Page/西九龍公路.md "wikilink")[青葵公路](../Page/青葵公路.md "wikilink")（葵青段）於1997年2月通車
  - [西區海底隧道於](../Page/西區海底隧道.md "wikilink")1997年4月通車
  - [青葵公路](../Page/青葵公路.md "wikilink")（青衣段）和[長青公路於](../Page/長青公路.md "wikilink")1997年5月通車
  - 包括[汀九橋和](../Page/汀九橋.md "wikilink")[大欖隧道的](../Page/大欖隧道.md "wikilink")[青朗公路於](../Page/青朗公路.md "wikilink")1998年5月通車

## 出口

\! colspan="4" style="border-bottom: 5px solid \#2FB435;" |  **3號幹線** |-
\! colspan="2" align="center" | 往[元朗方向](../Page/元朗.md "wikilink") \!\!
colspan="2" align="center" | 往[西營盤方向](../Page/西營盤.md "wikilink") |- |
width="70" align="center" | 編號 || width="170" align="center" | 前往地區 ||
width="170" align="center" | 前往地區 || width="70" align="center" | 編號 |-
\! colspan="4" align="center" | 連接
[干諾道西天橋](../Page/干諾道西天橋.md "wikilink") |- | ||
||
[西營盤](../Page/西營盤.md "wikilink")、[上環](../Page/上環.md "wikilink")、[中區](../Page/中環.md "wikilink")
及[灣仔](../Page/灣仔.md "wikilink")  || 1A |- | || ||
[石塘咀](../Page/石塘咀.md "wikilink")、[堅尼地城](../Page/堅尼地城.md "wikilink")、薄扶林及香港仔
 || 1B |- | colspan="4" align="center" |
[<File:AB-Tunnel.svg>](https://zh.wikipedia.org/wiki/File:AB-Tunnel.svg "fig:File:AB-Tunnel.svg")
[西區海底隧道](../Page/西區海底隧道.md "wikilink") |- | 未有出口編號 || 尖沙咀、九龍站、佐敦道\[1\]
|| || |- | rowspan="2" | 2 ||
[大角咀](../Page/大角咀.md "wikilink")、[深水埗](../Page/深水埗.md "wikilink")、荔枝角及[沙田](../Page/沙田區.md "wikilink")
|| rowspan="2" |
[尖沙咀](../Page/尖沙咀.md "wikilink")、[油麻地](../Page/油麻地.md "wikilink")、
[機場快線站](../Page/九龍站_\(港鐵\).md "wikilink") || rowspan="2" | 2 |- |
[油麻地](../Page/油麻地.md "wikilink")、[旺角及九龍塘](../Page/旺角.md "wikilink")
|- | 3 ||
[青衣](../Page/青衣島.md "wikilink")、[9號貨櫃碼頭](../Page/葵青貨櫃碼頭.md "wikilink")、[大嶼山](../Page/大嶼山.md "wikilink")、元朗、屯門（經[南灣隧道](../Page/南灣隧道.md "wikilink")）
 || [荔枝角](../Page/荔枝角.md "wikilink")、[長沙灣](../Page/長沙灣.md "wikilink") ||
3 |- | 4 ||
[1－8號貨櫃碼頭](../Page/葵青貨櫃碼頭.md "wikilink")、[荔景](../Page/荔景.md "wikilink")、沙田
|| 1－8號貨櫃碼頭、大嶼山 || 4 |- | || || [觀塘](../Page/觀塘.md "wikilink")、沙田  || 4A
|- | 4B || [機場快線站](../Page/青衣站.md "wikilink")、青衣（北）、
[荃灣](../Page/荃灣.md "wikilink")、[葵涌](../Page/葵涌.md "wikilink") 　|| || |-
| 4C || 青衣（南） || || |- | colspan="4" align="center" |
[<File:AB-Brücke.svg>](https://zh.wikipedia.org/wiki/File:AB-Brücke.svg "fig:File:AB-Brücke.svg")
[長青橋](../Page/長青橋.md "wikilink") |- | colspan="4" align="center" |
[<File:AB-Tunnel.svg>](https://zh.wikipedia.org/wiki/File:AB-Tunnel.svg "fig:File:AB-Tunnel.svg")
[長青隧道](../Page/長青隧道.md "wikilink") |- | || ||
[8-9號貨櫃碼頭](../Page/葵青貨櫃碼頭.md "wikilink")、沙田（經南灣隧道）
 || 4D（右） |- | || || 青衣、[機場快線站](../Page/青衣站.md "wikilink")、荃灣 || 4E |- |
5 || [大嶼山](../Page/大嶼山.md "wikilink")  || 大嶼山  || 5 |- | 5A || 行政大樓 ||
|| |- | colspan="4" align="center" |
[<File:AB-Brücke.svg>](https://zh.wikipedia.org/wiki/File:AB-Brücke.svg "fig:File:AB-Brücke.svg")
[汀九橋](../Page/汀九橋.md "wikilink") |- | 6 ||
[深井](../Page/深井.md "wikilink")、[青龍頭](../Page/青龍頭.md "wikilink")、[屯門](../Page/屯門.md "wikilink")（經[屯門公路](../Page/屯門公路.md "wikilink")）
 || 荃灣、葵涌、沙田  || 6 |- | colspan="4" align="center" |
[<File:AB-Tunnel.svg>](https://zh.wikipedia.org/wiki/File:AB-Tunnel.svg "fig:File:AB-Tunnel.svg")
[大欖隧道](../Page/大欖隧道.md "wikilink") |- | 6A ||
[八鄉](../Page/八鄉.md "wikilink") || || |- | 6B ||
[錦田](../Page/錦田.md "wikilink")、[石崗](../Page/石崗.md "wikilink") ||
|| |- | ||
[元朗](../Page/元朗.md "wikilink")、[天水圍](../Page/天水圍.md "wikilink")、[香港濕地公園](../Page/香港濕地公園.md "wikilink")、流浮山及[深圳灣](../Page/深圳灣.md "wikilink")
 || || |- | ||
[米埔](../Page/米埔.md "wikilink")、[牛潭尾](../Page/牛潭尾.md "wikilink")、[落馬洲](../Page/落馬洲.md "wikilink")、古洞及[上水](../Page/上水.md "wikilink")
 || || |- \! colspan="4" align="center" | 連接
[新田公路](../Page/新田公路.md "wikilink")／[元朗公路](../Page/元朗公路.md "wikilink")

## 參考資料

## 外部連結

  - 運輸署：3號幹線路線圖
    [JPG版本](http://www.td.gov.hk/mini_site/hksrens/2008/TC/images/Route3.jpg)
    [PDF版本](http://www.td.gov.hk/mini_site/hksrens/2008/TC/images/Route3.pdf)



[Category:香港幹線](../Category/香港幹線.md "wikilink")
[香港3號幹線](../Category/香港3號幹線.md "wikilink")

1.  本出口位於西區海底隧道範圍，需要在收費站前選定行車綫，進入出口，經佐敦道往尖沙咀及九龍站，按照現時出口編號的規則，應為出口1C。