[Art_Museum_The_Chinese_University_of_Hong_Kong_201607.jpg](https://zh.wikipedia.org/wiki/File:Art_Museum_The_Chinese_University_of_Hong_Kong_201607.jpg "fig:Art_Museum_The_Chinese_University_of_Hong_Kong_201607.jpg")
[Art_Museum_The_Chinese_University_of_Hong_Kong_Interior_2012.jpg](https://zh.wikipedia.org/wiki/File:Art_Museum_The_Chinese_University_of_Hong_Kong_Interior_2012.jpg "fig:Art_Museum_The_Chinese_University_of_Hong_Kong_Interior_2012.jpg")
**香港中文大學文物館**（），位於[香港中文大學](../Page/香港中文大學.md "wikilink")[林蔭大道](../Page/百萬大道.md "wikilink")，成立於1971年，隸屬於中文大學中國文化研究所，是[香港公眾](../Page/香港.md "wikilink")[博物館之一](../Page/博物館.md "wikilink")。

文物館以館藏及惠借文物精品為基礎，致力於弘揚[中國文化](../Page/中國文化.md "wikilink")，為大學其他院系的中國藝術在社會、文化、技術及歷史的跨學科[研究提供](../Page/研究.md "wikilink")[史料](../Page/史料.md "wikilink")，促進中外學術交流，貢獻社會。文物館曾經出版了很多刊物，內容包括了出土文物、書畫、[陶瓷](../Page/陶瓷.md "wikilink")、[玉器](../Page/玉器.md "wikilink")、[漆器](../Page/漆器.md "wikilink")、[璽印等等](../Page/璽印.md "wikilink")。

除此之外，文物館附設參考圖書室，收藏了很多圖冊、捐贈的參考書籍，還有文物圖片資料庫。文物館在1977年設置文物修護工作室、書畫裝裱室、攝影室及木工室，為文物館的展覽提供技術協助，亦進行書畫和文物的保存和修復研究。

為了加強與社會聯繫，館方在1981年成立了文物館館友會。其成員都是熱愛中國藝術和支持文物館工作的社會人士，他們積極舉辦講座或研習班、導賞員培訓班、組織文物參觀團等等。另外，文物館本身也積極與其他博物館合作，譬如[日本東京富士美術館](../Page/日本東京富士美術館.md "wikilink")、[美國耶魯大學美術館](../Page/美國耶魯大學美術館.md "wikilink")、[故宮博物院](../Page/故宮博物院.md "wikilink")、[上海博物館](../Page/上海博物館.md "wikilink")、[浙江省博物館](../Page/浙江省博物館.md "wikilink")、[南京博物院](../Page/南京博物院.md "wikilink")、[湖北省博物館](../Page/湖北省博物館.md "wikilink")、[廣東省博物館](../Page/廣東省博物館.md "wikilink")、[廣州藝術博物院](../Page/廣州藝術博物院.md "wikilink")、[江西省博物館](../Page/江西省博物館.md "wikilink")、[湖南省博物館](../Page/湖南省博物館.md "wikilink")、[北京大學圖書館](../Page/北京大學圖書館.md "wikilink")。

2018年，適逢戊戌狗年，中文大學文物館舉辦「戊戌說狗」賀歲展覽。早在農業社會出現前，狗已經被人類馴化，成為人類第一位朋友。狗隻在古代中國社會，為人類守家、狩獵，甚至帶來歡樂，其蹤跡亦被人類紀錄在各式各樣的文物中，向後世講述與這位朋友相處的點滴\[1\]。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{東鐵綫色彩}}">█</font>[東鐵綫](../Page/東鐵綫.md "wikilink")：[大學站](../Page/大學站_\(新界\).md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 外部連結

  - [香港中文大學文物館](http://www.artmuseum.cuhk.edu.hk/zh/)

[category:中国高校博物馆](../Page/category:中国高校博物馆.md "wikilink")

[Category:香港文物館](../Category/香港文物館.md "wikilink")
[Category:香港中文大學](../Category/香港中文大學.md "wikilink")
[Category:香港美术馆](../Category/香港美术馆.md "wikilink")

1.  [「戊戌說狗」展覽賀新歲　介紹人類第一位朋友](https://www.hkcnews.com/article/9902/%E8%BE%B2%E6%9B%86%E6%96%B0%E5%B9%B4-%E4%B8%AD%E6%96%87%E5%A4%A7%E5%AD%B8-%E7%8B%97%E5%B9%B4-9902/%E3%80%8C%E6%88%8A%E6%88%8C%E8%AA%AA%E7%8B%97%E3%80%8D%E5%B1%95%E8%A6%BD%E8%B3%80%E6%96%B0%E6%AD%B2-%E4%BB%8B%E7%B4%B9%E4%BA%BA%E9%A1%9E%E7%AC%AC%E4%B8%80%E4%BD%8D%E6%9C%8B%E5%8F%8B)