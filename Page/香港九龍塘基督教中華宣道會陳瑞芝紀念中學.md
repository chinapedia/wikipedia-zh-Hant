[West_of_Christian_Alliance_S_C_Chan_Memorial_College_after_repaint.jpg](https://zh.wikipedia.org/wiki/File:West_of_Christian_Alliance_S_C_Chan_Memorial_College_after_repaint.jpg "fig:West_of_Christian_Alliance_S_C_Chan_Memorial_College_after_repaint.jpg")
[East_of_Christian_Alliance_S_C_Chan_Memorial_College.jpg](https://zh.wikipedia.org/wiki/File:East_of_Christian_Alliance_S_C_Chan_Memorial_College.jpg "fig:East_of_Christian_Alliance_S_C_Chan_Memorial_College.jpg")
[West_of_Christian_Alliance_S_C_Chan_Memorial_College.jpg](https://zh.wikipedia.org/wiki/File:West_of_Christian_Alliance_S_C_Chan_Memorial_College.jpg "fig:West_of_Christian_Alliance_S_C_Chan_Memorial_College.jpg")

**香港九龍塘基督教中華宣道會陳瑞芝紀念中學**（，簡稱：CASCCMC、SCC、陳瑞芝）是[香港一所資助中學](../Page/香港.md "wikilink")，校舍位於[新界](../Page/新界.md "wikilink")[屯門](../Page/屯門.md "wikilink")[友愛邨](../Page/友愛邨.md "wikilink")，現時是以[英語授課的男女校](../Page/香港英文授課中學.md "wikilink")。[校訓為](../Page/校訓.md "wikilink")[聖經](../Page/聖經.md "wikilink")[箴言九章十節](../Page/箴言.md "wikilink")：「敬畏[耶和華是智慧的開端](../Page/耶和華.md "wikilink")，認識至聖者便是聰明。」\[1\]在2017香港最具教育競爭力中學50強龍虎榜中排名第29位\[2\]。

## 歷史

香港九龍塘基督教中華宣道會陳瑞芝紀念中學於1980年由[九龍塘宣道會創校](../Page/九龍塘宣道會.md "wikilink")\[3\]，於當年9月1日正式開學，創校[校長為羅康先生](../Page/校長.md "wikilink")。當時校舍尚未落成，學校暫借救世軍三聖邨劉伍英小學上課，直至1981年4月26日才遷入[屯門](../Page/新界區.md "wikilink")[友愛邨校舍](../Page/友愛邨.md "wikilink")，並沿用至今。

該校現任[校長為黃順琪先生](../Page/校長.md "wikilink")，副校長郭家輝先生、張國威先生，助理校長孫永康先生、杜芷恩女士。\[4\]

## 辦學宗旨

宣道會陳瑞芝紀念中學的辦學宗旨，是以[聖經的教訓作為教育的基礎](../Page/聖經.md "wikilink")，引導學生明辨是非，追尋知識及真理，啟發智慧，注重德、智、體、群、美、靈均衡的發展。幫助學生建立正確的人生觀，培養高尚的情操，掌握生活技能，提高思辨能力和學科知識，培育學生成為社會上良好的公民與優秀的人材。\[5\]

## 學生特質

學校主張就讀於該校的學生擁有以下特質：\[6\]

  - 彬彬有禮（Courteous）
  - 堅毅自信（Assertive）
  - 勤奮好學（Studious）
  - 認真盡責（Conscientious）
  - 顧及他人（Considerate）
  - 積極進取（Motivated）
  - 效法基督（Christlike）

## 教師

宣道會陳瑞芝紀念中學的教師均為[基督徒](../Page/基督教.md "wikilink")。在2012至2013學年，61位教師（其中一位為外籍英語教師）中有31人擁有[學士學位](../Page/學士.md "wikilink")，30人擁有[碩士學位](../Page/碩士.md "wikilink")。61人中有38人有十年或以上教學年資\[7\]，當中韋秀妍副校長、黃啟榮主任、黃順琪校長及雷保衛老師均由創校開始服務，其中雷保衛老師於2006-2007年度以健康為由已離任，而黃啟榮主任則於2010-2011年榮休。

2012年，服務學校多年的林家幸校長榮休，校長一職由黃順琪主任接任\[8\]。

2014年，兼任化學科主任的余池光副校長榮休，由物理科主任郭家輝主任接任\[9\]。

## 收生及班級結構

宣道會陳瑞芝紀念中學校內大部份中一新生是由[教育局實施的](../Page/教育局_\(香港\).md "wikilink")[香港中學學位分配辦法中招收](../Page/香港中學學位分配辦法.md "wikilink")，另一部份則是在[自行分配學位中招收](../Page/自行分配學位.md "wikilink")。學生均為第一組別（Band
1） 的學生，學術成績優良。

班級結構方面，宣道會陳瑞芝紀念中學在校學生約850人。中一設A,B,C,D,E共5個班別,中二至中六設A、B、C、D共4個班別 （2015
年度中一及中四有五班），每班約30-40人。初中(中一至中三)成績較佳的學生編入B和D班(俗稱精英班)，當中B班比較優異。新學制下，中四學生可按意願選科，選科依成績及學生意願在中三後選定。

值得注意的是宣道會陳瑞芝紀念中學為[香港](../Page/香港.md "wikilink")114所使用[英文作為教學語言的中學之一](../Page/香港英文授課中學.md "wikilink")（該校所處[屯門區僅得](../Page/屯門區.md "wikilink")7所）。除[中國語文](../Page/中文.md "wikilink")、[中國歷史](../Page/中國歷史.md "wikilink")、[中國文學](../Page/中國文學.md "wikilink")、[普通話](../Page/普通話.md "wikilink")、個人成長教育、[通識教育及](../Page/通識教育.md "wikilink")[聖經外](../Page/聖經.md "wikilink")，其餘所有科目均使用[英文作為教學語言](../Page/英文.md "wikilink")。

## 校舍及設施

宣道會陳瑞芝紀念中學校舍座落[新界](../Page/新界.md "wikilink")[屯門](../Page/屯門區.md "wikilink")[友愛邨](../Page/友愛邨.md "wikilink")，與[順德聯誼總會譚伯羽中學校舍相連](../Page/順德聯誼總會譚伯羽中學.md "wikilink")（兩校禮堂均在同一建築內）。

校內設施如下\[10\]\[11\]:

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><strong>29</strong>個課室
<ul>
<li>位於1樓的101至103室</li>
<li>位於2樓的201至207室</li>
<li>位於3樓的301至306室</li>
<li>位於4樓的401至406室</li>
<li>位於5樓的501至507室</li>
</ul></li>
<li><strong>4</strong>個<a href="../Page/實驗室.md" title="wikilink">實驗室</a>
<ul>
<li><a href="../Page/物理.md" title="wikilink">物理實驗室</a></li>
<li><a href="../Page/生物.md" title="wikilink">生物實驗室</a></li>
<li><a href="../Page/化學.md" title="wikilink">化學實驗室</a>（翁平化學實驗室）</li>
<li><a href="../Page/科學.md" title="wikilink">綜合科學實驗室</a>（陳英生實驗室）</li>
</ul></li>
</ul></td>
<td><ul>
<li>多用途禮堂<small>（陳朱素華堂）</small></li>
<li>十週年紀念廣場（每天舉行午間講壇）[12]</li>
<li>學生活動中心</li>
<li><a href="../Page/圖書館.md" title="wikilink">圖書館</a>（榮華圖書館）</li>
<li><a href="../Page/地理.md" title="wikilink">地理室</a>（鄺永祺地理室）</li>
<li>家政室</li>
<li>英文室</li>
<li><a href="../Page/多媒體.md" title="wikilink">多媒體學習室</a></li>
<li><a href="../Page/音樂.md" title="wikilink">音樂室</a>（楊佩欣紀念室）</li>
<li><a href="../Page/美術.md" title="wikilink">美術室</a></li>
<li>學生會辦事處</li>
<li>醫療室</li>
<li>訓導室</li>
<li>社工室</li>
</ul></td>
<td><ul>
<li>校務處</li>
<li>會議室/家長教師會室</li>
<li>教職員室（設有呼叫系統，方便學生呼叫老師）</li>
<li>教員休息室</li>
<li>有蓋操場</li>
<li><a href="../Page/小食.md" title="wikilink">小食部</a></li>
<li><a href="../Page/羽毛球.md" title="wikilink">羽毛球場</a></li>
<li><a href="../Page/籃球.md" title="wikilink">籃球場</a></li>
<li><a href="../Page/排球.md" title="wikilink">排球場</a></li>
<li><a href="../Page/乒乓球.md" title="wikilink">乒乓球場</a></li>
<li>體育用品室</li>
<li>男女更衣室（供學生於體育課前後更換校服）</li>
<li>天台花園</li>
<li>工藝室</li>
</ul></td>
</tr>
</tbody>
</table>

其中鄺永祺地理室為地理科老師鄺永祺捐助，音樂室則紀念因病逝世的楊佩欣老師。

在2005年暑假，該校全面翻新[籃球場和](../Page/籃球.md "wikilink")[排球場](../Page/排球.md "wikilink")，鋪上軟膠墊，並在有蓋操場裝設捲閘、風閘、空調系統，更新視聽裝置，使之成為另一個可進行聚會的小禮堂。該校又在正門入口位置設置展覽角，並裝上紀功匾等，以美化入口大堂。此外，所有課室的[電腦被全面更新](../Page/電腦.md "wikilink")，裝在特別設計的教師桌內，20台課室及特別室的電腦投影機亦被更換，讓教師在應用資訊科技教學時效果更理想。於創校25週年時，校方曾以命名特別室為學校籌募經費，括號中是該次活動後的名字。在2011年，學校為切合環保需求，將校舍的照明系統全部換成環保光管。

## 社別

所有學生均被分配至四個社（House）中的其中一個。四個社以[新約聖經](../Page/新約聖經.md "wikilink")[四福音書命名](../Page/四福音書.md "wikilink")，分別為:

  - [馬太社](../Page/馬太福音.md "wikilink")（Matthew），社色為青藍色<span style="color: cyan;">●</span>。
  - [馬可社](../Page/馬可福音.md "wikilink")（Mark），社色為[黃色](../Page/黃色.md "wikilink")<span style="color: yellow;">●</span>。
  - [路加社](../Page/路加福音.md "wikilink")（Luke），社色為[紅色](../Page/紅色.md "wikilink")<span style="color: red;">●</span>。
  - [約翰社](../Page/約翰福音.md "wikilink")（John），社色為[綠色](../Page/綠色.md "wikilink")<span style="color: green;">●</span>。

四社於陸運會、水運會、社際常識問答、歌唱比賽和其他社際比賽中互相競爭，鼓勵學生的團隊和合作精神，也使不同年級的學生能夠互相接觸合作。此外，宣道會陳瑞芝紀念中學經常參予校際比賽，而且屢獲殊榮。

## 著名/傑出校友

  - [呂慧儀](../Page/呂慧儀.md "wikilink")：香港女演員
  - [邵家臻](../Page/邵家臻.md "wikilink")：香港立法會議員（1989年中七畢業）
  - [張慧慈](../Page/張慧慈.md "wikilink")：香港作家及節目主持
  - [許榮臻](../Page/許榮臻.md "wikilink")：香港指揮家
  - [何佩珉](../Page/何佩珉.md "wikilink")：香港女演員（2010年中五畢業）
  - [袁嘉諾](../Page/袁嘉諾.md "wikilink")：元朗民政事務專員

## 參閱

  - [香港英文授課中學](../Page/香港英文授課中學.md "wikilink")
  - [屯門區](../Page/屯門區.md "wikilink")，學校所在的[區份](../Page/香港行政區劃.md "wikilink")。
  - [友愛邨](../Page/友愛邨.md "wikilink")，學校所在的[屋邨](../Page/屋邨.md "wikilink")。
  - [九龍塘宣道會](../Page/九龍塘宣道會.md "wikilink")，辦學團體。
  - [宣道中學](../Page/宣道中學.md "wikilink")，同為九龍塘宣道會開辦，位於屯門的中學。

## 參考資料

## 外部連結

  - [宣道會陳瑞芝紀念中學 - 校網](http://www.scc.edu.hk)
  - [宣道會陳瑞芝紀中學圖書館](http://www.scc.edu.hk/library/)

[Category:屯門市中心](../Category/屯門市中心.md "wikilink")
[C](../Category/屯門區中學.md "wikilink")
[Category:1980年創建的教育機構](../Category/1980年創建的教育機構.md "wikilink")
[Category:香港九龍塘基督教中華宣道會](../Category/香港九龍塘基督教中華宣道會.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")

1.  英文原文為：The fear of the Lord is the beginning of wisdom, and knowledge
    of the Holy One is understanding.

2.  [2017香港最具教育競爭力中學50強龍虎榜-香港專業教育出版社](http://www.hkpep.com/index.php?main_page=page_2&curkey=zhongxue#.WudEq5e-lPY)

3.

4.
5.
6.

7.
8.
9.
10.
11.

12.