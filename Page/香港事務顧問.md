**香港事務顧問**（\[1\]，簡稱**港事顧問**）是[中華人民共和國政府為更廣泛地徵求](../Page/中華人民共和國政府.md "wikilink")[香港各界人士對香港實現平穩過渡](../Page/香港.md "wikilink")、保持繁榮穩定的意見和要求，以[國務院港澳事務辦公室和](../Page/國務院港澳事務辦公室.md "wikilink")[新華社香港分社的名義決定聘請有關人士擔任港事顧問](../Page/新華社香港分社.md "wikilink")，任期至1997年6月30日，即[香港回歸前](../Page/香港回歸.md "wikilink")。

中國政府自1992年3月先後聘請四批合共187人為港事顧問，他們以個人名義受聘，主要來自工商事業界別最上層的人士。\[2\]人選方面只挑選[中央認同的人物](../Page/中華人民共和國中央人民政府.md "wikilink")，因而缺乏反對意見。

另一方面，由於聘書由[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")、[國務院總理](../Page/國務院總理.md "wikilink")[李鵬親自頒發](../Page/李鵬.md "wikilink")，港事顧問成為了中央認同的標籤，使不少不獲得聘任的人士以各種方式謀求獲得聘任。例如，前[香港城市大學校長](../Page/香港城市大學.md "wikilink")[鄭耀宗就曾因為](../Page/鄭耀宗.md "wikilink")[香港理工大學校長](../Page/香港理工大學.md "wikilink")[潘宗光獲聘而自己未有受聘而召開記者招待會抗議](../Page/潘宗光.md "wikilink")，引起其他人效尤，結果使中央多次需要擴大港事顧問團的團體。[馮檢基曾因此揶揄這批人](../Page/馮檢基.md "wikilink")，戲稱他們為「講是顧問」或「房事顧問」\[3\]。

## 港事顧問名單

  - 第一批（44人，1992年3月11日聘請，1994年3月2日續聘43人）

[黃保欣](../Page/黃保欣.md "wikilink")　[方黃吉雯](../Page/方黃吉雯.md "wikilink")　[安子介](../Page/安子介.md "wikilink")　[朱幼麟](../Page/朱幼麟.md "wikilink")　[李國寶](../Page/李國寶.md "wikilink")　[李福善](../Page/李福善.md "wikilink")　[李嘉誠](../Page/李嘉誠.md "wikilink")　[吳康民](../Page/吳康民.md "wikilink")　[邵友保](../Page/邵友保.md "wikilink")　[邵逸夫](../Page/邵逸夫.md "wikilink")　[胡法光](../Page/胡法光.md "wikilink")　[胡應湘](../Page/胡應湘.md "wikilink")　[查濟民](../Page/查濟民.md "wikilink")　[唐一柱](../Page/唐一柱.md "wikilink")
[唐翔千](../Page/唐翔千.md "wikilink")　　[列顯倫](../Page/列顯倫.md "wikilink")　[徐四民](../Page/徐四民.md "wikilink")　[徐是雄](../Page/徐是雄.md "wikilink")　[徐展堂](../Page/徐展堂.md "wikilink")　[陳日新](../Page/陳日新_\(香港\).md "wikilink")　[曹宏威](../Page/曹宏威.md "wikilink")　[梁振英](../Page/梁振英.md "wikilink")　[張永珍](../Page/張永珍.md "wikilink")　[黃志祥](../Page/黃志祥_\(商人\).md "wikilink")　[黃宜弘](../Page/黃宜弘.md "wikilink")　[程介南](../Page/程介南.md "wikilink")
[鄔維庸](../Page/鄔維庸.md "wikilink")　　[曾憲梓](../Page/曾憲梓.md "wikilink")　[閔建蜀](../Page/閔建蜀.md "wikilink")　[董建華](../Page/董建華.md "wikilink")　[廖本懷](../Page/廖本懷.md "wikilink")　[廖瑤珠](../Page/廖瑤珠.md "wikilink")　[鄭維健](../Page/鄭維健.md "wikilink")　[鄭耀棠](../Page/鄭耀棠.md "wikilink")　[劉定中](../Page/劉定中.md "wikilink")　[劉皇發](../Page/劉皇發.md "wikilink")　[霍英東](../Page/霍英東.md "wikilink")　[鄺廣傑](../Page/鄺廣傑.md "wikilink")　[鍾士元](../Page/鍾士元.md "wikilink")\[4\]
[簡福飴](../Page/簡福飴.md "wikilink")　　[羅康瑞](../Page/羅康瑞.md "wikilink")　[羅德丞](../Page/羅德丞.md "wikilink")　[譚惠珠](../Page/譚惠珠.md "wikilink")　[釋覺光](../Page/釋覺光.md "wikilink")

  - 第二批（49人，1993年4月1日聘請，1995年5月4日續聘48人）

[林百欣](../Page/林百欣.md "wikilink")　[王英偉](../Page/王英偉.md "wikilink")　[伍淑清](../Page/伍淑清.md "wikilink")　[阮北耀](../Page/阮北耀.md "wikilink")　[李兆基](../Page/李兆基.md "wikilink")　　[李明堃](../Page/李明堃.md "wikilink")　[李連生](../Page/李連生_\(香港\).md "wikilink")　[李業廣](../Page/李業廣.md "wikilink")　[李鵬飛](../Page/李鵬飛.md "wikilink")　[吳光正](../Page/吳光正.md "wikilink")　[吳家瑋](../Page/吳家瑋.md "wikilink")　[吳清輝](../Page/吳清輝.md "wikilink")　[邵善波](../Page/邵善波.md "wikilink")　
[馬　力](../Page/馬力_\(民建聯主席\).md "wikilink")　[胡經昌](../Page/胡經昌.md "wikilink")　[侯瑞培](../Page/侯瑞培.md "wikilink")　[夏利萊](../Page/夏利萊.md "wikilink")\[5\]　[倪少傑](../Page/倪少傑.md "wikilink")　[陳文裘](../Page/陳文裘.md "wikilink")　[陳永棋](../Page/陳永棋.md "wikilink")　[陳樺碩](../Page/陳樺碩.md "wikilink")　[陳婉嫻](../Page/陳婉嫻.md "wikilink")　[陳耀華](../Page/陳耀華.md "wikilink")　[梁定邦](../Page/梁定邦_\(醫生\).md "wikilink")　[梁愛詩](../Page/梁愛詩.md "wikilink")　[高　錕](../Page/高錕.md "wikilink")
[郭志權](../Page/郭志權_\(商人\).md "wikilink")　[郭炳湘](../Page/郭炳湘.md "wikilink")　[郭鶴年](../Page/郭鶴年.md "wikilink")　[黃良會](../Page/黃良會.md "wikilink")　　[黃紹倫](../Page/黃紹倫.md "wikilink")　[黃景強](../Page/黃景強.md "wikilink")　[黃學海](../Page/黃學海.md "wikilink")　[曹光彪](../Page/曹光彪.md "wikilink")　[張人龍](../Page/張人龍.md "wikilink")　[張佑啟](../Page/張佑啟.md "wikilink")　[張鑑泉](../Page/張鑑泉.md "wikilink")　[溫嘉旋](../Page/溫嘉旋.md "wikilink")　[曾鈺成](../Page/曾鈺成.md "wikilink")
[楊耀忠](../Page/楊耀忠.md "wikilink")　[蔣　震](../Page/蔣震.md "wikilink")　[鄭裕彤](../Page/鄭裕彤.md "wikilink")　[潘國濂](../Page/潘國濂.md "wikilink")　　[劉兆佳](../Page/劉兆佳.md "wikilink")　[劉漢銓](../Page/劉漢銓.md "wikilink")　[謝志偉](../Page/謝志偉_\(香港\).md "wikilink")　[謝國民](../Page/謝國民.md "wikilink")　[鍾逸傑](../Page/鍾逸傑.md "wikilink")　[譚耀宗](../Page/譚耀宗.md "wikilink")

  - 第三批（50人，1994年5月26日聘請）

[王紹爾](../Page/王紹爾.md "wikilink")　[尹　林](../Page/尹林.md "wikilink")　[田北俊](../Page/田北俊.md "wikilink")　　[包陪慶](../Page/包陪慶.md "wikilink")　[江紹倫](../Page/江紹倫.md "wikilink")　[杜葉錫恩](../Page/杜葉錫恩.md "wikilink")　[李秀恆](../Page/李秀恆.md "wikilink")　[李啟明](../Page/李啟明_\(香港\).md "wikilink")　[李國章](../Page/李國章.md "wikilink")　[李澤鉅](../Page/李澤鉅.md "wikilink")　[岑才生](../Page/岑才生.md "wikilink")　[利國偉](../Page/利國偉.md "wikilink")　[何冬青](../Page/何冬青.md "wikilink")
[何鍾泰](../Page/何鍾泰.md "wikilink")　[余國春](../Page/余國春.md "wikilink")　[林貝聿嘉](../Page/林貝聿嘉.md "wikilink")　[郁德芬](../Page/郁德芬.md "wikilink")　[周厚澄](../Page/周厚澄.md "wikilink")　[屈　超](../Page/屈超.md "wikilink")　　[施子清](../Page/施子清.md "wikilink")　[洪清源](../Page/洪清源.md "wikilink")　[唐英年](../Page/唐英年.md "wikilink")　[陳乃強](../Page/陳乃強.md "wikilink")　[陳文鴻](../Page/陳文鴻.md "wikilink")　[陳協平](../Page/陳協平.md "wikilink")　[陳萬雄](../Page/陳萬雄.md "wikilink")
[陳潤根](../Page/陳潤根.md "wikilink")　[許賢發](../Page/許賢發.md "wikilink")　[高苕華](../Page/高苕華.md "wikilink")　　[梁秉中](../Page/梁秉中.md "wikilink")　[梁欽榮](../Page/梁欽榮.md "wikilink")　[梁錦松](../Page/梁錦松.md "wikilink")　　[張家敏](../Page/張家敏.md "wikilink")　~~[張炳良](../Page/張炳良.md "wikilink")~~\[6\]　[黃　霑](../Page/黃霑.md "wikilink")　[葉天養](../Page/葉天養.md "wikilink")　[曾慶文](../Page/曾慶文.md "wikilink")　[曾澍基](../Page/曾澍基.md "wikilink")　[馮檢基](../Page/馮檢基.md "wikilink")
[楊孫西](../Page/楊孫西.md "wikilink")　[蔡偉石](../Page/蔡偉石.md "wikilink")　[鄭明訓](../Page/鄭明訓.md "wikilink")　　[鄭海泉](../Page/鄭海泉.md "wikilink")　[鄭耀宗](../Page/鄭耀宗.md "wikilink")　[鄧兆棠](../Page/鄧兆棠.md "wikilink")　　[潘宗光](../Page/潘宗光.md "wikilink")　[劉江華](../Page/劉江華.md "wikilink")　[霍震寰](../Page/霍震寰.md "wikilink")　[鍾期榮](../Page/鍾期榮.md "wikilink")　[鍾瑞明](../Page/鍾瑞明.md "wikilink")

  - 第四批（45人，1995年4月28日聘請）

[方　鏗](../Page/方鏗.md "wikilink")　[丘福雄](../Page/丘福雄.md "wikilink")　[李乃堯](../Page/李乃堯.md "wikilink")　[李　宗](../Page/李宗.md "wikilink")　[李宗德](../Page/李宗德.md "wikilink")　[李東海](../Page/李東海.md "wikilink")　[李健鴻](../Page/李健鴻.md "wikilink")　[李家祥](../Page/李家祥_\(香港\).md "wikilink")　[李倫傑](../Page/李倫傑.md "wikilink")　[李樂詩](../Page/李樂詩_\(探險家\).md "wikilink")　[吳文拱](../Page/吳文拱.md "wikilink")　[吳連烽](../Page/吳連烽.md "wikilink")　[吳思遠](../Page/吳思遠.md "wikilink")
[吳樹熾](../Page/吳樹熾.md "wikilink")　[何　弢](../Page/何弢.md "wikilink")　[何承天](../Page/何承天_\(建築師\).md "wikilink")　[呂　辛](../Page/呂辛.md "wikilink")　[呂明華](../Page/呂明華.md "wikilink")　[周子京](../Page/周子京.md "wikilink")　[侯叔祺](../Page/侯叔祺.md "wikilink")　[施學概](../Page/施學概.md "wikilink")　[陳立志](../Page/陳立志.md "wikilink")　[陳弘毅](../Page/陳弘毅.md "wikilink")　[陳金烈](../Page/陳金烈.md "wikilink")　[陳祖澤](../Page/陳祖澤.md "wikilink")　[唐鏡升](../Page/唐鏡升.md "wikilink")
[黃士心](../Page/黃士心.md "wikilink")　[黃守正](../Page/黃守正.md "wikilink")　[黃定光](../Page/黃定光.md "wikilink")　[黃金富](../Page/黃金富.md "wikilink")　[黃國礎](../Page/黃國礎.md "wikilink")　[張妙清](../Page/張妙清.md "wikilink")　[許冠文](../Page/許冠文.md "wikilink")　[莊紹綏](../Page/莊紹綏.md "wikilink")　[梅嶺昌](../Page/梅嶺昌.md "wikilink")　[程介明](../Page/程介明.md "wikilink")　[馮志堅](../Page/馮志堅.md "wikilink")　[費明儀](../Page/費明儀.md "wikilink")　[葉國華](../Page/葉國華.md "wikilink")
[楊啟彥](../Page/楊啟彥.md "wikilink")　[趙世彭](../Page/趙世彭.md "wikilink")　[劉榮廣](../Page/劉榮廣.md "wikilink")　[廖烈科](../Page/廖烈科.md "wikilink")　[謝中民](../Page/謝中民.md "wikilink")　[饒餘慶](../Page/饒餘慶.md "wikilink")

## 另見

  - [香港地區事務顧問](../Page/香港地區事務顧問.md "wikilink")
  - [香港親共人士](../Page/香港親共人士.md "wikilink")
  - [香港地區全國人大代表](../Page/香港地區全國人大代表.md "wikilink")
  - [香港地區全國政協委員](../Page/香港地區全國政協委員.md "wikilink")

## 参考文獻及注釋

[Category:香港過渡時期史](../Category/香港過渡時期史.md "wikilink")
[Category:香港特定人群称谓](../Category/香港特定人群称谓.md "wikilink")
[Category:香港政治人物](../Category/香港政治人物.md "wikilink")
[Category:中华人民共和国特定人群称谓
(政治术语)](../Category/中华人民共和国特定人群称谓_\(政治术语\).md "wikilink")

1.
2.  \#p.210 張浚生訪談錄
3.  [經典龍門陣---馮檢基part6](http://www.youtube.com/watch?v=mdWjumVg9Sk)
4.
5.
6.  張炳良獲聘後不久，因匯點宣佈與香港民主同盟合併為民主黨，張炳良加入民主黨，即被除名。