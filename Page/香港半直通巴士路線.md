**半直通路線**是指[香港的巴士路線](../Page/香港.md "wikilink")，定線上會行經有巴士站的普通道路，但不會停站或只作有限停站的路線。

## 半直通路線的歷史

**14號線**是[九巴首條以](../Page/九巴.md "wikilink")「半直通路線」型式運作的[九龍市區路線](../Page/九龍.md "wikilink")。在1960年代（[六七暴動前後](../Page/六七暴動.md "wikilink")），[觀塘區正急速發展](../Page/觀塘區.md "wikilink")，但往來[觀塘至](../Page/觀塘.md "wikilink")[九龍西部及南部](../Page/九龍西.md "wikilink")，沒有[東九龍走廊或](../Page/東九龍走廊.md "wikilink")[機場隧道之類的特快途徑](../Page/機場隧道.md "wikilink")，[地鐵亦未興建](../Page/香港地鐵.md "wikilink")，[公共小巴亦未曾合法化](../Page/香港小巴.md "wikilink")（1969年才正式合法化），[基層市民出入一定要乘搭](../Page/基層市民.md "wikilink")[巴士](../Page/香港巴士.md "wikilink")。加上觀塘區不斷有[工廠及](../Page/工廠.md "wikilink")[住宅區落成](../Page/住宅區.md "wikilink")，通勤人流急速上升，因此需要增闢巴士路線，專門服務觀塘區至[九龍城以西的市區](../Page/九龍城.md "wikilink")，九巴於1967年9月至1968年4月期間，便開辦四條以「半直通路線」形式運作的路線，包括：

  - 13D
    [秀茂坪](../Page/秀茂坪.md "wikilink")←→[旺角碼頭](../Page/旺角碼頭.md "wikilink")
  - 14
    [油塘](../Page/油塘.md "wikilink")←→[佐敦道碼頭](../Page/佐敦道碼頭.md "wikilink")
  - 14A 佐敦道碼頭←→[觀塘](../Page/觀塘.md "wikilink")，只在1967年9月至1968年4月期間服務，後來取消
  - 15
    [藍田](../Page/藍田_\(香港\).md "wikilink")（北）←→[紅磡碼頭](../Page/紅磡碼頭.md "wikilink")

它們的特點在於[觀塘道近](../Page/觀塘道.md "wikilink")[勵業街至](../Page/勵業街.md "wikilink")[九龍城迴旋處一段不設車站](../Page/世運公園.md "wikilink")（13D更要到[亞皆老街近](../Page/亞皆老街.md "wikilink")[窩打老道後才有車站](../Page/窩打老道.md "wikilink")）。

「半直通路線」可謂[香港早期的](../Page/香港.md "wikilink")[市區特快路線](../Page/市區特快路線.md "wikilink")，初時為疏導上下班及上下課通勤[乘客而設](../Page/乘客.md "wikilink")，當時巴士服務供不應求情況非常嚴重。直到1980年代前期，市民已普遍接受[地鐵](../Page/香港地鐵.md "wikilink")，巴士乘客銳減，因此九巴於1982年8月8日起，將該三條路線取消「半直通路線」稱呼，於原先不停站的路段增設車站，開放給沿線乘客上落客，以吸引流失客源回流。三個月後（1982年11月26日），香港才有首批行走[快速公路的特別路線九巴](../Page/快速公路.md "wikilink")14X、15X投入服務，奠定今日特快線的基礎。

在佔領期間，有些路線改道暫時變為「半直通路線」，如41/45/E21A由黃竹街至培正道不設站直上何文田；102/118/171由黃竹街至理大不設站直往紅隧口；6C由欽州街至衛理道不設站直往紅磡，車程比平常快了不少。

需注意途經內街而未有「站站停」的未必是半直通路線，兩個站距極接近的中途站只選停一站不可視作半直通。例如[過海隧道巴士116線和](../Page/過海隧道巴士116線.md "wikilink")[過海隧道巴士302A線與走線相同的其他路線不同](../Page/過海隧道巴士302A線.md "wikilink")，不在[保良局第一張永慶中學設站](../Page/保良局第一張永慶中學.md "wikilink")，[德愛中學後下一站已是](../Page/德愛中學.md "wikilink")[鑽石山火葬場](../Page/鑽石山火葬場.md "wikilink")（其他如[九龍巴士3D線則三站均停](../Page/九龍巴士3D線.md "wikilink")），但由於[保良局第一張永慶中學和](../Page/保良局第一張永慶中學.md "wikilink")[鑽石山火葬場兩個分站極近](../Page/鑽石山火葬場.md "wikilink")，實際上沒有半直通效果。

## 中巴及新巴的應用

港島歷史上有許多的特快巴士服務，其中以[東區最多](../Page/東區.md "wikilink")，理由是由[東區前往](../Page/東區.md "wikilink")[中環](../Page/中環.md "wikilink")，必須穿越人煙綢密的[灣仔區](../Page/灣仔區.md "wikilink")，從[柴灣經](../Page/柴灣.md "wikilink")[軒尼詩道前往](../Page/軒尼詩道.md "wikilink")[中環必定會使行車時間超過一小時](../Page/中環.md "wikilink")。東區是港島最密集的住宅區，自然需要有效率的巴士路線往返商業區。另一方面，中巴把這些路線當作對抗紅色公共小巴和地鐵的最佳武器，姑勿論最後結果如何，這些路線仍有一定存在價值。由於這些路線在當時[中巴的定義是屬於特別班次](../Page/中巴.md "wikilink")，所以其巴士站站牌絕大部份都不是一般路線的<span style="color: black; background: white;">白底黑字</span>，途經[告士打道的路線多為](../Page/告士打道.md "wikilink")<span style="color: red; background: yellow;">黃底紅字</span>。有黃色站牌的路線被統稱為「市區半直通路線」，俗稱「黃（皇）牌線」，顏色繼承自2號、4號和10號線的特別快車班次。

### 特別快車

港島最早的特快線於1967年9月25日投入服務，乃是[中巴10線的輔助路線](../Page/城巴10線.md "wikilink")，並無編號，來往及[中環街市](../Page/中環街市.md "wikilink")，雖途經[軒尼詩道](../Page/軒尼詩道.md "wikilink")，但沿途只停數站，於每天早上至傍晚服務。而對岸[九巴最早的市區半直通路線](../Page/九巴.md "wikilink")線只比此路線早一天開辦，兩者服務形式類同。中巴眼見試驗成功，於翌年10月21日，增設一條無編號半直通路線輔助[2號線](../Page/新巴2號線.md "wikilink")，來往筲箕灣愛秩序街及[中環街市](../Page/中環街市.md "wikilink")，亦是途經軒尼詩道和沿途有限度停站，但只在早上、中午及傍晚服務，後來增加星期六上午服務。

因為曾經有乘客把沒路線牌、途經北角的半直通路線車輛誤認為10號，中巴就在行走這兩線的巴士都同時掛上黃底黑字的「<span style="font-weight: bold; color: black; background: yellow;">特別快車EXPRESS</span>」牌，後來的黃牌就傳承了這一做法；但是路線仍沒有編號，乘客仍然感到混亂。中巴最後在1970年初為[北角和](../Page/北角.md "wikilink")[筲箕灣的半直通路線分別正名為](../Page/筲箕灣.md "wikilink")10號線特別快車及2號線特別快車，以茲識別。後來中巴於1971年因應[南區交通需求](../Page/南區.md "wikilink")，開辦[4號線特別快車](../Page/新巴4號線.md "wikilink")，服務形式和東區兩條特別快車大同小異。

### 市區半直通路線

上述所謂的特別快車路線，與常規路線的差異只在少停站，除此以外仍會受交通擠塞之苦，加上收取車費較高昂（三毫）又不設優惠，特別快車的客量流失向紅色小巴。其後[中巴停辦特別快車](../Page/中巴.md "wikilink")，以一系列市區半直通路線（又名市區特快路線，Urban
Express
Route）取代，除[40線開辦時和](../Page/城巴40線.md "wikilink")4號線特別快車沒有太大分別外，所有東區半直通路線都改經[清風街天橋](../Page/清風街天橋.md "wikilink")、[維園道及](../Page/維園道.md "wikilink")[告士打道](../Page/告士打道.md "wikilink")，避過[灣仔區的繁忙路段](../Page/灣仔區.md "wikilink")，縮短行車時間。

這些市區半直通路線開辦時，地點和數字布牌都是<span style="color: red; background: yellow;">黃布紅字</span>，和豪華巴士路線共用，但到八十年代起停用，改用普通的<span style="color: white; background: black;">黑布白字</span>。

### 半直通的沒落

由於[東區的半直通路線與地鐵](../Page/東區.md "wikilink")[港島綫嚴重重疊](../Page/港島綫.md "wikilink")，收費卻比[電車和流水線昂貴](../Page/香港電車.md "wikilink")，又比不上[港島綫和](../Page/港島綫.md "wikilink")[東區走廊特快線快捷](../Page/東區走廊.md "wikilink")，且部分路線車費大小同價，並且普遍只於平日日間行走，結果半直通路線網絡在1990年代起不斷萎縮，僅存兼顧西區的線，但該線於[西港島綫通車後亦難逃被重創的命運](../Page/西港島綫.md "wikilink")，改為只於繁忙時間服務，不再服務西區。由和線取代其服務。[南區的半直通路線則因為未受地鐵影響](../Page/南區.md "wikilink")，仍然能維持服務，其中線客量仍然高企。

[城巴由](../Page/城巴.md "wikilink")1991年踏足專營巴士業務時，車費等級表已沒有類似市區特快或半直通路線的類別。該公司於1993和1995年先後接辦、及線時，即時將有關路線改為全空調服務，並改用全空調路線的站牌貼紙，故此[城巴並沒有](../Page/城巴.md "wikilink")「市區半直通路線」的概念存在。然而在2006年由嘉湖山莊居民巴士和專利化而成的，卻獲城巴絕無僅有地冠以「限定車站特快服務」（Limited
Stop Express Service）之稱號，當中「Limited
Stop」（有限度停站）意為半直通，成為至今城巴惟一以半直通作招徠的路線。

[新巴獲得專營權後](../Page/新巴.md "wikilink")，由其首份車費等級表起已取消「半直通路線」這個收費級別\[1\]，但營運初期仍繼承[中巴的傳統](../Page/中巴.md "wikilink")，保留全部原有半直通路線的黃底紅字站牌。而由[新巴全新開辦的路線](../Page/新巴.md "wikilink")，除了第一代4X、及外，則再沒有採用這種配色。[新巴30X線理論上完全符合半直通路線最傳統的定義](../Page/新巴30X線.md "wikilink")，但是新巴於30X線投入服務時已廢去此一繼承自中巴的做法，直接以「[四號幹線特快](../Page/四號幹線.md "wikilink")」作招徠。

2010年[4月17日起](../Page/4月17日.md "wikilink")，線延長至[瑞安中心](../Page/瑞安中心.md "wikilink")，站牌標示亦由原本的黃底紅字改為普通的，從半直通路線降格為普通路線，亦揭開了[新巴將旗下港島路線統一顏色的序幕](../Page/新巴.md "wikilink")。繼同為半直通路線的線從原有的黃底紅字改為普通線的紫底白字後，和線亦於同年5月15日生效的乳豬紙上更換了路線編號配色，而站牌上的編號貼紙亦自5月下旬起陸續更換，新巴的半直通路線稱號因而走進歷史。

## 現在的半直通路線

由於現在的[快速公路網絡已大大改善](../Page/快速公路.md "wikilink")，很少特快路線需要使用普通道路作半直通運作，但仍有少量路線保留半直通運作。
現在的半直通路線有：

  - [九龍巴士2X線](../Page/九龍巴士2X線.md "wikilink")，方向於[太子道東近](../Page/太子道東.md "wikilink")[采頤花園至](../Page/采頤花園.md "wikilink")[太子道西近](../Page/太子道西.md "wikilink")[拔萃男書院之間不設站](../Page/拔萃男書院.md "wikilink")；方向於[亞皆老街旺角段後至](../Page/亞皆老街.md "wikilink")[太子道東之間有限度停站](../Page/太子道東.md "wikilink")。
  - [九龍巴士13P線](../Page/九龍巴士13P線.md "wikilink")，由[九龍灣起](../Page/九龍灣.md "wikilink")，至[亞皆老街近](../Page/亞皆老街.md "wikilink")[怡安閣不設分站](../Page/怡安閣.md "wikilink")
  - [九龍巴士15X線](../Page/九龍巴士15X線.md "wikilink")，往藍田方向，由[亞皆老街球場至](../Page/亞皆老街球場.md "wikilink")[啟業邨不設分站](../Page/啟業邨.md "wikilink")；往紅磡方向則經[啟德隧道](../Page/啟德隧道.md "wikilink")
  - [九龍巴士58P線](../Page/九龍巴士58P線.md "wikilink")，[屯門公路只設](../Page/屯門公路.md "wikilink")[屯門公路轉車站一站](../Page/屯門公路轉車站.md "wikilink")。
  - [九龍巴士80A線](../Page/九龍巴士80線.md "wikilink")、[九龍巴士268P線](../Page/九龍巴士268C線.md "wikilink")、[九龍巴士269S線](../Page/九龍巴士269C線.md "wikilink")，[龍翔道只停](../Page/龍翔道.md "wikilink")[沙田㘭道一站](../Page/沙田㘭道.md "wikilink")。
  - [九龍巴士234C線](../Page/九龍巴士234C線.md "wikilink")，[龍翔道只停](../Page/龍翔道.md "wikilink")[畢架山花園和](../Page/畢架山花園.md "wikilink")[豐力樓兩站](../Page/豐力樓.md "wikilink")。
  - [九龍巴士234D線](../Page/九龍巴士234D線.md "wikilink")，[龍翔道只停](../Page/龍翔道.md "wikilink")[畢架山花園](../Page/畢架山花園.md "wikilink")、[豐力樓和](../Page/豐力樓.md "wikilink")[沙田坳道三站](../Page/沙田坳道.md "wikilink")。
  - [九龍巴士290線](../Page/九龍巴士290線.md "wikilink")，由[順利邨道至](../Page/順利邨道.md "wikilink")[秀茂坪道不設分站](../Page/秀茂坪道.md "wikilink")，龍翔道只停[黃大仙及](../Page/黃大仙.md "wikilink")[彩虹兩個分站](../Page/彩虹.md "wikilink")
  - [九龍巴士290A線](../Page/九龍巴士290A線.md "wikilink")，龍翔道只停[黃大仙及](../Page/黃大仙.md "wikilink")[彩虹兩個分站](../Page/彩虹.md "wikilink")
  - [九龍巴士296C線](../Page/九龍巴士296C線.md "wikilink")，太子道東至旺角之間有限度設站
  - [九龍巴士296P線](../Page/九龍巴士296P線.md "wikilink")，[旺角](../Page/旺角.md "wikilink")－道方向於[太子道東近](../Page/太子道東.md "wikilink")[采頤花園至](../Page/采頤花園.md "wikilink")[太子道西近](../Page/太子道西.md "wikilink")[拔萃男書院之間不設站](../Page/拔萃男書院.md "wikilink")；方向於旺角至太子道東之間有限度設站
  - [新巴30X線](../Page/新巴30X線.md "wikilink")，[薄扶林道只停](../Page/薄扶林道.md "wikilink")[李陞小學](../Page/李陞小學.md "wikilink")（只限回程）、[香港大學](../Page/香港大學.md "wikilink")、[蒲飛路](../Page/蒲飛路.md "wikilink")（只限回程）、[瑪麗醫院](../Page/瑪麗醫院.md "wikilink")、[薄扶林水塘道](../Page/薄扶林水塘道.md "wikilink")（只限回程）、[薄扶林村和](../Page/薄扶林村.md "wikilink")[余振強紀念中學合共四個](../Page/余振強紀念中學.md "wikilink")（去程）或七個（回程）分站
  - [過海隧道巴士969B線](../Page/過海隧道巴士969B線.md "wikilink")，主班次於[翠湖居至](../Page/翠湖居.md "wikilink")[麗湖居之間的](../Page/麗湖居.md "wikilink")[天瑞路及](../Page/天瑞路.md "wikilink")[天華路不設站](../Page/天華路.md "wikilink")，所有班次往灣仔方向於中環只停[中環街市和](../Page/中環街市.md "wikilink")[皇后像廣場分站](../Page/皇后像廣場.md "wikilink")，往天水圍方向則途經[林士街天橋](../Page/林士街天橋.md "wikilink")。（此線被稱為「限定車站特快服務」）
  - [城巴E22A線往](../Page/城巴E22A線.md "wikilink")[機場方向](../Page/香港國際機場.md "wikilink")，由[將軍澳隧道至](../Page/將軍澳隧道.md "wikilink")[鑽石山不設分站](../Page/鑽石山.md "wikilink")
  - [新巴81S線和](../Page/新巴81S線.md "wikilink")[城巴85P線往](../Page/城巴85P線.md "wikilink")[寶馬山方向](../Page/寶馬山.md "wikilink")，於[英皇道只停](../Page/英皇道.md "wikilink")[新時代廣場分站](../Page/新時代廣場.md "wikilink")

## 註釋及參考資料

[半直通巴士路線](../Category/香港巴士路線種類.md "wikilink")

1.  [1998-09-01生效，載於619 Bus
    Page的新巴車費等級表](https://web.archive.org/web/19990221061900/http://home.bre.polyu.edu.hk/~bs509898/nwfbfare.htm)