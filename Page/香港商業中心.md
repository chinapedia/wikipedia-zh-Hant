[Hong_Kong_Plaza_Entrance_Void_view_2016.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Plaza_Entrance_Void_view_2016.jpg "fig:Hong_Kong_Plaza_Entrance_Void_view_2016.jpg")
**香港商業中心**（****）為[香港](../Page/香港.md "wikilink")[中西區](../Page/中西區_\(香港\).md "wikilink")[西環](../Page/西環.md "wikilink")[干諾道西](../Page/干諾道西.md "wikilink")188號的小型[商場及商廈](../Page/商場.md "wikilink")。大廈樓高42層，佔地25,977平方呎，於1984年落成，發展商為[新鴻基地產](../Page/新鴻基地產.md "wikilink")、利興置業有限公司、怡華置業有限公司和益新置業有限公司，曾經是[西環最高的](../Page/西環.md "wikilink")[建築物](../Page/建築物.md "wikilink")，前身是[香港人造花廠](../Page/香港人造花廠.md "wikilink")。

大廈正門位於[德輔道西](../Page/德輔道西.md "wikilink")，為[石塘咀繼](../Page/石塘咀.md "wikilink")[創業中心後代表建築物之一](../Page/創業中心.md "wikilink")。

## 商場

商場樓高兩層，於2016年進行翻新。到2017年2月開設全球首間以[Hello
Kitty為主題的](../Page/Hello_Kitty.md "wikilink")[一田超級市場](../Page/一田百貨.md "wikilink")。

## 寫字樓租戶

  - [太平洋恩利](../Page/太平洋恩利.md "wikilink")（SEHK \#1174）\[1\]
  - [新城市管理服務有限公司](../Page/新城市管理服務有限公司.md "wikilink") \[2\]
  - [富士達(香港)有限公司](../Page/富士達.md "wikilink")

<File:HK> Shek Tong Tsui Des Voeux Road West Hong Kong Plaza Shopping
Mall front door 1.JPG|翻新前商場正門 <File:Hong> Kong Plaza Entrance void
201603.jpg|商場入口中庭壁畫，在2017年翻新後消失 <File:Hong> Kong Plaza Office lobby
201603.jpg|寫字樓大堂 Hong Kong Plaza Level 1 goji warrior 2018.jpg|1樓goji
warrior健身中心

## 鄰近地點

  - [太平洋廣場](../Page/太平洋廣場.md "wikilink")
  - [均益大廈](../Page/均益大廈.md "wikilink")1期
  - [香港今旅](../Page/香港今旅.md "wikilink")

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{港島綫色彩}}">█</font><a href="../Page/港島綫.md" title="wikilink">港島綫</a>：<a href="../Page/香港大學站.md" title="wikilink">香港大學站B</a>2出口</li>
</ul>
<dl>
<dt><a href="../Page/香港電車.md" title="wikilink">香港電車</a></dt>

</dl>
<ul>
<li><a href="../Page/屈地街總站.md" title="wikilink">屈地街總站</a></li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 人流

香港商業中心商場的遊人，都是其上層[寫字樓的文職人員](../Page/寫字樓.md "wikilink")，及附近的街坊。此大廈沒有大[酒樓或者著名](../Page/酒樓.md "wikilink")[餐廳](../Page/餐廳.md "wikilink")，[人流來自南面毗鄰的太平洋廣場](../Page/人流.md "wikilink")，及東面[香港賽馬會投注站的](../Page/香港賽馬會.md "wikilink")[顧客](../Page/顧客.md "wikilink")。[大家樂和](../Page/大家樂.md "wikilink")[大快活](../Page/大快活.md "wikilink")[下午茶餐特價食客也不少](../Page/下午茶.md "wikilink")。

## 外部連結

  - [香港商業中心](http://www.shkp.com/zh-hk/scripts/property/property_mall_hkplaza.php)

## 參考文獻

[category:新鴻基地產物業](../Page/category:新鴻基地產物業.md "wikilink")

[Category:中西區商場 (香港)](../Category/中西區商場_\(香港\).md "wikilink")
[Category:石塘咀](../Category/石塘咀.md "wikilink")

1.
2.  <http://www.newcityhk.com/contact.html>