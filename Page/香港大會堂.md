**香港大會堂**（）是[香港第一座公共文娛中心](../Page/香港.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[中西區](../Page/中西區_\(香港\).md "wikilink")[中環](../Page/中環.md "wikilink")[愛丁堡廣場](../Page/愛丁堡廣場.md "wikilink")5號，於1962年3月2日落成開幕，現由[康樂及文化事務署管理](../Page/康樂及文化事務署.md "wikilink")。

香港大會堂跟同樣位於[愛丁堡廣場的](../Page/愛丁堡廣場.md "wikilink")[郵政總局](../Page/香港郵政總局.md "wikilink")、昔日第三代的[中環天星碼頭及](../Page/中環天星碼頭.md "wikilink")[停車場](../Page/天星碼頭多層停車場.md "wikilink")、及已拆卸的[皇后碼頭屬同一時期的建築](../Page/皇后碼頭.md "wikilink")，並形成一個大眾市民的[公共空間](../Page/公共空間.md "wikilink")。

## 歷史

[Old_City_Hall_HK.jpg](https://zh.wikipedia.org/wiki/File:Old_City_Hall_HK.jpg "fig:Old_City_Hall_HK.jpg")
[舊香港大會堂於](../Page/舊香港大會堂.md "wikilink")1869年落成，位於[香港匯豐總行大廈現址旁](../Page/香港匯豐總行大廈.md "wikilink")。1933年，舊大會堂的用地被購予[香港上海匯豐銀行重建並擴充其第三代總行大廈之用](../Page/香港上海匯豐銀行.md "wikilink")。舊大會堂於1947年全部被拆卸，部分土地則為1950年興建[中國銀行大廈之用](../Page/中國銀行大廈_\(香港\).md "wikilink")。

1950年，中英協會敦促政府興建新大會堂，政府其後委託[市政局參與策劃建築物的設計與發展](../Page/市政局_\(香港\).md "wikilink")，包括設立博物館、公共圖書館和演藝場館等。最後選址於[中環](../Page/中環.md "wikilink")[愛丁堡廣場的](../Page/愛丁堡廣場.md "wikilink")[填海地上](../Page/填海.md "wikilink")。呈現[包浩斯建築風格的新大會堂於](../Page/包豪斯.md "wikilink")1962年3月2日落成啟用，建築費高達2,000萬港元，由當時的[香港總督](../Page/香港總督.md "wikilink")[柏立基](../Page/柏立基.md "wikilink")[爵士主持開幕禮](../Page/爵士.md "wikilink")，於3月4日由[倫敦愛樂樂團在音樂廳舉行揭幕音樂會](../Page/倫敦愛樂樂團.md "wikilink")。同年夏天[颱風溫黛襲港](../Page/颱風溫黛_\(1962年\).md "wikilink")，使大會堂內部出現輕微水浸。

1960年代落成的香港大會堂與鄰近一系列新型公共建築如[中環天星碼頭](../Page/中環天星碼頭.md "wikilink")、[卜公碼頭一般](../Page/卜公碼頭.md "wikilink")，設計講求簡樸實用，而凸顯統治者地位的殖民地式、歐式建築逐步走進歷史\[1\]。

香港大會堂曾經是官方儀式和慶典舉行的重要場地，殖民地時代的多位港督都在大會堂舉行宣誓就職，而接待訪港的英國皇室成員的歡迎活動，都是在香港大會堂及對開的愛丁堡廣場進行。現時每年的法律年度開啟典禮都在1月於香港大會堂及對開的愛丁堡廣場進行。

基於大會堂的重要歷史地位，[古物古蹟辦事處於](../Page/古物古蹟辦事處.md "wikilink")2009年將其列為[一級歷史建築](../Page/一級歷史建築.md "wikilink")。\[2\]

## 建築風格

新大會堂選擇以經典[包浩斯建築風格設計](../Page/包豪斯.md "wikilink")，整體主張[功能主義](../Page/功能主義建築.md "wikilink")，偏向僕素而非浮誇：大樓使用簡潔的外觀、俐落的線條，與多功能的設施相互配合。同時採用突破傳統的[現代建築風格設計](../Page/现代主义建筑.md "wikilink")，是[二戰後期相當流行的設計意念](../Page/第二次世界大战.md "wikilink")。大會堂呈現的[國際風格亦在香港建築界曾掀起一股潮流](../Page/國際風格.md "wikilink")。\[3\]

## 翻新工程

香港大會堂於1993年進行了大規模翻新工程，在設計上保留原來包浩斯式的建築風格，再增添新元素，令香港大會堂更富時代感。翻新範圍包括高座及低座大堂。亦將大會堂紀念花園重新設計，引用中軸線概念以彰顯紀念龕的重要角色，並以動態區和靜態區劃分空間布局，藉此優化功能。\[4\]

2010年進行翻新工程為翻新低座[美心皇宮及展覽廳](../Page/美心皇宮.md "wikilink")。

## 文化藝術推廣

大會堂在本地文化藝術的推廣工作扮演重要角色。香港大會堂高座設有香港首間公共圖書館（即[大會堂公共圖書館](../Page/大會堂公共圖書館.md "wikilink")）和美術館，後者設於大會堂高座10樓及11樓，1969年更名為香港美術博物館，1975年正式分家成香港博物館（現稱[香港歷史博物館](../Page/香港歷史博物館.md "wikilink")）及[香港藝術館](../Page/香港藝術館.md "wikilink")。兩館於1975年及1991年分別遷離大會堂，而香港視覺藝術中心亦因原址於1988至1990年間飽和而遷往1992年建成之香港公園（第二期）。

文化表演方面，1973年的第一屆[香港藝術節](../Page/香港藝術節.md "wikilink")、1976年的第一屆亞洲藝術節、1977年的第一屆[香港國際電影節](../Page/香港國際電影節.md "wikilink")、1982年的第一屆[國際綜藝合家歡等本地大型文化節目](../Page/國際綜藝合家歡.md "wikilink")，皆在大會堂揭幕登場。不少蜚聲國際的藝團和藝人皆曾獲邀在大會堂獻技，大會堂成為香港市民接觸到世界各地不同藝術表演的主要管道，為香港近代的文化藝術發展作出重要貢獻。

2012年5月19日起至6月11日全球首個城市舉辦「情陷聖羅蘭」展覽，展出87件由時裝大師聖羅蘭（Yves Saint
Laurent）設計的經典服飾，以及12張攝影大師Jeanloup Sieff的照片。

2013年5月4日至6月9日舉辦「尚‧高克多20世紀巴黎景觀的魅影」展覽

香港大會堂是[幻彩詠香江的參與大廈之一](../Page/幻彩詠香江.md "wikilink")。

## 設施

香港大會堂佔地11,100平方米，由兩座獨立的建築物（低座及高座）及一個紀念花園組成。
[Hong_Kong_City_Hall_View_2014.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_City_Hall_View_2014.jpg "fig:Hong_Kong_City_Hall_View_2014.jpg")
[Hong_Kong_City_Hall_Lobby_201411.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_City_Hall_Lobby_201411.jpg "fig:Hong_Kong_City_Hall_Lobby_201411.jpg")

  - 低座設施：

<!-- end list -->

1.  設有1,434個座位，可供[交響樂及](../Page/交響樂.md "wikilink")[室樂演奏](../Page/室樂.md "wikilink")、獨奏、[爵士樂](../Page/爵士樂.md "wikilink")、[歌劇](../Page/歌劇.md "wikilink")、合唱及獨唱演出的音樂廳
2.  設有463個座位，適合小型舞台表演的劇院
3.  面積約590平方米的展覽廳（原為跳舞廳）
4.  [城市售票網售票處](../Page/城市售票網.md "wikilink")
5.  詢問處
6.  三所食肆（[美心集團營運兩所西餐廳及大會堂](../Page/美心集團.md "wikilink")[美心皇宮](../Page/美心皇宮.md "wikilink")）
7.  演藝禮品店
8.  停車場
9.  附屬建築物：1976年5月18日，毗鄰大會堂低座的市政局大樓啟用，內設市政局秘書處及會議廳等非開放設施，2012年經翻新後成為[展城館的館址](../Page/展城館.md "wikilink")。

[Hong_Kong_City_Hall_Theatre_201205.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_City_Hall_Theatre_201205.jpg "fig:Hong_Kong_City_Hall_Theatre_201205.jpg")

  - 高座設施：

<!-- end list -->

1.  大會堂婚姻登記處
2.  [大會堂公共圖書館](../Page/大會堂公共圖書館.md "wikilink")
3.  兩個可以容納40個座位的會議室
4.  容納111個座位，設有半圓型舞台的演奏廳
5.  面積約260平方米的展覽館
6.  一所食肆（[美心集團營運的快餐店](../Page/美心集團.md "wikilink")[MX](../Page/美心MX.md "wikilink")）

<File:The> City Hall High Block Night View 201409.jpg|香港大會堂高座夜景
<File:HK> Central City Hall floors.JPG|香港大會堂高座樓層圖 <File:Hong> Kong City
Hall Library Entrance
201402.jpg|[大會堂公共圖書館](../Page/大會堂公共圖書館.md "wikilink")10樓入口
<File:The> City Hall High Block Stairs 201402.jpg|高座樓梯可享維港美景
<File:Queen's> Pier at
night.jpg|香港大會堂低座及[皇后碼頭](../Page/皇后碼頭.md "wikilink")

  - [大會堂紀念花園](../Page/香港大會堂紀念花園.md "wikilink")：

為紀念在[香港保衛戰及](../Page/香港保衛戰.md "wikilink")[日佔時期捐躯的軍民而建](../Page/香港日佔時期.md "wikilink")，在花園中央建有一座十二邊形的紀念龕，裡面放置了陣亡者名冊和鑄刻陣亡隊伍名字的木匾，牆上並鑲有「英靈不滅、浩氣長存」中文字樣及「These
had seen movement and heard music; known slumber and waking; loved; gone
proundly friended」的英文字樣。紀念花園內亦放置多件藝術雕塑。

此外，[市政局於](../Page/市政局_\(香港\).md "wikilink")1997年[香港主權移交時曾埋下一個](../Page/香港主權移交.md "wikilink")[時光錦囊](../Page/時光錦囊.md "wikilink")，原定於2007年出土，但因市政局於2000年被殺局，時光錦囊遂遭遺忘\[5\]。該時光錦囊最終於2012年3月28日開啟。\[6\]

<File:The> City Hall Memorial Garden 2012.jpg|香港大會堂紀念花園 <File:The> City
Hall Memorial Garden View 2012.jpg|香港大會堂紀念花園 <File:The> City Hall
Memorial Garden Memorial Shrine 2012.jpg|紀念龕
[File:十二邊形紀念龕.JPG|紀念龕夜景](File:十二邊形紀念龕.JPG%7C紀念龕夜景)
<File:HK> CityHallMemorialGarden
Corridor.JPG|圍繞花園的「L」形行人走廊，上方平台是觀賞[維港的理想地點](../Page/維多利亞港.md "wikilink")

## 交通

[-HK_CityHall_SeaView_51217_3.jpg](https://zh.wikipedia.org/wiki/File:-HK_CityHall_SeaView_51217_3.jpg "fig:-HK_CityHall_SeaView_51217_3.jpg")
[City_Hall_Edinburgh_Place_signage_2017.jpg](https://zh.wikipedia.org/wiki/File:City_Hall_Edinburgh_Place_signage_2017.jpg "fig:City_Hall_Edinburgh_Place_signage_2017.jpg")

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")、<font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[中環站K出入口](../Page/中環站.md "wikilink")
  - <font color="{{東涌綫色彩}}">█</font>[東涌綫](../Page/東涌綫.md "wikilink")、<font color={{機場快綫色彩}}>█</font>[機場快綫](../Page/機場快綫.md "wikilink")：[香港站A出入口](../Page/香港站.md "wikilink")

<!-- end list -->

  - 行人天橋

<!-- end list -->

  - [中區行人天橋系統](../Page/中區行人天橋系統.md "wikilink")

<!-- end list -->

  - 停車場

<!-- end list -->

  - [天星碼頭多層停車場](../Page/天星碼頭多層停車場.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - 綠色[專線小巴](../Page/專線小巴.md "wikilink")

<!-- end list -->

  - 紅色[公共小巴](../Page/公共小巴.md "wikilink")

<!-- end list -->

  - 西環↔銅鑼灣
  - 荃灣↔灣仔

</div>

</div>

## 參見

  - **香港的其他同類設施**
      - [沙田大會堂](../Page/沙田大會堂.md "wikilink")
      - [荃灣大會堂](../Page/荃灣大會堂.md "wikilink")
      - [屯門大會堂](../Page/屯門大會堂.md "wikilink")

<!-- end list -->

  - **香港大會堂的周邊設施**
      - [愛丁堡廣場](../Page/愛丁堡廣場.md "wikilink")
      - [展城館](../Page/展城館.md "wikilink")

<!-- end list -->

  - [香港文娛活動](../Page/香港文娛活動.md "wikilink")

## 參考資料

**參考書目**

  - 香港市政局（1983）《香港大會堂1962-1982：回顧二十年》
  - 香港市政局（1987）《港藝薈萃：香港大會堂銀禧紀念》（ISBN 9627040231）
  - 香港市政局（1992）《香港大會堂三十週年紀念：藝萃薈聚三十年》
  - 明報（2002）《香港大會堂40週年：點滴情懷》

## 外部連結

  - [香港大會堂官方網站](http://www.cityhall.gov.hk)
  - [幻彩詠香江—香港大會堂](http://www.tourism.gov.hk/symphony/tc_chi/participating/participating_hk_15.html)
  - 香港電台經典重溫頻道
    [「香港大會堂開幕典禮」](http://www.rthk.org.hk/classicschannel/audio/60s_0003.asx)，1962年3月2日（廣播節目）
  - [「伴我成長四十年　香港大會堂回顧展今起舉行」](http://www.info.gov.hk/gia/general/200203/07/0307114.htm)
    香港特區政府，2002年3月7日
  - 香港政府新聞網 [「大會堂屆不惑之年
    殿堂地位無可代替」](https://web.archive.org/web/20070930024852/http://www3.news.gov.hk/isd/ebulletin/tc/category/healthandcommunity/030313/features/html/030313tc05006.htm)，2003年3月16日
  - 香港政府新聞網
    [「大會堂見證維港喜與樂」](https://web.archive.org/web/20070927211311/http://www3.news.gov.hk/isd/ebulletin/tc/category/healthandcommunity/030313/features/html/030313tc05010.htm)，2003年3月16日

{{-}}

[Category:香港表演場館](../Category/香港表演場館.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港地標](../Category/香港地標.md "wikilink")
[Category:幻彩詠香江](../Category/幻彩詠香江.md "wikilink")
[Category:香港一級歷史建築](../Category/香港一級歷史建築.md "wikilink")

1.
2.  [香港古物委員會：香港大會堂擬評為一級歷史建築](http://www.hkcna.hk/content/2009/0320/7257.shtml)
    香港中國通訊社 2009年03月20日
3.
4.  [香港大會堂](http://www.10mostlikedarchitecture.hk/page.php?76)
5.  [埋大會堂外 市局時光錦囊
    無人記得](http://news.hk.msn.com/local/article.aspx?cp-documentid=5291466)，《明報》，2011年7月15日
6.