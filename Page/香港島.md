[Hong_Kong_view_from_The_Peak_01.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_view_from_The_Peak_01.jpg "fig:Hong_Kong_view_from_The_Peak_01.jpg")上所拍攝的景觀，下半部為港島北岸一帶商業區\]\]
**香港島**（），簡稱**港島**或**香港**，與[九龍和](../Page/九龍.md "wikilink")[新界同為](../Page/新界.md "wikilink")[香港特区三大地域之一](../Page/香港.md "wikilink")。面積約78.64[平方公里](../Page/平方公里.md "wikilink")\[1\]，[最高峰是](../Page/香港山峰.md "wikilink")[太平山](../Page/太平山_\(香港\).md "wikilink")，[海拔](../Page/海拔.md "wikilink")552米，為[境內第二大島嶼](../Page/香港島嶼.md "wikilink")。香港島屬[萬山群島的一部份](../Page/萬山群島.md "wikilink")\[2\]。香港島是[香港開埠時最早發展的地區](../Page/香港開埠.md "wikilink")，當1842年香港成為[英國殖民地而](../Page/英國殖民地.md "wikilink")[維多利亞城又未落成時](../Page/維多利亞城.md "wikilink")，位於港島南區的[赤柱是當時的行政中心](../Page/赤柱.md "wikilink")。在[英國殖民地初期](../Page/香港開埠初期歷史.md "wikilink")，香港島北部人口密集，並劃為維多利亞城。香港島與[九龍半島由](../Page/九龍.md "wikilink")[維多利亞港分隔](../Page/維多利亞港.md "wikilink")，海岸是昔日貿易船隻停泊的地方。

香港島是香港的政治和經濟中心，[立法會綜合大樓](../Page/立法會綜合大樓.md "wikilink")、[政府總部和](../Page/政府總部_\(添馬艦\).md "wikilink")[終審法院皆位於香港島](../Page/香港終審法院.md "wikilink")；[中環](../Page/中環.md "wikilink")、[金鐘和](../Page/金鐘.md "wikilink")[銅鑼灣等主要商業區也位於香港島](../Page/銅鑼灣.md "wikilink")。而[地區劃分](../Page/香港行政區劃.md "wikilink")（例如[作為立法會選區](../Page/香港島選區.md "wikilink")）上的香港島，還包括[鴨脷洲](../Page/鴨脷洲.md "wikilink")、[大小青洲](../Page/大小青洲.md "wikilink")、[熨波洲](../Page/熨波洲.md "wikilink")、[銀洲等附屬島嶼](../Page/銀洲_\(南區\).md "wikilink")，即[中西區](../Page/中西區_\(香港\).md "wikilink")、[灣仔區](../Page/灣仔區.md "wikilink")、[東區和](../Page/東區_\(香港\).md "wikilink")[南區四區的總和](../Page/南區_\(香港\).md "wikilink")。

## 歷史

### 古代文化

香港島的[赤柱](../Page/赤柱.md "wikilink")、[舂坎角曾發現](../Page/舂坎角.md "wikilink")[新石器時代文物](../Page/新石器時代.md "wikilink")。

[秦始皇南攻](../Page/秦始皇.md "wikilink")[百越](../Page/百越.md "wikilink")，今之香港島始屬[南海郡](../Page/南海郡_\(中国\).md "wikilink")[番禺縣轄](../Page/番禺縣.md "wikilink")，至[西晉](../Page/西晉.md "wikilink")。[東晉](../Page/東晉.md "wikilink")[咸和六年](../Page/咸和.md "wikilink")（331年）後屬[寶安縣](../Page/寶安縣.md "wikilink")。根據《港島東區風物志》記載，[香港遊艇會](../Page/香港遊艇會.md "wikilink")（舊名燈籠洲，
又名[奇力島](../Page/奇力島.md "wikilink"))（今日[紅隧港島入口處](../Page/海底隧道_\(香港\).md "wikilink")）曾出土過隋唐至兩宋時期銅錢，估計至少在[隋](../Page/隋朝.md "wikilink")[唐年間](../Page/唐朝.md "wikilink")，島上已經有商業活動。至[明朝](../Page/明朝.md "wikilink")[萬曆元年](../Page/萬曆.md "wikilink")（1573年）為[新安縣領地](../Page/寶安縣.md "wikilink")，直至被清國[割讓給](../Page/割讓.md "wikilink")[英國](../Page/英國.md "wikilink")。

開埠前[香港島有其他別名](../Page/香港地名來源.md "wikilink")：中文有清國[乾隆九年](../Page/乾隆帝.md "wikilink")（1744年）《[海國聞見錄](../Page/海國聞見錄.md "wikilink")》中的「紅香爐」\[3\]，西方則最早1780年記載有「Fan-chin-chow」（范春洲）\[4\]。

[大清帝國連年積弱](../Page/大清帝國.md "wikilink")，在[鴉片戰爭中戰敗於](../Page/鴉片戰爭.md "wikilink")[英國](../Page/英國.md "wikilink")，英方於《[穿鼻草約](../Page/穿鼻草約.md "wikilink")》中要求將香港島割讓给英國，并与清廷在《[南京条约](../Page/南京条约.md "wikilink")》中正式确定割让。

### 英治時期

[Hong_Kong_Map_made_by_Edward_Belcher_in_1841.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Map_made_by_Edward_Belcher_in_1841.jpg "fig:Hong_Kong_Map_made_by_Edward_Belcher_in_1841.jpg")於英國皇家海軍炮艦[硫磺號上繪製](../Page/硫磺號.md "wikilink")\]\]
[Treaty_of_Nankin.jpg](https://zh.wikipedia.org/wiki/File:Treaty_of_Nankin.jpg "fig:Treaty_of_Nankin.jpg")》\]\]

1841年1月26日，[英國的駐華商務總監](../Page/英國.md "wikilink")（未獲清廷承認）暨[英國皇家海軍軍官](../Page/英國皇家海軍.md "wikilink")[查理·義律](../Page/查理·義律.md "wikilink")（Charles
Elliot）最先佔領香港島，並於[水坑口](../Page/水坑口.md "wikilink")（，意思是佔領點，故又譯「波些臣」）登陸，並稱香港島為「」（意即貧瘠的小島）。英軍又舉行升旗儀式和鳴炮，宣示主權。在香港開埠以前，[赤柱人口最多](../Page/赤柱.md "wikilink")，經濟較繁榮，而且，不少外國商人、水手亦是從那裡登陸，故於開埠初期，英國人曾計劃以港島南的[赤柱](../Page/赤柱.md "wikilink")、[香港仔和](../Page/香港仔.md "wikilink")[黃竹坑一帶為發展基地](../Page/黃竹坑.md "wikilink")。惜[赤柱一帶土地狹少](../Page/赤柱.md "wikilink")，且有海盜威脅；更重要的是當時井水不潔，更有疫症蔓延，故此英國人決定轉往港島北岸填海發展。後來，英國人在香港的[中環一帶駐腳](../Page/中環.md "wikilink")，先建成香港第一條街道——[荷-{里}-活道](../Page/荷里活道.md "wikilink")，並把中環一帶發展成行政和商業中心（[維多利亞城](../Page/維多利亞城.md "wikilink")）。19世紀至20世紀初，香港主要是以商業貿易為經濟主軸，幾乎所有公司都在中環一帶進駐，中環成為當時香港島的商業中心區。此外，香港島還有很多高級住宅區，像中環山上的[半山區](../Page/半山區.md "wikilink")、[太平山頂和](../Page/太平山_\(香港\).md "wikilink")[淺水灣都是當時開始發展的](../Page/淺水灣.md "wikilink")。

1842年，清朝與英國再簽訂《[南京條約](../Page/南京條約.md "wikilink")》，香港島正式成為英國在[遠東的](../Page/遠東.md "wikilink")[殖民地之一](../Page/殖民地.md "wikilink")。

### 日軍侵領

1930年初，[英軍意識到](../Page/英軍.md "wikilink")[日軍可能會攻佔香港](../Page/日軍.md "wikilink")，加上[黃泥涌峽在軍事上的重要性](../Page/黃泥涌峽.md "wikilink")，所以興建了大規模的[防禦工事](../Page/防御工事.md "wikilink")，包括皇家[炮兵第](../Page/炮兵.md "wikilink")5AA[高射炮陣地](../Page/高射炮.md "wikilink")、[榴彈炮炮台以及多個](../Page/榴彈炮.md "wikilink")[機槍堡等](../Page/機槍.md "wikilink")。1941年12月8日[香港保衛戰爆發](../Page/香港保衛戰.md "wikilink")，而日軍於12月18日成功搶灘香港島[北角](../Page/北角.md "wikilink")，12月19日已抵達黃泥涌峽。當時英軍第3[義勇軍步兵連第](../Page/皇家香港軍團_\(義勇軍\).md "wikilink")7、8、9排，少量[蘇格蘭營及](../Page/蘇格蘭.md "wikilink")[加拿大溫尼伯](../Page/加拿大.md "wikilink")[榴彈兵部隊D連](../Page/榴彈.md "wikilink")，為扼守這個通往香港島南部的要道以及[黃泥涌水塘](../Page/黃泥涌水塘公園.md "wikilink")，與日軍爆發激戰。雖然英軍的頑強防守使日軍有600多人傷亡，並使義勇軍第3連成為抗戰英雄，但日軍於12月23日終於佔領黃泥涌峽。由於英軍僅餘[赤柱炮台可守](../Page/赤柱炮台.md "wikilink")，加上香港另一主要山峽[灣仔峽於](../Page/灣仔峽.md "wikilink")12月24日亦失守，惟有選擇投降，結束了香港保衛戰，並開始了[香港日佔時期](../Page/香港日佔時期.md "wikilink")。

### 戰後時期

自[第二次世界大戰後香港島人口劇增](../Page/第二次世界大戰.md "wikilink")，由於中環一帶土地不敷應用，香港島不少地方都開始發展，如[北角](../Page/北角.md "wikilink")、[筲箕灣](../Page/筲箕灣.md "wikilink")、[香港仔和](../Page/香港仔.md "wikilink")[黃竹坑](../Page/黃竹坑.md "wikilink")。這些新開發地區為昔日的工廠區，自[製造業式微後](../Page/香港製造業.md "wikilink")，由於香港島的地價租金一直遠比九龍對岸的高，所以慢慢地香港島的商業中心除了在中環外，還有[灣仔和](../Page/灣仔.md "wikilink")[銅鑼灣一帶](../Page/銅鑼灣.md "wikilink")。1997年7月1日，香港岛与[九龙半岛和](../Page/九龙半岛.md "wikilink")[新界的主权一同移交](../Page/新界.md "wikilink")[中华人民共和国](../Page/中华人民共和国.md "wikilink")。

## 行政區劃

香港島的[行政分區有](../Page/香港行政區劃.md "wikilink")：

  - [中西區](../Page/中西區_\(香港\).md "wikilink")：包括[金鐘](../Page/金鐘.md "wikilink")、[中環](../Page/中環.md "wikilink")、[上環](../Page/上環.md "wikilink")、[西環](../Page/西環.md "wikilink")<small>（[西營盤](../Page/西營盤.md "wikilink")、[石塘咀](../Page/石塘咀.md "wikilink")、[堅尼地城](../Page/堅尼地城.md "wikilink")、[摩星嶺](../Page/摩星嶺_\(香港\).md "wikilink")）</small>、[太平山](../Page/太平山_\(香港\).md "wikilink")、[薄扶林](../Page/薄扶林.md "wikilink")、[大小青洲等地方](../Page/大小青洲.md "wikilink")。
  - [灣仔區](../Page/灣仔區.md "wikilink")：包括[灣仔](../Page/灣仔.md "wikilink")<small>（[摩利臣山](../Page/摩利臣山.md "wikilink")、[灣仔北](../Page/灣仔北.md "wikilink")）</small>、[銅鑼灣西](../Page/銅鑼灣.md "wikilink")、[銅鑼灣東](../Page/銅鑼灣.md "wikilink")<small>（[炮台山南](../Page/炮台山.md "wikilink")、[天后](../Page/天后_\(香港\).md "wikilink")）</small>、[跑馬地](../Page/跑馬地.md "wikilink")<small>（[渣甸山](../Page/渣甸山.md "wikilink")、[灣仔峽](../Page/灣仔峽.md "wikilink")）</small>、[大坑等地方](../Page/大坑_\(香港\).md "wikilink")。
  - [東區](../Page/東區_\(香港\).md "wikilink")：包括[北角](../Page/北角.md "wikilink")<small>（[炮台山北](../Page/炮台山.md "wikilink")、[北角半山](../Page/北角半山.md "wikilink")）</small>、[鰂魚涌](../Page/鰂魚涌.md "wikilink")<small>（[太古城](../Page/太古城.md "wikilink")）</small>、[筲箕灣](../Page/筲箕灣.md "wikilink")<small>（[西灣河](../Page/西灣河.md "wikilink")）</small>及[柴灣](../Page/柴灣.md "wikilink")<small>（[杏花邨](../Page/杏花邨.md "wikilink")、[小西灣](../Page/小西灣.md "wikilink")）</small>等地方。
  - [南區](../Page/南區_\(香港\).md "wikilink")：包括[香港仔](../Page/香港仔.md "wikilink")、[鴨脷洲](../Page/鴨脷洲.md "wikilink")、[鋼綫灣](../Page/鋼綫灣.md "wikilink")、[薄扶林](../Page/薄扶林.md "wikilink")、[黃竹坑](../Page/黃竹坑.md "wikilink")、[深水灣](../Page/深水灣.md "wikilink")、[淺水灣](../Page/淺水灣.md "wikilink")、[赤柱](../Page/赤柱.md "wikilink")、[大潭及](../Page/大潭_\(香港\).md "wikilink")[石澳等地](../Page/石澳.md "wikilink")，以及當中的[鴨脷洲和](../Page/鴨脷洲.md "wikilink")[鴨脷排等十多個島嶼](../Page/鴨脷排.md "wikilink")。

[1995年香港立法局選舉亦曾使用](../Page/1995年香港立法局選舉.md "wikilink")「港島中」、「港島東」、「港島南」及「港島西」分區。自[1998年香港立法會選舉起](../Page/1998年香港立法會選舉.md "wikilink")，地方直選改為比例代表制，並由[中西區](../Page/中西區_\(香港\).md "wikilink")、[灣仔區](../Page/灣仔區.md "wikilink")、[東區及](../Page/東區_\(香港\).md "wikilink")[南區組成](../Page/南區_\(香港\).md "wikilink")[「香港島」選區](../Page/香港島選區.md "wikilink")，成為全港五個地方選區之一。

## 地理

[Hong_Kong_Island_-_Land_reclamation.png](https://zh.wikipedia.org/wiki/File:Hong_Kong_Island_-_Land_reclamation.png "fig:Hong_Kong_Island_-_Land_reclamation.png")\]\]
[HK_Island_1840.jpg](https://zh.wikipedia.org/wiki/File:HK_Island_1840.jpg "fig:HK_Island_1840.jpg")
[Hong_Kong_Island_Skyline_2009.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Island_Skyline_2009.jpg "fig:Hong_Kong_Island_Skyline_2009.jpg")拍攝港島區一帶商業區\]\]
[Western_District_View_201308.jpg](https://zh.wikipedia.org/wiki/File:Western_District_View_201308.jpg "fig:Western_District_View_201308.jpg")拍攝西營盤、石塘咀和堅尼地城\]\]
[Hong_Kong_Island_Skyline_201108.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Island_Skyline_201108.jpg "fig:Hong_Kong_Island_Skyline_201108.jpg")

香港島位於[珠江口](../Page/珠江.md "wikilink")、香港南部，[北緯](../Page/北緯.md "wikilink")22.2度，[東經](../Page/東經.md "wikilink")114.2度的位置上。香港島面積約78.64平方公里\[5\]，佔全香港面積大約7%，是香港第二大島嶼，位於大嶼山之後。島上[平地主要集中在北部一列狹長海旁土地上](../Page/平原.md "wikilink")，由[堅尼地城一直伸展至](../Page/堅尼地城.md "wikilink")[小西灣](../Page/小西灣.md "wikilink")；香港島的主要經濟發展也集中於[維多利亞港沿岸地區](../Page/維多利亞港.md "wikilink")。舊日商船自[粤省](../Page/粤.md "wikilink")（省城[廣州](../Page/廣州.md "wikilink")）往来[上海](../Page/上海市.md "wikilink")、[福建等地必經港島海道](../Page/福建省.md "wikilink")，為省城東路咽喉要害\[6\]。香港島的土地從香港開埠以來多年一直依賴[填海工程而增加](../Page/香港填海造地.md "wikilink")。

香港島與九龍半島相隔着維多利亞港，現時共有3條過海行車隧道及3條過海鐵路接駁港九兩岸，中間亦有港內線[渡輪行走](../Page/渡輪.md "wikilink")。本來銅鑼灣對出海域有一[奇力島](../Page/奇力島.md "wikilink")，在1955年興建新的[銅鑼灣避風塘時與香港島連接](../Page/銅鑼灣避風塘.md "wikilink")，當時奇力島由[波斯富街尾一直築海堤得以接連](../Page/波斯富街.md "wikilink")。1969年至1972年，為興建[海底隧道入口](../Page/海底隧道_\(香港\).md "wikilink")，[灣仔及](../Page/灣仔.md "wikilink")[銅鑼灣北進行了另一次填海工程](../Page/銅鑼灣.md "wikilink")，遂發展成今天的地貌。

### 山峰

香港島地形山多平地少，最高點為海拔552米的扯旗山（即[太平山](../Page/太平山_\(香港\).md "wikilink")）。

| 名稱                                        | 高度（米） | 郊野公園                                                                                    | 行政區劃                                    |
| ----------------------------------------- | ----- | --------------------------------------------------------------------------------------- | --------------------------------------- |
| [扯旗山](../Page/太平山_\(香港\).md "wikilink")   | 552   | 郊野公園之外                                                                                  | [中西區](../Page/中西區_\(香港\).md "wikilink") |
| [柏架山](../Page/柏架山.md "wikilink")          | 532   | [大潭和](../Page/大潭郊野公園.md "wikilink")[大潭（鰂魚涌擴建部份）](../Page/大潭郊野公園（鰂魚涌擴建部份）.md "wikilink") | [東區](../Page/東區_\(香港\).md "wikilink")   |
| [奇力山](../Page/奇力山.md "wikilink")          | 501   | 郊野公園之外                                                                                  | 中西區                                     |
| [西高山](../Page/西高山.md "wikilink")          | 494   | [薄扶林](../Page/薄扶林郊野公園.md "wikilink")                                                    |                                         |
| [歌賦山](../Page/歌賦山.md "wikilink")          | 479   | 郊野公園之外                                                                                  |                                         |
| [金馬倫山](../Page/金馬倫山_\(香港\).md "wikilink") | 439   | [香港仔](../Page/香港仔郊野公園.md "wikilink")                                                    | [灣仔區](../Page/灣仔區.md "wikilink")        |
| [畢拿山](../Page/畢拿山.md "wikilink")          | 436   | [大潭和](../Page/大潭郊野公園.md "wikilink")[大潭（鰂魚涌擴建部份）](../Page/大潭郊野公園（鰂魚涌擴建部份）.md "wikilink") | 東區                                      |
| [紫羅蘭山](../Page/紫羅蘭山.md "wikilink")        | 433   | [大潭郊野公園](../Page/大潭郊野公園.md "wikilink")                                                  | [南區](../Page/南區_\(香港\).md "wikilink")   |
| [渣甸山](../Page/渣甸山.md "wikilink")          | 433   | [大潭](../Page/大潭郊野公園.md "wikilink")                                                      | 灣仔區                                     |
| [聶高信山](../Page/聶高信山.md "wikilink")        | 430   | [香港仔](../Page/香港仔郊野公園.md "wikilink")                                                    |                                         |
| [小馬山](../Page/小馬山.md "wikilink")          | 424   | [大潭和](../Page/大潭郊野公園.md "wikilink")[大潭（鰂魚涌擴建部份）](../Page/大潭郊野公園（鰂魚涌擴建部份）.md "wikilink") | 東區                                      |
| [孖崗山](../Page/孖崗山.md "wikilink")          | 386   | [大潭](../Page/大潭郊野公園.md "wikilink")                                                      | 南區                                      |
| [歌連臣山](../Page/歌連臣山.md "wikilink")        | 340   | [石澳](../Page/石澳郊野公園.md "wikilink")                                                      |                                         |
| [南朗山](../Page/南朗山.md "wikilink")          | 284   | 郊野公園之外                                                                                  |                                         |
| [摩星嶺](../Page/摩星嶺.md "wikilink")          | 269   | 中西區                                                                                     |                                         |
| [龍虎山](../Page/龍虎山_\(香港\).md "wikilink")   | 253   | [龍虎山](../Page/龍虎山郊野公園.md "wikilink")                                                    |                                         |
| [壽臣山](../Page/壽臣山.md "wikilink")          | 140   | 郊野公園之外                                                                                  | 南區                                      |

### 水文

香港島水系列表：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>雲爛坑</li>
<li><a href="../Page/黃竹坑.md" title="wikilink">黃竹坑</a></li>
<li>打石石澗</li>
<li>龍爛坑</li>
<li>銀坑</li>
<li>遊龍坑</li>
<li>風門石澗</li>
<li>金鴨石澗</li>
<li><a href="../Page/賽西湖水塘.md" title="wikilink">賽西湖水塘</a></li>
<li><a href="../Page/薄扶林飛瀑.md" title="wikilink">薄扶林飛瀑</a></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li>西孖肚坑</li>
<li>薑花澗</li>
<li><a href="../Page/深水灣.md" title="wikilink">深水灣石澗</a></li>
<li>紫深石澗</li>
<li>紫淺石澗</li>
<li>紫潭石澗</li>
<li>雙潭石澗</li>
<li>渣大石澗</li>
<li>畢潭石澗</li>
<li>小馬坑</li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li>老虎坑</li>
<li>潭崗石澗</li>
<li>柴大石澗</li>
<li><a href="../Page/太古水塘.md" title="wikilink">太古水塘</a></li>
<li>藍塘水塘</li>
<li>西柏石澗</li>
<li>柏畢馬坑</li>
<li>寶馬石澗</li>
<li>柏柴坑</li>
<li>馬東上坑</li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li>馬東下坑</li>
</ul></td>
</tr>
</tbody>
</table>

### 鄰近島嶼

香港島東面為[東龍洲](../Page/東龍洲.md "wikilink")，中間以[藍塘海峽相峙](../Page/藍塘海峽.md "wikilink")。而南面主要為[南丫島](../Page/南丫島.md "wikilink")、[鴨脷洲及](../Page/鴨脷洲.md "wikilink")[蒲台群島等島嶼](../Page/蒲台群島.md "wikilink")，而當中的鴨脷洲與香港仔隔着[香港仔海峽對望](../Page/香港仔海峽.md "wikilink")，中間以[鴨脷洲大橋連接](../Page/鴨脷洲大橋.md "wikilink")。[南丫島發電廠則為整個香港島供應](../Page/南丫島發電廠.md "wikilink")[電力](../Page/電力.md "wikilink")。香港島的西北面有兩個小島，分別為[青洲及](../Page/青洲_\(中西區\).md "wikilink")[小青洲](../Page/小青洲.md "wikilink")，合稱[大小青洲](../Page/大小青洲.md "wikilink")，與香港島中間隔著[硫磺海峽](../Page/硫磺海峽.md "wikilink")。政府原計劃將青洲以填海方式連接香港島，並發展成為全新住宅區，同時建議作為以青洲作發展[10號幹線中連接大嶼山的海底隧道的起點](../Page/10號幹線.md "wikilink")。然而，在環保團體的強烈反對下，有關計劃已擱置。目前兩個島上均無居民。

## 人口

[缩略图](https://zh.wikipedia.org/wiki/File:《中國叢報》香港島人口統計_Hong_Kong_Island_Population_in_"Chinese_Repository",_1841.jpg "fig:缩略图")
根據2011年[香港人口普查](../Page/香港人口普查.md "wikilink")，香港島總人口有1,270,876人，佔全港總人口17.97%。

2000年時，香港島[人口有](../Page/人口.md "wikilink")1,367,900人，約佔全港人口19%。[人口密度每](../Page/人口密度.md "wikilink")[平方公里](../Page/平方公里.md "wikilink")18,000人，高於整體密度（每平方公里7,000人）。如果單以島嶼的比較，香港島是全香港人口最多的島，也是[中華人民共和國第二最多常住人口的島](../Page/中華人民共和國.md "wikilink")，僅次於[海南島](../Page/海南島.md "wikilink")（詳見[中國島嶼](../Page/中華人民共和國島嶼列表.md "wikilink")）。

據1841年5月15日《[香港轅門報](../Page/香港轅門報.md "wikilink")》（Hong Kong
Gazette）所載香港島在開埠開始[原居民人口分佈情況](../Page/原居民.md "wikilink")，島上除去無人居住廢村，共有16條村，當時香港島原居民主要是使用客家語，其中包括「[紅香爐](../Page/紅香爐.md "wikilink")」，有50名、[赤柱](../Page/赤柱.md "wikilink")2000名、[筲箕灣](../Page/筲箕灣.md "wikilink")1200名、[黃泥涌](../Page/跑馬地.md "wikilink")300名、[香港村](../Page/香港村.md "wikilink")200名、[亞公岩](../Page/亞公岩.md "wikilink")200名、[石澳](../Page/石澳.md "wikilink")150名、[土地灣](../Page/土地灣.md "wikilink")60名之後，而在[掃桿甫](../Page/掃桿甫.md "wikilink")10名、[西灣](../Page/西灣.md "wikilink")30名、[石塘咀](../Page/石塘咀.md "wikilink")25名等。\[7\]

## 旅遊

香港島有很多特色旅遊景點，像[太平山頂](../Page/太平山_\(香港\).md "wikilink")、[香港海洋公園](../Page/香港海洋公園.md "wikilink")、[中環](../Page/中環.md "wikilink")、[銅鑼灣](../Page/銅鑼灣.md "wikilink")、[維多利亞公園](../Page/維多利亞公園.md "wikilink")、[香港仔](../Page/香港仔.md "wikilink")、[淺水灣](../Page/淺水灣.md "wikilink")、[赤柱等都是聞名中外旅遊點](../Page/赤柱.md "wikilink")。

## 康樂及保育

[Tai_Tam_2.jpg](https://zh.wikipedia.org/wiki/File:Tai_Tam_2.jpg "fig:Tai_Tam_2.jpg")
[Repulsebaypano.jpg](https://zh.wikipedia.org/wiki/File:Repulsebaypano.jpg "fig:Repulsebaypano.jpg")
香港島地勢較高的中心地帶，除[摩星嶺](../Page/摩星嶺.md "wikilink")、山頂、[黃泥涌峽和部分](../Page/黃泥涌峽.md "wikilink")[渣甸山之外](../Page/渣甸山.md "wikilink")，大都屬於[郊野公園範圍](../Page/香港郊野公園.md "wikilink")，包括[龍虎山郊野公園](../Page/龍虎山郊野公園.md "wikilink")、[薄扶林郊野公園](../Page/薄扶林郊野公園.md "wikilink")、[香港仔郊野公園](../Page/香港仔郊野公園.md "wikilink")、[大潭郊野公園](../Page/大潭郊野公園.md "wikilink")、[大潭郊野公園（鰂魚涌擴建部份）和](../Page/大潭郊野公園（鰂魚涌擴建部份）.md "wikilink")[石澳郊野公園](../Page/石澳郊野公園.md "wikilink")。除了給遊人享受郊遊樂趣之外，郊野公園為香港島「[市肺](../Page/市肺.md "wikilink")」，並確保[集水區不受城市污染](../Page/集水區.md "wikilink")。園內的[薄扶林水塘](../Page/薄扶林水塘.md "wikilink")、[香港仔水塘和](../Page/香港仔水塘.md "wikilink")[大潭水塘和](../Page/大潭水塘.md "wikilink")[黃泥涌水塘為香港早期主要](../Page/黃泥涌水塘公園.md "wikilink")[食水來源](../Page/食水.md "wikilink")。

香港島主要[遠足徑為](../Page/遠足.md "wikilink")[港島徑](../Page/港島徑.md "wikilink")，全長共50[公里](../Page/公里.md "wikilink")，分為8段。起點位於[山頂](../Page/太平山_\(香港\).md "wikilink")[爐峰峽](../Page/爐峰峽.md "wikilink")，而終點則為[大浪灣](../Page/大浪灣_\(香港島\).md "wikilink")。跨越港九新界的[衛奕信徑的第一和第二段亦途經大潭郊野公園和大潭郊野公園](../Page/衛奕信徑.md "wikilink")（鰂魚涌擴建部份）。

除郊野公園之外，香港島[鶴咀亦設有香港的唯一一個海岸保護區](../Page/鶴咀.md "wikilink")──[鶴咀海岸保護區](../Page/鶴咀海岸保護區.md "wikilink")。

郊野公園和海岸保護區均由[漁農自然護理署管理](../Page/漁農自然護理署.md "wikilink")。

香港島有多個[泳灘](../Page/泳灘.md "wikilink")，包括[深水灣](../Page/深水灣.md "wikilink")、[淺水灣](../Page/淺水灣.md "wikilink")、[中灣](../Page/中灣.md "wikilink")、[南灣](../Page/南灣_\(香港島\).md "wikilink")、[舂坎灣](../Page/舂坎灣.md "wikilink")、[聖士提反灣](../Page/聖士提反灣.md "wikilink")、[赤柱正灘](../Page/赤柱正灘.md "wikilink")、[夏萍灣](../Page/夏萍灣.md "wikilink")、[龜背灣](../Page/龜背灣.md "wikilink")、[石澳](../Page/石澳.md "wikilink")、[大浪灣](../Page/大浪灣_\(香港島\).md "wikilink")，為進行[水上活動熱門地點](../Page/水類運動.md "wikilink")。泳灘均由[康樂及文化事務署管理](../Page/康樂及文化事務署.md "wikilink")。

## 交通

### 陸上交通

[ISL_ga_map.png](https://zh.wikipedia.org/wiki/File:ISL_ga_map.png "fig:ISL_ga_map.png")
[Victoriaharbourbridge.jpg](https://zh.wikipedia.org/wiki/File:Victoriaharbourbridge.jpg "fig:Victoriaharbourbridge.jpg")維多利亞港倡議興建跨海大橋時之模擬圖片\]\]
[HK_Cross_Harbour_Tunnel.jpg](https://zh.wikipedia.org/wiki/File:HK_Cross_Harbour_Tunnel.jpg "fig:HK_Cross_Harbour_Tunnel.jpg")（紅磡海底隧道）九龍入口\]\]
[HK_Central_Pier_View.jpg](https://zh.wikipedia.org/wiki/File:HK_Central_Pier_View.jpg "fig:HK_Central_Pier_View.jpg")
由於香港島發展集中於北岸，故交通也以北部較為發達。

[港鐵](../Page/港鐵.md "wikilink")[港島綫與](../Page/港島綫.md "wikilink")[香港電車路線均於香港島沿岸地區來回行駛](../Page/香港電車.md "wikilink")，貫通東、西各區成一橫線。電車東行只到[筲箕灣](../Page/筲箕灣.md "wikilink")，西行至[堅尼地城一帶](../Page/堅尼地城.md "wikilink")，另有路線南至[跑馬地](../Page/跑馬地.md "wikilink")；港鐵港島綫東行至[柴灣站](../Page/柴灣站.md "wikilink")，西行至[堅尼地城站](../Page/堅尼地城站.md "wikilink")。兩種公共交通工具互補不足。然而，港鐵亦已於2016年底開設呈南北走向的[南港島綫](../Page/南港島綫.md "wikilink")，連接[金鐘站與](../Page/金鐘站.md "wikilink")[鴨脷洲的](../Page/鴨脷洲.md "wikilink")[海怡半島站](../Page/海怡半島站.md "wikilink")。[山頂纜車為香港歷史最悠久的公共交通服務](../Page/山頂纜車.md "wikilink")，來往[太平山頂](../Page/太平山_\(香港\).md "wikilink")、[半山區及](../Page/半山區.md "wikilink")[中環](../Page/中環.md "wikilink")，亦是吸引不少遊客乘搭。此外，香港島交通還依賴著[巴士](../Page/香港巴士.md "wikilink")、[小巴等交通服務](../Page/香港小巴.md "wikilink")，尤其是[半山等沒有大型運輸工具的地區](../Page/半山區.md "wikilink")。

### 跨海交通

由於香港島四面環海，對外交通方面非常重要，香港島主要依靠[海底隧道穿過維多利亞港連接](../Page/海底隧道.md "wikilink")[九龍半島](../Page/九龍半島.md "wikilink")，再往各區。現時共有3條行車過海隧道，分別為[香港海底隧道](../Page/香港海底隧道.md "wikilink")、[東區海底隧道和](../Page/東區海底隧道.md "wikilink")[西區海底隧道](../Page/西區海底隧道.md "wikilink")，以及4條港鐵路線[東涌綫](../Page/東涌綫.md "wikilink")、[荃灣綫和](../Page/荃灣綫.md "wikilink")[將軍澳綫和](../Page/將軍澳綫.md "wikilink")[機場快綫](../Page/機場快綫.md "wikilink")。[第四條過海行車隧道及](../Page/香港第四條過海隧道.md "wikilink")[第四條過海鐵路目前正在計劃中](../Page/香港第四條過海鐵路.md "wikilink")，以疏導目前擠塞的交通。

而連接香港島和[鴨脷洲的](../Page/鴨脷洲.md "wikilink")[鴨脷洲大橋於](../Page/鴨脷洲大橋.md "wikilink")1980年落成，使鴨脷洲與港島其他地區之間的關係更緊密。

### 海上交通

海上運輸方面，香港島設有多個[渡輪碼頭](../Page/渡輪.md "wikilink")，主要集中於[中環](../Page/中環.md "wikilink")、[灣仔和](../Page/灣仔.md "wikilink")[北角](../Page/北角.md "wikilink")，設有頻密的渡輪航班來往九龍及[離島](../Page/香港離島.md "wikilink")，在[上環亦同時設有往的離境碼頭](../Page/上環.md "wikilink")。

維多利亞港內亦設有一些公眾碼頭，如知名的[皇后碼頭](../Page/皇后碼頭.md "wikilink")，主要供小型駁船、遊艇、觀光船等作上落客服務。亦有小量的補給和貨物轉運。

#### 往九龍航線

前往九龍的渡輪服務稱為「港內線」，即以維多利亞港內的航線。有一段長時間大部份航線由[油麻地小輪提供服務](../Page/油麻地小輪.md "wikilink")，主要路線有[北角至](../Page/北角.md "wikilink")[觀塘](../Page/觀塘.md "wikilink")、[九龍城及紅磡及](../Page/九龍城.md "wikilink")[中環至](../Page/中環.md "wikilink")[佐敦道碼頭](../Page/佐敦道碼頭.md "wikilink")、[大角咀及](../Page/大角咀.md "wikilink")[深水埗](../Page/深水埗.md "wikilink")。1980年代初期，港島東區至中環的交通十分擠塞時更曾開辦由[太古城至中環的非過海渡輪服務](../Page/太古城.md "wikilink")，至[地鐵](../Page/香港地鐵.md "wikilink")[港島綫通車後取消](../Page/港島綫.md "wikilink")。早期渡海小輪的服務時間至午夜止，在海底隧道及通宵過海巴士出現前，停航後市民只能等待第2天早上過海，又或者乘搭收費較昂貴且危險的「[嘩啦嘩啦](../Page/嘩啦嘩啦.md "wikilink")」電船。

1990年代起，由於陸上交通事業發達，香港政府對渡輪事業沒有大力支持，現時僅剩下5條航線：[北角至](../Page/北角.md "wikilink")[觀塘](../Page/觀塘.md "wikilink")、[九龍城及](../Page/九龍城.md "wikilink")[紅磡](../Page/紅磡.md "wikilink")；[中環至尖沙咀](../Page/中環.md "wikilink")，以及[灣仔尖沙咀航線](../Page/灣仔.md "wikilink")，分別由富裕小輪、新渡輪及天星小輪營運。小型航線則有[海祐小輪經營的](../Page/海祐小輪.md "wikilink")[中環至](../Page/中環.md "wikilink")[尖沙咀東飛翔船服務](../Page/尖沙咀東.md "wikilink")（已停航）及[珊瑚海船務經營的](../Page/珊瑚海船務.md "wikilink")[西灣河至](../Page/西灣河.md "wikilink")[三家村及](../Page/三家村_\(九龍\).md "wikilink")[觀塘航線](../Page/觀塘.md "wikilink")。

#### 往離島航線

往[離島航線稱為](../Page/香港離島.md "wikilink")「港外線」。幾乎均集中在[中環碼頭](../Page/中環碼頭.md "wikilink")，前往的地區包括[馬灣](../Page/馬灣.md "wikilink")、[愉景灣](../Page/愉景灣.md "wikilink")、[梅窩](../Page/梅窩.md "wikilink")、[坪洲](../Page/坪洲.md "wikilink")、[長洲及](../Page/長洲_\(香港\).md "wikilink")[南丫島](../Page/南丫島.md "wikilink")，由多家公司分別經營。有部份市民選擇遷入離島，每天都乘搭渡輪前往中環一帶上班。

此外，一些地區亦設有[街渡](../Page/街渡.md "wikilink")，以小型渡輪接載乘客前往一些客量不高的地方，以作輔助一般渡輪服務，彈性較大。例如香港仔設有街渡航線前往[鴨脷洲](../Page/鴨脷洲.md "wikilink")、[南丫島及](../Page/南丫島.md "wikilink")[長洲](../Page/長洲.md "wikilink")：

  - [香港仔小輪](../Page/香港小輪.md "wikilink")：香港仔↔鴨脷洲
  - [全記渡](../Page/全記渡.md "wikilink")：香港仔↔南丫島[索罟灣](../Page/索罟灣.md "wikilink")（經[模達灣](../Page/模達灣.md "wikilink")）
  - [翠華船務](../Page/翠華船務.md "wikilink")：香港仔↔南丫島[榕樹灣](../Page/榕樹灣.md "wikilink")（經[北角村](../Page/北角村.md "wikilink")）、（Maris
    Ferry）香港仔↔[長洲](../Page/長洲.md "wikilink")
  - [碧海船務](../Page/碧海船務.md "wikilink")：[西灣河](../Page/西灣河.md "wikilink")↔[東龍島](../Page/東龍島.md "wikilink")

#### 往境外航線

位於[上環的](../Page/上環.md "wikilink")[港澳客輪碼頭為前往](../Page/港澳客輪碼頭.md "wikilink")[澳門](../Page/澳門.md "wikilink")／[氹仔以及](../Page/氹仔.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[中山](../Page/中山市.md "wikilink")、[珠海](../Page/珠海.md "wikilink")、[廣州](../Page/廣州.md "wikilink")[番禺](../Page/番禺.md "wikilink")、[深圳](../Page/深圳.md "wikilink")[蛇口等多個中國內地地區的海上交通樞紐](../Page/蛇口.md "wikilink")，設有多條高速客輪航線服務。碼頭天台亦設置[直昇機停機坪](../Page/直昇機.md "wikilink")，提供來往港澳的直昇機航班，來往香港及澳門只需15分鐘。

### 未來發展

[South_Island_Line_and_West_Island_Line_proposal.svg](https://zh.wikipedia.org/wiki/File:South_Island_Line_and_West_Island_Line_proposal.svg "fig:South_Island_Line_and_West_Island_Line_proposal.svg")

在南港島綫通車之前，港島[南區是](../Page/南區_\(香港\).md "wikilink")[香港十八區唯一未有鐵路服務地區](../Page/香港十八區.md "wikilink")，因此南區居民一直大力爭取[香港地鐵能夠伸延至南區](../Page/香港地鐵.md "wikilink")。早於2003年[地鐵公司已開始研究該計劃](../Page/地鐵公司.md "wikilink")，但一直未能落實。為回應南區居民長久以來對改善交通及要求更多公共交通工具選擇的強烈訴求，政府承諾南港島綫於2011年動工，已於2016年12月28日竣工完成。

南港島綫分為東西兩段，總長7公里，已落成的東段服務[海洋公園](../Page/香港海洋公園.md "wikilink")、[黃竹坑及](../Page/黃竹坑.md "wikilink")[鴨脷洲](../Page/鴨脷洲.md "wikilink")，造價超過126億港元。而西段正在規劃中，預期南港島綫西段落成後，能夠舒緩區內交通壓力，並促進區內發展。

## 法定古蹟

## 註釋

## 相關

  - [鴉片戰爭](../Page/鴉片戰爭.md "wikilink")

## 參看

  - [國際都會城市](../Page/國際都會城市.md "wikilink")

## 參見

  - [香港割讓](../Page/香港割讓.md "wikilink")
  - [香港開埠](../Page/香港開埠.md "wikilink")
  - [維多利亞城](../Page/維多利亞城.md "wikilink")
  - [跑馬地馬場](../Page/跑馬地馬場.md "wikilink")
  - [國際金融中心](../Page/國際金融中心_\(香港\).md "wikilink")

## 参考文献

### 引用

### 书籍

  - 《香港島與蒲台島》。香港：地球之友(慈善)有限公司，1998年。ISBN 9628119079
  - 郭少棠：《東區風物志：集體記憶社區情》，香港，東區區議會，2003。

## 外部連結

  - [香港島熱門景點](http://www.discoverhongkong.com/tc/attractions/hk.html)-
    香港旅遊發展局

{{-}}

[香港島](../Category/香港島.md "wikilink")
[Category:香港岛屿](../Category/香港岛屿.md "wikilink")
[Category:香港地方](../Category/香港地方.md "wikilink")

1.  [地政總署測繪處：香港地理資料](http://www.landsd.gov.hk/mapping/en/publications/hk_geographic_data_sheet.pdf)
2.  [峰峦叠错的万山群岛](http://www.lrn.cn/science/seaknowledge/200803/t20080324_211564.htm)
    ，资源网，国土资源部信息中心
3.
4.
5.  [2018年2月地政總署統計](http://www.landsd.gov.hk/mapping/tc/publications/total.htm)
6.  廣東海圖說·香港 張之洞 廣雅書局 1889
7.  [紅香爐與紅香爐天后廟](http://www.lcsd.gov.hk/CE/Museum/History/b5/pspecial_6.php)