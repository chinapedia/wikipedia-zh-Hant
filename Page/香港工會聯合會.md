**香港工會聯合會**（簡稱**工聯會**；，縮寫為），成立於1948年，前稱「**港九工會聯合會**」，1986年改為現名。工聯會是[香港第一大](../Page/香港.md "wikilink")[工會聯合組織](../Page/工會.md "wikilink")，代表全港逾40万雇员；也是香港的[傳統亲共工會](../Page/香港親共人士.md "wikilink")。工聯會在[香港立法會和](../Page/香港立法會.md "wikilink")[香港區議會](../Page/香港區議會.md "wikilink")、[全国人民代表大会](../Page/全国人民代表大会.md "wikilink")、[中国人民政治协商会议佔有席位](../Page/中国人民政治协商会议.md "wikilink")，少數议员同时有[民建联党籍](../Page/民建联.md "wikilink")。

工聯會屬下有超過200間屬會和61間贊助會\[1\]，涵蓋[行業廣泛](../Page/行業.md "wikilink")，主要分為[汽車](../Page/汽車.md "wikilink")[鐵路](../Page/鐵路.md "wikilink")[交通業](../Page/交通.md "wikilink")、海員[海港](../Page/海港.md "wikilink")[運輸業](../Page/運輸.md "wikilink")、[航空業](../Page/航空.md "wikilink")、[政府機構](../Page/政府.md "wikilink")、[公共事業](../Page/公共事業.md "wikilink")、文職及專業、[旅遊](../Page/旅遊.md "wikilink")[飲食](../Page/飲食.md "wikilink")[零售業](../Page/零售.md "wikilink")、[服務業](../Page/服務業.md "wikilink")、[製造業](../Page/工程.md "wikilink")、[造船](../Page/造船.md "wikilink")[機械製造業](../Page/機械.md "wikilink")、[建造業等](../Page/建築.md "wikilink")，會員人數422903人，為全香港會員人數最多及最具規模的工會，也是香港最大的[建制派組織](../Page/建制派.md "wikilink")\[2\]。

## 歷史

历史上工联会曾多次参与劳工维权、灾民救济及社会运动，現時主要工作包括为雇员追讨欠薪、工伤赔偿、遣散费及长期服务金，政纲包括取消强积金对冲、改革政府外判、14周产假、7日侍产假。

### 創辦勞工子弟學校

1946年之前，香港有6間官立小學，每學期學費30元；而私立學校收費100元以上。當時一名五金廠女工日薪僅1.5元，無可能負擔高昂學費。1946年9月，工聯會前身的「港九勞工子弟教育促進會」（後改名為「港九勞工教育促進會」，簡稱「勞教會」）創辦勞工子弟學校（簡稱「勞校」），為基層勞工子女提供教育機會。當時工聯會未正式成立，缺乏獨立校舍，只能借用海軍船塢華員職工會會址上課。直至1948年工聯會正式成立後，勞工子弟學校已有12間。

1949年5月，港英政府擔心左派工人勢力增長，企圖解散勞校，工人、家長、「勞校」師生展開大規模護校運動\[3\]。由於其[左派背景](../Page/左派.md "wikilink")，劳工子弟学校在英國殖民時期受歧視，畢業生難以入職[公務員](../Page/香港公務員.md "wikilink")\[4\]。

### 大火救災

1951年11月21日，東頭村發生大火，有16,000人受災。當時香港底層民眾普遍生活困難，災民缺乏食物。工聯會翌日將5,000包餅乾送至災民手上\[5\]。

1953年12月25日，[石硤尾發生大火](../Page/石硤尾大火.md "wikilink")，58,000名災民無家可歸。工聯會受內地中國人民救濟總會委託，在界限街、楓樹街長沙灣球場，將70萬斤大米和23萬港元款項發放給災民。當時一碗細蓉（雲吞麵）售價5毫\[6\]。

### 工人醫療所及俱樂部

香港當時失業嚴重，很多基層市民三餐不繼，更缺乏醫療及藥物。五十年代，工聯會創辦第一工人[醫療所](../Page/醫療所.md "wikilink")、九龍及香港兩間工人[留產所](../Page/留產所.md "wikilink")、第二工人醫療所、荃灣工人醫療所、九龍及香港兩間[中醫診所](../Page/中醫.md "wikilink")，為基層工人提供廉價醫療服務\[7\]\[8\]。

50年代，香港工人普遍缺乏娛樂場所，生活苦悶。1958年8月，工聯會11屆二次會議議決籌建工人大會堂（「工人俱樂部」前身），9月成立了籌建工人大會堂委員會。1958年至1964年期間，工聯會為工人俱樂部四處籌款，引來眾多工人捐款響應。香港資深藝人周驄當時亦動員演藝界人士參與義演籌款\[9\]。8月28日，工人俱樂部建成開幕，為當時工人提供了一個多樣化的活動場所\[10\]。

### 六七暴動

1967年，香港發生多宗勞工維權事件，遭到港英政府鎮壓。香港左派受[中國大陸](../Page/中國大陸.md "wikilink")[文化大革命和澳門](../Page/文化大革命.md "wikilink")[一二·三事件極端主義的影響而發生暴動](../Page/一二·三事件.md "wikilink")，工聯會旗下的工會是主要發起者。時任工聯會理事長[楊光出任](../Page/楊光_\(工聯會\).md "wikilink")[港九各界同胞反對港英迫害鬥爭委員會的主任委員](../Page/港九各界同胞反對港英迫害鬥爭委員會.md "wikilink")。暴動期間最少造成212名警務人員在內的802人受傷及51人死亡。[香港商業電台節目主持](../Page/香港商業電台.md "wikilink")[林彬在節目中強烈批評](../Page/林彬.md "wikilink")[左派的暴力行徑](../Page/香港親共人士.md "wikilink")，數日後，他與堂弟遭人投擲[汽油彈燒死](../Page/汽油彈.md "wikilink")，被認為是香港六七暴動場面最為慘烈的標誌性事件。

### 廢除集體談判權及市政局

工聯會被批評曾經多次投下出賣工人的議案，包括于1997年在[臨時立法會投票支持廢除香港工人的](../Page/臨時立法會.md "wikilink")[集體談判權](../Page/集體談判權.md "wikilink")\[11\]，理由是原議案部分條款不利工人維權，同時允許工會將資金用於境外社會運動。1998年，工联会投票廢除[市政局及](../Page/市政局.md "wikilink")[區域市政局](../Page/區域市政局.md "wikilink")\[12\]。

### 男士待產假

2012年工联会立法会议员[黄国健及](../Page/黄国健.md "wikilink")[王国兴均曾以](../Page/王国兴.md "wikilink")「要求立法推行7天男士有薪侍產假」作為政綱，但是立法會討論男士3日有薪侍產假的《2014年僱傭(修訂)條例草案》時，[公共專業聯盟](../Page/公共專業聯盟.md "wikilink")[梁繼昌議員雖然曾提出修正案](../Page/梁繼昌.md "wikilink")，要求將假期增加至7日，但工联会全體议员皆沒有投票支持這項修訂，被質疑政綱出爾反爾。\[13\]\[14\]工聯會議員事後解釋，不支持修訂是因為修訂等同推翻勞資談判結果，因為議員提出的修訂政府認為未取得勞顧會共識，政府強調3日侍產假的建議是經過勞顧會中勞資雙方反覆辯證而達成的共識並且平衡了勞方福利和資方承受力，政府稱如果議員提出之任何修訂獲通過，當局將無可奈何地撤回條例。最後工聯會迫於無奈只能投棄權票。工聯會[陳婉嫻事後說不投贊成票被斥責出賣工人](../Page/陳婉嫻.md "wikilink")「絕不好受」，亦介意有關指控。\[15\]\[16\]

2018年，工联会和劳联6位代表勞工階層的建制派議員支持政府提出的5日待產假議案，但是反對7日待產假的建議。當議案表決時，只是按下「出席」按鈕。他們辯解指7日待產假一旦通過，政府會收回法例，故他們要「務實」。而不支持修訂是因為修訂等同推翻勞資談判結果，破壞勞顧會共識，令資方有藉口拒絕談判，而該黨重視勞顧會談判結果，最後迫於無奈只能投棄權票，又稱希望立法會今次通過草案後，政府未來可繼續向7日的目標邁進。而7日待產假提議最終相差5票被否決。\[17\]\[18\]無論如何，工聯會這次表決再次違背當年「7日待產假」的競選承諾。

## 組織架構

### 第37屆常務理事會（2018─20）

由29間工會組成，包括：

  - [汽車交通運輸業總工會](../Page/汽車交通運輸業總工會.md "wikilink")
  - [海港運輸業總工會](../Page/海港運輸業總工會.md "wikilink")
  - [香港海員工會](../Page/香港海員工會.md "wikilink")
  - [香港民用航空事業職工總會](../Page/香港民用航空事業職工總會.md "wikilink")
  - [香港海關人員總會](../Page/香港海關人員總會.md "wikilink")
  - [香港中華煤氣公司華員職工會](../Page/香港中華煤氣公司華員職工會.md "wikilink")
  - [香港洋務工會](../Page/香港洋務工會.md "wikilink")
  - [香港銀行業僱員協會](../Page/香港銀行業僱員協會.md "wikilink")
  - [香港理髮化粧業職工總會](../Page/香港理髮化粧業職工總會.md "wikilink")
  - [香港百貨商業僱員總會](../Page/香港百貨商業僱員總會.md "wikilink")

<!-- end list -->

  - [飲食業職工總會](../Page/飲食業職工總會.md "wikilink")
  - [香港五金電子科技業總工會](../Page/香港五金電子科技業總工會.md "wikilink")
  - [香港印刷業工會](../Page/香港印刷業工會.md "wikilink")
  - [香港電梯業總工會](../Page/香港電梯業總工會.md "wikilink")
  - [香港坭水建築業職工會](../Page/香港坭水建築業職工會.md "wikilink")
  - [香港鐵路員工總會](../Page/香港鐵路員工總會.md "wikilink")
  - [貨櫃運輸業職工總會](../Page/貨櫃運輸業職工總會.md "wikilink")
  - [物流理貨職工會](../Page/物流理貨職工會.md "wikilink")
  - [政府人員協會](../Page/政府人員協會.md "wikilink")
  - [香港醫療人員總工會](../Page/香港醫療人員總工會.md "wikilink")

<!-- end list -->

  - [香港I.T.人協會](../Page/香港I.T.人協會.md "wikilink")
  - [香港文職及專業人員總會](../Page/香港文職及專業人員總會.md "wikilink")
  - [香港保險業總工會](../Page/香港保險業總工會.md "wikilink")
  - [服務業總工會](../Page/服務業總工會.md "wikilink")
  - [香港旅遊業僱員總會](../Page/香港旅遊業僱員總會.md "wikilink")
  - [港九紡織染業職工總會](../Page/港九紡織染業職工總會.md "wikilink")
  - [香港製造業總工會](../Page/香港製造業總工會.md "wikilink")
  - [香港服裝業總工會](../Page/香港服裝業總工會.md "wikilink")
  - [香港建造業總工會](../Page/香港建造業總工會.md "wikilink")

  - 榮譽會長：

<!-- end list -->

  - [林淑儀](../Page/林淑儀.md "wikilink")（香港五金電子科技業總工會）
  - [陳婉嫻](../Page/陳婉嫻.md "wikilink")（香港百貨商業僱員總會）

<!-- end list -->

  - 會長：

<!-- end list -->

  - [吳秋北](../Page/吳秋北.md "wikilink")（香港文職及專業人員總會）

<!-- end list -->

  - 副會長：

<!-- end list -->

  - [黃國健](../Page/黃國健.md "wikilink")（香港海員工會）
  - [梁頌恩](../Page/梁頌恩.md "wikilink")（香港保險業總工會）
  - [唐賡堯](../Page/唐賡堯.md "wikilink")（香港理髮化粧業職總工會）
  - [周聯僑](../Page/周聯僑.md "wikilink")（香港建造業總工會）

<!-- end list -->

  - 理事長：

<!-- end list -->

  - [黃國](../Page/黃國\(工聯會\).md "wikilink")（服務業總工會）

<!-- end list -->

  - 副理事長：

<!-- end list -->

  - [謝愛紅](../Page/謝愛紅.md "wikilink")（貨櫃運輸業職工總會）
  - [麥美娟](../Page/麥美娟.md "wikilink")（社區、社會及個人服務業（新界西）總工會）
  - [陳兆華](../Page/陳兆華.md "wikilink")（汽車交通運輸業總工會）
  - [李永富](../Page/李永富.md "wikilink")（香港民用航空事業職總工會）
  - [李子健](../Page/李子健.md "wikilink")（香港醫療人員總工會）
  - [魯志薪](../Page/魯志薪.md "wikilink")（香港鐵路員工總會）
  - [馬志成](../Page/馬志成.md "wikilink")（政府人員協會）
  - [孔繼明](../Page/孔繼明.md "wikilink")（香港中華煤氣公司華員總工會）
  - [陳鄧源](../Page/陳鄧源.md "wikilink")（香港製造業總工會）
  - [梁芳遠](../Page/梁芳遠.md "wikilink")（香港旅遊業僱員總會）

<!-- end list -->

  - 秘書長：

<!-- end list -->

  - [馬光如](../Page/馬光如.md "wikilink")

<!-- end list -->

  - 副秘書長：

<!-- end list -->

  - [馮鈺鋒](../Page/馮鈺鋒.md "wikilink")、[曾志文](../Page/曾志文.md "wikilink")、[林偉江](../Page/林偉江.md "wikilink")、[楊榮輝](../Page/楊榮輝.md "wikilink")

<!-- end list -->

  - 司庫：

<!-- end list -->

  - 馬光如（兼任）

[File:HK_Hong_Kong_Seamen's_Union.JPG|香港海員工會](File:HK_Hong_Kong_Seamen's_Union.JPG%7C香港海員工會)
[File:HK_Hong_Kong_Printing_Industry_Workers_Union.JPG|香港印刷業工會](File:HK_Hong_Kong_Printing_Industry_Workers_Union.JPG%7C香港印刷業工會)
[File:HK_Hong_Kong_Union_of_Chinese_Workers_in_Western_Style_Employment.JPG|香港洋務工會](File:HK_Hong_Kong_Union_of_Chinese_Workers_in_Western_Style_Employment.JPG%7C香港洋務工會)
[File:HK_Hong_Kong_Shipbuilding_Machinery_Manufacturing_Electrical_and_Steel_Industries_Employees_General_Union.JPG|香港造船機電鋼鐵業總工會](File:HK_Hong_Kong_Shipbuilding_Machinery_Manufacturing_Electrical_and_Steel_Industries_Employees_General_Union.JPG%7C香港造船機電鋼鐵業總工會)
[File:HK_Hong_Kong_Construction_Industry_Employees_General_Union.JPG|香港建造業總工會](File:HK_Hong_Kong_Construction_Industry_Employees_General_Union.JPG%7C香港建造業總工會)

|- |

### 歷任會長

1.  [陳耀材](../Page/陳耀材.md "wikilink")（1957-1980）
2.  [楊光](../Page/楊光_\(鬥委會\).md "wikilink")（1980-1988）
3.  [李澤添](../Page/李澤添.md "wikilink")（1988-2000）
4.  [鄭耀棠](../Page/鄭耀棠.md "wikilink")（2000-2012）
5.  [林淑儀](../Page/林淑儀.md "wikilink")（2012-2018）
6.  [吳秋北](../Page/吳秋北.md "wikilink")（2018-）

### 榮譽會長

1.  [鄭耀棠](../Page/鄭耀棠.md "wikilink")（2012-2018）
2.  [陳婉嫻](../Page/陳婉嫻.md "wikilink")（2012-）
3.  [林淑儀](../Page/林淑儀.md "wikilink")（2018-）

|

### 歷任理事長

1.  [朱敬文](../Page/朱敬文.md "wikilink")（1948-1949）
2.  [張振南](../Page/張振南.md "wikilink")（1949-1951）
3.  [陳文漢](../Page/陳文漢.md "wikilink")（1951-1954）
4.  [陳耀材](../Page/陳耀材.md "wikilink")（1954-1957）
5.  [李生](../Page/李生.md "wikilink")（1957-1962）
6.  [楊光](../Page/楊光_\(鬥委會\).md "wikilink")（1962-1980）
7.  [潘江偉](../Page/潘江偉.md "wikilink")（1980-1986）
8.  [鄭耀棠](../Page/鄭耀棠.md "wikilink")（1986-2000）
9.  [黃國健](../Page/黃國健.md "wikilink")（2000-2009）
10. [吳秋北](../Page/吳秋北.md "wikilink")（2009-2018）
11. [黃國](../Page/黃國.md "wikilink")（2018-）

|}

## [行政會議成員](../Page/香港行政會議.md "wikilink")

  - [黃國健](../Page/黃國健.md "wikilink")

## 議員

### 立法會議員

<div style="overflow:auto; width:100%">

| 席位                                           | 選區／界別                                                                                        | 議員                                                                                           | 2004-2008                                                                                    | 2008-2012                                                                                    | 2012-2016                                                                                    | 2016-2020 | 備註 |
| -------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | --------- | -- |
| [地方選區](../Page/地方選區.md "wikilink")           | [香港島](../Page/香港島選區.md "wikilink")                                                           | [王國興](../Page/王國興_\(香港\).md "wikilink")                                                      |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |           |    |
| [郭偉强](../Page/郭偉强.md "wikilink")             |                                                                                              |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |           |    |
| [九龍東](../Page/九龍東選區.md "wikilink")           | [陳婉嫻](../Page/陳婉嫻.md "wikilink")                                                             | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |                                                                                              |           |    |
| [黃國健](../Page/黃國健.md "wikilink")             |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |           |    |
| [新界西](../Page/新界西選區.md "wikilink")           | [王國興](../Page/王國興_\(香港\).md "wikilink")                                                      |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |           |    |
| [麥美娟](../Page/麥美娟.md "wikilink")             |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |           |    |
| [功能界別](../Page/功能界別.md "wikilink")           | [勞工界](../Page/勞工界功能界別.md "wikilink")                                                         | [鄺志堅](../Page/鄺志堅.md "wikilink")                                                             | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |           |    |
| [王國興](../Page/王國興_\(香港\).md "wikilink")      | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |                                                                                              |                                                                                              |           |    |
| [潘佩璆](../Page/潘佩璆.md "wikilink")             |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |                                                                                              |           |    |
| [葉偉明](../Page/葉偉明.md "wikilink")             |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |                                                                                              |           |    |
| [郭偉强](../Page/郭偉强.md "wikilink")             |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |           |    |
| [鄧家彪](../Page/鄧家彪.md "wikilink")             |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |                                                                                              |           |    |
| [何啟明](../Page/何啟明_\(工聯會\).md "wikilink")     |                                                                                              |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |           |    |
| [陸頌雄](../Page/陸頌雄.md "wikilink")             |                                                                                              |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |           |    |
| [區議會（第二）](../Page/區議會（第二）功能界別.md "wikilink") | [陳婉嫻](../Page/陳婉嫻.md "wikilink")                                                             |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                              |           |    |
| **所得議席**                                     | 3                                                                                            | 4                                                                                            | 6                                                                                            | 5                                                                                            |                                                                                              |           |    |

</div>

### 區議員

2016年至2019年，工聯會在11個區議會共有30個議席，議員包括：

| 區議會                                  | 選區號碼 | 選區                                       | 議員                                        | 備註        |
| ------------------------------------ | ---- | ---------------------------------------- | ----------------------------------------- | --------- |
| [東區](../Page/東區區議會.md "wikilink")    | C07  | 杏花邨                                      | [何毅淦](../Page/何毅淦.md "wikilink")          |           |
| C10                                  | 小西灣  | [王國興](../Page/王國興_\(香港\).md "wikilink")  |                                           |           |
| C11                                  | 景怡   | [梁國鴻](../Page/梁國鴻.md "wikilink")         |                                           |           |
| C18                                  | 和富   | [郭偉强](../Page/郭偉强.md "wikilink")         | 同時兼任立法會議員                                 |           |
| C27                                  | 興東   | [許林慶](../Page/許林慶.md "wikilink")         |                                           |           |
| C30                                  | 上耀東  | [趙資強](../Page/趙資強.md "wikilink")         |                                           |           |
| C35                                  | 佳曉   | [植潔玲](../Page/植潔玲.md "wikilink")         | 2018年區議會補選勝出，民建聯成員                        |           |
| [深水埗區](../Page/深水埗區議會.md "wikilink") | F10  | 麗閣                                       | [陳穎欣](../Page/陳穎欣_\(政治人物\).md "wikilink") |           |
| [黃大仙區](../Page/黃大仙區議會.md "wikilink") | H03  | 龍上                                       | [林文輝](../Page/林文輝.md "wikilink")          |           |
| H23                                  | 彩雲西  | [譚美普](../Page/譚美普.md "wikilink")         | 工聯會及民建聯成員                                 |           |
| H25                                  | 彩虹   | [莫建榮](../Page/莫建榮.md "wikilink")         |                                           |           |
| [觀塘區](../Page/觀塘區議會.md "wikilink")   | J17  | 藍田                                       | [簡銘東](../Page/簡銘東.md "wikilink")          |           |
| J20                                  | 栢雅   | [何啟明](../Page/何啟明_\(工聯會\).md "wikilink") | 同時兼任立法會議員                                 |           |
| [荃灣區](../Page/荃灣區議會.md "wikilink")   | K05  | 福來                                       | [葛兆源](../Page/葛兆源.md "wikilink")          |           |
| [屯門區](../Page/屯門區議會.md "wikilink")   | L09  | 景興                                       | [陳有海](../Page/陳有海.md "wikilink")          |           |
| L10                                  | 興澤   | [徐帆](../Page/徐帆.md "wikilink")           |                                           |           |
| L23                                  | 田景   | [李洪森](../Page/李洪森.md "wikilink")         |                                           |           |
| L28                                  | 富泰   | [陳文偉](../Page/陳文偉.md "wikilink")         |                                           |           |
| [元朗區](../Page/元朗區議會.md "wikilink")   | M20  | 富恩                                       | [劉桂容](../Page/劉桂容.md "wikilink")          |           |
| M22                                  | 天恆   | [陸頌雄](../Page/陸頌雄.md "wikilink")         | 同時兼任立法會議員                                 |           |
| M23                                  | 宏逸   | [姚國威](../Page/姚國威.md "wikilink")         |                                           |           |
| M24                                  | 晴景   | [鄧焯謙](../Page/鄧焯謙.md "wikilink")         |                                           |           |
| [北區](../Page/北區區議會.md "wikilink")    | N07  | 盛福                                       | [溫和達](../Page/溫和達.md "wikilink")          |           |
| N10                                  | 御太   | [曾勁聰](../Page/曾勁聰.md "wikilink")         |                                           |           |
| N14                                  | 天平西  | [黃宏滔](../Page/黃宏滔.md "wikilink")         |                                           |           |
| [西貢區](../Page/西貢區議會.md "wikilink")   | Q24  | 尚德                                       | [簡兆祺](../Page/簡兆祺.md "wikilink")          |           |
| [葵青區](../Page/葵青區議會.md "wikilink")   | S08  | 安蔭                                       | [梁子穎](../Page/梁子穎.md "wikilink")          |           |
| S18                                  | 葵盛西邨 | [劉美璐](../Page/劉美璐.md "wikilink")         |                                           |           |
| S20                                  | 偉盈   | [麥美娟](../Page/麥美娟.md "wikilink")         | 同時兼任立法會議員                                 |           |
| [離島區](../Page/離島區議會.md "wikilink")   | T02  | 逸東邨北                                     | [鄧家彪](../Page/鄧家彪.md "wikilink")          | 工聯會及民建聯成員 |

## 選舉

### 立法會選舉

<table>
<thead>
<tr class="header">
<th><p>選舉</p></th>
<th><p>民選得票</p></th>
<th><p>民選得票比例</p></th>
<th><p>地方選區議席</p></th>
<th><p>功能界別議席</p></th>
<th><p>總議席</p></th>
<th><p>增減</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1998年香港立法會選舉.md" title="wikilink">1998</a></p></td>
<td><p>同時以<a href="../Page/民建聯.md" title="wikilink">民建聯名義參選</a></p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td></td>
<td><p>1 </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年香港立法會選舉.md" title="wikilink">2000</a></p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td></td>
<td><p>1 </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年香港立法會選舉.md" title="wikilink">2004</a></p></td>
<td><p>52,564 </p></td>
<td><p>2.97% </p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td></td>
<td><p>0 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年香港立法會選舉.md" title="wikilink">2008</a></p></td>
<td><p>86,311 </p></td>
<td><p>5.70% </p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2012年香港立法會選舉.md" title="wikilink">2012</a></p></td>
<td><p>127,857 </p></td>
<td><p>7.06% </p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td></td>
<td><p>2 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年香港立法會選舉.md" title="wikilink">2016</a></p></td>
<td><p>169,854 </p></td>
<td><p>7.71% </p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2018年香港立法會補選.md" title="wikilink">2018<br />
<small>（補選）</small></a></p></td>
<td><p>同時以<a href="../Page/民建聯.md" title="wikilink">民建聯名義參選</a></p></td>
<td><p>±0</p></td>
<td></td>
<td></td>
<td><p>0 </p></td>
<td></td>
</tr>
</tbody>
</table>

### 區議會選舉

<table>
<thead>
<tr class="header">
<th><p>選舉</p></th>
<th><p>民選得票</p></th>
<th><p>民選得票比例</p></th>
<th><p>民選議席</p></th>
<th><p>委任議席</p></th>
<th><p>當然議席</p></th>
<th><p>總議席</p></th>
<th><p>增減</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1988年香港區議會選舉.md" title="wikilink">1988</a></p></td>
<td><p>3,360</p></td>
<td><p>0.53%</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td></td>
<td><p>2 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1991年香港區議會選舉.md" title="wikilink">1991</a></p></td>
<td><p>6,229 </p></td>
<td><p>1.17% </p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td></td>
<td><p>2 </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1999年香港區議會選舉.md" title="wikilink">1999</a></p></td>
<td><p>1,074 </p></td>
<td><p>0.13% </p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td></td>
<td><p>3 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2003年香港區議會選舉.md" title="wikilink">2003</a></p></td>
<td><p>3,928 </p></td>
<td><p>0.37% </p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年香港區議會選舉.md" title="wikilink">2007</a></p></td>
<td><p>42,045 </p></td>
<td><p>3.69% </p></td>
<td><p>15</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td></td>
<td><p>16 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2011年香港區議會選舉.md" title="wikilink">2011</a></p></td>
<td><p>64,385 </p></td>
<td><p>5.45% </p></td>
<td><p>24</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td></td>
<td><p>9 </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015年香港區議會選舉.md" title="wikilink">2015</a></p></td>
<td><p>95,583 </p></td>
<td><p>6.61% </p></td>
<td><p>29</p></td>
<td></td>
<td><p>0</p></td>
<td></td>
<td><p>4 </p></td>
</tr>
</tbody>
</table>

  - 已停止舉辦的立法局選舉

<table>
<thead>
<tr class="header">
<th><p>選舉</p></th>
<th><p>民選得票</p></th>
<th><p>民選得票比例</p></th>
<th><p>地方選區議席</p></th>
<th><p>功能界別議席</p></th>
<th><p>總議席</p></th>
<th><p>增減</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1991年香港立法局選舉.md" title="wikilink">1991</a></p></td>
<td><p>44,894 </p></td>
<td><p>3.28% </p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td></td>
<td><p>1 </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1995年香港立法局選舉.md" title="wikilink">1995</a></p></td>
<td><p>同時以<a href="../Page/民建聯.md" title="wikilink">民建聯名義參選</a></p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td></td>
<td><p>0 </p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 相關組織

  - [中國內地諮詢服務中心](../Page/中國內地.md "wikilink")：自2004年底，香港工會聯合會與[廣州](../Page/廣州.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[東莞三地市總工會合辦](../Page/東莞.md "wikilink")，為[香港居民回中國大陸工作就業](../Page/香港人.md "wikilink")、生活、投資、商業貿易、進修學習多方面，提供義務諮詢及協助，例如：迷路、遇險、申請內地及香港福利（廣東計劃）、遭遇法律糾紛、[交通意外緊急援助等](../Page/交通意外.md "wikilink")。\[19\]

## 参考文献

## 外部連結

  - [香港工會聯合會](http://www.ftu.org.hk)
  - [工聯會香港立法會成員](https://web.archive.org/web/20060811012128/http://www.ftulegco.org.hk/index.htm)
  - [:臨時立法會](../Page/:臨時立法會.md "wikilink")
  - [工聯會業餘進修課程資訊](http://8lala.info/%E5%B7%A5%E8%81%AF%E6%9C%83%E6%A5%AD%E9%A4%98%E9%80%B2%E4%BF%AE.html)

## 參見

  - [香港親共人士](../Page/香港親共人士.md "wikilink")
  - [民主建港協進聯盟](../Page/民主建港協進聯盟.md "wikilink")
  - [澳門工會聯合總會](../Page/澳門工會聯合總會.md "wikilink")
  - [黄色工会](../Page/黄色工会.md "wikilink")、[工贼](../Page/工贼.md "wikilink")
  - [六七暴動](../Page/六七暴動.md "wikilink")
  - [港九各界同胞反對港英迫害鬥爭委員會](../Page/港九各界同胞反對港英迫害鬥爭委員會.md "wikilink")

<!-- end list -->

  - 其他香港勞工組織

<!-- end list -->

  - [香港職工會聯盟](../Page/香港職工會聯盟.md "wikilink")
  - [街坊工友服務處](../Page/街坊工友服務處.md "wikilink")
  - [港九勞工社團聯會](../Page/港九勞工社團聯會.md "wikilink")
  - [港九工團聯合總會](../Page/港九工團聯合總會.md "wikilink")

{{-}}

[Category:香港政黨](../Category/香港政黨.md "wikilink")
[Category:中国工会](../Category/中国工会.md "wikilink")
[Category:香港勞工組織](../Category/香港勞工組織.md "wikilink")
[Category:香港建制派組織](../Category/香港建制派組織.md "wikilink")
[香港工會聯合會](../Category/香港工會聯合會.md "wikilink")

1.  [工聯會屬會及贊助會](http://www.ftu.org.hk/zh-hant/club?industry=&occupation=)
2.  [工聯會雙月刊2018年l月至2月](http://www.ftu.org.hk/zh-hant/magazine?id=39#magazine/)
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19. [工聯內地諮詢服務中心](http://www.ftu.org.hk/mainland/center/center_index.htm)