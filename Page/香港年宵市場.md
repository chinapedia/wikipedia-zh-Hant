[Booths_in_Victoria_Park_Lunar_New_Year_Fair_2015.JPG](https://zh.wikipedia.org/wiki/File:Booths_in_Victoria_Park_Lunar_New_Year_Fair_2015.JPG "fig:Booths_in_Victoria_Park_Lunar_New_Year_Fair_2015.JPG")

**香港年宵市場**一般指由[食物環境衞生署所舉辦的](../Page/食物環境衞生署.md "wikilink")[年宵市場](../Page/年宵市場.md "wikilink")，舉行日期為每年[農曆十二月廿四日](../Page/農曆.md "wikilink")（2011年及2014-19年為廿五日），至[正月初一日清晨六時結束](../Page/正月初一.md "wikilink")，全部均為戶外舉行。

## 年宵市場位置

全港總共有15個戶外年宵市場，分別位於：

  - 港島

[Entrance_of_2018_Chinese_New_Year_Fair_in_HK_Victoria_Park.jpg](https://zh.wikipedia.org/wiki/File:Entrance_of_2018_Chinese_New_Year_Fair_in_HK_Victoria_Park.jpg "fig:Entrance_of_2018_Chinese_New_Year_Fair_in_HK_Victoria_Park.jpg")入口\]\]
[2016_Lunar_New_Year_Fair_at_the_Victoria_Park_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:2016_Lunar_New_Year_Fair_at_the_Victoria_Park_\(Hong_Kong\).jpg "fig:2016_Lunar_New_Year_Fair_at_the_Victoria_Park_(Hong_Kong).jpg")\]\]

  - [銅鑼灣](../Page/銅鑼灣.md "wikilink")[維多利亞公園](../Page/維多利亞公園.md "wikilink")

<!-- end list -->

  - 九龍

<!-- end list -->

  - [深水埗](../Page/深水埗.md "wikilink")[花墟公園](../Page/花墟公園.md "wikilink")
  - [長沙灣遊樂場](../Page/長沙灣遊樂場.md "wikilink")
  - [黃大仙](../Page/黃大仙.md "wikilink")[摩士公園](../Page/摩士公園.md "wikilink")
  - [觀塘](../Page/觀塘.md "wikilink")[觀塘遊樂場](../Page/觀塘遊樂場.md "wikilink")（由於觀塘遊樂場進行重建工程，故2010年-2015年觀塘年宵市場暫移至[康寧道遊樂場舉行](../Page/康寧道遊樂場.md "wikilink")，新址為舊有[游泳池位置](../Page/觀塘游泳池.md "wikilink")）

<!-- end list -->

  - 新界

[Lunar_New_year_Fair_stalls_at_Sha_Tsui_Road_Playground.jpg](https://zh.wikipedia.org/wiki/File:Lunar_New_year_Fair_stalls_at_Sha_Tsui_Road_Playground.jpg "fig:Lunar_New_year_Fair_stalls_at_Sha_Tsui_Road_Playground.jpg")舉行的年宵市場\]\]
[Lunar_New_year_Fair_at_Sha_Tsui_Road_Playground_(2013).jpg](https://zh.wikipedia.org/wiki/File:Lunar_New_year_Fair_at_Sha_Tsui_Road_Playground_\(2013\).jpg "fig:Lunar_New_year_Fair_at_Sha_Tsui_Road_Playground_(2013).jpg")舉行的年宵市場\]\]
[Lunar_New_Year_Fair_at_Sha_Tsui_Road_Playground,_Tsuen_Wan,_2015_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Lunar_New_Year_Fair_at_Sha_Tsui_Road_Playground,_Tsuen_Wan,_2015_\(Hong_Kong\).jpg "fig:Lunar_New_Year_Fair_at_Sha_Tsui_Road_Playground,_Tsuen_Wan,_2015_(Hong_Kong).jpg")舉行的年宵市場\]\]

  - [荃灣](../Page/荃灣.md "wikilink")[沙咀道遊樂場](../Page/沙咀道遊樂場.md "wikilink")
  - [葵青](../Page/葵青.md "wikilink")[葵涌運動場](../Page/葵涌運動場.md "wikilink")
  - [西貢](../Page/西貢.md "wikilink")[萬宜遊樂場](../Page/萬宜遊樂場.md "wikilink")
    （1991年首次增加）\[1\]
  - [將軍澳](../Page/將軍澳.md "wikilink")[寶康公園](../Page/寶康公園.md "wikilink")
  - [沙田](../Page/沙田.md "wikilink")[源禾遊樂場](../Page/源禾遊樂場.md "wikilink")
  - [大埔](../Page/大埔_\(香港\).md "wikilink")[天后宮風水廣場](../Page/天后宮風水廣場.md "wikilink")
  - [屯門](../Page/屯門.md "wikilink")[天后廟廣場](../Page/天后廟廣場.md "wikilink")
  - [元朗](../Page/元朗.md "wikilink")[東頭工業區遊樂場](../Page/東頭工業區遊樂場.md "wikilink")
  - [上水](../Page/上水.md "wikilink")[石湖墟遊樂場](../Page/石湖墟遊樂場.md "wikilink")

<!-- end list -->

  - 離島

<!-- end list -->

  - [大嶼山](../Page/大嶼山.md "wikilink")[東涌](../Page/東涌.md "wikilink")[達東路花園](../Page/達東路花園.md "wikilink")

<!-- end list -->

  - 其他曾用作年宵市場之地點

<!-- end list -->

  - [新蒲崗](../Page/新蒲崗.md "wikilink")[東啟德遊樂場](../Page/東啟德遊樂場.md "wikilink")（－2006年）
  - [大埔運動場](../Page/大埔運動場.md "wikilink")（今為[大埔綜合大樓](../Page/大埔綜合大樓.md "wikilink")）（－2003年）
  - [屯門](../Page/屯門.md "wikilink")
    [石排頭遊樂場](../Page/石排頭遊樂場.md "wikilink")（－2013年）
  - [屯門](../Page/屯門.md "wikilink")[新墟遊樂場](../Page/新墟遊樂場.md "wikilink")（－2002年）（與[新發邨一併拆卸](../Page/新發邨.md "wikilink")，今為[西鐵綫](../Page/西鐵綫.md "wikilink")[屯門站及](../Page/屯門站_\(西鐵綫\).md "wikilink")[V
    City](../Page/V_City.md "wikilink")）
  - [大埔頭遊樂場](../Page/大埔頭遊樂場.md "wikilink")（－2005）
  - [元朗](../Page/元朗.md "wikilink")[安興遊樂場](../Page/安興遊樂場.md "wikilink")（－2004年）
  - [觀塘康寧道遊樂場](../Page/觀塘.md "wikilink")（2010年－2015年）（臨時）

[Lunar_New_year_Fair_at_Tung_Chung_Road_Soccer_Pitch_(2014)_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Lunar_New_year_Fair_at_Tung_Chung_Road_Soccer_Pitch_\(2014\)_\(Hong_Kong\).jpg "fig:Lunar_New_year_Fair_at_Tung_Chung_Road_Soccer_Pitch_(2014)_(Hong_Kong).jpg")的東北面盡頭迴旋處。\]\]

  - [大嶼山](../Page/大嶼山.md "wikilink")[東涌道足球場](../Page/東涌道.md "wikilink")（2014年首次增加）\[2\]

## 營運方式和特點

香港的年宵市場內分「濕貨攤位」、「乾貨攤位」和「快餐攤位」。

  - 「濕貨攤位」售賣年花（如[水仙](../Page/水仙.md "wikilink")、[年桔](../Page/年桔.md "wikilink")、[桃花等等](../Page/桃花.md "wikilink")）。

<!-- end list -->

  - 「乾貨攤位」售賣的貨品大多是該年的[生肖有關的物品](../Page/生肖.md "wikilink")，由經營者設計，如[風車](../Page/風車.md "wikilink")、[氣球](../Page/氣球.md "wikilink")、玩具及售賣。有部分攤檔會售賣乾性食品和其他物品（熟食除外）。但亦有部份物品是[直銷物品](../Page/直銷.md "wikilink")，如清潔用品等等。部份攤位的收益會捐贈給[慈善機構](../Page/慈善機構.md "wikilink")。

<!-- end list -->

  - 「快餐攤位」現場售賣[快餐](../Page/快餐.md "wikilink")、[小食](../Page/小吃.md "wikilink")，供行年宵的市民遊客充飢，如：[魚蛋](../Page/魚蛋.md "wikilink")、[燒賣](../Page/燒賣.md "wikilink")、[格仔餅等](../Page/格仔餅.md "wikilink")。快餐攤位可售賣預先煮熟的快餐食品、樽裝及非樽裝飲品、雪糕、麵包西餅和即場加工處理的即食小食，但亦只限在這些攤位範圍售賣。經營快餐攤位者須領有食物環境衛生署簽發的臨時食物製造廠牌照及遵守有關衞生規定。\[3\]

## 競投方式

每年11月，食環署都會把翌年的年宵市場檔位以價高者得的方式公開競投。

### 經營者

經營者包括[中學和](../Page/中學.md "wikilink")[大學校內組織](../Page/大學.md "wikilink")、慈善團體、社會團體、直銷公司、賣花公司等。

## 年宵市場之最

  - 面積最大：銅鑼灣維多利亞公園
  - 人數最多：銅鑼灣維多利亞公園
      - 乾貨攤位：300個
      - 濕貨攤位：180個
          - （位於足球場，分為八列，每列60個攤位，三列為濕貨攤位，五列為乾貨攤位）
      - 熟食攤檔：3個（1個大的鄰近[銅鑼灣及](../Page/銅鑼灣.md "wikilink")2個較小的鄰近[天后](../Page/天后_\(香港\).md "wikilink")）
  - 2007及2015年面積第二大：荃灣[沙咀道遊樂場](../Page/沙咀道.md "wikilink")
  - 人數第二多：深水埗[花墟公園](../Page/花墟公園.md "wikilink")
  - 人流最少：大嶼山[東涌道足球場](../Page/東涌道.md "wikilink")\[4\]

## 文化

由於香港的年宵市場已舉辦多年，檔主們已有一個默契，就是只會投回自己的檔位，以方便顧客。故此，如有外來競爭者加入競投，檔主們都會以不友善的目光對待外來競爭者，甚至以[粗言穢語及](../Page/粗口.md "wikilink")[暴力對待](../Page/暴力.md "wikilink")，令其知難而退。不過，如此事件損害政府的庫房收入。近年警方都會派出[有組織罪案及三合會調查科探員在拍賣場監察競投過程](../Page/有組織罪案及三合會調查科.md "wikilink")。

另外，近年部分[中學和](../Page/中學.md "wikilink")[大學亦開始組織學生競投年宵市場檔位](../Page/大學.md "wikilink")，讓學生可以實習真實市場的交易情況，也令學生能吸收[市場學](../Page/市場學.md "wikilink")[營商經驗](../Page/市場營銷_#市場行銷策略.md "wikilink")。然而由於有校方[資助](../Page/補貼.md "wikilink")，部分學生會以低價[傾銷貨品](../Page/傾銷.md "wikilink")，曾引起鄰近檔主不滿。

## 爭議

以往部份濕貨攤位經營者，不甘滯銷的年花給遊人免費執拾，便把年花當地銷毀，造成垃圾，食環署後來在競投攤位時候定下守則，如果有檔主把年花銷毀，署方會把該經營者列入[黑名單](../Page/黑名單.md "wikilink")，來年禁止該檔主競投攤位。自此後，滯銷的年花會被運送給[安老中心](../Page/安老院.md "wikilink")。

## 其他年宵市場

上述的14個是室外的年宵市場，較受天氣影響。於1999年時在[九龍灣的](../Page/九龍灣.md "wikilink")[國際展貿中心曾舉辦過室內年宵市場](../Page/國際展貿中心.md "wikilink")，雖然乾貨的款式不及室外的那麼繁多，加上場地所限，不能像室外的市宵市場般可售賣各式年花，但受到市民歡迎。故主辦單位於2000年在同一地點再次舉辦室內年宵市場，但無論入場人數和受歡迎程度不及1999年。自此主辦單位亦再沒有再舉辦室內年宵市場。兩年主辦單位都有安排免費[穿梭巴士](../Page/穿梭巴士.md "wikilink")，於[九龍灣](../Page/九龍灣.md "wikilink")[地鐵站](../Page/地鐵.md "wikilink")（現為[港鐵站](../Page/港鐵.md "wikilink")）和年宵市場外接載遊人往返。

2006年，[觀塘的](../Page/觀塘.md "wikilink")[apm市場內設室內年宵市場](../Page/apm.md "wikilink")，規模不及1999年和2000年的室內年宵市場。在2007年，再次有主辦單位在[灣仔](../Page/灣仔.md "wikilink")[香港會議展覽中心舉辦室內年宵市場](../Page/香港會議展覽中心.md "wikilink")。

## 參见

  - [深圳迎春花市](../Page/深圳迎春花市.md "wikilink")

## 參考資料

[Category:香港墟市](../Category/香港墟市.md "wikilink")
[Category:香港飲食](../Category/香港飲食.md "wikilink")
[Category:香港新春習俗](../Category/香港新春習俗.md "wikilink")
[Category:香港節慶活動](../Category/香港節慶活動.md "wikilink")
[Category:香港政府](../Category/香港政府.md "wikilink")

1.  《一九九一年年宵市場》(環境衛生事務委員會備忘錄文件 EH/30/90 (6.12.90) 及 康樂文化事務委員會備忘錄文件
    R\&C/56/90 (11.12.90))，區域市政局，1990年10月29日，檔案編號：RSD 11/HQ 301/73/X)
2.
3.
4.