[Mandarin_Oriental_Hong_Kong_Lobby_Stairs_2008.jpg](https://zh.wikipedia.org/wiki/File:Mandarin_Oriental_Hong_Kong_Lobby_Stairs_2008.jpg "fig:Mandarin_Oriental_Hong_Kong_Lobby_Stairs_2008.jpg")
**香港文華東方酒店**（[英文](../Page/英文.md "wikilink")：**Mandarin Oriental Hong
Kong**）位於[香港](../Page/香港.md "wikilink")[中環](../Page/中環.md "wikilink")[干諾道中](../Page/干諾道中.md "wikilink")5號，是[文華東方酒店集團旗下的](../Page/文華東方酒店集團.md "wikilink")[旗艦酒店](../Page/旗艦.md "wikilink")。

## 歷史

香港文華東方酒店是五星級酒店，位於[中環](../Page/中環.md "wikilink")[皇后像廣場西北側](../Page/皇后像廣場.md "wikilink")，原址為[皇后行](../Page/皇后行.md "wikilink")，可眺望[維多利亞港景色](../Page/維多利亞港.md "wikilink")，酒店由香港中環著名地產商[置地公司興建](../Page/置地公司.md "wikilink")，1963年9月開業，當時名為文華酒店。酒店開業至今，由大堂的裝修、油畫至扒房的炭火燒烤爐等，一直都維持[英國式傳統](../Page/英國.md "wikilink")。

香港文華東方酒店是亞洲其中一家最佳酒店，不少名人政要曾在此居住，包括已故[戴安娜王妃](../Page/戴安娜王妃.md "wikilink")、前英國首相[-{zh-hans:撒切尔夫人;zh-hk:戴卓爾夫人;zh-tw:柴契爾夫人;}-](../Page/戴卓爾夫人.md "wikilink")、美國前總統[-{zh-hans:尼克森;zh-hk:尼克遜;zh-tw:尼克森;}-](../Page/尼克松.md "wikilink")、[福特與](../Page/福特.md "wikilink")[-{zh-hans:老布什;zh-hk:老布殊;zh-tw:老布希;}-](../Page/老布殊.md "wikilink")、[-{zh-hans:
汤姆·克鲁斯; zh-hant:湯告魯斯}-](../Page/湯告魯斯.md "wikilink")、[-{zh-hans: 凯文·科斯特纳;
zh-hant:奇雲高士拿;}-](../Page/奇雲高士拿.md "wikilink")、[馬友友](../Page/馬友友.md "wikilink")、[濱崎步](../Page/濱崎步.md "wikilink")、[汶萊](../Page/汶萊.md "wikilink")[蘇丹等](../Page/苏丹_\(称谓\).md "wikilink")。

酒店在2005年耗資十億零八千萬港元進行大規模翻新工程，由2005年12月28日至2006年8月暫停營業以進行翻新。並已於2006年9月28日重新開業。

## 餐廳

  - Pierre:榮獲米芝蓮兩星級的法國餐廳
  - 文華扒房:港式西式扒房餐廳
  - 文華廳:傳統粵菜餐館
  - 庫克廳:私人宴會廳
  - Cafe Causette:提供國際美食的餐廳
  - 快船廊:提供早餐、早午餐、晚餐和特色下午茶的國際自助餐餐廳
  - 千日里吧:英國餐廳
  - M bar
  - 船長吧
  - 文華餅店:文華東方酒店的餅店

## 相册

<File:Mandarin> Oriental Hong Kong Christie Digital
20171125.JPG|2017年「光·影·香港夜」，大樓外牆設光雕投影「LUX, An Experience」
<File:HK> Mandarine Oriental Hotel 610
wkw.jpg|往酒店[商場的](../Page/商場.md "wikilink")[行人天橋](../Page/行人天橋.md "wikilink")
<File:Mandarin> Oriental Hong Kong Lobby.jpg|酒店大堂 <File:Mandarin>
Oriental Hong Kong Bakery Shop.jpg|文華餅店 <File:HK> Central 文華東方酒店
Mandarin Oriental Hotel - Bathroom bathtube mirror Feb-2012.jpg|酒店房間一角
<File:Clipper> lounge buffet restaurant.jpg|快船廊

## 事件

1970年7月18日，一名[贊育醫院見習醫生李玉儀](../Page/贊育醫院.md "wikilink")（24歲）在酒店21樓一房間內服食過量藥物自殺，送到[瑪麗醫院證實不治](../Page/瑪麗醫院.md "wikilink")，有兩封英文遺書，一封給雙親，一封給弟弟，未提自殺原因。

2003年4月1日，香港著名歌手[張國榮在酒店](../Page/張國榮.md "wikilink")24樓咖啡室一躍而下喪生。有传24楼被封是作为缅怀「哥哥」张国荣，其一原因是张国荣曾经称赞24楼的天花板很漂亮，张国荣他自己也很喜欢。其二是因为张国荣在此楼丧生。每年的4月1日，大批市民在酒店近[雪廠街一側放滿鮮花紀念這位一代巨星](../Page/雪廠街.md "wikilink")。

## 相關

  - [公爵大廈](../Page/公爵大廈.md "wikilink")[置地文華東方酒店](../Page/置地文華東方酒店.md "wikilink")

## 參考資料

## 外部链接

  - [香港文華東方酒店(官方網站)](http://www.mandarinoriental.com/hongkong/)

[Category:文华东方酒店集团](../Category/文华东方酒店集团.md "wikilink")
[Category:中環酒店](../Category/中環酒店.md "wikilink")
[Category:香港置地物業](../Category/香港置地物業.md "wikilink")
[Category:香港摩天大樓](../Category/香港摩天大樓.md "wikilink")
[Category:1963年香港建立](../Category/1963年香港建立.md "wikilink")