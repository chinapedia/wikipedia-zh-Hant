[香港約有](../Page/香港.md "wikilink")80種爬行動物：

## [龜鱉目](../Page/龜鱉目.md "wikilink") Testudines

### [淡水龜科](../Page/淡水龜科.md "wikilink") Bataguridae

1.  [珍珠龜](../Page/珍珠龜.md "wikilink") (烏龜、草龜) - *Chinemys reevesi*
2.  [花龜](../Page/花龜.md "wikilink") (斑龜、長尾龜) - *Ocadia sinensis*
3.  [黃喉擬水龜](../Page/黃喉擬水龜.md "wikilink") (石金錢龜、柴棺龜) - *Mauremys mutica*
4.  [大頭烏龜](../Page/大頭烏龜.md "wikilink") - *Chinemys megalocephala*
    ※2009年新記錄
5.  [馬來閉殼龜](../Page/馬來閉殼龜.md "wikilink") (馬來箱龜) - *Cuora amboinensis*
    ※引入種
6.  [黃緣閉殼龜](../Page/黃緣閉殼龜.md "wikilink") (黃緣盒龜、食蛇龜) - *Cuora
    flavomarginata* ※引入種
7.  [三線閉殼龜](../Page/三線閉殼龜.md "wikilink") (三線盒龜、金錢龜) - *Cuora
    trifasciata*
8.  [眼斑水龜](../Page/眼斑水龜.md "wikilink") (眼斑龜) - *Sacalia bealei*
9.  [紅耳龜](../Page/紅耳龜.md "wikilink") (巴西龜) - *Trachemys scripta elegans*
    ※引入種

### [平胸龜科](../Page/平胸龜科.md "wikilink") Platysternidae

1.  [平胸龜](../Page/平胸龜.md "wikilink")
    (鷹咀龜、[大頭龜](../Page/大頭龜.md "wikilink")) -
    *Platysternon megacephalum*

### [鱉科](../Page/鱉科.md "wikilink") Trionchidae

1.  [中華鱉](../Page/中華鱉.md "wikilink") (甲魚、水魚) - *Pelodiscus sinensis*

### [海龜科](../Page/海龜科.md "wikilink") Cheloniidae

1.  [綠海龜](../Page/綠海龜.md "wikilink") - *Chelonia mydas*
    ※受動植物（瀕危物種保護）條例（第586章）保護
2.  [麗海龜](../Page/麗海龜.md "wikilink") (欖龜) - *Lepidochelys olivacea*

### [稜皮龜科](../Page/稜皮龜科.md "wikilink") Dermochelyidae

1.  [稜皮龜](../Page/稜皮龜.md "wikilink") - *Dermochelys coriacea*
    ※受動植物（瀕危物種保護）條例（第586章）保護

## [有鱗目](../Page/有鱗目.md "wikilink") Squamata

### 蜥蜴亞目 Lacertilia

  - [鬣蜥科](../Page/鬣蜥科.md "wikilink") Agamidae

<!-- end list -->

1.  [變色樹蜥](../Page/變色樹蜥.md "wikilink") (雞冠蛇) - *Calotes versicolor*

<!-- end list -->

  - [巨蜥科](../Page/巨蜥科.md "wikilink") Varanidae

<!-- end list -->

1.  [水巨蜥](../Page/水巨蜥.md "wikilink") (五爪金龍) - *Varanus salvator*
    ※受動植物（瀕危物種保護）條例（第586章）保護

<!-- end list -->

  - [石龍子科](../Page/石龍子科.md "wikilink") Scincidae

<!-- end list -->

1.  [中國光蜥](../Page/中國光蜥.md "wikilink") - *Ateuchosaurus chinensis*
2.  [中國石龍子](../Page/中國石龍子.md "wikilink") (山龍子) - *Eumeces chinensis
    chinensis*
3.  [藍尾石龍子](../Page/藍尾石龍子.md "wikilink") (麗紋石龍子) - *Eumeces elegans*
4.  [四線石龍子](../Page/四線石龍子.md "wikilink") - *Eumeces quadrilineatus*
5.  [長尾南蜥](../Page/長尾南蜥.md "wikilink") - *Mabuya longicaudata*
6.  [寧波滑蜥](../Page/寧波滑蜥.md "wikilink") - *Scincella modesta*
7.  [南滑蜥](../Page/南滑蜥.md "wikilink") - *Scincella reevesii*
8.  [股銅蜓蜥](../Page/股銅蜓蜥.md "wikilink") (股鱗蜓蜥) - *Sphenomorphus
    incognitus*
9.  [蝘蜓蜥](../Page/蝘蜓蜥.md "wikilink") (銅蜓蜥) - *Sphenomorphus indicus*
10. [中國稜蜥](../Page/中國稜蜥.md "wikilink") - *Tropidophorus sinicus*

<!-- end list -->

  - [蜥蜴科](../Page/蜥蜴科.md "wikilink") Lacertidae

<!-- end list -->

1.  [南草蜥](../Page/南草蜥.md "wikilink") (草龍) - *Takydromus sexlineatus
    ocellatus*

<!-- end list -->

  - [雙足蜥科](../Page/雙足蜥科.md "wikilink") Dibamidae

<!-- end list -->

1.  [鮑氏雙足蜥](../Page/鮑氏雙足蜥.md "wikilink") - *Dibamus
    bogadeki*※香港[特有種](../Page/特有種.md "wikilink")

<!-- end list -->

  - [壁虎科](../Page/壁虎科.md "wikilink") Gekkonidae

<!-- end list -->

1.  [截趾壁虎](../Page/截趾壁虎.md "wikilink") - *Gehyra mutilata*
2.  [中國壁虎](../Page/中國壁虎.md "wikilink") - *Gekko chinensis*
3.  [大壁虎](../Page/大壁虎.md "wikilink") (蛤蚧) - *Gekko gecko*
4.  [原尾蜥虎](../Page/原尾蜥虎.md "wikilink") (無疣蜥虎) - *Hemidactylus bowringii*
5.  [密疣蜥虎](../Page/密疣蜥虎.md "wikilink") - *Hemidactylus brookii* ※引入種
6.  [鋸尾蜥虎](../Page/鋸尾蜥虎.md "wikilink") - *Hemidactylus garnotii*

### 蛇亞目 Serpentes

  - [蟒科](../Page/蟒科.md "wikilink") Pythonidae

<!-- end list -->

1.  [緬甸蟒](../Page/緬甸蟒.md "wikilink") (蟒蛇) - *Python molurus bivittatus*
    ※受動植物（瀕危物種保護）條例（第586章）保護

<!-- end list -->

  - [眼鏡蛇科](../Page/眼鏡蛇科.md "wikilink") Elapidae

<!-- end list -->

1.  [金環蛇](../Page/金環蛇.md "wikilink") (金腳帶) - *Bungarus fasciatus*
2.  [銀環蛇](../Page/銀環蛇.md "wikilink") (銀腳帶) - *Bungarus multicinctus
    multicinctus*
3.  [麗紋蛇](../Page/麗紋蛇.md "wikilink") (珊瑚蛇) - *Calliophis macclellandi*
4.  [中國眼鏡蛇](../Page/中國眼鏡蛇.md "wikilink") (飯鏟頭) - *Naja atra*
    ※受動植物（瀕危物種保護）條例（第586章）保護
5.  [眼鏡王蛇](../Page/眼鏡王蛇.md "wikilink") (過山烏) - *Ophiophagus hannah*
    ※受動植物（瀕危物種保護）條例（第586章）保護
6.  [越南烙鐵頭](../Page/越南烙鐵頭.md "wikilink") - ''Ovophis tonkinensis ''

<!-- end list -->

  - [海蛇科](../Page/海蛇科.md "wikilink") Hydrophiidae

<!-- end list -->

1.  [青環海蛇](../Page/青環海蛇.md "wikilink") - *Hydrophis cyanocinctus*
2.  [小頭海蛇](../Page/小頭海蛇.md "wikilink") - *Hydrophis gracilis*
3.  [淡灰海蛇](../Page/淡灰海蛇.md "wikilink") - *Hydrophis ornatus*
4.  [平頦海蛇](../Page/平頦海蛇.md "wikilink") - *Lapemis curtus*
5.  [長吻海蛇](../Page/長吻海蛇.md "wikilink") - *Pelamis platurus*
6.  [海奎](../Page/海奎.md "wikilink") - *Praescutata viperina*

<!-- end list -->

  - [奎蛇科](../Page/奎蛇科.md "wikilink") Daboia

<!-- end list -->

1.  [白唇竹葉青](../Page/白唇竹葉青.md "wikilink") (青竹蛇) - *Trimeresurus
    albolabris*
2.  [烙鐵頭](../Page/烙鐵頭.md "wikilink") - *Protobothrops mucrosquamatus*

<!-- end list -->

  - [游蛇科](../Page/游蛇科.md "wikilink") Colubridae

<!-- end list -->

1.  [紅脖頸槽蛇](../Page/紅脖頸槽蛇.md "wikilink") Rhabdophis subminiatus
2.  [棕脊蛇](../Page/棕脊蛇.md "wikilink") - *Achalinus rufescens*
3.  [綠瘦蛇亞種](../Page/綠瘦蛇.md "wikilink") - *Ahaetulla prasina medioxima*
    (Lazzell, 2002)
4.  [無顳鱗游蛇](../Page/無顳鱗游蛇.md "wikilink") (無顳鱗腹鏈蛇) - *Amphiesma
    atemporale*
5.  [白眉游蛇](../Page/白眉游蛇.md "wikilink") (白眉腹鏈蛇) - *Amphiesma boulengeri*
6.  [草游蛇](../Page/草游蛇.md "wikilink") (草腹鏈蛇) - *Amphiesma stolatum*
7.  [繁花林蛇](../Page/繁花林蛇.md "wikilink") (繁花蛇) - *Boiga multimaculata*
8.  [鈍尾兩頭蛇](../Page/鈍尾兩頭蛇.md "wikilink") - *Calmaria septentrionalis*
9.  [翠青蛇](../Page/翠青蛇.md "wikilink") - *Cyclophiops major*
10. [香港過樹蛇](../Page/香港過樹蛇.md "wikilink")*Dendrelaphis hollonrakei*
    Lazell, 2002
11. [紫灰錦蛇](../Page/紫灰錦蛇.md "wikilink") (紅竹蛇) - *Elaphe porphyracea
    nigrofasciata*
12. [三索錦蛇](../Page/三索錦蛇.md "wikilink") (三索線) - *Elaphe radiata*
13. [黑斑水蛇](../Page/黑斑水蛇.md "wikilink") - *Enhydris bennettii*
14. [中國水蛇](../Page/中國水蛇.md "wikilink") - *Enhydris chinensis*
15. [鉛色水蛇](../Page/鉛色水蛇.md "wikilink") - *Enhydris plumbea*
16. [白環蛇](../Page/白環蛇.md "wikilink") - *Lycodon aulicus*
17. [黑背白環蛇](../Page/黑背白環蛇.md "wikilink") - *Lycodon ruhstrati ruhstrati*
18. [細白環蛇](../Page/細白環蛇.md "wikilink") - *Lycodon subcinctus*
19. [紫棕小頭蛇](../Page/紫棕小頭蛇.md "wikilink") (棕枰杆蛇)- *Oligodon cinereus
    cinereus*
20. [台灣小頭蛇](../Page/台灣小頭蛇.md "wikilink") (台灣枰杆蛇) - *Oligodon formosanus*
21. [香港后稜蛇](../Page/香港后稜蛇.md "wikilink") - *Opisthotropis andersonii*
22. [橫紋后稜蛇](../Page/橫紋后稜蛇.md "wikilink") - *Opisthotropis balteata*
23. [挂墩后稜蛇](../Page/挂墩后稜蛇.md "wikilink") - *Opisthotropis kuatunensis*
24. [側條后稜蛇](../Page/側條后稜蛇.md "wikilink") - *Opisthotropis lateralis*
25. [鈍頭蛇](../Page/鈍頭蛇.md "wikilink") - *Pareas chinensis*
26. [橫紋鈍頭蛇](../Page/橫紋鈍頭蛇.md "wikilink") - *Pareas margaritophorus*
27. [紫沙蛇](../Page/紫沙蛇.md "wikilink") (茶斑大頭蛇) - *Psammodynastes
    pulverulentus*
28. [灰鼠蛇](../Page/灰鼠蛇.md "wikilink") (過樹榕) - *Ptyas korros*
29. [滑鼠蛇](../Page/滑鼠蛇.md "wikilink") (水律) - *Ptyas mucosus*
30. [頸槽游蛇](../Page/頸槽游蛇.md "wikilink") - *Rhabdophis nuchalis nuchalis*
31. [紅脖游蛇](../Page/紅脖游蛇.md "wikilink") - *Rhabdophis subminiatus
    helleri*
32. [黑頭劍蛇](../Page/黑頭劍蛇.md "wikilink") (黑頭蛇) - *Sibynophis chinensis
    chinensis*
33. [環紋游蛇](../Page/環紋游蛇.md "wikilink") (環紋華游蛇) - *Sinonatrix
    aequifasciata*
34. [烏游蛇](../Page/烏游蛇.md "wikilink") (華游蛇) - *Sinonatrix percarinata
    percarinata*
35. [漁游蛇](../Page/漁游蛇.md "wikilink") (草花蛇) - *Xenochrophis piscator*

<!-- end list -->

  - [盲蛇科](../Page/盲蛇科.md "wikilink") Typhlopidae

<!-- end list -->

1.  [白頭盲蛇](../Page/白頭盲蛇.md "wikilink") - *Ramphotyphlops albiceps*
2.  [鉤盲蛇](../Page/鉤盲蛇.md "wikilink") (鐵線蛇) - *Ramphotyphlops braminus*
3.  [拉氏盲蛇](../Page/拉氏盲蛇.md "wikilink")(香港盲蛇) - *Typhlops lazelli*
    ※香港[特有種](../Page/特有種.md "wikilink")

## 參考

  - Karsen, S. J., Lau, M.W.N, & Bogadek, A. (1998). *Hong Kong
    Amphibians and Reptiles* (2nd Edition). Provisional Urban Council
    Hong Kong.
  - [AFCD-香港爬行動物名錄](http://www.afcd.gov.hk/tc_chi/conservation/hkbiodiversity/speciesgroup/files/RepChi20071227.pdf)

## 外部链接

  - [香港兩棲及爬蟲協會 - 香港首個及現存唯一關注兩棲及爬行動物的認可慈善機構。](http://www.hkherp.org/)

[Category:香港動物](../Category/香港動物.md "wikilink")