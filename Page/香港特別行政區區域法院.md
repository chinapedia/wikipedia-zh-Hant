[HK_WanChaiTower.JPG](https://zh.wikipedia.org/wiki/File:HK_WanChaiTower.JPG "fig:HK_WanChaiTower.JPG")\]\]
[HK-location-of-courts.png](https://zh.wikipedia.org/wiki/File:HK-location-of-courts.png "fig:HK-location-of-courts.png")

**香港特別行政區區域法院**（）是[香港次於](../Page/香港.md "wikilink")[高等法院的原訟法院](../Page/香港高等法院.md "wikilink")，[香港主權移交前稱為](../Page/香港主權移交.md "wikilink")**地方法院**，具有限的[刑事及](../Page/刑事.md "wikilink")[民事](../Page/民事.md "wikilink")[司法管轄權](../Page/司法管轄權.md "wikilink")，負責處理涉及款項300萬港元以下的民事訴訟。而刑事案件中，區域法院法官的判刑上限是7年監禁。區域法院處理的刑事罪行都是由[裁判法院移交的](../Page/香港裁判法院.md "wikilink")，較嚴重的刑事罪行（如[謀殺](../Page/謀殺.md "wikilink")、[誤殺和](../Page/誤殺.md "wikilink")[強姦](../Page/強姦.md "wikilink")）不會由區域法院審理，而是直接移交[高等法院](../Page/香港高等法院.md "wikilink")。而區域法院的上訴案件會轉至高等法院的上訴法庭。

香港區域法院專門處理[婚姻](../Page/婚姻.md "wikilink")[訴訟](../Page/訴訟.md "wikilink")（[離婚](../Page/離婚.md "wikilink")、[贍養費](../Page/贍養費.md "wikilink")、[子女撫養權](../Page/子女撫養權.md "wikilink")）和[領養申請的法庭](../Page/領養.md "wikilink")，稱為家事法庭。

[行政長官](../Page/行政長官.md "wikilink")[林鄭月娥於](../Page/林鄭月娥.md "wikilink")2017年的施政報告中提出將於銅鑼灣加路連山道興建一座新的區域法院綜合大樓，以全面解決各級法院的長遠需要。\[1\]

## 歷史

1991年前，香港共有六所地方法院，分別為:

  - [維多利亞地方法院](../Page/前法國外方傳道會大樓.md "wikilink")（處理港島案件）；
  - [九龍地方法院](../Page/前南九龍裁判法院.md "wikilink")（處理九龍案件）；
  - [沙田地方法院](../Page/沙田裁判法院.md "wikilink")、[粉嶺地方法院](../Page/前粉嶺裁判法院.md "wikilink")（處理新界東案件）；
  - [荃灣地方法院](../Page/荃灣裁判法院.md "wikilink")、[屯門地方法院](../Page/屯門裁判法院.md "wikilink")（處理新界西案件）。

1991年，所有地方法院的案件統一由灣仔現址處理。

## 歷任首席法官及現任法官

### 歷任首席法官

| 中文姓名                               | 任期                     |
| ---------------------------------- | ---------------------- |
| [貝珊法官](../Page/貝珊.md "wikilink")   | 1997年7月1日－1997年12月1日   |
| [韓敬善法官](../Page/韓敬善.md "wikilink") | 1999年3月19日－2001年5月15日  |
| [馮驊法官](../Page/馮驊.md "wikilink")   | 2001年5月16日－2006年11月26日 |
| [李瀚良法官](../Page/李瀚良.md "wikilink") | 2008年7月2日－2012年8月16日   |
| [潘兆童法官](../Page/潘兆童.md "wikilink") | 2012年9月17日－2019年1月13日  |

### 現任法官

| 首席區域法院法官                                  |
| ----------------------------------------- |
| 暫空缺                                       |
| 主任家事法庭法官                                  |
| [陳振國法官](../Page/陳振國.md "wikilink")        |
| 區域法院法官                                    |
| [黃一鳴法官](../Page/黃一鳴.md "wikilink")        |
| [葉佐文法官](../Page/葉佐文.md "wikilink")        |
| [高勁修法官](../Page/高勁修.md "wikilink")        |
| [郭啟安法官](../Page/郭啟安.md "wikilink")        |
| [許家灝法官](../Page/許家灝.md "wikilink")        |
| [林嘉欣法官](../Page/林嘉欣_\(法官\).md "wikilink") |
| [余啟肇法官](../Page/余啟肇.md "wikilink")        |
| [勞杰民法官](../Page/勞杰民.md "wikilink")        |
| [翁喬奇法官](../Page/翁喬奇.md "wikilink")        |
| [李俊文法官](../Page/李俊文_\(法官\).md "wikilink") |

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{港島綫色彩}}">█</font><a href="../Page/港島綫.md" title="wikilink">港島綫</a>：<a href="../Page/灣仔站.md" title="wikilink">灣仔站A</a>5出入口</li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 参考文献

### 引用

### 来源

  - [律政司](../Page/律政司.md "wikilink")，[《香港的法律制度》—法院](http://www.doj.gov.hk/chi/legal/#9)，香港：律政司，2004年.
  - 文思成，《香港政府與施政架構》，香港：三聯，1994年(增訂本).

## 外部連結

  - [香港區域法院](http://www.judiciary.gov.hk/tc/crt_services/pphlt/html/dc.htm)

## 参见

  - [香港司法機構](../Page/香港司法機構.md "wikilink")
  - [区域法院](../Page/区域法院.md "wikilink")

{{-}}

[Category:香港特别行政区法院](../Category/香港特别行政区法院.md "wikilink")
[Category:灣仔北](../Category/灣仔北.md "wikilink")

1.