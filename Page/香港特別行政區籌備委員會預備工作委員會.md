**香港特別行政區籌備委員會預備工作委員會**（簡稱**香港特區籌委會預委會**或**預委會**）是[全國人民代表大會常務委員會](../Page/全國人民代表大會常務委員會.md "wikilink")（全國人大常委會）下設的工作委員會，根據《全國人民代表大會常務委員會關於設立全國人大常委會香港特別行政區籌備委員會預備工作委員會的決定》成立。預備工作委員會共70名委員。1993年7月16日召開第一次全體會議，預委會共召開了6次全體會議，而轄下小組也舉行了多次會議和研討會。並在1995年12月[香港特別行政區籌備委員會成立後結束工作](../Page/香港特別行政區籌備委員會.md "wikilink")。

## 組織及成員

預委會轄下設有秘書處及五個專題小組，分別為：政務專題小組、經濟專題小組、法律專題小組、文化專題小組以及社會及保安專題小組。

  - 主任

[钱其琛](../Page/钱其琛.md "wikilink")

  - 副主任

[安子介](../Page/安子介.md "wikilink")　[霍英東](../Page/霍英東.md "wikilink")　[鲁平](../Page/鲁平.md "wikilink")　[周南](../Page/周南_\(新华社香港分社社长\).md "wikilink")　[姜恩柱](../Page/姜恩柱.md "wikilink")　[郑义](../Page/郑义_\(政治家\).md "wikilink")　[李福善](../Page/李福善.md "wikilink")

  - 委員（按姓名筆劃排列）

[万绍芬](../Page/万绍芬.md "wikilink")（女）　[方黃吉雯](../Page/方黃吉雯.md "wikilink")（女）　[王凤超](../Page/王凤超.md "wikilink")　[王启人](../Page/王启人.md "wikilink")　[王叔文](../Page/王叔文.md "wikilink")　[甘子玉](../Page/甘子玉.md "wikilink")　[田期玉](../Page/田期玉.md "wikilink")　[劉兆佳](../Page/劉兆佳.md "wikilink")　[劉皇發](../Page/劉皇發.md "wikilink")　[安子介](../Page/安子介.md "wikilink")　[朱幼麟](../Page/朱幼麟.md "wikilink")　[鄔維庸](../Page/鄔維庸.md "wikilink")　[李偉庭](../Page/李偉庭.md "wikilink")　[李澤添](../Page/李澤添.md "wikilink")　[李国华](../Page/李国华.md "wikilink")（女）　[李國寶](../Page/李國寶.md "wikilink")　[李福善](../Page/李福善.md "wikilink")　[李嘉誠](../Page/李嘉誠.md "wikilink")　[肖蔚云](../Page/肖蔚云.md "wikilink")　[吴建璠](../Page/吴建璠.md "wikilink")　[吳康民](../Page/吳康民.md "wikilink")　[邵天任](../Page/邵天任.md "wikilink")　[邵友保](../Page/邵友保.md "wikilink")　[陈元](../Page/陈元.md "wikilink")　[陳日新](../Page/陳日新_\(香港\).md "wikilink")　陈伟　[陈滋英](../Page/陈滋英.md "wikilink")　[郑义](../Page/郑义_\(政治家\).md "wikilink")　[范徐麗泰](../Page/范徐麗泰.md "wikilink")（女）　[羅叔清](../Page/羅叔清.md "wikilink")　[羅康瑞](../Page/羅康瑞.md "wikilink")　[羅德丞](../Page/羅德丞.md "wikilink")　[周小川](../Page/周小川.md "wikilink")　[周成奎](../Page/周成奎.md "wikilink")　[周南](../Page/周南_\(新华社香港分社社长\).md "wikilink")　[经叔平](../Page/经叔平.md "wikilink")　[姜恩柱](../Page/姜恩柱.md "wikilink")　[赵稷华](../Page/赵稷华.md "wikilink")　[查濟民](../Page/查濟民.md "wikilink")　[鍾士元](../Page/鍾士元.md "wikilink")　[俞晓松](../Page/俞晓松.md "wikilink")　[高尚全](../Page/高尚全.md "wikilink")　[秦文俊](../Page/秦文俊.md "wikilink")　[倪少傑](../Page/倪少傑.md "wikilink")　[徐四民](../Page/徐四民.md "wikilink")　[徐展堂](../Page/徐展堂.md "wikilink")　[徐惠滋](../Page/徐惠滋.md "wikilink")　[钱其琛](../Page/钱其琛.md "wikilink")　[梁振英](../Page/梁振英.md "wikilink")　[黃保欣](../Page/黃保欣.md "wikilink")　[曾憲梓](../Page/曾憲梓.md "wikilink")　[曾鈺成](../Page/曾鈺成.md "wikilink")　[鲁平](../Page/鲁平.md "wikilink")　[廖瑤珠](../Page/廖瑤珠.md "wikilink")（女）　[譚惠珠](../Page/譚惠珠.md "wikilink")（女）　[譚耀宗](../Page/譚耀宗.md "wikilink")　[霍英東](../Page/霍英東.md "wikilink")　[乌兰木伦](../Page/乌兰木伦.md "wikilink")\[1\]　[张良栋](../Page/张良栋.md "wikilink")\[2\]　[徐泽](../Page/徐泽.md "wikilink")\[3\]　[翁心桥](../Page/翁心桥.md "wikilink")\[4\]　[郭丰民](../Page/郭丰民.md "wikilink")\[5\]　[王英偉](../Page/王英偉.md "wikilink")\[6\]　[劉漢銓](../Page/劉漢銓.md "wikilink")\[7\]　[李祖澤](../Page/李祖澤.md "wikilink")\[8\]　[吳家瑋](../Page/吳家瑋.md "wikilink")\[9\]　[吳清輝](../Page/吳清輝.md "wikilink")\[10\]　[鄭明訓](../Page/鄭明訓.md "wikilink")\[11\]　[賈施雅](../Page/賈施雅.md "wikilink")\[12\]　[黃宜弘](../Page/黃宜弘.md "wikilink")\[13\]

  - 秘書長

[鲁平](../Page/鲁平.md "wikilink")（兼）

  - 副秘書長

[秦文俊](../Page/秦文俊.md "wikilink")　[陈滋英](../Page/陈滋英.md "wikilink")

## 注釋

## 参见

  - [香港特別行政區籌備委員會](../Page/香港特別行政區籌備委員會.md "wikilink")

[Category:香港過渡時期史](../Category/香港過渡時期史.md "wikilink")
[Category:全国人大常委会](../Category/全国人大常委会.md "wikilink")

1.  1994年5月12日，第八屆全國人大常委會第七次會議決定增補的內地委員

2.
3.
4.
5.
6.  1994年5月12日，第八屆全國人大常委會第七次會議決定增補的香港委員

7.
8.
9.
10.
11.
12.
13.