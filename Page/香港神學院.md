[BibleSeminaryOfHongKong.jpg](https://zh.wikipedia.org/wiki/File:BibleSeminaryOfHongKong.jpg "fig:BibleSeminaryOfHongKong.jpg")
**香港神學院**（）於1952年在[香港創立](../Page/香港.md "wikilink")，原名為**香港聖經學院**，最初發起人為華僑遍傳福音會的余履真、顏東美、許蓁蓁、戎玉琴等，贊助人及顧問是[中華傳道會負責人宋德成牧師](../Page/中華傳道會.md "wikilink")，第一任院長是中華傳道會前任監督周志禹牧師，院址設在[紅磡](../Page/紅磡.md "wikilink")[馬頭圍道](../Page/馬頭圍道.md "wikilink")。1960年搬到[九龍塘](../Page/九龍塘.md "wikilink")[金巴倫道十七號](../Page/金巴倫道.md "wikilink")（鄰近三角公園）現址。1970年改名為香港神學院，並發展神學學位課程。自2003年起，每年4月均舉辦大型公開講座。

## 全職教師

以下為香港神學院的全時間教師團隊：\[1\]

  - 褚永華牧師：教授
  - 張天和牧師：實用神學科副教授、教務主任
  - 趙崇明博士：神學及歷史科副教授、圖書館主任及學術主任
  - 張祥志先生：聖經科副教授、院政及拓展主任
  - 張慧玲小姐：聖經及實用神學副教授、校牧
  - 蘇遠泰博士：神學及歷史科副教授、資訊科技主任及支持講師行動統籌
  - [鄧瑞強博士](../Page/鄧瑞強.md "wikilink")：神學及歷史科副教授、校牧
  - 蔡式平博士：聖經科副教授、延伸部主任及校友會顧問
  - 梁俊豪博士：聖經科副教授、實習部主任

## 校舍設備

香港神學院建有兩個禮堂、多個課室、圖書館（藏書約二萬冊）、電腦中心、溫習室、閒情閣（視聽休息室）、飯堂、學生宿舍、講師宿舍、祈禱室、練琴室、學生活動中心及空中花園等等。

## 課程資料

香港神學院1970年起開展神學學位課程，1990年設立信徒神學文憑課程，1998年設立聖經延伸證書課程。2006年新設3個全新碩士課程：道學碩士、道學碩士（教牧進修）及基督教研究碩士課程。

### 主要課程

  - 神學文憑（Diploma in Theology）：全日制，修讀1年，部分時間制修讀2至5年，共修讀36學分。
  - 神學學士（Bachelor of Theology）：全日制，修讀4年，共修讀139學分。
  - 基督教研究碩士（Master of Christian Studies）：部分時間制，最少3年，最多7年內完成，共修讀51學分。
  - 道學碩士（教牧進修）（Master of Divinity（Pastoral
    Studies））：部分時間制，最多7年內完成，共修讀70學分。
  - 道學碩士（Master of Divinity）：全日制，修讀3年，共修讀106學分。

### 神學延伸課程

神學延伸課程分聖經研究、神學及歷史、實用神學三個範圍，旨在讓學員對信仰知識有一全面的認識。課程著重學科的實用性，每科只限6至8堂，課程不設就讀年期要求。延伸課程為證書課程，學員必須修畢8科，其中包括至少4科聖經研究範圍科目，原則上學員可在一年內修畢本證書課程。

### 已停辦的課程

  - 聖經研究副學士（Assoicate Degree in Biblical Studies）：2002年開設此課程，2006年停辦。
  - 文學碩士（教牧研究）：1999年與美國國際神學研究院（International Theological
    Seminary）合辦此課程，2006年停辦。

## 主要交通

九龍塘站（東鐵線及觀塘線）

## 外部連結

  - [香港神學院](http://www.bshk.edu.hk/)

[Category:香港神學院](../Category/香港神學院.md "wikilink")
[Category:九龍塘](../Category/九龍塘.md "wikilink")
[Category:中華傳道會](../Category/中華傳道會.md "wikilink")
[Category:香港基督教新教學校](../Category/香港基督教新教學校.md "wikilink")
[Category:1952年創建的教育機構](../Category/1952年創建的教育機構.md "wikilink")

1.  {{ cite web |url=<https://www.bshk.edu.hk/index.php/team/lecturer>
    |title=港師團隊(全時間) |date= |publisher=香港神學院 |author= }}