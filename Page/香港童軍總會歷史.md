## 成立之初

| 時間         | 事件                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ---------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1909年      | 香港童軍由[英國的](../Page/英國.md "wikilink")[童軍運動推廣進入](../Page/童軍運動.md "wikilink")\[1\]。                                                                                                                                                                                                                                                                                                                                                  |
| 1911年      | 一些[英國商人及軍人才正式組織童軍旅團](../Page/英國.md "wikilink")。                                                                                                                                                                                                                                                                                                                                                                                   |
| 1912年      | [香港總督](../Page/香港總督.md "wikilink")[梅含理爵士出任](../Page/梅含理.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")。                                                                                                                                                                                                                                                                                                                   |
| 1912年4月16日 | [童軍運動始創人](../Page/童軍運動.md "wikilink")[貝登堡勳爵訪問](../Page/貝登堡.md "wikilink")[香港](../Page/香港.md "wikilink")。                                                                                                                                                                                                                                                                                                                          |
| 1913年9月20日 | [聖若瑟書院的](../Page/聖若瑟書院.md "wikilink")**[香港第1旅](../Page/香港第1旅.md "wikilink")**率先成立。                                                                                                                                                                                                                                                                                                                                                |
| 1914年5月1日  | [香港第1旅正式在](../Page/香港第1旅.md "wikilink")[英國童軍總會註冊](../Page/英國童軍總會.md "wikilink")，成為香港童軍史上的第一個旅團。同年，[麥夏德少校被委任為](../Page/麥夏德.md "wikilink")[香港總監](../Page/#總領袖及總監.md "wikilink")。                                                                                                                                                                                                                                                    |
| 1915年3月20日 | [香港首次](../Page/香港.md "wikilink")[童軍大會操舉行](../Page/#香港童軍大會操.md "wikilink")。                                                                                                                                                                                                                                                                                                                                                        |
| 1915年7月    | 「香港童軍總會」組成，負責統籌一般訓練及活動事務。然而，由於適逢[第一次世界大戰的爆發](../Page/第一次世界大戰.md "wikilink")，當時在港的[英軍](../Page/英軍.md "wikilink")，包括大部分的童軍領袖，都需要回國服務，因此[香港的](../Page/香港.md "wikilink")[童軍運動被迫暫時進入了休眠狀態](../Page/童軍運動.md "wikilink")。                                                                                                                                                                                                                |
| 1919年      | [香港總督](../Page/香港總督.md "wikilink")[司徒拔爵士出任](../Page/司徒拔.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")。                                                                                                                                                                                                                                                                                                                   |
| 1920年      | 適逢戰後，[香港第1旅創辦人之一的](../Page/香港第1旅.md "wikilink")[寶雲中校](../Page/寶雲_\(童軍\).md "wikilink")（前少校）再到香港重組香港童軍總會。                                                                                                                                                                                                                                                                                                                          |
| 1921年1月8日  | 在[花園道口的](../Page/花園道.md "wikilink")[美利操場](../Page/美利操場.md "wikilink")（現[長江集團中心的位置](../Page/長江集團中心.md "wikilink")）舉行戰後首次[童軍大會操](../Page/#香港童軍大會操.md "wikilink")，到現在仍有定期舉行 \[2\]。                                                                                                                                                                                                                                                   |
| 1921年      | [華德利牧師被委任為](../Page/華德利.md "wikilink")[香港總監](../Page/#總領袖及總監.md "wikilink")。                                                                                                                                                                                                                                                                                                                                                      |
| 1922年4月6日  | 當時[英國的](../Page/英國.md "wikilink")[威爾斯親王](../Page/威爾斯親王.md "wikilink")（亦稱[威爾斯太子](../Page/威爾斯太子.md "wikilink")，也是後來的[英王](../Page/英王.md "wikilink")[愛德華八世](../Page/愛德華八世.md "wikilink")）訪問[香港](../Page/香港.md "wikilink")，並捐贈一面繡有其「紋章」之錦旗授給香港童軍總會作為童軍比賽的錦標。                                                                                                                                                                           |
| 1923年      | 首屆[威爾斯太子錦標賽舉行](../Page/威爾斯太子錦標賽.md "wikilink")，到現在仍有定期舉行（於1999年起更名為[深資童軍錦標賽](../Page/深資童軍錦標賽.md "wikilink")）。                                                                                                                                                                                                                                                                                                                     |
| 1925年      | [香港總督](../Page/香港總督.md "wikilink")[金文泰爵士出任](../Page/金文泰.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")。                                                                                                                                                                                                                                                                                                                   |
| 1929年      | 初期的童軍活動沒有一個固定的會址，多年來都是臨時借用舊政府大樓的房間作為辦公室及集會之用，而一些經常性活動也得借用位於[告士打道的](../Page/告士打道.md "wikilink")「[海員會所](../Page/香港海員俱樂部.md "wikilink")」來舉行。但在這一年，香港童軍總會以16,000港元購得位於[柴灣海旁的一個營地](../Page/柴灣.md "wikilink")，命名為「[柴灣營地](../Page/柴灣營地.md "wikilink")」（[柴灣公園現址](../Page/柴灣公園.md "wikilink")）。同年，香港童軍開始參與國際性童軍活動，參加了[英格蘭西北部的](../Page/英格蘭.md "wikilink")[伯肯赫德鎮舉行](../Page/伯肯赫德.md "wikilink")「第3屆[世界童軍大會](../Page/世界童軍大會.md "wikilink")」。 |
| 1930年      | [香港總督](../Page/香港總督.md "wikilink")[貝璐爵士出任](../Page/貝璐.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")。                                                                                                                                                                                                                                                                                                                     |
| 1934年      | [侯利華會督被委任為](../Page/侯利華.md "wikilink")[香港總監](../Page/#總領袖及總監.md "wikilink")。                                                                                                                                                                                                                                                                                                                                                      |

\[3\]

## 第二次世界大戰期間\[4\]

| 時間                                       | 事件                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ---------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1936年                                    | [香港總督](../Page/香港總督.md "wikilink")[郝德傑爵士出任](../Page/郝德傑.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")。同年，首屆[童軍木章訓練班(第二段)在](../Page/童軍木章訓練班\(第二段\).md "wikilink")[柴灣營地舉行](../Page/柴灣營地.md "wikilink")。                                                                                                                                                                                                                                                                                                                                       |
| 1937年                                    | [香港總督](../Page/香港總督.md "wikilink")[羅富國爵士出任](../Page/羅富國.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")。                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| [第二次世界大戰](../Page/第二次世界大戰.md "wikilink") | [中國全面](../Page/中國.md "wikilink")[抗戰](../Page/抗日戰爭_\(中國\).md "wikilink")，大量[難民湧入](../Page/難民.md "wikilink")[香港](../Page/香港.md "wikilink")，童軍不但分擔救助難民工作，香港童軍總會還應[香港政府要求](../Page/香港政府.md "wikilink")，分擔[防空](../Page/防空.md "wikilink")、[傳訊和](../Page/傳訊.md "wikilink")[救護等工作](../Page/救護.md "wikilink")，並且成立[童軍傳訊隊](../Page/童軍傳訊隊.md "wikilink")。一些童軍領袖或年長的童軍成員在各類民防單位工作，如在後備[消防](../Page/消防.md "wikilink")、後備[警察](../Page/警察.md "wikilink")、[特務警察或在義勇軍中服役](../Page/特務警察.md "wikilink")。他們在[香港保衛戰中作出了貢獻](../Page/香港保衛戰.md "wikilink")，一些隊員甚至犧牲了生命\[5\]。 |
| 1940年8月                                  | 首屆[-{樂行}-童軍大會在](../Page/樂行童軍大會.md "wikilink")[香港島](../Page/香港島.md "wikilink")[聶高信山舉行](../Page/聶高信山.md "wikilink")。                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 1941年                                    | [香港總督](../Page/香港總督.md "wikilink")[楊慕琦爵士出任](../Page/楊慕琦.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")。其時童軍運動成員已達1500人。                                                                                                                                                                                                                                                                                                                                                                                                                      |

## 戰後發展\[6\]

[日軍投降後](../Page/日軍.md "wikilink")，[香港遍地殘垣](../Page/香港.md "wikilink")，[英國童軍總會特別派遺了一隊](../Page/英國童軍總會.md "wikilink")「國際童子軍支援服務隊」來[港](../Page/香港.md "wikilink")，協助重建工作。其後，在[侯利華會督及](../Page/侯利華.md "wikilink")[柯昭璋的領導下](../Page/柯昭璋.md "wikilink")，[童軍運動再度復活](../Page/童軍運動.md "wikilink")，1946年的童軍運動成員突破2000人。亦成立了「先進童軍」，即現時的深資童軍。

1947年，[香港總督](../Page/香港總督.md "wikilink")[葛量洪爵士士出任](../Page/葛量洪.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")。而且得到軍部的幫助，撥出[花園道近](../Page/花園道.md "wikilink")[山頂纜車站的一塊土地建造新總部之用](../Page/山頂纜車.md "wikilink")，由當年香港童軍總會會長[摩士爵士主持落成儀式](../Page/摩士.md "wikilink")，一所命名為「[摩士小屋](../Page/摩士小屋.md "wikilink")」的新總部於1949年11月12日正式啟用。同年，[田家騰出任首位](../Page/田家騰.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。

1950年，[柯昭璋被委任為](../Page/柯昭璋.md "wikilink")[香港總監](../Page/#總領袖及總監.md "wikilink")。

1951年，香港童軍總會將香港劃分為香港區、九龍區、新界區3個童軍區。同年，香港童軍總會成立[香港訓練總隊](../Page/香港訓練總隊.md "wikilink")、舉行首屆[嘉爾頓錦標比賽](../Page/嘉爾頓錦標比賽.md "wikilink")；[盧孟暄被委任為](../Page/盧孟暄.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")；香港童軍代表團參加第7屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")。

1952年，香港區及新界區各自劃分為2個童軍區。

1953年，[陸榮生被委任為](../Page/陸榮生.md "wikilink")[香港總監](../Page/#總領袖及總監.md "wikilink")，[韓志海被委任為](../Page/韓志海.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。

1954年，[高本被委任為](../Page/高本.md "wikilink")[香港總監](../Page/#總領袖及總監.md "wikilink")，[赫達被委任為](../Page/赫達.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。同年6月24日，[英國及](../Page/英國童軍總領袖.md "wikilink")[英聯邦童軍總領袖](../Page/英聯邦童軍總領袖.md "wikilink")[羅華倫勳爵訪港](../Page/羅華倫.md "wikilink")。

1956年8月1日，弱能童軍支部成立。同年，香港童軍代表團參加第3屆[泛太平洋區大露營](../Page/泛太平洋區大露營.md "wikilink")。

1957年，首屆[訓練隊隊員專訓練班舉行](../Page/訓練隊隊員專訓練班.md "wikilink")。同年，香港童軍代表團參加第9屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")。同年10月19日至10月22日，首次[香港童軍大露營在](../Page/香港童軍大露營.md "wikilink")[新界](../Page/新界.md "wikilink")[上水](../Page/上水.md "wikilink")[金錢村舉行](../Page/金錢村.md "wikilink")。

1958年，[香港總督](../Page/香港總督.md "wikilink")[柏立基爵士出任](../Page/柏立基.md "wikilink")[香港童軍總領袖](../Page/#總領袖及總監.md "wikilink")，[馬基被委任為](../Page/馬基.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。同年，[童軍運動始創人](../Page/童軍運動.md "wikilink")[貝登堡勳爵夫人訪港](../Page/貝登堡.md "wikilink")。同年8月29日至8月30日，第1屆[基維爾聯誼會在](../Page/基維爾聯誼會.md "wikilink")[新界](../Page/新界.md "wikilink")[將軍澳舉行](../Page/將軍澳.md "wikilink")。

1959年，香港童軍代表團參加第10屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")。

## 六七十年代\[7\]

[Prince_of_Wales_Banner.jpg](https://zh.wikipedia.org/wiki/File:Prince_of_Wales_Banner.jpg "fig:Prince_of_Wales_Banner.jpg")錦旗\]\]
六七十年代是香港童軍的黃金時期開始，首先是於1960年獲[香港政府撥出一塊於](../Page/香港政府.md "wikilink")[九龍](../Page/九龍.md "wikilink")[飛鵝山約](../Page/飛鵝山.md "wikilink")200,000平方米之土地用作童軍營地，定名為「[基維爾營地](../Page/基維爾營地.md "wikilink")」，後再獲撥出位於[荃灣西約的土地](../Page/荃灣西約.md "wikilink")，名命為「[下花山營地](../Page/下花山營地.md "wikilink")」。

1960年，[黃境康被委任為](../Page/黃境康.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。

1961年，[英國及](../Page/英國童軍總領袖.md "wikilink")[英聯邦童軍總領袖](../Page/英聯邦童軍總領袖.md "wikilink")[麥祺連爵士訪港](../Page/麥祺連.md "wikilink")。同年，[黃益康被委任為](../Page/黃益康.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。

而1961年，50週年金禧紀念，香港童軍總會於12月27日至1962年1月2日在[啟德機場附近的](../Page/啟德機場.md "wikilink")[九龍仔](../Page/九龍仔.md "wikilink")(即現在的[九龍仔公園](../Page/九龍仔公園.md "wikilink"))舉行了首次在[香港舉行之國際大](../Page/香港.md "wikilink")[露營](../Page/露營.md "wikilink")─「[香港金禧大露營](../Page/香港金禧大露營.md "wikilink")」，參加者共2,732人。\[8\]

1962年，香港童軍代表團參加第3屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")。同年，[貝登堡勳爵夫人訪港](../Page/貝登堡.md "wikilink")。

1963年，[羅徵勤被委任為](../Page/羅徵勤.md "wikilink")[香港總監](../Page/香港總監.md "wikilink")。同年，香港童軍總會於[羅富國教育學院成立](../Page/羅富國教育學院.md "wikilink")[童軍輔助團](../Page/童軍輔助團.md "wikilink")；香港童軍代表團參加第11屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")。

1964年，[香港總督](../Page/香港總督.md "wikilink")[戴麟趾爵士出任](../Page/戴麟趾.md "wikilink")[香港童軍總領袖](../Page/香港童軍總領袖.md "wikilink")，[馬基再次出任](../Page/馬基.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。同年，香港童軍代表團以觀察員身分參加第4屆[遠東區童軍會議](../Page/遠東區童軍會議.md "wikilink")。

1965年，[香港總監](../Page/香港總監.md "wikilink")[羅徵勤以英國童軍代表團成員身分](../Page/羅徵勤.md "wikilink")，參加[世界童軍會議](../Page/世界童軍會議.md "wikilink")。同年，[盧觀榮被委任為](../Page/盧觀榮.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。

1966年，香港童軍代表團參加第4屆[世界童軍訓練會議](../Page/世界童軍訓練會議.md "wikilink")、第4屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")。同年，香港童軍總會成立首個[見習領袖旅](../Page/見習領袖旅.md "wikilink")；[貝登堡勳爵夫人訪港](../Page/貝登堡.md "wikilink")。

1967年，[馬基再次出任](../Page/馬基.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。同年，香港童軍總會實施第1個[五年拓展計劃](../Page/五年拓展計劃.md "wikilink")、在[九龍](../Page/九龍.md "wikilink")[基維爾營地舉行](../Page/基維爾營地.md "wikilink")[小隊長大露營](../Page/小隊長大露營.md "wikilink")；香港童軍代表團參加第12屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")；。

1968年，香港童軍總會成為[遠東區童軍議會之輔助會員](../Page/遠東區童軍議會.md "wikilink")。同年3月31日，[新界](../Page/新界.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[下花山營地正式開放使用](../Page/下花山營地.md "wikilink")。同年，香港童軍代表團參加第6屆[遠東區童軍會議](../Page/遠東區童軍會議.md "wikilink")；「[港島地域](../Page/香港童軍總會港島地域.md "wikilink")」、「[九龍地域](../Page/香港童軍總會九龍地域.md "wikilink")」及「[新界地域](../Page/香港童軍總會新界地域.md "wikilink")」成立。

1969年，[童軍知友社成立](../Page/童軍知友社.md "wikilink")。同年，香港童軍代表團以觀察員身分出席第22屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")。

1970年，香港童軍代表團參加第5屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")、出席第7屆[遠東區童軍會議](../Page/遠東區童軍會議.md "wikilink")。同年，香港童軍總會舉行首屆[香港總監盾幼童軍比賽](../Page/香港總監盾幼童軍比賽.md "wikilink")；[陳溥志被委任為](../Page/陳溥志.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。

1970年，由於[柴灣的發展](../Page/柴灣.md "wikilink")，[柴灣營地被](../Page/柴灣營地.md "wikilink")[香港政府收回](../Page/香港政府.md "wikilink")，於是[香港政府在](../Page/香港政府.md "wikilink")[大潭撥地讓香港童軍成立](../Page/大潭_\(香港\).md "wikilink")[大潭童軍中心](../Page/大潭童軍中心.md "wikilink")，此外香港童軍又在[大埔區增設了](../Page/大埔區.md "wikilink")[洞梓童軍中心](../Page/洞梓童軍中心.md "wikilink")。

1971年，[香港總督](../Page/香港總督.md "wikilink")[麥理浩爵士出任](../Page/麥理浩.md "wikilink")[香港童軍總領袖](../Page/香港童軍總領袖.md "wikilink")、[洪昭安被委任為](../Page/洪昭安.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。同年7月22日至7月28日，香港童軍總會舉行[鑽禧大露營](../Page/鑽禧大露營.md "wikilink")。同年7月23日，[香港郵政發行](../Page/香港郵政.md "wikilink")3枚[童軍鑽禧紀念郵票](../Page/童軍鑽禧紀念郵票.md "wikilink")。同年，香港童軍總會主辦第2屆[世界童軍公共關係研討會](../Page/世界童軍公共關係研討會.md "wikilink")、第5屆[英聯邦童軍會議](../Page/英聯邦童軍會議.md "wikilink")、舉行首屆[亞太區專業童軍領袖訓練班](../Page/亞太區專業童軍領袖訓練班.md "wikilink")、在[香港島](../Page/香港島.md "wikilink")[馬禮遜堂舉行首屆](../Page/馬禮遜堂.md "wikilink")[深資童軍領袖會議](../Page/深資童軍領袖會議.md "wikilink")；香港童軍代表團參加第13屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")。

1972年，香港童軍總會實施第2個[五年拓展計劃](../Page/五年拓展計劃.md "wikilink")。同年，香港童軍代表團出席第8屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")；[大潭童軍營地開放啟用](../Page/大潭童軍營地.md "wikilink")。

1973年，[馬基被委任為](../Page/馬基.md "wikilink")[香港總監](../Page/香港總監.md "wikilink")。同年，刊物《香港童軍》創刊；香港童軍代表團參加首屆[亞太區暨菲律賓金禧大露營](../Page/亞太區暨菲律賓金禧大露營.md "wikilink")。

1974年，[古匡昌被委任為香港訓練總監](../Page/古匡昌.md "wikilink")。同年，香港童軍代表團出席第9屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、參加第6屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")；[童軍知友社榮獲](../Page/童軍知友社.md "wikilink")[亞太區童軍傑出社會服務獎](../Page/亞太區童軍傑出社會服務獎.md "wikilink")；香港童軍總會成立審訂深資童軍支部及童軍服務員之訓練課程的研究組、成立檢討現行之領袖訓練政策的研究組、在[烏溪沙青年新村舉行](../Page/烏溪沙青年新村.md "wikilink")[青年隊伍聯誼大會](../Page/青年隊伍聯誼大會.md "wikilink")、舉行[全港區總監會議](../Page/全港區總監會議.md "wikilink")、舉行[全港旅團領袖評議會](../Page/全港旅團領袖評議會.md "wikilink")、設立[童軍獎勵計劃](../Page/童軍獎勵計劃.md "wikilink")、開始推行[領袖意見調查](../Page/領袖意見調查.md "wikilink")、開始推行[旅團實況調查](../Page/旅團實況調查.md "wikilink")。

1975年3月9日，[大埔](../Page/大埔_\(香港\).md "wikilink")[洞梓童軍中心正式開放啟用](../Page/洞梓童軍中心.md "wikilink")。同年，香港童軍總會舉行第1屆[全港童軍青年論壇](../Page/全港童軍青年論壇.md "wikilink")、成立接納外界女性加入深資童軍支部之試驗性拓展團；香港童軍代表團出席第3屆[亞太區社區發展研討會](../Page/亞太區社區發展研討會.md "wikilink")、參加第14屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")；新領袖訓練政策研究組及審訂深資童軍支部及童軍服務員訓練課程研究組之報告書面世；[馬基兼任](../Page/馬基.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")、[鄭永生被委任為](../Page/鄭永生.md "wikilink")[亞太區童軍公共關係工作小組委員](../Page/亞太區童軍公共關係工作小組.md "wikilink")。

1976年，香港童軍總會舉辦第4屆[亞太區社區發展研討會](../Page/亞太區社區發展研討會.md "wikilink")、成立[訓練研究委員會](../Page/訓練研究委員會.md "wikilink")；香港童軍總會[會務委員會通過新會之會章](../Page/會務委員會.md "wikilink")；[東九龍地域成立](../Page/香港童軍總會東九龍地域.md "wikilink")；香港童軍代表團出席第10屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、參加由[菲律賓童軍總會贊助之](../Page/菲律賓童軍總會.md "wikilink")[童軍交換計劃](../Page/童軍交換計劃.md "wikilink")、參加由[日本童軍總會贊助之](../Page/日本童軍總會.md "wikilink")[童軍交換計劃](../Page/童軍交換計劃.md "wikilink")。

1977年4月16日，香港童軍總會成為[世界童軍組織的第](../Page/世界童軍組織.md "wikilink")111名會員，脫離了[英國童軍總會](../Page/英國童軍總會.md "wikilink")。
\[9\]

同年，[麥理浩爵士出任](../Page/麥理浩爵士.md "wikilink")[香港總領袖](../Page/香港總領袖.md "wikilink")、[馬基出任](../Page/馬基.md "wikilink")[香港總監](../Page/香港總監.md "wikilink")、[石耀祖被委任為](../Page/石耀祖.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")；香港童軍總會成立[銀禧區](../Page/銀禧區.md "wikilink")、公佈[七年拓展計劃(計劃E)政策文件](../Page/七年拓展計劃\(計劃E\).md "wikilink")、開始推動[銀禧計劃](../Page/銀禧計劃.md "wikilink")、舉行[銀禧紀念餐舞會](../Page/銀禧紀念餐舞會.md "wikilink")、在[新界](../Page/新界.md "wikilink")[上水](../Page/上水.md "wikilink")[鳳溪中學舉行第](../Page/鳳溪中學.md "wikilink")3屆[青年隊伍聯誼大會](../Page/青年隊伍聯誼大會.md "wikilink")、重設樂行童軍支部；香港童軍代表團出席第26屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")、前往[馬來西亞童軍總會](../Page/馬來西亞童軍總會.md "wikilink")、[新加坡童軍總會](../Page/新加坡童軍總會.md "wikilink")、[印度尼西亞童軍總會及](../Page/印度尼西亞童軍總會.md "wikilink")[泰國童軍總會訪問](../Page/泰國童軍總會.md "wikilink")、出席第2屆[亞太區童軍大露營](../Page/亞太區童軍大露營.md "wikilink")、[印度尼西亞全國童軍大露營](../Page/印度尼西亞全國童軍大露營.md "wikilink")、[韓國全國童軍大露營及](../Page/韓國全國童軍大露營.md "wikilink")[日本全國童軍大露](../Page/日本全國童軍大露.md "wikilink")。

1978年，香港童軍總會正式接納女性為深資童軍支部成員。同年，香港童軍總會通過「香港童軍總會《政策、組織及規條》第一冊」、通過[十年計劃之目標](../Page/十年計劃.md "wikilink")、通過女性可擔任各級領袖及總監人員之職務、通過修訂會章、舉辦第11屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、[香港第](../Page/香港.md "wikilink")1屆[領袖訓練員專訓班](../Page/領袖訓練員專訓班.md "wikilink")；[港島地域總部](../Page/香港童軍總會港島地域.md "wikilink")[鄧肇堅大廈啟用](../Page/鄧肇堅大廈.md "wikilink")；香港童軍代表團參加第1屆[亞太區社區服務營](../Page/亞太區社區服務營.md "wikilink")、第4屆[馬來西亞全國童軍大露營及第](../Page/馬來西亞全國童軍大露營.md "wikilink")7屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")。

1979年，香港童軍代表團出席第27屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")、參加第3屆[特能童軍大露營](../Page/特能童軍大露營.md "wikilink")、[亞太區訓練研討會及第](../Page/亞太區訓練研討會.md "wikilink")4屆[亞太區童軍大露營](../Page/亞太區童軍大露營.md "wikilink")。同年，香港童軍總會在[九龍](../Page/九龍.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")[海運大廈舉行](../Page/海運大廈.md "wikilink")「歡樂的童軍」大型展覽會。

## 八九十年代\[10\]

1980年，[香港總監](../Page/香港總監.md "wikilink")[馬基獲選為](../Page/馬基.md "wikilink")[亞太區童軍委員會委員](../Page/亞太區童軍委員會.md "wikilink")，並獲[世界童軍組織頒授](../Page/世界童軍組織.md "wikilink")[銅狼勳章](../Page/銅狼勳章.md "wikilink")。同年，第1屆[全港童軍總監政務會議舉行](../Page/全港童軍總監政務會議.md "wikilink")；香港童軍總會實施新修訂之[支部領袖訓練系統](../Page/支部領袖訓練系統.md "wikilink")、修訂組織系統、將-{zh-hans:羅浮;zh-hant:樂行}-童軍支部納入[訓練署](../Page/訓練署.md "wikilink")；電視播映共13集有關[童軍運動之戲劇性連續片集](../Page/童軍運動.md "wikilink")《[小虎隊](../Page/小虎隊.md "wikilink")》；香港童軍代表團參加第12屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、第10屆[亞太區童軍社區發展研討會](../Page/亞太區童軍社區發展研討會.md "wikilink")、[日本童軍總會之邀請活動](../Page/日本童軍總會.md "wikilink")、第10屆[泰國童軍大露營](../Page/泰國童軍大露營.md "wikilink")、[新加坡童軍七十週年紀念大露營](../Page/新加坡童軍七十週年紀念大露營.md "wikilink")；[世界童軍基金會主席](../Page/世界童軍基金會.md "wikilink")[富馬博士訪港](../Page/富馬.md "wikilink")；[石耀祖被委任為](../Page/石耀祖.md "wikilink")[亞太區童軍訓練小組委員會副主席](../Page/亞太區童軍訓練小組委員會.md "wikilink")；[梁超文被委任為](../Page/梁超文.md "wikilink")[亞太區童軍財務小組委員會委員](../Page/亞太區童軍財務小組委員會.md "wikilink")。

1981年，[黃楊子潔被委任為](../Page/黃楊子潔.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")。同年，香港童軍代表團出席第28屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")、[美國全國童軍大露營](../Page/美國全國童軍大露營.md "wikilink")、[亞太區童軍大露營](../Page/亞太區童軍大露營.md "wikilink")、訪問[斯里蘭卡童軍總會](../Page/斯里蘭卡童軍總會.md "wikilink")、出席[日本童軍總會之邀請節目](../Page/日本童軍總會.md "wikilink")；香港童軍總會舉辦各項特別節目以配合[國際傷殘人士年](../Page/國際傷殘人士年.md "wikilink")、公佈「[八十年代的主要方針](../Page/八十年代的主要方針.md "wikilink")」、主辦[國際特能童軍研討會](../Page/國際特能童軍研討會.md "wikilink")。

1982年，[香港總督](../Page/香港總督.md "wikilink")[尤德爵士出任為](../Page/尤德.md "wikilink")[香港童軍總領袖](../Page/香港童軍總領袖.md "wikilink")。同年，[大同地域成立](../Page/大同地域.md "wikilink")；香港童軍總會舉辦「[世界童軍年](../Page/世界童軍年.md "wikilink")」各項節目活動以慶祝[世界童軍](../Page/世界童軍.md "wikilink")75週年、主辦[香港國際大露營](../Page/香港國際大露營.md "wikilink")、進行「[天虹計劃](../Page/天虹計劃.md "wikilink")」；香港童軍代表團出席第13屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、第7屆[亞太區童軍大露營](../Page/亞太區童軍大露營.md "wikilink")、[傷健大露營](../Page/傷健大露營.md "wikilink")、[白浪島大露營](../Page/白浪島大露營.md "wikilink")、第8屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")、[日本童軍總會之邀請計劃](../Page/日本童軍總會.md "wikilink")、第8屆[亞太區童軍大露營](../Page/亞太區童軍大露營.md "wikilink")。

1983年，香港童軍代表團參加第15屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")、出席第29屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")、[特能童軍活動研討會](../Page/特能童軍活動研討會.md "wikilink")、[泰國全國深資童軍大露營](../Page/泰國全國深資童軍大露營.md "wikilink")、第3屆[亞太區童軍公共關係研討會](../Page/亞太區童軍公共關係研討會.md "wikilink")、[瑞典國際大露營](../Page/瑞典國際大露營.md "wikilink")、[日本童軍總會之邀請計劃](../Page/日本童軍總會.md "wikilink")、[防止濫用藥物童軍研討會](../Page/防止濫用藥物童軍研討會.md "wikilink")。同年，[白沙灣海上活動中心啟用](../Page/白沙灣海上活動中心.md "wikilink")。

1984年，[許宗盛被委任為](../Page/許宗盛.md "wikilink")[亞太區童軍社區發展小組委員會委員](../Page/亞太區童軍社區發展小組委員會.md "wikilink")，並獲[泰國童軍總會頒授](../Page/泰國童軍總會.md "wikilink")[一級榮譽獎章](../Page/一級榮譽獎章.md "wikilink")。同年，[盧觀榮被委任為](../Page/盧觀榮.md "wikilink")[亞太區童軍財務小組委員會委員](../Page/亞太區童軍財務小組委員會.md "wikilink")、[黃楊子潔被委任為](../Page/黃楊子潔.md "wikilink")[亞太區童軍訓練委員會主席及](../Page/亞太區童軍訓練委員會.md "wikilink")[世界童軍訓練委員會委員](../Page/世界童軍訓練委員會.md "wikilink")、[盧偉誠被委任為](../Page/盧偉誠.md "wikilink")[亞太區童軍青少年活動小組委員會委員](../Page/亞太區童軍青少年活動小組委員會.md "wikilink")；香港童軍總會舉辦多項節目活動以嚮應[國際青年年](../Page/國際青年年.md "wikilink")、舉行[亞太區專業童軍執行幹事管理証書課程](../Page/亞太區專業童軍執行幹事管理証書課程.md "wikilink")、成立小童軍訓練支部、開始接納女童為幼童軍支部成員；香港童軍代表團參加第14屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、第1屆[菲律賓童軍社區拓展研討會](../Page/菲律賓童軍社區拓展研討會.md "wikilink")、[國際童軍活動與訓練研討會](../Page/國際童軍活動與訓練研討會.md "wikilink")、第19屆[蘇格蘭國際小隊露營](../Page/蘇格蘭國際小隊露營.md "wikilink")、[日本童軍總會之邀請計劃](../Page/日本童軍總會.md "wikilink")、[日本深資童軍露營](../Page/日本深資童軍露營.md "wikilink")、[韓國國際樂行童軍大會](../Page/韓國國際樂行童軍大會.md "wikilink")。

1985年，[周湛燊被委任為](../Page/周湛燊.md "wikilink")[香港總監](../Page/香港總監.md "wikilink")。同年，香港童軍代表團參加第30屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")、[梅富根100週年紀念](../Page/梅富根100週年紀念.md "wikilink")、[新加坡青年論壇](../Page/新加坡青年論壇.md "wikilink")、[85年美國大露營](../Page/85年美國大露營.md "wikilink")、[韓國國際青年年童軍露營](../Page/韓國國際青年年童軍露營.md "wikilink")、[日本童軍總會邀請計劃](../Page/日本童軍總會.md "wikilink")、[亞太區國際青年年實務工作營](../Page/亞太區國際青年年實務工作營.md "wikilink")、第9屆[亞太區童軍大露營](../Page/亞太區童軍大露營.md "wikilink")、[85'穗港澳青年聯歡節](../Page/85'穗港澳青年聯歡節.md "wikilink")；香港童軍總會獲[世界童軍組織頒發](../Page/世界童軍組織.md "wikilink")「[國際青年年傑出活動](../Page/國際青年年.md "wikilink")」獎牌。

1986年，[香港總監](../Page/香港總監.md "wikilink")[周湛燊被選任為](../Page/周湛燊.md "wikilink")[亞太區童軍委員會委員](../Page/亞太區童軍委員會.md "wikilink")，並與[黃楊子潔一同獲](../Page/黃楊子潔.md "wikilink")[亞太區童軍總會頒授](../Page/亞太區童軍總會.md "wikilink")[三十週年紀念章](../Page/三十週年紀念章.md "wikilink")。同年，香港童軍代表團參加第15屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、[奧地利童軍大露營](../Page/奧地利童軍大露營.md "wikilink")、[日本童軍總會邀請計劃](../Page/日本童軍總會.md "wikilink")、第7屆[韓國童軍大露營](../Page/韓國童軍大露營.md "wikilink")、第9屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")；香港童軍總會在[柴灣公園](../Page/柴灣公園.md "wikilink")(即舊[柴灣童軍營地](../Page/柴灣童軍營地.md "wikilink"))豎立牌匾以紀念香港童軍75週年。

1987年，[香港總督](../Page/香港總督.md "wikilink")[衛奕信爵士出任](../Page/衛奕信.md "wikilink")[香港童軍總領袖](../Page/香港童軍總領袖.md "wikilink")。同年，香港童軍總會修訂會章、出版《[香港童軍七十五](../Page/香港童軍七十五.md "wikilink")》；[新界地域總部](../Page/香港童軍總會新界地域.md "wikilink")[鄧肇堅男女童軍中心落成啟用](../Page/鄧肇堅男女童軍中心.md "wikilink")；香港童軍代表團參加第16屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")、第1屆[尼泊爾童軍大露營](../Page/尼泊爾童軍大露營.md "wikilink")、[日本童軍總會邀請計劃](../Page/日本童軍總會.md "wikilink")、第5屆[日本傷健大露營](../Page/日本傷健大露營.md "wikilink")、[六一國際兒童節](../Page/六一國際兒童節.md "wikilink")。

1988年，[許宗盛被委任為](../Page/許宗盛.md "wikilink")[亞太區童軍社區發展小組委員會副主席](../Page/亞太區童軍社區發展小組委員會.md "wikilink")。同年，香港童軍代表團參加第31屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")、第5屆[曼谷市大露營](../Page/曼谷市大露營.md "wikilink")、第21屆[蘇格蘭小隊露營](../Page/蘇格蘭小隊露營.md "wikilink")、[日本深資童軍大露營](../Page/日本深資童軍大露營.md "wikilink")、訪問[廣州](../Page/廣州.md "wikilink")[桂林](../Page/桂林.md "wikilink")；香港童軍總會出版新《政策、組織及規條》綜合版本。

1989年，[許招賢被委任為](../Page/許招賢.md "wikilink")[亞太區童軍社區發展小組委員會委員](../Page/亞太區童軍社區發展小組委員會.md "wikilink")。同年，香港童軍代表團參加第16屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、[日本童軍總會邀請計劃](../Page/日本童軍總會.md "wikilink")、第12屆[泰國童軍大露營](../Page/泰國童軍大露營.md "wikilink")；[白沙灣譚華正海上活動中心行政大樓啟用](../Page/白沙灣譚華正海上活動中心.md "wikilink")。

1990年，[香港總監](../Page/香港總監.md "wikilink")[周湛燊獲](../Page/周湛燊.md "wikilink")[世界童軍組織頒授](../Page/世界童軍組織.md "wikilink")[銅狼勳章](../Page/銅狼勳章.md "wikilink")。同年，香港童軍代表團出席第32屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")、[穗港青年大露營](../Page/穗港青年大露營.md "wikilink")、第8屆[韓國童軍大露營](../Page/韓國童軍大露營.md "wikilink")、第10屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")、[日本童軍總會邀請計劃](../Page/日本童軍總會.md "wikilink")、第22屆[蘇格蘭國際小隊露營](../Page/蘇格蘭國際小隊露營.md "wikilink")、[馬來西亞國際大露營](../Page/馬來西亞國際大露營.md "wikilink")。

1991年為香港童軍80週年慶典，香港童軍總會相繼舉行了多項多姿多采的紀念活動。同年舉行新總部大廈「[香港童軍中心](../Page/香港童軍中心.md "wikilink")」的奠基典禮；[盧偉誠被委任為香港訓練總監](../Page/盧偉誠.md "wikilink")；香港童軍總會成立[活動與訓練檢討委員會](../Page/活動與訓練檢討委員會.md "wikilink")；香港童軍代表團參加第17屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")、[日本童軍總會邀請計劃](../Page/日本童軍總會.md "wikilink")、第13屆[泰國童軍大露營](../Page/泰國童軍大露營.md "wikilink")、[美國童軍夏令營職員計劃](../Page/美國童軍夏令營職員計劃.md "wikilink")、[亞太區童軍社區發展檢討工作坊](../Page/亞太區童軍社區發展檢討工作坊.md "wikilink")、[亞太區童軍總幹事高峰會議](../Page/亞太區童軍總幹事高峰會議.md "wikilink")。

1992年，[香港總督](../Page/香港總督.md "wikilink")[彭定康出任](../Page/彭定康.md "wikilink")[香港童軍總領袖](../Page/香港童軍總領袖.md "wikilink")、[盧偉誠被委任為](../Page/盧偉誠.md "wikilink")[亞太區童軍訓練小組委員會副主席](../Page/亞太區童軍訓練小組委員會.md "wikilink")、[梁國生被委任為](../Page/梁國生.md "wikilink")[亞太區童軍價值觀小組委員會成員](../Page/亞太區童軍價值觀小組委員會.md "wikilink")、[譚超雄被委任為](../Page/譚超雄.md "wikilink")[亞太區童軍策略研究工作小組委員](../Page/亞太區童軍策略研究工作小組.md "wikilink")。同年，香港童軍總會獲[世界童軍協會](../Page/世界童軍協會.md "wikilink")—[亞太區指定為](../Page/亞太區.md "wikilink")[亞太區童軍基金的信託管理人](../Page/亞太區童軍基金.md "wikilink")；香港童軍代表團出席第17屆[亞太區童軍會議香港代表團分別參加](../Page/亞太區童軍會議.md "wikilink")[韶關](../Page/韶關.md "wikilink")、[日本](../Page/日本.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[美國舉行之大露營](../Page/美國.md "wikilink")、交換計劃、研討會等海外活動、出席[亞太區童軍總幹事高峰會議](../Page/亞太區童軍總幹事高峰會議.md "wikilink")。

1993年1月1日，[盧觀榮被委任為](../Page/盧觀榮.md "wikilink")[署理香港總監](../Page/署理香港總監.md "wikilink")。同年7月1日，[周湛燊被委任為](../Page/周湛燊.md "wikilink")[香港總監](../Page/香港總監.md "wikilink")。同年，[香港總監](../Page/香港總監.md "wikilink")[周湛燊獲選為](../Page/周湛燊.md "wikilink")[世界童軍委員會委員](../Page/世界童軍委員會.md "wikilink")、[盧偉誠被委任為](../Page/盧偉誠.md "wikilink")[世界童軍人力資源委員會委員](../Page/世界童軍人力資源委員會.md "wikilink")、[盧觀榮被委任為](../Page/盧觀榮.md "wikilink")[亞太區童軍財務小組委員會司庫](../Page/亞太區童軍財務小組委員會.md "wikilink")；香港童軍代表團參加第33屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")、第14屆[泰國童軍大露營](../Page/泰國童軍大露營.md "wikilink")、[美國童軍大露營](../Page/美國童軍大露營.md "wikilink")、[日本童軍總會舉行之](../Page/日本童軍總會舉行.md "wikilink")[世界兒童大會及邀請計劃](../Page/世界兒童大會.md "wikilink")、[韶關香港青少年夏令營](../Page/韶關香港青少年夏令營.md "wikilink")。同年1月30日至1月31日，香港童軍總會在[新界](../Page/新界.md "wikilink")[沙田](../Page/沙田.md "wikilink")[香港體育中心舉行](../Page/香港體育中心.md "wikilink")[領袖訓練人員會議](../Page/領袖訓練人員會議.md "wikilink")。同年12月5日，香港童軍總會在[香港島](../Page/香港島.md "wikilink")[黃竹坑警察訓練學校舉行](../Page/黃竹坑警察訓練學校.md "wikilink")[全港區總監研討會](../Page/全港區總監研討會.md "wikilink")。

1994年6月，香港童軍總會連同[九龍地域](../Page/香港童軍總會九龍地域.md "wikilink")、[東九龍地域由](../Page/香港童軍總會東九龍地域.md "wikilink")[摩士大廈遷往](../Page/摩士大廈.md "wikilink")[香港童軍中心](../Page/香港童軍中心.md "wikilink")，[英國童軍總會會長](../Page/英國童軍總會.md "wikilink")[根德公爵探訪新總部大廈](../Page/根德.md "wikilink")。同年，[許宗盛被委任為](../Page/許宗盛.md "wikilink")[亞太區童軍基金委員會委員](../Page/亞太區童軍基金委員會.md "wikilink")；香港童軍總會舉行第1屆[亞太區童軍活動與訓練總監研討會](../Page/亞太區童軍活動與訓練總監研討會.md "wikilink")；香港童軍代表團參加第11屆[日本全國童軍大露營](../Page/日本全國童軍大露營.md "wikilink")、第9屆[韓國童軍大露營](../Page/韓國童軍大露營.md "wikilink")。

1995年，[國際總監](../Page/國際總監.md "wikilink")[許宗盛獲委任為](../Page/許宗盛.md "wikilink")[亞太區童軍委員會委員](../Page/亞太區童軍委員會委員.md "wikilink")、[盧偉誠獲委任為](../Page/盧偉誠.md "wikilink")[亞太區童軍人力資源小組委員會主席](../Page/亞太區童軍人力資源小組委員會.md "wikilink")、[葉建生獲委任為](../Page/葉建生.md "wikilink")[亞太區童軍青少年活動小組委員會委員](../Page/亞太區童軍青少年活動小組委員會.md "wikilink")、[王健明獲委任為](../Page/王健明.md "wikilink")[亞太區童軍公共關係小組委員會委員](../Page/亞太區童軍公共關係小組委員會.md "wikilink")。同年，香港童軍代表團出席第18屆[亞太區童軍會議](../Page/亞太區童軍會議.md "wikilink")、第18屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")、[95韓國童軍露營](../Page/95韓國童軍露營.md "wikilink")、[瑞典童軍露營](../Page/瑞典童軍露營.md "wikilink")；[貝登堡勳爵家族成員](../Page/貝登堡.md "wikilink")[米高及](../Page/米高.md "wikilink")[溫蒂訪港](../Page/溫蒂.md "wikilink")，為懸掛在[香港童軍中心地下大堂中央的](../Page/香港童軍中心.md "wikilink")[貝登堡勳爵肖像揭幕](../Page/貝登堡.md "wikilink")；[童軍知友社](../Page/童軍知友社.md "wikilink")[延續教育中心啟用](../Page/延續教育中心.md "wikilink")。

1996年2月5日至2月13日，第40屆[亞太區童軍專業領袖基本管理訓練班在](../Page/亞太區童軍專業領袖基本管理訓練班.md "wikilink")[香港島](../Page/香港島.md "wikilink")[大潭童軍中心舉行](../Page/大潭童軍中心.md "wikilink")。同年2月10日至2月11日，[亞太區童軍人力資源委員會暨成人領袖訓練工作小組聯合會議在](../Page/亞太區童軍人力資源委員會暨成人領袖訓練工作小組聯合會議.md "wikilink")[香港舉行](../Page/香港.md "wikilink")。同年12月28日，香港童軍總會在[香港島](../Page/香港島.md "wikilink")[黃竹坑警察訓練學校舉行](../Page/黃竹坑警察訓練學校.md "wikilink")[周湛燊總監告別會操](../Page/周湛燊總監告別會操.md "wikilink")。同年，香港童軍代表團參加第34屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")；香港童軍總會與[永隆銀行合辦](../Page/永隆銀行.md "wikilink")[童軍信用卡](../Page/童軍信用卡.md "wikilink")。

1997年1月1日，[許招賢被委任為](../Page/許招賢.md "wikilink")[香港總監](../Page/香港總監.md "wikilink")。同年4月4日至4月6日，香港童軍代表團出席首屆[亞太區童軍人力資源管理研討會](../Page/亞太區童軍人力資源管理研討會.md "wikilink")。同年9月4日，[訓練署分拆為](../Page/訓練署.md "wikilink")[訓練署及](../Page/訓練署.md "wikilink")[青少年活動署](../Page/青少年活動署.md "wikilink")、[康寶森被委任為](../Page/康寶森.md "wikilink")[香港訓練總監](../Page/香港訓練總監.md "wikilink")、[陳肖齡被委任為首位](../Page/陳肖齡.md "wikilink")[香港青少年活動總監](../Page/香港青少年活動總監.md "wikilink")。

1998年7月19日至7月24日，[亞太區童軍領袖訓練主任訓練班在](../Page/亞太區童軍領袖訓練主任訓練班.md "wikilink")[新界](../Page/新界.md "wikilink")[烏溪沙青年新村舉行](../Page/烏溪沙青年新村.md "wikilink")。同年7月22日至7月24日，[第二屆亞太區青年論壇在](../Page/第二屆亞太區青年論壇.md "wikilink")[突破青年村舉行](../Page/突破青年村.md "wikilink")。同年7月26日至7月31日，第19屆[亞太區童軍會議在](../Page/亞太區童軍會議.md "wikilink")[香港童軍中心舉行](../Page/香港童軍中心.md "wikilink")。同年12月27日至1999年1月6日，香港童軍代表團參加第19屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")。

1999年，香港童軍代表團參加第35屆[世界童軍會議](../Page/世界童軍會議.md "wikilink")。

1999年12月28日至2000年1月2日於[西貢東郊野公園之](../Page/西貢東郊野公園.md "wikilink")[灣仔半島舉行](../Page/灣仔_\(西貢\).md "wikilink")「[香港童軍跨世紀大露營](../Page/香港童軍跨世紀大露營.md "wikilink")」，主題為「邁向新紀元，同享大自然」，參加人數達3,350人。在營期的其中一天(即1999年12月30日)，參加者更於5分鐘內造出世界最長之[平結鍊](../Page/平結.md "wikilink")，當中共有4,500個平結，全長6,858米。

## 2000年代

2000年3月1日，[童軍技能評審局成立](../Page/童軍技能評審局.md "wikilink")。同年4月1日，[新界東地域成立](../Page/香港童軍總會新界東地域.md "wikilink")、[訓練署修訂之](../Page/訓練署.md "wikilink")[領袖木章訓練系統開始執行](../Page/領袖木章訓練系統.md "wikilink")。同年8月29日，香港童軍總會成立[救護服務隊](../Page/救護服務隊.md "wikilink")。同年，香港童軍總會設立[訓練中心](../Page/訓練中心.md "wikilink")、派出專責小組協助[柬埔寨重新組織](../Page/柬埔寨.md "wikilink")[童軍運動](../Page/童軍運動.md "wikilink")、舉辦[優異旅團獎勵計劃](../Page/優異旅團獎勵計劃.md "wikilink")、確立「抱負、使名和價值觀」發展方向。

2001年1月12日，香港童軍總會採用新修訂之[童軍誓詞](../Page/童軍誓詞.md "wikilink")。同年12月23日至12月26日，香港童軍總會於[新界](../Page/新界.md "wikilink")[西貢](../Page/西貢.md "wikilink")[灣仔半島舉行](../Page/灣仔半島.md "wikilink")[香港童軍運動九十週年紀念大露營](../Page/香港童軍運動九十週年紀念大露營.md "wikilink")。同年，香港童軍總會成立[內地聯絡辦公室](../Page/內地聯絡辦公室.md "wikilink")。

2002年2月，香港童軍總會成立[策劃署](../Page/策劃署.md "wikilink")。同年4月1日，將軍澳區成立。同年4月26日至4月28日，第45屆[貝登堡會士會盟在](../Page/貝登堡會士會盟.md "wikilink")[香港舉行](../Page/香港.md "wikilink")，[世界童軍基金會名譽會長](../Page/世界童軍基金會.md "wikilink")[卡爾十六世·古斯塔夫](../Page/卡爾十六世·古斯塔夫.md "wikilink")([瑞典國王](../Page/瑞典.md "wikilink"))親臨出席。同年6月1日，大埔區分拆為大埔南區及大埔北區、北區分拆為雙魚區及壁峰區。同年7月15日至7月19日，香港童軍代表團出席[世界童軍大會](../Page/世界童軍大會.md "wikilink")。同年，香港童軍總會參與[香港政府](../Page/香港政府.md "wikilink")[教育統籌局的](../Page/教育統籌局.md "wikilink")[制服團體計劃](../Page/制服團體計劃.md "wikilink")、成立[國際及內聯署](../Page/國際及內聯署.md "wikilink")；[副香港總監(支援)](../Page/副香港總監\(支援\).md "wikilink")[許宗盛被推選為](../Page/許宗盛.md "wikilink")[世界童軍委員會委員](../Page/世界童軍委員會.md "wikilink")。同年12月28日至2003年1月8日，香港童軍代表團出席第20屆[世界童軍大露營](../Page/世界童軍大露營.md "wikilink")。

2003年[非典型肺炎在](../Page/非典型肺炎.md "wikilink")[港爆發](../Page/香港.md "wikilink")，加強保障個人及公共衛生便成為抗禦傳染病蔓延的首要任務。為支持[香港政府推行](../Page/香港政府.md "wikilink")「[全港每月清潔大行動](../Page/全港每月清潔大行動.md "wikilink")」，香港童軍總會特別發動「[香港衛生先鋒抗疫大行動](../Page/香港衛生先鋒抗疫大行動.md "wikilink")」，以喚起全[港市民對清潔](../Page/香港.md "wikilink")[香港的關注](../Page/香港.md "wikilink")，並於2003年6月15日舉行啟動禮。這些行動得到了30,000多名童軍成員及其親友的響應。

2003年4月1日，沙田南區及沙田北區重組為沙田東區、沙田南區、沙田西區及沙田北區。同年7月1日，香港童軍總會重新規劃旺角區、深水埗東區、深水埗西區及油尖區的區界，並成立\[深旺區。同年11月，[香港童軍總會通善演藝中心啟用](../Page/香港童軍總會通善演藝中心.md "wikilink")。同年，香港童軍總會成立[區/旅團童軍發展資助計劃](../Page/區/旅團童軍發展資助計劃.md "wikilink")。

2004年1月1日，[鮑紹雄被委任為](../Page/鮑紹雄.md "wikilink")[香港總監](../Page/香港總監.md "wikilink")。同年1月24日至1月27日，香港童軍總會與[深圳青年聯合會及](../Page/深圳青年聯合會.md "wikilink")[深圳市少先隊工作委員會在](../Page/深圳市少先隊工作委員會.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")[觀蘭湖高爾夫球會鄉村俱樂部合辦](../Page/觀蘭湖高爾夫球會鄉村俱樂部.md "wikilink")[深港青少年交流營](../Page/深港青少年交流營.md "wikilink")。

## 参考文献

[Category:香港童軍](../Category/香港童軍.md "wikilink")

1.

2.

3.  香港童軍總會領袖初級訓練班(資料手冊)第一章 童軍運動-香港童軍運動簡史(06/05)

4.
5.

6.
7.
8.

9.

10.