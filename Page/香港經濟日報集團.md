**香港經濟日報集團有限公司**（**，），是一間香港上市公司，在2005年在[開曼群島註冊](../Page/開曼群島.md "wikilink")\[1\]。集團的前身，是在1986年創立\[2\]的同名香港公司**香港經濟日報（集團）有限公司**（又名**拓利有限公司**、**港經日報有限公司**及**經濟(集團)有限公司'''）\[3\]，並於1988年開始出版《[香港經濟日報](../Page/香港經濟日報.md "wikilink")》。創辦人為[馮紹波](../Page/馮紹波.md "wikilink")、[麥華章](../Page/麥華章.md "wikilink")，以及[石鏡泉](../Page/石鏡泉.md "wikilink")。集團總部位於[香港](../Page/香港.md "wikilink")[北角](../Page/北角.md "wikilink")[渣華道](../Page/渣華道.md "wikilink")321號柯達大廈二期6樓。

## 業務

集團主要承印及銷售刊物，包括《[香港經濟日報](../Page/香港經濟日報.md "wikilink")》、《投資理財周刊》、《置業家居》、《[晴報](../Page/晴報.md "wikilink")》、《[e-zone](../Page/e-zone.md "wikilink")》，《U周刊》以及《iMoney
智富雜誌》。而財經通訊社、資訊及軟件業務也是集團經營範圍，包括[經濟通](../Page/經濟通.md "wikilink")、交易通、環富通及經濟地產庫。其他業務包括招聘廣告及培訓業務，包括[經濟商學院](http://www.etbc.com.hk)及CTgoodjobs；另外
優質生活網站包括樂本．健及U Lifestyle及其成員包括 U Travel、港生活、U Beauty、U Food、U Blog
等。\[4\]

## 歷史

1993年，[英屬處女群島公司Golden](../Page/英屬處女群島.md "wikilink")
Rooster成立，成為香港經濟日報集團的控股股東。Golden
Rooster的股東為馮紹波、[朱裕倫及](../Page/朱裕倫.md "wikilink")[梁家齊](../Page/梁家齊.md "wikilink")\[5\]。2004年香港經濟日報集團向集團控股股東Golden
Rooster、集團董事\[6\]麥炳良、[陳早標](../Page/陳早標.md "wikilink")、石鏡泉及[史秀美收購其所持的全部ET](../Page/史秀美.md "wikilink")
Net (BVI)股權，作價10萬美元\[7\]；ET Net
(BVI)是經濟通有限公司的母公司\[8\]，經營《[經濟通](../Page/經濟通.md "wikilink")》網頁；當時ET
Net (BVI)或經濟通的小股東為Network Holdings及Top Media兩間公司\[9\]。

2005年，集團[首次公開發行的招股價定為](../Page/首次公開發行.md "wikilink")1.43至1.7港元之間，發行新股1.04億，股其中9,360萬股用作配售\[10\]，集資額為約1.8億港元\[11\]
。在收集所有認購後，最終定價為1.7港元\[12\]，並於2005年8月3日在[香港交易所主板上市](../Page/香港交易所.md "wikilink")\[13\]，並於首目錄得35%升幅\[14\]。

集團於2011年10月斥資2.15億元收購印刷公司泰業(香港)，強化印刷部門。

## 參考

## 連結

  -
[CATEGORY:香港報紙](../Page/CATEGORY:香港報紙.md "wikilink")
[CATEGORY:香港經濟日報](../Page/CATEGORY:香港經濟日報.md "wikilink")
[CATEGORY:1988年成立的公司](../Page/CATEGORY:1988年成立的公司.md "wikilink")

1.
2.

3.  香港公司註冊處

4.  [香港經濟日報集團有限公司簡介](http://www.hketgroup.com/hketgroup/abt_overview_c.html)


5.

6.

7.
8.
9.
10. [香港經濟日報集團有限公司提供招股資料](http://www.hkexnews.hk/listedco/listconews/SEHK/2005/0722/00423/F102_c.pdf)

11.

12.
13.

14.