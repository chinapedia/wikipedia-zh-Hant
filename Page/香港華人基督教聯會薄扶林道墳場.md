[Hong_Kong_Christian_Churches_Union_Po_Fu_Lam_Road_Cemetery_1.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Christian_Churches_Union_Po_Fu_Lam_Road_Cemetery_1.jpg "fig:Hong_Kong_Christian_Churches_Union_Po_Fu_Lam_Road_Cemetery_1.jpg")

**香港華人基督教聯會薄扶林道墳場**，簡稱**薄扶林華人基督教墳場**，是[香港華人基督教聯會轄下的一個](../Page/香港華人基督教聯會.md "wikilink")[墳場](../Page/墳場.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[薄扶林道](../Page/薄扶林道.md "wikilink")119號至125號，範圍約為[域多利道近](../Page/域多利道.md "wikilink")[沙灣](../Page/沙灣_\(香港\).md "wikilink")[東華義莊以東](../Page/東華義莊.md "wikilink")，薄扶林道以西的山坡。

墳場內葬有多位名人，包括昔日香港四大百貨公司（[先施百貨](../Page/先施百貨.md "wikilink")、[永安百貨](../Page/永安百貨.md "wikilink")、[大新百貨](../Page/大新百貨.md "wikilink")、[新新百貨](../Page/新新百貨.md "wikilink")）的家族、著名作家[許地山](../Page/許地山.md "wikilink")、革命家[謝纘泰及音樂家](../Page/謝纘泰.md "wikilink")[何大傻等](../Page/何大傻.md "wikilink")。

## 名人墓碑

  - [尹維清](../Page/尹維清.md "wikilink")（1805年－1885年）：尹文楷（1870－1927）之父
  - [區鳳墀](../Page/區鳳墀.md "wikilink")（1847年－1914年）孫中山中文老師
  - [何玉泉](../Page/何玉泉.md "wikilink")（1805年－1885年）
  - [王元深](../Page/王元深.md "wikilink")（1817年—1914年），名常福，字開胜。中國基督教新教傳教士，信義宗禮賢會奠基者，中國第一本教會史撰稿人，王寵益祖父
  - [王謙如](../Page/王謙如.md "wikilink")（1847年－1907年），王元深次子
  - [王澤民](../Page/王澤民.md "wikilink")，王謙如長子
  - [王煜初](../Page/王煜初.md "wikilink")（1843年－1902年），王寵益父親
  - [王寵益](../Page/王寵益.md "wikilink")（1888年9月15日－1930年12月15日），香港的第一位華人教授
  - [曾廣珊](../Page/曾廣珊.md "wikilink")（1871年—1949年6月21日），曾國藩孫女，曾紀鴻獨生女，前中華民國國防部長俞大維之母，葬在戊段五級A3706號
  - [許地山](../Page/許地山.md "wikilink")（1894年2月3日－1941年8月4日）
  - [許淇安](../Page/許淇安.md "wikilink")（1943年10月10日－2009年5月3日），前[香港警務處處長](../Page/香港警務處處長.md "wikilink")\[1\]
  - [謝纘泰](../Page/謝纘泰.md "wikilink")（1872年5月16日－1938年4月4日）
  - [鄭士良](../Page/鄭士良.md "wikilink")（1863年2月16日－1901年8月27日），三洲田起義的首領
  - [何大傻](../Page/何大傻.md "wikilink")（何澤民）（1896年－1957年6月15日）
  - [李煜堂](../Page/李煜堂.md "wikilink")（1851年8月23日－1936年1月1日）
  - [陳潔貞](../Page/陳潔貞.md "wikilink")，李煜堂原配夫人
  - [周修平](../Page/周修平.md "wikilink")，李煜堂妾侍
  - [李自重](../Page/李自重.md "wikilink")（1883年－1971年），李煜堂長子
  - [余斌臣](../Page/余斌臣.md "wikilink")（1883年－1971年）
  - [陳子橋](../Page/陳子橋.md "wikilink")，陳少白的父親、嶺南大學前身格致書院的倡辦人
  - [李學柏](../Page/李學柏.md "wikilink")（1848年9月11日－1935年1月7日），李樹芬的父親
  - [李樹芬](../Page/李樹芬.md "wikilink")（1887年－1966年），孫中山的醫學顧問
  - [孫婉](../Page/孫婉.md "wikilink")（1896年11月12日－1979年6月3日），孫中山次女
  - [戴恩赛](../Page/戴恩赛.md "wikilink")（1892年5月6日－1955年1月16日），孫中山女婿
  - [左斗山](../Page/左斗山.md "wikilink")（1830年－1911年）
  - [左吉帆](../Page/左吉帆.md "wikilink")，左斗山兒子
  - [王慧珍](../Page/王慧珍.md "wikilink")，王煜初第二女，左吉帆夫人
  - [胡爾桂](../Page/胡爾桂.md "wikilink")
  - [胡爾楝](../Page/胡爾楝.md "wikilink")
  - [胡爾楷](../Page/胡爾楷.md "wikilink")（？－1898年）
  - [胡王麗珊](../Page/胡王麗珊.md "wikilink")（胡爾楷夫人）
  - [胡素貞](../Page/胡素貞.md "wikilink")（1890年－1979年），胡爾楷之女，香港第一位女博士。
  - [胡惠德](../Page/胡惠德.md "wikilink")（1887年10月23日－1964年3月3日），胡爾楷長子
  - [尹維清](../Page/尹維清.md "wikilink")（又名華川，？－1877年），尹文楷之父
  - [尹文楷](../Page/尹文楷.md "wikilink")（1870年－1927年），第一个取得香港行医执照的中国医学堂毕业生
  - [伍于簪](../Page/伍于簪.md "wikilink")（1873年－1934年）
  - [伍于簫](../Page/伍于簫.md "wikilink")
  - [容覲彤](../Page/容覲彤.md "wikilink")（1876年—1933年），容閎長子，容永道繼父
  - [馬在明](../Page/馬在明.md "wikilink")（1822年－1916年），馬應彪之父
  - [霍静山](../Page/霍静山.md "wikilink")（1851年－1918年），馬應彪外父
  - [馬應彪](../Page/馬應彪.md "wikilink")（1861年－1944年）
  - [霍慶棠](../Page/霍慶棠.md "wikilink")（1872年－1957年）[香港基督教女青年會創辦人之一](../Page/香港基督教女青年會.md "wikilink")，也是先施和香港第一位女售貨員。
  - [廖薌儂](../Page/廖薌儂.md "wikilink")（1886年－1930年）
  - [黃詠德](../Page/黃詠德.md "wikilink")（1867年－1924年），黃勝之子
  - [王頌剛](../Page/王頌剛.md "wikilink")（？－1941年），1941年香港淪陷時，為救濟貧民糧食，被日軍射殺。
  - [李正高](../Page/李正高.md "wikilink")，香港巴色會第一位客家籍牧師
  - [孫智興](../Page/孫智興.md "wikilink")（1868年－1937年）
  - [林護夫婦合葬墓](../Page/林護.md "wikilink")（1873年－1933年）
  - [杜應坤](../Page/杜應坤.md "wikilink")（1881年－1928年），廖仲愷之妹夫、譚雅士岳父
  - [律敦治夫人](../Page/律敦治.md "wikilink")（1886年8月6日－1974年12月20日）
  - [陳潔玿](../Page/陳潔玿.md "wikilink")（1911年－1938年）
  - [陳任](../Page/陳任.md "wikilink")（1945年－2008年11月1日），香港著名元老級中文唱片騎師\[2\]
  - [郭泉](../Page/郭泉_\(商人\).md "wikilink")（1879年－1957年4月），字鳳輝，永安百貨創辦人之一
  - [郭浩](../Page/郭浩.md "wikilink")（1880年－1946年），永安百貨創辦人之一
  - [郭順](../Page/郭順.md "wikilink")（1884年－1976年4月16日）
  - [郭琳珊](../Page/郭琳珊.md "wikilink")（1916年9月2日－1983年4月27日）
  - [郭琳褒](../Page/郭琳褒.md "wikilink")
  - [郭琳弼](../Page/郭琳弼.md "wikilink")
  - [郭琳驤](../Page/郭琳驤.md "wikilink")（1914年10月6日－1971年12月21日）
  - [李文華](../Page/李文華.md "wikilink")，商人，佐敦恆豐酒店的創辦人
  - [蔡潤之](../Page/蔡潤之.md "wikilink")（184年－1926年）蔡昌、蔡興及蔡聰之父
  - [蔡興](../Page/蔡興.md "wikilink")（1869年－？），大新百貨（香港）的創辦人
  - [蔡昌](../Page/蔡昌.md "wikilink")（1877年11月2日－1951年7月13日），大新百貨創辦人
  - [蔡聰](../Page/蔡聰.md "wikilink")
  - [蔡東生](../Page/蔡東生.md "wikilink")（1902年－1987年），蔡興長子
  - [陳子褒](../Page/陳子褒.md "wikilink")（1862年－1922年7月4日）
  - [林子豐](../Page/林子豐.md "wikilink")（1892年11月6日－1971年4月17日），中信嘉華銀行及浸會大學的創辦人
  - [馬儀英](../Page/馬儀英.md "wikilink")（1909年12月23日－1974年2月7日），九龍真光中學創校校長
  - [石堅](../Page/石堅.md "wikilink")\[3\]（1913年1月1日－2009年6月3日）
  - [陳麗珍](../Page/陳麗珍.md "wikilink")（？－1937年5月13日）
  - [廖新基](../Page/廖新基.md "wikilink")（1897年1月28日－1941年2月22日），崇正總會創辦人之一
  - [張祝齡牧師](../Page/張祝齡.md "wikilink")（1877年－1961年）
  - [簡達才](../Page/簡達才.md "wikilink")
  - [陳榮袞](../Page/陳榮袞.md "wikilink")（1862年3月11日－1922年7月4日）
  - [陸灼文](../Page/陸灼文.md "wikilink")
  - [陆谦受](../Page/陆谦受.md "wikilink")（1904年－1992年）
  - [陸鏡輝](../Page/陸鏡輝.md "wikilink")（1888年2月22日－1979年1月20日）
  - [杜福茂](../Page/杜福茂.md "wikilink")（1824年－1911年）
  - [吳秋湘](../Page/吳秋湘.md "wikilink")（1838年9月28日－1911年1月31日）
  - [吳天保](../Page/吳天保.md "wikilink")（1880年－1941年2月4日）吳秋湘之子
  - [王國璇](../Page/王國璇.md "wikilink")（1888年－1974年）
  - [羅光雲](../Page/羅光雲.md "wikilink")、[羅國瑞](../Page/羅國瑞.md "wikilink")（1861年－？）之父
  - [李子方](../Page/李子方.md "wikilink")（1891年9月21日－1953年9月5日）
  - [黎新祥](../Page/黎新祥.md "wikilink")（1950年9月1日－2010年6月20日），前香港足球教練及足球員\[4\]
  - [曹仁超](../Page/曹仁超.md "wikilink")（1947年－2016年2月21日）

## 參見

  - [香港墳場列表](../Page/香港墳場列表.md "wikilink")

## 參考文獻

## 外部連結

  - [香港華人基督教會墓園部官方網頁](http://www.hkcccu.org.hk/CS/index.html)

[Category:香港墳場](../Category/香港墳場.md "wikilink")
[Category:薄扶林](../Category/薄扶林.md "wikilink")

1.

2.
3.

4.