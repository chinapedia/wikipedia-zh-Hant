**資歷架構**，[香港特別行政區政府](../Page/香港特別行政區政府.md "wikilink")[教育局](../Page/教育局_\(香港\).md "wikilink")（前身為[教育統籌局](../Page/教育統籌局.md "wikilink")）為促進終身學習所發展出的一套學術資格認可機制。架構共分為7層，由最基本的第一層（[舊制中五程度](../Page/香港中學會考.md "wikilink")）至最高的第七層（[博士](../Page/博士.md "wikilink")）。每層或每級別皆訂有通用指標，具體說明同級資歷的共同特徵。\[1\]

## 學歷級別

  - 第一級：[基礎證書](../Page/基礎證書.md "wikilink")、[證書](../Page/畢業證書.md "wikilink")（[舊制中五程度](../Page/香港中學會考.md "wikilink")）
  - 第二級：[基礎證書](../Page/基礎證書.md "wikilink")、[證書](../Page/證書.md "wikilink")[文憑](../Page/文憑.md "wikilink")（[新制中六程度](../Page/香港中學文憑考試.md "wikilink")）
  - 第三級：[證書](../Page/證書.md "wikilink")、[文憑](../Page/文憑.md "wikilink")（[舊制中七程度](../Page/香港高級程度會考.md "wikilink")）
  - 第四級：[高等文憑](../Page/高等文憑.md "wikilink")、[高級文憑](../Page/高級文憑.md "wikilink")、[副學士](../Page/副學士.md "wikilink")、[專業文憑](../Page/專業文憑.md "wikilink")、[文憑](../Page/文憑.md "wikilink")、[高等證書](../Page/高等證書.md "wikilink")、[高級證書](../Page/高級證書.md "wikilink")、[專業證書](../Page/專業證書.md "wikilink")、[證書](../Page/證書.md "wikilink")
  - 第五級：[學士](../Page/學士.md "wikilink")、[高等文憑](../Page/高等文憑.md "wikilink")、[專業文憑](../Page/專業文憑.md "wikilink")、[文憑](../Page/文憑.md "wikilink")、[高等證書](../Page/高等證書.md "wikilink")、[專業證書](../Page/專業證書.md "wikilink")、[證書](../Page/證書.md "wikilink")
  - 第六級：[碩士](../Page/碩士.md "wikilink")、[深造文憑](../Page/深造文憑.md "wikilink")、[高等文憑](../Page/高等文憑.md "wikilink")、[專業文憑](../Page/專業文憑.md "wikilink")、[文憑](../Page/文憑.md "wikilink")、[深造證書](../Page/深造證書.md "wikilink")[高等證書](../Page/高等證書.md "wikilink")、[專業證書](../Page/專業證書.md "wikilink")、[證書](../Page/證書.md "wikilink")
  - 第七級：[博士](../Page/博士.md "wikilink")

## 批評

資歷架構被工會人士批評為忽視實際情況，指某部分行業較為重視實際工作經驗，而資歷架構又未能提供證書，擔心就算經驗豐富的僱員，都要減薪或遭解僱欠缺實質配套。\[2\]

調查指出有九成半中學生不清楚資歷架構的推行詳情。同時負責調查的[區議員指](../Page/區議員.md "wikilink")，現今私營或公營進修課程學費偏高阻礙全民[進修](../Page/進修.md "wikilink")。\[3\]\[4\]

## 參見

  - [職業](../Page/職業.md "wikilink")

## 參考

<div class="references-column">

<references/>

</div>

## 外部連結

  - [資歷架構](http://www.hkqf.gov.hk/)
  - [報看天下﹕資歷架構提升專業](http://edu.sina.com.hk/cgi-bin/nw/show.cgi/3/1/4/7222/1.html)

[Category:香港教育](../Category/香港教育.md "wikilink")
[Category:香港專上教育](../Category/香港專上教育.md "wikilink")

1.  [香港資歷架構級別](https://www.am730.com.hk/news/特刊/升學及持續進修課程特輯善用資歷名冊-選擇深造課程-117935)
2.
3.
4.