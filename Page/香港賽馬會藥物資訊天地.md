[Drag_info_Centre_interior_2017.jpg](https://zh.wikipedia.org/wiki/File:Drag_info_Centre_interior_2017.jpg "fig:Drag_info_Centre_interior_2017.jpg")
[Drag_info_Centre_upper_2017.jpg](https://zh.wikipedia.org/wiki/File:Drag_info_Centre_upper_2017.jpg "fig:Drag_info_Centre_upper_2017.jpg")
[Drag_info_Centre_Resources_Centre_2017.jpg](https://zh.wikipedia.org/wiki/File:Drag_info_Centre_Resources_Centre_2017.jpg "fig:Drag_info_Centre_Resources_Centre_2017.jpg")
**香港賽馬會藥物資訊天地**，簡稱**藥物資訊天地**，是[香港第一個以](../Page/香港.md "wikilink")[藥物教育為主題的永久展覽館](../Page/藥物.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[金鐘的](../Page/金鐘.md "wikilink")[金鐘道政府合署低座頂層](../Page/金鐘道政府合署.md "wikilink")，2004年6月開幕。展覽館由[禁毒常務委員會倡議興建](../Page/禁毒常務委員會.md "wikilink")，得到[香港賽馬會資助](../Page/香港賽馬會.md "wikilink")5,058萬[港元興建](../Page/港元.md "wikilink")，並由[香港特別行政區政府](../Page/香港特別行政區政府.md "wikilink")[保安局](../Page/保安局.md "wikilink")[禁毒處負責管理](../Page/禁毒處.md "wikilink")。

雖然該館名為「藥物資訊天地」，實際上提供的是反[濫用藥物及物質的資訊](../Page/物質濫用.md "wikilink")，而不是一般醫藥資訊。

## 展覽內容

香港賽馬會藥物資訊天地共分兩層，佔地900[平方米](../Page/平方米.md "wikilink")。第一層是主要展覽場地，主題是人物、藥物及環境；第二層設有互動影院、[課室](../Page/課室.md "wikilink")、資訊站、[圖書館及專題展覽區](../Page/圖書館.md "wikilink")。

## 開放時間

  - 星期一至六上午10時至下午6時開館，免費入場
  - 星期日及公眾假期休館

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{荃灣綫色彩}}">█</font><a href="../Page/荃灣綫.md" title="wikilink">荃灣綫</a>、<font color="{{港島綫色彩}}">█</font><a href="../Page/港島綫.md" title="wikilink">港島綫</a>、<font color="{{南港島綫色彩}}">█</font><a href="../Page/南港島綫.md" title="wikilink">南港島綫</a>：<a href="../Page/金鐘站.md" title="wikilink">金鐘站C</a>1出口</li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 外部連結

  - [香港賽馬會藥物資訊天地](http://www.nd.gov.hk/tc/druginfocentre.htm)

[Category:香港博物館](../Category/香港博物館.md "wikilink")
[Category:金鐘](../Category/金鐘.md "wikilink")