**香港足球聯賽**是[亞洲最歷史悠久的職業聯賽之一](../Page/亞洲.md "wikilink")。

## 聯賽制度

### 現時制度

  - 職業聯賽\[1\]：

<!-- end list -->

  - [香港超級聯賽](../Page/香港超級聯賽.md "wikilink")

<!-- end list -->

  - 業餘聯賽

<!-- end list -->

  - [香港甲組聯賽](../Page/香港甲組聯賽.md "wikilink")
  - [香港乙組聯賽](../Page/香港乙組聯賽.md "wikilink")
  - [香港丙組聯賽](../Page/香港丙組聯賽.md "wikilink")
  - [香港預備組聯賽](../Page/香港預備組聯賽.md "wikilink")

#### 圖表

**2014–15年**起的聯賽系統如下：*(最後更新為2018–19球季)*

<table style="width:136%;">
<colgroup>
<col style="width: 40%" />
<col style="width: 96%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>級別</strong></p></td>
<td style="text-align: center;"><p><strong>聯賽／組別</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港超級聯賽.md" title="wikilink">香港超級聯賽</a></strong><br />
<strong>10 隊</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港甲組聯賽.md" title="wikilink">香港甲組聯賽</a></strong><br />
<strong>14 隊</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>3</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港乙組聯賽.md" title="wikilink">香港乙組聯賽</a></strong><br />
<strong>14 隊</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>4</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港丙組聯賽.md" title="wikilink">香港丙組聯賽</a></strong><br />
<strong>14 隊</strong></p></td>
</tr>
</tbody>
</table>

### 舊制

<table style="width:136%;">
<colgroup>
<col style="width: 40%" />
<col style="width: 96%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>級別</strong></p></td>
<td style="text-align: center;"><p><strong>聯賽／組別</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港甲組足球聯賽.md" title="wikilink">香港甲組足球聯賽</a></strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港乙組足球聯賽.md" title="wikilink">香港乙組足球聯賽</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>3</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港丙組足球聯賽.md" title="wikilink">香港丙組足球聯賽</a></strong><br />
分為甲及地區兩組 (2012–13球季前)</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>4</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港丁組足球聯賽.md" title="wikilink">香港丁組足球聯賽</a></strong><br />
僅曾短暫實施，為丙組聯賽合併後產生的過渡聯賽</p></td>
</tr>
</tbody>
</table>

## 歷屆聯賽冠軍

### 舊有制度

<div>

  - 1908–1946年

| 球季      | 甲組                                 |
| ------- | ---------------------------------- |
| 1908–09 | The Buffs                          |
| 1909–10 | 炮兵                                 |
| 1910–11 | The Buffs                          |
| 1911–12 | 京士陸軍                               |
| 1912–13 | 炮兵                                 |
| 1913–14 | D.C.L.I.                           |
| 1914–15 | 炮兵                                 |
| 1915–16 | 炮兵                                 |
| 1916–17 | 工程                                 |
| 1917–18 | 炮兵                                 |
| 1918–19 | 海軍                                 |
| 1919–20 | [香港會](../Page/香港足球會.md "wikilink") |
| 1920–21 | 威路沙陸軍                              |
| 1921–22 | 居了艦隊                               |
| 1922–23 | 京士陸軍                               |
| 1923–24 | [南華](../Page/南華足球隊.md "wikilink")  |
| 1924–25 | 東沙利陸軍                              |
| 1925–26 | 九龍                                 |
| 1926–27 | 西洋會                                |
| 1927–28 | 中華                                 |
| 1928–29 | 中華                                 |
| 1929–30 | 中華                                 |
| 1930–31 | [南華](../Page/南華足球隊.md "wikilink")  |
| 1931–32 | 海軍                                 |
| 1932–33 | [南華](../Page/南華足球隊.md "wikilink")  |
| 1933–34 | 威爾殊                                |
| 1934–35 | 南華A                                |
| 1935–36 | 南華A                                |
| 1936–37 | 奧斯德                                |
| 1937–38 | 南華B                                |
| 1938–39 | 南華A                                |
| 1939–40 | 南華A                                |
| 1940–41 | [南華](../Page/南華足球隊.md "wikilink")  |
| 1941–45 | 第二次世界大戰，聯賽停頓                       |
| 1945–46 | 空軍                                 |

  - 1946–1980年

<table>
<thead>
<tr class="header">
<th><p>球季</p></th>
<th><p>甲組</p></th>
<th><p>預備組</p></th>
<th><p>乙組</p></th>
<th><p>丙組</p></th>
<th><p>丁組</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1946–47</p></td>
<td><p>星島</p></td>
<td><p>—</p></td>
<td><p>星島</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1947–48</p></td>
<td><p><a href="../Page/傑志.md" title="wikilink">傑志</a></p></td>
<td><p>—</p></td>
<td><p><a href="../Page/東方足球隊.md" title="wikilink">東方</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1948–49</p></td>
<td><p>南華A</p></td>
<td><p>—</p></td>
<td><p>中華</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1949–50</p></td>
<td><p><a href="../Page/傑志.md" title="wikilink">傑志</a></p></td>
<td><p>—</p></td>
<td><p>報聯</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1950–51</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>—</p></td>
<td><p><a href="../Page/傑志.md" title="wikilink">傑志</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1951–52</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>—</p></td>
<td><p>南華</p></td>
<td><p>空軍通訊367營</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1952–53</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>—</p></td>
<td><p>南華</p></td>
<td><p>大東</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1953–54</p></td>
<td><p>九巴</p></td>
<td><p>—</p></td>
<td><p>九巴</p></td>
<td><p>民航</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1954–55</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>—</p></td>
<td><p>九巴</p></td>
<td><p>九巴</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1955–56</p></td>
<td><p><a href="../Page/東方足球隊.md" title="wikilink">東方</a></p></td>
<td><p>—</p></td>
<td><p>九巴</p></td>
<td><p>軍醫</p></td>
<td><p>太華</p></td>
</tr>
<tr class="odd">
<td><p>1956–57</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>中華</p></td>
<td><p>渣甸</p></td>
<td><p>加山</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1957–58</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>南華</p></td>
<td><p>柴灣</p></td>
<td><p><a href="../Page/愉園體育會.md" title="wikilink">愉園</a></p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1958–59</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>星島</p></td>
<td><p><a href="../Page/愉園體育會.md" title="wikilink">愉園</a></p></td>
<td><p>冠雲</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1959–60</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>南華</p></td>
<td><p>加山</p></td>
<td><p>五一七</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1960–61</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>南華</p></td>
<td><p>五一七</p></td>
<td><p>太華</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1961–62</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>星島</p></td>
<td><p><a href="../Page/東昇足球隊.md" title="wikilink">東昇</a></p></td>
<td><p>警察</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1962–63</p></td>
<td><p>元朗</p></td>
<td><p>傑志</p></td>
<td><p>加山</p></td>
<td><p>怡和</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1963–64</p></td>
<td><p><a href="../Page/傑志.md" title="wikilink">傑志</a></p></td>
<td><p>南華</p></td>
<td><p>東昇</p></td>
<td><p>筲漁</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1964–65</p></td>
<td><p>愉園</p></td>
<td><p>警察</p></td>
<td><p><a href="../Page/香港流浪足球會.md" title="wikilink">流浪</a></p></td>
<td><p><a href="../Page/消防足球隊.md" title="wikilink">消防</a></p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1965–66</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>警察</p></td>
<td><p>陸軍</p></td>
<td><p>電話</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1966–67</p></td>
<td><p>九巴</p></td>
<td><p>警察</p></td>
<td><p>電話</p></td>
<td><p>怡和</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1967–68</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>警察</p></td>
<td><p>怡和</p></td>
<td><p>市政、中健<br />
（雙冠軍）</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1968–69</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>怡和</p></td>
<td><p>消防</p></td>
<td><p>愉園</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1969–70</p></td>
<td><p>怡和</p></td>
<td><p>消防</p></td>
<td><p>愉園</p></td>
<td><p>鐵行</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1970–71</p></td>
<td><p>流浪</p></td>
<td><p>怡和</p></td>
<td><p>荃灣</p></td>
<td><p>精工</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1971–72</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>愉園</p></td>
<td><p>精工</p></td>
<td><p>晨星</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1972–73</p></td>
<td><p><a href="../Page/精工體育會.md" title="wikilink">精工</a></p></td>
<td><p>愉園</p></td>
<td><p>香港會</p></td>
<td><p>怡和</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1973–74</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>光華</p></td>
<td><p>怡和</p></td>
<td><p>警察</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1974–75</p></td>
<td><p><a href="../Page/精工體育會.md" title="wikilink">精工</a></p></td>
<td><p>光華</p></td>
<td><p>警察</p></td>
<td><p>海蜂</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1975–76</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>東昇</p></td>
<td><p>九巴</p></td>
<td><p>駒騰</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1976–77</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>愉園</p></td>
<td><p>香港會</p></td>
<td><p>天鵝</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1977–78</p></td>
<td><p><a href="../Page/南華足球隊.md" title="wikilink">南華</a></p></td>
<td><p>市政</p></td>
<td><p>警察</p></td>
<td><p>寶路華</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>1978–79</p></td>
<td><p><a href="../Page/精工體育會.md" title="wikilink">精工</a></p></td>
<td><p>愉園</p></td>
<td><p>香港會</p></td>
<td><p>香港會</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>1979–80</p></td>
<td><p><a href="../Page/精工體育會.md" title="wikilink">精工</a></p></td>
<td><p>香港會</p></td>
<td><p>警察</p></td>
<td><p>菱電</p></td>
<td><p>—</p></td>
</tr>
</tbody>
</table>

  - 1980–2014年

| 球季      | 甲組                                | 預備組  | 乙組                                | 丙組                                | 丁組  |
| ------- | --------------------------------- | ---- | --------------------------------- | --------------------------------- | --- |
| 1980–81 | [精工](../Page/精工體育會.md "wikilink") | 精工   | 保濟丸                               | 晨星                                | —   |
| 1981–82 | [精工](../Page/精工體育會.md "wikilink") | 荃灣   | 菱電                                | 華雄                                | —   |
| 1982–83 | [精工](../Page/精工體育會.md "wikilink") | 精工   | 警察                                | 佳寧                                | —   |
| 1983–84 | [精工](../Page/精工體育會.md "wikilink") | 愉園   | 警察                                | 港燈                                | —   |
| 1984–85 | [精工](../Page/精工體育會.md "wikilink") | 東昇   | 港燈                                | 星島                                | —   |
| 1985–86 | [南華](../Page/南華足球隊.md "wikilink") | 東方   | 香港會                               | 金鷹                                | —   |
| 1986–87 | [南華](../Page/南華足球隊.md "wikilink") | 南華   | 星島                                | 香港會                               | —   |
| 1987–88 | [南華](../Page/南華足球隊.md "wikilink") | —    | 香港會                               | 英軍                                | —   |
| 1988–89 | [愉園](../Page/愉園體育會.md "wikilink") | —    | 保濟丸                               | 馬天尼                               | —   |
| 1989–90 | [南華](../Page/南華足球隊.md "wikilink") | 南華   | 馬天尼                               | 大田                                | —   |
| 1990–91 | [南華](../Page/南華足球隊.md "wikilink") | 麗新   | 警察                                | 五一七                               | —   |
| 1991–92 | [南華](../Page/南華足球隊.md "wikilink") | 愉園   | [傑志](../Page/傑志.md "wikilink")    | 流浪                                | —   |
| 1992–93 | [東方](../Page/東方足球隊.md "wikilink") | 愉園   | 香港會                               | 瑞聯                                | —   |
| 1993–94 | [東方](../Page/東方足球隊.md "wikilink") | 快譯通  | 發景                                | 瑞聯                                | —   |
| 1994–95 | [東方](../Page/東方足球隊.md "wikilink") | 東方   | 香港會                               | 五一七                               | —   |
| 1995–96 | 快譯通                               | 快譯通  | [東寶](../Page/東寶體育會.md "wikilink") | 二合                                | —   |
| 1996–97 | [南華](../Page/南華足球隊.md "wikilink") | 好易通  | 二合                                | 淦源                                | —   |
| 1997–98 | 快譯通                               | 流浪   | 香港會                               | [傑志](../Page/傑志.md "wikilink")    | —   |
| 1998–99 | [愉園](../Page/愉園體育會.md "wikilink") | 星島   | 香港會                               | [福建](../Page/福建體育會.md "wikilink") | —   |
| 1999–00 | [南華](../Page/南華足球隊.md "wikilink") | 流浪   | 公民                                | 浩運                                | —   |
| 2000–01 | [愉園](../Page/愉園體育會.md "wikilink") | 南華   | 香港會                               | 教聯                                | —   |
| 2001–02 | [晨曦](../Page/晨曦體育會.md "wikilink") | 澎馬流浪 | 福建                                | 國強                                | —   |
| 2002–03 | [愉園](../Page/愉園體育會.md "wikilink") | 澎馬流浪 | [傑志](../Page/傑志.md "wikilink")    | 聖約瑟                               | —   |
| 2003–04 | [晨曦](../Page/晨曦體育會.md "wikilink") | 晨曦   | 公民                                | [大埔](../Page/大埔足球會.md "wikilink") | —   |
| 2004–05 | [晨曦](../Page/晨曦體育會.md "wikilink") | 愉園   | 港會                                | 德高朋友                              | —   |
| 2005–06 | [愉園](../Page/愉園體育會.md "wikilink") | 愉園   | 港會                                | 石硤尾                               | —   |
| 2006–07 | [南華](../Page/南華足球隊.md "wikilink") | 愉園   | 東寶                                | 東盟大中                              | —   |
| 2007–08 | [南華](../Page/南華足球隊.md "wikilink") | 南華   | 淦源                                | 沙田                                | —   |
| 2008–09 | [南華](../Page/南華足球隊.md "wikilink") | 四海   | 沙田                                | 屯門                                | —   |
| 2009–10 | [南華](../Page/南華足球隊.md "wikilink") | 四海流浪 | 港會                                | 深水埗                               | —   |
| 2010–11 | [傑志](../Page/傑志.md "wikilink")    | 四海流浪 | 深水埗                               | 灣仔                                | —   |
| 2011–12 | [傑志](../Page/傑志.md "wikilink")    | 標準流浪 | 金峰科技                              | [東方](../Page/東方足球隊.md "wikilink") | —   |
| 2012–13 | [南華](../Page/南華足球隊.md "wikilink") | 標準流浪 | 元朗                                | 黃大仙                               | 油尖旺 |
| 2013–14 | [傑志](../Page/傑志.md "wikilink")    | 標準流浪 | 和富大埔                              | 駿其天旭                              | 西貢  |

</div>

### 現時制度

| 球季      | 超聯                                | 預備組                                   | 甲組                                  | 乙組                                         | 丙組                                  |
| ------- | --------------------------------- | ------------------------------------- | ----------------------------------- | ------------------------------------------ | ----------------------------------- |
| 2014–15 | [傑志](../Page/傑志.md "wikilink")    | [太陽飛馬](../Page/太陽飛馬.md "wikilink")    | 天旭                                  | 永義                                         | 東昇                                  |
| 2015–16 | [東方](../Page/東方足球隊.md "wikilink") | [標準流浪](../Page/香港流浪足球會.md "wikilink") | [和富大埔](../Page/大埔足球會.md "wikilink") | 東昇                                         | 凱景                                  |
| 2016–17 | [傑志](../Page/傑志.md "wikilink")    | [標準灝天](../Page/標準灝天.md "wikilink")    | [晨曦](../Page/晨曦體育會.md "wikilink")   | [鹿特丹斯巴達 (淦源)](../Page/淦源足球會.md "wikilink") | [愉園](../Page/愉園體育會.md "wikilink")   |
| 2017–18 | [傑志](../Page/傑志.md "wikilink")    | [和富大埔](../Page/大埔足球隊.md "wikilink")   | [港會](../Page/香港足球會.md "wikilink")   | [愉園](../Page/愉園體育會.md "wikilink")          | [高力北區](../Page/北區足球會.md "wikilink") |

<small>資料來源：[足總歷史](http://www.hkfa.com/zh-hk/load_page.php?pid=93)</small>

## 盃賽

### 港超盃賽

  - [香港高級組銀牌](../Page/香港高級組銀牌.md "wikilink")
  - [香港足總盃](../Page/香港足總盃.md "wikilink")
  - [香港菁英盃](../Page/香港菁英盃.md "wikilink")
  - [社區盃](../Page/社區盃.md "wikilink")
  - [香港聯賽盃](../Page/香港聯賽盃.md "wikilink") *(已取消)*
  - [香港超級聯賽季後附加賽](../Page/香港超級聯賽季後附加賽.md "wikilink") *(已取消)*

### 甲組、乙組及丙組盃賽

  - [香港初級組銀牌](../Page/香港初級組銀牌.md "wikilink")
  - [香港足總盃](../Page/香港足總盃.md "wikilink")

## 著名球會

| <font color="white"> 香港足球聯賽系統著名球會    |
| ------------------------------------ |
| [南華](../Page/南華足球隊.md "wikilink")    |
| [南區](../Page/南區足球會.md "wikilink")    |
| [福建](../Page/香港福建體育會.md "wikilink")  |
| [東寶](../Page/東寶.md "wikilink") *已解散* |

## 曾參加的非香港球會

<table>
<thead>
<tr class="header">
<th><p><font color="white"> 参与香港足球聯賽系統的非香港球会</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/深圳香雪上清飲足球俱樂部.md" title="wikilink">香雪上清飲</a>（2008–09年）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/广州香雪.md" title="wikilink">香雪製藥</a>（2002–05年）</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [香港足總官方網頁](http://hkfa.com)

## 注釋

[香港足球賽事](../Category/香港足球賽事.md "wikilink")
[Category:足球聯賽系統](../Category/足球聯賽系統.md "wikilink")

1.  [聯賽章則](http://www.hkfa.com/upload/rules/league1112.pdf)