[HKTA_The_Yuen_Yuen_Institute_No._1_Secondary_School_(blue_sky).jpg](https://zh.wikipedia.org/wiki/File:HKTA_The_Yuen_Yuen_Institute_No._1_Secondary_School_\(blue_sky\).jpg "fig:HKTA_The_Yuen_Yuen_Institute_No._1_Secondary_School_(blue_sky).jpg")
**[香港道教聯合會圓玄學院第一中學](../Page/香港道教聯合會圓玄學院第一中學.md "wikilink")**（，簡稱圓玄一中、YY1）是[香港一所由](../Page/香港.md "wikilink")[香港道教聯合會主辦的政府](../Page/香港道教聯合會.md "wikilink")[津貼男女](../Page/津貼.md "wikilink")[文法中學](../Page/文法中學.md "wikilink")，成立於1979年，校訓為「明道立德」。報稱位於[葵青區](../Page/葵青區.md "wikilink")[和宜合道](../Page/和宜合道.md "wikilink")42號，在[麗晶中心對面](../Page/麗晶中心.md "wikilink")，現任[校長為簡偉鴻先生](../Page/校長.md "wikilink")。

## 辦學宗旨   

香港道教聯合會圓玄學院第一中學秉承母會「以道為宗，以德為化，以修為教，以仁為育」的辦學宗旨，推行「道化教育」，並以「明道立德」為校訓，透過品德和學業兼備的全人教育，使學生在德、智、體、群、美各方面都得到全面發展。 
 
除了學習和體育成績外，學校還秉承校訓「明道立德」，十分重視道德教育，透過早會、週會、班主任課和生活教育科向學生灌輸正確的道德觀，以及教導學生整潔儀容、禮儀、謙恭和節儉的美德，讓學生在現今的社會潮流中不會隨波逐流。
為了提高德育成效，學校還加強與家長的合作。 學校和另外四間香港道教聯合會屬下中學從優質教育基金取得撥款，以在校內推廣道德教育。

## 歷任校長

1.  尤漢基博士（任期：1979年 -
    2009年），為香港道教聯合會圓玄學院第一中學的創校校長，擔任校長一職三十年，至2009年8月榮休，現為該校[法團校董會的辦學團體校董之一](../Page/法團校董會.md "wikilink")。
2.  文靜芬女士（任期：2009年 -
    2018年），於該校任職36年，至2009年9月起接任榮休的尤漢基博士出任該校的第二任校長，至2018年8月榮休。現為該校[法團校董會的辦學團體校董之一](../Page/法團校董會.md "wikilink")。
3.  簡偉鴻先生（任期：2018年至今），於該校任職18年，至2018年9月起接任榮休的文靜芬校長出任該校的第三任校長。

## 設施

學校的[建築物形成一個](../Page/建築物.md "wikilink")[方形](../Page/方形.md "wikilink")，總佔地達12000平方米，中間是[籃球場暨](../Page/籃球場.md "wikilink")[操場](../Page/操場.md "wikilink")，除[教室外亦設有互動學習中心](../Page/教室.md "wikilink")/音樂室、禮堂、飯堂、雨天操場、看台、演講廳、體適能訓練中心、電腦室、資訊及通訊科技中心、語文自學中心、英語角、普通話角、語文角、通識角、智能教室、圖書館、舞蹈室、視藝室、地理室、銀樂室、活動室、攀石牆、學生輔導室、升學及就業輔導室、學習支援室、學生食堂、學生會室、醫療室、影音控制室、錄音室、烹飪室、縫紉室、綜合科技室、科學室（組合科學）、生物實驗室、物理實驗室、化學實驗室、綜合科學實驗室、禮堂及家長教師會資源中心。天台有有機耕種的種植地點，全校均已安裝空調及無線寬頻網絡（WiFi)，全部課室已安裝實物投映機、投映機及電腦設備，以便發展資訊科技教學。升降機現正興建中，預計將於2018年尾落成。

## 歷史

1979年，第一中學的[校舍工程尚未完成](../Page/校舍.md "wikilink")，因此學校暫借位於[葵盛邨第十九座的](../Page/葵盛邨.md "wikilink")[香港道教鄧顯紀念學校上課](../Page/香港道教鄧顯紀念學校.md "wikilink")。學校於9月3日開課，當時共有6班中一，學生252人，[教師](../Page/教師.md "wikilink")8人，由尤漢基先生擔任校長。第一中學採用[文法中學課程](../Page/文法学校.md "wikilink")，學生須修讀[中文](../Page/漢語.md "wikilink")、[英文](../Page/英語.md "wikilink")、[數學](../Page/數學.md "wikilink")、綜合科學、[中史](../Page/中國歷史.md "wikilink")、[地理](../Page/地理.md "wikilink")、[世史](../Page/世界歷史.md "wikilink")、經濟及公共事務、[美術設計](../Page/美術.md "wikilink")、[體育各科](../Page/體育.md "wikilink")。
課外活動逢星期六舉行，有交通安全隊、國樂團等興趣小組。並成立「明」「道」「立」「德」四社進行社際活動，計有[拔河](../Page/拔河.md "wikilink")、[乒乓](../Page/乒乓.md "wikilink")、[籃球等比賽](../Page/籃球.md "wikilink")。為方便居住於上[葵涌區學生到葵盛上課](../Page/葵涌區.md "wikilink")，故辦理校車服務，乘搭[學生達](../Page/學生.md "wikilink")173人。

1980年，學校已辦至中二級，中一八班中二六班，學生572人，教師19人；惟因校舍仍未竣工，故仍暫借[香港道教鄧顯紀念學校上課](../Page/香港道教鄧顯紀念學校.md "wikilink")。為貫徹會方辦校宗旨，重視道德教育，從本年度開始編訂周訓大綱，並舉辦活動以配合發展。
增設科目計有：音樂，家政和工藝。體育科則編排游泳課。
為瞭解學生社群情況：本校從本年度起每年均為每班舉行社群調查，以加強群育工作。增加課外活動組別，愛丁堡獎勵計劃，男童軍，少年警訊。大型課外活動有水運會、冬至聯歡會、樂韻播萬千音樂會及暑期活動。

1981年，學校首次參加[初中派位辦法](../Page/初中派位辦法.md "wikilink")，153個中三[學生有](../Page/學生.md "wikilink")92%獲派資助中學學位（當年全港派位率為65%）。第一屆[陸運會假葵涌運動場](../Page/陸運會.md "wikilink")[舉行](../Page/舉行.md "wikilink")。此外學校為強化學校與[家長之間](../Page/家長.md "wikilink")[關係](../Page/關係.md "wikilink")，1982年3月14日舉辦第一屆家長座談會。

1989年，[學校](../Page/學校.md "wikilink")10[周年](../Page/周年.md "wikilink")[校慶](../Page/校慶.md "wikilink")，蒙總[校監](../Page/校監.md "wikilink")[湯國華蒞臨主禮](../Page/湯國華.md "wikilink")，隨即舉行為期兩天之[開放日](../Page/開放日.md "wikilink")。

1999年20周年[校慶](../Page/校慶.md "wikilink")，由[行政會議成員](../Page/行政會議成員.md "wikilink")[王{{僻字博士及](../Page/王䓪鳴.md "wikilink")[教育局](../Page/教育局.md "wikilink")[高級助理署長](../Page/高級助理署長.md "wikilink")[莊國傑先生主禮](../Page/莊國傑.md "wikilink")。當日有兩文三語、[太極](../Page/太極.md "wikilink")、[舞蹈](../Page/舞蹈.md "wikilink")、[沿繩下滑等表演](../Page/沿繩下滑.md "wikilink")。翌日舉行[開放日](../Page/開放日.md "wikilink")，有各項展覽及遊戲活動，學生、家長、舊生及各界參觀者眾。已完成[網際網路管道](../Page/網際網路.md "wikilink")、節點和[伺服器室的建設](../Page/伺服器.md "wikilink")[工程](../Page/工程.md "wikilink")。建立了以[Linux為運行系統的](../Page/Linux.md "wikilink")[內聯網](../Page/內聯網.md "wikilink")。
多媒體學習中心於6月底落成，可供43人一起[學習](../Page/學習.md "wikilink")，並於7月為教師舉行操作[訓練](../Page/訓練.md "wikilink")。為配合[資訊](../Page/資訊.md "wikilink")[科技](../Page/科技.md "wikilink")[設備所需](../Page/設備.md "wikilink")[電力](../Page/電力.md "wikilink")，開展了加建電力[變壓房的](../Page/變壓.md "wikilink")[工程](../Page/工程.md "wikilink")，電力公司為學校額外增加了一條臨時[電纜](../Page/電纜.md "wikilink")，以應付未建成[變壓房之前的需要](../Page/變壓.md "wikilink")。本年度《荳芽集》就[香港藝術發展局資助出版](../Page/香港藝術發展局.md "wikilink")。

尤漢基校長於2009年退休，職位由文靜芬繼任。同年，第一中學正式推行[三三四學制](../Page/三三四學制.md "wikilink")。高中學生可選讀兩至三個選修科，上課時間改為雙星期十日制。學校斥資改建實驗室，並加建三個小型課室，以便進行小組學習。2009年11月，第一中學在荃灣悅來酒店舉行「三十周年聯歡晚宴」。

直至2018年，簡偉鴻副校長、李彬老師及郭俊廷老師獲頒發「[行政長官卓越教學獎](../Page/行政長官卓越教學獎.md "wikilink")」。

## 外部連結

  - [香港道教聯合會圓玄學院第一中學](http://www.yy1.edu.hk)
  - [學校宣傳短片](https://www.youtube.com/watch?v=2XoTWLHxaxs/)
  - [YY1校園電視台](http://yy1.schooltube.hk/)
  - [2017年十八區STEM學校巡禮](https://media.openschool.hk/images/temp/stem02_bookB.pdf)

[Category:葵涌](../Category/葵涌.md "wikilink")
[Category:香港道教聯合會學校](../Category/香港道教聯合會學校.md "wikilink")
[H](../Category/葵青區中學.md "wikilink")