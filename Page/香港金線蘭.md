**香港金線蘭**（[学名](../Page/学名.md "wikilink")：**），[蘭科](../Page/蘭科.md "wikilink")[開唇蘭屬](../Page/開唇蘭屬.md "wikilink")，[香港稀有物種](../Page/香港.md "wikilink")，地生性草本。

香港金線蘭只生於[大帽山](../Page/大帽山.md "wikilink")、[馬鞍山及](../Page/馬鞍山_\(香港山峰\).md "wikilink")[大東山高地近溪流的石縫或林地上](../Page/大東山.md "wikilink")。特徵為純白色絲狀根狀匍匐，淺桃紅色莖呈圓柱形，匙形葉片對生。葉脈明顯如金紅色絲網狀玫麗圖案。故此得名金線蘭。

## 參考文獻

1.  曾建飛、王惠君《中國植物志》第 17 卷, 科學出版社, 1999.
2.  S.Y.Hu, The Orchidaceae of China, Quarterly Journal of the Taiwan
    Museum, 24 (3-4): 1971.
3.  G.D.Barretto et J.L.Young Saye, Hong Kong Orchids, Hong Kong Urban
    Council, 1980.
4.  S.Y.Hu, The genera of Orchidaceae in Hong Kong, The Chinese
    University Press, 1977.

## 外部連結

  - [香港金線蘭](http://www.ngensis.com/flora-b/F389.htm)

[yungianus](../Category/開唇蘭屬.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")