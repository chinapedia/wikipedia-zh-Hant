**香港電-{台}-第一-{台}-**（）是[香港電台擁有的一條以](../Page/香港電台.md "wikilink")[粵語廣播為主的頻道](../Page/粵語.md "wikilink")，該頻道為香港電-{台}-的三條以粵語廣播為主的頻道之一。

該台頻率為[FM](../Page/頻率調制.md "wikilink") 92.6-94.4
[MHz](../Page/兆赫.md "wikilink")。

## 歷史與發展

香港電-{台}-第一-{台}-是香港電-{台}-第二個設立的廣播頻道，當時稱為**華語-{台}-**，[呼號為](../Page/呼號.md "wikilink")**ZEK**，使用[AM](../Page/調幅.md "wikilink")
640
[KHz](../Page/千赫.md "wikilink")。1948年8月華語-{台}-取消呼號，1981年華語-{台}-改為第一-{台}-。1989年前未調整現頻率時，使用的頻率為[AM](../Page/調幅.md "wikilink")
783 [kHz](../Page/千赫.md "wikilink")、FM 91.6 MHz、FM 92.3 MHz及FM 93.0 MHz。

第一-{台}-以[粵語廣播](../Page/粵語.md "wikilink")，定位為「新聞及知識-{台}-」，本着「以市民心為心，以天下事為事」的精神，傳遞最新、最全面的新聞時事資訊，並且提供交流空間，讓民意和政府政策得以互相傳達。第一-{台}-還致力透過生活知識型節目，與聽眾同步增值。

2013年4月22日，第一-{台}-由「新聞、時事、資訊台」重新定位為「新聞、時事、知識台」，並推出新節目《[好佬唔易做](../Page/好佬唔易做.md "wikilink")》、《[音樂從呢度開始](../Page/音樂從呢度開始.md "wikilink")》、《[周末午夜場](../Page/周末午夜場.md "wikilink")》、《[電視風雲半世紀](../Page/電視風雲半世紀.md "wikilink")》、《[全城知識](../Page/全城知識.md "wikilink")》、《[聲音的力量](../Page/聲音的力量.md "wikilink")》。

## 發射頻率

<table style="width:60%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 30%" />
<col style="width: 15%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>發射站</p></td>
<td><p>行車隧道轉播系統</p></td>
<td><p>特高頻／調頻（FM）頻率（MHz）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歌賦山.md" title="wikilink">歌賦山</a></p></td>
<td><p><a href="../Page/大圍隧道.md" title="wikilink">大圍隧道</a>、<a href="../Page/沙田嶺隧道.md" title="wikilink">沙田嶺隧道</a>、<a href="../Page/尖山隧道.md" title="wikilink">尖山隧道</a>、<a href="../Page/南灣隧道.md" title="wikilink">南灣隧道</a>、<a href="../Page/海底隧道_(香港).md" title="wikilink">海底隧道</a>、<a href="../Page/西區海底隧道.md" title="wikilink">西區海底隧道</a>、<a href="../Page/獅子山隧道.md" title="wikilink">獅子山隧道</a>、<a href="../Page/香港仔隧道.md" title="wikilink">香港仔隧道</a>、<a href="../Page/啟德隧道.md" title="wikilink">啟德隧道</a>、<a href="../Page/長青隧道.md" title="wikilink">長青隧道</a></p></td>
<td><p>92.6</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/九龍坑山.md" title="wikilink">九龍坑山</a></p></td>
<td></td>
<td><p>93.2</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/青山.md" title="wikilink">青山</a></p></td>
<td><p><a href="../Page/愉景灣隧道.md" title="wikilink">愉景灣隧道</a></p></td>
<td><p>93.4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金山.md" title="wikilink">金山</a></p></td>
<td><p><a href="../Page/大欖隧道.md" title="wikilink">大欖隧道</a>、<a href="../Page/城門隧道.md" title="wikilink">城門隧道</a></p></td>
<td><p>92.9</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南丫島.md" title="wikilink">南丫島</a></p></td>
<td></td>
<td><p>93.6</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/筆架山.md" title="wikilink">筆架山</a></p></td>
<td></td>
<td><p>93.5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/飛鵝山.md" title="wikilink">飛鵝山</a></p></td>
<td><p><a href="../Page/東區海底隧道.md" title="wikilink">東區海底隧道</a>、<a href="../Page/大老山隧道.md" title="wikilink">大老山隧道</a>、<a href="../Page/將軍澳隧道.md" title="wikilink">將軍澳隧道</a></p></td>
<td><p>94.4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/元朗.md" title="wikilink">元朗</a><a href="../Page/374山.md" title="wikilink">374山</a></p></td>
<td></td>
<td><p>93.6</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

\[1\]

## 節目表

|             |                                                                                                    |                                                                                  |                                                                               |     |     |     |     |
| ----------- | -------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- | --- | --- | --- | --- |
| 時間          | 星期一                                                                                                | 星期二                                                                              | 星期三                                                                           | 星期四 | 星期五 | 星期六 | 星期日 |
| 05:00-06:00 | [清晨爽利](../Page/清晨爽利.md "wikilink") <sup>R5</sup>                                                   | [有聲好書](../Page/有聲好書.md "wikilink")（精華版）<small>［星期五 0230-］</small>                | [周末午夜場](../Page/周末午夜場.md "wikilink") <sup>R5</sup> <small>［星期六 0200-］</small> |     |     |     |     |
| 06:00-06:30 | [知識會社](../Page/知識會社.md "wikilink") <sup>R2、PTH</sup>                                               | [Beautiful Sunday](../Page/Beautiful_Sunday.md "wikilink") <sup>R2、R5、PTHC</sup> |                                                                               |     |     |     |     |
| 06:30-07:00 | [晨早新聞天地](../Page/新聞天地.md "wikilink") <sup>R5</sup>                                                 |                                                                                  |                                                                               |     |     |     |     |
| 07:00-08:00 | [BBC 時事一週](../Page/BBC_時事一週.md "wikilink")                                                         |                                                                                  |                                                                               |     |     |     |     |
| 08:00-08:10 | [千禧年代](../Page/千禧年代_\(電台節目\).md "wikilink") <sup>TV31</sup><small>（0805-0900與港台電視31、31A聯播）</small> | [星期六問責](../Page/星期六問責.md "wikilink") <sup>TV31</sup>                             | [晨早新聞天地](../Page/新聞天地.md "wikilink")                                          |     |     |     |     |
| 08:10-09:00 | [舊日的足跡](../Page/舊日的足跡_\(電台節目\).md "wikilink")                                                      |                                                                                  |                                                                               |     |     |     |     |
| 09:00-09:20 | [香港家書](../Page/香港家書_\(節目\).md "wikilink")                                                          |                                                                                  |                                                                               |     |     |     |     |
| 09:20-10:00 | [投資新世代](../Page/投資新世代.md "wikilink") <sup>TV31</sup>                                               |                                                                                  |                                                                               |     |     |     |     |
| 10:00-10:20 | [一桶金](../Page/一桶金.md "wikilink")                                                                   | [講東講西](../Page/講東講西.md "wikilink")                                               |                                                                               |     |     |     |     |
| 10:20-11:00 | [開心日報](../Page/開心日報.md "wikilink")                                                                 |                                                                                  |                                                                               |     |     |     |     |
| 11:00-12:00 | [十萬八千里](../Page/十萬八千里.md "wikilink")                                                               |                                                                                  |                                                                               |     |     |     |     |
| 12:00-12:15 | [午間新聞天地](../Page/新聞天地.md "wikilink")                                                               | [午間新聞天地](../Page/新聞天地.md "wikilink")                                             | [午間新聞天地](../Page/新聞天地.md "wikilink")                                          |     |     |     |     |
| 12:15-12:20 | [一分鐘閱讀 (精華版)](../Page/一分鐘閱讀_\(精華版\).md "wikilink")                                                 |                                                                                  |                                                                               |     |     |     |     |
| 12:20-12:30 | [大氣候](../Page/大氣候.md "wikilink")                                                                   |                                                                                  |                                                                               |     |     |     |     |
| 12:30-13:00 | [一桶金](../Page/一桶金.md "wikilink")                                                                   |                                                                                  |                                                                               |     |     |     |     |
| 13:00-14:00 | [精靈一點](../Page/精靈一點.md "wikilink") <sup>TV31</sup><small>（1310-1400與港台電視31、31A聯播）</small>          | [非常人物生活雜誌](../Page/非常人物生活雜誌.md "wikilink")                                       |                                                                               |     |     |     |     |
| 14:00-15:00 | [五十年後](../Page/五十年後.md "wikilink")                                                                 | [管理新思維](../Page/管理新思維.md "wikilink")                                             |                                                                               |     |     |     |     |
| 15:00-16:00 | [中國點點點](../Page/中國點點點.md "wikilink")                                                               | [政壇新秀訓練班](../Page/政壇新秀訓練班.md "wikilink")                                         |                                                                               |     |     |     |     |
| 16:00-16:30 | [旅遊樂園](../Page/旅遊樂園.md "wikilink")                                                                 | [懶人包](../Page/懶人包_\(電台節目\).md "wikilink")                                        |                                                                               |     |     |     |     |
| 16:30-16:41 | [體壇動靜 430](../Page/體壇動靜_430.md "wikilink")                                                         |                                                                                  |                                                                               |     |     |     |     |
| 16:41-17:00 | [一桶金之財經新思維](../Page/一桶金之財經新思維.md "wikilink")                                                       |                                                                                  |                                                                               |     |     |     |     |
| 17:00-18:00 | [自由風自由 PHONE](../Page/自由風自由_PHONE.md "wikilink")                                                   | [城市論壇](../Page/城市論壇.md "wikilink")                                               |                                                                               |     |     |     |     |
| 18:00-18:20 | [傍晚新聞天地](../Page/新聞天地.md "wikilink") <sup>R5</sup>                                                 | [傍晚新聞天地](../Page/新聞天地.md "wikilink") <sup>R5</sup>                               |                                                                               |     |     |     |     |
| 18:20-18:35 | [波樂無窮](../Page/波樂無窮.md "wikilink")                                                                 | [萬千寵愛](../Page/萬千寵愛.md "wikilink")                                               |                                                                               |     |     |     |     |
| 18:35-19:00 | [自由風自由 PHONE](../Page/自由風自由_PHONE.md "wikilink") <small>［續］</small>                                |                                                                                  |                                                                               |     |     |     |     |
| 19:00-20:00 | [十項全能](../Page/十項全能_\(電台節目\).md "wikilink")                                                        |                                                                                  |                                                                               |     |     |     |     |
| 20:00-20:30 | [守下留情](../Page/守下留情.md "wikilink")                                                                 | [古今風雲人物](../Page/古今風雲人物.md "wikilink")                                           | [大學堂](../Page/大學堂.md "wikilink")                                              |     |     |     |     |
| 20:30-21:00 | [教學有心人](../Page/教學有心人.md "wikilink")                                                               | [捉心理](../Page/捉心理.md "wikilink")                                                 |                                                                               |     |     |     |     |
| 21:00-22:00 | [法醫研究所](../Page/法醫研究所_\(電台節目\).md "wikilink")                                                      | [港台講台](../Page/港台講台.md "wikilink")                                               |                                                                               |     |     |     |     |
| 22:00-22:20 | [晚間新聞天地](../Page/新聞天地.md "wikilink") <sup>R5</sup>                                                 | [晚間新聞天地](../Page/新聞天地.md "wikilink") <sup>R5</sup>                               |                                                                               |     |     |     |     |
| 22:20-22:35 | [週末不「嬲」夜](../Page/週末不「嬲」夜.md "wikilink")                                                           | [我們不是怪獸](../Page/我們不是怪獸.md "wikilink")                                           |                                                                               |     |     |     |     |
| 22:35-23:00 | [大城小事](../Page/大城小事_\(電台節目\).md "wikilink")                                                        |                                                                                  |                                                                               |     |     |     |     |
| 23:00-00:00 | [講東講西](../Page/講東講西.md "wikilink")<small>（星期一至五）</small>                                           |                                                                                  |                                                                               |     |     |     |     |
| 00:00-01:00 | [我得你都得](../Page/我得你都得.md "wikilink")                                                               | [大城小事（精華版）](../Page/大城小事_\(電台節目\).md "wikilink")                                 |                                                                               |     |     |     |     |
| 01:00-02:00 | [那些年 那些事](../Page/那些年_那些事.md "wikilink")                                                           |                                                                                  |                                                                               |     |     |     |     |
| 02:00-02:30 | [香港故事](../Page/香港故事_\(電台節目\).md "wikilink") <sup>PTHC</sup>                                        | [周末午夜場](../Page/周末午夜場.md "wikilink") <sup>R5</sup><small>［星期日 -0600］</small>     | [有聲好書](../Page/有聲好書.md "wikilink")                                            |     |     |     |     |
| 02:30-04:00 | [有聲好書](../Page/有聲好書.md "wikilink")                                                                 | [有聲好書](../Page/有聲好書.md "wikilink")（精華版）<small>［星期六 -0600］</small>                |                                                                               |     |     |     |     |
| 04:00-05:00 | [大自然之聲](../Page/大自然之聲.md "wikilink")                                                               | [大自然之聲](../Page/大自然之聲.md "wikilink")                                             |                                                                               |     |     |     |     |
|             |                                                                                                    |                                                                                  |                                                                               |     |     |     |     |

<sup>TV31</sup>：與[港台電視31、31A聯播](../Page/港台電視31、31A.md "wikilink")
<sup>R2</sup>：與[第二台聯播](../Page/香港電台第二台.md "wikilink")
<sup>R5</sup>：與[第五台聯播](../Page/香港電台第五台.md "wikilink")
<sup>PTHC</sup>：與[普通話台聯播](../Page/香港電台普通話台.md "wikilink")

\[2\]

## 節目

香港電台第一-{台}-現時以[粵語廣播收聽](../Page/粵語.md "wikilink")，該頻道主要播放新聞、時事、資訊節目，以及逢星期日早上7時播放[BBC製作](../Page/BBC.md "wikilink")[粵語時事資訊節目](../Page/粵語.md "wikilink")《[時事一周](../Page/時事一周.md "wikilink")》\[3\]。

### 特備節目

  - [本地大事回顧](../Page/本地大事回顧.md "wikilink")
  - [兩岸大事回顧](../Page/兩岸大事回顧.md "wikilink")
  - [國際大事回顧](../Page/國際大事回顧.md "wikilink")
  - [有聲大事回顧](../Page/有聲大事回顧.md "wikilink")
  - [奧運第一台](../Page/奧運第一台.md "wikilink")（於夏季[奥林匹克运动会期間播出](../Page/奥林匹克运动会.md "wikilink")）
  - [亞運第一台](../Page/亞運第一台.md "wikilink")
  - [世界盃第一台](../Page/世界盃第一台.md "wikilink")
  - [香港電台足球直播](../Page/香港電台足球直播.md "wikilink")
  - [美國總統大選特輯](../Page/美國總統大選特輯.md "wikilink")
  - [國慶慶典特輯](../Page/國慶慶典特輯.md "wikilink")
  - [行政長官立法會答問大會](../Page/行政長官立法會答問大會.md "wikilink")
  - [行政長官答問大會](../Page/行政長官答問大會.md "wikilink")
  - [行政長官施政答問](../Page/行政長官施政答問.md "wikilink")
  - [特首施政報告發表](../Page/特首施政報告發表.md "wikilink")
  - [施政論壇](../Page/施政論壇.md "wikilink")
  - [預算案論壇](../Page/預算案論壇.md "wikilink")
  - [財政預算案演辭](../Page/財政預算案演辭.md "wikilink")
  - [財政預算案論壇](../Page/財政預算案論壇.md "wikilink")
  - [財政司司長熱線](../Page/財政司司長熱線.md "wikilink")
  - [立法會選舉論壇](../Page/立法會選舉論壇.md "wikilink")
  - [區議會選舉論壇](../Page/區議會選舉論壇.md "wikilink")

## 頻道象徵

RTHKRadio1 logo.svg|香港電台第一台於2019年4月前使用的標誌 RTHK Radio 1 Logo
(2019).svg|香港電台第一台於2019年4月起使用的標誌

## 參考資料

## 外部連結

  - [香港電-{台}-相關網站 -
    香港電-{台}-第一-{台}-](http://programme.rthk.hk/channel/radio/index.php?c=radio1)

  -
[Category:香港電台](../Category/香港電台.md "wikilink")

1.
2.
3.  如[天文台發出](../Page/天文台.md "wikilink")[八號烈風或暴風信號](../Page/八號烈風或暴風信號.md "wikilink")，節目將取消