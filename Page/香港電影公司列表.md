**香港電影公司**可分為發行公司、製作公司，其中很多公司是兩者兼營。

## 製作和發行兩者兼營

  - [Applause Pictures](../Page/Applause_Pictures.md "wikilink")
    [1](http://www.applausepictures.com/)
  - [RED ON
    RED](../Page/RED_ON_RED.md "wikilink")[2](http://www.redonred.com/)
  - [夢成電影娛樂海外有限公司](../Page/夢成電影娛樂海外有限公司.md "wikilink")（Dream Movie
    Entertainments Overseas Ltd (DME)）[3](http://www.dream-movie.com/)
  - [天映娛樂有限公司](../Page/天映娛樂有限公司.md "wikilink")
  - [名威影業](../Page/名威影業.md "wikilink")（MY
    WAY）[4](http://www.mywayfilm.com.hk/)
  - [藍空間電影](../Page/藍空間.md "wikilink")（[IndBlue](../Page/IndBlue.md "wikilink")）
  - [星皓娛樂有限公司](../Page/星皓娛樂有限公司.md "wikilink")（簡稱：[星皓](../Page/星皓.md "wikilink")）
  - [银都机构有限公司](../Page/银都机构有限公司.md "wikilink")
  - [映藝娛樂有限公司](../Page/映藝娛樂有限公司.md "wikilink")（簡稱：映藝、映藝娛樂，另有映藝音樂）
  - [百分百電影製作及發行有限公司](../Page/百分百電影製作及發行有限公司.md "wikilink")（簡稱：[百分百電影](../Page/百分百電影.md "wikilink")）
  - [英皇影業有限公司](../Page/英皇電影.md "wikilink")（簡稱：[英皇電影](../Page/英皇電影.md "wikilink")）
  - [寰亞電影有限公司](../Page/寰亞電影有限公司.md "wikilink")（[寰亞綜藝娛樂集團有限公司旗下的電影製作公司](../Page/寰亞綜藝集團.md "wikilink")）
  - [中國3D數碼娛樂有限公司](../Page/HMV數碼中國集團.md "wikilink")（[HMV數碼中國集團有限公司旗下的電影製作及娛樂公司](../Page/HMV數碼中國集團.md "wikilink")）
  - [驕陽電影有限公司](../Page/驕陽電影有限公司.md "wikilink")（簡稱：[驕陽電影](../Page/驕陽電影.md "wikilink")）（Sundream
    Motion Pictures
    Ltd.）[5](https://web.archive.org/web/20070929200047/http://www.sundream-films.com/)
  - [天馬電影](../Page/天馬電影.md "wikilink")
  - [漢傳媒集團有限公司](../Page/漢傳媒集團有限公司.md "wikilink")（簡稱：[漢傳媒集團](../Page/漢傳媒集團.md "wikilink")）
  - [影王朝有限公司](../Page/影王朝有限公司.md "wikilink")
  - [無限飛卓娛樂製作有限公司](../Page/無限飛卓娛樂製作有限公司.md "wikilink")
  - [新時代影業製作有限公司](../Page/新時代影業製作有限公司.md "wikilink")

## 製作公司

  - [一百年電影有限公司](../Page/中國星.md "wikilink")（簡稱：[一百年電影](../Page/中國星.md "wikilink")）（One
    Hundred Years of Film Company Limited）
  - [中大電影創作室](../Page/中大電影創作室.md "wikilink")（B\&S Films Creation Works
    House）[6](https://web.archive.org/web/20050415011736/http://www.bs-818.com.hk/)
  - [天下媒體集團有限公司](../Page/天下媒體集團有限公司.md "wikilink")（Big Idea Group Ltd.）
  - [天幕電影製作有限公司](../Page/天幕電影製作有限公司.md "wikilink")（簡稱：[天幕電影](../Page/天幕電影.md "wikilink")）
  - [金牌大風電影有限公司](../Page/金牌大風.md "wikilink")（簡稱：[金牌大風](../Page/金牌大風.md "wikilink")）
  - [東方電影](../Page/東方電影.md "wikilink")[7](https://web.archive.org/web/20061222111707/http://www.mandarin.films.com.hk/)
  - [永盛娛樂製作有限公司](../Page/中國星.md "wikilink")（簡稱：[永盛](../Page/永盛.md "wikilink")、Win's）[8](http://www.dianying.com/ft/company/Yongsheng3)
  - [澤東製作有限公司](../Page/澤東製作有限公司.md "wikilink")（簡稱：[澤東](../Page/澤東.md "wikilink")）
  - [美亞電影製作有限公司](../Page/美亞電影製作有限公司.md "wikilink")（簡稱：[美亞電影](../Page/美亞電影.md "wikilink")）
  - [銀河映像(香港)有限公司](../Page/銀河映像\(香港\)有限公司.md "wikilink")（簡稱：[銀河映像](../Page/銀河映像.md "wikilink")）
  - [電影工作室](../Page/電影工作室.md "wikilink")（Film
    Workshop）[9](http://www.filmworkshop.net/)
  - [博美電影有限公司](../Page/博美電影有限公司.md "wikilink")（簡稱：[博美電影](../Page/博美電影.md "wikilink")）
  - [世紀創作室有限公司](../Page/世紀創作室有限公司.md "wikilink")
  - [藝行者電影公司](../Page/藝行者電影公司.md "wikilink")(Artwalker)[10](http://www.artwalker.org/)
  - [MM2影視娛樂製作香港有限公司](../Page/MM2影視娛樂製作香港有限公司.md "wikilink")（簡稱：[MM2香港](../Page/MM2香港.md "wikilink")）[11](http://www.mm2entertainment.com/index)
  - [星王朝有限公司](../Page/王晶.md "wikilink")

## 發行公司

  - [寰亞電影發行有限公司](../Page/寰亞電影發行有限公司.md "wikilink")（[寰亞綜藝集團旗下的電影發行公司](../Page/寰亞綜藝集團.md "wikilink")）
  - [美亞娛樂資訊集團有限公司](../Page/美亞娛樂資訊集團有限公司.md "wikilink")（簡稱：[美亞娛樂](../Page/美亞娛樂.md "wikilink")）
  - [中國星集團有限公司](../Page/中國星集團有限公司.md "wikilink")（簡稱：[中國星](../Page/中國星.md "wikilink")）
  - [橙天嘉禾](../Page/橙天嘉禾.md "wikilink")（全稱：[橙天嘉禾娛樂集團有限公司](../Page/橙天嘉禾娛樂集團有限公司.md "wikilink")，本來亦兼營製片，但近年只專注發行）
  - [洲立影片發行(香港)有限公司](../Page/洲立影片發行\(香港\)有限公司.md "wikilink")
  - [鐳射企業有限公司](../Page/鐳射企業有限公司.md "wikilink")
  - [寰宇影片發行有限公司](../Page/寰宇影片發行有限公司.md "wikilink")（Universe Film
    Distribution Co.
    Ltd.、簡稱：[寰宇](../Page/寰宇.md "wikilink")）[12](http://www.uih.com.hk)
  - [東方娛樂控股有限公司](../Page/東方娛樂控股有限公司.md "wikilink")
  - [邵氏兄弟影業有限公司](../Page/邵氏兄弟.md "wikilink")（簡稱：[邵氏](../Page/邵氏.md "wikilink")、[邵氏電影](../Page/邵氏電影.md "wikilink")、[邵氏兄弟](../Page/邵氏兄弟.md "wikilink")）
  - [安樂影片有限公司](../Page/安樂影片有限公司.md "wikilink")
  - [得利影視](../Page/得利影視.md "wikilink")（Deltamac (Hong
    Kong)）[13](http://www.deltamac.com.hk/)
  - [發行工作室(香港)有限公司](../Page/發行工作室\(香港\)有限公司.md "wikilink")（Distribution
    Workshop (Hong Kong) Co. Ltd.）

## 已结业電影公司

  - [新艺城影业有限公司](../Page/新艺城影业有限公司.md "wikilink")（簡稱：[新艺城](../Page/新艺城.md "wikilink")）

## 参考文献

<div class="references-small">

<references />

  - [中文電影資料庫](http://www.dianying.com/ft/)
  - [香港十大電影公司](http://gb.cri.cn/9964/2006/06/12/1326@1086071.htm)

</div>

[\*](../Category/香港電影公司.md "wikilink")
[影](../Category/香港公司列表.md "wikilink")
[Category:香港娛樂相關列表](../Category/香港娛樂相關列表.md "wikilink")