[HK_Causeway_Bay_禮頓道_Leighton_Road_禮頓里_Leighton_Lane_香港靈糧堂_Hong_Kong_Ling_Liang_Church_entrance_Aug-2010.JPG](https://zh.wikipedia.org/wiki/File:HK_Causeway_Bay_禮頓道_Leighton_Road_禮頓里_Leighton_Lane_香港靈糧堂_Hong_Kong_Ling_Liang_Church_entrance_Aug-2010.JPG "fig:HK_Causeway_Bay_禮頓道_Leighton_Road_禮頓里_Leighton_Lane_香港靈糧堂_Hong_Kong_Ling_Liang_Church_entrance_Aug-2010.JPG")香港靈糧堂\]\]
[HK_Causeway_Bay_禮頓道_Leighton_Road_禮頓里_Leighton_Lane_香港靈糧堂_Hong_Kong_Ling_Liang_Church_timetable_Public_notice_Aug-2010.JPG](https://zh.wikipedia.org/wiki/File:HK_Causeway_Bay_禮頓道_Leighton_Road_禮頓里_Leighton_Lane_香港靈糧堂_Hong_Kong_Ling_Liang_Church_timetable_Public_notice_Aug-2010.JPG "fig:HK_Causeway_Bay_禮頓道_Leighton_Road_禮頓里_Leighton_Lane_香港靈糧堂_Hong_Kong_Ling_Liang_Church_timetable_Public_notice_Aug-2010.JPG")香港靈糧堂\]\]
**香港靈糧堂**（），[基督教靈糧世界佈道會成員](../Page/基督教靈糧世界佈道會.md "wikilink")，是由[趙世光牧師在](../Page/趙世光_\(牧師\).md "wikilink")1959年5月31日於[香港創立](../Page/香港.md "wikilink")。香港靈糧堂亦為總堂，位於[香港](../Page/香港.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[禮頓道](../Page/禮頓道.md "wikilink")[禮頓里](../Page/禮頓里.md "wikilink")6號。建堂宗旨“往普天下去傳福音給萬民聽，並建立基督的教會”，多年來該教會積極展開佈道工作，成為香港及海外的宣教基地。香港靈糧堂屬下有大埔靈糧堂、東涌靈糧堂、藍田靈糧堂，附設的幼兒教育學校、小學及中學共8間，並在馬來西亞設立古晉靈糧堂。

## 歷史

  - 1942年，趙世光牧師在[上海創立](../Page/上海.md "wikilink")[中國基督教靈糧世界佈道會](../Page/中國基督教靈糧世界佈道會.md "wikilink")。1949年10月，趙牧師轉赴香港，曾租借[香港島](../Page/香港島.md "wikilink")[娛樂戲院舉行復興佈道會](../Page/娛樂戲院.md "wikilink")，其後建立[九龍靈糧堂](../Page/九龍靈糧堂.md "wikilink")。1959年5月31日，香港靈糧堂新堂落成。
  - 相繼成立[馬鞍山靈糧堂](../Page/馬鞍山靈糧堂.md "wikilink")、[大埔靈糧堂](../Page/大埔靈糧堂.md "wikilink")、[東涌靈糧堂](../Page/東涌靈糧堂.md "wikilink")。
  - 1995年，在[馬來西亞建立](../Page/馬來西亞.md "wikilink")[古晉靈糧堂](../Page/古晉靈糧堂.md "wikilink")。

## 組織

### 教會

  - 香港堂與分堂：

<!-- end list -->

  - **香港靈糧堂**（主堂，位於[銅鑼灣](../Page/銅鑼灣.md "wikilink")[禮頓道](../Page/禮頓道.md "wikilink")）
  - [大埔靈糧堂](../Page/大埔靈糧堂.md "wikilink")
  - [東涌靈糧堂](../Page/東涌靈糧堂.md "wikilink")
  - [藍田靈糧堂](../Page/藍田靈糧堂.md "wikilink")

<!-- end list -->

  - 新界五堂

<!-- end list -->

  - [洪水橋靈糧堂](../Page/洪水橋靈糧堂.md "wikilink")
  - [屯門靈糧堂](../Page/屯門靈糧堂.md "wikilink")
  - [元朗靈糧堂](../Page/元朗靈糧堂.md "wikilink")，位於[元朗大馬路](../Page/元朗大馬路.md "wikilink")
  - [元福靈糧堂](../Page/元福靈糧堂.md "wikilink")，位於[元朗](../Page/元朗.md "wikilink")[擊壤路](../Page/擊壤路.md "wikilink")
  - [元光靈糧堂](../Page/元光靈糧堂.md "wikilink")，位於[元朗](../Page/元朗.md "wikilink")

<!-- end list -->

  - 獨立堂會

<!-- end list -->

  - [九龍靈糧堂](../Page/九龍靈糧堂.md "wikilink")，位於[九龍城](../Page/九龍城.md "wikilink")[嘉林邊道](../Page/嘉林邊道.md "wikilink")
  - [主恩靈糧堂](../Page/主恩靈糧堂.md "wikilink")，位於[九龍城](../Page/九龍城.md "wikilink")[界限街](../Page/界限街.md "wikilink")
  - [旺角靈糧堂](../Page/旺角靈糧堂.md "wikilink")
  - [荃灣靈糧堂](../Page/荃灣靈糧堂.md "wikilink")
  - [沙田靈糧堂](../Page/沙田靈糧堂.md "wikilink")
  - [馬鞍山靈糧堂](../Page/馬鞍山靈糧堂.md "wikilink")，2000年自立

<!-- end list -->

  - 其他

<!-- end list -->

  - [西環靈糧堂](../Page/西環靈糧堂.md "wikilink")
  - [將軍澳主恩堂](../Page/將軍澳主恩堂.md "wikilink")
  - [古晉靈糧堂](../Page/古晉靈糧堂.md "wikilink")（[馬來西亞](../Page/馬來西亞.md "wikilink")）

### 屬下教育組織

[Ling_Liang_Church_Sau_Tak_Primary_School_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Ling_Liang_Church_Sau_Tak_Primary_School_\(Hong_Kong\).jpg "fig:Ling_Liang_Church_Sau_Tak_Primary_School_(Hong_Kong).jpg")

  - [香港靈糧堂幼稚園](../Page/香港靈糧堂幼稚園.md "wikilink")
  - [藍田靈糧幼稚園](../Page/藍田靈糧幼稚園.md "wikilink")
  - [馬鞍山靈糧幼稚園](../Page/馬鞍山靈糧幼稚園.md "wikilink")
  - [鑽石山靈糧幼稚園](../Page/鑽石山靈糧幼稚園.md "wikilink")
  - [香港靈糧堂荃灣幼稚園](../Page/香港靈糧堂荃灣幼稚園.md "wikilink")
  - [香港靈糧堂秀德幼稚園](../Page/香港靈糧堂秀德幼稚園.md "wikilink")
  - [馬鞍山靈糧小學](../Page/馬鞍山靈糧小學.md "wikilink")
  - [靈糧堂秀德小學](../Page/靈糧堂秀德小學.md "wikilink")
  - [靈糧堂劉梅軒中學](../Page/靈糧堂劉梅軒中學.md "wikilink")
  - [靈糧堂怡文中學](../Page/靈糧堂怡文中學.md "wikilink")

## 對靈恩派之立場

香港靈糧堂於教會網頁列明香港靈糧堂宗派對靈恩派之立場，以回應近親[臺北靈糧堂香港分堂](../Page/臺北靈糧堂.md "wikilink")（如[香港611靈糧堂](../Page/香港611靈糧堂.md "wikilink")、[大埔611靈糧堂](../Page/大埔611靈糧堂.md "wikilink")）與其他靈恩運動所產生之教會，視之為劃清界線。\[1\]

[臺北靈糧堂及其全球分堂都不是](../Page/臺北靈糧堂.md "wikilink")[基督教靈糧世界佈道會的成員](../Page/基督教靈糧世界佈道會.md "wikilink")。\[2\]

## 參考文獻

## 外部連結

  - [香港靈糧堂](http://www.lingliangchurch.org/)
  - [香港靈糧堂
    昔日網站](https://web.archive.org/web/20090208234336/http://hkllc.org/)
  - [大埔靈糧堂](https://web.archive.org/web/20090225112305/http://llcmhlau.edu.hk/tpllc/)
  - [東涌靈糧堂](http://www.llcew.edu.hk/church)
  - [荃灣靈糧堂](http://www.twllc.org.hk/)
      - [荃灣靈糧堂基教部網頁一](https://web.archive.org/web/20160702142425/http://www.arthurmak.hk/phpTWLLT/TWLLC.html)
      - [荃灣靈糧堂基教部網頁二](https://web.archive.org/web/20170307044815/http://www.arthurmak.hk/phpTWLLT/addCourse/addCourse.html)
      - [荃灣靈糧堂基教部網頁三](https://web.archive.org/web/20170306210538/http://www.arthurmak.hk/phpTWLLT/addCourse/jTEventSorted.html)

[Category:香港基督教新教组织](../Category/香港基督教新教组织.md "wikilink")
[Category:香港辦學團體](../Category/香港辦學團體.md "wikilink")

1.
2.