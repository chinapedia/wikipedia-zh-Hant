**香港馬王**，指一匹為馬迷所公認擁有卓越戰績的馬匹。

香港有百年賽馬歷史，其中三十年代曾出現過一匹出賽二十多次從未落敗的馬匹「[自由灣](../Page/自由灣_\(馬匹\).md "wikilink")」（Liberty
Bay）；（另有中文報紙譯名「麗碧灣」）。「自由灣」稱雄三季至退役也未曾落敗，可謂不敗馬王。\[1\]
七十年代初為馬迷所公認的馬王有：「[永勝](../Page/永勝_\(馬匹\).md "wikilink")」、「[金源](../Page/金源_\(馬匹\).md "wikilink")」、「[番攤](../Page/番攤_\(馬匹\).md "wikilink")」等。

自從1978年5月開始，香港馬王前身是由[香港評馬同業協進會創辦及煙草冠名贊助商](../Page/香港評馬同業協進會.md "wikilink")[萬寶路贊助](../Page/萬寶路.md "wikilink")，又名「萬寶路香港馬王」，所有香港職業馬評人都受邀請參加，評選準則以馬匹在該季成績為依歸。\[2\]
現從1999年起改為是由[香港賽馬會營運創辦](../Page/香港賽馬會.md "wikilink")，是每年從香港參賽馬匹中選出一匹馬是為**香港馬王**，現今是為[冠軍人馬獎最重要的獎項](../Page/冠軍人馬獎.md "wikilink")。

## 歷屆香港馬王名單

過往曾膺此榮銜的馬匹為（註：兩屆或以上的香港馬王以**粗體**顯示）：

### 1978年－1998年

| 屆次 | 年度          | 得獎馬匹                                      | 練馬師                                | 馬主                                                                | 該季勝出主要賽事                                                                                                                                                     |
| -- | ----------- | ----------------------------------------- | ---------------------------------- | ----------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 1  | 1977年－1978年 | **[祿怡](../Page/祿怡.md "wikilink")**        | [佐治摩亞](../Page/佐治摩亞.md "wikilink") | [容永道](../Page/容永道.md "wikilink")                                  | [香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")、沙宣盃                                                                                                               |
| 2  | 1978年－1979年 | **祿怡**                                    | 佐治摩亞                               | 容永道                                                               | [沙田錦標](../Page/沙田錦標.md "wikilink")、[董事盃](../Page/董事盃_\(香港\).md "wikilink")、[香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")                                        |
| 3  | 1979年－1980年 | [奪錦](../Page/奪錦.md "wikilink")            | [譚文居](../Page/譚文居.md "wikilink")   | 謝啟鑄                                                               | [香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")                                                                                                                   |
| 4  | 1980年－1981年 | **祿怡**                                    | 佐治摩亞                               | 容永道                                                               | [香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")、[香港金盃](../Page/香港金盃.md "wikilink")                                                                                |
| 5  | 1981年－1982年 | [足球](../Page/足球_\(馬匹\).md "wikilink")     | [鄭棣池](../Page/鄭棣池.md "wikilink")   | 劉耀柱                                                               | [香港打吡大賽](../Page/香港打吡大賽.md "wikilink")、[香港金盃](../Page/香港金盃.md "wikilink")                                                                                    |
| 6  | 1982年－1983年 | **[同德](../Page/同德_\(馬匹\).md "wikilink")** | [王登平](../Page/王登平.md "wikilink")   | 崔德剛與崔德宣                                                           | [香港打吡大賽](../Page/香港打吡大賽.md "wikilink")、[香港金盃](../Page/香港金盃.md "wikilink")、[香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")、馬登盃                                     |
| 7  | 1983年－1984年 | **同德**                                    | 王登平                                | 崔德剛與崔德宣                                                           | [香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")、[沙田錦標](../Page/沙田錦標.md "wikilink")、生力啤酒銀盃                                                                         |
| 8  | 1984年－1985年 | [通靈](../Page/通靈_\(馬匹\).md "wikilink")     | [伍碧權](../Page/伍碧權.md "wikilink")   | 李裕生                                                               | [主席短途獎](../Page/主席短途獎.md "wikilink")、沙宣盃、[半島金禧盃](../Page/半島金禧盃.md "wikilink")、華商會盃                                                                           |
| 9  | 1985年－1986年 | [你知幾時](../Page/你知幾時.md "wikilink")        | [簡炳墀](../Page/簡炳墀.md "wikilink")   | D C da Silva                                                      | [香港打吡大賽](../Page/香港打吡大賽.md "wikilink")                                                                                                                       |
| 10 | 1986年－1987年 | [飛躍舞士](../Page/飛躍舞士.md "wikilink")        | 簡炳墀                                | [林百欣](../Page/林百欣.md "wikilink")                                  | [香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")                                                                                                                   |
| 11 | 1987年－1988年 | [鑽中寶](../Page/鑽中寶.md "wikilink")          | [羅國洲](../Page/羅國洲.md "wikilink")   | [鄭裕彤](../Page/鄭裕彤.md "wikilink")                                  | [香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")、[女皇盃](../Page/女皇盃.md "wikilink")、人頭馬盃                                                                             |
| 12 | 1988年－1989年 | **[及時而出](../Page/及時而出.md "wikilink")**    | 伍碧權                                | 盧成培                                                               | [主席短途獎](../Page/主席短途獎.md "wikilink")、沙宣盃                                                                                                                     |
| 13 | 1989年－1990年 | **及時而出**                                  | 伍碧權                                | 盧成培                                                               | [香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")、[主席短途獎](../Page/主席短途獎.md "wikilink")、[女皇盃](../Page/女皇盃.md "wikilink")、樂聲盃                                         |
| 14 | 1990年－1991年 | **[翠河](../Page/翠河.md "wikilink")**        | [許怡](../Page/許怡.md "wikilink")     | [張奧偉與](../Page/張奧偉.md "wikilink")[夏佳理](../Page/夏佳理.md "wikilink") | [香港打吡大賽](../Page/香港打吡大賽.md "wikilink")、[香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")                                                                            |
| 15 | 1991年－1992年 | **翠河**                                    | 許怡                                 | 張奧偉與夏佳理                                                           | [香港盃](../Page/香港盃.md "wikilink")、[女皇盃](../Page/女皇盃.md "wikilink")、[香港金盃](../Page/香港金盃.md "wikilink")、[香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")              |
| 16 | 1992年－1993年 | [喜蓮之星](../Page/喜蓮之星.md "wikilink")        | [賓康](../Page/賓康.md "wikilink")     | [胡寶星與張惠慶](../Page/胡寶星.md "wikilink")                              | [香港打吡大賽](../Page/香港打吡大賽.md "wikilink")、華商會盃、聖安度碟                                                                                                             |
| 17 | 1993年－1994年 | **翠河**                                    | 許怡                                 | 張奧偉與夏佳理                                                           | [董事盃](../Page/董事盃_\(香港\).md "wikilink")、[香港金盃](../Page/香港金盃.md "wikilink")、[香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")                                        |
| 18 | 1994年－1995年 | [有性格](../Page/有性格.md "wikilink")          | [約翰摩亞](../Page/約翰摩亞.md "wikilink") | 黃榮柏                                                               | 人頭馬盃、[香港打吡大賽](../Page/香港打吡大賽.md "wikilink")、[香港金盃](../Page/香港金盃.md "wikilink")、[香港冠軍暨遮打盃](../Page/香港冠軍暨遮打盃.md "wikilink")                                    |
| 19 | 1995年－1996年 | [活力先生](../Page/活力先生.md "wikilink")        | [愛倫](../Page/愛倫.md "wikilink")     | [榮智健](../Page/榮智健.md "wikilink")                                  | [董事盃](../Page/董事盃_\(香港\).md "wikilink")、[跑馬地錦標](../Page/跑馬地錦標.md "wikilink")、[百週年紀念短途盃](../Page/百週年紀念短途盃.md "wikilink")、[主席短途獎](../Page/主席短途獎.md "wikilink") |
| 20 | 1996年－1997年 | [百勝威](../Page/百勝威.md "wikilink")          | 愛倫                                 | [郭素姿](../Page/郭素姿.md "wikilink")                                  | 人頭馬盃、賓臣盃                                                                                                                                                     |
| 21 | 1997年－1998年 | [奔騰](../Page/奔騰_\(馬匹\).md "wikilink")     | 愛倫                                 | [榮智健](../Page/榮智健.md "wikilink")                                  | [女皇盃](../Page/女皇盃.md "wikilink")、[主席短途獎](../Page/主席短途獎.md "wikilink")                                                                                        |

### 1999年至今

<table>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>年度</p></th>
<th><p>得獎馬匹</p></th>
<th><p>練馬師</p></th>
<th><p>馬主</p></th>
<th><p>該季勝出主要賽事</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>22</p></td>
<td><p>1998年－1999年</p></td>
<td><p><a href="../Page/原居民_(馬).md" title="wikilink">原居民</a></p></td>
<td><p><a href="../Page/愛倫.md" title="wikilink">愛倫</a></p></td>
<td><p><a href="../Page/彭遠慶.md" title="wikilink">彭遠慶夫婦</a></p></td>
<td><p><a href="../Page/沙田錦標.md" title="wikilink">沙田錦標</a>、<a href="../Page/樂聲盃.md" title="wikilink">樂聲盃</a>、<a href="../Page/香港瓶.md" title="wikilink">香港瓶</a>、<a href="../Page/香港金盃.md" title="wikilink">香港金盃</a></p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p>1999年－2000年</p></td>
<td><p><a href="../Page/靚蝦王.md" title="wikilink">靚蝦王</a></p></td>
<td><p><a href="../Page/姚本輝.md" title="wikilink">姚本輝</a>／<a href="../Page/愛倫.md" title="wikilink">愛倫</a></p></td>
<td><p><a href="../Page/劉錫康.md" title="wikilink">劉錫康夫婦</a></p></td>
<td><p><a href="../Page/香港短途錦標.md" title="wikilink">香港短途錦標</a>、<a href="../Page/安田紀念賽.md" title="wikilink">安田紀念賽</a></p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>2000年－2001年</p></td>
<td><p><a href="../Page/靚蝦王.md" title="wikilink">靚蝦王</a></p></td>
<td><p><a href="../Page/愛倫.md" title="wikilink">愛倫</a></p></td>
<td><p><a href="../Page/劉錫康.md" title="wikilink">劉錫康夫婦</a></p></td>
<td><p><a href="../Page/跑馬地錦標.md" title="wikilink">跑馬地錦標</a>、<a href="../Page/董事盃_(香港).md" title="wikilink">董事盃</a>、<a href="../Page/洋紫荊短途錦標.md" title="wikilink">洋紫荊短途錦標</a>、<a href="../Page/主席短途獎.md" title="wikilink">主席短途獎</a></p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p>2001年－2002年</p></td>
<td><p><a href="../Page/電子麒麟.md" title="wikilink">電子麒麟</a></p></td>
<td><p><a href="../Page/蔡約翰.md" title="wikilink">蔡約翰</a></p></td>
<td><p><a href="../Page/盧榮民.md" title="wikilink">盧榮民夫婦</a></p></td>
<td><p><a href="../Page/樂聲盃.md" title="wikilink">樂聲盃</a>、<a href="../Page/其士盃.md" title="wikilink">其士盃</a>、<a href="../Page/董事盃_(香港).md" title="wikilink">董事盃</a></p></td>
</tr>
<tr class="odd">
<td><p>26</p></td>
<td><p>2002年－2003年</p></td>
<td><p><a href="../Page/喜勁寶.md" title="wikilink">喜勁寶</a></p></td>
<td><p><a href="../Page/蔡約翰.md" title="wikilink">蔡約翰</a></p></td>
<td><p><a href="../Page/羅建生.md" title="wikilink">羅建生</a></p></td>
<td><p><a href="../Page/華商會挑戰盃.md" title="wikilink">華商會挑戰盃</a>、<a href="../Page/洋紫荊短途錦標.md" title="wikilink">洋紫荊短途錦標</a>、<a href="../Page/百週年紀念短途盃.md" title="wikilink">百週年紀念短途盃</a>、<a href="../Page/主席短途獎.md" title="wikilink">主席短途獎</a></p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td><p>2003年－2004年</p></td>
<td><p><a href="../Page/精英大師.md" title="wikilink">精英大師</a></p></td>
<td><p><a href="../Page/告東尼.md" title="wikilink">告東尼</a></p></td>
<td><p><a href="../Page/施雅治.md" title="wikilink">施雅治與</a><a href="../Page/費葆奇.md" title="wikilink">費葆奇</a></p></td>
<td><p><a href="../Page/沙田短途錦標.md" title="wikilink">沙田短途錦標</a>、<a href="../Page/香港短途錦標預賽.md" title="wikilink">香港短途錦標預賽</a>、<a href="../Page/香港短途錦標.md" title="wikilink">香港短途錦標</a>、<a href="../Page/洋紫荊短途錦標.md" title="wikilink">洋紫荊短途錦標</a>、<a href="../Page/百週年紀念短途盃.md" title="wikilink">百週年紀念短途盃</a>、<a href="../Page/主席短途獎.md" title="wikilink">主席短途獎</a></p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p>2004年－2005年</p></td>
<td><p><a href="../Page/精英大師.md" title="wikilink">精英大師</a></p></td>
<td><p><a href="../Page/告東尼.md" title="wikilink">告東尼</a></p></td>
<td><p><a href="../Page/施雅治.md" title="wikilink">施雅治與</a><a href="../Page/費葆奇.md" title="wikilink">費葆奇</a></p></td>
<td><p><a href="../Page/香港短途錦標預賽.md" title="wikilink">香港短途錦標預賽</a>、<a href="../Page/香港短途錦標.md" title="wikilink">香港短途錦標</a>、<a href="../Page/洋紫荊短途錦標.md" title="wikilink">洋紫荊短途錦標</a>、<a href="../Page/百週年紀念短途盃.md" title="wikilink">百週年紀念短途盃</a>、<a href="../Page/主席短途獎.md" title="wikilink">主席短途獎</a>、<a href="../Page/女皇銀禧紀念盃.md" title="wikilink">女皇銀禧紀念盃</a></p></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p>2005年－2006年</p></td>
<td><p><a href="../Page/牛精福星.md" title="wikilink">牛精福星</a></p></td>
<td><p><a href="../Page/告東尼.md" title="wikilink">告東尼</a></p></td>
<td><p><a href="../Page/王永強.md" title="wikilink">王永強</a></p></td>
<td><p><a href="../Page/冠軍一哩賽.md" title="wikilink">冠軍一哩賽</a>、<a href="../Page/安田紀念賽.md" title="wikilink">安田紀念賽</a></p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td><p>2006年－2007年</p></td>
<td><p><a href="../Page/爪皇凌雨.md" title="wikilink">爪皇凌雨</a></p></td>
<td><p><a href="../Page/霍利時.md" title="wikilink">霍利時</a></p></td>
<td><p><a href="../Page/周朱美屏.md" title="wikilink">周朱美屏與</a><a href="../Page/周漢文.md" title="wikilink">周漢文</a><br />
（<a href="../Page/周南_(香港馬主).md" title="wikilink">周南遺產執行人</a>）</p></td>
<td><p><a href="../Page/香港金盃.md" title="wikilink">香港金盃</a>、<a href="../Page/杜拜司馬經典賽.md" title="wikilink">杜拜司馬經典賽</a></p></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td><p>2007年－2008年</p></td>
<td><p><a href="../Page/好爸爸.md" title="wikilink">好爸爸</a></p></td>
<td><p><a href="../Page/孫達志.md" title="wikilink">孫達志</a></p></td>
<td><p><a href="../Page/袁仕傑.md" title="wikilink">袁仕傑</a></p></td>
<td><p><a href="../Page/香港一哩錦標預賽.md" title="wikilink">香港一哩錦標預賽</a>、<a href="../Page/香港一哩錦標.md" title="wikilink">香港一哩錦標</a>、<a href="../Page/董事盃_(香港).md" title="wikilink">董事盃</a>、<a href="../Page/女皇銀禧紀念盃.md" title="wikilink">女皇銀禧紀念盃</a>、<a href="../Page/冠軍一哩賽.md" title="wikilink">冠軍一哩賽</a></p></td>
</tr>
<tr class="odd">
<td><p>32</p></td>
<td><p>2008年－2009年</p></td>
<td><p><a href="../Page/爆冷.md" title="wikilink">爆冷</a></p></td>
<td><p><a href="../Page/約翰摩亞.md" title="wikilink">約翰摩亞</a></p></td>
<td><p><a href="../Page/何鴻燊.md" title="wikilink">何鴻燊</a></p></td>
<td><p><a href="../Page/香港冠軍暨遮打盃.md" title="wikilink">香港冠軍暨遮打盃</a>、<a href="../Page/香港金盃.md" title="wikilink">香港金盃</a>、<a href="../Page/香港盃預賽.md" title="wikilink">香港盃預賽</a></p></td>
</tr>
<tr class="even">
<td><p>33</p></td>
<td><p>2009年－2010年</p></td>
<td><p><a href="../Page/蓮華生輝.md" title="wikilink">蓮華生輝</a></p></td>
<td><p><a href="../Page/姚本輝.md" title="wikilink">姚本輝</a></p></td>
<td><p><a href="../Page/冼鏡煜.md" title="wikilink">冼鏡煜</a></p></td>
<td><p><a href="../Page/香港短途錦標.md" title="wikilink">香港短途錦標</a>、<a href="../Page/百週年紀念短途盃.md" title="wikilink">百週年紀念短途盃</a>、<a href="../Page/主席短途獎.md" title="wikilink">主席短途獎</a></p></td>
</tr>
<tr class="odd">
<td><p>34</p></td>
<td><p>2010年－2011年</p></td>
<td><p><a href="../Page/雄心威龍.md" title="wikilink">雄心威龍</a></p></td>
<td><p><a href="../Page/苗禮德.md" title="wikilink">苗禮德</a></p></td>
<td><p><a href="../Page/林培雄.md" title="wikilink">林培雄</a></p></td>
<td><p><a href="../Page/香港打吡大賽.md" title="wikilink">香港打吡大賽</a>、<a href="../Page/香港經典盃.md" title="wikilink">香港經典盃</a>、<a href="../Page/愛彼女皇盃.md" title="wikilink">愛彼女皇盃</a></p></td>
</tr>
<tr class="even">
<td><p>35</p></td>
<td><p>2011年－2012年</p></td>
<td><p><a href="../Page/雄心威龍.md" title="wikilink">雄心威龍</a></p></td>
<td><p><a href="../Page/苗禮德.md" title="wikilink">苗禮德</a></p></td>
<td><p><a href="../Page/林培雄.md" title="wikilink">林培雄與</a><a href="../Page/林顯裕.md" title="wikilink">林顯裕</a></p></td>
<td><p><a href="../Page/國慶盃.md" title="wikilink">國慶盃</a>、<a href="../Page/董事盃_(香港).md" title="wikilink">董事盃</a>、<a href="../Page/香港金盃.md" title="wikilink">香港金盃</a></p></td>
</tr>
<tr class="odd">
<td><p>36</p></td>
<td><p>2012年－2013年</p></td>
<td><p><a href="../Page/軍事出擊.md" title="wikilink">軍事出擊</a></p></td>
<td><p><a href="../Page/約翰摩亞.md" title="wikilink">約翰摩亞</a></p></td>
<td><p><a href="../Page/羅傑承.md" title="wikilink">羅傑承先生及</a><a href="../Page/梁芷珊.md" title="wikilink">夫人</a></p></td>
<td><p><a href="../Page/新航國際盃.md" title="wikilink">新航國際盃</a>、<a href="../Page/愛彼女皇盃.md" title="wikilink">愛彼女皇盃</a>、<a href="../Page/香港金盃.md" title="wikilink">香港金盃</a></p></td>
</tr>
<tr class="even">
<td><p>37</p></td>
<td><p>2013年－2014年</p></td>
<td><p><a href="../Page/威爾頓_(馬).md" title="wikilink">威爾頓</a></p></td>
<td><p>約翰摩亞</p></td>
<td><p><a href="../Page/鄭強輝.md" title="wikilink">鄭強輝</a></p></td>
<td><p><a href="../Page/香港經典盃.md" title="wikilink">香港經典盃</a>、<a href="../Page/香港打吡大賽.md" title="wikilink">香港打吡大賽</a>、<a href="../Page/愛彼女皇盃.md" title="wikilink">愛彼女皇盃</a></p></td>
</tr>
<tr class="odd">
<td><p>38</p></td>
<td><p>2014年－2015年</p></td>
<td><p><a href="../Page/步步友.md" title="wikilink">步步友</a></p></td>
<td><p><a href="../Page/約翰摩亞.md" title="wikilink">約翰摩亞</a></p></td>
<td><p><a href="../Page/李福鋆.md" title="wikilink">李福鋆醫生及夫人</a></p></td>
<td><p><a href="../Page/馬會一哩錦標.md" title="wikilink">馬會一哩錦標</a>、<a href="../Page/香港一哩錦標.md" title="wikilink">香港一哩錦標</a>、<a href="../Page/董事盃_(香港).md" title="wikilink">董事盃</a>、<a href="../Page/女皇銀禧紀念盃.md" title="wikilink">女皇銀禧紀念盃</a>、<a href="../Page/主席錦標.md" title="wikilink">主席錦標</a>、<a href="../Page/冠軍一哩賽.md" title="wikilink">冠軍一哩賽</a></p></td>
</tr>
<tr class="even">
<td><p>39</p></td>
<td><p>2015年－2016年</p></td>
<td><p><a href="../Page/明月千里.md" title="wikilink">明月千里</a></p></td>
<td><p><a href="../Page/約翰摩亞.md" title="wikilink">約翰摩亞</a></p></td>
<td><p><a href="../Page/程凱信.md" title="wikilink">程凱信</a></p></td>
<td><p><a href="../Page/香港打吡大賽.md" title="wikilink">香港打吡大賽</a>、<a href="../Page/愛彼女皇盃.md" title="wikilink">愛彼女皇盃</a></p></td>
</tr>
<tr class="odd">
<td><p>40</p></td>
<td><p>2016年－2017年</p></td>
<td><p><a href="../Page/佳龍駒.md" title="wikilink">佳龍駒</a></p></td>
<td><p><a href="../Page/約翰摩亞.md" title="wikilink">約翰摩亞</a></p></td>
<td><p><a href="../Page/洪祖杭.md" title="wikilink">洪祖杭</a></p></td>
<td><p><a href="../Page/香港經典一哩賽.md" title="wikilink">香港經典一哩賽</a>、<a href="../Page/香港經典盃.md" title="wikilink">香港經典盃</a>、<a href="../Page/香港打吡大賽.md" title="wikilink">香港打吡大賽</a>、<a href="../Page/主席錦標.md" title="wikilink">主席錦標</a></p></td>
</tr>
<tr class="even">
<td><p>41</p></td>
<td><p>2017年－2018年</p></td>
<td><p><a href="../Page/美麗傳承.md" title="wikilink">美麗傳承</a></p></td>
<td><p><a href="../Page/約翰摩亞.md" title="wikilink">約翰摩亞</a></p></td>
<td><p><a href="../Page/郭浩泉.md" title="wikilink">郭浩泉</a></p></td>
<td><p><a href="../Page/慶典盃.md" title="wikilink">慶典盃</a>、<a href="../Page/沙田錦標.md" title="wikilink">沙田錦標</a>、<a href="../Page/香港一哩錦標.md" title="wikilink">香港一哩錦標</a>、<a href="../Page/女皇銀禧紀念盃.md" title="wikilink">女皇銀禧紀念盃</a>、<a href="../Page/冠軍一哩賽.md" title="wikilink">冠軍一哩賽</a></p></td>
</tr>
</tbody>
</table>

### 统计

| 屆數 | 得獎馬匹 | 該季冠 | 亞 | 季 | 負 | 最多連勝 | 總計勝場數 |
| -- | ---- | --- | - | - | - | ---- | ----- |
| 1  | 祿怡   | 2   | 0 | 2 | 4 | 5    | 15    |
| 2  | 祿怡   | 6   | 1 | 0 | 1 | 5    | 15    |
| 3  | 奪錦   | 2   | 1 | 1 | 2 |      |       |
| 4  | 祿怡   | 4   | 2 | 0 | 0 | 5    | 15    |
| 5  | 足球   | 2   | 0 | 1 | 2 | 2    | 7     |
| 6  | 同德   | 5   | 0 | 0 | 0 | 10   | 12    |
| 7  | 同德   | 5   | 1 | 0 | 2 | 10   | 12    |
| 8  | 通靈   | 4   | 1 | 1 | 2 | 2    | 6     |
| 9  | 你知幾時 | 5   | 2 | 0 | 0 | 4    | 7     |
| 10 | 飛躍舞士 | 2   | 1 | 1 | 4 | 2    | 5     |
| 11 | 鑽中寶  | 4   | 1 | 1 | 3 | 2    | 8     |
| 12 | 及時而出 | 3   | 2 | 0 | 0 | 3    | 11    |
| 13 | 及時而出 | 4   | 2 | 0 | 3 | 3    | 11    |
| 14 | 翠河   | 6   | 0 | 0 | 0 | 6    | 16    |
| 15 | 翠河   | 4   | 2 | 1 | 0 | 6    | 16    |
| 16 | 喜蓮之星 | 5   | 2 | 1 | 0 | 5    | 8     |
| 17 | 翠河   | 3   | 1 | 0 | 3 | 6    | 16    |
| 18 | 有性格  | 4   | 1 | 1 | 2 | 2    | 6     |
| 19 | 活力先生 | 5   | 0 | 0 | 2 | 6    | 10    |
| 20 | 百勝威  | 2   | 3 | 3 | 2 | 2    | 10    |
| 21 | 奔騰   | 2   | 4 | 0 | 2 | 2    | 7     |
| 22 | 原居民  | 4   | 2 | 0 | 2 | 4    | 12    |
| 23 | 靚蝦王  | 2   | 4 | 0 | 1 | 2    | 12    |
| 24 | 靚蝦王  | 4   | 3 | 0 | 2 | 2    | 12    |
| 25 | 電子麒麟 | 3   | 1 | 0 | 1 | 3    | 11    |
| 26 | 喜勁寶  | 6   | 1 | 3 | 1 | 4    | 6     |
| 27 | 精英大師 | 6   | 0 | 0 | 0 | 17   | 18    |
| 28 | 精英大師 | 6   | 1 | 1 | 0 | 17   | 18    |
| 29 | 牛精福星 | 2   | 2 | 0 | 5 | 2    | 10    |
| 30 | 爪皇凌雨 | 2   | 2 | 1 | 3 | 5    | 9     |
| 31 | 好爸爸  | 5   | 1 | 0 | 1 | 5    | 16    |
| 32 | 爆冷   | 4   | 1 | 2 | 1 | 3    | 13    |
| 33 | 蓮華生輝 | 3   | 1 | 0 | 0 | 5    | 17    |
| 34 | 雄心威龍 | 7   | 0 | 0 | 1 | 6    | 13    |
| 35 | 雄心威龍 | 3   | 2 | 0 | 3 | 6    | 13    |
| 36 | 軍事出擊 | 5   | 0 | 0 | 5 | 4    | 10    |
| 37 | 威爾頓  | 4   | 1 | 0 | 2 | 3    | 10    |
| 38 | 步步友  | 6   | 0 | 0 | 2 | 6    | 12    |
| 39 | 明月千里 | 3   | 2 | 1 | 0 | 2    | 6     |
| 40 | 佳龍駒  | 4   | 0 | 0 | 2 | 4    | 8     |
| 41 | 美麗傳承 | 5   | 0 | 1 | 3 | 2    | 7     |

## 參考

## 外部連結

  - [香港賽馬會冠軍人馬獎：歷屆冠軍人馬](https://web.archive.org/web/20070612151859/http://special.hkjc.com/promo/ch/2007_champion_awards/07ca_past_winner.asp)

[Category:香港馬王](../Category/香港馬王.md "wikilink")
[Category:香港賽馬](../Category/香港賽馬.md "wikilink")

1.  《香港賽馬話舊》，[簡而清](../Page/簡而清.md "wikilink") 著（三聯書店）1992年
2.  《明報》1978年5月15日馬經版