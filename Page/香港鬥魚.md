**香港鬥魚**（[學名](../Page/學名.md "wikilink")：**）(“或俗稱：冷飯皮”)，[鬥魚科魚類](../Page/鬥魚科.md "wikilink")，一度被認為只分佈在[香港東北部](../Page/香港.md "wikilink")(曾有發現分佈於[大嶼山](../Page/大嶼山.md "wikilink"))，但現時已證實亦分佈於[廣東和](../Page/廣東.md "wikilink")[福建](../Page/福建.md "wikilink")。模式標本產地是[沙羅洞](../Page/沙羅洞.md "wikilink")，多棲息於在低地淡水[濕地和流速緩慢的河溪](../Page/濕地.md "wikilink")。模式標本及對模式標本現藏於[德國](../Page/德國.md "wikilink")[柏林](../Page/柏林.md "wikilink")[洪堡大學自然博物館](../Page/洪堡大學.md "wikilink")。

曾被誤認為產於[越南的](../Page/越南.md "wikilink")[黑叉尾鬥魚](../Page/黑叉尾鬥魚.md "wikilink")（誤認學名為
*Macropodus
concolor*），在1999年於香港發現時以為是一個舊有物種的產區增加記錄，其後於2002年確認為未曾被發表科學描述的世界新種，由於首先發現於香港境內，因此描述者以此為種名作紀念。

全球最大的香港鬥魚棲息地為[西貢](../Page/西貢.md "wikilink")[深涌](../Page/深涌.md "wikilink")，但已於1999年被發現遭受私人發展商破壞，令香港鬥魚在[深涌數量大幅減少](../Page/深涌.md "wikilink")，保育人士進行緊急的拯救，將[沼澤僅餘發現的鬥魚送到](../Page/沼澤.md "wikilink")[嘉道理農場暨植物園](../Page/嘉道理農場暨植物園.md "wikilink")。

由於分佈過於狹窄，加上[城市發展等威脅](../Page/城市.md "wikilink")，令香港鬥魚面臨[絕種威脅](../Page/絕種.md "wikilink")，香港政府正進行保育計畫。[嘉道理農場暨植物園動物保育部亦為了繁殖](../Page/嘉道理農場暨植物園.md "wikilink")，保護香港鬥魚這個瀕危物種，飼養了一定數量的個體，以進行保育繁殖的計劃。

2018年，有攝影師以紀錄片形式以鬥魚為主題拍攝短片, 帶出發展與生態的平衡\[1\]。

## 參考資料

<div class="references-small">

<references />

  - [全球魚庫FishBase](http://fishbase.sinica.edu.tw/Summary/SpeciesSummary.php?id=59719&lang=Chinese)
    </div>
  - [Why called Macropodus
    hongkongensis](http://www.hku.hk/ecology/porcupine/por26/26-vert-parafish.htm)

<div class="references-small">

<references />

  - [嘉道理農場暨植物園](http://www.kfbg.org/content/96/16/2/IUCN_paradise_fish_Final-reduced.pdf)
    </div>

[Category:香港魚類](../Category/香港魚類.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:中國魚類](../Category/中國魚類.md "wikilink")
[Category:鬥魚屬](../Category/鬥魚屬.md "wikilink")

1.  [【星期日人物】23歲生態攝影師　立志拍出港版Discovery
    Channel](https://bkb.mpweekly.com/社會/20180721-78852)