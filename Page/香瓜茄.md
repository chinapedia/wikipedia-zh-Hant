[thumb](../Page/image:Ripe_pepinos.jpg.md "wikilink")

**香瓜茄**又名**人參果**，是原产[南美洲](../Page/南美洲.md "wikilink")[安第斯山的一种](../Page/安第斯山.md "wikilink")[水果](../Page/水果.md "wikilink")。香瓜茄屬於[茄科的](../Page/茄科.md "wikilink")[多年生](../Page/多年生.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")。\[1\]

## 別稱

在中国，[甘肃](../Page/甘肃.md "wikilink")[武威地区等地引进栽种](../Page/武威.md "wikilink")。近年来依据[神话中的](../Page/神话.md "wikilink")[人参果改名](../Page/人参果_\(神话\).md "wikilink")\[2\]。臺灣[臺南市左鎮區](../Page/臺南市.md "wikilink")、[澎湖](../Page/澎湖.md "wikilink")、桃園[拉拉山及](../Page/拉拉山.md "wikilink")[南投清境等處亦有栽種](../Page/南投.md "wikilink")，[春分時節](../Page/春分.md "wikilink")，盛產。\[3\]

## 用途

果實可食，可當水果直接生食，或涼拌、清炒、煮湯皆可。\[4\]

## 參考來源

[Category:茄屬](../Category/茄屬.md "wikilink")
[Category:草本植物](../Category/草本植物.md "wikilink")
[Category:水果](../Category/水果.md "wikilink")

1.
2.
3.
4.