刻劃[外省人漂移定居臺灣的電影作品](../Page/外省人.md "wikilink")。[臺灣導演](../Page/臺灣導演.md "wikilink")[王童藉主人翁門栓的遭遇](../Page/王童.md "wikilink")，描述外省平民離開原鄉、流離臺灣的生活，為享譽國際的「臺灣近代三部曲」之第二部曲\[1\]（另兩部為[稻草人及](../Page/稻草人_\(電影\).md "wikilink")[無言的山丘](../Page/無言的山丘.md "wikilink")）。

## 概述

故事劇情描寫[國民政府士官兵們撤守到台灣的處境](../Page/國民政府.md "wikilink")，經由極盡荒謬的奇特遭遇，探討對省籍認同的身份與問題，傳達對[白色恐怖的批判與自省](../Page/白色恐怖.md "wikilink")。

## 劇情簡介

憨厚的良家子弟門栓原是國民軍隊文宣隊隊員，1949年隨軍撤至台灣。原幻想到台灣吃香蕉，哪知被懷疑是[中國共產黨間諜](../Page/中國共產黨.md "wikilink")，遂與李得勝一起逃離部隊。途中遇到月香母子。月香的丈夫病重身亡。為了照顧她們母子，門栓頂替了她丈夫，用死者北平[輔仁大學英文系的畢業證書找到工作](../Page/輔仁大學.md "wikilink")。冒牌貨在工作中鬧了不少笑話，險些丟掉飯碗，最後都化險為夷。門栓照顧患難與共的朋友李得勝，像自己家人一樣。月香與門栓共同生活，吃苦耐勞無怨無尤。四十年過去，[中國國民黨允許人民到大陸探親](../Page/中國國民黨.md "wikilink")。月香的兒子到山東找到祖父，並將祖父接到香港，他在港打長途電話，要父母到香港與祖父一敘家常。門栓驚慌，怕四十年的秘密被識破，急忙要月香回憶與公公在一起的往事。誰知月香也是冒牌貨。原來，她是在紛亂中被輔仁大學英文系的學生救出來的，這個學生的妻子剛去世，留下個嬰兒，他本人又抱病在身，月香便和他同行，以作照顧。學生死後，把畢業證書留給了月香
。門栓與月香抱在一起哭訴自己不幸的遭遇，但還是強忍著內心的悲痛，硬著頭皮去與“父子”相見。分隔四十年的“父子”開始對話，彼此問好 ，哭訴相思..

## 人物角色

| 角色名稱                           | 演員                               | 備註     |
| ------------------------------ | -------------------------------- | ------ |
| 門閂                             | [鈕承澤](../Page/鈕承澤.md "wikilink") |        |
| 左富貴                            | 門閂的新身份                           |        |
| 李得勝                            | [張世](../Page/張世.md "wikilink")   |        |
| 柳金元                            | 李得勝的新身份                          |        |
| 李傳孝                            | 李得勝的新身份                          |        |
| 李麒麟                            | [鈕承澤](../Page/鈕承澤.md "wikilink") | 門閂的新身份 |
| [李昆](../Page/李昆.md "wikilink") | 老年                               |        |
|                                | 少年                               |        |
| 阿祥                             | 方龍                               | 香蕉嫂的丈夫 |
| 香蕉嫂                            | [文英](../Page/文英.md "wikilink")   | 阿祥的妻子  |
| 阿珍                             |                                  | 阿祥的女兒  |
| 月香                             | [曾慶瑜](../Page/曾慶瑜.md "wikilink") | 李麒麟的妻子 |
| 耀華                             |                                  | 李麒麟的兒子 |
| 淑華                             |                                  |        |
| 劉小姐                            | 李欣                               | 李麒麟的同事 |
|                                |                                  |        |

## 獎項

[第26届金馬獎](../Page/第26届金馬獎.md "wikilink") (1989年)

<table style="width:50%;">
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>入圍者</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/金馬獎最佳劇情片.md" title="wikilink">最佳劇情片</a></p></td>
<td><p><strong><a href="../Page/中影公司.md" title="wikilink">中影股份有限公司</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金馬獎最佳男主角.md" title="wikilink">最佳男主角</a></p></td>
<td><p><strong><a href="../Page/鈕承澤.md" title="wikilink">鈕承澤</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金馬獎最佳男配角.md" title="wikilink">最佳男配角</a></p></td>
<td><p><strong><a href="../Page/張世.md" title="wikilink">張世</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金馬獎最佳原著劇本.md" title="wikilink">最佳原著劇本</a></p></td>
<td><p><strong><a href="../Page/王小棣.md" title="wikilink">王小棣</a>、<a href="../Page/宋紘.md" title="wikilink">宋紘</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金馬獎最佳造型設計.md" title="wikilink">最佳服裝設計</a></p></td>
<td><p><strong><a href="../Page/李寶琳.md" title="wikilink">李寶琳</a>、<a href="../Page/王童.md" title="wikilink">王童</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金馬獎最佳音效.md" title="wikilink">最佳錄音</a></p></td>
<td><p><a href="../Page/忻江盛.md" title="wikilink">忻江盛</a>、<a href="../Page/胡定一.md" title="wikilink">胡定一</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

  - [第二十六屆金馬獎得獎名單](https://web.archive.org/web/20070926230219/http://student.wtuc.edu.tw/~1093211020/34.htm)
  - [香蕉天堂 \\
    大時代下外省人的悲情故事](https://web.archive.org/web/20070527054358/http://www.taiwan123.com.tw/song/movie/movie11.htm)，〈台灣咁仔店〉
  - 胡清輝，2005年2月26日，《[華語電影參加國際影展得獎紀錄、重要回顧展（1982～2003）](https://web.archive.org/web/20160414112101/http://cinema.nccu.edu.tw/cinemaV2/lwisdominfo.htm?MID=16)》，〈[台灣電影資料庫](../Page/台灣電影資料庫.md "wikilink")〉

## 備註

<references/>

## 外部連結

  - {{@movies|fVcmb4041873}}

  -
  -
  -
  -
  - [香蕉天堂（Banana
    Paradise）](https://web.archive.org/web/20041121034500/http://cinema.nccu.edu.tw/cinemaV2/film_show.htm?SID=1873)，〈[台灣電影資料庫](../Page/台灣電影資料庫.md "wikilink")〉

[9](../Category/1980年代台灣電影作品.md "wikilink")
[Category:1980年代劇情片](../Category/1980年代劇情片.md "wikilink")
[Category:台灣劇情片](../Category/台灣劇情片.md "wikilink")
[Category:台灣眷村背景作品](../Category/台灣眷村背景作品.md "wikilink")
[Category:種族相關電影](../Category/種族相關電影.md "wikilink")
[Category:王童電影](../Category/王童電影.md "wikilink")
[Category:台灣戰後時期背景電影](../Category/台灣戰後時期背景電影.md "wikilink")
[Category:中影公司電影](../Category/中影公司電影.md "wikilink")

1.  [臺灣電影節在芝加哥登場](http://www.epochtimes.com/b5/5/9/28/n1067141.htm)，〈[大紀元時報](../Page/大紀元時報.md "wikilink")〉