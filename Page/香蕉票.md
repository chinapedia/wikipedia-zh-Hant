[Notes_issued_by_the_Japanese_Government_during_the_occupation_of_Malaya,_North_Borneo,_Sarawak_and_Brunei,_used_in_Batu_Lintang_camp,_Sarawak_(1942–1945,_obverse).jpg](https://zh.wikipedia.org/wiki/File:Notes_issued_by_the_Japanese_Government_during_the_occupation_of_Malaya,_North_Borneo,_Sarawak_and_Brunei,_used_in_Batu_Lintang_camp,_Sarawak_\(1942–1945,_obverse\).jpg "fig:Notes_issued_by_the_Japanese_Government_during_the_occupation_of_Malaya,_North_Borneo,_Sarawak_and_Brunei,_used_in_Batu_Lintang_camp,_Sarawak_(1942–1945,_obverse).jpg")巴都林當收容所囚犯中流通的香蕉票鈔票。香蕉票一詞來自10元鈔票（最下圖）上的香蕉樹樣式。\]\]

**日本香蕉票**或称**香蕉币**，俗称为日本钱或日本纸，为[日本佔領](../Page/大日本帝国.md "wikilink")[马来西亚](../Page/日本占领马来亚.md "wikilink")、[新加坡及](../Page/新加坡日治时期.md "wikilink")[北婆羅洲时期所通行的货币](../Page/英屬婆羅洲日佔時期.md "wikilink")。实际使用年限从1941年末至1945年[日本战败](../Page/二戰.md "wikilink")。[日本军用手票被称为香蕉币](../Page/日本军用手票.md "wikilink")，是因为十元面值的日本军用手票印有香蕉树。

香蕉币为不具[准备金的货币](../Page/准备金.md "wikilink")，面值最小是一仙而面值最大的是一千元，与[叻币等值](../Page/叻币.md "wikilink")，不過因為日本濫印香蕉幣，導致了通貨膨脹，變成1元[叻币可以換到好幾十元香蕉幣](../Page/叻币.md "wikilink")。

因為日本的戰敗導致各殖民地的軍用手票一文不值，在現代，香蕉幣時常被馬來西亞華人代稱「不值錢的貨幣」或者指「某件東西不值錢」，例如在2016底至2017年間，[馬來西亞令吉貶至](../Page/馬來西亞令吉.md "wikilink")18年來新低、國內物價高昂的時期，馬來西亞國民即用「香蕉票」來戲稱當時[購買力下跌的馬來西亞令吉](../Page/購買力.md "wikilink")。

## 日本政府發行的馬來亞和婆羅洲軍票

<table>
<caption>1942-45年日本政府發行軍票（馬來亞和婆羅洲）</caption>
<thead>
<tr class="header">
<th><p>圖片</p></th>
<th><p>價值</p></th>
<th><p>發行日期</p></th>
<th><p>印版</p></th>
<th><p>描述</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M1b-Malaya-Japanese_Occupation-One_Cent_ND_(1942).jpg" title="fig:MAL-M1b-Malaya-Japanese_Occupation-One_Cent_ND_(1942).jpg">MAL-M1b-Malaya-Japanese_Occupation-One_Cent_ND_(1942).jpg</a></p></td>
<td><p>1分</p></td>
<td><center>
<p>1942年</p></td>
<td><center></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M2a-Malaya-Japanese_Occupation-Five_Cents_ND_(1942).jpg" title="fig:MAL-M2a-Malaya-Japanese_Occupation-Five_Cents_ND_(1942).jpg">MAL-M2a-Malaya-Japanese_Occupation-Five_Cents_ND_(1942).jpg</a></p></td>
<td><p>5分</p></td>
<td><center>
<p>1942年</p></td>
<td><center></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M3b-Malaya-Japanese_Occupation-10_Cents_ND_(1942).jpg" title="fig:MAL-M3b-Malaya-Japanese_Occupation-10_Cents_ND_(1942).jpg">MAL-M3b-Malaya-Japanese_Occupation-10_Cents_ND_(1942).jpg</a></p></td>
<td><p>10分</p></td>
<td><center>
<p>1942年</p></td>
<td><center></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M4b-Malaya-Japanese_Occupation-50_Cents_ND_(1942).jpg" title="fig:MAL-M4b-Malaya-Japanese_Occupation-50_Cents_ND_(1942).jpg">MAL-M4b-Malaya-Japanese_Occupation-50_Cents_ND_(1942).jpg</a></p></td>
<td><p>50分</p></td>
<td><center>
<p>1942年</p></td>
<td><center>
<p>MA–MT</p></td>
<td><p>正：<a href="../Page/旅人蕉.md" title="wikilink">旅人蕉</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M5c-Malaya-Japanese_Occupation-One_Dollar_ND_(1942).jpg" title="fig:MAL-M5c-Malaya-Japanese_Occupation-One_Dollar_ND_(1942).jpg">MAL-M5c-Malaya-Japanese_Occupation-One_Dollar_ND_(1942).jpg</a></p></td>
<td><p>1元</p></td>
<td><center>
<p>1942年</p></td>
<td><center>
<p>MA–MO<br />
MR–MS</p></td>
<td><p>正：<a href="../Page/麵包樹.md" title="wikilink">麵包樹</a>（左）和<a href="../Page/棕櫚樹.md" title="wikilink">棕櫚樹</a>（右）)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M6c-Malaya-Japanese_Occupation-Five_Dollars_ND_(1942).jpg" title="fig:MAL-M6c-Malaya-Japanese_Occupation-Five_Dollars_ND_(1942).jpg">MAL-M6c-Malaya-Japanese_Occupation-Five_Dollars_ND_(1942).jpg</a></p></td>
<td><p>5元</p></td>
<td><center>
<p>1942年</p></td>
<td><center>
<p>MA–MK<br />
MO–MP<br />
MR</p></td>
<td><p>正：<a href="../Page/棕櫚樹.md" title="wikilink">棕櫚樹</a>（左），<a href="../Page/泡泡樹.md" title="wikilink">泡泡樹</a>（右）</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M7c-Malaya-Japanese_Occupation-10_Dollars_ND_(1944).jpg" title="fig:MAL-M7c-Malaya-Japanese_Occupation-10_Dollars_ND_(1944).jpg">MAL-M7c-Malaya-Japanese_Occupation-10_Dollars_ND_(1944).jpg</a></p></td>
<td><p>10元</p></td>
<td><center>
<p>1944年</p></td>
<td><center>
<p>MB–MP</p></td>
<td><p>正：水果和樹<br />
反：天際線上的船</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M8b-Malaya-Japanese_Occupation-100_Dollars_ND_(1944).jpg" title="fig:MAL-M8b-Malaya-Japanese_Occupation-100_Dollars_ND_(1944).jpg">MAL-M8b-Malaya-Japanese_Occupation-100_Dollars_ND_(1944).jpg</a></p></td>
<td><p>100元</p></td>
<td><center>
<p>1944年</p></td>
<td><center>
<p>BA</p></td>
<td><p>正：樹林和水邊小屋<br />
反：河中一名男人與兩頭<a href="../Page/水牛.md" title="wikilink">水牛群</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M9-Malaya-Japanese_Occupation-100_Dollars_ND_(1945).jpg" title="fig:MAL-M9-Malaya-Japanese_Occupation-100_Dollars_ND_(1945).jpg">MAL-M9-Malaya-Japanese_Occupation-100_Dollars_ND_(1945).jpg</a></p></td>
<td><p>100元</p></td>
<td><center>
<p>1945年</p></td>
<td><center>
<p>BA</p></td>
<td><p>正：採收<a href="../Page/天然橡膠.md" title="wikilink">橡膠的工人群</a><br />
反：海邊小屋群</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:MAL-M10b-Malaya-Japanese_Occupation-1000_Dollars_ND_(1945).jpg" title="fig:MAL-M10b-Malaya-Japanese_Occupation-1000_Dollars_ND_(1945).jpg">MAL-M10b-Malaya-Japanese_Occupation-1000_Dollars_ND_(1945).jpg</a></p></td>
<td><p>1,000元</p></td>
<td><center>
<p>1945年</p></td>
<td><center>
<p>BA</p></td>
<td><p>正：牛車<br />
反：河中一名男人與兩頭水牛群</p></td>
</tr>
</tbody>
</table>

## 備註

<references group="nb"/>

## 參考資料

<references />

## 參見

  - [日本軍用手票](../Page/日本軍用手票.md "wikilink")

[Category:已废止的日本货币](../Category/已废止的日本货币.md "wikilink")
[Category:马来西亚日占时期](../Category/马来西亚日占时期.md "wikilink")
[Category:马来西亚经济史](../Category/马来西亚经济史.md "wikilink")
[Category:新加坡日占时期](../Category/新加坡日占时期.md "wikilink")
[Category:新加坡经济史](../Category/新加坡经济史.md "wikilink")
[Category:文莱日占时期](../Category/文莱日占时期.md "wikilink")
[Category:文莱经济史](../Category/文莱经济史.md "wikilink")