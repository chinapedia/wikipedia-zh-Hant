**馬丁·約瑟夫·穆辛格**（**Martin Josef
Munzinger**，）是一位[瑞士](../Page/瑞士.md "wikilink")[政治家](../Page/政治家.md "wikilink")。

1848年11月16日，約瑟夫·穆辛格当选为[瑞士联邦委员会](../Page/瑞士联邦委员会.md "wikilink")[委员](../Page/瑞士联邦委员会委员.md "wikilink")，成为委员会最初的七位委员之一。在任期内，他主要主持领导了以下部门的工作：

  - [财政部](../Page/瑞士财政部.md "wikilink")（1848年-1850年）
  - [政治部](../Page/瑞士政治部.md "wikilink")（1851年）
  - [财政部](../Page/瑞士财政部.md "wikilink")（1852年）
  - [邮政与建设部](../Page/瑞士邮政与建设部.md "wikilink")（1853年-1854年）
  - [贸易与海关部](../Page/瑞士贸易与海关部.md "wikilink")（1855年）

他于1851年出任[瑞士联邦总统](../Page/瑞士联邦总统.md "wikilink")。

約瑟夫·穆辛格于1855年2月6日在任内去世。

## 参见

  - [瑞士联邦](../Page/瑞士联邦.md "wikilink")
  - [瑞士联邦委员会](../Page/瑞士联邦委员会.md "wikilink")
  - [瑞士联邦总统](../Page/瑞士联邦总统.md "wikilink")
  - [瑞士联邦总统列表](../Page/瑞士联邦总统列表.md "wikilink")

## 参考文献

  - Hans Haeflinger: *Bundesrat Josef Munzinger*，1953年

## 外部链接

  - Hans-Rudolf Merz: *[Bundesrat Munzinger: Der Vater des
    Schweizerfrankens](http://www.stadtolten.info/dl.php/de/20050610210640/Rede+Munzinger.pdf)*，2005年


  -
[Category:瑞士政治人物](../Category/瑞士政治人物.md "wikilink")