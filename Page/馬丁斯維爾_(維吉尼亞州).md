**馬丁斯維爾**（Martinsville,
Virginia）是[美國](../Page/美國.md "wikilink")[維吉尼亞州南部的一個獨立小城市](../Page/維吉尼亞州.md "wikilink")。面積28.5平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口15,416人。

馬丁斯維爾成立於1928年。城名紀念早前殖民者約瑟·馬丁。\[1\]

因為工作機會不多，導致青壯年人口外流至格林斯堡市、羅諾克市。

## 参考文献

<div class="references-small">

<references />

</div>

[M](../Category/弗吉尼亚州城市.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.