**馬佳莉塔·德雷莎**公主（[西班牙语](../Page/西班牙语.md "wikilink")：、[德语](../Page/德语.md "wikilink")：，
1651年7月12日，馬德里 西班牙 - 1673年3月12日，維也納
奧地利），是[西班牙的公主和](../Page/西班牙.md "wikilink")[神聖羅馬帝國皇帝](../Page/神聖羅馬帝國.md "wikilink")[利奧波德一世的皇后](../Page/利奧波德一世_\(神聖羅馬帝國\).md "wikilink")。

## 简介

她是西班牙國王[腓力四世](../Page/腓力四世_\(西班牙\).md "wikilink")（Philip IV of
Spain）和他的第二任妻子[馬莉安納](../Page/馬莉安納.md "wikilink")（Mariana of
Austria）所生。她同時也是[西班牙哈布斯堡王朝最後一位皇帝](../Page/西班牙哈布斯堡王朝.md "wikilink")[卡洛斯二世的姐姐](../Page/卡洛斯二世.md "wikilink")。

## 相关图片

<File:Diego> Velázquez 024.jpg|1653年、2歳 <File:Diego> Velázquez
029.jpg|1655年、4歳
[File:Diego_Velázquez_028b.jpg|1656年、5歲。by](File:Diego_Velázquez_028b.jpg%7C1656年、5歲。by)[蒂埃哥·委拉士開茲](../Page/蒂埃哥·委拉士開茲.md "wikilink")，[美術史美術館](../Page/Kunsthistorisches_Museum.md "wikilink")，[維也納](../Page/維也納.md "wikilink")，奧地利
<File:Diego> Velázquez 027.jpg|1659年、8歳
<File:Las_Meninas,_by_Diego_Velázquez,_from_Prado_in_Google_Earth.jpg>|「女官服侍圖」1656-7年
<File:Diego> Velázquez 026.jpg|1659年、8歳（普拉多博物館） <File:Margarita> Teresa
of Spain Mourningdress.jpg|**德雷莎公主著禱告服**（1666年）by [Juan Bautista
Martínez del
Mazo](../Page/Juan_Bautista_Martínez_del_Mazo.md "wikilink")，[普拉多博物館](../Page/普拉多博物館.md "wikilink")，[馬德里](../Page/馬德里.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")

[Category:1651年出生](../Category/1651年出生.md "wikilink")
[Category:1673年逝世](../Category/1673年逝世.md "wikilink")
[Category:西班牙王女](../Category/西班牙王女.md "wikilink")
[Category:西班牙君主女儿](../Category/西班牙君主女儿.md "wikilink")
[Category:神圣罗马帝国皇后](../Category/神圣罗马帝国皇后.md "wikilink")
[Category:馬德里人](../Category/馬德里人.md "wikilink")