**馬俊賢**（\[1\]，），第125任[澳門總督](../Page/澳門總督.md "wikilink")，是一位文人的總督。

來[澳門之前是醫學教授](../Page/澳門.md "wikilink")，於1986年5月15日到澳門，17日正式開始履行總督的職務，但他的任期只有一年多。他提倡人道主義和社會福利。

在他任內，於1987年2月24日至27日，馬俊賢夫妇應當時的[廣東省省長](../Page/廣東省.md "wikilink")[葉選平](../Page/葉選平.md "wikilink")，邀請他在[廣州參觀訪問](../Page/廣州.md "wikilink")。

## 參考資料

## 外部連結

  - [1](http://202.76.36.61/vol%2022/VOL22index2.htm)

[Category:澳門總督](../Category/澳門總督.md "wikilink")
[Category:葡萄牙政治人物](../Category/葡萄牙政治人物.md "wikilink")

1.  [澳門百科全書
    附件三:人名錄](http://www.macaudata.com/macaubook/encyclopedia/documents/690.htm)