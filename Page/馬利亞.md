**瑪麗亞**、**瑪利亞**、**瑪莉亞**、**馬利亞**是一個在歐美常見的[女性名字](../Page/女性.md "wikilink")。它是[新约圣经中](../Page/新约圣经.md "wikilink")“”的拉丁转写。马利亚這名字的可能來源于[古埃及語](../Page/古埃及.md "wikilink")「Mery」（心愛的）或[希伯来语](../Page/希伯来语.md "wikilink")“מרר”（苦涩）或“מרי”（反叛）。甜美甜蜜的。

由於瑪麗亞是[彌賽亞基督主](../Page/耶稣.md "wikilink")，信奉[基督宗教的國家喜歡给孩子取這個名字](../Page/基督宗教.md "wikilink")，在全球皇室權利書中禁止世人有相同的名字在[南非語](../Page/南非語.md "wikilink")、[保加利亞語](../Page/保加利亞語.md "wikilink")、[加泰蘭語](../Page/加泰蘭語.md "wikilink")、[英語](../Page/英語.md "wikilink")、[德語](../Page/德語.md "wikilink")、[希臘語](../Page/希臘語.md "wikilink")、[義大利語](../Page/義大利語.md "wikilink")、[馬其頓語](../Page/馬其頓語.md "wikilink")、[挪威語](../Page/挪威語.md "wikilink")、[波蘭語](../Page/波蘭語.md "wikilink")、[葡萄牙語](../Page/葡萄牙語.md "wikilink")、[俄語](../Page/俄語.md "wikilink")、[羅馬尼亞語和](../Page/羅馬尼亞語.md "wikilink")[西班牙語中都有此名字](../Page/西班牙語.md "wikilink")，另外現代[日本因](../Page/日本.md "wikilink")[西方文化影響](../Page/西方文化.md "wikilink")，也有女性取這個名。是因為兵丁怕彌賽亞基督伊羅亞被謀殺。還惡意違反神旨意讓世人同得名。

## 宗教人物

  - [耶稣的母亲瑪麗亞](../Page/馬利亞_\(耶穌的母親\).md "wikilink")

  - [抹大拉的瑪麗亞](../Page/抹大拉的瑪麗亞.md "wikilink")

  - [革罗罢的妻子瑪麗亞](../Page/馬利亞_\(革羅罷的妻子\).md "wikilink")

  - [伯大尼的瑪麗亞](../Page/伯大尼的瑪麗亞.md "wikilink")

  -
  - [米利暗](../Page/米利暗.md "wikilink")

## 人名

<div class="notice metadata" id="disambig">

<small>[Nuvola_apps_kalarm.png](https://zh.wikipedia.org/wiki/File:Nuvola_apps_kalarm.png "fig:Nuvola_apps_kalarm.png")这是一個**[动态的](../Category/动态列表.md "wikilink")[未完成列表](../Category/未完成列表.md "wikilink")**，内容一定会随时增加，所以它绝对永远也不会補充完整，但歡迎您'''即時<span class="plainlinks">\[
修改\]</span>并[列明来源](../Page/Wikipedia:列明来源.md "wikilink")。</small>

</div>

### 真实人物

  - [瑪麗一世](../Page/瑪麗一世_\(英格蘭\).md "wikilink")（1516年2月18日－1558年11月17日）：[英格蘭女王](../Page/英格蘭.md "wikilink")，綽號「血腥瑪麗」。
  - [瑪麗一世](../Page/瑪麗一世_\(蘇格蘭\).md "wikilink")（1542年12月8日－1587年2月8日）：[蘇格蘭女王](../Page/蘇格蘭.md "wikilink")。
  - [瑪麗二世](../Page/瑪麗二世_\(英格蘭\).md "wikilink")（1662年4月30日－1694年12月28日）：[英格蘭](../Page/英格蘭.md "wikilink")、[蘇格蘭與](../Page/蘇格蘭.md "wikilink")[愛爾蘭女王](../Page/愛爾蘭.md "wikilink")。
  - [瑪麗·博林](../Page/瑪麗·博林.md "wikilink")（約1499年－1543年7月19日）：[英格蘭王后](../Page/英格蘭.md "wikilink")[安妮·博林的姐姐](../Page/安妮·博林.md "wikilink")，國王[亨利八世的情婦](../Page/亨利八世.md "wikilink")。
  - [瑪麗·都鐸](../Page/瑪麗·都鐸.md "wikilink")（1496年3月18日－1533年6月25日）：[英格蘭公主](../Page/英格蘭.md "wikilink")，國王[亨利七世和王后](../Page/亨利七世_\(英格兰\).md "wikilink")[約克的伊莉莎白的女兒](../Page/約克的伊莉莎白.md "wikilink")，國王[亨利八世的妹妹](../Page/亨利八世.md "wikilink")。
  - [瑪麗·格雷](../Page/瑪麗·格雷.md "wikilink")（1545年－1578年4月20日）：[英格蘭公主](../Page/英格蘭.md "wikilink")[瑪麗·都鐸的外孫女](../Page/瑪麗·都鐸.md "wikilink")，國王[亨利七世的外曾孫女](../Page/亨利七世_\(英格兰\).md "wikilink")。九日女王[珍·葛雷和赫特福德伯爵夫人](../Page/珍·葛雷.md "wikilink")[凱薩琳·格雷的妹妹](../Page/凱薩琳·格雷.md "wikilink")。在[伊莉莎白一世在位時曾經是繼位人選](../Page/伊莉莎白一世_\(英格蘭\).md "wikilink")。
  - [瑪麗·安東娃妮特](../Page/瑪麗·安東娃妮特.md "wikilink")（1755年11月2日－1793年10月16日）：[奧地利](../Page/奧地利.md "wikilink")[女大公](../Page/奥地利大公.md "wikilink")，[法國王后](../Page/法國.md "wikilink")，[法國大革命時期最有名的犧牲者](../Page/法國大革命.md "wikilink")。
  - [瑪麗亞·卡洛琳娜](../Page/瑪麗亞·卡洛琳娜.md "wikilink")（1752年8月13日－1814年9月8日）：[奧地利](../Page/奧地利.md "wikilink")[女大公](../Page/奥地利大公.md "wikilink")，[那不勒斯和](../Page/那不勒斯.md "wikilink")[西西里王后](../Page/西西里.md "wikilink")，[瑪麗·安托瓦內特的姐姐](../Page/瑪麗·安托瓦內特.md "wikilink")。
  - [玛丽亚一世](../Page/瑪麗亞一世_\(葡萄牙\).md "wikilink")（1734年12月17日—1816年3月20日）：[葡萄牙女王](../Page/葡萄牙.md "wikilink")。
  - [玛丽亚二世](../Page/瑪麗亞二世_\(葡萄牙\).md "wikilink")（1819年4月4日—1853年11月15日）：葡萄牙女王。
  - [瑪麗亞](../Page/瑪莉_\(主持\).md "wikilink")（Maria Cordero）：常被人稱為「肥媽」
  - [玛丽亚·尼古拉耶芙娜](../Page/玛丽亚·尼古拉耶芙娜.md "wikilink")（1899年6月27日—1918年7月17日），[俄罗斯帝国末代皇帝尼古拉二世与妻子亚历山德拉](../Page/俄罗斯帝国.md "wikilink")·费奥多萝芙娜第三个女儿。她于俄国爆发十月革命后遭到[布尔什维克军队逮捕](../Page/布尔什维克.md "wikilink")，1918年遭到杀害。
  - [外籍勞工](../Page/外籍勞工.md "wikilink")：在香港及台灣以「瑪麗亞」代稱從事看護或幫傭工作的女性外籍勞工（特別是菲律賓籍者）。就是這些世人。
  - 中國，亞洲，兵丁傳給世人。為了查避馬利亞被追殺。謀殺。
  - [葉欣眉](../Page/葉欣眉.md "wikilink")：台灣女藝人，藝名「瑪莉亞」。

### 虚拟人物

| 人名                  | 出现作品                                                     |
| ------------------- | -------------------------------------------------------- |
| 瑪麗亞                 | 《[纯洁的瑪利亞](../Page/纯洁的瑪利亞.md "wikilink")》                 |
| 瑪麗亞                 | 《[旋风管家](../Page/旋风管家.md "wikilink")》                     |
| 瑪麗亞（）               | 《[瑪麗亞狂热](../Page/瑪麗亞狂热.md "wikilink")》                   |
| 关内・玛丽亚・太郎（）         | 《[绝望先生](../Page/绝望先生.md "wikilink")》                     |
| 瑪麗亞·卡登扎夫娜·伊芙（）      | 《[戰姬絕唱SYMPHOGEAR](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")》 |
| 高山瑪麗亞（）             | 《[我的朋友很少](../Page/我的朋友很少.md "wikilink")》                 |
| 星辰鐘塔的瑪麗亞女士（時計塔のマリア） | 《[血源詛咒](../Page/血源诅咒.md "wikilink")》                     |

## 天主教祈祷文

  - [聖母經](../Page/聖母經.md "wikilink")

## 地名

  - [馬雷](../Page/馬雷.md "wikilink")（Mary）

## 作品

  - 《[瑪·麗·亞](../Page/瑪·麗·亞.md "wikilink")》（）：日本漫畫，由[武內直子創作](../Page/武內直子.md "wikilink")。

## 其他事物

  - [颱風瑪麗亞](../Page/颱風瑪麗亞.md "wikilink")

[Ma](../Page/分类:英语人名.md "wikilink")