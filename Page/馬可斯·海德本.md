**馬可斯·海德本**（，），出生於[挪威](../Page/挪威.md "wikilink")[奧斯陸](../Page/奧斯陸.md "wikilink")，來自[瑞典](../Page/瑞典.md "wikilink")[隆德的](../Page/隆德.md "wikilink")[男模特兒](../Page/男模.md "wikilink")。

他在[荷蘭](../Page/荷蘭.md "wikilink")[阿姆斯特丹的街上被Tony](../Page/阿姆斯特丹.md "wikilink")
Jones模特兒經紀公司的人發掘，並迅速在[義大利](../Page/義大利.md "wikilink")[米蘭和](../Page/米蘭.md "wikilink")[美國](../Page/美國.md "wikilink")[紐約的](../Page/紐約.md "wikilink")[時裝周中嶄露頭角](../Page/時裝周.md "wikilink")，並在2007年2月12日登上[Models.com的](../Page/Models.com.md "wikilink")\[<http://models.com/model_culture/model_of_week/marcus/>|
Models of the week\]，且在兩年之內擠進Models.com中的世界男模特排行榜中的第36名。

[Category:各职业瑞典人](../Category/各职业瑞典人.md "wikilink")
[Category:瑞典模特兒](../Category/瑞典模特兒.md "wikilink")