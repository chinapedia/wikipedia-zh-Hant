**馬吉里歐·歐多尼兹**（****，），[委內瑞拉](../Page/委內瑞拉.md "wikilink")[職業棒球明星](../Page/職業棒球.md "wikilink")，出生于[加拉加斯](../Page/加拉加斯.md "wikilink")，現已退休。他於1997年正式登上[美国职业棒球大联盟](../Page/美国职业棒球大联盟.md "wikilink")，當時所屬球隊是[芝加哥白襪隊](../Page/芝加哥白襪.md "wikilink")，后因受伤被白袜队放弃，转投[底特律老虎队](../Page/底特律老虎队.md "wikilink")，效力至2011年，並於2012年退休。

## 外部連結

  -
[Category:委內瑞拉棒球選手](../Category/委內瑞拉棒球選手.md "wikilink")
[Category:芝加哥白襪隊球員](../Category/芝加哥白襪隊球員.md "wikilink")
[Category:底特律老虎隊球員](../Category/底特律老虎隊球員.md "wikilink")
[Category:美國聯盟打擊王](../Category/美國聯盟打擊王.md "wikilink")
[Category:美國聯盟全明星球員](../Category/美國聯盟全明星球員.md "wikilink")