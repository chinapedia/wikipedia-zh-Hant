**馬尼拉大屠殺**（Manila
massacre）也稱**馬尼拉大慘案**，發生在[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[菲律賓](../Page/菲律賓.md "wikilink")[馬尼拉戰役中](../Page/馬尼拉戰役_\(1945年\).md "wikilink")，共有超過10万人遇難。

[Manila_Walled_City_Destruction_May_1945.jpg](https://zh.wikipedia.org/wiki/File:Manila_Walled_City_Destruction_May_1945.jpg "fig:Manila_Walled_City_Destruction_May_1945.jpg")
[Japanese_atrocities._Philippines,_China,_Burma,_Japan_-_NARA_-_292598.jpg](https://zh.wikipedia.org/wiki/File:Japanese_atrocities._Philippines,_China,_Burma,_Japan_-_NARA_-_292598.jpg "fig:Japanese_atrocities._Philippines,_China,_Burma,_Japan_-_NARA_-_292598.jpg")

## 過程

1942年1月2日[菲律賓首都](../Page/菲律賓.md "wikilink")[馬尼拉被](../Page/馬尼拉.md "wikilink")[日本軍所佔領](../Page/日本軍.md "wikilink")。

1945年2月[二战](../Page/第二次世界大戰.md "wikilink")[太平洋战争末期](../Page/太平洋战争.md "wikilink")，日军在[菲律賓諸島節節敗退](../Page/菲律賓.md "wikilink")。在[美軍兵臨](../Page/美軍.md "wikilink")[馬尼拉之前](../Page/馬尼拉.md "wikilink")，日軍方面，[陸軍](../Page/大日本帝國陸軍.md "wikilink")[第14方面軍](../Page/第14方面軍_\(日本陸軍\).md "wikilink")[大將司令官](../Page/大將.md "wikilink")[山下奉文考慮到大量民眾不可能在短期內撤出](../Page/山下奉文.md "wikilink")，宣布馬尼拉為無防備都市，並將司令部撤往[碧瑤](../Page/碧瑤市.md "wikilink")，下令部隊撤出馬尼拉；但以[海軍](../Page/大日本帝國海軍.md "wikilink")[少將](../Page/少將.md "wikilink")為首的拒絕服從，此外陸軍的3個大隊也拒絕撤出馬尼拉。結果日軍仍有14,300名陸海軍官兵仍在馬尼拉市內，以及700,000來不及撤出的菲律賓平民。

在[馬尼拉戰役期間](../Page/馬尼拉戰役_\(1945年\).md "wikilink")，美軍強攻馬尼拉，日軍對平民的屠殺達到高峰；直到2月23日美軍奪回，據知死亡的菲律賓人總數達100,000人以上，平均每天有3,000人遇害。死亡的100,000人中，哪些是日軍直接屠殺的、哪些是雙方交火中死亡，已難以判定，因為美軍的炮火同樣把街道化為灰燼\[1\]。

战后[远东国际军事法庭裁定](../Page/远东国际军事法庭.md "wikilink")：日軍曾在[聖保羅大學一次殺害](../Page/聖保羅大學.md "wikilink")994名菲律賓兒童；1945年2月4日至2月10日，日軍在[巴石河南岸肆意姦淫屠殺](../Page/巴石河.md "wikilink")，燒毀了[教堂和](../Page/教堂.md "wikilink")[圖書館](../Page/圖書館.md "wikilink")，燒死避難所的3,000名難民；1945年2月5日，日軍勒令城中男女分開排列街上，將男子用[機關槍射殺](../Page/機關槍.md "wikilink")，將女子肆意[強姦後射殺](../Page/強姦.md "wikilink")，用[手榴彈炸死來不及殺害的無辜百姓](../Page/手榴彈.md "wikilink")，屍橫遍地。

此役日軍[戰死者約](../Page/戰死.md "wikilink")12,000人，美軍戰死者約1,020人、受傷5,565人。平民老百姓死伤約100,000人至150,000人之間，約為當時馬尼拉人口數的10%。

戰後成為[菲律賓總統的](../Page/菲律賓總統.md "wikilink")[埃爾皮迪奧·基里諾](../Page/埃爾皮迪奧·基里諾.md "wikilink")，妻子與5個兒女中的3個都在屠殺中遇害。日後在[韓戰期間](../Page/韓戰.md "wikilink")[菲律賓政府對日本提出](../Page/菲律賓政府.md "wikilink")80億[美元的戰爭賠償遭到日本拒絕後](../Page/美元.md "wikilink")，基里諾立刻在1天之內下令將14名[日本](../Page/日本.md "wikilink")[戰犯處決](../Page/戰犯.md "wikilink")。

屠殺倖存者成立“铭记马尼拉1945基金会”，举办纪念活动，要求[日本政府承认日军暴行並正式道歉](../Page/日本政府.md "wikilink")，不要[篡改历史](../Page/篡改历史.md "wikilink")\[2\]。

## 参见

  - [日軍戰爭罪行](../Page/日軍戰爭罪行.md "wikilink")

## 註釋

## 參考文獻

  - Taylor, Lawrence. *A Trial of Generals.* Icarus Press, South Bend
    IN, 1981

  -
## 外部連結

  - [The Battling Bastards of
    Bataan](http://www.battlingbastardsbataan.com/som.htm)
  - [The Historical Atlas of the Twentieth Century by Matthew
    White](http://users.erols.com/mwhite28/20centry.htm)

[Category:1945年太平洋战场](../Category/1945年太平洋战场.md "wikilink")
[Category:日军在太平洋战争中的战争罪行](../Category/日军在太平洋战争中的战争罪行.md "wikilink")
[Category:馬尼拉历史](../Category/馬尼拉历史.md "wikilink")
[Category:二戰中的大屠殺](../Category/二戰中的大屠殺.md "wikilink")
[Category:20世纪亚洲屠杀事件](../Category/20世纪亚洲屠杀事件.md "wikilink")
[Category:1945年2月](../Category/1945年2月.md "wikilink")

1.
2.