[Ma_Shi_Chau_Pat_Sin_Leng.jpg](https://zh.wikipedia.org/wiki/File:Ma_Shi_Chau_Pat_Sin_Leng.jpg "fig:Ma_Shi_Chau_Pat_Sin_Leng.jpg")（後）\]\]
**馬屎洲**（）位於[香港](../Page/香港.md "wikilink")[新界東北部](../Page/新界.md "wikilink")，是[大埔區](../Page/大埔區.md "wikilink")[吐露港內的一個小](../Page/吐露港.md "wikilink")[島](../Page/島.md "wikilink")。馬屎洲跟[鹽田仔由一](../Page/鹽田仔_\(大埔區\).md "wikilink")[沙洲連接](../Page/沙洲.md "wikilink")，連接點為該島西面的[水茫田的鹽田仔東面的](../Page/水茫田.md "wikilink")[門樓頸](../Page/門樓頸.md "wikilink")。1999年4月9日，由於馬屎洲擁有香港境內罕有的[連島沙洲和](../Page/連島沙洲.md "wikilink")[潮汐生態](../Page/潮汐.md "wikilink")，故此[香港政府把它與鄰近的](../Page/香港政府.md "wikilink")[洋洲](../Page/洋洲_\(大埔區\).md "wikilink")、[丫洲和一個位於鹽田仔](../Page/丫洲.md "wikilink")[聯益新村東北海面的一個未命名小島合稱](../Page/聯益新村.md "wikilink")[馬屎洲特別地區](../Page/馬屎洲特別地區.md "wikilink")，而馬屎洲則為該特別地區中最大的島嶼。當地的岩石因此受保護（例：[二疊紀形成的](../Page/二疊紀.md "wikilink")[沉積岩](../Page/沉積岩.md "wikilink")）。

由於馬屎洲屬一連島沙洲，故此遊人不需乘船前往。但於大雨或[潮漲時](../Page/潮漲.md "wikilink")，連接著鹽田仔的沙洲會被海水淹蓋，直至1990年代，於沙洲上堆放了一些大石，大雨不能連接的問題就得以解決，馬屎洲已基本上與大陸連接了。

## 水茫田

具特殊科學價值的馬屎洲水茫田，2008年被揭發遭人違規興建墳場[遠福園](../Page/遠福園.md "wikilink")，[地政總署已在](../Page/地政總署.md "wikilink")2011年下令清拆未經批准的建築物，期限在2012年2月28日屆滿，[香港蘋果日報記者在](../Page/蘋果日報_\(香港\).md "wikilink")2012年2月29日到現場視察，發現該處約有50個墓穴已有先人入土，並沒清拆迹象。[香港地貌岩石保育協會宣傳及公關蔡慕貞表示](../Page/香港地貌岩石保育協會.md "wikilink")，該處已被嚴重破壞，墓園前的貝殼灘難以恢復原貌。\[1\]

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left; float: left; width: 50%;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 注腳

<references/>

## 參見

  - [橋頭](../Page/橋頭_\(西貢區\).md "wikilink")
  - [長索](../Page/長索.md "wikilink")
  - [水浸咀排](../Page/水浸咀排.md "wikilink")
  - [雞翼角](../Page/雞翼角.md "wikilink")

## 參考資料

  -
## 外部連結

  - [新界東北及中部—027
    馬屎洲](https://web.archive.org/web/20071205064754/http://hk.geocities.com/hiking0002/h_ne027.htm)

<!-- end list -->

  - [大埔地質教育中心](https://web.archive.org/web/20150718013304/http://www.taipoea.org.hk/tpgeopark/index1.php?group=C3)

[Category:大埔區島嶼](../Category/大埔區島嶼.md "wikilink")

1.  [地質公園非法墳場　政府懶理](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20120301&sec_id=4104&subsec_id=11867&art_id=16116070)
    蘋果日報 (香港). 2012年03月01日.