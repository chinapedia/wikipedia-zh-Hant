[Marrack_Goulding_Perquín_1992.jpg](https://zh.wikipedia.org/wiki/File:Marrack_Goulding_Perquín_1992.jpg "fig:Marrack_Goulding_Perquín_1992.jpg")
**馬拉克·艾爾文·古丁**爵士，[KCMG](../Page/KCMG.md "wikilink")（Sir **Marrack Irvine
Goulding**，），[英國](../Page/英國.md "wikilink")[外交官](../Page/外交官.md "wikilink")，1986年至1997年間任[聯合國](../Page/聯合國.md "wikilink")[副秘書長](../Page/聯合國副秘書長.md "wikilink")，曾任[牛津大學](../Page/牛津大學.md "wikilink")[聖安東尼學院長](../Page/牛津大學聖安東尼學院.md "wikilink")。

## 早期生活

出生於英格蘭普利茅斯，就讀聖保祿學校，後來在倫敦研究人文學科。

## 生涯

1959年進入英國外交界服務，1961年被派往英國駐[科威特](../Page/科威特.md "wikilink")[大使館](../Page/大使館.md "wikilink")。1964年回國，在英國外交和聯邦事務部工作。1968年，他被再一次派駐海外。

1986年1月1日，成了聯合國特別政治事務副秘書長。

2004年，簽署了一份意向書，批評英國在中東的政策\[1\]。

## 注釋

<div class="references-small">

<references />

</div>

[Category:聯合國官員](../Category/聯合國官員.md "wikilink")
[Category:英國外交官](../Category/英國外交官.md "wikilink")
[Category:英國駐安哥拉大使](../Category/英國駐安哥拉大使.md "wikilink")
[Category:英國駐聖多美普林西比大使](../Category/英國駐聖多美普林西比大使.md "wikilink")
[Category:牛津大学麦格达伦学院校友](../Category/牛津大学麦格达伦学院校友.md "wikilink")
[Category:德文郡人](../Category/德文郡人.md "wikilink")

1.