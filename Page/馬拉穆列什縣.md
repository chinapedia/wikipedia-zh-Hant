**馬拉穆列什縣**（）是[羅馬尼亞西北部的一個](../Page/羅馬尼亞.md "wikilink")[縣](../Page/羅馬尼亞行政區劃.md "wikilink")。北鄰[烏克蘭](../Page/烏克蘭.md "wikilink")。面積6,304
平方公里，人口510,110 (2002年)。首府[巴亞馬雷](../Page/巴亞馬雷.md "wikilink")。

下設2市、6镇、62鄉。

## 外部連結

  - [Consiliul Județean Maramureș](http://www.cjmaramures.ro/)
  - [Prefectura Județului Maramureș](http://www.prefecturamaramures.ro/)

  - [Harta turistică a județului
    Maramureș](http://www.hartis.ro/jud/maramures.html)

***民族志***

  - [Antologie de folclor din
    Maramureș](http://ro.wikisource.org/wiki/Antologie_de_folclor_din_Maramure%C5%9F)

  - [Istoria folcloristicii
    maramureșene](http://ro.wikisource.org/wiki/Istoria_folcloristicii_maramure%C5%9Fene)

  - [Muzeul virtual al monumentelor etnografice în aer liber din
    România](http://monumente-etnografice.cimec.ro/muzee-in-aer-liber.asp?Tip=&JUD=19&ZON=&ETN=)
    (județul Maramureș)

[M](../Category/罗马尼亚行政区划.md "wikilink")