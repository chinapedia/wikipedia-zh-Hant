**馬普龍屬**（屬名：*Mapusaurus*）、又稱為**地龍**，意為「大地蜥蜴」，是種巨型[肉食龍下目](../Page/肉食龍下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於晚[白堊紀的](../Page/白堊紀.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")。馬普龍的化石是在1997到2001年期間，由一群阿根廷與[加拿大共同組成的團隊](../Page/加拿大.md "wikilink")，在阿根廷的Canadon
de Gato所挖掘出來；該地屬於Rio Limay群的[烏因庫爾組](../Page/烏因庫爾組.md "wikilink")（Huincul
Formation），地質年代為[森諾曼階](../Page/森諾曼階.md "wikilink")。在2006年，[羅多爾夫·科里亞](../Page/羅多爾夫·科里亞.md "wikilink")（Rodolfo
Coria）與[菲力·柯爾](../Page/菲力·柯爾.md "wikilink")（Phil
Currie）將這些化石正式敘述、命名\[1\]。

馬普龍的體型接近與牠們的近親[南方巨獸龍](../Page/南方巨獸龍.md "wikilink")。在2006年，科里亞、科爾根據編號MCF-PVPH-208.203標本，估計馬普龍的身長約10.2公尺。馬普龍的其他零散骨頭來自於更大個體，體型接近於[南方巨獸龍](../Page/南方巨獸龍.md "wikilink")，而科里亞、科爾估計南方巨獸龍約12.2公尺。因此馬普龍的身長約10.2到12.2公尺。體重則是根據編號MCF-PVPH-208.234標本的[股骨](../Page/股骨.md "wikilink")，重量接近3公噸\[2\]。

## 詞源

馬普龍的屬名為*Mapusaurus*，*Mapu*來自於[馬普切人的字詞](../Page/馬普切人.md "wikilink")，意為「陸地的」或「大地的」，而*sauros*在[希臘文中意為](../Page/希臘文.md "wikilink")「蜥蜴」。[模式種為](../Page/模式種.md "wikilink")**玫瑰馬普龍**（*Mapusaurus
roseae*），是以化石所發現的[玫瑰色岩石為名](../Page/玫瑰.md "wikilink")，也同時以這次挖掘活動的贊助者Rose
Letwin為名。

## 古生物學

[Mapusaurus.jpg](https://zh.wikipedia.org/wiki/File:Mapusaurus.jpg "fig:Mapusaurus.jpg")
馬普龍的化石是在在一個包含至少7個不同個體的[骨床中所挖掘出來](../Page/骨床.md "wikilink")，來自於不同年齡層。在2006年，[羅多爾夫·科里亞](../Page/羅多爾夫·科里亞.md "wikilink")（Rodolfo
Coria）與[菲力·柯爾](../Page/菲力·柯爾.md "wikilink")（Phil
Currie）推測這個骨床可能是許多屍體堆積而成，是某種掠食動物陷阱，也許能提供馬普龍行為的線索。目前已知的其他[獸腳類骨床包括](../Page/獸腳類.md "wikilink")：[猶他州](../Page/猶他州.md "wikilink")[克利夫蘭勞埃德採石場的](../Page/克利夫蘭勞埃德採石場.md "wikilink")[異特龍骨床](../Page/異特龍.md "wikilink")、[亞伯達省的](../Page/亞伯達省.md "wikilink")[艾伯塔龍骨床](../Page/艾伯塔龍.md "wikilink")、以及[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[懼龍骨床](../Page/懼龍.md "wikilink")。

[古生物學家羅多爾夫](../Page/古生物學家.md "wikilink")·科里亞否定他之前所提出的假說，他在一個會議上提出這個化石集合處，可能顯示馬普龍以群體獵食，共同圍捕大型獵物，例如巨大的[蜥腳下目](../Page/蜥腳下目.md "wikilink")[阿根廷龍](../Page/阿根廷龍.md "wikilink")\[3\]。如果這個假設屬實，這骨床將是除了[暴龍以外的大型獸腳類恐龍群體獵食的第一個大量證據](../Page/暴龍.md "wikilink")；但仍不確定馬普龍是以有組織的群體獵食（類似[狼](../Page/狼.md "wikilink")），還是以隨機聚集的方式獵食。烏因庫爾組的沉積環境被解釋成淡水水道沉積層，有短暫或季節性河流的乾旱或半乾旱環境\[4\]。在缺乏化石的烏因庫爾組中，這個骨床的存在顯得相當有趣。

## 相關化石與鑑定

[Mapusaurus_skulls.jpg](https://zh.wikipedia.org/wiki/File:Mapusaurus_skulls.jpg "fig:Mapusaurus_skulls.jpg")
[模式種玫瑰馬普龍的](../Page/模式種.md "wikilink")[正模式標本是一個獨自的右](../Page/正模式標本.md "wikilink")[鼻骨](../Page/鼻骨.md "wikilink")（標號MCF-PVPH-108.1）。已有12個[副模式標本被標明](../Page/副模式標本.md "wikilink")，為另外的獨自骨骸。這些從[骨床發現的獨立標本組合起來後](../Page/骨床.md "wikilink")，形成馬普龍大部分的骨骸\[5\]。

在2006年，科里亞與柯爾將馬普龍鑑定為：一種[獸腳亞目](../Page/獸腳亞目.md "wikilink")[鯊齒龍科恐龍](../Page/鯊齒龍科.md "wikilink")，與[南方巨獸龍的差異為頭顱骨有厚](../Page/南方巨獸龍.md "wikilink")、表面凹凸不平、未固定的鼻骨，鼻骨與[上頜骨和](../Page/上頜骨.md "wikilink")[淚骨的接合處前段較狹窄](../Page/淚骨.md "wikilink")；[眶前窩在上頜骨之上有較大的延伸](../Page/眶前窩.md "wikilink")；較小的上頜孔；眶前孔與上頜孔之間有較寬的骨棒；位置較低、較平坦的淚骨角；[額前骨橫切面與淚骨寬度相比較寬](../Page/額前骨.md "wikilink")；[眼瞼骨腹側面往後緣彎曲](../Page/眼瞼骨.md "wikilink")；較淺的齒間骨板；美克耳氏館的位置較高；[齒骨後腹側緣更往後傾斜](../Page/齒骨.md "wikilink")。

玫瑰馬普龍的獨特處在於：[顴骨的上方顴骨突分裂成兩叉](../Page/顴骨.md "wikilink")、下頜舌骨的前孔小，位於[齒骨與](../Page/齒骨.md "wikilink")[夾骨連接處上方](../Page/夾骨.md "wikilink")、第二與第三[掌骨固定](../Page/掌骨.md "wikilink")、[肱骨有較寬的末端](../Page/肱骨.md "wikilink")，跟[髁狀突之間有小分隔](../Page/髁狀突.md "wikilink")、[腸骨的短肌窩往](../Page/腸骨.md "wikilink")[坐骨腳延伸](../Page/坐骨.md "wikilink")。

馬普龍與南方巨獸龍的差異還有：頸椎的[骨骺呈圓椎狀且微彎](../Page/骨骺.md "wikilink")、第一頸椎的後關節突在中間連接、頸椎中央有較小的前脊板、頸椎[神經棘的後緣銳利](../Page/神經棘.md "wikilink")、神經棘高而寬、[坐骨幹彎曲](../Page/坐骨.md "wikilink")、[腓骨較修長](../Page/腓骨.md "wikilink")\[6\]。

[Mapusaurus_scale.png](https://zh.wikipedia.org/wiki/File:Mapusaurus_scale.png "fig:Mapusaurus_scale.png")的體型比較圖\]\]

## 系統發生學

在2006年，科里亞與柯爾的[親緣分支分類法研究顯示馬普龍屬於](../Page/親緣分支分類法.md "wikilink")[鯊齒龍科](../Page/鯊齒龍科.md "wikilink")，而馬普龍[股骨上的結構物顯示牠們與](../Page/股骨.md "wikilink")[南方巨獸龍的關係較近](../Page/南方巨獸龍.md "wikilink")，而離[鯊齒龍較遠](../Page/鯊齒龍.md "wikilink")。科里亞與柯爾根據這層關係，提出一個新的[單系群分類](../Page/單系群.md "wikilink")，南方巨獸龍亞科；該亞科的定義為：在鯊齒龍科之中，親緣關係較接近南方巨獸龍與馬普龍，而離鯊齒龍較遠的所有物種。[魁紂龍暫時被分類於南方巨獸龍亞科](../Page/魁紂龍.md "wikilink")，需要更詳細的敘述才能做更正確的分類。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [馬普龍的標本詳細資料](https://archive.is/20120630212335/home.comcast.net/~eoraptor/Carnosauria.htm%23Mapusaurusroseae)

[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:鯊齒龍科](../Category/鯊齒龍科.md "wikilink")

1.
2.
3.  Associated Press (2006). Details Revealed About Huge Dinosaurs. ABC
    News US.
    [1](http://abcnews.go.com/Technology/wireStory?id=1852246&page=1)

4.  Coria R. A. & Currie P. J. (2006). A new carcharodontosaurid
    (Dinosauria, Theropoda) from the Upper Cretaceous of Argentina.
    *Geodiversitas* 28(1):71-118.
    [PDF](http://www.mnhn.fr/museum/front/medias/publication/7653_g06n1a4.pdf)

5.
6.