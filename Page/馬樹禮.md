**馬樹禮**（），[中華民國政治人物](../Page/中華民國.md "wikilink")、[外交官](../Page/外交官.md "wikilink")、知日派，[中華民國與](../Page/中華民國.md "wikilink")[日本斷交後的首任駐日大使](../Page/日本.md "wikilink")。

[日本](../Page/日本.md "wikilink")[明治大学政治系](../Page/明治大学.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")[马尼拉](../Page/马尼拉.md "wikilink")[圣托马斯大学政治外交系毕业](../Page/聖道頓馬士大學.md "wikilink")。中央训练团党政班第5期、国防研究院第6期结业。民国37年2月，参加上海市选区的[立法委员选举并当选](../Page/立法委员.md "wikilink")。1982年獲頒明治大学[榮譽博士](../Page/榮譽博士.md "wikilink")。早年旅居南洋一帶（[东南亚](../Page/东南亚.md "wikilink")），曾在[马来亚](../Page/马来亚.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律宾等地担任](../Page/菲律宾.md "wikilink")「侨报」编辑、总编辑（如印尼[中華商報社長](../Page/中華商報.md "wikilink")），以及「侨校」教员、主任、校长等职（如馬來亞[柔佛](../Page/柔佛.md "wikilink")[新山](../Page/新山.md "wikilink")[寬柔學校校長](../Page/寬柔中學.md "wikilink")）。

曾任[臺北駐日本經濟文化代表處代表](../Page/臺北駐日本經濟文化代表處.md "wikilink")（[亞東關係協會代表](../Page/亞東關係協會.md "wikilink")）、[中國國民黨秘書長](../Page/中國國民黨秘書長.md "wikilink")（其副秘書長為[宋楚瑜及](../Page/宋楚瑜.md "wikilink")[馬英九](../Page/馬英九.md "wikilink")）、[中國國民黨中央評議委員會主席團主席](../Page/中國國民黨中央評議委員會.md "wikilink")、[三民主義統一中國大同盟理事會主任委員](../Page/三民主義統一中國大同盟.md "wikilink")。

1987年7月卸任，翌年初当选「三民主义统一中国大同盟」主委。曾多次赴美与[中國大陆留学生](../Page/中國大陆.md "wikilink")、学者座谈，並曾邀请五位大陆留学生访臺。2000年4月曾回大陆地區[扫墓祭祖](../Page/扫墓.md "wikilink")\[1\]。

馬樹禮通曉多國語言，包括[英語](../Page/英語.md "wikilink")、[日語](../Page/日語.md "wikilink")、[马来語和](../Page/马来語.md "wikilink")[印尼语等](../Page/印尼语.md "wikilink")，岳父為[顧祝同](../Page/顧祝同.md "wikilink")。

馬樹禮於[2000年中華民國總統選舉中](../Page/2000年中華民國總統選舉.md "wikilink")，以國民黨黨員身分支持脫離國民黨獨立參選的[宋楚瑜](../Page/宋楚瑜.md "wikilink")，後因許多國民黨黨員反對處分馬樹禮而未遭[黨紀處分](../Page/黨紀.md "wikilink")\[2\]。

## 著作

  - 《反共抗俄基本論》翻譯為印尼文出版(Chiang Kai Sek, Dasar anti-komunis, Djakarta :
    Inmajority, 1956)
  - 《印尼商業年鑑》(雅加達：中華商報社，1955)
  - 《印尼的變與亂》(台北：海外出版社，1956)
  - 《印尼現局之分析》(手冊，1957)
  - 《印尼獨立運動史》(香港：新聞天地社，1957)
  - 《印尼政情與僑情》(演講稿，1966)
  - 《黨務與僑務之配合》([僑務委員會海外僑團工作幹部講習會講義](../Page/僑務委員會.md "wikilink")，1968)\[3\]。
  - 《使日十二年》（台北：[聯經出版事業公司](../Page/聯經出版事業公司.md "wikilink")，1997）

馬樹禮所編著《印尼獨立運動史》等書，被學界公認為以華文撰寫的印尼近代史經典之作\[4\]。

## 參考資料

|- |colspan="3" style="text-align:center;"| |-

[category:中華民國駐日本代表](../Page/category:中華民國駐日本代表.md "wikilink")
[category:中視董事長](../Page/category:中視董事長.md "wikilink")

[Category:中華民國總統府資政](../Category/中華民國總統府資政.md "wikilink")
[Category:第1屆正選立法委員](../Category/第1屆正選立法委員.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:中廣董事長](../Category/中廣董事長.md "wikilink")
[Category:台灣外省人支持統一者](../Category/台灣外省人支持統一者.md "wikilink")
[Category:明治大學校友](../Category/明治大學校友.md "wikilink")
[Category:聖道頓馬士大學校友](../Category/聖道頓馬士大學校友.md "wikilink")
[Category:江蘇裔台灣人](../Category/江蘇裔台灣人.md "wikilink")
[Category:淮安人](../Category/淮安人.md "wikilink")
[S樹](../Category/馬姓.md "wikilink")

1.  [马树礼](http://218.4.83.214:8089/datalib/2003/Character/DL/DL-20031227130647/view?searchterm=None)
2.  [國民黨清黨今開鍘
    首波逾50人](http://forums.chinatimes.com/report/presidentreview/89407p40.htm)
3.  [誰是台灣的「印尼通」？
    記馬樹禮先生在印尼](http://kamadevas.pixnet.net/blog/post/202250927-%E8%AA%B0%E6%98%AF%E5%8F%B0%E7%81%A3%E7%9A%84%E3%80%8C%E5%8D%B0%E5%B0%BC%E9%80%9A%E3%80%8D%EF%BC%9F-%E8%A8%98%E9%A6%AC%E6%A8%B9%E7%A6%AE%E5%85%88%E7%94%9F%E5%9C%A8%E5%8D%B0)
4.  [前印尼華文報人‧徐競先遺著加拿大再版](http://indonesia.sinchew.com.my/node/43575?tid=6)