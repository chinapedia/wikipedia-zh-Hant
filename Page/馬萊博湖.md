**馬萊博湖**（[法语](../Page/法语.md "wikilink"):，又称**恩戈比拉湖**，法语：
或**斯坦利潭**，法语：）是[刚果河下游的](../Page/刚果河.md "wikilink")[湖泊](../Page/湖泊.md "wikilink")，位于[刚果民主共和国与](../Page/刚果民主共和国.md "wikilink")[刚果共和国之间](../Page/刚果共和国.md "wikilink")，湖畔西北边有[布拉柴维尔](../Page/布拉柴维尔.md "wikilink")，对岸是[金沙萨](../Page/金沙萨.md "wikilink")。湖中有[班慕岛](../Page/班慕岛.md "wikilink")（，又作
）。

刚果河在马莱博湖以下是[李文斯頓瀑布](../Page/李文斯頓瀑布.md "wikilink")，瀑布过大落差阻断河上船只航行，马莱博湖便是刚果河最后可航行的一段。

## 名字

马莱博湖旧称**斯坦利潭**，是以19世纪[英裔](../Page/英国.md "wikilink")[美籍](../Page/美国.md "wikilink")[探险家与](../Page/探险家.md "wikilink")[记者](../Page/记者.md "wikilink")[亨利·莫顿·史丹利命名](../Page/亨利·莫顿·史丹利.md "wikilink")。后来改名为马莱博湖。

“马莱博”来自当地[方言](../Page/方言.md "wikilink")
lilebo一字的复数malebo（意为“[糖棕](../Page/糖棕.md "wikilink")”，学名*Borassus
flabellifer*），
此名得自于湖畔和湖中岛屿间所茂盛的糖棕树林。而湖名中的“”一字却源自[英语](../Page/英语.md "wikilink")，本意为“水池”或“水潭”。

另外，在[殖民时代以前](../Page/殖民地.md "wikilink")，当地人称此湖为Nkunda。

## 參見

  - [金夏沙](../Page/金夏沙.md "wikilink")
  - [布拉柴维尔](../Page/布拉柴维尔.md "wikilink")
  - [刚果河](../Page/刚果河.md "wikilink")

## 参考

[Category:非洲跨國湖泊](../Category/非洲跨國湖泊.md "wikilink")
[Category:刚果共和国湖泊](../Category/刚果共和国湖泊.md "wikilink")
[Category:刚果民主共和国湖泊](../Category/刚果民主共和国湖泊.md "wikilink")
[Category:剛果民主共和國-剛果共和國邊界](../Category/剛果民主共和國-剛果共和國邊界.md "wikilink")
[Category:剛果河](../Category/剛果河.md "wikilink")