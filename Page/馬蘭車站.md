**馬蘭車站**位於[台灣](../Page/台灣.md "wikilink")[臺東縣](../Page/臺東縣.md "wikilink")[臺東市](../Page/臺東市.md "wikilink")，曾為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[臺東線](../Page/臺東線.md "wikilink")（現已廢止）的[鐵路車站](../Page/鐵路車站.md "wikilink")。

## 車站構造

  - 島式月台一座。

## 利用狀況

  - 已廢止。

## 車站週邊

  - [台東糖廠](../Page/台東糖廠.md "wikilink")
  - 台東縣衛生局台東市衛生所
  - 台東縣光明國民小學
  - 中油公司馬蘭油庫（[南迴鐵路通車後](../Page/南迴鐵路.md "wikilink")，中油公司從[楠梓車站北邊的橋頭油庫裝填油罐車由台鐵運送至馬蘭油庫](../Page/楠梓車站.md "wikilink")）

## 歷史

  - 1922年4月20日：設「**馬蘭驛**」（）。
  - 1982年5月：車站大樓重建。\[1\]
  - 2001年6月1日：停辦客運業務。
  - 2001年8月1日：廢止。
  - 曾經由鐵路局租給民間業者做為餐廳，目前已歇業\[2\]。現為自行車道休息站\[3\]。

## 參考文獻

<references/>

  - [裁撤小站巡禮
    馬蘭站](http://www.railway.gov.tw/Hualien-Transportation/CP.aspx?SN=14480)

## 鄰近車站

  - **東拓前營運模式**

<!-- end list -->

  - **東拓後營運模式**

[廢](../Category/台東線車站.md "wikilink")
[花](../Category/台灣鐵路廢站.md "wikilink")
[廢](../Category/台東縣鐵路車站.md "wikilink")
[Category:臺東市](../Category/臺東市.md "wikilink")
[Category:1922年启用的铁路车站](../Category/1922年启用的铁路车站.md "wikilink")
[Category:2001年關閉的鐵路車站](../Category/2001年關閉的鐵路車站.md "wikilink")

1.  {{Cite
    web|url=[http://a.domaindlx.com/spk2001/ma-lan.htm|title=馬蘭車站（驛站之旅](http://a.domaindlx.com/spk2001/ma-lan.htm%7Ctitle=馬蘭車站（驛站之旅)）\]
2.
3.  [電桿也成畫布
    馬蘭車站彩繪變身](http://www.libertytimes.com.tw/2014/new/mar/4/today-south1.htm)