[Pixelization_mosaic.jpg](https://zh.wikipedia.org/wiki/File:Pixelization_mosaic.jpg "fig:Pixelization_mosaic.jpg")
**马赛克技术**（，）是一種利用與[鑲嵌畫裝飾藝術](../Page/鑲嵌畫.md "wikilink")（）類似原理的[影像处理方法](../Page/影像处理.md "wikilink")，在[香港又稱](../Page/香港.md "wikilink")**打格仔**。此方法將影像特定區域的色階細節劣化並造成色塊打亂的效果，其目的是为了使另一个人无法辨认，同时用在影像處理時有時也稱為**碼賽克**、**打碼**（由單純音譯加入了[密碼的涵義](../Page/密碼.md "wikilink")）。由於常用於處理敏感影像，马赛克常常也影射了敏感影像本身，形成一種暗喻。

## 使用場合

### 物件

某些文件，如[車牌號碼](../Page/車牌.md "wikilink")、[信用卡](../Page/信用卡.md "wikilink")、[身份证中的私隱时打馬賽克](../Page/身份证.md "wikilink")，以免泄露。另一方面，也可作为样版展示，也會將不雅字眼以此處理。

另外台灣有禁止新聞使用廣告，政府又限制[置入性行銷等規定](../Page/置入性行銷.md "wikilink")，在台灣的新聞頻道中，若新聞片段有攝入廣告招牌會依法將之以馬賽克處理；甚至連吸菸的影像也要依照規定以馬賽克遮蔽，連在台灣播出的[ONE
PIECE (動畫)也不例外](../Page/ONE_PIECE_\(動畫\).md "wikilink")\[1\]\[2\]。

### 肖像

某些人，因为各种原因而不想露真面目，而在映像或照片中使用马赛克，常見對象如犯罪被害人與家屬、[嫌疑犯](../Page/嫌疑犯.md "wikilink")、[囚犯](../Page/囚犯.md "wikilink")、[戰俘等等](../Page/戰俘.md "wikilink")。總之只要有人有「馬賽克」請求，便可為他「馬賽克」。

### 色情影像

大多數國家法律會規定在[電影](../Page/電影.md "wikilink")、[錄影帶](../Page/錄影帶.md "wikilink")、[動漫及](../Page/動漫.md "wikilink")[電子遊戲等](../Page/電子遊戲.md "wikilink")[多媒體作品中](../Page/多媒體.md "wikilink")，需使用馬賽克或遮蓋、[霧化裸露的隱私部分](../Page/霧化.md "wikilink")，如[乳頭](../Page/乳頭.md "wikilink")（女性）、[陰毛](../Page/陰毛.md "wikilink")、[肛門及](../Page/肛門.md "wikilink")[生殖器等部位](../Page/生殖器.md "wikilink")，方可公開播映與發行。另外亦需以馬賽克遮蓋[髒話字眼](../Page/髒話.md "wikilink")。

[日本刑法第](../Page/日本刑法.md "wikilink")175條「猥瑣物頒佈罪」（），規定不能直接裸露隱私部分。日本[成人视频制造商在發片前](../Page/成人视频制造商.md "wikilink")，必須先將作品送交[映倫](../Page/映倫.md "wikilink")（）[審查](../Page/审查制度.md "wikilink")，要求[女演員必須已成年](../Page/AV女優.md "wikilink")，陰毛、肛門及生殖器等“重點位置”的-{zh-hans:录像;zh-hant:影像}-必須打[馬賽克](../Page/馬賽克_\(影像處理\).md "wikilink")，審核通過才允許上市\[3\]。

不過，一些业者在國外註冊法人規避[审查制度](../Page/审查制度.md "wikilink")，把未經審查的[色情片或](../Page/色情片.md "wikilink")[成人動畫外銷到管制較鬆脫的歐美第三國](../Page/成人動畫.md "wikilink")，再通過網路「進口」販售至日本。此類色情片又稱[无码片](../Page/无码视频.md "wikilink")（）或无修正（）。

### 網路地圖

在[網路地圖的](../Page/網路地圖.md "wikilink")[衛星地圖及](../Page/衛星地圖.md "wikilink")[街景地圖等功能中](../Page/街景地圖.md "wikilink")，畫面若涵蓋到特定設施（例如[軍事基地](../Page/軍事基地.md "wikilink")）時，地圖供應者會因應各地法律要求，將軍事基地所在區塊以馬賽克處理，或者降低局部解析度達到類似大範圍遮蔽的效果。如[Google地圖和](../Page/Google地圖.md "wikilink")[NAVER地圖對](../Page/NAVER地圖.md "wikilink")[首爾的](../Page/首爾.md "wikilink")[駐韓美軍](../Page/駐韓美軍.md "wikilink")[龍山基地](../Page/龍山基地.md "wikilink")，有類似的影像處理。

## 注释

## 参考资料

[Category:影像科技](../Category/影像科技.md "wikilink")
[Category:审查技术](../Category/审查技术.md "wikilink")

1.  <http://www.appledaily.com.tw/appledaily/article/headline/20100216/32305917/>
    《航海王》抽菸打馬賽克「很扯」 | 蘋果日報
2.  <http://ent.ltn.com.tw/news/breakingnews/332200>
    《航海王》抽菸馬賽克遭議　台視恢復原版播出 - 自由娛樂
3.