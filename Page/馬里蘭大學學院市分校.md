**馬里蘭大學學院市分校**（，英文简称UM、UMD、UMCP或Maryland），是一所在美国排名前列，并在全世界范围内拥有一定知名度的公立研究性大学。大学创建于1856年，位于美国东北部[马里兰州](../Page/马里兰州.md "wikilink")[大学公园市](../Page/大学公园市.md "wikilink")，是[马里兰大学系統的旗舰分校](../Page/马里兰大学系統.md "wikilink")，同时也是馬里蘭州以及华盛顿都會区最大的大學，也被認為是[公立常春藤院校之一](../Page/公立常春藤.md "wikilink")。马大首位华裔校长[陆道逵](../Page/陆道逵.md "wikilink")（Wallace
D. Loh）于2010年11月1日正式上任。

## 历史

[UMD_Morrill_Hall.JPG](https://zh.wikipedia.org/wiki/File:UMD_Morrill_Hall.JPG "fig:UMD_Morrill_Hall.JPG")

### 早期历史

1856年3月6日，后来成为马里兰大学的马里兰农业学院成立。1858年，巴尔的摩男爵的后裔Charles Benedict
Calvert用两万一千[美元在大学园购买了](../Page/美元.md "wikilink")420[英亩的Riverdale种植园提供给马里兰农业学院](../Page/英亩.md "wikilink")。同年Charles
Benedict
Calvert又把一些其[股票所得的资金提供给马里兰农业学院](../Page/股票.md "wikilink")。1859年10月6日，第一批包括Charles
Benedict Calvert的四个儿子在内的34名学生开始在马里兰农业学院学习。

1862年7月，马里兰农业学院授予了第一批学位。同月，[林肯签署了Morrill](../Page/林肯.md "wikilink")
Land拨款法案。这项法案旨在用联邦政府资金资助提供[农业](../Page/农业.md "wikilink")、[工程和](../Page/工程.md "wikilink")[军事教育的学校](../Page/军事.md "wikilink")。在马里兰立法院通过了Morrill
Land拨款法案后，马里兰农业学院在1864年2月成为受该法案支持的学校。

### [美国内战时期](../Page/美国内战.md "wikilink")

1864年4月，Ambrose E.
Burnside将军率美国第九陆军集团军的6000名北方士兵在马里兰农业学院的校园驻扎。在此期间士兵破坏了数百英尺长的木制篱笆去生火并试图烧毁一个石质谷仓。马里兰农业学院后来试图起诉联邦政府赔偿损失但不幸败诉。

1864年[夏天](../Page/夏天.md "wikilink")，Bradley T.
Johnson将军率领大约400名南方士兵在马里兰农业学院的校园驻扎。当时同情南方的校长Henry
Onderdonk对南方军队的到来表示了欢迎。

在内战期间经济问题迫使学校当局出售了300英亩的土地。但入学人数的锐减最终导致马里兰农业学院破产。随后马里兰农业学院的校园被用作男童预备学校。

### 战后阶段

美国内战结束后马里兰州立法院结束了马里兰农业学院的破产状态并获取了其一半的所有权。马里兰农业学院成为一所半公立学院。内战期间南方将军[罗伯特·李的儿子](../Page/罗伯特·李.md "wikilink")[喬治·華盛頓·卡斯蒂斯·李被任命为战后第一任校长](../Page/喬治·華盛頓·卡斯蒂斯·李.md "wikilink")。但是由于公众的不满，喬治·華盛頓·卡斯蒂斯·李拒绝了这个职位。1867年10月马里兰农业学院重新成立并招收了11名学生。在随后的6年里，马里兰农业学院偿还了所有的债务。在此后的二十年内，由于联邦政府投资的一个农业试验站的成立，马里兰农业学院开始成为一个研究机构。同期马里兰农业学院还在一系列领域获得了管理权，例如农场疾病控制，牧草检查；还建立了州气象局以及林业会议。

1888年马里兰农业学院开始了正式的校际篮球比赛，当年的对手有St. John's
College和[美国海军学院](../Page/美国海军学院.md "wikilink")。

1897年，第一个[兄弟会Phi](../Page/兄弟会.md "wikilink") Sigma Kappa进入了马里兰农业学院。

1898年Morrill Hall建立，这是马里兰大学仍然在使用的教学楼中历史最悠久的建筑。

### 1912年大火

1912年11月29日晚10点30分，正在举行[感恩节舞会的新行政楼阁楼发生火灾](../Page/感恩节.md "wikilink")。起火原因可能是电线短路。在场的大约80名学生全部安全逃生并排队传递水桶救火。但是由于当时的大风，学生宿舍，以及除Morrill
Hall之外的所有学术建筑都被引燃。尽管没有人伤亡，学校损失约25万美元，折合2005年汇率约5百万美元。校长Richard
Silvester辞职。当时很多人以为马里兰农业学院会关闭。

但是学生没有放弃。在寒假结束后除两人外其他全部学生回到了学校并坚持学校应继续正常教学。在学生宿舍建好之前，学生被安置在附近的居民家中，租金后来由学校支付。但是行政楼一直到1940年才重建。

在1912年学校的中心所在的位置建立了一个混凝土指南针，指南针上指出了当年被火烧毁的每一座建筑的方向。

### 近期历史

1916年马里兰州获取了马里兰农业学院的全部所有权并把学校更名为马里兰州立学院(Maryland State
College)。同年第一个女生入学，四年后姐妹会进入学校。1920年4月9日，马里兰州立学院同[巴尔的摩的一些职业学校合并成为马里兰大学](../Page/巴尔的摩.md "wikilink")(University
of
Maryland)。同年马里兰大学研究生院授予了第一个[哲学](../Page/哲学.md "wikilink")[博士学位](../Page/博士.md "wikilink")，学校学生总数超过了500人。

1951年第一个[非洲裔学生入学](../Page/非洲.md "wikilink")，当时学生总数已经接近10000人，其中约4000名女生。1957年校长Wilson
H.
Elkins发起了一个提高学校教学水平的运动。最终学校建立了一个学术考察计划。在学术考察计划实行的第一年，占当时学生总数18％的1550名学生面临开除。随后学校的教学水平显著提高。

1988年，马里兰大学系统成立，大学园校区被正式命名为马里兰大学。

1997年，马里兰人民大会(the Maryland General
Assembly)通过立法决定将马里兰大学學院市分校称为马里兰大学。需要指出的是其他名称中带有“马里兰大学”的学校并非马里兰大学學院市分校的分校。

2001年9月24日，[九一一袭击事件后](../Page/九一一袭击事件.md "wikilink")13天，一场[龙卷风横扫马里兰大学學院市](../Page/龙卷风.md "wikilink")，导致两名学生死亡，12栋建筑损坏。学校损失达到1500万美元。

2004年11月17日，马里兰大学与中国[南开大学签署协议在马里兰大学建立北美洲第一家](../Page/南开大学.md "wikilink")[孔子学院](../Page/孔子学院.md "wikilink")——[马里兰大学孔子学院](../Page/马里兰大学孔子学院.md "wikilink")\[1\]。

## 學術

马里兰大学部分老师获得过各种奖励和荣誉称号，其中，历史学教授 Ira
Berlin被美国总统任命为国家人权委员会顾问。通过学术研究，经济开发以及教育项目，马里兰大学研发、传播的知识、技术、发明、专利；促进了国家、地区经济的发展。目前，马里兰大学正和加州大学柏克萊分校、密西根大學、伊利诺大学、加州大学洛杉机分校，北卡罗莱纳大学建立伙伴关系，力争把該校建设成国际最著名公立研究型大学之一。

马里兰大学录取率为：39%\[2\]，被錄取新生的SAT中間分數：SAT閱讀：570 - 680 提供分数人數占总人数95%；SAT數學：600
- 700 提供分数人數占总人数95%\[3\]。

## 金刚背泥龟

1932年，Curly
Byrd提议把学校的[吉祥物定为马里兰州爬行动物](../Page/吉祥物.md "wikilink")[金刚背泥龟](../Page/金刚背泥龟.md "wikilink")(Diamondback
Terrapin，简称Terrapin或Diamondback)。吉祥物的名字为Testudo，意为有保护性的壳。1933年毕业生集资用青铜铸造了第一个Testudo的塑像，摆放在一号路旁边的Ritchie体育馆前。1947年[约翰·霍普金斯大学的学生把Testudo的塑像劫持到了约翰](../Page/约翰·霍普金斯大学.md "wikilink")·霍普金斯大学校园。大学园的学生迅速赶到巴尔的摩，围攻藏有Testudo的约翰·霍普金斯大学学生宿舍。最终有超过200名[防暴警察干涉](../Page/防暴警察.md "wikilink")。1949年，Testudo又被劫持到[弗吉尼亚大学](../Page/弗吉尼亚大学.md "wikilink")。1951年，Testudo被固定在地面上，但是仍然遭到其他学校学生的破坏。在1960年代，Testudo被移到了远离一号路的McKeldin图书馆前。马里兰大学的学生有在考试或其他重大事件前摸Testudo的[鼻子或者在Testudo前摆放食物](../Page/鼻子.md "wikilink")，鲜花等“贡品”冀希望得到好运的传统。

## 学生活动

### 金刚背泥龟报

金刚背泥龟报(The Diamondback)是马里兰大学学院市分校的独立学生报纸。成立与1910年，诞生之初这张报纸的名称为《三角报》(The
Triangle)，然后1921年为了致敬本地爬虫更名。在春季和秋季学期，这家报纸从周一至周五每天出版。它的发行量约为17000份、每年的广告收入超过100万美元。\[4\]

2005-2006学年，金刚背泥龟报获到[专业记者學会](../Page/专业记者學会.md "wikilink") ([The Society
of Professional
Journalists](../Page/:en:Society_of_Professional_Journalists.md "wikilink"))的优秀奖。该报被評為全美第三好的学生日报。
\[5\]

著名金刚背泥龟报记者包括HBO电台的《The Wire》的作家[David
Simon](../Page/:en:David_Simon\(writer\).md "wikilink")，前[纽约时报记者Jayson](../Page/纽约时报.md "wikilink")
Blair(金刚背泥龟报1996年的主编 )，ESPN 专栏作家[Norman
Chad](../Page/:en:Norman_Chad.md "wikilink") (1978年的主编)，漫画家[Aaron
McGruder](../Page/:en:Aaron_McGruder.md "wikilink") (他的漫画[The
Boondocks是在该报纸开始出版的](../Page/The_Boondocks.md "wikilink"))，和漫画家[Frank
Cho](../Page/Frank_Cho.md "wikilink")。McGruder毕业以后，漫画有了广泛成功，并有自己的电视节目。

## 学科分布

马里兰大学学院市分校下设13个学院，分别为

  - [工程学院](http://www.engr.umd.edu/)
  - [农业与自然资源学院](http://www.agnr.umd.edu/)
  - [建筑规划与保护学院](http://www.arch.umd.edu/)
  - [艺术与人文学院](http://www.arhu.umd.edu/)
  - [行为与社会科学学院](http://www.bsos.umd.edu/)
  - [計算機科学, 数学, 与自然科学学院](http://cmns.umd.edu/)
  - [教育学院](http://www.education.umd.edu/)
  - [公共衛生学院](http://www.sph.umd.edu/)
  - [信息学院](http://ischool.umd.edu/)
  - [新闻学院](http://www.journalism.umd.edu/)
  - [商学院 (史密斯商學院-Robert H. Smith School of
    Business)](http://www.smith.umd.edu/)
  - [公共政策学院](http://www.publicpolicy.umd.edu/)

## 语言屋

马里蘭大学的聖瑪莉堂(St. Mary's
Hall)又叫语言屋\[6\]，只容许学外语学生居住。这宿舍里面有十种语言，它们分别是：[阿拉伯语](../Page/阿拉伯语.md "wikilink")，[中文](../Page/汉语.md "wikilink")，[法语](../Page/法语.md "wikilink")，[德语](../Page/德语.md "wikilink")，[希伯来语](../Page/希伯来语.md "wikilink")，[意大利语](../Page/意大利语.md "wikilink")，[日语](../Page/日语.md "wikilink")，[波斯語](../Page/波斯語.md "wikilink")，[俄语和](../Page/俄语.md "wikilink")[西班牙语](../Page/西班牙语.md "wikilink")。

## 图书馆

马里兰大学学院市分校有八个图书馆，分别为

  - [McKeldin图书馆](http://www.lib.umd.edu/MCK/mckeldin.html)
    [McKeldin.jpg](https://zh.wikipedia.org/wiki/File:McKeldin.jpg "fig:McKeldin.jpg")
  - [艺术图书馆](https://web.archive.org/web/20050913212329/http://www.lib.umd.edu/ART/art.html)
  - [建筑图书馆](https://web.archive.org/web/20050913212759/http://www.lib.umd.edu/ARCH/architecture.html)
  - [工程与物理图书馆](http://www.lib.umd.edu/ENGIN/engin.html)
  - [Hornbake图书馆](http://www.lib.umd.edu/HBK/hornbake.html)
  - [表演艺术图书馆](http://www.lib.umd.edu/PAL/music.html)
  - [Shady
    Grove图书馆与媒体中心](https://web.archive.org/web/20050913212904/http://www.lib.umd.edu/shadygrove/)
  - [化学图书馆](http://www.lib.umd.edu/CHEM/chemistry.html)

## 体育

马里兰大学学院市分校属于
[十大联盟](../Page/十大联盟.md "wikilink")。马里兰大学曾经是[大西洋沿岸联盟的创始成员](../Page/大西洋沿岸联盟.md "wikilink")，但是在2014年马里兰大学离开大西洋沿岸联盟，转投十大联盟。马里兰大学学院市分校的知名运动项目包括[篮球](../Page/篮球.md "wikilink")，[橄榄球](../Page/橄榄球.md "wikilink")，[足球](../Page/足球.md "wikilink")，[长曲棍球等](../Page/长曲棍球.md "wikilink")。共获得过19次全国冠军。马里兰大学的[长曲棍球是全美国国领先的项目](../Page/长曲棍球.md "wikilink")，女子长曲棍球获得过10个全国冠军，并于1995至2001年保持七连冠。

最近获得全国冠军的项目有女子篮球(2006)，曲棍球(2005)，男子足球(2005)， 男子籃球(2002)。

### 传统对手

在男子[篮球方面](../Page/篮球.md "wikilink")，Duke大学是马里兰大学的传统对手。

在[橄榄球方面](../Page/橄榄球.md "wikilink")，[北卡州立大学](../Page/北卡州立大学.md "wikilink")，[弗吉尼亚大学和](../Page/弗吉尼亚大学.md "wikilink")[西弗吉尼亚大学是马里兰大学的传统对手](../Page/西弗吉尼亚大学.md "wikilink")。同[西弗吉尼亚大学之间的橄榄球比赛从](../Page/西弗吉尼亚大学.md "wikilink")1980年开始年年举行。[宾夕法尼亚州立大学在](../Page/宾夕法尼亚州立大学.md "wikilink")90年代以前同马里兰大学经常比赛，目前两个学校正在谈判准备重新开始两校之间的比赛。

马里兰大学和[约翰斯·霍普金斯大学是男子长曲棍球的传统对手](../Page/约翰斯·霍普金斯大学.md "wikilink")。

## 知名人士

### 校友

  - [Sergey
    Brin](../Page/:en:Sergey_Brin.md "wikilink")，[Google公司创始人之一](../Page/Google公司.md "wikilink")，1993年毕业于数学与計算機科学学院(計算機科学、数学与自然科学院)。
  - [Robert D. Briskman](../Page/:en:Robert_D._Briskman.md "wikilink"),
    [SIRIUS Satellite
    Radio创始人之一](../Page/:en:_SIRIUS_Satellite_Radio.md "wikilink")
  - [Joseph Hardiman](../Page/:en:Joseph_Hardiman.md "wikilink"),
    [納斯達克總裁](../Page/納斯達克.md "wikilink")
  - [Kevin Plank](../Page/:en:Kevin_Plank.md "wikilink"), [Under
    Armour運動品牌創始人](../Page/:en:Under_Armour.md "wikilink")。
  - [Carly
    Fiorina](../Page/:en:Carly_Fiorina.md "wikilink")，[惠普公司前CEO](../Page/惠普公司.md "wikilink")。
  - [Harvey
    Sanders](../Page/:en:Harvey_Sanders.md "wikilink")，[Nautica時尚品牌公司CEO](../Page/:en:Nautica.md "wikilink")/總裁/主席。
  - [Jim
    Walton](../Page/:en:Jim_Walton.md "wikilink")，[有线电视新闻网](../Page/有线电视新闻网.md "wikilink")([CNN](../Page/:en:CNN.md "wikilink"))总裁/CEO。
  - [Jim
    Henson](../Page/:en:Jim_Henson.md "wikilink")，[Muppets的创造者](../Page/:en:Muppets.md "wikilink")。
  - [Herbert
    Hauptman](../Page/:en:Herbert_Hauptman.md "wikilink")，1985年[诺贝尔化学奖得主](../Page/诺贝尔化学奖.md "wikilink")。
  - [Judith
    Resnik](../Page/:en:Judith_Resnik.md "wikilink")，[宇航员](../Page/宇航员.md "wikilink")，死于[挑战者号事故](../Page/挑战者号.md "wikilink")。
  - [William C.
    McCool](../Page/:en:William_C._McCool.md "wikilink")，宇航员，死于[哥伦比亚号事故](../Page/哥伦比亚号.md "wikilink")。
  - [Paul W. Richards](../Page/:en:Paul_W._Richards.md "wikilink")，宇航员。
  - [Michael
    Griffin](../Page/:en:Michael_Griffin.md "wikilink")，[美国](../Page/美国.md "wikilink")[NASA负责人](../Page/NASA.md "wikilink")。
  - [Gordon England](../Page/:en:Gordon_England.md "wikilink")，美国海军部长。
  - [Connie Chung](../Page/:en:Connie_Chung.md "wikilink")，宗毓華，華裔新聞主播。
  - [Raymond Davis,
    Jr.](../Page/:en:Raymond_Davis,_Jr..md "wikilink")，化学家，物理学家，1938年化学专业学士毕业，2002年诺贝尔物理学奖得主。
  - [Herbert A.
    Hauptman](../Page/:en:Herbert_A._Hauptman.md "wikilink")，数学家，1955年数学博士毕业，1985年诺贝尔化学奖得主。
  - [Charles
    Fefferman](../Page/:en:Charles_Fefferman.md "wikilink")，数学家，1978年[菲尔兹奖得主](../Page/菲尔兹奖.md "wikilink")。1966年数学及物理学士毕业，22岁获[芝加哥大学数学系正教授](../Page/芝加哥大学.md "wikilink")，为美国历来最年轻的正教授。
  - [海那波拉·瓜那瑞塔](../Page/海那波拉·瓜那瑞塔.md "wikilink")([Bhante Henepola
    Gunaratana](../Page/:en:Henepola_Gunaratana.md "wikilink"))，當代[內觀大師](../Page/內觀.md "wikilink")，[北美地區地位最高的](../Page/北美地區.md "wikilink")[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")[上座部佛教](../Page/上座部佛教.md "wikilink")[長老](../Page/長老.md "wikilink")。
  - [汤姆·麦克米伦](../Page/汤姆·麦克米伦.md "wikilink")，美国[NBA联盟的前职业](../Page/NBA.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，退役后成为[政治家](../Page/政治家.md "wikilink")
  - [李慶安](../Page/李慶安.md "wikilink")，台灣政治人物。
  - [马英九](../Page/马英九.md "wikilink")：[中华民国总统](../Page/中华民国.md "wikilink")，曾任马里兰大学法学院研究顾问。

<!-- end list -->

  - [秦泗钊](../Page/秦泗钊.md "wikilink")：1992年马里兰大学（College
    Park）获得化学工程博士学位，国际自动控制联合会（IFAC）2013年新当选的会士。
  - [葉望輝](../Page/葉望輝.md "wikilink")：美國共和黨員、前白宮官員

### 教职研究人员

  - [威廉·丹尼尔·菲利普斯](../Page/威廉·丹尼尔·菲利普斯.md "wikilink"), 物理教授,
    [諾貝爾物理學獎得主](../Page/諾貝爾物理學獎.md "wikilink")。
  - [托马斯·谢林](../Page/托马斯·克罗姆比·谢林.md "wikilink")(Thomas
    Shelling)，经济学家，公共政策学院教授。2005[诺贝尔经济学奖得主](../Page/诺贝尔经济学奖.md "wikilink")。
  - [丘宏達](../Page/丘宏達.md "wikilink")：馬里蘭大學法學教授，[中研院院士候選人](../Page/中研院.md "wikilink")

## 爭議事件

### 楊舒平講演風波

杨舒平講演風波是指2017年5月21日該校毕业典礼上，[中国大陆](../Page/中国大陆.md "wikilink")[留学生杨舒平在发表毕业演讲时](../Page/留学生.md "wikilink")，以[美国的学习](../Page/美国.md "wikilink")、生活的自身经历為主軸的演講所引發的政治風波。主要原因是其稱中國家鄉昆明空氣質量不佳\[7\]，并由此其提到并赞赏美国的[空气质量](../Page/空气质量.md "wikilink")、[言论自由以及人民的参政热情](../Page/言论自由.md "wikilink")，并将美国与中国大陆进行了对比。此講演內容引起中國大陸的留學生群體以及中國媒體和民眾的激烈反應，部分留學生並以激烈且帶有攻擊性的言語稱其为“以诋毁祖国的方式博眼球”、“是逃到美国的[脱北者](../Page/脱北者.md "wikilink")”。事後她在微博上進行了公開道歉。\[8\]\[9\]

## 參考文獻

## 外部連結

  - [马里兰大学学院市分校 The University of Maryland](http://www.umd.edu)
  - [金刚背泥龟报 The Diamondback](http://www.diamondbackonline.com/)

[Category:馬里蘭大學系統](../Category/馬里蘭大學系統.md "wikilink")
[Category:公立常春藤](../Category/公立常春藤.md "wikilink")
[Category:1856年創建的教育機構](../Category/1856年創建的教育機構.md "wikilink")
[馬里蘭大學學院市分校](../Category/馬里蘭大學學院市分校.md "wikilink")
[Category:十大聯盟](../Category/十大聯盟.md "wikilink")

1.
2.  [Collegeboard的马里兰大学介绍](http://collegesearch.collegeboard.com/search/CollegeDetail.jsp?collegeId=3633&profileId=0)
3.  [Collegeboard的馬里蘭大學介紹](http://collegesearch.collegeboard.com/search/CollegeDetail.jsp?collegeId=3633&profileId=6)
4.  [Case
    No. 02-1326](http://www.rcfp.org/news/documents/20020530rossignolv.html)
5.  [Society of Professional Journalists: Mark of Excellence
    Awards](http://www.spj.org/moe05.asp)
6.  <http://www.languages.umd.edu/lh/>
7.
8.
9.