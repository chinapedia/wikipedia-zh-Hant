**馬錢科**或**馬錢子科**（*Loganiaceae*）是[雙子葉植物綱](../Page/雙子葉植物綱.md "wikilink")，[龍膽目的一個科](../Page/龍膽目.md "wikilink")，有13屬。分佈於全世界的[熱帶地區](../Page/熱帶.md "wikilink")。

早期的分類方法在馬錢科內包含有29屬，某些分類系統（Takhtajan
System）將其細分為四個科：Strychnaceae、Antoniaceae、Spigeliaceae、Loganiaceae。

包括[馬錢屬的](../Page/馬錢屬.md "wikilink")[馬錢子在內](../Page/馬錢子.md "wikilink")，該科的許多種植物均有劇毒。\[1\]

## 屬

  - *Antonia*
  - *Bonyunia*
  - [蓬萊葛屬](../Page/蓬萊葛屬.md "wikilink")（*Gardneria*）
  - [髯管花属](../Page/髯管花属.md "wikilink")（*Geniostoma*）
  - *Labordia*
  - [姬苗属](../Page/姬苗属.md "wikilink")（*Mitrasacme*）
  - [度量草属](../Page/度量草属.md "wikilink")（*Mitreola*）
  - *Neuburgia*
  - *Norrisia*
  - *Spigelia*
  - [馬錢屬](../Page/馬錢屬.md "wikilink")（*Strychnos*）
  - *Usteria*

## 参考文献

[\*](../Category/馬錢科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.  Flowering Plants of the World by consultant editor [Vernon H.
    Heywood](../Page/Vernon_H._Heywood.md "wikilink"), 1978, [Oxford
    University Press](../Page/Oxford_University_Press.md "wikilink"),
    Walton Street, [Oxford](../Page/Oxford.md "wikilink") OX2 6DP,
    [England](../Page/England.md "wikilink"),