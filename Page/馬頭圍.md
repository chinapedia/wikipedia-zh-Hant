**馬頭圍**（）位於[香港](../Page/香港.md "wikilink")[九龍城區的中部](../Page/九龍城區.md "wikilink")，[九龍城以南](../Page/九龍城.md "wikilink")，[靠背壟北面](../Page/靠背壟.md "wikilink")，以[太子道西及](../Page/太子道西.md "wikilink")[馬頭涌道為界](../Page/馬頭涌道.md "wikilink")，主要為[住宅區](../Page/住宅.md "wikilink")。

## 歷史

[Kai_Tak_Bridge_1875.jpg](https://zh.wikipedia.org/wiki/File:Kai_Tak_Bridge_1875.jpg "fig:Kai_Tak_Bridge_1875.jpg")（攝於1875年）\]\]

馬頭圍原名**古瑾圍**，因為古時[九龍寨城附近的](../Page/九龍寨城.md "wikilink")[龍津碼頭而得名](../Page/龍津碼頭.md "wikilink")「碼頭圍」，隨後才出現「馬頭圍」的寫法。\[1\]\[2\]位置即現今的[聖德肋撒醫院旁的](../Page/聖德肋撒醫院.md "wikilink")[露明道公園對面](../Page/露明道公園.md "wikilink")。而南部[二皇殿山下設有](../Page/二皇殿山.md "wikilink")[二皇殿村](../Page/二皇殿村.md "wikilink")。[二皇殿村清拆後則在附近興建](../Page/二皇殿村.md "wikilink")[馬頭圍邨和](../Page/馬頭圍邨.md "wikilink")[真善美邨](../Page/真善美邨.md "wikilink")。

馬頭圍亦與[南宋皇帝有關聯](../Page/南宋.md "wikilink")。南宋末年，宋朝朝廷被逼在海上流亡。1277年，他們抵達[九龍半島近馬頭圍一帶](../Page/九龍半島.md "wikilink")，短住半年。後人為紀念[宋帝昰曾經來過](../Page/宋帝昰.md "wikilink")，便在東面[馬頭涌](../Page/馬頭涌.md "wikilink")[聖山上他休息過的一塊大石上刻上](../Page/聖山.md "wikilink")「[宋王臺](../Page/宋王臺.md "wikilink")」三字。該石於[香港日佔時期被](../Page/香港日佔時期.md "wikilink")[日軍所毀](../Page/日軍.md "wikilink")，用作擴建[啓德機場之用](../Page/啓德機場.md "wikilink")，不過現時於宋王臺公園的宋王臺石並非仿製品，而是戰後由被毀原石中撿出刻有「宋王臺」三字的殘石修葺而成。

## 大型私人／居屋屋苑

  - [嘉景花園](../Page/嘉景花園.md "wikilink")
  - [懿薈](../Page/懿薈.md "wikilink")
  - [雅閣花園](../Page/雅閣花園.md "wikilink")
  - [君柏](../Page/君柏.md "wikilink")

## 公共屋邨

  - [馬頭圍邨](../Page/馬頭圍邨.md "wikilink")
  - [真善美邨](../Page/真善美邨.md "wikilink")

## 公共/社區設施

  - [九龍城裁判法院](../Page/九龍城裁判法院.md "wikilink")
  - [九龍城警署](../Page/九龍城警署.md "wikilink")
  - [靈光診所](../Page/靈光診所.md "wikilink")
  - [馬頭涌消防局](../Page/馬頭涌消防局.md "wikilink")
  - [宋王臺公園](../Page/宋王臺公園.md "wikilink")
  - [馬頭圍道遊樂場](../Page/馬頭圍道遊樂場.md "wikilink")
  - [聖德肋撒醫院](../Page/聖德肋撒醫院.md "wikilink")
  - [播道醫院](../Page/播道醫院.md "wikilink")

### 學校

  - [協恩中學](../Page/協恩中學.md "wikilink")
  - [中華基督教會基道中學](../Page/中華基督教會基道中學.md "wikilink")
  - [馬頭涌官立小學](../Page/馬頭涌官立小學.md "wikilink")
  - [香港培道小學](../Page/香港培道小學.md "wikilink")
  - [新亞中學](../Page/新亞中學.md "wikilink")
  - [聖三一堂小學](../Page/聖三一堂小學.md "wikilink")
  - [保良局林文燦英文小學](../Page/保良局林文燦英文小學.md "wikilink")
  - [香港宣道會劉平齋紀念國際學校](../Page/香港宣道會劉平齋紀念國際學校.md "wikilink")
  - [獻主會聖母院書院](../Page/聖母院書院.md "wikilink")
  - [閩光書院](../Page/閩光書院.md "wikilink")
  - [鄧鏡波學校](../Page/鄧鏡波學校.md "wikilink")
  - [英皇佐治五世學校](../Page/英皇佐治五世學校.md "wikilink")

## 著名地點

  - [宋王臺](../Page/宋王臺.md "wikilink")
  - [聖三一座堂](../Page/聖三一座堂.md "wikilink")
  - [上帝古廟](../Page/上帝古廟.md "wikilink")
  - [古瑾圍](../Page/古瑾圍.md "wikilink")

## 交通

### 主要交通幹道

  - [馬頭圍道](../Page/馬頭圍道.md "wikilink")/[馬頭涌道](../Page/馬頭涌道.md "wikilink")/[馬頭角道](../Page/馬頭角道.md "wikilink")
  - [太子道](../Page/太子道西.md "wikilink")
  - [亞皆老街](../Page/亞皆老街.md "wikilink")

### 其他街道

  - [天光道](../Page/天光道.md "wikilink")
  - [富寧街](../Page/富寧街.md "wikilink")
  - [露明道](../Page/露明道.md "wikilink")

### 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{屯馬綫色彩}}">█</font>[屯馬綫](../Page/屯馬綫.md "wikilink")：[宋皇臺站](../Page/宋皇臺站.md "wikilink")（預計2019年通車）\[3\]

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [盛德街巴士總站](../Page/盛德街巴士總站.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - 紅色公共小巴

</div>

</div>

## 區議會

根據[九龍城區選區分界圖](../Page/九龍城區.md "wikilink")，馬頭圍橫跨兩個選區：馬頭圍和太子

|     |                         |
| --- | ----------------------- |
| 選區  | 屬於馬頭圍的範圍                |
| 馬頭圍 | 所有                      |
| 太子  | 太子道西、露明道、亞皆老街、喇沙利道之間的範圍 |
|     |                         |

## 參看

  - [九龍城區](../Page/九龍城區.md "wikilink")
  - [九龍城](../Page/九龍城.md "wikilink")
  - [土瓜灣](../Page/土瓜灣.md "wikilink")
  - [龍津石橋](../Page/龍津石橋.md "wikilink")
  - [馬頭角](../Page/馬頭角.md "wikilink")
  - [馬頭涌](../Page/馬頭涌.md "wikilink")
  - [啟德機場](../Page/啟德機場.md "wikilink")

## 參考資料

<references />

{{-}}

{{-}}

[Category:馬頭圍](../Category/馬頭圍.md "wikilink")
[Category:九龍城區](../Category/九龍城區.md "wikilink")

1.  《九龍街道命名考源》梁濤 著，第66頁，市政局出版，1993年
2.  《趣談九龍街道》 爾東 著，第189-191頁，明報出版社，2004年11月，ISBN 962-8871-46-3
3.  [九龍城區區議會沙中綫區議會諮詢文件](http://www.mtr.com.hk/chi/projects/images/KCDC%20Paper_final.pdf)，[港鐵公司](../Page/港鐵公司.md "wikilink")，2009年6月1日