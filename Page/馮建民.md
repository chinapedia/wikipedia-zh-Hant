**馮建民**<span style="font-size:smaller;">，[PMSM](../Page/榮譽獎章.md "wikilink")</span>（），已故[香港警務人員](../Page/香港警察.md "wikilink")，階至[總警司](../Page/總警司.md "wikilink")。

## 生平

馮建民於16歲（1973年）時在一所[瑪利諾中學完成中四](../Page/瑪利諾中學.md "wikilink")\[1\]後因一股當警察的衝動驅使他放棄了學業，加入第一屆的[警察少年訓練學校](../Page/警察少年訓練學校.md "wikilink")，次年[畢業](../Page/畢業.md "wikilink")，並以優異成績完成[香港中學會考課程](../Page/香港中學會考.md "wikilink")\[2\]。他在[香港仔警察訓練學校時獲得俗稱](../Page/香港仔.md "wikilink")「銀雞頭」的「最佳結業學員」獎\[3\]。後來晉升督察時又獲得「榮譽警棍獎暨施禮榮盾」\[4\]。馮建民於1974年至1979年的5年間，分別駐守過[毒品調查科](../Page/毒品調查科.md "wikilink")、[尖沙咀警區及](../Page/尖沙咀警區.md "wikilink")[警察機動部隊](../Page/警察機動部隊.md "wikilink")。

不到21歲，就獲提拔晉升為[警長](../Page/警長.md "wikilink")，並且出任少年警察訓練學校[教官](../Page/教官.md "wikilink")。於1983年，升為[督察](../Page/督察.md "wikilink")，三年後升至[總督察](../Page/總督察.md "wikilink")；於2003年，升為總警司，為[有組織罪案及三合會調查科](../Page/有組織罪案及三合會調查科.md "wikilink")[主管](../Page/主管.md "wikilink")。期間，坊間稱他為「**O記一哥**」。上任後，馮建民對[季炳雄案極為重視](../Page/季炳雄.md "wikilink")，並且派員[跟蹤其](../Page/跟蹤.md "wikilink")[同黨](../Page/同黨.md "wikilink")[吳振強](../Page/吳振強.md "wikilink")。警隊後憑吳振強與外界的[電話聯絡](../Page/電話.md "wikilink")，成功勾出季炳雄的行蹤，最終成功緝獲頭號[通緝犯季炳雄](../Page/通緝犯.md "wikilink")。此外，他亦成功在名為「火苗」的掃黃行動中，成功消滅了一個年賺逾億，操縱近千名[妓女的大型賣淫集團](../Page/妓女.md "wikilink")，風頭一時無兩。

馮建民曾經獲得邀請到[英國](../Page/英國.md "wikilink")[布林斯山英國警察訓練及發展中心](../Page/布林斯山.md "wikilink")，在一個海外高級警務人員國際策略領袖課程中作專題演講，講解[亞太區](../Page/亞太區.md "wikilink")[跨境罪行的情況](../Page/跨境罪行.md "wikilink")。他亦曾接受布林斯山英國警察訓練學校以及[美國](../Page/美國.md "wikilink")[聯邦調查局的訓練](../Page/聯邦調查局.md "wikilink")\[5\]，是警隊中極少數可以被保送到後者參與訓練的警務人員\[6\]。

2005年末，他被指在「[Ba叔案](../Page/陳達志.md "wikilink")」中收受賄款，被[廉政公署調查](../Page/廉政公署_\(香港\).md "wikilink")，被警隊暫止一切的職務，假後被調往[總部](../Page/總部.md "wikilink")[支援課](../Page/支援課.md "wikilink")，負責內勤事務。2006年3月8日下午，他被發現半[昏迷於](../Page/昏迷.md "wikilink")[大埔](../Page/大埔_\(香港\).md "wikilink")[汀角路一間](../Page/汀角路.md "wikilink")[村屋內](../Page/村屋.md "wikilink")，懷疑曾經企圖[自殺](../Page/自殺.md "wikilink")。[警察在現場發現有](../Page/警察.md "wikilink")[燒炭的痕跡](../Page/燒炭.md "wikilink")，並且發現有[藥丸](../Page/藥丸.md "wikilink")，但是沒有[遺書](../Page/遺書.md "wikilink")。馮建民被送往[那打素醫院](../Page/那打素醫院.md "wikilink")，情況嚴重但無生命危險，警方將案為企圖自殺案。2006年9月8日下午，馮建民在[葵涌](../Page/葵涌.md "wikilink")[祖堯邨](../Page/祖堯邨.md "wikilink")[墮樓身亡](../Page/墮樓.md "wikilink")，\[7\]於9月29日，在[世界殯儀館設靈](../Page/世界殯儀館.md "wikilink")。

## 獲頒榮譽

  - （1997年）香港警察長期服務獎章
  - （1999年）香港警察長期服務獎章加敘第一勳扣
  - （2003年）香港警察榮譽獎章
  - （2004年）香港警察長期服務獎章加敘第二勳扣

## 參考來源

## 外部連結

  - [馮建民出席英國課程-警聲](http://www.info.gov.hk/police/offbeat/777/chi/n03.htm)
  - [馮建民與一些同事皆熱中於樓宇買賣投資，在1997年樓市高峰時，以千多萬港元](http://hk.news.yahoo.com/060908/12/1sqqn.html)
    購入[華景山莊](../Page/華景山莊.md "wikilink")\]

[J](../Category/馮姓.md "wikilink")
[Category:香港總警司](../Category/香港總警司.md "wikilink")
[Category:香港自殺人物](../Category/香港自殺人物.md "wikilink")
[Category:从高空跳下自杀身亡者](../Category/从高空跳下自杀身亡者.md "wikilink")
[Category:瑪利諾中學校友](../Category/瑪利諾中學校友.md "wikilink")

1.  [瑪利諾中學 30周年校慶特刊，第40頁，1996年](http://www.mssch.edu.hk/publication/30th/img48.htm)

2.
3.
4.
5.
6.
7.  [明報即時新聞網](http://www.mpinews.com/htm/INews/20060908/gb51512c.htm)
    〔2006年9月8日〕