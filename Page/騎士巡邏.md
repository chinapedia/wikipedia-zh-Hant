**騎士巡邏**（）是指在按照[国际象棋中](../Page/国际象棋.md "wikilink")[骑士的规定走法走遍整个](../Page/马_\(国际象棋\).md "wikilink")[棋盘的每一个方格](../Page/棋盘.md "wikilink")，而且每个网格只能夠经过一次。假若騎士能夠從走回到最初位置，則稱此巡邏為「封閉巡邏」，否則，稱為「開巡邏」。對於8\*8棋盤，一共有26,534,728,821,064種封閉巡邏，但是到底有多少種開巡邏仍然未知。\[1\]\[2\]\[3\]

由骑士巡逻引申出了一个著名的数学问题
：**骑士巡逻问题**--找出所有的骑士巡逻路径。编写一个程序来找出骑士巡逻路径经常在计算机系的学生的练习中出现。骑士巡逻问题的变种包括各种尺寸的棋盘甚至非正方形的棋盘。

## 历史

[Turk-knights-tour.svg](https://zh.wikipedia.org/wiki/File:Turk-knights-tour.svg "fig:Turk-knights-tour.svg")执行的骑士巡逻。由于其路线是一条闭路，因此从棋盘上任何一点开始都能完成巡逻。\[4\]\]\]
已知的最早的骑士巡逻问题可以追溯到[九世紀的古印度](../Page/九世紀.md "wikilink")[恰圖蘭卡](../Page/恰圖蘭卡.md "wikilink")。\[5\]

[欧拉是最早研究骑士巡逻的数学家中的一员](../Page/欧拉.md "wikilink")，而H. C. von
Warnsdorff在1823年提出了第一个系统化解决骑士巡逻问题的方法--Warnsdorff规则。

在20世纪，一批的作家将这个问题用在了其它的地方。最明显的例子：的小说**的章节顺序就是按照棋盘的骑士巡逻路径来编排的。在[2010年国际象棋世界冠军对抗赛的第六场比赛中](../Page/2010年国际象棋世界冠军对抗赛.md "wikilink")，棋手[阿南德连续](../Page/维斯瓦纳坦·阿南德.md "wikilink")13次移动骑士（使用了两个骑士），在线评论员打趣地说阿南德试图在游戏过程中解决骑士巡逻问题。

## 实质

[Knight's_graph_showing_number_of_possible_moves.svg](https://zh.wikipedia.org/wiki/File:Knight's_graph_showing_number_of_possible_moves.svg "fig:Knight's_graph_showing_number_of_possible_moves.svg")
骑士巡逻问题实际上是[哈密顿路径问题的一种特殊形式](../Page/哈密顿路径问题.md "wikilink")，寻找骑士巡逻的闭巡逻路径的个数实际上也是[哈密顿循环问题的一种特殊形式](../Page/哈密顿路径问题.md "wikilink")。但是和一般的[哈密顿路径问题不同](../Page/哈密顿路径问题.md "wikilink")，骑士巡逻问题可以在[线性时间内解决](../Page/线性时间.md "wikilink")。\[6\]

## 路径的个数

  - 在一个的棋盘中，有26,534,728,821,064中[有向封闭巡逻路径](../Page/有向.md "wikilink")（相互对称的巡逻路径被视为不同的巡逻路径）。\[7\]\[8\]
    。

  - 在的棋盘中，共有9862个闭巡逻。\[9\]

  - 棋盘中开巡逻的个数仍然未知。对于\(n\times n\)（*n*=1，2……）的棋盘中开巡逻的个数是：

<!-- end list -->

  -
    1, 0, 0, 0, 1728, 6637920, 165575218320,……（）

<!-- end list -->

  - Schwenk证明了，除了以下3種情況外，任何的（m\(\le\)n）棋盘都至少有1个闭巡逻，。\[10\]

<!-- end list -->

1.  m和n都为奇数
2.  m= 1, 2, 4
3.  m= 3且n= 4, 6, 8

<!-- end list -->

  - Cull和Conrad证明了对于任何一个（5\(\le\)m\(\le\)n）棋盘，至少有一个（可能是开巡逻）骑士巡逻路径。\[11\]\[12\]

## 解决方法

[Knight's_Tour_24x24.svg](https://zh.wikipedia.org/wiki/File:Knight's_Tour_24x24.svg "fig:Knight's_Tour_24x24.svg")的方法在的棋盘中寻找到的一条闭巡逻。\]\]
借助计算机的帮助，人们已经发现了很多种寻找骑士巡逻路径的方法。其中一部分依靠一些计算机[算法](../Page/算法.md "wikilink"),而另外一些则依靠[启发法](../Page/启发法.md "wikilink")。

### 穷举法

用[穷举法来寻找骑士巡逻路径适用于格数较小的棋盘](../Page/暴力搜索.md "wikilink")，因为当方格数过多时，可能的路径过多。例如，8×8棋盘中大约有4×10^51种可能的路径。\[13\]如此大的运算量已经超出了现代计算机的运算能力。

### 分治法

利用[分治法将棋盘分成很多小块](../Page/分治法.md "wikilink")，计算出每一小块中的所有可能路径，然后将这些小块合并再计算所有可能的路径。

### 人工神经网络方法

骑士巡逻问题同样可以使用[人工神经网络来解决](../Page/人工神经网络.md "wikilink")。\[14\]

### Warnsdorff规则

Warnsdorff规则指在所有可走且未经过的方格中，马只可能走这样一个方格：从该方格出发,马能跳的方格数最少；如果可跳的方格数相等，则从当前位置看,方格序号小的优先。依照这一规则往往可以找到一条路径但是并不一定能够成功。

## 參考資料

## 外部連結

  - [Knight's Tour Notes](http://www.ktn.freeuk.com/)
  - [Knight's
    Tour](https://web.archive.org/web/20051219005826/http://www.borderschess.org/KnightTour.htm)(Javascript)
  - [JAVA：Knight's tour](http://episte.math.ntu.edu.tw/java/jav_knight/)

[Category:數學遊戲](../Category/數學遊戲.md "wikilink")
[Category:图论](../Category/图论.md "wikilink")
[Category:图算法](../Category/图算法.md "wikilink")
[Category:数学问题](../Category/数学问题.md "wikilink")
[Category:國際象棋棋謎](../Category/國際象棋棋謎.md "wikilink")

1.   **Remark:** The authors later
    [admitted](http://www.combinatorics.org/Volume_3/Comments/v3i1r5.html)
    that the announced number is incorrect. According to McKay's report,
    the correct number is 13,267,364,410,532 and this number is repeated
    in Wegener's 2000 book.
2.
3.
4.  Standage, 30–31.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14. Y. Takefuji, K. C. Lee. "Neural network computing for knight's tour
    problems." *Neurocomputing*, 4(5):249–254, 1992.