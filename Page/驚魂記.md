是一部1960年的美国驚悚[恐怖電影](../Page/恐怖電影.md "wikilink")，由[亞弗列德·希區考克执导](../Page/亞弗列德·希區考克.md "wikilink")，[安東尼·柏金斯](../Page/安東尼·柏金斯.md "wikilink")、[珍妮·李](../Page/珍妮特·利.md "wikilink")、和主演。担任编剧，改编了的，小说的灵感来源于美國[威斯康辛州殺手兼](../Page/威斯康辛州.md "wikilink")[盗墓者](../Page/盗墓.md "wikilink")[艾德·蓋恩的罪行](../Page/艾德·蓋恩.md "wikilink")\[1\]。

影片讲述了一位贪污了老板的巨款后逃跑至一家偏僻汽車旅館的秘書，和患心理疾病的旅馆主人相遇后发生的一系列事件\[2\]。该片的拍摄在当时被认为背离了希区柯克的前作《[西北偏北](../Page/西北偏北.md "wikilink")》，因为它是黑白片，而且预算很低，只用了电视剧的工作人员。《惊魂记》一开始收到的评价褒贬不一，但出色的票房表现促使影评人重新看待它，最终收获了压倒性的好评和4项[奥斯卡奖提名](../Page/奥斯卡奖.md "wikilink")，包括利的[最佳女配角奖和希区柯克的](../Page/奥斯卡最佳女配角奖.md "wikilink")[最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")。

本片现在被視為希區考克的最佳作品之一\[3\]，並且被全球影评人和学者赞誉为体现电影艺术的杰作。它被视为影史最佳电影之一，在美国提高了公众对电影中的暴力、性和异常行为的接受度\[4\]。希区柯克于1980年去世后，[环球影业开始制作相关作品](../Page/环球影业.md "wikilink")：三部、一部、一部[电视电影和一部](../Page/贝茨汽车旅馆.md "wikilink")[电视剧](../Page/贝茨汽车旅馆_\(电视剧\).md "wikilink")。1992年，本片因其在“文化、历史和审美方面的显著成就”，被[美国国会图书馆列入](../Page/國會圖書館_\(美國\).md "wikilink")[国家电影登记部下属的国家电影保存委员会保护电影名单](../Page/国家电影登记部.md "wikilink")。

## 演員

  - **[珍妮·李](../Page/珍妮特·利.md "wikilink")**…… 飾 瑪麗安·克萊恩(Marion Crane)
  - **[安東尼·柏金斯](../Page/安東尼·柏金斯.md "wikilink")**…… 飾 諾曼·貝茲(Norman Bates)
  - **[馬丁·巴山姆](../Page/馬丁·巴山姆.md "wikilink")**…… 飾 米爾頓·亞伯蓋茲偵探(Detective
    Milton Arbogast)
  - **[約翰·嘉文](../Page/約翰·嘉文.md "wikilink")**…… 飾 山姆·魯米斯(Sam Loomis)
  - **[薇拉·麥爾斯](../Page/薇拉·麥爾斯.md "wikilink")**…… 飾 萊拉·克萊恩(Lila Crane)
  - **[西蒙·奧克蘭](../Page/西蒙·奧克蘭.md "wikilink")**…… 飾 佛瑞德·里奇蒙醫生(Dr. Fred
    Richmond)
  - **[約翰·麥金太爾](../Page/約翰·麥金太爾.md "wikilink")**…… 飾 艾爾·錢伯斯警長(Sheriff Al
    Chambers)
  - **[法蘭克·亞伯森](../Page/法蘭克·亞伯森.md "wikilink")**…… 飾 湯姆·卡西迪(Tom Cassidy)
  - **[派翠西亞·希區考克](../Page/派翠西亞·希區考克.md "wikilink")**…… 飾 卡洛琳(Caroline)
  - **[泰德·奈特](../Page/泰德·奈特.md "wikilink")**…… 飾 警察(Policeman)

## 淋浴場景

影片中之淋浴場景被視為電影界最驚嚇的一幕，並引起後期許多研究和猜想。它從1959年12月17至23日拍攝。有人曾認為希區考克為求尖叫效果逼真，於該場景中使用冷水，但其後珍妮·李曾在許多場合多次否認。亦有人認為珍妮·李在拍攝該場景時並不知道會被謀殺，以至效果逼真，而之後珍妮·李更有一段長時間不敢在沒有人看管之下洗澡，此事亦只為虛構。還有人認為當時珍妮·李其實使用了替身，但在她和阿爾伯特·羅傑之後所接受的訪問中便否認了此事。在此場景中，希區考克使用朱古力漿來當作血，因為朱古力漿在黑白電影中看起來更像血。亦有傳聞表示其實此淋浴場景是由圖像設計師[索爾·巴斯導演](../Page/索爾·巴斯.md "wikilink")，珍妮·李後來亦否認說「當然不是！我在許多訪問中都強調了這一點。我在任何人的面前也是如斯說。我拍攝這淋浴場景有七天，請相信我，希區考克在那七天裏都在鏡頭旁邊」。\[5\]

## 續集和重拍

《驚魂記》經典電影衍生三部系列續作：1983年的《[驚魂記2](../Page/驚魂記2.md "wikilink")》（*Psycho
II*）、1986年的《[驚魂記3](../Page/驚魂記3.md "wikilink")》（*Psycho
III*）和1990年的[前傳](../Page/前傳.md "wikilink")《[驚魂記4](../Page/驚魂記4.md "wikilink")》（*Psycho
IV: The
Beginning*），最後一部是電視電影，由最初電影編劇[約瑟夫·史蒂法諾](../Page/約瑟夫·史蒂法諾.md "wikilink")（Joseph
Stefano）所寫。續集作品大致被認為程度上不及首集。\[6\]\[7\]

  - [安東尼·柏金斯在三部續集裡同樣擔任著諾曼](../Page/安東尼·柏金斯.md "wikilink")·貝茲的要角，同時也是第三集的導演。
  - 諾曼·貝茲母親諾瑪·貝茲的聲音在前三集一直維持知名女廣播員 Virginia Gregg
    所配音，除了第四集是由[奧莉薇·荷西](../Page/奧莉薇·荷西.md "wikilink")（Olivia
    Hussey）扮演諾瑪·貝茲的角色。
  - [薇拉·麥爾斯也在](../Page/薇拉·麥爾斯.md "wikilink")《驚魂記2》以萊拉·克萊恩（Lila
    Crane）角色再度出場。
  - [希區考克也從未參與任何](../Page/希區考克.md "wikilink")《驚魂記》系列續集的製作（在續集陸續完成前早已過世）。

[吉士·雲·辛曾於](../Page/吉士·雲·辛.md "wikilink")1998年將此片[重拍](../Page/重拍.md "wikilink")，名為《[-{zh-hk:觸目驚心;
zh-tw:1999驚魂記;
zh-hans:1999惊魂记;}-](../Page/1999驚魂記.md "wikilink")》。片中所有鏡頭都直接沿用了[希區考克原本的拍攝方法](../Page/希區考克.md "wikilink")，但電影上映後卻不太賣座，更廣受劣評。

2013年，A\&E推出了影集《[貝茲旅館](../Page/貝茲旅館.md "wikilink")》，是本作的當代前傳兼重拍，由[佛瑞迪·海默飾演諾曼](../Page/佛萊迪·海默爾.md "wikilink")·貝茲、[薇拉·法蜜嘉飾演諾瑪](../Page/薇拉·法蜜嘉.md "wikilink")·貝茲/「母親」。各季皆受到影評人以及觀眾的高度讚譽。該劇的主要演員[薇拉·法蜜嘉和](../Page/薇拉·法蜜嘉.md "wikilink")[佛瑞迪·海默的演出受到特别讚揚](../Page/佛萊迪·海默爾.md "wikilink")，前者更獲得[黃金時段艾美獎提名](../Page/黃金時段艾美獎.md "wikilink")。

## 文化影響

《驚魂記》在[流行文化各方面上已被引用過無數次](../Page/流行文化.md "wikilink")，許多電影、電視或音樂作曲都向此部影片致敬，特別是經典的「淋浴場景」和尖叫般的小提琴配樂。

  - 電影
      - 導演[布萊恩·狄·帕瑪](../Page/布萊恩·狄·帕瑪.md "wikilink")(Brian De
        Palma)往往受到希區考克賦予靈感，[西西·史派克](../Page/西西·史派克.md "wikilink")(Sissy
        Spacek)的角色在**《[魔女嘉莉](../Page/魔女嘉莉.md "wikilink")(Carrie)》**中一所稱為**貝茲**高中的學校就讀，學校的取名應該是狄帕瑪向希區考克的《驚魂記》的致敬。
      - [約翰·卡本特](../Page/約翰·卡本特.md "wikilink")(John
        Carpenter)1978年**《[月光光心慌慌](../Page/月光光心慌慌.md "wikilink")(Halloween)》**電影中，[唐納·普萊森斯](../Page/唐納·普萊森斯.md "wikilink")(Donald
        Pleasence)所飾演的醫生角色就取名為**山姆·路米斯**(Dr. Sam
        Loomis)。此外，在電影中護士角色被稱為"Chambers"。
      - 角色名稱在整合許多經典恐怖電影的**《[驚聲尖叫](../Page/驚聲尖叫.md "wikilink")(Scream)》**中再度被使用，像是[史基·尤瑞奇](../Page/史基·尤瑞奇.md "wikilink")(Skeet
        Ulrich)所飾演**比利·路米斯**(Billy
        Loomis)一角。透過此角色，在最後受到致命傷且快死的時候，路米斯引述了《驚魂記》安東尼·柏金斯的名言「有時候我們都會有點瘋狂。(We
        all go a little mad sometimes.)」
      - [梅爾·布魯克](../Page/梅爾·布魯克.md "wikilink")(Mel
        Brooks)在他的1977年的希區考克諷刺模仿電影**《[恐高症](../Page/恐高症.md "wikilink")(High
        Anxiety)》**自編自導，影片中他在他的旅館房間裡[淋浴](../Page/淋浴.md "wikilink")，特意模仿戲謔《驚魂記》裡的「淋浴場景」經典鏡頭。
      - **《[反斗智多星](../Page/反斗智多星.md "wikilink")(Wayne's
        World)》**中偉恩(Wayne)用一支牙刷當作刀子攻擊葛斯(Garth)。
      - 1993年喜劇**《[窈窕奶爸](../Page/窈窕奶爸.md "wikilink")(Mrs.
        Doubtfire)》**[羅賓·威廉斯所飾演的丹尼](../Page/羅賓·威廉斯.md "wikilink")(Daniel
        Hillard)，拼命不停來回變男變女為了騙過他的公司主管，最後面具被拆穿，他脫下浴袍和假髮後使眾人大吃一驚並說「啊，**諾曼貝茲**」。

<!-- end list -->

  - 淋浴場景在許多影片中致敬或是被惡搞，著名的例子包括：\[8\]
      - 《[-{zh:辛普森一家;zh-hans:辛普森一家;zh-hant:辛普森一家;zh-cn:辛普森一家;zh-tw:辛普森家庭;zh-hk:阿森一族;zh-mo:阿森一族;}-](../Page/辛普森一家.md "wikilink")》
        <span style="font-size:smaller;">(動畫)</span>
      - 《[恐高症](../Page/恐高症.md "wikilink")(High Anxiety)》
      - 《[歌剧魅影](../Page/歌剧魅影.md "wikilink")(Phantom of the Opera)》
      - 《The Ren and Stimpy Show》
        <span style="font-size:smaller;">(動畫)</span>
      - 《[樂一通大顯身手](../Page/樂一通大顯身手.md "wikilink")(Looney Tunes: Back in
        Action)》
      - 《[警察學校四：全民警察](../Page/警察學校四：全民警察.md "wikilink")(Police Academy
        4: Citizens on Patrol)》
      - 《[賓尼兔大冒險](../Page/迷你樂一通.md "wikilink")(Tiny Toon Adventures)》
        <span style="font-size:smaller;">(動畫)</span>
      - 《[上校的遺產](../Page/上校的遺產.md "wikilink")(The Colonel's Bequest)》
        <span style="font-size:smaller;">(遊戲)</span>
      - 《[瘋狂假期](../Page/瘋狂假期.md "wikilink")(National Lampoon's
        Vacation)》
      - 《[巧克力冒險工廠](../Page/巧克力冒險工廠.md "wikilink")(Charlie and the
        Chocolate Factory)》
      - 《That '70s Show》 <span style="font-size:smaller;">(電視影集)</span>
      - 《[德克斯特的實驗室](../Page/德克斯特的實驗室.md "wikilink")(Dexter's
        Laboratory)》 <span style="font-size:smaller;">(動畫)</span>
      - 《[親親麻吉](../Page/親親麻吉.md "wikilink")(Foster's Home for Imaginary
        Friends)》 <span style="font-size:smaller;">(動畫)</span>
      - 《[高飛家族](../Page/高飛家族.md "wikilink")(Goof Troop)》
        <span style="font-size:smaller;">(動畫)</span>
      - 《Chalkzone》 <span style="font-size:smaller;">(動畫)</span>
      - 《[天才吉米兵](../Page/天才吉米兵.md "wikilink")(Jimmy Neutron)》
        <span style="font-size:smaller;">(動畫)</span>
      - 《[麻辣女孩](../Page/麻辣女孩.md "wikilink")(Kim Possible)》
        <span style="font-size:smaller;">(動畫)</span>
      - 《The Far Side》 <span style="font-size:smaller;">(動畫)</span>
      - 《Round Numbers》
      - 《[淘小子看世界](../Page/淘小子看世界.md "wikilink")(Boy Meets World)》
        <span style="font-size:smaller;">(電視影集)</span>
      - 《[絕望先生](../Page/絕望先生.md "wikilink")(さよなら絶望先生)》
        <span style="font-size:smaller;">(漫畫)</span>
      - 《[星之卡比](../Page/星之卡比_\(動畫\).md "wikilink")(星のカービィ)》
        <span style="font-size:smaller;">(動畫)</span>
      - 《[尖叫女王](../Page/尖叫女王.md "wikilink")》<span style="font-size:smaller;">（電視影集）</span>：由珍妮·李的女兒[潔美·李·寇蒂斯重現此場景](../Page/潔美·李·寇蒂斯.md "wikilink")，亦有致敬意味。
      - 《[貝茲旅館](../Page/貝茲旅館.md "wikilink")》<span style="font-size:smaller;">（電視影集）</span>：由[佛瑞迪·海默飾演諾曼](../Page/佛萊迪·海默爾.md "wikilink")·貝茲、[薇拉·法蜜嘉飾演諾瑪](../Page/薇拉·法蜜嘉.md "wikilink")·貝茲/「母親」、[蕾哈娜飾演瑪莉安](../Page/蕾哈娜.md "wikilink")·克萊恩以及[奧斯汀·尼可斯飾演山姆](../Page/奧斯汀·尼可斯.md "wikilink")·魯米斯。第五季時出現了兩段淋浴場景，但與電影不同，瑪莉安毫髮無傷的離開了貝茲旅館，在浴室被諾曼殺害的則是前去找瑪莉安的山姆。此外，諾曼在殺人時並沒有被「母親」掌握，而是在「母親」的言語煽動以及本身對山姆的極大不滿下動手殺人。

## 參見

  - 《[驚悚大師：希區考克](../Page/希区柯克_\(电影\).md "wikilink")》
  - 《[24小時驚魂記](../Page/24小時觸目驚心.md "wikilink")》
  - 《[貝茲旅館](../Page/貝茲旅館.md "wikilink")》：本作的當代前傳兼重拍

## 参考

## 外部連結

  -
  -
  -
  -
  -
  - [Bright Lights Film
    驚魂記期刊專欄](http://www.brightlightsfilm.com/28/psycho1.html)

  - [Filmsite: 驚魂記](http://www.filmsite.org/psyc.html) 電影深入分析

  - [驚魂記特別剪輯發行版本](http://www.dvd.reviewer.co.uk/reviews/review.asp?Index=3090&User=10889)

  - [Psycho vs.
    Psycho](http://cinemademerde.com/Essay-Psycho_vs_Psycho.shtml)
    新版和舊版的比較

  - [驚魂記經典鏡頭](https://web.archive.org/web/20091221015055/http://www.hitchcockfans.com/gallery_psycho.php)

[Category:1960年電影](../Category/1960年電影.md "wikilink")
[Category:黑白電影](../Category/黑白電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國驚悚片](../Category/美國驚悚片.md "wikilink")
[Category:美國恐怖片](../Category/美國恐怖片.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")
[Category:亞弗列德·希區考克電影](../Category/亞弗列德·希區考克電影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:环球影业电影](../Category/环球影业电影.md "wikilink")
[Category:驚悚小說改編電影](../Category/驚悚小說改編電影.md "wikilink")
[Category:加利福尼亞州背景電影](../Category/加利福尼亞州背景電影.md "wikilink")
[Category:亞利桑那州背景電影](../Category/亞利桑那州背景電影.md "wikilink")
[Category:女裝電影](../Category/女裝電影.md "wikilink")
[Category:亞利桑那州取景電影](../Category/亞利桑那州取景電影.md "wikilink")

1.
2.

3.  *Psycho* is the top listed Hitchcock film in *The 100 Greatest
    Movies of All Time* by *[Entertainment
    Weekly](../Page/Entertainment_Weekly.md "wikilink")*, and the
    highest Hitchcock film on [AFI's 100 Years...100
    Movies](../Page/AFI's_100_Years...100_Movies.md "wikilink").

4.

5.

6.  [Ebert, Roger](../Page/Ebert,_Roger.md "wikilink") ''*'驚魂記3*.*Roger
    Ebert' Movie Home Companion*. Kansas City: Andrews and McMeel, 1991

7.

8.