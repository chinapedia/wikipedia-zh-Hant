**驪興閔氏**（）是[朝鮮半島的一個大氏族](../Page/朝鮮半島.md "wikilink")。根据其族谱记载，朝鲜闵氏始祖是[孔子的弟子](../Page/孔子.md "wikilink")[闵损](../Page/闵损.md "wikilink")（子骞），世居中国[山东](../Page/山东.md "wikilink")。[高丽时期](../Page/高丽.md "wikilink")，闵损后代[闵称道以使臣身份渡海](../Page/闵称道.md "wikilink")，后来定居在骊兴（[驪州旧称](../Page/骊州市.md "wikilink")），成为朝鮮半島闵氏的始祖（闵氏在朝鲜仅有骊兴闵氏一门）。

闵氏在[高丽时期即成为望族](../Page/高丽.md "wikilink")，闵称道曾孙[闵令谋曾任高丽](../Page/闵令谋.md "wikilink")[太子太师和](../Page/太子太师.md "wikilink")[门下侍郎](../Page/门下侍郎.md "wikilink")[平章事](../Page/平章事.md "wikilink")，闵氏在高丽时期出现过[闵湜](../Page/闵湜.md "wikilink")、[闵公珪](../Page/闵公珪.md "wikilink")、[闵曦](../Page/闵曦.md "wikilink")、[闵渍](../Page/闵渍.md "wikilink")、[闵萱](../Page/闵萱.md "wikilink")、[闵頔等名臣](../Page/闵頔.md "wikilink")。[李成桂建立朝鲜时](../Page/李成桂.md "wikilink")，[闵汝翼协助其创业有功](../Page/闵汝翼.md "wikilink")，升为[大司宪](../Page/大司宪.md "wikilink")、[户曹](../Page/户曹.md "wikilink")[判书](../Page/判书.md "wikilink")。闵頔之孙之女嫁给李芳远，即[元敬王后](../Page/元敬王后.md "wikilink")。闵霁被封为骊兴[府院君](../Page/府院君.md "wikilink")，其子[闵无咎](../Page/闵无咎.md "wikilink")、[闵无疾](../Page/闵无疾.md "wikilink")、[闵无悔](../Page/闵无悔.md "wikilink")、[闵无恤等人在](../Page/闵无恤.md "wikilink")[第一次王子之乱时立功](../Page/第一次王子之乱.md "wikilink")，成为功臣。闵霁死后，闵无咎、闵无疾以离间宗室之名被流放，闵氏气势有所下降。

[中宗反正后](../Page/中宗反正.md "wikilink")，参与政变的[闵孝曾被封为靖国功臣](../Page/闵孝曾.md "wikilink")、左赞成、骊平[府院君](../Page/府院君.md "wikilink")，闵氏得以中兴。[肃宗时期](../Page/朝鮮肃宗.md "wikilink")，[闵蓍重](../Page/闵蓍重.md "wikilink")、[闵鼎重](../Page/闵鼎重.md "wikilink")、[闵维重兄弟三人声势显赫](../Page/闵维重.md "wikilink")，被称为“闵氏三房”。闵维重之女[仁显王后嫁给肃宗](../Page/仁显王后.md "wikilink")。但闵氏后来牵涉到[南人与](../Page/南人.md "wikilink")[西人的](../Page/西人.md "wikilink")[党争](../Page/党争.md "wikilink")，在[英祖時期再度失势](../Page/朝鮮英祖.md "wikilink")。

闵氏的第三次兴旺和李氏朝鲜末年的王室变动有关。[高宗之父](../Page/朝鲜高宗.md "wikilink")[兴宣大院君的生母是闵氏](../Page/兴宣大院君.md "wikilink")，夫人（高宗生母）也是闵氏。高宗迎娶[闵致禄之女](../Page/闵致禄.md "wikilink")[閔茲暎](../Page/閔茲暎.md "wikilink")，即闵妃（[明成皇后](../Page/明成皇后.md "wikilink")）。闵妃大量起用闵氏家族成员，如[吏曹判书](../Page/吏曹.md "wikilink")[闵长镐](../Page/闵长镐.md "wikilink")、吏曹及[兵曹判书](../Page/兵曹.md "wikilink")[闵谦镐](../Page/闵谦镐.md "wikilink")、[右议政](../Page/议政府.md "wikilink")[闵圭镐](../Page/闵圭镐.md "wikilink")、[工曹判书](../Page/工曹.md "wikilink")[閔致久](../Page/閔致久.md "wikilink")、[礼曹及工曹](../Page/礼曹.md "wikilink")、兵曹判书等，形成了宫中的闵氏势力。1895年明成皇后被杀害，闵氏势力再度衰落。1905年[日本迫使朝鲜签订](../Page/日本.md "wikilink")[乙巳条约时](../Page/乙巳条约.md "wikilink")，[闵泳焕自尽抗议](../Page/闵泳焕.md "wikilink")，被视为殉国英雄。、[闵泳翊等人则拒绝接受日本授予的](../Page/闵泳翊.md "wikilink")[爵位](../Page/爵位.md "wikilink")。

根据[韩国经济企划院](../Page/韩国经济企划院.md "wikilink")1975年的人口调查，闵氏占韩国人口的0.3%，人数在韩国249个姓氏中居第43位。

## 字輩表

<table>
<thead>
<tr class="header">
<th><p>24世</p></th>
<th><p>25世</p></th>
<th><p>26世</p></th>
<th><p>27世</p></th>
<th><p>28世</p></th>
<th><p>29世</p></th>
<th><p>30世</p></th>
<th><p>31世</p></th>
<th><p>32世</p></th>
<th><p>33世</p></th>
<th><p>34世</p></th>
<th><p>35世</p></th>
<th><p>36世</p></th>
<th><p>37世</p></th>
<th><p>38世</p></th>
<th><p>39世</p></th>
<th><p>40世</p></th>
<th><p>41世</p></th>
<th><p>42世</p></th>
<th><p>43世</p></th>
<th><p>44世</p></th>
<th><p>45世</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>백(百)</p></td>
<td><p>口혁(赫)<br />
口현(顯)</p></td>
<td><p>치(致)</p></td>
<td><p>口호(鎬)</p></td>
<td><p>영(泳)</p></td>
<td><p>口식(植)</p></td>
<td><p>병(丙)</p></td>
<td><p>口기(基)<br />
口규(圭)<br />
口배(培)</p></td>
<td><p>경(庚)</p></td>
<td><p>口홍(泓)<br />
口원(源)</p></td>
<td><p>동(東)</p></td>
<td><p>口희(熙)</p></td>
<td><p>형(馨)<br />
재(在)</p></td>
<td><p>口석(錫)</p></td>
<td><p>준(準)</p></td>
<td><p>口권(權)</p></td>
<td><p>용(容)</p></td>
<td><p>口범(範)</p></td>
<td><p>선(善)</p></td>
<td><p>口순(淳)</p></td>
<td><p>상(相)</p></td>
<td><p>口영(榮)</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 闵氏三房

**长房**
[闵蓍重](../Page/闵蓍重.md "wikilink")→[闵镇夏](../Page/闵镇夏.md "wikilink")→[闵承洙](../Page/闵承洙.md "wikilink")→[闵百亨](../Page/闵百亨.md "wikilink")→[闵台爀](../Page/闵台爀.md "wikilink")
　　　　[闵镇周](../Page/闵镇周.md "wikilink")→[闵应洙](../Page/闵应洙.md "wikilink")→[闵百行](../Page/闵百行.md "wikilink")→[闵济烈](../Page/闵济烈.md "wikilink")→[闵致殷](../Page/闵致殷.md "wikilink")→[闵世镐](../Page/闵世镐.md "wikilink")
　　　　　　　　　　　　[闵百昌](../Page/闵百昌.md "wikilink")→[闵命爀](../Page/闵命爀.md "wikilink")→[闵致文](../Page/闵致文.md "wikilink")→[闵达镐](../Page/闵达镐.md "wikilink")→[闵泳穆](../Page/闵泳穆.md "wikilink")
　　　　　　　　　　　　　　　　　　　　[闵致殷](../Page/闵致殷.md "wikilink")(过继给闵济烈)
　　　　[闵镇鲁](../Page/闵镇鲁.md "wikilink")→[闵兴洙](../Page/闵兴洙.md "wikilink")→[闵百宪](../Page/闵百宪.md "wikilink")
　　　　　　　　　　　　[闵百寅](../Page/闵百寅.md "wikilink")→[闵祯爀](../Page/闵祯爀.md "wikilink")→[闵致叙](../Page/闵致叙.md "wikilink")→[闵斗镐](../Page/闵斗镐.md "wikilink")→[闵泳徽](../Page/闵泳徽.md "wikilink")→[闵衡植](../Page/闵衡植.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　[闵大植](../Page/闵大植.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　[闵天植](../Page/闵天植.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　[闵奎植](../Page/闵奎植.md "wikilink")
**二房**
[闵鼎重](../Page/闵鼎重.md "wikilink")→[闵镇长](../Page/闵镇长.md "wikilink")→[闵德洙](../Page/闵德洙.md "wikilink")→[闵百宪](../Page/闵百宪.md "wikilink")→[闵台爀](../Page/闵台爀.md "wikilink")
　　　　　　　　　　　　　　　　[闵命爀](../Page/闵命爀.md "wikilink")
　　　　　　　　[閔安洙](../Page/閔安洙.md "wikilink")→[闵百徵](../Page/闵百徵.md "wikilink")→[闵景爀](../Page/闵景爀.md "wikilink")→郡夫人→**[兴宣大院君](../Page/兴宣大院君.md "wikilink")**
**三房**
[闵维重](../Page/闵维重.md "wikilink")→[闵镇厚](../Page/闵镇厚.md "wikilink")→[闵翼洙](../Page/闵翼洙.md "wikilink")→[闵百奋](../Page/闵百奋.md "wikilink")→[闵耆显](../Page/闵耆显.md "wikilink")→[闵致禄](../Page/闵致禄.md "wikilink")→[闵升镐](../Page/闵升镐.md "wikilink")→[闵泳翊](../Page/闵泳翊.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　**[明成皇后](../Page/明成皇后.md "wikilink")**（高宗皇后）
　　　　　　　　[闵遇洙](../Page/闵遇洙.md "wikilink")→[闵百瞻](../Page/闵百瞻.md "wikilink")→[闵彝显](../Page/闵彝显.md "wikilink")→[闵致秉](../Page/闵致秉.md "wikilink")→[闵正镛](../Page/闵正镛.md "wikilink")→[闵泳商](../Page/闵泳商.md "wikilink")
　　　　　　　　　　　　[闵百谦](../Page/闵百谦.md "wikilink")
　　　 **[仁显王后](../Page/仁显王后.md "wikilink")**（肃宗王后）
旁系
　　　　　　　　　　　　[闵百述](../Page/闵百述.md "wikilink")→[闵端显](../Page/闵端显.md "wikilink")→[闵致久](../Page/闵致久.md "wikilink")→闵升镐(过继给闵致禄)
　　　　　　　　　　　　　　　　　　　　　　　　[闵谦镐](../Page/闵谦镐.md "wikilink")→
　　　　　　　　　　　　　　　　　　　　　　　　　　　→[闵泳焕](../Page/闵泳焕.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　**骊兴府大夫人**（高宗生母）
　　　　　　　　　　　　　　　　[闵龙显](../Page/闵龙显.md "wikilink")→[闵致庠](../Page/闵致庠.md "wikilink")
　　　　　　　　　　　　　　　　　　　　[闵致悳](../Page/闵致悳.md "wikilink")→[闵商镐](../Page/闵商镐.md "wikilink")
　　　　　　　　　　　　　　　　　　　　[闵致五](../Page/闵致五.md "wikilink")→[闵奎镐](../Page/闵奎镐.md "wikilink")
　　　　　　　　　　　　　　　　　　　　[闵致三](../Page/闵致三.md "wikilink")→[閔台鎬](../Page/閔台鎬.md "wikilink")→[闵泳翊](../Page/闵泳翊.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　　　　　
　　　　　　　　　　　　　　　　　　　　　　　　　　　　[纯明孝皇后](../Page/纯明孝皇后.md "wikilink")（纯宗皇后）
　　　　　　　　　　　　　　　　　　　　[闵致亿](../Page/闵致亿.md "wikilink")→[闵商镐](../Page/闵商镐.md "wikilink")
　　　　　　　　　　　　　　　　[闵钟显](../Page/闵钟显.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵哲镐](../Page/闵哲镐.md "wikilink")→[闵泳韶](../Page/闵泳韶.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵述镐](../Page/闵述镐.md "wikilink")→[闵泳璘](../Page/闵泳璘.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵泰镐](../Page/闵泰镐.md "wikilink")→[闵泳焕](../Page/闵泳焕.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵建镐](../Page/闵建镐.md "wikilink")→[闵泳敦](../Page/闵泳敦.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵周镐](../Page/闵周镐.md "wikilink")→[闵泳国](../Page/闵泳国.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵璟镐](../Page/闵璟镐.md "wikilink")→[闵泳奎](../Page/闵泳奎.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵峻镐](../Page/闵峻镐.md "wikilink")→[闵泳绮](../Page/闵泳绮.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵观镐](../Page/闵观镐.md "wikilink")→[闵泳达](../Page/闵泳达.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵璋镐](../Page/闵璋镐.md "wikilink")→[闵泳柱](../Page/闵泳柱.md "wikilink")→[闵京植](../Page/闵京植.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　[闵善镐](../Page/闵善镐.md "wikilink")→[闵泳喆](../Page/闵泳喆.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　　　　　[闵泳愚](../Page/闵泳愚.md "wikilink")→[闵应植](../Page/闵应植.md "wikilink")
　　　　　　　　　　　　　　　　　　　　　　　　　　　　[闵泳稷](../Page/闵泳稷.md "wikilink")→[闵正植](../Page/闵正植.md "wikilink")

## 参见

  - [朝鮮王朝](../Page/朝鮮王朝.md "wikilink")
  - [外戚](../Page/外戚.md "wikilink")
  - [韓國外來歸化姓氏](../Page/韓國外來歸化姓氏.md "wikilink")

[驪興閔氏](../Category/驪興閔氏.md "wikilink")