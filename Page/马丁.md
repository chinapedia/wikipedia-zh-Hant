**马丁**（，直到1950年使用Turčiansky Svätý
Martin；）是[斯洛伐克共和国北部](../Page/斯洛伐克.md "wikilink")[喀尔巴阡山麓的小城市](../Page/咯尔巴阡山脉.md "wikilink")，位于[小法特拉山脉和](../Page/小法特拉山脉.md "wikilink")[大法特拉山脉之间](../Page/大法特拉山脉.md "wikilink")，[图列茨河沿岸](../Page/图列茨河.md "wikilink")。人口大约6.1万，是斯洛伐克第八大城市。1918年10月30日在这座城市召开的会议颁布了，决定斯洛伐克和[捷克合并成为一个共和国](../Page/捷克.md "wikilink")。

## 外部链接

  - [马丁市官方网站](http://www.martin.sk/index.asp)

[Category:斯洛伐克城市](../Category/斯洛伐克城市.md "wikilink")