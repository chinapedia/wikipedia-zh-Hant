**馬俊仁**（），[辽宁](../Page/辽宁省.md "wikilink")[辽阳](../Page/辽阳市.md "wikilink")\[1\]人，曾任[中國](../Page/中國.md "wikilink")[田徑教練](../Page/田徑.md "wikilink")，以訓練出遼寧女子[中长跑運動員](../Page/中长跑.md "wikilink")（泛稱“馬家軍”）闻名。

## 簡歷

  - 中专毕业。1960年参加工作。\[2\]
  - 1962年至1968年：马俊仁是[中国人民解放军](../Page/中国人民解放军.md "wikilink")3368部队（辽宁省军区独立第2师）任战士、[班长](../Page/班长.md "wikilink")。
  - 1970年入讀[鞍山市](../Page/鞍山市.md "wikilink")“五·七”师范学校体育短训班，毕业后任[鞍山市第](../Page/鞍山市.md "wikilink")55、17、
    29、67中学体育教师。
  - 1982年：起任[辽宁省田径队](../Page/辽宁省.md "wikilink")[马拉松教练](../Page/马拉松.md "wikilink")；1986年任鞍山市业余体校教练。
  - 1988年：起任辽宁省田径队女子中长跑组教练。所帶領的組別後來被人稱作“馬家軍”，主要成員有[王軍霞](../Page/王軍霞.md "wikilink")、[曲雲霞](../Page/曲雲霞.md "wikilink")、[劉東等](../Page/劉東.md "wikilink")。
  - 1993年：[七運會期間帶領遼寧田徑隊奪下](../Page/七運會.md "wikilink")12面金牌，“馬家軍”一炮而紅。同年，他率領“馬家軍”參加在[斯圖加特舉行的第](../Page/斯圖加特.md "wikilink")4届世界田徑錦標賽上奪下女子1500米、3000米及10000米共三面金牌，壟斷女子中長跑世界賽。
    全年產生共66項紀錄，當中包括中國全國紀錄、亞洲紀錄及世界紀錄。
  - 1994年：“馬家軍”成員王军霞離隊，轉至[毛德镇手下](../Page/毛德镇.md "wikilink")。“馬家軍”開始衰落。
  - 1996年：因病辭去教練工作，改任遼寧省田徑訓練中心經理；1998年任辽宁省体委副主任。
  - 2004年10月28日，马俊仁正式宣布退出教练生涯。目前任中国[藏獒俱乐部主席](../Page/藏獒.md "wikilink")。

## 興奮劑事件

2009年[袁伟民自传](../Page/袁伟民.md "wikilink")《[袁伟民与体坛风云](../Page/袁伟民与体坛风云.md "wikilink")》一书发表。袁伟民在书中披露：2000年，[悉尼奧運會開幕前](../Page/悉尼奧運會.md "wikilink")1個月，中國[國家體育總局向各運動員進行藥檢](../Page/國家體育總局.md "wikilink")，當中某田径队獲奧運參賽資格的7名運動員中，2人尿檢呈陽性、4人血檢超標，共6人證實服用了興奮劑或有強烈的服用禁藥嫌疑。遂即終止其隊伍參賽奧運資格。

有网站报道\[3\]中，明确写出这支队伍就是辽宁田径队。其结果是遼寧田徑隊禁賽一年。“馬家軍”突然在奧運會消失。

2016年2月，中國大陸媒體開始報道馬家軍當年服用興奮劑 \[4\]。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:中国田径教练](../Category/中国田径教练.md "wikilink")
[Category:辽阳人](../Category/辽阳人.md "wikilink")
[Category:马姓](../Category/马姓.md "wikilink")

1.

2.
3.  [中国广播网的报道](http://www.cnr.cn/gundong/200910/t20091012_505506433.html)

4.  [網易：马家军被删章节:马俊仁亲自打针
    王军霞举报](http://news.163.com/16/0203/11/BET60R0I00011229.html)