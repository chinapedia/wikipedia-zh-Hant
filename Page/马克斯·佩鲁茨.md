**马克斯·费迪南德·佩鲁茨**，[OM](../Page/功績勳章_\(英國\).md "wikilink")（，），[奥地利](../Page/奥地利.md "wikilink")-[英国](../Page/英国.md "wikilink")[分子生物学家](../Page/分子生物学.md "wikilink")，1962年获[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。

## 参考资料

## 外部链接

[Category:奥地利生物学家](../Category/奥地利生物学家.md "wikilink")
[Category:英国生物学家](../Category/英国生物学家.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:X射线晶体学](../Category/X射线晶体学.md "wikilink")
[Category:欧洲分子生物学组织会员](../Category/欧洲分子生物学组织会员.md "wikilink")
[Category:劍橋大學彼得學院校友](../Category/劍橋大學彼得學院校友.md "wikilink")
[Category:維也納大學校友](../Category/維也納大學校友.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:宗座科学院院士](../Category/宗座科学院院士.md "wikilink")