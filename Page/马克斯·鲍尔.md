[OberstleutnantMaxBauer.jpg](https://zh.wikipedia.org/wiki/File:OberstleutnantMaxBauer.jpg "fig:OberstleutnantMaxBauer.jpg")[Blue_Max.jpg](https://zh.wikipedia.org/wiki/File:Blue_Max.jpg "fig:Blue_Max.jpg")[PLMeichenlaub.jpg](https://zh.wikipedia.org/wiki/File:PLMeichenlaub.jpg "fig:PLMeichenlaub.jpg")
**马克斯·鲍尔**（，），是[德国](../Page/德国.md "wikilink")[-{A专家](../Page/炮兵.md "wikilink")。

## 生平

[一战中在](../Page/一战.md "wikilink")[德军](../Page/德军.md "wikilink")[总参谋部](../Page/总参谋部.md "wikilink")<small>（德意志帝國[最高陸軍指揮](../Page/最高陸軍指揮.md "wikilink")[埃里希·鲁登道夫將軍麾下](../Page/埃里希·鲁登道夫.md "wikilink")）</small>中[服役](../Page/兵役.md "wikilink")，曾负责与科学家如[瓦尔特·能斯特联络](../Page/瓦尔特·能斯特.md "wikilink")。在1915年7月，他成為總參謀部的第一廳的廳長。他動員工業生產彈藥和寫了一篇關於「[防守戰術](../Page/軍事戰術.md "wikilink")」小冊子。1916年12月他獲頒[功勛勳章](../Page/功勛勳章.md "wikilink")，1918年3月18日授「」作為更高一級的獎勵。[一战后为](../Page/一战.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[德国顾问团的成员](../Page/德国顾问团.md "wikilink")。\[1\]1929年5月6日，因[蔣桂戰爭感染](../Page/蔣桂戰爭.md "wikilink")[天花病死](../Page/天花.md "wikilink")，初葬於[上海市](../Page/上海市.md "wikilink")。\[2\]1929年8月5日，他的遺體運回德國並安葬於[希維諾烏伊希切](../Page/希維諾烏伊希切.md "wikilink")。\[3\]\[4\]

## 参见

  - [中德关系](../Page/中德关系.md "wikilink")

:\* [中德合作 (1911年—1941年)](../Page/中德合作_\(1911年—1941年\).md "wikilink")

:\* 中華民國[德国顾问团](../Page/德国顾问团.md "wikilink")

## 註釋

  - 腳注

<!-- end list -->

  - 引用

[Category:德国第一次世界大战军事人物](../Category/德国第一次世界大战军事人物.md "wikilink")
[Category:外籍援华人士](../Category/外籍援华人士.md "wikilink")
[Category:死于天花的人](../Category/死于天花的人.md "wikilink")

1.
2.
3.
4.