**马利克·米拉吉·哈立德**（[烏爾都語](../Page/烏爾都語.md "wikilink")：****，拉丁化：****，）是一名[巴基斯坦政治家](../Page/巴基斯坦.md "wikilink")，於1996年11月5日至1997年2月7日擔任[巴基斯坦過渡時期的](../Page/巴基斯坦.md "wikilink")[總理](../Page/巴基斯坦總理.md "wikilink")。他出生於靠近[拉合爾的小村莊](../Page/拉合爾.md "wikilink")\[1\]。他在1948年開始以律師為職業。1965年，他首次獲選為[西巴基斯坦](../Page/西巴基斯坦.md "wikilink")（[West
Pakistan](../Page/:en:West_Pakistan.md "wikilink")）省議會的議員。他於1968年[巴基斯坦人民黨創黨後就加入該黨](../Page/巴基斯坦人民黨.md "wikilink")，並擔任該黨[拉合爾黨部的首長](../Page/拉合爾.md "wikilink")。因為[巴基斯坦人民黨的提名](../Page/巴基斯坦人民黨.md "wikilink")，他於1970年順利選上國會議員。

## 與佐勒菲卡爾·阿里·布托的友誼

馬利克·米拉吉·哈立德最令人熟知的是他對於1970年代[巴基斯坦最亮眼的](../Page/巴基斯坦.md "wikilink")[總理](../Page/巴基斯坦總理.md "wikilink")[佐勒菲卡爾·阿里·布托的紳士風度與誠實](../Page/佐勒菲卡爾·阿里·布托.md "wikilink")，這也使他成為[布托的最愛之一](../Page/佐勒菲卡爾·阿里·布托.md "wikilink")。[布托在哈立德的政治生涯中佔有很重要的地位](../Page/佐勒菲卡爾·阿里·布托.md "wikilink")，他於1971年12月任命哈利德擔任食物與農業與開發地區部長（Minister
for Food and Agriculture and Under-Developed
Areas）\[2\]。之後，他於1972年11月任命哈利德擔任政黨議員事務長（Chief of
the Party's Parliamentary Affairs），1975年任命哈利德為社會福利、地方政府與農村部長（Minister of
Social Welfare, Local Government and Rural
Development）。最後他被選為[國會議長](../Page/巴基斯坦國會議長.md "wikilink")。

## 國會議員與議長

在[佐勒菲卡爾·阿里·布托於](../Page/佐勒菲卡爾·阿里·布托.md "wikilink")1979年4月被處決之後，他被提名為[巴基斯坦人民黨中央委員](../Page/巴基斯坦人民黨.md "wikilink")，而他最後於1988年1月辭去這個職務。在1988年又一次成功地回到國會之後，他再次被選為[國會議長](../Page/巴基斯坦國會議長.md "wikilink")。然而，他在1993年的選舉中失利，並且有一段時間遠離政治。在這段孤獨的日子中，他在[伊斯蘭瑪巴德國際伊斯蘭大學](../Page/伊斯蘭瑪巴德國際伊斯蘭大學.md "wikilink")（[International
Islamic University
Islamabad](../Page/:en:International_Islamic_University_Islamabad.md "wikilink")）任教。但最後他放棄了這樣的生活，並且受[巴基斯坦總統](../Page/巴基斯坦總統.md "wikilink")[法魯克·萊加里的邀請於](../Page/法魯克·萊加里.md "wikilink")1996年11月擔任大選前的過渡時期總理。

## 過渡時期總理

在擔任過渡時期總理的時期，馬利克·米拉吉·哈立德立下了一個節儉的新慣例。在大多數的場合中，他常沒有事先通知就巡視旅遊。他常在[拉合爾的商場中散步](../Page/拉合爾.md "wikilink")。

## 參考文獻

## 外部連結

  - [Chronicles Of
    Pakistan](https://web.archive.org/web/20040416030217/http://pakistanspace.tripod.com/47.htm)
  - [A profile of Malik Meraj
    Khalid](http://www.pakistaneconomist.com/database2/cover/c96-81.asp)
  - [Meraj Khalid passes away](http://www.dawn.com/2003/06/14/top12.htm)

[Category:巴基斯坦總理](../Category/巴基斯坦總理.md "wikilink")
[Category:巴基斯坦政治人物](../Category/巴基斯坦政治人物.md "wikilink")
[Category:巴基斯坦國會議長](../Category/巴基斯坦國會議長.md "wikilink")
[Category:巴基斯坦律師](../Category/巴基斯坦律師.md "wikilink")
[Category:拉合爾人](../Category/拉合爾人.md "wikilink")
[Category:馬克思主義作家](../Category/馬克思主義作家.md "wikilink")

1.  [Malik Meraj Khalid - The story of
    Pakistan](http://www.storyofpakistan.com/person.asp?perid=P026)

2.