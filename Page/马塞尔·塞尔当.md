**马塞尔·塞尔当**（****，）是一名出生在[阿尔及利亚的](../Page/阿尔及利亚.md "wikilink")[法国人](../Page/法国.md "wikilink")，他是世界[拳击冠军](../Page/拳击.md "wikilink")，被许多拳击专家和拳击迷称为法国以及[欧洲最伟大的拳击手](../Page/欧洲.md "wikilink")，同时也被更多的拳击迷称为来自[非洲的最伟大的拳击手之一](../Page/非洲.md "wikilink")。他的一生以其运动成绩、社会生活方式以及最终的悲剧而著名。

马塞尔·塞尔当出生在阿尔及利亚[西迪贝勒阿巴斯](../Page/西迪贝勒阿巴斯.md "wikilink")（Side
Bel-Abbes）。1934年11月4日在[摩洛哥](../Page/摩洛哥.md "wikilink")[梅克内斯](../Page/梅克内斯.md "wikilink")（Meknes），在一场与[Marcel
Bucchianeri六回合决出胜负的比赛中](../Page/Marcel_Bucchianeri.md "wikilink")，他开始了他的职业拳击生涯。从第一次较量到1939年1月4日在[伦敦与](../Page/伦敦.md "wikilink")[Harry
Cresner的比赛中被取消参赛资格而第一次失利](../Page/Harry_Cresner.md "wikilink")，他连续赢得了一系列47场比赛的胜利。
[阿尔·贝克](../Page/阿尔·贝克.md "wikilink")（Al
Baker）和[艾萨·阿塔夫](../Page/艾萨·阿塔夫.md "wikilink")（Aisa
Attaf）是这一系列被击败的拳击手中的两个，他们都两次被击倒。塞尔当在他的职业生涯中在摩洛哥和阿尔及利亚参加了大量的比赛，随后又在他父亲的家乡[法国开始继续他的比赛](../Page/法国.md "wikilink")。1938年，他在[卡萨布兰卡一场](../Page/卡萨布兰卡.md "wikilink")12回合的比赛中击败了[奥马尔·库韦德里](../Page/奥马尔·库韦德里.md "wikilink")（Omar
Kouidri），赢得了法国次中量级拳击冠军。

在他的第一场失利之后，塞尔当因连续5场的胜利而有机会在意大利米兰挑战萨维耶罗·图列洛（Saviello
Turiello）的欧洲次中量级拳击冠军的头衔。

1949年10月28日，他乘搭的[法國航空航班在降落](../Page/法國航空.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")[亞速爾群島時](../Page/亞速爾群島.md "wikilink")[撞山墜毀](../Page/1949年法国航空星座式客机空难.md "wikilink")，終年33歲。

塞尔当的纪录是106场胜利和4场失利，其中有61场是击倒对手而获胜。

他与拉莫塔以及扎莱同是[国际拳击名人堂的成员](../Page/国际拳击名人堂.md "wikilink")。

1983年，塞尔当和[艾迪特·皮雅芙的故事被搬上银幕](../Page/艾迪特·皮雅芙.md "wikilink")，拍成了电影“*埃迪特与马塞尔*”，小马塞尔·塞尔当在其中扮演他父亲。

[Category:世界拳擊冠軍](../Category/世界拳擊冠軍.md "wikilink")
[Category:法國拳擊運動員](../Category/法國拳擊運動員.md "wikilink")
[Category:阿爾及利亞法國人](../Category/阿爾及利亞法國人.md "wikilink")
[Category:法國空難身亡者](../Category/法國空難身亡者.md "wikilink")