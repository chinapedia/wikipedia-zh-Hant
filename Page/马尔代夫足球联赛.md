{{ infobox football league | name = 馬爾代夫足球聯賽 | another_name = Dhivehi
League | image = | pixels = | country =  | confed =
[AFC](../Page/亞洲足球聯合總會.md "wikilink")（[亞洲](../Page/亞洲.md "wikilink")）
| founded = 1983年 | folded = 2014年 | first = | teams = 8 隊 | relegation
= [馬爾代夫乙組足球聯賽](../Page/馬爾代夫乙組足球聯賽.md "wikilink") | level = 1 |
domestic_cup = [馬爾代夫足總盃](../Page/馬爾代夫足總盃.md "wikilink")
[馬爾代夫主席盃](../Page/馬爾代夫主席盃.md "wikilink")
[馬爾代夫慈善盾](../Page/馬爾代夫慈善盾.md "wikilink")
[馬爾代夫超級盃](../Page/馬爾代夫超級盃.md "wikilink") | international_cup =
[亞洲足協盃](../Page/亞洲足協盃.md "wikilink") | season = 2014年 |
champions = [拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink") |
most_successful_club = [勝利競技](../Page/勝利體育俱樂部.md "wikilink") |
no_of_titles = 10 次 | website = | current = }} **馬爾代夫足球聯賽**（**Dhivehi
League**）是前[馬爾代夫國內最高級別的](../Page/馬爾代夫.md "wikilink")[足球聯賽賽事](../Page/足球.md "wikilink")，聯賽成立於1983年，聯賽於2014年由全新的[馬爾代夫超級足球聯賽取代而成為歷史](../Page/馬爾代夫超級足球聯賽.md "wikilink")。[勝利競技為奪冠次數最多的隊伍](../Page/勝利體育俱樂部.md "wikilink")，一共
10 次奪標。

## 歷屆冠軍

  - 1983：[華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink")
  - 1984：[華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink")
  - 1985：
  - 1986：[勝利競技](../Page/勝利體育俱樂部.md "wikilink")
  - 1987：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink")
  - 1988：[勝利競技](../Page/勝利體育俱樂部.md "wikilink")
  - 1989：[潟湖俱樂部](../Page/潟湖俱樂部.md "wikilink")
  - 1990：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink")
  - 1991：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink")
  - 1992：[勝利競技](../Page/勝利體育俱樂部.md "wikilink")
  - 1993：[勝利競技](../Page/勝利體育俱樂部.md "wikilink")

<!-- end list -->

  - 1994：[華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink")
  - 1995：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink")
  - 1996：[勝利競技](../Page/勝利體育俱樂部.md "wikilink")
  - 1997：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink") 2-1
    [靴尼雅](../Page/靴尼雅體育俱樂部.md "wikilink")
  - 1998：[華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink") 1-1 2-0
    [勝利競技](../Page/勝利體育俱樂部.md "wikilink")
  - 1999：[華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink") 2-1
    [靴尼雅](../Page/靴尼雅體育俱樂部.md "wikilink")
  - 2000：[勝利競技](../Page/勝利體育俱樂部.md "wikilink")
  - 2001：[勝利競技](../Page/勝利體育俱樂部.md "wikilink") 2-1
    [華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink")
  - 2002：[勝利競技](../Page/勝利體育俱樂部.md "wikilink") 4-2
    [華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink")
  - 2003：[勝利競技](../Page/勝利體育俱樂部.md "wikilink") 2-1
    [華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink")
  - 2004：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink") 1-1
    [華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink") \[aet,
    点球6-5\]

<!-- end list -->

  - 2005：[勝利競技](../Page/勝利體育俱樂部.md "wikilink") 1-0
    [拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink")
  - 2006：[勝利競技](../Page/勝利體育俱樂部.md "wikilink") 0-0
    [華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink") \[aet,
    点球3-1\]
  - 2007：[勝利競技](../Page/勝利體育俱樂部.md "wikilink")
  - 2008：[華倫西亞會](../Page/瓦倫西亞俱樂部.md "wikilink")
  - 2009：[VB競技](../Page/VB阿杜足球會.md "wikilink")
  - 2010：[VB競技](../Page/VB阿杜足球會.md "wikilink")
  - 2011：[VB競技](../Page/VB阿杜足球會.md "wikilink")
  - 2012：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink")
  - 2013：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink")
  - 2014：[拉迪恩](../Page/新拉迪安特體育俱樂部.md "wikilink")

## 外部連結

  - [Competition Details &
    History](https://web.archive.org/web/20160304094207/http://www.footballdata.info/Competition.aspx?c=Maldives%20Dhivehi%20League)

[Category:马尔代夫足球](../Category/马尔代夫足球.md "wikilink")
[Maldives](../Category/亞洲各國足球聯賽.md "wikilink")
[亚](../Category/國家頂級足球聯賽.md "wikilink")
[Category:亞洲頂級足球聯賽](../Category/亞洲頂級足球聯賽.md "wikilink")