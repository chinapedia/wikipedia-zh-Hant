**马尔科·马特拉齐**（，），生於[意大利](../Page/意大利.md "wikilink")[萊切](../Page/萊切.md "wikilink")，現退役的意大利足球運動員。

[2006年世界杯足球赛决赛中](../Page/2006年世界杯足球赛.md "wikilink")，他故意挑衅[齐达内](../Page/齐达内.md "wikilink")，对方盛怒头撞并当即被红牌重罚下场，他也因此声名大噪，争议甚广。

## 俱乐部生涯

馬特拉斯早年出身於中下游球會墨西拿，1993年加盟马尔萨拉（Marsala）展開職業足球生涯。1995年加盟[佩魯賈](../Page/佩魯賈足球俱樂部.md "wikilink")，但2年後才取得正選位置。其後，他成為了該隊首席的[自由球和](../Page/自由球.md "wikilink")[點球劊子手](../Page/點球.md "wikilink")；2001年以後衛身份在[意甲射入](../Page/意甲.md "wikilink")12個入球，直到現在仍是意甲後衛单赛季進球最多的紀錄保持者。翌年以600萬英鎊的身價轉會國際米蘭。在國際米蘭的五年，他受到多位教練的重用，當年更被傳謀視為是[埃克托·库珀教練的](../Page/埃克托·库珀.md "wikilink")「中流砥柱」。马特拉齐个性十足，球场作风咄咄逼人，被球迷称为“屠夫”（The
Butcher）或“人間凶器”。

2011年6月，马特拉齐与国际米兰的合同到期且不获续约，成为自由球员。

2014年9月，马特拉齐与印超球隊簽署總值2百萬美元的2年合約，擔任[球員兼](../Page/足球員.md "wikilink")[教練](../Page/教練.md "wikilink")。

## 国家队生涯

馬特拉斯於2001年才獲[意大利國家隊徵召](../Page/意大利國家足球隊.md "wikilink")，成為國家隊的第三中堅，於2001年4月在同[南非的友誼賽中首次入選國家隊](../Page/南非國家足球隊.md "wikilink")。

[2006年世界杯期间](../Page/2006年世界杯.md "wikilink")，[内斯塔在小组赛第一轮后受伤](../Page/内斯塔.md "wikilink")，马特拉齐遂替代其作为先发，在该届赛事表现有如神助，於決賽为意大利頂入扳平一球，[點球決勝中射入第二球](../Page/點球決勝.md "wikilink")，是意大利捧得该届世界杯冠军的功臣之一。

[2008年欧洲杯意大利以](../Page/2008年欧洲足球锦标赛.md "wikilink")0:3败于荷兰的比赛，马特拉齐最后一次代表国家队出战。其在之后宣布退出意大利国家队。

## 齐达内头撞事件

事緣於[2006年世界杯意大利進入決賽對戰](../Page/2006年世界杯.md "wikilink")[法國隊時](../Page/法國國家足球隊.md "wikilink")，在比赛进行到109分钟时法国队队长[齐达内突然用頭撞擊馬特拉斯的胸部](../Page/齐内丁·齐达内.md "wikilink")，被红牌罰下。\[1\]當月下旬[国际足联和意大利足總就事件判罪马特拉齐停賽兩場並罰款](../Page/国际足联.md "wikilink")5000[瑞士法郎](../Page/瑞士法郎.md "wikilink")。\[2\]

马特拉齐究竟说了什么导致齐达内如此愤怒，以至于在比赛中公然头撞他成为了赛后人们感兴趣的话题。最后在距该届世界杯1年后的2007年，马特拉齐在意大利杂志《微笑和歌唱电视》(TV
Sorrisi e
Canzoni)中揭露真相，也让这段持续了一年多的谜案终于水落石出。齐达内当时對拉扯自己的马特拉齐说：“如果你喜欢我的球衣，赛後我就把球衣送你*<small>(If
you want my shirt, I will give to you
afterwards)</small>*。”马特拉齐随即回击：“我倒比較喜欢你的那个婊子姐姐
*<small>(preferisco quella buttana di tua sorella)</small>*。”\[3\]

马特拉齐曾表示愿意与齐达内和好如初，\[4\]不过齐达内时隔四年后表态称，他永远也不会原谅马特拉齐。\[5\]
2010年11月，兩人在4年前的世界盃之後的首次見面，施丹和馬特拉斯擁抱問候之後，還進行了短暫的交談。馬特拉斯在自己的官網上寫到「我擁抱了施丹，我選擇和他和解。」\[6\]

2012年9月，施丹頭頂馬特拉斯的塑像在[法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[蓬皮杜國家藝術文化中心展覽館門前進行了公開展覽](../Page/龐畢度中心.md "wikilink")。\[7\]

## 轶事

  - 马特拉齐的父親就是著名教練[朱塞佩·马特拉齐](../Page/朱塞佩·马特拉齐.md "wikilink")（）；他曾執教[比萨](../Page/比萨.md "wikilink")，[拉素](../Page/拉素.md "wikilink")，[巴里](../Page/巴里.md "wikilink")，[帕多瓦](../Page/帕多瓦.md "wikilink")，[布雷西亞](../Page/布雷西亞.md "wikilink")，[皮亞琴察](../Page/皮亞琴察.md "wikilink")，[-{zh-hans:里斯本竞技;zh-hk:士砵亭}-和](../Page/里斯本竞技.md "wikilink")[天津泰达隊](../Page/天津泰达.md "wikilink")。
  - 马特拉齐年幼時父親並不贊成他成為足球員，認為他該當職業[籃球員](../Page/籃球.md "wikilink")。
  - 马特拉齐已婚，育有二子一女，大兒子叫吉安馬科，二兒子叫大衛，女兒叫安娜。
  - [若泽·穆里尼奥於](../Page/若泽·穆里尼奥.md "wikilink")2010年協助國米成為[三冠王](../Page/三冠王.md "wikilink")，在若泽·穆里尼奥離開國米時，記者拍攝到馬達拉斯和若泽·穆里尼奥擁抱，馬達拉斯認為若泽·穆里尼奥是世界第一的[領隊](../Page/領隊.md "wikilink")。\[8\]

## 参考文献

<div class="references-small">

<references />

</div>

## 相关链接

  - [马特拉齐个人网站](http://www.marcomaterazzi.it)
  - [Profile and
    stats](http://www.footballdatabase.com/site/players/index.php?dumpPlayer=99)

[Category:意大利足球运动员](../Category/意大利足球运动员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:義大利國家足球隊球員](../Category/義大利國家足球隊球員.md "wikilink")
[Category:佩魯賈球員](../Category/佩魯賈球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")

1.  YouTube: [Zidane VS
    Materazzi](http://www.youtube.com/watch?v=XGFVhzmnYWg)

2.  ESPN：[Materazzi reveals details of Zidane World Cup
    slur](http://soccernet.espn.go.com/news/story?id=378084&cc=4716)

3.  网易体育：[马特拉齐解开激怒齐达内之谜
    侮辱其姐姐是妓女](http://sports.163.com/07/0818/21/3M77K4QK00051CD5.html)

4.
5.  腾讯体育：[齐达内：永不原谅马特拉齐
    支持卡塔尔世界杯](http://sports.qq.com/a/20110210/000003.htm)

6.

7.

8.