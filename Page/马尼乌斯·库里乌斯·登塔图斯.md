**马尼乌斯·库里乌斯·登塔图斯**（Manius Curius
Dentatus，？\~前270年）[古罗马军事活动家和政治家](../Page/古罗马.md "wikilink")，曾4次当选为[执政官](../Page/执政官_\(罗马\).md "wikilink")（前290年，前284年，前275年和前274年），一次当选为[监察官](../Page/监察官.md "wikilink")（前272年）。他因领导[罗马军队取得](../Page/罗马军团.md "wikilink")[第三次萨莫奈战争的胜利而名垂史册](../Page/第三次萨莫奈战争.md "wikilink")。

库里乌斯·登塔图斯出身于[平民](../Page/平民.md "wikilink")[氏族](../Page/氏族.md "wikilink")。据[老普林尼记载](../Page/老普林尼.md "wikilink")，他生下来就有[牙](../Page/牙.md "wikilink")，因此得到“登塔图斯”（意为“有牙的”）这个绰号作为其家族的[第三名](../Page/古罗马人命名习俗.md "wikilink")。

库里乌斯·登塔图斯可谓常胜将军。他在前290年第一次执政，并于当年决定性地打败了反抗罗马的[萨莫奈人及其盟友](../Page/萨莫奈人.md "wikilink")[萨宾人](../Page/萨宾人.md "wikilink")，从而结束了第三次萨莫奈战争（前298年至前290年）。这次战争是罗马统一全意大利过程中的重要事件之一。经过此役，罗马人控制了从[波河到](../Page/波河.md "wikilink")[卢卡尼亚之间的全部土地](../Page/卢卡尼亚.md "wikilink")。根据某些史料，库里乌斯·登塔图斯因为这次战役的胜利而得到了两次[凯旋式](../Page/凯旋式.md "wikilink")（一次为战胜萨莫奈人，一次为战胜萨宾人）。前284年执政官[卢基乌斯·卡埃基利乌斯·梅特鲁斯·登特尔在与](../Page/卢基乌斯·卡埃基利乌斯·梅特鲁斯·登特尔.md "wikilink")[高卢人](../Page/高卢人.md "wikilink")（他们因为畏惧罗马的威胁而支持萨莫奈人）的战斗中阵亡，库里乌斯·登塔图斯成为补任执政官，并迅速打败了高卢人。前275年他第三次任执政官，在[贝内文托战役中挫败了前来支援南意大利各](../Page/贝内文托战役.md "wikilink")[希腊城邦的](../Page/希腊城邦.md "wikilink")[伊庇鲁斯国王](../Page/伊庇鲁斯.md "wikilink")[皮洛士](../Page/皮洛士.md "wikilink")，迫使后者离开意大利。此役使他获得广大罗马人民的拥护。在前274年最后一个执政官任内，他打败了[卢卡尼人](../Page/卢卡尼人.md "wikilink")。

在内政方面，库里乌斯·登塔图斯以廉洁和简朴著称。他曾经负责部分排干[维利努斯湖的工作](../Page/维利努斯湖.md "wikilink")（前289年），并在前272年任监察官时领导修建罗马的第二条[水道](../Page/罗马水道.md "wikilink")。

## 资料来源

  - [李维](../Page/李维.md "wikilink")、[波利比阿](../Page/波利比阿.md "wikilink")、[普卢塔克等人的著作](../Page/普卢塔克.md "wikilink")

[Category:前270年去世](../Category/前270年去世.md "wikilink")
[Category:罗马执政官](../Category/罗马执政官.md "wikilink")
[Category:古罗马政治人物](../Category/古罗马政治人物.md "wikilink")
[Category:古罗马军事人物](../Category/古罗马军事人物.md "wikilink")
[Category:罗马共和国执政官](../Category/罗马共和国执政官.md "wikilink")