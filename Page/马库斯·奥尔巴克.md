**马库斯·克里斯蒂安·奥尔巴克**（，），是一名前[瑞典足球运动员](../Page/瑞典.md "wikilink")，司职前锋。

柯碧克曾在多個歐洲聯賽球會效力，包括[意甲的](../Page/意甲.md "wikilink")[-{巴里}-](../Page/巴里足球俱乐部.md "wikilink")、[荷蘭的](../Page/荷蘭.md "wikilink")[海倫芬](../Page/海倫芬足球會.md "wikilink")、英超的[阿士東維拉](../Page/阿斯顿维拉足球俱乐部.md "wikilink")、德國的[羅斯托克](../Page/汉莎罗斯托克足球俱乐部.md "wikilink")。他亦曾入選[瑞典國家隊出戰](../Page/瑞典国家足球队.md "wikilink")2002年世界杯、2004年歐洲國家杯和[2006年世界杯](../Page/2006年世界盃足球賽.md "wikilink")。

在2006年世界杯，柯碧克在對英格蘭的賽事以2比2打成平手，其中柯碧克的入球成為過去十八屆世界杯中第二千個入球。

## 榮譽

  - [哥本哈根](../Page/哥本哈根足球會.md "wikilink")

<!-- end list -->

  - [丹麥足球超級聯賽](../Page/丹麥足球超級聯賽.md "wikilink") **冠軍**(2): 2005-06,
    2006-07

[奧基迪](../Page/奥尔格里特足球俱乐部.md "wikilink")

  - [瑞典盃](../Page/瑞典盃.md "wikilink") **冠軍**: 2000

**個人**

  - 哥本哈根最佳球員: 2006-07

[Category:瑞典足球運動員](../Category/瑞典足球運動員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:瑞超球員](../Category/瑞超球員.md "wikilink")
[Category:丹超球員](../Category/丹超球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:奧基迪球員](../Category/奧基迪球員.md "wikilink")
[Category:寧比球員](../Category/寧比球員.md "wikilink")
[Category:巴里球員](../Category/巴里球員.md "wikilink")
[Category:海倫芬球員](../Category/海倫芬球員.md "wikilink")
[Category:阿士東維拉球員](../Category/阿士東維拉球員.md "wikilink")
[Category:羅斯托克球員](../Category/羅斯托克球員.md "wikilink")
[Category:哥本哈根球員](../Category/哥本哈根球員.md "wikilink")
[Category:意大利外籍足球運動員](../Category/意大利外籍足球運動員.md "wikilink")
[Category:荷蘭外籍足球運動員](../Category/荷蘭外籍足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:丹麥外籍足球運動員](../Category/丹麥外籍足球運動員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")