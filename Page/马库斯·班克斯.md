**阿瑟·勒马库斯·班克斯三世**（，），出生于[内华达州](../Page/内华达州.md "wikilink")[拉斯维加斯](../Page/拉斯维加斯.md "wikilink")，[美国职业篮球运动员](../Page/美国.md "wikilink")，司职[控球后卫](../Page/控球后卫.md "wikilink")。

## 生涯

### 内华达大学拉斯维加斯分校

身高6尺2寸 (188 cm)体重200磅
(90公斤)的班克斯在犹他迪克斯州立学院打了两个赛季后，班克斯转校去了内华达大学拉斯维加斯分校。\[1\]他在四年级时期和人分享了年度最佳防守球员。

### 波士顿凯尔特人时期

班克斯在[2003年NBA选秀中第一轮第](../Page/2003年NBA选秀.md "wikilink")13顺位被[孟菲斯灰熊队选中](../Page/孟菲斯灰熊队.md "wikilink")，随后和凯尔特人的[肯德里克·帕金斯进行了交换](../Page/肯德里克·帕金斯.md "wikilink")。
在波士顿班克斯以良好的防守和处理球能力著称，但是糟糕的投篮和对球选择方面是他的弱点。加上主教练[道格·里弗斯对新秀控卫](../Page/道格·里弗斯.md "wikilink")[德朗特·韦斯特偏爱有加](../Page/德朗特·韦斯特.md "wikilink")，直接导致凯尔特人总经理[丹尼·安吉最终放弃了让班克斯留在队中的想法](../Page/丹尼·安吉.md "wikilink")。

### 明尼苏达森林狼时期

2006年1月26日，
班克斯连同[里基·戴维斯](../Page/里基·戴维斯.md "wikilink")、[马克·布朗特](../Page/马克·布朗特.md "wikilink")、[贾斯汀·里德和一个第二轮的选秀权交换去](../Page/贾斯汀·里德.md "wikilink")[明尼苏达森林狼队以交换](../Page/明尼苏达森林狼队.md "wikilink")[沃利·斯泽比亚克](../Page/沃利·斯泽比亚克.md "wikilink")、[迈克尔·奥洛沃坎迪](../Page/迈克尔·奥洛沃坎迪.md "wikilink")、[德韦恩·琼斯以及一个有条件的首轮选秀名额](../Page/德韦恩·琼斯.md "wikilink")。\[2\]

### 菲尼克斯太阳时期

班克斯在2006年7月18日以自由球员身份加盟了菲尼克斯太阳队。\[3\]

### 邁阿密熱火时期

班克斯在2008年2月6日與[肖恩·馬里昂一起被交易至](../Page/肖恩·馬里昂.md "wikilink")[邁阿密熱火隊](../Page/邁阿密熱火隊.md "wikilink")，以換取[沙奎爾·奧尼爾](../Page/沙奎爾·奧尼爾.md "wikilink")。\[4\]

## 参考资料

<references/>

## 外部链接

  - [班克斯NBA官方资料](http://www.nba.com/playerfile/marcus_banks/index.html)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:非洲裔美国人](../Category/非洲裔美国人.md "wikilink")
[Category:波士顿凯尔特人队球员](../Category/波士顿凯尔特人队球员.md "wikilink")
[Category:明尼苏达森林狼队球员](../Category/明尼苏达森林狼队球员.md "wikilink")
[Category:菲尼克斯太阳队球员](../Category/菲尼克斯太阳队球员.md "wikilink")
[Category:迈阿密热火队球员](../Category/迈阿密热火队球员.md "wikilink")
[Category:多伦多猛龙队球员](../Category/多伦多猛龙队球员.md "wikilink")

1.  [Marcus Banks NBA.com
    bio](http://www.nba.com/playerfile/marcus_banks/bio.html)
2.  [Szczerbiak, Davis change teams in seven-player
    trade](http://sports.espn.go.com/nba/news/story?id=2307791),
    更新于2006年1月
3.  <http://sports.espn.go.com/nba/news/story?id=2524053>
4.  <http://sports.espn.go.com/nba/news/story?id=3234099>