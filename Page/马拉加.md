**马拉加**（，）是位于[西班牙南部](../Page/西班牙.md "wikilink")[安达卢西亚](../Page/安达卢西亚.md "wikilink")、[地中海](../Page/地中海.md "wikilink")[太阳海岸的一个城市](../Page/太阳海岸.md "wikilink")，也是[马拉加省的省会](../Page/马拉加省.md "wikilink")，西班牙的第二大港口。据2015年估算，它拥有人口572,947人。而整个马拉加都市圈人口约为1,074,074，是西班牙五大都市圈之一。马拉加被群山和两条注入地中海的河流所环抱，其主要收入来源于农业和旅游观光业。與[伊比利亞的其他城市一樣](../Page/伊比利亞.md "wikilink")，融入了[伊斯蘭和](../Page/伊斯蘭.md "wikilink")[基督的文化](../Page/基督.md "wikilink")。

画家[巴勃罗·毕加索](../Page/巴勃罗·毕加索.md "wikilink")、19世纪政治家[安东尼奥·卡诺瓦斯·德尔卡斯蒂略和演员](../Page/安东尼奥·卡诺瓦斯·德尔卡斯蒂略.md "wikilink")[安东尼奥·班德拉斯均出生在马拉加](../Page/安东尼奥·班德拉斯.md "wikilink")。

## 历史

约公元前1000年，由[腓尼基人興建了Malaka](../Page/腓尼基.md "wikilink")，这个名字可能源自[腓尼基语的](../Page/腓尼基语.md "wikilink")“盐”\[1\]，因为此地港口附近的鱼都比较鹹。

约七个世纪以后，[罗马帝国征服包括马拉加在内的](../Page/罗马帝国.md "wikilink")[迦太基西班牙地区](../Page/迦太基.md "wikilink")。从五世纪开始，[西哥特人将马拉加置于统治之下](../Page/西哥特人.md "wikilink")。8世纪，隨著[摩尔人征服了西班牙](../Page/摩尔人.md "wikilink")，马拉加成为重要的贸易中心。在此期间，它被称为Mālaqah。統治勢力一直變更到reconquista的末期，马拉加又重新成为基督教占主导的城市，不斷發展至今。

在1936年[西班牙内战期间](../Page/西班牙内战.md "wikilink")，马拉加遭遇了[意大利和西班牙叛军的空袭](../Page/意大利.md "wikilink")。1960年代，城市附近的[太阳海岸极大地推进了马拉加经济的发展](../Page/太阳海岸.md "wikilink")。

## 地理

### 位置

<center>

|                                                                                                                  |                                                                                                                                                      |                                                                                                                                                |
| ---------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| width ="35%" align="center" | *西北:* [阿尔莫希亚](../Page/阿尔莫希亚.md "wikilink")                                         | width ="30%" align="center" | *北:* [卡萨韦尔梅哈](../Page/卡萨韦尔梅哈.md "wikilink")                                                                            | width ="35%" align="center" | *东北:* [科尔梅纳尔和](../Page/科尔梅纳尔.md "wikilink")[科马雷斯](../Page/科马雷斯.md "wikilink")                                    |
| width ="10%" align="center" | *西:* [卡尔塔马和](../Page/卡尔塔马.md "wikilink")[阿尔奥林德拉托雷](../Page/阿尔奥林德拉托雷.md "wikilink") | width ="35%" align="center" | [Rosa_de_los_vientos.svg](https://zh.wikipedia.org/wiki/File:Rosa_de_los_vientos.svg "fig:Rosa_de_los_vientos.svg") | width ="30%" align="center" | *东:* [埃尔沃尔赫](../Page/埃尔沃尔赫.md "wikilink"), [莫克利内霍和](../Page/莫克利内霍.md "wikilink")[托塔兰](../Page/托塔兰.md "wikilink") |
| width ="35%" align="center" | *西南:* [托雷莫利诺斯](../Page/托雷莫利诺斯.md "wikilink")                                       | width ="30%" align="center" | *南:* [地中海](../Page/地中海.md "wikilink")                                                                                  | width ="35%" align="center" | *东南:* [地中海](../Page/地中海.md "wikilink")                                                                           |

</center>

### 自然地理

[马拉加山脉穿过该市东北部](../Page/马拉加山脉.md "wikilink")，山脉最高点1032米。\[2\]该市西侧是[马哈斯山脉](../Page/马哈斯山脉.md "wikilink")。

马拉加位于[安达卢西亚](../Page/安达卢西亚.md "wikilink")-[地中海盆地](../Page/地中海.md "wikilink")，有两条重要河流经过该市：[瓜达洛塞河和](../Page/瓜达洛塞河.md "wikilink")[瓜达尔麦迪娜河](../Page/瓜达尔麦迪娜河.md "wikilink")，这两条河流最终注入地中海，并在此地形成了一个沿海冲积平原。然而降雨的不规则性导致这两条河流的水道在夏季通常是干燥状态。\[3\]

### 生态

[Bahía_de_Málaga.jpg](https://zh.wikipedia.org/wiki/File:Bahía_de_Málaga.jpg "fig:Bahía_de_Málaga.jpg")远望马拉加湾\]\]
马拉加附近地区拥有地中海原生植被：如[冬青櫟](../Page/冬青櫟.md "wikilink")、[西班牙栓皮栎以及](../Page/西班牙栓皮栎.md "wikilink")[栗](../Page/栗.md "wikilink")、[胡桃](../Page/普通胡桃.md "wikilink")、[杨树](../Page/杨属.md "wikilink")、[梣树](../Page/梣树.md "wikilink")、[草莓樹和](../Page/草莓樹.md "wikilink")[长角豆等](../Page/长角豆.md "wikilink")。其他植被还有[帚石楠](../Page/帚石楠.md "wikilink")、[唇形花](../Page/唇形科.md "wikilink")、[叢櫚](../Page/叢櫚.md "wikilink")、[百里香](../Page/百里香属.md "wikilink")、[迷迭香和](../Page/迷迭香.md "wikilink")[芦笋等](../Page/芦笋.md "wikilink")。在灌木丛和[松树林中](../Page/松树.md "wikilink")，还有[金雀儿](../Page/金雀儿.md "wikilink")、[刺柏和](../Page/刺柏属.md "wikilink")[白歐石楠](../Page/白歐石楠.md "wikilink")。\[4\]

马拉加的河流入海口[三角洲有一片](../Page/三角洲.md "wikilink")[河流湿地](../Page/河流湿地.md "wikilink")，这里有[芦苇](../Page/芦苇.md "wikilink")、[香附](../Page/香附.md "wikilink")、[碱蓬等](../Page/碱蓬属.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")，也有[杨树](../Page/杨属.md "wikilink")、[桉树](../Page/桉树.md "wikilink")、[柳树](../Page/柳树.md "wikilink")、[红荆和](../Page/红荆.md "wikilink")[棕榈等](../Page/棕榈科.md "wikilink")[木本植物](../Page/木本植物.md "wikilink")。\[5\]

马拉加市地区拥有大量的动物物种。据计算，这里已有超过151种[脊椎动物和更多的](../Page/脊椎动物.md "wikilink")[无脊椎动物](../Page/无脊椎动物.md "wikilink")。\[6\]在入海口[湿地](../Page/湿地.md "wikilink")，拥有着大量[爬行动物和](../Page/爬行动物.md "wikilink")[两栖动物](../Page/两栖动物.md "wikilink")，如[火烈鸟和](../Page/火烈鸟.md "wikilink")[鹳](../Page/鹳形目.md "wikilink")，以及更加常见的[红嘴巨鸥](../Page/红嘴巨鸥.md "wikilink")、[牛背鹭](../Page/牛背鹭.md "wikilink")、[燕鷗和](../Page/燕鷗.md "wikilink")[鸭类](../Page/鸭科.md "wikilink")。\[7\]此外，马拉加湾是许多水生动物的繁殖地。在沙子、泥土和岩石底部拥有众多的[软体动物](../Page/软体动物.md "wikilink")，如[蛤蜊和](../Page/蛤蜊.md "wikilink")[鸟蛤](../Page/鸟蛤科.md "wikilink")。其他物种还包括[歐洲鯷](../Page/歐洲鯷.md "wikilink")、[螺和](../Page/螺.md "wikilink")[縱帶羊魚](../Page/縱帶羊魚.md "wikilink")。\[8\]\[9\]

### 氣候

在[柯本气候分类法中](../Page/柯本气候分类法.md "wikilink")，马拉加属于[亚热带](../Page/亚热带.md "wikilink")-[地中海式气候](../Page/地中海式气候.md "wikilink")（*Csa*）。\[10\]冬季温和，夏季炎热。马拉加日照时间较长，年降水天数在40-45天左右。从地中海吹来的季风能够调节马拉加夏季的气温。\[11\]</ref>

<div style="width:75%;">

</div>

## 人口

[Calle_Marques_de_Larios,_Málaga.jpg](https://zh.wikipedia.org/wiki/File:Calle_Marques_de_Larios,_Málaga.jpg "fig:Calle_Marques_de_Larios,_Málaga.jpg")
[Botanic_Garden_La_Concepcion_Malaga.jpg](https://zh.wikipedia.org/wiki/File:Botanic_Garden_La_Concepcion_Malaga.jpg "fig:Botanic_Garden_La_Concepcion_Malaga.jpg")
**马拉加市人口变化图**： <timeline> Colors=

`id:a value:gray(0.9)`
`id:b value:gray(0.7)`
`id:c value:rgb(1,1,1)`
`id:d value:rgb(0.6,0.7,0.8)`

ImageSize = width:750 height:350 PlotArea = left:50 bottom:30 top:30
right:30 DateFormat = x.y Period = from:0 till:600000 TimeAxis =
orientation:vertical AlignBars = justify ScaleMajor = gridcolor:b
increment:100000 start:0 ScaleMinor = gridcolor:a increment:10000
start:0 BackgroundColors = canvas:c

BarData=

`bar:1842 text:1842`
`bar:1857 text:1857`
`bar:1877 text:1877`
`bar:1887 text:1887`
`bar:1900 text:1900`
`bar:1910 text:1910`
`bar:1920 text:1920`
`bar:1930 text:1930`
`bar:1940 text:1940`
`bar:1950 text:1950`
`bar:1960 text:1960`
`bar:1970 text:1970`
`bar:1981 text:1981`
`bar:1991 text:1991`
`bar:2001 text:2001`
`bar:2011 text:2011`
`bar:2017 text:2017`

PlotData=

`color:d width:20 align:left`

`bar:1842 from:0 till: 68271`
`bar:1857 from:0 till: 94293`
`bar:1877 from:0 till: 116143`
`bar:1887 from:0 till: 134016`
`bar:1900 from:0 till: 131063`
`bar:1910 from:0 till: 136365`
`bar:1920 from:0 till: 150584`
`bar:1930 from:0 till: 188010`
`bar:1940 from:0 till: 238085`
`bar:1950 from:0 till: 276222`
`bar:1960 from:0 till: 301048`
`bar:1970 from:0 till: 374452`
`bar:1981 from:0 till: 503251`
`bar:1991 from:0 till: 534683`
`bar:2001 from:0 till: 524414`
`bar:2011 from:0 till: 568030`
`bar:2017 from:0 till: 569002`

PlotData=

`bar:1842 at: 68271 fontsize:s text: 68 271 shift:(-10,5)`
`bar:1857 at: 94293 fontsize:s text: 94 293 shift:(-10,5)`
`bar:1877 at: 116143 fontsize:s text: 116 143 shift:(-10,5)`
`bar:1887 at: 134016 fontsize:s text: 134 016 shift:(-10,5)`
`bar:1900 at: 131063 fontsize:s text: 131 063 shift:(-15,5)`
`bar:1910 at: 136365 fontsize:s text: 136 365 shift:(-15,5)`
`bar:1920 at: 150584 fontsize:s text: 150 584 shift:(-15,5)`
`bar:1930 at: 188010 fontsize:s text: 188 010 shift:(-15,5)`
`bar:1940 at: 238085 fontsize:s text: 238 085 shift:(-15,5)`
`bar:1950 at: 276222 fontsize:s text: 276 222 shift:(-15,5)`
`bar:1960 at: 301048 fontsize:s text: 301 048 shift:(-15,5)`
`bar:1970 at: 374452 fontsize:s text: 374 452 shift:(-15,5)`
`bar:1981 at: 503251 fontsize:s text: 503 251 shift:(-15,5)`
`bar:1991 at: 534683 fontsize:s text: 534 683 shift:(-15,5)`
`bar:2001 at: 524414 fontsize:s text: 524 414 shift:(-15,5)`
`bar:2011 at: 568479 fontsize:S text: 568 479 shift:(-15,5)`
`bar:2017 at: 569009 fontsize:S text: 569 009 shift:(-15,5)`

</timeline>

  -
    <small>来源: INE</small>\[12\]\[13\]

自20世纪末以来，大量国外移民进入马拉加市。外国居民在2011年达到47925人\[14\]，占当时马拉加市人口总数的8.43％。移民的来源主要是[摩洛哥](../Page/摩洛哥.md "wikilink")，[巴拉圭](../Page/巴拉圭.md "wikilink")，[罗马尼亚](../Page/罗马尼亚.md "wikilink")，[乌克兰](../Page/乌克兰.md "wikilink")，[阿根廷](../Page/阿根廷.md "wikilink")，[尼日利亚](../Page/尼日利亚.md "wikilink")，[意大利和](../Page/意大利.md "wikilink")[中华人民共和国](../Page/中华人民共和国.md "wikilink")。\[15\]其中少数族裔[罗姆人社区人口不到](../Page/罗姆人.md "wikilink")2%。\[16\]

2010年，马拉加市人口的平均收入为18588[€](../Page/€.md "wikilink")。\[17\]

**2011年马拉加市外国移民人口排名**

| 国家   | 人口\[18\] | 国家   | 人口\[19\] | 国家   | 人口\[20\] |
| ---- | -------- | ---- | -------- | ---- | -------- |
| **** | 8251     | **** | 2223     | **** | 900      |
| **** | 3818     | **** | 1910     | **** | 818      |
| **** | 3338     | **** | 1478     | **** | 768      |
| **** | 3160     | **** | 1350     | **** | 620      |
| **** | 3129     | **** | 1171     | **** | 536      |
| **** | 2726     | **** | 1073     | **** | 502      |
| **** | 2325     | **** | 788      | **** | 379      |

## 經濟

### 旅游观光

马拉加是著名的旅游胜地，每年会接待大量的游客。有各种非常便宜的包机往返于马拉加和[阿姆斯特丹及](../Page/阿姆斯特丹.md "wikilink")[伦敦之间](../Page/伦敦.md "wikilink")。许多游客到马拉加来享受好天气和美丽的太阳海岸。

### 其他產業

除了農作物以外，還盛產[番紅花和](../Page/番紅花.md "wikilink")[辰砂](../Page/辰砂.md "wikilink")、[珊瑚等](../Page/珊瑚.md "wikilink")。

## 著名人物

[MuseoPicassoMalaga.jpg](https://zh.wikipedia.org/wiki/File:MuseoPicassoMalaga.jpg "fig:MuseoPicassoMalaga.jpg")

  - [伊本·盖比鲁勒](../Page/伊本·盖比鲁勒.md "wikilink")
    （1021–1058）：11世纪[安达卢斯](../Page/安达卢斯.md "wikilink")[犹太学者](../Page/犹太.md "wikilink")\[21\]
  - [安东尼奥·卡诺瓦斯·德尔卡斯蒂略](../Page/安东尼奥·卡诺瓦斯·德尔卡斯蒂略.md "wikilink")
    （1828–1897）：[阿方索十二世时期的](../Page/阿方索十二世.md "wikilink")[西班牙首相](../Page/西班牙首相.md "wikilink")\[22\]
  - [巴勃羅·畢卡索](../Page/巴勃羅·畢卡索.md "wikilink") （1881–1973）：著名画家\[23\]
  - [豪爾赫·蘭多](../Page/豪爾赫·蘭多.md "wikilink")
    （1941–）：[新表现主义画家](../Page/新表现主义.md "wikilink")\[24\]
  - [安东尼奥·班德拉斯](../Page/安东尼奥·班德拉斯.md "wikilink")
    （1960–）：男演员，出生于[貝納爾馬德納](../Page/貝納爾馬德納.md "wikilink")\[25\]
  - [米格·安哲爾·希梅尼茲](../Page/米格·安哲爾·希梅尼茲.md "wikilink")
    （1964–）：职业[高尔夫运动员](../Page/高尔夫.md "wikilink")
  - [保羅·艾波朗](../Page/保羅·艾波朗.md "wikilink") （1989–）：歌手\[26\]
  - [伊斯高](../Page/伊斯高.md "wikilink")
    （1992–）：职业足球运动员，出生于[貝納爾馬德納](../Page/貝納爾馬德納.md "wikilink")\[27\]
  - [米莉·芭比·布朗](../Page/米莉·芭比·布朗.md "wikilink") （2004–）：女演员\[28\]

## 友好城市

[Paolo_Monti_-_Servizio_fotografico_(Milano,_1953)_-_BEIC_6356204.jpg](https://zh.wikipedia.org/wiki/File:Paolo_Monti_-_Servizio_fotografico_\(Milano,_1953\)_-_BEIC_6356204.jpg "fig:Paolo_Monti_-_Servizio_fotografico_(Milano,_1953)_-_BEIC_6356204.jpg")\]\]
马拉加的[友好城市有](../Page/友好城市.md "wikilink")：\[29\]

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/法鲁.md" title="wikilink">法鲁</a>[30]</p></li>
<li><p><a href="../Page/马尼拉.md" title="wikilink">马尼拉</a>[31]</p></li>
<li><p><a href="../Page/墨尔本.md" title="wikilink">墨尔本</a>[32]</p></li>
<li><p><a href="../Page/莫比尔_(阿拉巴马州).md" title="wikilink">莫比尔</a>[33]</p></li>
<li><p><a href="../Page/帕绍.md" title="wikilink">帕绍</a>[34][35]</p></li>
<li><p><a href="../Page/波帕扬.md" title="wikilink">波帕扬</a>[36][37]</p></li>
<li><p><a href="../Page/萨卡特卡斯.md" title="wikilink">萨卡特卡斯</a>[38]</p></li>
</ul></td>
</tr>
</tbody>
</table>

## 图集

<center>

<File:Montesmalaga.jpg>|[马拉加山脉](../Page/马拉加山脉.md "wikilink")
[File:Desembocadura_del_Guadalhorce_at_dusk_08.jpg|马拉加湾东部海岸](File:Desembocadura_del_Guadalhorce_at_dusk_08.jpg%7C马拉加湾东部海岸)[瓜达洛塞河畔](../Page/瓜达洛塞河.md "wikilink")，远处可见[马罗马峰](../Page/马罗马峰.md "wikilink")
<File:PN_Desembocadura_del_Guadalhorce.jpg>|[瓜达洛塞河河口](../Page/瓜达洛塞河.md "wikilink")
[File:Málaga_vista_aérea.jpg|航拍图](File:Málaga_vista_aérea.jpg%7C航拍图)
[File:La_Marina_mlg.jpg|黄昏时的马拉加](File:La_Marina_mlg.jpg%7C黄昏时的马拉加)
[File:Malaga-harbour-2005sep15@16.23.jpg|马拉加港口](File:Malaga-harbour-2005sep15@16.23.jpg%7C马拉加港口)
[File:Muelle_1_Malaga.jpg|港口](File:Muelle_1_Malaga.jpg%7C港口)
[File:View_Malaga_From_Muelle_1.jpg|马拉加港口夜景](File:View_Malaga_From_Muelle_1.jpg%7C马拉加港口夜景)
[File:Palacio_de_la_Aduana,_Málaga_01.JPG|考古博物馆](File:Palacio_de_la_Aduana,_Málaga_01.JPG%7C考古博物馆)
[File:FMCE2013.jpg|马加拉2013年电影节](File:FMCE2013.jpg%7C马加拉2013年电影节)
[File:Mercadoatarazana.jpg|集市](File:Mercadoatarazana.jpg%7C集市)
[File:CAC_Málaga_2013.jpg|CAC](File:CAC_Málaga_2013.jpg%7CCAC)
[File:(622)_Irisbus_Citelis_Castrosua_Magnus_EMT_Malaga.jpg|市内巴士](File:\(622\)_Irisbus_Citelis_Castrosua_Magnus_EMT_Malaga.jpg%7C市内巴士)
[File:Estación_de_Ciudad_de_la_Justicia_18.JPG|地铁](File:Estación_de_Ciudad_de_la_Justicia_18.JPG%7C地铁)

</center>

## 参考文献

## 外部链接

[M](../Category/马拉加省市镇.md "wikilink")

1.  请参考：现代[希伯来语的](../Page/希伯来语.md "wikilink") מלח
    *mélaḥ*或[阿拉伯语的](../Page/阿拉伯语.md "wikilink") ملح
    *malaḥ*）

2.  Instituto de Cartografía de Andalucía (编). [«Mapa del Parque Natural
    Montes de Málaga.
    Escala 1:25.000»](http://www.juntadeandalucia.es/medioambiente/consolidado/publicacionesdigitales/MAPAS/PARQUE_NATURAL_MONTES_DE_MALAGA.JPG).

3.  Junta de Andalucía (编). [«Plan Director de Riberas de Andalucía. 3.
    Confederaciones y cuencas
    hidrográficas»](http://www.juntadeandalucia.es/medioambiente/web/Bloques_Tematicos/Estrategias_Ambientales/Planes/Planes_tematicos/plan_director_riberas/riberas06_3.pdf).

4.

5.

6.
7.
8.

9.

10.

11.
12. INEbase. Variaciones intercensales. Alteraciones de los municipios
    en los censos de población desde
    1842.[Consultado 17-06-2011](http://www.ine.es/intercensal/).

13.

14.

15.
16.

17.

18.
19.
20.
21.

22. Octavio Ruiz, "Spain on the Threshold of a New Century: Society and
    Politics before and after the Disaster of 1898," *Mediterranean
    Historical Review* (June 1998), Vol. 13 Issue 1/2, pp 7–27

23.

24. Pallarés, Carmen. Hamburg Notebooks. Madrid. Victor i Fills Art
    Gallery. ISBN 978-84-613-7714-5. pp. 436-440

25.

26.

27.

28.

29.

30.

31.

32.
33.
34.
35.

36.
37.

38.