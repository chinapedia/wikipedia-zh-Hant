**马斯里足球俱乐部**（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：**النادي المصري للألعاب
الرياضية**; [拉丁文转换](../Page/拉丁文.md "wikilink")：**Al-Masry Sporting
Club**）是埃及足球俱乐部，位于[埃及](../Page/埃及.md "wikilink")[塞得港](../Page/塞得港.md "wikilink")。

球队的主体育场为塞港体育场(Port Said
Stadium)，匈牙利著名球星[费伦茨·普斯卡什曾在](../Page/费伦茨·普斯卡什.md "wikilink")1979/80赛季执教过马斯里足球俱乐部。

## 球队荣誉

  - **[埃及足球杯](../Page/埃及足球杯.md "wikilink")**
      - 冠军: 1998年

## 知名教练

  -  [费伦茨·普斯卡什](../Page/费伦茨·普斯卡什.md "wikilink")

## 参见

  - [塞德港球迷暴力骚乱](../Page/塞德港球迷暴力骚乱.md "wikilink")

## 外部链接

  - [马斯里官方网站](http://www.almasryclub.com/)
  - [Masry fans
    site](https://web.archive.org/web/20110505020947/http://www.masryclubfu.com/)

[Category:埃及足球俱乐部](../Category/埃及足球俱乐部.md "wikilink")