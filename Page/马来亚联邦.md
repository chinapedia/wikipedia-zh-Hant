**马来亚联邦**（****，1946年—1948年）是[第二次世界大战之后](../Page/第二次世界大战.md "wikilink")，[英国](../Page/英国.md "wikilink")[殖民政府为整合](../Page/殖民.md "wikilink")[英属马来亚](../Page/英属马来亚.md "wikilink")，而在[马来半岛所策划的](../Page/马来半岛.md "wikilink")[联邦体制](../Page/联邦.md "wikilink")，属[英国皇家殖民地](../Page/英国皇家殖民地.md "wikilink")，由第一任[总督](../Page/总督.md "wikilink")在1946年4月1日宣布成立。后来在[马来人](../Page/马来人.md "wikilink")[民族主义高涨的反对声浪下](../Page/民族主义.md "wikilink")，于1948年1月31日终止，由[马来亚联合邦所取代](../Page/马来亚联合邦.md "wikilink")。

马来亚联邦由[马来联邦](../Page/马来联邦.md "wikilink")、[马来属邦与](../Page/马来属邦.md "wikilink")[海峡殖民地](../Page/海峡殖民地.md "wikilink")（不包括[新加坡](../Page/新加坡.md "wikilink")）所组成，共11个州属。

## 背景

1945年10月，[二战结束](../Page/二战.md "wikilink")，英国军部有意成立马来亚联邦（计划已于最早公元1944年5月呈于当时的战争内阁）。同一月哈罗德·麦克米歇尔爵士（Sir
Harold
McMichael）受委负责获取各州统治者批准成立马来亚联邦。他很快成功获取各统治者之批准。马来亚联邦方案剥夺许多马来统治者之政治权利，惟马来统治者因有于[日治时期与](../Page/日治时期.md "wikilink")[日军合作之嫌](../Page/日军.md "wikilink")，怕受英殖民政府对付而王位遭罢黜，故同意该方案。

## 成立

公元1946年4月1日，马来亚联邦成立，爱德华·真特爵士（Sir Edward Gent）为其总督，吉隆坡为其首府。

## 制度

## 马来民族运动

[马来人](../Page/马来人.md "wikilink")（巫來由人）反对马来亚联邦通过报纸，和平集会。

1946年1月23日，[翁嘉化投書](../Page/翁嘉化.md "wikilink")《马来前锋报》，呼吁[马来人反对成立马来亚联邦](../Page/马来人.md "wikilink")。

1946年5月11日，全马来亚马来民族大会第三次会议成立马来民族全国统一机构（United Malays National
Organization），简称巫统（UMNO），由翁渣化担任主席。

## 废除

1948年1月30日,
[英国废除马来亚联邦](../Page/英国.md "wikilink")，被[马来亚联合邦取代](../Page/马来亚联合邦.md "wikilink")

## 参看

  - [马来西亚历史](../Page/马来西亚历史.md "wikilink")
  - [马来西亚](../Page/马来西亚.md "wikilink")

[Category:馬來西亞歷史](../Category/馬來西亞歷史.md "wikilink")
[Category:前英国殖民地](../Category/前英国殖民地.md "wikilink")
[Category:已不存在的亞洲國家](../Category/已不存在的亞洲國家.md "wikilink")
[Category:1946年建立的國家或政權](../Category/1946年建立的國家或政權.md "wikilink")