{{ Otheruses|subject=包含了缅甸、泰国和马来西亚国土的半岛|other=该半岛南部的马来西亚国土|马来西亚半岛}}
[LocationMalayPeninsula.png](https://zh.wikipedia.org/wiki/File:LocationMalayPeninsula.png "fig:LocationMalayPeninsula.png")
[Relief_map_of_the_Malay_Peninsula_and_Gulf_of_Thailand.png](https://zh.wikipedia.org/wiki/File:Relief_map_of_the_Malay_Peninsula_and_Gulf_of_Thailand.png "fig:Relief_map_of_the_Malay_Peninsula_and_Gulf_of_Thailand.png")
[Night_Pass_over_Malaysia.ogv](https://zh.wikipedia.org/wiki/File:Night_Pass_over_Malaysia.ogv "fig:Night_Pass_over_Malaysia.ogv")遠征28的隊員所拍摄的马来半岛夜景影片\]\]
[ISS028-E-29803.jpg](https://zh.wikipedia.org/wiki/File:ISS028-E-29803.jpg "fig:ISS028-E-29803.jpg")遠征28的隊員所拍摄的马来半岛夜景\]\]
**马来半岛**（，）位于[亚洲大陆最南端](../Page/亚洲.md "wikilink")，是[東南亞](../Page/東南亞.md "wikilink")[中南半岛的一個主要](../Page/中南半岛.md "wikilink")[半島](../Page/半島.md "wikilink")，与[苏门答腊岛隔着](../Page/苏门答腊.md "wikilink")[马六甲海峡](../Page/马六甲海峡.md "wikilink")，是为[太平洋和](../Page/太平洋.md "wikilink")[印度洋的分界线](../Page/印度洋.md "wikilink")。

半島走向大致從北至南，最狹窄之處在[克拉地峽](../Page/克拉地峽.md "wikilink")，它在[第二次世界大戰時是當地的一個要塞](../Page/第二次世界大戰.md "wikilink")。[蒂迪旺沙山脈構成了半島的主體](../Page/蒂迪旺沙山脈.md "wikilink")。

半岛上有3个国家的[领土](../Page/领土.md "wikilink")：

  - [緬甸领土於半島西北部](../Page/緬甸.md "wikilink")，这部分也是该国领土的最南部；
  - [泰国领土位于半島中部和東北部](../Page/泰国.md "wikilink")；
  - [马来西亚领土位于余下的半岛南部](../Page/马来西亚.md "wikilink")，这部分又稱[馬來西亞半岛](../Page/馬來西亞半岛.md "wikilink")、[馬來亞或](../Page/馬來亞.md "wikilink")[西马](../Page/西马.md "wikilink")。

環繞半島的水體從東北起，按順時針順序分別為：[暹羅灣](../Page/暹羅灣.md "wikilink")、[南中國海](../Page/南中國海.md "wikilink")（與[砂拉越](../Page/砂拉越.md "wikilink")、[汶萊](../Page/汶萊.md "wikilink")、[纳闽](../Page/纳闽.md "wikilink")、[沙巴相對](../Page/沙巴.md "wikilink")）、[柔佛海峡](../Page/柔佛海峡.md "wikilink")（與[新加坡](../Page/新加坡.md "wikilink")、[廖內群島相對](../Page/廖內群島.md "wikilink")）、[馬六甲海峽](../Page/馬六甲海峽.md "wikilink")（與[蘇門答臘相對](../Page/蘇門答臘.md "wikilink")）及[安達曼海](../Page/安達曼海.md "wikilink")。

## 參看

  - [馬來群島](../Page/馬來群島.md "wikilink")
  - [蒂迪旺沙山脈](../Page/蒂迪旺沙山脈.md "wikilink")

[马来半岛](../Category/马来半岛.md "wikilink")