[Dewan_Undangan_Negeri_2008_Equal_Area.svg](https://zh.wikipedia.org/wiki/File:Dewan_Undangan_Negeri_2008_Equal_Area.svg "fig:Dewan_Undangan_Negeri_2008_Equal_Area.svg")
[Dewan_Undangan_Negeri_2008_Equal_Area_(changes).svg](https://zh.wikipedia.org/wiki/File:Dewan_Undangan_Negeri_2008_Equal_Area_\(changes\).svg "fig:Dewan_Undangan_Negeri_2008_Equal_Area_(changes).svg")
**马来西亚第12届州议员列表**所列者为2008年3月8日[马来西亚第12届全国大选时与](../Page/2008年马来西亚大选.md "wikilink")[马来西亚国会同步选举的](../Page/马来西亚国会.md "wikilink")12个州议会在选举中所产生的州议员，不包括2006年及2011年产生的[砂拉越州议员](../Page/砂拉越.md "wikilink")。

以下列表也包含州议会届满前的卸任与补选变动。

__NOTOC__

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **目录:** [玻璃市](../Page/#玻璃市.md "wikilink") | [吉打](../Page/#吉打.md "wikilink") | [吉兰丹](../Page/#吉兰丹.md "wikilink") | [登嘉楼](../Page/#登嘉楼.md "wikilink") | [槟城](../Page/#槟城.md "wikilink") | [霹雳](../Page/#霹雳.md "wikilink") | [彭亨](../Page/#彭亨.md "wikilink") | [雪兰莪](../Page/#雪兰莪.md "wikilink") | [森美兰](../Page/#森美兰.md "wikilink") | [马六甲](../Page/#马六甲.md "wikilink") | [柔佛](../Page/#柔佛.md "wikilink") | [沙巴](../Page/#沙巴.md "wikilink") | [总议席数](../Page/#总议席数.md "wikilink") |

<table>
<tbody>
<tr class="odd">
<td><h3 id="玻璃市"><a href="../Page/玻璃市.md" title="wikilink">玻璃市</a></h3>
<p><strong>13</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>2</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="吉打"><a href="../Page/吉打.md" title="wikilink">吉打</a></h3>
<p><strong>14</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>1</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>5</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>16</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="吉兰丹"><a href="../Page/吉兰丹.md" title="wikilink">吉兰丹</a></h3>
<p><strong>7</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>1</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>37</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
<tr class="odd">
<td><p>N37</p></td>
</tr>
<tr class="even">
<td><p>N38</p></td>
</tr>
<tr class="odd">
<td><p>N39</p></td>
</tr>
<tr class="even">
<td><p>N40</p></td>
</tr>
<tr class="odd">
<td><p>N41</p></td>
</tr>
<tr class="even">
<td><p>N42</p></td>
</tr>
<tr class="odd">
<td><p>N43</p></td>
</tr>
<tr class="even">
<td><p>N44</p></td>
</tr>
<tr class="odd">
<td><p>N45</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="登嘉楼"><a href="../Page/登嘉楼.md" title="wikilink">登嘉楼</a></h3>
<p><strong>24</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>8</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="槟城"><a href="../Page/槟城.md" title="wikilink">槟城</a></h3>
<p>国阵 <strong>11</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>19</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>9</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>1</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
<tr class="odd">
<td><p>N37</p></td>
</tr>
<tr class="even">
<td><p>N38</p></td>
</tr>
<tr class="odd">
<td><p>N39</p></td>
</tr>
<tr class="even">
<td><p>N40</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="霹雳"><a href="../Page/霹雳州.md" title="wikilink">霹雳</a></h3>
<p>国阵 <strong>28</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>17</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>5</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>6</strong> |<strong>独立人士</strong> 3</p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
<tr class="odd">
<td><p>N37</p></td>
</tr>
<tr class="even">
<td><p>N38</p></td>
</tr>
<tr class="odd">
<td><p>N39</p></td>
</tr>
<tr class="even">
<td><p>N40</p></td>
</tr>
<tr class="odd">
<td><p>N41</p></td>
</tr>
<tr class="even">
<td><p>N42</p></td>
</tr>
<tr class="odd">
<td><p>N43</p></td>
</tr>
<tr class="even">
<td><p>N44</p></td>
</tr>
<tr class="odd">
<td><p>N45</p></td>
</tr>
<tr class="even">
<td><p>N46</p></td>
</tr>
<tr class="odd">
<td><p>N47</p></td>
</tr>
<tr class="even">
<td><p>N48</p></td>
</tr>
<tr class="odd">
<td><p>N49</p></td>
</tr>
<tr class="even">
<td><p>N50</p></td>
</tr>
<tr class="odd">
<td><p>N51</p></td>
</tr>
<tr class="even">
<td><p>N52</p></td>
</tr>
<tr class="odd">
<td><p>N53</p></td>
</tr>
<tr class="even">
<td><p>N54</p></td>
</tr>
<tr class="odd">
<td><p>N55</p></td>
</tr>
<tr class="even">
<td><p>N56</p></td>
</tr>
<tr class="odd">
<td><p>N57</p></td>
</tr>
<tr class="even">
<td><p>N58</p></td>
</tr>
<tr class="odd">
<td><p>N59</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="彭亨"><a href="../Page/彭亨.md" title="wikilink">彭亨</a></h3>
<p>国阵 <strong>36</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>2</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>2</strong>| <strong>独立人士 1</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
<tr class="odd">
<td><p>N37</p></td>
</tr>
<tr class="even">
<td><p>N38</p></td>
</tr>
<tr class="odd">
<td><p>N39</p></td>
</tr>
<tr class="even">
<td><p>N40</p></td>
</tr>
<tr class="odd">
<td><p>N41</p></td>
</tr>
<tr class="even">
<td><p>N42</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="雪兰莪"><a href="../Page/雪兰莪.md" title="wikilink">雪兰莪</a></h3>
<p>国阵 <strong>21</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>13</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>14</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>8</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
<tr class="odd">
<td><p>N37</p></td>
</tr>
<tr class="even">
<td><p>N38</p></td>
</tr>
<tr class="odd">
<td><p>N39</p></td>
</tr>
<tr class="even">
<td><p>N40</p></td>
</tr>
<tr class="odd">
<td><p>N41</p></td>
</tr>
<tr class="even">
<td><p>N42</p></td>
</tr>
<tr class="odd">
<td><p>N43</p></td>
</tr>
<tr class="even">
<td><p>N44</p></td>
</tr>
<tr class="odd">
<td><p>N45</p></td>
</tr>
<tr class="even">
<td><p>N46</p></td>
</tr>
<tr class="odd">
<td><p>N47</p></td>
</tr>
<tr class="even">
<td><p>N48</p></td>
</tr>
<tr class="odd">
<td><p>N49</p></td>
</tr>
<tr class="even">
<td><p>N50</p></td>
</tr>
<tr class="odd">
<td><p>N51</p></td>
</tr>
<tr class="even">
<td><p>N52</p></td>
</tr>
<tr class="odd">
<td><p>N53</p></td>
</tr>
<tr class="even">
<td><p>N54</p></td>
</tr>
<tr class="odd">
<td><p>N55</p></td>
</tr>
<tr class="even">
<td><p>N56</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="森美兰"><a href="../Page/森美兰.md" title="wikilink">森美兰</a></h3>
<p>国阵 <strong>21</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>10</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>4</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>1</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="马六甲"><a href="../Page/马六甲.md" title="wikilink">马六甲</a></h3>
<p>国阵 <strong>22</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>5</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>0</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="柔佛"><a href="../Page/柔佛.md" title="wikilink">柔佛</a></h3>
<p>国阵 <strong>50</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>4</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>2</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
<tr class="odd">
<td><p>N37</p></td>
</tr>
<tr class="even">
<td><p>N38</p></td>
</tr>
<tr class="odd">
<td><p>N39</p></td>
</tr>
<tr class="even">
<td><p>N40</p></td>
</tr>
<tr class="odd">
<td><p>N41</p></td>
</tr>
<tr class="even">
<td><p>N42</p></td>
</tr>
<tr class="odd">
<td><p>N43</p></td>
</tr>
<tr class="even">
<td><p>N44</p></td>
</tr>
<tr class="odd">
<td><p>N45</p></td>
</tr>
<tr class="even">
<td><p>N46</p></td>
</tr>
<tr class="odd">
<td><p>N47</p></td>
</tr>
<tr class="even">
<td><p>N48</p></td>
</tr>
<tr class="odd">
<td><p>N49</p></td>
</tr>
<tr class="even">
<td><p>N50</p></td>
</tr>
<tr class="odd">
<td><p>N51</p></td>
</tr>
<tr class="even">
<td><p>N52</p></td>
</tr>
<tr class="odd">
<td><p>N53</p></td>
</tr>
<tr class="even">
<td><p>N54</p></td>
</tr>
<tr class="odd">
<td><p>N55</p></td>
</tr>
<tr class="even">
<td><p>N56</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="沙巴"><a href="../Page/沙巴.md" title="wikilink">沙巴</a></h3>
<p>国阵 <strong>59</strong> | <a href="https://zh.wikipedia.org/wiki/File:DAP-100px.jpg" title="fig:DAP-100px.jpg">DAP-100px.jpg</a> <strong>1</strong> | <a href="https://zh.wikipedia.org/wiki/File:PKR-100px.jpg" title="fig:PKR-100px.jpg">PKR-100px.jpg</a> <strong>0</strong> | <a href="https://zh.wikipedia.org/wiki/File:PAS_logo.svg" title="fig:PAS_logo.svg">PAS_logo.svg</a> <strong>0</strong></p></td>
</tr>
<tr class="even">
<td><p>选区编号</p></td>
</tr>
<tr class="odd">
<td><p>N1</p></td>
</tr>
<tr class="even">
<td><p>N2</p></td>
</tr>
<tr class="odd">
<td><p>N3</p></td>
</tr>
<tr class="even">
<td><p>N4</p></td>
</tr>
<tr class="odd">
<td><p>N5</p></td>
</tr>
<tr class="even">
<td><p>N6</p></td>
</tr>
<tr class="odd">
<td><p>N7</p></td>
</tr>
<tr class="even">
<td><p>N8</p></td>
</tr>
<tr class="odd">
<td><p>N9</p></td>
</tr>
<tr class="even">
<td><p>N10</p></td>
</tr>
<tr class="odd">
<td><p>N11</p></td>
</tr>
<tr class="even">
<td><p>N12</p></td>
</tr>
<tr class="odd">
<td><p>N13</p></td>
</tr>
<tr class="even">
<td><p>N14</p></td>
</tr>
<tr class="odd">
<td><p>N15</p></td>
</tr>
<tr class="even">
<td><p>N16</p></td>
</tr>
<tr class="odd">
<td><p>N17</p></td>
</tr>
<tr class="even">
<td><p>N18</p></td>
</tr>
<tr class="odd">
<td><p>N19</p></td>
</tr>
<tr class="even">
<td><p>N20</p></td>
</tr>
<tr class="odd">
<td><p>N21</p></td>
</tr>
<tr class="even">
<td><p>N22</p></td>
</tr>
<tr class="odd">
<td><p>N23</p></td>
</tr>
<tr class="even">
<td><p>N24</p></td>
</tr>
<tr class="odd">
<td><p>N25</p></td>
</tr>
<tr class="even">
<td><p>N26</p></td>
</tr>
<tr class="odd">
<td><p>N27</p></td>
</tr>
<tr class="even">
<td><p>N28</p></td>
</tr>
<tr class="odd">
<td><p>N29</p></td>
</tr>
<tr class="even">
<td><p>N30</p></td>
</tr>
<tr class="odd">
<td><p>N31</p></td>
</tr>
<tr class="even">
<td><p>N32</p></td>
</tr>
<tr class="odd">
<td><p>N33</p></td>
</tr>
<tr class="even">
<td><p>N34</p></td>
</tr>
<tr class="odd">
<td><p>N35</p></td>
</tr>
<tr class="even">
<td><p>N36</p></td>
</tr>
<tr class="odd">
<td><p>N37</p></td>
</tr>
<tr class="even">
<td><p>N38</p></td>
</tr>
<tr class="odd">
<td><p>N39</p></td>
</tr>
<tr class="even">
<td><p>N40</p></td>
</tr>
<tr class="odd">
<td><p>N41</p></td>
</tr>
<tr class="even">
<td><p>N42</p></td>
</tr>
<tr class="odd">
<td><p>N43</p></td>
</tr>
<tr class="even">
<td><p>N44</p></td>
</tr>
<tr class="odd">
<td><p>N45</p></td>
</tr>
<tr class="even">
<td><p>N46</p></td>
</tr>
<tr class="odd">
<td><p>N47</p></td>
</tr>
<tr class="even">
<td><p>N48</p></td>
</tr>
<tr class="odd">
<td><p>N49</p></td>
</tr>
<tr class="even">
<td><p>N50</p></td>
</tr>
<tr class="odd">
<td><p>N51</p></td>
</tr>
<tr class="even">
<td><p>N52</p></td>
</tr>
<tr class="odd">
<td><p>N53</p></td>
</tr>
<tr class="even">
<td><p>N54</p></td>
</tr>
<tr class="odd">
<td><p>N55</p></td>
</tr>
<tr class="even">
<td><p>N56</p></td>
</tr>
<tr class="odd">
<td><p>N57</p></td>
</tr>
<tr class="even">
<td><p>N58</p></td>
</tr>
<tr class="odd">
<td><p>N59</p></td>
</tr>
<tr class="even">
<td><p>N60</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><h3 id="总议席数">总议席数</h3></td>
</tr>
<tr class="even">
<td><p>国阵</p></td>
</tr>
<tr class="odd">
<td><p>308</p></td>
</tr>
</tbody>
</table>

## 参见

  - [2008年马来西亚大选](../Page/2008年马来西亚大选.md "wikilink")
  - [马来西亚第12届国会议员列表](../Page/马来西亚第12届国会议员列表.md "wikilink")

## 注释

<div class="references-small">

</div>

## 参考资料

[Category:马来西亚选举](../Category/马来西亚选举.md "wikilink")
[Category:马来西亚政治](../Category/马来西亚政治.md "wikilink")
[Category:马来西亚各州议员](../Category/马来西亚各州议员.md "wikilink")