[Muszaphar_shukor.jpg](https://zh.wikipedia.org/wiki/File:Muszaphar_shukor.jpg "fig:Muszaphar_shukor.jpg")
**马来西亚马来人** （）
是[马来西亚最大的](../Page/马来西亚.md "wikilink")[民族](../Page/民族.md "wikilink")，根据2010年马来西亚人口普查数据显示，马来西亚总人口为2601万，马来人占54.6%
、华人占24.6%、印度裔占7.3%、其他占民族7.8%。
根据[马来西亚联邦宪法第](../Page/马来西亚联邦宪法.md "wikilink")160条的释文，“马来人”的定义如下：

  - 信奉[伊斯兰教](../Page/伊斯兰教.md "wikilink")、能操[马来语](../Page/马来语.md "wikilink")、奉行马来传统民俗及文化；
  - 在[独立日](../Page/马来西亚国庆日.md "wikilink")前出生于联邦或[新加坡](../Page/新加坡.md "wikilink")、或者父母任何一方出生于联邦或新加坡、或者独立日当日正定居于联邦或新加坡。
  - 或者是上述人们的后裔。

## 定義

对于[独立日前的就居住在马来亚及新加坡的居民来说](../Page/独立日.md "wikilink")，这个定义是宽松的，只要当时在语言、宗教及文化上是认同马来风俗，就可以被视为马来人，因此它不同于人类学的马来民族。

然而，对于在独立日以后才有意归化为马来人的第三代以上非马来人来说，除了要在语言、宗教和文化认同之外，还必须拥有上述人们的血统，才可以成为马来人。比如非土著和马来人通婚的后代，一般都会被自动归类为马来人；而单单信奉伊斯兰教、说马来语及奉行马来文化的非马来人则不会。

## 語言

[馬來語屬於](../Page/馬來語.md "wikilink")[南島語系](../Page/南島語系.md "wikilink")，在[馬來西亞](../Page/馬來西亞.md "wikilink")，[印度尼西亞](../Page/印度尼西亞.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")，[文萊和](../Page/文萊.md "wikilink")[泰國被使用](../Page/泰國.md "wikilink")。標準馬來語的使用總數約為1800萬人，還有約1.7億人說[印尼語](../Page/印尼語.md "wikilink")，為一種馬來語方言的形式。

## 文化

1500年前，[三佛齊王國便與來自中國和印度的商人做貿易](../Page/三佛齊.md "wikilink")。帶來了黃金和絲綢的到來，佛教和印度教也傳播到了馬來西亞。伊斯蘭教在數千年後首次傳播到馬來西亞，主要是來自印度的古吉拉特族穆斯林和中國的回族穆斯林。在15世紀成為的主要宗教，特別是在西部沿海港口和貿易中心。。當葡萄牙人抵達馬來西亞時，他們所見到的帝國比自己的國家更加國際化。\[1\]

## 海外分佈

從馬來西亞移民聖誕島的馬來人社群佔了澳大利亞[聖誕島總人口的](../Page/聖誕島.md "wikilink")20％。\[2\]

## 人口統計

馬來人是馬來西亞的主要民族。除了[沙巴和](../Page/沙巴.md "wikilink")[砂拉越以外](../Page/砂拉越.md "wikilink")，馬來西亞其他州屬的[馬來族人數都在](../Page/馬來族.md "wikilink")40％到90％以上，以下數字為2010年人口普查和2015年人口普查的數字，這個數字也包括非馬來西亞公民。

<table>
<thead>
<tr class="header">
<th><p>州屬</p></th>
<th><p>人口</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2010[3]</p></td>
<td><p>2015*[4]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柔佛.md" title="wikilink">柔佛</a></p></td>
<td><p>1,759,537</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吉打.md" title="wikilink">吉打</a></p></td>
<td><p>1,460,746</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吉蘭丹.md" title="wikilink">吉蘭丹</a></p></td>
<td><p>1,426,373</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬六甲.md" title="wikilink">馬六甲</a></p></td>
<td><p>517,441</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/森美蘭.md" title="wikilink">森美蘭</a></p></td>
<td><p>572,006</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彭亨.md" title="wikilink">彭亨</a></p></td>
<td><p>1,052,774</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/霹靂.md" title="wikilink">霹靂</a></p></td>
<td><p>1,238,357</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/檳城.md" title="wikilink">檳城</a></p></td>
<td><p>636,146</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/玻璃市.md" title="wikilink">玻璃市</a></p></td>
<td><p>198,710</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沙巴.md" title="wikilink">沙巴</a></p></td>
<td><p>184,197</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/砂拉越.md" title="wikilink">砂拉越</a></p></td>
<td><p>568,113</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雪蘭莪.md" title="wikilink">雪蘭莪</a></p></td>
<td><p>2,814,597</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丁加奴.md" title="wikilink">丁加奴</a></p></td>
<td><p>985,011</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a></p></td>
<td><p>679,236</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/納閩.md" title="wikilink">納閩</a></p></td>
<td><p>30,001</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布城.md" title="wikilink">布城</a></p></td>
<td><p>68,475</p></td>
</tr>
<tr class="even">
<td><p><strong>總人口</strong></p></td>
<td><p><strong>14,191,720</strong></p></td>
</tr>
<tr class="odd">
<td><ul>
<li></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

## 背叛伊斯兰教者

然而，如果任何一个马来西亚马来人有意[退出伊斯兰教](../Page/叛教_\(伊斯蘭教\).md "wikilink")，必须经[伊斯兰法庭批准](../Page/伊斯兰法庭.md "wikilink")。一般上宗教局会在执法和司法上对其劝阻（视各州属的伊斯兰法律而定）。到目前为止，成功退出了伊斯兰教的马来西亚马来人的人数极少，联邦法院在[丽娜·乔依改教案中判决穆斯林改教必须取得伊斯兰法庭的许可](../Page/丽娜·乔依.md "wikilink")，非宗教法庭无权干预。

## 注释

\<div class="references-small\>

</div>

## 参考文献

## 外部链接

## 参见

  - [马来西亚回教](../Page/马来西亚回教.md "wikilink")
  - [馬來西亞土著](../Page/馬來西亞土著.md "wikilink")
  - [马来人至上](../Page/马来人至上.md "wikilink")
  - [马来人](../Page/马来人.md "wikilink")
  - [巫统](../Page/巫统.md "wikilink")
  - [馬來西亞民族](../Page/馬來西亞民族.md "wikilink")

{{-}}

[Category:各族群馬來西亞人](../Category/各族群馬來西亞人.md "wikilink")
[馬來西亞馬來人](../Category/馬來西亞馬來人.md "wikilink")
[Category:各地马來人](../Category/各地马來人.md "wikilink")

1.
2.
3.
4.