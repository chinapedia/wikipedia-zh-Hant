**马特·巴斯比**爵士，[CBE](../Page/CBE.md "wikilink")（，）全名**亞歷山大·馬修·巴斯比**（Alexander
Matthew
Busby）是著名足球运动员和传奇主教练，[英格兰足球名人堂成员](../Page/英格兰足球名人堂.md "wikilink")，执教[曼彻斯特联长达二十多年](../Page/曼彻斯特联.md "wikilink")。曼联主场[-{zh-cn:老特拉福德球场;zh-tw:老特拉福德球場;zh-hk:奧脫福球場;}-外](../Page/老特拉福德球场.md "wikilink")，矗立著他的塑像。

## 生平

马特·巴斯比出生于[苏格兰](../Page/苏格兰.md "wikilink")[格拉斯哥地区的兰纳克郡](../Page/格拉斯哥.md "wikilink")（Lanarkshire），父亲与叔叔都在[第一次世界大战中丧生](../Page/第一次世界大战.md "wikilink")。在英格兰[曼城效力伊始](../Page/曼城足球俱乐部.md "wikilink")，曾出任锋线球员，随后逐渐占据中后卫线，以视野和出色的传球能力而著称，并随队获得了[英格兰足总杯冠军](../Page/英格兰足总杯.md "wikilink")。1936年，[利物浦以](../Page/利物浦足球俱乐部.md "wikilink")8000英镑转会费收购巴斯比。1940年英国职业足球比赛由于[第二次世界大战而停止](../Page/第二次世界大战.md "wikilink")，巴斯比的球员生涯至此结束。

二战结束后，巴斯比寻求在利物浦执教，但是最终无法接受俱乐部管理层的干涉，转而执教曼联。巴斯比率队在1952年、1956年和1957年三夺英甲联赛冠军，曼联名声大振。其中获得1956、1957二连冠的阵容基本全部由俱乐部青年训练培养，平均年龄只有21至22岁，被爱称为“[巴斯比宝贝](../Page/巴斯比宝贝.md "wikilink")”。

1958年2月6日，在巴斯比率队参加欧洲杯比赛后途经[德国](../Page/德国.md "wikilink")[慕尼黑返國时](../Page/慕尼黑.md "wikilink")，球队包机在起飞时失速坠毁，造成惨重伤亡，一共八名“巴斯比宝贝”遇难。在這次[慕尼黑空难中](../Page/慕尼黑空难.md "wikilink")，巴斯比本人也身受重伤，之后九周留院治疗中曾多次病危，甚至有两次已经做完临终祷告。但巴斯比最终却奇迹般康复，并继续执教球队。稍後獲英女王授予[CBE勳銜](../Page/CBE.md "wikilink")。

重建后的曼联，迎来了球队史上最出名的一批球员，包括[丹尼斯·劳](../Page/丹尼斯·劳.md "wikilink")、[博比·查尔顿](../Page/博比·查尔顿.md "wikilink")、[乔治·贝斯特等](../Page/乔治·贝斯特.md "wikilink")。球队之后在巴斯比的率领下，夺得[英格兰足总杯和英甲联赛雙料冠军](../Page/英格兰足总杯.md "wikilink")，并在[1968年夺得](../Page/1968年欧洲冠军杯决赛.md "wikilink")[欧洲冠军杯](../Page/欧洲冠军杯.md "wikilink")，一偿巴斯比称霸欧洲的梦想。巴斯比也在同年被授予[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")，并于1969年从俱乐部主教练一职引退。1970-1971赛季曾短暂复出，代替[威尔夫·麦克吉内斯短暂执教](../Page/威尔夫·麦克吉内斯.md "wikilink")，但很快再次退休。

之后，巴斯比出任俱乐部董事，1980年出任俱乐部主席，至1993年退任，其間見證曼聯在[亞歷斯·費格遜帶領下復興](../Page/亞歷斯·費格遜.md "wikilink")，奪得首屆[英超联赛冠军](../Page/英超.md "wikilink")，亦是自巴斯比年代後首個[英格蘭頂級聯賽冠軍](../Page/英格蘭足球冠軍.md "wikilink")。1994年因癌症与世长辞，享年{{\#expr:(1994)-(1909)-((1)\<(5)or(1)=(5)and(20)\<(26))}}岁。

## 参见

  - [慕尼黑空难](../Page/慕尼黑空难.md "wikilink")
  - [曼联历史](../Page/曼联历史.md "wikilink")

[Category:蘇格蘭足球運動員](../Category/蘇格蘭足球運動員.md "wikilink")
[Category:曼城球員](../Category/曼城球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:苏格兰足球领队](../Category/苏格兰足球领队.md "wikilink")
[Category:曼聯領隊](../Category/曼聯領隊.md "wikilink")
[Category:蘇格蘭足球代表隊領隊](../Category/蘇格蘭足球代表隊領隊.md "wikilink")
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:英格蘭足球名人堂領隊](../Category/英格蘭足球名人堂領隊.md "wikilink")
[Category:蘇格蘭足球名人堂入選人](../Category/蘇格蘭足球名人堂入選人.md "wikilink")
[B](../Category/欧冠冠军主教练.md "wikilink")