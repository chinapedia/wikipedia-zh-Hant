**马修·艾布拉姆·格勒宁**（，），出生于[美国](../Page/美国.md "wikilink")[俄勒冈州](../Page/俄勒冈州.md "wikilink")[波特兰市](../Page/波特兰_\(俄勒冈州\).md "wikilink")，是一位[漫画家](../Page/漫画家.md "wikilink")、电视制片人及编剧。格勒宁以其创造的[动画](../Page/动画.md "wikilink")[情景喜剧](../Page/情景喜剧.md "wikilink")《[辛普森一家](../Page/辛普森一家.md "wikilink")》而闻名于世；另外，他还是动画片《[飞出个未来](../Page/飞出个未来.md "wikilink")》的创造者，以及每周连环漫画《[地狱生活](../Page/地狱生活.md "wikilink")
》的作者。1978年，格勒宁的第一部连环漫画《地狱生活》发表于[先锋派杂志](../Page/先锋派.md "wikilink")《[
湿](../Page/湿_\(杂志\).md "wikilink")》上。现今共有250余种周报仍在连载这部漫画。

1985年，《地狱生活》引起了[詹姆斯·L·布鲁克斯的注意](../Page/詹姆斯·L·布鲁克斯.md "wikilink")。布鲁克斯与格勒宁取得了联系，建议他为[福克斯的](../Page/福克斯广播公司.md "wikilink")《[特蕾西·厄尔曼秀](../Page/特蕾西·厄尔曼秀.md "wikilink")
》引入一部[动画作品](../Page/动画.md "wikilink")。起初，布鲁克斯希望格勒宁能将《地狱生活》的人物引入该节目中，但当格勒宁了解到自己会失去那些人物的所有权后，他决定创作一批新的角色，并因此诞生了[辛普森这一动画家庭](../Page/辛普森家族.md "wikilink")。格勒宁用自己父母与妹妹的名字给这一家人命名——而代表自己的“Bart”（巴特）则源于颠倒字母后的“Brat”（小鬼）。《[辛普森短剧](../Page/辛普森短剧.md "wikilink")》在后来被扩展为了一个独立系列《辛普森一家》，至今已播映了19季400余集。1997年，格勒宁与[戴维·X·科恩](../Page/戴维·X·科恩.md "wikilink")
一同发展了一部新的动画《[飞出个未来](../Page/飞出个未来.md "wikilink")》，这部关于公元3000年的动画系列始映于1999年。在制播4年后，福克斯取消了《飞出个未来》；不过，[喜剧中心](../Page/喜剧中心.md "wikilink")
频道已计划制作该系列的16集新动画，并打算在2008年播放。

格勒宁赢得过10次[黄金时段艾美奖](../Page/黄金时段艾美奖.md "wikilink")，其中9次为《辛普森一家》，1次为《飞出个未来》；另外他在2004年还曾被[英国喜剧奖](../Page/英国喜剧奖.md "wikilink")
授予“喜剧杰出贡献奖 ”。2002年，他以《地狱生活》被[全美漫画家协会](../Page/全美漫画家协会.md "wikilink")
授予鲁班奖。

## 外部链接

  -
  - [格勒宁出现在《辛普森一家》中的不完全列表](https://web.archive.org/web/20060427061953/http://www.snpp.com/guides/meta.html#groening)－[辛普森档案](../Page/辛普森档案.md "wikilink")

  - [故事（1969）－马特·格勒宁的父亲霍默记录的一段关于他的妹妹莉萨和玛吉的影像](http://www.archive.org/details/the_story)

[Category:黃金時段艾美獎獲獎者](../Category/黃金時段艾美獎獲獎者.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美國電視監製](../Category/美國電視監製.md "wikilink")
[Category:美国卡通画家](../Category/美国卡通画家.md "wikilink")
[Category:美国男配音演员](../Category/美国男配音演员.md "wikilink")
[Category:美國不可知論者](../Category/美國不可知論者.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:挪威裔美國人](../Category/挪威裔美國人.md "wikilink")
[Category:俄勒岡州人](../Category/俄勒岡州人.md "wikilink")
[Category:辛普森一家](../Category/辛普森一家.md "wikilink")
[Category:美國動畫師](../Category/美國動畫師.md "wikilink")
[Category:美國藝術家](../Category/美國藝術家.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")
[Category:美國電視編劇](../Category/美國電視編劇.md "wikilink")
[Category:乃出個未來](../Category/乃出個未來.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:加拿大裔美國人](../Category/加拿大裔美國人.md "wikilink")
[Category:安妮奖获得者](../Category/安妮奖获得者.md "wikilink")
[Category:美國男性音樂家](../Category/美國男性音樂家.md "wikilink")
[Category:皮博迪獎得主](../Category/皮博迪獎得主.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")