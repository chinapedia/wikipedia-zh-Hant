**-{zh-hans:马特峰;zh-mo:馬特峰; zh-hk:馬特峰;
zh-tw:馬特峰}-**（[德語](../Page/德語.md "wikilink")：****，[意大利語](../Page/意大利語.md "wikilink")：****，[法語](../Page/法語.md "wikilink")：****）也称**马特洪峰**、**切尔维诺峰**，是[阿爾卑斯山脈中最著名的山峰](../Page/阿爾卑斯山.md "wikilink")。马特峰的位置在[瑞士](../Page/瑞士.md "wikilink")、[意大利邊境](../Page/意大利.md "wikilink")，附近是瑞士[瓦萊州小鎮](../Page/瓦萊州.md "wikilink")[采尔马特和意大利](../Page/采尔马特.md "wikilink")[瓦莱达奥斯塔的小鎮](../Page/瓦萊達奧斯塔.md "wikilink")（Breuil-Cervinia）。马特峰的名稱是由德語“Matt”（意为山谷、草地）和“horn”（意为山峰呈錐狀像一隻角）\[1\]。

马特峰是一個有四個面的[錐體](../Page/錐體.md "wikilink")，分別面向東南西北。每一個面都非常陡峭，因此只有少量的[雪黏在表面](../Page/雪.md "wikilink")，間中發生的[雪崩把過多積雪推到峰下的](../Page/雪崩.md "wikilink")[冰川裡](../Page/冰川.md "wikilink")。登峰者慣常從東北角的Hörnli山脊上山。

## 攀登史

[Matterhorn_Riffelsee_2005-06-11.jpg](https://zh.wikipedia.org/wiki/File:Matterhorn_Riffelsee_2005-06-11.jpg "fig:Matterhorn_Riffelsee_2005-06-11.jpg")
马特峰是[阿爾卑斯山脈中最後一個被征服的主要山峰](../Page/阿爾卑斯山脈.md "wikilink")。原因不只在於攀登技術上的種種困難，也在於此峰陡峭的外形給予早年攀山者的心理恐懼。攀山家們大約於1858年左右開始嘗試征服马特峰，他們多數選擇從南邊登山，事實上南面路線比較困難，他們經常發現自己身處濕滑的岩石上而決定放棄前進。

1865年7月14日，[爱德华·温珀](../Page/爱德华·温珀.md "wikilink")（Edward
Whymper）、[查尔斯·赫德森](../Page/查尔斯·赫德森.md "wikilink")（Charles
Hudson）、弗朗西斯·道格拉斯（Francis Douglas）、道格拉斯·羅伯特·哈多（Douglas Robert
Hadow）、[米歇尔·克罗](../Page/米歇尔·克罗.md "wikilink")（Michel
Croz）和彼得·陶格瓦尔德（Peter
Taugwalder）父子從策馬特登山，成為首支成功登上马特峰的登山隊。不幸地是，下山的时候米歇尔·克罗、道格拉斯·哈多、查尔斯·赫德森和弗朗西斯·道格拉斯四人發生意外，墮進1,400米以下的冰川裡身亡。事後，除道格拉斯的屍首不能尋回外，其餘三人都葬在了[策馬特](../Page/策馬特.md "wikilink")。

三年後，让-安托万·卡雷尔（Jean-Antoine Carrel）帶領的團隊成功從南邊登頂。尤利乌斯·埃利奥特（Julius
Elliott）在1868年成為第二支從[策馬特登頂的登山队](../Page/策馬特.md "wikilink")，同年[约翰·廷德乐](../Page/约翰·廷德乐.md "wikilink")、J·J·马基纳斯（J.
J. Maquinaz）與J·P·马基纳斯（J. P.
Maquinaz）成功跨越山峰。[露西·沃克和](../Page/露西·沃克.md "wikilink")[梅塔·布雷武特在](../Page/梅塔·布雷武特.md "wikilink")1871年分別成為首名及次名攀登馬特洪頂峰的女性。

現在，登山者一年四季都可從马特峰的每個面或山脊登峰。在夏季由策馬特從Hörnli上山的人駱驛不絕。登上马特峰需要一定技術，但對老手來說不算困難（難度評級為AD，請參閱[攀登難度等級](../Page/攀登難度等級.md "wikilink")），在部分路段更設有固定繩索輔助登山者。雖然如此，每年總有數人因為缺乏經驗、落石或路線太擁擠等原因而意外身亡。登山者通常先由策馬特乘黑湖缆車（Schwarzsee
Cable
Car）上山，然後步行上海拔3,260米的Hörnli-hütte，在大石屋裡待一晚。登山者須於第二天早上4時出發登頂，以確保他們可以在下午的雲霧及風暴到來前平安下山。

除Hörnli外，登山者可選擇其他路線如意大利脊（Italian Ridge）（難度評級為D）、Zmutt
Ridge（難度評級為D）及難度最高的北面路線（難度評級為TD+）（到1931年才被施密德兄弟（Toni
Schmid与Franz Schmid）征服，因此該路線又稱為施密德路线。

## 突出高度

雖然马特峰外形突出、山勢雄偉，但如果以山峰突出高度計算，马特峰在[阿爾卑斯山中卻](../Page/阿爾卑斯山.md "wikilink")100名不入。原因是附近的山峰如[罗莎峰](../Page/罗莎峰.md "wikilink")、[多姆峰](../Page/多姆峰.md "wikilink")、[利斯卡姆峰及](../Page/利斯卡姆峰.md "wikilink")[魏斯峰的海拔高度都比马特峰高](../Page/魏斯峰.md "wikilink")。這張[全景相](https://web.archive.org/web/20061116183141/http://www.ii.uib.no/~petter/mountains/Sveits/42-51_annotate.jpg)拍下了阿爾卑斯山脈群峰（由[芬斯特拉峰向北面拍攝](../Page/芬斯特拉峰.md "wikilink")），马特峰位於相片的偏左方。

[Matterhorn.jpg](https://zh.wikipedia.org/wiki/File:Matterhorn.jpg "fig:Matterhorn.jpg")附近拍攝\]\]

## 文化影響

  - 美国加利福尼亚州[迪士尼樂園內的](../Page/迪士尼樂園.md "wikilink")「馬特峰雪橇」（Matterhorn
    Bobsleds）過山車，就是建於一座仿照马特峰的假山上，比例是1/100，高147呎。

<!-- end list -->

  - 瑞士[三角巧克力](../Page/三角巧克力.md "wikilink")（Toblerone）形狀的意念來自马特峰，包裝上亦以马特峰為商標\[2\]。

<!-- end list -->

  - [派拉蒙電影公司開場片段的山峰商標常被誤認為是马特峰或](../Page/派拉蒙.md "wikilink")[美國](../Page/美國.md "wikilink")[猶他州的Ben](../Page/猶他州.md "wikilink")
    Lomond。其實是美國的大提頓峰Grand Teton。

<center>

<File:Disneyland>
Matterhorn.jpg|加州[迪士尼樂園的](../Page/迪士尼樂園.md "wikilink")"馬特峰"

</center>

## 其他“馬特洪”

世界上有很多突出的錐形尖峰都被別稱作該地區的“马特峰”，例如：Cnicht（[威爾斯的马特峰](../Page/威爾斯.md "wikilink")）、Mount
Assiniboine（[洛磯山脈的马特峰](../Page/洛磯山脈.md "wikilink")）、Innerdalstårnet（[挪威的马特峰](../Page/挪威.md "wikilink")）、[槍岳](../Page/槍岳.md "wikilink")（[日本的马特峰](../Page/日本.md "wikilink")）、Machhapuchhare（[尼泊爾的马特峰](../Page/尼泊爾.md "wikilink")）、Shivling（[印度的马特峰](../Page/印度.md "wikilink")）、Ama
Dablam（[喜馬拉雅山的马特峰](../Page/喜馬拉雅山.md "wikilink")）、Mount
Aspiring（[新西兰](../Page/新西兰.md "wikilink")，南方的马特峰）、[加拿大內華達山脈的马特峰](../Page/加拿大.md "wikilink")（12,264呎）及[美國](../Page/美國.md "wikilink")[科羅拉多州聖胡安山脈的马特峰](../Page/科羅拉多州.md "wikilink")（13,590呎）。

另外，[火星探測器](../Page/火星.md "wikilink")「火星探路者號」附近一塊75厘米高的[岩石也被稱作](../Page/岩石.md "wikilink")「迷你馬特洪」。

[赫伯特·乔治·威尔斯在](../Page/赫伯特·乔治·威尔斯.md "wikilink")1904年出版的短編故事《The Country
of the
Blind》裡面一座虛構的山Parascotopetl，別稱「[安第斯的馬特峰](../Page/安第斯山脈.md "wikilink")」。

美國作家[-{zh-hans:卡尔·马兰蒂斯;
zh-hant:卡爾·馬藍提斯;}-出版於](../Page/卡爾·馬藍提斯.md "wikilink")2010年的暢銷[越戰小說](../Page/越戰.md "wikilink")《》中，一座位於[南北越交界區的虛構高山被駐屯戰區的](../Page/越南非軍事區.md "wikilink")[美國海軍陸戰隊命名為](../Page/美國海軍陸戰隊.md "wikilink")「馬特峰」。

## 參看

  - 從[法國](../Page/法國.md "wikilink")[夏慕尼至](../Page/夏慕尼.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[策馬特的](../Page/策馬特.md "wikilink")
  - [阿爾卑斯山脈](../Page/阿爾卑斯山脈.md "wikilink")
  - [阿爾卑斯山脈山峰列表](../Page/阿爾卑斯山脈山峰列表.md "wikilink")

## 參考資料

<div class="references-small">

<references />

  - , *Le Cervin* (, 1948)

  - , *Scrambles Amongst the Alps* (1871)

</div>

## 外部連結

  - [攀山者雜誌](https://web.archive.org/web/20071017122848/http://www.alpinist.com/doctcl/ALP16/profile-matterhorn)
    Mountain Profile - Issue 16
  - [马特峰網路攝影機](http://bergbahnen.zermatt.ch/d/web-cam/zermatt4.html)
  - [M
    Summitpost马特峰資料](http://www.summitpost.org/show/mountain_link.pl/mountain_id/53)
  - [PeakWare马特峰資料](https://web.archive.org/web/20030802173944/http://www.peakware.com/encyclopedia/peaks/matterhorn.htm)
  - [4000er.de马特峰資料](http://www.4000er.de/gipfel.php?vid=50&lang=en)
  - [華特迪士尼與策馬特](http://michaelbarrier.com/Essays/EuropeanJournal_Zermatt/europeanjournal_zermatt.htm)
  - [马特峰攀登史(德文)](http://www.weltderberge.de/alpen/matter.htm)

## 相片

<File:3818> - Riffelberg - Matterhorn viewed from
Gornergratbahn.JPG|馬特洪峰 <File:Cervin> 2 by grunk.jpg|東面及北面
[File:matterhorn.zermatt.arp.500pix.jpg|馬特洪在Riffelsee的倒影](File:matterhorn.zermatt.arp.500pix.jpg%7C馬特洪在Riffelsee的倒影)
[File:Matterhorn-EastAndNorthside-viewedFromZermatt.jpg|從策馬特看東及北面](File:Matterhorn-EastAndNorthside-viewedFromZermatt.jpg%7C從策馬特看東及北面)
[File:Matterhorn-SouthSide-viewedFromSkiRegionBreuilCervinia.jpg|從Skigebiet](File:Matterhorn-SouthSide-viewedFromSkiRegionBreuilCervinia.jpg%7C從Skigebiet)
Breuil-Cervinia看到山峰的南面
[File:Matterhorn-mostlyEastSide-viewedFromRothorn.jpg|由Rothorn看東及北面](File:Matterhorn-mostlyEastSide-viewedFromRothorn.jpg%7C由Rothorn看東及北面)
[File:Zermatt_and_Matterhorn.jpg|馬特洪峰與策馬特](File:Zermatt_and_Matterhorn.jpg%7C馬特洪峰與策馬特)
<File:Matterhorn> Riffelsee 2005-06-11.jpg|馬特洪在Riffelsee的倒影
<File:Matterhorn> aerial9.JPG|從高空俯瞰馬特洪(另一角度) (1/1)

[Category:瑞士山峰](../Category/瑞士山峰.md "wikilink")
[Category:意大利山峰](../Category/意大利山峰.md "wikilink")
[Category:阿爾卑斯山脈](../Category/阿爾卑斯山脈.md "wikilink")
[Category:歐洲地標](../Category/歐洲地標.md "wikilink")
[Category:義大利-瑞士邊界](../Category/義大利-瑞士邊界.md "wikilink")

1.  [Swiss Alps - mountains in Switzerland:
    Names](http://www.swissworld.org/eng/spotlight/Names.html?siteSect=904&sid=4124638&cKey=1158238588000&rubricId=17100)
    www.swissworld.org
2.  [Toblerone - Shape &
    Name](http://www.toblerone.com/our_secret/shape-en.html)
    www.toblerone.com.