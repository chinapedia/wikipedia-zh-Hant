\-{H|zh-cn:马瑙斯;zh-tw:瑪瑙斯}-
[Manaus-Amazon-NASA.jpg](https://zh.wikipedia.org/wiki/File:Manaus-Amazon-NASA.jpg "fig:Manaus-Amazon-NASA.jpg")
**马瑙斯市**（[葡萄牙语](../Page/葡萄牙语.md "wikilink")：）是[巴西](../Page/巴西.md "wikilink")[亚马逊州的首府](../Page/亚马遜_\(巴西州份\).md "wikilink")，位于[亚马逊河支流](../Page/亚马逊河.md "wikilink")[黑河](../Page/内格罗河_\(亚马逊河支流\).md "wikilink")（Negro，又译内格罗河）和[索里芒斯河](../Page/索里芒斯河.md "wikilink")（即亚马逊河干流）交汇处，面积337平方千米，在2014年時人口约为190万，是[亞馬遜州內人口最多的城市](../Page/亞馬遜州_\(巴西\).md "wikilink")。\[1\]
由於马瑙斯位於亞馬遜雨林中央，主要依靠船運與空運，這種隔離有助於保護該城的自然與文化，相較於巴西其他城市，瑪瑙斯保留許多巴西傳統土著文化。\[2\]
[玛瑙斯亦因其](../Page/玛瑙斯.md "wikilink")[自由贸易区而著名](../Page/自由贸易区.md "wikilink")。在自由贸易区投资可以享受政府的一些优惠政策。工业以冶金、化工、造船、电子和机械为主。马瑙斯市拥有世界上最大的[浮动码头](../Page/浮动码头.md "wikilink")，全长1313米。主要有柑橘、木瓜等水果农产品，該地也出口巴西堅果，橡膠，黃麻和花梨木油等產品。

马瑙斯是[2014年世界杯足球賽的主辦城市之一](../Page/2014年世界杯足球賽.md "wikilink")。這是在亞馬遜熱帶雨林中唯一主辦城市，也是本次主辦城市中位在最北與最西，並且最偏僻的城市。

## 历史

马瑙斯的欧洲殖民的历史始于1499年，當時西班牙人首次发现該地。[西班牙之後则继续向巴西北部殖民](../Page/西班牙.md "wikilink")。

随着人口增长，1695年，传教士（[加尔默罗](../Page/加尔默罗会.md "wikilink")、[耶稣会](../Page/耶稣会.md "wikilink")、[方济各会](../Page/方济各会.md "wikilink")）在此建立了教堂，在当时成为城市的守护神\[3\]。它是一个战略要地。在1832年11月13日，卢格大巴拉升格为镇而得名马瑙斯。1856年9月4日，总督费雷拉·佩纳终于给它取名为“马瑙斯”。

马瑙斯的橡胶热潮在19世纪后期。这一时期修建了一座歌剧院，现为[亚马逊歌剧院](../Page/亚马逊歌剧院.md "wikilink")。马瑙斯成为了经济繁荣的城市。

## 地理

瑪瑙斯是巴西北部最大的城市，是[巴西](../Page/巴西.md "wikilink")[亚马逊州的首府](../Page/亚马逊_\(巴西州份\).md "wikilink")，位在[亞馬遜森林中](../Page/亞馬遜森林.md "wikilink")，佔地11,401.06平方公里（4,402平方英里），人口密度為144.4人/平方公里。

## 氣候

瑪瑙斯在[柯本氣候分類法內被分為在](../Page/柯本氣候分類法.md "wikilink")[熱帶季風氣候區內](../Page/熱帶季風氣候.md "wikilink")，每年溫度改變的幅度不大。由於全城旱季在七到九月，且八月降雨量小於66毫米，使瑪瑙斯無法被分在[熱帶雨林氣候區內](../Page/熱帶雨林氣候.md "wikilink")。

## 人口

根據巴西國家地理及統計局在2012的結果顯示，在市中心約有1,861,906人居住，而整個瑪瑙斯大都會區總共擁有2,283,906人，這使瑪瑙斯成為巴西國內第七大城市。\[4\]城市的人口增長高於全國平均水準，並且以10％以上高過首都[巴西利亞](../Page/巴西利亞.md "wikilink")。大部分人口位於城市的北部與東部地區，當中以新城地區人口最多，有超過26萬的居民居住於此。

根據最後一次的人口普查結果，城市人口從1960年的343,038人，到1970年增至622,733人，1990年人口更是增長到1,025,979人，其密度增加為至90.0人/平方公里。

## 經濟

[Noturnamao_2014.jpg](https://zh.wikipedia.org/wiki/File:Noturnamao_2014.jpg "fig:Noturnamao_2014.jpg")

虽然马瑙斯20世纪的主要主业是[橡胶](../Page/橡胶.md "wikilink")，但它的重要性已经下降。它的[鱼](../Page/鱼.md "wikilink")，野生[水果](../Page/水果.md "wikilink")，如[巴西莓](../Page/巴西莓.md "wikilink")，[巴西坚果等弥补了重要的交易](../Page/巴西坚果.md "wikilink")，石油精炼，肥皂制造，化工等行业出现。在过去的几十年中，联邦投资税收激励制度已经把周边地区成为主要的工业中心（玛瑙斯自由经济区）。

马瑙斯绵延很大，但大多数的酒店和景点的位置都集中在市中心。作为[亚马逊河上最大的城市](../Page/亚马逊河.md "wikilink")，主要港口。马瑙斯商业发达。本地行业包括酿酒，造船，肥皂制造，所生产的驳船和旅游带来的化学品，电脑，摩托车和石油炼油。

全市[国内生产总值约为](../Page/国内生产总值.md "wikilink")319.16亿[雷亚尔](../Page/雷亚尔.md "wikilink")（2006年）\[5\]。

## 地区

### 大都市区

马瑙斯都市区（RMM），其中人口2283906人（2012年）是巴西的第8大城市，但没有卫星城。

### 地区

马瑙斯分为七个区域：北部地区，南部地区，中部地区，南部地区，东部地区，西部地区，中西部地区和农村地区。城市东部地区的人口最多，大约有60万居民（2007），最近几年人口增长率是最高的，并拥有城市中最大的社区，城市的北部地区是新城区。中南部地区的人均收入是最高的。

## 交通

### 机场

[缩略图](https://zh.wikipedia.org/wiki/File:Aeroporto_de_Manaus.JPG "fig:缩略图")
[马瑙斯-爱德华多·戈梅斯国际机场是服务于马瑙斯的国际机场](../Page/马瑙斯-爱德华多·戈梅斯国际机场.md "wikilink")，航運量位居巴西第三\[6\]
。该机场拥有两座航站楼，一为定期航班，另一个用于支线航空。它也有三个货运站点。

### 公路

有2条国道相交于马瑙斯。有一条柏油路向北（BR-174）连接马瑙斯和[罗赖马州首府](../Page/罗赖马州.md "wikilink")[博阿维斯塔](../Page/博阿维斯塔_\(罗赖马州\).md "wikilink")。严格地说，马瑙斯是有道路连接到巴西其他地方的，因为它可以沿公路驶入委内瑞拉，然后重新进入巴西的[阿克里州穿过](../Page/阿克里州.md "wikilink")[哥伦比亚](../Page/哥伦比亚.md "wikilink")，[厄瓜多尔和](../Page/厄瓜多尔.md "wikilink")[秘鲁等国](../Page/秘鲁.md "wikilink")。因为这样的路线行驶较困难，马瑙斯的运输方式绝大多数是乘船或飞机，除了前往罗赖马州。独立报指出“马瑙斯仍然没有公路”到全国其他地区。

### 港口

马瑙斯的港口码头在市中心的[内格罗河河畔](../Page/内格罗河_\(亚马逊河\).md "wikilink")，是[亚马逊雨林的心脏地带](../Page/亚马逊雨林.md "wikilink")。一些手机企业在马瑙斯的制造工厂，以及其他主要电子产品制造商都有工厂在那里。主要出口产品包括巴西坚果，化工，石油，电力设备，林产品和生态旅游，收入为城市的一个日益重要的来源。该地区最近发现的石油给这里带来了巨大潜力。\[7\]

## 旅游

马瑙斯是巴西旅游名城，在其周围可领略亚马逊流域的[热带雨林风情](../Page/热带雨林.md "wikilink")，距市区80公里就是野生热带雨林区。主要景点有海关大楼（砖瓦从英国进口）、[马瑙斯大剧院](../Page/马瑙斯大剧院.md "wikilink")、[印第安人博物馆等](../Page/印第安人.md "wikilink")、内格罗河（又译黑河）黑色的河水和亚马逊河白色河水交汇的奇观、原始森林的动植物景观等。

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/貝倫_(巴西).md" title="wikilink">貝倫</a></p></li>
<li><p><a href="../Page/夏洛特_(北卡羅來納州).md" title="wikilink">夏洛特</a></p></li>
<li><p><a href="../Page/戈亞尼亞.md" title="wikilink">戈亞尼亞</a></p></li>
<li><p><a href="../Page/濱松市.md" title="wikilink">濱松市</a></p></li>
<li><p><a href="../Page/耶路撒冷.md" title="wikilink">耶路撒冷</a></p></li>
<li><p><a href="../Page/馬辰.md" title="wikilink">馬辰</a></p></li>
<li><p><a href="../Page/梅薩_(亞利桑那州).md" title="wikilink">梅薩</a></p></li>
<li><p><a href="../Page/佩魯賈.md" title="wikilink">佩魯賈</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/里約熱內盧.md" title="wikilink">里約熱內盧</a></p></li>
<li><p><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></li>
<li><p><a href="../Page/聖多明哥.md" title="wikilink">聖多明哥</a></p></li>
<li><p><a href="../Page/上海市.md" title="wikilink">上海市</a></p></li>
<li><p><a href="../Page/伊基托斯.md" title="wikilink">伊基托斯</a></p></li>
<li><p><a href="../Page/薩尼亞市.md" title="wikilink">薩尼亞市</a></p></li>
<li><p><a href="../Page/首爾.md" title="wikilink">首爾</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

[Category:巴西城市](../Category/巴西城市.md "wikilink")
[Category:巴西亚马逊州城市](../Category/巴西亚马逊州城市.md "wikilink")
[Category:马瑙斯](../Category/马瑙斯.md "wikilink")

1.  [Manaus tem população estimada em 1,9 milhão de habitantes, diz IBGE
    ](http://g1.globo.com/am/amazonas/noticia/2013/08/manaus-e-setima-capital-mais-populosa-do-pais-diz-ibge.html)
2.  [Manaus Guide](http://www.manausguide.com/)
3.  [History of
    Manaus](http://creyete.com/camera/331-Manaus---Amazonas/)
4.
5.
6.  [Cargo movement in International Airport of
    Manaus](http://www.infraero.gov.br/aero_prev_home.php?ai=47)
7.  [Port of Manaus](http://www.triposo.com/poi/Port_of_Manaus)