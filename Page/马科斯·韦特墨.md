**马科斯·韦特墨**（**Max
Wertheimer**，）同[科特·考夫卡和](../Page/科特·考夫卡.md "wikilink")[沃尔夫冈·苛勒一起](../Page/沃尔夫冈·苛勒.md "wikilink")，是[格式塔心理学的创始人](../Page/格式塔心理学.md "wikilink")。

1880年4月15日，韦特墨出生在[奥匈帝国的](../Page/奥匈帝国.md "wikilink")[布拉格一个](../Page/布拉格.md "wikilink")[犹太人家庭](../Page/犹太人.md "wikilink")，1900年就读于[查理大学攻读](../Page/查理大学.md "wikilink")[法律](../Page/法律.md "wikilink")，2年后放弃法律，转学[哲学](../Page/哲学.md "wikilink")。以后又转入[柏林大学和](../Page/柏林大学.md "wikilink")[维尔茨堡大学](../Page/维尔茨堡大学.md "wikilink")。1904年，韦特墨在的指导下，获得[维尔茨堡大学博士学位](../Page/维尔茨堡大学.md "wikilink")（[拉丁文学位荣誉最优等](../Page/拉丁文学位荣誉.md "wikilink")）。

1910年夏天，韦特墨在从[维也纳前往](../Page/维也纳.md "wikilink")[莱茵兰度假的火车上](../Page/莱茵兰.md "wikilink")，突然在[直觉中对窗外的风景产生的](../Page/直觉.md "wikilink")[视错觉](../Page/视错觉.md "wikilink")——[似动现象产生了一种理论解释](../Page/似动现象.md "wikilink")，于是中途在[法兰克福下车](../Page/法兰克福.md "wikilink")，留在法兰克福大学进行心理学实验，研究在一定条件下，当两个静止的对象相继在不同的地方出现时，看起来就象在活动的现象，从而始创格式塔心理学。在那里他对[知觉发生兴趣](../Page/知觉.md "wikilink")。他与两位年轻的助教，[沃尔夫冈·苛勒和](../Page/沃尔夫冈·苛勒.md "wikilink")[科特·考夫卡一起进行研究工作](../Page/科特·考夫卡.md "wikilink")。他用[速示器](../Page/速示器.md "wikilink")（tachistoscope）研究活动图像的效果。1912年，他发表了标志格式塔心理学形成的代表性论文——《似动现象实验研究》，获得讲师职位。

从1916年到1925年，他在柏林（1922年成为助理教授）。1925年，他回到法兰克福，1929年被评为全职[教授](../Page/教授.md "wikilink")。

1933年，纳粹在德国上台，开始迫害犹太人，韦特墨匆忙逃离德国，前往[美国](../Page/美国.md "wikilink")，任教于[纽约市的](../Page/纽约市.md "wikilink")[社会研究新学校](../Page/新學院.md "wikilink")，1943年9月完成《创造性思维》。在《创造性思维》中，韦特墨表达他的支持自上而下加工或整体学习。

韦特墨被公认为是格式塔心理学的创立者之一，性格热情幽默，爱好作诗和作曲。他经常思如泉涌，又擅长演讲和热情的鼓动，但是著述极少。

在完成《创造性思维》三周后，韦特墨逝世于心脏病发。他被埋葬於纽约北郊的新罗彻尔市。

## 参考

  - Wertheimer,
    Michael.（2000）《心理学简史》第4版，[德克萨斯州沃斯堡](../Page/德克萨斯州.md "wikilink")：Harcourt
    Brace & Company.
  - 美国心理学会（2000），《心理学先驱肖像》，APA and Ehrlbaum.

## 外部链接

  - [国际格式塔心理学理论及其应用协会（GTA）网站](http://www.gestalttheory.net/)

[Category:奥地利心理學家](../Category/奥地利心理學家.md "wikilink")
[Category:德國心理學家](../Category/德國心理學家.md "wikilink")
[Category:格式塔心理學](../Category/格式塔心理學.md "wikilink")
[Category:法蘭克福大學教師](../Category/法蘭克福大學教師.md "wikilink")
[Category:柏林洪堡大學教師](../Category/柏林洪堡大學教師.md "wikilink")
[Category:維爾茨堡大學校友](../Category/維爾茨堡大學校友.md "wikilink")
[Category:布拉格查理大學校友](../Category/布拉格查理大學校友.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:奥地利猶太人](../Category/奥地利猶太人.md "wikilink")
[Category:布拉格人](../Category/布拉格人.md "wikilink")