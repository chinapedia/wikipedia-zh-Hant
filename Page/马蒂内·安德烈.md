**安德烈·马蒂内**（1908年—1999年），[法国](../Page/法国.md "wikilink")[语言学家](../Page/语言学.md "wikilink")，1908年出生于[萨瓦省](../Page/萨瓦省.md "wikilink")。法国主要的[布拉格学派](../Page/布拉格学派.md "wikilink")[结构主义语言学的代表](../Page/结构主义.md "wikilink")。曾在[巴黎教](../Page/巴黎.md "wikilink")[音位学](../Page/音位学.md "wikilink")，1946年—1955年在[美国教学](../Page/美国.md "wikilink")，1955年回到法国任[巴黎大学](../Page/巴黎大学.md "wikilink")[教授](../Page/教授.md "wikilink")，1957年到高等实验学校任职。著有《[语言变化的结构](../Page/语言变化的结构.md "wikilink")》*（Economie
des changements phonétiques)*

## 著作

  - La gémination consonantique d'origine expressive dans les langues
    germaniques, Copenhague, Munksgaard, 1937.

  - La phonologie du mot en danois, Paris, Klincksieck, 1937.

  - , Paris, Droz, 1945.

  - Économie des changements phonétiques, Berne, Francke, 1955.

  - La description phonologique avec application au parler
    francoprovençal d'Hauteville (Savoie), coll. « Publication romanes
    et françaises », Genève, Librairie Droz, 1956.

  - Éléments de linguistique générale, Paris, Armand Colin, 1960.

  - Langue et fonction, 1962.

  - La linguistique synchronique, Paris, Presses universitaires de
    France, 1965.

  - Le français sans fard, coll. « Sup », Paris, PUF, 1969.

  - André Martinet, Langue et Fonction, Paris : Denoël, 1969, ©1962.

  - Studies in Functional Syntax, München, Wilhelm Fink Verlag, 1975.

  - Évolution des langues et reconstruction, Paris, PUF, 1975.

  - Syntaxe générale, 1985.

  - Des steppes aux océans, Paris, Payot, 1986.

  - Fonction et dynamique des langues, Paris, Armand Colin, 1989.

  - Mémoires d'un linguiste, vivre les langues, Paris, Quai Voltaire,
    1993 (with G. Kassai and J. Martinet).

[Category:法国语言学家](../Category/法国语言学家.md "wikilink")