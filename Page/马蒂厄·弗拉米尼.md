**马蒂厄·弗拉米尼**（，），是一位擁有[意大利血統的](../Page/意大利.md "wikilink")[法國足球運動員](../Page/法國.md "wikilink")，司職[中場](../Page/中場.md "wikilink")，成名於[英超球會](../Page/英超.md "wikilink")[阿森纳](../Page/阿森纳足球俱乐部.md "wikilink")，目前效力[西甲球會](../Page/西班牙甲組足球聯賽.md "wikilink")[赫塔費](../Page/赫塔費足球俱樂部.md "wikilink")。弗拉米尼同时也是一家生物技术公司GF
Biochemicals的创始人。

## 生平

法明尼出生於法國南部大城[馬賽](../Page/馬賽.md "wikilink")，父親為意大利人，並有親屬在羅馬居住。\[1\]

### 馬賽

早在少年時代已加入當地大球會[馬賽](../Page/馬賽足球會.md "wikilink")，為學徒球員。他於2003年12月20日一場比賽裡首度登場，之後上陣了14場聯賽。該季他隨馬賽打入[歐洲足協盃決賽](../Page/歐洲足協盃.md "wikilink")，不過卻敗於[西甲爭標分子](../Page/西甲.md "wikilink")[華倫西亞](../Page/瓦倫西亞足球俱樂部.md "wikilink")。

### 阿仙奴

至2004年夏季始被英超球會[阿仙奴收購](../Page/阿仙奴.md "wikilink")。首場英超聯賽乃於2004年8月15日勝[愛華頓一場聯賽](../Page/愛華頓.md "wikilink")。由於阿仙奴人材濟濟，他一直未成為正選，但仍有上陣機會，只是有時要司職右閘或左閘。2007年夏季有傳他被[意大利](../Page/意大利.md "wikilink")[AC米蘭招攬](../Page/AC米蘭.md "wikilink")，惟一直未成事。雖然在阿仙奴只是後備，但他也入選過國家隊。2007年2月7日他在一場對[阿根廷的友誼賽中被徵召入伍](../Page/阿根廷國家足球隊.md "wikilink")，代替因傷退出的杜拿蘭。

2007-08年球季，是法明尼開始成熟，成為阿仙奴的主力球員。[基拔圖·施華被貶為後備](../Page/基拔圖·施華.md "wikilink")，法明尼比[基拔圖·施華更有活力是主要原因](../Page/基拔圖·施華.md "wikilink")。法明尼亦不時有精采遠射，代表作是2007-08年球季，主場以3-0輕取[紐卡素的賽事](../Page/紐卡素足球會.md "wikilink")，他在離開二十五碼射入的「世界波」，更贏得領隊雲加的點名讚賞。[雲加在該仗賽後表示](../Page/雲加.md "wikilink")：「法明尼的拼勁太出色了，而且走位的時機和最後處理球的一下都很出色，很冷靜。以前他可不是這樣，今晚便射入了一個精彩的入球。」可見法明尼甚得雲加歡心。

然而，法明尼與阿仙奴的合約將於該季後結束，法明尼不欲再為阿仙奴效力，阿仙奴的續約談判亦失敗。

### AC米蘭

同時，法明尼本人亦表示2008-09新球季時會為AC米蘭披甲，AC米蘭官方亦表示在5月初會為成為自由身的法明尼提供一份由2008-09球季起，為期4年的合同，並表示法明尼通過體檢後便完成轉會程序，法明尼由阿仙奴轉會至AC米蘭已成事實。2008年5月5日，雙方正式簽訂4年合約﹐年薪高達400萬歐元。\[2\]\[3\]

自從加盟AC米蘭以來，高額的年薪和不斷的傷患。從加盟之初的2008-09球季開始，費明尼的出場次數每況愈下。2011-12賽季，費明尼幾乎養傷整季，出場次數只有2次。

2012年夏天AC米蘭原打算放棄費明尼，但多名老將離開，米蘭最終還是和費明尼低薪續約1年\[4\]。2013年，即在球隊最後一年，法明尼重獲上陣機會，並協助球隊升上第三名\[5\]。

### 重返阿仙奴

2013年7月，法明尼與AC米蘭的合約將於該季後結束，法明尼的續約談判失敗。

當地時間8月29日，法明尼以自由身重返阿仙奴，簽定3年合約，周薪4萬鎊\[6\]。

2016年5月14日，[阿仙奴領隊](../Page/阿仙奴.md "wikilink")[雲加在記者會上宣布](../Page/雲加.md "wikilink")，法明尼與兩名隊友[路斯基和](../Page/路斯基.md "wikilink")[阿迪達將會在季尾約滿離隊](../Page/阿迪達.md "wikilink")。

### 水晶宮

2016年法明尼以自由身加入[英超球隊](../Page/英格蘭超級聯賽.md "wikilink")[水晶宫足球俱乐部](../Page/水晶宫足球俱乐部.md "wikilink")，賽季後球隊宣佈不會跟他續約\[7\]。

### 赫塔費

2018年2月2日，法明尼加入[赫塔費](../Page/赫塔費足球俱樂部.md "wikilink")\[8\]。

## 榮譽

### 球會

  -  阿仙奴

<!-- end list -->

  - [英格蘭足總盃冠軍](../Page/英格蘭足總盃.md "wikilink")﹕2005年、2014年 、2015年
  - [英格蘭社區盾冠軍](../Page/英格蘭社區盾.md "wikilink")﹕2004年、2014年
  - [酋長盃冠軍](../Page/酋長盃.md "wikilink")﹕2007年

<!-- end list -->

  -  AC米蘭

<!-- end list -->

  - [意甲冠軍](../Page/意甲.md "wikilink")﹕2011年
  - [意大利超級盃冠軍](../Page/意大利超級盃.md "wikilink")﹕2011年

## 參考資料

[Category:法國足球運動員](../Category/法國足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:馬賽球員](../Category/馬賽球員.md "wikilink")
[Category:阿仙奴球員](../Category/阿仙奴球員.md "wikilink")
[Category:AC米兰球员](../Category/AC米兰球员.md "wikilink")
[Category:水晶宮球員](../Category/水晶宮球員.md "wikilink")
[Category:赫塔費球員](../Category/赫塔費球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")

1.
2.  [soccernet.com 2008年5月5日](http://soccernet.espn.go.com/news/story?id=531988&cc=4716)
3.  [bbc
    news 2008年5月5日](http://news.bbc.co.uk/sport2/hi/football/teams/a/arsenal/7383314.stm)
4.  [費明尼冬季或轉會重返馬賽](http://www.goal.com/hk/news/489/%E6%84%8F%E5%A4%A7%E5%88%A9/2012/10/10/3438847/%E8%B2%BB%E6%98%8E%E5%B0%BC%E5%86%AC%E5%AD%A3%E6%88%96%E8%BD%89%E6%9C%83%E9%87%8D%E8%BF%94%E9%A6%AC%E8%B3%BD)
5.  <http://soccer.sina.com.hk/news/85/2/1/2309051/1.html>
6.
7.
8.