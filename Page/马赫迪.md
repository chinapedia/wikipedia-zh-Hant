**马赫迪**（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：****
**），或译**麦赫迪**、**迈赫迪**，意为“导师”，是[伊斯兰教教典中记载的将于](../Page/伊斯兰教.md "wikilink")[最后审判日之前](../Page/最后审判日.md "wikilink")7年、9年或者19年降临世间的[救世主](../Page/救世主.md "wikilink")。

马赫迪的概念在《[古兰经](../Page/古兰经.md "wikilink")》内没有清晰的体现，但是[穆罕默德在](../Page/穆罕默德.md "wikilink")《[圣训](../Page/圣训.md "wikilink")》中多次提及。根据伊斯兰传统，[尔撒](../Page/尔撒.md "wikilink")（[基督教中稱為](../Page/基督教.md "wikilink")[耶穌](../Page/耶穌.md "wikilink")）会再次降临人世，以穆斯林身份和马赫迪並肩作戰。

[巴哈伊信仰相信](../Page/巴哈伊信仰.md "wikilink")[巴孛即为马赫迪](../Page/巴孛.md "wikilink")，而19年后宣誓的[巴哈欧拉则为耶稣的回归](../Page/巴哈欧拉.md "wikilink")。

## 参考文献

## 外部链接

  - [Mahdi](http://www.britannica.com/eb/article-9050117/mahdi) an
    article by Encyclopedia Britannica Online
  - [A supportive view of the belief of the Mahdi at Sunni-oriented
    islam-qa.com](http://www.islam-qa.com/index.php?ref=1252&ln=eng&txt=mahdi)
  - [A critical look at the belief in the Mahdi at Qurannic-oriented
    understanding-islam.com](http://www.understanding-islam.com/related/text.asp?type=question&qid=40)

## 参见

  - [伊斯兰教末世论](../Page/伊斯兰教末世论.md "wikilink")、[敌基督](../Page/敌基督.md "wikilink")、[爾撒](../Page/爾撒.md "wikilink")
  - [哈里发](../Page/哈里发.md "wikilink")
  - [马赫迪 (消歧义)](../Page/马赫迪_\(消歧义\).md "wikilink")
  - [马赫迪耶](../Page/马赫迪耶.md "wikilink")

{{-}}

[Category:伊斯兰教末世论](../Category/伊斯兰教末世论.md "wikilink")
[Category:伊斯蘭教尊稱](../Category/伊斯蘭教尊稱.md "wikilink")
[Category:伊斯兰教宗教领袖](../Category/伊斯兰教宗教领袖.md "wikilink")
[Category:伊斯兰教君主](../Category/伊斯兰教君主.md "wikilink")
[Category:弥赛亚主义](../Category/弥赛亚主义.md "wikilink")
[Category:预言](../Category/预言.md "wikilink")
[Category:十二伊瑪目](../Category/十二伊瑪目.md "wikilink")