[Ma_LianLiang_1966.jpg](https://zh.wikipedia.org/wiki/File:Ma_LianLiang_1966.jpg "fig:Ma_LianLiang_1966.jpg")

**马连良**（），[回族](../Page/回族.md "wikilink")，[北京人](../Page/北京.md "wikilink")，字**温如**，[中国民主同盟成员](../Page/中国民主同盟.md "wikilink")\[1\]。中国著名[京剧艺术家](../Page/京剧.md "wikilink")，[老生演员](../Page/老生.md "wikilink")。[民国时期](../Page/中华民国.md "wikilink")[京剧三大家之一](../Page/京剧三大家.md "wikilink")。是[扶风社的招牌人物](../Page/扶风社.md "wikilink")。拿手戏目有《借东风》，《甘露寺》，《青风亭》等。

## 生平

父马西园，是一茶馆业主，与著名京剧演员[谭小培熟识](../Page/谭小培.md "wikilink")。家庭的熏陶，使马连良从小热爱京剧艺术，希望在艺术舞台上得到发展。

9岁入北京[喜连成科班](../Page/富连成.md "wikilink")，先从[茹莱卿学武生](../Page/茹莱卿.md "wikilink")，开蒙戏即《[石秀探庄](../Page/石秀.md "wikilink")》，后受业于[叶春善](../Page/叶春善.md "wikilink")、[蔡容贵](../Page/蔡容贵.md "wikilink")、[萧长华等老生名家](../Page/萧长华.md "wikilink")，并酷爱[谭派](../Page/譚鑫培.md "wikilink")、[贾派艺术](../Page/賈洪林.md "wikilink")。萧长华据[孙派](../Page/孫菊仙.md "wikilink")《雍良关》，创作《借东风》一剧，传授给他。马连良努力研究，终使《借东风》成为响誉全国的名剧。23岁自行组班[扶風社](../Page/扶風社.md "wikilink")，发展成为独树一帜的“马派”表演风格，自1920年代至1960年代盛行不衰。

1940年代末旅居[香港](../Page/香港.md "wikilink")，1950年代初回[中國](../Page/中國.md "wikilink")，出任北京京劇團（現在的[北京京劇院](../Page/北京京劇院.md "wikilink")）團長（總團長），1962年起同時兼任[北京市戏曲学校校长](../Page/北京市戏曲学校.md "wikilink")。

[中華人民共和國攝製的](../Page/中華人民共和國.md "wikilink")《群英會》、《秦香蓮》京劇電影保存紀錄了他的京劇表演藝術，[中央人民廣播電台和](../Page/中央人民廣播電台.md "wikilink")[中國唱片公司給他錄製了許多廣播京劇和京劇唱片](../Page/中國唱片公司.md "wikilink")。

馬連良因《海瑞罷官》事件被迫害致死。在[文化大革命中被抄家與關進](../Page/文化大革命.md "wikilink")[牛棚裡](../Page/牛棚_\(文革用语\).md "wikilink")，馬連良曾與[老舍](../Page/老舍.md "wikilink")、蕭軍、荀慧生、白芸生等一起被批鬥，當場剃成陰陽頭，墨汁淋在腦袋上，圍跪在大火四周，一面灼烤，一面用鋼頭皮帶抽頭，並拳打腳踢，個個頭破血流。老舍無法受此屈辱因而自殺。

至1966年10月初，12月13日因意外摔倒被送至[北京阜外醫院](../Page/北京阜外醫院.md "wikilink")，3天後不治。1979年3月27日北京市文化局恢复其名誉。

馬連良主演的京劇電影：《梅龍鎮》、《借東風》、《漁夫恨》、《群英會》、《秦香蓮》。

主演現代京劇（[京劇現代戲](../Page/京劇現代戲.md "wikilink")）：《[杜鵑山](../Page/杜鵑山.md "wikilink")》（1964年版）、《年年有餘》（1965年）等。

## 培養后進

門下弟子有[李萬春](../Page/李萬春.md "wikilink")、[朱耀良](../Page/朱耀良.md "wikilink")、[李慕良](../Page/李慕良.md "wikilink")、[言少朋](../Page/言少朋.md "wikilink")、[周嘯天](../Page/周嘯天.md "wikilink")、[王和霖](../Page/王和霖.md "wikilink")、[王金璐](../Page/王金璐.md "wikilink")、[遲金聲](../Page/遲金聲.md "wikilink")、[汪正華](../Page/汪正華.md "wikilink")、[梁益鳴](../Page/梁益鳴.md "wikilink")、[張學津](../Page/張學津.md "wikilink")、[馮志孝](../Page/馮志孝.md "wikilink")（馮志效）、[張克讓](../Page/張克讓.md "wikilink")、[安雲武](../Page/安雲武.md "wikilink")、[楊汝震](../Page/楊汝震.md "wikilink")、[趙世璞](../Page/趙世璞.md "wikilink")、[申鳳梅](../Page/申鳳梅.md "wikilink")、[郎石林](../Page/郎石林.md "wikilink")、[劉廣義等](../Page/劉廣義.md "wikilink")，[馬長禮是他的義子](../Page/馬長禮.md "wikilink")（養子），[梅葆玥是他的乾女兒](../Page/梅葆玥.md "wikilink")。

在北京戏校做校长期間培養了許許多多學生，[李寶春就是其中](../Page/李寶春.md "wikilink")1位。

## 家庭

马连良原配夫人王慧茹，生五子二女：崇仁、崇义、崇礼、崇智、崇延，萍秋、莉；继室陈慧琏，生二子二女：崇政、崇恩及静敏、小曼。[崇仁为京剧净角](../Page/马崇仁.md "wikilink")，[小曼为京剧旦角](../Page/马小曼.md "wikilink")。

## 參考書籍

  - 《一陣風，留下了千古絕唱─馬連良往事》[章詒和](../Page/章詒和.md "wikilink")
    著，收錄在同名作品，牛津大學出版社2005年1月初版，ISBN
    0-19-597498-0；台灣時報出版2005年1月初版，ISBN 9571342661
  - 《[伶人往事](../Page/伶人往事.md "wikilink")：写给不看戏的人看》，湖南文藝出版社2006年10月版，ISBN
    7540438177

## 參考資料

[M马](../Page/category:北京人.md "wikilink")
[M马](../Page/category:京剧演员.md "wikilink")

[M马](../Category/生行演员.md "wikilink") [M马](../Category/回族人.md "wikilink")
[L连](../Category/馬姓.md "wikilink") [M马](../Category/文革受难者.md "wikilink")
[M马](../Category/中国民主同盟盟员.md "wikilink")

1.  [相声演员于谦已低调加入中国国民党革命委员会](http://www.thepaper.cn/newsDetail_forward_1466267)