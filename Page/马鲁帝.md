**马鲁帝**（），是[马来西亚](../Page/马来西亚.md "wikilink")[砂拉越州](../Page/砂拉越州.md "wikilink")[美里省](../Page/美里省.md "wikilink")[马鲁帝县的首府](../Page/马鲁帝县.md "wikilink")，亦是[美里省第二大城](../Page/美里省.md "wikilink")。马鲁帝位于巴南河的北岸，距离[美里约](../Page/美里.md "wikilink")80公里\[1\]。市镇的主要景点是霍斯堡（Fort
Hose），同时也可通往[姆鲁山国家公园](../Page/姆鲁山国家公园.md "wikilink")。市镇早期是北砂的行政中心，直到后来被美里取代。
[Marudi_Museum.jpg](https://zh.wikipedia.org/wiki/File:Marudi_Museum.jpg "fig:Marudi_Museum.jpg")

## 参考

[Category:砂拉越](../Category/砂拉越.md "wikilink")
[Category:馬來西亞市镇](../Category/馬來西亞市镇.md "wikilink")

1.