**马鲛属**（[学名](../Page/学名.md "wikilink")：）通称**马鲛鱼**、**马交鱼**、**马加鱼**、**鰆\[1\]
魚**，俗名**鰆魚**，**𩵚魠魚**、**土托（魠、鮀）魚**\[2\]、**鲅鱼**、**竹鲛**、**馬加魚**、**梭齒魚**；属[鲭形目](../Page/鲭形目.md "wikilink")（或传统[鱸形目](../Page/鱸形目.md "wikilink")）[鯖科海鱼](../Page/鯖科.md "wikilink")。

## 特征

马鲛鱼身体延长，一般长为25至50厘米，体重300至1000克，最大可达一米左右，体重达到4.5千克以上。身体银灰色，具有暗色横纹，肋下有7-8个灰色斑点；吻尖突，口大斜裂，牙齿偏侧，似刀刃般锋利，上下颌各一行；鱼鳞细小或退化；背鳍2个，第2背鳍及臀鳍后部各具有7－9个小鳍。
[Serra_Spanish_mackerel.jpg](https://zh.wikipedia.org/wiki/File:Serra_Spanish_mackerel.jpg "fig:Serra_Spanish_mackerel.jpg")*\]\]
[Sccom_u0.gif](https://zh.wikipedia.org/wiki/File:Sccom_u0.gif "fig:Sccom_u0.gif")*\]\]
[Scomberomorus_regalis.jpg](https://zh.wikipedia.org/wiki/File:Scomberomorus_regalis.jpg "fig:Scomberomorus_regalis.jpg")''\]\]

## 习性

幼鱼以[甲壳类](../Page/甲壳类.md "wikilink")、鱼类等为食，长大以后为食鱼性动物，主要以[鳀鱼等为食](../Page/鳀鱼.md "wikilink")。

## 分布

分布于[北太平洋西部](../Page/北太平洋.md "wikilink")、产于东海、黄海、渤海和南海。在产区，以大洲岛海域出产的马鲛鱼为佳。

### 臺灣分布

可能指數種魚類：

1.  [台灣馬加鰆](../Page/台灣馬加鰆.md "wikilink")，闽南語為白腹仔。
2.  [高麗馬加鰆](../Page/高麗馬加鰆.md "wikilink")，闽南語為闊腹。
3.  [日本馬加鰆](../Page/日本馬加鰆.md "wikilink")，闽南語為正馬加或正馬鮫。
4.  [康氏馬加鰆](../Page/康氏馬加鰆.md "wikilink")，闽南語為土托或塗托。
5.  [中華馬加鰆](../Page/中華馬加鰆.md "wikilink")，闽南語為疏齒或大耳。
6.  [棘鰆](../Page/棘鰆.md "wikilink")，闽南語為竹節或石橋。
7.  [東方齒鰆](../Page/東方齒鰆.md "wikilink")，闽南語為煙虎

<!-- end list -->

  - 台灣[台中與台南為捕](../Page/台中.md "wikilink")魠魚基地之一。而冬天時澎湖地區海域所產土魠魚亦富盛名。

## 食用

[臺灣人重視此魚類](../Page/臺灣人.md "wikilink")，列為十大美味魚類之一，稱一[鯃](../Page/午魚.md "wikilink")、二[紅魦](../Page/布氏鯧鰺.md "wikilink")、三[鯧](../Page/銀鯧.md "wikilink")、四**馬鮫**、五[鮸](../Page/鮸.md "wikilink")、六[嘉鱲](../Page/嘉鱲魚.md "wikilink")、七[赤鯮](../Page/赤鯮.md "wikilink")、八[馬頭](../Page/日本馬頭魚.md "wikilink")、九[烏喉](../Page/烏喉魚.md "wikilink")、十[寸子](../Page/黃姑魚.md "wikilink")\[3\]。

[潮州人稱](../Page/潮州人.md "wikilink")：「好魚[馬鮫](../Page/馬鮫.md "wikilink")、[鯧](../Page/鯧.md "wikilink")，好菜[芥藍薳](../Page/芥藍.md "wikilink")，好戲《蘇六娘》（[潮剧](../Page/潮剧.md "wikilink")）」\[4\]

  - **[烤马鲛鱼](../Page/烤马鲛鱼.md "wikilink")**：为中國古时宫廷料理，被美食家誉为“仙界美食”，自古以来就备受宠爱。
  - **鲅鱼肉馅[饺子](../Page/饺子.md "wikilink")**：中国渤海湾沿岸地区常见的食品。
  - **土魠魚羹**：台灣普遍的小吃，用土魠魚肉塊裹粉油炸再放進羹湯中，有時會搭配[麵與](../Page/麵.md "wikilink")[米粉或白飯](../Page/米粉.md "wikilink")。

## 物种

<center>

<File:Scbra> u0.gif|[巴西馬鮫](../Page/巴西馬鮫.md "wikilink")
*S. brasiliensis* <File:Sccav>
u0.gif|[大耳馬鮫](../Page/大耳馬鮫.md "wikilink")
*S. cavalla* <File:Sccom>
u0.gif|[康氏馬鮫](../Page/康氏馬鮫.md "wikilink")/康氏馬加鰆
*S. commerson* <File:Scgut>
u0.gif|[斑點馬鮫](../Page/斑點馬鮫.md "wikilink")/台灣馬加鰆
*S. guttatus* <File:Sckor>
u0.gif|[朝鮮馬鮫](../Page/朝鮮馬鮫.md "wikilink")/高麗馬加鰆
*S. koreanus* <File:Sclin> u0.gif|[線紋馬鮫](../Page/線紋馬鮫.md "wikilink")
*S. lineolatus* <File:Scmac> u0.gif|[橢斑馬鮫](../Page/橢斑馬鮫.md "wikilink")
*S. maculatus* <File:Scmun> u0.gif|[澳洲馬鮫](../Page/澳洲馬鮫.md "wikilink")
*S. munroi* <File:Scnip>
u0.gif|[藍點馬鮫](../Page/藍點馬鮫.md "wikilink")/日本馬加鰆
*S. niphonius* <File:Scque>
u0.gif|[昆士蘭馬鮫](../Page/昆士蘭馬鮫.md "wikilink")
*S. queenslandicus* <File:Screg>
u0.gif|[條斑馬鮫](../Page/條斑馬鮫.md "wikilink")
*S. regalis* <File:Scsie>
u0.gif|[東太平洋馬鮫](../Page/東太平洋馬鮫.md "wikilink")
*S. sierra* <File:Scsin>
u0.gif|[中華馬鮫](../Page/中華馬鮫.md "wikilink")/中華馬加鰆
*S. sinensis* <File:Sctri> u0.gif|[西非馬鮫](../Page/西非馬鮫.md "wikilink")
*S. tritor*

</center>

  -   - [美洲馬鮫](../Page/美洲馬鮫.md "wikilink")(*Scomberomorus concolor*)　
      - [巴布亞馬鮫](../Page/巴布亞馬鮫.md "wikilink")(*Scomberomorus
        multiradiatus*)　
      - [南非馬鮫](../Page/南非馬鮫.md "wikilink")(*Scomberomorus
        plurilineatus*)
      - [半線馬鮫](../Page/半線馬鮫.md "wikilink")(*Scomberomorus
        semifasciatus*)　

## 註釋

[Category:食用魚](../Category/食用魚.md "wikilink")
[Category:马鲛属](../Category/马鲛属.md "wikilink")

1.  「鰆」：音同「春」。
2.  [教育部重編國語辭典修訂本](http://dict.revised.moe.edu.tw/cgi-bin/newDict/dict.sh?cond=%A4g%A6%AB&syn=1&pieceLen=100&fld=1&cat=&ukey=-903626608&serial=1&recNo=0&op=f&imgFont=1)
3.  [在薩爾瓦多城遇見馬頭魚](https://opinion.cw.com.tw/blog/profile/194/article/7762)
4.  [潮汕人认为最好吃的蔬菜
    连苏东坡都写诗称赞](http://k.sina.com.cn/article_1012411383_3c582bf700100e1t3.html)