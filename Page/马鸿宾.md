[Ma_Hongbin.jpg](https://zh.wikipedia.org/wiki/File:Ma_Hongbin.jpg "fig:Ma_Hongbin.jpg")

**馬鴻賓**（），[字子寅](../Page/表字.md "wikilink")，[回族](../Page/回族.md "wikilink")，[中国](../Page/中国.md "wikilink")[甘肃](../Page/甘肃.md "wikilink")[河州人](../Page/河州.md "wikilink")，[马家军主要将领之一](../Page/马家军.md "wikilink")，[国民革命军陆军上将](../Page/国民革命军.md "wikilink")。馬鴻賓與[马步芳](../Page/马步芳.md "wikilink")、[马鸿逵合稱西北三馬](../Page/马鸿逵.md "wikilink")。\[1\]

## 生平

1884年9月14日出生于[临夏](../Page/临夏.md "wikilink")。1904年任叔父[马福祥侍从](../Page/马福祥.md "wikilink")，1908年升任[西宁矿务马队队官](../Page/西宁.md "wikilink")，1910年到[宁夏](../Page/宁夏.md "wikilink")，任骑兵营营长。1912年任宁夏新军管带，后任[甘肃新军司令](../Page/甘肃.md "wikilink")（辖骑兵5个营）。1916年2月被[北洋政府授予陆军少将衔](../Page/北洋政府.md "wikilink")。1920年4月1日晋升为陆军中将衔。1921年任宁夏镇守使兼新军司令。1925年任[冯玉祥隶下的宁夏镇守使](../Page/冯玉祥.md "wikilink")。1926年9月任国民军第22师师长。1927年4月任冯玉祥的国民军第2集团军第22师师长。
1927年9月任国民革命军第2集团军第四方面军第24军军长。1928年3月任国民政府军事委员会委员。\[2\]

1930年，被冯玉祥任命为宁夏省主席，派兵追剿趁[中原大戰兵變的](../Page/中原大戰.md "wikilink")[馬廷賢](../Page/馬廷賢.md "wikilink")。\[3\]\[4\]

1931年，被[蒋介石任命为国军暂编第](../Page/蒋介石.md "wikilink")7师师长、甘肃省政府代理主席，8月正式任命为甘肃省政府主席，發生[雷馬事變](../Page/雷馬事變.md "wikilink")。1933年，任[庐山军官训练团副团长](../Page/庐山军官训练团.md "wikilink")，同年12月任第35师师长。1935年9月，任西北剿匪总部第4纵队司令兼第35师师长。1936年9月14日，被授予[国民革命军陆军中将衔](../Page/国民革命军.md "wikilink")，同年马家军歼灭[中国工农红军](../Page/中国工农红军.md "wikilink")[西路军](../Page/西路军.md "wikilink")。1937年，任国民革命军第17集团军副总司令兼第81军军长。1939年4月，兼任第17集团军副总司令。1940年，马鸿宾率81军在傅作義指揮下參與[綏西戰役與](../Page/綏西戰役.md "wikilink")[五原戰役](../Page/五原戰役.md "wikilink")，最後取得勝利，將日軍逐出綏遠西部。1941年，被蒋介石任命为绥西防守司令。1943年，[傅作义部完全接替了绥西防务](../Page/傅作义.md "wikilink")，遂率部撤回宁夏[中宁县进行休整](../Page/中宁.md "wikilink")。1945年6月，当选[中国国民党中央监察委员](../Page/中国国民党.md "wikilink")。1946年3月，任国民革命军西北行营副主任。1947年7月，任[陕甘宁边区剿匪总司令部司令](../Page/陕甘宁边区.md "wikilink")。1948年任[西北军政长官公署副司令长官](../Page/西北军政长官公署.md "wikilink")。\[5\]
[Chiang_Kaishek_with_Ma_Hongkui_and_Ma_Hongbin.jpg](https://zh.wikipedia.org/wiki/File:Chiang_Kaishek_with_Ma_Hongkui_and_Ma_Hongbin.jpg "fig:Chiang_Kaishek_with_Ma_Hongkui_and_Ma_Hongbin.jpg")（中）在[宁夏会见回族将领马鸿宾](../Page/宁夏.md "wikilink")（左二）和[马鸿逵](../Page/马鸿逵.md "wikilink")（右二）\]\]
1949年9月19日，马鸿宾与其子[马惇靖](../Page/马惇靖.md "wikilink")、[马惇信率部于](../Page/马惇信.md "wikilink")[宁夏](../Page/宁夏.md "wikilink")[中卫宣布](../Page/中卫.md "wikilink")“和平[起义](../Page/起义.md "wikilink")”，加入[中国人民解放军](../Page/中国人民解放军.md "wikilink")，所部被改编为[中国人民解放军](../Page/中国人民解放军.md "wikilink")[西北野战军独立第](../Page/西北野战军.md "wikilink")2军。后来，马鸿宾历任[宁夏省人民政府副主席](../Page/宁夏省人民政府.md "wikilink")、[甘肃省人民委员会副省长](../Page/甘肃省人民委员会.md "wikilink")、[西北军政委员会副主席](../Page/西北军政委员会.md "wikilink")、[中华人民共和国国防委员会委员](../Page/中华人民共和国国防委员会.md "wikilink")、[中华人民共和国民族事务委员会委员等职](../Page/中华人民共和国民族事务委员会.md "wikilink")。1960年，马鸿宾在[兰州病逝](../Page/兰州.md "wikilink")。\[6\]

## 家庭

  - 父為[馬福祿](../Page/馬福祿.md "wikilink")，叔父為[马福祥](../Page/马福祥.md "wikilink")。

## 参考文献

[Category:馬千齡家族](../Category/馬千齡家族.md "wikilink")
[Category:西北军将领](../Category/西北军将领.md "wikilink")
[M马](../Category/北洋将军府冠字将军.md "wikilink")
[Category:宁夏护军使](../Category/宁夏护军使.md "wikilink")
[M馬](../Category/临夏县人.md "wikilink")
[M馬](../Category/回族人.md "wikilink")
[H鸿](../Category/馬姓.md "wikilink")

1.  刘国铭主编，中国国民党九千将领，北京：中华工商联合出版社, 1993年

2.
3.  [清末民国两马家](http://www.gs.xinhuanet.com/dfpd/2007-11/21/content_11731055.htm)


4.
5.
6.