**驴**（[学名](../Page/学名.md "wikilink")：）是常见的[马科](../Page/马科.md "wikilink")[马属家畜](../Page/马属.md "wikilink")，是[非洲野驴被人类驯化所形成的](../Page/非洲野驴.md "wikilink")[亚种](../Page/亚种.md "wikilink")，和[马体形相似](../Page/马.md "wikilink")，但耳朵长，尾巴有尾柄，类似牛尾巴。

## 特征

驢是[奇蹄目的成員](../Page/奇蹄目.md "wikilink")。其他成員包括[斑馬和](../Page/斑馬.md "wikilink")[馬](../Page/馬.md "wikilink")，它們都有相同形態的腳[蹄](../Page/蹄.md "wikilink")。Holacomo
estas kakaroto, biba gocu

驴体色一般为灰色，也有白色和黑色品种，但都有一个白色眼圈。由于不同品种，体形大小相当不同，小的类似大狗，大的和马一样高。中国的“[关中驴](../Page/关中驴.md "wikilink")”十分高大，有1.7米。

驢叫聲響亮且高尖刺耳。

## 历史

驴在人类[历史记载和](../Page/历史.md "wikilink")[神话中经常出现](../Page/神话.md "wikilink")，与骑马的八面威风不同，骑驴是超凡脱俗的风雅之举。

  - 聖經記載[耶稣就是骑驴进入](../Page/耶稣.md "wikilink")[耶路撒冷](../Page/耶路撒冷.md "wikilink")。

<!-- end list -->

  - 中亚民间传说中的智者[阿凡提骑驴](../Page/阿凡提.md "wikilink")。

<!-- end list -->

  - [塞万提斯的](../Page/塞万提斯.md "wikilink")《[堂吉诃德](../Page/堂吉诃德.md "wikilink")》中，唐吉诃德骑马，其侍从桑丘就骑驴。

<!-- end list -->

  - [中国古代成语](../Page/中国.md "wikilink")“黔驴技穷”讲的是身高马大的[关中驴剛开始能吓坏没有见过驴的](../Page/关中.md "wikilink")[老虎](../Page/老虎.md "wikilink")。

<!-- end list -->

  - [八仙中的](../Page/八仙.md "wikilink")[张果老骑驴](../Page/張果老.md "wikilink")。

<!-- end list -->

  - [三国演义中](../Page/三国演义.md "wikilink")[黄承彦](../Page/黄承彦.md "wikilink")“骑驴过小桥，独叹梅花瘦”。

<!-- end list -->

  - [水浒传中](../Page/水浒传.md "wikilink")[陈抟骑驴](../Page/陈抟.md "wikilink")。

## 繁殖

公驴可以和母马交配，生下[骡](../Page/骡.md "wikilink")（“马骡”），骡个大，具有驴的负重能力和抵抗能力，有马的灵活性和奔跑能力，是非常好的役畜，但基本不能生育，极少数可以生育。如果是公马和母驴交配，生下的叫“驴骡”，又叫“駃騠”，驴骡个小，一般不如马骡好，但有时能生育。

## 用途

### 畜力

驴比马的适应性强，可以忍受粗食、重负，比马的价格低，因此一直是人类的重要役使动物。有的小驴可以驮负相当高大的人。
一般驢的負重可以達到一百公斤，連續走動五至六小時的山路沒有問題。驴对危险相当警觉，因此在某些对自己有危险的情况下不听人的驱使，显得相当执拗，所谓“驴脾气”。但一般情况下脾气温顺，可以服从孩子的调遣，因此现在不再使用役用动物的发达国家，有许多人把小型驴作为[宠物](../Page/宠物.md "wikilink")，可以供孩子骑乘。

### 食用

驢肉可食用，清朝有“生挫驢肉”的吃法\[1\]。中國[河北](../Page/河北.md "wikilink")[保定有著名小吃](../Page/保定.md "wikilink")“[驢肉火燒](../Page/驴肉火烧.md "wikilink")”，是在烙熟的麵餅中加入熟驢肉而成。驢腸也是一道名菜，北宋[韓縝愛吃驢腸](../Page/韓縝.md "wikilink")，每次宴客必用驢腸做菜，而且要現殺現煮，有客人起身如廁，路過廚房，現場看見活驢被剖肚抽腸的慘狀，心有不忍，終生不再吃驢肉。\[2\]明朝隆慶皇帝[朱載垕在裕王府時期好食驢腸](../Page/朱載垕.md "wikilink")，即位後，頗思節儉，聽說[光禄寺每天要殺一只驢](../Page/光禄寺.md "wikilink")，就不再吃了。\[3\]

## 注释

## 参考文献

## 外部链接

  - [英国的驴庇护所](http://www.thedonkeysanctuary.org.uk/)
  - [Bourricot and Co, Le monde des ânes sur le web : actualité, infos,
    sélection de sites et annuaire](http://bourricotandco.free.fr)

## 参见

  - [役用動物](../Page/役用動物.md "wikilink")

[Category:驢子](../Category/驢子.md "wikilink")
[Category:马属](../Category/马属.md "wikilink")

1.  [錢泳的](../Page/錢泳.md "wikilink")《履園叢話》：“以草驢一頭，養得極肥，先醉以酒，滿身排打。欲割其肉，先釘四樁，將足捆住﹔而以木一根橫于背，系其頭尾，
    使不得動。初以百滾湯沃其身，將毛刮盡，再以快刀零割。 要食前后腿，或肚當，或背脊，或頭尾肉，各隨客便﹔當客下箸時，其驢尚未死絕也。”
2.  [洪邁](../Page/洪邁.md "wikilink")《[夷堅志](../Page/夷堅志.md "wikilink")》卷十六
3.  《明實錄》記載：“潛邸時，嘗食驢腸而甘。及即位，間以問左右，左右請詔光祿。上：‘若爾，則光祿必日殺一驢，以備宣索，吾不忍也。’乃止。”