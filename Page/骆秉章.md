**駱秉章**（），原名**俊**，37歲時改名秉章，字**籲門**，號**儒齋**，[廣東](../Page/廣東.md "wikilink")[花縣人](../Page/花縣.md "wikilink")，[清后期封疆大吏](../Page/清.md "wikilink")。

## 生平

駱秉章父珝元，号诚斋。[乾隆五十八年](../Page/乾隆.md "wikilink")（1793年）三月十八日，駱秉章出生於廣東省花縣炭步鎮華嶺村。嘉庆二十四年己卯科举人，\[1\][道光十二年](../Page/道光.md "wikilink")（1832年）中進士，其[座师为](../Page/座师.md "wikilink")[穆彰阿](../Page/穆彰阿.md "wikilink")，選翰林院[庶吉士](../Page/庶吉士.md "wikilink")，散館授編修。道光二十年（1840年），受命稽察吏部银库，道光二十八年（1848年）任湖北按察使、道光三十年（1850年）由貴州布政使升任湖南巡撫。

[咸豐二年](../Page/咸豐.md "wikilink")（1852年）夏，[太平軍入湖南](../Page/太平軍.md "wikilink")，骆秉章率清兵拼死抵抗，并令用炮轰击，其事未平，被革職留任。後以守長沙有功（太平軍圍攻八十餘日不能克）而复职。咸豐三年（1853年）支持[曾國藩辦](../Page/曾國藩.md "wikilink")[團練](../Page/團練.md "wikilink")，又聘[左宗棠為幕僚](../Page/左宗棠.md "wikilink")，事無巨細，皆聽之\[2\]，左宗棠说：“所计画无不立从。一切公文，画诺而已，绝不检校。”一時傳為佳話。

咸丰五年（1855），太平军攻克武昌，湖北巡抚[胡林翼向骆秉章求救](../Page/胡林翼.md "wikilink")。骆秉章派[鲍超赴湖北](../Page/鲍超.md "wikilink")，以[彭玉麟继其后](../Page/彭玉麟.md "wikilink")。咸丰六年（1856），[曾国藩告戒其弟](../Page/曾国藩.md "wikilink")[曾国荃要](../Page/曾国荃.md "wikilink")“一听骆中丞、左季兄之命，敕东则东，敕西则西！”[潘祖荫曾言](../Page/潘祖荫.md "wikilink")：“楚军之得力，由于骆秉章之调度有方，实由于左宗棠之运筹决胜”\[3\]。咸豐十一年（1861年）任[四川總督](../Page/四川總督.md "wikilink")，鎮壓云南[藍朝鼎领导的起义势力](../Page/藍朝鼎.md "wikilink")，解绵州之围，杀四万余人。赏加太子少保衔。

[同治元年](../Page/同治.md "wikilink")（1862年），骆秉章派重兵拢子守[大渡河](../Page/大渡河.md "wikilink")，斷[石达开後路](../Page/石达开.md "wikilink")，石达开被围于安顺场，不忍全軍餓死，寫信给骆秉章，希望“宥我将士，请免诛戮”。駱佯稱答應，後將石达开俘虜，解至成都[凌遲处死](../Page/凌遲.md "wikilink")。同治四年（1865年），有眼病，力疾视事。同治六年（1867年）被任命为协办大学士、四川总督，不久舊病復發，死于任所。追赠为太子太傅，谥号“文忠”。

## 遺跡

工書法，[佛山市博物館藏有](../Page/佛山市博物館.md "wikilink")《楷書聯》，他在佛山園林建築撰有“群星草堂”匾額。

## 注釋

## 參考資料

  - 《[清史列传](../Page/清史列传.md "wikilink")·骆秉章传》

## 外部連結

  - [駱秉章](http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%C0d%AA%C3%B3%B9)
    中研院史語所

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝湖北按察使](../Category/清朝湖北按察使.md "wikilink")
[Category:清朝貴州布政使](../Category/清朝貴州布政使.md "wikilink")
[Category:清朝湖南巡撫](../Category/清朝湖南巡撫.md "wikilink")
[Category:清朝四川總督](../Category/清朝四川總督.md "wikilink")
[Category:清朝協辦大學士](../Category/清朝協辦大學士.md "wikilink")
[Category:清朝輕車都尉](../Category/清朝輕車都尉.md "wikilink")
[Category:花縣人](../Category/花縣人.md "wikilink")
[B秉](../Category/駱姓.md "wikilink")
[Category:諡文忠](../Category/諡文忠.md "wikilink")

1.
2.  王定安《湘军记》有评论：“骆文忠沉毅静镇，碌碌若无所能，而其大用在任贤不二，屈已以从人。……当其在湖南独任左文襄，筹饷募兵，事专于幕寮，谗丛毁积而弗之改。于是援师四出，捷音望于道，勋业巍巍被邻省矣。文襄既已大任，乃挈刘公蓉筹蜀事，用诸生不三载超擢藩抚。刘公勋望不逮文襄，要其文章志节，固一时之杰也。”
3.  第一历史档案馆所存档案:《潘祖荫折》（咸丰10、闰3、23）