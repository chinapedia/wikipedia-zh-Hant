**骠国**（,
;\[1\]\[2\]）是7至9世纪[缅甸骠人](../Page/缅甸.md "wikilink")（pyū）所建立的城邦国家，以[卑谬爲首都故也叫做](../Page/卑谬.md "wikilink")**卑谬王国**。

骠国留於今日緬甸的（ဟန်လင်း，Hañlìñ）、（ဗိဿနိုး，Bí Thá
Noù）和的遺跡已被列為[世界遺產](../Page/世界遺產.md "wikilink")，其他城市在未來可供該世界遺產的擴展。\[3\]\[4\]

## 略歷

[魏晋时期的](../Page/魏晋.md "wikilink")《[西南异方志](../Page/西南异方志.md "wikilink")》、《[南中八郡志](../Page/南中八郡志.md "wikilink")》等书首均载其名，有“位于[永昌西南三千里](../Page/永昌.md "wikilink")，君臣父子，长幼有序”的记载。另有**剽**、**僄**、**缥**、**漂越**等异译。其首都在（今[缅甸](../Page/缅甸.md "wikilink")[伊洛瓦底江下游](../Page/伊洛瓦底江.md "wikilink")[卑蔑](../Page/卑蔑.md "wikilink")），《[大唐西域记](../Page/大唐西域记.md "wikilink")》又称之为。\[5\]\[6\]另外，《[新唐书](../Page/新唐书.md "wikilink")》中还有**朱波**、**突罗朱**、**徒里掘**等异称。

## 興衰

8世纪时，骠国疆土北至南诏（今[云南](../Page/云南.md "wikilink")[德宏和缅甸交界地带](../Page/德宏.md "wikilink")），东至陆真腊（今[泰国](../Page/泰国.md "wikilink")、[老挝](../Page/老挝.md "wikilink")、[柬埔寨接壤地带](../Page/柬埔寨.md "wikilink")），西至东天竺（今[印度](../Page/印度.md "wikilink")[阿萨姆邦等地](../Page/阿萨姆邦.md "wikilink")），拥有整个伊洛瓦底江流域，共计9个城镇、18个属国、298个部落。[唐](../Page/唐朝.md "wikilink")[大和六年](../Page/大和.md "wikilink")（832年），骠国败于[南诏](../Page/南诏.md "wikilink")，自此衰落，而被缅族建立的[蒲甘王国取代](../Page/蒲甘王国.md "wikilink")，其属国部落民族也逐渐退入山区，被[缅人边缘化](../Page/缅人.md "wikilink")。

## 文化經貿

驃國根據印度的文字創製了自己的文字——[驃文](../Page/驃文.md "wikilink")，是[藏緬語族諸族中第二個有自己文字的民族](../Page/藏緬語族.md "wikilink")，略早於[藏文](../Page/藏文.md "wikilink")。

唐[贾耽](../Page/贾耽.md "wikilink")《[皇华四达记](../Page/皇华四达记.md "wikilink")》和[樊绰](../Page/樊绰.md "wikilink")《[蛮书](../Page/蛮书.md "wikilink")》中有很多条[中国与骠国交通通道的详细记载](../Page/中国.md "wikilink")，说明时双方往来已经很密切。骠国以[佛教音乐著称](../Page/佛教.md "wikilink")。唐[贞元十年](../Page/贞元.md "wikilink")（[:794年](../Page/:794年.md "wikilink")）南诏向唐朝称臣，骠国国王[雍羌也想臣服于唐](../Page/雍羌.md "wikilink")，曾几度遣使来中国献佛乐，并进贡了大量“骠乐器”。这些乐器用周制“[八音](../Page/八音.md "wikilink")”无法区分。唐贞元十七年（[:801年](../Page/:801年.md "wikilink")），骠国国王遣其子[舒难陀](../Page/舒难陀.md "wikilink")（Shwenadaw）率乐队和舞蹈家来到长安表演。[唐德宗授其父王以](../Page/唐德宗.md "wikilink")[太常卿](../Page/太常.md "wikilink")、舒难陀以[太仆卿之号](../Page/太仆.md "wikilink")。\[7\]唐朝诗人[白居易专作](../Page/白居易.md "wikilink")《[骠国乐](../Page/:s:骠国乐.md "wikilink")》以记述这件事情，《新唐书·骠国传》中对其歌舞有详细记载。\[8\]\[9\]\[10\]\[11\]\[12\]

## 相關影劇

  - 2013年末，央视电视剧《[舞乐传奇](../Page/舞乐传奇.md "wikilink")》，讲述骠国使节入唐献乐促成唐骠友好的经过。

## 圖片

<File:20160810> Palace Sri Ksetra 9190.jpg|室利差呾羅遺跡 <File:20160810>
Bawbawgyi Pogoda Sri Ksetra Pyay Myanmar 9252.jpg|包包枝佛塔 <File:Payama>
from SSE.jpg|帕耶摩佛塔
<File:Myazedi-Inscription-Pyu.JPG>|中的[驃文](../Page/驃文.md "wikilink")

## 腳注

[Category:古代族群](../Category/古代族群.md "wikilink")
[Category:緬甸歷史](../Category/緬甸歷史.md "wikilink")

1.   1967: 8 -
2.  [Aung-Thwin](http://www.hawaii.edu/cseas/faculty/aung-thwin.html)
    1996: 77 -
3.
4.  [骠国古城——缅甸的世界文化遗产](http://mhwmm.com/Ch/NewsView.asp?ID=18391)
5.  [:s:大唐西域記/10](../Page/:s:大唐西域記/10.md "wikilink")
6.  [:s:海國圖志/卷029](../Page/:s:海國圖志/卷029.md "wikilink")
7.  唐[白居易](../Page/白居易.md "wikilink")，《[與驃國王雍羌書](../Page/:s:與驃國王雍羌書.md "wikilink")》
8.  [:s:蠻書/卷10](../Page/:s:蠻書/卷10.md "wikilink")
9.  [:s:新唐書/卷022](../Page/:s:新唐書/卷022.md "wikilink")
10. [:s:新唐書/卷222下](../Page/:s:新唐書/卷222下.md "wikilink")
11. [:s:白氏長慶集/卷057](../Page/:s:白氏長慶集/卷057.md "wikilink")
12. [:s:讀史方輿紀要/卷一百十九](../Page/:s:讀史方輿紀要/卷一百十九.md "wikilink")