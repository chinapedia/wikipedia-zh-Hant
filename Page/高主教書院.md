**高主教書院**（英語：Raimondi
College，簡稱「**RC**」）是[香港一所由](../Page/香港.md "wikilink")[天主教香港教區於](../Page/天主教香港教區.md "wikilink")1958年創辦的傳統英中\[1\]
。校舍位於[中環](../Page/中環.md "wikilink")[半山](../Page/半山區.md "wikilink")[羅便臣道](../Page/羅便臣道.md "wikilink")2號，鄰近香港明愛總部和聖母無原罪主教座堂，主要由三座大樓組成，主建築樓高165[呎](../Page/呎.md "wikilink")，共有14層，為全港最多層數的中學，全校舍佔地10,200平方米\[2\]，同時也是香港[海拔最高的](../Page/海拔.md "wikilink")[中學](../Page/中學.md "wikilink")。學校同時於[灣仔和](../Page/灣仔.md "wikilink")[北角設有](../Page/北角.md "wikilink")[小學部及](../Page/#小學部.md "wikilink")[幼稚園部](../Page/#幼稚園部.md "wikilink")，中小幼全部合共約有2,100名學生就讀。

## 歷史

[HK_3A-H_Robinson_Garden_Apartment_view_Robinson_Road_bridges_高主教書院_Raimondi_College_Oct-2010.JPG](https://zh.wikipedia.org/wiki/File:HK_3A-H_Robinson_Garden_Apartment_view_Robinson_Road_bridges_高主教書院_Raimondi_College_Oct-2010.JPG "fig:HK_3A-H_Robinson_Garden_Apartment_view_Robinson_Road_bridges_高主教書院_Raimondi_College_Oct-2010.JPG")旁的山坡上\]\]
高主教書院的名稱來自一名宗座外方傳教會會士，主教（）。高雷門主教在1827年出生，曾於1874年至1894年從[意大利](../Page/意大利.md "wikilink")[米蘭遠赴香港傳教](../Page/米蘭.md "wikilink")，並成為香港第一任[宗座代牧](../Page/宗座代牧.md "wikilink")。

位置方面，高主教書院地勢較高，其所處的[羅便臣道](../Page/羅便臣道.md "wikilink")2號地皮在早期屬於[香港聖母無原罪主教座堂一部份](../Page/香港聖母無原罪主教座堂.md "wikilink")，後來在1921年成為[香港華仁書院的主校舍](../Page/香港華仁書院.md "wikilink")，該址至今仍然在天主教香港教區的管理範圍內。

1955年，[香港華仁書院遷往](../Page/香港華仁書院.md "wikilink")[皇后大道東新校舍](../Page/皇后大道.md "wikilink")。1958年4月10日，適值[宗座外方傳教會慶祝來港服務](../Page/宗座外方傳教會.md "wikilink")100周年，高主教書院由當時的[教區](../Page/教區.md "wikilink")[主教](../Page/主教.md "wikilink")[白英奇主教奠基興建](../Page/白英奇.md "wikilink")，並邀請訪港的[聖座傳信部部長](../Page/傳信部.md "wikilink")[樞機為該校主持祝聖](../Page/樞機.md "wikilink")。

高主教書院的舊生會、家長教師會和學生會先後於1961年、1966年和1969年成立\[3\]，期間隨著中六大樓（現）落成也於1967年開辦了三班中六預科課程。1978年，高主教書院成為[津貼學校](../Page/津貼學校.md "wikilink")。

高主教書院在創校之初已採用英語授課，而[教育統籌局也在](../Page/教育統籌局.md "wikilink")1998年正式批准高主教書院使用[英語作為主要教學語言](../Page/英語.md "wikilink")，該校隨即成為香港114間[英文授課中學](../Page/英文授課中學.md "wikilink")（即學校）\[4\]之一。

2008年，高主教書院迎來50週年校慶，同年宣布小學部遷往灣仔[司徒拔道](../Page/司徒拔道.md "wikilink")[肇輝臺](../Page/肇輝臺.md "wikilink")1E\[5\]，即前聖瑪加利書院校園。中小學部亦同時轉為男女校\[6\]\[7\]\[8\]。

### 年表

[HK_RaimondiCollege.jpg](https://zh.wikipedia.org/wiki/File:HK_RaimondiCollege.jpg "fig:HK_RaimondiCollege.jpg")
[HK_16th_Caine_Rd_60405_hi.JPG](https://zh.wikipedia.org/wiki/File:HK_16th_Caine_Rd_60405_hi.JPG "fig:HK_16th_Caine_Rd_60405_hi.JPG")

  - 1958年：
    校舍主大樓落成，第一任校監及校長為祈良[神父](../Page/神父.md "wikilink")。首屆入讀中小學部的學生合共有2430人。
  - 1960年：
    11月，舉行首屆陸運會。
  - 1961年：
    12月，推出首冊校刊《》。
    舊生會（當時稱為the Old Boys' Association）成立。

<!-- end list -->

  - 1962年：
    家長教師會成立。

<!-- end list -->

  - 1967年：
    10月，成立班長與學會主席委員會。
    中六大樓落成，開辦了三班中六預科課程。
  - 1968年：
    班長與學會主席委員會更名為高主教書院學生會（）。
    10月，舉行首屆水運會。
    11月，[學生會推出首本文集](../Page/#學生會.md "wikilink")《砥柱》。
  - 1971年：
    4月，時任[港督](../Page/港督.md "wikilink")[戴麟趾參觀學校](../Page/戴麟趾.md "wikilink")。
    9月，學校校舍擴展工程完成。
  - 1977年：
    8月，舊生會推出首份新聞集《》。

<!-- end list -->

  - 1978年：
    高主教書院成為[津貼學校](../Page/津貼學校.md "wikilink")。
  - 1990年：
    高主教書院學生會進行架構重組，並由「」更名為「」。
    創校校長，時任校監祁良神父逝世。
  - 1992年：
    曾慶文神父接替已辭世的祁良神父成為高主教書院校監。
    祁良神父教育基金正式成立。

<!-- end list -->

  - 1996年：

<!-- end list -->

  -
    中學部禮堂（祁良堂）完成翻新。
  - 1998年
    成為香港114間[英文授課中學](../Page/香港英文授課中學.md "wikilink")（即EMI學校）之一。

<!-- end list -->

  - 2000年：

<!-- end list -->

  -
    在這學年起，預科開始招收女生。

<!-- end list -->

  - 2002年：

<!-- end list -->

  -
    舊生會英文名稱 Raimondi Old Boys' Association 更名為 Raimondi Alumni
    Association (RAA)。
  - 2008年：
    1月，高主教書院宣布小一和中一同時開始招收女生\[9\]\[10\]，並積極推行[三三四高中教育改革](../Page/三三四高中教育改革.md "wikilink")。
    5月，於香港文化中心舉辦50周年校慶音樂會
    8月，高主教書院小學部正式搬遷往[灣仔](../Page/灣仔.md "wikilink")[司徒拔道肇輝臺](../Page/司徒拔道.md "wikilink")1E\[11\]，即前[聖瑪加利書院](../Page/聖瑪加利書院.md "wikilink")[校園](../Page/校園.md "wikilink")\[12\]\[13\]。
  - 2012年:
    9月，接辦位於[北角建華街三十號的天主教聖猶達幼稚園](../Page/北角.md "wikilink")，正式復辦高主教書院幼稚園部\[14\]。
    李崇德太平紳士接替曾慶文神父成為高主教書院校監。
  - 2013年
    5月，於香港大會堂舉行校慶音樂會「」，慶祝學校創校55週年。
    9月，校長區嘉為退休，由前[荔景天主教中學校長盧詠琴女士接任該職](../Page/荔景天主教中學.md "wikilink")\[15\]。
  - 2014年
    9月，開始推行[小班教學](../Page/小班教學.md "wikilink")，將中一由4班擴至5班，每班人數從一班30多人減至20多人，並逐年往較高班級擴展\[16\]。
    家長教師會進行改組，將中小學部的家長教師會分拆，成立高主教書院家長教師會。
  - 2015年
    4月26日，高主教書院家長教師會首次於[山頂舉行](../Page/山頂.md "wikilink")「高峰跑」籌款活動\[17\]。
    5月14日，於[香港大會堂舉行音樂會](../Page/香港大會堂.md "wikilink")「」。該場音樂會除了是再次與小學部合辦之外，也首次邀請了幼稚園部學生參與演出\[18\]。
    5月21日，首次與小學部合辦水運會。
  - 2017年
    2月25日，高主教書院舊生會首次舉行「高瞻行」步行籌款活動，由小學部步行至中學部。
    7月3日，於[香港大會堂舉行音樂會](../Page/香港大會堂.md "wikilink")「」。該場音樂會除了是再次與小學部合辦之外，也邀請了幼稚園部學生參與演出\[19\]，慶祝學校即將邁向60周年鑽禧。
    11月26日，高主教書院家長教師會再次於[山頂舉行](../Page/山頂.md "wikilink")「高峰跑」籌款活動\[20\]。
  - 2018年
    3月24日，高主教書院舊生會於[香港會議展覽中心舉行](../Page/香港會議展覽中心.md "wikilink")60周年校慶晚宴，筵開120席<ref>

[1](http://www.raimondi.edu.hk/en/latest_new/60th-anniversary-banquet)</ref>。

### 歷任校監

<table>
<thead>
<tr class="header">
<th></th>
<th><p>姓名</p></th>
<th><p>任期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1.</p></td>
<td></td>
<td><p>1958 - 1991</p></td>
</tr>
<tr class="even">
<td><p>2.</p></td>
<td><p>Father J. B. Tsang</p></td>
<td><p>1992 - 2012</p></td>
</tr>
<tr class="odd">
<td><p>3.</p></td>
<td><p>Mr. Peter S. T. Lee BBS JP</p></td>
<td><p>2012 -</p></td>
</tr>
</tbody>
</table>

### 歷任校長

<table>
<thead>
<tr class="header">
<th></th>
<th><p>姓名</p></th>
<th><p>任期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1.</p></td>
<td></td>
<td><p>1958 - 1961</p></td>
</tr>
<tr class="even">
<td><p>2.</p></td>
<td></td>
<td><p>1961 - 1964</p></td>
</tr>
<tr class="odd">
<td><p>3.</p></td>
<td></td>
<td><p>1964 - 1968</p></td>
</tr>
<tr class="even">
<td><p>4.</p></td>
<td></td>
<td><p>1968 - 1995</p></td>
</tr>
<tr class="odd">
<td><p>5.</p></td>
<td></td>
<td><p>1995 - 2013</p></td>
</tr>
<tr class="even">
<td><p>6.</p></td>
<td></td>
<td><p>2013 -</p></td>
</tr>
</tbody>
</table>

## 學校象徵

### 校訓

  - 堅毅力行（）

### 核心價值

  - Love - 愛心
  - Unity - 協和
  - Perseverance - 堅毅

## 校舍

[RC_Front.JPG](https://zh.wikipedia.org/wiki/File:RC_Front.JPG "fig:RC_Front.JPG")
整個校園分為三幢建築物，全部設有有蓋通道相連。

  - （前稱：主大樓）樓高14層，中一至中五所有班級的課室均位於此大樓。大樓有升降機連接地下至7樓，而前往較高樓層則可於5樓至8樓轉乘另一部升降機。中一至中四級全部在課室內設有儲物櫃。

  - （前稱：）樓高10層，多數特別室均位於此大樓。大樓未設有升降機，但地下至8樓均與相連，可以使用相鄰的升降機。

  - （前稱：）樓高5層（樓層對應另外兩座大樓，設定為3樓至7樓），所有中六班別課室均位於此大樓。大樓共設有兩條通道通往，分別是5樓的無蓋通道連接
    5樓、以及7樓由[宏基國際賓館管理的有蓋通道連接](../Page/宏基國際賓館.md "wikilink")
    8樓，當中5樓與7樓的通道又有樓梯互相相連。5樓無蓋通道路旁另有職員通道連接「」花園和3樓。

  - [宏基國際賓館緊鄰](../Page/宏基國際賓館.md "wikilink")及，位於正對面，部分空間獲劃予學校使用。

全校所有課室均設有多媒體投影機、銀幕及上網設備。

### 設施

<table>
<tbody>
<tr class="odd">
<td><dl>
<dt>主要設施</dt>

</dl>
<ul>
<li><strong>57</strong>個課室</li>
<li><strong>2</strong>個禮堂
<ul>
<li>耀基堂（Block A）</li>
<li>祈良堂（Block C）</li>
</ul></li>
<li><strong>1</strong>個<a href="../Page/圖書館.md" title="wikilink">圖書館</a>（Block A）</li>
<li><strong>6</strong>個<a href="../Page/實驗室.md" title="wikilink">實驗室</a>
<ul>
<li><strong>1</strong>個<a href="../Page/生物.md" title="wikilink">生物實驗室</a>（Block A）</li>
<li><strong>2</strong>個<a href="../Page/物理.md" title="wikilink">物理實驗室</a>（Block A、C）</li>
<li><strong>2</strong>個<a href="../Page/化學.md" title="wikilink">化學實驗室</a>（Block A、C）</li>
<li><strong>1</strong>個綜合科學實驗室（Block B）</li>
</ul></li>
<li><strong>5</strong>個操場
<ul>
<li><strong>1</strong>個有蓋<a href="../Page/籃球.md" title="wikilink">籃球場</a>（Block A）</li>
<li><strong>1</strong>個露天籃球場（Block A）</li>
<li><strong>3</strong>個有蓋操場（Block B、C）</li>
</ul></li>
<li><strong>4</strong>個教員室（Block A、B、C）</li>
<li><strong>3</strong>個<a href="../Page/電腦.md" title="wikilink">電腦室</a>（Block B、C）</li>
</ul></td>
<td><dl>
<dt>其他設施</dt>

</dl>
<ul>
<li>Block A
<ul>
<li><a href="../Page/童軍.md" title="wikilink">童軍室</a>（1樓）</li>
<li><a href="../Page/融合教育.md" title="wikilink">SEN室</a>（2樓）</li>
<li><a href="../Page/小食.md" title="wikilink">小食部</a>（4樓）</li>
<li>男女更衣室（4樓）</li>
<li><a href="../Page/地理.md" title="wikilink">地理室</a>（5樓）</li>
<li><a href="../Page/醫療.md" title="wikilink">醫療室</a>（5樓）</li>
<li>攝影學會攝製室（5樓）</li>
<li>校務處（9樓）</li>
<li>面談室（9樓）</li>
<li>升學輔導中心（14樓）</li>
<li><a href="../Page/舞蹈.md" title="wikilink">舞蹈室</a>（14樓）</li>
</ul></li>
</ul></td>
<td><ul>
<li>Block B
<ul>
<li>VR Cave (建設中)（1樓）</li>
<li>會議室（2樓）</li>
<li>祈禱室（2樓）</li>
<li><a href="../Page/音樂.md" title="wikilink">音樂室</a>（3樓）</li>
<li><a href="../Page/美術.md" title="wikilink">美術室</a>（5樓）</li>
<li>飛行學會室（5樓）</li>
<li><a href="../Page/健身.md" title="wikilink">健身室</a>（8樓）</li>
<li><a href="../Page/社工.md" title="wikilink">社工輔導室</a>（8樓）</li>
<li><a href="../Page/多媒體.md" title="wikilink">多媒體</a><a href="../Page/學習.md" title="wikilink">學習中心</a>（9樓）</li>
<li><a href="../Page/家政.md" title="wikilink">家政室</a>（10樓）</li>
</ul></li>
<li>Block C
<ul>
<li>學生會辦事處（6樓）</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 班級結構

中一及中二級自2014-2015學年起推行全面小班教學，每級各設五班，每班人數24-29人，其中E班為精英班。2017-2018學年起，中一級試行兩班精英班，因此1D班亦成為精英班，而此亦延伸至2018-2019學年2D班。

中三級共設四班，每班人數27-33人，其中3D班為精英班，3ABC班中文、英文、數學課堂或會分組上課。

中四至中六級每級各設四班，每班人數23-36人，根據學生高中選修科而決定班別，其中A班為經濟科班（），B班為生物科班（），C班為化學科班（），D班為物理科班（）。中三級學生在下學期末開始填寫選修科意願，並於升中四的暑假開始時（七月第三個星期）由副校長透過學校網站宣佈中四班別的結果，而上述四班所代表的選修科，必須在填寫意願時涵蓋至少其中一科在意願中。上述四個主要選修科除四班本身自己班個別時間表外，亦在時間表上選修科時段另設一班供其餘三班選修同樣科目的學生就讀。此外，中四及中五通識科會分組上課，為A、B班及C、D班各自分三組；2018-2019學年中六級中文科按成績高低分組上課而非按自己班別上課。

近年學校轉型為男女校，但女生數目普遍較少。
初時初中的女生因人數少而主要集中在一級其中1-2班，但近年女生比例比以往有所上升，因此學校亦安排女社工在有需要時為女學生作出適當輔導。2018-2019年度中一級共取錄35名女生，創歷年新高。

教師編制方面，全校26班中，初中全面實施雙班主任制；而高中自新高中學制開始後已轉為單班主任制。2017-2018學年起，中五級試行重推雙班主任制，2018-2019學年則只限中四級推行。

## 科目

#### 必修科目

  - [中國語文](../Page/中國語文.md "wikilink")（中一至中六級，中一至中三級包括[普通話](../Page/普通話.md "wikilink")，中四及中六級設特定說話練習時段）
  - [英國語文](../Page/英國語文.md "wikilink")（另設教授英語文化的課程：中一至中三級設俗稱LA的語言藝術課，現改稱Language
    Across Curriculum（English
    LAC），中一至中二級及部分中三班別由外籍教師教授；中四至中五級則設英文科選修部分；中六級設指定說話練習時段）
  - [數學（必修部分）](../Page/數學.md "wikilink")（中一至中六級）
  - [通識教育](../Page/通識教育.md "wikilink")（中四至中六級）
  - 綜合科學（中一、中二級）
  - [物理](../Page/物理.md "wikilink")（中三級）
  - [化學](../Page/化學.md "wikilink")（中三級）
  - [生物](../Page/生物.md "wikilink")（中三級）
  - 生活與社會（中一、中二級）
  - 經濟與財務（中三級）
  - [地理](../Page/地理.md "wikilink")（中一至中三級）
  - [歷史](../Page/歷史.md "wikilink")（中一至中三級）
  - [中國歷史](../Page/中國歷史.md "wikilink")（中一至中三級）
  - [音樂](../Page/音樂.md "wikilink")（中一至中四級）
  - [視覺藝術](../Page/視覺藝術.md "wikilink")（中一至中四級）
  - [體育](../Page/體育.md "wikilink")（中一至中六級）
  - 普通電腦（中一至中三級）

中三級英文科與音樂科於每年五月下旬舉辦音樂劇，全體中三級學生於英文課（語言藝術課堂）進行劇本撰寫，透過課堂教授的語言技巧學以致用，配合選擇適合的音樂，於音樂劇表演當天演出，以每組約半班（15-18人）一組演出。

中四級於每年五月份舉辦時裝表演，由視覺藝術科及音樂科合辦，於視覺藝術課介紹各種布料應用及設計再選取合適背景音樂。每組可獲最多$80資助。

中五級英文科於每年二月份舉行Trade
Market，學生分成每半班一組，在攤位向中一學生提供服務或售賣貨品；而中一學生將以老師事先派發的代用券支付相關貨品或服務。每組最多可獲學校英文科提供$200撥款資助。

#### [香港中學文憑考試指定選修科目](../Page/香港中學文憑考試.md "wikilink")

  - [物理](../Page/物理.md "wikilink")（中四至中六級D班及ABC班選修，每級共兩班）
  - [化學](../Page/化學.md "wikilink")（中四至中六級C班及ABD班選修，每級共兩班）
  - [生物](../Page/生物.md "wikilink")（中四至中六級B班及ACD班選修，每級共兩班）
  - 資訊及通訊科技（簡稱ICT，中四至中六級）
  - [經濟](../Page/經濟.md "wikilink")（中四至中六級A班及BCD班選修，每級共兩班）
  - 企業、會計與財務概論（俗稱BAFS，中四至中六級，中四級下學期（一般為5月）分拆為企業班及會計班，由科主任按成績及意願安排）
  - [地理](../Page/地理.md "wikilink")（中四至中六級）
  - [歷史](../Page/歷史.md "wikilink")（中四至中六級）
  - [中國歷史](../Page/中國歷史.md "wikilink")（中四至中六級）
  - [視覺藝術](../Page/視覺藝術.md "wikilink")（中四至中六級，須待視覺藝術科主任批准）
  - [體育](../Page/體育.md "wikilink")（中四至中六級，只限校內體育校隊學生修讀）
  - [數學（延伸部分）](../Page/數學.md "wikilink")（單元一微積分與統計，俗稱M1；單元二代數與微積分，俗稱M2；2017年前為中四至中六級選修科，2017-2018年度起納入中四級必修部分及改作只限中五及中六級選修）

#### 近年停辦的[香港中學文憑考試指定選修科目](../Page/香港中學文憑考試.md "wikilink")

  - [音樂](../Page/音樂.md "wikilink")（2015-2016年度中四級為最後一屆，該屆只有一名女學生修讀）

## 學生組織

### 學生會

高主教書院學生會成立於1969年，每年由5至10人組成執行委員會。執行委員會在為期一年的任期內需負責舉辦各種校內校外活動、向鄰近商鋪爭取福利、以及代表學生與學校管理層交流。每年學生會執行委員會亦會通過[面試](../Page/面試.md "wikilink")，挑選最多40位學生成爲附屬委員協助舉辦各個活動。全校學生均為學生會會員，可享用學生會的福利和參與其舉辦的活動。學生會辦事處位於Block
C 6樓電腦室旁，於午膳時間和放學後開放。

#### 執行委員會架構

  - 核心職位：

<!-- end list -->

  - 會長
  - 內務副會長
  - 外務副會長
  - 秘書長
  - 財務秘書

<!-- end list -->

  - 附加職位：

<!-- end list -->

  - 內務秘書
  - 外務秘書
  - 活動總監
  - 宣傳總監
  - 公共關係總監

#### 選舉

學生會執行委員會每年舉行一次全校選舉，以選出繼任內閣。中四至中五級的學生可自由組成候選內閣，並在選舉前推出參選[政綱](../Page/政綱.md "wikilink")、公開[拉票](../Page/拉票.md "wikilink")、出席答問大會與[辯論大會等各種競選活動](../Page/辯論.md "wikilink")，再由全校學生投票選出。在棄權票少於半數時，最高票的候選內閣將會當選；棄權票過半數時則將由學生會評議會（由各班級班會主席、正班長和各學會主席組成）舉行特別會議進行重選，對每個職位進行投票。在沒對手競爭的情況下，學生仍需進行信任投票，若不信任票多於信任票內閣則告落選，必須同樣經學生會評議會進行重選。若當年度沒有競選內閣、或唯一內閣在重選後仍未滿足五個核心職位最低要求，各個空置職位將由校長任命中五學生出任。

#### 活動

  - 校園[定向](../Page/定向運動.md "wikilink")：於校園各處設計任務，供初中學生參加。
  - 聯校口試訓練：與其他學校合辦或主辦口試訓練，供中六學生參加。
  - 聯校聖誕[舞會](../Page/舞會.md "wikilink")：與其他學校合辦或主辦聖誕舞會，供中四至中六學生參加。
  - 維園年宵攤檔：與舊生會合辦，於[維多利亞公園年宵市場營運攤檔售賣由學生設計的貨品](../Page/維多利亞公園年宵市場.md "wikilink")，資金由舊生會提供。在組織委員會，籌備，設計產品等工作上都由學生作領導的角色，老師和舊生只會在旁協助和提供意見，以訓練學生的領導才能。攤位由創始起一直沿用「高檔」作為名稱\[21\]，亦會在每年踏入大年初一的凌晨高唱校歌。
  - [盤菜宴](../Page/盤菜.md "wikilink")：與舊生會合辦，邀請舊生於晚上聚餐。
  - 微笑日：當日於全校邀請師生拍照，並合成師生的笑容製成橫額。
  - 高主教節：舉辦各種比賽及活動，包括與體育聯會合辦師生足球和籃球比賽。
  - 綜藝表演）：作為中一至中五試後活動，執行委員會需負責編寫並親身演出一場綜藝表演。

#### 年表

  - 2004/2005年度
    第36屆，「」
    發佈第一代高主教書院學生會憲章。
  - 2008/2009年度
    第40屆，「」
    首次於陸運會舉行全校師生大合照。
    重新推出停刊已久的學生會官方刊物《》。
    獲舊生會贊助，開始在[維多利亞公園年宵市場營辦攤檔](../Page/維多利亞公園年宵市場.md "wikilink")。
  - 2009/2010年度
    第41屆，「」
    因應[三三四高中教育改革](../Page/三三四高中教育改革.md "wikilink")，開始允許中五級學生競選學生會。
  - 2010/2011年度
    第42屆，「」
    首次由中五級學生競選學生會，同時產生首名中五級會長。
  - 2011/2012年度
    第43屆，「」
    唯一候選內閣「」未能得到過半信任票，重組後經學生會評議會進行重選，易名為「」。
  - 2013/2014年度
    第45屆，「」\[22\]
    2013年11月13日，首次以拼字方式於陸運會拍攝全校師生大合照。
    2014年2月22日，首次與舊生會合辦盤菜宴（）。
    2014年4月7日，首次舉辦微笑日（）。
  - 2014/2015年度
    第46屆，「」\[23\]
    2014年10月16日，首次組成打氣隊出席學界游泳比賽。
    2015年2月11日，與攝影學會合作，首次於陸運會使用[四軸飛行器拍攝全校師生大合照](../Page/四軸飛行器.md "wikilink")。
    2015年4月17日，憲章訂正草案在學生會評議會獲得通過，明確定義並更新學生會執行委員會職位組成架構和選舉流程等事項。
    2015年4月26日，推出全新校訓T-Shirt系列。
  - 2015/2016年度
    第47屆，「」\[24\]
    2016年2月2日，首次組成打氣隊出席學界田徑比賽；於當日田徑隊成功晉升到下一組別。
    2016年2月27日，首次在盤菜宴中於地下操場擺放巨型展板供舊生拍照。
    2016年4月29日，首次舉辦「Running Student」活動。
  - 2016/2017年度
    第48屆，「」\[25\]
    2017年1月23日，首次為高主教中六學生送上特別版校訓T-Shirt。
    2017年5月7日，首次在學校上空掛出大型橫額，為高主教節（RC Festival）展開序幕。
    2017年7月19日，首次舉辦領袖訓練營（）。
  - 2017/2018年度
    第49屆，「」
    2017年9月21日，「」作為唯一候選內閣未能得到過半信任票，重組後經學生會評議會重選並當選。
    2017年9月29日， 經過重選後，內閣由9位成員減至8位成員。
    2017年11月26日，聯同高主教書院家長教師會合辦「60週年鑽禧校慶高峯跑」（Diamond Jubilee Running To
    The Peak)
    2018年3月24日，聯同高主教書院家長教師會以及高主教書院舊生會，於香港會議展覽中心合辦高主教書院「高朋滿座」60週年鑽禧校慶晚宴。
    2018年5月12日，舉行高主教節（RC Festival) FIRANDE，並首次於校內外各懸掛4塊大型橫額。
    2018年5月16日，於高主教節（RC Festival）首次舉辦閃避盤比賽，並邀請副校長談國軒先生一同參與比賽。
  - 2018/2019年度
    第50屆，「」\[26\]

### 學長團

學長團（）是高主教書院的[風紀組織](../Page/風紀.md "wikilink")，負責於全校執行紀律管制。學長團同時負責在陸運會、水運會等大型活動中維持秩序，也在學生會選舉中擔任監票的角色。

學長團執行委員會由七名中五學生組成，分別有一名首席學長（）、一名副首席學長（），以及五名助理首席學長（），當中五名助理首席學長也各自負責「」、「」、「」、「」和「」五個部門。

2017-2018年度，負責「」的助理首席學長因私人理由退出執行委員會，因此委員會減至六人，由副首席學長（）兼任負責「」部門。

### 社制

社制始於1980年，目的在於促進良性競爭的氣氛，加強同學對其所屬的社的歸屬感。在一個社中，有來自各級各班的學生，由高中生帶領低年級學生投入校園生活，參加不同的校內活動，如陸運會、水運會等競技活動，皆是以「社」為單位進行比賽，以鼓勵學生的團隊精神和合作精神，也使不同年級的學生能夠融洽共處。而每個社分別有一名社長，由學生主動自薦後，經老師選出最佳人選擔任；而社監則會為老師擔任，每個社都有一名社監負責協助轄下的社長舉辦活動。

校內設有6個社，但於90年代末更改為5個社，後來於千禧後取消了社制，並以[臺灣流行的班](../Page/臺灣.md "wikilink")-{制}-取代。校方分別在2015年4月和2016年3月舉辦兩次全校諮詢會，並於2016年9月復辦社-{制}-。

現在的4個社分別為：

| 社           | 顏色                                   |
| ----------- | ------------------------------------ |
| St. Luke    | <span style="color: white">紅色</span> |
| St. Mark    | <span style="color: white">藍色</span> |
| St. John    | <span style="color: navy">黃色</span>  |
| St. Matthew | <span style="color: white">綠色</span> |

### 課外學會及校隊

學生會屬下設有各類學會，學生可自由選擇參加任何學會，亦可在老師的指導下籌辦學會，以培養學生對各方面的興趣和知識。所有學會均由學生擔任幹事籌辦活動。

為配合[三三四高中教育改革](../Page/三三四高中教育改革.md "wikilink")，學校在2012年開辦[通識學會](../Page/通識.md "wikilink")，新聞聯會亦於同年更名為文社。2015年，校園電視台併入攝影學會，英文名稱更改為「」。

#### 主要科目學會

  - 中文學社
  - 英文學會
  - 數學學會
  - [通識學會](../Page/通識.md "wikilink")
  - 科學聯會
  - [社會科學聯會](../Page/社會科學.md "wikilink")

#### 服務及制服團體

#### 運動校隊

#### 文化學會

#### 宗教學會

  - [天主教同學會](../Page/天主教.md "wikilink")
  - 基督徒團契
  - [聖母軍](../Page/聖母軍.md "wikilink")（）

#### 興趣學會

### 音樂及體育發展

高主教書院多年來積極參與[香港校際音樂節和](../Page/香港校際音樂節.md "wikilink")[香港學校朗誦節](../Page/香港學校朗誦節.md "wikilink")，學校的管樂團中亦有不少團員是香港青年管樂演奏家的團員。在2008年及2009年，學校的弦樂團於第60及61屆香港校際音樂節的弦樂中級組中奪得冠軍。

在運動方面，亦擁有各類運動校隊，包括[田徑](../Page/田徑.md "wikilink")、[游泳](../Page/游泳.md "wikilink")、[足球](../Page/足球.md "wikilink")、[籃球](../Page/籃球.md "wikilink")、[欖球](../Page/欖球.md "wikilink")、[羽毛球](../Page/羽毛球.md "wikilink")、[乒乓球](../Page/乒乓球.md "wikilink")、[越野及](../Page/越野.md "wikilink")[劍擊隊等](../Page/劍擊.md "wikilink")，多年來積極參加各項校際比賽，當中如羽毛球等隊伍更長年於第一組別作賽。在2007/2008學年，學校的田徑隊於[中學校際田徑錦標賽中取得第三組的全場總冠軍](../Page/中學校際田徑錦標賽.md "wikilink")，同年籃球隊於[中學校際籃球比賽](../Page/中學校際籃球比賽.md "wikilink")（港島區）中亦取得第一組的全場總亞軍及第二組的全場總冠軍。在2017/2018學年，學校的游泳隊於中學校際游泳比賽第二組中取得男子全場總冠軍，亦取得男子甲組冠軍，男子乙組季軍及男子丙組亞軍，成功晉升第一組。是次為游泳隊自成立50年來，首次以全場總冠軍晉升中學校際游泳比賽第一組。

另外，學校也申請了[優質教育基金](../Page/優質教育基金.md "wikilink")，使學生能夠更加完善地進行訓練。學校目前擁有三個制服團體，分別為港島第99旅童軍團、港島第99旅深資童軍團和[香港紅十字會青年團第](../Page/香港紅十字會青年團.md "wikilink")7團。

### 出版刊物

<table>
<thead>
<tr class="header">
<th><p>刊物</p></th>
<th><p>類型</p></th>
<th><p>製作部門</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>校刊</p></td>
<td><p>學校</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>年刊</p></td>
<td><p>學生會</p></td>
</tr>
<tr class="odd">
<td><p>（《砥柱》）</p></td>
<td><p>散文集</p></td>
<td><p>文社</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>雜誌</p></td>
<td><p>英文學會</p></td>
</tr>
</tbody>
</table>

{{-}}

## 小學部

**高主教書院小學部**（），是[天主教香港教區屬下的一所](../Page/天主教香港教區.md "wikilink")[英文小學](../Page/英文.md "wikilink")，創辦於1958年。

高主教書院小學部直屬於高主教書院，原與中學部共用位於香港中環[半山](../Page/半山區.md "wikilink")[羅便臣道](../Page/羅便臣道.md "wikilink")2號的校舍，但爲了讓中學部在推行[三三四高中教育改革後有更大空間作教學用途](../Page/三三四高中教育改革.md "wikilink")，小學部於2008年8月搬遷往位於[灣仔司徒拔道肇輝臺](../Page/灣仔.md "wikilink")1E的前[聖瑪加利書院校舍](../Page/聖瑪加利書院.md "wikilink")，同時轉為男女校。2014-2015年，陳麗儀校長退休，由呂麗青女士接任，修訂考試制度並增設測驗。2016年4月1日，原為副校長的劉薇薇女士接替呂麗青女士，擔任校長一職。劉薇薇女士其後於2017年8月榮休結束其在高主教書院小學部41年的服務，而校長一職由林銀燕女士接任。

{{-}}

## 幼稚園部

**高主教書院幼稚園部**（）是[天主教香港教區屬下的一所](../Page/天主教香港教區.md "wikilink")[幼稚園](../Page/幼稚園.md "wikilink")，直屬於高主教書院，創辦於2012年。

高主教書院幼稚園部早期位於小學部校舍香港[羅便臣道](../Page/羅便臣道.md "wikilink")2號，校方於1999年9月因小學部地方不敷應用而暫停營辦。

[天主教香港教區於](../Page/天主教香港教區.md "wikilink")2012年9月重辦高主教書院幼稚園部，於教區屬校天主教聖猶達幼稚園香港[北角](../Page/北角.md "wikilink")[建華街](../Page/建華街.md "wikilink")30號的校址為高主教書院幼稚園部校址提供幼兒教育服務。

## 香港傑出學生選舉

直至2018年（第33屆），在[香港傑出學生選舉共出產](../Page/香港傑出學生選舉.md "wikilink")1名傑出學生。

## 知名校友

### 政界

  - [唐英年](../Page/唐英年.md "wikilink")：[全國政協常委](../Page/全國政協常委.md "wikilink")、前[政務司司長](../Page/政務司司長.md "wikilink")（高主教書院小學部）
  - [林瑞麟](../Page/林瑞麟.md "wikilink")：
    前[政務司司長](../Page/政務司司長.md "wikilink")（高主教書院小學部）
  - [石禮謙](../Page/石禮謙.md "wikilink")：[立法會議員（地產及建造界）](../Page/第六屆香港立法會.md "wikilink")、[北京清華大學顧問教授](../Page/北京清華大學.md "wikilink")、前[土地發展公司總裁](../Page/土地發展公司.md "wikilink")
  - [鄭承隆](../Page/鄭承隆.md "wikilink")：新思維副主席、北斗基金會長、前[新民黨副主席](../Page/新民黨_\(香港\).md "wikilink")、[仁濟醫院第四十五屆董事局主席](../Page/仁濟醫院_\(香港\).md "wikilink")、仁濟執行委員會主席、仁濟醫院管治委員會前主席，前高主教校友會會長
  - [鄭耀棠](../Page/鄭耀棠.md "wikilink")：港區[全國人大代表](../Page/全國人大代表.md "wikilink")、[行政會議非官守成員](../Page/行政會議.md "wikilink")、[香港工會聯合會會長](../Page/香港工會聯合會.md "wikilink")、中港關係策略發展研究基金主席、樂群社會服務處主席\[27\]
  - [黃志偉](../Page/黃志偉.md "wikilink")：[民主黨創黨成員](../Page/民主黨_\(香港\).md "wikilink")，現職於[香港城市大學](../Page/香港城市大學.md "wikilink")
  - [吳兆康](../Page/吳兆康.md "wikilink")：民主黨半山東社區顧問，[中西區區議員](../Page/中西區_\(香港\).md "wikilink")
  - [朱凱迪](../Page/朱凱迪.md "wikilink")：[立法會議員（新界西）](../Page/第六屆香港立法會.md "wikilink")，撰稿人，社會運動人士

### 公務員

  - [張建宗](../Page/張建宗.md "wikilink")：政務司司長、前[勞工及福利局局長](../Page/勞工及福利局局長.md "wikilink")、前[勞工處處長](../Page/勞工處.md "wikilink")、前[教育署署長](../Page/教育署.md "wikilink")、前[教育統籌局副局長](../Page/教育統籌局.md "wikilink")
  - [楊立門](../Page/楊立門.md "wikilink")：前[民政事務局](../Page/民政事務局.md "wikilink")[常任秘書長](../Page/常任秘書長.md "wikilink")
  - [孫德基](../Page/孫德基.md "wikilink")：前[審計署署長](../Page/審計署署長.md "wikilink")、前[安永會計師事務所中國區主席](../Page/安永會計師事務所.md "wikilink")\[28\]
  - [曾偉雄](../Page/曾偉雄.md "wikilink")：前[警務處處長](../Page/警務處處長.md "wikilink")
  - [梁志彬](../Page/梁志彬.md "wikilink")：[香港警務處前港島總區指揮官](../Page/香港警務處.md "wikilink")
  - [關劍輝](../Page/關劍輝.md "wikilink")：[香港警務處前觀塘警區副指揮官](../Page/香港警務處.md "wikilink")（歿）
  - [林振敏](../Page/林振敏.md "wikilink")：前[香港消防處處長](../Page/香港消防處處長.md "wikilink")
  - [卓永興](../Page/卓永興.md "wikilink")：[創新及科技局](../Page/創新及科技局.md "wikilink")[常任秘書長](../Page/常任秘書長.md "wikilink")、前[勞工處處長](../Page/勞工處.md "wikilink")、前[食物及衛生局副秘書長](../Page/食物及衛生局.md "wikilink")、前[食物環境衛生署署長](../Page/食物環境衛生署.md "wikilink")
  - [韓志強](../Page/韓志強.md "wikilink")：[發展局常任秘書長](../Page/發展局.md "wikilink")(工務)、前[土木工程拓展署署長](../Page/土木工程拓展署.md "wikilink")
  - [王永平](../Page/王永平_\(香港\).md "wikilink")：前[公務員事務局局長](../Page/公務員事務局.md "wikilink")
  - [馮載祥](../Page/馮載祥.md "wikilink")：前[立法會秘書長](../Page/香港立法會.md "wikilink")
  - [麥靖宇](../Page/麥靖宇.md "wikilink")：前[行政長官辦公室](../Page/香港特別行政區行政長官辦公室.md "wikilink")[常任秘書長](../Page/常任秘書長.md "wikilink")

### 學術界

  - [唐健垣](../Page/唐健垣.md "wikilink")：前[香港中文大學](../Page/香港中文大學.md "wikilink")、[美國](../Page/美國.md "wikilink")[康乃狄克州威士連大學](../Page/康乃狄克州.md "wikilink")、[密芝根大學](../Page/密芝根大學.md "wikilink")、[香港演藝學院教授](../Page/香港演藝學院.md "wikilink")、[古琴家](../Page/古琴.md "wikilink")、[古箏](../Page/古箏.md "wikilink")[演奏家](../Page/演奏家.md "wikilink")、[南音和](../Page/南音.md "wikilink")[甲骨文研究專家](../Page/甲骨文.md "wikilink")，[茶藝和](../Page/茶藝.md "wikilink")[園林專家](../Page/園林.md "wikilink")\[29\]
  - [霍偉棟](../Page/霍偉棟.md "wikilink")：[香港大學工程學院助理院長](../Page/香港大學.md "wikilink")
  - [榮潤國](../Page/榮潤國.md "wikilink")：[香港中文大學醫學院精神科學系教授](../Page/香港中文大學.md "wikilink")
  - [趙志成](../Page/趙志成.md "wikilink")：[香港中文大學教育學院教授](../Page/香港中文大學.md "wikilink")
  - [李鉅威](../Page/李鉅威.md "wikilink")：[香港城市大學商學院經濟及金融系副教授](../Page/香港城市大學.md "wikilink")、經濟學者
  - [梁天偉](../Page/梁天偉.md "wikilink")：[香港樹仁大學新聞與傳播學系主任](../Page/香港樹仁大學.md "wikilink")、資深[傳媒工作者](../Page/傳媒工作者.md "wikilink")
  - [馮浩恩](../Page/馮浩恩.md "wikilink")：[現代教育補習老師](../Page/現代教育.md "wikilink")
  - [梁建勳](../Page/梁建勳.md "wikilink")（Keith
    Leung）：前[遵理學校補習老師](../Page/遵理學校.md "wikilink")
  - [葉健民](../Page/葉健民.md "wikilink")：[香港城市大學公共及社會行政學系教授](../Page/香港城市大學.md "wikilink")
  - [黄君保](../Page/黄君保.md "wikilink")：[心光學校校長](../Page/心光學校.md "wikilink")、[心光機構副院長](../Page/心光機構.md "wikilink")
  - [麥志豪](../Page/麥志豪.md "wikilink")：[聖保羅書院小學校長](../Page/聖保羅書院小學.md "wikilink")
  - [趙文宗](../Page/趙文宗.md "wikilink")：[香港樹仁大學法律與商業學系系主任](../Page/香港樹仁大學.md "wikilink")、資深[法律學者及作家](../Page/法律學者及作家.md "wikilink")
  - [李揚真](../Page/李揚真.md "wikilink")：[中華基督教銘基書院校長](../Page/中華基督教銘基書院.md "wikilink")
  - [陳漢鏵](../Page/陳漢鏵.md "wikilink")：心臟科專科醫生

### 演藝、體育及文化界

  - [卓亦謙](../Page/卓亦謙.md "wikilink")（Nick
    Cheuk）：[香港電影編劇](../Page/香港電影.md "wikilink")、[第37屆香港電影金像獎創作總監](../Page/第37屆香港電影金像獎.md "wikilink")
  - [鍾鎮濤](../Page/鍾鎮濤.md "wikilink")：歌手
  - [傅家俊](../Page/傅家俊.md "wikilink")：桌球運動員\[30\]
  - [劉祖德](../Page/劉祖德.md "wikilink")：[音樂人](../Page/音樂人.md "wikilink")、和聲、歌唱老師
  - [曾奕文](../Page/曾奕文.md "wikilink")（Edmond
    Tsang）：[作曲家](../Page/作曲家.md "wikilink")
  - [許創基](../Page/許創基.md "wikilink")：[音樂人](../Page/音樂人.md "wikilink")
  - [詹德隆](../Page/詹德隆.md "wikilink")：[專欄作家](../Page/專欄作家.md "wikilink")、學者、文化及政治評論員、1980年[香港十大傑出青年](../Page/香港十大傑出青年.md "wikilink")
  - [陳浩民](../Page/陳浩民.md "wikilink")：[演員](../Page/演員.md "wikilink")
  - [阮兆祥](../Page/阮兆祥.md "wikilink")：[無綫電視](../Page/無綫電視.md "wikilink")[藝員、](../Page/藝員、.md "wikilink")[商業電台節目主持人](../Page/香港商業電台.md "wikilink")
  - [歐錦棠](../Page/歐錦棠.md "wikilink")：[演員](../Page/演員.md "wikilink")
  - [李潤庭](../Page/李潤庭.md "wikilink")：[無綫電視](../Page/無綫電視.md "wikilink")[主播](../Page/主播.md "wikilink")
  - [章志文](../Page/章志文.md "wikilink")：[無綫電視](../Page/無綫電視.md "wikilink")[藝員](../Page/藝員.md "wikilink")
  - [陳學鳴](../Page/陳學鳴.md "wikilink")（司徒夾帶）：跨[媒體創作人](../Page/媒體.md "wikilink")
  - [馮翰銘](../Page/馮翰銘.md "wikilink")（Alex Fung）：唱作歌手、音樂人（小學部）
  - [蕭樹勝](../Page/蕭樹勝.md "wikilink")（Jimmy
    Siu）：[香港電台第四台](../Page/香港電台.md "wikilink")（古典音樂台）總監（曾為高主教書院中學部銀樂隊團長）
  - [蘇炫閣](../Page/蘇炫閣.md "wikilink")（William So）：攝影師
  - [莫旭秋](../Page/莫旭秋.md "wikilink")：歌手
  - [黎榮](../Page/黎榮.md "wikilink")（Ban Lai）：公共運輸事業人士
  - [蘇永康](../Page/蘇永康.md "wikilink")：歌手（小學部）

### 商界

  - [張永霖](../Page/張永霖.md "wikilink")：[香港中文大學兼任教授](../Page/香港中文大學.md "wikilink")、[中國聯通](../Page/中國聯通.md "wikilink")（香港）有限公司獨立[非執行董事](../Page/非執行董事.md "wikilink")、前[亞洲電視執行主席](../Page/亞洲電視.md "wikilink")，曾任職電訊盈科及國泰航空，曾直升為電盈副主席。
  - [朱國樑](../Page/朱國樑.md "wikilink")：[太古集團](../Page/太古集團.md "wikilink")（中國）主席、[國泰航空非常務董事兼前](../Page/國泰航空.md "wikilink")[行政總裁](../Page/行政總裁.md "wikilink")
  - [徐建東](../Page/徐建東.md "wikilink")（Tony
    Tsui）：[和記黃埔地產副](../Page/和記黃埔地產.md "wikilink")[董事總經理](../Page/董事總經理.md "wikilink")、[和記港陸董事總經理](../Page/和記港陸.md "wikilink")
  - [繆家濤](../Page/繆家濤.md "wikilink")：[國際獅子會中國港澳三零三區前主席](../Page/國際獅子會.md "wikilink")、香港鑪峯獅子會前主席、香港品牌發展局成員
  - [陳志強](../Page/陳志強.md "wikilink")：[康宏金融集團創辦人](../Page/康宏金融集團.md "wikilink")
  - [陳柏熹](../Page/陳柏熹_\(慕詩集團\).md "wikilink")：[慕詩國際創作總監](../Page/慕詩國際.md "wikilink")
  - [余俊敏](../Page/余俊敏.md "wikilink")：[興業太陽能財務總監兼公司祕書](../Page/興業太陽能.md "wikilink")

### 網絡名人

  - [盧廷彬](../Page/盧廷彬.md "wikilink")：著名社交網絡足球發文者,有強心針針哥之稱,以[國際米蘭球迷身份活躍並著名於社交網絡平台](../Page/國際米蘭.md "wikilink")

## 參看

  - [香港中學列表](../Page/香港中學列表.md "wikilink")

## 註釋

## 參考資料

## 外部連結

  - [高主教書院](http://www.raimondi.edu.hk/)
  - [高主教書院Facebook專頁](https://www.facebook.com/raimondicollege/)
  - [高主教書院小學部](http://rcps.raimondi.edu.hk/)
  - [高主教書院幼稚園部](http://www.rckg.edu.hk/)
  - [高主教書院舊生會](http://www.raa.com.hk/)
  - [高主教書院校友會（多倫多）](http://www.raimondicollege.ca/)
  - [Libre - 第四十五屆高主教書院學生會](http://www.facebook.com/libre45th)
  - [Vector - 第四十六屆高主教書院學生會](http://www.facebook.com/rcvector1415)
  - [ENOXA - 第四十七屆高主教書院學生會](https://www.facebook.com/RCENOXA?fref=ts)
  - [FERO - 第四十八屆高主教書院學生會](https://www.facebook.com/rcfero/)
  - [高主教書院港島第99旅童軍團](https://www.facebook.com/99hkgst)
  - [主教座堂堂區屬校宣傳影片:高主教書院](https://www.facebook.com/catholiccathedralhk/videos/vb.710108079107052/828891167228742/?type=2&theater)

[Category:中環](../Category/中環.md "wikilink")
[Category:半山區](../Category/半山區.md "wikilink")
[R](../Category/中西區中學.md "wikilink")
[Category:灣仔區小學](../Category/灣仔區小學.md "wikilink")
[S](../Category/香港幼稚園.md "wikilink")
[Category:香港天主教學校](../Category/香港天主教學校.md "wikilink")
[Category:1958年創建的教育機構](../Category/1958年創建的教育機構.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")

1.

2.

3.
4.

5.

6.

7.

8.

9.
10.

11.
12.
13.
14.

15. 津校展宏圖 與直資比高，[星島日報](../Page/星島日報.md "wikilink") 2013年9月2日

16.

17. [山頂高峰跑2015](http://fitz.hk/sports/running/山頂高峰跑2015/)

18. [高主教書院中小幼合辦音樂會](https://archive.is/20150905111133/http://kkp.org.hk/node/7963)

19.

20.

21. [年宵花開好諷時弊產品熱賣](http://www.epochtimes.com.hk/b5/15/2/16/204240.htm?p=all)

22. [Libre- Raimondi College 45th Student
    Union](https://www.facebook.com/libre45th)

23. [Vector - 46th Raimondi College Student
    Union](https://www.facebook.com/rcvector1415)

24. [ENOXA - 47th Raimondi College Student
    Union](https://www.facebook.com/RCENOXA?fref=ts)

25. [FERO - 48th Raimondi College Student
    Union](https://www.facebook.com/rcfero/)

26. [VIGOR - 50th Raimondi College Student
    Union](https://facebook.com/rcvigor1819/)

27.

28.

29.

30.