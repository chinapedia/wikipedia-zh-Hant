**高俊強**（1982年2月25日－）為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，前[中華職棒](../Page/中華職棒.md "wikilink")[中信鯨隊](../Page/中信鯨.md "wikilink")，守備位置為[三壘手](../Page/三壘手.md "wikilink")，原名為**高曉強**。青棒時代為投手，與[林岳平](../Page/林岳平.md "wikilink")、[張億得](../Page/張億得.md "wikilink")、[黃俊中合稱高雄市三民高中的](../Page/黃俊中.md "wikilink")「三民四劍客」，加入台電隊後專任野手。

因涉入[2009年中華職棒假球事件在當年](../Page/2009年中華職棒假球事件.md "wikilink")12月遭板橋地檢署收押禁見，棒球協會除名。

## 經歷

  - [高雄市復興國小少棒隊](../Page/高雄市.md "wikilink")
  - [高雄市立五福國民中學青少棒隊](../Page/高雄市立五福國民中學.md "wikilink")
  - [高雄市三民高中青棒隊](../Page/高雄市.md "wikilink")
  - 台灣電力棒球隊
  - [中信鯨隊代訓球員](../Page/中信鯨.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[中信鯨隊](../Page/中信鯨.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                               | 出賽  | 打數  | 安打  | 全壘打 | 打點  | 盜壘 | 四死  | 三振  | 壘打數 | 雙殺打 | 打擊率   |
| ----- | -------------------------------- | --- | --- | --- | --- | --- | -- | --- | --- | --- | --- | ----- |
| 2006年 | [中信鯨](../Page/中信鯨.md "wikilink") | 69  | 220 | 52  | 5   | 32  | 1  | 25  | 42  | 76  | 4   | 0.236 |
| 2007年 | [中信鯨](../Page/中信鯨.md "wikilink") | 98  | 342 | 103 | 7   | 55  | 19 | 61  | 56  | 149 | 5   | 0.301 |
| 2008年 | [中信鯨](../Page/中信鯨.md "wikilink") | 41  | 149 | 39  | 2   | 17  | 7  | 19  | 27  | 55  | 2   | 0.262 |
| 合計    | 3年                               | 208 | 711 | 194 | 14  | 104 | 27 | 105 | 125 | 280 | 11  | 0.273 |

  - 成績累績至2008年10月10日

## 個人年表

  - 1999年 --
    青棒時期號稱強打少年，與[林岳平](../Page/林岳平.md "wikilink")、[張億得](../Page/張億得.md "wikilink")、[黃俊中合稱](../Page/黃俊中.md "wikilink")「三民四劍客」。
  - 2005年 --
    中華職棒16年代訓賽四冠王(打擊率、全壘打、打點、安打)，以250萬台幣簽約金加盟[中信鯨](../Page/中信鯨.md "wikilink")。
  - 2008年 --
    季中下放至中信二軍至球季結束，[中信鯨解散後經特別選秀會無球隊選進離開職棒圈](../Page/中信鯨.md "wikilink")。
  - 2009年 --
    涉及[職棒簽賭案請回並協助調查](../Page/職棒簽賭案.md "wikilink")，後再度調查遭收押禁見結束棒球生涯。

## 外部連結

[K](../Category/高姓.md "wikilink") [K](../Category/在世人物.md "wikilink")
[K](../Category/1982年出生.md "wikilink")
[K](../Category/台灣棒球選手.md "wikilink")
[K](../Category/中信鯨隊球員.md "wikilink")
[K](../Category/中華少棒隊球員.md "wikilink")
[K](../Category/高雄市人.md "wikilink")
[Category:中華職棒終身禁賽名單](../Category/中華職棒終身禁賽名單.md "wikilink")