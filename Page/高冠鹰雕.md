**高冠鹰雕**（[學名](../Page/學名.md "wikilink")：*Lophaetus
occipitalis*）是[鷹科的一種](../Page/鷹科.md "wikilink")[猛禽](../Page/猛禽.md "wikilink")，過去曾歸入[鷹雕屬](../Page/鷹雕屬.md "wikilink")，目前一般單獨歸為**高冠鷹雕屬**。

高冠鹰雕是一種偏大的[猛禽](../Page/猛禽.md "wikilink")，一般身長55[公分](../Page/公分.md "wikilink")，分佈於[撒哈拉沙漠](../Page/撒哈拉沙漠.md "wikilink")。牠們一般棲息於[森林](../Page/森林.md "wikilink")、森林的邊緣和[農園](../Page/農園.md "wikilink")。

## 參考資料

[Category:鵟亞科](../Category/鵟亞科.md "wikilink")