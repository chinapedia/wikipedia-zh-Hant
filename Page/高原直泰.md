**高原直泰**，[日本足球运动员](../Page/日本.md "wikilink")，司职[前锋](../Page/前锋.md "wikilink")。

高原曾先后效力于[磐田喜悦](../Page/磐田喜悦足球俱乐部.md "wikilink")、[博卡青年和](../Page/博卡青年足球俱乐部.md "wikilink")[汉堡](../Page/汉堡足球俱乐部.md "wikilink")，在2006年以75萬[歐元的轉會費轉投](../Page/歐元.md "wikilink")[法兰克福](../Page/法兰克福足球俱乐部.md "wikilink")。

2008年宣告返回[浦和紅鑽](../Page/浦和紅鑽.md "wikilink")。2010年7月高原直泰正式加盟韩国[K联赛球队](../Page/K联赛.md "wikilink")[水原三星藍翼](../Page/水原三星藍翼.md "wikilink")。

2011年高原直泰再次回国並加盟[清水心跳](../Page/清水心跳.md "wikilink")。

## 外部連結

  - [TAKA-WEB 高原直泰個人網站](http://www.shizuokaonline.com/taka/)
  - [高原直泰](https://web.archive.org/web/20061205030417/http://takahara.de/)

[Category:日本足球運動員](../Category/日本足球運動員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:日職球員](../Category/日職球員.md "wikilink")
[Category:磐田喜悦球員](../Category/磐田喜悦球員.md "wikilink")
[Category:小保加球員](../Category/小保加球員.md "wikilink")
[Category:漢堡球員](../Category/漢堡球員.md "wikilink")
[Category:法蘭克福球員](../Category/法蘭克福球員.md "wikilink")
[Category:浦和红宝石球员](../Category/浦和红宝石球员.md "wikilink")
[Category:水原三星蓝翼球员](../Category/水原三星蓝翼球员.md "wikilink")
[Category:清水心跳球員](../Category/清水心跳球員.md "wikilink")
[Category:东京日视1969球员](../Category/东京日视1969球员.md "wikilink")
[Category:沖繩體育會球員](../Category/沖繩體育會球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:靜岡縣出身人物](../Category/靜岡縣出身人物.md "wikilink")
[Category:日本旅外足球運動員](../Category/日本旅外足球運動員.md "wikilink")
[Category:日本國家足球隊成員](../Category/日本國家足球隊成員.md "wikilink")
[Category:2000年夏季奧林匹克運動會足球運動員](../Category/2000年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:2000年亞洲盃足球賽球員](../Category/2000年亞洲盃足球賽球員.md "wikilink")
[Category:2003年洲際國家盃球員](../Category/2003年洲際國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2007年亞洲盃足球賽球員](../Category/2007年亞洲盃足球賽球員.md "wikilink")
[Category:亞洲盃足球賽冠軍隊球員](../Category/亞洲盃足球賽冠軍隊球員.md "wikilink")