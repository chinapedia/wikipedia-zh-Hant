**高孟定**（），生於[雲林縣](../Page/雲林縣.md "wikilink")，臺灣政治人物。曾任[立法委員](../Page/立法委員.md "wikilink")、[國民大會代表](../Page/國民大會.md "wikilink")。現為[逢甲大學都市計畫與空間資訊學系副教授](../Page/逢甲大學.md "wikilink")，為[雲林張派成員](../Page/台灣地方派系_#雲林縣.md "wikilink")，在[雲林縣長](../Page/雲林縣長.md "wikilink")[張榮味操盤下](../Page/張榮味.md "wikilink")，當選[第5屆中華民國立法委員](../Page/第5屆中華民國立法委員名單.md "wikilink")。

## 經歷

1996年，高孟定獲得[台灣綠黨提名在](../Page/台灣綠黨.md "wikilink")[雲林縣參加](../Page/雲林縣.md "wikilink")[第三屆國民大會代表選舉並當選](../Page/1996年國民大會代表選舉.md "wikilink")，成為台灣綠黨唯一的政治代言人。

1997年5月，高孟定退出台灣綠黨，並與[張榮味搭擋競選雲林縣正副縣長](../Page/張榮味.md "wikilink")。1997年5月16日，台灣綠黨宣佈將召開中央執評委會聯席會議，決議開除高孟定黨籍\[1\]。

2001年，獲得[張榮味推薦的](../Page/張榮味.md "wikilink")[高孟定以](../Page/高孟定.md "wikilink")[無黨籍身份參加](../Page/無黨籍.md "wikilink")[雲林縣](../Page/雲林縣.md "wikilink")[第5屆中華民國立法委員](../Page/第5屆中華民國立法委員名單.md "wikilink")\[2\]，當選。

2004年，高孟定加入[民進黨](../Page/民進黨.md "wikilink")。2004年6月，高孟定以個人從政生涯規劃為由，宣佈放棄接受民進黨徵召參加[2004年立法委員選舉](../Page/2004年中華民國立法委員選舉.md "wikilink")。2004年6月27日，高孟定否認自己是當時[中華民國總統](../Page/中華民國總統.md "wikilink")[陳水扁欽定的雲林縣代理縣長人選](../Page/陳水扁.md "wikilink")，並稱陳水扁、[行政院與民進黨中央黨部皆未與他洽談雲林縣代理縣長人選問題](../Page/行政院.md "wikilink")\[3\]。

2007年11月15日，雲林縣[-{斗}-六市市長](../Page/斗六市.md "wikilink")[張和平因涉及貪瀆案被](../Page/張和平.md "wikilink")[起訴而辭職](../Page/起訴.md "wikilink")。高孟定獲得民進黨提名參加-{斗}-六市長補選\[4\]，但未當選。

2009年8月30日，高孟定出席國民黨[雲林縣第二選區立法委員補選候選人](../Page/2009年雲林縣第二選區立法委員補選.md "wikilink")[張艮輝](../Page/張艮輝.md "wikilink")-{斗}-六區競選總部成立大會，批評民進黨「向下沉淪」、提名「一個只想為自己爭一口氣的人選」[劉建國參選](../Page/劉建國.md "wikilink")、犧牲「博士人才」[李應元](../Page/李應元.md "wikilink")，所以他對民進黨已經沒有期待，他不怕被民進黨開除黨籍，所以他公開支持張艮輝以讓雲林縣「向上提升」。\[5\]
民進黨雲林縣黨部主任委員[許根尉反批](../Page/許根尉.md "wikilink")，高孟定鄙視劉建國的[學歷](../Page/學歷.md "wikilink")，雲林縣民應該體會「學歷不代表能力，雲林縣要的是為民服務的立委」。\[6\]

## 政治

### 2001年立法委員選舉

| 2001年雲林縣選舉區立法委員選舉結果 |
| ------------------- |
| 應選6席                |
| 號次                  |
| 1                   |
| 2                   |
| 3                   |
| 4                   |
| 5                   |
| 6                   |
| 7                   |
| 8                   |
| 9                   |
| 10                  |
| 11                  |
| 12                  |

## 資料來源

<references/>

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00096&stage=5)
  - [高孟定的部落格](http://tw.myblog.yahoo.com/wwwgmtm/)

[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:雲林縣副縣長](../Category/雲林縣副縣長.md "wikilink")
[Category:第3屆中華民國國民大會代表](../Category/第3屆中華民國國民大會代表.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:前台灣綠黨黨員](../Category/前台灣綠黨黨員.md "wikilink")
[Category:逢甲大學教授](../Category/逢甲大學教授.md "wikilink")
[Category:國立臺灣大學工學院校友](../Category/國立臺灣大學工學院校友.md "wikilink")
[Category:逢甲大學校友](../Category/逢甲大學校友.md "wikilink")
[Category:臺灣省立斗六高級中學校友](../Category/臺灣省立斗六高級中學校友.md "wikilink")
[Category:雲林人](../Category/雲林人.md "wikilink")
[Meng孟](../Category/高姓.md "wikilink")

1.  台灣綠黨新聞稿，[〈綠黨壯士斷腕放棄唯一國代
    針對高孟定自行脫黨參選之公開說明〉](http://www.greenparty.org.tw/history.php?itemid=247)，台灣綠黨，1997年5月16日
2.  [〈90大選／雲林立委戰情緊繃　高孟定扮演黑馬〉](http://www.nownews.com/2001/07/13/1087-459476.htm)
3.  陳正芬
    -{斗}-南報導，[〈雲林／傳欽定代理縣長　高孟定消毒〉](http://www.nownews.com/2004/06/27/124-1650287.htm)，《東森新聞報》，2004年6月27日
4.  葉子綱
    雲林縣2008年1月18日電，〈-{斗}-六市長補選26日投票　綠營高孟定誓師〉，[中央社](../Page/中央社.md "wikilink")，2008年1月18日
5.  許麗秀 -{斗}-六報導，〈張艮輝-{斗}-六總部成立
    高孟定站台〉，《[民眾日報](../Page/民眾日報.md "wikilink")》，2009年8月31日
6.  段鴻裕 -{斗}-六報導，[〈前縣長張榮味
    力挺張艮輝〉](http://www.udn.com/2009/8/31/NEWS/DOMESTIC/DOM5/5107941.shtml)
    ，《[聯合報](../Page/聯合報.md "wikilink")》，2009年8月31日