**高安路第一小学**，简称**高一小学**，位于中国[上海市](../Page/上海市.md "wikilink")[徐汇区](../Page/徐汇区.md "wikilink")，地处文化艺术界人士居住较为集中的地段，是上海市规模较大的一所小学。创建于1955年，是全日制公办小学，学校现有康平（[高安路与](../Page/高安路_\(上海\).md "wikilink")[康平路路口](../Page/康平路_\(上海\).md "wikilink")）、宛平（[宛平南路与](../Page/宛平南路.md "wikilink")[衡山路路口](../Page/衡山路.md "wikilink")）两个校区，占地面积共7634平方米、建筑面积7046平方米，目前有1800多名学生，44个班级。100多位教职员工。高安路第一小学的校匾是由已故[上海市市长](../Page/上海市市长.md "wikilink")、[海协会会长](../Page/海协会.md "wikilink")[汪道涵先生所题写的](../Page/汪道涵.md "wikilink")。

学校建有弦乐队、舞蹈团、篮球队、合唱团、儿童文学社等学生社团，使学生在校的每一天，都拥有自信、拥有快乐，能拥有本领，发挥特长，使身心得到健康成长。

中国国家副主席[李源潮](../Page/李源潮.md "wikilink")、排球名宿[沈富麟](../Page/沈富麟.md "wikilink")、NBA球星[姚明](../Page/姚明.md "wikilink")\[1\]\[2\]、演艺圈艺人[黄圣依](../Page/黄圣依.md "wikilink")、[柯蓝皆为学校校友](../Page/柯蓝.md "wikilink")。

## 外部链接

  - [高安路第一小学](http://g1xx.xhedu.sh.cn/)
  - [《新闻晚报》](http://sports.163.com/04/1014/15/12LOG3IM00051CA1.html)
  - [《新民晚报》](http://xmwb.news365.com.cn/ls/200701/t20070118_1258515.htm)
  - [《新快报》](http://ent.people.com.cn/GB/1082/3295866.html)
  - [中国篮球协会官方网站](http://basketball.sport.org.cn/Database/Knowledge/2005-01-19/5891.html)

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[Category:上海小学](../Category/上海小学.md "wikilink")
[Category:徐汇区教育](../Category/徐汇区教育.md "wikilink")
[Category:1955年中国建立](../Category/1955年中国建立.md "wikilink")
[Category:1955年创建的教育机构](../Category/1955年创建的教育机构.md "wikilink")

1.  [姚明母校为其助威季后赛
    学校小篮球队以姚明命](http://news.rednet.cn/c/2004/04/20/552117.htm)
2.  [姚明两度回到高安路第一小学侧记](http://topic.news365.com.cn/nbazgsxgxw_2526/200410/t20041014_251513.htm)