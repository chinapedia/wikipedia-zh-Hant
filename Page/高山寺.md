**高山寺**（）是一座位于[日本](../Page/日本.md "wikilink")[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[右京區梅ヶ畑栂尾](../Page/右京區.md "wikilink")（）町的寺院。栂尾位于京都市區西北部的山中。是[真言宗系的單立寺廟](../Page/真言宗.md "wikilink")。建于[奈良時代](../Page/奈良時代.md "wikilink")，但實際的開基著（創立者）為[鎌倉時代的](../Page/鎌倉時代.md "wikilink")[明惠](../Page/明惠.md "wikilink")。其中以「[鳥獸戲畫](../Page/鳥獸戲畫.md "wikilink")」聞名，同時寺廟內還有大量的繪畫、典籍、文書收藏，擁有眾多的珍貴文物。以[古都京都的文化財的一部份列入](../Page/古都京都的文化財.md "wikilink")[世界遺產名單中](../Page/世界遺產.md "wikilink")。

## 伽藍

<File:Kozanji> Kyoto Kyoto09s5s4592.jpg|金堂 <File:Sekisuiin> Kozanji
Kyoto Kyoto06s5s4350.jpg|石水院（日本的[國寶](../Page/國寶.md "wikilink")）
<File:Kozanji> Kyoto
Kyoto14s5s4500.jpg|開山堂（[重要文化財](../Page/重要文化財.md "wikilink")）

## 關聯條目

  - [古都京都的文化財](../Page/古都京都的文化財.md "wikilink")
  - [日本寺院列表](../Page/日本寺院列表.md "wikilink")

## 參考文獻

  -
  -
  -
  -
  -
  -
## 外部連結

  - [高山寺](http://www.kosanji.com/)

[Category:日本世界遺產](../Category/日本世界遺產.md "wikilink")
[Category:京都市佛寺](../Category/京都市佛寺.md "wikilink")
[Category:右京區](../Category/右京區.md "wikilink")
[Category:真言宗寺院](../Category/真言宗寺院.md "wikilink")