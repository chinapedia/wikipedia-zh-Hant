**高島鞆之助**（1844年12月18日－1916年1月11日），[日本](../Page/日本.md "wikilink")[鹿兒島人](../Page/鹿兒島.md "wikilink")，[日本陸軍](../Page/日本陸軍.md "wikilink")[將領](../Page/將領.md "wikilink")、政治人物，

係[薩摩](../Page/薩摩.md "wikilink")[藩士](../Page/藩士.md "wikilink")[高島嘉兵衛的四子](../Page/高島嘉兵衛.md "wikilink")，[名為](../Page/名字.md "wikilink")**昭光**。他官至日本[陸軍](../Page/陸軍.md "wikilink")[中將](../Page/中將.md "wikilink")[正二位](../Page/正二位.md "wikilink")[勳一等](../Page/勳一等.md "wikilink")[子爵](../Page/子爵.md "wikilink")；歷任[陸軍大臣](../Page/陸軍大臣.md "wikilink")、[拓殖大臣](../Page/拓務省.md "wikilink")、[樞密院顧問官等](../Page/樞密院_\(日本\).md "wikilink")，他也是[台灣日治時期](../Page/台灣日治時期.md "wikilink")50年裡，唯一的[台灣副總督](../Page/台灣副總督.md "wikilink")。

## 生平

高島鞆之助為[薩摩藩貴族出身](../Page/薩摩藩.md "wikilink")。早年從軍，於1874年晉升至陸軍[大佐](../Page/大佐.md "wikilink")。後於日本[西南戰爭嶄露頭角](../Page/西南戰爭.md "wikilink")，從此於日本陸軍占有一席之地。1883年（明治16年）晉升陸軍中將
，1884年：封[子爵](../Page/子爵.md "wikilink")，1891年（明治24年）：[第1次松方内閣的陸軍大臣](../Page/第1次松方内閣.md "wikilink")，1895年，在[乙未戰爭中](../Page/乙未戰爭.md "wikilink")，擔任[台灣副總督](../Page/台灣副總督.md "wikilink")，是[台灣日治時期唯一一位副總督](../Page/台灣日治時期.md "wikilink")。

返日後，他再擔任[拓殖大臣與陸軍大臣等](../Page/拓務省.md "wikilink")[內閣](../Page/內閣.md "wikilink")[閣員](../Page/閣員.md "wikilink")。另外，[追手門学院前身的](../Page/追手門学院.md "wikilink")[大阪偕行社学院也是他所創立](../Page/大阪偕行社学院.md "wikilink")，而[中日戰爭中的](../Page/中日戰爭.md "wikilink")[高島友武為他養嗣子](../Page/高島友武.md "wikilink")。

## 經歷

  - 早年曾在[薩摩藩的](../Page/薩摩藩.md "wikilink")[藩校](../Page/藩校.md "wikilink")([造士館](../Page/造士館.md "wikilink"))學習，後於[戊辰戰爭](../Page/戊辰戰爭.md "wikilink")(1868年－1869年)時從軍。
  - [明治](../Page/明治.md "wikilink")7年（1874年）：被任命[陸軍](../Page/陸軍.md "wikilink")[大佐](../Page/大佐.md "wikilink")。
  - [明治](../Page/明治.md "wikilink")10年（1877年）：[西南戰爭時期](../Page/西南戰爭.md "wikilink")，擔任別働第1旅團司令長官([旅長](../Page/旅.md "wikilink"))。
  - 明治16年（1883年）：陸軍中將
  - 明治17年（1884年）7月7日：敘封[子爵](../Page/子爵.md "wikilink")。
  - 明治20年（1887年）11月2日：被授予[勲一等](../Page/勲一等.md "wikilink")[旭日大綬章](../Page/旭日大綬章.md "wikilink")。
  - 明治21年（1888年）：擔任[大阪鎮台](../Page/大阪鎮台.md "wikilink")[司令官](../Page/司令官.md "wikilink")([師長](../Page/師_\(軍事\).md "wikilink"))，在此期間創設了[大阪偕行社附屬小學](../Page/大阪偕行社附屬小學.md "wikilink")(即現在的[追手門學院小學](../Page/追手門學院小學.md "wikilink")）。
  - 明治24年（1891年）：被任命為[第1次松方内閣的陸軍大臣](../Page/第1次松方内閣.md "wikilink")。
  - 明治25年（1892年）：被任命為[樞密院顧問官](../Page/樞密院_\(日本\).md "wikilink")。
  - 明治28年（1895年）：擔任[台灣副總督](../Page/台灣副總督.md "wikilink")。
  - 明治29年（1896年）：在[第2次伊藤内閣和](../Page/第2次伊藤内閣.md "wikilink")[第2次松方内閣中分別擔任拓殖務大臣與陸軍大臣](../Page/第2次松方内閣.md "wikilink")。
  - 明治32年（1899年）：再度被任命為[樞密院顧問官](../Page/樞密院_\(日本\).md "wikilink")（直至逝世）。
  - [大正](../Page/大正.md "wikilink")5年（1916年）1月11日：逝世，享年71歲，被贈予

[勳一等](../Page/勳一等.md "wikilink")[旭日桐花大綬章](../Page/桐花章.md "wikilink")。

## 參考資料

  - [秦郁彦編](../Page/秦郁彦.md "wikilink")、『日本陸海軍総合事典』，[東京大學出版會](../Page/東京大學.md "wikilink")(1991年)。
  - 熊本兵團戰史編纂委員會、『[熊本兵団戦史](../Page/熊本.md "wikilink")　\[\[九一八事變|

満州事変\]\]以前編』，[熊本日日新聞社](../Page/熊本日日新聞.md "wikilink")(1965年)。      　

[Category:1844年出生](../Category/1844年出生.md "wikilink")
[Category:1916年逝世](../Category/1916年逝世.md "wikilink")
[Category:台灣日治時期政治人物](../Category/台灣日治時期政治人物.md "wikilink")
[Category:日本陸軍軍人](../Category/日本陸軍軍人.md "wikilink")
[Category:鹿兒島縣出身人物](../Category/鹿兒島縣出身人物.md "wikilink")
[Category:勳一等旭日大綬章獲得者](../Category/勳一等旭日大綬章獲得者.md "wikilink")
[Category:日本樞密顧問官](../Category/日本樞密顧問官.md "wikilink")