**高市郡**（）為[奈良縣屬下的](../Page/奈良縣.md "wikilink")[郡](../Page/郡.md "wikilink")。人口14,624人、面積49.85
km²。（2003年）

这里是日本第一个信佛教的地区。 高市郡包含以下一個町及一村。

  - [高取町](../Page/高取町.md "wikilink")（たかとりちょう）
  - [明日香村](../Page/明日香村.md "wikilink")（あすかむら）

## 沿革

1889年4月1日町村制施行以來沿革

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1945年</p></th>
<th><p>1945年 - 1955年</p></th>
<th><p>1956年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>八木町</p></td>
<td><p>八木町</p></td>
<td><p>八木町</p></td>
<td><p>1956年2月11日<br />
<a href="../Page/橿原市.md" title="wikilink">橿原市</a></p></td>
<td><p>橿原市</p></td>
<td><p>橿原市</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>今井町</p></td>
<td><p>今井町</p></td>
<td><p>今井町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>白橿村</p></td>
<td><p>1928年2月11日<br />
町制改稱<br />
畝傍町</p></td>
<td><p>畝傍町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>鴨公村</p></td>
<td><p>鴨公村</p></td>
<td><p>鴨公村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>真菅村</p></td>
<td><p>真菅村</p></td>
<td><p>真菅村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>金橋村</p></td>
<td><p>金橋村</p></td>
<td><p>金橋村</p></td>
<td><p>1956年7月3日<br />
被併入橿原市</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>新澤村</p></td>
<td><p>新澤村</p></td>
<td><p>新澤村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>高取村</p></td>
<td><p>1882年6月3日<br />
町制</p></td>
<td><p>1954年10月1日<br />
<a href="../Page/高取町.md" title="wikilink">高取町</a></p></td>
<td><p>高取町</p></td>
<td><p>高取町</p></td>
<td><p>高取町</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>船倉村</p></td>
<td><p>船倉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>越智岡村</p></td>
<td><p>越智岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>阪合村</p></td>
<td><p>阪合村</p></td>
<td><p>阪合村</p></td>
<td><p>1956年7月3日<br />
<a href="../Page/明日香村.md" title="wikilink">明日香村</a></p></td>
<td><p>明日香村</p></td>
<td><p>明日香村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>高市村</p></td>
<td><p>高市村</p></td>
<td><p>高市村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>飛鳥村</p></td>
<td><p>飛鳥村</p></td>
<td><p>飛鳥村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>天滿村</p></td>
<td><p>天滿村</p></td>
<td><p>天滿村</p></td>
<td><p>1957年3月1日<br />
被併入<a href="../Page/大和高田市.md" title="wikilink">大和高田市</a></p></td>
<td></td>
<td><p>大和高田市</p></td>
</tr>
</tbody>
</table>

## 和名抄有記載的高市郡郷名

  - 巨勢
  - [波多](../Page/波多_\(高市郡\).md "wikilink")
  - 遊部
  - 檜前（比乃久末）
  - 久米
  - 雲梯
  - 賀美