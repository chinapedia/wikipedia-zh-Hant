**高師謙**（英文名：**Nicholas Kao Se Tseien**,
O.C.S.D.，），嚴規[熙篤會會士](../Page/熙篤會.md "wikilink")，也是至目前為止最長壽的[華人](../Page/華人.md "wikilink")[天主教](../Page/天主教.md "wikilink")[神父](../Page/神父.md "wikilink")。2007年時，他渡過了他一百一十歲的生日，並同時成為了[超級人瑞](../Page/超級人瑞.md "wikilink")。2007年10月13日之後，因為[日本人](../Page/日本人.md "wikilink")[奥村儀一的逝去](../Page/奥村儀一.md "wikilink")，他成了[亞洲第二老以及世界上第七老的男性](../Page/亞洲.md "wikilink")。

出生於[福建省](../Page/福建省.md "wikilink")[福州](../Page/福州.md "wikilink")[長樂市](../Page/長樂市.md "wikilink")\[1\]
，高是四位兄弟之一，[廈門大學法律系畢業](../Page/廈門大學.md "wikilink")，在18歲時轉入[西班牙](../Page/西班牙.md "wikilink")[多明我會的](../Page/多明我會.md "wikilink")[天主教學校](../Page/天主教.md "wikilink")。雖然被訓練如一位老師而且在晚上學習法律，他決定在他親愛朋友的死亡之後成為一位神父，一個西班牙[修道士](../Page/修道士.md "wikilink")。他在1915年領洗聖名為「尼各老」\[2\]，1933年晉鐸為神父。[第二次国共内战後](../Page/第二次国共内战.md "wikilink")，隨著[國民政府來台從事宣教工作](../Page/國民政府.md "wikilink")，直到74歲才離開[台灣教區](../Page/台灣教區.md "wikilink")。

他的神父生活從[中國大陸到](../Page/中國大陸.md "wikilink")[台灣](../Page/台灣.md "wikilink")、[馬來西亞和最後](../Page/馬來西亞.md "wikilink")1972年到達的[香港](../Page/香港.md "wikilink")。他在[大嶼山](../Page/大嶼山.md "wikilink")[大水坑的](../Page/大水坑_\(大嶼山\).md "wikilink")[聖母神樂院擔任神父超過](../Page/聖母神樂院.md "wikilink")30年。

在走過三個世紀的生命中，他歷經十位天主教[教宗](../Page/教宗.md "wikilink")、兩位[中國皇帝](../Page/中國皇帝.md "wikilink")，包含一皇朝及兩共和時代。2005年11月12日，高師謙以108歲高齡從香港來台，在[台中](../Page/台中.md "wikilink")[法蒂瑪天主堂觀禮其姪孫](../Page/法蒂瑪天主堂.md "wikilink")[高豪晉鐸為神父](../Page/高豪.md "wikilink")\[3\]。

2007年12月11日早上在聖母神樂院內辭世\[4\]，終年110歲330天。

## 參見

  - [老化](../Page/老化.md "wikilink")
  - [存活於世超級人瑞列表](../Page/存活於世超級人瑞列表.md "wikilink")
  - [最年長者](../Page/最年長者.md "wikilink")

## 參考文獻

## 外部連結

  - [紀念高師謙神父－聖母神樂院](http://hk.myblog.yahoo.com/nicolauskao110)
  - [高師謙神父專頁－公教報](http://kkp.catholic.org.hk/FrKao/FrKao_mainpage.html)
  - [An
    interview](https://web.archive.org/web/20120716185959/http://www.thestandard.com.hk/news_detail.asp?we_cat=4&art_id=3031&sid=4908247&con_type=1&d_str=20051008)
    ，香港《[英文虎報](../Page/英文虎報.md "wikilink")》，2005年10月8日
  - [淡水天主堂](https://web.archive.org/web/20070203172506/http://www.catholic.org.tw/fatimads/e12004-a4.htm)
  - [108歲神父高師謙
    來台宣神恩](http://www.libertytimes.com.tw/2005/new/nov/13/today-center1.htm)，自由電子報，2005年11月13日。
  - [高神父106歲時接受香港電台訪問的片段](http://www.youtube.com/watch?v=Th1UC_c3jYY)

[Category:中华民国天主教神父](../Category/中华民国天主教神父.md "wikilink")
[Category:香港天主教徒](../Category/香港天主教徒.md "wikilink")
[Category:廈門大學校友](../Category/廈門大學校友.md "wikilink")
[Category:中国超级人瑞](../Category/中国超级人瑞.md "wikilink")
[Category:香港人瑞](../Category/香港人瑞.md "wikilink")
[Category:白内障患者](../Category/白内障患者.md "wikilink")
[S](../Category/高姓.md "wikilink")

1.  [熙篤會高師謙神父
    一百一十高齡安息](http://kkp.catholic.org.hk/lo/lo3330/lo3330_08.htm)公教報
    3330期，2007年12月16日

2.
3.  [天主教隱修會的108歲神父高師謙與侄孫高豪神父](http://www.dfun.com.tw/topic/topic_06.aspx?articale_id=1177)台灣社區新聞網，2005年11月18日

4.