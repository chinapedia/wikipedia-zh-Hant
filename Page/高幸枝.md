**高幸枝**（），[台灣](../Page/台灣.md "wikilink")[女演員](../Page/女演員.md "wikilink")、[電視製作人](../Page/電視製作人.md "wikilink")。因演技出眾，被歸類於「實力派演員」，並從事演員生涯達40年以上。晚年也參與[電視幕後工作](../Page/電視.md "wikilink")，2002年製作之電視劇《素心蘭》為其代表作。2006年6月26日，尚於[大愛電視台參與連續劇演出的高幸枝因](../Page/大愛電視台.md "wikilink")[腎臟](../Page/腎臟.md "wikilink")[癌](../Page/癌.md "wikilink")[腫瘤破裂過世](../Page/腫瘤.md "wikilink")，年63歲。

## 生平

1962年，仍於[國立台灣藝術專科學校](../Page/國立台灣藝術專科學校.md "wikilink")（國立藝專，今[國立台灣藝術大學](../Page/國立台灣藝術大學.md "wikilink")）就讀的高幸枝，投考[中央電影公司](../Page/中央電影公司.md "wikilink")，同期或前後期有[王莫愁](../Page/王莫愁.md "wikilink")、[歸亞蕾](../Page/歸亞蕾.md "wikilink")、[勾峰等知名演員](../Page/勾峰.md "wikilink")。之後，高幸枝多參與[台語電影演出](../Page/台語電影.md "wikilink")。高幸枝畢業後，亦從事台語電影之演員工作。

1964年，高幸枝首度擔綱主演台語片《恩重如山》（知名攝影[廖萬文之作品](../Page/廖萬文.md "wikilink")），演出[老師一職](../Page/老師.md "wikilink")；因科班表演技巧，被歸於實力派演員。同年，台語片迅速沒落，她也參與[國語片](../Page/國語片.md "wikilink")，其中擔任女配角的《[蚵女](../Page/蚵女.md "wikilink")》（台灣首部[彩色電影](../Page/彩色.md "wikilink")）一片，於[亞太影展打敗](../Page/亞太影展.md "wikilink")《[梁山伯與祝英台](../Page/梁山伯與祝英台.md "wikilink")》，獲得最佳影片獎。

高幸枝真正讓台灣觀眾熟悉的，是電視劇演出。1972年以[現場直播方式播出](../Page/現場直播.md "wikilink")，由台灣[西螺](../Page/西螺鎮.md "wikilink")[少林](../Page/少林.md "wikilink")[武術傳承的真實故事改編的](../Page/武術.md "wikilink")[中華電視台連續劇](../Page/中華電視台.md "wikilink")《[西螺七劍](../Page/西螺七劍.md "wikilink")》，是她所主演戲劇中，最為人熟悉的電視劇。該齣連演222集的連續劇中，扮演「四崁」（施翠蓮）的高幸枝，亦與劇中主角之一的「[阿善師](../Page/阿善師.md "wikilink")」[劉林成為夫妻](../Page/劉林.md "wikilink")。據聞這段婚姻因為劉林的一段[外遇](../Page/外遇.md "wikilink")，高幸枝和劉林10多年不講話，至她去世前兩天，劉林才把簽好的那份[離婚同意書給了高幸枝](../Page/離婚.md "wikilink")。

雖然因為外型關係，高幸枝並無主演甚多影片，但飾演了許多如慈母或壞婆婆的配角角色，為台語電視劇常見的熟面孔。除此，她也偶而參與其他電影的演出，如國語電影《[小畢的故事](../Page/小畢的故事.md "wikilink")》。之後，她嘗試參與電視幕後工作，2002年製作之台語連續劇《素心蘭》為其代表作。

2006年6月26日，尚於[大愛電視台參與連續劇演出的高幸枝因腎臟癌腫瘤破裂過世](../Page/大愛電視台.md "wikilink")，年63歲。

## 演出作品

### 電影

| 年份    | 片名                                                 | 角色    |
| ----- | -------------------------------------------------- | ----- |
| 1962年 | 《龍山寺之戀》                                            |       |
| 1963年 | 《新妻鏡》                                              |       |
| 1963年 | 《薇薇的週記》                                            |       |
| 1964年 | 《義犬救子》                                             |       |
| 1964年 | 《恩重如山》                                             |       |
| 1964年 | 《蚵女》                                               |       |
| 1965年 | 《雷堡風雲》                                             |       |
| 1965年 | 《為愛走天涯》                                            |       |
| 1967年 | 《大某小姨》                                             | 舞女-秋雲 |
| 1968年 | 《往日的舊夢》                                            |       |
| 1968年 | 《男人的眼淚》                                            |       |
| 1968年 | 《當作沒愛過我》                                           |       |
| 1968年 | 《河邊春夢》                                             |       |
| 1968年 | 《海邊春情》                                             |       |
| 1969年 | 《燒酒仙大鬧女人島》                                         |       |
| 1969年 | 《婚夜奇聞》                                             |       |
| 1969年 | 《恨你入骨》                                             |       |
| 1969年 | 《飛龍王子破群妖》                                          |       |
| 1969年 | 《丈夫與生育》                                            |       |
| 1969年 | 《男之罪孽》                                             |       |
| 1969年 | 《危險的青春》（兼任監製）                                      |       |
| 1969年 | 《[今天不回家](../Page/今天不回家_\(1969年電影\).md "wikilink")》 | 仙人跳女子 |
| 1970年 | 《再見阿郎》                                             |       |
| 1970年 | 《桃太郎斬七妖》                                           |       |
| 1971年 | 《我心碎了》                                             |       |
| 1974年 | 《秋燈夜雨》                                             |       |
| 1975年 | 《桃花女鬥周公》                                           |       |
| 1978年 | 《保鑣》                                               |       |
| 1979年 | 《秋蓮》                                               |       |
| 1980年 | 《博多夜船》                                             |       |
|       |                                                    |       |

### 電視劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頻道</p></th>
<th><p>劇名</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1971年12月6日～1972年4月10日</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《<a href="../Page/嘉慶皇帝.md" title="wikilink">嘉慶君與</a><a href="../Page/王得祿.md" title="wikilink">王得祿</a>》（共68集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1972年3月7日～1972年10月13日</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《西螺七劍》（共222集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1972年10月16日～1973年1月28日</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《鳳山虎》（共105集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年6月2日～1984年9月22日<br />
每週六20:00～22:00播出</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《<a href="../Page/大俠沈勝衣.md" title="wikilink">大俠沈勝衣</a>》第十一單元：雷霆千里（單元劇）</p></td>
<td><p>杜筠</p></td>
</tr>
<tr class="odd">
<td><p>1985年</p></td>
<td><p><a href="../Page/台視.md" title="wikilink">台視</a></p></td>
<td><p>《<a href="../Page/笑傲江湖.md" title="wikilink">笑傲江湖</a>》</p></td>
<td><p>定逸師太</p></td>
</tr>
<tr class="even">
<td><p>1985年4月23日～1985年5月28日<br />
每週二21:30～23:00播出</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《當時明月在》（單元劇）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1985年</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《<a href="../Page/生死門.md" title="wikilink">生死門</a>》</p></td>
<td><p>妙一神尼</p></td>
</tr>
<tr class="even">
<td><p>1985年10月8日～1985年11月18日</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《<a href="../Page/藍與黑.md" title="wikilink">藍與黑</a>》（共30集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989年2月13日～1989年4月7日</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《<a href="../Page/海鷗飛處彩雲飛.md" title="wikilink">海鷗飛處彩雲飛</a>》（共40集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992年11月9日</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《<a href="../Page/劉伯溫傳奇.md" title="wikilink">劉伯溫傳奇</a>》法內情</p></td>
<td><p>王春娥</p></td>
</tr>
<tr class="odd">
<td><p>1993年9月20日～1993年9月27日<br />
每週一播出</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《<a href="../Page/劉伯溫傳奇.md" title="wikilink">劉伯溫傳奇</a>》歸鄉</p></td>
<td><p>駱青</p></td>
</tr>
<tr class="even">
<td><p>|<a href="../Page/台視.md" title="wikilink">台視</a></p></td>
<td><p>《大瓦厝》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年5月24日，1999年5月31日</p></td>
<td><p><a href="../Page/中華電視公司.md" title="wikilink">中華電視公司</a></p></td>
<td><p>《<a href="../Page/台灣靈異事件.md" title="wikilink">台灣靈異事件</a>》芋頭鬼與蕃薯鬼</p></td>
<td><p>阿霞</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/大愛電視台.md" title="wikilink">大愛電視台</a></p></td>
<td><p>《明月照紅塵》</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  -
  -
  -
[G高](../Category/台湾電影女演员.md "wikilink")
[G高](../Category/台灣電視製作人.md "wikilink")
[G高](../Category/1943年出生.md "wikilink")
[G高](../Category/2006年逝世.md "wikilink")
[Category:國立臺灣藝術專科學校校友](../Category/國立臺灣藝術專科學校校友.md "wikilink")
[Category:高姓](../Category/高姓.md "wikilink")