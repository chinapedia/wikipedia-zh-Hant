**高棉语**（，[国际音标](../Page/国际音标.md "wikilink")：）是[高棉族的语言](../Page/高棉族.md "wikilink")；亦是以高棉人为主体民族的[柬埔寨的](../Page/柬埔寨.md "wikilink")[官方语言和通用语言](../Page/官方语言.md "wikilink")，故亦称**柬埔寨语**。高棉语属于[南亚语系](../Page/南亚语系.md "wikilink")[孟-高棉语族](../Page/孟-高棉语族.md "wikilink")。在柬埔寨，约有1300万人以高棉语为母语，约100万人以高棉语为第二语言；在[泰国](../Page/泰国.md "wikilink")、[寮國和](../Page/寮國.md "wikilink")[越南也有約](../Page/越南.md "wikilink")200万的使用者，以当地高棉人为主\[1\]\[2\]。

## 历史

高棉语的历史发展大致可以分为三个时期：（1）高棉[祖语和上古高棉语时期](../Page/祖语.md "wikilink")：公元14世纪以前；（2）中古高棉语时期：公元14-19世纪；（3）现代高棉语时期：公元19世纪以后。\[3\]

高棉祖语历史悠久，公元初期生活在今天柬埔寨境内居民使用的语言就与现在的高棉语有联系。\[4\]

上古高棉语时期，在受到[印度文化深远影响的柬埔寨](../Page/印度文化圈.md "wikilink")，社会非正式地分成掌握[梵语的精英阶层和只会高棉语的平民百姓](../Page/梵语.md "wikilink")。在柬埔寨发现的最早的有年份可考的高棉语碑文刻于公元611年，[梵文碑文则刻于两年之后](../Page/梵文.md "wikilink")。梵文碑文都采用诗歌的形式，歌颂神灵、国王、统治阶层或僧侣；高棉语碑文则用散文的形式，记录寺庙建造的过程，规定奴隶的职责，制定征税制度，诅咒企图破坏寺庙的人。\[5\]13世纪，柬埔寨大多数人改信[上座部佛教](../Page/上座部佛教.md "wikilink")，[巴利语的地位逐渐提高](../Page/巴利语.md "wikilink")。\[6\]

[高棉王国后期](../Page/高棉王国.md "wikilink")，大量印度文化的学者和经典逐渐从[吴哥转移到了](../Page/吴哥城.md "wikilink")[泰国的](../Page/泰国.md "wikilink")[阿瑜陀耶](../Page/阿瑜陀耶.md "wikilink")。从此到[昂占一世时期](../Page/昂占一世.md "wikilink")，柬埔寨的语言产生了永久性的变化，高棉语的地位大为上升。\[7\]

19世纪中后期，柬埔寨成为[法国的](../Page/法国.md "wikilink")[保护国](../Page/柬埔寨保护国.md "wikilink")，[法语成为官方语言](../Page/法语.md "wikilink")。高棉语被排斥，学校不教授高棉语。柬埔寨独立后，高棉语重新成为官方语言，被写入宪法。这一时期，为解决高棉语中缺乏现代政治、经济、科技词汇的问题，[僧王和](../Page/僧王.md "wikilink")从巴利语中大量借用词汇，创造出现代高棉语词汇，编著了《高棉语大辞典》，有效地保护了高棉语和高棉文化，避免其被法语所取代。\[8\]

## 方言

### 地域方言

在柬埔寨境内，高棉语可以分为三个大的[方言区](../Page/方言.md "wikilink")：西部方言区，以[马德望省为中心](../Page/马德望省.md "wikilink")，与泰国东北部接壤；东部方言区，临近越南；中部方言区，以[首都](../Page/首都.md "wikilink")[金边为中心](../Page/金边.md "wikilink")\[9\]。

高棉语的标准音为金边的口音\[10\]。不过作为基础方言的金边方言本身也处于发展之中，现在的金边方言与国家电视台、广播台使用的语言相比，在语音和词汇上都产生了一定的差异\[11\]。

### 社会方言

高棉语的使用中存在严格的等级，各人使用的语言有异\[12\]。柬埔寨社会的两个特殊阶级——皇族和僧侣——都拥有自己的特殊语言习惯，较多使用梵语和巴利语词汇。与皇族成员和僧侣相关的身体、器官、器具、人物、动物名词都跟一般口语不同，对皇室成员和僧侣的称呼亦有专门的代词，使用的动词亦有不同。僧侣或皇族内部，僧侣对信徒，皇室对百姓，对话时使用的语言与普通人使用的语言均有所不同\[13\]。

## 音韻

### 辅音

标准高棉语的辅音情况可归纳如下\[14\]：

<table>
<thead>
<tr class="header">
<th><p>发音部位</p></th>
<th><p><a href="../Page/塞音.md" title="wikilink">塞音</a></p></th>
<th><p><a href="../Page/内爆音.md" title="wikilink">内爆音</a></p></th>
<th><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></th>
<th><p><a href="../Page/颤音.md" title="wikilink">颤音</a></p></th>
<th><p><a href="../Page/边音.md" title="wikilink">边音</a></p></th>
<th><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></th>
<th><p><a href="../Page/近音.md" title="wikilink">近音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>不送气</p></td>
<td><p><a href="../Page/送气.md" title="wikilink">送气</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/双唇音.md" title="wikilink">双唇</a>/<a href="../Page/唇齿音.md" title="wikilink">唇齿音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/齿龈音.md" title="wikilink">齿龈音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/硬腭音.md" title="wikilink">硬腭音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/软腭音.md" title="wikilink">软腭音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/声门音.md" title="wikilink">声门音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

高棉语的音节首可以有[复辅音](../Page/复辅音.md "wikilink")，相连的辅音不超过两个。\[15\]

### 元音

标准高棉语元音可归纳如下\[16\]：

<table>
<thead>
<tr class="header">
<th><p>长元音</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>长双元音</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>短元音</p></td>
<td></td>
<td><p>|</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>短双元音</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

高棉语马德望省方言的元音可归纳如下\[17\]：

<table>
<thead>
<tr class="header">
<th><p>长元音</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>长双元音</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>短元音</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>短双元音</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 声调

与周边的[泰语](../Page/泰语.md "wikilink")、[越南语等语言不同](../Page/越南语.md "wikilink")，高棉语没有[声调](../Page/声调.md "wikilink")。但有研究显示，金边方言中出现了音高曲拱，显示其正在向着有声调语言转变。\[18\]

## 词汇

由于历史上受到印度教-梵语文化和佛教-巴利语文化的影响，高棉语中有大量梵语和巴利语借词；一些新兴事物、现象也常借用巴利语词汇。柬埔寨曾经是法国的保护国，许多法语词汇也进入了高棉语。此外，柬埔寨历史上与周边的泰国和越南交流频繁，高棉语中也有一定的[泰语和](../Page/泰语.md "wikilink")[越南语借词](../Page/越南语.md "wikilink")。\[19\]

### 人称代词

在传统高棉语中，除了亲友间的谈话以外（亲戚间通常使用家庭代词），没有可以直译为“你”或“我”的无词汇意义、纯粹虚化的[人称代词](../Page/人称代词.md "wikilink")。人们使用的人称代词都会强调说话人和听话的相对身份和地位，表示出身份的“升格”和“降格”。\[20\]（即类似于汉语文言中的“朕”、“孤”、“臣”、“僕”、“君”、“阁下”等。）

### 核心词汇

高棉语的[斯瓦迪士核心词汇表如下](../Page/斯瓦迪士核心詞列表.md "wikilink")\[21\]\[22\]：

<table>
<caption>高棉语核心词汇表</caption>
<thead>
<tr class="header">
<th><p>顺序</p></th>
<th><p>词表（用英文表示）</p></th>
<th><p>高棉文</p></th>
<th><p>音标</p></th>
<th><p>《柬汉词典》解释</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>I</p></td>
<td></td>
<td></td>
<td><p>我，本人，鄙人（对长辈或平辈的自称）；仆从</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>you (singular)</p></td>
<td></td>
<td></td>
<td><p>先生，您</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>你</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>he</p></td>
<td></td>
<td></td>
<td><p>他，她</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>他；他们；别人，人家【梵语；巴利语】</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>we</p></td>
<td></td>
<td></td>
<td><p>我们，咱们</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>you (plural)</p></td>
<td></td>
<td></td>
<td><p>你们</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>they</p></td>
<td></td>
<td></td>
<td><p>他们</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>this</p></td>
<td></td>
<td></td>
<td><p>这，此</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>that</p></td>
<td></td>
<td></td>
<td><p>那</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>here</p></td>
<td></td>
<td></td>
<td><p>此，这里，此地，本地</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>there</p></td>
<td></td>
<td></td>
<td><p>彼，那里</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>who</p></td>
<td></td>
<td></td>
<td><p>哪个，谁</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>谁</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>谁</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>what</p></td>
<td></td>
<td></td>
<td><p>什么，何</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>什么</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>where</p></td>
<td></td>
<td></td>
<td><p>哪儿</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>在哪里</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>when</p></td>
<td></td>
<td></td>
<td><p>何时，什么时候（指问话或表示疑惑）</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>how</p></td>
<td></td>
<td></td>
<td><p>怎么；如何</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>not</p></td>
<td></td>
<td></td>
<td><p>（表示否定或提问时用的）不，否，吗</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>all</p></td>
<td></td>
<td></td>
<td><p>完全的，完整的，圆满的，十足的，足够的</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>many</p></td>
<td></td>
<td></td>
<td><p>多的，众多的，多数的</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>some</p></td>
<td></td>
<td></td>
<td><p>有些，一些；一些，某些，有的，有些</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>few</p></td>
<td></td>
<td></td>
<td><p>少的</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>other</p></td>
<td></td>
<td></td>
<td><p>其他*</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>one</p></td>
<td></td>
<td></td>
<td><p>一</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>two</p></td>
<td></td>
<td></td>
<td><p>二</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>three</p></td>
<td></td>
<td></td>
<td><p>三</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>four</p></td>
<td></td>
<td></td>
<td><p>四</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p>five</p></td>
<td></td>
<td></td>
<td><p>五</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>big</p></td>
<td></td>
<td></td>
<td><p>大的，巨大的</p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p>long</p></td>
<td></td>
<td></td>
<td><p>长的</p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p>wide</p></td>
<td></td>
<td></td>
<td><p>向两旁延伸的，展开的；宏大的，宽广的</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>宽的，宽广的，宽大的，宽敞的，广阔的</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>广阔的，宽广的</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p>thick</p></td>
<td></td>
<td></td>
<td><p>厚的</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>茂密的，茂盛的；浓浓的</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td><p>heavy</p></td>
<td></td>
<td></td>
<td><p>重的，沉重的，严重的</p></td>
</tr>
<tr class="odd">
<td><p>32</p></td>
<td><p>small</p></td>
<td></td>
<td></td>
<td><p>小的，细的</p></td>
</tr>
<tr class="even">
<td><p>33</p></td>
<td><p>short</p></td>
<td></td>
<td></td>
<td><p>短的，短小的</p></td>
</tr>
<tr class="odd">
<td><p>34</p></td>
<td><p>narrow</p></td>
<td></td>
<td></td>
<td><p>狭窄，狭小</p></td>
</tr>
<tr class="even">
<td><p>35</p></td>
<td><p>thin</p></td>
<td></td>
<td></td>
<td><p>薄的，微不足道的</p></td>
</tr>
<tr class="odd">
<td><p>36</p></td>
<td><p>woman</p></td>
<td></td>
<td></td>
<td><p><俗>女人，女性，妇女</p></td>
</tr>
<tr class="even">
<td><p>37</p></td>
<td><p>man (adult male)</p></td>
<td></td>
<td></td>
<td><p>男人，男子【巴利语；梵语】</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>男人，男子【巴利语；梵语】</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td><p>man (human being)</p></td>
<td></td>
<td></td>
<td><p>人，人们【梵语；巴利语】</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>人【巴利语；梵语】</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p>child</p></td>
<td></td>
<td></td>
<td><p>孩子，儿子；小孩；</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p>wife</p></td>
<td></td>
<td></td>
<td><p>妻子【梵语；巴利语】</p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td><p>husband</p></td>
<td></td>
<td></td>
<td><p>丈夫【梵语；巴利语】</p></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td><p>mother</p></td>
<td></td>
<td></td>
<td><p>妈妈</p></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td><p>father</p></td>
<td></td>
<td></td>
<td><p>父亲，爸爸</p></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td><p>animal</p></td>
<td></td>
<td></td>
<td><p>动物</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>45</p></td>
<td><p>fish</p></td>
<td></td>
<td></td>
<td><p>鱼</p></td>
</tr>
<tr class="odd">
<td><p>46</p></td>
<td><p>bird</p></td>
<td></td>
<td></td>
<td><p>鸟类，雄性鸟类【梵语；巴利语】</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>飞禽，鸟类；雌性鸟类【梵语；巴利语】</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td><p>dog</p></td>
<td></td>
<td></td>
<td><p>狗</p></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td><p>louse</p></td>
<td></td>
<td></td>
<td><p>（人和动物身上的）跳蚤；虱子</p></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td><p>snake</p></td>
<td></td>
<td></td>
<td><p>蛇</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>蛇【梵语；巴利语】</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>50</p></td>
<td><p>worm</p></td>
<td></td>
<td></td>
<td><p>虫</p></td>
</tr>
<tr class="even">
<td><p>51</p></td>
<td><p>tree</p></td>
<td></td>
<td></td>
<td><p>木，树，木材</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>树，草</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>52</p></td>
<td><p>forest</p></td>
<td></td>
<td></td>
<td><p>森林，树林，丛林</p></td>
</tr>
<tr class="odd">
<td><p>53</p></td>
<td><p>stick</p></td>
<td></td>
<td></td>
<td><p>枝，桠</p></td>
</tr>
<tr class="even">
<td><p>54</p></td>
<td><p>fruit</p></td>
<td></td>
<td></td>
<td><p>水果（总称）【梵语；巴利语】</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>果，果实</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>55</p></td>
<td><p>seed</p></td>
<td></td>
<td></td>
<td><p>种，种子【梵语；巴利语】</p></td>
</tr>
<tr class="odd">
<td><p>56</p></td>
<td><p>leaf</p></td>
<td></td>
<td></td>
<td><p>叶子</p></td>
</tr>
<tr class="even">
<td><p>57</p></td>
<td><p>root</p></td>
<td></td>
<td></td>
<td><p>根，根部</p></td>
</tr>
<tr class="odd">
<td><p>58</p></td>
<td><p>bark (of a tree)</p></td>
<td></td>
<td></td>
<td><p>树皮，皮，果皮，壳，果壳</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>59</p></td>
<td><p>flower</p></td>
<td></td>
<td></td>
<td><p>花，花朵，花卉，花序</p></td>
</tr>
<tr class="even">
<td><p>60</p></td>
<td><p>grass</p></td>
<td></td>
<td></td>
<td><p>草，草本植物</p></td>
</tr>
<tr class="odd">
<td><p>61</p></td>
<td><p>rope</p></td>
<td></td>
<td></td>
<td><p>线，绳，带子，条</p></td>
</tr>
<tr class="even">
<td><p>62</p></td>
<td><p>skin</p></td>
<td></td>
<td></td>
<td><p>皮，皮肤</p></td>
</tr>
<tr class="odd">
<td><p>63</p></td>
<td><p>meat</p></td>
<td></td>
<td></td>
<td><p>【解剖学】（人、动物的）肉，骨肉，（果）肉</p></td>
</tr>
<tr class="even">
<td><p>64</p></td>
<td><p>blood</p></td>
<td></td>
<td></td>
<td><p>【医学】血，血液</p></td>
</tr>
<tr class="odd">
<td><p>65</p></td>
<td><p>bone</p></td>
<td></td>
<td></td>
<td><p>骨，骨头，骸骨</p></td>
</tr>
<tr class="even">
<td><p>66</p></td>
<td><p>fat (noun)</p></td>
<td></td>
<td></td>
<td><p>油脂，脂肪，荤油</p></td>
</tr>
<tr class="odd">
<td><p>67</p></td>
<td><p>egg</p></td>
<td></td>
<td></td>
<td><p>卵；蛋</p></td>
</tr>
<tr class="even">
<td><p>68</p></td>
<td><p>horn</p></td>
<td></td>
<td></td>
<td><p>（动物的）角</p></td>
</tr>
<tr class="odd">
<td><p>69</p></td>
<td><p>tail</p></td>
<td></td>
<td></td>
<td><p>尾，尾巴</p></td>
</tr>
<tr class="even">
<td><p>70</p></td>
<td><p>feather</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>71</p></td>
<td><p>hair</p></td>
<td></td>
<td></td>
<td><p>头发</p></td>
</tr>
<tr class="even">
<td><p>72</p></td>
<td><p>head</p></td>
<td></td>
<td></td>
<td><p>头，头部，首</p></td>
</tr>
<tr class="odd">
<td><p>73</p></td>
<td><p>ear</p></td>
<td></td>
<td></td>
<td><p>耳朵</p></td>
</tr>
<tr class="even">
<td><p>74</p></td>
<td><p>eye</p></td>
<td></td>
<td></td>
<td><p>目，眼睛</p></td>
</tr>
<tr class="odd">
<td><p>75</p></td>
<td><p>nose</p></td>
<td></td>
<td></td>
<td><p>鼻</p></td>
</tr>
<tr class="even">
<td><p>76</p></td>
<td><p>mouth</p></td>
<td></td>
<td></td>
<td><p>口，嘴</p></td>
</tr>
<tr class="odd">
<td><p>77</p></td>
<td><p>tooth</p></td>
<td></td>
<td></td>
<td><p>【生理学】牙，牙齿</p></td>
</tr>
<tr class="even">
<td><p>78</p></td>
<td><p>tongue</p></td>
<td></td>
<td></td>
<td><p>舌，舌头</p></td>
</tr>
<tr class="odd">
<td><p>79</p></td>
<td><p>fingernail</p></td>
<td></td>
<td></td>
<td><p>指甲；爪，爪状物</p></td>
</tr>
<tr class="even">
<td><p>80</p></td>
<td><p>foot</p></td>
<td></td>
<td></td>
<td><p>脚，足，蹄</p></td>
</tr>
<tr class="odd">
<td><p>81</p></td>
<td><p>leg</p></td>
<td></td>
<td></td>
<td><p>脚，足，蹄</p></td>
</tr>
<tr class="even">
<td><p>82</p></td>
<td><p>knee</p></td>
<td></td>
<td></td>
<td><p>膝，膝部</p></td>
</tr>
<tr class="odd">
<td><p>83</p></td>
<td><p>hand</p></td>
<td></td>
<td></td>
<td><p>手，手臂</p></td>
</tr>
<tr class="even">
<td><p>84</p></td>
<td><p>wing</p></td>
<td></td>
<td></td>
<td><p>翅膀，羽翼</p></td>
</tr>
<tr class="odd">
<td><p>85</p></td>
<td><p>belly</p></td>
<td></td>
<td></td>
<td><p>腹，腹部，肚</p></td>
</tr>
<tr class="even">
<td><p>86</p></td>
<td><p>guts</p></td>
<td></td>
<td></td>
<td><p>肠</p></td>
</tr>
<tr class="odd">
<td><p>87</p></td>
<td><p>neck</p></td>
<td></td>
<td></td>
<td><p>颈，脖</p></td>
</tr>
<tr class="even">
<td><p>88</p></td>
<td><p>back</p></td>
<td></td>
<td></td>
<td><p>【生理学】背，背部；背面，（东西的）背</p></td>
</tr>
<tr class="odd">
<td><p>89</p></td>
<td><p>breast</p></td>
<td></td>
<td></td>
<td><p>乳房</p></td>
</tr>
<tr class="even">
<td><p>90</p></td>
<td><p>heart</p></td>
<td></td>
<td></td>
<td><p>【生理学】心脏</p></td>
</tr>
<tr class="odd">
<td><p>91</p></td>
<td><p>liver</p></td>
<td></td>
<td></td>
<td><p>肝，肝脏</p></td>
</tr>
<tr class="even">
<td><p>92</p></td>
<td><p>to drink</p></td>
<td></td>
<td></td>
<td><p>喝，饮，吃</p></td>
</tr>
<tr class="odd">
<td><p>93</p></td>
<td><p>to eat</p></td>
<td></td>
<td></td>
<td><p>吃（指孩子）</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>吃，食（指小辈与平辈，动物）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>94</p></td>
<td><p>to bite</p></td>
<td></td>
<td></td>
<td><p>咬，啃</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>（鸟类）啄；（蛇）咬</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>95</p></td>
<td><p>to suck</p></td>
<td></td>
<td></td>
<td><p>吸，吮吸，吸收</p></td>
</tr>
<tr class="even">
<td><p>96</p></td>
<td><p>to spit</p></td>
<td></td>
<td></td>
<td><p>吐（指唾沫及痰）</p></td>
</tr>
<tr class="odd">
<td><p>97</p></td>
<td><p>to vomit</p></td>
<td></td>
<td></td>
<td><p>【医学】吐，呕吐，作呕</p></td>
</tr>
<tr class="even">
<td><p>98</p></td>
<td><p>to blow</p></td>
<td></td>
<td></td>
<td><p>刮</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>吹，吹奏</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>99</p></td>
<td><p>to breathe</p></td>
<td></td>
<td></td>
<td><p>呼吸，喘息，透气</p></td>
</tr>
<tr class="odd">
<td><p>100</p></td>
<td><p>to laugh</p></td>
<td></td>
<td></td>
<td><p>笑</p></td>
</tr>
<tr class="even">
<td><p>101</p></td>
<td><p>to see</p></td>
<td></td>
<td></td>
<td><p>看见，看出，看到</p></td>
</tr>
<tr class="odd">
<td><p>102</p></td>
<td><p>to hear</p></td>
<td></td>
<td></td>
<td><p>听见，闻知</p></td>
</tr>
<tr class="even">
<td><p>103</p></td>
<td><p>to know</p></td>
<td></td>
<td></td>
<td><p>会，能；理解，通晓，懂得</p></td>
</tr>
<tr class="odd">
<td><p>104</p></td>
<td><p>to think</p></td>
<td></td>
<td></td>
<td><p>想，思考，考虑，盘算</p></td>
</tr>
<tr class="even">
<td><p>105</p></td>
<td><p>to smell</p></td>
<td></td>
<td></td>
<td><p>嗅，闻（不及物）</p></td>
</tr>
<tr class="odd">
<td><p>106</p></td>
<td><p>to fear</p></td>
<td></td>
<td></td>
<td><p>恐怕，畏惧，惧怕</p></td>
</tr>
<tr class="even">
<td><p>107</p></td>
<td><p>to sleep</p></td>
<td></td>
<td></td>
<td><p>睡，躺</p></td>
</tr>
<tr class="odd">
<td><p>108</p></td>
<td><p>to live</p></td>
<td></td>
<td></td>
<td><p>活着，生存</p></td>
</tr>
<tr class="even">
<td><p>109</p></td>
<td><p>to die</p></td>
<td></td>
<td></td>
<td><p>死，死亡，逝世</p></td>
</tr>
<tr class="odd">
<td><p>110</p></td>
<td><p>to kill</p></td>
<td></td>
<td></td>
<td><p>屠杀，宰杀，戕害；杀死，杀害</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>111</p></td>
<td><p>to fight</p></td>
<td></td>
<td></td>
<td><p>碰，撞，绊；角斗，摔打</p></td>
</tr>
<tr class="even">
<td><p>112</p></td>
<td><p>to hunt</p></td>
<td></td>
<td></td>
<td><p>射击动物</p></td>
</tr>
<tr class="odd">
<td><p>113</p></td>
<td><p>to hit</p></td>
<td></td>
<td></td>
<td><p>打，敲；打击，攻击</p></td>
</tr>
<tr class="even">
<td><p>114</p></td>
<td><p>to cut</p></td>
<td></td>
<td></td>
<td><p>剪，剪裁；切，切断；割，截；【医学】切除</p></td>
</tr>
<tr class="odd">
<td><p>115</p></td>
<td><p>to split</p></td>
<td></td>
<td></td>
<td><p>劈</p></td>
</tr>
<tr class="even">
<td><p>116</p></td>
<td><p>to stab</p></td>
<td></td>
<td></td>
<td><p>刺，插</p></td>
</tr>
<tr class="odd">
<td><p>117</p></td>
<td><p>to scratch</p></td>
<td></td>
<td></td>
<td><p>搔，挠，抓，扒</p></td>
</tr>
<tr class="even">
<td><p>118</p></td>
<td><p>to dig</p></td>
<td></td>
<td></td>
<td><p>挖，掘，开凿</p></td>
</tr>
<tr class="odd">
<td><p>119</p></td>
<td><p>to swim</p></td>
<td></td>
<td></td>
<td><p>游，游泳</p></td>
</tr>
<tr class="even">
<td><p>120</p></td>
<td><p>to fly</p></td>
<td></td>
<td></td>
<td><p>飞，飞翔</p></td>
</tr>
<tr class="odd">
<td><p>121</p></td>
<td><p>to walk</p></td>
<td></td>
<td></td>
<td><p>行，走</p></td>
</tr>
<tr class="even">
<td><p>122</p></td>
<td><p>to come</p></td>
<td></td>
<td></td>
<td><p>来</p></td>
</tr>
<tr class="odd">
<td><p>123</p></td>
<td><p>to lie (as in a bed)</p></td>
<td></td>
<td></td>
<td><p>睡，躺</p></td>
</tr>
<tr class="even">
<td><p>124</p></td>
<td><p>to sit</p></td>
<td></td>
<td></td>
<td><p>坐</p></td>
</tr>
<tr class="odd">
<td><p>125</p></td>
<td><p>to stand</p></td>
<td></td>
<td></td>
<td><p>站，立</p></td>
</tr>
<tr class="even">
<td><p>126</p></td>
<td><p>to turn (intransitive)</p></td>
<td></td>
<td></td>
<td><p>转，转向</p></td>
</tr>
<tr class="odd">
<td><p>127</p></td>
<td><p>to fall</p></td>
<td></td>
<td></td>
<td><p>下，落下，降下</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>跌，跌倒</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>128</p></td>
<td><p>to give</p></td>
<td></td>
<td></td>
<td><p>给，给予</p></td>
</tr>
<tr class="even">
<td><p>129</p></td>
<td><p>to hold</p></td>
<td></td>
<td></td>
<td><p>拿，握，持</p></td>
</tr>
<tr class="odd">
<td><p>130</p></td>
<td><p>to squeeze</p></td>
<td></td>
<td></td>
<td><p>勒，挤</p></td>
</tr>
<tr class="even">
<td><p>131</p></td>
<td><p>to rub</p></td>
<td></td>
<td></td>
<td><p>擦，磨，磨平，擦光，研磨（玻璃、金属等）</p></td>
</tr>
<tr class="odd">
<td><p>132</p></td>
<td><p>to wash</p></td>
<td></td>
<td></td>
<td><p>洗发；淋湿，弄湿</p></td>
</tr>
<tr class="even">
<td><p>133</p></td>
<td><p>to wipe</p></td>
<td></td>
<td></td>
<td><p>擦，抹；擦净，擦干</p></td>
</tr>
<tr class="odd">
<td><p>134</p></td>
<td><p>to pull</p></td>
<td></td>
<td></td>
<td><p>（连根）拔出，拔除；拔掉</p></td>
</tr>
<tr class="even">
<td><p>135</p></td>
<td><p>to push</p></td>
<td></td>
<td></td>
<td><p>推</p></td>
</tr>
<tr class="odd">
<td><p>136</p></td>
<td><p>to throw</p></td>
<td></td>
<td></td>
<td><p>抛，抛弃，丢掉；投掷，扔</p></td>
</tr>
<tr class="even">
<td><p>137</p></td>
<td><p>to tie</p></td>
<td></td>
<td></td>
<td><p>捆，缚，系，拴，束</p></td>
</tr>
<tr class="odd">
<td><p>138</p></td>
<td><p>to sew</p></td>
<td></td>
<td></td>
<td><p>缝，缝纫</p></td>
</tr>
<tr class="even">
<td><p>139</p></td>
<td><p>to count</p></td>
<td></td>
<td></td>
<td><p>计算，算数</p></td>
</tr>
<tr class="odd">
<td><p>140</p></td>
<td><p>to say</p></td>
<td></td>
<td></td>
<td><p>说，讲话，道</p></td>
</tr>
<tr class="even">
<td><p>141</p></td>
<td><p>to sing</p></td>
<td></td>
<td></td>
<td><p>唱，吟</p></td>
</tr>
<tr class="odd">
<td><p>142</p></td>
<td><p>to play</p></td>
<td></td>
<td></td>
<td><p>玩，玩耍</p></td>
</tr>
<tr class="even">
<td><p>143</p></td>
<td><p>to float</p></td>
<td></td>
<td></td>
<td><p>浮，漂浮，漂游</p></td>
</tr>
<tr class="odd">
<td><p>144</p></td>
<td><p>to flow</p></td>
<td></td>
<td></td>
<td><p>流，流动</p></td>
</tr>
<tr class="even">
<td><p>145</p></td>
<td><p>to freeze</p></td>
<td></td>
<td></td>
<td><p>凝固，凝结；结冰，冻结</p></td>
</tr>
<tr class="odd">
<td><p>146</p></td>
<td><p>to swell</p></td>
<td></td>
<td></td>
<td><p>膨胀，鼓起</p></td>
</tr>
<tr class="even">
<td><p>147</p></td>
<td><p>sun</p></td>
<td></td>
<td></td>
<td><p>白天，白昼；日；太阳</p></td>
</tr>
<tr class="odd">
<td><p>148</p></td>
<td><p>moon</p></td>
<td></td>
<td></td>
<td><p>月亮</p></td>
</tr>
<tr class="even">
<td><p>149</p></td>
<td><p>star</p></td>
<td></td>
<td></td>
<td><p>星</p></td>
</tr>
<tr class="odd">
<td><p>150</p></td>
<td><p>water</p></td>
<td></td>
<td></td>
<td><p>水，汁；液体【巴利语】</p></td>
</tr>
<tr class="even">
<td><p>151</p></td>
<td><p>rain</p></td>
<td></td>
<td></td>
<td><p>雨，雨水</p></td>
</tr>
<tr class="odd">
<td><p>152</p></td>
<td><p>river</p></td>
<td></td>
<td></td>
<td><p>江，河，川</p></td>
</tr>
<tr class="even">
<td><p>153</p></td>
<td><p>lake</p></td>
<td></td>
<td></td>
<td><p>湖</p></td>
</tr>
<tr class="odd">
<td><p>154</p></td>
<td><p>sea</p></td>
<td></td>
<td></td>
<td><p>海，大海【巴利语；梵语】</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>155</p></td>
<td><p>salt</p></td>
<td></td>
<td></td>
<td><p>盐</p></td>
</tr>
<tr class="even">
<td><p>156</p></td>
<td><p>stone</p></td>
<td></td>
<td></td>
<td><p>石头；【地质】岩石</p></td>
</tr>
<tr class="odd">
<td><p>157</p></td>
<td><p>sand</p></td>
<td></td>
<td></td>
<td><p>沙</p></td>
</tr>
<tr class="even">
<td><p>158</p></td>
<td><p>dust</p></td>
<td></td>
<td></td>
<td><p>灰尘，尘土，尘埃【巴利语】</p></td>
</tr>
<tr class="odd">
<td><p>159</p></td>
<td><p>earth</p></td>
<td></td>
<td></td>
<td><p>土，土地；泥土，土壤</p></td>
</tr>
<tr class="even">
<td><p>160</p></td>
<td><p>cloud</p></td>
<td></td>
<td></td>
<td><p>云，云彩，云雾</p></td>
</tr>
<tr class="odd">
<td><p>161</p></td>
<td><p>fog</p></td>
<td></td>
<td></td>
<td><p><俗>雾</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>162</p></td>
<td><p>sky</p></td>
<td></td>
<td></td>
<td><p>天，天空【梵语；巴利语】</p></td>
</tr>
<tr class="odd">
<td><p>163</p></td>
<td><p>wind</p></td>
<td></td>
<td></td>
<td><p>风；气，空气</p></td>
</tr>
<tr class="even">
<td><p>164</p></td>
<td><p>snow</p></td>
<td></td>
<td></td>
<td><p>雪【梵语；巴利语】</p></td>
</tr>
<tr class="odd">
<td><p>165</p></td>
<td><p>ice</p></td>
<td></td>
<td></td>
<td><p>冰</p></td>
</tr>
<tr class="even">
<td><p>166</p></td>
<td><p>smoke</p></td>
<td></td>
<td></td>
<td><p>烟，烟雾</p></td>
</tr>
<tr class="odd">
<td><p>167</p></td>
<td><p>fire</p></td>
<td></td>
<td></td>
<td><p>火</p></td>
</tr>
<tr class="even">
<td><p>168</p></td>
<td><p>ash</p></td>
<td></td>
<td></td>
<td><p>灰，灰烬，余烬</p></td>
</tr>
<tr class="odd">
<td><p>169</p></td>
<td><p>to burn</p></td>
<td></td>
<td></td>
<td><p>燃烧，烧毁</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>点火，点燃；燃烧</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>170</p></td>
<td><p>road</p></td>
<td></td>
<td></td>
<td><p>路，道路，途径，线路；公路</p></td>
</tr>
<tr class="even">
<td><p>171</p></td>
<td><p>mountain</p></td>
<td></td>
<td></td>
<td><p>山</p></td>
</tr>
<tr class="odd">
<td><p>172</p></td>
<td><p>red</p></td>
<td></td>
<td></td>
<td><p>红色的</p></td>
</tr>
<tr class="even">
<td><p>173</p></td>
<td><p>green</p></td>
<td></td>
<td></td>
<td><p>绿的，绿色的【泰语】</p></td>
</tr>
<tr class="odd">
<td><p>174</p></td>
<td><p>yellow</p></td>
<td></td>
<td></td>
<td><p>黄色的</p></td>
</tr>
<tr class="even">
<td><p>175</p></td>
<td><p>white</p></td>
<td></td>
<td></td>
<td><p>白的</p></td>
</tr>
<tr class="odd">
<td><p>176</p></td>
<td><p>black</p></td>
<td></td>
<td></td>
<td><p>黑的，黑色的；黑暗的</p></td>
</tr>
<tr class="even">
<td><p>177</p></td>
<td><p>night</p></td>
<td></td>
<td></td>
<td><p>夜，夜晚</p></td>
</tr>
<tr class="odd">
<td><p>178</p></td>
<td><p>day</p></td>
<td></td>
<td></td>
<td><p>白天，白昼；日；太阳</p></td>
</tr>
<tr class="even">
<td><p>179</p></td>
<td><p>year</p></td>
<td></td>
<td></td>
<td><p>年，年份</p></td>
</tr>
<tr class="odd">
<td><p>180</p></td>
<td><p>warm</p></td>
<td></td>
<td></td>
<td><p>热的，炎热的，烫的</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>热的，微温的【巴利语；梵语】</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>181</p></td>
<td><p>cold</p></td>
<td></td>
<td></td>
<td><p>冷的，凉的；凉爽的</p></td>
</tr>
<tr class="even">
<td><p>182</p></td>
<td><p>full</p></td>
<td></td>
<td></td>
<td><p>满，满足，够，足够</p></td>
</tr>
<tr class="odd">
<td><p>183</p></td>
<td><p>new</p></td>
<td></td>
<td></td>
<td><p>新的，新颖的，崭新的；最近的，新近的</p></td>
</tr>
<tr class="even">
<td><p>184</p></td>
<td><p>old</p></td>
<td></td>
<td></td>
<td><p>老的，旧的</p></td>
</tr>
<tr class="odd">
<td><p>185</p></td>
<td><p>good</p></td>
<td></td>
<td></td>
<td><p>好的，美好的</p></td>
</tr>
<tr class="even">
<td><p>186</p></td>
<td><p>bad</p></td>
<td></td>
<td></td>
<td><p>坏的，恶劣的，不好的</p></td>
</tr>
<tr class="odd">
<td><p>187</p></td>
<td><p>rotten</p></td>
<td></td>
<td></td>
<td><p>腐朽的，腐烂的，变质的</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>腐烂，溃烂</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>188</p></td>
<td><p>dirty</p></td>
<td></td>
<td></td>
<td><p>沾满灰尘的，肮脏的</p></td>
</tr>
<tr class="even">
<td><p>189</p></td>
<td><p>straight</p></td>
<td></td>
<td></td>
<td><p>直的，笔直的；挺直的</p></td>
</tr>
<tr class="odd">
<td><p>190</p></td>
<td><p>round</p></td>
<td></td>
<td></td>
<td><p>圆的</p></td>
</tr>
<tr class="even">
<td><p>191</p></td>
<td><p>sharp (as a knife)</p></td>
<td></td>
<td></td>
<td><p>锋利的，尖锐的</p></td>
</tr>
<tr class="odd">
<td><p>192</p></td>
<td><p>dull (as a knife)</p></td>
<td></td>
<td></td>
<td><p>钝的</p></td>
</tr>
<tr class="even">
<td><p>193</p></td>
<td><p>smooth</p></td>
<td></td>
<td></td>
<td><p>滑的</p></td>
</tr>
<tr class="odd">
<td><p>194</p></td>
<td><p>wet</p></td>
<td></td>
<td></td>
<td><p>被淋湿的，浸湿的，沾湿的</p></td>
</tr>
<tr class="even">
<td><p>195</p></td>
<td><p>dry</p></td>
<td></td>
<td></td>
<td><p>枯的，干枯的，焦的</p></td>
</tr>
<tr class="odd">
<td><p>196</p></td>
<td><p>correct</p></td>
<td></td>
<td></td>
<td><p>对的，正确的；适合的，适宜的</p></td>
</tr>
<tr class="even">
<td><p>197</p></td>
<td><p>near</p></td>
<td></td>
<td></td>
<td><p>近的，接近的</p></td>
</tr>
<tr class="odd">
<td><p>198</p></td>
<td><p>far</p></td>
<td></td>
<td></td>
<td><p>远的，遥远的</p></td>
</tr>
<tr class="even">
<td><p>199</p></td>
<td><p>right</p></td>
<td></td>
<td></td>
<td><p>右面的</p></td>
</tr>
<tr class="odd">
<td><p>200</p></td>
<td><p>left</p></td>
<td></td>
<td></td>
<td><p>左，左边</p></td>
</tr>
<tr class="even">
<td><p>201</p></td>
<td><p>at</p></td>
<td></td>
<td></td>
<td><p>在，存在；住，住在；于</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>在，在于，至于，就……而言，就……方面</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>202</p></td>
<td><p>in</p></td>
<td></td>
<td></td>
<td><p>在…里，在…内，在…之中；以…，处于…</p></td>
</tr>
<tr class="odd">
<td><p>203</p></td>
<td><p>with</p></td>
<td></td>
<td></td>
<td><p>一起，一道，一块儿</p></td>
</tr>
<tr class="even">
<td><p>204</p></td>
<td><p>and</p></td>
<td></td>
<td></td>
<td><p>和，以及</p></td>
</tr>
<tr class="odd">
<td><p>205</p></td>
<td><p>if</p></td>
<td></td>
<td></td>
<td><p>假使，如果，倘若</p></td>
</tr>
<tr class="even">
<td><p>206</p></td>
<td><p>because</p></td>
<td></td>
<td></td>
<td><p>因为，由于</p></td>
</tr>
<tr class="odd">
<td><p>207</p></td>
<td><p>name</p></td>
<td></td>
<td></td>
<td><p>名字，姓名，称呼</p></td>
</tr>
</tbody>
</table>

## 语法

高棉语属于[分析语](../Page/分析语.md "wikilink")，大部分词没有[词形变化](../Page/词形变化.md "wikilink")，小部分词的[词性可按规律变动](../Page/词性.md "wikilink")，还有几个特殊[名词具备充当前缀](../Page/名词.md "wikilink")、使[动词或](../Page/动词.md "wikilink")[形容词变成名词的功能](../Page/形容词.md "wikilink")。语法意义主要由[词序和](../Page/词序.md "wikilink")[虚词表示](../Page/虚词.md "wikilink")。基本语序为[主-谓-宾](../Page/SVO.md "wikilink")，[形容词后置于修饰对象](../Page/形容词.md "wikilink")。\[23\]

## 文字

高棉語的书写工具是[高棉文字](../Page/高棉文字.md "wikilink")。

高棉文字属于[元音附标文字](../Page/元音附标文字.md "wikilink")。[辅音字母自带默认](../Page/辅音.md "wikilink")[元音](../Page/元音.md "wikilink")，根据所带默认元音不同可以分为高辅音和低辅音；通过添加其他元音附标，可以替代默认元音；通过添加辅音附标，可以表示[复辅音](../Page/复辅音.md "wikilink")；此外亦有独立元音字母，还有许多复杂标记和规则。\[24\]\[25\]如：

<table>
<thead>
<tr class="header">
<th><p>高棉文</p></th>
<th><p>国际音标</p></th>
<th><p>字母与附标的组合过程</p></th>
<th><p>解释</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p>/</p></td>
<td><p>辅音字母自带默认元音</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>辅音字母加元音附标，替代默认元音</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>辅音字母加辅音附标，组成复辅音<br />
另有“复辅音中、前一个辅音不分送气与否”之规则</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>辅音字母加辅音附标组成复辅音，再加元音附标替代默认元音</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>同上<br />
另有“词尾的不发音”之规则</p></td>
</tr>
</tbody>
</table>

現時絕大多數的[作業系統都不支援高棉文字](../Page/作業系統.md "wikilink")，因為高棉文字的標準制定比較遲完成。現時，在互聯網上有[Khmer
Software
Initiative](https://web.archive.org/web/20061205205430/http://www.khmeros.info/drupal/)計劃，主要為現時的主流作業系統提供高棉文字的字型、以及開發以高棉語為基礎語言的[KhmerOS](../Page/KhmerOS.md "wikilink")。除此之外，[Windows
Vista及](../Page/Windows_Vista.md "wikilink")[OpenSUSE](../Page/OpenSUSE.md "wikilink")
11.0 都支援高棉文。

### 高棉数字

[Khmer_numerals.png](https://zh.wikipedia.org/wiki/File:Khmer_numerals.png "fig:Khmer_numerals.png")

高棉数字为柬埔寨通行的[数字书写形式](../Page/记数系统.md "wikilink")。高棉数字为[十进制](../Page/十进制.md "wikilink")，共10个数字，与[阿拉伯数字的](../Page/阿拉伯数字.md "wikilink")“0”到“9”相对应。\[26\]

## 参考资料

## 外部連結

  - [柬英词典、高棉语词典在线查询](http://sealang.net/khmer/dictionary.htm)
  - [“民族语”网站的高棉语页面](http://www.ethnologue.com/subgroups/khmer)
  - [UCLA语言材料计划](https://web.archive.org/web/20060720065425/http://www.pavelicpapers.com/documents/odpor/index.html)的[高棉语](http://www.lmp.ucla.edu/Profile.aspx?LangID=75)页面

[Category:高棉语](../Category/高棉语.md "wikilink")
[Category:孟-高棉语族](../Category/孟-高棉语族.md "wikilink")
[Category:柬埔寨文化](../Category/柬埔寨文化.md "wikilink")
[Category:泰國語言](../Category/泰國語言.md "wikilink")
[Category:東南亞語言](../Category/東南亞語言.md "wikilink")
[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")

1.
2.
3.
4.

5.
6.
7.
8.

9.

10.

11.
12.
13.
14.

15.
16.
17.

18.
19.
20.
21.

22.

23.

24.
25.
26.