是[日本](../Page/日本.md "wikilink")[Hudson
Soft公司於](../Page/Hudson_Soft.md "wikilink")1986年9月12日發售的[紅白機遊戲](../Page/紅白機.md "wikilink")，改編自[世嘉的遊戲](../Page/世嘉.md "wikilink")《[神奇男孩](../Page/神奇男孩_\(游戏\).md "wikilink")》。

## 概要

和《[超級瑪利歐](../Page/超級瑪利歐.md "wikilink")》、《[刺猬索尼克](../Page/刺猬索尼克_\(1991年游戏\).md "wikilink")》等遊戲一樣，這是一款2D[橫向捲軸遊戲](../Page/橫向捲軸遊戲.md "wikilink")。其實這遊戲並非HUDSON的原創作品，HUDSON是從Westone（《神奇男孩》的開發者）得到遊戲的授權。這是[冒險島系列在FC平台上的第一個作品](../Page/高橋名人之冒險島#系列作品.md "wikilink")。遊戲的目的是救出主角高橋名人的戀人蒂娜。整個遊戲共有8個場景，而每一個場景又包括4個階段。主角會在最後一個階段的結尾遇到BOSS角色。只有打倒了BOSS（弱點在頭部）才可以順利通關，對付BOSS的難度隨著場景數的增加而增加（比如打倒場景1的BOSS只需攻擊8次，以後場景數每增加1攻擊數目增加2，到了場景8需攻擊22次方能打倒BOSS）。游戏续作《[高橋名人之冒險島II](../Page/高橋名人之冒險島II.md "wikilink")》于1991年7月在FC平台发行。

## 遊戲設定

在遊戲中如主角撞上敵方角色、巨大滾石、火焰，或落水、掉入深坑等，均會導致死亡。主角可從沿途中各種物品（如水果、蛋）中攝取能量和獲得新能力。新能力包括（但不限於）攻擊力、滑板和妖精保護。

## 制作

《冒险岛》的制作始于世嘉接机游戏《神奇男孩》的移植。Hudson获得从制作商Escape的移植授权。在移植过程中，游戏的主角进行了改动，并以Hudson的发言人[高桥名人命名](../Page/高桥名人.md "wikilink")。\[1\]在西方版本中，高桥名人命名为Master
Higgins。

## 评价

《高桥名人之冒险岛》收到正面至混合的评价。[GameSpot给与游戏](../Page/GameSpot.md "wikilink")6.5/10的分数。\[2\]GamesRadar将本游戏列为FC游戏机最佳游戏第23位。

## 参考资料

## 外部連結

  - [高橋名人之冒險島（HUDSON
    Selection）](http://www.hudson.co.jp/gamenavi/gamedb/softinfo/hu_select/advi/index.html)
  - [高橋名人之冒險島（Virtual
    Console）](http://www.nintendo.co.jp/wii/vc/vc_tmb/index.html)

[1](../Category/高桥名人之冒险岛系列.md "wikilink")
[Category:1986年电子游戏](../Category/1986年电子游戏.md "wikilink")
[Category:红白机游戏](../Category/红白机游戏.md "wikilink") [Category:Game Boy
Advance遊戲](../Category/Game_Boy_Advance遊戲.md "wikilink") [Category:Game
Boy游戏](../Category/Game_Boy游戏.md "wikilink")
[Category:MSX游戏](../Category/MSX游戏.md "wikilink") [Category:Wii
Virtual Console游戏](../Category/Wii_Virtual_Console游戏.md "wikilink")
[Category:行動電話遊戲](../Category/行動電話遊戲.md "wikilink")

1.
2.