**高橋香波**（），[日本](../Page/日本.md "wikilink")[女演員](../Page/女演員.md "wikilink")。[東京都出身](../Page/東京都.md "wikilink")。[Advance
Production所屬](../Page/Advance_Production.md "wikilink")。此前曾為[LesPros
Entertainment](../Page/LesPros_Entertainment.md "wikilink")（Next
Star）所屬。

## 演出

### 電視節目

  - 「」（[Sky PerfecTV\!](../Page/Sky_PerfecTV!.md "wikilink")
    371頻道、2003年）。
  - 「」（Sky PerfecTV\! 371頻道、2004年）。
  - 「Media之ABC」（[日本放送協會教育頻道](../Page/日本放送協會.md "wikilink")、2004年）。
  - 「」（[朝日電視台](../Page/朝日電視台.md "wikilink")、2010年12月20日 - ）飾演。

### 電視劇

  - 「[人間的証明](../Page/人間的証明.md "wikilink")」（[富士電視台](../Page/富士電視台.md "wikilink")、2004年）飾演[松坂慶子](../Page/松坂慶子.md "wikilink")・郡恭子的少女時代。
  - 「[真實的恐怖故事](../Page/真實的恐怖故事.md "wikilink")」（第1、2輯）（富士電視台、2004、2005年）。
  - 「[女王的教室](../Page/女王的教室.md "wikilink")」（[日本電視台](../Page/日本電視台.md "wikilink")、2005年）飾演田端美知子。
  - 「[野豬大改造](../Page/野豬大改造.md "wikilink")」（第2集）（日本電視台、2005年）。
  - 「新・[科搜研之女](../Page/科搜研之女.md "wikilink") 第2輯
    FILE-1」（[朝日電視台](../Page/朝日電視台.md "wikilink")、2005年）。
  - 「[唯愛](../Page/唯愛.md "wikilink")」（大結局）（日本電視台、2006年）。
  - 「[演歌女王](../Page/演歌女王.md "wikilink")」（日本電視台、2007年）飾演頑皮學生。
  - 「」（第2集）（[TBS電視台](../Page/TBS電視台.md "wikilink")、2014年）。
  - 「」（第1集）（[東京電視台](../Page/東京電視台.md "wikilink")、2014年）。

### 電影

  - [令人討厭的松子的一生](../Page/令人討厭的松子的一生.md "wikilink")（[東寶](../Page/東寶映畫.md "wikilink")、2006年）飾演學校合唱團團員。

### 舞台劇

  - Comic Musical
    XYX（[惠比壽Echo劇場](../Page/惠比壽Echo劇場.md "wikilink")、2008年8月28日
    - 31日）飾演。

### 廣告

  - [MTI](../Page/MTI.md "wikilink")「」（2003年）
  - [任天堂](../Page/任天堂.md "wikilink")「Mario Party 5」（2004年）
  - [Taraka玩具](../Page/Taraka玩具.md "wikilink")「人生 Game Candy」（2005年）
  - [House食品](../Page/House食品.md "wikilink")「[佛蒙特咖喱](../Page/佛蒙特咖喱.md "wikilink")」（2006年）
  - [日本放送協會](../Page/日本放送協會.md "wikilink")「Digital Campaign」（2006年）
  - [萬代](../Page/萬代.md "wikilink")「Amirin」（2006年）

### 雜誌

  - 「好朋友」（[講談社](../Page/講談社.md "wikilink")）（2003、2004年）
  - 「Nintendo Kids」（[雙葉社](../Page/雙葉社.md "wikilink")）（2003年）
  - 「」（[小學館](../Page/小學館.md "wikilink")）（2004年）
    ※[玩具反斗城](../Page/玩具反斗城.md "wikilink")「Girl
    Staff」的廣告
  - 「PURE PURE」（[辰巳出版](../Page/辰巳出版.md "wikilink")）（2004年）

### 硬照

  - [東大島](../Page/東大島.md "wikilink")「SORANTCITY」（2003、2004年）

### 宣傳錄影帶

  - [東大島](../Page/東大島.md "wikilink")「SORANTCITY」（2003、2004年）

### 其他演出

  - [櫻井智與](../Page/櫻井智.md "wikilink")[朝倉薰劇團](../Page/朝倉薰.md "wikilink")「Christmas
    Fantasy Live Broadcasting」（2003年）

### DVD

  - U-15
    Films「永遠」／[下垣真香](../Page/下垣真香.md "wikilink")×[佐武宇綺](../Page/佐武宇綺.md "wikilink")×高橋香波×

<!-- end list -->

  -
    製造商：JDC
    TRUST　產品編號：ENFD-0019　定價：（連稅）3990[日圓](../Page/日圓.md "wikilink")　發售日：2006/04/07

## 站外連結

  - [Advance
    Production官方個人詳細資料](http://advance-production.jp/talent/2013/09/post-34.html)
  - [LesPros
    Entertainment前官方個人詳細資料](https://web.archive.org/web/20130116054901/http://www.lespros.co.jp/nextstar/kanami_takahashi/)
  - [富士電視台「真實的恐怖故事」官方網站中的個人資料](http://www.fujitv.co.jp/honkowa/member/18.html)

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本兒童演員](../Category/日本兒童演員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink") [Category:前Burning
Production系列所屬藝人](../Category/前Burning_Production系列所屬藝人.md "wikilink")