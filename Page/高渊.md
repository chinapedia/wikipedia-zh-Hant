**高渊**（），是[马来西亚](../Page/马来西亚.md "wikilink")[槟城州](../Page/槟城州.md "wikilink")[威南縣最南端也是](../Page/威南縣.md "wikilink")[威省境内第三大的市镇](../Page/威省.md "wikilink")。南有[巴里文打](../Page/巴里文打.md "wikilink")，东有[万拉峇鲁](../Page/万拉峇鲁.md "wikilink")。

## 历史

高渊的名字而来是因为高渊有许多名叫Nibong的大树，Tebal则是因为早期人民把Nibong的叶放在屋顶上，而中文名则是取至早上当地一位官员的名字，而该官员的名字叫「高远」，久而久之就变成了「高渊」。高渊的历史悠久，在1900年，高渊就有人居住，并定居在[吉辇河](../Page/吉辇河.md "wikilink")（Sungai
Kerian）两岸。

## 人口

高渊人口以讲[潮州语的华裔为主](../Page/潮州语.md "wikilink")，还有[海南语等](../Page/海南语.md "wikilink")。

## 地形

高渊地形平缓，较少高地。东边地势较高，西边地势较平缓，适合发展。区内有一座州立公园（武吉班卓州公园）。

## 经济

高渊的经济以[渔业为主](../Page/渔业.md "wikilink")。高渊的渔业可说是[马来西亚最大的](../Page/马来西亚.md "wikilink")。高渊是拥有[马来西亚最多的](../Page/马来西亚.md "wikilink")[养鱼场](../Page/养鱼场.md "wikilink")，高渊也是一个建于[港口的地方](../Page/港口.md "wikilink")，故称高渊。[高渊港口](../Page/高渊港口.md "wikilink")（Sungai
Udang）建于河岸，使许多渔民出售鱼的地方。近年来，由于捕鱼业受到[槟城二桥的工程所影响](../Page/苏丹阿都哈林大桥.md "wikilink")，使得捕鱼数量逐年减少。因此，许多商人渐渐转向投资海上[养殖业](../Page/养殖业.md "wikilink")，造成养殖地区爆满，所以需要要有一个全面的方案，解决海上养殖业的问题。此外，高渊的[农业也较发达](../Page/农业.md "wikilink")。有大片的油棕园。高渊还有些[服务业](../Page/服务业.md "wikilink")、[制造业](../Page/制造业.md "wikilink")、[汽车业等等](../Page/汽车业.md "wikilink")。武吉班卓工业花园则是高渊的一个工业区，制造了许多就业机会，许多工厂都设立在这里。

## 教育

此区有三间小学、四间中学，而大部分[华裔学生都就读于培德华小](../Page/马来西亚华人.md "wikilink")。

### 学校

  - 高渊培德华文小学（SJKC Pai Teik）
  - 绍佳娜英达国民小学（SK Saujana Indah）
  - 美以美国民小学（SK Rendah Methodist）
  - 高渊东姑阿都拉曼国民中学（SMK Tunku Abdul Rahman）
  - 美以美国民中学（SMK Methodist）
  - 斯里尼蒙国民中学（SMK Seri Nibong）
  - 绍佳娜英达国民中学（SMK Saujana Indah）

### 高等学府

[缩略图](https://zh.wikipedia.org/wiki/File:Usm_kejuruteraan.jpg "fig:缩略图")
马来西亚著名大学——[马来西亚理科大学的工程系分校](../Page/马来西亚理科大学.md "wikilink")(Universiti
Sains Malaysia， Engineering
Campus)就位于此，而其坐落于霹雳仔（Transkrain，位于[槟城](../Page/槟城.md "wikilink")[霹雳边界](../Page/霹雳州.md "wikilink")）。

## 基建

高渊拥有良好的基建，在高渊卫星市有一所[诊所](../Page/诊所.md "wikilink")，方便附近的居民来看诊。此外，成功花园里设有高渊最大的高渊巴刹，并在近年来进行翻新，售卖商品多样化。此外，横跨高渊市中心的火车站也是高渊繁荣的强心剂，但已被电气化的[高渊站取代为当地居民提供服务](../Page/高渊站.md "wikilink")。\[1\]

## 交通

高渊有良好的交通网，汽车可以自由往来。高渊没有飞机场，但可通过高速工具，或[槟城二桥](../Page/苏丹阿都哈林大桥.md "wikilink")，可以轻易抵达高渊。高渊地处一个十分良好的地段，北上有[大山脚](../Page/大山脚.md "wikilink")、[乔治市等城市](../Page/乔治市.md "wikilink")，南下则能够抵达[太平](../Page/太平_\(霹雳\).md "wikilink")、[怡保](../Page/怡保.md "wikilink")、[吉隆坡等大型城市](../Page/吉隆坡.md "wikilink")。如今，「[檳島南部](../Page/檳島.md "wikilink")」已建設一座[大橋](../Page/苏丹阿都哈林大桥.md "wikilink")，與對岸的[威省連接](../Page/威省.md "wikilink")（此座[東南亞最長的橋是由](../Page/東南亞.md "wikilink")[馬來西亞與](../Page/馬來西亞.md "wikilink")[中華人民共和國共同出資興建](../Page/中華人民共和國.md "wikilink")）。现在，人们可以更快的从槟岛抵达高渊，也可通过[马来亚铁道](../Page/马来亚铁道.md "wikilink")[通勤铁路(KTM
Komuter)的](../Page/KTM通勤铁路北马区.md "wikilink")[高渊火车站抵达这里](../Page/高渊站.md "wikilink")。

## 社会与文化

每年，高渊会举办一年一度的[盂兰胜会](../Page/盂兰胜会.md "wikilink")。每逢这个时候，高渊有几个花园都会举办这一个盛事，热闹非凡。

## 景点

### 大伯公庙

高渊有一个大伯公庙，历史悠久，是游客必有的其中一个地方。大伯公庙由一个小庙宇变成如此庞大的庙，实在堪称奇迹。

### 观赏萤火虫

高渊还有观赏[萤火虫的地方](../Page/萤火虫.md "wikilink")。只要乘船去[高渊港口大约一公里外河边](../Page/高渊港口.md "wikilink")，就能观赏萤火虫了。但是，游客在观赏萤火虫时必须安静，以免吓走萤火虫。

### 高渊大街

高渊大街是高渊重要的道路。它可衔接至多个地方。高渊大街大约1公里。中间一排的路灯，造型独特。晚上时灯光闪闪，十分漂亮。

### 高渊卫星市

高渊卫星市是高渊其中一个花园，素有好吃街以及 “高渊夜市场"
之名称。高渊卫星市設有数家饮食中心，夜晚时十分热闹，提供多种食物选择。每逢星期日晚上，卫星市都会有华人夜市（Pasar
Malam），主要由当地华人经营。

### 百货公司

高渊一座百货公司——华达（YAWATA），是高渊最大的百货公司，前方有许多档口。 【G mart Supermarket】 是最新的百货超市，

## 花园

  - Taman Pancur Permai
  - Taman Bukit Panchor
  - Taman Pekaka
  - Taman Ilmu
  - Taman Minamah
  - Taman Nibong Tebal Jaya
  - Taman Seri Nibong
  - Taman Sentosa（圣淘沙花园）
  - Taman Berjaya（成功花园）
  - Taman Bersatu
  - Taman Helang Jaya
  - Taman Penting
  - Taman Sempadan
  - Taman Seri Emas
  - Taman Merbok
  - 东海园
  - 庆丰园
  - 礼园
  - 金玉园
  - Taman Bistari
  - Taman Seri Bistari
  - Taman Nuri
  - Taman Belatuk（美德花园）
  - Taman Tekukur Indah
  - Taman Panchor Mutiara
  - Taman Cowin
  - Taman Veerapen
  - Taman Permai（平安园）
  - Taman Sintar Indah
  - Taman Camar Jaya (高帝园）

## 图片库

## 参见

  - [爪夷村](../Page/爪夷村.md "wikilink")
  - [高渊港口](../Page/高渊港口.md "wikilink")
  - [万拉峇鲁](../Page/万拉峇鲁.md "wikilink")
  - [巴里文打](../Page/巴里文打.md "wikilink")

## 参考文献

  -
[Category:威南县](../Category/威南县.md "wikilink")
[Category:威省](../Category/威省.md "wikilink")
[Category:马来西亚市镇](../Category/马来西亚市镇.md "wikilink")

1.  [1](http://www.sinchew.com.my/node/776442)