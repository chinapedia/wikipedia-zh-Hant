[Golden_Computer_Center_2007.JPG](https://zh.wikipedia.org/wiki/File:Golden_Computer_Center_2007.JPG "fig:Golden_Computer_Center_2007.JPG")
[Golden_Computer_Center_Interior_2010.jpg](https://zh.wikipedia.org/wiki/File:Golden_Computer_Center_Interior_2010.jpg "fig:Golden_Computer_Center_Interior_2010.jpg")
**高登電腦中心**（；中文名稱「高登」[音譯自其英文名稱](../Page/音譯.md "wikilink")「Golden」。）是[香港著名的](../Page/香港.md "wikilink")[電腦產品](../Page/電腦.md "wikilink")[銷售](../Page/銷售.md "wikilink")[商場](../Page/商場.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[深水埗區](../Page/深水埗區.md "wikilink")[深水埗](../Page/深水埗.md "wikilink")[福華街](../Page/福華街.md "wikilink")146至152號1樓，上蓋物業為私人住宅黃金大廈。而**高登電腦廣場**（）一般被認為是高登電腦中心的新翼，兩個商場之間互通，卻各自有獨立的樓梯和扶手電梯上落，地面入口只有[桂林街一處](../Page/桂林街.md "wikilink")。高登電腦廣場於2007年年初才落成，為原來的高登33電腦廣場及銀都桌球城改建部份。

位置分佈上，高登電腦中心及高登電腦廣場位於上層（1樓），而[黃金商場則位於地下及地庫](../Page/黃金商場.md "wikilink")。高登電腦中心及高登電腦廣場主要售賣[電腦硬件](../Page/電腦硬件.md "wikilink")，黃金商場則主要售賣[電腦軟件](../Page/電腦軟件.md "wikilink")，週邊配件例如[CD-R](../Page/CD-R.md "wikilink")、[DVD-R](../Page/DVD-R.md "wikilink")、[滑鼠](../Page/滑鼠.md "wikilink")、[鍵盤](../Page/鍵盤.md "wikilink")、電腦[書籍](../Page/图书.md "wikilink")、電腦傢具、[電視及](../Page/電視.md "wikilink")[遊戲機](../Page/遊戲機.md "wikilink")，亦有一些電腦硬件店舖。下層的黃金商場與[台北同類型的](../Page/台北.md "wikilink")[光華商場十分相似](../Page/光華商場.md "wikilink")。三個商場過百家店舖形成了一個大型的電腦購物中心。

## 歷史

黃金大廈的商場在1970年代本是一個以時裝批發為主的商場，隨著香港電子業發達，加上鄰近電子貨品集中地[鴨寮街](../Page/鴨寮街.md "wikilink")，商場在1980年代開始變成銷售電腦為主。最早改建成電腦商場的是位於黃金戲院樓上的高登電腦中心，當時主要發售和電腦有關的電子零件及[蘋果電腦](../Page/蘋果電腦.md "wikilink")[Apple
II](../Page/Apple_II.md "wikilink")
-{兼容}-機。後來因為[香港政府推行電腦教育](../Page/香港政府.md "wikilink")，加上高登商場的名聲開始在社會上流傳，所以連地下本來賣時裝的黃金商場也轉型售賣電腦產品。到1980年代中，電視遊戲機及[IBM](../Page/IBM.md "wikilink")[桌上電腦](../Page/桌上電腦.md "wikilink")-{兼容}-機成為商場主要銷售的產品，而樓下的店舖亦開始售賣[盜版軟件](../Page/盜版軟件.md "wikilink")。1990年代，[香港海關大力打擊盜版軟件](../Page/香港海關.md "wikilink")，使賴以為生的非法經營必須從良。

## 文化

[Golden_Shopping_Center_2007.JPG](https://zh.wikipedia.org/wiki/File:Golden_Shopping_Center_2007.JPG "fig:Golden_Shopping_Center_2007.JPG")近攝\]\]

1990年代前，在全球市場上電腦的價格都非常高昂，所以著名廠家出產的[個人電腦在](../Page/個人電腦.md "wikilink")[香港社會上並不常見](../Page/香港社會.md "wikilink")，價錢昂貴促使很多[香港人在商場購買組裝電腦](../Page/香港人.md "wikilink")，以節省金錢。當時在黃金與高登銷售的組裝電腦被稱為“黃金嘜”，有些人認為黃金嘜價廉物美，又可以自由組合。1990年，一臺[Compaq出產的](../Page/Compaq.md "wikilink")[486DX33售價約](../Page/Intel_80486.md "wikilink")33,000港元，惟一部具備同等功能的“黃金嘜”則只需要8,000港元，為廠家出產的25%價錢。不過，對於不熟悉電腦的買家來說，由於黃金嘜店舖的經營手法良莠不齊，而且售後服務不足，使到他們不太願意前往購買組裝電腦。

## 高登討論區

1990年代後期，由於[互聯網在香港流行](../Page/互聯網.md "wikilink")，使到商店間的競爭帶到網上。為了保障顧客免於受騙，商場同業建立了一個網站，將電腦產品的市價在網站上公開。網站後來增設[討論區](../Page/討論區.md "wikilink")，並且慢慢演變成為了今時今日著名的[高登討論區](../Page/高登討論區.md "wikilink")。

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FF00FF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[深水埗站D](../Page/深水埗站.md "wikilink")2出口

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

## 參見

  - [黃金電腦商場](../Page/黃金電腦商場.md "wikilink")
  - [新高登電腦廣場](../Page/新高登電腦廣場.md "wikilink")
  - [福仁電腦商場](../Page/福仁電腦商場.md "wikilink")
  - [高登討論區](../Page/高登討論區.md "wikilink")
  - [香港電腦節](../Page/香港電腦節.md "wikilink")
  - [秋葉原](../Page/秋葉原.md "wikilink")
  - [華強北](../Page/華強北.md "wikilink")

## 外部連結

  - [香港高登](http://www.hkgolden.com/)
  - [黃金電腦商場官方網站](https://web.archive.org/web/20100405072545/http://www.goldenarcade.org/)
  - [新高登電腦商場官方網站](http://nccp.hk/)

[en:Sham Shui Po\#Golden Shopping
Centre](../Page/en:Sham_Shui_Po#Golden_Shopping_Centre.md "wikilink")

[Category:香港電腦商場](../Category/香港電腦商場.md "wikilink")
[Category:深水埗區商場](../Category/深水埗區商場.md "wikilink")