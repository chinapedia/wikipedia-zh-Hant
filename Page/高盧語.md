**高盧語**是一種已滅亡的[凱爾特語言](../Page/凱爾特語族.md "wikilink")，通行於[古羅馬帝國](../Page/羅馬帝國.md "wikilink")、[法蘭克人](../Page/法蘭克人.md "wikilink")、及不列顛[凱爾特人入侵前的](../Page/凱爾特人.md "wikilink")[高盧](../Page/高盧.md "wikilink")，從發現於法國及比利時各處的上百個墓碑、石柱、紀念碑了解這種語言，其中最著名的，就是發現於法國與瑞士邊界
Ain 河畔的科利尼曆法，是[凱爾特民族曆法史料的一大發現](../Page/凱爾特人.md "wikilink")。

## 歷史

高盧語是一種[大陸凱爾特語言](../Page/大陸凱爾特語支.md "wikilink")，有著相當複雜的曲折語形，約有六或七種的[詞形變化](../Page/詞形變化.md "wikilink")，由法國歷史學家
A. Lot
等人重建，被認為與[拉丁語相當相似](../Page/拉丁語.md "wikilink")，使當初的高盧人在學習拉丁語時毫不費力，並很快地就採納了後者。然而這種說法還有相當多的爭議。

高盧語並沒有像某些人認為的如此快地消逝。事實上，高盧語存在了幾世紀，才被之後盛行的拉丁語完全取代；在某些地區，它甚至殘存到法蘭克人入侵的時間。[第六世紀時](../Page/六世紀.md "wikilink")，杜爾的聖格列高利也記載到，他居住的地方，有些人們還是能說高盧語。直到今天，我们还可以从很多法国地名中找到高卢语的痕迹，比如[里昂](../Page/里昂.md "wikilink")(Lyon)来自Lugdunum，[雷恩](../Page/雷恩_\(伊勒-维莱讷省\).md "wikilink")(Rennes)来自高卢部落名Redones。

從足夠的高盧語碑銘及文稿得知，高盧語屬於一種 P 凱爾特語支的語言，這意味著當另個語支 Q 凱爾特語支的凱爾特語言保留了早先印歐語系的子音
*kw* 或 *k* 時，高盧語已將它們轉為 *p*，也因此高盧語的「兒子」是 \**mabos* 或 \**mapos*，而其他 Q
凱爾特語支的「兒子」是 *maccos* 或 *maqqos*（自歐甘銘文上取料）。

被記載下的高盧語都是由外來者帶來的[字母所寫成](../Page/字母.md "wikilink")，[希臘字母](../Page/希臘字母.md "wikilink")、[拉丁字母](../Page/拉丁字母.md "wikilink")、和愛屈利亞字母都曾被使用於書寫高盧語，而在不列顛群島上發現的[歐甘銘文則不曾發現被使用於書寫此種語言](../Page/歐甘銘文.md "wikilink")。

[布列塔尼語是在](../Page/布列塔尼語.md "wikilink")[布列塔尼使用的另一種凱爾特語言](../Page/布列塔尼.md "wikilink")，此種語言也受到高盧語的些許影響，保留了高盧語的一些特徵。

## 科利尼曆法

現存最長且連續的高盧文語料是發現於[法國](../Page/法國.md "wikilink")[里昂附近的](../Page/里昂.md "wikilink")[科利尼](../Page/科利尼.md "wikilink")[曆法](../Page/曆法.md "wikilink")。科利尼曆法和一個後來被辨識為太陽神[阿波羅的石像一起被發現](../Page/阿波羅.md "wikilink")（有人認為是戰神[瑪爾斯](../Page/瑪爾斯.md "wikilink")）。

這個科利尼曆法上面記載了當時高盧人使用的月曆，並將全年分為兩大部分，分別以 SAMON 和 GIAMON 起始；SAMON 代表
Samonios「夏季」（[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：*Samhradh*；[威爾斯語](../Page/威爾斯語.md "wikilink")：*Haf*），意思是「落種」；GIAMON
代表 Giamonios「冬季」（愛爾蘭語：*Geamhradh*；威爾斯語：*Gaeaf*），意思是「射雪」。

只有一些像祭典或節慶這樣重要且特別的日子被特別命名，例如其中一個 SAMON- xvii 代表 TRINVX SAMO SINDIV，被解讀為
Trinouxtion Samonii sindiu 的縮寫，意思是「今日是夏季的"三夜期"（節慶或祭典名）」。

## 相關條目

  - [高盧](../Page/高盧.md "wikilink")

## 外部連結

  - [L.A. Curchin, "Gaulish
    language"](https://web.archive.org/web/20180218100057/http://www.orbilat.com/Encyclopaedia/G/Gaulish_language.html)
  - [Gaulish
    language](https://web.archive.org/web/20050310221937/http://indoeuro.bizland.com/tree/celt/gaulish.html)
  - [Samhain: Season of Death and
    Renewal](https://web.archive.org/web/20050214211607/http://www.jaguarmoon.org/public/Wheel/Samhain/Kondratiev.htm)
  - [The Coligny Calendar](http://technovate.org/web/coligny.htm)

[Category:大陸凱爾特語支](../Category/大陸凱爾特語支.md "wikilink")
[Category:已灭亡语言](../Category/已灭亡语言.md "wikilink")
[Category:法國語言](../Category/法國語言.md "wikilink")
[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")
[Category:高盧](../Category/高盧.md "wikilink")