**高砂市**（）是位於[日本](../Page/日本.md "wikilink")[兵庫縣南部](../Page/兵庫縣.md "wikilink")，臨[播磨灘的城市](../Page/播磨灘.md "wikilink")，由轄區東南側流過，主要市區位於加古川河口西側的平原，北部區域則是山地，臨海區域則是工業區。

## 歷史

在[鎌倉時代在現在的荒井町地區開始設有](../Page/鎌倉時代.md "wikilink")[鹽田生產](../Page/鹽田.md "wikilink")[鹽](../Page/鹽.md "wikilink")；[室町時代](../Page/室町時代.md "wikilink")在此築有高砂城，並在[戰國時代因為屬於](../Page/戰國時代.md "wikilink")的支城而被捲入[三木合戰](../Page/三木合戰.md "wikilink")。

[江戶時代屬於](../Page/江戶時代.md "wikilink")[姬路藩的領地](../Page/姬路藩.md "wikilink")，由於位於出海口，成加古川流域貨物的集散港口。\[1\]

海岸區域在明治維新後則成為工業區，在[二次大戰期間也成為軍需產業的要地](../Page/二次大戰.md "wikilink")。

1889年日本實施[町村制時](../Page/町村制.md "wikilink")，設置了，隸屬[加古郡](../Page/加古郡.md "wikilink")；1954年，高砂町與、[印南郡](../Page/印南郡.md "wikilink")、合併為**高砂市**，隔年再併入位於西側的，形成現在高砂市的轄區範圍。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1926年</p></th>
<th><p>1926年-1944年</p></th>
<th><p>1944年-1954年</p></th>
<th><p>1954年-1989年</p></th>
<th><p>1989年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>高砂町</p></td>
<td><p>1954年7月1日<br />
高砂市</p></td>
<td><p>高砂市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>荒井村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>曾根村</p></td>
<td><p>曾根町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>伊保村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>北濱村</p></td>
<td><p>1957年3月10日<br />
併入高砂市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [山陽本線](../Page/山陽本線.md "wikilink")（[JR神戶線](../Page/JR神戶線.md "wikilink")）：（←[加古川市](../Page/加古川市.md "wikilink")）
        -  -  - （[姬路市](../Page/姬路市.md "wikilink")→）
      - [山陽新幹線](../Page/山陽新幹線.md "wikilink")：（←加古川市） - （通過轄區內但未設有車站） -
        （姬路市→）
  - [山陽電氣鐵道](../Page/山陽電氣鐵道.md "wikilink")
      - [本線](../Page/山陽電氣鐵道本線.md "wikilink")：（←加古川市） -  -  -  -  - （姬路市）
        - （約150公尺路段再次通過高砂市轄區但未設有車站） - （姬路市）

過去還曾有[日本國有鐵道](../Page/日本國有鐵道.md "wikilink")由加古川市的通往市內高砂町地區，但已於[1984年停止營運](../Page/1984年.md "wikilink")。

<File:JR> Sone sta 01.jpg|曾根車站
[File:SY-TakasagoStation.jpg|高砂車站](File:SY-TakasagoStation.jpg%7C高砂車站)
<File:Sanyo-Arai> Station north entrance.jpg|荒井車站 <File:Iho>
Station.jpg|伊保車站 <File:Sanyo> Sone Station 03.jpg|山陽曾根車站

### 公路

  - 一般國道

<!-- end list -->

  - [國道2號](../Page/國道2號_\(日本\).md "wikilink")、[國道250號](../Page/國道250號.md "wikilink")

## 觀光資源

  - 及：國家史跡

  -
  - 高砂地區歴史的景觀形成地區：兵庫縣指定景觀形成重要建造物

## 本地出身之名人

  - 體育

<!-- end list -->

  - [鶴岡一成](../Page/鶴岡一成.md "wikilink")：棒球選手
  - [真田裕貴](../Page/真田裕貴.md "wikilink")：棒球選手
  - [妙義龍泰成](../Page/妙義龍泰成.md "wikilink"):[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")

<!-- end list -->

  - 演藝

<!-- end list -->

  - [北野勇作](../Page/北野勇作.md "wikilink")：小說作家
  - [速水奨](../Page/速水奨.md "wikilink")：[聲優](../Page/聲優.md "wikilink")

<!-- end list -->

  - 學術

<!-- end list -->

  - [美濃部達吉](../Page/美濃部達吉.md "wikilink")：法學學者、曾任[貴族院議員](../Page/貴族院.md "wikilink")

<!-- end list -->

  - 政治

<!-- end list -->

  - [渡海元三郎](../Page/渡海元三郎.md "wikilink")：曾任眾議院議員、[建設大臣](../Page/建設大臣.md "wikilink")
  - [渡海紀三朗](../Page/渡海紀三朗.md "wikilink")：渡海元三郎之子，曾任眾議院議員、[文部科學大臣](../Page/文部科學大臣.md "wikilink")

## 參考資料

## 相關條目

## 外部連結

  -

  - [高砂市觀光協會](http://www.takasago-kanko.com/)

<!-- end list -->

1.