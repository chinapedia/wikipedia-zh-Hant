**高罗佩**（，），字笑忘，号芝台、吟月庵主，[荷兰](../Page/荷兰.md "wikilink")[汉学家](../Page/汉学家.md "wikilink")、[东方学家](../Page/东方学家.md "wikilink")、[外交官](../Page/外交官.md "wikilink")、[翻译家](../Page/翻译家.md "wikilink")、[小说家](../Page/小说家.md "wikilink")。

## 生平

[高羅佩和水世芳.jpg](https://zh.wikipedia.org/wiki/File:高羅佩和水世芳.jpg "fig:高羅佩和水世芳.jpg")
1910年，生于[荷兰](../Page/荷兰.md "wikilink")[聚特芬](../Page/聚特芬.md "wikilink")（Zutphen）。

1915年－1923年，随父母侨居[印度尼西亚](../Page/印度尼西亚.md "wikilink")[巴达维亚](../Page/雅加达.md "wikilink")。少年高罗佩在印度尼西亚学会[汉语](../Page/汉语.md "wikilink")、[爪哇语和](../Page/爪哇语.md "wikilink")[马来语](../Page/马来语.md "wikilink")。

1929年－1934年，高罗佩在[荷兰](../Page/荷兰.md "wikilink")[莱頓大学和](../Page/莱頓大学.md "wikilink")[乌特勒支大学攻读荷属东印度法律和](../Page/乌特勒支大学.md "wikilink")[印尼文化](../Page/印尼.md "wikilink")，兼修[中文](../Page/中文.md "wikilink")、[藏文](../Page/藏文.md "wikilink")、[印度文](../Page/印度文.md "wikilink")、[日文](../Page/日文.md "wikilink")。

高罗佩在1935年，入[荷兰](../Page/荷兰.md "wikilink")[外交部工作](../Page/外交部.md "wikilink")，先后派驻[日本](../Page/日本.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[中国](../Page/中国.md "wikilink")、[印度](../Page/印度.md "wikilink")、[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[美国](../Page/美国.md "wikilink")、[印度和](../Page/印度.md "wikilink")[马来西亚](../Page/马来西亚.md "wikilink")。在中國期間師從[關仲航學習](../Page/關仲航.md "wikilink")[古琴](../Page/古琴.md "wikilink")。\[1\]

1943年，娶[水世芳为妻子](../Page/水世芳.md "wikilink")。

1943年－1945年，在中国[重庆任](../Page/重庆.md "wikilink")第一秘书。

1965年－1967年，任荷兰驻日大使。

1967年，高罗佩因[癌症在](../Page/癌症.md "wikilink")[荷兰](../Page/荷兰.md "wikilink")[海牙逝世](../Page/海牙.md "wikilink")。

## 著作

### 狄公案

1949年高罗佩将中国8世纪以[狄仁杰为主人公的小说](../Page/狄仁杰.md "wikilink")《[狄公案](../Page/狄公案.md "wikilink")》翻译成英文在[东京出版](../Page/东京.md "wikilink")。后来索性自己以狄仁杰为主人公创作推理小说集《[大唐狄公案](../Page/大唐狄公案.md "wikilink")》。

### 学术专著

奠定高罗佩学术地位的是他的一系列[汉学专著](../Page/汉学.md "wikilink")：

  - 《[秘戏图考](../Page/秘戏图.md "wikilink") 附论汉代至清代的中国性生活》（删节本） 杨权译 广东人民出版社
    ISBN 7-218-00952-2（*Erotic Colour Prints of the Ming Period*,
    Privately printed, Tokyo, 1951）
  - 《中国古代[房内考](../Page/房中术.md "wikilink")》 上海人民出版社 1990 ISBN
    7-208-00642-3（*Sexual Life in Ancient China: A Preliminary Survey of
    Chinese Sex and Society from ca. 1500 B.C. Till 1644 A.D.*, 1961）
  - 同上台湾版 《中國艷情-東方愛典》 台北風雲時代出版社 2004年 ISBN：, 9861460616
  - 《明末义僧东皋禅师集刊》（*Collected Writings of the Ch'an Master Tung-kao, a
    Loyal Monk of the End of the Ming Period*, Chongqing, 1944）
  - 《中国琴道》（*The Lore of the Chinese Lute: An Essay in Ch'in Ideology*,
    Tokyo, 1941）
  - 《嵇康及其琴赋》（*Hsi K'ang and His Poetical Essay on the Lute*, Tokyo,
    1941）
  - 《中国漆屏风》
  - 《中国绘画鉴赏》（*Chinese Pictorial Art, as Viewed by the Connoisseur*,
    Limited edition of 950 copies, Rome, 1958）
  - 《砚史》
  - 《中国长臂猿》（*The Gibbon in China: An Essay in Chinese Animal Lore*,
    Leiden, 1967)）
  - 《中日梵文研究中论》（*Siddham: An Essay on the History of Sanskrit Studies in
    China and Japan*, 1956）

另词典一部：

  - 《英语-乌足语词典》（*A Blackfoot-English Vocabulary Based on Material from
    the Southern Peigans, with Christianus Cornelius Uhlenbeck*,
    Amsterdam, 1934）

## 参考文献

## 外部链接

  - [Janwillem van de
    Wetering](../Page/Janwillem_van_de_Wetering.md "wikilink");
    <cite>Robert van Gulik: Zijn Leven Zijn Werk</cite>; Loeb,
    Amsterdam, ISBN 90-6213-899-3 (Hardback, First edition 1989)
  - Janwillem van de Wetering; <cite>Robert van Gulik: His Life His
    Work</cite>; Soho Press, Inc.; ISBN 1-56947-124-X
  - C.D. Barkman, H. de Vries-van der Hoeven; Een man van drie levens (A
    man of three lives); Forum, Amsterdam, ISBN 90-225-1650-4 (1993)
  - [John Thompson's collected anecdotes on van Gulik's Guqin
    artistry](http://www.silkqin.com/10ideo/vgulik.htm)
  - [*Judge Dee* website](http://www.judge-dee.info/)
  - [The Dutch language *Rechter Tie*
    website](http://www.rechtertie.nl/)

## 参见

  - 《[大唐狄公案](../Page/大唐狄公案.md "wikilink")》

[Category:荷兰作家](../Category/荷兰作家.md "wikilink")
[Category:荷兰汉学家](../Category/荷兰汉学家.md "wikilink")
[Category:中文—英文翻译家](../Category/中文—英文翻译家.md "wikilink")
[Category:荷兰外交官](../Category/荷兰外交官.md "wikilink")
[Category:烏特勒支大學校友](../Category/烏特勒支大學校友.md "wikilink")
[Category:萊頓大學校友](../Category/萊頓大學校友.md "wikilink")
[Category:古琴演奏家](../Category/古琴演奏家.md "wikilink")
[Category:在中國的荷蘭人](../Category/在中國的荷蘭人.md "wikilink")
[Category:荷蘭駐日本大使](../Category/荷蘭駐日本大使.md "wikilink")
[Category:荷兰驻韩国大使](../Category/荷兰驻韩国大使.md "wikilink")
[Category:20世纪翻译家](../Category/20世纪翻译家.md "wikilink")

1.