**高美燈塔**是一座位於[臺灣](../Page/臺灣.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[清水區西北隅高美里的](../Page/清水區_\(臺灣\).md "wikilink")[燈塔](../Page/灯塔.md "wikilink")，該地位處[大甲溪出海口南岸及](../Page/大甲溪.md "wikilink")[高美濕地旁](../Page/高美野生動物保護區.md "wikilink")。該燈塔興建於1967年，塔高34.4公尺，燈高38.7公尺，[光程](../Page/光程.md "wikilink")16.2[浬](../Page/浬.md "wikilink")。2014年9月27日，經[交通部航港局翻修後開始開放參觀](../Page/交通部航港局.md "wikilink")\[1\]。

高美燈塔西側海岸為高美野生動物保護區（高美濕地），西南遠處為[台中港](../Page/台中港.md "wikilink")[風力發電白色風車](../Page/風力發電.md "wikilink")，東側有大甲[鐵砧山](../Page/鐵砧山.md "wikilink")，向東北望去即[火炎山](../Page/火炎山_\(苗栗縣\).md "wikilink")。

## 特色

高美燈塔塔身建築與[芳苑燈塔相似](../Page/芳苑燈塔.md "wikilink")，為[八角形的](../Page/八角形.md "wikilink")[鋼筋](../Page/鋼筋.md "wikilink")[混凝土之](../Page/混凝土.md "wikilink")[建築物](../Page/建築物.md "wikilink")，是[臺灣唯一外觀呈](../Page/臺灣.md "wikilink")[紅](../Page/紅色.md "wikilink")、[白相間且平行條紋的燈塔](../Page/白色.md "wikilink")，塔高：34.4公尺。但於1982年停止運作，並將燈器拆除移至[臺中港燈塔使用](../Page/臺中港燈塔.md "wikilink")。

## 燈塔行政諸元

  - 管轄機關：由[交通部航港局管轄](../Page/交通部航港局.md "wikilink")。
  - 人員編制：未知。
  - 開放參觀，參觀時間於不同季節分別為9時至18時（4月至10月）、9時至17時（11月至3月），逢星期一不開放。
  - 電話：04-26113924。

## 關連項目

  - [台灣燈塔列表](../Page/台灣燈塔列表.md "wikilink")
  - [高美濕地](../Page/高美野生動物保護區.md "wikilink")

## 註釋

## 參考文獻

1.  李素芳
    編著，《台灣的燈塔》，遠足文化事業，[臺北縣](../Page/新北市.md "wikilink")，2002年12月，pp. 88–91。ISBN
    957-28031-2-3
2.  葉倫會
    著，《海洋領航者－台灣燈塔展》，[高雄市立歷史博物館](../Page/高雄市立歷史博物館.md "wikilink")，[高雄市](../Page/高雄市.md "wikilink")，2000年12月，pp. 40–41。ISBN
    957-02-7455-7
3.  財政部關稅總局編撰，《中華民國海關簡史》，[台北市](../Page/台北市.md "wikilink")，1998年7月。ISBN
    9570048611

## 外部連結

  - [高美燈塔](http://travel.taichung.gov.tw/zh-tw/Attractions/Intro/948/高美燈塔)（臺中觀光旅遊網）
  - [台灣海關博物館](https://web.archive.org/web/20090602040949/http://museum.customs.gov.tw/english/index.htm)

[G](../Category/台灣燈塔.md "wikilink")
[G](../Category/台中市旅遊人文景點.md "wikilink") [Category:清水區
(臺灣)](../Category/清水區_\(臺灣\).md "wikilink")
[Category:1967年完工建築物](../Category/1967年完工建築物.md "wikilink")

1.  [「高美燈塔」27日重啟開幕](http://www.taichung.gov.tw/ct.asp?xItem=1260533&ctNode=712&mp=100010)，臺中市新聞局，2014-09-23