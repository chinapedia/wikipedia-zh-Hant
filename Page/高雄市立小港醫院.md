**高雄市立小港醫院**位於[高雄市](../Page/高雄市.md "wikilink")[小港區](../Page/小港區_\(高雄市\).md "wikilink")，被[行政院衛生署評鑑為](../Page/中華民國衛生福利部.md "wikilink")「區域教學醫院」，現任院長為[郭昭宏先生](../Page/郭昭宏.md "wikilink")，目前[高雄市政府委託](../Page/高雄市政府.md "wikilink")[高雄醫學大學經營](../Page/高雄醫學大學.md "wikilink")。

## 交通

  - [高雄捷運](../Page/高雄捷運.md "wikilink")[紅線](../Page/高雄捷運紅線.md "wikilink")[小港站](../Page/小港站.md "wikilink")4號出口出站後，於**小港轉運站**轉乘下列[高雄市公車路線即可抵達](../Page/高雄市公車.md "wikilink")
      - 30
      - 62
      - 69A
      - 69B
      - 紅1(R1)
      - 紅2A(R2A)
      - 紅2B(R2B)
      - 紅2C(R2C)
      - 紅2D(R2D)
      - 紅3A(R3A)
      - 紅3B(R3B)
      - 紅3C(R3C)
      - 紅3E(R3E)
      - 紅3F(R3F)
      - 紅8A(R8A)
      - 紅8B(R8B)
      - 紅8C(R8C)
      - 紅8D(R8D)
      - 紅8E(R8E)

## 參見

  - [臺灣醫院列表](../Page/臺灣醫院列表.md "wikilink")
  - [中華民國公立醫院列表](../Page/中華民國公立醫院列表.md "wikilink")
  - [高雄醫學大學](../Page/高雄醫學大學.md "wikilink")

## 外部連結

  - [高雄醫學大學](http://www.kmu.edu.tw/)
  - [高雄市立小港醫院](http://www.kmsh.org.tw/)

[K](../Category/高雄市醫院.md "wikilink")
[B](../Category/台灣區域醫院.md "wikilink")
[Category:高雄醫學大學](../Category/高雄醫學大學.md "wikilink")
[Category:高雄市政府](../Category/高雄市政府.md "wikilink")