**鬚角鮟鱇科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮟鱇目的其中一科](../Page/鮟鱇目.md "wikilink")。

## 分類

**鬚角鮟鱇科**下分5個屬，如下：

### 無刺鮟鱇屬（*Acentrophryne*）

  - [長絲無刺鮟鱇](../Page/長絲無刺鮟鱇.md "wikilink")（*Acentrophryne dolichonema*）
  - [長牙無刺鮟鱇](../Page/長牙無刺鮟鱇.md "wikilink")（*Acentrophryne longidens*）

### 貪口樹鬚魚屬（*Borophryne*）

  - [貪口樹鬚魚](../Page/貪口樹鬚魚.md "wikilink")（*Borophryne apogon*）：又稱貪口鬚鮟鱇。

### 獨樹鬚魚屬（*Haplophryne*）

  - [獨樹鬚魚](../Page/獨樹鬚魚.md "wikilink")（*Haplophryne mollis*）：又稱獨鬚鮟鱇。
  - [皇獨樹鬚魚](../Page/皇獨樹鬚魚.md "wikilink")（*Haplophryne
    triregium*）：又稱皇獨鬚鮟鱇。

### 樹鬚魚(鬚鮟鱇)屬（*Linophryne*）

  - [藻髯樹鬚魚](../Page/藻髯樹鬚魚.md "wikilink")（*Linophryne
    algibarbata*）：又稱藻髯鬚鮟鱇。
  - [安氏樹鬚魚](../Page/安氏樹鬚魚.md "wikilink")（*Linophryne
    andersoni*）：又稱安氏鬚鮟鱇。
  - [樹鬚魚](../Page/樹鬚魚.md "wikilink")（*Linophryne arborifera*）：又稱新西蘭鬚鮟鱇。
  - [阿氏樹鬚魚](../Page/阿氏樹鬚魚.md "wikilink")（*Linophryne arcturi*）：又稱阿氏鬚鮟鱇。
  - [銀樹鬚魚](../Page/銀樹鬚魚.md "wikilink")（*Linophryne argyresca*）：又稱銀鬚鮟鱇。
  - [雙角樹鬚魚](../Page/雙角樹鬚魚.md "wikilink")（*Linophryne bicornis*）：又稱雙角鬚鮟鱇。
  - [雙翼樹鬚魚](../Page/雙翼樹鬚魚.md "wikilink")（*Linophryne
    bipennata*）：又稱雙翼鬚鮟鱇。
  - [短髭樹鬚魚](../Page/短髭樹鬚魚.md "wikilink")（*Linophryne
    brevibarbata*）：又稱短髭鬚鮟鱇。
  - [球狀樹鬚魚](../Page/球狀樹鬚魚.md "wikilink")（*Linophryne coronata*）：又稱球狀鬚鮟鱇。
  - [長杆樹鬚魚](../Page/長杆樹鬚魚.md "wikilink")（*Linophryne
    densiramus*）：又稱長杆鬚鮟鱇。
  - [趾杆樹鬚魚](../Page/趾杆樹鬚魚.md "wikilink")（*Linophryne digitopogon*）
  - [夏威夷樹鬚魚](../Page/夏威夷樹鬚魚.md "wikilink")（*Linophryne
    escaramosa*）：又稱夏威夷鬚鮟鱇。
  - [印度鬚角鮟鱇](../Page/印度鬚角鮟鱇.md "wikilink")（*Linophryne indica*）：又稱印度樹鬚魚。
  - [燈籠樹鬚魚](../Page/燈籠樹鬚魚.md "wikilink")（*Linophryne lucifer*）：又稱燈籠鬚鮟鱇。
  - [大齒樹鬚魚](../Page/大齒樹鬚魚.md "wikilink")（*Linophryne macrodon*）：又稱大齒鬚鮟鱇。
  - [馬德拉樹鬚魚](../Page/馬德拉樹鬚魚.md "wikilink")（*Linophryne
    maderensis*）：又稱馬德拉鬚鮟鱇。
  - [珀氏樹鬚魚](../Page/珀氏樹鬚魚.md "wikilink")（*Linophryne parini*）：又稱珀氏鬚鮟鱇。
  - [羽髯樹鬚魚](../Page/羽髯樹鬚魚.md "wikilink")（*Linophryne
    pennibarbata*）：又稱羽髯鬚鮟鱇。
  - [多鬚樹鬚魚](../Page/多鬚樹鬚魚.md "wikilink")（*Linophryne
    polypogon*）：又稱多鬚鬚鮟鱇。
  - [五岐樹鬚魚](../Page/五岐樹鬚魚.md "wikilink")（*Linophryne
    quinqueramosa*）：又稱五岐鬚鮟鱇。
  - [莖樹鬚魚](../Page/莖樹鬚魚.md "wikilink")（*Linophryne racemifera*）：又稱莖鬚鮟鱇。
  - [六線樹鬚魚](../Page/六線樹鬚魚.md "wikilink")（*Linophryne sexfilis*）：又稱六線鬚鮟鱇。
  - [特氏樹鬚魚](../Page/特氏樹鬚魚.md "wikilink")（*Linophryne
    trewavasae*）：又稱特氏鬚鮟鱇。

### 光棒鮟鱇屬（*Photocorynus*）

  - [棘頭光棒鮟鱇](../Page/棘頭光棒鮟鱇.md "wikilink")（*Photocorynus spiniceps*）

[\*](../Category/鬚角鮟鱇科.md "wikilink")