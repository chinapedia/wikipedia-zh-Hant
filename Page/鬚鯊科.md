**鬚鯊科**（學名Orectolobidae）是[鬚鮫目中](../Page/鬚鮫目.md "wikilink")**鬚鮫亞目**的其中一[科](../Page/科.md "wikilink")。

## 分布

鬚鯊生活在西[太平洋和东](../Page/太平洋.md "wikilink")[印度洋热带温和的浅水中](../Page/印度洋.md "wikilink")，主要在[澳大利亚和](../Page/澳大利亚.md "wikilink")[印度尼西亚水域中](../Page/印度尼西亚.md "wikilink")，一个种也生活在[日本海域中](../Page/日本.md "wikilink")（[日本鬚鯊](../Page/日本鬚鯊.md "wikilink")
*Orectolobus japonicus*）。

## 深度

水深3至110公尺。

## 特徵

鬚鯊體呈圓筒狀或肥厚而平扁。口為短橫裂，鼻口間溝發達，口角有唇摺。鰓裂小或中等，後方第一至第三鰓裂在胸鰭基底上方。背鰭2枚，第一背鰭遠較尾鰭為短，其起點在腹鰭上方或略後。臀鰭在第二背鰭下方。尾鰭長或短，後緣部為彎月狀，後端下方有缺刻，尾基無凹窪。

## 分類

**鬚鯊科**下分3個屬12種，如下\[1\]：

  - 鬚鯊属（*Orectolobus*）
    <small>[Bonaparte](../Page/Charles_Lucien_Bonaparte.md "wikilink"),
    1834</small>
      - （*Orectolobus
        floridus*）<small>[Last](../Page/Peter_R._Last.md "wikilink") &
        [Chidlow](../Page/Justin_A._Chidlow.md "wikilink"), 2008</small>

      - [哈氏鬚鲨](../Page/哈氏鬚鲨.md "wikilink")（*Orectolobus
        halei*）<small>[Whitley](../Page/Gilbert_Percy_Whitley.md "wikilink"),
        1940</small>.\[2\]

      - （*Orectolobus hutchinsi*）<small>Last, Chidlow & Compagno,
        2006</small>.\[3\]

      - [日本鬚鯊](../Page/日本鬚鯊.md "wikilink")（*Orectolobus
        japonicus*）<small>Regan, 1906</small>：又稱日本鬚鮫。

      - （*Orectolobus leptolineatus*）<small>Last,
        [Pogonoski](../Page/John_J._Pogonoski.md "wikilink") & [W. T.
        White](../Page/William_T._White.md "wikilink"), 2010</small>

      - [斑点鬚鯊](../Page/斑点鬚鯊.md "wikilink")（*Orectolobus
        maculatus*）<small>[Bonnaterre](../Page/Pierre_Joseph_Bonnaterre.md "wikilink"),
        1788</small>：又稱斑紋鬚鯊、班鬚鮫。

      - [妆饰鬚鯊](../Page/妆饰鬚鯊.md "wikilink")（*Orectolobus
        ornatus*）<small>[De
        Vis](../Page/Charles_Walter_De_Vis.md "wikilink"), 1883</small>

      - （*Orectolobus parvimaculatus*）<small>Last & Chidlow,
        2008</small>

      - （*Orectolobus reticulatus*）<small>Last, Pogonoski & W. T. White,
        2008</small>

      - [北澳大利亚鬚鯊](../Page/北澳大利亚鬚鯊.md "wikilink")（*Orectolobus
        wardi*）<small>Whitley, 1939</small>
  - 鞋头鲨属（*Sutorectus*）<small>Whitley, 1939</small>：又稱疣背鬚鯊屬
      - [鞋头鲨](../Page/鞋头鲨.md "wikilink")（*Sutorectus
        tentaculatus*）<small>[W. K. H.
        Peters](../Page/Wilhelm_Peters.md "wikilink"),
        1864</small>：又稱疣背鬚鯊。
  - 葉鬚鯊屬(*Eucrossorhinus*)
    <small>[Regan](../Page/Charles_Tate_Regan.md "wikilink"),
    1908</small>
      - [葉鬚鯊](../Page/葉鬚鯊.md "wikilink")(*Eucrossorhinus dasypogon*)
        <small>[Bleeker](../Page/Pieter_Bleeker.md "wikilink"),
        1867</small>

[化石屬包括](../Page/化石.md "wikilink")：

  - *[Eometlaouia](../Page/Eometlaouia.md "wikilink")* <small>Noubhani &
    Cappetta, 2002</small>

## 生態

鬚鯊生活在海底，大多数时间它们静沉在海底，往往附于岩石和海草之间。最大的鬚鯊是[斑点鬚鯊](../Page/斑点鬚鯊.md "wikilink")（*Orectolobus
maculatus*），可以长达3.2米。鬚鯊身上大块的、对称的图案使它们看上去像[地毯](../Page/地毯.md "wikilink")，为它们提供了很好的[伪装](../Page/伪装.md "wikilink")。此外它们的嘴边的皮肤上还有小植物似的突出物，更加改善了它们的伪装。鬚鯊依靠这样的伪装躲藏在岩石间，袭击游得太近的小鱼。

鬚鯊不食人，假如它们不受到挑衅的话也不造成威胁。有人不小心在浅水踩到鬚鯊後被咬。潜水的人假如对它们进行挑衅或者抓它们，或者将它们的逃路切断的话也会被咬。鬚鯊非常灵活，它们可以轻而易举地咬抓着它们的尾部的手。鬚鯊有许多小的、尖的牙齿，被它们咬可以受重伤，即使穿潜水衣业有可能受重伤。鬚鯊咬住后不肯松口，很难摆脱它们。为了避免被咬在潜水时不要挑衅或围绕鬚鯊，注意海底，当心不要踩到没有看见的鬚鯊上。

## 經濟利用

雖然鬚鯊不吃人，但它们经常被人吃。鬚鯊肉经常被做成[鱼片](../Page/鱼片.md "wikilink")，澳大利亚的[炸鱼薯条常用鬚鯊肉](../Page/炸鱼薯条.md "wikilink")。鬚鯊的皮被用来做[皮革](../Page/皮革.md "wikilink")。

## 参考文献

  - [FishBase](http://fishbase.sinica.edu.tw/)
  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[\*](../Category/鬚鯊科.md "wikilink")
[Category:鯊魚](../Category/鯊魚.md "wikilink")

1.
2.
3.