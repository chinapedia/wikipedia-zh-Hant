**鬥吻角龍屬**（[屬名](../Page/屬.md "wikilink")：），是種小型[角龍下目](../Page/角龍下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於晚[白堊紀的](../Page/白堊紀.md "wikilink")[坎潘階](../Page/坎潘階.md "wikilink")。化石發現於[蒙大拿州的](../Page/蒙大拿州.md "wikilink")。

鬥吻角龍的[模式種是](../Page/模式種.md "wikilink")**霍斯基氏鬥吻角龍**（），是由[美國的兩位](../Page/美國.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")：[布蘭達·金納利](../Page/布蘭達·金納利.md "wikilink")（Brenda
J. Chinnery）與（Jack R.
Horner）在2007年敘述、命名。屬名在[拉丁文意為](../Page/拉丁文.md "wikilink")「紅色的面孔」；種名則是取自化石發現者
Wilosn Hodgskiss 的姓氏。[正模標本](../Page/正模標本.md "wikilink")（編號MOR
300）的完整度高達80%。鬥吻角龍屬於角龍下目，角龍下目是群[草食性恐龍](../Page/草食性.md "wikilink")，具有喙嘴，生存於白堊紀時期的[北美洲與](../Page/北美洲.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")。鬥吻角龍是種基底[新角龍類](../Page/新角龍類.md "wikilink")，但詳細的分類則不確定；牠們一度被類於[纖角龍科](../Page/纖角龍科.md "wikilink")，但其他研究則認為牠們是纖角龍科的姐妹分類。\[1\]

## 參考資料

## 外部連結

  - [在蒙大拿州找到恐龍的失落環節](http://news.nationalgeographic.com/news/2007/10/071002-dinosaur-fossil.html)
    - [國家地理雜誌報導](../Page/國家地理雜誌.md "wikilink")。網頁中的骨骸是蒙大拿角龍。

[Category:冠飾角龍類](../Category/冠飾角龍類.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")

1.