<table>
<thead>
<tr class="header">
<th><p>鬥牛獒</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bullmastiff_edited.JPG" title="fig:Bullmastiff_edited.JPG">Bullmastiff_edited.JPG</a></p></td>
</tr>
<tr class="even">
<td><p>英文名</p></td>
</tr>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td><p>Bullmastiff</p></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td><p>原產地</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英國.md" title="wikilink">英國</a></p></td>
</tr>
</tbody>
</table>

**鬥牛獒**（）是一種大型的犬種。其正式記載的歷史大約從1860年的[英國開始](../Page/英國.md "wikilink")，有部分繁殖者相信在這年期之前數世纪，已經出現。在當時[英國的貴族](../Page/英國.md "wikilink")，會在他們巨大的莊園裏建立獵場，同時飼養大量動物作為狩獵用途。而當時的社會環境相當艱難，貧窮的人根本沒有金錢購買肉類作為食物。祇能偷偷地在這些貴族的獵場盜獵動物。當時的[法律偷獵是一項嚴重罪行](../Page/法律.md "wikilink")，會被判處[死刑](../Page/死刑.md "wikilink")，或送往國外。但亦不能阻止偷獵的活動。盗獵者亦都明白被捉拿後的懲罰，所以會對獵場守衛作出強烈反抗。經常都會發生人命傷亡事件。而獵場守衛為了保謢自己生命安全，唯有利用[犬隻來守護](../Page/犬.md "wikilink")。當時一般貴族都會飼養一些大型犬，保护他們的堡壘，由其喜愛一些高貴和優雅，長着漂亮的長毛大型犬。但對於外貌粗野，短毛的[獒犬](../Page/獒犬.md "wikilink")，就不大喜歡。通常會送到自己的農場作守衛之用。而獵場守衛就選取了這些高大，強壯的[獒犬用作守衛](../Page/獒犬.md "wikilink")。而盜獵者就會用一種稱為[lurchers混種](../Page/lurchers.md "wikilink")[靈提作為盜獵犬](../Page/靈提.md "wikilink")，守護獵場的[獒犬雖然高大強壯](../Page/獒犬.md "wikilink")，但就不靈活，缺乏耐力及侵略性。獵場守衛雖要一種強壯，沉靜，無畏無懼，敏捷和充滿爆炸力的[犬種](../Page/犬.md "wikilink")。這時候在[英國中下層流行飼養一種稱為](../Page/英國.md "wikilink")[鬥牛犬的品種](../Page/鬥牛犬.md "wikilink")，牠符合了獵場守衛的要求，只是細小一点。正好補償[獒犬的缺点](../Page/獒犬.md "wikilink")。他們將兩個品種結合，成了今天的**鬥牛獒**。牠亦有另一個別名**獵場守衛者**。早期的鬥牛獒樣貌同現今不太相似，主要是在二十年代，有很多不同系统的鬥牛獒出現，直至到1924年制定出必須三代都是鬥牛獒配對繁殖，方可註冊。而同時以60％的[獒犬和](../Page/獒犬.md "wikilink")40％的[鬥牛犬配種模式給與繁殖者参考](../Page/鬥牛犬.md "wikilink")。但在二十世紀初的[鬥牛犬同早期已经有很大變化](../Page/鬥牛犬.md "wikilink")，得出的結果，就是現今的**鬥牛獒**。

以往的獵場守衛特別喜歡虎班毛色，因為在晚上不容易被發現，黄色品種在月光的照射下容易被盜獵者發現。

## 一般外貌

充滿力量的結構，展現強大的爆發力，敏捷而充滿活力，絕不會有累贅的感覺。注意力強，機警，忠心。頭大，從任何角度觀看都呈正方型，整個頭額的圓周相等於肩膊至地面的高度。嘴短，長度等於整個頭部的三分之一，闊度與深度相等。平齒咬合，輕微倒及咬合接受。短毛，毛色有不同深淺虎班，鹿黃至火紅，任何毛色都必須保持着黑臉譜的特点。

  - 體高雄犬63.5至68.5厘米，雌犬61-66，厘米。
  - 體重雄犬50至59公斤，雌犬41至50公斤。

來源[美國犬業俱樂部](../Page/美國犬業俱樂部.md "wikilink")

## 名犬

[韋基舜與**果種**](https://zh.wikipedia.org/wiki/File:WKS_bullmastiff.JPG "fig:韋基舜與果種")
在[香港犬壇的年長者都熟悉在六十年代](../Page/香港.md "wikilink")，有一頭很出名的**鬥牛獒**。當年風魔了很多愛好[純種犬的人士](../Page/純種犬.md "wikilink")，亦是首次有[中文報章爭相報導](../Page/中文報章.md "wikilink")。牠的名字叫**果種**(Pip)，主人就是社會名人[韋基舜先生](../Page/韋基舜.md "wikilink")。當年韋先生花費二萬多元，從[英國引進到](../Page/英國.md "wikilink")[香港](../Page/香港.md "wikilink")。由於當時犬隻是不能用[飛機運送](../Page/飛機.md "wikilink")，須交由[船隻經海路運送](../Page/船.md "wikilink")，但運返[香港時](../Page/香港.md "wikilink")，遇上[穌彝士運河發生事故](../Page/穌彝士運河.md "wikilink")，航道受阻，結果花了三個月才運送到[香港](../Page/香港.md "wikilink")。早期很多人都不孰悉這是什麼[犬種](../Page/犬.md "wikilink")，誤會牠的名字**果種**就是品種的名稱。**果種**參加五屆狗展，榮獲四屆全場[冠軍](../Page/冠軍.md "wikilink")。牠本身出自[英國著名犬舍Buttonoak](../Page/英國.md "wikilink")。牠父親是1958年的[克魯夫狗展Cruft全場後備總](../Page/克魯夫狗展.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")。當年的評審員有[世界知名](../Page/世界.md "wikilink")，己故[美國Mr](../Page/美國.md "wikilink").
Maxwell
Riddle，他曾經兩次到[香港担任評審工作](../Page/香港.md "wikilink")。他在1962年接受訪問時說，我曾經在[世界各地評審超過五個以上的大規模](../Page/世界.md "wikilink")**鬥牛獒**單獨展，**果種**在我首次來港評審時牠應是幼犬，我已預測牠將來是明日之星。今天我可以確定沒有錯，牠無論到[世界各地都會勝出](../Page/世界.md "wikilink")，牠有一個完美的體格，充滿力量、醒目、永遠都受人注目。
[韋基舜先生除了是](../Page/韋基舜.md "wikilink")**果種**的主人亦是全[世界首位將](../Page/世界.md "wikilink")[傳統中國沙皮犬引入參加](../Page/傳統中國沙皮犬.md "wikilink")[犬展的人士](../Page/犬展.md "wikilink")。

## 参考文獻

  - THE BULLMASTIFF: PEERLESS PROTECTOR,by Jack Shastid and Gerry Roach.
  - EVERYONE'S GUIDE TO THE BULLMASTIFF,by Carol H. Beans
  - RAISING THE BULLMASTIFF,by Mona Webb
  - BULLMASTIFFS TODAY,by Lyn Pratt.
  - THE MASTIFF AND BULLMASTIFF HANDBOOK,by Douglas Oliff.
  - 吾土吾情,[韋基舜](../Page/韋基舜.md "wikilink").
  - k9quan.

[Category:鬥牛犬](../Category/鬥牛犬.md "wikilink")