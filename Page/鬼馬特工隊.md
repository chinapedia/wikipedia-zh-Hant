《**鬼馬特工隊**》（**The Secret
Show**，又譯**秘密行動**，臺灣稱**特務小英雄**）是一部由[英國](../Page/英國.md "wikilink")[BBC](../Page/英國廣播公司.md "wikilink")[電視台兒童頻道製作的](../Page/電視台.md "wikilink")[動作](../Page/動作.md "wikilink")、[搞笑](../Page/搞笑.md "wikilink")[動畫片](../Page/動畫片.md "wikilink")。除了在英國和美國地區播放外，香港的[無線電視](../Page/無線電視.md "wikilink")[明珠台也有播放](../Page/明珠台.md "wikilink")。

## 概要

此動畫片主要講述在2082年，U.Z.Z.組織的兩名秘密成員在吩咐下到達世界各地去保衛地球並阻止敵人的奸計。每次開場白都是The Fluffy
Bunny
Show在表演，但每次都因緊急原因而被並強制性清場，然後要開始執行秘密行動。兩位成員在執行任務途中總會鬧出一些笑話，但又真的能順利完成任務。此動畫的特點是角色們的腳和手都很瘦，尤其是接近身體那部份。這些角色全是由電腦構成，沒有任何手畫部份。

## 人物簡介

  - Victor Volt
    秘密成員之一。
  - Anita Knight
    秘密成員之一。
  - 每天更換（Changed Daily）
    U.Z.Z.組織的主席，由於機密的關係，名字由他的手機隨機製造，有時名字會很趣怪而令眾人笑個不停。
  - Professor Professor
    Special Agent Ray
    Doctor Doctor
    是反派組織T.H.E.M.的最高領導人。
  - Sweet Little Granny
    就是The Fluffy Bunny Show的主持人，一個和藹可親的老婆婆。

## 相關網站

  -
  - [官方網站](https://web.archive.org/web/20070621021232/http://www.thesecretshow.com/website.htm)

  - [遊戲網站](https://web.archive.org/web/20070817024315/http://www.bbc.co.uk/cbbc/games/games/spies/)

[Category:英國動畫影集](../Category/英國動畫影集.md "wikilink")
[Category:BBC兒童電視節目](../Category/BBC兒童電視節目.md "wikilink")