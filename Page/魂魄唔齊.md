是一部於2002年上映的[香港電影](../Page/香港電影.md "wikilink")，屬輕鬆恐怖片種，由[梁栢堅](../Page/梁栢堅.md "wikilink")[導演](../Page/導演.md "wikilink")，[陳奕迅](../Page/陳奕迅.md "wikilink")、[容祖兒主演](../Page/容祖兒.md "wikilink")，以粵劇戲棚作為背景。類似電影包括1980年代[許鞍華執導的](../Page/許鞍華.md "wikilink")《[撞到正](../Page/撞到正.md "wikilink")》及[關錦鵬的](../Page/關錦鵬.md "wikilink")《[胭脂扣](../Page/胭脂扣.md "wikilink")》等。

魂魄唔齊在[香港上映期間票房為](../Page/香港.md "wikilink")5,836,377.00港元。

## 演員表

  - **[陳奕迅](../Page/陳奕迅.md "wikilink")** 飾 **路初八**
  - **[容祖兒](../Page/容祖兒.md "wikilink")** 飾 **紫雲飛**
  - [伍詠薇](../Page/伍詠薇.md "wikilink") 飾 娟姐
  - [黃秋生](../Page/黃秋生.md "wikilink") 飾 洪霸天
  - [鄭希怡](../Page/鄭希怡.md "wikilink") 飾 陸環彩
  - [謝霆鋒](../Page/謝霆鋒.md "wikilink") 飾 蔣浩風
  - [楊淇](../Page/楊淇_\(香港演員\).md "wikilink") 飾 Miko
  - [文千歲](../Page/文千歲.md "wikilink") 飾 吉叔
  - [劉浩龍](../Page/劉浩龍.md "wikilink") 飾 十仔
  - [蕭亮](../Page/蕭亮.md "wikilink") 飾
  - [黃岳泰](../Page/黃岳泰.md "wikilink") 飾
  - [車婉婉](../Page/車婉婉.md "wikilink") 飾
  - [劉洵](../Page/劉洵_\(演員\).md "wikilink") 飾

## 資料來源

  - [《魂魄唔齊》 —
    戲台上下情難捨](https://web.archive.org/web/20070519041133/http://www.filmcritics.org.hk/big5/criticism_section_article.php?catid=46&id=85&PHPSESSID=d134c61b34e2000f38d0f4a6dd9371b3)

## 外部連結

  - [魂魄唔齊](http://demi-haunted.emg.com.hk/)

  - {{@movies|fwhk44074481}}

  -
  -
  -
  -
  -
[Category:香港驚悚片](../Category/香港驚悚片.md "wikilink")
[2](../Category/2000年代香港電影作品.md "wikilink")