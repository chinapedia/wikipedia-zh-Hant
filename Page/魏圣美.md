**魏圣美**（****，****，）是[韓裔美國人](../Page/韓裔美國人.md "wikilink")，出生于[夏威夷](../Page/夏威夷.md "wikilink")[火奴鲁鲁](../Page/火奴鲁鲁.md "wikilink")，美国职业[高尔夫球球员](../Page/高尔夫球.md "wikilink")。

魏圣美4岁起学习高尔夫球。她是取得美国职业高尔夫球协会锦标赛（15岁）和美国女子职业高尔夫球协会锦标赛（14岁）参赛资格最年轻的球员。她在2005年10月5日16岁生日前夕宣布成为职业选手。她与[Nike及](../Page/Nike.md "wikilink")[索尼公司的赞助合同金额估计达每年一千万美元以上](../Page/索尼.md "wikilink")。

她身高，开球有力，推球进洞的能力稍弱。她极受媒体重视，每次比赛都有大量记者和支持她的观众围观。

## 參見

  - [神童](../Page/神童.md "wikilink")

## 外部連結

  -
  -
[Category:美國女子高爾夫球運動員](../Category/美國女子高爾夫球運動員.md "wikilink")
[Category:韓裔美國人](../Category/韓裔美國人.md "wikilink")
[Category:檀香山人](../Category/檀香山人.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:魏姓](../Category/魏姓.md "wikilink")