**魏晉玄學**，為[魏](../Page/曹魏.md "wikilink")[晉時代思想主流](../Page/晋朝.md "wikilink")，與[先秦諸子](../Page/先秦諸子.md "wikilink")、[兩漢經學](../Page/兩漢經學.md "wikilink")、[隋唐佛學](../Page/隋唐佛學.md "wikilink")、[宋明理學](../Page/宋明理學.md "wikilink")、[當代新儒家皆為中國哲學史之重要脈絡](../Page/當代新儒家.md "wikilink")。

## 思想背景

[兩漢時期](../Page/兩漢.md "wikilink")，[經學尤其是以公羊傳為代表的](../Page/經學.md "wikilink")[今文經學獨尊](../Page/今文經學.md "wikilink")，[漢武帝時被列入官方意識形態作為朝廷入仕干祿之門](../Page/漢武帝.md "wikilink")；而步入[東漢](../Page/東漢.md "wikilink")，雖今古文逐漸彌合趨同，但由於[光武帝劉秀本身便以](../Page/汉光武帝.md "wikilink")[讖緯立國](../Page/讖緯.md "wikilink")，遂使經學日益讖緯化，以[白虎觀會議為經學國教化](../Page/白虎觀會議.md "wikilink")，神學化之標誌。[天人](../Page/天人.md "wikilink")、[陰陽](../Page/陰陽.md "wikilink")、[符應等觀念大盛](../Page/符應.md "wikilink")，使學術依附政治，而流於荒誕，深受[王充](../Page/王充.md "wikilink")、[仲長統](../Page/仲長統.md "wikilink")、[荀悅等人批判](../Page/荀悅.md "wikilink")；而[古文經學雖偏重](../Page/古文經學.md "wikilink")[實證](../Page/實證.md "wikilink")[訓詁](../Page/訓詁.md "wikilink")，但經過賈逵等人的政治調和，日漸讖緯，流於支離。在士人集團中，經歷了[黨錮之禍](../Page/党锢之祸.md "wikilink")，使本已開始陷入虛矯的[東漢](../Page/東漢.md "wikilink")[氣節更為凋敝噤聲](../Page/氣節.md "wikilink")。至漢末魏晉時，儒家經學雖仍為官方學術主流，然玄學風氣則隨[名士](../Page/名士.md "wikilink")[清談逐漸流行](../Page/清談.md "wikilink")，以《[老子](../Page/道德經.md "wikilink")》、《[莊子](../Page/莊子_\(書\).md "wikilink")》、《[易經](../Page/易經.md "wikilink")》為討論張本，喜好討論[有無](../Page/有無.md "wikilink")、[本末等玄理](../Page/本末.md "wikilink")，論辨深具理致。

漢末，由於天下大亂，[劉表於](../Page/劉表.md "wikilink")[荊州](../Page/荆州_\(古代\).md "wikilink")，招致士人，當地局勢大體安定，文士、學者多前往歸附，日漸形成特殊學風，後人研究有稱為荊州學派者。荊州學風，逐漸捨棄[象數](../Page/象數.md "wikilink")、吉凶等說法，而改以義理內容為主。

漢代時，[氣化思想](../Page/氣化思想.md "wikilink")、[宇宙生成論盛行](../Page/宇宙生成論.md "wikilink")，演述[陰陽](../Page/陰陽.md "wikilink")、天人等論題。而[魏晉時期](../Page/魏晉.md "wikilink")，此類討論漸往[形而上學形式發展](../Page/形而上學.md "wikilink")，以[王弼](../Page/王弼_\(三國\).md "wikilink")、[郭象為其代表](../Page/郭象.md "wikilink")。而漢代對人性的討論，逐步發展成為魏晉時「[才性](../Page/才性.md "wikilink")」與「人物鑑賞」等論題，其中以劉劭《[人物誌](../Page/人物誌.md "wikilink")》為其代表。當時政治勢力更替，局勢混亂，原有價值體系面臨挑戰，「[名教與](../Page/名教.md "wikilink")[自然](../Page/自然\(哲學\).md "wikilink")」、「[聖人論](../Page/聖人論.md "wikilink")」亦隨之而起。

## 名義

所謂魏晉玄學，與世俗所謂[玄學](../Page/玄學.md "wikilink")、玄虛實有不同。觀念應出自《老子》，王弼注《老子》時，曾提出「玄者，物之極也。」「玄者，冥也。默然無有也。」，乃是探索[萬物根源](../Page/萬物根源.md "wikilink")、[本體等層次的觀念](../Page/本體.md "wikilink")。對於當時所流行的相關論題，魏晉人又稱為「[名理](../Page/名理之學.md "wikilink")」之學，詳加分析事物觀念，考究「形名」、「言意」等論題。

## 論題

玄學重視萬物根源[存有等相關論題](../Page/存有.md "wikilink")，深受[老](../Page/老子.md "wikilink")[莊思想影響](../Page/莊子.md "wikilink")，而有深入發展，[王弼](../Page/王弼_\(三國\).md "wikilink")《[老子注](../Page/老子注.md "wikilink")》、[郭象](../Page/郭象.md "wikilink")《[莊子注](../Page/莊子注.md "wikilink")》為魏晉玄學重要論著，更為老、莊最首要[注疏](../Page/注疏.md "wikilink")。而當時名士詮釋儒家[典籍](../Page/典籍.md "wikilink")，如[何晏](../Page/何晏.md "wikilink")《[論語集解](../Page/論語集解.md "wikilink")》、王弼《[周易注](../Page/周易注.md "wikilink")》、《[論語釋疑](../Page/論語釋疑.md "wikilink")》、《[周易略例](../Page/周易略例.md "wikilink")》等多以[道家思想](../Page/道家.md "wikilink")，援引解釋[儒家觀念](../Page/儒家.md "wikilink")。會通[孔老乃為當時重要議題](../Page/孔子.md "wikilink")。

## 分期

  - 正始玄學（204年\~249年）
  - 竹林玄學（254年\~262年）
  - 元康玄學（290年前後）
  - 東晉玄學

## 代表人物

  - [王弼](../Page/王弼.md "wikilink")
  - [何晏](../Page/何晏.md "wikilink")
  - [阮籍](../Page/阮籍.md "wikilink")
  - [嵇康](../Page/嵇康.md "wikilink")
  - [向秀](../Page/向秀.md "wikilink")
  - [裴頠](../Page/裴頠.md "wikilink")
  - [郭象](../Page/郭象.md "wikilink")
  - [張湛](../Page/張湛.md "wikilink")

## 參考

  - 湯用彤：《理學．佛學．玄學》（北京：北京大學出版社，1991）。
  - 唐翼明：《魏晉文學與玄學》（武漢：長江文藝出版社，2004）。

[WJXX](../Category/晉朝歷史事件.md "wikilink")
[Category:中国思想史](../Category/中国思想史.md "wikilink")