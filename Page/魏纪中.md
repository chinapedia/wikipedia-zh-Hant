**魏纪中**（），[浙江](../Page/浙江.md "wikilink")[余姚人](../Page/余姚.md "wikilink")，曾任[国际排球联合会](../Page/国际排球联合会.md "wikilink")[主席](../Page/主席.md "wikilink")，是[中国体育产业的权威](../Page/中国.md "wikilink")。

## 简介

1951年，就读于上海市[南洋模范中学](../Page/南洋模范中学.md "wikilink")。1954年，就读于[南京大学](../Page/南京大学.md "wikilink")，专业为[法国](../Page/法国.md "wikilink")[文学](../Page/文学.md "wikilink")，曾为南大排球队主力\[1\]。1958年，就职于中国国家体育运动委员会（今中国[国家体育总局](../Page/国家体育总局.md "wikilink")），担任国际司的科员，并历任副处长，处长，国际司司长。1994年，担任中国国家体育运动委员会专职委员，并兼任主任助理。1997年，出任[中体产业集团股份有限公司](../Page/中体产业集团股份有限公司.md "wikilink")[董事长](../Page/董事长.md "wikilink")。2008年出任[国际排球联合会主席](../Page/国际排球联合会.md "wikilink")。

## 任职

  - 1984年至今，先后担任国际排球联合会的理事，副主席，第一副主席
  - 1984年至今，担任亚洲排球联合会的主席
  - 1986年至1997年，担任中国奥林匹克运动委员会（简称“奥委会”）秘书长
  - 1986年至今，担任中国排球协会副主席
  - 1986年至今，担任亚洲奥林匹克理事会理事和体育运动委员会会主席
  - 2000年至今，担任世界跆拳道联合会的执行委员
  - 2004年至今，担任中华全国体育总会的名誉委员
  - 2002年至今，担任[北京](../Page/北京.md "wikilink")2008年[奥林匹克运动会组织委员会](../Page/奥林匹克运动会.md "wikilink")（简称北京奥组委）的高级顾问
  - 2008年至2012年，担任国际排联主席

## 參考

<references />

## 外部链接

  - [体育频道 \> 评论 \> 体育评论频道 \> 魏纪中视点 \>
    魏纪中](http://sports.sohu.com/s2005/8291/s226125363.shtml)
  - [魏纪中：走过奥运50年](http://finance.sina.com.cn/roll/20040811/0922940612.shtml)

[Ji](../Category/魏姓.md "wikilink")
[Category:余姚人](../Category/余姚人.md "wikilink")
[Category:南京大学校友](../Category/南京大学校友.md "wikilink")
[Category:上海市南洋模范中学校友](../Category/上海市南洋模范中学校友.md "wikilink")
[Category:中华人民共和国政府官员](../Category/中华人民共和国政府官员.md "wikilink")
[Category:国际排球联合会主席](../Category/国际排球联合会主席.md "wikilink")
[Category:中国跆拳道协会主席](../Category/中国跆拳道协会主席.md "wikilink")
[Category:中国排球协会副主席](../Category/中国排球协会副主席.md "wikilink")

1.  [从南大主力到国际排联主席
    魏纪中自述排坛历程](http://sports.sohu.com/20080630/n257820926.shtml)