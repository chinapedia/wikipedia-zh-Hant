**魏节闵帝元恭**（；在位531年-532年），字**修业**，[河南郡](../Page/河南郡.md "wikilink")[洛阳县](../Page/洛阳县.md "wikilink")（今[河南省](../Page/河南省.md "wikilink")[洛阳市东](../Page/洛阳市.md "wikilink")）人，[魏献文帝拓跋弘之孙](../Page/魏献文帝.md "wikilink")，广陵惠王[元羽之子](../Page/元羽.md "wikilink")，母[王氏](../Page/先太妃.md "wikilink")，是[南北朝时期](../Page/南北朝.md "wikilink")[北魏的皇帝](../Page/北魏.md "wikilink")。

## 生平

先前元恭的伯父[北魏孝文帝](../Page/北魏孝文帝.md "wikilink")[元宏为鼓励鲜卑与汉族通婚而将元羽嫡妻降为妾室](../Page/元宏.md "wikilink")，另聘娶[郑始容为元羽嫡妻](../Page/郑始容.md "wikilink")。史书没有记载元恭的母亲[王氏是否就是元羽被降为妾室的原配](../Page/先太妃.md "wikilink")，仅记载元羽死后袭爵广陵王的是元恭，而非元恭的哥哥[元欣](../Page/元欣.md "wikilink")。

[建明二年二月廿九日](../Page/建明_\(北魏\).md "wikilink")（531年4月1日），[爾朱榮堂弟](../Page/爾朱榮.md "wikilink")[爾朱世隆废](../Page/爾朱世隆.md "wikilink")[元晔](../Page/元晔.md "wikilink")，立元恭為帝。[军阀](../Page/军阀.md "wikilink")[高欢则立渤海太守安定王](../Page/高欢.md "wikilink")[元朗为帝](../Page/元朗_\(安定王\).md "wikilink")。高欢打败尔朱氏后，考虑到元朗世系疏远，一度想尊奉元恭，派仆射[魏兰根慰谕洛阳观察节闵帝为人](../Page/魏兰根.md "wikilink")。魏兰根认为节闵帝神采高明，日后难制，与[侍中](../Page/侍中.md "wikilink")、[司空](../Page/司空.md "wikilink")[高-{乾}-兄弟及](../Page/高乾_\(北魏\).md "wikilink")[黄门侍郎](../Page/黄门侍郎.md "wikilink")[崔㥄强调节闵帝系尔朱氏所立](../Page/崔㥄.md "wikilink")，共劝高欢为了讨伐尔朱氏师出有名而废帝。532年6月（农历四月），节闵帝被高欢所废，囚禁于崇训佛寺。元朗亦被高欢所迫禅位给平阳王[元修即](../Page/元修.md "wikilink")[北魏孝武帝](../Page/北魏孝武帝.md "wikilink")。节闵帝赋诗：“朱門久可患，紫極非情玩。顛覆立可待，一年三易換。時運正如此，唯有修真觀。”

6月21日（农历五月三日），魏节闵帝在门下外省被[孝武帝](../Page/元修.md "wikilink")[毒死](../Page/毒死.md "wikilink")，葬以亲王殊礼\[1\]；加九旒、銮辂、黄屋、左纛，班剑百二十人，二卫、羽林备仪卫。东魏称**广陵王**或**前废帝**，西魏谥**节闵帝**。

2013年，洛阳市文物考古研究院完成北魏节愍帝的陵墓考古挖掘工作，陵墓中出土东罗马帝国金币一枚。\[2\]

## 家庭

### 兄弟姐妹

  - [元欣](../Page/元欣.md "wikilink")，西魏使持节、太傅、柱国大将军、大宗师、大司徒、广陵容王
  - [元永业](../Page/元永业.md "wikilink")，东魏金紫光禄大夫、高密王
  - 元氏，嫁孟氏\[3\]\[4\]

### 皇后

  - [尔朱皇后](../Page/尔朱皇后_\(北魏节闵帝\).md "wikilink")

### 儿子

  - [元子恕](../Page/元子恕.md "wikilink")，北魏沛郡王

## 參考資料

[Category:北魏宗室](../Category/北魏宗室.md "wikilink")
[Category:河南元氏](../Category/河南元氏.md "wikilink")
[Category:北魏皇帝](../Category/北魏皇帝.md "wikilink")
[Category:北朝被毒死皇帝](../Category/北朝被毒死皇帝.md "wikilink")
[Category:北朝废帝](../Category/北朝废帝.md "wikilink")
[Category:谥节闵](../Category/谥节闵.md "wikilink")

1.  《资治通鉴·卷一百五十五·梁纪十一》：五月，丙申，魏主鸩节闵帝于门下外省，诏百司会丧，葬用殊礼。
2.  [河南洛阳发掘疑似北魏帝陵
    出土东罗马帝国金币](http://finance.takungpao.com.hk/hgjj/q/2013/1026/1351774.html)
3.  《周书·卷二十二·列传第十四》：广陵王元欣，魏之懿亲。其甥孟氏，屡为匈横。或有告其盗牛。
4.  《北史·卷六十四·列传第五十二》：广陵王欣，魏之懿亲。其甥孟氏，屡为凶横。或有告其盗牛。