《**魔俠震天雷**》（），美國電影導演[山姆·雷米融合黑暗與暴力的電影作品](../Page/山姆·雷米.md "wikilink")。亦獲得少數支持者的喜愛及推崇，常被歸類於[次文化之列](../Page/次文化.md "wikilink")。

## 概述

本作塑造出有別於以往以漫畫改編[超級英雄電影人物](../Page/超級英雄.md "wikilink")，採取使用以暴制暴的行事作風。影片內容融合黑暗驚悚風格與科幻暴力，表現出強烈的視覺風格。

## 劇情簡介

培頓是一位生化學博士，致力研究一種能讓[皮膚再造的](../Page/皮膚.md "wikilink")[醫療科技](../Page/醫療.md "wikilink")。但是，這人造皮膚一旦在陽光下曝露100分鐘後，就會損壞變質。某日，他的[律師女友茱莉將一份罪證文件留在實驗室內](../Page/律師.md "wikilink")，導致歹徒前往追尋，不但將實驗室毀壞並將他整個人毀容。

大難不死的培頓卻因此體無完膚，更不敢與女友相認，唯有利用這項[醫療科技來變換臉孔及身分](../Page/醫療.md "wikilink")。他選擇以暴治暴的手段，開始籌劃報復傷害他的歹徒，……

## 外部連結

  - [魔俠震天雷（Darkman）](http://www.imdb.com/title/tt0099365/)，〈[網路電影資料庫](../Page/網路電影資料庫.md "wikilink")〉

[Category:1990年電影](../Category/1990年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:美国科幻片](../Category/美国科幻片.md "wikilink")
[Category:美国動作片](../Category/美国動作片.md "wikilink")
[Category:山姆·雷米电影](../Category/山姆·雷米电影.md "wikilink")
[Category:环球影业电影](../Category/环球影业电影.md "wikilink")
[Category:私刑電影](../Category/私刑電影.md "wikilink")