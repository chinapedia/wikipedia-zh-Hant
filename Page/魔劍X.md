**魔劍X**（Maken Deus Ex
Machina）是日本[電視遊戲公司](../Page/電視遊戲.md "wikilink")[ATLUS為](../Page/ATLUS.md "wikilink")[Dreamcast開發的一款](../Page/Dreamcast.md "wikilink")[第一身視點](../Page/第一身視點.md "wikilink")[動作遊戲](../Page/動作遊戲.md "wikilink")，於1999年發售。玩家在遊戲中扮演的就是具有自我意識的武器**魔劍**，透過乘取其他人類的肉體在全世界冒險。魔劍在故事一開始吸收了女主角**相模
桂**的「IMAGE」（魔劍X世界中對[靈魂](../Page/靈魂.md "wikilink")、[精神等的專稱](../Page/精神.md "wikilink")），與其對話及選擇與**封劍士**還是**三業會**的勢力合作將會影響[遊戲關卡流程及發展出高達](../Page/遊戲關卡.md "wikilink")7個的結局。

由於遊戲是由[女神轉生系列的開發組ATLUS第一研發部開發](../Page/女神轉生.md "wikilink")，加上與[真女神轉生系列十分相似的世界觀及](../Page/真女神轉生.md "wikilink")[金子一馬的大膽人設](../Page/金子一馬.md "wikilink")，一直被女神轉生的愛好者視為女神轉生的非冠名外傳作品。

## 魔劍爻

**魔劍爻**（歐版：Maken Shao: Demon
Sword）是ATLUS於2001年將魔劍X移植到[PS2的](../Page/PS2.md "wikilink")「改良版」，其中的改變部分包括將X中表達劇情的靜止畫面改為全動畫；X中被玩家批評導致[頭暈的第一身視點改為玩家角色背後的](../Page/晕动病.md "wikilink")[第三身視點](../Page/第三身視點.md "wikilink")；加入了練功元素，可玩角色的技能不再如X般能全部立即使用，在爻中需要累積打倒敵人獲得的IMAGE逐步開啟。

## 外部連結

  -   - [「魔剣Ｘ」官方網頁](https://web.archive.org/web/20010522154440/http://www.atlus.co.jp/cs/game/dcast/maken/index.html)
      - [「魔剣爻」官方網頁](https://web.archive.org/web/20030203035912/http://www.atlus.co.jp/cs/game/pstation2/shao/index.html)

[M](../Category/1999年电子游戏.md "wikilink")
[Category:Dreamcast遊戲](../Category/Dreamcast遊戲.md "wikilink")
[M](../Category/PlayStation_2游戏.md "wikilink")
[M](../Category/動作遊戲.md "wikilink")
[Category:Atlus游戏](../Category/Atlus游戏.md "wikilink")
[Category:月刊Magazine Z](../Category/月刊Magazine_Z.md "wikilink")