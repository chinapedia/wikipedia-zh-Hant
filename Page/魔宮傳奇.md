《**魔宮傳奇**》
（）是1984年的科幻冒險電影，由[史蒂芬·史匹柏執導](../Page/史蒂芬·史匹柏.md "wikilink")，[喬治·盧卡斯擔任故事編劇](../Page/喬治·盧卡斯.md "wikilink")，為《[印第安納·瓊斯](../Page/印第安納·瓊斯.md "wikilink")》系列电影时间顺序的前传電影（片头出现1935年，是讲述法柜奇兵前一年的发生的事）。考古學教授兼探險家[印第安納·瓊斯](../Page/印第安納·瓊斯.md "wikilink")（Indiana
Jones）由[哈里遜·福特飾演](../Page/哈里遜·福特.md "wikilink")，片中敘述他另一次的驚險考古之旅。

## 劇情簡介

1935年，考古學教授印第安納·瓊斯帶著夜總會歌女威莉和12歲的孤兒-{zh-cn:小滑溜;zh-tw:小滑溜;zh-hk:小圓}-在中國[上海一起逃命冒險](../Page/上海.md "wikilink")，逃脫後意外到了一個破落的[印度小村莊](../Page/印度.md "wikilink")，這個小村落的所有幼童和村裡的1塊珍貴聖石都失蹤了。印第安納·瓊斯承諾協助村長尋回失蹤的珍貴聖石和兒童，來到盤踞邪惡勢力的王室宮殿，找到3塊具有神秘法力的寶石。印第安納·瓊斯他們再度展開驚險的鬥智鬥力和奪寶追逐戰，最终，在经历了一系列惊险（比如食用野味，被强灌迷魂药，矿车追逐等）后，终于以宫内的邪教教主
Mola Ram掉进河里喂食鳄鱼，三颗钻石其一被夺回，以及孩子们获救为完美结局。而琼斯博士也在最后和威莉亲吻在一块。

## 製作

本片開始時，印第安納·瓊斯跟一個上海黑幫爭奪[鑽石及解藥的場面](../Page/鑽石.md "wikilink")，實景在[澳門拍攝](../Page/澳門.md "wikilink")。

香港著名演員[喬宏獲邀出演黑幫頭目Lao](../Page/喬宏.md "wikilink") Che一角。

## 獎項記錄

  - [奧斯卡最佳特效獎](../Page/奧斯卡.md "wikilink")、英國金像獎最佳特效。

## 外部連結

  - [IndianaJones.com](http://www.indianajones.com/), 喬治·盧卡斯的印第安納·瓊斯網站

  - [The Indiana Jones Wiki](http://indianajones.wikicities.com)
    印第安納·瓊斯迷的網站

  -
  -
  -
  -
  -
[Category:1984年電影](../Category/1984年電影.md "wikilink")
[Category:1980年代動作片](../Category/1980年代動作片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國續集電影](../Category/美國續集電影.md "wikilink")
[Category:美国特摄电影](../Category/美国特摄电影.md "wikilink")
[Category:1930年代背景電影](../Category/1930年代背景電影.md "wikilink")
[Category:上海背景電影](../Category/上海背景電影.md "wikilink")
[Category:華盛頓州取景電影](../Category/華盛頓州取景電影.md "wikilink")
[Category:亞利桑那州取景電影](../Category/亞利桑那州取景電影.md "wikilink")
[Category:佛羅里達州取景電影](../Category/佛羅里達州取景電影.md "wikilink")
[Category:澳門取景電影](../Category/澳門取景電影.md "wikilink")
[Category:奥斯卡最佳视觉效果获奖电影](../Category/奥斯卡最佳视觉效果获奖电影.md "wikilink")
[Category:寻宝电影](../Category/寻宝电影.md "wikilink")
[Category:盧卡斯影業電影](../Category/盧卡斯影業電影.md "wikilink")
[Category:約翰·威廉斯配樂電影](../Category/約翰·威廉斯配樂電影.md "wikilink")
[Category:印第安納·瓊斯電影](../Category/印第安納·瓊斯電影.md "wikilink")