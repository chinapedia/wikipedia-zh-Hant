1999年由[New World
Computing在](../Page/New_World_Computing.md "wikilink")[Windows平台上开发的](../Page/Microsoft_Windows.md "wikilink")[回合制策略魔幻游戏](../Page/回合制策略游戏.md "wikilink")，其出版商是[3DO](../Page/3DO.md "wikilink")。稍后3DO和Loki
Software分别推出了可以[苹果机和](../Page/Apple_Macintosh.md "wikilink")[Linux系统上运行的版本](../Page/Linux.md "wikilink")。这是[魔法门之英雄无敌系列的第三代](../Page/魔法门之英雄无敌.md "wikilink")。游戏情节参照第一次贯穿了《[魔法门VI：天堂之令](../Page/魔法门VI：天堂之令.md "wikilink")》，并且部分作为了《[魔法门VII：血统与荣耀](../Page/魔法门VII：血统与荣耀.md "wikilink")》的前传。玩家可以选择六个战役之中的一个来进行情节模式，也可以选择场景来进行与电脑或其他玩家的对抗。

游戏的操作非常类似于它的前作：玩家控制一定数量的英雄来控制[神话](../Page/神话.md "wikilink")[传说中的生物组成的](../Page/传说.md "wikilink")[部队](../Page/部队.md "wikilink")。游戏分为两部分：在战略地图上进行探索以及回合制的战斗系统。玩家可以通过花费资源来招募英雄和部队。英雄通过和敌人与野外生物不断的战斗提升经验值。胜利的条件因地图而异，包括消灭所有的敌人、运送指定宝物、积累一定数量的资源或生物、找到[神器并建造奇迹](../Page/神器.md "wikilink")，等等。

英雄无敌III有两个资料片——《[魔法门之英雄无敌III：末日之刃](../Page/魔法门之英雄无敌III：末日之刃.md "wikilink")》和《[魔法门之英雄无敌III：死亡阴影](../Page/魔法门之英雄无敌III：死亡阴影.md "wikilink")》。还有以一系列简短故事构成的《[英雄无敌历代记](../Page/英雄无敌历代记.md "wikilink")》。2000年还发行了特别版本：2CD的《英雄无敌III完全版》，包括原版游戏和两部资料片。其后由爱好者开发的非官方版本又在英雄无敌III引擎的基础上作了大幅度变动，推出了一个内部元素更加复杂繁琐并且更不平衡的《[魔法门之英雄无敌III：追隨神跡](../Page/魔法门之英雄无敌III：追隨神跡.md "wikilink")》（Wake
Of God, [WOG](../Page/WOG.md "wikilink")）。

《英雄无敌III》及其资料片的简体中文版由[上海育碧代理](../Page/上海育碧.md "wikilink")，繁体中文版游戏则由[欧乐影视代理](../Page/欧乐影视.md "wikilink")。资料片《末日之刃》因欧乐突然倒闭而没有繁体中文版本。《死亡阴影》和《英雄无敌历代记》繁体中文版本由[第三波代理](../Page/第三波公司.md "wikilink")。

## 故事情节

游戏情节通过叙述设置在[埃拉西亚大陆上的七个战役而展开](../Page/埃拉西亚大陆.md "wikilink")。在战役中，情节通过观点的交锋而展开，使玩家能够有机会体验到每个种族的发展过程。

在《[魔法门VI：天堂之令](../Page/魔法门VI：天堂之令.md "wikilink")》中，凯瑟琳在嫁给罗兰德国王后成为了王后。在此期间，其父格里冯哈特（格里芬·哈特，狮鹫心）被暗杀。没有了他们敬爱的国王，埃拉西亚被地下王国尼贡和魔鬼王国伊欧弗的军队所入侵。凯瑟琳女王回国寻求帮助以对抗蹂躏国家的邪恶势力。

埃拉西亚的首都斯坦德威克被地下城之王尼贡和地狱之王伊欧弗洗劫。凯瑟琳的首要任务就是要通过援助在被征服的王国上建立一个立足点。布拉卡达巫师和埃里精灵答应了其请求，共同进攻斯坦德威克并最终夺回了它。与此同时，来自德珈王国负责刺杀格里冯哈特国王的亡灵巫师们企图复活国王的尸体作为行尸，计划用他的智慧领导亡灵军队。然而，格里冯哈特国王的智慧超过了巫师们能控制的范围，使他变为了超级僵尸。因为没有其他的办法，凯瑟琳女王只有同亡灵巫师们联合起来在行尸格里冯哈特国王强大起来之前消灭他。

游戏中还有两个支线战役。其中之一是两个中立种族为边界争端而发生的宿怨；另外一个战役是在主线战役完成后出现，主要讲述的是布拉卡达和埃里独立的故事。由于厌倦了边界争端带来的家园动荡，他们一起为从两个大国之间独立而努力。

## 游戏操作

游戏分为两部分：在战略地图上进行探索以及回合制的战斗系统。在一般的游戏模式中，玩家控制一定数量的“英雄”，英雄是神话传说中的各种生物组成部队的将领和指挥者。玩家可以通过完成地图创建者设置的目标而达成“胜利”。而胜利条件可以包括征服地图上所有的城堡、运送指定宝物、积累一定数量的资源或生物、找到[神器并建造奇迹等等](../Page/神器.md "wikilink")。失败条件包括：失去所有城镇和英雄、失去指定英雄、其它玩家获得胜利、超过指定时间等等。

大多游戏地图分为两“层”——地上和地下。游戏中有通向地下的入口。

如果玩家发现了神器，即可将神器运送回自己的城堡，通过建造一个特殊的建筑（奇迹）将其安放。神器可以促进城堡里的生物大量增长，并可提高每周的收入。另外还有随城镇不同而起不同作用的额外奖励，比如塔楼城的飞船，可以使地图全开，并使守城的英雄在守城时增加15点知识。

### 种族

[英雄无敌III游戏界面截图](https://zh.wikipedia.org/wiki/File:Snap_of_heroes3_SOD.jpg "fig:英雄无敌III游戏界面截图")
英雄无敌III提供了8个[种族供](../Page/种族.md "wikilink")[玩家选择](../Page/玩家.md "wikilink")：三个“正义”种族（城堡、塔楼和壁垒）、三个“邪恶”种族（地狱、地下城和墓园）以及两个“中立”种族（据点和要塞）。每个种族都有七种基本生物，每种都可以升级为更强的生物。每个种族的英雄也有两种类型：一种倾向于战斗性，另外一种倾向于魔法性。也有玩家认为还有第三类，倾向于辅助性。有些种族本身就有战斗或是魔法的倾向，英雄的类型仅仅只是程度上的影响而已。

  - **城堡**：这个种族往往被叫作[人类并拥有一个明亮的气氛](../Page/人类.md "wikilink")，它的设计是非常适合[初学者的](../Page/初学者.md "wikilink")。[生物包括](../Page/生物.md "wikilink")[枪兵](../Page/枪兵.md "wikilink")，[弩手](../Page/弩手.md "wikilink")，[狮鹫](../Page/狮鹫.md "wikilink")，[剑客](../Page/剑客.md "wikilink")，[骑兵](../Page/骑兵.md "wikilink")，[天使](../Page/天使.md "wikilink")，它们都可以在城堡中被雇佣或升级。英雄包括[骑士和](../Page/骑士.md "wikilink")[牧师](../Page/牧师.md "wikilink")。

<!-- end list -->

  - **壁垒**：壁垒都是由各种[魔幻小说和](../Page/魔幻小说.md "wikilink")[传说中的](../Page/传说.md "wikilink")[神话生物构成的](../Page/神话.md "wikilink")，[生物包括](../Page/生物.md "wikilink")[半人马](../Page/半人马.md "wikilink")，[矮人](../Page/矮人.md "wikilink")，[精灵](../Page/精灵.md "wikilink")，[飞马骑兵](../Page/飞马.md "wikilink")，[树妖](../Page/树妖.md "wikilink")，[独角兽](../Page/独角兽.md "wikilink")，[龙](../Page/龙.md "wikilink")。和地牢的龙形成对比的是，它的龙是[绿色和](../Page/绿色.md "wikilink")[金色的](../Page/金色.md "wikilink")。英雄包括[巡逻兵和](../Page/巡逻兵.md "wikilink")[德鲁依](../Page/德鲁依.md "wikilink")。

<!-- end list -->

  - **塔楼**：这个[种族是由各种魔法生物和魔法的使用者组成的](../Page/种族.md "wikilink")。生物包括[侏儒](../Page/侏儒.md "wikilink")，[石像鬼](../Page/石像鬼.md "wikilink")，[石头人](../Page/石头人.md "wikilink")，[法师](../Page/法师.md "wikilink")，[灯神](../Page/灯神.md "wikilink")(又叫[神怪](../Page/神怪.md "wikilink"))，[那加](../Page/那加.md "wikilink")，[巨人](../Page/巨人.md "wikilink")。巨人可以升级成这部游戏中最强大的远程射击单位——[泰坦](../Page/泰坦.md "wikilink")。英雄包括[炼金术士和](../Page/炼金术士.md "wikilink")[术士](../Page/术士.md "wikilink")。

<!-- end list -->

  - **地狱**：地狱或多或少是城堡的反面——黑暗，嗜血。它的生物都来自阴森的地狱。生物包括[恶魔之子](../Page/恶魔之子.md "wikilink")，[歌戈](../Page/歌戈.md "wikilink")，[地狱犬](../Page/地狱犬.md "wikilink")，[长角魔鬼](../Page/长角魔鬼.md "wikilink")，[穴居魔](../Page/穴居魔.md "wikilink")，[火怪](../Page/火怪.md "wikilink")，[恶魔](../Page/恶魔.md "wikilink")。英雄包括[狂魔和](../Page/狂魔.md "wikilink")[异教徒](../Page/异教徒.md "wikilink")。

<!-- end list -->

  - **墓园**：墓园是亡灵生物的城镇。生物都是由亡灵组成的，并且他们都免疫精神和心灵状态的影响，如中毒，失明，士气等。生物包括[骷髅](../Page/骷髅.md "wikilink")，[僵尸](../Page/僵尸.md "wikilink")，[鬼魂](../Page/鬼魂.md "wikilink")，[吸血鬼](../Page/吸血鬼.md "wikilink")，[巫妖](../Page/巫妖.md "wikilink")，[黑骑士](../Page/黑骑士.md "wikilink")，[骨龙](../Page/骨龙.md "wikilink")。英雄包括[死亡骑士和](../Page/死亡骑士.md "wikilink")[亡灵巫师](../Page/亡灵巫师.md "wikilink")。

<!-- end list -->

  - **地牢**：也叫**地下城**，和他的名字一样，他是黑暗和腐朽生物的联合，各种[真菌总是装饰着这个阴暗的城镇](../Page/真菌.md "wikilink")，弥漫着恐怖的气息。生物包括[穴居人](../Page/穴居人.md "wikilink")，[鹰身女妖](../Page/鹰身女妖.md "wikilink")，[邪眼](../Page/邪眼.md "wikilink")，[美杜莎](../Page/美杜莎.md "wikilink")，[牛头人](../Page/牛头怪.md "wikilink")，[蝎狮](../Page/蝎狮.md "wikilink")，[龙](../Page/龙.md "wikilink")。它的龙是[红色和](../Page/红色.md "wikilink")[黑色的](../Page/黑色.md "wikilink")。英雄包括[领主和](../Page/领主.md "wikilink")[魔法师](../Page/魔法师.md "wikilink")。

<!-- end list -->

  - **据点**：据点是由各种[野兽和](../Page/野兽.md "wikilink")[蛮族组成的](../Page/蛮族.md "wikilink")。生物包括[大耳怪](../Page/大耳怪.md "wikilink")，[狼骑士](../Page/狼骑士.md "wikilink")，[半兽人](../Page/半兽人.md "wikilink")，[食人魔](../Page/食人魔.md "wikilink")，[雷鸟](../Page/雷鸟_\(传说生物\).md "wikilink")，[独眼巨人](../Page/独眼巨人.md "wikilink")，[貝西摩斯](../Page/貝西摩斯.md "wikilink")。英雄包括[野蛮人和](../Page/野蛮人.md "wikilink")[战斗法师](../Page/战斗法师.md "wikilink")。

<!-- end list -->

  - **要塞**：这是一个建筑在[沼泽上的城镇](../Page/沼泽.md "wikilink")，他的生物都是[湿地上的](../Page/湿地.md "wikilink")，而且与其说像人不如说更像[蜥蜴](../Page/蜥蜴.md "wikilink")。生物包括[狼头怪](../Page/狼头怪.md "wikilink")，[蜥蜴人](../Page/蜥蜴人.md "wikilink")，[龙蝇](../Page/龙蝇.md "wikilink")，[石化蜥蜴](../Page/石化蜥蜴.md "wikilink")，[蛇皮兽](../Page/蛇皮兽.md "wikilink")，[双足飞龙](../Page/双足飞龙.md "wikilink")，[九头蛇](../Page/九头蛇.md "wikilink")。英雄包括[驯兽师和](../Page/驯兽师.md "wikilink")[女巫](../Page/女巫.md "wikilink")。

<!-- end list -->

  - **汇集**：又叫**元素城**，是末日之刃新推出的[种族](../Page/种族.md "wikilink")。它的组成主要是[四大元素](../Page/四大元素.md "wikilink")。生物包括[小仙子](../Page/小仙子.md "wikilink")，[气元素](../Page/气元素.md "wikilink")，[水元素](../Page/水元素.md "wikilink")，[火元素](../Page/火元素.md "wikilink")，[土元素](../Page/土元素.md "wikilink")，[精神元素](../Page/精神元素.md "wikilink")，[凤凰](../Page/凤凰.md "wikilink")。英雄包括[行星人和](../Page/行星人.md "wikilink")[元素使](../Page/元素使.md "wikilink")。

## 资料片

英雄无敌III包括两部[资料片](../Page/资料片.md "wikilink")。第一部资料片是《[末日之刃](../Page/魔法门之英雄无敌III：末日之刃.md "wikilink")》，它包括9个种族，新增了元素城；一个随机地图生成器和大量新的生物、英雄和建筑；还有七个新的战役。

第二部资料片是《[死亡阴影](../Page/魔法门之英雄无敌III：死亡阴影.md "wikilink")》，是一个独立的资料片，内含有《埃拉西亚的光复》，并增加了七个新的战役以及大量的宝物（包括合成宝物）。合成宝物是通过收集一系列特殊的低级宝物而构成的具有巨大效力的道具。

无论是《末日之刃》还是《死亡阴影》都不能在Macintosh和Linux平台上良好兼容。而包括有《埃拉西亚的光复》和两部资料片的《魔法门之英雄无敌III完整版》则可兼容Windows和Macintosh平台。

### 追随神迹

2001年11月20日，由爱好者开发的非官方版本《魔法门之英雄无敌3½：追随神迹》\[1\]（，缩写：）发布。它使用了全新的游戏[脚本语言](../Page/脚本语言.md "wikilink")，以及增强的MOD。另外，此版本还包括大量新的生物和建筑、改变了的平衡性和几个新战役。《追随神迹》仅能运行于Windows平台。

## 评价

《英雄无敌III》发行后获得了良好的评价。\[2\]\[3\]它在英雄无敌系列中被广泛地赞誉为最佳作品。

## 魔法门之英雄无敌III完整版

《魔法门之英雄无敌III完整版（收藏版）》包括了原版游戏、《末日之刃》资料片、《死亡阴影》资料片和大量的特别元素。它同时兼容Windows和Macintosh，能让使用Macintosh的玩家体验到原来因为运行平台的原因而无法玩到的两个资料片和随机地图生成器。

## 參考資料

## 外部链接

  - [《魔法门之英雄无敌三》中文手册下载](https://web.archive.org/web/20051225043139/http://www.wakeofgods.com/soul.htm)
  - [《魔法门之英雄无敌三》中文攻略网站](http://www.heroworld.net/heroes3site/index.htm)
  - [《魔法门之英雄无敌三》对战联盟](http://www.heroworld.net/heroes4site/cha/main.asp)（中文）
  - [Age of Heroes 英雄无敌爱好者网站](http://www.heroesofmightandmagic.com/)（英语）
  - [Heroes 3 wiki or Heroes of Might and Magic III wiki -
    英雄无敌3及其资料片的维基站点](http://heroes.thelazy.net/)（英语）

[Category:1999年电子游戏](../Category/1999年电子游戏.md "wikilink")
[3](../Category/魔法门之英雄无敌系列.md "wikilink")
[Category:回合制策略遊戲](../Category/回合制策略遊戲.md "wikilink")
[Category:New World
Computing游戏](../Category/New_World_Computing游戏.md "wikilink")
[Category:3DO公司游戏](../Category/3DO公司游戏.md "wikilink") [Category:Mac
OS遊戲](../Category/Mac_OS遊戲.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:Linux游戏](../Category/Linux游戏.md "wikilink")
[Category:製作中止的Dreamcast遊戲](../Category/製作中止的Dreamcast遊戲.md "wikilink")
[Category:有资料片的游戏](../Category/有资料片的游戏.md "wikilink")

1.  最初名字是神之苏醒，但目前更多被利用的中文译名为追随神迹。
2.  [《魔法门之英雄无敌III》回顾](http://www.gamespot.com/pc/strategy/heroesofmightandmagic3/review.html)
    来自*[Gamespot](../Page/Gamespot.md "wikilink")*（英语）
3.  [《魔法门之英雄无敌III》：没有坏，就别修——英雄无敌系列的回归](http://pc.ign.com/articles/160/160222p1.html)
    来自*[IGN](../Page/IGN.md "wikilink")*（英语）