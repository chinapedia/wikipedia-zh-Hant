[Stingray_2016-9.jpg](https://zh.wikipedia.org/wiki/File:Stingray_2016-9.jpg "fig:Stingray_2016-9.jpg")
**魟科**，又名**土魟科**，是[軟骨魚綱](../Page/軟骨魚綱.md "wikilink")[燕魟目的一](../Page/燕魟目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")。

## 分布

本科魚類廣泛分布於全球各大洋和淡水流域。

## 深度

水深3至200公尺以上。

## 分類

**魟科**下分11個屬，如下：

### 雙褶魟屬（*Amphotistius*）

  - [覆瓦雙褶魟](../Page/覆瓦雙褶魟.md "wikilink")（*Amphotistius imbricatus*）
  - [老撾雙褶魟](../Page/老撾雙褶魟.md "wikilink")（*Amphotistius laosensis*）
  - [大西洋雙褶魟](../Page/大西洋雙褶魟.md "wikilink")（*Amphotistius sabina*）

### 魟屬（*Dasyatis*）

  - [尖吻土魟](../Page/尖吻土魟.md "wikilink")（*Dasyatis acutirostra*）：又稱尖吻魟。
  - [赤土魟](../Page/赤土魟.md "wikilink")（*Dasyatis akajei*）：又稱赤魟。
  - [美洲魟](../Page/美洲魟.md "wikilink")（*Dasyatis americana*）
  - [褐魟](../Page/褐魟.md "wikilink")（*Dasyatis annotatus*）
  - [黑魟](../Page/黑魟.md "wikilink")（*Dasyatis atratus*）
  - [黃土魟](../Page/黃土魟.md "wikilink")（*Dasyatis bennetti*）：又稱黃魟。
  - [短尾魟](../Page/短尾魟.md "wikilink")（*Dasyatis brevicaudatus*）
  - [鞭尾魟](../Page/鞭尾魟.md "wikilink")（*Dasyatis brevis*）
  - [深魟](../Page/深魟.md "wikilink")（*Dasyatis brucco*）
  - [粗尾魟](../Page/粗尾魟.md "wikilink")（*Dasyatis centroura*）
  - [金魟](../Page/金魟.md "wikilink")（*Dasyatis chrysonota*）
  - [菱魟](../Page/菱魟.md "wikilink")（*Dasyatis dipterura*）
  - [溪魟](../Page/溪魟.md "wikilink")（*Dasyatis fluviorum*）
  - [加魯阿魟](../Page/加魯阿魟.md "wikilink")（*Dasyatis garouaensis*）
  - [吉氏魟](../Page/吉氏魟.md "wikilink")（*Dasyatis geijskesi*）
  - [齊氏魟](../Page/齊氏魟.md "wikilink")（*Dasyatis gerrardi*）
  - [巨魟](../Page/巨魟.md "wikilink")（*Dasyatis giganteus*）
  - [吉勒氏魟](../Page/吉勒氏魟.md "wikilink")（*Dasyatis guileri*）
  - [長吻魟](../Page/長吻魟.md "wikilink")（*Dasyatis guttata*）
  - [伊豆魟](../Page/伊豆魟.md "wikilink")（*Dasyatis izuensis*）
  - [詹氏魟](../Page/詹氏魟.md "wikilink")（*Dasyatis jenkinsii*）
  - [古氏土魟](../Page/古氏土魟.md "wikilink")（*Dasyatis kuhlii*）
  - [光土魟](../Page/光土魟.md "wikilink")（*Dasyatis laevigata*）
  - [光魟](../Page/光魟.md "wikilink")（*Dasyatis laevigatus*）
  - [鬼土魟](../Page/鬼土魟.md "wikilink")（*Dasyatis latus*）：又稱鬼魟。
  - [萊氏魟](../Page/萊氏魟.md "wikilink")（*Dasyatis leylandi*）
  - [長魟](../Page/長魟.md "wikilink")（*Dasyatis longus*）
  - [珠粒魟](../Page/珠粒魟.md "wikilink")（*Dasyatis margarita*）
  - [珍魟](../Page/珍魟.md "wikilink")（*Dasyatis margaritella*）
  - [花背魟](../Page/花背魟.md "wikilink")（*Dasyatis marmorata*）
  - [松原魟](../Page/松原魟.md "wikilink")（*Dasyatis matsubarai*）
  - [黑斑魟](../Page/黑斑魟.md "wikilink")（*Dasyatis melanospilos*）
  - [小眼土魟](../Page/小眼土魟.md "wikilink")（*Dasyatis microphthalmus*）：又稱小眼魟。
  - [細眼魟](../Page/細眼魟.md "wikilink")（*Dasyatis microps*）
  - [黑土魟](../Page/黑土魟.md "wikilink")（*Dasyatis navarrae*）：又稱奈氏魟。
  - [藍紋魟](../Page/藍紋魟.md "wikilink")（*Dasyatis pastinaca*）：又稱光魟。
  - [糙魟](../Page/糙魟.md "wikilink")（*Dasyatis rudis*）
  - [薩氏魟](../Page/薩氏魟.md "wikilink")（*Dasyatis sayi*）
  - [中國魟](../Page/中國魟.md "wikilink")（*Dasyatis sinensis*）
  - [棘尾魟](../Page/棘尾魟.md "wikilink")（*Dasyatis thetidis*）
  - [托氏魟](../Page/托氏魟.md "wikilink")（*Dasyatis tortonesei*）
  - [花點魟](../Page/花點魟.md "wikilink")（*Dasyatis uarnak*）
  - [尤克魟](../Page/尤克魟.md "wikilink")（*Dasyatis ukpam*）
  - [牛土魟](../Page/牛土魟.md "wikilink")（*Dasyatis ushiei*）：又稱尤氏魟。
  - [紫魟](../Page/紫魟.md "wikilink")（*Dasyatis violacea*）
  - [尖嘴土魟](../Page/尖嘴土魟.md "wikilink")（*Dasyatis zugei*）：又稱尖嘴魟。

### 河盤魟屬（*Disceus*）

  - [蘋果魟](../Page/蘋果魟.md "wikilink")（*Disceus aiereba*）

### 窄尾魟屬（*Himantura*）

  - [阿氏窄尾魟](../Page/阿氏窄尾魟.md "wikilink")（*Himantura alcocki*）
  - [布氏窄尾魟](../Page/布氏窄尾魟.md "wikilink")（*Himantura bleekeri*）
  - [疣突窄尾魟](../Page/疣突窄尾魟.md "wikilink")（*Himantura draco*）
  - [費氏窄尾魟](../Page/費氏窄尾魟.md "wikilink")（*Himantura fai*）
  - [蜂巢窄尾魟](../Page/蜂巢窄尾魟.md "wikilink")（*Himantura fava*）
  - [河棲窄尾魟](../Page/河棲窄尾魟.md "wikilink")（*Himantura fluviatilis*）
  - [齊氏魟](../Page/齊氏魟.md "wikilink")（*Himantura gerrardi*）：又稱杰氏窄尾魟。
  - [細點窄尾魟](../Page/細點窄尾魟.md "wikilink")（*Himantura granulata*）
  - [詹氏窄尾魟](../Page/詹氏窄尾魟.md "wikilink")（*Himantura jenkinsii*）
  - [克氏窄尾魟](../Page/克氏窄尾魟.md "wikilink")（*Himantura krempfi*）
  - [緣邊窄尾魟](../Page/緣邊窄尾魟.md "wikilink")（*Himantura marginata*）
  - [小眼魟](../Page/小眼魟.md "wikilink")（*Himantura microphthalma*）
  - [尖吻窄尾魟](../Page/尖吻窄尾魟.md "wikilink")（*Himantura oxyrhynchus*）
  - [太平洋窄尾魟](../Page/太平洋窄尾魟.md "wikilink")（*Himantura pacifica*）
  - [施氏窄尾魟](../Page/施氏窄尾魟.md "wikilink")（*Himantura schmardae*）
  - [大窄尾魟](../Page/大窄尾魟.md "wikilink")（*Himantura signifer*）
  - [斑點窄尾魟](../Page/斑點窄尾魟.md "wikilink")（*Himantura toshi*）
  - [豹紋魟](../Page/豹紋魟.md "wikilink")（*Himantura uarnak*）：又稱黃線窄尾魟。
  - [波緣窄尾魟](../Page/波緣窄尾魟.md "wikilink")（*Himantura undulata*）
  - [沃爾窄尾魟](../Page/沃爾窄尾魟.md "wikilink")（*Himantura walga*）

### 新魟屬（*Neotrygon*）

  - [古氏土魟](../Page/古氏土魟.md "wikilink")（*Neotrygon kuhlii*）：又稱古氏魟。
  - [似新魟](../Page/似新魟.md "wikilink")（*Neotrygon trigonoidae*）

### 副江魟屬（*Paratrygon*）

  - [巴西副江魟](../Page/巴西副江魟.md "wikilink")（*Paratrygon aiereba*）

### 蘿蔔魟屬（*Pastinachus*）

  - [褶尾蘿蔔魟](../Page/褶尾蘿蔔魟.md "wikilink")（*Pastinachus sephen*）

### 近江魟屬（*Plesiotrygon*）

  - [近江魟](../Page/近江魟.md "wikilink")（*Plesiotrygon iwamae*）
  - [天線滿天星魟](../Page/天線滿天星魟.md "wikilink")（*Plesiotrygon iwmae*）

### 河魟屬（*Potamotrygon* ）

  - [黑白魟](../Page/黑白魟.md "wikilink")（*Potamotrygon Leopoldi*）
  - [珍珠魟](../Page/珍珠魟.md "wikilink")（*Potamotrygon motoro*）
  - [迷你魟](../Page/迷你魟.md "wikilink")（*Potamotrygon reticulata*）
  - [帝王魟](../Page/帝王魟.md "wikilink")（*Potamotrygon menchacai*）
  - [金點魟](../Page/金點魟.md "wikilink")（*Potamotrygon henlei*）
  - [泰魯魟](../Page/泰魯魟.md "wikilink")（*Potamotrygon castexi var*）
  - [龜甲魟](../Page/龜甲魟.md "wikilink")（*potamotrygon humerosa*）
  - [綠豆魟](../Page/綠豆魟.md "wikilink")(*Potamotrygon castex*)
  - [卡氏江魟](../Page/卡氏江魟.md "wikilink")(*Potamotrygon falkneri*)

### 條尾魟屬（*Taeniura*）

  - [馬德拉群島條尾魟](../Page/馬德拉群島條尾魟.md "wikilink")（*Taeniura altavela*）
  - [亞馬遜河條尾魟](../Page/亞馬遜河條尾魟.md "wikilink")（*Taeniura constellata*）
  - [杜氏條尾魟](../Page/杜氏條尾魟.md "wikilink")（*Taeniura dumerilii*）
  - [圓條尾魟](../Page/圓條尾魟.md "wikilink")（*Taeniura grabata*）
  - [巴西條尾魟](../Page/巴西條尾魟.md "wikilink")（*Taeniura henlei*）
  - [藍斑條尾魟](../Page/藍斑條尾魟.md "wikilink")（*Taeniura lymma*）：又稱藍點魟。
  - [邁氏條尾魟](../Page/邁氏條尾魟.md "wikilink")（*Taeniura meyeni*）

### 沙粒魟屬（*Urogymnus*）

  - [糙沙粒魟](../Page/糙沙粒魟.md "wikilink")（*Urogymnus asperrimus*）
  - [南非沙粒魟](../Page/南非沙粒魟.md "wikilink")（*Urogymnus natalensis*）
  - [多鱗沙粒魟](../Page/多鱗沙粒魟.md "wikilink")（*Urogymnus
    polylepis*）：又稱**黃貂魚**、查菲窄尾魟。

## 特徵

魟科[鱼和](../Page/鱼.md "wikilink")[鳐科鱼不同的地方是腰带中轴软骨是弯的](../Page/鳐科.md "wikilink")，因此体盘中间圆滑的鼓起，不像[鳐科鱼中间有一明显的鼓棱](../Page/鳐科.md "wikilink")。魟科鱼体盘宽小于体盘的长，尾细长，較體常之左右徑為長。尾部上方具有毒強棘1枚，平扁，有向下之鋸齒。口底乳突長短不一。
魟魚尾部的刺有劇毒，一旦被刺傷會有強烈的疼痛，如果患者拖太久沒送醫急救可能會危及生命，在海邊逗魟魚玩不慎被刺傷的話要馬上泡熱水防止毒性導致心臟衰竭，並且趕緊就醫觀察6小時。

## 生態

本科魚類廣泛分布於全球各大洋,河流。有些具有地域性，大部分為海水魚，有時見於河口，一部分則生活於南美洲、非洲及東南亞淡水水域。活動力差，通常將身體埋於沙泥中，僅露出雙眼和呼吸孔，或利用胸鰭做波浪狀的運動而貼游於底層水域。屬肉食性，以小魚、甲殼類及軟體動物為食。卵胎生。

## 經濟利用

[Stingray_wallets.JPG](https://zh.wikipedia.org/wiki/File:Stingray_wallets.JPG "fig:Stingray_wallets.JPG")的魟鱼皮钱包\]\]
食用魚，但味道較腥，以紅燒為宜，或當作下雜魚處理。鱼皮可制成各种皮带，皮包等用品。

## 参考文献

  - [FishBase](http://fishbase.sinica.edu.tw/)
  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

## 參照

  - [鲼科](../Page/鲼科.md "wikilink")

## 外部連線

  - [图片](https://web.archive.org/web/20040811090305/http://www.photovault.com/Link/Animals/Aquatic/Chondrichthyes/Species/StingrayDasyatis.html)

<!-- end list -->

  - [台灣觀賞魚圖庫 - - 魟魚](http://www.twaa.org.tw/twaa/type.asp?typeid=27/)

[\*](../Category/魟科.md "wikilink")