**魯文傑**（，）\[1\]，香港男演員，前[香港](../Page/香港.md "wikilink")[無綫電視及](../Page/無綫電視.md "wikilink")[香港電視網絡合約藝員](../Page/香港電視網絡.md "wikilink")，及[三號排練室劇團團員](../Page/三號排練室劇團.md "wikilink")。

## 经历

魯文傑早年於[葵盛邨長大](../Page/葵盛邨.md "wikilink")，中學就讀[樂善堂顧超文中學](../Page/樂善堂顧超文中學.md "wikilink")，1996年畢業於[香港演藝學院戲劇學院](../Page/香港演藝學院.md "wikilink")。魯文傑曾是香港[兒童節目](../Page/兒童節目.md "wikilink")[閃電傳真機主持人](../Page/閃電傳真機.md "wikilink")，另担任1994年[TVB兒童節大使](../Page/TVB兒童節.md "wikilink")。魯文傑在1992年亦曾拍攝一輯[維他奶飲品廣告](../Page/維他奶.md "wikilink")，當時更前往[美國三藩市實地取景](../Page/美國.md "wikilink")。

魯文傑曾於1997年的《[鑑證實錄](../Page/鑑證實錄.md "wikilink")》擔當曾家喬一角，當時演主角的弟弟、戲份還算頗多，後來《[迷離檔案](../Page/迷離檔案.md "wikilink")》、《[鑑證實錄II](../Page/鑑證實錄II.md "wikilink")》都演主角弟弟的角色，本來以為會慢慢朝一線男主角進軍，沒想到後來戲份越來越少，從二線男主角慢慢變成重要的[甘草演員](../Page/甘草演員.md "wikilink")。

魯文傑在話劇界的發展較電視媒體好，曾經憑《[藍月亮](../Page/藍月亮_\(舞臺劇\).md "wikilink")》、《[野鴨](../Page/野鴨_\(舞臺劇\).md "wikilink")》及《[大顛世界](../Page/大顛世界.md "wikilink")》獲提名[香港舞台劇獎最佳男配角及最佳男主角](../Page/香港舞台劇獎.md "wikilink")，並於2008年憑[糊塗戲班的](../Page/糊塗戲班.md "wikilink")《[爆谷殺人狂](../Page/爆谷殺人狂.md "wikilink")》獲得香港舞台劇獎最佳男配角（喜劇／鬧劇）。\[2\]\[3\]2007年，魯文傑更初次自編自導自演《[看更](../Page/看更_\(舞台劇\).md "wikilink")》一劇；2009年4月重演，同時邀得[湯盈盈擔任女主角](../Page/湯盈盈.md "wikilink")。\[4\]\[5\]現為[香港話劇團戲劇課程導師及](../Page/香港話劇團.md "wikilink")[海洋公園合約節目經理](../Page/海洋公園.md "wikilink")。

## 演出作品

### 電影

  - 1993年：[搶錢夫妻](../Page/搶錢夫妻.md "wikilink")
  - 1994年：[清官難審](../Page/清官難審.md "wikilink")（與[金超群](../Page/金超群.md "wikilink")、[何家勁](../Page/何家勁.md "wikilink")、[周慧敏合演](../Page/周慧敏.md "wikilink")）

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

#### 1997年

  - [迷離檔案](../Page/迷離檔案.md "wikilink") 飾
    文嘉浩（《[輪迴](../Page/輪迴.md "wikilink")》單元主人翁）
  - [鑑證實錄](../Page/鑑證實錄.md "wikilink") 飾 曾家喬

#### 1998年

  - [網上有情人](../Page/網上有情人.md "wikilink") 飾 郭浩南

#### 1999年

  - [鑑證實錄II](../Page/鑑證實錄II.md "wikilink") 飾 曾家喬
  - [刑事偵緝檔案IV](../Page/刑事偵緝檔案IV.md "wikilink") 飾 馬力（作證時加入自己的想像，導致一連串的悲劇）

#### 2000年

  - [緣份無邊界](../Page/緣份無邊界.md "wikilink") 飾 金 吉
  - [廟街·媽·兄弟](../Page/廟街·媽·兄弟.md "wikilink") 飾 郭家輝

#### 2001年

  - [婚前昏後](../Page/婚前昏後.md "wikilink") 飾 羅金寶

#### 2002年

  - [無考不成冤家](../Page/無考不成冤家.md "wikilink") 飾 士巴拿
  - [蕭十一郎](../Page/蕭十一郎_\(2002年香港電視劇\).md "wikilink") 飾 法淨
  - [再生緣 (2002年電視劇)](../Page/再生緣_\(2002年電視劇\).md "wikilink") 饰 鲁智双

#### 2003年

  - [金牌冰人](../Page/金牌冰人.md "wikilink") 飾 唐文笙
  - [衝上雲霄](../Page/衝上雲霄.md "wikilink") 飾 朱潤田（Leslie）
  - [西關大少](../Page/西關大少.md "wikilink") 飾 Peter（第20集）
  - [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink") 飾 健（第162集）

#### 2005年

  - [甜孫爺爺](../Page/甜孫爺爺.md "wikilink") 飾 電視台節目主持
  - [酒店風雲](../Page/酒店風雲.md "wikilink") 飾 趙信明

#### 2006年

  - [施公奇案](../Page/施公奇案_\(香港\).md "wikilink") 飾 陳明
  - [謎情家族](../Page/謎情家族.md "wikilink") 飾 傻仔
  - [法證先鋒](../Page/法證先鋒.md "wikilink") 飾 張家樂
  - [鳳凰四重奏](../Page/鳳凰四重奏.md "wikilink") 飾 亞　坤 (David)、酒　客
  - [刑事情報科](../Page/刑事情報科_\(電視劇\).md "wikilink") 飾 酒客

#### 2007年

  - [十兄弟](../Page/十兄弟_\(無綫電視劇集\).md "wikilink") 飾 光師兄
  - [師奶兵團](../Page/師奶兵團.md "wikilink") 飾 Peter
  - [歲月風雲](../Page/歲月風雲.md "wikilink") 飾 鋼鐵廠員工
  - [奸人堅](../Page/奸人堅.md "wikilink") 飾 方有成
  - [兩妻時代](../Page/兩妻時代.md "wikilink") 飾 蛋散

#### 2008年

  - [原來愛上賊](../Page/原來愛上賊.md "wikilink") 飾 司機
  - [少年四大名捕](../Page/少年四大名捕_\(無綫電視劇\).md "wikilink") 飾 金國世子

#### 2009年

  - [老婆大人II](../Page/老婆大人II.md "wikilink") 飾 亞　貓（We Rap Bar調酒師）
  - [學警狙擊](../Page/學警狙擊.md "wikilink") 飾 克
  - [大冬瓜](../Page/大冬瓜_\(電視劇\).md "wikilink") 飾 金有福
  - [桌球天王](../Page/桌球天王.md "wikilink") 飾 靳曉高
  - [畢打自己人](../Page/畢打自己人.md "wikilink") 飾 Nothing哥

#### 2010年

  - [鐵馬尋橋](../Page/鐵馬尋橋.md "wikilink") 飾 陳博文
  - [搜下留情](../Page/搜下留情.md "wikilink") 飾 吳仲健
  - [飛女正傳](../Page/飛女正傳.md "wikilink") 飾 周大茂
  - [掌上明珠](../Page/掌上明珠.md "wikilink") 飾 阿元
  - [巾幗梟雄之義海豪情](../Page/巾幗梟雄之義海豪情.md "wikilink") 飾 車永平
  - [依家有喜](../Page/依家有喜.md "wikilink") 飾 魯連傑

#### 2011年

  - [誰家灶頭無煙火](../Page/誰家灶頭無煙火.md "wikilink") 飾 Peter
  - [法證先鋒III](../Page/法證先鋒III.md "wikilink") 飾 王頌安

#### 2012年

  - [On Call 36小時](../Page/On_Call_36小時.md "wikilink") 飾 林樹森
  - [東西宮略](../Page/東西宮略.md "wikilink") 飾 夏送秋
  - [當旺爸爸](../Page/當旺爸爸.md "wikilink") 飾 黃達雄
  - [造王者](../Page/造王者_\(電視劇\).md "wikilink") 飾 方良/張東/張發
  - [巴不得媽媽...](../Page/巴不得媽媽....md "wikilink") 飾 范禮金
  - [我外母唔係人](../Page/我外母唔係人.md "wikilink") 飾 雷震子

#### 2013年

  - [神探高倫布](../Page/神探高倫布.md "wikilink") 飾 王志雄
  - [凶城計中計](../Page/凶城計中計.md "wikilink") 飾 村民

### 電視劇（[香港電視網絡](../Page/香港電視網絡.md "wikilink")）

#### 2014年

  - [選戰](../Page/選戰.md "wikilink") 飾 連志遠
  - [來生不做香港人](../Page/來生不做香港人.md "wikilink") 飾 堅

#### 2015年

  - [我阿媽係黑玫瑰](../Page/我阿媽係黑玫瑰.md "wikilink") 飾 史公子（第7集）
  - [導火新聞線](../Page/導火新聞線.md "wikilink") 飾 黎克儉

### 電視劇（[邵氏兄弟](../Page/邵氏兄弟.md "wikilink")）

#### 2018年

  - [守護神之保險調查](../Page/守護神之保險調查.md "wikilink") 飾 阿　榮

### 電視劇（[香港電視娛樂](../Page/香港電視娛樂.md "wikilink") - [ViuTV](../Page/ViuTV.md "wikilink")）

#### 2016年

  - [三一如三](../Page/三一如三.md "wikilink") 飾 鍾友（第5至6集）

#### 2018年

  - [身後事務所](../Page/身後事務所.md "wikilink") 飾 大蝦（第9至10集）

### 節目主持（[無線電視](../Page/無線電視.md "wikilink")）

  - 1993-1997年：[閃電傳真機](../Page/閃電傳真機.md "wikilink")
  - 1994年：[TVB兒童節大使](../Page/TVB兒童節.md "wikilink")
  - 1996年: 發發發，發到飛起
  - [為食開心果](../Page/為食開心果.md "wikilink")

### 廣告

  - 1992年：[維他奶](../Page/維他奶.md "wikilink")

### 舞台劇

  - [香港演藝學院](../Page/香港演藝學院.md "wikilink")《[聖女貞德](../Page/聖女貞德.md "wikilink")》
  - [香港演藝學院](../Page/香港演藝學院.md "wikilink")《[秋城故事](../Page/秋城故事.md "wikilink")》
  - [香港演藝學院](../Page/香港演藝學院.md "wikilink")《[夢斷城西](../Page/夢斷城西.md "wikilink")》
  - [香港演藝學院](../Page/香港演藝學院.md "wikilink")《[第十二夜](../Page/第十二夜.md "wikilink")》
  - [香港演藝學院](../Page/香港演藝學院.md "wikilink")《[片片謊言夢裡尋](../Page/片片謊言夢裡尋.md "wikilink")》
  - [香港演藝學院](../Page/香港演藝學院.md "wikilink")《[無政府主義者的意外死亡](../Page/無政府主義者的意外死亡.md "wikilink")》
  - [香港演藝學院](../Page/香港演藝學院.md "wikilink")《[藍月亮](../Page/藍月亮.md "wikilink")》
  - [香港演藝學院](../Page/香港演藝學院.md "wikilink")《[物理學家](../Page/物理學家.md "wikilink")》
  - [觀塘劇團](../Page/觀塘劇團.md "wikilink")《[扭計情殺案](../Page/扭計情殺案.md "wikilink")》
  - [赫墾坊劇團](../Page/赫墾坊劇團.md "wikilink")《[龍情化不開](../Page/龍情化不開.md "wikilink")》
  - [劇場組合](../Page/劇場組合.md "wikilink")《[大小不良](../Page/大小不良.md "wikilink")》
  - [演戲家族](../Page/演戲家族.md "wikilink")《[人盡．訶夫](../Page/人盡．訶夫.md "wikilink")》
  - [演戲家族](../Page/演戲家族.md "wikilink")《[蛋散與豬扒-地盤工人札記之純屬意外](../Page/蛋散與豬扒-地盤工人札記之純屬意外.md "wikilink")》
  - [演戲家族](../Page/演戲家族.md "wikilink")《[一屋寶貝](../Page/一屋寶貝.md "wikilink")》三度公演
  - [劇場工作室](../Page/劇場工作室.md "wikilink")《[愛上愛上誰人的新娘](../Page/愛上愛上誰人的新娘.md "wikilink")》
  - [香港戲劇協會](../Page/香港戲劇協會.md "wikilink")《[麻煩家姊妹花](../Page/麻煩家姊妹花.md "wikilink")》
  - [香港戲劇協會](../Page/香港戲劇協會.md "wikilink")《[大建築師](../Page/大建築師.md "wikilink")》
  - [香港戲劇協會](../Page/香港戲劇協會.md "wikilink")《[承受清風](../Page/承受清風.md "wikilink")》
  - [無人地帶](../Page/無人地帶.md "wikilink")《[活在背叛愛的時代](../Page/活在背叛愛的時代.md "wikilink")》
  - [同流工作坊](../Page/同流工作坊.md "wikilink")《[脫色](../Page/脫色.md "wikilink")》
  - [香港藝術節](../Page/香港藝術節.md "wikilink")《[竹取物語](../Page/竹取物語.md "wikilink")》
  - [毛俊輝實驗劇場](../Page/毛俊輝.md "wikilink")《[跟住嗰靚妹氹氹轉](../Page/跟住嗰靚妹氹氹轉.md "wikilink")》
  - 《[情場響尾蛇](../Page/情場響尾蛇.md "wikilink")》
  - [香港戲劇協會](../Page/香港戲劇協會.md "wikilink")《[野鴨](../Page/野鴨.md "wikilink")》（2006）
  - [糊塗戲班](../Page/糊塗戲班.md "wikilink")《[爆谷殺人狂](../Page/爆谷殺人狂.md "wikilink")》（2007）
  - 《[看更](../Page/看更_\(舞台劇\).md "wikilink")》（2007年，自編自導自演）
  - 香港戲劇協會《[大顛世界](../Page/大顛世界.md "wikilink")》（2008年，與[唐寧等演員合演](../Page/唐寧_\(香港\).md "wikilink")）
  - 《看更》（2009年4月，重演，與[湯盈盈等演員合演](../Page/湯盈盈.md "wikilink")
  - [香港藝術節](../Page/香港藝術節.md "wikilink")《[情話紫釵](../Page/情話紫釵.md "wikilink")》

## 注釋

## 参考文献

<div class="references-small">

<references />

</div>

[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:前香港電視網絡藝員](../Category/前香港電視網絡藝員.md "wikilink")
[Category:香港舞臺劇演員](../Category/香港舞臺劇演員.md "wikilink")
[Category:香港兒童節目主持](../Category/香港兒童節目主持.md "wikilink")
[Category:香港演藝學院校友](../Category/香港演藝學院校友.md "wikilink")
[Category:樂善堂顧超文中學校友](../Category/樂善堂顧超文中學校友.md "wikilink")
[MAN](../Category/魯姓.md "wikilink")

1.  [鄧萃雯Blog\>\>兩位好同事,生日快樂\!](http://tangshuiman.mysinablog.com/index.php?op=ViewArticle&articleId=2287546)
2.
3.
4.
5.  [《看更》之網誌](http://hk.myblog.yahoo.com/nsm.watcher)