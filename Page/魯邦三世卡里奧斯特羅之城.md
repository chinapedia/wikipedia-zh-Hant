《**魯邦三世‧卡里奧斯特羅城**》（），(港譯:無敵特務
偽鈔堡壘)是於1979年12月15日上映的《[魯邦三世](../Page/魯邦三世.md "wikilink")》系列[動畫電影第二部作品](../Page/動畫電影.md "wikilink")，也是[宮崎駿首次執導的動畫長片](../Page/宮崎駿.md "wikilink")。片長100分鐘。

## 概要

當初負責製作本片的[東京電影新社](../Page/東京電影新社.md "wikilink")（），是以[鈴木清順等](../Page/鈴木清順.md "wikilink")《魯邦三世》TV版第二部（通稱「新魯邦」）與前作電影版《魯邦三世
魯邦VS複製人》（）的編劇小組所寫的[劇本](../Page/劇本.md "wikilink")，邀請[大塚康生擔任](../Page/大塚康生.md "wikilink")[導演](../Page/導演.md "wikilink")。無意執導本片的大塚推薦[宮崎駿來執導](../Page/宮崎駿.md "wikilink")。當時[宮崎駿正在](../Page/宮崎駿.md "wikilink")[日本動畫公司](../Page/日本動畫公司.md "wikilink")（）擔任[高畑勲的](../Page/高畑勲.md "wikilink")《[清秀佳人](../Page/清秀佳人_\(動畫\).md "wikilink")》（）構圖與分鏡工作，在答應接手本片後離開了《清》片製作團隊，於1979年5月開始進入本片的製作準備。本片與[宮崎駿後來的作品中所採行的做法相同](../Page/宮崎駿.md "wikilink")，他不寫劇本，而是直接先畫出概念圖與分鏡，正式的劇本則是由共同掛名的山崎晴哉重新撰寫。

大塚原本為TV版第一部（通稱「舊魯邦」）的作畫監督，[宮崎駿與高畑也曾經共同以](../Page/宮崎駿.md "wikilink")「A製作演出集團」（）的匿名名義，擔任TV版第一部後半的各集導演工作。因此宮崎與大塚設計的人物與小道具、概念圖均以第一部為基準，當時TV版第二部與電影版前作《魯邦VS複製人》中魯邦身穿的紅色上衣，在本片中改回了TV版第一部的綠色。

由於本片受當時的科幻熱潮下所影響，造成上映票房成績遠遜於前作（僅約為前作的一半左右），結果還使得[宮崎駿之後在業界](../Page/宮崎駿.md "wikilink")，更出現了三年無工作可接的空窗期。但本片後來在電視播映與上映會中，卻開始漸漸廣獲好評，而[宮崎駿的導演與構圖手法](../Page/宮崎駿.md "wikilink")，也對之後的動畫業界帶來了影響。但對於這部在構想、製作期間僅半年之下所拍出的電影，[宮崎駿也曾經表示](../Page/宮崎駿.md "wikilink")：「在拍這部作品的過程中，頭一次體會到自己體力的極限」。

## 劇情大綱

世界聞名的怪盗[魯邦三世及其搭檔](../Page/魯邦三世.md "wikilink")[次元大介](../Page/次元大介.md "wikilink")，成功從[摩納哥國營](../Page/摩納哥.md "wikilink")[賭場的金庫中盜走一筆鉅款](../Page/賭場.md "wikilink")，並甩開追兵乘車逃走。兩人在車内雖然可說是被埋在[鈔票堆裡](../Page/鈔票.md "wikilink")，但眼尖的魯邦卻發現，這是史上製作最精巧的夢幻[偽鈔](../Page/偽鈔.md "wikilink")「哥德鈔」。在魯邦「不偷假貨」的原則下，他將哥德鈔毫不吝惜地撒在路上，並選定了他的下一個目標就是要揭開哥德鈔的秘密。於是魯邦出發前往疑雲重重的[歐洲獨立](../Page/歐洲.md "wikilink")[國家](../Page/國家.md "wikilink")「卡里奧斯特羅公國」。

魯邦與次元兩人改變身份潛入卡里奧斯特羅公國，但車子卻在路上爆胎。在修[輪胎時](../Page/輪胎.md "wikilink")，他們發現一名身穿[結婚禮服的少女被惡漢所追](../Page/結婚.md "wikilink")，於是出手擊退惡漢，救了墜崖的少女。但留下了一枚刻有[哥德文字](../Page/哥德文字.md "wikilink")[戒指的少女](../Page/戒指.md "wikilink")，卻被另一群人給帶走。之後證實，那名少女正是卡里奧斯特羅公國[大公家的繼承人克蕾莉絲](../Page/大公.md "wikilink")‧德‧卡里奧斯特羅。目前由於現任大公猝逝，由萊瑟‧德‧卡里奧斯特羅[伯爵擔任](../Page/伯爵.md "wikilink")[攝政](../Page/攝政.md "wikilink")，留下了大公之位的空缺。

身為卡里奧斯特羅公國實質[統治者的萊瑟](../Page/統治者.md "wikilink")，打算藉由娶克蕾莉絲為妻，取得大公之位，進行[獨裁統治](../Page/獨裁.md "wikilink")。再度成為階下囚的克蕾莉絲，被關進了萊瑟的居城卡里奧斯特羅城。魯邦為了將她救出，找來了老戰友[石川五右衛門](../Page/石川五右衛門_\(魯邦三世\).md "wikilink")。但得知魯邦對萊瑟發下預告書的[錢形警部](../Page/錢形警部.md "wikilink")，也率領了警官隊（[埼玉縣](../Page/埼玉縣.md "wikilink")[警察局的機動隊](../Page/警察局.md "wikilink")）前來，準備[逮捕魯邦](../Page/逮捕.md "wikilink")。加上早已變裝為[家庭教師潛入卡里奧斯特羅城的](../Page/家庭教師.md "wikilink")[峰不二子在內](../Page/峰不二子.md "wikilink")，「魯邦家族」至此全數到齊了。以卡里奧斯特羅城為舞台，為了救出克蕾莉絲與揭開哥德鈔之謎，展開了一場大混戰。

## 工作人員

  - [製作](../Page/執行製作人.md "wikilink") - [藤岡豐](../Page/藤岡豐.md "wikilink")
  - 原作 - [Monkey Punch](../Page/モンキー・パンチ.md "wikilink")
  - 导演- [宮崎駿](../Page/宮崎駿.md "wikilink")
  - [监制](../Page/电影监制.md "wikilink") -
    [片山哲生](../Page/片山哲生.md "wikilink")
  - 编剧 - 宮崎駿、[山崎晴哉](../Page/山崎晴哉.md "wikilink")
  - 音乐 - [大野雄二](../Page/大野雄二.md "wikilink")
  - 选曲 - [鈴木清司](../Page/鈴木清司.md "wikilink")
  - [作画监督](../Page/作画监督.md "wikilink") -
    [大塚康生](../Page/大塚康生.md "wikilink")
  - [美术](../Page/美术指导.md "wikilink") -
    [小林七郎](../Page/小林七郎.md "wikilink")
  - 录音 -
    [加藤敏](../Page/加藤敏.md "wikilink")（[東北新社](../Page/東北新社.md "wikilink")）
  - [音效](../Page/音效.md "wikilink") -
    [倉橋静男](../Page/倉橋静男.md "wikilink")（[東洋音響](../Page/東洋音響効果グループ.md "wikilink")）
  - [原画](../Page/原画.md "wikilink") -
    [篠原征子](../Page/篠原征子.md "wikilink")、[友永和秀](../Page/友永和秀.md "wikilink")、[河内日出夫](../Page/河内日出夫.md "wikilink")、[富澤信雄](../Page/富澤信雄.md "wikilink")、[丹内司](../Page/丹内司.md "wikilink")、[山内昇寿郎](../Page/山内昇寿郎.md "wikilink")、[丸山晃二](../Page/丸山晃二.md "wikilink")、[真鍋讓二](../Page/真鍋讓二.md "wikilink")、[田中敦子](../Page/田中敦子_\(动画师\).md "wikilink")、[新川信正](../Page/新川信正.md "wikilink")
  - 制作合作 - Telecom Animation Film
  - 发行商 - [東宝](../Page/東宝.md "wikilink")
  - 制作 - [东京电影新社](../Page/东京电影新社.md "wikilink")

## 配音

  - 魯邦三世：[山田康雄](../Page/山田康雄.md "wikilink")
  - 次元大介：[小林清志](../Page/小林清志.md "wikilink")
  - 峰不二子：[增山江威子](../Page/增山江威子.md "wikilink")
  - 石川五右衛門：[井上真樹夫](../Page/井上真樹夫.md "wikilink")
  - 銭形幸一：[納谷悟朗](../Page/納谷悟朗.md "wikilink")
  - 克蕾莉絲（克蕾莉絲‧德・卡-{里}-奧斯特羅）：[島本須美](../Page/島本須美.md "wikilink")
  - 卡-{里}-奧斯特羅伯爵（萊瑟・德・卡-{里}-奧斯特羅）：[石田太郎](../Page/石田太郎.md "wikilink")
  - 喬多：[永井一郎](../Page/永井一郎.md "wikilink")
  - 老園丁：[宮內幸平](../Page/宮內幸平.md "wikilink")
  - 古斯塔夫：[常泉忠通](../Page/常泉忠通.md "wikilink")
  - [大主教](../Page/大主教.md "wikilink")：[梓欽造](../Page/梓欽造.md "wikilink")
  - 埼玉縣警機動隊隊長：[松田重治](../Page/松田重治.md "wikilink")
  - 餐館女侍：[山岡葉子](../Page/山岡葉子.md "wikilink")
  - [國際刑警組織長官](../Page/國際刑警組織.md "wikilink")：[平林尚三](../Page/平林尚三.md "wikilink")
  - [日本代表](../Page/日本.md "wikilink")：[野島昭生](../Page/野島昭生.md "wikilink")
  - [英國代表](../Page/英國.md "wikilink")：[阪脩](../Page/阪脩.md "wikilink")
  - [西德代表](../Page/西德.md "wikilink")：[寺島幹夫](../Page/寺島幹夫.md "wikilink")
  - [蘇聯代表](../Page/蘇聯.md "wikilink")：[鎗田順吉](../Page/鎗田順吉.md "wikilink")
  - [僕人](../Page/僕人.md "wikilink")：[綠川稔](../Page/綠川稔.md "wikilink")
  - 印刷廠主任：[加藤正之](../Page/加藤正之.md "wikilink")
  - [大主教的司機](../Page/大主教.md "wikilink")：[峰惠研](../Page/峰惠研.md "wikilink")

## 得獎經歷

  - 第34屆每日電影獎大藤信郎獎（1979年）

  -
  -
  -
## 參見

[Category:鲁邦三世劇場版](../Category/鲁邦三世劇場版.md "wikilink")
[Category:宮崎駿電影](../Category/宮崎駿電影.md "wikilink")
[Category:1979年日本劇場動畫](../Category/1979年日本劇場動畫.md "wikilink")
[Category:虛構國家背景電影](../Category/虛構國家背景電影.md "wikilink")
[Category:歐洲背景電影](../Category/歐洲背景電影.md "wikilink")
[Category:宫崎骏](../Category/宫崎骏.md "wikilink")
[Category:偽造貨幣題材作品](../Category/偽造貨幣題材作品.md "wikilink")