**魯閔公**，即**姬啟**，一名**啟方**，為[春秋](../Page/春秋.md "wikilink")[諸侯國](../Page/諸侯國.md "wikilink")[魯國君主之一](../Page/魯國.md "wikilink")，是魯國第十七任君主。他為[魯莊公](../Page/魯莊公.md "wikilink")、叔姜的兒子。近人考証謂於[周惠王八年](../Page/周惠王.md "wikilink")（前669年）出生，至周惠王十七年（前660年）去世，年約十歲。（楊伯峻《春秋左傳注》，頁254）

莊公死前，弟弟[叔牙建議立莊公庶長兄](../Page/叔牙.md "wikilink")[慶父](../Page/慶父.md "wikilink")，另一位弟弟季友則支持立[子般](../Page/子般.md "wikilink")，季友於是借莊公之命[賜死叔牙](../Page/賜死.md "wikilink")，莊公病逝，季友立子般為君，十月慶父殺子般，立莊公另一庶子啟為魯君，即魯閔公，魯閔公亦是[齊桓公的外甥](../Page/齊桓公.md "wikilink")，對[齊桓公很尊敬](../Page/齊桓公.md "wikilink")，因此齊魯無大事，直到兩年後[公子慶父以毒](../Page/公子慶父.md "wikilink")[餅殺死魯閔公](../Page/餅.md "wikilink")，[齊桓公才派兵迎立魯閔公之弟](../Page/齊桓公.md "wikilink")[魯-{釐}-公](../Page/魯釐公.md "wikilink")。

在位期間的卿為[公子慶父](../Page/公子慶父.md "wikilink")、[季友](../Page/季友.md "wikilink")。

## 在位年與西曆對照表

<div class="NavFrame" style="clear: both; border: 1px solid #999; margin: 0.5em auto;">

<div class="NavHead" style="background-color: #CCCCFF; font-size: 100%; border-left: 3em soli; text-align: center; font-weight: bold;">

在位年與西曆對照表

</div>

<div class="NavContent" style="padding: 1em 0 0 0; font-size: 100%; text-align: center;">

| 魯閔公                            | 元年                             | 二年                             |
| ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前661年                          | 前660年                          |
| [干支](../Page/干支.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") |

</div>

</div>

## 參見

  -
## 參考文獻

  - 陸峻嶺、林幹合編，《中國歷代各族紀年表》，1982年，臺北，木鐸出版社

[Category:魯國君主](../Category/魯國君主.md "wikilink")
[Category:春秋人](../Category/春秋人.md "wikilink")
[Category:姬姓](../Category/姬姓.md "wikilink")