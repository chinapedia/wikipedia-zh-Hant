**鮑姓**为[中文姓氏之一](../Page/中文姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》中排名第62位。

## 起源

据《元和姓纂》和《通志·氏族略》记载，鮑姓源出于[姒姓](../Page/姒姓.md "wikilink")。春秋时，夏[禹的后人](../Page/禹.md "wikilink")[杞国公子敬叔食邑於](../Page/杞国.md "wikilink")[齐国鲍邑](../Page/齐国.md "wikilink")（今山东[济南市](../Page/济南市.md "wikilink")[历城区](../Page/历城区.md "wikilink")），其后以鲍为氏。世代为齐国上卿，战国初期被田氏攻灭，子孙出逃别国。

《姓苑》记载：“系出姒姓。夏禹后。春秋时杞公子有仕齐者，食采于鲍，因以命氏”。

另有北魏时鲜卑姓[俟力伐氏改为汉姓](../Page/俟力伐氏.md "wikilink")「鲍」氏。

## 郡望堂號

  - [上党郡](../Page/上党郡.md "wikilink")：上黨地名最早見於春秋時期的晉國，此後趙魏韓三家分晉，都佔領了上黨地區的一部分，但是具體的郡置已經難以考證。
  - [东海郡](../Page/东海郡.md "wikilink")：
  - [泰山郡](../Page/泰山郡.md "wikilink")：
  - [河南郡](../Page/河南郡.md "wikilink")：

## 歷史名人

  - [鲍叔牙](../Page/鲍叔牙.md "wikilink")，春秋时期齐国大夫
  - [鮑宣](../Page/鮑宣.md "wikilink")，西漢官員
  - [鮑信](../Page/鮑信.md "wikilink")，東漢末年人物
  - [鮑勳](../Page/鮑勳.md "wikilink")，曹魏官員
  - [鮑令暉](../Page/鮑令暉.md "wikilink")，中國南北朝女詩人
  - [鮑照](../Page/鮑照.md "wikilink")，中國南北朝詩人
  - [鮑姑](../Page/鮑姑.md "wikilink")，中國四大女醫之一，[葛洪妻](../Page/葛洪.md "wikilink")
  - [鮑超](../Page/鮑超.md "wikilink")，湘軍名將
  - [鲍廷博](../Page/鲍廷博.md "wikilink")，字以文。清朝安徽歙县人，藏书家
  - [鮑信](../Page/鮑信.md "wikilink")，東漢末年之將領
  - [鮑方](../Page/鮑方.md "wikilink")：香港演員
  - [鮑漢琳](../Page/鮑漢琳.md "wikilink")：香港演員

## 現代名人

  - [鮑春來](../Page/鮑春來.md "wikilink")：中國羽毛球運動員
  - [鮑國安](../Page/鮑國安.md "wikilink")：國家一級演員
  - [鮑德熹](../Page/鮑德熹.md "wikilink")：香港著名攝影師，奧斯卡最佳攝影得主
  - [鮑起靜](../Page/鮑起靜.md "wikilink")：香港著名女演員
  - [鮑天琦](../Page/鮑天琦.md "wikilink")：中國女演員
  - [鮑彤](../Page/鮑彤.md "wikilink")：原中國共產黨中央委員會委員，六四事件後被撤職
  - [鮑笑薇](../Page/鮑笑薇.md "wikilink")：香港特別行政區前行政長官[曾蔭權妻子](../Page/曾蔭權.md "wikilink")
  - [鮑比達](../Page/鮑比達.md "wikilink")：香港作曲家
  - [鮑喜順](../Page/鮑喜順.md "wikilink")：中國身高最高的人，曾被吉尼斯认证为世界第一高

## 外部链接

  - [鲍姓
    维库](https://web.archive.org/web/20101229002227/http://www.wikilib.com/wiki/%E9%B2%8D%E5%A7%93)

[B鮑](../Category/漢字姓氏.md "wikilink") [\*](../Category/鮑姓.md "wikilink")