## 註釋

  - [鮑德溫曾三任](../Page/鮑德溫.md "wikilink")[英國首相](../Page/英國首相.md "wikilink")，期間曾三度組閣。第一次日期為1923年5月至1924年1月；第二次日期為1924年11月至1929年6月；第三次叫做**第三次聯合內閣**，日期為1935年6月至1937年5月。

## 鮑德溫內閣

### 第一次鮑德溫內閣 （1923年5月—1924年1月）

  - 鮑德溫：[英國首相](../Page/英國首相.md "wikilink")、[財政大臣及](../Page/財政大臣.md "wikilink")[下議院領袖](../Page/下議院領袖.md "wikilink")
  - [凱夫勳爵](../Page/喬治·凱夫，第一代凱夫子爵.md "wikilink")（Lord
    Cave）：[大法官](../Page/英國大法官.md "wikilink")
  - [索爾斯伯利勳爵](../Page/詹姆士·蓋斯科因-塞西爾，第四代索爾斯伯利侯爵.md "wikilink")（Lord
    Salisbury）：[樞密院議長](../Page/樞密院議長.md "wikilink")
  - [塞爾活的塞西爾勳爵](../Page/羅伯特·塞西爾，塞爾活的第一代塞西爾子爵.md "wikilink")（Lord Cecil
    of Chelwood）：[掌璽大臣](../Page/掌璽大臣.md "wikilink")
  - [威廉·克萊芙·布里奇曼](../Page/威廉·克萊芙·布里奇曼，第一代布里奇曼子爵.md "wikilink")（William
    Clive Bridgeman）：[內務大臣](../Page/內務大臣.md "wikilink")
  - [凱德爾斯頓的寇松勳爵](../Page/喬治·納撒尼爾·寇松，凱德爾斯頓的第一代寇松侯爵.md "wikilink")（Lord
    Curzon of
    Kedleston）：[外務大臣及](../Page/外交及聯邦事務大臣.md "wikilink")[上議院領袖](../Page/上議院領袖.md "wikilink")
  - [德文郡公爵](../Page/維克多·卡文迪許，第九代德文郡公爵.md "wikilink")：[殖民地大臣](../Page/殖民地大臣.md "wikilink")
  - [德比勳爵](../Page/愛德華·斯坦利，第十七代德比伯爵.md "wikilink")（Lord
    Derby）：[陸軍大臣](../Page/陸軍大臣.md "wikilink")
  - [皮爾勳爵](../Page/威廉·威爾斯利·皮爾，第一代皮爾伯爵.md "wikilink")（Lord
    Peel）：[印度大臣](../Page/印度大臣.md "wikilink")
  - [繆爾·霍爾爵士](../Page/繆爾·霍爾，第一代坦普爾伍德子爵.md "wikilink")（Sir Samuel
    Hoare）：[空軍大臣](../Page/空軍大臣.md "wikilink")
  - [傲華爾勳爵](../Page/羅納德·芒羅-弗格森，第一代傲華爾子爵.md "wikilink")（Lord
    Novar）：[蘇格蘭大臣](../Page/蘇格蘭大臣.md "wikilink")
  - [里奧·艾默里](../Page/利奧波德·斯登尼·艾默里.md "wikilink")（Leo
    Amery）：[第一海軍大臣](../Page/第一海軍大臣.md "wikilink")
  - [菲力浦·勞埃-格雷姆爵士](../Page/菲力浦·坎利夫-李斯德，第一代斯維頓伯爵.md "wikilink")（Sir
    Philip Lloyd-Greame）：[貿易委員會主席](../Page/貿易委員會主席.md "wikilink")
  - [羅伯特·山德斯爵士](../Page/羅伯特·亞瑟·山德斯，第一代貝佛男爵.md "wikilink")（Sir Robert
    Sanders）：[農業部長](../Page/農業、漁業及食品部長.md "wikilink")
  - [愛德華·腓特烈·林黎·伍德](../Page/愛德華·伍德，第一代哈利法克斯伯爵.md "wikilink")（Edward
    Frederick Lindley Wood）：[教育委員會主席](../Page/教育委員會主席.md "wikilink")
  - [安德生·孟塔古-巴羅爵士](../Page/安德生·孟塔古-巴羅.md "wikilink")（Sir Anderson
    Montague-Barlow）：[勞工部長](../Page/就業大臣.md "wikilink")
  - [內維爾·張伯倫](../Page/內維爾·張伯倫.md "wikilink")：[衛生部長](../Page/衛生大臣.md "wikilink")
  - [威廉·喬因森-希克斯爵士](../Page/威廉·喬因森-希克斯，第一代布倫特福德子爵.md "wikilink")（Sir
    William Joynson-Hicks）：[財政部財務秘書](../Page/財政部財務秘書.md "wikilink")
  - [拉明·沃辛頓-埃文斯爵士](../Page/拉明·沃辛頓-埃文斯.md "wikilink")（Sir Laming
    Worthington-Evans）：[郵政總局局長](../Page/英國郵政總局局長.md "wikilink")

#### 變動

  - 1923年8月：內維爾·張伯倫接替鮑德溫為財政大臣。威廉·喬因森-希克斯爵士接替張伯倫為衛生部長。接替喬因森-希克斯爵士出任財政部財務秘書的人不在內閣供職。

### 第二次鮑德溫內閣 （1924年11月—1929年6月）

  - 鮑德溫：[英國首相及](../Page/英國首相.md "wikilink")[下議院領袖](../Page/下議院領袖.md "wikilink")
  - [凱夫勳爵](../Page/喬治·凱夫，第一代凱夫子爵.md "wikilink")：[大法官](../Page/英國大法官.md "wikilink")
  - [凱德爾斯頓的寇松勳爵](../Page/喬治·納撒尼爾·寇松，凱德爾斯頓的第一代寇松侯爵.md "wikilink")：[樞密院議長及](../Page/樞密院議長.md "wikilink")[上議院領袖](../Page/上議院領袖.md "wikilink")
  - [索爾斯伯利勳爵](../Page/詹姆士·蓋斯科因-塞西爾，第四代索爾斯伯利侯爵.md "wikilink")：[掌璽大臣](../Page/掌璽大臣.md "wikilink")
  - [邱吉爾](../Page/邱吉爾.md "wikilink")：[財政大臣](../Page/財政大臣.md "wikilink")
  - [威廉·喬因森-希克斯爵士](../Page/威廉·喬因森-希克斯，第一代布倫特福德子爵.md "wikilink")：[內務大臣](../Page/內務大臣.md "wikilink")
  - [奧斯汀·張伯倫爵士](../Page/奧斯汀·張伯倫.md "wikilink")（Sir Austen
    Chamberlain）：[外務大臣及下議院副領袖](../Page/外交及聯邦事務大臣.md "wikilink")
  - [里奧·艾默里](../Page/利奧波德·斯登尼·艾默里.md "wikilink")：[殖民地大臣](../Page/殖民地大臣.md "wikilink")
  - [拉明·沃辛頓-埃文斯爵士](../Page/拉明·沃辛頓-埃文斯.md "wikilink")：[陸軍大臣](../Page/陸軍大臣.md "wikilink")
  - [伯肯赫德勳爵](../Page/弗雷德里克·埃德溫·史密斯，第一代伯肯赫德伯爵.md "wikilink")（Lord
    Birkenhead）：[印度大臣](../Page/印度大臣.md "wikilink")
  - [繆爾·霍爾爵士](../Page/繆爾·霍爾，第一代坦普爾伍德子爵.md "wikilink")：[空軍大臣](../Page/空軍大臣.md "wikilink")
  - [約翰·吉爾摩爵士](../Page/約翰·吉爾摩.md "wikilink")（Sir John
    Gilmour）：[蘇格蘭大臣](../Page/蘇格蘭大臣.md "wikilink")
  - [威廉·克萊芙·布里奇曼](../Page/威廉·克萊芙·布里奇曼，第一代布里奇曼子爵.md "wikilink")：[第一海軍大臣](../Page/第一海軍大臣.md "wikilink")
  - [塞爾活的塞西爾勳爵](../Page/羅伯特·塞西爾，塞爾活的第一代塞西爾子爵.md "wikilink")：[蘭卡斯特公爵領地總裁](../Page/蘭卡斯特公爵領地總裁.md "wikilink")
  - [菲力浦·坎利夫-李斯德爵士](../Page/菲力浦·坎利夫-李斯德，第一代斯維頓伯爵.md "wikilink")（Sir
    Philip Cunliffe-Lister）：[貿易委員會主席](../Page/貿易委員會主席.md "wikilink")
  - [愛德華·腓特烈·林黎·伍德](../Page/愛德華·伍德，第一代哈利法克斯伯爵.md "wikilink")：[農業部長](../Page/農業、漁業及食品部長.md "wikilink")
  - [尤斯塔斯·柏西勳爵](../Page/尤斯塔斯·柏西，紐卡素的第一代柏西男爵.md "wikilink")（Lord Eustace
    Percy）：[教育委員會主席](../Page/教育委員會主席.md "wikilink")
  - [皮爾勳爵](../Page/威廉·威爾斯利·皮爾，第一代皮爾伯爵.md "wikilink")：[第一工務專員](../Page/第一工務專員.md "wikilink")
  - [亞瑟·斯蒂爾-梅特蘭爵士](../Page/亞瑟·斯蒂爾-梅特蘭.md "wikilink")（Sir Arthur
    Steel-Maitland）：[勞工部長](../Page/就業大臣.md "wikilink")
  - [內維爾·張伯倫](../Page/內維爾·張伯倫.md "wikilink")：[衛生部長](../Page/衛生大臣.md "wikilink")
  - [道格拉斯·霍格爵士](../Page/道格拉斯·霍格，第一代海爾什子爵.md "wikilink")（Sir Douglas
    Hogg）：[律政司](../Page/律政司_\(英國\).md "wikilink")

#### 變動

  - 1925年4月：寇松勳爵病逝，樞密院議長一職由貝爾福勳爵 （Lord
    Balfour）補上。索爾斯伯利勳爵則接任上議院領袖，但仍任掌璽大臣。
  - 1925年6月：新設[自治領事務大臣一職](../Page/自治領事務大臣.md "wikilink")，由原殖民地大臣里奧·艾默里所兼領。
  - 1925年11月：[沃爾特·吉尼斯](../Page/沃爾特·愛德華·吉尼斯，第一代莫因男爵.md "wikilink")（Walter
    Guinness）接替愛德華·腓特烈·林黎·伍德為農業部長。
  - 1926年7月：蘇格蘭大臣一職獲提升為[國務大臣職級](../Page/國務大臣.md "wikilink")。
  - 1927年10月：柯興登勳爵（Lord Cushendun）接替塞爾活的塞西爾勳爵出任蘭卡斯特公爵領地總裁。
  - 1928年3月：已晉為男爵的海爾什勳爵（Lord
    Hailsham，原稱「道格拉斯·霍格爵士」）接替凱夫勳爵為大法官。接替海爾什勳爵為律政司的人不在內閣供職。
  - 1928年10月：皮爾勳爵取代伯肯赫德勳爵為印度大臣。倫敦德里勳爵（Lord Londonderry）則接替皮爾勳爵為第一工務專員。

### 第三次聯合內閣 （1935年6月—1937年5月）

  - 鮑德溫：[英國首相及](../Page/英國首相.md "wikilink")[下議院領袖](../Page/下議院領袖.md "wikilink")
  - [海爾什勳爵](../Page/道格拉斯·霍格，第一代海爾什子爵.md "wikilink")：[大法官](../Page/英國大法官.md "wikilink")
  - [拉姆齊·麥克唐納](../Page/拉姆齊·麥克唐納.md "wikilink")：[樞密院議長](../Page/樞密院議長.md "wikilink")
  - [倫敦德里勳爵](../Page/查爾斯·范內-坦佩斯特-史都華德，第七代倫敦德里侯爵.md "wikilink")：[掌璽大臣及](../Page/掌璽大臣.md "wikilink")[上議院領袖](../Page/上議院領袖.md "wikilink")
  - [內維爾·張伯倫](../Page/內維爾·張伯倫.md "wikilink")：[財政大臣](../Page/財政大臣.md "wikilink")
  - [約翰·西蒙爵士](../Page/約翰·奧爾斯布魯克·西蒙，第一代西蒙子爵.md "wikilink")（Sir John
    Simon）：[內務大臣及下議院副領袖](../Page/內務大臣.md "wikilink")
  - [繆爾·霍爾爵士](../Page/繆爾·霍爾，第一代坦普爾伍德子爵.md "wikilink")：[外務大臣](../Page/外務及英聯邦事務大臣.md "wikilink")
  - [馬爾科姆·麥克唐納](../Page/馬爾科姆·麥克唐納.md "wikilink")（Malcolm
    MacDonald）：[殖民地大臣](../Page/殖民地大臣.md "wikilink")
  - [J·H·托馬斯](../Page/詹姆士·亨利·托馬斯.md "wikilink")（J.H.
    Thomas）：[自治領大臣](../Page/自治領事務大臣.md "wikilink")
  - [哈利法克斯勳爵](../Page/愛德華·伍德，第一代哈利法克斯伯爵.md "wikilink")：[陸軍大臣](../Page/陸軍大臣.md "wikilink")
  - [澤特蘭勳爵](../Page/勞倫斯·鄧達斯，第二代澤特蘭侯爵.md "wikilink")（Lord
    Zetland）：[印度大臣](../Page/印度大臣.md "wikilink")
  - [斯維頓勳爵](../Page/菲力浦·坎利夫-李斯德，第一代斯維頓伯爵.md "wikilink")（Lord
    Swinton）：[空軍大臣](../Page/空軍大臣.md "wikilink")
  - [戈弗雷·柯林斯爵士](../Page/戈弗雷·柯林斯.md "wikilink")（Sir Godfrey
    Collins）：[蘇格蘭大臣](../Page/蘇格蘭大臣.md "wikilink")
  - [博爾頓·艾利斯-蒙塞爾](../Page/博爾頓·艾利斯-蒙塞爾，第一代蒙塞爾子爵.md "wikilink")（Bolton
    Eyres-Monsell）：[第一海軍大臣](../Page/第一海軍大臣.md "wikilink")
  - [華特·朗西曼](../Page/華特·朗西曼，鐸斯福的第一代朗西曼子爵.md "wikilink")（Walter
    Runciman）：[貿易委員會主席](../Page/貿易委員會主席.md "wikilink")
  - [奧利弗·斯坦利](../Page/奧利弗·斯坦利.md "wikilink")（Oliver
    Stanley）：[教育委員會主席](../Page/教育及技術大臣.md "wikilink")
  - [沃爾特·艾略特](../Page/沃爾特·艾略特_\(政治家\).md "wikilink")（Walter
    Elliot）：[農業部長](../Page/農業、漁業及食物部長.md "wikilink")
  - [歐內斯特·布朗](../Page/歐內斯特·布朗.md "wikilink")（Ernest
    Brown）：[勞工部長](../Page/就業大臣.md "wikilink")
  - [金斯利·伍德爵士](../Page/金斯利·伍德.md "wikilink")（Sir Kingsley
    Wood）：[衛生部長](../Page/衛生大臣.md "wikilink")
  - [威廉·奧姆斯比-戈爾](../Page/威廉·奧姆斯比-戈爾，第四代哈勒赫男爵.md "wikilink")（William
    Ormsby-Gore）：[第一工務專員](../Page/第一工務專員.md "wikilink")
  - [安東尼·艾登](../Page/安東尼·艾登.md "wikilink")：[不管部大臣](../Page/不管部大臣.md "wikilink")，主理[國際聯盟事宜](../Page/國際聯盟.md "wikilink")
  - [尤斯塔斯·柏西勳爵](../Page/尤斯塔斯·柏西，紐卡素的第一代柏西男爵.md "wikilink")：[不管部大臣](../Page/不管部大臣.md "wikilink")，主理政府政策

#### 變動

  - 1935年11月：馬爾科姆·麥克唐納接替J·H·托馬斯為自治領大臣。托馬斯則同樣接替麥克唐納為殖民地大臣。另外，哈利法克斯勳爵接替倫敦德里勳爵為掌璽大臣兼上議院領袖，[多弗·柯柏](../Page/多弗·柯柏，第一代諾里奇子爵.md "wikilink")（Duff
    Cooper）則接替哈利法克斯勳爵出任陸軍大臣。菲力浦·坎利夫-李斯德爵士和博爾頓·艾利斯-蒙塞爾分別取得[斯維頓子爵](../Page/斯維頓子爵.md "wikilink")（Viscount
    Swinton）和[蒙塞爾子爵](../Page/蒙塞爾子爵.md "wikilink")（Viscount
    Monsell）頭銜，兩人仍在內閣供職。
  - 1935年12月：安東尼·艾登不再任不管部大臣，並取代繆爾·霍爾爵士為外務大臣，霍爾爵士脫離內閣。
  - 1936年3月：[托馬斯·英斯基普爵士](../Page/托馬斯·英斯基普，第一代蓋德高子爵.md "wikilink")（Sir
    Thomas Inskip）以國防協調部長之身份加入內閣。尤斯塔斯·柏西勳爵脫離內閣。
  - 1936年5月：威廉·奧姆斯比-戈爾接替J·H·托馬斯為殖民地大臣。[斯坦厄普勳爵](../Page/詹姆士·斯坦厄普，第七代斯坦厄普伯爵.md "wikilink")（Lord
    Stanhope）則接替奧姆斯比-戈爾為第一工務專員。
  - 1936年6月：繆爾·霍爾爵士重新進入內閣，接替蒙塞爾勳爵擔任第一海軍大臣。
  - 1936年10月：沃爾特·艾略特接替戈弗雷·柯林斯爵士為蘇格蘭大臣。[威廉·謝潑德·莫里森](../Page/威廉·莫里森，第一代鄧羅西爾子爵.md "wikilink")（William
    Shepherd
    Morrison）接替艾略特為農業部長。[萊斯利·霍爾-貝利沙](../Page/萊斯利·霍爾-貝利沙，第一代霍爾-貝利沙男爵.md "wikilink")（Leslie
    Hore-Belisha）以[交通部長之身份加入內閣](../Page/交通大臣.md "wikilink")。

[B](../Category/英國內閣.md "wikilink")