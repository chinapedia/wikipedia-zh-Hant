**鯉魚潭**位於[台灣](../Page/台灣.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")[壽豐鄉](../Page/壽豐鄉.md "wikilink")，地處[花東縱谷國家風景區的最北端](../Page/花東縱谷國家風景區.md "wikilink")。鯉魚潭為[木瓜溪支流](../Page/木瓜溪.md "wikilink")[銅文蘭溪之上游與](../Page/銅文蘭溪.md "wikilink")[花蓮溪支流](../Page/花蓮溪.md "wikilink")[荖溪之間](../Page/荖溪.md "wikilink")[河川襲奪所形成的](../Page/河川襲奪.md "wikilink")[堰塞湖](../Page/堰塞湖.md "wikilink")，湖面呈南北長東西窄的橢圓形，面積約104[公頃左右](../Page/公頃.md "wikilink")，湖水來自於地底湧[泉](../Page/泉.md "wikilink")，所以終年清澈，水最深處達15公尺。\[1\]

而「鯉魚潭」的得名說法有二：一是因為側邊[山岳形似](../Page/山岳.md "wikilink")[鯉魚](../Page/鯉魚.md "wikilink")，故山與湖同齊名為鯉魚，有「[鯉魚山上有鯉魚](../Page/鯉魚山.md "wikilink")，鯉魚潭裡水中游。」俚語的稱呼。另一種說法源於最早活動於此處的[台灣原住民](../Page/台灣原住民.md "wikilink")[太魯閣族](../Page/太魯閣族.md "wikilink")，因為登至山頂往下看，潭的形狀就像是一隻剛捕獲的鯉魚在跳躍，所以以此名之。

該處環境優美如畫，環境與水質均保持良好。近年來每年四、五月均舉辦[螢火蟲季](../Page/螢火蟲.md "wikilink")，更顯示當地生態與環境保育的成功。優美的景色吸引許多遊客來此，為[台灣東部與](../Page/台灣東部.md "wikilink")[太魯閣等](../Page/太魯閣.md "wikilink")[風景區為知名的](../Page/國家風景區.md "wikilink")[旅遊勝地](../Page/旅遊.md "wikilink")。

## 參見

  - [東部海岸國家風景區](../Page/東部海岸國家風景區.md "wikilink")
  - [池南國家森林遊樂區](../Page/池南國家森林遊樂區.md "wikilink")
  - [富源國家森林遊樂區](../Page/富源國家森林遊樂區.md "wikilink")
  - [六十石山](../Page/六十石山.md "wikilink")
  - [鹿野高台](../Page/鹿野高台.md "wikilink")
  - [知本國家森林遊樂區](../Page/知本國家森林遊樂區.md "wikilink")

## 圖片

<File:Taiwan> 2009 HuaLien Carp Mountain Trail FRD 5351.jpg
<File:Taiwan> 2009 HuaLien Carp Mountain Walking Trail FRD 5358.jpg
<File:Taiwan> 2009 HuaLien Carp Mountain Walking Trail FRD 5361.jpg
<File:Taiwan_LiYu_Lake.JPG> <File:Wongwt> 鯉魚潭 (16573768349).jpg

## 參考文獻

## 外部連結

  - [鯉魚潭 與「河川襲奪」](http://blog.xuite.net/t006kong/wretch/370520371)
  - [花蓮黃頁網路電話簿 鯉魚潭](http://www.tel038.com.tw/news_po1.php?no=22)
  - [花蓮縣觀光資訊網
    鯉魚潭](http://tour-hualien.hl.gov.tw/index.jsp?page=case_spot&CaseId=1&SceneId=319)

[category:台灣湖泊](../Page/category:台灣湖泊.md "wikilink")

[Category:堰塞湖](../Category/堰塞湖.md "wikilink")
[Category:花蓮縣旅遊景點](../Category/花蓮縣旅遊景點.md "wikilink")
[Category:花東縱谷國家風景區](../Category/花東縱谷國家風景區.md "wikilink")
[Category:壽豐鄉](../Category/壽豐鄉.md "wikilink")

1.  [花蓮‧鯉魚潭](http://forestlife.info/slide/s271.htm)