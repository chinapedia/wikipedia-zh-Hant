**鯊齒龍科**（Carcharodontosauridae）是群大型[肉食性](../Page/肉食性.md "wikilink")[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。[恩斯特·斯特莫](../Page/恩斯特·斯特莫.md "wikilink")（Ernst
Stromer）在1931年建立鯊齒龍科，在現代[古生物學中是](../Page/古生物學.md "wikilink")[肉食龍下目的一個](../Page/肉食龍下目.md "wikilink")[演化支](../Page/演化支.md "wikilink")。鯊齒龍科包含某些曾出現過的最大型陸地掠食動物：[南方巨獸龍](../Page/南方巨獸龍.md "wikilink")、[馬普龍](../Page/馬普龍.md "wikilink")、[鯊齒龍](../Page/鯊齒龍.md "wikilink")、以及[魁紂龍](../Page/魁紂龍.md "wikilink")，上述屬在體型上與[暴龍相等或者超越](../Page/暴龍.md "wikilink")。

## 演化

[Carcharodontosaurus.jpg](https://zh.wikipedia.org/wiki/File:Carcharodontosaurus.jpg "fig:Carcharodontosaurus.jpg")的頭骨模型\]\]
在2011年，[奧利佛·勞赫](../Page/奧利佛·勞赫.md "wikilink")（Oliver
Rauhut）將發現於[坦尚尼亞](../Page/坦尚尼亞.md "wikilink")[敦達古魯組](../Page/敦達古魯組.md "wikilink")（Tendaguru
Formation）地層的化石，建立為鯊齒龍科的新屬[舊鯊齒龍](../Page/舊鯊齒龍.md "wikilink")（*Veterupristisaurus*），地質年代約1億5400萬到1億5000萬年前，相當於[侏儸紀晚期的](../Page/侏儸紀.md "wikilink")[啟莫里階到早](../Page/啟莫里階.md "wikilink")[提通階](../Page/提通階.md "wikilink")。生存於侏儸紀晚期的舊鯊齒龍，是已知生存時代最早的鯊齒龍科\[1\]。

在[白堊紀早到中期](../Page/白堊紀.md "wikilink")，鯊齒龍科與[棘龍科是](../Page/棘龍科.md "wikilink")[岡瓦納大陸的最大型掠食動物](../Page/岡瓦納大陸.md "wikilink")，[北美洲的](../Page/北美洲.md "wikilink")[高棘龍](../Page/高棘龍.md "wikilink")、[亞洲的](../Page/亞洲.md "wikilink")[假鯊齒龍以及](../Page/假鯊齒龍.md "wikilink")[歐洲的](../Page/歐洲.md "wikilink")[昆卡獵龍也屬於鯊齒龍科](../Page/昆卡獵龍.md "wikilink")\[2\]。牠們的生存年代從[巴列姆階](../Page/巴列姆階.md "wikilink")（1億2700萬年前到1億2100萬年前）到[土侖階](../Page/土侖階.md "wikilink")（9300萬年前到8900萬年前）。

然而，土侖階之後似乎就沒有鯊齒龍科的存活跡象。牠們在[岡瓦納大陸被較小型的](../Page/岡瓦納大陸.md "wikilink")[阿貝力龍科取代](../Page/阿貝力龍科.md "wikilink")，在[北美洲與](../Page/北美洲.md "wikilink")[亞洲被](../Page/亞洲.md "wikilink")[暴龍科所取代](../Page/暴龍科.md "wikilink")。根據[奧尼拉斯·諾瓦斯](../Page/奧尼拉斯·諾瓦斯.md "wikilink")（Fernando
Novas）與他的同僚的說法，鯊齒龍科與棘龍科在岡瓦納大陸與北美洲消失，顯示這次動物群的取代是全球性事件\[3\]。

發現於[巴西Marília組地層](../Page/巴西.md "wikilink")（[馬斯垂克階](../Page/馬斯垂克階.md "wikilink")）的獸腳類牙齒、巴西Presidente
Prudente組地層（[坎潘階到馬斯垂克階](../Page/坎潘階.md "wikilink")）的右[上頜骨碎片](../Page/上頜骨.md "wikilink")，可能屬於鯊齒龍科，代表這群恐龍可能持續存活到白堊紀末期\[4\]\[5\]。

## 分類系統

### 分類學

在1931年，恩斯特·斯特莫建立鯊齒龍科，以包含新發現的[撒哈拉鯊齒龍](../Page/鯊齒龍.md "wikilink")（*Carcharodontosaurus
saharicus*）。在1995年，撒哈拉鯊齒龍的近親[南方巨獸龍被敘述](../Page/南方巨獸龍.md "wikilink")、命名，並被歸類於這個科。另外，許多[古生物學家將](../Page/古生物學家.md "wikilink")[高棘龍包含在此科內](../Page/高棘龍.md "wikilink")（Sereno等人
1996, Harris 1998, Holtz 2000, Rauhut
2003），而其他人則將高棘龍分類於相近的[異特龍科](../Page/異特龍科.md "wikilink")（Currie
& Carpenter, 2000; Coria & Currie, 2002, Eddy & Clarke, 2011, Rauhut
2011）。

當2006年發現[馬普龍時](../Page/馬普龍.md "wikilink")，[羅多爾夫·科里亞](../Page/羅多爾夫·科里亞.md "wikilink")（Rodolfo
Coria）與[菲力·柯爾](../Page/菲力·柯爾.md "wikilink")（Phil
Currie）建立[南方巨獸龍亞科](../Page/南方巨獸龍亞科.md "wikilink")，以包含更先進的[南美洲物種](../Page/南美洲.md "wikilink")，這些物種彼此關係更為接近，而離[非洲與](../Page/非洲.md "wikilink")[歐洲物種較遠](../Page/歐洲.md "wikilink")。科里亞與柯爾並未將[魁紂龍歸類於此亞科](../Page/魁紂龍.md "wikilink")，需要更詳細的敘述，但根據牠們的[股骨特徵](../Page/股骨.md "wikilink")，牠們可能也屬於南方巨獸龍亞科\[6\]。

[Acrocanthosaurusskeleton.jpg](https://zh.wikipedia.org/wiki/File:Acrocanthosaurusskeleton.jpg "fig:Acrocanthosaurusskeleton.jpg")的骨架模型\]\]

### 系統發生學

在1998年，[保羅·塞里諾](../Page/保羅·塞里諾.md "wikilink")（Paul
Sereno）將鯊齒龍科定義為一個[演化支](../Page/演化支.md "wikilink")，包含：撒哈拉鯊齒龍、與親緣關係接近鯊齒龍，而離[脆弱異特龍](../Page/異特龍.md "wikilink")、[董氏中華盜龍](../Page/中華盜龍.md "wikilink")、[單脊龍](../Page/單脊龍.md "wikilink")、[冰脊龍較遠的所有物種](../Page/冰脊龍.md "wikilink")。因此，這個演化支不屬於[異特龍科](../Page/異特龍科.md "wikilink")。

以下[演化樹是根據布魯薩特等人的](../Page/演化樹.md "wikilink")2009年研究\[7\]。但是，其中的[新獵龍已被歸類於](../Page/新獵龍.md "wikilink")[新獵龍科](../Page/新獵龍科.md "wikilink")，已不屬於鯊齒龍科\[8\]：

以下演化樹則是F. Ortega等人在2010年提出的版本\[9\]︰

高棘龍分類位置仍未確定，大部分研究人員將牠們分類於鯊齒龍科\[10\]，而其他人則分類於異特龍科。在2011年的[克拉瑪依龍重新研究](../Page/克拉瑪依龍.md "wikilink")，發現克拉瑪依龍屬於鯊齒龍科。根據[堅尾龍類的](../Page/堅尾龍類.md "wikilink")[種系發生學研究](../Page/種系發生學.md "wikilink")，發現克拉瑪依龍是種基礎鯊齒龍科恐龍，而[始鯊齒龍也是種原始鯊齒龍科](../Page/始鯊齒龍.md "wikilink")\[11\]。[巴哈利亞龍也曾被假設為鯊齒龍科](../Page/巴哈利亞龍.md "wikilink")，但牠們的化石太少而不能確定。巴哈利亞龍似乎是[角鼻龍下目](../Page/角鼻龍下目.md "wikilink")[三角洲奔龍的一個](../Page/三角洲奔龍.md "wikilink")[異名](../Page/異名.md "wikilink")。

鯊齒龍科曾認為與[阿貝力龍科有較接近親緣關係](../Page/阿貝力龍科.md "wikilink")，而離[異特龍科較遠](../Page/異特龍科.md "wikilink")。這是因為這兩個演化支有某些共同的頭蓋骨特徵。然而，這些相似處似乎是兩個演化支的[平行演化而來的](../Page/平行演化.md "wikilink")。有眾多的頭蓋骨與顱後特徵，支持鯊齒龍科與異特龍科之間有接近親緣關係。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [See entry on Carcharodontosauridae Cranial Anatomy at Dinodata
    (registration required, free)](http://www.dinodata.org/index.php)
  - [Carcharodontosauridae at Theropod
    Database](https://web.archive.org/web/20080115190147/http://home.myuw.net/eoraptor/Carnosauria.htm#Carcharodontosauridae)
  - [Carcharodontosauridae/Abelisauridae](http://dml.cmnh.org/1999Mar/msg00493.html)
  - [Carcharodontosauridae/Abelisauridae 2](http://dml.cmnh.org/1999Mar/msg00504.html)

[\*](../Category/鯊齒龍科.md "wikilink")

1.

2.  Brusatte, S., Benson, R., Chure, D., Xu, X., Sullivan, C., and Hone,
    D. (2009). "The first definitive carcharodontosaurid (Dinosauria:
    Theropoda) from Asia and the delayed ascent of tyrannosaurids."
    *Naturwissenschaften*,

3.  Novas, de Valais, Vickers-Rich, and Rich. (2005). "A large
    Cretaceous theropod from Patagonia, Argentina, and the evolution of
    carcharodontosaurids." *Naturwissenschaften*,

4.

5.

6.

7.
8.  Benson, R.B.J., Carrano, M.T and Brusatte, S.L. (2009). "A new clade
    of archaic large-bodied predatory dinosaurs (Theropoda:
    Allosauroidea) that survived to the latest Mesozoic."
    *Naturwissenschaften*, ''' '''(): . <doi:10.1007/s00114-009-0614-x>

9.   [Supporting
    Information](http://www.nature.com/nature/journal/v467/n7312/extref/nature09181-s1.pdf)

10.

11.