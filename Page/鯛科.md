**鯛科**（[学名](../Page/学名.md "wikilink")：）為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鲷形目](../Page/鲷形目.md "wikilink")（或传统上为[鱸形目](../Page/鱸形目.md "wikilink")[鲈亚目](../Page/鲈亚目.md "wikilink")）的一個[科](../Page/科.md "wikilink")。

## 分布

本[科魚類廣泛分布於全世界各大洋之熱帶及溫帶沿岸之淺水區內](../Page/科.md "wikilink")。少數棲息在深水區。

## 深度

棲息水深平均約在十公尺至一百公尺。

## 特徵

本[科魚類外型類似](../Page/科.md "wikilink")[笛鯛科或](../Page/笛鯛科.md "wikilink")[石鱸科](../Page/石鱸科.md "wikilink")，但鯛的體型較高，更側扁；上下頜之門齒、犬狀齒以至於兩側之臼齒均較發達。體呈橢圓或卵圓形，頭大，前半部體較高，背緣彎曲，腹緣較平。被大型弱櫛之櫛鱗或圓鱗，頭部和兩頰通常有鱗片。側線單一且完整，偏向背側。在體形及體側斑紋會有變異。

## 分類

**鯛科**其下分34個屬，如下：

  - [棘鯛屬](../Page/棘鯛屬.md "wikilink")（*Acanthopagrus*）

<!-- end list -->

  - [赤崎棘鯛](../Page/赤崎棘鯛.md "wikilink")（*Acanthopagrus akazakii*）
  - [澳洲棘鯛](../Page/澳洲棘鯛.md "wikilink")（*Acanthopagrus
    australis*）：又稱[澳洲黑鯛](../Page/澳洲黑鯛.md "wikilink")。
  - [灰鰭棘鯛](../Page/灰鰭棘鯛.md "wikilink")（*Acanthopagrus
    berda*）：又稱[灰鰭鯛](../Page/灰鰭鯛.md "wikilink")。
  - [雙帶棘鯛](../Page/雙帶棘鯛.md "wikilink")（*Acanthopagrus bifasciatus*）
  - [布氏棘鯛](../Page/布氏棘鯛.md "wikilink")（*Acanthopagrus butcheri*）
  - [琉球黃鰭棘鯛](../Page/琉球黃鰭棘鯛.md "wikilink")（*Acanthopagrus chinshira*）
  - [黃鰭棘鯛](../Page/黃鰭棘鯛.md "wikilink")（*Acanthopagrus
    latus*）：又稱[黃鰭鯛](../Page/黃鰭鯛.md "wikilink")。
  - [黑緣棘鯛](../Page/黑緣棘鯛.md "wikilink")（*Acanthopagrus omanensis*）
  - [太平洋棘鯛](../Page/太平洋棘鯛.md "wikilink")（*Acanthopagrus pacificus*）
  - [北澳棘鯛](../Page/北澳棘鯛.md "wikilink")（*Acanthopagrus palmaris*）
  - [中東黑棘鯛](../Page/中東黑棘鯛.md "wikilink")（*Acanthopagrus randalli*）
  - [科楚奇黑棘鯛](../Page/科楚奇黑棘鯛.md "wikilink")（*Acanthopagrus schlegelii
    czerskii*）：又稱[切氏黑鯛](../Page/切氏黑鯛.md "wikilink")。
  - [黑棘鯛](../Page/黑棘鯛.md "wikilink")（*Acanthopagrus schlegelii
    schlegelii*）：又稱[黑鯛](../Page/黑鯛.md "wikilink")。
  - [桔鰭棘鯛](../Page/桔鰭棘鯛.md "wikilink")（*Acanthopagrus
    sivicolus*）：又稱[橘鰭鯛](../Page/橘鰭鯛.md "wikilink")。
  - [台灣棘鯛](../Page/台灣棘鯛.md "wikilink")（*Acanthopagrus
    taiwanensis*）：又稱[台灣黑鯛](../Page/台灣黑鯛.md "wikilink")。
  - [迷走棘鯛](../Page/迷走棘鯛.md "wikilink")（*Acanthopagrus vagus*）

<!-- end list -->

  - [羊鯛屬](../Page/羊鯛屬.md "wikilink")（*Archosargus*）

<!-- end list -->

  - [波氏羊鯛](../Page/波氏羊鯛.md "wikilink")（*Archosargus pourtalesii*）
  - [羊鯛](../Page/羊鯛.md "wikilink")（*Archosargus
    probatocephalus*）：又稱南方羊鯛。
  - [菱羊鯛](../Page/菱羊鯛.md "wikilink")（*Archosargus rhomboidalis*）

<!-- end list -->

  - [四長棘鯛屬](../Page/四長棘鯛屬.md "wikilink")（*Argyrops*）

<!-- end list -->

  - [四長棘鯛](../Page/四長棘鯛.md "wikilink")（*Argyrops bleekeri*）：又稱小長棘鯛。
  - [鐮胸四長棘鯛](../Page/鐮胸四長棘鯛.md "wikilink")（*Argyrops filamentosus*）
  - [紅海四長棘鯛](../Page/紅海四長棘鯛.md "wikilink")（*Argyrops megalommatus*）
  - [高體四長棘鯛](../Page/高體四長棘鯛.md "wikilink")（*Argyrops spinifer*）：又稱長棘鯛。

<!-- end list -->

  - [銀帶鯛屬](../Page/銀帶鯛屬.md "wikilink")（*Argyrozona*）

<!-- end list -->

  - [犬牙銀帶鯛](../Page/犬牙銀帶鯛.md "wikilink")（*Argyrozona argyrozona*）

<!-- end list -->

  - [牛眼鯛屬](../Page/牛眼鯛屬.md "wikilink")（*Boops*）

<!-- end list -->

  - [牛眼鯛](../Page/牛眼鯛.md "wikilink")（*Boops boops*）
  - [條紋牛眼鯛](../Page/條紋牛眼鯛.md "wikilink")（*Boops lineatus*）

<!-- end list -->

  - [擬牛眼鯛屬](../Page/擬牛眼鯛屬.md "wikilink")（*Boopsoidea*）

<!-- end list -->

  - [丑擬牛眼鯛](../Page/丑擬牛眼鯛.md "wikilink")（*Boopsoidea inornata*）

<!-- end list -->

  - [蘆鯛屬](../Page/蘆鯛屬.md "wikilink")（*Calamus*）

<!-- end list -->

  - [北蘆鯛](../Page/北蘆鯛.md "wikilink")（*Calamus arctifrons*）
  - [大西洋蘆鯛](../Page/大西洋蘆鯛.md "wikilink")（*Calamus bajonado*）
  - [短體蘆鯛](../Page/短體蘆鯛.md "wikilink")（*Calamus brachysomus*）
  - [蘆鯛](../Page/蘆鯛.md "wikilink")（Calamus calamus''）
  - [彎口蘆鯛](../Page/彎口蘆鯛.md "wikilink")（*Calamus campechanus*）
  - [鹿角蘆鯛](../Page/鹿角蘆鯛.md "wikilink")（*Calamus cervigoni*）
  - [白骼蘆鯛](../Page/白骼蘆鯛.md "wikilink")（*Calamus leucosteus*）
  - [巴西蘆鯛](../Page/巴西蘆鯛.md "wikilink")（*Calamus mu*）
  - [瘤突蘆鯛](../Page/瘤突蘆鯛.md "wikilink")（*Calamus nodosus*）
  - [羊頭蘆鯛](../Page/羊頭蘆鯛.md "wikilink")（*Calamus penna*）
  - [翼蘆鯛](../Page/翼蘆鯛.md "wikilink")（*Calamus pennatula*）
  - [小頭蘆鯛](../Page/小頭蘆鯛.md "wikilink")（*Calamus proridens*）
  - [韌皮蘆鯛](../Page/韌皮蘆鯛.md "wikilink")（*Calamus taurinus*）

<!-- end list -->

  - [冬鯛屬](../Page/冬鯛屬.md "wikilink")（*Cheimerius*）

<!-- end list -->

  - [松原氏冬鯛](../Page/松原氏冬鯛.md "wikilink")（*Cheimerius matsubarai*）
  - [紫背冬鯛](../Page/紫背冬鯛.md "wikilink")（*Cheimerius nufar*）

<!-- end list -->

  - [麗眼鯛屬](../Page/麗眼鯛屬.md "wikilink")（*Chrysoblephus*）

<!-- end list -->

  - [紅帶麗眼鯛](../Page/紅帶麗眼鯛.md "wikilink")（*Chrysoblephus
    anglicus*）：又稱英吉金鯛。
  - [紅鰭麗眼鯛](../Page/紅鰭麗眼鯛.md "wikilink")（*Chrysoblephus cristiceps*）
  - [駝背麗眼鯛](../Page/駝背麗眼鯛.md "wikilink")（*Chrysoblephus gibbiceps*）
  - [白條麗眼鯛](../Page/白條麗眼鯛.md "wikilink")（*Chrysoblephus laticeps*）
  - [馬臉麗眼鯛](../Page/馬臉麗眼鯛.md "wikilink")（*Chrysoblephus lophus*）
  - [隆背麗眼鯛](../Page/隆背麗眼鯛.md "wikilink")（*Chrysoblephus puniceus*）

<!-- end list -->

  - [金鯛屬](../Page/金鯛屬.md "wikilink")（*Chrysophrys*）

<!-- end list -->

  - [銀金鯛](../Page/銀金鯛.md "wikilink")（*Chrysophrys auratus*）

<!-- end list -->

  - [波牙鯛屬](../Page/波牙鯛屬.md "wikilink")（*Crenidens*）

<!-- end list -->

  - [波牙鯛](../Page/波牙鯛.md "wikilink")（*Crenidens crenidens*）

<!-- end list -->

  - [眶鱗鯛屬](../Page/眶鱗鯛屬.md "wikilink")（*Cymatoceps*）

<!-- end list -->

  - [眶鱗鯛](../Page/眶鱗鯛.md "wikilink")（*Cymatoceps nasutus*）

[Dentex_fourmanoiri.jpg](https://zh.wikipedia.org/wiki/File:Dentex_fourmanoiri.jpg "fig:Dentex_fourmanoiri.jpg")''\]\]

  - [牙鯛屬](../Page/牙鯛屬.md "wikilink")（*Dentex*）

<!-- end list -->

  - [阿部牙鯛](../Page/阿部牙鯛.md "wikilink")（*Dentex abei*）
  - [安哥拉牙鯛](../Page/安哥拉牙鯛.md "wikilink")（*Dentex angolensis*）
  - [巴氏牙鯛](../Page/巴氏牙鯛.md "wikilink")（*Dentex barnardi*）
  - [紅尾牙鯛](../Page/紅尾牙鯛.md "wikilink")（*Dentex canariensis*）
  - [剛果牙鯛](../Page/剛果牙鯛.md "wikilink")（*Dentex congoensis*）
  - [細點牙鯛](../Page/細點牙鯛.md "wikilink")（*Dentex dentex*）
  - [弗氏牙鯛](../Page/弗氏牙鯛.md "wikilink")（*Dentex fourmanoiri*）
  - [絲鰭牙鯛](../Page/絲鰭牙鯛.md "wikilink")（*Dentex gibbosus*）：又稱線冬鯛。
  - [大眼牙鯛](../Page/大眼牙鯛.md "wikilink")（*Dentex macrophthalmus*）
  - [摩洛哥牙鯛](../Page/摩洛哥牙鯛.md "wikilink")（*Dentex maroccanus*）
  - [鯛形牙鯛](../Page/鯛形牙鯛.md "wikilink")（*Dentex spariformis*）
  - [黃牙鯛](../Page/黃牙鯛.md "wikilink")（*Dentex tumifrons*）

<!-- end list -->

  - [重牙鯛屬](../Page/重牙鯛屬.md "wikilink")（*Diplodus*）

<!-- end list -->

  - [尾斑重牙鯛](../Page/尾斑重牙鯛.md "wikilink")（*Diplodus annularis*）
  - [銀重牙鯛](../Page/銀重牙鯛.md "wikilink")（*Diplodus argenteus argenteus*）
  - [尾斑銀重牙鯛](../Page/尾斑銀重牙鯛.md "wikilink")（*Diplodus argenteus
    caudimacula*）
  - [縱帶重牙鯛](../Page/縱帶重牙鯛.md "wikilink")（*Diplodus bellottii*）
  - [百慕達重牙鯛](../Page/百慕達重牙鯛.md "wikilink")（*Diplodus bermudensis*）
  - [黑尾重牙鯛](../Page/黑尾重牙鯛.md "wikilink")（*Diplodus capensis*）：又稱暗帶重牙鯛。
  - [橫帶重牙鯛](../Page/橫帶重牙鯛.md "wikilink")（*Diplodus cervinus cervinus*）
  - [黑帶重牙鯛](../Page/黑帶重牙鯛.md "wikilink")（*Diplodus cervinus
    hottentotus*）
  - [阿曼重牙鯛](../Page/阿曼重牙鯛.md "wikilink")（*Diplodus cervinus omanensis*）
  - [六帶重牙鯛](../Page/六帶重牙鯛.md "wikilink")（*Diplodus fasciatus*）
  - [霍氏重牙鯛](../Page/霍氏重牙鯛.md "wikilink")（*Diplodus holbrooki*）
  - [紅海重牙鯛](../Page/紅海重牙鯛.md "wikilink")（*Diplodus noct*）
  - [雙帶重牙鯛](../Page/雙帶重牙鯛.md "wikilink")（*Diplodus prayensis*）
  - [尖吻重牙鯛](../Page/尖吻重牙鯛.md "wikilink")（*Diplodus puntazzo*）
  - [囊重牙鯛](../Page/囊重牙鯛.md "wikilink")（*Diplodus sargus ascensionis*）
  - [異帶重牙鯛](../Page/異帶重牙鯛.md "wikilink")（*Diplodus sargus cadenati*）
  - [沼澤重牙鯛](../Page/沼澤重牙鯛.md "wikilink")（*Diplodus sargus helenae*）
  - [單斑重牙鯛](../Page/單斑重牙鯛.md "wikilink")（*Diplodus sargus kotschyi*）
  - [少帶重牙鯛](../Page/少帶重牙鯛.md "wikilink")（*Diplodus sargus lineatus*）
  - [沙重牙鯛](../Page/沙重牙鯛.md "wikilink")（*Diplodus sargus sargus*）
  - [項帶重牙鯛](../Page/項帶重牙鯛.md "wikilink")（*Diplodus vulgaris*）

<!-- end list -->

  - [犁齒鯛屬](../Page/犁齒鯛屬.md "wikilink")（*Evynnis*）

<!-- end list -->

  - [二長棘犁齒鯛](../Page/二長棘犁齒鯛.md "wikilink")（*Evynnis cardinalis*）：又稱魬鯛。
  - [犁齒鯛](../Page/犁齒鯛.md "wikilink")（*Evynnis japonica*）：又稱日本真鯛。
  - [單線梨齒鯛](../Page/單線梨齒鯛.md "wikilink")（*Evynnis mononematos*）
  - [赤鯮](../Page/赤鯮.md "wikilink")（*Evynnis tumifrons*）：又稱黃牙鯛。

<!-- end list -->

  - [獠牙鯛屬](../Page/獠牙鯛屬.md "wikilink")（*Gymnocrotaphus*）

<!-- end list -->

  - [藍線獠牙鯛](../Page/藍線獠牙鯛.md "wikilink")（*Gymnocrotaphus curvidens*）

<!-- end list -->

  - [兔牙鯛屬](../Page/兔牙鯛屬.md "wikilink")（*Lagodon*）

<!-- end list -->

  - [菱體兔牙鯛](../Page/菱體兔牙鯛.md "wikilink")（*Lagodon rhomboides*）

<!-- end list -->

  - [石頜鯛屬](../Page/石頜鯛屬.md "wikilink")（*Lithognathus*）

<!-- end list -->

  - [短頭石頜鯛](../Page/短頭石頜鯛.md "wikilink")（*Lithognathus aureti*）
  - [長頭石頜鯛](../Page/長頭石頜鯛.md "wikilink")（*Lithognathus lithognathus*）
  - [細條石頜鯛](../Page/細條石頜鯛.md "wikilink")（*Lithognathus mormyrus*）
  - [奧氏石頜鯛](../Page/奧氏石頜鯛.md "wikilink")（*Lithognathus olivieri*）

<!-- end list -->

  - [尾斑鯛屬](../Page/尾斑鯛屬.md "wikilink")（*Oblada*）

<!-- end list -->

  - [黑尾斑鯛](../Page/黑尾斑鯛.md "wikilink")（*Oblada melanura*）

<!-- end list -->

  - [切齒鯛屬](../Page/切齒鯛屬.md "wikilink")（*Pachymetopon*）

<!-- end list -->

  - [藍切齒鯛](../Page/藍切齒鯛.md "wikilink")（*Pachymetopon aeneum*）
  - [布氏切齒鯛](../Page/布氏切齒鯛.md "wikilink")（*Pachymetopon blochii*）
  - [鏽色切齒鯛](../Page/鏽色切齒鯛.md "wikilink")（*Pachymetopon grande*）

<!-- end list -->

  - [小鯛屬](../Page/小鯛屬.md "wikilink")（*Pagellus*）

<!-- end list -->

  - [腋斑小鯛](../Page/腋斑小鯛.md "wikilink")（*Pagellus acarne*）
  - [阿拉伯小鯛](../Page/阿拉伯小鯛.md "wikilink")（*Pagellus affinis*）
  - [貝氏小鯛](../Page/貝氏小鯛.md "wikilink")（*Pagellus bellottii*）
  - [南非小鯛](../Page/南非小鯛.md "wikilink")（*Pagellus natalensis*）
  - [黑斑小鯛](../Page/黑斑小鯛.md "wikilink")（*Pagellus bogaraveo*）
  - [緋小鯛](../Page/緋小鯛.md "wikilink")（*Pagellus erythrinus*）

<!-- end list -->

  - [赤鯛屬](../Page/赤鯛屬.md "wikilink")（*Pagrus*）

<!-- end list -->

  - [非洲赤鯛](../Page/非洲赤鯛.md "wikilink")（*Pagrus africanus*）
  - [三長棘赤鯛](../Page/三長棘赤鯛.md "wikilink")（*Pagrus auriga*）
  - [藍點赤鯛](../Page/藍點赤鯛.md "wikilink")（*Pagrus caeruleostictus*）
  - [真鯛](../Page/真鯛.md "wikilink")（*Pagrus major*）：又稱嘉鱲魚、真赤鯛。
  - [赤鯛](../Page/赤鯛.md "wikilink")（*Pagrus pagrus*）

<!-- end list -->

  - [二長棘鯛屬](../Page/二長棘鯛屬.md "wikilink")（*Parargyrops*）

<!-- end list -->

  - [二長棘鯛](../Page/二長棘鯛.md "wikilink")（*Parargyrops edita*）

<!-- end list -->

  - [強齒鯛屬](../Page/強齒鯛屬.md "wikilink")（*Petrus*）

<!-- end list -->

  - [南非強齒鯛](../Page/南非強齒鯛.md "wikilink")（*Petrus rupestris*）

<!-- end list -->

  - [鈍牙鯛屬](../Page/鈍牙鯛屬.md "wikilink")（*Polyamblyodon*）

<!-- end list -->

  - [長腹鈍牙鯛](../Page/長腹鈍牙鯛.md "wikilink")（*Polyamblyodon germanum*）
  - [駝背鈍牙鯛](../Page/駝背鈍牙鯛.md "wikilink")（*Polyamblyodon
    gibbosum*）：又稱莫桑比克鈍牙鯛。

<!-- end list -->

  - [擬牙鯛屬](../Page/擬牙鯛屬.md "wikilink")（*Polysteganus*）

<!-- end list -->

  - [貝氏擬牙鯛](../Page/貝氏擬牙鯛.md "wikilink")（*Polysteganus baissaci*）
  - [藍點擬牙鯛](../Page/藍點擬牙鯛.md "wikilink")（*Polysteganus
    coeruleopunctatus*）
  - [馬斯克林擬牙鯛](../Page/馬斯克林擬牙鯛.md "wikilink")（*Polysteganus
    mascarenensis*）
  - [前眶擬牙鯛](../Page/前眶擬牙鯛.md "wikilink")（*Polysteganus praeorbitalis*）
  - [波紋擬牙鯛](../Page/波紋擬牙鯛.md "wikilink")（*Polysteganus undulosus*）

<!-- end list -->

  - [豬嘴鯛屬](../Page/豬嘴鯛屬.md "wikilink")（*Porcostoma*）

<!-- end list -->

  - [紅豬嘴鯛](../Page/紅豬嘴鯛.md "wikilink")（*Porcostoma dentata*）

<!-- end list -->

  - [裸翼鯛屬](../Page/裸翼鯛屬.md "wikilink")（*Pterogymnus*）

<!-- end list -->

  - [紅裸翼鯛](../Page/紅裸翼鯛.md "wikilink")（*Pterogymnus laniarius*）

<!-- end list -->

  - [平鯛屬](../Page/平鯛屬.md "wikilink")（*Rhabdosargus*）

<!-- end list -->

  - [圓頭平鯛](../Page/圓頭平鯛.md "wikilink")（*Rhabdosargus globiceps*）
  - [紅海平鯛](../Page/紅海平鯛.md "wikilink")（*Rhabdosargus haffara*）
  - [黃帶平鯛](../Page/黃帶平鯛.md "wikilink")（*Rhabdosargus holubi*）
  - [黃錫鯛](../Page/黃錫鯛.md "wikilink")（*Rhabdosargus sarba*）：又稱平鯛。
  - [大眼平鯛](../Page/大眼平鯛.md "wikilink")（*Rhabdosargus thorpei*）

<!-- end list -->

  - [叉牙鯛屬](../Page/叉牙鯛屬.md "wikilink")（*Sarpa*）

<!-- end list -->

  - [叉牙鯛](../Page/叉牙鯛.md "wikilink")（*Sarpa salpa*）

<!-- end list -->

  - [矛鯛屬](../Page/矛鯛屬.md "wikilink")（*Sparidentex*）

<!-- end list -->

  - [矛鯛](../Page/矛鯛.md "wikilink")（*Sparidentex hasta*）

<!-- end list -->

  - [石齒鯛屬](../Page/石齒鯛屬.md "wikilink")（*Sparodon*）

<!-- end list -->

  - [桔尾石齒鯛](../Page/桔尾石齒鯛.md "wikilink")（*Sparodon durbanensis*）

<!-- end list -->

  - [鯛屬](../Page/鯛屬.md "wikilink")（*Sparus*）

<!-- end list -->

  - [金頭鯛](../Page/金頭鯛.md "wikilink")（*Sparus aurata*）

<!-- end list -->

  - [椎鯛屬](../Page/椎鯛屬.md "wikilink")（*Spondyliosoma*）

<!-- end list -->

  - [黑椎鯛](../Page/黑椎鯛.md "wikilink")（*Spondyliosoma canthara*）
  - [絨牙椎鯛](../Page/絨牙椎鯛.md "wikilink")（*Spondyliosoma emarginatum*）

<!-- end list -->

  - [門齒鯛屬](../Page/門齒鯛屬.md "wikilink")（*Stenotomus*）

<!-- end list -->

  - [長棘門齒鯛](../Page/長棘門齒鯛.md "wikilink")（*Stenotomus caprinus*）
  - [門齒鯛](../Page/門齒鯛.md "wikilink")（*Stenotomus chrysops*）

<!-- end list -->

  - [突頜鯛屬](../Page/突頜鯛屬.md "wikilink")（*Virididentex*）

<!-- end list -->

  - [突頜鯛](../Page/突頜鯛.md "wikilink")（*Virididentex acromegalus*）

## 生態

大部分屬於群游性，少數會入侵河口區。本[科大多屬於肉食性魚類或雜食性魚類](../Page/科.md "wikilink")。許多鯛科為兩性體，即體內同時擁有雌性和雄性的生殖腺。有些則會由雄性變雌性或由雌性變雄性，但體色不會隨「性轉變」而變色。

## 經濟利用

屬於高級的食用魚類，具高經濟及商業價值，部份種類更是為馴化為養殖魚類。適合各種烹飪方式食用。另外也是甚佳的遊釣魚類。

## 注释

## 参考文献

## 外部链接

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[\*](../Category/鯛科.md "wikilink")