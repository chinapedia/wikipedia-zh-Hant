**鯥科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目的其中一個](../Page/鱸形目.md "wikilink")[科](../Page/科.md "wikilink")，只有一屬三種，皆為海魚。鯥科魚有兩條[背鰭](../Page/背鰭.md "wikilink")，特色是其背鰭的柔軟部分及[尾鰭均有](../Page/尾鰭.md "wikilink")[魚鱗覆蓋](../Page/魚鱗.md "wikilink")。魚眼大。生活於深海多石的地區，最深可達400公尺。總長度最長可達1.5公尺，重16公斤。

## 分類

**鯥科**其下僅有一屬：

  - [青鯥屬](../Page/青鯥屬.md "wikilink")（*Scombrops*）
      - [牛眼青鯥](../Page/牛眼青鯥.md "wikilink")（*Scombrops boops*
        <small>([Houttuyn](../Page/:en:Martinus_Houttuyn.md "wikilink"),
        1782)</small>）：又稱牛尾鯥。
      - [吉氏青鯥](../Page/吉氏青鯥.md "wikilink")（*Scombrops gilberti*
        <small>([Jordan](../Page/:en:David_Starr_Jordan.md "wikilink") &
        Snyder,
        1901)</small>）：生活於[西太平洋](../Page/西太平洋.md "wikilink")，包括日本。
      - [古巴青鯥](../Page/古巴青鯥.md "wikilink")（*Scombrops oculatus*
        <small>([Poey](../Page/:en:Felipe_Poey.md "wikilink"),
        1860)</small>）：生活於[西大西洋的亞熱帶海域](../Page/西大西洋.md "wikilink")，特別是[美國的](../Page/美國.md "wikilink")[佛羅里達州及](../Page/佛羅里達州.md "wikilink")[巴哈馬群島一帶](../Page/巴哈馬群島.md "wikilink")。

## 參考資料

  -
[鯥科](../Category/鯥科.md "wikilink")
[Category:單屬科魚類](../Category/單屬科魚類.md "wikilink")