**鱂脂鯉科**，又稱**短嘴脂鯉科**\[1\]，為[脂鯉亞目的其一](../Page/脂鯉亞目.md "wikilink")[科](../Page/科.md "wikilink")。其下生物原產於[哥斯達黎加](../Page/哥斯達黎加.md "wikilink")、[巴拿馬和](../Page/巴拿馬.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")。鱂脂鯉科的魚個體都很小，成年個體長度大約在之間。它們以昆蟲的幼蟲為食。\[2\]

## 分類

鱂脂鯉科下分2亞科7屬，如下：

  - [鱂脂鯉亞科](../Page/鱂脂鯉亞科.md "wikilink")（Lebiasininae）
      - [德勒姆鱂脂鯉屬](../Page/德勒姆鱂脂鯉屬.md "wikilink")（*Derhamia*）
      - [鱂脂鯉屬](../Page/鱂脂鯉屬.md "wikilink")（*Lebiasina*）
      - [片鱂脂鯉屬](../Page/片鱂脂鯉屬.md "wikilink")（*Piabucina*）

<!-- end list -->

  - [翹嘴脂鯉亞科](../Page/翹嘴脂鯉亞科.md "wikilink")（Pyrrhulininae）
      - [短頜鱂脂鯉屬](../Page/短頜鱂脂鯉屬.md "wikilink")（*Copeina*）
      - [絲鰭脂鯉屬](../Page/絲鰭脂鯉屬.md "wikilink")（*Copella*）
      - [鉛筆魚屬](../Page/鉛筆魚屬.md "wikilink")（小口脂鯉屬）（*Nannostomus*）
      - [翹嘴脂鯉屬](../Page/翹嘴脂鯉屬.md "wikilink")（*Pyrrhulina*）

## 參考文獻

## 外部連結

  -
  - Nelson, Joseph S. (2006). *Fishes of the World*. John Wiley & Sons,
    Inc. ISBN 978-0-471-25031-9

[\*](../Category/短嘴脂鯉科.md "wikilink")

1.
2.