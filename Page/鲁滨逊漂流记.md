**鲁滨遜漂流记**（，又译作**鲁滨孙漂流记**，或**鲁宾逊漂流记**，**鲁宾孙漂流记**。直译作**鲁宾逊·克鲁索**）是一本由[丹尼爾·笛福](../Page/丹尼爾·笛福.md "wikilink")59岁时所著的第一部[小说](../Page/小说.md "wikilink")，首次出版于1719年4月25日。这本小说被认为是第一本用[英文以日記形式写成的小说](../Page/英文.md "wikilink")，享有英国第一部现实主义长篇小说的头衔。由於原書名過長，通常簡稱為上述的標題。

小说讲述了一位海难的幸存者鲁滨逊在一个偏僻荒凉的热带小岛-特立尼达拉岛上度过28年的故事，而其伙伴是主人公从食人族手中救下的一个被俘虏的土著人。

这个故事的创作一般認為是由蘇格蘭人[亞歷山大·塞爾科克的亲身经历所启发](../Page/亞歷山大·塞爾科克.md "wikilink")，亚历山大·塞尔科克曾流落于智利南太平洋岛的一个叫做Más
a
Tierra的小岛四年之久（该岛于1966更名为[鲁宾逊·克鲁索岛](../Page/魯賓遜克魯索島.md "wikilink")），是[智利](../Page/智利.md "wikilink")[胡安·費爾南德斯群島中的第二大島](../Page/胡安·費爾南德斯群島.md "wikilink")。

## 故事梗概

1651年8月，鲁滨逊（姓氏来自于德文"Kreutznaer"）违背父母希望他去从事法律专业的期望，下海启程前往赫尔的女王港。旅程十分颠簸，船在暴风中遇险，然而出于对航海的执着，鲁滨逊再次远航。这次，船被[塞拉海盗劫持](../Page/塞拉_\(摩洛哥\).md "wikilink")，鲁滨逊成了[摩尔人的奴隶](../Page/摩尔人.md "wikilink")。两年后，他跟随一位名叫萊克的男孩逃跑；葡萄牙航船的船长在西非海岸救了他。船去了巴西。在船长的帮助下，鲁滨逊获得了一大片种植园。

几年后，鲁滨逊加入了贩奴航船。1659年9月30日，船在离小岛40里外的[奥里诺科河口处遇到风暴](../Page/奥里诺科河.md "wikilink")。鲁滨逊小岛可能源自于加勒比的[多巴哥岛](../Page/多巴哥岛.md "wikilink")，因为该小岛地处委内瑞拉海岸北部，靠近奥里诺科河开口，在[特立尼达岛视野范围内](../Page/特立尼达岛.md "wikilink")。\[1\]鲁滨逊和三只动物，即船长的狗和两只猫幸免于难。挺过绝望情绪后，鲁滨逊趁船还没完全沉下去时，找出了武器、工具以及其它补给。他在岛上建造了避难所，通过在木头上刻字来记录时间。在工具的帮助下，鲁滨逊可以打猎、种植谷物、风干葡萄、做陶器、养羊。他也捉到了一只小鹦鹉。

几年后，鲁滨逊发现当地人活在同类相食的吃人社会下，有时这些土著会到访海岛，杀死并吃掉俘虏。最初，鲁滨逊计划将他们全部杀死，但后来他意识到自己无权这么做，因为食人族们并不知道这样做是错误的。鲁滨逊试图通过解救俘虏来获得一些仆人；当一个俘虏逃跑时，鲁滨逊帮助了他，由于当天是星期五，因而给该土著人命名为“星期五”。在经历了[野人](../Page/野人.md "wikilink")、[俘虏之后](../Page/俘虏.md "wikilink")，在岛上以[总督自居](../Page/总督.md "wikilink")，成为荒岛的统治者。

更多的土著跑到岛上开食人派对，鲁滨逊和星期五随即击杀了他们，并救出了两名俘虏。其中一位是星期五的父亲，另一位是个西班牙人，后者告诉鲁滨逊其它遇难的西班牙船只。他们做出计划：西班牙人与星期五的父亲回去，把更多的人带回来，建造船只，驶向西班牙港口。

然而，在西班牙人出现之前，一艘英国船出现了；叛徒控制了船只，打算把他们的船长扔在岛上。鲁滨逊、船长和忠于船长的船员平息了叛乱，并把叛徒扔在了岛上。在启程去英国前，鲁滨逊告诉叛徒自己是如何在岛上生存的，并告知还有其它人会来。1686年12月19日，鲁滨逊启程，并于1687年6月11日到达英国。他得知家人认为自己死了，所以父亲的遗嘱上没有给他留什么份。鲁滨逊便去里斯本寻找自己在巴西的财产，他将财富运回英国，回避了海上的航行，获得了丰厚的收入；娶妻生子，过起安定的生活。最后一次的冒险是鲁滨逊和星期五在穿越比利牛斯山时击退饥饿的狼。

## 创作背景

[Robinson_Crusoe_and_Man_Friday_Offterdinger.jpg](https://zh.wikipedia.org/wiki/File:Robinson_Crusoe_and_Man_Friday_Offterdinger.jpg "fig:Robinson_Crusoe_and_Man_Friday_Offterdinger.jpg")
小说《鲁滨逊漂流记》是笛福受[苏格兰水手](../Page/苏格兰.md "wikilink")[亚历山大·塞尔科克的经历所启发而写成](../Page/亚历山大·塞尔科克.md "wikilink")。塞尔科克在一次远航猎[鲸航中](../Page/鲸.md "wikilink")，被抛弃在荒岛上生活了四年零四个多月，于1709年被救返回英国。\[2\]而笛福在一次朋友的聚会上遇到塞尔科克，以其经历为主线写就《鲁滨逊漂流记》。该作品被翻译成为[英](../Page/英语.md "wikilink")、[法](../Page/法语.md "wikilink")、[德](../Page/德语.md "wikilink")、[意](../Page/意大利語.md "wikilink")、[荷](../Page/荷兰语.md "wikilink")、[漢等各种语言版本](../Page/漢語.md "wikilink")。

## 反响和续集

[SFEC_HULL_CRUSOE1.JPG](https://zh.wikipedia.org/wiki/File:SFEC_HULL_CRUSOE1.JPG "fig:SFEC_HULL_CRUSOE1.JPG")
小说在1719年4月25日出版，并在年终前四次再版。

在十九世纪末，西方文学中还没有哪部书比《鲁滨逊漂流记》有更多的版本、衍生品、译文，后者甚至有700多种改编，包括无文字的小儿图书。\[3\]

笛福继续写了一些不出名的续集，如《鲁滨逊·克鲁索的更多漂流记》(*The Farther Adventures of Robinson
Crusoe*)。该书原本是小说的最后一部分，根据第一版原先的标题页上这是第三部分，即后加的《鲁滨逊·克鲁索对人生和冒险的严肃回顾，以及他預見的神間世界》(*Serious
Reflections During the Life & Surprising Adventures of Robinson Crusoe,
With His Vision of the Angelic World*)；基本上，这部分主要是道德论述，并赋以克鲁索的名字以增添吸引力。

## 解释

### 殖民

作家[詹姆斯·乔伊斯注意到不列颠帝国征服的真正象征就是鲁滨逊本人](../Page/詹姆斯·乔伊斯.md "wikilink")：“他是不列颠殖民者的真实原型。
… 整个盎格鲁-撒克逊精神就是克鲁索：富含男子独立性、无意识地残忍、固执、缓慢但有效的智能、性别冷漠、精于算计。”\[4\]

克鲁索尝试在岛上重建自己的社会。这是通过使用欧洲技术、农业、甚至是一些政治阶级来实现的。好几次，小说中克鲁索都把自己比作岛上的“国王”，而船长将自己描述为反叛者的“执政官”。小说末尾，小岛被赤裸裸地描述为“殖民地”。理想主义的主仆关系则是克鲁索和星期五，即[文化帝国主义](../Page/文化帝国主义.md "wikilink")。克鲁索代表着‘启蒙的’欧洲，而星期五则是‘原始的’人类，后被同化、进入克鲁索的世界，以逃脱野蛮的生活。不管怎样，笛福都抓住了机会，批评西班牙对南美的征服。

### 道德

就食人而言，克鲁索在[文化相對論倍感纠结](../Page/文化相對論.md "wikilink")。虽然他对此感到十分恶心，但这一问题在当地文化中根深蒂固，克鲁索也无法追究其道义上的责任。不过，克鲁索依然坚持了自己对道德绝对标准的信仰；他认为同类相食是‘国家级大罪’，不允许星期五这么做。

### 经济

笛福早年的曾为[政府撰写过大量的](../Page/政府.md "wikilink")[政论文](../Page/政论文.md "wikilink")，因此其作品有明显的自我思考与[政治映射](../Page/政治.md "wikilink")，这其中包括了对[社会](../Page/社会.md "wikilink")、[人性以及](../Page/人性.md "wikilink")[宗教等的思考](../Page/宗教.md "wikilink")，同时也反映了当时的社会[价值观和](../Page/价值观.md "wikilink")[道德标准](../Page/道德.md "wikilink")。马列主义史观认为:
小说《鲁滨逊漂流记》作为一部[旅行与](../Page/旅行.md "wikilink")[历险小说](../Page/历险小说.md "wikilink")（冒险小说），在描写主人公鲁滨逊的种种经历的同时，也展示和剖析了人物的思想性格。歌颂了人类的智慧与勤劳美德。同时，由于鲁滨逊的形象富有与18世纪的英国[资本主义](../Page/资本主义.md "wikilink")[资产阶级类似的奋斗进取与开拓征服的精神](../Page/资产阶级.md "wikilink")，作品亦被认作为歌颂资本主义精神以及企图使[大人共产主义合法化的叙事](../Page/大人共产主义.md "wikilink")。\[5\]

## 附註

### 註解

### 參考文獻

<span style="font-size:smaller;">

1.  </span>

<!-- end list -->

1.  . 笛福，徐霞村(翻译).《鲁滨孙漂流记》.北京：人民文学出版社，1959 ISBN 978-7-02-003988-3
2.  . [Audiobook
    MP3](http://www.classicistranieri.com/daniel-de-foe-robison-crusoe-audiobook-mp3.html)

[R](../Category/英国小说.md "wikilink") [R](../Category/英格蘭小說.md "wikilink")
[Category:遇難漂流題材小說](../Category/遇難漂流題材小說.md "wikilink")
[Category:無人島背景小说](../Category/無人島背景小说.md "wikilink")
[Category:海上虚构题材作品](../Category/海上虚构题材作品.md "wikilink")
[Category:人吃人題材作品](../Category/人吃人題材作品.md "wikilink")
[Category:海盜書籍](../Category/海盜書籍.md "wikilink")
[Category:改編成電影的英國小說](../Category/改編成電影的英國小說.md "wikilink")
[Category:電視劇原著小說](../Category/電視劇原著小說.md "wikilink")

1.  *Robinson Crusoe*, Chapter 23.
2.
3.  [Ian Watt](../Page/Ian_Watt.md "wikilink"). "Robinson Crusoe as a
    Myth", from *Essays in Criticism* (April 1951). Reprinted in the
    Norton Critical Edition (second edition, 1994) of *Robinson Crusoe*.
4.  James Joyce, “Daniel Defoe,” translated from Italian manuscript and
    edited by Joseph Prescott, *Buffalo Studies* 1 (1964): 24–25
5.