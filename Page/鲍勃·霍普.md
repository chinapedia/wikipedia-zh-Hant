[BobHope-T-33.jpg](https://zh.wikipedia.org/wiki/File:BobHope-T-33.jpg "fig:BobHope-T-33.jpg")

**莱斯利·汤斯·霍普**（，），常被称为**鲍勃·霍普**（），是[美国著名的演艺者](../Page/美国.md "wikilink")、喜剧演员，曾经出现在[百老汇](../Page/百老汇.md "wikilink")、广播中、电视上以及电影中。霍普以他几部百老汇音乐剧开始闻名。在成为演员之前，鲍勃·霍普曾以「Packey
East」的名字在拳击场打[拳击](../Page/拳击.md "wikilink")。

鲍勃·霍普出生于[英国的](../Page/英国.md "wikilink")[埃尔特姆](../Page/埃尔特姆.md "wikilink")，在7兄弟中排行第五。他的父亲英国人威廉·亨利·霍普是一名石匠，他的母亲艾维斯·汤斯（Avis
Townes）是一个戏剧歌唱家。他一家人于1907年移居美國[克里夫蘭](../Page/克里夫蘭_\(俄亥俄州\).md "wikilink")，于1920年获得美国公民身份。

曾经4次获得[奥斯卡荣誉奖](../Page/奥斯卡荣誉奖.md "wikilink")。在美国享有很高声誉。他是[共和黨支持者](../Page/共和黨_\(美國\).md "wikilink")，與[隆納·雷根總統及共事過的影星](../Page/隆納·雷根.md "wikilink")[詹姆斯·史都華交情甚篤](../Page/詹姆斯·史都華.md "wikilink")。

霍普曾于1979年6月至[中国拍摄电视片](../Page/中華人民共和國.md "wikilink")《[去中国之路](../Page/去中国之路.md "wikilink")》，在此期间曾登上过[长城](../Page/长城.md "wikilink")、去过[天安门广场](../Page/天安门广场.md "wikilink")，成为当时中国与西方世界交流的使者。此段影像亦出现在纪录片《[天安门](../Page/天安门_\(纪录片\).md "wikilink")》中。

在他100岁生日的两个月后，2003年7月27日，他因[肺炎在家中逝世](../Page/肺炎.md "wikilink")。

在鲍勃·霍普還在世時，[美國海軍以他的名字替新下水的](../Page/美國海軍.md "wikilink")（USNS Bob Hope
T-AKR-300）命名，使得霍普除了成為海軍史上少數還在世時就被選為船隻命名來源的人之外，也是第一位被作為軍用船隻命名的藝人。除此之外，[美國空軍也將一架編號AF](../Page/美國空軍.md "wikilink")99-211的[C-17「空中霸主III式」運輸機命名為](../Page/C-17.md "wikilink")「鮑勃·霍普精神號」（Spirit
of Bob Hope），是一系列以[榮譽勳章](../Page/榮譽勳章.md "wikilink")（Medal of
Honor）得主命名的C-17機隊之一。

## 參考文獻

## 外部链接

  - [鲍勃·霍普的主页](https://web.archive.org/web/20030802082316/http://www.bobhope.com/bob.htm)

  -
  -
{{-}}

[Category:奥斯卡荣誉奖获得者](../Category/奥斯卡荣誉奖获得者.md "wikilink")
[Category:美國天主教徒](../Category/美國天主教徒.md "wikilink")
[Category:美國人瑞](../Category/美國人瑞.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:美國電視主持人](../Category/美國電視主持人.md "wikilink")
[Category:加利福尼亚州共和党人](../Category/加利福尼亚州共和党人.md "wikilink")
[Category:金球獎終身成就獎獲得者](../Category/金球獎終身成就獎獲得者.md "wikilink")
[Category:國會金質獎章獲得者](../Category/國會金質獎章獲得者.md "wikilink")
[Category:天主教皈依者](../Category/天主教皈依者.md "wikilink")
[Category:死于肺炎的人](../Category/死于肺炎的人.md "wikilink")
[Category:肯尼迪中心荣誉奖得主](../Category/肯尼迪中心荣誉奖得主.md "wikilink")
[Category:皮博迪獎得主](../Category/皮博迪獎得主.md "wikilink")
[Category:克里夫蘭人](../Category/克里夫蘭人.md "wikilink")
[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:美國國家藝術獎章獲得者](../Category/美國國家藝術獎章獲得者.md "wikilink")
[Category:歌舞雜耍表演者](../Category/歌舞雜耍表演者.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")