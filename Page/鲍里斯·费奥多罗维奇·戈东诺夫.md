[Boris_Godunov_by_anonim_(17th_c.,_GIM).jpg](https://zh.wikipedia.org/wiki/File:Boris_Godunov_by_anonim_\(17th_c.,_GIM\).jpg "fig:Boris_Godunov_by_anonim_(17th_c.,_GIM).jpg")
**鲍里斯·费奥多罗维奇·戈东诺夫**（，），是1598年－1605年在位的[俄国沙皇](../Page/俄国君主列表.md "wikilink")。

[鞑靼贵族出身](../Page/鞑靼.md "wikilink")。曾经侍奉过[伊凡雷帝](../Page/伊凡四世.md "wikilink")。他妹妹伊琳娜·戈东诺娃嫁给伊凡雷帝的幼子[费奧多尔·伊万诺维奇](../Page/费奧多尔·伊万诺维奇.md "wikilink")。费奥多尔身体有病，智力不健全，因此大权被戈东诺夫所掌握。1598年费奧多尔去世，无子嗣。[留里克王朝灭亡](../Page/留里克王朝.md "wikilink")。全俄缙绅会议推举戈东诺夫为俄罗斯沙皇。

戈东诺夫继承伊凡雷帝遗志，停止与[波兰的战争](../Page/波兰.md "wikilink")，在北方向[瑞典发动战争](../Page/瑞典.md "wikilink")，扩大了[波罗的海出海口](../Page/波罗的海.md "wikilink")。向东继续侵略[西伯利亚汗国](../Page/西伯利亚汗国.md "wikilink")，南方与[克里米亚汗国交战](../Page/克里米亚汗国.md "wikilink")，修建了一系列要塞城市。他推进俄罗斯农奴化进程，规定如果一个自由人为他人工作满六个月以上，就沦为这个人的奴仆。还公布逃亡[农奴的追捕期限为五年](../Page/农奴.md "wikilink")。在位末期俄罗斯不断发生农奴起义。

1604年波兰利用留里克王朝后嗣[德米特里·伊萬諾維奇死亡疑案](../Page/烏格里奇的德米特里.md "wikilink")，支持[偽德米特里一世入侵俄国](../Page/偽德米特里一世.md "wikilink")。

1605年戈东诺夫去世，其子[费多尔即位](../Page/费奥多尔·戈东诺夫.md "wikilink")，不久被杀死。其后偽德米特里一世在波兰支持下自立为沙皇。

[俄国文豪](../Page/俄国.md "wikilink")[普希金將戈东诺夫的一生](../Page/亚历山大·谢尔盖耶维奇·普希金.md "wikilink")，并由作曲家[穆索尔斯基](../Page/莫杰斯特·彼得罗维奇·穆索尔斯基.md "wikilink")[再改编成歌剧](../Page/鲍里斯·戈杜诺夫_\(歌剧\).md "wikilink")。

## 参见

  - [俄罗斯历史](../Page/俄罗斯历史.md "wikilink")

[Category:俄国君主](../Category/俄国君主.md "wikilink")