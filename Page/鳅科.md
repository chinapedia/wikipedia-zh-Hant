**鳅科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鯉形目的其中一](../Page/鯉形目.md "wikilink")[科](../Page/科.md "wikilink")。

## 鰍亞科(Cobitinae)

### 擬長鰍屬（*Acanthopsoides*）

  - [豚形擬長鰍](../Page/豚形擬長鰍.md "wikilink")（*Acanthopsoides delphax*）
  - [細擬長鰍](../Page/細擬長鰍.md "wikilink")（*Acanthopsoides gracilentus*）
  - [擬長鰍](../Page/擬長鰍.md "wikilink")（*Acanthopsoides
    gracilis*）：又稱湄公河擬長鰍。
  - [軟擬長鰍](../Page/軟擬長鰍.md "wikilink")（*Acanthopsoides hapalias*）
  - [小口擬長鰍](../Page/小口擬長鰍.md "wikilink")（*Acanthopsoides molobrion*）
  - （*Acanthopsoides namromensis*）
  - [勞氏擬長鰍](../Page/勞氏擬長鰍.md "wikilink")（*Acanthopsoides robertsi*）

### 小刺眼鰍屬（*Acantopsis*）

  - [沙小刺眼鰍](../Page/沙小刺眼鰍.md "wikilink")（*Acantopsis arenae*）
  - [馬頭小刺眼鰍](../Page/馬頭小刺眼鰍.md "wikilink")（*Acantopsis choirorhynchos*）
  - [蒼帶小刺眼鰍](../Page/蒼帶小刺眼鰍.md "wikilink")（*Acantopsis dialuzona*）
  - （*Acantopsis guttatus*）
  - [多斑小刺眼鰍](../Page/多斑小刺眼鰍.md "wikilink")（*Acantopsis multistigmatus*）
  - [八線小刺眼鰍](../Page/八線小刺眼鰍.md "wikilink")（*Acantopsis octoactinotos*）
  - [纖細小刺眼鰍](../Page/纖細小刺眼鰍.md "wikilink")(*Acantopsis spectabilis*)
  - [泰國小刺眼鰍](../Page/泰國小刺眼鰍.md "wikilink")（*Acantopsis thiemmedhi*）

### 雙鬚鰍屬（*Bibarba*）

  - [雙鬚鰍](../Page/雙鬚鰍.md "wikilink")（*Bibarba bibarba*）

### 瘦身馱鰍屬（*Canthophrys*）

  - [瘦身馱鰍](../Page/瘦身馱鰍.md "wikilink")（*Canthophrys gongota*）

### 鰍屬（*Cobitis*）

  - [白鰍](../Page/白鰍.md "wikilink")（*Cobitis albicoloris*）
  - (*Cobitis amphilekta*)
  - [地中海鰍](../Page/地中海鰍.md "wikilink")（*Cobitis arachthosensis*）
  - [南部鰍](../Page/南部鰍.md "wikilink")(*Cobitis australis*)
  - [雙線鰍](../Page/雙線鰍.md "wikilink")（*Cobitis bilineata*）
  - [比氏鰍](../Page/比氏鰍.md "wikilink")（*Cobitis bilseli*）
  - [琵琶湖鰍](../Page/琵琶湖鰍.md "wikilink")（*Cobitis biwae*）
  - [考氏鰍](../Page/考氏鰍.md "wikilink")（*Cobitis calderoni*）
  - [喬氏鰍](../Page/喬氏鰍.md "wikilink")（*Cobitis choii*）
  - [達爾馬鰍](../Page/達爾馬鰍.md "wikilink")（*Cobitis dalmatina*）
  - [長吻鰍](../Page/長吻鰍.md "wikilink")（*Cobitis dolichorhynchus*）
  - [艾拉澤鰍](../Page/艾拉澤鰍.md "wikilink")（*Cobitis elazigensis*）
  - [長鰍](../Page/長鰍.md "wikilink")（*Cobitis elongata*）
  - [似長鰍](../Page/似長鰍.md "wikilink")（*Cobitis elongatoides*）
  - [伊氏鰍](../Page/伊氏鰍.md "wikilink")（*Cobitis evreni*）
  - [費氏鰍](../Page/費氏鰍.md "wikilink")（*Cobitis fahirae*）
  - (*Cobitis faridpaki*)
  - [北方鰍](../Page/北方鰍.md "wikilink")（*Cobitis granoei*）
  - [韓鰍](../Page/韓鰍.md "wikilink")（*Cobitis hankugensis*）
  - [澤鰍](../Page/澤鰍.md "wikilink")（*Cobitis hellenica*）
  - [濘鰍](../Page/濘鰍.md "wikilink")（*Cobitis illyrica*）
  - [克羅地亞鰍](../Page/克羅地亞鰍.md "wikilink")（*Cobitis jadovaensis*）
  - [柏原氏鰍](../Page/柏原氏鰍.md "wikilink")(*Cobitis kaibarai*)
  - [凱文氏鰍](../Page/凱文氏鰍.md "wikilink")(*Cobitis keyvani*)
  - [凱氏鰍](../Page/凱氏鰍.md "wikilink")（*Cobitis kellei*）
  - [庫氏鰍](../Page/庫氏鰍.md "wikilink")（*Cobitis kurui*）
  - [老撾鰍](../Page/老撾鰍.md "wikilink")（*Cobitis laoensis*）
  - [萊氏鰍](../Page/萊氏鰍.md "wikilink")（*Cobitis lebedevi*）
  - [捷鰍](../Page/捷鰍.md "wikilink")（*Cobitis levantina*）
  - [線鰍](../Page/線鰍.md "wikilink")（*Cobitis linea*）
  - [黑龍江鰍](../Page/黑龍江鰍.md "wikilink")（*Cobitis lutheri*）
  - [大斑鰍](../Page/大斑鰍.md "wikilink")（*Cobitis macrostigma*）
  - (*Cobitis magnostriata*)
  - [摩洛哥鰍](../Page/摩洛哥鰍.md "wikilink")（*Cobitis maroccana*）
  - [松原氏鰍](../Page/松原氏鰍.md "wikilink")（*Cobitis matsubarai*）
  - [黑點鰍](../Page/黑點鰍.md "wikilink")（*Cobitis megaspila*）
  - （*Cobitis melanoleuca*）
      - [格氏黑白鰍](../Page/格氏黑白鰍.md "wikilink")（*Cobitis melanoleuca
        gladkovi*）
      - [黑白鰍](../Page/黑白鰍.md "wikilink")（*Cobitis melanoleuca
        melanoleuca*）
  - [南歐鰍](../Page/南歐鰍.md "wikilink")（*Cobitis meridionalis*）
  - （*Cobitis microcephala*）
  - [水森氏鰍](../Page/水森氏鰍.md "wikilink")(*Cobitis minamorii*)
      - [琵琶湖水森氏鰍](../Page/琵琶湖水森氏鰍.md "wikilink")(*Cobitis minamorii
        oumiensis*)
      - [山陰水森氏鰍](../Page/山陰水森氏鰍.md "wikilink")(*Cobitis minamorii
        saninensis*)
      - [東海水森氏鰍](../Page/東海水森氏鰍.md "wikilink")(*Cobitis minamorii
        tokaiensis*)
      - [淀川水森氏鰍](../Page/淀川水森氏鰍.md "wikilink")(*Cobitis minamorii
        yodoensis*)
  - [多彩鰍](../Page/多彩鰍.md "wikilink")（*Cobitis multimaculata*）
  - [納倫鰍](../Page/納倫鰍.md "wikilink")（*Cobitis narentana*）
  - （*Cobitis nuicocensis*）
  - [奧赫里德鰍](../Page/奧赫里德鰍.md "wikilink")（*Cobitis ohridana*）
  - [太平鰍](../Page/太平鰍.md "wikilink")（*Cobitis pacifica*）
  - [沼澤鰍](../Page/沼澤鰍.md "wikilink")（*Cobitis paludica*）
  - [海防鰍](../Page/海防鰍.md "wikilink")（*Cobitis phongnhaensis*）
  - [沼鰍](../Page/沼鰍.md "wikilink")（*Cobitis pontica*）
  - [中斑鰍](../Page/中斑鰍.md "wikilink")（*Cobitis puncticulata*）
  - [斑條鰍](../Page/斑條鰍.md "wikilink")（*Cobitis punctilineata*）
  - [保加利亞鰍](../Page/保加利亞鰍.md "wikilink")（*Cobitis rhodopensis*）
  - [黑海鰍](../Page/黑海鰍.md "wikilink")（*Cobitis rossomeridionalis*）
  - [薩氏鰍](../Page/薩氏鰍.md "wikilink")（*Cobitis satunini*）
  - [四國島鰍](../Page/四國島鰍.md "wikilink")（*Cobitis shikokuensis*）
  - [單棘鰍](../Page/單棘鰍.md "wikilink")（*Cobitis simplicispina*）
  - [中華花鰍](../Page/中華花鰍.md "wikilink")（*Cobitis sinensis*）
  - [明鰍](../Page/明鰍.md "wikilink")（*Cobitis splendens*）
  - （*Cobitis squataeniatus*）
  - [斯氏鰍](../Page/斯氏鰍.md "wikilink")（*Cobitis stephanidisi*）
  - [條紋鰍](../Page/條紋鰍.md "wikilink")（*Cobitis striata*）
  - [瘤鰍](../Page/瘤鰍.md "wikilink")（*Cobitis strumicae*）
  - [花鰍](../Page/花鰍.md "wikilink")（*Cobitis taenia*）
  - [高鰍](../Page/高鰍.md "wikilink")（*Cobitis takatsuensis*）
  - [烏克蘭鰍](../Page/烏克蘭鰍.md "wikilink")（*Cobitis tanaitica*）
  - [克里米亞鰍](../Page/克里米亞鰍.md "wikilink")（*Cobitis taurica*）
  - [四線鰍](../Page/四線鰍.md "wikilink")（*Cobitis tetralineata*）
  - [希臘鰍](../Page/希臘鰍.md "wikilink")（*Cobitis trichonica*）
  - [土耳其鰍](../Page/土耳其鰍.md "wikilink")（*Cobitis turcica*）
  - [短命鰍](../Page/短命鰍.md "wikilink")（*Cobitis vardarensis*）
  - [西班牙鰍](../Page/西班牙鰍.md "wikilink")（*Cobitis vettonica*）
  - （*Cobitis ylengensis*）
  - [贊氏鰍](../Page/贊氏鰍.md "wikilink")（*Cobitis zanandreai*）
  - [浙江鰍](../Page/浙江鰍.md "wikilink")（*Cobitis zhejiangensis*）

### 益秀朝鮮鰍屬（*Iksookimia*）

  - [休氏益秀朝鮮鰍](../Page/休氏益秀朝鮮鰍.md "wikilink")（*Iksookimia hugowolfeldi*）
  - [益秀朝鮮鰍](../Page/益秀朝鮮鰍.md "wikilink")（*Iksookimia koreensis*）
  - [長身益秀朝鮮鰍](../Page/長身益秀朝鮮鰍.md "wikilink")（*Iksookimia longicorpus*）
  - [斑紋益秀朝鮮鰍](../Page/斑紋益秀朝鮮鰍.md "wikilink")（*Iksookimia pumila*）
  - [盈德益秀朝鮮鰍](../Page/盈德益秀朝鮮鰍.md "wikilink")（*Iksookimia yongdokensis*）

### 動鰍屬（*Kichulchoia*）

  - [短紋動鰍](../Page/短紋動鰍.md "wikilink")（*Kichulchoia brevifasciata*）

### 高麗鰍屬（*Koreocobitis*）

  - [洛東江高麗鰍](../Page/洛東江高麗鰍.md "wikilink")（*Koreocobitis naktongensis*）
  - [圓尾高麗鰍](../Page/圓尾高麗鰍.md "wikilink")（*Koreocobitis
    rotundicaudata*）：又稱白吻鰍。

### 柯氏鰍屬（*Kottelatlimia*）

  - [馬吻柯氏鰍](../Page/馬吻柯氏鰍.md "wikilink")（*Kottelatlimia hipporhynchos*）
  - [凱蒂柯氏鰍](../Page/凱蒂柯氏鰍.md "wikilink")（*Kottelatlimia katik*）
  - [銼柯氏鰍](../Page/銼柯氏鰍.md "wikilink")（*Kottelatlimia pristes*）

### 似鱗頭鰍屬（*Lepidocephalichthys*）

  - [秀美似鱗頭鰍](../Page/秀美似鱗頭鰍.md "wikilink")（*Lepidocephalichthys alkaia*）
  - [安氏似鱗頭鰍](../Page/安氏似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    annandalei*）
  - [印度似鱗頭鰍](../Page/印度似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    arunachalensis*）
  - （*Lepidocephalichthys barbatuloides*）
  - [柏氏似鱗頭鰍](../Page/柏氏似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    berdmorei*）
  - [尾斑似鱗頭鰍](../Page/尾斑似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    birmanicus*）
  - [克羅芒似鱗頭鰍](../Page/克羅芒似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    coromandelensis*)
  - [叉尾似鱗頭鰍](../Page/叉尾似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    furcatus*）
  - [戈阿爾帕拉似鱗頭鰍](../Page/戈阿爾帕拉似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    goalparensis*)
  - [岡特似鱗頭鰍](../Page/岡特似鱗頭鰍.md "wikilink")（*Lepidocephalichthys guntea*）
  - [赫氏似鱗頭鰍](../Page/赫氏似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    hasselti*）
  - [雀斑鱗頭鰍](../Page/雀斑鱗頭鰍.md "wikilink")（*Lepidocephalichthys irrorata*）
  - [喬氏似鱗頭鰍](../Page/喬氏似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    jonklaasi*）
  - [細似鱗頭鰍](../Page/細似鱗頭鰍.md "wikilink")（*Lepidocephalichthys kranos*）
  - [羅氏似鱗頭鰍](../Page/羅氏似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    lorentzi*）
  - [曼尼普爾河似鱗頭鰍](../Page/曼尼普爾河似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    manipurensis*）
  - [小鬚似鱗頭鰍](../Page/小鬚似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    micropogon*）
  - [溫泉鱗頭鰍](../Page/溫泉鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    thermalis*）
  - [背紋似鱗頭鰍](../Page/背紋似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    tomaculum*）
  - [澤氏似鱗頭鰍](../Page/澤氏似鱗頭鰍.md "wikilink")（*Lepidocephalichthys
    zeppelini*）

### 鱗頭鰍屬（*Lepidocephalus*）

  - [鱗頭鰍](../Page/鱗頭鰍.md "wikilink")（*Lepidocephalus macrochir*）
  - [印尼鱗頭鰍](../Page/印尼鱗頭鰍.md "wikilink")（*Lepidocephalus spectrum*）

### 微鰍（*Microcobitis*）

  - [越南微鰍](../Page/越南微鰍.md "wikilink")（*Microcobitis misgurnoides*）

### 泥鰍屬（*Misgurnus*）

  - [泥鰍](../Page/泥鰍.md "wikilink")（*Misgurnus anguillicaudatus*）
  - [朝鮮泥鰍](../Page/朝鮮泥鰍.md "wikilink")（*Misgurnus buphoensis*）
  - [縱帶泥鰍](../Page/縱帶泥鰍.md "wikilink")（*Misgurnus fossilis*）
  - [大鱗泥鰍](../Page/大鱗泥鰍.md "wikilink")（*Misgurnus mizolepis*）
  - [黑龍江泥鰍](../Page/黑龍江泥鰍.md "wikilink")（*Misgurnus mohoity*）
  - [俄羅斯泥鰍](../Page/俄羅斯泥鰍.md "wikilink")（*Misgurnus nikolskyi*）
  - [越南泥鰍](../Page/越南泥鰍.md "wikilink")（*Misgurnus tonkinensis*）

### 新真髭鰍屬（*Neoeucirrhichthys*）

  - [梅氏新真髭鰍](../Page/梅氏新真髭鰍.md "wikilink")（*Neoeucirrhichthys maydelli*）

### 後鰭花鰍屬（*Niwaella*）

  - [後鰭花鰍](../Page/後鰭花鰍.md "wikilink")（*Niwaella delicata*）
  - [側斑後鰭花鰍](../Page/側斑後鰭花鰍.md "wikilink")（*Niwaella
    laterimaculata*）：又稱斑條鰍。
  - [長鬚後鰭花鰍](../Page/長鬚後鰭花鰍.md "wikilink")（*Niwaella longibarba*）
  - [多帶後鰭花鰍](../Page/多帶後鰭花鰍.md "wikilink")（*Niwaella multifasciata*）
  - [信江後鰭花鰍](../Page/信江後鰭花鰍.md "wikilink")（*Niwaella xinjiangensis*）

### 潘鰍屬（*Pangio*）

  - [阿格瑪潘鰍](../Page/阿格瑪潘鰍.md "wikilink")（*Pangio agma*）
  - [似雀潘鰍](../Page/似雀潘鰍.md "wikilink")（*Pangio alcoides*）
  - [上眼潘鰍](../Page/上眼潘鰍.md "wikilink")（*Pangio alternans*）
  - (*Pangio ammophila*)
  - [鰻形潘鰍](../Page/鰻形潘鰍.md "wikilink")（*Pangio anguillaris*）
  - [無足潘鰍](../Page/無足潘鰍.md "wikilink")（*Pangio apoda*）
  - [喜暖潘鰍](../Page/喜暖潘鰍.md "wikilink")（*Pangio atactos*）
  - [秀美潘鰍](../Page/秀美潘鰍.md "wikilink")（*Pangio bitaimac*）
  - [加里曼丹潘鰍](../Page/加里曼丹潘鰍.md "wikilink")（*Pangio borneensis*）
  - [肉桂潘鰍](../Page/肉桂潘鰍.md "wikilink")（*Pangio cuneovirgata*）
  - [多里潘鰍](../Page/多里潘鰍.md "wikilink")（*Pangio doriae*）
  - [長身潘鰍](../Page/長身潘鰍.md "wikilink")（*Pangio elongata*）
  - [野潘鰍](../Page/野潘鰍.md "wikilink")（*Pangio filinaris*）
  - [棕色潘鰍](../Page/棕色潘鰍.md "wikilink")（*Pangio fusca*）
  - [印度潘鰍](../Page/印度潘鰍.md "wikilink")（*Pangio goaensis*）
  - [婆羅洲潘鰍](../Page/婆羅洲潘鰍.md "wikilink")（*Pangio incognito*）
  - [庫勒潘鰍](../Page/庫勒潘鰍.md "wikilink")（*Pangio kuhlii*）
  - [利氏潘鰍](../Page/利氏潘鰍.md "wikilink")（*Pangio lidi*）
  - [長翅潘鰍](../Page/長翅潘鰍.md "wikilink")（*Pangio longimanus*）
  - [長鰭潘鰍](../Page/長鰭潘鰍.md "wikilink")（*Pangio longipinnis*）
  - [蚓狀潘鰍](../Page/蚓狀潘鰍.md "wikilink")（*Pangio lumbriciformis*）
  - [馬來亞潘鰍](../Page/馬來亞潘鰍.md "wikilink")（*Pangio malayana*）
  - [瑪利潘鰍](../Page/瑪利潘鰍.md "wikilink")（*Pangio mariarum*）
  - （*Pangio muraeniformis*）
  - [邁爾潘鰍](../Page/邁爾潘鰍.md "wikilink")（*Pangio myersi*）
  - [橢圓潘鰍](../Page/橢圓潘鰍.md "wikilink")（*Pangio oblonga*）
  - [直潘鰍](../Page/直潘鰍.md "wikilink")（*Pangio pangia*）
  - [椒潘鰍](../Page/椒潘鰍.md "wikilink")（*Pangio piperata*）
  - [印尼潘鰍](../Page/印尼潘鰍.md "wikilink")（*Pangio pulla*）
  - [銹色潘鰍](../Page/銹色潘鰍.md "wikilink")（*Pangio robiginosa*）
  - [半帶潘鰍](../Page/半帶潘鰍.md "wikilink")（*Pangio semicincta*）
  - [謝氏潘鰍](../Page/謝氏潘鰍.md "wikilink")（*Pangio shelfordii*）
  - [條尾潘鰍](../Page/條尾潘鰍.md "wikilink")（*Pangio signicauda*）
  - [小眼潘鰍](../Page/小眼潘鰍.md "wikilink")（*Pangio superba*）

### 細頭鰍屬（*Paralepidocephalus*）

  - [圭山細頭鰍](../Page/圭山細頭鰍.md "wikilink")（*Paralepidocephalus
    guishanensis*）
  - [細頭鰍](../Page/細頭鰍.md "wikilink")（*Paralepidocephalus yui*）

### 副泥鰍屬（*Paramisgurnus*）

  - [大鱗副泥鰍](../Page/大鱗副泥鰍.md "wikilink")（*Paramisgurnus dabryanus*）

### 原花鰍屬（*Protocobitis*）

  - [多鱗原花鰍](../Page/多鱗原花鰍.md "wikilink")（*Protocobitis polylepis*）
  - [盲眼原花鰍](../Page/盲眼原花鰍.md "wikilink")（*Protocobitis typhlops*）

### 薩瓦納鰍屬（*Sabanejewia*）

  - [金色薩瓦納鰍](../Page/金色薩瓦納鰍.md "wikilink")（*Sabanejewia aurata aurata*）
  - [巴爾幹半島薩瓦納鰍](../Page/巴爾幹半島薩瓦納鰍.md "wikilink")（*Sabanejewia
    balcanica*）
  - [山溪薩瓦納鰍](../Page/山溪薩瓦納鰍.md "wikilink")（*Sabanejewia baltica*）
  - [保加利亞薩瓦納鰍](../Page/保加利亞薩瓦納鰍.md "wikilink")（*Sabanejewia bulgarica*）
  - [裏海薩瓦納鰍](../Page/裏海薩瓦納鰍.md "wikilink")（''Sabanejewia caspia ''）
  - [高加索薩瓦納鰍](../Page/高加索薩瓦納鰍.md "wikilink")（*Sabanejewia caucasica*）
  - [庫班河薩瓦納鰍](../Page/庫班河薩瓦納鰍.md "wikilink")（*Sabanejewia kubanica*）
  - [幼薩瓦納鰍](../Page/幼薩瓦納鰍.md "wikilink")（*Sabanejewia larvata*）
  - [羅馬尼亞薩瓦納鰍](../Page/羅馬尼亞薩瓦納鰍.md "wikilink")（*Sabanejewia romanica*）
  - [沙溪薩瓦納鰍](../Page/沙溪薩瓦納鰍.md "wikilink")（*Sabanejewia vallachica*）

### 穴鰍屬(*Theriodes*)

  - [山打根穴鰍](../Page/山打根穴鰍.md "wikilink")(*Theriodes sandakanensis*)

## 參考資料

1.  [台灣魚類資料庫](http://fishdb.sinica.edu.tw/AjaxTree/tree.php)

[\*](../Category/鳅科.md "wikilink")