[Imperial_Encyclopaedia_-_Animal_Kingdom_-_pic366.svg](https://zh.wikipedia.org/wiki/File:Imperial_Encyclopaedia_-_Animal_Kingdom_-_pic366.svg "fig:Imperial_Encyclopaedia_-_Animal_Kingdom_-_pic366.svg")
**鳙鱼**（[学名](../Page/学名.md "wikilink")：*Hypophthalmichthys
nobilis*），又叫**花鲢**、**黑鰱**、**大头鰱**、**胖头鱼**、**大头鱼**，為[鰱屬的一](../Page/鰱屬.md "wikilink")[種](../Page/種.md "wikilink")，為[四大家鱼之一](../Page/四大家鱼.md "wikilink")。

## 分布

本魚原產於[中國](../Page/中國.md "wikilink")，並引進世界各地的淡水水域，目前已對部分國家造成生態問題。

## 特徵

本魚體側扁，头部特大，[眼在头的下半部](../Page/眼.md "wikilink")。背部暗黑色，有不规则的深色斑块，腹部灰白色且呈圓滑狀。尾鰭叉形，背鰭硬棘3枚；背鰭軟條7枚；臀鰭硬棘1至3枚；臀鰭軟條12至14枚，體長可達112公分。

## 生態

本魚為初級淡水魚，栖息在江河水庫、湖泊的中上层，屬濾食性，以细密的[鳃耙滤食](../Page/鳃耙.md "wikilink")[浮游动物](../Page/浮游动物.md "wikilink")，性情温和。鳙鱼具有[洄游习性](../Page/洄游.md "wikilink")，平时多生活在有一定流速的水中。生长迅速，最大个体可达40千克。

## 經濟利用

為高經濟性的養殖魚類，[清蒸](../Page/清蒸.md "wikilink")、[紅燒](../Page/紅燒.md "wikilink")、[沙鍋魚頭或煮味增湯皆宜](../Page/沙鍋魚頭.md "wikilink")。

## 参考文献

  -
  - {{ cite book | title = 魚類圖鑑 | publisher = 遠流出版社 | date = 2003年 }}

  -
[HN](../Category/IUCN數據缺乏物種.md "wikilink")
[HN](../Category/食用魚.md "wikilink")
[nobilis](../Category/鲢属.md "wikilink")
[Category:中國魚類](../Category/中國魚類.md "wikilink")