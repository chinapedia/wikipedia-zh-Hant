**鳳山溪**位於[台灣北部](../Page/台灣.md "wikilink")，屬於[中央管河川](../Page/中央管河川.md "wikilink")，總長約45.45公里，流域經過[新竹縣及](../Page/新竹縣.md "wikilink")[桃園市](../Page/桃園市.md "wikilink")，包括有新竹縣[尖石鄉](../Page/尖石鄉.md "wikilink")、[關西鎮](../Page/關西鎮_\(台灣\).md "wikilink")、[新埔鎮](../Page/新埔鎮.md "wikilink")、[橫山鄉](../Page/橫山鄉_\(臺灣\).md "wikilink")、[湖口鄉](../Page/湖口鄉.md "wikilink")、[竹北市及桃園市](../Page/竹北市.md "wikilink")[龍潭區](../Page/龍潭區_\(台灣\).md "wikilink")，是新竹縣僅次於[頭前溪的重要河流](../Page/頭前溪.md "wikilink")，流域面積約250.10平方公里。

鳳山溪發源自[新竹縣](../Page/新竹縣.md "wikilink")[關西鎮](../Page/關西鎮_\(台灣\).md "wikilink")、[尖石鄉與桃園市](../Page/尖石鄉.md "wikilink")[復興區交界處的](../Page/復興區_\(台灣\).md "wikilink")[外鳥嘴山西側](../Page/外鳥嘴山.md "wikilink")\[1\]，於崁子腳附近與南邊之頭前溪匯合注入[台灣海峽](../Page/台灣海峽.md "wikilink")。

## 水系主要河川

  - **鳳山溪**
      - [太平窩溪](../Page/太平窩溪.md "wikilink")：新竹縣[新埔鎮](../Page/新埔鎮.md "wikilink")
          - [燒炭窩溪](../Page/燒炭窩溪.md "wikilink")：
      - [霄裡溪](../Page/霄裡溪.md "wikilink")：新竹縣[新埔鎮](../Page/新埔鎮.md "wikilink")、[關西鎮](../Page/關西鎮_\(台灣\).md "wikilink")、桃園市[龍潭區](../Page/龍潭區_\(台灣\).md "wikilink")
          - [汶水河](../Page/汶水河.md "wikilink")：新竹縣新埔鎮
      - [旱坑溪](../Page/旱坑溪.md "wikilink")：新竹縣新埔鎮、關西鎮
      - [下橫坑溪](../Page/下橫坑溪.md "wikilink")：新竹縣關西鎮
      - [水坑溪](../Page/水坑溪.md "wikilink")：新竹縣關西鎮
      - [牛欄河](../Page/牛欄河.md "wikilink")：新竹縣關西鎮、桃園市龍潭區
          - [拱子溝](../Page/拱子溝.md "wikilink")：新竹縣關西鎮
      - [四寮溪](../Page/四寮溪.md "wikilink")：新竹縣關西鎮
          - [三屯圳](../Page/三屯圳.md "wikilink")：新竹縣關西鎮
      - [新城溪](../Page/新城溪_\(新竹縣\).md "wikilink")：新竹縣關西鎮、[橫山鄉](../Page/橫山鄉_\(臺灣\).md "wikilink")
          - [老社寮溪](../Page/老社寮溪.md "wikilink")：新竹縣關西鎮
      - **[錦山溪](../Page/錦山溪.md "wikilink")**：新竹縣關西鎮
          - [樹橋窩溪](../Page/樹橋窩溪.md "wikilink")
          - [曲窩溪](../Page/曲窩溪.md "wikilink")
          - **[馬武督溪](../Page/馬武督溪.md "wikilink")**

## 主要橋樑

以下由[河口至](../Page/河口.md "wikilink")[源頭列出](../Page/源頭.md "wikilink")[主流上之主要](../Page/主流.md "wikilink")[橋樑](../Page/橋樑.md "wikilink")：

### 鳳山溪河段

  - [鳳山溪橋](../Page/鳳山溪橋_\(台15線\).md "wikilink")（省道[台15線](../Page/台15線.md "wikilink")）
  - [鳳岡大橋](../Page/鳳岡大橋.md "wikilink")（鄉道[竹73線](../Page/竹73線.md "wikilink")）
  - [鳳山溪橋](../Page/鳳山溪橋_\(台1線\).md "wikilink")（省道[台1線](../Page/台1線.md "wikilink")）
  - [台鐵鳳山溪橋](../Page/台鐵鳳山溪橋.md "wikilink")
  - [國道1號鳳山溪橋](../Page/國道1號鳳山溪橋.md "wikilink")
  - [義民橋](../Page/義民橋.md "wikilink")（鄉道[竹14線](../Page/竹14線.md "wikilink")）
  - [褒忠大橋](../Page/褒忠大橋.md "wikilink")（[縣道117號](../Page/縣道117號.md "wikilink")）
  - [高鐵鳳山溪橋](../Page/高鐵鳳山溪橋.md "wikilink")
  - [新埔大橋](../Page/新埔大橋.md "wikilink")（[縣道118號](../Page/縣道118號.md "wikilink")）
  - [寶石橋](../Page/寶石橋.md "wikilink")（[縣道115號](../Page/縣道115號.md "wikilink")）
  - [雲埔橋](../Page/雲埔橋.md "wikilink")
  - [坪林大橋](../Page/坪林大橋.md "wikilink")（鄉道[竹25線](../Page/竹25線.md "wikilink")）
  - [國道3號鳳山溪橋](../Page/國道3號鳳山溪橋.md "wikilink")
  - 無名橋（鄉道[竹16線](../Page/竹16線.md "wikilink")）
  - [南山大橋](../Page/南山大橋.md "wikilink")
  - [渡船頭橋](../Page/渡船頭橋.md "wikilink")（鄉道[竹25線](../Page/竹25線.md "wikilink")）
  - [南華橋](../Page/南華橋.md "wikilink")（省道[台3線](../Page/台3線.md "wikilink")）

### 錦山溪河段

  - [東光橋](../Page/東光橋.md "wikilink")（鄉道[竹30線](../Page/竹30線.md "wikilink")）
  - [八股橋](../Page/八股橋.md "wikilink")
  - [十股橋](../Page/十股橋.md "wikilink")
  - [錦山橋](../Page/錦山橋.md "wikilink")（[縣道118號](../Page/縣道118號.md "wikilink")）
  - [壽桃橋](../Page/壽桃橋.md "wikilink")（阿化窩道路）

### 馬武督溪河段

  - 無名橋
  - [錦發橋](../Page/錦發橋.md "wikilink")（[縣道118號](../Page/縣道118號.md "wikilink")）
  - [東明橋](../Page/東明橋.md "wikilink")
  - 無名橋

## 相關條目

  - [中央管河川](../Page/中央管河川.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")
  - [台灣河流長度列表](../Page/台灣河流長度列表.md "wikilink")

## 外部連結

  - 經濟部水利署[水利櫥窗：讓我們看河去(重要河川)--鳳山溪](https://web.archive.org/web/20080331074609/http://www.wra.gov.tw/ct.asp?xItem=14299&CtNode=4350)

[category:新竹縣河川](../Page/category:新竹縣河川.md "wikilink")

[鳳山溪水系](../Category/鳳山溪水系.md "wikilink")

1.  [《自然地景》，臺灣客庄文化數位典藏](http://archives.hakka.gov.tw/category_detail_list.php?p=13&type=7.1)