**鴕鳥屬**（[學名](../Page/學名.md "wikilink")：），是[鴕鳥目下的一個](../Page/鴕鳥目.md "wikilink")[屬](../Page/属_\(生物\).md "wikilink")。
[_Struthio_camelus_MHNT.ZOO.2010.11.1.1.jpg](https://zh.wikipedia.org/wiki/File:_Struthio_camelus_MHNT.ZOO.2010.11.1.1.jpg "fig:_Struthio_camelus_MHNT.ZOO.2010.11.1.1.jpg")

## 物種

在這個屬之下，有十個已知物種，其中八種已經絕種。There are five more possible species of which
[trace fossils](../Page/trace_fossil.md "wikilink") have been found.
They are:

  - †*[Struthio coppensi](../Page/Struthio_coppensi.md "wikilink")*
    (Early Miocene of Elizabethfeld, Namibia)
  - †[臨夏鴕鳥](../Page/臨夏鴕鳥.md "wikilink")(*Struthio
    linxiaensis*)生活在[中新世](../Page/中新世.md "wikilink")，現已絕種，[化石在中國](../Page/化石.md "wikilink")[臨夏縣發現](../Page/臨夏縣.md "wikilink")\[1\]。
  - †*[Struthio orlovi](../Page/Struthio_orlovi.md "wikilink")* (Late
    Miocene of Moldavia)
  - †*[Struthio
    karingarabensis](../Page/Struthio_karingarabensis.md "wikilink")*
    (Late Miocene - Early Pliocene of SW and CE Africa) -
    [oospecies](../Page/Trace_fossil_classification.md "wikilink")(?)
  - †*[Struthio
    kakesiensis](../Page/Struthio_kakesiensis.md "wikilink")* (Laetolil
    Early Pliocene of Laetoli, Tanzania) -
    [oospecies](../Page/Trace_fossil_classification.md "wikilink")
  - †*[Struthio wimani](../Page/Struthio_wimani.md "wikilink")* (Early
    Pliocene of China and Mongolia)
  - †*[Struthio
    daberasensis](../Page/Struthio_daberasensis.md "wikilink")* (Early -
    Middle Pliocene of Namibia) -
    [oospecies](../Page/Trace_fossil_classification.md "wikilink")
  - †*[Struthio
    brachydactylus](../Page/Struthio_brachydactylus.md "wikilink")*
    (Pliocene of Ukraine)
  - †*[Struthio
    chersonensis](../Page/Struthio_chersonensis.md "wikilink")*
    (Pliocene of SE Europe to WC Asia) -
    [oospecies](../Page/Trace_fossil_classification.md "wikilink")
  - †*[Struthio asiaticus](../Page/Struthio_asiaticus.md "wikilink")*,
    Asian ostrich (Early Pliocene - Late Pleistocene of Central Asia to
    China ?and Morocco)
  - †*[Struthio dmanisensis](../Page/Giant_ostrich.md "wikilink")*,
    giant ostrich (Late Pliocene/Early Pleistocene of
    [Dmanisi](../Page/Dmanisi.md "wikilink"), Georgia)
  - †*[Struthio oldawayi](../Page/Struthio_oldawayi.md "wikilink")*
    (Early Pleistocene of Tanzania) - probably subspecies of *S.
    camelus*
  - †*[Struthio anderssoni](../Page/Struthio_anderssoni.md "wikilink")*
    - in N China/Mongolia\[2\]
    [oospecies](../Page/Trace_fossil_classification.md "wikilink")(?)
  - [鴕鳥](../Page/鴕鳥.md "wikilink")（*Struthio camelus*）一般的鴕鳥
  - [駝雞](../Page/索马利鸵鸟.md "wikilink")（*Struthio molybdophanes*）\[3\]

Fossil records and egg shell fragments show that the ancestors of this
genus originated about 40-58 [million years
ago](../Page/mya_\(unit\).md "wikilink") (mya) in the
[Asiatic](../Page/Asia.md "wikilink")
[steppes](../Page/steppe.md "wikilink") as small flightless birds. The
earliest fossils from this genus are from the early
[Miocene](../Page/Miocene.md "wikilink") (20-25mya), and are from
Africa, so it is proposed that they originated there. Then by the middle
to late [Miocene](../Page/Miocene.md "wikilink") (5-13mya) they had
spread to [Eurasia](../Page/Eurasia.md "wikilink").\[4\] By about 12 mya
they had evolved into the larger size of which we are familiar. By this
time they had spread to [Mongolia](../Page/Mongolia.md "wikilink") and,
later, [South Africa](../Page/South_Africa.md "wikilink").\[5\]

## 參考文獻

[鴕鳥屬](../Category/鴕鳥屬.md "wikilink")

1.  ChinSciBull50:1808
2.  Lisa Janz et al, Dating North Asian surface assemblages with ostrich
    eggshell: implications for palaeoecology and extirpation. Journal of
    Archaeological Science, Volume 36, Issue 9, September 2009, Pages
    1982–1989; also J. G. Andersson, Essays on the cenozoic of northern
    China. In: Memoirs of the Geological Survey of China (Peking),
    Series A, No. 3 (1923), pp. 1-152, especially pp. 53-77: "On the
    occurrence of fossil remains of Struthionidae in China."; and J. G.
    Andersson, Research into the prehistory of the Chinese. Bulletin of
    the Museum of Far Eastern Antiquities 15 (1943), 1-300, plus 200
    plates.
3.  向达校注 《[西洋番国志](../Page/西洋番国志.md "wikilink")》附录二·五《长乐山南山寺天妃之神灵应记》，53页
    中华书局
4.  Hou, L. et al. (2005)
5.  Davies, S.J.J.F. (2003)