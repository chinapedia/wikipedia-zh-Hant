**鵝鑾鼻燈塔**是一座位於臺灣[屏東縣](../Page/屏東縣.md "wikilink")[恆春鎮](../Page/恆春鎮.md "wikilink")[墾丁國家公園](../Page/墾丁國家公園.md "wikilink")[鵝鑾鼻的](../Page/鵝鑾鼻公園.md "wikilink")[燈塔](../Page/灯塔.md "wikilink")。該燈塔一度被認定為台灣最南端的標誌也是照射距離最遠的燈塔，後被[台灣最南點地標取代](../Page/台灣最南點.md "wikilink")。最初落成於1883年，並經多次重修，現今建物為[中華民國政府於](../Page/中華民國.md "wikilink")1962年重修建成。

## 歷史

### 清同治時期

鵝鑾鼻附近的巴士海峽外海有[七星岩](../Page/七星岩_\(屏東\).md "wikilink")[暗礁](../Page/暗礁.md "wikilink")，過去常有不熟悉附近水域的船隻觸礁沈沒。1867年（[清](../Page/清.md "wikilink")[同治六年](../Page/同治.md "wikilink")），一艘美國商船從[汕頭開往](../Page/汕頭.md "wikilink")[牛莊途中](../Page/牛莊.md "wikilink")，在暴風中迷失方向，漂至七星岩附近觸礁沈沒，船長夫婦和船員游泳登岸後，除一[中國船員逃至](../Page/中國.md "wikilink")[打狗外](../Page/打狗.md "wikilink")，其他人均被龜仔角社（[社頂](../Page/社頂.md "wikilink")）的[原住民俘虜殺害](../Page/原住民.md "wikilink")，被稱為[羅發號事件](../Page/羅發號事件.md "wikilink")。另外，1871年時[琉球人也在](../Page/琉球.md "wikilink")[南岬一帶遇難](../Page/南岬.md "wikilink")，被稱為[八瑤灣事件](../Page/八瑤灣事件.md "wikilink")，還因此引發1874年日軍攻台的[牡丹社事件](../Page/牡丹社事件.md "wikilink")。[美國和](../Page/美國.md "wikilink")[日本政府因此皆要求](../Page/日本.md "wikilink")[清朝在此處設燈塔](../Page/清朝.md "wikilink")。

### 清光緒時期

在外交壓力下，清廷於1875年（[清](../Page/清.md "wikilink")[光緒元年](../Page/光緒.md "wikilink")）委託[英國皇家地理學會會員](../Page/皇家地理學會.md "wikilink")[畢齊禮](http://www.tipp.org.tw/researchsourse/word/55299302016.pdf)（[Michael
Beazeley](http://www.tipp.org.tw/researchsourse/word/55299302016.pdf)），於6月18日自打狗出發前往瑯嶠（今[恆春](../Page/恆春鎮.md "wikilink")）地區勘察燈塔建地，並支付一百銀兩向[龜仔角社](../Page/龜仔角社.md "wikilink")（今[墾丁國家森林遊樂區旁](../Page/墾丁國家森林遊樂區.md "wikilink")[社頂部落](../Page/社頂公園.md "wikilink")）[原住民購買燈塔預定用地建塔](../Page/原住民.md "wikilink")。燈塔於1881年（[清](../Page/清.md "wikilink")[光緒](../Page/光緒.md "wikilink")7年）動工，1883年（[清](../Page/清.md "wikilink")[光緒](../Page/光緒.md "wikilink")9年）建成。由於當地是原住民的地盤，因此在興建前後，官方還派了五百兵力守護。
砲壘式的外觀與滿佈槍眼的圍牆，圍牆外還有[壕溝](../Page/壕溝.md "wikilink")，而整個塔區建築屋頂都是「蓄水坪」，雨水沿著水管集流到地面下九座花崗石蓄水池，水源自給自足，這些都是為了防止原住民的侵襲與圍困而建造的設施，也因此鵝鑾鼻燈塔成為全世界獨一無二的武裝燈塔。建造費用總數為白銀20餘萬兩。

[甲午戰爭後](../Page/甲午戰爭.md "wikilink")，1895年（[清](../Page/清.md "wikilink")[光緒](../Page/光緒.md "wikilink")21年）清軍在離台前，恆春[知縣](../Page/知縣.md "wikilink")[歐陽萱奉令秘密焚毀南台灣鵝鑾鼻燈樓](../Page/歐陽萱.md "wikilink")、石牆，以及石路碼頭。燈塔在9月13日摧毀。

### 台灣日治時期

在1898年（[日本](../Page/日本.md "wikilink")[明治](../Page/明治.md "wikilink")31年）[日治時代時燈塔整修完成](../Page/日治時代.md "wikilink")，是[大日本帝國最南端的燈塔](../Page/大日本帝國.md "wikilink")，1904年（[明治](../Page/明治.md "wikilink")37年）燈塔架設電話線，是為恆春地區第一部電話。[二次大戰時燈塔遭](../Page/二次大戰.md "wikilink")[盟軍空襲毀壞](../Page/盟軍.md "wikilink")。當時日本人重建的燈塔，白色的塔身在海拔五十五公尺，高出地面十八公尺，周圍一百公尺是鐵鑄圓柱形，內分四層，頂端有二萬六千燭光的一等白燭光，可照射二十海浬。\[1\]

### 中華民國時期

燈塔於1962年由政府整修後，塔高21.4公尺，塔頂換裝新式大型四等旋轉透鏡電燈，經過大型旋轉透鏡後，光力為180萬-{zh-cn:[坎德拉](../Page/坎德拉.md "wikilink");
zh-tw:[燭光](../Page/燭光.md "wikilink");
zh-hk:[坎德拉](../Page/坎德拉.md "wikilink");
zh-sg:[坎德拉](../Page/坎德拉.md "wikilink")}-，每十秒一閃，照射距離達27.2-{zh-cn:[海里](../Page/海里.md "wikilink");
zh-tw:[海浬](../Page/海浬.md "wikilink");
zh-hk:[海里](../Page/海里.md "wikilink");}-，是目前台灣光力最強的燈塔，被稱為「東亞之光」。

鵝鑾鼻為台灣南端海角，屬於[珊瑚礁](../Page/珊瑚礁.md "wikilink")[石灰岩地形](../Page/石灰岩.md "wikilink")，所以地理景觀上十分奇特，有怪石、巨礁及洞穴，海景更是一絕，被視為「[台灣八景](../Page/台灣八景.md "wikilink")」之一。在1982年成立[鵝鑾鼻公園](../Page/鵝鑾鼻公園.md "wikilink")，以燈塔為主體，規劃出鄰近50[公頃的風景區](../Page/公頃.md "wikilink")。

2017年5月，燈塔主任彭念陸發現燈塔內部鐵身出現鏽蝕，於月底進行啟用以來首次全面除鏽工程，也是燈塔啟用以來134年首次除鏽。

## 燈塔結構與行政諸元

### 燈塔結構諸元

  - 塔高：21.4公尺。
  - 塔身塗色・構造：外觀漆[白](../Page/白色.md "wikilink")，頂層外有環繞[陽台](../Page/陽台.md "wikilink")。
  - 燈高：56.4公尺〈以高潮面至燈火中心計〉。
  - 燈器：大型4等旋轉透鏡電燈。
  - 燈質（頻率）：每10秒白明光1閃。明1.0秒，暗9.0秒。
  - [光力](../Page/發光強度.md "wikilink")：1,800,000支[燭光](../Page/燭光.md "wikilink")。
  - 公稱[光程](../Page/光程.md "wikilink")：27.2[浬](../Page/浬.md "wikilink")。
  - 明弧：234度-099度但在距離該塔15浬內234度-266度為陸地遮蔽，不見燈光。

### 燈塔行政諸元

  - 管轄機關：燈塔原為[中華民國](../Page/中華民國.md "wikilink")[財政部關務署管理](../Page/財政部關務署.md "wikilink")，已於2013年1月1日轉交由中華民國[交通部航港局管轄](../Page/交通部航港局.md "wikilink")。
  - 人員編制：未知。
  - 開放參觀與否：可，但有時間限制。1982年12月起採半開放式：除星期一、[國定假日隔日及](../Page/國定假日.md "wikilink")[三大節日](../Page/三大節日.md "wikilink")（[春節](../Page/春節.md "wikilink")、[端午節](../Page/端午節.md "wikilink")、[中秋節](../Page/中秋節.md "wikilink")）不對外開放外，其他時間上午九時至下午六時（夏令時間4月1日至10月31日），上午九時至下午五時（冬令時間11月1日至3月31日），每逢星期一內部整理不開放。免費自由參觀。
  - 電話：08-8851111。

## 寫真

<File:鵝鑾鼻燈塔> 屏東縣 歷史建築燈塔 Venation 2.JPG|鵝鑾鼻燈塔 <File:Eluanbi> Lighthouse,
Kenting National Park, Pingtung County, Taiwan.jpg|鵝鑾鼻燈塔 <File:Eluanbi>
metal.JPG|台灣八景石碑，其中「鵝鑾鼻」模仿[王羲之筆跡寫成](../Page/王羲之.md "wikilink")「鵞鑾鼻」三字
<File:Eluanbi> Lighthouse 01.jpg|鵝鑾鼻燈塔 <File:Eluanbi> Lighthouse
20060404.png|鵝鑾鼻燈塔側面
[File:Erluanbi-lighthouce-maindoor.jpg|鵝鑾鼻燈塔正門](File:Erluanbi-lighthouce-maindoor.jpg%7C鵝鑾鼻燈塔正門)
<File:Eluanbi> Lighthouse 02.jpg|鵝鑾鼻燈塔
[File:Oluampi.jpg|鵝鑾鼻燈塔](File:Oluampi.jpg%7C鵝鑾鼻燈塔)
[File:鵝鑾鼻燈塔除鏽.jpg|鵝鑾鼻燈塔除鏽](File:鵝鑾鼻燈塔除鏽.jpg%7C鵝鑾鼻燈塔除鏽)
<File:Emblem> of Pingtung County.svg|屏東縣縣徽為鵝鑾鼻燈塔

## 關連項目

  - [台灣燈塔列表](../Page/台灣燈塔列表.md "wikilink")
  - [日本燈塔50選](../Page/日本燈塔50選.md "wikilink")

## 參考文獻

### 引用

### 书籍

  - 李素芳
    編著，《台灣的燈塔》，遠足文化事業，[臺北縣](../Page/新北市.md "wikilink")，2002年12月，pp. 126–131。ISBN
    957-28031-2-3
  - 葉倫會
    著，《海洋領航者－台灣燈塔展》，[高雄市立歷史博物館](../Page/高雄市立歷史博物館.md "wikilink")，[高雄市](../Page/高雄市.md "wikilink")，2000年12月，pp. 71–73。ISBN
    957-02-7455-7
  - [財政部關稅總局編撰](../Page/財政部關稅總局.md "wikilink")，《中華民國海關簡史》，[台北市](../Page/台北市.md "wikilink")，1998年7月。ISBN
    9570048611

## 外部連結

  - [鵝鑾鼻燈塔-財政部關稅總局](http://web.customs.gov.tw/ct.asp?xItem=17559&ctNode=6021)
  - [鵝鑾鼻燈塔](http://wshnt.kuas.edu.tw/sundayasp2/g4/new_page_21.htm)
  - [墾丁國家公園](http://www.ktnp.gov.tw)
  - [臺博館藏屏東獅子鄉排灣族19世紀末相關古文書研究](http://www.tipp.org.tw/researchsourse/word/55299302016.pdf)
    國立臺灣博物館學刊 68(3):11-48, 2015

{{-}}     [分類:台灣地標](../Page/分類:台灣地標.md "wikilink")

[Category:屏東縣建築物](../Category/屏東縣建築物.md "wikilink")
[Category:台灣燈塔](../Category/台灣燈塔.md "wikilink")
[Category:墾丁國家公園](../Category/墾丁國家公園.md "wikilink")
[Category:台灣歷史建築](../Category/台灣歷史建築.md "wikilink")
[Category:屏東縣文化資產](../Category/屏東縣文化資產.md "wikilink")

1.  林衡道口述，楊鴻博整理，《鯤島探源》第六冊，頁1224。