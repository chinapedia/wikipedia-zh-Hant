[Pang_Ching_Court_Volleyball_Court.jpg](https://zh.wikipedia.org/wiki/File:Pang_Ching_Court_Volleyball_Court.jpg "fig:Pang_Ching_Court_Volleyball_Court.jpg")
**鵬程苑**（英語：）是香港[黃大仙區的一個](../Page/黃大仙區.md "wikilink")[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")，共有816個住宅單位。此屋苑原先為[竹園北邨內用作興建中學的預留用地](../Page/竹園北邨.md "wikilink")，其後改作興建公屋。及被，這幢未命名的「第16座」改為居屋出售，並於1991年入伙。

## 屋邨資料

### 樓宇

| 樓宇名稱 | 樓宇類型                             | 落成年份  |
| ---- | -------------------------------- | ----- |
| 鵬程苑  | [Y3型](../Page/Y3型.md "wikilink") | 1991年 |
|      |                                  |       |

Pang Ching Court Children Play Area.jpg|兒童遊樂場 Pang Ching Court Car
Park.jpg|停車場

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/黃大仙站.md" title="wikilink">黃大仙站</a>（步行約10分鐘，或轉乘巴士線<a href="../Page/九龍巴士211線.md" title="wikilink">211</a>）</li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [房委會鵬程苑資料](https://web.archive.org/web/20111117080527/http://www.housingauthority.gov.hk/b5/interactivemap/court/0%2C%2C3-347-12_5489%2C00.html)

[Category:黃大仙](../Category/黃大仙.md "wikilink")
[Category:單幢公營房屋](../Category/單幢公營房屋.md "wikilink")
[Category:竹園](../Category/竹園.md "wikilink")