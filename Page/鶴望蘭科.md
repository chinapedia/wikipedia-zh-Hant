**鹤望兰科**（[學名](../Page/學名.md "wikilink")：）又稱**旅人蕉科**、**天堂鸟科**，為[單子葉植物](../Page/單子葉植物.md "wikilink")[薑目的一科](../Page/薑目.md "wikilink")，共有3屬7種，原產於[南美洲熱帶](../Page/南美洲.md "wikilink")、[非洲東南部及](../Page/非洲.md "wikilink")[馬達加斯加](../Page/馬達加斯加.md "wikilink")。[鹤望兰](../Page/鹤望兰.md "wikilink")（）是本科的[模式种](../Page/模式种.md "wikilink")，原產於[南非洲](../Page/南非洲.md "wikilink")，現在己廣泛種植於世界各地的熱帶與亞熱帶花園。

## 分类

本科3属7种如下：

  - [漁人蕉屬](../Page/漁人蕉屬.md "wikilink") *Phenakospermum*
      - [渔人蕉](../Page/渔人蕉.md "wikilink") *Phenakospermum guyannense*
  - [旅人蕉屬](../Page/旅人蕉屬.md "wikilink") *Ravenala*
      - [旅人蕉](../Page/旅人蕉.md "wikilink") *Ravenala madagascariensis*
  - [鹤望兰屬](../Page/鹤望兰屬.md "wikilink") *Strelitzia*
      - [扇芭蕉](../Page/扇芭蕉.md "wikilink") *Strelitzia alba*
      - [山鹤望兰](../Page/山鹤望兰.md "wikilink") *Strelitzia caudata*
      - [棒叶鹤望兰](../Page/棒叶鹤望兰.md "wikilink") *Strelitzia juncea*
      - [白鳥蕉](../Page/白鳥蕉.md "wikilink") *Strelitzia nicolai*：又名大鹤望兰
      - [鹤望兰](../Page/鹤望兰.md "wikilink") *Strelitzia reginae*

## 参考文献

[\*](../Category/鹤望兰科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")