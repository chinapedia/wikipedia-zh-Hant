俗稱海鷗，是最為普遍的海鳥，在鸟类传统分类系统中是鸟纲鸻形目的一个科。鷗與燕鷗關係密切，而涉禽類、海雀及剪嘴鷗則是其遠房親戚。大部份的鷗均是屬於鷗屬。

牠們是中型至大型的[鳥類](../Page/鳥綱.md "wikilink")，其外表為灰色或白色，通常其頭部或[翅膀有著黑色的標記](../Page/翅膀.md "wikilink")。其有著矮腿、長[喙及](../Page/喙.md "wikilink")[蹼](../Page/蹼.md "wikilink")。

大部份鷗，特別是屬於鷗屬的[物種](../Page/物種.md "wikilink")，是[雜食性動物](../Page/雜食性.md "wikilink")。其會食用有生命的食物或腐肉，有生命的食物包括[蟹及小魚](../Page/蟹.md "wikilink")。

除了[三趾鷗外](../Page/三趾鷗.md "wikilink")，鷗通常是沿海或內地的物種，極少數會冒險在[大洋上生活](../Page/海洋.md "wikilink")。大型鷗四歲時成年，而小型鷗則在兩歲時成年。

鷗，特別是大型鷗，是富於謀略及擁有高度智慧的鳥類，這可以由其複雜的溝通方法及高度開發的社會結構看出，而部份物種如[銀鷗更有著使用工具的習性](../Page/銀鷗.md "wikilink")。很多物種的鷗已學會與[人類共存的方法](../Page/人類.md "wikilink")，並能在人類居住的地方繁衍。

## 鷗分類

鷗愛好者通常將鷗分為兩類：

  - **大白頭鷗**，指銀鷗或相類的物種。
  - **白翼鷗**，指在兩極繁衍的物種，即[冰島鷗及](../Page/冰島鷗.md "wikilink")[北極鷗](../Page/北極鷗.md "wikilink")。

不同物種的鷗間的雜交情況很常出現，詳見[鷗雜交](../Page/鷗雜交.md "wikilink")。

大白頭鷗的分類通常更為複雜。

## 分类

[Silver_gull_at_Sydney.jpg](https://zh.wikipedia.org/wiki/File:Silver_gull_at_Sydney.jpg "fig:Silver_gull_at_Sydney.jpg")
[Seagull_on_sale_pier.jpg](https://zh.wikipedia.org/wiki/File:Seagull_on_sale_pier.jpg "fig:Seagull_on_sale_pier.jpg")
[老虎滩_海鸥_13.JPG](https://zh.wikipedia.org/wiki/File:老虎滩_海鸥_13.JPG "fig:老虎滩_海鸥_13.JPG")

[美國鳥類學家聯合會將](../Page/美國鳥類學家聯合會.md "wikilink")[燕鷗科](../Page/燕鷗科.md "wikilink")、[賊鷗科及](../Page/賊鷗科.md "wikilink")[剪嘴鷗科歸類為鷗科的亞科](../Page/剪嘴鷗科.md "wikilink")。

## 鷗剪影

<File:Western> Gull.jpg|[西方鷗](../Page/西方鷗.md "wikilink")
<File:herring.gull.northdevon.arp.750pix.jpg>|[銀鷗](../Page/銀鷗.md "wikilink")
<File:Seagull04.jpg>|[銀色鷗](../Page/銀色鷗.md "wikilink")
[File:Seagull03.jpg|銀色鷗](File:Seagull03.jpg%7C銀色鷗)
[File:Seagull02.jpg|銀色鷗](File:Seagull02.jpg%7C銀色鷗) <File:Larus>
dominicanus.jpg|[黑背鷗](../Page/黑背鷗.md "wikilink") <File:Mewa> siodlata
2.jpg|[大黑脊鷗](../Page/大黑脊鷗.md "wikilink") <File:Red> legged
seagulls.jpg|銀色鷗 <File:Seagull> on sale pier02.jpg|銀色鷗 <File:Seagull>
with bread in mouth.jpg|銀色鷗 <File:Yellow> legged seagull.jpg|黃腳銀鷗
[File:Seagull_lakes_entrance.jpg|銀鷗](File:Seagull_lakes_entrance.jpg%7C銀鷗)
<File:Larus> portrait.jpg|黃腳銀鷗 <File:Heermann's> Gull breeding
adult.jpg|Heermann's Gull <File:Red> billed gull-02.jpg|新西兰红嘴鸥
<File:Head> of seagull on Granville Island.jpg

[Category:鸻形目](../Category/鸻形目.md "wikilink")
[\*](../Category/鸥科.md "wikilink")