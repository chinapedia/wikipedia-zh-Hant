[雞尾酒的配方經常有變化](../Page/雞尾酒.md "wikilink")，每年都有新的配方出現，如果新品種受到廣泛歡迎，就會保留下來，否則就要被淘汰，每個地方的調酒師都有自己拿手的配方品種，調酒師有時也經常向外地客人請教，以豐富自己的品種菜單。

## [國際調酒師協會指定比賽雞尾酒](../Page/國際調酒師協會官方雞尾酒列表.md "wikilink")

  - [阿美利加諾](../Page/阿美利加諾.md "wikilink")
  - [畢和畢](../Page/畢和畢.md "wikilink")（B\&B）
  - [貝利尼](../Page/貝利尼\(雞尾酒\).md "wikilink")（Bellini）
  - [血腥瑪麗](../Page/血瑪麗.md "wikilink")
  - [布朗克斯](../Page/Bronx_\(cocktail\).md "wikilink")（Bronx）
  - [德思威他](../Page/德思威他.md "wikilink")（Dolce Vita）
  - [干马天尼](../Page/干马天尼.md "wikilink")（Dry Martini）
  - [干冽玫瑰](../Page/干冽玫瑰.md "wikilink")（Dry Rose）
  - [佛妙3](../Page/佛妙3.md "wikilink")(Formule 3)
  - [金菲士](../Page/金菲士.md "wikilink")（Gin Fizz）
  - [金合義](../Page/金合義.md "wikilink")（Gin and it）
  - [完美馬丁尼](../Page/完美馬丁尼.md "wikilink")（Medium or perfect Martini）
  - [甜馬丁尼](../Page/甜馬丁尼.md "wikilink")（Sweet Martini）
  - [尼格龍尼](../Page/尼格龍尼.md "wikilink")（Negroni）
  - [古典雞尾酒](../Page/古典雞尾酒.md "wikilink")（Old fashioned）
  - [波菲利普](../Page/波菲利普.md "wikilink")（Porto Flip）
  - [羅伯洛伊](../Page/羅伯洛伊.md "wikilink")（Rob Roy）
  - [蘇格蘭酸](../Page/蘇格蘭酸.md "wikilink")（Scotch sour）
  - [螺絲刀](../Page/螺丝刀_\(鸡尾酒\).md "wikilink")
  - [痛擊](../Page/痛擊.md "wikilink")（Stinger）
  - [湯姆可林](../Page/湯姆可林.md "wikilink")（Tom Collins）
  - [伏特加丁尼](../Page/伏特加丁尼.md "wikilink")（Vodkatini）

## [亞歷山大類](../Page/亞歷山大類.md "wikilink")

  - [亞歷山大](../Page/亞歷山大_\(雞尾酒\).md "wikilink")（Alexander）
  - [咖啡亞歷山大](../Page/咖啡亞歷山大.md "wikilink")（Coffee Alexander）
  - [金酒亞歷山大](../Page/金酒亞歷山大.md "wikilink")（Gin Alexander）

## [霸克類](../Page/霸克類.md "wikilink")

  - [金霸克](../Page/金霸克.md "wikilink")（Gin Buck）
  - [白蘭地霸克](../Page/白蘭地霸克.md "wikilink")（Brandy Buck）

## [可林類](../Page/可林類.md "wikilink")

  - [蘭姆可林](../Page/蘭姆可林.md "wikilink")（Rum Collins）
  - [波本可林](../Page/波本可林.md "wikilink")（Bourbon Collins）

## [考比勒類](../Page/考比勒類.md "wikilink")

  - [白蘭地考比勒](../Page/白蘭地考比勒.md "wikilink")（Brandy Cobbler）
  - [香槟考比勒](../Page/香槟考比勒.md "wikilink")（Champagne Cabbler）
  - [威士忌考比勒](../Page/威士忌考比勒.md "wikilink")（Whisky Cabbler）

## [福贊類](../Page/福贊類.md "wikilink")（Frozen）

  - [冰冻黛克蕾](../Page/冰冻黛克蕾.md "wikilink")（Frozen Daiquiri）

## [菲麗普類](../Page/菲麗普類.md "wikilink")

  - [白蘭地菲麗普](../Page/白蘭地菲麗普.md "wikilink")（Brandy Flip）
  - [雪梨菲麗普](../Page/雪梨菲麗普.md "wikilink")（Sherry Flip）
  - [味美思菲麗普](../Page/味美思菲麗普.md "wikilink")（Vermouth Flip）

## [戴茲類](../Page/戴茲類.md "wikilink")

  - [馨馨戴茲](../Page/馨馨戴茲.md "wikilink")（Ching Ching Daisy）
  - [金戴茲](../Page/金戴茲.md "wikilink")（Gin Daisy）
  - [威士忌戴茲](../Page/威士忌戴茲.md "wikilink")（Whisky Daisy）

## [蛋諾類](../Page/蛋諾類.md "wikilink")

  - [白蘭地蛋諾](../Page/白蘭地蛋諾.md "wikilink")（Brandy Eggnog）
  - [覆盆子蛋諾](../Page/覆盆子蛋諾.md "wikilink")（Eggnog Framboise）
  - [波旁蛋諾](../Page/波旁蛋諾.md "wikilink")（Bourbon Eggnog）

## [賓治類](../Page/賓治類.md "wikilink")

  - [拓荒者賓治](../Page/拓荒者賓治.md "wikilink")（Plant's Punch）
  - [蘋果白蘭地賓治](../Page/蘋果白蘭地賓治.md "wikilink")（Applejack Punch）
  - [茶賓治](../Page/茶賓治.md "wikilink")（Tea Punch）
  - [威士忌賓治](../Page/威士忌賓治.md "wikilink")（Whisky Punch）

## [雞尾類](../Page/雞尾類.md "wikilink")

  - [吉布森](../Page/吉布森.md "wikilink")（Gibson）

  - [蚱蜢](../Page/蚱蜢_\(雞尾酒\).md "wikilink")（Grasshopper）

  - [日出特吉拉](../Page/日出特吉拉.md "wikilink")（Taquila Sunrise）

  - [傑克玫瑰](../Page/傑克玫瑰.md "wikilink")（Jack Rose）

  - [天堂](../Page/天堂_\(雞尾酒\).md "wikilink")（Paradise）

  - [粉紅佳人](../Page/粉紅佳人_\(雞尾酒\).md "wikilink")（Pink Lady）

  - [黑天鵝絨](../Page/黑天鵝絨.md "wikilink")（Black Velvet）

  - （Blue Lagoon）

  - [白俄](../Page/白俄_\(雞尾酒\).md "wikilink")（White Russian）

  - （Black Russian）

  - （Moscow Mule）

  - [瑪格麗塔](../Page/瑪格麗塔.md "wikilink")（Margarita）

  - [反舌鳥](../Page/反舌鳥_\(雞尾酒\).md "wikilink")

## [德貴麗類](../Page/德貴麗類.md "wikilink")

  - [德貴麗](../Page/德貴麗.md "wikilink")（Daiquiri）
  - [菠蘿德貴麗](../Page/菠蘿德貴麗.md "wikilink")（Pineapple Daiquiri）
  - [桃德貴麗](../Page/桃德貴麗.md "wikilink")（Peach Daiquiri）

## 外部連結

[國際調酒師協會](http://iba-world.com/)

[\*](../Category/雞尾酒.md "wikilink")
[Category:飲食相關列表](../Category/飲食相關列表.md "wikilink")