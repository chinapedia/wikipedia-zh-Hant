**鸡鸣驿**，又名**鸡鸣山驿**，是位于[河北省](../Page/河北省.md "wikilink")[张家口市](../Page/张家口市.md "wikilink")[怀来县鸡鸣驿乡的一个](../Page/怀来县.md "wikilink")[驿站](../Page/驿站.md "wikilink")，始建于[明朝初期](../Page/明朝.md "wikilink")（一说始建于[辽](../Page/辽朝.md "wikilink")[金而成于](../Page/金朝.md "wikilink")[元代](../Page/元朝.md "wikilink")），是中国现存的最大的驿站。\[1\]2001年入选第五批全国重点文物保护单位。

[八国联军进北京后](../Page/八国联军侵华战争.md "wikilink")，[慈禧太后连夜](../Page/慈禧太后.md "wikilink")“西狩”，第一夜就住在鸡鸣驿城内的贺家大院内。后来慈禧归京后，专门赐给贺家“鸿喜接福”四个字。目前城内残存有大量明清时期的古建筑，但由于年久失修，大多破败不堪，如果不加以保护维修，很快鸡鸣驿就只是徒有城墙了。2004年，2006年两度被世界文化遗产基金会列入世界百处濒危文物\[2\]
\[3\]

传言电影《[大话西游](../Page/大话西游.md "wikilink")》的结尾片段在鸡鸣驿的西门城楼上拍摄，这其实是个误传。
《[大话西游](../Page/大话西游.md "wikilink")》的结尾片段是在宁夏的[镇北堡西部影视城拍摄](../Page/镇北堡西部影城.md "wikilink")。
\[4\]

## 参考资料及注释

[Category:张家口文物保护单位](../Category/张家口文物保护单位.md "wikilink")
[Category:張家口历史](../Category/張家口历史.md "wikilink")
[Category:張家口地理](../Category/張家口地理.md "wikilink")
[Category:明朝交通](../Category/明朝交通.md "wikilink")
[Category:清朝交通建筑物](../Category/清朝交通建筑物.md "wikilink")
[Category:怀来县](../Category/怀来县.md "wikilink")
[Category:中国驿站](../Category/中国驿站.md "wikilink")
[Category:明朝建築](../Category/明朝建築.md "wikilink")
[Category:河北清代建築](../Category/河北清代建築.md "wikilink")
[冀](../Category/中国历史文化名村.md "wikilink")
[Category:中国建筑之最](../Category/中国建筑之最.md "wikilink")

1.  [全国最大驿城](http://www.sach.gov.cn/tabid/299/InfoID/18286/Default.aspx)
    。
2.  。
3.  。
4.  [1](http://ido.3mt.com.cn/Article/201105/show2202874c33p1.html)