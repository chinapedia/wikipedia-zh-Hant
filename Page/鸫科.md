**鸫科**（学名：*Turdidae*），在[鸟类传统分类系统中是](../Page/鸟类传统分类系统.md "wikilink")[雀形目的一个](../Page/雀形目.md "wikilink")[科](../Page/科.md "wikilink")，而[鳥類DNA分類系統則为雀形目中的](../Page/鳥類DNA分類系統.md "wikilink")[鹟科的一个](../Page/鹟科.md "wikilink")[亚科](../Page/亚科.md "wikilink")，在此以鸟类传统分类系统為主。

大致分成鴝鳥和較大體形的鶇鳥等，總稱「鶇」；嘴细长而侧扁，翅膀长，善于飞翔，叫得很好听，鸫科的中小型雀形鸟（thrush），包括多种出色的鸣禽，专食水果、蠕虫或昆虫的幼体
。

## 分類

  - [鶇屬](../Page/鶇屬.md "wikilink") *Turdus*
  - [地鶇屬](../Page/地鶇屬.md "wikilink") *Zoothera*
  - [磯鶇屬](../Page/磯鶇屬.md "wikilink") *Monticola*
  - [鴝屬](../Page/鴝屬.md "wikilink") *Tarsiger*
  - <s>[水鴝屬](../Page/水鴝屬.md "wikilink") *Rhyacornis*</s>
    （併入[紅尾鴝屬](../Page/紅尾鴝屬.md "wikilink")）
  - [水鴝屬](../Page/水鴝屬.md "wikilink") *Rhyacornis*
  - [溪鴝屬](../Page/溪鴝屬.md "wikilink") *Chaimarrornis*
  - [歌鴝屬](../Page/歌鴝屬.md "wikilink") *Luscinia*
  - [鵲鴝屬](../Page/鵲鴝屬.md "wikilink") *Copsychus*
  - [石{{僻字](../Page/石䳭屬.md "wikilink") *Saxicola*\[1\]
  - [燕尾屬](../Page/燕尾屬.md "wikilink") *Enivurus*
  - [紫嘯鶇屬](../Page/紫嘯鶇屬.md "wikilink") *Myiophonus*

## 註釋

## 參考資料

<div class="references-small">

<references />

  - [基于核基因c-mos的鸫亚科部分鸟类系统发生关系](http://www.ceps.com.tw/ec/ecjnlarticleView.aspx?atliid=572014&issueiid=40764&jnliid=3382)

</div>

[鸫科](../Category/鸫科.md "wikilink")

1.  在[鳥類DNA分類系統歸於](../Page/鳥類DNA分類系統.md "wikilink")[鹟亚科](../Page/鹟亚科.md "wikilink")。