**鴨下
一郎**，出生於[東京都](../Page/東京都.md "wikilink")[足立區](../Page/足立區.md "wikilink")。[日本](../Page/日本.md "wikilink")[政治家](../Page/政治家.md "wikilink")、[醫生](../Page/醫生.md "wikilink")，[自由民主黨所屬的](../Page/自由民主黨_\(日本\).md "wikilink")[眾議院](../Page/眾議院_\(日本\).md "wikilink")[議員](../Page/日本眾議院議員.md "wikilink")。於2007年至2008年擔任第9、10任[環境大臣](../Page/環境大臣.md "wikilink")。

[Category:日本醫生](../Category/日本醫生.md "wikilink")
[Category:二戰後日本政治人物](../Category/二戰後日本政治人物.md "wikilink")
[Category:日本環境大臣](../Category/日本環境大臣.md "wikilink")
[Category:第一次安倍內閣閣僚](../Category/第一次安倍內閣閣僚.md "wikilink")
[Category:福田康夫內閣閣僚](../Category/福田康夫內閣閣僚.md "wikilink")
[Category:日本自由民主黨國會對策委員長](../Category/日本自由民主黨國會對策委員長.md "wikilink")
[Category:日本新黨黨員](../Category/日本新黨黨員.md "wikilink")
[Category:新進黨黨員](../Category/新進黨黨員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本大學校友](../Category/日本大學校友.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:日本眾議院議員
2003–2005](../Category/日本眾議院議員_2003–2005.md "wikilink")
[Category:日本眾議院議員
2005–2009](../Category/日本眾議院議員_2005–2009.md "wikilink")
[Category:日本眾議院議員
2009–2012](../Category/日本眾議院議員_2009–2012.md "wikilink")
[Category:日本眾議院議員
2012–2014](../Category/日本眾議院議員_2012–2014.md "wikilink")
[Category:日本眾議院議員
2014–2017](../Category/日本眾議院議員_2014–2017.md "wikilink")
[Category:日本眾議院議員 2017–](../Category/日本眾議院議員_2017–.md "wikilink")
[Category:東京都選出日本眾議院議員](../Category/東京都選出日本眾議院議員.md "wikilink")
[Category:東京比例代表區選出日本眾議院議員](../Category/東京比例代表區選出日本眾議院議員.md "wikilink")