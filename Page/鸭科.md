**鸭科**（[学名](../Page/学名.md "wikilink")：）在[鸟纲](../Page/鸟纲.md "wikilink")[雁形目的一个](../Page/雁形目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，有43个[属](../Page/属.md "wikilink")，包括[鴨](../Page/鴨.md "wikilink")、[雁](../Page/雁.md "wikilink")、[天鵝等常見鳥類](../Page/天鵝.md "wikilink")。鴨科動物都能游泳和飛。有明顯的頸部。

## 分类

### 属

本科所包含的部分属如下：

  - [鸳鸯属](../Page/鸳鸯属.md "wikilink")
  - [埃及雁属](../Page/埃及雁属.md "wikilink")
  - [鸭属](../Page/鸭属.md "wikilink")
  - [雁属](../Page/雁属.md "wikilink")
  - [潜鸭属](../Page/潜鸭属.md "wikilink")
  - [黑雁属](../Page/黑雁属.md "wikilink")
  - [鹊鸭属](../Page/鹊鸭属.md "wikilink")
  - [长尾鸭属](../Page/长尾鸭属.md "wikilink")
  - [天鹅属](../Page/天鹅属.md "wikilink")
  - [树鸭属](../Page/树鸭属.md "wikilink")
  - [丑鸭属](../Page/丑鸭属.md "wikilink")
  - [海番鸭属](../Page/海番鸭属.md "wikilink")
  - [秋沙鸭属](../Page/秋沙鸭属.md "wikilink")
  - [狭嘴潜鸭属](../Page/狭嘴潜鸭属.md "wikilink")
  - [棉凫属](../Page/棉凫属.md "wikilink")
  - [硬尾鸭属](../Page/硬尾鸭属.md "wikilink")
  - [小绒鸭属](../Page/小绒鸭属.md "wikilink")
  - [瘤鸭属](../Page/瘤鸭属.md "wikilink")
  - [麻鸭属](../Page/麻鸭属.md "wikilink")

## 参考文献

[\*](../Category/鸭科.md "wikilink")
[Category:雁形目](../Category/雁形目.md "wikilink")