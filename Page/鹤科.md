**鹤科**（[學名](../Page/學名.md "wikilink")：）在[动物分类学上是](../Page/动物分类学.md "wikilink")[鸟纲](../Page/鸟纲.md "wikilink")[鹤形目的一个](../Page/鹤形目.md "wikilink")[科](../Page/科.md "wikilink")，包括四個[属及十五個](../Page/属.md "wikilink")[物種](../Page/物種.md "wikilink")。鶴是鳥類中體型較大（全長
80 至 175
公分）也較著名的家族，大多分布於[歐亞大陸與](../Page/歐亞大陸.md "wikilink")[非洲大陸](../Page/非洲大陸.md "wikilink")，少數幾種則分布於[澳洲及](../Page/澳洲.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。

## 描述

[Crane_(5880572945).jpg](https://zh.wikipedia.org/wiki/File:Crane_\(5880572945\).jpg "fig:Crane_(5880572945).jpg")\]\]
[Demoiselle_Crane_(Anthropoides_virgo)_(3).jpg](https://zh.wikipedia.org/wiki/File:Demoiselle_Crane_\(Anthropoides_virgo\)_\(3\).jpg "fig:Demoiselle_Crane_(Anthropoides_virgo)_(3).jpg")的頭部特寫\]\]
[SarusTrachea.jpg](https://zh.wikipedia.org/wiki/File:SarusTrachea.jpg "fig:SarusTrachea.jpg")的氣管構造\]\]
腳與頸部都很細長，與[鷺類似](../Page/鷺.md "wikilink")，但不同的是，就算以最大型的鷺科鳥類——[巨鷺與鶴作比較](../Page/巨鷺.md "wikilink")，至少有約一半的種類體型較巨鷺大（以翼展的最大長度進行比較）；且鷺在飛行時通常會將頸部呈
S 形彎曲，而鶴則常會將頸部伸直。

所有鶴當中體型最小者為[簑羽鶴](../Page/簑羽鶴.md "wikilink")（全長平均約 90
公分），最大型者屬[赤頸鶴](../Page/赤頸鶴.md "wikilink")（身長最長約
156 公分；最高約 175 公分），體重最重的是[丹頂鶴](../Page/丹頂鶴.md "wikilink")（最重將近 12
公斤）；通常雄鳥的體型會比雌鳥大，兩者的羽色一般來說不會有太大的差異\[1\]，可能沒有或有極為不明顯的[兩性異形](../Page/兩性異形.md "wikilink")。

羽色常會因棲地而有所變化，通常棲息在開闊濕地或草原地區的種類多少身上都有些白色羽毛；而棲息於小型濕地或森林中的種類通常體羽多呈淺灰色，可能是因為掠食者在光線較弱的環境下較不容易看清楚較暗色的體羽，有助於遮蔽及隱藏。類似的方式也見於少數棲息於開放環境的種類，在繁殖季節，[灰鶴與](../Page/灰鶴.md "wikilink")[沙丘鶴為了不被掠食者發現其巢](../Page/沙丘鶴.md "wikilink")，於築巢期間會用泥巴或腐草塗抹自己的羽毛，藉此融入環境。另一種有關羽色的說法是：分布於較北方的種類體羽較偏白，分布於較南方者則較偏暗色。

大多數的鶴臉部都有裸露的皮膚（[藍鶴與](../Page/藍鶴.md "wikilink")[簑羽鶴則無](../Page/簑羽鶴.md "wikilink")），據信這能作為與其他同類溝通的一種工具，透過臉部肌肉的收縮傳達訊息。同樣也可能具有溝通的方式，[藍鶴](../Page/藍鶴.md "wikilink")、[簑羽鶴與](../Page/簑羽鶴.md "wikilink")[肉垂鶴能將頭部某個特定部分的羽毛豎起或進行移動](../Page/肉垂鶴.md "wikilink")，被認為可能是訊息傳遞的一種方式。

鶴的腳趾和其他鶴形目的鳥類一樣屬於[不等趾型](../Page/趾型#不等趾型.md "wikilink")，即三趾向前而一趾向後的趾型，[冠鶴屬鳥類的後趾較長](../Page/冠鶴屬.md "wikilink")，能與前三指對握而具有抓握能力，因此能停棲於樹上；而[鶴屬鳥類則較短且位置較高](../Page/鶴屬.md "wikilink")，無法抓握，而不能像冠鶴一樣停棲於樹的枝幹上\[2\]。

值得一提的是，大部分的鶴發音器官——[氣管都很長](../Page/氣管.md "wikilink")（[冠鶴屬鳥類氣管較短](../Page/冠鶴屬.md "wikilink")），最長可達
150 公分（約 5
英尺），在[胸骨內形成捲曲形](../Page/胸骨.md "wikilink")，構造類似於[長號或](../Page/長號.md "wikilink")[法國號等](../Page/法國號.md "wikilink")[管樂器](../Page/管樂器.md "wikilink")，這有助於鶴的發聲，也因此其叫聲可傳到數公里外\[3\]\[4\]\[5\]。多數的鶴聲音都很響亮，通常鶴會鳴叫的情況有：平時與同類傳達訊息時或是起飛前都會鳴叫，然而最令人印象深刻的莫過於繁殖期時的叫聲，此時鶴的叫聲最宏亮，如[丹頂鶴會一邊進行類似舞蹈的動作](../Page/丹頂鶴.md "wikilink")，一邊發出叫聲。

## 分布

[東亞地區是鶴分布範圍的中心](../Page/東亞地區.md "wikilink")，超過一半的種類（已知有八種）分布於此地區；其次是[非洲](../Page/非洲.md "wikilink")，除了僅分布於此大陸的四種外，另有兩種（灰鶴和簑羽鶴）在此地度冬；再來是[北美與](../Page/北美.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")，兩地區各有兩種；而其他的分布範圍大多都只有一種，早期鶴也分布於[不列顛群島](../Page/不列顛群島.md "wikilink")，但已在十七世紀左右滅絕。[灰鶴是分布最為廣泛的鶴](../Page/灰鶴.md "wikilink")，分布範圍橫跨歐亞大陸，非洲也有其分布。

## 棲地

[濕地是鶴主要的棲地](../Page/濕地.md "wikilink")（其他棲地包括[沼澤](../Page/沼澤.md "wikilink")、[凍原或](../Page/凍原.md "wikilink")[稀樹草原](../Page/稀樹草原.md "wikilink")），尤其是面積較大也較開闊的濕地較可能有鶴棲息。除了是棲地外，濕地也是許多鶴築巢的地點，大部分種類的幼鳥主要都在濕地活動，而有些種類在幼鳥孵化且已具備行走能力時，親鳥便會帶著幼鳥至草原覓食，晚上則返回巢。而如藍鶴、簑羽鶴或冠鶴這類生活在較乾燥地區的種類的巢位則多選在草原、草甸上或半沙漠地區。

## 習性

[Grus_grus_Hula_ground_2.jpg](https://zh.wikipedia.org/wiki/File:Grus_grus_Hula_ground_2.jpg "fig:Grus_grus_Hula_ground_2.jpg")的度冬的[灰鶴群](../Page/灰鶴.md "wikilink")\]\]
[Sandhill_at_Sunrise_with_Eggs_-_Flickr_-_Andrea_Westmoreland.jpg](https://zh.wikipedia.org/wiki/File:Sandhill_at_Sunrise_with_Eggs_-_Flickr_-_Andrea_Westmoreland.jpg "fig:Sandhill_at_Sunrise_with_Eggs_-_Flickr_-_Andrea_Westmoreland.jpg")
鶴屬於[日行性的生物](../Page/日行性.md "wikilink")，也是典型的[群居性鳥類](../Page/群居動物.md "wikilink")，在繁殖季節以外的時期常見幾隻或一群聚在一起棲息或覓食，主要是因為群聚活動較不易遭受掠食動物的攻擊。相反的，由於在繁殖期時具有很強的領域性，大部分的鶴主要都在監看自己的領地，大多都只見單隻或一對出現，較沒有群聚的現象。

### 遷徙

鶴會隨著季節而作[遷徙](../Page/鳥類遷徙.md "wikilink")，飛行至最少相隔一千多公里遠的地區度冬或繁殖，當中[白鶴是遷徙距離最長的鶴](../Page/白鶴.md "wikilink")，距離最長甚至超過
8,000 公里；像[雁一樣](../Page/雁.md "wikilink")，鶴群在飛行時也會排成「人」字形，通常飛行的高度約在 2,000
公尺左右，罕見的情況下高度可高達 10,000 公尺，以一般鶴的飛行速度而言，飛行時速 60 至 80 公里，而一天內可飛行約 300
公里，若速度快可達 800
公里。然而不是所有的鶴都具有這種習性，如[冠鶴](../Page/冠鶴屬.md "wikilink")、[藍鶴](../Page/藍鶴.md "wikilink")、[肉垂鶴](../Page/肉垂鶴.md "wikilink")、[澳洲鶴以及一部份的](../Page/澳洲鶴.md "wikilink")[美洲鶴與](../Page/美洲鶴.md "wikilink")[丹頂鶴族群屬於留棲性](../Page/丹頂鶴.md "wikilink")（[留鳥](../Page/留鳥.md "wikilink")），通常會一直待在同一地區，或是僅偶爾作短距離的移動\[6\]。

### 繁殖

求偶是鶴較著名的行為之一，在繁殖期常可見多對鶴出現類似「舞蹈」的習性，動作極為多樣化，有時會因種類而有所不同，動作包括上下擺動頭部、彎腰俯首、旋轉身體、展翅、跳躍、撲翅等，而交配行為常伴隨著「舞蹈」動作發生；除了成鳥外，這種行為也常見於尚未繁殖的亞成鳥\[7\]。

屬於[一夫一妻制的鳥類](../Page/一夫一妻制.md "wikilink")。一年僅繁殖一次，通常產兩顆卵，孵化期 27 至 40
天。巢主要是以枯枝葉或苔蘚築成的平台\[8\]。

此外，此時的鶴為了保衛領地，有極強的攻擊性，多是在半空中朝對方揮動腳爪，或是朝對方撲去。

## 分类

现存的鹤科生物可以分为4属15种。\[9\]2010年发表的一项[分子系统发生学研究表明](../Page/分子系统发生学.md "wikilink")，[鹤属是](../Page/鹤属.md "wikilink")[多系群的](../Page/多系群.md "wikilink")。\[10\]所以，在对鹤科重新分类中，[白鹤单成](../Page/白鹤.md "wikilink")[一属](../Page/單型.md "wikilink")，而[沙丘鹤](../Page/沙丘鹤.md "wikilink")、[白枕鹤](../Page/白枕鹤.md "wikilink")、[赤颈鹤和](../Page/赤颈鹤.md "wikilink")[澳洲鹤被移到了](../Page/澳洲鹤.md "wikilink")[赤颈鹤属](../Page/赤颈鹤属.md "wikilink")。\[11\]

**冠鶴亞科 BALEARICINAE**

  - **[冠鶴屬](../Page/冠鶴屬.md "wikilink") *Balearica***
      - [黑冕鶴](../Page/黑冕鶴.md "wikilink") *Balearica pavonina*
      - [灰冕鶴](../Page/灰冕鶴.md "wikilink") *Balearica regulorum*

**鶴亞科 GRUINAE**

  - **白鹤属 *Leucogeranus***
      - [白鹤](../Page/白鹤.md "wikilink"), *Leucogeranus leucogeranus*
  - **赤颈鹤属 *Antigone***
      - [沙丘鹤](../Page/沙丘鹤.md "wikilink"), *Antigone canadensis*
      - [白枕鶴](../Page/白枕鶴.md "wikilink"), *Antigone vipio*
      - [赤颈鹤](../Page/赤颈鹤.md "wikilink"), *Antigone antigone*
      - [澳洲鶴](../Page/澳洲鶴.md "wikilink"), *Antigone rubicunda*
  - **[鹤属](../Page/鹤属.md "wikilink") *Grus***
      - [肉垂鹤](../Page/肉垂鹤.md "wikilink"), *Grus carunculata*
      - [藍鶴](../Page/藍鶴.md "wikilink"), *Grus paradisea*
      - [蓑羽鹤](../Page/蓑羽鹤.md "wikilink"), *Grus virgo*
      - [丹顶鹤](../Page/丹顶鹤.md "wikilink"), *Grus japonensis*
      - [美洲鶴](../Page/美洲鶴.md "wikilink"), *Grus americana*
      - [灰鹤](../Page/灰鹤.md "wikilink"), *Grus grus*
      - [白头鹤](../Page/白头鹤.md "wikilink"), *Grus monacha*
      - [黑颈鹤](../Page/黑颈鹤.md "wikilink"), *Grus nigricollis*

## 歷史

在中國鶴頗受喜愛，是長壽的象徵，《[抱朴子](../Page/抱朴子.md "wikilink")·对俗》：“知龟鹤之遐寿，故效其道引以增年。”史載[卫懿公好鹤](../Page/卫懿公.md "wikilink")，不理朝政，最後因鶴亡國。晉朝的[支遁亦好鶴](../Page/支遁.md "wikilink")。北宋处士[林逋隐居杭州孤山](../Page/林逋.md "wikilink")，植梅放鹤，称“梅妻鹤子”。

## 注釋

## 参考文献

## 外部链接

  - [Saving Cranes website (ICF)](http://www.savingcranes.org/)
  - [Craneworld
    website](http://home.nikocity.de/craneworld/index_en.htm)
  - [Gruidae
    videos](http://ibc.hbw.com/ibc/phtml/familia.phtml?idFamilia=43) on
    the Internet Bird Collection

## 参见

  - [千羽鶴](../Page/千羽鶴.md "wikilink")
  - [國際鶴類基金會](../Page/國際鶴類基金會.md "wikilink")（ICF）

[Category:鹤形目](../Category/鹤形目.md "wikilink")
[鹤科](../Category/鹤科.md "wikilink")

1.

2.

3.
4.
5.

6.
7.
8.
9.

10. {{ cite journal | last1=Krajewski | first1=C. | last2=Sipiorski |
    first2=J.T. | last3=Anderson | first3=F.E. | year=2010 |
    title=Mitochondrial genome sequences and the phylogeny of cranes
    (Gruiformes: Gruidae) | journal=Auk | volume=127 | issue=2 |
    pages=440–452 | doi=10.1525/auk.2009.09045 |
    url=<https://www.researchgate.net/publication/277486138_Complete_Mitochondrial_Genome_Sequences_and_the_Phylogeny_of_Cranes_Gruiformes_Gruidae>
    }}

11.