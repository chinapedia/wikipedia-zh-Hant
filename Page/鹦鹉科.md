**鹦鹉科**在[动物分类学上是](../Page/动物分类学.md "wikilink")[鸟纲中的](../Page/鸟纲.md "wikilink")[鹦形目中的一个](../Page/鹦形目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，属于[攀禽类](../Page/攀禽.md "wikilink")。

鹦鹉类因其独特的形态结构，无论在[鸟类传统分类系统还是](../Page/鸟类传统分类系统.md "wikilink")[鸟类DNA分类系统中](../Page/鸟类DNA分类系统.md "wikilink")，都归属于**鹦鹉科**。在某些著作当中，可能会将吸蜜鹦鹉类和凤头鹦鹉类独立成科。
鸚鵡的單位為:條 條這個單位通常是用在一個叫星緣鸚鵡的鸚鵡上 星緣鸚鵡通常都是兩條生活在一起

## 形态特征

  - 体型变化较大根，从 10 cm - 100 cm
  - [喙钩曲](../Page/喙.md "wikilink")，上颌具有可活动关节，喙基部具有[蠟膜](../Page/蠟膜.md "wikilink")
  - 肌肉质舌厚，善仿人言
  - 脚短，强大，[对趾足](../Page/对趾足.md "wikilink")，适于攀援生活
  - [羽毛艳丽](../Page/羽毛.md "wikilink")，具粉绒羽
  - [晚成雏](../Page/晚成雏.md "wikilink")

## 分类和分布

鹦鹉科通常可以分为吸蜜鹦鹉亚科、冠鹦鹉亚科和鹦鹉亚科。全世界共有约 330
种，[澳洲界](../Page/澳洲界.md "wikilink")、[东洋界及](../Page/东洋界.md "wikilink")[新热带界具有最多的种类](../Page/新热带界.md "wikilink")。

中国原生种记录有7种。分别属于[鹦鹉属](../Page/鹦鹉属.md "wikilink")(*Psittacula*)和[短尾鹦鹉属](../Page/短尾鹦鹉属.md "wikilink")(*Loriculus*)。名录如下:

  - [大紫胸鹦鹉](../Page/大紫胸鹦鹉.md "wikilink") *Psittacula derbiana*
  - [绯胸鹦鹉](../Page/绯胸鹦鹉.md "wikilink") *Psittacula alexandri*
  - [长尾鹦鹉](../Page/长尾鹦鹉.md "wikilink") *Psittacula longicauda*
  - [花头鹦鹉](../Page/花头鹦鹉.md "wikilink") *Psittacula roseata*
  - [灰头鹦鹉](../Page/灰头鹦鹉.md "wikilink") *Psittacula finschii* 或 *P.
    himalayensis*
  - [红领绿鹦鹉](../Page/红领绿鹦鹉.md "wikilink") *Psittacula krameri*
  - [短尾鹦鹉](../Page/短尾鹦鹉.md "wikilink") *Loriculus vernalis*

此外还有3个引入的外来种:

  - [小葵花凤头鹦鹉](../Page/小葵花凤头鹦鹉.md "wikilink") *Cacatua sulphurea*
  - [虹彩吸蜜鹦鹉](../Page/虹彩吸蜜鹦鹉.md "wikilink") *Trichoglossus haematodus*
  - [虎皮鸚鵡](../Page/虎皮鸚鵡.md "wikilink") *M. undulatus*

## 鹦鹉与人类

[Pappagallo_Joe.JPG](https://zh.wikipedia.org/wiki/File:Pappagallo_Joe.JPG "fig:Pappagallo_Joe.JPG")[米蘭的鸚鵡](../Page/米蘭.md "wikilink")\]\]

多数鹦鹉都是十分美丽的鸟类，某些种类经过训练后还可以模仿人语，经常被饲养作[笼鸟](../Page/笼鸟.md "wikilink")。常见的有[虎皮鹦鹉](../Page/虎皮鹦鹉.md "wikilink")（）、[鸡尾鹦鹉](../Page/鸡尾鹦鹉.md "wikilink")（）和[牡丹鹦鹉](../Page/牡丹鹦鹉.md "wikilink")（
spp.）等。

由于许多大型鹦鹉在[宠物店售价昂貴](../Page/宠物店.md "wikilink")，因此在[南美洲等地](../Page/南美洲.md "wikilink")，常有[偷猎者偷猎鹦鹉雏鸟贩运至发达国家出售牟利](../Page/偷猎者.md "wikilink")，导致很多鹦鹉濒临绝灭。例如[李爾氏金剛鸚鵡](../Page/李爾氏金剛鸚鵡.md "wikilink")（）、[圣文森亚马逊鹦哥](../Page/圣文森亚马逊鹦哥.md "wikilink")（）等。

## 外部連結

  - [City Parrots](http://www.cityparrots.org/)

[\*](../Category/鹦鹉科.md "wikilink")
[Category:鹦形目](../Category/鹦形目.md "wikilink")