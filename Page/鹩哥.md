[Gracula_religiosa_MHNT_227_Jardin_des_plantes_de_Paris.jpg](https://zh.wikipedia.org/wiki/File:Gracula_religiosa_MHNT_227_Jardin_des_plantes_de_Paris.jpg "fig:Gracula_religiosa_MHNT_227_Jardin_des_plantes_de_Paris.jpg")

**鹩哥**（[学名](../Page/学名.md "wikilink")：**）是最善於學習人類語言的動物，又稱之為**-{zh-cn:九官鸟;zh-tw:鷯哥;}-**，據說一隻年輕的鹩哥只需一星期便能學會一句簡單的話。

## 生态环境

棲息在常綠闊葉林的邊緣，活躍在田野林間，卻少見於密林之中。常與[八哥](../Page/八哥.md "wikilink")、[椋鳥等合群在果樹上覓食](../Page/椋鳥.md "wikilink")。

## 分布地域

由[斯里蘭卡至](../Page/斯里蘭卡.md "wikilink")[巽他群島也有牠們](../Page/巽他群島.md "wikilink")。

專長

## 食物

鹩哥是雜食性動物，吃各種昆蟲、[植物種子及醬果](../Page/植物.md "wikilink")。

## 繁殖与保护

## 鹩哥与非法鸟类贸易

## 參考

[GR](../Category/IUCN無危物種.md "wikilink")
[GR](../Category/椋鳥科.md "wikilink")
[Category:能言鳥類](../Category/能言鳥類.md "wikilink")