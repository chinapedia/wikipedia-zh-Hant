**鹰潭市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[江西省下辖的](../Page/江西省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，是中国重要的铜工业基地，也是中国重要的铁路枢纽，是[沪昆铁路](../Page/沪昆铁路.md "wikilink")、[鹰厦铁路](../Page/鹰厦铁路.md "wikilink")、[皖赣铁路的交汇点](../Page/皖赣铁路.md "wikilink")。

## 历史

清[乾隆三十年](../Page/乾隆.md "wikilink")（1765年）置鹰潭巡检司。[同治三年](../Page/同治.md "wikilink")（1864年）设鹰潭镇，属贵溪县。

1957年因建设鹰厦铁路，升为县级鹰潭镇。1958年复为贵溪县辖镇。1960年再次升为县级镇，由上饶专区直辖。1979年改镇为县级鹰潭市，属上饶地区。

1983年7月以原上饶地区县级鹰潭市及贵溪、余江2县设地级鹰潭市，原县级鹰潭市境置月湖区。

1996年，贵溪被撤县，设立县级贵溪市。

2018年2月，撤销余江县，设立鹰潭市余江区。

## 地理

鹰潭市位於江西省東部，與[上饒市](../Page/上饒市.md "wikilink")、[撫州市](../Page/撫州市.md "wikilink")、[福建省等地接壤](../Page/福建省.md "wikilink")。

北部为[怀玉山脉](../Page/怀玉山脉.md "wikilink")，中部为[信江冲积平原](../Page/信江.md "wikilink")，南部为[武夷山脉](../Page/武夷山脉.md "wikilink")。南部的[龙虎山为典型的](../Page/龙虎山.md "wikilink")[丹霞地貌](../Page/丹霞地貌.md "wikilink")。

主要河流有信江、[白塔河](../Page/白塔河.md "wikilink")。

## 政治

### 现任领导

<table>
<caption>鹰潭市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党鹰潭市委员会.md" title="wikilink">中国共产党<br />
鹰潭市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/鹰潭市人民代表大会.md" title="wikilink">鹰潭市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/鹰潭市人民政府.md" title="wikilink">鹰潭市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议鹰潭市委员会.md" title="wikilink">中国人民政治协商会议<br />
鹰潭市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/郭安.md" title="wikilink">郭安</a>[1]</p></td>
<td><p><a href="../Page/郭清.md" title="wikilink">郭清</a>[2]</p></td>
<td><p><a href="../Page/于秀明.md" title="wikilink">于秀明</a>[3]</p></td>
<td><p><a href="../Page/戴春英.md" title="wikilink">戴春英</a>（女）[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/江西省.md" title="wikilink">江西省</a><a href="../Page/信丰县.md" title="wikilink">信丰县</a></p></td>
<td></td>
<td><p>江西省<a href="../Page/鄱阳县.md" title="wikilink">鄱阳县</a></p></td>
<td><p>江西省<a href="../Page/乐平市.md" title="wikilink">乐平市</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年3月</p></td>
<td><p>2016年11月</p></td>
<td><p>2017年7月</p></td>
<td><p>2016年11月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖2个[市辖区](../Page/市辖区.md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[月湖区](../Page/月湖区.md "wikilink")、[余江区](../Page/余江区.md "wikilink")
  - 县级市：[贵溪市](../Page/贵溪市.md "wikilink")

另外，位于贵溪市的[龙虎山风景名胜区为鹰潭市设置的县级管理单位](../Page/龙虎山风景名胜区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>鹰潭市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>360600</p></td>
</tr>
<tr class="odd">
<td><p>360602</p></td>
</tr>
<tr class="even">
<td><p>360603</p></td>
</tr>
<tr class="odd">
<td><p>360681</p></td>
</tr>
<tr class="even">
<td><p>注：月湖区数字包含鹰潭高新技术产业开发区所辖白露街道；贵溪市数字包含龙虎山风景名胜区所辖上清、龙虎山2镇。</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>鹰潭市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>鹰潭市</p></td>
<td><p>1125156</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>月湖区</p></td>
<td><p>214028</p></td>
<td><p>19.03</p></td>
</tr>
<tr class="even">
<td><p>余江区</p></td>
<td><p>352427</p></td>
<td><p>31.33</p></td>
</tr>
<tr class="odd">
<td><p>贵溪市</p></td>
<td><p>558451</p></td>
<td><p>49.64</p></td>
</tr>
<tr class="even">
<td><p>注：常住人口数据中，月湖区数字包含鹰潭高新技术产业开发区22238人；贵溪市数字包含龙虎山风景名胜区22172人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口总数为](../Page/常住人口.md "wikilink")1124906人\[8\]，同2000年[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，10年共增加了75592人，增长7.20%，平均每年增加7559人，年平均增长率为0.70%。其中，男性为590734人，占总人口的52.51%；女性为534172人，占总人口的47.49%。人口性别比（以女性为100）为110.59。0—14岁的人口为243055人，占总人口的21.60%；15—64岁的人口为798641人，占总人口的71.00%；65岁及以上人口为83210人，占总人口的7.40%。

市区以铁路移民为主，通用[普通话和](../Page/普通话.md "wikilink")[上饶铁路话](../Page/上饶铁路话.md "wikilink")\[9\]。

## 民族

汉族人口占99.71%，少数民族占0.29%。少数民族主要有畲、回、蒙、满、侗、壮、苗等民族，其中畲族人口最多。

## 交通

  - 铁路：[沪昆铁路](../Page/沪昆铁路.md "wikilink")、[鹰厦铁路](../Page/鹰厦铁路.md "wikilink")、[皖赣铁路的交汇点](../Page/皖赣铁路.md "wikilink")，[沪昆高铁设有](../Page/沪昆高铁.md "wikilink")[鹰潭北站](../Page/鹰潭北站.md "wikilink")。
  - 公路：[320国道](../Page/320国道.md "wikilink")、[206国道](../Page/206国道.md "wikilink")、[沪昆高速公路](../Page/沪昆高速公路.md "wikilink")

## 经济

## 风景名胜

  - [龙虎山](../Page/龙虎山.md "wikilink")

## 外部链接

  - [江西鹰潭政府网站](http://www.yingtan.gov.cn/)
  - [行政区划网](http://www.xzqh.org/quhua/36jx/06yingtan.htm)
  - [鹰潭网](http://www.0701w.com/)

## 参考

[Category:鹰潭](../Category/鹰潭.md "wikilink")
[Category:江西地级市](../Category/江西地级市.md "wikilink")
[赣](../Category/中国小城市.md "wikilink")
[赣](../Category/海峡西岸经济区.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.