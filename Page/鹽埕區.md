**鹽埕區**為[臺灣](../Page/臺灣.md "wikilink")[高雄市一](../Page/高雄市.md "wikilink")[市轄區](../Page/市轄區.md "wikilink")，位於市內西南端，市中心中南端，西北面與[鼓山區接壤](../Page/鼓山區.md "wikilink")，東鄰[前金區](../Page/前金區.md "wikilink")、[三民區](../Page/三民區.md "wikilink")，東南隔[愛河出口與](../Page/愛河.md "wikilink")[苓雅區相望](../Page/苓雅區.md "wikilink")，是高雄市面積最小的行政區。

本區主要是由[高雄港建港時的港底泥沙填海造陸而成](../Page/高雄港.md "wikilink")，地勢平坦，氣候屬[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")，產業以[工商業為主](../Page/工商業.md "wikilink")。此區如今沒有過去繁榮，但已漸有復甦跡象。

## 歷史

今日的鹽埕區一帶在[荷據時期以前](../Page/臺灣荷西殖民時期.md "wikilink")，便有船隻至此進行[烏魚撈捕作業](../Page/烏魚.md "wikilink")。[明鄭時期此地居民便以曬鹽為生](../Page/明鄭時期.md "wikilink")。1701年（[清](../Page/清.md "wikilink")[康熙](../Page/康熙.md "wikilink")49年），[鳳山縣衙認為打狗海濱有漁鹽之利](../Page/鳳山縣_\(臺灣\).md "wikilink")，故招徠20餘人來此開闢鹽田，因鄰近打狗港口，故稱之為「打狗鹽埕」。鹽埕的「埕」與[臺灣閩南語的](../Page/臺灣閩南語.md "wikilink")「庭」同義，皆指「工作場所的廣場」。
之後因鹽場沒落，航運興起，開始出現「[拆船](../Page/拆船.md "wikilink")」熱潮。最盛時期，在此開業的拆船業者近數十間，甚至出現所謂的「拆船大王」，然而再度沒落，高雄[侯家即是以此起家](../Page/侯姓.md "wikilink")，現[東和鋼鐵掌門人侯西泉](../Page/東和鋼鐵.md "wikilink")、侯西峰兄弟的祖父輩和父執輩都做過此行業，替侯家纘了日後創業的大量資源。

1908年[高雄港築港完成](../Page/高雄港.md "wikilink")，以港底泥沙覆蓋在本區，從鹽田澤國搖身成為[海埔新生地](../Page/海埔新生地.md "wikilink")。1928年，[高雄市役所遷至本區](../Page/高雄市.md "wikilink")[榮町](../Page/榮町.md "wikilink")（即舊高雄市政府，今[高雄市立歷史博物館](../Page/高雄市立歷史博物館.md "wikilink")），發展迅速。戰後合併堀江町、入船町、鹽埕町、榮町、北野町，設置高雄市鹽埕區至今，在1960年代時是全市人口最多的行政區，但因商業中心的東移而繁華落盡，人口開始大幅滑落，在2004年已成為高雄市鬧區中人口最少的行政區。

[位於鹽埕區瀨南街上的彰化銀行第二倉庫.jpg](https://zh.wikipedia.org/wiki/File:位於鹽埕區瀨南街上的彰化銀行第二倉庫.jpg "fig:位於鹽埕區瀨南街上的彰化銀行第二倉庫.jpg")

## 歷任區長列表

| 區長姓名                           | 區長任期               | 備註 |
| ------------------------------ | ------------------ | -- |
| [林迦](../Page/林迦.md "wikilink") | 1945年12月 - 1951年6月 |    |
| 郭萬枝                            | 1951年6月 - 1960年8月  |    |
| 黃嘉賓                            | 1960年8月 - 1975年6月  |    |
| 楊世漢                            | 1975年6月 - 1977年3月  |    |
| 張廷江                            | 1977年3月 - 1977年7月  |    |
| 謝今井                            | 1977年7月 - 1982年12月 |    |
| 莊錦進                            | 1982年12月 - 1988年8月 |    |
| 陳文樟                            | 1988年8月 - 1991年3月  |    |
| 黃清雄                            | 1991年4月 - 1994年6月  |    |
| 陳良啟                            | 1994年7月 - 1995年5月  |    |
| 李海鈺                            | 1995年6月 - 1996年6月  |    |
| 吳岳璋                            | 1996年6月 - 2000年12月 |    |
| 陳居豐                            | 2000年12月 - 2002年1月 |    |
| 郭金池                            | 2002年1月 - 2004年9月  |    |
| 郭明道                            | 2004年9月 - 2007年1月  |    |
| 林玉魁                            | 2007年1月 - 2009年4月  |    |
| 許金津                            | 2009年4月 - 2013年6月  |    |
| 薛米惠                            | 2013年6月 - 2015年1月  |    |
| 顏賜山                            | 2015年1月 - 現任       |    |

## 人口

### 人口變化

  - 資料時間：2019年1月-2019年12月；來源：內政部戶政司人口資料庫\[1\]

## 行政區

[Yancheng_villages2.svg](https://zh.wikipedia.org/wiki/File:Yancheng_villages2.svg "fig:Yancheng_villages2.svg")
[Yancheng_District_Administration_Center,_Kaohsiung_City.jpg](https://zh.wikipedia.org/wiki/File:Yancheng_District_Administration_Center,_Kaohsiung_City.jpg "fig:Yancheng_District_Administration_Center,_Kaohsiung_City.jpg")

  - 中山-{里}-、江南里、育仁里、南端里、港都里、新豐里、壽星里
  - 中原里、光明-{里}-、府北里、教仁里、新化里、博愛里、藍橋里
  - 江西里、沙地里、河濱里、陸橋里、新樂里、慈愛里、瀨南里

## 交通

### 捷運

  - [高雄捷運](../Page/高雄捷運.md "wikilink")

<!-- end list -->

  - <font color={{高雄捷運色彩|橘}}>█</font>
    [橘線](../Page/高雄捷運橘線.md "wikilink")：[鹽埕埔站](../Page/鹽埕埔站.md "wikilink")
  - <font color={{高雄捷運色彩|環}}>█</font>
    [環狀輕軌](../Page/環狀輕軌.md "wikilink")：[真愛碼頭站](../Page/真愛碼頭站.md "wikilink")
    - [駁二大義站](../Page/駁二大義站.md "wikilink")

### 主要道路

  - [建國四路](../Page/建國四路.md "wikilink")
  - [五福四路](../Page/五福四路.md "wikilink")
  - [七賢三路](../Page/七賢三路.md "wikilink")
  - [中正四路](../Page/中正四路.md "wikilink")
  - [真愛碼頭](../Page/真愛碼頭.md "wikilink")

### 公共腳踏車

目前本區共設有4座公共腳踏車租賃站，分別為：

  - [史博館站](../Page/中正路_\(高雄市\)#沿線設施.md "wikilink")
  - [漁人碼頭站](../Page/七賢路#沿線設施.md "wikilink")
  - [電影圖書館站](../Page/新樂街#主要設施.md "wikilink")
  - [鹽埕埔站](../Page/大勇路#主要設施.md "wikilink")

## 教育

### 國民中學

  - [高雄市立鹽埕國民中學](http://www.yacjh.kh.edu.tw/)

### 國民小學

  - [高雄市鹽埕區鹽埕國民小學](http://www.yacps.kh.edu.tw/)
  - [高雄市鹽埕區忠孝國民小學](http://www.chuhps.kh.edu.tw/)
  - [高雄市鹽埕區光榮國民小學](http://www.kjes.kh.edu.tw/)

## 宗教禮拜場所

#### 道教和臺灣民間信仰

  - [文武聖殿](../Page/文武聖殿.md "wikilink")
  - [鹽埕三山國王廟](../Page/鹽埕三山國王廟.md "wikilink")
  - [高雄市霞海城隍廟](../Page/高雄市霞海城隍廟.md "wikilink")
  - [鹽埕大舞台威靈宮](../Page/鹽埕大舞台威靈宮.md "wikilink")
  - [高雄朝后宮](../Page/高雄朝后宮.md "wikilink")
  - 東嶽靈殿
  - 溫王廟
  - 三聖殿
  - 沙多宮
  - 鹽埕保安宮
  - 吉貝武聖殿
  - 高案北極殿
  - 鹽埕壽山宮

#### 基督新教

  - 臺灣基督教長老教會（鹽埕教會）
  - 財團法人基督教新希望教會

## 旅遊

[Kaohsiung_Museum_of_History_face_20070106.jpg](https://zh.wikipedia.org/wiki/File:Kaohsiung_Museum_of_History_face_20070106.jpg "fig:Kaohsiung_Museum_of_History_face_20070106.jpg")
[Toabutai.jpg](https://zh.wikipedia.org/wiki/File:Toabutai.jpg "fig:Toabutai.jpg")
\]\]

  - [愛河](../Page/愛河.md "wikilink")
  - [駁二藝術特區](../Page/駁二藝術特區.md "wikilink")
  - [真愛碼頭](../Page/真愛碼頭.md "wikilink")
  - [高雄市立歷史博物館](../Page/高雄市立歷史博物館.md "wikilink")
  - [市立電影圖書館](../Page/高雄市電影館.md "wikilink")
  - [高雄巿立音樂館](../Page/高雄巿音樂館.md "wikilink")

## 當地名人

  - [莊朱玉女](../Page/莊朱玉女.md "wikilink")：慈善家，長年在高雄市公園陸橋下賣新臺幣10元價格的愛心自助餐。
  - [余陳月瑛](../Page/余陳月瑛.md "wikilink")：[余家班成員](../Page/余家班.md "wikilink")，前[高雄縣縣長](../Page/高雄縣.md "wikilink")，[余登發之媳](../Page/余登發.md "wikilink")。
  - [施明德](../Page/施明德.md "wikilink")：[紅杉軍總指揮](../Page/紅杉軍.md "wikilink")，前[民進黨主席](../Page/民主進步黨.md "wikilink")，[美麗島事件主要人物](../Page/美麗島事件.md "wikilink")。
  - [黃小琥](../Page/黃小琥.md "wikilink")：知名歌手、歌唱評審，因其對參賽者嚴格、銳利的講評而被封為「**滅絕師太**」。

## 外部連結

  - [高雄市鹽埕區公所](https://yancheng.kcg.gov.tw:4444/yancheng/index.aspx)

[鹽埕區](../Category/鹽埕區.md "wikilink")
[Category:高雄市行政區劃](../Category/高雄市行政區劃.md "wikilink")

1.  [近期各月人口統計資料 - 表八、各鄉鎮市區戶數及人口數統計表](http://www.ris.gov.tw/zh_TW/346)