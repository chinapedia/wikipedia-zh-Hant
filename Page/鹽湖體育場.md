**鹽湖體育場**,官方稱印度青年體育場([孟加拉語](../Page/孟加拉語.md "wikilink") যুব ভারতী
ক্রীড়াঙ্গন, *Yuva Bharati
Krirangan*)。是一個位於[印度](../Page/印度.md "wikilink")[加爾各答的多用途的體育場](../Page/加爾各答.md "wikilink")。

球場是全球第二大的足球場，僅次於[朝鮮](../Page/朝鮮民主主義人民共和國.md "wikilink")[平壤的](../Page/平壤.md "wikilink")[綾羅島5月1日競技場](../Page/綾羅島5月1日競技場.md "wikilink")。現在，球場主要用於[足球及](../Page/足球.md "wikilink")[田徑方面](../Page/田徑.md "wikilink")。球場於1984年建成，並在三列的結構下可以容納120,000名觀眾。\[1\]

球場距離[加爾各答的市中心有](../Page/加爾各答.md "wikilink")7[公里的距離](../Page/公里.md "wikilink")。球場呈橢圓形，頂部以[鋁管及](../Page/鋁.md "wikilink")[混凝土製成](../Page/混凝土.md "wikilink")。球場有兩個電子計分屏幕及指揮室，亦有符合國際標準的照明系統及為電視廣播而設的各種設備。

球場總面積達到
76.40[英畝](../Page/英畝.md "wikilink")，並於1984年1月正式開幕。球場除[體育活動外](../Page/體育.md "wikilink")，還可以舉行演唱會等文化活動。

## 足球

[足球是體育場最主要的活動](../Page/足球.md "wikilink")。鹽湖體育場是印度球會[東孟加拉](../Page/東孟加拉足球俱樂部.md "wikilink")、[莫亨巴根和](../Page/莫亨巴根競技俱樂部.md "wikilink")[伊斯蘭教徒的主場](../Page/伊斯蘭教徒體育俱樂部.md "wikilink")。傳統的[加爾各答](../Page/加爾各答.md "wikilink")[打比](../Page/打比.md "wikilink")（[東孟加拉對](../Page/東孟加拉足球俱樂部.md "wikilink")[莫亨巴根](../Page/莫亨巴根競技俱樂部.md "wikilink")）的比賽亦會於體育場內舉行。另外，很多[印度國家足球隊的賽事都會在體育場上演](../Page/印度國家足球隊.md "wikilink")。

## 田徑

球場亦可以舉行田徑比賽。球場亦曾舉行1987年的[南亞運動會](../Page/南亞運動會.md "wikilink")。

## 圖片

[Saltlake_stadium_kolkata.jpg](https://zh.wikipedia.org/wiki/File:Saltlake_stadium_kolkata.jpg "fig:Saltlake_stadium_kolkata.jpg")
[Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta.jpg](https://zh.wikipedia.org/wiki/File:Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta.jpg "fig:Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta.jpg")
[Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta_2.jpg](https://zh.wikipedia.org/wiki/File:Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta_2.jpg "fig:Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta_2.jpg")
[Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan,_Kolkata_-_Calcutta_4.jpg](https://zh.wikipedia.org/wiki/File:Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan,_Kolkata_-_Calcutta_4.jpg "fig:Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan,_Kolkata_-_Calcutta_4.jpg")
[Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta_5.jpg](https://zh.wikipedia.org/wiki/File:Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta_5.jpg "fig:Salt_Lake_Stadium_-_Yuva_Bharati_Krirangan_,_Kolkata_-_Calcutta_5.jpg")
[Salt_Lake_Stadium_(_Yuba_Bharati_Krirangan_)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_4.jpg](https://zh.wikipedia.org/wiki/File:Salt_Lake_Stadium_\(_Yuba_Bharati_Krirangan_\)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_4.jpg "fig:Salt_Lake_Stadium_(_Yuba_Bharati_Krirangan_)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_4.jpg")
[Salt_Lake_Stadium_(_Yuba_Bharati_Krirangan_)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_1.jpg](https://zh.wikipedia.org/wiki/File:Salt_Lake_Stadium_\(_Yuba_Bharati_Krirangan_\)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_1.jpg "fig:Salt_Lake_Stadium_(_Yuba_Bharati_Krirangan_)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_1.jpg")
[Salt_Lake_Stadium_(_Yuba_Bharati_Krirangan_)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_2.jpg](https://zh.wikipedia.org/wiki/File:Salt_Lake_Stadium_\(_Yuba_Bharati_Krirangan_\)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_2.jpg "fig:Salt_Lake_Stadium_(_Yuba_Bharati_Krirangan_)_Kolkata_India_-_FC_Bayern_Munich_Mohun_Bagan_Oliver_Kahn_2.jpg")

## 外部連結

  - [鹽湖體育場資料及圖片](http://www.sports-venue.info/FIFA/Salt_Lake_Stadium.html)

## 參考資料

[Category:印度足球場](../Category/印度足球場.md "wikilink")
[Category:加爾各答](../Category/加爾各答.md "wikilink")

1.  [eastbengalfootballclub.com](http://eastbengalfootballclub.com/stadium/)