**鹿傳霖**（），[字](../Page/表字.md "wikilink")**潤萬**，又字**滋轩**，[號](../Page/號.md "wikilink")**迂叟**，[直隸](../Page/直隶省.md "wikilink")[定兴](../Page/定兴.md "wikilink")（今[河北](../Page/河北.md "wikilink")）人，[蒙古族寶格氏](../Page/蒙古族.md "wikilink")，[清末政治人物](../Page/清末.md "wikilink")，[進士出身](../Page/進士.md "wikilink")。

## 生平

父鹿丕宗，[字](../Page/表字.md "wikilink")**杰人**，[號](../Page/號.md "wikilink")**簡堂**，官至[都匀府](../Page/都匀府.md "wikilink")[知府](../Page/知府.md "wikilink")，死于[苗民起义](../Page/苗民起义.md "wikilink")，鹿传霖是鹿丕宗第五子。鹿丕宗守卫都匀时，苗民聚集城下，鹿传霖正率领精锐士卒迎接军饷，听闻都匀警报，迅速返回协助守城，双方相持十余月，援兵断绝而都匀陷落。鹿传霖向[云贵总督报告其父死事](../Page/云贵总督.md "wikilink")，其后[清军收复都匀](../Page/清军.md "wikilink")，鹿传霖奉父母遗骸归葬，当时鹿传霖年仅二十岁，因此知名\[1\]。鹿传霖之后以[举人身份跟随](../Page/举人.md "wikilink")[钦差大臣](../Page/钦差大臣.md "wikilink")[胜保征讨](../Page/胜保.md "wikilink")[捻军](../Page/捻军.md "wikilink")，授任[同知](../Page/同知.md "wikilink")。[同治元年](../Page/同治.md "wikilink")（1862年）登壬戌科[进士](../Page/进士.md "wikilink")，选[庶吉士](../Page/庶吉士.md "wikilink")，[散馆改](../Page/散馆.md "wikilink")[广西](../Page/广西.md "wikilink")[兴安縣](../Page/兴安縣.md "wikilink")[知县](../Page/知县.md "wikilink")。因督剿[柳州](../Page/柳州.md "wikilink")、雒容土匪之功，获赐[孔雀翎](../Page/清朝官员服饰#翎羽.md "wikilink")，升任[桂林府](../Page/桂林府.md "wikilink")[知府](../Page/知府.md "wikilink")\[2\]。

[光绪四年](../Page/光绪.md "wikilink")（1878年）调[廉州](../Page/廉州.md "wikilink")。当时李扬才反叛入[越南](../Page/越南.md "wikilink")，鹿传霖紧急追捕，将其党羽解散。旋即升任[惠潮嘉道](../Page/惠潮嘉道.md "wikilink")，提升为[福建](../Page/福建.md "wikilink")[按察使](../Page/按察使.md "wikilink")，再调赴[四川](../Page/四川.md "wikilink")，迁任[布政使](../Page/布政使.md "wikilink")。光绪九年（1883年）授任[河南巡抚](../Page/河南巡抚.md "wikilink")，他清釐州县纳粮积弊，年收入增加三十馀万。光绪十一年（1885年）调任[陕西巡抚](../Page/陕西巡抚.md "wikilink")，因病回乡\[3\]。[光绪十五年](../Page/光绪.md "wikilink")（1889年）再次出任[陕西巡抚](../Page/陕西巡抚.md "wikilink")。正值[黄河河道向西侵蚀](../Page/黄河.md "wikilink")，将与[洛水相通](../Page/洛水.md "wikilink")。鹿传霖增筑石[坝三十馀座](../Page/坝.md "wikilink")，得以无患。[中日甲午战争期间](../Page/中日甲午战争.md "wikilink")，鹿传霖遣兵入卫，[清廷命他兼摄西安将军](../Page/清廷.md "wikilink")。[光绪二十一年](../Page/光绪.md "wikilink")（1895年）升任[四川总督](../Page/四川总督.md "wikilink")。[蜀地过去多盗贼](../Page/蜀地.md "wikilink")，鹿传霖特别设立一军整肃治安。[夔州](../Page/夔州.md "wikilink")、[万州饥荒](../Page/万州.md "wikilink")，鹿传霖征发[长江上游地区积粮](../Page/长江上游.md "wikilink")，又采购[湖北粮米平价出售](../Page/湖北.md "wikilink")\[4\]。

当时[英国](../Page/英国.md "wikilink")、[沙俄都窥视](../Page/沙俄.md "wikilink")[西藏](../Page/西藏.md "wikilink")，[噶厦依仗沙俄援助](../Page/噶厦.md "wikilink")，阻挠英国划定边界。英国唆使廓尔喀与西藏交战，是為[廓藏戰爭而](../Page/廓藏戰爭.md "wikilink")[瞻对地区土民苦于藏官苛政](../Page/瞻对.md "wikilink")，期望内附。鹿传霖认为瞻对为[四川门户](../Page/四川.md "wikilink")，瞻对不化服，就无法威慑西藏噶厦；噶厦不听命，则边界不能划定。适逢朱窝、章谷二[土司相互争袭](../Page/土司.md "wikilink")，鹿传霖传檄命知府[罗以礼](../Page/罗以礼.md "wikilink")、知县穆秉文往谕，以[提督周万顺统防边各军进驻](../Page/提督.md "wikilink")[打箭炉](../Page/打箭炉.md "wikilink")\[5\]。瞻对土司首领仔仲则忠札霸率军入侵章谷，抵抗[清军](../Page/清军.md "wikilink")。鹿传霖乘机进发，相继攻克诸要害。各土司全部降服，率兵听从调遣。清军渡过[雅砻江抵达瞻对](../Page/雅砻江.md "wikilink")，杀敌超过相抵之数，终于完全收服瞻对。于是鹿传霖请求归流改汉，陈述善后之策，奏疏呈上十余次。适逢[成都将军恭寿](../Page/成都.md "wikilink")、[驻藏大臣](../Page/驻藏大臣.md "wikilink")[文海交替上疏称其不便](../Page/文海.md "wikilink")，[达赖十三世也上奏解释](../Page/达赖十三世.md "wikilink")，朝廷态度于是改变，鹿传霖最终被解职\[6\]。

[光绪二十四年](../Page/光绪.md "wikilink")（1898年）任[广东巡抚](../Page/广东巡抚.md "wikilink")，旋即调往[江苏](../Page/江苏.md "wikilink")，摄理[两江总督](../Page/两江总督.md "wikilink")。[光绪二十六年](../Page/光绪.md "wikilink")（1900年）[八国联军侵华](../Page/八国联军侵华.md "wikilink")，鹿传霖征集三营士兵入卫，在[大同追及两宫銮驾](../Page/大同.md "wikilink")。到达[太原后](../Page/太原.md "wikilink")，任[两广总督](../Page/两广总督.md "wikilink")。随后入直[军机处](../Page/军机处.md "wikilink")，跟从[慈禧太后](../Page/慈禧太后.md "wikilink")、[光绪帝前往](../Page/光绪帝.md "wikilink")[西安](../Page/西安.md "wikilink")\[7\]。升任[左都御史](../Page/左都御史.md "wikilink")，迁[礼部尚书](../Page/礼部尚书.md "wikilink")，兼署[工部](../Page/工部.md "wikilink")。第二年，两宫回銮，鹿传霖兼任[督办政务大臣](../Page/政务处.md "wikilink")。凡是上疏请求增加赋税扩充财富、损害民众增益皇室的，鹿传霖一律摈弃；而力求裁汰冗余费用，停止不急工程的，都奏报可以。慈禧太后下诏后宫内部供需皆由[内务府负责](../Page/内务府.md "wikilink")，户部专掌国家财政收入，这是鹿传霖率先提议的。[光绪三十年](../Page/光绪.md "wikilink")（1904年）转任[吏部尚书](../Page/吏部尚书.md "wikilink")。[光绪三十二年](../Page/光绪.md "wikilink")（1906年）[清朝改革官制](../Page/清朝.md "wikilink")，鹿传霖罢值[军机](../Page/军机.md "wikilink")，专管部事。随即仍任[军机大臣](../Page/军机大臣.md "wikilink")，解除[吏部事务](../Page/吏部.md "wikilink")，以[尚书协办](../Page/尚书.md "wikilink")[大学士](../Page/大学士.md "wikilink")\[8\]。

[宣统帝即位后](../Page/宣统帝.md "wikilink")，鹿传霖与[摄政王](../Page/摄政王.md "wikilink")[载沣一同接受遗诏](../Page/载沣.md "wikilink")，加[太子少保](../Page/太子少保.md "wikilink")，晋[太子太保](../Page/太子太保.md "wikilink")。历拜[体仁阁](../Page/体仁阁.md "wikilink")、[东阁大学士](../Page/东阁大学士.md "wikilink")，兼任[经筵讲官](../Page/经筵讲官.md "wikilink")。[宣统二年](../Page/宣统.md "wikilink")（1910年）春，病情发作，他四次上奏请求解职，[清廷都](../Page/清廷.md "wikilink")“温谕慰留”。七月，鹿传霖逝世，时年七十五岁，入祀[贤良祠](../Page/贤良祠.md "wikilink")，追赠[太保](../Page/太保.md "wikilink")，[谥](../Page/谥.md "wikilink")**文端**\[9\]。

## 家族

鹿傳霖出身定興望族鹿氏。是[张之洞的姐夫](../Page/张之洞.md "wikilink")。[民國十三年](../Page/民國.md "wikilink")（1924年）[北京政变中将逊帝](../Page/北京政变.md "wikilink")[溥仪逐出](../Page/溥仪.md "wikilink")[紫禁城的](../Page/紫禁城.md "wikilink")[鹿钟麟則是其同宗](../Page/鹿钟麟.md "wikilink")\[10\]。

## 註釋

## 參考文獻

  - [趙爾巽等](../Page/趙爾巽.md "wikilink")，《清史稿》，中華書局點校本。

{{-}}

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝廣西興安縣知縣](../Category/清朝廣西興安縣知縣.md "wikilink")
[Category:清朝桂林府知府](../Category/清朝桂林府知府.md "wikilink")
[Category:清朝廉州府知府](../Category/清朝廉州府知府.md "wikilink")
[Category:清朝惠潮嘉道](../Category/清朝惠潮嘉道.md "wikilink")
[Category:清朝福建按察使](../Category/清朝福建按察使.md "wikilink")
[Category:清朝四川布政使](../Category/清朝四川布政使.md "wikilink")
[Category:清朝河南巡撫](../Category/清朝河南巡撫.md "wikilink")
[Category:清朝兩廣總督](../Category/清朝兩廣總督.md "wikilink")
[Category:清朝左都御史](../Category/清朝左都御史.md "wikilink")
[Category:军机大臣](../Category/军机大臣.md "wikilink")
[Category:清朝戶部尚書](../Category/清朝戶部尚書.md "wikilink")
[Category:參預政務大臣](../Category/參預政務大臣.md "wikilink")
[Category:清朝吏部尚書](../Category/清朝吏部尚書.md "wikilink")
[Category:清朝太子少保](../Category/清朝太子少保.md "wikilink")
[Category:清朝太子太保](../Category/清朝太子太保.md "wikilink")
[Category:諡文端](../Category/諡文端.md "wikilink")
[Category:定興鹿氏](../Category/定興鹿氏.md "wikilink")
[Category:定興人](../Category/定興人.md "wikilink")
[C](../Category/鹿姓.md "wikilink")

1.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“鹿传霖，字滋轩，直隶定兴人。父丕宗，官都匀知府，死寇难，谥壮节，传霖其第五子也。当丕宗守都匀时，叛苗麕聚城下，传霖方率健卒迎饷，闻警，驰还助城守，相持十阅月，援绝城陷。传霖投总督告父死状，大兵攻复都匀，奉父母遗骸归葬，时年甫二十，由是知名。”
2.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“以举人从钦差大臣胜保征捻，授同知。同治元年，成进士，选庶吉士，散馆改广西知县。以督剿柳、雒土匪功，赐孔雀翎，擢桂林知府。”
3.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“光绪四年，调廉州。时李扬才将叛扰越南，急捕之，立散其党。旋升惠潮嘉道。擢福建按察使，调四川，迁布政使。九年，授河南巡抚，清釐州县纳粮积弊，岁增三十馀万。十一年，调陕西，引疾归。”
4.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“十五年，再出抚陕。值黄河西啮，将与洛通。传霖增筑石坝三十馀座，得无患。中日构衅，遣兵入卫，命兼摄西安将军。二十一年，擢四川总督。蜀故多盗，特立一军捕治之。夔、万大饥，发上游积穀，又采湖北粮米平粜。”
5.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“是时英、俄交窥西藏，藏番恃俄援，梗英画界。英嗾廓尔喀与藏构兵，而瞻对土民苦藏官苛虐，思内附。传霖以瞻对为蜀门户，瞻不化服，无以威藏番；藏番不听命，则界无时定。而英之忌俄者益急图藏，藏亡瞻必随亡，行且及於蜀。会朱窝、章谷土司争袭事起，传霖檄知府罗以礼、知县穆秉文往谕，以提督周万顺统防边各军进驻打箭炉。”
6.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“瞻酋仔仲则忠札霸以兵侵章谷，抗我军。传霖乘机进发，迭克诸要害。各土司詟服，率兵听调。渡雅龙江抵瞻巢，斩馘过当，尽收三瞻地，乃请归流改汉，条陈善后之策，疏十数上。会成都将军恭寿、驻藏办事大臣文海交章言其不便，达赖复疏诉於朝，廷议中变，传霖解职去。”
7.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“二十四年，召授广东巡抚，旋移江苏，摄两江总督。二十六年，拳匪乱作，传霖募三营入卫，奔及乘舆於大同。至太原，授两广总督。旋命入直军机，从幸长安。”
8.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“擢左都御史，迁礼部尚书，兼署工部。明年，回跸，兼督办政务大臣。凡疏陈加赋括财、损民以益上者，传霖率摈勿用；而务汰冗费，去中饱，并奏罢不急之工：均报可。有诏自后宫内供需皆取给内务府，户部专掌军国大计，实传霖发之也。三十年，转吏部。三十二年，新官制成，乃退直，专治部事。寻仍入直，解部务，以尚书协办大学士。命查办归化城垦务大臣贻穀，论遣戍，参劾不职者数十人。”
9.  [民國](../Page/民國.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷438）:“宣统嗣立，与摄政醇亲王同受遗诏，加太子少保，晋太子太保。历拜体仁阁、东阁大学士，兼经筵讲官。二年春，疾作，章四上，皆温谕慰留。七月，卒，年七十五，赠太保，谥文端。”
10.