是[周星驰拍摄于](../Page/周星驰.md "wikilink")1992年的[电影](../Page/电影.md "wikilink")，是影片《[鹿鼎記](../Page/鹿鼎記_\(1992年電影\).md "wikilink")》的续集。該作品也是[林青霞和周星馳唯一一次合作的電影](../Page/林青霞.md "wikilink")。

1992年，周星馳到台灣宣傳鹿鼎記時，台灣記者問林青霞為什麼要出演王晶的電影（王晶素有爛片之王和三級片之王的稱號），林青霞趕忙解釋說：“和王晶無關，我只為周星馳而來！”記者又問周星馳怎麼看林青霞，[周星馳回答](../Page/周星馳.md "wikilink")：“台灣第一美人！”

## 演员表

|                                        |                                       |                                  |
| -------------------------------------- | ------------------------------------- | -------------------------------- |
| **演员**                                 | **角色**                                | '''粵語配音                          |
| [周星驰](../Page/周星驰.md "wikilink")       | [韦小宝](../Page/韦小宝.md "wikilink")      |                                  |
| [林青霞](../Page/林青霞.md "wikilink")       | 龙儿/神龍教主                               | [龍寶鈿](../Page/龍寶鈿.md "wikilink") |
| [邱淑贞](../Page/邱淑贞.md "wikilink")       | [建宁公主](../Page/和碩恪純長公主.md "wikilink") | [何錦華](../Page/何錦華.md "wikilink") |
| [李嘉欣](../Page/李嘉欣.md "wikilink")       | [阿珂](../Page/阿珂.md "wikilink")        | [鄭麗麗](../Page/鄭麗麗.md "wikilink") |
| [陈百祥](../Page/陈百祥.md "wikilink")       | 多隆                                    |                                  |
| [吳君如](../Page/吳君如.md "wikilink")       | 韦春花                                   | [陸惠玲](../Page/陸惠玲.md "wikilink") |
| [刘松仁](../Page/劉松仁.md "wikilink")       | [陈近南](../Page/陳近南.md "wikilink")      |                                  |
| [温兆伦](../Page/温兆伦.md "wikilink")       | [康熙帝](../Page/康熙帝.md "wikilink")      |                                  |
| [秦　沛](../Page/秦沛.md "wikilink")        | [吴三桂](../Page/吴三桂.md "wikilink")      |                                  |
| [汤镇业](../Page/湯鎮業.md "wikilink")       | [吴應熊](../Page/吳應熊.md "wikilink")      | [張炳強](../Page/張炳強.md "wikilink") |
| [陈德容](../Page/陈德容.md "wikilink")       | 双儿（双胞胎之一）                             | [鄭麗麗](../Page/鄭麗麗.md "wikilink") |
| [袁洁莹](../Page/袁洁莹.md "wikilink")       | 双儿（双胞胎之一）                             | [鄭麗麗](../Page/鄭麗麗.md "wikilink") |
| [任世官](../Page/任世官.md "wikilink")       | [馮錫範](../Page/馮錫範.md "wikilink")      |                                  |
| [馬海倫](../Page/馬海倫.md "wikilink")       | [獨臂神尼](../Page/獨臂神尼.md "wikilink")    | [陸惠玲](../Page/陸惠玲.md "wikilink") |
| [张　敏](../Page/張敏_\(演員\).md "wikilink") | 龙儿（易容）                                | [龍寶鈿](../Page/龍寶鈿.md "wikilink") |
| [羅　蘭](../Page/羅蘭_\(香港\).md "wikilink") | 神龍教主                                  |                                  |
| [楊菁菁](../Page/楊菁菁.md "wikilink")       | 神龍教教徒                                 |                                  |
|                                        |                                       |                                  |

## 外部連結

  - {{@movies|fRatm0878013}}

  -
  -
  -
  -
  -
[Category:王晶電影](../Category/王晶電影.md "wikilink")
[2](../Category/1990年代香港電影作品.md "wikilink")
[Category:1990年代喜劇片](../Category/1990年代喜劇片.md "wikilink")
[Category:周星馳](../Category/周星馳.md "wikilink")
[Category:鹿鼎記改編影片](../Category/鹿鼎記改編影片.md "wikilink")
[Category:香港續集電影](../Category/香港續集電影.md "wikilink")
[Category:香港喜劇片](../Category/香港喜劇片.md "wikilink")
[Category:永盛电影](../Category/永盛电影.md "wikilink")
[Category:胡伟立配乐电影](../Category/胡伟立配乐电影.md "wikilink")
[Category:嘉禾电影](../Category/嘉禾电影.md "wikilink")