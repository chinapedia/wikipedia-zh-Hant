**麗尊酒店（英語：The Lees
Hotel）**為[高雄市的五星級酒店](../Page/高雄市.md "wikilink")，飯店內設有艾可柏菲自助料理、芙悅軒湘粵料理、加勒碧海休閒中心等設施。麗尊酒店地理位置方便，位於[五福一路](../Page/五福一路.md "wikilink")，鄰近[高雄市文化中心](../Page/高雄市文化中心.md "wikilink")、[大統百貨](../Page/大統百貨.md "wikilink")、[新堀江商圈](../Page/新堀江.md "wikilink")、[高雄捷運](../Page/高雄捷運.md "wikilink")[橘線](../Page/高雄捷運橘線.md "wikilink")[信義國小站](../Page/信義國小站.md "wikilink")。麗尊酒店與麗景酒店、帕可麗酒店同為麗尊集團旗下飯店品牌。

## 外部連結

  - [麗尊酒店官方網站](http://www.theleeshotel.com/)
      -
      -

[L](../Category/高雄市旅館.md "wikilink")