**麗水縣**（）是[越南](../Page/越南.md "wikilink")[北中部的](../Page/北中部.md "wikilink")[廣平省下辖的一个縣](../Page/廣平省.md "wikilink")。全縣總面積1420.52平方公里，1998年統計，人口140804人。該縣東临[南海](../Page/南海.md "wikilink")，以[安南山脈為自然界](../Page/安南山脈.md "wikilink")，西方跟[老挝相鄰](../Page/老挝.md "wikilink")，北依[廣寧縣](../Page/廣寧縣_\(越南\).md "wikilink")，南方接[永灵縣](../Page/永灵縣.md "wikilink")，距省會[洞海市](../Page/洞海市.md "wikilink")20公里，距[河內](../Page/河內.md "wikilink")550公里。建江是該縣的主要水源，主要經濟是農業。县政府驻[建江市镇](../Page/建江.md "wikilink")。[武元甲](../Page/武元甲.md "wikilink")、[吳廷琰生長在麗水縣](../Page/吳廷琰.md "wikilink")。

## 行政区划

麗水縣下轄2市镇26社。

  - 建江市镇（Thị trấn Kiến Giang）
  - 丽宁农场市镇（Thị trấn Nông Trường Lệ Ninh）
  - 安水社（Xã An Thuỷ）
  - 甘水社（Xã Cam Thủy）
  - 阳水社（Xã Dương Thủy）
  - 华水社（Xã Hoa Thủy）
  - 洪水社（Xã Hồng Thủy）
  - 兴水社（Xã Hưng Thủy）
  - 金水社（Xã Kim Thủy）
  - 林水社（Xã Lâm Thủy）
  - 连水社（Xã Liên Thủy）
  - 禄水社（Xã Lộc Thủy）
  - 梅水社（Xã Mai Thủy）
  - 美水社（Xã Mỹ Thủy）
  - 银水社（Xã Ngân Thủy）
  - 鱼水北社（Xã Ngư Thủy Bắc）
  - 鱼水南社（Xã Ngư Thủy Nam）
  - 鱼水中社（Xã Ngư Thủy Trung）
  - 丰水社（Xã Phong Thủy）
  - 富水社（Xã Phú Thủy）
  - 莲水社（Xã Sen Thủy）
  - 山水社（Xã Sơn Thủy）
  - 新水社（Xã Tân Thủy）
  - 泰水社（Xã Thái Thủy）
  - 清水社（Xã Thanh Thủy）
  - 长水社（Xã Trường Thủy）
  - 文水社（Xã Văn Thủy）
  - 春水社（Xã Xuân Thủy）

## 交通

[國路1A](../Page/國路1A\(越南\).md "wikilink")
和[北南鐵路通過此縣](../Page/北南鐵路\(越南\).md "wikilink")。

## 參考资料

[Category:廣平省](../Category/廣平省.md "wikilink")
[L](../Category/越南县份.md "wikilink")