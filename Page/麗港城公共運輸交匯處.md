**麗港城公共運輸交匯處**（[英文](../Page/英文.md "wikilink")：****）是[香港](../Page/香港.md "wikilink")[藍田](../Page/藍田_\(香港\).md "wikilink")[晒草灣最大型的](../Page/晒草灣.md "wikilink")[公共運輸交匯處](../Page/公共運輸交匯處.md "wikilink")，位於有康街和[茶果嶺道交界](../Page/茶果嶺道.md "wikilink")，上蓋為[茜草灣鄰里社區中心](../Page/茜草灣鄰里社區中心.md "wikilink")。該公共運輸交匯處設有2條全日巴士路線的巴士總站，以及專線小巴總站。

## 總站路線資料（巴士）

### [九龍巴士40線](../Page/九龍巴士40線.md "wikilink")

  -
1995年12月7日起，[觀塘區的終點站從](../Page/觀塘區.md "wikilink")[觀塘碼頭延長至位於](../Page/觀塘碼頭.md "wikilink")[晒草灣的](../Page/晒草灣.md "wikilink")[麗港城](../Page/麗港城.md "wikilink")。2013年8月24日起，改經龍翔道和呈祥道，是該處唯一的[新界區巴士路線](../Page/新界區.md "wikilink")，主要為往返[觀塘](../Page/觀塘.md "wikilink")、[牛頭角](../Page/牛頭角.md "wikilink")、[九龍灣](../Page/九龍灣.md "wikilink")、[牛池灣](../Page/牛池灣.md "wikilink")、[鑽石山](../Page/鑽石山.md "wikilink")、[黃大仙](../Page/黃大仙.md "wikilink")、[横頭磡](../Page/横頭磡.md "wikilink")、[大窩坪](../Page/大窩坪.md "wikilink")、[荔枝角](../Page/荔枝角.md "wikilink")（往荃灣方向）、[美孚](../Page/美孚.md "wikilink")、[葵涌](../Page/葵涌.md "wikilink")、[葵芳](../Page/葵芳.md "wikilink")、[葵興](../Page/葵興.md "wikilink")、[大窩口](../Page/大窩口.md "wikilink")、[荃灣等地的乘客提供服務](../Page/荃灣.md "wikilink")。

### [九龍巴士219X線](../Page/九龍巴士219X線.md "wikilink")

  -
1994年5月9日起投入服務，為麗港城往返[九龍灣](../Page/九龍灣.md "wikilink")、[尖沙咀](../Page/尖沙咀.md "wikilink")、[佐敦以及](../Page/佐敦.md "wikilink")[紅磡的乘客而設](../Page/紅磡.md "wikilink")。該線可謂是麗港城居民的專線，皆因往返麗港城只途經觀塘區一個分站。凡尖沙咀因節日或舉辦大型活動期間封路，本線會改以[加連威老道作為臨時循環點](../Page/加連威老道.md "wikilink")，並於[香港科學館門外加設臨時站方便乘客](../Page/香港科學館.md "wikilink")。

### [過海隧道巴士621線](../Page/過海隧道巴士621線.md "wikilink")

  -
1997年2月17日起投入服務，取道[東區海底隧道和](../Page/東區海底隧道.md "wikilink")[東區走廊往返](../Page/東區走廊.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")、[金鐘和](../Page/金鐘.md "wikilink")[中環](../Page/中環.md "wikilink")，早上班次可謂是麗港城居民的專線，皆因它是途經東區海底隧道收費站而唯一不設分站的專利巴士路線。回程客量不保證的情況下，需於[銅鑼灣](../Page/銅鑼灣.md "wikilink")、東區海底隧道收費站和港鐵[藍田站設站](../Page/藍田站.md "wikilink")，間接輔助路線[601](../Page/過海隧道巴士601線.md "wikilink")、[619和](../Page/過海隧道巴士619線.md "wikilink")[671](../Page/過海隧道巴士671線.md "wikilink")。

## 總站路線資料（專線小巴）

### [九龍區專線小巴69線](../Page/九龍區專線小巴69線.md "wikilink")

  -
1997年4月1日起投入服務，車費$8.2（2013年8月24日前為$7.5），開辦初期途經[新蒲崗](../Page/新蒲崗.md "wikilink")、[啟德機場](../Page/啟德機場.md "wikilink")、[宋皇臺往返九龍城與麗港城之間](../Page/宋皇臺.md "wikilink")，自啟德政府大樓關閉後，取消途經[啟德](../Page/啟德.md "wikilink")，但此線為唯一往來麗港城至新蒲崗、九龍城路線，仍有一定的客量支持。途經[譽．港灣](../Page/譽．港灣.md "wikilink")、[富豪東方酒店](../Page/富豪東方酒店.md "wikilink")、[傲雲峰](../Page/傲雲峰.md "wikilink")、[衙前圍道](../Page/衙前圍道.md "wikilink")、[九龍城廣場](../Page/九龍城廣場.md "wikilink")(獅子石道)。

### [九龍區專線小巴69A線](../Page/九龍區專線小巴69A線.md "wikilink")

  -
2013年8月24日起投入服務，車費$9，配合路線[40改行龍翔道和呈祥道](../Page/九龍巴士40線.md "wikilink")，不再途經太子道東，界限街和花墟，從69分拆。途經[譽．港灣](../Page/譽．港灣.md "wikilink")、[富豪東方酒店](../Page/富豪東方酒店.md "wikilink")、[傲雲峰](../Page/傲雲峰.md "wikilink")、[聖德肋撒醫院](../Page/聖德肋撒醫院.md "wikilink")、[聖德肋撒堂](../Page/聖德肋撒堂.md "wikilink")、[新世紀廣場](../Page/新世紀廣場.md "wikilink")(基堤道)、[喇沙小學](../Page/喇沙小學.md "wikilink")。

## 途經路線資料（巴士）

### [城巴E22(X)線](../Page/城巴E22線.md "wikilink")

  - E22：

1997年6月1日起投入服務，當時並不途經[麗港城](../Page/麗港城.md "wikilink")。1998年7月6日起，[城巴E23線投入服務](../Page/城巴E23線.md "wikilink")，此路線前往[藍田](../Page/藍田_\(香港\).md "wikilink")（北）的班次會途經麗港城公共運輸交匯處出口旁的巴士站。及至1999年4月25日重組路線後，改由E22線途經，在此處登車可前往康田苑、康逸苑、德田邨等地方。

  - E22X:

2012年1月16日起投入服務，在此處登車可前往[康田苑](../Page/康田苑.md "wikilink")、[康逸苑](../Page/康逸苑.md "wikilink")、[德田邨](../Page/德田邨.md "wikilink")、[康雅苑](../Page/康雅苑.md "wikilink")、[廣田邨](../Page/廣田邨.md "wikilink")、[高俊苑等地方](../Page/高俊苑.md "wikilink")。

### [九巴](../Page/九巴.md "wikilink")

  - W2: [觀塘站](../Page/觀塘站.md "wikilink") ↔
    [西九龍站](../Page/西九龍站.md "wikilink")

2018年9月23日起投入服務，當時途經近[麗港公園的](../Page/麗港公園.md "wikilink")[偉發道但不設車站](../Page/偉發道.md "wikilink")。2018年12月23日起，路綫往香港西九龍站單方向增設麗港公園站，方便居民前往高鐵站、[柯士甸站及佐敦一帶](../Page/柯士甸站.md "wikilink")。

## 環景攝影圖

[Category:觀塘區巴士總站](../Category/觀塘區巴士總站.md "wikilink")
[Category:晒草灣](../Category/晒草灣.md "wikilink")
[觀塘晒草灣](../Category/香港公共運輸交匯處.md "wikilink")