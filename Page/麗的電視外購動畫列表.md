**麗的電視外購卡通**是[麗的電視從開台](../Page/麗的電視.md "wikilink")（1957年）至電視台易手（1982年），其間的粵語配音卡通片集。

## 日本

  - 1966年__月__日：[小飛俠](../Page/小飛俠阿童木.md "wikilink") ([鉄腕アトム -
    1963](../Page/:ja:鉄腕アトム.md "wikilink"))
  - 1967年__月__日：[太空神童](../Page/太空神童.md "wikilink") ([宇宙エース -
    1965](../Page/:ja:宇宙エース.md "wikilink"))
  - 1967年__月__日：[巨無霸](../Page/鐵人28號.md "wikilink") ([鉄人28号 -
    1963](../Page/:ja:鉄人28号.md "wikilink"))
  - 1968年__月__日：[飛虎隊](../Page/彩虹戰隊羅賓.md "wikilink") ([レインボー戦隊ロビン -
    1966](../Page/:ja:レインボー戦隊ロビン.md "wikilink"))
  - 1968年__月__日：[萬獸之王](../Page/萬獸之王.md "wikilink") ([ジャングル大帝(新) -
    1966](../Page/:ja:ジャングル大帝\(新\).md "wikilink")
  - 1968年__月__日：[Q太郎](../Page/Q太郎.md "wikilink") ([オバケのQ太郎 -
    1965](../Page/:ja:オバケのQ太郎.md "wikilink"))
  - 1969年__月__日：[小仙女](../Page/魔法使莎莉.md "wikilink") ([魔法使いサリー -
    1966](../Page/:ja:魔法使いサリー.md "wikilink"))
  - 1971年__月__日：[神袍小俠](../Page/神袍小俠.md "wikilink") ([紅三四郎 -
    1969](../Page/:ja:紅三四郎.md "wikilink"))
  - 1971年__月__日：[蜜蜂王子](../Page/蜜蜂王子.md "wikilink") ([昆虫物語 みなしごハッチ -
    1970](../Page/:ja:昆虫物語_みなしごハッチ.md "wikilink"))
  - 1971年__月__日：
  - 1971年__月__日：[魔法人魚公主](../Page/魔法人魚公主.md "wikilink")([魔法のマコちゃん](../Page/:ja:魔法のマコちゃん.md "wikilink"))
  - 1973年__月__日：[無敵金剛009](../Page/無敵金剛009.md "wikilink") ([サイボーグ009
    - 1968](../Page/:ja:サイボーグ009.md "wikilink"))
  - 1974年6月__日：[沙漠神童](../Page/沙漠神童.md "wikilink")
    ([バビル2世(1973)](../Page/:ja:バビル2世.md "wikilink"))
  - 1974年8月__日：[鐵甲萬能俠](../Page/鐵甲萬能俠.md "wikilink") ([マジンガーZ -
    1972](../Page/:ja:マジンガーZ.md "wikilink"))
  - 1975年__月__日：[三一萬能俠](../Page/三一萬能俠.md "wikilink") ([ゲッターロボ -
    1974](../Page/:ja:ゲッターロボ.md "wikilink"))
  - 1975年__月__日：[仙女下凡](../Page/仙女下凡.md "wikilink") ([魔女っ子メグちゃん -
    1974](../Page/:ja:魔女っ子メグちゃん.md "wikilink"))
  - 1976年__月__日：[新鐵甲萬能俠](../Page/金剛大魔神.md "wikilink") ([グレートマジンガー -
    1974](../Page/:ja:グレートマジンガー.md "wikilink"))
  - 1976年__月__日：[新木偶奇遇記](../Page/新木偶奇遇記.md "wikilink") ([樫の木モック -
    1972](../Page/:ja:樫の木モック.md "wikilink"))
  - 1976年__月__日：[新三一萬能俠](../Page/盖塔机器人G.md "wikilink") ([ゲッターロボG -
    1975](../Page/:ja:ゲッターロボG.md "wikilink"))
  - 1976年4月1日：[宇宙飛龍](../Page/宇宙飛龍.md "wikilink") ([大空魔竜ガイキング -
    1976](../Page/:ja:大空魔竜ガイキング.md "wikilink"))
  - 1976年__月__日：[太寶郎](../Page/太寶郎.md "wikilink") ([UFO戦士ダイアポロン -
    1976](../Page/:ja:UFO戦士ダイアポロン.md "wikilink"))
  - 1977年1月__日：[頑童歷險記](../Page/頑童歷險記.md "wikilink")([ハックルベリィの冒険](../Page/:ja:ハックルベリィの冒険.md "wikilink"))
  - 1977年2月28日：
  - 1977年6月__日：[七海小英雄](../Page/海底小遊俠.md "wikilink") ([海底少年マリン -
    1969](../Page/:ja:海底少年マリン.md "wikilink"))
  - 1977年6月__日：[神勇飛鷹俠](../Page/神勇飛鷹俠.md "wikilink") ([科学忍者隊ガッチャマン -
    1972](../Page/:ja:科学忍者隊ガッチャマン.md "wikilink"))
  - 1977年6月__日：[電光再造人](../Page/電光再造人.md "wikilink") ([新造人間キャシャーン -
    1973](../Page/:ja:新造人間キャシャーン.md "wikilink"))
  - 1977年6月3日：[萬能怪獸](../Page/萬能怪獸.md "wikilink") ([おらぁグズラだどキャシャーン -
    1967](../Page/:ja:おらぁグズラだど.md "wikilink"))
  - 1977年8月29日：[連環小金剛](../Page/連環小金剛.md "wikilink") ([超合体魔術ロボ ギンガイザー -
    1977](../Page/:ja:超合体魔術ロボ_ギンガイザー.md "wikilink"))
  - 1977年11月15日：[無敵鐵羅剎](../Page/無敵太空船.md "wikilink") ([グロイザーX -
    1976](../Page/:ja:グロイザーX.md "wikilink"))
  - 1978年2月__日：[太空小五義](../Page/太空小五義.md "wikilink") ([ゴワッパー5 ゴーダム -
    1976](../Page/:ja:ゴワッパー5_ゴーダム.md "wikilink"))
  - 1978年3月__日：[金銀島](../Page/金銀島.md "wikilink")
  - 1978年4月__日：[小人國歷險記](../Page/小人國歷險記.md "wikilink")
  - 1978年4月__日：[神貓劍俠](../Page/神貓劍俠.md "wikilink")
  - 1978年4月__日：[安徒生童話](../Page/安徒生童話.md "wikilink") ([アンデルセン物語 -
    1971](../Page/:ja:アンデルセン物語#テレビアニメ「アンデルセン物語」（1971年）.md "wikilink"))
  - 1978年9月__日：[巨靈神](../Page/巨靈神.md "wikilink") ([UFOロボ グレンダイザー -
    1975](../Page/:ja:UFOロボ_グレンダイザー.md "wikilink"))
  - 1978年11月5日：[太空五虎將](../Page/波羅五號.md "wikilink") ([超電磁マシーン ボルテスV -
    1977](../Page/:ja:超電磁マシーン_ボルテスV.md "wikilink"))
  - 1978年12月3日：[淘氣小飛俠](../Page/淘氣小飛俠.md "wikilink")
  - 1978年12月__日：[太空俠盜](../Page/太空俠盜.md "wikilink") ([宇宙海賊キャプテンハーロック -
    1978](../Page/:ja:宇宙海賊キャプテンハーロック.md "wikilink"))
  - 1978年12月__日：[星仔走天涯](../Page/星仔走天涯.md "wikilink")([家なき子](../Page/:ja:家なき子.md "wikilink"))
  - 1979年5月20日：[太空奇艦](../Page/宇宙戰艦大和號.md "wikilink") ([宇宙戦艦ヤマト -
    1974](../Page/:ja:宇宙戦艦ヤマト.md "wikilink"))
  - 1979年7月__日：[玲瓏三劍俠](../Page/玲瓏三劍俠.md "wikilink") ([ミクロイドS
    1973](../Page/:ja:ミクロイドS.md "wikilink"))
  - 1979年9月23日：[陰陽電磁俠](../Page/無敵龍捲風.md "wikilink") （[マグネロボ　ガ・キーン
    1976](../Page/:ja:マグネロボ_ガ・キーン.md "wikilink")）
  - 1979年12月__日：[銀河鐵道999](../Page/銀河鐵道999.md "wikilink") ([銀河鉄道999 -
    1978](../Page/:ja:銀河鉄道999_\(アニメ\).md "wikilink"))
  - 1980年__月__日：[迅雷特工](../Page/迅雷特工.md "wikilink") （[ゼロテスター -
    1973](../Page/:ja:ゼロテスター.md "wikilink")）
  - 1980年1月6日：[金剛小寶貝](../Page/金剛小寶貝.md "wikilink") （[合身戦隊メカンダーロボ -
    1977](../Page/:ja:合身戦隊メカンダーロボ.md "wikilink")）
  - 1980年1月21日：
  - 1981年1月__日：[太空保衛團](../Page/太空保衛團.md "wikilink")
  - 1981年1月__日：[神探醒目女](../Page/神探醒目女.md "wikilink")
  - 1981年3月15日：[波仔奇遇記](../Page/小寶歷險記.md "wikilink")（[ポールのミラクル大作戦 -
    1976](../Page/:ja:ポールのミラクル大作戦.md "wikilink")）
  - 1981年5月24日：[神化嬌嬌女](../Page/神奇糖.md "wikilink")（[ふしぎなメルモ -
    1971](../Page/:ja:ふしぎなメルモ.md "wikilink")）
  - 1981年5月__日：[世界偉人卡通](../Page/世界偉人卡通.md "wikilink")
  - 1980年6月4日：[機靈小和尚](../Page/機靈小和尚.md "wikilink")（[一休さん](../Page/:ja:一休さん.md "wikilink")）
  - 1982年7月22日：[戰國魔神](../Page/戰國魔神.md "wikilink")（[戦国魔神ゴーショーグン](../Page/:ja:戦国魔神ゴーショーグン.md "wikilink")）

## 美國

  - 197_年__月__日：[聰明笨伯](../Page/聰明笨伯.md "wikilink") ([The
    Flintstones 1960](../Page/:en:The_Flintstones.md "wikilink"))
  - 197_年__月__日：[大力水手](../Page/大力水手.md "wikilink") ([Popeye the
    Sailor
    1960s](../Page/:en:Popeye_the_Sailor_\(1960s_TV_series\).md "wikilink"))
  - 197_年__月__日：[高竇貓](../Page/高竇貓.md "wikilink") ([Top Cat
    1961](../Page/:en:Top_Cat.md "wikilink"))
  - 1978年__月__日：[宇宙泰山](../Page/宇宙泰山.md "wikilink") ([The Herculoids
    1967](../Page/:en:The_Herculoids.md "wikilink"))
  - 1978年__月__日：[披頭四卡通](../Page/披頭四卡通.md "wikilink") ([The Beatles
    1965-69](../Page/:en:The_Beatles_\(TV_series\).md "wikilink"))
  - 1978年__月__日：[小鳥必必](../Page/小鳥必必.md "wikilink") ([The Road Runner
    Show 1966-68](../Page/:en:The_Road_Runner_Show.md "wikilink"))
  - 1978年__月__日：[蜘蛛俠](../Page/蜘蛛俠＿卡通.md "wikilink")（[Spider-Man
    1967-TV
    series](../Page/:en:Spider-Man_\(1967_TV_series\).md "wikilink")）
  - 197_年__月__日：[神奇飛俠](../Page/神奇飛俠.md "wikilink") ([Fantastic Four
    1967-TV
    series](../Page/:en:Fantastic_Four_\(1967_TV_series\).md "wikilink"))
  - 1980年__月__日：[黃色潛水艇](../Page/黃色潛水艇.md "wikilink")
    ([披頭四卡通](../Page/披頭四.md "wikilink"))
  - 1981年__月__日：[新蜘蛛俠](../Page/新蜘蛛俠.md "wikilink") ([Spider-Man and
    His Amazing Friends
    1981](../Page/:en:Spider-Man_and_His_Amazing_Friends.md "wikilink"))

## 參見

[Category:香港電視節目列表](../Category/香港電視節目列表.md "wikilink")
[Category:動畫相關列表](../Category/動畫相關列表.md "wikilink")
[Category:麗的電視](../Category/麗的電視.md "wikilink")