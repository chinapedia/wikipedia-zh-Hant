**麗閣邨**（）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[深水埗](../Page/深水埗.md "wikilink")，大約於1977年開始興建，於1981年開始入伙，並由[香港房屋委員會負責屋邨管理](../Page/香港房屋委員會.md "wikilink")。

**怡閣苑**（）是[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[深水埗](../Page/深水埗.md "wikilink")[長沙灣道](../Page/長沙灣道.md "wikilink")329號，在麗閣邨旁邊，共有七座樓宇，在1981年落成，是香港最早落成的居屋之一。跟一般居屋不同，怡閣苑第一期所有單位均不用[補地價](../Page/補地價.md "wikilink")，而且一律都可以在公開市場自由轉讓。\[1\]

## 歷史

麗閣邨的地皮位於[欽州街西面的](../Page/欽州街.md "wikilink")[填海地](../Page/填海.md "wikilink")，前身是深水埗軍營，麗閣邨的興建與[深水埗軍營是息息相關的](../Page/深水埗軍營.md "wikilink")。深水埗軍營於1927年開始使用，軍營分為南京軍營和銀禧軍營。

及至[第二次世界大戰時](../Page/第二次世界大戰.md "wikilink")，[日佔香港](../Page/日佔香港.md "wikilink")，日軍把軍營改變為戰俘集中營，用以囚禁當時駐港而被俘虜的加拿大、印度和英國籍的士兵。第二次世界大戰後英軍重新使用深水埗軍營至1977年關閉，部分位置便建成了麗閣邨及深水埗公園，而現在麗安邨和西九龍中心的位置改為越南船民營，越南船民營於1989年關閉後，其位置建成了[麗安邨和](../Page/麗安邨.md "wikilink")[西九龍中心](../Page/西九龍中心.md "wikilink")，分別於1993年和1994年建成和啟用。

而麗閣邨鄰近長沙灣道和荔枝角道，而長沙灣道是各條大部分來往新界西的巴士線必經之路，交通十分方便。本邨亦同時鄰近[西九龍中心](../Page/西九龍中心.md "wikilink")，擁有完善的生活需要設施。

本邨在入伙前，曾命名為「荔角邨」，原因是位處荔枝角道以北地段，但因以免與荔枝角混淆，故改名為麗閣邨。

酒樓,街市和3樓商場在8,90年代非常興旺。

麗閣大酒樓1981年開業，1990年代改名為嘉龍閣海鮮酒家，2000年代酒樓改名為君好海鮮酒家，2019年2月28日被大幅加租正式結業。

2000年代屋邨人口老化，3樓商場,街市和酒樓開始生意慘淡，大不如前，領展2005年接手後，租金經過多次加租後地下街市商戶遞減至僅餘10檔，3樓商場商戶被逼遷往地下街市。

2014年，已開業多年的麗萱樓舊7-Eleven（已結業），彩絲髮廊，新興餐廳，酒樓前報攤與友誼士多先後在同一年結業。

2018年，在入伙時已開業的寶華餐廳粥麵家易手，改名為金翠餐廳。

2019年3月31日，商場在新業主基滙資本接手後大翻新地下街市，多個開業多年的商戶被逼結業。

## 屋邨資料

[Lai_Kok_Shopping_Centre_Vanguard_Supermarket_and_Chinese_Food_Restaurant.jpg](https://zh.wikipedia.org/wiki/File:Lai_Kok_Shopping_Centre_Vanguard_Supermarket_and_Chinese_Food_Restaurant.jpg "fig:Lai_Kok_Shopping_Centre_Vanguard_Supermarket_and_Chinese_Food_Restaurant.jpg")及茶餐廳\]\]
[Lai_Kok_Temporary_Market.jpg](https://zh.wikipedia.org/wiki/File:Lai_Kok_Temporary_Market.jpg "fig:Lai_Kok_Temporary_Market.jpg")
[Lai_Kok_Community_Hall.jpg](https://zh.wikipedia.org/wiki/File:Lai_Kok_Community_Hall.jpg "fig:Lai_Kok_Community_Hall.jpg")
[Lai_Kok_Estate_Slide.jpg](https://zh.wikipedia.org/wiki/File:Lai_Kok_Estate_Slide.jpg "fig:Lai_Kok_Estate_Slide.jpg")
[Lai_Kok_Estate_Fitness_and_Chess_Area.jpg](https://zh.wikipedia.org/wiki/File:Lai_Kok_Estate_Fitness_and_Chess_Area.jpg "fig:Lai_Kok_Estate_Fitness_and_Chess_Area.jpg")
[Lai_Kok_Estate_Pavilion_and_Nursery_School.jpg](https://zh.wikipedia.org/wiki/File:Lai_Kok_Estate_Pavilion_and_Nursery_School.jpg "fig:Lai_Kok_Estate_Pavilion_and_Nursery_School.jpg")
[Lai_Kok_Estate_Community_Farm.jpg](https://zh.wikipedia.org/wiki/File:Lai_Kok_Estate_Community_Farm.jpg "fig:Lai_Kok_Estate_Community_Farm.jpg")
[Yee_Kok_Court_Covered_Walkway.jpg](https://zh.wikipedia.org/wiki/File:Yee_Kok_Court_Covered_Walkway.jpg "fig:Yee_Kok_Court_Covered_Walkway.jpg")

### 麗閣邨

| 屋邨/屋苑 | 樓宇名稱                             | 樓宇類型                                    | 落成年份 |
| ----- | -------------------------------- | --------------------------------------- | ---- |
| 麗閣邨   | 麗萱樓                              | [三座相連「I」字型](../Page/I型大廈.md "wikilink") | 1981 |
| 麗蘿樓   |                                  |                                         |      |
| 麗薇樓   |                                  |                                         |      |
| 麗芙樓   | [舊長型](../Page/舊長型.md "wikilink") |                                         |      |
| 麗荷樓   |                                  |                                         |      |
| 麗菊樓   |                                  |                                         |      |
| 麗葵樓   |                                  |                                         |      |
| 麗蘭樓   |                                  |                                         |      |
| 怡閣苑   | 怡樂閣                              | 舊十字型                                    |      |
| 怡美閣   |                                  |                                         |      |
| 怡建閣   |                                  |                                         |      |
| 怡泰閣   |                                  |                                         |      |
| 怡康閣   |                                  |                                         |      |
| 怡欣閣   |                                  |                                         |      |
| 怡秀閣   |                                  |                                         |      |
|       |                                  |                                         |      |

Lai Kok Estate.jpg|麗閣邨麗荷樓及麗蘭樓 Lai Kok Estate (Hong
Kong).JPG|麗閣邨麗萱樓，前方道路為[荔枝角道](../Page/荔枝角道.md "wikilink")。
Yee Kok Court, Yee Yan House (Hong
Kong).jpg|怡閣苑怡欣閣，前方道路為[長沙灣道](../Page/長沙灣道.md "wikilink")。
Lai Kok Estate Shopping Arcade
2015.jpg|麗閣商場麵包店及[屈臣氏](../Page/屈臣氏.md "wikilink")
Lai Kok Market interior.jpg|翻新前的麗閣街市

Lai Kok Estate Child Swing.jpg|小童鞦韆 Lai Kok Estate Covered
Walkway.jpg|麗閣邨有蓋行人通道 Lai Kok Estate Mosquito Killing
Machine.jpg|滅蚊機 Yee Kok Court Car Park.jpg|停車場

## 教育及福利設施

### 幼稚園

  - [佛教曾果成中英文幼稚園](http://www.tsangkorsing.edu.hk)（1981年創辦）（位於麗萱樓3樓平台）
  - [中華基督教會基真幼稚園](http://kck.ccc.edu.hk)（1981年創辦）（位於麗蘿樓114-127號）
  - [香港聖公會基愛幼兒學校](http://www.kons.edu.hk)（1985年創辦）（位於麗菊樓地下103-116及118室）
  - [長沙灣街坊福利會林譚燕華幼稚園](http://www.lamtyw.edu.hk)（1988年創辦）（位於麗葵樓3樓310-316室）

### 綜合青少年服務中心

  - [香港小童群益會賽馬會長沙灣青少年綜合服務中心](https://csw.bgca.org.hk)（位於麗荷樓地下101-120室）

### 安老院

  - [明愛麗閣苑](https://www.caritasse.org.hk/services/main/residentia/lkho.html)（位於麗蘿樓2樓）

### 長者鄰舍中心

  - [明愛麗閣長者中心](https://www.caritasse.org.hk/services/main/neighbourhood/lknec.html)（位於麗荷樓3樓306-310室）

## 地方行政

麗閣邨與毗鄰麗安邨合組成一選區，屬為[深水埗區議會](../Page/深水埗區議會.md "wikilink")[麗閣
(選區)](../Page/麗閣_\(選區\).md "wikilink")。現時區議員為同時為[工聯會及](../Page/工聯會.md "wikilink")[民建聯的成員陳穎欣](../Page/民建聯.md "wikilink")。

## 途經之公共交通服務

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{荃灣綫色彩}}">█</font><a href="../Page/荃灣綫.md" title="wikilink">荃灣綫</a>：<a href="../Page/長沙灣站.md" title="wikilink">長沙灣站</a>、<a href="../Page/深水埗站.md" title="wikilink">深水埗站</a></li>
</ul>
<dl>
<dt><strong><a href="../Page/長沙灣道.md" title="wikilink">長沙灣道</a></strong></dt>

</dl>
<dl>
<dt><strong><a href="../Page/荔枝角道.md" title="wikilink">荔枝角道</a></strong></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/觀塘.md" title="wikilink">觀塘至</a><a href="../Page/美孚.md" title="wikilink">美孚線</a>[2]</li>
</ul>
<dl>
<dt><strong><a href="../Page/深水埗（欽州街）巴士總站.md" title="wikilink">深水埗（欽州街）巴士總站</a>/<a href="../Page/欽州街.md" title="wikilink">欽州街</a></strong></dt>

</dl>
<dl>
<dt><strong><a href="../Page/東京街.md" title="wikilink">東京街</a></strong></dt>

</dl>
<dl>
<dt><strong><a href="../Page/元州街.md" title="wikilink">元州街</a></strong></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/青山道.md" title="wikilink">青山道至</a><a href="../Page/油塘.md" title="wikilink">油塘線</a>[3]</li>
<li>青山道至<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣</a>/<a href="../Page/紅磡.md" title="wikilink">紅磡線</a> (24小時服務)[4]</li>
<li>青山道至<a href="../Page/黃大仙.md" title="wikilink">黃大仙</a>/<a href="../Page/九龍城.md" title="wikilink">九龍城線</a>[5]</li>
<li>青山道至<a href="../Page/觀塘.md" title="wikilink">觀塘線</a>[6]</li>
<li>青山道至<a href="../Page/觀塘.md" title="wikilink">觀塘</a>/<a href="../Page/黃大仙.md" title="wikilink">黃大仙線</a> (通宵線)[7]</li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [香港房屋委員會麗閣邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2755)
  - [香港房屋委員會怡閣苑資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=2&id=3091)
  - [公共屋邨原擬名稱](http://www.hk-place.com/db.php?post=d005003)

## 參考

[en:Public housing estates in Sham Shui Po\#Lai Kok
Estate](../Page/en:Public_housing_estates_in_Sham_Shui_Po#Lai_Kok_Estate.md "wikilink")

[Category:深水埗](../Category/深水埗.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.
2.  [觀塘協和街—美孚](http://www.16seats.net/chi/rmb/r_k64.html)
3.  [青山道香港紗廠—藍田及油塘](http://www.16seats.net/chi/rmb/r_k38.html)
4.  [土瓜灣及紅磡—青山道](http://www.16seats.net/chi/rmb/r_k61.html)
5.  [黃大仙及九龍城—青山道](http://www.16seats.net/chi/rmb/r_k62.html)
6.  [觀塘協和街—青山道香港紗廠](http://www.16seats.net/chi/rmb/r_k02.html)
7.  [觀塘及黃大仙—青山道](http://www.16seats.net/chi/rmb/r_k22.html)