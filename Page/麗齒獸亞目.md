**麗齒獸亞目**（Gorgonopsia）又譯**蛇髮女妖獸亞目**，是[合弓綱](../Page/合弓綱.md "wikilink")[獸孔目的一個亞目](../Page/獸孔目.md "wikilink")。雖然合弓綱最初被認為是[爬行動物的一個亞綱](../Page/爬行動物.md "wikilink")。在近年的[親緣分支分類法上](../Page/親緣分支分類法.md "wikilink")，合弓綱包括其後代[哺乳類](../Page/哺乳類.md "wikilink")。麗齒獸亞目的屬名意為「[蛇髮女妖面孔](../Page/蛇髮女妖.md "wikilink")」，以[希臘神話的蛇髮女妖為名](../Page/希臘神話.md "wikilink")。

## 特徵

麗齒獸類通常被認為是[似哺乳爬行動物](../Page/似哺乳爬行動物.md "wikilink")，但因為許多類似哺乳類的特徵，只有少數類似爬行動物的特徵，牠們更常被認為是類似爬行動物的哺乳類。麗齒獸類的類似哺乳類特徵包括：特化的牙齒形狀（[異齒型](../Page/異齒型.md "wikilink")）、完全發展的[顳顬孔](../Page/顳顬孔.md "wikilink")、直立的後肢、具有耳骨。麗齒獸類屬於[獸孔目中較特化的](../Page/獸孔目.md "wikilink")[獸齒類](../Page/獸齒類.md "wikilink")。麗齒獸類是[二疊紀晚期的最大型肉食性動物之一](../Page/二疊紀.md "wikilink")。目前已知最大型的屬是[狼蜥獸](../Page/狼蜥獸.md "wikilink")（*Inostrancevia*），身長接近4.5公尺，頭骨有45公分長，刀刃狀的牙齒有12公分長，應該是肉食性的演化結果。目前仍不確定麗齒獸類是否擁有皮毛、鱗片、或皮膚。

## 演化史

麗齒獸類、犬齒獸類、以及犬齒獸類的哺乳類後代，都屬於[獸孔目中的](../Page/獸孔目.md "wikilink")[獸齒類](../Page/獸齒類.md "wikilink")。麗齒獸類是[二疊紀中期的獸孔類動物演化而來](../Page/二疊紀.md "wikilink")。早期麗齒獸類的體型不大於[狗](../Page/狗.md "wikilink")。[二疊紀中期的優勢動物](../Page/二疊紀.md "wikilink")[恐頭獸類的滅亡](../Page/恐頭獸類.md "wikilink")，使麗齒獸類在[二疊紀晚期成為優勢掠食動物](../Page/二疊紀.md "wikilink")。在[二疊紀晚期](../Page/二疊紀.md "wikilink")，許多麗齒獸類發展到大型[狗或](../Page/狗.md "wikilink")[牛的大小](../Page/牛.md "wikilink")，有些成長至[犀牛的大小](../Page/犀牛.md "wikilink")，例如最大型的麗齒獸類[狼蜥獸](../Page/狼蜥獸.md "wikilink")（*Inostrancevia*）。麗齒獸類在[二疊紀-三疊紀滅絕事件時滅亡](../Page/二疊紀-三疊紀滅絕事件.md "wikilink")，是唯一在這是滅絕事件滅亡的獸齒類[演化支](../Page/演化支.md "wikilink")。

## 分類

麗齒獸亞目是[獸齒類的三個主要](../Page/獸齒類.md "wikilink")[演化支之一](../Page/演化支.md "wikilink")，另兩個是[獸頭亞目與](../Page/獸頭亞目.md "wikilink")[犬齒獸亞目](../Page/犬齒獸亞目.md "wikilink")。獸齒類與[草食性的](../Page/草食性.md "wikilink")[二齒獸類有接近親緣關係](../Page/二齒獸類.md "wikilink")。麗齒獸類通常包括三個亞科：麗齒獸亞科、狼蜥獸亞科、Rubidgeinae亞科，還有許多未分類到以上三個亞科的屬。其中最著名的是[麗齒獸](../Page/麗齒獸.md "wikilink")、[狼蜥獸](../Page/狼蜥獸.md "wikilink")、*Rubidgea*。

目前最廣泛的麗齒獸類研究是由Denise
Sigogneau-Russell在1989年所寫。然而，到目前還沒有詳細的[親緣分支分類法研究](../Page/親緣分支分類法.md "wikilink")。
[Lycaenops_BW2.jpg](https://zh.wikipedia.org/wiki/File:Lycaenops_BW2.jpg "fig:Lycaenops_BW2.jpg")\]\]
[Gorgonops_whaitsii1.jpg](https://zh.wikipedia.org/wiki/File:Gorgonops_whaitsii1.jpg "fig:Gorgonops_whaitsii1.jpg")\]\]
[Arctops_watsoni.jpg](https://zh.wikipedia.org/wiki/File:Arctops_watsoni.jpg "fig:Arctops_watsoni.jpg")\]\]
[Inostrancevia_4DB.jpg](https://zh.wikipedia.org/wiki/File:Inostrancevia_4DB.jpg "fig:Inostrancevia_4DB.jpg")\]\]

  - **[獸齒類](../Page/獸齒類.md "wikilink") Therapsida**
  - **麗齒獸亞目 GORGONOPSIA**
      - **麗齒獸科 Gorgonopsidae**
          - [貓頜獸](../Page/貓頜獸.md "wikilink") *Aelurognathus*
          - [貓龍獸](../Page/貓龍獸.md "wikilink") *Aelurosaurus*
          - [狐龍獸](../Page/狐龍獸.md "wikilink") *Aloposaurus*
          - [熊頜獸](../Page/熊頜獸.md "wikilink") *Arctognathus*
          - [熊面獸](../Page/熊面獸.md "wikilink") *Arctops*
          - [布魯姆頭獸](../Page/布魯姆頭獸.md "wikilink") *Broomisaurus*
          - *Cephalicustroidus*
          - *Cerdorhinus*
          - *Clelandina*
          - [犬龍獸](../Page/犬龍獸.md "wikilink") *Cyonosaurus*
          - [恐蛇髮女妖獸](../Page/恐蛇髮女妖獸.md "wikilink") *Dinogorgon*
          - [始熊面獸](../Page/始熊面獸.md "wikilink") *Eoarctops*
          - [鼩鱷獸](../Page/鼩鱷獸.md "wikilink") *Galesuchus*
          - [麗齒獸](../Page/麗齒獸.md "wikilink") *Gorgonops*
          - [獅頭獸](../Page/獅頭獸.md "wikilink") *Leontocephalus*
          - [雷塞獸](../Page/雷塞獸.md "wikilink") *Lycaenops*
          - *Paragalerhinus*
          - *Scylacognathus*
          - *Sycosaurus*
          - *Viatkogorgon*
          - **蛇髮女妖獸亞科 Gorgonopsinae**
              - [蜥熊獸](../Page/蜥熊獸.md "wikilink") *Sauroctonus*
              - *Scylacops*
          - **Rubidgeinae亞科**
              - [布魯姆頭獸](../Page/布魯姆頭獸.md "wikilink") *Broomicephalus*
              - *Niuksenitia*
              - *Prorubidgea*
              - *Rubidgea*
          - **狼蜥獸亞科 Inostranceviinae**
              - *Pravoslavleria*
              - [狼蜥獸](../Page/狼蜥獸.md "wikilink") *Inostrancevia*

## 參考資料

  - Robert T. Bakker (1986), *The Dinosaur Heresies*, Kensington
    Publishing Corp.
  - Cox, B. and Savage, R.J.G. and Gardiner, B. and Harrison, C. and
    Palmer, D. (1988) *The Marshall illustrated encyclopedia of
    dinosaurs & prehistoric animals*, 2nd Edition, Marshall Publishing
  - Carol Lane Fenton and Mildred Adams Fenton (1958) *The Fossil Book*,
    Doubleday Publishing
  - Hore, P.M. (2005), *The Dinosaur Age*, Issue \#18. National Dinosaur
    Museum
  - Denise Sigogneau-Russell, 1989, "Theriodontia I - Phthinosuchia,
    Biarmosuchia, Eotitanosuchia, Gorgonopsia" Part 17 B I,
    *Encyclopedia of Paleoherpetology*, Gutsav Fischer Verlag, Stuttgart
    and New York
  - Peter Ward (2004), *Gorgon*, Viking Penguin
  - [Gebauer (2007), Phylogeny and Evolution of the Gorgonopsia with a
    Special Reference to the Skull and Skeleton of GPIT/RE/7113
    (*‘Aelurognathus?’ parringtoni*). Ph.D. Dissertation, Tübingen
    University.](http://tobias-lib.ub.uni-tuebingen.de/volltexte/2007/2935/pdf/Eva_Gebauer.pdf%7CEva)

[麗齒獸亞目](../Category/麗齒獸亞目.md "wikilink")