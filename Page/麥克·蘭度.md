**麥克·李安納·蘭度**（，），[英格蘭職業足球員](../Page/英格蘭.md "wikilink")，司職[中場](../Page/中場.md "wikilink")，現時效力[英乙球會](../Page/英乙.md "wikilink")[車士打菲特](../Page/切斯特菲尔德足球俱乐部.md "wikilink")。

## 生平

### 球會

  - 阿仙奴預備組

蘭度以學童身份加盟阿仙奴並於2005-06球季[預備組上陣](../Page/英格蘭足球預備組聯賽.md "wikilink")7場。
蘭度亦是唯一一名球員參與2007-08球季預備組聯賽的所有賽事。他曾於2006年7月，[荷蘭](../Page/荷蘭.md "wikilink")[阿姆斯特丹舉行的](../Page/阿姆斯特丹.md "wikilink")[柏金告別賽中上陣](../Page/柏金.md "wikilink")，以及隨隊參與[奧地利和](../Page/奧地利.md "wikilink")[荷蘭的季前熱身賽之旅](../Page/荷蘭.md "wikilink")。

  - 晉升一隊

蘭度在一隊於2006年10月24日[英格蘭聯賽盃戰勝](../Page/英格蘭聯賽盃.md "wikilink")[西布朗一場首次上陣](../Page/西布朗.md "wikilink")，他後備入替
[丹尼遜](../Page/丹尼臣·佩雷拉·尼維斯.md "wikilink")。他亦有參與第四圈戰勝[愛華頓一場](../Page/愛華頓.md "wikilink")，同樣入替丹尼遜，在8強對[利物浦時](../Page/利物浦足球會.md "wikilink")，他只列在後備席，並沒有上陣。

2007年2月8日，蘭度和阿仙奴簽訂一隊合約，於7月參與了阿仙奴一隊到[奧地利的季前熱身賽](../Page/奧地利.md "wikilink")。可是，他在2007-08球季開始了不久已經失去位置，因為他於8月尾，預備組的開咧戰對[富咸的賽事受傷](../Page/富咸足球會.md "wikilink")。蘭度於2007年12月18日的聯賽盃8強對[布力般流浪得其今季首次上陣的機會](../Page/布力般流浪.md "wikilink")，而於四強對[熱刺的首回合得到第二次上陣機會](../Page/熱刺.md "wikilink")。

  - 外借般尼

蘭度於2008年1月31日（即[冬季轉會市場關窗日](../Page/冬季轉會市場.md "wikilink")）正式外借至[般尼](../Page/般尼足球會.md "wikilink")。他共代表般尼於英冠上陣10場。他於2008年5月4日英冠完結日重返阿仙奴，並於英超最後一戰對新特蘭首次於英超聯賽上陣，他於81分鐘後備入替，於比賽最後階段入球，但入球無效。\[1\]

  - 返回家鄉

在2008/09球季重返阿仙奴的蘭度，繼續缺乏上陣機會，只在盃賽上陣。2009/10年球季情況依舊，僅在一場[英格蘭聯賽盃後補上陣](../Page/英格蘭聯賽盃.md "wikilink")。2010年1月15日獲外會到家鄉球會[米尔顿凯恩斯直到球季結束](../Page/米尔顿凯恩斯足球俱乐部.md "wikilink")\[2\]，期間在聯賽取得上陣16場，並於2010年2月9日一場作客對[-{zh-hans:南安普顿;
zh-hk:修咸頓;}-的](../Page/南安普顿足球俱乐部.md "wikilink")[英格蘭聯賽錦標射入](../Page/英格蘭聯賽錦標.md "wikilink")1球。

  - 外借洛達咸

蘭度返回阿仙奴後仍然無法晉身一隊行列，於2010年10月24日被外借到[英乙球會](../Page/英乙.md "wikilink")[洛達咸三個月汲取上陣經驗](../Page/罗瑟勒姆足球俱乐部.md "wikilink")\[3\]，但於首仗對[修安聯即因](../Page/绍森德足球俱乐部.md "wikilink")[鎖骨](../Page/鎖骨.md "wikilink")[骨折僅上陣](../Page/骨折.md "wikilink")21分鐘便提早離場\[4\]，其後僅再取得3場後補上陣機會，於2011年[元旦](../Page/元旦.md "wikilink")5-0大勝[維爾港射入](../Page/韦尔港足球俱乐部.md "wikilink")1球。1月21日獲洛達咸延長借用至球季結束\[5\]。

  - 車士打菲特

蘭度於球季結束後遭阿仙奴放棄，於2011年6月24日加盟新升[英甲的](../Page/英甲.md "wikilink")[車士打菲特](../Page/切斯特菲尔德足球俱乐部.md "wikilink")，簽約一年\[6\]。首季上陣18場，雖然球隊降班[英乙](../Page/英乙.md "wikilink")，但他於2012年6月8日同意續約1年\[7\]。

## 榮譽

  - 車士打菲特

<!-- end list -->

  - [英格蘭聯賽錦標冠軍](../Page/英格蘭聯賽錦標.md "wikilink")：2011/12年；

## 參考資料

## 外部連結

  -
  - [Randall Signs Pro Terms With
    Arsenal](https://web.archive.org/web/20071103053117/http://www.arsenal.com/article.asp?thisNav=News&article=446974&lid=NewsHeadline&Title=Randall+signs+professional+terms+with+Arsenal)

  - [Profile at
    arsenal.com](https://web.archive.org/web/20071123175532/http://www.arsenal.com/player.asp?thisNav=first+team&plid=60261&clid=4421&cpid=703)

[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:阿仙奴球員](../Category/阿仙奴球員.md "wikilink")
[Category:般尼球員](../Category/般尼球員.md "wikilink")
[Category:米爾頓凱恩斯球員](../Category/米爾頓凱恩斯球員.md "wikilink")
[Category:洛達咸球員](../Category/洛達咸球員.md "wikilink")
[Category:車士打菲特球員](../Category/車士打菲特球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")

1.  <http://uk.eurosport.yahoo.com/football/premier-league/2007-2008/sunderland-arsenal-195963.html>
2.
3.
4.
5.
6.
7.