**麥可·鮑爾**（'''Michael Latham Powell
'''，）是一位[英國](../Page/英國.md "wikilink")[導演](../Page/導演.md "wikilink")，也是英國電影史上最有影響力的導演之一。麥可·鮑爾與[匈牙利籍的導演](../Page/匈牙利.md "wikilink")[艾默利·普萊斯柏格](../Page/艾默利·普萊斯柏格.md "wikilink")（Emeric
Pressburger）合作過許多著名的電影。

## 生平

## 評價

在很長的一段時間裡，麥可·鮑爾的作品並不受到重視。直到1970年代以後，麥可·鮑爾的作品開始逐漸受到社會大眾的注意。

## 作品

  - 1937年：《*The Edge of the World*》
  - 1941年：《*49th Parallel*》
  - 1943年：《[百战将军](../Page/百战将军.md "wikilink")》
  - 1943年：《*One of Our Aircraft is Missing*》
  - 1947年：《[黑水仙](../Page/黑水仙.md "wikilink")》
  - 1949年：《[紅菱艷](../Page/紅菱艷.md "wikilink")》
  - 1960年：《[魔光幻影](../Page/魔光幻影.md "wikilink")》

## 外部連結

  - [Michael Powell](http://www.powell-pressburger.org/Reviews/Micky) at
    the [Powell & Pressburger
    Pages](http://www.powell-pressburger.org/).

  -
[Category:英國導演](../Category/英國導演.md "wikilink")