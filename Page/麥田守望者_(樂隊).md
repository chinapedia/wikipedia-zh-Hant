**麥田守望者**是一支來自中國北京的搖滾樂隊，成立於1994年10月。現在由蕭瑋（主唱兼[吉他手](../Page/吉他手.md "wikilink")），劉樂（吉他手），大樂（[貝斯手](../Page/貝斯手.md "wikilink")），蘇陽（[鼓手](../Page/鼓手.md "wikilink")）組成。

## 簡介

麥田守望者成立於1994年10月，當時由蕭瑋（主唱兼吉他手），劉恩（吉他手），大樂（貝斯手），蘇陽（鼓手）組成。同年12月於北京軒豪夜總會首次演出。1996年簽約紅星生產社，同年蕭瑋、大樂、蘇陽與付翀，眼鏡蛇樂隊的肖楠、林雪、虞進，殤樂隊的劉樂組成毛毛蟲大樂隊，翻唱The
Cure樂隊作品。1997年，麥田守望者錄製首張同名專輯《[麥田守望者](../Page/麥田守望者_\(專輯\).md "wikilink")》，毛毛蟲大樂隊解散。1998年8月，《[麥田守望者](../Page/麥田守望者_\(專輯\).md "wikilink")》發行，風格以流行朋克和英式搖滾作品為主。
2000年2月，《[Save
As...](../Page/Save_As....md "wikilink")》發行，在此期間，樂隊創作以鍵盤為主，因此作品電子化，氣氛低沉憂傷。
2003年，麥田守望者與紅星生產社解約，並於2004年加入[太合麥田唱片公司](../Page/太合麥田.md "wikilink")。
2006年麥田守望者成為[綠色和平組織志願者](../Page/綠色和平.md "wikilink")，被授予“森林守望者”稱號，蕭瑋並獲邀親自前往巴布亞新幾內亞考察。同年6月樂隊發行第三張專輯《[我們的世界](../Page/我們的世界.md "wikilink")》，回歸吉他搖滾，但與《[麥田守望者](../Page/麥田守望者_\(專輯\).md "wikilink")》中的流行朋克作品不同，《[我們的世界](../Page/我們的世界.md "wikilink")》中的作品偏向英式搖滾、老搖滾和post-rock。
2007年7月發行精選集《[所有你想要的 (Best of 1997 －
2007)](../Page/所有你想要的.md "wikilink")》，收錄樂隊為[綠色和平創作的作品Green的英文版](../Page/綠色和平.md "wikilink")，同年8月，與同唱片公司歌手[李宇春合唱](../Page/李宇春.md "wikilink")、共同推出Green中文版EP，同時赴香港參加“[人山人海擁抱](../Page/人山人海.md "wikilink")[綠色和平音樂會](../Page/綠色和平.md "wikilink")”演唱會，在台上與香港組合[at17合唱Green中文版](../Page/at17.md "wikilink")。

## 成員

  - 蕭瑋 - 主唱、吉他手（1994年起）....
  - 劉樂 - 吉他手（1999年起）
  - 大樂 - 貝斯手（1994年起）
  - 蘇陽 - 鼓手（1994年起）

### 已離團成員

  - 劉恩 - 吉他手（1994年—1998年）
  - 黎鵬 - 鍵盤手（2001年—2005年）

## 作品

### 專輯

  - 1998年8月 - 《[麥田守望者](../Page/麥田守望者_\(專輯\).md "wikilink")》
  - 2000年2月 - 《[Save As...](../Page/Save_As....md "wikilink")》
  - 2006年6月 - 《[我們的世界](../Page/我們的世界.md "wikilink")》

### 精選集

  - 2007年7月 - 《[所有你想要的 (Best of 1997 －
    2007)](../Page/所有你想要的.md "wikilink")》

### EP

  - 2007年8月 - Green (中文版) Featuring [李宇春](../Page/李宇春.md "wikilink")

### 宣傳單曲

  - 2005年12月 - 《[The Perfect
    Day](../Page/The_Perfect_Day.md "wikilink")》（數字下載）
  - 2006年 - [Super Star](../Page/Super_Star.md "wikilink")
  - 2006年 - 《[一意孤行](../Page/一意孤行.md "wikilink")》
  - 2007年 - 《[所有你想要的/Green](../Page/所有你想要的/Green.md "wikilink")》

### 參與合輯

  - 1996年 - 《紅星四號》

:\*收錄：《無題》、《犧牲》

  - 1998年 - 《摩登天空2》

:\*收錄：My Sunday

  - 1998年 - 《紅星震撼1號》

:\*收錄：My Sunday （延長混音）

  - 1999年 - 《紅星六號》

:\*收錄：《大鬧天宮》

  - 2000年 - 《超高壓2000伏》

:\*收錄：OK?\!

  - 2000年 - 《卓越2000》

:\*收錄：《時間潛艇》、《電子祝福》、《卓越2000》（參與合唱）

  - 2004年 - 《紅星音樂十周年音樂紀念特輯》

:\*收錄：《在路上》

  - 2005年 - 《我的祖國》

:\*收錄：《太陽島上》

## 重要演出

  - 1994年12月25日 - 北京軒豪夜總會首次正式演出
  - 1998年5月8日 - 參與香港“Levis Body Language Battle of the Band”演唱会
  - 1999年11月24日 - 香港會展中心新翼[王菲拉闊音樂會](../Page/王菲.md "wikilink")，與張亞東出任嘉賓
  - 2000年3月24日 - 北京師範大學《[Save
    As...](../Page/Save_As....md "wikilink")》專輯發布會
  - 2000年4月6日 - 意大利都靈“BIG Torino 2000-International Biennial of Young
    Creativity”文化交流活动
  - 2004年12月3日 - 北京無名高地酒吧“1，2，3，開始！”成軍10年演出
  - 2005年12月2日 - 北京無名高地酒吧“Friday I'm In Love”演出
  - 2006年5月3日 - 北京“綠色與和平”謎笛音樂節MIDI主舞台
  - 2006年5月4日 - 北京朝陽公園“第三屆朝陽音樂節”
  - 2006年7月5日 - 北京男孩女孩酒吧《[我們的世界](../Page/我們的世界.md "wikilink")》專輯發布會
  - 2006年12月2日 - 北京男孩女孩酒吧“The Saturday Afternoon”演出
  - 2007年5月6日 - 北京朝陽公園“第四屆朝陽音樂節”
  - 2007年6月5日 - 北京星光現場“所有你想要的”新浪歌友會
  - 2007年8月21日 - 任香港“人山人海擁抱綠色和平音樂會”嘉賓
  - 2007年8月22日 - 香港尖沙咀M1 Bar & Lounge專場演出
  - 2007年9月21日 - 北京Mao Live House專場演出
  - 2007年10月3日 - 北京摩登天空音樂節演出
  - 2007年11月28日- 香港浸會大學-人文素質課程:\[麥田校園音樂會\]

## 外部連結

  - [官方網站](https://web.archive.org/web/20080522111100/http://www.cir.com.cn/)
  - [樂隊blog](http://blog.sina.com.cn/mtswz/)
  - [绿色和平：萧玮出使天堂雨林](http://www.greenpeace.org/china/zh/news/catcher-in-the-rye)

[Category:1994年成立的音樂團體](../Category/1994年成立的音樂團體.md "wikilink")
[Category:中国摇滚乐队](../Category/中国摇滚乐队.md "wikilink")