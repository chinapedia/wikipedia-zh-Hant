**麥當勞道**（舊稱**麥當奴道**，於1957年改名）位於[香港](../Page/香港.md "wikilink")[港島](../Page/香港島.md "wikilink")[金鐘](../Page/金鐘.md "wikilink")[半山區](../Page/半山區.md "wikilink")。道路西端連接[花園道](../Page/花園道.md "wikilink")，東端則連接[堅尼地道](../Page/堅尼地道.md "wikilink")。道路以第六任[香港總督](../Page/香港總督.md "wikilink")[麥當奴命名](../Page/麥當奴.md "wikilink")，跟美式[快餐店](../Page/快餐.md "wikilink")[麥當勞無關](../Page/麦当劳.md "wikilink")，而此街道亦沒有該快餐店。\[1\]

麥當勞道最有名的是麥當勞道2號聖約翰大樓，它的名字跟花園道33號的[聖約翰大廈相似](../Page/聖約翰大廈.md "wikilink")，所以前者多被稱為「麥當勞道2號」，或者以其用途名之「[香港聖約翰救護機構總部](../Page/香港聖約翰救護機構.md "wikilink")」。

麥當勞道是人稱的高尚住宅區，中環名校網區。名校包括有香港名人輩出的[聖保羅男女中學](../Page/聖保羅男女中學.md "wikilink")。

途經此道路的公共交通有連接太平山的[山頂纜車](../Page/山頂纜車.md "wikilink")，來往[中環及](../Page/中環.md "wikilink")[金鐘的](../Page/金鐘.md "wikilink")[城巴](../Page/城巴.md "wikilink")12A線，以及來往中環[香港站的小巴](../Page/香港站.md "wikilink")1A線。

該處有一座「麥當奴道公廁」，於1911年建成，有過百年歷史。

## 鄰近設施

  - [香港基督教女青年會](../Page/香港基督教女青年會.md "wikilink")
  - [聖保羅男女中學](../Page/聖保羅男女中學.md "wikilink")
  - [香港聖約翰救護機構總部](../Page/香港聖約翰救護機構.md "wikilink")
  - [山頂纜車](../Page/山頂纜車.md "wikilink")[麥當勞道站](../Page/麥當勞道站.md "wikilink")
  - [基督科學教會香港第一分會](../Page/基督科學教會香港第一分會.md "wikilink")

<File:HK> MacDonnell Road 2 St John Tower.JPG|麥當勞道2號 <File:HK> St Paul's
Co-Educational College MacD1.JPG|聖保羅男女中學正門
[File:HK_YWCA_1.JPG|香港基督教女青年會在麥當勞道1號](File:HK_YWCA_1.JPG%7C香港基督教女青年會在麥當勞道1號)
[File:First_Church_of_Christ_Scientist_Hong_Kong_Entrance.jpg|二級歷史建築：基督科學教會香港第一分會](File:First_Church_of_Christ_Scientist_Hong_Kong_Entrance.jpg%7C二級歷史建築：基督科學教會香港第一分會)

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

</div>

</div>

## 參考文獻

## 外部链接

  - [麥當勞道地圖](http://www.centamap.com/scripts/centamapgif.asp?lg=B5&tp=2&sx=&sy=&sl=&ss=0&mx=834076&my=815277&vm=&ly=&lb=&ms=2&ca=0&x=834466&y=815277&z=4)

[Category:金鐘街道](../Category/金鐘街道.md "wikilink")
[Category:半山區街道](../Category/半山區街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")

1.  [軟硬演唱會特別嘉賓 -
    Eason陳奕迅](http://www.youtube.com/watch?v=bWG3vymTLR4&list=FLh-uwhJyg1u_oms9kjMMgbg&index=11&feature=plpp_video)