**麦克斯韦-玻尔兹曼分布**是一个描述一定温度下微观粒子运动速度的[概率分布](../Page/概率分布.md "wikilink")，在[物理学和](../Page/物理学.md "wikilink")[化学中有应用](../Page/化学.md "wikilink")。最常见的应用是[统计力学的领域](../Page/统计力学.md "wikilink")。任何（宏观）物理系统的温度都是组成该系统的[分子和](../Page/分子.md "wikilink")[原子的](../Page/原子.md "wikilink")[运动的结果](../Page/运动.md "wikilink")。这些粒子有一个不同速度的范围，而任何单个粒子的[速度都因与其它粒子的](../Page/速度.md "wikilink")[碰撞而不断变化](../Page/碰撞.md "wikilink")。然而，对于大量粒子来说，处于一个特定的速度范围的粒子所占的比例却几乎不变，如果系统处于或接近处于平衡。麦克斯韦-玻尔兹曼分布具体说明了这个比例，对于任何速度范围，作为系统的温度的[函数](../Page/函数.md "wikilink")。它以[詹姆斯·麦克斯韦和](../Page/詹姆斯·麦克斯韦.md "wikilink")[路德维希·玻尔兹曼命名](../Page/路德维希·玻尔兹曼.md "wikilink")。

这个分布可以视为一个三维[向量的大小](../Page/向量.md "wikilink")，它的分量是独立和[正态分布的](../Page/正态分布.md "wikilink")，其期望值为0，[标准差为](../Page/标准差.md "wikilink")\(a\)。如果\(X_i\)的分布为\(\ X \sim N(0, a^2)\)，那么

\[Z = \sqrt{X_1^2+X_2^2+X_3^2}\] 就呈麦克斯韦-玻尔兹曼分布，其参数为\(a\)。

## 麦克斯韦-玻尔兹曼分布的物理应用

麦克斯韦-玻尔兹曼分布形成了[分子运动论的基础](../Page/分子运动论.md "wikilink")，它解释了许多基本的[气体性质](../Page/气体.md "wikilink")，包括[压强和](../Page/压强.md "wikilink")[扩散](../Page/扩散.md "wikilink")。麦克斯韦-玻尔兹曼分布通常指气体中分子的速率的分布，但它还可以指分子的速度、动量，以及动量的大小的分布，每一个都有不同的概率分布函数，而它们都是联系在一起的。

麦克斯韦-玻尔兹曼分布可以用[统计力学来推导](../Page/统计力学.md "wikilink")（参见[麦克斯韦-玻尔兹曼统计](../Page/麦克斯韦-玻尔兹曼统计.md "wikilink")）。它对应于由大量不相互作用的粒子所组成、以碰撞为主的系统中最有可能的速率分布，其中量子效应可以忽略。由于气体中分子的相互作用一般都是相当小的，因此麦克斯韦-玻尔兹曼分布提供了气体状态的非常好的近似。

在许多情况下（例如[非弹性碰撞](../Page/非弹性碰撞.md "wikilink")），这些条件不适用。例如，在[电离层和空间](../Page/电离层.md "wikilink")[等离子体的物理学中](../Page/等离子体.md "wikilink")，特别对电子而言，重组和碰撞激发（也就是辐射过程）是重要的。如果在这个情况下应用麦克斯韦-玻尔兹曼分布，就会得到错误的结果。另外一个不适用麦克斯韦-玻尔兹曼分布的情况，就是当气体的量子与粒子之间的距离相比不够小时，由于有显著的量子效应也不能使用麦克斯韦-玻尔兹曼分布。另外，由于它是基于[非相对论的假设](../Page/非相对论.md "wikilink")，因此麦克斯韦-玻尔兹曼分布不能做出分子的速度大于[光速的概率为零的预言](../Page/光速.md "wikilink")。

## 推导

[麦克斯韦最初的推导假设了三个方向上的表现都相同](../Page/詹姆斯·克拉克·麦克斯韦.md "wikilink")，但后来在[玻尔兹曼的一个推导中利用](../Page/玻尔兹曼.md "wikilink")[分子运动论去掉了这个假设](../Page/分子运动论.md "wikilink")。现在，麦克斯韦-玻尔兹曼分布可以轻易地从能量的[玻尔兹曼分布推出](../Page/玻尔兹曼分布.md "wikilink")：

\[\frac{N_i}{N} = \frac{g_i \exp\left(-E_i/kT \right) } { \sum_{j}^{} g_j \,{\exp\left(-E_j/kT\right)} }
\qquad\qquad (1)\]

其中*N*<sub>*i*</sub>是平衡温度*T*时，处于状态 *i* 的粒子数目，具有能量 *E*<sub>*i*</sub> 和简并度
*g<sub>i</sub>* ，*N*
是系统中的总粒子数目，*k*是[玻尔兹曼常数](../Page/玻尔兹曼常数.md "wikilink")。（注意有时在上面的方程中不写出简并度*g*<sub>*i*</sub>。在这个情况下，指标*i*将指定了一个单态，而不是具有相同能量*E*<sub>*i*</sub>的*g*<sub>*i*</sub>的多重态。）由于速度和速率与能量有关，因此方程1可以用来推出气体的温度和分子的速度之间的关系。这个方程中的分母称为正则[配分函数](../Page/配分函数.md "wikilink")。

### 动量向量的分布

下列所述的推导，与[詹姆斯·克拉克·麦克斯韦描述的推导和后来由](../Page/詹姆斯·克拉克·麦克斯韦.md "wikilink")[路德维希·玻尔兹曼描述的具有较少假设的推导都有很大不同](../Page/路德维希·玻尔兹曼.md "wikilink")。它与玻尔兹曼在1877年的探讨比较接近。

对于“理想气体”（由基态的非相互作用原子所组成）的情况，所有能量都是动能的形式。宏观粒子的动能与动量的关系为：

\[E=\frac{p^2}{2m}
\qquad\qquad (2)\]

其中*p*<sup>2</sup>是动量向量**p** = \[*p*<sub>*x*</sub>, *p*<sub>*y*</sub>, *p*<sub>*z*</sub>\]的平方。因此，我们可以把方程1写成：

\[\frac{N_i}{N} =
\frac{1}{Z}
\exp \left[
-\frac{p_x^2 + p_y^2 + p_z^2}{2mkT}
\right]
\qquad\qquad (3)\]

其中*Z*是[配分函数](../Page/配分函数.md "wikilink")，对应于方程1中的分母。在这里，*m*是气体的分子质量，*T*是热力学温度，*k*是[玻尔兹曼常数](../Page/玻尔兹曼常数.md "wikilink")。这个*N*<sub>**i**</sub>/*N*的分布与找到具有这些动量分量值的分子的[概率密度函数](../Page/概率密度函数.md "wikilink")*f*<sub>**p**</sub>成[正比](../Page/正比.md "wikilink")，因此：

\[f_\mathbf{p}(p_x, p_y, p_z) =
\frac{c}{Z}
\exp \left[
-\frac{p_x^2 + p_y^2 + p_z^2}{2mkT}
\right].
\qquad\qquad (4)\]

[歸一化常數](../Page/歸一化常數.md "wikilink")*c*可以通过认识到分子具有*任何*动量的概率必须为1来决定。因此，方程4在所有*p*<sub>*x*</sub>、*p*<sub>*y*</sub>和*p*<sub>*z*</sub>上的积分必须是1。

可以证明：

\[c = \frac{Z}{(2 \pi mkT)^{3/2}}.
\qquad\qquad (5)\]

把方程5代入方程4，得出：

\[f_\mathbf{p}(p_x, p_y, p_z) =
\left( \frac{1}{2 \pi mkT} \right)^{3/2}
\exp \left[
-\frac{p_x^2 + p_y^2 + p_z^2}{2mkT}
\right].
\qquad\qquad (6)\]

可以看出，这个分布是三个独立、呈[正态分布的变量](../Page/正态分布.md "wikilink")\(p_x\)、\(p_y\)和\(p_z\)的乘积，其方差为\(mkT\)。此外，可以看出动量的大小呈麦克斯韦-玻尔兹曼分布，其中\(a=\sqrt{mkT}\)。

### 能量的分布

利用*p*² = 2*mE*，以及动量的大小的分布函数（参见以下速率分布的章节），我们便得出能量的分布：

\[f_E\,dE=f_p\left(\frac{dp}{dE}\right)\,dE =2\sqrt{\frac{E}{\pi(kT)^3}}~\exp\left[\frac{-E}{kT}\right]\,dE. \qquad \qquad (7)\]

由于能量与三个呈正态分布的动量分量的平方和成正比，因此这个分布是具有三个自由度的[卡方分布](../Page/卡方分布.md "wikilink")：

\[f_E(E)\,dE=\chi^2(x;3)\,dx\]

其中

\[x=\frac{2E}{kT}.\,\]

麦克斯韦-玻尔兹曼分布还可以通过把气体视为[量子气体来获得](../Page/盒中气体.md "wikilink")。

### 速度向量的分布

认识到速度的概率密度函数*f*<sub>**v**</sub>与动量的概率密度函数成正比：

\[f_\mathbf{v} d^3v = f_\mathbf{p} \left(\frac{dp}{dv}\right)^3 d^3v\]

并利用**p** = m**v**，我们便得到：

\[f_\mathbf{v}(v_x, v_y, v_z) =
\left(\frac{m}{2 \pi kT} \right)^{3/2}
\exp \left[-
\frac{m(v_x^2 + v_y^2 + v_z^2)}{2kT}
\right],
\qquad\qquad\]

这就是麦克斯韦-玻尔兹曼速度分布。在速度[相空间](../Page/相空间.md "wikilink")（*v*<sub>*x*</sub>, *v*<sub>*y*</sub>, *v*<sub>*z*</sub>）的一块无穷小区域\[*dv*<sub>*x*</sub>, *dv*<sub>*y*</sub>, *dv*<sub>*z*</sub>\]内找到具有特定速度**v** = \[*v*<sub>*x*</sub>, *v*<sub>*y*</sub>, *v*<sub>*z*</sub>\]的气体分子的几率为

\[f_\mathbf{v} \left(v_x, v_y, v_z\right)\, dv_x\, dv_y\, dv_z.\]

像动量一样，这个分布是三个独立、呈[正态分布的变量](../Page/正态分布.md "wikilink")\(v_x\)、\(v_y\)和\(v_z\)的乘积，但方差为\(\frac{kT}{m}\)。还可以看出，对于速度向量\[*v*<sub>*x*</sub>, *v*<sub>*y*</sub>, *v*<sub>*z*</sub>\]，麦克斯韦-玻尔兹曼速度分布是三个方向上的分布的乘积：

\[f_v \left(v_x, v_y, v_z\right) = f_v(v_x)f_v(v_y)f_v(v_z)\]

其中一个方向上的分布为：

\[f_v(v_i) =
\sqrt{\frac{m}{2 \pi kT}}
\exp \left[
\frac{-mv_i^2}{2kT}
\right].
\qquad\qquad\]

这个分布具有[正态分布的形式](../Page/正态分布.md "wikilink")，其方差为\(\frac{kT}{m}\)。正如所预料的，对于静止的气体，在任何方向上的平均速度都是零。

### 速率的分布

[MaxwellBoltzmann-en.svg](https://zh.wikipedia.org/wiki/File:MaxwellBoltzmann-en.svg "fig:MaxwellBoltzmann-en.svg")在298.15 K（25 °C）的温度下的速率分布函数。y轴的单位为s/m，因此任何一段曲线下的面积（它表示速度处于那个范围的概率）都是无量纲的。\]\]

通常，我们更感兴趣于分子的速率，而不是它们的速度分量。麦克斯韦-玻尔兹曼速率分布为：

\[f(v) = \sqrt{\frac{2}{\pi}\left(\frac{m}{kT}\right)^3}\, v^2 \exp \left(\frac{-mv^2}{2kT}\right)\]

其中速率*v*定义为：

\[v = \sqrt{v_x^2 + v_y^2 + v_z^2}\]

注意：在这个方程中，f(v)的单位是概率每速率，或仅仅是速率的倒数，如右图那样。

由于速率是三个独立、呈正态分布的速度分量的平方之和的平方根，因此这个分布是麦克斯韦-玻尔兹曼分布。

我们通常更感兴趣于粒子的平均速率，而不是它们的实际分布。平均速率、最概然速率（众数），以及均方根速率可以从麦克斯韦-玻尔兹曼分布的性质获得。

### 典型的速率

虽然以上的方程给出了速率的分布，或具有特定速率的分子的比例，我们通常更感兴趣于粒子的平均速率，而不是它们的实际分布。

#### 最概然速率(最大可能速率)

最概然速率*v*<sub>*p*</sub>，是系统中任何分子最有可能具有的速率，对应于*f*(*v*)的最大值或[众数](../Page/众数.md "wikilink")。要把它求出来，我们计算*df*/*dv*，设它为零，然后对*v*求解：

\[\frac{df(v)}{dv} =  0\]

得出：

\[v_p = \sqrt { \frac{2kT}{m} } = \sqrt { \frac{2RT}{M} }\]

其中*R*是[气体常数](../Page/气体常数.md "wikilink")，*M* =
[*N<sub>A</sub>*](../Page/阿伏伽德罗常数.md "wikilink")*m*是物质的[摩尔质量](../Page/摩尔质量.md "wikilink")。

对于[室温](../Page/室温.md "wikilink")（300[K](../Page/开尔文.md "wikilink")）下的氮气（[空气的主要成分](../Page/空气.md "wikilink")），可得\(v_p = 422\)m/s。

#### 平均速率

平均速率是速率分布的数学期望值：

\[\langle v \rangle = \int_0^{\infin} v \, f(v) \, dv= \sqrt { \frac{8kT}{\pi m}}= \sqrt { \frac{8RT}{\pi M}}\]

#### 均方根速率

[均方根速率](../Page/均方根.md "wikilink")*v*<sub>rms</sub>是速率的平方的平均值的平方根：

\[v_\mathrm{rms} = \left(\int_0^{\infin} v^2 \, f(v) \, dv  \right)^{1/2}= \sqrt { \frac{3kT}{m}}= \sqrt { \frac{3RT}{M} }\]

#### 三种典型速率的关系

它们具有以下的关系：

\[v_p : v_{average} : v_\mathrm{rms} \approx 1:1.128:1.224\]。\[1\]

#### 非統計的推導方式

馬克斯威-波茲曼速率分布也可直接由氣體速率均向性以及分離變數的假設以微分方程計算得到指數函數之形式，微分方程解的未定數項則由粒子總數以及方均根速率和波茲曼常數的氣體動力論關係兩者聯立得解.詳見外部連結.

### 相对论气体的速率分布

[Plot_showing_Maxwell-Juttner_distribution_(relativistic_Maxwellian)_for_electron_gas_at_different_temperatures.png](https://zh.wikipedia.org/wiki/File:Plot_showing_Maxwell-Juttner_distribution_\(relativistic_Maxwellian\)_for_electron_gas_at_different_temperatures.png "fig:Plot_showing_Maxwell-Juttner_distribution_(relativistic_Maxwellian)_for_electron_gas_at_different_temperatures.png")

当气体越来越热时，*kT*趋于或超过*mc<sup>2</sup>*，这个相对论麦克斯韦气体的速率分布由Maxwell-Juttner分布给出：\[2\]:

\[f(\gamma) = \frac {\gamma^2 \beta }{\theta K_2(1/\theta)}
\mathrm{exp}
\left(
- \frac {\gamma}{\theta}
\right)
\qquad (11)\]

其中\(\beta = \frac {v}{c},\) \(\gamma=\frac{1}{\sqrt { 1-{\beta^2} }},\)
\(\theta=\frac{kT}{mc^2},\)和\(K_2\)是第二类变形[贝塞尔函数](../Page/贝塞尔函数.md "wikilink")。

## 参考文献

## 外部链接

  - [Maxwell
    Distribution](http://mathworld.wolfram.com/MaxwellDistribution.html)
    at [Mathworld](../Page/Mathworld.md "wikilink")
  - [簡易推導(非統計)](https://web.archive.org/web/20150511012854/http://www.eecis.udel.edu/~breech/physics/physics-notes/node32.html)
  - [詳細推導(非統計)](http://nptel.ac.in/courses/112103016/33)

## 参见

  - [玻尔兹曼因子](../Page/玻尔兹曼因子.md "wikilink")
  - [瑞利分布](../Page/瑞利分布.md "wikilink")
  - [理想气体状态方程](../Page/理想气体状态方程.md "wikilink")
  - [詹姆斯·克拉克·麦克斯韦](../Page/詹姆斯·克拉克·麦克斯韦.md "wikilink")
  - [分子运动论](../Page/分子运动论.md "wikilink")

{{-}}

[Category:连续分布](../Category/连续分布.md "wikilink")
[Category:气体](../Category/气体.md "wikilink")
[Category:粒子统计学](../Category/粒子统计学.md "wikilink")
[Category:统计力学](../Category/统计力学.md "wikilink")
[Category:詹姆斯·克拉克·馬克士威](../Category/詹姆斯·克拉克·馬克士威.md "wikilink")

1.
2.  Synge, J.L., *The relativistic gas*, Noord-Holland, **1957**