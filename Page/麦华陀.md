**麦华陀**爵士（1822年-1885年），一译麦特赫斯脱，全名瓦尔特·亨利·麦华陀爵士(Sir Walter Henry
Medhurst)，[英国外交官员](../Page/英国.md "wikilink")。

麦华陀是英国著名[传教士](../Page/传教士.md "wikilink")[麦都思之子](../Page/麦都思.md "wikilink")，1822年出生在[荷属东印度](../Page/荷属东印度.md "wikilink")[巴达维亚](../Page/巴达维亚.md "wikilink")（今[印度尼西亚](../Page/印度尼西亚.md "wikilink")[雅加达](../Page/雅加达.md "wikilink")），曾在英国Blundell's
School和澳门接受教育，学习[中文](../Page/中文.md "wikilink")、[荷兰语和](../Page/荷兰语.md "wikilink")[马来语](../Page/马来语.md "wikilink")。1839年随父亲[麦都思来中国](../Page/麦都思.md "wikilink")。在[鸦片战争期间](../Page/鸦片战争.md "wikilink")，他担任总司令[乔治·懿律和](../Page/乔治·懿律.md "wikilink")[砵甸乍爵士的中文翻译](../Page/砵甸乍.md "wikilink")。后历任英国驻[福州](../Page/福州.md "wikilink")、[上海](../Page/上海.md "wikilink")、[杭州和](../Page/杭州.md "wikilink")[汉口](../Page/汉口.md "wikilink")[领事](../Page/领事.md "wikilink")。1854年6月，曾代表英国驻华公使[包令到南京访问太平天国](../Page/包令.md "wikilink")。1861年领导上海外侨进行抵抗太平军的上海保卫战。1868年，麦华陀受上海领事[阿礼国派遣](../Page/阿礼国.md "wikilink")，积极干预[扬州教案](../Page/扬州教案.md "wikilink")，率军舰前往南京，迫使两江总督[曾国藩将扬州知府撤职](../Page/曾国藩.md "wikilink")，重申保护教会。1876年促使[上海公共租界设立了招收华人的非教会学校](../Page/上海公共租界.md "wikilink")[格致书院](../Page/格致书院.md "wikilink")。1876年底退休回国，获得爵位。1881年，他参与创建[北婆罗洲渣打公司](../Page/北婆罗洲渣打公司.md "wikilink")，接着又代表公司组织[苦力到婆罗洲](../Page/苦力.md "wikilink")。著有《在远东中国的外国人》。

过去，上海公共租界有一条以他命名的马路——麦特赫斯脱路（Medhurst
Road），即今天的[泰兴路](../Page/泰兴路.md "wikilink")。

## 参考

  - C. A. Harris, "Medhurst, Sir Walter Henry (1822–1885)," rev. T. G.
    Otte, *Oxford Dictionary of National Biography,* Oxford University
    Press, 2004; online edn, May 2007, accessed 3 Aug 2007.
  - F.J Snell, "The chronicles of Twyford, being a new and popular
    history of the town of Tiverton in Devonshire: with some account of
    Blundell's School founded A.D. 1604", Simpkin, Marshall, Hamilton,
    Kent & Co, Limited, 1892.

[M](../Page/category:1822年出生.md "wikilink")
[M](../Page/category:1885年逝世.md "wikilink")

[M](../Category/英国外交官.md "wikilink")
[Category:在中国的英国人](../Category/在中国的英国人.md "wikilink")
[Category:第一次鸦片战争人物](../Category/第一次鸦片战争人物.md "wikilink")