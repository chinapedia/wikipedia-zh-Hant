**麦卡伦国际机场**（'''，）是服务[美国](../Page/美国.md "wikilink")[内华达州](../Page/内华达州.md "wikilink")[拉斯维加斯市和](../Page/拉斯维加斯.md "wikilink")[克拉克县的主要](../Page/克拉克县_\(内华达州\).md "wikilink")[機場](../Page/機場.md "wikilink")，位于拉斯维加斯市中心以南约8公里的[天堂市](../Page/天堂市.md "wikilink")，总面积为1,133公顷，由克拉克县航空局营运。

2007年，麦卡伦国际机场是全球第14多乘客使用的机场，全年共有47,595,140人次使用；该年它也是全球第6多起降班次的机场，全年共有609,472班航班起降。\[1\]

## 航空公司與目的地

### 客運

## 注释

## 外部链接

  - [官方网站](http://www.mccarran.com/)

  - [History of McCarran
    airport](http://www.onlinenevada.org/McCarran_International_Airport),
    *onlinenevada.org*

  - [Flight and checkpoint
    delays](http://www.lasvegassun.com/guides/flightdelays/),
    *lasvegassun.com*

  - [Howard W. Cannon Aviation
    Museum](http://www.clarkcountynv.gov/Depts/parks/Pages/cannon-aviation-museum.aspx)
    – official site

  - from [Nevada DOT](../Page/Nevada_DOT.md "wikilink")

  -
  -
  - Jeppesen airport diagrams for
    [1955](http://www.flickr.com/photos/12530375@N08/8041962050/sizes/l)
    and
    [1966](http://www.flickr.com/photos/12530375@N08/8066233386/sizes/l)

[M](../Category/內華達州機場.md "wikilink")
[M](../Category/拉斯维加斯谷交通.md "wikilink")
[M](../Category/冠以人名的美洲機場.md "wikilink")
[M](../Category/天堂市建築物.md "wikilink")
[M](../Category/1942年启用的机场.md "wikilink")

1.