**麦积区**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省](../Page/甘肃省.md "wikilink")[天水市下属的一个](../Page/天水市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。面积3452平方公里，2004年人口58万。因境内有[麦积山石窟而得名](../Page/麦积山石窟.md "wikilink")。

2005年1月之前称为**北道区**\[1\]。

## 行政区划

下辖3个[街道办事处](../Page/街道办事处.md "wikilink")、17个[镇](../Page/镇.md "wikilink")：

。

## 外部链接

  - [麦积区政府网站](http://www.maiji.gov.cn/)

## 参考文献

[甘/甘肃](../Page/category:国家级贫困县.md "wikilink")

[麦积区](../Category/麦积区.md "wikilink") [区](../Category/天水区县.md "wikilink")
[天水](../Category/甘肃省市辖区.md "wikilink")

1.