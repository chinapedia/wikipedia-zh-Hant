[Britney_Spears_MSG.jpg](https://zh.wikipedia.org/wiki/File:Britney_Spears_MSG.jpg "fig:Britney_Spears_MSG.jpg")
**麥迪遜廣場花園**（），常簡稱為MSG，是[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市的一座著名体育场馆](../Page/紐約市.md "wikilink")，位于[賓夕法尼亞車站的建址之上](../Page/賓夕法尼亞車站_\(紐約市\).md "wikilink")，是许多大型体育比赛、演唱会和政治活动的举办地。亦同是[NBA](../Page/NBA.md "wikilink")[紐約尼克和](../Page/紐約尼克.md "wikilink")[國家冰球聯盟](../Page/國家冰球聯盟.md "wikilink")（NHL）[紐約遊騎兵隊之主場](../Page/紐約遊騎兵.md "wikilink")。

麦迪逊广场花园始建于1879年，因原址位于[麦迪逊广场而得名](../Page/麦迪逊广场.md "wikilink")，其后虽然于1925年和1968年两次迁往新址，但是名称并未改变。

目前的麥迪遜廣場花園是1986年2月11日開幕，被認同是紐約大都會區最悠久也是最活躍的運動場地之一。
麥迪遜同時也是[國家冰球聯盟最悠久的主場館](../Page/國家冰球聯盟.md "wikilink")，以及[國家籃球協會第二悠久的主場館](../Page/國家籃球協會.md "wikilink")，另外由售票紀錄來看，也是世界第三大的娛樂表演場地，僅次於[英格蘭的曼城體育館與](../Page/英格蘭.md "wikilink")[O2體育館](../Page/O2體育館.md "wikilink")。
在2013年，由售票紀錄來看麥迪遜為世界第23名的娛樂表演場地 \[1\]

## 紀錄及重要事件

在1980年，麥迪遜舉辦了第一屆的世界女子柔道大賽，由[英國的Jane](../Page/英國.md "wikilink")
Bridge獲得48公斤項目首面金牌﹔於1985年,舉辦首屆了由世界摔角協會(今[世界摔角娛樂](../Page/世界摔角娛樂.md "wikilink"))主辦的摔角狂熱(Wrestlemania)，以及於1988年舉辦的首屆[夏日衝擊](../Page/夏日衝擊.md "wikilink")。
在1990年， Andrew Dice
Clay是歷史上第一位在麥迪遜連續演出兩晚售票表演的喜劇藝人；2009年，[NCAA創下史上第二長的男子籃球賽由當時正在進行大西區男子籃球巡迴賽](../Page/NCAA.md "wikilink")(Big
East Men's Basketball
Tournament)的[雪城橘對上](../Page/雪城大学橙人队.md "wikilink")[康乃狄克哈士奇](../Page/康涅狄格大学哈士奇队.md "wikilink")，共進入6次加時延長賽。

[香港歌手](../Page/香港.md "wikilink")[張學友曾于](../Page/張學友.md "wikilink")1995年的世界巡迴演唱會紐約站中使用麦迪逊广场花园作為兩場大型演唱會的場地，是[亞洲歌手首次的麦迪逊广场花园個人大型表演](../Page/亞洲.md "wikilink")，并在當時被《[紐約時報](../Page/紐約時報.md "wikilink")》等紐約當地重要傳媒廣泛報導，引起不小反響。\[2\]\[3\]

  - 華人登台紀錄

<!-- end list -->

  - 1979年10月3日，[羅文演唱會](../Page/羅文.md "wikilink")，由環球（國際）娛樂公司Worldwide
    Entertainment主辦。[3](http://www.nytimes.com/1979/09/30/archives/arts-and-leisure-guide-of-special-interest.html)
  - 1995年，[張學友](../Page/張學友.md "wikilink")95世界巡迴演唱會紐約站，\[4\][張學友95世界巡迴演唱會紐約站](../Page/友學友95世界巡迴演唱會.md "wikilink")。
  - 2014年3月22日，[五月天第一個](../Page/五月天.md "wikilink")[華人](../Page/華人.md "wikilink")[樂團舉行](../Page/樂團.md "wikilink")《[諾亞方舟世界巡迴演唱會](../Page/五月天諾亞方舟世界巡迴演唱會.md "wikilink")》[紐約站](../Page/紐約.md "wikilink")。

<!-- end list -->

  - 韓國與日本登台紀錄

<!-- end list -->

  - 1985年8月14日，[LOUDNESS為第一個亞洲樂團和日本重金屬樂團登上此地舉行](../Page/響度樂團.md "wikilink")
    Thunder in the East Tour in the West 1985，暖場由[Mötley
    Crüe擔任](../Page/克魯小丑樂團.md "wikilink")。
  - 2006年2月2日-3日，[RAIN](../Page/Rain.md "wikilink")“Rainy Day in New
    York”個人演唱會。
  - 2011年11月23日，[SMTOWN Live '10 World
    Tour第一個在麥迪遜舉辦的韓國藝人演唱會](../Page/SMTOWN_Live_'10_World_Tour.md "wikilink")。（包括[東方神起](../Page/東方神起.md "wikilink")、[BoA](../Page/BoA.md "wikilink")、[安七炫](../Page/安七炫.md "wikilink")、[Super
    Junior](../Page/Super_Junior.md "wikilink")、[少女時代](../Page/少女時代.md "wikilink")、[SHINee](../Page/SHINee.md "wikilink")、[f(x)](../Page/f\(x\)_\(組合\).md "wikilink")）
  - 2012年3月25日，日本搖滾樂團[L'Arc〜en〜Ciel](../Page/L'Arc〜en〜Ciel.md "wikilink")[彩虹樂團舉行演唱會](../Page/彩虹樂團.md "wikilink")《[L'Arc〜en〜Ciel
    WORLD TOUR
    2012](../Page/L'Arc〜en〜Ciel_WORLD_TOUR_2012.md "wikilink")》
  - 2014年10月12日，視覺系搖滾樂團[X JAPAN登台演出](../Page/X_JAPAN.md "wikilink")。

## 外部連結

  - [TheGarden.com](https://web.archive.org/web/20160204001416/http://www.thegarden.com/)

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[Category:紐約市建築物](../Category/紐約市建築物.md "wikilink")
[Category:美國室內體育館](../Category/美國室內體育館.md "wikilink")
[Category:紐約市體育](../Category/紐約市體育.md "wikilink")
[Category:演唱會場地](../Category/演唱會場地.md "wikilink")

1.  2013年 Pollstar 售票紀錄 (由1/1/2013至31/12/2013)
    [1](http://sprint_center.s3.amazonaws.com/doc/2013YearEndWorldwideTicketSalesTop200ArenaVenues.pdf)
2.  Garden of Dreams: Madison Square Garden 125 Years (Hardcover)
    Stewart, Tabori and Chang; Not Indicated edition (November 1, 2004)
3.  紐約時報：《充滿甜美而柔情的廣東歌之王》Published: October 10, 1995
    [2](http://www.nytimes.com/1995/10/10/arts/music-review-so-sweet-so-smooth-such-a-king-of-canto-pop.html)
4.  Garden of Dreams: Madison Square Garden 125 Years (Hardcover)
    Stewart, Tabori and Chang; Not Indicated edition (November 1, 2004)