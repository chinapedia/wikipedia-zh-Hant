**麻原彰晃**（，），[日本](../Page/日本.md "wikilink")[殺人犯](../Page/殺人犯.md "wikilink")、[恐怖份子](../Page/恐怖份子.md "wikilink")、[邪教組織](../Page/邪教組織.md "wikilink")「[奧姆真理教](../Page/奧姆真理教.md "wikilink")」[教主](../Page/教主.md "wikilink")，[恐怖組織](../Page/恐怖組織.md "wikilink")「[真理國](../Page/真理國.md "wikilink")」[神聖法皇](../Page/太上法皇.md "wikilink")\[1\]（領導人），1980年代化名為**麻原彰晃**\[2\]，本名**松本智津夫**\[3\]\[4\]，生於[九州](../Page/九州_\(日本\).md "wikilink")[熊本縣](../Page/熊本縣.md "wikilink")[八代市](../Page/八代市.md "wikilink")\[5\]\[6\]。

1984年，麻原彰晃成立了一個融合[藏傳佛教](../Page/藏傳佛教.md "wikilink")、[印度教](../Page/印度教.md "wikilink")、[基督教](../Page/基督教.md "wikilink")、[奧修](../Page/奧修.md "wikilink")、[瑜伽](../Page/瑜伽.md "wikilink")、[氣功等元素的](../Page/氣功.md "wikilink")[新興宗教](../Page/新興宗教.md "wikilink")[奧姆真理教](../Page/奧姆真理教.md "wikilink")。一度有15,000名信眾\[7\]\[8\]\[9\]，還在[俄羅斯](../Page/俄羅斯.md "wikilink")、[斯里蘭卡設立支部](../Page/斯里蘭卡.md "wikilink")\[10\]，涉及大量[詐騙](../Page/詐騙.md "wikilink")、違法吸金、恐嚇取財、性侵、綁架與謀殺，其資產高達一百多億[日圓](../Page/日圓.md "wikilink")\[11\]。

但麻原要成為[日本之王](../Page/日本之王.md "wikilink")\[12\]\[13\]，一度組黨參選，想要當上[總理大臣](../Page/日本內閣總理大臣.md "wikilink")\[14\]，選舉慘敗之後，他打算發動[武裝暴動](../Page/武裝暴動.md "wikilink")，推翻[日本政府](../Page/日本政府.md "wikilink")，創立了[奧姆國家计划](../Page/奧姆真理教的顛覆國家计划.md "wikilink")，開始各種[恐怖攻擊](../Page/恐怖攻擊.md "wikilink")。1995年3月，造成13人死亡、
6,500人受傷的[东京地铁沙林毒气事件爆發](../Page/东京地铁沙林毒气事件.md "wikilink")，麻原彰晃被捕\[15\]。也被查出涉及指使[松本沙林事件](../Page/松本沙林事件.md "wikilink")、[坂本堤律師一家殺害事件](../Page/坂本堤律師一家殺害事件.md "wikilink")、[龜戶異臭事件等](../Page/龜戶異臭事件.md "wikilink")13宗[恐怖活動](../Page/恐怖主義.md "wikilink")\[16\]\[17\]。2018年7月6日，经时任[法務大臣](../Page/法務大臣.md "wikilink")[上川陽子批准後](../Page/上川陽子.md "wikilink")，麻原與原教团核心干部、[井上嘉浩](../Page/井上嘉浩.md "wikilink")、、、和一同[絞刑](../Page/絞刑.md "wikilink")\[18\]\[19\]\[20\]\[21\]。[安倍內閣已加強警方戒備](../Page/安倍內閣.md "wikilink")，嚴防信眾報復\[22\]。7月26日，上川陽子批准（又名「小池泰男」）、、、、[豐田亨](../Page/豐田亨.md "wikilink")、[廣瀨健一等六人處決](../Page/廣瀨健一.md "wikilink")，至此全案13名死刑犯全數[伏法](../Page/伏法.md "wikilink")\[23\]。

## 生平

### 早年

1955年，麻原彰晃生於[熊本縣](../Page/熊本縣.md "wikilink")[八代市](../Page/八代市.md "wikilink")\[24\]\[25\]，祖父曾為派駐[朝鲜半島的](../Page/朝鲜半島.md "wikilink")[警官](../Page/警官.md "wikilink")\[26\]。麻原的父亲生于今日的[南韓](../Page/南韓.md "wikilink")[全羅北道](../Page/全羅北道.md "wikilink")[益山市](../Page/益山市.md "wikilink")，在[日本二戰投降](../Page/日本二戰投降.md "wikilink")，[朝鮮日治時期結束後](../Page/朝鮮日治時期.md "wikilink")，才被[盟軍遣返](../Page/盟軍.md "wikilink")，回到熊本老家，是[疊蓆](../Page/疊蓆.md "wikilink")（[榻榻米](../Page/榻榻米.md "wikilink")）織造商人\[27\]\[28\]\[29\]。麻原一家共有九名兄弟姊妹，麻原彰晃排行老七，其中有兩位兄長是盲人，家境清寒\[30\]。麻原彰晃患有先天性[白內障](../Page/白內障.md "wikilink")，一目完全[失明](../Page/失明.md "wikilink")，另一目視力極差（僅剩下60%视力）\[31\]。因為他眼睛還有微弱視力，並學過一些[柔道與](../Page/柔道.md "wikilink")[防身術](../Page/防身術.md "wikilink")，幼年即在盲童寄宿學校中[霸凌其他盲童](../Page/霸凌.md "wikilink")\[32\]，同學稱「對他而言，暴力就像嗜好。他一火大就無法停手」\[33\]。

曾學[針灸](../Page/針灸.md "wikilink")，十九歲時取得針灸師執照而離開盲人學校\[34\]，後兩度投考[東京大學法律系](../Page/東京大學.md "wikilink")，都落榜\[35\]。1976年麻原被控傷害罪，並被處以罰款。1978年，麻原結婚，並在[千葉縣](../Page/千葉縣.md "wikilink")[船橋市開辦針灸院](../Page/船橋市.md "wikilink")。1980年，麻原加入[新興宗教](../Page/新興宗教.md "wikilink")[阿含宗](../Page/阿含宗.md "wikilink")；1981年，[山口組曾意圖刺殺他但不成功](../Page/山口組.md "wikilink")\[36\]。1982年開辦[漢方藥店](../Page/漢方.md "wikilink")“亞細亞堂”，為了詐財，他出售用[橘子皮浸](../Page/橘子.md "wikilink")[醋做成的](../Page/醋.md "wikilink")“萬能[草藥](../Page/草藥.md "wikilink")”，服用者完全無效，被指控賣假藥而遭逮捕、罰款\[37\]\[38\]，而後受到帕坦佳利《瑜珈經》的影響，開始自學瑜珈與冥想，據說因此前往[印度](../Page/印度.md "wikilink")，並買了幾本[奧修的書](../Page/奧修.md "wikilink")\[39\]，兩年後成為一名[瑜伽教練](../Page/瑜伽.md "wikilink")，並吸收[印度教中](../Page/印度教.md "wikilink")[濕婆派與](../Page/濕婆派.md "wikilink")[左道性力的觀念](../Page/左道性力.md "wikilink")，開始創立奧姆真理教。\[40\]

### 創教

1983年，松本智津夫在[东京都](../Page/东京都.md "wikilink")[澀谷區](../Page/澀谷區.md "wikilink")[樱丘町注册一间名为](../Page/櫻丘町_\(澀谷區\).md "wikilink")“凤凰庆林馆”的[補習班](../Page/補習班.md "wikilink")，随后化名为麻原彰晃，其中“麻原”与“[阿修罗](../Page/阿修罗_\(佛教\).md "wikilink")”谐音，“彰晃”与“[释迦](../Page/释迦牟尼.md "wikilink")”谐音\[41\]。1984年2月，麻原设立奥姆真理教的前身，[瑜伽道場](../Page/瑜伽.md "wikilink")“[奥姆神仙会](../Page/奥姆神仙会.md "wikilink")”\[42\]\[43\]，1986年，麻原自稱前去[喜馬拉雅山脈修道](../Page/喜馬拉雅山脈.md "wikilink")，並說他已[覺悟](../Page/覺悟.md "wikilink")，得到「最終[解脫](../Page/解脫.md "wikilink")」。1987年，他將「奧姆神仙會」改称“奥姆真理教”，自称[教主](../Page/教主.md "wikilink")，並經常在公眾顯示自己在[空中漂浮的](../Page/悬停.md "wikilink")[合成照片](../Page/合成照片.md "wikilink")\[44\]，藉以吸引信眾，尤其是被[泡沫經濟](../Page/日本泡沫经济.md "wikilink")\[45\]影響的年輕人\[46\]。1988年，麻原与和[十四世达赖有深交的](../Page/十四世达赖.md "wikilink")接近，并表示：“想请[藏传佛教的](../Page/藏传佛教.md "wikilink")[长老看看自己的修行到了哪种程度](../Page/上座_\(佛教\).md "wikilink")”\[47\]。班玛噶波介绍其在[达兰萨拉的宗教文化厅和当地长老一起](../Page/达兰萨拉.md "wikilink")[冥想](../Page/冥想.md "wikilink")、[修行](../Page/修行.md "wikilink")，并获得很高评价。因此，[达赖喇嘛接见了麻原彰晃](../Page/达赖喇嘛.md "wikilink")，麻原並利用此事大作文章\[48\]，達賴多次澄清，僅以為他是一位日本來訪的[密宗信徒而已](../Page/密宗.md "wikilink")，讚揚他有心向佛，未曾給予其[灌頂](../Page/灌頂.md "wikilink")\[49\]，達賴還多次否認跟他的關係，更是聲明並未接受其捐獻\[50\]\[51\]。

麻原彰晃為了[傳教](../Page/傳教.md "wikilink")，無所不用其極，他聲稱三歲時就[靈魂出竅](../Page/靈魂出竅.md "wikilink")，六歲時就直接死亡後[復活](../Page/復活.md "wikilink")，還有[奪舍](../Page/奪舍.md "wikilink")、[他心通等](../Page/他心通.md "wikilink")[法術](../Page/法術.md "wikilink")\[52\]，自稱是[婆羅門教的](../Page/婆羅門教.md "wikilink")[濕婆神](../Page/濕婆神.md "wikilink")\[53\]、[佛教的](../Page/佛教.md "wikilink")[摩訶迦羅](../Page/摩訶迦羅.md "wikilink")\[54\]（[大黑天](../Page/大黑天.md "wikilink")）\[55\]，[基督教的](../Page/基督教.md "wikilink")[耶穌](../Page/耶穌.md "wikilink")\[56\]\[57\]\[58\]、[埃及的](../Page/埃及.md "wikilink")[印和闐與](../Page/印和闐.md "wikilink")[法老王](../Page/法老王.md "wikilink")\[59\]、[中國的](../Page/中國.md "wikilink")[明太祖](../Page/明太祖.md "wikilink")\[60\]、[日本的](../Page/日本.md "wikilink")[德川家康](../Page/德川家康.md "wikilink")\[61\][轉世](../Page/轉世.md "wikilink")，是[釋迦牟尼佛的真傳弟子](../Page/釋迦牟尼佛.md "wikilink")\[62\]\[63\]，是最後一位[救世主](../Page/救世主.md "wikilink")\[64\]，真正的[彌賽亞](../Page/彌賽亞.md "wikilink")\[65\]，要對世界萬民作「[最後的審判](../Page/最後的審判.md "wikilink")」\[66\]，殺死所有的[敵基督與](../Page/敵基督.md "wikilink")[不信者](../Page/不信者.md "wikilink")\[67\]\[68\]，還宣稱[第三次世界大戰](../Page/第三次世界大戰.md "wikilink")、[大爆炸與](../Page/大爆炸.md "wikilink")[彗星撞地球](../Page/彗星撞地球.md "wikilink")\[69\]即將發生，導致[世界末日](../Page/末世論.md "wikilink")，只有信奉他的教徒可以倖免於難\[70\]\[71\]，麻原透過讓信徒服用[安非他命](../Page/安非他命.md "wikilink")\[72\]與[LSD](../Page/LSD.md "wikilink")[迷幻藥的方式](../Page/迷幻藥.md "wikilink")\[73\]，使奥姆真理教的信徒對他非常崇信，一度达到[神職人員](../Page/神職人員.md "wikilink")1,400人，[平信徒](../Page/平信徒.md "wikilink")1.4万人的庞大规模，他還會選擇名校出身的律師、醫生和科學家當「[閣員](../Page/閣員.md "wikilink")」\[74\]，且封那些重要的幹部為[阿難尊者](../Page/阿難尊者.md "wikilink")、[文殊菩薩等](../Page/文殊菩薩.md "wikilink")\[75\]。

在[俄國](../Page/俄國.md "wikilink")、[美國的](../Page/美國.md "wikilink")[紐約與](../Page/紐約.md "wikilink")[加州等地區](../Page/加州.md "wikilink")，也有信徒數千人\[76\]\[77\]。

### 教內敗行

麻原彰晃有「戒殺」之語，絕對禁止[殺生](../Page/殺生.md "wikilink")\[78\]，但「尊師」麻原彰晃的命令是例外。他下令的殺人被定位成「拯救」，因此擁有[化武工廠](../Page/化武.md "wikilink")，可製造[爆裂物與](../Page/爆裂物.md "wikilink")[毒氣](../Page/毒氣.md "wikilink")，教徒參與殺人搶劫等犯罪\[79\]。對於信眾[清規森嚴](../Page/清規.md "wikilink")，信眾必須[素食](../Page/素食.md "wikilink")，標準的餐點是只吃米飯（最好是[糙米](../Page/糙米.md "wikilink")）、蔬菜、豆類、湯和水果（最好是[香蕉](../Page/香蕉.md "wikilink")），絕對不准葷腥，但麻原本身每天大魚大肉，還拿[炸蝦](../Page/炸蝦.md "wikilink")[天婦羅當](../Page/天婦羅.md "wikilink")[夜宵](../Page/夜宵.md "wikilink")\[80\]\[81\]。

麻原「嚴禁[邪淫](../Page/欲邪行.md "wikilink")」，命令信徒不得與[配偶或](../Page/配偶.md "wikilink")[戀人以外的人發生](../Page/戀人.md "wikilink")[性行為](../Page/性行為.md "wikilink")，甚至[禁慾](../Page/禁慾.md "wikilink")，不許[自慰](../Page/自慰.md "wikilink")。但麻原彰晃凌駕一切戒律，夜夜姦淫女信徒，並有自己的[後宮情婦部隊](../Page/後宮.md "wikilink")「[荼枳尼天](../Page/荼枳尼天.md "wikilink")」\[82\]，共有一百多名情婦\[83\]，頭號情婦「教團財務大臣」（又被稱為「女帝」，與麻原育有三個孩子\[84\]）專人替他「選妃」，對象通常是15歲到20歲貌美的處女\[85\]，最多不可以超過25歲，麻原就從這支「荼枳尼天」部隊，夜夜召集許多個女信徒同時臨幸，進行[男女雙修](../Page/男女雙修.md "wikilink")[歡喜法](../Page/歡喜法.md "wikilink")\[86\]（即性交）。女信徒入教時，照片會被送到教團總部，然後依麻原喜好挑選，麻原彰晃特別喜歡膚白、長髮\[87\]的未成年[處女](../Page/處女.md "wikilink")，入選者將直接由麻原本人面試，並進行「做愛測驗」，若符合資格而得到麻原寵幸，便能坐享許多特權，其中有相當美貌的一些女子日後成為[模特兒](../Page/模特兒.md "wikilink")\[88\]，還有人離開教團後，成為[AV女優](../Page/AV女優.md "wikilink")\[89\]\[90\]。麻原的縱慾「歡喜法」\[91\]，被信徒尊稱為「尊師的性行為」\[92\]。

麻原彰晃宣稱這其實是一種犧牲\[93\]，他曾經說：「這是為了讓少女們得以提升到至高境界，我作為最終解脫者，只好勉為其難用旁門左道為她們進行宗教儀式。」\[94\]麻原彰晃所謂的儀式，其實就是變態的性交。麻原彰晃與情婦們性交時，除了[體內射精的情況下](../Page/體內射精.md "wikilink")，情婦們還必須含著其[精液](../Page/精液.md "wikilink")\[95\]，並以口對口[接吻互傳](../Page/接吻.md "wikilink")、飲用\[96\]\[97\]，說這就像飲用[基督的](../Page/基督.md "wikilink")[寶血一樣](../Page/寶血.md "wikilink")。麻原還效法[俄國](../Page/俄國.md "wikilink")[顛僧](../Page/顛僧.md "wikilink")[拉斯普丁](../Page/拉斯普丁.md "wikilink")\[98\]，將性交對象的陰毛剃下，用瓶子蒐集再貼上姓名\[99\]\[100\]\[101\]，麻原被捕的時候，[警方就曾沒收了](../Page/警方.md "wikilink")100瓶女性陰毛\[102\]\[103\]，心理非常變態。

麻原彰晃除了吸收女教徒作為[性奴外](../Page/性奴.md "wikilink")，更用心斂財，他高價販賣自己的頭髮、鬍鬚、指甲、血液、精液、[淋巴細胞](../Page/淋巴.md "wikilink")，甚至是洗澡水，並聲稱服用後可以獲得[解脫](../Page/解脫.md "wikilink")，有大批信徒買單\[104\]\[105\]\[106\]，麻原的洗澡水煮菜每200毫升要價2萬日圓\[107\]，含有他微量血液的食物則要價100萬日圓\[108\]，甚至連他的頭髮、指甲都能被當成聖物販賣\[109\]\[110\]，頭髮每根1,000日圓，有時還會被分配到麻原的陰毛\[111\]，麻原坐駕的名車無數，有[賓士](../Page/賓士.md "wikilink")、[寶馬](../Page/寶馬.md "wikilink")、[保時捷](../Page/保時捷.md "wikilink")、[法拉利](../Page/法拉利.md "wikilink")、[麥拉倫](../Page/麥拉倫.md "wikilink")、[藍寶堅尼](../Page/藍寶堅尼.md "wikilink")、[瑪莎拉蒂](../Page/瑪莎拉蒂.md "wikilink")、[奧斯頓馬丁等](../Page/奧斯頓馬丁.md "wikilink")，他大多使用[傀儡的名義登記](../Page/傀儡.md "wikilink")，最愛的是一輛白色的[勞斯萊斯](../Page/勞斯萊斯.md "wikilink")。最後麻原把許多信徒的全部財產剝削，其激烈的言論如「不論是哭是喊，都要全部搶下來」、「剝奪身外之物，是積累功德的偉大事業」、「為幫助靈魂的飛躍而剝奪所有」等\[112\]。他還宣稱得到[天啟](../Page/天啟.md "wikilink")、[神示](../Page/神示.md "wikilink")，[耶和華告诉他](../Page/耶和華.md "wikilink")《[圣经](../Page/圣经.md "wikilink")》[启示录的](../Page/启示录.md "wikilink")[異象即將出現](../Page/異象.md "wikilink")，信徒要出家并将全部财产布施给该教，方可获救\[113\]。據1995年11月[警視廳關於該教資產的調查報告](../Page/警視廳.md "wikilink")，奧姆真理教信徒[布施金額達一百幾十億日圓](../Page/布施.md "wikilink")，[不動產實際價值約為](../Page/不動產.md "wikilink")30億日圓，從1989年至1995年，總資產在短短6年內擴大了250倍，這些金錢成為[恐怖攻擊的資本](../Page/恐怖攻擊.md "wikilink")。甚至麻原被捕時，床上枕頭底下還藏著1000萬日圓現金\[114\]\[115\]。

麻原彰晃種種惡行逐漸導致信眾反感而退出教團，麻原主張「絕對不許[叛教](../Page/叛教.md "wikilink")」，設置了單人牢房，把對教團有疑問的信徒關進單人牢房內，播放麻原的[講道錄音](../Page/講道.md "wikilink")，直到該信徒崩潰為止。凡是得知有想要[退教的](../Page/退教.md "wikilink")，麻原還會唆使教眾加以痛毆、虐打、電擊，甚至[肉刑或謀殺](../Page/肉刑.md "wikilink")，不少有意打退堂鼓的信徒（甚至親人）遭虐殺或「[被失蹤](../Page/被失蹤.md "wikilink")」，甚至強行注射[麻醉劑](../Page/麻醉劑.md "wikilink")、[興奮劑](../Page/興奮劑.md "wikilink")，或拷打致死。包括在1989年欲退教而遭[絞死的田口修二](../Page/絞死.md "wikilink")，還有勸阻妹妹捐出全部身家的仮谷清志等\[116\]。隨著社會輿論的反感，麻原開始強調教團內部有[警方](../Page/警方.md "wikilink")[臥底潛伏](../Page/臥底.md "wikilink")，所以要求信徒互相監視、告密，麻原並開始試圖[思想控制](../Page/思想控制.md "wikilink")，麻原命令他的「治療省大臣」[林郁夫醫生開發了一種](../Page/林郁夫.md "wikilink")「吐實藥」[硫噴妥鈉](../Page/硫噴妥鈉.md "wikilink")，硫噴妥鈉的作用，是使一個人[自我意識減低](../Page/自我意識.md "wikilink")，得到放鬆，所以很難說謊，麻原於是命令幹部們，將懷疑的信徒綁來，使用「吐實藥」，來測驗其忠誠度。麻原變本加厲，為了自保，也對信徒使用電擊。麻原也曾經將一名女信徒綁來測試，女信徒使用「吐實藥」後，自認是「被[綁架來的](../Page/綁架.md "wikilink")」，於是被連續三次100瓦的電擊，女信徒回答「不知道為甚麼在這」\[117\]，經過這樣的虐待後，許多受過電擊的被害者因而[腦損傷](../Page/腦損傷.md "wikilink")，有的[失憶](../Page/失憶.md "wikilink")，有的變成[閱讀障礙或](../Page/閱讀障礙.md "wikilink")[文盲](../Page/文盲.md "wikilink")\[118\]。

### 恐怖活動

1989年11月4日，曾打擊過[統一教的](../Page/統一教.md "wikilink")「反邪教律师」[坂本堤](../Page/坂本堤.md "wikilink")（33歲）接受受害信徒的投訴\[119\]，意圖為信徒主持正義，坂本堤質疑麻原是邪教神棍，因麻原彰晃宣稱自己的血液具備某種神奇力量，坂本堤於是說服了麻原提供自己的血液樣本做測試，結果麻原彰晃與一般人的血液「一模一樣」，毫無特別之處，麻原彰晃惱羞成怒。當時坂本堤打算在[TBS電視揭弊](../Page/TBS電視台.md "wikilink")，[TBS電視臺的員工將此事洩漏給麻原彰晃](../Page/TBS錄影帶問題.md "wikilink")，於是麻原彰晃稱「用妨礙『真理』攫取的金錢生活，也算是[惡業](../Page/業.md "wikilink")」的罪名，命令[村井秀夫](../Page/村井秀夫.md "wikilink")、、[岡崎一明](../Page/岡崎一明.md "wikilink")、、和等六名教內幹部[將坂本堤一家三口滅門](../Page/坂本堤律师一家杀害事件.md "wikilink")\[120\]\[121\]，他們闖入坂本堤的家，用鐵錘擊傷坂本堤，然後痛毆他的妻子坂本都子（29歲），並把其愛子坂本龍彥（1歲2個月）注射[氯化鉀毒死](../Page/氯化鉀.md "wikilink")。之後，他們相繼對重傷的坂本夫婦注射氯化鉀，最後把坂本夫婦[絞死](../Page/絞死.md "wikilink")\[122\]。

1990年2月[日本众议院的选举中](../Page/日本众议院.md "wikilink")，麻原彰晃组建[真理党](../Page/真理党_\(日本\).md "wikilink")，派出24名核心幹部，连同自己在内，共25人参加[此次选举](../Page/第39屆日本眾議院議員總選舉.md "wikilink")，尽管花费了两亿多日圓之多，麻原彰晃本人仅获得1,783张选票，其它24人也全部落选\[123\]。麻原彰晃经过这次國會重挫之后，认为用合法手段难以达到其政治目的，于是走上了与政府对抗，以[武力夺取政权的道路](../Page/武力夺取政权.md "wikilink")，欲藉由[恐怖攻擊殺害](../Page/恐怖攻擊.md "wikilink")「[天皇](../Page/天皇.md "wikilink")、[宰相](../Page/宰相.md "wikilink")、[閣員](../Page/閣員.md "wikilink")、[國會議員及廣大的](../Page/國會議員.md "wikilink")[東京民眾](../Page/東京.md "wikilink")\[124\]\[125\]\[126\]」\[127\]定1991年為「救濟元年」（教團內部以此作[年號使用](../Page/年號.md "wikilink")），一方面在名牌大学、高科技企业、[日本自卫队及政府中发展信徒](../Page/日本自卫队.md "wikilink")，其中不少是优秀的医生、化学家、物理学家和电脑奇才，并在自卫队中安插[间谍](../Page/间谍.md "wikilink")；一方面大量购置武器，组织[武装力量](../Page/武装力量.md "wikilink")，研制[沙林毒气](../Page/沙林毒气.md "wikilink")，1992年，麻原带领40名成员赴[剛果民主共和國](../Page/剛果民主共和國.md "wikilink")（[扎伊尔](../Page/扎伊尔.md "wikilink")），希望获得[伊波拉病毒作为大屠杀的工具](../Page/伊波拉病毒.md "wikilink")，但最后并未成功\[128\]。

1994年，奥姆真理教开始实施“”（后改名“**武装爱教突击队**”），公然培养士兵，准备与国家权力机关对抗。其教内的科技省为教团装备1,000支[步枪和](../Page/步枪.md "wikilink")100万发子弹，作为以後發動[暴動](../Page/暴動.md "wikilink")、夺取政权的主力。这支部队的主要成员是奥姆真理教的[出家修行信徒](../Page/出家.md "wikilink")。麻原利用在[俄罗斯传教的关系](../Page/俄罗斯.md "wikilink")，借用[俄军信徒](../Page/俄军.md "wikilink")，设计了所谓“[军训旅游](../Page/军训.md "wikilink")”，安排日本信徒到俄罗斯[特种部队受训](../Page/特种部队.md "wikilink")，以使其具备戰爭的能力\[129\]。奥姆真理教甚至从[前苏联买回了一架苏制](../Page/前苏联.md "wikilink")[Mi-17](../Page/Mi-17.md "wikilink")[武装直升机和一台军用检测](../Page/武装直升机.md "wikilink")[芥子气的仪器](../Page/芥子气.md "wikilink")，开设地下[兵工厂](../Page/兵工厂.md "wikilink")\[130\]\[131\]，計畫在1995年11月實施「十一月戰爭」，在[國會開幕式上](../Page/參議院_\(日本\)#国会开幕式.md "wikilink")，使用從前蘇聯購入的軍用直升機在東京上空散播[沙林毒氣](../Page/沙林毒氣.md "wikilink")、[炭疽桿菌](../Page/炭疽桿菌.md "wikilink")、[肉毒桿菌和](../Page/肉毒桿菌.md "wikilink")[VX神經毒劑等有毒氣體](../Page/VX神經毒劑.md "wikilink")\[132\]，乘著日本陷入混亂之際，利用日本的武器攻擊外國，引發[美國](../Page/美國.md "wikilink")、[俄國](../Page/俄國.md "wikilink")、[北韓之間的](../Page/北韓.md "wikilink")[核戰爭](../Page/核戰爭.md "wikilink")\[133\]，然後教團便可統治處於[無政府狀態的日本](../Page/無政府.md "wikilink")\[134\]，創造一個新的[日本帝國](../Page/日本帝國.md "wikilink")，也就是[真理國](../Page/真理國.md "wikilink")，最終統治全世界\[135\]\[136\]。

1994年2月，麻原率领奥姆真理教内[村井秀夫](../Page/村井秀夫.md "wikilink")、[井上嘉浩](../Page/井上嘉浩.md "wikilink")、、、、等重要人员访问中国。麻原非常崇拜[和尚出身](../Page/和尚.md "wikilink")、参加[明教夺得天下的](../Page/明教.md "wikilink")[明太祖](../Page/明太祖.md "wikilink")，所以自称[明太祖转世](../Page/明太祖.md "wikilink")，并且前往[明太祖陵参拜](../Page/明太祖陵.md "wikilink")。麻原彰晃在[中国说](../Page/中国.md "wikilink")：“1997年，我将成为[日本之王](../Page/日本之王.md "wikilink")；到2003年，世界大部分地区都将纳入奥姆真理教的势力范围，仇视真理的人必须尽早杀掉。”\[137\]\[138\]6月27日，麻原發動[松本沙林毒气事件](../Page/松本沙林毒气事件.md "wikilink")，造成8人死亡與至少600人受傷\[139\]\[140\]\[141\]；更在翌年（1995年）3月20日發動[东京地铁沙林毒气事件](../Page/东京地铁沙林毒气事件.md "wikilink")\[142\]。

1995年5月16日，麻原被认定为“地铁沙林事件”等案件的嫌疑人，於[山梨縣的道場遭逮捕](../Page/山梨縣.md "wikilink")\[143\]\[144\]，被捕當日，麻原還指使教徒製造了[東京都廳包裹炸彈事件](../Page/東京都廳包裹炸彈事件.md "wikilink")，寄出一份[黑索金炸藥的](../Page/黑索金.md "wikilink")[郵包炸彈到時任](../Page/郵包炸彈.md "wikilink")[東京都知事](../Page/東京都知事.md "wikilink")[青島幸男的辦公室](../Page/青島幸男.md "wikilink")，刺殺青島幸男，結果在東京都廳秘書室發生爆炸，造成一名職員受傷被截肢\[145\]。

### 入獄

麻原彰晃被關押在[東京監獄的獨居房](../Page/東京監獄.md "wikilink")，展現出精神病的樣子，一直胡言亂語，使用混雜[英語的](../Page/英語.md "wikilink")[日語](../Page/日語.md "wikilink")，與許多怪異的聲音，甚至食用自己的排泄物，所以獄方設有專人照料他，並每天幫他洗澡\[146\]。後來在审判中，他一直展現出[精神病的樣子](../Page/精神病.md "wikilink")，或者强调一系列事件都跟自己無關，自己并未参与或下指示，都是弟子自作主张，他頂多算是督導不週\[147\]\[148\]。他可自行用餐散步，需人協助沐浴，偶爾會自言自語，但被通知有訪客時，他從不答腔\[149\]。

2004年2月27日，麻原被被[東京地方法院一审判处](../Page/東京地方裁判所.md "wikilink")[死刑](../Page/死刑.md "wikilink")；同月30日：辯護團向[東京高等法院所提出程序異議](../Page/東京高等裁判所.md "wikilink")，再遭駁回；2006年9月15日[日本最高法院駁回特別抗告](../Page/日本最高法院.md "wikilink")，死刑定讞，但由于同案犯[高桥克也仍然在逃](../Page/高桥克也.md "wikilink")，死刑迟迟无法执行。高桥2012年被捕，2018年1月[无期徒刑定谳](../Page/无期徒刑.md "wikilink")。奥姆真理教事件，因而全案结案\[150\]。雖然麻原一直展現自己患有[精神病的樣子](../Page/精神病.md "wikilink")，但直到2017年5月醫師仍然鑑定麻原精神正常\[151\]。

### 伏法

2018年7月6日，奉[法務大臣](../Page/法務大臣.md "wikilink")[上川陽子批准](../Page/上川陽子.md "wikilink")，执行麻原的死刑\[152\]。

麻原於7月6日早上七時醒來後，用餐完畢，刑務官才告知他今日即將執行[絞刑](../Page/絞刑.md "wikilink")，麻原彰晃選擇不需要臨終教誨（臨終祈禱），並向刑務官表示由四女兒領取遺骨、遺物，刑務官為了謹慎，還向麻原確認了一次，最終在設有小[佛龕的前室蒙眼與戴上手銬](../Page/佛龕.md "wikilink")，然後在絞刑室伏法\[153\]。麻原彰晃死後，[安倍內閣](../Page/安倍內閣.md "wikilink")[官房長官](../Page/內閣官房長官.md "wikilink")[菅義偉表示](../Page/菅義偉.md "wikilink")，已指示警察當局作好萬全因應，避免信徒自殺「[殉教](../Page/殉教.md "wikilink")」或報復\[154\]。

9日，東京看守所在[東京都](../Page/東京都.md "wikilink")[府中把麻原的屍體](../Page/府中.md "wikilink")[火化](../Page/火化.md "wikilink")\[155\]，並依照麻原的遺願，將骨灰發交四女兒松本聡香（[化名](../Page/化名.md "wikilink")），但松本聡香以持有骨灰恐帶來生命危險為由拒絕，希望由日本政府保管\[156\]，但麻原的其他親友爭搶他的遺骨，妻子、次女松本宇未（化名）、三女松本麗華、長男等人曾聯名上書法務大臣上川陽子，希望領回麻原遺骨\[157\]。

11日，松本聡香改變心意，請律師瀧本太郎代發聲明表示，願意接受麻原遺願，收下骨灰，但打算比照[賓拉登之死](../Page/賓拉登之死.md "wikilink")，想[海葬在](../Page/海葬.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")，避免[土葬或安置](../Page/土葬.md "wikilink")[靈骨塔帶來其他問題](../Page/靈骨塔.md "wikilink")，如墓地變成信徒[聖地等](../Page/聖地.md "wikilink")，也避免邪教[復辟](../Page/復辟.md "wikilink")\[158\]。瀧本表示，如果將骨灰撒入太平洋海葬，寬廣無際的太平洋，不可能被定為特定的「聖地」，另外，還有顧慮松本聡香的人身安全，希望政府能協助進行海葬一事\[159\]。

### 教團改組

麻原[入獄後](../Page/入獄.md "wikilink")，“奥姆真理教”改稱「阿雷夫教」\[160\]（[Aleph](../Page/Aleph.md "wikilink")，，即[希伯來字母第一個字母](../Page/希伯來字母.md "wikilink")「א‎」）\[161\]。“阿雷夫教”現在分裂為「阿雷夫教」、「光之輪」與「山田集團」，據日本[情治單位統計資料](../Page/情治.md "wikilink")，阿雷夫等三個組織在日本，有35個據點，信徒約1,650人，在俄羅斯也有460名追隨者\[162\]\[163\]。

## 後代

麻原彰晃與其妻共有6名婚生子女，由於麻原有高達百人以上的女信徒情婦，使得他的私生子女數量很難計算，他的女兒松本聡香稱他至少有15名私生子女，他承認的共有12名子女\[164\]。

  - 長女：1978年生
  - 次女：（，假名），1981年生
  - 三女：（），又作「松本里香」，1983年4月生，在2004年3月、4月分別被[和光大學和](../Page/和光大學.md "wikilink")[文教大學拒絕入學](../Page/文教大學.md "wikilink")，原因都是怕其父的身份會影響其他學生的情緒，她只好提出訴訟，並在贏得判決後，進入文教大學就讀，順利畢業\[165\]\[166\]。
  - 四女：（，假名），1989年生。她從未喊過麻原彰晃一聲父親，從出生起就必須以「尊師」稱呼麻原。「尊師」不但逼迫她吃下混有瓷器碎片的煎蛋，更會在寒冬中逼她穿上單薄的衣物在戶外站好幾個小時，甚至是逼迫她泡在熱水中，直到失去意識。麻原彰晃等人因東京地鐵沙林毒氣事件被逮後，松本聡香轉而交由其他教徒扶養，那時她才不到6歲，她的受虐，在父母被逮後仍然沒有改善\[167\]\[168\]。
  - 長男：1992年生。
  - 次男：1994年生，考入[埼玉縣](../Page/埼玉縣.md "wikilink")[春日部市](../Page/春日部市.md "wikilink")，但被拒绝入学。2006年4月7日，他在东京地方裁判所对校方提起求偿5000万日元的损害赔偿诉讼\[169\]。

## 脚注

<div class="references-small">

</div>

## 参考资料

## 參見

  - [奥姆真理教](../Page/奥姆真理教.md "wikilink")
  - [奧姆真理教的顛覆國家計畫](../Page/奧姆真理教的顛覆國家計畫.md "wikilink")
  - [東京地鐵沙林毒氣事件](../Page/東京地鐵沙林毒氣事件.md "wikilink")

## 外部链接

  - [The doomsday guru Shoko Asahara and the XIV. Dalai
    Lama](http://www.trimondi.de/SDLE/Part-2-13.htm)

  -
  -
[Category:奧姆真理教人物](../Category/奧姆真理教人物.md "wikilink")
[Category:日本殺人罪罪犯](../Category/日本殺人罪罪犯.md "wikilink")
[Category:日本思想家](../Category/日本思想家.md "wikilink")
[Category:日本武器犯罪罪犯](../Category/日本武器犯罪罪犯.md "wikilink")
[Category:日本籍恐怖分子](../Category/日本籍恐怖分子.md "wikilink")
[Category:熊本縣出身人物](../Category/熊本縣出身人物.md "wikilink")
[Category:新興宗教創始人](../Category/新興宗教創始人.md "wikilink")
[Category:日本企业家](../Category/日本企业家.md "wikilink")
[Category:日本盲人](../Category/日本盲人.md "wikilink")
[Category:暗殺未遂倖存者](../Category/暗殺未遂倖存者.md "wikilink")
[Category:被日本处决的死刑犯](../Category/被日本处决的死刑犯.md "wikilink")
[Category:日本宗教人物](../Category/日本宗教人物.md "wikilink")
[Category:日本作家](../Category/日本作家.md "wikilink")

1.

2.

3.

4.

5.
6.

7.
8.
9.

10.
11.
12.
13.

14.
15.

16. [東京沙林毒氣恐怖攻擊 震驚全球今終伏法](https://www.nownews.com/news/20180706/2784300)

17.

18.
19.

20.

21. [](https://mainichi.jp/articles/20180706/k00/00e/040/198000c) 每日新闻
    2018年7月6日

22.
23.

24.
25.
26.
27.
28.
29.

30.
31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41. [早川紀代秀](../Page/早川紀代秀.md "wikilink")、[川村邦光](../Page/川村邦光.md "wikilink")『私にとってオウムとは何だったのか』
    p.120

42.
43.
44.
45.

46.
47. 高山文彦『麻原彰晃の誕生』文藝春秋〈文春新書〉、2006年2月 pp.149-150。ISBN 978-4166604920

48. 『オウム真理教事件 - 宗教者・科学者・哲学者からの発言 (仏教・別冊)』、1996年、pp.4-5

49.
50. [日本奧姆真理教教主麻原彰晃被執行死刑](https://cn.nytimes.com/asia-pacific/20180706/japan-cult-execute-sarin/zh-hant/)

51. [抹黑達賴
    稱與麻原「非常親密](https://tw.appledaily.com/sports/daily/20080419/30467491)

52.

53.

54.

55. [東京沙林毒氣恐怖攻擊震驚全球 今終伏法](http://globalnewstv.com.tw/201807/28787/)

56.

57.

58. [教義就是要屠殺？5大邪教組織事件簿](http://tw.mensuno.asia/content/%E6%95%99%E7%BE%A9%E5%B0%B1%E6%98%AF%E8%A6%81%E5%B1%A0%E6%AE%BA%EF%BC%9F5%E5%A4%A7%E9%82%AA%E6%95%99%E7%B5%84%E7%B9%94%E4%BA%8B%E4%BB%B6%E7%B0%BF)

59.
60.
61.
62.
63.

64.
65.
66.
67.

68.
69.
70.
71.

72.
73.
74.
75.
76.
77.

78.
79.
80.
81.
82.

83.

84.
85.
86.
87.
88.
89.
90.
91.
92.

93.
94.
95.
96.
97.
98.
99.
100.
101.
102.
103.
104.
105.

106.

107.
108.
109.
110.
111.
112.

113.
114.
115. [奥姆真理教：肆无忌惮攫取金钱](http://www.kaiwind.com/whsy/201301/t169402.htm)

116.
117.
118.
119.
120.
121.

122. [日本最惡邪教 放毒氣殺律師
     嬰兒也不放過](http://tube.chinatimes.com/20160710002921-261412)

123.
124.
125. [今天日本一次处死了7个人，里面有一个可怕的名字](https://news.sina.cn/gj/2018-07-06/detail-ihexfcvk6187011.d.html?vt=4&pos=9&cid=56262)

126. [那个躲过死刑12年的日本邪教头子今天终于被执行死刑](https://www.52hrtt.com/tw/n/w/info/G1530690583262)

127.
128. [13死6千傷
     日本戰後最恐怖 1995東京沙林毒氣襲擊](http://www.chinatimes.com/realtimenews/20180706001514-260408)

129.
130.
131.
132.
133.
134.
135.
136. 降幡『オウム法廷 グルのしもべたち』下巻、朝日新聞社、1998年、86頁。ISBN 978-4-02-261224-3。

137.
138.
139.
140.
141. [独家揭秘沙林：叙利亚化武袭击中的毒气之王](http://news.ifeng.com/a/20170407/50900888_0.shtml)

142.
143.
144. [日本对邪教“杀人狂魔”麻原彰晃执行死刑](http://news.sina.com.cn/w/2018-07-06/doc-ihexfcvk2726321.shtml)

145. [地鐵毒氣奪13命！奧姆真理教「奔跑炸彈女」　逆轉判無罪](https://www.ettoday.net/news/20171228/1081122.htm#ixzz5KYDgwdGU)

146.
147.
148. [日本奥姆真理教原教主麻原彰晃等７人被执行死刑](http://www.xinhuanet.com/world/2018-07/06/c_1123088687.htm)

149.
150. [](https://mainichi.jp/articles/20180120/k00/00m/040/001000c)

151.
152. [](https://www3.nhk.or.jp/news/html/20180706/k10011513621000.html)
     NHK 2018年7月6日

153. [【關鍵時刻動畫】麻原彰晃死刑伏法前　最後時刻細節曝光](https://tw.appledaily.com/new/realtime/20180713/1390684/)

154. [奧姆真理教後繼團體信徒逾二千人
     日政府慎防報復](https://udn.com/news/story/6809/3237606?from=udn-ch1_breaknews-1-cate5-news)

155.

156.
157.

158.

159.
160.

161.
162.
163.
164.

165.
166.
167.
168.
169.