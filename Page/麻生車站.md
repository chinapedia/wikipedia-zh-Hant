**麻生車站**（）是一位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")[北區北](../Page/北區_\(札幌市\).md "wikilink")40条西5丁目，隸屬於[札幌市交通局的](../Page/札幌市交通局.md "wikilink")[地下鐵車站](../Page/鐵路車站.md "wikilink")。麻生車站是[札幌市營地下鐵南北線的北端起站](../Page/札幌市營地下鐵南北線.md "wikilink")，車站編號N01。

經常被讀作「」，但正式站名與車站周邊的地區名讀法為「」。

## 車站構造

麻生車站是一個配置一座[島式月台兩條乘車線的](../Page/島式月台.md "wikilink")[地下車站](../Page/地下車站.md "wikilink")，列車會於進入本站後折返。

### 月台配置

| 月台      | 路線                                                                                                                             | 目的地                                                                                                                     |
| ------- | ------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------- |
| **1、2** | [Subway_SapporoNamboku.svg](https://zh.wikipedia.org/wiki/File:Subway_SapporoNamboku.svg "fig:Subway_SapporoNamboku.svg") 南北線 | [札幌](../Page/札幌站_\(札幌市營地下鐵\).md "wikilink")、[大通](../Page/大通站_\(北海道\).md "wikilink")、[真駒內方向](../Page/真駒內站.md "wikilink") |
|         |                                                                                                                                |                                                                                                                         |

## 相鄰車站

  - [ST_Logo.svg](https://zh.wikipedia.org/wiki/File:ST_Logo.svg "fig:ST_Logo.svg")
    札幌市營地下鐵
    [Subway_SapporoNamboku.svg](https://zh.wikipedia.org/wiki/File:Subway_SapporoNamboku.svg "fig:Subway_SapporoNamboku.svg")
    南北線
      -
        **麻生（N01）**－[北34條](../Page/北34條站.md "wikilink")（N02）

[Sabu](../Category/日本鐵路車站_A.md "wikilink") [Category:北區鐵路車站
(札幌市)](../Category/北區鐵路車站_\(札幌市\).md "wikilink") [Category:南北線車站
(札幌市營地下鐵)](../Category/南北線車站_\(札幌市營地下鐵\).md "wikilink")
[Category:1978年启用的铁路车站](../Category/1978年启用的铁路车站.md "wikilink")