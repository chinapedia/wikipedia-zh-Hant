**麻阳苗族自治县**位于湖南省西部，怀化市西北部，地处[辰水流域](../Page/辰水.md "wikilink")。地理坐标为东经109°24′-110°06′、北纬27°32′-28°01′。东临辰溪县，南连鹤城区和芷江县，西接贵州省铜仁市，北靠泸溪县和凤凰县。总面积1561平方千米。总人口40万人（2011年），少数民族以苗族为主，使用漢語[西南官话和](../Page/西南官话.md "wikilink")[苗語湘西方言](../Page/苗語湘西方言.md "wikilink")\[1\]。1988年10月31日改“麻阳县”置麻阳苗族自治县，[县治](../Page/县治.md "wikilink")[高村镇](../Page/高村镇.md "wikilink")。

麻阳是[平江起义领导人之一](../Page/平江起义.md "wikilink")、中华人民共和国第一任铁道部长、全国政协副主席[滕代远的家乡](../Page/滕代远.md "wikilink")。

## 行政区划

  - 6[镇](../Page/镇.md "wikilink")：[高村镇](../Page/高村镇_\(麻阳县\).md "wikilink")、锦和镇、岩门镇
    、江口墟镇、兰里镇和吕家坪镇；
  - 18[乡](../Page/乡.md "wikilink")：郭公坪乡、长潭乡、拖冲乡、尧市乡、文昌阁乡、大桥江乡、舒家村乡、隆家堡乡、谭家寨乡、石羊哨乡、板栗树乡、谷达坡乡、高村乡、兰村乡、栗坪乡、绿溪口乡、和坪溪乡、黄桑乡。

## 地理

麻阳地处[湖南](../Page/湖南.md "wikilink")、[贵州二省交界地](../Page/贵州.md "wikilink")。相邻[县级政区有](../Page/县.md "wikilink")，东临湖南[辰溪县](../Page/辰溪.md "wikilink")，南连怀化[市辖区](../Page/市辖区.md "wikilink")[鹤城和](../Page/鹤城区.md "wikilink")[芷江侗族自治县](../Page/芷江.md "wikilink")，西接贵州[铜仁市](../Page/铜仁市.md "wikilink")，北靠湖南[凤凰县和](../Page/凤凰县.md "wikilink")[泸溪县](../Page/泸溪.md "wikilink")。

县城距怀化市59公里，距大兴、芷江机场分别为130、110公里，距旅游热点城市[凤凰](../Page/凤凰县.md "wikilink")34公里，处于[怀化](../Page/怀化.md "wikilink")、[铜仁](../Page/铜仁.md "wikilink")、[吉首三大地级城市的中心点上](../Page/吉首.md "wikilink")。

## 注释

## 外部链接

  - [麻阳苗族自治县人民政府公众信息网](https://web.archive.org/web/20170606055353/http://www.mayang.gov.cn/)

[麻阳苗族自治县](../Category/麻阳苗族自治县.md "wikilink")
[县-自治县](../Category/怀化区县市.md "wikilink")
[Category:湖南省民族自治县](../Category/湖南省民族自治县.md "wikilink")
[湘](../Category/中国苗族自治县.md "wikilink")

1.