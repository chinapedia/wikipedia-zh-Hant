**黃偉聲**，現任香港[無綫電視](../Page/無綫電視.md "wikilink")[監製](../Page/監製.md "wikilink")。70年代未加入麗的電視，1981年轉投無線電視至今，曾擔任助理編導、編導\[1\]。

## 監製列表

### 電視劇（[無線電視](../Page/無線電視.md "wikilink")）

  - 1998年：《[網上有情人](../Page/網上有情人.md "wikilink")》
  - 2001年：《[婚姻乏術](../Page/婚姻乏術.md "wikilink")》
  - 2002年：《[蕭十一郎](../Page/蕭十一郎_\(2002年香港電視劇\).md "wikilink")》
    《[點指賊賊賊捉賊](../Page/點指賊賊賊捉賊.md "wikilink")》
    《[戇夫成龍](../Page/戇夫成龍.md "wikilink")》
  - 2003年：《[英雄·刀·少年](../Page/英雄·刀·少年.md "wikilink")》
  - 2004年：《[烽火奇遇結良緣](../Page/烽火奇遇結良緣.md "wikilink")》
  - 2005年：《[我師傅係黃飛鴻](../Page/我師傅係黃飛鴻.md "wikilink")》
    《[學警雄心](../Page/學警雄心.md "wikilink")》
    《[本草藥王](../Page/本草藥王.md "wikilink")》
    《[阿旺新傳](../Page/阿旺新傳.md "wikilink")》
  - 2007年：《[我外母唔係人](../Page/我外母唔係人.md "wikilink")》
    《[學警出更](../Page/學警出更.md "wikilink")》
    《[兩妻時代](../Page/兩妻時代.md "wikilink")》
  - 2008年：《[疑情別戀](../Page/疑情別戀.md "wikilink")》
  - 2009年：《[學警狙擊](../Page/學警狙擊.md "wikilink")》《[蔡鍔與小鳳仙](../Page/蔡鍔與小鳳仙.md "wikilink")》
  - 2010年：《[情人眼裏高一D](../Page/情人眼裏高一D.md "wikilink")》
    《[女王辦公室](../Page/女王辦公室.md "wikilink")》
  - 2011年：《[九江十二坊](../Page/九江十二坊.md "wikilink")》
  - 2012年：《[東西宮略](../Page/東西宮略.md "wikilink")》
  - 2013年：《[老表，你好嘢！](../Page/老表，你好嘢！_\(電視劇\).md "wikilink")》
  - 2014年：《[食為奴](../Page/食為奴.md "wikilink")》
    《[老表，你好hea！](../Page/老表，你好hea！.md "wikilink")》
  - 2015年：《[東坡家事](../Page/東坡家事.md "wikilink")》
  - 2016年：《[警犬巴打](../Page/警犬巴打.md "wikilink")》
  - 2017年：《[與諜同謀](../Page/與諜同謀.md "wikilink")》
    《[老表，畢業喇！](../Page/老表，畢業喇！.md "wikilink")》
  - 未播映：《[十二傳說](../Page/十二傳說.md "wikilink")》
    《[牛下女高音](../Page/牛下女高音.md "wikilink")》

## 常用主要演員

<small>只計在任監製時期（2010年代），含參與客串</small>

  - 5部：[王祖藍](../Page/王祖藍.md "wikilink")
  - 4部：[郭晉安](../Page/郭晉安.md "wikilink")
    、[萬綺雯](../Page/萬綺雯.md "wikilink")
    、[張繼聰](../Page/張繼聰.md "wikilink")
    、[林盛斌](../Page/林盛斌.md "wikilink")
    、[小寶](../Page/小寶.md "wikilink")
  - 3部：[鍾嘉欣](../Page/鍾嘉欣.md "wikilink")\*
    、[黃心穎](../Page/黃心穎.md "wikilink")
    、[陳山聰](../Page/陳山聰.md "wikilink")
    、[馬賽](../Page/馬賽_\(香港\).md "wikilink")\*
    、[陳明恩](../Page/陳明恩.md "wikilink")
    、[陳自瑤](../Page/陳自瑤.md "wikilink")
  - 2部：[何雁詩](../Page/何雁詩.md "wikilink")
    、[王菀之](../Page/王菀之.md "wikilink")\*
    、[朱咪咪](../Page/朱咪咪.md "wikilink")

<small>\*為已退出TVB</small>

## 參考

[Wei偉聲](../Category/黃姓.md "wikilink")
[黃](../Category/無綫電視監製.md "wikilink")

1.  [黃偉聲紅人魔術手](http://orientaldaily.on.cc/cnt/entertainment/20091009/00282_032.html)