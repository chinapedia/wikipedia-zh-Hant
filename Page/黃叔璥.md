**黃叔璥**（），[字](../Page/表字.md "wikilink")**玉圃**，[號](../Page/號.md "wikilink")**篤齋**，[清](../Page/清朝.md "wikilink")[順天府](../Page/順天府.md "wikilink")[大興縣人](../Page/大興縣.md "wikilink")（今屬[北京](../Page/北京.md "wikilink")），首任[巡臺御史](../Page/巡臺御史.md "wikilink")。其所著之《[臺海使槎錄](../Page/臺海使槎錄.md "wikilink")》、《[南征紀程](../Page/南征紀程.md "wikilink")》等書對於[閩南](../Page/閩南.md "wikilink")、[臺灣文化的研究具有重要價值](../Page/臺灣文化.md "wikilink")。兄[黃叔琳為康熙年間](../Page/黃叔琳.md "wikilink")[探花](../Page/探花.md "wikilink")。

## 生平

黃叔璥爲[康熙四十八年](../Page/康熙.md "wikilink")（1709年）己丑科[進士](../Page/進士.md "wikilink")。曾任[湖廣道](../Page/湖廣.md "wikilink")[巡按御史](../Page/巡按御史.md "wikilink")、[浙江道](../Page/浙江.md "wikilink")[巡按御史等職](../Page/巡按御史.md "wikilink")。康熙六十年（1721年）[朱一貴事件結束](../Page/朱一貴.md "wikilink")，清廷任命黃叔璥與[滿人](../Page/滿人.md "wikilink")[吳達禮共同擔任巡臺御史](../Page/吳達禮.md "wikilink")。

翌年6月，黃叔璥抵臺後，除致力與[朱一貴殘黨作戰外](../Page/朱一貴.md "wikilink")，也經常巡行各地，考察攻守險隘、海道風信、訪問原住民政權，著有《[臺海使槎錄](../Page/臺海使槎錄.md "wikilink")》（《[使臺錄](../Page/使臺錄.md "wikilink")》）8卷，分為《[赤嵌筆談](../Page/赤嵌筆談.md "wikilink")》（四卷）、《[番俗六考](../Page/番俗六考.md "wikilink")》（三卷）、《[番俗雜記](../Page/番俗雜記.md "wikilink")》（一卷）三篇。其中蕃俗六考，詳細記錄台灣的山川地勢、風土民俗。尤其對[臺灣原住民的樣貌](../Page/臺灣原住民.md "wikilink")，更是觀察入微，因此，該書為近現代考證[平埔族歷史之根基](../Page/平埔族.md "wikilink")。另著《[南征紀程](../Page/南征紀程.md "wikilink")》一書，記載來臺前自[燕京至](../Page/燕京.md "wikilink")[福建內陸的途中見聞](../Page/福建省.md "wikilink")。

## 五子登科的善报

据清朝李氏《[求己堂集](../Page/求己堂集.md "wikilink")》记载，黃叔璥的父亲[黄芳洲先生](../Page/黄芳洲.md "wikilink")，篤信[佛法](../Page/佛法.md "wikilink")、[道教](../Page/道教.md "wikilink")，康熙年间在[回阳当](../Page/回阳.md "wikilink")[教谕](../Page/教谕.md "wikilink")，与夫人一起都喜欢行善积德。他在任当官时，曾捐钱出刊刻印《[金刚经](../Page/金刚经.md "wikilink")》、《[太上感应篇](../Page/太上感应篇.md "wikilink")》、《[阴骘文广义](../Page/文昌帝君阴骘文.md "wikilink")》各经文数十部。他的夫人也随喜捐钱印赠《[玉歷寶鈔](../Page/玉歷寶鈔.md "wikilink")》一千本，广为[布施印送](../Page/布施.md "wikilink")。并且时常买鱼、鸟[放生](../Page/放生.md "wikilink")，其数成千上万。夫妇俩生有五个儿子：叔琳是康熙辛未年的探花，叔璥是康熙己丑科的进士，叔琪是康熙乙酉科的[举人](../Page/举人.md "wikilink")，叔琬是康熙己丑科进士，叔瑄是康熙癸巳科[秀才](../Page/秀才.md "wikilink")，都获得了很高的学历和功名。\[1\]

## 注释

## 參考資料

  -
## 外部連結

  - [國家圖書館
    臺灣歷史人物小傳—明清暨日據時期：黃叔璥](http://memory.ncl.edu.tw/tm_cgi/hypage.cgi?HYPAGE=toolbox_figure_detail.hpg&subject_name=%e8%87%ba%e7%81%a3%e6%ad%b7%e5%8f%b2%e4%ba%ba%e7%89%a9%e5%b0%8f%e5%82%b3%2d%2d%e6%98%8e%e6%b8%85%e6%9a%a8%e6%97%a5%e6%93%9a%e6%99%82%e6%9c%9f&subject_url=toolbox_figure.hpg&project_id=twpeop&dtd_id=15&xml_id=0000009061)
  - [黃叔璥「臺海使槎錄」主要路線推測圖](https://web.archive.org/web/20070823181003/http://thcts.ascc.net/template/sample9.asp?id=rc21)

{{-}}

[Category:清朝戶部主事](../Category/清朝戶部主事.md "wikilink")
[Category:清朝吏部員外郎](../Category/清朝吏部員外郎.md "wikilink")
[Category:巡視台灣監察御史](../Category/巡視台灣監察御史.md "wikilink")
[Category:清朝道員](../Category/清朝道員.md "wikilink")
[Category:清朝作家](../Category/清朝作家.md "wikilink")
[Category:大兴县人](../Category/大兴县人.md "wikilink")
[Shu叔璥](../Category/黃姓.md "wikilink")

1.  绘图[玉歷寶鈔劝世文](../Page/玉歷寶鈔.md "wikilink")