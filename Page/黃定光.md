**黃定光**（，），綽號「象哥」，生於[香港](../Page/香港.md "wikilink")，籍貫[廣東](../Page/廣東.md "wikilink")[東莞](../Page/東莞.md "wikilink")，[民建聯成員](../Page/民主建港協進聯盟.md "wikilink")，2004年9月在無競爭對手競逐下，循[功能界別自動當選為](../Page/功能界別.md "wikilink")[香港立法會議員](../Page/香港立法會.md "wikilink")（[進出口界](../Page/進出口界功能界別.md "wikilink")），並於同年獲特區政府頒授銅紫荊星章。2004年、2008年、2012年及2016年，自動當選功能組別進出口界立法會議員。

## 家庭背景

黃定光妻子是[香港工聯會前會長兼現任榮譽會長](../Page/香港工會聯合會.md "wikilink")[林淑儀](../Page/林淑儀.md "wikilink")。其仕途生涯中，較為人熟知的是宣稱被公認由左派人物澆上電油活活燒死的播音員[林彬並非由左派殺害致死](../Page/林彬.md "wikilink")。

黃定光的父母為黃佩球及阮祿安，兩人在[六七暴動期間](../Page/六七暴動.md "wikilink")，均為[各界鬥委會委員](../Page/港九各界同胞反對港英迫害鬥爭委員會.md "wikilink")。

黃佩球亦是國貨公司中建國貨的創辦人之一，該公司以只賣國貨為特色，已於2011年結業。

## 學歷

曾就讀香港[香島中學](../Page/香島中學.md "wikilink")（小一至中四）及[廣州市第六中學](../Page/廣州市第六中學.md "wikilink")（高中三）。

## 政途

2004年9月在無競爭對手競逐下，循進出口界[功能界別自動當選為立法會議員](../Page/功能界別.md "wikilink")，並於同年獲特區政府頒授[銅紫荊星章](../Page/銅紫荊星章.md "wikilink")。2008年及2012年，他再一次循功能界別競選連任立法會，結果再次在沒有競爭的情況下自動當選。

2010年5月，民建聯贊助[商業電台製作](../Page/商業電台.md "wikilink")《[十八仝人愛落區](../Page/十八仝人愛落區.md "wikilink")》節目，引起社會爭議，商台節目主持人[潘小濤指責商台](../Page/潘小濤.md "wikilink")「引入魔鬼」。黃定光批評潘小濤「妖言惑眾」，誹謗政黨
，違反《電台業務守則》必須持平、公正地報道及評論的原則\[1\]。及後黃又指著名廣播員[林彬並非由左派殺害致死](../Page/林彬.md "wikilink")，正因如此，一些香港市民開始認識黃定光。

2012年5月10日，[人民力量為了阻止立法會議員出缺機制的修訂](../Page/人民力量.md "wikilink")，在立法會開打拉布戰。期間黃在座位睡覺，[梁國雄以疑有](../Page/梁國雄.md "wikilink")「生命危險」为由，要求[立法會主席介入](../Page/香港立法會主席.md "wikilink")。及後，黃起身以行動顯示沒有昏迷。

2013年11月7日，黃在立法會就權力及特權法之事件發言時表示，強烈不滿近日屢遭支持[香港電視之黑衣人以無品之粗口爛話侮辱其本人及母親](../Page/香港電視.md "wikilink")，亦顯示出支持香港電視的人的低劣質素。

2015年1月30日，黃定光在立法會會議進行期間公然在座位上剪手指甲，[范國威因踢爆其不當行為後](../Page/范國威.md "wikilink")，黃惱羞成怒，在會議室外用疑似粗口（D7你）問候對方。\[2\]。

## 其他公職

保良局顧問局成員、教育統籌局進出口業技能提升計劃行業小組成員、香港中華出入口商會副會長、香港中華總商會會董、東莞同鄉總會榮譽會長。

## 註腳

## 外部連結

  - [香港立法會：黃定光議員](https://web.archive.org/web/20061215160154/http://www.legco.gov.hk/general/chinese/members/yr04-08/wtk.htm)

[Category:東莞人](../Category/東莞人.md "wikilink")
[黃](../Category/香港東莞人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:民主建港協進聯盟成員](../Category/民主建港協進聯盟成員.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")
[Ding定光](../Category/黃姓.md "wikilink")
[Category:香島中學校友](../Category/香島中學校友.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")
[Category:香港立法會議員](../Category/香港立法會議員.md "wikilink")

1.  [民建聯要立會談潘小濤立場](http://hk.apple.nextmedia.com/realtime/index.php?fuseaction=Mobile.articleWddx&iss_id=20100511&sec_id=6996647&art_id=14017700&dis_type=text)
2.  [剪指甲被踢爆
    黃定光「D7」范國威](https://hk.news.yahoo.com/%E5%89%AA%E6%8C%87%E7%94%B2%E8%A2%AB%E8%B8%A2%E7%88%86-%E9%BB%83%E5%AE%9A%E5%85%89-d7-%E8%8C%83%E5%9C%8B%E5%A8%81-215515991.html)