**黃崇仁**（[英語](../Page/英語.md "wikilink")：Frank
Huang，），[新北市](../Page/新北市.md "wikilink")[淡水區人](../Page/淡水區.md "wikilink")，[力捷電腦的創辦人](../Page/力捷電腦.md "wikilink")，也是[力晶科技的創始人暨執行長](../Page/力晶科技.md "wikilink")，前任[台北市電腦商業同業公會理事長](../Page/台北市電腦商業同業公會.md "wikilink")。

## 生平

黃崇仁的父親黃當時，出身[淡水](../Page/淡水區.md "wikilink")[竹圍](../Page/竹圍.md "wikilink")，曾就讀[日本](../Page/日本.md "wikilink")[東京帝國大學醫科](../Page/東京帝國大學.md "wikilink")，是一位醫師，曾任教於[台大醫學院](../Page/台大醫學院.md "wikilink")。其母[許碧瑜](../Page/許碧瑜.md "wikilink")，為淡水富商[許丙之女](../Page/許丙.md "wikilink")。黃崇仁在家排行第二，其姐[黃毓秀](../Page/黃毓秀.md "wikilink")。

黃崇仁於10歲開始學習繪畫，師從[廖繼春](../Page/廖繼春.md "wikilink")。

1987年時創設力捷電腦，當時已38歲的黃崇仁放下教職，轉向從商。

黃崇仁為了[旺宏電子的經營權大戰](../Page/旺宏電子.md "wikilink")，與旺宏電子董事長[吳敏求爆發激烈的對戰](../Page/吳敏求.md "wikilink")，也因為力晶[內線交易案遭到警方的調查](../Page/內線交易.md "wikilink")，嗣經檢察官諭知新台幣1,000萬元交保。

## 家庭

外祖父是[許丙](../Page/許丙.md "wikilink")，父親[黃當時是知名醫師](../Page/黃當時.md "wikilink")。

前藝人[-{于}-素珊](../Page/于素珊.md "wikilink")（藝名[-{于}-珊](../Page/于珊.md "wikilink")）是他的妻子。

## 參見

  - [力晶半導體](../Page/力晶半導體.md "wikilink")

[Category:淡水人](../Category/淡水人.md "wikilink")
[Category:臺北醫學大學教授](../Category/臺北醫學大學教授.md "wikilink")
[Category:台灣企業家](../Category/台灣企業家.md "wikilink")
[Category:黄姓](../Category/黄姓.md "wikilink")
[Category:國立臺灣大學物理學系系友](../Category/國立臺灣大學物理學系系友.md "wikilink")
[Category:紐約市立大學校友](../Category/紐約市立大學校友.md "wikilink")