**黃志祥**（****，），[香港](../Page/香港.md "wikilink")[地產發展商](../Page/地產發展商.md "wikilink")[信和置業董事局主席](../Page/信和置業.md "wikilink")、[律師](../Page/律師.md "wikilink")，信和置業創辦人[黃廷方長子](../Page/黃廷方.md "wikilink")。他籍貫[中國](../Page/中國.md "wikilink")[福建](../Page/福建.md "wikilink")[莆田](../Page/莆田.md "wikilink")[江口鎮](../Page/江口鎮_\(莆田市\).md "wikilink")。

## 生平

1970年代，黃廷方父子開始進軍香港地產，並在1972年將「信和地產」在[香港](../Page/香港.md "wikilink")[上市](../Page/上市公司.md "wikilink")。然而，那時香港地產業受到[中東石油危機影響而陷入低潮](../Page/第一次石油危机.md "wikilink")，但黃氏父子卻購入[尖東六片](../Page/尖東.md "wikilink")[土地](../Page/土地.md "wikilink")，興建成[尖沙咀中心](../Page/尖沙咀中心.md "wikilink")、[帝國中心](../Page/帝國中心.md "wikilink")、[好時中心](../Page/好時中心.md "wikilink")、[永安廣場](../Page/永安廣場.md "wikilink")、[南洋中心及已經易名為](../Page/南洋中心.md "wikilink")[明輝中心的尖沙咀廣場](../Page/明輝中心.md "wikilink")。黃氏父子能看出尖東的發展潛力極佳，結果為他們賺取巨額[利潤](../Page/利潤.md "wikilink")，掘出第一桶金。

1981年，黃氏父子將信和地產部分物業，以「信和置業」名義分拆在香港上市，其後成為集團地產發展的主力。\[1\]

1987年，黃志祥就以空殼公司透過自己的證券行買入了一萬張期指，看好股市。不過之後發生[1987年香港股災](../Page/1987年香港股災.md "wikilink")，香港股市停市四天，再開市時，股價已經下跌3份之2。期指亦下跌超過2,000點，一張期指虧蝕十萬港元。黃志祥的一萬張期指令他虧蝕十億港元。由於黃以空殼公司買入期指，因此他本打算將空殼公司清盤了事，不過當時的港督[衞奕信召了黃志祥到他的](../Page/衞奕信.md "wikilink")[粉嶺別墅談判](../Page/行政長官粉嶺別墅.md "wikilink")，結果為庫房收回七億五千萬港元。\[2\]

黃志祥向來有「地產超級大好友」的稱號。政府每次賣地都見到黃氏的蹤影。他對香港地產業十分樂觀，也支持[香港特區政府的](../Page/香港特區政府.md "wikilink")「[勾地](../Page/勾地.md "wikilink")」政策。\[3\]\[4\]不過，黃氏父子曾在1980年代香港地產低潮期間表現過分進取，一口氣購入多達10多幅土地，結果令信和集團當時損失估計超過10億港元，幾乎面臨[破產的局面](../Page/破產.md "wikilink")。

## 家庭狀況

黃志祥已婚，妻子為楊素瓊（楊素瓊為楊錦成的女兒），兩人共育有六名子女：[黃永光](../Page/黃永光.md "wikilink")、黃永龍、黃永耀、[黃敏華](../Page/黃敏華_\(企業家\).md "wikilink")、黃敏儀、黃敏音。

## 積極參與賽馬活動

黃志祥是[香港賽馬會的會員](../Page/香港賽馬會.md "wikilink")，名下馬匹大多數以**利多**兩字命名，包括大利多、贏不盡、基金精神、好利多、源利多、喜利多、吉利多、快利多、真利多、馬利多、信利多、正利多、博利多、豐利多、追日、年年旺、[民間智慧](../Page/民間智慧.md "wikilink")（法國及新加坡訓練）、創利多（新加坡）、中國夢（愛爾蘭）、心意傳達、羅馬精神（英國）。
其家族也繼承香港賽馬會成員。

## 相關條目

  - [黃永光](../Page/黃永光.md "wikilink")
  - [黃廷方家族](../Page/黃廷方家族.md "wikilink")
  - [黃廷方](../Page/黃廷方.md "wikilink")

## 參考

<references />

[category:海外福建人](../Page/category:海外福建人.md "wikilink")
[category:新加坡華人](../Page/category:新加坡華人.md "wikilink")
[Zhi志祥](../Page/category:黃姓.md "wikilink")

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")
[Category:福建人](../Category/福建人.md "wikilink")
[Category:新加坡企業家](../Category/新加坡企業家.md "wikilink")
[Category:新加坡億萬富豪](../Category/新加坡億萬富豪.md "wikilink")
[Category:信和集團](../Category/信和集團.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:在香港的新加坡人](../Category/在香港的新加坡人.md "wikilink")
[Category:福建企业家](../Category/福建企业家.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:第十二届全国政协委员](../Category/第十二届全国政协委员.md "wikilink")
[Category:黃廷方家族](../Category/黃廷方家族.md "wikilink")
[Category:法國藝術及文學勳章持有人](../Category/法國藝術及文學勳章持有人.md "wikilink")
[Category:律師出身的商人](../Category/律師出身的商人.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")

1.  [地產群英錄](http://www.angelfire.com/space/tokuhon/property_giant.txt)
2.
3.  [黃志祥父子兵
    競投土地](http://www.mpfinance.com/htm/Finance/20070314/News/ek1_ek1a1.htm)
4.  [黃志祥讚勾地多元化](http://www.takungpao.com/news/08/03/02/GW-871522.htm)