[新聞科1_0128_國防部長高華柱今（9）日下午率國防部主管拜會市長黃敏惠，就國防相關議題進行意見交流.jpg](https://zh.wikipedia.org/wiki/File:新聞科1_0128_國防部長高華柱今（9）日下午率國防部主管拜會市長黃敏惠，就國防相關議題進行意見交流.jpg "fig:新聞科1_0128_國防部長高華柱今（9）日下午率國防部主管拜會市長黃敏惠，就國防相關議題進行意見交流.jpg")
**黃敏惠**（），[中國國民黨籍](../Page/中國國民黨.md "wikilink")[臺灣政治人物](../Page/臺灣.md "wikilink")，現任[嘉義市市長](../Page/嘉義市市長.md "wikilink")。黃敏惠出生於[嘉義縣](../Page/嘉義縣.md "wikilink")[東石鄉](../Page/東石鄉_\(台灣\).md "wikilink")，[國立臺灣師範大學國文學系學士](../Page/國立臺灣師範大學.md "wikilink")、[國立嘉義大學](../Page/國立嘉義大學.md "wikilink")[EMBA管理研究所碩士](../Page/EMBA.md "wikilink")，曾任第三屆[國民大會代表](../Page/國民大會.md "wikilink")、第四到六屆[立法委員](../Page/立法委員.md "wikilink")（立法院教育文化、經濟能源委員會召集委員）及第七到八屆[嘉義市市長](../Page/嘉義市市長.md "wikilink")，自2008年起成為國民黨副主席，後因時任主席[朱立倫與第一副主席](../Page/朱立倫.md "wikilink")[郝龍斌辭職而代理](../Page/郝龍斌.md "wikilink")[中國國民黨主席](../Page/中國國民黨.md "wikilink")，成為中國國民黨成立百餘年來首位女性主席\[1\]，代理直至三月底新任黨主席[洪秀柱就職](../Page/洪秀柱.md "wikilink")。2018年再度代表[中國國民黨參選第十屆](../Page/中國國民黨.md "wikilink")[嘉義市市長](../Page/嘉義市市長.md "wikilink")，在泛藍嚴重分裂的情況下擊敗現任市長[涂醒哲順利當選](../Page/涂醒哲.md "wikilink")\[2\]。

黃敏惠早年曾任教於[臺北市立中山女中十餘年](../Page/臺北市立中山女子高級中學.md "wikilink")，後代表[中國國民黨參選多項公職](../Page/中國國民黨.md "wikilink")，同時也擔任嘉義青溪婦聯會主任委員、嘉義社區發展文教基董事長、[嘉義市婦女會常務理事](../Page/嘉義市.md "wikilink")、中華民國智能障礙者體育運動協會（[中華臺北特奧會](../Page/中華臺北特奧會.md "wikilink")）理事長及[國家運動訓練中心董事](../Page/國家運動訓練中心.md "wikilink")。

## 從政經歷

黃敏惠出身政治世家，早年為國文教師，於[臺北市](../Page/臺北市.md "wikilink")[中山女中任教十餘年](../Page/中山女中.md "wikilink")，父親是[嘉義市知名的前](../Page/嘉義市.md "wikilink")[臺灣省議員](../Page/臺灣省議員.md "wikilink")[黃永欽](../Page/黃永欽.md "wikilink")，任內爭取到許多經費，在地方上有「好好先生」之美稱。傳承父親黃永欽的親和力及魄力，黃敏惠的地方人脈關係在雲嘉南地區藍綠兩派都有相當重的份量，被普遍視為國民黨在臺灣中南部地區有影響力的重量級人士。

2004年黄敏惠與黨內同仁促起成立[臺灣新希望連線](../Page/臺灣新希望連線.md "wikilink")，以國民黨内的「新潮流」自詡，改革色彩濃厚。

2005年代表中國國民黨[參選嘉義市長](../Page/2005年中華民國縣市長暨縣市議員選舉.md "wikilink")，擊敗尋求連任的[民主進步黨籍市長](../Page/民主進步黨.md "wikilink")[陳麗貞](../Page/陳麗貞.md "wikilink")，成為[嘉義市升格](../Page/嘉義市.md "wikilink")[省轄市後第一位](../Page/省轄市.md "wikilink")[中國國民黨籍市長](../Page/中國國民黨.md "wikilink")。

2008年11月出任中國國民黨副主席，為六位副主席中唯一女性副主席。

2009年12月再度代表中國國民黨[參選第八屆嘉義市長](../Page/2009年中華民國縣市長暨縣市議員選舉.md "wikilink")，最後成功連任。

2014年[中國國民黨在](../Page/中國國民黨.md "wikilink")[2014年中華民國地方公職人員選舉大敗](../Page/2014年中華民國地方公職人員選舉.md "wikilink")，繼任行政院長[毛治國屬意由卸任嘉義市長黃敏惠擔任副院長一職](../Page/毛治國.md "wikilink")，不過遭婉拒。

2016年[中國國民黨在](../Page/中國國民黨.md "wikilink")[2016年中華民國總統副總統及立法委員選舉空前挫敗](../Page/2016年中華民國總統副總統及立法委員選舉.md "wikilink")，黨主席[朱立倫及敗選立委的副主席](../Page/朱立倫.md "wikilink")[郝龍斌請辭](../Page/郝龍斌.md "wikilink")，並於1月18日代理黨主席一職，成為首位女性黨主席（代理）。同月26日，託時任行政院副秘書長[徐中雄領表登記參選黨主席](../Page/徐中雄.md "wikilink")，最後獲得不足三分之一的得票，以第二高票敗給了前立法院副院長[洪秀柱](../Page/洪秀柱.md "wikilink")。

2018年，黃敏惠再度參選嘉義市市長一職。她以「拚經濟」、「勇敢向前行，嘉義一定贏」、「正派！廉能！斷黑金！」等口號做為競選主軸，提倡將嘉義市打造成台灣西部新都心。在泛藍分裂、對手抨擊「[回鍋肉](../Page/回鍋肉.md "wikilink")」及無黨公職資源下，最終於11月24日的[市長選舉中](../Page/2018年中華民國直轄市長及縣市長選舉.md "wikilink")，以2,302票的些微差距勝出選舉，擊敗尋求連任的[涂醒哲](../Page/涂醒哲.md "wikilink")\[3\]\[4\]。

## 第7、8屆市長任內政績

### 垂楊大橋

垂楊大橋於2014年8月20日完工、09月13日舉行通車典禮，全長815公尺。「垂楊大橋」啟用後，意謂著[嘉義市](../Page/嘉義市.md "wikilink")「公道一」全線通車，從市區可直通高鐵大道，平時可節省5分鐘車程，尖峰時段可節省15分車程，大幅舒緩交通壅塞問題\[5\]。

### 月影潭心藝術地景

月影潭心由國際藝術家[王文志所打造](../Page/王文志.md "wikilink")，與[蘭潭完全結合](../Page/蘭潭.md "wikilink")，每年春節創造近9萬造訪人次，並於2015年榮獲國家卓越建設奬\[6\]。

### 森林之歌與檜意森活村

檜意森活村（原[臺灣日治時期](../Page/臺灣日治時期.md "wikilink")[林務局員工宿舍群](../Page/林務局.md "wikilink")）於日治時期建造完成（年代不詳），園區內共28棟歷史建築（含[營林俱樂部](../Page/營林俱樂部.md "wikilink")，營林俱樂部大約興建於1914年間），分為高級主管的一戶建、主管及眷屬的二戶建、眷屬宿舍的四戶建以及單身宿舍的連棟建。不僅規劃錯落有致，深具[和風情緻](../Page/和風.md "wikilink")，並且在整個建築聚落中設有公共[澡堂](../Page/澡堂.md "wikilink")（湯屋）和[招待所](../Page/招待所.md "wikilink")，滿足生活機能與商業功能的雙重結合。2005年（[民國94年](../Page/民國94年.md "wikilink")）10月26日，由[嘉義市政府文化局公告為](../Page/嘉義市政府文化局.md "wikilink")[嘉義市歷史建築](../Page/嘉義市歷史建築.md "wikilink")。2008年進行大規模整建，在歷經四年與耗資四億多元後，於2012年9月29日與森林之歌共同進行啟用儀式\[7\]。

森林之歌運用了林務局提供之木材、鐵軌、黃藤及石材等素材，以高聳神木及阿里山火車鐵軌為主題，打造出這個高14公尺、長55公尺左右的大型裝置藝術品。目前連貫林森東路及忠孝路一帶的嘉義市林業觀光帶儼然成形，藉以重現嘉義「木材之都」獨特榮景。

### 還債70％

在黃敏惠擔任嘉義市長之前，嘉義市負債39億元。黃敏惠上任後進行開源節流，帶頭刪減特支費，所有首長特支費打75折；接著嘉義市通過中央社福、教育、基本設施及財政四大面向考核，近5年增加一般性補助款1億384萬，直到黃敏惠卸任前，嘉義市減少公共債務28.5億元\[8\]。

### 爭取2008年國慶煙火

2008年[中華民國國慶煙火在時任市長黃敏惠的爭取下移師到嘉義市舉辦](../Page/中華民國國慶煙火.md "wikilink")，晚間七時起在嘉義市[民權東路段](../Page/民權東路.md "wikilink")（[嘉義公園](../Page/嘉義公園.md "wikilink")）旁施放五十分鐘，由於施放地點為海拔50公尺高的山坡地，因此全嘉義市只要沒高樓遮蔽的地方通通看得到煙火施放。中華民國副總統[蕭萬長也返鄉與民眾歡慶雙十節](../Page/蕭萬長.md "wikilink")，全程的煙火秀吸引了超過25萬人參觀，嘉義市市府估計全台有兩百萬人關注這場煙火秀。該煙火於50分鐘內，施放7813枚煙火各種造型，而煙火圖騰的主題則為「全民向心力、開創新世紀」\[9\]\[10\]。

### 舉辦臺灣燈會

2010年的[臺灣燈會移師嘉義市嘉義公園](../Page/臺灣燈會.md "wikilink")（中山公園）舉行。臺灣燈會主燈主題為「福臨寶島」，取「虎」年與「福」發音相似，以金屬沖孔板雕塑外型，總高度達19.3公尺（主燈主體高度約14.8公尺，八卦基座高度約4.5公尺），主燈主體寬度約17.6公尺，總重達36公噸，造價新台幣1370萬元；四座副燈分別為位於中央噴水池的「鰲躍江海」、位於中山路北棟市府大樓附近的「瑞鳯翔飛」以及兩座位於燈會主場地的「龍戲虎祈福燈」及「麒麟獻瑞」。燈會開幕當天，人口僅27萬的嘉義市在一天之內湧入53萬人潮，為期8天的燈會則一共吸引超過400萬人次賞燈\[11\]。

## 选举

### 2004年立法委員選舉

| 2004年嘉義市選舉區立法委員選舉結果 |
| ------------------- |
| 應選2席                |
| 號次                  |
| 1                   |
| 2                   |
| 3                   |
| 4                   |

### 2005年嘉義市長選舉

| 2005年[嘉義市市長選舉結果](../Page/嘉義市市長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

### 2009年嘉義市長選舉

| 2009年[嘉義市市長選舉結果](../Page/嘉義市市長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| 3                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

### 2016年中國國民黨主席補選

| 2016年中國國民黨主席補選選舉結果 |
| ------------------ |
| 號次                 |
| 1                  |
| 2                  |
| 3                  |
| 4                  |
| **選舉人數**           |
| **投票數**            |
| **有效票**            |
| **無效票**            |
| **投票率**            |

### 2018年嘉義市長選舉

[缩略图](https://zh.wikipedia.org/wiki/File:Chiayi_provincial_2018.png "fig:缩略图")

\== 爭議 ==
2018年9月，已故的[台北市議員](../Page/台北市議員.md "wikilink")[李新的女友](../Page/李新_\(政治人物\).md "wikilink")[郭新政出面指控國民黨前立委](../Page/郭新政.md "wikilink")[羅淑蕾](../Page/羅淑蕾.md "wikilink")、黃敏惠與現任立委[黃昭順](../Page/黃昭順.md "wikilink")，指李新的死與羅、黃等人有關。還到臺北地檢署提告，直言羅淑蕾設局害死李新\[12\]\[13\]。

## 參考資料

<div style="font-size:89%;">

<references />

</div>

## 外部連結

  -
  - [黃敏惠udn部落格](http://blog.udn.com/cycgmayor)

  - [黃敏惠On Plurk](http://www.plurk.com/chiayi_mayor)

  - [黃敏惠Twitter](https://twitter.com/huangminhui)

  - [Mighty People Online -
    黃敏惠](http://mightypeople.org/politician/profile.php?id=474)

|- |colspan="3"
style="text-align:center;"|[Seal_of_Chiayi_City.svg](https://zh.wikipedia.org/wiki/File:Seal_of_Chiayi_City.svg "fig:Seal_of_Chiayi_City.svg")
**[嘉義市政府](../Page/嘉義市政府.md "wikilink")** |-

[Min敏惠](../Category/黃姓.md "wikilink") [H](../Category/東石人.md "wikilink")
[H](../Category/臺灣女性政治人物.md "wikilink")
[H](../Category/國立嘉義大學校友.md "wikilink")
[H](../Category/國立臺灣師範大學校友.md "wikilink")
[Category:臺灣高級中學教師](../Category/臺灣高級中學教師.md "wikilink")
[H](../Category/中國國民黨黨員.md "wikilink")
[Category:第3屆中華民國國民大會代表](../Category/第3屆中華民國國民大會代表.md "wikilink")
[H](../Category/第4屆中華民國立法委員.md "wikilink")
[H](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:女性市長](../Category/女性市長.md "wikilink")
[H](../Category/嘉義市市長.md "wikilink")
[H](../Category/中國國民黨副主席.md "wikilink")
[H](../Category/嘉義市人.md "wikilink")

1.
2.  [影／涂醒哲承認敗選　黃敏惠謙卑宣佈勝選](https://www.nownews.com/news/20181125/3088051/)
3.  [黃敏惠當選嘉義市長
    拚打造台灣西部新都心](https://www.cna.com.tw/news/firstnews/201811245012.aspx)，中央通訊社，2018-11-24
4.  [回鍋參選
    國民黨籍黃敏惠自行宣布當選嘉義市長](https://tw.news.yahoo.com/%E5%9C%8B%E6%B0%91%E9%BB%A8%E5%86%8D%E6%8B%BF%E4%B8%8B%E4%B8%80%E5%B8%AD-%E9%BB%83%E6%95%8F%E6%83%A0%E8%87%AA%E8%A1%8C%E5%AE%A3%E5%B8%83%E7%95%B6%E9%81%B8%E5%98%89%E7%BE%A9%E5%B8%82%E9%95%B7-121639995.html)
5.  [嘉市首座高架橋
    垂楊大橋通車](https://www.chinatimes.com/realtimenews/20140913002669-260405)
6.  [嘉市蘭潭觀光展新局　月影潭心藝術地景熱鬧啟用](https://www.chiayi.gov.tw/2015web/04_hot_news/content.aspx?id=33826)
7.  [〈中部〉花4億4年打造
    檜意森活村啟用](http://news.ltn.com.tw/news/local/paper/618961)
8.  [黃敏惠最大成就
    讓嘉義市民安居樂業](https://www.chinatimes.com/newspapers/20151106002660-260603)
9.
10. [台灣九十三年國慶煙火
    移師南部嘉義縣舉行](http://www.epochtimes.com/b5/4/9/6/n651601.htm)
11. [2010台灣燈會官方網站](http://taiwan.net.tw/2010taiwanlantern/NewsList.aspx?id=xNy%2fhlwHYEE%3d)
12. [「誰摔死了李新」羅淑蕾喊告　陳瑩分享影片籲：請大家轉發](https://www.setn.com/News.aspx?NewsID=468513)
    三立新聞網，2018年12月10日
13. [「誰摔死李新」幕後黑手擴大至13人
    網瘋問：蕭夫人是誰](https://www.chinatimes.com/realtimenews/20181214004527-260405)
    中時電子報，2015年12月14日