〈**黃昏的故鄉**〉（[白話字](../Page/白話字.md "wikilink")：；或作、等），是一首[臺語歌曲](../Page/臺語.md "wikilink")。原曲為1958年11月錄製上市，由日本人作曲、作詞、[三橋美智也所演唱](../Page/三橋美智也.md "wikilink")\[1\]，後由台灣音樂家[文夏填](../Page/文夏.md "wikilink")[台語詞後翻唱](../Page/台語.md "wikilink")。當年許多被[中華民國政府列入](../Page/中華民國政府.md "wikilink")[黑名單的台灣人二十餘年無法回台](../Page/黑名單_\(台灣\).md "wikilink")，所以在[海外台灣人的聚會中](../Page/台僑.md "wikilink")，〈黃昏的故鄉〉是最常被吟唱的歌曲\[2\]。今日，除了文夏的版本持續傳唱外，亦有許多[台語歌手翻唱此經典台語歌曲](../Page/台語流行音樂.md "wikilink")。

本曲在台灣[戒嚴與](../Page/台灣戒嚴時期.md "wikilink")[白色恐怖時期](../Page/台灣白色恐怖.md "wikilink")，與〈[望你早歸](../Page/望你早歸.md "wikilink")〉、〈[補破網](../Page/補破網.md "wikilink")〉、〈[望春風](../Page/望春風.md "wikilink")〉以及〈[媽媽請你也保重](../Page/媽媽請你也保重.md "wikilink")〉等曲並列[台獨及](../Page/台獨.md "wikilink")[黨外勢力的五大精神歌曲](../Page/黨外.md "wikilink")\[3\]，也因此使它被當年[國民黨主導的政府列為一首](../Page/國民黨.md "wikilink")[禁歌](../Page/禁歌.md "wikilink")。

## 相關條目

  - [黑名單 (台灣)](../Page/黑名單_\(台灣\).md "wikilink")
  - [台語流行音樂](../Page/台語流行音樂.md "wikilink")

## 參考與註釋

[category:台语歌曲](../Page/category:台语歌曲.md "wikilink")

[Category:日本歌曲](../Category/日本歌曲.md "wikilink")
[Category:翻唱歌曲](../Category/翻唱歌曲.md "wikilink")
[Category:演歌歌曲](../Category/演歌歌曲.md "wikilink")
[Category:臺灣禁歌](../Category/臺灣禁歌.md "wikilink")

1.  原日文歌名：「」，中文意爲「紅色夕陽的故鄉」。
2.
3.