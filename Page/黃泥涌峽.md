[Wong_Nai_Chung_Gap_2017.jpg](https://zh.wikipedia.org/wiki/File:Wong_Nai_Chung_Gap_2017.jpg "fig:Wong_Nai_Chung_Gap_2017.jpg")
[HK_WongNaiChungGapRoad.JPG](https://zh.wikipedia.org/wiki/File:HK_WongNaiChungGapRoad.JPG "fig:HK_WongNaiChungGapRoad.JPG")

**黃泥涌峽**（）是一個[香港的](../Page/香港.md "wikilink")[山坳](../Page/山坳.md "wikilink")，位於[香港島中部](../Page/香港島.md "wikilink")，[聶高信山和](../Page/聶高信山.md "wikilink")[渣甸山之間](../Page/渣甸山.md "wikilink")，[海拔](../Page/海拔.md "wikilink")250米，是5條道路的匯合處，位置相當險要，在[香港仔隧道未通車前是香港島南北岸之間的要道](../Page/香港仔隧道.md "wikilink")。

## 歷史

[West_Brigade_Headquarters_south_bunker.jpg](https://zh.wikipedia.org/wiki/File:West_Brigade_Headquarters_south_bunker.jpg "fig:West_Brigade_Headquarters_south_bunker.jpg")
[West_Brigade_Headquarters_nouth_bunker.JPG](https://zh.wikipedia.org/wiki/File:West_Brigade_Headquarters_nouth_bunker.JPG "fig:West_Brigade_Headquarters_nouth_bunker.JPG")

黃泥涌峽的命名沿自從黃泥涌峽一帶形成的[溪流所流下來的黃色泥水](../Page/溪流.md "wikilink")，故稱其為黃泥涌，後來該溪流改建成[寶靈頓運河](../Page/寶靈頓運河.md "wikilink")（鵝頸澗），現時已填成暗渠。隨著溪流而下的山峽口地方被稱為黃泥涌谷，即今天的[跑馬地](../Page/跑馬地.md "wikilink")。[明代出版的](../Page/明代.md "wikilink")《[粵大記](../Page/粵大記.md "wikilink")》，已淸楚標有黃泥涌的地名。

1930年代初期，[英國軍隊意識到](../Page/英國軍隊.md "wikilink")[日本皇軍可能會攻佔香港](../Page/日本皇軍.md "wikilink")，加上黃泥涌峽在軍事上的重要性，興建了大規模的[防禦工事](../Page/防禦工事.md "wikilink")，包括皇家炮兵第5AA[高射炮陣地](../Page/高射炮.md "wikilink")、[榴彈炮炮台及多座](../Page/榴彈炮.md "wikilink")[機槍堡等](../Page/機槍堡.md "wikilink")。1941年12月8日[香港保衛戰爆發](../Page/香港保衛戰.md "wikilink")，日本皇軍於12月18日成功搶灘香港島[北角](../Page/北角.md "wikilink")，於12月19日抵達黃泥涌峽。同早上10時30分，駐守於黃泥涌峽道旁的西旅指揮部受到日本皇軍包圍及攻擊，結果加軍司令兼任西旅旅長羅遜[准將及其僚屬全體殉職](../Page/准將.md "wikilink")，羅遜[准將成為](../Page/准將.md "wikilink")[香港保衛戰中最高級的殉職軍人](../Page/香港保衛戰.md "wikilink")\[1\]當時英國第3義勇軍步兵連第7、8及9排、少量蘇格蘭營及[加拿大溫尼伯榴彈兵部隊D連](../Page/加拿大.md "wikilink")，為了扼守此處通往香港島南部的要道及黃泥涌水塘，與日本皇軍爆發激戰。雖然英國、加拿大軍隊的頑強防守使到日本皇軍有逾600人傷亡，亦使到義勇軍第3連成為抗戰英雄，但是最終日本皇軍於12月23日成功佔領黃泥涌峽及水塘，斷絕了香港島的供水。致使英國軍隊無險可守，加上香港另外一處主要山峽[灣仔峽於](../Page/灣仔峽.md "wikilink")12月24日亦失守，惟有選擇投降，並且開始了[香港日佔時期](../Page/香港日佔時期.md "wikilink")。

## 黃泥涌水塘

[Wong_Nai_Chung_Reservoir_outlook.JPG](https://zh.wikipedia.org/wiki/File:Wong_Nai_Chung_Reservoir_outlook.JPG "fig:Wong_Nai_Chung_Reservoir_outlook.JPG")

建在黃泥涌峽的黃泥涌水塘於1899年建成，為香港的第三個[水塘](../Page/水塘.md "wikilink")。水塘有一弧形的[水壩](../Page/水壩.md "wikilink")，高50呎、長270呎。但由於水塘儲水量小（2,700萬[加侖](../Page/加侖.md "wikilink")）及維修費高昂的原因，該水塘在1978年停止運作。後改建成現時的[黃泥涌水塘公園](../Page/黃泥涌水塘公園.md "wikilink")。

## 主要建築

  - [陽明山莊](../Page/陽明山莊.md "wikilink")
  - [黃泥涌水塘公園](../Page/黃泥涌水塘公園.md "wikilink")（前黃泥涌水塘）
  - [香港網球中心](../Page/香港網球中心.md "wikilink")
  - [香港木球會](../Page/香港木球會.md "wikilink")

## 交通

### 主要道路

  - [黃泥涌峽道](../Page/黃泥涌峽道.md "wikilink")
  - [淺水灣道](../Page/淺水灣道.md "wikilink")
  - [深水灣道](../Page/深水灣道.md "wikilink")
  - [大潭水塘道](../Page/大潭水塘道.md "wikilink")
  - [布力徑](../Page/布力徑.md "wikilink")

### 公共交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{南港島綫色彩}}">█</font>[南港島綫](../Page/南港島綫.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [城巴](../Page/城巴.md "wikilink")
  - [新世界第一巴士](../Page/新世界第一巴士.md "wikilink")

## 參考資料

## 參見

  - [灣仔峽](../Page/灣仔峽.md "wikilink")
  - [黃泥涌谷](../Page/黃泥涌谷.md "wikilink")（今[跑馬地](../Page/跑馬地.md "wikilink")）
  - [大潭](../Page/大潭_\(香港\).md "wikilink")
  - [深水灣](../Page/深水灣.md "wikilink")
  - [淺水灣](../Page/淺水灣.md "wikilink")

{{-}}

[Category:香港山坳](../Category/香港山坳.md "wikilink")
[黃泥涌峽](../Category/黃泥涌峽.md "wikilink")
[Category:灣仔區](../Category/灣仔區.md "wikilink")

1.  《香港戰地指南(1941)》，高添強 著，三聯書店 出版，1995年7月，第65-72頁。ISBN 962-04-1278-8