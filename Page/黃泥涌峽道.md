**黃泥涌峽道**（**英文：**）是[香港](../Page/香港.md "wikilink")[香港島的一條道路](../Page/香港島.md "wikilink")，位於[黃泥涌峽之上](../Page/黃泥涌峽.md "wikilink")，呈東南走向。東面與[司徒拔道](../Page/司徒拔道.md "wikilink")、[寶雲道及](../Page/寶雲道.md "wikilink")[大坑道交界連接](../Page/大坑道.md "wikilink")，南面與[淺水灣道](../Page/淺水灣道.md "wikilink")、[深水灣道](../Page/深水灣道.md "wikilink")、[大潭水塘道及](../Page/大潭水塘道.md "wikilink")[布力徑交界連接](../Page/布力徑.md "wikilink")。全長1200米。在[南風隧道](../Page/南風隧道.md "wikilink")／[香港仔隧道未通車前](../Page/香港仔隧道.md "wikilink")，黃泥涌峽道是香港島東部連接南部的要道。

## 著名地點

  - [香港網球中心](../Page/香港網球中心.md "wikilink")
  - [香港木球會](../Page/香港木球會.md "wikilink")

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")
      - [南港島綫](../Page/南港島綫.md "wikilink")

| 編號     | 路線       | 備註 |
| ------ | -------- | -- |
| 港島巴士   |          |    |
| 6      | 中環（交易廣場） | ⇄  |
| 41A    | 華富（中）    | ⇄  |
| 63     | 北角碼頭     | ⇄  |
| 66     | 中環（交易廣場） | ⇄  |
| 76     | 石排灣      | ↺  |
| 港島專線小巴 |          |    |
| 5      | 香港仔中心    | ⇄  |

[Category:黃泥涌峽街道](../Category/黃泥涌峽街道.md "wikilink")