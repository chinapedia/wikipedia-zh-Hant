[Wong_Chuk_Hang_Aerial_view_2018.jpg](https://zh.wikipedia.org/wiki/File:Wong_Chuk_Hang_Aerial_view_2018.jpg "fig:Wong_Chuk_Hang_Aerial_view_2018.jpg")
[HK_Wong_Chuk_Hang_facades_黃竹坑_Wheelock_會德豐_One_Island_South_office_building_April-2012.JPG](https://zh.wikipedia.org/wiki/File:HK_Wong_Chuk_Hang_facades_黃竹坑_Wheelock_會德豐_One_Island_South_office_building_April-2012.JPG "fig:HK_Wong_Chuk_Hang_facades_黃竹坑_Wheelock_會德豐_One_Island_South_office_building_April-2012.JPG")
**黃竹坑**位於[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區_\(香港\).md "wikilink")，在[香港仔中心之東](../Page/香港仔.md "wikilink")，[深水灣之西](../Page/深水灣.md "wikilink")。目前主要是一個[住宅區及](../Page/住宅.md "wikilink")[輕工業集中地](../Page/輕工業.md "wikilink")，並有少量商業大廈和酒店，並且是香港著名的旅遊景點——[香港海洋公園所在地](../Page/香港海洋公園.md "wikilink")。

黃竹坑東部的[壽臣山一帶](../Page/壽臣山.md "wikilink")，曾經是[香港圍的所在地](../Page/香港圍.md "wikilink")，被認為是[香港地名來源之一](../Page/香港地名來源.md "wikilink")。原村的大部分已被政府拆卸，現僅存[黃竹坑新圍和小部分](../Page/黃竹坑新圍.md "wikilink")[黃竹坑舊圍](../Page/黃竹坑舊圍.md "wikilink")。

## 歷史

黃竹坑一帶早於[新石器時代便有人居住](../Page/新石器時代.md "wikilink")，現存的古蹟有[黃竹坑石刻](../Page/黃竹坑石刻.md "wikilink")。到了[明朝](../Page/明朝.md "wikilink")[嘉靖年間](../Page/嘉靖.md "wikilink")，有人在該處一帶建立[香港圍](../Page/香港圍.md "wikilink")，成為[香港島早期具規模的村落之一](../Page/香港島.md "wikilink")。而當時黃竹坑西部為一個內灣，也吸引了不少漁船停泊。

黃竹坑自1960年代開始開發，[南朗山北面山腰興建了](../Page/南朗山.md "wikilink")[政府廉租屋](../Page/政府廉租屋.md "wikilink")[黃竹坑邨](../Page/黃竹坑邨.md "wikilink")，其內灣也被填平發展工業大廈，使黃竹坑曾一度成為香港的其中一個主要[輕工業區](../Page/輕工業.md "wikilink")。但隨著1990年代大部份紛紛遷移至[中國大陸](../Page/中國大陸.md "wikilink")，製造業「日落西山」，加上現時[香港政府銳意將香港仔及其週邊地區發展成為一個](../Page/香港政府.md "wikilink")[旅遊區](../Page/旅遊.md "wikilink")，黃竹坑將會轉型成三四星級酒店為主的酒店區，部份工廠大廈會改建成酒店。

## 氣候

<div style="width:80%;">

</div>

## 商廈

  - [One Island
    South](../Page/One_Island_South.md "wikilink")（由會德豐發展，已拆售）
  - [環匯廣場](../Page/環匯廣場.md "wikilink")（由恆基兆業及協成行發展，已拆售）
  - [w50](../Page/w50_\(香港\).md "wikilink")（由新鴻基地產發展，已拆售）
  - [香葉道41號](../Page/香葉道41號.md "wikilink")（長實發展，現由[梁安琪持有](../Page/梁安琪_\(澳門\).md "wikilink")）
  - [南匯廣場A及B座](../Page/南匯廣場.md "wikilink")
  - [甄沾記大廈](../Page/甄沾記大廈.md "wikilink")

## 酒店

  - [如心南灣海景酒店](../Page/如心南灣海景酒店.md "wikilink")
  - [OVOLO HOTELS](../Page/OVOLO_HOTELS.md "wikilink")

## 名稱

[Tsing_Yi_Island_in_Yuet_Tai_Kei.png](https://zh.wikipedia.org/wiki/File:Tsing_Yi_Island_in_Yuet_Tai_Kei.png "fig:Tsing_Yi_Island_in_Yuet_Tai_Kei.png")
黃竹坑昔日有一條從[南朗山流出來的](../Page/南朗山.md "wikilink")[溪流](../Page/溪流.md "wikilink")（今[黃竹坑明渠](../Page/黃竹坑明渠.md "wikilink")），兩旁編佈黃色的[竹子](../Page/竹子.md "wikilink")，黃竹坑因而得名。[明朝](../Page/明朝.md "wikilink")[萬曆年間的](../Page/萬曆.md "wikilink")《[粵大記](../Page/粵大記.md "wikilink")》內所載的沿海圖中，已有鐵坑（黃竹坑）的紀錄。

## 著名地點

### 文娛康樂設施

黃竹坑是南區中擁有較遼闊平地的地區，因此設有不少[運動場地](../Page/運動.md "wikilink")，包括多個[足球場及](../Page/足球.md "wikilink")[籃球場及室內運動場等](../Page/籃球.md "wikilink")。而[香港仔運動場是香港島三大運動場之一](../Page/香港仔運動場.md "wikilink")，每年均舉辦不少大型活動，如[饑饉三十及](../Page/饑饉三十.md "wikilink")[太陽計劃等](../Page/太陽計劃.md "wikilink"),
自2012/13球季南區足球隊(現名皇室南區)升上香港甲組足球聯賽,[香港仔運動場成為南區足球隊的主場](../Page/香港仔運動場.md "wikilink")。

### 香港警察學院

[香港警察學院位於黃竹坑海洋公園道](../Page/香港警察學院.md "wikilink")18號，所有[香港警察要先在此接受訓練並畢業](../Page/香港警察.md "wikilink")，才可開始執行職務。

### 旅遊景點

黃竹坑最著名的旅遊景點，就是1977年開幕的[香港海洋公園](../Page/香港海洋公園.md "wikilink")。此外，[黃竹坑石刻亦是](../Page/黃竹坑石刻.md "wikilink")[香港法定古蹟之一](../Page/香港法定古蹟.md "wikilink")。

### 聖神修院

[聖神修院.jpg](https://zh.wikipedia.org/wiki/File:聖神修院.jpg "fig:聖神修院.jpg")\]\]

[聖神修院是香港訓練](../Page/聖神修院.md "wikilink")[天主教神職人員及供教友進修的重要地方](../Page/天主教.md "wikilink")，它也為教友提供圖書館、靜舍租用等服務以及開放教區文物館予已預約人士。

### 香港仔工業學校

1935年建校的[香港仔工業學校校舍是香港三級歷史建築](../Page/香港仔工業學校.md "wikilink")

### 聖士提反書院

1903年成立[聖士提反書院的書院大樓為古蹟](../Page/聖士提反書院.md "wikilink")

### 香港醫學專科學院

香港醫學專科學院位於黃竹坑道99號，乃是一所獨立的法定機構，有權組織、監察及評核所有醫學專科訓練，並頒授有關資格，同時亦負責提供延續醫學教育。

## 交通

### 主要交通幹道

黃竹坑的[黃竹坑道是](../Page/黃竹坑道.md "wikilink")[1號幹線的其中一段](../Page/1號幹線.md "wikilink")，接駁[香港仔海旁道](../Page/香港仔海旁道.md "wikilink")、鴨脷洲大橋及[香港仔隧道](../Page/香港仔隧道.md "wikilink")，也連接[南風道和](../Page/南風道.md "wikilink")[香島道](../Page/香島道.md "wikilink")，成為黃竹坑、香港仔及鴨脷洲通往港島其他地區及九龍的主要道路。

### 公共交通

[Platform_1_of_Wong_Chuk_Hang_Station.jpg](https://zh.wikipedia.org/wiki/File:Platform_1_of_Wong_Chuk_Hang_Station.jpg "fig:Platform_1_of_Wong_Chuk_Hang_Station.jpg")[黃竹坑站](../Page/黃竹坑站.md "wikilink")\]\]
[Ocean_Park_Station_(deep_blue_sky).jpg](https://zh.wikipedia.org/wiki/File:Ocean_Park_Station_\(deep_blue_sky\).jpg "fig:Ocean_Park_Station_(deep_blue_sky).jpg")\]\]

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{南港島綫色彩}}">█</font><a href="../Page/南港島綫.md" title="wikilink">南港島綫</a>：<a href="../Page/黃竹坑站.md" title="wikilink">黃竹坑站</a>、<a href="../Page/海洋公園站.md" title="wikilink">海洋公園站</a></li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 區議會議席分佈

為方便比較，以下列表以[鴨脷洲大橋](../Page/鴨脷洲大橋.md "wikilink")、[香港仔工業學校起向東至](../Page/香港仔工業學校.md "wikilink")[黃竹坑新圍](../Page/黃竹坑新圍.md "wikilink")、[香港仔隧道以西為範圍](../Page/香港仔隧道.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/南朗山道.md" title="wikilink">南朗山道</a>、<a href="../Page/警校道.md" title="wikilink">警校道</a>、<a href="../Page/香葉道.md" title="wikilink">香葉道明渠所包圍的範圍</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南朗山.md" title="wikilink">南朗山及</a><a href="../Page/黃竹坑道.md" title="wikilink">黃竹坑道沿線</a></p></td>
<td><p><a href="../Page/海灣_(選區).md" title="wikilink">海灣選區一部分</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參見

  - [深灣](../Page/深灣.md "wikilink")
  - [南朗山](../Page/南朗山.md "wikilink")
  - [香港仔](../Page/香港仔.md "wikilink")
  - [南區](../Page/南區_\(香港\).md "wikilink")

## 資料來源

[黃竹坑](../Category/黃竹坑.md "wikilink") [Category:南區
(香港)](../Category/南區_\(香港\).md "wikilink")
[Category:香港商業區](../Category/香港商業區.md "wikilink")