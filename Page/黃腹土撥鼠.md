**黃腹土撥鼠**（）在[旱獺屬中屬於大型粗壯健全的](../Page/旱獺屬.md "wikilink")[地松鼠](../Page/地松鼠.md "wikilink")。牠是十四種土撥鼠之一，原產於[加拿大西南部和](../Page/加拿大.md "wikilink")[美國西部的山區](../Page/美國西部.md "wikilink")，包括[落磯山脈](../Page/落磯山脈.md "wikilink")、[內華達山脈和](../Page/內華達山脈.md "wikilink")[華盛頓州的](../Page/華盛頓州.md "wikilink")[瑞尼爾山](../Page/瑞尼爾山.md "wikilink")，通常居住在海拔
 以上。毛皮主要為棕色，尾巴深色濃密，胸部黃色，眼睛間有白色斑毛，重約
。牠們生活在最多20隻的集群中，由一隻雄性統治。牠們是晝行性的，以植物、昆蟲和鳥蛋為食。從
8～9 月開始[冬眠持續整個冬季](../Page/冬眠.md "wikilink")。

## 描述

[Marmota_flaviventris_standing_S_of_Donahue_Pass.jpg](https://zh.wikipedia.org/wiki/File:Marmota_flaviventris_standing_S_of_Donahue_Pass.jpg "fig:Marmota_flaviventris_standing_S_of_Donahue_Pass.jpg")\]\]
成年黃腹土撥鼠體重約 ，雄性一般較雌性重\[1\]。一年之中體重變化十分大，與早春時體重最輕，晚秋時體重最重\[2\]。成年雄性體重約為
而成年雌性約為 。體長約為  ，擁有一條長  長滿蓬鬆毛的尾巴，後腳長 \[3\]。

## 參考來源

## 外部連結

  -
  -
[Category:旱獺](../Category/旱獺.md "wikilink")
[Category:美國哺乳動物](../Category/美國哺乳動物.md "wikilink")

1.

2.

3.