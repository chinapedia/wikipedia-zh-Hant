**黃金儲備**是各國[中央銀行所持有的](../Page/中央銀行.md "wikilink")[黃金資產](../Page/黃金.md "wikilink")。在[金本位制度被放棄之後](../Page/金本位.md "wikilink")，仍由各國[央行作](../Page/央行.md "wikilink")[貨幣發行準備之用](../Page/貨幣.md "wikilink")。

## 官方黃金儲備

[Gold_Reserves.png](https://zh.wikipedia.org/wiki/File:Gold_Reserves.png "fig:Gold_Reserves.png")
[Us_gold_reserves.png](https://zh.wikipedia.org/wiki/File:Us_gold_reserves.png "fig:Us_gold_reserves.png")。\]\]

官方黃金儲備是各國政府或銀行所持有的金條或金幣資產，相對於私人或非官方所擁有的黃金而言。

一般認為，位於[紐約的](../Page/紐約.md "wikilink")[聯邦儲備銀行是目前世界上最大的黃金貯藏地](../Page/聯邦儲備銀行.md "wikilink")，儲藏了約5,000噸的黃金，其中包括外國銀行和機構的信託。[瑞士銀行可能有更多](../Page/瑞士銀行.md "wikilink")，但沒有正式報告無法確認。

## 世界前40國家或組織黃金儲備列表

<table>
<caption>截至2016年6月（數據來自<a href="../Page/世界黃金協會.md" title="wikilink">世界黃金協會</a>）[1][2][3][4]</caption>
<thead>
<tr class="header">
<th><p>style="width:10%;" data-sort-type="number"| 排名</p></th>
<th><p>國家 / 組織</p></th>
<th><p>style="width:20%;" data-sort-type="number"| 黃金儲備（2016年6月）<br />
（噸）</p></th>
<th><p>style="width:20%;" data-sort-type="number"| 黃金佔儲備比率（2016年6月）<br />
（%）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>8,133.5</p></td>
<td><p>76%</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>3,378.2</p></td>
<td><p>70%</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/國際貨幣基金組織.md" title="wikilink">國際貨幣基金組織</a></p></td>
<td><p>2,814.0</p></td>
<td><p>N.A.</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>2,451.8</p></td>
<td><p>69%</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>2,435.8</p></td>
<td><p>65%</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>2,170.0</p></td>
<td><p>17%</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>1,842.6</p></td>
<td><p>2%</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>1,040.0</p></td>
<td><p>7%</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>765.2</p></td>
<td><p>3%</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>612.5</p></td>
<td><p>63%</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>557.8</p></td>
<td><p>6%</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Logo_European_Central_Bank.svg" title="fig:Logo_European_Central_Bank.svg">Logo_European_Central_Bank.svg</a> <a href="../Page/欧洲中央银行.md" title="wikilink">欧洲中央银行</a></p></td>
<td><p>504.8</p></td>
<td><p>28%</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>[5]</p></td>
<td><p>474.4</p></td>
<td><p>17%</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>422.7</p></td>
<td><p>4%</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>382.5</p></td>
<td><p>71%</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>322.9</p></td>
<td><p>2%</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>310.3</p></td>
<td><p>9%</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td></td>
<td><p>286.8</p></td>
<td><p>24%</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p>281.6</p></td>
<td><p>20%</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td></td>
<td><p>280.0</p></td>
<td><p>45%</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td></td>
<td><p>238.2</p></td>
<td><p>33%</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td></td>
<td><p>227.4</p></td>
<td><p>38%</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td></td>
<td><p>196.0</p></td>
<td><p>10%</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td></td>
<td><p>194.0</p></td>
<td><p>64%</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td></td>
<td><p>173.6</p></td>
<td><p>5%</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td></td>
<td><p>152.4</p></td>
<td><p>4%</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td></td>
<td><p>127.4</p></td>
<td><p>2%</p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td></td>
<td><p>125.7</p></td>
<td><p>9%</p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td></td>
<td><p>125.2</p></td>
<td><p>11%</p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td></td>
<td><p>121.1</p></td>
<td><p>3%</p></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td></td>
<td><p>116.6</p></td>
<td><p>6%</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td></td>
<td><p>112.7</p></td>
<td><p>63%</p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td></td>
<td><p>104.4</p></td>
<td><p>1%</p></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td><p><a href="../Page/國際清算銀行.md" title="wikilink">國際清算銀行</a></p></td>
<td><p>104.0</p></td>
<td><p>N.A.</p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td></td>
<td><p>103.7</p></td>
<td><p>11%</p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td></td>
<td><p>102.9</p></td>
<td><p>4%</p></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td></td>
<td><p>89.8</p></td>
<td><p>7%</p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td></td>
<td><p>79.9</p></td>
<td><p>7%</p></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td></td>
<td><p>79.0</p></td>
<td><p>10%</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td></td>
<td><p>78.1</p></td>
<td><p>3%</p></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><strong>前40個國家和組織總和</strong></p></td>
<td><p>31,428.9</p></td>
<td></td>
</tr>
</tbody>
</table>

## 私人持有黃金

官方黃金儲備大約只占世界所有黃金的20%，其餘大部份皆為私人持有。

在2007年1月，黃金ETF（gold exchange traded fund）共持有629噸私人或機構投資者擁有的黃金。

[印度是私人黃金儲備最大的國家](../Page/印度.md "wikilink")。由於對黃金的喜好與對其金融系統的不信任，印度人傾向購買黃金保值。根據[世界黃金協會](../Page/世界黃金協會.md "wikilink")（World
Gold Council）的估計，印度民間持有約10,000至15,000噸的黃金，大約是目前世界上10%的黃金。

## 黃金持有人

根據[美國地質調查局的資料](../Page/美國地質調查局.md "wikilink")，直到2011年，[人類共開採了](../Page/人類.md "wikilink")17萬噸黃金。\[6\]\[7\]

| 用途                                 | 持有量 (噸) | 比例     |
| ---------------------------------- | ------- | ------ |
| 總數                                 | 171,300 | 100%   |
| [珠寶](../Page/珠寶.md "wikilink")     | 84,300  | 49.2%  |
| [黃金投資](../Page/黃金投資.md "wikilink") | 33,000  | 19.26% |
| 中央銀行                               | 29,500  | 17.2%  |
| 工業用途                               | 20,800  | 12.14% |
| 未明                                 | 3,700   | 2.2%   |

黃金持有人 (2011)

## 参考文献

## 參見

  - [外汇储备](../Page/外汇储备.md "wikilink")

[Category:金](../Category/金.md "wikilink")
[Category:经济学术语](../Category/经济学术语.md "wikilink")

1.
2.
3.
4.  ["Top 40 reported official gold holdings (as at June 2016)" is on
    the 7th page of the pdf
    file.](https://www.gold.org/download/file/5101/GDT_Q2_2016_Gold_demand_statistics.pdf)
5.  [Gold has been added to Turkey's balance sheet as a result of a
    policy accepting gold in its reserve requirements from commercial
    banks.](http://www.tcmb.gov.tr/wps/wcm/connect/57c5777d-1f48-4eb4-98ba-af4c6aaddc20/ANO2012-38.pdf?MOD=AJPERES&CACHEID=57c5777d-1f48-4eb4-98ba-af4c6aaddc20)

6.  [on page 2 of the pdf file; last paragraph just **before** the
    "Production" section on that
    page](http://minerals.usgs.gov/minerals/pubs/commodity/gold/myb1-2011-gold.pdf)
7.