**黃靜修**（），別名**千年**，出生於[台灣](../Page/台灣.md "wikilink")[宜蘭](../Page/宜蘭縣.md "wikilink")，是[健美運動專欄作家](../Page/健美運動.md "wikilink")，網路自由撰稿人，中華民國現役[健美運動員](../Page/健美運動員.md "wikilink")。專長為競技健美、[重量訓練](../Page/重量訓練.md "wikilink")、[體適能運動指導](../Page/體適能.md "wikilink")、青少年[運動處方設計](../Page/運動處方.md "wikilink")，在臺灣[健身體適能界擁有千年教練的封號](../Page/健身.md "wikilink")，曾多次參加健美比賽，屢獲佳績。目前擔任中華民國健美協會健美運動教練與裁判。

千年於2004年開始在互聯網上創作健美運動相關文章，獲得健美界熱烈迴響。2005年與國內各大學健身社團共同籌組**中華民國大專健美運動聯盟**並擔任聯盟秘書長，在大學校園内推廣健美運動，該年度[中華民國大專院校體育總會首次將健美競賽編入行事曆](../Page/中華民國大專院校體育總會.md "wikilink")，並成功的主辦第一屆[93學年度全國大專盃健美錦標賽](../Page/93學年度全國大專盃健美錦標賽.md "wikilink")。2006年，千年創辦華文界第一份專業健美專欄網誌《**健美人視界**》，透过其專欄文章該網誌發行。

| **歷年競賽成績** |
| ---------- |
| 年份         |
| 2004       |
| 2005       |
| 2005       |
| 2005       |
| 2005       |
| 2005       |
| 2005       |
| 2005       |
| 2005       |
| 2006       |
| 2006       |
| 2006       |
| 2006       |
| 2006       |
| 2006       |
| 2007       |
| 2007       |
| 2009       |
| 2009       |
| 2009       |
| 2010       |
| 2011       |

## 参考

## 外部連結

  - [健美人視界 - 千年的個人網站暨健美運動網誌](http://blog.roodo.com/jinxiu)
  - [健美人論壇 - 專業健身健美運動精英社群](http://bodybuilders.sportboard.net)

[HJX](../Category/裁判员.md "wikilink") [黃](../Category/宜蘭人.md "wikilink")
[Category:台灣教練](../Category/台灣教練.md "wikilink")