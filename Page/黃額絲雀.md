**黃額絲雀**（学名：*Crithagra
mozambica*），俗稱**石燕**，視產地而定再細分為大金青、金青、吉打仔，為現今最普遍的[觀賞鳥之一](../Page/觀賞鳥.md "wikilink")。

## 生態環境

通常生活在林地和田園間，結群活動。

## 分佈地域

原產於[非洲](../Page/非洲.md "wikilink")[撒哈拉沙漠以南地區](../Page/撒哈拉沙漠.md "wikilink")。但由於世界性[鳥類貿易的盛行而被引入至](../Page/鳥類貿易.md "wikilink")[毛里求斯](../Page/毛里求斯.md "wikilink")、[波多黎各](../Page/波多黎各.md "wikilink")、[留尼汪](../Page/留尼汪.md "wikilink")、[塞舌尔和](../Page/塞舌尔.md "wikilink")[美國等地](../Page/美國.md "wikilink")。

## 特徵

雄性外型與[金黃絲雀十分相似](../Page/金黃絲雀.md "wikilink")，但體色比較暗淡。

## 食物

## 繁殖與保護

本物種未列入瀕危名單，但受到非法鳥類貿易的嚴重威脅。

## 黃額絲雀與鳥類貿易

黃額絲雀鳴聲清脆悅耳，色彩鮮艷，在中國是傳統的寵物鳥類，亦是中國非法鳥市中交易量最大的鳥類之一。黃額絲雀雖然成功人工繁殖，但始終進不上市場需求，因此鳥類貿易中，特別是歐美地區，大部分的黃額絲雀均直接從野外捕捉，這種非法鳥類貿易對野生鳥類種群造成極大的威脅。

## 參見

  - Database entry includes justification for why this species is of
    least concern

[Category:雀科](../Category/雀科.md "wikilink")
[Category:丝雀属](../Category/丝雀属.md "wikilink")