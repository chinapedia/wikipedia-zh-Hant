**黃高鰭刺尾魚**（[学名](../Page/学名.md "wikilink")：），又稱**黃高鰭刺尾鯛**，俗名黄金吊、黃三角、**黃三角倒吊**，是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[刺尾魚亞目](../Page/刺尾魚亞目.md "wikilink")[刺尾鱼科的一](../Page/刺尾鱼科.md "wikilink")[種](../Page/種.md "wikilink")。因為長相類似、容易跟黃倒吊搞混，但黃倒吊是印尼的觀賞魚種，也就是被稱作一字倒吊的刺尾魚。

## 分布

本魚分布於[太平洋區](../Page/太平洋.md "wikilink")，包括[琉球群島](../Page/琉球群島.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[夏威夷群島等海域](../Page/夏威夷群島.md "wikilink")。

## 深度

水深2至46公尺。

## 特徵

本魚體型為卵圓形而側扁，吻突出。口小，端位，上下頜齒較大，齒固定不可動，扁平，邊緣具缺刻。成魚的頭及身體呈一致的鮮黃色，死後不久即成棕色。在體側中央有許多淡色縱線，背鰭及臀鰭硬棘尖銳，尾柄兩側各有一長棘，其前方有許多毛狀突起。尾鰭截形。幼魚體白，上有許多淡棕色橫線，背鰭及臀鰭鰭條均較成魚長。背鰭硬棘5枚、背鰭軟條23至26枚、臀鰭硬棘3枚、臀鰭軟條19至22枚。體長可達15公分。

## 生態

棲息於珊瑚繁茂的礁湖及向海礁坡，海浪帶下方，通常單獨或形成一個鬆散的小群體在附生於石上的藻叢中活動。喜食藻類，但也以底生生物為食。

## 經濟利用

體色鮮艷，為高價值的觀賞魚，常會和黃倒吊搞混，因而稱作黃三角居多，但他的價格是黃倒吊的兩倍以上。

## 参考文献

  -
  -
  -
## 外部連結

  - [台灣生物
    TaiBNet](http://taibnet.sinica.edu.tw/chinese/taibnet_species_detail.asp?TSID=381294)
    Zebrasoma flavescens 黃高鰭刺尾魚
  - [國立海洋生物博物館](http://diginet.nmmba.gov.tw/chinese/freshfishpic.asp?science=Zebrasoma%20flavescens)
    Zebrasoma flavescens 黃高鰭刺尾魚
  - [AquariumDomain](https://web.archive.org/web/20070127054721/http://www.aquariumdomain.com/species/tang_yellowTang.asp)
    （英文） 鹹水水族箱資訊

[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[Category:食用魚](../Category/食用魚.md "wikilink")
[flavescens](../Category/高鰭刺尾魚屬.md "wikilink")