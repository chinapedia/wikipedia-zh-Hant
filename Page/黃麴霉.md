**黃麴黴菌**（[學名](../Page/學名.md "wikilink")：**''**''）或稱為**黃麴菌**、**黃麴霉**、**黃曲黴**與**黃曲霉**等，是一種[真菌](../Page/真菌.md "wikilink")。在自然環境中，它是一種常見的[黴菌](../Page/黴菌.md "wikilink")，在儲存的榖類中會造成儲存的問題。它也是一種人類的[病原](../Page/病原.md "wikilink")，會造成[肺的](../Page/肺.md "wikilink")[麴菌症](../Page/麴菌症.md "wikilink")（Aspergillosis），有時候也會引起[角膜](../Page/角膜.md "wikilink")、耳\[1\]與鼻眼框的感染。許多菌種會產生足量的[黃麴毒素](../Page/黃麴毒素.md "wikilink")\[2\]，這是一種有致癌性且有劇烈毒性的化合物。黃麴黴菌的孢子是一種[過敏原](../Page/過敏原.md "wikilink")（Allergen）。黃麴黴菌有時候也會造成[蠶孵卵所的損害](../Page/蠶.md "wikilink")。

## 對人類造成的疾病

黃麴黴菌是造成麴菌症第二常見的病媒，僅次於[薰煙麴菌](../Page/薰煙麴菌.md "wikilink")\[3\]（**）。黃麴黴菌會入侵肺或是腦的動脈，因而造成[梗塞](../Page/梗塞.md "wikilink")（Infarction）。[嗜中性白血球減少症](../Page/嗜中性白血球減少症.md "wikilink")\[4\]（Neutropenia）目前也被認定為是[麴菌屬的傳染病](../Page/麴菌屬.md "wikilink")。

黃麴黴菌也產生毒素（[黃麴毒素](../Page/黃麴毒素.md "wikilink")），而該毒素則是[肝細胞癌](../Page/肝細胞癌.md "wikilink")\[5\]（hepatocellular
carcinoma）的其中一種病源媒介\[6\]。

## 培養時的外觀

黃麴黴菌在培養時是成長成黃綠色的黴菌，「黃麴黴菌」的中文名稱也是來自於外觀的顏色。與其他[麴菌屬的物種一樣](../Page/麴菌屬.md "wikilink")，它也會產生一種特別的[分生孢子梗](../Page/分生孢子梗.md "wikilink")\[7\]（conidiophore），這個結構是由一條支撐著膨脹囊泡的長莖所組成。在囊泡上的[產孢細胞](../Page/產孢細胞.md "wikilink")（conidiogenous
cell）則會產生[分生孢子](../Page/分生孢子.md "wikilink")（conidia）。黃麴黴菌的許多菌種在[紫外光照射下會呈現出一種綠色的螢光](../Page/紫外光.md "wikilink")，這個與[黃麴毒素生產的程度有所相關](../Page/黃麴毒素.md "wikilink")。

## 霉害

黃麴黴菌特別普遍存在於[玉米](../Page/玉米.md "wikilink")、[花生或](../Page/花生.md "wikilink")[黃豆](../Page/黃豆.md "wikilink")\[8\]之中，造成的霉害如同[水損的](../Page/水損害.md "wikilink")（water
damage）[地毯一樣](../Page/地毯.md "wikilink")。並且，黃麴黴菌是其中幾種會產生知名的黃麴毒素的黴菌之一，而黃麴毒素則可能會導致[急性肝炎](../Page/急性肝炎.md "wikilink")（acute
hepatitis）、[免疫抑制](../Page/免疫抑制.md "wikilink")（immunosuppression）與肝細胞癌。在一些病毒性[肝炎高度流行的國家會缺乏對於這種真菌的任何屏障方式](../Page/肝炎.md "wikilink")，也會高度增加肝細胞[腫瘤的風險](../Page/惡性上皮細胞腫瘤.md "wikilink")。

## 參考文獻與注釋

  - Roth L., Frank H., Kormann K.: Giftpilze. Pilzgifte. Schimmelpilze.
    Mykotoxine. Vorkommen, Inhaltsstoffe, Pilzallergien. ecomed,
    Landsberg a. L. 1990, ISBN 3609647302
  - Diener U. L., Cole R. J., Sansders T. H., Payne G. A., Lee L.S.,
    Klich M. A. Ann. Rev. Pythopathol. 1987 25:249-70

## 外部連結

  - [黃麴黴菌](http://web2.nmns.edu.tw/fungi/Module/Module.php?Module=C01&Fungi_ID=15&Class_ID=10&Level2_ID=72&Level1_ID=17&cookie=del_L4)於[真菌百科](http://web2.nmns.edu.tw/fungi/Module/Module.php?Level1_ID=17&cookie=del_L1&Level1_BgDir=encyclopedia)

  - [黃麴黴菌基因定序計畫](https://web.archive.org/web/20071109231501/http://www.aspergillusflavus.org/genomics/)

  - [黃麴黴菌研究](https://web.archive.org/web/20070307151941/http://www.aspergillusflavus.org/)

[Category:曲霉属](../Category/曲霉属.md "wikilink")
[Category:寄生真菌](../Category/寄生真菌.md "wikilink")

1.  即[耳真菌症](../Page/耳真菌症.md "wikilink")（Otomycosis）。
2.
3.  又稱為[煙色麴菌](../Page/煙色麴菌.md "wikilink")。
4.  又稱為[嗜中性白血球缺乏症](../Page/嗜中性白血球缺乏症.md "wikilink")。
5.  肝細胞癌是[肝癌最常見的類型](../Page/肝癌.md "wikilink")。
6.  Crawford JM, *Liver and Biliary Tract.* Pathologic Basis of Disease,
    ed. Kumar V, et al. 2005, Philadelphia: Elsevier Saunders. p. 924
7.  又稱為[足細胞](../Page/足細胞.md "wikilink")。
8.  [黃麴黴菌](http://web2.nmns.edu.tw/fungi/Module/Module.php?Module=C01&Fungi_ID=15&Class_ID=10&Level2_ID=72&Level1_ID=17&cookie=del_L4)於[真菌百科](http://web2.nmns.edu.tw/fungi/Module/Module.php?Level1_ID=17&cookie=del_L1&Level1_BgDir=encyclopedia)