**黄初**（220年十月－226年）是[三国时期](../Page/三国.md "wikilink")[曹魏的君主魏文帝](../Page/曹魏.md "wikilink")[曹丕的](../Page/曹丕.md "wikilink")[年号](../Page/年号.md "wikilink")，共计7年。这是曹魏的第一个年号。

黄初七年五月魏明帝[曹叡即位沿用](../Page/曹叡.md "wikilink")。

## 纪年

<table>
<thead>
<tr class="header">
<th><p>黄初</p></th>
<th><p>元年</p></th>
<th><p>二年</p></th>
<th><p>三年</p></th>
<th><p>四年</p></th>
<th><p>五年</p></th>
<th><p>六年</p></th>
<th><p>七年</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/公元纪年.md" title="wikilink">公元</a></p></td>
<td><p>220年</p></td>
<td><p>221年</p></td>
<td><p>222年</p></td>
<td><p>223年</p></td>
<td><p>224年</p></td>
<td><p>225年</p></td>
<td><p>226年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/干支纪年.md" title="wikilink">干支</a></p></td>
<td><p><a href="../Page/庚子.md" title="wikilink">庚子</a></p></td>
<td><p><a href="../Page/辛丑.md" title="wikilink">辛丑</a></p></td>
<td><p><a href="../Page/壬寅.md" title="wikilink">壬寅</a></p></td>
<td><p><a href="../Page/癸卯.md" title="wikilink">癸卯</a></p></td>
<td><p><a href="../Page/甲辰.md" title="wikilink">甲辰</a></p></td>
<td><p><a href="../Page/乙巳.md" title="wikilink">乙巳</a></p></td>
<td><p><a href="../Page/丙午.md" title="wikilink">丙午</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蜀汉.md" title="wikilink">蜀汉</a></p></td>
<td><p><a href="../Page/建安.md" title="wikilink">建安二十五年</a></p></td>
<td><p>二十六年<br />
<a href="../Page/章武.md" title="wikilink">章武元年</a></p></td>
<td><p>二年</p></td>
<td><p>三年<br />
<a href="../Page/建兴_(蜀汉).md" title="wikilink">建兴元年</a></p></td>
<td><p>二年</p></td>
<td><p>三年</p></td>
<td><p>四年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/东吴.md" title="wikilink">东吴</a></p></td>
<td><p><a href="../Page/建安.md" title="wikilink">建安二十五年</a></p></td>
<td><p>二十六年</p></td>
<td><p>二十七年<br />
<a href="../Page/黄武.md" title="wikilink">黄武元年</a></p></td>
<td><p>二年</p></td>
<td><p>三年</p></td>
<td><p>四年</p></td>
<td><p>五年</p></td>
</tr>
</tbody>
</table>

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")

[Category:曹魏年号](../Category/曹魏年号.md "wikilink")
[H黄](../Category/3世纪年号.md "wikilink")