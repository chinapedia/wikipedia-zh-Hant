**黃得功**（），字虎山，三萬衛（[開原](../Page/開原.md "wikilink")）人\[1\]，明末、[南明將領](../Page/南明.md "wikilink")，出身[行伍](../Page/行伍.md "wikilink")，《[甲申朝事小紀](../Page/甲申朝事小紀.md "wikilink")》載其「每戰身自衝突，勁疾若飛，江、淮人呼曰『闖子』」。善使鐵[鞭](../Page/鞭.md "wikilink")，在[雜劇](../Page/雜劇.md "wikilink")《[桃花扇](../Page/桃花扇.md "wikilink")》中有登場，武器為[雙鞭](../Page/雙鞭.md "wikilink")。
在[太平府與清兵交戰](../Page/太平府_\(安徽省\).md "wikilink")，中流矢，傷重，[自刎而死](../Page/自刎.md "wikilink")。

## 簡介

先祖[南直隸](../Page/南直隸.md "wikilink")[合肥人](../Page/合肥.md "wikilink")。少負奇氣，「貌伟胡髯，两颐倒竖，膂力绝伦」，十二歲時，母親釀酒，被他全偷喝光。母親責備他，他笑道：「償還酒錢很簡單啊。」辽东战事起，黄得功在军中出战，[斩首二级](../Page/斩首.md "wikilink")，获赏[白银五十两](../Page/白银.md "wikilink")。他送给母亲说：“用这些偿还酒钱。”後從軍於[遼陽](../Page/遼陽.md "wikilink")，累功至[游击](../Page/游击.md "wikilink")，[崇祯九年](../Page/崇祯.md "wikilink")（1636年）升[副总兵](../Page/副总兵.md "wikilink")。

[崇祯十一年](../Page/崇祯.md "wikilink")（1638年）長期在[南直隸](../Page/南直隸.md "wikilink")[江北](../Page/江北.md "wikilink")、[河南一帶](../Page/河南.md "wikilink")，从[总督](../Page/总督.md "wikilink")[熊文灿與](../Page/熊文灿.md "wikilink")[張獻忠](../Page/張獻忠.md "wikilink")、革左五營等部義軍作戰，诏加[太子太师](../Page/太子太师.md "wikilink")。崇祯十五年，黃得功大敗流賊[張獻忠於](../Page/張獻忠.md "wikilink")[潛山](../Page/潛山.md "wikilink")。官升至[廬州](../Page/廬州.md "wikilink")[總兵](../Page/總兵.md "wikilink")，人號為「黃闖子」。明亡前夕隨[馬士英平定河南](../Page/馬士英.md "wikilink")[永城叛將](../Page/永城.md "wikilink")[劉超](../Page/劉超.md "wikilink")，論功封為**靖南伯**。軍紀嚴明，廬州、[桐城](../Page/桐城.md "wikilink")、[定遠等地都為他立生祠](../Page/定遠.md "wikilink")。[弘光帝](../Page/弘光帝.md "wikilink")[朱由崧之](../Page/朱由崧.md "wikilink")[淮揚四鎮](../Page/淮揚.md "wikilink")，令黃得功、[高傑](../Page/高傑.md "wikilink")、[劉澤清和](../Page/劉澤清.md "wikilink")[劉良佐統領](../Page/劉良佐.md "wikilink")。一次黄得功跪接诏书，觉得不合自己的意思，不待读完就爬起来，攘袂掀案，大詈曰：“去！速去！吾不知是何诏也！”\[2\]

弘光元年(1645年)三月明将[左良玉从](../Page/左良玉.md "wikilink")[武昌以](../Page/武昌.md "wikilink")“[清君侧](../Page/清君侧.md "wikilink")”的名义发兵，进攻弘光朝廷。四月初左良玉在[九江病死](../Page/九江.md "wikilink")，其子[左夢庚秘不发丧](../Page/左夢庚.md "wikilink")，继续进攻。黄得功率军击败左夢庚。左夢庚降清。

五月清兵渡江南下，[弘光帝](../Page/弘光帝.md "wikilink")[朱由崧逃走](../Page/朱由崧.md "wikilink")，清兵分兵襲取[太平府](../Page/太平府_\(安徽省\).md "wikilink")，黄得功率軍在[荻港和清兵大戰](../Page/荻港.md "wikilink")。此時原明降將[劉良佐在岸上大呼招降](../Page/劉良佐.md "wikilink")，黄得功怒叱“汝乃降乎！”突然一箭射中其咽喉偏左，黄得功傷重，自知回天乏術，拔箭自刺其喉而逝\[3\]。其妻聞之，亦[自縊而死](../Page/自縊.md "wikilink")。

## 注釋

[Category:明朝軍事人物](../Category/明朝軍事人物.md "wikilink")
[Category:明朝郡王](../Category/明朝郡王.md "wikilink")
[Category:南明人物](../Category/南明人物.md "wikilink")
[De得功](../Category/黃姓.md "wikilink")

1.  《[明史](../Page/明史.md "wikilink")·黄得功传》，卷268；或说是[顺天人](../Page/顺天.md "wikilink")（《[石匮书后集](../Page/石匮书.md "wikilink")·黄得功传》，卷38）；或称[安徽](../Page/安徽.md "wikilink")[凤阳](../Page/凤阳.md "wikilink")[灵璧县人](../Page/灵璧县.md "wikilink")（《[明季南略](../Page/明季南略.md "wikilink")》，卷1）
2.  [姜曰广](../Page/姜曰广.md "wikilink")：《[过江七事](../Page/过江七事.md "wikilink")》
3.  《明史·卷二六八·黃得功傳》：“忽飛矢至，中其喉，偏左。得功知不可為，擲刀，拾所拔箭剌吭死。”