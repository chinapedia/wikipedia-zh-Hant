[黃汲清.jpg](https://zh.wikipedia.org/wiki/File:黃汲清.jpg "fig:黃汲清.jpg")
**黄汲清**（），字**德淦**，[四川省](../Page/四川省.md "wikilink")[仁寿县青岗场人](../Page/仁寿县.md "wikilink")，中国地质学家，第一届中研院院士，1955年[中国科学院](../Page/中国科学院.md "wikilink")[学部委员](../Page/学部委员.md "wikilink")，任地质部石油局总工程师，创立多旋回构造运动学说和[陆相生油论](../Page/陆相生油论.md "wikilink")，对中国石油天然气勘探事业产生重大影响。

## 生平

1924年考入北大地质系，大三写出震动业界的论文，首创大地构造理论，让中国人找到了玉门油矿、大庆油矿。1932年夏，黄汲清受中华教育文化基金会的选派，赴瑞士留学。先在[伯尔尼大学](../Page/伯尔尼大学.md "wikilink")(University
of Bern)学习半年，1933年春转入[纳沙泰尔大学](../Page/纳沙泰尔大学.md "wikilink")(University
of Neuchâtel)地质系，在著名大地构造学家教授亲自指导下深造。
1933年和1934年，在[阿尔卑斯山的素女峰一带进行](../Page/阿尔卑斯山.md "wikilink")​​地质调查并填制地质图。
1935年，以法文写成博士论文《瑞士华莱县素女峰—破金瓜峰地区之地质研究》，获理学博士学位。
1936年春季回到祖国，任前实业部南京中央地质调查所地质主任。
1937年春，黄汲清任前中央地质调查所代理所长。同年7月，赴莫斯科参加第17届国际地质大会。
12月正式就任中央地质调查所所长。1948年榮獲[中華民國](../Page/中華民國.md "wikilink")[中央研究院第](../Page/中央研究院.md "wikilink")1屆(數理科學組)院士\[1\]。

文革中，他先被关入地下室180天，后流放[五七干校](../Page/五七干校.md "wikilink")。回来时惊发现自己的成果[大庆油田竟被冠于](../Page/大庆油田.md "wikilink")[李四光名下](../Page/李四光.md "wikilink")，于是写信要求正名——为那些曾经的付出而正名\[2\]。2009年建国60年中央电视台《奠基者》即为黄汲清正名。

## 家庭

女：黄洁生
子：黄浩生、[黄渝生](../Page/黄渝生.md "wikilink")（儿媳：[詹敏利](../Page/詹敏利.md "wikilink")）
孙女：黃婉（渝生之女）

## 参见

  - [翁文波](../Page/翁文波.md "wikilink")
  - [翁文灏](../Page/翁文灏.md "wikilink")
  - [李四光](../Page/李四光.md "wikilink")
  - [谢家荣](../Page/谢家荣.md "wikilink")
  - [孙健初](../Page/孙健初.md "wikilink")
  - [潘钟祥](../Page/潘钟祥.md "wikilink")

## 参考文献

  - [黄汲清与中国石油大发现](https://web.archive.org/web/20161129021629/http://www.oilnews.com.cn/bk/system/2007/04/29/001088142.shtml)
    石油百科 2007-04-29
  - [【特稿】拉古娜海滩的黄家](http://companies.caixin.com/2013-09-25/100586036.html)
    财新网 2013-09-25

## 外部链接

[Category:中央研究院數理科學組院士](../Category/中央研究院數理科學組院士.md "wikilink")
[Category:中華人民共和國地質學家](../Category/中華人民共和國地質學家.md "wikilink")
[Category:九三学社社员](../Category/九三学社社员.md "wikilink")
[Category:仁寿人](../Category/仁寿人.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:纳沙泰尔大学校友](../Category/纳沙泰尔大学校友.md "wikilink")
[Category:四川科学家](../Category/四川科学家.md "wikilink")
[J](../Category/黄姓.md "wikilink")

1.  [黃汲清 逝世院士一覽表](https://academicians.sinica.edu.tw/index.php?func=1-D)
    中央研究院
2.  《大庆油田发现真相》 四川人民出版社