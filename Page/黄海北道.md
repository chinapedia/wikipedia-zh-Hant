[缩略图](https://zh.wikipedia.org/wiki/File:Kaesong_model_complex.jpg "fig:缩略图")
**黄海北道**（），是[朝鲜民主主义人民共和国西南部的地方行政区](../Page/朝鲜民主主义人民共和国.md "wikilink")。[面积](../Page/面积.md "wikilink")8,154[平方公里](../Page/平方公里.md "wikilink")。人口2,113,672人（2008年统计）。[人口密度](../Page/人口密度.md "wikilink")260人/平方公里。

道政府所在[沙里院市](../Page/沙里院.md "wikilink")。管辖2市14郡272里50洞7劳动地区，[開城特級市位於此區](../Page/開城特級市.md "wikilink")。

## 地理

北接[平壤和](../Page/平壤.md "wikilink")[平安南道](../Page/平安南道.md "wikilink")，东临[江原道](../Page/江原道_\(朝鮮\).md "wikilink")，南邻[开城工业地区](../Page/开城工业地区.md "wikilink")，西与[黄海南道接壤](../Page/黄海南道.md "wikilink")。位于内陆。

## 历史

  - 1417年[黄州和](../Page/黃州郡.md "wikilink")[海州合为](../Page/海州市.md "wikilink")[黄海道](../Page/黄海道.md "wikilink")。
  - 1954年朝鲜政府将黄海道分为南北两部分，成立黄海北道。
  - 2003年开城工业地区从黄海北道分出。

## 行政区划

  - 市：[沙里院市](../Page/沙里院市.md "wikilink") -
    [-{松}-林市](../Page/松林市.md "wikilink") -
    [开城特级市](../Page/开城特级市.md "wikilink")
  - 郡：[谷山郡](../Page/谷山郡.md "wikilink") -
    [金川郡](../Page/金川郡.md "wikilink") -
    [開豐郡](../Page/開豐郡.md "wikilink") -
    [麟山郡](../Page/麟山郡.md "wikilink") -
    [鳳山郡](../Page/鳳山郡_\(朝鮮\).md "wikilink") -
    [瑞興郡](../Page/瑞興郡.md "wikilink") -
    [遂安郡](../Page/遂安郡_\(朝鮮\).md "wikilink") -
    [新溪郡](../Page/新溪郡.md "wikilink") -
    [新坪郡](../Page/新坪郡.md "wikilink") -
    [延山郡](../Page/延山郡.md "wikilink") -
    [燕灘郡](../Page/燕灘郡.md "wikilink") -
    [銀波郡](../Page/銀波郡.md "wikilink") -
    [長豐郡](../Page/長豐郡.md "wikilink") -
    [兔山郡](../Page/兔山郡.md "wikilink") -
    [平山郡](../Page/平山郡.md "wikilink") -
    [黃州郡](../Page/黃州郡.md "wikilink") -
    [中和郡](../Page/中和郡.md "wikilink") -
    [祥原郡](../Page/祥原郡.md "wikilink")
  - 区域：[胜湖区域](../Page/胜湖区域.md "wikilink")

## 高等院校

  - [桂應祥沙里院農業大學](../Page/桂應祥沙里院農業大學.md "wikilink") (Kye Ung Sang Sariwon
    University of Agriculture)
  - [沙里院地質大學](../Page/沙里院地質大學.md "wikilink")
  - [沙里院教員大學](../Page/沙里院教員大學.md "wikilink")
  - [沙里院藝術學院](../Page/沙里院藝術學院.md "wikilink")

## 觀光熱點

  - 中朝友誼林：[長豐郡](../Page/長豐郡.md "wikilink")[貴存里的](../Page/貴存里.md "wikilink")[馬嶺山山麓有](../Page/馬嶺山.md "wikilink")100平方公頃海松林，由[中國人民志願軍在](../Page/中國人民志願軍.md "wikilink")1958年的春季，在即將返回[中國時](../Page/中國.md "wikilink")，和當地的[朝鮮民眾一起植樹](../Page/朝鮮.md "wikilink")，稱植了30多平方公頃海松林。
  - [王建王陵](../Page/王建王陵.md "wikilink")(Tomb of King Wanggon)

[\*](../Category/黃海北道.md "wikilink")
[Category:1954年建立的行政區劃](../Category/1954年建立的行政區劃.md "wikilink")