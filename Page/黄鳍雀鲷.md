**黄鳍雀鲷**（[学名](../Page/学名.md "wikilink")：）为[雀鲷科](../Page/雀鲷科.md "wikilink")[雀鲷属的](../Page/雀鲷属.md "wikilink")[鱼类](../Page/鱼类.md "wikilink")，俗名菲律宾雀鲷。

## 分布

本魚分布於[印度西](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[琉球群島](../Page/琉球群島.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[索羅門群島](../Page/索羅門群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[東加](../Page/東加.md "wikilink")、[諾魯等海域](../Page/諾魯.md "wikilink")。

## 深度

水深2至15公尺。

## 特徵

本魚體呈橢圓形而側扁，體全身黑[褐色](../Page/褐色.md "wikilink")，但整個背鰭及尾鰭、臀鰭之上墨緣為[黃色](../Page/黃色.md "wikilink")。鱗片後端均有明顯黑緣，有如網狀。此外其胸鰭基部有一黑色斑，腹鰭[黑色](../Page/黑色.md "wikilink")。吻短而鈍圓。口中型；頜齒兩列，小而呈圓錐狀。眶下骨具鱗，下緣具鋸齒；前鰓蓋骨後緣具鋸齒。幼魚時體色為藍黑近[紫色](../Page/紫色.md "wikilink")，背鰭、臀鰭、尾鰭為黃色，與成魚不同。背鰭硬棘13枚、背鰭軟條14至15枚、臀鰭硬棘2枚、臀鰭軟條14至16枚。體長可達12公分。

## 生態

本魚多單獨或成群棲息在大礁或獨立礁之礁緣斜坡或是峭壁處。屬雜食性，以東物性[浮游生物或](../Page/浮游生物.md "wikilink")[藻類為食](../Page/藻類.md "wikilink")，白天在水層中覓食，夜間則就近在礁壁處覓洞歇息。

## 經濟利用

多用以水族觀賞，很少人食用。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[philippinus](../Category/雀鯛屬.md "wikilink")