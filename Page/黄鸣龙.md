**黄鸣龙**（），[江苏](../Page/江苏.md "wikilink")[扬州人](../Page/扬州.md "wikilink")，著名[有机化学家](../Page/有机化学.md "wikilink")，中国科学院院士。

## 生平简历

黄鸣龙在1898年8月6日出生于[江苏省](../Page/江苏省_\(清\).md "wikilink")[扬州府](../Page/扬州府.md "wikilink")（今[扬州市](../Page/扬州市.md "wikilink")）。

1918年，黄鸣龙在浙江医药专科学校（现[浙江大学医学院](../Page/浙江大学.md "wikilink")）毕业，随即远赴[瑞士](../Page/瑞士.md "wikilink")，在[苏黎世大学学习](../Page/苏黎世大学.md "wikilink")。1922年在[德国](../Page/德国.md "wikilink")[柏林大学深造](../Page/柏林洪堡大学.md "wikilink")，并在1924年，获[哲学博士学位](../Page/哲学博士.md "wikilink")。

1924年回到中国后，历任浙江省卫生试验所化验室主任、卫生署技正与化学科主任、浙江省立医药专科学校药科教授等职。

1934年，黄鸣龙再度赴德国，先在柏林用了一年时间补做有机合成和分析方面的实验，并学习有关的新技术，后于1935年入德国[维尔茨堡大学化学研究所进修](../Page/维尔茨堡大学.md "wikilink")。1938至1940年，黄鸣龙先在德国[先灵药厂研究](../Page/先灵公司.md "wikilink")，后又在[英国研究女性激素](../Page/英国.md "wikilink")。

1940至1945年，黄鸣龙回到国内任中央研究院化学研究所（昆明）研究员兼西南联合大学教授。其间进行了药物[山道年的立体异构研究](../Page/山道年.md "wikilink")，并为国内外后续研究奠定理论基础。

1945至1952年，黄鸣龙作为访问学者来到[美国](../Page/美国.md "wikilink")[哈佛大学做研究](../Page/哈佛大学.md "wikilink")，其间创造性的改进了[沃尔夫－凯惜纳还原反应](../Page/沃尔夫－凯惜纳－黄鸣龙还原反应.md "wikilink")，此后简称为**黄鸣龙改良还原法**。后到[默克药厂做研究员](../Page/默克大药厂.md "wikilink")。

1952年，黄鸣龙再次回国，任**军事医学科学院**化学系主任。1955年，选聘为[中国科学院院士](../Page/中国科学院.md "wikilink")，并于次年转到中国科学院上海有机化学研究所工作。1958年，研究出了利用国产薯蓣皂甙元为原料，七步合成可的松，获国家创造发明奖。之后，黄鸣龙的研究方向主要放在甾体化合物上，并合成出多种甾体激素类药物及口服避孕药。\[1\]

## 科学成就

### 山道年研究

1938年，黄鸣龙与Inhoffen研究用胆固醇改造合成雌性激素时，发现[胆甾双烯酮用](../Page/胆甾双烯酮.md "wikilink")[醋酸酐及微量](../Page/醋酸酐.md "wikilink")[浓硫酸处理](../Page/浓硫酸.md "wikilink")，发生[双烯酮－酚的移位反应](../Page/双烯酮－酚移位反应.md "wikilink")。\[2\]
在此研究工作的基础上，于1940年即从事[山道年一类物的立体化学的研究](../Page/山道年.md "wikilink")。黄鸣龙在研究这类化合物的相对构型时，发现四个变质山道年在酸碱作用下，其相对构型可成圈地互相转变，以及证明变质山道年因三个旋光中心互相邻近所产生的“邻位影响”。无取代反应而能使三个不对称中心成圈地转变，这在立体化学上，是个前所未有的发现。\[3\]\[4\]
各国学者根据黄鸣龙所解决的山道年及其一类物的相对构型，相继推定了它们的绝对构型。日本学者还报道了山道年、β－山道年以及若干异构体的全合成。\[5\]

### 改良沃尔夫-凯惜纳反应

在有机化合物的合成和结构的测定中，当需要将醛类或酮类的羰基还原为次甲基时，常常用[沃尔夫-凯惜纳还原法](../Page/沃尔夫－凯惜纳－黄鸣龙还原反应.md "wikilink")，但是，此法条件苛刻，要用封管和金属[钠以及难以制备和价值昂贵的无水水合](../Page/钠.md "wikilink")[肼](../Page/肼.md "wikilink")。同时在应用此法还原时，若有极少量的水份存在，往往不可避免地要引起一些副反应。

85％（有时可用50％）水合胼及[双缩乙二醇或](../Page/双缩乙二醇.md "wikilink")[三缩乙二醇](../Page/三缩乙二醇.md "wikilink")，同置于圆底烧瓶内，回流1小时，移去冷凝管，继续加热，直到溶液温度上升至190—200Ｃ时，再插上冷凝管，保持此温度23小时，然后按常规方法处理即得。改良的沃尔夫-凯惜纳（Wolff－Kishner）还原法，产率为95％。\[6\]\[7\]

黄鸣龙改良的沃尔夫-凯惜纳还原法在当时的国际上广泛应用（现又改良），并编入各国有机化学教科书中，是第一个用中国人名字命名的有机反应，简称为黄鸣龙还原法。\[8\]

### 甾体激素相关研究

1958年，黄鸣龙利用薯蓣皂甙元为原料，用微生物氧化加入11α－羟基和用氧化钙－碘－醋酸钾加入C21－OAc的方法，七步合成了可的松。\[9\]
有了合成可的松的工业基础，60年代初期，许多重要的甾体激素如[黄体素](../Page/黄体素.md "wikilink")、[睾丸素](../Page/睾酮.md "wikilink")、[强的松和](../Page/强的松.md "wikilink")[地塞米松等](../Page/地塞米松.md "wikilink")，都在先后生产出来。黄鸣龙对甲地孕酮的合成方法进行了改进，他改进的方法只要3步，总收率达40％。这一改进不但有一定经济价值，并且也有一定理论意义。他还首先发现了甲地孕酮的避孕作用。\[10\]

## 名誉

  - 国际《[四面体](../Page/四面体_\(期刊\).md "wikilink")》杂志名誉编辑；
  - 他和合作者共发表中外文论文80篇，专著及综述近40本（篇）
  - 有机所学术委员会主任、名誉主任；
  - 中国科学院数理化学部学部委员；
  - 中国科学技术委员会化学组成员；
  - 1978年全国科学大会的先进代表称号；
  - 国家科委计划生育组副组长；
  - 中国药学会上海分会名誉理事长，中国药学会副理事长，中国化学会理事；
  - 第三届全国人大代表；
  - 第二、三、五届全国政协委员；

<!-- end list -->

  - 《醋酸可的松的七步合成法》 1966年获国家发明奖；
  - 《甾体激素的合成与甾体反应的研究》 1982年获国家自然科学二等奖；

## 资料来源

<references />

1.  [Nicolai Kishner](../Page/Nicolai_Kishner.md "wikilink") *J. Russ.
    Chem. Soc.* **1911**, *43*, 582.

2.

## 参见

  - [沃尔夫-凯惜纳-黄鸣龙还原反应](../Page/沃尔夫-凯惜纳-黄鸣龙还原反应.md "wikilink")
  - [有关黄鸣龙的评价](http://www.docin.com/p-306045.html#docTitle)
  - [黄鸣龙简历](https://web.archive.org/web/20080915202918/http://www.sioc.ac.cn/yszc/wangml.htm)

[MING](../Category/黄姓.md "wikilink")
[Category:中国化学家](../Category/中国化学家.md "wikilink")
[Category:扬州人](../Category/扬州人.md "wikilink")
[Category:浙江大學校友](../Category/浙江大學校友.md "wikilink")
[Category:浙江大學教授](../Category/浙江大學教授.md "wikilink")
[Category:苏黎世大学校友](../Category/苏黎世大学校友.md "wikilink")
[Category:中央研究院研究员](../Category/中央研究院研究员.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:中华民国大陆时期教师](../Category/中华民国大陆时期教师.md "wikilink")
[Category:中国药学家](../Category/中国药学家.md "wikilink")
[Category:第三届全国人大代表](../Category/第三届全国人大代表.md "wikilink")
[Category:第二届全国政协委员](../Category/第二届全国政协委员.md "wikilink")
[Category:第三届全国政协委员](../Category/第三届全国政协委员.md "wikilink")
[Category:第五屆全國政協委員](../Category/第五屆全國政協委員.md "wikilink")
[Category:扬州中学校友](../Category/扬州中学校友.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.