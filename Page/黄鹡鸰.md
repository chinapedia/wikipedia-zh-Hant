**黄鶺鸰**（[学名](../Page/学名.md "wikilink")：）为[鶺鸰科](../Page/鶺鸰科.md "wikilink")[鶺鸰属的](../Page/鶺鸰属.md "wikilink")[鸟类](../Page/鸟类.md "wikilink")。分布于古北区、阿拉斯加、非洲、亚洲等地，一般生活于河谷、林缘、原野、池畔及居民点附近以及从平原至海拔4000米以上的高原地区均可见其踪迹。该物种的模式产地在瑞典\[1\]。

## 生态环境

河谷、林缘、原野、池畔及居民点附近，从平原至海拔4000米以上的高原地区均可见其踪迹。

## 分布地域

[中国东南部地区至](../Page/中国.md "wikilink")[新疆](../Page/新疆.md "wikilink")、[广东](../Page/广东.md "wikilink")、[广西](../Page/广西.md "wikilink")、[福建](../Page/福建.md "wikilink")、[海南](../Page/海南.md "wikilink")、[台湾](../Page/台湾.md "wikilink")；古北区、[阿拉斯加西部](../Page/阿拉斯加.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[印度](../Page/印度.md "wikilink")、[亚洲东南部](../Page/亚洲.md "wikilink")。

## 特征

身体纤细，体长15-16cm，带褐色或橄榄色；虹膜－褐色；嘴－褐色；脚－褐至黑色。
叫声－为重复的叫声间杂颤鸣声；並非直線飛行，而是一上一下地飛行，行走時尾巴會不斷上下擺動。

## 食物

以[昆蟲及種子为食](../Page/昆蟲.md "wikilink")。

## 繁殖

繁殖期3月開始，一次产4-8卵。

## 亚种

  - **黄鶺鸰北方东部亚种**（[学名](../Page/学名.md "wikilink")：）。分布于前苏联、蒙古、老挝、越南、缅甸、泰国以及[中国的](../Page/中国.md "wikilink")[北京](../Page/北京.md "wikilink")、[河北](../Page/河北.md "wikilink")、[天津](../Page/天津.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[四川](../Page/四川.md "wikilink")、[云南等地](../Page/云南.md "wikilink")。该物种的模式产地在西伯利亚Sharagolskaia,Transbaikalia\[2\]。
  - **黄鶺鸰北方西部亚种**（[学名](../Page/学名.md "wikilink")：）。分布于前苏联、印度、锡金、马来西亚、东非、肯尼亚以及[中国的](../Page/中国.md "wikilink")[新疆](../Page/新疆.md "wikilink")、[四川](../Page/四川.md "wikilink")、[西藏等地](../Page/西藏.md "wikilink")。该物种的模式产地在印度Dukhun\[3\]。
  - **黄鶺鸰准葛尔亚种**（[学名](../Page/学名.md "wikilink")：）。分布于蒙古、前苏联、伊朗、阿富汗、印度以及[中国的](../Page/中国.md "wikilink")[新疆等地](../Page/新疆.md "wikilink")。该物种的模式产地在新疆准噶尔盆地\[4\]。
  - **黄鶺鸰东北亚种**（[学名](../Page/学名.md "wikilink")：）。分布于前苏联、南至蒙古、朝鲜、印度、缅甸、泰国、马来亚半岛、苏门答腊以及[中国的](../Page/中国.md "wikilink")[内蒙古](../Page/内蒙古.md "wikilink")、东北、[甘肃](../Page/甘肃.md "wikilink")、[河北](../Page/河北.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[山东](../Page/山东.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[江苏](../Page/江苏.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[四川](../Page/四川.md "wikilink")、[云南](../Page/云南.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[福建](../Page/福建.md "wikilink")、[广东](../Page/广东.md "wikilink")、[广西](../Page/广西.md "wikilink")、[海南等地](../Page/海南.md "wikilink")。该物种的模式产地在西伯利亚符拉迪沃斯托克,即海参崴\[5\]。
  - **黄鶺鸰天山亚种**（[学名](../Page/学名.md "wikilink")：）。分布于前苏联、伊朗、阿富汗、俾路支、印度以及[中国的](../Page/中国.md "wikilink")[新疆等地](../Page/新疆.md "wikilink")。该物种的模式产地在印度\[6\]。
  - **黄鶺鸰极北亚种**（[学名](../Page/学名.md "wikilink")：）。分布于前苏联、印度以及[中国的东北](../Page/中国.md "wikilink")、[四川](../Page/四川.md "wikilink")、[湖北等地](../Page/湖北.md "wikilink")。该物种的模式产地在西伯利亚东北部的NihniKolymsk\[7\]。
  - **黄鶺鸰堪察加亚种**（[学名](../Page/学名.md "wikilink")：）。分布于前苏联西伯利亚Shelekhova湾沿岸、堪察加、千岛群岛、库页岛、朝鲜、日本、菲律宾、马来西亚、缅甸、印度、新几内亚、美国以及[中国的东北](../Page/中国.md "wikilink")、[河北](../Page/河北.md "wikilink")、[山西](../Page/山西.md "wikilink")、[山东](../Page/山东.md "wikilink")、[江苏](../Page/江苏.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[云南](../Page/云南.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[江西](../Page/江西.md "wikilink")、[福建](../Page/福建.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[广东等地](../Page/广东.md "wikilink")。该物种的模式产地在西伯利亚堪察加\[8\]。
  - **黄鶺鸰台湾亚种**（[学名](../Page/学名.md "wikilink")：）。分布于前苏联、菲律宾、印度支那、马来西亚、印度以及[中国的](../Page/中国.md "wikilink")[内蒙古](../Page/内蒙古.md "wikilink")、东北、[河北](../Page/河北.md "wikilink")、[山西](../Page/山西.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[青海](../Page/青海.md "wikilink")、[山东](../Page/山东.md "wikilink")、[江苏](../Page/江苏.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[广西](../Page/广西.md "wikilink")、[四川](../Page/四川.md "wikilink")、[福建](../Page/福建.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[广东](../Page/广东.md "wikilink")、[海南等地](../Page/海南.md "wikilink")。该物种的模式产地在台湾\[9\]。
  - **黄鶺鸰阿拉斯加亚种**（[学名](../Page/学名.md "wikilink")：）。分布于阿拉斯加、东至美国、南至Nushagak河、努尼瓦克岛、西伯利亚、阿纳德尔、南至阿纳德尔河、印度、爪哇以及[中国等地](../Page/中国.md "wikilink")。该物种的模式产地在前苏联Chukotski半岛\[10\]。

## 參考資料

  - \<<香港及華南鳥類>\>，尹璉、費嘉倫、林超英(1994)
  - \<<中國鳥類野外手冊>\>，馬敬能、菲利普斯、何芬奇(2000)
  - \<<觀鳥背後>\>，駱雅儀(2003)
  - \<<雀鳥分類名錄>\>，[香港觀鳥會](http://www.hkbws.org.hk)

## 参考文献

## 外部連結

  - [黃鶺鴒(Moticilla
    flava)(1)鳴聲](http://www.hkbws.org.hk/web/chi/birdcall/Ywagtai1.wav)香港觀鳥會鳥鳴集
  - [黃鶺鴒(Moticilla
    flava)(2)鳴聲](http://www.hkbws.org.hk/web/chi/birdcall/Ywagtail.wav)香港觀鳥會鳥鳴集

[Category:鹡鸰属](../Category/鹡鸰属.md "wikilink")

1.
2.

3.

4.

5.

6.

7.

8.

9.

10.