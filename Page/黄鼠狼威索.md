[I-am-Weasel.jpg](https://zh.wikipedia.org/wiki/File:I-am-Weasel.jpg "fig:I-am-Weasel.jpg")

**黃鼠狼威索**（）是一部[美國](../Page/美國.md "wikilink")[動畫](../Page/動畫.md "wikilink")，原創者為[大衛‧費斯](../Page/大衛‧費斯.md "wikilink")，在[卡通頻道播映](../Page/卡通頻道.md "wikilink")。從1997年到2000年，黃鼠狼威索脫離了[雞與牛](../Page/雞與牛.md "wikilink")，成為一部獨立的動畫，而[雞與牛中的其他角色也時常出現在這部動畫中](../Page/雞與牛.md "wikilink")。

## 角色

  - **威索（Weasel）**

<!-- end list -->

  -

<!-- end list -->

  -   - [黃鼠狼](../Page/黃鼠狼.md "wikilink")，聰明又多才多藝的天才人物，從鋼琴到哲學等專業技藝與知識無一不精，通常也是劇集中唯一的勝利者。

<!-- end list -->

  - **班布（Baboon）**

<!-- end list -->

  -

<!-- end list -->

  -   - [狒狒](../Page/狒狒.md "wikilink")，脾氣暴臊智商又低落的笨蛋，而且有很多語法錯誤（正如他把名字中的I.R.）。嫉妒威索，認為自己比他聰明而常常找威索比試，但下場都是被慘電，有藝術鑑賞天賦。
      - 班布只穿一件T恤，上面倒寫著*"I. R."*（我乃）。紅屁股經常被人笑話。
      - 幹蠢事或鬧笑話時背景會有猿猴吠叫的罐頭音效，嘲笑班布原始愚笨。

<!-- end list -->

  - **沒穿褲子先生（The Red Guy）**

<!-- end list -->

  -

<!-- end list -->

  -   - 是位全身紅通通的惡魔，但也被認為是愚蠢的惡魔。
      - 身為永遠的反派角色，總會以各種形象登場，但始終維持著奉承、滑稽又感到厭惡卻充滿吸引力的言行舉止。
      - 在[雞與牛亦有登場](../Page/雞與牛.md "wikilink")。
      - 威索給了他一個新的[口頭禪](../Page/口頭禪.md "wikilink")："大家好！我又來了！"（Hello\!\!
        It's me\!）。

## 參見

  - [雞與牛](../Page/雞與牛.md "wikilink")
  - [沒穿褲子先生](../Page/沒穿褲子先生.md "wikilink")

## 外部連結

  - [黃鼠狼威索](http://www.bcdb.com/cartoons/Hanna-Barbera_Studios/A-C/Cow_and_Chicken/I__M__Weasel/index.html)在卡通數據庫上的資料

[I](../Category/卡通卡通.md "wikilink")
[I](../Category/卡通網絡工作室.md "wikilink")
[I](../Category/虛構黃鼠狼.md "wikilink")
[I](../Category/食肉目主角故事.md "wikilink")
[I](../Category/靈長目主角故事.md "wikilink")