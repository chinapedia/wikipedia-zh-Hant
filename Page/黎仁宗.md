**黎仁宗**（；）名**黎邦基**（），又名**黎基隆**、**黎濬**，是[越南](../Page/越南.md "wikilink")[後黎朝第三任君主](../Page/後黎朝.md "wikilink")，1442年—1459年在位。他是黎太宗[黎元龍的三子](../Page/黎元龍.md "wikilink")。

即位後改元[大和](../Page/大和.md "wikilink")，年僅二歲，由宣慈太后[阮氏英攝政](../Page/阮氏英.md "wikilink")。宣慈太后當政時，頗有成就，如制定私人田產法律十四條，擊敗[占婆軍進犯](../Page/占婆.md "wikilink")，生擒占婆王[賁該](../Page/摩訶賁該.md "wikilink")。可是，太后聽信讒言，殺功臣[黎可等人](../Page/黎可.md "wikilink")，惹起大臣憤怒。1453年仁宗才親政。

1455年命[潘孚先修國史](../Page/潘孚先.md "wikilink")，修成《史記續編》，並制訂百官俸祿及王侯封地法。

1459年，仁宗和宣慈太后被兄長[諒山王](../Page/諒山.md "wikilink")[黎宜民殺死](../Page/黎宜民.md "wikilink")。

## 参考文献

{{-}}  |- style="text-align: center; background: \#FFE4E1;"
|align="center" colspan="3"|**黎仁宗** |-

[Category:後黎朝君主](../Category/後黎朝君主.md "wikilink")