**黎崱**（），[字](../Page/表字.md "wikilink")**景高**，[號](../Page/號.md "wikilink")**東山**，約生於13世紀60年代，約卒於14世紀40年代，[越南](../Page/越南.md "wikilink")[陳朝](../Page/陳朝_\(越南\).md "wikilink")[愛州](../Page/愛州.md "wikilink")（位於今[越南](../Page/越南.md "wikilink")[清化省](../Page/清化省.md "wikilink")）人，後歸附[元朝](../Page/元朝.md "wikilink")，是越南歷史著作《[安南志略](../Page/安南志略.md "wikilink")》的作者。

## 生平

### 早年

黎崱是[東晉](../Page/東晉.md "wikilink")[交州](../Page/交州.md "wikilink")[刺史阮敷後裔](../Page/刺史.md "wikilink")，年幼年時過繼給外祖舅黎琫，便改為黎氏。年長後曾入仕為[陳朝靜海軍節度使](../Page/陳朝_\(越南\).md "wikilink")[陳鍵的幕僚](../Page/陳鍵.md "wikilink")。

### 歸附[元朝](../Page/元朝.md "wikilink")

公元1285年，[中國元朝軍隊攻入越南](../Page/元朝.md "wikilink")，進入陳鍵駐守地點。陳鍵不敵而降，黎崱便與之一同入[元](../Page/元朝.md "wikilink")。（陳鍵在北上的途中，被[越南陳朝軍隊的箭射死](../Page/陳朝_\(越南\).md "wikilink")。）

黎崱到達[大都後](../Page/元大都.md "wikilink")，被[元室授為](../Page/元朝.md "wikilink")[侍郎](../Page/侍郎.md "wikilink")。後來，黎崱定居[湖北的](../Page/湖北.md "wikilink")[漢陽](../Page/漢陽_\(武漢三鎮\).md "wikilink")，結交士大夫，優遊山水，鑽研典籍。到14世紀40年代時逝世。\[1\]

## 著作

  - 《[安南志略](../Page/安南志略.md "wikilink")》：黎崱由於在[越南土生土長](../Page/越南.md "wikilink")，青年時期曾「十歲間奔走半國中，稍識山川地理」，加上晚年愛好書籍，因而對[越南史的認識有一定基礎](../Page/越南歷史.md "wikilink")。於是，他「采摭歷代國史、[交趾圖經](../Page/交趾.md "wikilink")，雜及方今混一典故」，寫成《[安南志略](../Page/安南志略.md "wikilink")》。\[2\]該書是[越南歷史文獻當中較早成書的其中一本](../Page/越南歷史.md "wikilink")。

<!-- end list -->

  - 其他：黎崱的若干詩文，被錄入《[越音詩集](../Page/越音詩集.md "wikilink")》及《[見聞小錄](../Page/見聞小錄.md "wikilink")》中。\[3\]

## 腳註

## 参考文献

  - 《[安南志略](../Page/安南志略.md "wikilink")》（點校本），黎崱撰，北京[中華書局](../Page/中華書局.md "wikilink")1995年版。
  - 《[東南亞歷史詞典](../Page/東南亞歷史詞典.md "wikilink")》（簡體字），[上海辭書出版社](../Page/上海辭書出版社.md "wikilink")1995年版。

[Category:越南歷史學家](../Category/越南歷史學家.md "wikilink")
[Category:元朝歷史學家](../Category/元朝歷史學家.md "wikilink")
[Category:越南陳朝官員](../Category/越南陳朝官員.md "wikilink")
[Category:元朝中央官員](../Category/元朝中央官員.md "wikilink")
[Z](../Category/黎姓.md "wikilink")
[Category:越南入仕中國者](../Category/越南入仕中國者.md "wikilink")

1.  《安南志略》[武尚清前言](../Page/武尚清.md "wikilink")，北京[中華書局](../Page/中華書局.md "wikilink")1995年版第1-2頁。
2.  《[安南志略](../Page/安南志略.md "wikilink")·自序》（點校本），黎崱撰，北京[中華書局](../Page/中華書局.md "wikilink")1995年版，第11頁。
3.  《[東南亞歷史詞典](../Page/東南亞歷史詞典.md "wikilink")·「黎崱」條》（簡體字），[上海辭書出版社](../Page/上海辭書出版社.md "wikilink")1995年版，第452頁。