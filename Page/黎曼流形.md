**黎曼流形**（Riemannian
manifold）是一個[微分流形](../Page/微分流形.md "wikilink")，其中每點*p*的[切空間都定義了](../Page/切空間.md "wikilink")[點積](../Page/點積.md "wikilink")，而且其數值隨*p*平滑地改變。它容許我們定義弧線長度、角度、面積、體積、[曲率](../Page/曲率.md "wikilink")、函數[梯度及](../Page/梯度.md "wikilink")[向量域的](../Page/向量場.md "wikilink")[散度](../Page/散度.md "wikilink")。

每個**R**<sup>*n*</sup>的平滑[子流形可以导出](../Page/子流形.md "wikilink")[黎曼度量](../Page/黎曼度量.md "wikilink")：把**R**<sup>*n*</sup>的[點積都限制於](../Page/點積.md "wikilink")[切空間內](../Page/切空間.md "wikilink")。實際上，根据[纳什嵌入定理](../Page/纳什嵌入定理.md "wikilink")，所有黎曼流形都可以這樣产生。

我們可以*定義*黎曼流形為和**R**<sup>*n*</sup>的平滑子流形是[等距同构的](../Page/等距同构.md "wikilink")[度量空間](../Page/度量空間.md "wikilink")，**等距**是指其[内蕴度量](../Page/内蕴度量.md "wikilink")（intrinsic
metric）和上述从**R**<sup>*n*</sup>导出的度量是相同的。这對建立[黎曼幾何是很有用的](../Page/黎曼幾何.md "wikilink")。

黎曼流形可以定义为平滑流形，其中给出了一个[切丛的正定二次形的光滑截面](../Page/切丛.md "wikilink")。它可產生度量空間：

如果γ : \[*a*, *b*\] → *M*是黎曼流形*M*中一段連續可微分的弧線，我們可以定義它的長度*L*（γ）為

\[L(\gamma) = \int_a^b \|\gamma'(t)\|\;dt\]
（注意：γ'（*t*）是切空間*M*在γ（*t*）點的元素；||·||是切空間的內積所得出的[範數](../Page/範數.md "wikilink")。）

使用这个长度的定义，每个[连通的黎曼流形](../Page/连通空间.md "wikilink")*M*很自然的成为一个[度量空間](../Page/度量空間.md "wikilink")（甚至是[長度度量空間](../Page/内蕴度量.md "wikilink")）：在*x*與*y*兩點之間的距離*d*（*x*,
*y*）定義為：

  -
    *d*(*x*,*y*) = [inf](../Page/下确界.md "wikilink"){ L(γ) :
    γ是连接*x*和*y*的一条光滑曲线}。

虽然黎曼流形通常是弯曲的，“直線”的概念依然存在：那就是[測地線](../Page/測地線.md "wikilink")。

在黎曼流形中，[測地線](../Page/測地線.md "wikilink")[完备的概念](../Page/完备.md "wikilink")，和[拓撲完备及](../Page/拓撲.md "wikilink")[度量完备是等价的](../Page/度量空间.md "wikilink")：每个完备性都可以推出其他的完备性，这就是[Hopf-Rinow定理的内容](../Page/Hopf-Rinow定理.md "wikilink")。

## 參看

  - [黎曼幾何](../Page/黎曼幾何.md "wikilink")
  - [芬斯勒流形](../Page/芬斯勒流形.md "wikilink")
  - [黎曼子流形](../Page/黎曼子流形.md "wikilink")
  - [假黎曼流形](../Page/假黎曼流形.md "wikilink")

## 參考

  - Jurgen Jost, *Riemannian Geometry and Geometric Analysis*, (2002)
    Springer-Verlag, Berlin ISBN 3-540-4267-2

[category:流形上的结构](../Page/category:流形上的结构.md "wikilink")

[Category:黎曼几何](../Category/黎曼几何.md "wikilink")
[L](../Category/微分几何.md "wikilink")