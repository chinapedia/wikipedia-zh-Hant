**黎純宗**（；），名**黎維祥**（）\[1\]，又名**黎維祜**\[2\]，是[越南](../Page/越南.md "wikilink")[後黎朝第二十二代君主](../Page/後黎朝.md "wikilink")，1732年—1735年在位。

## 生平

黎純宗生於正和二十年二月十五日（1699年3月16日），是[黎裕宗黎維禟長子](../Page/黎裕宗.md "wikilink")。[永慶四年](../Page/永慶.md "wikilink")（1732年）八月，[鄭-{杠}-廢皇帝](../Page/鄭杠.md "wikilink")[黎維祊為昏德公](../Page/黎維祊.md "wikilink")，改立黎維祥為帝，改元[龍德](../Page/龍德_\(黎純宗\).md "wikilink")，以生日為昌符聖節。

龍德四年閏四月十五日（1735年6月5日），黎維祥去世，得年三十七歲，[廟號](../Page/廟號.md "wikilink")**純宗**，[諡號](../Page/諡號.md "wikilink")**寬和敦敏柔遜謹恪沈潛坦易簡皇帝**\[3\]，葬於[平吳陵](../Page/平吳陵.md "wikilink")，權臣鄭-{杠}-立裕宗第十一子[黎維祳為帝](../Page/黎維祳.md "wikilink")，是為懿宗。

## 家庭

### 兄弟

  - 永慶帝（昏德公） [黎維祊](../Page/黎維祊.md "wikilink")（鄭氏所出）
  - [黎懿宗](../Page/黎懿宗.md "wikilink") 黎維祳
  - [黎維](../Page/黎維⿰礻密.md "wikilink")
  - [黎維](../Page/黎維⿰礻規.md "wikilink")

### 后宫

  - 柔慎皇太后\[4\]陶氏，[景兴三十六年](../Page/景兴.md "wikilink")（1775年）去世，谥柔慎皇太后，景兴四十四年（1783年），加谥柔慎慈寿安和皇太后\[5\]

### 子女

  - 長子[黎顯宗](../Page/黎顯宗.md "wikilink") 黎維祧

## 参考文献

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**純宗皇帝黎維祥** |-

[Category:後黎朝君主](../Category/後黎朝君主.md "wikilink")

1.  據《[大越史記全書](../Page/大越史記全書.md "wikilink")》和《[欽定越史通鑑綱目](../Page/欽定越史通鑑綱目.md "wikilink")》。
2.  據《[清實錄](../Page/清實錄.md "wikilink")》和《[清史稿](../Page/清史稿.md "wikilink")·越南傳》。
3.  《大越史記續編·純宗簡皇帝紀》
4.  一作“柔顺皇太后”
5.  《[皇越文选](../Page/皇越文选.md "wikilink")》卷五·追尊皇妣柔慎皇太后美字金笺文