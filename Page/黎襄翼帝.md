**黎襄翼**（；），名**黎瀠**（），又名**黎晭**（），諡號**襄翼帝**，是[越南](../Page/越南.md "wikilink")[後黎朝第九任君主](../Page/後黎朝.md "wikilink")。他殺害威穆帝[黎濬](../Page/黎濬.md "wikilink")，改元[洪順](../Page/洪順.md "wikilink")。

## 生平

襄翼帝早年被封**簡修公**（），後來在1509年殺威穆帝奪位。

襄翼帝當上皇帝後，初年能頒布教旨，敷敎愼罰，看起來即將大有作為，可是後來他卻和前任的威穆帝一樣，只是玩樂，不理政事，又大興土木，重役疲亡，在位期間國內史稱「小民失業，盗賊滋起」，[中國](../Page/中國.md "wikilink")[明朝使者諷稱他為](../Page/明朝.md "wikilink")「豬王」（）。黎瀠任內有數次內亂，又聽從匠人[武如蘇之言](../Page/武如蘇.md "wikilink")，大興土木，使得民眾困苦，士卒疲勞。最後他被一位大臣社堂燒香官[陳皓及其子](../Page/陳皓.md "wikilink")[陳昺所弒](../Page/陳昺_\(大臣\).md "wikilink")，廢他為**靈隱王**（）。《大越史記全書》則是記載，[陳暠造反](../Page/陳暠_\(義軍首領\).md "wikilink")，大臣[鄭惟㦃趁機弒帝](../Page/鄭惟㦃.md "wikilink")。[昭宗即位後](../Page/黎昭宗.md "wikilink")，於光紹二年五月二十八（1517年6月16日）尊為**襄翼帝**\[1\]。

《[大越史記全書](../Page/大越史記全書.md "wikilink")》記載：

襄翼帝妻子欽德皇后[阮氏禱](../Page/阮氏禱.md "wikilink")，同時被鄭惟㦃所弒，無子，僅有3個女兒，包括寶福公主。

## 家庭

### 妻妾

  - 欽德皇后[阮氏禱](../Page/阮氏禱.md "wikilink")

### 子女

  - 女
    1.  寶福公主[黎壽肅](../Page/黎壽肅.md "wikilink")
    2.  皇女[黎壽元](../Page/黎壽元.md "wikilink")
    3.  皇女[黎壽敬](../Page/黎壽敬.md "wikilink")

## 参考文献

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**黎襄翼帝** |-

[Category:後黎朝君主](../Category/後黎朝君主.md "wikilink")
[Category:越南遇刺身亡者](../Category/越南遇刺身亡者.md "wikilink")
[Category:後黎朝王爵](../Category/後黎朝王爵.md "wikilink")
[Category:後黎朝公爵](../Category/後黎朝公爵.md "wikilink")

1.