[blackbody-lg.png](https://zh.wikipedia.org/wiki/File:blackbody-lg.png "fig:blackbody-lg.png")降低時，[黑體輻射曲線峰值會移向較低](../Page/黑體輻射.md "wikilink")[強度和更長](../Page/強度.md "wikilink")[波長的位置](../Page/波長.md "wikilink")。黑體輻射曲線圖可以和經典模型[瑞利-金斯定律相比較](../Page/瑞利-金斯定律.md "wikilink")。\]\]
[PlanckianLocus.png](https://zh.wikipedia.org/wiki/File:PlanckianLocus.png "fig:PlanckianLocus.png")）設根據黑體的溫度變化的；這些顏色的[軌跡](../Page/軌跡.md "wikilink")，如上面的[CIE1931色彩空間圖所示](../Page/CIE1931色彩空間.md "wikilink")，為普朗克軌跡（或稱為黑體軌跡）。\]\]

在[熱力學中](../Page/熱力學.md "wikilink")，**黑体**（），旧称**绝对黑体**，是一个理想化的物体，它能夠[吸收外来的全部](../Page/吸收.md "wikilink")[电磁辐射](../Page/电磁辐射.md "wikilink")，並且不會有任何的[反射與](../Page/反射.md "wikilink")[透射](../Page/透射.md "wikilink")。隨著[溫度上升](../Page/溫度.md "wikilink")，黑體所輻射出來的[電磁波與光線則稱做](../Page/電磁波.md "wikilink")[黑體辐射](../Page/黑體辐射.md "wikilink")。這個名詞在1862年由[古斯塔夫·基爾霍夫所提出並引入](../Page/古斯塔夫·基爾霍夫.md "wikilink")[熱力學內](../Page/熱力學.md "wikilink")。

## 定義簡述

黑體對於任何[波长的](../Page/波长.md "wikilink")[电磁波的吸收係数为](../Page/电磁波.md "wikilink")1，[透射係数为](../Page/透射係数.md "wikilink")0。但黑體未必是黑色的，即使不反射任何的電磁波，它也可以放出電磁波，而這些電磁波的波長和能量則全取決於黑體的[溫度](../Page/溫度.md "wikilink")，不因其他因素而改變。

對於人的視覺而言，黑體在700[K以下時看起來是黑色的](../Page/絕對溫標.md "wikilink")，這是由於在700K之下的黑體所放出來的輻射能量很小且輻射波長在[可見光範圍之外](../Page/可見光.md "wikilink")。若黑體的溫度高過上述的溫度的話，黑體則不會再是黑色的了，它會開始變成紅色，並且隨著溫度的升高，而分別有橘色、黃色、白色等顏色出現，即黑體吸收和放出電磁波的過程遵循了[光譜](../Page/光譜.md "wikilink")，其[軌跡为普朗克軌跡](../Page/軌跡.md "wikilink")（或稱為黑體軌跡）。[黑體輻射實際上是黑體的](../Page/黑體輻射.md "wikilink")[熱輻射](../Page/熱輻射.md "wikilink")。在黑體的光譜中，由於高溫引起高頻率即短波長，因此較高溫度的黑體靠近光譜結尾的藍色區域而較低溫度的黑體靠近紅色區域。

在室溫下，黑體放出的基本為[紅外線](../Page/紅外線.md "wikilink")，但當溫度漲幅超過了百度之後，黑體開始放出[可見光](../Page/可見光.md "wikilink")，根據溫度的升高過程，分別變為紅色，橙色，黃色，白色和藍色。當黑體變為白色的時候，它同時會放出大量的[紫外線](../Page/紫外線.md "wikilink")。

黑体单位表面积的[辐射通量](../Page/辐射通量.md "wikilink")\(P\)与其温度的四次方成正比，即：

\[P=\sigma T^4\]
式中\(\sigma\)称为[斯特藩-玻爾茲曼常數](../Page/斯特藩-玻爾茲曼常數.md "wikilink")，又稱為斯特藩常数。

黑體的放射過程引發物理學家對[量子場內的](../Page/量子場.md "wikilink")[熱平衡狀態的興趣](../Page/熱平衡.md "wikilink")。在[經典物理中](../Page/經典物理.md "wikilink")，所有熱平衡的傅里葉模型都遵循[能量均分定理](../Page/能量均分定理.md "wikilink")。當物理學家使用經典物理解釋黑體時，不可避免的發生了[紫外災變](../Page/紫外災變.md "wikilink")，即用於計算黑體輻射強度的[瑞利-金斯定律在輻射頻率趨向於無窮大時計算結果也趨向於無窮大](../Page/瑞利-金斯定律.md "wikilink")。由於黑體可以用於檢驗熱平衡的性質，因為它放出的輻射遵循[熱力學](../Page/熱力學.md "wikilink")[散射](../Page/散射.md "wikilink")，歷史上對黑體的研究成為了[量子物理開始的契機](../Page/量子物理.md "wikilink")。

## 細節

在實驗室內，研究者們可以模擬最靠近黑體的設備是大型空腔表面所開的一個小洞。只要有光線射向這個小洞，光線便會在空腔內反射或者被空腔內的牆壁所吸收，而只剩下微乎極微的光線可以再由洞口射出，亦即入射的光線幾乎都被吸收了，而沒有反射。如此，這個小洞就有如一個黑體一般，而且當空腔開始加熱以後，小洞發出來的輻射所形成的[光譜將會以量子化计算且和空腔材質無關](../Page/光譜.md "wikilink")。依據[克希荷夫熱輻射定律](../Page/克希荷夫熱輻射定律.md "wikilink")，光譜的圖形只和空腔的[溫度有關](../Page/溫度.md "wikilink")，而和其他因素沒有關係。

## 解釋

### 黑體模擬裝置

[Black_body_realization.svg](https://zh.wikipedia.org/wiki/File:Black_body_realization.svg "fig:Black_body_realization.svg")
通常，可以用一個開有小孔的腔體模擬黑體。射入小孔的電磁波經過多次反射后也難以射出，可以認爲電磁波被完全吸收。因此這個小孔就成爲一個黑體。但對於波長大於小孔直徑的電磁波，將有部分被反射，因此對於這部分電磁波而言，小孔并不是黑體。

## 參考文獻

## 参见

  - [黑体辐射](../Page/黑体辐射.md "wikilink")
  - [普朗克公式](../Page/普朗克公式.md "wikilink")
  - [白体 (物理学)](../Page/白体_\(物理学\).md "wikilink")

[Category:红外线](../Category/红外线.md "wikilink")
[Category:热力学](../Category/热力学.md "wikilink")
[Category:传热](../Category/传热.md "wikilink")
[Category:電磁輻射](../Category/電磁輻射.md "wikilink")
[Category:天体物理学](../Category/天体物理学.md "wikilink")