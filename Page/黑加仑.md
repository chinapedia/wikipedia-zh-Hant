**黑加仑**（[学名](../Page/学名.md "wikilink")：），又名**黑醋栗**、**黑加倫子**、**黑豆果**、**黑茶藨子**、**黑穗醋栗**，为[虎耳草目](../Page/虎耳草目.md "wikilink")[茶藨子科小型灌木](../Page/茶藨子科.md "wikilink")，其成熟果实为黑色小浆果，可以食用，也可以加工成[果汁](../Page/果汁.md "wikilink")、[果酱等](../Page/果酱.md "wikilink")[食品](../Page/食品.md "wikilink")。

## 歷史

黑加侖原生於[中亞和](../Page/中亞.md "wikilink")[東歐](../Page/東歐.md "wikilink")。早於古代已有野生品種分布在[歐](../Page/歐洲.md "wikilink")[亞](../Page/亞洲.md "wikilink")。到了16世纪，[英國](../Page/英國.md "wikilink")、[荷蘭](../Page/荷蘭.md "wikilink")、[德國皆開始人工栽培](../Page/德國.md "wikilink")。有关黑加仑栽培的首次记录，出自英国17世纪初的药物志。因其果实和叶片的药用价值，而開始受到重视。

## 藥效

黑加仑含有非常丰富的维生素C、磷、镁、钾、钙、花青素、酚类物质。目前已经知道的黑加仑的保健功效包括预防痛风、贫血、水肿、关节炎、风湿病、口腔和咽喉疾病、咳嗽等。

## 参考资料

  -
[灌](../Category/藥用植物.md "wikilink")
[Category:水果](../Category/水果.md "wikilink")
[nigrum](../Category/茶藨子属.md "wikilink")