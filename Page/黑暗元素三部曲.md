《**-{zh-hans:黑暗物质三部曲;zh-hant:黑暗元素三部曲;}-**》（）是一套由[英国作家](../Page/英国.md "wikilink")[-{zh-hans:菲利普·普尔曼;zh-hant:菲力普·普曼;}-创作的](../Page/菲力普·普曼.md "wikilink")[奇幻小说](../Page/奇幻小说.md "wikilink")[三部曲](../Page/三部曲.md "wikilink")。其中包括1995年出版的《》、1997年出版的《》和2000年出版的《[-{zh-hans:琥珀望远镜;zh-hk:琥珀望遠鏡;zh-tw:琥珀望遠鏡;}-](../Page/琥珀望遠鏡.md "wikilink")》。

截至2008年《黑暗元素三部曲》已被翻譯20多種語言出版，並在全球發行近一千萬套\[1\]\[2\]。

作者普曼目前正執筆撰寫該系列的續篇。

## 三部曲

### 黃金羅盤

[萊拉·貝拉克自幼父母雙亡](../Page/萊拉·貝諾誇.md "wikilink")，在[牛津的約旦學院中由諸位學者撫養教育](../Page/牛津.md "wikilink")，只是萊拉是個個性野蠻，常常說謊的人，常常到學院的屋頂或地窖探險，或者率領牛津街坊的玩伴與外來的[吉普賽孩子打泥巴仗](../Page/吉普賽人.md "wikilink")。這般無憂無慮的生活，自從萊拉偷偷潛入院長貴賓室後而改變，並展開了一場冒險。萊拉在旅程中將會發現，並非她的伯父，而是親生父親；而考爾特夫人則是萊拉的親生母親\[3\]。

### 奧秘匕首

萊拉在一個充滿幽靈的城市喜喀則中遇見了，在威爾從前任匕首人取得奧秘匕首後，展開了一場穿越各個世界的冒險。而萊拉命運的預言亦完整地揭露出來。

### 琥珀望遠鏡

## 主要角色

  - [萊拉·貝拉克](../Page/萊拉·貝諾誇.md "wikilink")（Lyra Belacqua）
  - 潘拉蒙 （Pantalaimon）
  - 威尔·帕里（Will Parry）
  - 考爾特夫人（Mrs. Coulter）
  - 艾塞列公爵（Lord Asriel）
  - 羅傑·帕斯洛（Roger Parslow）
  - 歐瑞克·拜尼森（Iorek Byrnison）
  - 約翰·法 （John Faa）
  - 法德·克朗 （Farder Coram）
  - 可斯塔媽媽（Ma Costa）
  - 比利·可斯塔 （Billy Costa）
  - 李·史柯比 （Lee Scoresby）
  - 席拉芬娜·帕可拉（Serafina Pekkala）

## 中文版本

2001年，[中國](../Page/中華人民共和國.md "wikilink")[上海譯文出版社買下了](../Page/上海譯文出版社.md "wikilink")《黑暗元素三部曲》的出版權，至2006年出版完成。2007年當電影《[黃金羅盤](../Page/黃金羅盤.md "wikilink")》上映時，上海譯文出版社推出了《黑暗元素三部曲》的再版，為每本書增加了導讀\[4\]。

台灣譯本最初於2001年出版，由王晶翻譯。

## 獲獎及殊榮

三部曲中的首部《黃金羅盤》獲頒1995年的[卡內基獎章](../Page/卡內基獎章.md "wikilink")\[5\]，以及1996年的英國\[6\]。2007年6月21日，該作更獲選為卡內基獎章歷年來得獎作品中的最佳作品，卡內基獎章評委會表示：「這本書重新定義了兒童文學，改變了人們看兒童讀物的視角和思維。」\[7\]

第三部《琥珀望遠鏡》獲頒2001年的英國惠特布里德文学奖\[8\]。

在2003年[英國廣播公司](../Page/英國廣播公司.md "wikilink")（BBC）「[大閱讀](../Page/大閱讀.md "wikilink")」活動票選的「有史以來最受讀者喜愛的小說」中，《黑暗元素三部曲》贏得6.3萬張選票，位居第三名，排在《[魔戒](../Page/魔戒.md "wikilink")》和《[傲慢與偏見](../Page/傲慢與偏見.md "wikilink")》之後\[9\]\[10\]\[11\]。

## 改編作品

《黑暗元素三部曲》已被改編為各種形式的媒體，包括[舞台劇與](../Page/舞台劇.md "wikilink")[電影](../Page/電影.md "wikilink")。2015年，當菲力普·普曼得知《黑暗元素》將被[BBC改編為](../Page/BBC.md "wikilink")[電視劇後便表示](../Page/電視劇.md "wikilink")：「看到這個故事被改編成不同的形式、通過不同的媒體呈現，讓我獲得了源源不斷的樂趣。它被改成過廣播劇、舞台劇、有聲書、電影，還有漫畫，而現在又有了電視版本」\[12\]。

### 舞台劇

2003年《黑暗元素三部曲》被改編為舞台劇，於[倫敦皇家劇院演出](../Page/倫敦皇家歌劇院.md "wikilink")\[13\]。

### 電影

2007年，[新線影業製作](../Page/新線影業.md "wikilink")、改編自《黑暗元素三部曲》首部曲的電影《[黃金羅盤](../Page/黃金羅盤.md "wikilink")》上映，由[克里斯·魏茲執導](../Page/克里斯·魏茲.md "wikilink")，[戴可塔·普魯·李察士](../Page/戴可塔·普魯·李察士.md "wikilink")、[妮可·基嫚](../Page/妮可·基嫚.md "wikilink")、[丹尼爾·克雷格](../Page/丹尼爾·克雷格.md "wikilink")、[伊娃·葛林](../Page/伊娃·葛林.md "wikilink")、[伊恩·麥克連](../Page/伊恩·麥克連.md "wikilink")、[克里斯多福·李等人主演](../Page/克里斯多福·李.md "wikilink")\[14\]。

### 電視劇

2015年11月，BBC宣布將製作《黑暗元素三部曲》的電視劇版，由[新線影業和Bad](../Page/新線影業.md "wikilink")
Wolf公司共同製作\[15\]\[16\]。

## 參見

  -
  -
  - [很久很久以前，在北方](../Page/很久很久以前，在北方.md "wikilink")

  - [奇幻小說](../Page/奇幻小說.md "wikilink")

  - [納尼亞傳奇](../Page/納尼亞傳奇.md "wikilink")

  - [魔戒](../Page/魔戒.md "wikilink")

  - [哈利波特](../Page/哈利波特.md "wikilink")

## 參考資料

## 外部链接

  - [Philip Pullman](http://www.philip-pullman.com/)，作者官方网站
  - [Scholastic: *His Dark
    Materials*](https://web.archive.org/web/20070825171041/http://www.scholastic.co.uk/zone/book_philip-pullman-hdm.htm)，英国出版商官方网站
  - [Randomhouse: *His Dark
    Materials*](http://www.randomhouse.com/features/pullman/)，美国出版商官方网站
  - [《黄金罗盘》电影官方网站](http://www.goldencompassmovie.com)

[HD](../Category/兒童奇幻小說.md "wikilink")
[HD](../Category/平行世界題材小說.md "wikilink")
[D](../Category/小說三部曲.md "wikilink")
[HD](../Category/宗教題材作品.md "wikilink")
[Category:英國奇幻小說](../Category/英國奇幻小說.md "wikilink")
[黑暗元素三部曲](../Category/黑暗元素三部曲.md "wikilink")

1.
2.
3.  <http://www.eslitebooks.com/Program/Object/BookCN.aspx?PageNo=&PROD_ID=2680293119009>
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.