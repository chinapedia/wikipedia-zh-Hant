**黑森家族**可追溯至1264年，[布拉班特公爵亨利二世](../Page/布拉班特公爵.md "wikilink")（Henri
II，1207年-1248年）与第二位妻子[图林根公主](../Page/图林根.md "wikilink")[索非亚](../Page/索非亚.md "wikilink")（1224年-1275年）的长子“孩子”亨利（在亨利二世所有儿子中排行第二，所以不能继承[布拉班特公国](../Page/布拉班特公国.md "wikilink")）在这一年被封为**黑森伯爵**。[黑森的所有统治者都属于](../Page/黑森.md "wikilink")[洛林-布拉班特王朝](../Page/洛林-布拉班特王朝.md "wikilink")。

“孩子”亨利的曾祖父是[下洛林公爵](../Page/洛林统治者列表.md "wikilink")[高德瓦尔德三世](../Page/高德瓦尔德三世.md "wikilink")（Godevaard
III），外祖父是[图林根伯爵](../Page/图林根.md "wikilink")[路德维希四世](../Page/路德维希四世.md "wikilink")。他的祖先[马斯郜的雷基那尔一世](../Page/马斯郜的雷基那尔一世.md "wikilink")（Reginar
I von
Maasgau）作为[皇帝](../Page/神圣罗马帝国皇帝列表.md "wikilink")[洛泰尔一世的外孙被封为](../Page/洛泰尔一世.md "wikilink")[洛林公爵](../Page/洛林统治者列表.md "wikilink")，随着[卡洛林王朝的灭亡](../Page/卡洛林王朝.md "wikilink")，[洛林被当地的贵族们瓜分](../Page/洛林.md "wikilink")。[雷基那尔一世的曾孙](../Page/雷基那尔一世.md "wikilink")[朗贝尔一世](../Page/朗贝尔一世.md "wikilink")（Lambert
I）成了[鲁文伯爵](../Page/鲁文伯爵.md "wikilink")（Comte de
Louvain）。1106年，当时的鲁文伯爵[高德瓦尔德一世从](../Page/高德瓦尔德一世.md "wikilink")[下洛林公爵](../Page/洛林统治者列表.md "wikilink")[林堡伯爵交易到](../Page/林堡伯爵.md "wikilink")[下洛林公国](../Page/下洛林公国.md "wikilink")。由于当地贵族的武力反对，鲁文伯爵家族于1186年用[洛林换来](../Page/洛林.md "wikilink")[布拉班特](../Page/布拉班特.md "wikilink")，[下洛林公爵](../Page/洛林统治者列表.md "wikilink")[高德瓦尔德三世成为](../Page/高德瓦尔德三世.md "wikilink")[布拉班特伯爵](../Page/布拉班特伯爵.md "wikilink")，他的儿子[亨利一世将](../Page/亨利一世.md "wikilink")[布拉班特升为公国](../Page/布拉班特公国.md "wikilink")。作为[布拉班特和](../Page/布拉班特.md "wikilink")[图林根联姻的后代](../Page/图林根.md "wikilink")“孩子”亨利应拥有广大的土地，何况他母亲的叔叔[亨利·拉斯佩曾与](../Page/亨利·拉斯佩.md "wikilink")[皇帝](../Page/神圣罗马帝国皇帝列表.md "wikilink")[腓特烈二世争夺过王位](../Page/腓特烈二世_\(神圣罗马帝国\).md "wikilink")。然而，亨利二世与索非亚的一个表弟，[迈森的亨利](../Page/迈森的亨利.md "wikilink")（[维丁王朝的祖先](../Page/维丁王朝.md "wikilink")）取得了[萨克森和](../Page/萨克森.md "wikilink")[图林根的广阔领地](../Page/图林根.md "wikilink")，“孩子”亨利只得到[黑森作为安慰](../Page/黑森.md "wikilink")。

## [黑森伯爵](../Page/黑森伯爵.md "wikilink")

  - 1264年-1308年：[亨利一世](../Page/亨利一世.md "wikilink")（Heinrich I ），稱"das
    Kind"， <small>绰号“孩子”</small>
  - 1308年-1328年：[奥托一世](../Page/奥托一世.md "wikilink")（Otto I）
  - 1308年-1311年：[约翰](../Page/约翰.md "wikilink")（Johann
    ），<small>下黑森伯爵，“孩子”亨利的第四子，无嗣</small>
  - 1328年-1377年：[亨利二世](../Page/亨利二世.md "wikilink")（Heinrich II），稱 "der
    Eiserne" ，<small>奥托一世长子，在马尔堡，绰号“铁人”</small>
  - 1328年-1345年：[路德维希一世](../Page/路德维希一世.md "wikilink")（Ludwig I），
    <small>奥托一世第三子，在格鲁本施泰因</small>
  - 1328年-1368/70年：[赫尔曼一世](../Page/赫尔曼一世.md "wikilink")（Hermann I von
    Hessen-Nordeck ），<small>奥托一世第四子，在诺德克</small>
  - 1367年-1413年：[赫尔曼二世](../Page/赫尔曼二世.md "wikilink")（Hermann II
    ），<small>路德维希一世之子</small>
  - 1413年-1458年：[路德维希二世](../Page/路德维希二世.md "wikilink")（Ludwig II）

1458年，[黑森分为](../Page/黑森伯国.md "wikilink")**[黑森-卡塞尔](../Page/黑森-卡塞尔.md "wikilink")**和**[黑森-马尔堡](../Page/黑森-马尔堡.md "wikilink")**。

### 黑森-卡塞尔伯爵

  - 1458年-1471年：[路德维希二世](../Page/路德维希二世.md "wikilink")（Ludwig II von
    Hessen-Kassel ），<small>路德维希二世长子</small>
  - 1471年-1493年：[威廉一世](../Page/威廉一世.md "wikilink")（Wilhelm I
    ），<small>路德维希三世长子</small>
  - 1493年-1509年：[威廉二世](../Page/威廉二世.md "wikilink")（Wilhelm II），
    <small>路德维希三世次子，1500年得到马尔堡</small>
  - 1509年-1567年：[菲力普一世](../Page/菲力普一世_\(黑森\).md "wikilink")（Philipp I
    ），稱"der Grossmüthige"， <small>威廉二世的次子，绰号“宽宏的”</small>

1567年，[黑森分为](../Page/黑森伯国.md "wikilink")**[黑森-卡塞尔](../Page/黑森-卡塞尔伯国.md "wikilink")**、**[黑森-马尔堡](../Page/黑森-马尔堡伯国.md "wikilink")**、**[黑森-莱茵菲尔斯](../Page/黑森-莱茵菲尔斯伯国.md "wikilink")**和**[黑森-达姆施塔特](../Page/黑森-达姆施塔特伯国.md "wikilink")**。

### 黑森-马尔堡伯爵

  - 1458年-1483年：[亨利三世](../Page/亨利三世.md "wikilink")（Heinrich III von
    Hessen-Marburg ），<small>路德维希二世次子</small>
  - 1483年-1500年：[威廉三世](../Page/威廉三世.md "wikilink")（Wilhelm III）

[威廉三世没有子嗣](../Page/威廉三世.md "wikilink")，他死后[黑森-马尔堡并入](../Page/黑森-马尔堡.md "wikilink")[黑森-卡塞尔](../Page/黑森-卡塞尔.md "wikilink")。

## [黑森-卡塞尔伯爵](../Page/黑森-卡塞尔伯爵.md "wikilink")

  - 1567年-1592年：[威廉四世](../Page/威廉四世_\(黑森-卡塞尔\).md "wikilink")（Wilhelm IV
    von Hessen-Kassel ），<small>菲力普一世长子</small>
  - 1592年-1627年：[莫里茨](../Page/莫里茨_\(黑森-卡塞尔\).md "wikilink")（Moritz）

<!-- end list -->

  -
    1627年，[黑森-卡塞尔分出](../Page/黑森-卡塞尔.md "wikilink")**黑森-罗滕堡**和**黑森-埃施魏格**，1683年分出**黑森-莱茵菲尔斯-罗滕堡**。

<!-- end list -->

  - 1627年-1637年：[威廉五世](../Page/威廉五世_\(黑森-卡塞尔\).md "wikilink")（Wilhelm V
    von Hessen-Kassel ），<small>莫里茨第三子</small>
  - 1637年-1663年：[威廉六世](../Page/威廉六世_\(黑森-卡塞尔\).md "wikilink")（Wilhelm
    VI）

<!-- end list -->

  -
    1663年，黑森-卡塞尔分出**[黑森-菲利普斯塔尔](../Page/黑森-菲利普斯塔尔.md "wikilink")**

<!-- end list -->

  - 1663年-1670年：[威廉七世](../Page/威廉七世_\(黑森-卡塞尔\).md "wikilink")（Wilhelm
    VII）， <small>威廉六世长子</small>
  - 1670年-1730年：[卡尔](../Page/卡尔_\(黑森-卡塞尔\).md "wikilink")（Karl
    ），<small>威廉六世次子</small>
  - 1730年-1751年：[弗里德里希一世](../Page/弗雷德里克一世_\(瑞典\).md "wikilink")（Friedrich
    I ），<small>卡尔第三子，1720年起任[瑞典国王](../Page/瑞典君主列表.md "wikilink")</small>
  - 1751年-1760年：[威廉八世](../Page/威廉八世_\(黑森-卡塞尔\).md "wikilink")（Wilhelm
    VIII ），<small>卡尔第六子</small>
  - 1760年-1785年：[弗里德里希二世](../Page/弗里德里希二世_\(黑森-卡塞尔\).md "wikilink")（Friedrich
    II）
  - 1785年-1803年：[威廉九世](../Page/威廉一世_\(黑森选侯\).md "wikilink")（Wilhelm IX
    ）<small>1803年成为黑森选侯</small>

### 黑森-菲利普斯塔尔伯爵

  - 1663年-1721年：[菲利普](../Page/菲利普.md "wikilink")（Philipp von
    Hessen-Philippsthal ），<small>威廉六世第三子</small>

<!-- end list -->

  -
    1721年黑森-菲力普斯塔尔分出**黑森-菲利普斯塔尔-巴赫菲尔德**

<!-- end list -->

  - 1721年-1770年：[卡尔](../Page/卡尔.md "wikilink")（Karl），
    <small>菲力普的长子</small>
  - 1770年-1806年：[威廉](../Page/威廉.md "wikilink")（Wilhelm
    ），<small>1810年去世</small>

<!-- end list -->

  -
    1806年黑森-菲利普斯塔尔并入[威斯特法伦王国](../Page/威斯特法伦王国.md "wikilink")，1813年作为黑森-卡塞尔选侯国的一个邦国复辟。

<!-- end list -->

  - 1813年-1816年：[路德维希](../Page/路德维希.md "wikilink")（Ludwig）
  - 1816年-1849年：[恩斯特·康斯坦丁](../Page/恩斯特·康斯坦丁.md "wikilink")（Ernst
    Konstantin）
  - 1849年-1868年：[卡尔](../Page/卡尔.md "wikilink")（Karl ），

1866年黑森-菲利普斯塔尔被[普鲁士吞并](../Page/普鲁士.md "wikilink")，但伯爵称号仍然保留。

  - 1868年-1925年：[恩斯特](../Page/恩斯特.md "wikilink")（Ernst）

恩斯特没有子嗣，黑森-菲利普斯塔尔于1925年绝嗣。

#### 黑森-菲利普斯塔尔-巴赫菲尔德伯爵

  - 1721年-1761年：[威廉](../Page/威廉.md "wikilink")（Wilhelm von
    Hessen-Philippsthal-Barchfeld）， <small>菲利普第三子</small>
  - 1761年-1803年：[阿道夫](../Page/阿道夫.md "wikilink")（Adolf）
  - 1803年-1806年：[卡尔](../Page/卡尔.md "wikilink")（Karl ）

<!-- end list -->

  -
    1806年黑森-菲利普斯塔尔-巴赫菲尔德并入[威斯特法伦王国](../Page/威斯特法伦王国.md "wikilink")，1813年作为黑森-卡塞尔选侯国的一个邦国复辟。

<!-- end list -->

  - 1813年-1854年：卡尔（Karl ）
  - 1854年-1866年：[亚历克西斯](../Page/亚历克西斯_\(黑森-菲利普斯塔尔-巴赫菲尔德\).md "wikilink")（Alexis）

1866年黑森-菲利普斯塔尔-巴赫菲尔德被[普鲁士吞并](../Page/普鲁士.md "wikilink")，但伯爵称号仍然保留。

### 黑森-罗滕堡伯爵

  - 1627年-1658年：[赫尔曼](../Page/赫尔曼.md "wikilink")（Hermann von
    Hessen-Rotenburg
    ），<small>[莫里茨第五子](../Page/莫里茨.md "wikilink")，无子</small>

### 黑森-埃施魏格伯爵

  - 1627年-1655年：[弗里德里希](../Page/弗里德里希.md "wikilink")（Friedrich von
    Hessen-Eschwege ），<small>莫里茨第七子，没有子嗣</small>

### 黑森-莱茵菲尔斯-罗滕堡伯爵

  - 1683年-1693年：[恩斯特一世](../Page/恩斯特一世.md "wikilink")（Ernst I of
    Hessen-Rheinfels-Rotenburg ），<small>莫里茨第九子</small>

<!-- end list -->

  -
    1693年，黑森-莱茵菲尔斯-罗滕堡分出**黑森-万弗里德**。

<!-- end list -->

  - 1693年-1725年：[威廉](../Page/威廉.md "wikilink")（Wilhelm
    ），<small>恩斯特一世长子</small>
  - 1725年-1749年：[恩斯特·利奥波德](../Page/恩斯特·利奥波德.md "wikilink")（Ernst
    Leopold）
  - 1749年-1778年：[康斯坦丁](../Page/康斯坦丁.md "wikilink")（Konstantin ）
  - 1778年-1806年：[卡尔·埃曼努埃尔](../Page/卡尔·埃曼努埃尔.md "wikilink")（Karl Emanuel
    ），<small>1812年去世</small>

<!-- end list -->

  -
    1806年黑森-莱茵菲尔斯-罗滕堡并入[威斯特法伦王国](../Page/威斯特法伦王国.md "wikilink")，1813年作为黑森-卡塞尔选侯国的一个邦国复辟。

<!-- end list -->

  - 1813年-1834年：[维克多·阿玛多依斯](../Page/维克多·阿玛多依斯.md "wikilink")（Viktor
    Amadeus）， <small>被封为拉提波和科尔维公爵（*Herzog von Ratibor und
    Corvey*）</small>

维克多·阿玛多依斯没有子嗣，黑森-莱茵菲尔斯-罗滕堡于1834年并入黑森-卡塞尔选侯国。

#### 黑森-万弗里德伯爵

  - 1693年-1711年：[卡尔](../Page/卡尔.md "wikilink")（Karl von Hessen-Wanfried
    ），<small>恩斯特一世次子</small>
  - 1711年-1731年：[威廉](../Page/威廉.md "wikilink")（Wilhelm），

威廉没有子嗣，黑森-万弗里德并入黑森-莱茵菲尔斯-罗滕堡。

## [黑森-马尔堡伯爵](../Page/黑森-马尔堡伯爵.md "wikilink")

  - 1567年-1604年：[路德维希四世](../Page/路德维希四世.md "wikilink")（Ludwig IV），
    <small>[菲利普一世第三子](../Page/菲利普一世.md "wikilink")</small>

1604年[路德维希四世去世](../Page/路德维希四世.md "wikilink")，没有子嗣，[黑森-卡塞尔和](../Page/黑森-卡塞尔.md "wikilink")[黑森-达姆施塔特都要求得到](../Page/黑森-达姆施塔特.md "wikilink")[马尔堡](../Page/马尔堡.md "wikilink")，最终在1648年，[黑森-卡塞尔和](../Page/黑森-卡塞尔.md "wikilink")[黑森-达姆施塔特平分了](../Page/黑森-达姆施塔特.md "wikilink")[黑森-马尔堡](../Page/黑森-马尔堡.md "wikilink")。

## [黑森-莱茵菲尔斯伯爵](../Page/黑森-莱茵菲尔斯伯爵.md "wikilink")

  - 1567年-1583年：[菲利普二世](../Page/菲利普二世.md "wikilink")（Philipp II von
    Hesse-Rheinfels ），<small>菲利普一世第四子</small>

[菲力普二世没有子嗣](../Page/菲力普二世.md "wikilink")，他死后国土被[黑森-卡塞尔](../Page/黑森-卡塞尔.md "wikilink")、[黑森-马尔堡和](../Page/黑森-马尔堡.md "wikilink")[黑森-达姆施塔特平分](../Page/黑森-达姆施塔特.md "wikilink")。

## [黑森-达姆施塔特伯爵](../Page/黑森-达姆施塔特伯爵.md "wikilink")

  - 1567年-1596年：[格奥尔格一世](../Page/格奥尔格一世_\(黑森-达姆施塔特\).md "wikilink")（Georg
    I von Hessen-Darmstadt， <small>菲利普一世第五子</small>

<!-- end list -->

  -
    1596年，黑森-达姆施塔特分出**黑森-布茨巴赫**和**黑森-霍姆堡**。

<!-- end list -->

  - 1596年-1626年：[路德维希五世](../Page/路德维希五世_\(黑森-达姆施塔特\).md "wikilink")（Ludwig
    V）， <small>格奥尔格一世次子</small>
  - 1626年-1661年：[格奥尔格二世](../Page/格奥尔格二世_\(黑森-达姆施塔特\).md "wikilink")（Georg
    II ），<small>路德维希五世长子</small>

<!-- end list -->

  -
    1626年，黑森-达姆施塔特分出**黑森-布劳巴赫**：
      -
        1626年-1651年：约翰（Johann von Hessen-Braubach），
        <small>黑森-布劳巴赫伯爵，路德维希五世次子，无嗣</small>

<!-- end list -->

  - 1661年-1678年：[路德维希六世](../Page/路德维希六世_\(黑森-达姆施塔特\).md "wikilink")（Ludwig
    VI）， <small>格奥尔格二世长子</small>

<!-- end list -->

  -
    1661年，黑森-达姆施塔特分出**黑森-伊特尔**：
      -
        1661年-1676年：格奥尔格（Georg von Hessen-Itter
        ），<small>黑森-伊特尔伯爵，格奥尔格二世次子，无嗣</small>

<!-- end list -->

  - 1678年：[路德维希七世](../Page/路德维希七世_\(黑森-达姆施塔特\).md "wikilink")（Ludwig
    VII）
  - 1678年-1739年：[恩斯特·路德维希](../Page/恩斯特·路德维希_\(黑森-达姆施塔特\).md "wikilink")（Ernst
    Ludwig）
  - 1739年-1768年：[路德维希八世](../Page/路德维希八世_\(黑森-达姆施塔特\).md "wikilink")（Ludwig
    VIII）
  - 1768年-1790年：[路德维希九世](../Page/路德维希九世_\(黑森-达姆施塔特\).md "wikilink")（Ludwig
    IX）
  - 1790年-1806年：[路德维希十世](../Page/路德维希一世_\(黑森大公\).md "wikilink")（Ludwig X
    ），<small>1806年成为黑森大公</small>

### 黑森-布茨巴赫伯爵

  - 1596年-1643年：[菲利普](../Page/菲利普.md "wikilink")（Philipp von
    Hessen-Butzbach）， <small>格奥尔格一世第三子，无嗣</small>

黑森-布茨巴赫于1643年绝嗣，归于黑森-达姆施塔特。

### 黑森-霍姆堡伯爵

  - 1596年-1638年：[弗里德里希一世](../Page/弗里德里希一世_\(黑森-霍姆堡\).md "wikilink")（Friedrich
    I von Hessen-Homburg ），<small>格奥尔格一世第四子</small>
  - 1638年-1708年：[弗里德里希二世](../Page/弗里德里希二世_\(黑森-霍姆堡\).md "wikilink")（Friedrich
    II）
  - 1708年-1746年：[弗里德里希三世·雅各布](../Page/弗里德里希三世_\(黑森-霍姆堡\).md "wikilink")（Friedrich
    III Jakob）
  - 1746年-1751年：[弗里德里希四世·卡尔](../Page/弗里德里希四世_\(黑森-霍姆堡\).md "wikilink")（Friedrich
    IV Karl）， <small>弗里德里希三世·雅各布之侄</small>
  - 1751年-1820年：[弗里德里希五世·路德维希](../Page/弗里德里希五世_\(黑森-霍姆堡\).md "wikilink")（Friedrich
    V Ludwig）
  - 1820年-1829年：[弗里德里希六世·约瑟夫](../Page/弗里德里希六世_\(黑森-霍姆堡\).md "wikilink")（Friedrich
    VI Joseph）， <small>弗里德里希五世·路德维希长子</small>
  - 1829年-1839年：[路德维希](../Page/路德维希_\(黑森-霍姆堡\).md "wikilink")（Ludwig
    ），<small>弗里德里希五世·路德维希次子</small>
  - 1839年-1846年：[菲利普](../Page/菲利普_\(黑森-霍姆堡\).md "wikilink")（Philipp），
    <small>弗里德里希五世·路德维希第五子</small>
  - 1846年-1848年：[古斯塔夫](../Page/古斯塔夫_\(黑森-霍姆堡\).md "wikilink")（Gustav），
    <small>弗里德里希五世·路德维希第六子</small>
  - 1848年-1866年：[斐迪南](../Page/斐迪南_\(黑森-霍姆堡\).md "wikilink")（Ferdinand
    ），<small>弗里德里希五世·路德维希第七子</small>

弗里德里希五世·路德维希的儿子们都没有子嗣，因此[黑森-霍姆堡于](../Page/黑森-霍姆堡.md "wikilink")1866年绝嗣，被并入[黑森和莱茵大公国](../Page/黑森和莱茵大公国.md "wikilink")。

## [黑森（-卡塞尔）选侯](../Page/黑森選侯.md "wikilink")

黑森-卡塞尔于1803年被[拿破仑升为](../Page/拿破仑.md "wikilink")[选侯国](../Page/选侯国.md "wikilink")。

  - 1803年-1806年，1813年-1821年：[威廉一世](../Page/威廉一世_\(黑森选侯\).md "wikilink")（Wilhelm
    I）

<!-- end list -->

  -
    1806年黑森选侯国并入[威斯特法伦王国](../Page/威斯特法伦王国.md "wikilink")，1813年复辟。

<!-- end list -->

  - 1821年-1847年：[威廉二世](../Page/威廉二世_\(黑森选侯\).md "wikilink")（Wilhelm II）
  - 1847年-1866年：[弗里德里希·威廉](../Page/弗里德里希·威廉_\(黑森选侯\).md "wikilink")（Friedrich
    Wilhelm）

1866年，[黑森选侯国被](../Page/黑森选侯国.md "wikilink")[普鲁士吞并](../Page/普鲁士.md "wikilink")，成为[普鲁士](../Page/普鲁士.md "wikilink")[黑森-拿骚省的一部分](../Page/黑森-拿骚省.md "wikilink")。

## [黑森（-达姆施塔特）和莱茵大公](../Page/黑森和萊茵大公.md "wikilink")

1806年，黑森-达姆施塔特被升为黑森大公国，1816年得到[莱茵河畔部分领地](../Page/莱茵河.md "wikilink")，更名**黑森和莱茵大公国**（Hessen
und bei Rhein）。

  - 1806年-1830年：[路德维希一世](../Page/路德维希一世_\(黑森大公\).md "wikilink")（Ludwig
    I）
  - 1830年-1848年：[路德维希二世](../Page/路德维希二世_\(黑森大公\).md "wikilink")（Ludwig
    II）
  - 1848年-1877年：[路德维希三世](../Page/路德维希三世_\(黑森大公\).md "wikilink")（Ludwig
    III），无子嗣
  - 1877年-1892年：[路德维希四世](../Page/路德维希四世_\(黑森大公\).md "wikilink")（Ludwig
    IV ），<small>路德维希三世大公的侄儿</small>
  - 1892年-1918年：[恩斯特·路德维希](../Page/恩斯特·路德维希_\(黑森大公\).md "wikilink")（Ernst
    Ludwig）

1918年，[恩斯特·路德维希大公被推翻](../Page/恩斯特·路德维希_\(黑森大公\).md "wikilink")，黑森和莱茵大公国君主制覆灭。

### 黑森-巴滕堡亲王

1851年，[路德维希二世大公的第三子亚历山大与朱莉](../Page/路德维希二世_\(黑森大公\).md "wikilink")·冯·霍克女伯爵结婚，因为双方身份悬殊，他们的婚姻在当时被视为贵庶通婚，虽合法但他们的后代不再被视为黑森和莱茵大公国的成员，也无继承权。另贬赐为**巴滕堡亲王**的虚衔。1917年，巴滕堡亲王们放弃自己的德国贵族头衔，加入[英国国籍并改姓](../Page/英国.md "wikilink")**蒙巴顿**（Mountbatten）。

## [黑森家族族长](../Page/黑森家族族长.md "wikilink")

### 卡塞尔支

  - 1866年-1875年：[弗里德里希·威廉](../Page/弗里德里希·威廉_\(黑森选侯\).md "wikilink")（Friedrich
    Wilhelm）， <small>前选侯</small>

<!-- end list -->

  -
    1875年，选侯弗里德里希·威廉去世，由于他的与平民的婚姻，他的后代不能继承他的爵位，黑森-卡塞尔支的[选侯头衔也被取消](../Page/选侯.md "wikilink")。他的堂弟，选侯[威廉一世四弟弗里德里希的孙子弗里德里希成为卡塞尔支的族长](../Page/威廉一世_\(黑森选侯\).md "wikilink")，称**黑森伯爵**。

<!-- end list -->

  - 1875年-1884年：[弗里德里希](../Page/弗里德里希·威廉·格奥尔格·阿道夫_\(黑森-卡塞尔\).md "wikilink")（Friedrich）
  - 1884年-1888年：[弗里德里希·威廉](../Page/弗里德里希·威廉·尼库劳斯·卡尔_\(黑森-卡塞尔\).md "wikilink")（Friedrich
    Wilhelm）， <small>弗里德里希的次子，1888年遇海难去世</small>
  - 1888年-1925年：[亚历山大·弗里德里希](../Page/亚历山大·弗里德里希_\(黑森-卡塞尔\).md "wikilink")（Alexander
    Friedrich）， <small>弗里德里希的第四子，1925年放弃卡塞尔支族长的头衔</small>
  - 1925年-1940年：[弗里德里希·卡尔](../Page/弗里德里希·卡尔.md "wikilink")（Friedrich
    Karl
    ），<small>弗里德里希的第五子，1918年被推选为[芬兰国王](../Page/芬兰君主列表.md "wikilink")，称**韦伊诺一世**（*Väinö
    I*），但并未登基</small>
  - 1940年-1980年：[菲利普](../Page/菲利普_\(黑森\).md "wikilink")（Philipp
    ），<small>1968年成为全黑森的家族族长，黑森大公继承人</small>
  - 1980年-2013年：[莫里茨](../Page/莫里茨_\(黑森\).md "wikilink")（Moritz ）
  - 2013年至今：[多纳图斯](../Page/多纳图斯_\(黑森\).md "wikilink")（Donatus）

### 达姆施塔特支

  - 1918年-1937年：[恩斯特·路德维希](../Page/恩斯特·路德維希_\(黑森大公\).md "wikilink")（Ernst
    Ludwig ），<small>前大公</small>
  - 1937年：[格奥尔格·多纳图斯](../Page/格奥尔格·多纳图斯_\(黑森-达姆施塔特\).md "wikilink")（Georg
    Donatus ），<small>恩斯特·路德维希次子</small>
  - 1937年-1968年：[路德维希（五世）](../Page/路德维希_\(黑森-达姆施塔特\).md "wikilink")（Ludwig
    ） <small>恩斯特·路德维希第三子</small>

1968年，达姆施塔特支绝嗣，黑森家族只剩卡塞尔支和卡塞尔支的分支菲利普斯塔尔支，卡塞尔支成为长系并拥有大公继承权。

### 菲利普斯塔尔支

  - 1866年-1905年：[亚历克西斯](../Page/亚历克西斯_\(黑森-菲利普斯塔尔-巴赫菲尔德\).md "wikilink")（Alexis）
  - 1905年-1954年：[克洛维](../Page/克洛维_\(黑森-菲利普斯塔尔-巴赫菲尔德\).md "wikilink")（Chlodwig
    ），<small>亚历克西斯之侄，1925年成为黑森-菲利普斯塔尔伯爵</small>
  - 1954年至今：[威廉](../Page/威廉亲王_\(黑森-菲利普斯塔尔-巴赫菲尔德\).md "wikilink")（Wilhelm
    ），<small>克洛维的长孙，称黑森亲王和伯爵</small>

## 附註

[H](../Category/德国君主列表.md "wikilink")
[H](../Category/德國相關列表.md "wikilink")
[H](../Category/布拉班特王朝.md "wikilink")