**黑河市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[黑龙江省下辖的](../Page/黑龙江省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于黑龙江省西北部，[小兴安岭北端](../Page/小兴安岭.md "wikilink")，与[俄罗斯](../Page/俄罗斯.md "wikilink")[阿穆尔州首府](../Page/阿穆尔州.md "wikilink")[海兰泡隔](../Page/海兰泡.md "wikilink")[黑龙江相邻](../Page/黑龙江.md "wikilink")。是中国北方重要边境贸易中心。全市总面积6.69万平方公里，人口167.94万。

## 历史

1858年[清朝和](../Page/清朝.md "wikilink")[俄羅斯帝國簽定](../Page/俄羅斯帝國.md "wikilink")《[璦琿條約](../Page/璦琿條約.md "wikilink")》，在俄羅斯勢力南下前進中，清[康熙](../Page/康熙.md "wikilink")22年（1683年）12月13日，[黑龍江左岸瑷琿旧城設置](../Page/黑龍江.md "wikilink")[黑龍江將軍衙門](../Page/黑龍江將軍.md "wikilink")，康熙24年（1685年）在黑龍江右岸新設[瑷珲城](../Page/瑷珲城.md "wikilink")，康熙29年（1690年）在[墨爾根](../Page/墨爾根.md "wikilink")（現在的[嫩江縣](../Page/嫩江縣.md "wikilink")）設置璦琿新城，管轄著現在的黑河市整個地區。[宣统元年](../Page/宣统.md "wikilink")（1909年）设[瑷珲兵备道](../Page/瑷珲.md "wikilink")，同年设[瑷珲直隶厅和](../Page/瑷珲直隶厅.md "wikilink")[黑河府](../Page/黑河府.md "wikilink")。

[中華民國成立以黑河道管轄](../Page/中華民國.md "wikilink")，1913年瑷珲直隶厅改瑷珲县。[滿洲國成立後](../Page/滿洲國.md "wikilink")，1934年12月1日設置[黑河省](../Page/黑河省.md "wikilink")，[滿洲國崩潰後](../Page/滿洲國.md "wikilink")，1945年11月19日，[黑河地區由](../Page/黑河地區.md "wikilink")[嫩江省管轄](../Page/嫩江省.md "wikilink")，12月14日黑河地區改由[黑龍江省管轄](../Page/黑龍江省.md "wikilink")。1946年9月7日，[黑河專區成立](../Page/黑河專區.md "wikilink")，1947年黑、嫩兩省合併由[黑嫩省管轄](../Page/黑嫩省.md "wikilink")。1947年2月7日，改為[黑河第五專區](../Page/黑河第五專區.md "wikilink")，隨著黑嫩省解體，9月17日再次由[黑河專區管轄](../Page/黑河專區.md "wikilink")。1956年改[爱辉县](../Page/爱辉县.md "wikilink")，1967年4月改為[黑河地區革命委員會](../Page/黑河地區.md "wikilink")，1979年2月8日，改由黑河地區行政公署管轄，1980年析[爱辉县设黑河市](../Page/爱辉县.md "wikilink")，1983年爱辉县并入黑河市，1993年2月8日，黑河地區改为地級黑河市，原县级黑河市改设[爱辉区](../Page/爱辉区.md "wikilink")。

## 地理

### 自然资源

  - 矿产有[黄金](../Page/黄金.md "wikilink")、[银](../Page/银.md "wikilink")、[铜](../Page/铜.md "wikilink")、[锡](../Page/锡.md "wikilink")、[铅](../Page/铅.md "wikilink")、[锌](../Page/锌.md "wikilink")、[铀](../Page/铀.md "wikilink")（主要蕴藏于嫩江地区，无商业开采价值）、[铬](../Page/铬.md "wikilink")、[磁化铁](../Page/磁化铁.md "wikilink")、[硫化铁](../Page/硫化铁.md "wikilink")、[钛化铁](../Page/钛化铁.md "wikilink")、[方铅矿](../Page/方铅矿.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[萤石](../Page/萤石.md "wikilink")、[麦饭石](../Page/麦饭石.md "wikilink")、[石棉](../Page/石棉.md "wikilink")、[云母](../Page/云母.md "wikilink")、[大理石](../Page/大理石.md "wikilink")、[重晶石](../Page/重晶石.md "wikilink")、[石英砂](../Page/石英砂.md "wikilink")、[陶土](../Page/陶土.md "wikilink")、[油页岩等](../Page/油页岩.md "wikilink")。

主要的黄金矿藏有[逊克县](../Page/逊克县.md "wikilink")[东安岩](../Page/东安岩.md "wikilink")[金矿](../Page/金矿.md "wikilink")、[爱辉区](../Page/爱辉区.md "wikilink")[罕达气](../Page/罕达气.md "wikilink")[金矿](../Page/金矿.md "wikilink")，[爱辉区](../Page/爱辉区.md "wikilink")[五道沟](../Page/五道沟.md "wikilink")[金矿](../Page/金矿.md "wikilink")，[木耳气](../Page/木耳气.md "wikilink")[金矿](../Page/金矿.md "wikilink")，[湖通河](../Page/湖通河.md "wikilink")[金矿](../Page/金矿.md "wikilink")、[爱辉区](../Page/爱辉区.md "wikilink")[三道弯子](../Page/三道弯子.md "wikilink")[金矿](../Page/金矿.md "wikilink")、（其中[上马厂](../Page/上马厂.md "wikilink")[金矿尚在勘探中](../Page/金矿.md "wikilink")）

  - 野生动物有[狐](../Page/狐.md "wikilink")、[猞猁](../Page/猞猁.md "wikilink")、[水獭](../Page/水獭.md "wikilink")、[野猪](../Page/野猪.md "wikilink")、[狍子](../Page/狍子.md "wikilink")、[驼鹿](../Page/驼鹿.md "wikilink")、[马鹿](../Page/马鹿.md "wikilink")、[熊等](../Page/熊.md "wikilink")。
  - 药用植物有[黄芪](../Page/黄芪.md "wikilink")、[刺五加](../Page/刺五加.md "wikilink")、[百合](../Page/百合.md "wikilink")、[苍术](../Page/苍术.md "wikilink")、[白芍](../Page/白芍.md "wikilink")、[柴胡](../Page/柴胡.md "wikilink")、[党参等](../Page/党参.md "wikilink")。
  - 土特产有[蕨菜](../Page/蕨菜.md "wikilink")、[黄花菜](../Page/黄花菜.md "wikilink")、[蘑菇](../Page/蘑菇.md "wikilink")、[猴头](../Page/猴头.md "wikilink")、[木耳](../Page/木耳.md "wikilink")、[榛子](../Page/榛子.md "wikilink")、[都柿等](../Page/都柿.md "wikilink")。

### 气候

## 政治

### 现任领导

<table>
<caption>黑河市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党黑河市委员会.md" title="wikilink">中国共产党<br />
黑河市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/黑河市人民代表大会.md" title="wikilink">黑河市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/黑河市人民政府.md" title="wikilink">黑河市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议黑河市委员会.md" title="wikilink">中国人民政治协商会议<br />
黑河市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/秦恩亭.md" title="wikilink">秦恩亭</a>[1]</p></td>
<td><p><a href="../Page/谢宝禄.md" title="wikilink">谢宝禄</a>[2]</p></td>
<td><p><a href="../Page/于凤荣.md" title="wikilink">于凤荣</a>（女）[3]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/汶上县.md" title="wikilink">汶上县</a></p></td>
<td><p>山东省<a href="../Page/平原县.md" title="wikilink">平原县</a></p></td>
<td><p><a href="../Page/吉林省.md" title="wikilink">吉林省</a><a href="../Page/松原市.md" title="wikilink">松原市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年2月</p></td>
<td><p>2016年5月</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

[黑河中央街-3-2017.jpg](https://zh.wikipedia.org/wiki/File:黑河中央街-3-2017.jpg "fig:黑河中央街-3-2017.jpg")\]\]
现辖1个[市辖区](../Page/市辖区.md "wikilink")、3个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管2个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[爱辉区](../Page/爱辉区.md "wikilink")
  - 县级市：[北安市](../Page/北安市.md "wikilink")、[五大连池市](../Page/五大连池市.md "wikilink")
  - 县：[嫩江县](../Page/嫩江县.md "wikilink")、[逊克县](../Page/逊克县.md "wikilink")、[孙吴县](../Page/孙吴县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>区划代码[4]</p></th>
<th><p>区划名称</p></th>
<th><p>汉语拼音</p></th>
<th><p>面积[5]<br />
（平方公里）</p></th>
<th><p>政府驻地</p></th>
<th><p>邮政编码</p></th>
<th><p>行政区划[6]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>街道</p></td>
<td><p>镇</p></td>
<td><p>乡</p></td>
<td><p>其中：<br />
民族乡</p></td>
<td><p>社区</p></td>
<td><p>行政村</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>231100</p></td>
<td><p>黑河市</p></td>
<td></td>
<td><p>66802.65</p></td>
<td><p><a href="../Page/瑷珲区.md" title="wikilink">爱辉区</a></p></td>
<td><p>164300</p></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td><p>231102</p></td>
<td><p>爱辉区</p></td>
<td></td>
<td><p>14316.30</p></td>
<td><p><a href="../Page/兴安街道_(黑河市).md" title="wikilink">兴安街道</a></p></td>
<td><p>164300</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p>231121</p></td>
<td><p>嫩江县</p></td>
<td></td>
<td><p>14956.52</p></td>
<td><p><a href="../Page/嫩江镇.md" title="wikilink">嫩江镇</a></p></td>
<td><p>161400</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>231123</p></td>
<td><p>逊克县</p></td>
<td></td>
<td><p>17027.47</p></td>
<td><p><a href="../Page/奇克镇.md" title="wikilink">奇克镇</a></p></td>
<td><p>164400</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>231124</p></td>
<td><p>孙吴县</p></td>
<td></td>
<td><p>4318.85</p></td>
<td><p><a href="../Page/孙吴镇.md" title="wikilink">孙吴镇</a></p></td>
<td><p>164200</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>231181</p></td>
<td><p>北安市</p></td>
<td></td>
<td><p>6308.75</p></td>
<td><p><a href="../Page/铁西街道_(北安市).md" title="wikilink">铁西街道</a></p></td>
<td><p>164000</p></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p>231182</p></td>
<td><p>五大连池市</p></td>
<td></td>
<td><p>9874.74</p></td>
<td><p><a href="../Page/青山街道_(五大连池市).md" title="wikilink">青山街道</a></p></td>
<td><p>164100</p></td>
<td><p>1</p></td>
</tr>
</tbody>
</table>

## 人口

[Heihe_people_performing_dance.jpg](https://zh.wikipedia.org/wiki/File:Heihe_people_performing_dance.jpg "fig:Heihe_people_performing_dance.jpg")

<table>
<caption><strong>黑河市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[7]（2010年11月）</p></th>
<th><p>户籍人口[8]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>黑河市</p></td>
<td><p>1673899</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>爱辉区</p></td>
<td><p>211313</p></td>
<td><p>12.62</p></td>
</tr>
<tr class="even">
<td><p>嫩江县</p></td>
<td><p>495519</p></td>
<td><p>29.60</p></td>
</tr>
<tr class="odd">
<td><p>逊克县</p></td>
<td><p>101411</p></td>
<td><p>6.06</p></td>
</tr>
<tr class="even">
<td><p>孙吴县</p></td>
<td><p>102821</p></td>
<td><p>6.14</p></td>
</tr>
<tr class="odd">
<td><p>北安市</p></td>
<td><p>436444</p></td>
<td><p>26.07</p></td>
</tr>
<tr class="even">
<td><p>五大连池市</p></td>
<td><p>326391</p></td>
<td><p>19.50</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")1673898人\[9\]。其中，男性为853105人，占50.97%；女性为820793人，占49.03%。性别比（以女性为100）为103.94。0－14岁的人口为211645人，占12.64%；15－64岁的人口为1323188人，占79.05%；65岁及以上的人口为139065人，占8.31%。

### 民族

除漢族外，共有37個少數民族，其中[鄂倫春族](../Page/鄂倫春族.md "wikilink")、[俄羅斯族和](../Page/俄羅斯族.md "wikilink")[鄂溫克族較為突出](../Page/鄂溫克族.md "wikilink")。例如鄂倫春族人口1865人。

## 名胜古迹

中国重点文物保护单位：

  - 瑷珲城遗址：历史展览馆，将军墓群，女真族石墓群，王肃公园
  - [璦琿新城遺址](../Page/璦琿新城遺址.md "wikilink")

## 教育

[Heihe_Museum_2017.jpg](https://zh.wikipedia.org/wiki/File:Heihe_Museum_2017.jpg "fig:Heihe_Museum_2017.jpg")\]\]

  - [黑河学院](../Page/黑河学院.md "wikilink")
  - [黑河市职业学校](../Page/黑河市职业学校.md "wikilink")（中专）
  - [黑河市农业职业中学](../Page/黑河市农业职业中学.md "wikilink")（高中）
  - [黑河市第一中学](../Page/黑河市第一中学.md "wikilink")（高中）
  - [黑河市第二中学](../Page/黑河市第二中学.md "wikilink")（初中）
  - [黑河市第五中学](../Page/黑河市第五中学.md "wikilink")（高中及初中）
  - [黑河市第一小学](../Page/黑河市第一小学.md "wikilink")
  - [黑河市第二小学](../Page/黑河市第二小学.md "wikilink")
  - [黑河市第三小学](../Page/黑河市第三小学.md "wikilink")
  - [黑河市第四小学](../Page/黑河市第四小学.md "wikilink")
  - [黑河市第五小学](../Page/黑河市第五小学.md "wikilink")
  - [黑河市第六小学](../Page/黑河市第六小学.md "wikilink")
  - [黑河市第三中学](../Page/黑河市第三中学.md "wikilink")

## 参看

  - [黑河-腾冲线](../Page/黑河-腾冲线.md "wikilink")

## 参考文献

## 外部連結

  - [黑河市人民政府門戶網站](http://www.heihe.gov.cn/)

[黑河](../Category/黑河.md "wikilink")
[Category:黑龙江地级市](../Category/黑龙江地级市.md "wikilink")
[黑](../Category/中国小城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.