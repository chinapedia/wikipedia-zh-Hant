**黑白棋**，又叫**翻轉棋**（）、**蘋果棋**或**奧賽羅棋**（）。據日本棋類遊戲專家[長谷川五郎在](../Page/長谷川五郎.md "wikilink")2005年的統計數據，在日本，黑白棋愛好者的數目約為六千萬人（[日本將棋愛好者約一千五百萬人](../Page/日本將棋.md "wikilink")；[圍棋之愛好者約五百萬人](../Page/圍棋.md "wikilink")；[國際象棋愛好者約三百萬人](../Page/國際象棋.md "wikilink")）\[1\]

## 命名

一般[棋子雙面為](../Page/棋子.md "wikilink")[黑](../Page/黑.md "wikilink")[白兩色](../Page/白.md "wikilink")，故稱「黑白棋」。因為行棋之時將對方棋子翻轉，變為己方棋子，故又稱「翻轉棋」（Reversi）。棋子雙面為[紅](../Page/紅.md "wikilink")、[綠色的稱為](../Page/綠.md "wikilink")「蘋果棋」。

## 歷史

黑白棋是19世紀末[英國人發明的](../Page/英國人.md "wikilink")。直到上個世紀70年代[日本人](../Page/日本人.md "wikilink")[長谷川五郎將其發展](../Page/長谷川五郎.md "wikilink")，借用[莎士比亞名劇](../Page/莎士比亞.md "wikilink")[奧賽羅](../Page/奧賽羅.md "wikilink")（Othello）為這個遊戲重新命名。奧賽羅是一個[黑人](../Page/黑人.md "wikilink")，妻子是[白人](../Page/白人.md "wikilink")，因受小人挑撥，懷疑妻子不忠一直情海翻波，最終親手把妻子殺死。後來真相大白，奧賽羅懊悔不已，自殺而死。黑白棋就是借用這個黑人白人鬥爭的故事。\[2\]

類似的傳統棋類有[西藏夾棋](../Page/西藏夾棋.md "wikilink")，是夾住敵棋後，換成己方的棋子。

### 黑白棋的電子化

黑白棋出現在80年代的[任天堂遊戲機和](../Page/任天堂.md "wikilink")[Apple
II個人電腦遊戲裡然後就是](../Page/Apple_II.md "wikilink")1990年的[Windows
3.0的推出](../Page/Windows_3.0.md "wikilink")，當時[Windows自帶的遊戲就是黑白棋](../Page/Windows.md "wikilink")，由於當時[電腦還比較少](../Page/電腦.md "wikilink")，沒等到黑白棋傳開，[Windows
3.1推出](../Page/Windows_3.1.md "wikilink")，把自帶遊戲換成現在大家見到的[踩地雷和](../Page/踩地雷.md "wikilink")[接龍](../Page/接龍_\(紙牌遊戲\).md "wikilink")。在90年代中期流行的任天堂[Game
Boy當中也有一款黑白棋遊戲](../Page/Game_Boy.md "wikilink")。另外，[電子辭典中的附帶遊戲也大多包含黑白棋](../Page/電子辭典.md "wikilink")。

### 網上黑白棋的興起

在90年代中期互聯網開始普及，亦冒起了一些大型遊戲網站。最先出現的是[微軟的Microsoft](../Page/微軟.md "wikilink")
Games網站，被吸引進去下黑白棋的都是各國的好手，因為是外國網站，當時下棋的華人還是比較少。及後紛紛出現其他網站，具代表性的有：

  - 以台灣棋手為主：[宏碁戲谷](../Page/宏碁戲谷.md "wikilink")、[CYC遊戲大聯盟](../Page/CYC遊戲大聯盟.md "wikilink")
  - 多國棋手網站：[Kurnik](../Page/Kurnik.md "wikilink")（現為PlayOK）、[Playsite](../Page/Playsite.md "wikilink")、[VOG](../Page/VOG.md "wikilink")、[Yahoo\!
    Games](../Page/Yahoo!_Games.md "wikilink")、[新浪網](../Page/新浪網.md "wikilink")
  - 以中國棋手為主：[Chinaren](../Page/Chinaren.md "wikilink")、[聯眾世界](../Page/聯眾世界.md "wikilink")、[中國遊戲中心](../Page/中國遊戲中心.md "wikilink")、[邊峰網路遊戲](../Page/邊峰網路遊戲.md "wikilink")
  - 以香港棋手為主：[GGS
    (MIMOSA)](http://www.box.net/shared/a0roi7l3dp)、[Cybercity](../Page/Cybercity.md "wikilink")

值得一提的是微軟的[Windows XP裡也自帶了網路黑白棋](../Page/Windows_XP.md "wikilink")。

## 棋具

  - [棋子](../Page/棋子.md "wikilink")：黑白棋棋子每顆由[黑](../Page/黑.md "wikilink")[白兩色組成](../Page/白.md "wikilink")，一面白，一面黑，共64個（包括棋盤中央的4個）。棋子呈圓餅形。
  - [棋盤](../Page/棋盤.md "wikilink")：黑白棋棋盤由64格的正方格組成，遊戲進行時棋子要下在格內。棋盤可分為「[角](../Page/角_\(黑白棋\).md "wikilink")」、「[邊](../Page/邊_\(黑白棋\).md "wikilink")」以及「中腹」。現今的棋盤多以8x8較為普遍。
  - [棋鐘](../Page/棋鐘.md "wikilink")：正式的[比賽中可以使用棋鐘對選手的時間進行限制](../Page/比賽.md "wikilink")。非正式的對局中一般不使用棋鐘。

## 遊戲規則

[棋盤共有](../Page/棋盤.md "wikilink")8行8列共64格。開局時，[棋盤正中央的](../Page/棋盤.md "wikilink")4格先置放[黑](../Page/黑.md "wikilink")[白相隔的](../Page/白.md "wikilink")4枚棋子（亦有求變化相鄰放置）。通常[黑子先行](../Page/黑.md "wikilink")。雙方輪流落子。-{只}-要落子和[棋盤上任一枚己方的](../Page/棋盤.md "wikilink")[棋子在一條線上](../Page/棋子.md "wikilink")（橫、直、斜線皆可）夾著對方[棋子](../Page/棋子.md "wikilink")，就能將對方的這些[棋子轉變為我己方](../Page/棋子.md "wikilink")（翻面即可）。如果在任一位置落子都不能夾住對手的任一顆[棋子](../Page/棋子.md "wikilink")，就要讓對手下子。當雙方皆不能下子時，遊戲就結束，子多的一方勝。

## 棋盤的座標標示法

為了便於識別[棋子的](../Page/棋子.md "wikilink")[位置](../Page/位置.md "wikilink")，[棋盤上行以](../Page/棋盤.md "wikilink")1至8表示，列以A至H表示。
請看下例：

## 游戏策略

因为黑白棋独特的规则，很容易出现双方比分的剧烈变化，在游戏后期可能仅用几个回合就将大量对方棋子变成己方，从而扭转局势。因此，太着眼于比分是没有必要的，更重要的是占据有利位置。

中间位置的棋子最容易受到夹击，有橫、直、斜線共四个方向的可能。而边缘的棋子则只有一个可能被夹击的方向，四个角落上的位置被占据后，则完全不可能被攻击。

游戏的后期是关键位置的争夺，而前期的布局，就是为抢占关键位置作准备。比如，若不想让对方占据棋盘边缘的有利位置，那么自己就应避免在靠近边缘的那一排落子。

## 黑白棋術語

[C位](../Page/C位_\(黑白棋\).md "wikilink")、[星位](../Page/星位_\(黑白棋\).md "wikilink")（C-squares
and
X-squares）：C位就是位於a2、a7、b1、b8、g1、g8、h2和h7的位置，星位就是位於b2、b7、g2和g7的位置。這些位置務必要小心佔用。

[中心（Center）](../Page/中心_\(黑白棋\).md "wikilink")：局面的中心就是[內部子的集合](../Page/內部子_\(黑白棋\).md "wikilink")。

[控制中心（Control of the
center）](../Page/控制中心_\(黑白棋\).md "wikilink")：一種策略，它試圖在局面中心擁有儘可能多的棋子，沿[邊界擁有儘可能少的棋子](../Page/邊界_\(黑白棋\).md "wikilink")，以獲得最大可能的[行動力](../Page/行動力_\(黑白棋\).md "wikilink")。

[角（Corner）](../Page/角_\(黑白棋\).md "wikilink")：[角就是位於a](../Page/角_\(黑白棋\).md "wikilink")1、a8、h1和h8的位置。这些位置不可能被对方夹吃。

[爬邊（Edge
creeping）](../Page/爬邊_\(黑白棋\).md "wikilink")：一種以[弱邊](../Page/不平衡邊_\(黑白棋\).md "wikilink")([不平衡邊](../Page/不平衡邊_\(黑白棋\).md "wikilink")……)為代價，在一條或兩條[邊上獲得最大數量棋步的策略](../Page/邊_\(黑白棋\).md "wikilink")。[爬邊者試圖通過將全部邊界都留給對手來快速耗盡他的棋步](../Page/爬邊_\(黑白棋\).md "wikilink")，但是如果爬邊不能湊效，壞邊產生的效應將使他的局面迅速變弱。

[邊界（Frontier）](../Page/邊界_\(黑白棋\).md "wikilink")：邊緣子的集合，也就是說那些與空位相鄰的棋子。

[獲得餘裕手（Gain a
tempo）](../Page/獲得餘裕手_\(黑白棋\).md "wikilink")：在棋盤的某個區域內比對手多下一步棋，以迫使他在其他地方先開始下棋（從而延長他的邊界）。

[效應（Influence）](../Page/效應_\(黑白棋\).md "wikilink")：當棋手的棋子迫使他同時在多個方向上翻轉棋子時，我們就說這些棋子產生了效應。

[內部子（Internal discs）](../Page/內部子_\(黑白棋\).md "wikilink")、[邊緣子（external
discs）](../Page/邊緣子_\(黑白棋\).md "wikilink")：內部子就是不與空位相鄰的棋子。沒有內部子在戰略上是很糟的。

[自由（Liberty）](../Page/自由_\(黑白棋\).md "wikilink")：非災難性的棋步。「缺少自由」：在不久的將來不得不送角。

[多子策略（Maximum disc
strategy）](../Page/多子策略_\(黑白棋\).md "wikilink")：許多初學者所用的錯誤策略，他們每步棋都試圖翻轉最大數量的棋子。

[行動力（Mobility）](../Page/行動力_\(黑白棋\).md "wikilink")：棋手合法的可能棋步數量。更進一步說，當棋手擁有大量的可能棋步時，他就擁有好的行動力。

[奇偶性（Parity）](../Page/奇偶性_\(黑白棋\).md "wikilink")：一種在對手佔據的每個區域內都留下偶數個空位的策略。

[安靜步（Quiet move）](../Page/安靜步_\(黑白棋\).md "wikilink")：不翻轉邊緣子的一步棋，通常這是步好棋。

[穩定子（Stable
discs）](../Page/穩定子_\(黑白棋\).md "wikilink")：絕對不會被翻轉的棋子。角就是一個[穩定子的實例](../Page/穩定子_\(黑白棋\).md "wikilink")。

[斯通納陷阱/四通陷阱（Stoner
Trap）](../Page/斯通納陷阱_\(黑白棋\).md "wikilink")：一種針對弱邊局面強迫進行角交換的攻擊。

[不平衡邊（Unbalanced
edge）](../Page/不平衡邊_\(黑白棋\).md "wikilink")：由非6個同色棋子組成的邊結構。

## 黑白棋賽事

### 香港

  - 香港黑白棋公開賽（HKOC）

### 中國

  - 重慶黑白棋公開賽
  - 廣州黑白棋公開賽
  - 上海黑白棋公開賽
  - 北京黑白棋公開賽
  - 天津黑白棋公開賽

### 日本

  - [オセロ名人戦](../Page/オセロ名人戦.md "wikilink")
  - 全日本選手権大會
  - グランドオセロ大會
  - 全日本小學校別団體戦
  - 全日本マスターオープン

### 美國

  - 美國黑白棋公開賽（USOC）

### 新加坡

  - [新加坡黑白棋公開賽](../Page/新加坡黑白棋公開賽.md "wikilink")（SOC）

### 世界性賽事

  - [黑白棋世界賽](../Page/黑白棋世界賽.md "wikilink")（WOC）

## 棋手排名

  - [中國棋手排名](../Page/中國棋手排名_\(黑白棋\).md "wikilink")

<!-- end list -->

  - [新加坡棋手排名](../Page/新加坡棋手排名_\(黑白棋\).md "wikilink")

<!-- end list -->

  - [世界棋手排名](../Page/世界黑白棋棋手排名.md "wikilink")

## 黑白棋軟體與人工智慧

### 黑白棋程式簡史

在1980年代，[電腦並不普及](../Page/電腦.md "wikilink")，在黑白棋界裡，最強的仍然是棋手（人類）。

到了1990年代初，電腦的速度以[幾何級數增長](../Page/幾何級數.md "wikilink")，寫出來的黑白棋[程式雖然仍然有點笨拙](../Page/程式.md "wikilink")，但由於計算深度（電腦的速度快）和尾局的準確性，所以已經很強。1990年代初最有名的程式就是[Thor](../Page/Thor.md "wikilink")，在那時候是最強的程式（還是[DOS模式年代](../Page/DOS.md "wikilink")），棋力能比得上世界級棋手，在這個時期的程式都是人工地加入[行動力](../Page/行動力_\(黑白棋\).md "wikilink")、位置策略、[偶數重要性等等](../Page/奇偶性_\(黑白棋\).md "wikilink")，但又因為這些策略是直接編寫在程式裡，那程式的棋力很依賴[程式員本人的棋力](../Page/程式員.md "wikilink")，程式下起來比較像人類的下法。而且程式裡遺留了人類棋手的弱點、策略的不完整性等等的問題。因為早期Thor的通用性，影響到現在黑白棋界裡的統一[棋譜](../Page/棋譜.md "wikilink")[資料庫都是用](../Page/資料庫.md "wikilink")[Thor
database](../Page/Thor_database.md "wikilink")。

到了1994、1995年，黑白棋程式的編寫方法有了突破性的發展，首先是有了一個稱為IOS的網站（[GGS的前身](../Page/GGS.md "wikilink")），讓不同的程式同時連上去互相對局，突破了以往那種閉門造車的日子，在同期[Michael
Buro做出了能由程式自我學習的](../Page/Michael_Buro.md "wikilink")[Logistello](../Page/Logistello.md "wikilink")，在差不多的時間，大家都學起來，用相似的方法來編寫程式。程式員不再把人工的策略和下棋方法等死硬地寫在程式裡，而是由程式自我學習，程式會記錄好、壞的形狀（patterns），根據實戰的結果自動調整策略，又會把不同的開局棋譜根據實戰的過程來評分、保存，有些程式能保存幾十萬步的開局棋譜。

就是因為Logistello在根本方法的改進、先進的算法、編寫方面的高效率和準確性等等，一直在黑白棋界保持為世界程式冠軍。在1993年\~1997年的25場比賽中，Logistello有18場獲得冠軍、6場獲得亞軍。1997年8月，Logistello擊敗了1996年的世界冠軍[村上健](../Page/村上健.md "wikilink")（Takeshi
Murakami），從此黑白棋程式把人類棋手遠遠甩在後面。直到Michael在1998年1月宣布Logistello退休為止。現在已經有數個程式在棋力上超越已退休的Logistello，包括[edax](../Page/edax.md "wikilink"),
[cyrano](../Page/cyrano.md "wikilink"),
[saio](../Page/saio.md "wikilink")，主要突破點在於它們把算法改善為[多線程執行（multithreading）](../Page/多執行緒_\(電腦硬體\).md "wikilink")。

### WZebra

黑白棋程序「斑馬」（[WZebra](../Page/WZebra.md "wikilink")）是[Zebra的](../Page/Zebra.md "wikilink")[Windows版本](../Page/Windows.md "wikilink")，除了可以下棋外，還提供了打譜、複盤、棋局分析、自我學習等功能，也可以加載[Thor棋譜文件](../Page/Thor.md "wikilink")，進行針對性訓練。在標準比賽時間（2x15分鐘）內，其搜索深度可達中局18至27步、終局24至31步。[WZebra是](../Page/WZebra.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")，提供了中文版本，並提供中文幫助。

### S.A.I.O.

[Saio是System](../Page/Saio.md "wikilink") Artificial Intelligence
Othello縮寫，是黑白棋[程式之一](../Page/程式.md "wikilink")。並不開放下載。

## 網路黑白棋

自[互聯網興起](../Page/互聯網.md "wikilink")，網路下棋成為了大眾黑白棋對弈的主要形式。網路黑白棋比賽也日漸普及起來。網路黑白棋尋找棋友非常方便，對弈後資料保留非常妥當。

## 註釋

## 參考文獻

Othello books to increase skill to tournament-level play:

  - Ted Landau, *[Othello: Brief &
    Basic](http://www.tlandau.com/files/Othello-B%26B.pdf)*, 1990.
  - Emmanuel Lazard and Federation Francaise d'Othello, *[Othello: The
    Rules of the Game](http://radagast.se/othello/Help/strategy.html)*,
    1993.
  - Randy Fang, *[Othello: From Beginner to
    Master](http://othello.federation.free.fr/livres/beginner-Randy-Fang.pdf)*,
    2003.
  - Brian Rose, *[Othello: A Minute to Learn... A Lifetime to
    Master](https://web.archive.org/web/20061123043648/http://othellogateway.strategicviewpoints.com/rose/book.pdf)*,
    2005.
  - [British Othello Federation
    Newsletters](https://web.archive.org/web/20140308075631/http://www.britishothello.org.uk/newsletters.html)

## 參見

  - [時間複雜度](../Page/時間複雜度.md "wikilink")

## 外部連結

  - [World Othello
    Federation](https://web.archive.org/web/20161101154648/http://www.worldothellofederation.com/)

  - [World Othello Rating
    List](https://web.archive.org/web/20140520221025/http://ratings.worldothellofederation.com/ratings_main.php)

  - [Reversi – An Animated Guide](http://www.samsoft.org.uk/reversi/)

  -
  - [Pictures of the 19th century reversi
    boards](https://web.archive.org/web/20060622193543/http://www.ffothello.org/musee/musee.php?param_sous_categorie=vieux_reversi)

  - ["Othello
    (Reversi)"](https://web.archive.org/web/20071225181940/http://gamesmuseum.uwaterloo.ca/VirtualExhibits/Whitehill/othello/),
    University of Waterloo, Elliott Avedon Museum & Archive of Games

**書籍**

  - Marc Mandt, *[Introduction to Basic Othello Strategy and
    Algorithms](http://www.site-constructor.com/othello/Present/Basic_Strategy.html)*,
    2001
  - [British Othello Federation
    Newsletters](https://web.archive.org/web/20140308075631/http://www.britishothello.org.uk/newsletters.html)

[Category:棋類](../Category/棋類.md "wikilink")
[黑白棋](../Category/黑白棋.md "wikilink")

1.  [時尚週刊《風尚志》雜誌2008年第62期](http://dzb.sg.com.cn/fashionweekly/xss/415811.shtml)
2.  <http://www.beppi.it/public/OthelloMuseum/pages/history.php>