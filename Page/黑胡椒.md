**黑胡椒**（[學名](../Page/双名法.md "wikilink")：*Piper
nigrum*），又名**黑川**\[1\]，是[胡椒属的](../Page/胡椒属.md "wikilink")[开花](../Page/開花植物.md "wikilink")[藤本植物](../Page/藤本植物.md "wikilink")，[果实在晒](../Page/果实.md "wikilink")-{zh-hans:干;zh-hant:乾}-后通常可作为[香料和](../Page/香料.md "wikilink")[调味料使用](../Page/调味料.md "wikilink")。同样的果实还是**-{A|白胡椒}-**、**红胡椒**与**绿胡椒**的制作原料。\[2\]黑胡椒原产于[南印度](../Page/印度南部.md "wikilink")，在当地和其他热带地区都有着广泛的种植。黑胡椒的果实在熟透时会呈现黑红色，并包含一粒种子；果实在晒-{zh-hans:干;zh-hant:乾}-后会成为直径5毫米的**胡椒子**[核果](../Page/核果.md "wikilink")。

\-{zh-hans:干;zh-hant:乾}-燥的**黑胡椒粉**是[欧洲风格菜肴的常用香料](../Page/欧洲.md "wikilink")，自古以來，黑胡椒就因其在调味与[医学上的双重价值而备受珍视](../Page/医学.md "wikilink")。黑胡椒的香馥来自其含有的[胡椒碱](../Page/胡椒碱.md "wikilink")。常简称作“**胡椒**”的黑胡椒粉，是全世界使用最广泛的香料之一，在世界各地的餐桌上都可见到，通常会与[精制食盐放在一起](../Page/食盐.md "wikilink")。

## 名称

[Pfeffer-Längsschnitt.jpg](https://zh.wikipedia.org/wiki/File:Pfeffer-Längsschnitt.jpg "fig:Pfeffer-Längsschnitt.jpg")
**黑胡椒**，在某些语境下可简称作**胡椒**\[3\]。中文中的“胡椒”由“[胡](../Page/胡.md "wikilink")”字与“椒”字组合而来。“胡”原意指称北方和西方的游牧民族，后引申表述为来自不同文化背景的东西。如：“胡蘿-{zh-hans:
卜; zh-hant:
蔔;}-”、“[胡床](../Page/胡床.md "wikilink")”。“椒”则指某些种子或果实有刺激性味道的食物\[4\]。

胡椒的英语单词“pepper”衍生自[拉丁语中](../Page/拉丁语.md "wikilink")“piper”一词，罗马人用“piper”这个词来表述胡椒与[荜拔](../Page/荜拔.md "wikilink")，因为他们错误地认为这两种不同的香料取自同样的植物。“piper”则来自[梵语中的](../Page/梵语.md "wikilink")“pippali”，其意为荜拔\[5\]。现代英语中的“pepper”发展自[古英语中的](../Page/古英语.md "wikilink")“pipor”。拉丁语的“piper”亦是[德语](../Page/德语.md "wikilink")“pfeffer”、[法语](../Page/法语.md "wikilink")“poivre”、[荷兰语](../Page/荷蘭語.md "wikilink")“peper”及其他相似形式的起源。在16世纪，“pepper”的词义又进一步包括了在[新大陆发现的](../Page/新大陆.md "wikilink")[辣椒](../Page/辣椒.md "wikilink")。至少在1840年代时，“pepper”已发展出了“勇气”与“活力”的比喻义；在20世纪早期，还曾被缩写成“pep”的形式。\[6\]

## 品种

[Peppercorn-varieties.jpg](https://zh.wikipedia.org/wiki/File:Peppercorn-varieties.jpg "fig:Peppercorn-varieties.jpg")
[BlackPepperCorn.jpg](https://zh.wikipedia.org/wiki/File:BlackPepperCorn.jpg "fig:BlackPepperCorn.jpg")
[Spilled_Pepper.jpg](https://zh.wikipedia.org/wiki/File:Spilled_Pepper.jpg "fig:Spilled_Pepper.jpg")
胡椒的果实与种子通过不同的加工方法，可以得到黑胡椒、白胡椒、绿胡椒以及红胡椒。全世界胡椒出口总量的80%-85%为黑胡椒，15%-20%为白胡椒，约1%为绿胡椒。

  - 黑胡椒

黑胡椒是由胡椒藤上未成熟的[浆果制成的](../Page/浆果.md "wikilink")\[7\]。浆果首先会在热水中暂煮片刻，以清洗其表面并预备-{zh-hans:干;zh-hant:乾}-燥。同时热度会破坏果实的[细胞壁](../Page/细胞壁.md "wikilink")，加速-{zh-hans:干;zh-hant:乾}-燥过程中褐化[酶的作用](../Page/酶.md "wikilink")。其后几天时间裡，浆果会被曝晒于太阳下或在机器中烘-{zh-hans:干;zh-hant:乾}-。在此过程中，由于真菌反应的作用，包裹着种子的果皮会逐渐地变黑并收缩，最后成为薄皱的一层。在-{zh-hans:干;zh-hant:乾}-燥过程结束后，得到的产品便是黑胡椒子。

  - 白胡椒

白胡椒则是由移除果皮的种子制成的。因為多一道工序，價格常較同等級的黑胡椒高。白胡椒的制作通常会采用完全成熟的浆果\[8\]，并将浆果在水中浸泡约一个星期，在这段时间中果肉部分会松软并逐渐[腐烂](../Page/腐烂.md "wikilink")。通过摩擦去除果肉残留物后，再将裸露的种子干燥。还有其他的用于移除果肉的加工方法，包括移除由未成熟浆果制成黑胡椒的外表皮。

在[美国](../Page/美國.md "wikilink")，白胡椒常被用作浅色[醬汁或](../Page/醬.md "wikilink")[土豆泥等食品的调味料](../Page/薯泥.md "wikilink")，因为黑胡椒在浅色食物中容易被认出。黑胡椒与白胡椒谁更具有辛辣性这点是有争议的。由于外表皮的一些成分无法在种子中寻得，两种胡椒的气味不尽相同。

  - 绿胡椒

绿胡椒同黑胡椒一样，是由未成熟的浆果制成的。干燥后的绿胡椒在某种程度上还保留着绿色，因为它经过了[二氧化硫或](../Page/二氧化硫.md "wikilink")[冷凍乾燥之类过程的处理](../Page/冷凍乾燥.md "wikilink")。经过[食盐水或](../Page/食盐水.md "wikilink")[醋](../Page/醋.md "wikilink")[腌制后的胡椒子也会呈现绿色](../Page/醃.md "wikilink")。新鲜而未处理的胡椒浆果在西方非常罕见，它们主要出现在一些[亚洲菜特别是](../Page/亚洲菜.md "wikilink")[泰國菜中](../Page/泰國菜.md "wikilink")。\[9\]新鲜胡椒浆果的气味辛辣且清新，并带有浓郁的芳香。\[10\]未经-{zh-hans:干;zh-hant:乾}-燥或腌制的胡椒会迅速地腐烂掉。

  - 红胡椒

在食盐水和醋中腌制成熟的红胡椒浆果可以制成罕见的红胡椒；-{zh-hans:干;zh-hant:乾}-燥绿胡椒的颜色保存技术亦可用于-{zh-hans:干;zh-hant:乾}-燥更罕见的成熟红胡椒子。\[11\]胡椒中的红胡椒品种不同于另一种更常见的“红胡椒子”，后者是不同科的[秘鲁胡椒木](../Page/秘鲁胡椒.md "wikilink")（*Schinus
molle*）及其近亲[巴西胡椒木](../Page/巴西胡椒.md "wikilink")（*Schinus
terebinthifolius*）的果实。在过去曾有关于红胡椒子作为食品是否安全的争论，但现在该争论已经平息了。\[12\]黑胡椒在植物学上与被叫做“川椒”的[花椒无任何联系](../Page/花椒.md "wikilink")。

  - 其它

胡椒子通常会根据其出产地或发货港口进行分类。[印度](../Page/印度.md "wikilink")[马拉巴尔海岸出产两种著名的品种](../Page/马拉巴尔海岸.md "wikilink")：分别是马拉巴尔胡椒与[代利杰里胡椒](../Page/代利杰里.md "wikilink")。代利杰里胡椒是一种高级胡椒，该胡椒的制作原料，是代利杰里山上的马拉巴尔胡椒木结出的最大最成熟的浆果（约占总浆果量的10%）。\[13\][沙捞越胡椒出产于](../Page/砂拉越.md "wikilink")[婆罗洲的](../Page/婆罗洲.md "wikilink")[马来西亚部分](../Page/马来西亚.md "wikilink")，[楠榜](../Page/楠榜.md "wikilink")
胡椒出产于[印度尼西亚的](../Page/印度尼西亚.md "wikilink")[苏门答腊岛](../Page/蘇門答臘.md "wikilink")，而[蒙托克](../Page/蒙托克.md "wikilink")
胡椒则出产于印度尼西亚的[邦加岛](../Page/邦加島.md "wikilink")。\[14\]

## 植株

[Piper_nigrum_drawing_1832.jpg](https://zh.wikipedia.org/wiki/File:Piper_nigrum_drawing_1832.jpg "fig:Piper_nigrum_drawing_1832.jpg")
胡椒是一种[多年生的](../Page/多年生植物.md "wikilink")[木本](../Page/木本植物.md "wikilink")[藤蔓植物](../Page/藤.md "wikilink")，常攀缘于[树木](../Page/树.md "wikilink")、木棒或格架上，高度可达四米。胡椒具有蔓生性，当植物的茎-{zh:干;
zh-hans:干; zh-hant:幹}-接触到地表时会迅速地[生根](../Page/根.md "wikilink")。

胡椒的蔓近圆形，略微弯曲，幼株时呈紫红色或古铜色（有的品种是浅绿色），后来会转为绿色，木栓化后，会转成褐色，表面粗糙。胡椒蔓一般有3-5厘米粗细，蔓上有膨出的节，节上有根带，在节出现10至15天以后，就会变成约有一毫米粗的气根。气根的主要功能是依附在支架上，以支持藤蔓向上生长\[15\]。

胡椒的[叶片是椭圆形的](../Page/葉子.md "wikilink")，葉緣平整（全緣，Integrifolia），单叶[互生](../Page/互生.md "wikilink")，叶面是深绿色，长约八至十五厘米，宽约六至九厘米。叶柄很短。胡椒的[花朵很小](../Page/花.md "wikilink")，呈穗状花序。在枝条上的节处会结出长约四至八厘米的穗条，长度大约6到12厘米，上面有30至120朵不等的花，成螺旋状排列。果实成熟后，穗条的长度会增长到七至十五厘米\[16\]。

[Pepper091.jpg](https://zh.wikipedia.org/wiki/File:Pepper091.jpg "fig:Pepper091.jpg")

胡椒原产于热带雨林，要求高温、湿润、少风、土壤肥沃和排水良好的环境。适合黑胡椒生长的土壤不能过分-{zh-hans:干;zh-hant:乾}-燥，但也不能易受[泛滥等因素的影响](../Page/洪灾.md "wikilink")，还要具有良好的排水系统和富含有机成分的土壤。积水无法及时排除会造成植株烂根以及瘟病发生，温度也是胡椒生长的决定性条件，一般以年平均温度摄氏24至26度为佳。剪下约30至40厘米长度、年龄4至6个月、气根发达的蔓条作为插条在苗圃中进行育苗。约一个月之后移出苗圃，进行定植。一般在每隔约两米的距离将之与相邻的树木或格架绑在一起即可进行繁殖。树皮粗糙的树木会有利于胡椒藤的攀缘。定植前需要首先清剪与胡椒处于竞争关系的杂草等植物，只留下足够的树木来提供荫蔽和通风的环境。胡椒的根会被树叶[覆盖层与](../Page/覆盖层.md "wikilink")[肥料所覆盖](../Page/肥料.md "wikilink")，每年修剪两次枝条。在栽种下头三年的旱季中，每隔一天便需为-{zh:干;
zh-hans:干; zh-hant:乾}-燥土壤中的植物施水。\[17\]植物在栽下的第四或第五年结出果实，并通常会持续结果七年。

胡椒的一根茎上能结出20到30根穗条。当穗条基部的一颗或两颗浆果变红后，收割便可以开始了；如果任由其成熟，浆果会失去辛辣味，并最终脱落和遗失。收获到的穗条会铺在阳光下曝晒，然后胡椒子会从穗条上脱落\[18\]。

## 历史

[Sa-pepper.jpg](https://zh.wikipedia.org/wiki/File:Sa-pepper.jpg "fig:Sa-pepper.jpg")
在印度，胡椒自[史前时代便被用作香料](../Page/史前時代.md "wikilink")。J. Innes
Miller写到，虽然胡椒也生长于[泰国南部与马来西亚](../Page/泰国.md "wikilink")，但它最主要的来源是在[印度](../Page/印度.md "wikilink")，特别是在[马拉巴尔海岸地区](../Page/马拉巴尔海岸.md "wikilink")，也就是现今的[喀拉拉邦](../Page/喀拉拉邦.md "wikilink")。\[19\]被称做“黑色黄金”的胡椒子是贵重的贸易货物，它还被作为[实物货币使用](../Page/实物货币.md "wikilink")。在英语中，胡椒子的单词“peppercorn”至今都还有“空有其名的租金”之引申义。

黑胡椒的古代史经常会与[荜拔联系](../Page/荜拔.md "wikilink")（并混淆）在一起。罗马人知道这两种植物，但他们只用一个“piper”来描述两者。实际上，直到抵达新大陆并发现[辣椒后](../Page/辣椒.md "wikilink")，荜拨才不再受到欧洲人的欢迎。因为某些辣椒在-{zh-hans:干;zh-hant:乾}-燥后的形状和味道上都与荜拨相似，而且辣椒还可以在更广泛的地区栽培，这些对欧洲人来说都是极大的便利。

在[中世纪结束前](../Page/中世纪.md "wikilink")，欧洲、中东与北非市场上的黑胡椒都出自印度的马拉巴尔地区。在16世纪，胡椒开始在[爪哇岛](../Page/爪哇岛.md "wikilink")、[巽他群岛](../Page/巽他群岛.md "wikilink")、[苏门答腊岛](../Page/蘇門答臘.md "wikilink")、[马达加斯加岛](../Page/马达加斯加.md "wikilink")、马来西亚与东南亚的其他地区进行栽培，但这些地区种植的胡椒大多用于与[中国的贸易](../Page/中国.md "wikilink")，或者用于满足当地的需求。\[20\]马拉巴尔地区的港口还是远东地区香料贸易在印度洋的中转港。

黑胡椒与来自印度和远东地区的其他商品一起，开创了[地理大发现的时代](../Page/地理大发现.md "wikilink")，改变了世界历史的进程。这些珍贵的商品是促使欧洲人去寻找印度新航线并建立殖民地的原因之一；同时，在寻找新航线的过程中，欧洲人发现并殖民了美洲。

### 古代

法老[拉美西斯二世的](../Page/拉美西斯二世.md "wikilink")[木乃伊鼻孔中曾发现过黑胡椒子](../Page/木乃伊.md "wikilink")，应是在前1213年法老死后不久的木乃伊化仪式中放入的。现今对胡椒在[古埃及的使用及胡椒从印度运到](../Page/古埃及.md "wikilink")[尼罗河流域的方式都知之甚少](../Page/尼罗河.md "wikilink")。

早在公元前4世纪，希腊人便已经知道了胡椒与[荜拔的存在](../Page/荜拔.md "wikilink")：尽管胡椒在当时极有可能既罕见又昂贵，且只有极富阶层才能够买到。在当时，胡椒是通过陆路贸易或沿[阿拉伯海的水路航线运达欧洲的](../Page/阿拉伯海.md "wikilink")。印度西南部出产的荜拔比更南面出产的胡椒更易获取；由于荜拔贸易的优势，加上荜拔更浓郁的香馥，在那个时代的流行程度也许超过了黑胡椒。

[Italy_to_India_Route.PNG](https://zh.wikipedia.org/wiki/File:Italy_to_India_Route.PNG "fig:Italy_to_India_Route.PNG")

在[罗马帝国的早期](../Page/羅馬帝國.md "wikilink")，特别是在罗马征服埃及的前30年后，穿越阿拉伯海抵达[南印度马拉巴尔海岸的贸易航线开始繁忙起来](../Page/印度南部.md "wikilink")。《[厄立特利亚海航行记](../Page/厄立特利亚海航行记.md "wikilink")》中记载了这条贸易航线穿越印度洋的细节。根据希腊地理学家[斯特拉博的记述](../Page/斯特拉波.md "wikilink")，早期的罗马帝国每年会派遣一队约有120艘海船的舰队从事与印度的贸易。舰队每年定期地穿过阿拉伯海，以赶上每一年的[季风](../Page/季风.md "wikilink")。从印度返航时，舰队会停靠在[红海的港口中](../Page/红海.md "wikilink")，并通过陆路或运河运抵[尼罗河](../Page/尼罗河.md "wikilink")，进而从尼罗河运到[亚历山大港](../Page/亚历山大港.md "wikilink")，最后装船运到意大利与罗马。在新航线发现前的一千五百年间，将胡椒等香料贸易至欧洲的路线与此条路线大体相同。

由于海船驶出红海后便可以直接向马拉巴尔海岸航进，黑胡椒的贸易路线得以缩短，价格也得以降低。[老普林尼的](../Page/老普林尼.md "wikilink")《[博物志](../Page/博物志_\(老普林尼\).md "wikilink")》
中记载了公元77年胡椒与荜拔在罗马的价格：“荜拔……每磅为十五[迪纳里厄斯](../Page/迪纳里厄斯.md "wikilink")
，而白胡椒为七（迪纳里厄斯），黑胡椒为四（迪纳里厄斯）。”\[21\]老普林尼还抱怨道：“没有哪一年印度不会让罗马帝国流失一千五百万的[塞斯特斯](../Page/塞斯特斯.md "wikilink")，”\[22\]他还进一步地提到了胡椒：

尽管十分昂贵，黑胡椒是[罗马帝国时代的一种著名且使用广泛的调味品](../Page/羅馬帝國.md "wikilink")。3世纪的食谱书[Apicius的](../Page/Apicius.md "wikilink")[De
re
coquinaria中的大多数食谱都需要用到胡椒](../Page/De_re_coquinaria.md "wikilink")。[爱德华·吉本在他的](../Page/爱德华·吉本.md "wikilink")《[罗马帝国衰亡史](../Page/罗马帝国衰亡史.md "wikilink")》中写道，胡椒是“大多数奢华的罗马烹饪术中的一种特别常见的成分”\[23\]。

### 古典时代結束後的欧洲

在古代欧洲，价值不菲的胡椒时常被作为[担保物](../Page/担保.md "wikilink")
甚至货币使用。随着罗马城的陷落，胡椒的美味（及胡椒的货币价值）被征服者们攫取。据说在5世纪时，[西哥特的](../Page/西哥特人.md "wikilink")[亚拉里克一世与匈奴王](../Page/亚拉里克一世.md "wikilink")[阿提拉在包围罗马城时都曾要求罗马城进献超过一吨的胡椒](../Page/阿提拉.md "wikilink")。罗马陷落后，其他的国家开始插足于[香料贸易](../Page/香料贸易.md "wikilink")：首先是[波斯](../Page/波斯.md "wikilink")，然后是[阿拉伯](../Page/阿拉伯.md "wikilink")；Innes
Miller引用了曾向东旅行到印度的拜占庭作家[科斯马斯·印第科普莱特斯](../Page/科斯马斯·印第科普莱特斯.md "wikilink")
的一段账目，作为“6世纪时胡椒依然在从印度输入欧洲”的证据。\[24\]在[黑暗时代的末期](../Page/黑暗时代.md "wikilink")，[伊斯兰教势力控制了香料贸易的中段路线](../Page/伊斯兰教.md "wikilink")。进入地中海后，贸易则被意大利的势力所垄断，特别是[威尼斯和](../Page/威尼斯共和国.md "wikilink")[热那亚](../Page/热那亚共和国.md "wikilink")。香料贸易在很大程度上是这些[城邦](../Page/城邦.md "wikilink")
兴起的原因之一。

七世纪的[舍伯恩主教](../Page/舍伯恩主教.md "wikilink")
[圣亚浩](../Page/圣亚浩.md "wikilink")
出过的一个[谜语](../Page/谜语.md "wikilink")，反映了黑胡椒当时在[英格兰的地位](../Page/英格兰.md "wikilink")：

通常认为在[中世纪时](../Page/中世纪.md "wikilink")，胡椒被用于掩盖肉的腐味。但是，没有证据支持这一说法，且历史学家们的看法大不相同：在中世纪，作为奢侈品的胡椒只有富人才买得起，而富人们显然也不会吃腐烂的肉。\[25\]同样的，胡椒曾被大规模地作为防腐剂使用的说法也是有问题的：胡椒所含的化合物胡椒碱的确有一些杀菌的作用，但作为香料使用的胡椒所含的胡椒碱浓度很低，影响力微乎其微。\[26\]相比之下，食盐是更有效的防腐剂，而[咸肉也是常见的食物](../Page/咸肉.md "wikilink")：特别是在冬季。不过，长时间保存的肉类也许需要胡椒和其他香料来改善其味道。

[Calicut_1572.jpg](https://zh.wikipedia.org/wiki/File:Calicut_1572.jpg "fig:Calicut_1572.jpg")的绘图，当时葡萄牙人控制了胡椒贸易\]\]

在中世纪时，胡椒有着极其昂贵的价格——且胡椒在欧洲的贸易又被意大利人所垄断——这成为了[葡萄牙人寻找印度新航线的一个诱因](../Page/葡萄牙.md "wikilink")。1498年，[瓦斯科·达·伽马成为了第一个通过海路抵达印度的欧洲人](../Page/瓦斯科·達伽馬.md "wikilink")；[卡利卡特的阿拉伯人](../Page/科泽科德.md "wikilink")（会说西班牙语和意大利语）问他们为什么要到这里来，他答道：“我们为寻找基督徒与香料而来。”在完成了这次绕过[非洲南部抵达印度的首次航行后](../Page/非洲.md "wikilink")，大量的葡萄牙人迅速地涌入了阿拉伯海，并用海军的强大炮火攫取了阿拉伯海贸易的绝对控制权。这是欧洲国家第一次将势力扩张至亚洲地区，1494年签订的[托尔德西里亚斯条约让这次势力扩张拥有了合法性](../Page/托尔德西里亚斯条约.md "wikilink")（至少从欧洲的观点来看是这样的），条约承认了葡萄牙拥有包括胡椒产源地在内的一半世界之排外权。

葡萄牙人不久后便证明了自己无法控制香料的贸易。阿拉伯和威尼斯人成功穿越了葡萄牙的封锁，走私了大量香料；因此除了沿非洲的新航线外，亚历山大港和意大利间的旧有路线依然存在。到了17世纪，葡萄牙在印度洋的地位被[荷兰与](../Page/荷兰.md "wikilink")[英国所取代](../Page/英国.md "wikilink")。1661年到1663年间，马拉巴尔海岸的胡椒港悉数落入荷兰人手中。

[Le_livre_des_merveilles_de_Marco_Polo-pepper.jpg](https://zh.wikipedia.org/wiki/File:Le_livre_des_merveilles_de_Marco_Polo-pepper.jpg "fig:Le_livre_des_merveilles_de_Marco_Polo-pepper.jpg")》的一幅图稿\]\]
由于输入欧洲胡椒数量的增加，胡椒价格开始下落（尽管进口贸易的总值在大体上没有改变）。在中世纪早期为富人所独享的胡椒开始进入普通人家，并成为了日常的调味用品。胡椒在世界香料贸易中的比重也提高到了五分之一。\[27\]

### 中国

《[史记](../Page/史记.md "wikilink")·[西南夷列传](../Page/s:史記/卷116.md "wikilink")》可能是[中国最早记载有胡椒的文献](../Page/中国.md "wikilink")。据《史记》的记录，汉武帝在[建元六年](../Page/建元_\(西汉\).md "wikilink")（前135年）派遣番阳令[唐蒙出使](../Page/唐蒙.md "wikilink")[南越](../Page/南越国.md "wikilink")，南越王用“枸醬”来款待唐蒙，并告诉唐蒙“枸醬”是从[夜郎牁江](../Page/夜郎.md "wikilink")（今[北盘江](../Page/北盘江.md "wikilink")）运到番禺城（今[广州市附近](../Page/广州市.md "wikilink")）的。当唐蒙回到长安后，又从商人处得知夜郎市场上的“枸醬”出自蜀地（今[四川](../Page/四川省.md "wikilink")）。\[28\]根据[李时珍的考证](../Page/李时珍.md "wikilink")，现今多认为“枸醬”或“蒟酱”指的是胡椒属植物[蔞葉](../Page/蔞葉.md "wikilink")\[29\]，但也有一些不同观点，如[西晋](../Page/西晋.md "wikilink")[嵇含所著之](../Page/嵇含.md "wikilink")《[南方草木状](../Page/南方草木状.md "wikilink")》认为那是荜拨\[30\]，亦有后人认为那是黑胡椒\[31\]；此外，还有一些观点认为蒟酱是枳椇、[枸杞或魔芋](../Page/枸杞.md "wikilink")\[32\]。

[Flora_Sinensis_-_Pepper.JPG](https://zh.wikipedia.org/wiki/File:Flora_Sinensis_-_Pepper.JPG "fig:Flora_Sinensis_-_Pepper.JPG")著作《Flora
Sinensis》（译：中国植物志）笔下的胡椒（上）与茯苓树根（下）。\]\]
尽管胡椒可能在魏晋以前便已为中国所知晓，但胡椒大规模传入中国的时间是在唐朝，\[33\]宰相[元載因為貪賄被殺抄家](../Page/元載.md "wikilink")，便有贓物胡椒八百石\[34\]。据《[酉阳杂俎](../Page/酉阳杂俎.md "wikilink")》记载，“胡椒，出[摩伽陀國](../Page/摩揭陀.md "wikilink")，呼為昧履支”，当时的人已开始用胡椒来为肉类食品调味。\[35\]\[36\]到了12世纪，黑胡椒已经成为了权贵人家菜肴中的常见调料，甚至部分地取代了中国的原产调料[花椒](../Page/花椒.md "wikilink")。

[马可·波罗曾描述过他了解到的](../Page/马可·波罗.md "wikilink")“天城”（Kinsay，今浙江杭州）的胡椒消费量，这从一个侧面说明了胡椒在13世纪的中国的流行：“……马可·波罗从大汗海关的一个官吏处得悉，每日上市的胡椒有四十三担，而每担重二百二十三磅。”\[37\]

### 胡椒的药用

[alice_pig_and_pepper.png](https://zh.wikipedia.org/wiki/File:alice_pig_and_pepper.png "fig:alice_pig_and_pepper.png")》（1865），第6章：小猪和胡椒。注意厨师左手上的胡椒研磨器。\]\]
与所有的东方香料一样，胡椒有着悠长的用作香料和药物的历史。味道更浓的荜拨通常是首选的药物，不过荜拨和黑胡椒都被作为药物使用。

黑胡椒子在[印度是](../Page/印度.md "wikilink")[草药](../Page/印度草药医学.md "wikilink")
、[悉达](../Page/悉达.md "wikilink") 和[尤那尼](../Page/尤那尼.md "wikilink")
医学中的一种药物。5世纪的《叙利亚医学之书》（Syriac Book of
Medicines）指出胡椒（可能实际指的是荜拨）可治疗[便秘](../Page/便秘.md "wikilink")、[腹泻](../Page/腹瀉.md "wikilink")、[耳痛](../Page/耳痛.md "wikilink")
、[坏疽](../Page/坏疽.md "wikilink")、[心脏病](../Page/心脏病.md "wikilink")、[疝气](../Page/腹股沟疝.md "wikilink")
、声嘶 、[消化不良](../Page/消化不良.md "wikilink")
、昆虫叮咬、[失眠](../Page/失眠.md "wikilink")、[关节痛](../Page/关节痛.md "wikilink")
、[肝病](../Page/肝臟.md "wikilink")、[肺病](../Page/肺.md "wikilink")、口腔[脓肿](../Page/脓肿.md "wikilink")
、[晒伤](../Page/晒伤.md "wikilink")
、[龋齿与](../Page/龋齿.md "wikilink")[牙痛](../Page/牙痛.md "wikilink")\[38\]。在[中医学中](../Page/中医学.md "wikilink")，黑胡椒可治疗寒痰、食积、脘腹冷痛、反胃、呕吐清水、泄泻、冷痢，亦可用于食物中毒解毒。\[39\]5世纪后的许多著作也建议用胡椒来治疗眼疾，一般是将胡椒制成的软膏直接涂敷在眼睛上。在现代医学中，没有证据可以表明以上的这些治疗方法是有效的；将胡椒直接涂在眼睛上是很不舒服的，同时还有可能对眼睛造成损伤。\[40\]

胡椒在很长一段时期中都被认为会让人[打喷嚏](../Page/噴嚏.md "wikilink")；现今也是这么认为的。一些人认为胡椒碱会刺激鼻孔，从而让人打喷嚏\[41\]；另一些人则认为是胡椒粉的作用，还有一些人觉得胡椒并不是十分有效的喷嚏制造剂。但是，几乎没有人曾进行过控制环境下的实验来解答这个问题。

由于胡椒会刺激肠道，进行了腹部手术或有腹部溃疡的病人的食谱中不能出现胡椒：一般会使用较温和的食品替代之。

黑胡椒含有少量的[黄樟脑](../Page/黄樟脑.md "wikilink")\[42\]，这是一种[致癌物质](../Page/致癌物質.md "wikilink")。

## 气味

[Pfeffermühle.jpg](https://zh.wikipedia.org/wiki/File:Pfeffermühle.jpg "fig:Pfeffermühle.jpg")
[Piperin.svg](https://zh.wikipedia.org/wiki/File:Piperin.svg "fig:Piperin.svg")的分子結構\]\]
胡椒的辛辣味主要来源于化合物[胡椒碱](../Page/胡椒碱.md "wikilink")，胡椒碱同时存在于果皮和种子中。按毫克来计算，精制胡椒碱的辣度大约是辣椒中[辣椒素的百分之一](../Page/辣椒素.md "wikilink")。胡椒的外果皮中还含有可产生气味的[蒎烯](../Page/蒎烯.md "wikilink")
、[桧烯](../Page/桧烯.md "wikilink") 、[苯烯](../Page/苯烯.md "wikilink")
、[石竹烯](../Page/石竹烯.md "wikilink")
与[芳樟醇](../Page/芳樟醇.md "wikilink")
等[萜类](../Page/萜烯.md "wikilink")，这些还是让柠檬、树木和花朵等产生气味的物质。白胡椒几乎失去了这些气味，因为白胡椒去掉了果皮层。由于要经历更长的发酵阶段，白胡椒会获得其他的一些气味（包括霉味）\[43\]。

胡椒的气味会随化学物质挥发而流失，因此密封保存有助于长期保藏胡椒的香馥。在光线的照射下也会让胡椒失去一些香味，因为胡椒碱会被转变为几乎无味的[異胡椒脂鹼](../Page/異胡椒脂鹼.md "wikilink")
\[44\]。在研磨成粉后，胡椒的芳香会更迅速地挥发；因此大部分的烹调书都建议不要在使用前研磨胡椒子。手持胡椒研磨器可以机械地将胡椒子研磨或压碎并制成胡椒粉，同时也有市售的胡椒粉。在14世纪时的欧洲厨房中便已有胡椒研磨器这类香料研磨器的存在，不过更早前使用[臼杵](../Page/臼杵.md "wikilink")
来研磨胡椒也是一个常见的方法。\[45\]

## 现代的国际贸易

按照货币价值来算，胡椒是世界上贸易最广泛的香料[經濟作物](../Page/經濟作物.md "wikilink")，2002年全世界的胡椒进口量占香料进口量的20%。胡椒的价格并不稳定，年度间的波动很大；例如，1998年的胡椒进口量占到了所有香料进口量的39%。\[46\]如果按照重量计算的话，全世界每年贸易的辣椒量要略多于胡椒。[国际胡椒交易所设在](../Page/国际胡椒交易所.md "wikilink")[印度的](../Page/印度.md "wikilink")[柯枝市](../Page/柯枝.md "wikilink")。

至2009年，[越南是世界上最大的胡椒生产与出口国](../Page/越南.md "wikilink")（136,500[吨](../Page/吨.md "wikilink")）\[47\]。2005年，除越南外主要的胡椒生产国有印度（70,000吨）、印度尼西亚（35,000吨）、[巴西](../Page/巴西.md "wikilink")（35,000吨）、马来西亚（19,000吨）、中国（15,000吨）、斯里兰卡（14,000吨）。越南几乎占全世界胡椒出口市场的50%，其生产的胡椒几乎都用于出口：2005年，越南出口了96,179吨胡椒、印度15,800吨、印度尼西亚29,866吨、巴西33,977吨、马来西亚17,044吨、中国3,000吨、斯里兰卡7,981吨。\[48\]2008年，世界胡椒产量中，越南占34%，印度占19%，巴西占13%，印度尼西亚占9%，马来西亚占8%，斯里兰卡占6%，中国占6%，泰国占4%。世界胡椒产量在2003年达到峰值，为335,000吨。

## 注釋與参考来源

## 参考文献

<div class="references-small">

  -   -
  -   -
  -
  - 世界银行的一篇ARD（农业与乡村发展）文件．

  -
  -

</div>

## 外部链接

  - [黑胡椒的营养价值](http://www.whfoods.com/genpage.php?tname=foodspice&dbid=74)

  - [Gernot
    Katzer的香料页面](http://gernot-katzers-spice-pages.com/engl/Pipe_nig.html?spicenames=zh)

  - [胡椒
    Hujiao](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00430)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [胡椒
    Hujiao](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00184)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

[藤](../Category/藥用植物.md "wikilink")
[Category:可食用坚果与种子](../Category/可食用坚果与种子.md "wikilink")
[Category:精油](../Category/精油.md "wikilink")
[Category:单胺氧化酶抑制剂](../Category/单胺氧化酶抑制剂.md "wikilink")
[B](../Category/胡椒属.md "wikilink") [B](../Category/胡椒.md "wikilink")
[B](../Category/香料.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  江苏新医学院编．中药大词典(下册)．上海：上海人民出版社，1977．153

2.  绿[辣椒或甜椒有时也被叫做](../Page/辣椒.md "wikilink")“绿椒”，但与“绿胡椒”是完全不同的两种作物。

3.  根据《[现代汉语词典](../Page/现代汉语词典.md "wikilink")》与《[辞海](../Page/辞海.md "wikilink")》的解释，黑胡椒实际上只是胡椒的制品之一；但作为胡椒的主要制品，黑胡椒一名的历史十分悠久，同时亦成为此种作物的特指名称：故本文以“黑胡椒”为标题，但在文中的不同语境里亦会简写作“胡椒”。

4.

5.  “Pippali”是梵语中荜拔（long
    pepper，故亦有译法作“长椒”）的意思，“marica”才是黑胡椒的意思。[希腊语和拉丁语借](../Page/希腊语.md "wikilink")“pippali”以表述两者。

6.  道格拉斯·哈珀的《在线语源学辞典》（Douglas Harper's *Online Etymology
    Dictionary*）：[“pepper”](http://www.etymonline.com/index.php?term=pepper)、[“pep”](http://www.etymonline.com/index.php?term=pep)．于2007年11月20日查阅．

7.

8.
9.

10.

11.

12.

13.

14.

15. 黄宗道，《天堂的种子》，第126-127页

16. 黄宗道，《天堂的种子》，第127-128页

17.

18. 黄宗道，《天堂的种子》，第142-145页

19.

20. Dalby，93页．亦见于中译本之146页，中译本将巽他（Sunda）误译作苏丹（Sudan）．

21. “Long pepper ... is fifteen denarii per pound, while that of white
    pepper is seven, and of black, four.”

22. “there is no year in which India does not drain the Roman Empire of
    fifty million sesterces,”

23. “a favourite ingredient of the most expensive Roman cookery”

24. Innes Miller，*The Spice Trade*，83页．

25. Dalby，156页；也见Turner，108－109页．不过Turner没有继续讨论香料（非特指胡椒）是否曾被用作消除酒类变质后的怪味。

26.

27. Jaffee，10页．

28.

29.

30.

31. Dalby，74－75页．亦见于中译本114页．“蒟酱”是荜拨的观点最初出自公元四世纪的植物学作家嵇含，李惠林（音，Hui-lin
    Li）在1979年将《南方草木状》译为英文时将“蒟酱”注为黑胡椒，Dalby一书亦认为其为黑胡椒．

32.
33.

34.

35.
36.

37.

38. Turner，160页．

39.

40. Turner，171页．

41.

42.

43. McGee，428页．

44.
45.

46. Jaffee，12页，表2．

47.

48.