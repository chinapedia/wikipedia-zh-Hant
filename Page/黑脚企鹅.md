**黑腳企鵝**（[学名](../Page/学名.md "wikilink")：**），又名**非洲企鵝**、**斑嘴環企鵝**或**公驢企鵝**，是生活在[非洲西南岸的](../Page/非洲.md "wikilink")[企鵝](../Page/企鵝.md "wikilink")，在[納米比亞至](../Page/納米比亞.md "wikilink")[南非近](../Page/南非.md "wikilink")[伊莉莎白港的](../Page/伊莉莎白港.md "wikilink")[阿爾哥亞灣的](../Page/阿爾哥亞灣.md "wikilink")24個島上都有群族，最大的位於[戴爾島](../Page/戴爾島.md "wikilink")。於1980年代，可能由於[掠食者的減少](../Page/掠食者.md "wikilink")，黑腳企鵝群族擴展至近[開普敦的](../Page/開普敦.md "wikilink")[博爾德斯海灘及](../Page/博爾德斯海灘.md "wikilink")[貝蒂灣](../Page/貝蒂灣.md "wikilink")。另一個大陸群族是在納米比亞，但出現的時間則不明。由於在博爾德斯海灘的黑腳企鵝容許遊人的接近，所以當地吸引了很多遊客到來。

黑腳企鵝的近親是在[南美洲南部的](../Page/南美洲.md "wikilink")[漢波德企鵝及](../Page/漢波德企鵝.md "wikilink")[麥哲倫企鵝及近](../Page/麥哲倫企鵝.md "wikilink")[赤道](../Page/赤道.md "wikilink")[太平洋的](../Page/太平洋.md "wikilink")[加拉帕戈斯企鵝](../Page/加拉帕戈斯企鵝.md "wikilink")。

## 描述

黑腳企鵝高68-70厘米，重2-5公斤。牠們的[胸部有黑汶及黑點](../Page/胸部.md "wikilink")，每一隻黑腳企鵝都有個別的斑點，彷彿[人類的](../Page/人類.md "wikilink")[指紋](../Page/指紋.md "wikilink")。牠們[眼睛上有粉紅色的](../Page/眼睛.md "wikilink")[腺體](../Page/腺體.md "wikilink")，若體溫上升，體內會有較多[血液流經這個腺體](../Page/血液.md "wikilink")，從而降溫。雄鳥的體型及鳥喙都較雌鳥的大。黑腳企鵝的鳥喙則較[漢波德企鵝的尖銳](../Page/漢波德企鵝.md "wikilink")。牠們明顯的黑白色是一種[偽裝](../Page/偽裝.md "wikilink")：白色是向水底下的[掠食者向上看的偽裝](../Page/掠食者.md "wikilink")，而黑色則是向上空的掠食者各下看的偽裝。

## 行為

[African_penguins.jpg](https://zh.wikipedia.org/wiki/File:African_penguins.jpg "fig:African_penguins.jpg")
黑腳企鵝全年都會繁殖，主要由2月開始。雌鳥每胎會生兩蛋，38-42天孵化。
黑腳企鵝是唯一於[非洲繁殖的](../Page/非洲.md "wikilink")[企鵝](../Page/企鵝.md "wikilink")，但在世界其他地方卻沒有牠們的蹤跡。牠們是[一夫一妻制的](../Page/一夫一妻制.md "wikilink")，且是輪流孵化及餵養雛鳥。每年10月至翌年2月都會[換羽](../Page/換羽.md "wikilink")，大部份都於11月及12月間換羽，之後會出海覓食。於1月會回到配偶身邊，並約於2月至8月開始築巢。牠們吃細小的[魚類](../Page/魚類.md "wikilink")，如[沙丁魚](../Page/沙丁魚.md "wikilink")、[鳳尾魚等](../Page/鳳尾魚.md "wikilink")、[介蟲及](../Page/介蟲.md "wikilink")[烏賊](../Page/烏賊.md "wikilink")。黑腳企鵝會從所吃的魚類中獲得水份。

黑腳企鵝游泳速度平均每小時7公里，最高可達每小時20公里，潛入水中2分鐘。

黑腳企鵝成群的生活，平均壽命10歲，最高紀錄可以達至24歲。2-6歲就會開始[交配](../Page/交配.md "wikilink")。雌鳥4歲時就達至性成熟，雄鳥則要5歲。於2003年估計就有179000隻成鳥及56000繁殖對。過往幾年間，牠們的數量開始下降，1970年代時就有220000隻成鳥。

黑腳企鵝以往又名公驢企鵝，這是由於牠們的叫聲像[公驢](../Page/驢.md "wikilink")。但是由於[南美洲的幾種](../Page/南美洲.md "wikilink")[企鵝都有類似的叫聲](../Page/企鵝.md "wikilink")，所以牠們又名非洲企鵝。

## 威脅

[African_Penguins.jpg](https://zh.wikipedia.org/wiki/File:African_Penguins.jpg "fig:African_Penguins.jpg")[喬治亞水族館的黑腳企鵝](../Page/喬治亞水族館.md "wikilink")。\]\]
[Spheniscus_demersus_MHNT.ZOO.2010.11.43.13.jpg](https://zh.wikipedia.org/wiki/File:Spheniscus_demersus_MHNT.ZOO.2010.11.43.13.jpg "fig:Spheniscus_demersus_MHNT.ZOO.2010.11.43.13.jpg")
於1910年，黑腳企鵝的數目估計有150萬隻，直至20世紀，其數量就只餘下10%。捕取鳥蛋及棲息地的破壞差不多將牠們推向[滅絕](../Page/滅絕.md "wikilink")。

20世紀中期後，[企鵝蛋是一種佳餚](../Page/企鵝.md "wikilink")，並因而被採集。採集回來的企鵝蛋會被在幾天內打破，以求售出的是新鮮的企鵝蛋。另外，在[開普敦沿岸的島上亦有清理鳥類作肥料之用](../Page/開普敦.md "wikilink")，亦令牠們的棲息地受到破壞。企鵝亦被[石化產品的污染所影響](../Page/石化產品.md "wikilink")。

黑腳企鵝是《[非洲-歐亞大陸遷徙水鳥保護協定](../Page/非洲-歐亞大陸遷徙水鳥保護協定.md "wikilink")》的列名[物種之一](../Page/物種.md "wikilink")。牠們在《[世界自然保護聯盟瀕危物種紅色名錄](../Page/世界自然保護聯盟瀕危物種紅色名錄.md "wikilink")》內被到為易危物種。

黑腳企鵝在海中會被[鯊魚](../Page/鯊魚.md "wikilink")、[南非海狗及](../Page/南非海狗.md "wikilink")[殺人鯨所獵殺](../Page/殺人鯨.md "wikilink")。在陸地上亦面臨[獴科](../Page/獴科.md "wikilink")、[香貓](../Page/香貓.md "wikilink")、[家貓及](../Page/家貓.md "wikilink")[狗的威脅](../Page/狗.md "wikilink")。而[黑背鷗亦會偷牠們的蛋及雛鳥](../Page/黑背鷗.md "wikilink")。

## 參考

## 外部連結

  - [www.pinguins.info About all species of
    penguins](http://www.pinguins.info)
  - ARKive there are African penguins - [Images and movies of the
    African
    penguin](http://www.arkive.org/species/GES/birds/Spheniscus_demersus/)
  - [African Penguins from the International Penguin Conservation Web
    Site](http://www.penguins.cl/african-penguins.htm)
  - [African Penguins - Avian Demography Unit at the University of Cape
    Town](http://web.uct.ac.za/depts/stats/adu/species/sp003_00.htm)
  - [Stony Point penguin
    colony](http://web.uct.ac.za/depts/stats/adu/stonypt.htm)
  - [African penguins on the SANCCOB conservation
    website](https://web.archive.org/web/20080619131335/http://www.sanccob.co.za/african_penguin.htm)
  - [African Penguin
    images](http://www.jostimages.com/galerie/penguins-seals/african-penguin.html)

[SD](../Category/IUCN易危物種.md "wikilink")
[D](../Category/環企鵝屬.md "wikilink")
[SD](../Category/企鵝科.md "wikilink")