《**黑雨**》（）是1989年9月22日上映的美国[動作](../Page/動作片.md "wikilink")[犯罪電影](../Page/犯罪片.md "wikilink")，由英國導演[雷利·史考特執導](../Page/雷利·史考特.md "wikilink")，演員包括[麥可·道格拉斯](../Page/麥可·道格拉斯.md "wikilink")、[安迪·加西亞](../Page/安迪·加西亞.md "wikilink")、[松田優作](../Page/松田優作.md "wikilink")、[高倉健及](../Page/高倉健.md "wikilink")[凱特·卡普肖](../Page/凱特·卡普肖.md "wikilink")。劇情講述日本[大阪黑幫製作](../Page/大阪.md "wikilink")[美金](../Page/美金.md "wikilink")[偽鈔](../Page/偽鈔.md "wikilink")，美國刑警與日本警方合作緝捕，終於成功逮捕首謀；全片寫實描繪日本[重工業都市黑幫兇猛剽悍](../Page/重工業.md "wikilink")，在[黑幫電影裡](../Page/黑幫電影.md "wikilink")，演、導、劇本俱優，是部當年影響同類型電影極為成功的代表佳作。

## 劇情

尼克是一個我行我素、桀驁不馴的紐約警察，雖然辦案績效亮眼，卻也是長官眼中極為頭痛的麻煩人物，尤其最近政風處因為發現他每月有一千元的資金缺口而懷疑他收賄，對他進行調查，為此他感到心煩意亂。這天，尼克與同事查理在餐廳用餐，正當他為自己被調查的事對查理大吐苦水時，突然幾名日本黑道份子衝進餐廳用武器控制住所有人，其中一個叫佐藤浩史的人在眾目睽睽之下殺害兩名同樣在用餐的日本人，手段的兇狠令人不寒而慄。目睹一切的尼克與查理無法忍受這種「太歲頭上動土」的行徑，於是他們在佐藤一行人準備離開時掏槍喝止，卻遭到對方開火攻擊。混亂中，佐藤來不及駕車脫逃，但仍頑強的和在後頭緊追不捨的尼克搏鬥，不過最後尼克與查理還是成功地迫使佐藤束手就擒。

尼克認定這個案子是他的，因為是他目睹案發過程並抓到兇手的。而被帶回警局的佐藤在警局採不合作姿態，完全不透露自己行兇的動機。此時日本大使館出面干涉，搬出引渡條款，說佐藤是日本的重大槍擊要犯，必須將其引渡回日本接受審訊，迫於國際壓力，紐約警方只好答應，尼克在力爭不成下，只得和查理負責押解佐藤回日本，不料兩人因為一時不察，將佐藤交給了偽裝成警察的佐藤同伴，日本警方了解之後相當不悅，打算自行收拾爛攤子，並要求尼克與查理照計畫回國。但基於國際尊嚴與自身的正義感，尼克與查理執意留下，大阪府警刑事部長大橋只好勉強答應，但只承諾觀摩辦案過程，不得帶槍與介入調查，並派遣精通英文的警官松本正博負責監督兩人的一切舉動。

不久[道頓堀的一家酒吧發生了一起命案](../Page/道頓堀.md "wikilink")，尼克隨大阪警方前往查看，死者正是當時假冒警察接走佐藤的其中一名佐藤同伴，尼克因此推測是佐藤事後滅口。不過大橋堅持不讓尼克與查理過度參與，而尼克和松本因文化、民族間的意識衝突，產生溝通及認同上的問題，之後尼克發現佐藤殺人是為了搶走黑道大老菅井国雄所雕塑的印鈔模子，企圖佔為己有。在調查過程中，查理慘遭佐藤殺害，尼克頓時受到相當大的打擊，此外，日本警方因不滿他逾越身分調查此案，要將他遣送回美國，尼克不願就此罷手，偷偷地滯留日本，在松本的幫助下，繼續追查……

## 角色與演員

  - [麥可·道格拉斯](../Page/麥可·道格拉斯.md "wikilink") 飾 尼克·康克林（Nick
    Conklin），紐約市警署刑警

  - [安迪·加西亞](../Page/安迪·加西亞.md "wikilink") 飾 查理·文生（Charlie
    Vincent），紐約市警署刑警

  - [凱特·卡普肖](../Page/凱特·卡普肖.md "wikilink") 飾 喬伊絲（Joyce），CLUB MIYAKO的酒店小姐

  - 飾 奧利佛（Oliver），尼克的上司

  - 飾 法蘭基（Frankie）

  - [高倉健](../Page/高倉健.md "wikilink") 飾 松本正博警部補（Masahiro
    Matsumoto），大阪府警刑事部捜査共助課刑警

  - [松田優作](../Page/松田優作.md "wikilink") 飾 佐藤浩史（Koji Sato），菅井的部下

  - [若山富三郎](../Page/若山富三郎.md "wikilink") 飾 菅井国雄（Kunio Sugai）

  - 飾 大橋警視（Ohashi），松本刑警的上司・大阪府警刑事部長

  - [内田裕也](../Page/内田裕也.md "wikilink") 飾 梨田（Nashida），佐藤的部下

  - 飾 吉本，佐藤的部下

  - [安岡力也](../Page/安岡力也.md "wikilink") 飾 菅井的保鑣

  - [島木譲二](../Page/島木譲二.md "wikilink") 飾 菅井的部下

  - 飾 佐藤的部下

  - [小野美幸](../Page/小野美幸.md "wikilink") 飾 Miyuki，佐藤的情婦，CLUB MIYAKO的酒店小姐

  - 飾 The Kid

  - [史蒂芬·魯特](../Page/史蒂芬·魯特.md "wikilink") 飾 Berg

  - 飾 Crown

  - [陶喆](../Page/陶喆.md "wikilink") 飾 日本警察（当时擔任群众臨演）

## 獎項

得到1990年奧斯卡最佳音效剪輯、最佳音樂兩項提名。獲得1990年[日本电影金像奖最佳外語片](../Page/日本电影金像奖.md "wikilink")。

## 参考资料

## 外部链接

  -
  -
  -
[Category:大阪市背景電影](../Category/大阪市背景電影.md "wikilink")
[Category:紐約市背景電影](../Category/紐約市背景電影.md "wikilink")
[Category:1988年電影](../Category/1988年電影.md "wikilink")
[Category:1980年代驚悚片](../Category/1980年代驚悚片.md "wikilink")
[Category:1980年代動作片](../Category/1980年代動作片.md "wikilink")
[Category:美國動作驚悚片](../Category/美國動作驚悚片.md "wikilink")
[Category:黑幫電影](../Category/黑幫電影.md "wikilink")
[Category:日語電影](../Category/日語電影.md "wikilink")
[Category:英语电影](../Category/英语电影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:紐約市取景電影](../Category/紐約市取景電影.md "wikilink")
[Category:偽造貨幣題材作品](../Category/偽造貨幣題材作品.md "wikilink")
[Category:1980年代犯罪片](../Category/1980年代犯罪片.md "wikilink")
[Category:美国警匪片](../Category/美国警匪片.md "wikilink")
[Category:汉斯·季默配乐电影](../Category/汉斯·季默配乐电影.md "wikilink")
[Category:日本取景電影](../Category/日本取景電影.md "wikilink")
[Category:洛杉矶取景电影](../Category/洛杉矶取景电影.md "wikilink")
[Category:日本電影學院獎最佳外語片](../Category/日本電影學院獎最佳外語片.md "wikilink")