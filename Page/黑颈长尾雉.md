**黑颈长尾雉**，学名*Syrmaticus
humiae*，一种长尾雉，属于[鸡形目](../Page/鸡形目.md "wikilink")[雉科](../Page/雉科.md "wikilink")。

## 生态环境

本物种栖息于海拔1000-3000米的开阔林区。

## 分布地域

分布在[中国西南的](../Page/中国.md "wikilink")[云南](../Page/云南.md "wikilink")、[广西等省](../Page/广西.md "wikilink")，以及[印度](../Page/印度.md "wikilink")、[缅甸和](../Page/缅甸.md "wikilink")[泰国的森林中](../Page/泰国.md "wikilink")。

## 特征

黑颈长尾雉体型较大，体长约为55厘米，最长可达90厘米。**雄鸟**头部为灰褐色，眼周裸露为红色皮肤；上体为褐色，[喙黄色](../Page/喙.md "wikilink")，[虹膜黄褐色](../Page/虹膜.md "wikilink")，肩羽白色，颈部为金属蓝色。尾羽长而灰白，带黑色和褐色斑纹。**雌鸟**体羽褐色，喉部为白色，尾羽带白色尖端。

## 食物

黑颈长尾雉为植食性鸟类。主要以嫩芽、浆果、根茎为食，有时也吃[昆虫](../Page/昆虫.md "wikilink")。

## 保护情况

由于栖息地的日益减少，以及偷猎的不断发生，黑颈长尾雉目前的数量十分稀少，是[中国国家一级保护动物](../Page/中国国家一级保护动物.md "wikilink")。

  - CITES濒危等级：附录I
  - IUCN濒危等级：NT
  - 中国国家重点保护等级：一级

## 參考

  - Database entry includes a brief justification of why this species is
    near threatened

[Category:长尾雉属](../Category/长尾雉属.md "wikilink")
[Category:中国國家一級保護动物](../Category/中国國家一級保護动物.md "wikilink")