**黑龍江龍屬**（[屬名](../Page/屬.md "wikilink")：）是[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")[賴氏龍亞科的一屬](../Page/賴氏龍亞科.md "wikilink")，化石發現於[中國](../Page/中國.md "wikilink")[黑龍江省](../Page/黑龍江省.md "wikilink")，年代為[白堊紀晚期](../Page/白堊紀.md "wikilink")。

黑龍江龍的化石發現於黑龍江省[漁亮子組的一個](../Page/漁亮子組.md "wikilink")[屍骨層](../Page/屍骨層.md "wikilink")，與[鴨嘴龍亞科](../Page/鴨嘴龍亞科.md "wikilink")[烏拉嘎龍的化石一起被發現](../Page/烏拉嘎龍.md "wikilink")。在2008年，帕斯卡·迦得弗利茲教授（Pascal
Godefroit）與其同事宣布發現了這兩種恐龍。[模式種是](../Page/模式種.md "wikilink")**鄂倫春黑龍江龍**（*S.
elunchunorum*），屬名在[滿語中意為](../Page/滿語.md "wikilink")「黑」，意指[黑龍江](../Page/黑龍江.md "wikilink")；種名則是以[鄂倫春族為名](../Page/鄂倫春族.md "wikilink")\[1\]。

黑龍江龍的[正模標本](../Page/正模標本.md "wikilink")（編號GMH
W453）是一個部份頭骨。另外，迦得弗利茲與其同事將同一屍骨層的其他化石歸類於此屬，包含頭骨碎片、[肩帶](../Page/胸帶.md "wikilink")、[肱骨](../Page/肱骨.md "wikilink")、[骨盆](../Page/骨盆.md "wikilink")。黑龍江龍具有許多[獨有衍徵](../Page/獨有衍徵.md "wikilink")。迦得弗利茲等人提出一個[系統發生學研究](../Page/系統發生學.md "wikilink")，將黑龍江龍歸類於賴氏龍亞科的未定屬\[2\]如同其他鴨嘴龍類，黑龍江龍應是[草食性恐龍](../Page/草食性.md "wikilink")。\[3\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:賴氏龍亞科](../Category/賴氏龍亞科.md "wikilink")

1.

2.
3.