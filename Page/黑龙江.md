**黑龙江**（；“萨哈连”意为“黑”，“[乌拉](../Page/乌拉.md "wikilink")”意为“水”；，“哈拉穆连”），[俄罗斯称之为](../Page/俄罗斯.md "wikilink")**阿穆爾河**（，；此詞來自[通古斯語族](../Page/滿－通古斯語族.md "wikilink")，意為「大河」或「大水」\[1\]），是[亚洲](../Page/亚洲.md "wikilink")[东北部的一条河流](../Page/東北亞.md "wikilink")，发源于[蒙古国](../Page/蒙古国.md "wikilink")[肯特山东麓](../Page/肯特山.md "wikilink")，在[石勒喀河与](../Page/石勒喀河.md "wikilink")[额尔古纳河交汇处形成](../Page/额尔古纳河.md "wikilink")。经过[中国](../Page/中国.md "wikilink")[黑龙江省北界与](../Page/黑龙江省.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")[遠東聯邦管區南界](../Page/遠東聯邦管區.md "wikilink")，之後以東北向穿越[哈巴羅夫斯克邊疆區](../Page/哈巴羅夫斯克邊疆區.md "wikilink")，最終流入[韃靼海峽](../Page/韃靼海峽.md "wikilink")。其主流长2824千米，若以[海拉尔河为源头计算](../Page/海拉尔河.md "wikilink")，则总长度约4444公里，若以[克魯倫河為源頭計算](../Page/克魯倫河.md "wikilink")，則總長度5498公里。

黑龙江是中国四大河流之一、[世界十大河之一](../Page/世界.md "wikilink")。，至19世纪中后期[沙俄强行](../Page/沙俄.md "wikilink")[占领中国黑龙江以北](../Page/兼併阿穆爾.md "wikilink")、[乌苏里江以东大片领土之后](../Page/乌苏里江.md "wikilink")，才成为中俄界河。後來蒙古國獨立，遂流經三個國家。2004年，中国和俄罗斯签署《[中俄國界東段補充協定](../Page/中华人民共和国和俄罗斯联邦关于中俄国界东段的补充协定.md "wikilink")》，将两国国界以黑龙江为基本界限划清。

## 水系

  - 左岸支流：[欧里道依河](../Page/欧里道依河.md "wikilink")、[结雅河](../Page/结雅河.md "wikilink")、[比占河](../Page/比占河.md "wikilink")、[比拉河](../Page/比拉河.md "wikilink")、[布列亞河](../Page/布列亞河.md "wikilink")
  - 右岸支流：[额木尔河](../Page/额木尔河.md "wikilink")、[呼玛河](../Page/呼玛河.md "wikilink")、[逊河](../Page/逊河.md "wikilink")、[松花江](../Page/松花江.md "wikilink")、[乌苏里江](../Page/乌苏里江.md "wikilink")

## 历史

[1903-Syberia-Amur.jpg](https://zh.wikipedia.org/wiki/File:1903-Syberia-Amur.jpg "fig:1903-Syberia-Amur.jpg")
黑龙江在漢魏晉時被稱為**弱水**，南北朝時，黑龍江上游稱**完水**，[松花江及兩江匯流後被稱為](../Page/松花江.md "wikilink")**難水**。隋唐時始稱黑龍江下游為**黑水**，完水改作**望建水**、難水改作**那河**。《[辽史](../Page/辽史.md "wikilink")》第一次以“黑龙江”来称呼这条河流，因為江水色黑，蜿蜒如遊龍。黑龙江流域在金朝被纳入领土范围内，成为金國内河。元明时称為**混同江**。

黑龍江沿岸最早的居民是古亞細亞人，後受通古斯人壓力，只分布於黑龍江下游，代表性民族是[尼夫赫人](../Page/尼夫赫人.md "wikilink")，他們發展出發達的定居捕魚與海獸文化。

在[秦朝与](../Page/秦朝.md "wikilink")[汉朝时](../Page/汉朝.md "wikilink")，游牧民族，包括[肃慎](../Page/肃慎.md "wikilink")、[鲜卑](../Page/鲜卑.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[契丹](../Page/契丹.md "wikilink")、[女真](../Page/女真.md "wikilink")、[鄂伦春等](../Page/鄂伦春.md "wikilink")，都在这一带生活。

[唐朝属北方少数民族政权](../Page/唐朝.md "wikilink")：[室韦与](../Page/室韦.md "wikilink")[靺鞨部](../Page/靺鞨.md "wikilink")。

[元代属于](../Page/元代.md "wikilink")[辽阳等处行中书省管辖](../Page/辽阳等处行中书省.md "wikilink")，并在黑龙江下游设置[征东元帅府](../Page/征东元帅府.md "wikilink")。

[明代](../Page/明代.md "wikilink")[永乐年间明廷设置](../Page/永乐.md "wikilink")[奴儿干都指挥使司管辖奴儿干地区的所有军事建制机构](../Page/奴儿干都指挥使司.md "wikilink")。都司的主要官员初为派驻数年而轮调的[流官](../Page/流官.md "wikilink")，后为当地部落领袖所世袭。仅现[中国国内部分的黑龙江两岸明廷就设置百余卫所实行管辖](../Page/中华人民共和国.md "wikilink")。

至[清代](../Page/清代.md "wikilink")，黑龍江一名成為官方名稱，為清初犯人[流放之地](../Page/流放.md "wikilink")。[冯景尝言](../Page/冯景.md "wikilink")：“今乌喇得流人，绳系颈，兽畜之。死则裸而弃诸野，乌鸢饱其肉，风沙扬其骨……”\[2\]康熙時始廢流放烏拉\[3\]，但雍正五年[李煦又被流放至此](../Page/李煦.md "wikilink")。雍正十年（1732年）在烏拉建成「望祭殿」。1683年設[黑龍江將軍管轄黑龍江流域](../Page/黑龍江將軍.md "wikilink")，数年后签定的中俄《[尼布楚条约](../Page/尼布楚条约.md "wikilink")》规定黑龙江两岸皆为中国领土。

自1858年中俄《[璦琿條約](../Page/璦琿條約.md "wikilink")》签订后，江北土地被割讓給俄國，黑龙江开始成为中俄大部分地区的边界。1900年[八国联军向中国开进时](../Page/八国联军.md "wikilink")，俄国以保护[中东铁路为由出兵越过黑龙江](../Page/中东铁路.md "wikilink")，放火烧毁当时属于[清朝的](../Page/清朝.md "wikilink")[璦琿城](../Page/璦琿城.md "wikilink")，并制造了[海兰泡惨案](../Page/海兰泡惨案.md "wikilink")，并深入清朝国境约40公里。不久战事结束，并将原属清政府管辖的[江东六十四屯](../Page/江东六十四屯.md "wikilink")（位于黑龙江北岸）以武力占领。1907年江南部分建黑龍江省。簡稱**黑**。

自[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，[苏联与中华人民共和国一直以黑龙江为基本边界](../Page/苏联.md "wikilink")，搁置了沿岸土地的主权争议。但自[珍宝岛事件后中苏关系逐渐恶化](../Page/珍宝岛事件.md "wikilink")，两国军队开赴黑龙江沿岸，导致黑龙江地区局势危急。自20世纪80年代末才有所缓和。

1970年代，随着中国[改革开放的不断加强](../Page/改革开放.md "wikilink")，黑龙江省与俄罗斯签订大量贸易协定，并将[黑河市](../Page/黑河市.md "wikilink")、[同江市等数个沿江城市与俄罗斯的](../Page/同江市.md "wikilink")[布拉戈维申斯克](../Page/布拉戈维申斯克.md "wikilink")（海蘭泡）、[哈巴罗夫斯克](../Page/哈巴罗夫斯克.md "wikilink")（伯力）等远东城市辟为边境互贸城市，利用互补的资源优势带动黑龙江流域地区经济的发展。

中国与俄罗斯在1990年代至21世纪初相继确定了东端边界的走向。中国方面放弃了自19世纪中叶被俄国取得的大量土地的主权要求，以及被強佔的[江东六十四屯](../Page/江东六十四屯.md "wikilink")。同意将两国边界以实际控制线，即黑龙江和[烏蘇里江的主航道中心線为主要依據进行划分](../Page/烏蘇里江.md "wikilink")。目前边界走向已确定完毕。但[中華民國方面對俄國佔領江東六十四屯的合法性從未予以承認](../Page/中華民國.md "wikilink")。

## 城市

[Amur_River_Outpost.JPG](https://zh.wikipedia.org/wiki/File:Amur_River_Outpost.JPG "fig:Amur_River_Outpost.JPG")
河畔主要城市有：

  - 南岸

<!-- end list -->

  - [漠河市](../Page/漠河市.md "wikilink")、[塔河县](../Page/塔河县.md "wikilink")、[呼玛县](../Page/呼玛县.md "wikilink")、[黑河市](../Page/黑河市.md "wikilink")、[逊克县](../Page/逊克县.md "wikilink")、[嘉荫县](../Page/嘉荫县.md "wikilink")、[萝北县](../Page/萝北县.md "wikilink")、[同江市](../Page/同江市.md "wikilink")、[抚远县](../Page/抚远县.md "wikilink")
  - [伯力](../Page/伯力.md "wikilink")（）,俄罗斯远东地区最大的城市

<!-- end list -->

  - 北岸

<!-- end list -->

  - [下列宁斯克耶](../Page/下列宁斯克耶.md "wikilink")
  - [阿穆尔河畔共青城](../Page/阿穆尔河畔共青城.md "wikilink")（）
  - [庙街](../Page/庙街_\(俄罗斯\).md "wikilink")（，黑龙江之入海口）
  - [海兰泡](../Page/海兰泡.md "wikilink")（，阿穆尔州首府）

## 黄金水道

黑龙江沿线曾盛产沙金，在清朝达到繁荣，为带动当地经济发展起到了重要作用。

黑龙江作为中国北方边界的界河，中华人民共和国边界的最北端位于黑龙江主航道的中心线上。拥有丰富的水力资源，大小支流约有950余条（包括[时令河](../Page/时令河.md "wikilink")），其中黑龙江最长的支流是约1657公里的[松花江](../Page/松花江.md "wikilink")。在支流边，中国大约有大小港站160个。

黑龙江上也有众多岛屿分布，包括著名的[大黑河岛以及](../Page/大黑河岛.md "wikilink")[黑瞎子岛等岛屿分属中](../Page/黑瞎子岛.md "wikilink")、俄两国。其中大黑河岛已发展成为贸易中转站，设施较为齐全。

由于黑龙江两岸土壤多为具有大量腐殖质的黑土，流经黑龙江的水流冲刷岸边的土壤，使黑土沉入江中，沉积在江底。故在水体清澈的地方看黑龙江水往往是黑色的。黑龙江流域内森林资源分布不一，中国方面自[1987年大兴安岭火灾后](../Page/1987年大兴安岭火灾.md "wikilink")[大兴安岭和](../Page/大兴安岭.md "wikilink")[小兴安岭地区多受损害](../Page/小兴安岭.md "wikilink")，自[漠河市至黑河市很少见到树龄](../Page/漠河市.md "wikilink")20年以上的树木。俄罗斯方面林业资源极为丰富，一般在冬季黑龙江封冻时中国经常进口俄罗斯的木材进行加工。沿河流域也生产黄[金](../Page/金.md "wikilink")，自漠河市至[爱辉古城曾被称为](../Page/爱辉.md "wikilink")“黄金之路”。

黑龙江的冻期长。受河岸解冻及雨季降水的影响，汛期多集中于春、夏两季。靠近黑龙江的最有名的生物是住河谷的[黑龙江豹](../Page/黑龙江豹.md "wikilink")，只剩大约50只。黑龙江中[鳇因为渔民猎杀获取鱼子酱](../Page/鳇.md "wikilink")，数量大大下降。黑龙江也以盛产[远东红点鲑和](../Page/远东红点鲑.md "wikilink")[大马哈鱼而闻名](../Page/大马哈鱼.md "wikilink")。

## 通航权

黑龙江南段（中俄边境段）属于中俄两国共同拥有， 因此双方都有通航权。 北段为俄罗斯内河，外籍船只不能通行。 然而，
根据1992年签署的《中华人民共和国交通部和俄罗斯联邦运输部关于在黑龙江和松花江利用中俄船舶组织外贸货物运输协议》，
俄方从1992年起为中方开放俄罗斯段黑龙江的通航权。 因此中俄船只均可于黑龙江上航行。\[4\]

<File:Amur> River.jpg|漠河段结冰的江面 <File:Amur> Ussuri.jpg|乌苏里江汇入黑龙江处
[File:Amur.jpg|俄罗斯Verkhnaya](File:Amur.jpg%7C俄罗斯Verkhnaya) Ekon附近的阿穆爾河
[File:AmurInKhabarovsk.jpg|哈巴羅夫斯克附近的阿穆爾河](File:AmurInKhabarovsk.jpg%7C哈巴羅夫斯克附近的阿穆爾河)
[File:Amurbridge2.jpg|哈巴羅夫斯克1998年重建的阿穆爾河大橋](File:Amurbridge2.jpg%7C哈巴羅夫斯克1998年重建的阿穆爾河大橋)
<File:Khb> most 2005 puti.jpg|哈巴羅夫斯克1998年重建的阿穆爾河大橋

## 参考文献

{{-}}

[黑龍江水系](../Category/黑龍江水系.md "wikilink")
[黑龍江水路運輸](../Category/黑龍江水路運輸.md "wikilink")
[Category:亞洲跨國河流](../Category/亞洲跨國河流.md "wikilink")
[Category:中国河流](../Category/中国河流.md "wikilink")
[Category:黑龍江河流](../Category/黑龍江河流.md "wikilink")
[Category:內蒙古河流](../Category/內蒙古河流.md "wikilink")
[Category:俄罗斯河流](../Category/俄罗斯河流.md "wikilink")
[Category:蒙古国河流](../Category/蒙古国河流.md "wikilink")
[Category:中俄邊界](../Category/中俄邊界.md "wikilink")

1.  [](https://archive.is/20120803223713/http://www.levking.ru/amur3.htm)
2.  馮景：《解舂集文鈔》卷十二《奇奴傳》
3.  康熙二十一年五月，康熙特頒旨：“流徙烏拉人犯，無屋棲身，無力耕種。復重困于差徭；況南人脆弱，來此苦寒之地，風氣凜冽，必至顛踣溝壑，殊可憫惻。”
4.  [中国船舶能从黑龙江直接开到海里吗？](https://www.zhihu.com/question/37318316)