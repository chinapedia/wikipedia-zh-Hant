[MERCKS_INDEX.png](https://zh.wikipedia.org/wiki/File:MERCKS_INDEX.png "fig:MERCKS_INDEX.png")
《**默克索引**》（）是由[美国](../Page/美国.md "wikilink")[默克公司出版的记录](../Page/默克公司.md "wikilink")[化学品](../Page/化学品.md "wikilink")、[药物和](../Page/药物.md "wikilink")[生理性物质的综合性](../Page/生理性物质.md "wikilink")[百科全书](../Page/百科全书.md "wikilink")，收錄超過一萬多條有關個別的物質和其相關[化合物的專題文章](../Page/化合物.md "wikilink")。本書亦於附錄中收錄有關有機化學的[人名反應](../Page/人名反應.md "wikilink")。默克索引亦設有訂閱的電子檢索形式，普遍被[參考圖書館所採納](../Page/參考圖書館.md "wikilink")，以及在網上查閱的形式。有中譯本。

默克索引于1889年首次出版，2013年起改由[英國皇家化學學會](../Page/英國皇家化學學會.md "wikilink")（Royal
Society of Chemistry）发行第15版，但仍維持原書名。

默克索引的專題文章函蓋范圍包括：

  - [CAS號](../Page/CAS號.md "wikilink")
  - 物質的別名，例如俗名或[IUPAC命名法](../Page/IUPAC命名法.md "wikilink")
  - [化學式](../Page/化學式.md "wikilink")
  - [分子量](../Page/分子量.md "wikilink")
  - [成份分析](../Page/成份分析.md "wikilink")
  - [結構式](../Page/結構式.md "wikilink")
  - 對物質的外貌描述
  - [熔點和](../Page/熔點.md "wikilink")[沸點](../Page/沸點.md "wikilink")
  - 在[實驗室常用](../Page/實驗室.md "wikilink")[溶劑之](../Page/溶劑.md "wikilink")[溶解性](../Page/溶解性.md "wikilink")
  - 根據物質的[化學合成引述相關的文獻和資料](../Page/化學合成.md "wikilink")
  - 在合適的情況下加入相關的醫學用途\[1\]

## 版本

  - 第一版（1889年）
  - 第二版（1896年）
  - 第三版（1907年）
  - 第四版（1930年）
  - 第五版（1940年）
  - 第六版（1952年）
  - 第七版（1960年）
  - 第八版（1968年）
  - 第九版（1976年）
  - 第十版（1983年）ISBN 0-911910-27-1
  - 第十一版（1989年）ISBN 0-911910-28-X
  - 第十二版（1996年）ISBN 0-911910-12-3
  - 第十三版（2001年）ISBN 0-911910-13-1
  - 第十四版（2006年）ISBN 978-0-911910-00-1
  - 第十五版（2013年）ISBN 978-1-84973670-1

## 參考

## 外部連結

  - [The Merck Index Online - chemicals, drugs and
    biologicals](https://www.rsc.org/merck-index?e=1)

[Category:专业性百科全书](../Category/专业性百科全书.md "wikilink")
[Category:化学数据库](../Category/化学数据库.md "wikilink")
[Category:默克公司](../Category/默克公司.md "wikilink")

1.  [線上默克索引\[304](http://library.dialog.com/bluesheets/html/bl0304.html#AB)
    \]