**黛绮丝**，[金庸小说](../Page/金庸小说.md "wikilink")《[倚天屠龙记](../Page/倚天屠龙记.md "wikilink")》中的人物。[波斯與](../Page/波斯.md "wikilink")[中國](../Page/中國.md "wikilink")[混血兒](../Page/混血兒.md "wikilink")，原為波斯[明教三聖女之一](../Page/明教.md "wikilink")，總教於前任教主死後，各長老依三聖女所積功德高低而定新任教主，黛綺絲則至中土明教「立功積德」。為金庸小說中武功絕頂的高手之一。

初出場時的身份為「金花婆婆」，徒弟[殷離即為明教護教法王](../Page/殷離.md "wikilink")「白眉鷹王」[殷天正的孫女](../Page/殷天正.md "wikilink")。丈夫為银叶先生[韩千叶](../Page/韩千叶.md "wikilink")，兩人生育有一女[小昭](../Page/小昭.md "wikilink")。

## 生平

紫衫龍王黛綺絲雖著墨不多，但却着实是個大美人，傾慕者無數，想光明頂上，碧水潭畔，紫衫如花，長劍勝雪，她性子如冰，傲骨冷冽如梅。

紫衫龍王位列[明教](../Page/明教.md "wikilink")「[四大護教法王](../Page/四大護教法王.md "wikilink")」之首，更被[明教第三十三代教主](../Page/明教.md "wikilink")[陽頂天收為義女](../Page/陽頂天.md "wikilink")，素有武林第一美女盛譽，黛綺絲自幼於海邊長大，精通水性。

因代替[明教第三十三任教主](../Page/明教.md "wikilink")[陽頂天與前來報父仇的](../Page/陽頂天.md "wikilink")「银叶先生」[韩千叶對戰](../Page/韩千叶.md "wikilink")，而獲[陽夫人授](../Page/陽夫人.md "wikilink")「紫衫龙王」稱號。

後因與[韓千葉相戀互許終身](../Page/韓千葉.md "wikilink")，避總教追捕而**「易容」**為又老又醜的「金花婆婆」，[金花婆婆出場時總帶幾聲咳嗽](../Page/金花婆婆.md "wikilink")，事緣當年於「冰冷徹骨，縱在盛暑，也向來無人敢下」的碧水寒潭與銀葉先生交戰時，傷及肺部，以至惡疾纏身。

黛綺絲與韓千葉成親之後，黛綺絲偷進明教密道以尋「乾坤大挪移」心法以交回總教，望將功贖罪（波斯總教失落心法多時，僅中土明教有傳），以避總教將失貞聖女燒死之懲罰，但無果，因而和銀葉先生隱居東海[靈蛇島](../Page/蛇島_\(中國\).md "wikilink")，兩人生有[小昭](../Page/小昭.md "wikilink")。

[碧水寒潭之戰後](../Page/碧水寒潭.md "wikilink")，陽教主命「蝶谷醫仙」[胡青牛替受傷的韓千葉醫病](../Page/胡青牛.md "wikilink")，紫衫龍王破門出教後，胡青牛堅拒替銀葉先生治病，最後韓不治，紫衫龍王與明教結怨，並以計逼胡青牛替非明教中人治病，並登門追殺胡青牛及其妻[王難姑](../Page/王難姑.md "wikilink")。

黛綺絲始終不忘「[乾坤大挪移](../Page/乾坤大挪移.md "wikilink")」心法，因遣女[小昭設計入侍](../Page/小昭.md "wikilink")[楊不悔](../Page/楊不悔.md "wikilink")，間接協助[張無忌練就此無上心法](../Page/張無忌.md "wikilink")。

黛綺絲後於靈蛇島被[波斯](../Page/波斯.md "wikilink")[明教](../Page/明教.md "wikilink")[使者](../Page/使者.md "wikilink")「風雲三使」的[流雲使](../Page/流雲使.md "wikilink")、[妙風使](../Page/妙風使.md "wikilink")、[輝月使活捉](../Page/輝月使.md "wikilink")，於行[焚刑時被](../Page/焚刑.md "wikilink")[明教第三十四代教主](../Page/明教.md "wikilink")[張無忌與眾人合計拯救](../Page/張無忌.md "wikilink")。

後與女兒[小昭共同返回](../Page/小昭.md "wikilink")[波斯](../Page/波斯.md "wikilink")。

## 影视形象

### 电视剧

  - [施明](../Page/施明.md "wikilink")：1978香港无线电视《倚天屠龙记》
  - [郝曼丽](../Page/郝曼丽.md "wikilink")：1984台湾台视《倚天屠龙记》
  - [胡美仪](../Page/胡美仪.md "wikilink")：1986香港无线电视《倚天屠龙记》
  - [李婉华](../Page/李婉华.md "wikilink")：1994香港无线电视《金毛狮王》
  - [张海伦](../Page/张海伦.md "wikilink")：1994 台湾台视《倚天屠龙记》
  - [江欣燕](../Page/江欣燕.md "wikilink")、[冯素波](../Page/冯素波.md "wikilink")：2001香港无线电视《倚天屠龙记》
  - [阎青妤](../Page/阎青妤.md "wikilink")：2003合拍电视剧《倚天屠龙记》
  - [马羚](../Page/马羚.md "wikilink")：2009中國电视剧《倚天屠龙记》
  - [杨明娜](../Page/杨明娜.md "wikilink")：2019中國电视剧《倚天屠龙记》

### 电影

  - [夏萍](../Page/夏萍.md "wikilink")：1978香港邵氏电影《倚天屠龙记》

[D黛](../Page/category:金庸筆下虛構角色.md "wikilink")

[D黛](../Category/倚天屠龍記角色.md "wikilink")