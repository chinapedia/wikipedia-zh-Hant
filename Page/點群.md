在[數學裡](../Page/數學.md "wikilink")，**點群**是指固定一點不動之幾何[對稱](../Page/對稱.md "wikilink")（[等距同構](../Page/等距同構.md "wikilink")）的[群](../Page/群.md "wikilink")。

## 簡介

點群存在於任一維度的[歐幾里得空間中](../Page/歐幾里得空間.md "wikilink")。一個離散之有時會被稱為**薔薇圖案群（rosette
group）**，且被用來描述裝飾品的對稱性。則大量地被使用於化學之中，尤其是在描述一個[分子和形成](../Page/分子.md "wikilink")[共價鍵之](../Page/共價鍵.md "wikilink")[分子軌道的對稱性](../Page/分子軌道.md "wikilink")，且在一些文獻中亦會被稱成**分子點群**。

在每一個維度裡都有著無限多個離散點群。但[晶體侷限定理說只存在有限多個和](../Page/晶体学限制定理.md "wikilink")相容的離散點群。在一維裡有2個，二維裡有10個，三維裡則有32個，這些點群稱做**[晶體點群](../Page/晶体学点群.md "wikilink")**。
[Flag_of_Hong_Kong.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong.svg "fig:Flag_of_Hong_Kong.svg")區旗上的[洋紫荊有著C](../Page/洋紫荊.md "wikilink")<sub>5</sub>對稱；每片花瓣上的星形則有著D<sub>5</sub>對稱。\]\]

## 二維中

可以分成兩個不同的種類，根據其對稱性是只有[旋轉而已](../Page/旋轉.md "wikilink")，還是亦包括[鏡射](../Page/鏡射.md "wikilink")。其[循環群C](../Page/循環群.md "wikilink")<sub>*n*</sub>（Z<sub>*n*</sub>抽象群類型）由360/*n*度和其整數倍的旋轉所構成。例如，[卐有一](../Page/卐.md "wikilink")[對稱群C](../Page/對稱群.md "wikilink")<sub>4</sub>，是由0度、90度、180度及270度等旋轉所構成的。[正方形的對稱群屬於](../Page/正方形.md "wikilink")*[二面體群](../Page/二面體群.md "wikilink")*D<sub>*n*</sub>（Dih<sub>*n*</sub>抽象群類型）的類型，包含和旋轉一樣多的鏡射。圓的無限旋轉對稱表示其鏡射對稱也是無限的，但形式上，[圓群S](../Page/圓群.md "wikilink")<sub>1</sub>是不同於Dih（S<sub>1</sub>）的，因為其明確地包含了鏡射。

一個無限群不一定需要是連續的；例如，存在一由360/√2度的整數倍之旋轉組成的群，其中並不包含有180度的旋轉。

*n*=1,2,3,4,6的C<sub>*n*</sub>和D<sub>*n*</sub>可以和平移對稱相結點，有時還可以以不只一種的方式。因此，這十個群可以產生出17個。

## 三維中

更複雜的對稱產生於三維之中，詳見****。

## 一般性

在任何維數*d*裡，所有可能的定點等距同構之連續群為*[正交群](../Page/正交群.md "wikilink")*，標記為O(*d*)；且其所有可能的旋轉之連續子群為*特殊正交群*，標記為SO(*d*)。這並不是，而是從[李群理論中生出的習慣標記](../Page/李群.md "wikilink")。

## 另見

  - [晶體學](../Page/晶體學.md "wikilink")

  - [晶體點群](../Page/晶体学点群.md "wikilink")

  -
## 外部連結

  - [Downloadable point group
    tutorial](https://web.archive.org/web/20061207172015/http://www.chemistry.emory.edu/pointgrp/)（Mac
    and Windows only）
  - [Molecular symmetry
    examples](https://web.archive.org/web/20070223054725/http://www.uniovi.es/qcg/d-MolSym/)
  - [Web-based point group
    tutorial](http://www.reciprocalnet.org/edumodules/symmetry/index.html)（needs
    Java and Flash）

[D](../Category/晶體學.md "wikilink") [D](../Category/群论.md "wikilink")
[D](../Category/欧几里得对称.md "wikilink")