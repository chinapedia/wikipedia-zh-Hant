[Kaohsiung_City_Shoushan.JPG](https://zh.wikipedia.org/wiki/File:Kaohsiung_City_Shoushan.JPG "fig:Kaohsiung_City_Shoushan.JPG")眺望市區\]\]
**鼓山區**為[臺灣](../Page/臺灣.md "wikilink")[高雄市一](../Page/高雄市.md "wikilink")[市轄區](../Page/市轄區.md "wikilink")，位於市內西南方，市中心西北方，北臨[左營區](../Page/左營區.md "wikilink")，東鄰[三民區](../Page/三民區.md "wikilink")，東南連[鹽埕區](../Page/鹽埕區.md "wikilink")，並隔[愛河出海口與](../Page/愛河.md "wikilink")[苓雅區相望](../Page/苓雅區.md "wikilink")，西濱[臺灣海峽](../Page/臺灣海峽.md "wikilink")，南隔[高雄港與](../Page/高雄港.md "wikilink")[旗津區](../Page/旗津區.md "wikilink")、[前鎮區相望](../Page/前鎮區.md "wikilink")。

本區背山面海，除南側之[壽山外](../Page/柴山.md "wikilink")，全區地勢大致平坦，氣候屬[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")。產業以[工商業為主](../Page/工商業.md "wikilink")，[漁業也頗為興盛](../Page/漁業.md "wikilink")，觀光資源亦相當豐富，區內的[西子灣及](../Page/西子灣.md "wikilink")[壽山均為頗負盛名的觀光勝地](../Page/壽山.md "wikilink")。

## 歷史

高雄古地名為打狗，因此地是[馬卡道族打狗社所在地](../Page/馬卡道族.md "wikilink")，位置大約在目前壽山一帶，壽山舊名「打狗山」或「打鼓山」。「打狗」於明嘉靖年間即出現在中國文獻中，明清時代的方志、宦遊之作，多有提到，但名稱不同，另有「打狗山」、「打狗仔港」、「打鼓山」、「打鼓」等名稱。「打狗」、「打鼓」發音相近。明鄭至清末，打狗庄一直隸屬於鳳山縣興隆里，範圍大約是現在的鼓山區。

另有一傳說，「打鼓山」地名起因於早期因港口有巨石，水急船行危險，行船出入常打鼓以求神助，故稱之為「打鼓山」。此說應為望文生義之說。

[清治時期](../Page/臺灣清治時期.md "wikilink")，此地屬[鳳山縣管轄](../Page/鳳山縣_\(臺灣\).md "wikilink")。[日治初期曾先後隸屬於鳳山支廳](../Page/台灣日治時期.md "wikilink")、打狗支廳；1920年臺灣地方改制，分屬[高雄州](../Page/高雄州.md "wikilink")[高雄郡](../Page/高雄郡.md "wikilink")[高雄街](../Page/高雄街.md "wikilink")、[左營-{庄}-管轄](../Page/左營庄.md "wikilink")；在1944年時共設有[田町](../Page/田町.md "wikilink")、[壽町](../Page/壽町.md "wikilink")、[山下町](../Page/山下町.md "wikilink")、[湊町](../Page/湊町.md "wikilink")、[新濱町](../Page/新濱町.md "wikilink")、[哨船町等六町和大字](../Page/哨船町.md "wikilink")[內惟](../Page/內惟.md "wikilink")。戰後合併前述七地區和左營-{庄}-的[大字](../Page/大字_\(行政區劃\).md "wikilink")[前峰尾](../Page/前峰尾.md "wikilink")，設置高雄市鼓山區至今。

## 歷任區長

| 區長姓名 | 區長任期                     | 備註 |
| ---- | ------------------------ | -- |
| 周鶴壽  | 1959年7月1日 - 1961年1月15日   |    |
| 林金枝  | 1963年1月15日 - 1966年1月15日  |    |
| 周耀璋  | 1966年1月15日 - 1969年1月15日  |    |
| 劉嘉男  | 1973年1月15日 - 1973年10月1日  |    |
| 王振成  | 1973年10月1日 - 1974年10月15日 |    |
| 謝酉山  | 1974年10月15日 - 1977年6月1日  |    |
| 李楚傑  | 1977年6月1日 - 1977年7月20日   |    |
| 陳坤章  | 1977年7月20日 - 1982年12月15日 |    |
| 蔡得雄  | 1982年12月15日 - 1985年1月1日  |    |
| 楊壬生  | 1985年1月1日 - 1987年2月9日    |    |
| 陳坤章  | 1987年2月9日 - 1988年2月9日    |    |
| 林振忠  | 1988年2月14日 - 1991年3月31日  |    |
| 陳文樟  | 1991年4月1日 - 1996年6月17日   |    |
| 黃清和  | 1996年6月17日 - 1998年7月12日  |    |
| 陳文成  | 1998年7月13日 - 2002年1月30日  |    |
| 洪招祥  | 2002年1月30日 - 2005年7月21日  |    |
| 嚴文彬  | 2005年7月21日 - 2006年3月7日   |    |
| 藍美珍  | 2006年3月7日 - 2009年4月12日   |    |
| 李幸娟  | 2009年4月13日 - 2012年3月28日  |    |
| 陳淑芳  | 2012年3月28日 - 2014年12月31日 |    |
| 袁德明  | 2015年1月1日 - 2016年2月22日   |    |
| 鄭明興  | 2016年2月22日 - 2019年3月11日  |    |
| 林福成  | 2019年3月11日 - 現任          |    |

## 人口

### 人口變化

  - 資料時間：2019年1月-2019年12月；來源：內政部戶政司人口資料庫\[1\]

## 行政區

鼓山區共分為38里，全區由四個聚落組成\[2\]。
[Gushan_villages2.svg](https://zh.wikipedia.org/wiki/File:Gushan_villages2.svg "fig:Gushan_villages2.svg")

  - [哈瑪星聚落](../Page/哈瑪星.md "wikilink")：壽山-{里}-、延平里、哨船頭里、惠安里、維生里、麗興里、峰南里、登山-{里}-、桃源里、新民里
  - [巖仔聚落](../Page/巖仔.md "wikilink")：河邊里、光化里、興宗里、寶樹里、樹德里、山下里、鼓岩里、[綠川里](../Page/綠川里_\(高雄市\).md "wikilink")
  - [內惟聚落](../Page/內惟.md "wikilink")：正德里、[自強里](../Page/自強里_\(高雄市\).md "wikilink")、[建國里](../Page/建國里_\(高雄市\).md "wikilink")、鼓峰里、龍井-{里}-、[內惟里](../Page/內惟.md "wikilink")、民族-{里}-、忠正里、前峰里、雄峰里、厚生里、光榮里、平和里、民強里
  - [凹仔底聚落](../Page/凹仔底.md "wikilink")：龍子-{里}-、龍水-{里}-、[明誠里](../Page/明誠里.md "wikilink")、華豐里、裕興里、裕豐里

## 交通

### 鐵路

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [縱貫線](../Page/縱貫線_\(南段\).md "wikilink")：[內惟](../Page/內惟車站.md "wikilink")
    - [美術館](../Page/美術館車站.md "wikilink") -
    [鼓山](../Page/鼓山車站.md "wikilink");[~~<font color="#888888">壽山</font>~~](../Page/壽山車站.md "wikilink")（已廢止）
    -
    [~~<font color="#888888">高雄港</font>~~](../Page/高雄港車站.md "wikilink")（已廢止）
  - [屏東線](../Page/屏東線.md "wikilink")：[~~<font color="#888888">高雄港</font>~~](../Page/高雄港車站.md "wikilink")（已廢止）
    -
    [~~<font color="#888888">壽山</font>~~](../Page/壽山車站.md "wikilink")（已廢止）

### 捷運

  - [高雄捷運](../Page/高雄捷運.md "wikilink")

<!-- end list -->

  - <font color={{高雄捷運色彩|紅}}>█</font>
    [紅線](../Page/高雄捷運紅線.md "wikilink")：R14
    [巨蛋](../Page/巨蛋站.md "wikilink") - R13
    [凹子底](../Page/凹子底站.md "wikilink")
  - <font color={{高雄捷運色彩|橘}}>█</font>
    [橘線](../Page/高雄捷運橘線.md "wikilink")：O1
    [西子灣](../Page/西子灣站.md "wikilink")
  - <font color={{高雄捷運色彩|環}}>█</font>
    [環狀輕軌](../Page/環狀輕軌.md "wikilink")(二階興建中)：
    C13 [駁二蓬萊](../Page/駁二蓬萊站.md "wikilink") - C14
    [哈瑪星](../Page/哈瑪星站.md "wikilink") - C15
    [<font color="#888888">五福四路</font>](../Page/五福四路站.md "wikilink") -
    C16 [<font color="#888888">大公路</font>](../Page/大公路站.md "wikilink") -
    C17 [<font color="#888888">鼓山區公所</font>](../Page/興隆路站.md "wikilink")
    - C18 [<font color="#888888">鼓山</font>](../Page/鼓山車站.md "wikilink")
    - C19
    [<font color="#888888">九如四路</font>](../Page/九如四路站.md "wikilink")
    - C20
    [<font color="#888888">美術館</font>](../Page/美術館車站.md "wikilink")
    - C21
    [<font color="#888888">美術園區</font>](../Page/美術園區站.md "wikilink")
    - C22
    [<font color="#888888">聯合醫院</font>](../Page/聯合醫院站.md "wikilink")
    - C23 [<font color="#888888">龍德路</font>](../Page/龍德路站.md "wikilink")
    - C24
    [<font color="#888888">新市政中心</font>](../Page/新市政中心站.md "wikilink")

### 公路

  - ：[中華一路](../Page/中華一路.md "wikilink")

### 主要道路

  - [九如四路](../Page/九如四路.md "wikilink")
  - [五福四路](../Page/五福四路.md "wikilink")
  - [明誠三路](../Page/明誠三路.md "wikilink")、明誠四路
  - [新疆路](../Page/新疆路.md "wikilink")
  - [翠華路](../Page/翠華路.md "wikilink")
  - [臨海一路](../Page/臨海一路.md "wikilink")、臨海二路
  - [大順一路](../Page/大順一路.md "wikilink")
  - 河西一路
  - [馬卡道路](../Page/馬卡道路.md "wikilink")
  - 葆禎路
  - [裕誠路](../Page/裕誠路.md "wikilink")
  - [鼓山一路](../Page/鼓山一路.md "wikilink")、[鼓山二路](../Page/鼓山二路.md "wikilink")、[鼓山三路](../Page/鼓山三路.md "wikilink")

[Gushan_Ferry_Pier_20060813.jpg](https://zh.wikipedia.org/wiki/File:Gushan_Ferry_Pier_20060813.jpg "fig:Gushan_Ferry_Pier_20060813.jpg")

  - **鼓山**－[旗津渡輪](../Page/旗津區.md "wikilink")

### 公共腳踏車

目前[City Bike於本區共設有](../Page/高雄市公共腳踏車租賃系統.md "wikilink")10座租賃站，分別為：

  - [西子灣](../Page/西子灣站.md "wikilink")
  - [凹子底](../Page/凹子底站.md "wikilink")
  - [中山大學](../Page/國立中山大學.md "wikilink")
  - [華榮公園](../Page/華榮路#主要設施.md "wikilink")
  - [森林公園](../Page/神農路_\(高雄市\)#主要設施.md "wikilink")
  - [美術館藝片天](../Page/美術東一路_\(高雄市\)#主要設施.md "wikilink")
  - [西子樓](../Page/國立中山大學.md "wikilink")
  - [龍華國小](../Page/高雄市鼓山區龍華國民小學.md "wikilink")
  - [美術館馬卡道](../Page/高雄市立美術館.md "wikilink")
  - [森林公園（2）](../Page/神農路_\(高雄市\)#主要設施.md "wikilink")

## 教育

### 大專院校

  - [國立中山大學](../Page/國立中山大學.md "wikilink")

### 高級中等學校

  - [高雄市私立中華藝術學校](../Page/高雄市私立中華藝術學校.md "wikilink")
  - [高雄市立鼓山高級中學](../Page/高雄市立鼓山高級中學.md "wikilink")
  - [高雄市私立大榮高級中學](../Page/高雄市私立大榮高級中學.md "wikilink")
  - [高雄市私立明誠高級中學](../Page/高雄市私立明誠高級中學.md "wikilink")

### 國民中學

  - [高雄市立鼓山國民中學](http://www.kusjh.kh.edu.tw/)
  - [高雄市立明華國民中學](../Page/高雄市立明華國民中學.md "wikilink")
  - [高雄市立壽山國民中學](http://www.ssjh.kh.edu.tw/)
  - [高雄市立七賢國民中學](http://www.chihjh.kh.edu.tw/)

### 國民小學

  - [高雄市鼓山區鼓山國民小學](http://www.kusps.kh.edu.tw/)
  - [高雄市鼓山區九如國民小學](http://www.jrps.kh.edu.tw/)
  - [高雄市鼓山區鼓岩國民小學](https://web.archive.org/web/20070123134520/http://163.32.208.8/)
  - [高雄市鼓山區龍華國民小學](../Page/高雄市鼓山區龍華國民小學.md "wikilink")
  - [高雄市鼓山區中山國民小學](http://www.csps.kh.edu.tw/)
  - [高雄市鼓山區內惟國民小學](../Page/高雄市鼓山區內惟國民小學.md "wikilink")
  - [高雄市鼓山區壽山國民小學](http://www.ssps.kh.edu.tw/)
  - [高雄市鼓山區美術館國民小學](../Page/高雄市鼓山區美術館國民小學.md "wikilink")（籌備中）

## 宗教禮拜場所

#### 道教和臺灣民間信仰

  - [柴山山海宮](../Page/柴山山海宮.md "wikilink")
  - [高雄代天宮](../Page/高雄代天宮.md "wikilink")
  - [高雄大義宮](../Page/高雄大義宮.md "wikilink")
  - [高雄地嶽殿](../Page/高雄地嶽殿.md "wikilink")
  - [高南東嶽殿](../Page/高南東嶽殿.md "wikilink")
  - [開台福德宮](../Page/開台福德宮.md "wikilink")
  - [哈瑪星龍鳳宮](../Page/哈瑪星龍鳳宮.md "wikilink")（紅姑娘廟）
  - [高雄十八王公廟](../Page/高雄十八王公廟.md "wikilink")（靈興殿）
  - [化龍宮](../Page/化龍宮.md "wikilink")

#### 佛教

  - [元亨寺](../Page/元亨寺.md "wikilink")
  - [龍泉寺](../Page/萬壽山龍泉寺.md "wikilink")
  - 千光寺
  - 法興禪寺

#### 天主教

  - 鼓山德露聖母堂

#### 基督新教

  - 臺灣基督長老教會（鼓山教會、鼓巖教會、內惟教會）

## 旅遊

[Tzaishan_at_nsysu.jpg](https://zh.wikipedia.org/wiki/File:Tzaishan_at_nsysu.jpg "fig:Tzaishan_at_nsysu.jpg")校門望向柴山\]\]

  - [前清打狗英國領事館](../Page/前清打狗英國領事館.md "wikilink")
  - [西子灣](../Page/西子灣.md "wikilink")
  - [哈瑪星](../Page/哈瑪星.md "wikilink")
  - [鼓山渡輪站](../Page/鼓山渡輪站.md "wikilink")
  - [舊打狗驛故事館](../Page/舊打狗驛故事館.md "wikilink")
  - [雄鎮北門](../Page/雄鎮北門.md "wikilink")
  - [柴山](../Page/壽山_\(高雄市\).md "wikilink")
  - [高雄武德殿](../Page/高雄武德殿.md "wikilink")
  - [高雄市立壽山動物園](../Page/高雄市立壽山動物園.md "wikilink")
  - [高雄市忠烈祠](../Page/高雄市忠烈祠.md "wikilink")
  - [高雄港港史館](../Page/高雄港港史館.md "wikilink")
  - [高雄港漁人碼頭](../Page/高雄港漁人碼頭.md "wikilink")
  - [公園路橋天空雲臺](../Page/公園路橋天空雲臺.md "wikilink")
  - [高雄市立美術館](../Page/高雄市立美術館.md "wikilink")
  - [內惟埤文化園區](../Page/內惟埤文化園區.md "wikilink")
  - [高雄自強新村](../Page/高雄自強新村.md "wikilink")
  - [內惟李氏古宅](../Page/內惟李氏古宅.md "wikilink")
  - [壽山國家自然公園](../Page/壽山國家自然公園.md "wikilink")

## 注釋與參考資料

<references/>

## 外部連結

  - [鼓山區公所](http://gushan.kcg.gov.tw/)
  - [高雄市立壽山動物園](http://twup.org/khzoo/)
  - [高雄市鼓山區戶政事務所](https://web.archive.org/web/20101103043002/http://cabu.kcg.gov.tw/kushan/index.asp)
  - [高雄市旗鼓地區之文學地景書寫研究](http://ndltd.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi?o=dnclcdr&s=id=%22098CCU05625029%22.&searchmode=basic/)

[Category:高雄市行政區劃](../Category/高雄市行政區劃.md "wikilink")
[Category:鼓山區](../Category/鼓山區.md "wikilink")
[Category:高雄市旅遊景點](../Category/高雄市旅遊景點.md "wikilink")

1.  [近期各月人口統計資料 - 表八、各鄉鎮市區戶數及人口數統計表](http://www.ris.gov.tw/zh_TW/346)
2.  [高雄市鼓山區歷史沿革](http://gushan.kcg.gov.tw/introduce_03.php)