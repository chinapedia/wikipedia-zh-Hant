[Gulangyu-2.jpg](https://zh.wikipedia.org/wiki/File:Gulangyu-2.jpg "fig:Gulangyu-2.jpg")
[Gulangyu-4.jpg](https://zh.wikipedia.org/wiki/File:Gulangyu-4.jpg "fig:Gulangyu-4.jpg")风格的老房子\]\]
**鼓浪屿公共租界**是中国在清末到民国时期的2个公共租界之一（另外一个公共租界是[上海公共租界](../Page/上海公共租界.md "wikilink")）。

在[鸦片战争期间](../Page/鸦片战争.md "wikilink")，英军占领了[鼓浪屿岛](../Page/鼓浪屿.md "wikilink")，直到1845年才撤军。1843年后，[厦门根据](../Page/厦门.md "wikilink")《[中英南京条约](../Page/中英南京条约.md "wikilink")》开辟为[通商口岸](../Page/通商口岸.md "wikilink")，英国另在厦门本岛内港海岸获得了一块滩地兴建英租界，鼓浪屿则保持原有风貌。

## 歷史

  - 1895年中日[甲午战争后](../Page/甲午战争.md "wikilink")，[日本統治](../Page/日本.md "wikilink")[台湾](../Page/台湾.md "wikilink")，为避免日本进一步觊觎厦门，清朝政府决定寻求“国际保护”，请列强“兼护厦门”\[1\]。

<!-- end list -->

  - 1902年1月10日（光绪二十七年十二月初一日），[英国](../Page/英国.md "wikilink")、[美国](../Page/美国.md "wikilink")、[德国](../Page/德国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[丹麦](../Page/丹麦.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[瑞挪联盟](../Page/瑞挪联盟.md "wikilink")、日本等9国驻厦门领事与[清朝](../Page/清朝.md "wikilink")[福建省兴泉永道台延年在鼓浪屿日本领事馆签订](../Page/福建省.md "wikilink")《厦门鼓浪屿公共地界章程》\[2\]，鼓浪屿成为公共租界\[3\]，次年1月，鼓浪屿公共租界工部局成立。在此前后，陆续有英、美、法、德、日等13个国家先后在岛上设立[领事馆](../Page/领事馆.md "wikilink")。

这样，厦门地区就形成2个租界隔海对峙的局面。厦门英租界面积狭小，完全是商贸区；而鼓浪屿面积较大，形成大规模的西式住宅区。

  - 这段时期，有很多[传教士来到鼓浪屿](../Page/传教士.md "wikilink")，他们建立的学校对[中国现代教育有重大影响](../Page/中国教育.md "wikilink")\[4\]，例如：1898年，英国牧师韦玉振与夫人韦爱莉到鼓浪屿传教时创办的“怀德幼稚园”是中国第一所[幼儿园](../Page/幼儿园.md "wikilink")（现已更名为日光幼儿园）\[5\]。

<!-- end list -->

  - 1930年厦门英租界被[国民政府收回后](../Page/国民政府.md "wikilink")，鼓浪屿公共租界就成为厦门地区唯一的租界。
  - 1938年日军进攻并占領厦门，鼓浪嶼成為一座“孤島”（[廈門](../Page/廈門.md "wikilink")，[金門皆為日軍控制](../Page/金門.md "wikilink")），大批[华人為避日禍移居鼓浪屿或中轉返回自由區](../Page/华人.md "wikilink")。
  - 1941年12月[太平洋战争爆发后](../Page/太平洋战争.md "wikilink")，日军进驻鼓浪屿公共租界。
  - 1943年1月11日，美国和英国决定放弃在[中国的特殊权利](../Page/中国.md "wikilink")。
  - 1943年5月28日，[汪精卫政权收回鼓浪屿公共租界](../Page/汪精卫政权.md "wikilink")。
  - 1945年，鼓浪屿公共租界被[国民政府正式收回](../Page/国民政府.md "wikilink")。为厦门市[鼓浪屿区](../Page/鼓浪屿区.md "wikilink")，2003年降为[鼓浪屿街道](../Page/鼓浪屿街道.md "wikilink")，下辖龙头、内厝2社区。

## 参考文献

[Category:鼓浪屿](../Category/鼓浪屿.md "wikilink")
[Category:厦门租界](../Category/厦门租界.md "wikilink")
[Category:在华各国租界](../Category/在华各国租界.md "wikilink")

1.  张震世：《公共租界时期的鼓浪屿》，《厦门文史资料》第3辑，第20页
2.  中国第一历史档案馆外务部档案，综合类，第4462号
3.
4.  [1](http://www.amoymagic.com/discovergulangyu5eduzh.htm)
5.