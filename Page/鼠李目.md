[Koeh-255.jpg](https://zh.wikipedia.org/wiki/File:Koeh-255.jpg "fig:Koeh-255.jpg")
*Rhamnus cathartica*\]\]
**鼠李目**（Rhamnales）是[克朗奎斯特分类法下的一個植物目級分類元](../Page/克朗奎斯特分类法.md "wikilink")，属于[双子叶植物纲](../Page/双子叶植物纲.md "wikilink")[薔薇亞綱](../Page/薔薇亞綱.md "wikilink")，包括三[科](../Page/科.md "wikilink")。[APG
III分类法中不再承認鼠李目](../Page/被子植物APG_III分类法.md "wikilink")，葡萄科被獨立為葡萄目，另外[鼠李科及](../Page/鼠李科.md "wikilink")[火筒樹科則被併入薔薇目](../Page/火筒樹科.md "wikilink")\[1\]。

## 科

  - [鼠李科](../Page/鼠李科.md "wikilink")
    Rhamnaceae：現歸[薔薇目](../Page/薔薇目.md "wikilink")

  - [火筒樹科](../Page/火筒樹科.md "wikilink")
    Leeaceae：現歸[薔薇目](../Page/薔薇目.md "wikilink")

  - [葡萄科](../Page/葡萄科.md "wikilink")
    Ampelidaceae：現歸[葡萄目](../Page/葡萄目.md "wikilink")（Vitales）

  - ceae：現歸葡萄目葡萄科

## 參考文獻

  -
  -
  -
## 外部連結

1.