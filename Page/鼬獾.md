**鼬獾**（學名：*Melogale
moschata*），是[鼬獾屬四種動物的一種](../Page/鼬獾屬.md "wikilink")，俗名：撥田豬、小豚貓、田螺狗、鰗鰍貓\[1\]，[台語稱](../Page/台語.md "wikilink")「臭羶-{猫}-」（[白話字](../Page/白話字.md "wikilink")：tshàu-hiàn\[2\]-bâ\[3\]\[4\]）。

## 外型特徵

頭體長35～40公分，尾長14～20公分，體重1～1.75公斤。臉部呈黑褐色，由頭頂經後頸至背中央有一白色縱帶，額頭至眼睛周圍有明顯的白毛，神似國劇人物之化妝臉譜，故有「花臉貍」之稱。吻端突出似豬鼻，又有「小豚貓」之稱。全身披深灰褐色粗毛，身體瘦長，四肢細短呈污灰色，爪尖銳而長，尾部具白色長毛，略為蓬鬆。鼠蹊部有味道濃厚之臭腺，故又有「臭貍」之稱。
齒式：門齒3/3，犬齒1/1，前臼齒4/4，臼齒1/2；總齒數= 38。\[5\]

## 生態習性

白天躲藏於樹洞、土洞或岩洞內休息，日落黃昏後始外出覓食。有爬樹能力，但不常上樹活動；行動緩慢，不善跳躍。主要以嗅覺找尋食物，但聽力與觸覺亦佳，喜好捕食蜥蜴、鳥類、小型囓齒類，對於蝸牛、蚯蚓、大型昆蟲等無脊椎動物也來者不拒，有時亦會取食植物果實。排遺呈黑色細長螺旋狀，是明顯易辨的跡相。由於臭腺特別發達，受驚嚇或被逼迫時亦會分泌具惡臭之氣味以驅敵。每年5～6月為其生殖高峰期，通常每胎產1～3隻幼獸。\[6\]

## 棲地分布

原生地不詳，主要棲息於[中國大陸長江以南地區](../Page/中國大陸.md "wikilink")、中南半島東側、[印度東北部](../Page/印度.md "wikilink")、[臺灣本島](../Page/臺灣.md "wikilink")、[爪哇島](../Page/爪哇島.md "wikilink")、和[婆羅洲的綠地](../Page/婆羅洲.md "wikilink")。
台灣特有亞種普遍分布於低、中海拔山區，通常棲居於原始闊葉林、灌叢區及開墾地。分布海拔最高可達2,100公尺。\[7\]

## 亞種

鼬獾包括以下亞種\[8\]

  - （[學名](../Page/學名.md "wikilink")： (Hilzheimer, 1905)）
  - （[學名](../Page/學名.md "wikilink")： Zheng and Xu, 1983）
  - （[學名](../Page/學名.md "wikilink")： (Thomas, 1922)）
  - **鼬獾指名亞種**（[学名](../Page/学名.md "wikilink")： (Gray, 1831)）
  - （[學名](../Page/學名.md "wikilink")： (G. M. Allen, 1929)）
  - （[學名](../Page/學名.md "wikilink")： (Swinhoe, 1862)）
  - （[學名](../Page/學名.md "wikilink")： (Thomas, 1925)）

## 近親

婆羅鼬獾、爪哇鼬獾、緬甸鼬獾。

## 狂犬病

正如大部分哺乳類，鼬獾也可能成為[狂犬病宿主](../Page/狂犬病.md "wikilink")。

2012年五、六月在[台灣的](../Page/台灣.md "wikilink")[雲林](../Page/雲林.md "wikilink")[古坑鄉](../Page/古坑鄉.md "wikilink")、[南投縣](../Page/南投縣.md "wikilink")[魚池鄉及](../Page/魚池鄉_\(臺灣\).md "wikilink")[鹿谷鄉等地發現的鼬獾屍體經檢驗後](../Page/鹿谷鄉.md "wikilink")，於2013年六月檢驗證實有[狂犬病](../Page/狂犬病.md "wikilink")，經通報[世界動物衛生組織](../Page/世界動物衛生組織.md "wikilink")（OIE）後，台灣自非疫區除名。\[9\]

2013年七月22日，[台東縣](../Page/台東縣.md "wikilink")[東河鄉出現第一起狂犬病鼬獾咬人案例](../Page/東河鄉_\(臺灣\).md "wikilink")\[10\]。被咬的民眾將該鼬獾送驗，檢驗已證實該鼬獾帶有狂犬病病毒。因此該民眾隨即接受[免疫球蛋白及](../Page/免疫球蛋白.md "wikilink")[疫苗的注射](../Page/疫苗.md "wikilink")\[11\]

遭感染並發病之鼬獾，會有異於其生態習性的行為產生，需特別注意。

`2013/07/28 `[`南投縣`](../Page/南投縣.md "wikilink")[`仁愛鄉`](../Page/仁愛鄉.md "wikilink")`，鼬獾於白日闖進人類住所，並攻擊咬傷人`\[12\]
`2013/07/29 `[`台南市`](../Page/台南市.md "wikilink")[`南化區`](../Page/南化區.md "wikilink")`，鼬獾於白日離開山區，闖入人類住所，並與飼養之家犬搏鬥`\[13\]

## 参考文献

[MM](../Category/IUCN無危物種.md "wikilink")
[moschata](../Category/鼬獾屬.md "wikilink")
[M](../Category/台灣特有亞種.md "wikilink")
[M](../Category/香港原生物種.md "wikilink")

1.  [鼬獾](http://gis.ymsnp.gov.tw/nature/sys_guest/Main_AL_frm.cfm?TAB_CLASS=AL&etab=DB_A_Mammal)
    ，陽明山國家公園，自然資源資料庫

2.  <http://twblg.dict.edu.tw/holodict_new/result_detail.jsp?n_no=6645&curpage=1&sample=%E8%87%AD&radiobutton=1&querytarget=1&limit=20&pagenum=3&rowcount=47>

3.  <http://twblg.dict.edu.tw/holodict_new/result.jsp?radiobutton=1&limit=20&querytarget=1&sample=%E7%8C%AB&submit.x=54&submit.y=22>

4.  <http://twblg.dict.edu.tw/holodict_new/result_detail.jsp?n_no=6312&curpage=1&sample=%E7%8C%AB&radiobutton=1&querytarget=1&limit=20&pagenum=1&rowcount=3>

5.  [「臺灣大百科全書」網站
    <http://taiwanpedia.culture.tw/>](http://nrch.culture.tw/twpedia.aspx?id=7072)

6.
7.
8.

9.  [OIE通報紀錄](http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid=13775&newlang=en)

10. [聯合新聞網：台灣第一起！
    狂犬病鼬獾咬傷人](http://udn.com/NEWS/NATIONAL/NATS2/8048287.shtml)


11. [蘋果日報：【短片】被鼬獾咬傷家犬　猛流口水遭隔離](http://www.appledaily.com.tw/realtimenews/article/life/)

12. [`咬童鼬獾染狂犬病``
     ``9歲女急投藥`](http://news.cts.com.tw/cts/general/201307/201307301285888.htmlm)

13. [`鼬獾闖南化民宅咬狗``
     ``確染狂犬病`](http://news.cts.com.tw/cts/general/201307/201307281284746.html)