《**鼹鼠的故事**》（[捷克语](../Page/捷克语.md "wikilink")：Krteček，又译“小鼴鼠妙妙奇遇記”或“小鼴鼠”）是[捷克斯洛伐克艺术家](../Page/捷克斯洛伐克.md "wikilink")[兹德涅克·米莱尔创作的卡通片](../Page/兹德涅克·米莱尔.md "wikilink")，于1956年在[布拉格首映](../Page/布拉格.md "wikilink")。

## 制作

动画角色鼹鼠最早产生于1956年的[布拉格](../Page/布拉格.md "wikilink")，米莱尔想要创作一部介绍[亚麻加工的儿童动画片](../Page/亚麻.md "wikilink")，并希望把一个动物作为主角。一次散步中他被一个鼹鼠丘绊倒，这使他最终选择了鼹鼠。1956年上映的第一部作品是
"Jak krtek ke kalhotkám
přišel"（鼹鼠是怎样得到裤子的）。自1963年起，更多的动画被创作出来，至今已有45集短片及6集長片制作并上映。

第一部作品具有[旁白](../Page/旁白.md "wikilink")，但后来创作者希望各国观众都能看懂，于是在后面的作品中去掉旁白，改用自己女儿做配音演员，为片中角色配音，并且只用强调语气的嗓音（如hele，jé，ach
jo等词），不用真实的对白。

## 音乐

由于动画中很少出现对话，因此音乐在剧集中起到了很重要的作用。早期作品采用真实乐器与电子乐器配乐，近期作品则多采用合成器。

配乐作曲：

  - Miloš Vacek
  - Vadim Petrov （1974 - 1994年间的大多数作品）

## 影响

[Edfs_coach.JPG](https://zh.wikipedia.org/wiki/File:Edfs_coach.JPG "fig:Edfs_coach.JPG")
本片在[东欧国家](../Page/东欧.md "wikilink")、[德国](../Page/德国.md "wikilink")、[奥地利和](../Page/奥地利.md "wikilink")[中国都有广泛的影迷](../Page/中国.md "wikilink")。

  - 1985年，當時[中華民國的](../Page/中華民國.md "wikilink")[公共電視台沒有自己的頻道](../Page/公共電視台.md "wikilink")，必須徵用[三台的播放時段](../Page/老三台.md "wikilink")，公共電視台曾經徵用[中華電視公司的](../Page/中華電視公司.md "wikilink")17:30
    - 18:00播放時段播放本卡通，稱為『小鼴鼠』。
  - 2011年5月16日，一个鼹鼠的填充玩具被带到[奋进号航天飞机上](../Page/奋进号航天飞机.md "wikilink")。携带者为执行[STS-134任务的美国宇航员安德鲁](../Page/STS-134.md "wikilink")·费尤斯特，其妻子为捷克裔。\[1\]\[2\]\[3\]

## 各集列表

### 短片

| 集数   | 中文译名                                 | 捷克文原名                         | 制作年份 | 时长                 |
| ---- | ------------------------------------ | ----------------------------- | ---- | ------------------ |
| 01\. | 鼹鼠是怎样得到裤子的                           | Jak krtek ke kalhotkám přišel | 1957 | 11:51              |
| 02\. | 鼹鼠与[汽车](../Page/汽车.md "wikilink")    | Krtek a autíčko               | 1963 | 14:30              |
| 03\. | 鼹鼠与[火箭](../Page/火箭.md "wikilink")    | Krtek a raketa                | 1965 | 08:40              |
| 04\. | 鼹鼠与[收音机](../Page/收音机.md "wikilink")  | Krtek a transistor            | 1968 | 08:10              |
| 05\. | 鼹鼠与绿星星                               | Krtek a zelená hvězda         | 1969 | 07:30              |
| 06\. | 鼹鼠与口香糖                               | Krtek a žvýkačka              | 1969 | 07:20              |
| 07\. | 鼹鼠是个园丁                               | Krtek zahradníkem             | 1969 | 08:03              |
| 08\. | 鼹鼠在[动物园里](../Page/动物园.md "wikilink") | Krtek v Zoo                   | 1969 | 06:57              |
| 09\. | 鼹鼠与[电视机](../Page/电视机.md "wikilink")  | Krtek a televizor             | 1970 | 05:30              |
| 10\. | 鼹鼠与刺猬                                | Krtek a ježek                 | 1970 | 08:25              |
| 11\. | 鼹鼠与棒棒糖                               | Krtek a lízátko               | 1970 | 08:00              |
| 12\. | 鼹鼠与[雨伞](../Page/雨伞.md "wikilink")    | Krtek a paraplíčko            | 1971 | 07:50              |
| 13\. | 鼹鼠是个[画家](../Page/画家.md "wikilink")   | Krtek malířem                 | 1972 | 09:35              |
| 14\. | 鼹鼠与[火柴盒](../Page/火柴盒.md "wikilink")  | Krtek a zápalky               | 1974 | 05:30              |
| 15\. | 鼹鼠与[音乐](../Page/音乐.md "wikilink")    | Krtek a muzika                | 1974 | 05:30              |
| 16\. | 鼹鼠与[电话](../Page/电话.md "wikilink")    | Krtek a telefon               | 1974 | 05:45              |
| 17\. | 鼹鼠是个[化学家](../Page/化学家.md "wikilink") | Krtek chemikem                | 1974 | 05:30              |
| 18\. | 鼹鼠是个钟表匠                              | Krtek hodinářem               | 1974 | 05:25              |
| 19\. | 鼹鼠与魔毯                                | Krtek a koberec               | 1974 | 05:25              |
| 20\. | 鼹鼠与面具                                | Krtek a karneval              | 1975 | 05:47              |
| 21\. | 鼹鼠与[推土机](../Page/推土机.md "wikilink")  | Krtek a buldozér              | 1975 | 06:01              |
| 22\. | 鼹鼠与[攝像頭](../Page/攝像頭.md "wikilink")  | Krtek fotografem              | 1975 | 06:05              |
| 23\. | 鼹鼠在[沙漠里](../Page/沙漠.md "wikilink")   | Krtek na poušti               | 1975 | 06:10              |
| 24\. | 鼹鼠与雞蛋                                | Krtek a vejce                 | 1975 | 05:25              |
| 25\. | 鼹鼠过[圣诞节](../Page/圣诞节.md "wikilink")  | Krtek o Vánocích              | 1975 | 05:45              |
| 26\. | 鼹鼠在大城市                               | Krtek ve městě                | 1982 | 28:51<sup>長片</sup> |
| 27\. | 鼹鼠的梦                                 | Krtek ve snu                  | 1984 | 28:09<sup>長片</sup> |
| 28\. | 鼹鼠与药品                                | Krtek a medicína              | 1987 | 28:08<sup>長片</sup> |
| 29\. | 鼹鼠是个电影明星                             | Krtek filmová hvězda          | 1988 | 27:47<sup>長片</sup> |
| 30\. | 鼹鼠和老鹰                                | Krtek a orel                  | 1992 | 28:05<sup>長片</sup> |
| 31\. | 鼹鼠和钟                                 | Krtek a hodiny                | 1994 | 28:04<sup>長片</sup> |
| 32\. | 鼹鼠和朋友们                               | Krtek a kamarádi              | 1995 | 05:18              |
| 33\. | 鼹鼠的生日                                | Krtek a oslava                | 1995 | 05:34              |
| 34\. | 鼹鼠的周末                                | Krtek a víkend                | 1995 | 05:05              |
| 35\. | 鼹鼠和机器人                               | Krtek a robot                 | 1995 | 05:05              |
| 36\. | 鼹鼠和小鸭子                               | Krtek a kachničky             | 1995 | 05:25              |
| 37\. | 鼹鼠和煤炭                                | Krtek a uhlí                  | 1995 | 05:09              |
| 38\. | 鼹鼠和小野兔                               | Krtek a zajíček               | 1997 | 05:08              |
| 39\. | 鼹鼠和誕生                                | Krtek a maminka               | 1997 | 05:10              |
| 40\. | 鼹鼠开地下铁                               | Krtek a metro                 | 1997 | 05:19              |
| 41\. | 鼹鼠和蘑菇                                | Krtek a houby                 | 1997 | 05:20              |
| 42\. | 鼹鼠和雪人                                | Krtek a sněhulák              | 1997 | 05:09              |
| 43\. | 鼹鼠和洪水                                | Krtek a myška                 | 1997 | 05:34              |
| 44\. | 鼹鼠和海边                                | Krtek a dovolená              | 1998 | 01:15              |
| 45\. | 鼹鼠和音乐泉                               | Krtek a pramen                | 1999 | 05:33              |
| 46\. | 鼹鼠和笛子                                | Krtek a flétna                | 1999 | 05:05              |
| 47\. | 鼹鼠和畫筆                                | Krtek a šťoura                | 1999 | 05:21              |
| 48\. | 鼹鼠和小鱼                                | Krtek a rybka                 | 2000 | 05:02              |
| 49\. | 鼹鼠和燕子                                | Krtek a vlaštovka             | 2000 | 04:40              |
| 50\. | 鼹鼠和青蛙                                | Krtek a žabka                 | 2002 | 05:20              |

## 参考资料

## 外部链接

  - [中国中央电视台](../Page/中国中央电视台.md "wikilink")[《鼹鼠的故事》栏目](http://space.tv.cctv.com/podcast/yanshudegushi)
  - [填色頁 -
    鼹鼠的故事](http://cn.colorkid.net/%E5%A1%AB%E8%89%B2%E9%A0%81-%E5%8A%A8%E7%94%BB/%E5%A1%AB%E8%89%B2%E9%A0%81-%E9%BC%B9%E9%BC%A0%E7%9A%84%E6%95%85%E4%BA%8B)
  - Little mole fan website
    [www.littlemole.cz](../Page/www.littlemole.cz.md "wikilink")

[Category:動畫影集](../Category/動畫影集.md "wikilink")
[Category:捷克斯洛伐克](../Category/捷克斯洛伐克.md "wikilink")
[Category:繪本改編動畫](../Category/繪本改編動畫.md "wikilink")
[Category:哺乳動物主角題材故事](../Category/哺乳動物主角題材故事.md "wikilink")
[Category:虛構哺乳動物](../Category/虛構哺乳動物.md "wikilink")

1.
2.
3.