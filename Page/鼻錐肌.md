**鼻錐肌**是人體一塊小小的金字塔形[肌肉](../Page/肌肉.md "wikilink")，深至[眶上神經](../Page/眶上神經.md "wikilink")、[眶上動脈和](../Page/眶上動脈.md "wikilink")[眶上靜脈](../Page/眶上靜脈.md "wikilink")。

## 作用

鼻錐肌收縮時兩條眉毛之間的肌肉被拉下、拉攏，也是用來提起[鼻孔的其中一塊肌肉](../Page/鼻孔.md "wikilink")。同時其運用也可被視爲[憤怒的一種表現](../Page/憤怒.md "wikilink")。

## 外部連結

  - [解剖學(Anatomy)-肌肉(muscle)-Muscles of Facial
    Expression(顏面表情肌)](http://smallcollation.blogspot.com/2013/02/anatomy-muscle-muscles-of-facial.html)

  -
[Category:头颈肌肉](../Category/头颈肌肉.md "wikilink")