**齊召南**（），[字](../Page/表字.md "wikilink")**次風**，[號](../Page/號.md "wikilink")**瓊台**，[晚號](../Page/晚號.md "wikilink")**息園**。[浙江](../Page/浙江.md "wikilink")[天台人](../Page/天台县.md "wikilink")。[清代翰林](../Page/清代.md "wikilink")，[地理學家](../Page/地理學家.md "wikilink")。

## 生平

幼有神童之稱，精於輿地之學，又善[書法](../Page/書法.md "wikilink")。嘗臨[王羲之](../Page/王羲之.md "wikilink")《蘭亭帖》。[雍正七年](../Page/雍正.md "wikilink")（1729年），己酉科鄉試中副車，[雍正十一年](../Page/雍正.md "wikilink")（1733年），舉[博學鴻詞](../Page/博學鴻詞.md "wikilink")，以副榜[貢生被薦](../Page/貢生.md "wikilink")。[乾隆元年](../Page/乾隆.md "wikilink")（1736年），召試於[保和殿](../Page/保和殿.md "wikilink")，欽定二等第八名，為[翰林院](../Page/翰林院.md "wikilink")[庶吉士](../Page/庶吉士.md "wikilink")，授[檢討](../Page/檢討.md "wikilink")。次年参修《[大清一统志](../Page/大清一统志.md "wikilink")》。[乾隆六年](../Page/乾隆.md "wikilink")，撰《外藩书》，[乾隆帝赞道](../Page/乾隆帝.md "wikilink")：“齐召南之博学，一至是乎！”[乾隆十二年](../Page/乾隆.md "wikilink")，充《[续文献通考](../Page/续文献通考.md "wikilink")》[副总裁](../Page/副总裁.md "wikilink")。又曾前往[山東](../Page/山東.md "wikilink")、[江蘇](../Page/江蘇.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[福建](../Page/福建.md "wikilink")、[雲南等地進行勘查](../Page/雲南.md "wikilink")。[乾隆二十六年](../Page/乾隆.md "wikilink")（1761年）完成《水道提綱》28卷。[乾隆三十二年](../Page/乾隆.md "wikilink")，其族兄[齐周华因](../Page/齐周华.md "wikilink")[文字獄賈祸被](../Page/文字獄.md "wikilink")[凌迟处死](../Page/凌迟.md "wikilink")，召南遭受牵连，革职歸鄉。不久病卒，葬於[天台县街头镇花坑](../Page/天台县.md "wikilink")。

## 著作

著有《宝纶堂集古录》、《宝纶堂文钞诗钞》、《齐太史移居集》、《琼台集》、《历代帝王年表》、《后汉公卿表》等。另有《水道提纲》是最重要的作品。

## 參考書目

  - [杭世骏](../Page/杭世骏.md "wikilink")：《礼部侍郎齐公墓志铭》，《道古堂文集》卷四十一
  - [袁枚](../Page/袁枚.md "wikilink")：《原任礼部侍郎齐公墓志铭》，《小仓山房文集》卷二十五

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝地理學家](../Category/清朝地理學家.md "wikilink")
[Category:台州人](../Category/台州人.md "wikilink")
[Z召](../Category/齊姓.md "wikilink")