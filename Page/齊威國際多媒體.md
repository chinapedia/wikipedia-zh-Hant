**齊威國際多媒體股份有限公司**（**Power International Multimedia
Inc.**），簡稱**齊威國際**，是[台灣過去一家動畫代理公司](../Page/台灣.md "wikilink")。

## 概要

  - 前身為1996年成立的**齊威唱片有限公司**。公司[法人於](../Page/法人.md "wikilink")2012年12月25日解散。
  - 主要代理的範圍為[日本](../Page/日本.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[歐洲動畫與商品](../Page/歐洲.md "wikilink")，也有代理[古典音樂與](../Page/古典音樂.md "wikilink")[音樂劇](../Page/音樂劇.md "wikilink")。

## 代理動畫

### 日本經典動畫系列

主要為台灣早期[老三台播放過的日本動畫](../Page/老三台.md "wikilink")，其中不少是《[世界名作劇場](../Page/世界名作劇場.md "wikilink")》系列作品。

  - [悟空大冒險](../Page/悟空大冒險.md "wikilink")（）
  - [金銀島](../Page/金銀島_\(動畫\).md "wikilink")（）
  - [海王子](../Page/海王子.md "wikilink")（）
  - [咪咪流浪記](../Page/咪咪流浪記.md "wikilink")（）
  - [靈犬雪麗](../Page/靈犬雪麗.md "wikilink")（）
  - [寶馬王子](../Page/寶馬王子.md "wikilink")（）
  - [北海小英雄](../Page/北海小英雄.md "wikilink")（）
  - [小神童](../Page/骑鹅历险记.md "wikilink")（）
  - [未來少年柯南](../Page/未來少年柯南.md "wikilink")（）
  - [小蜜蜂美雅](../Page/小蜜蜂美雅.md "wikilink")（）
  - [天方夜譚](../Page/天方夜譚_\(電視動畫\).md "wikilink")（）
  - [綠野仙蹤](../Page/绿野仙踪_\(童话\).md "wikilink")（）
  - [喬琪姑娘](../Page/喬琪姑娘.md "wikilink")（）
  - [夢幻拉拉](../Page/夢幻拉拉.md "wikilink")（）
  - [小天使](../Page/小天使.md "wikilink")（）
  - [魔動王](../Page/魔動王.md "wikilink")（）

**世界名作劇場系列**

  - [龍龍與忠狗](../Page/龍龍與忠狗.md "wikilink")（）
  - [萬里尋母](../Page/萬里尋母.md "wikilink")（）
  - [小浣熊](../Page/小浣熊.md "wikilink")（）
  - [小英的故事](../Page/小英的故事.md "wikilink")（）
  - [清秀佳人](../Page/紅髮的安妮.md "wikilink")（）
  - [湯姆歷險記](../Page/湯姆索耶的冒險.md "wikilink")（）
  - [新魯賓遜漂流記](../Page/新魯賓遜漂流記.md "wikilink")（）
  - [莎拉公主](../Page/莎拉公主.md "wikilink")（）
  - [小安娜](../Page/小安娜.md "wikilink")（）
  - [小婦人](../Page/愛的小婦人物語.md "wikilink")（）
  - [小公子](../Page/小公子.md "wikilink")（）
  - [長腿叔叔](../Page/我的長腿叔叔.md "wikilink")（）
  - [真善美](../Page/崔普一家物語.md "wikilink")（）
  - [新小婦人](../Page/小婦人物語_南與喬老師.md "wikilink")（）
  - [七海小英雄](../Page/七海小英雄.md "wikilink")（）
  - [羅密歐的藍天](../Page/羅密歐的藍天.md "wikilink")（）

### 日本動畫

  - [千面女郎](../Page/玻璃假面.md "wikilink")（）

  - [怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")（）

  - [戰鬥陀螺](../Page/戰鬥陀螺.md "wikilink")（）

  - [花田少年史](../Page/花田少年史.md "wikilink")

  - [攻殼機動隊](../Page/攻殼機動隊.md "wikilink")

  - [三眼神童](../Page/三眼神童.md "wikilink")（）

  - [超激力戰鬥車](../Page/超激力戰鬥車.md "wikilink")（）

  - [極道鮮師](../Page/極道鮮師.md "wikilink")（）

  - [星空防衛隊](../Page/星空防衛隊.md "wikilink")（）

  -
  - [.hack//SIGN駭客時空](../Page/.hack/SIGN.md "wikilink")（）

  - [.hack//黃昏的腕輪傳說](../Page/.hack/黃昏的腕輪傳說.md "wikilink")（）

  - [復仇天使](../Page/復仇天使.md "wikilink")（）

  - [終極幻想世界-太空戰士](../Page/最终幻想：无限.md "wikilink")（）

  - [PEACE MAKER鐵](../Page/PEACE_MAKER鐵.md "wikilink")（）

  - [狼雨](../Page/狼雨.md "wikilink")（WOLF'S RAIN）

  - [青之6號](../Page/青之6號.md "wikilink")（）

  -
  - [鬼影投手](../Page/ONE_OUTS.md "wikilink")（）

  - [天國少女](../Page/天國少女.md "wikilink")（）

  - [最高機密](../Page/最高機密_\(漫畫\).md "wikilink")（）

### 其他

  - [雷鳥神機隊](../Page/雷鳥神機隊.md "wikilink")（THUNDERBIRDS）
  - [小鴨歷險記](../Page/小鴨歷險記.md "wikilink")（The First Snow of Winter）
  - [哆基朴的天空](../Page/哆基朴的天空.md "wikilink")（ Doggy Poo）
  - [復活森林](../Page/復活森林.md "wikilink")（THE LIVING FOREST）（西班牙）
  - [蜜蜜心世界](../Page/蜜蜜心世界.md "wikilink")（韓國）
  - [蟲蟲的異想世界](../Page/蟲蟲的異想世界.md "wikilink")（Funny Little Bugs）
  - [王子與公主](../Page/王子與公主.md "wikilink")（Prince et Princess）
  - 俄羅斯動畫大師作品集（MASTERS of RUSSIAN ANIMATION）

[Category:1996年成立的公司](../Category/1996年成立的公司.md "wikilink")
[Category:2012年結業公司](../Category/2012年結業公司.md "wikilink")
[Category:動畫產業公司](../Category/動畫產業公司.md "wikilink")
[Category:總部位於臺北市信義區的工商業機構](../Category/總部位於臺北市信義區的工商業機構.md "wikilink")