[Karte_westwall.png](https://zh.wikipedia.org/wiki/File:Karte_westwall.png "fig:Karte_westwall.png")
 **齐格菲防线**（[德文](../Page/德文.md "wikilink")：Der
Westwall／Siegfried-Linie）是[纳粹德国在](../Page/纳粹德国.md "wikilink")[第二次世界大战开始前](../Page/第二次世界大战.md "wikilink")，在其西部边境地区构筑的对抗法国[马其诺防线的筑垒体系](../Page/马其诺防线.md "wikilink")。

该项目由[德国著名的建筑工程组织](../Page/德国.md "wikilink")：托特机构负责，德国人称之为“西墙”或“齐格菲阵地”，其他国家多称之为“齐格菲防线”。构筑齐格菲防线的目的是为了掩护德国[西线](../Page/西线.md "wikilink")，并作为向西进攻的屯兵场以及支援进攻的[重炮阵地](../Page/重炮.md "wikilink")。防线工程是1936年德国占领[莱茵兰之后开始构筑的](../Page/莱茵兰.md "wikilink")，至1939年基本建成。防线从德国靠近[荷兰边境的](../Page/荷兰.md "wikilink")[克莱沃起沿着与](../Page/克莱沃.md "wikilink")[比利时](../Page/比利时.md "wikilink")、[卢森堡](../Page/卢森堡.md "wikilink")、[法国接壤的边境延伸至](../Page/法国.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[巴塞尔](../Page/巴塞尔.md "wikilink")，全长达630公里。

防线由障碍地带、主防御地带和后方阵地3個部分组成，纵深35公里至75公里。障碍地带主要是[地雷场](../Page/地雷.md "wikilink")、[铁丝网](../Page/铁丝网.md "wikilink")、防坦克壕以及著名的“[龙牙](../Page/龙牙.md "wikilink")”系统。主防御地带的最前缘位于障碍地带后方数十至数百米处，配备钢筋混凝土和钢铁装甲的机枪、火炮工事以及指挥所、观察所、人员掩蔽部、车辆洞库、弹药库、物资库等。后方阵地位于主防御地带后方数公里至数十公里处，主要是预备队人员掩蔽部、预备队车辆洞库以及战备物资库，在法德交界地段还配属有170毫米至305毫米要塞化远程重炮群（但1940年6月以后被撤出，用于[英吉利海峡群岛的要塞](../Page/英吉利海峡.md "wikilink")）。

与马其诺防线相比，齐格菲防线的特点是大部分工事较小，结构较简单，但数量多达11,860个，远超[马其诺防线](../Page/马其诺防线.md "wikilink")。该防线使用[混凝土](../Page/混凝土.md "wikilink")931万吨，是马其诺防线的2.4倍；使用[钢铁](../Page/钢铁.md "wikilink")35万吨，是马其诺防线的2.3倍。1939年9月德军在东面闪击波兰时，共有46个包含预备队的[步兵](../Page/步兵.md "wikilink")[师依托该防线防御法军](../Page/师.md "wikilink")，但法军没有进攻。该防线与对面的马其诺防线，共同促成了德法双方开战后长达8个月时间的著名“[静坐战](../Page/假战.md "wikilink")”。1944年9月，英、美[盟军从西线向德国本土进攻时](../Page/盟军.md "wikilink")，德军依托这一防线[阻挡了盟军的行动](../Page/許特根森林戰役.md "wikilink")。12月，美军以饱和轰炸和炮击支援试图突破，但没有成功。直到1945年2月盟军重新发动进攻时，该防线终被突破，至此该防线一共阻滞了盟军5个月时间。

## 參見

  - [西方的背叛](../Page/西方的背叛.md "wikilink")
  - [绥靖政策](../Page/绥靖政策.md "wikilink")

[Category:第二次世界大戰](../Category/第二次世界大戰.md "wikilink")
[Category:德国军事设施](../Category/德国军事设施.md "wikilink")
[Category:纳粹德国军事](../Category/纳粹德国军事.md "wikilink")
[Category:纳粹德国建筑物](../Category/纳粹德国建筑物.md "wikilink")