**齿鲸小目**（[学名](../Page/学名.md "wikilink")：），[鲸类小目之一](../Page/鲸.md "wikilink")，区别于[须鲸](../Page/须鲸.md "wikilink")，该小目所属各种的共同点是口中生有细密的[牙齒](../Page/牙齒.md "wikilink")，包含[海豚](../Page/海豚.md "wikilink")、[鼠海豚與一些有牙齒的鯨魚例如](../Page/鼠海豚科.md "wikilink")[喙鯨與](../Page/喙鯨.md "wikilink")[抹香鯨](../Page/抹香鯨.md "wikilink")，共有73種齒鯨。齒鯨與鬚鯨被認為在34[百萬年前分成兩群](../Page/百萬年前.md "wikilink")。

體型最小的齒鯨為[小頭鼠海豚](../Page/小頭鼠海豚.md "wikilink")，體長約 、而體重約 。體型最大的齒鯨則為抹香鯨，體長約
、而體重約
。許多種類的齒鯨為[兩性異形](../Page/兩性異形.md "wikilink")，兩性之間體型上有所差異。齒鯨具有流線型的身體與一對鰭狀肢，泳速可達
20
節。齿鲸重要食物为鱼类、乌贼，某些种，例如[虎鯨](../Page/虎鯨.md "wikilink")，有捕食海洋哺乳动物和海鸟的习性。齒鯨在水中與空氣中均具有靈敏的聽力，部分種類甚至視力退化，完全仰賴聽力感知周遭環境。[淡水豚以外的所有齒鯨都具有](../Page/淡水豚.md "wikilink")[鯨脂](../Page/鯨脂.md "wikilink")，可以隔絕體外的低水溫。

齒鯨主要受到的威脅為[捕鯨](../Page/捕鯨.md "wikilink")、[海豚驅獵](../Page/海豚驅獵.md "wikilink")、誤捕與[海洋汙染](../Page/海洋汙染.md "wikilink")。其中[白鱀豚由於](../Page/白鱀豚.md "wikilink")[長江的汙染](../Page/長江.md "wikilink")，數量急遽減少，現已被[國際自然保護聯盟宣布為](../Page/國際自然保護聯盟.md "wikilink")[功能性滅絕](../Page/功能性滅絕.md "wikilink")。

## 解剖学特征

齿鲸仅有一个[噴氣孔](../Page/噴氣孔_\(生物學\).md "wikilink")，位于头部前方接近嘴唇部。与须鲸相比，齿鲸体长较小，除[抹香鲸外](../Page/抹香鲸.md "wikilink")，典型体长小于10米，嘴喙明显（虎鲸、伪虎鲸和抹香鲸除外）。

齒鯨的體型多半遠小於[鬚鯨](../Page/鬚鯨.md "wikilink")，部分種類為兩性異形，雌性體型大於雄性。唯一的例外為[抹香鯨](../Page/抹香鯨.md "wikilink")，其雄性體型大於雌性\[1\]\[2\]。

除了淡水豚外，幾乎所有的齒鯨均具有一層厚[鯨脂](../Page/鯨脂.md "wikilink")。棲息於極區的部分齒鯨，鯨脂層厚度可達
。這些鯨脂可以提供浮力、保護來自掠食者的攻擊、能量儲存以及隔絕外界的低溫。剛出生的幼鯨缺乏鯨脂，有些種類透過[毫毛代替保暖](../Page/毫毛.md "wikilink")\[3\]\[4\]。

## 行为特征

### 鳴叫

绝大多数齿鲸小目的成员可以发出鸣叫，且叫声多样。

### 运动

齿鲸游泳动作较为迅速和灵活，小型种有骑浪行为。

### 社会行为

齿鲸大多结群活动，种群数量较大，海豚种群有时可达上千只之多。

## 分类

  - [海豚科](../Page/海豚科.md "wikilink") Delphinidae
      - [喙頭海豚屬](../Page/喙頭海豚屬.md "wikilink") *Cephalorhynchus*
          - [花斑喙頭海豚](../Page/花斑喙頭海豚.md "wikilink") *Cephalorhyncus
            commersonii*
          - [黑喙頭海豚](../Page/黑喙頭海豚.md "wikilink") *Cephalorhyncus
            eutropia*
          - [喙頭海豚](../Page/喙頭海豚.md "wikilink") *Cephalorhyncus
            heavisidii*
          - [白頭喙頭海豚](../Page/白頭喙頭海豚.md "wikilink") *Cephalorhyncus
            hectori*
      - [真海豚屬](../Page/真海豚屬.md "wikilink") *Delphinus*
          - [長喙真海豚](../Page/長喙真海豚.md "wikilink") *Delphinus capensis*
          - [短喙真海豚](../Page/短喙真海豚.md "wikilink") *Delphinus delphis*
          - [印度洋長喙真海豚](../Page/印度洋長喙真海豚.md "wikilink") *Delphinus
            tropicalis*
      - [倭圓頭鯨屬](../Page/倭圓頭鯨屬.md "wikilink") *Feresa*
          - [倭圓頭鯨](../Page/倭圓頭鯨.md "wikilink") *Feresa attenuata*
      - [圓頭鯨屬](../Page/圓頭鯨屬.md "wikilink") *Globicephala*
          - [短鰭圓頭鯨](../Page/短鰭圓頭鯨.md "wikilink") *Globicephala
            macrorhyncus*
          - [黑圓頭鯨](../Page/黑圓頭鯨.md "wikilink") *Globicephala melas*
      - [灰海豚屬](../Page/灰海豚屬.md "wikilink") *Grampus*
          - [灰海豚](../Page/灰海豚.md "wikilink") *Grampus griseus*
      - [壇喙海豚屬](../Page/壇喙海豚屬.md "wikilink") *Lagenodelphis*
          - [霍氏海豚](../Page/霍氏海豚.md "wikilink") *Lagenodelphis hosei*
      - [瓶喙海豚屬](../Page/瓶喙海豚屬.md "wikilink") *Lagenorhynchus*
          - [白腰斑紋海豚](../Page/白腰斑紋海豚.md "wikilink") *Lagenorhynchus
            acutus*
          - [白喙海豚](../Page/白喙海豚.md "wikilink") *Lagenorhynchus
            albirostris*
          - [黑頤海豚](../Page/黑頤海豚.md "wikilink") *Lagenorhynchus
            australis*
          - [十字紋海豚](../Page/十字紋海豚.md "wikilink") *Lagenorhynchus
            cruciger*
          - [斜紋海豚](../Page/斜紋海豚.md "wikilink") *Lagenorhynchus
            obliquidens*
          - [烏色海豚](../Page/烏色海豚.md "wikilink") *Lagenorhynchus obscurus*
      - [露脊海豚屬](../Page/露脊海豚屬.md "wikilink") *Lissodelphis*
          - [北露脊海豚](../Page/北露脊海豚.md "wikilink") *Lissodelphis borealis*
          - [南露脊海豚](../Page/南露脊海豚.md "wikilink") *Lissodelphis peronii*
      - [伊豚属](../Page/伊豚属.md "wikilink") *Orcaella*
          - [短吻海豚](../Page/短吻海豚.md "wikilink") *Orcaella brevirostris*
      - [虎鯨屬](../Page/虎鯨屬.md "wikilink") *Orcinus*
          - [虎鯨](../Page/虎鯨.md "wikilink") *Orcinus orca*
      - [瓜頭海豚屬](../Page/瓜頭海豚屬.md "wikilink") *Peponocephala*
          - [瓜頭海豚](../Page/瓜頭海豚.md "wikilink") *Peponocephala electra*
      - [僞虎鯨屬](../Page/僞虎鯨屬.md "wikilink") *Pseudorca*
          - [僞虎鯨](../Page/僞虎鯨.md "wikilink") *Pseudorca crassidens*
      - [侏儒白海豚屬](../Page/侏儒白海豚屬.md "wikilink") *Sotalia*
          - [亞馬遜白海豚](../Page/:南美長吻海豚.md "wikilink") *Sotalia
            fluviatilis*
      - [白海豚屬](../Page/白海豚屬.md "wikilink") *Sousa*
          - [中華白海豚](../Page/中華白海豚.md "wikilink") *Sousa chinensis*
          - [灰白海豚](../Page/灰白海豚.md "wikilink") *Sousa plumbea*
          - [西非白海豚](../Page/西非白海豚.md "wikilink") *Sousa teuszii*
      - [原海豚屬](../Page/原海豚屬.md "wikilink") *Stenella*
          - [白點原海豚](../Page/白點原海豚.md "wikilink") *Stenella attenuata*
          - [細斑原海豚](../Page/細斑原海豚.md "wikilink") *Stenella clymene*
          - [條紋原海豚](../Page/條紋原海豚.md "wikilink") *Stenella coeruleoalba*
          - [花斑原海豚](../Page/花斑原海豚.md "wikilink") *Stenella frontalis*
          - [長吻原海豚](../Page/長吻原海豚.md "wikilink") *Stenella longirostris*
      - [尖嘴海豚屬](../Page/尖嘴海豚屬.md "wikilink") *Steno*
          - [糙齒尖嘴海豚](../Page/糙齒尖嘴海豚.md "wikilink") *Steno bredanensis*
      - [寬吻海豚屬](../Page/寬吻海豚屬.md "wikilink") *Tursiops*
          - [東方寬吻海豚](../Page/東方寬吻海豚.md "wikilink") *Tursiops aduncus*
          - [寬吻海豚](../Page/寬吻海豚.md "wikilink") *Tursiops truncatus*
  - [一角鲸科](../Page/一角鲸科.md "wikilink") Monodontidae
      - [白鯨屬](../Page/白鯨屬.md "wikilink") *Delphinapterus*
          - [白鯨](../Page/白鯨.md "wikilink") *Delphinapterus leucas*
      - [一角鯨屬](../Page/一角鯨屬.md "wikilink") *Monodon*
          - [一角鯨](../Page/一角鯨.md "wikilink") *Monodon monoceros*
  - [鼠海豚科](../Page/鼠海豚科.md "wikilink") Phocoenidae
      - [江豚屬](../Page/江豚屬.md "wikilink") *Neophocaena*
          - [江豚](../Page/江豚.md "wikilink") *Neophocaena phocaenoides*
      - [鼠海豚屬](../Page/鼠海豚屬.md "wikilink") *Phocoena*
          - [南美鼠海豚](../Page/南美鼠海豚.md "wikilink") *Phocoena dioptrica*
          - [鼠海豚](../Page/鼠海豚.md "wikilink") *Phocoena phocaena*
          - [太平洋鼠海豚](../Page/太平洋鼠海豚.md "wikilink") *Phocoena sinus*
          - [棘鰭鼠海豚](../Page/棘鰭鼠海豚.md "wikilink") *Phocoena spinipinnis*
      - [無喙鼠海豚屬](../Page/無喙鼠海豚屬.md "wikilink") *Phocoenoides*
          - [無喙鼠海豚](../Page/無喙鼠海豚.md "wikilink") *Phocoenoides dalli*
  - [抹香鲸科](../Page/抹香鲸科.md "wikilink") Physeteridae
      - [抹香鯨屬](../Page/抹香鯨屬.md "wikilink") *Physeter*
          - [抹香鯨](../Page/抹香鯨.md "wikilink") *Physeter macrocephalus*
  - [小抹香鯨科](../Page/小抹香鯨科.md "wikilink") Kogiidae
      - [小抹香鯨屬](../Page/小抹香鯨屬.md "wikilink") *Kogia*
          - [小抹香鯨](../Page/小抹香鯨.md "wikilink") *Kogia breviceps*
          - [倭抹香鯨](../Page/倭抹香鯨.md "wikilink") *Kogia sima*
  - [亞馬孫河豚科](../Page/亞馬孫河豚科.md "wikilink") Iniidae
      - [亞馬孫河豚屬](../Page/亞馬孫河豚屬.md "wikilink") *Inia*
          - [亞馬孫河豚](../Page/亞馬孫河豚.md "wikilink") *Inia geoffrensis*
  - [白鱀豚科](../Page/白鱀豚科.md "wikilink") Lipotidae
      - [白鱀豚屬](../Page/白鱀豚屬.md "wikilink") *Lipotes*
          - [白鱀豚](../Page/白鱀豚.md "wikilink") *Lipotes vexillifer*
  - [普拉塔河豚科](../Page/普拉塔河豚科.md "wikilink") Pontoporiidae
      - [普拉塔河豚屬](../Page/普拉塔河豚屬.md "wikilink") *Pontoporia*
          - [普拉塔河豚](../Page/普拉塔河豚.md "wikilink") *Pontoporia
            blainvillei*
  - [恒河豚科](../Page/恒河豚科.md "wikilink") Platanistidae
      - [恒河豚屬](../Page/恒河豚屬.md "wikilink") *Platanista*
          - [恒河豚](../Page/恒河豚.md "wikilink") *Platanista gangetica*
  - [喙鯨科](../Page/喙鯨科.md "wikilink") Ziphidae
      - [貝喙鯨屬](../Page/貝喙鯨屬.md "wikilink") *Berardius*
          - [阿氏喙鯨](../Page/阿氏喙鯨.md "wikilink") *Berardius arnuxii*
          - [貝氏喙鯨](../Page/貝氏喙鯨.md "wikilink") *Berardius bairdii*
      - [瓶鼻鯨屬](../Page/瓶鼻鯨屬.md "wikilink") *Hyperoodon*
          - [北瓶鼻鯨](../Page/北瓶鼻鯨.md "wikilink") *Hyperoodon ampullatus*
          - [南瓶鼻鯨](../Page/南瓶鼻鯨.md "wikilink") *Hyperoodon planifrons*
      - [印太喙鯨屬](../Page/印太喙鯨屬.md "wikilink") *Indopacetus*
          - [熱帶瓶鼻鯨](../Page/熱帶瓶鼻鯨.md "wikilink") *Indopacetus pacificus*
      - [長喙鯨屬](../Page/長喙鯨屬.md "wikilink") *Mesoplodon*
          - [梭氏中喙鯨](../Page/梭氏中喙鯨.md "wikilink") *Mesoplodon bidens*
          - [安氏中喙鯨](../Page/安氏中喙鯨.md "wikilink") *Mesoplodon bowdoini*
          - [哈氏中喙鯨](../Page/哈氏中喙鯨.md "wikilink") *Mesoplodon carlhubbsi*
          - [柏氏中喙鯨](../Page/柏氏中喙鯨.md "wikilink") *Mesoplodon
            densirostris*
          - [傑氏中喙鯨](../Page/傑氏中喙鯨.md "wikilink") *Mesoplodon europaeus*
          - [銀杏齒中喙鯨](../Page/銀杏齒中喙鯨.md "wikilink") *Mesoplodon
            ginkgodens*
          - [哥氏中喙鯨](../Page/哥氏中喙鯨.md "wikilink") *Mesoplodon grayi*
          - [賀氏中喙鯨](../Page/賀氏中喙鯨.md "wikilink") *Mesoplodon hectori*
          - [長齒中喙鯨](../Page/長齒中喙鯨.md "wikilink") *Mesoplodon layardii*
          - [初氏中喙鯨](../Page/初氏中喙鯨.md "wikilink") *Mesoplodon mirus*
          - [佩氏中喙鯨](../Page/佩氏中喙鯨.md "wikilink") *Mesoplodon perrini*
          - [小中喙鯨](../Page/小中喙鯨.md "wikilink") *Mesoplodon peruvianus*
          - [史氏中喙鯨](../Page/史氏中喙鯨.md "wikilink") *Mesoplodon stejnegeri*
          - [鏟齒中喙鯨](../Page/鏟齒中喙鯨.md "wikilink") *Mesoplodon traversii*
      - [塔喙鯨屬](../Page/塔喙鯨屬.md "wikilink") *Tasmacetus*
          - [謝氏塔喙鯨](../Page/謝氏塔喙鯨.md "wikilink") *Tasmacetus shepherdi*
      - [柯喙鯨屬](../Page/柯喙鯨屬.md "wikilink") *Ziphius*
          - [柯氏喙鯨](../Page/柯氏喙鯨.md "wikilink") *Ziphius cavirostris*
  - [肯氏海豚科](../Page/肯氏海豚科.md "wikilink") *Kentriodontidae*†
      - [肯氏海豚](../Page/肯氏海豚.md "wikilink") *Kentriodon*†
  - *Rhabdosteidae*†
  - [鮫齒鲸科](../Page/鮫齒鲸科.md "wikilink") *Squalodontidae*†
      - [鮫齒鯨](../Page/鮫齒鯨.md "wikilink") *Squalodon*†
  - [海牛鯨科](../Page/海牛鯨科.md "wikilink") *Odobenocetopsidae*†
      - [海牛鯨](../Page/海牛鯨.md "wikilink") *Odobenocetops*†
  - [劍吻古豚科](../Page/劍吻古豚科.md "wikilink") *Eurhinodelphinidae*†

## 生存威脅

齒鯨有時會在商業漁業中遭到，被困在漁網中或是意外吞下魚鉤。[流刺網與](../Page/流刺網.md "wikilink")作業時常是鯨豚與其他[海洋哺乳動物的常見死因之一](../Page/海洋哺乳動物.md "wikilink")\[5\]。鯨豚也常受到[海洋汙染的影響](../Page/海洋汙染.md "wikilink")，由於牠們屬於海洋[食物鏈的頂端](../Page/食物鏈.md "wikilink")，因此體內時常累積有高濃度的[持久性有機污染物](../Page/持久性有機污染物.md "wikilink")，且齒鯨體內的濃度多半高於鬚鯨。這些汙染物也會透過哺乳傳染給幼鯨。汙染物會導致並且降低對感染疾病的抵抗力\[6\]。除了有機汙染物外，鯨豚也會誤食塑膠袋等海洋垃圾\[7\]，這些垃圾無法經由消化道排出，進而造成胃腸的阻塞。而[長江的汙染最後導致了](../Page/長江.md "wikilink")[白鱀豚的](../Page/白鱀豚.md "wikilink")[功能性滅絕](../Page/功能性滅絕.md "wikilink")\[8\]。[環保主義者認為](../Page/環境保護主義.md "wikilink")，[聲納會干擾鯨魚活動](../Page/聲納.md "wikilink")，進而對牠們的存活造成威脅。有些科學家認為聲納會導致[鯨魚擱淺](../Page/鯨魚擱淺.md "wikilink")，這些鯨魚為了躲避聲納而快速上浮，導致[減壓症產生而發生擱淺](../Page/減壓症.md "wikilink")\[9\]\[10\]\[11\]\[12\]。

## 參考文獻

[\*](../Category/齒鯨亞目.md "wikilink")

1.

2.

3.
4.

5.

6.

7.

8.

9.

10.

11.

12.