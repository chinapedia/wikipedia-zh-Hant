[Lung_Fu_Shan_1.jpg](https://zh.wikipedia.org/wiki/File:Lung_Fu_Shan_1.jpg "fig:Lung_Fu_Shan_1.jpg")
**龍虎山郊野公園**（）（劃定於1998年12月18日）是位於[香港](../Page/香港.md "wikilink")[香港島西部的一個](../Page/香港島.md "wikilink")[郊野公園](../Page/郊野公園.md "wikilink")。它是[香港主權移交後第一個劃定的郊野公園](../Page/香港主權移交.md "wikilink")。該公園佔地約47[公頃](../Page/公頃.md "wikilink")，為香港[面積最小的郊野公園](../Page/面積.md "wikilink")（不計[特別地區](../Page/香港特別地區.md "wikilink")）。它的覆蓋範圍為[龍虎山和](../Page/龍虎山_\(香港\).md "wikilink")[夏力道以北的](../Page/夏力道.md "wikilink")[西高山山腰](../Page/西高山.md "wikilink")，園內主要為樹木茂密的山坡。因為鄰近[西環](../Page/西環.md "wikilink")（[西半山](../Page/西半山.md "wikilink")）[住宅區](../Page/住宅.md "wikilink")（[西營盤](../Page/西營盤.md "wikilink")、[石塘咀等地](../Page/石塘咀.md "wikilink")），交通方便，所以很受晨運客歡迎。它的東面以[克頓道為界](../Page/克頓道.md "wikilink")，南面為夏力道，而西面和北面則以一條由[水務署建造的](../Page/水務署.md "wikilink")[引水道為界](../Page/引水道.md "wikilink")（引水道上面築有一條稱為[碧珊徑的小徑](../Page/碧珊徑.md "wikilink")）。

2015年9月[香港大學生物科科學學院博士](../Page/香港大學.md "wikilink")[管納德領導的研究團隊在龍虎山郊野公園環境教育中心下](../Page/管納德.md "wikilink")10米的[石壆位置發現全球新蟻種](../Page/石壆.md "wikilink")，命名為「[紫荊花細腰家蟻](../Page/紫荊花細腰家蟻.md "wikilink")」(Paratopula
bauhinia)，又稱[金樹蟻](../Page/金樹蟻.md "wikilink")。

## 景點

### 龍虎山山頂

[Lung_Fu_Shan_3.jpg](https://zh.wikipedia.org/wiki/File:Lung_Fu_Shan_3.jpg "fig:Lung_Fu_Shan_3.jpg")
龍虎山山頂上建有十一號涼亭，可眺望[維多利亞港兩岸](../Page/維多利亞港.md "wikilink")。遊人如果是沿克頓道登山，在牌坊過後的分叉路轉右，並在以後的分叉路再轉右，即可找到登上山頂的梯級。

### 松林廢堡

松林廢堡建於1903年，本是一個軍事用地，但在[香港淪陷時受到破壞](../Page/香港保衛戰.md "wikilink")。因為它已被荒廢，而又位於[松林之間](../Page/松.md "wikilink")，故得名。

由於當地有大量廢棄的營房，所以吸引了很多喜歡[打野戰的人前來](../Page/战争游戏.md "wikilink")。因此[漁農自然護理署](../Page/漁農自然護理署.md "wikilink")（郊野公園及海岸公園管理局）在廢堡設立了警告牌，說明「任何人未經當局許可攜帶任何種類火器、氣槍及投彈器具或裝置進入此區，可被檢控」，以免該處的歷史遺跡再受更多破壞。雖然如此，當地仍隨地可見大量[塑膠的](../Page/塑膠.md "wikilink")[BB彈](../Page/BB彈.md "wikilink")。

<File:Lung> Fu Shan Pinewood Battery 1.jpg|松林廢堡的指示牌 <File:Lung> Fu Shan
Pinewood Battery 2.jpg|荒廢了的營房 <File:Lung> Fu Shan Pinewood Battery
3.jpg|長滿雜草的樓梯 <File:Lung> Fu Shan Pinewood Battery 4.jpg|禁止打野戰的警告牌

### 龍虎山觀景台

龍虎山觀景台位於西高山西北面的山腰，夏力道盡頭的龍虎山郊野公園郊遊區2號場內，建於[西高山機槍堡之上](../Page/西高山機槍堡.md "wikilink")。從觀景台，遊人可以盡覽香港島西面的海景，包括西面的維多利亞港和南面的[東](../Page/東博寮海峽.md "wikilink")[西博寮海峽](../Page/西博寮海峽.md "wikilink")。而可以看到的地方（由左至右）則包括[華富邨](../Page/華富邨.md "wikilink")、[南丫島](../Page/南丫島.md "wikilink")、[瑪麗醫院](../Page/瑪麗醫院.md "wikilink")、[坪洲](../Page/坪洲.md "wikilink")、[交椅洲](../Page/交椅洲.md "wikilink")、[愉景灣](../Page/愉景灣.md "wikilink")、[摩星嶺](../Page/摩星嶺.md "wikilink")、[青洲](../Page/青洲_\(中西區\).md "wikilink")、[馬灣](../Page/馬灣.md "wikilink")、[青衣](../Page/青衣島.md "wikilink")、[青衣大橋](../Page/青衣大橋.md "wikilink")、[葵青貨櫃碼頭](../Page/葵青貨櫃碼頭.md "wikilink")、[大帽山](../Page/大帽山.md "wikilink")、[昂船洲](../Page/昂船洲.md "wikilink")、[龍虎山](../Page/龍虎山_\(香港\).md "wikilink")、[筆架山](../Page/筆架山_\(香港\).md "wikilink")、[獅子山和](../Page/獅子山_\(香港\).md "wikilink")[飛鵝山](../Page/飛鵝山.md "wikilink")。

[Lung_Fu_Shan_View_Compass_1.jpg](https://zh.wikipedia.org/wiki/File:Lung_Fu_Shan_View_Compass_1.jpg "fig:Lung_Fu_Shan_View_Compass_1.jpg")
[Lung_Fu_Shan_View_Compass_2.jpg](https://zh.wikipedia.org/wiki/File:Lung_Fu_Shan_View_Compass_2.jpg "fig:Lung_Fu_Shan_View_Compass_2.jpg")

## 鄰近的郊野公園

  - [薄扶林郊野公園](../Page/薄扶林郊野公園.md "wikilink")

## 參見

  - [香港郊野公園](../Page/香港郊野公園.md "wikilink")

## 站外連結

  - [漁農自然護理署
    龍虎山](http://www.afcd.gov.hk/tc_chi/country/cou_vis/cou_vis_cou/cou_vis_cou_lfs/cou_vis_cou_lfs.html)
  - [龍虎山劃定為第二十三個郊野公園](http://www.info.gov.hk/gia/general/199812/16/1215098.htm)
  - [陳尚權情繫
    龍虎山旅遊](http://www.cnwnc.com/content/2010-06/24/content_2544186.htm)
  - [龍虎山郊野公園晨運之友會](https://sites.google.com/site/lungfushanhongkong/)

[Category:香港郊野公園](../Category/香港郊野公園.md "wikilink") [Category:龍虎山
(香港)](../Category/龍虎山_\(香港\).md "wikilink")
[Category:西高山](../Category/西高山.md "wikilink")