是1987年[香港動作與警匪片](../Page/香港.md "wikilink")，由[林嶺東執導](../Page/林嶺東.md "wikilink")，[周潤發主演](../Page/周潤發.md "wikilink")。

## 劇情大要

高秋（[周潤發飾](../Page/周潤發.md "wikilink")）為警方[卧底](../Page/卧底.md "wikilink")，對要經常需要出賣身旁的人厭惡而準備辭職，打算與女友（[吳家麗飾](../Page/吳家麗.md "wikilink")）結婚。劉督察（[孫越飾](../Page/孫越.md "wikilink")）為高之上司，其卧底在調查一班珠寶劫匪時被殺，於是勸高秋為其作最後一次卧底，但高秋只答應幹到賣槍給劫匪為止。後來高秋成功滲透阿虎（[李修賢飾](../Page/李修賢.md "wikilink")）的匪幫，但因是次卧底之事，與女友再交惡，終致女友移情別戀。因為劉督察與隊中對頭John陳（[張耀揚飾](../Page/張耀揚.md "wikilink")）長期不和，而John陳急於立功爭取升職，引致高秋不但無法幹到交槍為止，還要幹到與匪幫一同行動。最後高秋在警匪槍戰中中彈而死，死前向阿虎透露他本身是卧底。

## 演員

|                                       |                                                   |
| ------------------------------------- | ------------------------------------------------- |
| **演員**                                | **角色**                                            |
| [周潤發](../Page/周潤發.md "wikilink")      | 高秋                                                |
| [李修賢](../Page/李修賢.md "wikilink")      | 阿虎                                                |
| [孫越](../Page/孫越_\(演員\).md "wikilink") | 劉督察 (粵語配音：[張炳強](../Page/張炳強.md "wikilink"))       |
| [吳家麗](../Page/吳家麗.md "wikilink")      | 阿紅 (粵語配音：[龍寶鈿](../Page/龍寶鈿.md "wikilink"))        |
| [張耀揚](../Page/張耀揚.md "wikilink")      | John陳 (粵語配音：[梁政平](../Page/梁政平.md "wikilink"))     |
| [劉江](../Page/劉江_\(香港\).md "wikilink") | 周警司                                               |
| [黃栢文](../Page/黃栢文.md "wikilink")      | 劉督察下屬 (粵語配音：[黃志成](../Page/黃志成.md "wikilink"))     |
| [黃光亮](../Page/黃光亮.md "wikilink")      | John陳下屬                                           |
| [方野](../Page/方野.md "wikilink")        | 南哥 (粵語配音：[黃志成](../Page/黃志成.md "wikilink"))        |
| [朱繼生](../Page/朱繼生.md "wikilink")      | 阿Joe                                              |
| 陳志輝                                   | 大喪                                                |
| [韓坤](../Page/韓坤.md "wikilink")        | 阿標                                                |
| [沈西城](../Page/沈西城.md "wikilink")      | 排骨                                                |
| [徐錦江](../Page/徐錦江.md "wikilink")      | 爛命華 (粵語配音：[陳欣](../Page/陳欣_\(配音員\).md "wikilink")) |

## 其他

[落水狗](../Page/落水狗.md "wikilink")

## 外部連結

  -
  -
  - {{@movies|fXcmb4043249}}

  -
  -
  -
  -
  -
  -
[7](../Category/1980年代香港電影作品.md "wikilink")
[Category:香港電影金像獎最佳導演獲獎電影](../Category/香港電影金像獎最佳導演獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳男主角獲獎電影](../Category/香港電影金像獎最佳男主角獲獎電影.md "wikilink")
[Category:林嶺東電影](../Category/林嶺東電影.md "wikilink")
[Category:香港警匪片](../Category/香港警匪片.md "wikilink")
[Category:粤语電影](../Category/粤语電影.md "wikilink")
[Category:新藝城電影](../Category/新藝城電影.md "wikilink")
[Category:卧底题材电影](../Category/卧底题材电影.md "wikilink")