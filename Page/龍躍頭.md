[LungYeukTau_LoWai_Outside.jpg](https://zh.wikipedia.org/wiki/File:LungYeukTau_LoWai_Outside.jpg "fig:LungYeukTau_LoWai_Outside.jpg")
[Lung_Yeuk_Tau_201701.jpg](https://zh.wikipedia.org/wiki/File:Lung_Yeuk_Tau_201701.jpg "fig:Lung_Yeuk_Tau_201701.jpg")，梧桐河左側爲小坑村，右側爲新圍、覲龍村和新屋村，其下方大片農田爲[馬屎埔](../Page/馬屎埔.md "wikilink")。\]\]
**龍躍頭**（），位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[粉嶺](../Page/粉嶺.md "wikilink")[聯和墟以北](../Page/聯和墟.md "wikilink")，[龍山山腳](../Page/龍山_\(粉嶺\).md "wikilink")，[麻笏河以東](../Page/麻笏河.md "wikilink")。俗名**龍骨頭**、**龍屈頭**或**龍嶺**，該地有山名龍躍嶺，相傳有龍跳躍其間而得名。

現今可考龍躍頭的最早居民爲宋代末年從[潮州](../Page/潮州.md "wikilink")[揭陽縣遷居而來的](../Page/揭陽.md "wikilink")[彭氏一族](../Page/粉嶺圍.md "wikilink")，惟彭氏在龍躍頭活動的歷史記錄甚少，而彭氏在該段時間的人丁相對單薄。[元朝末年](../Page/元朝.md "wikilink")，原籍[江西](../Page/江西.md "wikilink")[吉水的鄧氏鄧季琇](../Page/吉水.md "wikilink")（外號松嶺）遷移到龍躍頭，原龍躍頭彭氏出走至西南方的[粉嶺圍定居](../Page/粉嶺圍.md "wikilink")，形成今天的「粉嶺彭氏」。自鄧季琇於龍躍頭開村（即老圍）立業，龍躍頭鄧氏至今已歷800多年，因-{子}-孫繁衍而在附近另立村莊，形成現時的五圍六村，成爲[新界五大家族之一](../Page/新界五大家族.md "wikilink")[鄧氏的其中一個聚居處](../Page/邓姓.md "wikilink")，據說龍躍頭鄧氏在歷史中某些時段的勢力更曾超越比其更早來到新界定居[錦田](../Page/錦田.md "wikilink")、[廈村一帶的鄧氏族人](../Page/廈村.md "wikilink")。

## 五圍六村

龍躍頭的五圍是指[老圍](../Page/老圍_\(龍躍頭\).md "wikilink")、[麻笏圍](../Page/麻笏圍.md "wikilink")、[永寧圍](../Page/永寧圍.md "wikilink")、[東閣圍](../Page/東閣圍.md "wikilink")（或稱嶺角圍）和[新圍](../Page/新圍_\(龍躍頭\).md "wikilink")（或稱覲龍圍），六村則是[麻笏村](../Page/麻笏圍#麻笏村.md "wikilink")、[永寧村](../Page/永寧村.md "wikilink")（或稱大廳）、[祠堂村](../Page/祠堂村_\(龍躍頭\).md "wikilink")、[新屋村](../Page/新屋村_\(龍躍頭\).md "wikilink")、[小坑村和](../Page/小坑村.md "wikilink")[覲龍村](../Page/新圍_\(龍躍頭\)#覲龍村.md "wikilink")，五圍六村給[沙頭角公路從中分隔](../Page/沙頭角公路.md "wikilink")。

1999年12月4日[龍躍頭文物徑正式開放](../Page/龍躍頭文物徑.md "wikilink")，為[香港第二條](../Page/香港.md "wikilink")[文物徑](../Page/文物徑.md "wikilink")。部份文物包括[-{粉嶺天后宮}-](../Page/粉嶺天后宮.md "wikilink")、[松嶺鄧公祠](../Page/松嶺鄧公祠.md "wikilink")、[老圍門樓及圍牆等](../Page/老圍_\(龍躍頭\).md "wikilink")。

## 龍躍頭鄧氏

[新界鄧氏是中原](../Page/新界.md "wikilink")[士大夫之後](../Page/士大夫.md "wikilink")，在[西晉](../Page/晉朝.md "wikilink")[永嘉年間因](../Page/永嘉.md "wikilink")[五胡亂華而南遷](../Page/五胡亂華.md "wikilink")，次第經[江西](../Page/江西.md "wikilink")[吉水](../Page/吉水.md "wikilink")、[廣東](../Page/廣東.md "wikilink")[南雄](../Page/南雄.md "wikilink")、[東莞等地移居到](../Page/東莞.md "wikilink")[寶安](../Page/寶安.md "wikilink")。

寶安鄧氏始祖[鄧符協於](../Page/鄧符協.md "wikilink")[宋徽宗](../Page/宋徽宗.md "wikilink")[崇寧四年](../Page/崇寧.md "wikilink")（1105年）進士及第，獲授[陽春](../Page/陽春.md "wikilink")[縣令](../Page/縣令.md "wikilink")，官至[南雄倅](../Page/南雄.md "wikilink")。卸任後舉家落籍寶安[岑田退隱田園](../Page/錦田.md "wikilink")，建立「力瀛齋書院」，教化一方。再傳四代分為五大房：鄧元英、鄧元禧、鄧元禎、鄧元亮及鄧元和，在東莞興建宗祠「都慶堂」，設置祠產。隨年月推進，各房子孫開枝散葉，分散到東莞、[茂名](../Page/茂名.md "wikilink")，甚至廣西一帶。鄧元禎及鄧元亮兩房人丁興旺，子孫大部分留駐寶安（後曾名為[新安縣](../Page/新安縣.md "wikilink")），鄧元禎一系聚居在[元朗](../Page/元朗.md "wikilink")[屏山一帶](../Page/屏山_\(香港\).md "wikilink")。而鄧元亮一系除第二房（鄧杞）遷居東莞[石井外](../Page/石井.md "wikilink")，其餘各房分佈於新界的龍躍頭、錦田、[廈村](../Page/廈村.md "wikilink")、[大埔頭](../Page/大埔頭.md "wikilink")、[萊洞等地](../Page/萊洞.md "wikilink")。

[鄧元亮號銑](../Page/鄧元亮.md "wikilink")，[正六品承德郎](../Page/正六品.md "wikilink")，曾任江西[贛縣縣令](../Page/贛縣.md "wikilink")，相傳在[南宋](../Page/南宋.md "wikilink")[靖康](../Page/靖康.md "wikilink")[建炎間提兵](../Page/建炎.md "wikilink")[勤王](../Page/勤王.md "wikilink")，在亂軍中救出南逃的「幼宗姬」，後將她許配給兒子[鄧自明](../Page/鄧自明.md "wikilink")（號惟汲），原來此女是[宋高宗之女](../Page/宋高宗.md "wikilink")、[宋孝宗之姊](../Page/宋孝宗.md "wikilink")。[宋光宗即位後](../Page/宋光宗.md "wikilink")，終尋獲其下落，以「皇姑」相稱，追封鄧自明為「稅院郡馬」，並賜地於東莞。皇姑生有四子：鄧林、鄧杞、鄧槐、鄧梓，長子鄧林獲授「迪功郎」，其餘三人為「舍人」。鄧林和鄧杞隨母遷居東莞[莫家洞](../Page/莫家洞.md "wikilink")，鄧槐和鄧梓留守寶安。皇姑終年87歲，卜葬於東莞石井獅子嶺。

南宋覆亡，[元朝統一天下](../Page/元朝.md "wikilink")，對宋朝遺臣加以迫害，以皇室自居的鄧氏首當其衝，長房鄧林之曾孫[鄧季琇](../Page/鄧季琇.md "wikilink")（號松嶺），乃四兄弟中的老么，為人耿直，遭迫害變得一無所有，隻身離開[東莞](../Page/東莞.md "wikilink")，潛逃到龍躍頭圖謀發展。鄧季琇胼手胝足的開墾，開闢了百多[畝良田](../Page/畝.md "wikilink")，更將[彭氏逐離龍躍頭](../Page/粉嶺圍.md "wikilink")。鄧季琇中年喪妻後，直到60歲才在龍躍頭續弦再娶，晚年得兩子，只有鄧實安一子存活。[明朝](../Page/明朝.md "wikilink")[洪武年間](../Page/洪武.md "wikilink")，國是初定，在全國進行丈量土地以明徵錢糧，鄧季琇被東莞伯[何真誘騙](../Page/何真.md "wikilink")，將名下的土地用何真的名義申報以獲取豁免賦徭，當朝廷核實地權時，何真將全部土地侵吞。其後何真因一宗叛逆案被株連，鄧氏才得以收回原有的土地，但鄧季琇早已作古了。

**龍躍頭鄧氏源流圖**︰

## 區議會議席分佈

由於龍躍頭人口不足以成為一個區議會選區，所以往往跟鄰近的[軍地](../Page/軍地.md "wikilink")、[孔嶺等鄉村範圍劃為同一個選區](../Page/孔嶺.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>整個龍躍頭</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [龍躍頭文物徑](../Page/龍躍頭文物徑.md "wikilink")

## 參考資料

  - 《粉嶺》香港地區史研究之三，陳國成 著，三聯書店，ISBN 962-04-2527-8
  - 《新界宗族文化之旅》，嚴瑞源 編著，萬里機構，ISBN 962-14-2936-6
  - 《新界五大家族》，蕭國健 著，現代教育研究社，ISBN 962-11-1996-0
  - 《龍躍頭文物徑》，（古物古蹟辦事處）

## 外部連結

  - [香港地方：新界村落(二)龍躍頭](http://www.hk-place.com/view.php?id=128)

[龍躍頭](../Page/category:龍躍頭.md "wikilink") [category:北區
(香港)](../Page/category:北區_\(香港\).md "wikilink")
[category:粉嶺](../Page/category:粉嶺.md "wikilink")