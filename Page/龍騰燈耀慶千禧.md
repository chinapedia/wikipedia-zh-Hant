**龍騰燈耀慶千禧**是[香港政府於](../Page/香港特別行政區政府.md "wikilink")1999年12月31日[跨年夜晚上至](../Page/跨年夜.md "wikilink")2000年1月1日凌晨舉辦，慶祝[香港進入](../Page/香港.md "wikilink")2000年（[千禧年](../Page/千禧年.md "wikilink")）的大型[跨年慶典活動](../Page/跨年.md "wikilink")，於[跑馬地馬場中央草坪舉行](../Page/跑馬地馬場.md "wikilink")。節目由[香港電台製作](../Page/香港電台.md "wikilink")，[香港賽馬會協辦](../Page/香港賽馬會.md "wikilink")。[無綫](../Page/電視廣播有限公司.md "wikilink")[翡翠台當晚在跨世紀香港同心齊共創](../Page/翡翠台.md "wikilink")，
怡和午炮 荃灣沙咀道運動場及 龍發製藥香港輝黃2000演唱會現場直播。

節目參與人數超過一萬人，由當時的[香港特區行政長官](../Page/香港特區行政長官.md "wikilink")[董建華主禮](../Page/董建華.md "wikilink")，活動收入用作[慈善用途](../Page/慈善.md "wikilink")。節目於[香港時間晚上](../Page/香港時間.md "wikilink")7時開始，在場內有多項活動進行，包括[花燈展覽](../Page/花燈.md "wikilink")、[舞龍表演](../Page/舞龍.md "wikilink")、藝人演出、[賽馬及](../Page/賽馬.md "wikilink")[煙花匯演等](../Page/煙花.md "wikilink")。

## 表演項目

政府於12月31日晚上於快活谷馬場舉行音樂會，由多位藝人參與演出，晚上10時20分，由[成龍騎馬](../Page/成龍.md "wikilink")，帶領香港演藝界及體育界人士於[草地跑道上進行開幕巡遊](../Page/草.md "wikilink")，與[劉德華合作表演](../Page/劉德華.md "wikilink")《勁歌熱舞迎千禧》，及由當時的[政務司司長](../Page/政務司司長.md "wikilink")[陳方安生主持亮燈儀式](../Page/陳方安生.md "wikilink")。其後由歌手合唱節目主題曲《千禧盛世》，在凌晨12時前進行倒數；於12時正，千禧年來臨時舉行煙花匯演。香港電台則於當晚頒發第22屆[十大中文金曲之十優歌手獎項](../Page/十大中文金曲.md "wikilink")。

當晚香港怡和午炮、沙咀道運動場、[紅館](../Page/紅館.md "wikilink")、均有倒數迎接[千禧年](../Page/公元2000年.md "wikilink")。

### 表演嘉賓

  - [成龍](../Page/成龍.md "wikilink")-當晚亦出席「[龍發製藥香港輝黄](../Page/龍發製藥.md "wikilink")2000演唱會」擔任嘉賓
  - [洪金寶](../Page/洪金寶.md "wikilink")
  - [譚詠麟](../Page/譚詠麟.md "wikilink")
  - [張國榮](../Page/張國榮.md "wikilink")
  - [梅艷芳](../Page/梅艷芳.md "wikilink")
  - [張學友](../Page/張學友.md "wikilink")
  - [劉德華](../Page/劉德華.md "wikilink")
  - [黎明](../Page/黎明.md "wikilink")
  - [郭富城](../Page/郭富城.md "wikilink")
  - [王菲](../Page/王菲.md "wikilink")
  - [鄭秀文](../Page/鄭秀文.md "wikilink")
  - [陳慧琳](../Page/陳慧琳.md "wikilink")
  - [陳慧嫻](../Page/陳慧嫻.md "wikilink")
  - [趙薇](../Page/趙薇.md "wikilink")
  - [周潤發](../Page/周潤發.md "wikilink")

## 花燈會

特區政府在馬場中央舉行大型燈會，展出多個扎作彩燈。當中名為騰龍的[龍型彩燈](../Page/龍.md "wikilink")，全長280米、龍頭高度14米，佔地面積4800平方米，被列入《[健力氏世界紀錄大全](../Page/健力氏世界紀錄大全.md "wikilink")》成為世界最大的龍型彩燈。

## 賽馬

當晚在快活谷馬場舉行賽馬，特別設立一場千禧盃賽事，由第1、2班馬跑1800米，於1月1日0時45分開跑，成為全世界在2000年舉行的第一場賽馬，結果由[簡炳墀訓練](../Page/簡炳墀.md "wikilink")，[昆霑誠策騎的](../Page/昆霑誠.md "wikilink")「歡騰」（英文馬名：FLORAL
JOY，馬主：[陳昌利](../Page/陳昌利.md "wikilink")、[陳昌龍與](../Page/陳昌龍.md "wikilink")[陳昌育](../Page/陳昌育.md "wikilink")，以上三個皆為陳喜鎮的兒子）以4又4份1馬位勝出，賽後由行政長官[董建華主持頒獎儀式](../Page/董建華.md "wikilink")。

## 預防措施

香港政府為減低[電腦](../Page/電腦.md "wikilink")[2000年問題可能造成的影響](../Page/2000年問題.md "wikilink")，於踏入2000年的過渡期間成立政府中央協調中心，負責處理可能出現的突發事件。政府亦把[香港島市區部份道路封閉](../Page/香港島.md "wikilink")，及增加駐守警員，以疏散人群及減低意外發生的可能，幸好當晚並無重大事故發生。

活動舉行當日，香港賽馬會亦特別容許18歲以下的兒童進入快活谷馬場，及首次容許入場人士使用[手提電話](../Page/手提電話.md "wikilink")。

## 資料來源

  - [香港年報1999](http://www.yearbook.gov.hk/1999/b5/calendar/dec.htm)
  - [政務司司長龍騰快車迎千禧典禮致辭全文](http://www.info.gov.hk/gia/general/199911/12/1112144.htm).香港政府新聞網.1999-11-12
  - [《龍騰燈耀慶千禧》登記換購入場券](http://www.info.gov.hk/gia/general/199910/22/1022271.htm).香港政府新聞網.1999-10-22
  - [除夕晚「龍騰燈耀慶千禧」節目程序](https://web.archive.org/web/20070928010749/http://www.alan-tam.com/oldnews/1999/dec99/news13dec99-1.htm).大公報.1999-12-11
  - [龍騰燈耀慶千禧](https://web.archive.org/web/20070616103138/http://www.fsfolkart.com/newsshow.asp?newsid=81).佛山民間藝術研究社.2005-01-17

[Category:香港節慶活動](../Category/香港節慶活動.md "wikilink")
[Category:香港賽馬](../Category/香港賽馬.md "wikilink")
[Category:1999年香港](../Category/1999年香港.md "wikilink")
[Category:2000年香港](../Category/2000年香港.md "wikilink")