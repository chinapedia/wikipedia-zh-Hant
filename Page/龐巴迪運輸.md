**庞巴迪运输**（）是[加拿大](../Page/加拿大.md "wikilink")[庞巴迪集团的轨道设备](../Page/庞巴迪公司.md "wikilink")[子公司](../Page/子公司.md "wikilink")，为全球轨道设备及服务行业中规模最大的公司之一，其总部设于[德国](../Page/德国.md "wikilink")[柏林](../Page/柏林.md "wikilink")\[1\]。庞巴迪运输生产多种产品，包括客运[铁路车辆](../Page/铁路车辆.md "wikilink")、[机车](../Page/铁路机车.md "wikilink")、[转向架](../Page/转向架.md "wikilink")、牵引及控制系统，此外也提供大量服务解决方案。

公司现任[总裁为卢茨](../Page/总裁.md "wikilink")·贝尔特林（）\[2\]。截至2011年1月，公司共有雇员34,900人，其中25,400人在[欧洲工作](../Page/欧洲.md "wikilink")，并且在[全球各地设有](../Page/全球.md "wikilink")59间工厂\[3\]。

## 历史

[Bombardier_in_Hennigsdorf.jpg](https://zh.wikipedia.org/wiki/File:Bombardier_in_Hennigsdorf.jpg "fig:Bombardier_in_Hennigsdorf.jpg")工厂大楼\]\]
[160_Jahre_Waggonbau_in_Bautzen.jpg](https://zh.wikipedia.org/wiki/File:160_Jahre_Waggonbau_in_Bautzen.jpg "fig:160_Jahre_Waggonbau_in_Bautzen.jpg")工厂装配车间\]\]
[London_Underground_2009_Stock_front.jpg](https://zh.wikipedia.org/wiki/File:London_Underground_2009_Stock_front.jpg "fig:London_Underground_2009_Stock_front.jpg")[2009年型列車](../Page/倫敦地鐵2009年型列車.md "wikilink")\]\]
庞巴迪运输成立于1974年，其首个[铁路机车车辆的订单来自](../Page/铁路机车车辆.md "wikilink")[蒙特利尔交通局](../Page/蒙特利尔交通局.md "wikilink")，获后者授权为[蒙特利尔地铁生产](../Page/蒙特利尔地铁.md "wikilink")423辆[地铁车厢](../Page/地铁.md "wikilink")\[4\]。公司原有的核心业务的是[庞巴迪集团在](../Page/庞巴迪公司.md "wikilink")1975年收购的，并作为[机车事业部一直运营至](../Page/铁路机车.md "wikilink")1985年，期间还生产出了[摆式列车](../Page/摆式列车.md "wikilink")。1987年，庞巴迪又先后收购了[巴德公司和](../Page/巴德公司.md "wikilink")的部分资产。蒙特利尔机车厂后于1988年售予[通用电气](../Page/通用电气.md "wikilink")，庞巴迪运输则继续在[桑德贝经营轨道车辆业务](../Page/桑德贝.md "wikilink")。

在1980年代后期，庞巴迪运输又通过收购\[5\]（主要生产场地在[比利时](../Page/比利时.md "wikilink")[布鲁日](../Page/布鲁日.md "wikilink")）45%的股权份额以及收购（主要在[法国](../Page/法国.md "wikilink")[克雷斯潘](../Page/克雷斯潘.md "wikilink")）而获得了在[欧洲的生产业务](../Page/欧洲.md "wikilink")\[6\]。至1990年，主要负责生产车身外壳的、位于英国的也被庞巴迪收购，并更名为庞巴迪普罗铁路（）\[7\]。

1991年，庞巴迪在欧洲的子公司被重组为庞巴迪欧洲铁路（），其包括了布鲁日及尼韦尔铁路车辆制造厂、ANF工业公司、普罗铁路和\[8\]\[9\]。1992年，该公司又从[墨西哥政府手中收购了墨西哥最大的铁路装备制造商](../Page/墨西哥.md "wikilink")\[10\]。

在[德国](../Page/德国.md "wikilink")，庞巴迪分别于1995年收购了[亚琛的](../Page/亚琛.md "wikilink")和于1998年收购了，并接管了其设于[柏林](../Page/柏林.md "wikilink")、[莱比锡](../Page/莱比锡.md "wikilink")、[格尔利茨](../Page/格尔利茨.md "wikilink")、[包岑和](../Page/包岑.md "wikilink")[尼斯基的厂房](../Page/尼斯基.md "wikilink")。由于德国铁路车辆制造股份公司此前在1997年便已收购了设于[瑞士](../Page/瑞士.md "wikilink")[沃韦的](../Page/沃韦.md "wikilink")[沃韦技术工程公司](../Page/沃韦技术工程公司.md "wikilink")，因此后者也同样加入了庞巴迪\[11\]。

2001年，庞巴迪运输从[戴姆勒克莱斯勒收购了总部设于柏林北部的](../Page/戴姆勒克莱斯勒.md "wikilink")[Adtranz及其规模最大的](../Page/Adtranz.md "wikilink")，同时Adtranz设于[曼海姆](../Page/曼海姆.md "wikilink")、[卡塞尔](../Page/卡塞尔.md "wikilink")、[纽伦堡](../Page/纽伦堡.md "wikilink")、[锡根和](../Page/锡根.md "wikilink")[布伦瑞克的其它厂房也都并入庞巴迪运输](../Page/布伦瑞克.md "wikilink")，后者从而被许多西方媒体评估为世界最大的轨道装备制造商\[12\]。此项收购在得到欧盟竞争委员会的批准时收到了多项细微条款的约束，包括庞巴迪需要将持有的原Adtranz及合资的施泰德潘科有限公司（）股权进行分拆出售（售予施泰德），并保留[基普作为供应商](../Page/基普.md "wikilink")，以及维持作为收购后长期的合作伙伴关系\[13\]。Adtranz增加了庞巴迪在[铁路客车](../Page/铁路客车.md "wikilink")、[动车组以及](../Page/动车组.md "wikilink")[有轨电车的生产线](../Page/有轨电车.md "wikilink")，许多获得成功的Adtranz车辆从而成为庞巴迪的产品，并据此开发出了[庞巴迪TRAXX等新平台](../Page/庞巴迪TRAXX.md "wikilink")。通过此项收购，庞巴迪还得以加强了在其电力传动部件业务的竞争力。

在完成对Adtranz的收购后，庞巴迪运输于2001年发布了其在欧洲的核心制造策略：分别设于德国锡根、[英国](../Page/英国.md "wikilink")[德比和法国克雷斯潘的三个工厂主要负责生产](../Page/德比.md "wikilink")[转向架](../Page/转向架.md "wikilink")。车身制造需要在德国的包岑及格尔利茨、[瑞典的](../Page/瑞典.md "wikilink")[卡尔马](../Page/卡尔马.md "wikilink")、英国的和比利时的布鲁日进行。对于最后的总装，该公司则选择了在德国的亚琛和[黑尼格斯多夫](../Page/黑尼格斯多夫.md "wikilink")、[葡萄牙的](../Page/葡萄牙.md "wikilink")[阿马多拉](../Page/阿马多拉.md "wikilink")、以及分别设于英国德比、法国克雷斯潘、比利时布鲁日、瑞典卡尔马和瑞士[普拉特恩的工厂完成](../Page/普拉特恩.md "wikilink")。另外一些工厂则担当特殊的生产角色，例如[捷克的](../Page/捷克.md "wikilink")[捷克利帕和](../Page/捷克利帕.md "wikilink")[波兰的原](../Page/波兰.md "wikilink")将供应零部件及焊接结构，[奥地利](../Page/奥地利.md "wikilink")[维也纳和德国包岑的工厂专注于生产](../Page/维也纳.md "wikilink")[輕軌運輸车辆](../Page/輕軌運輸.md "wikilink")，而运用于德国市场的双层列车则在格尔利茨生产\[14\]\[15\]。

由于经济衰退及欧洲客运车辆的产能过剩，庞巴迪在2004年3月公布了重组计划：将有5个国家的7个生产基地被关闭，它们包括在英国德比的转向架工厂、庞巴迪普罗铁路以及[唐卡斯特的维护设施](../Page/唐卡斯特.md "wikilink")；在[欧洲大陆的普拉特恩](../Page/欧洲大陆.md "wikilink")、阿马多拉和卡尔马\[16\]。而位于德国东部的和费茨肖则已早在2001年关闭\[17\]\[18\]。同时，公司还将在全球范围内削减6,600个职位（其中86%在欧洲），其在庞巴迪运输的员工比例中达到18.5%。

在完成重组后，德国以8个工厂和约8,600名员工继续成为庞巴迪运输最大的生产基地，其次是英国有6家工厂和近4,300名员工。瑞士则保留了不到760名员工分布在[維勒訥沃及苏黎世工厂](../Page/維勒訥沃_\(佛立堡邦\).md "wikilink")。

2010年5月12日，庞巴迪运输从[瑞士联邦铁路获得了公司历史上最昂贵的合同订单](../Page/瑞士联邦铁路.md "wikilink")，其将为后者提供59列用于的双层列车。该订单的总价值约为19亿[瑞士法郎](../Page/瑞士法郎.md "wikilink")。庞巴迪运输声称新列车将在瑞士的維勒訥沃和德国的格尔利茨工厂生产\[19\]。

2012年10月，庞巴迪运输宣布在主要订单完成后于2013年夏天关闭亚琛工厂，并削减600个职位。这是由于该厂没有后续订单作为支持。尽管受到了不同群体的干预，但庞巴迪运输还是如期关闭了该厂\[20\]。

## 產品

[台北捷運INNOVIA APM 256型電聯車](../Page/龐巴迪INNOVIA_APM_256型電聯車.md "wikilink")

## 参考资料

### 参考书目

  -
## 外部連結

  - [龐巴迪運輸](http://transportation.bombardier.com)

[Category:1974年成立的公司](../Category/1974年成立的公司.md "wikilink")
[Category:龐巴迪集團](../Category/龐巴迪集團.md "wikilink")
[Category:德國鐵路車輛製造商](../Category/德國鐵路車輛製造商.md "wikilink")

1.  [Bombardier Transportation
    Headquarters](http://www.2findlocal.com/b/9164696)

2.

3.

4.  [JRTR No.42 (Dec. 2005)](../Page/#jrtr.md "wikilink")

5.
6.
7.

8.

9.

10.

11.

12. [Bombardier Transportation:
    History](http://www.bombardier.com/en/transportation/about-transportation/history?docID=0901260d8001dffa).

13.

14.

15.

16.

17.
18.
19. [*Bombardier baut die
    SBB-Doppelstockzüge*.](http://www.nzz.ch/nachrichten/panorama/bombardier_baut_die_sbb-doppelstockzuege_1.5703193.html)
    In: *Neue Zürcher Zeitung*, 12. Mai 2010

20. [WDR.de:Bombardier hält an Werksschließung in Aachen
    fest](http://www1.wdr.de/themen/infokompakt/nachrichten/nrwkompakt/nrwkompakt11248.html)
    abgerufen am 13. November 2012