**龐迪我**（**Diego de
Pantoja**，），字**順陽**，[耶穌會](../Page/耶穌會.md "wikilink")[西班牙籍教士](../Page/西班牙.md "wikilink")。[利玛窦的](../Page/利玛窦.md "wikilink")[同工](../Page/同工.md "wikilink")。

## 生平

龐迪我1571年生於西班牙。1589年加入[耶穌會](../Page/耶穌會.md "wikilink")，1596年6月前往[印度](../Page/印度.md "wikilink")[果阿](../Page/果阿.md "wikilink")，該年10月抵達。1597年4月，與范禮安、龍華民、陽瑪諾等人從果阿出發，該年7月抵達澳門。[万历二十七年](../Page/万历.md "wikilink")（1599年）受[范禮安神父派遣前往](../Page/范禮安.md "wikilink")[南京](../Page/南京.md "wikilink")，1600年隨[利瑪竇到達](../Page/利瑪竇.md "wikilink")[北京](../Page/北京.md "wikilink")，對利瑪竇的幫助甚大。史載：「龐迪者(我)、熊三拔等，攜有彼國曆法，多中國典籍所未備者」，可知其淵博知識\[1\]。利瑪竇死後，龐迪我與[熊三拔同奏請旨](../Page/熊三拔.md "wikilink")，讓利瑪竇安葬于北京城外。

在1602年，龐迪我给[西班牙](../Page/西班牙.md "wikilink")[托莱多總教區總主教](../Page/天主教托莱多總教區.md "wikilink")[路易士·德古斯曼写了一封长信](../Page/路易士·德古斯曼.md "wikilink")，介绍了一些中国情况，并纠正了[门多萨](../Page/胡安·岡薩雷斯·德·門多薩.md "wikilink")《[大中华帝国史](../Page/中华大帝国史.md "wikilink")》一书中的一些错误。1611年，龐迪我與熊三拔奉旨修曆，但1616年發生[南京教案](../Page/南京教案.md "wikilink")，龐迪我等辯解無效，被遣逐回[澳門](../Page/澳門.md "wikilink")，不久即病死。

龐迪我善[音樂](../Page/音樂.md "wikilink")，並曾為中國皇帝[明神宗繪有](../Page/明神宗.md "wikilink")[四大洲](../Page/洲.md "wikilink")[地圖](../Page/地圖.md "wikilink")，每圖周圍並附有解說，略述各地[歷史](../Page/歷史.md "wikilink")、[地理](../Page/地理.md "wikilink")、[政治和](../Page/政治.md "wikilink")[產物](../Page/產物.md "wikilink")。

## 著作

  - 《[七克](../Page/七克.md "wikilink")》
  - 《人類原始》（[上](https://web.archive.org/web/20060206071617/http://archives.catholic.org.hk/books/cyl2/index.htm)，[下](https://web.archive.org/web/20060206100146/http://archives.catholic.org.hk/books/cyl.12/index.htm)）
  - 《龐子遺銓》
  - 《實義續編》
  - 《天神魔鬼說》
  - 《受難始末》
  - 《辨揭》

## 參考來源

[category:在华天主教传教士](../Page/category:在华天主教传教士.md "wikilink")

[Category:西班牙天主教徒](../Category/西班牙天主教徒.md "wikilink")
[Category:耶穌會](../Category/耶穌會.md "wikilink")
[Category:西班牙音樂家](../Category/西班牙音樂家.md "wikilink")
[Category:澳門中外交流史](../Category/澳門中外交流史.md "wikilink")
[Category:西班牙天文学家](../Category/西班牙天文学家.md "wikilink")
[Category:西班牙地图制作者](../Category/西班牙地图制作者.md "wikilink")

1.  《明史/志/卷三十一志第七曆一/曆法沿革》：「大西洋歸化遠臣龐迪者、熊三拔等，攜有彼國曆法，多中國典籍所未備者」