**龙华寺**（）是座落于[中国](../Page/中华人民共和国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[徐匯區的一座](../Page/徐匯區.md "wikilink")[佛教](../Page/佛教.md "wikilink")[寺庙](../Page/寺庙.md "wikilink")。历经战乱，重建修复，今龙华寺的建筑建于[清朝](../Page/清朝.md "wikilink")[光绪年间](../Page/光绪.md "wikilink")。[上海四大佛寺之一](../Page/上海四大佛寺.md "wikilink")。

## 龙华寺的历史

始建于[三国](../Page/三国.md "wikilink")[吴大帝](../Page/吴大帝.md "wikilink")[赤乌年间](../Page/赤乌.md "wikilink")，为孝敬其母而建造，[唐时被毁](../Page/唐朝.md "wikilink")，[北宋初](../Page/北宋.md "wikilink")[吴越王重建](../Page/吴越国.md "wikilink")。北宋[治平元年改名空相寺](../Page/治平_\(北宋\).md "wikilink")。在[元末](../Page/元朝.md "wikilink")[明初寺院再次被摧毁](../Page/明朝.md "wikilink")。明朝[永乐年间重建](../Page/永乐.md "wikilink")，称龙华寺；[嘉靖三十二年赐名](../Page/嘉靖.md "wikilink")**万寿慈华禅寺**；[万历年间赐](../Page/万历.md "wikilink")[毗卢遮那佛像](../Page/毗卢遮那佛.md "wikilink")、[金印等](../Page/金印.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[顺治年间](../Page/顺治.md "wikilink")，[临济宗第三十二世](../Page/临济宗.md "wikilink")[韬明](../Page/韬明.md "wikilink")[禅师任寺院住持](../Page/禅师.md "wikilink")，清朝[咸丰](../Page/咸丰_\(年号\).md "wikilink")、[同治年间又遭兵焚](../Page/同治.md "wikilink")，经过僧俗的募捐修复，从清朝光绪元年至二十五年间修复寺院。

## 龙华寺的布局

### 龙华塔

龙华塔位于现今的龙华寺外，为八角砖木塔。总高七层，每层八面。相传始建于[东吴时期](../Page/东吴.md "wikilink")，现存结构建成于[北宋](../Page/北宋.md "wikilink")。该塔在历史上进行过多次整修，但在历代整修过程中被添加了大量非宋代风格的结构。1949年以后，这些后来添加的结构被先后拆除，并依照[宋代风格重建](../Page/宋代建筑.md "wikilink")，基本恢复了塔的原貌。2006年5月25日，该塔单独被国务院公布为第六批[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。

### 弥勒殿

弥勒殿内供奉[弥勒菩萨像](../Page/弥勒菩萨.md "wikilink")。

### 天王殿

天王殿内供奉[四大天王像](../Page/四大天王.md "wikilink")。

### 大雄宝殿

[LonghuaTemple.jpg](https://zh.wikipedia.org/wiki/File:LonghuaTemple.jpg "fig:LonghuaTemple.jpg")
大雄宝殿内供奉三尊金身佛像：[毗卢遮那佛](../Page/毗卢遮那佛.md "wikilink")、[文殊菩萨](../Page/文殊菩萨.md "wikilink")、[普贤菩萨](../Page/普贤菩萨.md "wikilink")。内有一口古钟，于[明朝万历十四年铸造](../Page/明朝.md "wikilink")。大殿前供奉二十诸天，大殿后供奉十六罗汉。释迦牟尼佛像背后是一幅海岛壁塑，描绘的是善才童子求法图，海岛壁有[观世音菩萨像](../Page/观世音菩萨.md "wikilink")。

### 三圣殿

三圣殿内供奉[阿弥陀佛](../Page/阿弥陀佛.md "wikilink")、[观世音菩萨和](../Page/观世音菩萨.md "wikilink")[大势至菩萨的金身佛像](../Page/大势至菩萨.md "wikilink")。殿内有前任龙华寺方丈[明旸法师和已故中国佛教协会首任会长](../Page/明旸法师.md "wikilink")[圆瑛法师的手迹](../Page/圆瑛法师.md "wikilink")。

### 方丈室

方丈接待和说法的地方，通常外宾禁止入内。

### 藏经楼

藏经楼内收集有各种版本[大藏经和佛教经籍](../Page/大藏经.md "wikilink")，以及法器、古玩、历史珍贵文物等。

### 钟楼

钟楼高三层，悬挂青龙晚钟，其鐘建于清光绪二十年。

### 鼓楼

古代计时，击鼓，敲钟的地方。

## 相关条目

  - [龙华塔](../Page/龙华塔.md "wikilink")
  - [靜安寺](../Page/靜安寺.md "wikilink")
  - [真如寺](../Page/真如寺.md "wikilink")
  - [玉佛寺](../Page/玉佛寺.md "wikilink")

[L](../Category/上海佛寺.md "wikilink")
[L](../Category/汉族地区佛教全国重点寺院.md "wikilink")