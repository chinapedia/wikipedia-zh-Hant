**龙德**（921年五月-923年十月）是[后梁末帝](../Page/后梁.md "wikilink")[朱友贞的](../Page/梁末帝.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。[吴越太祖](../Page/吴越国.md "wikilink")[錢鏐同时用该年号到](../Page/錢鏐.md "wikilink")923年十二月；[闽太祖](../Page/闽_\(十国\).md "wikilink")[王審知亦用此年号](../Page/王審知.md "wikilink")（921年五月-923年三月）\[1\]。

## 纪年

| 龙德                             | 元年                             | 二年                             | 三年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元.md "wikilink") | 921年                           | 922年                           | 923年                           |
| [干支](../Page/干支.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") |

## 参见

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [天佑](../Page/天祐_\(唐朝\).md "wikilink")（909年至923年）：晉—[李存勗之年號](../Page/唐庄宗.md "wikilink")
      - [同光](../Page/同光.md "wikilink")（923年四月至926年四月）：[後唐](../Page/后唐.md "wikilink")—李存勗之年號
      - [天佑](../Page/天祐_\(唐朝\).md "wikilink")（907年四月至924年）：[岐](../Page/岐.md "wikilink")—[李茂貞之年號](../Page/李茂貞.md "wikilink")
      - [順義](../Page/顺义_\(南吴\).md "wikilink")（921年二月至927年十月）：[吳](../Page/杨吴.md "wikilink")—[楊溥之年號](../Page/楊溥_\(十國\).md "wikilink")
      - [宝大](../Page/宝大.md "wikilink")（924年正月至925年十二月）：[吳越](../Page/吴越国.md "wikilink")—[錢鏐之年號](../Page/錢鏐.md "wikilink")
      - [乾亨](../Page/乾亨_\(劉龑\).md "wikilink")（917年七月至925年十一月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [乾德](../Page/乾德_\(王衍\).md "wikilink")（919年正月至924年十二月）：前蜀—[王衍之年號](../Page/王衍_\(前蜀\).md "wikilink")
      - [神冊](../Page/神冊.md "wikilink")（916年十二月至922年正月）：[契丹](../Page/辽朝.md "wikilink")—[耶律阿保機之年號](../Page/耶律阿保機.md "wikilink")
      - [天赞](../Page/天赞.md "wikilink")（922年二月至926年二月）：契丹—耶律阿保機之年號
      - [同慶](../Page/同慶_\(于闐\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/李聖天.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年—933年）：[高麗太祖](../Page/高麗_\(918年－1392年\).md "wikilink")[王建之年號](../Page/高麗太祖.md "wikilink")
      - [延喜](../Page/延喜.md "wikilink")（901年-922年）：平安時代[醍醐天皇之年號](../Page/醍醐天皇.md "wikilink")
      - [延長](../Page/延長_\(醍醐天皇\).md "wikilink")（923年閏四月十一日至931年四月二十六日）：平安時代[醍醐天皇之年號](../Page/醍醐天皇.md "wikilink")

## 参考文献

  - 后梁年号：

<!-- end list -->

  - 吴越年号：

<!-- end list -->

  - 闽年号：

[Category:后梁年号](../Category/后梁年号.md "wikilink")
[Category:吴越年号](../Category/吴越年号.md "wikilink")
[Category:闽年号](../Category/闽年号.md "wikilink")
[Category:920年代中国政治](../Category/920年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月，146，151 ISBN 7101025129