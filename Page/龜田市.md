**龜田市**（）是過去位於[北海道](../Page/北海道.md "wikilink")[渡島支廳的一](../Page/渡島支廳.md "wikilink")[城市](../Page/城市.md "wikilink")，於1971年改制為市，但在兩年後的1973年12月1日被併入[函館市](../Page/函館市.md "wikilink")，過去的轄區相當於現在的函館市龜田支所的管轄範圍。

## 歷史

  - 1902年4月1日：[龜田郡龜田村](../Page/龜田郡.md "wikilink")、神山村、鍛治村、桔梗村、石川村合併為龜田村，並成為北海道二級村。\[1\]
  - 1919年4月1日：成為北海道一級村
  - 1962年1月1日：改制為龜田町。
  - 1971年11月1日：改制為龜田市。
  - 1973年12月1日：龜田市被併入[函館市](../Page/函館市.md "wikilink")。

## 交通

### 鐵路、電車

  - [日本國有鐵道](../Page/日本國有鐵道.md "wikilink")（現在[北海道旅客鐵道的前身](../Page/北海道旅客鐵道.md "wikilink")）
      - [函館本線](../Page/函館本線.md "wikilink")：[五稜郭站](../Page/五稜郭站.md "wikilink")
        - [桔梗站](../Page/桔梗站.md "wikilink")
      - [江差線](../Page/江差線.md "wikilink")：五稜郭站
  - [函館市交通局](../Page/函館市交通局.md "wikilink")
      - 本線：龜田町 - 大野新道 - 鐵道工場前 - 五稜郭站前

## 參考資料

<div class="references-small">

<references />

</div>

[Category:函館市](../Category/函館市.md "wikilink")
[Category:渡島管內](../Category/渡島管內.md "wikilink")

1.