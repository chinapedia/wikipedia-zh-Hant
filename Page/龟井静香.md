**龟井静香**（）[日本](../Page/日本.md "wikilink")[自民党前重要领导人之一](../Page/自由民主黨_\(日本\).md "wikilink")，进入政界前为[日本警察官员](../Page/日本警察.md "wikilink")，曾任[自民党政务调查会会长](../Page/自由民主黨政務調查會.md "wikilink")、[建设大臣](../Page/建设大臣.md "wikilink")、[运输大臣](../Page/运输大臣.md "wikilink")。

1998年，龟井与[森喜朗出現權力争斗](../Page/森喜朗.md "wikilink")，导致自民党[三冢派分裂](../Page/三冢派.md "wikilink")，龟井组建以自己为领袖的龟井集团；后龟井集团与另一派系旧渡边派合并为江藤-龟井派，龟井任该派的代理会长，后又出任自民党政调会长。2003年，江藤-龟井派会长[江藤隆美引退](../Page/江藤隆美.md "wikilink")，龟井继任会长，该派名称改为[龟井派](../Page/龟井派.md "wikilink")。

2005年，龟井因为強烈反對[日本邮政民营化而与](../Page/日本邮政民营化.md "wikilink")[小泉纯一郎闹翻](../Page/小泉纯一郎.md "wikilink")，退出自民党，另立[国民新党](../Page/国民新党.md "wikilink")；龟井派在自民党内影响力迅速衰弱，反造就小泉纯一郎及之後[安倍晉三領導的](../Page/安倍晉三.md "wikilink")[町村派成為自民黨領導的最大派閥](../Page/町村派.md "wikilink")。

2005年[第44屆日本眾議院議員總選舉](../Page/第44屆日本眾議院議員總選舉.md "wikilink")，龜井以[無黨籍身分在](../Page/無黨籍.md "wikilink")[廣島縣第6區參選](../Page/廣島縣第6區.md "wikilink")，擊敗自民黨提名的[堀江貴文](../Page/堀江貴文.md "wikilink")。

2009年[民主黨在](../Page/民主党_\(日本\).md "wikilink")[第45屆日本眾議院議員總選舉首次贏得大選後](../Page/第45屆日本眾議院議員總選舉.md "wikilink")，2009年9月16日，民主黨領導國民新黨及[社民黨與民主派組成](../Page/社會民主黨_\(日本\).md "wikilink")[聯合政府](../Page/聯合政府.md "wikilink")，龜井獲新任首相[鳩山由紀夫指定出任內閣府特命擔當大臣](../Page/鳩山由紀夫.md "wikilink")，掌管郵政改革。

2010年6月11日社民黨退出聯合政府後，因[菅直人决定放弃在国会本会期通过邮政改革方案](../Page/菅直人.md "wikilink")，龜井辞去大臣职务。2012年，龜井因不支持「社会保障和税制一体化改革相关法案」而退出國民新黨，2012年12月28日加入[绿之风](../Page/绿之风.md "wikilink")。2013年12月31日，綠之風解散，龜井成為無黨籍人士。

[Category:日本警察官僚](../Category/日本警察官僚.md "wikilink")
[Category:警察出身的政治人物](../Category/警察出身的政治人物.md "wikilink")
[Category:日本武術家](../Category/日本武術家.md "wikilink")
[Category:二战后日本政治人物](../Category/二战后日本政治人物.md "wikilink")
[Category:金融擔當大臣](../Category/金融擔當大臣.md "wikilink")
[Category:日本建设大臣](../Category/日本建设大臣.md "wikilink")
[Category:日本运输大臣](../Category/日本运输大臣.md "wikilink")
[Category:村山內閣閣僚](../Category/村山內閣閣僚.md "wikilink")
[Category:第二次橋本內閣閣僚](../Category/第二次橋本內閣閣僚.md "wikilink")
[Category:鳩山由紀夫內閣閣僚](../Category/鳩山由紀夫內閣閣僚.md "wikilink")
[Category:菅內閣閣僚](../Category/菅內閣閣僚.md "wikilink")
[Category:日本自由民主黨政務調查會長](../Category/日本自由民主黨政務調查會長.md "wikilink")
[Category:前日本自由民主黨黨員](../Category/前日本自由民主黨黨員.md "wikilink")
[Category:國民新黨黨員](../Category/國民新黨黨員.md "wikilink")
[Category:日本廢除死刑運動者](../Category/日本廢除死刑運動者.md "wikilink")
[Category:廣島縣出身人物](../Category/廣島縣出身人物.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")
[Category:日本眾議院議員
1979–1980](../Category/日本眾議院議員_1979–1980.md "wikilink")
[Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1990–1993](../Category/日本眾議院議員_1990–1993.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:日本眾議院議員
2003–2005](../Category/日本眾議院議員_2003–2005.md "wikilink")
[Category:日本眾議院議員
2005–2009](../Category/日本眾議院議員_2005–2009.md "wikilink")
[Category:日本眾議院議員
2009–2012](../Category/日本眾議院議員_2009–2012.md "wikilink")
[Category:日本眾議院議員
2012–2014](../Category/日本眾議院議員_2012–2014.md "wikilink")
[Category:日本眾議院議員
2014–2017](../Category/日本眾議院議員_2014–2017.md "wikilink")
[Category:廣島縣選出日本眾議院議員](../Category/廣島縣選出日本眾議院議員.md "wikilink")