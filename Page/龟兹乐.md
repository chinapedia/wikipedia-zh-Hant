**龟兹乐**是古[龟兹的音乐](../Page/龟兹.md "wikilink")。古龟兹人擅长音乐舞蹈；(唐)[玄奘在](../Page/玄奘.md "wikilink")《[大唐西域记](../Page/大唐西域记.md "wikilink")》写道：“屈支国……管弦伎乐特善诸国”\[1\]。龟兹乐器有[竖箜篌](../Page/竖箜篌.md "wikilink")、[琵琶](../Page/琵琶.md "wikilink")、[五弦](../Page/五弦.md "wikilink")、[笙](../Page/笙.md "wikilink")、[笛](../Page/笛.md "wikilink")、[箫](../Page/箫.md "wikilink")、[篦篥](../Page/篦篥.md "wikilink")、[毛员鼓](../Page/毛员鼓.md "wikilink")、[都眃鼓](../Page/都眃鼓.md "wikilink")、[答臘鼓](../Page/答臘鼓.md "wikilink")、[腰鼓](../Page/腰鼓.md "wikilink")、[羯鼓](../Page/羯鼓.md "wikilink")、[雞籹鼓](../Page/雞籹鼓.md "wikilink")、[銅鈸](../Page/銅鈸.md "wikilink")、[貝](../Page/貝.md "wikilink")、[彈箏](../Page/彈箏.md "wikilink")、[候提鼓](../Page/候提鼓.md "wikilink")、[齊鼓](../Page/齊鼓.md "wikilink")、[檐鼓等二十种](../Page/檐鼓.md "wikilink")。龟兹乐曲有：《万岁》、《藏钩》、《七夕相逢》、《投壶》、
《舞席》、《同心髻》、《玉女行觞》、《神仙留客》、《掷砖续命》、《斗鹳子》、《斗百草》、《泛龙舟》、《还旧宫》、《长乐花》、《十二时》、《善善摩花》、《婆伽儿》、《小天》、《疏勒盐》等。

## 历史

  - [前秦](../Page/前秦.md "wikilink")[建元十八年](../Page/建元.md "wikilink")（382年）苻堅之大将[吕光灭龟兹](../Page/吕光.md "wikilink")，将**龟兹乐**带到[凉州](../Page/凉州.md "wikilink")。吕光亡后，**龟兹乐**分散。[后魏平定中原](../Page/后魏.md "wikilink")，此后[北魏](../Page/北魏.md "wikilink")[太武帝](../Page/太武帝.md "wikilink")[拓拔焘把](../Page/拓拔焘.md "wikilink")《龟兹乐》带到华北平城，“得其伶人器服，并择而存之”，\[2\]重新获得**龟兹乐**。
  - [南北朝](../Page/南北朝.md "wikilink")[北周](../Page/北周.md "wikilink")[周武帝时有](../Page/宇文邕.md "wikilink")[龟兹人苏祗婆](../Page/龟兹.md "wikilink")，从[突厥皇后入国](../Page/突厥.md "wikilink")，善[胡琵琶](../Page/胡琵琶.md "wikilink")，其所奏琵琶乐有七声:[宫声](../Page/宫声.md "wikilink")、[南吕声](../Page/南吕声.md "wikilink")、[角声](../Page/角声.md "wikilink")、[变徵声](../Page/变徵声.md "wikilink")、[徵声](../Page/徵声.md "wikilink")、[羽声](../Page/羽声.md "wikilink")、[变宫声](../Page/变宫声.md "wikilink")。
  - [隋文帝](../Page/隋文帝.md "wikilink")[开皇初](../Page/开皇.md "wikilink")（581年）定令置《七部乐》：《国伎》、《清商伎》、《高丽伎》、《天竺伎》、《安国伎》、《[龟兹伎](../Page/龟兹伎.md "wikilink")》、《文康伎》。[开皇中](../Page/开皇.md "wikilink")（590年），**龟兹乐器**大盛于朝野。当时著名乐师有曹妙达、王长通、李士衡、郭金乐、安进贵等人，精通龟兹弦乐、管乐，新声奇变，公王之间，争相慕尚。
  - 隋[大业中](../Page/大业.md "wikilink")（611年）隋炀帝定《九部乐》：《清乐》、《西凉乐》、《**龟兹乐**》、《天竺乐》、《康国乐》、《[疏勒乐](../Page/疏勒乐.md "wikilink")》、《安国乐》、《高丽乐》、《礼毕乐》。
  - 龟兹音乐舞蹈对唐代也有重要影响。唐朝设乐工196人，《[新唐书](../Page/新唐书.md "wikilink")》记载“分四部：一、**龟兹部**，二、大鼓部，三、胡部，四、军乐部。龟兹部，有羯鼓、揩鼓、腰鼓、鸡娄鼓、短笛、大小觱篥、拍板，皆八；长短箫、横笛、方响、大铜钹、贝，皆四。凡工八十八人，分四列，属舞筵四隅，以合节鼓。大鼓部，以四为列，凡二十四，居龟兹部前”。

根据历史学家[向达考证](../Page/向达.md "wikilink")，龟兹琵琶七调起源于[印度](../Page/印度.md "wikilink")[北宗音乐](../Page/北宗音乐.md "wikilink")。**龟兹乐**娑陀力（宫声）来自印度北宗音乐的Shadja，般赡调（羽声）来自印度北宗音乐的Panchama调\[3\]。龟兹音乐传入中国后，在唐代演变成为唐代[佛曲](../Page/佛曲.md "wikilink")。

## 參考文獻

[Category:龟兹](../Category/龟兹.md "wikilink")
[Category:西域艺术](../Category/西域艺术.md "wikilink")
[Category:中國傳統音樂](../Category/中國傳統音樂.md "wikilink")

1.  ；(唐)[玄奘在](../Page/玄奘.md "wikilink")《[大唐西域记](../Page/大唐西域记.md "wikilink")·屈支国》
2.  杜佑《通典》142卷
3.  [向达](../Page/向达.md "wikilink")《龟兹苏祗婆琵琶七调考原》，收入所著《[唐代长安与西域文明](../Page/唐代长安与西域文明.md "wikilink")》一书，河北教育出版社，2001
    ISBN 7-5434-4237-X