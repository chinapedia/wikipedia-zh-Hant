**龜背芋**（[学名](../Page/学名.md "wikilink")：），又稱為**龜背竹**、**蓬萊蕉**及**電信蘭**\[1\]，是[天南星科](../Page/天南星科.md "wikilink")[龜背芋属](../Page/龜背芋属.md "wikilink")[植物](../Page/植物.md "wikilink")。

龜背芋適應性強，性喜温暖，潮湿的环境，但也耐陰、耐陽及有一定的耐旱性，原产于[墨西哥的](../Page/墨西哥.md "wikilink")[热带雨林中](../Page/热带雨林.md "wikilink")。为著名大型观赏植物。\[2\]

## 形态

常绿[藤本植物](../Page/藤本植物.md "wikilink")，叶革质，茎干粗壮，下部常生有褐色[气生根](../Page/气生根.md "wikilink")。幼叶呈心形，植株在生长过程中的新发叶片逐渐会有不规则的羽状裂口，其因像龟背而得名。花为黄白色的[肉穗花序](../Page/肉穗花序.md "wikilink")，外具白色的[佛焰苞](../Page/佛焰苞.md "wikilink")。果实为緑色柱状[聚果](../Page/聚果.md "wikilink")。

果实有凤梨香气，但含草酸钙（蓚酸）结晶，会刺激皮肤及黏膜组织，带来些微刺痒的不适感。大量接触的话，可能会让皮肤发红搔痒，黏膜肿大发炎，故不宜多吃。但亦由于它的味美和古怪的样子，所以英文名叫monstera
deliciousa 。

## 参考文献

  -

</div>

[藤](../Category/藥用植物.md "wikilink")
[龟背竹](../Category/龟背竹属.md "wikilink")

1.
2.  [羽裂蔓綠絨 (又稱龜背芋)](http://www.ttvs.cy.edu.tw/kcc/961016pl/yu.htm)