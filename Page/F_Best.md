《**"F"
Best**》是[薛凱琪首張新曲加精選大碟](../Page/薛凱琪.md "wikilink")，於2007年6月12日推出，收錄《下次下次》及《樓梯轉角》，以及與[房祖名合唱的](../Page/房祖名.md "wikilink")《不知不覺》，合共三首新歌。《不知不覺》該曲，其實[薛凱琪早於自己的十一團火音樂會中已演繹過一次](../Page/薛凱琪.md "wikilink")，唯該次是由[薛凱琪獨唱](../Page/薛凱琪.md "wikilink")，另外亦為電影《[早熟](../Page/早熟.md "wikilink")》插曲。其餘亦收錄了15首[精選](../Page/精選.md "wikilink")，當然少不了《奇洛李維斯回信》，《小黑與我》等帶紅[薛凱琪的](../Page/薛凱琪.md "wikilink")[作品](../Page/作品.md "wikilink")。

## 曲目

CD：

| 次序   | 歌名         | 作曲                                              | 填詞                                            | 編曲                               | 監製                                   |
| ---- | ---------- | ----------------------------------------------- | --------------------------------------------- | -------------------------------- | ------------------------------------ |
| 1\.  | 下次下次       | [張家誠](../Page/張家誠.md "wikilink")                | [黃偉文](../Page/黃偉文.md "wikilink")              | Ted Lo                           | James Ting／崔炎德                       |
| 2\.  | 樓梯轉角       | [張家誠](../Page/張家誠.md "wikilink")                | [黃偉文](../Page/黃偉文.md "wikilink")              | Danny Leung                      | 崔炎德                                  |
| 3\.  | 不知不覺       | 黃韻仁／[房祖名](../Page/房祖名.md "wikilink")            | [房祖名](../Page/房祖名.md "wikilink")              | 黃韻仁                              | 黃韻仁／[房祖名](../Page/房祖名.md "wikilink") |
| 4\.  | 糖不甩        | [方大同](../Page/方大同.md "wikilink")                | [周耀輝](../Page/周耀輝.md "wikilink")              | [方大同](../Page/方大同.md "wikilink") | 崔炎德／Evi Yang                         |
| 5\.  | 小峽谷之1234   | Edward Chan / Charles Lee                       | [周耀輝](../Page/周耀輝.md "wikilink")              | Edward Chan / Charles Lee        | 崔炎德／Evi Yang                         |
| 6\.  | Dear Fiona | [YUI](../Page/YUI.md "wikilink")                | [游思行](../Page/游思行.md "wikilink")              | Adrain Chan                      | 崔炎德／Evi Yang                         |
| 7\.  | 有隻雀仔       | 阿BERT                                           | [黃偉文](../Page/黃偉文.md "wikilink")              | Billy Chan                       | 崔炎德／Evi Yang                         |
| 8\.  | 尋找獨角獸      | James Ting                                      | [黃偉文](../Page/黃偉文.md "wikilink")              | James Ting                       | James Ting / Evi Yang                |
| 9\.  | 白色戀人       | 蔡德才                                             | [周耀輝](../Page/周耀輝.md "wikilink")              | 蔡德才                              | James Ting / Evi Yang                |
| 10\. | 快樂到天亮      | [方樹樑](../Page/方樹樑.md "wikilink")／George Michael | [林夕](../Page/林夕.md "wikilink")／George Michael | [方樹樑](../Page/方樹樑.md "wikilink") |                                      |
| 11\. | 男孩像你       | [徐繼宗](../Page/徐繼宗.md "wikilink")                | [黃偉文](../Page/黃偉文.md "wikilink")              | Billy Chan                       |                                      |
| 12\. | 小黑與我       | [王菀之](../Page/王菀之.md "wikilink")                | [林夕](../Page/林夕.md "wikilink")                | Billy Chan                       | 崔炎德                                  |
| 13\. | 886        | Charles Lee                                     | [周耀輝](../Page/周耀輝.md "wikilink")              | Edward Chan / Charles Lee        | 崔炎德／Evi Yang                         |
| 14\. | 南瓜車        | 翁瑋盈                                             | [黃偉文](../Page/黃偉文.md "wikilink")              | [方樹樑](../Page/方樹樑.md "wikilink") | 崔炎德                                  |
| 15\. | 麥當娜一吻      | Edmond Tsang                                    | [周耀輝](../Page/周耀輝.md "wikilink")              | 許創基／Edmond Tsang                 | James Ting / Evi Yang                |
| 16\. | 上帝是男孩      | 鐘達茵                                             | [周耀輝](../Page/周耀輝.md "wikilink")              | Billy Chan                       | James Ting / Evi Yang                |
| 17\. | XBF        | [張佳添](../Page/張佳添.md "wikilink")                | [黃偉文](../Page/黃偉文.md "wikilink")              | [張佳添](../Page/張佳添.md "wikilink") | James Ting / Evi Yang                |
| 18\. | 奇洛李維斯回信    | 方樹樑                                             | [黃偉文](../Page/黃偉文.md "wikilink")              | 方樹樑                              | 崔炎德／Evi Yang                         |

DVD：

1.  有效期限
2.  下次下次（MV）
3.  樓梯轉角（MV）
4.  糖不甩（MV）
5.  糖不甩（Karaoke）
6.  小峽谷之1234 (MV)
7.  小峽谷之1234 (Karaoke)
8.  Dear Fiona (MV)
9.  Dear Fiona (Karaoke)
10. 尋找獨角獸（MV）
11. 尋找獨角獸（Karaoke）
12. 男孩像你（MV）
13. 男孩像你（Karaoke）
14. 小黑與我（MV）
15. 小黑與我（Karaoke）
16. 886 (MV)
17. 886 (Karaoke)
18. 南瓜車（MV）
19. 南瓜車（Karaoke）
20. 麥當娜一吻（MV）
21. 麥當娜一吻（Karaoke）
22. XBF (MV)
23. XBF (Karaoke)
24. 奇洛李維斯回信（MV）
25. 奇洛李維斯回信（Karaoke）

## 曲目資料

  - 《下次下次》及《樓梯轉角》均為2007年冠軍歌曲。
  - 兩首全新歌曲《下次下次》及《樓梯轉角》均由[黃偉文填詞](../Page/黃偉文.md "wikilink")。
  - 其餘一首《不知不覺》則由[房祖名親自](../Page/房祖名.md "wikilink")[作曲](../Page/作曲家.md "wikilink")，[填詞及](../Page/填詞.md "wikilink")[監製](../Page/監製.md "wikilink")，亦由[黃韻仁協助](../Page/黃韻仁.md "wikilink")[房祖名監製](../Page/房祖名.md "wikilink")，另外亦為電影[早熟插曲](../Page/早熟.md "wikilink")。。
  - 《有效期限》是一個[音樂電影](../Page/音樂電影.md "wikilink")，全程為一套13分56秒的[短片](../Page/短片.md "wikilink")，由[金卓執行](../Page/金卓.md "wikilink")[導演一職](../Page/导演.md "wikilink")，並找來[周孝安與](../Page/周孝安.md "wikilink")[薛凱琪合演該](../Page/薛凱琪.md "wikilink")[音樂電影](../Page/音樂電影.md "wikilink")。

[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")
[Category:薛凱琪音樂專輯](../Category/薛凱琪音樂專輯.md "wikilink")