# wikipedia.zh-Hant

本项目目标是导出维基百科繁体中文内容。目前有些已知问题：

* 部分词条在转换过程中报错。
* 图片无法显示
* 链接引用的位置不对
* 处理多语言的 wikitext 语法

本项目目前只有接受对 `Script` 目录的 PR，里面是 [pandoc](https://github.com/chinapedia/pandoc) 用到的过滤器脚本。也可以给 [mediawiki-to-gfm](https://github.com/chinapedia/mediawiki-to-gfm) 发 PR，改进数据生成工具。
